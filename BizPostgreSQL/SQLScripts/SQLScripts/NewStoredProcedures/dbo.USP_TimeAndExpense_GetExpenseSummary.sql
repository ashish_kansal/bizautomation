GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
                                    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_TimeAndExpense_GetExpenseSummary')
DROP PROCEDURE USP_TimeAndExpense_GetExpenseSummary
GO
CREATE PROCEDURE [dbo].[USP_TimeAndExpense_GetExpenseSummary]
(
	@numDomainID NUMERIC(18,0)
	,@dtFromDate DATE
	,@dtToDate DATE
	,@vcEmployeeName VARCHAR(200)
	,@vcTeams VARCHAR(MAX)
	,@tintPayrollType TINYINT
	,@ClientTimeZoneOffset INT
)
AS
BEGIN
	DECLARE @TEMP TABLE
	(
		numType TINYINT
		,monExpense DECIMAL(20,5)
	)

	INSERT INTO @TEMP
	(
		numType
		,monExpense
	)
	SELECT
		numType
		,monExpense
	FROM
		UserMaster
	INNER JOIN
		AdditionalContactsInformation
	ON
		UserMaster.numUserDetailId = AdditionalContactsInformation.numContactId
	OUTER APPLY
	(
		SELECT 
			numType
			,monExpense
		FROM
			dbo.fn_GetPayrollEmployeeExpense(@numDomainID,UserMaster.numUserDetailId,@dtFromDate,@dtToDate,@ClientTimeZoneOffset)
	) TEMPExpense
	WHERE
		UserMaster.numDomainID=@numDomainID
		AND ISNULL(UserMaster.bitActivateFlag,0) = 1
		AND (ISNULL(@vcEmployeeName,'') = '' OR AdditionalContactsInformation.vcFirstName LIKE CONCAT('%',@vcEmployeeName,'%') OR AdditionalContactsInformation.vcLastName LIKE CONCAT('%',@vcEmployeeName,'%'))
		AND (ISNULL(@vcTeams,'') = '' OR AdditionalContactsInformation.numTeam IN (SELECT Id FROM dbo.SplitIDs(@vcTeams,',')))
		AND 1 = (CASE WHEN @tintPayrollType=0 THEN 1 ELSE (CASE WHEN ISNULL(UserMaster.tintPayrollType,0)=@tintPayrollType THEN 1 ELSE 0 END) END)


	SELECT 
		ISNULL((SELECT CONVERT(varchar(5),DATEADD(MINUTE,ISNULL(SUM(monExpense),0),0),108) FROM @TEMP),0) monTotalExpense
		,ISNULL((SELECT CONVERT(varchar(5),DATEADD(MINUTE,ISNULL(SUM(monExpense),0),0),108) FROM @TEMP WHERE numType = 1),0) monTotalBillableExpense
		,ISNULL((SELECT CONVERT(varchar(5),DATEADD(MINUTE,ISNULL(SUM(monExpense),0),0),108) FROM @TEMP WHERE numType = 2),0) monTotalReimbursableExpense
		,ISNULL((SELECT CONVERT(varchar(5),DATEADD(MINUTE,ISNULL(SUM(monExpense),0),0),108) FROM @TEMP WHERE numType = 6),0) monTotalBillableReimbursableExpense
END
GO