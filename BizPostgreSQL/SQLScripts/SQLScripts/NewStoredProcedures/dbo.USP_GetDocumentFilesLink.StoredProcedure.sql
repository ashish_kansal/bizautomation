SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetDocumentFilesLink')
	DROP PROCEDURE USP_GetDocumentFilesLink
GO
CREATE PROCEDURE [dbo].[USP_GetDocumentFilesLink]    
	@numRecID AS NUMERIC(18) = 0,    
	@numDomainID AS NUMERIC(18) = 0,
	@calledFrom AS CHAR = NULL
AS
BEGIN   
	IF @calledFrom = 'P'
		SELECT 
			numGenericDocID
			,VcFileName
			,cUrlType
			,ISNULL(vcFileType,'') vcFileType
			,(CASE WHEN ISNULL(VcDocName,'') = '' THEN '-' ELSE VcDocName END) VcDocName
			,ISNULL(numFormFieldGroupId,0) numFormFieldGroupId
		FROM 
			GenericDocuments
		WHERE 
			numDomainID = @numDomainID
			AND numRecID = @numRecID 
			AND ISNULL(@numRecID,0) > 0
			AND ISNULL(vcFileName,'') <> ''
	ELSE IF @calledFrom = 'T'
		SELECT 
			numGenericDocID
			,VcFileName
			,cUrlType
			,ISNULL(vcFileType,'') vcFileType
			,(CASE WHEN ISNULL(VcDocName,'') = '' THEN '-' ELSE VcDocName END) VcDocName
			,ISNULL(numFormFieldGroupId,0) numFormFieldGroupId
		FROM 
			GenericDocuments
		WHERE 
			numDomainID = @numDomainID
			AND numRecID = @numRecID 
			AND ISNULL(@numRecID,0) > 0
			AND vcDocumentSection = 'T'
			AND ISNULL(vcFileName,'') <> ''
	ELSE IF @calledFrom = 'O'
		SELECT 
			numGenericDocID
			,VcFileName
			,cUrlType
			,ISNULL(vcFileType,'') vcFileType
			,(CASE WHEN ISNULL(VcDocName,'') = '' THEN '-' ELSE VcDocName END) VcDocName
			,ISNULL(numFormFieldGroupId,0) numFormFieldGroupId
		FROM 
			GenericDocuments
		WHERE 
			numDomainID = @numDomainID
			AND numRecID = @numRecID 
			AND ISNULL(@numRecID,0) > 0
			AND vcDocumentSection = 'O'
			AND ISNULL(vcFileName,'') <> ''
	ELSE IF @calledFrom = 'E'
		SELECT 
			numGenericDocID
			,VcFileName
			,cUrlType
			,ISNULL(vcFileType,'') vcFileType
			,(CASE WHEN ISNULL(VcDocName,'') = '' THEN '-' ELSE VcDocName END) VcDocName
			,ISNULL(numFormFieldGroupId,0) numFormFieldGroupId
		FROM 
			GenericDocuments
		WHERE 
			numDomainID = @numDomainID
			AND numRecID = @numRecID
			AND cURLType='L' 

	
END
GO
