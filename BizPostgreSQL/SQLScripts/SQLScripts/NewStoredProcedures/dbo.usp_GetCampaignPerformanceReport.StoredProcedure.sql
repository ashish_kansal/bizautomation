set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go

-- exec USP_GetCampaignPerformanceReport @numDomainID=72,@tintCRMtype=2,@dtStartDate='2009-02-09 00:00:00:000',@dtEndDate='2010-02-05 00:00:00:000'
-- exec USP_GetCampaignPerformanceReport @numDomainID=72,@tintCRMtype=0,@dtStartDate='2009-01-04 00:00:00:000',@dtEndDate='2010-02-03 00:00:00:000'
-- exec USP_GetCampaignPerformanceReport @numDomainID=72,@tintCRMtype=0,@dtStartDate='2006-02-10 00:00:00:000',@dtEndDate='2010-02-06 00:00:00:000'
/* Note:
Performace Report:The "Based On" drop-down shows 30, 60, 90, days etc... but when we display cost it's always for 1 month (assuming we're talking about a monthly campaign), 
we're not multiplying cost x 2 if 60 days is selected, etc.. (60 = x 2  90 x 3, 120 = x 4, 240 = x 8, 360 = x 12)  Idea - Why don't we just have a check box 
below the "Cost" field in campaign details called "Monthly Campaign", which if checked multiplies the cost value for the campaign performance when multiple months are slected as mentioned.
*/
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_GetCampaignPerformanceReport')
DROP PROCEDURE usp_GetCampaignPerformanceReport
GO
CREATE PROCEDURE [dbo].[usp_GetCampaignPerformanceReport]
               @numDomainID NUMERIC(9),
               @tintCRMtype TINYINT,
               @dtStartDate DATETIME,
               @dtEndDate   DATETIME
AS
	BEGIN
		DECLARE @Val INT
		SELECT @Val = ROUND(CONVERT(float,DATEDIFF(day,@dtStartDate,@dtEndDate))/30,0)--		SELECT ROUND(CONVERT(float,75)/30,0)
		IF ISNULL(@Val,0) = 0 
			SET @Val = 1
		--PRINT @Val
	
		-- Online Campaign, Will work based on Dates provided 
		SELECT 1
----		SELECT Y.* FROM ( SELECT X.*, CAST(dbo.[GetRecordsCountforCampaign](X.[numCampaignID],@dtStartDate,@dtEndDate,1,@tintCRMtype) AS DECIMAL(9,0)) AS RecordsCreated ,
----						  CASE @tintCRMtype WHEN 2 
----											THEN ISNULL(dbo.GetIncomeforCampaign(X.numCampaignId,2,@dtStartDate,@dtEndDate),0)
----											ELSE ''
----						  END AS AmtFromOrders,
----						  CASE @tintCRMtype WHEN 2 
----											THEN ISNULL(dbo.GetIncomeforCampaign(X.[numCampaignId],4,@dtStartDate,@dtEndDate),0)
----											ELSE ''
----						  END AS ROI
----						  FROM  
----						  (SELECT DISTINCT CM.numCampaignId,
----						    				vcCampaignName AS CampaignName,
----											CASE WHEN CM.bitIsMonthly = 1 THEN (ISNULL(((SELECT SUM(monAmount) FROM dbo.BillDetails BD JOIN dbo.BillHeader BH ON BD.numBillID = BH.numBillID AND numCampaignId = CM.numCampaignId GROUP BY numCampaignId)),0) * @Val)
----											ELSE isnull(((SELECT SUM(monAmount) FROM dbo.BillDetails BD JOIN dbo.BillHeader BH ON BD.numBillID = BH.numBillID AND numCampaignId = CM.numCampaignId GROUP BY numCampaignId)),0) END AS  monCampaignCost
----						   FROM     CampaignMaster CM
----						   WHERE CM.numDomainID = @numDomainID
----						   AND CM.[bitIsOnline] = 1
----						   AND CM.bitActive =1 -- bug fix 1440 		 
----						  ) X 
----	   
----						) Y
----		ORDER BY Y.RecordsCreated DESC 
    
    --Offline Campaign, Will not consider date.. It will show performance of whole time period.
    SELECT numCampaignId,SUM(monAmount) AS monAmount INTO #tempBill FROM dbo.BillHeader BH JOIN dbo.BillDetails BD 
																ON BD.numBillID = BH.numBillID WHERE BH.numDomainID = @numDomainID
																and isnull(numCampaignId,0)>0 GROUP BY numCampaignId
																
    SELECT DISTINCT CM.numCampaignId,
                    vcCampaignName AS CampaignName,
                    CASE WHEN CM.bitIsMonthly = 1 THEN (ISNULL(t.monAmount,0) * @Val)
						 ELSE isnull(t.monAmount,0) 
					END AS  monCampaignCost,
                    CAST(dbo.[GetRecordsCountforCampaign](CM.[numCampaignID],@dtStartDate,@dtEndDate,0,@tintCRMtype) AS DECIMAL(9,0)) AS RecordsCreated,
                    CASE @tintCRMtype 
						 WHEN 2 THEN ISNULL(dbo.GetIncomeforCampaign(CM.numCampaignId,2,@dtStartDate,@dtEndDate),0)
						 ELSE ''
                    END AS AmtFromOrders,
                    CASE @tintCRMtype 
						 WHEN 2 THEN ISNULL(dbo.GetIncomeforCampaign(CM.numCampaignId,4,@dtStartDate,@dtEndDate),0)
						 ELSE ''
                    END AS ROI,
                    CM.[intLaunchDate],
                    CM.[intEndDate]
    FROM     CampaignMaster CM LEFT JOIN #tempBill t ON CM.numCampaignId = t.numCampaignId
    WHERE    CM.numDomainID = @numDomainID
             AND CM.[bitIsOnline] <> 1
             AND CM.bitActive =1 -- bug fix 1440
    ORDER BY CampaignName
  END

