IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetDivPartnerAllContacts')
DROP PROCEDURE USP_GetDivPartnerAllContacts
GO
CREATE PROCEDURE [dbo].[USP_GetDivPartnerAllContacts]                                                                                    
 @numDomainID  numeric=0                   
AS                                                                       
BEGIN   
	
	SELECT DISTINCT A.numContactId,A.vcFirstName+' '+A.vcLastName AS vcGivenName FROM DivisionMaster AS O
	LEFT JOIN AdditionalContactsInformation AS A 
	ON O.numPartenerContact=A.numContactId
	WHERE A.numDomainID=@numDomainID AND ISNULL(O.numPartenerContact,0)>0 AND A.vcGivenName<>''

END
