--Usp_GetHelpByCategory ''
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='Usp_GetHelpByCategory')
DROP PROCEDURE Usp_GetHelpByCategory
GO
CREATE PROCEDURE Usp_GetHelpByCategory
    @helpcategory NUMERIC = 0
AS 
    BEGIN      
			SELECT ROW_NUMBER() OVER(ORDER BY intDisplayOrder ASC) AS row,
				c.[numHelpCategoryID],
                c.[vcCatecoryName],
                c.[bitLinkPage],
                c.[numPageID],
                c.[numParentCatID],
                c.[tintLevel],
                h.[numhelpID],
                h.[helpcategory],
                h.[helpheader],
                h.[helpMetatag],
                h.[helpDescription],
                h.[helpPageUrl],
                h.[helpshortdesc],
                h.[helpLinkingPageURL],
                ISNULL(h.intDisplayOrder, 0) intDisplayOrder,
                C.vcCatecoryName
        FROM    helpmaster h
                LEFT OUTER JOIN helpCategories c ON h.[helpcategory] = c.numHelpCategoryID
        WHERE   helpCategory = @helpcategory
                OR @helpcategory = 0
    END      
