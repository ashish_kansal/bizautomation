GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ReportDashboard_GetForDataCache')
DROP PROCEDURE dbo.USP_ReportDashboard_GetForDataCache
GO
CREATE PROCEDURE [dbo].[USP_ReportDashboard_GetForDataCache]

AS 
BEGIN
	SELECT 
		ReportDashboard.numDomainID
		,ReportDashboard.numUserCntID
		,ReportDashboard.numDashBoardID
		,ReportDashboard.numReportID
		,ISNULL(ReportListMaster.bitDefault,0) bitDefault
		,ISNULL(ReportListMaster.intDefaultReportID,0) intDefaultReportID
		,ISNULL(ReportListMaster.tintReportType,0) tintReportType
	FROM
		ReportDashboard
	INNER JOIN
		Domain
	ON
		ReportDashboard.numDomainID = Domain.numDomainId
	INNER JOIN
		Subscribers
	ON
		Domain.numDomainId = Subscribers.numTargetDomainID
	INNER JOIN
		ReportListMaster
	ON
		ReportDashboard.numReportID = ReportListMaster.numReportID
	WHERE
		CAST(Subscribers.dtSubEndDate AS DATE) >= CAST(GETUTCDATE() AS dATE)
		AND (DATEPART(HOUR,GETDATE()) >= 0 AND DATEPART(HOUR,GETDATE()) <= 7) -- BETWEEN 0 AM TO 7 AM
		AND (ReportDashboard.dtLastCacheDate IS NULL OR ReportDashboard.dtLastCacheDate <> CAST(GETUTCDATE() AS DATE))
END
GO