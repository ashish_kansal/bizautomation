GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_ManageWebAPIOrderReports' ) 
    DROP PROCEDURE USP_ManageWebAPIOrderReports
GO
--created by Joseph
CREATE PROC [dbo].[USP_ManageWebAPIOrderReports]
		@numOrdReportId AS NUMERIC(9),
          @numDomainID AS NUMERIC(9),
          @numWebApiId AS NUMERIC(9),
          @vcWebApiOrderReportId AS VARCHAR(25),
          @tintStatus AS TINYINT,
          @RStatus AS TINYINT = 0,
          @ReportTypeId AS TINYINT = 0

AS
  BEGIN
  DECLARE @Check as integer
  IF @numOrdReportId =0
	  BEGIN
		SELECT @Check = COUNT(*) FROM   [WebApiOrderReports] WHERE  [numDomainId] = @numDomainID AND [WebApiId] = @numWebApiId 
		AND vcWebApiOrderReportId = @vcWebApiOrderReportId AND tintStatus = 0
  		IF @Check =0    
		 BEGIN
  		 INSERT INTO [WebApiOrderReports]
						([numDomainId],
						[WebApiId],
						[vcWebApiOrderReportId],
						[tintStatus],
						[dtCreated],
						RStatus,
						tintReportTypeId)
			VALUES     (@numDomainID,
						@numWebApiId,
						@vcWebApiOrderReportId,
						@tintStatus,
						GETDATE(),
						@RStatus,
						@ReportTypeId)
	  END  
  END
  ELSE 
  IF @numOrdReportId <> 0
	  BEGIN
  			UPDATE [WebApiOrderReports]
			SET    [tintStatus] = @tintStatus,[dtModified] = GETDATE()
			WHERE  [numDomainId] = @numDomainID AND [WebApiId] = @numWebApiId
			 AND [vcWebApiOrderReportId] = @vcWebApiOrderReportId
					
	  END
  END