GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_SaveJournalDetailsForComission')
DROP PROCEDURE USP_SaveJournalDetailsForComission
GO
CREATE PROCEDURE [dbo].[USP_SaveJournalDetailsForComission]                                                                                         
@numJournalId as numeric(9)=0,                                                                                                  
@strRow as text='',                                                                                  
@Mode as numeric(9),                                                      
@RecurringMode as tinyint=0,             
@numDomainId as numeric(9)=0,                              
@numOppId as numeric(9)=0,  
@numCheckNo as numeric(9),
@numReturnID AS NUMERIC(9)=0                                                                                            
As                                                        
Begin                                                  
If @RecurringMode=0                                                                
 Begin                                                                                                       
  If convert(varchar(100),@strRow) <> ''                                                                                                                    
   Begin
		Declare @hDoc3A int                                                                                                                                                                        
    EXEC sp_xml_preparedocument @hDoc3A OUTPUT, @strRow                                                                                                                     
    If @Mode=0                                                                                  
     Begin                                                                                                           
      Insert Into General_Journal_Details(numJournalId,numDebitAmt,numCreditAmt,numChartAcntId,varDescription,numcontactid,numDomainId,bitReconcile,  
      numCurrencyID,fltExchangeRate,monOrgCreditAmt,monOrgDebitAmt,numoppitemtCode,chBizDocItems)                    
      Select * from (                                                                                                                     
      Select * from OPenXml (@hDoc3A,'/NewDataSet/Table1 [TransactionId=0]',2)                                                                                                                      
      With(                                                                                                                    
      numJournalId numeric(9),                                                                                                                    
      numDebitAmt DECIMAL(20,5),                                                                                                                     
      numCreditAmt DECIMAL(20,5),                                                                                                                      
      numChartAcntId numeric(9),                                                                                                                    
      varDescription varchar(8000),                                                                                                                    
      numcontactid numeric(9),                                                                                                        
      numDomainId numeric(9),            
      bitReconcile bit,  
      numCurrencyID numeric(9),  
      fltExchangeRate float,  
      monOrgCreditAmt DECIMAL(20,5),  
      monOrgDebitAmt DECIMAL(20,5),numoppitemtCode NUMERIC(9),chBizDocItems NCHAR(10)    
      ))X                                                                                                          
	END                                                                                                                    
   end
  end
 END