GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_MassSalesFulfillmentBatch_GetAll')
DROP PROCEDURE dbo.USP_MassSalesFulfillmentBatch_GetAll
GO

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[USP_MassSalesFulfillmentBatch_GetAll]
(
	@numDomainID NUMERIC(18,0)
)
AS 
BEGIN
	SELECT
		ID
		,vcName
	FROM
		MassSalesFulfillmentBatch
	WHERE
		numDomainID=@numDomainID
		AND ISNULL(bitEnabled,0) = 1
	ORDER BY
		vcName
END
GO