SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetEDICustomFields')
DROP PROCEDURE dbo.USP_GetEDICustomFields
GO


/****** Author :  Priya Sharma
		Date : 19 June 2018
 ******/
CREATE PROCEDURE [dbo].[USP_GetEDICustomFields]  
  
	 @numDomainID NUMERIC(18,0)
	,@Grp_id AS NUMERIC(5)
	,@RecID AS NUMERIC(9)
  AS

  BEGIN

	  IF @Grp_id = 19  -- Opp/Order 
	  BEGIN

	    SELECT CFM.Fld_id
			  ,CFM.Fld_label
			  ,CFM.Fld_type
			  ,CFM.Grp_id 
			  ,OPPEDI.FldDTLID
			  ,OPPEDI.Fld_Value		 
		  
		FROM CFW_Fld_Master CFM 
		LEFT JOIN CFW_Fld_Values_Opp_EDI OPPEDI
		ON CFM.Fld_ID = OPPEDI.Fld_id AND OPPEDI.RecId = @RecID	
		WHERE  CFM.Grp_id=@Grp_id AND CFM.numDomainID = @numDomainID

	  END

	  IF @Grp_id = 20  --- Item
	  BEGIN

	    SELECT CFM.Fld_id
			  ,CFM.Fld_label
			  ,CFM.Fld_type
			  ,CFM.Grp_id 
			  ,ITEMEDI.FldDTLID
			  ,ITEMEDI.Fld_Value		  
		  
		FROM CFW_Fld_Master CFM
		LEFT JOIN  CFW_FLD_Values_Item_EDI ITEMEDI
		ON CFM.Fld_ID = ITEMEDI.Fld_id AND ITEMEDI.RecId = @RecID		
		WHERE  CFM.Grp_id=@Grp_id AND CFM.numDomainID = @numDomainID

	  END

	  IF @Grp_id = 21  --- Opp Item
	  BEGIN

	     SELECT	   CFM.Fld_id
				  ,CFM.Fld_label
				  ,CFM.Fld_type
				  ,'21' AS  Grp_id
				  ,OPPITEMEDI.FldDTLID
				  ,CASE WHEN(ISNULL(OPPITEMEDI.Fld_Value,'') = '') THEN  ITEMEDI.Fld_Value 
							--(SELECT ITEMEDI.Fld_Value FROM CFW_FLD_Values_Item_EDI ITEMEDI 
							--	LEFT JOIN CFW_Fld_Master CFM ON CFM.Fld_ID = ITEMEDI.Fld_id	 
							--	WHERE CFM.Grp_id=20            -- If Fld Value is not set in 'order's item' then show it from Item
							--)
							ELSE OPPITEMEDI.Fld_Value
					END AS Fld_Value
		  
		FROM CFW_Fld_Master CFM
		LEFT JOIN  CFW_Fld_Values_OppItems_EDI OPPITEMEDI
			ON CFM.Fld_ID = OPPITEMEDI.Fld_id AND OPPITEMEDI.RecId = @RecID		
		LEFT JOIN CFW_FLD_Values_Item_EDI ITEMEDI 
			ON CFM.Fld_ID = ITEMEDI.Fld_id	
		WHERE  CFM.Grp_id=20 AND CFM.numDomainID = @numDomainID

	  END


  END
GO


