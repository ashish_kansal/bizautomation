GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_SelectCommunicationAttendees_Detail')
DROP PROCEDURE USP_SelectCommunicationAttendees_Detail
GO
CREATE PROCEDURE [dbo].[USP_SelectCommunicationAttendees_Detail]
@numCommId AS numeric(9)=0,
@numUserCntId AS NUMERIC(9)=0
AS
BEGIN
 SELECT ActivityID,numAttendeeId,ISNULL(numStatus,0) AS numStatus FROM CommunicationAttendees CA 
 WHERE CA.numCommId=@numCommId AND CA.numContactId=@numUserCntId
END