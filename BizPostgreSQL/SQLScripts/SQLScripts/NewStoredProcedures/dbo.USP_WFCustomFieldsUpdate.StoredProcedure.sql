GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_WFCustomFieldsUpdate' ) 
    DROP PROCEDURE USP_WFCustomFieldsUpdate
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Sachin Sadhu>
-- Create date: <Create Date,,10Oct2014>
-- Description:	<Description,,Work Flow Action :Update fields >
-- =============================================
Create PROCEDURE [dbo].[USP_WFCustomFieldsUpdate]
	-- Add the parameters for the stored procedure here
	@numRecordID numeric(18, 0),
	@numFormID numeric(18, 0),
	@Fld_ID  numeric(18,0),
	@Fld_Value varchar(1000),
	@boolInsert BIT OUTPUT

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	IF (@numFormID =68)--Organization
			BEGIN
					IF Not Exists (Select 'col1' from CFW_FLD_Values where RecId=@numRecordID and Fld_ID=@Fld_ID)
						BEGIN
									   INSERT INTO CFW_FLD_Values
												(Fld_ID, Fld_Value, RecId)
										VALUES  (@Fld_ID,@Fld_Value,@numRecordID)
				
								SET @boolInsert = 0 
						END
					ELSE
						BEGIN
								SET @boolInsert = 1 
						END
			END
	ELSE IF(@numFormID =69)--contacts
			BEGIN
						IF Not Exists (Select 'col1' from CFW_FLD_Values_Cont where RecId=@numRecordID and Fld_ID=@Fld_ID)
								BEGIN
											   INSERT INTO CFW_FLD_Values_Cont
														(Fld_ID, Fld_Value, RecId)
												VALUES  (@Fld_ID,@Fld_Value,@numRecordID)
				
										SET @boolInsert = 0 
								END
							ELSE
								BEGIN
										SET @boolInsert = 1 
								END
			END
	ELSE IF(@numFormID =70)--Ordres
			BEGIN
						IF Not Exists (Select 'col1' from CFW_Fld_Values_Opp where RecId=@numRecordID and Fld_ID=@Fld_ID)
								BEGIN
											   INSERT INTO CFW_Fld_Values_Opp
														(Fld_ID, Fld_Value, RecId)
												VALUES  (@Fld_ID,@Fld_Value,@numRecordID)
				
										SET @boolInsert = 0 
								END
							ELSE
								BEGIN
										SET @boolInsert = 1 
								END
			END
   ELSE IF(@numFormID =72)--Cases
			BEGIN
						IF Not Exists (Select 'col1' from CFW_FLD_Values_Case where RecId=@numRecordID and Fld_ID=@Fld_ID)
								BEGIN
											   INSERT INTO CFW_FLD_Values_Case
														(Fld_ID, Fld_Value, RecId)
												VALUES  (@Fld_ID,@Fld_Value,@numRecordID)
				
										SET @boolInsert = 0 
								END
							ELSE
								BEGIN
										SET @boolInsert = 1 
								END
			END
	ELSE IF(@numFormID =73)--Projects
			BEGIN
						IF Not Exists (Select 'col1' from CFW_FLD_Values_Pro where RecId=@numRecordID and Fld_ID=@Fld_ID)
								BEGIN
											   INSERT INTO CFW_FLD_Values_Pro
														(Fld_ID, Fld_Value, RecId)
												VALUES  (@Fld_ID,@Fld_Value,@numRecordID)
				
										SET @boolInsert = 0 
								END
							ELSE
								BEGIN
										SET @boolInsert = 1 
								END
			END
	ELSE IF(@numFormID =94)--Sales/Stage/Business Process
			BEGIN
						IF Not Exists (Select 'col1' from CFW_Fld_Values_Opp where RecId=@numRecordID and Fld_ID=@Fld_ID)
								BEGIN
											   INSERT INTO CFW_Fld_Values_Opp
														(Fld_ID, Fld_Value, RecId)
												VALUES  (@Fld_ID,@Fld_Value,@numRecordID)
				
										SET @boolInsert = 0 
								END
							ELSE
								BEGIN
										SET @boolInsert = 1 
								END
			END
  ELSE IF(@numFormID =124)--Action Items
			BEGIN
						IF Not Exists (Select 'col1' from CFW_Fld_Values_Opp where RecId=@numRecordID and Fld_ID=@Fld_ID)
								BEGIN
											   INSERT INTO CFW_Fld_Values_Opp
														(Fld_ID, Fld_Value, RecId)
												VALUES  (@Fld_ID,@Fld_Value,@numRecordID)
				
										SET @boolInsert = 0 
								END
							ELSE
								BEGIN
										SET @boolInsert = 1 
								END
			END
	ELSE --worst case
			BEGIN
			         IF Not Exists (Select 'col1' from CFW_FLD_Values where RecId=@numRecordID and Fld_ID=@Fld_ID)
						BEGIN
									   INSERT INTO CFW_FLD_Values
												(Fld_ID, Fld_Value, RecId)
										VALUES  (@Fld_ID,@Fld_Value,@numRecordID)
				
								SET @boolInsert = 0 
						END
					ELSE
						BEGIN
								SET @boolInsert = 1 
						END
			END
	
END