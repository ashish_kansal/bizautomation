GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_InsertMultiCompany')
DROP PROCEDURE USP_InsertMultiCompany
GO
CREATE PROCEDURE [dbo].[USP_InsertMultiCompany]
(@numNewDomainID int,
@numParentDomainID int,
@numParentType int,
@numSubscriberID int)

as
begin

declare @vcDomainCode varchar(50);
declare @vcStatus varchar(1);
declare @vcParentDomCode varchar(50);

--select cast( substring('01',2,1)  as numeric)
-- select * from DOmain

if @numParentType=1 
	begin	
		select @vcDomainCode = '0' +   cast( isnull( max(cast(substring(vcDomainCode,1,len(vcDomainCode)) as numeric))+1,1) as varchar) from Domain where numSubscriberID=@numSubscriberID and numParentDomainID =0;
		update Domain set numSubscriberID=@numSubscriberID,numParentDomainID=0, vcDomainCode=@vcDomainCode where numDomainID=@numNewDomainID;
		set @vcStatus='U'
	end
ELSE if @numParentType=2
	begin
		select @vcParentDomCode = vcDomainCode from DOmain where numDomainID=@numParentDomainID;

		select @vcDomainCode = @vcParentDomCode + '0'+  cast( isnull( max(cast(substring(vcDomainCode,len(@vcParentDomCode)+1,len(vcDomainCode)) as numeric))+1,1) as varchar) from Domain where numParentDomainID=@numParentDomainID and numSubscriberId=@numSubscriberID;
		update Domain set numSubscriberID=@numSubscriberID,numParentDomainID=@numParentDomainID, vcDomainCode=@vcDomainCode where numDomainID=@numNewDomainID  ;
		set @vcStatus='U'
	END
ELSE IF @numParentType=3
	begin
		select @vcParentDomCode = vcDomainCode from DOmain where numDomainID=@numNewDomainID;
		update Domain set numSubscriberID=Null,numParentDomainID=Null, vcDomainCode=Null where numSubscriberID=@numSubscriberID and vcDomainCode like @vcParentDomCode + '%';
		set @vcStatus='U';
	END
select @vcStatus
end

