GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p' AND NAME ='USP_ElasticSearchBizCart_GetItemCustomField')
DROP PROCEDURE USP_ElasticSearchBizCart_GetItemCustomField
GO

/****** Object:  StoredProcedure [dbo].[USP_ElasticSearchBizCart_GetItemCustomField]    Script Date: 27/04/2020 14:12:51 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[USP_ElasticSearchBizCart_GetItemCustomField]
	@numDomainId NUMERIC(9)
AS
BEGIN
	SET NOCOUNT ON;

	SELECT 
		Fld_id as numFormFieldId
		,REPLACE(Fld_label,' ','') AS vcFormFieldName
		,CASE GRP_ID 
			WHEN 4 then 'C' 
			WHEN 1 THEN 'D'
			WHEN 9 THEN CONVERT(VARCHAR(15), Fld_id)
			ELSE 'C' 
		END AS vcFieldType
		,fld_type as vcAssociatedControlType
		,ISNULL(C.numListID, 0) numListID
		,CASE 
			WHEN C.numListID > 0 THEN 'L'
			ELSE ''
		END vcListItemType
		,ROW_NUMBER() OVER (ORDER BY Fld_id ASC) AS intRowNum
	FROM  
		CFW_Fld_Master C 
	LEFT JOIN 
		CFW_Validation V 
	ON 
		V.numFieldID = C.Fld_id
	WHERE 
		C.numDomainID = @numDomainId
		AND GRP_ID IN (5,9)

END
GO


