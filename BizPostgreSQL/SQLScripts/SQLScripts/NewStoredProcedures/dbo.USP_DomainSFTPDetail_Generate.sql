SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_DomainSFTPDetail_Generate')
DROP PROCEDURE dbo.USP_DomainSFTPDetail_Generate
GO
CREATE PROCEDURE [dbo].[USP_DomainSFTPDetail_Generate]
	@numDomainID NUMERIC(18,0)
AS 
BEGIN
	IF NOT EXISTS (SELECT ID FROM DomainSFTPDetail WHERE numDomainID=@numDomainID)
	BEGIN
		BEGIN TRY
		BEGIN TRANSACTION
			DECLARE @TP1Username VARCHAr(20)
			DECLARE @TP1Password VARCHAr(20)
			DECLARE @TP2Username VARCHAr(20)
			DECLARE @TP2Password VARCHAr(20)

			;WITH CTETP1Username(n) AS (SELECT 1 UNION ALL SELECT n+1 FROM CTETP1Username WHERE n<68)
			SELECT 
				@TP1Username = LEFT((SELECT 
										'' + SUBSTRING('0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz+-!@#$',n,1)
									FROM 
										CTETP1Username 
									ORDER BY 
										NEWID() FOR XML PATH('')),10)

			;WITH CTETP1Password(n) AS (SELECT 1 UNION ALL SELECT n+1 FROM CTETP1Password WHERE n<68)
			SELECT 
				@TP1Password = LEFT((SELECT 
										'' + SUBSTRING('0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz+-!@#$',n,1)
									FROM 
										CTETP1Password 
									ORDER BY 
										NEWID() FOR XML PATH('')),10)

			;WITH CTETP2Username(n) AS (SELECT 1 UNION ALL SELECT n+1 FROM CTETP2Username WHERE n<68)
			SELECT 
				@TP2Username = LEFT((SELECT 
										'' + SUBSTRING('0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz+-!@#$',n,1)
									FROM 
										CTETP2Username 
									ORDER BY 
										NEWID() FOR XML PATH('')),10)

			;WITH CTETP2Password(n) AS (SELECT 1 UNION ALL SELECT n+1 FROM CTETP2Password WHERE n<68)
			SELECT 
				@TP2Password = LEFT((SELECT 
										'' + SUBSTRING('0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz+-!@#$',n,1)
									FROM 
										CTETP2Password 
									ORDER BY 
										NEWID() FOR XML PATH('')),10)

			INSERT INTO DomainSFTPDetail (numDomainID,vcUsername,vcPassword,tintType) VALUES (@numDomainID,@TP1Username,@TP1Password,1)
			INSERT INTO DomainSFTPDetail (numDomainID,vcUsername,vcPassword,tintType) VALUES (@numDomainID,@TP2Username,@TP2Password,2)

		COMMIT
		END TRY
		BEGIN CATCH
			DECLARE @ErrorMessage NVARCHAR(4000)
			DECLARE @ErrorNumber INT
			DECLARE @ErrorSeverity INT
			DECLARE @ErrorState INT
			DECLARE @ErrorLine INT
			DECLARE @ErrorProcedure NVARCHAR(200)

			IF @@TRANCOUNT > 0
				ROLLBACK TRANSACTION;

			SELECT 
				@ErrorMessage = ERROR_MESSAGE(),
				@ErrorNumber = ERROR_NUMBER(),
				@ErrorSeverity = ERROR_SEVERITY(),
				@ErrorState = ERROR_STATE(),
				@ErrorLine = ERROR_LINE(),
				@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

			RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);

		END CATCH
	END
END
GO