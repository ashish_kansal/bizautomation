SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_DomainSFTPDetail_Validate')
DROP PROCEDURE dbo.USP_DomainSFTPDetail_Validate
GO
CREATE PROCEDURE [dbo].[USP_DomainSFTPDetail_Validate]
	@vcUsername VARCHAR(20)
	,@vcPassword VARCHAR(20)
AS 
BEGIN
	SELECT
		numDomainID
		,tintType
	FROM
		DomainSFTPDetail
	WHERE
		vcUsername=@vcUsername
		AND vcPassword=@vcPassword
END
GO