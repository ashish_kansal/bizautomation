GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_UpdateFollowUpStatus')
DROP PROCEDURE USP_UpdateFollowUpStatus
GO
CREATE PROCEDURE USP_UpdateFollowUpStatus
@numContactID NUMERIC,

@numFollowUpID NUMERIC
AS 
    BEGIN
    
        DECLARE @numCompanyId NUMERIC(9)          
        DECLARE @numDivisionId NUMERIC(9)  
        DECLARE @numDomainID NUMERIC(9)        
 
		SELECT  @numDivisionId = [numDivisionId] FROM [AdditionalContactsInformation] WHERE [numContactId] = @numContactId
		SELECT @numCompanyId = [numCompanyID],@numDomainID=[numDomainID] FROM [DivisionMaster] WHERE [numDivisionID] =@numDivisionId
		
        UPDATE  DivisionMaster
        SET     [numFollowUpStatus] = @numFollowUpID
        WHERE   numDivisionID = @numDivisionId
                AND numCompanyID = @numCompanyId
                
		INSERT INTO [FollowUpHistory] (
			[numFollowUpstatus],
			[numDivisionID],
			[bintAddedDate],
			[numDomainID]
		) VALUES ( 
			/* numFollowUpstatus - numeric(18, 0) */ @numFollowUpID,
			/* numDivisionID - numeric(18, 0) */ @numDivisionId,
			/* bintAddedDate - datetime */ GETUTCDATE(),
			/* numDomainID - numeric(18, 0) */@numDomainID  )
           
    END