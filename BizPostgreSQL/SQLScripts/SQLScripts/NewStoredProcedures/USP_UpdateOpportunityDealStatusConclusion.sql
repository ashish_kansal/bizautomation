SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_UpdateOpportunityDealStatusConclusion')
DROP PROCEDURE USP_UpdateOpportunityDealStatusConclusion
GO
CREATE PROCEDURE [dbo].[USP_UpdateOpportunityDealStatusConclusion] 
( 
 @numDomainID NUMERIC(9),
 @numOppId NUMERIC(9),
 @tintOppStatus AS INT = 0,
 @lngPConclAnalysis NUMERIC(18,0)=0,
 @numUserCntID NUMERIC(18,0)=0
)
AS 
BEGIN
	IF(@tintOppStatus>0)
	BEGIN
		DECLARE @bitViolatePrice BIT
		DECLARE @intPendingApprovePending INT
		DECLARE @intExecuteDiv INT=0
		DECLARE @numDivisionID NUMERIC=0
		DECLARE @tintCommitAllocation TINYINT

		SELECT @tintCommitAllocation=ISNULL(tintCommitAllocation,1) FROM Domain WHERE numDomainId=@numDomainId
		
		IF EXISTS (SELECT numApprovalRuleID FROM ApprovalProcessItemsClassification WHERE numDomainId=@numDomainID AND ISNULL(bitMarginPriceViolated,0) = 1)
		BEGIN
			SET @bitViolatePrice = 1
		END
		SET @intPendingApprovePending=(SELECT COUNT(*) FROM OpportunityItems WHERE numOppId=@numOppId AND ISNULL(bitItemPriceApprovalRequired,0)=1)

		IF(@intPendingApprovePending > 0 AND @bitViolatePrice=1 AND @tintOppStatus='1')
		BEGIN
			SET @intExecuteDiv=1
		END

		IF ISNULL(@intExecuteDiv,0)=0
		BEGIN
			DECLARE @tintOldOppStatus AS TINYINT
			SELECT @tintOldOppStatus=tintOppStatus FROM OpportunityMaster WHERE numOppID=@numOppID              

			IF @tintOldOppStatus=0 AND @tintOppStatus='1' --Open to Won
			BEGIN
				SELECT @numDivisionID=numDivisionID FROM OpportunityMaster WHERE numOppID=@numOppID   
					
				DECLARE @tintCRMType AS TINYINT      
				SELECT @tintCRMType=tintCRMType FROM divisionmaster WHERE numDivisionID =@numDivisionID 

				-- Promote Lead to Account when Sales/Purchase Order is created against it
				IF @tintCRMType=0 --Lead & Order
				BEGIN        
					UPDATE 
						divisionmaster 
					SET 
						tintCRMType=2
						,bintLeadProm=GETUTCDATE()
						,bintLeadPromBy=@numUserCntID
						,bintProsProm=GETUTCDATE()
						,bintProsPromBy=@numUserCntID        
					WHERE 
						numDivisionID=@numDivisionID        
				END
				--Promote Prospect to Account
				ELSE IF @tintCRMType=1 
				BEGIN        
					UPDATE 
						divisionmaster 
					SET 
						tintCRMType=2
						,bintProsProm=GETUTCDATE()
						,bintProsPromBy=@numUserCntID        
					WHERE 
						numDivisionID=@numDivisionID        
				END    

				IF @tintCommitAllocation=1
				BEGIN
		 			EXEC USP_UpdatingInventoryonCloseDeal @numOppID,0,@numUserCntID
				END
				UPDATE OpportunityMaster SET bintOppToOrder=GETUTCDATE() WHERE numOppId=@numOppId and numDomainID=@numDomainID
				UPDATE OpportunityItems SET bitRequiredWarehouseCorrection=NULL WHERE numOppId=@numOppId
				EXEC USP_OpportunityItems_CheckWarehouseMapping @numDomainID,@numOppId
			END
			ELSE IF (@tintOppStatus='2') -- Win to Lost
			BEGIN
				UPDATE OpportunityMaster SET dtDealLost=GETUTCDATE() WHERE numOppId=@numOppId and numDomainID=@numDomainID
			END
				
			IF ((@tintOldOppStatus=1 and @tintOppStatus='2') or (@tintOldOppStatus=1 and @tintOppStatus='0')) AND @tintCommitAllocation=1 --Won to Lost or Won to Open
					EXEC USP_RevertDetailsOpp @numOppID,0,0,@numUserCntID 

			UPDATE OpportunityMaster SET tintOppStatus = @tintOppStatus WHERE numOppId=@numOppID
				
		END
		--UPDATE 
		--	OpportunityMaster
		--SET
		--	tintOppStatus=@tintOppStatus
		--WHERE
		--	numDomainId=@numDomainID AND
		--	numOppId=@numOppId
	END
	IF(@lngPConclAnalysis>0)
	BEGIN
		UPDATE 
			OpportunityMaster
		SET
			lngPConclAnalysis=@lngPConclAnalysis
		WHERE
			numDomainId=@numDomainID AND
			numOppId=@numOppId
	END
END

