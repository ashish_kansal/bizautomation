
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- Created By Priya 
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getstateforchangezip')
DROP PROCEDURE usp_getstateforchangezip
GO
CREATE PROCEDURE [dbo].[USP_GetStateForChangeZip]       
@numDomainID as numeric(9)=0,    
@vcState AS VARCHAR(300)=''      
as     

BEGIN   
	SELECT 
		numStateID
		,vcState
		
	FROM 
		State  	
	WHERE 
		State.numDomainID = @numDomainID 
		AND vcState = @vcState
order by  vcState
END
GO
