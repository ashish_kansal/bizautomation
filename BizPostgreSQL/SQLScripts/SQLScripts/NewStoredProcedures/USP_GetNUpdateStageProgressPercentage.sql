
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getnupdatestageprogresspercentage')
DROP PROCEDURE usp_getnupdatestageprogresspercentage
GO
CREATE PROCEDURE [dbo].[USP_GetNUpdateStageProgressPercentage]

	@numDomainid NUMERIC = 0,
	@numStageId NUMERIC = 0,   
    @numRecID NUMERIC = 0 ,
    @tintMode TINYINT=0
             
AS 


DECLARE @numProjectID NUMERIC
DECLARE @numOppID NUMERIC


IF @tintMode = 0 
BEGIN              
       SELECT  numStageDetailsId FROM [StagePercentageDetails] sp
        WHERE  
                sp.numdomainid = @numDomainid
                AND numStageDetailsId < @numStageId
                AND ISNULL(sp.numOppID,0)= @numRecID
				AND tinProgressPercentage < 100
        ORDER BY numStageDetailsId ASC        
END

ELSE IF @tintMode = 1
BEGIN              
        SELECT  numStageDetailsId FROM [StagePercentageDetails] sp
        WHERE  
                sp.numdomainid = @numDomainid
                AND numStageDetailsId < @numStageId
                AND ISNULL(sp.numProjectID,0)= @numRecID
				AND tinProgressPercentage < 100
        ORDER BY numStageDetailsId ASC        
END


