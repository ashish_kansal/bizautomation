GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_LandedCostBillDetails' ) 
    DROP PROCEDURE USP_LandedCostBillDetails
GO
-- exec USP_GetBillPaymentDetails 3,1,0
CREATE PROCEDURE [dbo].[USP_LandedCostBillDetails]
    @numDomainID NUMERIC,
    @numOppID NUMERIC
AS 
    SET NOCOUNT ON
    SET TRANSACTION ISOLATION LEVEL READ COMMITTED
    
    SELECT  BH.[numBillID],
            BH.[numDivisionID],
            BH.[dtBillDate],
            BH.[numTermsID],
            BH.[dtDueDate],
            BH.[monAmountDue],
            BH.[monAmtPaid],
            BH.[bitIsPaid],
            BH.[vcMemo],
            BH.[vcReference],
			BD.[numBillDetailID],
            BD.[numExpenseAccountID],
            ISNULL(GJD.numTransactionId,0) AS numTransactionId_Bill,
            ISNULL(GJDD.numTransactionId,0) AS numTransactionId_BillDetail,
			BH.dtBillDate,(SELECT CAST([BillingTerms].[numTermsID] AS VARCHAR(20)) + '~' + CAST([BillingTerms].[numNetDueInDays] AS VARCHAR(20)) FROM BillingTerms WHERE numTermsID = BH.[numTermsID] AND numDomainId = BH.numDomainID)  AS [vcTermName],BH.dtDueDate,
			OM.[vcLanedCost]
    FROM    [dbo].[BillHeader] BH
			JOIN [dbo].[BillDetails] AS BD ON [BH].[numBillID] = [BD].[numBillID]
			JOIN [dbo].[OpportunityMaster] AS OM ON BH.[numOppId] = OM.[numOppId]
            LEFT OUTER JOIN General_Journal_Details GJD ON GJD.tintReferenceType=3 AND GJD.numReferenceID=BH.numBillID
           	LEFT OUTER JOIN General_Journal_Details GJDD ON GJDD.tintReferenceType=4 AND GJDD.numReferenceID=BD.numBillDetailID 
    WHERE  BH.numDomainID = @numDomainID AND BH.[numOppId] = @numOppID AND ISNULL(BH.[bitLandedCost],0) = 1
            
GO


