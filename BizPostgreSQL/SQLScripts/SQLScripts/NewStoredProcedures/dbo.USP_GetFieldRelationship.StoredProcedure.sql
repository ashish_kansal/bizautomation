

--Created by Anoop Jayaraj
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetFieldRelationship')
DROP PROCEDURE USP_GetFieldRelationship
GO
CREATE PROCEDURE USP_GetFieldRelationship
@byteMode TINYINT,
@numFieldRelID NUMERIC(18,0),
@numDomainID NUMERIC(18,0),
@numModuleID NUMERIC(18,0),
@numPrimaryListID NUMERIC(18,0),
@numPrimaryListItemID NUMERIC(18,0),
@numSecondaryListID NUMERIC(18,0),
@bitTaskRelation BIT
as
BEGIN
	IF @byteMode=1
	BEGIN
		IF(@bitTaskRelation=1)
		BEGIN
			SELECT 
				FR.numFieldRelID,
				FR.numPrimaryListID,
				SD.vcStageName as RelationshipName
			FROM
				FieldRelationship AS FR
			LEFT JOIN
				StagePercentageDetails AS SD
			ON
				FR.numPrimaryListID=SD.numStageDetailsId
			WHERE
				FR.numDomainId=@numDomainID
				AND ISNULL(FR.bitTaskRelation,0)=1
				AND 1 = (CASE WHEN ISNULL(@numModuleID,0) = 0 THEN 1 ELSE (CASE WHEN ISNULL(numModuleID,0) = @numModuleID THEN 1 ELSE 0 END) END)
				AND 1 = (CASE WHEN ISNULL(@numPrimaryListID,0) = 0 THEN 1 ELSE (CASE WHEN ISNULL(FR.numPrimaryListID,0) = @numPrimaryListID THEN 1 ELSE 0 END) END)
				AND 1 = (CASE WHEN ISNULL(@numSecondaryListID,0) = 0 THEN 1 ELSE (CASE WHEN ISNULL(FR.numSecondaryListID,0) = @numSecondaryListID THEN 1 ELSE 0 END) END)
		END
		ELSE
		BEGIN
			SELECT 
				numFieldRelID,L1.vcListName +' - ' + L2.vcListName as RelationshipName 
			FROM 
				FieldRelationship 
			JOIN 
				ListMaster L1
			ON 
				L1.numListID=numPrimaryListID
			JOIN 
				ListMaster L2
			ON
				L2.numListID=numSecondaryListID
			WHERE 
				FieldRelationship.numDomainID=@numDomainID 
				AND ISNULL(FieldRelationship.numModuleID,0) <> 14
				AND ISNULL(bitTaskRelation,0)=0
				AND 1 = (CASE WHEN ISNULL(@numModuleID,0) = 0 THEN 1 ELSE (CASE WHEN ISNULL(FieldRelationship.numModuleID,0) = @numModuleID THEN 1 ELSE 0 END) END)
				AND 1 = (CASE WHEN ISNULL(@numPrimaryListID,0) = 0 THEN 1 ELSE (CASE WHEN ISNULL(FieldRelationship.numPrimaryListID,0) = @numPrimaryListID THEN 1 ELSE 0 END) END)
				AND 1 = (CASE WHEN ISNULL(@numSecondaryListID,0) = 0 THEN 1 ELSE (CASE WHEN ISNULL(FieldRelationship.numSecondaryListID,0) = @numSecondaryListID THEN 1 ELSE 0 END) END)
			UNION
			SELECT 
				numFieldRelID,L1.vcItemName +' - ' + L2.vcItemName as RelationshipName 
			FROM 
				FieldRelationship 
			JOIN 
				Item L1
			ON 
				L1.numItemCode=numPrimaryListID
			JOIN 
				Item L2
			ON 
				L2.numItemCode=numSecondaryListID
			WHERE 
				FieldRelationship.numDomainID=@numDomainID
				AND ISNULL(FieldRelationship.numModuleID,0) = 14
				AND ISNULL(bitTaskRelation,0) = 0
				AND 1 = (CASE WHEN ISNULL(@numModuleID,0) = 0 THEN 1 ELSE (CASE WHEN ISNULL(numModuleID,0) = @numModuleID THEN 1 ELSE 0 END) END)
				AND 1 = (CASE WHEN ISNULL(@numPrimaryListID,0) = 0 THEN 1 ELSE (CASE WHEN ISNULL(FieldRelationship.numPrimaryListID,0) = @numPrimaryListID THEN 1 ELSE 0 END) END)
				AND 1 = (CASE WHEN ISNULL(@numSecondaryListID,0) = 0 THEN 1 ELSE (CASE WHEN ISNULL(FieldRelationship.numSecondaryListID,0) = @numSecondaryListID THEN 1 ELSE 0 END) END)
		END
	END
	ELSE IF @byteMode=2
	BEGIN
		SELECT 
			numFieldRelID
			,ISNULL(FieldRelationship.numModuleID,0) numModuleID
			,numPrimaryListID
			,numSecondaryListID
			,L1.vcListName +' - ' + L2.vcListName AS RelationshipName
			,L1.vcListName AS Item1
			,L2.vcListName AS Item2 
		FROM 
			FieldRelationship 
		JOIN 
			ListMaster L1
		ON 
			L1.numListID=numPrimaryListID
		JOIN 
			ListMaster L2
		ON 
			L2.numListID=numSecondaryListID
		WHERE 
			numFieldRelID=@numFieldRelID 
			AND ISNULL(FieldRelationship.numModuleID,0) <> 14 
			AND ISNULL(bitTaskRelation,0)=0
		UNION
		SELECT 
			numFieldRelID
			,ISNULL(numModuleID,0) numModuleID
			,numPrimaryListID
			,numSecondaryListID
			,L1.vcItemName +' - ' + L2.vcItemName AS RelationshipName
			,L1.vcItemName as Item1
			,L2.vcItemName as Item2 
		FROM 
			FieldRelationship 
		JOIN 
			Item L1
		ON 
			L1.numItemCode=numPrimaryListID
		JOIN 
			Item L2
		ON 
			L2.numItemCode=numSecondaryListID
		WHERE 
			numFieldRelID=@numFieldRelID 
			AND ISNULL(numModuleID,0) = 14
			AND ISNULL(bitTaskRelation,0)=0

		IF @bitTaskRelation=1
		BEGIN
			SELECT 
				FieldRelationship.numFieldRelID
				,numFieldRelDTLID,SLI1.vcTaskName as PrimaryListItem
				,SLI2.vcTaskName as SecondaryListItem 
			FROM 
				FieldRelationship
			JOIN 
				FieldRelationshipDTL 
			ON 
				FieldRelationshipDTL.numFieldRelID=FieldRelationship.numFieldRelID
			JOIN 
				StagePercentageDetailsTask SLI1
			ON 
				SLI1.numTaskId=numPrimaryListItemID
			JOIN 
				StagePercentageDetailsTask SLI2
			ON 
				SLI2.numTaskId=numSecondaryListItemID
			WHERE 
				FieldRelationship.numFieldRelID=@numFieldRelID 
				AND numPrimaryListItemID=@numPrimaryListItemID 
				AND ISNULL(bitTaskRelation,0)=1
		END
		ELSE
		BEGIN
			SELECT 
				FieldRelationship.numFieldRelID
				,numFieldRelDTLID
				,L1.vcData AS PrimaryListItem
				,L2.vcData AS SecondaryListItem 
			FROM 
				FieldRelationship
			JOIN 
				FieldRelationshipDTL 
			ON 
				FieldRelationshipDTL.numFieldRelID=FieldRelationship.numFieldRelID
			JOIN 
				ListDetails L1
			ON 
				L1.numListItemID=numPrimaryListItemID
			JOIN 
				ListDetails L2
			ON 
				L2.numListItemID=numSecondaryListItemID
			WHERE 
				FieldRelationship.numFieldRelID=@numFieldRelID 
				AND (ISNULL(@numPrimaryListItemID,0) = 0 OR numPrimaryListItemID=@numPrimaryListItemID)
				AND ISNULL(numModuleID,0) <> 14
				AND ISNULL(bitTaskRelation,0)=0
			UNION
			SELECT 
				FieldRelationship.numFieldRelID
				,numFieldRelDTLID
				,L1.vcItemName AS PrimaryListItem
				,L2.vcItemName AS SecondaryListItem 
			FROM 
				FieldRelationship
			JOIN 
				FieldRelationshipDTL 
			ON 
				FieldRelationshipDTL.numFieldRelID=FieldRelationship.numFieldRelID
			JOIN 
				Item L1
			ON 
				L1.numItemCode=numPrimaryListItemID
			JOIN 
				Item L2
			ON 
				L2.numItemCode=numSecondaryListItemID
			WHERE 
				FieldRelationship.numFieldRelID=@numFieldRelID 
				AND (ISNULL(@numPrimaryListItemID,0) = 0 OR numPrimaryListItemID=@numPrimaryListItemID)
				AND ISNULL(numModuleID,0) = 14
				AND ISNULL(bitTaskRelation,0)=0
				AND ISNULL(numSecondaryListItemID,0) <> -1
			UNION
			SELECT 
				FieldRelationship.numFieldRelID
				,numFieldRelDTLID
				,L1.vcItemName AS PrimaryListItem
				,'Do not display' AS SecondaryListItem 
			FROM 
				FieldRelationship
			JOIN 
				FieldRelationshipDTL 
			ON 
				FieldRelationshipDTL.numFieldRelID=FieldRelationship.numFieldRelID
			JOIN 
				Item L1
			ON 
				L1.numItemCode=numPrimaryListItemID
			WHERE 
				FieldRelationship.numFieldRelID=@numFieldRelID 
				AND (ISNULL(@numPrimaryListItemID,0) = 0 OR numPrimaryListItemID=@numPrimaryListItemID)
				AND ISNULL(numModuleID,0) = 14
				AND ISNULL(bitTaskRelation,0)=0
				AND ISNULL(numSecondaryListItemID,0) = -1
		end

	end
	else if @byteMode=3
	begin


		Select L2.numListItemID,L2.vcData as SecondaryListItem   from FieldRelationship
		Join FieldRelationshipDTL 
		on FieldRelationshipDTL.numFieldRelID=FieldRelationship.numFieldRelID
		Join ListDetails L2
		on L2.numListItemID=numSecondaryListItemID
		where FieldRelationship.numDomainID=@numDomainID and numPrimaryListItemID=@numPrimaryListItemID
		and FieldRelationship.numSecondaryListID=@numSecondaryListID AND ISNULL(bitTaskRelation,0)=0
	end
END
GO

