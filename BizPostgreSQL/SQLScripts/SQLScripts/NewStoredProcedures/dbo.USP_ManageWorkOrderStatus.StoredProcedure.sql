
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO

-- exec USP_ManageWorkOrderStatus @numWOId=10086,@numWOStatus=23184,@numUserCntID=1,@numDomainID=1
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ManageWorkOrderStatus')
DROP PROCEDURE USP_ManageWorkOrderStatus
GO
CREATE PROCEDURE [dbo].[USP_ManageWorkOrderStatus]
@numWOId as numeric(9)=0,
@numWOStatus as numeric(9),
@numUserCntID AS NUMERIC(9)=0,
@numDomainID AS NUMERIC(18,0)
AS
BEGIN
BEGIN TRY
BEGIN TRANSACTION
	DECLARE @numDomain AS NUMERIC(18,0)
	DECLARE @CurrentWorkOrderStatus AS NUMERIC(18,0)
	SELECT @numDomain = @numDomainID

	--GET CURRENT STATUS OF WORK ORDER
	SELECT @CurrentWorkOrderStatus=numWOStatus FROM WorkOrder WHERE numWOId=@numWOId AND [numDomainID] = @numDomainID

	IF ISNULL(@CurrentWorkOrderStatus,0) = 23184 AND @numWOStatus=23184
	BEGIN
		RAISERROR('WORKORDER_IS_ALREADY_COMPLETED',16,1)
		RETURN
	END

	--ASSIGN NEW STATUS TO WORK ORDER
	UPDATE WorkOrder SET numWOStatus=@numWOStatus WHERE numWOId=@numWOId AND [numDomainID] = @numDomainID AND numWOStatus <> 23184 

	--IF CURRENT WORK ORDER STATUS IS NOT COMPLETED AND NEW WORK ORDER STATUS IS COMPLETED THEN MAKE INVENTORY CHANGES
	IF ISNULL(@CurrentWorkOrderStatus,0) <> 23184 AND @numWOStatus=23184
	BEGIN
		DECLARE @PendingChildWO NUMERIC(18,0)

		;WITH CTE (numWOID) AS
		(
			SELECT
				numWOID 
			FROM 
				WorkOrder
			WHERE 
				numParentWOID = @numWOId 
				AND numWOStatus <> 23184
			UNION ALL
			SELECT 
				WorkOrder.numWOID 
			FROM 
				WorkOrder
			INNER JOIN
				CTE c
			ON
				 WorkOrder.numParentWOID = c.numWOID
			WHERE 
				WorkOrder.numWOStatus <> 23184
		)

		SELECT @PendingChildWO = COUNT(*) FROM CTE

		--WE HAVE IMPLEMENTED CODE IN SUCH WAY THAT FIRST CHILD WORK ORDER GET COMPLETED
		--SO IF SOME REASON CHILD WORK ORDER IS NOT COMPLETED THAN RAISERROR
		IF ISNULL(@PendingChildWO,0) > 0
		BEGIN
			RAISERROR('CHILD WORK ORDER STILL NOT COMPLETED',16,1);
		END

		IF EXISTS (SELECT 
						WorkOrderDetails.numWODetailId
					FROM
						WorkOrderDetails
					OUTER APPLY
					(
						SELECT
							SUM(numPickedQty) numPickedQty
						FROM
							WorkOrderPickedItems
						WHERE
							WorkOrderPickedItems.numWODetailId = WorkOrderDetails.numWODetailId
					) TEMP
					WHERE
						WorkOrderDetails.numWOId=@numWOId
						AND ISNULL(numWareHouseItemId,0) > 0
						AND ISNULL(WorkOrderDetails.numQtyItemsReq,0) <> ISNULL(TEMP.numPickedQty,0))
		BEGIN
			RAISERROR('BOM_LEFT_TO_BE_PICKED',16,1);
			RETURN
		END

		DECLARE @numWareHouseItemID AS NUMERIC(9),@numQtyItemsReq AS FLOAT,@numQtyBuilt FLOAT,@numOppId AS NUMERIC(9), @numOppItemID NUMERIC(18,0)
		SELECT 
			@numWareHouseItemID=numWareHouseItemID
			,@numQtyItemsReq=numQtyItemsReq
			,@numQtyBuilt=ISNULL(numQtyBuilt,0)
			,@numOppId=ISNULL(numOppId,0) 
		FROM 
			WorkOrder 
		WHERE 
			numWOId=@numWOId 
			AND [numDomainID] = @numDomainID

		UPDATE WorkOrder SET bintCompletedDate=GETDATE(),numQtyBuilt=numQtyItemsReq WHERE numWOId=@numWOId AND [numDomainID] = @numDomainID

		-- COMMENTED BECAUSE WORK ORDER BUILD COMPLETION IS NOW MANAGED THROUGH PURCHASE FULFILLMENT
		--IF @numQtyItemsReq > @numQtyBuilt
		--BEGIN
		--	SET @numQtyItemsReq = @numQtyItemsReq - @numQtyBuilt
		--	EXEC USP_UpdatingInventory 0,@numWareHouseItemID,@numQtyItemsReq,@numWOId,@numUserCntID,@numOppId
		--END
	END
COMMIT
END TRY
BEGIN CATCH
	DECLARE @ErrorMessage NVARCHAR(4000)
	DECLARE @ErrorNumber INT
	DECLARE @ErrorSeverity INT
	DECLARE @ErrorState INT
	DECLARE @ErrorLine INT
	DECLARE @ErrorProcedure NVARCHAR(200)

	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;

	SELECT 
		@ErrorMessage = ERROR_MESSAGE(),
		@ErrorNumber = ERROR_NUMBER(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE(),
		@ErrorLine = ERROR_LINE(),
		@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

	RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);
END CATCH
END