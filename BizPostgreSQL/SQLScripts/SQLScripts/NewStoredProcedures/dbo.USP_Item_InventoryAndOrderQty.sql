/****** Object:  StoredProcedure [dbo].[USP_PreDefinedReptLinks]    Script Date: 07/26/2008 16:20:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_Item_InventoryAndOrderQty')
DROP PROCEDURE USP_Item_InventoryAndOrderQty
GO
CREATE PROCEDURE [dbo].[USP_Item_InventoryAndOrderQty]
(
	@numDomainID NUMERIC(18,0)
	,@numItemCode NUMERIC(18,0)
	,@numCurrentPage INT
	,@numPageSize INT
)
AS
BEGIN
	SELECT
		*
	FROM
	(

	SELECT 
		numItemCode,
		vcItemName,
		ListDetails.vcData AS vcItemClassification,
		SUM(numOnHand) AS numOnHand
		,SUM(numOnOrder) AS numOnOrder
		,SUM(numAllocation) AS numAllocation
		,SUM(numBackOrder) AS numBackOrder
		,ISNULL(OpenPO.numQtyOpenPO,0) AS numQtyOpenPO
		,ISNULL(ClosedPO.numQtyClosedPO,0) AS numQtyClosedPO
		,ISNULL(OpenSO.numQtyOpenSO,0) AS numQtyOpenSO
		,ISNULL(ClosedSO.numQtyClosedSO,0) AS numQtyClosedSO
		,ISNULL(SOReturn.numQtySOReturn,0) AS numQtySOReturn
		,ISNULL(POReturn.numQtyPOReturn,0) AS numQtyPOReturn
	FROM
		Item
	LEFT JOIN
		ListDetails
	ON
		Item.numItemClassification = ListDetails.numListItemID
	LEFT JOIN
		WareHouseItems
	ON
		WareHouseItems.numItemID = Item.numItemCode
	OUTER APPLY
	(
		SELECT
			SUM(OI.numUnitHour) AS numQtyOpenPO
		FROM
			OpportunityMaster OM
		INNER JOIN 
			OpportunityItems OI
		ON
			OM.numOppId = OI.numOppId
		WHERE
			OM.numDomainId = @numDomainID
			AND tintOppType=2
			AND tintOppStatus = 1
			AND ISNULL(tintshipped,0) = 0
			AND OI.numItemCode = Item.numItemCode
			AND ISNULL(OM.bitStockTransfer,0) = 0
	) AS  OpenPO
	OUTER APPLY
	(
		SELECT
			SUM(OI.numUnitHour) AS numQtyClosedPO
		FROM
			OpportunityMaster OM
		INNER JOIN 
			OpportunityItems OI
		ON
			OM.numOppId = OI.numOppId
		WHERE
			OM.numDomainId = @numDomainID
			AND tintOppType=2
			AND tintOppStatus = 1
			AND ISNULL(tintshipped,0) = 1
			AND OI.numItemCode = Item.numItemCode
			AND ISNULL(OM.bitStockTransfer,0) = 0
	) AS  ClosedPO
	OUTER APPLY
	(
		SELECT
			SUM(OI.numUnitHour) AS numQtyOpenSO
		FROM
			OpportunityMaster OM
		INNER JOIN 
			OpportunityItems OI
		ON
			OM.numOppId = OI.numOppId
		WHERE
			OM.numDomainId = @numDomainID
			AND tintOppType=1
			AND tintOppStatus = 1
			AND ISNULL(tintshipped,0) = 0
			AND OI.numItemCode = Item.numItemCode
			AND ISNULL(OM.bitStockTransfer,0) = 0
	) AS  OpenSO
	OUTER APPLY
	(
		SELECT
			SUM(OI.numUnitHour) AS numQtyClosedSO
		FROM
			OpportunityMaster OM
		INNER JOIN 
			OpportunityItems OI
		ON
			OM.numOppId = OI.numOppId
		WHERE
			OM.numDomainId = @numDomainID
			AND tintOppType=1
			AND tintOppStatus = 1
			AND ISNULL(tintshipped,0) = 1
			AND OI.numItemCode = Item.numItemCode
			AND ISNULL(OM.bitStockTransfer,0) = 0
	) AS  ClosedSO
	OUTER APPLY
	(
		SELECT
			SUM(RI.numUnitHour) AS numQtySOReturn
		FROM
			ReturnItems RI
		INNER JOIN
			ReturnHeader RH
		ON
			RI.numReturnHeaderID = RH.numReturnHeaderID
		WHERE
			RH.numDomainId =@numDomainID
			AND RI.numItemCode=Item.numItemCode
			AND RH.tintReturnType = 1
			AND RH.numReturnStatus = 303
	) AS SOReturn
	OUTER APPLY
	(
		SELECT
			SUM(RI.numUnitHour) AS numQtyPOReturn
		FROM
			ReturnItems RI
		INNER JOIN
			ReturnHeader RH
		ON
			RI.numReturnHeaderID = RH.numReturnHeaderID
		WHERE
			RH.numDomainId =@numDomainID
			AND RI.numItemCode=Item.numItemCode
			AND RH.tintReturnType = 2
			AND RH.numReturnStatus = 303
	) AS POReturn
	WHERE
		Item.numDomainID = @numDomainID
		AND charItemType = 'P'
		AND (Item.numItemCode=@numItemCode OR ISNULL(@numItemCode,0) = 0)
	GROUP BY
		Item.numItemCode
		,Item.vcItemName
		,ListDetails.vcData
		,OpenPO.numQtyOpenPO
		,ClosedPO.numQtyClosedPO
		,OpenSO.numQtyOpenSO
		,ClosedSO.numQtyClosedSO
		,SOReturn.numQtySOReturn
		,POReturn.numQtyPOReturn
	) TEMP
	ORDER BY
		numItemCode
	OFFSET 
		@numPageSize * (@numCurrentPage - 1) ROWS
	FETCH NEXT 
		@numPageSize ROWS ONLY;


	SELECT COUNT(*) FROM Item WHERE Item.numDomainID = @numDomainID AND charItemType = 'P' AND (Item.numItemCode=@numItemCode OR ISNULL(@numItemCode,0) = 0)
END
GO
