GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ManageSubscribers_GorupPermission')
DROP PROCEDURE dbo.USP_ManageSubscribers_GorupPermission
GO
CREATE PROCEDURE [dbo].[USP_ManageSubscribers_GorupPermission]
(
	@numTargetDomainID NUMERIC(18,0)
	,@numGroupId2 NUMERIC(18,0)
	,@numGroupUserCntID NUMERIC(18,0)
)
AS 
BEGIN

	INSERT  INTO AuthenticationGroupMaster
    (
        vcGroupName,
        numDomainID,
        tintGroupType,
        bitConsFlag 
    )
    VALUES  
	(
        'Executive',
        @numTargetDomainID,
        1,
        0
    )
    SET @numGroupId2 = SCOPE_IDENTITY() 

	INSERT  INTO GroupAuthorization
	(
		numGroupID
		,numModuleID
		,numPageID
		,intExportAllowed
		,intPrintAllowed
		,intViewAllowed
		,intAddAllowed
		,intUpdateAllowed
		,intDeleteAllowed
		,numDomainID
	)
	SELECT 
		@numGroupId2
		,numModuleID
		,numPageID
		,bitISExportApplicable
		,0
		,bitIsViewApplicable
		,bitIsAddApplicable
		,bitISUpdateApplicable
		,bitISDeleteApplicable
		,@numTargetDomainID
	FROM 
		PageMaster         
        
                
	--MainTab From TabMaster 
    INSERT INTO GroupTabDetails
    (
        numGroupId,
        numTabId,
        numRelationShip,
        bitallowed,
        numOrder,tintType
    )
    SELECT 
		@numGroupId2,
        x.numTabId,
        0,
        1,
        x.numOrder
		,0
    FROM 
	(
		SELECT 
			*
			,ROW_NUMBER() OVER(ORDER BY numtabid) AS numOrder
        FROM 
			TabMaster
        WHERE 
			tintTabtype=1 
			AND bitFixed=1
    ) x 

	--default Sub tab From cfw_grp_master
	INSERT  INTO GroupTabDetails
    (
        numGroupId,
        numTabId,
        numRelationShip,
        bitallowed,
        numOrder,tintType
    )
    SELECT 
		@numGroupId2,
        x.Grp_id,
        0,
        1,
        x.numOrder
		,1
    FROM
	(
		SELECT 
			*
			,ROW_NUMBER() OVER(ORDER BY Grp_id) AS numOrder
        FROM 
			cfw_grp_master
        WHERE 
			tintType=2 
			AND numDomainID = @numTargetDomainID
    ) x 

    INSERT  INTO DashBoardSize
    (
        tintColumn,
        tintSize,
        numGroupUserCntID,
        bitGroup
    )
    SELECT  
		tintColumn,
        tintSize,
        @numGroupId2,
        1
    FROM 
		DashBoardSize
    WHERE 
		bitGroup = 1
        AND numGroupUserCntID = @numGroupUserCntID
    ORDER BY 
		tintColumn 

    INSERT INTO Dashboard
    (
        numReportID,
        numGroupUserCntID,
        tintRow,
        tintColumn,
        tintReportType,
        vcHeader,
        vcFooter,
        tintChartType,
        bitGroup
    )
    SELECT 
		numReportID,
        @numGroupId2,
        tintRow,
        tintColumn,
        tintReportType,
        vcHeader,
        vcFooter,
        tintChartType,
        1
    FROM 
		Dashboard
    WHERE 
		bitGroup = 1
        AND numGroupUserCntID = @numGroupUserCntID
    ORDER BY 
		tintColumn,
        tintRow

    INSERT INTO 
		DashboardAllowedReports
    SELECT 
		numCustomReportID,
        @numGroupId2
    FROM 
		CustomReport
    WHERE 
		numDomainID = @numTargetDomainID 

	UPDATE
		Table2
    SET
		Table2.numReportID = Table1.numOrgReportID
    FROM 
		Dashboard Table2
    INNER JOIN 
	(
		SELECT  
			numReportID
			,(SELECT TOP 1 C1.numCustomReportID FROM Customreport C1 JOIN Customreport C2 ON C1.vcReportName = C2.vcReportName WHERE C1.numDomainID = @numTargetDomainID AND C2.numCustomReportID = Dashboard.numReportID AND C2.numDomainID = 1) numOrgReportID
        FROM 
			Dashboard
        WHERE 
			bitGroup = 1
            AND numGroupUserCntID = @numGroupUserCntID
	) Table1 
	ON 
		Table2.numReportID = Table1.numReportID
    WHERE   
		bitGroup = 1
        AND numGroupUserCntID = @numGroupId2
END
GO