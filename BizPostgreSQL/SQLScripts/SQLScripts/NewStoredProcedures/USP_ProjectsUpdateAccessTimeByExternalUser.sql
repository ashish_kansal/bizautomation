SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj                  
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ProjectsUpdateAccessTimeByExternalUser')
DROP PROCEDURE USP_ProjectsUpdateAccessTimeByExternalUser
GO
CREATE PROCEDURE [dbo].[USP_ProjectsUpdateAccessTimeByExternalUser]                          
(                          
@numProId numeric(9)=null ,  
@numDomainID as numeric(9) ,   
@bitDisplayTimeToExternalUsers AS BIT                        
)                          
as                          
                          
begin
	UPDATE	ProjectsMaster SET bitDisplayTimeToExternalUsers=@bitDisplayTimeToExternalUsers WHERE numProId=@numProId AND numDomainId=@numDomainID
END