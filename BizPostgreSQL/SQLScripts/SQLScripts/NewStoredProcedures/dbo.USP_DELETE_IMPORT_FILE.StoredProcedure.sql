GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_DELETE_IMPORT_FILE')
DROP PROCEDURE USP_DELETE_IMPORT_FILE
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE USP_DELETE_IMPORT_FILE
(
  @numImportFileID			BIGINT,
  @numDomainID				NUMERIC,
  @numUserCntID				NUMERIC
)
AS 
BEGIN

	DELETE FROM dbo.Import_File_Field_Mapping WHERE intImportFileID = @numImportFileID
	DELETE FROM dbo.Import_History WHERE intImportFileID = @numImportFileID
	
	DELETE FROM dbo.Import_File_Master 
		  WHERE intImportFileID = @numImportFileID 
		    AND numDomainID = @numDomainID
		    --AND numUserContactID = @numUserCntID
		    
END