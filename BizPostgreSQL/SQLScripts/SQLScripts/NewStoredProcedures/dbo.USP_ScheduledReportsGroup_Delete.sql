GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ScheduledReportsGroup_Delete')
DROP PROCEDURE dbo.USP_ScheduledReportsGroup_Delete
GO
CREATE PROCEDURE [dbo].[USP_ScheduledReportsGroup_Delete]
(
	@numDomainID NUMERIC(18,0)
	,@vcRecords NVARCHAR(300)
)
AS 
BEGIN
	IF ISNULL(@vcRecords,'') <> ''
	BEGIN
		DELETE SRGR FROM ScheduledReportsGroup SRG INNER JOIN ScheduledReportsGroupReports SRGR ON SRG.ID = SRGR.numSRGID WHERE SRG.numDomainID=@numDomainID AND SRG.ID IN (SELECT Id FROM dbo.SplitIDs(@vcRecords,','))
		DELETE FROM ScheduledReportsGroup WHERE numDomainID=@numDomainID AND ID IN (SELECT Id FROM dbo.SplitIDs(@vcRecords,','))
	END
END
GO