GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
                                    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_TimeAndExpense_GetTimeSummary')
DROP PROCEDURE USP_TimeAndExpense_GetTimeSummary
GO
CREATE PROCEDURE [dbo].[USP_TimeAndExpense_GetTimeSummary]
(
	@numDomainID NUMERIC(18,0)
	,@dtFromDate DATE
	,@dtToDate DATE
	,@vcEmployeeName VARCHAR(200)
	,@vcTeams VARCHAR(MAX)
	,@tintPayrollType TINYINT
	,@ClientTimeZoneOffset INT
)
AS
BEGIN
	DECLARE @TEMP TABLE
	(
		numType TINYINT
		,numMinutes INT
	)

	INSERT INTO @TEMP
	(
		numType
		,numMinutes
	)
	SELECT
		TEMPTotalTime.numType
		,TEMPTotalTime.numMinutes
	FROM
		UserMaster
	INNER JOIN
		AdditionalContactsInformation
	ON
		UserMaster.numUserDetailId = AdditionalContactsInformation.numContactId
	OUTER APPLY
	(
		SELECT 
			*
		FROM
			dbo.fn_GetPayrollEmployeeTime(@numDomainID,UserMaster.numUserDetailId,@dtFromDate,@dtToDate,@ClientTimeZoneOffset)
	) TEMPTotalTime
	WHERE
		UserMaster.numDomainID=@numDomainID
		AND ISNULL(UserMaster.bitActivateFlag,0) = 1
		AND (ISNULL(@vcEmployeeName,'') = '' OR AdditionalContactsInformation.vcFirstName LIKE CONCAT('%',@vcEmployeeName,'%') OR AdditionalContactsInformation.vcLastName LIKE CONCAT('%',@vcEmployeeName,'%'))
		AND (ISNULL(@vcTeams,'') = '' OR AdditionalContactsInformation.numTeam IN (SELECT Id FROM dbo.SplitIDs(@vcTeams,',')))
		AND 1 = (CASE WHEN @tintPayrollType=0 THEN 1 ELSE (CASE WHEN ISNULL(UserMaster.tintPayrollType,0)=@tintPayrollType THEN 1 ELSE 0 END) END)


	DECLARE @numTotalTime NUMERIC(18,0)
	DECLARE @numTotalBillableTime NUMERIC(18,0)
	DECLARE @numTotalNonBillableTime NUMERIC(18,0)
	SET @numTotalTime = ISNULL((SELECT SUM(numMinutes) FROM @TEMP WHERE numType NOT IN (4)),0)
	SET @numTotalBillableTime = ISNULL((SELECT SUM(numMinutes) FROM @TEMP WHERE numType = 1),0)
	SET @numTotalNonBillableTime = ISNULL((SELECT SUM(numMinutes) FROM @TEMP WHERE  numType NOT IN (1,4)),0)

	SELECT 
		CONCAT(FORMAT(@numTotalTime/60,'00'),':',FORMAT(@numTotalTime%60,'00')) vcTotalTime
		,CONCAT(FORMAT(@numTotalBillableTime/60,'00'),':',FORMAT(@numTotalBillableTime%60,'00')) vcTotalBillableTime
		,CONCAT(FORMAT(@numTotalNonBillableTime/60,'00'),':',FORMAT(@numTotalNonBillableTime%60,'00')) vcTotalNonBillableTime
END
GO