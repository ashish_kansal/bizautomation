GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_DeleteHelpCategories')
DROP PROCEDURE USP_DeleteHelpCategories
GO
CREATE PROCEDURE	 [dbo].[USP_DeleteHelpCategories]
	@numHelpCategoryID numeric
AS
SET NOCOUNT ON

DELETE FROM HelpCategories
WHERE [numHelpCategoryID] = @numHelpCategoryID