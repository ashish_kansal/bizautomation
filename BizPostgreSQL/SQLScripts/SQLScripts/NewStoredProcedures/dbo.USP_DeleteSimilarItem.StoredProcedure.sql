GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_DeleteSimilarItem')
DROP PROCEDURE USP_DeleteSimilarItem
GO
CREATE PROCEDURE USP_DeleteSimilarItem
	@numDomainID NUMERIC(9),
	@numParentItemCode NUMERIC(9),
	@numItemCode NUMERIC(9)
AS 
BEGIN
	BEGIN TRY
		BEGIN TRANSACTION
			DELETE FROM SimilarItems WHERE numDomainId = @numDomainId AND numItemCode = @numItemCode AND numParentItemCode = @numParentItemCode
		COMMIT
	END TRY
	BEGIN CATCH
		DECLARE @ErrorMessage NVARCHAR(4000)
		DECLARE @ErrorNumber INT
		DECLARE @ErrorSeverity INT
		DECLARE @ErrorState INT
		DECLARE @ErrorLine INT
		DECLARE @ErrorProcedure NVARCHAR(200)

		IF @@TRANCOUNT > 0
			ROLLBACK TRANSACTION;

		SELECT 
			@ErrorMessage = ERROR_MESSAGE(),
			@ErrorNumber = ERROR_NUMBER(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE(),
			@ErrorLine = ERROR_LINE(),
			@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

		RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);
	END CATCH
END