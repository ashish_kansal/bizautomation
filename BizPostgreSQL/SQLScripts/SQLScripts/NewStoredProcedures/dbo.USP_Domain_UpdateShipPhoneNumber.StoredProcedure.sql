SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_Domain_UpdateShipPhoneNumber')
DROP PROCEDURE dbo.USP_Domain_UpdateShipPhoneNumber
GO
-- =============================================
-- Author:		<Author,,Sachin Sadhu>
-- Create date: <Create Date,,27thAugust2014>
-- Description:	<Description,, Update Shipping Phone Number in E-commerce API>
-- =============================================
Create PROCEDURE USP_Domain_UpdateShipPhoneNumber
	-- Add the parameters for the stored procedure here
	@numDomainID as numeric(9)=0,       
	@vcShipToPhoneNumber as VARCHAR(50)=''
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	Update Domain                                       
	 SET     
		vcShipToPhoneNo = @vcShipToPhoneNumber
		WHERE   numDomainId=@numDomainID
END
Go
