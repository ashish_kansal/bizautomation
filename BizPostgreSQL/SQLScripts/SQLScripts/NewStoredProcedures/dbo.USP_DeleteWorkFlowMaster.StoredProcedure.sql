GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_DeleteWorkFlowMaster')
DROP PROCEDURE USP_DeleteWorkFlowMaster
GO
CREATE PROCEDURE [dbo].[USP_DeleteWorkFlowMaster]     
    @numDomainID numeric(18, 0),
	@numWFID numeric(18, 0)
as                 

DELETE FROM WorkFlowTriggerFieldList WHERE numWFID=@numWFID 

DELETE FROM WorkFlowConditionList WHERE numWFID=@numWFID 

DELETE FROM WorkFlowActionUpdateFields WHERE numWFActionID IN(SELECT numWFActionID FROM WorkFlowActionList WHERE numWFID=@numWFID)

DELETE FROM WorkFlowActionList WHERE numWFID=@numWFID 


DELETE FROM WorkFlowMaster WHERE numDomainID=@numDomainID AND numWFID=@numWFID
