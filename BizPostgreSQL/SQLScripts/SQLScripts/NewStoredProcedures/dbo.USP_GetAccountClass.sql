
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetAccountClass')
DROP PROCEDURE dbo.USP_GetAccountClass
GO
CREATE PROCEDURE [dbo].[USP_GetAccountClass]
(
	@numDomainID NUMERIC(18,0),
    @numUserCntID NUMERIC(18,0),
    @numDivisionID NUMERIC(18,0)
)
AS 
BEGIN
	DECLARE @numAccountClass AS NUMERIC(18,0) = 0
	DECLARE @tintDefaultClassType AS INT = 0
	SELECT @tintDefaultClassType = ISNULL(tintDefaultClassType,0) FROM Domain WHERE numDomainId = @numDomainID 

	IF @tintDefaultClassType = 1 --USER
	BEGIN
		SELECT 
			@numAccountClass=ISNULL(numDefaultClass,0) 
		FROM 
			dbo.UserMaster UM 
		JOIN 
			dbo.Domain D 
		ON 
			UM.numDomainID=D.numDomainId
		WHERE 
			D.numDomainId=@numDomainId 
			AND UM.numUserDetailId=@numUserCntID
	END
	ELSE IF @tintDefaultClassType = 2 --COMPANY
	BEGIN
		SELECT 
			@numAccountClass=ISNULL(numAccountClassID,0) 
		FROM 
			dbo.DivisionMaster DM 
		WHERE 
			DM.numDomainId=@numDomainId 
			AND DM.numDivisionID=@numDivisionID
	END
	ELSE
	BEGIN
		SET @numAccountClass = 0
	END

	SELECT @numAccountClass
END
GO