GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_BizDocExistingAddress')
DROP PROCEDURE USP_BizDocExistingAddress
GO
set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go

-- =============================================  
-- Author:  <Ajit Kumar Singh>  
-- Create date: <Tuesday, September 30, 2008>  
-- Description: <This procedure is used for fetch the Existing Address from DivisionMaster table.>  
-- =============================================  
 
CREATE PROCEDURE [dbo].[USP_BizDocExistingAddress]    
@numDomainId as numeric(9),        
@numUserId as numeric(9),        
@numDivisionId as numeric(9) ,
@vcAddressType as varchar(15),
@numPrimaryContactCheck as varchar(10)

as    
declare @numCompanyId as numeric(9)
declare @vcCompanyName as varchar(200)

declare @numContactId numeric(18, 0)
begin   
	select @numCompanyId=numCompanyId from divisionmaster where numDomainID=@numDomainId and numDivisionID=@numDivisionId;
	select @vcCompanyName=vcCompanyName from CompanyInfo where numCompanyId=@numCompanyId;
 
	if (@numPrimaryContactCheck ='Yes')
	begin
		select @numContactId=numcontactid from AdditionalContactsInformation 
		where numDomainID=@numDomainId and numDivisionID=@numDivisionId;
			
		SELECT @numCompanyId as numCompanyId,@vcCompanyName as vcCompanyName,
		vcStreet as Street, vcCity as City, vcPostalCode as PostCode , 
		numState as State, numCountry  as Country,vcAddressName,ISNULL(numContact,0) numContact, ISNULL(bitAltContact,0) bitAltContact, ISNULL(vcAltContact,'') vcAltContact
		FROM dbo.AddressDetails WHERE numDomainID=@numDomainId AND numRecordID=@numContactId AND tintAddressOf=1 AND tintAddressType=0 AND bitIsPrimary=1;
	end
	if (@numPrimaryContactCheck ='No')
	begin
		if (@vcAddressType ='Bill')
		begin
		   select   @numCompanyId as numCompanyId,
					@vcCompanyName as vcCompanyName,
					vcStreet as Street,
					vcCity as City,
					numState as State,
					dbo.[fn_GetState]([numState]) vcState,
					numCountry as Country,
					dbo.fn_GetListItemName(numCountry) AS vcCountry,
					vcPostalCode as PostCode,vcAddressName
					,ISNULL(numContact,0) numContact
					, ISNULL(bitAltContact,0) bitAltContact
					, ISNULL(vcAltContact,'') vcAltContact
		  FROM     dbo.AddressDetails
				   WHERE    numDomainID = @numDomainID
							AND numRecordID = @numDivisionID
							AND tintAddressOf = 2
							AND tintAddressType = 1
							AND bitIsPrimary=1
		end

		if (@vcAddressType ='Ship')
		begin
			  select   @numCompanyId as numCompanyId,
					@vcCompanyName as vcCompanyName,
					vcStreet as Street,
					vcCity as City,
					numState as State,
					dbo.[fn_GetState]([numState]) vcState,
					numCountry as Country,
					dbo.fn_GetListItemName(numCountry) AS vcCountry,
					vcPostalCode as PostCode,vcAddressName
					,ISNULL(numContact,0) numContact
					, ISNULL(bitAltContact,0) bitAltContact
					, ISNULL(vcAltContact,'') vcAltContact
		  FROM     dbo.AddressDetails
				   WHERE    numDomainID = @numDomainID
							AND numRecordID = @numDivisionID
							AND tintAddressOf = 2
							AND tintAddressType = 2
							AND bitIsPrimary=1
		end
	end
end 
go