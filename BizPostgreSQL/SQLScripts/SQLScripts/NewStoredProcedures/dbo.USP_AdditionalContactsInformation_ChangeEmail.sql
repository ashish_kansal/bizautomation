IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_AdditionalContactsInformation_ChangeEmail')
DROP PROCEDURE USP_AdditionalContactsInformation_ChangeEmail
GO

CREATE PROCEDURE USP_AdditionalContactsInformation_ChangeEmail
	@numDomainID NUMERIC(18,0)
	,@numUserCntID NUMERIC(18,0)
	,@numContactID NUMERIC(18,0)
	,@vcEmail VARCHAR(300)
AS 
BEGIN
	UPDATE
		AdditionalContactsInformation
	SET
		vcEmail=@vcEmail
		,numModifiedBy = @numUserCntID
		,bintModifiedDate = GETUTCDATE()
	WHERE
		numDomainID=@numDomainID
		AND numContactID=@numContactID
END
GO