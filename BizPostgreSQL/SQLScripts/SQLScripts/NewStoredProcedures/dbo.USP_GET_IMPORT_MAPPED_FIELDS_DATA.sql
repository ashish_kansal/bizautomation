GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GET_IMPORT_MAPPED_FIELDS_DATA')
DROP PROCEDURE USP_GET_IMPORT_MAPPED_FIELDS_DATA
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[USP_GET_IMPORT_MAPPED_FIELDS_DATA] 
(
	@numDomainID NUMERIC(18,0),
	@numFormID NUMERIC(18,0),
	@vcFields VARCHAR(MAX)
)
AS 
BEGIN
	DECLARE @TempFields TABLE
	(
		numFieldID NUMERIC(18,0)
		,intMapColumnNo INT
		,bitCustom BIT
	)

	DECLARE @hDocItem int
	EXEC sp_xml_preparedocument @hDocItem OUTPUT, @vcFields

	INSERT INTO @TempFields
	(
		numFieldID
		,intMapColumnNo
		,bitCustom
	)
	SELECT
		FormFieldID
		,ImportFieldID
		,IsCustomField
	FROM
		OPENXML (@hDocItem,'/NewDataSet/Table1',2)
	WITH
	(
		FormFieldID NUMERIC(18,0)
		,ImportFieldID NUMERIC(18,0)
		,IsCustomField BIT
	)
	
	EXEC sp_xml_removedocument @hDocItem


	SELECT 
		* 
	FROM 
	(
		SELECT DISTINCT 
			PARENT.numFieldID AS [FormFieldID]
			,IFFM.intMapColumnNo AS [ImportFieldID]
			,PARENT.vcDbColumnName AS [dbFieldName]
			,PARENT.vcFieldName AS [FormFieldName]
			,ISNULL(PARENT.vcAssociatedControlType,'') vcAssociatedControlType
			,ISNULL(PARENT.bitDefault,0) bitDefault
			,ISNULL(PARENT.vcFieldType,'') vcFieldType
			,ISNULL(PARENT.vcListItemType,'') vcListItemType
			,ISNULL(PARENT.vcLookBackTableName,'') vcLookBackTableName
			,PARENT.vcOrigDbColumnName AS [vcOrigDbColumnName]
			,ISNULL(PARENT.vcPropertyName,'') vcPropertyName
			,0 AS [bitCustomField],
			PARENT.numDomainID AS [DomainID]
			,PARENT.vcFieldDataType
			,PARENT.intFieldMaxLength
		FROM 
			dbo.View_DynamicDefaultColumns PARENT  
		INNER JOIN 
			@TempFields IFFM 
		ON 
			PARENT.numFieldID = IFFM.numFieldID
			AND ISNULL(IFFM.bitCustom,0) = 0    		
		WHERE 
			PARENT.numDomainID = @numDomainID
			AND numFormID = @numFormID
		UNION ALL		
		SELECT DISTINCT 
			IFFM.numFieldID AS [FormFieldID]
			,IFFM.intMapColumnNo AS [ImportFieldID]
			,CFW.Fld_label AS [dbFieldName]
			,CFW.Fld_label AS [FormFieldName]
			,CASE WHEN CFW.fld_Type = 'Drop Down List Box' THEN 'SelectBox' ELSE CFW.fld_Type END vcAssociatedControlType
			,0 AS bitDefault
			,'R' AS vcFieldType
			,'' AS vcListItemType
			,'' AS  vcLookBackTableName
			,CFW.Fld_label AS [vcOrigDbColumnName]
			,'' AS vcPropertyName
			,1 AS [bitCustomField]
			,0
			,'' vcFieldDataType
			,0 intFieldMaxLength
		FROM @TempFields IFFM 
		INNER JOIN dbo.CFW_Fld_Master CFW ON IFFM.numFieldID = CFW.Fld_id AND ISNULL(bitCustom,0) = 1
		INNER JOIN dbo.CFW_Loc_Master LOC ON CFW.Grp_id = LOC.Loc_id										 
		INNER JOIN dbo.DycFormSectionDetail DFSD ON DFSD.Loc_id = LOC.Loc_id
		WHERE (DFSD.numFormID = @numFormID OR DFSD.numFormID = (CASE WHEN @numFormID = 20 THEN 48 ELSE @numFormID END))
		  AND Grp_id = DFSD.Loc_Id 
	) TABLE1
	ORDER BY 
		ImportFieldID DESC 


END
GO