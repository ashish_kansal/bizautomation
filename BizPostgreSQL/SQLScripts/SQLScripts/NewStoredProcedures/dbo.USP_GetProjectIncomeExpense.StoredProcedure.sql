-- exec USP_GetProjectIncomeExpense @numDomainID=72,@numProId=193,@Mode=1
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetProjectIncomeExpense')
DROP PROCEDURE USP_GetProjectIncomeExpense
GO
CREATE PROCEDURE [dbo].[USP_GetProjectIncomeExpense]
@numDomainID NUMERIC(9),
@numProId	NUMERIC(9),
@Mode tinyint
AS 
BEGIN

DECLARE @BillableHours AS FLOAT
DECLARE @NonBillableHours AS FLOAT
DECLARE @Income AS DECIMAL(20,5)
DECLARE @Income1 AS DECIMAL(20,5)
DECLARE @Income2 AS DECIMAL(20,5)
DECLARE @Expense AS DECIMAL(20,5)
DECLARE @Expense1 AS DECIMAL(20,5)
DECLARE @Expense2 AS DECIMAL(20,5)
DECLARE @Expense3 AS DECIMAL(20,5)
DECLARE @Expense4 AS DECIMAL(20,5)
DECLARE @numAccountID NUMERIC(9)


IF @Mode= 0 
BEGIN
		select @BillableHours = SUM(CAST (CONVERT(VARCHAR(100), CONVERT(DECIMAL(10, 1), ( CONVERT(DECIMAL(10, 1), DATEDIFF(minute, dtFromDate, dtToDate)) / 60 ))) AS  FLOAT))
		from  timeandexpense
		where numProID=@numProID 
		and numCategory = 1 and tintTEType = 2 AND [numType]=1 
		--  	 Non-Billable Hours: 
		select 
			@NonBillableHours = SUM(CAST (CONVERT(VARCHAR(100), CONVERT(DECIMAL(10, 1), ( CONVERT(DECIMAL(10, 1), DATEDIFF(minute, dtFromDate, dtToDate)) / 60 ))) AS  FLOAT))
		from  timeandexpense
		where numProID=@numProID 
			and numCategory = 1 and tintTEType = 2 AND [numType]=2
		
		--Income of the Project = Sum(Item*rate) of SO linked with the Project.
		SELECT  
		@Income1= SUM(OBI.[monPrice] * OBI.[numUnitHour])
		FROM [ProjectsOpportunities] PO
			LEFT OUTER JOIN [OpportunityMaster] OM ON OM.[numOppId] = PO.[numOppId]
			LEFT OUTER JOIN [OpportunityBizDocs] OB ON OB.[numOppBizDocsId] = PO.[numOppBizDocID]
			LEFT OUTER JOIN [OpportunityBizDocItems] OBI ON OBI.numOppBizDocID = PO.[numOppBizDocID]
		WHERE PO.[numProId]=@numProId AND OB.[bitAuthoritativeBizDocs]=1 AND OM.[tintOppType] = 1 
			--below condition gets amount for Project->linked Purchase Order->Sales order which are linked to PO
		SELECT  
			@Income2= SUM((OBI.[monPrice] * OBI.[numUnitHour]))
		FROM [OpportunityMaster] OM 
			LEFT OUTER JOIN [OpportunityBizDocs] OB ON OB.[numOppID] = OM.numOppID
			LEFT OUTER JOIN [OpportunityBizDocItems] OBI ON OBI.numOppBizDocID = OB.[numOppBizDocsID]
		WHERE OM.numOppID 
			IN (
			SELECT
				OL.numChildOppID
				FROM [ProjectsOpportunities] PO
					LEFT OUTER JOIN [OpportunityMaster] OM ON OM.[numOppId] = PO.[numOppId]
					LEFT OUTER JOIN [OpportunityBizDocs] OB ON OB.[numOppBizDocsId] = PO.[numOppBizDocID]
					LEFT OUTER JOIN [OpportunityLinking] OL ON OL.[numParentOppID] = PO.[numOppId]
				WHERE PO.[numProId]=@numProId AND OB.[bitAuthoritativeBizDocs]=1 AND OM.[tintOppType] = 2
			)
			SET @Income = ISNULL(@Income1,0) + ISNULL(@Income2,0)
		--Expenses =[ Sum(Debit) where numAccountID=ProjectsMaster.numAccountId] +[ sum(item*rate) of PO linked with project] + Bills added to project
		SELECT
		@Expense1= SUM(OBI.[monPrice] * OBI.[numUnitHour])
		FROM [ProjectsOpportunities] PO
			LEFT OUTER JOIN [OpportunityMaster] OM ON OM.[numOppId] = PO.[numOppId]
			LEFT OUTER JOIN [OpportunityBizDocs] OB ON OB.[numOppBizDocsId] = PO.[numOppBizDocID]
			LEFT OUTER JOIN [OpportunityBizDocItems] OBI ON OBI.numOppBizDocID = PO.[numOppBizDocID]
			LEFT OUTER JOIN [Item] I ON OBI.[numItemCode] = I.[numItemCode]
			LEFT OUTER JOIN [Chart_Of_Accounts] COA ON I.[numCOGsChartAcntId] = COA.[numAccountId]
		WHERE PO.[numProId]=@numProId AND OB.[bitAuthoritativeBizDocs]=1 AND OM.[tintOppType] = 2 
		AND I.charItemType='P'
		AND COA.[numAccountId] IS NOT NULL
		
		SELECT
		@Expense4= SUM(OBI.[monPrice] * OBI.[numUnitHour])
		FROM [ProjectsOpportunities] PO
			LEFT OUTER JOIN [OpportunityMaster] OM ON OM.[numOppId] = PO.[numOppId]
			LEFT OUTER JOIN [OpportunityBizDocs] OB ON OB.[numOppBizDocsId] = PO.[numOppBizDocID]
			LEFT OUTER JOIN [OpportunityBizDocItems] OBI ON OBI.numOppBizDocID = PO.[numOppBizDocID]
			LEFT OUTER JOIN [Item] I ON OBI.[numItemCode] = I.[numItemCode]
			LEFT OUTER JOIN [Chart_Of_Accounts] COA ON I.[numIncomeChartAcntId] = COA.[numAccountId]
		WHERE PO.[numProId]=@numProId AND OB.[bitAuthoritativeBizDocs]=1 AND OM.[tintOppType] = 2 
		AND I.charItemType <> 'P'
		AND COA.[numAccountId] IS NOT null
		
		SELECT @numAccountID = numAccountID FROM [ProjectsMaster] WHERE [numProId]=@numProId
		
		SELECT @Expense2 = SUM([numDebitAmt]) FROM [General_Journal_Header] H 
			INNER JOIN [General_Journal_Details] D ON H.[numJournal_Id]=D.[numJournalId]
			LEFT OUTER JOIN [Chart_Of_Accounts] COA ON D.[numChartAcntId] = COA.[numAccountId]
		WHERE D.[numChartAcntId] = @numAccountID AND H.[numProjectID]=@numProId AND COA.[numAccountId] IS NOT null
		--Bill Expense
		SELECT
		@Expense3= SUM(OBD.monAmount)
		FROM [ProjectsOpportunities] PO
			LEFT OUTER JOIN [OpportunityBizDocsDetails] OBD ON OBD.[numBizDocsPaymentDetId] = PO.numBillID
			LEFT OUTER JOIN [Chart_Of_Accounts] COA ON OBD.[numExpenseAccount] = COA.[numAccountId]
		WHERE PO.[numProId]=@numProId AND COA.[numAccountId] IS NOT null
		
		
		SET @Expense = ISNULL(@Expense1,0) + ISNULL(@Expense2,0) + ISNULL(@Expense3,0) + ISNULL(@Expense4,0);

		SELECT ISNULL(@BillableHours,0) BillableHours,ISNULL(@NonBillableHours,0) NonBillableHours,ISNULL(@Income,0) Income, ISNULL(@Expense,0) Expense
				
		 SELECT DISTINCT PM.numProId,
				PM.numAccountID,
				ISNULL(PM.vcProjectName,'') vcProjectName,
				ISNULL(CM.[vcContractName],'') vcContractName,
				ISNULL(PM.[numContractId],0) numContractId,
				PM.tintProStatus,
				ISNULL(PM.txtComments,'') txtComments,
				PM.numDivisionId
		--        TE.[numType],
		--        TE.[monAmount]
		 FROM   ProjectsMaster PM
				INNER JOIN [TimeAndExpense] TE ON PM.[numProId] = TE.[numProId]
				LEFT OUTER JOIN [ContractManagement] CM ON PM.[numContractId] = CM.[numContractId]
		 WHERE  PM.numProId = @numProId AND PM.[numDomainId]=@numDomainID
		 
		 --below condition gets amount for Project->linked Purchase Order->Sales order which are linked to PO
		 SELECT SUM(X.IncomeAmount) IncomeAmount,X.vcAccountName,X.[Type] FROM 
		(
		SELECT  
			SUM((OBI.[monPrice] * OBI.[numUnitHour])) AS IncomeAmount,
			COA.[vcAccountName],
			'Income' AS [Type]
		FROM [OpportunityMaster] OM 
			LEFT OUTER JOIN [OpportunityBizDocs] OB ON OB.[numOppID] = OM.numOppID
			LEFT OUTER JOIN [OpportunityBizDocItems] OBI ON OBI.numOppBizDocID = OB.[numOppBizDocsID]
			LEFT OUTER JOIN [Item] I ON OBI.[numItemCode] = I.[numItemCode]
			LEFT OUTER JOIN [Chart_Of_Accounts] COA ON I.[numIncomeChartAcntId] = COA.[numAccountId]
		WHERE OM.numOppID 
			IN (
			SELECT
				OL.numChildOppID
				FROM [ProjectsOpportunities] PO
					LEFT OUTER JOIN [OpportunityMaster] OM ON OM.[numOppId] = PO.[numOppId]
					LEFT OUTER JOIN [OpportunityBizDocs] OB ON OB.[numOppBizDocsId] = PO.[numOppBizDocID]
					LEFT OUTER JOIN [OpportunityLinking] OL ON OL.[numParentOppID] = PO.[numOppId]
				WHERE PO.[numProId]=@numProId AND OB.[bitAuthoritativeBizDocs]=1 AND OM.[tintOppType] = 2
			)
		GROUP BY vcAccountName
		UNION
		SELECT  
			SUM((OBI.[monPrice] * OBI.[numUnitHour])) AS IncomeAmount,
			COA.[vcAccountName],
			'Income' AS [Type]
		FROM [ProjectsOpportunities] PO
			LEFT OUTER JOIN [OpportunityMaster] OM ON OM.[numOppId] = PO.[numOppId]
			LEFT OUTER JOIN [OpportunityBizDocs] OB ON OB.[numOppBizDocsId] = PO.[numOppBizDocID]
			LEFT OUTER JOIN [OpportunityBizDocItems] OBI ON OBI.numOppBizDocID = PO.[numOppBizDocID]
			LEFT OUTER JOIN [Item] I ON OBI.[numItemCode] = I.[numItemCode]
			LEFT OUTER JOIN [Chart_Of_Accounts] COA ON I.[numIncomeChartAcntId] = COA.[numAccountId]   
		WHERE PO.[numProId]=@numProId AND OB.[bitAuthoritativeBizDocs]=1 AND OM.[tintOppType] = 1 
		AND COA.[numAccountId] IS NOT null
		GROUP BY vcAccountName,COA.[numAccountId]
		)as X
		GROUP BY X.vcAccountName,X.[Type]
		ORDER BY [vcAccountName]
		
		--Expenses =[ Sum(Debit) where numAccountID=ProjectsMaster.numAccountId] +[ sum(item*rate) of PO linked with project]
		SELECT SUM(X.ExpenseAmount) ExpenseAmount,X.vcAccountName,X.[Type] FROM 
		(
		SELECT
		SUM(OBI.[monPrice] * OBI.[numUnitHour]) ExpenseAmount,
		COA.[vcAccountName],'Expense' AS [Type]
		FROM [ProjectsOpportunities] PO
			LEFT OUTER JOIN [OpportunityMaster] OM ON OM.[numOppId] = PO.[numOppId]
			LEFT OUTER JOIN [OpportunityBizDocs] OB ON OB.[numOppBizDocsId] = PO.[numOppBizDocID]
			LEFT OUTER JOIN [OpportunityBizDocItems] OBI ON OBI.numOppBizDocID = PO.[numOppBizDocID]
			LEFT OUTER JOIN [Item] I ON OBI.[numItemCode] = I.[numItemCode]
			LEFT OUTER JOIN [Chart_Of_Accounts] COA ON I.[numCOGsChartAcntId] = COA.[numAccountId]
		WHERE PO.[numProId]=@numProId AND OB.[bitAuthoritativeBizDocs]=1 AND OM.[tintOppType] = 2 
		AND I.[charItemType]='P'
		AND COA.[numAccountId] IS NOT null
		GROUP BY vcAccountName
		UNION
		SELECT
		SUM(OBI.[monPrice] * OBI.[numUnitHour]) ExpenseAmount,
		COA.[vcAccountName],'Expense' AS [Type]
		FROM [ProjectsOpportunities] PO
			LEFT OUTER JOIN [OpportunityMaster] OM ON OM.[numOppId] = PO.[numOppId]
			LEFT OUTER JOIN [OpportunityBizDocs] OB ON OB.[numOppBizDocsId] = PO.[numOppBizDocID]
			LEFT OUTER JOIN [OpportunityBizDocItems] OBI ON OBI.numOppBizDocID = PO.[numOppBizDocID]
			LEFT OUTER JOIN [Item] I ON OBI.[numItemCode] = I.[numItemCode]
			LEFT OUTER JOIN [Chart_Of_Accounts] COA ON I.[numIncomeChartAcntId] = COA.[numAccountId]
		WHERE PO.[numProId]=@numProId AND OB.[bitAuthoritativeBizDocs]=1 AND OM.[tintOppType] = 2 
		AND I.[charItemType]<>'P'
		AND COA.[numAccountId] IS NOT null
		GROUP BY vcAccountName
		UNION 
		SELECT  
			SUM([numDebitAmt]) ExpenseAmount,COA.[vcAccountName],'Expense' AS [Type] FROM [General_Journal_Header] H
			INNER JOIN [General_Journal_Details] D ON H.[numJournal_Id]=D.[numJournalId]
			LEFT OUTER JOIN [Chart_Of_Accounts] COA ON D.[numChartAcntId] = COA.[numAccountId]
		WHERE D.[numChartAcntId] = @numAccountID AND H.[numProjectID]=@numProId AND COA.[numAccountId] IS NOT null
		GROUP BY [vcAccountName]
		UNION -- Bills expense
		SELECT 
			ISNULL(SUM(OBD.monamount),0) ExpenseAmount,
			COA.[vcAccountName],'Expense' AS [Type]
		FROM [ProjectsOpportunities] PO
			LEFT OUTER JOIN [OpportunityBizDocsDetails] OBD ON OBD.[numBizDocsPaymentDetId] = PO.numBillID
			LEFT OUTER JOIN [Chart_Of_Accounts] COA ON OBD.[numExpenseAccount] = COA.[numAccountId]
		WHERE PO.[numProId]=@numProId AND COA.[numAccountId] IS NOT null
		GROUP BY vcAccountName
			)as X
		GROUP BY X.vcAccountName,X.[Type]
		ORDER BY [vcAccountName]
	
END
 
	IF @Mode=1 
	BEGIN

		SELECT @numAccountID = numAccountID FROM [ProjectsMaster] WHERE [numProId]=@numProId
		
		--below condition gets amount for Project->linked Purchase Order->Sales order which are linked to PO
		SELECT SUM(X.IncomeAmount) IncomeAmount,SUM(X.ExpenseAmount) ExpenseAmount,X.vcAccountName,X.[Type] FROM 
		(SELECT  
			SUM((OBI.[monPrice] * OBI.[numUnitHour])) AS Amount,
			SUM((OBI.[monPrice] * OBI.[numUnitHour])) AS IncomeAmount,
			0 AS ExpenseAmount, 
			COA.[vcAccountName],
			'Income' AS [Type]
		FROM [OpportunityMaster] OM 
			LEFT OUTER JOIN [OpportunityBizDocs] OB ON OB.[numOppID] = OM.numOppID
			LEFT OUTER JOIN [OpportunityBizDocItems] OBI ON OBI.numOppBizDocID = OB.[numOppBizDocsID]
			LEFT OUTER JOIN [Item] I ON OBI.[numItemCode] = I.[numItemCode]
			LEFT OUTER JOIN [Chart_Of_Accounts] COA ON I.[numIncomeChartAcntId] = COA.[numAccountId]
		WHERE OM.numOppID 
			IN (
			SELECT
				OL.numChildOppID
				FROM [ProjectsOpportunities] PO
					LEFT OUTER JOIN [OpportunityMaster] OM ON OM.[numOppId] = PO.[numOppId]
					LEFT OUTER JOIN [OpportunityBizDocs] OB ON OB.[numOppBizDocsId] = PO.[numOppBizDocID]
					LEFT OUTER JOIN [OpportunityLinking] OL ON OL.[numParentOppID] = PO.[numOppId]
				WHERE PO.[numProId]=@numProId AND OB.[bitAuthoritativeBizDocs]=1 AND OM.[tintOppType] = 2
			)
			AND COA.[numAccountId] IS NOT null
		GROUP BY vcAccountName
		UNION
		SELECT  
			SUM((OBI.[monPrice] * OBI.[numUnitHour])) AS Amount,
			SUM((OBI.[monPrice] * OBI.[numUnitHour])) AS IncomeAmount,
			0 AS ExpenseAmount,
			COA.[vcAccountName],
			'Income' AS [Type]
		FROM [ProjectsOpportunities] PO
			LEFT OUTER JOIN [OpportunityMaster] OM ON OM.[numOppId] = PO.[numOppId]
			LEFT OUTER JOIN [OpportunityBizDocs] OB ON OB.[numOppBizDocsId] = PO.[numOppBizDocID]
			LEFT OUTER JOIN [OpportunityBizDocItems] OBI ON OBI.numOppBizDocID = PO.[numOppBizDocID]
			LEFT OUTER JOIN [Item] I ON OBI.[numItemCode] = I.[numItemCode]
			LEFT OUTER JOIN [Chart_Of_Accounts] COA ON I.[numIncomeChartAcntId] = COA.[numAccountId]   
		WHERE PO.[numProId]=@numProId AND OB.[bitAuthoritativeBizDocs]=1 AND OM.[tintOppType] = 1 
		AND COA.[numAccountId] IS NOT null
		GROUP BY vcAccountName
		UNION 
		--Expenses =[ Sum(Debit) where numAccountID=ProjectsMaster.numAccountId] +[ sum(item*rate) of PO linked with project]
		SELECT
		SUM(OBI.[monPrice] * OBI.[numUnitHour]) Amount,
		0 AS IncomeAmount,
		SUM(OBI.[monPrice] * OBI.[numUnitHour]) ExpenseAmount,
		COA.[vcAccountName],'Expense' AS [Type]
		FROM [ProjectsOpportunities] PO
			LEFT OUTER JOIN [OpportunityMaster] OM ON OM.[numOppId] = PO.[numOppId]
			LEFT OUTER JOIN [OpportunityBizDocs] OB ON OB.[numOppBizDocsId] = PO.[numOppBizDocID]
			LEFT OUTER JOIN [OpportunityBizDocItems] OBI ON OBI.numOppBizDocID = PO.[numOppBizDocID]
			LEFT OUTER JOIN [Item] I ON OBI.[numItemCode] = I.[numItemCode]
			LEFT OUTER JOIN [Chart_Of_Accounts] COA ON I.[numCOGsChartAcntId] = COA.[numAccountId]
		WHERE PO.[numProId]=@numProId AND OB.[bitAuthoritativeBizDocs]=1 AND OM.[tintOppType] = 2 
			AND I.charItemType ='P'
			AND COA.[numAccountId] IS NOT null
		GROUP BY vcAccountName
		UNION
		SELECT
		SUM(OBI.[monPrice] * OBI.[numUnitHour]) Amount,
		0 AS IncomeAmount,
		SUM(OBI.[monPrice] * OBI.[numUnitHour]) ExpenseAmount,
		COA.[vcAccountName],'Expense' AS [Type]
		FROM [ProjectsOpportunities] PO
			LEFT OUTER JOIN [OpportunityMaster] OM ON OM.[numOppId] = PO.[numOppId]
			LEFT OUTER JOIN [OpportunityBizDocs] OB ON OB.[numOppBizDocsId] = PO.[numOppBizDocID]
			LEFT OUTER JOIN [OpportunityBizDocItems] OBI ON OBI.numOppBizDocID = PO.[numOppBizDocID]
			LEFT OUTER JOIN [Item] I ON OBI.[numItemCode] = I.[numItemCode]
			LEFT OUTER JOIN [Chart_Of_Accounts] COA ON I.[numIncomeChartAcntId] = COA.[numAccountId]
		WHERE PO.[numProId]=@numProId AND OB.[bitAuthoritativeBizDocs]=1 AND OM.[tintOppType] = 2 
			AND I.charItemType <>'P'
			AND COA.[numAccountId] IS NOT null
		GROUP BY vcAccountName
		UNION 
		SELECT  
			SUM([numDebitAmt]) Amount,
			0 AS IncomeAmount,
			SUM([numDebitAmt]) ExpenseAmount,COA.[vcAccountName],'Expense' AS [Type] FROM [General_Journal_Header] H
			INNER JOIN [General_Journal_Details] D ON H.[numJournal_Id]=D.[numJournalId]
			LEFT OUTER JOIN [Chart_Of_Accounts] COA ON D.[numChartAcntId] = COA.[numAccountId]
		WHERE D.[numChartAcntId] = @numAccountID AND H.[numProjectID]=@numProId AND COA.[numAccountId] IS NOT null
		GROUP BY [vcAccountName]
		UNION -- Bills expense
		SELECT 
			ISNULL(SUM(OBD.monamount),0) Amount,
			0 AS IncomeAmount,
			ISNULL(SUM(OBD.monamount),0) ExpenseAmount,
			COA.[vcAccountName],'Expense' AS [Type]
		FROM [ProjectsOpportunities] PO
			LEFT OUTER JOIN [OpportunityBizDocsDetails] OBD ON OBD.[numBizDocsPaymentDetId] = PO.numBillID
			LEFT OUTER JOIN [Chart_Of_Accounts] COA ON OBD.[numExpenseAccount] = COA.[numAccountId]
		WHERE PO.[numProId]=@numProId AND COA.[numAccountId] IS NOT null
		GROUP BY vcAccountName
		)as X
		GROUP BY X.vcAccountName,X.[Type]
		ORDER BY [vcAccountName]
		
		SELECT ISNULL(vcProjectName,'') vcProjectName FROM [ProjectsMaster] WHERE [numProId]=@numProId
	END 

END

