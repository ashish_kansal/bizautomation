GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='Usp_GetHelpByID')
DROP PROCEDURE Usp_GetHelpByID
GO
CREATE PROCEDURE Usp_GetHelpByID
    @numHelpId NUMERIC(9)
AS 
    BEGIN  
        SELECT  *
        FROM    helpmaster
        WHERE   numHelpId = @numHelpId  
    END 