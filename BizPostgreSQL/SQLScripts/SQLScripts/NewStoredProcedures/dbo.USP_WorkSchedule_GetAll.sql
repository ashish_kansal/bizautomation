GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_WorkSchedule_GetAll')
DROP PROCEDURE dbo.USP_WorkSchedule_GetAll
GO
CREATE PROCEDURE [dbo].[USP_WorkSchedule_GetAll]
(
	@numDomainID NUMERIC(18,0)
)
AS 
BEGIN
	SELECT
		UserMaster.numUserId
		,UserMaster.numUserDetailId
		,CONCAT(ISNULL(vcFirstName,'-'),' ',ISNULL(vcLastName,'-')) vcUserName
		,ISNULL(WorkSchedule.ID,0) AS numWorkScheduleID
		,WorkSchedule.numWorkHours
		,WorkSchedule.numWorkMinutes
		,WorkSchedule.numProductiveHours
		,WorkSchedule.numProductiveMinutes
		,WorkSchedule.tmStartOfDay
		,WorkSchedule.vcWorkDays
		,stuff((
			SELECT 
				CONCAT(', ',(CASE 
								WHEN WorkScheduleDaysOff.dtDayOffFrom = WorkScheduleDaysOff.dtDayOffTo 
								THEN dbo.FormatedDateFromDate(WorkScheduleDaysOff.dtDayOffFrom,@numDomainID) 
								ELSE CONCAT(dbo.FormatedDateFromDate(WorkScheduleDaysOff.dtDayOffFrom,@numDomainID),' - ',dbo.FormatedDateFromDate(WorkScheduleDaysOff.dtDayOffTo,@numDomainID))
							END))
			FROM 
				WorkScheduleDaysOff
			WHERE
				 numWorkScheduleID = ISNULL(WorkSchedule.ID,0)
			ORDER BY 
				dtDayOffFrom
			FOR XML PATH('')
		),1,1,'') AS vcDaysOff
	FROM
		UserMaster
	INNER JOIN
		AdditionalContactsInformation
	ON
		UserMaster.numUserDetailId = AdditionalContactsInformation.numContactId
	LEFT JOIN
		WorkSchedule
	ON
		UserMaster.numUserDetailId = WorkSchedule.numUserCntID
		AND WorkSchedule.numDomainID=@numDomainID
	WHERE
		UserMaster.numDomainID=@numDomainID
		AND ISNULL(UserMaster.bitActivateFlag,0) = 1
		AND ISNULL(UserMaster.numUserDetailId,0) > 0
	ORDER BY
		vcUserName
END
GO