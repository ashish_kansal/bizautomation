/****** Object:  StoredProcedure [dbo].[USP_GetCustomTabGetFields]    Script Date: 07/26/2008 16:15:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj                    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetCustomTabGetFields')
DROP PROCEDURE USP_GetCustomTabGetFields
GO
CREATE PROCEDURE [dbo].[USP_GetCustomTabGetFields]                              
@PageId as tinyint,                              
@numRelation as numeric(9),                              
@numRecordId as numeric(9),                  
@numDomainID as numeric(9),
@numUserCntID as numeric(9),
@numFormID as numeric(9),
@numTabID as numeric(9)=0,
@tintMode as tinyint=0            
as            

IF @tintMode=1
BEGIN
if((select count(*) from DycFormConfigurationDetails DFCD where DFCD.numUserCntID=@numUserCntID and DFCD.numDomainID=@numDomainID and DFCD.numFormId=@numFormID and
DFCD.numRelCntType=@numRelation AND DFCD.tintPageType=5 AND DFCD.numAuthGroupID=@numTabID) <>0 )
BEGIN
if @PageId= 1 
begin                              
select CFM.fld_id,fld_type,fld_label,numlistid,numOrder,Rec.FldDTLID,isnull(Rec.Fld_Value,0) as Value,subgrp as TabId,Grp_Name as tabname,vcURL,CFM.vcToolTip,
DFCD.intColumnNum as intcoulmn,DFCD.intRowNum 
from CFW_Fld_Master CFM join CFW_Fld_Dtl on Fld_id=numFieldId                              
join CFw_Grp_Master CGM on subgrp=CGM.Grp_id 
left join dbo.GetCustomFieldDTLIDAndValues(@PageId,@numRecordId) Rec on Rec.Fld_ID=CFM.fld_id         
join DycFormConfigurationDetails DFCD on DFCD.numAuthGroupID=CGM.Grp_id and DFCD.numFieldId=CFM.fld_id
where CFM.grp_id=@PageId and numRelation=@numRelation and CFM.numDomainID=@numDomainID                          
and DFCD.numUserCntID=@numUserCntID and DFCD.numDomainID=@numDomainID and DFCD.numFormId=@numFormID and
DFCD.numRelCntType=@numRelation AND DFCD.tintPageType=5 AND DFCD.numAuthGroupID=CGM.Grp_id
AND CGM.Grp_id=@numTabID
order by TabId,DFCD.intRowNum,DFCD.intColumnNum
end                              

---select Shared Custom fields and tab specific customfields -- where Pageid=1 stands for shared custom fields among accounts,leads,prospects
IF  @PageId= 12 or  @PageId= 13 or  @PageId= 14
BEGIN
	select CFM.fld_id,fld_type,fld_label,numlistid,numOrder,Rec.FldDTLID,isnull(Rec.Fld_Value,0) as Value,subgrp as TabId,Grp_Name as tabname,vcURL,CFM.vcToolTip,
DFCD.intColumnNum as intcoulmn,DFCD.intRowNum 
from CFW_Fld_Master CFM join CFW_Fld_Dtl on Fld_id=numFieldId                              
join CFw_Grp_Master CGM on subgrp=CGM.Grp_id 
left join dbo.GetCustomFieldDTLIDAndValues(@PageId,@numRecordId) Rec on Rec.Fld_ID=CFM.fld_id    
join DycFormConfigurationDetails DFCD on DFCD.numAuthGroupID=CGM.Grp_id and DFCD.numFieldId=CFM.fld_id
	where CFM.grp_id=@PageId and numRelation=@numRelation and CFM.numDomainID=@numDomainID                          
	and DFCD.numUserCntID=@numUserCntID and DFCD.numDomainID=@numDomainID and DFCD.numFormId=@numFormID and
DFCD.numRelCntType=@numRelation AND DFCD.tintPageType=5 AND DFCD.numAuthGroupID=CGM.Grp_id
AND CGM.Grp_id=@numTabID

	UNION 
	
	select CFM.fld_id,fld_type,fld_label,numlistid,numOrder,Rec.FldDTLID,isnull(Rec.Fld_Value,0) as Value,subgrp as TabId,Grp_Name as tabname,vcURL,CFM.vcToolTip,
DFCD.intColumnNum as intcoulmn,DFCD.intRowNum 
from CFW_Fld_Master CFM join CFW_Fld_Dtl on Fld_id=numFieldId                              
join CFw_Grp_Master  CGM on subgrp=CGM.Grp_id 
left join dbo.GetCustomFieldDTLIDAndValues(1,@numRecordId) Rec on Rec.Fld_ID=CFM.fld_id  
join DycFormConfigurationDetails DFCD on DFCD.numAuthGroupID=CGM.Grp_id and DFCD.numFieldId=CFM.fld_id
	where CFM.grp_id=1 and numRelation=@numRelation and CFM.numDomainID=@numDomainID
	     and DFCD.numUserCntID=@numUserCntID and DFCD.numDomainID=@numDomainID and DFCD.numFormId=@numFormID and
DFCD.numRelCntType=@numRelation AND DFCD.tintPageType=5 AND DFCD.numAuthGroupID=CGM.Grp_id
  AND CGM.Grp_id=@numTabID
  
order by TabId,intRowNum,intColumnNum
END


                              
if @PageId= 4                              
begin       
                              
select CFM.fld_id,fld_type,fld_label,numlistid,numOrder,
Rec.FldDTLID,isnull(Rec.Fld_Value,0) as Value,
subgrp as TabId,Grp_Name as tabname,vcURL,CFM.vcToolTip,
DFCD.intColumnNum as intcoulmn,DFCD.intRowNum 
from CFW_Fld_Master CFM join CFW_Fld_Dtl on Fld_id=numFieldId                              
join CFw_Grp_Master   CGM on subgrp=CGM.Grp_id  
left join dbo.GetCustomFieldDTLIDAndValues(@PageId,@numRecordId) Rec on Rec.Fld_ID=CFM.fld_id  
join DycFormConfigurationDetails DFCD on DFCD.numAuthGroupID=CGM.Grp_id and DFCD.numFieldId=CFM.fld_id
where CFM.grp_id=@PageId and numRelation=@numRelation and CFM.numDomainID=@numDomainID 
and DFCD.numUserCntID=@numUserCntID and DFCD.numDomainID=@numDomainID and DFCD.numFormId=@numFormID and
DFCD.numRelCntType=@numRelation AND DFCD.tintPageType=5 AND DFCD.numAuthGroupID=CGM.Grp_id
AND CGM.Grp_id=@numTabID
 order by TabId,DFCD.intRowNum,DFCD.intColumnNum
                           
end                              
                              
if @PageId= 2 or  @PageId= 3 or @PageId= 5 or @PageId= 6 or @PageId= 7 or @PageId= 8     or @PageId= 11                   
begin                              
select CFM.fld_id,fld_type,fld_label,numlistid,ISNULL(Rec.FldDTLID,0) FldDTLID,isnull(Rec.Fld_Value,0) as Value,subgrp as TabId,Grp_Name as tabname,vcURL,CFM.vcToolTip,
DFCD.intColumnNum as intcoulmn,DFCD.intRowNum  
from CFW_Fld_Master  CFM join CFw_Grp_Master   CGM on subgrp=CGM.Grp_id  
left join dbo.GetCustomFieldDTLIDAndValues(@PageId,@numRecordId) Rec on Rec.Fld_ID=CFM.fld_id 
join DycFormConfigurationDetails DFCD on DFCD.numAuthGroupID=CGM.Grp_id and DFCD.numFieldId=CFM.fld_id                            
where CFM.grp_id=@PageId and CFM.numDomainID=@numDomainID   
and DFCD.numUserCntID=@numUserCntID and DFCD.numDomainID=@numDomainID and DFCD.numFormId=@numFormID and
DFCD.numRelCntType=@numRelation AND DFCD.tintPageType=5 AND DFCD.numAuthGroupID=CGM.Grp_id
AND CGM.Grp_id=@numTabID
order by TabId,DFCD.intRowNum,DFCD.intColumnNum

end
END

ELSE
BEGIN

   CREATE TABLE #tempFieldsList(ID int IDENTITY(1,1) NOT NULL,fld_id numeric(9),fld_type VARCHAR(20),
fld_label VARCHAR(45),numlistid numeric(9),FldDTLID numeric(9),Value text,TabId varchar(30),Grp_Name varchar(50),
vcURL varchar(1000),vcToolTip varchar(1000),RowNo int,intcoulmn int,intRowNum int)


if @PageId= 1 
begin     
insert into #tempFieldsList(fld_id,fld_type,fld_label,numlistid,FldDTLID,Value,TabId,Grp_Name,vcURL,vcToolTip)                         
select CFM.fld_id,fld_type,fld_label,numlistid,Rec.FldDTLID,isnull(Rec.Fld_Value,0) as Value,subgrp as TabId,Grp_Name as tabname,vcURL,CFM.vcToolTip
from CFW_Fld_Master CFM join CFW_Fld_Dtl on Fld_id=numFieldId                              
join CFw_Grp_Master CGM on subgrp=CGM.Grp_id 
left join dbo.GetCustomFieldDTLIDAndValues(@PageId,@numRecordId) Rec on Rec.Fld_ID=CFM.fld_id         
where CFM.grp_id=@PageId and numRelation=@numRelation and CFM.numDomainID=@numDomainID                          
AND CGM.Grp_id=@numTabID
end                              

---select Shared Custom fields and tab specific customfields -- where Pageid=1 stands for shared custom fields among accounts,leads,prospects
IF  @PageId= 12 or  @PageId= 13 or  @PageId= 14
BEGIN
insert into #tempFieldsList(fld_id,fld_type,fld_label,numlistid,FldDTLID,Value,TabId,Grp_Name,vcURL,vcToolTip)                         
	select CFM.fld_id,fld_type,fld_label,numlistid,Rec.FldDTLID,isnull(Rec.Fld_Value,0) as Value,subgrp as TabId,Grp_Name as tabname,vcURL,CFM.vcToolTip
from CFW_Fld_Master CFM join CFW_Fld_Dtl on Fld_id=numFieldId                              
join CFw_Grp_Master CGM on subgrp=CGM.Grp_id 
left join dbo.GetCustomFieldDTLIDAndValues(@PageId,@numRecordId) Rec on Rec.Fld_ID=CFM.fld_id    
	where (CFM.grp_id=@PageId or CFM.grp_id=1) and numRelation=@numRelation and CFM.numDomainID=@numDomainID                          
	AND CGM.Grp_id=@numTabID

Union

	select CFM.fld_id,fld_type,fld_label,numlistid,Rec.FldDTLID,isnull(Rec.Fld_Value,0) as Value,subgrp as TabId,Grp_Name as tabname,vcURL,CFM.vcToolTip
from CFW_Fld_Master CFM join CFW_Fld_Dtl on Fld_id=numFieldId                              
join CFw_Grp_Master CGM on subgrp=CGM.Grp_id 
left join dbo.GetCustomFieldDTLIDAndValues(1,@numRecordId) Rec on Rec.Fld_ID=CFM.fld_id    
	where  CFM.grp_id=1 and numRelation=@numRelation and CFM.numDomainID=@numDomainID                          
	AND CGM.Grp_id=@numTabID
END


                              
if @PageId= 4                              
begin       

insert into #tempFieldsList(fld_id,fld_type,fld_label,numlistid,FldDTLID,Value,TabId,Grp_Name,vcURL,vcToolTip)                         
select CFM.fld_id,fld_type,fld_label,numlistid,
Rec.FldDTLID,isnull(Rec.Fld_Value,0) as Value,
subgrp as TabId,Grp_Name as tabname,vcURL,CFM.vcToolTip
from CFW_Fld_Master CFM join CFW_Fld_Dtl on Fld_id=numFieldId                              
join CFw_Grp_Master   CGM on subgrp=CGM.Grp_id  
left join dbo.GetCustomFieldDTLIDAndValues(@PageId,@numRecordId) Rec on Rec.Fld_ID=CFM.fld_id  
where CFM.grp_id=@PageId and numRelation=@numRelation and CFM.numDomainID=@numDomainID 
AND CGM.Grp_id=@numTabID
end                              
                              
if @PageId= 2 or  @PageId= 3 or @PageId= 5 or @PageId= 6 or @PageId= 7 or @PageId= 8     or @PageId= 11                   
begin
insert into #tempFieldsList(fld_id,fld_type,fld_label,numlistid,FldDTLID,Value,TabId,Grp_Name,vcURL,vcToolTip)                         
select CFM.fld_id,fld_type,fld_label,numlistid,ISNULL(Rec.FldDTLID,0) FldDTLID,isnull(Rec.Fld_Value,0) as Value,subgrp as TabId,Grp_Name as tabname,vcURL,CFM.vcToolTip
from CFW_Fld_Master  CFM join CFw_Grp_Master   CGM on subgrp=CGM.Grp_id  
left join dbo.GetCustomFieldDTLIDAndValues(@PageId,@numRecordId) Rec on Rec.Fld_ID=CFM.fld_id 
where CFM.grp_id=@PageId and CFM.numDomainID=@numDomainID  
AND CGM.Grp_id=@numTabID

end

Update #tempFieldsList set RowNo=ID-1
Update #tempFieldsList set  intRowNum=(RowNo/2)+1 ,intcoulmn= (RowNo%2)+1 

SELECT * FROM #tempFieldsList order by intRowNum,intcoulmn
        
DROP TABLE #tempFieldsList
END
END

ELSE
BEGIN
select GRp_Id as TabID,Grp_Name AS TabName from CFw_Grp_Master where Loc_ID=@PageId and numDomainID=@numDomainID
/*tintType stands for static tabs */ AND tintType<>2
END