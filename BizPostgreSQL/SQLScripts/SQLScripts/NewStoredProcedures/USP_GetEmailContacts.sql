GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_GetEmailContacts' ) 
    DROP PROCEDURE USP_GetEmailContacts
GO
Create PROCEDURE [dbo].[USP_GetEmailContacts]
@numDomainId as numeric(9),  
@numUserCntId as numeric(9)
AS
BEGIN
SELECT numContactId,vcFirstName,vcLastName FROM AdditionalContactsInformation AS A 
WHERE numDomainID=@numUserCntId AND numContactId IN(SELECT DISTINCT EM.numContactId FROM EmailHistory AS EH LEFT JOIN EmailMaster AS EM ON EH.numEmailId=EM.numEmailId WHERE EH.numUserCntId=@numUserCntId AND EH.numDomainID=@numDomainId AND ISNULL(EM.numContactId,0)<>0)
END        