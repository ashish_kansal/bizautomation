GO
/****** Object:  StoredProcedure [dbo].[USP_DELETE_CUSTOM_BOXES]    Script Date: 30/07/2012 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO

IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_DELETE_CUSTOM_BOXES' ) 
    DROP PROCEDURE USP_DELETE_CUSTOM_BOXES
GO
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE USP_DELETE_CUSTOM_BOXES 
(
	@numPackageTypeID		NUMERIC(18)
)	
AS 
BEGIN

DELETE FROM dbo.CustomPackages WHERE numPackageTypeID = @numPackageTypeID 	

END

	