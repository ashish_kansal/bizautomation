GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_Category_GetParentCategories')
DROP PROCEDURE dbo.USP_Category_GetParentCategories
GO
CREATE PROCEDURE [dbo].[USP_Category_GetParentCategories]
(
	@numDomainID NUMERIC(18,0)
	,@numCategoryID NUMERIC(18,0)
)
AS 
BEGIN
	;WITH CTE (numCategoryID,numDepCategory,vcCategoryName,tintLevel) AS
	(
		SELECT 
			Category.numCategoryID
			,Category.numDepCategory
			,Category.vcCategoryName
			,0
		FROM
			Category
		WHERE
			Category.numDomainID=@numDomainID
			AND Category.numCategoryID=@numCategoryID
		UNION ALL
		SELECT
			Category.numCategoryID
			,Category.numDepCategory
			,Category.vcCategoryName
			,C.tintLevel + 1
		FROM
			Category
		INNER JOIN
			CTE C
		ON
			Category.numCategoryID = C.numDepCategory
	)

	SELECT * FROM CTE ORDER BY tintLevel DESC
END
GO