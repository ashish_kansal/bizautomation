GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetRecurringTransactions')
DROP PROCEDURE USP_GetRecurringTransactions
GO
CREATE PROCEDURE USP_GetRecurringTransactions
@tintRecurringType TINYINT,
@dtStartDate DATETIME,
@dtEndDate DATETIME,
@numOppRecID NUMERIC=0
AS 
BEGIN
	
	IF @tintRecurringType=1
	BEGIN
		SELECT * FROM dbo.OpportunityRecurring WHERE tintRecurringType = tintRecurringType	
	END
	--Authoritative BizDocs
	IF @tintRecurringType=2
	BEGIN
	
	 
	
	  SELECT    numOppRecID,
				OPR.numOppId,
				OPR.numOppBizDocID,
				OPR.numRecurringId,
				OPR.dtRecurringDate,
				OPR.bitBillingTerms,
				OPR.fltBreakupPercentage,
				OPR.numDomainID,
				CASE OM.tintOppType WHEN 1 THEN (SELECT numAuthoritativeSales FROM dbo.AuthoritativeBizDocs WHERE numDomainID=OPR.numDomainID )
			   WHEN 2 THEN (SELECT numAuthoritativePurchase FROM dbo.AuthoritativeBizDocs WHERE numDomainID=OPR.numDomainID )
			   END AS AuthBizDocID,
			   OM.tintOppType,
			   OM.numRecOwner,
			   OM.numDivisionID,
--			   CASE OPR.bitBillingTerms 
--					WHEN 1 THEN dbo.GetListIemName(OPR.numBillingDays)
--					ELSE 0 
--			   END AS intBillingDays,
			   CASE OPR.bitBillingTerms 
					WHEN 1 THEN (SELECT numNetDueInDays FROM dbo.BillingTerms WHERE numTermsID = OM.intBillingDays)--dbo.GetListIemName(OPR.numBillingDays)
					ELSE 0 
			   END AS intBillingDays,
			   OPR.numBillingDays, OPR.tintRecurringType/*,ISNULL(D.bitAllowPPVariance,0) AS bitAllowPPVariance*/
			   
	  FROM      dbo.OpportunityRecurring OPR
				INNER JOIN dbo.OpportunityMaster OM ON OM.numOppID = OPR.numOppID
				JOIN Domain D ON D.numDomainID=OM.numDomainID
	  WHERE   
  OPR.tintRecurringType = tintRecurringType
				AND (
					(OPR.dtRecurringDate BETWEEN @dtStartDate
										AND     @dtEndDate)
										OR 
										(@numOppRecID > 0)
					)
				AND (ISNULL(OPR.numOppBizDocID, 0) = 0 or (OPR.tintRecurringType=4 and (select isnull(tintDeferred,0) from  OpportunityBizDocs where numOppBizDocsId=ISNULL(OPR.numOppBizDocID, 0) and tintDeferred=1)=1))
				AND 
				(OPR.numOppRecID =@numOppRecID OR @numOppRecID = 0)
	
	END
	
	
END