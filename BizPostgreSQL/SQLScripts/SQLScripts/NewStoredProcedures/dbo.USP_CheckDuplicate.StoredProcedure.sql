GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_CheckDuplicate')
DROP PROCEDURE USP_CheckDuplicate
GO
/****** Object:  StoredProcedure [dbo].[USP_CheckDuplicate]    Script Date: 05/07/2009 22:22:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[USP_CheckDuplicate]
@numDomainID NUMERIC(9),
@WhereCondition VARCHAR(8000)
AS 
BEGIN
	
DECLARE @Sql NVARCHAR(1000)

SET @Sql ='SELECT top 1 [vcCompanyName] FROM AdditionalContactsInformation ADC JOIN DivisionMaster DM ON ADC.numDivisionId = DM.numDivisionID JOIN CompanyInfo C ON DM.numCompanyID = C.numCompanyId 
		   WHERE C.[numDomainID] ='+ CONVERT(VARCHAR(20),@numDomainID) + ' and '
		
SET @Sql = @Sql + @WhereCondition
PRINT @Sql
EXEC(@Sql)
	
END