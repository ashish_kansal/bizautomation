GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_Sites_GetCategoryFilters')
DROP PROCEDURE dbo.USP_Sites_GetCategoryFilters
GO
CREATE PROCEDURE [dbo].[USP_Sites_GetCategoryFilters]
(
	@numDomainID NUMERIC(18,0)
	,@numCategoryID NUMERIC(18,0)
)
AS 
BEGIN
	SELECT
		CFW_Fld_Master.Fld_id
		,CFW_Fld_Master.Fld_label
		,CFW_Fld_Master.numlistid
	FROM
		Category
	INNER JOIN
		CategoryMatrixGroup 
	ON
		Category.numCategoryID = CategoryMatrixGroup.numCategoryID
	INNER JOIN
		ItemGroups
	ON
		CategoryMatrixGroup.numItemGroup = ItemGroups.numItemGroupID
	INNER JOIN
		ItemGroupsDTL
	ON
		ItemGroups.numItemGroupID = ItemGroupsDTL.numItemGroupID
		AND ItemGroupsDTL.tintType = 2
	INNER JOIN
		CFW_Fld_Master
	ON
		ItemGroupsDTL.numOppAccAttrID=CFW_Fld_Master.Fld_id    
	WHERE
		Category.numDomainID = @numDomainID
		AND Category.numCategoryID = @numCategoryID
	GROUP BY
		CFW_Fld_Master.Fld_id
		,CFW_Fld_Master.Fld_label
		,CFW_Fld_Master.numlistid
END
GO