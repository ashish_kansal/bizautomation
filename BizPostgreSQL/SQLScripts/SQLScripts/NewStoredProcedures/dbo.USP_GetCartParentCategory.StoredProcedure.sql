SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetCartParentCategory')
DROP PROCEDURE USP_GetCartParentCategory
GO
CREATE PROCEDURE [dbo].[USP_GetCartParentCategory]                        
@numSiteID AS NUMERIC(9)=0 
as   

DECLARE @bParentCatrgory bit
SELECT @bParentCatrgory=vcAttributeValue FROM PageElementDetail WHERE [numSiteID] = @numSiteID AND numAttributeID=39
SElect ISNULL(@bParentCatrgory,0)
