GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p' AND NAME ='USP_ElasticSearchBizCart_DisableDomain')
DROP PROCEDURE USP_ElasticSearchBizCart_DisableDomain
GO

/****** Object:  StoredProcedure [dbo].[USP_ElasticSearchBizCart_DisableDomain]    Script Date: 27/04/2020 14:17:46 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[USP_ElasticSearchBizCart_DisableDomain]
@numDomainID AS NUMERIC(9,0)
AS
BEGIN
	UPDATE eCommerceDTL 
	SET dtElasticSearchDisableDatetime = NULL
	WHERE numDomainId = @numDomainID
END
GO


