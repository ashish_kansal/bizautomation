GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ItemDetails_ValidateKitSelection')
DROP PROCEDURE USP_ItemDetails_ValidateKitSelection
GO
CREATE PROCEDURE [dbo].[USP_ItemDetails_ValidateKitSelection]  
	@numDomainID NUMERIC(18,0)
	,@numItemCode NUMERIC(18,0)
	,@vcKitChildItems VARCHAR(MAX)
AS  
BEGIN  
	DECLARE @bitValidKitSelection BIT = 1

	--TODO: Prepare validation logic

	SELECT @bitValidKitSelection
END
GO
