GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetEmbeddedCostConfig')
DROP PROCEDURE USP_GetEmbeddedCostConfig
GO
CREATE PROCEDURE USP_GetEmbeddedCostConfig
    @numOppBizDocID NUMERIC,
    @numDomainID NUMERIC
AS 
    BEGIN
        SELECT  
                [numLastViewedCostCatID]
        FROM    [EmbeddedCostConfig]
        WHERE   [numOppBizDocID] = @numOppBizDocID
                AND [numDomainID] = @numDomainID
    END
    
 