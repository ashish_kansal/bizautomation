
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_InsertGoogleContactGroups')
DROP PROCEDURE USP_InsertGoogleContactGroups
GO
CREATE PROCEDURE [dbo].[USP_InsertGoogleContactGroups]
@numDomainID AS numeric(9)=0,
@numContactId AS numeric(9)=0,
@tintType AS TINYINT=0,
@vcGroupId AS varchar(50)=0,
@tintMode AS TINYINT
AS
BEGIN

IF @tintMode=1
Begin
	insert into GoogleContactGroups(numDomainID,numContactId,tintType,vcGroupId)
		values(@numDomainID,@numContactId,@tintType,@vcGroupId)
END
ELSE IF @tintMode=2
BEGIN
	DELETE FROM GoogleContactGroups WHERE numDomainID=@numDomainID AND numContactId=@numContactId AND tintType=@tintType
END

END