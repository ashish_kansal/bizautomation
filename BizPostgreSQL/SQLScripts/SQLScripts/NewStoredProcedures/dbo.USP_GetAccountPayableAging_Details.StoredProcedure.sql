/****** Object:  StoredProcedure [dbo].[USP_GetAccountPayableAging_Details]    Script Date: 03/06/2009 00:30:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- exec [dbo].USP_GetAccountPayableAging_Details 1,7281,'90-'
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetAccountPayableAging_Details')
DROP PROCEDURE USP_GetAccountPayableAging_Details
GO
CREATE PROCEDURE [dbo].[USP_GetAccountPayableAging_Details]
(
    @numDomainId   AS NUMERIC(9)  = 0,
    @numDivisionID NUMERIC(9),
    @vcFlag        VARCHAR(20),
	@numAccountClass AS NUMERIC(9)=0,
	@dtFromDate AS DATE = NULL,
	@dtToDate AS DATE = NULL,
	@ClientTimeZoneOffset INT = 0
)
AS
  BEGIN
	DECLARE @strSqlJournal VARCHAR(8000);
	DECLARE @strSqlOrder VARCHAR(8000);
	Declare @strSqlCommission varchar(8000);
	Declare @strSqlBill varchar(8000);
	Declare @strSqlCheck varchar(8000);
	DECLARE @strReturn VARCHAR(MAX);
	DECLARE @strSqlCredit VARCHAR(MAX);
	
    DECLARE  @AuthoritativePurchaseBizDocId  AS INTEGER
    SELECT  @AuthoritativePurchaseBizDocId= isnull(numAuthoritativePurchase,0)
    FROM   AuthoritativeBizDocs
    WHERE  numDomainId =  @numDomainId
       ------------------------------------------      
    DECLARE @baseCurrency AS NUMERIC
	SELECT @baseCurrency=numCurrencyID FROM dbo.Domain WHERE numDomainId=@numDomainId
	/*when any Order is placed in Foreign currency (other than base currency)then show it in purple color */
	------------------------------------------      
	IF @dtFromDate IS NULL
		SET @dtFromDate = '1990-01-01 00:00:00.000'
	IF @dtToDate IS NULL
		SET @dtToDate = GETUTCDATE()
     
    --Get Master data
	 
	 SET @strSqlBill = 'SELECT  
						  Case When isnull(BH.bitLandedCost,0)=1 then  isnull(BH.numOppId,0) else 0 end AS numOppId,
						   ''Bill'' vcPOppName,
						   -2 AS [tintOppType],-- flag for bills 
						   0 numOppBizDocsId,
						   CASE WHEN len(BH.vcReference)=0 THEN '''' ELSE BH.vcReference END AS [vcBizDocID],
						   BH.monAmountDue as TotalAmount,
						   ISNULL(TablePayment.monAmtPaid, 0) as AmountPaid,
					       ISNULL(BH.monAmountDue, 0) - ISNULL(TablePayment.monAmtPaid, 0) as BalanceDue,
						   [dbo].[FormatedDateFromDate](BH.dtDueDate,BH.numDomainID) AS DueDate,
						   0 bitBillingTerms,
						   0 intBillingDays,
						   BH.dtCreatedDate dtCreatedDate,
						   '''' varCurrSymbol,
						   1 AS fltExchangeRate,
						   BH.dtDueDate AS dtFromDate,
						   0 as numBizDocsPaymentDetId,
						   1 AS CurrencyCount,0 as numComissionID,'''' as vcRefOrderNo,BH.numBillID,0 AS numCheckHeaderID
					FROM    
							BillHeader BH
					OUTER APPLY
					(
						SELECT
							SUM(monAmount) monAmtPaid
						FROM 
							BillPaymentDetails BPD
						INNER JOIN
							BillPaymentHeader BPH
						ON
							BPD.numBillPaymentID=BPH.numBillPaymentID
						WHERE
							BPD.numBillID=BH.numBillID
							AND CAST(BPH.dtPaymentDate AS DATE) <= ''' + CAST(@dtToDate AS VARCHAr) + '''
					) TablePayment
					WHERE   
							BH.numDomainID = '+ CONVERT(VARCHAR(15),@numDomainId) +'
							AND BH.numDivisionId = '+ CONVERT(VARCHAR(15),@numDivisionID) +'
							AND ISNULL(BH.monAmountDue, 0) - ISNULL(TablePayment.monAmtPaid, 0) > 0 
							AND (BH.numAccountClass= ' + CAST(@numAccountClass AS VARCHAR) + ' OR ' + CAST(@numAccountClass AS VARCHAR) + '=0)
							AND CONVERT(DATE,BH.[dtBillDate]) <= ''' + CAST(@dtToDate AS VARCHAr) + ''''
			 
		SET @strSqlCheck=' UNION
              SELECT  
						   0 AS numOppId,
						   ''Check'' vcPOppName,
						   -3 AS [tintOppType],-- flag for Checks
						   0 numOppBizDocsId,
						   '''' AS [vcBizDocID],
						   ISNULL(CD.monAmount, 0) as TotalAmount,
						   ISNULL(CD.monAmount, 0) as AmountPaid,
					       0 as BalanceDue,
						   [dbo].[FormatedDateFromDate](CH.dtCheckDate,CH.numDomainID) AS DueDate,
						   0 bitBillingTerms,
						   0 intBillingDays,
						   CH.dtCreatedDate dtCreatedDate,
						   '''' varCurrSymbol,
						   1 AS fltExchangeRate,
						   CH.dtCheckDate AS dtFromDate,
						   0 as numBizDocsPaymentDetId,
						   1 AS CurrencyCount,0 as numComissionID,'''' as vcRefOrderNo,0 AS numBillID,CH.numCheckHeaderID
					FROM   CheckHeader CH
						JOIN  dbo.CheckDetails CD ON CH.numCheckHeaderID=CD.numCheckHeaderID
                        INNER JOIN dbo.Chart_Of_Accounts COA ON COA.numAccountId = CD.numChartAcntId
					WHERE CH.numDomainID = '+ CONVERT(VARCHAR(15),@numDomainId) +' AND CH.tintReferenceType=1
						AND COA.vcAccountCode LIKE ''01020102%''
						AND (CH.numAccountClass= ' + CAST(@numAccountClass AS VARCHAR) + ' OR ' + CAST(@numAccountClass AS VARCHAR) + '=0)
						AND CH.[dtCheckDate] <= ''' + CAST(@dtToDate AS VARCHAr) + '''
                        AND CD.numCustomerId = '+ CONVERT(VARCHAR(15),@numDivisionID)
			SET @strSqlOrder=' UNION 
				SELECT 
               OM.[numOppId],
               OM.[vcPOppName],
               OM.[tintOppType],
               OB.[numOppBizDocsId],
               OB.[vcBizDocID],
               isnull(OB.monDealAmount * OM.fltExchangeRate,
                      0) TotalAmount,--OM.[monPAmount]
               (ISNULL(TablePayment.monPaidAmount,0) * OM.fltExchangeRate) AmountPaid,
               isnull(OB.monDealAmount * OM.fltExchangeRate
                        - ISNULL(TablePayment.monPaidAmount,0) * OM.fltExchangeRate,0)  BalanceDue,
               CASE ISNULL(OM.bitBillingTerms,0) 
                 --WHEN 1 THEN [dbo].[FormatedDateFromDate](DATEADD(dd,convert(int,ISNULL(dbo.fn_GetListItemName(isnull(OM.intBillingDays,0)),0)),OB.dtFromDate),
                 WHEN 1 THEN [dbo].[FormatedDateFromDate](DATEADD(dd,convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(OM.intBillingDays,0)), 0)),OB.dtFromDate),
                                                          '+ CONVERT(VARCHAR(15),@numDomainId) +')
                 WHEN 0 THEN [dbo].[FormatedDateFromDate](ob.[dtFromDate],'+ CONVERT(VARCHAR(15),@numDomainId) +')
               END AS DueDate,
               OM.bitBillingTerms,
               OM.intBillingDays,
               OB.dtCreatedDate,
               ISNULL(C.[varCurrSymbol],'''') varCurrSymbol,
               OM.fltExchangeRate,
               OB.dtFromDate,
			   0 AS numBizDocsPaymentDetId,
			   CASE WHEN '+ CONVERT(VARCHAR(15),@baseCurrency) +' <> OM.numCurrencyID THEN 2 ELSE 1 END AS CurrencyCount,0 as numComissionID,OB.vcRefOrderNo,0 as numBillID,0 AS numCheckHeaderID
        FROM   [OpportunityMaster] OM
               INNER JOIN [DivisionMaster] DM
                 ON OM.[numDivisionId] = DM.[numDivisionID]
               LEFT OUTER JOIN OpportunityBizDocs OB
                 ON OB.numOppId = OM.numOppID
               LEFT OUTER JOIN [Currency] C
                 ON OM.[numCurrencyID] = C.[numCurrencyID]
				 OUTER APPLY
				(
					SELECT
						SUm(BPD.monAmount) monPaidAmount
					FROM	
						BillPaymentDetails BPD
					INNER JOIN
						BillPaymentHeader BPH
					ON
						BPD.numBillPaymentID=BPH.numBillPaymentID
					WHERE
						BPD.numOppBizDocsID = OB.numOppBizDocsId
						AND CAST(BPH.dtPaymentDate AS DATE) <= ''' + CAST(@dtToDate AS VARCHAr) + '''
				) TablePayment
        WHERE  OM.[tintOppType] = 2
               AND OM.[tintOppStatus] = 1
               AND om.[numDomainId] = '+ CONVERT(VARCHAR(15),@numDomainId) +'
               AND OB.[numBizDocId] = '+ CONVERT(VARCHAR(15),ISNULL(@AuthoritativePurchaseBizDocId,0)) +' 
               AND DM.[numDivisionID] = '+ CONVERT(VARCHAR(15),@numDivisionID) +'
               AND OB.bitAuthoritativeBizDocs=1
               AND ISNULL(OB.tintDeferred,0) <> 1
			   AND (OM.numAccountClass= ' + CAST(@numAccountClass AS VARCHAR) + ' OR ' + CAST(@numAccountClass AS VARCHAR) + '=0)
			   AND CONVERT(DATE,OB.[dtCreatedDate]) <= ''' + CAST(@dtToDate AS VARCHAr) + '''
               AND  isnull(OB.monDealAmount * OM.fltExchangeRate,0) > 0 
			   AND isnull(OB.monDealAmount * OM.fltExchangeRate
                        - ISNULL(TablePayment.monPaidAmount,0) * OM.fltExchangeRate,0) > 0 '
		
		SET @strSqlCommission=' UNION 
		 SELECT  OM.[numOppId],
               OM.[vcPOppName],
               OM.[tintOppType],
               OB.[numOppBizDocsId],
               OB.[vcBizDocID],
               BDC.numComissionAmount,
               0 AmountPaid,
               BDC.numComissionAmount BalanceDue,
               CASE ISNULL(OM.bitBillingTerms,0) 
                 --WHEN 1 THEN [dbo].[FormatedDateFromDate](DATEADD(dd,convert(int,ISNULL(dbo.fn_GetListItemName(isnull(OM.intBillingDays,0)),0)),OB.dtFromDate),
                 WHEN 1 THEN [dbo].[FormatedDateFromDate](DATEADD(dd,convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(OM.intBillingDays,0)), 0)),OB.dtFromDate),
                                                          '+ CONVERT(VARCHAR(15),@numDomainId) +')
                 WHEN 0 THEN [dbo].[FormatedDateFromDate](ob.[dtFromDate],'+ CONVERT(VARCHAR(15),@numDomainId) +')
               END AS DueDate,
               OM.bitBillingTerms,
               OM.intBillingDays,
               OB.dtCreatedDate,
               ISNULL(C.[varCurrSymbol],'''') varCurrSymbol,
               OM.fltExchangeRate,
               OB.dtFromDate,
			   0 AS numBizDocsPaymentDetId,
			   CASE WHEN '+ CONVERT(VARCHAR(15),@baseCurrency) +' <> OM.numCurrencyID THEN 2 ELSE 1 END AS CurrencyCount,BDC.numComissionID,OB.vcRefOrderNo,0 as numBillID,0 AS numCheckHeaderID
        FROM    BizDocComission BDC JOIN  OpportunityBizDocs OB
                ON BDC.numOppBizDocId = OB.numOppBizDocsId
				join OpportunityMaster OM on OM.numOppId=OB.numOppId
				JOIN Domain D on D.numDomainID=BDC.numDomainID
				  LEFT OUTER JOIN [Currency] C
                 ON OM.[numCurrencyID] = C.[numCurrencyID]
				LEFT JOIN OpportunityBizDocsDetails OBDD ON BDC.numBizDocsPaymentDetId=isnull(OBDD.numBizDocsPaymentDetId,0)
        WHERE   OM.numDomainID = '+ CONVERT(VARCHAR(15),@numDomainId) +' and 
				 BDC.numDomainID = '+ CONVERT(VARCHAR(15),@numDomainId) +' 
				AND ISNULL(BDC.bitCommisionPaid,0)=0
				AND OB.bitAuthoritativeBizDocs = 1
				AND isnull(OBDD.bitIntegratedToAcnt,0) = 0
				AND (OM.numAccountClass= ' + CAST(@numAccountClass AS VARCHAR) + ' OR ' + CAST(@numAccountClass AS VARCHAR) + '=0)
				AND OB.[dtCreatedDate] <= ''' + CAST(@dtToDate AS VARCHAr) + '''
                AND D.numDivisionId = ' + CONVERT(VARCHAR(15),@numDivisionID) 		
                
    --Show Impact of AR journal entries in report as well 
    	
SET @strSqlJournal = '    	
		UNION 
		 SELECT 
				GJH.numJournal_Id [numOppId],
				''Journal'' [vcPOppName],
				-1 [tintOppType],
				0 [numOppBizDocsId],
				'''' [vcBizDocID],
				CASE WHEN GJD.numDebitAmt > 0 THEN GJD.numDebitAmt ELSE -1*GJD.numCreditAmt END TotalAmount,
				0 AmountPaid,
				1 BalanceDue,
				[dbo].[FormatedDateFromDate](GJH.datEntry_Date,'+ CONVERT(VARCHAR(15),@numDomainId) +') AS DueDate,
				0 bitBillingTerms,
				0 intBillingDays,
				GJH.datCreatedDate,
				ISNULL(C.[varCurrSymbol], '''') [varCurrSymbol],
				1 fltExchangeRate,
				GJH.datEntry_Date AS dtFromDate,
				0 numBizDocsPaymentDetId,
				1 AS CurrencyCount
				,0 as numComissionID
				,'''' as vcRefOrderNo
				,0 as numBillID
				,0 AS numCheckHeaderID
		 FROM   dbo.General_Journal_Header GJH
				INNER JOIN dbo.General_Journal_Details GJD ON GJH.numJournal_Id = GJD.numJournalId
															  AND GJH.numDomainId = GJD.numDomainId
				INNER JOIN dbo.Chart_Of_Accounts COA ON COA.numAccountId = GJD.numChartAcntId
				LEFT OUTER JOIN [Currency] C ON GJD.[numCurrencyID] = C.[numCurrencyID]
		 WHERE  GJH.numDomainId = '+ CONVERT(VARCHAR(15),@numDomainId) +'
				and GJD.numCustomerID =' + CONVERT(VARCHAR(15),@numDivisionID) +'
				AND COA.vcAccountCode LIKE ''01020102%''
				AND ISNULL(numOppId,0)=0 AND ISNULL(numOppBizDocsId,0)=0 
				AND ISNULL(GJH.numBillID,0)=0 AND ISNULL(GJH.numBillPaymentID,0)=0 AND ISNULL(GJH.numCheckHeaderID,0)=0 AND ISNULL(GJH.numReturnID,0)=0
				AND GJH.numJournal_Id NOT IN (
				--Exclude Bill and Bill payment entries
				SELECT GH.numJournal_Id FROM dbo.General_Journal_Header GH INNER JOIN dbo.OpportunityBizDocsDetails OBD ON GH.numBizDocsPaymentDetId = OBD.numBizDocsPaymentDetId 
				WHERE GH.numDomainId='+ CONVERT(VARCHAR(15),@numDomainId) +' and OBD.tintPaymentType IN (2,4,6) and GH.numBizDocsPaymentDetId>0
				) AND ISNULL(GJH.numBillID,0)=0 AND ISNULL(GJH.numBillPaymentID,0)=0 AND ISNULL(GJH.numCheckHeaderID,0)=0
				AND (GJH.numAccountClass= ' + CAST(@numAccountClass AS VARCHAR) + ' OR ' + CAST(@numAccountClass AS VARCHAR) + '=0)
				AND [GJH].[datEntry_Date] <= ''' + CAST(@dtToDate AS VARCHAr) + '''
				'

				SET @strReturn = ' UNION ALL
			 SELECT 
				0 AS numOppId,
				''Return'' vcPOppName,
				-1 AS [tintOppType],
				0 numOppBizDocsId,
				'''' AS [vcBizDocID],
				(CASE WHEN GJD.numDebitAmt > 0 THEN GJD.numDebitAmt * -1 ELSE GJD.numCreditAmt END) as TotalAmount,
				0 as AmountPaid,
				CASE WHEN GJD.numDebitAmt > 0 THEN GJD.numDebitAmt * -1 ELSE GJD.numCreditAmt END as BalanceDue,
				[dbo].[FormatedDateFromDate](GJH.datEntry_Date,RH.numDomainID) AS DueDate,
				0 bitBillingTerms,
				0 intBillingDays,
				RH.dtCreatedDate dtCreatedDate,
				'''' varCurrSymbol,
				1 AS fltExchangeRate,
				GJH.datEntry_Date AS dtFromDate,
				0 as numBizDocsPaymentDetId
				,GJD.numCurrencyID
				,0 as numComissionID
				,'''' as vcRefOrderNo
				,0
				,0 AS numCheckHeaderID  
			 FROM   dbo.ReturnHeader RH JOIN dbo.General_Journal_Header GJH ON RH.numReturnHeaderID = GJH.numReturnID
			JOIN General_Journal_Details GJD ON GJH.numJournal_Id = GJD.numJournalId 
			INNER JOIN dbo.Chart_Of_Accounts COA ON COA.numAccountId = GJD.numChartAcntId
			INNER JOIN [DivisionMaster] DM ON RH.[numDivisionId] = DM.[numDivisionID]
			INNER JOIN CompanyInfo CO ON CO.numCompanyId = DM.numCompanyID
			LEFT JOIN AdditionalContactsInformation ADC ON ADC.numDivisionId=DM.numDivisionID AND ADC.numContactId=RH.numContactId
			WHERE RH.tintReturnType=4 AND RH.numDomainId='+ CONVERT(VARCHAR(15),@numDomainId) +'  AND COA.vcAccountCode LIKE ''01020102%''
					AND (monBizDocAmount > 0) AND ISNULL(RH.numBillPaymentIDRef,0)>0
					AND (Rh.numAccountClass= ' + CAST(@numAccountClass AS VARCHAR) + ' OR ' + CAST(@numAccountClass AS VARCHAR) + '=0)
					AND GJH.datEntry_Date <= ''' + CAST(@dtToDate AS VARCHAr) + ''''
   
   SET @strSqlCredit = CONCAT(' UNION 
							SELECT 
								BPH.numBillPaymentID AS numOppId,
								''Credit'' vcPOppName,
								-1 AS [tintOppType],
								0 numOppBizDocsId,
								'''' AS [vcBizDocID],
								ISNULL(monPaymentAmount,0) as TotalAmount,
								ISNULL(monAppliedAmount,0) as AmountPaid,
								ISNULL(monPaymentAmount,0) - ISNULL(monAppliedAmount,0) as BalanceDue,
								[dbo].[FormatedDateFromDate]([BPH].[dtPaymentDate],BPH.numDomainID) AS DueDate,
								0 bitBillingTerms,
								0 intBillingDays,
								BPH.dtCreateDate dtCreatedDate,
								'''' varCurrSymbol,
								1 AS fltExchangeRate,
								BPH.dtCreateDate AS dtFromDate,
								0 as numBizDocsPaymentDetId,
								1 AS CurrencyCount
								,0 as numComissionID
								,'''' as vcRefOrderNo
								,0
								,0 AS numCheckHeaderID  
							FROM 
								BillPaymentHeader BPH
							WHERE 
								BPH.numDomainId=',@numDomainId,'
								AND (BPH.numDivisionId =', @numDivisionId,' OR ',@numDivisionId,' IS NULL)
								AND (ISNULL(monPaymentAmount,0) - ISNULL(monAppliedAmount,0)) > 0
								AND (BPH.numAccountClass=',@numAccountClass,' OR ',@numAccountClass,'=0)
								AND [BPH].[dtPaymentDate] <= ''',@dtToDate,'''') 

	
    
    
    IF (@vcFlag = '0+30')
      BEGIN
			SET @strSqlBill =@strSqlBill +' AND BH.dtDueDate BETWEEN dbo.GetUTCDateWithoutTime() AND dateadd(DAY,31,dbo.GetUTCDateWithoutTime()) '
                                               
            SET @strSqlCheck =@strSqlCheck +' AND CH.dtCheckDate BETWEEN dbo.GetUTCDateWithoutTime() AND dateadd(DAY,31,dbo.GetUTCDateWithoutTime()) '
                                               
            SET @strSqlOrder =@strSqlOrder +' AND 
               dateadd(DAY,CASE 
                                 --WHEN bitBillingTerms = 1 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))
                                 WHEN bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(intBillingDays,0)), 0))
                                 ELSE 0
                               END,dtFromDate) BETWEEN dbo.GetUTCDateWithoutTime() AND dateadd(DAY,31,dbo.GetUTCDateWithoutTime())
               AND dbo.GetUTCDateWithoutTime() <= dateadd(DAY,CASE 
                                                 --WHEN bitBillingTerms = 1 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))
                                                 WHEN bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(intBillingDays,0)), 0))
                                                 ELSE 0
                                               END,dtFromDate) '
                                                                                                                     
			SET @strSqlCommission =@strSqlCommission +' AND 
               dateadd(DAY,CASE 
                                 --WHEN bitBillingTerms = 1 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))
                                 WHEN bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(intBillingDays,0)), 0))
                                 ELSE 0
                               END,dtFromDate) BETWEEN dbo.GetUTCDateWithoutTime() AND dateadd(DAY,31,dbo.GetUTCDateWithoutTime())
               AND dbo.GetUTCDateWithoutTime() <= dateadd(DAY,CASE 
                                                 --WHEN bitBillingTerms = 1 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))
                                                 WHEN bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(intBillingDays,0)), 0))
                                                 ELSE 0
                                               END,dtFromDate) '
                                               
          SET @strSqlJournal =@strSqlJournal +' AND GJH.datEntry_Date BETWEEN dbo.GetUTCDateWithoutTime() AND dateadd(DAY,31,dbo.GetUTCDateWithoutTime())' 
      END
    ELSE
      IF (@vcFlag = '30+60')
        BEGIN
			SET @strSqlBill =@strSqlBill +' AND BH.dtDueDate BETWEEN dateadd(DAY,31,dbo.GetUTCDateWithoutTime()) AND dateadd(DAY,61,dbo.GetUTCDateWithoutTime())'
                                               
            SET @strSqlCheck =@strSqlCheck +' AND CH.dtCheckDate BETWEEN dateadd(DAY,31,dbo.GetUTCDateWithoutTime()) AND dateadd(DAY,61,dbo.GetUTCDateWithoutTime())'
            
            SET @strSqlOrder =@strSqlOrder +' AND  
                 dateadd(DAY,CASE 
                                   --WHEN bitBillingTerms = 1 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))
                                   WHEN bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(intBillingDays,0)), 0))
                                   ELSE 0
                                 END,dtFromDate) BETWEEN dateadd(DAY,31,dbo.GetUTCDateWithoutTime()) AND dateadd(DAY,61,dbo.GetUTCDateWithoutTime())
                 AND dbo.GetUTCDateWithoutTime() <= dateadd(DAY,CASE 
                                                   --WHEN bitBillingTerms = 1 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))
                                                   WHEN bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(intBillingDays,0)), 0))
                                                   ELSE 0
                                                 END,dtFromDate)'
                                                                          
			SET @strSqlCommission =@strSqlCommission +' AND  
                 dateadd(DAY,CASE 
                                   --WHEN bitBillingTerms = 1 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))
                                   WHEN bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(intBillingDays,0)), 0))
                                   ELSE 0
                                 END,dtFromDate) BETWEEN dateadd(DAY,31,dbo.GetUTCDateWithoutTime()) AND dateadd(DAY,61,dbo.GetUTCDateWithoutTime())
                 AND dbo.GetUTCDateWithoutTime() <= dateadd(DAY,CASE 
                                                   --WHEN bitBillingTerms = 1 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))
                                                   WHEN bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(intBillingDays,0)), 0))
                                                   ELSE 0
                                                 END,dtFromDate)'
                                                 
           SET @strSqlJournal =@strSqlJournal +' AND GJH.datEntry_Date BETWEEN dateadd(DAY,31,dbo.GetUTCDateWithoutTime()) AND dateadd(DAY,61,dbo.GetUTCDateWithoutTime())'
        END
      ELSE
        IF (@vcFlag = '60+90')
          BEGIN
			SET @strSqlBill =@strSqlBill +' AND BH.dtDueDate BETWEEN dateadd(DAY,61,dbo.GetUTCDateWithoutTime()) AND dateadd(DAY,91,dbo.GetUTCDateWithoutTime())'
                                               
            SET @strSqlCheck =@strSqlCheck +' AND CH.dtCheckDate BETWEEN dateadd(DAY,61,dbo.GetUTCDateWithoutTime()) AND dateadd(DAY,91,dbo.GetUTCDateWithoutTime())'
               
            SET @strSqlOrder =@strSqlOrder +' AND  
                   dateadd(DAY,CASE 
                                     --WHEN bitBillingTerms = 1 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))
                                     WHEN bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(intBillingDays,0)), 0))
                                     ELSE 0
                                   END,dtFromDate) BETWEEN dateadd(DAY,61,dbo.GetUTCDateWithoutTime()) AND dateadd(DAY,91,dbo.GetUTCDateWithoutTime())
                   AND dbo.GetUTCDateWithoutTime() <= dateadd(DAY,CASE 
                                                     --WHEN bitBillingTerms = 1 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))
                                                     WHEN bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(intBillingDays,0)), 0))
                                                     ELSE 0
                                                   END,dtFromDate) '
                                                                            
			SET @strSqlCommission =@strSqlCommission +' AND  
                   dateadd(DAY,CASE 
                                     --WHEN bitBillingTerms = 1 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))
                                     WHEN bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(intBillingDays,0)), 0))
                                     ELSE 0
                                   END,dtFromDate) BETWEEN dateadd(DAY,61,dbo.GetUTCDateWithoutTime()) AND dateadd(DAY,91,dbo.GetUTCDateWithoutTime())
                   AND dbo.GetUTCDateWithoutTime() <= dateadd(DAY,CASE 
                                                     --WHEN bitBillingTerms = 1 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))
                                                     WHEN bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(intBillingDays,0)), 0))
                                                     ELSE 0
                                                   END,dtFromDate) '
                                                   
			SET @strSqlJournal =@strSqlJournal +' AND GJH.datEntry_Date BETWEEN dateadd(DAY,61,dbo.GetUTCDateWithoutTime()) AND dateadd(DAY,91,dbo.GetUTCDateWithoutTime())'
          END
        ELSE
          IF (@vcFlag = '90+')
            BEGIN
            SET @strSqlBill =@strSqlBill +' AND BH.dtDueDate > dateadd(DAY,91,dbo.GetUTCDateWithoutTime())'
                                               
            SET @strSqlCheck =@strSqlCheck +' AND CH.dtCheckDate > dateadd(DAY,91,dbo.GetUTCDateWithoutTime())'

			SET @strSqlOrder =@strSqlOrder +' AND  
                     dateadd(DAY,CASE 
                                       --WHEN bitBillingTerms = 1 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))
                                       WHEN bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(intBillingDays,0)), 0))
                                       ELSE 0
                                     END,dtFromDate) > dateadd(DAY,91,dbo.GetUTCDateWithoutTime())
                     AND dbo.GetUTCDateWithoutTime() <= dateadd(DAY,CASE 
                                                       --WHEN bitBillingTerms = 1 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))
                                                       WHEN bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(intBillingDays,0)), 0))
                                                       ELSE 0
                                                     END,dtFromDate)'                                     
                                                     
            SET @strSqlCommission =@strSqlCommission +' AND  
                     dateadd(DAY,CASE 
                                       --WHEN bitBillingTerms = 1 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))
                                       WHEN bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(intBillingDays,0)), 0))
                                       ELSE 0
                                     END,dtFromDate) > dateadd(DAY,91,dbo.GetUTCDateWithoutTime())
                     AND dbo.GetUTCDateWithoutTime() <= dateadd(DAY,CASE 
                                                       --WHEN bitBillingTerms = 1 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))
                                                       WHEN bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(intBillingDays,0)), 0))
                                                       ELSE 0
                                                     END,dtFromDate)'
                                                     
			SET @strSqlJournal =@strSqlJournal +' AND GJH.datEntry_Date > dateadd(DAY,91,dbo.GetUTCDateWithoutTime())'
            END
          ELSE
            IF (@vcFlag = '0-30')
              BEGIN
              SET @strSqlBill =@strSqlBill +' AND datediff(DAY,BH.dtDueDate,dbo.GetUTCDateWithoutTime()) BETWEEN 0 AND 30'
                                               
            SET @strSqlCheck =@strSqlCheck +' AND datediff(DAY,CH.dtCheckDate ,dbo.GetUTCDateWithoutTime()) BETWEEN 0 AND 30'
                    
                    SET @strSqlOrder =@strSqlOrder +' AND  
                       dbo.GetUTCDateWithoutTime() > dateadd(DAY,CASE 
                                                        --WHEN bitBillingTerms = 1 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))
                                                        WHEN bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(intBillingDays,0)), 0))
                                                        ELSE 0
                                                      END,dtFromDate)
                       AND datediff(DAY,dateadd(DAY,CASE 
                                                      --WHEN bitBillingTerms = 1 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))
                                                      WHEN bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(intBillingDays,0)), 0))
                                                      ELSE 0
                                                    END,dtFromDate),dbo.GetUTCDateWithoutTime()) BETWEEN 0 AND 30'
                                                    
                                                                     
					SET @strSqlCommission =@strSqlCommission +' AND  
                       dbo.GetUTCDateWithoutTime() > dateadd(DAY,CASE 
                                                        --WHEN bitBillingTerms = 1 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))
                                                        WHEN bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(intBillingDays,0)), 0))
                                                        ELSE 0
                                                      END,dtFromDate)
                       AND datediff(DAY,dateadd(DAY,CASE 
                                                      --WHEN bitBillingTerms = 1 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))
                                                      WHEN bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(intBillingDays,0)), 0))
                                                      ELSE 0
                                                    END,dtFromDate),dbo.GetUTCDateWithoutTime()) BETWEEN 0 AND 30'
                                                    
				SET @strSqlJournal =@strSqlJournal +' AND datediff(DAY,GJH.datEntry_Date,dbo.GetUTCDateWithoutTime()) BETWEEN 0 AND 30 '
              END
            ELSE
              IF (@vcFlag = '30-60')
                BEGIN
                SET @strSqlBill =@strSqlBill +' AND datediff(DAY,BH.dtDueDate,dbo.GetUTCDateWithoutTime()) BETWEEN 31 AND 60'
                                               
				SET @strSqlCheck =@strSqlCheck +' AND datediff(DAY,CH.dtCheckDate,dbo.GetUTCDateWithoutTime()) BETWEEN 31 AND 60'
                
                SET @strSqlOrder =@strSqlOrder +' AND  
                         dbo.GetUTCDateWithoutTime() > dateadd(DAY,CASE 
                                                          --WHEN bitBillingTerms = 1 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))
                                                          WHEN bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(intBillingDays,0)), 0))
                                                          ELSE 0
                                                        END,dtFromDate)
                         AND datediff(DAY,dateadd(DAY,CASE 
                                                        --WHEN bitBillingTerms = 1 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))
                                                        WHEN bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(intBillingDays,0)), 0))
                                                        ELSE 0
                                                      END,dtFromDate),dbo.GetUTCDateWithoutTime()) BETWEEN 31 AND 60'
                                                                           
						SET @strSqlCommission =@strSqlCommission +' AND  
                         dbo.GetUTCDateWithoutTime() > dateadd(DAY,CASE 
                                                          --WHEN bitBillingTerms = 1 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))
                                                          WHEN bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(intBillingDays,0)), 0))
                                                          ELSE 0
                                                        END,dtFromDate)
                         AND datediff(DAY,dateadd(DAY,CASE 
                                                        --WHEN bitBillingTerms = 1 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))
                                                        WHEN bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(intBillingDays,0)), 0))
                                                        ELSE 0
                                                      END,dtFromDate),dbo.GetUTCDateWithoutTime()) BETWEEN 31 AND 60'
                                                      
					SET @strSqlJournal =@strSqlJournal +' AND datediff(DAY,GJH.datEntry_Date,dbo.GetUTCDateWithoutTime()) BETWEEN 31 AND 60 '
                END
              ELSE
                IF (@vcFlag = '60-90')
                  BEGIN
                  SET @strSqlBill =@strSqlBill +' AND datediff(DAY,BH.dtDueDate,dbo.GetUTCDateWithoutTime()) BETWEEN 61 AND 90 '
                                               
	              SET @strSqlCheck =@strSqlCheck +' AND datediff(DAY,CH.dtCheckDate,dbo.GetUTCDateWithoutTime()) BETWEEN 61 AND 90 '
                        
                        SET @strSqlOrder =@strSqlOrder +' AND  
                           dbo.GetUTCDateWithoutTime() > dateadd(DAY,CASE 
                                                            --WHEN bitBillingTerms = 1 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))
                                                            WHEN bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(intBillingDays,0)), 0))
                                                            ELSE 0
                                                          END,dtFromDate)
                           AND datediff(DAY,dateadd(DAY,CASE 
                                                          --WHEN bitBillingTerms = 1 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))
                                                          WHEN bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(intBillingDays,0)), 0))
                                                          ELSE 0
                                                        END,dtFromDate),dbo.GetUTCDateWithoutTime()) BETWEEN 61 AND 90'
                                                                     
						SET @strSqlCommission =@strSqlCommission +' AND  
                           dbo.GetUTCDateWithoutTime() > dateadd(DAY,CASE 
                                                            --WHEN bitBillingTerms = 1 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))
                                                            WHEN bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(intBillingDays,0)), 0))
                                                            ELSE 0
                                                          END,dtFromDate)
                           AND datediff(DAY,dateadd(DAY,CASE 
                                                          --WHEN bitBillingTerms = 1 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))
                                                          WHEN bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(intBillingDays,0)), 0))
                                                          ELSE 0
                                                        END,dtFromDate),dbo.GetUTCDateWithoutTime()) BETWEEN 61 AND 90'
                                                        
					SET @strSqlJournal =@strSqlJournal +' AND datediff(DAY,GJH.datEntry_Date,dbo.GetUTCDateWithoutTime()) BETWEEN 61 AND 90 '
                  END
                ELSE
                  IF (@vcFlag = '90-')
                    BEGIN
                    SET @strSqlBill =@strSqlBill +' AND datediff(DAY,BH.dtDueDate,dbo.GetUTCDateWithoutTime()) > 90 '
                                               
            SET @strSqlCheck =@strSqlCheck +' AND datediff(DAY,CH.dtCheckDate,dbo.GetUTCDateWithoutTime()) > 90 '
                             
                         SET @strSqlOrder =@strSqlOrder +' AND  
                             dbo.GetUTCDateWithoutTime() > dateadd(DAY,CASE 
                                                              --WHEN bitBillingTerms = 1 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))
                                                              WHEN bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(intBillingDays,0)), 0))
                                                              ELSE 0
                                                            END,dtFromDate)
                             AND datediff(DAY,dateadd(DAY,CASE 
                                                            --WHEN bitBillingTerms = 1 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))
                                                            WHEN bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(intBillingDays,0)), 0))
                                                            ELSE 0
                                                          END,dtFromDate),dbo.GetUTCDateWithoutTime()) > 90'
                                                                  
						SET @strSqlCommission =@strSqlCommission +' AND  
                             dbo.GetUTCDateWithoutTime() > dateadd(DAY,CASE 
                                                              --WHEN bitBillingTerms = 1 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))
                                                              WHEN bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(intBillingDays,0)), 0))
                                                              ELSE 0
                                                            END,dtFromDate)
                             AND datediff(DAY,dateadd(DAY,CASE 
                                                            --WHEN bitBillingTerms = 1 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))
                                                            WHEN bitBillingTerms = 1 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(intBillingDays,0)), 0))
                                                            ELSE 0
                                                          END,dtFromDate),dbo.GetUTCDateWithoutTime()) > 90'
                                                          
						SET @strSqlJournal =@strSqlJournal +' AND datediff(DAY,GJH.datEntry_Date,dbo.GetUTCDateWithoutTime()) > 90 '
                    END
                  ELSE
                    BEGIN
						SET @strSqlBill =@strSqlBill +''
						SET @strSqlCheck =@strSqlCheck +''
						SET @strSqlOrder =@strSqlOrder +''
						SET @strSqlCommission =@strSqlCommission +''
						SET @strSqlJournal =@strSqlJournal +''
                    END
                    
                    
	PRINT (@strSqlBill)
	PRINT (@strSqlCheck)
	PRINT (@strSqlOrder)
	PRINT (@strSqlCommission)
	PRINT (@strSqlJournal)
	PRINT (@strSqlCredit)
	PRINT (@strReturn)
	
	EXEC(@strSqlBill + @strSqlCheck + @strSqlOrder + @strSqlCommission + @strSqlJournal + @strSqlCredit + @strReturn)
  END
