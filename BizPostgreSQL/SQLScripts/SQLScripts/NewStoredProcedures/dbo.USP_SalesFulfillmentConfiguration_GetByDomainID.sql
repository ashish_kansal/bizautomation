SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_SalesFulfillmentConfiguration_GetByDomainID')
DROP PROCEDURE USP_SalesFulfillmentConfiguration_GetByDomainID
GO
CREATE PROCEDURE [dbo].[USP_SalesFulfillmentConfiguration_GetByDomainID]  
	@numDomainID NUMERIC(18,0)
AS
BEGIN
	SELECT * FROM SalesFulfillmentConfiguration WHERE numDomainID=@numDomainID
END