SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetReviews')
DROP PROCEDURE USP_GetReviews
GO
CREATE PROCEDURE [dbo].[USP_GetReviews]   

@numReviewId AS NUMERIC(18,0) =0, 
@numDomainId AS NUMERIC(18,0) ,
@numSiteId AS NUMERIC(18,0) ,      
@vcReferenceId AS varchar(100),
@tintTypeId AS INT ,
@Mode AS INT ,
@numContactId AS NUMERIC(18,0) ,
@SearchMode AS INT = 0

AS

BEGIN
	IF @Mode = 1
	  BEGIN
			 DECLARE @strSql as varchar(8000)
	         SET @strSql =  
	         'SELECT 
	         * ,
	        (SELECT COUNT(*) FROM ReviewDetail WHERE numReviewId = R.numReviewId  AND bitHelpful = 1) AS vcHelpfulVote ,
	        (SELECT COUNT(*) FROM ReviewDetail WHERE numReviewId = R.numReviewId  ) AS vcTotalVote ,
            (SELECT TOP 1 (vcFirstName + '' '' + vcLastName) FROM dbo.AdditionalContactsInformation WHERE numContactId = R.numContactId) AS vcContactName 
             FROM Review AS R  
             WHERE      numDomainId =' + CONVERT(VARCHAR(100) , @numDomainID)  + '   
                    AND numSiteId  =  ' + CONVERT(VARCHAR(100) , @numSiteId)  + '  
                    AND vcReferenceId = ' + CONVERT(VARCHAR(100) , @vcReferenceId )  + '   
                    AND tintTypeId = ' + CONVERT(VARCHAR(100) , @tintTypeId )  + ' 
                    AND bitHide = 0'
            IF (@SearchMode = 0)
				BEGIN
					set @strSql = @strSql + ' ORDER BY dtCreatedDate desc'    
				END
			ELSE
				BEGIN
					set @strSql = @strSql + 'ORDER BY vcHelpfulVote desc'    
				END	
			exec (@strSql)   	
      END
    ELSE IF @Mode = 2 --To view List on Admin side and show was this review helpful on client side
      BEGIN
	  	     SELECT 
	  	     numReviewId ,
	  	    (SELECT TOP 1 (vcFirstName + ' ' + vcFirstName) FROM dbo.AdditionalContactsInformation WHERE numContactId = R.numContactId) AS vcContactName ,
	  	      (CASE 
	  	         WHEN LEN(vcReviewTitle) > 25 
	  	         then SUBSTRING(vcReviewTitle ,0,25) + ' ' +  '[...]' 
	             ELSE
	              vcReviewTitle
	           END) AS vcReviewTitle  ,
	  	      (CASE 
	  	         WHEN LEN(vcReviewComment) > 25 
	  	         then SUBSTRING(vcReviewComment ,0,25) + ' ' +  '[...]' 
	             ELSE
	              vcReviewComment
	           END) AS vcReviewComment  ,
	  	     bitEmailMe ,
	  	     bitHide ,
	  	     vcIpaddress ,
	  	     (SELECT COUNT(*)  FROM ReviewDetail  WHERE numReviewId = R.numReviewId AND ISNULL(bitHelpful,0) <> 0) AS vcTotalHelpful  ,
	  	     (SELECT COUNT(*)  FROM ReviewDetail  WHERE numReviewId = R.numReviewId AND bitHelpful = 1) AS vcHelpful  ,
	  	     (SELECT COUNT(*)  FROM ReviewDetail  WHERE numReviewId = R.numReviewId AND bitAbuse = 1) AS vcTotalAbuse  ,
	  	     
	  	     dtCreatedDate
	  	     
	  	     FROM Review R WHERE 
	  	         numDomainId = @numDomainID   
	  	     AND numSiteId  = @numSiteId  
	  	     AND tintTypeId = @tintTypeId
	  	     
	  END  
	  ELSE IF @Mode = 3 --To view one record on admin side when Edit made 
	  BEGIN
	  	    SELECT * ,
	  	    (CASE 
	  	         WHEN tintTypeId = 1
	  	         then  'Category'
	  	         When tintTypeId = 2
	  	         THEN 'Product'
	  	         When tintTypeId = 3
	  	         THEN 'URL'  
	             ELSE
	              vcReviewComment
	           END) AS vcReviewType
	  	    FROM Review 
	  	    WHERE        numReviewId = @numReviewId  
	         	    AND  numDomainId = @numDomainID   
	         	    AND numSiteId  = @numSiteId
	         	    
	  END 
	  --Check where is mode 4 used
	  ELSE IF @Mode = 4
	  BEGIN
	  	SELECT * FROM Review 
	  	 WHERE numContactId = @numContactId AND numReviewId = @numReviewId
	  END
	  ELSE IF @Mode = 5
	  BEGIN
	  	SELECT COUNT(*) AS intReviewCount FROM Review 
	  	WHERE 
	  	         numDomainId = @numDomainID   
	  	     AND numSiteId  = @numSiteId  
	  	     AND tintTypeId = @tintTypeId
	  	     AND vcReferenceId = @vcReferenceId  
	  	     AND bitHide = 0
	  END
	  -- Mode 6 used in Permalink Page...Review get by ReviewId 
	  -- Mode 6 Usage :  And also used when was this review Helpful on cliend side clicked....
	  ELSE IF @Mode = 6
	  BEGIN
	  	SELECT 
	         * ,
	         (SELECT COUNT(*) FROM ReviewDetail WHERE numReviewId = R.numReviewId  AND bitHelpful = 1) AS vcHelpfulVote ,
	         (SELECT COUNT(*) FROM ReviewDetail WHERE numReviewId = R.numReviewId  ) AS vcTotalVote ,
            (SELECT TOP 1 (vcFirstName + ' ' + vcLastName) FROM dbo.AdditionalContactsInformation WHERE numContactId = R.numContactId) AS vcContactName 
             FROM Review AS R  
             WHERE      
                     bitHide = 0 AND 
	  	     numReviewId = @numReviewId 
	  END
END

-- select * from Review
--SELECT * FROM ReviewDetail
--INSERT INTO ReviewDetail
--VALUES(3,1,NULL,1)
--UPDATE ReviewDetail SET bitHelpful = 1 WHERE numReviewDetailId = 1
--SELECT * FROM ReviewDetail
--SELECT * FROM Ratings

