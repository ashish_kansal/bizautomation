GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_DeleteOpportunityItem_TimeExpense')
DROP PROCEDURE USP_DeleteOpportunityItem_TimeExpense
GO
CREATE PROCEDURE  USP_DeleteOpportunityItem_TimeExpense
	@numDomainId numeric(9),
    @numOppID numeric(9),
	@numoppitemtCode numeric(9)
AS 
    BEGIN


DECLARE @numCategoryHDRID NUMERIC(9)

SELECT @numCategoryHDRID=numCategoryHDRID
FROM OpportunityItems OPP 
Join TimeAndExpense TE on OPP.numOppId = TE.numOppId AND OPP.numoppitemtCode = TE.numOppItemID AND OPP.numProjectId=TE.numProId
where OPP.numOppID = @numOppID and OPP.numoppitemtCode = @numoppitemtCode

if @numCategoryHDRID>0
BEGIN
	exec USP_DeleteTimExpLeave @numCategoryHDRID, @numDomainID
END
END