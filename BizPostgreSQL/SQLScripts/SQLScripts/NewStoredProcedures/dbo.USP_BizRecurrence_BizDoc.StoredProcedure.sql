GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p' AND NAME ='USP_BizRecurrence_BizDoc')
DROP PROCEDURE USP_BizRecurrence_BizDoc
GO
  
Create PROCEDURE [dbo].[USP_BizRecurrence_BizDoc]  
	@numRecConfigID NUMERIC(18,0), 
	@numUserCntID NUMERIC(18,0),
	@numOppID NUMERIC(18,0),
	@numOppBizDocID NUMERIC(18,0),
	@numRecurBizDocID NUMERIC(18,0) OUT,
	@bitAuthorative BIT OUT,
	@bitBizDocRecur BIT,
	@numFrequency SMALLINT,
	@numRecurParentOppID NUMERIC(18,0),
	@Date AS DATE
AS  
BEGIN  
BEGIN TRANSACTION;

BEGIN TRY
	--DECLARE @Date AS DATE = GETDATE()
	DECLARE @numBizDocId AS INT
	DECLARE @numAuthBizDocId AS INT
	DECLARE @numDivisionID as numeric(9)
	DECLARE @numDomainID NUMERIC(9)
	DECLARE @tintOppType AS TINYINT	
	DECLARE @numNewOppBizDocID AS NUMERIC(18,0)
	DECLARE @fltExchangeRateBizDoc AS FLOAT
	DECLARE @numSequenceId AS NUMERIC(18,0)
	
	SELECT @fltExchangeRateBizDoc=fltExchangeRate FROM OpportunityMaster where numOppID=@numOppId
	SELECT @numDomainID=numDomainID,@numDivisionID=numDivisionID,@tintOppType=tintOppType FROM OpportunityMaster WHERE numOppID=@numOppId
	SELECT @numBizDocId = numBizDocId FROM OpportunityBizDocs WHERE numOppBizDocsId = @numOppBizDocID

	SELECT 
		@numSequenceId = ISNULL(MAX(CAST(ISNULL(OBD.numSequenceId,0) AS BigInt)),0) + 1 
	FROM 
		dbo.OpportunityMaster OM 
	JOIN 
		dbo.OpportunityBizDocs OBD 
	ON 
		OM.numOppId=OBD.numOppId
	WHERE 
		OM.numDomainId=@numDomainID AND OBD.numBizDocId=@numBizDocId

	--GET Authorative Biz Doc ID
	SELECT @numAuthBizDocId = CASE @tintOppType 
							WHEN 1 THEN [numAuthoritativeSales]
							WHEN 2 THEN [numAuthoritativePurchase]
							ELSE [numAuthoritativePurchase]
						  END
    FROM   
		[AuthoritativeBizDocs]
    WHERE  
		[numDomainId] =@numDomainId


	DECLARE @tintShipped AS TINYINT	
    DECLARE @dtShipped AS DATETIME
    DECLARE @bitAuthBizdoc AS BIT
        
	SELECT @tintShipped=ISNULL(tintShipped,0),@numDomainID=numDomainID FROM OpportunityMaster WHERE numOppId=@numOppId

	IF @tintShipped =1 
			SET @dtShipped = GETUTCDATE();
	ELSE
		SET @dtShipped = null;

	IF @tintOppType = 1 AND @numBizDocId = @numAuthBizDocId
		SET @bitAuthBizdoc = 1
	ELSE if @tintOppType = 2 AND @numBizDocId = @numAuthBizDocId
		SET @bitAuthBizdoc = 1
	ELSE 
		SET @bitAuthBizdoc = 0


	-- CREATE COPY OF PARENT BIZDOC
	INSERT INTO OpportunityBizDocs
    (
		numOppId,numBizDocId,numCreatedBy,dtCreatedDate,numModifiedBy,dtModifiedDate,vcComments,bitPartialFulfilment,
		dtFromDate,numBizDocStatus,[bitAuthoritativeBizDocs],numBizDocTempID,vcRefOrderNo,
		numSequenceId,numBizDocStatusOLD,bitAutoCreated,[numMasterBizdocSequenceID],fltExchangeRateBizDoc
	)
    SELECT 
		@numOppId,@numBizDocId,@numUserCntID,@Date,@numUserCntID,@Date,'',OBD.bitPartialFulfilment,
		DATETIMEFROMPARTS(YEAR(@Date),MONTH(@Date),DAY(@Date),23, 59, 59, 0),0,@bitAuthBizdoc,numBizDocTempID,vcRefOrderNo,
		@numSequenceId,0,1,@numSequenceId,@fltExchangeRateBizDoc
	FROM 
		OpportunityBizDocs OBD 
	WHERE   
		OBD.numOppBizDocsId=@numOppBizDocID

	SET @numNewOppBizDocID = SCOPE_IDENTITY()

	EXEC USP_OpportunityBizDocs_CT @numDomainID=@numDomainID,@numUserCntID=@numUserCntID,@numRecordID=@numNewOppBizDocID

	-- UPDATE BIZ DOC NAME 
	EXEC dbo.USP_UpdateBizDocNameTemplate @tintOppType,@numDomainID,@numNewOppBizDocID

	IF @numBizDocId = 297 OR @numBizDocId = 299
	BEGIN
		IF NOT EXISTS(SELECT * FROM dbo.NameTemplate WHERE numDomainID=@numDomainID AND numRecordID=@numBizDocId AND tintModuleID=2)
		BEGIN
			INSERT INTO NameTemplate
			(numDomainId,tintModuleID,numRecordID,vcNameTemplate,numSequenceId,numMinLength)
			SELECT 
				@numDomainId,2,@numBizDocId,UPPER(SUBSTRING(dbo.GetListIemName(@numBizDocId),0,4)) + '-',1,4

			SET @numSequenceId = 1
		END
		ELSE
		BEGIN
			SELECT @numSequenceId=numSequenceId FROM NameTemplate WHERE numDomainID=@numDomainID AND numRecordID=@numBizDocId AND tintModuleID=2
			UPDATE NameTemplate SET numSequenceId=ISNULL(numSequenceId,0) + 1 WHERE numDomainID=@numDomainID AND numRecordID=@numBizDocId AND tintModuleID=2
		END
	END

	UPDATE OpportunityBizDocs SET vcBizDocID=vcBizDocName + ISNULL(CAST(@numSequenceId AS VARCHAR(10)),''),monDealAmount=ISNULL(dbo.[GetDealAmount](@numOppId,@Date,@numNewOppBizDocID),0) WHERE  numOppBizDocsId = @numNewOppBizDocID

	IF @bitBizDocRecur = 1
	BEGIN
		-- Insert newly created bizdoc to transaction 
		INSERT INTO RecurrenceTransaction ([numRecConfigID],[numRecurrOppBizDocID],[dtCreatedDate])	VALUES (@numRecConfigID,@numNewOppBizDocID,GETDATE())

		-- Insert newly created bizdoc to transaction histrory table. Records are not deleted in this table when opp or bizdoc is deleted 
		INSERT INTO RecurrenceTransactionHistory ([numRecConfigID],[numRecurrOppBizDocID],[dtCreatedDate])	VALUES (@numRecConfigID,@numNewOppBizDocID,GETDATE())

		--Increase value of number of transaction completed by 1
		UPDATE RecurrenceConfiguration SET numTransaction = ISNULL(numTransaction,0) + 1 WHERE numRecConfigID = @numRecConfigID

		INSERT INTO OpportunityBizDocItems                                                                          
  		(
			numOppBizDocID,numOppItemID,numItemCode,numUnitHour,monPrice,monTotAmount,vcItemDesc,numWarehouseItmsID,vcType,
			vcAttributes,bitDropShip,bitDiscountType,fltDiscount,monTotAmtBefDiscount,vcNotes,bitEmbeddedCost,
			monEmbeddedCost,dtRentalStartDate,dtRentalReturnDate,tintTrackingStatus
		)
		SELECT 
			@numNewOppBizDocID,
			OBDI.numOppItemID,
			OBDI.numItemCode,
			(CASE 
			WHEN OBDI.numUnitHour > TEMP.QtytoFulFill THEN TEMP.QtytoFulFill
			ELSE OBDI.numUnitHour
			END) AS numUnitHour,
			OBDI.monPrice,
			(CASE 
			WHEN OBDI.numUnitHour > TEMP.QtytoFulFill THEN ((OI.monTotAmount / OI.numUnitHour) * TEMP.QtytoFulFill)
			ELSE OBDI.monTotAmount
			END) AS monTotAmount,
			OBDI.vcItemDesc,
			OBDI.numWarehouseItmsID,
			OBDI.vcType,
			OBDI.vcAttributes,
			OBDI.bitDropShip,
			OBDI.bitDiscountType,
			(CASE 
			WHEN OBDI.numUnitHour > TEMP.QtytoFulFill 
			THEN 
				(CASE 
				WHEN OBDI.bitDiscountType = 0 THEN OBDI.fltDiscount
				WHEN OBDI.bitDiscountType = 1 THEN ((OI.fltDiscount/OI.numUnitHour) * TEMP.QtytoFulFill)
				END)
			ELSE OBDI.fltDiscount
			END) As fltDiscount,
			OBDI.monTotAmtBefDiscount,
			OBDI.vcNotes,
			OBDI.bitEmbeddedCost,
			OBDI.monEmbeddedCost,
			OBDI.dtRentalStartDate,
			OBDI.dtRentalReturnDate,
			OBDI.tintTrackingStatus
		FROM
			OpportunityBizDocs OBD 
		JOIN 
			OpportunityBizDocItems OBDI 
		ON 
			OBD.numOppBizDocsId=OBDI.numOppBizDocID 
		JOIN
			OpportunityItems OI
		ON
			OBDI.numOppItemID = OI.numoppitemtCode
		INNER JOIN
		(
			SELECT
				*
			FROM
				(SELECT 
					OI.numoppitemtCode,   
					OI.numUnitHour AS QtyOrdered,
					(OI.numUnitHour - ISNULL(SUM(OBI.numUnitHour), 0)) AS QtytoFulFill                                   
				FROM      
					OpportunityItems OI
				LEFT JOIN 
					OpportunityMaster OM 
				ON 
					OI.numOppID = oM.numOppID 
				LEFT JOIN 
					OpportunityBizDocItems OBI 
				ON 
					OBI.numOppItemID = OI.numoppitemtCode AND OBI.numOppBizDocId IN (SELECT numOppBizDocsId FROM OpportunityBizDocs OB WHERE OB.numOppId = OI.numOppId AND ISNULL(OB.bitAuthoritativeBizDocs,0) = 1)
				LEFT JOIN 
					WareHouseItems WI 
				ON 
					WI.numWareHouseItemID = OI.numWarehouseItmsID
				INNER JOIN 
					Item I 
				ON 
					I.[numItemCode] = OI.[numItemCode]
				WHERE     
					OI.numOppId = @numOppId
				GROUP BY  
					OI.numoppitemtCode,
					OI.numUnitHour) X
				WHERE   
					X.QtytoFulFill > 0
				) AS TEMP
		ON
			OBDI.numOppItemID = TEMP.numoppitemtCode 
		WHERE
			OBDI.numOppBizDocID = @numOppBizDocID	
	
		--Set isCompleted to true if total quantity of all items is used in recurring bizdoc
		IF(SELECT 
				COUNT(*)
		   FROM
				OpportunityBizDocItems OBDI 
		   INNER JOIN
				(
					SELECT 
						*
					FROM
						(SELECT 
							OI.numoppitemtCode,   
							OI.numUnitHour AS QtyOrdered,
							(OI.numUnitHour - ISNULL(SUM(OBI.numUnitHour), 0)) AS QtytoFulFill                                   
						FROM      
							OpportunityItems OI
						LEFT JOIN 
							OpportunityMaster OM 
						ON 
							OI.numOppID = oM.numOppID 
						LEFT JOIN 
							OpportunityBizDocItems OBI 
						ON 
							OBI.numOppItemID = OI.numoppitemtCode AND OBI.numOppBizDocId IN (SELECT numOppBizDocsId FROM OpportunityBizDocs OB WHERE OB.numOppId = OI.numOppId AND ISNULL(OB.bitAuthoritativeBizDocs,0) = 1)
						LEFT JOIN 
							WareHouseItems WI 
						ON 
							WI.numWareHouseItemID = OI.numWarehouseItmsID
						INNER JOIN 
							Item I 
						ON 
							I.[numItemCode] = OI.[numItemCode]
						WHERE     
							OI.numOppId = @numOppId
						GROUP BY  
							OI.numoppitemtCode,
							OI.numUnitHour) X
						WHERE   
							X.QtytoFulFill > 0
				) AS TEMP
			ON
				OBDI.numOppItemID = TEMP.numoppitemtCode
			WHERE
				OBDI.numOppBizDocID=@numOppBizDocID) > 0
			BEGIN
				--Get next recurrence date for bizdoc 
				DECLARE @dtNextRecurrenceDate DATE = NULL

				SET @dtNextRecurrenceDate = (CASE @numFrequency
											WHEN 1 THEN DATEADD(D,1,@Date) --Daily
											WHEN 2 THEN DATEADD(D,7,@Date) --Weekly
											WHEN 3 THEN DATEADD(M,1,@Date) --Monthly
											WHEN 4 THEN DATEADD(M,4,@Date)  --Quarterly
											END)

				UPDATE RecurrenceConfiguration SET dtNextRecurrenceDate = @dtNextRecurrenceDate WHERE numRecConfigID = @numRecConfigID
			END
			ELSE
			BEGIN
				UPDATE RecurrenceConfiguration SET bitCompleted = 1 WHERE numRecConfigID = @numRecConfigID
			END

		UPDATE OpportunityBizDocs SET bitRecurred = 1 WHERE numOppBizDocsId = @numNewOppBizDocID
	END
	ELSE
	BEGIN
		-- INSERTS BizDoc Items
		INSERT INTO OpportunityBizDocItems                                                                          
  		(
			numOppBizDocID,numOppItemID,numItemCode,numUnitHour,monPrice,monTotAmount,vcItemDesc,numWarehouseItmsID,vcType,
			vcAttributes,bitDropShip,bitDiscountType,fltDiscount,monTotAmtBefDiscount,vcNotes,bitEmbeddedCost,
			monEmbeddedCost,dtRentalStartDate,dtRentalReturnDate,tintTrackingStatus
		)
		SELECT 
			@numNewOppBizDocID,OINew.numoppitemtCode,OBDI.numItemCode,OBDI.numUnitHour,OBDI.monPrice,OBDI.monTotAmount,OBDI.vcItemDesc,OBDI.numWarehouseItmsID,OBDI.vcType,
			OBDI.vcAttributes,OBDI.bitDropShip,OBDI.bitDiscountType,OBDI.fltDiscount,OBDI.monTotAmtBefDiscount,OBDI.vcNotes,OBDI.bitEmbeddedCost,
			OBDI.monEmbeddedCost,OBDI.dtRentalStartDate,OBDI.dtRentalReturnDate,OBDI.tintTrackingStatus
		FROM 
			OpportunityBizDocs OBD 
		JOIN 
			OpportunityBizDocItems OBDI 
		ON 
			OBD.numOppBizDocsId=OBDI.numOppBizDocID 
		JOIN 
			OpportunityItems OI 
		ON 
			OBDI.numOppItemID=OI.numoppitemtCode
		JOIN
			OpportunityItems OINew
		ON
			OINew.numRecurParentOppItemID = OI.numoppitemtCode
		JOIN 
			Item I 
		ON 
			OBDI.numItemCode=I.numItemCode
		WHERE  
			OBD.numOppId=@numRecurParentOppID 
			AND OBD.numOppBizDocsId=@numOppBizDocID
	END

	IF ISNULL((SELECT numBizDocID FROM OpportunityBizDocs WHERE numOppBizDocsID=@numNewOppBizDocID),0) = 29397
	BEGIN
		UPDATE
			OI
		SET
			OI.numQtyPicked = ISNULL((SELECT 
											SUM(OBDI.numUnitHour)
										FROM 
											OpportunityBizDocs OBD 
										INNER JOIN 
											OpportunityBizDocItems OBDI
										ON
											OBD.numOppBizDocsID = OBDI.numOppBizDocID
											AND OI.numoppitemtCode = OBDI.numOppItemID 
										WHERE 
											OBD.numOppID = OI.numOppID 
											AND OBD.numBizDocID = 29397),0)
		FROM	
			OpportunityItems OI
		WHERE
			OI.numOppID = @numOppId
	END

	SET @numRecurBizDocID = @numNewOppBizDocID
	SET @bitAuthorative = @bitAuthBizdoc
END TRY
BEGIN CATCH
	DECLARE @ErrorMessage NVARCHAR(4000);
    DECLARE @ErrorSeverity INT;
    DECLARE @ErrorState INT;

    SELECT @ErrorMessage = ERROR_MESSAGE(), @ErrorSeverity = ERROR_SEVERITY(), @ErrorState = ERROR_STATE();

    RAISERROR (@ErrorMessage, @ErrorSeverity, @ErrorState);

	IF @@TRANCOUNT > 0
        ROLLBACK TRANSACTION;
END CATCH

IF @@TRANCOUNT > 0
    COMMIT TRANSACTION;

END  
