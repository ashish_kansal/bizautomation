
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_UpdateDomainChangeDefaultAccount')
DROP PROCEDURE USP_UpdateDomainChangeDefaultAccount
GO
CREATE PROCEDURE [dbo].[USP_UpdateDomainChangeDefaultAccount]
(
	@numDomainID NUMERIC(18,0)
	,@numIncomeAccID NUMERIC(18,0)
	,@numCOGSAccID NUMERIC(18,0)
	,@numAssetAccID NUMERIC(18,0)
)
AS
BEGIN
	UPDATE 
		Domain
	SET
		numIncomeAccID=@numIncomeAccID
		,numCOGSAccID=@numCOGSAccID
		,numAssetAccID=@numAssetAccID
	WHERE
		numDomainId=@numDomainID
END