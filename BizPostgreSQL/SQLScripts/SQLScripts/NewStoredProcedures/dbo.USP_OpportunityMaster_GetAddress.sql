SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_OpportunityMaster_GetAddress' ) 
    DROP PROCEDURE USP_OpportunityMaster_GetAddress
GO
CREATE PROCEDURE [dbo].[USP_OpportunityMaster_GetAddress]
	@numDomainID NUMERIC(18,0)
    ,@numOppID AS NUMERIC(18,0)
	,@tintAddressType TINYINT
AS 
BEGIN
	IF @tintAddressType = 1 --Billing Address
	BEGIN
		EXEC usp_getoppaddress @numOppID,0,0
		SELECT vcAddress AS BillingAdderss,numContact,bitAltContact,vcAltContact FROM dbo.fn_getOPPAddressAndContact(@numOppID,@numDomainID,2) AS ShippingAdderss
	END
	ELSE IF @tintAddressType = 2 --Shipping Address
	BEGIN
		EXEC usp_getoppaddress @numOppID,1,0
		SELECT vcAddress AS ShippingAdderss,numContact,bitAltContact,vcAltContact FROM dbo.fn_getOPPAddressAndContact(@numOppID,@numDomainID,2) AS ShippingAdderss
	END

END
GO