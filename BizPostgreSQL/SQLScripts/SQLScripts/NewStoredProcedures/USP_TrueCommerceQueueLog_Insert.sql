SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_TrueCommerceQueueLog_Insert')
DROP PROCEDURE dbo.USP_TrueCommerceQueueLog_Insert
GO


CREATE PROCEDURE [dbo].[USP_TrueCommerceQueueLog_Insert]
(
	--@numTCQueueID NUMERIC(18,0),
	--@TCType INT,
	@FileName VARCHAR(50),
	@numDomainID NUMERIC(18,0),
    @PO VARCHAR(300),
	--@vcLog NTEXT,
	--@bitSuccess BIT,
	@vcMessage VARCHAR(MAX)
)
AS 
BEGIN
	INSERT INTO TrueCommerceLog
	(
		--numTCQueueID
		--,
		FileName
		,numDomainID
		,PurchaseOrder
		,vcMessage		
		,dtDate
	)
	VALUES
	(
		--@numTCQueueID
		--,
		@FileName
		,@numDomainID
		,@PO
		,@vcMessage
		,GETUTCDATE()
	)
	
END


GO


