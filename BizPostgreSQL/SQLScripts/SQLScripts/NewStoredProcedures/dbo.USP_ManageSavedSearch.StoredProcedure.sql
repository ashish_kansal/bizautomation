GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_ManageSavedSearch' ) 
    DROP PROCEDURE USP_ManageSavedSearch
GO
CREATE PROCEDURE USP_ManageSavedSearch
    @numSearchID NUMERIC  output,
    @vcSearchName VARCHAR(50),
    @vcSearchQuery VARCHAR(8000),
    @intSlidingDays INT= 0,
    @vcTimeExpression VARCHAR(1000),
    @numFormID NUMERIC(18, 0),
    @numUserCntID NUMERIC(18, 0),
    @numDomainID NUMERIC(18, 0),
    @tintMode TINYINT,
    @vcSearchQueryDisplay VARCHAR(8000),
	@vcSearchConditionJson VARCHAR(MAX),
	@vcSharedWithUsers VARCHAR(MAX),
	@vcDisplayColumns VARCHAR(MAX)
AS 
BEGIN

    IF @tintMode = 1 
    BEGIN
        SELECT  
			numSearchID,
            vcSearchName,
            vcSearchQuery,
            intSlidingDays,
            vcTimeExpression,
            S.numFormID,
            numUserCntID,
            numDomainID,
            vcSearchQueryDisplay
			,CASE DFM.numFormId WHEN 1 THEN 'Companies & Contacts Search' ELSE DFM.vcFormName END AS vcFormName
			,vcSearchConditionJson
			,vcDisplayColumns
			,STUFF((SELECT 
						CONCAT(',',numUserCntID)
					FROM 
						SavedSearch SS 
					WHERE 
						SS.numDomainID=@numDomainID 
						AND numSharedFromSearchID=S.numSearchID
					FOR XML PATH('')),1,1,'') vcSharedWith
        FROM
			dbo.SavedSearch S
        INNER JOIN
			dbo.DynamicFormMaster DFM 
		ON 
			DFM.numFormId = S.numFormId
        WHERE 
			(numSearchID = @numSearchID OR @numSearchID = 0)
			AND numDomainID = @numDomainID
			AND numUserCntID = @numUserCntID
			AND (S.numFormID=@numFormID OR @numFormID = 0)
    END
    ELSE IF @tintMode = 2 
    BEGIN
        IF EXISTS(select * from SavedSearch where numSearchID = @numSearchID)
        BEGIN 
            UPDATE 
				dbo.SavedSearch 
			SET 
				vcSearchName = @vcSearchName
				,vcSearchQuery = @vcSearchQuery
				,intSlidingDays = @intSlidingDays
				,vcTimeExpression = @vcTimeExpression
				,numFormID = @numFormID
				,numUserCntID = @numUserCntID
				,numDomainID = @numDomainID
				,vcSearchQueryDisplay = @vcSearchQueryDisplay
				,vcSearchConditionJson = @vcSearchConditionJson
				,vcDisplayColumns=@vcDisplayColumns
            WHERE 
				numSearchID = @numSearchID

			DELETE FROM SavedSearch WHERE numDomainID=@numDomainID AND numSharedFromSearchID=@numSearchID
			
			IF LEN(ISNULL(@vcSharedWithUsers,0)) > 0
			BEGIN
				INSERT  INTO dbo.SavedSearch 
				(
					vcSearchName
					,vcSearchQuery
					,intSlidingDays
					,vcTimeExpression
					,numFormID
					,numUserCntID
					,numDomainID
					,vcSearchQueryDisplay
					,vcSearchConditionJson
					,numSharedFromSearchID
					,vcDisplayColumns
				)
				SELECT
					@vcSearchName,
					@vcSearchQuery,
					@intSlidingDays,
					@vcTimeExpression,
					@numFormID,
					UserMaster.numUserDetailId,
					@numDomainID,
					@vcSearchQueryDisplay,
					@vcSearchConditionJson,
					@numSearchID,
					@vcDisplayColumns
				FROM 
					SplitIDs(@vcSharedWithUsers,',') AS TEMPTable
				INNER JOIN
					UserMaster
				ON
					TEMPTable.Id = UserMaster.numUserDetailId
				WHERE
					UserMaster.numDomainID= @numDomainID
			END
        END   
        ELSE 
        BEGIN
            INSERT  INTO dbo.SavedSearch 
			(
				vcSearchName
				,vcSearchQuery
				,intSlidingDays
				,vcTimeExpression
				,numFormID
				,numUserCntID
				,numDomainID
				,vcSearchQueryDisplay
				,vcSearchConditionJson
				,vcDisplayColumns
			)
			VALUES  
			(
                @vcSearchName,
                @vcSearchQuery,
                @intSlidingDays,
                @vcTimeExpression,
                @numFormID,
                @numUserCntID,
                @numDomainID,
                @vcSearchQueryDisplay,
				@vcSearchConditionJson,
				@vcDisplayColumns
            ) 

			SELECT @numSearchID =  SCOPE_IDENTITY()
                        
			
			IF LEN(ISNULL(@vcSharedWithUsers,0)) > 0
			BEGIN
				INSERT  INTO dbo.SavedSearch 
				(
					vcSearchName
					,vcSearchQuery
					,intSlidingDays
					,vcTimeExpression
					,numFormID
					,numUserCntID
					,numDomainID
					,vcSearchQueryDisplay
					,vcSearchConditionJson
					,numSharedFromSearchID
					,vcDisplayColumns
				)
				SELECT
					@vcSearchName,
					@vcSearchQuery,
					@intSlidingDays,
					@vcTimeExpression,
					@numFormID,
					UserMaster.numUserDetailId,
					@numDomainID,
					@vcSearchQueryDisplay,
					@vcSearchConditionJson,
					@numSearchID,
					@vcDisplayColumns
				FROM 
					SplitIDs(@vcSharedWithUsers,',') AS TEMPTable
				INNER JOIN
					UserMaster
				ON
					TEMPTable.Id = UserMaster.numUserDetailId
				WHERE
					UserMaster.numDomainID= @numDomainID
			END
        END
    END
    ELSE 
    IF @tintMode = 3 
    BEGIN
        DELETE FROM dbo.SavedSearch WHERE numUserCntID = @numUserCntID AND numDomainID = @numDomainID AND numSearchID = @numSearchID
    END
END