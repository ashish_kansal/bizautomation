GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ScheduledReportsGroup_GetAll')
DROP PROCEDURE dbo.USP_ScheduledReportsGroup_GetAll
GO
CREATE PROCEDURE [dbo].[USP_ScheduledReportsGroup_GetAll]
(
	@numDomainID NUMERIC(18,0)
	,@ClientTimeZoneOffset INT
	,@tintMode TINYINT
)
AS 
BEGIN
	SELECT
		ID
		,ISNULL(vcName,'') vcName
		,CONCAT((CASE tintFrequency WHEN 3 THEN 'Monthly' WHEN 2 THEN 'Weekly' ELSE 'Daily' END),' at ',DATEPART(HOUR, DATEADD(MINUTE,-@ClientTimeZoneOffset,dtStartDate)),SUBSTRING(CONVERT(varchar(20), DATEADD(MINUTE,-@ClientTimeZoneOffset,dtStartDate), 22), 18, 3), ' starting ',DATENAME(WEEKDAY,DATEADD(MINUTE,-@ClientTimeZoneOffset,dtStartDate)),' ',dbo.FormatedDateFromDate(DATEADD(MINUTE,-@ClientTimeZoneOffset,dtStartDate),@numDomainID)) vcSchedule
		,numEmailTemplate
		,ISNULL(GenericDocuments.vcDocName,'') vcEmailTemplate
		,ISNULL(vcRecipientsEmail,'') vcRecipients
	FROM
		ScheduledReportsGroup
	LEFT JOIN
		GenericDocuments
	ON
		ScheduledReportsGroup.numEmailTemplate = GenericDocuments.numGenericDocID
	WHERE
		ScheduledReportsGroup.numDomainID = @numDomainID
		AND 1 = (CASE WHEN @tintMode=1 THEN (CASE WHEN ISNULL((SELECT COUNT(*) FROM ScheduledReportsGroupReports SRGR WHERE SRGR.numSRGID=ScheduledReportsGroup.ID),0) < 5 THEN 1 ELSE 0 END) ELSE 1 END)
	ORDER BY
		vcName
END