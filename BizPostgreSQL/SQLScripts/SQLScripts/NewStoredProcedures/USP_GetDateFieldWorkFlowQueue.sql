GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_GetDateFieldWorkFlowQueue' ) 
    DROP PROCEDURE USP_GetDateFieldWorkFlowQueue
GO
CREATE PROCEDURE USP_GetDateFieldWorkFlowQueue

	  @numDomainID NUMERIC(18,0) 
	 ,@numFormID NUMERIC(18, 0)
	 ,@numWFID NUMERIC(18, 0)
	 ,@textCondition NVARCHAR(MAX)
	 ,@textConditionCustom NVARCHAR(MAX)
AS 
BEGIN 

	DECLARE @SQL NVARCHAR(MAX)
		
		IF @numFormID = 68  --Organization

		BEGIN

		SELECT  @SQL = 'SELECT DMT.numDivisionID AS numRecordID FROM DivisionMaster_TempDateFields DMT
			WHERE numDomainID = @numDomainID 
			AND DMT.numDivisionID NOT IN (SELECT numRecordID FROM WorkFlowDateFieldExecutionHistory WHERE (CAST(dtCreatedDate AS DATE) = (CAST(GETUTCDATE() AS DATE ))) AND numWFID = @numWFID)'
			 + @textCondition 
			 
			IF LEN(ISNULL(@textConditionCustom,'')) > 0
			BEGIN
				SET @SQL = @SQL + ' UNION SELECT RecId  AS numRecordID FRom CFW_FLD_Values WHERE ISDATE(Fld_Value) = 1 
				AND RecId NOT IN (SELECT numRecordID FROM WorkFlowDateFieldExecutionHistory WHERE (CAST(dtCreatedDate AS DATE) = (CAST(GETUTCDATE() AS DATE))) AND numWFID = @numWFID)'
				+ @textConditionCustom
			END

		 END

		 ELSE IF @numFormID = 69  --Contacts
		 BEGIN

		 SELECT  @SQL = 'SELECT ACIT.numContactId AS numRecordID, * FROM AdditionalContactsInformation_TempDateFields ACIT
			WHERE numDomainID = @numDomainID 
			AND ACIT.numContactId NOT IN (SELECT numRecordID FROM WorkFlowDateFieldExecutionHistory WHERE (CAST(dtCreatedDate AS DATE) = (CAST(GETUTCDATE() AS DATE ))) AND numWFID = @numWFID)'
			 + @textCondition 

			IF LEN(ISNULL(@textConditionCustom,'')) > 0
			BEGIN
				SET @SQL = @SQL + ' UNION SELECT RecId  AS numRecordID FRom CFW_FLD_Values_Cont WHERE ISDATE(Fld_Value) = 1 
				AND RecId NOT IN (SELECT numRecordID FROM WorkFlowDateFieldExecutionHistory WHERE (CAST(dtCreatedDate AS DATE) = (CAST(GETUTCDATE() AS DATE))) AND numWFID = @numWFID)'
				 + @textConditionCustom
			END
		  
		 END

		 ELSE IF @numFormID = 70  --Opp/Order
		 BEGIN

		 SELECT  @SQL = 'SELECT OMT.numOppId AS numRecordID, * FROM OpportunityMaster_TempDateFields OMT
			WHERE numDomainID = @numDomainID 
			AND OMT.numOppId NOT IN (SELECT numRecordID FROM WorkFlowDateFieldExecutionHistory WHERE (CAST(dtCreatedDate AS DATE) = (CAST(GETUTCDATE() AS DATE ))) AND numWFID = @numWFID)'
			 + @textCondition 

			IF LEN(ISNULL(@textConditionCustom,'')) > 0
			BEGIN
				SET @SQL = @SQL + ' UNION SELECT RecId  AS numRecordID FRom CFW_Fld_Values_Opp WHERE ISDATE(Fld_Value) = 1 
				AND RecId NOT IN (SELECT numRecordID FROM WorkFlowDateFieldExecutionHistory WHERE (CAST(dtCreatedDate AS DATE) = (CAST(GETUTCDATE() AS DATE))) AND numWFID = @numWFID)'
				 + @textConditionCustom
			END
		  
		 END

		ELSE IF @numFormID = 72  --Cases
		 BEGIN

		 SELECT  @SQL = 'SELECT CT.numCaseId AS numRecordID, * FROM Cases_TempDateFields CT
			WHERE numDomainID = @numDomainID 
			AND CT.numCaseId NOT IN (SELECT numRecordID FROM WorkFlowDateFieldExecutionHistory WHERE (CAST(dtCreatedDate AS DATE) = (CAST(GETUTCDATE() AS DATE ))) AND numWFID = @numWFID)'
			 + @textCondition 

			IF LEN(ISNULL(@textConditionCustom,'')) > 0
			BEGIN
				SET @SQL = @SQL + ' UNION SELECT RecId  AS numRecordID FRom CFW_FLD_Values_Case WHERE ISDATE(Fld_Value) = 1 
				AND RecId NOT IN (SELECT numRecordID FROM WorkFlowDateFieldExecutionHistory WHERE (CAST(dtCreatedDate AS DATE) = (CAST(GETUTCDATE() AS DATE))) AND numWFID = @numWFID)'
				 + @textConditionCustom
			END
		  
		 END

		 ELSE IF @numFormID = 73  --Projects
		 BEGIN

		 SELECT  @SQL = 'SELECT PMT.numProId AS numRecordID, * FROM ProjectsMaster_TempDateFields PMT
			WHERE numDomainID = @numDomainID 
			AND PMT.numProId NOT IN (SELECT numRecordID FROM WorkFlowDateFieldExecutionHistory WHERE (CAST(dtCreatedDate AS DATE) = (CAST(GETUTCDATE() AS DATE ))) AND numWFID = @numWFID)'
			 + @textCondition 

			IF LEN(ISNULL(@textConditionCustom,'')) > 0
			BEGIN
				SET @SQL = @SQL + ' UNION SELECT RecId  AS numRecordID FRom CFW_FLD_Values_Pro WHERE ISDATE(Fld_Value) = 1 
				AND RecId NOT IN (SELECT numRecordID FROM WorkFlowDateFieldExecutionHistory WHERE (CAST(dtCreatedDate AS DATE) = (CAST(GETUTCDATE() AS DATE))) AND numWFID = @numWFID)'
				 + @textConditionCustom
			END
		  
		 END

		  ELSE IF @numFormID = 124  --Action Items
		 BEGIN

		 SELECT  @SQL = 'SELECT CT.numCommId AS numRecordID, * FROM Communication_TempDateFields CT
			WHERE numDomainID = @numDomainID 
			AND CT.numCommId NOT IN (SELECT numRecordID FROM WorkFlowDateFieldExecutionHistory WHERE (CAST(dtCreatedDate AS DATE) = (CAST(GETUTCDATE() AS DATE ))) AND numWFID = @numWFID)'
			 + @textCondition 

			--IF LEN(ISNULL(@textConditionCustom,'')) > 0
			--BEGIN
			--	SET @SQL = @SQL + ' UNION SELECT RecId  AS numRecordID FRom CFW_FLD_Values_Pro WHERE ISDATE(Fld_Value) = 1 ' + @textConditionCustom
			--END
		  
		 END

		PRINT @SQL
		
		EXEC sp_executesql @SQL,N'@numDomainID numeric(18, 0),@numWFID NUMERIC(18, 0),@textCondition NVARCHAR(MAX),@textConditionCustom NVARCHAR(MAX)',
			@numDomainID = @numDomainID,@numWFID = @numWFID,@textCondition = @textCondition, @textConditionCustom =@textConditionCustom 
END


