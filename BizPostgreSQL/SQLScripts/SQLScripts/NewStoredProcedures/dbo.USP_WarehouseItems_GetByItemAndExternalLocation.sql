SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_WarehouseItems_GetByItemAndExternalLocation')
DROP PROCEDURE dbo.USP_WarehouseItems_GetByItemAndExternalLocation
GO
CREATE PROCEDURE [dbo].[USP_WarehouseItems_GetByItemAndExternalLocation]
@numDomainID NUMERIC(18,0),
@numWareHouseID NUMERIC(18,0),
@numItemCode NUMERIC(18,0)                                             
AS
BEGIN
	SELECT
		numWareHouseID,
		numWareHouseItemID
	FROM
		WareHouseItems
	WHERE
		numDomainID=@numDomainID
		AND numItemID=@numItemCode
		AND numWareHouseID=@numWareHouseID
	ORDER BY
		numWareHouseItemID
END