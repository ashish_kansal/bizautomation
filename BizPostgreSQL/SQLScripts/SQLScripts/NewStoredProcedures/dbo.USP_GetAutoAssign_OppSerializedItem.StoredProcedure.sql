SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetAutoAssign_OppSerializedItem')
DROP PROCEDURE USP_GetAutoAssign_OppSerializedItem
GO
CREATE PROCEDURE [dbo].[USP_GetAutoAssign_OppSerializedItem]  
    @numOppID as numeric(9)=0,  
    @numOppBizDocsId as numeric(9)=0,
    @numWarehouseItmsID as numeric(9)=0,
	@bitSerialized as bit,
    @bitLotNo as bit,
	@numUnitHour as FLOAT
as  

Declare @vcSerialNoList varchar(2000);SET @vcSerialNoList=''

Create table #tempWareHouseItmsDTL (                                                                    
ID numeric(18) IDENTITY(1,1) NOT NULL,vcSerialNo VARCHAR(100),numQty [numeric](18, 0),numWareHouseItmsDTLID [numeric](18, 0)                                             
 )   

INSERT INTO #tempWareHouseItmsDTL 
(
	vcSerialNo,numWarehouseItmsDTLID,numQty
)  
SELECT 
	vcSerialNo,numWareHouseItmsDTLID,ISNULL(numQty,0) as TotalQty
FROM  
	WareHouseItmsDTL   
WHERE 
	ISNULL(numQty,0) > 0
	AND numWareHouseItemID=@numWarehouseItmsID  
    AND numWareHouseItmsDTLID NOT IN (SELECT numWarehouseItmsDTLID FROM OppWarehouseSerializedItem WHERE numOppID=@numOppID AND numWarehouseItmsID=@numWarehouseItmsID AND numOppBizDocsId=@numOppBizDocsId) 
ORDER BY TotalQty desc

IF ((Select sum(numQty) from #tempWareHouseItmsDTL)>=@numUnitHour)
BEGIN
DECLARE  @maxWareID NUMERIC(9),@minWareID NUMERIC(9),@vcSerialNo varchar(100)
DECLARE  @numWarehouseItmsDTLID NUMERIC(9),@numQty NUMERIC(9),@numUseQty NUMERIC(9)
	
SELECT @maxWareID = max(ID),@minWareID = min(ID) FROM  #tempWareHouseItmsDTL 

WHILE (@numUnitHour>0 and @minWareID <= @maxWareID)
BEGIN
	SELECT @vcSerialNo =vcSerialNo,@numWarehouseItmsDTLID = numWarehouseItmsDTLID,@numQty = numQty FROM  #tempWareHouseItmsDTL  where ID=@minWareID

	IF @numUnitHour >= @numQty
		BEGIN
			SET @numUseQty=@numQty
	
			SET @numUnitHour=@numUnitHour-@numQty
		END
	else IF @numUnitHour < @numQty
		BEGIN
			SET @numUseQty=@numUnitHour

			SET @numUnitHour=0
		END
		
		IF @bitSerialized=1
			SET @vcSerialNoList = @vcSerialNoList + @vcSerialNo + ','
		ELSE IF @bitLotNo=1
			SET @vcSerialNoList = @vcSerialNoList + @vcSerialNo + '(' + Cast(@numUseQty as varchar(10)) + '),'

SET @minWareID=@minWareID + 1
END

END

drop table #tempWareHouseItmsDTL

select substring(@vcSerialNoList,0,LEN(@vcSerialNoList)) as vcSerialNoList
GO
