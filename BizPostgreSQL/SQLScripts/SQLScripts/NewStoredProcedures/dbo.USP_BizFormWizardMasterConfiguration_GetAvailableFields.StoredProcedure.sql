GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_BizFormWizardMasterConfiguration_GetAvailableFields' ) 
    DROP PROCEDURE USP_BizFormWizardMasterConfiguration_GetAvailableFields
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author: Sandeep Patel
-- Create date: 23 July 2014
-- Description:	Gets list of available fields of form
-- =============================================
CREATE PROCEDURE USP_BizFormWizardMasterConfiguration_GetAvailableFields
	@numDomainID NUMERIC(18,0),
	@numFormID NUMERIC(18,0),
	@numGroupID NUMERIC(18,0),
	@numRelCntType NUMERIC(18,0),
	@tintPageType TINYINT,
	@pageID NUMERIC(18,0),
	@bitGridConfiguration BIT = 0,
	@numFormFieldGroupId NUMERIC(18,0)=0
AS
BEGIN
	SET NOCOUNT ON;

	IF @bitGridConfiguration = 1 -- Retrive Available Fields For Grids Columns Configuration
	BEGIN           
		IF @PageId = 1 OR @PageId = 4
		BEGIN          
			SELECT  
				vcFieldName AS vcNewFormFieldName,
				vcFieldName AS vcFormFieldName,
				CONVERT(VARCHAR(15),numFieldId) + 'R' AS numFormFieldId,
				'R' as vcFieldType,
				vcAssociatedControlType,
				numListID,
				vcDbColumnName,
				vcListItemType,
				0 intColumnNum,
				0 intRowNum,
				0 boolRequired,
				0 numAuthGroupID,
				CASE WHEN vcAssociatedControlType = 'DateField' THEN 'D' ELSE vcFieldDataType END vcFieldDataType,
				0 boolAOIField,
				numFormID,
				vcLookBackTableName,
				CONVERT(VARCHAR(15), numFieldId) + '~' + CONVERT(VARCHAR(15),ISNULL(bitCustom,0)) + '~' + CONVERT(VARCHAR(15),ISNULL(tintOrder,0)) AS vcFieldAndType,
				0 AS bitDefaultMandatory,
				ISNULL(@numFormFieldGroupId,0) AS numFormGroupId
			FROM    View_DynamicDefaultColumns
			WHERE   numFormID = @numFormID
					AND ISNULL(bitSettingField, 0) = 1
					AND ISNULL(bitDeleted, 0) = 0
					AND numDomainID = @numDomainID
					AND numFieldId NOT IN (
								SELECT 
									numFieldID 
								FROM 
									BizFormWizardMasterConfiguration
								WHERE
									numFormId = @numFormID AND
									numDomainID = @numDomainID AND
									numGroupID = @numGroupID AND
									numRelCntType = @numRelCntType AND
									bitCustom = 0 AND
									bitGridConfiguration = 1 )
			UNION
			SELECT  
				fld_label as vcNewFormFieldName,
				fld_label as vcFormFieldName,
				CONVERT(VARCHAR(15),fld_id) + 'C' as numFormFieldId,
				'C' as vcFieldType,
				fld_type as vcAssociatedControlType,
				ISNULL(CFW_Fld_Master.numListID, 0) numListID,
				Fld_label vcDbColumnName,
				CASE WHEN CFW_Fld_Master.numListID > 0 THEN 'LI' ELSE '' END vcListItemType,
				0 intColumnNum,
				0 intRowNum,
				0 as boolRequired,
				0 numAuthGroupID,
				CASE WHEN CFW_Fld_Master.numListID > 0 THEN 'N' ELSE 'V' END vcFieldDataType,
				0 boolAOIField,
				@numFormId AS numFormID,
				'' AS vcLookBackTableName,
				CONVERT(VARCHAR(15), Fld_id) + '~1' + '~0' AS vcFieldAndType,
				0 AS bitDefaultMandatory,
				ISNULL(@numFormFieldGroupId,0) AS numFormGroupId
			FROM    CFW_Fld_Master
					LEFT JOIN CFW_Fld_Dtl ON Fld_id = numFieldId
												AND numRelation = CASE
																WHEN @numRelCntType IN (
																1, 2, 3 )
																THEN numRelation
																ELSE @numRelCntType
																END
					LEFT JOIN CFw_Grp_Master ON subgrp = CFw_Grp_Master.Grp_id
			WHERE   CFW_Fld_Master.grp_id = @PageId
					AND CFW_Fld_Master.numDomainID = @numDomainID
					AND Fld_type <> 'Link'
					AND fld_id NOT IN (
									SELECT 
										numFieldID 
									FROM 
										BizFormWizardMasterConfiguration
									WHERE
										numFormId = @numFormID AND
										numDomainID = @numDomainID AND
										numGroupID = @numGroupID AND
										numRelCntType = @numRelCntType AND
										bitCustom = 1 AND
										bitGridConfiguration = 1)
			ORDER BY 
					vcFieldName                                       
		END  
		ELSE
		BEGIN
			SELECT  
				vcFieldName AS vcNewFormFieldName,
				vcFieldName AS vcFormFieldName,
				CONVERT(VARCHAR(15),numFieldId) + 'R' AS numFormFieldId,
				'R' as vcFieldType,
				vcAssociatedControlType,
				numListID,
				vcDbColumnName,
				vcListItemType,
				0 intColumnNum,
				0 intRowNum,
				0 boolRequired,
				0 numAuthGroupID,
				CASE WHEN vcAssociatedControlType = 'DateField' THEN 'D' ELSE vcFieldDataType END vcFieldDataType,
				0 boolAOIField,
				numFormID,
				vcLookBackTableName,
				CONVERT(VARCHAR(15), numFieldId) + '~' + CONVERT(VARCHAR(15),ISNULL(bitCustom,0)) + '~' + CONVERT(VARCHAR(15),ISNULL(tintOrder,0)) AS vcFieldAndType,
				0 AS bitDefaultMandatory,
				ISNULL(@numFormFieldGroupId,0) AS numFormGroupId
			FROM    View_DynamicDefaultColumns
			WHERE   numFormID = @numFormID
					AND ISNULL(bitSettingField, 0) = 1
					AND ISNULL(bitDeleted, 0) = 0
					AND numDomainID = @numDomainID
					AND numFieldId NOT IN (
								SELECT 
									numFieldID 
								FROM 
									BizFormWizardMasterConfiguration
								WHERE
									numFormId = @numFormID AND
									numDomainID = @numDomainID AND
									numGroupID = @numGroupID AND
									numRelCntType = @numRelCntType AND
									bitCustom = 0 AND
									bitGridConfiguration = 1 )
			UNION
			SELECT  
				fld_label as vcNewFormFieldName,
				fld_label as vcFormFieldName,
				CONVERT(VARCHAR(15),fld_id) + 'C' as numFormFieldId,
				'C' as vcFieldType,
				fld_type as vcAssociatedControlType,
				ISNULL(CFW_Fld_Master.numListID, 0) numListID,
				Fld_label vcDbColumnName,
				CASE WHEN CFW_Fld_Master.numListID > 0 THEN 'LI' ELSE '' END vcListItemType,
				0 intColumnNum,
				0 intRowNum,
				0 as boolRequired,
				0 numAuthGroupID,
				CASE WHEN CFW_Fld_Master.numListID > 0 THEN 'N' ELSE 'V' END vcFieldDataType,
				0 boolAOIField,
				@numFormId AS numFormID,
				'' AS vcLookBackTableName,
				CONVERT(VARCHAR(15), Fld_id) + '~1' + '~0' AS vcFieldAndType,
				0 AS bitDefaultMandatory,
				ISNULL(@numFormFieldGroupId,0) AS numFormGroupId
			FROM    CFW_Fld_Master
					LEFT JOIN CFw_Grp_Master ON subgrp = CFw_Grp_Master.Grp_id
			WHERE   CFW_Fld_Master.grp_id = @PageId
					AND CFW_Fld_Master.numDomainID = @numDomainID
					AND Fld_type <> 'Link'
					AND fld_id NOT IN (
									SELECT 
										numFieldID 
									FROM 
										BizFormWizardMasterConfiguration
									WHERE
										numFormId = @numFormID AND
										numDomainID = @numDomainID AND
										numGroupID = @numGroupID AND
										numRelCntType = @numRelCntType AND
										bitCustom = 1 AND
										bitGridConfiguration = 1)
			ORDER BY 
					vcFieldName                   
		END
	END
	ELSE --Retrive Available Fields For Detail(Layout) Form
	BEGIN
		SELECT 
			vcFieldName AS vcNewFormFieldName,
			vcFieldName AS vcFormFieldName,
			CONVERT(VARCHAR(15),numFieldId) + 'R' AS numFormFieldId,
			'R' as vcFieldType,
			vcAssociatedControlType,
			numListID,
			vcDbColumnName,
			vcListItemType,
			0 intColumnNum,
			0 intRowNum,
			0 boolRequired,
			0 numAuthGroupID,
			CASE WHEN vcAssociatedControlType = 'DateField' THEN 'D' ELSE vcFieldDataType END vcFieldDataType,
			0 boolAOIField,
			numFormID,
			vcLookBackTableName,
			CONVERT(VARCHAR(15), numFieldId) + '~' + CONVERT(VARCHAR(15),ISNULL(bitCustom,0)) + '~' + CONVERT(VARCHAR(15),ISNULL(tintOrder,0)) AS vcFieldAndType,
			0 AS bitDefaultMandatory,
			ISNULL(@numFormFieldGroupId,0) AS numFormGroupId
		FROM 
			View_DynamicDefaultColumns
		WHERE 
			numFormId=@numFormID AND 
			numDomainID=@numDomainID  AND
			1=(CASE 
					WHEN @tintPageType=2 THEN 
					CASE 
						WHEN ISNULL(bitAddField,0)= 1 THEN 1 
						ELSE 0 
					END 
					WHEN @tintPageType=3 THEN 
					CASE 
						WHEN ISNULL(bitDetailField,0)=1 THEN 1 
						ELSE 0 
					END
					ELSE 
						0 
				END) AND 
			numFieldId NOT IN (
								SELECT 
									numFieldID 
								FROM 
									BizFormWizardMasterConfiguration
								WHERE
									numFormId = @numFormID AND
									numDomainID = @numDomainID AND
									numGroupID = @numGroupID AND
									numRelCntType = @numRelCntType AND
									tintPageType = @tintPageType AND
									bitCustom = 0 AND
									bitGridConfiguration = 0 )
		ORDER BY
			vcNewFormFieldName

		IF @pageID=4 OR @pageID=1  OR @pageID =12 OR @pageID =13  OR @pageID =14                
		BEGIN           
			SELECT 
				fld_label as vcNewFormFieldName,
				fld_label as vcFormFieldName,
				CONVERT(VARCHAR(15),fld_id) + 'C' as numFormFieldId,
				'C' as vcFieldType,
				fld_type as vcAssociatedControlType,
				ISNULL(CFM.numListID, 0) numListID,
				Fld_label vcDbColumnName,
				CASE WHEN CFM.numListID > 0 THEN 'LI' ELSE '' END vcListItemType,
				0 intColumnNum,
				0 intRowNum,
				0 as boolRequired,
				0 numAuthGroupID,
				CASE WHEN CFM.numListID > 0 THEN 'N' ELSE 'V' END vcFieldDataType,
				0 boolAOIField,
				@numFormId AS numFormID,
				'' AS vcLookBackTableName,
				CONVERT(VARCHAR(15), Fld_id) + '~1' + '~0' AS vcFieldAndType,
				0 AS bitDefaultMandatory,
				ISNULL(@numFormFieldGroupId,0) AS numFormGroupId
			FROM 
				CFW_Fld_Master CFM 
			JOIN 
				CFW_Fld_Dtl CFD 
			ON 
				Fld_id=numFieldId 
			LEFT JOIN 
				CFw_Grp_Master 
			ON 
				subgrp=CFM.Grp_id                              
			WHERE 
				CFM.grp_id=@pageID AND 
				CFD.numRelation=@numRelCntType AND 
				CFM.numDomainID=@numDomainID AND 
				subgrp=0 
				AND fld_id NOT IN (
									SELECT 
										numFieldID 
									FROM 
										BizFormWizardMasterConfiguration
									WHERE
										numFormId = @numFormID AND
										numDomainID = @numDomainID AND
										numGroupID = @numGroupID AND
										numRelCntType = @numRelCntType AND
										tintPageType = @tintPageType AND
										bitCustom = 1 AND
										bitGridConfiguration = 0)
			ORDER BY 
				vcNewFormFieldName,
				subgrp,
				numOrder
		END        
      
		IF @pageID= 2 or  @pageID= 3 or @pageID= 5 or @pageID= 6 or @pageID= 7 or @pageID= 8   or @pageID= 11                
		BEGIN 
			SELECT 
				fld_label as vcNewFormFieldName,
				fld_label as vcFormFieldName,
				CONVERT(VARCHAR(15),fld_id) + 'C' as numFormFieldId,
				'C' as vcFieldType,
				fld_type as vcAssociatedControlType,
				ISNULL(CFM.numListID, 0) numListID,
				Fld_label vcDbColumnName,
				CASE WHEN CFM.numListID > 0 THEN 'LI' ELSE '' END vcListItemType,
				0 intColumnNum,
				0 intRowNum,
				0 as boolRequired,
				0 numAuthGroupID,
				CASE WHEN CFM.numListID > 0 THEN 'N' ELSE 'V' END vcFieldDataType,
				0 boolAOIField,
				@numFormId AS numFormID,
				'' AS vcLookBackTableName,
				CONVERT(VARCHAR(15), Fld_id) + '~1' + '~0' AS vcFieldAndType,
				0 AS bitDefaultMandatory,
				ISNULL(@numFormFieldGroupId,0) AS numFormGroupId
			FROM 
				CFW_Fld_Master CFM 
			LEFT JOIN 
				CFw_Grp_Master 
			ON 
				subgrp=CFM.Grp_id                              
			WHERE 
				CFM.grp_id=@pageID AND 
				CFM.numDomainID=@numDomainID AND 
				subgrp=0 AND 
				fld_id NOT IN (
								SELECT 
										numFieldID 
								FROM 
									BizFormWizardMasterConfiguration
								WHERE
									numFormId = @numFormID AND
									numDomainID = @numDomainID AND
									numGroupID = @numGroupID AND
									numRelCntType = @numRelCntType AND
									tintPageType = @tintPageType AND
									bitCustom = 1 AND
									bitGridConfiguration = 0)
			ORDER BY 
				vcNewFormFieldName,
				subgrp                   
		END       
	END
END
GO