GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ManageElectiveItemFields')
DROP PROCEDURE USP_ManageElectiveItemFields
GO
CREATE PROCEDURE USP_ManageElectiveItemFields
@numDomainID NUMERIC(9),
@vcElectiveItemFields VARCHAR(500)=''
AS 
BEGIN
	UPDATE Domain SET vcElectiveItemFields=@vcElectiveItemFields WHERE numDomainId=@numDomainID
END