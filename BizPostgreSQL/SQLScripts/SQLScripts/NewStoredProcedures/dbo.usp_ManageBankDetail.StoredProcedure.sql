        
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_ManageBankDetail')
	DROP PROCEDURE usp_ManageBankDetail
GO

/****** Added By : Joseph ******/
/****** Add/Update New Bank Account Detail to Table BankDetails mapping Financial Institution from Table BankMaster******/

CREATE PROCEDURE [dbo].[usp_ManageBankDetail]
	@numBankDetailID numeric(18, 0),
	@numBankMasterID numeric(18, 0),
	@numDomainID numeric(18, 0),
	@numUserContactID numeric(18, 0),
	@numAccountID numeric(18, 0),
	@vcBankID varchar(50),
	@vcAccountNumber varchar(50),
	@vcAccountType varchar(50),
	@vcUserName varchar(50),
	@vcPassword varchar(50),
	@bitIsActive bit
AS

SET NOCOUNT ON

IF EXISTS(SELECT [numBankDetailID] FROM [dbo].[BankDetails] WHERE [numBankDetailID] = @numBankDetailID)
BEGIN
	UPDATE [dbo].[BankDetails] SET
		[numBankMasterID] = @numBankMasterID,
		[numDomainID] = @numDomainID,
		[numUserContactID] = @numUserContactID,
		[numAccountID] = @numAccountID,
		[vcBankID] = @vcBankID,
		[vcAccountNumber] = @vcAccountNumber,
		[vcAccountType] = @vcAccountType,
		[vcUserName] = @vcUserName,
		[vcPassword] = @vcPassword,
		[dtModifiedDate] = GETDATE(),
		bitIsActive = @bitIsActive
	WHERE
		[numBankDetailID] = @numBankDetailID
END
ELSE
BEGIN
	INSERT INTO [dbo].[BankDetails] (
		[numBankMasterID],
		[numDomainID],
		[numUserContactID],
		[numAccountID],
		[vcBankID],
		[vcAccountNumber],
		[vcAccountType],
		[vcUserName],
		[vcPassword],
		[dtCreatedDate],
		bitIsActive
	) VALUES (
		@numBankMasterID,
		@numDomainID,
		@numUserContactID,
		@numAccountID,
		@vcBankID,
		@vcAccountNumber,
		@vcAccountType,
		@vcUserName,
		@vcPassword,
		GETDATE(),
		@bitIsActive
	)
	SET @numBankDetailID = SCOPE_IDENTITY()	
	
	UPDATE dbo.Chart_Of_Accounts SET numBankDetailID = @numBankDetailID, IsConnected = 1
	WHERE numAccountId = @numAccountID
	AND numDomainId = @numDomainID
	
END


SELECT @numBankDetailID
GO
