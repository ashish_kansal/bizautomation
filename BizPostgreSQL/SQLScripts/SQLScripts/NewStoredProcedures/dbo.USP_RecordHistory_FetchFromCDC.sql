SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_RecordHistory_FetchFromCDC')
DROP PROCEDURE dbo.USP_RecordHistory_FetchFromCDC
GO
CREATE PROCEDURE [dbo].[USP_RecordHistory_FetchFromCDC]

AS
BEGIN
	-- GET Opp/Order History
	EXEC USP_RecordHistory_FetchFromCDCOppOrder
END