GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_PromotionOffer_GetPromotionApplicableToItem')
DROP PROCEDURE USP_PromotionOffer_GetPromotionApplicableToItem
GO
CREATE PROCEDURE [dbo].[USP_PromotionOffer_GetPromotionApplicableToItem]
(
	@numDomainID NUMERIC(18,0)
	,@numDivisionID NUMERIC(18,0)
	,@numSiteID NUMERIC(18,0)
	,@numItemCode NUMERIC(18,0)
)
AS
BEGIN
	DECLARE @numRelationship NUMERIC(18,0)
	DECLARE @numProfile NUMERIC(18,0)
	DECLARE @numItemClassification NUMERIC(18,0)

	SELECT 
		@numItemClassification=ISNULL(numItemClassification,0) 
	FROM 
		Item 
	WHERE 
		numDomainID=@numDomainID 
		AND numItemCode=@numItemCode

	IF ISNULL(@numDivisionID,0) > 0
	BEGIN
		SELECT 
			@numRelationship=numCompanyType,
			@numProfile=vcProfile
		FROM 
			DivisionMaster D                  
		JOIN 
			CompanyInfo C 
		ON 
			C.numCompanyId=D.numCompanyID                  
		WHERE 
			numDivisionID =@numDivisionID
	END
	ELSE IF ISNULL(@numSiteID,0) > 0
	BEGIN
		SELECT
			@numRelationship=ISNULL(numRelationshipId,0),
			@numProfile=ISNULL(numProfileId,0)
		FROM
			eCommerceDTL
		WHERE
			numSiteId=@numSiteID
	END

	SELECT
		PO.numProId
		,PO.vcProName
		,CONCAT(ISNULL(PO.vcShortDesc,'-'),(CASE WHEN ISNULL(PO.numOrderPromotionID,0) > 0 THEN ' (Coupon code required)' ELSE '' END)) AS vcShortDesc
		,CONCAT(ISNULL(PO.vcLongDesc,'-'),(CASE WHEN ISNULL(PO.numOrderPromotionID,0) > 0 THEN ' (Coupon code required)' ELSE '' END)) AS vcLongDesc
	FROM 
		PromotionOffer PO
	LEFT JOIN
		PromotionOffer POOrder
	ON
		PO.numOrderPromotionID = POOrder.numProId
	WHERE 
		PO.numDomainId=@numDomainID 
		AND ISNULL(PO.bitEnabled,0)=1 
		AND ISNULL(PO.IsOrderBasedPromotion,0) = 0
		AND 1 = (CASE 
					WHEN ISNULL(PO.numOrderPromotionID,0) > 0 
					THEN (CASE WHEN POOrder.bitNeverExpires = 1 THEN 1 ELSE (CASE WHEN GETUTCDATE()>=POOrder.dtValidFrom AND GETUTCDATE()<=POOrder.dtValidTo THEN 1 ELSE 0 END) END)
					ELSE (CASE WHEN PO.bitNeverExpires = 1 THEN 1 ELSE (CASE WHEN GETUTCDATE()>=PO.dtValidFrom AND GETUTCDATE()<=PO.dtValidTo THEN 1 ELSE 0 END) END)
				END)
		AND 1 = (CASE 
					WHEN ISNULL(PO.numOrderPromotionID,0) > 0 
					THEN
						(CASE WHEN (SELECT COUNT(*) FROM PromotionOfferOrganizations WHERE numProId=PO.numOrderPromotionID AND tintType=2 AND numRelationship=@numRelationship AND numProfile=@numProfile) > 0 THEN 1 ELSE 0 END)
					ELSE
						(CASE PO.tintCustomersBasedOn 
							WHEN 1 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferOrganizations WHERE numProId=PO.numProId AND tintType=1 AND numDivisionID=@numDivisionID) > 0 THEN 1 ELSE 0 END)
							WHEN 2 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferOrganizations WHERE numProId=PO.numProId AND tintType=2 AND numRelationship=@numRelationship AND numProfile=@numProfile) > 0 THEN 1 ELSE 0 END)
							WHEN 3 THEN 1
							ELSE 0
						END)
				END)
		AND 1 = (CASE 
					WHEN PO.tintOfferBasedOn = 1 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId=PO.numProId AND tintRecordType=5 AND tintType=1 AND numValue=@numItemCode) > 0 THEN 1 ELSE 0 END)
					WHEN PO.tintOfferBasedOn = 2 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId=PO.numProId AND tintRecordType=5 AND tintType=2 AND numValue=@numItemClassification) > 0 THEN 1 ELSE 0 END)
					WHEN PO.tintOfferBasedOn = 4 THEN 1 
					ELSE 0
				END)
	ORDER BY
		CASE 
			WHEN PO.tintOfferBasedOn = 1 AND PO.tintDiscoutBaseOn = 1 THEN 1
			WHEN PO.tintOfferBasedOn = 2 AND PO.tintDiscoutBaseOn = 1 THEN 2
			WHEN PO.tintOfferBasedOn = 1 AND PO.tintDiscoutBaseOn = 2 THEN 3
			WHEN PO.tintOfferBasedOn = 2 AND PO.tintDiscoutBaseOn = 2 THEN 4
			WHEN PO.tintOfferBasedOn = 1 AND PO.tintDiscoutBaseOn = 3 THEN 5
			WHEN PO.tintOfferBasedOn = 2 AND PO.tintDiscoutBaseOn = 3 THEN 6
		END
END
GO
