GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetProjectSummary')
DROP PROCEDURE USP_GetProjectSummary
GO
CREATE PROCEDURE [dbo].[USP_GetProjectSummary]
(
	@numDomainID NUMERIC(18,0)
	,@numProId NUMERIC(18,0)
	,@ClientTimeZoneOffset INT
)
AS 
BEGIN
SELECT P.vcProjectID,C.vcCompanyName,DT.vcDomainName AS vcByCompanyName,ST.vcTaskName,
CASE WHEN EXISTS (SELECT ID FROM StagePercentageDetailsTaskTimeLog WHERE numTaskID=ST.numTaskId AND tintAction=1)
				THEN DATEADD(MINUTE,-1 * @ClientTimeZoneOffset,(SELECT dtActionTime FROM StagePercentageDetailsTaskTimeLog WHERE numTaskID=ST.numTaskId AND tintAction=1))
				ELSE ''
			END AS dtStartDate
			,CASE WHEN EXISTS (SELECT ID FROM StagePercentageDetailsTaskTimeLog WHERE numTaskID=ST.numTaskId AND tintAction=4)
				THEN dbo.FormatedDateFromDate(DATEADD(MINUTE,-1 * @ClientTimeZoneOffset,(SELECT dtActionTime FROM StagePercentageDetailsTaskTimeLog WHERE numTaskID=ST.numTaskId AND tintAction=4)),@numDomainID)
				ELSE ''
			END AS dtFinishDate,
ltrim(right(convert(varchar(25),CASE WHEN EXISTS (SELECT ID FROM StagePercentageDetailsTaskTimeLog WHERE numTaskID=ST.numTaskId AND tintAction=1)
				THEN DATEADD(MINUTE,-1 * @ClientTimeZoneOffset,(SELECT dtActionTime FROM StagePercentageDetailsTaskTimeLog WHERE numTaskID=ST.numTaskId AND tintAction=1))
				ELSE ''
			END, 100), 7)) AS StartTiime
			,ltrim(right(convert(varchar(25),CASE WHEN EXISTS (SELECT ID FROM StagePercentageDetailsTaskTimeLog WHERE numTaskID=ST.numTaskId AND tintAction=4)
				THEN DATEADD(MINUTE,-1 * @ClientTimeZoneOffset,(SELECT dtActionTime FROM StagePercentageDetailsTaskTimeLog WHERE numTaskID=ST.numTaskId AND tintAction=4))
				ELSE ''
			END, 100), 7)) AS FinishTime,
			
			DATEDIFF(Minute,(CASE WHEN EXISTS (SELECT ID FROM StagePercentageDetailsTaskTimeLog WHERE numTaskID=ST.numTaskId AND tintAction=1)
				THEN DATEADD(MINUTE,-1 * @ClientTimeZoneOffset,(SELECT dtActionTime FROM StagePercentageDetailsTaskTimeLog WHERE numTaskID=ST.numTaskId AND tintAction=1))
				ELSE '' END),(CASE WHEN EXISTS (SELECT ID FROM StagePercentageDetailsTaskTimeLog WHERE numTaskID=ST.numTaskId AND tintAction=4)
				THEN DATEADD(MINUTE,-1 * @ClientTimeZoneOffset,(SELECT dtActionTime FROM StagePercentageDetailsTaskTimeLog WHERE numTaskID=ST.numTaskId AND tintAction=4))
				ELSE '' END)) As TaskDurationComplete,
				ADC.vcFirstName+' '+ADC.vcLastName AS AssignedTo,
				(((ST.numHours*60)+ST.numMinutes)-(DATEDIFF(Minute,(CASE WHEN EXISTS (SELECT ID FROM StagePercentageDetailsTaskTimeLog WHERE numTaskID=ST.numTaskId AND tintAction=1)
				THEN DATEADD(MINUTE,-1 * @ClientTimeZoneOffset,(SELECT dtActionTime FROM StagePercentageDetailsTaskTimeLog WHERE numTaskID=ST.numTaskId AND tintAction=1))
				ELSE '' END),(CASE WHEN EXISTS (SELECT ID FROM StagePercentageDetailsTaskTimeLog WHERE numTaskID=ST.numTaskId AND tintAction=4)
				THEN DATEADD(MINUTE,-1 * @ClientTimeZoneOffset,(SELECT dtActionTime FROM StagePercentageDetailsTaskTimeLog WHERE numTaskID=ST.numTaskId AND tintAction=4))
				ELSE '' END)) )) AS BalanceTime
				,dbo.GetTotalProgress(@numDomainID,@numProId,2,3,'',ST.numStageDetailsId) numTotalProgress,
				P.txtComments,
				ISNULL(P.bitDisplayTimeToExternalUsers,0) AS bitDisplayTimeToExternalUsers

FROM StagePercentageDetailsTask AS ST 
LEFT JOIN 
	ProjectsMaster AS P 
ON  ST.numProjectId=P.numProId 
LEFT JOIN DivisionMaster AS D 
ON P.numDivisionId=D.numDivisionID 
LEFT JOIN CompanyInfo AS C
ON D.numCompanyID=C.numCompanyId
LEFT JOIN Domain DT ON DT.numDomainId=P.numDomainId
LEFT JOIN AdditionalContactsInformation AS ADC ON ST.numAssignTo=ADC.numContactId
WHERE 
ST.numProjectId=@numProId AND ST.numDomainID=@numDomainID AND
(SELECT ISNULL((SELECT TOP 1 tintAction FROM StagePercentageDetailsTaskTimeLog WHERE numTaskID=ST.numTaskId ORDER BY dtActionTime DESC,ID DESC),0))=4
END 
GO