GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_OpportunityBizDocs_UpdateBizDocEditFields')
DROP PROCEDURE USP_OpportunityBizDocs_UpdateBizDocEditFields
GO
CREATE PROCEDURE [dbo].[USP_OpportunityBizDocs_UpdateBizDocEditFields]                          
	@numOppBizDocsId NUMERIC(18,0)
	,@numUserCntID NUMERIC(18,0)
	,@numBizDocTempID NUMERIC(18,0)
	,@dtFromDate DATETIME
	,@numShipVia NUMERIC(18,0)
	,@numSequenceId AS VARCHAR(50)
	,@vcTrackingNo AS VARCHAR(500)	
	,@vcRefOrderNo AS VARCHAR(300)
	,@numBizDocStatus NUMERIC(18,0)
AS 
BEGIN
	UPDATE
		OpportunityBizDocs
	SET
		numModifiedBy = @numUserCntID
		,dtModifiedDate = GETUTCDATE()
		,numBizDocTempID=@numBizDocTempID
		,dtFromDate=@dtFromDate
		,numShipVia=@numShipVia
		,numSequenceId=@numSequenceId
		,vcTrackingNo=@vcTrackingNo
		,vcRefOrderNo=@vcRefOrderNo
		,numBizDocStatus=@numBizDocStatus
	WHERE
		numOppBizDocsID = @numOppBizDocsId
END