SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_AssembledItem_Disassemble')
DROP PROCEDURE USP_AssembledItem_Disassemble
GO
CREATE PROCEDURE [dbo].[USP_AssembledItem_Disassemble]  
	@numDomainID NUMERIC(18,0),
	@numUserCntID AS NUMERIC(18,0),
	@ID NUMERIC(18,0),
	@numQty AS NUMERIC(18,0),
	@tintType AS TINYINT -- 1 - Assembled, 2 - Work Order
AS
BEGIN
BEGIN TRY
BEGIN TRANSACTION
	DECLARE @numItemCode AS NUMERIC(18,0)
	DECLARE @numWarehouseItemID AS NUMERIC(18,0)
	DECLARE @monAssemblyAverageCost AS DECIMAL(20,5)
	DECLARE @numRemainingQty AS NUMERIC(18,0)

	DECLARE @TMEP TABLE
	(
		RowNo INT IDENTITY(1,1),
		numItemCode NUMERIC(18,0),
		bitAssembly BIT,
		numWarehouseItemID NUMERIC(18,0),
		numQty FLOAT,
		monAverageCost DECIMAL(20,5)
	)

	DECLARE @i AS INT = 1
	DECLARE @COUNT AS INT
	DECLARE @TEMPItemCode AS NUMERIC(18,0)
	DECLARE @TEMPbitAssembly AS BIT
	DECLARE @TEMPWarehouseItemID AS NUMERIC(18,0)
	DECLARE @TEMPCurrentAverageCost AS DECIMAL(20,5)
	DECLARE @TEMPBuildAverageCost AS DECIMAL(20,5)
	DECLARE @TEMPToalOnHand AS FLOAT
	DECLARE @TEMPQty AS FLOAT
	DECLARE @Description AS VARCHAR(1000)

	IF @tintType = 1
	BEGIN
		SELECT @numItemCode=numItemCode,@numWarehouseItemID=numWarehouseItemID,@monAssemblyAverageCost=monAverageCost,@numRemainingQty=(numAssembledQty-ISNULL(numDisassembledQty,0)) FROM AssembledItem WHERE ID=@ID

		IF ISNULL((SELECT numOnHand FROM WareHouseItems WHERE numItemID=@numItemCode AND numWareHouseItemID=@numWarehouseItemID),0) >= @numQty AND  @numRemainingQty >= @numQty
		BEGIN
			INSERT INTO @TMEP 
			(
				numItemCode,
				bitAssembly,
				numWarehouseItemID,
				numQty,
				monAverageCost
			)
			SELECT
				AIC.numItemCode,
				bitAssembly,
				AIC.numWarehouseItemID,
				AIC.fltQtyRequiredForSingleBuild * @numQty,
				AIC.monAverageCost
			FROM 
				AssembledItemChilds AIC
			INNER JOIN
				Item
			ON
				AIC.numItemCode = Item.numItemCode
			WHERE
				numAssembledItemID = @ID

			SELECT @COUNT=COUNT(*) FROM @TMEP

			WHILE @i <= @COUNT
			BEGIN
				SELECT
					@TEMPItemCode =ISNULL(numItemCode,0),
					@TEMPbitAssembly=ISNULL(bitAssembly,0),
					@TEMPWarehouseItemID = ISNULL(numWarehouseItemID,0),
					@TEMPQty = ISNULL(numQty,0),
					@TEMPBuildAverageCost = ISNULL(monAverageCost,0)
				FROM
					@TMEP
				WHERE
					RowNo = @i

				SELECT @TEMPToalOnHand=SUM(numOnHand),@TEMPCurrentAverageCost=ISNULL(Item.monAverageCost,0) FROM WareHouseItems INNER JOIN Item ON WareHouseItems.numItemID=Item.numItemCode WHERE numItemID=@TEMPItemCode GROUP BY Item.numItemCode,Item.monAverageCost

				-- UPDATE AVERAGE COST
				UPDATE 
					Item
				SET
					monAverageCost = CAST(((@TEMPToalOnHand * @TEMPCurrentAverageCost) + (@TEMPQty * @TEMPBuildAverageCost)) / (@TEMPToalOnHand + @TEMPQty) AS DECIMAL(20,5))
				WHERE
					numItemCode = @TEMPItemCode


				-- INCREASE ON HAND QTY
				UPDATE 
					WareHouseItems
				SET    
					numOnHand= ISNULL(numOnHand,0) + @TEMPQty,
					dtModified = GETDATE() 
				WHERE   
					numWareHouseItemID = @TEMPWarehouseItemID

				--MAKE ENTRY IN WAREHOUSE TRACKING TABLE
				SET @Description= CONCAT('Assembly Item:',@numItemCode,' is disassembled ','(Qty:',@TEMPQty,')')

				EXEC dbo.USP_ManageWareHouseItems_Tracking
				@numWareHouseItemID = @TEMPWarehouseItemID, --  numeric(9, 0)
				@numReferenceID=@TEMPItemCode,
				@tintRefType = 1, --  tinyint,
				@vcDescription = @Description, --  varchar(100)
				@numModifiedBy = @numUserCntID,
				@numDomainID = @numDomainID

				SET @i = @i + 1
			END

			SELECT @TEMPToalOnHand=SUM(numOnHand),@TEMPCurrentAverageCost=ISNULL(Item.monAverageCost,0) FROM WareHouseItems INNER JOIN Item ON WareHouseItems.numItemID=Item.numItemCode WHERE numItemID=@numItemCode GROUP BY Item.numItemCode,Item.monAverageCost

			IF (@TEMPToalOnHand - @numQty) > 0
			BEGIN
				-- UPDATE AVERAGE COST
				UPDATE 
					Item
				SET
					monAverageCost = CAST(((@TEMPToalOnHand * @TEMPCurrentAverageCost) - (@numQty * @monAssemblyAverageCost)) / (@TEMPToalOnHand - @numQty) AS DECIMAL(20,5))
				WHERE
					numItemCode = @numItemCode
			END

			-- DECREASE ON HAND QTY OF ASSEMBLY
			UPDATE 
				WareHouseItems
			SET    
				numOnHand= ISNULL(numOnHand,0) - @numQty,
				dtModified = GETDATE() 
			WHERE   
				numWareHouseItemID = @numWarehouseItemID

			--MAKE ENTRY IN WAREHOUSE TRACKING TABLE
			SET @Description= CONCAT('Disassembled ','(Qty:',@numQty,')')

			EXEC dbo.USP_ManageWareHouseItems_Tracking
			@numWareHouseItemID = @numWarehouseItemID, --  numeric(9, 0)
			@numReferenceID=@numItemCode,
			@tintRefType = 1, --  tinyint
			@vcDescription = @Description, --  varchar(100)
			@numModifiedBy = @numUserCntID,
			@numDomainID = @numDomainID
		END
		ELSE
		BEGIN
			RAISERROR('INSUFFICIENT_ONHAND_QTY',16,1)
			RETURN
		END
	END
	ELSE IF @tintType = 2
	BEGIN
		SELECT @numItemCode=numItemCode,@numWarehouseItemID=numWarehouseItemID,@monAssemblyAverageCost=monAverageCost FROM WorkOrder WHERE numWOId=@ID

		IF ISNULL((SELECT numOnHand FROM WareHouseItems WHERE numItemID=@numItemCode AND numWareHouseItemID=@numWarehouseItemID),0) >= @numQty
		BEGIN
			INSERT INTO @TMEP 
			(
				numItemCode,
				bitAssembly,
				numWarehouseItemID,
				numQty,
				monAverageCost
			)
			SELECT
				WOD.numChildItemID,
				bitAssembly,
				WOD.numWarehouseItemID,
				WOD.[numQtyItemsReq_Orig] * @numQty,
				WOD.monAverageCost
			FROM 
				WorkOrderDetails WOD
			INNER JOIN
				Item
			ON
				WOD.numChildItemID = Item.numItemCode
			WHERE
				WOD.numWOId = @ID

			SELECT @COUNT=COUNT(*) FROM @TMEP

			WHILE @i <= @COUNT
			BEGIN
				SELECT
					@TEMPItemCode =ISNULL(numItemCode,0),
					@TEMPbitAssembly=ISNULL(bitAssembly,0),
					@TEMPWarehouseItemID = ISNULL(numWarehouseItemID,0),
					@TEMPQty = ISNULL(numQty,0),
					@TEMPBuildAverageCost = ISNULL(monAverageCost,0)
				FROM
					@TMEP
				WHERE
					RowNo = @i

				SELECT @TEMPToalOnHand=SUM(numOnHand),@TEMPCurrentAverageCost=ISNULL(Item.monAverageCost,0) FROM WareHouseItems INNER JOIN Item ON WareHouseItems.numItemID=Item.numItemCode WHERE numItemID=@TEMPItemCode GROUP BY Item.numItemCode,Item.monAverageCost

				-- UPDATE AVERAGE COST
				UPDATE 
					Item
				SET
					monAverageCost = CAST(((@TEMPToalOnHand * @TEMPCurrentAverageCost) + (@TEMPQty * @TEMPBuildAverageCost)) / (@TEMPToalOnHand + @TEMPQty) AS DECIMAL(20,5))
				WHERE
					numItemCode = @TEMPItemCode


				-- INCREASE ON HAND QTY
				UPDATE 
					WareHouseItems
				SET    
					numOnHand= ISNULL(numOnHand,0) + @TEMPQty,
					dtModified = GETDATE() 
				WHERE   
					numWareHouseItemID = @TEMPWarehouseItemID

				--MAKE ENTRY IN WAREHOUSE TRACKING TABLE
				SET @Description= CONCAT('Work Order of Assembly Item:',@numItemCode,' is disassembled ','(Qty:',@TEMPQty,')')

				EXEC dbo.USP_ManageWareHouseItems_Tracking
				@numWareHouseItemID = @TEMPWarehouseItemID, --  numeric(9, 0)
				@numReferenceID=@TEMPItemCode,
				@tintRefType = 1, --  tinyint,
				@vcDescription = @Description, --  varchar(100)
				@numModifiedBy = @numUserCntID,
				@numDomainID = @numDomainID

				SET @i = @i + 1
			END

			SELECT @TEMPToalOnHand=SUM(numOnHand),@TEMPCurrentAverageCost=ISNULL(Item.monAverageCost,0) FROM WareHouseItems INNER JOIN Item ON WareHouseItems.numItemID=Item.numItemCode WHERE numItemID=@numItemCode GROUP BY Item.numItemCode,Item.monAverageCost


			IF (@TEMPToalOnHand - @numQty) > 0
			BEGIN
				-- UPDATE AVERAGE COST
				UPDATE 
					Item
				SET
					monAverageCost = CAST(((@TEMPToalOnHand * @TEMPCurrentAverageCost) - (@numQty * @monAssemblyAverageCost)) / (@TEMPToalOnHand - @numQty) AS DECIMAL(20,5))
				WHERE
					numItemCode = @numItemCode
			END

			-- DECREASE ON HAND QTY OF ASSEMBLY
			UPDATE 
				WareHouseItems
			SET    
				numOnHand= ISNULL(numOnHand,0) - @numQty,
				dtModified = GETDATE() 
			WHERE   
				numWareHouseItemID = @numWarehouseItemID

			--MAKE ENTRY IN WAREHOUSE TRACKING TABLE
			SET @Description= CONCAT('Work Order Disassembled ','(Qty:',@numQty,')')

			EXEC dbo.USP_ManageWareHouseItems_Tracking
			@numWareHouseItemID = @numWarehouseItemID, --  numeric(9, 0)
			@numReferenceID=@numItemCode,
			@tintRefType = 1, --  tinyint
			@vcDescription = @Description, --  varchar(100)
			@numModifiedBy = @numUserCntID,
			@numDomainID = @numDomainID
		END
		ELSE
		BEGIN
			RAISERROR('INSUFFICIENT_ONHAND_QTY',16,1)
			RETURN
		END
	END
COMMIT
END TRY
BEGIN CATCH
	DECLARE @ErrorMessage NVARCHAR(4000)
	DECLARE @ErrorNumber INT
	DECLARE @ErrorSeverity INT
	DECLARE @ErrorState INT
	DECLARE @ErrorLine INT
	DECLARE @ErrorProcedure NVARCHAR(200)

	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;

	SELECT 
		@ErrorMessage = ERROR_MESSAGE(),
		@ErrorNumber = ERROR_NUMBER(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE(),
		@ErrorLine = ERROR_LINE(),
		@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

	RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);
END CATCH
END  