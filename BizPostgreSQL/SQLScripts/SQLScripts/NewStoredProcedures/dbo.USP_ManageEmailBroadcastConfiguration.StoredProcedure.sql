
GO
/****** Object:  StoredProcedure [dbo].[USP_ManageEmailBroadcastConfiguration]    Script Date: 30/07/2012 17:47:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ManageEmailBroadcastConfiguration')
	DROP PROCEDURE USP_ManageEmailBroadcastConfiguration
GO

CREATE PROCEDURE [dbo].[USP_ManageEmailBroadcastConfiguration]
	@numConfigurationId numeric(18, 0) output,
	@vcFromName nvarchar(50),
	@vcDomainName nvarchar(50),
	@vcDkimPublicKey nvarchar(1000),
	@vcDkimPrivateKey nvarchar(1000),
	@bitEnabled bit,
	@bitSpfPass bit,
	@bitDkimPass bit,
	@bitDomainKeyPass bit,
	@bitSenderIdPass bit,
	@numDomainId numeric(18, 0)
AS

IF EXISTS(SELECT * FROM [dbo].[EmailBroadcastConfiguration] WHERE [numConfigurationId] = @numConfigurationId)
BEGIN
	UPDATE [dbo].[EmailBroadcastConfiguration] SET
		[vcFromName] = @vcFromName,
		[vcDomainName] = @vcDomainName,
		[vcDkimPublicKey] = @vcDkimPublicKey,
		[vcDkimPrivateKey] = @vcDkimPrivateKey,
		[bitEnabled] = @bitEnabled,
		[bitSpfPass] = @bitSpfPass,
		[bitDkimPass] = @bitDkimPass,
		[bitDomainKeyPass] = @bitDomainKeyPass,
		[bitSenderIdPass] = @bitSenderIdPass		
	WHERE
		[numConfigurationID] = @numConfigurationID
END
ELSE
BEGIN
	INSERT INTO [dbo].[EmailBroadcastConfiguration] (
		[vcFromName],
		[vcDomainName],
		[vcDkimPublicKey],
		[vcDkimPrivateKey],
		[bitEnabled],
		[bitSpfPass],
		[bitDkimPass],
		[bitDomainKeyPass],
		[bitSenderIdPass],
		[numDomainId]
	) VALUES (
		@vcFromName,
		@vcDomainName,
		@vcDkimPublicKey,
		@vcDkimPrivateKey,
		@bitEnabled,
		@bitSpfPass,
		@bitDkimPass,
		@bitDomainKeyPass,
		@bitSenderIdPass,
		@numDomainId
	)
	 Select @numConfigurationId = @@identity
END

--declare @p1 bigint set @p1=0 exec USP_ManageEmailBroadcastConfiguration @numConfigurationId=@p1 output,@vcFromName=N'info',@vcDomainName=N'bizautomationmail.com',@vcDkimPublicKey=N'-----BEGIN PUBLIC KEY',@vcDkimPrivateKey=N'-----BEGIN RSA PRIVATE KEY-----END RSA PRIVATE KEY-----',@bitEnabled=1,@bitSpfPass=1,@bitDkimPass=1,@bitDomainKeyPass=1,@bitSenderIdPass=0,@numDomainId=1 select @p1
--declare @p1 bigint set @p1=0 exec USP_ManageEmailBroadcastConfiguration @numConfigurationId=@p1 output,@vcFromName=N'info',@vcDomainName=N'bizautomationmail.com',@vcDkimPublicKey=N'-----BEGIN PUBLIC KEY',@vcDkimPrivateKey=N'-----BEGIN RSA PRIVATE KEY-----END RSA PRIVATE KEY-----',@bitEnabled=1,@bitSpfPass=1,@bitDkimPass=1,@bitDomainKeyPass=1,@bitSenderIdPass=0,@numDomainId=159 select @p1
--select * from  EmailBroadcastConfiguration




