SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ManageVendorShipmentMethod')
DROP PROCEDURE USP_ManageVendorShipmentMethod
GO
CREATE PROCEDURE [dbo].[USP_ManageVendorShipmentMethod] 
( 
	@numDomainID as numeric(18,0)=0,    
	@numVendorid AS NUMERIC(18,0)=0,
	@numAddressID AS NUMERIC(18,0)=0,
	@numWarehouseID AS NUMERIC(18,0) = 0,
	@str AS TEXT = '',
	@bitPrimary AS bit=0
)
AS 
BEGIN
DECLARE @hDoc INT
	EXEC sp_xml_preparedocument @hDoc OUTPUT, @str

	DELETE FROM VendorShipmentMethod WHERE numDomainID=@numDomainID AND numVendorid=@numVendorid AND numAddressID=@numAddressID AND numWarehouseID=@numWarehouseID
	
	UPDATE VendorShipmentMethod SET bitPrimary=0 WHERE numDomainID=@numDomainID AND numVendorid=@numVendorid AND numWarehouseID=@numWarehouseID

	INSERT INTO VendorShipmentMethod (
		numDomainID,numVendorid,numAddressID,numWarehouseID,bitPrimary,
		numListItemID,
		numListValue,
		bitPreferredMethod		
	) 
	SELECT 
		@numDomainID,@numVendorid,@numAddressID,@numWarehouseID,@bitPrimary,
		X.numListItemID,
		X.numListValue,
		ISNULL(X.bitPreferredMethod,0)
		FROM 	( SELECT    *
          FROM      OPENXML (@hDoc, '/NewDataSet/ShipmentMethod',2) WITH (numListItemID NUMERIC(9), numListValue INT, bitPreferredMethod BIT)
        ) X

	EXEC sp_xml_removedocument @hDoc
END
GO