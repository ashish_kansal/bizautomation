        
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_ManageBankStatementTransaction')
	DROP PROCEDURE usp_ManageBankStatementTransaction
GO
/****** Added By : Joseph ******/
/****** Saves Statements Transactions to Table BankStatementTransactions******/

CREATE PROCEDURE [dbo].[usp_ManageBankStatementTransaction]
	@numTransactionID numeric(18, 0),
	@numStatementID numeric(18, 0),
	@monAmount DECIMAL(20,5),
	@vcCheckNumber varchar(50),
	@dtDatePosted datetime,
	@vcFITransactionID varchar(50),
	@vcPayeeName varchar(50),
	@vcMemo varchar(100),
	@vcTxType varchar(50),
	@numAccountID numeric(18, 0),
	@numDomainID numeric(18, 0)
AS

SET NOCOUNT ON
  DECLARE @Check as integer
IF EXISTS(SELECT [numTransactionID] FROM [dbo].[BankStatementTransactions] WHERE [numTransactionID] = @numTransactionID AND numAccountID=@numAccountID)
BEGIN
	UPDATE [dbo].[BankStatementTransactions] SET
		[numStatementID] = @numStatementID,
		[monAmount] = @monAmount,
		[vcCheckNumber] = @vcCheckNumber,
		[dtDatePosted] = @dtDatePosted,
		[vcFITransactionID] = @vcFITransactionID,
		[vcPayeeName] = @vcPayeeName,
		[vcMemo] = @vcMemo,
		[vcTxType] = @vcTxType,
		[numAccountID] = @numAccountID,
		[numDomainID] = @numDomainID,
		[dtModifiedDate] = GETDATE()
	WHERE
		[numTransactionID] = @numTransactionID
END
ELSE
BEGIN
SELECT @Check = COUNT(*) FROM   [BankStatementTransactions] WHERE  vcFITransactionID = @vcFITransactionID AND numAccountID= @numAccountID
IF @Check =0  
BEGIN 
	INSERT INTO [dbo].[BankStatementTransactions] (
		[numStatementID],
		[monAmount],
		[vcCheckNumber],
		[dtDatePosted],
		[vcFITransactionID],
		[vcPayeeName],
		[vcMemo],
		[vcTxType],
		[numAccountID],
		[numDomainID],
		[dtCreatedDate],
		bitIsActive
	) VALUES (
		@numStatementID,
		@monAmount,
		@vcCheckNumber,
		@dtDatePosted,
		@vcFITransactionID,
		@vcPayeeName,
		@vcMemo,
		@vcTxType,
		@numAccountID,
		@numDomainID,
		GETDATE(),
		1
	)
END
END


GO