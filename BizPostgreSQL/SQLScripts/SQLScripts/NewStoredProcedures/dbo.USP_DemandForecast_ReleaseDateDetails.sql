GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_DemandForecast_ReleaseDateDetails')
DROP PROCEDURE dbo.USP_DemandForecast_ReleaseDateDetails
GO
CREATE PROCEDURE [dbo].[USP_DemandForecast_ReleaseDateDetails]
	@numDomainID NUMERIC(18,0)
	,@numItemCode NUMERIC(18,0)
	,@numWarehouseItemID NUMERIC(18,0)
	,@dtReleaseDate DATE
	,@CurrentPage INT
    ,@PageSize INT
	,@ClientTimeZoneOffset INT
AS 
BEGIN
	DECLARE @TEMP TABLE
	(
		numOppID NUMERIC(18,0)
		,numDivisionID NUMERIC(18,0)
		,vcCompanyName VARCHAR(300)
		,numWOID NUMERIC(18,0)
		,dtCreatedDate DATETIME
		,vcOppName VARCHAR(500)
		,numOrderedQty FLOAT
		,numQtyToShip FLOAT
		,dtReleaseDate VARCHAR(50)
	)

	INSERT INTO @TEMP
	(
		numOppID
		,numDivisionID
		,vcCompanyName
		,numWOID
		,dtCreatedDate
		,vcOppName
		,numOrderedQty
		,numQtyToShip
		,dtReleaseDate
	)
	SELECT
		OpportunityMaster.numOppId
		,OpportunityMaster.numDivisionId
		,ISNULL(CompanyInfo.vcCompanyName,'')
		,0
		,OpportunityMaster.bintCreatedDate
		,CONCAT('<a href="#" onclick="return openInNewTab(',OpportunityMaster.numOppId,');">',OpportunityMaster.vcPOppName,'</a>')
		,OpportunityItems.numUnitHour
		,OpportunityItems.numUnitHour - ISNULL(numQtyShipped,0)
		,dbo.FormatedDateFromDate(ISNULL(OpportunityItems.ItemReleaseDate,OpportunityMaster.dtReleaseDate),@numDomainID)
	FROM
		OpportunityMaster
	INNER JOIN 
		DivisionMaster
	ON
		OpportunityMaster.numDivisionId=DivisionMaster.numDivisionID
	INNER JOIN 
		CompanyInfo
	ON
		DivisionMaster.numCompanyID=CompanyInfo.numCompanyId
	INNER JOIN
		OpportunityItems
	ON
		OpportunityMaster.numOppId = OpportunityItems.numOppId
	INNER JOIN
		Item
	ON
		OpportunityItems.numItemCode = Item.numItemCode
	WHERE
		OpportunityMaster.numDomainId=@numDomainID
		AND OpportunityMaster.tintOppType=1
		AND OpportunityMaster.tintOppStatus=1
		AND ISNULL(OpportunityMaster.tintshipped,0)=0
		AND (ISNULL(OpportunityItems.numUnitHour,0)  - ISNULL(OpportunityItems.numQtyShipped,0)) > 0
		AND ISNULL(OpportunityItems.bitDropShip,0) = 0
		AND ISNULL(Item.IsArchieve,0) = 0
		AND ISNULL(Item.bitAssembly,0) = 0
		AND ISNULL(Item.bitKitParent,0) = 0
		AND (OpportunityItems.ItemReleaseDate IS NOT NULL OR OpportunityMaster.dtReleaseDate IS NOT NULL)
		AND OpportunityItems.numItemCode=@numItemCode
		AND OpportunityItems.numWarehouseItmsID=@numWarehouseItemID
		AND ISNULL(OpportunityItems.ItemReleaseDate,OpportunityMaster.dtReleaseDate) = @dtReleaseDate

	-- USED IN KIT
	INSERT INTO @TEMP
	(
		numOppID
		,numDivisionID
		,vcCompanyName
		,numWOID
		,dtCreatedDate
		,vcOppName
		,numOrderedQty
		,numQtyToShip
		,dtReleaseDate
	)
	SELECT
		OM.numOppId
		,OM.numDivisionId
		,ISNULL(CompanyInfo.vcCompanyName,'')
		,0
		,OM.bintCreatedDate
		,CONCAT('<a href="#" onclick="return openInNewTab(',OM.numOppId,');">',OM.vcPOppName,' (Kit Memeber)','</a>')
		,OKI.numQtyItemsReq
		,OKI.numQtyItemsReq - ISNULL(OKI.numQtyShipped,0)
		,dbo.FormatedDateFromDate(ISNULL(OI.ItemReleaseDate,OM.dtReleaseDate),@numDomainID)
	FROM
		OpportunityMaster OM
	INNER JOIN 
		DivisionMaster
	ON
		OM.numDivisionId=DivisionMaster.numDivisionID
	INNER JOIN 
		CompanyInfo
	ON
		DivisionMaster.numCompanyID=CompanyInfo.numCompanyId
	INNER JOIN
		OpportunityItems OI
	ON
		OM.numOppId = OI.numOppId
	INNER JOIN
		OpportunityKitItems OKI
	ON
		OI.numoppitemtCode=OKI.numOppItemID
	INNER JOIN
		Item
	ON
		OKI.numChildItemID = Item.numItemCode
	INNER JOIN
		WareHouseItems WI
	ON
		OKI.numWareHouseItemId = WI.numWareHouseItemID
	WHERE
		OM.numDomainId=@numDomainID
		AND OM.tintOppType=1
		AND OM.tintOppStatus=1
		AND ISNULL(OM.tintshipped,0)=0
		AND ISNULL(OI.bitDropShip,0) = 0
		AND ISNULL(OI.bitWorkOrder,0) = 0
		AND ISNULL(Item.IsArchieve,0) = 0
		AND ISNULL(Item.bitAssembly,0) = 0
		AND ISNULL(Item.bitKitParent,0) = 0
		AND (ISNULL(OKI.numQtyItemsReq,0) - ISNULL(OKI.numQtyShipped,0)) > 0
		AND (OM.dtReleaseDate IS NOT NULL OR OI.ItemReleaseDate IS NOT NULL)
		AND OKI.numChildItemID=@numItemCode
		AND OKI.numWareHouseItemId=@numWarehouseItemID
		AND ISNULL(OI.ItemReleaseDate,OM.dtReleaseDate) = @dtReleaseDate

	-- USED IN KIT WITHIN KIT
	INSERT INTO @TEMP
	(
		numOppID
		,numDivisionID
		,vcCompanyName
		,numWOID
		,dtCreatedDate
		,vcOppName
		,numOrderedQty
		,numQtyToShip
		,dtReleaseDate
	)
	SELECT
		OM.numOppId
		,OM.numDivisionId
		,ISNULL(CompanyInfo.vcCompanyName,'')
		,0
		,OM.bintCreatedDate
		,CONCAT('<a href="#" onclick="return openInNewTab(',OM.numOppId,');">',OM.vcPOppName,' (Kit Memeber)','</a>')
		,OKCI.numQtyItemsReq
		,OKCI.numQtyItemsReq - ISNULL(OKCI.numQtyShipped,0)
		,dbo.FormatedDateFromDate(ISNULL(OI.ItemReleaseDate,OM.dtReleaseDate),@numDomainID)
	FROM
		OpportunityMaster OM
	INNER JOIN 
		DivisionMaster
	ON
		OM.numDivisionId=DivisionMaster.numDivisionID
	INNER JOIN 
		CompanyInfo
	ON
		DivisionMaster.numCompanyID=CompanyInfo.numCompanyId
	INNER JOIN
		OpportunityItems OI
	ON
		OM.numOppId = OI.numOppId
	INNER JOIN
		OpportunityKitChildItems OKCI
	ON
		OI.numoppitemtCode = OKCI.numOppItemID
	INNER JOIN
		Item
	ON
		OKCI.numItemID = Item.numItemCode
	INNER JOIN
		WareHouseItems WI
	ON
		OKCI.numWareHouseItemId = WI.numWareHouseItemID
	WHERE
		OM.numDomainId=@numDomainID
		AND OM.tintOppType=1
		AND OM.tintOppStatus=1
		AND ISNULL(OM.tintshipped,0)=0
		AND ISNULL(OI.bitDropShip,0) = 0
		AND ISNULL(OI.bitWorkOrder,0) = 0
		AND ISNULL(Item.IsArchieve,0) = 0
		AND ISNULL(Item.bitAssembly,0) = 0
		AND ISNULL(Item.bitKitParent,0) = 0
		AND (ISNULL(OKCI.numQtyItemsReq,0) - ISNULL(OKCI.numQtyShipped,0)) > 0
		AND (OM.dtReleaseDate IS NOT NULL OR OI.ItemReleaseDate IS NOT NULL)
		AND OKCI.numItemID=@numItemCode
		AND OKCI.numWareHouseItemId=@numWarehouseItemID
		AND ISNULL(OI.ItemReleaseDate,OM.dtReleaseDate) = @dtReleaseDate
		
	-- USED IN WORK ORDER
	INSERT INTO @TEMP
	(
		numOppID
		,numDivisionID
		,vcCompanyName
		,numWOID
		,dtCreatedDate
		,vcOppName
		,numOrderedQty
		,numQtyToShip
		,dtReleaseDate
	)
	SELECT
		OM.numOppId
		,ISNULL(OM.numDivisionId,0)
		,ISNULL(CompanyInfo.vcCompanyName,'')
		,WO.numWOId
		,CASE WHEN OM.numOppId IS NOT NULL THEN OM.bintCreatedDate ELSE WO.bintCreatedDate END
		,CONCAT(CASE WHEN OM.numOppId IS NOT NULL THEN OM.vcPOppName ELSE 'Work Order' END,' (Assembly Memeber)') 
		,WOD.numQtyItemsReq
		,WOD.numQtyItemsReq 
		,CASE WHEN OM.numOppId IS NOT NULL THEN dbo.FormatedDateFromDate(ISNULL(OI.ItemReleaseDate,OM.dtReleaseDate),@numDomainID) ELSE dbo.FormatedDateFromDate(WO.dtmEndDate,@numDomainID) END
	FROM
		WorkOrderDetails WOD
	INNER JOIN
		Item
	ON
		WOD.numChildItemID = Item.numItemCode
	INNER JOIN
		WareHouseItems WI
	ON
		WOD.numWareHouseItemId = WI.numWareHouseItemID
	INNER JOIN
		WorkOrder WO
	ON
		WOD.numWOId=WO.numWOId
	LEFT JOIN
		OpportunityItems OI
	ON
		WO.numOppItemID = OI.numoppitemtCode
	LEFT JOIN
		OpportunityMaster OM
	ON
		OI.numOppId=OM.numOppId
	LEFT JOIN 
		DivisionMaster
	ON
		OM.numDivisionId=DivisionMaster.numDivisionID
	LEFT JOIN 
		CompanyInfo
	ON
		DivisionMaster.numCompanyID=CompanyInfo.numCompanyId
	WHERE
		WO.numDomainID=@numDomainID
		AND WO.numWOStatus <> 23184 -- NOT COMPLETED
		AND ISNULL(Item.IsArchieve,0) = 0
		AND ISNULL(Item.bitAssembly,0) = 0
		AND ISNULL(Item.bitKitParent,0) = 0
		AND ISNULL(WOD.numQtyItemsReq,0) > 0
		AND (OM.dtReleaseDate IS NOT NULL OR OI.ItemReleaseDate IS NOT NULL OR WO.dtmEndDate IS NOT NULL)
		AND 1 = (CASE WHEN OM.numOppId IS NOT NULL THEN (CASE WHEN OM.tintOppStatus=1 THEN 1 ELSE 0 END) ELSE 1 END)
		AND WOD.numChildItemID = @numItemCode
		AND WOD.numWareHouseItemId=@numWarehouseItemID
		AND 1 = (CASE 
				WHEN OI.numoppitemtCode IS NOT NULL 
				THEN (CASE 
						WHEN ISNULL(OI.ItemReleaseDate,OM.dtReleaseDate) = @dtReleaseDate
						THEN 1 
						ELSE 0 
					END)
				ELSE 
					(CASE 
					WHEN CAST(WO.dtmEndDate AS DATE) = @dtReleaseDate
					THEN 1 
					ELSE 0 
					END)
				END)

	SELECT * FROM @TEMP ORDER BY dtCreatedDate
END
GO