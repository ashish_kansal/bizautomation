GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_CalculateCommissionUnPaidBizDoc')
DROP PROCEDURE USP_CalculateCommissionUnPaidBizDoc
GO
CREATE PROCEDURE [dbo].[USP_CalculateCommissionUnPaidBizDoc]
	-- Add the parameters for the stored procedure here
	 @numComPayPeriodID AS NUMERIC(18,0),
	 @numDomainID AS NUMERIC(18,0),
	 @ClientTimeZoneOffset INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
SET NOCOUNT ON;

BEGIN TRY
BEGIN TRANSACTION
	DECLARE @dtPayStart DATE 
	DECLARE @dtPayEnd DATE

	SELECT 
		@dtPayStart=dtStart
		,@dtPayEnd=dtEnd 
	FROM 
		CommissionPayPeriod 
	WHERE 
		numComPayPeriodID = @numComPayPeriodID

	-- DELETE UNPAID COMMISSION ENTRIES BETWEEN PAY PERIOD DATE RANGE
	DELETE FROM 
		BizDocComission 
	WHERE 
		numDomainId=@numDomainID 
		AND ISNULL(bitCommisionPaid,0)=0 
		AND numOppBizDocItemID IN (SELECT 
										ISNULL(numOppBizDocItemID,0)
									FROM 
										OpportunityBizDocs OppBiz
									INNER JOIN
										OpportunityMaster OppM
									ON
										OppBiz.numOppID = OppM.numOppID
									INNER JOIN
										OpportunityBizDocItems OppBizItems
									ON
										OppBiz.numOppBizDocsId = OppBizItems.numOppBizDocID
									WHERE
										OppM.numDomainId = @numDomainID 
										AND OppM.tintOppType = 1
										AND OppM.tintOppStatus = 1
										AND OppBiz.bitAuthoritativeBizDocs = 1
										AND ISNULL(OppBiz.monAmountPaid,0) < OppBiz.monDealAmount
										AND (OppBiz.dtCreatedDate IS NOT NULL AND (CAST(DATEADD(MINUTE,-@ClientTimeZoneOffset,OppBiz.dtCreatedDate) AS DATE) BETWEEN @dtPayStart AND @dtPayEnd)))
 
	--GET COMMISSION RULE OF DOMAIN
	DECLARE @TempCommissionRule TABLE
	(
		ID INT IDENTITY(1,1),
		numComRuleID NUMERIC(18,0),
		tintComBasedOn TINYINT,
		tintComType TINYINT,
		tintComAppliesTo TINYINT,
		tintComOrgType TINYINT,
		tintAssignTo TINYINT
	)

	INSERT INTO
		@TempCommissionRule
	SELECT
		CR.numComRuleID,
		CR.tintComBasedOn,
		CR.tintComType,
		CR.tintComAppliesTo,
		CR.tintComOrgType,
		CR.tintAssignTo
	FROM
		CommissionRules CR
	LEFT JOIN
		PriceBookPriorities PBP
	ON	
		CR.tintComAppliesTo = PBP.Step2Value 
		AND CR.tintComOrgType = PBP.Step3Value
	WHERE
		CR.numDomainID = @numDomainID
		AND ISNULL(PBP.[Priority],0) > 0
	ORDER BY
		ISNULL(PBP.[Priority],0) ASC

	--GET COMMISSION GLOBAL SETTINGS
	DECLARE @numDiscountServiceItemID NUMERIC(18,0)
	DECLARE @numShippingServiceItemID NUMERIC(18,0)
	DECLARE @tintCommissionType TINYINT
	DECLARE @bitIncludeTaxAndShippingInCommission BIT
	DECLARE @bitCommissionBasedOn BIT
	DECLARE @tintCommissionBasedOn TINYINT

	SELECT 
		@numDiscountServiceItemID=numDiscountServiceItemID, 
		@numShippingServiceItemID = numShippingServiceItemID, 
		@tintCommissionType=tintCommissionType, 
		@bitIncludeTaxAndShippingInCommission=bitIncludeTaxAndShippingInCommission
		,@tintCommissionBasedOn=ISNULL(tintCommissionBasedOn,1)
		,@bitCommissionBasedOn=ISNULL(bitCommissionBasedOn,0) 
	FROM 
		Domain 
	WHERE 
		numDomainID=@numDomainID

	--TEMPORARY TABLES TO HOLD DATA
	DECLARE @TempBizCommission TABLE
	(
		numUserCntID NUMERIC(18,0),
		numOppID NUMERIC(18,0),
		numOppBizDocID NUMERIC(18,0),
		numOppBizDocItemID NUMERIC(18,0),
		monCommission DECIMAL(20,5),
		numComRuleID NUMERIC(18,0),
		tintComType TINYINT,
		tintComBasedOn TINYINT,
		decCommission FLOAT,
		tintAssignTo TINYINT
	)

	DECLARE @TABLEPAID TABLE
	(
		ID INT IDENTITY(1,1),
		numDivisionID NUMERIC(18,0),
		numRelationship NUMERIC(18,0),
		numProfile NUMERIC(18,0),
		numAssignedTo NUMERIC(18,0),
		numRecordOwner NUMERIC(18,0),
		numItemCode NUMERIC(18,0),
		numItemClassification NUMERIC(18,0),
		numOppID NUMERIC(18,0),
		numOppBizDocID NUMERIC(18,0),
		numOppBizDocItemID NUMERIC(18,0),
		numUnitHour FLOAT,
		monTotAmount DECIMAL(20,5),
		monVendorCost DECIMAL(20,5),
		monAvgCost DECIMAL(20,5),
		numPartner NUMERIC(18,0),
		numPOID NUMERIC(18,0),
		numPOItemID NUMERIC(18,0),
		monPOCost DECIMAL(20,5)
	)

	-- GET FULLY PAID INVOICES WITHIN PAY PERIOD
	INSERT INTO 
		@TABLEPAID
	SELECT 
		OppM.numDivisionID,
		ISNULL(CompanyInfo.numCompanyType,0),
		ISNULL(CompanyInfo.vcProfile,0),
		OppM.numAssignedTo,
		OppM.numRecOwner,
		OppBizItems.numItemCode,
		Item.numItemClassification,
		OppM.numOppID,
		OppBizItems.numOppBizDocID,
		OppBizItems.numOppBizDocItemID,
		ISNULL(OppBizItems.numUnitHour,0),
		(CASE WHEN ISNULL(OppM.fltExchangeRate,0)=0 OR ISNULL(OppM.fltExchangeRate,0)=1 THEN ISNULL(OppBizItems.monTotAmount,0) ELSE ROUND(ISNULL(OppBizItems.monTotAmount,0) * OppM.fltExchangeRate,2) END),
		(ISNULL(OppMItems.monVendorCost,0) * ISNULL(OppBizItems.numUnitHour,0)),
		(ISNULL(OppMItems.monAvgCost,0) * ISNULL(OppBizItems.numUnitHour,0)),
		OppM.numPartner,
		ISNULL(TEMPMatchedPO.numOppId,TEMPPO.numOppId),
		ISNULL(TEMPMatchedPO.numoppitemtCode,TEMPPO.numoppitemtCode),
		ISNULL(ISNULL(TEMPMatchedPO.monPrice,TEMPPO.monPrice),0)
	FROM 
		OpportunityBizDocs OppBiz
	INNER JOIN
		OpportunityMaster OppM
	ON
		OppBiz.numOppID = OppM.numOppID
	INNER JOIN
		DivisionMaster 
	ON
		OppM.numDivisionID = DivisionMaster.numDivisionID
	INNER JOIN
		CompanyInfo
	ON
		DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
	INNER JOIN
		OpportunityBizDocItems OppBizItems
	ON
		OppBiz.numOppBizDocsId = OppBizItems.numOppBizDocID
	INNER JOIN
		OpportunityItems OppMItems 
	ON
		OppBizItems.numOppItemID = OppMItems.numoppitemtCode
	INNER JOIN
		Item
	ON
		OppBizItems.numItemCode = Item.numItemCode
	OUTER APPLY
	(
		SELECT TOP 1
			OMInner.numOppID
			,OMInner.vcPOppName
			,OIInner.numoppitemtCode
			,OIInner.monPrice
		FROM
			SalesOrderLineItemsPOLinking SOLIPL
		INNER JOIN
			OpportunityMaster OMInner
		ON
			SOLIPL.numPurchaseOrderID = OMInner.numOppId
		INNER JOIN
			OpportunityItems OIInner
		ON
			OMInner.numOppId = OIInner.numOppId
			AND SOLIPL.numPurchaseOrderItemID = OIInner.numoppitemtCode
		WHERE
			SOLIPL.numSalesOrderID = OppM.numOppId
			AND SOLIPL.numSalesOrderItemID = OppMItems.numoppitemtCode
	) TEMPMatchedPO
	OUTER APPLY
	(
		SELECT TOP 1
			OMInner.numOppID
			,OMInner.vcPOppName
			,OIInner.numoppitemtCode
			,OIInner.monPrice
		FROM
			OpportunityLinking OL
		INNER JOIN
			OpportunityMaster OMInner
		ON
			OL.numChildOppID = OMInner.numOppId
		INNER JOIN
			OpportunityItems OIInner
		ON
			OMInner.numOppId = OIInner.numOppId
		WHERE
			OL.numParentOppID = OppM.numOppId
			AND OIInner.numItemCode = OppMItems.numItemCode
			AND OIInner.vcAttrValues = OppMItems.vcAttrValues
	) TEMPPO
	WHERE
		OppM.numDomainId = @numDomainID 
		AND OppM.tintOppType = 1
		AND OppM.tintOppStatus = 1
		AND OppBiz.bitAuthoritativeBizDocs = 1
		AND ISNULL(OppBiz.monAmountPaid,0) < OppBiz.monDealAmount
		AND (OppBiz.dtCreatedDate IS NOT NULL AND (CAST(DATEADD(MINUTE,-@ClientTimeZoneOffset,OppBiz.dtCreatedDate) AS DATE) BETWEEN @dtPayStart AND @dtPayEnd))
		AND OppBizItems.numItemCode NOT IN ( @numDiscountServiceItemID )  /*DO NOT INCLUDE DISCOUNT ITEM*/
		AND 1 = (
					CASE 
						WHEN OppBizItems.numItemCode = @numShippingServiceItemID 
						THEN 
							CASE 
								WHEN ISNULL(@bitIncludeTaxAndShippingInCommission,0) = 1
								THEN	
									1
								ELSE	
									0
							END
						ELSE 
							1 
					END
				)/*CONDITION TO INCLUDE/EXCLUDE INCLUDE SHIPPING ITEM */


		-- LOOP ALL COMMISSION RULES
		DECLARE @i INT = 1
		DECLARE @COUNT INT = 0

		SELECT @COUNT=COUNT(*) FROM @TempCommissionRule

		DECLARE @TEMPITEM TABLE
		(
			ID INT,
			UniqueID INT,
			numUserCntID NUMERIC(18,0),
			numOppID NUMERIC(18,0),
			numOppBizDocID NUMERIC(18,0),
			numOppBizDocItemID NUMERIC(18,0),
			numItemCode NUMERIC(18,0),
			numUnitHour FLOAT,
			monTotAmount DECIMAL(20,5),
			monVendorCost DECIMAL(20,5),
			monAvgCost DECIMAL(20,5),
			numPOID NUMERIC(18,0),
			numPOItemID NUMERIC(18,0),
			monPOCost DECIMAL(20,5)
		)

		WHILE @i <= @COUNT
		BEGIN
			DECLARE @numComRuleID NUMERIC(18,0)
			DECLARE @tintComBasedOn TINYINT
			DECLARE @tintComAppliesTo TINYINT
			DECLARE @tintComOrgType TINYINT
			DECLARE @tintComType TINYINT
			DECLARE @tintAssignTo TINYINT
			DECLARE @numTotalAmount AS DECIMAL(20,5)
			DECLARE @numTotalUnit AS FLOAT

			--CLEAR PREVIOUS VALUES FROM TEMP TABLE
			DELETE FROM @TEMPITEM

			--FETCH COMMISSION RULE
			SELECT @numComRuleID=numComRuleID,@tintComBasedOn=tintComBasedOn,@tintComType=tintComType,@tintComAppliesTo=tintComAppliesTo,@tintComOrgType=tintComOrgType,@tintAssignTo=tintAssignTo FROM @TempCommissionRule WHERE ID=@i

			--FIND ALL BIZ DOC ITEMS WHERE COMMISSION IS NOT PAID AND CURRENT RULE ITEMS, ORGANIZATION AND ASSINGED TO FILTER CAN BE APPLIED
			INSERT INTO
				@TEMPITEM
			SELECT
				ROW_NUMBER() OVER (ORDER BY ID),
				ID,
				(CASE @tintAssignTo WHEN 1 THEN numAssignedTo WHEN 2 THEN numRecordOwner ELSE numPartner END),
				numOppID,
				numOppBizDocID,
				numOppBizDocItemID,
				numItemCode,
				numUnitHour,
				monTotAmount,
				monVendorCost,
				monAvgCost,
				numPOID,
				numPOItemID,
				monPOCost
			FROM
				@TABLEPAID
			WHERE
				1 = (CASE @tintComAppliesTo
						--SPECIFIC ITEMS
						WHEN 1 THEN CASE WHEN (SELECT COUNT(*) FROM CommissionRuleItems WHERE numComRuleID = @numComRuleID AND ISNULL(numValue,0) = numItemCode) > 0 THEN 1 ELSE 0 END
						-- ITEM WITH SPECIFIC CLASSIFICATIONS
						WHEN 2 THEN CASE WHEN (SELECT COUNT(*) FROM CommissionRuleItems WHERE numComRuleID = @numComRuleID AND ISNULL(numValue,0) = numItemClassification) > 0 THEN 1 ELSE 0 END
						-- ALL ITEMS
						ELSE 1
					END)
				AND 1 = (CASE @tintComOrgType
							-- SPECIFIC ORGANIZATION
							WHEN 1 THEN CASE WHEN (SELECT COUNT(*) FROM CommissionRuleOrganization WHERE numComRuleID = @numComRuleID AND ISNULL(numValue,0) = numDivisionID) > 0 THEN 1 ELSE 0 END
							-- ORGANIZATION WITH SPECIFIC RELATIONSHIP AND PROFILE
							WHEN 2 THEN CASE WHEN (SELECT COUNT(*) FROM CommissionRuleOrganization WHERE numComRuleID = @numComRuleID AND ISNULL(numValue,0) = numRelationship AND ISNULL(numProfile,0) = numProfile) > 0 THEN 1 ELSE 0 END
							-- ALL ORGANIZATIONS
							ELSE 1
						END)
				AND 1 = (CASE @tintAssignTo 
							-- ORDER ASSIGNED TO
							WHEN 1 THEN CASE WHEN (SELECT COUNT(*) FROM CommissionRuleContacts WHERE numComRuleID = @numComRuleID AND numValue=numAssignedTo) > 0 THEN 1 ELSE 0 END
							-- ORDER OWNER
							WHEN 2 THEN CASE WHEN (SELECT COUNT(*) FROM CommissionRuleContacts WHERE numComRuleID = @numComRuleID AND numValue=numRecordOwner) > 0 THEN 1 ELSE 0 END
							-- ORDER OWNER
							WHEN 3 THEN CASE WHEN (SELECT COUNT(*) FROM CommissionRuleContacts WHERE numComRuleID = @numComRuleID AND numValue=numPartner) > 0 THEN 1 ELSE 0 END
							-- NO OPTION SELECTED IN RULE
							ELSE 0
						END)

			--CALCULATE COMMISSION AND STORE IN TEMPARORY TABLE
			INSERT INTO @TempBizCommission 
			(
				numUserCntID,
				numOppID,
				numOppBizDocID,
				numOppBizDocItemID,
				monCommission,
				numComRuleID,
				tintComType,
				tintComBasedOn,
				decCommission,
				tintAssignTo
			)
			SELECT
				numUserCntID,
				numOppID,
				numOppBizDocID,
				numOppBizDocItemID,
				CASE @tintComType 
					WHEN 1 --PERCENT
						THEN 
							CASE 
								WHEN ISNULL(@bitCommissionBasedOn,1) = 1
								THEN
									(CASE 
										WHEN numPOItemID IS NOT NULL
										THEN (ISNULL(monTotAmount,0) - ISNULL(monPOCost,0)) * (T2.decCommission / CAST(100 AS FLOAT))
										ELSE CASE @tintCommissionBasedOn
												--ITEM GROSS PROFIT (VENDOR COST)
												WHEN 1 THEN (ISNULL(monTotAmount,0) - ISNULL(monVendorCost,0)) * (T2.decCommission / CAST(100 AS FLOAT))
												--ITEM GROSS PROFIT (AVERAGE COST)
												WHEN 2 THEN(ISNULL(monTotAmount,0) - ISNULL(monAvgCost,0)) * (T2.decCommission / CAST(100 AS FLOAT))
											END
									END)
								ELSE
									monTotAmount * (T2.decCommission / CAST(100 AS FLOAT))
							END
					ELSE  --FLAT
						T2.decCommission
				END,
				@numComRuleID,
				@tintComType,
				@tintComBasedOn,
				T2.decCommission,
				@tintAssignTo
			FROM 
				@TEMPITEM AS T1
			CROSS APPLY
			(
				SELECT TOP 1 
					ISNULL(decCommission,0) AS decCommission
				FROM 
					CommissionRuleDtl 
				WHERE 
					numComRuleID=@numComRuleID 
					AND ISNULL(decCommission,0) > 0
					AND 1 = (CASE 
							WHEN @tintComBasedOn = 1 --BASED ON AMOUNT SOLD 
							THEN (CASE WHEN (T1.monTotAmount BETWEEN intFrom AND intTo) THEN 1 ELSE 0 END)
							WHEN @tintComBasedOn = 2 --BASED ON UNITS SOLD
							THEN (CASE WHEN (T1.numUnitHour BETWEEN intFrom AND intTo) THEN 1 ELSE 0 END)
							ELSE 0
							END)
			) AS T2
			WHERE
				(CASE 
					WHEN ISNULL(@bitCommissionBasedOn,1) = 1
					THEN
						(CASE 
							WHEN numPOItemID IS NOT NULL
							THEN (ISNULL(monTotAmount,0) - ISNULL(monPOCost,0)) * (T2.decCommission / CAST(100 AS FLOAT))
							ELSE CASE @tintCommissionBasedOn
									--ITEM GROSS PROFIT (VENDOR COST)
									WHEN 1 THEN (ISNULL(monTotAmount,0) - ISNULL(monVendorCost,0)) * (T2.decCommission / CAST(100 AS FLOAT))
									--ITEM GROSS PROFIT (AVERAGE COST)
									WHEN 2 THEN(ISNULL(monTotAmount,0) - ISNULL(monAvgCost,0)) * (T2.decCommission / CAST(100 AS FLOAT))
								END
						END)
					ELSE
						monTotAmount * (T2.decCommission / CAST(100 AS FLOAT))
				END) > 0 
				AND (
						SELECT 
							COUNT(*) 
						FROM 
							@TempBizCommission 
						WHERE 
							numUserCntID=T1.numUserCntID 
							AND numOppID=T1.numOppID 
							AND numOppBizDocID = T1.numOppBizDocID 
							AND numOppBizDocItemID = T1.numOppBizDocItemID
							AND tintAssignTo = @tintAssignTo
					) = 0

			SET @i = @i + 1

			SET @numComRuleID = 0
			SET @tintComBasedOn = 0
			SET @tintComAppliesTo = 0
			SET @tintComOrgType = 0
			SET @tintComType = 0
			SET @tintAssignTo = 0
			SET @numTotalAmount = 0
			SET @numTotalUnit = 0
		END

		--INSERT CALCULATED ENTERIES IN BIZDOCCOMMISSION TABLE
		INSERT INTO BizDocComission
		(	
			numDomainID,
			numUserCntID,
			numOppBizDocId,
			numComissionAmount,
			numOppBizDocItemID,
			numComRuleID,
			tintComType,
			tintComBasedOn,
			decCommission,
			numOppID,
			bitFullPaidBiz,
			numComPayPeriodID,
			tintAssignTo
		)
		SELECT
			@numDomainID,
			numUserCntID,
			numOppBizDocID,
			monCommission,
			numOppBizDocItemID,
			numComRuleID,
			tintComType,
			tintComBasedOn,
			decCommission,
			numOppID,
			0,
			@numComPayPeriodID,
			tintAssignTo
		FROM
			@TempBizCommission
COMMIT
END TRY
BEGIN CATCH
	DECLARE @ErrorMessage NVARCHAR(4000)
	DECLARE @ErrorNumber INT
	DECLARE @ErrorSeverity INT
	DECLARE @ErrorState INT
	DECLARE @ErrorLine INT
	DECLARE @ErrorProcedure NVARCHAR(200)

	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;

	SELECT 
		@ErrorMessage = ERROR_MESSAGE(),
		@ErrorNumber = ERROR_NUMBER(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE(),
		@ErrorLine = ERROR_LINE(),
		@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

	RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);
END CATCH
END

GO

