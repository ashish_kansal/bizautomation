GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ManageProjectCommission')
DROP PROCEDURE USP_ManageProjectCommission
GO
CREATE PROCEDURE USP_ManageProjectCommission
    (     
      @numDomainId NUMERIC,
      @numProId numeric(9)
    )

AS BEGIN

DECLARE @tintCommissionType AS TINYINT
DECLARE @tintComAppliesTo AS TINYINT 

--Get CommissionType (total amounts paid : 1 ,gross profit : 2 ,Project Gross Profit : 3)
SELECT @tintCommissionType=ISNULL(tintCommissionType, 1),@tintComAppliesTo = ISNULL(tintComAppliesTo,3) FROM dbo.Domain WHERE numDomainId=@numDomainId

--If Commission set for Project Gross Profit
IF @tintCommissionType=3
BEGIN
	
	--If Project Status set as Completed (27492 : from ListDetails) 
	IF EXISTS (SELECT * FROM dbo.ProjectsMaster WHERE numdomainid = @numDomainId AND numProId=@numProId AND numProjectStatus=27492)
	BEGIN
		DECLARE @monTotalGrossProfit AS DECIMAL(20,5);SET @monTotalGrossProfit=0
		DECLARE @monTotalExpense AS DECIMAL(20,5);SET @monTotalExpense=0
		DECLARE @monTotalIncome AS DECIMAL(20,5);SET @monTotalIncome=0
		DECLARE @dtCompletionDate AS DATETIME;SET @dtCompletionDate=GETUTCDATE()
		
--		SELECT @monProjectGrossProfit=ISNULL(monBalance,0) FROM dbo.GetProjectIncomeExpense(@numDomainId,@numProId)
		
		SELECT @monTotalGrossProfit=ISNULL(monTotalGrossProfit,0),@monTotalExpense=ISNULL(monTotalExpense,0),
		 @monTotalIncome=ISNULL(monTotalIncome,0),@dtCompletionDate=dtCompletionDate FROM ProjectsMaster 
		 WHERE numdomainid = @numDomainId AND numProId=@numProId
		
		--If Project Gross Profit (+)
		IF @monTotalGrossProfit>0
		BEGIN
			DECLARE @numassignedto NUMERIC,@numrecowner NUMERIC
			DECLARE @numDivisionID NUMERIC,@bitCommContact AS BIT,@numUserCntID NUMERIC
			DECLARE @decCommission DECIMAL(18,2),@monCommission AS DECIMAL(20,5),@numComRuleID NUMERIC,@tintComBasedOn TINYINT,@tinComDuration TINYINT,@tintComType TINYINT
			DECLARE @dFrom DATETIME,@dTo DATETIME,@monTotalProjectGrossProfit AS DECIMAL(20,5),@monTotalProjectIncome AS DECIMAL(20,5)  
			
			--Get AssignTo and Record Owner of Oppertunity
			SELECT @numassignedto=numassignedto,@numrecowner=numrecowner FROM ProjectsMaster 
			    WHERE numdomainid = @numDomainId AND numProId=@numProId

				    DECLARE @i AS INT;SET @i=1
					
					--Loop for AssignTo i=1 and RecordOwner i=2 (set tintAssignTo=i)
					WHILE @i<3
					BEGIN
						
					IF @i=1
						SET @numUserCntID=@numassignedto
					ELSE IF @i=2
						SET @numUserCntID=@numrecowner
						
					DECLARE @monProTotalGrossProfit AS DECIMAL(20,5);SET @monProTotalGrossProfit=0
					DECLARE @monProTotalIncome AS DECIMAL(20,5);SET @monProTotalIncome=0
					
					SELECT @monProTotalGrossProfit=ISNULL(monProTotalGrossProfit,0),@monProTotalIncome=ISNULL(monProTotalIncome,0) FROM dbo.BizDocComission WHERE numdomainid = @numDomainId AND numProId=@numProId AND numUserCntID=@numUserCntID	
					
					SET @monProTotalGrossProfit=@monTotalGrossProfit - @monProTotalGrossProfit
					SET @monProTotalIncome = @monTotalIncome - @monProTotalIncome
					
					IF @monProTotalGrossProfit>0
					BEGIN
						--Check For Commission Contact	
						IF EXISTS(SELECT UM.numUserId from UserMaster UM  join AdditionalContactsInformation A  on UM.numUserDetailId=A.numContactID          
								where UM.numDomainID=@numDomainID and UM.numDomainID=A.numDomainID AND A.numContactID =@numUserCntID)
							BEGIN
								SET @bitCommContact=0
								SET @numDivisionID=0
							END	
						 ELSE
							BEGIN
								 SET @bitCommContact=1
								 SELECT @numDivisionID=A.numDivisionId from AdditionalContactsInformation A  where A.numContactId =@numUserCntID 
							END  		
							
						--All Items	 tintComAppliesTo=3
						IF EXISTS (SELECT CR.numComRuleID FROM CommissionRules CR JOIN CommissionRuleContacts CRC ON CR.numComRuleId=CRC.numComRuleId  
										WHERE CR.tintComAppliesTo=3 AND CR.numDomainID=@numDomainID AND CR.tintAssignTo=@i AND CRC.numValue=(CASE @bitCommContact WHEN 0 THEN @numUserCntID WHEN 1 THEN @numDivisionID END) AND CRC.bitCommContact=@bitCommContact) AND @tintComAppliesTo=3
							BEGIN
								SELECT TOP 1 @numComRuleID=CR.numComRuleID,@tintComBasedOn=CR.tintComBasedOn,@tinComDuration=CR.tinComDuration,@tintComType=CR.tintComType 
								FROM CommissionRules CR JOIN CommissionRuleContacts CRC ON CR.numComRuleId=CRC.numComRuleId  WHERE CR.tintComAppliesTo=3 AND CR.numDomainID=@numDomainID 
								AND CR.tintAssignTo=@i AND CRC.numValue=(CASE @bitCommContact WHEN 0 THEN @numUserCntID WHEN 1 THEN @numDivisionID END) AND CRC.bitCommContact=@bitCommContact
							END
						
						IF @numComRuleID>0
						BEGIN
							--Get From and To date for Commission Calculation
							IF @tinComDuration=1 --Month
								SELECT @dFrom=DATEADD(m, DATEDIFF(m, 0, GETDATE()), 0),
										@dTo=DATEADD(d, -1, DATEADD(m, DATEDIFF(m, 0, GETDATE()) + 1, 0)) 
		
							ELSE IF @tinComDuration=2 --Quarter
								SELECT @dFrom=DATEADD(q, DATEDIFF(q, 0, GETDATE()), 0),
										@dTo=DATEADD(d, -1, DATEADD(q, DATEDIFF(q, 0, GETDATE()) + 1, 0)) 
										
							ELSE IF @tinComDuration=3 --Year
								SELECT @dFrom=DATEADD(yy, DATEDIFF(yy, 0, GETDATE()), 0),
										@dTo=DATEADD(d, -1, DATEADD(yy, DATEDIFF(yy, 0, GETDATE()) + 1, 0)) 
							
							
							--Total of Units and Amount for all oppertunity belogs to selected Item and selected AssignTo/RecordOwner
							SELECT @monTotalProjectGrossProfit=ISNULL(SUM(PM.monTotalGrossProfit),0),
								   @monTotalProjectIncome=ISNULL(SUM(PM.monTotalIncome),0)
								FROM dbo.ProjectsMaster PM 
								WHERE PM.numdomainid = @numDomainId AND PM.numProId<>@numProId AND PM.numProjectStatus=27492
								AND 1= (CASE @i WHEN 1 THEN CASE @bitCommContact WHEN 0 THEN CASE WHEN numassignedto=@numUserCntID THEN 1 ELSE 0 END 
																				 WHEN 1 THEN CASE WHEN numassignedto IN (SELECT numContactId FROM AdditionalContactsInformation AC WHERE AC.numDivisionID = @numDivisionID) THEN 1 ELSE 0 END  END
												WHEN 2 THEN CASE @bitCommContact WHEN 0 THEN CASE WHEN numrecowner=@numUserCntID THEN 1 ELSE 0 END 
																				WHEN 1 THEN CASE WHEN numrecowner IN (SELECT numContactId FROM AdditionalContactsInformation AC WHERE AC.numDivisionID = @numDivisionID) THEN 1 ELSE 0 END END END)	 				 	 
								AND PM.dtCompletionDate BETWEEN @dFrom AND @dTo
							
							
							IF @tintComBasedOn=3 --Project Gross Profit Amt
							BEGIN
								SET @monTotalProjectGrossProfit = @monTotalGrossProfit + @monTotalProjectGrossProfit
							END
							ELSE IF @tintComBasedOn=4 --Project Total Income %
							BEGIN
								SET @monTotalProjectGrossProfit = ((@monTotalGrossProfit + @monTotalProjectGrossProfit) / (@monTotalIncome + @monTotalProjectIncome)) * 100
							END
							
							DECLARE @intFromNextTier AS DECIMAL(18,2)
							
							SELECT TOP 1 @decCommission = decCommission,@intFromNextTier=intTo + 1 FROM CommissionRuleDtl 
										WHERE numComRuleID=@numComRuleID AND @monTotalProjectGrossProfit BETWEEN intFrom AND intTo
					
					
							DECLARE @nexttier AS varchar(100)

							SELECT TOP 1 @nexttier = CAST(numComRuleDtlID AS VARCHAR(10))  + ',' + CAST(numComRuleID AS VARCHAR(10)) + ',' + CAST(intFrom AS VARCHAR(10)) + ',' + CAST(intTo AS VARCHAR(10)) + ',' + CAST(decCommission AS VARCHAR(10)) 
							FROM CommissionRuleDtl WHERE numComRuleID=@numComRuleID AND intFrom >= @intFromNextTier	ORDER BY intFrom 
								
							IF @tintComBasedOn=3 --Project Gross Profit Amt
							BEGIN
								SET @monCommission=@monProTotalGrossProfit * @decCommission / 100
							END
							ELSE IF @tintComBasedOn=4 --Project Total Income %
							BEGIN
								SET @monCommission=@monProTotalIncome * @decCommission / 100
							END
							
							if @monCommission>0
							begin
	--							IF NOT exists (SELECT * FROM [BizDocComission] WHERE numDomainId=@numDomainId AND numUserCntID=@numUserCntID AND numProId=@numProId)
	--							BEGIN
									INSERT INTO [BizDocComission]([numDomainId],[numUserCntID],[numProId],[numComissionAmount],
											[numComRuleID],[tintComType],[tintComBasedOn],[decCommission],vcnexttier,dtProCompletionDate,monProTotalExpense,monProTotalIncome,monProTotalGrossProfit) 
											values (@numDomainId,@numUserCntID,@numProId,@monCommission,
												   @numComRuleID,@tintComType,@tintComBasedOn,@decCommission,@nexttier,@dtCompletionDate,@monTotalExpense,@monTotalIncome,@monTotalGrossProfit);
								--END
							END
						END
						END	
						SELECT @numComRuleID=0,@tintComBasedOn=0,@tinComDuration=0,@tintComType=0,@monCommission=0,@bitCommContact=0,@numDivisionID=0,@monTotalProjectGrossProfit=0,@monTotalProjectIncome=0
							SET @i = @i + 1
					END
		END
	END
	
END
    
END

GO


