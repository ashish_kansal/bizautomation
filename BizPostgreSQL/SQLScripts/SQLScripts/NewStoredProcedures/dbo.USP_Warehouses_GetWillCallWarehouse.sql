GO
/****** Object:  StoredProcedure [dbo].[USP_UpdateOppAddress]    Script Date: 05/31/2009 15:44:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO  
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_Warehouses_GetWillCallWarehouse')
DROP PROCEDURE USP_Warehouses_GetWillCallWarehouse
GO
CREATE PROCEDURE [dbo].[USP_Warehouses_GetWillCallWarehouse]      
@numDomainID NUMERIC(18,0)
,@numWarehouseID numERIC(18,0)
AS
BEGIN
	SELECT TOP 1 numWareHouseID, isnull(vcWStreet,'') + ', ' + isnull(vcWCity,'') + ', ' +  ISNULL(dbo.fn_GetState(numWState),'') + ', ' + ISNULL(dbo.fn_GetListItemName(numWCountry),'') + ' - ' + ISNULL(vcWPinCode,'') vcFullAddress FROM Warehouses WHERE numDomainID=@numDomainID AND (numWareHouseID = @numWarehouseID OR ISNULL(@numWarehouseID,0) = 0)
	
END
