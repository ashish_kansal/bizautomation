GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetOrderRule')
DROP PROCEDURE USP_GetOrderRule
GO
CREATE PROCEDURE USP_GetOrderRule
@numRuleID numeric(18,0),
@numDomainID numeric(18,0)

as

select numRuleID,numBillToID,numShipToID,numBizDocStatus,
(case numBillToID when 0 then 'Employer' when 1 then 'Customer' end) as [Bill To],
(case numShipToID when 0 then 'Employer' when 1 then 'Customer' end) as [Ship To],
(case btFullPaid when 0 then 'No' when 1 then 'Yes' end) as [Amount Paid Full],
(case numBizDocStatus when 0 then '-Select One--' else ITB.vcData end) AS [Biz Doc Status]
 from OrderAutoRule OAR  LEFT OUTER JOIN 
	(select LD.vcData,LD.numListItemID from ListDetails LD,LISTMASTER LM  where LM.numListID=11 
		and LD.numListID=LM.numListID AND LD.numDomainID=@numDomainID)	 ITB ON ITB.numListItemID=OAR.numBizDocStatus 
where OAR.numDomainID=@numDomainID