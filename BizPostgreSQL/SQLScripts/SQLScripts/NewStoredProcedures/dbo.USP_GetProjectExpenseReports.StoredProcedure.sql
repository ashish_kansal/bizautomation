GO
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetProjectExpenseReports')
DROP PROCEDURE USP_GetProjectExpenseReports
GO
CREATE PROCEDURE [dbo].[USP_GetProjectExpenseReports]
(
    @numDomainID NUMERIC(18,0),
    @numProId NUMERIC(18,0)
)
AS 
BEGIN
	DECLARE @numCost TINYINT
	DECLARE @numCurrencyID NUMERIC(18,0)
	DECLARE @varCurrSymbol VARCHAR(10)
	SELECT @numCost = ISNULL(numCost,1),@numCurrencyID=ISNULL(numCurrencyID,0) FROM Domain WHERE numDomainId=@numDomainID

	SELECT @varCurrSymbol = ISNULL(varCurrSymbol,'$') FROM Currency WHERE numCurrencyID = @numCurrencyID

	DECLARE @TEMP TABLE
	(
		vcItem TEXT
		,vcUnits TEXT
		,vcIncomeSource TEXT
		,vcExpenseSource TEXT
		,vcIncomeExpense TEXT
		,vcNetIncome TEXT
		,monIncome DECIMAL(20,5)
		,monExpense DECIMAL(20,5)
		,monNetIncome DECIMAL(20,5)
		,bitExpensePending BIT
	)

	-- Sales Orders
	INSERT INTO @TEMP
	(
		vcItem
		,vcUnits
		,vcIncomeSource
		,vcExpenseSource
		,monIncome
		,monExpense
		,bitExpensePending
	)
	SELECT 
		CONCAT(ISNULL(OI.vcItemName,I.vcItemName),' <i style="color:#afabab">(',(CASE 
																						WHEN I.charItemType='P' 
																						THEN CONCAT('Inventory',(CASE WHEN ISNULL(OI.bitDropship,0) = 1 THEN ' Drop ship' ELSE '' END))
																						WHEN I.charItemType='N' THEN 'Non-Inventory' 
																						WHEN I.charItemType='S' THEN 'Service' 
																						WHEN I.charItemType='A' THEN 'Accessory' 
																					END),')</i> ',OI.vcItemDesc)
		,CONCAT(dbo.fn_UOMConversion(ISNULL(I.numBaseUnit, 0),I.numItemCode, @numDomainID,ISNULL(OI.numUOMId, 0)) * OI.numUnitHour,' (',ISNULL(UOM.vcUnitName,'-'),')')
		,CONCAT('<a href="../opportunity/frmOpportunities.aspx?OpID=',OM.numOppID,'" target="_blank">',vcPOppName,'</a>')
		,(CASE 
			WHEN I.charItemType='S' AND ISNULL(I.bitExpenseItem,0) = 1 
			THEN (CASE 
					WHEN BH.numBillID IS NULL 
					THEN '<span class="text-orange">Pending</span>' 
					ELSE CONCAT('<a href="javascript:void(0)" onclick="OpenBizInvoice(0,0,',BH.numBillID,',2);">',((CASE WHEN ISNULL(BH.bitLandedCost,0)=1 THEN 'Laned Cost-' ELSE 'Bill-' END) + CONVERT(VARCHAR(10),BH.numBillID)),'</a>') 
				END)
			ELSE (CASE @numCost 
					WHEN 3 THEN 'Primary Vendor Cost'
					WHEN 2 THEN 'Products & Services Cost' 
					ELSE (CASE WHEN I.charItemType='P' THEN 'Average Cost' ELSE 'Primary Vendor Cost' END)
				END)
		END)
		,OI.monTotAmount
		,(CASE 
			WHEN I.charItemType='S' AND ISNULL(I.bitExpenseItem,0) = 1 
			THEN (CASE 
					WHEN BH.numBillID IS NULL 
					THEN 0
					ELSE ISNULL(BD.monAmount,0)
				END)
			ELSE (CASE @numCost 
					WHEN 3 THEN ISNULL(OI.numUnitHour,0) * ISNULL(OI.monVendorCost,0)
					WHEN 2 THEN ISNULL(OI.numUnitHour,0) * ISNULL(OI.numCost,0)
					ELSE (CASE 
							WHEN I.charItemType='P' 
							THEN ((ISNULL(OI.numUnitHour,0) - ISNULL(TEMPFulfilledItems.numUnitHour,0)) * ISNULL(OI.monAvgCost,0)) + ISNULL(TEMPFulfilledItems.monCost,0)
							ELSE ISNULL(OI.numUnitHour,0) * ISNULL(OI.monVendorCost,0) 
						END)
				END)
		END)
		,(CASE 
			WHEN I.charItemType='S' AND ISNULL(I.bitExpenseItem,0) = 1 
			THEN (CASE 
					WHEN BH.numBillID IS NULL 
					THEN 1
					ELSE 0
				END)
			ELSE 0
		END)
	FROM 
		OpportunityMaster OM
	INNER JOIN
		OpportunityItems OI
	ON
		OM.numOppId = OI.numOppId
	INNER JOIN
		Item I
	ON
		OI.numItemCode = I.numItemCode
	LEFT JOIN
		UOM
	ON
		OI.numUOMId = UOM.numUOMId
	LEFT JOIN
		BillDetails BD
	ON
		OI.numoppitemtCode = BD.numOppItemID
	LEFT JOIN
		BillHeader BH
	ON
		BD.numBillID = BH.numBillID
	OUTER APPLY
	(
		SELECT 
			SUM(numUnitHour) numUnitHour
			,SUM(ISNULL(OBDI.numUnitHour,0) * ISNULL(OBDI.monAverageCost,0)) monCost
		FROM 
			OpportunityBizDocs OBD
		INNER JOIN
			OpportunityBizDocItems OBDI
		ON
			OBD.numOppBizDocsId = OBDI.numOppBizDocID
		WHERE
			OBD.numOppId = OM.numOppID
			AND OBD.numBizDocId = 296
			AND OBDI.numOppItemID = OI.numoppitemtCode
	) TEMPFulfilledItems
	WHERE
		OM.numDomainId=@numDomainID
		AND OM.tintOppType = 1
		AND OM.tintOppStatus = 1
		AND ISNULL(OI.numProjectID,OM.numProjectID) = @numProId
		

	-- Bills
	INSERT INTO @TEMP
	(
		vcItem
		,vcUnits
		,vcIncomeSource
		,vcExpenseSource
		,monIncome
		,monExpense
		,bitExpensePending
	)
	SELECT
		ISNULL(BD.vcDescription,'')
		,''
		,''
		,(CASE WHEN(BD.numBillID>0) THEN  Case When isnull(BH.bitLandedCost,0)=1 THEN 'Laned Cost-' ELSE 'Bill-' END + CONVERT(VARCHAR(10),BH.numBillID) END)
		,0
		,ISNULL(monAmount,0)
		,0
	FROM	
		BillHeader BH
	INNER JOIN
		BillDetails BD
	ON
		BH.numBillID = BD.numBillID
	WHERE
		BH.numDomainID = @numDomainID
		AND BD.numProjectID = @numProId
		AND ISNULL(BD.numOppItemID,0) = 0

	UPDATE
		@TEMP
	SET 
		vcNetIncome = (CASE 
						WHEN ISNULL(bitExpensePending,0) = 1 THEN CONCAT('<span class="text-orange">',@varCurrSymbol,' ',FORMAT(ISNULL(monIncome,0) - ISNULL(monExpense,0), '#,##0.00###'),'</span>')
						WHEN ISNULL(monIncome,0) - ISNULL(monExpense,0) >= 0 
						THEN CONCAT('<span class="text-green">',@varCurrSymbol,' ',FORMAT(ISNULL(monIncome,0) - ISNULL(monExpense,0), '#,##0.00###'),'</span>') 
						ELSE CONCAT('<span class="text-red">',@varCurrSymbol,' ',FORMAT(ISNULL(monIncome,0) - ISNULL(monExpense,0), '#,##0.00###'),'</span>') 
					END)
		,monNetIncome = ISNULL(monIncome,0) - ISNULL(monExpense,0)
		,vcIncomeExpense = CONCAT(FORMAT(ISNULL(monIncome,0), '#,##0.00###'),(CASE WHEN ISNULL(bitExpensePending,0) = 1 THEN '' ELSE CONCAT(' (',FORMAT(ISNULL(monExpense,0), '#,##0.00###'),')') END))
			
	SELECT * FROM @TEMP

	;WITH CTE AS 
	(SELECT
		ISNULL((SELECT SUM(monIncome) FROM @TEMP),0) AS monIncomeAR
		,ISNULL((SELECT SUM(monExpense) FROM @TEMP),0) AS monNonLaborExpense
		,ISNULL((SELECT SUM(monNetIncome) FROM @TEMP),0) AS monTotalIncome
		,0 AS monExpenseAP
		,ISNULL((SELECT 
					SUM(dbo.GetTimeSpendOnTaskInMinutes(@numDomainID,SPDT.numTaskId) * (ISNULL(SPDT.monHourlyRate,UM.monHourlyRate)/60))
				FROM
					Sales_process_List_Master SPLM
				INNER JOIN
					StagePercentageDetails SPD
				ON
					SPLM.Slp_Id = SPD.slp_id
				INNER JOIN
					StagePercentageDetailsTask SPDT
				ON
					SPD.numStageDetailsId = SPDT.numStageDetailsId
				INNER JOIN
					UserMaster UM
				ON
					SPDT.numAssignTo = UM.numUserDetailId
				WHERE
					SPLM.numdomainid=@numDomainID
					AND SPLM.numProjectId = @numProID
					AND EXISTS (SELECT SPDTTL.ID FROM StagePercentageDetailsTaskTimeLog SPDTTL WHERE SPDTTL.numTaskID = SPDT.numTaskId AND tintAction=4)),0) AS monLaborExpense
		,ISNULL((SELECT SUM(monIncome) FROM @TEMP WHERE ISNULL(bitExpensePending,0) = 1),0) AS monIncomePendingExpense)

	SELECT *,ISNULL(monTotalIncome,0) - ISNULL(monLaborExpense,0) AS monGrossProfit FROM CTE
		


--IF @tintMode=0 --Use Order Amounts
--BEGIN
--            SELECT 1 as iTEType,'Sales Order' AS vcTimeExpenseType,I.[vcItemName],
--                    CASE numType
--                      WHEN 1 THEN ISNULL(OPP.numUnitHour, 0)
--                      ELSE ISNULL(CONVERT(VARCHAR(100), CONVERT(DECIMAL(10, 1), ( CONVERT(DECIMAL(10, 1), DATEDIFF(minute, TE.dtFromDate, TE.dtToDate)) / 60 ))),
--                                  0)
--                    END AS [numUnitHour],
--                    ISNULL(OPP.[monPrice], 0) monPrice,
--					ISNULL(OPP.[monPrice], 0) * CASE numType
--                      WHEN 1 THEN ISNULL(OPP.numUnitHour, 0)
--                      ELSE ISNULL(CONVERT(VARCHAR(100), CONVERT(DECIMAL(10, 1), ( CONVERT(DECIMAL(10, 1), DATEDIFF(minute, TE.dtFromDate, TE.dtToDate)) / 60 ))),
--                                  0)
--                    END - (CASE WHEN I.charItemType='P' THEN CASE WHEN ISNULL(Opp.monAvgCost,0)=0 THEN ISNULL(Opp.monVendorCost,0) ELSE ISNULL(Opp.monAvgCost,0) END ELSE 0 END)  AS ExpenseAmount,
--                    dbo.[FormatedDateFromDate](TE.[dtTCreatedOn], @numDomainID) AS dtDateEntered,
--                    dbo.fn_GetContactName(TE.numTCreatedBy) AS vcCreatedBy,TE.numOppId,1 as numOppType,
--					TE.[numCategoryHDRID],isnull(OPP.numProjectStageID,0) as numProjectStageID,0 as numBillId,
--					Case When OPP.numProjectStageID>0 then dbo.fn_GetProjectStageName(OPP.numProjectId,OPP.numProjectStageID) when OPP.numProjectStageID=-1 then 'T&E' else 'S.O.' end as vcFrom,
--					OPP.numProjectId as numProId,TE.[dtTCreatedOn] as dtCreatedOn,isnull((SELECT SUBSTRING((SELECT ',' + Cast(OB.numOppBizDocsId as varchar(18)) from  [OpportunityBizDocs] OB 
--					INNER JOIN [OpportunityBizDocItems] OBI ON OBI.numOppBizDocID = OB.numOppBizDocsId AND OPP.numoppitemtCode = OBI.numOppItemID 
--					WHERE OB.bitAuthoritativeBizDocs=1 and OB.numOppId = OM.numOppId FOR XML PATH('')),2,200000)),'') AS vcBizDoc
--					,0 AS numDepositId,0 AS numCheckHeaderID,0 AS numJournalId,ISNULL(Opp.monAvgCost,0) AS monAvgCost,ISNULL(Opp.monVendorCost,0) AS monVendorCost
--            FROM    [TimeAndExpense] TE
--                    INNER JOIN OpportunityMaster OM ON TE.numOppId = OM.numOppId
--                    INNER JOIN OpportunityItems OPP ON OPP.numOppId = TE.numOppId
--							AND OPP.numoppitemtCode = TE.numOppItemID AND OPP.numProjectId=TE.numProId
--					INNER JOIN Item I ON OPP.[numItemCode] = I.[numItemCode]
--            WHERE   [numProId] = @numProId
--                    AND TE.[numDomainID] = @numDomainID
--                    AND numType =1 --Billable 
--					AND OM.[tintOppType] = 1

--			UNION ALL
	
--			SELECT 1 as iTEType,'Sales Order' AS vcTimeExpenseType,I.[vcItemName],
--                    ISNULL(OPP.numUnitHour, 0) AS [numUnitHour],
--                    ISNULL(OPP.[monPrice], 0) monPrice,
--					ISNULL(OPP.[monPrice], 0) * ISNULL(OPP.numUnitHour, 0) - (CASE WHEN I.charItemType='P' THEN CASE WHEN ISNULL(Opp.monAvgCost,0)=0 THEN ISNULL(Opp.monVendorCost,0) ELSE ISNULL(Opp.monAvgCost,0) END ELSE 0 END)  AS ExpenseAmount,
--                    dbo.[FormatedDateFromDate](OM.bintCreatedDate, @numDomainID) AS dtDateEntered,
--                    dbo.fn_GetContactName(OM.numCreatedBy) AS vcCreatedBy,OM.numOppId,1 as numOppType,
--					0 as numCategoryHDRID,isnull(OPP.numProjectStageID,0) as numProjectStageID,0 as numBillId,
--					Case When OPP.numProjectStageID>0 then dbo.fn_GetProjectStageName(OPP.numProjectId,OPP.numProjectStageID) when OPP.numProjectStageID=-1 then 'T&E' else 'S.O.' end as vcFrom,
--					OPP.numProjectId as numProId,OM.bintCreatedDate as dtCreatedOn,isnull((SELECT SUBSTRING((SELECT ',' + Cast(OB.numOppBizDocsId as varchar(18)) from  [OpportunityBizDocs] OB 
--					INNER JOIN [OpportunityBizDocItems] OBI ON OBI.numOppBizDocID = OB.numOppBizDocsId AND OPP.numoppitemtCode = OBI.numOppItemID 
--					WHERE OB.bitAuthoritativeBizDocs=1 and OB.numOppId = OM.numOppId FOR XML PATH('')),2,200000)),'') AS vcBizDoc,0 AS numDepositId,0 AS numCheckHeaderID,0 AS numJournalId
--					,ISNULL(Opp.monAvgCost,0) AS monAvgCost,ISNULL(Opp.monVendorCost,0) AS monVendorCost
--            FROM    OpportunityMaster OM INNER JOIN OpportunityItems OPP ON OPP.numOppId = OM.numOppId
--					INNER JOIN Item I ON OPP.[numItemCode] = I.[numItemCode]
--            WHERE  OM.[numDomainID] = @numDomainID AND OPP.numProjectID = @numProId
--                    AND OPP.numProjectStageID=0 AND OM.[tintOppType] = 1

--			UNION ALL
			
--			SELECT  2 as iTEType,'Non-Billable Time' AS vcTimeExpenseType,
--                    ISNULL(CONVERT(VARCHAR(25),txtDesc) + '..' ,'') AS [vcItemName],
--                    ISNULL(CONVERT(VARCHAR(100), CONVERT(DECIMAL(10, 1), ( CONVERT(DECIMAL(10, 1), DATEDIFF(minute, TE.dtFromDate, TE.dtToDate)) / 60 ))),0)
--                    AS [numUnitHour],
--                    0 monPrice,
--					0 AS ExpenseAmount,
--                    dbo.[FormatedDateFromDate](TE.[dtTCreatedOn], @numDomainID) AS dtDateEntered,
--                    dbo.fn_GetContactName(TE.numTCreatedBy) AS vcCreatedBy,0 as numOppId,0 as numOppType,
--					TE.[numCategoryHDRID],isnull(TE.numStageId,0) as numProjectStageID,0 as numBillId,
--					Case When TE.numStageId>0 then dbo.fn_GetProjectStageName(TE.numProId,TE.numStageId) else 'Time' end as vcFrom,
--					TE.numProId,TE.[dtTCreatedOn] as dtCreatedOn,'' as vcBizDoc,0 AS numDepositId,0 AS numCheckHeaderID,0 AS numJournalId
--					,0 AS monAvgCost,0 AS monVendorCost
--            FROM    [TimeAndExpense] TE
--            WHERE   [numProId] = @numProId
--                    AND TE.[numDomainID] = @numDomainID
--                    AND numType =2 --Non Billable

--		    UNION ALL

--            select 3 as iTEType,'Purchase Order' AS vcTimeExpenseType,I.[vcItemName],
--                    ISNULL(OPP.numUnitHour, 0) as numUnitHour,
--                    ISNULL(OPP.[monPrice], 0) monPrice,
--	   			    (OPP.[monPrice] * OPP.[numUnitHour]) AS ExpenseAmount,
--					dbo.[FormatedDateFromDate](OM.bintCreatedDate, @numDomainID) AS dtDateEntered,
--                    dbo.fn_GetContactName(OM.numCreatedBy) AS vcCreatedBy,OM.numOppId,2 as numOppType,
--					0 as numCategoryHDRID,isnull(OPP.numProjectStageID,0) as numProjectStageID,0 as numBillId,
--					Case When OPP.numProjectStageID>0 then dbo.fn_GetProjectStageName(OPP.numProjectId,OPP.numProjectStageID) else 'P.O.' end as vcFrom,
--					OPP.numProjectID as numProId,OM.bintCreatedDate as dtCreatedOn,isnull((SELECT SUBSTRING((SELECT ',' + Cast(OB.numOppBizDocsId as varchar(18)) from  [OpportunityBizDocs] OB 
--					INNER JOIN [OpportunityBizDocItems] OBI ON OBI.numOppBizDocID = OB.numOppBizDocsId AND OPP.numoppitemtCode = OBI.numOppItemID 
--					WHERE OB.bitAuthoritativeBizDocs=1 and OB.numOppId = OM.numOppId FOR XML PATH('')),2,200000)),'') AS vcBizDoc,0 AS numDepositId,0 AS numCheckHeaderID,0 AS numJournalId
--					,ISNULL(Opp.monAvgCost,0) AS monAvgCost,ISNULL(Opp.monVendorCost,0) AS monVendorCost
--			 from  [OpportunityMaster] OM 
--						JOIN [OpportunityItems] OPP ON OM.[numOppId] = OPP.[numOppId]
--                        LEFT OUTER JOIN [Item] I ON OPP.[numItemCode] = I.[numItemCode]
--						LEFT OUTER JOIN [Chart_Of_Accounts] COA ON I.[numCOGsChartAcntId] = COA.[numAccountId]
--			 where  OM.numDomainId=@numDomainID  
--					AND OPP.numProjectID = @numProId
--					AND OM.[tintOppType] = 2	

--			 Union ALL

--			select 4 as iTEType,'Bill' as vcTimeExpenseType,BD.vcDescription as vcItemName,0 as numUnitHour,0 as monPrice,BD.monAmount AS ExpenseAmount,
--				dbo.[FormatedDateFromDate](BH.dtCreatedDate, @numDomainID) AS dtDateEntered,
--                    dbo.fn_GetContactName(BH.numCreatedBy) AS vcCreatedBy,0 as numOppId,0 as numOppType,
--					0 as numCategoryHDRID,isnull(PO.numStageID,0) as numProjectStageID,PO.numBillId,
--					Case When PO.numStageID>0 then dbo.fn_GetProjectStageName(PO.numProID,PO.numStageID) else 'Bill' end as vcFrom,
--					PO.numProID,BH.dtCreatedDate as dtCreatedOn,'' as vcBizDoc,0 AS numDepositId,0 AS numCheckHeaderID,0 AS numJournalId
--					,0 AS monAvgCost,0 AS monVendorCost
--			 from  ProjectsOpportunities PO 
--				   JOIN BillHeader BH ON BH.numBillID = PO.numBillID
--				   JOIN BillDetails BD ON BH.numBillID=BD.numBillID AND PO.numProID=BD.numProjectID
--			 where  PO.numDomainId=@numDomainID  
--					AND PO.numProID = @numProId
--					and isnull(PO.numBillId,0)>0
					
--			Union ALL

--			select 4 as iTEType,'Bill' as vcTimeExpenseType,BD.vcDescription as vcItemName,0 as numUnitHour,0 as monPrice,BD.monAmount AS ExpenseAmount,
--				dbo.[FormatedDateFromDate](BH.dtCreatedDate, @numDomainID) AS dtDateEntered,
--                    dbo.fn_GetContactName(BH.numCreatedBy) AS vcCreatedBy,0 as numOppId,0 as numOppType,
--					0 as numCategoryHDRID,0 as numProjectStageID,BD.numBillId,
--					'Bill' as vcFrom,
--					BD.numProjectID,BH.dtCreatedDate as dtCreatedOn,'' as vcBizDoc,0 AS numDepositId,0 AS numCheckHeaderID,0 AS numJournalId
--					,0 AS monAvgCost,0 AS monVendorCost
--			 from  BillHeader BH 
--				   JOIN BillDetails BD ON BH.numBillID=BD.numBillID 
--			 where  BH.numDomainId=@numDomainID  
--					AND BD.numProjectID = @numProId
							
			
--			UNION ALL

--			SELECT  5 as iTEType,'Employee Expense' AS vcTimeExpenseType,
--                    ISNULL(CONVERT(VARCHAR(25),txtDesc) + '..' ,'') AS [vcItemName],
--                    ISNULL(CONVERT(VARCHAR(100), CONVERT(DECIMAL(10, 1), ( CONVERT(DECIMAL(10, 1), DATEDIFF(minute, TE.dtFromDate, TE.dtToDate)) / 60 ))),0)
--                    AS [numUnitHour],
--                    isnull(UM.monHourlyRate,0) monPrice,
--					isnull(Cast(datediff(minute,TE.dtfromdate,dttodate) as float)/Cast(60 as float),0) * isnull(UM.monHourlyRate,0) AS ExpenseAmount,
--                    dbo.[FormatedDateFromDate](TE.[dtTCreatedOn], @numDomainID) AS dtDateEntered,
--                    dbo.fn_GetContactName(TE.numTCreatedBy) AS vcCreatedBy,0 as numOppId,0 as numOppType,
--					TE.[numCategoryHDRID],isnull(TE.numStageId,0) as numProjectStageID,0 as numBillId,
--					Case When TE.numStageId>0 then dbo.fn_GetProjectStageName(TE.numProId,TE.numStageId) else 'Expense' end as vcFrom,
--					TE.numProId,TE.[dtTCreatedOn] as dtCreatedOn,'' as vcBizDoc,0 AS numDepositId,0 AS numCheckHeaderID,0 AS numJournalId
--					,0 AS monAvgCost,0 AS monVendorCost
--            FROM    [TimeAndExpense] TE join UserMaster UM on UM.numUserDetailId=TE.numUserCntID
--            WHERE   [numProId] = @numProId
--                    AND TE.[numDomainID] = @numDomainID
--                    AND numType in (1,2) --Non Billable & Billable
--					AND TE.numUserCntID>0
----					AND bitReimburse=1 and  numCategory=2  

--			UNION ALL
			
--			select 6 as iTEType,'Deposit' as vcTimeExpenseType,DED.vcMemo as vcItemName,0 as numUnitHour,0 as monPrice,DED.monAmountPaid AS ExpenseAmount,
--				dbo.[FormatedDateFromDate](DEM.dtCreationDate, @numDomainID) AS dtDateEntered,
--                    dbo.fn_GetContactName(DEM.numCreatedBy) AS vcCreatedBy,0 as numOppId,0 as numOppType,
--					0 as numCategoryHDRID,0 as numProjectStageID,0 AS numBillId,
--					'' as vcFrom,
--					DED.numProjectID AS numProID,DEM.dtCreationDate as dtCreatedOn,'' as vcBizDoc,DEM.numDepositId,0 AS numCheckHeaderID,0 AS numJournalId
--					,0 AS monAvgCost,0 AS monVendorCost
--			 from  DepositMaster DEM JOIN DepositeDetails DED ON DEM.numDepositID=DED.numDepositID
--			 WHERE DEM.tintDepositePage=1 AND DEM.numDomainId=@numDomainID  
--					AND DED.numProjectID = @numProId
			
--			UNION ALL
					
--			select 7 as iTEType,'Check' as vcTimeExpenseType,CD.vcDescription as vcItemName,0 as numUnitHour,0 as monPrice,CD.monAmount AS ExpenseAmount,
--				dbo.[FormatedDateFromDate](CH.dtCreatedDate, @numDomainID) AS dtDateEntered,
--                    dbo.fn_GetContactName(CH.numCreatedBy) AS vcCreatedBy,0 as numOppId,0 as numOppType,
--					0 as numCategoryHDRID,0 as numProjectStageID,0 AS numBillId,
--					'' as vcFrom,
--					CD.numProjectID AS numProID,CH.dtCreatedDate as dtCreatedOn,'' as vcBizDoc,0 AS numDepositId,CH.numCheckHeaderID,0 AS numJournalId
--					,0 AS monAvgCost,0 AS monVendorCost
--			 from  CheckHeader CH JOIN CheckDetails CD ON CH.numCheckHeaderID=CD.numCheckHeaderID
--			 WHERE CH.tintReferenceType=1 AND CH.numDomainId=@numDomainID  
--					AND CD.numProjectID = @numProId		
			
--			UNION ALL
			
--			select 9 as iTEType,'Journal' as vcTimeExpenseType,GD.varDescription as vcItemName,0 as numUnitHour,0 as monPrice,GD.numDebitAmt AS ExpenseAmount,
--				dbo.[FormatedDateFromDate](GH.datCreatedDate, @numDomainID) AS dtDateEntered,
--                    dbo.fn_GetContactName(GH.numCreatedBy) AS vcCreatedBy,0 as numOppId,0 as numOppType,
--					0 as numCategoryHDRID,0 as numProjectStageID,0 AS numBillId,
--					'' as vcFrom,
--					GD.numProjectID AS numProID,GH.datCreatedDate as dtCreatedOn,'' as vcBizDoc,0 AS numDepositId,0 AS numCheckHeaderID,GD.numJournalId
--					,0 AS monAvgCost,0 AS monVendorCost
--			 from  dbo.General_Journal_Header GH JOIN dbo.General_Journal_Details GD ON GH.numJournal_Id = GD.numJournalId
--					JOIN dbo.Chart_Of_Accounts COA ON COA.numAccountId = GD.numChartAcntId
--			 WHERE ISNULL(GH.numCheckHeaderID,0)=0 AND ISNULL(GH.numBillPaymentID,0)=0 AND ISNULL(GH.numBillID,0)=0
--					AND ISNULL(GH.numDepositId,0)=0 AND ISNULL(GH.numOppId,0)=0 AND ISNULL(GH.numOppBizDocsId,0)=0
--					AND ISNULL(GH.numReturnId,0)=0
--					AND GH .numDomainId=@numDomainID  AND GD.numProjectID = @numProId
--					AND ISNULL(GD.numDebitAmt,0)>0	AND (COA.vcAccountCode LIKE '0103%' OR  COA.vcAccountCode LIKE '0104%' OR  COA.vcAccountCode LIKE '0106%')	
				
--			UNION ALL
					
--			 select 8 as iTEType,'Journal' as vcTimeExpenseType,GD.varDescription as vcItemName,0 as numUnitHour,0 as monPrice,GD.numCreditAmt AS ExpenseAmount,
--				dbo.[FormatedDateFromDate](GH.datCreatedDate, @numDomainID) AS dtDateEntered,
--                    dbo.fn_GetContactName(GH.numCreatedBy) AS vcCreatedBy,0 as numOppId,0 as numOppType,
--					0 as numCategoryHDRID,0 as numProjectStageID,0 AS numBillId,
--					'' as vcFrom,
--					GD.numProjectID AS numProID,GH.datCreatedDate as dtCreatedOn,'' as vcBizDoc,0 AS numDepositId,0 AS numCheckHeaderID,GD.numJournalId
--					,0 AS monAvgCost,0 AS monVendorCost
--			 from  dbo.General_Journal_Header GH JOIN dbo.General_Journal_Details GD ON GH.numJournal_Id = GD.numJournalId
--					JOIN dbo.Chart_Of_Accounts COA ON COA.numAccountId = GD.numChartAcntId
--			 WHERE ISNULL(GH.numCheckHeaderID,0)=0 AND ISNULL(GH.numBillPaymentID,0)=0 AND ISNULL(GH.numBillID,0)=0
--					AND ISNULL(GH.numDepositId,0)=0 AND ISNULL(GH.numOppId,0)=0 AND ISNULL(GH.numOppBizDocsId,0)=0
--					AND ISNULL(GH.numReturnId,0)=0
--					AND GH .numDomainId=@numDomainID  AND GD.numProjectID = @numProId
--					AND ISNULL(GD.numCreditAmt,0)>0  AND (COA.vcAccountCode LIKE '0103%' OR  COA.vcAccountCode LIKE '0104%' OR  COA.vcAccountCode LIKE '0106%')
					
								
--			order by dtCreatedOn
-- END 

--ELSE IF @tintMode=1 --Use Auth BizDoc Amounts
--BEGIN
--            SELECT 1 as iTEType,'Sales Order' AS vcTimeExpenseType,I.[vcItemName],
--                    ISNULL(X.[numUnitHour], 0) AS [numUnitHour],
--                    ISNULL(OPP.[monPrice], 0) monPrice,
--					ISNULL(OPP.[monPrice], 0) * ISNULL(X.numUnitHour, 0) - (CASE WHEN I.charItemType='P' THEN CASE WHEN ISNULL(Opp.monAvgCost,0)=0 THEN ISNULL(Opp.monVendorCost,0) ELSE ISNULL(Opp.monAvgCost,0) END ELSE 0 END) AS ExpenseAmount,
--                    dbo.[FormatedDateFromDate](TE.[dtTCreatedOn], @numDomainID) AS dtDateEntered,
--                    dbo.fn_GetContactName(TE.numTCreatedBy) AS vcCreatedBy,TE.numOppId,1 as numOppType,
--					TE.[numCategoryHDRID],isnull(OPP.numProjectStageID,0) as numProjectStageID,0 as numBillId,
--					Case When OPP.numProjectStageID>0 then dbo.fn_GetProjectStageName(OPP.numProjectId,OPP.numProjectStageID) when OPP.numProjectStageID=-1 then 'T&E' else 'S.O.' end as vcFrom,
--					OPP.numProjectId as numProId,TE.[dtTCreatedOn] as dtCreatedOn,isnull((SELECT SUBSTRING((SELECT ',' + Cast(OB.numOppBizDocsId as varchar(18)) from  [OpportunityBizDocs] OB 
--					INNER JOIN [OpportunityBizDocItems] OBI ON OBI.numOppBizDocID = OB.numOppBizDocsId AND OPP.numoppitemtCode = OBI.numOppItemID 
--					WHERE OB.bitAuthoritativeBizDocs=1 and OB.numOppId = OM.numOppId FOR XML PATH('')),2,200000)),'') AS vcBizDoc,0 AS numDepositId,0 AS numCheckHeaderID,0 AS numJournalId
--					,ISNULL(Opp.monAvgCost,0) AS monAvgCost,ISNULL(Opp.monVendorCost,0) AS monVendorCost
--            FROM    [TimeAndExpense] TE
--                    INNER JOIN OpportunityMaster OM ON TE.numOppId = OM.numOppId
--                    INNER JOIN OpportunityItems OPP ON OPP.numOppId = TE.numOppId
--							AND OPP.numoppitemtCode = TE.numOppItemID AND OPP.numProjectId=TE.numProId
--					INNER JOIN Item I ON OPP.[numItemCode] = I.[numItemCode]
--					Inner Join (select sum(ISNULL(OBI.[numUnitHour], 0)) as numUnitHour,OM.numOppId,OPP.numoppitemtCode from  [TimeAndExpense] TE
--                    INNER JOIN OpportunityMaster OM ON TE.numOppId = OM.numOppId
--                    INNER JOIN OpportunityItems OPP ON OPP.numOppId = TE.numOppId
--							AND OPP.numoppitemtCode = TE.numOppItemID AND OPP.numProjectId=TE.numProId
--					INNER JOIN [OpportunityBizDocs] OB ON OB.numOppId = OM.numOppId
--					INNER JOIN [OpportunityBizDocItems] OBI ON OBI.numOppBizDocID = OB.numOppBizDocsId 
--						AND OPP.numoppitemtCode = OBI.numOppItemID 
--					WHERE [numProId] = @numProId AND TE.[numDomainID] = @numDomainID AND numType =1 and OB.bitAuthoritativeBizDocs=1
--						group by OM.numOppId,OPP.numoppitemtCode) X on
--					X.numOppId = OM.numOppId and OPP.numoppitemtCode = X.numoppitemtCode
--            WHERE   [numProId] = @numProId
--                    AND TE.[numDomainID] = @numDomainID
--                    AND numType =1 --Billable 
--					AND OM.[tintOppType] = 1

--			UNION ALL
			
--			SELECT 1 as iTEType,'Sales Order' AS vcTimeExpenseType,I.[vcItemName],
--                    ISNULL(X.numUnitHour, 0) AS [numUnitHour],
--                    ISNULL(OPP.[monPrice], 0) monPrice,
--					ISNULL(OPP.[monPrice], 0) * ISNULL(X.numUnitHour, 0) - (CASE WHEN I.charItemType='P' THEN CASE WHEN ISNULL(Opp.monAvgCost,0)=0 THEN ISNULL(Opp.monVendorCost,0) ELSE ISNULL(Opp.monAvgCost,0) END ELSE 0 END)  AS ExpenseAmount,
--                    dbo.[FormatedDateFromDate](OM.bintCreatedDate, @numDomainID) AS dtDateEntered,
--                    dbo.fn_GetContactName(OM.numCreatedBy) AS vcCreatedBy,OM.numOppId,1 as numOppType,
--					0 as numCategoryHDRID,isnull(OPP.numProjectStageID,0) as numProjectStageID,0 as numBillId,
--					Case When OPP.numProjectStageID>0 then dbo.fn_GetProjectStageName(OPP.numProjectId,OPP.numProjectStageID) when OPP.numProjectStageID=-1 then 'T&E' else 'S.O.' end as vcFrom,
--					OPP.numProjectId as numProId,OM.bintCreatedDate as dtCreatedOn,isnull((SELECT SUBSTRING((SELECT ',' + Cast(OB.numOppBizDocsId as varchar(18)) from  [OpportunityBizDocs] OB 
--					INNER JOIN [OpportunityBizDocItems] OBI ON OBI.numOppBizDocID = OB.numOppBizDocsId AND OPP.numoppitemtCode = OBI.numOppItemID 
--					WHERE OB.bitAuthoritativeBizDocs=1 and OB.numOppId = OM.numOppId FOR XML PATH('')),2,200000)),'') AS vcBizDoc,0 AS numDepositId,0 AS numCheckHeaderID,0 AS numJournalId
--					,ISNULL(Opp.monAvgCost,0) AS monAvgCost,ISNULL(Opp.monVendorCost,0) AS monVendorCost
--            FROM    OpportunityMaster OM INNER JOIN OpportunityItems OPP ON OPP.numOppId = OM.numOppId
--					INNER JOIN Item I ON OPP.[numItemCode] = I.[numItemCode]
--					Inner Join (select sum(ISNULL(OBI.[numUnitHour], 0)) as numUnitHour,OM.numOppId,OPP.numoppitemtCode from  
--                    OpportunityMaster OM INNER JOIN OpportunityItems OPP ON OPP.numOppId = OM.numOppId
--					INNER JOIN [OpportunityBizDocs] OB ON OB.numOppId = OM.numOppId
--					INNER JOIN [OpportunityBizDocItems] OBI ON OBI.numOppBizDocID = OB.numOppBizDocsId 
--						AND OPP.numoppitemtCode = OBI.numOppItemID 
--					WHERE OM.[numDomainID] = @numDomainID AND OPP.numProjectID = @numProId AND OPP.numProjectStageID=0 and OB.bitAuthoritativeBizDocs=1
--						group by OM.numOppId,OPP.numoppitemtCode) X on
--					X.numOppId = OM.numOppId and OPP.numoppitemtCode = X.numoppitemtCode
--            WHERE  OM.[numDomainID] = @numDomainID AND OPP.numProjectID = @numProId
--                    AND OPP.numProjectStageID=0 AND OM.[tintOppType] = 1

--			Union ALL

--			SELECT  2 as iTEType,'Non-Billable Time' AS vcTimeExpenseType,
--                    ISNULL(CONVERT(VARCHAR(25),txtDesc) + '..' ,'') AS [vcItemName],
--                    ISNULL(CONVERT(VARCHAR(100), CONVERT(DECIMAL(10, 1), ( CONVERT(DECIMAL(10, 1), DATEDIFF(minute, TE.dtFromDate, TE.dtToDate)) / 60 ))),0)
--                    AS [numUnitHour],
--                    0 monPrice,
--					0 AS ExpenseAmount,
--                    dbo.[FormatedDateFromDate](TE.[dtTCreatedOn], @numDomainID) AS dtDateEntered,
--                    dbo.fn_GetContactName(TE.numTCreatedBy) AS vcCreatedBy,0 as numOppId,0 as numOppType,
--					TE.[numCategoryHDRID],isnull(TE.numStageId,0) as numProjectStageID,0 as numBillId,
--					Case When TE.numStageId>0 then dbo.fn_GetProjectStageName(TE.numProId,TE.numStageId) else 'Time' end as vcFrom,
--					TE.numProId,TE.[dtTCreatedOn] as dtCreatedOn,'' as vcBizDoc,0 AS numDepositId,0 AS numCheckHeaderID,0 AS numJournalId
--					,0 AS monAvgCost,0 AS monVendorCost
--            FROM    [TimeAndExpense] TE
--            WHERE   [numProId] = @numProId
--                    AND TE.[numDomainID] = @numDomainID
--                    AND numType =2 --Non Billable

--		    UNION ALL

--            select 3 as iTEType,'Purchase Order' AS vcTimeExpenseType,I.[vcItemName],
--                    ISNULL(X.[numUnitHour], 0) as numUnitHour,
--                    ISNULL(OPP.[monPrice], 0) monPrice,
--	   			    (OPP.[monPrice] * X.[numUnitHour]) AS ExpenseAmount,
--					dbo.[FormatedDateFromDate](OM.bintCreatedDate, @numDomainID) AS dtDateEntered,
--                    dbo.fn_GetContactName(OM.numCreatedBy) AS vcCreatedBy,OM.numOppId,2 as numOppType,
--					0 as numCategoryHDRID,isnull(OPP.numProjectStageID,0) as numProjectStageID,0 as numBillId,
--					Case When OPP.numProjectStageID>0 then dbo.fn_GetProjectStageName(OPP.numProjectId,OPP.numProjectStageID) else 'P.O.' end as vcFrom,
--					OPP.numProjectID as numProId,OM.bintCreatedDate as dtCreatedOn,isnull((SELECT SUBSTRING((SELECT ',' + Cast(OB.numOppBizDocsId as varchar(18)) from  [OpportunityBizDocs] OB 
--					INNER JOIN [OpportunityBizDocItems] OBI ON OBI.numOppBizDocID = OB.numOppBizDocsId AND OPP.numoppitemtCode = OBI.numOppItemID 
--					WHERE OB.bitAuthoritativeBizDocs=1 and OB.numOppId = OM.numOppId FOR XML PATH('')),2,200000)),'') AS vcBizDoc,0 AS numDepositId,0 AS numCheckHeaderID,0 AS numJournalId
--					,ISNULL(Opp.monAvgCost,0) AS monAvgCost,ISNULL(Opp.monVendorCost,0) AS monVendorCost
--			 from  [OpportunityMaster] OM 
--						JOIN [OpportunityItems] OPP ON OM.[numOppId] = OPP.[numOppId]
--                        LEFT OUTER JOIN [Item] I ON OPP.[numItemCode] = I.[numItemCode]
--						LEFT OUTER JOIN [Chart_Of_Accounts] COA ON I.[numCOGsChartAcntId] = COA.[numAccountId]
--						Inner Join (select sum(ISNULL(OBI.[numUnitHour], 0)) as numUnitHour,OM.numOppId,OPP.numoppitemtCode from  
--                    OpportunityMaster OM 
--                    JOIN [OpportunityItems] OPP ON OM.[numOppId] = OPP.[numOppId]
--					INNER JOIN [OpportunityBizDocs] OB ON OB.numOppId = OM.numOppId
--					INNER JOIN [OpportunityBizDocItems] OBI ON OBI.numOppBizDocID = OB.numOppBizDocsId 
--						AND OPP.numoppitemtCode = OBI.numOppItemID 
--					WHERE OM.numDomainId=@numDomainID AND OPP.numProjectID = @numProId AND OM.[tintOppType] = 2 and OB.bitAuthoritativeBizDocs=1
--						group by OM.numOppId,OPP.numoppitemtCode) X on
--					X.numOppId = OM.numOppId and OPP.numoppitemtCode = X.numoppitemtCode
--			 where  OM.numDomainId=@numDomainID  
--					AND OPP.numProjectID = @numProId
--					AND OM.[tintOppType] = 2

--			 Union ALL
			
--			select 4 as iTEType,'Bill' as vcTimeExpenseType,BD.vcDescription as vcItemName,0 as numUnitHour,0 as monPrice,BD.monAmount AS ExpenseAmount,
--				dbo.[FormatedDateFromDate](BH.dtCreatedDate, @numDomainID) AS dtDateEntered,
--                    dbo.fn_GetContactName(BH.numCreatedBy) AS vcCreatedBy,0 as numOppId,0 as numOppType,
--					0 as numCategoryHDRID,isnull(PO.numStageID,0) as numProjectStageID,PO.numBillId,
--					Case When PO.numStageID>0 then dbo.fn_GetProjectStageName(PO.numProID,PO.numStageID) else 'Bill' end as vcFrom,
--					PO.numProID,BH.dtCreatedDate as dtCreatedOn,'' as vcBizDoc,0 AS numDepositId,0 AS numCheckHeaderID,0 AS numJournalId
--					,0 AS monAvgCost,0 AS monVendorCost
--			 from  ProjectsOpportunities PO 
--				   JOIN BillHeader BH ON BH.numBillID = PO.numBillID
--				   JOIN BillDetails BD ON BH.numBillID=BD.numBillID AND PO.numProID=BD.numProjectID
--			 where  PO.numDomainId=@numDomainID  
--					AND PO.numProID = @numProId
--					and isnull(PO.numBillId,0)>0
			
--			UNION ALL

--			SELECT  5 as iTEType,'Employee Expense' AS vcTimeExpenseType,
--                    ISNULL(CONVERT(VARCHAR(25),txtDesc) + '..' ,'') AS [vcItemName],
--                    ISNULL(CONVERT(VARCHAR(100), CONVERT(DECIMAL(10, 1), ( CONVERT(DECIMAL(10, 1), DATEDIFF(minute, TE.dtFromDate, TE.dtToDate)) / 60 ))),0)
--                    AS [numUnitHour],
--                    isnull(UM.monHourlyRate,0) monPrice,
--					isnull(Cast(datediff(minute,TE.dtfromdate,dttodate) as float)/Cast(60 as float),0) * isnull(UM.monHourlyRate,0) AS ExpenseAmount,
--                    dbo.[FormatedDateFromDate](TE.[dtTCreatedOn], @numDomainID) AS dtDateEntered,
--                    dbo.fn_GetContactName(TE.numTCreatedBy) AS vcCreatedBy,0 as numOppId,0 as numOppType,
--					TE.[numCategoryHDRID],isnull(TE.numStageId,0) as numProjectStageID,0 as numBillId,
--					Case When TE.numStageId>0 then dbo.fn_GetProjectStageName(TE.numProId,TE.numStageId) else 'Expense' end as vcFrom,
--					TE.numProId,TE.[dtTCreatedOn] as dtCreatedOn,'' as vcBizDoc,0 AS numDepositId,0 AS numCheckHeaderID,0 AS numJournalId
--					,0 AS monAvgCost,0 AS monVendorCost
--            FROM    [TimeAndExpense] TE join UserMaster UM on UM.numUserDetailId=TE.numUserCntID
--            WHERE   [numProId] = @numProId
--                    AND TE.[numDomainID] = @numDomainID
--                    AND numType in (1,2) --Non Billable & Billable
--					AND TE.numUserCntID>0
----					AND bitReimburse=1 and  numCategory=2  

--			select 6 as iTEType,'Deposit' as vcTimeExpenseType,DED.vcMemo as vcItemName,0 as numUnitHour,0 as monPrice,DED.monAmountPaid AS ExpenseAmount,
--				dbo.[FormatedDateFromDate](DEM.dtCreationDate, @numDomainID) AS dtDateEntered,
--                    dbo.fn_GetContactName(DEM.numCreatedBy) AS vcCreatedBy,0 as numOppId,0 as numOppType,
--					0 as numCategoryHDRID,0 as numProjectStageID,0 AS numBillId,
--					'' as vcFrom,
--					DED.numProjectID AS numProID,DEM.dtCreationDate as dtCreatedOn,'' as vcBizDoc,DEM.numDepositId,0 AS numCheckHeaderID,0 AS numJournalId
--					,0 AS monAvgCost,0 AS monVendorCost
--			 from  DepositMaster DEM JOIN DepositeDetails DED ON DEM.numDepositID=DED.numDepositID
--			 WHERE DEM.tintDepositePage=1 AND DEM.numDomainId=@numDomainID  
--					AND DED.numProjectID = @numProId
				
--			UNION ALL
					
--			select 7 as iTEType,'Check' as vcTimeExpenseType,CD.vcDescription as vcItemName,0 as numUnitHour,0 as monPrice,CD.monAmount AS ExpenseAmount,
--				dbo.[FormatedDateFromDate](CH.dtCreatedDate, @numDomainID) AS dtDateEntered,
--                    dbo.fn_GetContactName(CH.numCreatedBy) AS vcCreatedBy,0 as numOppId,0 as numOppType,
--					0 as numCategoryHDRID,0 as numProjectStageID,0 AS numBillId,
--					'' as vcFrom,
--					CD.numProjectID AS numProID,CH.dtCreatedDate as dtCreatedOn,'' as vcBizDoc,0 AS numDepositId,CH.numCheckHeaderID,0 AS numJournalId
--					,0 AS monAvgCost,0 AS monVendorCost
--			 from  CheckHeader CH JOIN CheckDetails CD ON CH.numCheckHeaderID=CD.numCheckHeaderID
--			 WHERE CH.tintReferenceType=1 AND CH.numDomainId=@numDomainID  
--					AND CD.numProjectID = @numProId		
			
--			UNION ALL
			
--			select 9 as iTEType,'Journal' as vcTimeExpenseType,GD.varDescription as vcItemName,0 as numUnitHour,0 as monPrice,GD.numDebitAmt AS ExpenseAmount,
--				dbo.[FormatedDateFromDate](GH.datCreatedDate, @numDomainID) AS dtDateEntered,
--                    dbo.fn_GetContactName(GH.numCreatedBy) AS vcCreatedBy,0 as numOppId,0 as numOppType,
--					0 as numCategoryHDRID,0 as numProjectStageID,0 AS numBillId,
--					'' as vcFrom,
--					GD.numProjectID AS numProID,GH.datCreatedDate as dtCreatedOn,'' as vcBizDoc,0 AS numDepositId,0 AS numCheckHeaderID,GD.numJournalId
--					,0 AS monAvgCost,0 AS monVendorCost
--			 from  dbo.General_Journal_Header GH JOIN dbo.General_Journal_Details GD ON GH.numJournal_Id = GD.numJournalId
--					JOIN dbo.Chart_Of_Accounts COA ON COA.numAccountId = GD.numChartAcntId
--			 WHERE ISNULL(GH.numCheckHeaderID,0)=0 AND ISNULL(GH.numBillPaymentID,0)=0 AND ISNULL(GH.numBillID,0)=0
--					AND ISNULL(GH.numDepositId,0)=0 AND ISNULL(GH.numOppId,0)=0 AND ISNULL(GH.numOppBizDocsId,0)=0
--					AND ISNULL(GH.numReturnId,0)=0
--					AND GH .numDomainId=@numDomainID  AND GD.numProjectID = @numProId
--					AND ISNULL(GD.numDebitAmt,0)>0	AND (COA.vcAccountCode LIKE '0103%' OR  COA.vcAccountCode LIKE '0104%' OR  COA.vcAccountCode LIKE '0106%')	
				
--			UNION ALL
					
--			 select 8 as iTEType,'Journal' as vcTimeExpenseType,GD.varDescription as vcItemName,0 as numUnitHour,0 as monPrice,GD.numCreditAmt AS ExpenseAmount,
--				dbo.[FormatedDateFromDate](GH.datCreatedDate, @numDomainID) AS dtDateEntered,
--                    dbo.fn_GetContactName(GH.numCreatedBy) AS vcCreatedBy,0 as numOppId,0 as numOppType,
--					0 as numCategoryHDRID,0 as numProjectStageID,0 AS numBillId,
--					'' as vcFrom,
--					GD.numProjectID AS numProID,GH.datCreatedDate as dtCreatedOn,'' as vcBizDoc,0 AS numDepositId,0 AS numCheckHeaderID,GD.numJournalId
--					,0 AS monAvgCost,0 AS monVendorCost
--			 from  dbo.General_Journal_Header GH JOIN dbo.General_Journal_Details GD ON GH.numJournal_Id = GD.numJournalId
--					JOIN dbo.Chart_Of_Accounts COA ON COA.numAccountId = GD.numChartAcntId
--			 WHERE ISNULL(GH.numCheckHeaderID,0)=0 AND ISNULL(GH.numBillPaymentID,0)=0 AND ISNULL(GH.numBillID,0)=0
--					AND ISNULL(GH.numDepositId,0)=0 AND ISNULL(GH.numOppId,0)=0 AND ISNULL(GH.numOppBizDocsId,0)=0
--					AND ISNULL(GH.numReturnId,0)=0
--					AND GH .numDomainId=@numDomainID  AND GD.numProjectID = @numProId
--					AND ISNULL(GD.numCreditAmt,0)>0  AND (COA.vcAccountCode LIKE '0103%' OR  COA.vcAccountCode LIKE '0104%' OR  COA.vcAccountCode LIKE '0106%')
									
--			order by dtCreatedOn
-- END 
	   
END        
        

