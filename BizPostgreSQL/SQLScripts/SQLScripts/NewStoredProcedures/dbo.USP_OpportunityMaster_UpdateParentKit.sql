GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_OpportunityMaster_UpdateParentKit')
DROP PROCEDURE dbo.USP_OpportunityMaster_UpdateParentKit
GO
CREATE PROCEDURE [dbo].[USP_OpportunityMaster_UpdateParentKit]
(
	@numDomainID NUMERIC(18,0),
    @numOppID NUMERIC(18,0),
    @numOppItemID NUMERIC(18,0),
	@numParentOppItemID NUMERIC(18,0)
)
AS
BEGIN
	UPDATE 
		OpportunityItems 
	SET 
		numParentOppItemID=@numParentOppItemID
	WHERE 
		numOppId=@numOppID 
		AND numoppitemtCode=@numOppItemID
END
GO