GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_BizRecurrence_WorkOrder')
DROP PROCEDURE USP_BizRecurrence_WorkOrder
GO
  
Create PROCEDURE [dbo].[USP_BizRecurrence_WorkOrder]   
	@numDomainID NUMERIC(18,0),
	@numUserCntID NUMERIC(18,0),
	@numOppID NUMERIC(18,0),
	@numParentOppID NUMERIC(18,0)
AS  
BEGIN  

IF (SELECT COUNT(*) FROM WorkOrder WHERE numOppId=@numOppID) = 0 AND
	(SELECT COUNT(*) FROM WorkOrder WHERE numOppId=@numParentOppID) > 0
BEGIN
   
	DECLARE @numItemCode AS numeric(9),@numUnitHour AS FLOAT,@numWarehouseItmsID AS numeric(9),@vcItemDesc AS VARCHAR(1000), @numWarehouseID NUMERIC(18,0)
  
	DECLARE @numWOId AS NUMERIC(9),@vcInstruction AS VARCHAR(1000),@bintCompliationDate AS DATETIME,@numWOAssignedTo NUMERIC(9)

	DECLARE @numoppitemtCode NUMERIC(18,0)

	DECLARE @TEMPitem TABLE
	(
		numItemKitID NUMERIC(18,0)
		,numItemCode NUMERIC(18,0)
		,numWarehouseItmsID NUMERIC(18,0)
		,numCalculatedQty FLOAT
		,txtItemDesc VARCHAR(500)
		,sintOrder INT
		,numUOMId NUMERIC(18,0)
		,numQtyItemsReq FLOAT
		,charItemType CHAR
	)


	DECLARE WOCursor CURSOR FOR 
		SELECT 
			X.numoppitemtCode,
			X.numItemCode,
			dbo.fn_UOMConversion(X.numUOMId,numItemCode,@numDomainId,null) * x.numUnitHour AS numUnitHour,
			X.numWarehouseItmsID,
			X.vcItemDesc,
			X.vcInstruction,
			X.bintCompliationDate,
			X.numAssignedTo
		FROM
			(
				SELECT
					OpportunityItems.numoppitemtCode,
					OpportunityItems.numItemCode,
					OpportunityItems.bitWorkOrder,
					OpportunityItems.numUOMId,
					OpportunityItems.numUnitHour,
					OpportunityItems.numWarehouseItmsID,
					OpportunityItems.vcItemDesc,
					WorkOrder.vcInstruction,
					WorkOrder.bintCompliationDate,
					WorkOrder.numAssignedTo
				FROM
					OpportunityItems
				INNER JOIN	
					WorkOrder
				ON
					WorkOrder.numItemCode = OpportunityItems.numItemCode AND
					WorkOrder.numWareHouseItemId = OpportunityItems.numWarehouseItmsID AND
					WorkOrder.numOppId = OpportunityItems.numOppId
				WHERE
					OpportunityItems.numOppId = @numParentOppID
			)X  
		WHERE 
			X.bitWorkOrder=1

	OPEN WOCursor;

	FETCH NEXT FROM WOCursor INTO @numoppitemtCode,@numItemCode, @numUnitHour,@numWarehouseItmsID,@vcItemDesc,@vcInstruction,@bintCompliationDate,@numWOAssignedTo

	WHILE @@FETCH_STATUS = 0
	BEGIN
		DELETE FROM @TEMPitem
		SET @numWarehouseID = ISNULL((SELECT numWareHouseID FROM WarehouseItems WHERE numWarehouseitemID=@numWarehouseItmsID),0)

		;WITH CTE(numItemKitID,numItemCode,numCalculatedQty,txtItemDesc,sintOrder,numUOMId,numQtyItemsReq,charItemType)
		AS
		(
			select CAST(@numItemCode AS NUMERIC(9)),numItemCode
			,CAST(((DTL.numQtyItemsReq * ISNULL(dbo.fn_UOMConversion(Dtl.numUOMID,item.numItemCode,item.numDomainID,item.numBaseUnit),1)) * @numUnitHour)AS FLOAT) AS numCalculatedQty,ISNULL(Dtl.vcItemDesc,txtItemDesc) AS txtItemDesc,ISNULL(sintOrder,0) AS sintOrder,
			Dtl.numUOMId,DTL.numQtyItemsReq,item.charItemType
			from item                                
			INNER join ItemDetails Dtl on numChildItemID=numItemCode
			where  numItemKitID=@numItemCode 
			UNION ALL
			select CAST(Dtl.numItemKitID AS NUMERIC(9)),i.numItemCode,
			CAST(((DTL.numQtyItemsReq * ISNULL(dbo.fn_UOMConversion(Dtl.numUOMID,i.numItemCode,i.numDomainID,i.numBaseUnit),1)) * c.numCalculatedQty) AS FLOAT) AS numCalculatedQty,ISNULL(Dtl.vcItemDesc,i.txtItemDesc) AS txtItemDesc,ISNULL(Dtl.sintOrder,0) AS sintOrder,
			Dtl.numUOMId,DTL.numQtyItemsReq,i.charItemType
			from item i                               
			INNER JOIN ItemDetails Dtl on Dtl.numChildItemID=i.numItemCode
			INNER JOIN CTE c ON Dtl.numItemKitID = c.numItemCode 
			--WHERE i.charItemType IN ('P','I')
			where Dtl.numChildItemID!=@numItemCode
		)

		INSERT INTO @TEMPitem
		(
			numItemKitID
			,numItemCode
			,numCalculatedQty
			,numWarehouseItmsID
			,txtItemDesc
			,sintOrder
			,numQtyItemsReq
			,numUOMId 
			,charItemType
		)
		SELECT 
			numItemKitID
			,numItemCode
			,numCalculatedQty
			,ISNULL((SELECT TOP 1 numWareHouseItemId FROM WarehouseItems WHERE numItemID=numItemCode AND numWareHouseID=@numWarehouseID ORDER BY numWareHouseItemID),0)
			,txtItemDesc
			,sintOrder
			,numQtyItemsReq
			,numUOMId
			,charItemType
		FROM 
			CTE

		IF EXISTS (SELECT numItemKitID FROM @TEMPitem WHERE charItemType='P' AND ISNULL(numWarehouseItmsID,0) = 0)
		BEGIN
			RAISERROR('SELECTED_WAREHOUSE_IS_NOT_AVAILABLE_FOR_ALL_CHILD_ITEMS',16,1)
			RETURN
		END

	INSERT INTO WorkOrder
	(
		numItemCode, numQtyItemsReq, numWareHouseItemId, numCreatedBy, bintCreatedDate,	numDomainID,
		numWOStatus, numOppId, vcItemDesc, vcInstruction, bintCompliationDate, numAssignedTo, numOppItemID
	)
	VALUES
	(
		@numItemCode,@numUnitHour,@numWarehouseItmsID,@numUserCntID,getutcdate(),@numDomainID,
		0,@numOppID,@vcItemDesc,@vcInstruction,@bintCompliationDate,@numWOAssignedTo, @numoppitemtCode
	)
	
	set @numWOId=@@IDENTITY		
	

	INSERT INTO [WorkOrderDetails] 
	(
		numWOId,numItemKitID,numChildItemID,numQtyItemsReq,numWareHouseItemId,vcItemDesc,sintOrder,numQtyItemsReq_Orig,numUOMId
	)
	SELECT 
		@numWOId,numItemKitID,numItemCode,numCalculatedQty,numWarehouseItmsID,txtItemDesc,sintOrder,numQtyItemsReq,numUOMId FROM CTE
	
	FETCH NEXT FROM WOCursor INTO @numoppitemtCode, @numItemCode, @numUnitHour,@numWarehouseItmsID,@vcItemDesc,@vcInstruction,@bintCompliationDate,@numWOAssignedTo
END

	CLOSE WOCursor;
	DEALLOCATE WOCursor;
 
END 

END  
