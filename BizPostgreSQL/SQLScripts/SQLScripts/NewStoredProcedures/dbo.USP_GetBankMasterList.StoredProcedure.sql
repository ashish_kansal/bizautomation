
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetBankMasterList')
	DROP PROCEDURE USP_GetBankMasterList
GO
/****** Added By : Joseph ******/
/****** Gets Financial Institutions List  from Table BankMaster******/

CREATE PROCEDURE [dbo].[USP_GetBankMasterList]
as
SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

BEGIN  
SELECT numBankMasterID,vcFIName FROM BankMaster




END