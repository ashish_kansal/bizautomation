SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ReportListMaster_Top10CampaignsByROI')
DROP PROCEDURE USP_ReportListMaster_Top10CampaignsByROI
GO
CREATE PROCEDURE [dbo].[USP_ReportListMaster_Top10CampaignsByROI]
	@numDomainID NUMERIC(18,0)
AS
BEGIN 
	SELECT TOP 10
		[vcCampaignName],
		CONCAT('~/Marketing/frmCampaignDetails.aspx?CampID=',numCampaignId) URL,
		dbo.GetIncomeforCampaign(numCampaignId,4,NULL,NULL) as ROI
	FROM 
		CampaignMaster
	WHERE
		numDomainID=@numDomainID
		AND dbo.GetIncomeforCampaign(numCampaignId,4,NULL,NULL) <> 0
	ORDER BY
		dbo.GetIncomeforCampaign(numCampaignId,4,NULL,NULL) DESC
END
GO

