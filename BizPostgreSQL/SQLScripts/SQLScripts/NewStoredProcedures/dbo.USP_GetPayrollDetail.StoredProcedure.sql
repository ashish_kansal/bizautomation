
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetPayrollDetail')
DROP PROCEDURE USP_GetPayrollDetail
GO
CREATE PROCEDURE [dbo].[USP_GetPayrollDetail]
(
    @numDomainId AS NUMERIC(9) = 0,
    @numPayrollHeaderID AS NUMERIC(9)=0,
    @ClientTimeZoneOffset INT
)
AS
  BEGIN
  	DECLARE @dtStartDate AS DATETIME
DECLARE @dtEndDate AS DATETIME
 
SELECT  numPayrollHeaderID,
		numPayrolllReferenceNo,
		numDomainId,
		dtFromDate,
		dtToDate,
		dtPaydate,
		numCreatedBy,
		dtCreatedDate,
		numModifiedBy,
		dtModifiedDate,
		ISNULL(numPayrollStatus,0) AS [numPayrollStatus] 
FROM  PayrollHeader WHERE numPayrollHeaderID=@numPayrollHeaderID AND numDomainId=@numDomainId

SELECT @dtStartDate=dtFromDate,@dtEndDate=dtToDate FROM dbo.PayrollHeader WHERE numPayrollHeaderID=@numPayrollHeaderID AND numDomainId=@numDomainId

CREATE TABLE #tempHrs (numPayrollDetailID NUMERIC,numCheckStatus NUMERIC,numUserId NUMERIC,vcUserName VARCHAR(100)
,numUserCntID NUMERIC,vcEmployeeId VARCHAR(50),monHourlyRate DECIMAL(20,5),bitOverTime BIT,bitOverTimeHrsDailyOrWeekly BIT,numLimDailHrs FLOAT,monOverTimeRate DECIMAL(20,5),
 decRegularHrs decimal(10,2),decActualRegularHrs decimal(10,2),decOvertimeHrs decimal(10,2),decActualOvertimeHrs decimal(10,2),
 decPaidLeaveHrs decimal(10,2),decActualPaidLeaveHrs decimal(10,2),decTotalHrs decimal(10,2),decActualTotalHrs decimal(10,2),
 monExpense DECIMAL(20,5),monActualExpense DECIMAL(20,5),monReimburse DECIMAL(20,5),monActualReimburse DECIMAL(20,5),
 monCommPaidInvoice DECIMAL(20,5),monActualCommPaidInvoice DECIMAL(20,5),monCommUNPaidInvoice DECIMAL(20,5),monActualCommUNPaidInvoice DECIMAL(20,5),monDeductions DECIMAL(20,5))

 INSERT INTO #tempHrs(numPayrollDetailID,numCheckStatus,numUserId,vcUserName,numUserCntID,vcEmployeeId,monHourlyRate,bitOverTime,bitOverTimeHrsDailyOrWeekly,numLimDailHrs,monOverTimeRate,monDeductions)
 SELECT ISNULL(PD.numPayrollDetailID,0) AS numPayrollDetailID,ISNULL(PD.numCheckStatus,0) AS numCheckStatus,UM.numUserId,Isnull(ADC.vcfirstname + ' ' + adc.vclastname,'-') vcUserName,
           adc.numcontactid AS numUserCntID,Isnull(um.vcEmployeeId,'') AS vcEmployeeId,
 CASE WHEN ISNULL(PD.numPayrollDetailID,0)>0 THEN ISNULL(PD.monHourlyRate,0) ELSE ISNULL(UM.monHourlyRate,0) END AS monHourlyRate,
 CASE WHEN ISNULL(PD.numPayrollDetailID,0)>0 THEN ISNULL(PD.bitOverTime,0) ELSE ISNULL(UM.bitOverTime,0) END AS bitOverTime,
 CASE WHEN ISNULL(PD.numPayrollDetailID,0)>0 THEN ISNULL(PD.bitOverTimeHrsDailyOrWeekly,0) ELSE ISNULL(UM.bitOverTimeHrsDailyOrWeekly,0) END AS bitOverTimeHrsDailyOrWeekly,
 CASE WHEN ISNULL(PD.numPayrollDetailID,0)>0 THEN ISNULL(PD.numLimDailHrs,0) ELSE ISNULL(UM.numLimDailHrs,0) END AS numLimDailHrs,
 CASE WHEN ISNULL(PD.numPayrollDetailID,0)>0 THEN ISNULL(PD.monOverTimeRate,0) ELSE ISNULL(UM.monOverTimeRate,0) END AS monOverTimeRate,ISNULL(PD.monDeductions,0) AS monDeductions
 FROM  dbo.UserMaster UM JOIN dbo.AdditionalContactsInformation adc ON adc.numcontactid = um.numuserdetailid
	   LEFT JOIN PayrollDetail PD ON PD.numUserCntID=ADC.numcontactid AND PD.numPayrollHeaderID=@numPayrollHeaderID
			WHERE UM.numDomainId=@numDomainId 

	
  Declare @decTotalHrsWorked as decimal(10,2),@decActualTotalHrsWorked as decimal(10,2);       
  Declare @decTotalOverTimeHrsWorked as decimal(10,2),@decActualTotalOverTimeHrsWorked as decimal(10,2);    
  Declare @decTotalHrs as decimal(10,2),@decActualTotalHrs as decimal(10,2);  
  DECLARE @decTotalPaidLeaveHrs AS DECIMAL(10,2),@decActualTotalPaidLeaveHrs AS DECIMAL(10,2)     
  DECLARE @bitOverTime bit,@bitOverTimeHrsDailyOrWeekly bit,@decOverTime decimal          
  DECLARE @monExpense DECIMAL(20,5),@monReimburse DECIMAL(20,5),@monCommPaidInvoice DECIMAL(20,5),@monCommUNPaidInvoice DECIMAL(20,5) 
  DECLARE @monActualExpense DECIMAL(20,5),@monActualReimburse DECIMAL(20,5),@monActualCommPaidInvoice DECIMAL(20,5),@monActualCommUNPaidInvoice DECIMAL(20,5) 
    
DECLARE  @minID NUMERIC(9),@maxID NUMERIC(9)

SELECT * INTO #tempPayrollTracking FROM PayrollTracking WHERE numDomainId=@numDomainId

SELECT @maxID = max(numUserCntID),@minID = min(numUserCntID) FROM #tempHrs

  WHILE @minID <= @maxID
  BEGIN
  
  SELECT @decTotalHrsWorked=0,@decTotalOverTimeHrsWorked=0,@decTotalHrs=0,@decTotalPaidLeaveHrs=0,
		@monExpense=0,@monReimburse=0,@monCommPaidInvoice=0,@monCommUNPaidInvoice=0,
		@decActualTotalHrsWorked=0,@decActualTotalOverTimeHrsWorked=0,@decActualTotalHrs=0,
		@decActualTotalPaidLeaveHrs=0,@monActualExpense=0,@monActualReimburse=0,@monActualCommPaidInvoice=0,@monActualCommUNPaidInvoice=0
   
  SELECT @bitOverTime=bitOverTime,@bitOverTimeHrsDailyOrWeekly=bitOverTimeHrsDailyOrWeekly,@decOverTime=numLimDailHrs
   FROM #tempHrs WHERE numUserCntID=@minID
  
  
  --Regular Hrs
  Select @decTotalHrsWorked=sum(x.Hrs) From (Select  isnull(Sum(Cast(datediff(minute,dtfromdate,dttodate) as float)/Cast(60 as float)),0) as Hrs            
  from TimeAndExpense Where (numType=1 Or numType=2)  And numCategory=1                 
--  And ((dateadd(minute,-@ClientTimeZoneOffset,dtFromDate) between @dtStartDate And 	@dtEndDate )
--	Or (dateadd(minute,-@ClientTimeZoneOffset,dtToDate) between @dtStartDate And @dtEndDate)) 
	And             
  numUserCntID=@minID  And numDomainID=@numDomainId 
  AND numCategoryHDRID NOT IN (SELECT numCategoryHDRID FROM #tempPayrollTracking WHERE numPayrollHeaderID!=@numPayrollHeaderID AND ISNULL(numCategoryHDRID,0)>0)) x           
  
  --Actual Regular Hrs
  Select @decActualTotalHrsWorked=sum(x.Hrs) From (Select  isnull(Sum(Cast(datediff(minute,dtfromdate,dttodate) as float)/Cast(60 as float)),0) as Hrs            
  from TimeAndExpense Where (numType=1 Or numType=2)  And numCategory=1                 
--  And ((dateadd(minute,-@ClientTimeZoneOffset,dtFromDate) between @dtStartDate And 	@dtEndDate )
--	Or (dateadd(minute,-@ClientTimeZoneOffset,dtToDate) between @dtStartDate And @dtEndDate)) 
	And             
  numUserCntID=@minID  And numDomainID=@numDomainId 
  AND numCategoryHDRID IN (SELECT numCategoryHDRID FROM #tempPayrollTracking WHERE numPayrollHeaderID=@numPayrollHeaderID AND ISNULL(numCategoryHDRID,0)>0)) x           

 --OrverTime Hrs         
 if @bitOverTime=1  AND @bitOverTimeHrsDailyOrWeekly=1 AND @decTotalHrsWorked > @decOverTime                     
 Begin                      
	SET @decTotalOverTimeHrsWorked=@decTotalHrsWorked - @decOverTime
 END
 
  if @bitOverTime=1  AND @bitOverTimeHrsDailyOrWeekly=1 AND @decActualTotalHrsWorked > @decOverTime                     
 Begin                      
	SET @decActualTotalOverTimeHrsWorked= @decActualTotalHrsWorked - @decOverTime
 END
    
 --Paid Leave Hrs
 SELECT @decTotalPaidLeaveHrs=ISNULL(SUM(CASE WHEN dtfromdate = dttodate THEN 24 - (CASE WHEN bitfromfullday = 0 THEN 12 ELSE 0 END)
           ELSE (Datediff(DAY,dtfromdate,dttodate) + 1) * 24 
                                - (CASE WHEN bitfromfullday = 0 THEN 12 ELSE 0 END)
                                - (CASE WHEN bittofullday = 0 THEN 12 ELSE 0 END) END),0)
            FROM   timeandexpense
            WHERE  numtype = 3 AND numcategory = 3 AND numusercntid = @minID AND numdomainid = @numDomainId 
--                   And ((dateadd(minute,-@ClientTimeZoneOffset,dtFromDate) between @dtStartDate And @dtEndDate )
--					Or (dateadd(minute,-@ClientTimeZoneOffset,dtToDate) between @dtStartDate And @dtEndDate))
					AND numCategoryHDRID NOT IN (SELECT numCategoryHDRID FROM #tempPayrollTracking WHERE numPayrollHeaderID!=@numPayrollHeaderID AND ISNULL(numCategoryHDRID,0)>0)
 
 --Actual Paid Leave Hrs
 SELECT @decActualTotalPaidLeaveHrs=ISNULL(SUM(CASE WHEN dtfromdate = dttodate THEN 24 - (CASE WHEN bitfromfullday = 0 THEN 12 ELSE 0 END)
           ELSE (Datediff(DAY,dtfromdate,dttodate) + 1) * 24 
                                - (CASE WHEN bitfromfullday = 0 THEN 12 ELSE 0 END)
                                - (CASE WHEN bittofullday = 0 THEN 12 ELSE 0 END) END),0)
            FROM   timeandexpense
            WHERE  numtype = 3 AND numcategory = 3 AND numusercntid = @minID AND numdomainid = @numDomainId 
--                   And ((dateadd(minute,-@ClientTimeZoneOffset,dtFromDate) between @dtStartDate And 	@dtEndDate )
--					Or (dateadd(minute,-@ClientTimeZoneOffset,dtToDate) between @dtStartDate And @dtEndDate))
					AND numCategoryHDRID IN (SELECT numCategoryHDRID FROM #tempPayrollTracking WHERE numPayrollHeaderID=@numPayrollHeaderID AND ISNULL(numCategoryHDRID,0)>0)

 --Expenses
Select @monExpense=isnull((Sum(Cast(monAmount as float)))   ,0)    
from TimeAndExpense Where numCategory=2 And numType in (1,2) 
And numUserCntID=@minID And numDomainID=@numDomainId 
--AND (dtFromDate between @dtStartDate And @dtEndDate Or dtToDate between @dtStartDate And @dtEndDate) 
AND numCategoryHDRID NOT IN (SELECT numCategoryHDRID FROM #tempPayrollTracking WHERE numPayrollHeaderID!=@numPayrollHeaderID AND ISNULL(numCategoryHDRID,0)>0)    

 --Actual Expenses
Select @monActualExpense=isnull((Sum(Cast(monAmount as float)))   ,0)    
from TimeAndExpense Where numCategory=2 And numType in (1,2) 
And numUserCntID=@minID And numDomainID=@numDomainId 
--AND (dtFromDate between @dtStartDate And @dtEndDate Or dtToDate between @dtStartDate And @dtEndDate) 
AND numCategoryHDRID IN (SELECT numCategoryHDRID FROM #tempPayrollTracking WHERE numPayrollHeaderID=@numPayrollHeaderID AND ISNULL(numCategoryHDRID,0)>0)    


 --Reimbursable Expenses
SELECT @monReimburse=Isnull((SUM(CAST(monamount AS FLOAT))),0)
            FROM   timeandexpense 
            WHERE  bitreimburse = 1 AND numcategory = 2 
                   AND numusercntid = @minID AND numdomainid = @numDomainId
                   --AND dtfromdate BETWEEN @dtStartDate AND @dtEndDate
                   AND numCategoryHDRID NOT IN (SELECT numCategoryHDRID FROM #tempPayrollTracking WHERE numPayrollHeaderID!=@numPayrollHeaderID AND ISNULL(numCategoryHDRID,0)>0)

 --Actual Reimbursable Expenses
SELECT @monActualReimburse=Isnull((SUM(CAST(monamount AS FLOAT))),0)
            FROM   timeandexpense 
            WHERE  bitreimburse = 1 AND numcategory = 2 
                   AND numusercntid = @minID AND numdomainid = @numDomainId
                   --AND dtfromdate BETWEEN @dtStartDate AND @dtEndDate
                   AND numCategoryHDRID IN (SELECT numCategoryHDRID FROM #tempPayrollTracking WHERE numPayrollHeaderID=@numPayrollHeaderID AND ISNULL(numCategoryHDRID,0)>0)

--LOGIC OF COMMISSION TYPE 3 (PROEJCT GROSS PROFIT) IS REMOVED BECAUSE THIS OPTION IS REMOVED FROM GLOBAL SETTINGS 

	--Commission Paid Invoice 
   SELECT @monCommPaidInvoice=isnull(sum(BC.numComissionAmount - ISNULL(PT.monCommissionAmt,0)),0) 
   FROM OpportunityMaster Opp 
		INNER JOIN OpportunityBizDocs oppBiz on oppBiz.numOppId=opp.numOppId 
        INNER JOIN BizDocComission BC on BC.numOppBizDocId=oppBiz.numOppBizDocsId 
        LEFT JOIN #tempPayrollTracking PT ON PT.numComissionID=BC.numComissionID AND PT.numPayrollHeaderID!=@numPayrollHeaderID
   where Opp.numDomainId=@numDomainId and
   BC.numUserCntId=@minID AND BC.bitCommisionPaid=0 
   AND oppBiz.bitAuthoritativeBizDocs = 1 AND isnull(oppBiz.monAmountPaid,0) >=oppBiz.monDealAmount
   --AND ((dateadd(minute,-@ClientTimeZoneOffset,oppBiz.dtCreatedDate) between @dtStartDate And @dtEndDate))


--Actual Commission Paid Invoice 
   SELECT @monActualCommPaidInvoice=isnull(sum(PT.monCommissionAmt),0) 
   FROM OpportunityMaster Opp 
		INNER JOIN OpportunityBizDocs oppBiz on oppBiz.numOppId=opp.numOppId 
        INNER JOIN BizDocComission BC on BC.numOppBizDocId=oppBiz.numOppBizDocsId 
        JOIN #tempPayrollTracking PT ON PT.numComissionID=BC.numComissionID AND PT.numPayrollHeaderID=@numPayrollHeaderID
   where Opp.numDomainId=@numDomainId and
   BC.numUserCntId=@minID AND BC.bitCommisionPaid=0 
   AND oppBiz.bitAuthoritativeBizDocs = 1 AND isnull(oppBiz.monAmountPaid,0) >=oppBiz.monDealAmount
   --AND ((dateadd(minute,-@ClientTimeZoneOffset,oppBiz.dtCreatedDate) between @dtStartDate And @dtEndDate))
   

--Commission UnPaid Invoice 
 SELECT @monCommUNPaidInvoice=isnull(sum(BC.numComissionAmount - ISNULL(PT.monCommissionAmt,0)),0) 
   FROM OpportunityMaster Opp 
		INNER JOIN OpportunityBizDocs oppBiz on oppBiz.numOppId=opp.numOppId 
        INNER JOIN BizDocComission BC on BC.numOppBizDocId=oppBiz.numOppBizDocsId
        LEFT JOIN #tempPayrollTracking PT ON PT.numComissionID=BC.numComissionID AND PT.numPayrollHeaderID!=@numPayrollHeaderID
   where Opp.numDomainId=@numDomainId and
   BC.numUserCntId=@minID AND BC.bitCommisionPaid=0 
   AND oppBiz.bitAuthoritativeBizDocs = 1 AND isnull(oppBiz.monAmountPaid,0) <oppBiz.monDealAmount
   --AND ((dateadd(minute,-@ClientTimeZoneOffset,oppBiz.dtCreatedDate) between @dtStartDate And @dtEndDate)) 
    
	   
--Actual Commission UnPaid Invoice 
 SELECT @monActualCommUNPaidInvoice=isnull(sum(PT.monCommissionAmt),0) 
   FROM OpportunityMaster Opp 
		INNER JOIN OpportunityBizDocs oppBiz on oppBiz.numOppId=opp.numOppId 
        INNER JOIN BizDocComission BC on BC.numOppBizDocId=oppBiz.numOppBizDocsId
        JOIN #tempPayrollTracking PT ON PT.numComissionID=BC.numComissionID AND PT.numPayrollHeaderID=@numPayrollHeaderID
   where Opp.numDomainId=@numDomainId and
   BC.numUserCntId=@minID AND BC.bitCommisionPaid=0 
   AND oppBiz.bitAuthoritativeBizDocs = 1 AND isnull(oppBiz.monAmountPaid,0) <oppBiz.monDealAmount
   --AND ((dateadd(minute,-@ClientTimeZoneOffset,oppBiz.dtCreatedDate) between @dtStartDate And @dtEndDate))                                   

                                           
 UPDATE #tempHrs SET    decRegularHrs=@decTotalHrsWorked-@decTotalOverTimeHrsWorked,
						decOvertimeHrs=@decTotalOverTimeHrsWorked,
						decTotalHrs=@decTotalHrsWorked ,
						decPaidLeaveHrs=@decTotalPaidLeaveHrs,
						monExpense=@monExpense,
						monReimburse=@monReimburse,
						monCommPaidInvoice=@monCommPaidInvoice,
						monCommUNPaidInvoice=@monCommUNPaidInvoice,
						
						decActualRegularHrs=ISNULL(@decActualTotalHrsWorked,0)-ISNULL(@decActualTotalOverTimeHrsWorked,0),
						decActualOvertimeHrs=ISNULL(@decActualTotalOverTimeHrsWorked,0),
						decActualTotalHrs=ISNULL(@decActualTotalHrsWorked,0),
						decActualPaidLeaveHrs=ISNULL(@decActualTotalPaidLeaveHrs,0),
						monActualExpense=ISNULL(@monActualExpense,0),
						monActualReimburse=ISNULL(@monActualReimburse,0),
						monActualCommPaidInvoice=ISNULL(@monActualCommPaidInvoice,0),
						monActualCommUNPaidInvoice=ISNULL(@monActualCommUNPaidInvoice,0)
          WHERE numUserCntID=@minID                  

    SELECT @minID = min(numUserCntID) FROM #tempHrs WHERE numUserCntID > @minID 
  END

SELECT * FROM #tempHrs
DROP TABLE #tempPayrollTracking
DROP TABLE #tempHrs

SELECT MAX(numPayrolllReferenceNo) + 1 AS [numMaxPayrolllReferenceNo] FROM dbo.PayrollHeader WHERE numDomainId = @numDomainId

END
GO