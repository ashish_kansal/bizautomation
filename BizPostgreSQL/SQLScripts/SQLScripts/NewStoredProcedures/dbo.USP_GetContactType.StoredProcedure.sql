GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetContactType')
DROP PROCEDURE USP_GetContactType
GO
CREATE PROCEDURE USP_GetContactType
@numUserCntId NUMERIC(9)
AS 
BEGIN
	SELECT ISNULL((SELECT [numContactType] FROM [AdditionalContactsInformation] WHERE [numContactId] = @numUserCntId),0)
END