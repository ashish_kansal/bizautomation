GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_OpportunityMaster_MergePurchaseOrders')
DROP PROCEDURE USP_OpportunityMaster_MergePurchaseOrders
GO
CREATE PROCEDURE [dbo].[USP_OpportunityMaster_MergePurchaseOrders]  
	@numDomainID NUMERIC(18,0),
	@numUserCntID NUMERIC(18,0),
	@numMasterPOID NUMERIC(18,0),
	@vcItemsToMerge VARCHAR(MAX)
AS  
BEGIN
	IF EXISTS (SELECT numOppId FROM OpportunityMaster WHERE numDomainId=@numDomainID AND numOppId=@numMasterPOID)
	BEGIN
		DECLARE @hDocItem INT

		DECLARE @TEMP TABLE
		(
			ID INT IDENTITY(1,1)
			,numOppID NUMERIC(18,0)
			,numOppItemID NUMERIC(18,0)
			,numUnitHour FLOAT
			,monPrice DECIMAL(20,5)
		)

		EXEC sp_xml_preparedocument @hDocItem OUTPUT, @vcItemsToMerge

		INSERT INTO @TEMP
		(
			numOppID
			,numOppItemID
			,numUnitHour
			,monPrice
		)
		SELECT
			ISNULL(OppID,0)
			,ISNULL(OppItemID,0)
			,ISNULL(Units,0)
			,ISNULL(UnitCost,0)
		FROM
			OPENXML (@hDocItem,'/NewDataSet/Table1',2)
		WITH
		(
			OppID  NUMERIC(18,0)
			,OppItemID NUMERIC(18,0)
			,Units FLOAT
			,UnitCost DECIMAL(20,5)
		)

		EXEC sp_xml_removedocument @hDocItem

		IF EXISTS (SELECT OB.numOppBizDocsId FROM OpportunityBizDocs OB INNER JOIN @TEMP T ON OB.numOppId = T.numOppID)
		BEGIN
			RAISERROR('BIZ_DOC_EXISTS',16,1)
		END
		ELSE
		BEGIN
			BEGIN TRY
			BEGIN TRANSACTION                                                                  
				DECLARE @numContactId NUMERIC(18,0)=null                                                                        
				DECLARE @numDivisionId numeric(9)=null                                                                          
				DECLARE @tintSource numeric(9)=null                                                                          
				DECLARE @vcPOppName Varchar(100)=''                                                                 
				DECLARE @Comments varchar(1000)=''                                                                          
				DECLARE @bitPublicFlag bit=0                                                                                                                                                         
				DECLARE @monPAmount DECIMAL(20,5) =0                                                 
				DECLARE @numAssignedTo as numeric(9)=0                                                                                                                                                                                                                                                                                         
				DECLARE @strItems as VARCHAR(MAX)=null                                                                          
				DECLARE @strMilestone as VARCHAR(100)=null                                                                          
				DECLARE @dtEstimatedCloseDate datetime                                                                          
				DECLARE @CampaignID as numeric(9)=null                                                                          
				DECLARE @lngPConclAnalysis as numeric(9)=null                                                                         
				DECLARE @tintOppType as tinyint                                                                                                                                             
				DECLARE @tintActive as tinyint                                                              
				DECLARE @numSalesOrPurType as numeric(9)              
				DECLARE @numRecurringId as numeric(9)
				DECLARE @numCurrencyID as numeric(9)=0
				DECLARE @DealStatus as tinyint =0
				DECLARE @numStatus AS NUMERIC(9)
				DECLARE @vcOppRefOrderNo VARCHAR(100)
				DECLARE @numBillAddressId numeric(9)=0
				DECLARE @numShipAddressId numeric(9)=0
				DECLARE @bitStockTransfer BIT=0
				DECLARE @WebApiId INT = 0
				DECLARE @tintSourceType TINYINT=0
				DECLARE @bitDiscountType as bit
				DECLARE @fltDiscount  as float
				DECLARE @bitBillingTerms as bit
				DECLARE @intBillingDays as integer
				DECLARE @bitInterestType as bit
				DECLARE @fltInterest as float
				DECLARE @tintTaxOperator AS TINYINT
				DECLARE @numDiscountAcntType AS NUMERIC(9)
				DECLARE @vcWebApiOrderNo VARCHAR(100)=NULL
				DECLARE @vcCouponCode		VARCHAR(100) = ''
				DECLARE @vcMarketplaceOrderID VARCHAR(100)=NULL
				DECLARE @vcMarketplaceOrderDate  datetime=NULL
				DECLARE @vcMarketplaceOrderReportId VARCHAR(100)=NULL
				DECLARE @numPercentageComplete NUMERIC(9)
				DECLARE @bitUseShippersAccountNo BIT = 0
				DECLARE @bitUseMarkupShippingRate BIT = 0
				DECLARE @numMarkupShippingRate NUMERIC(19,2) = 0
				DECLARE @intUsedShippingCompany INT = 0
				DECLARE @numShipmentMethod NUMERIC(18,0)=0
				DECLARE @dtReleaseDate DATE = NULL
				DECLARE @numPartner NUMERIC(18,0)=0
				DECLARE @tintClickBtn INT=0
				DECLARE @numPartenerContactId NUMERIC(18,0)=0
				DECLARE @numAccountClass NUMERIC(18,0) = 0
				DECLARE @numWillCallWarehouseID NUMERIC(18,0) = 0
				DECLARE @numVendorAddressID NUMERIC(18,0) = 0

				--GET MASTER PO FIELDS 
				SELECT
					@numContactId =numContactId,@numDivisionId=numDivisionId,@tintSource=tintSource,@vcPOppName=vcPOppName,@Comments=txtComments,
					@bitPublicFlag=bitPublicFlag,@monPAmount=monPAmount,@numAssignedTo=numAssignedTo,@strMilestone=null,@dtEstimatedCloseDate=intPEstimatedCloseDate,
					@CampaignID=numCampainID,@lngPConclAnalysis=lngPConclAnalysis,@tintOppType=tintOppType,@tintActive=tintActive,@numSalesOrPurType=numSalesOrPurType,
					@numRecurringId=null,@numCurrencyID=numCurrencyID,@DealStatus=tintOppStatus,@numStatus=numStatus,@vcOppRefOrderNo=vcOppRefOrderNo,@numBillAddressId=0,
					@numShipAddressId=0,@bitStockTransfer=0,@WebApiId=0,@tintSourceType=tintSourceType,@bitDiscountType=bitDiscountType,@fltDiscount=fltDiscount,
					@bitBillingTerms=bitBillingTerms,@intBillingDays=intBillingDays,@bitInterestType=bitInterestType,@fltInterest=fltInterest,@tintTaxOperator=tintTaxOperator,
					@numDiscountAcntType=numDiscountAcntType,@vcWebApiOrderNo=vcWebApiOrderNo,@vcCouponCode=vcCouponCode,@vcMarketplaceOrderID=vcMarketplaceOrderID,
					@vcMarketplaceOrderDate=bintCreatedDate,@vcMarketplaceOrderReportId=vcMarketplaceOrderReportId,@numPercentageComplete= numPercentageComplete,
					@bitUseShippersAccountNo=bitUseShippersAccountNo,@bitUseMarkupShippingRate=bitUseMarkupShippingRate,@numMarkupShippingRate=numMarkupShippingRate,
					@intUsedShippingCompany= intUsedShippingCompany,@numShipmentMethod=numShipmentMethod,@dtReleaseDate=dtReleaseDate,@numPartner=numPartner,@numPartenerContactId=numPartenerContact
					,@numAccountClass=numAccountClass,@numVendorAddressID=numVendorAddressID
				FROM
					OpportunityMaster
				WHERE
					numDomainId = @numDomainID
					AND numOppId = @numMasterPOID 

				DECLARE @TEMPMasterItems TABLE
				(
					numoppitemtCode NUMERIC(18,0),numItemCode NUMERIC(18,0),numUnitHour FLOAT,monPrice DECIMAL(20,5),
					monTotAmount DECIMAL(20,5),vcItemDesc VARCHAR(1000),numWarehouseItmsID NUMERIC(18,0),numToWarehouseItemID NUMERIC(18,0),
					Op_Flag INT, ItemType VARCHAR(50),DropShip BIT,bitDiscountType BIT, fltDiscount FLOAT, monTotAmtBefDiscount DECIMAL(20,5),
					vcItemName VARCHAR(300),numUOM NUMERIC(18,0), bitWorkOrder BIT,numVendorWareHouse NUMERIC(18,0),
					numShipmentMethod NUMERIC(18,0),numSOVendorId NUMERIC(18,0),numProjectID NUMERIC(18,0),
					numProjectStageID NUMERIC(18,0),Attributes VARCHAR(200),bitItemPriceApprovalRequired BIT
				)

				--GET ITEMS OF MASTER PO
				INSERT INTO 
					@TEMPMasterItems
				SELECT 
					numoppitemtCode,numItemCode,numUnitHour,monPrice,monTotAmount,vcItemDesc,numWarehouseItmsID,numToWarehouseItemID,
					2,vcType,bitDropShip,bitDiscountType,fltDiscount,monTotAmtBefDiscount,vcItemName,numUOMId,bitWorkOrder,numVendorWareHouse,
					numShipmentMethod,numSOVendorId,numProjectID,numProjectStageID,vcAttributes,bitItemPriceApprovalRequired
				FROM 
					OpportunityItems
				WHERE 
					numOppId = @numMasterPOID

				--GET ITEMS OF PO NEEDS TO BE MERGED TO MASTER PO
				INSERT INTO 
					@TEMPMasterItems
				SELECT 
					0,numItemCode,T.numUnitHour,T.monPrice,(T.numUnitHour * T.monPrice),vcItemDesc,numWarehouseItmsID,numToWarehouseItemID,
					1,vcType,bitDropShip,bitDiscountType,0,(T.numUnitHour * T.monPrice),vcItemName,numUOMId,bitWorkOrder,numVendorWareHouse,
					numShipmentMethod,numSOVendorId,numProjectID,numProjectStageID,vcAttributes,bitItemPriceApprovalRequired
				FROM 
					@TEMP T
				INNER JOIN
					OpportunityItems OI
				ON
					T.numOppItemID = OI.numoppitemtCode
		
				-- Op_Flag is 2(UPDATE) FOR MASTER PO AND 1(INSERT) FOR OTHER
				SET @strItems =(
									SELECT 
										numoppitemtCode,numItemCode,CAST(numUnitHour AS DECIMAL(18,8)) AS numUnitHour,monPrice,monTotAmount,vcItemDesc,numWarehouseItmsID,numToWarehouseItemID,
										Op_Flag, ItemType,DropShip,	bitDiscountType, CAST(fltDiscount AS DECIMAL(18,8)) AS fltDiscount, monTotAmtBefDiscount,	vcItemName,numUOM, bitWorkOrder,
										numVendorWareHouse,numShipmentMethod,numSOVendorId,numProjectID,numProjectStageID,Attributes,bitItemPriceApprovalRequired
									FROM 
										@TEMPMasterItems
									FOR XML RAW ('Item'), ROOT ('NewDataSet'), ELEMENTS
								)
		
				--UPDATE MASTER PO AND ADD MERGE PO ITEMS
				EXEC USP_OppManage @numOppID=@numMasterPOID OUTPUT,@numContactId=@numContactId,@numDivisionId=@numDivisionId,@tintSource=@tintSource,@vcPOppName=@vcPOppName output,
									@Comments=@Comments,@bitPublicFlag=@bitPublicFlag,@numUserCntID=@numUserCntID,@monPAmount=@monPAmount,@numAssignedTo=@numAssignedTo,
									@numDomainId=@numDomainId,@strItems=@strItems,@strMilestone=@strMilestone,@dtEstimatedCloseDate=@dtEstimatedCloseDate,@CampaignID=@CampaignID,
									@lngPConclAnalysis=@lngPConclAnalysis,@tintOppType=@tintOppType,@tintActive=@tintActive,@numSalesOrPurType=@numSalesOrPurType,
									@numRecurringId=@numRecurringId,@numCurrencyID=@numCurrencyID,@DealStatus=@DealStatus,@numStatus=@numStatus,@vcOppRefOrderNo=@vcOppRefOrderNo,
									@numBillAddressId=@numBillAddressId,@numShipAddressId=@numShipAddressId,@bitStockTransfer=@bitStockTransfer,@WebApiId=@WebApiId,
									@tintSourceType=@tintSourceType,@bitDiscountType=@bitDiscountType,@fltDiscount=@fltDiscount,@bitBillingTerms=@bitBillingTerms,
									@intBillingDays=@intBillingDays,@bitInterestType=@bitInterestType,@fltInterest=@fltInterest,@tintTaxOperator=@tintTaxOperator,
									@numDiscountAcntType=@numDiscountAcntType,@vcWebApiOrderNo=@vcWebApiOrderNo,@vcCouponCode=@vcCouponCode,@vcMarketplaceOrderID=@vcMarketplaceOrderID,
									@vcMarketplaceOrderDate=@vcMarketplaceOrderDate,@vcMarketplaceOrderReportId=@vcMarketplaceOrderReportId,@numPercentageComplete=@numPercentageComplete,
									@bitUseShippersAccountNo=@bitUseShippersAccountNo,@bitUseMarkupShippingRate=@bitUseMarkupShippingRate,
									@numMarkupShippingRate=@numMarkupShippingRate,@intUsedShippingCompany=@intUsedShippingCompany,@numShipmentMethod=@numShipmentMethod,@dtReleaseDate=@dtReleaseDate,@numPartner=@numPartner,@numPartenerContactId=@numPartenerContactId
									,@numAccountClass=@numAccountClass,@numVendorAddressID=@numVendorAddressID					

				DECLARE @TEMPOpp TABLE
				(
					ID INT IDENTITY(1,1)
					,numOppID NUMERIC(18,0)
				)

				INSERT INTO @TEMPOpp
				(
					numOppID
				)
				SELECT DISTINCT
					numOppID
				FROM
					@TEMP


				DECLARE @i INT = 1
				DECLARE @iCount INT
				DECLARE @numOppID NUMERIC(18,0)

				SELECT @iCount=COUNT(*) FROM @TEMPOpp

				-- DELETE MERGED POS
				WHILE @i <= @iCount
				BEGIN
					SELECT @numOppID=numOppID FROM @TEMPOpp WHERE ID = @i

					IF EXISTS (SELECT numOppId FROM OpportunityMaster WHERE numDomainId=@numDomainID AND numOppId=@numOppID) AND NOT EXISTS (SELECT numoppitemtCode FROM OpportunityItems OI WHERE OI.numOppId=@numOppID AND OI.numoppitemtCode NOT IN (SELECT numOppItemID FROM @TEMP))
					BEGIN
						EXEC USP_DeleteOppurtunity @numOppId=@numOppID,@numDomainID=@numDomainID,@numUserCntID=@numUserCntID
					END
					ELSE
					BEGIN
						EXEC USP_RevertDetailsOpp @numOppID,0,@numUserCntID

						DELETE
							OI
						FROM
							@TEMP T
						INNER JOIN
							OpportunityItems OI
						ON
							T.numOppItemID = OI.numoppitemtCode
						WHERE
							T.numOppID = @numOppID

						EXEC USP_UpdatingInventoryonCloseDeal @numOppID,0,@numUserCntID


						UPDATE OpportunityMaster SET monDealAmount=dbo.GetDealAmount(@numOppID,getdate(),0),bintModifiedDate=GETUTCDATE(),numModifiedBy=@numUserCntID WHERE numOppId=@numOppID

						--INSERT TAX FOR DIVISION   
						IF (select isnull(bitPurchaseTaxCredit,0) from Domain where numDomainId=@numDomainId)=1
						BEGIN
							--Delete Tax for Opportunity Items if item deleted 
							DELETE FROM OpportunityItemsTaxItems WHERE numOppId=@numOppId AND numOppItemID NOT IN (SELECT numoppitemtCode FROM OpportunityItems WHERE numOppId=@numOppID)

							--Insert Tax for Opportunity Items
							INSERT INTO dbo.OpportunityItemsTaxItems 
							(
								numOppId,
								numOppItemID,
								numTaxItemID,
								numTaxID
							) 
							SELECT 
								@numOppId,
								OI.numoppitemtCode,
								TI.numTaxItemID,
								0
							FROM 
								dbo.OpportunityItems OI 
							JOIN 
								dbo.ItemTax IT 
							ON 
								OI.numItemCode=IT.numItemCode 
							JOIN
								TaxItems TI 
							ON 
								TI.numTaxItemID = IT.numTaxItemID 
							WHERE 
								OI.numOppId=@numOppID 
								AND IT.bitApplicable=1 
								AND OI.numoppitemtCode NOT IN (SELECT numOppItemID FROM OpportunityItemsTaxItems WHERE numOppId=@numOppID)
							UNION
							SELECT 
								@numOppId,
								OI.numoppitemtCode,
								0,
								0
							FROM 
								dbo.OpportunityItems OI 
							JOIN 
								dbo.Item I 
							ON 
								OI.numItemCode=I.numItemCode
							WHERE 
								OI.numOppId=@numOppID 
								AND I.bitTaxable=1 
								AND OI.numoppitemtCode NOT IN (SELECT numOppItemID FROM OpportunityItemsTaxItems WHERE numOppId=@numOppID)
							UNION
							SELECT
								@numOppId,
								OI.numoppitemtCode,
								1,
								TD.numTaxID
							FROM
								dbo.OpportunityItems OI 
							INNER JOIN
								ItemTax IT
							ON
								IT.numItemCode = OI.numItemCode
							INNER JOIN
								TaxDetails TD
							ON
								TD.numTaxID = IT.numTaxID
								AND TD.numDomainId = @numDomainId
							WHERE
								OI.numOppId = @numOppID
								AND OI.numoppitemtCode NOT IN (SELECT numOppItemID FROM OpportunityItemsTaxItems WHERE numOppId=@numOppID)
						END
					END

					SET @i = @i + 1
				END
			COMMIT
			END TRY
			BEGIN CATCH
				DECLARE @ErrorMessage NVARCHAR(4000)
				DECLARE @ErrorNumber INT
				DECLARE @ErrorSeverity INT
				DECLARE @ErrorState INT
				DECLARE @ErrorLine INT
				DECLARE @ErrorProcedure NVARCHAR(200)

				IF @@TRANCOUNT > 0
					ROLLBACK TRANSACTION;

				SELECT 
					@ErrorMessage = ERROR_MESSAGE(),
					@ErrorNumber = ERROR_NUMBER(),
					@ErrorSeverity = ERROR_SEVERITY(),
					@ErrorState = ERROR_STATE(),
					@ErrorLine = ERROR_LINE(),
					@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

				RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);
			END CATCH	
		END
	END
	ELSE
	BEGIN
		RAISERROR('MASTER_PO_DOES_NOT_EXISTS',16,1)
	END
END
GO



