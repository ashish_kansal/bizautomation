SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_BulkOppTemp')
DROP PROCEDURE USP_BulkOppTemp
GO
CREATE PROCEDURE [dbo].[USP_BulkOppTemp]  
	@numContactId AS BIGINT,
	@numDivisionId AS BIGINT,
	@numOppId AS BIGINT,
	@IsCreated AS INT
AS
BEGIN
	INSERT INTO MassSalesOrderQueue
	(
		numContactId
		,numDivisionId
		,numOppId
		,bitExecuted
	)
	VALUES 
	(
		@numContactId
		,@numDivisionId
		,@numOppId
		,@IsCreated
	)
END