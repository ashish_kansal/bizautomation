SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_ValidateOppSerializLot_Name')
DROP PROCEDURE usp_ValidateOppSerializLot_Name
GO
CREATE PROCEDURE [dbo].[usp_ValidateOppSerializLot_Name]
    (
    @numOppItemTcode as numeric(9)=0,  
    @numOppID as numeric(9)=0,  
    @strItems as VARCHAR(1000)='',
    @numBizDocID as numeric(9)=0,
    @numWarehouseItmsID as numeric(9)=0,
    @bitSerialized as bit,
    @bitLotNo as bit
    )
AS 
    BEGIN
 
 -- THIS PROCEDURE IS COMMENTED BECAUSE IT IS NOT USED NOW AND ITS LOGIC IS NOT RELEVENT TO BIZ NOW. SO DO NOT USE IT.

-- SELECT vcSerialNo,  isnull(numQty,0) 
--  - isnull((select sum(w.numQty) from OppWarehouseSerializedItem w INNER JOIN OpportunityMaster 
--  opp ON (w.numOppId=opp.numOppId AND opp.tintOppType=1) where isnull(opp.tintshipped,0)=0 and 
--  w.numWarehouseItmsDTLID=WareHouseItmsDTL.numWarehouseItmsDTLID),0)
--  
--  + isnull((select sum(w.numQty) from OppWarehouseSerializedItem w INNER JOIN OpportunityMaster 
--  opp ON (w.numOppId=opp.numOppId AND opp.tintOppType=1) where isnull(opp.tintshipped,0)=0 and 
--  w.numWarehouseItmsDTLID=WareHouseItmsDTL.numWarehouseItmsDTLID AND numOppBizDocsId=@numBizDocID),0)
--   as TotalQty
--INTO #tempTable
-- from   WareHouseItmsDTL   
--    where (tintStatus is null or tintStatus=0) and  numWareHouseItemID=@numWarehouseItmsID

--Create table #tempTable (                                                                    
-- vcSerialNo VARCHAR(100),TotalQty [numeric](18, 0),numWarehouseItmsDTLID [numeric](18, 0)                                             
-- )                       

--INSERT INTO #tempTable (vcSerialNo,numWarehouseItmsDTLID,TotalQty)  
--            SELECT vcSerialNo,WareHouseItmsDTL.numWareHouseItmsDTLID,
--  isnull(WareHouseItmsDTL.numQty,0) - isnull((select sum(w.numQty) from OppWarehouseSerializedItem w INNER JOIN OpportunityMaster opp ON (w.numOppId=opp.numOppId AND opp.tintOppType=1) where isnull(opp.tintshipped,0)=0 and w.numWarehouseItmsDTLID=WareHouseItmsDTL.numWarehouseItmsDTLID),0) + isnull(OppWarehouseSerializedItem.numQty,0) as TotalQty
-- from   OppWarehouseSerializedItem      
-- join WareHouseItmsDTL      
-- on WareHouseItmsDTL.numWareHouseItmsDTLID= OppWarehouseSerializedItem.numWarehouseItmsDTLID                           
-- where numOppID=@numOppID and  numWareHouseItemID=@numWarehouseItmsID and numOppBizDocsId=@numBizDocID ORDER BY vcSerialNo,TotalQty desc


--INSERT INTO #tempTable (vcSerialNo,numWarehouseItmsDTLID,TotalQty)  
--		 SELECT vcSerialNo,numWareHouseItmsDTLID,
--  isnull(numQty,0) - isnull((select sum(w.numQty) from OppWarehouseSerializedItem w INNER JOIN OpportunityMaster opp ON (w.numOppId=opp.numOppId AND opp.tintOppType=1) where isnull(opp.tintshipped,0)=0 and w.numWarehouseItmsDTLID=WareHouseItmsDTL.numWarehouseItmsDTLID),0) as TotalQty
-- from   WareHouseItmsDTL   
--    where (tintStatus is null or tintStatus=0) and  numWareHouseItemID=@numWarehouseItmsID  
--    and numWareHouseItmsDTLID not in(select numWarehouseItmsDTLID from OppWarehouseSerializedItem where numOppID=@numOppID and  numWarehouseItmsID=@numWarehouseItmsID and numOppBizDocsId=@numBizDocID) ORDER BY vcSerialNo,TotalQty desc

--Create table #tempError (                                                                    
-- vcSerialNo VARCHAR(100),UsedQty [numeric](18, 0),AvailableQty [numeric](18, 0),vcError VARCHAR(100)                                             
-- )                       

  
--DECLARE @posComma int, @strKeyVal varchar(20)

--SET @strItems=RTRIM(@strItems)
--IF RIGHT(@strItems, 1)!=',' SET @strItems=@strItems+','

--SET @posComma=PatIndex('%,%', @strItems)
--WHILE @posComma>1
--BEGIN
--	SET @strKeyVal=ltrim(rtrim(substring(@strItems, 1, @posComma-1)))
	
--	DECLARE @posBStart INT,@posBEnd int, @strQty varchar(20),@strName varchar(20)

--    IF @bitLotNo=1 
--    BEGIN
--		SET @posBStart=PatIndex('%(%', @strKeyVal)
--		SET @posBEnd=PatIndex('%)%', @strKeyVal)
--		IF( @posBStart>1 AND @posBEnd>1)
--			BEGIN
--				SET @strName=ltrim(rtrim(substring(@strKeyVal, 1, @posBStart-1)))
--				SET	@strQty=ltrim(rtrim(substring(@strKeyVal, @posBStart+1,len(@strKeyVal)-@posBStart-1)))
--			END		
			
--		ELSE
--			BEGIN
--				SET @strName=@strKeyVal
--				SET	@strQty=1
--			END
--	END
--	ELSE
--	BEGIN
--			SET @strName=@strKeyVal
--			SET	@strQty=1
--	END  
	
--	PRINT  'Name->' + @strName + ':Qty->' + @strQty
	  
--	  DECLARE @AvailableQty NUMERIC(9)
	  
--	 IF NOT EXISTS(SELECT 1 FROM #tempTable WHERE vcSerialNo=@strName)
--		 INSERT INTO #tempError (vcSerialNo,UsedQty,AvailableQty,vcError) VALUES (@strName,0,0,'Serial / Lot # not available')
--	 ELSE
--	 BEGIN 
--	    SELECT TOP 1 @AvailableQty=TotalQty FROM #tempTable WHERE vcSerialNo=@strName 
	    
--	    IF  (@strQty>@AvailableQty)	 
--		 INSERT INTO #tempError (vcSerialNo,UsedQty,AvailableQty,vcError) VALUES (@strName,@strQty,@AvailableQty,'Used Serial / Lot # more than Available Serial / Lot #')
--	  END
	  
--	SET @strItems=substring(@strItems, @posComma +1, len(@strItems)-@posComma)
--	SET @posComma=PatIndex('%,%',@strItems)
	
--END
----SELECT * FROM #tempTable

--SELECT * FROM #tempError
--DROP TABLE #tempError
--DROP TABLE #tempTable

END
