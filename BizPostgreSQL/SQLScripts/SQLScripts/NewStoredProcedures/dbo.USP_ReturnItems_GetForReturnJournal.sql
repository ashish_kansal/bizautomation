SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_ReturnItems_GetForReturnJournal' ) 
    DROP PROCEDURE USP_ReturnItems_GetForReturnJournal
GO

-- EXEC USP_GetHeaderReturnDetails 53, 1
CREATE PROCEDURE [dbo].[USP_ReturnItems_GetForReturnJournal]
(
    @numReturnHeaderID NUMERIC(18,0),
    @numDomainId NUMERIC(18,0)
)
AS 
BEGIN
    SELECT  
		ISNULL(RH.numDivisionId, 0) AS [numDivisionId]
		,numReturnItemID
		,RI.numItemCode
		,ISNULL(I.numCOGsChartAcntId,0) AS ItemCoGs
        ,ISNULL(I.numAssetChartAcntId,0) AS ItemInventoryAsset
        ,ISNULL(I.numIncomeChartAcntId,0) AS ItemIncomeAccount
		,numUnitHourReceived
		,monTotalDiscount
		,charItemType
        ,(CASE WHEN ISNULL(i.bitVirtualInventory,0) = 1 THEN 0 ELSE isnull((SELECT monAverageCost FROM OpportunityBizDocItems OBDI WHERE OBDI.numOppBizDocItemID=RI.numOppBizDocItemID),isnull(i.monAverageCost,'0')) END) as AverageCost
		,CONVERT(DECIMAL(20,5),(ISNULL(monTotAmount,0) / RI.numUnitHour)) AS monUnitSalePrice
		,ISNULL(I.bitKitParent,0) AS bitKitParent
		,0 AS tintChildLevel
    FROM 
		dbo.ReturnHeader RH
		LEFT JOIN dbo.ReturnItems RI ON RH.numReturnHeaderID = RI.numReturnHeaderID
		LEFT JOIN dbo.OpportunityMaster OM ON OM.numOppID = RH.numOppID AND RH.numDomainID = OM.numDomainID 
		LEFT JOIN Item I ON I.numItemCode = RI.numItemCode
        LEFT JOIN divisionMaster D ON RH.numDivisionID = D.numDivisionID
        LEFT JOIN CompanyInfo C2 ON C2.numCompanyID = D.numCompanyID
        LEFT JOIN dbo.OpportunityAddress OA ON RH.numReturnHeaderID = OA.numReturnHeaderID
        LEFT JOIN AdditionalContactsInformation con ON con.numdivisionId = RH.numdivisionId AND con.numContactid = RH.numContactId
        LEFT JOIN WareHouseItems WItems ON WItems.numWareHouseItemID = RI.numWareHouseItemID
		LEFT JOIN Warehouses W ON W.numWarehouseID = WItems.numWarehouseID
	WHERE  
		RH.numDomainID = @numDomainId
        AND RH.numReturnHeaderID = @numReturnHeaderID
	UNION ALL
	SELECT 
		ISNULL(RH.numDivisionId, 0) AS [numDivisionId]
		,numReturnItemID
		,RI.numItemCode
		,ISNULL(I.numCOGsChartAcntId,0) AS ItemCoGs
        ,ISNULL(I.numAssetChartAcntId,0) AS ItemInventoryAsset
        ,ISNULL(I.numIncomeChartAcntId,0) AS ItemIncomeAccount
		,numUnitHourReceived * ISNULL(OKI.numQtyItemsReq_Orig,0) numUnitHourReceived
		,monTotalDiscount
		,charItemType
        ,(CASE WHEN ISNULL(i.bitVirtualInventory,0) = 1 THEN 0 ELSE isnull(OBDKI.monAverageCost,isnull(i.monAverageCost,0)) END) as AverageCost
		,0 AS monUnitSalePrice -- Sale Price is set to 0 intentioanlly for kit child items. Don't change it
		,ISNULL(I.bitKitParent,0) AS bitKitParent
		,1 AS tintChildLevel
    FROM 
		dbo.ReturnHeader RH
		INNER JOIN dbo.ReturnItems RI ON RH.numReturnHeaderID = RI.numReturnHeaderID
		INNER JOIN dbo.OpportunityBizDocKitItems OBDKI ON RI.numOppBizDocItemID = OBDKI.numOppBizDocItemID
		INNER JOIN dbo.OpportunityKitItems OKI ON OBDKI.numOppChildItemID = OKI.numOppChildItemID
		INNER JOIN dbo.OpportunityMaster OM ON OM.numOppID = RH.numOppID AND RH.numDomainID = OM.numDomainID 
		INNER JOIN Item I ON I.numItemCode = OBDKI.numChildItemID
        LEFT JOIN divisionMaster D ON RH.numDivisionID = D.numDivisionID
        LEFT JOIN CompanyInfo C2 ON C2.numCompanyID = D.numCompanyID
        LEFT JOIN dbo.OpportunityAddress OA ON RH.numReturnHeaderID = OA.numReturnHeaderID
        LEFT JOIN AdditionalContactsInformation con ON con.numdivisionId = RH.numdivisionId AND con.numContactid = RH.numContactId
        LEFT JOIN WareHouseItems WItems ON WItems.numWareHouseItemID = RI.numWareHouseItemID
		LEFT JOIN Warehouses W ON W.numWarehouseID = WItems.numWarehouseID
	WHERE  
		RH.numDomainID = @numDomainId
        AND RH.numReturnHeaderID = @numReturnHeaderID
		AND ISNULL(I.bitKitParent,0) = 0
	UNION ALL
	SELECT 
		ISNULL(RH.numDivisionId, 0) AS [numDivisionId]
		,numReturnItemID
		,RI.numItemCode
		,ISNULL(I.numCOGsChartAcntId,0) AS ItemCoGs
        ,ISNULL(I.numAssetChartAcntId,0) AS ItemInventoryAsset
        ,ISNULL(I.numIncomeChartAcntId,0) AS ItemIncomeAccount
		,numUnitHourReceived * ISNULL(OKCI.numQtyItemsReq_Orig,0) numUnitHourReceived
		,monTotalDiscount
		,charItemType
        ,(CASE WHEN ISNULL(i.bitVirtualInventory,0) = 1 THEN 0 ELSE isnull(OBDKCI.monAverageCost,isnull(i.monAverageCost,0)) END) as AverageCost
		,0 AS monUnitSalePrice -- Sale Price is set to 0 intentioanlly for kit child items. Don't change it
		,ISNULL(I.bitKitParent,0) AS bitKitParent
		,2 AS tintChildLevel
    FROM 
		dbo.ReturnHeader RH
		INNER JOIN dbo.ReturnItems RI ON RH.numReturnHeaderID = RI.numReturnHeaderID
		INNER JOIN dbo.OpportunityBizDocKitChildItems OBDKCI ON RI.numOppBizDocItemID = OBDKCI.numOppBizDocItemID
		INNER JOIN dbo.OpportunityKitChildItems OKCI ON OBDKCI.numOppKitChildItemID = OKCI.numOppKitChildItemID
		INNER JOIN dbo.OpportunityMaster OM ON OM.numOppID = RH.numOppID AND RH.numDomainID = OM.numDomainID 
		INNER JOIN Item I ON I.numItemCode = OBDKCI.numChildItemID
        LEFT JOIN divisionMaster D ON RH.numDivisionID = D.numDivisionID
        LEFT JOIN CompanyInfo C2 ON C2.numCompanyID = D.numCompanyID
        LEFT JOIN dbo.OpportunityAddress OA ON RH.numReturnHeaderID = OA.numReturnHeaderID
        LEFT JOIN AdditionalContactsInformation con ON con.numdivisionId = RH.numdivisionId AND con.numContactid = RH.numContactId
        LEFT JOIN WareHouseItems WItems ON WItems.numWareHouseItemID = RI.numWareHouseItemID
		LEFT JOIN Warehouses W ON W.numWarehouseID = WItems.numWarehouseID
	WHERE  
		RH.numDomainID = @numDomainId
        AND RH.numReturnHeaderID = @numReturnHeaderID
		AND ISNULL(I.bitKitParent,0) = 0
END
GO