GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ManageContactTypeMapping')
DROP PROCEDURE USP_ManageContactTypeMapping
GO
CREATE PROCEDURE [dbo].[USP_ManageContactTypeMapping]
	@numMappingID NUMERIC(9) =0 output,
	@numContactTypeID NUMERIC(9),
	@numAccountID NUMERIC(9),
	@numDomainID NUMERIC(9)
AS
SET NOCOUNT ON
IF @numMappingID = 0 BEGIN
	INSERT INTO ContactTypeMapping (
		[numContactTypeID],
		[numAccountID],
		[numDomainID]
	)
	VALUES (
		@numContactTypeID,
		@numAccountID,
		@numDomainID
	)
	SET @numMappingID = SCOPE_IDENTITY() 
END
ELSE BEGIN
	UPDATE ContactTypeMapping SET 
		[numAccountID] = @numAccountID
	WHERE [numMappingID] = @numMappingID
END