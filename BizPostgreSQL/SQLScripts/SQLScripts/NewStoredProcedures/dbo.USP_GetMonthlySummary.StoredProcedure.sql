set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go

--SELECT * FROM [Chart_Of_Accounts] WHERE [numDomainId]=72

--SELECT SUBSTRING(CONVERT(VARCHAR(20),GETUTCDATE()),0,4)
-- EXEC USP_GetMonthlySummary 72, '2008-09-26 00:00:00.000', '2009-09-26 00:00:00.000','71'
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetMonthlySummary')
DROP PROCEDURE USP_GetMonthlySummary
GO
CREATE PROCEDURE [dbo].[USP_GetMonthlySummary](
               @numDomainId   AS INT,
               @dtFromDate    AS DATETIME,
               @dtToDate      AS DATETIME,
               @vcChartAcntId AS VARCHAR(500),
               @ClientTimeZoneOffset INT, --Added by Chintan to enable calculation of date according to client machine,
			   @numAccountClass NUMERIC(18,0) = 0
               )
AS
  BEGIN
  
	--SET @dtFromDate = DateAdd(minute, @ClientTimeZoneOffset, @dtFromDate);
	--SET @dtToDate = DateAdd(minute, @ClientTimeZoneOffset, @dtToDate);

	PRINT @dtFromDate 
	PRINT @dtToDate

	DECLARE @mnOpeningBalance AS DECIMAL(20,5)
	SELECT @mnOpeningBalance = ISNULL(mnOpeningBalance,0)
	FROM dbo.fn_GetOpeningBalance(@vcChartAcntId,@numDomainID,@dtFromDate,@ClientTimeZoneOffset)
	

		/*RollUp of Sub Accounts */
		SELECT  COA2.numAccountId INTO #Temp /*,COA1.vcAccountCode ,COA2.vcAccountCode*/
		FROM    dbo.Chart_Of_Accounts COA1
				INNER JOIN dbo.Chart_Of_Accounts COA2 ON COA1.numDomainId = COA2.numDomainId
		WHERE   COA1.numAccountId IN(SELECT  cast(ID AS NUMERIC(9)) FROM dbo.SplitIDs(@vcChartAcntId, ','))
				AND COA1.numDomainId = @numDomainID
				AND (COA2.vcAccountCode LIKE COA1.vcAccountCode OR (COA2.vcAccountCode LIKE COA1.vcAccountCode + '%' AND COA2.numParentAccID>0))
				
		SELECT @vcChartAcntId = ISNULL( STUFF((SELECT ',' + CAST(numAccountID AS VARCHAR(10)) FROM #Temp FOR XML PATH('')),1, 1, '') , '') 
  
  
    SELECT   numAccountId,
             COAvcAccountCode,
             vcAccountName,
             numAccountTypeID,
             vcAccountType,
             SUBSTRING(CONVERT(VARCHAR(20),datEntry_Date),0,4) +' ' +  CONVERT(VARCHAR(4), YEAR(datEntry_Date)) AS MONTH,
             MONTH(datEntry_Date) AS MonthNumber,
             YEAR(datEntry_Date) AS YEAR,
             SUM(ISNULL(Debit,0)) AS Debit,
             SUM(ISNULL(Credit,0)) AS Credit,
             ISNULL(@mnOpeningBalance,0) AS Opening, 
             0.00 AS Closing
             
             INTO #Temp1
    FROM     VIEW_JOURNAL
    WHERE    datEntry_Date BETWEEN @dtFromDate AND @dtToDate
             AND numDomainId = @numDomainID
             AND numAccountId IN (SELECT * FROM   dbo.SplitIds(@vcChartAcntId,','))
			 AND (numAccountClass = @numAccountClass OR ISNULL(@numAccountClass,0) = 0)
    GROUP BY numAccountId,
             COAvcAccountCode,
             vcAccountName,
             numAccountTypeID,
             vcAccountType,
             MONTH(datEntry_Date),
             YEAR(datEntry_Date),
             SUBSTRING(CONVERT(VARCHAR(20),datEntry_Date),0,4) +' ' +  CONVERT(VARCHAR(4), YEAR(datEntry_Date))
    ORDER BY YEAR(datEntry_Date),MONTH(datEntry_Date)
    
    
--    SELECT DATEPART(MONTH,dtPeriodFrom) FY_Opening_Month,DATEPART(year,dtPeriodFrom) FY_Opening_year, * FROM dbo.FinancialYear WHERE numDomainId=@numDomainId
--Following logic applieds to only income, expense and cogs account
IF EXISTS( SELECT * FROM #Temp1 where SUBSTRING(COAvcAccountCode,0,5) IN ('0103','0104','0106') )
BEGIN
	 alter table #Temp1 alter column MONTH VARCHAR(50);
	 ALTER TABLE [#Temp1] ADD IsFinancialMonth BIT;
	 UPDATE [#Temp1] SET IsFinancialMonth = 0

    DECLARE @numMaxFinYearID NUMERIC
    DECLARE @Month INT
    DECLARE @year  INT
    
    SELECT @numMaxFinYearID = MAX(numFinYearId) FROM dbo.FinancialYear WHERE numDomainId=@numDomainId 
--    PRINT @numMaxFinYearID
    WHILE @numMaxFinYearID > 0
    BEGIN
			SELECT @Month = DATEPART(MONTH,dtPeriodFrom),@year =DATEPART(year,dtPeriodFrom) FROM dbo.FinancialYear WHERE numDomainId=@numDomainId AND numFinYearId=@numMaxFinYearID
    
--    PRINT @Month
--    PRINT @year
	/*Rule:make first month of each FY zero as opening balance and show report respectively */
			UPDATE #Temp1  SET Opening = -0.0001 ,[MONTH] = [MONTH] + ' FY Start', IsFinancialMonth = 1
			WHERE MonthNumber = @Month AND YEAR= @year
			
			IF @@ROWCOUNT=0 -- when No data avail for First month of FY
			BEGIN
				SELECT TOP 1  @Month = MonthNumber FROM #Temp1 WHERE MonthNumber>@Month AND YEAR=@year
				UPDATE #Temp1  SET Opening = -0.0001 ,[MONTH] = [MONTH] + ' FY Start', IsFinancialMonth = 1
				WHERE MonthNumber = @Month AND YEAR= @year
			END
			
    	
    	
    	 SELECT @numMaxFinYearID = MAX(numFinYearId) FROM dbo.FinancialYear WHERE numDomainId=@numDomainId AND numFinYearId < @numMaxFinYearID
    	IF @@ROWCOUNT = 0 
    	SET @numMaxFinYearID = 0
    END
END
   

    
    SELECT * FROM #Temp1
    
     DROP TABLE #Temp
     DROP TABLE #Temp1
  END
  
--  exec USP_GetMonthlySummary @numDomainId=89,@dtFromDate='2011-12-01 00:00:00:000',@dtToDate='2013-03-01 23:59:59:000',@vcChartAcntId='1444',@ClientTimeZoneOffset=-330

