/****** Object:  StoredProcedure [dbo].[USP_GetPageElementAttributes]    Script Date: 08/08/2009 16:15:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--EXEC [USP_GetPageElementAttributes] 3,35,10
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetPageElementAttributes')
DROP PROCEDURE USP_GetPageElementAttributes
GO
CREATE PROCEDURE [dbo].[USP_GetPageElementAttributes]
               @numSiteID    NUMERIC(9),
               @numElementID NUMERIC(9),
			   @numPageID NUMERIC(9)
AS
  BEGIN
  
  --SELECT PA.[numAttributeID],
  --             PA.numElementID,
  --             PA.[vcAttributeName],
		--	   CASE	WHEN  @numElementID = (SELECT numElementID 
		--				  FROM [dbo].[PageElementMaster] 
		--				  WHERE [vcTagName] = '{#BreadCrumb#}')
		--			THEN (SELECT [vcPageName] 
		--				  FROM [dbo].[SitePages] 
		--				  WHERE [numPageID] = @numPageID)
		--	   WHEN ISNULL(PA.bitEditor,0)=1 THEN [vcHtml] 
		--	   ELSE ISNULL(vcAttributeValue,'')  
		--	   END vcAttributeValue,
		--	   PA.[vcControlType],
		--	   PA.[vcControlValues],ISNULL(PA.bitEditor,0) bitEditor
  --      FROM   [PageElementAttributes] PA
		--LEFT OUTER JOIN PageElementDetail PED ON PED.numAttributeID = PA.numAttributeID
  --      WHERE PA.[numElementID] = @numElementID
  --      AND numSiteID = @numSiteID 
        
        SELECT PA.[numAttributeID],
               PA.numElementID,
               PA.[vcAttributeName],
			   CASE	WHEN  @numElementID = (SELECT numElementID 
						  FROM [dbo].[PageElementMaster] 
						  WHERE [vcTagName] = '{#BreadCrumb#}')
					THEN (SELECT [vcPageName] 
						  FROM [dbo].[SitePages] 
						  WHERE [numPageID] = @numPageID)
			   WHEN ISNULL(PA.bitEditor,0)=1 THEN (SELECT [vcHtml]
                       FROM   [PageElementDetail]
                       WHERE  numSiteID = @numSiteID
                              AND numElementID = @numElementID
                              AND numAttributeID = PA.numAttributeID)
			   ELSE ISNULL((SELECT [vcAttributeValue]
                       FROM   [PageElementDetail]
                       WHERE  numSiteID = @numSiteID
                              AND numElementID = @numElementID
                              AND numAttributeID = PA.numAttributeID),'') 
			   END vcAttributeValue,
			   PA.[vcControlType],
			   PA.[vcControlValues],ISNULL(PA.bitEditor,0) bitEditor
        FROM   [PageElementAttributes] PA
        --           LEFT OUTER JOIN [PageElementDetail] PD
        --             ON PA.[numAttributeID] = PD.[numAttributeID]
        WHERE PA.[numElementID] = @numElementID
      END


