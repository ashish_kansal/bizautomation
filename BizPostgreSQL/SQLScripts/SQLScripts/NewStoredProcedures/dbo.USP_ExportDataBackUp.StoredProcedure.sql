-- USP_ExportDataBackUp 1,9
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ExportDataBackUp')
DROP PROCEDURE USP_ExportDataBackUp
GO
CREATE PROCEDURE [dbo].[USP_ExportDataBackUp]
    @numDomainID NUMERIC(9),
    @tintTable TINYINT,
	@dtFromDate DATETIME,
	@dtToDate DATETIME
AS 
    BEGIN
		
        IF @tintTable = 1 
            BEGIN
                SELECT  CMP.numCompanyID CompanyID,
                        CMP.vcCompanyName CompanyName,
                        ( SELECT    vcdata
                          FROM      listdetails
                          WHERE     numListItemID = DM.numStatusID
                        ) AS CompanyStatus,
                        ( SELECT    vcdata
                          FROM      listdetails
                          WHERE     numListItemID = CMP.numCompanyRating
                        ) AS CompanyRating,
                        ( SELECT    vcdata
                          FROM      listdetails
                          WHERE     numListItemID = CMP.numCompanyType
                        ) AS CompanyType,
                        CASE WHEN ISNULL(DM.bitPublicFlag, 0) = 0 THEN 'False'
                             ELSE 'True'
                        END IsPrivate,
                        DM.numDivisionID DivisionID,
                        DM.vcDivisionName DivisionName,
                        ISNULL(AD1.vcStreet, '') AS BillStreet,
                        ISNULL(AD1.vcCity, '') AS BillCity,
                        ISNULL(AD1.vcPostalCode, '') AS BillPostCode,
                        dbo.fn_GetListName(AD1.numCountry, 0) BillCountry,
                        ISNULL(dbo.fn_GetState(AD1.numState), '') AS BillState,
                        
                        ISNULL(AD2.vcStreet, '') AS ShipStreet,
                        ISNULL(AD2.vcCity, '') AS ShipCity,
                        ISNULL(AD2.vcPostalCode, '') AS ShipPostCode,
                        dbo.fn_GetListName(AD2.numCountry, 0) ShipCountry,
                        ISNULL(dbo.fn_GetState(AD2.numState), '') AS ShipState,
                        ( SELECT    vcdata
                          FROM      listdetails
                          WHERE     numListItemID = numFollowUpStatus
                        ) AS FollowUpStatus,
                        CMP.vcWebLabel1 WebLabel1,
                        CMP.vcWebLink1 WebLink1,
                        CMP.vcWebLabel2 WebLabel2,
                        CMP.vcWebLink2 WebLink2,
                        CMP.vcWebLabel3 WebLabel3,
                        CMP.vcWebLink3 WebLink3,
                        CMP.vcWeblabel4 Weblabel4,
                        CMP.vcWebLink4 WebLink4,
                        CASE WHEN ISNULL(tintBillingTerms, 0) = 0 THEN 'False'
                             ELSE 'True'
                        END IsBillingTerms,
                        --(SELECT vcData FROM dbo.ListDetails WHERE numListID = 296 AND numDomainID = CMP.numDomainID AND numListItemID = numBillingDays ) AS [BillingDays],
                        (SELECT numNetDueInDays FROM dbo.BillingTerms WHERE numDomainID = CMP.numDomainID AND numTermsID = numBillingDays) AS [BillingDays],
                        (SELECT vcTerms FROM dbo.BillingTerms WHERE numDomainID = CMP.numDomainID AND numTermsID = numBillingDays) AS [BillingDaysName],
--                        tintInterestType InterestType,
--                        fltInterest Interest,
                      
                        ( SELECT    vcdata
                          FROM      listdetails
                          WHERE     numListItemID = DM.numTerID
                        ) AS Territory,
--                        DM.numTerID TerritoryID,
                        ( SELECT    vcdata
                          FROM      listdetails
                          WHERE     numListItemID = CMP.numCompanyIndustry
                        ) AS CompanyIndustry,
--                        CMP.numCompanyIndustry,
                        ( SELECT    vcdata
                          FROM      listdetails
                          WHERE     numListItemID = CMP.numCompanyCredit
                        ) AS CompanyCredit,
--                        CMP.numCompanyCredit,
                        ISNULL(CMP.txtComments, '') AS Comments,
                        ISNULL(CMP.vcWebSite, '') WebSite,
                        ( SELECT    vcdata
                          FROM      listdetails
                          WHERE     numListItemID = CMP.numAnnualRevID
                        ) AS AnnualRevenue,
--                        CMP.numAnnualRevID,
                        ( SELECT    vcdata
                          FROM      listdetails
                          WHERE     numListItemID = CMP.numNoOfEmployeesID
                        ) AS NoOfEmployees,
--                        numNoOfEmployeesID,
                        ( SELECT    vcdata
                          FROM      listdetails
                          WHERE     numListItemID = CMP.vcProfile
                        ) AS [Profile],
--                        CMP.vcProfile,
                        DM.numCreatedBy CreatedBy,
                        dbo.fn_GetContactName(DM.numRecOwner) AS RecordOwner,
                        DM.numRecOwner AS RecordOwnerID,
                        DM.numCreatedBy AS CreatedBy,
                        DM.bintCreatedDate CreateDate,
--                        DM.numRecOwner RecordOwner,
                        DM.numModifiedBy AS ModifiedBy,
                        ISNULL(vcComPhone, '') AS CompanyPhone,
                        vcComFax CompanyFax,
--                        DM.numGrpID,
                        ( SELECT    vcGrpName
                          FROM      Groups
                          WHERE     numGrpId = DM.numGrpID
                        ) AS GroupName,
                        ( SELECT    vcdata
                          FROM      listdetails
                          WHERE     numListItemID = CMP.vcHow
                        ) AS InformationSource,
--                        CMP.vcHow AS vcInfoSource,
                        CASE DM.tintCRMType
                          WHEN 0 THEN 'Leads'
                          WHEN 1 THEN 'Prospects'
                          WHEN 2 THEN 'Accounts'
                          ELSE 'NA'
                        END [RelationshipType],
                        ( SELECT    vcCampaignName
                          FROM      CampaignMaster
                          WHERE     numCampaignID = DM.numCampaignID
                        ) AS CampaignName,
                        numCampaignID CampaignID,
                        DM.numAssignedBy AssignedByID,
                        ISNULL(ACI2.vcFirstName,'') + ' ' + ISNULL(ACI2.vcLastName,'') AS [AssignedBy],
                        CASE WHEN ISNULL(bitNoTax, 0) = 0 THEN 'False'
							 ELSE 'True'
						END [IsSalesTax],
--                        ( SELECT    A.vcFirstName + ' ' + A.vcLastName
--                          FROM      AdditionalContactsInformation A
--                                    LEFT JOIN DivisionMaster D ON D.numDivisionID = A.numDivisionID
--                          WHERE     A.numContactID = D.numAssignedTo AND D.numDomainId=cmp.numDomainID and cmp.numCompanyId=D.numCompanyId
--                        ) AS AssignedName,
						ISNULL(ACI1.vcFirstName,'') + ' ' + ISNULL(ACI1.vcLastName,'') AS [AssignedTo],
                        DM.numAssignedTo AS AssignedToID,
                        ACI.numContactID ContactID,
                        ACI.vcFirstName FirstName,
                        ACI.vcLastName LastName,
                        ACI.vcEmail ContactEmailAddress,
                        ACI.vcAltEmail AlternetEmailAddress,
                        ACI.numPhone ContactPhone,
                        ACI.numPhoneExtension ContactPhoneExt,
                        L8.vcData ContactStatus,
                        L9.vcData ContactType,
                        ACI.vcGivenName GivenName,
--                        ACI.numTeam,
                        ( SELECT    vcdata
                          FROM      listdetails
                          WHERE     numListItemID = ACI.numTeam
                        ) AS Team,
                        ACI.vcFax FaxNo,
                        CASE ISNULL(ACI.charSex, '')
                          WHEN 'M' THEN 'Male'
                          WHEN 'F' THEN 'Female'
                          ELSE ''
                        END AS Gender,
                        ACI.bintDOB DateOfBirth,
                        ( SELECT    vcdata
                          FROM      listdetails
                          WHERE     numListItemID = ACI.vcPosition
                        ) AS Position,
                        ACI.txtNotes Notes,
                        ACI.numCell CellNo,
                        ACI.NumHomePhone HomePhone,
                        ACI.vcAsstFirstName AsstFirstName,
                        ACI.vcAsstLastName AsstLastName,
                        ACI.numAsstPhone AsstPhone,
                        ACI.numAsstExtn AsstExtn,
                        ACI.vcAsstEmail AsstEmail,
                        ( SELECT    vcdata
                          FROM      listdetails
                          WHERE     numListItemID = ACI.vcDepartment
                        ) AS Department,
                        AD.vcStreet PrimaryStreet,
                        AD.vcCity PrimaryCity,
                        dbo.fn_GetState(AD.numState) AS PrimaryState,
                        dbo.fn_GetListName(AD.numCountry, 0) AS PrimaryCountry,
                        AD.vcPostalCode PrimaryPotalCode,
--                        AddC.vcContactLocation,
--                        AddC.vcStreet,
--                        AddC.vcCity,
--                        AddC.vcState,
--                        AddC.intPostalCode,
--                        AddC.vcCountry,
                        ACI.numManagerID ManagersContactID,
                        ACI.vcCategory,
                        ACI.vcTitle,
                        CASE WHEN ISNULL(ACI.bitOptOut, 0) = 0 THEN 'False'
                             ELSE 'True'
                        END ContactEmailOptOut,
                         CASE WHEN ISNULL(ACI.bitPrimaryContact, 0) = 0 THEN 'False'
                             ELSE 'True'
                        END PrimaryContact,
                        C.vcCurrencyDesc AS [DefaultTradingCurrency],
                        IACR.numNonBizCompanyID AS NonBizCompanyID,
                        IAI.numNonBizContactID AS NonBizContactID
                FROM    CompanyInfo CMP
                        JOIN DivisionMaster DM ON DM.numCompanyID = CMP.numCompanyID
                        LEFT OUTER JOIN AdditionalContactsInformation ACI ON DM.numDivisionID = ACI.numDivisionID
                        LEFT JOIN AdditionalContactsInformation ACI1 ON ACI1.numContactID = DM.numAssignedTo 
                        LEFT JOIN AdditionalContactsInformation ACI2 ON ACI2.numContactID = DM.numAssignedBy 
                        LEFT JOIN ListDetails L8 ON L8.numListItemID = ACI.numEmpStatus
                        LEFT JOIN ListDetails L9 ON L9.numListItemID = ACI.numContactType
                        LEFT JOIN dbo.AddressDetails AD ON AD.numRecordID = ACI.numContactID AND AD.tintAddressOf=1 AND AD.tintAddressType=0 AND AD.bitIsPrimary=1
                        LEFT JOIN dbo.AddressDetails AD1 ON AD1.numRecordID = DM.numDivisionID AND AD1.tintAddressOf=2 AND AD1.tintAddressType=1 AND AD1.bitIsPrimary=1
                        LEFT JOIN dbo.AddressDetails AD2 ON AD2.numRecordID = DM.numDivisionID AND AD2.tintAddressOf=2 AND AD2.tintAddressType=2 AND AD2.bitIsPrimary=1
                        LEFT JOIN [Currency] C ON C.[numCurrencyID] = DM.[numCurrencyID] AND DM.numDomainID = C.numDomainID
                        LEFT JOIN dbo.ImportActionItemReference IAI ON IAI.numContactID  =ACI.numContactId
                        LEFT JOIN dbo.ImportOrganizationContactReference IACR ON IACR.numDivisionID =DM.numDivisionID
                WHERE   DM.numDomainID = @numDomainID
                ORDER BY DM.numCompanyID   
                
                --Get field name 
				-- 14 for leads,13 for Accounts,12 for Prospects

                SELECT  *
                FROM    [CFW_Fld_Master]
                        LEFT JOIN CFw_Grp_Master ON subgrp = CFw_Grp_Master.Grp_id
                WHERE   CFW_Fld_Master.[numDomainID] = @numDomainID
                        AND CFW_Fld_Master.Grp_id = 1
                UNION
                SELECT  *
                FROM    [CFW_Fld_Master]
                        LEFT JOIN CFw_Grp_Master ON subgrp = CFw_Grp_Master.Grp_id
                WHERE   CFW_Fld_Master.[numDomainID] = @numDomainID
                        AND CFW_Fld_Master.Grp_id = 12
                UNION
                SELECT  *
                FROM    [CFW_Fld_Master]
                        LEFT JOIN CFw_Grp_Master ON subgrp = CFw_Grp_Master.Grp_id
                WHERE   CFW_Fld_Master.[numDomainID] = @numDomainID
                        AND CFW_Fld_Master.Grp_id = 13
                UNION
                SELECT  *
                FROM    [CFW_Fld_Master]
                        LEFT JOIN CFw_Grp_Master ON subgrp = CFw_Grp_Master.Grp_id
                WHERE   CFW_Fld_Master.[numDomainID] = @numDomainID
                        AND CFW_Fld_Master.Grp_id = 14
				
                --Get values
                SELECT  [FldDTLID],
                        CF.[Fld_ID],
                        CM.[Fld_label],
                        CASE WHEN ISNULL(CM.[numlistid], 0) > 0
                             THEN dbo.fn_GetCustFldStringValue(CF.[Fld_ID],CF.[RecId],CF.[Fld_Value])
                             ELSE CF.[Fld_Value]
                        END Fld_Value,
                        [RecId]
                FROM    [CFW_FLD_Values] CF
                        INNER JOIN [DivisionMaster] DM ON DM.[numDivisionID] = CF.[RecId]
                        INNER JOIN [CFW_Fld_Master] CM ON CM.[Fld_id] = CF.[Fld_ID]
                WHERE   DM.[numDomainId] = @numDomainID
						AND CM.numDomainID = @numDomainID
                
            END
            
        IF @tintTable = 2 
            BEGIN
                SELECT  Opp.numoppid OpportunityID,
                        ( SELECT    vcFirstname + ' ' + vcLastName
                          FROM      additionalContactsinformation
                          WHERE     numContactId = Opp.numContactId
                        ) AS ContactName,
                        Opp.txtComments Comments,
                        Opp.numDivisionID DivisionID,
                        CASE tintOppType
                          WHEN 1 THEN 'Sales'
                          WHEN 2 THEN 'Purchase'
                          ELSE 'NA'
                        END OpportunityType,
--                        ISNULL(bintAccountClosingDate, '') AS bintAccountClosingDate,
                        Opp.numContactID AS ContactID,
                        ( SELECT    vcData
                          FROM      [ListDetails]
                          WHERE     numListItemID = Opp.tintSource
                        ) AS OpportunitySource,
                        C2.vcCompanyName CompanyName, 
--                        D2.tintCRMType,
                        Opp.vcPoppName OpportunityName,
--                        intpEstimatedCloseDate,
                        opp.monDealAmount CalculatedAmount,
--                        lngPConclAnalysis,
                        monPAmount AS OppAmount,
--                        numSalesOrPurType,
                        CASE WHEN ISNULL(Opp.tintActive, 0) = 0 THEN 'False'
                             ELSE 'True'
                        END IsActive,
                        Opp.numCreatedby Createdby,
                        Opp.numModifiedBy ModifiedBy,
                        dbo.fn_GetContactName(Opp.numRecOwner) AS RecordOwner,
                        Opp.numRecOwner RecordOwnerID,
                        CASE WHEN ISNULL(tintshipped, 0) = 0 THEN 'False'
                             ELSE 'True'
                        END AS IsShipped,
--                        ISNULL(tintOppStatus, 0) AS tintOppStatus,
                        CASE ISNULL(tintOppStatus, 0)
                          WHEN 0 THEN 'Open'
                          WHEN 1 THEN 'Closed-Won'
                          WHEN 2 THEN 'Closed-Lost'
                        END OpportunityStatus,
                        Opp.numAssignedTo AssignedTo,
                        Opp.numAssignedBy AssignedBy,
                        ( SELECT    vcdata
                          FROM      listdetails
                          WHERE     numListItemID = D2.numTerID
                        ) AS Territory,
                        ISNULL(C.varCurrSymbol, '') varCurrSymbol,
                        Opp.bintCreatedDate CreatedDate,
                        Opp.bintAccountClosingDate ClosingDate,
                        Opp.intPEstimatedCloseDate DueDate,
                        dbo.fn_GetContactAddedToOpportunity(numOppID) AS AssociatedContactIds
                FROM    OpportunityMaster Opp
                        JOIN divisionMaster D2 ON Opp.numDivisionID = D2.numDivisionID
                        LEFT JOIN CompanyInfo C2 ON C2.numCompanyID = D2.numCompanyID
                        LEFT JOIN [Currency] C ON C.[numCurrencyID] = Opp.[numCurrencyID]
                WHERE   Opp.numDomainID = @numDomainID
                ORDER BY Opp.numDivisionID 
                
                SELECT  OI.[numoppitemtCode] OppItemID,
                        OI.[numOppId] OpportunityID,
                        OI.[numItemCode] ItemID,
                        OI.[numUnitHour] Quantity,
                        OI.[monPrice] Price,
                        OI.[monTotAmount] TotalAmount,
--					   OI.[numSourceID],
                        OI.[vcItemDesc] ItemDescription,
--					   OI.[numWarehouseItmsID] WarehouseItemID,
                        CASE OI.[vcType]
                          WHEN 'P' THEN 'Inventory Item'
                          WHEN 'S' THEN 'Service'
                          WHEN 'N' THEN 'Non-Inventory Item'
                          WHEN 'A' THEN 'Asset Item'
                          ELSE OI.[vcType]
                        END ItemType,
                        OI.[vcAttributes] Attributes,
                        OI.[bitDropShip] IsDropship,
--					   OI.[numUnitHourReceived],
--					   OI.[bitDiscountType],
--					   OI.[fltDiscount],
                        OI.[monTotAmtBefDiscount] TotalAmtBeforeDiscount
--					   OI.[numQtyShipped]
                FROM    [OpportunityMaster] OM
                        INNER JOIN [OpportunityItems] OI ON OM.numOppID = OI.numOppID
                WHERE   OM.numDomainID = @numDomainID
                
				--Get field name 
                SELECT  *
                FROM    [CFW_Fld_Master]
                        LEFT JOIN CFw_Grp_Master ON subgrp = CFw_Grp_Master.Grp_id
                WHERE   CFW_Fld_Master.[numDomainID] = @numDomainID
                        AND CFW_Fld_Master.Grp_id IN ( 2, 6 ) -- 2 for Sales Records, 6 for Purchase records
				
                --Get values
                SELECT  [FldDTLID],
                        CF.[Fld_ID],
                        CM.[Fld_label],
                        CASE WHEN ISNULL(CM.[numlistid], 0) > 0
                             THEN dbo.fn_GetCustFldStringValue(CF.[Fld_ID],CF.[RecId],CF.[Fld_Value])
                             ELSE CF.[Fld_Value]
                        END Fld_Value,
                        [RecId]
                FROM    CFW_Fld_Values_Opp CF
                        INNER JOIN [OpportunityMaster] OM ON OM.[numOppId] = CF.[RecId]
                        INNER JOIN [CFW_Fld_Master] CM ON CM.[Fld_id] = CF.[Fld_ID]
                WHERE   OM.[numDomainId] = @numDomainID
						AND CM.numDomainID = @numDomainID
                
            END
        IF @tintTable = 3 
            BEGIN
                SELECT  pro.numProID ProjID,
                        pro.vcProjectName ProjectName,
                        pro.vcProjectID ProjectID,
                        pro.numDivisionID DivisionID,
                        pro.numintPrjMgr IntPrjMgrID,
                        dbo.fn_GetContactName(pro.numintPrjMgr) AS InternalProjectMgr,
                        dbo.fn_GetContactName(pro.numCustPrjMgr) AS CustomerPrjectMgr,
                        pro.numCustPrjMgr CustPrjMgrID,
                        pro.txtComments Comments,
                        [dbo].[fn_GetContactName](pro.numAssignedTo) AS AssignedTo,
                        pro.numAssignedTo AssignedToID,
                        pro.intDueDate DueDate,
                        pro.[bintProClosingDate] ClosingDate,
                        pro.[bintCreatedDate] CreatDate
                FROM    ProjectsMaster pro
                WHERE   pro.numdomainID = @numDomainID 
	   		--Get field name 
                SELECT  *
                FROM    [CFW_Fld_Master]
                        LEFT JOIN CFw_Grp_Master ON subgrp = CFw_Grp_Master.Grp_id
                WHERE   CFW_Fld_Master.[numDomainID] = @numDomainID
                        AND CFW_Fld_Master.Grp_id = 11 -- 11 for projects
				
                --Get values
                SELECT  [FldDTLID],
                        CF.[Fld_ID],
                        CM.[Fld_label],
                        CASE WHEN ISNULL(CM.[numlistid], 0) > 0
                             THEN dbo.fn_GetCustFldStringValue(CF.[Fld_ID],CF.[RecId],CF.[Fld_Value])
                             ELSE CF.[Fld_Value]
                        END Fld_Value,
                        [RecId]
                FROM    [CFW_FLD_Values_Pro] CF
                        INNER JOIN [ProjectsMaster] PM ON PM.[numProId] = CF.[RecId]
                        INNER JOIN [CFW_Fld_Master] CM ON CM.[Fld_id] = CF.[Fld_ID]
                WHERE   PM.[numDomainId] = @numDomainID
						AND CM.numDomainID = @numDomainID
                
            END
        IF @tintTable = 4 
            BEGIN
                SELECT  C.numCaseId CaseID,
                        C.numContactID ContactID,
                        vcCaseNumber CaseNumber,
                        intTargetResolveDate ResolveDate,
                        textSubject Subject,
                        Div.numDivisionID DivisionID,
                        dbo.[GetListIemName](numStatus) Status,
                        dbo.[GetListIemName](numPriority) Priority,
                        textDesc CaseDescription,
--                        tintCRMType ,
                        textInternalComments InternalComments,
                        dbo.[GetListIemName](numReason) Reason,
                        C.numContractID ContractID,
                        dbo.[GetListIemName](numOrigin) CaseOrigin,
                        dbo.[GetListIemName](numType) CaseType,
                        C.bintCreatedDate CreateDate,
                        C.numCreatedby CreatedBy,
                        C.numModifiedBy ModifiedBy,
                        C.bintModifiedDate ModifiedDate,
                        C.numRecOwner RecordOwnerID,
--                        tintSupportKeyType,
                        C.numAssignedTo AssignedTo,
                        C.numAssignedBy AssignedBy,
                        ISNULL(Addc.vcFirstName, '') + ' '
                        + ISNULL(Addc.vcLastName, '') AS ContactName,
                        ISNULL(vcEmail, '') AS ContactEmail,
                        ISNULL(numPhone, '')
                        + CASE WHEN numPhoneExtension IS NULL THEN ''
                               WHEN numPhoneExtension = '' THEN ''
                               ELSE ', ' + numPhoneExtension
                          END AS Phone,
                        Com.numCompanyId CompanyID,
                        Com.vcCompanyName CompanyName
                FROM    Cases C
                        LEFT JOIN AdditionalContactsInformation Addc ON Addc.numContactId = C.numContactID
                        JOIN DivisionMaster Div ON Div.numDivisionId = Addc.numDivisionId
                        JOIN CompanyInfo Com ON Com.numCompanyID = div.numCompanyID
                WHERE   C.numDomainID = @numDomainID
            
            --Get field name 
                SELECT  *
                FROM    [CFW_Fld_Master]
                        LEFT JOIN CFw_Grp_Master ON subgrp = CFw_Grp_Master.Grp_id
                WHERE   CFW_Fld_Master.[numDomainID] = @numDomainID
                        AND CFW_Fld_Master.Grp_id = 3 -- 3 for cases
				
                --Get values
                SELECT  [FldDTLID],
                        CF.[Fld_ID],
                        CM.[Fld_label],
                        CASE WHEN ISNULL(CM.[numlistid], 0) > 0
                             THEN dbo.fn_GetCustFldStringValue(CF.[Fld_ID],CF.[RecId],CF.[Fld_Value])
                             ELSE CF.[Fld_Value]
                        END Fld_Value,
                        [RecId]
                FROM    [CFW_FLD_Values_Case] CF
                        INNER JOIN [Cases] C ON C.[numCaseId] = CF.[RecId]
                        INNER JOIN [CFW_Fld_Master] CM ON CM.[Fld_id] = CF.[Fld_ID]
                WHERE   C.[numDomainId] = @numDomainID
						AND CM.numDomainID = @numDomainID
            END
	
    END
    IF @tintTable = 5 
        BEGIN
            SELECT  numSolnID SolutionID,
                    dbo.[GetListIemName](numCategory) Category,
                    vcSolnTitle SolutionName,
                    txtSolution SolutionDescription,
                    vcLink URL
            FROM    SolutionMaster
            WHERE   [numDomainID] = @numDomainID
            
        END
    IF @tintTable = 6 
        BEGIN
            SELECT  [numContractId] ContractID,
                    [numDivisionID] DivisionID,
                    [vcContractName] ContractName,
                    [bintStartDate] StatDate,
                    [bintExpDate] ExpirationDate,
                    [numIncidents] NoOfIncidents,
                    [numHours] NoOfHours,
                    [numEmailDays] EmailDays,
                    [numEmailIncidents] NoOfEmailIncidents,
                    [numEmailHours] EmailHours,
                    [numUserCntID] ContactID,
                    [vcNotes] Notes,
                    [bintCreatedOn] CreateDate,
                    [numAmount] Amount,
                    [bitAmount] IsAmount,
                    [bitIncidents] IsIncidents,
                    [bitDays] IsDays,
                    [bitHour] IsHour,
                    [bitEDays] IsEMailDays,
                    [bitEIncidents] IsEmailIncedents,
                    [bitEHour],
                    [numTemplateId] EmailTemplateID,
                    [decRate],
                    dbo.fn_GetContactAddedToContract(numDomainID,
                                                     numContractID) AS ContactsAddedToContract
            FROM    [ContractManagement]
            WHERE   [numDomainID] = @numDomainID
			
        END 
        
    IF @tintTable = 7 
        BEGIN
            SELECT  --[numJournal_Id] JournalID,
                    [datEntry_Date] JournalEntryDate,
					[TransactionType],
					[CompanyName],
                    [varDescription] Description,
					[BizDocID] BizDocName,
					[numOppBizDocsId] BizDocID,
                    --[numTransactionId] TransationID,
                    --[numOppId] OpportunityID,
					[numDebitAmt] DebitAmount,
                    [numCreditAmt] CreditAmount,
                    [TranRef] TransactinoReferece,
                    [TranDesc] TransactionDescription,
					[BizPayment],
					[CheqNo] CheckNo
                    --[numAccountId] ChartOfAccountID,
                    --[vcAccountName] AccountName,
                    --[numCheckId] CheckID,
--                    [numCashCreditCardId] ,
                    --[numDepositId] DespositeID,
                    --[numCategoryHDRID] TimeExpenseID,
                    --[tintTEType] TimeExpenseType,
                    --[numCategory] CategoryID,
                    --[dtFromDate] FromDate,
                    --[numUserCntID] ContactID,
                    --[numDivisionID] DivisionID
            FROM    [VIEW_GENERALLEDGER]
            WHERE   [numDomainId] = @numDomainID
            AND [VIEW_GENERALLEDGER].[datEntry_Date] BETWEEN @dtFromDate AND @dtToDate
        END 
        
    IF @tintTable = 8 
        BEGIN
			DECLARE @CustomFields AS VARCHAR(1000)

				SELECT @CustomFields = COALESCE (@CustomFields,'') + '[' + Fld_label + '],' FROM CFW_Fld_Master WHERE numDomainID=@numDomainID AND Grp_id=5
				-- Removes last , from string
				IF DATALENGTH(@CustomFields) > 0
					SET @CustomFields = LEFT(@CustomFields, LEN(@CustomFields) - 1)

				DECLARE @vcSQL VARCHAR(8000)

				SET @vcSQL = CONCAT('SELECT  
										I.numItemCode [Item ID],
										vcItemName [Item Name],
										ISNULL(txtItemDesc, '''') [Item Description],
										CASE charItemType
										  WHEN ''P'' THEN ''Inventory Item''
										  WHEN ''S'' THEN ''Service''
										  WHEN ''N'' THEN ''Non-Inventory Item''
										  WHEN ''A'' THEN ''Asset Item''
										  ELSE charItemType
										END [Item Type],
										vcModelID [Model ID],
										(select vcItemGroup from ItemGroups where numItemGroupID=numItemGroup and numDomainId=',@numDomainID,') as [Item Group],
										vcSKU [SKU(NI)],
										dbo.[GetListIemName](numItemClassification) [Item Classification],
										STUFF((SELECT CONCAT('', '',vcCategoryName) FROM ItemCategory IC INNER JOIN CATEGORY CT ON IC.numCategoryID=CT.numCategoryId AND CT.numDomainID=',@numDomainID,' and IC.numItemId=i.numItemCode GROUP BY CT.numCategoryID,CT.vcCategoryName FOR XML PATH(''''),TYPE).value(''text()[1]'',''nvarchar(max)''),1,2,N'''') as [Item Category],
										ISNULL(fltWeight, 0) AS Weight,
										ISNULL(fltHeight, 0) AS Height,
										ISNULL(fltWidth, 0) AS Width,
										ISNULL(fltLength, 0) AS Length,
										(select vcUnitName from UOM where numUOMId=ISNULL(I.numBaseUnit,0)) AS [Base Unit],
										(select vcUnitName from UOM where numUOMId=ISNULL(I.numPurchaseUnit,0)) AS [Purchase Unit],
										(select vcUnitName from UOM where numUOMId=ISNULL(I.numSaleUnit,0)) AS [Sale Unit],
										CASE charItemType WHEN ''P'' THEN ISNULL(monListPrice,0) END AS [List Price (I)],
										CASE charItemType WHEN ''N'' THEN ISNULL(monListPrice,0) END AS [List Price (NI)],
										ISNULL(CAST(numBarCodeId AS VARCHAR(50)), '''') AS BarCodeId,
										ISNULL(vcManufacturer,'''') as Manufacturer,
										(select vcCompanyName from DivisionMaster DM INNER JOIN CompanyInfo CI ON DM.numCompanyId=CI.numCompanyID where DM.numDivisionId=numVendorID) as Vendor,
										(select vcAccountName from chart_of_Accounts COA where
										COA.numAccountId=numCOGsChartAcntId and COA.numDomainID=',@numDomainID,') as [COGs/Expense Account],
										(select vcAccountName from chart_of_Accounts COA where
										COA.numAccountId=numAssetChartAcntId and COA.numDomainID=',@numDomainID,') as [Asset Account],
										(select vcAccountName from chart_of_Accounts COA where
										COA.numAccountId=numIncomeChartAcntId and COA.numDomainID=',@numDomainID,') as [Income Account],
										(select vcCompanyName from DivisionMaster DM INNER JOIN CompanyInfo CI ON DM.numCompanyId=CI.numCompanyID where  
										DM.numDivisionId=CA.numDivId) as AssetOwner,
										isnull(numAppvalue,0) as AppreciationValue,
										isnull(numDepValue,0) as DepreciationValue,
										isnull(dtPurchase,'''') as PurchaseDate,
										isnull(dtWarrentyTill,'''') as WarranteeTill,
										(SELECT TOP 1 vcPathForImage FROM dbo.ItemImages II WHERE II.numItemCode = I.numItemCode AND II.numDomainId= I.numDomainId AND II.bitDefault=1 AND II.bitIsImage = 1 ) ImagePath,
										(SELECT TOP 1 vcPathForTImage FROM dbo.ItemImages II WHERE II.numItemCode = I.numItemCode AND II.numDomainId= I.numDomainId AND II.bitDefault=1 AND II.bitIsImage = 1) ThumbImagePath,
										CASE WHEN ISNULL(bitTaxable, 0) = 0 THEN ''False''
											 ELSE ''True''
										END [Taxable],
										CASE WHEN ISNULL(bitKitParent, 0) = 0 THEN ''False''
											 ELSE ''True''
										END [Is Kit],
										CASE WHEN ISNULL(bitSerialized, 0) = 0 THEN ''False''
											 ELSE ''True''
										END [Is Serialize],                    					
										monAverageCost [Average Cost],
										monCampaignLabourCost CampaignLabourCost,
										numCreatedBy CreatedBy,
										bintCreatedDate CreatedDate,
										numModifiedBy ModifiedBy,
										bintModifiedDate ModifiedDate,                    
										CASE WHEN ISNULL(bitFreeShipping, 0) = 0 THEN ''False''
											 ELSE ''True''
										END IsFreeShipping,
										CASE WHEN ISNULL(bitAllowBackOrder, 0) = 0 THEN ''False''
											 ELSE ''True''
										END [Allow Back Orders],
										CASE WHEN ISNULL(bitCalAmtBasedonDepItems, 0) = 0
											 THEN ''False''
											 ELSE ''True''
										END CalAmtBasedonDepItems,
										CASE WHEN ISNULL(bitAssembly, 0) = 0 THEN ''False''
											 ELSE ''True''
										END [Is Assembly] , 
										CASE WHEN ISNULL(IsArchieve, 0) = 0 THEN ''False''
											 ELSE ''True''
										END [Archive],       
										(SELECT  ISNULL(vcData, '''') FROM  dbo.ListDetails WHERE numDomainID = 1 AND numListItemID = numItemClass AND numListID = 381) AS [Item Class],
										(SELECT  ISNULL(vcData, '''') FROM  dbo.ListDetails WHERE numDomainID = 1 AND numListItemID = numShipClass AND numListID = 461) AS [Shipping Class]
										' + (CASE WHEN DATALENGTH(@CustomFields) > 0 THEN ',CustomFields.*' ELSE '' END) + '
								FROM    Item I  left outer join  (select * from CompanyAssets CA where CA.numDomainID=',@numDomainID,') CA
										ON CA.numItemCode=I.numItemCode
									' + (CASE WHEN DATALENGTH(@CustomFields) > 0 THEN CONCAT('OUTER APPLY
										(
											SELECT 
											* 
											FROM 
											(
												SELECT  
													CFW_Fld_Master.Fld_label,
													ISNULL(dbo.fn_GetCustFldStringValue(CFW_Fld_Values_Item.Fld_ID, CFW_Fld_Values_Item.RecId, CFW_Fld_Values_Item.Fld_Value),'''') Fld_Value 
												FROM 
													CFW_Fld_Master
												LEFT JOIN 
													CFW_Fld_Values_Item
												ON
													CFW_Fld_Values_Item.Fld_ID = CFW_Fld_Master.Fld_id
												WHERE 
													RecId = I.numItemCode
											) p PIVOT (MAX([Fld_Value]) FOR Fld_label  IN (',@CustomFields,') ) AS pvt
										) CustomFields') ELSE '' END) + ' WHERE   I.[numDomainiD] = ',@numDomainID)

	
					EXEC (@vcSQL)
            
					SELECT  [numVendorID] VendorID,
							[vcPartNo] [Vendor Part#],
							[monCost] [Vendor Cost],
							[numItemCode] [Item ID],
							[intMinQty] [Vendor Minimum Qty]
					FROM    [Vendor]
					WHERE   [numDomainID] = @numDomainID
			--Get field name 
            SELECT  *
            FROM    [CFW_Fld_Master]
                    LEFT JOIN CFw_Grp_Master ON subgrp = CFw_Grp_Master.Grp_id
            WHERE   CFW_Fld_Master.[numDomainID] = -1 --PASSED VALUE -1 TO PREVENT CUSTOM FIELDS ROW BECAUE NOW CODE DOESN'T REQUIRES VALUE FROM THIS TABLE
                    AND CFW_Fld_Master.Grp_id = 5 -- 5 for Item details
				
                --Get values
            SELECT  [FldDTLID],
                    CF.[Fld_ID],
                    CM.[Fld_label],
                    CASE WHEN ISNULL(CM.[numlistid], 0) > 0
                         THEN dbo.fn_GetCustFldStringValue(CF.[Fld_ID],CF.[RecId],CF.[Fld_Value])
                         ELSE CF.[Fld_Value]
                    END Fld_Value,
                    [RecId]
            FROM    [CFW_FLD_Values_Item] CF
                    INNER JOIN [Item] I ON I.[numItemCode] = CF.[RecId]
                    INNER JOIN [CFW_Fld_Master] CM ON CM.[Fld_id] = CF.[Fld_ID]
            WHERE   I.[numDomainId] = -1 --PASSED VALUE -1 TO PREVENT CUSTOM FIELDS ROW BECAUE NOW CODE DOESN'T REQUIRES VALUE FROM THIS TABLE
                    AND CM.Grp_id = 5
                    AND CM.numDomainID = @numDomainID
                
        END
			
    IF @tintTable = 9 
        BEGIN
            SELECT  c.numAccountId ChartOfAccountID,
                    AT.[numAccountTypeID] AccountTypeID,
                    c.[numParntAcntTypeId] ParentAcntTypeID,
                    c.[vcAccountCode] AccountCode,
                    c.vcAccountName AS AccountName,
                    AT.[vcAccountType] AccountType,
                    CASE WHEN c.[numParntAcntTypeId] IS NULL THEN NULL
                         ELSE c.numOpeningBal
                    END AS OpeningBal,
                    c.[vcAccountDescription] Description,
                    C.[dtOpeningDate] OpeningDate,
					CASE WHEN ISNULL(c.[bitFixed], 0) = 0 THEN 'False'
                         ELSE 'True'
                    END IsFixed,
                    CASE WHEN ISNULL(c.[bitDepreciation], 0) = 0 THEN 'False'
                         ELSE 'True'
                    END IsDepreciation,
                    CASE WHEN ISNULL(c.[bitOpeningBalanceEquity], 0) = 0 THEN 'False'
                         ELSE 'True'
                    END IsOpeningBalanceEquity,
					c.[monEndingOpeningBal] EndingOpeningBalace,
					c.[monEndingBal] EndingBalance,
					c.[dtEndStatementDate] EndStatementDate,
					CASE WHEN ISNULL(c.[bitProfitLoss], 0) = 0 THEN 'False'
                         ELSE 'True'
                    END IsProfitLoss,
                    c.[vcNumber] AS [AccountNumber]
					,CASE WHEN ISNULL(c.[bitIsSubAccount], 0) = 0 THEN 'False'
                         ELSE 'True'
                     END [IsSubAccount]
					,CCC.vcAccountName AS [ParentAccountName]
					,CASE WHEN ISNULL(c.[IsBankAccount], 0) = 0 THEN 'False'
                         ELSE 'True'
                     END [IsBankAccount]
					,c.[vcStartingCheckNumber] AS [StartingCheckNumber]
					,c.[vcBankRountingNumber] AS [BankRountingNumber]
            FROM    Chart_Of_Accounts c
                    LEFT OUTER JOIN [AccountTypeDetail] AT ON AT.[numAccountTypeID] = c.[numParntAcntTypeId]
                    LEFT JOIN Chart_Of_Accounts CCC ON c.numParentAccId = Ccc.numAccountId
            WHERE   c.numDomainId = @numDomainID
            ORDER BY c.numAccountID ASC
            
--            EXEC USP_ChartofChildAccounts @numDomainId = 72,
--                @numUserCntId = 17, @strSortOn = 'ID',
--                @strSortDirection = 'ASCENDING'
			
        END

        