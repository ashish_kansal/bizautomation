GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
            
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_Item_GetEcommerceWarehouseInTransit')
DROP PROCEDURE USP_Item_GetEcommerceWarehouseInTransit
GO
CREATE PROCEDURE [dbo].[USP_Item_GetEcommerceWarehouseInTransit]    
(
	@numDomainID NUMERIC(18,0)
	,@numItemCode NUMERIC(18,0)
)
AS
BEGIN
	SELECT 
		FORMAT(ISNULL(SUM(numOnOrder),0), '#,##0')
	FROM
		WarehouseItems
	INNER JOIN
		Warehouses
	ON
		WarehouseItems.numWareHouseID = Warehouses.numWareHouseID
	WHERE
		WarehouseItems.numDomainID=@numDomainID
		AND WarehouseItems.numItemID=@numItemCode
END