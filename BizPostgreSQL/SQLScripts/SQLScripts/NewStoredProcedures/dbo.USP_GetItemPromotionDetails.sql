SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetItemPromotionDetails')
DROP PROCEDURE USP_GetItemPromotionDetails
GO
CREATE PROCEDURE [dbo].[USP_GetItemPromotionDetails]
	@numDomainID NUMERIC(18,0),
	@numDivisionID NUMERIC(18,0),
	@numItemCode NUMERIC(18,0),
	@numPromotionID NUMERIC(18,0),
	@numUnitHour FLOAT,
	@monTotalAmount DECIMAL(20,5)
AS
BEGIN
	IF ISNULL(@numPromotionID,0) > 0
	BEGIN
		DECLARE @vcCurrency VARCHAR(10)
		SELECT @vcCurrency=ISNULL(vcCurrency,'$') FROm Domain WHERE numDomainId=@numDomainID

		SELECT TOP 1
			numProId 
			,ISNULL(vcLongDesc,'-') vcLongDesc
			,CONCAT(ISNULL(vcShortDesc,'-'),(CASE WHEN ISNULL(numOrderPromotionID,0) > 0 THEN ' (Coupon code required)' ELSE '' END)) vcShortDesc
			,ISNULL(tintOfferTriggerValueTypeRange,1) tintOfferTriggerValueTypeRange
			,ISNULL(tintOfferTriggerValueType,1) tintOfferTriggerValueType
			,ISNULL(fltOfferTriggerValue,0) fltOfferTriggerValue
			,ISNULL(fltOfferTriggerValueRange,0) fltOfferTriggerValueRange
			,(CASE 
				WHEN ISNULL(tintOfferTriggerValueType,1) = 1 --Based on quantity
				THEN
					CASE
						WHEN ISNULL(tintOfferTriggerValueTypeRange,1) = 2
						THEN
							(CASE WHEN ISNULL(@numUnitHour,0) BETWEEN ISNULL(fltOfferTriggerValue,0) AND ISNULL(fltOfferTriggerValueRange,0) THEN 1 ELSE 0 END)
						ELSE
							(CASE WHEN ISNULL(@numUnitHour,0) >= fltOfferTriggerValue THEN 1 ELSE 0 END)
					END
				WHEN ISNULL(tintOfferTriggerValueType,1) = 2 --Based on amount
				THEN
					CASE
						WHEN ISNULL(tintOfferTriggerValueTypeRange,1) = 2
						THEN
							(CASE WHEN ISNULL(@monTotalAmount,0) BETWEEN ISNULL(fltOfferTriggerValue,0) AND ISNULL(fltOfferTriggerValueRange,0) THEN 1 ELSE 0 END)
						ELSE
							(CASE WHEN ISNULL(@monTotalAmount,0) >= fltOfferTriggerValue THEN 1 ELSE 0 END)
					END
				END) canUsePromotion
			,CONCAT((CASE 
				WHEN ISNULL(tintOfferTriggerValueType,1) = 1 --Based on quantity
				THEN
					(CASE
						WHEN ISNULL(tintOfferTriggerValueTypeRange,1) = 2
						THEN
							(CASE 
								WHEN ISNULL(@numUnitHour,0) BETWEEN ISNULL(fltOfferTriggerValue,0) AND ISNULL(fltOfferTriggerValueRange,0) 
								THEN dbo.GetPromotionDiscountDescription(numProId,@vcCurrency)
								ELSE CONCAT('Increase qty to between ',ISNULL(fltOfferTriggerValue,0),' & ',ISNULL(fltOfferTriggerValueRange,0),' to use promotion') 
							END)
						ELSE
							(CASE 
								WHEN ISNULL(@numUnitHour,0) >= fltOfferTriggerValue 
								THEN dbo.GetPromotionDiscountDescription(numProId,@vcCurrency)
								ELSE CONCAT('Increase qty to ',ISNULL(fltOfferTriggerValue,0),' to use promotion') 
							END)
					END)
				WHEN ISNULL(tintOfferTriggerValueType,1) = 2 --Based on amount
				THEN
					(CASE
						WHEN ISNULL(tintOfferTriggerValueTypeRange,1) = 2
						THEN
							(CASE 
								WHEN ISNULL(@monTotalAmount,0) BETWEEN ISNULL(fltOfferTriggerValue,0) AND ISNULL(fltOfferTriggerValueRange,0) 
								THEN dbo.GetPromotionDiscountDescription(numProId,@vcCurrency)
								ELSE CONCAT('Increase amount to between ',@vcCurrency,ISNULL(fltOfferTriggerValue,0),' & ',@vcCurrency,ISNULL(fltOfferTriggerValueRange,0),' to use promotion') 
							END)
						ELSE
							(CASE 
								WHEN ISNULL(@monTotalAmount,0) >= fltOfferTriggerValue 
								THEN dbo.GetPromotionDiscountDescription(numProId,@vcCurrency) 
								ELSE CONCAT('Increase amount to ',@vcCurrency,ISNULL(fltOfferTriggerValue,0),' to use promotion') 
							END)
					END)
			END),(CASE WHEN ISNULL(numOrderPromotionID,0) > 0 THEN ' (Coupon code required)' ELSE '' END)) vcPromotionStatus
			,tintOfferBasedOn
			,(CASE WHEN ISNULL(numOrderPromotionID,0) > 0 THEN 1 ELSE 0 END) bitRequireCouponCode
		FROM 
			PromotionOffer PO
		WHERE 
			numDomainId=@numDomainID 
			AND numProId = @numPromotionID	
	END
	ELSE
	BEGIN
		DECLARE @numRelationship NUMERIC(18,0)
		DECLARE @numProfile NUMERIC(18,0)

		SELECT 
			@numRelationship=numCompanyType,
			@numProfile=vcProfile
		FROM 
			DivisionMaster D                  
		JOIN 
			CompanyInfo C 
		ON 
			C.numCompanyId=D.numCompanyID                  
		WHERE 
			numDivisionID =@numDivisionID 

		DECLARE @numItemClassification AS NUMERIC(18,0)
		SELECT @numItemClassification = numItemClassification FROM Item WHERE Item.numItemCode = @numItemCode

		SELECT TOP 1
			numProId 
			,ISNULL(vcShortDesc,'-') vcShortDesc
			,(CASE 
				WHEN ISNULL(tintOfferTriggerValueType,1) = 1 --Based on quantity
				THEN
					CASE
						WHEN ISNULL(tintOfferTriggerValueTypeRange,1) = 2
						THEN
							(CASE WHEN ISNULL(@numUnitHour,0) BETWEEN ISNULL(fltOfferTriggerValue,0) AND ISNULL(fltOfferTriggerValueRange,0) THEN 1 ELSE 0 END)
						ELSE
							(CASE WHEN ISNULL(@numUnitHour,0) >= fltOfferTriggerValue THEN 1 ELSE 0 END)
					END
				WHEN ISNULL(tintOfferTriggerValueType,1) = 2 --Based on amount
				THEN
					CASE
						WHEN ISNULL(tintOfferTriggerValueTypeRange,1) = 2
						THEN
							(CASE WHEN ISNULL(@monTotalAmount,0) BETWEEN ISNULL(fltOfferTriggerValue,0) AND ISNULL(fltOfferTriggerValueRange,0) THEN 1 ELSE 0 END)
						ELSE
							(CASE WHEN ISNULL(@monTotalAmount,0) >= fltOfferTriggerValue THEN 1 ELSE 0 END)
					END
			END) canUsePromotion
			,ISNULL(bitRequireCouponCode,0) bitRequireCouponCode
		FROM 
			PromotionOffer PO
		WHERE 
			numDomainId=@numDomainID 
			AND ISNULL(bitEnabled,0)=1 
			AND ISNULL(IsOrderBasedPromotion,0) = 0
			AND 1 = (CASE WHEN bitNeverExpires = 1 THEN 1 ELSE (CASE WHEN GETUTCDATE()>=dtValidFrom AND GETUTCDATE()<=dtValidTo THEN 1 ELSE 0 END) END)
			AND 1 = (CASE 
						WHEN ISNULL(numOrderPromotionID,0) > 0
						THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferOrganizations WHERE numProId=numOrderPromotionID AND tintType=2 AND numRelationship=@numRelationship AND numProfile=@numProfile) > 0 THEN 1 ELSE 0 END)
						ELSE
							(CASE tintCustomersBasedOn 
								WHEN 1 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferOrganizations WHERE numProId=PO.numProId AND tintType=1 AND numDivisionID=@numDivisionID) > 0 THEN 1 ELSE 0 END)
								WHEN 2 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferOrganizations WHERE numProId=PO.numProId AND tintType=2 AND numRelationship=@numRelationship AND numProfile=@numProfile) > 0 THEN 1 ELSE 0 END)
								WHEN 3 THEN 1
								ELSE 0
							END)
					END)
			AND 1 = (CASE 
						WHEN tintOfferBasedOn = 1 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId=PO.numProId AND tintRecordType=5 AND tintType=1 AND numValue=@numItemCode) > 0 THEN 1 ELSE 0 END)
						WHEN tintOfferBasedOn = 2 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId=PO.numProId AND tintRecordType=5 AND tintType=2 AND numValue=@numItemClassification) > 0 THEN 1 ELSE 0 END)
						WHEN tintOfferBasedOn = 4 THEN 1 
						ELSE 0
					END)
		ORDER BY
			(CASE 
				WHEN PO.tintOfferBasedOn = 1 AND PO.tintDiscoutBaseOn = 1 THEN 1
				WHEN PO.tintOfferBasedOn = 2 AND PO.tintDiscoutBaseOn = 1 THEN 2
				WHEN PO.tintOfferBasedOn = 1 AND PO.tintDiscoutBaseOn = 2 THEN 3
				WHEN PO.tintOfferBasedOn = 2 AND PO.tintDiscoutBaseOn = 2 THEN 4
				WHEN PO.tintOfferBasedOn = 1 AND PO.tintDiscoutBaseOn = 3 THEN 5
				WHEN PO.tintOfferBasedOn = 2 AND PO.tintDiscoutBaseOn = 3 THEN 6
			END) ASC,ISNULL(PO.fltOfferTriggerValue,0) DESC
	END
END 
GO