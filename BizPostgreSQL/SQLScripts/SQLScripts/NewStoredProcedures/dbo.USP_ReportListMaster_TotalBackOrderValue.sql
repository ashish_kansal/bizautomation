GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

GO
IF EXISTS (SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ReportListMaster_TotalBackOrderValue')
DROP PROCEDURE USP_ReportListMaster_TotalBackOrderValue
GO

CREATE PROCEDURE [dbo].[USP_ReportListMaster_TotalBackOrderValue]
@numDomainID numeric
,@vcFilterBy TINYINT  -- 1:Assign TO, 2: Record Owner, 3: Teams (Based on Assigned To), 4: Employees
,@vcFilterValue VARCHAR(MAX) 

as  
begin  
WITH
  cteReports
  AS 
(
SELECT 
--isnull(dbo.fn_GetContactName(OpportunityMaster.numRecOwner),'') as RecordOwner
CASE @vcFilterBy
WHEN 1 THEN  isnull(dbo.fn_GetContactName(OpportunityMaster.numAssignedTo),'')
WHEN 2 THEN isnull(dbo.fn_GetContactName(OpportunityMaster.numRecOwner),'')
ELSE isnull(dbo.fn_GetContactName(OpportunityMaster.numRecOwner),'')
END as RecordOwner
,
 CASE @vcFilterBy
 WHEN 1 THen OpportunityMaster.numAssignedTo
when 2 then OpportunityMaster.numRecOwner 
 END AS ID,
 @vcFilterBy as vcFilterBy,
--OpportunityMaster.numAssignedTo as ID,
	ISNULL(((CASE WHEN OpportunityMaster.tintOppType = 1 AND OpportunityMaster.tintOppStatus = 1 THEN (CASE WHEN (Item.charItemType = 'p' AND ISNULL(OpportunityItems.bitDropShip, 0) = 0 AND ISNULL(Item.bitAsset,0)=0)THEN dbo.CheckOrderItemInventoryStatus(OpportunityItems.numItemCode,OpportunityItems.numUnitHour,OpportunityItems.numoppitemtCode,OpportunityItems.numWarehouseItmsID,(CASE WHEN ISNULL(Item.bitKitParent,0) = 1 AND ISNULL(Item.bitAssembly,0) = 1 THEN 0 WHEN ISNULL(Item.bitKitParent,0) = 1 THEN 1 ELSE 0 END)) ELSE 0 END) ELSE 0 END) * (isnull(Item.monAverageCost,'0'))),0) as BackOrderValue
	--,ISNULL(((CASE WHEN OpportunityMaster.tintOppType = 1 AND OpportunityMaster.tintOppStatus = 1 THEN (CASE WHEN (Item.charItemType = 'p' AND ISNULL(OpportunityItems.bitDropShip, 0) = 0 AND ISNULL(Item.bitAsset,0)=0)THEN dbo.CheckOrderItemInventoryStatus(OpportunityItems.numItemCode,OpportunityItems.numUnitHour,OpportunityItems.numoppitemtCode,OpportunityItems.numWarehouseItmsID,(CASE WHEN ISNULL(Item.bitKitParent,0) = 1 AND ISNULL(Item.bitAssembly,0) = 1 THEN 0 WHEN ISNULL(Item.bitKitParent,0) = 1 THEN 1 ELSE 0 END)) ELSE 0 END) ELSE 0 END) * (isnull(WareHouseItems.monWListPrice,'0'))),0) as Sales
	,ISNULL(((CASE WHEN OpportunityMaster.tintOppType = 1 AND OpportunityMaster.tintOppStatus = 1 THEN (CASE WHEN (Item.charItemType = 'p' AND ISNULL(OpportunityItems.bitDropShip, 0) = 0 AND ISNULL(Item.bitAsset,0)=0)THEN dbo.CheckOrderItemInventoryStatus(OpportunityItems.numItemCode,OpportunityItems.numUnitHour,OpportunityItems.numoppitemtCode,OpportunityItems.numWarehouseItmsID,(CASE WHEN ISNULL(Item.bitKitParent,0) = 1 AND ISNULL(Item.bitAssembly,0) = 1 THEN 0 WHEN ISNULL(Item.bitKitParent,0) = 1 THEN 1 ELSE 0 END)) ELSE 0 END) ELSE 0 END) * (isnull(OpportunityItems.monPrice,'0'))),0) as Sales
FROM OpportunityMaster
inner join DivisionMaster on OpportunityMaster.numDivisionId=DivisionMaster.numDivisionId 
inner join CompanyInfo on CompanyInfo.numCompanyid = DivisionMaster.numCompanyid  
inner join AdditionalContactsInformation on OpportunityMaster.numContactId=AdditionalContactsInformation.numContactId  
--Left Join OpportunityBizDocs on OpportunityMaster.numOppId = OpportunityBizDocs.numOppId 
--Left Join OpportunityBizDocItems on OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID 
Left Join OpportunityItems on OpportunityMaster.numOppId = OpportunityItems.numOppId 
--and OpportunityBizDocItems.numOppItemID=OpportunityItems.numoppitemtCode 
Left Join Item on OpportunityItems.numItemcode = Item.numItemcode 
--Left Join Vendor on Vendor.numDomainId = @numDomainID and Vendor.numVendorID = Item.numVendorID and  Item.numItemCode= Vendor.numItemCode 
--Left Join WareHouseItems  on WareHouseItems.numDomainId = @numDomainID and WareHouseItems.numWareHouseItemID = OpportunityItems.numWarehouseItmsID 
--LEFT JOIN WarehouseLocation ON WarehouseLocation.numWLocationID = WareHouseItems.numWLocationID  
--Left Join Warehouses on Warehouses.numDomainId =  @numDomainID and Warehouses.numWareHouseID = WareHouseItems.numWareHouseID  

WHERE 1=1 AND OpportunityMaster.numDomainId = @numDomainID AND (((OpportunityMaster.tintOppType=1 and OpportunityMaster.tintOppStatus=1)))
AND (CASE WHEN OpportunityMaster.tintOppType = 1 AND OpportunityMaster.tintOppStatus = 1 THEN (CASE WHEN (Item.charItemType = 'p' AND ISNULL(OpportunityItems.bitDropShip, 0) = 0 AND ISNULL(Item.bitAsset,0)=0)THEN dbo.CheckOrderItemInventoryStatus(OpportunityItems.numItemCode,OpportunityItems.numUnitHour,OpportunityItems.numoppitemtCode,OpportunityItems.numWarehouseItmsID,(CASE WHEN ISNULL(Item.bitKitParent,0) = 1 AND ISNULL(Item.bitAssembly,0) = 1 THEN 0 WHEN ISNULL(Item.bitKitParent,0) = 1 THEN 1 ELSE 0 END)) ELSE 0 END) ELSE 0 END) >0
 AND
	 1=(CASE   
     WHEN LEN(ISNULL(@vcFilterValue,0)) > 0 AND @vcFilterBy IN (1,2,3)  
     THEN   
      (CASE @vcFilterBy  
       WHEN 1  -- Assign To  
       THEN (CASE WHEN OpportunityMaster.numAssignedTo IN (SELECT Id FROM dbo.SplitIDs(@vcFilterValue,',')) THEN 1 ELSE 0 END)  
       WHEN 2  -- Record Owner  
       THEN (CASE WHEN OpportunityMaster.numRecOwner IN (SELECT Id FROM dbo.SplitIDs(@vcFilterValue,',')) THEN 1 ELSE 0 END)  
       WHEN 3  -- Teams (Based on Assigned To)  
       --THEN (CASE WHEN OpportunityMaster.numAssignedTo IN (SELECT numUserCntID FROM UserTeams WHERE numDomainID=@numDomainID AND numTeam IN (SELECT Id FROM dbo.SplitIDs(@vcFilterValue,','))) THEN 1 ELSE 0 END) 
	    THEN (CASE WHEN OpportunityMaster.numAssignedTo IN (SELECT numContactId FROM AdditionalContactsInformation WHERE numDomainID=@numDomainID AND numTeam IN (SELECT Id FROM dbo.SplitIDs(@vcFilterValue,','))) THEN 1 ELSE 0 END)
      END)  
     ELSE 1   
    END)

)

SELECT cteReports.RecordOwner,Count(cteReports.RecordOwner) as TotalRecords,cteReports.ID,cteReports.vcFilterBy,
CAST(SUM(Sales) as decimal(18,0)) as SalesPrice , CAST(Sum(BackOrderValue) as decimal(18,0)) as CostPrice


 FROM cteReports
  GROUP BY cteReports.RecordOwner,cteReports.ID,cteReports.vcFilterBy
 order by SalesPrice desc

END
GO
