--SELECT * FROM dOMAIN WHERE vcDomainCode is not null


--select * from VIEW_INVENTORYOPPSALES where numDomainId in (select numDomainId from dOMAIN  WHERE vcDomainCode is not null)




--exec USP_MUTLICOMPINVENTORY @numRepDomain=103, @numDomainID=0,@numSubscriberID=103,@FromDate='01/Jan/2008',@ToDate='31/Dec/2008',@ReportType=4,@TopCount=5

--select * from domain where vcdomaincode is not null order by vcdomaincode

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_MultiCompInventory')
DROP PROCEDURE USP_MultiCompInventory
GO
CREATE PROCEDURE USP_MultiCompInventory
(@numDomainID int,
@numSubscriberID int,
@FromDate datetime,
@ToDate datetime,
@ReportType int,
@TopCount int,
@numRepDomain int)
as
begin

declare @Sql varchar(500);

create table #MostSold		
		(numDomainID int, vcDomainName varchar(250), Item varchar(250),SalesUnit numeric(18,3));

if @ReportType=1 -- MOST UNIT SOLD
	begin
	insert into #MostSold

	select DN.numDomainID,DN.vcDomainName, vcItemName + ',' + ItemClass as Item, sum(numUnitHour)  from VIEW_INVENTORYOPPSALES A,
	Domain DN ,
	Domain DNA
	where  A.numDomainID=DN.numDomainID and DN.numSubscriberID=@numSubscriberID and 
			DN.vcDomainCode like DNA.vcDomainCode + '%' and 
			OppDate between @FromDate and @ToDate and 
			DNA.numSubscriberID=@numSubscriberID and DNA.numParentDomainID=@numDomainID
			group by DN.numDomainID,DN.vcDomainName, vcItemName + ',' + ItemClass
	UNION

	select DN.numDomainID,DN.vcDomainName, vcItemName + ',' + ItemClass as Item, sum(numUnitHour)  from VIEW_INVENTORYOPPSALES A,
	Domain DN 
	where  A.numDomainID=DN.numDomainID and
			DN.numDomainID=@numDomainID and
			OppDate between @FromDate and @ToDate and 
			DN.numSubscriberID=@numSubscriberID
	group by DN.numDomainID,DN.vcDomainName, vcItemName + ',' + ItemClass;

	end
ELSE IF @ReportType=2 -- MAXAVG COST 
	begin
	insert into #MostSold

	select DN.numDomainID,DN.vcDomainName, vcItemName + ',' + ItemClass as Item, MAX(AvgCost)  from VIEW_INVENTORYOPPSALES A,
	Domain DN ,
	Domain DNA
	where  A.numDomainID=DN.numDomainID and DN.numSubscriberID=@numSubscriberID and 
			DN.vcDomainCode like DNA.vcDomainCode + '%' and 
			OppDate between @FromDate and @ToDate and 
			DNA.numSubscriberID=@numSubscriberID and DNA.numParentDomainID=@numDomainID
			group by DN.numDomainID,DN.vcDomainName, vcItemName + ',' + ItemClass
	UNION

	select DN.numDomainID,DN.vcDomainName, vcItemName + ',' + ItemClass as Item, MAX(AvgCost)   from VIEW_INVENTORYOPPSALES A,
	Domain DN 
	where  A.numDomainID=DN.numDomainID and
			DN.numDomainID=@numDomainID and
			OppDate between @FromDate and @ToDate and 
			DN.numSubscriberID=@numSubscriberID
	group by DN.numDomainID,DN.vcDomainName, vcItemName + ',' + ItemClass;

	end
ELSE IF @ReportType=3 -- MAXAVG COST 
	begin
	insert into #MostSold

	select DN.numDomainID,DN.vcDomainName, vcItemName + ',' + ItemClass as Item, sum(COGS)  from VIEW_INVENTORYOPPSALES A,
	Domain DN ,
	Domain DNA
	where  A.numDomainID=DN.numDomainID and DN.numSubscriberID=@numSubscriberID and 
			DN.vcDomainCode like DNA.vcDomainCode + '%' and 
			OppDate between @FromDate and @ToDate and 
			DNA.numSubscriberID=@numSubscriberID and DNA.numParentDomainID=@numDomainID
			group by DN.numDomainID,DN.vcDomainName, vcItemName + ',' + ItemClass
	UNION

	select DN.numDomainID,DN.vcDomainName, vcItemName + ',' + ItemClass as Item, sum(COGS)   from VIEW_INVENTORYOPPSALES A,
	Domain DN 
	where  A.numDomainID=DN.numDomainID and
			DN.numDomainID=@numDomainID and
			OppDate between @FromDate and @ToDate and 
			DN.numSubscriberID=@numSubscriberID
	group by DN.numDomainID,DN.vcDomainName, vcItemName + ',' + ItemClass;
	END
ELSE IF @ReportType=4 -- PROFIT
	begin
	insert into #MostSold

	select DN.numDomainID,DN.vcDomainName, vcItemName + ',' + ItemClass as Item, sum(Profit)   from VIEW_INVENTORYOPPSALES A,
	Domain DN ,
	Domain DNA
	where  A.numDomainID=DN.numDomainID and DN.numSubscriberID=@numSubscriberID and 
			DN.vcDomainCode like DNA.vcDomainCode + '%' and 
			OppDate between @FromDate and @ToDate and 
			DNA.numSubscriberID=@numSubscriberID and DNA.numParentDomainID=@numDomainID
			group by DN.numDomainID,DN.vcDomainName, vcItemName + ',' + ItemClass
	UNION

	select DN.numDomainID,DN.vcDomainName, vcItemName + ',' + ItemClass as Item,sum(Profit)    from VIEW_INVENTORYOPPSALES A,
	Domain DN 
	where  A.numDomainID=DN.numDomainID and
			DN.numDomainID=@numDomainID and
			OppDate between @FromDate and @ToDate and 
			DN.numSubscriberID=@numSubscriberID
	group by DN.numDomainID,DN.vcDomainName, vcItemName + ',' + ItemClass;
	end


	set @Sql ='select top ' +  cast( @TopCount as varchar) + ' item,salesunit from #MostSold where numDomainID ='+ cast (@numRepDomain as varchar) + ' order by SalesUnit Desc'
			exec (@SQl)
	drop table #MostSold
end