GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_OpportunityBizDocStatusChange' ) 
    DROP PROCEDURE USP_OpportunityBizDocStatusChange
GO
CREATE PROCEDURE USP_OpportunityBizDocStatusChange
    @numDomainID numeric(18, 0),
    @numOppId numeric(18, 0),
    @numOppBizDocsId numeric(18, 0),
    @numBizDocStatus numeric(18, 0),
    @numUserCntID numeric(18, 0),
    @tintMode AS TINYINT=0
AS 
BEGIN
		IF EXISTS (SELECT numOppId FROM OpportunityBizDocs WHERE numOppId=@numOppId AND numOppBizDocsId=@numOppBizDocsId AND ISNULL(numBizDocStatus,0) = @numBizDocStatus)
		BEGIN
			RETURN 	
		END
		
		IF @tintMode=0
		BEGIN
			IF EXISTS (SELECT * FROM OpportunityAutomationQueue WHERE numDomainID=@numDomainID AND numOppId=@numOppId 
						AND numOppBizDocsId=@numOppBizDocsId AND tintProcessStatus=2)
			BEGIN
				RAISERROR ('NOT_ALLOWED_ChangeBizDicStatus',16,1);
				RETURN -1
			END
		END
				
		DECLARE @numBizDocId AS NUMERIC(9);SET @numBizDocId=0		
		DECLARE @bitAuthoritativeBizDocs AS BIT;SET @bitAuthoritativeBizDocs=0		
		
		UPDATE OpportunityBizDocs SET @numBizDocId=numBizDocId,@bitAuthoritativeBizDocs=isnull(bitAuthoritativeBizDocs,0),numBizDocStatusOLD=numBizDocStatus,numBizDocStatus = @numBizDocStatus,
									  numModifiedBy=@numUserCntID,dtModifiedDate=GETUTCDATE() WHERE numOppId=@numOppId AND numOppBizDocsId=@numOppBizDocsId 

		--IF @numBizDocStatus>0
		--BEGIN
				INSERT INTO dbo.OppFulfillmentBizDocsStatusHistory (
					numDomainId,numOppId,numOppBizDocsId,numBizDocStatus,numUserCntID,dtCreatedDate
				) VALUES (@numDomainID,@numOppID,@numOppBizDocsId,@numBizDocStatus,@numUserCntID,GETUTCDATE())  
		 --END	
		 
		 
		 DECLARE @tintOppType AS TINYINT;SET @tintOppType=0
		 DECLARE @tintOppstatus AS TINYINT;SET @tintOppstatus=0
		 
		 SELECT @tintOppType=ISNULL(tintOppType,0),@tintOppstatus=ISNULL(tintOppstatus,0) FROM OpportunityMaster WHERE numOppID=@numOppID
		 
		 IF @tintOppType=1 AND @tintOppstatus=1
		 BEGIN
					
				 
				IF @bitAuthoritativeBizDocs=1 OR @numBizDocId=296
				BEGIN
				DECLARE @numBizDocStatus11 AS NUMERIC(9);SET @numBizDocStatus11=0
				SELECT @numBizDocStatus11=numBizDocStatus1 FROM OpportunityAutomationRules WHERE numDomainID=@numDomainID AND numRuleID=11

				IF ISNULL(@numBizDocStatus11,0)>0
				BEGIN
					IF NOT EXISTS (SELECT OI.numoppitemtCode FROM dbo.OpportunityMaster OM JOIN dbo.OpportunityItems OI ON OM.numOppId = OI.numOppId
						  LEFT JOIN dbo.OpportunityBizDocs OBD ON OBD.numOppId=OM.numOppId AND OBD.numBizDocId=296 AND ISNULL(OBD.numBizDocStatus,0)=@numBizDocStatus11 AND OBD.numBizDocId=@numBizDocId
						  LEFT JOIN dbo.OpportunityBizDocItems OBDI ON OBDI.numOppBizDocID=OBD.numOppBizDocsId AND OBDI.numOppItemID=OI.numoppitemtCode
						   WHERE OM.numDomainId=@numDomainId AND OM.numOppId=@numOppID GROUP BY OI.numoppitemtCode HAVING ISNULL(SUM(OI.numUnitHour),0) - ISNULL(SUM(OBDI.numUnitHour),0) > 0)
					BEGIN
							EXEC USP_ManageOpportunityAutomationQueue
								@numOppQueueID = 0, -- numeric(18, 0)
								@numDomainID = @numDomainID, -- numeric(18, 0)
								@numOppId = @numOppID, -- numeric(18, 0)
								@numOppBizDocsId = 0, -- numeric(18, 0)
								@numBizDocStatus = 0, -- numeric(18, 0)
								@numUserCntID = @numUserCntID, -- numeric(18, 0)
								@tintProcessStatus = 1, -- tinyint
								@tintMode = 1, -- TINYINT
								@numRuleID = 11
					END	
				END
				
				DECLARE @numBizDocStatus12 AS NUMERIC(9);SET @numBizDocStatus12=0
				DECLARE @numBizDocStatus12_2 AS NUMERIC(9);SET @numBizDocStatus12_2=0
				SELECT @numBizDocStatus12=numBizDocStatus1,@numBizDocStatus12_2=numBizDocStatus2 FROM OpportunityAutomationRules WHERE numDomainID=@numDomainID AND numRuleID=12
				
				IF ISNULL(@numBizDocStatus12,0)>0 AND ISNULL(@numBizDocStatus12_2,0)>0
				BEGIN
					IF NOT EXISTS (SELECT OI.numoppitemtCode FROM dbo.OpportunityMaster OM JOIN dbo.OpportunityItems OI ON OM.numOppId = OI.numOppId
						  LEFT JOIN dbo.OpportunityBizDocs OBD ON OBD.numOppId=OM.numOppId AND OBD.numBizDocId=296 AND ISNULL(OBD.numBizDocStatus,0)=@numBizDocStatus12
						  LEFT JOIN dbo.OpportunityBizDocItems OBDI ON OBDI.numOppBizDocID=OBD.numOppBizDocsId AND OBDI.numOppItemID=OI.numoppitemtCode
						   WHERE OM.numDomainId=@numDomainId AND OM.numOppId=@numOppID GROUP BY OI.numoppitemtCode HAVING ISNULL(SUM(OI.numUnitHour),0) - ISNULL(SUM(OBDI.numUnitHour),0) > 0)
					BEGIN
						IF NOT EXISTS (SELECT OI.numoppitemtCode FROM dbo.OpportunityMaster OM JOIN dbo.OpportunityItems OI ON OM.numOppId = OI.numOppId
						  LEFT JOIN dbo.OpportunityBizDocs OBD ON OBD.numOppId=OM.numOppId AND ISNULL(OBD.bitAuthoritativeBizDocs,0)=1 AND ISNULL(OBD.numBizDocStatus,0)=@numBizDocStatus12_2 
						  LEFT JOIN dbo.OpportunityBizDocItems OBDI ON OBDI.numOppBizDocID=OBD.numOppBizDocsId AND OBDI.numOppItemID=OI.numoppitemtCode
						   WHERE OM.numDomainId=@numDomainId AND OM.numOppId=@numOppID GROUP BY OI.numoppitemtCode HAVING ISNULL(SUM(OI.numUnitHour),0) - ISNULL(SUM(OBDI.numUnitHour),0) > 0)
							BEGIN
										EXEC USP_ManageOpportunityAutomationQueue
												@numOppQueueID = 0, -- numeric(18, 0)
												@numDomainID = @numDomainID, -- numeric(18, 0)
												@numOppId = @numOppID, -- numeric(18, 0)
												@numOppBizDocsId = 0, -- numeric(18, 0)
												@numBizDocStatus = 0, -- numeric(18, 0)
												@numUserCntID = @numUserCntID, -- numeric(18, 0)
												@tintProcessStatus = 1, -- tinyint
												@tintMode = 1, -- TINYINT
												@numRuleID = 12
							END  
					END	
				END
			END 
			
			EXEC USP_ManageOpportunityAutomationQueue
					@numOppQueueID = 0, -- numeric(18, 0)
					@numDomainID = @numDomainID, -- numeric(18, 0)
					@numOppId = @numOppID, -- numeric(18, 0)
					@numOppBizDocsId = @numOppBizDocsId, -- numeric(18, 0)
					@numBizDocStatus = @numBizDocStatus, -- numeric(18, 0)
					@numUserCntID = @numUserCntID, -- numeric(18, 0)
					@tintProcessStatus = 1, -- tinyint
					@tintMode = 1 -- TINYINT
		 END
		 
END



