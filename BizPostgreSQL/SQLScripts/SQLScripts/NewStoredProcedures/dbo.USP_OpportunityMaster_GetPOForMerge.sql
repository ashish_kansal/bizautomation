GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_OpportunityMaster_GetPOForMerge')
DROP PROCEDURE dbo.USP_OpportunityMaster_GetPOForMerge
GO
CREATE PROCEDURE [dbo].[USP_OpportunityMaster_GetPOForMerge]
(
	@numDomainID NUMERIC(18,0)
	,@numWarehouseID NUMERIC(18,0)
	,@bitOnlyMergable BIT
	,@bitExcludePOSendToVendor BIT
	,@bitDisplayOnlyCostSaving BIT
	,@tintCostType TINYINT
	,@ClientTimeZoneOffset INT
)
AS 
BEGIN
	DECLARE @TEMP TABLE
	(
		numDivisionID NUMERIC(18,0)
		,numOppId NUMERIC(18,0)
		,numItemCode NUMERIC(18,0)
		,vcCompanyName VARCHAR(300)
		,vcNavigationURL VARCHAR(300)
		,vcPOppName VARCHAR(300)
		,vcCreatedBy VARCHAR(300)
		,bintCreatedDate VARCHAR(50)
		,monDealAmount DECIMAL(20,5)
		,vcItemName VARCHAR(300)
		,vcSKU VARCHAR(100)
		,fltWeight FLOAT
		,numoppitemtCode NUMERIC(18,0)
		,numUnitHour FLOAT
		,monPrice DECIMAL(20,5)
		,vcItemCostSaving VARCHAR(MAX)
	)

	INSERT INTO @TEMP
	(
		numDivisionID
		,numOppId
		,numItemCode
		,vcCompanyName
		,vcNavigationURL
		,vcPOppName
		,vcCreatedBy
		,bintCreatedDate
		,monDealAmount
		,vcItemName
		,vcSKU
		,fltWeight
		,numoppitemtCode
		,numUnitHour
		,monPrice
		,vcItemCostSaving
	)
	SELECT
		DivisionMaster.numDivisionID
		,OpportunityMaster.numOppId
		,Item.numItemCode
		,CompanyInfo.vcCompanyName
		,(CASE DivisionMaster.tintCRMType 
			WHEN 2 THEN CONCAT('../account/frmAccounts.aspx?DivID=',DivisionMaster.numDivisionID) 
			WHEN 1 THEN CONCAT('../prospects/frmProspects.aspx?DivID=',DivisionMaster.numDivisionID) 
			ELSE CONCAT('../Leads/frmLeads.aspx?DivID=',DivisionMaster.numDivisionID) 
		END)
		,OpportunityMaster.vcPOppName
		,dbo.fn_GetContactName(OpportunityMaster.numCreatedBy) vcCreatedBy
		,dbo.FormatedDateFromDate(DATEADD(MINUTE,@ClientTimeZoneOffset * -1,OpportunityMaster.bintCreatedDate),@numDomainID) bintCreatedDate
		,OpportunityMaster.monDealAmount
		,Item.vcItemName
		,Item.vcSKU
		,ISNULL(Item.fltWeight,0)
		,OpportunityItems.numoppitemtCode
		,ISNULL(OpportunityItems.numUnitHour,0)
		,ISNULL(OpportunityItems.monPrice,0)
		,(CASE 
			WHEN @tintCostType = 2 
			THEN ISNULL(STUFF((SELECT 
					CONCAT(', <input type="button" class="btn btn-xs btn-success" value="$" onClick="return RecordSelected(this,',OM.numDivisionId,',',ISNULL(OI.monPrice,0),',',ISNULL(V.intMinQty,0),')" /> <b>$'
					,FORMAT((ISNULL(OpportunityItems.numUnitHour,0) * ISNULL(OI.monPrice,0)) - (ISNULL(OpportunityItems.numUnitHour,0) * ISNULL(OpportunityItems.monPrice,0)), '#,##0.#####')
					,'</b> (',ISNULL(OI.numUnitHour,0),' U) '
					,CONCAT('<span style="color:',(CASE
								WHEN (DATEDIFF(DAY,OM.bintCreatedDate,GETUTCDATE()) = 0 AND DATEDIFF(DAY,OM.bintCreatedDate,GETUTCDATE()) <= 90)
								THEN '#00b050'
								WHEN (DATEDIFF(DAY,OM.bintCreatedDate,GETUTCDATE()) = 91 AND DATEDIFF(DAY,OM.bintCreatedDate,GETUTCDATE()) <= 180)
								THEN '#ff9933'
								WHEN (DATEDIFF(DAY,OM.bintCreatedDate,GETUTCDATE()) = 181 AND DATEDIFF(DAY,OM.bintCreatedDate,GETUTCDATE()) <= 270)
								THEN '#9148c8'
								ELSE '#ff0000'
							END),'">',DATEDIFF(DAY,OM.bintCreatedDate,GETUTCDATE()),'d</span> ')
						,'<b><a target="_blank" href="../opportunity/frmOpportunities.aspx?OpID=',OM.numOppId,'">',OM.vcPOppName,'</a></b>'
						,' (',CI.vcCompanyName,')') 
				FROM 
					OpportunityMaster OM 
				INNER JOIN
					DivisionMaster DM
				ON
					OM.numDivisionId = DM.numDivisionID
				INNER JOIN
					CompanyInfo CI
				ON
					DM.numCompanyID = CI.numCompanyId
				INNER JOIN 
					OpportunityItems OI 
				ON 
					OM.numOppID=OI.numOppID 
				LEFT JOIN
					Vendor V
				ON
					V.numVendorID = DM.numDivisionID
					AND V.numItemCode = OI.numItemCode
				WHERE 
					OM.numDomainId=@numDomainID 
					AND OM.numOppId <> OpportunityMaster.numOppId
					AND OM.tintOppType = 2 
					AND OM.tintOppStatus = 1 
					AND ISNULL(OM.bitStockTransfer,0) = 0
					AND OI.numItemCode = Item.numItemCode
					AND 1 = (CASE 
								WHEN ISNULL(@bitDisplayOnlyCostSaving,0) = 1 
								THEN (CASE WHEN ISNULL(OI.monPrice,0) <> 0 AND ISNULL(OI.monPrice,0) < ISNULL(OpportunityItems.monPrice,0) THEN 1 ELSE 0 END)
								ELSE 1 
							END)
					AND DATEDIFF(DAY,OM.bintCreatedDate,GETUTCDATE()) <= 600
				ORDER BY
					(ISNULL(OpportunityItems.numUnitHour,0) * ISNULL(OI.monPrice,0)) - (ISNULL(OpportunityItems.numUnitHour,0) * ISNULL(OpportunityItems.monPrice,0)) ASC OFFSET 0 ROWS FETCH NEXT 5 ROWS ONLY
				FOR XML PATH(''), TYPE).value('.', 'varchar(max)'),1,1,''),'') 
			ELSE ''
		END) vcItemCostSaving
	FROM
		OpportunityMaster
	INNER JOIN
		DivisionMaster
	ON
		OpportunityMaster.numDivisionId = DivisionMaster.numDivisionID
	INNER JOIN
		CompanyInfo
	ON
		DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
	INNER JOIN
		OpportunityItems
	ON
		OpportunityMaster.numOppId = OpportunityItems.numOppId
	INNER JOIN
		Item
	ON
		OpportunityItems.numItemCode = Item.numItemCode
	INNER JOIN
		WareHouseItems
	ON
		OpportunityItems.numWarehouseItmsID = WareHouseItems.numWareHouseItemID
	WHERE
		OpportunityMaster.numDomainId=@numDomainID
		AND OpportunityMaster.tintOppType = 2
		AND OpportunityMaster.tintOppStatus = 1
		AND ISNULL(OpportunityMaster.bitStockTransfer,0) = 0
		AND ISNULL(OpportunityMaster.tintshipped,0) = 0
		AND (SELECT COUNT(*) FROM OpportunityBizDocs WHERE OpportunityBizDocs.numOppId=OpportunityMaster.numOppId) = 0
		AND WareHouseItems.numWareHouseID=@numWarehouseID
		AND ISNULL(OpportunityItems.numUnitHourReceived,0) = 0
		AND 1 = (CASE WHEN ISNULL(@bitExcludePOSendToVendor,0)=1 THEN (CASE WHEN EXISTS (SELECT numCorrespondenceID FROM Correspondence WHERE numOpenRecordID=OpportunityMaster.numOppId AND tintCorrType IN (2,4)) THEN 0 ELSE 1 END) ELSE 1 END)
		AND 1 = (CASE 
					WHEN @tintCostType = 2 
					THEN (CASE WHEN EXISTS (SELECT 
												OI.numoppitemtCode
											FROM 
												OpportunityMaster OM 
											INNER JOIN
												DivisionMaster DM
											ON
												OM.numDivisionId = DM.numDivisionID
											INNER JOIN
												CompanyInfo CI
											ON
												DM.numCompanyID = CI.numCompanyId
											INNER JOIN 
												OpportunityItems OI 
											ON 
												OM.numOppID=OI.numOppID 
											WHERE 
												OM.numDomainId=@numDomainID 
												AND OM.numOppId <> OpportunityMaster.numOppId
												AND OM.tintOppType = 2 
												AND OM.tintOppStatus = 1 
												AND ISNULL(OM.bitStockTransfer,0) = 0
												AND OI.numItemCode = Item.numItemCode
												AND 1 = (CASE 
															WHEN ISNULL(@bitDisplayOnlyCostSaving,0) = 1 
															THEN (CASE WHEN ISNULL(OI.monPrice,0) <> 0 AND ISNULL(OI.monPrice,0) < ISNULL(OpportunityItems.monPrice,0) THEN 1 ELSE 0 END)
															ELSE 1 
														END)
												AND DATEDIFF(DAY,OM.bintCreatedDate,GETUTCDATE()) <= 600) THEN 1 ELSE 0 END)
					ELSE 1 
				END)
	
	IF ISNULL(@bitOnlyMergable,0) = 1
	BEGIN
		DELETE FROM @TEMP WHERE numDivisionID IN (SELECT numDivisionID FROM (SELECT numDivisionID,numOppId FROM @TEMP GROUP BY numDivisionID,numOppId) TEMP GROUP BY numDivisionID HAVING COUNT(*) < 2)
	END

	SELECT 
		numDivisionID
		,vcCompanyName
		,vcNavigationURL
		,CONCAT('[',STUFF((SELECT 
					CONCAT(', {"intType":"',PurchaseIncentives.intType ,'", "vcIncentives":"',PurchaseIncentives.vcIncentives,'", "vcbuyingqty":"',PurchaseIncentives.vcbuyingqty,'", "tintDiscountType":"',ISNULL(PurchaseIncentives.tintDiscountType,0),'"}') 
				FROM 
					PurchaseIncentives 
				WHERE
					PurchaseIncentives.numDivisionID = T.numDivisionID
				FOR XML PATH('')),1,1,''),']') AS vcPurchaseIncentive 
	FROM 
		@TEMP T 
	GROUP BY 
		numDivisionID,vcCompanyName,vcNavigationURL
	ORDER BY
		T.vcCompanyName

	SELECT
		numDivisionID
		,numOppId
		,vcPOppName
		,vcCreatedBy
		,bintCreatedDate
		,monDealAmount
		,(CASE WHEN EXISTS (SELECT numCorrespondenceID FROM Correspondence WHERE numOpenRecordID=T.numOppId AND tintCorrType IN (2,4)) THEN 1 ELSE 0 END) AS bitPOSendToVendor
		,ISNULL((SELECT TOP 1
					(CASE 
						WHEN EmailHistory.numEmailHstrID IS NOT NULL 
						THEN CONCAT('<i></i>',dbo.FormatedDateFromDate(DateAdd(minute,-@ClientTimeZoneOffset,EmailHistory.dtReceivedOn),@numDomainID))
						ELSE dbo.FormatedDateFromDate(DateAdd(minute,-@ClientTimeZoneOffset, dtCreatedDate),@numDomainID)
					END) 
				FROM 
					Correspondence 
				LEFT JOIN 
					EmailHistory 
				ON 
					Correspondence.numEmailHistoryID=EmailHistory.numEmailHstrID 
				LEFT JOIN 
					Communication
				ON
					Correspondence.numCommID = Communication.numCommId
				WHERE 
					numOpenRecordID=T.numOppId
					AND tintCorrType IN (2,4)),'') vcPOSend
	FROM
		@TEMP T
	GROUP BY
		numDivisionID,numOppId,vcPOppName,vcCreatedBy,bintCreatedDate,monDealAmount
	ORDER BY
		numOppId

	SELECT
		numOppId
		,numoppitemtCode
		,numItemCode
		,vcItemName
		,vcSKU
		,fltWeight
		,numUnitHour
		,monPrice
		,vcItemCostSaving
	FROM 
		@TEMP T
END
GO