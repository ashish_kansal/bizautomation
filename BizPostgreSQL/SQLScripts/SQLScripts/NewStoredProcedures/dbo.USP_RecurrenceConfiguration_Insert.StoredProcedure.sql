GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_RecurrenceConfiguration_Insert')
DROP PROCEDURE USP_RecurrenceConfiguration_Insert
GO
  
-- =============================================  
-- Author:  Sandeep
-- Create date: 9 Oct 2014
-- Description: Inserts recurrence configuration
-- =============================================  
Create PROCEDURE [dbo].[USP_RecurrenceConfiguration_Insert]   
	@numDomainID NUMERIC(18,0),
	@numUserCntID NUMERIC(18,0),
	@numOppID NUMERIC(18,0),
	@numOppBizDocID NUMERIC(18,0) = NULL,
	@dtStartDate DATE,
	@dtEndDate DATE=NULL,
	@numType SMALLINT,
	@vcType VARCHAR(20),
	@vcFrequency VARCHAR(20),
	@numFrequency SMALLINT
AS  
BEGIN  
 
 SET NOCOUNT ON;  
 
 IF @numType = 1
 BEGIN
	IF EXISTS (SELECT * FROM RecurrenceConfiguration WHERE numOppID = @numOppID AND numDomainID = @numDomainID)
		RETURN
 END

 IF @numType = 2
 BEGIN
	IF EXISTS (SELECT * FROM RecurrenceConfiguration WHERE numOppID = @numOppID AND numOppBizDocID=@numOppBizDocID AND numDomainID = @numDomainID)
		RETURN
 END

INSERT INTO RecurrenceConfiguration
(
	[numDomainID],
	[numOppID],
	[numOppBizDocID],
	[dtStartDate],
	[dtEndDate],
	[numType],
	[vcType],
	[numFrequency],
	[vcFrequency],
	[dtNextRecurrenceDate],
	[numCreatedBy],
	[dtCreatedDate]
)
VALUES
(
	@numDomainId,
	@numOppID,
	@numOppBizDocID,
	@dtStartDate,
	@dtEndDate,
	@numType,
	@vcType,
	@numFrequency,
	@vcFrequency,
	@dtStartDate,
	--(CASE @numFrequency
	--	WHEN 1 THEN DATEADD(D,1,@dtStartDate) --Daily
	--	WHEN 2 THEN DATEADD(D,7,@dtStartDate) --Weekly
	--	WHEN 3 THEN DATEADD(M,1,@dtStartDate) --Monthly
	--	WHEN 4 THEN DATEADD(M,4,@dtStartDate) --Quarterly
	--END),
	@numUserCntID,
	GETDATE()
)

END  
