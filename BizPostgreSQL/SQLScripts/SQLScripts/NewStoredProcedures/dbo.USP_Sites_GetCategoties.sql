SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_Sites_GetCategoties')
DROP PROCEDURE USP_Sites_GetCategoties
GO
CREATE PROCEDURE [dbo].[USP_Sites_GetCategoties] 
	@numDomainID NUMERIC(18,0),
	@numSiteID NUMERIC(18,0)
AS  
BEGIN
	DECLARE @numCategoryProfileID NUMERIC(18,0)

	SELECT  
		@numCategoryProfileID=CPS.numCategoryProfileID
	FROM 
		Sites S
	JOIN
		CategoryProfileSites CPS
	ON
		S.numSiteID=CPS.numSiteID
	JOIN
		CategoryProfile CP
	ON
		CPS.numCategoryProfileID = CP.ID
	WHERE 
		S.numSiteID=@numSiteID

	;WITH CategoryList (numCategoryID,numParentCategoryID,vcCategoryName,tintLevel,intDisplayOrder,vcDescription)
	As 
	( 
		SELECT 
			C.numCategoryID,
			ISNULL(C.numDepCategory,0),
			ISNULL(C.vcCategoryName,''),
			0,
			ISNULL(C.intDisplayOrder,0),
			C.vcDescription
		FROM 
			[SiteCategories] SC
		INNER JOIN 
			[Category] C 
		ON 
			SC.[numCategoryID] = C.[numCategoryID]
		WHERE
			SC.numSiteID=@numSiteID
			AND C.numCategoryProfileID=@numCategoryProfileID
			AND C.numDomainID=@numDomainID
			AND ISNULL(numDepCategory,0) = 0
	 UNION ALL
	 SELECT 
			C.numCategoryID,
			ISNULL(C.numDepCategory,0),
			ISNULL(C.vcCategoryName,''),
			ISNULL(CL.tintLevel + 1,0),
			ISNULL(C.intDisplayOrder,0),
			C.vcDescription
		FROM 
			[SiteCategories] SC
		INNER JOIN 
			[Category] C 
		ON 
			SC.[numCategoryID] = C.[numCategoryID]
		INNER JOIN
			CategoryList CL
		ON
			C.numDepCategory = CL.numCategoryID
		WHERE
			SC.numSiteID=@numSiteID
			AND C.numCategoryProfileID=@numCategoryProfileID
			AND C.numDomainID=@numDomainID
	)

	SELECT * FROM CategoryList ORDER BY tintLevel,intDisplayOrder
END