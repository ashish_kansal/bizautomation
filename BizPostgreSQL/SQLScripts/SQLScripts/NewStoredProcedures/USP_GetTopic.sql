SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetTopic')
DROP PROCEDURE USP_GetTopic
GO
CREATE PROCEDURE [dbo].[USP_GetTopic]
    @intRecordType As Int=0,
	@numRecordId As NUMERIC(18,0),
	@numDomainID As NUMERIC(18,0),
	@bitExternalUser AS BIT =0,
	@numTopicId AS NUMERIC(18,0)=0
AS
BEGIN
	DECLARE @dynamicQuery AS NVARCHAR(MAX)=''
	SET @dynamicQuery = ' select 
			numTopicId, 
			intRecordType, 
			numRecordId, 
			vcTopicTitle, 
			numCreateBy, 
			dtmCreatedOn, 
			numUpdatedBy, 
			dtmUpdatedOn, 
			bitIsInternal, 
			TC.numDomainId,
			(select Count(*) FROM MessageMaster where numTopicId= TC.numTopicId) AS MessageCount,
			dbo.fn_GetContactName(TC.numCreateBy) AS CreatedBy,
			dbo.fn_GetContactName(TC.numUpdatedBy) AS UpdatedBy,
			C.vcCompanyName AS CreatedByOrganization,
			CUP.vcCompanyName AS UpdatedByOrganization,
			ISNULL(LI.vcData,''-'') AS CreatedByDesignation,
			ISNULL(LIUP.vcData,''-'') AS UpdatedByDesignation,
			(select
				   distinct  
					stuff((
						select '','' + TM.vcAttachmentName +''$@#''+CAST(TM.numAttachmentId AS VARCHAR(100))
						from TopicMessageAttachments TM
						where TM.vcAttachmentName = vcAttachmentName AND numTopicId=TC.numTopicId
						order by TM.vcAttachmentName
						for xml path('''')
					),1,1,'''') as userlist
				from TopicMessageAttachments WHERE numTopicId=TC.numTopicId
				group by vcAttachmentName) AS vcAttachmentName,
				ADC.vcEmail AS CreatedByEmail
			From TopicMaster AS TC
			LEFT JOIN AdditionalContactsInformation AS ADC ON ADC.numContactID=TC.numCreateBy
			LEFT JOIN AdditionalContactsInformation AS ADCUP ON ADCUP.numContactID=TC.numUpdatedBy
			LEFT JOIN DivisionMaster AS Div ON Div.numDivisionID=ADC.numDivisionId
			LEFT JOIN DivisionMaster AS DivUP ON DivUP.numDivisionID=ADCUP.numDivisionId
			LEFT JOIN CompanyInfo as C on Div.numCompanyID=C.numCompanyId
			LEFT JOIN CompanyInfo as CUP on DivUP.numCompanyID=CUP.numCompanyId
			LEFT JOIN ListDetails AS LI ON LI.numListItemID=ADC.vcPosition
			LEFT JOIN ListDetails AS LIUP ON LIUP.numListItemID=ADCUP.vcPosition '
		SET @dynamicQuery = @dynamicQuery + ' Where TC.numDomainId='+CAST(@numDomainID AS varchar(500))+' '
		IF(@numTopicId>0)
		BEGIN
			SET @dynamicQuery = @dynamicQuery + ' AND numTopicId= '+CAST(@numTopicId AS varchar(500))+''
		END
		IF(@numRecordId>0)
		BEGIN
			SET @dynamicQuery = @dynamicQuery + ' AND numRecordId= '+CAST(@numRecordId AS varchar(500))+''
		END
		IF(@intRecordType>0)
		BEGIN
			SET @dynamicQuery = @dynamicQuery + ' AND intRecordType= '+CAST(@intRecordType AS varchar(500))+''
		END
		IF(@bitExternalUser=1)
		BEGIN
			SET @dynamicQuery = @dynamicQuery + ' AND ISNULL(bitIsInternal,0)=0 '
		END
		 SET @dynamicQuery = @dynamicQuery +' 	 ORDER BY dtmCreatedOn desc  '
		EXEC(@dynamicQuery)	
END