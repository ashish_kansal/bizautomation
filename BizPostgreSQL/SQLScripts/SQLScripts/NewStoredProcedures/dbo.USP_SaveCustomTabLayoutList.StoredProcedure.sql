/****** Object:  StoredProcedure [dbo].[USP_SaveCustomTabLayoutList]    Script Date: 07/26/2008 16:21:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_SaveCustomTabLayoutList')
DROP PROCEDURE USP_SaveCustomTabLayoutList
GO
CREATE PROCEDURE [dbo].[USP_SaveCustomTabLayoutList]                                  
@numUserCntID Numeric=0,                                  
@numDomainID Numeric=0,                         
@strRow as text='',                      
@intcoulmn numeric=0,   
@numFormID as numeric=0,                          
@numRelCntType numeric=0,
@tintPageType TINYINT,
@numTabID as numeric(9)=0            
AS                            
                        
delete  DycFormConfigurationDetails where numUserCntId=@numUserCntID and numDomainId=@numDomainId 
			and intColumnNum >= @intcoulmn and  numRelCntType=@numRelCntType AND numFormId=@numFormID
			AND tintPageType=@tintPageType and numAuthGroupID=@numTabID
  
                        
if convert(varchar(10),@strRow) <> ''                            
begin                                                        
   DECLARE @hDoc4 int                                                                
   EXEC sp_xml_preparedocument @hDoc4 OUTPUT, @strRow                                                                
                                                                  
    insert into  DycFormConfigurationDetails(numFormId,numFieldId,intRowNum,intColumnNum,
    numUserCntId,numDomainId,numRelCntType,bitCustom,tintPageType,numAuthGroupID)                        
 select @numFormId,*,@tintPageType,@numTabID from (                           
SELECT * FROM OPENXML (@hDoc4,'/NewDataSet/Table',2)                                                                
 WITH  (                                                            
  numFieldID numeric(9),                        
  tintRow numeric(9),                        
  intColumn numeric(9),                        
  numUserCntId numeric(9),                        
  numDomainID numeric(9),        
numRelCntType numeric(9),        
bitCustomField bit))X                         
                               
                           
end                          
                       
                
EXEC sp_xml_removedocument @hDoc4
GO
