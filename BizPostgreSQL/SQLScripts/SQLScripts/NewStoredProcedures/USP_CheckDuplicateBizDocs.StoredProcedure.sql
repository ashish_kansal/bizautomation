GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_CheckDuplicateBizDocs' ) 
    DROP PROCEDURE USP_CheckDuplicateBizDocs
GO
-- =============================================
-- Author:		<Author,,Sachin Sadhu>
-- Create date: <Create Date,,10thJun2014>
-- Description:	<Description,,BizDoc details>
-- =============================================
CREATE PROCEDURE USP_CheckDuplicateBizDocs
	-- Add the parameters for the stored procedure here
 @numDomainId numeric(18,0) ,
 @numBizDocId numeric(18,0),
 @tintOppType int,
 @numBizDocTemplateID numeric(18,0),
 @numOppId numeric(18,0)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
SELECT 'col1' FROM OpportunityBizDocs as od INNER JOIN OpportunityMaster o on od.numOppId=o.numOppId  
			  WHERE o.numDomainId=@numDomainId AND
					  od.numBizDocId=@numBizDocId AND 
					  o.tintOppType=@tintOppType AND
					  od.numBizDocTempID=@numBizDocTemplateID
					  and o.numOppId=@numOppId
END


