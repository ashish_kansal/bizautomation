/****** Object:  StoredProcedure [dbo].[Usp_CreateCloneItem]    Script Date: 18/06/2013 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created By Manish Anjara
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='Usp_CreateCloneItem')
DROP PROCEDURE Usp_CreateCloneItem
GO
CREATE PROCEDURE [dbo].[Usp_CreateCloneItem]
(
	@numItemCode		BIGINT OUTPUT,
	@numDomainID		NUMERIC(18,0)
)
AS 

BEGIN
--Item 	
--ItemAPI-
--ItemCategory
--ItemDetails
--ItemImages
--ItemTax
--Vendor,
--WareHouseItems
--CFW_FLD_Values_Item
BEGIN TRY
BEGIN TRANSACTION
	DECLARE @bitCloneLocationWithItem BIT
	DECLARE @numItemGroup AS NUMERIC(18,0)
	DECLARE @bitMatrix AS NUMERIC(18,0)
	DECLARE @vcItemName AS VARCHAR(300)

	SELECT @bitCloneLocationWithItem=ISNULL(bitCloneLocationWithItem,0) FROM Domain WHERE numDomainID=@numDomainID
	SELECT @numItemGroup=numItemGroup,@bitMatrix=bitMatrix,@vcItemName=vcItemName FROM Item WHERE numItemCode = @numItemCode AND numDomainID = @numDomainID

	DECLARE @numNewItemCode AS BIGINT
	
	INSERT INTO dbo.Item 
	(vcItemName,txtItemDesc,charItemType,monListPrice,numItemClassification,bitTaxable,vcSKU,bitKitParent,numVendorID,numDomainID,numCreatedBy,bintCreatedDate,
	bintModifiedDate,numModifiedBy,bitAllowBackOrder,bitSerialized,vcModelID,numItemGroup,numCOGsChartAcntId,numAssetChartAcntId,numIncomeChartAcntId,
	monAverageCost,monCampaignLabourCost,bitFreeShipping,fltLength,fltWidth,fltHeight,fltWeight,vcUnitofMeasure,bitCalAmtBasedonDepItems,bitShowDeptItemDesc,
	bitShowDeptItem,bitAssembly,numBarCodeId,vcManufacturer,numBaseUnit,numPurchaseUnit,numSaleUnit,bitLotNo,IsArchieve,numItemClass,vcPathForImage,
	vcPathForTImage,tintStandardProductIDType,numShipClass,vcExportToAPI,bitAllowDropShip,bitVirtualInventory,bitContainer,bitMatrix,numManufacturer,vcASIN,tintKitAssemblyPriceBasedOn,
	bitSOWorkOrder,bitKitSingleSelect,numExpenseChartAcntId,bitExpenseItem,numBusinessProcessId)
	SELECT vcItemName,txtItemDesc,charItemType,monListPrice,numItemClassification,bitTaxable,vcSKU,bitKitParent,numVendorID,numDomainID,numCreatedBy,GETDATE(),
	GETDATE(),numModifiedBy,bitAllowBackOrder,bitSerialized,vcModelID,numItemGroup,numCOGsChartAcntId,numAssetChartAcntId,numIncomeChartAcntId,
	0,monCampaignLabourCost,bitFreeShipping,fltLength,fltWidth,fltHeight,fltWeight,vcUnitofMeasure,bitCalAmtBasedonDepItems,bitShowDeptItemDesc,
	bitShowDeptItem,bitAssembly,numBarCodeId,vcManufacturer,numBaseUnit,numPurchaseUnit,numSaleUnit,bitLotNo,IsArchieve,numItemClass,vcPathForImage,
	vcPathForTImage,tintStandardProductIDType,numShipClass,vcExportToAPI,bitAllowDropShip,bitVirtualInventory,bitContainer,bitMatrix,numManufacturer,vcASIN,tintKitAssemblyPriceBasedOn,
	bitSOWorkOrder,bitKitSingleSelect,numExpenseChartAcntId,bitExpenseItem,numBusinessProcessId
	FROM Item WHERE numItemCode = @numItemCode AND numDomainID = @numDomainID
	 
	SELECT @numNewItemCode  = SCOPE_IDENTITY()


	IF ISNULL(@bitMatrix,0) = 1
	BEGIN
		DECLARE @vcProductVariation VARCHAR(300)

		WITH data (ProductId, ProductOptionGroupId, ProductOptionId) AS 
		(
			SELECT
				@numItemCode
				,CFW_Fld_Master.Fld_id
				,ListDetails.numListItemID
			FROM
				ItemGroupsDTL
			INNER JOIN
				CFW_Fld_Master
			ON
				ItemGroupsDTL.numOppAccAttrID = CFW_Fld_Master.Fld_id
			INNER JOIN
				ListDetails
			ON
				CFW_Fld_Master.numlistid = ListDetails.numListID
			WHERE
				numItemGroupID = @numItemGroup
		),
		ranked AS (
		/* ranking the group IDs */
			SELECT
				ProductId,
				ProductOptionGroupId,
				ProductOptionId,
				GroupRank = DENSE_RANK() OVER (PARTITION BY ProductId ORDER BY ProductOptionGroupId)
			FROM 
				data
		),
		crossjoined AS (
			/* obtaining all possible combinations */
			SELECT
				ProductId,
				ProductOptionGroupId,
				GroupRank,
				ProductVariant = CAST(CONCAT(ProductOptionGroupId,':',ProductOptionId) AS varchar(250))
			FROM 
				ranked
			WHERE 
				GroupRank = 1
			UNION ALL
			SELECT
				r.ProductId,
				r.ProductOptionGroupId,
				r.GroupRank,
				ProductVariant = CAST(CONCAT(c.ProductVariant,',',r.ProductOptionGroupId,':',r.ProductOptionId) AS VARCHAR(250))
			FROM 
				ranked r
			INNER JOIN 
				crossjoined c 
			ON 
				r.ProductId = c.ProductId
			AND 
				r.GroupRank = c.GroupRank + 1
		  ),
		  maxranks AS (
			/* getting the maximum group rank value for every product */
			SELECT
				ProductId,
				MaxRank = MAX(GroupRank)
			FROM 
				ranked
			GROUP BY 
				ProductId
		  )
		
		SELECT TOP 1  
			@vcProductVariation = c.ProductVariant 
		FROM 
			crossjoined c 
		INNER JOIN 
			maxranks m 
		ON 
			c.ProductId = m.ProductId
			AND c.GroupRank = m.MaxRank
			AND c.ProductVariant NOT IN (SELECT 
											STUFF((SELECT ',' + CONCAT(FLD_ID,':',FLD_Value) FROM ItemAttributes WHERE numDomainID=@numDomainID AND numItemCode=IR.numItemCode FOR XML PATH(''),TYPE).value('(./text())[1]','VARCHAR(MAX)'),1,1,'') AS NameValues
										FROM 
											Item
										INNER JOIN
											ItemAttributes IR
										ON
											IR.numDomainID=@numDomainID
											AND Item.numItemCode =IR.numItemCode
										WHERE 
											Item.numDomainID=@numDomainID
											AND bitMatrix = 1
											AND numItemGroup = @numItemGroup
											AND vcItemName = @vcItemName
										GROUP BY
											IR.numItemCode)

		IF LEN(ISNULL(@vcProductVariation,'')) = 0
		BEGIN
			RAISERROR('ATTRIBUTES_ARE_NOT_AVAILABLE_OR_NO_COMBINATION_LEFT_TO_ADD',16,1)
		END
		ELSE
		BEGIN
			INSERT INTO ItemAttributes
			(
				numDomainID
				,numItemCode
				,FLD_ID
				,FLD_Value
			)
			SELECT
				@numDomainID
				,@numNewItemCode
				,SUBSTRING(OutParam,0,CHARINDEX(':',OutParam,0))
				,SUBSTRING(OutParam,CHARINDEX(':',OutParam,0) + 1,LEN(OutParam))
			FROM
				dbo.SplitString(@vcProductVariation,',')
		END
	END
	 
	INSERT INTO dbo.ItemAPI 
	(WebApiId,numDomainId,numItemID,vcAPIItemID,numCreatedby,dtCreated,numModifiedby,dtModified)
	SELECT WebApiId,numDomainId,@numNewItemCode,vcAPIItemID,numCreatedby,GETDATE(),numModifiedby,GETDATE()
	FROM dbo.ItemAPI WHERE numItemID = @numItemCode AND numDomainId = @numDomainId
	
	INSERT INTO dbo.ItemCategory ( numItemID,numCategoryID )
	SELECT @numNewItemCode, numCategoryID FROM ItemCategory WHERE numItemID = @numItemCode
	
	INSERT INTO dbo.ItemDetails (numItemKitID,numChildItemID,numQtyItemsReq,numUOMId,vcItemDesc,sintOrder)
	SELECT @numNewItemCode,numChildItemID,numQtyItemsReq,numUOMId,vcItemDesc,sintOrder FROM dbo.ItemDetails WHERE numItemKitID = @numItemCode
	
	INSERT INTO dbo.ItemImages (numItemCode,vcPathForImage,vcPathForTImage,bitDefault,intDisplayOrder,numDomainId,bitIsImage)
	SELECT 
		@numNewItemCode
		,CONCAT(SUBSTRING(vcPathForImage, 0, CHARINDEX('.',vcPathForImage,0)),'_',@numNewItemCode,SUBSTRING(vcPathForImage, charindex('.',vcPathForImage,0), LEN(vcPathForImage)))
		,CONCAT(SUBSTRING(vcPathForTImage, 0, CHARINDEX('.',vcPathForTImage,0)),'_',@numNewItemCode,SUBSTRING(vcPathForTImage, charindex('.',vcPathForTImage,0), LEN(vcPathForTImage)))
		,bitDefault
		,intDisplayOrder
		,numDomainId
		,bitIsImage 
	FROM dbo.ItemImages WHERE numItemCode = @numItemCode  AND numDomainId = @numDomainID
	
	INSERT INTO dbo.ItemTax (numItemCode,numTaxItemID,bitApplicable)
	SELECT @numNewItemCode,numTaxItemID,bitApplicable FROM dbo.ItemTax WHERE numItemCode = @numItemCode
	
	INSERT INTO dbo.Vendor (numVendorID,vcPartNo,numDomainID,monCost,numItemCode,intMinQty)
	SELECT numVendorID,vcPartNo,numDomainID,monCost,@numNewItemCode,intMinQty 
	FROM dbo.Vendor WHERE numItemCode = @numItemCode AND numDomainID = @numDomainID
	
	INSERT INTO dbo.WareHouseItems (numItemID,numWareHouseID,numOnHand,numOnOrder,numReorder,numAllocation,numBackOrder,monWListPrice,vcLocation,vcWHSKU,
	vcBarCode,numDomainID,dtModified,numWLocationID) 
	SELECT @numNewItemCode,numWareHouseID,0,0,0,0,0,monWListPrice,vcLocation,vcWHSKU,vcBarCode,numDomainID,GETDATE(),numWLocationID 
	FROM dbo.WareHouseItems  WHERE numItemID = @numItemCode AND numDomainID = @numDomainID AND 1 = (CASE WHEN @bitCloneLocationWithItem=1 THEN 1 ELSE (CASE WHEN ISNULL(numWLocationID,0) = 0 THEN 1 ELSE 0 END) END)

	IF ISNULL(@bitMatrix,0) = 1 AND (SELECT COUNT(*) FROM ItemAttributes WHERE numDomainID=@numDomainID AND numItemCode=@numNewItemCode) > 0
	BEGIN
		-- INSERT NEW WAREHOUSE ATTRIBUTES
		INSERT INTO CFW_Fld_Values_Serialized_Items
		(
			Fld_ID,
			Fld_Value,
			RecId,
			bitSerialized
		)                                                  
		SELECT 
			Fld_ID,
			ISNULL(CAST(Fld_Value AS VARCHAR),'') AS Fld_Value,
			TEMPWarehouse.numWarehouseItemID,
			0 
		FROM 
			ItemAttributes  
		CROSS APPLY
		(
			SELECT
				numWarehouseItemID
			FROM 
				WarehouseItems 
			WHERE 
				numDomainID=@numDomainID 
				AND numItemID=@numNewItemCode
		) AS TEMPWarehouse
		WHERE
			numDomainID=@numDomainID 
			AND numItemCode=@numNewItemCode
	END
	
	INSERT INTO CFW_FLD_Values_Item (Fld_ID,Fld_Value,RecId)  
	SELECT Fld_ID,Fld_Value,@numNewItemCode FROM CFW_FLD_Values_Item  WHERE RecId = @numItemCode 


	SET @numItemCode = @numNewItemCode
COMMIT
END TRY
BEGIN CATCH
	DECLARE @ErrorMessage NVARCHAR(4000)
	DECLARE @ErrorNumber INT
	DECLARE @ErrorSeverity INT
	DECLARE @ErrorState INT
	DECLARE @ErrorLine INT
	DECLARE @ErrorProcedure NVARCHAR(200)

	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;

	SELECT 
		@ErrorMessage = ERROR_MESSAGE(),
		@ErrorNumber = ERROR_NUMBER(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE(),
		@ErrorLine = ERROR_LINE(),
		@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

	RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);
END CATCH	
END