SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--exec USP_CaseDetails @numCaseId = 34                  
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_SaveCaseSolutionShortMessage')
DROP PROCEDURE USP_SaveCaseSolutionShortMessage
GO
CREATE PROCEDURE [dbo].[USP_SaveCaseSolutionShortMessage]                                                
@numDomainID bigint ,      
@numCaseId as bigint   ,                   
@vcSolutionShortMessage as varchar(250)=null ,
@numUserCntID AS bigint
AS
BEGIN
	UPDATE Cases SET vcSolutionShortMessage=@vcSolutionShortMessage WHERE numDomainID=@numDomainID AND numCaseId=@numCaseId
	insert into Comments(numCaseID,vcHeading,txtComments,numCreatedby,bintCreatedDate,numStageID)    
values(@numCaseId,'',@vcSolutionShortMessage,@numUserCntID,getutcdate(),NULL)  
END