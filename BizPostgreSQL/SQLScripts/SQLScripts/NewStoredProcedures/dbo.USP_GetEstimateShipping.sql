SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
         
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetEstimateShipping')
DROP PROCEDURE USP_GetEstimateShipping
GO
CREATE PROCEDURE [dbo].[USP_GetEstimateShipping]
	@numDomainID NUMERIC(18,0)
	,@numShipVia INT
	,@numShipService INT
AS 
BEGIN
	SELECT 
		numServiceTypeID
		,0 AS numRuleID
		,numShippingCompanyID
		,vcServiceName
		,monRate
		,intNsoftEnum
		,intFrom
		,intTo
		,fltMarkup
		,bitMarkUpType
		,bitEnabled
		,1 AS tintFixedShippingQuotesMode
		,L.vcData AS vcShippingCompanyName
		,S.vcItemClassification
		,SR.bitItemClassification
		,SR.numWareHouseID
		,SR.numSiteID
		,SR.numRuleID AS numItemClassificationRuleID
	FROM 
		ShippingServiceTypes S
	LEFT JOIN 
		ListDetails AS L
	ON
		S.numShippingCompanyID=L.numListItemID
	LEFT JOIN ShippingRules AS SR ON S.numRuleID=SR.numRuleID
	WHERE 
		S.numDomainID=@numDomainID 
		AND 1 = (CASE 
					WHEN ISNULL(@numShipVia,0) > 0 AND ISNULL(@numShipService,0) > 0 
					THEN (CASE WHEN ISNULL(S.numShippingCompanyID,0)=@numShipVia AND ISNULL(S.intNsoftEnum,0) = @numShipService THEN 1 ELSE 0 END)
					ELSE (CASE WHEN ISNULL(bitEnabled,0)=1 THEN 1 ELSE 0 END) 
				END)
END      




