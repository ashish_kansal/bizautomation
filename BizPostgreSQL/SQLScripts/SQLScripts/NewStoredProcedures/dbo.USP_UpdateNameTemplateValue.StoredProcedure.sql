GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_UpdateNameTemplateValue')
DROP PROCEDURE USP_UpdateNameTemplateValue
GO
CREATE PROCEDURE USP_UpdateNameTemplateValue
@tintType TINYINT,
@numDomainID NUMERIC,
@RecordID numeric
AS 
BEGIN
	DECLARE @Tempate VARCHAR(200)
	DECLARE @OrgName AS VARCHAR(100)
	
	DECLARE @numSequenceId NUMERIC(9),@numMinLength NUMERIC(9)
	SET @numSequenceId=0

	  IF NOT EXISTS ( SELECT  * FROM dbo.NameTemplate WHERE   numDomainID = @numDomainID AND tintModuleID=1) 
                BEGIN
                    INSERT  INTO dbo.NameTemplate (numDomainID,vcModuleName,vcNameTemplate,
                                                    tintModuleID,numSequenceId,numMinLength,numRecordID)
                            SELECT  @numDomainID,vcModuleName,vcNameTemplate,
                                    tintModuleID,numSequenceId,numMinLength,numRecordID
                            FROM    dbo.NameTemplate
                            WHERE   numDomainID = 0	AND tintModuleID=1
                END
		
	--Get Custom Template If enabled
	SELECT @Tempate = ISNULL(vcNameTemplate,''),@numSequenceId=ISNULL(numSequenceId,1),@numMinLength=ISNULL(numMinLength,4) FROM dbo.NameTemplate WHERE numDomainID=@numDomainID 
	AND tintModuleID=1 AND numRecordID=@tintType
	
	--Replace Other Values
	SET @Tempate = REPLACE(@Tempate,'$YEAR$',DATENAME(year ,GETUTCDATE()) )
	SET @Tempate = REPLACE(@Tempate,'$MONTH$',DATENAME(month ,GETUTCDATE()) )
	SET @Tempate = REPLACE(@Tempate,'$DAY$',DATENAME(day ,GETUTCDATE()) )
	SET @Tempate = REPLACE(@Tempate,'$QUARTER$',DATENAME(quarter ,GETUTCDATE()) )
		
--		SELECT DATENAME(quarter,GETUTCDATE())
	
		--Replace Organization Name
		IF CHARINDEX(lower(@Tempate),'$organizationname$')>=0
		BEGIN
			SELECT @OrgName= ISNULL(C.vcCompanyName,'') FROM dbo.OpportunityMaster OM INNER JOIN dbo.DivisionMaster DM ON OM.numDivisionId=DM.numDivisionID
			INNER JOIN dbo.CompanyInfo C ON C.numCompanyId = DM.numCompanyID
			WHERE numOppId=@RecordID;
			
			SET @Tempate = REPLACE(@Tempate,'$OrganizationName$',@OrgName)
		END
	
		SET @Tempate = REPLACE(@Tempate,'$OrderID$',@RecordID )
		
	    SET @Tempate = @Tempate + ISNULL(REPLICATE('0', @numMinLength - LEN(@numSequenceId)),'') + CAST(@numSequenceId AS VARCHAR(18))
	
		UPDATE dbo.OpportunityMaster SET vcPOppName = ISNULL(@Tempate,ISNULL(vcPOppName,'')) WHERE numOppId=@RecordID		
		
		UPDATE NameTemplate SET numSequenceId=@numSequenceId + 1  WHERE numDomainID=@numDomainID 
			AND tintModuleID=1 AND numRecordID=@tintType
END
/*
EXEC USP_UpdateNameTemplateValue
	@tintType = 1, -- TINYINT
	@numDomainID = 1, -- NUMERIC
	@RecordID = 12494 -- numeric
SELECT * FROM dbo.OpportunityMaster WHERE numDomainId=1	AND numOppId=12494

EXEC USP_UpdateNameTemplateValue
	@tintType = 3, -- TINYINT
	@numDomainID = 1, -- NUMERIC
	@RecordID = 12138 -- numeric
SELECT * FROM dbo.OpportunityBizDocs WHERE numOppId=12494
*/