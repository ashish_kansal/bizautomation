GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
                    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_StagePercentageDetailsTask_GetByStage')
DROP PROCEDURE USP_StagePercentageDetailsTask_GetByStage
GO
CREATE PROCEDURE [dbo].[USP_StagePercentageDetailsTask_GetByStage]                             
	@numDomainID AS NUMERIC(18,0)                            
	,@numStageDetailsId NUMERIC(18,0)
AS                            
BEGIN
	SELECT
		numTaskId
		,vcTaskName
	FROM
		StagePercentageDetailsTask
	WHERE
		numDomainId=@numDomainID
		AND numStageDetailsId=@numStageDetailsId
END
GO