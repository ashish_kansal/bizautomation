/****** Object:  StoredProcedure [dbo].[USP_LeadConversionReport]    Script Date: 07/26/2008 16:19:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Create by Siva           
             
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ItemListForContainer')
DROP PROCEDURE USP_ItemListForContainer
GO
CREATE PROCEDURE [dbo].[USP_ItemListForContainer]            
@numDomainID numeric=0           
As                     
Begin                    
       SELECT 
			numItemCode,vcItemName
	   FROM
			Item WITH (NOLOCK)
		WHERE
			numDomainID=@numDomainID AND ISNULL(bitContainer,0)=1
End
GO
