GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_StagePercentageDetailsTask_Close')
DROP PROCEDURE dbo.USP_StagePercentageDetailsTask_Close
GO
CREATE PROCEDURE [dbo].[USP_StagePercentageDetailsTask_Close]
(
	@numDomainID NUMERIC(18,0)
	,@numUserCntID NUMERIC(18,0)
	,@numTaskID NUMERIC(18,0)
)
AS 
BEGIN
	IF EXISTS (SELECT numTaskId FROM StagePercentageDetailsTask WHERE numDomainID=@numDomainID AND numTaskId = @numTaskID AND ISNULL(bitTaskClosed,0) = 0)
	BEGIN
		UPDATE
			StagePercentageDetailsTask
		SET
			bitTaskClosed=1
		WHERE
			numDomainID=@numDomainID 
			AND numTaskId = @numTaskID

		DECLARE @numOppId INT,@numProjectId INT,@numWorkOrderId INT

		SELECT 
			@numOppId=ISNULL(numOppId,0)
			,@numProjectId=ISNULL(numProjectId,0)
			,@numWorkOrderId=ISNULL(numWorkOrderId,0)
		FROM 
			StagePercentageDetailsTask
		WHERE 
			numDomainID=@numDomainID 
			AND numTaskId = @numTaskID

		IF(@numOppId>0 OR @numProjectId>0 OR @numWorkOrderID > 0)
		BEGIN
			DECLARE @intTotalProgress AS INT =0
			DECLARE @intTotalTaskCount AS INT=0
			DECLARE @intTotalTaskClosed AS INT =0
			
			SET @intTotalTaskCount=(SELECT COUNT(*) FROM StagePercentageDetailsTask WHERE (numOppId=@numOppId AND numProjectId=@numProjectId AND ISNULL(numWorkOrderId,0)=@numWorkOrderID) AND bitSavedTask=1)
			SET @intTotalTaskClosed=(SELECT COUNT(*) FROM StagePercentageDetailsTask WHERE (numOppId=@numOppId AND numProjectId=@numProjectId AND ISNULL(numWorkOrderId,0)=@numWorkOrderID) AND bitSavedTask=1 AND bitTaskClosed=1)
			
			IF(@intTotalTaskCount>0 AND @intTotalTaskClosed>0)
			BEGIN
				SET @intTotalProgress=ROUND(((@intTotalTaskClosed*100)/@intTotalTaskCount),0)
			END
			IF((SELECT COUNT(*) FROM ProjectProgress WHERE (ISNULL(numOppId,0)=@numOppId AND ISNULL(numProId,0)=@numProjectId AND ISNULL(numWorkOrderId,0)=@numWorkOrderID)AND numDomainId=@numDomainID)>0)
			BEGIN
				UPDATE ProjectProgress SET intTotalProgress=@intTotalProgress WHERE (ISNULL(numOppId,0)=@numOppId AND ISNULL(numProId,0)=@numProjectId AND ISNULL(numWorkOrderId,0)=@numWorkOrderID) AND numDomainId=@numDomainID
			END
			ELSE
			BEGIN
				IF(@numOppId=0)
				BEGIN
					SET @numOppId = NULL
				END
				IF(@numProjectId=0)
				BEGIN
					SET @numProjectId = NULL
				END
				IF (@numWorkOrderID = 0)
				BEGIN
					SET @numWorkOrderID = NULL
				END

				INSERT INTO ProjectProgress
				(
					numOppId,
					numProId,
					numWorkOrderID,
					numDomainId,
					intTotalProgress
				)VALUES(
					@numOppId,
					@numProjectId,
					@numWorkOrderID,
					@numDomainID,
					@intTotalProgress
				)
			END
		END
	END
END
GO