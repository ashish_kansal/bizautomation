GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ProjectsMaster_GetAssociatedContacts')
DROP PROCEDURE USP_ProjectsMaster_GetAssociatedContacts
GO
CREATE PROCEDURE [dbo].[USP_ProjectsMaster_GetAssociatedContacts]
(
	@numDomainID NUMERIC(18,0)
	,@numProId NUMERIC(18,0)
)
AS
BEGIN
	DECLARE @TEMPContacts TABLE
	(
		numContactID NUMERIC(18,0)
		,vcContact VARCHAR(200)
		,vcCompanyname VARCHAR(300)
		,tinUserType TINYINT
		,ActivityID NUMERIC(18,0)
		,Email VARCHAR(150)
		,vcStatus VARCHAR(200)
		,vcPosition VARCHAR(200)
		,vcEmail VARCHAR(200)
		,numPhone VARCHAR(200)
		,numPhoneExtension VARCHAR(200)
		,bitDefault BIT
		,vcUserName VARCHAR(200)
		,numRights TINYINT
		,numProjectRole NUMERIC(18,0)
	)

	INSERT INTO 
		@TEMPContacts
	SELECT 
		AC.numContactId
		,CONCAT(ISNULL(vcFirstName,''),' ',ISNULL(vcLastName,'')) AS vcContact
		,CI.vcCompanyName AS vcCompanyname
		,CASE WHEN UM.numUserDetailId IS NULL THEN 2 ELSE 1 END AS tinUserType
		,0 AS ActivityID
		,AC.vcEmail as Email
		,'' vcStatus
		,dbo.GetListIemName(AC.vcPosition) vcPosition
		,AC.vcEmail
		,AC.numPhone
		,AC.numPhoneExtension
		,1
		,CONCAT(ISNULL(vcFirstName,''),' ',ISNULL(vcLastName,''))
		,1
		,0
	FROM
		ProjectsMaster PM
	INNER JOIN 
		AdditionalContactsInformation AC 
	ON 
		PM.numIntPrjMgr=AC.numContactID  
	LEFT JOIN
		DivisionMaster DM 
	ON 
		DM.numDivisionID=AC.numDivisionID 
	LEFT JOIN 
		CompanyInfo CI 
	ON 
		DM.numCompanyId=CI.numCompanyId 
	LEFT JOIN 
		UserMaster UM  
	ON 
		UM.numUserDetailID=AC.numContactID
	WHERE 
		PM.numDomainID=@numDomainId
		AND PM.numProId=@numProId

	INSERT INTO 
		@TEMPContacts
	SELECT 
		AC.numContactId
		,CONCAT(ISNULL(vcFirstName,''),' ',ISNULL(vcLastName,'')) AS vcContact
		,CI.vcCompanyName AS vcCompanyname
		,CASE WHEN UM.numUserDetailId IS NULL THEN 2 ELSE 1 END AS tinUserType
		,0 AS ActivityID
		,AC.vcEmail as Email
		,'' vcStatus
		,dbo.GetListIemName(AC.vcPosition) vcPosition
		,AC.vcEmail
		,AC.numPhone
		,AC.numPhoneExtension
		,1
		,CONCAT(ISNULL(vcFirstName,''),' ',ISNULL(vcLastName,''))
		,1
		,0
	FROM
		ProjectsMaster PM
	INNER JOIN 
		AdditionalContactsInformation AC 
	ON 
		PM.numCustPrjMgr=AC.numContactID  
	LEFT JOIN
		DivisionMaster DM 
	ON 
		DM.numDivisionID=AC.numDivisionID 
	LEFT JOIN 
		CompanyInfo CI 
	ON 
		DM.numCompanyId=CI.numCompanyId 
	LEFT JOIN 
		UserMaster UM  
	ON 
		UM.numUserDetailID=AC.numContactID
	WHERE 
		PM.numDomainID=@numDomainId
		AND PM.numProId=@numProId
		AND AC.numContactId NOT IN (SELECT TC.numContactID FROM @TEMPContacts TC)

	INSERT INTO 
		@TEMPContacts
	SELECT DISTINCT
		AC.numContactId
		,CONCAT(ISNULL(vcFirstName,''),' ',ISNULL(vcLastName,'')) AS vcContact
		,CI.vcCompanyName AS vcCompanyname
		,CASE WHEN UM.numUserDetailId IS NULL THEN 2 ELSE 1 END AS tinUserType
		,0 AS ActivityID
		,AC.vcEmail as Email
		,'' vcStatus
		,dbo.GetListIemName(AC.vcPosition) vcPosition
		,AC.vcEmail
		,AC.numPhone
		,AC.numPhoneExtension
		,1
		,CONCAT(ISNULL(vcFirstName,''),' ',ISNULL(vcLastName,''))
		,1
		,0
	FROM
		ProjectsMaster PM
	INNER JOIN
		StagePercentageDetailsTask SPDT
	ON
		PM.numProId = SPDT.numProjectId
	INNER JOIN 
		AdditionalContactsInformation AC 
	ON 
		SPDT.numAssignTo=AC.numContactID  
	LEFT JOIN
		DivisionMaster DM 
	ON 
		DM.numDivisionID=AC.numDivisionID 
	LEFT JOIN 
		CompanyInfo CI 
	ON 
		DM.numCompanyId=CI.numCompanyId 
	LEFT JOIN 
		UserMaster UM  
	ON 
		UM.numUserDetailID=AC.numContactID
	WHERE 
		PM.numDomainID=@numDomainId
		AND PM.numProId=@numProId
		AND AC.numContactId NOT IN (SELECT TC.numContactID FROM @TEMPContacts TC)

	INSERT INTO 
		@TEMPContacts
	SELECT DISTINCT
		PC.numContactID
		,CONCAT(ISNULL(vcFirstName,''),' ',ISNULL(vcLastName,'')) AS vcContact
		,CI.vcCompanyName AS vcCompanyname
		,CASE WHEN UM.numUserDetailId IS NULL THEN 2 ELSE 1 END AS tinUserType
		,0 AS ActivityID
		,AC.vcEmail as Email
		,'' vcStatus
		,dbo.GetListIemName(AC.vcPosition) vcPosition
		,AC.vcEmail
		,AC.numPhone
		,AC.numPhoneExtension
		,0
		,CONCAT(ISNULL(vcFirstName,''),' ',ISNULL(vcLastName,''))
		,1
		,0
	FROM 
		ProjectsMaster PM
	INNER JOIN
		ProjectsContacts PC 
	ON
		PM.numProId = PC.numProId
	INNER JOIN 
		AdditionalContactsInformation AC 
	ON 
		PC.numContactID=AC.numContactID  
	LEFT JOIN
		DivisionMaster DM 
	ON 
		DM.numDivisionID=AC.numDivisionID 
	LEFT JOIN 
		CompanyInfo CI 
	ON 
		DM.numCompanyId=CI.numCompanyId 
	LEFT JOIN 
		UserMaster UM  
	ON 
		UM.numUserDetailID=AC.numContactID
	WHERE 
		PM.numDomainID=@numDomainId
		AND PM.numProId=@numProId
		AND AC.numContactId NOT IN (SELECT TC.numContactID FROM @TEMPContacts TC)

	SELECT * FROM @TEMPContacts ORDER BY vcContact
END
GO