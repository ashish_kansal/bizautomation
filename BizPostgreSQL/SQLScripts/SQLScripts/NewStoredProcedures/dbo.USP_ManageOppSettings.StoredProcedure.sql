SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ManageOppSettings')
DROP PROCEDURE USP_ManageOppSettings
GO
CREATE PROCEDURE [dbo].[USP_ManageOppSettings] 
(                                                                          
  @numOppID as numeric,                                                                                                                                  
  @numDomainId numeric(9),                                                                                                                                                                                   
  --@bitDiscountType as bit,
  --@fltDiscount  as float,
  @bitBillingTerms as bit,
  @intBillingDays as integer,
  @bitInterestType as bit,
  @fltInterest as float,
  @tintTaxOperator AS TINYINT,
  --@numDiscountAcntType AS NUMERIC(9),
  @fltExchangeRate AS FLOAT,
  @dtItemReceivedDate DATETIME,
  @numUserCntId numeric(9),
  @bitRecur BIT,
  @dtStartDate DATE,
  @dtEndDate DATE,
  @numFrequency SMALLINT,
  @vcFrequency VARCHAR(20),
  @numRecConfigID NUMERIC(18,0) OUT,
  @bitDisable NUMERIC(18,0)
) 
AS 
BEGIN

BEGIN TRY
	--SELECT numOppID,numDomainId,bitDiscountType,fltDiscount,bitBillingTerms,intBillingDays,bitInterestType,fltInterest,tintTaxOperator,numDiscountAcntType,fltExchangeRate FROM dbo.OpportunityMaster
	
	UPDATE dbo.OpportunityMaster 
	SET 
--		bitDiscountType = @bitDiscountType,
--	    fltDiscount = @fltDiscount,
	    bitBillingTerms = @bitBillingTerms,
	    intBillingDays = @intBillingDays,
	    --bitInterestType = @bitInterestType,
	    --fltInterest = @fltInterest,
	    tintTaxOperator = @tintTaxOperator,
	    --numDiscountAcntType = @numDiscountAcntType,
	    fltExchangeRate = @fltExchangeRate,
	    dtItemReceivedDate = @dtItemReceivedDate 
	WHERE numOppID = @numOppID 
	AND numDomainId = @numDomainID
	
	
	   UPDATE OBD SET monDealAmount= [dbo].[GetDealAmount](@numOppId ,getutcdate(),OBD.numOppBizDocsId ) 
FROM dbo.OpportunityBizDocs OBD WHERE numOppId=@numOppID

	-- 1 if user has selected for order recurrence
	IF ISNULL(@bitRecur,0) = 1
	BEGIN
		-- First Check if receurrece is already created for any invoice in sales order because user should not 
		-- be able to create recurrence on both order and invoice
		IF (SELECT COUNT(*) FROM RecurrenceConfiguration WHERE numOppID = @numOppId AND numType=2) > 0 
		BEGIN
			RAISERROR ('RECURRENCE_ALREADY_CREATED_FOR_ORDER',16,1)
		END	
		ELSE
		BEGIN
			EXEC USP_RecurrenceConfiguration_Insert
			@numDomainID = @numDomainId,
			@numUserCntID = @numUserCntId,
			@numOppID = @numOppID,
			@numOppBizDocID = NULL,
			@dtStartDate = @dtStartDate,
			@dtEndDate =@dtEndDate,
			@numType = 1,
			@vcType = 'Sales Order',
			@vcFrequency = @vcFrequency,
			@numFrequency = @numFrequency

			SET @numRecConfigID = SCOPE_IDENTITY()

			UPDATE OpportunityMaster SET vcRecurrenceType = 'Sales Order' WHERE numOppId = @numOppID
		END
	END
	ELSE IF ISNULL(@numRecConfigID,0) > 0 AND ISNULL(@bitDisable,0)= 1
	BEGIN
		UPDATE
			RecurrenceConfiguration
		SET
			bitDisabled = 1,
			numDisabledBy = @numUserCntId,
			dtDisabledDate = GETDATE()
		WHERE	
			numRecConfigID = @numRecConfigID
	END 
END TRY
BEGIN CATCH
    DECLARE @ErrorMessage NVARCHAR(4000);
    DECLARE @ErrorSeverity INT;
    DECLARE @ErrorState INT;

    SELECT 
        @ErrorMessage = ERROR_MESSAGE(),
        @ErrorSeverity = ERROR_SEVERITY(),
        @ErrorState = ERROR_STATE();

    -- Use RAISERROR inside the CATCH block to return error
    -- information about the original error that caused
    -- execution to jump to the CATCH block.
    RAISERROR (@ErrorMessage, -- Message text.
               @ErrorSeverity, -- Severity.
               @ErrorState -- State.
               );
END CATCH; 
END  
                     