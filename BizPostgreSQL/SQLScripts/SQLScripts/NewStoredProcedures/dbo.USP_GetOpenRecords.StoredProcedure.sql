GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetOpenRecords')
DROP PROCEDURE USP_GetOpenRecords
GO
CREATE PROCEDURE USP_GetOpenRecords
	@DomainID NUMERIC,
	@numDivisionID NUMERIC,
    @byteMode AS TINYINT = 0
AS 
    BEGIN
    
    --sales Opp
        IF @byteMode = 1
            BEGIN      
                SELECT  numOppId,
                        vcPOppName
                FROM    OpportunityMaster
                WHERE   tintActive = 1
                        AND tintShipped = 0
                        AND [tintOppType] = 1
                        AND [tintOppStatus]=0
                        AND numDomainId = @domainId
                        AND numDivisionID = @numDivisionID
                        
                ORDER BY numOppId DESC
            END
            --Purchase Opp 
        IF @byteMode = 2 
            BEGIN      
                SELECT  numOppId,
                        vcPOppName
                FROM    OpportunityMaster
                WHERE   tintActive = 1
                        AND tintShipped = 0
                        AND [tintOppType] = 2
                        AND [tintOppStatus]=0
                        AND numDomainId = @domainId
                        AND numDivisionID = @numDivisionID
                ORDER BY numOppId DESC     
            END 
            --sales Order
        IF @byteMode = 3 
            BEGIN      
                SELECT  numOppId,
                        vcPOppName
                FROM    OpportunityMaster
                WHERE   tintActive = 1
                        AND tintShipped = 0
                        AND [tintOppType] = 1
                        AND [tintOppStatus]=1
                        AND numDomainId = @domainId
                        AND numDivisionID = @numDivisionID
                        
                ORDER BY numOppId DESC     
            END 
            --Purchase Order
        IF @byteMode = 4 
            BEGIN      
                SELECT  numOppId,
                        vcPOppName
                FROM    OpportunityMaster
                WHERE   tintActive = 1
                        AND tintShipped = 0
                        AND [tintOppType] = 2
                        AND [tintOppStatus]=1
                        AND numDomainId = @domainId
                        AND numDivisionID = @numDivisionID
                ORDER BY numOppId DESC     
            END 
    END