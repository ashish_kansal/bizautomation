GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_OpportunityMaster_GetFieldValue')
DROP PROCEDURE USP_OpportunityMaster_GetFieldValue
GO
CREATE PROCEDURE [dbo].[USP_OpportunityMaster_GetFieldValue]                                   
(                                                   
	@numDomainID NUMERIC(18,0),    
	@numOppID NUMERIC(18,0),
	@vcFieldName VARCHAR(100)                           
)                                    
as
BEGIN
	IF @vcFieldName = 'numDivisionID'
	BEGIN
		SELECT numDivisionID FROM OpportunityMaster WHERE numDomainId=@numDomainID AND numOppId=@numOppID
	END
	ELSE IF @vcFieldName = 'vcPOppName'
	BEGIN
		SELECT ISNULL(vcPOppName,'') FROM OpportunityMaster WHERE numDomainId=@numDomainID AND numOppId=@numOppID
	END
	ELSE
	BEGIN
		RAISERROR('FIELD_NOT_FOUND',16,1)
	END
END
GO