GO
/****** Object:  StoredProcedure [dbo].[USP_ManageItemsAndKits]    Script Date: 02/15/2010 21:44:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created By Anoop Jayaraj                                                          
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ItemCategory_MassUpdate')
DROP PROCEDURE USP_ItemCategory_MassUpdate
GO
CREATE PROCEDURE [dbo].[USP_ItemCategory_MassUpdate]     
	@numDomainID NUMERIC(9)
	,@tintType TINYINT
	,@numCategoryProfileID NUMERIC(18,0)
	,@vcItemCodes VARCHAR(MAX)
	,@vcCategories VARCHAR(MAX)
AS
BEGIN
	IF ISNULL(@vcCategories,'') <> '' AND ISNULL(@vcItemCodes,'') <> ''
	BEGIN
		IF @tintType = 1 -- ADD
		BEGIN
			DECLARE @TEMP TABLE
			(
				numItemCode NUMERIC(18,0)
				,numCategoryID NUMERIC(18,0)
			)
			INSERT INTO
				@TEMP
			SELECT
				t1.ID
				,t2.ID
			FROM
				(SELECT ISNULL(ID,0) AS ID FROM dbo.SplitIDs(@vcItemCodes,',')) t1
			CROSS APPLY
				(SELECT ISNULL(ID,0) AS ID FROM dbo.SplitIDs(@vcCategories,',')) t2

			INSERT INTO ItemCategory
			(
				numCategoryID
				,numItemID
			)
			SELECT
				t1.numCategoryID
				,t1.numItemCode
			FROM	
				@TEMP t1
			LEFT JOIN
				ItemCategory
			ON
				t1.numCategoryID = ItemCategory.numCategoryID
				AND t1.numItemCode = ItemCategory.numItemID
			WHERE
				t1.numCategoryID IN (SELECT ISNULL(numCategoryID,0) FROM Category WHERE numDomainID=@numDomainID AND numCategoryProfileID=@numCategoryProfileID)
				AND ItemCategory.numCategoryID IS NULL
		END
		ELSE IF @tintType = 2 -- REMOVE
		BEGIN
			DELETE 
				IC
			FROM 
				dbo.ItemCategory IC
			INNER JOIN 
				Category 
			ON 
				IC.numCategoryID = Category.numCategoryID
			WHERE 
				numItemID IN (SELECT ISNULL(ID,0) from dbo.SplitIDs(@vcItemCodes,','))
				AND IC.numCategoryID IN (SELECT ISNULL(ID,0) from dbo.SplitIDs(@vcCategories,','))		
				AND numCategoryProfileID = @numCategoryProfileID
		END
	END
END