GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_SalesConversionReport' ) 
    DROP PROCEDURE USP_SalesConversionReport
GO
CREATE PROCEDURE USP_SalesConversionReport
    @numDomainId NUMERIC,
    @dtStartDate DATETIME,
    @dtEndDate DATETIME,
    @RecordType TINYINT,  --1: Employee is the Assignee  2: Employee is the Owner
    @numContactID NUMERIC(9),
    @numUserCntID NUMERIC(9),
	@tintType tinyint --1: Full Report 2:Company Detail 3:Sales Detail
AS 
BEGIN
   
    SELECT A.numContactID,A.vcFirstName+' '+A.vcLastName as EmployeeName,UM.numDomainID INTO #tempUser         
	from UserMaster UM join AdditionalContactsInformation A on UM.numUserDetailId=A.numContactID          
	where UM.numDomainID=@numDomainId and UM.numDomainID=A.numDomainID AND [bitActivateFlag]=1
	AND 1=CASE WHEN @numContactID=0 THEN 1 ELSE CASE WHEN A.numContactID=@numContactID THEN 1 ELSE 0 END END
	AND A.numTeam in(select F.numTeam from ForReportsByTeam F                   
 where F.numuserCntId=@numUserCntID and F.numDomainid=@numDomainId AND f.tintType=53)
 
 
    CREATE TABLE #tempSales(numContactID NUMERIC(9),EmployeeName VARCHAR(100),numDivisionID NUMERIC(9),vcDivisionName VARCHAR(100),vcCompanyName VARCHAR(100),
		DVCreatedDate DATETIME,tintCRMType TINYINT,LeadPromDate DATETIME,ProsPromDate DATETIME,
		TotalSalesIncome DECIMAL(20,5),TotalSalesOrder INT,FirSalesDate DATETIME,FirSalesAmount DECIMAL(20,5),DaysTook INT)
	
	INSERT INTO #tempSales (numContactID,EmployeeName,numDivisionID,vcDivisionName,vcCompanyName,DVCreatedDate,tintCRMType,LeadPromDate,ProsPromDate)
    SELECT tU.numContactID,tU.EmployeeName,DV.numDivisionID,DV.vcDivisionName,CI.vcCompanyName,DV.bintCreatedDate,tintCRMType,bintLeadProm,bintProsProm
    FROM DivisionMaster DV JOIN #tempUser tU ON DV.numDomainID=tU.numDomainID
    JOIN CompanyInfo CI ON DV.numCompanyId=CI.numCompanyId
		WHERE (1= CASE WHEN @RecordType=1 THEN CASE WHEN DV.numAssignedTo=tU.numContactID THEN 1 ELSE 0 END 
				      WHEN @RecordType=2 THEN CASE WHEN DV.numRecOwner=tU.numContactID THEN 1 ELSE 0 END END) 
				    AND (DV.bintCreatedDate >= @dtStartDate AND DV.bintCreatedDate <= @dtEndDate)  

	IF @tintType=3
	BEGIN
 		SELECT ts1.EmployeeName,ts1.vcCompanyName,ts1.numDivisionID,ts1.tintCRMType,OM.numOppId,OM.vcPOppName,OM.tintopptype,
                        CASE ISNULL(OM.tintshipped, 0)
                          WHEN 0 THEN 'Open'
                          WHEN 1 THEN 'Completed'
                        END vcDealStatus,dbo.GetListIemName(OM.numStatus) vcStatus,
                        OM.monDealAmount,dbo.FormatedDateFromDate(OM.bintCreatedDate,OM.numDomainId) bintCreatedDate 
             FROM   #tempSales tS1 JOIN OpportunityMaster OM ON OM.numDivisionId=tS1.numDivisionID 
						WHERE OM.tintOppType=1 AND (OM.bintCreatedDate >= @dtStartDate AND OM.bintCreatedDate <= @dtEndDate)
	END	
	ELSE IF @tintType=2 or @tintType=1
	BEGIN
    UPDATE tS SET TotalSalesIncome=OMS.TotalSalesIncome,TotalSalesOrder=OMS.TotalSalesOrder
		FROM #tempSales tS INNER JOIN 
		   (SELECT OM.numDivisionId,SUM(ISNULL(OM.monDealAmount,0)) AS TotalSalesIncome,COUNT(OM.numOppId) AS TotalSalesOrder from #tempSales tS1 JOIN OpportunityMaster OM ON OM.numDivisionId=tS1.numDivisionID 
			WHERE OM.tintOppType=1 AND (OM.bintCreatedDate >= @dtStartDate AND OM.bintCreatedDate <= @dtEndDate)  
			GROUP BY OM.numDivisionId) OMS ON OMS.numDivisionId=tS.numDivisionID 
	
	
	 UPDATE tS SET FirSalesAmount=OMS.FirSalesAmount,FirSalesDate=OMS.FirSalesDate,DaysTook=DATEDIFF(dd,tS.DVCreatedDate, OMS.FirSalesDate)
		FROM #tempSales tS INNER JOIN 
		   (SELECT TOP 1 OM.numDivisionId,ISNULL(OM.monDealAmount,0) AS FirSalesAmount,OM.bintCreatedDate AS FirSalesDate,
		   ROW_NUMBER() over (partition by OM.numDivisionId order by OM.bintCreatedDate) as RowNumber
			from #tempSales tS1 JOIN OpportunityMaster OM ON OM.numDivisionId=tS1.numDivisionID 
			WHERE OM.tintOppType=1 AND (OM.bintCreatedDate >= @dtStartDate AND OM.bintCreatedDate <= @dtEndDate) 
			) OMS ON OMS.numDivisionId=tS.numDivisionID AND OMS.ROWNUMBER=1 


		IF @tintType=2
		Begin
			SELECT * FROM #tempSales	
	    END		
		ELSE IF @tintType=1
		BEGIN
			SELECT 	numContactID,EmployeeName,TotalLP,TotalLPConverted,TotalSales,TotalSO,AvgFirstSales,TotalDaysTook,
			CASE WHEN TotalLPConverted > 0 THEN TotalDaysTook/TotalLPConverted ELSE 0 END AS AvgDayConvert,
			Cast(CONVERT(DECIMAL(10,2),(TotalLPConverted * 100) / CAST(TotalLP AS float)) as NUMERIC(9,2))  AS  TotalLPConvertedPer
			INTO #tempReport
			FROM (SELECT numContactID,EmployeeName,COUNT(*) AS TotalLP,
			SUM(CASE WHEN (LeadPromDate >= @dtStartDate AND LeadPromDate <= @dtEndDate) 
				OR (ProsPromDate >= @dtStartDate AND ProsPromDate <= @dtEndDate) THEN 1 ELSE 0 END) TotalLPConverted,
			SUM(ISNULL(TotalSalesIncome,0)) AS TotalSales,
			SUM(ISNULL(TotalSalesOrder,0)) AS TotalSO,
			CASE WHEN SUM(ISNULL(TotalSalesOrder,0))=0 THEN 0 ELSE SUM(ISNULL(FirSalesAmount,0)) /SUM(ISNULL(TotalSalesOrder,0)) END AS AvgFirstSales,
			SUM(ISNULL(DaysTook,0)) TotalDaysTook
			FROM #tempSales GROUP BY numContactID,EmployeeName) temp
			
			
			SELECT * FROM #tempReport
			
			
			SELECT CAST(SUM(TotalLPConvertedPer)/COUNT(*) as NUMERIC(9,2)) TotalLPConvertedPer,SUM(TotalLPConverted)/COUNT(*) TotalLPConverted,
					SUM(AvgFirstSales) AvgFirstSales,SUM(TotalSales) AS TotalSales FROM #tempReport
			
			
			DROP TABLE #tempReport
		END	
	END	
			
	DROP TABLE #tempSales

	drop table #tempUser 
	
----    SELECT tU.EmployeeName,COUNT(*) AS TotalLP,SUM(CASE WHEN tintCRMType=2 THEN 1 ELSE 0 END) AS TotalConversion 
----    ,SUM(ISNULL(OM.monDealAmount,0)) AS TotalSalesIncome,COUNT(OM.numOppId) TotalSalesOrder,SUM(ISNULL(OMTOP1.monDealAmount,0)),CASE WHEN COUNT(OM.numOppId)=0 THEN 0 ELSE SUM(ISNULL(OMTOP1.monDealAmount,0)) / COUNT(OM.numOppId) END AS AvgIncome
----    FROM DivisionMaster DV JOIN #tempUser tU ON DV.numDomainID=tU.numDomainID
----    LEFT JOIN OpportunityMaster OM ON OM.numDivisionId=DV.numDivisionID AND OM.tintOppType=1
----    Cross JOIN (SELECT TOP 1 numDivisionId,monDealAmount FROM OpportunityMaster WHERE numDomainId=@numDomainId AND numDivisionId=DV.numDivisionID AND tintOppType=1) OMTOP1 
----		WHERE 1= CASE WHEN @RecordType=1 THEN CASE WHEN DV.numAssignedTo=tU.numContactID THEN 1 ELSE 0 END 
----				      WHEN @RecordType=2 THEN CASE WHEN DV.numRecOwner=tU.numContactID THEN 1 ELSE 0 END END 
----		GROUP BY tU.numContactID,tu.EmployeeName
----		
END


