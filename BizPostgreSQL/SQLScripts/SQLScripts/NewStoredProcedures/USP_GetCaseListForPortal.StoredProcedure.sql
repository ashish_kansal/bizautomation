/****** Object:  StoredProcedure [dbo].[USP_GetCaseListForPortal]    Script Date: 15/10/2013 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_GetCaseListForPortal' ) 
    DROP PROCEDURE USP_GetCaseListForPortal
GO
CREATE PROCEDURE [dbo].[USP_GetCaseListForPortal]
    @numUserCntId NUMERIC(9) = 0,
    @tintSortOrder NUMERIC = 0,
    @numDomainID NUMERIC(9) = 0,
    @tintUserRightType TINYINT,
    @SortChar CHAR(1) = '0',
    @CustName VARCHAR(100) = '',
    @FirstName VARCHAR(100) = '',
    @LastName VARCHAR(100) = '',
    @CurrentPage INT,
    @PageSize INT,
    @TotRecs INT OUTPUT,
    @columnName AS VARCHAR(50),
    @columnSortOrder AS VARCHAR(10),
    @numDivisionID AS NUMERIC(9) = 0,
    @bitPartner AS BIT = 0,
    @vcRegularSearchCriteria VARCHAR(1000) = '',
    @vcCustomSearchCriteria VARCHAR(1000) = '',
    @ClientTimeZoneOffset AS INT,
    @numStatus AS BIGINT = 0,
	@tintMode AS TINYINT = 0
AS 
	DECLARE @firstRec AS INTEGER                                                            
    DECLARE @lastRec AS INTEGER                                                            
    SET @firstRec = ( @CurrentPage - 1 ) * @PageSize                                                            
    SET @lastRec = ( @CurrentPage * @PageSize + 1 )
	DECLARE @strSql AS NVARCHAR(MAX)

IF @tintMode = 0
BEGIN
    DECLARE @column AS VARCHAR(50)         
    SET @column = @columnName                
    DECLARE @join AS VARCHAR(400)          
    SET @join = ''         
    DECLARE @lookbckTable AS VARCHAR(50)              
    SET @lookbckTable = ''          
          
    IF @column LIKE 'CFW.Cust%' 
        BEGIN        
        
            SET @column = 'CFW.Fld_Value'        
            SET @join = ' left Join CFW_FLD_Values_Case CFW on CFW.RecId=cs.numCaseId  and CFW.fld_id= ' + REPLACE(@columnName, 'CFW.Cust', '')                                                  
        
          
        END                                 
    IF @Column LIKE 'DCust%' 
        BEGIN        
        
            SET @column = 'LstCF.vcData'                                                  
            SET @join = ' left Join CFW_FLD_Values_Case CFW on CFW.RecId=cs.numCaseId and CFW.fld_id= ' + REPLACE(@columnName, 'DCust', '')                                                                                           
            SET @join = @join + ' left Join ListDetails LstCF on LstCF.numListItemID=CFW.Fld_Value'        
        
        END         

    DECLARE @strShareRedordWith AS VARCHAR(300) ;
    SET @strShareRedordWith = ' Cs.numCaseID in (select SR.numRecordID FROM ShareRecord SR WHERE SR.numDomainID=' + CONVERT(VARCHAR(15), @numDomainID)
							+ ' AND SR.numModuleID = 7 AND SR.numAssignedTo = ' + CONVERT(VARCHAR(15), @numUserCntId) + ') '
                                                              
                                                                
    SET @strSql = 'WITH tblcases AS (SELECT  '                                               
    IF @tintSortOrder = 3
        OR @tintSortOrder = 4 
        SET @strSql = @strSql + ' TOP 20 '                  
    SET @strSql = @strSql + ' ROW_NUMBER() OVER (ORDER BY ' + CASE WHEN PATINDEX('%TEXT%', @column) > 0 
																   THEN ' CONVERT(VARCHAR(MAX),' + @column + ')'
																   ELSE @column
													          END + ' ' + @columnSortOrder + ') AS RowNumber,Cs.numCaseID,
							  COUNT(*) OVER(PARTITION BY NULL) AS TotalRowCount                                                                             
							  FROM                
							  AdditionalContactsInformation ADC                                                             
							  JOIN Cases Cs ON ADC.numContactId = Cs.numContactId                                                             
							  JOIN DivisionMaster Div ON ADC.numDivisionId = Div.numDivisionID '                              
							  
    IF @bitPartner = 1 
        SET @strSql = @strSql
            + ' left join CaseContacts CCont on CCont.numCaseID=Cs.numCaseID and CCont.bitPartner=1 and CCont.numContactId=' + CONVERT(VARCHAR(15), @numUserCntID) + ''                              
                                                               
    SET @strSql = @strSql
        + ' JOIN CompanyInfo CMP ON Div.numCompanyID = CMP.numCompanyId                                                             
			LEFT JOIN listdetails lst ON lst.numListItemID = cs.numPriority                       
			LEFT JOIN listdetails lst1 ON lst1.numListItemID = cs.numStatus                                                           
			LEFT JOIN AdditionalContactsInformation ADC1 ON Cs.numAssignedTo = ADC1.numContactId ' + @join + '                                                            
			WHERE cs.numDomainID=' + CONVERT(VARCHAR(15), @numDomainID) + ''
 
    IF @numStatus <> 0 
        BEGIN
            SET @strSql = @strSql + ' AND cs.numStatus = ' + CONVERT(VARCHAR(10), @numStatus)                                      	
        END
    ELSE 
        BEGIN
            SET @strSql = @strSql + ' AND cs.numStatus <> 136 ' 
        END
 
 
    IF @FirstName <> '' 
        SET @strSql = @strSql + 'and ADC.vcFirstName  like ''' + @FirstName + '%'''                                                                 
 
    IF @LastName <> '' 
        SET @strSql = @strSql + 'and ADC.vcLastName like ''' + @LastName + '%'''                                      
 
    IF @CustName <> '' 
        SET @strSql = @strSql + ' and CMP.vcCompanyName like ''' + @CustName + '%'''                                                            
                                                                                                                        
    IF @numDivisionID <> 0 
        SET @strSql = @strSql + ' And div.numDivisionID = ' + CONVERT(VARCHAR(15), @numDivisionID)                                                      
        
    IF @SortChar <> '0' 
        SET @strSql = @strSql + ' And (ADC.vcFirstName like ''' + @SortChar + '%'''                                                              
        
    IF @SortChar <> '0' 
        SET @strSql = @strSql + ' or ADC.vcLastName like ''' + @SortChar + '%'')'                                                             
                                                             
    IF @tintUserRightType = 1 
        SET @strSql = @strSql + ' AND (Cs.numRecOwner = ' + CONVERT(VARCHAR(15), @numUserCntId) 
							  + ' or Cs.numAssignedTo = ' + CONVERT(VARCHAR(15), @numUserCntID)
							  + CASE WHEN @bitPartner = 1	
									 THEN ' or (CCont.bitPartner = 1 AND CCont.numContactId = '+ CONVERT(VARCHAR(15), @numUserCntID) + ')'
									 ELSE ''
								END + ' OR ' + @strShareRedordWith + ')'                
    ELSE 
        IF @tintUserRightType = 2 
            SET @strSql = @strSql
                + ' AND (Div.numTerID IN (select numTerritoryID from  UserTerritory where numUserCntID = ' + CONVERT(VARCHAR(15), @numUserCntId)
                + ' ) OR Div.numTerID = 0 or Cs.numAssignedTo = ' + CONVERT(VARCHAR(15), @numUserCntID) + ' or ' + @strShareRedordWith + ')'                   
                  
    IF @numDivisionID = 0 
        BEGIN                                                    
            IF @tintSortOrder = 0 
                SET @strSql = @strSql + ' AND (Cs.numRecOwner = ' + CONVERT(VARCHAR(15), @numUserCntId)
                    + ' or Cs.numAssignedTo = ' + CONVERT(VARCHAR(15), @numUserCntId) + ' or ' + @strShareRedordWith + ')'                                         
        END                                                            
        
    IF @tintSortOrder = 2 
        SET @strSql = @strSql + 'And cs.numStatus <> 136  ' + ' AND cs.bintCreatedDate> ''' + CONVERT(VARCHAR(20), DATEADD(day, -7, GETUTCDATE()))
            + ''''                                 
    ELSE 
        IF @tintSortOrder = 3 
            SET @strSql = @strSql + 'And cs.numStatus <> 136  ' + ' and cs.numCreatedBy = ' + CONVERT(VARCHAR(15), @numUserCntId)       
        ELSE 
            IF @tintSortOrder = 4 
                SET @strSql = @strSql + 'And cs.numStatus <> 136  ' + ' and cs.numModifiedBy = ' + CONVERT(VARCHAR(15), @numUserCntId)                                                            
            ELSE 
                IF @tintSortOrder = 5 
                    SET @strSql = @strSql + 'And cs.numStatus = 136  ' + ' and cs.numModifiedBy = ' + CONVERT(VARCHAR(15), @numUserCntId)                                                            
 
    IF @vcRegularSearchCriteria <> '' 
        SET @strSql = @strSql + ' and ' + @vcRegularSearchCriteria 
        
    IF @vcCustomSearchCriteria <> '' 
        SET @strSql = @strSql + ' and cs.numCaseId in (select distinct CFW.RecId from CFW_FLD_Values_Case CFW where ' + @vcCustomSearchCriteria + ')'
    
    SET @strSql = @strSql + ')'             
                                                             
    DECLARE @tintOrder AS TINYINT                                                  
    DECLARE @vcFieldName AS VARCHAR(50)                                                  
    DECLARE @vcListItemType AS VARCHAR(3)                                             
    DECLARE @vcListItemType1 AS VARCHAR(1)                                                 
    DECLARE @vcAssociatedControlType VARCHAR(20)                                                  
    DECLARE @numListID AS NUMERIC(9)                                                  
    DECLARE @vcDbColumnName VARCHAR(20)                      
    DECLARE @WhereCondition VARCHAR(2000)                       
    DECLARE @vcLookBackTableName VARCHAR(2000)                
    DECLARE @bitCustom AS BIT          
    DECLARE @numFieldId AS NUMERIC  
    DECLARE @bitAllowSorting AS CHAR(1)   
    DECLARE @bitAllowEdit AS CHAR(1)                 
    DECLARE @vcColumnName AS VARCHAR(500)                  
               
    SET @tintOrder = 0                                                  
    SET @WhereCondition = ''                 
                   
    DECLARE @Nocolumns AS TINYINT                
    SET @Nocolumns = 0                
 
    SELECT  @Nocolumns = ISNULL(SUM(TotalRow), 0)
    FROM    ( SELECT    COUNT(*) TotalRow
              FROM      View_DynamicColumns
              WHERE     numFormId = 80
                        AND numUserCntID = @numUserCntID
                        AND numDomainID = @numDomainID
                        --AND tintPageType = 1
              UNION
              SELECT    COUNT(*) TotalRow
              FROM      View_DynamicCustomColumns
              WHERE     numFormId = 80
                        AND numUserCntID = @numUserCntID
                        AND numDomainID = @numDomainID
                        --AND tintPageType = 1
            ) TotalRows

    CREATE TABLE #tempForm
        (
          tintOrder TINYINT,
          vcDbColumnName NVARCHAR(50),
          vcOrigDbColumnName NVARCHAR(50),
          vcFieldName NVARCHAR(50),
          vcAssociatedControlType NVARCHAR(50),
          vcListItemType VARCHAR(3),
          numListID NUMERIC(9),
          vcLookBackTableName VARCHAR(50),
          bitCustomField BIT,
          numFieldId NUMERIC,
          bitAllowSorting BIT,
          bitAllowEdit BIT,
          bitIsRequired BIT,
          bitIsEmail BIT,
          bitIsAlphaNumeric BIT,
          bitIsNumeric BIT,
          bitIsLengthValidation BIT,
          intMaxLength INT,
          intMinLength INT,
          bitFieldMessage BIT,
          vcFieldMessage VARCHAR(500),
          ListRelID NUMERIC(9),
          intColumnWidth INT,
          bitAllowFiltering BIT,
          vcFieldDataType CHAR(1)
        )

    SET @strSql = @strSql
        + ' select ADC.numContactId,DM.numDivisionID,isnull(DM.numTerID,0) numTerID,cs.numRecOwner,DM.tintCRMType,cs.numCaseID,RowNumber'                
  
    IF @Nocolumns = 0 
        BEGIN
            INSERT  INTO DycFormConfigurationDetails
                    (
                      numFormID,
                      numFieldID,
                      intColumnNum,
                      intRowNum,
                      numDomainID,
                      numUserCntID,
                      numRelCntType,
                      tintPageType,
                      bitCustom,
                      intColumnWidth
                    )
                    SELECT  80,
                            numFieldId,
                            0,
                            Row_number() OVER ( ORDER BY tintRow DESC ),
                            @numDomainID,
                            @numUserCntID,
                            NULL,
                            1,
                            0,
                            intColumnWidth
                    FROM    View_DynamicDefaultColumns
                    WHERE   numFormId = 80
                            AND bitDefault = 1
                            AND ISNULL(bitSettingField, 0) = 1
                            AND numDomainID = @numDomainID
                    ORDER BY tintOrder ASC      

        END
            
  

    INSERT  INTO #tempForm
            SELECT  tintRow + 1 AS tintOrder,
                    vcDbColumnName,
                    vcOrigDbColumnName,
                    ISNULL(vcCultureFieldName, vcFieldName),
                    vcAssociatedControlType,
                    vcListItemType,
                    numListID,
                    vcLookBackTableName,
                    bitCustom,
                    numFieldId,
                    bitAllowSorting,
                    bitInlineEdit,
                    bitIsRequired,
                    bitIsEmail,
                    bitIsAlphaNumeric,
                    bitIsNumeric,
                    bitIsLengthValidation,
                    intMaxLength,
                    intMinLength,
                    bitFieldMessage,
                    vcFieldMessage vcFieldMessage,
                    ListRelID,
                    intColumnWidth,
                    bitAllowFiltering,
                    vcFieldDataType
            FROM    View_DynamicColumns
            WHERE   numFormId = 80
                    AND numUserCntID = @numUserCntID
                    AND numDomainID = @numDomainID
                    AND tintPageType = 1
                    AND ISNULL(bitSettingField, 0) = 1
                    AND ISNULL(bitCustom, 0) = 0
            UNION
            SELECT  tintRow + 1 AS tintOrder,
                    vcDbColumnName,
                    vcDbColumnName,
                    vcFieldName,
                    vcAssociatedControlType,
                    '' AS vcListItemType,
                    numListID,
                    '',
                    bitCustom,
                    numFieldId,
                    bitAllowSorting,
                    bitAllowEdit,
                    bitIsRequired,
                    bitIsEmail,
                    bitIsAlphaNumeric,
                    bitIsNumeric,
                    bitIsLengthValidation,
                    intMaxLength,
                    intMinLength,
                    bitFieldMessage,
                    vcFieldMessage,
                    ListRelID,
                    intColumnWidth,
                    0,
                    ''
            FROM    View_DynamicCustomColumns
            WHERE   numFormId = 80
                    AND numUserCntID = @numUserCntID
                    AND numDomainID = @numDomainID
                    AND tintPageType = 1
                    AND ISNULL(bitCustom, 0) = 1
            ORDER BY tintOrder ASC  
           
--set @DefaultNocolumns=  @Nocolumns              
    DECLARE @ListRelID AS NUMERIC(9) 
       
    SELECT TOP 1
            @tintOrder = tintOrder + 1,
            @vcDbColumnName = vcDbColumnName,
            @vcFieldName = vcFieldName,
            @vcAssociatedControlType = vcAssociatedControlType,
            @vcListItemType = vcListItemType,
            @numListID = numListID,
            @vcLookBackTableName = vcLookBackTableName,
            @bitCustom = bitCustomField,
            @numFieldId = numFieldId,
            @bitAllowSorting = bitAllowSorting,
            @bitAllowEdit = bitAllowEdit,
            @ListRelID = ListRelID
    FROM    #tempForm
    ORDER BY tintOrder ASC            
                                        
    WHILE @tintOrder > 0                                
        BEGIN  
			--PRINT @tintOrder
			--PRINT @vcDbColumnName    
			--PRINT @vcAssociatedControlType                                           
            IF @bitCustom = 0 
                BEGIN        
                    DECLARE @Prefix AS VARCHAR(5)                
                    IF @vcLookBackTableName = 'AdditionalContactsInformation' 
                        SET @Prefix = 'ADC.'                
                    IF @vcLookBackTableName = 'DivisionMaster' 
                        SET @Prefix = 'DM.'                
                    IF @vcLookBackTableName = 'Cases' 
                        SET @PreFix = 'Cs.'      
--					IF @vcLookBackTableName = 'OpportunityMaster'
--						SET @PreFix = 'Opp.'  
    
                    SET @vcColumnName = @vcDbColumnName + '~' + CONVERT(VARCHAR(10), @numFieldId) + '~0'
					
					PRINT @vcColumnName 
										
                    IF @vcAssociatedControlType = 'SelectBox' 
                        BEGIN                                                  
                                     
                            IF @vcListItemType = 'LI' 
                                BEGIN                                                  
                                    SET @strSql = @strSql + ',L' + CONVERT(VARCHAR(3), @tintOrder) + '.vcData' + ' [' + @vcColumnName + ']'                                                  
                                    SET @WhereCondition = @WhereCondition 
                                        + ' left Join ListDetails L' + CONVERT(VARCHAR(3), @tintOrder) + ' on L' + CONVERT(VARCHAR(3), @tintOrder) + '.numListItemID=' + @vcDbColumnName                                                  
                                END                                                  
                            ELSE 
                                IF @vcListItemType = 'S' 
                                    BEGIN                                                  
                                        SET @strSql = @strSql + ',S' + CONVERT(VARCHAR(3), @tintOrder) + '.vcState' + ' [' + @vcColumnName + ']'                                                  
                                        SET @WhereCondition = @WhereCondition
                                            + ' left join State S' + CONVERT(VARCHAR(3), @tintOrder) + ' on S' + CONVERT(VARCHAR(3), @tintOrder) + '.numStateID=' + @vcDbColumnName                                                  
                                    END                                                  
                                ELSE 
                                    IF @vcListItemType = 'T' 
                                        BEGIN                                                  
                                            SET @strSql = @strSql + ',L'
                                                + CONVERT(VARCHAR(3), @tintOrder)
                                                + '.vcData' + ' ['
                                                + @vcColumnName + ']'                                                  
                                            SET @WhereCondition = @WhereCondition
                                                + ' left Join ListDetails L'
                                                + CONVERT(VARCHAR(3), @tintOrder)
                                                + ' on L'
                                                + CONVERT(VARCHAR(3), @tintOrder)
                                                + '.numListItemID='
                                                + @vcDbColumnName                                                  
                                        END                 
                                    ELSE 
                                        IF @vcListItemType = 'U' 
                                            BEGIN                 
                
                 
                                                SET @strSql = @strSql + ',dbo.fn_GetContactName(' + @Prefix + @vcDbColumnName + ') [' + @vcColumnName + ']'                                                  
                                            END                
                        END     
                              
                    ELSE 

                        IF @vcAssociatedControlType = 'DateField' 
                            BEGIN            
                                IF @vcDbColumnName = 'intTargetResolveDate' 
                                    BEGIN
                                        SET @strSql = @strSql
                                            + ',case when convert(varchar(11),'
                                            + @Prefix + @vcDbColumnName
                                            + ')= convert(varchar(11),getdate()) then ''<b><font color=red>Today</font></b>'''      
                                        SET @strSql = @strSql
                                            + 'when convert(varchar(11),'
                                            + @Prefix + @vcDbColumnName
                                            + ')= convert(varchar(11),dateadd(day,-1,getdate())) then ''<b><font color=purple>YesterDay</font></b>'''      
                                        SET @strSql = @strSql
                                            + 'when convert(varchar(11),'
                                            + @Prefix + @vcDbColumnName
                                            + ')= convert(varchar(11),dateadd(day,1,getdate())) then''<b><font color=orange>Tommorow</font></b>'' '      
                                        SET @strSql = @strSql
                                            + 'else dbo.FormatedDateFromDate('
                                            + @Prefix + @vcDbColumnName + ','
                                            + CONVERT(VARCHAR(10), @numDomainId)
                                            + ') end  [' + @vcColumnName + ']'               
                                    END
                                ELSE 
                                    BEGIN
                                        SET @strSql = @strSql
                                            + ',case when convert(varchar(11),DateAdd(minute, '
                                            + CONVERT(VARCHAR(15), -@ClientTimeZoneOffset)
                                            + ',' + @Prefix + @vcDbColumnName
                                            + ') )= convert(varchar(11),getdate()) then ''<b><font color=red>Today</font></b>'''      
                                        SET @strSql = @strSql
                                            + 'when convert(varchar(11),DateAdd(minute, '
                                            + CONVERT(VARCHAR(15), -@ClientTimeZoneOffset)
                                            + ',' + @Prefix + @vcDbColumnName
                                            + ') )= convert(varchar(11),dateadd(day,-1,getdate())) then ''<b><font color=purple>YesterDay</font></b>'''      
                                        SET @strSql = @strSql
                                            + 'when convert(varchar(11),DateAdd(minute, '
                                            + CONVERT(VARCHAR(15), -@ClientTimeZoneOffset)
                                            + ',' + @Prefix + @vcDbColumnName
                                            + ') )= convert(varchar(11),dateadd(day,1,getdate())) then''<b><font color=orange>Tommorow</font></b>'' ' 
                                        SET @strSql = @strSql
                                            + 'else dbo.FormatedDateFromDate(DateAdd(minute, '
                                            + CONVERT(VARCHAR(15), -@ClientTimeZoneOffset)
                                            + ',' + @Prefix + @vcDbColumnName
                                            + '),'
                                            + CONVERT(VARCHAR(10), @numDomainId)
                                            + ') end  [' + @vcColumnName + ']'
                                    END
   -- end      
--  else      
--     set @strSql=@strSql+',dbo.FormatedDateFromDate('+@vcDbColumnName+','++convert(varchar(10),@numDomainId)+') ['+ @vcFieldName+'~'+ @vcDbColumnName+']'            
                            END          
                        ELSE 
                            IF @vcAssociatedControlType = 'TextBox' 
                                BEGIN           
                                    SET @strSql = @strSql + ','
                                        + CASE WHEN @vcDbColumnName = 'numAge'
                                               THEN 'year(getutcdate())-year(bintDOB)'
                                               WHEN @vcDbColumnName = 'numDivisionID'
                                               THEN 'DM.numDivisionID'
                                               WHEN @vcDbColumnName = 'textSubject'
                                               THEN 'convert(varchar(max),textSubject)'
                                               WHEN @vcDbColumnName = 'textDesc'
                                               THEN 'convert(varchar(max),textDesc)'
                                               WHEN @vcDbColumnName = 'textInternalComments'
                                               THEN 'convert(varchar(max),textInternalComments)'
                                               ELSE @vcDbColumnName
                                          END + ' [' + @vcColumnName + ']'            
                                END  
                            ELSE 
                                IF @vcAssociatedControlType = 'TextArea' 
                                    BEGIN  
                                        SET @strSql = @strSql + ', '
                                            + @vcDbColumnName + ' ['
                                            + @vcColumnName + ']'            
                                    END 
                                ELSE 
                                    IF @vcAssociatedControlType = 'Popup'
                                        AND @vcDbColumnName = 'numShareWith' 
                                        BEGIN      
                                            SET @strSql = @strSql
                                                + ',(SELECT SUBSTRING(
													(SELECT '','' + A.vcFirstName+'' ''+A.vcLastName + CASE WHEN ISNULL(SR.numContactType,0)>0 THEN ''('' + dbo.fn_GetListItemName(SR.numContactType) + '')'' ELSE '''' END
													FROM ShareRecord SR 
													JOIN UserMaster UM ON UM.numUserDetailId = SR.numAssignedTo 
													JOIN AdditionalContactsInformation A ON UM.numUserDetailId = A.numContactID 
													WHERE SR.numDomainID = Cs.numDomainID 
													AND SR.numModuleID = 7 
													AND SR.numRecordID = Cs.numCaseID
													AND UM.numDomainID = Cs.numDomainID 
													AND UM.numDomainID = A.numDomainID FOR XML PATH('''')),2,200000)) ['
                                                + @vcColumnName + ']'                
                                        END     
                END                                            
            ELSE 
                BEGIN        
       
--   select @vcFieldName=FLd_label,@vcAssociatedControlType=fld_type,@vcDbColumnName='Cust'+Convert(varchar(10),Fld_Id)  from CFW_Fld_Master                                         
--   where  CFW_Fld_Master.Fld_Id = @numFieldId                                    
       
                    SET @vcColumnName = @vcDbColumnName + '~'
                        + CONVERT(VARCHAR(10), @numFieldId) + '~1'
       
        
                    IF @vcAssociatedControlType = 'TextBox'
                        OR @vcAssociatedControlType = 'TextArea' 
                        BEGIN         
           
                            SET @strSql = @strSql + ',CFW'
                                + CONVERT(VARCHAR(3), @tintOrder)
                                + '.Fld_Value  [' + @vcColumnName + ']'           
                            SET @WhereCondition = @WhereCondition
                                + ' left Join CFW_FLD_Values_Case CFW'
                                + CONVERT(VARCHAR(3), @tintOrder) + '         
    on CFW' + CONVERT(VARCHAR(3), @tintOrder) + '.Fld_Id='
                                + CONVERT(VARCHAR(10), @numFieldId)
                                + 'and CFW' + CONVERT(VARCHAR(3), @tintOrder)
                                + '.RecId=cs.numCaseId   '                                                 
                        END        
           
                    ELSE 
                        IF @vcAssociatedControlType = 'CheckBox' 
                            BEGIN          
             
                                SET @strSql = @strSql
                                    + ',case when isnull(CFW'
                                    + CONVERT(VARCHAR(3), @tintOrder)
                                    + '.Fld_Value,0)=0 then ''No'' when isnull(CFW'
                                    + CONVERT(VARCHAR(3), @tintOrder)
                                    + '.Fld_Value,0)=1 then ''Yes'' end   ['
                                    + @vcColumnName + ']'             
                                SET @WhereCondition = @WhereCondition
                                    + ' left Join CFW_FLD_Values_Case CFW'
                                    + CONVERT(VARCHAR(3), @tintOrder)
                                    + '           
    on CFW' + CONVERT(VARCHAR(3), @tintOrder) + '.Fld_Id='
                                    + CONVERT(VARCHAR(10), @numFieldId)
                                    + 'and CFW'
                                    + CONVERT(VARCHAR(3), @tintOrder)
                                    + '.RecId=cs.numCaseId   '                                                   
                            END          
                        ELSE 
                            IF @vcAssociatedControlType = 'DateField' 
                                BEGIN        
           
                                    SET @strSql = @strSql
                                        + ',dbo.FormatedDateFromDate(CFW'
                                        + CONVERT(VARCHAR(3), @tintOrder)
                                        + '.Fld_Value,'
                                        + CONVERT(VARCHAR(10), @numDomainId)
                                        + ')  [' + @vcColumnName + ']'           
                                    SET @WhereCondition = @WhereCondition
                                        + ' left Join CFW_FLD_Values_Case CFW'
                                        + CONVERT(VARCHAR(3), @tintOrder)
                                        + '         
    on CFW' + CONVERT(VARCHAR(3), @tintOrder) + '.Fld_Id='
                                        + CONVERT(VARCHAR(10), @numFieldId)
                                        + 'and CFW'
                                        + CONVERT(VARCHAR(3), @tintOrder)
                                        + '.RecId=cs.numCaseId   '                                                 
                                END        
                            ELSE 
                                IF @vcAssociatedControlType = 'SelectBox' 
                                    BEGIN          
                                        SET @vcDbColumnName = 'DCust'
                                            + CONVERT(VARCHAR(10), @numFieldId)        
                                        SET @strSql = @strSql + ',L'
                                            + CONVERT(VARCHAR(3), @tintOrder)
                                            + '.vcData' + ' [' + @vcColumnName
                                            + ']'                                                  
                                        SET @WhereCondition = @WhereCondition
                                            + ' left Join CFW_FLD_Values_Case CFW'
                                            + CONVERT(VARCHAR(3), @tintOrder)
                                            + '         
     on CFW' + CONVERT(VARCHAR(3), @tintOrder) + '.Fld_Id='
                                            + CONVERT(VARCHAR(10), @numFieldId)
                                            + 'and CFW'
                                            + CONVERT(VARCHAR(3), @tintOrder)
                                            + '.RecId=cs.numCaseId   '                                                 
                                        SET @WhereCondition = @WhereCondition
                                            + ' left Join ListDetails L'
                                            + CONVERT(VARCHAR(3), @tintOrder)
                                            + ' on L'
                                            + CONVERT(VARCHAR(3), @tintOrder)
                                            + '.numListItemID=CFW'
                                            + CONVERT(VARCHAR(3), @tintOrder)
                                            + '.Fld_Value'        
                                    END           
                END        
            
            PRINT @tintOrder - 1                                    
			---SELECT TOP 1 * FROM  #tempForm  WHERE   tintOrder > @tintOrder - 1 ORDER BY tintOrder ASC      
			
            SELECT TOP 1
                    @tintOrder = tintOrder + 1,
                    @vcDbColumnName = vcDbColumnName,
                    @vcFieldName = vcFieldName,
                    @vcAssociatedControlType = vcAssociatedControlType,
                    @vcListItemType = vcListItemType,
                    @numListID = numListID,
                    @vcLookBackTableName = vcLookBackTableName,
                    @bitCustom = bitCustomField,
                    @numFieldId = numFieldId,
                    @bitAllowSorting = bitAllowSorting,
                    @bitAllowEdit = bitAllowEdit,
                    @ListRelID = ListRelID
            FROM    #tempForm
            WHERE   tintOrder > @tintOrder - 1
            ORDER BY tintOrder ASC            
 
			
            IF @@rowcount = 0 
                SET @tintOrder = 0 
           
        END                       

      
-------Change Row Color-------
--    DECLARE @vcCSOrigDbCOlumnName AS VARCHAR(50),
--        @vcCSLookBackTableName AS VARCHAR(50),
--        @vcCSAssociatedControlType AS VARCHAR(50)
--    SET @vcCSOrigDbCOlumnName = ''
--    SET @vcCSLookBackTableName = ''

--select top 1 @vcCSOrigDbCOlumnName=DFM.vcOrigDbCOlumnName,@vcCSLookBackTableName=DFM.vcLookBackTableName from DycFieldColorScheme DFCS join DycFieldMaster DFM on 
--DFCS.numModuleID=DFM.numModuleID and DFCS.numFieldID=DFM.numFieldID
--where DFCS.numDomainID=@numDomainID and DFM.numModuleID=1

--    CREATE TABLE #tempColorScheme
--        (
--          vcOrigDbCOlumnName VARCHAR(50),
--          vcLookBackTableName VARCHAR(50),
--          vcFieldValue VARCHAR(50),
--          vcFieldValue1 VARCHAR(50),
--          vcColorScheme VARCHAR(50),
--          vcAssociatedControlType VARCHAR(50)
--        )
--
--    INSERT  INTO #tempColorScheme
--            SELECT  DFM.vcOrigDbCOlumnName,
--                    DFM.vcLookBackTableName,
--                    DFCS.vcFieldValue,
--                    DFCS.vcFieldValue1,
--                    DFCS.vcColorScheme,
--                    DFFM.vcAssociatedControlType
--            FROM    DycFieldColorScheme DFCS
--                    JOIN DycFormField_Mapping DFFM ON DFCS.numFieldID = DFFM.numFieldID
--                    JOIN DycFieldMaster DFM ON DFM.numModuleID = DFFM.numModuleID
--                                               AND DFM.numFieldID = DFFM.numFieldID
--            WHERE   DFCS.numDomainID = @numDomainID
--                    AND DFFM.numFormID = 80
--                    AND DFCS.numFormID = 80
--                    AND ISNULL(DFFM.bitAllowGridColor, 0) = 1
--
--    IF ( SELECT COUNT(*)
--         FROM   #tempColorScheme
--       ) > 0 
--        BEGIN
--            SELECT TOP 1
--                    @vcCSOrigDbCOlumnName = vcOrigDbCOlumnName,
--                    @vcCSLookBackTableName = vcLookBackTableName,
--                    @vcCSAssociatedControlType = vcAssociatedControlType
--            FROM    #tempColorScheme
--        END   
---------------------------- 
 
--    IF LEN(@vcCSOrigDbCOlumnName) > 0
--        AND LEN(@vcCSLookBackTableName) > 0 
--        BEGIN
--            SET @strSql = @strSql + ',tCS.vcColorScheme'
--        END
                                                                   
    SET @strSql = @strSql
        + ' ,TotalRowCount FROM                                                               
   AdditionalContactsInformation ADC                                                             
 JOIN Cases  Cs                                                              
   ON ADC.numContactId =Cs.numContactId                                                             
 JOIN DivisionMaster DM                                                              
  ON ADC.numDivisionId = DM.numDivisionID                                                             
  JOIN CompanyInfo CMP                                                             
 ON DM.numCompanyID = CMP.numCompanyId                                                             
 left join listdetails lst                                                            
 on lst.numListItemID= cs.numPriority                                                  
     ' + @WhereCondition + '                              
 join tblcases T on T.numCaseID=Cs.numCaseID'                                                            
              
--' union select count(*),null,null,null,null,null '+@strColumns+' from tblcases order by RowNumber'               
                                                         
   
--    IF LEN(@vcCSOrigDbCOlumnName) > 0
--        AND LEN(@vcCSLookBackTableName) > 0 
--        BEGIN
--            IF @vcCSLookBackTableName = 'AdditionalContactsInformation' 
--                SET @Prefix = 'ADC.'                  
--            IF @vcCSLookBackTableName = 'DivisionMaster' 
--                SET @Prefix = 'DM.'
--            IF @vcCSLookBackTableName = 'CompanyInfo' 
--                SET @Prefix = 'CMP.' 
--            IF @vcCSLookBackTableName = 'Cases' 
--                SET @Prefix = 'Cs.'   
-- 
--            IF @vcCSAssociatedControlType = 'DateField' 
--                BEGIN
--                    SET @strSql = @strSql
--                        + ' left join #tempColorScheme tCS on CONVERT(VARCHAR(10),'
--                        + @Prefix + @vcCSOrigDbCOlumnName
--                        + ',111) >= CONVERT(VARCHAR(10),DateAdd(day,cast(tCS.vcFieldValue as int),GETUTCDATE()),111)
--			 and CONVERT(VARCHAR(10),' + @Prefix + @vcCSOrigDbCOlumnName
--                        + ',111) <= CONVERT(VARCHAR(10),DateAdd(day,cast(tCS.vcFieldValue1 as int),GETUTCDATE()),111)'
--                END
--            ELSE 
--                IF @vcCSAssociatedControlType = 'SelectBox'
--                    OR @vcCSAssociatedControlType = 'ListBox'
--                    OR @vcCSAssociatedControlType = 'CheckBox' 
--                    BEGIN
--                        SET @strSql = @strSql
--                            + ' left join #tempColorScheme tCS on tCS.vcFieldValue='
--                            + @Prefix + @vcCSOrigDbCOlumnName
--                    END
--        END
    

    SET @strSql = @strSql + ' WHERE RowNumber > '
        + CONVERT(VARCHAR(10), @firstRec) + ' and RowNumber <'
        + CONVERT(VARCHAR(10), @lastRec) + ' order by RowNumber'

                                          
    PRINT @strSql                      
              
    EXEC ( @strSql
        )  

    SELECT  *
    FROM    #tempForm

    DROP TABLE #tempForm

    --DROP TABLE #tempColorScheme
END
ELSE
BEGIN
	Create table #tempTable ( ID INT IDENTITY PRIMARY KEY,                                          
		  numCaseID VARCHAR(15)                                           
	 )                                            
	                                             
	--declare @strSql as varchar(5000)                                            
	set @strSql='SELECT  '                               
	if @tintSortOrder=3 or @tintSortOrder=4  set @strSql=@strSql + ' top 20 '                                             
	   set @strSql=@strSql+ '                                             
	   Cs.numCaseID                                                             
	 FROM                                               
	   AdditionalContactsInformation ADC                                             
	 JOIN Cases  Cs                                              
	   ON ADC.numContactId =Cs.numContactId                                             
	 JOIN DivisionMaster Div                                               
	  ON ADC.numDivisionId = Div.numDivisionID'              
	   if @bitPartner=1 set @strSql=@strSql+' left join CaseContacts CCont on CCont.numCaseID=Cs.numCaseID and CCont.bitPartner=1 and CCont.numContactId='+convert(varchar(15),@numUserCntID)+ ''              
	                                               
	   set @strSql=@strSql+' JOIN CompanyInfo CMP                                             
	 ON Div.numCompanyID = CMP.numCompanyId                                             
	 left join listdetails lst                                            
	 on lst.numListItemID= cs.numPriority       
	 left join listdetails lst1                                            
	 on lst1.numListItemID= cs.numStatus                                                                
	 left join AdditionalContactsInformation ADC1                                            
	 on Cs.numAssignedTo=ADC1.numContactId                                            
	 Where                                            
	 cs.numDomainID='+ convert(varchar(15),@numDomainID )+''                      
	 
	IF @numStatus >0 
--	 set @strSql=@strSql+ 'and Cs.numStatus = '+ CONVERT(VARCHAR(15),@numStatus) 
--	ELSE
	 set @strSql=@strSql+ 'and Cs.numStatus<>136 '
	   
	 
	 if @FirstName<>'' set @strSql=@strSql+'and ADC.vcFirstName  like '''+@FirstName+'%'''                                                 
	 if @LastName<>'' set @strSql=@strSql+'and ADC.vcLastName like '''+@LastName+'%'''                      
	 if @CustName<>'' set @strSql=@strSql+' and CMP.vcCompanyName like '''+@CustName+'%'''                                            
	                                             
	                                           
	if @numDivisionID <>0 set @strSql=@strSql + ' And div.numDivisionID =' + convert(varchar(15),@numDivisionID)                                      
	if @SortChar<>'0' set @strSql=@strSql + ' And (ADC.vcFirstName like '''+@SortChar+'%'''                                              
	if @SortChar<>'0' set @strSql=@strSql + ' or ADC.vcLastName like '''+@SortChar+'%'')'                                             
	                                             
	if @tintUserRightType=1 set @strSql=@strSql + ' AND (Cs.numRecOwner = '+convert(varchar(15),@numUserCntId)+ ' or Cs.numAssignedTo='+convert(varchar(15),@numUserCntID) +        
	+ case when @bitPartner=1 then ' or (CCont.bitPartner=1 and CCont.numContactId='+convert(varchar(15),@numUserCntID)+ ')' else '' end  +')'  
	else if @tintUserRightType=2 set @strSql=@strSql + ' AND (Div.numTerID in (select numTerritoryID from  UserTerritory where numUserCntID= '+convert(varchar(15),@numUserCntId)+' ) or Div.numTerID=0 or Cs.numAssignedTo='+convert(varchar(15),@numUserCntID)+'
	) '   
	  
	if @numDivisionID=0                                    
	begin                                    
	 if @tintSortOrder=0  set @strSql=@strSql + ' AND (Cs.numRecOwner = '+convert(varchar(15),@numUserCntId)+' or Cs.numAssignedTo = '+convert(varchar(15),@numUserCntId)+')'                                                  
	end                                            
	if @tintSortOrder=2  set @strSql=@strSql + ' AND cs.bintCreatedDate> '''+convert(varchar(20),dateadd(day,-7,getutcdate()))+''''                 
	else if @tintSortOrder=3  set @strSql=@strSql + ' and cs.numCreatedBy ='+ convert(varchar(15),@numUserCntId)                              
	else if @tintSortOrder=4  set @strSql=@strSql + ' and cs.numModifiedBy ='+ convert(varchar(15),@numUserCntId)                                            
	                                       
	if @tintSortOrder=0 set @strSql=@strSql + ' ORDER BY ' + @columnName +' '+ @columnSortOrder                                           
	else if @tintSortOrder=1  set @strSql=@strSql + ' ORDER BY ' + @columnName +' '+ @columnSortOrder                                            
	else if @tintSortOrder=2  set @strSql=@strSql + ' ORDER BY ' + @columnName +' '+ @columnSortOrder                                            
	else if @tintSortOrder=3  set @strSql=@strSql + ' ORDER BY cs.bintCreatedDate '                                           
	else if @tintSortOrder=4  set @strSql=@strSql + ' ORDER BY cs.bintModifiedDate '                              

	PRINT @strSql;                                       
	insert into #tempTable (numCaseID)                                            
	exec( @strSql)
                                          
    set @firstRec= (@CurrentPage-1) * @PageSize                                            
	set @lastRec= (@CurrentPage*@PageSize+1)                     
	                    
	                    
	SELECT   ADC.vcFirstName +' '+ADC.vcLastName as Name,                                            
	   CMP.vcCompanyName,                                              
	   Cs.numCaseID,                                              
	   Cs.vcCaseNumber,                                              
	   Cs.intTargetResolveDate,                                              
	   Cs.numCreatedBy,                                              
	   Cs.textSubject,                                              
	   lst.vcdata as Priority,                                                  
	   CMP.numCompanyID,                                              
	   Div.numDivisionID,                                              
	   Div.numTerId,                                              
	   Div.tintCRMType,                                              
	   ADC.numContactID as ActCntID,                                            
	   ADC1.vcFirstName +' '+ADC1.vcLastName +'/ '+dbo.fn_GetContactName(Cs.numAssignedBy)  as AssignedToBy,                       
	   dbo.fn_GetListItemName(Cs.numStatus) as Status,     
	   Cs.numRecOwner                                          
	 FROM                                               
	   AdditionalContactsInformation ADC                                             
	 JOIN Cases  Cs                                              
	   ON ADC.numContactId =Cs.numContactId                                             
	 JOIN DivisionMaster Div                                               
	  ON ADC.numDivisionId = Div.numDivisionID                                             
	  JOIN CompanyInfo CMP                                             
	 ON Div.numCompanyID = CMP.numCompanyId                                             
	 left join listdetails lst                                            
	 on lst.numListItemID= cs.numPriority                                  
	 left join AdditionalContactsInformation ADC1                                            
	 on Cs.numAssignedTo=ADC1.numContactId                     
	 join #tempTable T on T.numCaseID=Cs.numCaseID                                            
	 where ID > @firstRec and ID < @lastRec               
	                    
	                                         
	set @TotRecs=(select count(*) from #tempTable)                                            
	drop table #tempTable
END

GO
