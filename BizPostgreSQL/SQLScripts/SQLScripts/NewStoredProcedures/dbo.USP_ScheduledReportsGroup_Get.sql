GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ScheduledReportsGroup_Get')
DROP PROCEDURE dbo.USP_ScheduledReportsGroup_Get
GO
CREATE PROCEDURE [dbo].[USP_ScheduledReportsGroup_Get]
(
	@numDomainID NUMERIC(18,0)
	,@numSRGID NUMERIC(18,0)
	,@ClientTimeZoneOffset INT
)
AS 
BEGIN
	SELECT 
		ID
		,vcName
		,tintFrequency
		,DATEADD(MINUTE,-1 * @ClientTimeZoneOffset,dtStartDate) dtStartDate
		,numEmailTemplate
		,vcSelectedTokens
		,vcRecipientsEmail
	FROM
		ScheduledReportsGroup
	WHERE
		numDomainID=@numDomainID 
		AND ID = @numSRGID
END
GO