GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetListItemData')
DROP PROCEDURE USP_GetListItemData
GO
CREATE PROCEDURE USP_GetListItemData
	@vcFldValue VARCHAR(MAX),
	@numDomainID NUMERIC(18,0) 
AS 
BEGIN
	SELECT STUFF((
					SELECT 
						CONCAT(', ',ISNULL([vcData],''))
					FROM 
						[ListDetails] 
					WHERE 
						[numDomainID]=@numDomainID 
						AND numListItemID IN (SELECT Id FROM dbo.SplitIDs(@vcFldValue,','))
					FOR XML PATH('')
				),1,2,'') as vcData	
END
GO