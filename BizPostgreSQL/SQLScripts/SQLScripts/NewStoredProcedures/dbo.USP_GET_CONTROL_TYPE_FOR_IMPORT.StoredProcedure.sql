GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GET_CONTROL_TYPE_FOR_IMPORT')
DROP PROCEDURE USP_GET_CONTROL_TYPE_FOR_IMPORT
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- USP_GET_CONTROL_TYPE_FOR_IMPORT 1,1
CREATE PROCEDURE USP_GET_CONTROL_TYPE_FOR_IMPORT
(
	@intImportFileID	BIGINT,
	@numDomainID		NUMERIC
)
AS 
BEGIN
	SELECT  PARENT.intImportFileID,
			vcImportFileName,
			PARENT.numDomainID,
		    DYNAMIC_FIELD.numFieldId AS [numFormFieldId],
		    DYNAMIC_FIELD.vcAssociatedControlType,
		    DYNAMIC_FIELD.vcDbColumnName,
		    DYNAMIC_FIELD.vcFieldName AS [vcFormFieldName],
		    FIELD_MAP.intMapColumnNo
	FROM Import_File_Master PARENT
	INNER JOIN Import_File_Field_Mapping FIELD_MAP ON PARENT.intImportFileID = FIELD_MAP.intImportFileID
	INNER JOIN dbo.DycFieldMaster DYNAMIC_FIELD ON FIELD_MAP.numFormFieldID = DYNAMIC_FIELD.numFieldId
													   AND DYNAMIC_FIELD.numFieldId = FIELD_MAP.numFormFieldID
	WHERE vcAssociatedControlType = 'SelectBox'
	  AND PARENT.intImportFileID = @intImportFileID
	  AND PARENT.numDomainID = @numDomainID 
	  
END
