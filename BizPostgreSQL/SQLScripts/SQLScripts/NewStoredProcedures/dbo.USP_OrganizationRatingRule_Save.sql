SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_OrganizationRatingRule_Save')
DROP PROCEDURE dbo.USP_OrganizationRatingRule_Save
GO
CREATE PROCEDURE [dbo].[USP_OrganizationRatingRule_Save]
(
	@numDomainID NUMERIC(18,0),
	@numUserCntID NUMERIC(18,0),
	@vcRules TEXT
)
AS 
BEGIN
BEGIN TRY
BEGIN TRANSACTION
	DECLARE @hDocItem INT
	EXEC sp_xml_preparedocument @hDocItem OUTPUT, @vcRules

	DECLARE @TEMP TABLE
	(
		ID INT IDENTITY(1,1)
		,tintPerformance TINYINT
		,numFromAmount NUMERIC(18,0)
		,numToAmount NUMERIC(18,0)
		,numOrganizationRatingID NUMERIC(18,0)
	)

	INSERT INTO @TEMP
	(
		tintPerformance
		,numFromAmount
		,numToAmount
		,numOrganizationRatingID
	)
	SELECT
		tintPerformance
		,numFromAmount
		,numToAmount
		,numOrganizationRatingID
	FROM
		OPENXML (@hDocItem,'/NewDataSet/Table',2)
	WITH
	(
		tintPerformance TINYINT
		,numFromAmount NUMERIC(18,0)
		,numToAmount NUMERIC(18,0)
		,numOrganizationRatingID NUMERIC(18,0)
	)

	IF (SELECT COUNT(*) FROM (SELECT DISTINCT tintPerformance FROM @TEMP) T1) > 1
	BEGIN
		RAISERROR('DIFFERENT_PERFORMANCE_RATING',16,1)
	END

	DELETE FROM OrganizationRatingRule WHERE numDomainID = @numDomainID

	DECLARE @i AS INT = 1
	DECLARE @COUNT AS iNT
	DECLARE @numORRID NUMERIC(18,0)
	DECLARE @tintPerformance TINYINT
	DECLARE @numFromAmount NUMERIC(18,0)
	DECLARE @numToAmount NUMERIC(18,0)
	DECLARE @numOrganizationRatingID NUMERIC(18,0)
	SELECT @COUNT = COUNT(*) FROM @TEMP

	WHILE @i <= @COUNT
	BEGIN
		SELECT
			@tintPerformance=tintPerformance
			,@numFromAmount=numFromAmount
			,@numToAmount=numToAmount
			,@numOrganizationRatingID=numOrganizationRatingID
		FROM
			@TEMP
		WHERE
			ID = @i

		IF EXISTS (SELECT * FROM OrganizationRatingRule WHERE numDomainID=@numDomainID AND (@numFromAmount BETWEEN numFromAmount AND numToAmount OR @numToAmount BETWEEN numFromAmount AND numToAmount))
		BEGIN
			RAISERROR('RULE_EXISTS_WITH_NUMBER',16,1)
		END

		-- INSERT NEW RULES
		INSERT INTO OrganizationRatingRule
		(
			numDomainID
			,tintPerformance
			,numFromAmount
			,numToAmount
			,numOrganizationRatingID
			,numCreatedBy
			,dtCreated
		)
		VALUES
		(
			@numDomainID
			,@tintPerformance
			,@numFromAmount
			,@numToAmount
			,@numOrganizationRatingID
			,@numUserCntID
			,GETUTCDATE()
		)

		SET @i = @i + 1
	END	
		
	
		 
	IF (SELECT COUNT(*) FROM OrganizationRatingRule WHERE numDomainID = @numDomainID) > 0
	BEGIN
		SET @tintPerformance = (SELECT DISTINCT tintPerformance FROM OrganizationRatingRule WHERE numDomainID = @numDomainID)
		EXEC USP_CompanyInfo_UpdateRating @numDomainID,0,@tintPerformance

		UPDATE DycFormField_Mapping SET bitAllowEdit=0, bitInlineEdit=0 WHERE numFieldID IN (SELECT numFieldId FROM DycFieldMaster WHERE vcDbColumnName = 'numCompanyRating') AND numFormID IN (34,35,36)
	END
	ELSE
	BEGIN
		UPDATE DycFormField_Mapping SET bitAllowEdit=1, bitInlineEdit=1 WHERE numFieldID IN (SELECT numFieldId FROM DycFieldMaster WHERE vcDbColumnName = 'numCompanyRating') AND numFormID IN (34,35,36)
	END
	
COMMIT
END TRY
BEGIN CATCH
	DECLARE @ErrorMessage NVARCHAR(4000)
	DECLARE @ErrorNumber INT
	DECLARE @ErrorSeverity INT
	DECLARE @ErrorState INT
	DECLARE @ErrorLine INT
	DECLARE @ErrorProcedure NVARCHAR(200)

	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;

	SELECT 
		@ErrorMessage = ERROR_MESSAGE(),
		@ErrorNumber = ERROR_NUMBER(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE(),
		@ErrorLine = ERROR_LINE(),
		@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

	RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);

END CATCH
END