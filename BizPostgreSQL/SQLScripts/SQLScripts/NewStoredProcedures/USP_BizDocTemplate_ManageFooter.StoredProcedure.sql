GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_BizDocTemplate_ManageFooter' ) 
    DROP PROCEDURE USP_BizDocTemplate_ManageFooter
GO
-- =============================================    
-- Author:  <Author,,Sachin Sadhu>    
-- Create date: <Create Date,,23rdDec2013>    
-- Description: <Description,,To Save Footer in BizDocTemplate Table>    
-- =============================================    
CREATE PROCEDURE [dbo].[USP_BizDocTemplate_ManageFooter]     
 -- Add the parameters for the stored procedure here    
    (  
      @numBizDocTempID NUMERIC(18) ,  
      @numDomainId AS NUMERIC(9) = 0 ,  
      @byteMode AS SMALLINT = 0 ,  
      @vcBizDocFooter AS VARCHAR(100) = ''    
     
    )  
AS   
    BEGIN    
 -- SET NOCOUNT ON added to prevent extra result sets from    
 -- interfering with SELECT statements.    
    
        SET NOCOUNT ON;    
      
        IF @byteMode = 1   
            BEGIN      
                UPDATE  BizDocTemplate  
                SET     vcBizDocFooter = @vcBizDocFooter  
                WHERE   numDomainID = @numDomainID  
                        AND numBizDocTempID = @numBizDocTempID     
            END      
        ELSE   
            IF @byteMode = 2   
                BEGIN      
                    UPDATE  BizDocTemplate  
                    SET     vcBizDocFooter = NULL  
                    WHERE   numDomainID = @numDomainID  
                            AND numBizDocTempID = @numBizDocTempID    
                END      
            ELSE   
                IF @byteMode = 3   
                    BEGIN      
                        UPDATE  BizDocTemplate  
                        SET     vcPurBizDocFooter = @vcBizDocFooter  
                        WHERE   numDomainID = @numDomainID  
                                AND numBizDocTempID = @numBizDocTempID    
                    END      
                ELSE   
                    IF @byteMode = 4   
                        BEGIN      
                            UPDATE  BizDocTemplate  
                            SET     vcPurBizDocFooter = NULL  
                            WHERE   numDomainID = @numDomainID  
                                    AND numBizDocTempID = @numBizDocTempID    
                        END      
      
    END    


