SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ManageTaxCountryConfiguration')
DROP PROCEDURE USP_ManageTaxCountryConfiguration
GO
CREATE PROCEDURE [dbo].[USP_ManageTaxCountryConfiguration] 
( 
@numDomainID as numeric(9)=0,    
@tintMode AS TINYINT,
@numTaxCountryConfi as numeric(9),
@numCountry as numeric(9),
@tintBaseTax AS TINYINT,
@tintBaseTaxOnArea AS TINYINT
)
AS 

IF @tintMode=1
BEGIN
	select numTaxCountryConfi,dbo.fn_getlistitemname(numCountry) as vCountryName,
		Case tintBaseTax when 1 then 'Billing Address' when 2 then 'Shipping Address' end as vcBaseTax,
		Case tintBaseTaxOnArea when 0 then 'State' when 1 then 'City' when 2 then 'Zip Code/Postal' end as vcBaseTaxOnArea  from TaxCountryConfi where numDomainId=@numDomainId
END

ELSE IF @tintMode=2
BEGIN
	select numTaxCountryConfi,numCountry,tintBaseTax,tintBaseTaxOnArea from TaxCountryConfi where numDomainId=@numDomainId and numTaxCountryConfi=@numTaxCountryConfi
END

Else IF @tintMode=3
BEGIN
	IF EXISTS (SELECT numTaxCountryConfi FROM TaxCountryConfi where numDomainId=@numDomainId and numCountry=@numCountry)
	BEGIN
		Update TaxCountryConfi set tintBaseTax=@tintBaseTax,tintBaseTaxOnArea=@tintBaseTaxOnArea where numTaxCountryConfi=@numTaxCountryConfi and numDomainId=@numDomainId
	END
	ELSE
	BEGIN
		Insert into TaxCountryConfi(numDomainId,numCountry,tintBaseTax,tintBaseTaxOnArea) 
							values (@numDomainId,@numCountry,@tintBaseTax,@tintBaseTaxOnArea)
	END
END
Else IF @tintMode=4
BEGIN
	select numTaxCountryConfi,numCountry,tintBaseTax,tintBaseTaxOnArea from TaxCountryConfi where numDomainId=@numDomainId and numCountry=@numCountry
END

