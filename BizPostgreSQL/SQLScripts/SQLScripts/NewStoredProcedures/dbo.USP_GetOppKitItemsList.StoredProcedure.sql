SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
                    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetOppKitItemsList')
DROP PROCEDURE USP_GetOppKitItemsList
GO
CREATE PROCEDURE [dbo].[USP_GetOppKitItemsList]   
	@numOppID AS NUMERIC(9),
	@numOppItemID as numeric(9)
as                            

SELECT I.vcItemName,OKI.numQtyItemsReq,OKI.numQtyShipped,W.vcWareHouse,ISNULL(numOnHand,0) numOnHand,isnull(numOnOrder,0) numOnOrder,isnull(numReorder,0) numReorder,isnull(numAllocation,0) numAllocation,isnull(numBackOrder,0) numBackOrder,
case when i.charItemType='P' then 'Inventory Item' when i.charItemType='S' then 'Service' when i.charItemType='A' then 'Accessory' when i.charItemType='N' then 'Non-Inventory Item' end as vcItemType
from OpportunityKitItems OKI join Item I on OKI.numChildItemID=I.numItemCode 
LEFT OUTER JOIN [WareHouseItems] WI ON WI.[numItemID] = I.numItemCode 
       AND WI.[numWareHouseItemID] = OKI.numWareHouseItemId
LEFT OUTER JOIN Warehouses W ON W.numWareHouseID=WI.numWareHouseID 
	where OKI.numOppID=@numOppID AND OKI.numOppItemID=@numOppItemID
  
GO
