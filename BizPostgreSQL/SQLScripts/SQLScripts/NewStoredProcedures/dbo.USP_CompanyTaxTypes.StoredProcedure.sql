GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_CompanyTaxTypes')
DROP PROCEDURE USP_CompanyTaxTypes
GO

--Created By Anoop Jayaraj
CREATE PROCEDURE USP_CompanyTaxTypes
@numDivisionID as numeric(9),
@numDomainID as numeric(9)
as

Select TaxItems.numTaxItemID,vcTaxName,isnull(bitApplicable,0) as bitApplicable from TaxItems
left join DivisionTaxTypes 
on DivisionTaxTypes.numTaxItemID=TaxItems.numTaxItemID and numDivisionID=@numDivisionID
where  numDomainID=@numDomainID order by TaxItems.numTaxItemID


