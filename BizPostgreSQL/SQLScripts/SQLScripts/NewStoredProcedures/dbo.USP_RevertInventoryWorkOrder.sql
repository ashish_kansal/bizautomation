SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
                    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_RevertInventoryWorkOrder')
DROP PROCEDURE USP_RevertInventoryWorkOrder
GO
CREATE PROCEDURE [dbo].[USP_RevertInventoryWorkOrder]
	@numDomainID AS NUMERIC(18,0),
	@numUserCntID AS NUMERIC(18,0),
	@numWOID AS NUMERIC(18,0)
AS
BEGIN
	--REVERT INVENTORY OF WORK ORDER ITEMS
	DECLARE @TEMP TABLE
	(
		ID INT IDENTITY(1,1),
		numWarehouseItemID NUMERIC(18,0),
		numQtyItemsReq FLOAT
	)

	INSERT INTO @TEMP SELECT numWareHouseItemId,numQtyItemsReq FROM WorkOrderDetails WHERE numWOId=@numWOId AND ISNULL(numWareHouseItemId,0) > 0


	DECLARE @i INT = 1
	DECLARE @COUNT INT = 0
	SELECT @COUNT = COUNT(*) FROM @TEMP

	DECLARE @numWareHouseItemID NUMERIC(18,0)
	DECLARE @numQtyItemsReq FLOAT
	DECLARE @Description VARCHAR(2000)
	DECLARE @onHand FLOAT
	DECLARE @onOrder FLOAT
	DECLARE @onAllocation FLOAT
	DECLARE @onBackOrder FLOAT

	WHILE @i <= @COUNT
	BEGIN
		SELECT @numWareHouseItemID=ISNULL(numWarehouseItemID,0),@numQtyItemsReq=ISNULL(numQtyItemsReq,0) FROM @TEMP WHERE ID = @i


		SET @Description = CONCAT('Items Allocation Reverted For Work Order',' (Qty:',@numQtyItemsReq,')')

		--GET CURRENT INEVENTORY STATUS
		SELECT
			@onHand = ISNULL(numOnHand,0),
			@onOrder = ISNULL(numOnOrder,0),
			@onAllocation = ISNULL(numAllocation,0),
			@onBackOrder = ISNULL(numBackOrder,0)
		FROM
			WareHouseItems
		WHERE
			numWareHouseItemID = @numWareHouseItemID

		--CHANGE INVENTORY
		IF @numQtyItemsReq >= @onBackOrder 		BEGIN			SET @numQtyItemsReq = @numQtyItemsReq - @onBackOrder			SET @onBackOrder = 0                            			IF (@onAllocation - @numQtyItemsReq >= 0)					SET @onAllocation = @onAllocation - @numQtyItemsReq									SET @onHand = @onHand + @numQtyItemsReq                                                          		END                                            		ELSE IF @numQtyItemsReq < @onBackOrder 		BEGIN			SET @onBackOrder = @onBackOrder - @numQtyItemsReq		END

		--UPDATE INVENTORY AND WAREHOUSE TRACKING
		EXEC USP_UpdateInventoryAndTracking 
		@numOnHand=@onHand,
		@numOnAllocation=@onAllocation,
		@numOnBackOrder=@onBackOrder,
		@numOnOrder=@onOrder,
		@numWarehouseItemID=@numWareHouseItemID,
		@numReferenceID=@numWOID,
		@tintRefType=2,
		@numDomainID=@numDomainID,
		@numUserCntID=@numUserCntID,
		@Description=@Description 

		SET @i = @i + 1
	END
END