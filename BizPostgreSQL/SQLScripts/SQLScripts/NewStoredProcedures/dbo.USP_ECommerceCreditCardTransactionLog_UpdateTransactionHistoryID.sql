GO
IF EXISTS (SELECT * FROM sysobjects WHERE xtype = 'p' AND NAME = 'USP_ECommerceCreditCardTransactionLog_UpdateTransactionHistoryID' ) 
    DROP PROCEDURE USP_ECommerceCreditCardTransactionLog_UpdateTransactionHistoryID
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[USP_ECommerceCreditCardTransactionLog_UpdateTransactionHistoryID]
(
    @numID NUMERIC(18,0)
	,@numTransHistoryID NUMERIC(18,0)
)
AS 
BEGIN
	UPDATE
		ECommerceCreditCardTransactionLog
	SET
		numTransHistoryID=@numTransHistoryID
	WHERE
		ID=@numID

	SELECT SCOPE_IDENTITY()

END
GO