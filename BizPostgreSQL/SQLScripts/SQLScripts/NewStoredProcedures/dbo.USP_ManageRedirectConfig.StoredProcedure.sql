        
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ManageRedirectConfig')
	DROP PROCEDURE USP_ManageRedirectConfig
GO

CREATE PROCEDURE [dbo].[USP_ManageRedirectConfig]
	@numRedirectConfigID numeric(18, 0) OUTPUT,
	@numDomainId numeric(18, 0),
	@numUserCntId numeric(18, 0),
	@numSiteId numeric(18, 0)=0,
	@vcOldUrl varchar(500)='',
	@vcNewUrl varchar(500)='',
	@numRedirectType numeric(18, 0)=0,
	@numReferenceType numeric(18, 0)=0,
	@numReferenceNo numeric(18, 0)=0
	
AS

SET NOCOUNT ON

DECLARE @Check as integer
SELECT @Check = COUNT(*) FROM [dbo].[RedirectConfig] WHERE  [numDomainId] = @numDomainId AND [numSiteId] = @numSiteId 
		AND ([vcOldUrl] = @vcOldUrl OR [vcOldUrl] = @vcNewUrl OR [vcNewUrl] = @vcOldUrl) AND bitIsActive = 1
SELECT @Check 
IF @Check <= 0
BEGIN


IF EXISTS(SELECT [numRedirectConfigID] FROM [dbo].[RedirectConfig] WHERE [numRedirectConfigID] = @numRedirectConfigID)
BEGIN
	UPDATE [dbo].[RedirectConfig] SET
		[numDomainId] = @numDomainId,
		[numUserCntId] = @numUserCntId,
		[numSiteId] = @numSiteId,
		[vcOldUrl] = @vcOldUrl,
		[vcNewUrl] = @vcNewUrl,
		[numRedirectType] = @numRedirectType,
		[numReferenceType] = @numReferenceType,
		[numReferenceNo] = @numReferenceNo,
		[dtModified] = GETDATE()
	WHERE
		[numRedirectConfigID] = @numRedirectConfigID
SELECT @numRedirectConfigID
END
ELSE
BEGIN
	INSERT INTO [dbo].[RedirectConfig] (
		[numDomainId],
		[numUserCntId],
		[numSiteId],
		[vcOldUrl],
		[vcNewUrl],
		[numRedirectType],
		[numReferenceType],
		[numReferenceNo],
		[dtCreated]
	) VALUES (
		@numDomainId,
		@numUserCntId,
		@numSiteId,
		@vcOldUrl,
		@vcNewUrl,
		@numRedirectType,
		@numReferenceType,
		@numReferenceNo,
		GETDATE()
	)
SELECT @numRedirectConfigID = @@IDENTITY

END

END

ELSE

BEGIN 
DECLARE @DuplicateId as integer
SELECT @DuplicateId = [numRedirectConfigID] FROM [dbo].[RedirectConfig] WHERE  [numDomainId] = @numDomainId AND [numSiteId] = @numSiteId 
		AND ([vcOldUrl] = @vcOldUrl OR [vcOldUrl] = @vcNewUrl OR [vcNewUrl] = @vcOldUrl) AND bitIsActive = 1

RAISERROR('URL_ALREADY_MAPPED,%i',16,1,@DuplicateId); 
END
