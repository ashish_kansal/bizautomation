-- CREATED BY MANISH ANJARA : 1st OCT, 2014
/*
EXEC USP_GetJournalEntries_Virtual @numDomainId = 169, @vcAccountId = '6013',
    @dtFromDate = '2014-01-01 00:00:00', @dtToDate = '2014-01-01 23:59:59',
    @vcTranType = '', @varDescription = '', @CompanyName = '',
    @vcBizPayment = '', @vcCheqNo = '', @vcBizDocID = '', @vcTranRef = '',
    @vcTranDesc = '', @numDivisionID = 0, @ClientTimeZoneOffset = -330,
    @charReconFilter = 'A', @tintMode = 1, @numItemID = 0, @CurrentPage = 1,
    @PageSize = 100

*/

GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_GetJournalEntries_Virtual' )
    DROP PROCEDURE USP_GetJournalEntries_Virtual
GO
CREATE PROCEDURE [dbo].[USP_GetJournalEntries_Virtual]
(
      @numDomainID INT,
      @vcAccountId VARCHAR(4000),
      @dtFromDate DATETIME,
      @dtToDate DATETIME,
      @vcTranType VARCHAR(50) = '',
      @varDescription VARCHAR(50) = '',
      @CompanyName VARCHAR(50) = '',
      @vcBizPayment VARCHAR(50) = '',
      @vcCheqNo VARCHAR(50) = '',
      @vcBizDocID VARCHAR(50) = '',
      @vcTranRef VARCHAR(50) = '',
      @vcTranDesc VARCHAR(50) = '',
	  @numDivisionID NUMERIC(9)=0,
      @ClientTimeZoneOffset INT,  --Added by Chintan to enable calculation of date according to client machine
      @charReconFilter CHAR(1)='',
	  @tintMode AS TINYINT=0,
      @numItemID AS NUMERIC(9)=0,
      @CurrentPage INT=0,
	  @PageSize INT=0
)
WITH Recompile 
AS 
    BEGIN

	DECLARE @vcAccountIDs AS VARCHAR(MAX)
	SELECT @vcAccountIDs = COALESCE(@vcAccountIDs + ',', '') + CAST([COA].[numAccountId] AS VARCHAR(20))
	FROM [dbo].[Chart_Of_Accounts] AS COA 
	WHERE [COA].[numDomainId] = @numDomainID
	PRINT @vcAccountIDs
	SET @vcAccountId= @vcAccountIDs

DECLARE @dtFinFromDate DATETIME;
SET @dtFinFromDate = ( SELECT   dtPeriodFrom
                       FROM     FINANCIALYEAR
                       WHERE    dtPeriodFrom <= @dtFromDate
                                AND dtPeriodTo >= @dtFromDate
                                AND numDomainId = @numDomainId
                     );

PRINT @dtFromDate
PRINT @dtToDate

DECLARE @dtFinYearFromJournal DATETIME ;
DECLARE @dtFinYearFrom DATETIME ;
SELECT @dtFinYearFromJournal = MIN(datEntry_Date) FROM view_journal WHERE numDomainID=@numDomainId 
SET @dtFinYearFrom = ( SELECT   dtPeriodFrom
                        FROM     FINANCIALYEAR
                        WHERE    dtPeriodFrom <= @dtFromDate
                                AND dtPeriodTo >= @dtFromDate
                                AND numDomainId = @numDomainId
                        ) ;

SELECT * INTO #view_journal FROM view_journal WHERE numDomainID=@numDomainId 

DECLARE @numMaxFinYearID NUMERIC
DECLARE @Month INT
DECLARE @year  INT
SELECT @numMaxFinYearID = MAX(numFinYearId) FROM dbo.FinancialYear WHERE numDomainId=@numDomainId
SELECT @Month = DATEPART(MONTH,dtPeriodFrom),@year =DATEPART(year,dtPeriodFrom) FROM dbo.FinancialYear WHERE numDomainId=@numDomainId AND numFinYearId=@numMaxFinYearID
PRINT 'MONTH:' + CAST(@Month AS VARCHAR(100))
PRINT 'YEAR:' + CAST(@year AS VARCHAR(100))
		 
DECLARE @first_id NUMERIC, @startRow int
DECLARE @CurrRecord as INT

if @tintMode = 0
BEGIN
SELECT  COA1.numAccountId INTO #Temp1 /*,COA1.vcAccountCode ,COA2.vcAccountCode*/
		FROM    dbo.Chart_Of_Accounts COA1
		WHERE   COA1.numAccountId IN(SELECT  cast(ID AS NUMERIC(9)) FROM dbo.SplitIDs(@vcAccountId, ','))
				AND COA1.numDomainId = @numDomainID
	SELECT @vcAccountId = ISNULL( STUFF((SELECT ',' + CAST(numAccountID AS VARCHAR(10)) FROM #Temp1 FOR XML PATH('')),1, 1, '') , '') 
    
    	--SELECT  @numDomainID AS numDomainID,* FROM dbo.fn_GetOpeningBalance(@vcAccountID,@numDomainID,@dtFromDate,@ClientTimeZoneOffset)
		SELECT  @numDomainID AS numDomainID, numAccountId,vcAccountName, numParntAcntTypeID,	vcAccountDescription,	vcAccountCode,	dtAsOnDate,	
		CASE WHEN ([Table1].[vcAccountCode] LIKE '0103%' OR [Table1].[vcAccountCode] LIKE '0104%' OR [Table1].[vcAccountCode] LIKE '0106%') AND (@year + @Month) = (DATEPART(YEAR,@dtFromDate) + DATEPART(MONTH,@dtFromDate)) THEN 0 
			 WHEN (@year + @Month) > (DATEPART(YEAR,@dtFromDate) + DATEPART(MONTH,@dtFromDate)) THEN isnull(t1.[OPENING],0)
			 WHEN (@year + @Month) < (DATEPART(YEAR,@dtFromDate) + DATEPART(MONTH,@dtFromDate)) THEN isnull(t2.[OPENING],0)
			 ELSE isnull(mnOpeningBalance,0)
		END  [mnOpeningBalance],
							(SELECT ISNULL(SUM(GJD.numDebitAmt),0) - ISNULL(SUM(GJD.numCreditAmt),0)
							FROM
							General_Journal_Details GJD 
							JOIN Domain D ON D.[numDomainId] = GJD.numDomainID AND D.[numDomainId] = @numDomainID
							INNER JOIN General_Journal_Header GJH ON GJD.numJournalId = GJH.numJournal_Id AND 
							GJD.numDomainId = @numDomainID AND 
							GJD.numChartAcntId = [Table1].numAccountId AND 
							GJH.numDomainID = D.[numDomainId] AND 
							GJH.datEntry_Date <= @dtFinFromDate) 
FROM dbo.fn_GetOpeningBalance(@vcAccountID,@numDomainID,@dtFromDate,@ClientTimeZoneOffset) [Table1]
OUTER APPLY (SELECT   SUM(Debit - Credit) AS OPENING
            FROM     #view_journal VJ
            WHERE    VJ.numDomainId = @numDomainId
                    AND VJ.numAccountID = [Table1].numAccountID /*AND VJ.COAvcAccountCode like COA.vcAccountCode + '%'*/ /*VJ.numAccountId = COA.numAccountId*/
                    AND datEntry_Date BETWEEN (CASE WHEN [Table1].[vcAccountCode] LIKE '0103%' OR [Table1].[vcAccountCode] LIKE '0104%' OR [Table1].[vcAccountCode] LIKE '0106%' 
													THEN @dtFinYearFrom 
													ELSE @dtFinYearFromJournal 
											   END ) AND  DATEADD(Minute,-1,@dtFromDate)) AS t1
OUTER APPLY (SELECT   SUM(Debit - Credit) AS OPENING
            FROM     #view_journal VJ
            WHERE    VJ.numDomainId = @numDomainId
                    AND VJ.numAccountID = [Table1].numAccountID /*AND VJ.COAvcAccountCode like COA.vcAccountCode + '%'*/ /*VJ.numAccountId = COA.numAccountId*/
                    AND datEntry_Date BETWEEN (CASE WHEN [Table1].[vcAccountCode] LIKE '0103%' OR [Table1].[vcAccountCode] LIKE '0104%' OR [Table1].[vcAccountCode] LIKE '0106%' 
													THEN @dtFinYearFrom 
													ELSE @dtFinYearFromJournal 
											   END ) AND  DATEADD(Minute,1,@dtFromDate)) AS t2

		--SET @dtFromDate = DateAdd(minute, @ClientTimeZoneOffset, @dtFromDate);
		--SET @dtToDate = DateAdd(minute, @ClientTimeZoneOffset, @dtToDate);
SELECT * FROM 
(
SELECT TOP 1
dbo.General_Journal_Details.numDomainId,
 [TABLE1].numAccountId , 
dbo.General_Journal_Header.numJournal_Id,
dbo.[FormatedDateFromDate](dbo.General_Journal_Header.datEntry_Date,dbo.General_Journal_Details.numDomainID) Date,
dbo.General_Journal_Header.varDescription, 
dbo.CompanyInfo.vcCompanyName AS CompanyName,
dbo.General_Journal_Details.numTransactionId,

ISNULL(ISNULL(ISNULL(dbo.VIEW_BIZPAYMENT.Narration,dbo.General_Journal_Header.varDescription),'') + ' ' + 
(CASE WHEN isnull(dbo.General_Journal_Header.numReturnID, 0) <> 0 THEN ' Chek No: ' + isnull( CAST(CH.numCheckNo AS VARCHAR(50)),'No-Cheq') 
ELSE  dbo.VIEW_JOURNALCHEQ.Narration 
END),'') as Narration,

ISNULL(dbo.General_Journal_Details.vcReference,'') AS TranRef, 
ISNULL(dbo.General_Journal_Details.varDescription,'') AS TranDesc, 
CONVERT(VARCHAR(20),ISNULL([GJD].numDebitAmt,0)) numDebitAmt,
CONVERT(VARCHAR(20),ISNULL(General_Journal_Details.numDebitAmt,0)) [numDebitAmt_General_Journal_Details],
CONVERT(VARCHAR(20),ISNULL(General_Journal_Details.numCreditAmt,0)) numCreditAmt,
CONVERT(VARCHAR(20),ISNULL([GJD].numCreditAmt,0)) [numCreditAmt_GJD],
CASE WHEN ISNULL(General_Journal_Details.[numDebitAmt],0) = 0 THEN [TABLE1].vcAccountName ELSE '' END [vcAccountName],
CASE WHEN ISNULL([GJD].[numCreditAmt],0) = 0 THEN [TABLE2].[vcAccountName] ELSE '' END AS [vcSplitAccountName],
'' as balance,
CASE 
WHEN isnull(dbo.General_Journal_Header.numCheckHeaderID, 0) <> 0 THEN + 'Checks' 
WHEN isnull(dbo.General_Journal_Header.numDepositId, 0) <> 0 AND DM.tintDepositePage = 1 THEN 'Deposit' 
WHEN isnull(dbo.General_Journal_Header.numDepositId, 0) <> 0 AND DM.tintDepositePage = 2 THEN 'Receved Pmt' 
WHEN isnull(dbo.General_Journal_Header.numOppId, 0) <> 0 AND isnull(dbo.General_Journal_Header.numOppBizDocsId, 0) <> 0 AND isnull(dbo.General_Journal_Header.numBizDocsPaymentDetId, 0) = 0 AND 
dbo.OpportunityMaster.tintOppType = 1 THEN 'BizDocs Invoice' 
WHEN isnull(dbo.General_Journal_Header.numOppId, 0) <> 0 AND isnull(dbo.General_Journal_Header.numOppBizDocsId, 0) <> 0 AND isnull(dbo.General_Journal_Header.numBizDocsPaymentDetId, 0) = 0 AND 
dbo.OpportunityMaster.tintOppType = 2 THEN 'BizDocs Purchase' 
WHEN isnull(dbo.General_Journal_Header.numCategoryHDRID, 0) <> 0 THEN 'Time And Expenses' 
WHEN ISNULL(General_Journal_Header.numBillId,0) <>0 AND isnull(BH.bitLandedCost,0)=1 then 'Landed Cost' 
WHEN isnull(dbo.General_Journal_Header.numBillID, 0) <> 0 THEN 'Bill'
WHEN isnull(dbo.General_Journal_Header.numBillPaymentID, 0) <> 0 THEN 'Pay Bill'
WHEN isnull(dbo.General_Journal_Header.numReturnID, 0) <> 0 THEN 
																CASE 
																	WHEN RH.tintReturnType=1 AND RH.tintReceiveType=1 THEN 'Sales Return Refund'
																	WHEN RH.tintReturnType=1 AND RH.tintReceiveType=2 THEN 'Sales Return Credit Memo'
																	WHEN RH.tintReturnType=2 AND RH.tintReceiveType=2 THEN 'Purchase Return Credit Memo'
																	WHEN RH.tintReturnType=3 AND RH.tintReceiveType=2 THEN 'Credit Memo'
																	WHEN RH.tintReturnType=4 AND RH.tintReceiveType=1 THEN 'Refund Against Credit Memo'
																END 
WHEN isnull(dbo.General_Journal_Header.numOppId, 0) <> 0 AND isnull(dbo.General_Journal_Header.numOppBizDocsId, 0) = 0 THEN 'PO Fulfillment'
WHEN dbo.General_Journal_Header.numJournal_Id <> 0 THEN 'Journal' 
END AS TransactionType,

ISNULL(dbo.General_Journal_Details.bitCleared,0) AS bitCleared,
ISNULL(dbo.General_Journal_Details.bitReconcile,0) AS bitReconcile,
ISNULL(dbo.General_Journal_Details.numItemID,0) AS numItemID,
Case When isnull(BH.bitLandedCost,0)=1 then  isnull(BH.numOppId,0) else 0 end AS numLandedCostOppId,
--STUFF((SELECT ', ' + CAST([COA].[vcAccountName] AS VARCHAR(4000))
--			FROM [dbo].[General_Journal_Details] AS GJD JOIN [dbo].[Chart_Of_Accounts] AS COA ON GJD.[numChartAcntId] = COA.[numAccountId]
--			WHERE [GJD].[numJournalId] = dbo.General_Journal_Header.numJournal_Id 
--			AND [COA].[numDomainId] = General_Journal_Header.[numDomainId]
--			AND [COA].[numAccountId] <> dbo.Chart_Of_Accounts.numAccountId FOR XML PATH('')) ,1,2, '') AS [vcSplitAccountName]
[dbo].[General_Journal_Details].numEntryDateSortOrder1
FROM dbo.General_Journal_Header 
INNER JOIN dbo.General_Journal_Details ON dbo.General_Journal_Header.numJournal_Id = dbo.General_Journal_Details.numJournalId 
INNER JOIN dbo.General_Journal_Details GJD ON dbo.General_Journal_Header.numJournal_Id = GJD.numJournalId 
INNER JOIN dbo.Chart_Of_Accounts ON dbo.General_Journal_Details.numChartAcntId = dbo.Chart_Of_Accounts.numAccountId AND dbo.General_Journal_Details.[numChartAcntId] <> GJD.[numChartAcntId]
OUTER APPLY (SELECT vcAccountName,[COA].[numAccountId] FROM [dbo].[Chart_Of_Accounts] AS COA 
			WHERE dbo.Chart_Of_Accounts.numAccountId = COA.[numAccountId]
			AND [COA].[numDomainId] = General_Journal_Header.[numDomainId]
			AND dbo.General_Journal_Details.[numChartAcntId] = [COA].[numAccountId]) TABLE1
OUTER APPLY (SELECT vcAccountName,[COA1].[numAccountId] FROM [dbo].[Chart_Of_Accounts] AS COA1 
			WHERE dbo.Chart_Of_Accounts.numAccountId <> COA1.[numAccountId]
			AND [COA1].[numDomainId] = General_Journal_Header.[numDomainId]
			AND [GJD].[numChartAcntId] = [COA1].[numAccountId]) TABLE2
LEFT OUTER JOIN dbo.TimeAndExpense ON dbo.General_Journal_Header.numCategoryHDRID = dbo.TimeAndExpense.numCategoryHDRID 
LEFT OUTER JOIN dbo.DivisionMaster ON dbo.General_Journal_Details.numCustomerId = dbo.DivisionMaster.numDivisionID and 
dbo.General_Journal_Details.numDomainID = dbo.DivisionMaster.numDomainID
LEFT OUTER JOIN dbo.CompanyInfo ON dbo.DivisionMaster.numCompanyID = dbo.CompanyInfo.numCompanyId 
LEFT OUTER JOIN dbo.OpportunityMaster ON dbo.General_Journal_Header.numOppId = dbo.OpportunityMaster.numOppId LEFT OUTER JOIN
dbo.OpportunityBizDocs ON dbo.General_Journal_Header.numOppBizDocsId = dbo.OpportunityBizDocs.numOppBizDocsId LEFT OUTER JOIN
dbo.VIEW_JOURNALCHEQ ON (dbo.General_Journal_Header.numCheckHeaderID = dbo.VIEW_JOURNALCHEQ.numCheckHeaderID 
or (dbo.General_Journal_Header.numBillPaymentID=dbo.VIEW_JOURNALCHEQ.numReferenceID and dbo.VIEW_JOURNALCHEQ.tintReferenceType=8))LEFT OUTER JOIN
dbo.VIEW_BIZPAYMENT ON dbo.General_Journal_Header.numBizDocsPaymentDetId = dbo.VIEW_BIZPAYMENT.numBizDocsPaymentDetId LEFT OUTER JOIN
dbo.DepositMaster DM ON DM.numDepositId = dbo.General_Journal_Header.numDepositId  LEFT OUTER JOIN
dbo.ReturnHeader RH ON RH.numReturnHeaderID = dbo.General_Journal_Header.numReturnID LEFT OUTER JOIN
dbo.CheckHeader CH ON CH.numReferenceID=RH.numReturnHeaderID AND CH.tintReferenceType=10
LEFT OUTER JOIN BillHeader BH  ON ISNULL(General_Journal_Header.numBillId,0) = BH.numBillId
WHERE   dbo.General_Journal_Header.datEntry_Date BETWEEN  @dtFromDate AND @dtToDate -- Removed Tmezone bug id 1029 
AND dbo.General_Journal_Header.numDomainId = @numDomainID 
 AND (dbo.DivisionMaster.numDivisionID = @numDivisionID OR @numDivisionID = 0)
AND ([dbo].[General_Journal_Details].bitCleared = CASE @charReconFilter WHEN 'C' THEN 1 ELSE 0 END  OR RTRIM(@charReconFilter)='A')
AND ([dbo].[General_Journal_Details].bitReconcile = CASE @charReconFilter WHEN 'R' THEN 1 ELSE 0 END  OR RTRIM(@charReconFilter)='A') 
AND ([TABLE1].numAccountId IN (SELECT numAccountId FROM #Temp1))
--AND numJournal_Id = 181544
--ORDER BY dbo.General_Journal_Header.numEntryDateSortOrder asc
--ORDER BY dbo.Chart_Of_Accounts.numAccountId ASC,  dbo.General_Journal_Header.datEntry_Date ASC, dbo.General_Journal_Details.numTransactionId asc 
) TABLE1 WHERE ISNULL(TransactionType,'') = 'Journal'
ORDER BY TABLE1.numEntryDateSortOrder1 ASC
END
ELSE IF @tintMode = 1
BEGIN

--SELECT @vcAccountID

SELECT  @numDomainID AS numDomainID, numAccountId,vcAccountName, numParntAcntTypeID,	vcAccountDescription,	vcAccountCode,	dtAsOnDate,	
		CASE WHEN ([Table1].[vcAccountCode] LIKE '0103%' OR [Table1].[vcAccountCode] LIKE '0104%' OR [Table1].[vcAccountCode] LIKE '0106%') AND (@year + @Month) = (DATEPART(YEAR,@dtFromDate) + DATEPART(MONTH,@dtFromDate)) THEN 0 
			 WHEN (@year + @Month) > (DATEPART(YEAR,@dtFromDate) + DATEPART(MONTH,@dtFromDate)) THEN isnull(t1.[OPENING],0)
			 WHEN (@year + @Month) < (DATEPART(YEAR,@dtFromDate) + DATEPART(MONTH,@dtFromDate)) THEN isnull(t2.[OPENING],0)
			 ELSE isnull(mnOpeningBalance,0)
		END  [mnOpeningBalance],
							(SELECT ISNULL(SUM(GJD.numDebitAmt),0) - ISNULL(SUM(GJD.numCreditAmt),0)
							FROM
							General_Journal_Details GJD 
							JOIN Domain D ON D.[numDomainId] = GJD.numDomainID AND D.[numDomainId] = @numDomainID
							INNER JOIN General_Journal_Header GJH ON GJD.numJournalId = GJH.numJournal_Id AND 
							GJD.numDomainId = @numDomainID AND 
							GJD.numChartAcntId = [Table1].numAccountId AND 
							GJH.numDomainID = D.[numDomainId] AND 
							GJH.datEntry_Date <= @dtFinFromDate) 
FROM dbo.fn_GetOpeningBalance(@vcAccountID,@numDomainID,@dtFromDate,@ClientTimeZoneOffset) [Table1]
OUTER APPLY (SELECT   SUM(Debit - Credit) AS OPENING
            FROM     #view_journal VJ
            WHERE    VJ.numDomainId = @numDomainId
                    AND VJ.numAccountID = [Table1].numAccountID /*AND VJ.COAvcAccountCode like COA.vcAccountCode + '%'*/ /*VJ.numAccountId = COA.numAccountId*/
                    AND datEntry_Date BETWEEN (CASE WHEN [Table1].[vcAccountCode] LIKE '0103%' OR [Table1].[vcAccountCode] LIKE '0104%' OR [Table1].[vcAccountCode] LIKE '0106%' 
													THEN @dtFinYearFrom 
													ELSE @dtFinYearFromJournal 
											   END ) AND  DATEADD(Minute,-1,@dtFromDate)) AS t1
OUTER APPLY (SELECT   SUM(Debit - Credit) AS OPENING
            FROM     #view_journal VJ
            WHERE    VJ.numDomainId = @numDomainId
                    AND VJ.numAccountID = [Table1].numAccountID /*AND VJ.COAvcAccountCode like COA.vcAccountCode + '%'*/ /*VJ.numAccountId = COA.numAccountId*/
                    AND datEntry_Date BETWEEN (CASE WHEN [Table1].[vcAccountCode] LIKE '0103%' OR [Table1].[vcAccountCode] LIKE '0104%' OR [Table1].[vcAccountCode] LIKE '0106%' 
													THEN @dtFinYearFrom 
													ELSE @dtFinYearFromJournal 
											   END ) AND  DATEADD(Minute,1,@dtFromDate)) AS t2

--SET @dtFromDate = DateAdd(minute, @ClientTimeZoneOffset, @dtFromDate);
--SET @dtToDate = DateAdd(minute, @ClientTimeZoneOffset, @dtToDate);

SET @CurrRecord = ((@CurrentPage - 1) * @PageSize) + 1
PRINT @CurrRecord
SET ROWCOUNT @CurrRecord

SELECT @first_id = dbo.General_Journal_Details.numEntryDateSortOrder1
FROM dbo.General_Journal_Header 
INNER JOIN dbo.General_Journal_Details ON dbo.General_Journal_Header.numJournal_Id = dbo.General_Journal_Details.numJournalId   
INNER JOIN dbo.Chart_Of_Accounts ON dbo.General_Journal_Details.numChartAcntId = dbo.Chart_Of_Accounts.numAccountId 
LEFT OUTER JOIN dbo.TimeAndExpense ON dbo.General_Journal_Header.numCategoryHDRID = dbo.TimeAndExpense.numCategoryHDRID 
WHERE dbo.General_Journal_Header.datEntry_Date BETWEEN  @dtFromDate AND @dtToDate 
AND dbo.General_Journal_Header.numDomainId = @numDomainID 
AND dbo.General_Journal_Details.numDomainId = @numDomainID 
AND dbo.Chart_Of_Accounts.numDomainId = @numDomainID 
AND (bitCleared = CASE @charReconFilter WHEN 'C' THEN 1 ELSE 0 END  OR RTRIM(@charReconFilter)='A')
AND (bitReconcile = CASE @charReconFilter WHEN 'R' THEN 1 ELSE 0 END  OR RTRIM(@charReconFilter)='A') 
--AND (dbo.Chart_Of_Accounts.numAccountId IN (SELECT [SS].[OutParam] FROM [dbo].[SplitString](@vcAccountId,',') AS SS))
AND (General_Journal_Details.numItemID = @numItemID OR @numItemID = 0)
AND dbo.General_Journal_Details.numEntryDateSortOrder1 > 0
ORDER BY dbo.General_Journal_Details.numEntryDateSortOrder1 asc

PRINT @first_id
PRINT ROWCOUNT_BIG()
SET ROWCOUNT @PageSize

SELECT * FROM 
(
SELECT dbo.General_Journal_Details.numDomainId, [TABLE1].numAccountId , dbo.General_Journal_Header.numJournal_Id,
dbo.[FormatedDateFromDate](dbo.General_Journal_Header.datEntry_Date,dbo.General_Journal_Details.numDomainID) Date,
dbo.General_Journal_Header.varDescription, 
dbo.CompanyInfo.vcCompanyName AS CompanyName,
dbo.General_Journal_Details.numTransactionId,
ISNULL(ISNULL(ISNULL(dbo.VIEW_BIZPAYMENT.Narration,dbo.General_Journal_Header.varDescription),'') + ' ' + 
(CASE WHEN isnull(dbo.General_Journal_Header.numReturnID, 0) <> 0 THEN ' Chek No: ' + isnull( CAST(CH.numCheckNo AS VARCHAR(50)),'No-Cheq') 
ELSE  dbo.VIEW_JOURNALCHEQ.Narration 
END),'') as Narration,
ISNULL(dbo.General_Journal_Details.vcReference,'') AS TranRef, 
ISNULL(dbo.General_Journal_Details.varDescription,'') AS TranDesc, 
CONVERT(VARCHAR(20),ISNULL([GJD].numDebitAmt,0)) numDebitAmt,
CONVERT(VARCHAR(20),ISNULL(General_Journal_Details.numDebitAmt,0)) [numDebitAmt_General_Journal_Details],
CONVERT(VARCHAR(20),ISNULL(General_Journal_Details.numCreditAmt,0)) numCreditAmt,
CONVERT(VARCHAR(20),ISNULL([GJD].numCreditAmt,0)) [numCreditAmt_GJD],
CASE WHEN ISNULL(General_Journal_Details.[numDebitAmt],0) = 0 THEN [TABLE1].vcAccountName ELSE '' END [vcAccountName],
CASE WHEN ISNULL([GJD].[numCreditAmt],0) = 0 THEN [TABLE2].[vcAccountName] ELSE '' END AS [vcSplitAccountName],
'' as balance,
CASE 
WHEN isnull(dbo.General_Journal_Header.numCheckHeaderID, 0) <> 0 THEN + 'Checks' 
WHEN isnull(dbo.General_Journal_Header.numDepositId, 0) <> 0 AND DM.tintDepositePage = 1 THEN 'Deposit' 
WHEN isnull(dbo.General_Journal_Header.numDepositId, 0) <> 0 AND DM.tintDepositePage = 2 THEN 'Receved Pmt' 
WHEN isnull(dbo.General_Journal_Header.numOppId, 0) <> 0 AND isnull(dbo.General_Journal_Header.numOppBizDocsId, 0) <> 0 AND isnull(dbo.General_Journal_Header.numBizDocsPaymentDetId, 0) = 0 AND 
dbo.OpportunityMaster.tintOppType = 1 THEN 'BizDocs Invoice' 
WHEN isnull(dbo.General_Journal_Header.numOppId, 0) <> 0 AND isnull(dbo.General_Journal_Header.numOppBizDocsId, 0) <> 0 AND isnull(dbo.General_Journal_Header.numBizDocsPaymentDetId, 0) = 0 AND 
dbo.OpportunityMaster.tintOppType = 2 THEN 'BizDocs Purchase' 
WHEN isnull(dbo.General_Journal_Header.numCategoryHDRID, 0) <> 0 THEN 'Time And Expenses' 
WHEN ISNULL(General_Journal_Header.numBillId,0) <>0 AND isnull(BH.bitLandedCost,0)=1 then 'Landed Cost' 
WHEN isnull(dbo.General_Journal_Header.numBillID, 0) <> 0 THEN 'Bill'
WHEN isnull(dbo.General_Journal_Header.numBillPaymentID, 0) <> 0 THEN 'Pay Bill'
WHEN isnull(dbo.General_Journal_Header.numReturnID, 0) <> 0 THEN 
																CASE 
																	WHEN RH.tintReturnType=1 AND RH.tintReceiveType=1 THEN 'Sales Return Refund'
																	WHEN RH.tintReturnType=1 AND RH.tintReceiveType=2 THEN 'Sales Return Credit Memo'
																	WHEN RH.tintReturnType=2 AND RH.tintReceiveType=2 THEN 'Purchase Return Credit Memo'
																	WHEN RH.tintReturnType=3 AND RH.tintReceiveType=2 THEN 'Credit Memo'
																	WHEN RH.tintReturnType=4 AND RH.tintReceiveType=1 THEN 'Refund Against Credit Memo'
																END 
WHEN isnull(dbo.General_Journal_Header.numOppId, 0) <> 0 AND isnull(dbo.General_Journal_Header.numOppBizDocsId, 0) = 0 THEN 'PO Fulfillment'
WHEN dbo.General_Journal_Header.numJournal_Id <> 0 THEN 'Journal' 
END AS TransactionType,
ISNULL(dbo.General_Journal_Details.bitCleared,0) AS bitCleared,
ISNULL(dbo.General_Journal_Details.bitReconcile,0) AS bitReconcile,
ISNULL(dbo.General_Journal_Details.numItemID,0) AS numItemID,
dbo.General_Journal_Details.numEntryDateSortOrder1 AS numEntryDateSortOrder,
Case When isnull(BH.bitLandedCost,0)=1 then  isnull(BH.numOppId,0) else 0 end AS numLandedCostOppId,
--STUFF((SELECT ', ' + CAST([COA].[vcAccountName] AS VARCHAR(4000))
--			FROM [dbo].[General_Journal_Details] AS GJD JOIN [dbo].[Chart_Of_Accounts] AS COA ON GJD.[numChartAcntId] = COA.[numAccountId]
--			WHERE [GJD].[numJournalId] = dbo.General_Journal_Header.numJournal_Id 
--			AND [COA].[numDomainId] = General_Journal_Header.[numDomainId]
--			AND [COA].[numAccountId] <> dbo.Chart_Of_Accounts.numAccountId FOR XML PATH('')) ,1,2, '') AS [vcSplitAccountName]
[dbo].[General_Journal_Details].numEntryDateSortOrder1
FROM dbo.General_Journal_Header 
LEFT JOIN dbo.General_Journal_Details ON dbo.General_Journal_Header.numJournal_Id = dbo.General_Journal_Details.numJournalId 
LEFT JOIN dbo.General_Journal_Details GJD ON dbo.General_Journal_Header.numJournal_Id = GJD.numJournalId 
INNER JOIN dbo.Chart_Of_Accounts ON dbo.General_Journal_Details.numChartAcntId = dbo.Chart_Of_Accounts.numAccountId AND dbo.General_Journal_Details.[numChartAcntId] <> GJD.[numChartAcntId]
OUTER APPLY (SELECT vcAccountName,[COA].[numAccountId] FROM [dbo].[Chart_Of_Accounts] AS COA 
			WHERE dbo.Chart_Of_Accounts.numAccountId = COA.[numAccountId]
			AND [COA].[numDomainId] = General_Journal_Header.[numDomainId]
			AND dbo.General_Journal_Details.[numChartAcntId] = [COA].[numAccountId]) TABLE1
OUTER APPLY (SELECT vcAccountName,[COA1].[numAccountId] FROM [dbo].[Chart_Of_Accounts] AS COA1 
			WHERE dbo.Chart_Of_Accounts.numAccountId <> COA1.[numAccountId]
			AND [COA1].[numDomainId] = General_Journal_Header.[numDomainId]
			AND [GJD].[numChartAcntId] = [COA1].[numAccountId]) TABLE2 
LEFT OUTER JOIN dbo.TimeAndExpense ON dbo.General_Journal_Header.numCategoryHDRID = dbo.TimeAndExpense.numCategoryHDRID 
LEFT OUTER JOIN dbo.DivisionMaster ON dbo.General_Journal_Details.numCustomerId = dbo.DivisionMaster.numDivisionID and 
dbo.General_Journal_Details.numDomainID = dbo.DivisionMaster.numDomainID
LEFT OUTER JOIN dbo.CompanyInfo ON dbo.DivisionMaster.numCompanyID = dbo.CompanyInfo.numCompanyId 
LEFT OUTER JOIN dbo.OpportunityMaster ON dbo.General_Journal_Header.numOppId = dbo.OpportunityMaster.numOppId LEFT OUTER JOIN
dbo.OpportunityBizDocs ON dbo.General_Journal_Header.numOppBizDocsId = dbo.OpportunityBizDocs.numOppBizDocsId LEFT OUTER JOIN
dbo.VIEW_JOURNALCHEQ ON (dbo.General_Journal_Header.numCheckHeaderID = dbo.VIEW_JOURNALCHEQ.numCheckHeaderID 
or (dbo.General_Journal_Header.numBillPaymentID=dbo.VIEW_JOURNALCHEQ.numReferenceID and dbo.VIEW_JOURNALCHEQ.tintReferenceType=8))LEFT OUTER JOIN
dbo.VIEW_BIZPAYMENT ON dbo.General_Journal_Header.numBizDocsPaymentDetId = dbo.VIEW_BIZPAYMENT.numBizDocsPaymentDetId LEFT OUTER JOIN
dbo.DepositMaster DM ON DM.numDepositId = dbo.General_Journal_Header.numDepositId  LEFT OUTER JOIN
dbo.ReturnHeader RH ON RH.numReturnHeaderID = dbo.General_Journal_Header.numReturnID LEFT OUTER JOIN
dbo.CheckHeader CH ON CH.numReferenceID=RH.numReturnHeaderID AND CH.tintReferenceType=10
LEFT OUTER JOIN BillHeader BH  ON ISNULL(General_Journal_Header.numBillId,0) = BH.numBillId
WHERE   dbo.General_Journal_Header.datEntry_Date BETWEEN  @dtFromDate AND @dtToDate -- Removed Tmezone bug id 1029 
AND dbo.General_Journal_Header.numDomainId = @numDomainID 
AND (dbo.DivisionMaster.numDivisionID = @numDivisionID OR @numDivisionID = 0)
AND ([dbo].[General_Journal_Details].bitCleared = CASE @charReconFilter WHEN 'C' THEN 1 ELSE 0 END  OR RTRIM(@charReconFilter)='A')
AND ([dbo].[General_Journal_Details].bitReconcile = CASE @charReconFilter WHEN 'R' THEN 1 ELSE 0 END  OR RTRIM(@charReconFilter)='A') 
--AND (dbo.Chart_Of_Accounts.numAccountId =@vcAccountId)
--AND (dbo.Chart_Of_Accounts. IN ( SELECT numAccountId FROM #Temp) OR  @tintMode=1)
--AND (dbo.Chart_Of_Accounts.numAccountId IN (SELECT [SS].[OutParam] FROM [dbo].[SplitString](@vcAccountId,',') AS SS))
AND (General_Journal_Details.numItemID = @numItemID OR @numItemID = 0)
--AND numJournal_Id = 181544
   AND dbo.General_Journal_Details.numEntryDateSortOrder1  >= @first_id              
--AND dbo.General_Journal_Details.numTransactionId >= @first_id
--ORDER BY dbo.Chart_Of_Accounts.numAccountId ASC,   dbo.General_Journal_Details.numTransactionId asc 
) TABLE1 WHERE ISNULL(TransactionType,'') = 'Journal'
ORDER BY TABLE1.numEntryDateSortOrder1 ASC

SET ROWCOUNT 0

END
	
IF object_id('TEMPDB.DBO.#Temp1') IS NOT NULL drop table #Temp1	
END


--    (
--      @numDomainID INT ,
--      @dtFromDate DATETIME ,
--      @dtToDate DATETIME ,
--      @ClientTimeZoneOffset INT ,  --To enable calculation of date according to client machine
--      @charReconFilter CHAR(1) = '' ,
--      @CurrentPage INT = 0 ,
--      @PageSize INT = 0
--    )
--    WITH RECOMPILE -- To avoid parameter sniffing
--AS
--    BEGIN
		
--		DECLARE @vcAccountIDs AS VARCHAR(MAX)
--		SELECT @vcAccountIDs = COALESCE(@vcAccountIDs + ',', '') + CAST([COA].[numAccountId] AS VARCHAR(20))
--		FROM [dbo].[Chart_Of_Accounts] AS COA 
--		WHERE [COA].[numDomainId] = @numDomainID
--		PRINT @vcAccountIDs

--        DECLARE @dtFinFromDate DATETIME;
--        SET @dtFinFromDate = ( SELECT   dtPeriodFrom
--                               FROM     FINANCIALYEAR
--                               WHERE    dtPeriodFrom <= @dtFromDate
--                                        AND dtPeriodTo >= @dtFromDate
--                                        AND numDomainId = @numDomainId
--                             );

--        PRINT @dtFromDate
--        PRINT @dtToDate
	 
--        DECLARE @first_id NUMERIC ,
--            @startRow INT
--        DECLARE @CurrRecord AS INT


--        BEGIN
--			--SELECT  @numDomainID AS numDomainID, numAccountId,vcAccountName, numParntAcntTypeID,	vcAccountDescription,	vcAccountCode,	dtAsOnDate,	
--			--mnOpeningBalance  [mnOpeningBalance],
--			--							(SELECT ISNULL(SUM(GJD.numDebitAmt),0) - ISNULL(SUM(GJD.numCreditAmt),0)
--			--							FROM
--			--							General_Journal_Details GJD 
--			--							JOIN Domain D ON D.[numDomainId] = GJD.numDomainID AND D.[numDomainId] = @numDomainID
--			--							INNER JOIN General_Journal_Header GJH ON GJD.numJournalId = GJH.numJournal_Id AND 
--			--							GJD.numDomainId = @numDomainID AND 
--			--							GJD.numChartAcntId = [Table1].numAccountId AND 
--			--							GJH.numDomainID = D.[numDomainId] AND 
--			--							GJH.datEntry_Date <= @dtFinFromDate) 
--			--FROM dbo.fn_GetOpeningBalance(@vcAccountID,@numDomainID,@dtFromDate,@ClientTimeZoneOffset) [Table1]

--            SET @CurrRecord = ( ( @CurrentPage - 1 ) * @PageSize ) + 1
--            PRINT @CurrRecord
--            SET ROWCOUNT @CurrRecord

--            SELECT  @first_id = dbo.General_Journal_Details.numEntryDateSortOrder1
--            FROM    dbo.General_Journal_Header
--                    INNER JOIN dbo.General_Journal_Details ON dbo.General_Journal_Header.numJournal_Id = dbo.General_Journal_Details.numJournalId
--                    INNER JOIN dbo.Chart_Of_Accounts ON dbo.General_Journal_Details.numChartAcntId = dbo.Chart_Of_Accounts.numAccountId
--                    LEFT OUTER JOIN dbo.TimeAndExpense ON dbo.General_Journal_Header.numCategoryHDRID = dbo.TimeAndExpense.numCategoryHDRID -- LEFT OUTER JOIN
--            WHERE   dbo.General_Journal_Header.datEntry_Date BETWEEN @dtFromDate
--                                                             AND
--                                                              @dtToDate
--                    AND dbo.General_Journal_Header.numDomainId = @numDomainID
--                    AND dbo.General_Journal_Details.numDomainId = @numDomainID
--                    AND dbo.Chart_Of_Accounts.numDomainId = @numDomainID
--                    AND ( bitCleared = CASE @charReconFilter
--                                         WHEN 'C' THEN 1
--                                         ELSE 0
--                                       END
--                          OR RTRIM(@charReconFilter) = 'A'
--                        )
--                    AND ( bitReconcile = CASE @charReconFilter
--                                           WHEN 'R' THEN 1
--                                           ELSE 0
--                                         END
--                          OR RTRIM(@charReconFilter) = 'A'
--                        )
--                    AND ( dbo.Chart_Of_Accounts.numAccountId = [dbo].[General_Journal_Details].[numChartAcntId] )
--                    --AND dbo.General_Journal_Details.numEntryDateSortOrder1 > 0
--            ORDER BY dbo.General_Journal_Details.numEntryDateSortOrder1 ASC

--            PRINT @first_id
--            PRINT ROWCOUNT_BIG()
--            SET ROWCOUNT @PageSize

--            SELECT  dbo.General_Journal_Details.numDomainId ,
--                    dbo.Chart_Of_Accounts.numAccountId ,
--                    dbo.General_Journal_Header.numJournal_Id ,
--                    dbo.[FormatedDateFromDate](dbo.General_Journal_Header.datEntry_Date,
--                                               dbo.General_Journal_Details.numDomainID) Date ,
--                    dbo.General_Journal_Header.varDescription ,
--                    dbo.CompanyInfo.vcCompanyName AS CompanyName ,
--                    dbo.General_Journal_Details.numTransactionId ,
--                    ISNULL(ISNULL(ISNULL(dbo.VIEW_BIZPAYMENT.Narration,
--                                         dbo.General_Journal_Header.varDescription),
--                                  '') + ' '
--                           + ( CASE WHEN ISNULL(dbo.General_Journal_Header.numReturnID,
--                                                0) <> 0
--                                    THEN ' Chek No: '
--                                         + ISNULL(CAST(CH.numCheckNo AS VARCHAR(50)),
--                                                  'No-Cheq')
--                                    ELSE dbo.VIEW_JOURNALCHEQ.Narration
--                               END ), '') AS Narration ,
--                    ISNULL(dbo.General_Journal_Details.vcReference, '') AS TranRef ,
--                    ISNULL(dbo.General_Journal_Details.varDescription, '') AS TranDesc ,
--                    CONVERT(VARCHAR(20), ISNULL(General_Journal_Details.numDebitAmt,
--                                                0)) numDebitAmt ,
--                    CONVERT(VARCHAR(20), ISNULL(General_Journal_Details.numCreditAmt,
--                                                0)) numCreditAmt ,
--                    dbo.Chart_Of_Accounts.vcAccountName ,
--                    '' AS balance ,
--                    CASE WHEN ISNULL(dbo.General_Journal_Header.numCheckHeaderID,
--                                     0) <> 0 THEN +'Checks'
--                         WHEN ISNULL(dbo.General_Journal_Header.numDepositId,
--                                     0) <> 0
--                              AND DM.tintDepositePage = 1 THEN 'Deposit'
--                         WHEN ISNULL(dbo.General_Journal_Header.numDepositId,
--                                     0) <> 0
--                              AND DM.tintDepositePage = 2 THEN 'Receved Pmt'
--                         WHEN ISNULL(dbo.General_Journal_Header.numOppId, 0) <> 0
--                              AND ISNULL(dbo.General_Journal_Header.numOppBizDocsId,
--                                         0) <> 0
--                              AND ISNULL(dbo.General_Journal_Header.numBizDocsPaymentDetId,
--                                         0) = 0
--                              AND dbo.OpportunityMaster.tintOppType = 1
--                         THEN 'BizDocs Invoice'
--                         WHEN ISNULL(dbo.General_Journal_Header.numOppId, 0) <> 0
--                              AND ISNULL(dbo.General_Journal_Header.numOppBizDocsId,
--                                         0) <> 0
--                              AND ISNULL(dbo.General_Journal_Header.numBizDocsPaymentDetId,
--                                         0) = 0
--                              AND dbo.OpportunityMaster.tintOppType = 2
--                         THEN 'BizDocs Purchase'
--                         WHEN ISNULL(dbo.General_Journal_Header.numCategoryHDRID,
--                                     0) <> 0 THEN 'Time And Expenses'
--                         WHEN ISNULL(General_Journal_Header.numBillId, 0) <> 0
--                              AND ISNULL(BH.bitLandedCost, 0) = 1
--                         THEN 'Landed Cost'
--                         WHEN ISNULL(dbo.General_Journal_Header.numBillID, 0) <> 0
--                         THEN 'Bill'
--                         WHEN ISNULL(dbo.General_Journal_Header.numBillPaymentID,
--                                     0) <> 0 THEN 'Pay Bill'
--                         WHEN ISNULL(dbo.General_Journal_Header.numReturnID, 0) <> 0
--                         THEN CASE WHEN RH.tintReturnType = 1
--                                        AND RH.tintReceiveType = 1
--                                   THEN 'Sales Return Refund'
--                                   WHEN RH.tintReturnType = 1
--                                        AND RH.tintReceiveType = 2
--                                   THEN 'Sales Return Credit Memo'
--                                   WHEN RH.tintReturnType = 2
--                                        AND RH.tintReceiveType = 2
--                                   THEN 'Purchase Return Credit Memo'
--                                   WHEN RH.tintReturnType = 3
--                                        AND RH.tintReceiveType = 2
--                                   THEN 'Credit Memo'
--                                   WHEN RH.tintReturnType = 4
--                                        AND RH.tintReceiveType = 1
--                                   THEN 'Refund Against Credit Memo'
--                              END
--                         WHEN ISNULL(dbo.General_Journal_Header.numOppId, 0) <> 0
--                              AND ISNULL(dbo.General_Journal_Header.numOppBizDocsId,
--                                         0) = 0 THEN 'PO Fulfillment'
--                         WHEN dbo.General_Journal_Header.numJournal_Id <> 0
--                         THEN 'Journal'
--                    END AS TransactionType ,
--                    ISNULL(dbo.General_Journal_Details.bitCleared, 0) AS bitCleared ,
--                    ISNULL(dbo.General_Journal_Details.bitReconcile, 0) AS bitReconcile ,
--                    ISNULL(dbo.General_Journal_Details.numItemID, 0) AS numItemID ,
--                    dbo.General_Journal_Details.numEntryDateSortOrder1 AS numEntryDateSortOrder ,
--					(SELECT mnOpeningBalance FROM dbo.fn_GetOpeningBalance(dbo.Chart_Of_Accounts.numAccountId,@numDomainID,@dtFromDate,@ClientTimeZoneOffset) AS TABLE1
--					WHERE TABLE1.[numAccountId] = dbo.Chart_Of_Accounts.numAccountId) [mnOpeningBalance],
--                    CASE WHEN ISNULL(BH.bitLandedCost, 0) = 1
--                         THEN ISNULL(BH.numOppId, 0)
--                         ELSE 0
--                    END AS numLandedCostOppId
--            FROM    dbo.General_Journal_Header
--                    INNER JOIN dbo.General_Journal_Details ON dbo.General_Journal_Header.numJournal_Id = dbo.General_Journal_Details.numJournalId
--                    INNER JOIN dbo.Chart_Of_Accounts ON dbo.General_Journal_Details.numChartAcntId = dbo.Chart_Of_Accounts.numAccountId
--                    LEFT OUTER JOIN dbo.TimeAndExpense ON dbo.General_Journal_Header.numCategoryHDRID = dbo.TimeAndExpense.numCategoryHDRID
--                    LEFT OUTER JOIN dbo.DivisionMaster ON dbo.General_Journal_Details.numCustomerId = dbo.DivisionMaster.numDivisionID
--                                                          AND dbo.General_Journal_Details.numDomainID = dbo.DivisionMaster.numDomainID
--                    LEFT OUTER JOIN dbo.CompanyInfo ON dbo.DivisionMaster.numCompanyID = dbo.CompanyInfo.numCompanyId
--                    LEFT OUTER JOIN dbo.OpportunityMaster ON dbo.General_Journal_Header.numOppId = dbo.OpportunityMaster.numOppId
--                    LEFT OUTER JOIN dbo.OpportunityBizDocs ON dbo.General_Journal_Header.numOppBizDocsId = dbo.OpportunityBizDocs.numOppBizDocsId
--                    LEFT OUTER JOIN dbo.VIEW_JOURNALCHEQ ON ( dbo.General_Journal_Header.numCheckHeaderID = dbo.VIEW_JOURNALCHEQ.numCheckHeaderID
--                                                              OR ( dbo.General_Journal_Header.numBillPaymentID = dbo.VIEW_JOURNALCHEQ.numReferenceID
--                                                              AND dbo.VIEW_JOURNALCHEQ.tintReferenceType = 8
--                                                              )
--                                                            )
--                    LEFT OUTER JOIN dbo.VIEW_BIZPAYMENT ON dbo.General_Journal_Header.numBizDocsPaymentDetId = dbo.VIEW_BIZPAYMENT.numBizDocsPaymentDetId
--                    LEFT OUTER JOIN dbo.DepositMaster DM ON DM.numDepositId = dbo.General_Journal_Header.numDepositId
--                    LEFT OUTER JOIN dbo.ReturnHeader RH ON RH.numReturnHeaderID = dbo.General_Journal_Header.numReturnID
--                    LEFT OUTER JOIN dbo.CheckHeader CH ON CH.numReferenceID = RH.numReturnHeaderID
--                                                          AND CH.tintReferenceType = 10
--                    LEFT OUTER JOIN BillHeader BH ON ISNULL(General_Journal_Header.numBillId,
--                                                            0) = BH.numBillId
--            WHERE   dbo.General_Journal_Header.datEntry_Date BETWEEN @dtFromDate
--                                                             AND
--                                                              @dtToDate
--                    AND dbo.General_Journal_Header.numDomainId = @numDomainID
--                    AND ( bitCleared = CASE @charReconFilter
--                                         WHEN 'C' THEN 1
--                                         ELSE 0
--                                       END
--                          OR RTRIM(@charReconFilter) = 'A'
--                        )
--                    AND ( bitReconcile = CASE @charReconFilter
--                                           WHEN 'R' THEN 1
--                                           ELSE 0
--                                         END
--                          OR RTRIM(@charReconFilter) = 'A'
--                        )
--                    AND ( dbo.Chart_Of_Accounts.numAccountId = [dbo].[General_Journal_Details].[numChartAcntId] )
--                    --AND dbo.General_Journal_Details.numEntryDateSortOrder1 >= @first_id
--            ORDER BY dbo.General_Journal_Details.numEntryDateSortOrder1 ASC

--            SET ROWCOUNT 0

    
--            IF OBJECT_ID('TEMPDB.DBO.#Temp1') IS NOT NULL
--                DROP TABLE #Temp1	
--        END
--    END