/****** Object:  StoredProcedure [dbo].[USP_ProjectsDTLPL]    Script Date: 07/26/2008 16:20:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_SaveProjectID')
DROP PROCEDURE usp_SaveProjectID
GO
CREATE PROCEDURE [dbo].[usp_SaveProjectID]                                   
(                         
@numDomainID numeric(9),                             
@numProId numeric(9)=null  ,    
@vcProjectID VARCHAR(500)                     
)                                    
as                                                             
begin     
	UPDATE ProjectsMaster SET vcProjectID=@vcProjectID WHERE numDomainId=@numDomainID AND numProId=@numProId
END