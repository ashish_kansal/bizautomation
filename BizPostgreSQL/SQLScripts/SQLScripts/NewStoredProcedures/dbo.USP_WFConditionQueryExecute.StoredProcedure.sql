GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_WFConditionQueryExecute')
DROP PROCEDURE USP_WFConditionQueryExecute
GO
CREATE PROCEDURE [dbo].[USP_WFConditionQueryExecute]     
    @numDomainID numeric(18, 0),
    @numRecordID numeric(18, 0),
    @textQuery NTEXT,
    @boolCondition BIT OUTPUT,
    @tintMode AS TINYINT
as                 

IF @tintMode=1 --Condition Check
BEGIN
	DECLARE @TotalRecords AS int
	
	EXEC sp_executesql @textQuery,N'@numDomainID numeric(18, 0),@numRecordID numeric(18,0),@TotalRecords int OUTPUT',
	@numDomainID=@numDomainID,@numRecordID=@numRecordID,@TotalRecords=@TotalRecords OUTPUT

	SET @boolCondition = CASE WHEN @TotalRecords=0 THEN 0 ELSE 1 END	
END
ELSE IF @tintMode=2 --Field Update
BEGIN
	EXEC sp_executesql @textQuery,N'@numDomainID numeric(18, 0),@numRecordID numeric(18,0)',
	@numDomainID=@numDomainID,@numRecordID=@numRecordID

	If CHARINDEX('OpportunityMaster',@textQuery) > 0 AND CHARINDEX('numStatus',@textQuery) > 0
	BEGIN
		DECLARE @numOppType TINYINT
		DECLARE @tintOppStatus TINYINT
		DECLARE @numStatus NUMERIC(18,0)

		SELECT
			@numOppType=tintOppType
			,@tintOppStatus=tintOppStatus
			,@numStatus=@numStatus
		FROM
			OpportunityMaster
		WHERE
			numDomainId=@numDomainID
			AND numOppId=@numRecordID


		IF @numOppType=1 AND @tintOppStatus=1
		BEGIN
			EXEC USP_SalesFulfillmentQueue_Save @numDomainID,0,@numRecordID,@numStatus
		END
	END
END
