GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_WorkScheduleDaysOff_Delete')
DROP PROCEDURE dbo.USP_WorkScheduleDaysOff_Delete
GO
CREATE PROCEDURE [dbo].[USP_WorkScheduleDaysOff_Delete]
(
	@numWorkScheduleID NUMERIC(18,0)
)
AS 
BEGIN
	DELETE FROM WorkScheduleDaysOff WHERE numWorkScheduleID=@numWorkScheduleID
END
GO