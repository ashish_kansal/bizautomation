--exec USP_GetMultiCompany @numDomainID=72,@numSubscriberID=103
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetMultiCompany')
DROP PROCEDURE USP_GetMultiCompany
GO
CREATE PROCEDURE USP_GetMultiCompany
(@numDomainID int,
@numSubscriberID int)
as

begin

create table #MultiComp
(vcDomainCode varchar(100), vcDomainName varchar(250),numDomainID numeric)

INSERT INTO #MultiComp

select DN.vcDomainCode, DN.vcDomainName,DN.numDomainID from
	Domain DN 
	where  			DN.numDomainID=@numDomainID and			
			DN.numSubscriberID=@numSubscriberID

union

select DN.vcDomainCode, DN.vcDomainName,DN.numDomainID from	
	Domain DN ,
	Domain DNA
	where  DN.numSubscriberID=@numSubscriberID and 
			DN.vcDomainCode like DNA.vcDomainCode + '%' and 
			DNA.numSubscriberID=@numSubscriberID and DNA.numParentDomainID=@numDomainID;

			
SELECT * FROM #MultiComp ORDER BY vcDomainCode

	
end