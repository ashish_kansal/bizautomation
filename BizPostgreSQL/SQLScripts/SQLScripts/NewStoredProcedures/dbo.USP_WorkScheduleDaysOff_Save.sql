GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_WorkScheduleDaysOff_Save')
DROP PROCEDURE dbo.USP_WorkScheduleDaysOff_Save
GO
CREATE PROCEDURE [dbo].[USP_WorkScheduleDaysOff_Save]
(
	@numDomainID NUMERIC(18,0)
	,@numUserCntID NUMERIC(18,0)
	,@numWorkScheduleID NUMERIC(18,0)
	,@dtDayOffFrom Date
	,@dtDayOffTo Date
)
AS 
BEGIN
	IF NOT EXISTS (SELECT ID FROM WorkSchedule WHERE numDomainID=@numDomainID AND ID=ISNULL(@numWorkScheduleID,0))
	BEGIN
		INSERT INTO WorkSchedule
		(
			numDomainID
			,numUserCntID
			,numWorkHours
			,numWorkMinutes
			,numProductiveHours
			,numProductiveMinutes
			,tmStartOfDay
			,vcWorkDays
		)
		VALUES
		(
			@numDomainID
			,@numUserCntID
			,0
			,0
			,0
			,0
			,'00:00:00'
			,''
		)

		SET @numWorkScheduleID = SCOPE_IDENTITY()
	END
	
	INSERT INTO WorkScheduleDaysOff
	(
		numWorkScheduleID
		,dtDayOffFrom
		,dtDayOffTo
	)
	VALUES
	(
		@numWorkScheduleID
		,@dtDayOffFrom
		,@dtDayOffTo
	)
END
GO