
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by Priya Sharma (16 July 2018)                    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getbizdocidsfororderstatus')
DROP PROCEDURE usp_getbizdocidsfororderstatus
GO
CREATE PROCEDURE [dbo].[USP_GetBizDocIdsForOrderStatus]
(                                     
	@numOppId as numeric(9)=null
)                        
AS

BEGIN

	SELECT numOppId,
      
       max(CASE WHEN numBizDocId = 287 THEN numBizDocId ELSE 0 END) as Invoice,      
       max(CASE WHEN numBizDocId = 29397 THEN numBizDocId ELSE 0 END) as PackingSlip

	FROM OpportunityBizDocs
	WHERE numOppId = @numOppId
	GROUP BY numOppId
  
                        
END
GO