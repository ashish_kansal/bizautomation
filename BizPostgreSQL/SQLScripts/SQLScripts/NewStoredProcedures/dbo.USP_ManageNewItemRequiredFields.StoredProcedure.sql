GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_ManageNewItemRequiredFields' ) 
    DROP PROCEDURE USP_ManageNewItemRequiredFields
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author: Sandeep Patel
-- Create date: 30 May 2014
-- Description:	Adds default required fields for Field Management -> New Item in administation section
-- =============================================
CREATE PROCEDURE USP_ManageNewItemRequiredFields
	@numDomainID NUMERIC(18,0)
AS
BEGIN

-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

		---- Inserts required field mapping in DycFormConfigurationDetails table so it can not be removed form configuration wizard for new inventory item form
	    IF NOT EXISTS (SELECT * FROM DycFormConfigurationDetails WHERE numFormId = 86 AND numFieldId = 189 AND numDomainId = @numDomainId)
	    BEGIN
			INSERT INTO DycFormConfigurationDetails ([numFormId],[numFieldId],[intColumnNum],[intRowNum],[numDomainId],[numUserCntID],[numRelCntType],[tintPageType],[bitCustom]) VALUES (86,189,1,1,@numDomainId,0,0,2,0)
		END
		IF NOT EXISTS (SELECT * FROM DycFormConfigurationDetails WHERE numFormId = 86 AND numFieldId = 270 AND numDomainId = @numDomainId)
	    BEGIN
			INSERT INTO DycFormConfigurationDetails ([numFormId],[numFieldId],[intColumnNum],[intRowNum],[numDomainId],[numUserCntID],[numRelCntType],[tintPageType],[bitCustom]) VALUES (86,270,1,2,@numDomainId,0,0,2,0)
		END
		IF NOT EXISTS (SELECT * FROM DycFormConfigurationDetails WHERE numFormId = 86 AND numFieldId = 271 AND numDomainId = @numDomainId)
	    BEGIN
			INSERT INTO DycFormConfigurationDetails ([numFormId],[numFieldId],[intColumnNum],[intRowNum],[numDomainId],[numUserCntID],[numRelCntType],[tintPageType],[bitCustom]) VALUES (86,271,2,1,@numDomainId,0,0,2,0)
		END
		IF NOT EXISTS (SELECT * FROM DycFormConfigurationDetails WHERE numFormId = 86 AND numFieldId = 272 AND numDomainId = @numDomainId)
	    BEGIN
			INSERT INTO DycFormConfigurationDetails ([numFormId],[numFieldId],[intColumnNum],[intRowNum],[numDomainId],[numUserCntID],[numRelCntType],[tintPageType],[bitCustom]) VALUES (86,272,2,2,@numDomainId,0,0,2,0)
		END
		
		--- Inserts required field mapping in DynamicFormField_Validation for validation on form new inventory item form
		IF NOT EXISTS (SELECT * FROM DynamicFormField_Validation WHERE numFormId = 86 AND numFormFieldId = 189 AND numDomainId = @numDomainId)
		BEGIN
			INSERT INTO DynamicFormField_Validation (numFormId,numFormFieldId,numDomainId,vcNewFormFieldName,bitIsRequired) VALUES (86,189,@numDomainId,'Item',1)
		END
		IF NOT EXISTS (SELECT * FROM DynamicFormField_Validation WHERE numFormId = 86 AND numFormFieldId = 270 AND numDomainId = @numDomainId)
		BEGIN
			INSERT INTO DynamicFormField_Validation (numFormId,numFormFieldId,numDomainId,vcNewFormFieldName,bitIsRequired) VALUES (86,270,@numDomainId,'Income Account',1)
		END
		IF NOT EXISTS (SELECT * FROM DynamicFormField_Validation WHERE numFormId = 86 AND numFormFieldId = 271 AND numDomainId = @numDomainId)
		BEGIN
			INSERT INTO DynamicFormField_Validation (numFormId,numFormFieldId,numDomainId,vcNewFormFieldName,bitIsRequired) VALUES (86,271,@numDomainId,'Asset Account',1)
		END
		IF NOT EXISTS (SELECT * FROM DynamicFormField_Validation WHERE numFormId = 86 AND numFormFieldId = 272 AND numDomainId = @numDomainId)
		BEGIN
			INSERT INTO DynamicFormField_Validation (numFormId,numFormFieldId,numDomainId,vcNewFormFieldName,bitIsRequired) VALUES (86,272,@numDomainId,'COGS Account',1)
		END
		
		---- Inserts required field mapping in DycFormConfigurationDetails table so it can not be removed form configuration wizard for new non inventory item form
		IF NOT EXISTS (SELECT * FROM DycFormConfigurationDetails WHERE numFormId = 87 AND numFieldId = 189 AND numDomainId = @numDomainId)
	    BEGIN
			INSERT INTO DycFormConfigurationDetails ([numFormId],[numFieldId],[intColumnNum],[intRowNum],[numDomainId],[numUserCntID],[numRelCntType],[tintPageType],[bitCustom]) VALUES (87,189,1,1,@numDomainId,0,0,2,0)
		END
		IF NOT EXISTS (SELECT * FROM DycFormConfigurationDetails WHERE numFormId = 87 AND numFieldId = 270 AND numDomainId = @numDomainId)
	    BEGIN
			INSERT INTO DycFormConfigurationDetails ([numFormId],[numFieldId],[intColumnNum],[intRowNum],[numDomainId],[numUserCntID],[numRelCntType],[tintPageType],[bitCustom]) VALUES (87,270,1,2,@numDomainId,0,0,2,0)
		END
		IF NOT EXISTS (SELECT * FROM DycFormConfigurationDetails WHERE numFormId = 87 AND numFieldId = 272 AND numDomainId = @numDomainId)
			BEGIN
		INSERT INTO DycFormConfigurationDetails ([numFormId],[numFieldId],[intColumnNum],[intRowNum],[numDomainId],[numUserCntID],[numRelCntType],[tintPageType],[bitCustom]) VALUES (87,272,2,1,@numDomainId,0,0,2,0)
		END
		
		--- Inserts resuired field mapping in DynamicFormField_Validation for validation on form
		IF NOT EXISTS (SELECT * FROM DynamicFormField_Validation WHERE numFormId = 87 AND numFormFieldId = 189 AND numDomainId = @numDomainId)
	    BEGIN
			INSERT INTO DynamicFormField_Validation (numFormId,numFormFieldId,numDomainId,vcNewFormFieldName,bitIsRequired) VALUES (87,189,@numDomainId,'Item',1)
		END
		IF NOT EXISTS (SELECT * FROM DynamicFormField_Validation WHERE numFormId = 87 AND numFormFieldId = 270 AND numDomainId = @numDomainId)
	    BEGIN
			INSERT INTO DynamicFormField_Validation (numFormId,numFormFieldId,numDomainId,vcNewFormFieldName,bitIsRequired) VALUES (87,270,@numDomainId,'Income Account',1)
		END
		IF NOT EXISTS (SELECT * FROM DynamicFormField_Validation WHERE numFormId = 87 AND numFormFieldId = 272 AND numDomainId = @numDomainId)
	    BEGIN
			INSERT INTO DynamicFormField_Validation (numFormId,numFormFieldId,numDomainId,vcNewFormFieldName,bitIsRequired) VALUES (87,272,@numDomainId,'COGS Account',1)
		END
		
		 ---- Inserts required field mapping in DycFormConfigurationDetails table so it can not be removed form configuration wizard for new service/lot item form
		 IF NOT EXISTS (SELECT * FROM DycFormConfigurationDetails WHERE numFormId = 88 AND numFieldId = 189 AND numDomainId = @numDomainId)
	    BEGIN
			INSERT INTO DycFormConfigurationDetails ([numFormId],[numFieldId],[intColumnNum],[intRowNum],[numDomainId],[numUserCntID],[numRelCntType],[tintPageType],[bitCustom]) VALUES (88,189,1,1,@numDomainId,0,0,2,0)
		END
		IF NOT EXISTS (SELECT * FROM DycFormConfigurationDetails WHERE numFormId = 88 AND numFieldId = 270 AND numDomainId = @numDomainId)
	    BEGIN
			INSERT INTO DycFormConfigurationDetails ([numFormId],[numFieldId],[intColumnNum],[intRowNum],[numDomainId],[numUserCntID],[numRelCntType],[tintPageType],[bitCustom]) VALUES (88,270,1,2,@numDomainId,0,0,2,0)
		END
		IF NOT EXISTS (SELECT * FROM DycFormConfigurationDetails WHERE numFormId = 88 AND numFieldId = 271 AND numDomainId = @numDomainId)
	    BEGIN
			INSERT INTO DycFormConfigurationDetails ([numFormId],[numFieldId],[intColumnNum],[intRowNum],[numDomainId],[numUserCntID],[numRelCntType],[tintPageType],[bitCustom]) VALUES (88,271,2,1,@numDomainId,0,0,2,0)
		END
		IF NOT EXISTS (SELECT * FROM DycFormConfigurationDetails WHERE numFormId = 88 AND numFieldId = 272 AND numDomainId = @numDomainId)
	    BEGIN
			INSERT INTO DycFormConfigurationDetails ([numFormId],[numFieldId],[intColumnNum],[intRowNum],[numDomainId],[numUserCntID],[numRelCntType],[tintPageType],[bitCustom]) VALUES (88,272,2,2,@numDomainId,0,0,2,0)
		END
		
		--- Inserts required field mapping in DynamicFormField_Validation for validation on form new service lot item form
		IF NOT EXISTS (SELECT * FROM DynamicFormField_Validation WHERE numFormId = 88 AND numFormFieldId = 189 AND numDomainId = @numDomainId)
		BEGIN
			INSERT INTO DynamicFormField_Validation (numFormId,numFormFieldId,numDomainId,vcNewFormFieldName,bitIsRequired) VALUES (88,189,@numDomainId,'Item',1)
		END
		IF NOT EXISTS (SELECT * FROM DynamicFormField_Validation WHERE numFormId = 88 AND numFormFieldId = 270 AND numDomainId = @numDomainId)
		BEGIN
			INSERT INTO DynamicFormField_Validation (numFormId,numFormFieldId,numDomainId,vcNewFormFieldName,bitIsRequired) VALUES (88,270,@numDomainId,'Income Account',1)
		END
		IF NOT EXISTS (SELECT * FROM DynamicFormField_Validation WHERE numFormId = 88 AND numFormFieldId = 271 AND numDomainId = @numDomainId)
		BEGIN
			INSERT INTO DynamicFormField_Validation (numFormId,numFormFieldId,numDomainId,vcNewFormFieldName,bitIsRequired) VALUES (88,271,@numDomainId,'Asset Account',1)
		END
		IF NOT EXISTS (SELECT * FROM DynamicFormField_Validation WHERE numFormId = 88 AND numFormFieldId = 272 AND numDomainId = @numDomainId)
		BEGIN
			INSERT INTO DynamicFormField_Validation (numFormId,numFormFieldId,numDomainId,vcNewFormFieldName,bitIsRequired) VALUES (88,272,@numDomainId,'COGS Account',1)
		END

END
GO