GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ManageSurveyTemplate')
DROP PROCEDURE USP_ManageSurveyTemplate
GO
CREATE PROCEDURE [dbo].[USP_ManageSurveyTemplate]
    @numDomainID NUMERIC,
    @numSurId NUMERIC,
    @numPageType TINYINT, 
    @txtTemplate TEXT,
    @strItems      TEXT  = NULL
AS 

DECLARE @numSurTemplateId NUMERIC;

    IF NOT EXISTS ( SELECT  * FROM  [SurveyTemplate] WHERE   numDomainID = @numDomainID AND numSurId = @numSurId AND [numPageType]=@numPageType) 
        BEGIN
            INSERT  INTO SurveyTemplate
                    (
                      [numDomainID],
                      [numSurId],
                      [txtTemplate],
					  [numPageType]
	              )
            VALUES  (
                      @numDomainID,
                      @numSurId,
                      @txtTemplate,
					  @numPageType
	              )
            Select @numSurTemplateId=SCOPE_IDENTITY()
        END
    ELSE 
        BEGIN
            UPDATE  SurveyTemplate
            SET   [txtTemplate] = @txtTemplate
            WHERE   [numDomainID] = @numDomainID AND [numSurId] = @numSurId AND [numPageType]=@numPageType
            
            SELECT @numSurTemplateId=numSurTemplateId FROM SurveyTemplate
 WHERE [numDomainID] = @numDomainID AND [numSurId] = @numSurId AND [numPageType] = @numPageType

        END

 DELETE FROM [StyleSheetDetails]
  WHERE       [numSurTemplateId] = @numSurTemplateId
  DECLARE  @hDocItem INT
  IF CONVERT(VARCHAR(10),@strItems) <> ''
    BEGIN
      EXEC sp_xml_preparedocument
        @hDocItem OUTPUT ,
        @strItems
      INSERT INTO [StyleSheetDetails]
                 ([numSurTemplateId],
                  [numCssID])
      SELECT @numSurTemplateId,
             X.numCssID
      FROM   (SELECT *
              FROM   OPENXML (@hDocItem, '/NewDataSet/Item', 2)
                        WITH (numCssID NUMERIC(9))) X
      EXEC sp_xml_removedocument
        @hDocItem
   END