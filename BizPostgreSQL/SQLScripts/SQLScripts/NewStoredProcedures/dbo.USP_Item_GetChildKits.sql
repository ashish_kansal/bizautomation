GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_Item_GetChildKits')
DROP PROCEDURE USP_Item_GetChildKits
GO
CREATE PROCEDURE [dbo].[USP_Item_GetChildKits]
	@numDomainID NUMERIC(18,0)
	,@numItemCode NUMERIC(18,0)
AS  
BEGIN
	SELECT 
		I.numItemCode
		,I.vcItemName
	FROM
		ItemDetails ID
	INNER JOIN
	 	Item I
	ON
		ID.numChildItemID = I.numItemCode
	WHERE
		I.numDomainID=@numDomainID
		AND ID.numItemKitID = @numItemCode
END
GO
