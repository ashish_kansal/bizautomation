GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_DeleteFinancialReport' ) 
    DROP PROCEDURE USP_DeleteFinancialReport
GO
CREATE PROC [dbo].[USP_DeleteFinancialReport] 
(@numFRID NUMERIC(18, 0),
@numDomainID numeric
)
AS 
DELETE FROM dbo.FinancialReportDetail WHERE numFRID =@numFRID 
DELETE  FROM [dbo].[FinancialReport]
WHERE   [numFRID] = @numFRID AND numDomainID = @numDomainID 
GO