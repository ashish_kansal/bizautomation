GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_MassSalesFulfillmentWM_GetWarehousePriority')
DROP PROCEDURE USP_MassSalesFulfillmentWM_GetWarehousePriority
GO
CREATE PROCEDURE [dbo].[USP_MassSalesFulfillmentWM_GetWarehousePriority]
(
    @numDomainID NUMERIC(18,0)
	,@numOrderSource NUMERIC(18,0)
	,@tintSourceType TINYINT
	,@numCountryID NUMERIC(18,0)
	,@numStateID NUMERIC(18,0)
)
AS 
BEGIN
	SELECT
		MassSalesFulfillmentWP.numWarehouseID
	FROM
		MassSalesFulfillmentWM
	INNER JOIN
		MassSalesFulfillmentWMState
	ON
		MassSalesFulfillmentWM.ID = MassSalesFulfillmentWMState.numMSFWMID
	INNER JOIN
		MassSalesFulfillmentWP
	ON
		MassSalesFulfillmentWM.ID = MassSalesFulfillmentWP.numMSFWMID
	WHERE
		MassSalesFulfillmentWM.numDomainID=@numDomainID
		AND MassSalesFulfillmentWM.numOrderSource=@numOrderSource
		AND MassSalesFulfillmentWM.tintSourceType=@tintSourceType
		AND MassSalesFulfillmentWM.numCountryID=@numCountryID
		AND MassSalesFulfillmentWMState.numStateID = @numStateID
	ORDER BY
		MassSalesFulfillmentWP.intOrder
END
GO