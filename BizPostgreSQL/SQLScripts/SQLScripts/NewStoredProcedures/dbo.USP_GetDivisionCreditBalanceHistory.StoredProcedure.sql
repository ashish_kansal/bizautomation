GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetDivisionCreditBalanceHistory')
DROP PROCEDURE USP_GetDivisionCreditBalanceHistory
GO
CREATE PROCEDURE [dbo].[USP_GetDivisionCreditBalanceHistory]                      
@numDomainId   NUMERIC(9)  = 0,
@numDivisionID numeric(9)=0,
@tintOppType tinyint
as                      

SELECT opp.numoppid,r.numreturnid,om.vcPOppname,I.vcItemName,opp.numitemcode,
		sum(CBH.monAmount) as monAmount,0 as numOppBizDocsId,NULL as vcBizDocID,CASE om.tintOpptype WHEN 1 THEN 'Sales' WHEN 2 THEN 'Purchase' END AS [type],OM.tintOppType
             FROM   opportunityitems AS opp INNER JOIN [returns] AS r ON opp.numoppid = r.numoppid and opp.numitemcode = r.numitemcode
                    INNER JOIN [opportunitymaster] om ON om.[numoppid] = opp.[numoppid]
                    INNER JOIN [Item] I ON I.numItemCode = opp.numitemcode
					INNER JOIN divisionMaster div on div.numdivisionid=om.numdivisionid        
					INNER JOIN CreditBalanceHistory CBH on CBH.numDivisionID=div.numDivisionID     
             WHERE  om.numDomainId = @numDomainId and om.numDivisionID=@numDivisionID and CBH.numReturnID=r.numReturnID and om.tintOppType=@tintOppType
			 group by opp.numoppid,r.numreturnid,om.vcPOppname,I.vcItemName,opp.numitemcode,om.tintOpptype

Union

	SELECT om.numoppid,Null as numreturnid,om.vcPOppname,Null as vcItemName,Null as numitemcode,
			CBH.monAmount,CBH.numOppBizDocsId,oppBD.vcBizDocID,CASE om.tintOpptype WHEN 1 THEN 'Sales Return' WHEN 2 THEN 'Purchase Return' END AS [type],OM.tintOppType
				 FROM CreditBalanceHistory CBH join OpportunityBizDocs oppBD on  CBH.numOppBizDocsId= oppBD.numOppBizDocsId
                    INNER JOIN [opportunitymaster] om ON om.[numoppid] = oppBD.[numoppid]
					INNER JOIN divisionMaster div on div.numdivisionid=om.numdivisionid        
             WHERE  om.numDomainId = @numDomainId and om.numDivisionID=@numDivisionID
					and CBH.numDomainId = @numDomainId and CBH.numDivisionID=@numDivisionID and om.tintOppType=@tintOppType
UNION 
	SELECT null numoppid,Null as numreturnid,vcMemo,Null as vcItemName,Null as numitemcode,
			CBH.monAmount,NULL numOppBizDocsId,vcReference AS vcBizDocID,'Over Payment Credit' AS [type], 0 tintOppType
	FROM dbo.CreditBalanceHistory CBH WHERE numDomainId=@numDomainId AND numDivisionId = @numDivisionID