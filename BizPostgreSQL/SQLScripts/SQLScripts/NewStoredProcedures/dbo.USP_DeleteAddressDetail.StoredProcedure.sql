GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_DeleteAddressDetail')
DROP PROCEDURE USP_DeleteAddressDetail
GO
CREATE PROCEDURE USP_DeleteAddressDetail
@numDomainID NUMERIC,
@numAddressID NUMERIC
AS 
BEGIN
IF (SELECT bitIsPrimary FROM AddressDetails WHERE numAddressID=@numAddressID) =1
BEGIN
	raiserror('PRIMARY',16,1);
		RETURN ;
END
	DELETE FROM dbo.AddressDetails WHERE numAddressID=@numAddressID AND numDomainID=@numDomainID
	
END