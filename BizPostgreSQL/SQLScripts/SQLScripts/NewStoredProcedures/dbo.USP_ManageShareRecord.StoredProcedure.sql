SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_ManageShareRecord' ) 
    DROP PROCEDURE USP_ManageShareRecord
GO
CREATE PROCEDURE [dbo].[USP_ManageShareRecord]
     @numDomainID Numeric(9),        
	 @numRecordID Numeric(9),      
	 @numModuleID tinyint ,    
	 @strXml   as text ='',
	 @tintMode AS TINYINT=0 
AS 
	IF @tintMode=0 --Select
	BEGIN
		SELECT CAST(A.numContactID AS VARCHAR(18)) +'~0' AS numContactID ,A.vcFirstName+' '+A.vcLastName as vcUserName          
			from UserMaster UM join AdditionalContactsInformation A  on UM.numUserDetailId=A.numContactID          
			where UM.numDomainID=@numDomainID  and UM.intAssociate=1 and UM.numDomainID=A.numDomainID AND A.numContactID NOT IN (SELECT numAssignedTo FROM ShareRecord WHERE numDomainID=@numDomainID AND numModuleID=@numModuleID AND numRecordID=@numRecordID)   
			order by vcUserName
		SELECT CAST(SR.numAssignedTo AS VARCHAR(18)) +'~'+ CAST(ISNULL(SR.numContactType,0) AS VARCHAR(18)) AS numContactID,
				A.vcFirstName+' '+A.vcLastName + CASE WHEN ISNULL(SR.numContactType,0)>0 THEN '(' + dbo.fn_GetListItemName(SR.numContactType) + ')' ELSE '' END as vcUserName
		 FROM ShareRecord SR JOIN UserMaster UM ON UM.numUserDetailId=SR.numAssignedTo 
							 JOIN AdditionalContactsInformation A ON UM.numUserDetailId=A.numContactID 
		 WHERE SR.numDomainID=@numDomainID AND SR.numModuleID=@numModuleID AND SR.numRecordID=@numRecordID
				AND UM.numDomainID=@numDomainID and UM.numDomainID=A.numDomainID
		order by vcUserName
	END
	ELSE IF @tintMode=1 --Insert
	BEGIN
			delete from ShareRecord where numDomainID=@numDomainID and numRecordID= @numRecordID  AND numModuleID=@numModuleID    
			    
			 DECLARE @hDoc1 int                                      
			 EXEC sp_xml_preparedocument @hDoc1 OUTPUT, @strXml                                      

			 insert into ShareRecord(numDomainID,numRecordID,numAssignedTo,numContactType,numModuleID)    
					select @numDomainID,@numRecordID,X.numAssignedTo,X.numContactType,@numModuleID
						from (SELECT *FROM OPENXML (@hDoc1,'/NewDataSet/Table',2)                                      
							WITH (numAssignedTo numeric(9), numContactType numeric(9)))X                                     
			                                      
			 EXEC sp_xml_removedocument @hDoc1
	END

    