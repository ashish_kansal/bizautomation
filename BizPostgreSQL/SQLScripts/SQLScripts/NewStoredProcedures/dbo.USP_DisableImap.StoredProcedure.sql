GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_DisableImap')
DROP PROCEDURE USP_DisableImap
GO
CREATE PROCEDURE USP_DisableImap
    @numUserCntId NUMERIC(9),
    @numDomainId NUMERIC(9)
AS 
    BEGIN

        DECLARE @tintRetryLeft TINYINT

		--give 10 retry
        SELECT  @tintRetryLeft = ISNULL(tintRetryLeft, 10)
        FROM    [ImapUserDetails]
        WHERE   numUserCntId = @numUserCntId
                AND numDomainID = @numDomainId
	
        IF @tintRetryLeft = 0 -- then disable imap account
            BEGIN
                UPDATE  ImapUserDetails
                SET     bitImap = 0,
                        tintRetryLeft = 0,
                        bitNotified=0
                WHERE   numUserCntId = @numUserCntId
                        AND numDomainID = @numDomainId
            END
        ELSE 
            BEGIN
            
                IF @tintRetryLeft > 0 
                    UPDATE  [ImapUserDetails]
                    SET     tintRetryLeft = @tintRetryLeft - 1
                    WHERE   numUserCntId = @numUserCntId
                            AND numDomainID = @numDomainId	
            END
		
	
	
	
    END