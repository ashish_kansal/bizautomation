SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_DeleteReview')
DROP PROCEDURE USP_DeleteReview
GO
CREATE PROCEDURE [dbo].[USP_DeleteReview]   

@numReviewId AS NUMERIC(18,0) 

AS

BEGIN

	IF EXISTS(SELECT * FROM ReviewDetail WHERE numReviewId = @numReviewId)
	BEGIN
		DELETE FROM ReviewDetail WHERE numReviewId = @numReviewId
	END
	
	DELETE FROM Review WHERE numReviewId = @numReviewId

END

--SELECT * FROM Review
--SELECT * FROM ReviewDetail
--UPDATE ReviewDetail SET numContactID = 18 WHERE numReviewDetailId = 4
--INSERT INTO ReviewDetail
--VALUES(12,0,1,20)
--
--INSERT INTO Review
--values(2,19017,'Sample Delete' ,'sample Comment' ,1 ,'255.125.12.1',20,91,1,NULL,0,1)
--
--
