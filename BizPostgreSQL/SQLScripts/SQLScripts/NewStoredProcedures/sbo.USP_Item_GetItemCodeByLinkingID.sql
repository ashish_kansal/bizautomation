SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_Item_GetItemCodeByLinkingID')
DROP PROCEDURE dbo.USP_Item_GetItemCodeByLinkingID
GO
CREATE PROCEDURE [dbo].[USP_Item_GetItemCodeByLinkingID]
(
	@numDomainID NUMERIC(9),
    @tintItemLinkingID TINYINT,
	@vcValue AS VARCHAR(500)
)
AS
BEGIN
	DECLARE @ItemCount INT

	IF @tintItemLinkingID = 2 --SKU
	BEGIN
		SELECT @ItemCount = COUNT(*) FROM Item WHERE numDomainID=@numDomainID AND vcSKU=@vcValue

		IF @ItemCount = 0
		BEGIN
			RAISERROR('ITEM_NOT_FOUND_WITH_GIVEN_SKU',16,1)
		END
		ELSE IF @ItemCount > 1
		BEGIN
			RAISERROR('MULTIPLE_ITEMS_FOUND_WITH_GIVEN_SKU',16,1)
		END
		ELSE
		BEGIN
			SELECT numItemCode FROM Item WHERE numDomainID=@numDomainID AND vcSKU=@vcValue
		END
	END
	ELSE IF @tintItemLinkingID = 3 --UPC
	BEGIN
		SELECT @ItemCount = COUNT(*) FROM Item WHERE numDomainID=@numDomainID AND numBarCodeId=@vcValue

		IF @ItemCount = 0
		BEGIN
			RAISERROR('ITEM_NOT_FOUND_WITH_GIVEN_UPC',16,1)
		END
		ELSE IF @ItemCount > 1
		BEGIN
			RAISERROR('MULTIPLE_ITEMS_NOT_FOUND_WITH_GIVEN_UPC',16,1)
		END
		ELSE
		BEGIN
			SELECT numItemCode FROM Item WHERE numDomainID=@numDomainID AND numBarCodeId=@vcValue
		END
	END
	ELSE IF @tintItemLinkingID = 4 --ModelID
	BEGIN
		SELECT @ItemCount = COUNT(*) FROM Item WHERE numDomainID=@numDomainID AND vcModelID=@vcValue

		IF @ItemCount = 0
		BEGIN
			RAISERROR('ITEM_NOT_FOUND_WITH_GIVEN_ModelID',16,1)
		END
		ELSE IF @ItemCount > 1
		BEGIN
			RAISERROR('MULTIPLE_ITEMS_NOT_FOUND_WITH_GIVEN_ModelID',16,1)
		END
		ELSE
		BEGIN
			SELECT numItemCode FROM Item WHERE numDomainID=@numDomainID AND vcModelID=@vcValue
		END
	END
	ELSE IF @tintItemLinkingID = 5 --SKU (M)
	BEGIN
		SELECT @ItemCount = COUNT(*) FROM WareHouseItems WHERE numDomainID=@numDomainID AND vcWHSKU=@vcValue

		IF @ItemCount = 0
		BEGIN
			RAISERROR('ITEM_NOT_FOUND_WITH_GIVEN_SKU (M)',16,1)
		END
		ELSE IF @ItemCount > 1
		BEGIN
			RAISERROR('MULTIPLE_ITEMS_NOT_FOUND_WITH_GIVEN_SKU (M)',16,1)
		END
		ELSE
		BEGIN
			SELECT numItemID AS numItemCode FROM WareHouseItems WHERE numDomainID=@numDomainID AND vcWHSKU=@vcValue
		END
	END
	ELSE IF @tintItemLinkingID = 6 -- UPC (M)
	BEGIN
		SELECT @ItemCount = COUNT(*) FROM WareHouseItems WHERE numDomainID=@numDomainID AND vcBarCode=@vcValue

		IF @ItemCount = 0
		BEGIN
			RAISERROR('ITEM_NOT_FOUND_WITH_GIVEN_UPC (M)',16,1)
		END
		ELSE IF @ItemCount > 1
		BEGIN
			RAISERROR('MULTIPLE_ITEMS_NOT_FOUND_WITH_GIVEN_UPC (M)',16,1)
		END
		ELSE
		BEGIN
			SELECT numItemID AS numItemCode FROM WareHouseItems WHERE numDomainID=@numDomainID AND vcBarCode=@vcValue
		END
	END
END