SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
                    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_SearchOrders')
DROP PROCEDURE USP_SearchOrders
GO
CREATE PROCEDURE [dbo].[USP_SearchOrders]                             
	@numDomainID NUMERIC (18,0),
	@numUserCntID NUMERIC(18,0),
	@searchText VARCHAR(100),
	@tinOppType TINYINT, 
	@tintUserRightType TINYINT = 0,
	@tintMode TINYINT,
	@tintShipped TINYINT = 2,
	@tintOppStatus TINYINT = 0                                                 
AS
BEGIN
	DECLARE  @PageId  AS TINYINT
	DECLARE  @numFormId  AS INT 
  
	SET @PageId = 0

	DECLARE @tintDecimalPoints TINYINT

	SELECT
		@tintDecimalPoints=ISNULL(tintDecimalPoints,0)
	FROM
		Domain 
	WHERE 
		numDomainId=@numDomainID


	IF @tinOppType = 1 AND @tintOppStatus = 0 -- SALES OPPORTUNITY
	BEGIN           
		SET @numFormId=38          
	END
	ELSE IF @tinOppType = 2 AND @tintOppStatus = 0 -- PURCHASE OPPORTUNITY         
	BEGIN
		SET @numFormId=40           
	END
	ELSE IF @tinOppType = 1 AND @tintOppStatus = 1 -- SALES ORDER
	BEGIN
		SET @numFormId=39
	END
	ELSE IF @tinOppType = 2 AND @tintOppStatus = 1 -- PURCHASE ORDER
	BEGIN
		SET @numFormId=41
	END
	
	
	-- GET GRID COLUMN CONFIGURATION
	DECLARE @TempTable TABLE
	(
		ID INT IDENTITY(1,1), 
		vcField VARCHAR(300),
		vcAssociatedControlType VARCHAR(50),
		vcFieldName VARCHAR(200),
		vcDbColumnName VARCHAR(200),
		vcLookBackTableName VARCHAR(100),
		numListID INT,
		vcListItemType VARCHAR(10),
		Custom BIT,
		tintRow INT
	)

	INSERT INTO @TempTable 
	(
		vcField,
		vcAssociatedControlType,
		vcFieldName,
		vcDbColumnName,
		vcLookBackTableName,
		numListID,
		vcListItemType,
		Custom,
		tintRow
	)
	SELECT 
		CONCAT(vcLookBackTableName,'.',vcOrigDbColumnName),
		vcAssociatedControlType,
		vcFieldName,
		vcOrigDbColumnName,
		vcLookBackTableName,
		numListID,
		vcListItemType,
		0,
		tintRow
	FROM 
		View_DynamicColumns 
	WHERE 
		numDomainID=@numDomainID 
		AND numUserCntID=@numUserCntID 
		AND numFormId=@numFormID 
		AND numRelCntType=@numFormID 
	UNION
    SELECT 
		CONCAT('[Cus_',numFieldID,']'),
		'',
		vcFieldName,
		vcFieldName,
		'',
		0,
		'',
		1,
		tintRow
	FROm 
		View_DynamicCustomColumns 
	WHERE 
		numDomainID=@numDomainID 
		AND numUserCntID=@numUserCntID 
		AND numFormId=@numFormID 
		AND numRelCntType=@numFormID
	ORDER BY 
		tintRow ASC   

	DECLARE @CustomFields AS VARCHAR(1000)
	SELECT @CustomFields = COALESCE(@CustomFields + ',','') + vcField FROM @TempTable WHERE Custom=1

	IF @tintMode = 1 
	BEGIN
		DECLARE @Query AS VARCHAR(MAX)
		SET @Query = 'SELECT OpportunityMaster.numOppID' 

		-- GETS TOP 3 COLUMNS TO DISPLAY FROM SELECTION
		DECLARE @i AS INT = 1
		DECLARE @DisplayColumns AS VARCHAR(1000) = ''

		DECLARE @vcField AS VARCHAR(200)
		DECLARE @vcFieldName AS VARCHAR(200)
		DECLARE @vcDbColumnName AS VARCHAR(200)
		DECLARE @vcAssociatedControlType AS VARCHAR(50)
		DECLARE @numListID AS INT
		DECLARE @vcListItemType AS VARCHAR(10)

		WHILE @i <= 3
		BEGIN
			SELECT @vcField=vcField, @vcDbColumnName = vcDbColumnName, @vcAssociatedControlType=vcAssociatedControlType, @numListID=numListID, @vcFieldName=vcFieldName,@vcListItemType=vcListItemType FROM @TempTable WHERE ID = @i
            
			IF @vcAssociatedControlType = 'SelectBox'
			BEGIN
				IF @vcDbColumnName = 'tintSource'
				BEGIN
				  SET @DisplayColumns = @DisplayColumns + ',ISNULL(dbo.fn_GetOpportunitySourceValue(ISNULL(OpportunityMaster.tintSource,0),ISNULL(OpportunityMaster.tintSourceType,0),OpportunityMaster.numDomainID),''Internal Order'')' + ' [' + @vcDbColumnName + ']'
				END            
				ELSE IF @vcDbColumnName = 'vcInventoryStatus'
				BEGIN
					SET @DisplayColumns = @DisplayColumns + ', ISNULL(dbo.CheckOrderInventoryStatus(OpportunityMaster.numOppID,OpportunityMaster.numDomainID,1),'''') ' + ' [' + @vcDbColumnName + ']'
				END
				ELSE IF @vcDbColumnName = 'numContactId'
				BEGIN
					SET @DisplayColumns = CONCAT(@DisplayColumns,',dbo.fn_GetContactName(',@vcField,') [' + @vcDbColumnName + ']')
				END
				ELSE IF @vcDbColumnName = 'numCampainID'
				BEGIN
					SET @DisplayColumns = @DisplayColumns + ', (SELECT vcCampaignName FROM CampaignMaster WHERE numCampaignID= OpportunityMaster.numCampainID) ' + ' [' + @vcDbColumnName + ']'
				END
				ELSE IF @vcListItemType = 'LI'
				BEGIN
					SET @DisplayColumns = CONCAT(@DisplayColumns + ', ISNULL((SELECT vcData FROM dbo.ListDetails WHERE numListID = ',@numListID,' AND numDomainID =',@numDomainID, ' AND numListItemID =', @vcField, '),'''')',' [' + @vcDbColumnName + ']')
				END
				ELSE IF @vcListItemType = 'U'
				BEGIN
					SET @DisplayColumns = CONCAT(@DisplayColumns,',dbo.fn_GetContactName(',@vcField,') [' + @vcDbColumnName + ']')
				END
				ELSE IF @vcListItemType='PP'
				BEGIN
					SET @DisplayColumns = @DisplayColumns + ', (CASE WHEN ProjectProgress.numOppID >0 then CONCAT(ISNULL(ProjectProgress.intTotalProgress,0),''%'') ELSE dbo.GetListIemName(OpportunityMaster.numPercentageComplete) END) '+' ['+ @vcDbColumnName+']'
				END
				ELSE
				BEGIN
					SET @DisplayColumns = @DisplayColumns + ',' + @vcField + ' [' + @vcDbColumnName + ']' 
				END
			END
			ELSE IF @vcAssociatedControlType = 'DateField'
			BEGIN
				SET @DisplayColumns = @DisplayColumns + ',dbo.FormatedDateFromDate(' + @vcField + ',' + CONVERT(VARCHAR(10),@numDomainId) + ') [' + @vcDbColumnName + ']' 	
			END
			ELSE IF @vcDbColumnName = 'vcOrderedShipped'
			BEGIN
				SET @DisplayColumns = CONCAT(@DisplayColumns,',','dbo.GetOrderShipReceiveStatus(OpportunityMaster.numOppId,1,OpportunityMaster.tintshipped,',@tintDecimalPoints,')',' [',@vcDbColumnName,']')		
			END
			ELSE IF @vcDbColumnName = 'vcOrderedReceived'
			BEGIN
				SET @DisplayColumns = CONCAT(@DisplayColumns,',','dbo.GetOrderShipReceiveStatus(OpportunityMaster.numOppId,2,OpportunityMaster.tintshipped,',@tintDecimalPoints,')',' [', @vcDbColumnName,']')
			END
			ELSE IF @vcField = 'OpportunityMaster.monDealAmount' 
			BEGIN
				SET @DisplayColumns = @DisplayColumns + ',' + '[dbo].[getdealamount](OpportunityMaster.numOppId,Getutcdate(),0)' + ' [' + @vcDbColumnName + ']' 
			END
			ELSE IF @vcField = 'OpportunityBizDocs.monDealAmount' 
			BEGIN
				SET @DisplayColumns = @DisplayColumns + ',' + 'ISNULL((SELECT SUM(ISNULL(monDealAmount,0)) FROM   OpportunityBizDocs BD WHERE  BD.numOppId = OpportunityMaster.numOppId AND ISNULL(bitAuthoritativeBizDocs,0)=1),0)' + ' [' + @vcDbColumnName + ']' 
			END
			ELSE IF @vcField = 'OpportunityBizDocs.monAmountPaid' 
			BEGIN
				SET @DisplayColumns = @DisplayColumns + ',' + 'ISNULL((SELECT SUM(ISNULL(monAmountPaid,0)) FROM   OpportunityBizDocs BD WHERE  BD.numOppId = OpportunityMaster.numOppId AND ISNULL(bitAuthoritativeBizDocs,0)=1),0)' + ' [' + @vcDbColumnName + ']'
			END
			ELSE IF @vcField = 'OpportunityBizDocs.vcBizDocsList' 
			BEGIN	
				SET  @DisplayColumns = @DisplayColumns + ',' + 'ISNULL((SELECT STUFF((SELECT CONCAT('', '', vcBizDocID) FROM OpportunityBizDocs WHERE numOppId=OpportunityMaster.numOppId FOR XML PATH('''')), 1, 1, '''')),'''')' + ' [' + @vcDbColumnName + ']'
			END
			ELSE IF @vcField = 'OpportunityMaster.tintOppStatus' 
			BEGIN
				SET  @DisplayColumns = @DisplayColumns + ',' + '(CASE OpportunityMaster.tintOppStatus WHEN 0 then ''Open'' WHEN 1 THEN ''Closed-Won'' WHEN 2 THEN ''Closed-Lost'' END)' + ' [' + @vcDbColumnName + ']'
			END
            ELSE IF @vcField = 'OpportunityMaster.tintOppType' 
			BEGIN
				SET @DisplayColumns = @DisplayColumns + ',' + '(CASE OpportunityMaster.tintOppType WHEN 1 then ''Sales'' WHEN 2 THEN ''Purchase'' Else ''NA'' END)' + ' [' + @vcDbColumnName + ']'
			END
			ELSE IF  @vcField = 'ShareRecord.numShareWith'
				SET @DisplayColumns = @DisplayColumns + ',' + '(SELECT SUBSTRING(
								(SELECT '','' + A.vcFirstName+'' ''+A.vcLastName + CASE WHEN ISNULL(SR.numContactType,0)>0 THEN ''('' + dbo.fn_GetListItemName(SR.numContactType) + '')'' ELSE '''' END
								FROM ShareRecord SR JOIN UserMaster UM ON UM.numUserDetailId=SR.numAssignedTo 
								JOIN AdditionalContactsInformation A ON UM.numUserDetailId=A.numContactID 
								WHERE SR.numDomainID=OpportunityMaster.numDomainID AND SR.numModuleID=3 AND SR.numRecordID=OpportunityMaster.numOppId
								AND UM.numDomainID=OpportunityMaster.numDomainID and UM.numDomainID=A.numDomainID FOR XML PATH('''')),2,200000))' + ' [' + @vcDbColumnName + ']'
			ELSE
			BEGIN
				SET @DisplayColumns = @DisplayColumns + ',' + @vcField + ' [' + @vcDbColumnName + ']' 
			END

			SET @i = @i + 1
		END


		IF LEN(@DisplayColumns) = 0 OR CHARINDEX('OpportunityMaster.vcPOppName',@DisplayColumns) = 0
		BEGIN
			SET @DisplayColumns = @DisplayColumns + ',OpportunityMaster.vcPOppName '
		END

		DECLARE @strShareRedordWith AS VARCHAR(300);
		SET @strShareRedordWith=CONCAT(' OpportunityMaster.numOppId IN (SELECT SR.numRecordID FROM ShareRecord SR WHERE SR.numDomainID=',@numDomainID,' AND SR.numModuleID=3 AND SR.numAssignedTo=',@numUserCntId,')')

		DECLARE @MasterQuery AS VARCHAR(MAX)
		SET @MasterQuery = CONCAT(' FROM
									OpportunityMaster
								LEFT JOIN
									DivisionMaster 
								ON
									OpportunityMaster.numDivisionId = DivisionMaster.numDivisionID
								LEFT JOIN
									CompanyInfo 
								ON
									DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
								LEFT JOIN
									AdditionalContactsInformation
								ON
									OpportunityMaster.numContactId = AdditionalContactsInformation.numContactId
								LEFT JOIN
									OpportunityAddress
								ON
									OpportunityMaster.numOppId = OpportunityAddress.numOppID
								LEFT JOIN
									OpportunityRecurring
								ON
									OpportunityMaster.numOppId = OpportunityRecurring.numOppId
								LEFT JOIN
									ProjectProgress
								ON
									OpportunityMaster.numOppId = ProjectProgress.numOppId
								LEFT JOIN 
									OpportunityLinking
								ON 
									OpportunityLinking.numChildOppID=OpportunityMaster.numOppId
								LEFT JOIN
									Sales_process_List_Master
								ON
									OpportunityMaster.numBusinessProcessID = Sales_process_List_Master.Slp_Id', 
								(CASE WHEN LEN(@CustomFields) > 0 THEN
								CONCAT(' OUTER APPLY
									(
										SELECT 
											* 
										FROM 
											(
												SELECT  
													CONCAT(''Cus_'',CFW_Fld_Values_Opp.Fld_ID) Fld_ID,
													(
														CASE 
														WHEN (fld_Type=''TextBox'' or fld_Type=''TextArea'')
														THEN ISNULL(Fld_Value,'''')
														WHEN fld_Type=''SelectBox''
														THEN dbo.GetListIemName(Fld_Value)
														WHEN fld_Type=''CheckBox'' 
														THEN (CASE WHEN Fld_Value=0 THEN ''No'' ELSE ''Yes'' END)
														WHEN fld_type = ''CheckBoxList''
														THEN
															(SELECT 
																STUFF((SELECT CONCAT('', '', vcData) 
															FROM 
																ListDetails 
															WHERE 
																numlistitemid IN (SELECT ISNULL(Id,0) FROM dbo.SplitIDs(Fld_Value,'','')) FOR XML PATH('''')), 1, 1, ''''))
														ELSE
															Fld_Value
														END
													) AS Fld_Value 
												FROM 
													CFW_Fld_Values_Opp 
												JOIN
													CFW_Fld_Master
												ON
													CFW_Fld_Values_Opp.Fld_ID = CFW_Fld_Master.Fld_id
												WHERE 
													RecId = OpportunityMaster.numOppID
											) p PIVOT (MAX([Fld_Value]) FOR Fld_ID  IN (',@CustomFields,') ) AS pvt
									) AS TableCustomFields') ELSE '' END),'
								WHERE 
									OpportunityMaster.numDomainID=',@numDomainID,' AND (OpportunityMaster.tintShipped = ',@tintShipped, ' OR ',@tintShipped, ' = 2) AND tintOppStatus=',@tintOppStatus,' AND OpportunityMaster.tintOppType=',@tinOppType)


		SET @Query = @Query + @DisplayColumns + @MasterQuery


		-- GENERATES SEARCH CONDITION FOR ALL SELLECTED COLUMNS
		DECLARE @SearchCondition VARCHAR(MAX)
		SELECT @SearchCondition = COALESCE(@SearchCondition + ' OR ','') +  
		(CASE 
		WHEN vcField = 'OpportunityMaster.tintSource' THEN 'ISNULL(dbo.fn_GetOpportunitySourceValue(ISNULL(OpportunityMaster.tintSource,0),ISNULL(OpportunityMaster.tintSourceType,0),OpportunityMaster.numDomainID),''Internal Order'')'
		WHEN vcField = 'OpportunityMaster.numCampainID' THEN '(SELECT vcCampaignName FROM CampaignMaster WHERE numCampaignID= OpportunityMaster.numCampainID)'
		WHEN vcField = 'OpportunityMaster.vcOrderedShipped' THEN CONCAT('dbo.GetOrderShipReceiveStatus(OpportunityMaster.numOppId,1,OpportunityMaster.tintshipped,',@tintDecimalPoints,')')
		WHEN vcField = 'OpportunityMaster.vcOrderedReceived' THEN CONCAT('dbo.GetOrderShipReceiveStatus(OpportunityMaster.numOppId,2,OpportunityMaster.tintshipped,',@tintDecimalPoints,')')
		WHEN vcField = 'OpportunityMaster.monDealAmount' THEN '[dbo].[getdealamount](OpportunityMaster.numOppId,Getutcdate(),0)'
		WHEN vcField = 'OpportunityBizDocs.monDealAmount' THEN 'ISNULL((SELECT SUM(ISNULL(monDealAmount,0)) FROM OpportunityBizDocs BD WHERE  BD.numOppId = OpportunityMaster.numOppId AND ISNULL(bitAuthoritativeBizDocs,0)=1),0)'
		WHEN vcField = 'OpportunityBizDocs.monAmountPaid' THEN 'ISNULL((SELECT SUM(ISNULL(monAmountPaid,0)) FROM OpportunityBizDocs BD WHERE  BD.numOppId = OpportunityMaster.numOppId AND ISNULL(bitAuthoritativeBizDocs,0)=1),0)'
		WHEN vcField = 'OpportunityBizDocs.vcBizDocsList' THEN 'ISNULL((SELECT STUFF((SELECT CONCAT('', '', vcBizDocID) FROM OpportunityBizDocs WHERE numOppId=OpportunityMaster.numOppId FOR XML PATH('''')), 1, 1, '''')),'''')'
		WHEN vcField = 'OpportunityMaster.tintOppStatus' THEN '(CASE OpportunityMaster.tintOppStatus WHEN 0 then ''Open'' WHEN 1 THEN ''Closed-Won'' WHEN 2 THEN ''Closed-Lost'' END)'
		WHEN vcField = 'OpportunityMaster.tintOppType' THEN '(CASE OpportunityMaster.tintOppType WHEN 1 then ''Sales'' WHEN 2 THEN ''Purchase'' Else ''NA'' END)'
		WHEN vcField = 'ProjectProgress.intTotalProgress' THEN '(CASE WHEN ProjectProgress.numOppID >0 then CONCAT(ISNULL(ProjectProgress.intTotalProgress,0),''%'') ELSE dbo.GetListIemName(OpportunityMaster.numPercentageComplete) END)'
		WHEN vcField = 'OpportunityMaster.numContactId' THEN CONCAT('dbo.fn_GetContactName(', vcField ,')')
		WHEN vcAssociatedControlType = 'SelectBox' AND vcListItemType = 'LI' THEN CONCAT('ISNULL((SELECT vcData FROM dbo.ListDetails WHERE numListID = ',numListID,' AND numDomainID =',@numDomainID, ' AND numListItemID =', vcField, '),'''')')
		WHEN vcAssociatedControlType = 'SelectBox' AND vcListItemType = 'U' THEN CONCAT('dbo.fn_GetContactName(',vcField,')')
		WHEN vcAssociatedControlType = 'DateField' THEN 'dbo.FormatedDateFromDate(' + vcField + ',' + CONVERT(VARCHAR(10),@numDomainId) + ')'
		WHEN vcField = 'ShareRecord.numShareWith' THEN '(SELECT SUBSTRING(
								(SELECT '','' + A.vcFirstName+'' ''+A.vcLastName + CASE WHEN ISNULL(SR.numContactType,0)>0 THEN ''('' + dbo.fn_GetListItemName(SR.numContactType) + '')'' ELSE '''' END
								FROM ShareRecord SR JOIN UserMaster UM ON UM.numUserDetailId=SR.numAssignedTo 
								JOIN AdditionalContactsInformation A ON UM.numUserDetailId=A.numContactID 
								WHERE SR.numDomainID=OpportunityMaster.numDomainID AND SR.numModuleID=3 AND SR.numRecordID=OpportunityMaster.numOppId
								AND UM.numDomainID=OpportunityMaster.numDomainID and UM.numDomainID=A.numDomainID FOR XML PATH('''')),2,200000))'
		ELSE vcField 
		END) 
		+ ' LIKE ''%' + @searchText + '%''' FROM @TempTable WHERE Custom=1 OR CHARINDEX(vcLookBackTableName,@Query) > 0

		SET @Query = @Query + ' AND (' + @SearchCondition  + ')'

		IF @tintUserRightType = 0
		BEGIN
			SET @Query = @Query + ' AND OpportunityMaster.tintOppType = 0'
		END
		ELSE IF @tintUserRightType = 1
		BEGIN
			SET @Query = CONCAT(@Query,' AND (OpportunityMaster.numRecOwner = ',@numUserCntID,' OR OpportunityMaster.numAssignedTo = ',@numUserCntID,' OR ',@strShareRedordWith +')')
		END
		ELSE IF @tintUserRightType = 2
		BEGIN
			SET @Query = CONCAT(@Query,' AND (DivisionMaster.numTerID IN (SELECT numTerritoryID FROM UserTerritory WHERE numUserCntID = ',@numUserCntID,') OR DivisionMaster.numTerID=0 OR OpportunityMaster.numAssignedTo = ',@numUserCntID,' OR ',@strShareRedordWith,')')
		END

		PRINT @Query
		EXEC(@Query)
	END
	ELSE
	BEGIN
		IF (SELECT COUNT(*) FROM @TempTable) = 0
		BEGIN
			INSERT INTO @TempTable
			(
				vcField,
				vcAssociatedControlType,
				vcFieldName,
				vcDbColumnName,
				vcLookBackTableName,
				numListID,
				vcListItemType,
				Custom
			)
			VALUES
			(
				'',
				'Label',
				(CASE WHEN @tinOppType=1 THEN 'Sales Order Name' ELSE 'Purchase Order Name' END),
				'vcPOppName',
				'OpportunityMaster',
				0,
				'',
				0
			)
		END
		
		SELECT TOP 3 * FROM @TempTable
	END

END                  