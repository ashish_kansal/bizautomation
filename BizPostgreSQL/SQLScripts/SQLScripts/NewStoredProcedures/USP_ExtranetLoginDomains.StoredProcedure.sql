/****** Object:  StoredProcedure [dbo].[USP_ExtranetLoginDomains]    Script Date: 16/10/2013  ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_ExtranetLoginDomains' ) 
    DROP PROCEDURE USP_ExtranetLoginDomains
GO
CREATE PROCEDURE [dbo].[USP_ExtranetLoginDomains]
    @Email AS VARCHAR(100),
    @Password AS VARCHAR(100)
AS 
    BEGIN
        SELECT  DISTINCT A.numDomainID,(SELECT vcDomainName FROM dbo.Domain WHERE dbo.Domain.numDomainID = A.numDomainID) AS [vcDomainName]
        FROM    AdditionalContactsInformation A
                JOIN ExtranetAccountsDtl E ON A.numContactID = E.numContactID AND A.numDomainID = E.numDomainID
        WHERE   vcemail = @Email
                AND vcPassword = @Password
                AND tintAccessAllowed = 1
    END