SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_DemandForecast_DeleteOpp')
DROP PROCEDURE dbo.USP_DemandForecast_DeleteOpp
GO
CREATE PROCEDURE [dbo].[USP_DemandForecast_DeleteOpp]
	@vcOppID AS VARCHAR(MAX),
	@numDomainID AS NUMERIC(18,0),
	@numUserCntID AS NUMERIC(18,0)
AS 
BEGIN

BEGIN TRY 
BEGIN TRAN

	DECLARE @TEMPTABLE TABLE
	(
		NUMBER INT,
		numOppID NUMERIC(18,0)
	)

	INSERT INTO @TEMPTABLE
		(
			NUMBER,
			numOppID
		)
	SELECT
		ROW_NUMBER() OVER (ORDER BY Items) AS NUMBER,
		CAST(Items AS NUMERIC(18,0))
	FROM 
		dbo.Split(@vcOppID,',')


	DECLARE @i As INT = 1
	DECLARE @Count AS INT
	DECLARE @numOppID AS NUMERIC(18,0)
	SELECT @Count = COUNT(*) FROM @TEMPTABLE

	WHILE @i <= @Count
	BEGIN

		SELECT @numOppID = numOppID FROM @TEMPTABLE WHERE NUMBER = @i

		EXEC [USP_DeleteOppurtunity]  @numOppId = @numOppID, @numDomainID = @numDomainID,@numUserCntID = @numUserCntID 

		SET @i = @i + 1
	END
COMMIT TRAN 
END TRY 
BEGIN CATCH		
    DECLARE @strMsg VARCHAR(200)
    SET @strMsg = ERROR_MESSAGE()
    IF ( @@TRANCOUNT > 0 ) 
        BEGIN
            RAISERROR ( @strMsg, 16, 1 ) ;
            ROLLBACK TRAN
        END
END CATCH	
	
END
