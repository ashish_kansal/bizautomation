
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_DeleteRedirectConfig')
	DROP PROCEDURE USP_DeleteRedirectConfig
GO

CREATE PROCEDURE [dbo].[USP_DeleteRedirectConfig]
	@numDomainID numeric (18, 0),
	@numSiteID numeric(18, 0),
	@numRedirectConfigID numeric(18, 0) =0
AS

SET NOCOUNT ON

DELETE FROM [dbo].[RedirectConfig]
	   WHERE [numDomainID] = @numDomainID 
	   AND [numSiteID] = @numSiteID 
	   AND [numRedirectConfigID] = @numRedirectConfigID


GO