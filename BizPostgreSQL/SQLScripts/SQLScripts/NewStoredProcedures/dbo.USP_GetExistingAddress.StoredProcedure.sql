GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetExistingAddress')
DROP PROCEDURE USP_GetExistingAddress
GO
CREATE PROCEDURE [dbo].[USP_GetExistingAddress]
@numDomainId as numeric(9),        
@numUserId as numeric(9),        
@numDivisionId as numeric(9) 
--@vcAddress varchar(500) output       
as   

declare @vcBillAddress	varchar(250)
declare @vcShipAddress	varchar(250)
declare @numBillAddress	NUMERIC(18);SET @numBillAddress=0
declare @numShipAddress	NUMERIC(18);SET @numShipAddress=0

          
Begin    
 
 DECLARE @numBillToCountry AS NUMERIC(10)
 DECLARE @numBillToState AS NUMERIC(10)
 DECLARE @vcBillToCity AS VARCHAR(100)
 DECLARE @vcBillToPostal AS VARCHAR(10)
 DECLARE @numShipToCountry AS NUMERIC(10)
 DECLARE @numShipToState AS NUMERIC(10)
 DECLARE @vcShipToCity AS VARCHAR(100)
 DECLARE @vcShipToPostal AS VARCHAR(10)
 DECLARE @numWarehouseID AS NUMERIC(18,0)
 
 SELECT @vcBillAddress = ISNULL(( vcStreet + ',' ), '') + ISNULL(( vcCity + ',' ), '')
        + isnull(dbo.fn_GetState(numState),'') + '||'
        + isnull(dbo.fn_GetListName(numCountry,0),'')  + ISNULL(( vcPostalCode ), ''),
        @numBillAddress=numAddressID,
        @numBillToCountry = ISNULL(numCountry,0) ,
        @numBillToState = ISNULL(numState,0) ,
        @vcBillToCity = ISNULL(vcCity,''),
        @vcBillToPostal = ISNULL(vcPostalCode,'')
 FROM   dbo.AddressDetails
 WHERE  numDomainID = @numDomainID
        AND numRecordID = @numDivisionID
        AND tintAddressOf = 2
        AND tintAddressType = 1
        AND bitIsPrimary = 1

	select @vcShipAddress= isnull((vcStreet + ','),'') + isnull((vcCity + ','),'') + 
			+ isnull(dbo.fn_GetState(numState),'') + '||' 
			+ isnull(dbo.fn_GetListName(numCountry,0),'') + isnull((vcPostalCode),''),
			@numShipAddress=numAddressID,
			@numShipToCountry = ISNULL(numCountry,0) ,
			@numShipToState = ISNULL(numState,0) ,
			@vcShipToCity = ISNULL(vcCity,''),
			@vcShipToPostal = ISNULL(vcPostalCode,'')
			,@numWarehouseID = ISNULL((SELECT TOP 1 numWarehouseID FROM Warehouses WHERE numDomainID=@numDomainID AND numAddressID=numAddressID),0)
			FROM   dbo.AddressDetails
 WHERE  numDomainID = @numDomainID
        AND numRecordID = @numDivisionID
        AND tintAddressOf = 2
        AND tintAddressType = 2
        AND bitIsPrimary = 1
	
	
	--set @vcAddress=(isnull(@vcBillAddress,''+ '||' + '' + '||' + '') + isnull(@vcShipAddress,''+ '||' + '' + '||' + '')); 
	SELECT ISNULL(@vcBillAddress,'||') AS vcBillAddress,
		   @numBillAddress AS numBillAddress,
		   ISNULL(@vcShipAddress,'||') AS vcShipAddress,
		   @numShipAddress AS numShipAddress,
		   @numBillToCountry AS numBillToCountry,
		   @numBillToState AS numBillToState,
		   @vcBillToCity AS vcBillToCity,
		   @vcBillToPostal AS vcBillToPostal,
		   @numShipToCountry AS numShipToCountry,
		   @numShipToState AS numShipToState,
		   @vcShipToCity AS vcShipToCity,
		   @vcShipToPostal AS vcShipToPostal,
		   isnull(dbo.fn_GetState(@numShipToState),'') vcState,
		   dbo.fn_GetComapnyName(@numDivisionId) AS vcCompanyName,
		   @numWarehouseID AS numWarehouseID
		   
End  


