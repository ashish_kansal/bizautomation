SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_CreateCommissionsContacts')
DROP PROCEDURE USP_CreateCommissionsContacts
GO
CREATE PROCEDURE [dbo].[USP_CreateCommissionsContacts]
@numDivisionID as numeric(9)=0,
@numContactID as numeric(9)=0,
@numDomainID AS NUMERIC
AS

  
if not exists(select * from CommissionContacts where numDivisionID=@numDivisionID and numContactID=@numContactID AND numDomainID=@numDomainID)
begin
    insert into CommissionContacts(numDivisionID,numContactID,numDomainID)
	values(@numDivisionID,@numContactID,@numDomainID)
END
	
GO
