GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ScheduledReportsGroupLog_Save')
DROP PROCEDURE dbo.USP_ScheduledReportsGroupLog_Save
GO
CREATE PROCEDURE [dbo].[USP_ScheduledReportsGroupLog_Save]
(
	@numSRGID NUMERIC(18,0)
	,@vcException NVARCHAR(MAX)
)
AS 
BEGIN
	UPDATE ScheduledReportsGroup SET intNotOfTimeTried = ISNULL(intNotOfTimeTried,0) + 1 WHERE ID=@numSRGID

	IF ISNULL((SELECT intNotOfTimeTried FROM ScheduledReportsGroup WHERE ID=@numSRGID),0) = 5
	BEGIN
		DECLARE @numDomainID NUMERIC(18,0)
		SELECT @numDomainID=numDomainID FROM ScheduledReportsGroup WHERE ID=@numSRGID
		EXEC USP_ScheduledReportsGroup_UpdateNextDate @numDomainID,@numSRGID
	END

	INSERT INTO ScheduledReportsGroupLog
	(
		numSRGID
		,dtExecutionDate
		,vcException
	)
	VALUES
	(
		@numSRGID
		,GETUTCDATE()
		,@vcException
	)
END
GO