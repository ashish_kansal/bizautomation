SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_DeleteOrderBasedPromotionRule')
DROP PROCEDURE USP_DeleteOrderBasedPromotionRule
GO
CREATE PROCEDURE [dbo].[USP_DeleteOrderBasedPromotionRule]
	@numDomainID AS NUMERIC(18,0),
	@fltSubTotal AS FLOAT
AS
BEGIN TRY
BEGIN TRANSACTION	
	DECLARE @numProId AS NUMERIC(18,0), @numProOrderBasedID AS NUMERIC(18,0)
	SELECT @numProId = numProid, @numProOrderBasedID = numProOrderBasedID FROM PromotionOfferOrderBased 
	WHERE fltSubTotal = @fltSubTotal AND numDomainId = @numDomainId

	--DELETE FROM PromotionOfferItems WHERE numProId = @numProId AND tintType = 1 AND tintRecordType = 5
	DELETE POI
	FROM PromotionOfferItems POI
	LEFT JOIN PromotionOfferOrderBased POB ON POB.numProItemId = POI.numValue AND POI.numProId = @numProId
	WHERE numProOrderBasedID = @numProOrderBasedID AND tintType = 1 AND tintRecordType = 5

	DELETE FROM PromotionOfferOrderBased WHERE fltSubTotal = @fltSubTotal AND numDomainId = @numDomainId
		
COMMIT
END TRY
BEGIN CATCH
	DECLARE @ErrorMessage NVARCHAR(4000)
	DECLARE @ErrorNumber INT
	DECLARE @ErrorSeverity INT
	DECLARE @ErrorState INT
	DECLARE @ErrorLine INT
	DECLARE @ErrorProcedure NVARCHAR(200)

	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;

	SELECT 
		@ErrorMessage = ERROR_MESSAGE(),
		@ErrorNumber = ERROR_NUMBER(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE(),
		@ErrorLine = ERROR_LINE(),
		@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

	RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);
END CATCH
