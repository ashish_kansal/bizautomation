GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
                                                        
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_OpportunityMaster_GetSOBackOrderItems')
DROP PROCEDURE USP_OpportunityMaster_GetSOBackOrderItems
GO
CREATE PROCEDURE [dbo].[USP_OpportunityMaster_GetSOBackOrderItems]
(
	@numDomainID NUMERIC(18,0)
	,@numOppID NUMERIC(18,0)
)
AS
BEGIN
	IF NOT EXISTS (SELECT * FROM OpportunityMaster WHERE numDomainId=@numDomainID AND numOppId=@numOppID)
	BEGIN
		RAISERROR('INVALID_OPPID',16,1)
		RETURN
	END 

	DECLARE @tintDefaultCost AS NUMERIC(18,0)
	DECLARE @bitReOrderPoint AS BIT
	DECLARE @numReOrderPointOrderStatus AS NUMERIC(18,0)
	DECLARE @tintOppStautsForAutoPOBackOrder AS TINYINT
	DECLARE @tintUnitsRecommendationForAutoPOBackOrder AS TINYINT
	DECLARE @bitIncludeRequisitions AS BIT

	SELECT 
		@tintDefaultCost = ISNULL(numCost,0)
		,@bitReOrderPoint=ISNULL(bitReOrderPoint,0)
		,@numReOrderPointOrderStatus=ISNULL(numReOrderPointOrderStatus,0)
		,@tintOppStautsForAutoPOBackOrder=ISNULL(tintOppStautsForAutoPOBackOrder,0)
		,@tintUnitsRecommendationForAutoPOBackOrder=ISNULL(tintUnitsRecommendationForAutoPOBackOrder,1)
		,@bitIncludeRequisitions=ISNULL(bitIncludeRequisitions,0)
	FROM 
		Domain 
	WHERE 
		numDomainId = @numDomainID


	SELECT
		@numReOrderPointOrderStatus AS numReOrderPointOrderStatus
		,@tintOppStautsForAutoPOBackOrder AS tintOppStautsForAutoPOBackOrder
		,@tintUnitsRecommendationForAutoPOBackOrder AS tintUnitsRecommendationForAutoPOBackOrder

	IF @tintOppStautsForAutoPOBackOrder IN (0,1)
	BEGIN
		SELECT
			OI.numItemCode
			,CONCAT(I.vcItemName, CASE WHEN dbo.fn_GetAttributes(OI.numWarehouseItmsID,0) <> '' THEN CONCAT(' (',dbo.fn_GetAttributes(OI.numWarehouseItmsID,0),')') ELSE '' END) AS vcItemName
			,I.vcModelID
			,(CASE WHEN ISNULL(I.numItemGroup,0) > 0 AND bitMatrix = 0 THEN ISNULL(WI.vcWHSKU,ISNULL(I.vcSKU,''))  ELSE ISNULL(I.vcSKU,'') END) vcSKU
			,V.vcNotes
			,I.charItemType
			,ISNULL((SELECT TOP 1 vcPathForTImage FROM ItemImages WHERE numDomainId=@numDomainID AND numItemCode=I.numItemCode AND bitDefault =1), '') AS vcPathForTImage
			,ISNULL(OI.numWarehouseItmsID,0) AS numWarehouseItemID
			,(CASE 
				WHEN @tintUnitsRecommendationForAutoPOBackOrder=1 --Add �Re-Order Qty� or �Vendor Min Order Qty� (whichever is greater) + �Back Order Qty� - �On Order + Requisitions�
				THEN 
					(CASE WHEN ISNULL(I.fltReorderQty,0) > ISNULL(v.intMinQty,0) THEN ISNULL(I.fltReorderQty,0) ELSE ISNULL(v.intMinQty,0) END) 
					+ ISNULL(WI.numBackOrder,0) - (ISNULL(numOnOrder,0) + (CASE WHEN @bitIncludeRequisitions = 1 THEN ISNULL((SELECT 
																						SUM(numUnitHour) 
																					FROM 
																						OpportunityItems 
																					INNER JOIN 
																						OpportunityMaster 
																					ON 
																						OpportunityItems.numOppID=OpportunityMaster.numOppId 
																					WHERE 
																						OpportunityMaster.numDomainId=@numDomainID 
																						AND OpportunityMaster.tintOppType=2 
																						AND OpportunityMaster.tintOppStatus=0 
																						AND OpportunityItems.numItemCode=OI.numItemCode 
																						AND OpportunityItems.numWarehouseItmsID=OI.numWarehouseItmsID),0) ELSE 0 END))
				WHEN @tintUnitsRecommendationForAutoPOBackOrder=2 --Add �Re-Order Qty� or �Vendor Min Order Qty� or �Back-Order Qty� (whichever is greater) - �On Order + Requisitions�
				THEN 
					(CASE
						WHEN ISNULL(I.fltReorderQty,0) >= ISNULL(v.intMinQty,0) AND ISNULL(I.fltReorderQty,0) >= ISNULL(WI.numBackOrder,0) THEN ISNULL(I.fltReorderQty,0)
						WHEN ISNULL(v.intMinQty,0) >= ISNULL(I.fltReorderQty,0) AND ISNULL(v.intMinQty,0) >= ISNULL(WI.numBackOrder,0) THEN ISNULL(v.intMinQty,0)
						WHEN ISNULL(WI.numBackOrder,0) >= ISNULL(I.fltReorderQty,0) AND ISNULL(WI.numBackOrder,0) >= ISNULL(v.intMinQty,0) THEN ISNULL(WI.numBackOrder,0)
						ELSE ISNULL(I.fltReorderQty,0)
					END) - (ISNULL(numOnOrder,0) + (CASE WHEN @bitIncludeRequisitions = 1 THEN ISNULL((SELECT 
																SUM(numUnitHour) 
															FROM 
																OpportunityItems 
															INNER JOIN 
																OpportunityMaster 
															ON 
																OpportunityItems.numOppID=OpportunityMaster.numOppId 
															WHERE 
																OpportunityMaster.numDomainId=@numDomainID 
																AND OpportunityMaster.tintOppType=2 
																AND OpportunityMaster.tintOppStatus=0 
																AND OpportunityItems.numItemCode=OI.numItemCode 
																AND OpportunityItems.numWarehouseItmsID=OI.numWarehouseItmsID),0) ELSE 0 END))
				WHEN @tintUnitsRecommendationForAutoPOBackOrder=3 --Add �On-Order Qty� � + Requisitions� - �Back Order� + �Re-Order Qty� or �Vendor Min Order Qty� (whichever is greater)
				THEN 
					(ISNULL(numOnOrder,0) + (CASE WHEN @bitIncludeRequisitions = 1 THEN ISNULL((SELECT 
																SUM(numUnitHour) 
															FROM 
																OpportunityItems 
															INNER JOIN 
																OpportunityMaster 
															ON 
																OpportunityItems.numOppID=OpportunityMaster.numOppId 
															WHERE 
																OpportunityMaster.numDomainId=@numDomainID 
																AND OpportunityMaster.tintOppType=2 
																AND OpportunityMaster.tintOppStatus=0 
																AND OpportunityItems.numItemCode=OI.numItemCode 
																AND OpportunityItems.numWarehouseItmsID=OI.numWarehouseItmsID),0) ELSE 0 END)) - ISNULL(WI.numBackOrder,0) + (CASE
					WHEN ISNULL(I.fltReorderQty,0) >= ISNULL(v.intMinQty,0) THEN ISNULL(I.fltReorderQty,0) ELSE ISNULL(v.intMinQty,0) END)
				ELSE ISNULL(WI.numBackOrder,0) + ISNULL(V.intMinQty,0) 
			END) AS numUnitHour
			,ISNULL(WI.numBackOrder,0) AS numBackOrder
			,ISNULL(I.numBaseUnit,0) AS numUOMID
			,dbo.fn_UOMConversion(OI.numUOMId, OI.numItemCode,OM.numDomainId,I.numBaseUnit) AS fltUOMConversionFactor 
			,ISNULL(I.numVendorID, 0) AS numVendorID
			,CASE WHEN @tintDefaultCost = 3 THEN (ISNULL(V.monCost, 0) / dbo.fn_UOMConversion(I.numPurchaseUnit,I.numItemCode,I.numDomainID,I.numBaseUnit)) ELSE ISNULL(I.monAverageCost,0) END AS monCost
			,ISNULL(V.intMinQty,0) AS intMinQty
			,ISNULL(@tintDefaultCost,0) AS tintDefaultCost
			,ISNULL(dbo.fn_UOMConversion(I.numPurchaseUnit,I.numItemCode,I.numDomainID,I.numBaseUnit),1) AS fltUOMConversionFactorForPurchase
		FROM    
			dbo.OpportunityItems OI
			INNER JOIN dbo.OpportunityMaster OM ON OM.numOppId = OI.numOppId
			INNER JOIN dbo.Item I ON I.numItemCode = OI.numItemCode
			INNER JOIN WarehouseItems WI ON OI.numWarehouseItmsID = WI.numWareHouseItemID
			LEFT JOIN Vendor V ON V.numVendorID = I.numVendorID	AND I.numItemCode = V.numItemCode
		WHERE   
			OI.numOppId = @numOppID
			AND OM.numDomainId = @numDomainId
			AND ISNULL(@bitReOrderPoint,0) = 1
			AND (ISNULL((SELECT SUM(WIInner.numOnHand) FROM WarehouseItems WIInner WHERE WIInner.numItemID = WI.numItemID AND WIInner.numWareHouseID=WI.numWareHouseID),0) + ISNULL(WI.numOnOrder,0) <= ISNULL(WI.numReOrder,0))
			AND OM.tintOppType = 1
			AND OM.tintOppStatus=1
			AND ISNULL(I.bitAssembly, 0) = 0
			AND ISNULL(I.bitKitParent, 0) = 0
			AND ISNULL(OI.bitDropship,0) = 0
			AND ISNULL(WI.numBackorder,0) > 0
		UNION
		SELECT
			IChild.numItemCode
			,CONCAT(IChild.vcItemName, CASE WHEN dbo.fn_GetAttributes(WI.numWareHouseItemID,0) <> '' THEN CONCAT(' (',dbo.fn_GetAttributes(WI.numWareHouseItemID,0),')') ELSE '' END) AS vcItemName
			,IChild.vcModelID
			,(CASE WHEN ISNULL(IChild.numItemGroup,0) > 0 AND IChild.bitMatrix = 0 THEN ISNULL(WI.vcWHSKU,ISNULL(IChild.vcSKU,''))  ELSE ISNULL(IChild.vcSKU,'') END) vcSKU
			,V.vcNotes
			,IChild.charItemType
			,ISNULL((SELECT TOP 1 vcPathForTImage FROM ItemImages WHERE numDomainId=@numDomainID AND numItemCode=IChild.numItemCode AND bitDefault =1), '') AS vcPathForTImage
			,ISNULL(WI.numWareHouseItemID,0) AS numWarehouseItemID
			,(CASE 
				WHEN @tintUnitsRecommendationForAutoPOBackOrder=1 --Add �Re-Order Qty� or �Vendor Min Order Qty� (whichever is greater) + �Back Order Qty� - �On Order + Requisitions�
				THEN 
					(CASE WHEN ISNULL(IChild.fltReorderQty,0) > ISNULL(v.intMinQty,0) THEN ISNULL(IChild.fltReorderQty,0) ELSE ISNULL(v.intMinQty,0) END) 
					+ ISNULL(WI.numBackOrder,0) - (ISNULL(numOnOrder,0) + (CASE WHEN @bitIncludeRequisitions = 1 THEN ISNULL((SELECT 
																						SUM(numUnitHour) 
																					FROM 
																						OpportunityItems 
																					INNER JOIN 
																						OpportunityMaster 
																					ON 
																						OpportunityItems.numOppID=OpportunityMaster.numOppId 
																					WHERE 
																						OpportunityMaster.numDomainId=@numDomainID 
																						AND OpportunityMaster.tintOppType=2 
																						AND OpportunityMaster.tintOppStatus=0 
																						AND OpportunityItems.numItemCode=IChild.numItemCode 
																						AND OpportunityItems.numWarehouseItmsID=WI.numWareHouseItemID),0) ELSE 0 END))
				WHEN @tintUnitsRecommendationForAutoPOBackOrder=2 --Add �Re-Order Qty� or �Vendor Min Order Qty� or �Back-Order Qty� (whichever is greater) - �On Order + Requisitions�
				THEN 
					(CASE
						WHEN ISNULL(IChild.fltReorderQty,0) >= ISNULL(v.intMinQty,0) AND ISNULL(IChild.fltReorderQty,0) >= ISNULL(WI.numBackOrder,0) THEN ISNULL(IChild.fltReorderQty,0)
						WHEN ISNULL(v.intMinQty,0) >= ISNULL(IChild.fltReorderQty,0) AND ISNULL(v.intMinQty,0) >= ISNULL(WI.numBackOrder,0) THEN ISNULL(v.intMinQty,0)
						WHEN ISNULL(WI.numBackOrder,0) >= ISNULL(IChild.fltReorderQty,0) AND ISNULL(WI.numBackOrder,0) >= ISNULL(v.intMinQty,0) THEN ISNULL(WI.numBackOrder,0)
						ELSE ISNULL(IChild.fltReorderQty,0)
					END) - (ISNULL(numOnOrder,0) + (CASE WHEN @bitIncludeRequisitions = 1 THEN ISNULL((SELECT 
																SUM(numUnitHour) 
															FROM 
																OpportunityItems 
															INNER JOIN 
																OpportunityMaster 
															ON 
																OpportunityItems.numOppID=OpportunityMaster.numOppId 
															WHERE 
																OpportunityMaster.numDomainId=@numDomainID 
																AND OpportunityMaster.tintOppType=2 
																AND OpportunityMaster.tintOppStatus=0 
																AND OpportunityItems.numItemCode=IChild.numItemCode 
																AND OpportunityItems.numWarehouseItmsID=WI.numWareHouseItemID),0) ELSE 0 END))
				WHEN @tintUnitsRecommendationForAutoPOBackOrder=3 --Add �On-Order Qty� � + Requisitions� - �Back Order� + �Re-Order Qty� or �Vendor Min Order Qty� (whichever is greater)
				THEN 
					(ISNULL(numOnOrder,0) + (CASE WHEN @bitIncludeRequisitions = 1 THEN ISNULL((SELECT 
																SUM(numUnitHour) 
															FROM 
																OpportunityItems 
															INNER JOIN 
																OpportunityMaster 
															ON 
																OpportunityItems.numOppID=OpportunityMaster.numOppId 
															WHERE 
																OpportunityMaster.numDomainId=@numDomainID 
																AND OpportunityMaster.tintOppType=2 
																AND OpportunityMaster.tintOppStatus=0 
																AND OpportunityItems.numItemCode=IChild.numItemCode 
																AND OpportunityItems.numWarehouseItmsID=WI.numWareHouseItemID),0) ELSE 0 END)) - ISNULL(WI.numBackOrder,0) + (CASE
					WHEN ISNULL(IChild.fltReorderQty,0) >= ISNULL(v.intMinQty,0) THEN ISNULL(IChild.fltReorderQty,0) ELSE ISNULL(v.intMinQty,0) END)
				ELSE ISNULL(WI.numBackOrder,0) + ISNULL(V.intMinQty,0) 
			END) AS numUnitHour
			,ISNULL(WI.numBackOrder,0) AS numBackOrder
			,ISNULL(IChild.numBaseUnit,0) AS numUOMID
			,dbo.fn_UOMConversion(OKI.numUOMId, IChild.numItemCode,OM.numDomainId,IChild.numBaseUnit) AS fltUOMConversionFactor 
			,ISNULL(IChild.numVendorID, 0) AS numVendorID
			,CASE WHEN @tintDefaultCost = 3 OR @tintDefaultCost = 2 THEN (ISNULL(V.monCost, 0) / dbo.fn_UOMConversion(IChild.numPurchaseUnit,IChild.numItemCode,IChild.numDomainID,IChild.numBaseUnit)) ELSE ISNULL(IChild.monAverageCost,0) END AS monCost
			,ISNULL(V.intMinQty,0) AS intMinQty
			,ISNULL(@tintDefaultCost,0) AS tintDefaultCost
			,ISNULL(dbo.fn_UOMConversion(IChild.numPurchaseUnit,IChild.numItemCode,IChild.numDomainID,IChild.numBaseUnit),1) AS fltUOMConversionFactorForPurchase
		FROM
			OpportunityItems OI
		INNER JOIN 
			dbo.OpportunityMaster OM 
		ON 
			OM.numOppId = OI.numOppId
		INNER JOIN
			Item I
		ON
			OI.numItemCode = I.numItemCode
		INNER JOIN
			OpportunityKitItems OKI
		ON
			OI.numoppitemtCode = OKI.numOppItemID
		INNER JOIN
			Item IChild
		ON
			OKI.numChildItemID = IChild.numItemCode
			AND ISNULL(IChild.bitKitParent,0) = 0
		INNER JOIN
			Vendor V
		ON
			IChild.numVendorID = V.numVendorID
			AND IChild.numItemCode = V.numItemCode
		INNER JOIN
			WareHouseItems WI
		ON
			OKI.numWareHouseItemId = WI.numWareHouseItemID
		WHERE
			OI.numOppId=@numOppID
			AND OM.tintOppType = 1
			AND OM.tintOppStatus=1
			AND ISNULL(bitDropShip,0) = 0
			AND ISNULL(bitWorkOrder,0) = 0
			AND ISNULL(I.bitKitParent,0) = 1
			AND ISNULL(WI.numBackorder,0) > 0
			AND ISNULL((SELECT SUM(WIInner.numOnHand) FROM WarehouseItems WIInner WHERE WIInner.numItemID = WI.numItemID AND WIInner.numWareHouseID=WI.numWareHouseID),0) + ISNULL(WI.numOnOrder,0) <= ISNULL(WI.numReorder,0)
		UNION
		SELECT
			IChild.numItemCode
			,CONCAT(IChild.vcItemName, CASE WHEN dbo.fn_GetAttributes(WI.numWareHouseItemID,0) <> '' THEN CONCAT(' (',dbo.fn_GetAttributes(WI.numWareHouseItemID,0),')') ELSE '' END) AS vcItemName
			,IChild.vcModelID
			,(CASE WHEN ISNULL(IChild.numItemGroup,0) > 0 AND IChild.bitMatrix = 0 THEN ISNULL(WI.vcWHSKU,ISNULL(IChild.vcSKU,''))  ELSE ISNULL(IChild.vcSKU,'') END) vcSKU
			,V.vcNotes
			,IChild.charItemType
			,ISNULL((SELECT TOP 1 vcPathForTImage FROM ItemImages WHERE numDomainId=@numDomainID AND numItemCode=IChild.numItemCode AND bitDefault =1), '') AS vcPathForTImage
			,ISNULL(WI.numWareHouseItemID,0) AS numWarehouseItemID
			,(CASE 
				WHEN @tintUnitsRecommendationForAutoPOBackOrder=1 --Add �Re-Order Qty� or �Vendor Min Order Qty� (whichever is greater) + �Back Order Qty� - �On Order + Requisitions�
				THEN 
					(CASE WHEN ISNULL(IChild.fltReorderQty,0) > ISNULL(v.intMinQty,0) THEN ISNULL(IChild.fltReorderQty,0) ELSE ISNULL(v.intMinQty,0) END) 
					+ ISNULL(WI.numBackOrder,0) - (ISNULL(numOnOrder,0) + (CASE WHEN @bitIncludeRequisitions = 1 THEN ISNULL((SELECT 
																						SUM(numUnitHour) 
																					FROM 
																						OpportunityItems 
																					INNER JOIN 
																						OpportunityMaster 
																					ON 
																						OpportunityItems.numOppID=OpportunityMaster.numOppId 
																					WHERE 
																						OpportunityMaster.numDomainId=@numDomainID 
																						AND OpportunityMaster.tintOppType=2 
																						AND OpportunityMaster.tintOppStatus=0 
																						AND OpportunityItems.numItemCode=OKCI.numItemID 
																						AND OpportunityItems.numWarehouseItmsID=WI.numWareHouseItemID),0) ELSE 0 END))
				WHEN @tintUnitsRecommendationForAutoPOBackOrder=2 --Add �Re-Order Qty� or �Vendor Min Order Qty� or �Back-Order Qty� (whichever is greater) - �On Order + Requisitions�
				THEN 
					(CASE
						WHEN ISNULL(IChild.fltReorderQty,0) >= ISNULL(v.intMinQty,0) AND ISNULL(IChild.fltReorderQty,0) >= ISNULL(WI.numBackOrder,0) THEN ISNULL(IChild.fltReorderQty,0)
						WHEN ISNULL(v.intMinQty,0) >= ISNULL(IChild.fltReorderQty,0) AND ISNULL(v.intMinQty,0) >= ISNULL(WI.numBackOrder,0) THEN ISNULL(v.intMinQty,0)
						WHEN ISNULL(WI.numBackOrder,0) >= ISNULL(IChild.fltReorderQty,0) AND ISNULL(WI.numBackOrder,0) >= ISNULL(v.intMinQty,0) THEN ISNULL(WI.numBackOrder,0)
						ELSE ISNULL(IChild.fltReorderQty,0)
					END) - (ISNULL(numOnOrder,0) + (CASE WHEN @bitIncludeRequisitions = 1 THEN ISNULL((SELECT 
																SUM(numUnitHour) 
															FROM 
																OpportunityItems 
															INNER JOIN 
																OpportunityMaster 
															ON 
																OpportunityItems.numOppID=OpportunityMaster.numOppId 
															WHERE 
																OpportunityMaster.numDomainId=@numDomainID 
																AND OpportunityMaster.tintOppType=2 
																AND OpportunityMaster.tintOppStatus=0 
																AND OpportunityItems.numItemCode=OKCI.numItemID 
																AND OpportunityItems.numWarehouseItmsID=WI.numWareHouseItemID),0) ELSE 0 END))
				WHEN @tintUnitsRecommendationForAutoPOBackOrder=3 --Add �On-Order Qty� � + Requisitions� - �Back Order� + �Re-Order Qty� or �Vendor Min Order Qty� (whichever is greater)
				THEN 
					(ISNULL(numOnOrder,0) + (CASE WHEN @bitIncludeRequisitions = 1 THEN ISNULL((SELECT 
																SUM(numUnitHour) 
															FROM 
																OpportunityItems 
															INNER JOIN 
																OpportunityMaster 
															ON 
																OpportunityItems.numOppID=OpportunityMaster.numOppId 
															WHERE 
																OpportunityMaster.numDomainId=@numDomainID 
																AND OpportunityMaster.tintOppType=2 
																AND OpportunityMaster.tintOppStatus=0 
																AND OpportunityItems.numItemCode=OKCI.numItemID 
																AND OpportunityItems.numWarehouseItmsID=WI.numWareHouseItemID),0) ELSE 0 END)) - ISNULL(WI.numBackOrder,0) + (CASE
					WHEN ISNULL(IChild.fltReorderQty,0) >= ISNULL(v.intMinQty,0) THEN ISNULL(IChild.fltReorderQty,0) ELSE ISNULL(v.intMinQty,0) END)
				ELSE ISNULL(WI.numBackOrder,0) + ISNULL(V.intMinQty,0) 
			END) AS numUnitHour
			,ISNULL(WI.numBackOrder,0) AS numBackOrder
			,ISNULL(IChild.numBaseUnit,0) AS numUOMID
			,dbo.fn_UOMConversion(OKCI.numUOMId, OKCI.numItemID,OM.numDomainId,IChild.numBaseUnit) AS fltUOMConversionFactor 
			,ISNULL(IChild.numVendorID, 0) AS numVendorID
			,CASE WHEN (@tintDefaultCost = 3 OR @tintDefaultCost=2) THEN (ISNULL(V.monCost, 0) / dbo.fn_UOMConversion(IChild.numPurchaseUnit,IChild.numItemCode,IChild.numDomainID,IChild.numBaseUnit)) ELSE ISNULL(IChild.monAverageCost,0) END AS monCost
			,ISNULL(V.intMinQty,0) AS intMinQty
			,ISNULL(@tintDefaultCost,0) AS tintDefaultCost
			,ISNULL(dbo.fn_UOMConversion(IChild.numPurchaseUnit,IChild.numItemCode,IChild.numDomainID,IChild.numBaseUnit),1) AS fltUOMConversionFactorForPurchase
		FROM
			OpportunityItems OI
		INNER JOIN 
			dbo.OpportunityMaster OM 
		ON 
			OM.numOppId = OI.numOppId
		INNER JOIN
			Item I
		ON
			OI.numItemCode = I.numItemCode
		INNER JOIN
			OpportunityKitItems OKI
		ON
			OI.numoppitemtCode = OKI.numOppItemID
		INNER JOIN
			OpportunityKitChildItems OKCI
		ON
			OKCI.numOppChildItemID = OKI.numOppChildItemID
		INNER JOIN
			Item IChild
		ON
			OKCI.numItemID = IChild.numItemCode
			AND ISNULL(IChild.bitKitParent,0) = 0
		INNER JOIN
			Vendor V
		ON
			IChild.numVendorID = V.numVendorID
			AND IChild.numItemCode = V.numItemCode
		INNER JOIN
			WareHouseItems WI
		ON
			OKCI.numWareHouseItemId = WI.numWareHouseItemID
		WHERE
			OI.numOppId=@numOppID
			AND OM.tintOppType = 1
			AND OM.tintOppStatus=1
			AND ISNULL(bitDropShip,0) = 0
			AND ISNULL(bitWorkOrder,0) = 0
			AND ISNULL(I.bitKitParent,0) = 1
			AND ISNULL(WI.numBackorder,0) > 0
			AND ISNULL((SELECT SUM(WIInner.numOnHand) FROM WarehouseItems WIInner WHERE WIInner.numItemID = WI.numItemID AND WIInner.numWareHouseID=WI.numWareHouseID),0) + ISNULL(WI.numOnOrder,0) <= ISNULL(WI.numReorder,0)
	END
END
