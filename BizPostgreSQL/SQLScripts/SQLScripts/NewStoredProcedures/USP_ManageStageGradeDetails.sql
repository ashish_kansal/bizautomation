
GO
/****** Object:  StoredProcedure [dbo].[usp_UpdateStagePercentageDetails]    Script Date: 09/21/2010 12:00:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
----Created By Gangadhar    
    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ManageStageGradeDetails')
DROP PROCEDURE USP_ManageStageGradeDetails
GO
CREATE PROCEDURE [dbo].[USP_ManageStageGradeDetails]
	@numDomainID as numeric(9)=0,    
	@numStageDetailsGradeId AS NUMERIC(18)=0,
	@numStageDetailsId as numeric(18)=0,                
	@vcAssignedId as VARCHAR(500) =0,           
	@numCreatedBy as numeric(18)=0,
	@numTaskId AS NUMERIC=0,
	@tintMode AS INT=0,
	@vcGradeId AS VARCHAR(100),
	@status AS VARCHAR(100) OUTPUT 
AS
BEGIN
	IF(@tintMode=1)
	BEGIN
		IF((SELECT COUNT(*) FROM tblStageGradeDetails WHERE numDomainId=@numDomainID AND numStageDetailsId=@numStageDetailsId AND numTaskId=@numTaskId AND vcGradeId=@vcGradeId AND numAssigneId IN (SELECT Items FROM
				Split(@vcAssignedId,',') WHERE Items<>''))>0)
		BEGIN
			SET @status='2'
		END
		ELSE 
		BEGIN
			INSERT INTO  tblStageGradeDetails(
				numStageDetailsId, 
				numAssigneId, 
				numTaskId,
				vcGradeId, 
				numDomainId, 
				numCreatedBy, 
				bintCreatedDate, 
				numModifiedBy, 
				bintModifiedDate
			)
			SELECT 
				@numStageDetailsId, 
				CAST(Items AS NUMERIC(18,0)), 
				@numTaskId,
				@vcGradeId, 
				@numDomainID, 
				@numCreatedBy, 
				GETDATE(), 
				@numCreatedBy, 
				GETDATE()
			FROM
				Split(@vcAssignedId,',') WHERE Items<>''
			SET @status='1'
		END
	END
	IF(@tintMode=2)
	BEGIN
		DELETE FROM
			tblStageGradeDetails
		WHERE 
			numStageDetailsGradeId=@numStageDetailsGradeId
		SET @status='3'
	END
	IF(@tintMode=3)
	BEGIN
		SELECT 
			ISNULL(AC.vcFirstName+' '+AC.vcLastName,'-') As vcAssignedId,
			ST.vcTaskName,
			S.vcGradeId,
			S.numStageDetailsId,
			S.numStageDetailsGradeId
		FROM 
			tblStageGradeDetails AS S
		LEFT JOIN
			AdditionalContactsInformation AS AC
		ON
			S.numAssigneId=AC.numContactId
		LEFT JOIN
			StagePercentageDetailsTask AS ST
		ON
			S.numTaskId=ST.numTaskId
		WHERE
			S.numDomainId=@numDomainID AND S.numStageDetailsId=@numStageDetailsId
	END
END