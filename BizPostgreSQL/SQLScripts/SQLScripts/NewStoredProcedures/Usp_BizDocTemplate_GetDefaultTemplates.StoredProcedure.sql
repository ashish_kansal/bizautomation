GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'Usp_BizDocTemplate_GetDefaultTemplates' ) 
    DROP PROCEDURE Usp_BizDocTemplate_GetDefaultTemplates
GO
-- =============================================  
-- Author:  <Author,,Sachin Sadhu>  
-- Create date: <Create Date,,10thMarch2014>  
-- Description: <Description,,To find existing default templates>  
-- =============================================  
CREATE PROCEDURE Usp_BizDocTemplate_GetDefaultTemplates  
(  
   
 @numDomainID NUMERIC,  
 @numBizDocID NUMERIC,  
 @numOppType NUMERIC,   
 @tintTemplateType TINYINT  
   
)  
AS  
BEGIN  
 -- SET NOCOUNT ON added to prevent extra result sets from  
 -- interfering with SELECT statements.  
 SET NOCOUNT ON;  
  
    -- Insert statements for procedure here  
 SELECT 'col1' FROM BizDocTemplate WHERE bitDefault=1 AND numDomainID=@numDomainID AND numBizDocID=@numBizDocID AND numOppType=@numOppType AND  tintTemplateType=@tintTemplateType  
   
   
END  


