GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_InboxOpenEmailByDivision')
DROP PROCEDURE USP_InboxOpenEmailByDivision
GO
CREATE PROCEDURE [dbo].[USP_InboxOpenEmailByDivision]                                                                          
 @numDomainID as numeric(9)=0,                  
 @numDivisionID as numeric(9)=0,
 @bDivision as tinyint=0
                                                                             
AS       
 SELECT DISTINCT
        ( EH.numEmailHstrID ),
        dbo.FormatedDateTimeFromDate(EH.dtReceivedOn, EH.numDomainId) dtReceivedOn,
        vcSubject AS [Subject],
        ISNULL(vcSize, 0) AS vcSize,
        vcFrom + ',' + vcTo AS [From],
        CASE WHEN LEN(CAST(vcBodyText AS VARCHAR(1000))) > 150
             THEN SUBSTRING(vcBodyText, 0, 150) + '...'
             ELSE CAST(vcBodyText AS VARCHAR(1000))
        END AS vcBody
FROM    emailHistory EH join 
dbo.EmailMaster EM ON EM.numEmailID=EH.numEmailID
WHERE   EH.numDomainID = @numDomainID
        AND EH.bitIsRead = 0
        AND EH.tintType = 1 
        AND EM.numContactId IN (SELECT numContactId FROM dbo.AdditionalContactsInformation WHERE numDivisionId = @numDivisionID AND 
(1=Case when @bDivision=1 then 1 else Case when bitPrimaryContact=1 then 1 else 0 end end))
         
