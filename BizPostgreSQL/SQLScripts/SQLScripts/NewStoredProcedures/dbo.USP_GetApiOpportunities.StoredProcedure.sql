GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_GetApiOpportunities' ) 
    DROP PROCEDURE USP_GetApiOpportunities
GO



--created by Joseph
CREATE PROC [dbo].[USP_GetApiOpportunities]
          @numDomainID AS NUMERIC(9),
		  @numWebApiId AS NUMERIC(9),  
		  @Mode TINYINT = 0,	       
		  @vcApiOrderID VARCHAR(50),
		  @dtFromDate DATETIME,
		  @dtToDate DATETIME

AS
  BEGIN

	if(@Mode =0)
		BEGIN
--			SELECT numOppId,bintCreatedDate,vcMarketplaceOrderId FROM opportunityMaster WHERE numDomainId = @numDomainID
--			AND bintCreatedDate  BETWEEN @dtFromDate AND @dtToDate AND tintSourceType = 3 AND tintSource = @numWebApiId

			SELECT OM.numOppId,OM.bintCreatedDate,OM.vcMarketplaceOrderId,IAO.numOppId,IAO.bitIsActive ,OM.vcMarketplaceOrderReportId
			FROM opportunityMaster OM left Join ImportApiOrder IAO ON OM.numOppId = IAO.numOppId

			WHERE OM.numDomainId = @numDomainID	
				AND OM.bintCreatedDate  BETWEEN @dtFromDate AND @dtToDate 
				AND OM.tintSourceType = 3 
				AND OM.tintSource = @numWebApiId
				AND (IAO.bitIsActive IS NULL OR IAO.bitIsActive=0) 

		END


	if(@Mode =1)
		BEGIN
			SELECT numOppId,bintCreatedDate,vcMarketplaceOrderId,vcMarketplaceOrderReportId FROM opportunityMaster WHERE numDomainId = @numDomainID
			AND vcMarketplaceOrderID = @vcApiOrderID AND tintSourceType = 3 AND tintSource = @numWebApiId
		END


END



