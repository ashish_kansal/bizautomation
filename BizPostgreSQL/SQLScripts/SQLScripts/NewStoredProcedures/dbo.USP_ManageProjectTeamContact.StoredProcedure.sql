
GO
/****** Object:  StoredProcedure [dbo].[USP_ManageProjectTeamContact]    Script Date: 09/17/2010 17:35:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ManageProjectTeamContact')
DROP PROCEDURE USP_ManageProjectTeamContact
GO
CREATE PROCEDURE [dbo].[USP_ManageProjectTeamContact]
@numProjectTeamId as numeric(9)=0,
@strContactId as TEXT

as

   BEGIN
        IF @numProjectTeamId > 0 
            BEGIN

				DELETE FROM ProjectTeam WHERE numProjectTeamId =@numProjectTeamId

                DECLARE @hDocItem INT
                IF CONVERT(VARCHAR(10), @strContactId) <> '' 
                    BEGIN
                        EXEC sp_xml_preparedocument @hDocItem OUTPUT,
                            @strContactId
                           INSERT INTo ProjectTeam(numProjectTeamId, numContactId)
                                SELECT  @numProjectTeamId,
										X.numContactId
                                FROM    ( SELECT    *
                                          FROM      OPENXML (@hDocItem, '/NewDataSet/Table1', 2)
                                                    WITH ( numContactId NUMERIC)
                                        ) X
                        EXEC sp_xml_removedocument @hDocItem
                    END
            END
              
      
    END





