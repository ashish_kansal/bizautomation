--Created By Prasanta                                                  
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_ManageFormFieldGroupConfigurarion' ) 
    DROP PROCEDURE USP_ManageFormFieldGroupConfigurarion
GO
CREATE PROCEDURE [dbo].[USP_ManageFormFieldGroupConfigurarion]  
@numFormFieldGroupId numeric=0,
@numFormId numeric=0,                        
@numGroupId numeric=0,                                    
@numDomainId numeric=0,                                    
@vcGroupName varchar(500) = '',               
@tintActionType TINYINT   =0,
@vcOutput as TINYINT OUTPUT
AS
BEGIN
	IF(@tintActionType=1)
	BEGIN
		IF((SELECT COUNT(*) FROM FormFieldGroupConfigurarion where numFormId=@numFormId AND numDomainId=@numDomainId AND vcGroupName=@vcGroupName)>0)
		BEGIN
			SET @vcOutput=2
		END
		ELSE
		BEGIN
			INSERT INTO FormFieldGroupConfigurarion(
				numFormId, 
				numGroupId, 
				vcGroupName, 
				numDomainId
			)VALUES(
				@numFormId,
				0,
				@vcGroupName,
				@numDomainId
			)
			SET @vcOutput=1
		END
	END
	IF(@tintActionType=2)
	BEGIN
		IF((SELECT COUNT(*) FROM FormFieldGroupConfigurarion where numFormId=@numFormId AND numDomainId=@numDomainId AND vcGroupName=@vcGroupName AND numFormFieldGroupId<>@numFormFieldGroupId)>0)
		BEGIN
			SET @vcOutput=2
		END
		ELSE
		BEGIN
			UPDATE 
				FormFieldGroupConfigurarion 
			SET
				vcGroupName=@vcGroupName
			WHERE
				numFormFieldGroupId=@numFormFieldGroupId
			SET @vcOutput=3
		END
	END
	IF(@tintActionType=3)
	BEGIN
		DELETE FROM BizFormWizardMasterConfiguration WHERE ISNULL(numFormFieldGroupId,0)=@numFormFieldGroupId
		DELETE FROM FormFieldGroupConfigurarion WHERE numFormFieldGroupId=@numFormFieldGroupId
		SET @vcOutput=4
	END
	IF(@tintActionType=4)
	BEGIN
		SELECT 
			vcGroupName,
			numFormFieldGroupId
		FROM
			FormFieldGroupConfigurarion
		WHERE 
			numFormId=@numFormId AND numDomainId=@numDomainId 
		ORDER BY 
			numOrder
	END
	IF(@tintActionType=5)
	BEGIN
		UPDATE ST SET ST.numOrder=Row# FROM FormFieldGroupConfigurarion AS ST
		LEFT JOIN
		(SELECT Items,ROW_NUMBER()  OVER(ORDER BY (SELECT 1)) AS Row# FROM Split(@vcGroupName,',')) AS T
		ON ST.numFormFieldGroupId=T.Items
		WHERE ST.numDomainID=@numDomainID AND numFormId=@numFormId AND ST.numFormFieldGroupId=T.Items
	END
END