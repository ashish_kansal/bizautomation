GO
IF EXISTS (SELECT * FROM sysobjects WHERE xtype = 'p' AND NAME = 'USP_CompanyInfo_SearchByName')
    DROP PROCEDURE USP_CompanyInfo_SearchByName
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Sandeep Patel
-- Create date: 11 DEC 2019
-- Description:	Search customer
-- =============================================
CREATE PROCEDURE USP_CompanyInfo_SearchByName
    @numDomainID AS NUMERIC(18,0),
    @str AS VARCHAR(1000),
    @numPageIndex AS INT,
    @numPageSize AS INT,
	@numTotalRecords INT OUTPUT
AS
BEGIN
	SELECT 
		@numTotalRecords = COUNT(*) 
	FROM 
		CompanyInfo
	INNER JOIN
		DivisionMaster 
	ON
		CompanyInfo.numCompanyId=DivisionMaster.numCompanyID
	WHERE
		CompanyInfo.numDomainID = @numDomainID
		AND DivisionMaster.numDomainID = @numDomainID
		AND vcCompanyName LIKE CONCAT('%',@str,'%')

	SELECT
		numDivisionID AS id
		,vcCompanyName AS [text]
	FROM
		CompanyInfo
	INNER JOIN
		DivisionMaster 
	ON
		CompanyInfo.numCompanyId=DivisionMaster.numCompanyID
	WHERE
		CompanyInfo.numDomainID = @numDomainID
		AND DivisionMaster.numDomainID = @numDomainID
		AND vcCompanyName LIKE CONCAT('%',@str,'%')
	ORDER BY
		vcCompanyName
	OFFSET 
		(@numPageIndex * @numPageSize) ROWS 
	FETCH NEXT @numPageSize ROWS ONLY
END
GO