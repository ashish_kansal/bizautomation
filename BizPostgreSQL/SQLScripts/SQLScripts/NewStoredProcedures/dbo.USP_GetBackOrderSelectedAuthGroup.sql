
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by Priya

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getbackorderselectedauthgroup')
DROP PROCEDURE usp_getbackorderselectedauthgroup
GO
CREATE PROCEDURE [dbo].[USP_GetBackOrderSelectedAuthGroup]    
@numDomainID as numeric(9)=0


AS
BEGIN        

SELECT AGB.numGroupID,AGM.vcGroupName
 
 FROM AuthenticationGroupBackOrder AGB
 JOIN AuthenticationGroupMaster AGM
 ON AGB.numGroupID = AGM.numGroupID
 WHERE AGB.numDomainID=@numDomainID 

END
GO
