GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetMutltiCashBank')
DROP PROCEDURE USP_GetMutltiCashBank
GO
Create PROCEDURE [dbo].[USP_GetMutltiCashBank]
(@numParentDomainID  int,
 @dtFromDate datetime,
 @dtToDate datetime,
 @numSubscriberID INT)
as
begin

CREATE TABLE #TempCashBank
(numDomainID numeric,
vcDomainName varchar(200),
vcDomainCode varchar(50),
Debit DECIMAL(20,5),
Credit DECIMAL(20,5),
Total DECIMAL(20,5));


INSERT INTO #TempCashBank 

select DN.numDomainID,DN.vcDomainName,DN.vcDomainCode, Sum(Debit) as Debit, Sum(Credit) as Credit, Sum(Total) as Total
from VIEW_CASHBANKDAILYSUMMARY VARD
INNER JOIN 
(select * from DOMAIN DN where numSubscriberID=@numSubscriberID) DN ON VARD.numDomainID  =  DN.numDomainID 
INNER JOIN (select * from DOMAIN DNV where numSubscriberID=@numSubscriberID) DNV ON VARD.numDomainID = DNV.numDomainID INNER JOIN 
financialyear FY ON FY.numDomainID=DN.numDomainID AND 
FY.dtPeriodFrom <= @dtFromDate and dtPeriodTo >= @dtFromDate 
AND ( DN.numDomainID=@numParentDomainID)
AND datEntry_Date between FY.dtPeriodFrom and @dtFromDate - 1
GROUP BY DN.numDomainID,DN.vcDomainName,DN.vcDomainCode
union
select DN.numDomainID,DN.vcDomainName,DN.vcDomainCode, Sum(Debit) as Debit, Sum(Credit) as Credit, Sum(Total) as Total
from VIEW_CASHBANKDAILYSUMMARY VARD
INNER JOIN 
(select * from DOMAIN DN where numSubscriberID=@numSubscriberID) DN ON VARD.numDomainID  =  DN.numDomainID 
INNER JOIN (select * from DOMAIN DNV where numSubscriberID=@numSubscriberID) DNV ON VARD.numDomainID = DNV.numDomainID
AND (DN.numDomainID = @numParentDomainID  )
AND datEntry_Date between @dtFromDate and @dtToDate  
GROUP BY DN.numDomainID,DN.vcDomainName,DN.vcDomainCode

----------------------------------------
union 
--------------------------
select DN.numDomainID,DN.vcDomainName,DN.vcDomainCode, Sum(Debit) as Debit, Sum(Credit) as Credit, Sum(Total) as Total
from VIEW_CASHBANKDAILYSUMMARY VARD
INNER JOIN 
(select * from DOMAIN DN where numSubscriberID=@numSubscriberID) DN ON VARD.vcDomainCode  like  DN.vcDomainCode + '%'
INNER JOIN (select * from DOMAIN DNV where numSubscriberID=@numSubscriberID) DNV ON VARD.numDomainID = DNV.numDomainID INNER JOIN 
financialyear FY ON FY.numDomainID=DN.numDomainID AND 
FY.dtPeriodFrom <= @dtFromDate and dtPeriodTo >= @dtFromDate 
AND (DN.numParentDomainID = @numParentDomainID )
AND datEntry_Date between FY.dtPeriodFrom and @dtFromDate - 1
GROUP BY DN.numDomainID,DN.vcDomainName,DN.vcDomainCode
union
select DN.numDomainID,DN.vcDomainName,DN.vcDomainCode, Sum(Debit) as Debit, Sum(Credit) as Credit, Sum(Total) as Total
from VIEW_CASHBANKDAILYSUMMARY VARD
INNER JOIN 
(select * from DOMAIN DN where numSubscriberID=@numSubscriberID) DN ON VARD.vcDomainCode  like  DN.vcDomainCode + '%'
INNER JOIN (select * from DOMAIN DNV where numSubscriberID=@numSubscriberID) DNV ON VARD.numDomainID = DNV.numDomainID
AND (DN.numParentDomainID = @numParentDomainID  )
AND datEntry_Date between @dtFromDate and @dtToDate  
GROUP BY DN.numDomainID,DN.vcDomainName,DN.vcDomainCodE


SELECT vcDomainName ,isnull(SUM(isnull(TOTAL,0)),0) as Total  FROM #TempCashBank GROUP BY vcDomainName;

DROP TABLE #TempCashBank;
end
