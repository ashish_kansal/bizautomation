GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
                    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_StagePercentageDetailsTask_ChangeTimeProductionPlanning')
DROP PROCEDURE USP_StagePercentageDetailsTask_ChangeTimeProductionPlanning
GO
CREATE PROCEDURE [dbo].[USP_StagePercentageDetailsTask_ChangeTimeProductionPlanning]                             
	@numDomainID NUMERIC(18,0)                            
	,@numTaskID NUMERIC(18,0)
	,@dtStartTime DATETIME
	,@dtEndTime DATETIME
AS                            
BEGIN
	IF DATEDIFF(MINUTE,@dtStartTime,@dtEndTime) > 0
	BEGIN
		UPDATE
			StagePercentageDetailsTask
		SET
			dtStartTime=@dtStartTime
			,dtEndTime=@dtEndTime
			,numHours = FLOOR(DATEDIFF(MINUTE,@dtStartTime,@dtEndTime) / 60)
			,numMinutes = DATEDIFF(MINUTE,@dtStartTime,@dtEndTime) % 60
		WHERE
			numDomainID = @numDomainID
			AND numTaskId = @numTaskID
	END
	ELSE
	BEGIN
		RAISERROR('INVALID_START_AND_END_TIME',16,1)
	END
END
GO


