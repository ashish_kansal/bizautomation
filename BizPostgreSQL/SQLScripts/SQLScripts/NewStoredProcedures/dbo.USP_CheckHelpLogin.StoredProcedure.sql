
GO
/****** Object:  StoredProcedure [dbo].[USP_CheckHelpLogin]    Script Date: 12/08/2011 15:10:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_CheckHelpLogin')
DROP PROCEDURE USP_CheckHelpLogin
GO

CREATE PROCEDURE [dbo].[USP_CheckHelpLogin]
@numAppAccessId NUMERIC(18,0)
AS
BEGIN 

DECLARE @numDomainId NUMERIC(18,0)
DECLARE @numHour NUMERIC(18,0)
DECLARE @maxHour NUMERIC(18,0)
DECLARE @flag BIT 
 SELECT @numHour =(datediff(minute,dtLoggedInTime,GETUTCDATE())) , @numDomainId = numDomainId  FROM dbo.UserAccessedDTL WHERE numAppAccessID = @numAppAccessId AND dtLoggedOutTime IS NULL 
 SELECT @maxHour = ISNULL(tintSessionTimeOut,1) *60 FROM dbo.Domain WHERE numDomainId=@numDomainId
 PRINT @maxHour;
 PRINT @numHour;
 IF  @numHour <=@maxHour
 BEGIN 
 SET @flag =1
 END 
 ELSE
 BEGIN
 SET @flag = 0
 END 
 
SELECT @flag 


END 
