
GO
/****** Object:  StoredProcedure [dbo].[USP_ManageSiteBreadCumbs]    Script Date: 08/08/2009 16:32:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Anoop Krishnan
-- Create date: 25-JUL-2009
-- Description:	To insert or update bread crumb details
-- =============================================
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ManageSiteBreadCumbs')
DROP PROCEDURE USP_ManageSiteBreadCumbs
GO
CREATE PROCEDURE [dbo].[USP_ManageSiteBreadCumbs]
	@numBreadCrumbID numeric(18) = 0 OUTPUT, 
	@numSiteID numeric(18) = 0, 
	@numDomainID numeric(18) = 0,
	@numPageID numeric(18) = 0,
	@tintLevel tinyint,
	@vcDisplayName varchar(15)
AS
BEGIN
	SET NOCOUNT ON;
	IF(@numBreadCrumbID = 0 AND NOT EXISTS(SELECT [numBreadCrumbID] 
										FROM [SiteBreadCrumb]
										WHERE [vcDisplayName] = @vcDisplayName
											AND [numSiteID] = @numSiteID
											AND [numDomainID] = @numDomainID))
	BEGIN
		INSERT INTO [dbo].[SiteBreadCrumb]
			   ([numPageID]
			   ,[numSiteID]
			   ,[numDomainID]
			   ,[tintLevel]
			   ,[vcDisplayName])
		 VALUES
			   (@numPageID
			   ,@numSiteID
			   ,@numDomainID
			   ,@tintLevel 
			   ,@vcDisplayName)
		SELECT @numBreadCrumbID = SCOPE_IDENTITY()
	END
	ELSE IF NOT EXISTS(SELECT [numBreadCrumbID] 
						FROM [SiteBreadCrumb]
						WHERE [vcDisplayName] = @vcDisplayName
							AND [numSiteID] = @numSiteID
							AND [numDomainID] = @numDomainID)
	BEGIN
		UPDATE [dbo].[SiteBreadCrumb]
		SET [numPageID] = @numPageID 
		  ,[numSiteID] = @numSiteID
		  ,[numDomainID] = @numDomainID
		  ,[tintLevel] = @tintLevel
		  ,[vcDisplayName] = @vcDisplayName
		WHERE [numBreadCrumbID] = @numBreadCrumbID
		SELECT @numBreadCrumbID = SCOPE_IDENTITY()
	END
END
