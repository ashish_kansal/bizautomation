GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_Item_GetDetailForStockTransfer' ) 
    DROP PROCEDURE USP_Item_GetDetailForStockTransfer
GO
CREATE PROCEDURE USP_Item_GetDetailForStockTransfer
    @numDomainId NUMERIC(18,0),
    @numItemCode NUMERIC(18,0),
	@numFromWarehouseItemID NUMERIC(18,0),
	@numToWarehouseItemID NUMERIC(18,0)
AS 
BEGIN
	IF ISNULL(@numFromWarehouseItemID,0) > 0 AND ISNULL(@numToWarehouseItemID,0) > 0
	BEGIN
		SELECT
			Item.numItemCode AS numFromItemCode
			,ISNULL(Item.bitSerialized,0) bitSerialized
			,ISNULL(Item.bitLotNo,0) bitLotNo
			,CONCAT(vcItemName,', ',Warehouses.vcWareHouse,(CASE WHEN LEN(ISNULL(WarehouseLocation.vcLocation,'')) > 0 THEN CONCAT(' (',WarehouseLocation.vcLocation,')') ELSE '' END)) AS vcFrom
			,WareHouseItems.numWareHouseItemID AS numFromWarehouseItemID
			,ISNULL(WareHouseItems.numOnHand,0) numFromOnHand
			,ISNULL(WareHouseItems.numOnHand,0) + ISNULL(WareHouseItems.numAllocation,0) numFromAvailable
			,ISNULL(WareHouseItems.numBackOrder,0) numFromBackOrder
			,WarehouseTo.numItemCode AS numToItemCode
			,WarehouseTo.vcTo
			,WarehouseTo.numWareHouseItemID AS numToWarehouseItemID
			,WarehouseTo.numToAvailable
			,WarehouseTo.numToBackOrder
			,WareHouseItems.numWareHouseID AS numFromWarehouseID
			,WarehouseTo.numWareHouseID AS numToWarehouseID
		FROM
			Item 
		INNER JOIN
			WareHouseItems
		ON
			WareHouseItems.numItemID = Item.numItemCode
			AND WareHouseItems.numWareHouseItemID = @numFromWarehouseItemID
		INNER JOIN
			Warehouses
		ON
			WareHouseItems.numWareHouseID = Warehouses.numWareHouseID
		LEFT JOIN
			WarehouseLocation
		ON
			WareHouseItems.numWLocationID = WarehouseLocation.numWLocationID
		CROSS APPLY
		(
			SELECT 
				numItemCode
				,CONCAT(vcItemName,', ',WTo.vcWareHouse,(CASE WHEN LEN(ISNULL(WLTo.vcLocation,'')) > 0 THEN CONCAT(' (',WLTo.vcLocation,')') ELSE '' END)) AS vcTo
				,WHITo.numWareHouseID
				,WHITo.numWareHouseItemID
				,ISNULL(WHITo.numOnHand,0) + ISNULL(WHITo.numAllocation,0) numToAvailable
				,ISNULL(WHITo.numBackOrder,0) numToBackOrder
			FROM
				WareHouseItems WHITo
			INNER JOIN
				Item ITo
			ON	
				WHITo.numItemID = ITo.numItemCode
			INNER JOIN
				Warehouses WTo
			ON
				WHITo.numWareHouseID = WTo.numWareHouseID
			LEFT JOIN
				WarehouseLocation WLTo
			ON
				WHITo.numWLocationID = WLTo.numWLocationID
			WHERE
				WHITo.numWareHouseItemID=@numToWarehouseItemID
		) WarehouseTo
		WHERE
			Item.numDomainID=@numDomainId
			AND Item.numItemCode=@numItemCode
	END
	ELSE
	BEGIN
		SELECT
			ISNULL(vcSKU,'') vcSKU
			,dbo.fn_GetItemAttributes(@numDomainId,@numItemCode,0) vcAttributes
			,ISNULL(Item.bitSerialized,0) bitSerialized
			,ISNULL(Item.bitLotNo,0) bitLotNo
		FROM
			Item
		WHERE
			numDomainID=@numDomainId
			AND numItemCode=@numItemCode
	END

	
END
GO