
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_DashboardTemplate_Save')
DROP PROCEDURE USP_DashboardTemplate_Save
GO
CREATE PROCEDURE [dbo].[USP_DashboardTemplate_Save]
	@numDomainID NUMERIC(18,0)
	,@numUserCntID NUMERIC(18,0)
	,@bitUpdate BIT
	,@numTemplateID NUMERIC(18,0)
	,@vcTemplateName VARCHAR(300)
AS
BEGIN 
BEGIN TRY
BEGIN TRANSACTION
	IF @bitUpdate = 0
	BEGIN
		DECLARE @NewTemplateID NUMERIC(18,0)

		INSERT INTO DashboardTemplate
		(
			numDomainID
			,vcTemplateName
			,dtCreatedDate
			,numCreatedBy
		)
		VALUES
		(
			@numDomainID
			,@vcTemplateName
			,GETUTCDATE()
			,@numUserCntID
		)

		SET @NewTemplateID=SCOPE_IDENTITY()


		INSERT INTO DashboardTemplateReports
		(
			numDomainID
			,numDashboardTemplateID
			,numReportID
			,tintReportType
			,tintChartType
			,vcHeaderText
			,vcFooterText
			,tintRow
			,tintColumn
			,vcXAxis
			,vcYAxis
			,vcAggType
			,tintReportCategory
			,intHeight
			,intWidth
		)
		SELECT
			numDomainID
			,@NewTemplateID
			,numReportID
			,tintReportType
			,tintChartType
			,vcHeaderText
			,vcFooterText
			,tintRow
			,tintColumn
			,vcXAxis
			,vcYAxis
			,vcAggType
			,tintReportCategory
			,intHeight
			,intWidth
		FROM
			ReportDashboard
		WHERE
			numDomainID=@numDomainID
			AND numUserCntID=@numUserCntID
			AND ISNULL(numDashboardTemplateID,0)=ISNULL(@numTemplateID,0)

		DELETE FROM ReportDashboard WHERE numDomainID=@numDomainID AND ISNULL(numDashboardTemplateID,0)=ISNULL(@numTemplateID,0) AND numUserCntID = @numUserCntID AND ISNULL(bitNewAdded,0)=1
	END
	ELSE
	BEGIN
		UPDATE DashboardTemplate SET dtModifiedDate=GETUTCDATE(),numModifiedBy=@numUserCntID WHERE numTemplateID=@numTemplateID

		DELETE FROM DashboardTemplateReports WHERE numDashboardTemplateID=@numTemplateID

		INSERT INTO DashboardTemplateReports
		(
			numDomainID
			,numDashboardTemplateID
			,numReportID
			,tintReportType
			,tintChartType
			,vcHeaderText
			,vcFooterText
			,tintRow
			,tintColumn
			,vcXAxis
			,vcYAxis
			,vcAggType
			,tintReportCategory
			,intHeight
			,intWidth
		)
		SELECT
			numDomainID
			,numDashboardTemplateID
			,numReportID
			,tintReportType
			,tintChartType
			,vcHeaderText
			,vcFooterText
			,tintRow
			,tintColumn
			,vcXAxis
			,vcYAxis
			,vcAggType
			,tintReportCategory
			,intHeight
			,intWidth
		FROM
			ReportDashboard
		WHERE
			numDomainID=@numDomainID
			AND numUserCntID=@numUserCntID
			AND numDashboardTemplateID=@numTemplateID

		
		UPDATE ReportDashboard SET bitNewAdded=0 WHERE numDomainID=@numDomainID AND numDashboardTemplateID=@numTemplateID AND numUserCntID = @numUserCntID

		-- DELETE DASHBOARD REPORT CONFIGURATION FOR ALL USERS WHO ARE USING EDITED DASHBOARD TEMPLATE EXCEPT CURRENT USER(@numUserCntID)
		DELETE FROM 
			ReportDashboard 
		WHERE 
			numDomainID=@numDomainID 
			AND numDashboardTemplateID=@numTemplateID 
			AND numUserCntID IN (SELECT numUserDetailId FROM UserMaster WHERE numDomainID=@numDomainID AND numUserDetailId <> @numUserCntID)

		INSERT INTO [dbo].[ReportDashboard]
        (
			numDomainID
			,numReportID
			,numUserCntID
			,tintReportType
			,tintChartType
			,vcHeaderText
			,vcFooterText
			,tintRow
			,tintColumn
			,vcXAxis
			,vcYAxis
			,vcAggType
			,tintReportCategory
			,intHeight
			,intWidth
			,numDashboardTemplateID
		)
		SELECT
			numDomainID
			,numReportID
			,numUserDetailId
			,tintReportType
			,tintChartType
			,vcHeaderText
			,vcFooterText
			,tintRow
			,tintColumn
			,vcXAxis
			,vcYAxis
			,vcAggType
			,tintReportCategory
			,intHeight
			,intWidth
			,numDashboardTemplateID
		FROM
			DashboardTemplateReports
		CROSS APPLY
		(
			SELECT 
				numUserDetailId 
			FROM 
				UserMaster 
			WHERE 
				numDomainID=@numDomainID 
				AND numDashboardTemplateID=@numTemplateID
				 AND numUserDetailId <> @numUserCntID
		) DashboardTemplateAssignee
		WHERE
			numDomainID=@numDomainID
			AND numDashboardTemplateID=@numTemplateID
	END
COMMIT
END TRY
BEGIN CATCH
	DECLARE @ErrorMessage NVARCHAR(4000)
	DECLARE @ErrorNumber INT
	DECLARE @ErrorSeverity INT
	DECLARE @ErrorState INT
	DECLARE @ErrorLine INT
	DECLARE @ErrorProcedure NVARCHAR(200)

	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;

	SELECT 
		@ErrorMessage = ERROR_MESSAGE(),
		@ErrorNumber = ERROR_NUMBER(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE(),
		@ErrorLine = ERROR_LINE(),
		@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

	RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);
END CATCH
END
GO