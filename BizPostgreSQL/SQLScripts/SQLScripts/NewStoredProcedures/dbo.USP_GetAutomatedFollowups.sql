SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetAutomatedFollowups')
DROP PROCEDURE USP_GetAutomatedFollowups
GO

--exec [USP_GetAutomatedFollowups] 279573,72

----Created By Priya (15 Jan 2018)

CREATE PROCEDURE [dbo].[USP_GetAutomatedFollowups]
	@numContactId NUMERIC,
	@numDomainId AS NUMERIC(9)
AS
BEGIN
SET NOCOUNT ON; 
--DECLARE FollowUpCampaign as  VARCHAR(MAX)
--DECLARE	LastFollowUp VARCHAR(MAX)
--DECLARE	NextFollowUp VARCHAR(MAX)

	SELECT

	(CASE WHEN 
			ISNULL((SELECT COUNT(*) FROM ConECampaignDTL INNER JOIN ConECampaign ON ConECampaignDTL.numConECampID = ConECampaign.numConEmailCampID
										WHERE ConECampaign.numContactID = A.numContactID AND  ConECampaign.numECampaignID = A.numECampaignID AND bitEngaged =1 AND ISNULL(bitSend,0) = 1),0) 
			= ISNULL((SELECT COUNT(*) FROM ConECampaignDTL INNER JOIN ConECampaign ON ConECampaignDTL.numConECampID = ConECampaign.numConEmailCampID
									     WHERE ConECampaign.numContactID = A.numContactID AND ConECampaign.numECampaignID = A.numECampaignID AND bitEngaged =1 ),0)
	THEN

	ISNULL((SELECT CONCAT('<img alt="Campaign completed" height="16px" width="16px" title="Campaign Completed" src="../images/comflag.png"> ',
							vcECampName, ' (' + 
									 CAST(ISNULL((SELECT COUNT(*) FROM ConECampaignDTL INNER JOIN ConECampaign ON ConECampaignDTL.numConECampID = ConECampaign.numConEmailCampID
										WHERE ConECampaign.numContactID = A.numContactID AND  ECampaign.numECampaignID = ConECampaign.numECampaignID AND bitEngaged =1 AND ISNULL(bitSend,0) = 1 ),0) AS varchar) + ' Of ' +
									 CAST(ISNULL((SELECT COUNT(*) FROM ConECampaignDTL INNER JOIN ConECampaign ON ConECampaignDTL.numConECampID = ConECampaign.numConEmailCampID
									     WHERE ConECampaign.numContactID = A.numContactID AND ECampaign.numECampaignID = ConECampaign.numECampaignID AND bitEngaged =1),0) AS VARCHAR) + ') ' 
		 ) FROM [ECampaign] WHERE numECampaignID = A.numECampaignID),'')
	ELSE

	ISNULL((SELECT CONCAT('<img alt="Active Campaign" height="16px" width="16px" title="Active Campaign" src="../images/Flag_Green.png"> ',
							vcECampName, ' (' + 
									 CAST(ISNULL((SELECT COUNT(*) FROM ConECampaignDTL INNER JOIN ConECampaign ON ConECampaignDTL.numConECampID = ConECampaign.numConEmailCampID
										WHERE ConECampaign.numContactID = A.numContactID AND  ECampaign.numECampaignID = ConECampaign.numECampaignID AND bitEngaged =1 AND ISNULL(bitSend,0) = 1 ),0) AS varchar) + ' Of ' +
									 CAST(ISNULL((SELECT COUNT(*) FROM ConECampaignDTL INNER JOIN ConECampaign ON ConECampaignDTL.numConECampID = ConECampaign.numConEmailCampID
									     WHERE ConECampaign.numContactID = A.numContactID AND ECampaign.numECampaignID = ConECampaign.numECampaignID AND bitEngaged =1),0) AS VARCHAR) + ') ' 
		 ) FROM [ECampaign] WHERE numECampaignID = A.numECampaignID),'')

	END
	)  FollowUpCampaign,

	dbo.fn_FollowupDetailsInOrgList(@numContactId,1,@numDomainId)LastFollowUp,
	dbo.fn_FollowupDetailsInOrgList(@numContactId,2,@numDomainId)NextFollowUp
	--dbo.fn_GetFollowUpDetails(@numContactId,3,@numDomainId)LastEmailStatus


FROM AdditionalContactsInformation A
WHERE A.numContactId = @numContactId AND A.numDomainID = @numDomainId 


END


GO


