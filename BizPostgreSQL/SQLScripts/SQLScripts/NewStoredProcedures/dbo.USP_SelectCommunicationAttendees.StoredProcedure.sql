GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_SelectCommunicationAttendees')
DROP PROCEDURE USP_SelectCommunicationAttendees
GO
CREATE PROCEDURE [dbo].[USP_SelectCommunicationAttendees]
@numCommId AS numeric(9)=0
AS
BEGIN
 SELECT ISNULL(vcFirstName,'')+' '+ISNULL(vcLastName,'') as vcContact,CA.numContactID,CI.vcCompanyName AS vcCompanyname,
  CASE WHEN UM.numUserDetailId IS NULL THEN 2 ELSE 1 END AS tinUserType,CA.ActivityID,AC.vcEmail as Email,
  dbo.GetListIemName(CA.numStatus) vcStatus,
  dbo.GetListIemName(AC.vcPosition) vcPosition,
  AC.vcEmail,
  AC.numPhone,
  AC.numPhoneExtension,
  0 AS bitDefault
 FROM CommunicationAttendees CA LEFT JOIN AdditionalContactsInformation AC ON CA.numContactID=AC.numContactID  
 LEFT JOIN divisionmaster DM on DM.numDivisionID=AC.numDivisionID 
 LEFT JOIN CompanyInfo CI on DM.numCompanyId=CI.numCompanyId 
 LEFT JOIN UserMaster UM  ON UM.numUserDetailId=CA.numContactID
 WHERE CA.numCommId=@numCommId
END