GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_Cases_ChangeOrganization')
DROP PROCEDURE USP_Cases_ChangeOrganization
GO
CREATE PROCEDURE [dbo].[USP_Cases_ChangeOrganization]                                   
(                                                   
	@numDomainID NUMERIC(18,0),    
	@numUserCntID NUMERIC(18,0),
	@numCaseId NUMERIC(18,0),
	@numNewDivisionID NUMERIC(18,0),
	@numNewContactID NUMERIC(18,0)               
)                                    
as
BEGIN
	DECLARE @numOldDivisionID NUMERIC(18,0)

	SELECT @numOldDivisionID=numDivisionId FROM Cases WHERE numDomainId=@numDomainID AND numCaseId = @numCaseId

	BEGIN TRY
	BEGIN TRANSACTION
		UPDATE 
			Cases 
		SET 
			numDivisionId=@numNewDivisionID
			,numContactId=@numNewContactID
		WHERE 
			numDomainId=@numDomainID 
			AND numCaseId = @numCaseId
		
		INSERT INTO [dbo].[RecordOrganizationChangeHistory]
		(
			numDomainID,numCaseID,numOldDivisionID,numNewDivisionID,numModifiedBy,dtModified
		)
		VALUES
		(
			@numDomainID,@numCaseId,@numOldDivisionID,@numNewDivisionID,@numUserCntID,GETUTCDATE()
		)
	COMMIT
	END TRY
	BEGIN CATCH
		DECLARE @ErrorMessage NVARCHAR(4000)
		DECLARE @ErrorNumber INT
		DECLARE @ErrorSeverity INT
		DECLARE @ErrorState INT
		DECLARE @ErrorLine INT
		DECLARE @ErrorProcedure NVARCHAR(200)

		IF @@TRANCOUNT > 0
			ROLLBACK TRANSACTION;

		SELECT 
			@ErrorMessage = ERROR_MESSAGE(),
			@ErrorNumber = ERROR_NUMBER(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE(),
			@ErrorLine = ERROR_LINE(),
			@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

		RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);
	END CATCH
END
GO