SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetStateCountryIDs')
DROP PROCEDURE dbo.USP_GetStateCountryIDs
GO

CREATE PROCEDURE [dbo].[USP_GetStateCountryIDs]
    @vcState VARCHAR(50),
	--@vcCountryCode VARCHAR(50),
	@vcCountryName Varchar(50),
    @numDomainID NUMERIC    

AS 
BEGIN
		--DECLARE @vcCountryName Varchar(50)
		DECLARE @numCountryID NUMERIC	
		DECLARE @numStateID NUMERIC

		--SET @vcCountryName = (SELECT TOP 1 vcCountryName from ShippingCountryMaster where vcCountryCode = @vcCountryCode)

		SET @numCountryID = (SELECT TOP 1 numListItemID FROM ListDetails WHERE numListID=40 AND (constFlag=1 OR numDomainID=@numDomainID) AND vcData = @vcCountryName)

		IF ISNULL(@numCountryID,0) = 0 AND ISNULL(@vcCountryName,'') <> ''
		BEGIN

			INSERT INTO ListDetails
				(
					numListID
					,vcData
					,numCreatedBY
					,bintCreatedDate
					,numModifiedBy
					,bintModifiedDate
					,bitDelete
					,numDomainId
					,constFlag
					,sintOrder
				)
				VALUES
				(
					40
					,@vcCountryName
					,1
					,GETDATE()
					,1
					,GETDATE()
					,0
					,@numDomainID
					,0
					,0
				)

				SET @numCountryID = SCOPE_IDENTITY()			

		END

		IF ISNULL(@numCountryID,0) > 0
		BEGIN
			SET @numStateID = (SELECT TOP 1 numStateID FROM [State] WHERE numDomainID=@numDomainID AND numCountryID=@numCountryID AND (vcState = @vcState OR vcAbbreviations = @vcState))

			IF ISNULL(@numStateID,0) = 0 AND ISNULL(@vcState,'') <> ''
			BEGIN
				
				INSERT INTO [STATE]
				(
					 numCountryID
					,vcState
					,numCreatedBy
					,bintCreatedDate
					,numModifiedBy
					,bintModifiedDate
					,numDomainID
					,constFlag
					,vcAbbreviations
					,numShippingZone

				)
				VALUES
				(
					@numCountryID
					,@vcState
					,1
					,GETDATE()
					,1
					,GETDATE()
					,@numDomainID
					,0
					,NULL
					,NULL
				)

				SET @numStateID = SCOPE_IDENTITY()		
			END
		END

		

	SELECT @numCountryID AS 'CountryId', @numStateID AS 'StateId'
END
GO


