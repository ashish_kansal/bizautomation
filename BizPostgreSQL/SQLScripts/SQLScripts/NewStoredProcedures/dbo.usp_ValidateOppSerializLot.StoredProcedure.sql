SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_ValidateOppSerializLot')
DROP PROCEDURE usp_ValidateOppSerializLot
GO
CREATE PROCEDURE [dbo].[usp_ValidateOppSerializLot]
    (
      @numOppID NUMERIC(9) = 0
    )
AS 
    BEGIN
   
--    SELECT numoppitemtCode,opp.vcItemName,CASE WHEN SUM(oppI.numQty)=SUM(opp.numUnitHour) THEN 0 ELSE 1 END Error
--FROM OpportunityItems opp JOIN Item i ON i.numItemCode=opp.numItemCode left JOIN OppWarehouseSerializedItem oppI ON (opp.numoppitemtCode=oppI.numOppItemID AND oppI.numOppID=Opp.numOppId)
-- LEFT JOIN WareHouseItmsDTL whi ON oppI.numWareHouseItmsDTLID=whi.numWareHouseItmsDTLID
--where opp.numOppId=@numOppID AND (ISNULL(i.bitSerialized,0)=1 OR ISNULL(i.bitLotNo,0)=1) GROUP BY numoppitemtCode,opp.vcItemName
DECLARE @numOppType TINYINT
DECLARE @numOppStatus TINYINT
DECLARE @bitStockTransfer BIT

SELECT @numOppType=tintOppType, @numOppStatus=tintOppStatus, @bitStockTransfer=bitStockTransfer FROM OpportunityMaster WHERE numOppID =  @numOppID

--Validate serial number in case of sales order and sotck transfer only
If (@numOppType = 1 AND @numOppStatus=1) OR @bitStockTransfer = 1
BEGIN
    SELECT numoppitemtCode,opp.vcItemName,CASE WHEN SUM(opp.numUnitHour)=
    (SELECT SUM(oppI.numQty) FROM OppWarehouseSerializedItem oppI JOIN WareHouseItmsDTL whi ON oppI.numWareHouseItmsDTLID=whi.numWareHouseItmsDTLID WHERE opp.numoppitemtCode=oppI.numOppItemID AND oppI.numOppID=@numOppID) THEN 0 ELSE 1 END Error
FROM OpportunityItems opp JOIN Item i ON i.numItemCode=opp.numItemCode
where opp.numOppId=@numOppID AND (ISNULL(i.bitSerialized,0)=1 OR ISNULL(i.bitLotNo,0)=1) GROUP BY numoppitemtCode,opp.vcItemName
END
ELSE IF (@numOppType = 2 AND @numOppStatus=1)
	SELECT 
		numoppitemtCode,
		opp.vcItemName,
		CASE 
			WHEN SUM(opp.numUnitHour) = (SELECT SUM(oppI.numQty) FROM OppWarehouseSerializedItem oppI JOIN WareHouseItmsDTL whi ON oppI.numWareHouseItmsDTLID=whi.numWareHouseItmsDTLID WHERE opp.numoppitemtCode=oppI.numOppItemID AND oppI.numOppID=@numOppID) 
			THEN 0 
			ELSE 1 
		END Error
	FROM 
		OpportunityItems opp 
	JOIN 
		Item i 
	ON 
		i.numItemCode=opp.numItemCode
	WHERE 
		opp.numOppId=@numOppID 
		AND (ISNULL(i.bitSerialized,0)=1 OR ISNULL(i.bitLotNo,0)=1)
	GROUP BY 
		numoppitemtCode,opp.vcItemName
ELSE
BEGIN
	 SELECT 
		numoppitemtCode,
		opp.vcItemName,
		0 AS Error
	FROM 
		OpportunityItems opp 
	JOIN 
		Item i 
	ON 
		i.numItemCode=opp.numItemCode
	WHERE 
		opp.numOppId=@numOppID AND 
		(ISNULL(i.bitSerialized,0)=1 OR ISNULL(i.bitLotNo,0)=1) 
	GROUP BY 
		numoppitemtCode,opp.vcItemName
END

END
