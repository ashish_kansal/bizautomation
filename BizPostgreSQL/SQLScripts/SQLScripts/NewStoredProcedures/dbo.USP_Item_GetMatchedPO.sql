GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_Item_GetMatchedPO')
DROP PROCEDURE USP_Item_GetMatchedPO
GO
CREATE PROCEDURE [dbo].[USP_Item_GetMatchedPO]
	@numDomainId NUMERIC(18,0)
	,@vcItems VARCHAR(MAX)
AS
BEGIN
	DECLARE @TEMP TABLE
	(
		[Index] INT 
		,ItemCode NUMERIC(18,0)
        ,Item VARCHAR(300)
        ,Attributes VARCHAR(1000)
        ,AttributeValues VARCHAR(500)
        ,UnitHour FLOAT
        ,POs VARCHAR(MAX)
	)

	DECLARE @hDoc as INTEGER
	EXEC sp_xml_preparedocument @hDoc OUTPUT, @vcItems

	INSERT INTO
		@TEMP
	SELECT
		[Index]
		,ItemCode
        ,Item
        ,Attributes
        ,AttributeValues
        ,UnitHour
		,''
	FROM
		OPENXML(@hDoc,'/NewDataSet/Table1',2)                                                                          
	WITH                       
	(
		[Index] INT 
		,ItemCode NUMERIC(18,0)
        ,Item VARCHAR(300)
        ,Attributes VARCHAR(1000)
        ,AttributeValues VARCHAR(500)
        ,UnitHour FLOAT
        ,POs VARCHAR(MAX)
	)

	EXEC sp_xml_removedocument @hDoc

	UPDATE	
		TP
	SET
		POs = CONCAT('[',STUFF((SELECT 
									CONCAT(', {"numOppID":',OM.numOppId,', "numOppItemID":',OI.numoppitemtCode,', "POName":"',ISNULL(OM.vcPOppName,''),'", "Vendor":"',ISNULL(CI.vcCompanyName,''),'","Price":',ISNULL(OI.monPrice,0),'}') 
								FROM 
									OpportunityMaster OM
								INNER JOIN
									DivisionMaster DM
								ON
									OM.numDivisionId = DM.numDivisionID
								INNER JOIN
									CompanyInfo CI
								ON
									DM.numCompanyID = CI.numCompanyId
								INNER JOIN
									OpportunityItems OI
								ON
									OM.numOppId = OI.numOppId
								WHERE 
									OM.numDomainId = @numDomainId
									AND OM.tintOppType = 2
									AND OM.tintOppStatus = 1
									AND ISNULL(OM.tintshipped,0) = 0
									AND OI.numItemCode = TP.ItemCode
									AND OI.numUnitHour = TP.UnitHour
									AND ISNULL(OI.vcAttrValues,'') = ISNULL(TP.AttributeValues,'')
								FOR XML PATH('')),1,1,''),']')
	FROM
		@TEMP TP


	SELECT * FROM @TEMP
END
GO