SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_Import_GetContactDetails')
DROP PROCEDURE USP_Import_GetContactDetails
GO
CREATE PROCEDURE [dbo].[USP_Import_GetContactDetails]  
	@numDomainID NUMERIC(18,0)
	,@numContactID NUMERIC(18,0)
AS  
BEGIN
	SELECT
		numDivisionId
		,numContactID
		,vcFirstName
		,vcLastName
		,vcEmail
		,numContactType
		,numPhone
		,numPhoneExtension
		,numCell
		,numHomePhone
		,vcFax
		,vcCategory
		,numTeam
		,vcPosition
		,vcTitle
		,vcDepartment
		,txtNotes
		,ISNULL((SELECT TOP 1 numNonBizCompanyID FROM ImportOrganizationContactReference WHERE numDomainID=@numDomainID AND numContactId=@numContactID),'') vcNonBizContactID
		,bitPrimaryContact
	FROM
		AdditionalContactsInformation
	WHERE
		numDomainID=@numDomainID
		AND numContactId=@numContactID
END