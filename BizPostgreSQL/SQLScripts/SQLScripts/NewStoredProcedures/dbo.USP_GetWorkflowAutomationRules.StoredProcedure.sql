
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetWorkflowAutomationRules')
	DROP PROCEDURE USP_GetWorkflowAutomationRules
GO

CREATE PROCEDURE [dbo].[USP_GetWorkflowAutomationRules]
	@numDomainID numeric(18, 0),
	@tintMode TINYINT =0,
	@numAutomationID numeric(18, 0)=0,
	@numRuleID numeric(18, 0)=0

AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED
If @tintMode = 0
BEGIN
SELECT
	[numAutomationID],
	[numRuleID],
	[numBizDocTypeId],
	[numBizDocStatus1],
	[numBizDocStatus2],
	[tintOppType],
	[numOrderStatus],
	[numDomainID],
	[numUserCntID],
	[numEmailTemplate],
	[numBizDocCreatedFrom],
	[numOpenRecievePayment],
	[numCreditCardOption],
	[dtCreated],
	[dtModified] 
FROM
	[dbo].[WorkflowAutomation]
WHERE ([numAutomationID] = @numAutomationID or @numAutomationID = 0) 
		AND ([numRuleID] = @numRuleID or @numRuleID = 0) AND [bitIsActive] = 1 AND [numDomainID] = @numDomainID

END

ELSE IF @tintMode = 1
BEGIN

SELECT
	[numAutomationID],
	[numRuleID],
	[numBizDocTypeId],
	[numBizDocStatus1],
	[numBizDocStatus2],
	[tintOppType],
	[numOrderStatus],
	[numDomainID],
	[numUserCntID],
	[numEmailTemplate],
	[numBizDocCreatedFrom],
	[numOpenRecievePayment],
	[numCreditCardOption],
	[dtCreated],
	[dtModified] 
FROM
	[dbo].[WorkflowAutomation]
WHERE ([numRuleID] = @numRuleID or @numRuleID = 0) 
AND [bitIsActive] = 1 
AND [numDomainID] = @numDomainID
AND [numBizDocCreatedFrom] IN(1,2)

END

GO