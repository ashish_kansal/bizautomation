GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_MassSalesFulfillment_UpdateItemsPickedQty')
DROP PROCEDURE dbo.USP_MassSalesFulfillment_UpdateItemsPickedQty
GO

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[USP_MassSalesFulfillment_UpdateItemsPickedQty]
(
	@numDomainID NUMERIC(18,0)
	,@numUserCntID NUMERIC(18,0)
	,@vcSelectedRecords VARCHAR(MAX)
	,@ClientTimeZoneOffset INT
)
AS
BEGIN
	DECLARE @numStatus NUMERIC(18,0) = 0
	IF EXISTS (SELECT ID FROM MassSalesFulfillmentConfiguration WHERE numDomainID=@numDomainID)
	BEGIN
		SELECT @numStatus = ISNULL(numOrderStatusPicked,0) FROM MassSalesFulfillmentConfiguration WHERE numDomainID=@numDomainID
	END

	DECLARE @TempOrders TABLE
	(
		ID INT IDENTITY(1,1)
		,numOppID NUMERIC(18,0)
		,numOppItemID NUMERIC(18,0)
		,numOppChildItemID NUMERIC(18,0)
		,numOppKitChildItemID NUMERIC(18,0)
		,numQtyPicked FLOAT
	)

	DECLARE @hDocItem INT

	DECLARE @Temp TABLE
	(
		ID INT IDENTITY(1,1)
		,numPickedQTY FLOAT
		,vcWarehouseItemIDs XML
		,vcOppItems VARCHAR(MAX)
		,numOppChildItemID NUMERIC(18,0)
		,numOppKitChildItemID NUMERIC(18,0)
	)

	EXEC sp_xml_preparedocument @hDocItem OUTPUT, @vcSelectedRecords

	INSERT INTO @Temp
	(
		numPickedQty,
		vcWarehouseItemIDs,
		vcOppItems,
		numOppChildItemID,
		numOppKitChildItemID
	)
	SELECT
		numPickedQty,
		WarehouseItemIDs,
		vcOppItems,
		numOppChildItemID,
		numOppKitChildItemID
	FROM
		OPENXML (@hDocItem,'/NewDataSet/Table1',2)
	WITH
	(
		numPickedQty FLOAT,
		WarehouseItemIDs xml,
		vcOppItems VARCHAR(MAX),
		numOppChildItemID NUMERIC(18,0),
		numOppKitChildItemID NUMERIC(18,0)
	)

	EXEC sp_xml_removedocument @hDocItem 

	DECLARE @TEMPItems TABLE
	(
		ID INT
		,numOppID NUMERIC(18,0)
		,numOppItemID NUMERIC(18,0)
		,numOppChildItemID NUMERIC(18,0)
		,numOppKitChildItemID NUMERIC(18,0)
		,bitSerial BIT
		,bitLot BIT
		,numWarehouseItemID NUMERIC(18,0)
		,numQtyLeftToPick FLOAT		
	)

	DECLARE @TEMPWarehouseLocations TABLE
	(
		ID INT
		,numWarehouseItemID NUMERIC(18,0)
		,numPickedQty FLOAT
		,vcSerialLot# VARCHAR(MAX)
	)

	DECLARE @TEMPSerialLot TABLE
	(
		ID INT
		,numWarehouseItemID NUMERIC(18,0)
		,vcSerialNo VARCHAR(300)
		,numQty FLOAT
	)

	DECLARE @TEMPWarehouseDTL TABLE
	(
		ID INT
		,numWareHouseItmsDTLID NUMERIC(18,0)
	)


	DECLARE @i INT = 1
	DECLARE @iCount INT
	DECLARE @j INT = 1
	DECLARE @jCount INT
	DECLARE @x INT = 1
	DECLARE @xCount INT = 0
	DECLARE @y INT = 1
	DECLARE @yCount INT = 0
	DECLARE @numPickedQty FLOAT
	DECLARE @numOppID NUMERIC(18,0)
	DECLARE @numOppItemID NUMERIC(18,0)
	DECLARE @numOppChildItemID NUMERIC(18,0)
	DECLARE @numOppKitChildItemID NUMERIC(18,0)
	DECLARE @bitSerial BIT
	DECLARE @bitLot BIT
	DECLARE @numWarehouseItemID NUMERIC(18,0)
	DECLARE @numQtyLeftToPick FLOAT
	DECLARE @vcWarehouseItemIDs VARCHAR(4000)
	DECLARE @vcOppItems VARCHAR(MAX)
	DECLARE @numTempWarehouseItemID NUMERIC(18,0)
	DECLARE @numWarehousePickedQty FLOAT
	DECLARE @numFromOnHand FLOAT
	DECLARE @numFromOnOrder FLOAT
	DECLARE @numFromAllocation FLOAT
	DECLARE @numFromBackOrder FLOAT
	DECLARE @numToOnHand FLOAT
	DECLARE @numToOnOrder FLOAT
	DECLARE @numToAllocation FLOAT
	DECLARE @numToBackOrder FLOAT
	DECLARE @vcDescription VARCHAR(300)
	DECLARE @numSerialWarehoueItemID NUMERIC(18,0)
	DECLARE @numMaxID NUMERIC(18,0)
	DECLARE @vcSerialLot# VARCHAR(MAX)
	DECLARE @vcSerialLotNo VARCHAR(200)	
	DECLARE @numSerialLotQty FLOAT
	DECLARE @numSerialLotQtyToPick FLOAT
	DECLARE @numWareHouseItmsDTLID NUMERIC(18,0)

	SET @iCount = (SELECT COUNT(*) FROM @TEMP)

	BEGIN TRY
	BEGIN TRANSACTION

		WHILE @i <= @iCount
		BEGIN
			SELECT
				@numPickedQty=ISNULL(numPickedQty,0)
				,@vcWarehouseItemIDs = CAST(vcWarehouseItemIDs.query('//WarehouseItemIDs/child::*') AS VARCHAR(MAX))
				,@vcOppItems=ISNULL(vcOppItems,'')
				,@numOppChildItemID=numOppChildItemID
				,@numOppKitChildItemID=numOppKitChildItemID
			FROM 
				@TEMP 
			WHERE 
				ID=@i

		
			DELETE FROM @TEMPItems
			DELETE FROM @TEMPWarehouseLocations
			DELETE FROM @TEMPSerialLot

			INSERT INTO @TEMPItems
			(
				ID
				,numOppID
				,numOppItemID
				,numOppChildItemID
				,numOppKitChildItemID
				,bitSerial
				,bitLot
				,numWarehouseItemID
				,numQtyLeftToPick
			)
			SELECT 
				ROW_NUMBER() OVER(ORDER BY numOppItemID ASC)
				,OpportunityItems.numOppId
				,TEMP.numOppItemID
				,@numOppChildItemID
				,@numOppKitChildItemID
				,ISNULL(Item.bitSerialized,0)
				,ISNULL(Item.bitLotNo,0)
				,OpportunityItems.numWarehouseItmsID
				,ISNULL(OpportunityItems.numUnitHour,0) - ISNULL(OpportunityItems.numQtyPicked,0)
			FROM
			(
				SELECT
					CAST(SUBSTRING(OutParam,0,CHARINDEX('(',OutParam)) AS NUMERIC) numOppItemID
				FROM
					dbo.SplitString(@vcOppItems,',')
			) TEMP
			INNER JOIN
				OpportunityItems
			ON
				Temp.numOppItemID = OpportunityItems.numoppitemtCode
				AND @numOppChildItemID = 0
				AND @numOppKitChildItemID = 0
			INNER JOIN
				Item
			ON
				OpportunityItems.numItemCode = Item.numItemCode

			INSERT INTO @TEMPItems
			(
				ID
				,numOppID
				,numOppItemID
				,numOppChildItemID
				,numOppKitChildItemID
				,bitSerial
				,bitLot
				,numWarehouseItemID
				,numQtyLeftToPick
			)
			SELECT 
				ROW_NUMBER() OVER(ORDER BY OpportunityKitItems.numOppItemID ASC)
				,OpportunityKitItems.numOppId
				,TEMP.numOppItemID
				,OpportunityKitItems.numOppChildItemID
				,0
				,ISNULL(Item.bitSerialized,0)
				,ISNULL(Item.bitLotNo,0)
				,OpportunityKitItems.numWareHouseItemId
				,@numPickedQty
			FROM
			(
				SELECT
					CAST(SUBSTRING(OutParam,0,CHARINDEX('(',OutParam)) AS NUMERIC) numOppItemID
				FROM
					dbo.SplitString(@vcOppItems,',')
			) TEMP
			INNER JOIN
				OpportunityItems
			ON
				Temp.numOppItemID = OpportunityItems.numoppitemtCode
			INNER JOIN
				OpportunityKitItems
			ON
				Temp.numOppItemID = OpportunityKitItems.numOppItemID
				AND OpportunityKitItems.numOppChildItemID = @numOppChildItemID
				AND @numOppKitChildItemID = 0
			INNER JOIN
				Item
			ON
				OpportunityKitItems.numChildItemID = Item.numItemCode

			INSERT INTO @TEMPItems
			(
				ID
				,numOppID
				,numOppItemID
				,numOppChildItemID
				,numOppKitChildItemID
				,bitSerial
				,bitLot
				,numWarehouseItemID
				,numQtyLeftToPick
			)
			SELECT 
				ROW_NUMBER() OVER(ORDER BY OpportunityKitChildItems.numOppItemID ASC)
				,OpportunityKitChildItems.numOppId
				,TEMP.numOppItemID
				,OpportunityKitChildItems.numOppChildItemID
				,OpportunityKitChildItems.numOppKitChildItemID
				,ISNULL(Item.bitSerialized,0)
				,ISNULL(Item.bitLotNo,0)
				,OpportunityKitChildItems.numWareHouseItemId
				,@numPickedQty
			FROM
			(
				SELECT
					CAST(SUBSTRING(OutParam,0,CHARINDEX('(',OutParam)) AS NUMERIC) numOppItemID
				FROM
					dbo.SplitString(@vcOppItems,',')
			) TEMP
			INNER JOIN
				OpportunityItems
			ON
				Temp.numOppItemID = OpportunityItems.numoppitemtCode
			INNER JOIN
				OpportunityKitChildItems
			ON
				Temp.numOppItemID = OpportunityKitChildItems.numOppItemID
				AND OpportunityKitChildItems.numOppChildItemID = @numOppChildItemID
				AND OpportunityKitChildItems.numOppKitChildItemID = @numOppKitChildItemID
			INNER JOIN
				OpportunityKitItems
			ON
				OpportunityKitChildItems.numOppChildItemID = OpportunityKitItems.numOppChildItemID
			INNER JOIN
				Item
			ON
				OpportunityKitChildItems.numItemID = Item.numItemCode

			IF LEN(ISNULL(@vcWarehouseItemIDs,'')) > 0
			BEGIN
				DECLARE @hDocSerialLotItem int
				EXEC sp_xml_preparedocument @hDocSerialLotItem OUTPUT, @vcWarehouseItemIDs

				INSERT INTO @TEMPWarehouseLocations
				(
					ID
					,numWarehouseItemID
					,numPickedQty
					,vcSerialLot#
				)
				SELECT 
					ROW_NUMBER() OVER(ORDER BY ID DESC)
					,ID
					,ISNULL(Qty,0)
					,ISNULL(SerialLotNo,'') 
				FROM 
					OPENXML (@hDocSerialLotItem, '/NewDataSet/Table1',2)
				WITH 
				(
					ID NUMERIC(18,0),
					Qty FLOAT,
					SerialLotNo VARCHAR(MAX)
				)

				EXEC sp_xml_removedocument @hDocSerialLotItem
			END

			IF ISNULL((SELECT TOP 1 bitSerial FROM @TEMPItems),0) = 1 AND (SELECT COUNT(*) FROM @TEMPWarehouseLocations) > 0
			BEGIN
				SET @x = 1
				SELECT @xCount=COUNT(*) FROM @TEMPWarehouseLocations

				WHILE @x <= @xCount
				BEGIN
					SELECT @numSerialWarehoueItemID=numWarehouseItemID,@vcSerialLot#=ISNULL(vcSerialLot#,'') FROM @TEMPWarehouseLocations WHERE ID=@x
					SET @numMaxID = ISNULL((SELECT MAX(ID) FROM @TEMPSerialLot),0)

					IF @vcSerialLot# <> ''
					BEGIN
						INSERT INTO @TEMPSerialLot
						(
							ID
							,numWarehouseItemID
							,vcSerialNo
							,numQty
						)
						SELECT
							@numMaxID + ROW_NUMBER() OVER(ORDER BY OutParam)
							,@numSerialWarehoueItemID
							,RTRIM(LTRIM(OutParam))
							,1
						FROM
							dbo.SplitString(@vcSerialLot#,',')
					END
					

					SET @x = @x + 1
				END
			END
			ELSE IF ISNULL((SELECT TOP 1 bitLot FROM @TEMPItems),0) = 1
			BEGIN
				SET @x = 1
				SELECT @xCount=COUNT(*) FROM @TEMPWarehouseLocations

				WHILE @x <= @xCount
				BEGIN
					SELECT @numSerialWarehoueItemID=numWarehouseItemID,@vcSerialLot#=ISNULL(vcSerialLot#,'') FROM @TEMPWarehouseLocations WHERE ID=@x
					SET @numMaxID = ISNULL((SELECT MAX(ID) FROM @TEMPSerialLot),0)

					IF @vcSerialLot# <> ''
					BEGIN
						INSERT INTO @TEMPSerialLot
						(
							ID
							,numWarehouseItemID
							,vcSerialNo
							,numQty
						)
						SELECT
							@numMaxID + ROW_NUMBER() OVER(ORDER BY OutParam)
							,@numSerialWarehoueItemID
							,(CASE 
								WHEN PATINDEX('%(%', OutParam) > 0 AND PATINDEX('%)%', OutParam) > 0 AND PATINDEX('%)%',OutParam) > (PATINDEX('%(%',OutParam) + 1)
								THEN RTRIM(LTRIM(SUBSTRING(OutParam,0,PATINDEX('%(%', OutParam))))
								ELSE ''
							END)
							,(CASE 
								WHEN PATINDEX('%(%', OutParam) > 0 AND PATINDEX('%)%', OutParam) > 0 AND PATINDEX('%)%',OutParam) > (PATINDEX('%(%',OutParam) + 1)
								THEN (CASE WHEN ISNUMERIC(SUBSTRING(OutParam,(PATINDEX('%(%', OutParam) + 1),(PATINDEX('%)%', OutParam) - PATINDEX('%(%', OutParam) - 1))) = 1 
										THEN CAST(SUBSTRING(OutParam,(PATINDEX('%(%', OutParam) + 1),(PATINDEX('%)%', OutParam) - PATINDEX('%(%', OutParam) - 1)) AS FLOAT)  
										ELSE -1 
										END) 
								ELSE -1
							END)	
						FROM
							dbo.SplitString(@vcSerialLot#,',')
					END
					

					SET @x = @x + 1
				END
			END

			IF EXISTS (SELECT vcSerialNo FROM @TEMPSerialLot WHERE numQty = -1)
			BEGIN
				RAISERROR('INVALID_SERIALLOT_FORMAT',16,1)
				RETURN
			END
		
			SET @j = 1
			SET @jCount = (SELECT COUNT(*) FROM @TEMPItems)

			WHILE @j <= @jCount
			BEGIN
				SELECT 
					@numOppID=numOppID
					,@numOppItemID=numOppItemID
					,@numOppChildItemID=numOppChildItemID
					,@numOppKitChildItemID=numOppKitChildItemID
					,@bitSerial=bitSerial
					,@bitLot=bitLot
					,@numWarehouseItemID=numWarehouseItemID
					,@numQtyLeftToPick=numQtyLeftToPick
				FROM
					@TEMPItems
				WHERE
					ID=@j

				UPDATE
					OpportunityItems
				SET 
					numQtyPicked = ISNULL(numQtyPicked,0) + (CASE WHEN @numPickedQty > @numQtyLeftToPick THEN @numQtyLeftToPick ELSE @numPickedQty END)
				WHERE 
					numoppitemtCode=@numOppItemID
					AND ISNULL(@numOppChildItemID,0) = 0
					AND ISNULL(@numOppKitChildItemID,0) = 0

				IF NOT EXISTS (SELECT ID FROM @TempOrders WHERE numOppID=@numOppID AND numOppItemID=@numOppItemID AND numOppChildItemID=@numOppChildItemID AND numOppKitChildItemID=@numOppKitChildItemID)
				BEGIN
					INSERT INTO @TempOrders
					(
						numOppID
						,numOppItemID
						,numOppChildItemID
						,numOppKitChildItemID
						,numQtyPicked
					)
					VALUES
					(
						@numOppID
						,@numOppItemID
						,@numOppChildItemID
						,@numOppKitChildItemID
						,(CASE WHEN @numPickedQty > @numQtyLeftToPick THEN @numQtyLeftToPick ELSE @numPickedQty END)
					)
				END
				ELSE
				BEGIN
					UPDATE
						@TempOrders
					SET
						numQtyPicked=ISNULL(numQtyPicked,0) + ISNULL((CASE WHEN @numPickedQty > @numQtyLeftToPick THEN @numQtyLeftToPick ELSE @numPickedQty END),0)
					WHERE
						numOppID=@numOppID
						AND numOppItemID=@numOppItemID
						AND numOppChildItemID=@numOppChildItemID 
						AND numOppKitChildItemID=@numOppKitChildItemID
				END

				SET @numPickedQty = @numPickedQty - (CASE WHEN @numPickedQty > @numQtyLeftToPick THEN @numQtyLeftToPick ELSE @numPickedQty END)

				IF EXISTS (SELECT numoppitemtCode FROM OpportunityItems WHERE numoppitemtCode=@numOppItemID AND @numOppChildItemID=0 AND @numOppKitChildItemID=0 AND ISNULL(numQtyPicked,0) > ISNULL(numUnitHour,0))
				BEGIN
					RAISERROR('PICKED_QTY_IS_GREATER_THEN_REMAINING_QTY',16,1)
				END

				IF EXISTS (SELECT ID FROM @TEMPWarehouseLocations WHERE numWarehouseItemID=@numWarehouseItemID)
				BEGIN
					SELECT 
						@numTempWarehouseItemID = numWarehouseItemID
						,@numWarehousePickedQty = ISNULL(numPickedQty,0)
					FROM 
						@TEMPWarehouseLocations
					WHERE 
						numWarehouseItemID=@numWarehouseItemID

					UPDATE 
						@TEMPWarehouseLocations
					SET
						numPickedQty = ISNULL(numPickedQty,0) - (CASE WHEN ISNULL(numPickedQty,0) > @numQtyLeftToPick THEN @numQtyLeftToPick ELSE ISNULL(numPickedQty,0) END)
					WHERE
						numWarehouseItemID=@numTempWarehouseItemID

					IF ISNULL(@bitSerial,0)=1 OR ISNULL(@bitLot,0)=1
					BEGIN
						IF  ISNULL((SELECT 
										SUM(ISNULL(TSL.numQty,0))
									FROM 
										WareHouseItmsDTL WID
									INNER JOIN
										@TEMPSerialLot TSL
									ON
										WID.vcSerialNo = WID.vcSerialNo
									WHERE 
										WID.numWareHouseItemID=@numWarehouseItemID
										AND TSL.numWarehouseItemID = @numWarehouseItemID
										AND ISNULL(TSL.numQty,0) > 0
										AND ISNULL(WID.numQty,0) >= ISNULL(TSL.numQty,0)),0) >= (CASE WHEN @numWarehousePickedQty >= @numQtyLeftToPick THEN @numQtyLeftToPick ELSE @numWarehousePickedQty END)
										

						BEGIN
							SET @x = 1
							SELECT @xCount=COUNT(*) FROM @TEMPSerialLot
							SET @numSerialLotQtyToPick = (CASE WHEN @numWarehousePickedQty >= @numQtyLeftToPick THEN @numQtyLeftToPick ELSE @numWarehousePickedQty END)

							WHILE @x <= @xCount AND @numSerialLotQtyToPick > 0
							BEGIN
								SELECT @vcSerialLotNo=vcSerialNo,@numSerialLotQty=numQty FROM @TEMPSerialLot WHERE ID=@x

								IF ISNULL(@numSerialLotQty,0) > 0
								BEGIN
									IF @bitSerial = 1
									BEGIN
										IF EXISTS (SELECT numWareHouseItmsDTLID FROM WareHouseItmsDTL WHERE numWareHouseItemID=@numWarehouseItemID AND vcSerialNo=@vcSerialLotNo AND numQty>=@numSerialLotQty)
										BEGIN
											SELECT TOP 1
												@numWareHouseItmsDTLID =numWareHouseItmsDTLID
											FROM
												WareHouseItmsDTL 
											WHERE 
												numWareHouseItemID=@numWarehouseItemID 
												AND vcSerialNo=@vcSerialLotNo 
												AND numQty>=@numSerialLotQty

											INSERT INTO OppWarehouseSerializedItem
											(
												numWarehouseItmsDTLID
												,numWarehouseItmsID
												,numOppID
												,numOppItemID
												,numQty
											)
											SELECT
												numWareHouseItmsDTLID
												,numWareHouseItemID
												,@numOppID
												,@numOppItemID
												,@numSerialLotQty
											FROM 
												WareHouseItmsDTL 
											WHERE 
												numWareHouseItmsDTLID=@numWareHouseItmsDTLID

											SET @numSerialLotQtyToPick = @numSerialLotQtyToPick - @numSerialLotQty
											UPDATE @TEMPSerialLot SET numQty = ISNULL(numQty,0) - @numSerialLotQty  WHERE ID=@x
											UPDATE WareHouseItmsDTL SET numQty = ISNULL(numQty,0) - @numSerialLotQty  WHERE numWareHouseItmsDTLID=@numWareHouseItmsDTLID
										END
										ELSE
										BEGIN
											RAISERROR('INVLAID_SERIALLOT_QTY',16,1)
											RETURN
										END
									END
									ELSE IF @bitLot = 1
									BEGIN
										IF ISNULL((SELECT SUM(numQty) FROM WareHouseItmsDTL WHERE numWareHouseItemID=@numWarehouseItemID AND vcSerialNo=@vcSerialLotNo),0) >= @numSerialLotQty
										BEGIN
											DELETE FROM @TEMPWarehouseDTL

											INSERT INTO @TEMPWarehouseDTL
											(
												ID
												,numWareHouseItmsDTLID
											)
											SELECT
												ROW_NUMBER() OVER(ORDER BY numWareHouseItmsDTLID)
												,numWareHouseItmsDTLID
											FROM
												WareHouseItmsDTL 
											WHERE 
												numWareHouseItemID=@numWarehouseItemID 
												AND vcSerialNo=@vcSerialLotNo

											SET @y = 1
											SELECT COUNT(*) FROM @TEMPWarehouseDTL

											WHILE @y <= @yCount AND @numSerialLotQty > 0
											BEGIN
												SELECT @numWareHouseItmsDTLID=numWareHouseItmsDTLID FROM @TEMPWarehouseDTL WHERE ID=@y

												IF ISNULL((SELECT numQty FROM WareHouseItmsDTL WHERE numWareHouseItmsDTLID=@numWareHouseItmsDTLID),0) > @numSerialLotQty
												BEGIN
													INSERT INTO OppWarehouseSerializedItem
													(
														numWarehouseItmsDTLID
														,numWarehouseItmsID
														,numOppID
														,numOppItemID
														,numQty
													)
													SELECT
														numWareHouseItmsDTLID
														,@numWarehouseItemID
														,@numOppID
														,@numOppItemID
														,@numSerialLotQty
													FROM 
														WareHouseItmsDTL 
													WHERE 
														numWareHouseItmsDTLID=@numWareHouseItmsDTLID

													SET @numSerialLotQtyToPick = @numSerialLotQtyToPick - @numSerialLotQty
													SET @numSerialLotQty = 0
													UPDATE @TEMPSerialLot SET numQty = ISNULL(numQty,0) - @numSerialLotQty WHERE ID=@x
													UPDATE WareHouseItmsDTL SET numQty = ISNULL(numQty,0) -  @numSerialLotQty  WHERE numWareHouseItmsDTLID=@numWareHouseItmsDTLID
												END
												ELSE
												BEGIN
													INSERT INTO OppWarehouseSerializedItem
													(
														numWarehouseItmsDTLID
														,numWarehouseItmsID
														,numOppID
														,numOppItemID
														,numQty
													)
													SELECT
														numWareHouseItmsDTLID
														,@numWarehouseItemID
														,@numOppID
														,@numOppItemID
														,numQty
													FROM 
														WareHouseItmsDTL 
													WHERE 
														numWareHouseItmsDTLID=@numWareHouseItmsDTLID

													SET @numSerialLotQtyToPick = @numSerialLotQtyToPick - ISNULL((SELECT numQty FROM WareHouseItmsDTL WHERE numWareHouseItmsDTLID=@numWareHouseItmsDTLID),0)
													SET @numSerialLotQty = @numSerialLotQty - ISNULL((SELECT numQty FROM WareHouseItmsDTL WHERE numWareHouseItmsDTLID=@numWareHouseItmsDTLID),0)
													UPDATE @TEMPSerialLot SET numQty = ISNULL(numQty,0) - ISNULL((SELECT numQty FROM WareHouseItmsDTL WHERE numWareHouseItmsDTLID=@numWareHouseItmsDTLID),0)  WHERE ID=@x
													UPDATE WareHouseItmsDTL SET numQty = 0  WHERE numWareHouseItmsDTLID=@numWareHouseItmsDTLID
												END

												SET @y = @y + 1
											END

											IF ISNULL(@numSerialLotQty,0) > 0
											BEGIN
												RAISERROR('INVLAID_SERIALLOT_QTY',16,1)
												RETURN
											END
										ELSE
										BEGIN
											RAISERROR('INVLAID_SERIALLOT_QTY',16,1)
											RETURN
										END
									END
								END
								END

								SET @x = @x + 1
							END

							IF @numSerialLotQtyToPick > 0
							BEGIN
								RAISERROR('SUFFICIENT_SERIALLOT_QTY_NOT_PICKED',16,1)
								RETURN
							END
						END
						ELSE
						BEGIN
							RAISERROR('INVLAID_SERIALLOT_QTY',16,1)
							RETURN
						END

					END

					SET @numQtyLeftToPick = @numQtyLeftToPick - (CASE WHEN @numWarehousePickedQty >= @numQtyLeftToPick THEN @numQtyLeftToPick ELSE @numWarehousePickedQty END)
				END

				IF @numQtyLeftToPick > 0
				BEGIN
					DECLARE @k INT = 1
					DECLARE @kCount INT 

					SELECT @kCount=COUNT(*) FROM @TEMPWarehouseLocations

					WHILE @k <= @kCount
					BEGIN
						SELECT 
							@numTempWarehouseItemID = numWarehouseItemID
							,@numWarehousePickedQty = ISNULL(numPickedQty,0)
						FROM 
							@TEMPWarehouseLocations 
						WHERE 
							ID = @k

						UPDATE 
							@TEMPWarehouseLocations
						SET
							numPickedQty = ISNULL(numPickedQty,0) - (CASE WHEN ISNULL(numPickedQty,0) > @numQtyLeftToPick THEN @numQtyLeftToPick ELSE ISNULL(numPickedQty,0) END)
						WHERE
							numWarehouseItemID=@numTempWarehouseItemID

						IF ISNULL(@bitSerial,0)=1 OR ISNULL(@bitLot,0)=1
						BEGIN
							IF  ISNULL((SELECT 
											SUM(ISNULL(TSL.numQty,0))
										FROM 
											WareHouseItmsDTL WID
										INNER JOIN
											@TEMPSerialLot TSL
										ON
											WID.vcSerialNo = WID.vcSerialNo
										WHERE 
											WID.numWareHouseItemID=@numTempWarehouseItemID
											AND TSL.numWarehouseItemID = @numTempWarehouseItemID
											AND ISNULL(TSL.numQty,0) > 0
											AND ISNULL(WID.numQty,0) >= ISNULL(TSL.numQty,0)),0) >= (CASE WHEN @numWarehousePickedQty >= @numQtyLeftToPick THEN @numQtyLeftToPick ELSE @numWarehousePickedQty END)										
							BEGIN
								SET @x = 1
								SELECT @xCount=COUNT(*) FROM @TEMPSerialLot
								SET @numSerialLotQtyToPick = (CASE WHEN @numWarehousePickedQty >= @numQtyLeftToPick THEN @numQtyLeftToPick ELSE @numWarehousePickedQty END)

								WHILE @x <= @xCount AND @numSerialLotQtyToPick > 0
								BEGIN
									SELECT @vcSerialLotNo=vcSerialNo,@numSerialLotQty=numQty FROM @TEMPSerialLot WHERE ID=@x

									IF ISNULL(@numSerialLotQty,0) > 0
									BEGIN
										IF @bitSerial = 1
										BEGIN
											IF EXISTS (SELECT numWareHouseItmsDTLID FROM WareHouseItmsDTL WHERE numWareHouseItemID=@numTempWarehouseItemID AND vcSerialNo=@vcSerialLotNo AND numQty>=@numSerialLotQty)
											BEGIN
												SELECT TOP 1
													@numWareHouseItmsDTLID =numWareHouseItmsDTLID
												FROM
													WareHouseItmsDTL 
												WHERE 
													numWareHouseItemID=@numTempWarehouseItemID 
													AND vcSerialNo=@vcSerialLotNo 
													AND numQty>=@numSerialLotQty

												INSERT INTO OppWarehouseSerializedItem
												(
													numWarehouseItmsDTLID
													,numWarehouseItmsID
													,numOppID
													,numOppItemID
													,numQty
												)
												SELECT
													numWareHouseItmsDTLID
													,@numWarehouseItemID
													,@numOppID
													,@numOppItemID
													,@numSerialLotQty
												FROM 
													WareHouseItmsDTL 
												WHERE 
													numWareHouseItmsDTLID=@numWareHouseItmsDTLID

												SET @numSerialLotQtyToPick = @numSerialLotQtyToPick - @numSerialLotQty
												UPDATE @TEMPSerialLot SET numQty = ISNULL(numQty,0) - @numSerialLotQty  WHERE ID=@x
												UPDATE WareHouseItmsDTL SET numQty = ISNULL(numQty,0) - @numSerialLotQty,numWareHouseItemID=@numWarehouseItemID  WHERE numWareHouseItmsDTLID=@numWareHouseItmsDTLID
											END
											ELSE
											BEGIN
												RAISERROR('INVLAID_SERIALLOT_QTY',16,1)
												RETURN
											END
										END
										ELSE IF @bitLot = 1
										BEGIN
											IF ISNULL((SELECT SUM(numQty) FROM WareHouseItmsDTL WHERE numWareHouseItemID=@numTempWarehouseItemID AND vcSerialNo=@vcSerialLotNo),0) >= @numSerialLotQty
											BEGIN
												DELETE FROM @TEMPWarehouseDTL

												INSERT INTO @TEMPWarehouseDTL
												(
													ID
													,numWareHouseItmsDTLID
												)
												SELECT
													ROW_NUMBER() OVER(ORDER BY numWareHouseItmsDTLID)
													,numWareHouseItmsDTLID
												FROM
													WareHouseItmsDTL 
												WHERE 
													numWareHouseItemID=@numTempWarehouseItemID 
													AND vcSerialNo=@vcSerialLotNo

												SET @y = 1
												SELECT COUNT(*) FROM @TEMPWarehouseDTL

												WHILE @y <= @yCount AND @numSerialLotQty > 0
												BEGIN
													SELECT @numWareHouseItmsDTLID=numWareHouseItmsDTLID FROM @TEMPWarehouseDTL WHERE ID=@y

													IF ISNULL((SELECT numQty FROM WareHouseItmsDTL WHERE numWareHouseItmsDTLID=@numWareHouseItmsDTLID),0) > @numSerialLotQty
													BEGIN
														INSERT INTO OppWarehouseSerializedItem
														(
															numWarehouseItmsDTLID
															,numWarehouseItmsID
															,numOppID
															,numOppItemID
															,numQty
														)
														SELECT
															numWareHouseItmsDTLID
															,@numWarehouseItemID
															,@numOppID
															,@numOppItemID
															,@numSerialLotQty
														FROM 
															WareHouseItmsDTL 
														WHERE 
															numWareHouseItmsDTLID=@numWareHouseItmsDTLID

														INSERT INTO WareHouseItmsDTL
														(
															numWareHouseItemID
															,vcSerialNo
															,numQty
														)
														VALUES
														(
															@numWarehouseItemID
															,@vcSerialLotNo
															,@numSerialLotQty
														)

														SET @numSerialLotQtyToPick = @numSerialLotQtyToPick - @numSerialLotQty
														SET @numSerialLotQty = 0
														UPDATE @TEMPSerialLot SET numQty = ISNULL(numQty,0) - @numSerialLotQty WHERE ID=@x
														UPDATE WareHouseItmsDTL SET numQty = ISNULL(numQty,0) -  @numSerialLotQty  WHERE numWareHouseItmsDTLID=@numWareHouseItmsDTLID
													END
													ELSE
													BEGIN
														INSERT INTO OppWarehouseSerializedItem
														(
															numWarehouseItmsDTLID
															,numWarehouseItmsID
															,numOppID
															,numOppItemID
															,numQty
														)
														SELECT
															numWareHouseItmsDTLID
															,@numWarehouseItemID
															,@numOppID
															,@numOppItemID
															,numQty
														FROM 
															WareHouseItmsDTL 
														WHERE 
															numWareHouseItmsDTLID=@numWareHouseItmsDTLID

														SET @numSerialLotQtyToPick = @numSerialLotQtyToPick - ISNULL((SELECT numQty FROM WareHouseItmsDTL WHERE numWareHouseItmsDTLID=@numWareHouseItmsDTLID),0)
														SET @numSerialLotQty = @numSerialLotQty - ISNULL((SELECT numQty FROM WareHouseItmsDTL WHERE numWareHouseItmsDTLID=@numWareHouseItmsDTLID),0)
														UPDATE @TEMPSerialLot SET numQty = ISNULL(numQty,0) - ISNULL((SELECT numQty FROM WareHouseItmsDTL WHERE numWareHouseItmsDTLID=@numWareHouseItmsDTLID),0)  WHERE ID=@x
														UPDATE WareHouseItmsDTL SET numWareHouseItemID=@numWarehouseItemID WHERE numWareHouseItmsDTLID=@numWareHouseItmsDTLID
													END

													SET @y = @y + 1
												END

												IF ISNULL(@numSerialLotQty,0) > 0
												BEGIN
													RAISERROR('INVLAID_SERIALLOT_QTY',16,1)
													RETURN
												END
											ELSE
											BEGIN
												RAISERROR('INVLAID_SERIALLOT_QTY',16,1)
												RETURN
											END
										END
									END
									END

									SET @x = @x + 1
								END

								IF @numSerialLotQtyToPick > 0
								BEGIN
									RAISERROR('SUFFICIENT_SERIALLOT_QTY_NOT_PICKED',16,1)
									RETURN
								END
							END
							ELSE
							BEGIN
								RAISERROR('INVLAID_SERIALLOT_QTY',16,1)
								RETURN
							END
						END

						IF @numWarehousePickedQty > 0 AND (@numWarehouseItemID <> @numTempWarehouseItemID)
						BEGIN
							SELECT
								@numFromOnHand=ISNULL(numOnHand,0)
								,@numFromOnOrder=ISNULL(numOnOrder,0)
								,@numFromAllocation=ISNULL(numAllocation,0)
								,@numFromBackOrder=ISNULL(numBackOrder,0)
							FROM
								WareHouseItems
							WHERE
								numWareHouseItemID=@numTempWarehouseItemID

							SELECT
								@numToOnHand=ISNULL(numOnHand,0)
								,@numToOnOrder=ISNULL(numOnOrder,0)
								,@numToAllocation=ISNULL(numAllocation,0)
								,@numToBackOrder=ISNULL(numBackOrder,0)
							FROM
								WareHouseItems
							WHERE
								numWareHouseItemID=@numWarehouseItemID

							IF @numWarehousePickedQty >= @numQtyLeftToPick
							BEGIN
								SET @vcDescription = CONCAT('Items picked Qty(:',@numQtyLeftToPick,')') 

								IF (@numFromOnHand+@numFromAllocation) >= @numQtyLeftToPick
								BEGIN
									IF @numFromOnHand >= @numQtyLeftToPick
									BEGIN
										SET @numFromOnHand = @numFromOnHand - @numQtyLeftToPick
									END
									ELSE 
									BEGIN
										SET @numFromOnHand = 0
										SET @numFromAllocation = @numFromAllocation - (@numQtyLeftToPick - @numFromOnHand)
										SET @numFromBackOrder = @numFromBackOrder + (@numQtyLeftToPick - @numFromOnHand)
									END
								END
								ELSE
								BEGIN
									RAISERROR('AVAILABLE_QUANTITY_LESS_THEN_PICKED_QTY',16,1)
								END

								IF @numToBackOrder >= @numQtyLeftToPick
								BEGIN
									SET @numToBackOrder = @numToBackOrder - @numQtyLeftToPick
									SET @numToAllocation = @numToAllocation + @numQtyLeftToPick
								END
								ELSE
								BEGIN
									SET @numToAllocation = @numToAllocation + @numToBackOrder
									SET @numToOnHand = @numToOnHand + (@numQtyLeftToPick - @numToBackOrder)
									SET @numToBackOrder = 0
								END

								SET @numQtyLeftToPick = 0
							END
							ELSE
							BEGIN
								SET @vcDescription = CONCAT('Items picked Qty(:',@numWarehousePickedQty,')') 

								IF (@numFromOnHand+@numFromAllocation) >= @numWarehousePickedQty
								BEGIN
									IF @numFromOnHand >= @numWarehousePickedQty
									BEGIN
										SET @numFromOnHand = @numFromOnHand - @numWarehousePickedQty
									END
									ELSE 
									BEGIN
										SET @numFromOnHand = 0
										SET @numFromAllocation = @numFromAllocation - (@numWarehousePickedQty - @numFromOnHand)
										SET @numFromBackOrder = @numFromBackOrder + (@numWarehousePickedQty - @numFromOnHand)
									END
								END
								ELSE
								BEGIN
									RAISERROR('AVAILABLE_QUANTITY_LESS_THEN_PICKED_QTY',16,1)
								END

								IF @numToBackOrder >= @numWarehousePickedQty
								BEGIN
									SET @numToBackOrder = @numToBackOrder - @numWarehousePickedQty
									SET @numToAllocation = @numToAllocation + @numWarehousePickedQty
								END
								ELSE
								BEGIN
									SET @numToAllocation = @numToAllocation + @numToBackOrder
									SET @numToOnHand = @numToOnHand + (@numWarehousePickedQty - @numToBackOrder)
									SET @numToBackOrder = 0
								END
								

								SET @numQtyLeftToPick = @numQtyLeftToPick - @numWarehousePickedQty
							END

							--UPDATE INVENTORY AND WAREHOUSE TRACKING
							EXEC USP_UpdateInventoryAndTracking 
							@numOnHand=@numFromOnHand,
							@numOnAllocation=@numFromAllocation,
							@numOnBackOrder=@numFromBackOrder,
							@numOnOrder=@numFromOnOrder,
							@numWarehouseItemID=@numTempWarehouseItemID,
							@numReferenceID=@numOppID,
							@tintRefType=3,
							@numDomainID=@numDomainID,
							@numUserCntID=@numUserCntID,
							@Description=@vcDescription 

							SET @vcDescription = REPLACE(@vcDescription,'picked','receieved')
							EXEC USP_UpdateInventoryAndTracking 
							@numOnHand=@numToOnHand,
							@numOnAllocation=@numToAllocation,
							@numOnBackOrder=@numToBackOrder,
							@numOnOrder=@numToOnOrder,
							@numWarehouseItemID=@numWarehouseItemID,
							@numReferenceID=@numOppID,
							@tintRefType=3,
							@numDomainID=@numDomainID,
							@numUserCntID=@numUserCntID,
							@Description=@vcDescription 

						END
						ELSE IF @numWarehousePickedQty > 0
						BEGIN
							IF @numWarehousePickedQty > @numFromAllocation
							BEGIN
								RAISERROR('AVAILABLE_QUANTITY_LESS_THEN_PICKED_QTY',16,1)
							END
							ELSE
							BEGIN
								IF @numWarehousePickedQty >= @numQtyLeftToPick
								BEGIN
									SET @numQtyLeftToPick = 0
								END
								ELSE
								BEGIN
									SET @numQtyLeftToPick = @numQtyLeftToPick - @numWarehousePickedQty
								END
							END
						END

						IF @numQtyLeftToPick <= 0
							BREAK

						SET @k = @k + 1
					END
				END

				IF @numPickedQty <= 0
				BEGIN
					BREAK
				END

				SET @j = @j + 1
			END

			IF @numPickedQty > 0 AND @numOppChildItemID=0 AND @numOppKitChildItemID=0
			BEGIN
				RAISERROR('PICKED_QTY_IS_GREATER_THEN_REMAINING_QTY',16,1)
			END

			SET @i = @i + 1
		END

		IF ISNULL(@numStatus,0) > 0
		BEGIN
			UPDATE
				OpportunityMaster
			SET
				numStatus=@numStatus
			WHERE
				numDomainId=@numDomainID
				AND numOppId IN (SELECT DISTINCT OI.numOppId FROM @TEMPItems T1 INNER JOIN OpportunityItems OI ON T1.numOppItemID = OI.numoppitemtCode)
				AND NOT EXISTS (SELECT
									numOppItemtcode
								FROM
									OpportunityItems
								WHERE
									numOppId = OpportunityMaster.numOppId
									AND ISNULL(numWarehouseItmsID,0) > 0
									AND ISNULL(bitDropShip,0) = 0
									AND ISNULL(numUnitHour,0) <> ISNULL(numQtyPicked,0))
		END


		DECLARE @TempPickList TABLE
		(
			ID INT IDENTITY(1,1)
			,numOppID NUMERIC(18,0)
			,vcItems VARCHAR(MAX)
		)

		INSERT INTO @TempPickList
		(
			numOppID
			,vcItems
		)
		SELECT
			numOppID
			,CONCAT('<NewDataSet>',stuff((
					SELECT 
						CONCAT(' <BizDocItems><OppItemID>',numOppItemID,'</OppItemID>','<Quantity>',ISNULL(numQtyPicked,0),'</Quantity>','<Notes></Notes><monPrice>0</monPrice></BizDocItems>')
					FROM 
						@TempOrders TOR
					WHERE 
						TOR.numOppID = TORMain.numOppID
						AND ISNULL(numOppChildItemID,0) = 0
						AND ISNULL(numOppKitChildItemID,0) = 0
					FOR XML PATH, TYPE).value(N'.[1]', N'nvarchar(max)'),1,1,''),'</NewDataSet>')
		FROM
			@TempOrders TORMain
		GROUP BY
			numOppID


		SET @k = 1
		SELECT @kCount=COUNT(*) FROM @TempPickList
		DECLARE @numTempOppID NUMERIC(18,0)
		DECLARE @vcItems VARCHAR(MAX)
		DECLARE @intUsedShippingCompany NUMERIC(18,0)
		DECLARE @dtFromDate DATETIME
		DECLARE @numBizDocTempID NUMERIC(18,0)

		SELECT TOP 1
			@numBizDocTempID=numBizDocTempID
		FROM
			BizDocTemplate
		WHERE
			numDomainID=@numDomainID
			AND numBizDocID= 29397
		ORDER BY
			ISNULL(bitEnabled,0) DESC
			,ISNULL(bitDefault,0) DESC

		WHILE @k <= @kCount
		BEGIN
			SELECT @numTempOppID=numOppID,@vcItems=vcItems FROM @TempPickList WHERE ID=@k
			
			IF ISNULL(@vcItems,'') <> ''
			BEGIN
				SELECT 
					@intUsedShippingCompany = ISNULL(intUsedShippingCompany,0)
					,@dtFromDate = DATEADD(MINUTE,-1 * @ClientTimeZoneOffset,GETUTCDATE())
				FROM 
					OpportunityMaster
				WHERE
					numOppId=@numTempOppID

				EXEC USP_CreateBizDocs                                  
					 @numTempOppID,                        
					 29397,                        
					 @numUserCntID,                        
					 0,
					 '',
					 1,
					 @vcItems,
					 @intUsedShippingCompany,
					 '',
					 @dtFromDate,
					 0,
					 0,
					 '',
					 0,
					 0,
					 @ClientTimeZoneOffset,
					 0,
					 0,
					 @numBizDocTempID,
					 '',
					 '',
					 0, --(joseph) Added to Get Online Marketplace Sales Tax Percentage
					 0,
					 1,
					 0,
					 0,
					 0,
					 0,
					 NULL,
					 NULL,
					 0,
					 '',
					 0,
					 0,
					 0,
					 0,
					 0,
					 NULL,
					 0
			END

			SET @k = @k + 1
		END
	COMMIT
	END TRY
	BEGIN CATCH
		DECLARE @ErrorMessage NVARCHAR(4000)
		DECLARE @ErrorNumber INT
		DECLARE @ErrorSeverity INT
		DECLARE @ErrorState INT
		DECLARE @ErrorLine INT
		DECLARE @ErrorProcedure NVARCHAR(200)

		IF @@TRANCOUNT > 0
			ROLLBACK TRANSACTION;

		SELECT 
			@ErrorMessage = ERROR_MESSAGE(),
			@ErrorNumber = ERROR_NUMBER(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE(),
			@ErrorLine = ERROR_LINE(),
			@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

		RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);
	END CATCH
END
GO