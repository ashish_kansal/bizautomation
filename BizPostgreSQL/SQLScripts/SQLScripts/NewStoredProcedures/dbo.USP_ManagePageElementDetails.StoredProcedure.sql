
GO
/****** Object:  StoredProcedure [dbo].[USP_ManagePageElementDetails]    Script Date: 08/08/2009 16:16:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ManagePageElementDetails')
DROP PROCEDURE USP_ManagePageElementDetails
GO
CREATE PROCEDURE [dbo].[USP_ManagePageElementDetails]
          @numElementID NUMERIC(9),
          @numSiteID    NUMERIC(9),
          @numDomainID  NUMERIC(9),
          @strItems     TEXT  = NULL
AS
  BEGIN
    DELETE FROM PageElementDetail
    WHERE       numSiteID = @numSiteID
                AND numElementID = @numElementID;
    DECLARE  @hDocItem INT
    IF CONVERT(VARCHAR(10),@strItems) <> ''
      BEGIN
        EXEC sp_xml_preparedocument
          @hDocItem OUTPUT ,
          @strItems
        INSERT INTO PageElementDetail
                   (numElementID,
                    numAttributeID,
                    vcAttributeValue,
                    numSiteID,
                    numDomainID,vcHtml)
        SELECT @numElementID,
               X.numAttributeID,
               ISNULL(X.vcAttributeValue,''),
               @numSiteID,
               @numDomainID,X.vcHtml
        FROM   (SELECT *
                FROM   OPENXML (@hDocItem, '/NewDataSet/Item', 2)
                          WITH (numAttributeID   NUMERIC(9)  ,
                                vcAttributeValue VARCHAR(500),vcHtml text)) X
        EXEC sp_xml_removedocument
          @hDocItem
      END
 SELECT 0
  END

