USE [BizExch2007DB]
GO
/****** Object:  StoredProcedure [dbo].[USP_ManageWebAPIDetails]    Script Date: 03/25/2009 16:24:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by chintan prajapati
ALTER PROC [dbo].[USP_ManageWebAPIDetails]
          @numDomainID AS NUMERIC(9),
          @numWebApiId AS NUMERIC(9),
          @bitStatus   AS BIT
--           @str         AS TEXT
AS
  BEGIN
    IF (SELECT COUNT(* ) FROM   [WebAPIDetail] WHERE  [numDomainId] = @numDomainID AND [WebApiId] = @numWebApiId) > 0
      BEGIN
        UPDATE [WebAPIDetail]
        SET    [bitEnableAPI] = @bitStatus
        WHERE  [numDomainId] = @numDomainID AND [WebApiId] = @numWebApiId
      END
    ELSE
      BEGIN
        INSERT INTO [WebAPIDetail]
                   ([WebApiId],
                    [numDomainId],
                    [bitEnableAPI])
        VALUES     (@numWebApiId,
                    @numDomainID,
                    @bitStatus)
      END
  END
