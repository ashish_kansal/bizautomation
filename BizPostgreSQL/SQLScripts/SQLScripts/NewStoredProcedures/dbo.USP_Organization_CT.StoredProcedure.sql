/****** Object:  StoredProcedure [dbo].[USP_GetPageElementAttributes]    Script Date: 08/08/2009 16:15:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--EXEC [USP_GetPageElementAttributes] @numSiteID = 3,@numElementID =4
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_Organization_CT')
DROP PROCEDURE USP_Organization_CT
GO
-- =============================================
-- Author:		<Author,,Sachin Sadhu>
-- Create date: <Create Date,,23April2014>
-- Description:	<Description,,Change Tracking>
-- =============================================
Create PROCEDURE [dbo].[USP_Organization_CT]
	-- Add the parameters for the stored procedure here
    @numDomainID numeric(18, 0)=0,
    @numUserCntID numeric(18, 0)=0,
    @numRecordID numeric(18, 0)=0 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;	
	-- Insert statements for procedure here
	DECLARE @tblFields TABLE 
	(
		FieldName varchar(200),
		FieldValue varchar(200)
	)

	DECLARE @Type char(1)
	DECLARE @ChangeVersion bigint
	DECLARE @CreationVersion bigint
	DECLARE @last_synchronization_version bigInt=null

	SELECT 			
		@ChangeVersion=Ct.Sys_Change_version
		,@CreationVersion=Ct.Sys_Change_Creation_version
	FROM 
		CHANGETABLE(CHANGES dbo.DivisionMaster, 0) AS CT
	WHERE 
		numDivisionID=@numRecordID


	IF (@ChangeVersion=@CreationVersion)--Insert
	BEGIN
			SELECT 
				@Type = CT.SYS_CHANGE_OPERATION
			FROM 
				CHANGETABLE(CHANGES dbo.DivisionMaster, @last_synchronization_version) AS CT
			WHERE  
				CT.numDivisionID=@numRecordID
	END
	ELSE --Update
	BEGIN
		SET @last_synchronization_version=@ChangeVersion-1     

		SELECT 
			@Type = CT.SYS_CHANGE_OPERATION
		FROM 
			CHANGETABLE(CHANGES dbo.DivisionMaster, @last_synchronization_version) AS CT
		WHERE 
			CT.numDivisionID=@numRecordID

		--Fields Update & for Type :Insert or Update:
			Declare @UFFields as table 
			(
			    bitActiveInActive varchar(70),
				numAssignedBy varchar(70),
				numAssignedTo varchar(70),
				numCampaignID varchar(70),
				numCompanyDiff varchar(70),
				vcCompanyDiff varchar(70), 
				numCurrencyID varchar(70),
				numFollowUpStatus1 varchar(70),
				numFollowUpStatus varchar(70),
				numGrpID varchar(70),
				bintCreatedDate varchar(70),
				vcComFax varchar(70),
				bitPublicFlag varchar(70),
				tintCRMType varchar(70),
				bitNoTax varchar(70),
				numTerID varchar(70),
				numStatusID varchar(70),
				vcComPhone varchar(70),
				numAccountClassID varchar(70),
				numBillingDays VARCHAR(70),
				tintPriceLevel VARCHAR(70),
				numDefaultPaymentMethod VARCHAR(70),
				intShippingCompany VARCHAR(70),
				numDefaultShippingServiceID VARCHAR(70)
			)

		INSERT into @UFFields
		(
			bitActiveInActive,numAssignedBy,numAssignedTo,numCampaignID,numCompanyDiff,vcCompanyDiff,numCurrencyID,numFollowUpStatus1,numFollowUpStatus,numGrpID,bintCreatedDate
			,vcComFax,bitPublicFlag,tintCRMType,bitNoTax,numTerID,numStatusID,vcComPhone,numAccountClassID,numBillingDays,tintPriceLevel,numDefaultPaymentMethod,intShippingCompany,
			numDefaultShippingServiceID
		)
		SELECT
		CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.DivisionMaster'), 'bitActiveInActive', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS bitActiveInActive ,
		CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.DivisionMaster'), 'numAssignedBy', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS numAssignedBy, 
		CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.DivisionMaster'), 'numAssignedTo', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS numAssignedTo, 
		CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.DivisionMaster'), 'numCampaignID', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS numCampaignID, 
		CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.DivisionMaster'), 'numCompanyDiff', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS numCompanyDiff	,
		CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.DivisionMaster'), 'vcCompanyDiff', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS  vcCompanyDiff ,
		CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.DivisionMaster'), 'numCurrencyID', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS numCurrencyID, 
        CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.DivisionMaster'), 'numFollowUpStatus1', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS numFollowUpStatus1,
        CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.DivisionMaster'), 'numFollowUpStatus', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS numFollowUpStatus,
		CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.DivisionMaster'), 'numGrpID', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS numGrpID,
		CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.DivisionMaster'), 'bintCreatedDate', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS bintCreatedDate,
		CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.DivisionMaster'), 'vcComFax', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS vcComFax,
		CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.DivisionMaster'), 'bitPublicFlag', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS bitPublicFlag,
		CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.DivisionMaster'), 'tintCRMType', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS tintCRMType,
		CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.DivisionMaster'), 'bitNoTax', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS bitNoTax,
		CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.DivisionMaster'), 'numTerID', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS numTerID,
		CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.DivisionMaster'), 'numStatusID', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS numStatusID,
		CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.DivisionMaster'), 'vcComPhone', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS vcComPhone,
		CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.DivisionMaster'), 'numAccountClassID', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS numAccountClassID,
		CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.DivisionMaster'), 'numBillingDays', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS numBillingDays,
		CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.DivisionMaster'), 'tintPriceLevel', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS tintPriceLevel,
		CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.DivisionMaster'), 'numDefaultPaymentMethod', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS numDefaultPaymentMethod,
		CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.DivisionMaster'), 'intShippingCompany', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS intShippingCompany,
		CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.DivisionMaster'), 'numDefaultShippingServiceID', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS numDefaultShippingServiceID
		FROM 
		CHANGETABLE(CHANGES dbo.DivisionMaster, @last_synchronization_version) AS CT
		WHERE 
		ct.numDivisionID=@numRecordID

		--sp_helptext USP_GetWorkFlowFormFieldMaster
	--exec USP_GetWorkFlowFormFieldMaster 1,68
		INSERT INTO @tblFields
		(
			FieldName
			,FieldValue
		)
		SELECT
			FieldName,
			FieldValue
		FROM
			(
				SELECT 
					bitActiveInActive,numAssignedBy,numAssignedTo,numCampaignID,numCompanyDiff,vcCompanyDiff,numCurrencyID,numFollowUpStatus1,numFollowUpStatus,numGrpID,bintCreatedDate
					,vcComFax,bitPublicFlag,tintCRMType,bitNoTax,numTerID,numStatusID,vcComPhone,numAccountClassID,numBillingDays,tintPriceLevel,numDefaultPaymentMethod,intShippingCompany
					,numDefaultShippingServiceID
				FROM @UFFields	
			) AS UP
		UNPIVOT
		(
			FieldValue FOR FieldName IN (bitActiveInActive,numAssignedBy,numAssignedTo,numCampaignID,numCompanyDiff,vcCompanyDiff,numCurrencyID,numFollowUpStatus1,numFollowUpStatus
			,numGrpID,bintCreatedDate,vcComFax,bitPublicFlag,tintCRMType,bitNoTax,numTerID,numStatusID,vcComPhone,numAccountClassID,numBillingDays,tintPriceLevel
			,numDefaultPaymentMethod,intShippingCompany,numDefaultShippingServiceID)
		) AS upv
		WHERE 
			FieldValue<>0
	END

	DECLARE @tintWFTriggerOn AS TINYINT
	SET @tintWFTriggerOn=CASE @Type WHEN 'I' THEN 1 WHEN 'U' THEN 2 WHEN 'D' THEN 5 END

	--DateField WorkFlow Logic

	Declare @TempbintCreatedDate AS VARCHAR(70)
	SET @TempbintCreatedDate = (SELECT bintCreatedDate FROM DivisionMaster WHERE numDivisionID=@numRecordID)	
	
	Declare @TempnumDivisionId AS NUMERIC
	IF @tintWFTriggerOn = 1
	BEGIN
		IF EXISTS (SELECT numDivisionID FROM DivisionMaster_TempDateFields WHERE numDivisionID=@numRecordID)
		BEGIN
			IF (@TempbintCreatedDate >= DATEADD(DAY,-30,GETUTCDATE()) AND @TempbintCreatedDate <= DATEADD(DAY,90,GETUTCDATE()) )
			BEGIN
				UPDATE DivisionMaster_TempDateFields
				SET bintCreatedDate = @TempbintCreatedDate
				WHERE numDivisionID = @numRecordID
			END
			ELSE
			BEGIN
				DELETE FROM DivisionMaster_TempDateFields WHERE numDivisionID = @numRecordID
			END
		END
		ELSE
		BEGIN
			INSERT INTO [DivisionMaster_TempDateFields] (numDivisionID, numDomainID, bintCreatedDate) 
			SELECT numDivisionID,numDomainID, bintCreatedDate
			FROM DivisionMaster
			WHERE (
					(bintCreatedDate >= DATEADD(DAY,-30,GETUTCDATE()) AND bintCreatedDate <= DATEADD(DAY,90,GETUTCDATE()) )
				  )
				AND numDivisionID=@numRecordID
		END
	END
	ELSE IF @tintWFTriggerOn = 2
	BEGIN
		
		SET @TempnumDivisionId = (SELECT numDivisionID FROM DivisionMaster_TempDateFields WHERE numDivisionID=@numRecordID)

		IF(@TempnumDivisionId IS NOT NULL)
		BEGIN
			IF (@TempbintCreatedDate >= DATEADD(DAY,-30,GETUTCDATE()) AND @TempbintCreatedDate <= DATEADD(DAY,90,GETUTCDATE()) )
			BEGIN
				UPDATE DivisionMaster_TempDateFields
				SET bintCreatedDate = @TempbintCreatedDate
				WHERE numDivisionID = @numRecordID
			END
			ELSE
			BEGIN
				DELETE FROM DivisionMaster_TempDateFields WHERE numDivisionID = @numRecordID
			END
		END
	END

	ELSE IF @tintWFTriggerOn = 5
	BEGIN
		DELETE FROM DivisionMaster_TempDateFields WHERE numDivisionID = @numRecordID
	END	

	--DateField WorkFlow Logic

	Declare @numAssignedTo numeric(18,0)
	Declare @numAssignedBy numeric(18,0)
	Declare @numWebLead numeric(18,0)
	Declare @bitLeadBoxFlg bit

	Select @numAssignedTo=numAssignedTo,@numAssignedBy=numAssignedBy,@numWebLead=numGrpId,@bitLeadBoxFlg=bitLeadBoxFlg from DivisionMaster where numDivisionID=@numRecordID

	--For Fields Update Exection Point
	DECLARE @Columns_Updated VARCHAR(1000)
	SELECT @Columns_Updated = COALESCE(@Columns_Updated + ',', '') + FieldName FROM @tblFields 
	SET @Columns_Updated=ISNULL(@Columns_Updated,'')

	select @Columns_Updated,@numAssignedTo,@numAssignedBy,@numWebLead
	IF (@Columns_Updated='numAssignedBy,numAssignedTo')
	BEGIN	
		IF (@numWebLead=1) --web Leads
		BEGIN
			IF	(@bitLeadBoxFlg=1) --(through System)
			BEGIN
				IF(@numAssignedTo=0 AND @numAssignedBy=0)
				BEGIN
					SET @tintWFTriggerOn=1
				END
				ELSE
				BEGIN
					SET @tintWFTriggerOn=2
				END
			END
			ELSE		--(through Web)			
			BEGIN
				IF(@numAssignedTo>0 AND @numAssignedBy>0)
				BEGIN
					SET @tintWFTriggerOn=1
				END
				ELSE
				BEGIN
					SET @tintWFTriggerOn=2
				END
			END
		END

		ELSE    --Normal Leads
		BEGIN
			IF	(@numAssignedTo=0 AND @numAssignedBy=0)
			BEGIN
				SET @tintWFTriggerOn=1
			END
			ELSE
			BEGIN
				SET @tintWFTriggerOn=2
			END
		END
	END

--Adding data As per Opertaion
	EXEC dbo.USP_ManageWorkFlowQueue
	@numWFQueueID = 0, --  numeric(18, 0)
	@numDomainID = @numDomainID, --  numeric(18, 0)
	@numUserCntID = @numUserCntID, --  numeric(18, 0)
	@numRecordID = @numRecordID, --  numeric(18, 0)
	@numFormID = 68, --  numeric(18, 0)
	@tintProcessStatus = 1, --  tinyint
	@tintWFTriggerOn = @tintWFTriggerOn, --  tinyint
	@tintMode = 1, --  tinyint
	@vcColumnsUpdated = @Columns_Updated

	
/***************************************************Commented SP for Checking RowVersion with Change Tracking:Date:23Apr||Sachin********************************************************/



--DECLARE @synchronization_version BIGINT 



--SET @synchronization_version = CHANGE_TRACKING_CURRENT_VERSION();











--SELECT 



--    CT.numOppID, CT.SYS_CHANGE_OPERATION, 



--    CT.SYS_CHANGE_COLUMNS, CT.SYS_CHANGE_CONTEXT, 



--    CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.OpportunityMaster'), 'numOppID', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS numOppID, 



--    CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.OpportunityMaster'), 'vcPOppName', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS vcPOppName 



--FROM 



--    CHANGETABLE(CHANGES dbo.OpportunityMaster, @synchronization_version) AS CT











--SET @synchronization_version = CHANGE_TRACKING_CURRENT_VERSION();



/***************************************************End of Comment||Sachin********************************************************/



END


