--USP_GetPriceTable 360
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ManagePricingNamesTable')
DROP PROCEDURE USP_ManagePricingNamesTable
GO
CREATE PROCEDURE USP_ManagePricingNamesTable
    @numDomainID NUMERIC,
    @numCreatedBy AS NUMERIC,
    @strItems TEXT
AS 
BEGIN
	DELETE  FROM [PricingNamesTable] WHERE numDomainID= @numDomainID
		
    DECLARE @hDocItem INT

	IF CONVERT(VARCHAR(10), @strItems) <> '' 
    BEGIN
        EXEC sp_xml_preparedocument @hDocItem OUTPUT, @strItems
          
        INSERT  INTO [PricingNamesTable]
        (
            numDomainID,
            [numCreatedBy],
            [dtCreatedDate],
            [tintPriceLevel],
            [vcPriceLevelName]
        )
        SELECT  
			@numDomainID,
			@numCreatedBy,
			GETUTCDATE(),
			X.[tintPriceLevel],
			X.vcPriceLevelName
        FROM    
		( 
			SELECT * FROM 
			OPENXML (@hDocItem, '/NewDataSet/Item', 2)
            WITH ( tintPriceLevel TINYINT, vcPriceLevelName VARCHAR(300))
        ) X

        EXEC sp_xml_removedocument @hDocItem
    END

END