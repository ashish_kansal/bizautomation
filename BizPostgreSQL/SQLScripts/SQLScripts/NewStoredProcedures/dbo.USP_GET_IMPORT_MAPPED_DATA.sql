GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GET_IMPORT_MAPPED_DATA')
DROP PROCEDURE USP_GET_IMPORT_MAPPED_DATA
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- USP_GET_IMPORT_MAPPED_DATA 1,1
CREATE PROCEDURE [dbo].[USP_GET_IMPORT_MAPPED_DATA] 
(
	@intImportFileID	BIGINT,
	@numDomainID		BIGINT
)
AS 
BEGIN
	DECLARE @strSQL AS NVARCHAR(MAX)
	DECLARE @strWHERE AS VARCHAR(MAX)
	
	SET @strWHERE = ' WHERE 1=1 '
	
	IF ISNULL(@intImportFileID,0) <> 0
		BEGIN
			SET @strWHERE = @strWHERE + ' AND PARENT.intImportFileID = ' + CONVERT(VARCHAR,@intImportFileID)
		END
		
		SET @strSQL = 'SELECT   PARENT.intImportFileID AS [ImportFileID],
			   					IMPORT.vcImportName AS [ImportType],
								PARENT.vcImportFileName AS [ImportFileName],
								(CASE 
									WHEN PARENT.tintStatus = 0 THEN ''PENDING''
									WHEN PARENT.tintStatus = 1 THEN ''COMPLETED''
									WHEN PARENT.tintStatus = 2 THEN ''ERROR''	
								END) AS [Status],								
								dbo.FormatedDateTimeFromDate(PARENT.dtCreateDate,numDomainId) AS [CreatedDate],
								dbo.fn_GetContactName(PARENT.numUserContactID) AS [CreatedBy] ,
								ISNULL((CASE 
									WHEN PARENT.tintStatus = 1 
										THEN 
											CASE
												WHEN (
															ISNULL(PARENT.numRecordAdded,0) <> 0 
														AND ISNULL(PARENT.numRecordUpdated,0) = 0 
														AND ISNULL(PARENT.numRecordUpdated,0) = 0 
														AND ISNULL(PARENT.numErrors,0) = 0
													  )
												THEN CONVERT(VARCHAR,PARENT.numRecordAdded) + '' Records Added''
												WHEN ISNULL(PARENT.numErrors,0) = 0
												THEN CONVERT(VARCHAR,PARENT.numRecordAdded) + '' Errors''
											END		
								END),''-'') AS [Records],
								PARENT.tintStatus AS [intStatus],
								PARENT.numMasterID AS [MasterID]
						FROM dbo.Import_File_Master PARENT 
						INNER JOIN dbo.Import_Master IMPORT ON PARENT.numMasterID = IMPORT.intImportMasterID'
						+ @strWHERE + ' AND PARENT.numDomainId = ' + CONVERT(VARCHAR,@numDomainID)
		PRINT @strSQL
		EXEC SP_EXECUTESQL @strSQL
		
		-------------------------------------------------------------------------------------------------------------
		       		
		SELECT  CHILD.numFormFieldID AS [FormFieldID],
				CHILD.intMapColumnNo AS [ImportFieldID],
				CHILD.intImportFileID,
				CHILD.intImportTransactionID,
				CASE WHEN ISNULL(bitCustomField,0) = 1
					 THEN CFW.Fld_label
					 ELSE DYNAMIC_FIELD.vcDbColumnName
				END AS [dbFieldName],	 
				--DYNAMIC_FIELD.vcDbColumnName AS [dbFieldName],
				CASE WHEN ISNULL(bitCustomField,0) = 1
					 THEN CFW.Fld_label
					 ELSE DYNAMIC_FIELD.vcFormFieldName
				END AS [FormFieldName],	 
				--DYNAMIC_FIELD.vcFormFieldName AS [FormFieldName],
				CASE WHEN CFW.fld_Type = 'Drop Down List Box'
					 THEN 'SelectBox'
					 ELSE ISNULL(DYNAMIC_FIELD.vcAssociatedControlType,'') 
				END	 vcAssociatedControlType,
				ISNULL(DYNAMIC_FIELD.bitDefault,0) bitDefault,
				ISNULL(DYNAMIC_FIELD.vcFieldType,'') vcFieldType,
				ISNULL(DYNAMIC_FIELD.vcListItemType,'') vcListItemType,
				ISNULL(DYNAMIC_FIELD.vcLookBackTableName,'') vcLookBackTableName,
				CASE WHEN ISNULL(bitCustomField,0) = 1
					 THEN CFW.Fld_label
					 ELSE DYNAMIC_FIELD.vcOrigDbColumnName
				END AS [vcOrigDbColumnName],
				ISNULL(DYNAMIC_FIELD.vcPropertyName,'') vcPropertyName,
				ISNULL(bitCustomField,0) AS [bitCustomField],
			    PARENT.numDomainID AS [DomainID]
		FROM Import_File_Field_Mapping CHILD 
		INNER JOIN dbo.Import_File_Master PARENT ON PARENT.intImportFileID = CHILD.intImportFileID
		INNER JOIN dbo.Import_Master IMPORT ON IMPORT.intImportMasterID = PARENT.numMasterID
		LEFT JOIN DynamicFormFieldMaster DYNAMIC_FIELD ON CHILD.numFormFieldID = DYNAMIC_FIELD.numFormFieldId 
		LEFT JOIN dbo.CFW_Fld_Master CFW ON CHILD.numFormFieldID = CFW.Fld_id
		WHERE CHILD.intImportFileID = @intImportFileID

END		