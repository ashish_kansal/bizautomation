GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_MassSalesFulfillment_GetShippingBizDocForPacking')
DROP PROCEDURE dbo.USP_MassSalesFulfillment_GetShippingBizDocForPacking
GO

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[USP_MassSalesFulfillment_GetShippingBizDocForPacking]
(
	@numDomainID NUMERIC(18,0)
	,@numUserCntID NUMERIC(18,0)
	,@numOppID NUMERIC(18,0)
	,@numOppBizDocID NUMERIC(18,0)
	,@numOppItemID NUMERIC(18,0)
)
AS
BEGIN
	DECLARE @numShippingServiceItemID NUMERIC(18,0)
	DECLARE @monCurrentShipRate DECIMAL(20,5)

	SELECT 
		@numShippingServiceItemID = ISNULL(numShippingServiceItemID,0)
	FROM 
		Domain 
	WHERE 
		numDomainId=@numDomainID 

	SET @monCurrentShipRate = ISNULL((SELECT TOP 1 monPrice FROM OpportunityItems OIInner WHERE OIInner.numOppID=@numOppID AND numItemCode=@numShippingServiceItemID),-1)
	


	IF EXISTS (SELECT numShippingReportId FROM ShippingReport WHERE numOppID=@numOppID AND numOppBizDocId=@numOppBizDocID)
	BEGIN
		DECLARE @numShippingReportID NUMERIC(18,0)
		DECLARE @vcPOppName VARCHAR(300)

		SELECT TOP 1  
			@numShippingReportID = numShippingReportId
			,@vcPOppName=OpportunityMaster.vcPOppName
		FROM 
			ShippingReport 
		INNER JOIN
			OpportunityMaster
		ON
			ShippingReport.numOppID = OpportunityMaster.numOppId
		WHERE 
			ShippingReport.numOppID=@numOppID 
			AND ShippingReport.numOppBizDocId=@numOppBizDocID


		SELECT 
			@numShippingReportID AS numShippingReportID
			,@vcPOppName vcPOppName
			,numBoxID
			,numShippingReportId
			,vcBoxName
			,fltTotalWeight
			,fltHeight
			,fltWidth
			,fltLength
			,@monCurrentShipRate monCurrentShipRate
		FROM 
			ShippingBox 
		WHERE 
			numShippingReportId = @numShippingReportID


		SELECT 
			ShippingReportItems.numBoxID
			,ShippingReportItems.numOppBizDocItemID
			,Item.numItemCode
			,Item.vcItemName
			,ShippingReportItems.intBoxQty 
		FROM 
			ShippingReportItems 
		INNER JOIN 
			Item 
		ON 
			ShippingReportItems.numItemCode=Item.numItemCode 
		WHERE 
			numShippingReportId = @numShippingReportID
	END
	ELSE
	BEGIN
		SELECT 
			OM.numOppID
			,ISNULL(OM.intUsedShippingCompany,ISNULL(DM.intShippingCompany,0)) numShipVia
			,ISNULL(OM.numShippingService,ISNULL(DM.numDefaultShippingServiceID,0)) numShipService
			,OBD.numOppBizDocsId
			,OBDI.numOppBizDocItemID
			,OM.vcPOppName
			,I.numContainer
			,ISNULL(IContainer.vcItemName,'') vcContainer
			,(CASE WHEN ISNULL(I.numContainer,0) > 0 THEN I.numNoItemIntoContainer ELSE OI.numUnitHour END) AS numNoItemIntoContainer
			,ISNULL(IContainer.fltWeight,0) fltContainerWeight
			,ISNULL(IContainer.fltHeight,0) fltContainerHeight
			,ISNULL(IContainer.fltWidth,0) fltContainerWidth
			,ISNULL(IContainer.fltLength,0) fltContainerLength
			,OI.numoppitemtCode
			,I.numItemCode
			,I.vcItemName
			,ISNULL(I.fltWeight,0) fltItemWeight
			,ISNULL(I.fltHeight,0) fltItemHeight
			,ISNULL(I.fltWidth,0) fltItemWidth
			,ISNULL(I.fltLength,0) fltItemLength
			,ISNULL(OBDI.numUnitHour,0) numTotalQty
			,@monCurrentShipRate monCurrentShipRate
		FROM 
			OpportunityBizDocs OBD
		INNER JOIN
			OpportunityBizDocItems OBDI
		ON
			OBD.numOppBizDocsId = OBDI.numOppBizDocID
		INNER JOIN
			OpportunityMaster OM
		ON
			OBD.numOppId = OM.numOppId
		INNER JOIN
			DivisionMaster DM
		ON
			OM.numDivisionId = DM.numDivisionID
		INNER JOIN
			OpportunityItems OI
		ON
			OM.numOppId = OI.numOppId
			AND OBDI.numOppItemID = OI.numoppitemtCode
		INNER JOIN 
			Item AS I
		ON 
			OI.numItemCode = I.numItemCode
		LEFT JOIN
			Item AS IContainer
		ON
			I.numContainer = IContainer.numItemCode
		WHERE
			OM.numOppId=@numOppID
			AND OBD.numOppBizDocsId = @numOppBizDocID
			AND ISNULL(I.bitContainer,0) = 0
			AND (OI.numoppitemtCode=@numOppItemID OR ISNULL(@numOppItemID,0) = 0)
	END	
END
GO