
GO
/****** Object:  StoredProcedure [dbo].[usp_GetDocumentStatus]    Script Date: 02/12/2010 14:39:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--exec usp_GetDocumentStatus @numDomain=72
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_GetDocumentStatus')
DROP PROCEDURE usp_GetDocumentStatus
GO
CREATE PROCEDURE [dbo].[usp_GetDocumentStatus]
@numDomain numeric(9)
as
begin

CREATE TABLE #TempList
(vcData varchar(250),
numListItemID numeric(18,0),
intSortId numeric(1));

insert into #TempList

select vcData,numListItemID,-2 from listdetails where numListID=29 and  (constFlag=1 or  numDomainID=@numDomain) 
union
select vcData,numListItemID,-2 from listdetails where numListID=28 and  (constFlag=1 or  numDomainID=@numDomain) 
union
select vcData,numListItemID,-3 from listdetails where numListID=27 and  (constFlag=1 or numDomainID=@numDomain) 

select * from #TempList order by intSortId,numListItemID,vcData

select  vcData,numListItemID from listdetails where numListID=11 and numDomainID=@numDomain
order by vcData

drop table #TempList;
end

