GO
/****** Object:  StoredProcedure [dbo].[USP_OrderAutomationPO]    Script Date: 03/09/2010 15:47:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_OrderAutomationPO')
DROP PROCEDURE USP_OrderAutomationPO
GO
CREATE PROCEDURE [dbo].[USP_OrderAutomationPO]
@numOppBizDocId numeric(9)

AS

BEGIN

	DECLARE @numOppId NUMERIC(9)
	DECLARE @monPaidAmount DECIMAL(20,5)
	DECLARE @monDealAmount DECIMAL(20,5)
	DECLARE @numDefBizDocId NUMERIC(9)
	DECLARE @numDomainId NUMERIC(9)
	DECLARE @numOppBizDocItemId NUMERIC(9)
	DECLARE @numBizDocId NUMERIC(9)
	DECLARE @bitDropShip bit
	DECLARE @numItemCode NUMERIC(9)

	SELECT  @numOppId = numOppId,@numBizDocId=numBizDocId,@monPaidAmount=monAmountPaid
    FROM    OpportunityBizDocs
    WHERE   numOppBizDocsId = @numOppBizDocId

	

	select @numDomainId=numDomainId from OpportunityMaster where numOppId=@numOppId



	select 	@monDealAmount=dbo.[GetDealAmount](@numOppId, GETDATE(),
                                                @numOppBizDocId)


	select @numDefBizDocId =numAuthoritativeSales from AuthoritativeBizDocs where numDomainID=@numDomainId



declare BizDocOrderAuto CURSOR FOR
Select numOppBizDocItemId,bitDropShip,numItemCode from OpportunityBizDocItems where numOppBizDocId=@numOppBizDocId

OPEN BizDocOrderAuto

	FETCH   NEXT FROM BizDocOrderAuto Into @numOppBizDocItemId,@bitDropShip,@numItemCode;
	WHILE @@FETCH_STATUS = 0
	   BEGIN				
			
			if isnull(@bitDropShip,0)=1 and @numDefBizDocId=@numBizDocId
				begin
					EXEC USP_MangeAutoPOGeneration
					@numParentOppID = @numOppId,
					@numOppBizDocID =@numOppBizDocId,
					@numItemCode = @numItemCode,
					@numDomainID =@numDomainId,
					@numOppBizDocItemID=@numOppBizDocItemID,
					@monDealAmount=@monDealAmount,
					@monPaidAmount=@monPaidAmount
					
				end
			else IF @numDefBizDocId=@numBizDocId
				begin
					EXEC USP_MangeAutoPOGenerationRule2
					@numParentOppID = @numOppId,
					@numOppBizDocID =@numOppBizDocId,
					@numItemCode = @numItemCode,
					@numDomainID =@numDomainId,
					@numOppBizDocItemID=@numOppBizDocItemID,
					@monDealAmount=@monDealAmount,
					@monPaidAmount=@monPaidAmount
				end	
				
			FETCH  NEXT  FROM BizDocOrderAuto into @numOppBizDocItemId,@bitDropShip,@numItemCode
		END

	close BizDocOrderAuto;
	DEALLOCATE BizDocOrderAuto;
END
