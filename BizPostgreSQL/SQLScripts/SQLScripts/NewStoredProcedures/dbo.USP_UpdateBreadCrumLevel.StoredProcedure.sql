
GO
/****** Object:  StoredProcedure [dbo].[USP_UpdateBreadCrumLevel]    Script Date: 08/05/2009 23:20:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_UpdateBreadCrumLevel')
DROP PROCEDURE USP_UpdateBreadCrumLevel
GO
CREATE PROCEDURE [dbo].[USP_UpdateBreadCrumLevel]
	@strItems TEXT=null
AS
BEGIN
	DECLARE  @hDocItem INT
      IF CONVERT(VARCHAR(10),@strItems) <> ''
        BEGIN
          EXEC sp_xml_preparedocument
            @hDocItem OUTPUT ,@strItems
            
          UPDATE [SiteBreadCrumb] SET [SiteBreadCrumb].[tintLevel] = X.tintLevel
          FROM   OPENXML (@hDocItem, '/NewDataSet/Item', 2)
                            WITH (numBreadCrumbID NUMERIC(18),tintLevel int) X
		  WHERE [SiteBreadCrumb].[numBreadCrumbID] = X.numBreadCrumbID
          EXEC sp_xml_removedocument
            @hDocItem
        END
END