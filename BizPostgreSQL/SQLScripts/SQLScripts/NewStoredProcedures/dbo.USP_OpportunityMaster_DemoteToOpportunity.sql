--modified by anoop jayaraj              
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_OpportunityMaster_DemoteToOpportunity')
DROP PROCEDURE USP_OpportunityMaster_DemoteToOpportunity
GO
CREATE PROCEDURE [dbo].[USP_OpportunityMaster_DemoteToOpportunity]
	@numDomainID NUMERIC(18,0),
	@numUserCntID NUMERIC(18,0),
	@numOppId NUMERIC(18,0)                   
AS                          
BEGIN             
	IF EXISTS(SELECT * FROM OpportunityMaster WHERE numOppID=@numOppId and numDomainID=@numDomainID)         
	BEGIN 
		SET XACT_ABORT ON;

		BEGIN TRY 
		BEGIN TRAN

			IF (SELECT COUNT(*) FROM MassSalesOrderQueue WHERE numOppID = @numOppId AND ISNULL(bitExecuted,0) = 0) > 0
			BEGIN
				RAISERROR ( 'MASS SALES ORDER', 16, 1 )
				RETURN
			END
			IF (SELECT COUNT(*) FROM RecurrenceConfiguration WHERE numOppID = @numOppId AND numDomainID=@numDomainID AND ISNULL(bitMarkForDelete,0) = 0) > 0
			BEGIN
				RAISERROR ( 'RECURRING ORDER OR BIZDOC', 16, 1 )
				RETURN
			END
			ELSE IF (SELECT COUNT(*) FROM RecurrenceTransaction INNER JOIN RecurrenceConfiguration ON RecurrenceTransaction.numRecConfigID = RecurrenceConfiguration.numRecConfigID WHERE RecurrenceTransaction.numRecurrOppID = @numOppId AND ISNULL(RecurrenceConfiguration.bitDisabled,0) = 0) > 0
			BEGIN	
				RAISERROR ( 'RECURRED ORDER', 16, 1 ) ;
				RETURN ;
			END
			ELSE IF (SELECT COUNT(*) FROM OpportunityBizdocs WHERE numOppId=@numOppId AND numBizDocId=296 AND ISNULL(bitFulfilled,0) = 1) > 0
			BEGIN
				RAISERROR ( 'FULFILLED_ITEMS', 16, 1 ) ;
				RETURN ;
			END

			IF EXISTS (SELECT * FROM dbo.ReturnHeader WHERE numOppId = @numOppId AND numDomainId= @numDomainID)         
			BEGIN
				RAISERROR ( 'RETURN_EXIST', 16, 1 ) ;
				RETURN ;
			END	

     
			DECLARE @tintError TINYINT;SET @tintError=0

			EXECUTE USP_CheckCanbeDeleteOppertunity @numOppId,@tintError OUTPUT
  
			IF @tintError=1
			BEGIN
  				RAISERROR ( 'OppItems DEPENDANT', 16, 1 ) ;
				RETURN ;
			END
	
			--Credit Balance
			Declare @monCreditAmount as DECIMAL(20,5);Set @monCreditAmount=0
			Declare @monCreditBalance as DECIMAL(20,5);Set @monCreditBalance=0
			Declare @numDivisionID as numeric(9);Set @numDivisionID=0
			Declare @tintOppType as tinyint

			Select @numDivisionID=numDivisionID,@tintOppType=tintOppType from OpportunityMaster WHERE  numOppId=@numOppId  and numDomainID= @numDomainID 
   
			IF @tintOppType=1 --Sales
			BEGIN
			  Select @monCreditBalance=isnull(monSCreditBalance,0) from DivisionMaster WHERE  numDivisionID=@numDivisionID  and numDomainID= @numDomainID 
			END
			ELSE IF @tintOppType=2 --Purchase
			BEGIN
				UPDATE OpportunityMaster SET numReleaseStatus = 1 WHERE numDomainId=@numDomainID AND numOppId IN (SELECT DISTINCT numOppId FROM OpportunityItemsReleaseDates WHERE  numPurchasedOrderID = @numOppId)
				UPDATE OpportunityItemsReleaseDates SET tintStatus = 1, numPurchasedOrderID=0 WHERE numPurchasedOrderID = @numOppId
			END

			SELECT @monCreditBalance=isnull(monPCreditBalance,0) from DivisionMaster WHERE  numDivisionID=@numDivisionID  and numDomainID= @numDomainID 
   
			SELECT @monCreditAmount=sum(monAmount) from CreditBalanceHistory where numReturnID in (select numReturnID from [Returns] WHERE [numOppId] = @numOppId)
	
			IF (@monCreditAmount > @monCreditBalance)         
			BEGIN
				RAISERROR ( 'CreditBalance DEPENDANT', 16, 1 ) ;
				RETURN ;
			END	
        
			DECLARE @tintOppStatus AS TINYINT
			DECLARE @tintShipped AS TINYINT                
			SELECT @tintOppStatus=tintOppStatus,@tintShipped=tintShipped from OpportunityMaster where numOppID=@numOppId                
  
			DECLARE @isOrderItemsAvailable AS INT = 0

			SELECT 
				@isOrderItemsAvailable = COUNT(*) 
			FROM    
				OpportunityItems OI
			JOIN 
				dbo.OpportunityMaster OM ON OI.numOppId = OM.numOppId
			JOIN 
				Item I ON OI.numItemCode = I.numItemCode
			WHERE   
				(charitemtype='P' OR 1=(CASE WHEN tintOppType=1 THEN 
										CASE WHEN ISNULL(I.bitKitParent,0) = 1 AND ISNULL(I.bitAssembly,0) = 1 THEN 0
											 WHEN ISNULL(I.bitKitParent,0) = 1 THEN 1
											 ELSE 0 END 
										ELSE 0 END)) AND OI.numOppId = @numOppId
									   AND ( bitDropShip = 0
											 OR bitDropShip IS NULL
										   ) 

			IF (@tintOppStatus=1 and @tintShipped=0) OR  @tintShipped=1               
			BEGIN    
				EXEC USP_RevertDetailsOpp @numOppId,0,2,@numUserCntID                
			END                                 
                        
			DELETE FROM [OpportunitySalesTemplate] WHERE [numOppId] =@numOppId 
			DELETE OpportunityLinking WHERE numChildOppID=@numOppId OR numParentOppID=@numOppId                    
                

			IF @tintOppType = 1 AND @tintOppStatus = 1 -- SALES ORDER
			BEGIN
				-- MAKE SERIAL/LOT# NUUMBER AVAILABLE FOR USE AS SALES ORDER IS DELETED
				UPDATE WHIDL
					SET WHIDL.numQty = (CASE WHEN Item.bitLotNo = 1 THEN ISNULL(WHIDL.numQty,0) + ISNULL(OWSI.numQty,0) ELSE 1 END)
				FROM 
					WareHouseItmsDTL WHIDL
				INNER JOIN
					OppWarehouseSerializedItem OWSI
				ON
					WHIDL.numWareHouseItmsDTLID = OWSI.numWarehouseItmsDTLID
					AND WHIDL.numWareHouseItemID = OWSI.numWarehouseItmsID
				INNER JOIN
					WareHouseItems
				ON
					WHIDL.numWareHouseItemID = WareHouseItems.numWareHouseItemID
				INNER JOIN
					Item
				ON
					WareHouseItems.numItemID = Item.numItemCode
				WHERE
					OWSI.numOppID = @numOppId
			END
			ELSE IF @tintOppType = 2 AND @tintOppStatus = 1 -- PURCHASE ORDER
			BEGIN
				IF (
					SELECT 
						COUNT(*)
					FROM 
						OppWarehouseSerializedItem OWSI 
					INNER JOIN 
						OpportunityMaster OM 
					ON 
						OWSI.numOppID=OM.numOppId 
						AND tintOppType=1 
					WHERE 
						OWSI.numWarehouseItmsDTLID IN (SELECT numWarehouseItmsDTLID FROM OppWarehouseSerializedItem OWSIInner WHERE OWSIInner.numOppID = @numOppID)
					) > 0
				BEGIN
					RAISERROR ('SERIAL/LOT#_USED', 16, 1 ) ;
				END
				ELSE
				BEGIN
					-- REMOVE SERIAL/LOT# NUMBER FROM INVENTORY BECAUSE PURCHASE ORDER IS DELETED
					UPDATE WHIDL
						SET WHIDL.numQty = (CASE WHEN Item.bitLotNo = 1 THEN ISNULL(WHIDL.numQty,0) - ISNULL(OWSI.numQty,0) ELSE 0 END)
					FROM 
						WareHouseItmsDTL WHIDL
					INNER JOIN
						OppWarehouseSerializedItem OWSI
					ON
						WHIDL.numWareHouseItmsDTLID = OWSI.numWarehouseItmsDTLID
						AND WHIDL.numWareHouseItemID = OWSI.numWarehouseItmsID
					INNER JOIN
						WareHouseItems
					ON
						WHIDL.numWareHouseItemID = WareHouseItems.numWareHouseItemID
					INNER JOIN
						Item
					ON
						WareHouseItems.numItemID = Item.numItemCode
					WHERE
						OWSI.numOppID = @numOppId
				END
			END
									        
	                
		  DELETE FROM OpportunityItemLinking where numNewOppID=@numOppId                      
                         
	  


		  DELETE FROM OpportunityBizDocKitItems WHERE [numOppBizDocItemID] IN 
		  (SELECT [numOppBizDocItemID] FROM [OpportunityBizDocItems] WHERE [numOppBizDocID] IN 
			(select numOppBizDocsId from OpportunityBizDocs where numOppId=@numOppId))
	
		  delete from OpportunityBizDocItems where numOppBizDocID in                        
		  (select numOppBizDocsId from OpportunityBizDocs where numOppId=@numOppId)

		  DELETE FROM OpportunityBizDocDtl WHERE numBizDocID in                        
		  (select numOppBizDocsId from OpportunityBizDocs where numOppId=@numOppId)                        
           
		   DELETE FROM OpportunityBizDocsPaymentDetails WHERE [numBizDocsPaymentDetId] IN 
			 (SELECT [numBizDocsPaymentDetId] FROM [OpportunityBizDocsDetails] where numBizDocsId in                        
				(select numOppBizDocsId from OpportunityBizDocs where numOppId=@numOppId)) 
               
		  delete from OpportunityBizDocsDetails where numBizDocsId in                        
		  (select numOppBizDocsId from OpportunityBizDocs where numOppId=@numOppId)  

		DELETE FROM [ShippingReportItems] WHERE [numShippingReportId] IN  (SELECT numShippingReportId from [ShippingReport] WHERE [numOppBizDocId] IN (select numOppBizDocsId from OpportunityBizDocs where numOppId=@numOppId))
		DELETE FROM [ShippingBox] WHERE [numShippingReportId] IN  (SELECT numShippingReportId from [ShippingReport] WHERE [numOppBizDocId] IN (select numOppBizDocsId from OpportunityBizDocs where numOppId=@numOppId))
		DELETE FROM [ShippingReport] WHERE [numOppBizDocId] IN (select numOppBizDocsId from OpportunityBizDocs where numOppId=@numOppId)

		delete from DocumentWorkflow where numDocID in 
		(select numOppBizDocsId from OpportunityBizDocs where numOppId=@numOppId) and cDocType='B'      

		delete from BizDocAction where numBizActionId in(select numBizActionId from BizActionDetails where numOppBizDocsId in
		(select numOppBizDocsId from OpportunityBizDocs where numOppId=@numOppId) and btDocType=1)

		delete from BizActionDetails where numOppBizDocsId in
		(select numOppBizDocsId from OpportunityBizDocs where numOppId=@numOppId) and btDocType=1

		  delete from OpportunityBizDocs where numOppId=@numOppId                        
                                          
		  delete from OpportunityDependency where numOpportunityId=@numOppId                        
                          
	  
  
		  --Credit Balance
		  delete from CreditBalanceHistory where numReturnID in (select numReturnID from [Returns] WHERE [numOppId] = @numOppId)

			IF @tintOppType=1 --Sales
				update DivisionMaster set monSCreditBalance=isnull(monSCreditBalance,0) - isnull(@monCreditAmount,0) where numDivisionID=@numDivisionID and numDomainID=@numDomainID
			else IF @tintOppType=2 --Purchase
				update DivisionMaster set monPCreditBalance=isnull(monPCreditBalance,0) - isnull(@monCreditAmount,0) where numDivisionID=@numDivisionID and numDomainID=@numDomainID
  
			UPDATE
				PO
			SET 
				intCouponCodeUsed = ISNULL(intCouponCodeUsed,0) - (CASE WHEN ISNULL(T1.intUsed,0) > 0 THEN 1 ELSE 0 END)
			FROM
				PromotionOffer PO
			INNER JOIN
				(
					SELECT
						numPromotionID,
						COUNT(*) AS intUsed
					FROM
						OpportunityMaster
					INNER JOIN	
						OpportunityItems
					ON
						OpportunityMaster.numOppId=OpportunityItems.numOppId
					WHERE
						OpportunityMaster.numOppId=@numOppId
					GROUP BY
						numPromotionID
				) AS T1
			ON
				PO.numProId = T1.numPromotionID
			WHERE
				numDomainId=@numDomainId
				AND bitRequireCouponCode = 1
  
		  /*
		  added by chintan: reason: when bizdoc is deleted and journal entries are not deleted in exceptional case, 
		  it throws Foreign key reference error when deleting order.
		  */
		  DELETE FROM dbo.General_Journal_Details WHERE numJournalId IN (SELECT numJournal_Id FROM dbo.General_Journal_Header WHERE numOppId=@numOppId AND numDomainId=@numDomainID)
		  DELETE FROM dbo.General_Journal_Header WHERE numOppId=@numOppId AND numDomainId= @numDomainID
 
		  IF ISNULL(@tintOppStatus,0)=0 OR ISNULL(@isOrderItemsAvailable,0) = 0 OR (SELECT COUNT(*) FROM WareHouseItems_Tracking WHERE numDomainID=@numDomainID AND numReferenceID=@numOppId AND tintRefType IN (3,4) AND vcDescription LIKE '%Deleted%' AND CAST(dtCreatedDate AS DATE) = CAST(GETUTCDATE() AS DATE)) > 0
			UPDATE OpportunityMaster SET tintOppStatus=0,bintModifiedDate=GETUTCDATE(),numModifiedBy=@numUserCntID where numOppId=@numOppId and numDomainID= @numDomainID             
		  ELSE
			RAISERROR ( 'INVENTORY IM-BALANCE', 16, 1 ) ;


		COMMIT TRAN 

		END TRY 
		BEGIN CATCH		
			DECLARE @strMsg VARCHAR(200)
			SET @strMsg = ERROR_MESSAGE()
			IF ( @@TRANCOUNT > 0 ) 
			BEGIN
				RAISERROR ( @strMsg, 16, 1 ) ;
				ROLLBACK TRAN
				RETURN 1
			END
		END CATCH	

		SET XACT_ABORT OFF;
	END 
END
GO