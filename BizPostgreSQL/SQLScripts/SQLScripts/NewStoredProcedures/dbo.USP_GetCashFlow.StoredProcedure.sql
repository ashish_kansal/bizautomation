--exec USP_GetCashFlow @numDomainId=72,@dtFromDate='2009-01-01 00:00:00:000',@dtToDate='2009-10-16 00:00:00:000'
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetCashFlow')
DROP PROCEDURE USP_GetCashFlow
GO
CREATE PROCEDURE USP_GetCashFlow
(@numDomainId as int,
@dtFromDate AS DATETIME,
@dtToDate as DATETIME,
@ClientTimeZoneOffset INT, --Added by Chintan to enable calculation of date according to client machine      
@numAccountClass AS NUMERIC(9)=0                                             
)
AS
BEGIN

CREATE TABLE #CashFlow
(
	Description1 varchar(200),
	Description2 varchar(200),
	Description3 varchar(200),
    vcAccountType varchar(100),
	vcAccountCode varchar(100),
	Amount DECIMAL(20,5));
                           
 

DECLARE @numFinYear INT;
DECLARE @dtFinYearFrom datetime;
DECLARE @CashOpen DECIMAL(20,5);



set @numFinYear= (SELECT numFinYearId FROM FINANCIALYEAR WHERE dtPeriodFrom <= @dtFromDate AND  
dtPeriodTo >=@dtFromDate AND numDomainId= @numDomainId);

set @dtFinYearFrom = (SELECT dtPeriodFrom FROM FINANCIALYEAR WHERE dtPeriodFrom <= @dtFromDate AND  
dtPeriodTo >=@dtFromDate AND numDomainId= @numDomainId);

SET @dtFromDate = DateAdd(minute, @ClientTimeZoneOffset, @dtFromDate);
SET @dtToDate = DateAdd(minute, @ClientTimeZoneOffset, @dtToDate);


INSERT INTO #CashFlow

select  
(CASE SUBSTRING(ATD.vcAccountCode,1,4) 
 WHEN '0102' THEN '1) Cash flows from (used in) operating activities'	
 WHEN '0103' THEN '1) Cash flows from (used in) operating activities'
 WHEN '0104' THEN '1) Cash flows from (used in) operating activities'
 WHEN '0105' THEN '3) Cash flows from (used in) financing activities'
 WHEN '0106' THEN '1) Cash flows from (used in) operating activities'   END) AS Description1,

(CASE SUBSTRING(ATD.vcAccountCode,1,6) 
	WHEN '010101' THEN '1) Cash flows from (used in) operating activities'    
    WHEN '010102' THEN '2) Cash flows from (used in) investing activities' END) AS Description2,

(CASE ATD.vcAccountCode WHEN '0101' THEN '  Cash flows from (used in) financing activities'
	END) AS Description3,


 ATD.vcAccountType,ATD.vcAccountCode,sum(numCreditAmt) +( sum(numDebitAmt)* (-1)) as Amount from General_Journal_Details GJD INNER JOIN 
(SELECT  numJournalID,datEntry_Date FROM VIEW_CASHFLOWJOURNAL  
WHERE numDomainID=@numDomainID AND (numAccountClass=@numAccountClass OR @numAccountClass=0) GROUP BY  numJournalID,datEntry_Date) VCF on 
VCF.numJournalID=GJD.numJournalID INNER JOIN 
Chart_Of_Accounts COA ON 
GJD.numChartAcntID=COA.numAccountId INNER JOIN
AccountTypeDetail ATD ON COA.numAcntTypeID=ATD.numAccountTypeid
AND COA.numDomainID=@numDomainID AND COA.bitActive = 1 AND 
GJD.numDomainID=@numDomainID and 
VCF.datEntry_Date BETWEEN @dtFromDate AND @dtToDate AND 
GJD.numTranSactionID not in (select numTranSactionID from VIEW_CASHFLOWJOURNAL VCF1 WHERE VCF1.numJournalID=VCF.numJournalID)
group by ATD.vcAccountType,ATD.vcAccountCode;

set @CashOpen=0;


SELECT @CashOpen=isnull(monOpening,0) from CHARTACCOUNTOPENING CAO INNER JOIN CHART_OF_ACCOUNTS COA
ON COA.numAccountId=CAO.numAccountId and
numFinYearId=@numFinYear and CAO.numDomainID=@numDomainId and COA.numDomainID=@numDomainId AND 
(COA.vcAccountCode like '01010101%' OR
COA.vcAccountCode like  '01010102%') WHERE COA.bitActive = 1;

SELECT @CashOpen= @CashOpen+ ISNULL(SUM(GJD.numDebitAmt),0) - ISNULL(SUM(GJD.numCreditAmt),0)
FROM
General_Journal_Details GJD INNER JOIN 
General_Journal_Header GJH ON GJD.numJournalId =GJH.numJournal_Id 
INNER JOIN CHART_OF_ACCOUNTS COA ON 
GJD.numChartAcntId=COA.numAccountId AND
GJD.numDomainId=COA.numDomainId AND 
COA.numDomainID=@numDomainId AND 
GJD.numDomainID=@numDomainId  AND
GJH.numDomainID=@numDomainId  AND 
(COA.vcAccountCode like '01010101%' OR
COA.vcAccountCode like  '01010102%') AND
GJH.datEntry_Date BETWEEN @dtFinYearFrom AND @dtFromDate-1 WHERE COA.bitActive = 1 AND (GJH.numAccountClass=@numAccountClass OR @numAccountClass=0);

SELECT isnull(Description1,ISNULL(Description2,Description3)) As Caption,SUM(Amount) AS AmountTotal FROM #CashFlow  GROUP BY isnull(Description1,ISNULL(Description2,Description3));

SELECT isnull(Description1,ISNULL(Description2,Description3)) As Caption,vcAccountType,Amount  FROM #CashFlow 
UNION
SELECT '4) Cash and cash equivalents, beginning of the period','Cash & Bank Account',@CashOpen 
order by isnull(Description1,ISNULL(Description2,Description3))


DROP TABLE #CashFlow;

END