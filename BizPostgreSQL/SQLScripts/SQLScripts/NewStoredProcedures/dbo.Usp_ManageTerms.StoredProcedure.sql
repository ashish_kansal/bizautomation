GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='Usp_ManageTerms')
DROP PROCEDURE Usp_ManageTerms
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE Usp_ManageTerms
(
	@numTermsID					BIGINT,
	@vcTerms					VARCHAR(100),
	@tintApplyTo				TINYINT,
	@numNetDueInDays			INT,
	@numDiscount				NUMERIC(10,2),
	@numDiscountPaidInDays		INT,
	@numListItemID				NUMERIC(18,0),
	@numDomainID				NUMERIC(18,0),
	@bitActive					BIT
)
AS 
BEGIN
	IF EXISTS(SELECT * FROM dbo.BillingTerms WHERE vcTerms = @vcTerms AND [numTermsID] <> @numTermsID AND numDomainID = @numDomainID)
	BEGIN
	    RAISERROR ('ERROR: Term details already exists. Please provide another Term.', -- Message text.
				   16, -- Severity.
				   1 -- State.
				   );
        RETURN
	END	
	
	IF EXISTS(SELECT [numTermsID] FROM [dbo].[BillingTerms] WHERE [numTermsID] = @numTermsID)
		BEGIN
			UPDATE dbo.BillingTerms SET vcTerms = @vcTerms, 
										tintApplyTo = @tintApplyTo,
										numNetDueInDays = @numNetDueInDays,
										numDiscount = @numDiscount,
										numDiscountPaidInDays = @numDiscountPaidInDays,
										numListItemID = @numListItemID,
										bitActive = @bitActive
			WHERE numTermsID = @numTermsID
			AND numDomainID = @numDomainID										
		END
		
	ELSE
		BEGIN
			INSERT INTO dbo.BillingTerms (
				vcTerms,
				tintApplyTo,
				numNetDueInDays,
				numDiscount,
				numDiscountPaidInDays,
				numListItemID,
				numDomainID,
				bitActive
			) 
			VALUES ( 
				@vcTerms ,
				@tintApplyTo ,
				@numNetDueInDays,
				@numDiscount,
				@numDiscountPaidInDays,
				@numListItemID,
				@numDomainID,
				@bitActive )			
		END	

END