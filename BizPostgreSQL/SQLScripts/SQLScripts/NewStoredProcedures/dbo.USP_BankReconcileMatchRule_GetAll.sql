SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_BankReconcileMatchRule_GetAll')
DROP PROCEDURE dbo.USP_BankReconcileMatchRule_GetAll
GO
CREATE PROCEDURE [dbo].[USP_BankReconcileMatchRule_GetAll]
(
	@numDomainID NUMERIC(18,0)
)
AS 
BEGIN
	SELECT
		ID
		,vcName
		,(CASE WHEN bitMatchAllConditions = 1 THEN 'YES' ELSE 'NO' END) AS vcMatchAll
		,STUFF((SELECT ',' + vcAccountName FROM Chart_Of_Accounts WHERE numDomainId=@numDomainID AND numAccountId IN (SELECT Id FROM dbo.SplitIDs(BRMR.vcBankAccounts,',')) FOR XML PATH(''), TYPE).value('.', 'NVARCHAR(MAX)'), 1, 1, '') as vcBankAccounts
		,STUFF((SELECT '<br/>' + CONCAT('When <b>',(CASE tintColumn WHEN 1 THEN 'Payee ' WHEN 2 THEN 'Description ' WHEN 3 THEN 'Reference ' END),'</b>',(CASE tintConditionOperator WHEN 1 THEN 'contains ' WHEN 2 THEN 'equals ' WHEN 3 THEN 'starts with ' WHEN 4 THEN 'ends with ' END),'<b>',vcTextToMatch,'</b> than select organization <b>',(SELECT vcCompanyName FROM DivisionMaster INNER JOIN CompanyInfo ON DivisionMaster.numCompanyID=CompanyInfo.numCompanyId WHERE DivisionMaster.numDomainID=@numDomainID AND DivisionMaster.numDivisionID=BRMRC.numDivisionID),'</b>') FROM BankReconcileMatchRuleCondition BRMRC WHERE numRuleID=BRMR.ID FOR XML PATH(''), TYPE).value('.', 'NVARCHAR(MAX)'), 1, 5, '') AS vcConditions
		,tintOrder
	FROM
		BankReconcileMatchRule BRMR
	WHERE
		numDomainID = @numDomainID
	ORDER BY
		tintOrder
END