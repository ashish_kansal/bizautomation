/****** Object:  StoredProcedure [dbo].[USP_CheckMasterlistItem]    Script Date: 02/20/2013 15:10:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created By Joseph 
-- for Validating API Setting (Checks List Detail Mapping are exists or removed)
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_CheckMasterlistItem')
DROP PROCEDURE USP_CheckMasterlistItem
GO
CREATE PROCEDURE [dbo].[USP_CheckMasterlistItem]          
@numDomainID as numeric(9)=0,
@ListID as numeric(9)=0,        
@ListItemID as numeric(9)=0    
as          
 
SELECT COUNT(*) as IsExists FROM ListDetails LD WHERE  LD.numListItemId = @ListItemID AND LD.numListId = @ListID  AND (LD.numDomainId = @numDomainID Or LD.ConstFlag = 1)

GO