GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_CommissionDatePayPeriod_Get')
DROP PROCEDURE USP_CommissionDatePayPeriod_Get
GO
Create PROCEDURE [dbo].[USP_CommissionDatePayPeriod_Get]
	-- Add the parameters for the stored procedure here
	 @numDomainID AS NUMERIC(18,0)
AS
BEGIN
-- SET NOCOUNT ON added to prevent extra result sets from
-- interfering with SELECT statements.
SET NOCOUNT ON;

SELECT TOP 1 dtFromDate,dtToDate FROM PayrollHeader WHERE numDomainID=@numDomainID order by numPayrollHeaderID desc

END
GO