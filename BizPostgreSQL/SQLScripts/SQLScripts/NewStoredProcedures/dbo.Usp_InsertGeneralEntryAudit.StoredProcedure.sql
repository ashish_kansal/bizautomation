GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='Usp_InsertGeneralEntryAudit')
DROP PROCEDURE Usp_InsertGeneralEntryAudit
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- Usp_InsertGeneralEntryAudit 
CREATE PROCEDURE Usp_InsertGeneralEntryAudit
(
	@numDomainID NUMERIC(18,0),
    @numJournalID NUMERIC(18,0), 
    @numTransactionID NUMERIC(18,0),
    @dtCreatedDate DATETIME,
    @varDescription VARCHAR(1000)
)
AS 
BEGIN

INSERT INTO [dbo].[GenealEntryAudit]
           ([numDomainID]
           ,[numJournalID]
           ,[numTransactionID]
           ,[dtCreatedDate]
           ,[varDescription])
     VALUES
           (@numDomainID,
           @numJournalID, 
           @numTransactionID,
           @dtCreatedDate,
           @varDescription)
END

GO

