
GO
/****** Object:  StoredProcedure [dbo].[Usp_UpdateReturns]    Script Date: 01/22/2009 01:46:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='Usp_UpdateReturns')
DROP PROCEDURE Usp_UpdateReturns
GO
CREATE PROCEDURE  [dbo].[Usp_UpdateReturns]
(
@numReturnID NUMERIC(9), 
--@numQtyReturned NUMERIC(9),
@numReturnStatus NUMERIC(9),
@numModifiedBy NUMERIC(9),
@vcReferencePO varchar(50)=''
)
AS 
BEGIN
	UPDATE 
		[Returns] 
   SET 
      --[numQtyReturned] = @numQtyReturned,
      [numReturnStatus] = @numReturnStatus,
      [numModifiedBy] = @numModifiedBy,
	  [dtModifiedDate] = GETDATE(),
	  vcReferencePO =@vcReferencePO
	WHERE 
		numreturnid = @numReturnID
 
	
END