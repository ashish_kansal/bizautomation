/****** Object:  StoredProcedure [dbo].[USP_GetMergeFieldsByModuleID]    Script Date: 07/26/2008 16:17:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
--exec [USP_GetMergeFieldsByModuleID] 2,0,0
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetMergeFieldsByModuleID')
DROP PROCEDURE USP_GetMergeFieldsByModuleID
GO
CREATE PROCEDURE [dbo].[USP_GetMergeFieldsByModuleID]
@numModuleID as numeric(18,0),
@tintMode TINYINT=0,
@tintModuleType TINYINT=0
as  
IF @tintMode =0 
BEGIN
	SELECT  EMF.intEmailMergeFields,
		EMF.vcMergeField,
		EMF.vcMergeFieldValue,
		EMM.vcModuleName
FROM    EmailMergeFields EMF
        INNER JOIN dbo.EmailMergeModule EMM ON EMF.numModuleID = EMM.numModuleID
WHERE   EMF.numModuleID = @numModuleID AND EMM.tintModuleType=@tintModuleType AND EMM.tintModuleType = EMF.tintModuleType--- ISNULL(EMF.tintModuleType,0)
ORDER BY vcMergeField asc
	
END
ELSE IF @tintMode =1 
BEGIN
	SELECT numModuleID,vcModuleName FROM dbo.EmailMergeModule WHERE tintModuleType=@tintModuleType 
END
GO


