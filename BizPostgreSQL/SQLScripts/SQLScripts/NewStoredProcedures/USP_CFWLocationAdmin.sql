SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_CFWLocationAdmin')
DROP PROCEDURE dbo.USP_CFWLocationAdmin
GO


CREATE PROCEDURE [dbo].[USP_CFWLocationAdmin]  
  @numDomainId NUMERIC(18,0)
AS  
 
 
IF (SELECT ISNULL(bitEDI,0) FROM Domain WHERE numDomainId = @numDomainId) = 0

	SELECT Loc_id,Loc_name 
	FROM CFW_Loc_Master
	WHERE Loc_id <> 19 AND Loc_id <> 20 AND Loc_id <> 21 AND Loc_id NOT IN(12
,13
,14
,15
,16
,17
,18
,19
,20
,21)

ELSE

	SELECT Loc_id,Loc_name 
	FROM CFW_Loc_Master
	WHERE Loc_id <> 21 AND Loc_id NOT IN(12
,13
,14
,15
,16
,17
,18
,19
,20
,21)


GO


