/****** Object:  StoredProcedure [dbo].[USP_CaseDetailsdtlPL]    Script Date: 07/26/2008 16:15:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--exec USP_CaseDetails @numCaseId = 34                  
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_CaseDueListForWeek')
DROP PROCEDURE USP_CaseDueListForWeek
GO
CREATE PROCEDURE [dbo].[USP_CaseDueListForWeek]                                                
@numDomainID bigint ,      
@ClientTimeZoneOffset as int   ,                   
@numContactId as numeric(9)=null 
AS
BEGIN
	SELECT 
		vcCaseNumber,
		textDesc,
		numCaseId,
		convert(varchar(20),dateadd(minute,-@ClientTimeZoneOffset,intTargetResolveDate),101) AS TargetResolveDate
	FROM 
		Cases
	WHERE
		numDomainID=@numDomainID AND (numRecOwner=@numContactId OR numAssignedTo=@numContactId) AND (CAST(intTargetResolveDate AS DATE) BETWEEN DATEADD(DAY, -2, GETDATE()) AND DATEADD(DAY, 5, GETDATE()))
	ORDER BY intTargetResolveDate 

END