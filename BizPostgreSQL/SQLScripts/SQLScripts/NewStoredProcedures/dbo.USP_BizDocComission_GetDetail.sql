GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_BizDocComission_GetDetail')
DROP PROCEDURE USP_BizDocComission_GetDetail
GO
CREATE PROCEDURE [dbo].[USP_BizDocComission_GetDetail]
(
	@numDomainId NUMERIC(18,0)
	,@numUserCntID NUMERIC(18,0)
	,@numDivisionID NUMERIC(18,0)
	,@numComPayPeriodID NUMERIC(18,0)
	,@ClientTimeZoneOffset INT
	,@tintMode TINYINT
	,@numOppID NUMERIC(18,0)
	,@numOppBizDocID NUMERIC(18,0)
)
AS
BEGIN
	IF @tintMode = 1
	BEGIN
		SELECT
			OpportunityMaster.numOppId
			,OpportunityMaster.vcPOppName AS Name
			,SUM(ISNULL(monOrderSubTotal,0)) AS monSOSubTotal
			,CAST(SUM(ISNULL(OBDI.monTotAmount,OpportunityItems.monTotAmount) - (ISNULL(OBDI.numUnitHour,OpportunityItems.numUnitHour) * ISNULL(OpportunityItems.monVendorCost,0) * dbo.fn_UOMConversion(Item.numBaseUnit,Item.numItemCode,@numDomainID,Item.numPurchaseUnit))) AS DECIMAL(20,5)) monGPVendorCost
			,CAST(SUM(ISNULL(OBDI.monTotAmount,OpportunityItems.monTotAmount) - (ISNULL(OBDI.numUnitHour,OpportunityItems.numUnitHour) * ISNULL(OpportunityItems.monAvgCost,0))) AS DECIMAL(20,5)) monGPAverageCost
			,ISNULL(OpportunityBizDocs.numOppBizDocsId,0) numOppBizDocsId
			,ISNULL(OpportunityBizDocs.vcBizDocID,'') vcBizDocID
			,SUM(ISNULL(monInvoiceSubTotal,0)) AS monInvoiceSubTotal
			,SUM(ISNULL(numComissionAmount,0)) AS monTotalCommission
			,SUM(ISNULL(monCommissionPaid,0)) AS monCommissionPaid
			,SUM(ISNULL(numComissionAmount,0) - ISNULL(monCommissionPaid,0)) AS monCommissionToPay
		FROM
			BizDocComission
		INNER JOIN
			OpportunityMaster
		ON
			BizDocComission.numOppID = OpportunityMaster.numOppId
		INNER JOIN
			OpportunityItems
		ON
			BizDocComission.numOppItemID=OpportunityItems.numOppitemtcode
		INNER JOIN
			Item
		ON
			OpportunityItems.numItemCode = Item.numItemCode
		LEFT JOIN
			Vendor
		ON
			Item.numVendorID = Vendor.numVendorID
			AND Item.numItemCode = Vendor.numItemCode
		LEFT JOIN
			OpportunityBizDocs
		ON
			BizDocComission.numOppBizDocId = OpportunityBizDocs.numOppBizDocsId
		LEFT JOIN
			OpportunityBizDocItems OBDI
		ON
			BizDocComission.numOppBizDocItemID = OBDI.numOppBizDocItemID
		WHERE
			numComPayPeriodID=@numComPayPeriodID
			AND BizDocComission.numUserCntID=@numUserCntID
		GROUP BY
			OpportunityMaster.numOppId
			,OpportunityMaster.vcPOppName
			,OpportunityMaster.bintCreatedDate
			,OpportunityBizDocs.numOppBizDocsId
			,OpportunityBizDocs.vcBizDocID
		ORDER BY
			OpportunityMaster.bintCreatedDate
	END
	ELSE IF @tintMode = 2
	BEGIN
		SELECT
			I.numItemCode
			,I.vcItemName
			,ISNULL(OBDI.numUnitHour,OI.numUnitHour) numUnitHour
			,ISNULL(OI.monVendorCost,0) * dbo.fn_UOMConversion(I.numPurchaseUnit,I.numItemCode,@numDomainId,I.numBaseUnit) * ISNULL(OBDI.numUnitHour,OI.numUnitHour) AS VendorCost
			,ISNULL(OI.monAvgCost,0) * ISNULL(OBDI.numUnitHour,OI.numUnitHour) AS monAvgCost
			,ISNULL(OBDI.monPrice,OI.monPrice) monPrice
			,ISNULL(OBDI.monTotAmount,OI.monTotAmount) monTotAmount
			,ISNULL(BDC.numComissionAmount,0) monCommissionEarned
			,ISNULL(BDC.monCommissionPaid,0) monCommissionPaid
			,BDC.decCommission
			,Case when BDC.tintComBasedOn = 1 then 'Amount' when BDC.tintComBasedOn = 2 then 'Units' end AS BasedOn
			,Case when BDC.tintComType = 1 then 'Percentage' when BDC.tintComType = 2 then 'Flat' end as CommissionType
		FROM
			BizDocComission BDC
		INNER JOIN
			OpportunityItems OI
		ON
			BDC.numOppItemID = OI.numoppitemtCode
		INNER JOIN 
			Item I 
		ON 
			OI.numItemCode=I.numItemCode
		LEFT JOIN
			OpportunityBizDocItems OBDI
		ON
			BDC.numOppBizDocItemID = OBDI.numOppBizDocItemID
		
		WHERE
			BDC.numComPayPeriodID=@numComPayPeriodID
			AND BDC.numUserCntID=@numUserCntID
			AND BDC.numOppID=@numOppID
			AND ISNULL(BDC.numOppBizDocId,0) = ISNULL(@numOppBizDocID,0) 
	END
END
GO
