GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_MANAGE_IMPORT_FILE_DATA')
DROP PROCEDURE USP_MANAGE_IMPORT_FILE_DATA
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[USP_MANAGE_IMPORT_FILE_DATA]
(
   @intImportFileID			BIGINT	OUTPUT,	
   @vcImportFileName		VARCHAR(1000),
   @numMasterID				NUMERIC,
   @numRecordAdded			NUMERIC,
   @numRecordUpdated		NUMERIC,
   @numErrors				NUMERIC,
   @numDuplicates			NUMERIC,
   @dtCreateDate			DATETIME,
   @dtImportDate			DATETIME,
   @numDomainID				NUMERIC,
   @numUserContactID		NUMERIC,
   @tintStatus				TINYINT,
   @btHasSendEmail			BIT,
   @vcHistory_Added_Value	VARCHAR(MAX),
   @dtHistoryDateTime		DATETIME,
   @intMode					INT,
   @tintItemLinkingID INT,
   @tintImportType TINYINT,
   @bitSingleOrder BIT,
   @bitExistingOrganization BIT,
   @numDivisionID NUMERIC(18,0),
   @numContactID NUMERIC(18,0),
   @vcCompanyName VARCHAR(300),
   @vcContactFirstName VARCHAR(100),
   @vcContactLastName VARCHAR(100),
   @vcContactEmail VARCHAR(100),
   @bitMatchOrganizationID BIT,
   @numMatchFieldID NUMERIC(18,0)
)
		--> 0 - FOR INSERT
		--> 1 - FOR UPDATE
		--> 2 - FOR UPDATE ROLLBACK STATUS
AS 
BEGIN
	IF @intMode = 0 -- FOR INSERT
		BEGIN
			INSERT INTO Import_File_Master
			(
				vcImportFileName
				,numMasterID
				,numRecordAdded
				,numRecordUpdated
				,numErrors
				,numDuplicates				   
				,dtCreateDate
				,dtImportDate
				,numDomainID
				,numUserContactID
				,tintStatus
				,btHasSendEmail
				,tintItemLinkingID
				,tintImportType
				,bitSingleOrder
				,bitExistingOrganization
				,numDivisionID
				,numContactID
				,vcCompanyName
				,vcContactFirstName
				,vcContactLastName
				,vcContactEmail
				,bitMatchOrganizationID
				,numMatchFieldID
			)
			 VALUES
			(	
				@vcImportFileName	
				,@numMasterID			
				,@numRecordAdded		
				,@numRecordUpdated	
				,@numErrors			
				,@numDuplicates		
				,GETUTCDATE()		
				,GETUTCDATE()		
				,@numDomainID			
				,@numUserContactID	
				,@tintStatus			
				,@btHasSendEmail
				,@tintItemLinkingID
				,@tintImportType
				,@bitSingleOrder
				,@bitExistingOrganization
				,@numDivisionID
				,@numContactID
				,@vcCompanyName
				,@vcContactFirstName
				,@vcContactLastName
				,@vcContactEmail
				,@bitMatchOrganizationID
				,@numMatchFieldID
			)

			SET @intImportFileID = SCOPE_IDENTITY()	   
			
			INSERT INTO dbo.Import_History
			        (intImportFileID,vcHistory_Added_Value ,dtHistoryDateTime )
			VALUES  (@intImportFileID,@vcHistory_Added_Value,@dtHistoryDateTime )
			
		END  
		
	IF @intMode = 1 -- FOR UPDATE	         
		BEGIN
			
			UPDATE dbo.Import_History SET vcHistory_Added_Value = @vcHistory_Added_Value
									WHERE intImportFileID = @intImportFileID  
									  					
			DECLARE @numHistoryID AS NUMERIC
			SELECT @numHistoryID = numHistoryID FROM dbo.Import_History WHERE intImportFileID = @intImportFileID
			
			UPDATE Import_File_Master SET [vcImportFileName] = @vcImportFileName
										  ,[numMasterID] = @numMasterID
										  ,[numRecordAdded] = @numRecordAdded
										  ,[numRecordUpdated] = @numRecordUpdated
										  ,[numErrors] = @numErrors
										  ,[numDuplicates] = @numDuplicates
										  ,[numHistoryID] = @numHistoryID
										  ,[dtImportDate] = GETUTCDATE()
										  ,[tintStatus] = @tintStatus
										  ,[btHasSendEmail] = @btHasSendEmail
			WHERE intImportFileID = @intImportFileID
			  AND numDomainID = @numDomainID
			  AND numUserContactID =  @numUserContactID 
		END	
		
	IF @intMode = 2 -- UPDATE STATUS FOR ROLLBACK
		BEGIN
			UPDATE Import_File_Master SET tintStatus = @tintStatus 
			WHERE intImportFileID = @intImportFileID
			  AND numDomainID = @numDomainID
			  AND numUserContactID = @numUserContactID 
			  AND ISNULL(numRecordAdded,0) > 0
		END	         		
END
