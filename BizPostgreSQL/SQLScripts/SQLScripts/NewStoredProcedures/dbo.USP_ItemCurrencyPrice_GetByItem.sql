GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ItemCurrencyPrice_GetByItem')
DROP PROCEDURE USP_ItemCurrencyPrice_GetByItem
GO
CREATE PROCEDURE [dbo].[USP_ItemCurrencyPrice_GetByItem]
(
	@numDomainID NUMERIC(18,0)
	,@numItemCode NUMERIC(18,0)
)
AS
BEGIN
	SELECT
		Currency.numCurrencyID
		,Currency.vcCurrencyDesc
		,Currency.chrCurrency
		,ISNULL(ItemCurrencyPrice.monListPrice,0) monListPrice
	FROM
		Currency
	INNER JOIN
		Domain
	ON
		Currency.numDomainID = Domain.numDomainId
	LEFT JOIN
		ItemCurrencyPrice
	ON
		ItemCurrencyPrice.numDomainID = @numDomainID
		AND ItemCurrencyPrice.numItemCode = @numItemCode
		AND Currency.numCurrencyID = ItemCurrencyPrice.numCurrencyID
	WHERE
		Currency.numDomainID=@numDomainID
		AND ISNULL(bitEnabled,0) = 1
		AND ISNULL(Currency.numCurrencyID,0) <> ISNULL(Domain.numCurrencyID,0)
	ORDER BY
		vcCurrencyDesc
END
GO