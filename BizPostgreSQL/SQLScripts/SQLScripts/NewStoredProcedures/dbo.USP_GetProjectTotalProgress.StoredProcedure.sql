
GO
/****** Object:  StoredProcedure [dbo].[USP_GetProjectTotalProgress]    Script Date: 09/29/2010 16:00:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Now this procedure is being used to update total progress       
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetProjectTotalProgress')
DROP PROCEDURE USP_GetProjectTotalProgress
GO
CREATE PROCEDURE [dbo].[USP_GetProjectTotalProgress]
@numDomainID as numeric(9)=0,
@numProId as numeric(9)=0,
@tintMode TINYINT=-1
as       
 
DECLARE @TotalProgress AS INT 

IF Not Exists (select * from StagePercentageDetails where @numProId=(case @tintMode when 0 then numOppID 
				when 1 then numProjectID end) and tinProgressPercentage<100)
BEGIN
SET @TotalProgress=100
END
ELSE
BEGIN
;with SW(numStagePercentageId,numStagePercentage,StageProgress) as(
select SPD.numStagePercentageId,SPM.numStagePercentage,cast (cast(SPD.tintPercentage as int) * cast(isnull(SPD.tinProgressPercentage,1)  as int) as int)/100  as StageProgress
  FROM StagePercentageDetails SPD 
join StagePercentageMaster SPM on SPD.numStagePercentageId=SPM.numStagePercentageId 
where @numProId=(case @tintMode when 0 then SPD.numOppID 
				when 1 then SPD.numProjectID end) 
),
MW as (select numStagePercentageId,numStagePercentage, SUM(StageProgress) 
as MileStoneProgress from SW  group by numStagePercentageId,numStagePercentage 
)
,tRowNumber as (SELECT *,ROW_NUMBER() OVER (ORDER BY numStagePercentage) ROWNUMBER FROM MW)

,tFinal as (select top 1 *,cast(numStagePercentage as int) as diff,
(cast(numStagePercentage as int) * MileStoneProgress )/100 as total from tRowNumber

union all select f.*,cast((f.numStagePercentage - p.numStagePercentage) as int) as diff,
(cast((f.numStagePercentage - p.numStagePercentage) as int) * f.MileStoneProgress )/100 as total
		from tRowNumber f INNER JOIN tFinal p ON p.ROWNUMBER = (f.ROWNUMBER-1))

--select * from tFinal

select @TotalProgress = ISNULL(sum(total),0) from tFinal
END

IF EXISTS(SELECT * FROM dbo.ProjectProgress WHERE  @numProId=(case @tintMode when 0 then numOppID when 1 then numProID end)  AND numDomainId=@numDomainID)
	BEGIN
		UPDATE dbo.ProjectProgress SET intTotalProgress=@TotalProgress WHERE  @numProId=(case @tintMode when 0 then numOppID 
			when 1 then numProID end)  AND numDomainId=@numDomainID					
	END
	ELSE
	BEGIN
		 DECLARE @numOppID AS NUMERIC
		 IF @tintMode = 0 
		 BEGIN
		 	SET @numOppID = @numProId
			SET @numProId = null
		 END
		 ELSE IF @tintMode = 1 
			SET @numOppID = NULL
			
		 INSERT INTO dbo.ProjectProgress ( numProId,
										   numOppId,
										   numDomainId,
										   intTotalProgress )
		 VALUES ( /* numProId - numeric(18, 0) */ @numProId,
							/* numOppId - numeric(18, 0) */ @numOppID,
							/* numDomainId - numeric(18, 0) */ @numDomainID,
							/* intTotalProgress - int */ @TotalProgress ) 
	END




