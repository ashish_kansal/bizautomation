SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ActivityFormConfiguration_Save')
DROP PROCEDURE USP_ActivityFormConfiguration_Save
GO
CREATE PROCEDURE [dbo].[USP_ActivityFormConfiguration_Save]  
	@numDomainID AS NUMERIC(18,0),
	@numGroupId AS NUMERIC(18,0),
	@bitFollowupStatus AS BIT=0,
	@bitPriority AS BIT=0,
	@bitActivity AS BIT=0,
	@bitCustomField AS BIT=0,
	@bitFollowupAnytime AS BIT=0,
	@bitAttendee AS BIT=0,
	@bitTitle AS BIT=0,
	@bitDescription AS BIT=0,
	@bitLocation AS BIT=0,
		@numUserCntId AS NUMERIC(18,0)
AS
BEGIN
BEGIN TRY
BEGIN TRANSACTION
	IF EXISTS(SELECT numActivityAuthenticationId FROM ActivityFormAuthentication WHERE numDomainID=@numDomainID AND numGroupId=@numGroupId)
		BEGIN
			UPDATE 
				ActivityFormAuthentication
			SET 
				bitFollowupStatus=@bitFollowupStatus, 
				bitPriority=@bitPriority, 
				bitActivity=@bitActivity, 
				bitCustomField=@bitCustomField, 
				bitFollowupAnytime=@bitFollowupAnytime,
				bitAttendee=@bitAttendee
			WHERE
				numDomainId=@numDomainId AND
				numGroupId=@numGroupId
		END
	ELSE
		BEGIN
			INSERT INTO ActivityFormAuthentication
				(
					numDomainId, 
					numGroupId, 
					bitFollowupStatus, 
					bitPriority, 
					bitActivity, 
					bitCustomField, 
					bitFollowupAnytime,
					bitAttendee
				)
			VALUES
				(
					@numDomainId, 
					@numGroupId, 
					@bitFollowupStatus, 
					@bitPriority, 
					@bitActivity, 
					@bitCustomField, 
					@bitFollowupAnytime,
					@bitAttendee
				)
			
		END

		IF EXISTS(SELECT numDisplayId FROM ActivityDisplayConfiguration WHERE numDomainID=@numDomainID AND numUserCntId=@numUserCntId)
		BEGIN
			UPDATE 
				ActivityDisplayConfiguration
			SET 
				bitTitle=@bitTitle, 
				bitLocation=@bitLocation, 
				bitDescription=@bitDescription
			WHERE
				numDomainId=@numDomainId AND
				numUserCntId=@numUserCntId
		END
	ELSE
		BEGIN
			INSERT INTO ActivityDisplayConfiguration
				(
					numDomainId, 
					numUserCntId, 
					bitTitle,
					bitLocation,
					bitDescription
				)
			VALUES
				(
					@numDomainId, 
					@numUserCntId, 
					@bitTitle, 
					@bitLocation, 
					@bitDescription
				)
			
		END

COMMIT
END TRY
BEGIN CATCH
	DECLARE @ErrorMessage NVARCHAR(4000)
	DECLARE @ErrorNumber INT
	DECLARE @ErrorSeverity INT
	DECLARE @ErrorState INT
	DECLARE @ErrorLine INT
	DECLARE @ErrorProcedure NVARCHAR(200)

	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;

	SELECT 
		@ErrorMessage = ERROR_MESSAGE(),
		@ErrorNumber = ERROR_NUMBER(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE(),
		@ErrorLine = ERROR_LINE(),
		@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

	RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);
END CATCH
END