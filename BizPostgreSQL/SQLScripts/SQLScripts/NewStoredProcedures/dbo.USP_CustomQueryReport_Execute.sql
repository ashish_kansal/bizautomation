SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_CustomQueryReport_Execute')
DROP PROCEDURE dbo.USP_CustomQueryReport_Execute
GO
CREATE PROCEDURE [dbo].[USP_CustomQueryReport_Execute]
	@numReportID NUMERIC(18,0)
AS 
BEGIN
	DECLARE @vcQuery AS VARCHAR(MAX)

	SELECT @vcQuery = CAST(vcQuery AS VARCHAR(MAX)) FROM CustomQueryReport WHERE numReportID = @numReportID

	IF LEN(@vcQuery) > 0
	BEGIN
		EXEC (@vcQuery)
	END
END