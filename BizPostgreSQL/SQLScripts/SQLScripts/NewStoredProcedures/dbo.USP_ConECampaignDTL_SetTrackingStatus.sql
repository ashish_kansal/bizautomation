GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ConECampaignDTL_SetTrackingStatus')
DROP PROCEDURE USP_ConECampaignDTL_SetTrackingStatus
GO
CREATE PROCEDURE [dbo].[USP_ConECampaignDTL_SetTrackingStatus]                            
 @numConECampDTLID NUMERIC(9 )= 0
AS                            
BEGIN                            
	UPDATE ConECampaignDTL SET bitEmailRead = 1 WHERE numConECampDTLID = @numConECampDTLID
END
GO



