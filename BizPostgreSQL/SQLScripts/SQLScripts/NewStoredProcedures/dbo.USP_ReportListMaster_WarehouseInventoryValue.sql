GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

GO
IF EXISTS (SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ReportListMaster_WarehouseInventoryValue')
DROP PROCEDURE USP_ReportListMaster_WarehouseInventoryValue
GO


CREATE procedure [dbo].[USP_ReportListMaster_WarehouseInventoryValue] 
@numDomainID numeric
As
Begin
With 
Cte
as
(
Select --top 15 15 AS TotalParentRecords, 
--isnull(WareHouseItems.numOnHand,'0') as OnHand,
isnull(Item.monAverageCost,'0') as AverageCost,
isnull(Warehouses.vcWareHouse,'') as WareHouse,
--CONCAT('~/Items/frmItemList.aspx?Page=InventoryItems&ItemGroup=0&WID=',Warehouses.numWareHouseID) AS URL,
ISNULL(Warehouses.numWareHouseID,'') as WareHouseID,

--isnull((WareHouseItems.numOnHand + WarehouseItems.numAllocation)*(Item.monAverageCost),0)	as InventoryValue	 
(isnull(WareHouseItems.numOnHand,0) + isnull(WarehouseItems.numAllocation,0))*(isnull(Item.monAverageCost,0))as InventoryValue
FROM Item   
Left Join Vendor on Vendor.numDomainId = @numDomainID and Vendor.numVendorID = Item.numVendorID and  Item.numItemCode= Vendor.numItemCode   
join WareHouseItems  on WareHouseItems.numDomainId = @numDomainID and Item.numItemCode=WareHouseItems.numItemID 
LEFT JOIN WarehouseLocation ON WarehouseLocation.numWLocationID = WareHouseItems.numWLocationID   
join Warehouses Warehouses on Warehouses.numDomainId = @numDomainID and Warehouses.numWareHouseID=WareHouseItems.numWareHouseID 
WHERE 1=1 AND Item.numDomainId = @numDomainID AND (ISNULL(WareHouseItems.numOnHand,0) + ISNULL(WareHouseItems.numAllocation,0) >0)  and isnull(Item.monAverageCost,0) >0 
and  isnull(Item.IsArchieve,0) = 0 

)
select cte.WareHouse,
cte.WareHouseID,
--Cte.URL,
cast(SUM(cte.InventoryValue) as DECIMAL(18,0)) as InventoryValue
from  cte group by Cte.WareHouse,Cte.WareHouseID
--Cte.URL 
order by InventoryValue desc

END
GO