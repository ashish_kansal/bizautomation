SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetReportListMaster')
DROP PROCEDURE USP_GetReportListMaster
GO
CREATE PROCEDURE [dbo].[USP_GetReportListMaster]      
@numDomainId as numeric(18,0),  
@numUserCntID NUMERIC(18,0),
@numReportModuleID as numeric(18,0),   
@CurrentPage int,                                                        
@PageSize int,                                                        
@TotRecs int output,     
@SortChar char(1)='0' ,                                                       
@columnName as Varchar(50),                                                        
@columnSortOrder as Varchar(50)  ,
@SearchStr AS VARCHAR(50),
@tintReportType INT,
@tintPerspective INT,
@vcTimeline VARCHAR(100)
AS
BEGIN       
    SET NOCOUNT ON

	DECLARE @numGroupID NUMERIC(18,0)
	SELECT @numGroupID=ISNULL(numGroupID,0) FROM UserMaster WHERE numUserDetailId=@numUserCntID
     
	Create table #tempTable
	(
		ID INT IDENTITY PRIMARY KEY,                                                         
		numReportID NUMERIC(18,0)                                                       
	)     
	DECLARE @strSql AS VARCHAR(8000)                                                  
    
	SET  @strSql= CONCAT('Select ReportListMaster.numReportID from ReportListMaster INNER JOIN ReportDashboardAllowedReports ON ReportListMaster.numReportID = ReportDashboardAllowedReports.numReportID  WHERE ReportListMaster.numdomainid=',@numDomainID,' AND ReportDashboardAllowedReports.numDomainID=',@numDomainId,' AND ReportDashboardAllowedReports.numGrpID=',@numGroupID)
	IF @numReportModuleID <> 0 
	BEGIN
		SET @strSql=@strSql + ' And numReportModuleID = '+ convert(varchar(15),@numReportModuleID) +''   
	END

	IF @SearchStr <> '' 
	BEGIN
		SET @strSql=@strSql + ' AND LOWER(vcReportName) like ''%'+ LOWER(@SearchStr) +'%'''
	END

	IF @tintReportType <> -1
	BEGIN
		SET @strSql=@strSql + CONCAT(' AND tintReportType = ',@tintReportType)
	END

	IF @tintPerspective <> -1
	BEGIN
		SET @strSql=@strSql + CONCAT(' AND tintRecordFilter = ',@tintPerspective)
	END

	IF LEN(ISNULL(@vcTimeline,'')) > 0 AND ISNULL(@vcTimeline,'') <> '-1'
	BEGIN
		SET @strSql=@strSql + CONCAT(' AND vcDateFieldValue = ''',@vcTimeline,'''')
	END
    
	SET  @strSql=@strSql +' ORDER BY ReportListMaster.' + @columnName +' '+ @columnSortOrder    
    
    PRINT @strSql
	INSERT INTO #tempTable(numReportID) EXEC(@strSql)    
    
	DECLARE @firstRec as integer                                                        
	DECLARE @lastRec as integer                                                        
	SET @firstRec= (@CurrentPage-1) * @PageSize                                                        
	SET @lastRec= (@CurrentPage*@PageSize+1)                                                         

	SELECT  @TotRecs = COUNT(*)  FROM #tempTable	
	
	SELECT 
		RLM.numReportID
		,RLM.vcReportName
		,RLM.vcReportDescription
		,RMM.vcModuleName
		,RMGM.vcGroupName
		,(CASE RLM.tintReportType WHEN 0 THEN 'Tabular' WHEN 1 THEN 'Summary' WHEN 2 THEN 'Matrix' WHEN 3 THEN 'KPI' WHEN 4 THEN 'ScoreCard' END) AS vcReportType
		,(CASE RLM.tintRecordFilter WHEN 5 THEN 'User logged in (Record Owner)' WHEN 4 THEN 'User logged in (Assigned-To)' WHEN 3 THEN 'User logged in (Territory Scope)' WHEN 2 THEN 'User logged in (Team Scope)' WHEN 1 THEN 'User logged in (Assigned-To/Record Owner)' ELSE 'All Records' END) AS vcPerspective
		,(CASE 
			WHEN ISNULL(RLM.numDateFieldID,0) > 0 AND LEN(ISNULL(RLM.vcDateFieldValue,'')) > 0
			THEN 
				CONCAT((CASE WHEN bitDateFieldColumnCustom = 1 THEN (SELECT Fld_label FROM CFW_Fld_Master WHERE Fld_Id = numDateFieldID) ELSE (SELECT vcFieldName FROM DycFieldMaster WHERE numFieldId=numDateFieldID) END),' (',
				(CASE RLM.vcDateFieldValue
					WHEN 'AllTime' THEN 'All Time'
					WHEN 'Custom' THEN 'Custom'
					WHEN 'CurYear' THEN 'Current CY'
					WHEN 'PreYear' THEN 'Previous CY'
					WHEN 'Pre2Year' THEN 'Previous 2 CY'
					WHEN 'Ago2Year' THEN '2 CY Ago'
					WHEN 'NextYear' THEN 'Next CY'
					WHEN 'CurPreYear' THEN 'Current and Previous CY'
					WHEN 'CurPre2Year' THEN 'Current and Previous 2 CY'
					WHEN 'CurNextYear' THEN 'Current and Next CY'
					WHEN 'CuQur' THEN 'Current CQ'
					WHEN 'CurNextQur' THEN 'Current and Next CQ'
					WHEN 'CurPreQur' THEN 'Current and Previous CQ'
					WHEN 'NextQur' THEN 'Next CQ'
					WHEN 'PreQur' THEN 'Previous CQ'
					WHEN 'LastMonth' THEN 'Last Month'
					WHEN 'ThisMonth' THEN 'This Month'
					WHEN 'NextMonth' THEN 'Next Month'
					WHEN 'CurPreMonth' THEN 'Current and Previous Month'
					WHEN 'CurNextMonth' THEN 'Current and Next Month'
					WHEN 'LastWeek' THEN 'Last Week'
					WHEN 'ThisWeek' THEN 'This Week'
					WHEN 'NextWeek' THEN 'Next Week'
					WHEN 'Yesterday' THEN 'Yesterday'
					WHEN 'Today' THEN 'Today'
					WHEN 'Tomorrow' THEN 'Tomorrow'
					WHEN 'Last7Day' THEN 'Last 7 Days'
					WHEN 'Last30Day' THEN 'Last 30 Days'
					WHEN 'Last60Day' THEN 'Last 60 Days'
					WHEN 'Last90Day' THEN 'Last 90 Days'
					WHEN 'Last120Day' THEN 'Last 120 Days'
					WHEN 'Next7Day' THEN 'Next 7 Days'
					WHEN 'Next30Day' THEN 'Next 30 Days'
					WHEN 'Next60Day' THEN 'Next 60 Days'
					WHEN 'Next90Day' THEN 'Next 90 Days'
					WHEN 'Next120Day' THEN 'Next 120 Days'
					ELSE '-'
				END),')')
			ELSE 
				'' 
		END) AS vcTimeline,
		(CASE WHEN (SELECT COUNT(*) FROM ReportFilterList WHERE numReportID=RLM.numReportID) > 0 THEN STUFF((SELECT DISTINCT ', ' + vcFilterValue FROM ReportFilterList WHERE numReportID=RLM.numReportID AND numFieldID=199 FOR XML PATH('')),1,2,'') ELSE '' END) AS vcLocationSource
	FROM 
		ReportListMaster RLM 
	JOIN 
		#tempTable T 
	ON 
		T.numReportID=RLM.numReportID 
	JOIN 
		ReportModuleMaster RMM 
	ON 
		RLM.numReportModuleID=RMM.numReportModuleID
	JOIN 
		ReportModuleGroupMaster RMGM 
	ON 
		RLM.numReportModuleGroupID=RMGM.numReportModuleGroupID                                                  
	WHERE 
		ID > @firstRec 
		AND ID < @lastRec 
	ORDER BY
		ID
   
	DROP TABLE #tempTable
END
GO
