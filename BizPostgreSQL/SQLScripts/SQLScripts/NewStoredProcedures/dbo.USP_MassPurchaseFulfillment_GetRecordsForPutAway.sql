GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_MassPurchaseFulfillment_GetRecordsForPutAway')
DROP PROCEDURE dbo.USP_MassPurchaseFulfillment_GetRecordsForPutAway
GO

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[USP_MassPurchaseFulfillment_GetRecordsForPutAway]
(
	@numDomainID NUMERIC(18,0)
	,@numUserCntID NUMERIC(18,0)
	,@tintMode TINYINT
	,@vcSelectedRecords VARCHAR(MAX)
)
AS
BEGIN
	DECLARE @numDomainDivisionID NUMERIC(18,0)
	SELECT @numDomainDivisionID=ISNULL(@numDomainDivisionID,0) FROM Domain WHERE numDomainId=@numDomainID

	DECLARE @TempFieldsRight TABLE
	(
		ID VARCHAR(50)
		,numFieldID NUMERIC(18,0)
		,vcFieldName VARCHAR(100)
		,vcOrigDbColumnName VARCHAR(100)
		,bitAllowSorting BIT
		,bitAllowFiltering BIT
		,vcAssociatedControlType VARCHAR(20)
		,bitCustomField BIT
		,intRowNum INT
		,intColumnWidth FLOAT
	)

	INSERT INTO @TempFieldsRight
	(
		ID
		,numFieldID
		,vcFieldName
		,vcOrigDbColumnName
		,bitAllowSorting
		,bitAllowFiltering
		,vcAssociatedControlType
		,bitCustomField
		,intRowNum
		,intColumnWidth
	)
	SELECT
		CONCAT(143,'~',DFM.numFieldID,'~',0)
		,DFM.numFieldID
		,DFFM.vcFieldName
		,DFM.vcOrigDbColumnName
		,DFFM.bitAllowSorting
		,DFFM.bitAllowFiltering
		,DFM.vcAssociatedControlType
		,0
		,DFCD.intRowNum
		,ISNULL(DFCD.intColumnWidth,0)
	FROM
		DycFormConfigurationDetails DFCD
	INNER JOIN
		DycFormField_Mapping DFFM
	ON
		DFCD.numFormId = DFFM.numFormID
		AND DFCD.numFieldId = DFFM.numFieldID
	INNER JOIN
		DycFieldMaster DFM
	ON
		DFFM.numFieldID = DFM.numFieldId
	WHERE 
		DFCD.numDomainId=@numDomainID 
		AND DFCD.numUserCntID=@numUserCntID 
		AND DFCD.numFormId=143 
		AND ISNULL(DFFM.bitDeleted,0) = 0
		AND ISNULL(DFFM.bitSettingField,0) = 1
		AND ISNULL(DFCD.bitCustom,0) = 0

	IF (SELECT COUNT(*) FROM @TempFieldsRight) = 0
	BEGIN
		INSERT INTO @TempFieldsRight
		(
			ID
			,numFieldID
			,vcFieldName
			,vcOrigDbColumnName
			,bitAllowSorting
			,bitAllowFiltering
			,vcAssociatedControlType
			,bitCustomField
			,intRowNum
			,intColumnWidth
		)
		SELECT
			CONCAT(143,'~',DFM.numFieldID,'~',0)
			,DFM.numFieldID
			,DFFM.vcFieldName
			,DFM.vcOrigDbColumnName
			,DFFM.bitAllowSorting
			,DFFM.bitAllowFiltering
			,DFM.vcAssociatedControlType
			,0
			,DFFM.[order]
			,100
		FROM
			DycFormField_Mapping DFFM
		INNER JOIN
			DycFieldMaster DFM
		ON
			DFFM.numFieldID = DFM.numFieldId
		WHERE 
			DFFM.numFormId=143 
			AND ISNULL(DFFM.bitDeleted,0) = 0
			AND ISNULL(DFFM.bitSettingField,0) = 1
			AND (ISNULL(DFFM.bitDefault,0) = 1 OR ISNULL(DFFM.bitRequired,0) = 1)
	END
	ELSE
	BEGIN
		INSERT INTO @TempFieldsRight
		(
			ID
			,numFieldID
			,vcFieldName
			,vcOrigDbColumnName
			,bitAllowSorting
			,bitAllowFiltering
			,vcAssociatedControlType
			,bitCustomField
			,intRowNum
			,intColumnWidth
		)
		SELECT
			CONCAT(143,'~',DFM.numFieldID,'~',0)
			,DFM.numFieldID
			,DFFM.vcFieldName
			,DFM.vcOrigDbColumnName
			,DFFM.bitAllowSorting
			,DFFM.bitAllowFiltering
			,DFM.vcAssociatedControlType
			,0
			,DFFM.[order]
			,100
		FROM
			DycFormField_Mapping DFFM
		INNER JOIN
			DycFieldMaster DFM
		ON
			DFFM.numFieldID = DFM.numFieldId
		WHERE 
			DFFM.numFormId=143 
			AND ISNULL(DFFM.bitDeleted,0) = 0
			AND ISNULL(DFFM.bitSettingField,0) = 1
			AND ISNULL(DFFM.bitRequired,0) = 1
			AND DFM.numFieldId NOT IN (SELECT T1.numFieldID FROM @TempFieldsRight T1)
	END

	DECLARE @TEMP TABLE
	(
		ID INT IDENTITY(1,1)
		,numOppID NUMERIC(18,0)
		,numWOID NUMERIC(18,0)
		,numOppItemID NUMERIC(18,0)
	)

	DECLARE @hDocItem int
	EXEC sp_xml_preparedocument @hDocItem OUTPUT, @vcSelectedRecords

	INSERT INTO @TEMP
	(
		numOppID
		,numWOID
		,numOppItemID
	)
	SELECT
		ISNULL(OppID,0)
		,ISNULL(WOID,0)
		,ISNULL(OppItemID,0)
	FROM
		OPENXML (@hDocItem,'/NewDataSet/Table1',2)
	WITH
	(
		OppID NUMERIC(18,0)
		,WOID NUMERIC(18,0)
		,OppItemID NUMERIC(18,0)
	)
	
	EXEC sp_xml_removedocument @hDocItem

	DECLARE @TEMPItems TABLE
	(	
		ID NUMERIC(18,0)
		,numSortOrder INT
		,numOppID NUMERIC(18,0)
		,numWOID NUMERIC(18,0)
		,bitStockTransfer BIT
		,numDivisionID NUMERIC(18,0)
		,numOppItemID NUMERIC(18,0)
		,numItemCode NUMERIC(18,0)
		,numWarehouseItemID NUMERIC(18,0)
		,vcPoppName VARCHAR(300)
		,numBarCodeId VARCHAR(300)
		,vcItemName VARCHAR(300)
		,vcItemDesc VARCHAR(MAX)
		,vcSKU VARCHAR(300)
		,vcPathForTImage VARCHAR(MAX)
		,vcAttributes VARCHAR(MAX)
		,numRemainingQty FLOAT
		,numQtyToShipReceive FLOAT
		,bitSerialized BIT
		,bitLotNo BIT
		,bitPPVariance BIT
		,numCurrencyID NUMERIC(18,0)
		,fltExchangeRate FLOAT
		,charItemType VARCHAR(3)
		,vcItemType VARCHAR(50)
		,monPrice DECIMAL(20,5)
		,bitDropShip BIT
		,numProjectID NUMERIC(18,0)
		,numClassID NUMERIC(18,0)
		,numAssetChartAcntId NUMERIC(18,0)
		,numCOGsChartAcntId NUMERIC(18,0)
		,numIncomeChartAcntId NUMERIC(18,0)
		,SerialLotNo VARCHAR(MAX)
		,vcWarehouses VARCHAR(MAX)
	)

	INSERT INTO @TEMPItems
	(	
		ID
		,numSortOrder
		,numOppID
		,numWOID
		,bitStockTransfer
		,numDivisionID
		,numOppItemID 
		,numItemCode
		,numWarehouseItemID
		,vcPoppName
		,numBarCodeId
		,vcItemName
		,vcItemDesc
		,vcSKU
		,vcPathForTImage
		,vcAttributes 
		,numRemainingQty
		,numQtyToShipReceive
		,bitSerialized
		,bitLotNo
		,bitPPVariance
		,numCurrencyID
		,fltExchangeRate
		,charItemType
		,vcItemType
		,monPrice
		,bitDropShip
		,numProjectID
		,numClassID
		,numAssetChartAcntId
		,numCOGsChartAcntId
		,numIncomeChartAcntId
		,SerialLotNo
		,vcWarehouses
	)
	SELECT 
		T1.ID
		,ISNULL(OpportunityItems.numSortOrder,0)
		,OpportunityMaster.numOppID
		,WorkOrder.numWOId
		,ISNULL(OpportunityMaster.bitStockTransfer,0)
		,(CASE WHEN WorkOrder.numWOId IS NOT NULL THEN @numDomainDivisionID ELSE OpportunityMaster.numDivisionId END)
		,OpportunityItems.numoppitemtCode
		,Item.numItemCode
		,WareHouseItems.numWareHouseItemID
		,(CASE WHEN WorkOrder.numWOId IS NOT NULL THEN ISNULL(WorkOrder.vcWorkOrderName,'-') ELSE OpportunityMaster.vcPoppName END)
		,Item.numBarCodeId
		,Item.vcItemName
		,Item.txtItemDesc AS vcItemDesc
		,Item.vcSKU
		,ISNULL(ItemImages.vcPathForTImage,'') vcPathForTImage
		,ISNULL(OpportunityItems.vcAttributes,'') AS vcAttributes
		,(CASE 
				WHEN @tintMode=1 THEN ISNULL((CASE WHEN WorkOrder.numWOId IS NOT NULL THEN WorkOrder.numQtyBuilt ELSE OpportunityItems.numUnitHour END),0) - ISNULL((CASE WHEN WorkOrder.numWOId IS NOT NULL THEN WorkOrder.numUnitHourReceived ELSE OpportunityItems.numUnitHourReceived END),0)
				WHEN @tintMode=3 THEN ISNULL((CASE WHEN WorkOrder.numWOId IS NOT NULL THEN WorkOrder.numQtyReceived ELSE OpportunityItems.numQtyReceived END),0) - ISNULL((CASE WHEN WorkOrder.numWOId IS NOT NULL THEN WorkOrder.numUnitHourReceived ELSE OpportunityItems.numUnitHourReceived END),0)
				ELSE 0
			END) numRemainingQty
		,0 numQtyToShipReceive
		,ISNULL(bitSerialized,0)
		,ISNULL(bitLotNo,0)
		,ISNULL(OpportunityMaster.bitPPVariance,0)
		,ISNULL(OpportunityMaster.numCurrencyID,0)
		,ISNULL(OpportunityMaster.fltExchangeRate,0)
		,Item.charItemType
		,OpportunityItems.vcType
		,ISNULL(OpportunityItems.monPrice,0)
		,ISNULL(OpportunityItems.bitDropShip,0)
		,ISNULL(OpportunityItems.numProjectID,0)
		,ISNULL(OpportunityItems.numClassID,0)
		,ISNULL(Item.numAssetChartAcntId,0)
		,ISNULL(Item.numCOGsChartAcntId,0)
		,ISNULL(Item.numIncomeChartAcntId,0)
		,(CASE 
			WHEN WorkOrder.numWOId IS NOT NULL THEN '' 
			ELSE SUBSTRING((SELECT 
								CONCAT(',',vcSerialNo,(CASE WHEN isnull(Item.bitLotNo,0)=1 THEN ' (' + CONVERT(VARCHAR(15),whi.numQty) + ')' ELSE '' END))
							FROM 
								OppWarehouseSerializedItem oppI 
							JOIN 
								WareHouseItmsDTL whi
							ON 
								oppI.numWareHouseItmsDTLID=whi.numWareHouseItmsDTLID 
							WHERE 
								oppI.numOppID=OpportunityItems.numOppId 
								and oppI.numOppItemID=OpportunityItems.numoppitemtCode 
							ORDER BY vcSerialNo FOR XML PATH('')),2,200000) END) 
		,(CASE 
			WHEN EXISTS (SELECT numWareHouseItemID FROM WareHouseItems WIInner INNER JOIN WarehouseLocation WL ON WIInner.numWLocationID=WL.numWLocationID WHERE WIInner.numDomainID=@numDomainID AND WIInner.numItemID=Item.numItemCode AND WIInner.numWareHouseID = WareHouseItems.numWareHouseID)
			THEN CONCAT('[',STUFF((SELECT 
										CONCAT(', {"WarehouseItemID":',WIInner.numWareHouseItemID,', "Location":"',ISNULL(WL.vcLocation,''),'"}') 
									FROM 
										WareHouseItems WIInner
									INNER JOIN
										WarehouseLocation WL
									ON
										WIInner.numWLocationID = WL.numWLocationID
									WHERE 
										WIInner.numDomainID=@numDomainID 
										AND WIInner.numItemID=Item.numItemCode 
										AND WIInner.numWareHouseID = WareHouseItems.numWareHouseID
									ORDER BY
										WL.vcLocation ASC
									FOR XML PATH('')),1,1,''),']')
			ELSE CONCAT('[',STUFF((SELECT 
								CONCAT(', {"WarehouseItemID":',WIInner.numWareHouseItemID,', "Location":"',ISNULL(WL.vcLocation,''),'"}') 
							FROM 
								WareHouseItems WIInner
							LEFT JOIN
								WarehouseLocation WL
							ON
								WIInner.numWLocationID = WL.numWLocationID
							WHERE 
								WIInner.numDomainID=@numDomainID 
								AND WIInner.numItemID=Item.numItemCode 
								AND WIInner.numWareHouseItemID=WareHouseItems.numWareHouseItemID
							ORDER BY
								WL.vcLocation ASC
							FOR XML PATH('')),1,1,''),']')
		END) 
	FROM
		@TEMP T1
	LEFT JOIN
		WorkOrder
	ON
		T1.numWOID = WorkOrder.numWOId
	LEFT JOIN
		OpportunityMaster
	ON
		OpportunityMaster.numOppID= T1.numOppID
	LEFT JOIN
		OpportunityItems
	ON
		OpportunityMaster.numOppID = OpportunityItems.numOppID
		AND (OpportunityItems.numOppItemtcode = T1.numOppItemID OR ISNULL(T1.numOppItemID,0) = 0)
	LEFT JOIN
		WareHouseItems 
	ON
		WareHouseItems.numWareHouseItemID = (CASE 
												WHEN WorkOrder.numWOId IS NOT NULL 
												THEN WorkOrder.numWareHouseItemId 
												ELSE (CASE WHEN ISNULL(OpportunityMaster.bitStockTransfer,0) = 1 THEN OpportunityItems.numToWarehouseItemID ELSE OpportunityItems.numWarehouseItmsID END) 
											END)
	INNER JOIN
		Item 
	ON
		(CASE WHEN WorkOrder.numWOId IS NOT NULL THEN WorkOrder.numItemCode ELSE OpportunityItems.numItemCode END) = Item.numItemCode
	LEFT JOIN
		ItemImages
	ON
		ItemImages.numDomainId=@numDomainID
		AND ItemImages.numItemCode = Item.numItemCode
		AND ItemImages.bitDefault=1 
		AND ItemImages.bitIsImage=1
	WHERE
		1 = (CASE 
				WHEN WorkOrder.numWOId IS NOT NULL 
				THEN (CASE WHEN WorkOrder.numDomainID=@numDomainID THEN 1 ELSE 0 END) 
				ELSE (CASE WHEN OpportunityMaster.numDomainID=@numDomainID THEN 1 ELSE 0 END)
			END)
		AND 1 = (CASE 
					WHEN WorkOrder.numWOId IS NOT NULL 
					THEN 1
					ELSE (CASE WHEN ISNULL(OpportunityMaster.tintshipped,0) = 0 THEN 1 ELSE 0 END)
				END)
		AND (WorkOrder.numWOId IS NOT NULL OR (OpportunityMaster.numOppId IS NOT NULL AND OpportunityItems.numoppitemtCode IS NOT NULL))
		AND (CASE 
				WHEN @tintMode=1 THEN ISNULL((CASE WHEN WorkOrder.numWOId IS NOT NULL THEN WorkOrder.numQtyBuilt ELSE OpportunityItems.numUnitHour END),0) - ISNULL((CASE WHEN WorkOrder.numWOId IS NOT NULL THEN WorkOrder.numUnitHourReceived ELSE OpportunityItems.numUnitHourReceived END),0)
				WHEN @tintMode=3 THEN ISNULL((CASE WHEN WorkOrder.numWOId IS NOT NULL THEN WorkOrder.numQtyReceived ELSE OpportunityItems.numQtyReceived END),0) - ISNULL((CASE WHEN WorkOrder.numWOId IS NOT NULL THEN WorkOrder.numUnitHourReceived ELSE OpportunityItems.numUnitHourReceived END),0)
				ELSE 0
			END) > 0
	ORDER BY
		T1.ID, OpportunityItems.numSortOrder

	SELECT * FROM @TEMPItems ORDER BY ID	
	SELECT * FROM @TempFieldsRight ORDER BY intRowNum
END
GO