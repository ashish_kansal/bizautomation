SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_ValidateSerializLot_Name')
DROP PROCEDURE usp_ValidateSerializLot_Name
GO
CREATE PROCEDURE [dbo].[usp_ValidateSerializLot_Name]
(
    @numItemCode as numeric(9)=0,
    @numWarehouseItmsID as numeric(9)=0,
    @strItems as VARCHAR(3000)='',
    @bitSerialized as bit,
    @bitLotNo as bit
)
AS 
    BEGIN
 
Create table #tempTable 
(                                                                    
	vcSerialNo VARCHAR(100),
	TotalQty [numeric](18,0),
	numWarehouseItmsDTLID [numeric](18, 0)                                             
)                       

INSERT INTO #tempTable 
(
	vcSerialNo,TotalQty,numWarehouseItmsDTLID
)  
SELECT 
	vcSerialNo,numWareHouseItmsDTLID,ISNULL(numQty,0) AS TotalQty
FROM   
	WareHouseItmsDTL   
WHERE 
	numWareHouseItemID=@numWarehouseItmsID  
ORDER BY 
	vcSerialNo,TotalQty desc

Create table #tempError 
(                                                                    
	vcSerialNo VARCHAR(100),
	UsedQty [numeric](18, 0),
	AvailableQty [numeric](18, 0),
	vcError VARCHAR(100)                                             
)                       

  
DECLARE @posComma int, @strKeyVal varchar(20)

SET @strItems=RTRIM(@strItems)
IF RIGHT(@strItems, 1)!=',' SET @strItems=@strItems+','

SET @posComma=PatIndex('%,%', @strItems)
WHILE @posComma>1
BEGIN
	SET @strKeyVal=ltrim(rtrim(substring(@strItems, 1, @posComma-1)))
	
	DECLARE @posBStart INT,@posBEnd int, @strQty varchar(20),@strName varchar(20)

    IF @bitLotNo=1 
    BEGIN
		SET @posBStart=PatIndex('%(%', @strKeyVal)
		SET @posBEnd=PatIndex('%)%', @strKeyVal)
		IF( @posBStart>1 AND @posBEnd>1)
		BEGIN
			SET @strName=LTRIM(RTRIM(SUBSTRING(@strKeyVal, 1, @posBStart-1)))
			SET	@strQty=LTRIM(RTRIM(SUBSTRING(@strKeyVal, @posBStart+1,len(@strKeyVal)-@posBStart-1)))
		END
		ELSE
		BEGIN
			SET @strName=@strKeyVal
			SET	@strQty=1
		END
	END
	ELSE
	BEGIN
		SET @strName=@strKeyVal
		SET	@strQty=1
	END  
	  
	DECLARE @AvailableQty NUMERIC(9)
	  
	 IF NOT EXISTS(SELECT 1 FROM #tempTable WHERE vcSerialNo=@strName)
	 BEGIN
		 INSERT INTO #tempError (vcSerialNo,UsedQty,AvailableQty,vcError) VALUES (@strName,@strQty,0,'Serial / Lot # not available')
	END
	 ELSE
	 BEGIN 
	    SELECT TOP 1 @AvailableQty=TotalQty FROM #tempTable WHERE vcSerialNo=@strName 
	    
	    IF  (@strQty>@AvailableQty)	 
		 INSERT INTO #tempError (vcSerialNo,UsedQty,AvailableQty,vcError) VALUES (@strName,@strQty,@AvailableQty,'Used Serial / Lot # more than Available Serial / Lot #')
	  END
	  
	SET @strItems=substring(@strItems, @posComma +1, len(@strItems)-@posComma)
	SET @posComma=PatIndex('%,%',@strItems)
	
END
--SELECT * FROM #tempTable

SELECT * FROM #tempError
DROP TABLE #tempError
DROP TABLE #tempTable

END
