GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetCashFlowReport')
DROP PROCEDURE USP_GetCashFlowReport
GO
CREATE PROCEDURE USP_GetCashFlowReport
(
	@numDomainId as int,
	@dtFromDate AS DATETIME,
	@dtToDate as DATETIME,
	@ClientTimeZoneOffset INT,
	@numAccountClass AS NUMERIC(9)=0,                                           
	@DateFilter AS VARCHAR(20),
	@ReportColumn AS VARCHAR(20)
)
AS
BEGIN
	CREATE TABLE #View_Journal
	(
		numAccountId NUMERIC(18,0),
		datEntry_Date SMALLDATETIME,
		vcAccountCode VARCHAR(300),
		Debit DECIMAL(20,5),
		Credit DECIMAL(20,5)
	)

	INSERT INTO 
		#View_Journal
	SELECT 
		numAccountId
		,datEntry_Date
		,vcAccountCode
		,Debit
		,Credit 
	FROM 
		VIEW_JOURNAL 
	WHERE 
		numDomainId = @numDomainID 
		AND (numAccountClass=@numAccountClass OR @numAccountClass=0)

	DECLARE @PLCHARTID AS NUMERIC(18,0)
	SELECT TOP 1 @PLCHARTID=numAccountId FROM Chart_Of_Accounts WHERE numDomainId=@numDomainID AND bitProfitLoss=1

	DECLARE @FinancialYearStartDate AS DATETIME
	SELECT @FinancialYearStartDate = dbo.GetFiscalStartDate(YEAR(GETDATE()),@numDomainId)

	DECLARE @TEMPQuarter AS TABLE
	(
		ID INT,
		StartDate DATETIME,
		EndDate DATETIME
	)

	INSERT INTO @TEMPQuarter VALUES (1,@FinancialYearStartDate,DATEADD(ms,-3,DATEADD(QUARTER,1,@FinancialYearStartDate)))
	INSERT INTO @TEMPQuarter VALUES (2,DATEADD(QUARTER,1,@FinancialYearStartDate),DATEADD(ms,-3,DATEADD(QUARTER,2,@FinancialYearStartDate)))
	INSERT INTO @TEMPQuarter VALUES (3,DATEADD(QUARTER,2,@FinancialYearStartDate),DATEADD(ms,-3,DATEADD(QUARTER,3,@FinancialYearStartDate)))
	INSERT INTO @TEMPQuarter VALUES (4,DATEADD(QUARTER,3,@FinancialYearStartDate),DATEADD(ms,-3,DATEADD(QUARTER,4,@FinancialYearStartDate)))

	IF @DateFilter = 'CurYear'
	BEGIN
		SELECT @dtFromDate = @FinancialYearStartDate,
			@dtToDate = DATEADD(ms,-3,DATEADD(YEAR,1,@FinancialYearStartDate));
	END
	ELSE IF @DateFilter = 'PreYear'
	BEGIN
		SELECT @dtFromDate = DATEADD(YEAR,-1,@FinancialYearStartDate),
			@dtToDate = DATEADD(ms,-3,DATEADD(YEAR,1,DATEADD(YEAR,-1,@FinancialYearStartDate)));
	END
	ELSE IF @DateFilter = 'CurPreYear'
	BEGIN
		SELECT @dtFromDate = DATEADD(YEAR,-1,@FinancialYearStartDate),
			@dtToDate = DATEADD(ms,-3,DATEADD(YEAR,1,@FinancialYearStartDate));
	END
	ELSE IF @DateFilter = 'CuQur'
	BEGIN
		SELECT @dtFromDate = StartDate, @dtToDate=EndDate FROM @TEMPQuarter WHERE GetDate() BETWEEN StartDate AND EndDate
	END
	ELSE IF @DateFilter = 'CurPreQur'
	BEGIN
		SELECT @dtFromDate = DATEADD(QUARTER,-1,StartDate), @dtToDate=EndDate FROM @TEMPQuarter WHERE GetDate() BETWEEN StartDate AND EndDate
	END
	ELSE IF @DateFilter = 'PreQur'
	BEGIN
		SELECT @dtFromDate = DATEADD(QUARTER,-1,StartDate), @dtToDate=DATEADD(QUARTER,-1,EndDate) FROM @TEMPQuarter WHERE GetDate() BETWEEN StartDate AND EndDate
	END
	ELSE IF @DateFilter = 'LastMonth'
	BEGIN
		select @dtFromDate = DATEADD(mm,-1,DATEADD(mm,DATEDIFF(mm,0,GETDATE()),0)),
				@dtToDate = DATEADD(ms,-3,DATEADD(mm,0,DATEADD(mm,DATEDIFF(mm,0,GETDATE()),0)));  
	END
	ELSE IF @DateFilter = 'ThisMonth'
	BEGIN
		select @dtFromDate = DATEADD(mm,DATEDIFF(mm,0,GETDATE()),0),
				@dtToDate = DATEADD(ms,- 3,DATEADD(mm,0,DATEADD(mm,DATEDIFF(mm,0,GETDATE())+1,0)));  
	END
	ELSE IF @DateFilter = 'CurPreMonth'
	BEGIN
		select @dtFromDate = DATEADD(mm,-1,DATEADD(mm,DATEDIFF(mm,0,GETDATE()),0)),
				@dtToDate = DATEADD(ms,- 3,DATEADD(mm,0,DATEADD(mm,DATEDIFF(mm,0,GETDATE())+1,0))); 
	END

	DECLARE @TEMP TABLE
	(
		ParentId VARCHAR(300)
		,vcCompundParentKey VARCHAR(300)
		,numAccountTypeID NUMERIC(18,0)
		,numAccountID NUMERIC(18,0)
		,vcAccountType VARCHAR(300)
		,vcAccountCode VARCHAR(300)
		,[LEVEL] INT
		,Struc VARCHAR(300)
		,bitTotal BIT
		,[Type] INT
	)

	INSERT INTO @TEMP
	(
		ParentId,vcCompundParentKey,numAccountTypeID,vcAccountType,[LEVEL],numAccountID,vcAccountCode,Struc,bitTotal,[Type]
	)
	VALUES
	(
		'','-1',-1,'Operating Activities',0,NULL,NULL,'-1',0,2
	),
	(
		'-1','-2',-2,'Net Income',1,NULL,NULL,'-1#2',0,1
	),
	(
		'-1','-3',-3,'Adjustments to reconcile Net Income to Net Cash provided by operations:',1,NULL,NULL,'-1#3',0,2
	),
	(
		'-1','-3',-3,'Total Adjustments to reconcile Net Income to Net Cash provided by operations:',1,NULL,NULL,'-1#3#Total',1,2
	),
	(
		'','-1',-1,'Net cash provided by operating activities',0,NULL,NULL,'-1#Total',1,2
	),
	(
		'','-4',-4,'Investing Activities',0,NULL,NULL,'-4',0,2
	),
	(
		'','-4',-4,'Net cash provided by investing activities',0,NULL,NULL,'-4#Total',1,2
	),
	(
		'','-5',-5,'Financing Activities',0,NULL,NULL,'-5',0,2
	),
	(
		'','-5',-5,'Net cash provided by financing activities',0,NULL,NULL,'-5#Total',1,2
	),
	(
		'','-6',-6,'Net cash increase for period',0,NULL,NULL,'-6',1,2
	),
	(
		'','-7',-7,'Net cash at Beginning of Period',0,NULL,NULL,'-7',0,2
	),
	(
		'','-8',-8,'Net cash at End of Period',0,NULL,NULL,'-8',1,2
	)

	-- Operating Activities
	;WITH DirectReport (ParentId,vcCompundParentKey,numAccountTypeID,vcAccountType,[LEVEL],numAccountID,vcAccountCode,Struc,bitTotal,[Type])
	AS
	(
		SELECT
			CAST('-3' AS VARCHAR)
			,CAST(CONCAT(COA.numParntAcntTypeId,'#',COA.numAccountId) AS VARCHAR)
			,NULL
			,vcAccountName
			,2
			,numAccountID
			,vcAccountCode
			,CAST(CONCAT('-1#3#',numAccountID) AS VARCHAR(300))
			,0
			,1
		FROM 
			[dbo].[Chart_of_Accounts] AS COA 
		WHERE 
			[COA].[numDomainID]=@numDomainId 
			AND ISNULL(COA.bitIsSubAccount,0) = 0
			AND (vcAccountCode LIKE '01010105%' OR vcAccountCode LIKE '01010104%' OR vcAccountCode LIKE '010105%' OR vcAccountCode LIKE '01020102%' OR vcAccountCode LIKE '01020101%')
		UNION ALL
		SELECT 
			CAST(CONCAT(COA.numParntAcntTypeId,'#',COA.numParentAccId) AS VARCHAR) AS ParentId,
			CAST(CONCAT(COA.numParntAcntTypeId,'#',COA.numAccountId) AS VARCHAR),
			NULL,
			vcAccountName,
			D.[LEVEL] + 1,
			[COA].[numAccountId],
			COA.[vcAccountCode],
			CAST(d.Struc + '#' + CAST(COA.[numAccountId] AS VARCHAR) AS VARCHAR(300)) AS Struc
			,0
			,1
		FROM 
			[dbo].[Chart_of_Accounts] AS COA 
		JOIN 
			[DirectReport] D 
		ON 
			D.numAccountId = COA.numParentAccId
		WHERE 
			[COA].[numDomainID]=@numDomainId 
			AND ISNULL(COA.bitIsSubAccount,0) = 1
			AND (COA.vcAccountCode LIKE '01010105%' OR COA.vcAccountCode LIKE '01010104%' OR COA.vcAccountCode LIKE '010105%' OR COA.vcAccountCode LIKE '01020102%' OR COA.vcAccountCode LIKE '01020101%')
	)

	INSERT INTO @TEMP
	(
		ParentId,vcCompundParentKey,numAccountTypeID,vcAccountType,[LEVEL],numAccountID,vcAccountCode,Struc,bitTotal,[Type]
	)
	SELECT 
		ParentId,vcCompundParentKey,numAccountTypeID,vcAccountType,[LEVEL],numAccountID,vcAccountCode,Struc,bitTotal,[Type]
	FROM
		DirectReport

	-- Investing Activities
	;WITH DirectReport (ParentId,vcCompundParentKey,numAccountTypeID,vcAccountType,[LEVEL],numAccountID,vcAccountCode,Struc,bitTotal,[Type])
	AS
	(
		SELECT
			CAST('-4' AS VARCHAR)
			,CAST(CONCAT(COA.numParntAcntTypeId,'#',COA.numAccountId) AS VARCHAR)
			,NULL
			,vcAccountName
			,1
			,numAccountID
			,vcAccountCode
			,CAST(CONCAT('-4#',numAccountID) AS VARCHAR(300))
			,0
			,1
		FROM 
			[dbo].[Chart_of_Accounts] AS COA 
		WHERE 
			[COA].[numDomainID]=@numDomainId 
			AND ISNULL(COA.bitIsSubAccount,0) = 0
			AND vcAccountCode LIKE '010102%'
		UNION ALL
		SELECT 
			CAST(CONCAT(COA.numParntAcntTypeId,'#',COA.numParentAccId) AS VARCHAR) AS ParentId,
			CAST(CONCAT(COA.numParntAcntTypeId,'#',COA.numAccountId) AS VARCHAR),
			NULL,
			[vcAccountName],
			LEVEL + 1,
			[COA].[numAccountId], 
			COA.[vcAccountCode],			
			CAST(d.Struc + '#' + CAST(COA.[numAccountId] AS VARCHAR) AS VARCHAR(300)) AS Struc
			,0
			,1
		FROM 
			[dbo].[Chart_of_Accounts] AS COA 
		JOIN 
			[DirectReport] D 
		ON 
			D.numAccountId = COA.numParentAccId
		WHERE 
			[COA].[numDomainID]=@numDomainId 
			AND ISNULL(COA.bitIsSubAccount,0) = 1
			AND COA.vcAccountCode LIKE '010102%'
	)

	INSERT INTO @TEMP
	(
		ParentId,vcCompundParentKey,numAccountTypeID,vcAccountType,[LEVEL],numAccountID,vcAccountCode,Struc,bitTotal,[Type]
	)
	SELECT 
		ParentId,vcCompundParentKey,numAccountTypeID,vcAccountType,[LEVEL],numAccountID,vcAccountCode,Struc,bitTotal,[Type]
	FROM
		DirectReport

	-- Financing Activities
	;WITH DirectReport (ParentId,vcCompundParentKey,numAccountTypeID,vcAccountType,[LEVEL],numAccountID,vcAccountCode,Struc,bitTotal,[Type])
	AS
	(
		SELECT
			CAST('-5'  AS VARCHAR)
			,CAST(CONCAT(COA.numParntAcntTypeId,'#',COA.numAccountId) AS VARCHAR)
			,NULL
			,vcAccountName
			,1
			,numAccountID
			,vcAccountCode
			,CAST(CONCAT('-5#',numAccountID) AS VARCHAR(300))
			,0
			,1
		FROM 
			[dbo].[Chart_of_Accounts] AS COA 
		WHERE 
			[COA].[numDomainID]=@numDomainId 
			AND ISNULL(COA.bitIsSubAccount,0) = 0
			AND vcAccountCode LIKE '010202%'
		UNION ALL
		SELECT 
			CAST(CONCAT(COA.numParntAcntTypeId,'#',COA.numParentAccId) AS VARCHAR) AS ParentId,
			CAST(CONCAT(COA.numParntAcntTypeId,'#',COA.numAccountId) AS VARCHAR),
			NULL,
			[vcAccountName],
			LEVEL + 1,
			[COA].[numAccountId],
			COA.[vcAccountCode],
			CAST(d.Struc + '#' + CAST(COA.[numAccountId] AS VARCHAR) AS VARCHAR(300)) AS Struc
			,0
			,1
		FROM 
			[dbo].[Chart_of_Accounts] AS COA 
		JOIN 
			[DirectReport] D 
		ON 
			D.numAccountId = COA.numParentAccId
		WHERE 
			[COA].[numDomainID]=@numDomainId 
			AND ISNULL(COA.bitIsSubAccount,0) = 1
			AND COA.vcAccountCode LIKE '010202%'
	)

	INSERT INTO @TEMP
	(
		ParentId,vcCompundParentKey,numAccountTypeID,vcAccountType,[LEVEL],numAccountID,vcAccountCode,Struc,bitTotal,[Type]
	)
	SELECT 
		ParentId,vcCompundParentKey,numAccountTypeID,vcAccountType,[LEVEL],numAccountID,vcAccountCode,Struc,bitTotal,[Type]
	FROM
		DirectReport

	DECLARE @columns VARCHAR(8000);--SET @columns = '';
	DECLARE @SUMColumns VARCHAR(8000);--SET @SUMColumns = '';
	DECLARE @PivotColumns VARCHAR(8000);--SET @PivotColumns = '';
	DECLARE @Select VARCHAR(8000) = 'SELECT ParentId, vcCompundParentKey, numAccountTypeID, vcAccountType, LEVEL, vcAccountCode, numAccountId,Struc'
	DECLARE @Where VARCHAR(8000)
	DECLARE @Update6 VARCHAR(MAX) = ''
	DECLARE @Update8 VARCHAR(MAX) = ''
	DECLARE @sql VARCHAR(MAX) = ''

	IF @ReportColumn = 'Year'
	BEGIN
		SELECT 
			*
		INTO 
			#tempDirectReportYear
		FROM 
			@TEMP

		CREATE TABLE #tempYearMonth
		(
			intYear INT,
			intMonth INT,
			MonthYear VARCHAR(50),
			StartDate DATETIME,
			EndDate DATETIME,
			NetIncomeAmount DECIMAL(20,5),
			CashAtBegining DECIMAL(20,5)
		)

		;WITH CTE AS 
		(
			SELECT
				YEAR(@dtFromDate) AS 'yr',
				MONTH(@dtFromDate) AS 'mm',
				FORMAT(@dtFromDate,'MMM') AS 'mon',
				(FORMAT(@dtFromDate,'MMM') + '_' + CAST(YEAR(@dtFromDate) AS VARCHAR)) AS MonthYear,
				CAST(DATEFROMPARTS(YEAR(@dtFromDate),MONTH(@dtFromDate),1) AS DATETIME) dtStartDate,
				DATEADD(MS,-2,CAST(DATEADD(MONTH,1,DATEFROMPARTS(YEAR(@dtFromDate),MONTH(@dtFromDate),1)) AS DATETIME)) dtEndDate,
				@dtFromDate 'new_date'
			UNION ALL
			SELECT
				YEAR(DATEADD(d,1,new_date)) AS 'yr',
				MONTH(DATEADD(d,1,new_date)) AS 'mm',
				FORMAT(DATEADD(d,1,new_date),'MMM') AS 'mon',
				(FORMAT(DATEADD(d,1,new_date),'MMM') + '_' + CAST(YEAR(DATEADD(d,1,new_date)) AS VARCHAR)) AS MonthYear,
				CAST(DATEFROMPARTS(YEAR(DATEADD(d,1,new_date)),MONTH(DATEADD(d,1,new_date)),1) AS DATETIME) dtStartDate,
				DATEADD(MS,-2,CAST(DATEADD(MONTH,1,DATEFROMPARTS(YEAR(DATEADD(d,1,new_date)),MONTH(DATEADD(d,1,new_date)),1)) AS DATETIME)) dtEndDate,
				DATEADD(d,1,new_date) 'new_date'
			FROM CTE
			WHERE DATEADD(d,1,new_date) < @dtToDate
		)
	
		INSERT INTO 
			#tempYearMonth
		SELECT 
			yr,mm,MonthYear,dtStartDate,dtEndDate,0,0
		FROM 
			CTE
		GROUP BY 
			yr, mm, MonthYear, dtStartDate, dtEndDate
		ORDER BY 
			yr, mm
		OPTION (MAXRECURSION 5000)

		INSERT INTO #tempYearMonth VALUES (5000,5000,'Total',@dtFromDate,@dtToDate,0,0)

		UPDATE 
			#tempYearMonth
		SET
			EndDate = (CASE WHEN EndDate < @dtToDate THEN EndDate ELSE @dtToDate END)

		UPDATE 
			#tempYearMonth
		SET
			NetIncomeAmount = ISNULL((SELECT 
											ISNULL(SUM(VJ.Credit),0) - ISNULL(SUM(VJ.Debit),0) 
										FROM 
											#View_Journal VJ
										WHERE 
											((VJ.vcAccountCode  like '0103%' OR VJ.vcAccountCode LIKE '0104%' OR VJ.vcAccountCode LIKE '0106%') OR VJ.numaccountid = @PLCHARTID) 
											AND VJ.datentry_date BETWEEN StartDate AND EndDate),0) + ISNULL((SELECT 
																												ISNULL(SUM(VJ.Credit),0) - ISNULL(SUM(VJ.Debit),0) 
																											FROM 
																												#View_Journal VJ
																											WHERE 
																												((VJ.vcAccountCode  like '0103%' OR VJ.vcAccountCode LIKE '0104%' OR VJ.vcAccountCode LIKE '0106%') OR VJ.numaccountid = @PLCHARTID) 
																												AND VJ.datentry_date <  StartDate),0)
			,CashAtBegining = ISNULL((SELECT 
											ISNULL(SUM(credit),0) - ISNULL(SUM(debit),0)
										FROM 
											#View_Journal vj 
										WHERE 
											vcAccountCode LIKE '01010101%' 
											AND datentry_date < StartDate),0)


		SELECT @columns = STUFF((SELECT ', ISNULL(' + MonthYear  + ',0) AS [' + REPLACE(MonthYear,'_',' ') + ']' FROM #tempYearMonth ORDER BY intYear,intMonth FOR XML PATH('')), 1, 2, ''); 
		SELECT @sumColumns = STUFF((SELECT ', SUM(ISNULL(V.[' + REPLACE(MonthYear,'_',' ') + ']' + ',0)) AS [' + REPLACE(MonthYear,'_',' ') + ']' FROM #tempYearMonth ORDER BY intYear,intMonth FOR XML PATH('')), 1, 2, ''); 
		SET @Update6 = CONCAT('UPDATE #tempFinalDataYear1 SET ',STUFF((SELECT ', [' + REPLACE(MonthYear,'_',' ') + '] = ISNULL((SELECT SUM(TInner.[' + REPLACE(MonthYear,'_',' ') + ']) FROM #tempFinalDataYear1 TInner WHERE TInner.numAccountTypeID IN (-1,-4,-5) AND bitTotal=1),0)' FROM #tempYearMonth ORDER BY intYear,intMonth FOR XML PATH('')), 1, 2, ''),' WHERE numAccountTypeID=-6')
		SET @Update8 = CONCAT('UPDATE #tempFinalDataYear1 SET ',STUFF((SELECT ', [' + REPLACE(MonthYear,'_',' ') + '] = ISNULL((SELECT SUM(TInner.[' + REPLACE(MonthYear,'_',' ') + ']) FROM #tempFinalDataYear1 TInner WHERE TInner.numAccountTypeID IN (-6,-7)),0)' FROM #tempYearMonth ORDER BY intYear,intMonth FOR XML PATH('')), 1, 2, ''),' WHERE numAccountTypeID=-8')

		SELECT @PivotColumns = STUFF((SELECT ', ' + MonthYear FROM #tempYearMonth ORDER BY intYear,intMonth FOR XML PATH('')), 1, 2, ''); 


		SET @sql = 'SELECT
						ParentId,
						vcCompundParentKey,
						numAccountId,
						numAccountTypeID,
						vcAccountType,
						LEVEL,
						vcAccountCode,
						Struc,
						[Type],
						bitTotal,' + @columns + '
					INTO
						#tempFinalDataYear
					FROM
					(
						SELECT 
							T.ParentId,
							T.vcCompundParentKey,
							T.numAccountId,
							T.numAccountTypeID,
							T.vcAccountType,
							T.LEVEL,
							T.vcAccountCode,
							T.Struc,
							(CASE WHEN ISNULL(T.numAccountId,0) > 0 THEN 1 ELSE 2 END) [Type],
							T.bitTotal,
							(CASE 
								WHEN numAccountTypeID = -2
								THEN Period.NetIncomeAmount
								WHEN numAccountTypeID = -7
								THEN Period.CashAtBegining
								ELSE
									ISNULL(TCurrent.monAmount,0) - ISNULL(TOpening.monAmount,0)
							END) AS Amount,
							Period.MonthYear
						FROM 
							#tempDirectReportYear T
						OUTER APPLY
						(
							SELECT
								MonthYear,
								StartDate,
								EndDate,
								NetIncomeAmount,
								CashAtBegining
							FROM
								#tempYearMonth
						) AS Period
						OUTER APPLY
						(
							SELECT
								ISNULL(SUM(VF.Credit),0) - ISNULL(SUM(VF.debit),0) monAmount
							FROM
								#View_Journal VF 
							WHERE  
								VF.numAccountId = T.numAccountId
								AND VF.datEntry_Date < Period.StartDate
						) TOpening
						OUTER APPLY
						(
							SELECT
								ISNULL(SUM(VT.Credit),0) - ISNULL(SUM(VT.debit),0) monAmount
							FROM
								#View_Journal VT 
							WHERE  
								VT.numAccountId = T.numAccountId
								AND VT.datEntry_Date <= Period.EndDate
						) TCurrent
						
					) AS SourceTable
					pivot
					(
						SUM(AMOUNT)
						FOR [MonthYear] IN ( ' + @pivotcolumns + ' )
					) AS P; '

		SET @sql = CONCAT(@sql,'SELECT 
									COA.ParentId
									,COA.vcCompundParentKey
									,COA.numAccountTypeID
									,COA.vcAccountType
									,COA.[LEVEL]
									,COA.vcAccountCode
									,COA.numAccountId
									,(''#'' + COA.Struc + ''#'') AS Struc
									,COA.[Type]
									,COA.bitTotal
									,',@SUMColumns,' INTO #tempFinalDataYear1
								FROM 
									#tempDirectReportYear COA 
								LEFT JOIN 
									#tempFinalDataYear V
								ON  
									V.Struc like REPLACE(COA.Struc,''#Total'','''') + ''%''
								GROUP BY 
									COA.ParentId
									,COA.vcCompundParentKey
									,COA.numAccountTypeID
									,COA.vcAccountType
									,COA.LEVEL
									,COA.vcAccountCode
									,COA.numAccountId
									,COA.Struc
									,COA.[Type]
									,COA.bitTotal
								ORDER BY
									COA.Struc;',@Update6,';',@Update8,'; SELECT * FROM #tempFinalDataYear1; DROP TABLE #tempFinalDataYear; DROP TABLE #tempFinalDataYear1;')

		PRINT CAST(@sql AS NTEXT)

		EXECUTE (@sql)

		DROP TABLE #tempYearMonth
		DROP TABLE #tempDirectReportYear
		
	END
	ELSE IF @ReportColumn = 'Quarter'
	BEGIN
		SELECT 
			*
		INTO 
			#tempDirectReportQuarter
		FROM 
			@TEMP

		CREATE TABLE #TempYearMonthQuarter
		(
			intYear INT,
			intQuarter INT,
			MonthYear VARCHAR(50),
			StartDate DATETIME,
			EndDate DATETIME,
			NetIncomeAmount DECIMAL(20,5),
			CashAtBegining DECIMAL(20,5)
		)

		;WITH CTE AS 
		(
			SELECT
				dbo.GetFiscalyear(@dtFromDate,@numDomainId) AS 'yr',
				dbo.GetFiscalQuarter(@dtFromDate,@numDomainId) AS 'qq',
				('Q' + cast(dbo.GetFiscalQuarter(@dtFromDate,@numDomainId) AS VARCHAR) + '_' + cast(dbo.GetFiscalyear(@dtFromDate,@numDomainId) AS VARCHAR)) AS MonthYear,
				CASE dbo.GetFiscalQuarter(@dtFromDate,@numDomainId) 
				WHEN 1 THEN dateadd(month,0,dbo.GetFiscalStartDate(dbo.GetFiscalyear(@dtFromDate,@numDomainId),@numDomainId))
				WHEN 2 THEN dateadd(month,3,dbo.GetFiscalStartDate(dbo.GetFiscalyear(@dtFromDate,@numDomainId),@numDomainId)) 
				WHEN 3 THEN dateadd(month,6,dbo.GetFiscalStartDate(dbo.GetFiscalyear(@dtFromDate,@numDomainId),@numDomainId))
				WHEN 4 THEN dateadd(month,9,dbo.GetFiscalStartDate(dbo.GetFiscalyear(@dtFromDate,@numDomainId),@numDomainId)) 
				END AS dtStartDate,
				CASE dbo.GetFiscalQuarter(@dtFromDate,@numDomainId) 
				WHEN 1 THEN dateadd(MS,-3,dateadd(month,3,dbo.GetFiscalStartDate(dbo.GetFiscalyear(@dtFromDate,@numDomainId),@numDomainId)))
				WHEN 2 THEN dateadd(MS,-3,dateadd(month,6,dbo.GetFiscalStartDate(dbo.GetFiscalyear(@dtFromDate,@numDomainId),@numDomainId)))
				WHEN 3 THEN dateadd(MS,-3,dateadd(month,9,dbo.GetFiscalStartDate(dbo.GetFiscalyear(@dtFromDate,@numDomainId),@numDomainId)))
				WHEN 4 THEN dateadd(MS,-3,dateadd(month,12,dbo.GetFiscalStartDate(dbo.GetFiscalyear(@dtFromDate,@numDomainId),@numDomainId)))
				END AS dtEndDate,
				@dtFromDate 'new_date'
			UNION ALL
			SELECT
				dbo.GetFiscalyear(DATEADD(d,1,new_date),@numDomainId) AS 'yr',
				dbo.GetFiscalQuarter(DATEADD(d,1,new_date),@numDomainId) AS 'qq',
				('Q' + cast(dbo.GetFiscalQuarter(DATEADD(d,1,new_date),@numDomainId) AS VARCHAR) + '_' + cast(dbo.GetFiscalyear(DATEADD(d,1,new_date),@numDomainId) AS VARCHAR)) AS MonthYear,
				CASE dbo.GetFiscalQuarter(DATEADD(d,1,new_date),@numDomainId) 
				WHEN 1 THEN dateadd(month,0,dbo.GetFiscalStartDate(dbo.GetFiscalyear(DATEADD(d,1,new_date),@numDomainId),@numDomainId))
				WHEN 2 THEN dateadd(month,3,dbo.GetFiscalStartDate(dbo.GetFiscalyear(DATEADD(d,1,new_date),@numDomainId),@numDomainId)) 
				WHEN 3 THEN dateadd(month,6,dbo.GetFiscalStartDate(dbo.GetFiscalyear(DATEADD(d,1,new_date),@numDomainId),@numDomainId))
				WHEN 4 THEN dateadd(month,9,dbo.GetFiscalStartDate(dbo.GetFiscalyear(DATEADD(d,1,new_date),@numDomainId),@numDomainId)) 
				END AS dtStartDate,
				CASE dbo.GetFiscalQuarter(DATEADD(d,1,new_date),@numDomainId) 
				WHEN 1 THEN dateadd(MS,-3,dateadd(month,3,dbo.GetFiscalStartDate(dbo.GetFiscalyear(DATEADD(d,1,new_date),@numDomainId),@numDomainId)))
				WHEN 2 THEN dateadd(MS,-3,dateadd(month,6,dbo.GetFiscalStartDate(dbo.GetFiscalyear(DATEADD(d,1,new_date),@numDomainId),@numDomainId)))
				WHEN 3 THEN dateadd(MS,-3,dateadd(month,9,dbo.GetFiscalStartDate(dbo.GetFiscalyear(DATEADD(d,1,new_date),@numDomainId),@numDomainId)))
				WHEN 4 THEN dateadd(MS,-3,dateadd(month,12,dbo.GetFiscalStartDate(dbo.GetFiscalyear(DATEADD(d,1,new_date),@numDomainId),@numDomainId)))
				END AS dtEndDate,
				DATEADD(d,1,new_date) 'new_date'
			FROM CTE
			WHERE DATEADD(d,1,new_date) < @dtToDate
		)
	
		INSERT INTO 
			#TempYearMonthQuarter 		
		SELECT 
			yr,qq,MonthYear,dtStartDate,dtEndDate,0,0
		FROM 
			CTE
		GROUP BY 
			yr, qq, MonthYear, dtStartDate, dtEndDate
		ORDER BY 
			yr, qq
		OPTION
			(MAXRECURSION 5000)

		INSERT INTO #TempYearMonthQuarter VALUES (5000,5000,'Total',@dtFromDate,@dtToDate,0,0)

		UPDATE 
			#TempYearMonthQuarter
		SET
			EndDate = (CASE WHEN EndDate < @dtToDate THEN EndDate ELSE @dtToDate END)

		UPDATE 
			#TempYearMonthQuarter
		SET
			NetIncomeAmount = ISNULL((SELECT 
											ISNULL(SUM(VJ.Credit),0) - ISNULL(SUM(VJ.Debit),0) 
										FROM 
											#View_Journal VJ
										WHERE 
											((VJ.vcAccountCode  like '0103%' OR VJ.vcAccountCode LIKE '0104%' OR VJ.vcAccountCode LIKE '0106%') OR VJ.numaccountid = @PLCHARTID) 
											AND VJ.datentry_date BETWEEN StartDate AND EndDate),0) + ISNULL((SELECT 
																												ISNULL(SUM(VJ.Credit),0) - ISNULL(SUM(VJ.Debit),0) 
																											FROM 
																												#View_Journal VJ
																											WHERE 
																												((VJ.vcAccountCode  like '0103%' OR VJ.vcAccountCode LIKE '0104%' OR VJ.vcAccountCode LIKE '0106%') OR VJ.numaccountid = @PLCHARTID) 
																												AND VJ.datentry_date <  StartDate),0)
			,CashAtBegining = ISNULL((SELECT 
											ISNULL(SUM(credit),0) - ISNULL(SUM(debit),0)
										FROM 
											#View_Journal vj 
										WHERE 
											vcAccountCode LIKE '01010101%' 
											AND datentry_date < StartDate),0)

		SELECT @columns = STUFF((SELECT ', ISNULL(' + MonthYear  + ',0) AS [' + REPLACE(MonthYear,'_',' ') + ']' FROM #TempYearMonthQuarter ORDER BY intYear,intQuarter FOR XML PATH('')), 1, 2, ''); 
		SELECT @sumColumns = STUFF((SELECT ', SUM(ISNULL(V.[' + REPLACE(MonthYear,'_',' ') + ']' + ',0)) AS [' + REPLACE(MonthYear,'_',' ') + ']' FROM #TempYearMonthQuarter ORDER BY intYear,intQuarter FOR XML PATH('')), 1, 2, ''); 
		SET @Update6 = CONCAT('UPDATE #tempFinalDataQuarter1 SET ',STUFF((SELECT ', [' + REPLACE(MonthYear,'_',' ') + '] = ISNULL((SELECT SUM(TInner.[' + REPLACE(MonthYear,'_',' ') + ']) FROM #tempFinalDataQuarter1 TInner WHERE TInner.numAccountTypeID IN (-1,-4,-5) AND bitTotal=1),0)' FROM #TempYearMonthQuarter ORDER BY intYear,intQuarter FOR XML PATH('')), 1, 2, ''),' WHERE numAccountTypeID=-6')
		SET @Update8 = CONCAT('UPDATE #tempFinalDataQuarter1 SET ',STUFF((SELECT ', [' + REPLACE(MonthYear,'_',' ') + '] = ISNULL((SELECT SUM(TInner.[' + REPLACE(MonthYear,'_',' ') + ']) FROM #tempFinalDataQuarter1 TInner WHERE TInner.numAccountTypeID IN (-6,-7)),0)' FROM #TempYearMonthQuarter ORDER BY intYear,intQuarter FOR XML PATH('')), 1, 2, ''),' WHERE numAccountTypeID=-8')

		SELECT @PivotColumns = STUFF((SELECT ', ' + MonthYear FROM #TempYearMonthQuarter ORDER BY intYear,intQuarter FOR XML PATH('')), 1, 2, ''); 


		SET @sql = 'SELECT
						ParentId,
						vcCompundParentKey,
						numAccountId,
						numAccountTypeID,
						vcAccountType,
						LEVEL,
						vcAccountCode,
						Struc,
						[Type],
						bitTotal,' + @columns + '
					INTO
						#tempFinalDataQuarter
					FROM
					(
						SELECT 
							T.ParentId,
							T.vcCompundParentKey,
							T.numAccountId,
							T.numAccountTypeID,
							T.vcAccountType,
							T.LEVEL,
							T.vcAccountCode,
							T.Struc,
							(CASE WHEN ISNULL(T.numAccountId,0) > 0 THEN 1 ELSE 2 END) [Type],
							T.bitTotal,
							(CASE 
								WHEN numAccountTypeID = -2
								THEN Period.NetIncomeAmount
								WHEN numAccountTypeID = -7
								THEN Period.CashAtBegining
								ELSE
									ISNULL(TCurrent.monAmount,0) - ISNULL(TOpening.monAmount,0)
							END) AS Amount,
							Period.MonthYear
						FROM 
							#tempDirectReportQuarter T
						OUTER APPLY
						(
							SELECT
								MonthYear,
								StartDate,
								EndDate,
								NetIncomeAmount,
								CashAtBegining
							FROM
								#TempYearMonthQuarter
						) AS Period
						OUTER APPLY
						(
							SELECT
								ISNULL(SUM(VF.Credit),0) - ISNULL(SUM(VF.debit),0) monAmount
							FROM
								#View_Journal VF 
							WHERE  
								VF.numAccountId = T.numAccountId
								AND VF.datEntry_Date < Period.StartDate
						) TOpening
						OUTER APPLY
						(
							SELECT
								ISNULL(SUM(VT.Credit),0) - ISNULL(SUM(VT.debit),0) monAmount
							FROM
								#View_Journal VT 
							WHERE  
								VT.numAccountId = T.numAccountId
								AND VT.datEntry_Date <= Period.EndDate
						) TCurrent
						
					) AS SourceTable
					pivot
					(
						SUM(AMOUNT)
						FOR [MonthYear] IN ( ' + @pivotcolumns + ' )
					) AS P; '

		SET @sql = CONCAT(@sql,'SELECT 
									COA.ParentId
									,COA.vcCompundParentKey
									,COA.numAccountTypeID
									,COA.vcAccountType
									,COA.[LEVEL]
									,COA.vcAccountCode
									,COA.numAccountId
									,(''#'' + COA.Struc + ''#'') AS Struc
									,COA.[Type]
									,COA.bitTotal
									,',@SUMColumns,' INTO #tempFinalDataQuarter1
								FROM 
									#tempDirectReportQuarter COA 
								LEFT JOIN 
									#tempFinalDataQuarter V
								ON  
									V.Struc like REPLACE(COA.Struc,''#Total'','''') + ''%''
								GROUP BY 
									COA.ParentId
									,COA.vcCompundParentKey
									,COA.numAccountTypeID
									,COA.vcAccountType
									,COA.LEVEL
									,COA.vcAccountCode
									,COA.numAccountId
									,COA.Struc
									,COA.[Type]
									,COA.bitTotal
								ORDER BY
									COA.Struc;',@Update6,';',@Update8,'; SELECT * FROM #tempFinalDataQuarter1; DROP TABLE #tempFinalDataQuarter; DROP TABLE #tempFinalDataQuarter1;')

		PRINT CAST(@sql AS NTEXT)

		EXECUTE (@sql)

		DROP TABLE #TempYearMonthQuarter
		DROP TABLE #tempDirectReportQuarter
	END
	ELSE 
	BEGIN
		SELECT 
			COA.ParentId
			,COA.vcCompundParentKey
			,COA.numAccountTypeID
			,COA.vcAccountType
			,COA.[LEVEL]
			,COA.vcAccountCode
			,COA.numAccountId
			,COA.Struc
			,COA.[Type]
			,COA.bitTotal
			,0 Total
		INTO 
			#tempDirectReportMain 
		FROM 
			 @TEMP COA

		UPDATE
			T
		SET
			T.Total = ISNULL(TCurrent.monAmount,0) - ISNULL(TOpening.monAmount,0)
		FROM
			#tempDirectReportMain  T
		OUTER APPLY
		(
			SELECT
				ISNULL(SUM(VF.Credit),0) - ISNULL(SUM(VF.debit),0) monAmount
			FROM
				#View_Journal VF 
			WHERE  
				VF.numAccountId = T.numAccountId
				AND VF.datEntry_Date < @dtFromDate
		) TOpening
		OUTER APPLY
		(
			SELECT
				ISNULL(SUM(VT.Credit),0) - ISNULL(SUM(VT.debit),0) monAmount
			FROM
				#View_Journal VT 
			WHERE  
				VT.numAccountId = T.numAccountId
				AND VT.datEntry_Date <= @dtToDate
		) TCurrent
		WHERE 
			T.[numAccountId] IS NOT NULL

		UPDATE
			#tempDirectReportMain
		SET
			Total = ISNULL((SELECT 
								ISNULL(SUM(VJ.Credit),0) - ISNULL(SUM(VJ.Debit),0) 
							FROM 
								#View_Journal VJ
							WHERE 
								((VJ.vcAccountCode  like '0103%' OR VJ.vcAccountCode LIKE '0104%' OR VJ.vcAccountCode LIKE '0106%') OR VJ.numaccountid = @PLCHARTID) 
								AND VJ.datentry_date BETWEEN @dtFromDate AND @dtToDate),0) + ISNULL((SELECT 
																									ISNULL(SUM(VJ.Credit),0) - ISNULL(SUM(VJ.Debit),0) 
																								FROM 
																									#View_Journal VJ
																								WHERE 
																									((VJ.vcAccountCode  like '0103%' OR VJ.vcAccountCode LIKE '0104%' OR VJ.vcAccountCode LIKE '0106%') OR VJ.numaccountid = @PLCHARTID) 
																									AND VJ.datentry_date <  @dtFromDate),0)
		WHERE
			numAccountTypeID=-2

		SELECT 
			ParentId
			,vcCompundParentKey
			,numAccountTypeID
			,vcAccountType
			,[LEVEL]
			,vcAccountCode
			,numAccountId
			,('#' + Struc + '#') AS Struc
			,[Type]
			,bitTotal
			,Total
		INTO 
			#tempFinalDataMain
		FROM
		(
			SELECT 
				COA.ParentId
				,COA.vcCompundParentKey
				,COA.numAccountTypeID
				,COA.vcAccountType
				,COA.[LEVEL]
				,COA.vcAccountCode
				,COA.numAccountId
				,COA.Struc
				,COA.[Type]
				,COA.bitTotal
				,SUM(ISNULL(V.Total,0)) AS Total
			FROM 
				@TEMP COA 
			LEFT JOIN 
				#tempDirectReportMain V
			ON  
				V.Struc like REPLACE(COA.Struc,'#Total','') + '%'
			GROUP BY 
				COA.ParentId
				,COA.vcCompundParentKey
				,COA.numAccountTypeID
				,COA.vcAccountType
				,COA.LEVEL
				,COA.vcAccountCode
				,COA.numAccountId
				,COA.Struc
				,COA.[Type]
				,COA.bitTotal			
		) AS t
		ORDER BY 
			Struc

		UPDATE 
			#tempFinalDataMain
		SET
			Total = ISNULL((SELECT SUM(Total) FROM #tempFinalDataMain TInner WHERE TInner.numAccountTypeID IN (-1,-4,-5) AND bitTotal=1),0)
		WHERE
			numAccountTypeID=-6

		UPDATE 
			#tempFinalDataMain
		SET
			Total = ISNULL((SELECT 
								ISNULL(SUM(credit),0) - ISNULL(SUM(debit),0)
							FROM 
								#View_Journal vj 
							WHERE 
								vcAccountCode LIKE '01010101%' 
								AND datentry_date < @dtFromDate),0)
		WHERE
			numAccountTypeID=-7

		UPDATE 
			#tempFinalDataMain
		SET
			Total =  ISNULL((SELECT SUM(Total) FROM #tempFinalDataMain TInner WHERE TInner.numAccountTypeID IN (-6,-7)),0)
		WHERE
			numAccountTypeID=-8


		SELECT * FROM #tempFinalDataMain

		DROP TABLE #tempDirectReportMain
		DROP TABLE #tempFinalDataMain
	END

	DROP TABLE #View_Journal
	
END