GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO                 
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype = 'p' AND NAME ='USP_UpdatePOFulfillmentSettings')
DROP PROCEDURE USP_UpdatePOFulfillmentSettings
GO
CREATE PROCEDURE [dbo].[USP_UpdatePOFulfillmentSettings]
	@numDomainID AS NUMERIC(18) = 0, 	
	@numPartialReceiveOrderStatus AS NUMERIC(18) = NULL, 
	@numFullReceiveOrderStatus AS NUMERIC(18) = NULL,
	@bitPartialQtyOrderStatus AS BIT,
	@bitAllQtyOrderStatus AS BIT,
	--@bitBizDoc AS BIT,
	@bitClosePO AS BIT,
	@numUserId AS NUMERIC(18) = 0,
	@numBizDocID AS NUMERIC(18) = 0 
AS                                      
BEGIN TRY
	BEGIN TRANSACTION   
	                                 
	--IF @numPOFulfillmentID > 0
	IF EXISTS (SELECT numPOFulfillmentID FROM PurchaseOrderFulfillmentSettings WHERE numDomainID=@numDomainID)
	BEGIN
		UPDATE 
			PurchaseOrderFulfillmentSettings
		SET 		
			numPartialReceiveOrderStatus = @numPartialReceiveOrderStatus,
			numFullReceiveOrderStatus = @numFullReceiveOrderStatus,
			bitPartialQtyOrderStatus = @bitPartialQtyOrderStatus,
			bitAllQtyOrderStatus = @bitAllQtyOrderStatus,
			--bitBizDoc = @bitBizDoc,
			numBizDocID = @numBizDocID,
			bitClosePO = @bitClosePO,
			numModifiedBy = @numUserId,
			bintModifiedDate = GETDATE()
		WHERE 
			numDomainId = @numDomainID
			--AND numPOFulfillmentID = @numPOFulfillmentID
	END
	ELSE
	BEGIN
		INSERT INTO PurchaseOrderFulfillmentSettings
		(
			numDomainID
			,numPartialReceiveOrderStatus
			,numFullReceiveOrderStatus
			,bitPartialQtyOrderStatus
			,bitAllQtyOrderStatus
			--,bitBizDoc
			,numBizDocID
			,bitClosePO
			,numCreatedBY
			,bintCreatedDate
		)
		VALUES
		(
			@numDomainID
			,@numPartialReceiveOrderStatus
			,@numFullReceiveOrderStatus
			,@bitPartialQtyOrderStatus
			,@bitAllQtyOrderStatus
			--,@bitBizDoc
			,@numBizDocID
			,@bitClosePO
			,@numUserId
			,GETDATE()
		)
	END

	COMMIT
END TRY
BEGIN CATCH
	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;

	SELECT 
		ERROR_MESSAGE(),
		ERROR_NUMBER(),
		ERROR_SEVERITY(),
		ERROR_STATE(),
		ERROR_LINE(),
		ISNULL(ERROR_PROCEDURE(), '-');
END CATCH