SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ManageCommissionRuleItems')
DROP PROCEDURE USP_ManageCommissionRuleItems
GO
CREATE PROCEDURE [dbo].[USP_ManageCommissionRuleItems]    
@numComRuleID as numeric(9)=0,    
@tintAppRuleType as tinyint,  
@numValue as numeric(9)=0,  
@numComRuleItemID as numeric(9)=0,  
@byteMode as TINYINT,
@numProfile AS NUMERIC(18,0)
as    
BEGIN TRY
BEGIN TRANSACTION
	IF @byteMode=0  
	BEGIN
		IF @numValue<>0
		BEGIN  
			DECLARE @numDomainID as numeric(9)
			DECLARE @tintAssignTo AS TINYINT
			DECLARE @tintComAppliesTo TINYINT
			DECLARE @tintComOrgType TINYINT

		
			SELECT 
				@numDomainID=numDomainID,
				@tintAssignTo=tintAssignTo,
				@tintComAppliesTo = ISNULL(tintComAppliesTo,0),
				@tintComOrgType = ISNULL(tintComOrgType,0)
			FROM 
				[CommissionRules] 
			WHERE 
				numComRuleID = @numComRuleID

			DECLARE @bitItemDuplicate AS BIT = 0
			DECLARE @bitOrgDuplicate AS BIT = 0
			DECLARE @bitContactDuplicate AS BIT = 0

			/*Duplicate Rule values checking */
			-- CHECK IF OTHER COMMISSION RULE IS EXISTS IN ACCOUNT WIRH SAME OPTION IN SETP 2,3,4 and 5
			IF (SELECT COUNT(*) FROM CommissionRules WHERE numDomainID=@numDomainID AND tintComAppliesTo=@tintComAppliesTo AND tintComOrgType=@tintComOrgType AND tintAssignTo = @tintAssignTo AND numComRuleID <> @numComRuleID) > 0
			BEGIN
				-- ITEM
				IF @tintComAppliesTo = 1 OR @tintComAppliesTo = 2
				BEGIN
					-- ITEM ID OR ITEM CLASSIFICATION
					IF (SELECT COUNT(*) FROM CommissionRuleItems WHERE numValue = @numValue AND numComRuleID IN (SELECT ISNULL(numComRuleID,0) FROM CommissionRules WHERE numDomainID=@numDomainID AND numComRuleID <> @numComRuleID)) > 0
					BEGIN
						SET @bitItemDuplicate = 1
					END
				END
				ELSE IF @tintComAppliesTo = 3
				BEGIN
					-- ALL ITEMS
					SET @bitItemDuplicate = 1
				END

				--ORGANIZATION
				IF @tintComOrgType = 1
				BEGIN
					-- ORGANIZATION ID
					IF (SELECT COUNT(*) FROM CommissionRuleOrganization WHERE numValue = @numValue AND numComRuleID IN (SELECT ISNULL(numComRuleID,0) FROM CommissionRules WHERE numDomainID=@numDomainID AND numComRuleID <> @numComRuleID)) > 0
					BEGIN
						SET @bitOrgDuplicate = 1
					END
				END
				ELSE IF @tintComOrgType = 2
				BEGIN
					-- ORGANIZATION RELATIONSHIP OR PROFILE
					IF (SELECT COUNT(*) FROM CommissionRuleOrganization WHERE numValue=@numValue AND numProfile = @numProfile AND numComRuleID IN (SELECT ISNULL(numComRuleID,0) FROM CommissionRules WHERE numDomainID=@numDomainID AND numComRuleID <> @numComRuleID)) > 0
					BEGIN
						SET @bitOrgDuplicate = 1
					END
				END
				ELSE IF  @tintComOrgType = 3
				BEGIN
					-- ALL ORGANIZATIONS
					SET @bitOrgDuplicate = 1
				END

				--CONTACT
				IF (
					SELECT 
						COUNT(*) 
					FROM 
						CommissionRuleContacts 
					WHERE  
						numValue IN (SELECT ISNULL(numValue,0) FROM CommissionRuleContacts WHERE numComRuleID=@numComRuleID) 
						AND numComRuleID IN (SELECT ISNULL(numComRuleID,0) FROM CommissionRules WHERE numDomainID=@numDomainID AND numComRuleID <> @numComRuleID)) > 0
				BEGIN
					SET @bitContactDuplicate = 1
				END
			END

			IF (ISNULL(@bitItemDuplicate,0) = 1 AND ISNULL(@bitOrgDuplicate,0) = 1 AND ISNULL(@bitContactDuplicate,0) = 1)
			BEGIN
				RAISERROR ( 'DUPLICATE',16, 1 )
				RETURN ;
			END
				
			IF NOT EXISTS(SELECT * FROM CommissionRuleItems WHERE numComRuleID=@numComRuleID and numValue=@numValue and tintType=@tintAppRuleType)
			BEGIN
				IF @tintAppRuleType > 2
				BEGIN
					IF @tintAppRuleType=3 
					BEGIN
						DELETE FROM CommissionRuleOrganization WHERE numComRuleID=@numComRuleID AND tintType = 4
						UPDATE CommissionRules SET tintComOrgType = 1 WHERE [numComRuleID]=@numComRuleID
						INSERT INTO CommissionRuleOrganization (numComRuleID,numValue,numProfile,tintType)  values(@numComRuleID,@numValue,0,@tintAppRuleType)
					END
					ELSE IF @tintAppRuleType=4 
					BEGIN
						DELETE FROM CommissionRuleOrganization WHERE numComRuleID=@numComRuleID AND tintType = 3
						UPDATE CommissionRules SET tintComOrgType = 2 WHERE [numComRuleID]=@numComRuleID
						INSERT INTO CommissionRuleOrganization (numComRuleID,numValue,numProfile,tintType)  values(@numComRuleID,@numValue,@numProfile,@tintAppRuleType)
					END
				END
				ELSE
				BEGIN
					IF @tintAppRuleType=1 
					BEGIN
						delete from CommissionRuleItems where tintType = 2 and numComRuleID=@numComRuleID
						UPDATE [CommissionRules] SET [tintComAppliesTo] = 1 WHERE [numComRuleID]=@numComRuleID
					END
					ELSE IF @tintAppRuleType=2 
						BEGIN
							delete from CommissionRuleItems where tintType = 1 and numComRuleID=@numComRuleID
							UPDATE [CommissionRules] SET [tintComAppliesTo] = 2 WHERE [numComRuleID]=@numComRuleID
						END
			
					INSERT INTO [CommissionRuleItems] (numComRuleID,numValue,tintType)	VALUES (@numComRuleID,@numValue,@tintAppRuleType)
				END
			END
		END
	END  
	ELSE IF @byteMode=1  
	BEGIN  
		IF (@tintAppRuleType=1 OR @tintAppRuleType=2)
			DELETE FROM [CommissionRuleItems] WHERE numComRuleItemID = @numComRuleItemID
		ELSE 
			DELETE FROM [CommissionRuleOrganization] WHERE numComRuleOrgID = @numComRuleItemID
	END
COMMIT
END TRY
BEGIN CATCH
	DECLARE @ErrorMessage NVARCHAR(4000)
	DECLARE @ErrorNumber INT
	DECLARE @ErrorSeverity INT
	DECLARE @ErrorState INT
	DECLARE @ErrorLine INT
	DECLARE @ErrorProcedure NVARCHAR(200)

	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;

	SELECT 
		@ErrorMessage = ERROR_MESSAGE(),
		@ErrorNumber = ERROR_NUMBER(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE(),
		@ErrorLine = ERROR_LINE(),
		@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

	RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);
END CATCH
GO
