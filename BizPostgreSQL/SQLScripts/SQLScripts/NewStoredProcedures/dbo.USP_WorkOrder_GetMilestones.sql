GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_WorkOrder_GetMilestones')
DROP PROCEDURE dbo.USP_WorkOrder_GetMilestones
GO
CREATE PROCEDURE [dbo].[USP_WorkOrder_GetMilestones]
(
	@numDomainID NUMERIC(18,0)
	,@numWOID NUMERIC(18,0)
)
AS 
BEGIN
	SELECT 
		vcMileStoneName
		,CONCAT(StagePercentageDetails.vcMileStoneName,' (',ISNULL(StagePercentageMaster.numStagePercentage,0),'% of total)') vcMileStone
		,dbo.GetTotalProgress(@numDomainID,@numWOID,1,2,vcMileStoneName,0) numTotalProgress
	FROM 
		StagePercentageDetails
	LEFT JOIN
		StagePercentageMaster
	ON
		StagePercentageDetails.numParentStageID = StagePercentageMaster.numStagePercentageId
	WHERE
		StagePercentageDetails.numDomainID = @numDomainID
		AND StagePercentageDetails.numWorkOrderId = @numWOID
	GROUP BY
		vcMileStoneName
		,StagePercentageMaster.numStagePercentage
END
GO