GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ElasticSearchModifiedRecords_Get')
DROP PROCEDURE dbo.USP_ElasticSearchModifiedRecords_Get
GO
CREATE PROCEDURE [dbo].[USP_ElasticSearchModifiedRecords_Get]

AS 
BEGIN
	DELETE FROM ElasticSearchModifiedRecords WHERE numDomainID NOT IN (SELECT numDomainId FROM Domain WHERE bitElasticSearch=1)

	DECLARE @TEMP TABLE
	(
		ID NUMERIC(18,0)
		,numDomainID NUMERIC(18,0)
		,vcModule VARCHAR(15)
		,numRecordID NUMERIC(18,0)
		,vcAction VARCHAR(10)
	)
	INSERT INTO @TEMP (ID,numDomainID,vcModule,numRecordID,vcAction) SELECT TOP 8000 ID,numDomainID,vcModule,numRecordID,vcAction FROM ElasticSearchModifiedRecords ORDER BY ID
	DELETE FROM ElasticSearchModifiedRecords WHERE ID IN (SELECT ID FROM @TEMP)

	SELECT * FROM (SELECT MIN(ID) AS ID,T1.numDomainID,vcModule,numRecordID,vcAction FROM @TEMP T1 INNER JOIN Domain ON T1.numDomainID=Domain.numDomainId WHERE ISNULL(Domain.bitElasticSearch,0)=1 AND vcAction='Insert' AND T1.numDomainID > 0 GROUP BY T1.numDomainID,vcModule,numRecordID,vcAction) TEMP ORDER BY ID
	SELECT * FROM (SELECT MIN(ID) AS ID,T1.numDomainID,vcModule,numRecordID,vcAction FROM @TEMP T1 INNER JOIN Domain ON T1.numDomainID=Domain.numDomainId WHERE ISNULL(Domain.bitElasticSearch,0)=1 AND vcAction='Update' AND T1.numDomainID > 0 GROUP BY T1.numDomainID,vcModule,numRecordID,vcAction) TEMP ORDER BY ID
	SELECT * FROM (SELECT MIN(ID) AS ID,T1.numDomainID,vcModule,numRecordID,vcAction FROM @TEMP T1 INNER JOIN Domain ON T1.numDomainID=Domain.numDomainId WHERE ISNULL(Domain.bitElasticSearch,0)=1 AND vcAction='Delete' AND T1.numDomainID > 0 GROUP BY T1.numDomainID,vcModule,numRecordID,vcAction) TEMP ORDER BY ID
END