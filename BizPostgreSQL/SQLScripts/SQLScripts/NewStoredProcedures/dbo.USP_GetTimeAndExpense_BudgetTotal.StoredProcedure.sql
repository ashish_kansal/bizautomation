
GO
/****** Object:  StoredProcedure [dbo].[USP_GetTimeAndExpense_BudgetTotal]    Script Date: 10/28/2010 15:51:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetTimeAndExpense_BudgetTotal')
DROP PROCEDURE USP_GetTimeAndExpense_BudgetTotal
GO
CREATE PROCEDURE [dbo].[USP_GetTimeAndExpense_BudgetTotal]
    @numDomainID AS NUMERIC(9) = 0,
    @numProId AS NUMERIC(9) = 0,
    @numStageId AS NUMERIC(9) = 0
AS 
   
          
DECLARE @TotalTime as DECIMAL(10, 2),@TotalExpense as DECIMAL(10, 2);
SET @TotalTime=0;SET @TotalExpense=0

DECLARE @TimeBudget as DECIMAL(10, 2),@ExpenseBudget as DECIMAL(10, 2);
SET @TimeBudget=0;SET @ExpenseBudget=0

DECLARE @bitTimeBudget as bit,@bitExpenseBudget as bit


SELECT @TotalTime=isnull(CONVERT(DECIMAL(10, 2), CAST( SUM(CAST(CAST(DATEDIFF(minute, dtFromDate, dtToDate) AS DECIMAL(10,2))/60 AS DECIMAL(10,2)) ) AS DECIMAL(10,2))),0)
from  timeandexpense where numProID=@numProId and numStageID=@numStageId AND numCategory = 1 and tintTEType = 2 
AND numType= 1 GROUP BY numType

SELECT @TotalExpense=isnull([dbo].[fn_GetExpenseDtlsbyProStgID](@numProId,@numStageId,1),0)

select @TimeBudget=isnull(monTimeBudget,0),@ExpenseBudget=isnull(monExpenseBudget,0),@bitTimeBudget=isnull(bitTimeBudget,0),@bitExpenseBudget=isnull(bitExpenseBudget,0) from StagePercentageDetails where numProjectID=@numProId and numStageDetailsId=@numStageId

if @bitTimeBudget=1
   SET @TotalTime=@TimeBudget-@TotalTime

if @bitExpenseBudget=1
   SET @TotalExpense=@ExpenseBudget-@TotalExpense


select ISNULL(@TotalTime,0) TotalTime,ISNULL(@TotalExpense,0) TotalExpense,ISNULL(@bitTimeBudget,0) bitTimeBudget,ISNULL(@bitExpenseBudget,0) bitExpenseBudget

 
