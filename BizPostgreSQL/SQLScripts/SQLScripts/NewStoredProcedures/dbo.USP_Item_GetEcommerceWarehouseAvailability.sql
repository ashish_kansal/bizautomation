GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
            
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_Item_GetEcommerceWarehouseAvailability')
DROP PROCEDURE USP_Item_GetEcommerceWarehouseAvailability
GO
CREATE PROCEDURE [dbo].[USP_Item_GetEcommerceWarehouseAvailability]    
(
	@numDomainID NUMERIC(18,0)
	,@numItemCode NUMERIC(18,0)
	,@tintWarehouseAvailability TINYINT
	,@bitDisplayQtyAvailable BIT
)
AS
BEGIN
	IF @tintWarehouseAvailability = 3
	BEGIN
		SELECT 
			FORMAT(ISNULL(SUM(numOnHand),0), '#,##0')
		FROM
			WarehouseItems
		INNER JOIN
			Warehouses
		ON
			WarehouseItems.numWareHouseID = Warehouses.numWareHouseID
		WHERE
			WarehouseItems.numDomainID=@numDomainID
			AND WarehouseItems.numItemID=@numItemCode
	END
	ELSE
	BEGIN
		SELECT STUFF((SELECT
					CONCAT(', ',vcWareHouse,(CASE WHEN @bitDisplayQtyAvailable=1 THEN CONCAT('(',numOnHand,')') ELSE '' END))
				FROM
				(
					SELECT 
						Warehouses.numWareHouseID
						,Warehouses.vcWareHouse
						,SUM(numOnHand) numOnHand
					FROM
						WarehouseItems
					INNER JOIN
						Warehouses
					ON
						WarehouseItems.numWareHouseID = Warehouses.numWareHouseID
					WHERE
						WarehouseItems.numDomainID=@numDomainID
						AND WarehouseItems.numItemID=@numItemCode
					GROUP BY
						Warehouses.numWareHouseID
						,Warehouses.vcWareHouse
				) TEMP
				WHERE
					1 = (CASE WHEN @tintWarehouseAvailability=2 THEN (CASE WHEN Temp.numOnHand > 0 THEN 1 ELSE 0 END) ELSE 1 END)
				ORDER BY
					vcWareHouse
				FOR XML PATH('')),1,1,'')
	END	
END