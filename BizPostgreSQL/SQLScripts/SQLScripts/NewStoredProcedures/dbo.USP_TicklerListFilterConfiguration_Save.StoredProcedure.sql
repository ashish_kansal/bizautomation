SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_TicklerListFilterConfiguration_Save')
DROP PROCEDURE dbo.USP_TicklerListFilterConfiguration_Save
GO
CREATE PROCEDURE [dbo].[USP_TicklerListFilterConfiguration_Save]
    (
	  @numDomainID NUMERIC(18,0),
      @numUserCntID NUMERIC(18,0),
	  @vcSelectedEmployee VARCHAR(500),
	  @numCriteria INT,
	  @vcActionTypes VARCHAR(500),
	  @vcSelectedEmployeeOpp VARCHAR(500),
	  @vcSelectedEmployeeCases VARCHAR(500),
	  @vcAssignedStages VARCHAR(500),
	  @numCriteriaCases INT
    )
AS 
BEGIN

	IF EXISTS (SELECT ID FROM TicklerListFilterConfiguration WHERE numDomainID = @numDomainID AND numUserCntID = @numUserCntID)
	BEGIN
		UPDATE
			TicklerListFilterConfiguration
		SET
			vcSelectedEmployee=@vcSelectedEmployee,
			numCriteria=@numCriteria,
			vcActionTypes=@vcActionTypes,
			vcSelectedEmployeeOpp=@vcSelectedEmployeeOpp,
			vcSelectedEmployeeCases=@vcSelectedEmployeeCases,
			vcAssignedStages=@vcAssignedStages,
			numCriteriaCases=@numCriteriaCases
		WHERE
			numDomainID = @numDomainID AND 
			numUserCntID = @numUserCntID
	END
	ELSE
	BEGIN
		INSERT INTO TicklerListFilterConfiguration
			(
				numDomainID,
				numUserCntID,
				vcSelectedEmployee,
				numCriteria,
				vcActionTypes,
				vcSelectedEmployeeOpp,
				vcSelectedEmployeeCases,
				vcAssignedStages,
				numCriteriaCases
			)
		VALUES
			(
				@numDomainID,
				@numUserCntID,
				@vcSelectedEmployee,
				@numCriteria,
				@vcActionTypes,
				@vcSelectedEmployeeOpp,
				@vcSelectedEmployeeCases,
				@vcAssignedStages,
				@numCriteriaCases
			)
	END

END
