GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetUOM')
DROP PROCEDURE USP_GetUOM
GO
CREATE PROCEDURE [dbo].[USP_GetUOM]                                                                          
 @numDomainID as numeric(9),
 @tintUnitType TINYINT               
AS       
   
--   IF((SELECT COUNT(*) FROM UOM WHERE numDomainID=@numDomainID)=0)
--		BEGIN
--			INSERT INTO UOM (numDomainId,vcUnitName,tintUnitType,bitEnabled)
--			SELECT @numDomainID, vcUnitName,tintUnitType,bitEnabled FROM UOM WHERE numDomainId=0
--		END
      
   SELECT numUOMId,vcUnitName,tintUnitType,CASE tintUnitType WHEN 1 
   THEN 'English' WHEN 2 THEN 'Matrix' ELSE '' END AS vcUnitType,
	CASE isnull(bitEnabled,0) WHEN 1 THEN 'Yes' ELSE 'No' END AS vcEnable  FROM UOM WHERE numDomainID=@numDomainID
   AND tintUnitType = @tintUnitType
