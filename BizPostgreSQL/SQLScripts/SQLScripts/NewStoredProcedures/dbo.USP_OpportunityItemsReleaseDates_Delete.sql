SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_OpportunityItemsReleaseDates_Delete')
DROP PROCEDURE USP_OpportunityItemsReleaseDates_Delete
GO
CREATE PROCEDURE [dbo].[USP_OpportunityItemsReleaseDates_Delete]
(
	@ID NUMERIC(18,0)
)
AS 
BEGIN
	DELETE FROM OpportunityItemsReleaseDates WHERE numID=@ID
END