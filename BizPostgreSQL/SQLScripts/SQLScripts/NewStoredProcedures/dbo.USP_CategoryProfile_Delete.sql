SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_CategoryProfile_Delete')
DROP PROCEDURE dbo.USP_CategoryProfile_Delete
GO
CREATE PROCEDURE [dbo].[USP_CategoryProfile_Delete]
(
	@ID NUMERIC(18,0),
	@numDomainID NUMERIC(18,0)
)
AS
BEGIN
	IF EXISTS (SELECT numItemID FROM ItemCategory WHERE numCategoryID IN (SELECT numCategoryID FROM Category WHERE numDomainID=@numDomainID AND numCategoryProfileID=@ID))
	BEGIN
		RAISERROR('ITEMS_EXITS',16,1)
		RETURN
	END
	ELSE 
	BEGIN
		DELETE FROM SiteCategories WHERE numCategoryID IN (SELECT ISNULL(numCategoryID,0) FROM Category WHERE numCategoryProfileID = @ID)
		DELETE FROM Category WHERE numCategoryProfileID = @ID
		DELETE FROM CategoryProfileSites WHERE numCategoryProfileID = @ID
		DELETE FROM CategoryProfile WHERE ID=@ID
	END
END