IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_AdditionalContactsInformation_CheckIfDuplicateEmail')
DROP PROCEDURE USP_AdditionalContactsInformation_CheckIfDuplicateEmail
GO

CREATE PROCEDURE USP_AdditionalContactsInformation_CheckIfDuplicateEmail
	@numDomainID NUMERIC(18,0)
	,@numContactID NUMERIC(18,0)
	,@vcEmail VARCHAR(300)
AS 
BEGIN
	IF LEN(ISNULL(@vcEmail,'')) > 0 AND EXISTS (SELECT TOP 1 
													numContactID
												FROM
													AdditionalContactsInformation 
												INNER JOIN
													DivisionMaster
												ON
													AdditionalContactsInformation.numDivisionID=DivisionMaster.numDivisionID
												INNER JOIN
													CompanyInfo
												ON
													DivisionMaster.numCompanyID = CompanyInfo.numCompanyID
												WHERE 
													AdditionalContactsInformation.numDomainID=@numDomainID 
													AND vcEmail=ISNULL(@vcEmail,'') 
													AND AdditionalContactsInformation.numContactID <> ISNULL(@numContactID,0))
	BEGIN
		SELECT 1
	END
	ELSE
	BEGIN
		SELECT 0
	END
END
GO