SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetPriceNamesTable')
DROP PROCEDURE dbo.USP_GetPriceNamesTable
GO
CREATE PROCEDURE [dbo].[USP_GetPriceNamesTable]
(
	@numDomainID NUMERIC(18,0)
)
AS
BEGIN
	SELECT  
		[pricingNamesID],
        [numDomainID],
        [tintPriceLevel],
 		ISNULL(vcPriceLevelName,'') vcPriceLevelName
    FROM 
		[PricingNamesTable]
	WHERE 
		numDomainID = @numDomainID
END