
GO
/****** Object:  StoredProcedure [dbo].[USP_GetSitePages]    Script Date: 08/08/2009 16:10:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetSitePages')
DROP PROCEDURE USP_GetSitePages
GO
CREATE PROCEDURE [dbo].[USP_GetSitePages]
          @numPageID   NUMERIC(9),
          @numSiteID   NUMERIC(9),
          @numDomainID NUMERIC(9)
AS
  SELECT numPageID,
		 numSiteID,
         ISNULL(SP.vcPageName,'') vcPageName,
         ISNULL(SP.vcPageURL,'') vcPageURL,
         ISNULL(SP.tintPageType,2) tintPageType,
         ISNULL(SP.vcPageTitle,'') vcPageTitle,
         [numTemplateID],
         (SELECT vcTemplateName FROM [SiteTemplates] st WHERE st.[numTemplateID] = SP.numTemplateID) vcTemplateName,
         --ISNULL(MT.vcMetaTag,'') vcMetaTag,
         ISNULL([vcMetaDescription],'') AS vcMetaDescription,
         ISNULL([vcMetaKeywords],'') AS vcMetaKeywords,
         ISNULL(MT.numMetaID,0) numMetaID,
         ISNULL((SELECT vcHostName FROM Sites WHERE [numSiteID] = @numSiteID),'') vcHostName,
		 CASE WHEN [bitIsActive] = 1 THEN 'Inactivate'
		      ELSE 'Activate'
		 END AS 'Status',
		 [bitIsActive],
		 ISNULL([bitIsMaintainScroll],0) AS bitIsMaintainScroll
  FROM   SitePages SP
         LEFT OUTER JOIN MetaTags MT
           ON MT.numReferenceID = SP.numPageID
  WHERE  (numPageID = @numPageID
           OR @numPageID = 0)
         AND numSiteID = @numSiteID
         AND numDomainID = @numDomainID
	ORDER BY
		vcPageName

  IF @numPageID > 0
    BEGIN
      --Select StyleSheets for Page
      SELECT S.[numCssID]
      FROM   [StyleSheetDetails] SD
             INNER JOIN [StyleSheets] S
               ON SD.[numCssID] = S.[numCssID]
      WHERE  [numPageID] = @numPageID
             AND [tintType] = 0
      --Select Javascripts for Page
      SELECT S.[numCssID]
      FROM   [StyleSheetDetails] SD
             INNER JOIN [StyleSheets] S
               ON SD.[numCssID] = S.[numCssID]
      WHERE  [numPageID] = @numPageID
             AND [tintType] = 1
    END
