--SELECT [numItemCode], [numAssetChartAcntId],[numCOGsChartAcntId],[numIncomeChartAcntId],[numDomainID] FROM [Item]
-- EXEC USP_ValidateItemAccounts 16,72
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ValidateItemAccounts')
DROP PROCEDURE USP_ValidateItemAccounts
GO
CREATE PROCEDURE  USP_ValidateItemAccounts
    @numItemCode AS NUMERIC(9),
    @numDomainID AS NUMERIC(9),
    @tintOppType AS TINYINT=0 -- 1 for sales, 2 for purchase
AS 
    BEGIN
        DECLARE @numCOGsChartAcntId NUMERIC(9)
        DECLARE @numAssetChartAcntId NUMERIC(9)
        DECLARE @numIncomeChartAcntId NUMERIC(9)
        DECLARE @ItemType CHAR(1)
		DECLARE @bitKitParent BIT
		DECLARE @bitExpenseNonInventoryItem BIT
		
		SELECT @bitExpenseNonInventoryItem=ISNULL(bitExpenseNonInventoryItem,0) FROM dbo.Domain WHERE numDomainId=@numDomainID
        SELECT  @numCOGsChartAcntId = [numCOGsChartAcntId],
                @numAssetChartAcntId = [numAssetChartAcntId],
                @numIncomeChartAcntId = [numIncomeChartAcntId],
                @ItemType = [charItemType],@bitKitParent=(CASE WHEN ISNULL(bitKitParent,0) = 1
                               AND ISNULL(bitAssembly,0) = 1 THEN 0
                          WHEN ISNULL(bitKitParent,0) = 1 THEN 1
                          ELSE 0 END)
        FROM    item
        WHERE   [numItemCode] = @numItemCode
                AND [numDomainID] = @numDomainID
       
        IF (@ItemType = 'P' OR @ItemType = 'A')
            BEGIN
                IF ( @numAssetChartAcntId > 0
                     AND @numCOGsChartAcntId > 0
                     AND @numIncomeChartAcntId > 0
                   ) 
                   BEGIN
					 
					 IF @bitKitParent = 1
						BEGIN
							exec USP_ValidateItemAccountsForEmbeddedKits @numItemCode
						END 
			 
						ELSE
						BEGIN
							SELECT  1
						END	
					END
                   
                ELSE 
                    SELECT  0 	
            END        
		
        IF (@ItemType = 'N' OR @ItemType = 'S')
            BEGIN
				IF @bitExpenseNonInventoryItem=1
				BEGIN
					IF @tintOppType=2 -- Validate Item Expense Account for Purchase Order Only
					BEGIN
						IF (@numCOGsChartAcntId > 0)
							BEGIN
								SELECT  1
								RETURN;
							END
						ELSE
							BEGIN
								SELECT  0
								RETURN;	
							END
					END
				END
					
            
                IF (@numIncomeChartAcntId > 0)
                    SELECT  1
                ELSE 
                    SELECT  0 	
            END        
		
			
    END