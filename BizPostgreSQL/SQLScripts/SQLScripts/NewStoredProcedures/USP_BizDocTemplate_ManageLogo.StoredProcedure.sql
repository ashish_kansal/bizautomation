
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_BizDocTemplate_ManageLogo' ) 
    DROP PROCEDURE USP_BizDocTemplate_ManageLogo
GO
-- =============================================  
-- Author:  <Author,,Sachin Sadhu>  
-- Create date: <Create Date,,23rdDec2013>  
-- Description: <Description,,To Save Logo in BizDocTemplate Table>  
-- =============================================  
CREATE PROCEDURE USP_BizDocTemplate_ManageLogo   
 -- Add the parameters for the stored procedure here  
    (
      @numBizDocTempID NUMERIC(18) ,
      @numDomainId AS NUMERIC(9) = 0 ,
      @vcImagePath AS VARCHAR(100) = '' ,
      @sintbyteMode AS SMALLINT = 0   
    )
AS 
    BEGIN  
 -- SET NOCOUNT ON added to prevent extra result sets from  
 -- interfering with SELECT statements.  
  
        SET NOCOUNT ON;  
    
        IF @sintbyteMode = 0 
            UPDATE  BizDocTemplate
            SET     vcBizDocImagePath = @vcImagePath
            WHERE   numDomainId = @numDomainId
                    AND numBizDocTempID = @numBizDocTempID  
       
        ELSE 
            IF @sintbyteMode = 1 
                UPDATE  BizDocTemplate
                SET     vcBizDocImagePath = @vcImagePath
                WHERE   numDomainId = @numDomainId
                        AND numBizDocTempID = @numBizDocTempID  
       
  
    
    END  

