GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_TransactionHistory_GetByOrder' ) 
    DROP PROCEDURE USP_TransactionHistory_GetByOrder
GO

CREATE PROCEDURE [dbo].[USP_TransactionHistory_GetByOrder]
    @numDomainID NUMERIC(18, 0),
    @numOppID NUMERIC(18, 0)
AS 
    SET NOCOUNT ON
    SET TRANSACTION ISOLATION LEVEL READ COMMITTED

	SELECT
		TH.[numTransHistoryID],
		TH.[numDivisionID],
		TH.[numContactID],
		TH.[numOppID],
		TH.[numOppBizDocsID],
		[vcTransactionID],
		[tintTransactionStatus],
		(CASE WHEN tintTransactionStatus = 1 THEN 'Authorized/Pending Capture' 
			 WHEN tintTransactionStatus = 2 THEN 'Captured'
			 WHEN tintTransactionStatus = 3 THEN 'Void'
			 WHEN tintTransactionStatus = 4 THEN 'Failed'
			 WHEN tintTransactionStatus = 5 THEN 'Credited'
			 ELSE 'Not Authorized'
		END) AS vcTransactionStatus,	 
		[vcPGResponse],
		[tintType],
		[monAuthorizedAmt],
		[monCapturedAmt],
		[monRefundAmt],
		[vcCardHolder],
		[vcCreditCardNo],
		isnull((SELECT vcData FROM dbo.ListDetails WHERE numListItemID = isnull([numCardType],0)),'-') as vcCardType,
		[vcCVV2],
		[tintValidMonth],
		[intValidYear],
		TH.dtCreatedDate,
		ISNULL(OBD.vcBizDocID, ISNULL(OM.vcPOppName,'')) AS [vcBizOrderID],
		vcSignatureFile,
		ISNULL(tintOppType,0) AS [tintOppType],
		ISNULL(vcResponseCode,'') vcResponseCode
		,ISNULL(DD.monAmountPaid,0) AS monAmountPaid
		,ISNULL(TH.intPaymentGateWay,0) intPaymentGateWay
 	FROM
		dbo.TransactionHistory TH
	INNER JOIN 
		dbo.OpportunityMaster OM 
	ON 
		TH.numoppID = OM.numOppID
	LEFT JOIN
		DepositMaster DM
	ON
		TH.numTransHistoryID = DM.numTransHistoryID
	LEFT JOIN
		DepositeDetails DD
	ON
		DM.numDepositId = DD.numDepositID
	LEFT JOIN 
		dbo.OpportunityBizDocs OBD 
	ON 
		OBD.numOppID=OM.numOppID 
		AND OBD.numOppBizDocsId = DD.numOppBizDocsID 
	WHERE
		TH.numDomainID=@numDomainID
		AND TH.numOppID=@numOppID
	
GO

