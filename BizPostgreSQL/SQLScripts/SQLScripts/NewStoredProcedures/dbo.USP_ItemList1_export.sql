GO
IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'USP_ItemList1_export') AND type in (N'P', N'PC'))
DROP PROCEDURE USP_ItemList1_export
GO
Create Procedure USP_ItemList1_export
 @numDomainID AS NUMERIC(9) = 0,
 @numWarehouseID AS NUMERIC(18,0)=0
as 
begin 

SELECT
	I.numItemCode as ItemId
	,I.vcItemName as Itemname
	,WareHouseItems.numWareHouseItemID
	,numOnHand AS OnHand
	,numAllocation AS OnAllocation
	,ISNULL(numOnHand,0) + ISNULL(numAllocation,0) AS TotalOnHandPlusAllocation
	,isnull(I.monAverageCost,0) as AverageCost
	,(ISNULL(numOnHand,0) + ISNULL(numAllocation,0)) * isnull(I.monAverageCost,0)  AS StockValue
	,numWareHouseID 
											
FROM
	WareHouseItems
	Inner join Item I on I.numItemCode = WareHouseItems.numItemID 
WHERE
	(WareHouseItems.numWareHouseID=@numWarehouseID OR @numWarehouseID=0)
	and  I.numDomainID = @numDomainID
	and ISNULL(I.IsArchieve,0) = 0 
	and (isnull(WarehouseItems.numOnHand,0) + isnull(WarehouseItems.numAllocation,0) >0) and (isnull(I.monAverageCost,0)>0)  
end 
GO