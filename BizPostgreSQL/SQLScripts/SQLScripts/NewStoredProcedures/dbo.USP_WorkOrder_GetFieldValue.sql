GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
                                    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_WorkOrder_GetFieldValue')
DROP PROCEDURE USP_WorkOrder_GetFieldValue
GO
CREATE PROCEDURE [dbo].[USP_WorkOrder_GetFieldValue]
(
	@numDomainID NUMERIC(18,0)
	,@numWOID NUMERIC(18,0)
	,@vcFieldName VARCHAR(200)
)
AS
BEGIN
	IF @vcFieldName = 'numQtyItemsReq'
	BEGIN
		SELECT ISNULL(numQtyItemsReq,0) FROM WorkOrder	WHERE numDomainID = @numDomainID AND numWOId = @numWOID
	END
	ELSE IF @vcFieldName = 'numItemCode'
	BEGIN
		SELECT ISNULL(numItemCode,0) FROM WorkOrder	WHERE numDomainID = @numDomainID AND numWOId = @numWOID
	END
	ELSE IF @vcFieldName = 'numQuantityBuilt'
	BEGIN
		IF EXISTS (SELECT numWOId FROM WorkOrder WHERE numDomainID = @numDomainID AND numWOId = @numWOID AND (ISNULL(numQtyItemsReq,0) - ISNULL(numQtyBuilt,0)) > 0)
		BEGIN
			SELECT ISNULL(numQtyItemsReq,0) - ISNULL(numQtyBuilt,0) FROM WorkOrder WHERE numDomainID = @numDomainID AND numWOId = @numWOID
		END
		ELSE
		BEGIN
			SELECT 0
		END
	END
	ELSE
	BEGIN
		RAISERROR('FIELD_NOT_FOUND',16,1)
	END
END
GO