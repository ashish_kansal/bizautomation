GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetSimilarItemWithPreCheckout')
-- exec USP_GetSimilarItemWithPreCheckout 72,4648,0
DROP PROCEDURE USP_GetSimilarItemWithPreCheckout
GO
CREATE PROCEDURE USP_GetSimilarItemWithPreCheckout 
	@numDomainID NUMERIC(9),
	@numParentItemCode NUMERIC(9),
	@numWarehouseID NUMERIC(18,0)
AS 
BEGIN
	SELECT 
		I.numItemCode,
		I.vcItemName AS vcItemName,
		(SELECT TOP 1 vcPathForTImage FROM dbo.ItemImages II WHERE II.numItemCode = I.numItemCode AND II.numDomainId= I.numDomainId AND II.bitDefault=1) As vcPathForTImage,
		I.txtItemDesc AS txtItemDesc, 
		dbo.fn_GetUOMName(I.numSaleUnit) AS vcUOMName,
		ISNULL(WarehouseItems.numWareHouseItemID,0) AS numWareHouseItemID
		,CASE WHEN charItemType='P' THEN ISNULL((SELECT TOP 1 monWListPrice FROM WarehouseItems WHERE numDomainID=@numDomainID AND numItemID=SI.numItemCode AND ISNULL(monWListPrice,0) > 0),0) ELSE monListPrice END monListPrice   
		--,(CASE WHEN I.charItemType='P' THEN ISNULL(monWListPrice,0) ELSE I.monListPrice END) monListPrice
		,ISNULL(vcRelationship,'') vcRelationship
		,ISNULL(bitPreUpSell,0) [bitPreUpSell]
		,ISNULL(bitPostUpSell,0) [bitPostUpSell]
		,ISNULL(bitRequired,0) [bitRequired]
		,ISNULL(vcUpSellDesc,'') [vcUpSellDesc]
		,C.vcCategoryName
	FROM 
		SimilarItems SI 
	INNER JOIN Item I ON I.numItemCode = SI.numItemCode
	LEFT JOIN ItemCategory IC ON IC.numItemID = I.numItemCode
	LEFT JOIN Category C ON C.numCategoryID = IC.numCategoryID 
	OUTER APPLY
	(
		SELECT TOP 1
			ISNULL(numWareHouseItemID,0) AS numWareHouseItemID,
			monWListPrice
		FROM
			WarehouseItems
		WHERE
			WarehouseItems.numDomainID=@numDomainID
			AND WarehouseItems.numWarehouseID=@numWarehouseID
			AND WarehouseItems.numItemID = I.numItemCode
	) AS WarehouseItems
	WHERE SI.numDomainId = @numDomainId 
		AND SI.numParentItemCode = @numParentItemCode
		AND SI.bitPreUpSell = 1
END

