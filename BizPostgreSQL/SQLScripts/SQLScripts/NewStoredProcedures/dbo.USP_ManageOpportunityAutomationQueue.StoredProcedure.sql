GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_ManageOpportunityAutomationQueue' ) 
    DROP PROCEDURE USP_ManageOpportunityAutomationQueue
GO
CREATE PROCEDURE USP_ManageOpportunityAutomationQueue
    @numOppQueueID numeric(18, 0)=0,
    @numDomainID numeric(18, 0)=0,
    @numOppId numeric(18, 0)=0,
    @numOppBizDocsId numeric(18, 0)=0,
    @numBizDocStatus numeric(18, 0)=0,
    @numOrderStatus NUMERIC(18,0)=0,
    @numUserCntID numeric(18, 0)=0,
    @tintProcessStatus TINYINT=1,
    @vcDescription varchar(1000)='',
    @numRuleID numeric(18, 0)=0,
    @bitSuccess BIT=0,
    @tintMode TINYINT,
    @numShippingReportID NUMERIC(18,0)=0
AS 
BEGIN
    IF @tintMode = 2
        BEGIN
			  SELECT TOP 25 OAQ.*,OBD.numBizDocId,OM.numDivisionId,OM.tintOppStatus,OM.tintshipped,OM.tintOppType FROM OpportunityAutomationQueue OAQ JOIN dbo.OpportunityMaster OM ON OAQ.numOppId=OM.numOppId
				 LEFT JOIN dbo.OpportunityBizDocs OBD ON OAQ.numOppId=OBD.numOppId AND OAQ.numOppBizDocsId=OBD.numOppBizDocsId
				 WHERE tintProcessStatus IN (1,2) ORDER BY numOppQueueID
			--SELECT TOP 1 @numOppQueueID=numOppQueueID FROM OpportunityAutomationQueue WHERE tintProcessStatus IN (1,2) ORDER BY numOppQueueID
			--UPDATE OpportunityAutomationQueue SET tintProcessStatus=2 WHERE numOppQueueID=@numOppQueueID
			
			--SELECT * FROM OpportunityAutomationQueue WHERE numOppQueueID=@numOppQueueID
        END
    ELSE IF @tintMode = 3
        BEGIN
			SELECT @numOrderStatus=ISNULL(numStatus,0) FROM OpportunityMaster WHERE numOppId=@numOppId
			
			IF ISNULL(@numOppBizDocsId,0)>0
			BEGIN
				SELECT @numBizDocStatus=ISNULL(numBizDocStatus,0) FROM OpportunityBizDocs WHERE numOppId=@numOppId AND numOppBizDocsId=@numOppBizDocsId
			END
			
			INSERT INTO dbo.OpportunityAutomationQueueExecution (
						numOppQueueID,bitSuccess,vcDescription,dtExecutionDate,numRuleID,numOppId,numOppBizDocsId,numOrderStatus,numBizDocStatus
					) VALUES ( @numOppQueueID,@bitSuccess,@vcDescription,GETUTCDATE(),@numRuleID,@numOppId,@numOppBizDocsId,@numOrderStatus,@numBizDocStatus)
        END    
	ELSE IF @tintMode = 1 
        BEGIN
			IF @numOppQueueID>0
			BEGIN
				UPDATE [dbo].[OpportunityAutomationQueue] SET [tintProcessStatus] = @tintProcessStatus WHERE  [numOppQueueID] = @numOppQueueID AND [numDomainID] = @numDomainID
			END
			ELSE
			BEGIN
				IF EXISTS (SELECT * FROM OpportunityAutomationQueue WHERE numDomainID=@numDomainID AND numOppId=@numOppId AND numOppBizDocsId=@numOppBizDocsId AND tintProcessStatus=1 AND ISNULL(numRuleID,0)=@numRuleID)
				BEGIN
					UPDATE OpportunityAutomationQueue SET tintProcessStatus=5 WHERE numDomainID=@numDomainID AND numOppId=@numOppId AND numOppBizDocsId=@numOppBizDocsId AND tintProcessStatus=1 AND ISNULL(numRuleID,0)=@numRuleID
				END 
				
				IF @numBizDocStatus>0 OR @numRuleID>0 OR @numOrderStatus>0
				BEGIN
					INSERT INTO [dbo].[OpportunityAutomationQueue] ([numDomainID], [numOppId], [numOppBizDocsId], [numBizDocStatus], [numCreatedBy], [dtCreatedDate], [tintProcessStatus],numRuleID,numOrderStatus,numShippingReportID)
						SELECT @numDomainID, @numOppId, @numOppBizDocsId, @numBizDocStatus, @numUserCntID, GETUTCDATE(), @tintProcessStatus,@numRuleID,@numOrderStatus,@numShippingReportID	
				END
			END
        END	
 END



