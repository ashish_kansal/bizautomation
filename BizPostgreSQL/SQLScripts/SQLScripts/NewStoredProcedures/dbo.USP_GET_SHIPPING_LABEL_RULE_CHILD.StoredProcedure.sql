GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_GET_SHIPPING_LABEL_RULE_CHILD' ) 
    DROP PROCEDURE USP_GET_SHIPPING_LABEL_RULE_CHILD
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[USP_GET_SHIPPING_LABEL_RULE_CHILD]
    @numShippingRuleID NUMERIC(18, 0),
    @numDomainID NUMERIC(18, 0)
AS 
	SELECT DISTINCT numChildShipID,SLRM.[numShippingRuleID],[numFromValue] AS [FromValue],[numToValue] AS [ToValue], [numDomesticShipID] AS [Domestic],
					[numInternationalShipID] AS [International],
					SLRC.[numPackageTypeID] AS [PackageType],CP.numShippingCompanyID
					,CP.vcPackageName
				FROM dbo.ShippingLabelRuleMaster SLRM 
				INNER JOIN [dbo].[ShippingLabelRuleChild] SLRC ON SLRM.numShippingRuleID = SLRC.numShippingRuleID   
				INNER JOIN dbo.ShippingServiceTypes SST1 ON SST1.intNsoftEnum = SLRC.numDomesticShipID AND SLRM.numDomainID = SST1.numDomainID
				INNER JOIN dbo.ShippingServiceTypes SST2 ON SST2.intNsoftEnum = SLRC.numInternationalShipID AND SLRM.numDomainID = SST2.numDomainID
				INNER JOIN CustomPackages CP ON SLRC.numPackageTypeID = CP.numCustomPackageID 
				WHERE SLRM.numDomainID =  @numDomainID
				AND SLRM.numShippingRuleID = @numShippingRuleID
				
--    DECLARE @strSQL AS NVARCHAR(MAX)
--    DECLARE @strWhere AS VARCHAR(MAX)
--
--    SET @strWhere = ' AND 1=1 '
--
--    IF ISNULL(@numShippingRuleID, 0) <> 0 
--        BEGIN
--            SET @strWhere = @strWhere + ' AND SLRM.numShippingRuleID = '
--                + CAST(@numShippingRuleID AS VARCHAR(10))
--        END
--
--    SET @strSQL = ' SELECT DISTINCT numChildShipID,SLRM.[numShippingRuleID],[numFromValue] AS [FromValue],[numToValue] AS [ToValue], [numDomesticShipID] AS [Domestic],
--					[numInternationalShipID] AS [International],
--					SLRC.[numPackageTypeID] AS [PackageType],CP.numShippingCompanyID
--					,CP.vcPackageName
--				FROM [dbo].[ShippingLabelRuleChild] SLRC
--				LEFT JOIN dbo.ShippingLabelRuleMaster SLRM ON SLRM.numShippingRuleID = SLRC.numShippingRuleID   
--				LEFT JOIN dbo.ShippingServiceTypes SST1 ON SST1.intNsoftEnum = SLRC.numDomesticShipID AND SLRM.numDomainID = SST1.numDomainID
--				LEFT JOIN dbo.ShippingServiceTypes SST2 ON SST2.intNsoftEnum = SLRC.numInternationalShipID AND SLRM.numDomainID = SST2.numDomainID
--				LEFT JOIN CustomPackages CP ON SLRC.numPackageTypeID = CP.numCustomPackageID 
--				WHERE SLRM.numDomainID =  ' + CAST(@numDomainID AS VARCHAR(10))
--				
--    SET @strSQL = @strSQL + @strWhere
--    PRINT @strSQL
--
--    EXEC SP_EXECUTESQL @strSQL				

GO

