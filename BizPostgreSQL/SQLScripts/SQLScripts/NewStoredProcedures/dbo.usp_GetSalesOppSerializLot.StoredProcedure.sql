SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_GetSalesOppSerializLot')
DROP PROCEDURE usp_GetSalesOppSerializLot
GO
CREATE PROCEDURE [dbo].[usp_GetSalesOppSerializLot]
    (
	  @numOppID NUMERIC(9),
      @numOppItemID NUMERIC(9)
    )
AS 
    BEGIN
   
--    SELECT ISNULL(SUM(ISNULL(oppI.numQty,0)),0) numQty FROM OppWarehouseSerializedItem oppI WHERE oppI.numOppID=@numOppID AND oppI.numOppItemID=@numOppItemID
   
   
   SELECT oppM.vcPOppName,numoppitemtCode,opp.vcItemName,ISNULL(SUM(ISNULL(oppI.numQty,0)),0) AS numQty,ISNULL(bitSerialized,0) AS bitSerialized,ISNULL(bitLotNo,0) AS bitLotNo
FROM OpportunityMaster oppM JOIN OpportunityItems opp ON oppM.numOppId=opp.numOppId JOIN Item i ON i.numItemCode=opp.numItemCode 
LEFT JOIN OppWarehouseSerializedItem oppI ON (opp.numoppitemtCode=oppI.numOppItemID AND oppI.numOppID=opp.numOppID)
WHERE opp.numOppId=@numOppID AND opp.numoppitemtCode=@numOppItemID
GROUP BY numoppitemtCode,opp.vcItemName,bitSerialized,bitLotNo,vcPOppName

    END
