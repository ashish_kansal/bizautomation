GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ElasticSearch_GetActionItems')
DROP PROCEDURE dbo.USP_ElasticSearch_GetActionItems
GO
CREATE PROCEDURE [dbo].[USP_ElasticSearch_GetActionItems]
	@numDomainID NUMERIC(18,0)
	,@vcActionItemIds VARCHAR(MAX)
AS
BEGIN
	SELECT 
		numCommId AS id
		,'actionitem' AS module
		,ISNULL(Communication.numCreatedBy,0) numRecOwner
		,ISNULL(Communication.numAssign,0) numAssignedTo
		,ISNULL(DivisionMaster.numTerID,0) numTerID
		,CONCAT('<b style="color:#9900cc">Action Item', CASE WHEN LEN(ListDetails.vcData) > 0 THEN CONCAT(' (',ListDetails.vcData,')') ELSE '' END ,':</b> ',vcCompanyName,', ', ISNULL(Communication.textDetails,'')) AS displaytext
		,ISNULL(Communication.textDetails,'') AS [text]
		,CONCAT('/admin/actionitemdetails.aspx?CommId=',numCommId) AS url
		,ISNULL(vcCompanyName,'') AS Search_vcCompanyName
		,ISNULL(AdditionalContactsInformation.vcFirstName,'') AS Search_vcFirstName 
		,ISNULL(AdditionalContactsInformation.vcLastName,'') AS Search_vcLastName
		,ISNULL(ListDetails.vcData,'') AS Search_vcActionItemType
		,ISNULL(Communication.textDetails,'') AS Search_textDetails
	FROM 
		Communication
	INNER JOIN
		DivisionMaster
	ON
		Communication.numDivisionId = DivisionMaster.numDivisionID
	INNER JOIN
		CompanyInfo
	ON
		DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
	LEFT JOIN
		AdditionalContactsInformation
	ON
		Communication.numContactId = AdditionalContactsInformation.numContactId
	LEFT JOIN
		ListDetails
	ON
		ListDetails.numListID=73
		AND Communication.bitTask = ListDetails.numListItemID
	WHERE 
		Communication.numDomainID=@numDomainID
		AND ISNULL(bitClosedFlag,0)=0
		AND (@vcActionItemIds IS NULL OR Communication.numCommId IN (SELECT Id FROM dbo.SplitIDs(ISNULL(@vcActionItemIds,'0'),',')))

	IF @vcActionItemIds IS NULL
	BEGIN
		DELETE FROM ElasticSearchModifiedRecords WHERE numDomainID=@numDomainID AND vcModule='ActionItem'
	END
END