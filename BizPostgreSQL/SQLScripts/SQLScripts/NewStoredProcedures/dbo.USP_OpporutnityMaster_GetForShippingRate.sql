GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_OpporutnityMaster_GetForShippingRate')
DROP PROCEDURE USP_OpporutnityMaster_GetForShippingRate
GO
CREATE PROCEDURE [dbo].[USP_OpporutnityMaster_GetForShippingRate]
(
	@numDomainID NUMERIC(18,0)
	,@numOppID NUMERIC(18,0)
	,@numWareHouseID NUMERIC(18,0)
	,@dtShipByDate DATE
	,@bitFromService BIT
)
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	SELECT TOP 15
		OM.numDomainId
		,OM.numOppId
		,ISNULL(OM.monMarketplaceShippingCost,0) monMarketplaceShippingCost
		,ISNULL(OM.intUsedShippingCompany,0) numShipVia
		,ISNULL(OM.numShippingService,0) numShipService
		,ISNULL(DM.intShippingCompany,0) numPreferredShipVia
		,ISNULL(DM.numDefaultShippingServiceID,0) numPreferredShipService
		,W.numWareHouseID
		,OM.dtExpectedDate
		,ISNULL(OI.ItemReleaseDate,CAST(OM.dtReleaseDate AS DATE)) dtShipByDate
		,MSFSC.tintType
		,MSFSC.vcPriorities
		,MSFSC.numShipVia
		,MSFSC.numShipService
		,MSFSC.bitOverride
		,MSFSC.fltWeight
		,MSFSC.numShipViaOverride
	FROM
		OpportunityMaster OM
	INNER JOIN
		DivisionMaster DM
	ON
		OM.numDivisionId = DM.numDivisionID
	INNER JOIN
		Domain D
	ON
		OM.numDomainId = D.numDomainId
	INNER JOIN
		MassSalesFulfillmentShippingConfig MSFSC
	ON
		OM.numDomainId = MSFSC.numDomainID
		AND OM.tintSource = MSFSC.numOrderSource
		AND OM.tintSourceType = MSFSC.tintSourceType
	INNER JOIN
		OpportunityItems OI
	ON
		OM.numOppId = OI.numOppId
	INNER JOIN
		WareHouseItems WI
	ON
		OI.numWarehouseItmsID = WI.numWareHouseItemID
	INNER JOIN
		Warehouses W
	ON
		WI.numWareHouseID = W.numWareHouseID
	INNER JOIN
		Item I
	ON
		OI.numItemCode = I.numItemCode
	LEFT JOIN
		OpportunityMasterShippingRateError OMSRE
	ON
		OM.numDomainId = OMSRE.numDomainID
		AND OM.numOppId = OMSRE.numOppID
	WHERE
		(ISNULL(@numDomainID,0) = 0 OR OM.numDomainId = @numDomainID)
		AND (ISNULL(@numOppID,0) = 0 OR OM.numOppId = @numOppID)
		AND (ISNULL(@numWareHouseID,0) = 0 OR W.numWareHouseID = @numWareHouseID)
		AND 1 = (CASE WHEN @bitFromService = 1 THEN 1 ELSE (CASE WHEN ISNULL(OI.ItemReleaseDate,CAST(OM.dtReleaseDate AS DATE))=@dtShipByDate THEN 1 ELSE 0 END) END)
		AND OM.tintOppType=1
		AND OM.tintOppStatus=1
		AND ISNULL(OM.tintshipped,0) = 0
		AND 1 = (CASE WHEN ISNULL(@bitFromService,0) = 1 THEN (CASE WHEN DATEDIFF(DAY,OM.bintCreatedDate,GETUTCDATE()) <= 1 THEN 1 ELSE 0 END) ELSE 1 END)
		AND ISNULL(D.numShippingServiceItemID,0) > 0
		AND ISNULL(OMSRE.intNoOfTimeFailed,0) < 3
		AND NOT EXISTS (SELECT OIInner.numoppitemtCode FROM OpportunityItems OIInner WHERE OIInner.numOppId=OM.numOppId AND OIInner.numItemCode=D.numShippingServiceItemID AND OIInner.numShipFromWarehouse=W.numWareHouseID AND OIInner.ItemReleaseDate=ISNULL(OI.ItemReleaseDate,CAST(OM.dtReleaseDate AS DATE)))
	GROUP BY
		OM.numDomainId
		,OM.numOppId
		,OM.monMarketplaceShippingCost
		,OM.dtExpectedDate
		,OM.intUsedShippingCompany
		,OM.numShippingService
		,DM.intShippingCompany
		,DM.numDefaultShippingServiceID
		,OM.dtReleaseDate
		,OI.ItemReleaseDate
		,W.numWareHouseID
		,MSFSC.tintType
		,MSFSC.vcPriorities
		,MSFSC.numShipVia
		,MSFSC.numShipService
		,MSFSC.bitOverride
		,MSFSC.fltWeight
		,MSFSC.numShipViaOverride
		,OMSRE.intNoOfTimeFailed
	ORDER BY
		ISNULL(OMSRE.intNoOfTimeFailed,0) ASC
		,OM.numOppId
END
GO