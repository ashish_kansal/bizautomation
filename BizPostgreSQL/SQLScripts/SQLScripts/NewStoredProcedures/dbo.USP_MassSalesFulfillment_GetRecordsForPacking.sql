GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_MassSalesFulfillment_GetRecordsForPacking')
DROP PROCEDURE dbo.USP_MassSalesFulfillment_GetRecordsForPacking
GO

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[USP_MassSalesFulfillment_GetRecordsForPacking]
(
	@numDomainID NUMERIC(18,0)
	,@numUserCntID NUMERIC(18,0)
	,@numOppID NUMERIC(18,0)
	,@numOppItemID NUMERIC(18,0)
)
AS
BEGIN
	DECLARE @tintPackingMode TINYINT
	DECLARE @numDefaultSalesShippingDoc NUMERIC(18,0)
	DECLARE @numShippingServiceItemID NUMERIC(18,0)

	SELECT 
		@numDefaultSalesShippingDoc=ISNULL(numDefaultSalesShippingDoc,0)
		,@numShippingServiceItemID = ISNULL(numShippingServiceItemID,0)
	FROM 
		Domain 
	WHERE 
		numDomainId=@numDomainID 

	SELECT
		@tintPackingMode=tintPackingMode
	FROM
		MassSalesFulfillmentConfiguration
	WHERE
		numDomainID = @numDomainID

	SELECT 
		OM.numOppID
		,ISNULL(OM.intUsedShippingCompany,ISNULL(DM.intShippingCompany,0)) numShipVia
		,ISNULL(OM.numShippingService,ISNULL(DM.numDefaultShippingServiceID,0)) numShipService
		,OM.vcPOppName
		,I.numContainer
		,ISNULL(IContainer.vcItemName,'') vcContainer
		,(CASE WHEN ISNULL(I.numContainer,0) > 0 THEN I.numNoItemIntoContainer ELSE OI.numUnitHour END) AS numNoItemIntoContainer
		,ISNULL(IContainer.fltWeight,0) fltContainerWeight
		,ISNULL(IContainer.fltHeight,0) fltContainerHeight
		,ISNULL(IContainer.fltWidth,0) fltContainerWidth
		,ISNULL(IContainer.fltLength,0) fltContainerLength
		,OI.numoppitemtCode
		,I.numItemCode
		,I.vcItemName
		,ISNULL(I.fltWeight,0) fltItemWeight
		,ISNULL(I.fltHeight,0) fltItemHeight
		,ISNULL(I.fltWidth,0) fltItemWidth
		,ISNULL(I.fltLength,0) fltItemLength
		,ISNULL((SELECT TOP 1 monPrice FROM OpportunityItems OIInner WHERE OIInner.numOppID=OM.numOppID AND numItemCode=@numShippingServiceItemID),-1) monCurrentShipRate
		,(CASE WHEN ISNULL(@tintPackingMode,1) = 2 THEN ISNULL(OI.numQtyPicked,0) ELSE ISNULL(OI.numUnitHour,0) END) - ISNULL((SELECT 
																																	SUM(OpportunityBizDocItems.numUnitHour)
																																FROM
																																	OpportunityBizDocs
																																INNER JOIN
																																	OpportunityBizDocItems
																																ON
																																	OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
																																WHERE
																																	OpportunityBizDocs.numOppId = OI.numOppID
																																	AND OpportunityBizDocs.numBizDocId=@numDefaultSalesShippingDoc
																																	AND OpportunityBizDocItems.numOppItemID = OI.numoppitemtcode),0) numTotalQty
	FROM 
		OpportunityMaster OM
	INNER JOIN
		DivisionMaster DM
	ON
		OM.numDivisionId = DM.numDivisionID
	INNER JOIN
		OpportunityItems OI
	ON
		OM.numOppId = OI.numOppId
	INNER JOIN 
		Item AS I
	ON 
		OI.numItemCode = I.numItemCode
	LEFT JOIN
		Item AS IContainer
	ON
		I.numContainer = IContainer.numItemCode
	WHERE
		OM.numOppId=@numOppID
		AND I.charItemType ='P'
		AND ISNULL(I.bitContainer,0) = 0
		AND (OI.numoppitemtCode=@numOppItemID OR ISNULL(@numOppItemID,0) = 0)
END
GO