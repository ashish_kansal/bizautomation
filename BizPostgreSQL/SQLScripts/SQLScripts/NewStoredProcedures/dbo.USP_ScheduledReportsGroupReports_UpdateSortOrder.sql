GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ScheduledReportsGroupReports_UpdateSortOrder')
DROP PROCEDURE dbo.USP_ScheduledReportsGroupReports_UpdateSortOrder
GO
CREATE PROCEDURE [dbo].[USP_ScheduledReportsGroupReports_UpdateSortOrder]
(
	@numDomainID NUMERIC(18,0)
	,@numSRGID NUMERIC(18,0)
	,@numSRGRID NUMERIC(18,0)
	,@intSortOrder INT
)
AS 
BEGIN
	UPDATE 
		SRGR
	SET 
		intSortOrder = ISNULL(@intSortOrder,0)
	FROM 
		ScheduledReportsGroup SRG 
	INNER JOIN 
		ScheduledReportsGroupReports SRGR 
	ON 
		SRG.ID = SRGR.numSRGID 
	WHERE 
		SRG.numDomainID = @numDomainID 
		AND numSRGID=@numSRGID
		AND SRGR.ID = @numSRGRID
END
GO