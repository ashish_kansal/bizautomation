-- exec USP_GetHelpCategories @numHelpCategoryID=3,@tintMode=0
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetHelpCategories')
DROP PROCEDURE USP_GetHelpCategories
GO
CREATE PROCEDURE [dbo].[USP_GetHelpCategories] 
    @numHelpCategoryID NUMERIC,
    @tintMode TINYINT
AS 
    IF @tintMode = 0 
        BEGIN
            SELECT  [numHelpCategoryID],
                    [vcCatecoryName],
                    [bitLinkPage],
                    [numPageID],
                    [numParentCatID],
                    [tintLevel]
            FROM    HelpCategories
            WHERE   ([numHelpCategoryID] = @numHelpCategoryID	OR @numHelpCategoryID=0)
        END
    ELSE 
        IF @tintMode = 1 --select parent category dropdown
            BEGIN
                SELECT  [numHelpCategoryID],
                        [vcCatecoryName]
                FROM    HelpCategories
                WHERE   [numHelpCategoryID] <> @numHelpCategoryID
            END