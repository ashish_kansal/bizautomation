GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_ManagePageElementMaster' ) 
    DROP PROCEDURE USP_ManagePageElementMaster
GO
CREATE PROCEDURE [dbo].[USP_ManagePageElementMaster]
    @numElementId NUMERIC(9) = 0,
    @vcElementName VARCHAR(50),
    @vcUserControlPath VARCHAR(200),
    @vcTagName VARCHAR(50),
    @bitCustomization BIT,
    @bitAdd BIT = 0,
    @bitDelete BIT = 0,
    @bitFlag BIT = 0
AS 
BEGIN
    IF @bitFlag = 0 
        BEGIN
            BEGIN TRY 
                BEGIN TRAN  
        
				DECLARE @numNewElementId NUMERIC(9)
				SELECT @numNewElementId = ISNULL(MAX(numElementID),200) +1 FROM dbo.PageElementMaster WHERE numElementID >=200
                INSERT  INTO dbo.PageElementMaster ( numElementID,vcElementName,
                                                     vcUserControlPath,
                                                     vcTagName,
                                                     bitCustomization,
                                                     bitAdd,
                                                     bitDelete )
                VALUES  (
                          @numNewElementId,@vcElementName,
                          @vcUserControlPath,
                          @vcTagName,
                          @bitCustomization,
                          @bitAdd,
                          @bitDelete
                        ) 
		
                
		
                CREATE TABLE #tempTable ( vcAttributeName VARCHAR(500),
                                          vcControlType VARCHAR(200),
                                          vcControlValues VARCHAR(200),
                                          bitEditor BIT )
		
                INSERT  INTO #tempTable
                        SELECT  vcAttributeName,
                                vcControlType,
                                vcControlValues,
                                bitEditor
                        FROM    dbo.PageElementAttributes
                        WHERE   numElementID = @numElementId
		
                INSERT  INTO dbo.PageElementAttributes
                        SELECT  @numNewElementId,
                                vcAttributeName,
                                vcControlType,
                                vcControlValues,
                                bitEditor
                        FROM    #tempTable 
		
		
                INSERT  INTO emailMergeModule ( numModuleId,
                                                vcModuleName,
                                                tintModuleType )
                VALUES  (
                          @numNewElementId,
                          @vcElementName,
                          1
                        )
		                      
                CREATE TABLE #tempTable1 ( vcMergeField VARCHAR(500),
                                           vcMergeFieldValue VARCHAR(200),
                                           tintModuleType TINYINT )
                INSERT  INTO #tempTable1
                        SELECT  vcMergeField,
                                vcMergeFieldValue,
                                tintModuleType
                        FROM    dbo.EmailMergeFields
                        WHERE   numModuleId = @numElementId
                INSERT  INTO dbo.EmailMergeFields
                        SELECT  vcMergeField,
                                vcMergeFieldValue,
                                @numNewElementId,
                                tintModuleType
                        FROM    #tempTable1
                DROP TABLE #tempTable 
                DROP TABLE #tempTable1
    
    
                COMMIT TRAN 
            END TRY 
            BEGIN CATCH		
                DECLARE @strMsg VARCHAR(200)
                SET @strMsg = ERROR_MESSAGE()
                IF ( @@TRANCOUNT > 0 ) 
                    BEGIN
                        RAISERROR ( @strMsg, 16, 1 ) ;
                        ROLLBACK TRAN
                        RETURN 1
                    END
            END CATCH
        
        END
    ELSE 
        BEGIN
            DELETE  FROM dbo.EmailMergeFields
            WHERE   numModuleId = @numElementId  
            DELETE  FROM emailMergeModule
            WHERE   numModuleId = @numElementId 
            DELETE  FROM dbo.PageElementDetail
            WHERE   numElementId = @numElementId   
            DELETE  FROM dbo.PageElementAttributes
            WHERE   numElementID = @numElementId  
            DELETE  FROM dbo.PageElementMaster
            WHERE   numElementID = @numElementId
		
        END 
	
END

