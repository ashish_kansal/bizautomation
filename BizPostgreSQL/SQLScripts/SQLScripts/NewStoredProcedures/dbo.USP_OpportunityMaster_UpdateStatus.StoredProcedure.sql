GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_OpportunityMaster_UpdateStatus')
DROP PROCEDURE USP_OpportunityMaster_UpdateStatus
GO
-- =============================================
-- Author:		<Author,,Sachin Sadhu>
-- Create date: <Create Date,, 25thjul2014>
-- Description:	<Description,, Update Order Status>
-- Used      IN: Purchase FullFillment
-- =============================================
CREATE PROCEDURE [dbo].[USP_OpportunityMaster_UpdateStatus]
	-- Add the parameters for the stored procedure here		
		@numOppID as numeric(18,0), 
		@numDomainId numeric(9),
		@numStatus numeric(9),
		@numModifiedBy numeric(9)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	Update 
		OpportunityMaster 
	SET 
		numStatus=@numStatus
		,numModifiedBy=@numModifiedBy
		,bintModifiedDate=GETUTCDATE() 
	WHERE 
		numOppId=@numOppId 
		and numDomainId=@numDomainId 

	IF EXISTS (SELECT numOppId FROM OpportunityMaster WHERE numOppID=@numOppId AND tintOppType=1 AND tintOppStatus=1)
	BEGIN
		EXEC USP_SalesFulfillmentQueue_Save @numDomainID,@numModifiedBy,@numOppID,@numStatus
	END

END
