GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p' AND NAME ='USP_GetPOFulfillmentSettings')
DROP PROCEDURE USP_GetPOFulfillmentSettings
GO
CREATE PROCEDURE [dbo].[USP_GetPOFulfillmentSettings]                                          
	@numDomainID AS NUMERIC(9) = 0                                          
AS  

	SELECT * FROM PurchaseOrderFulfillmentSettings WHERE numDomainID = @numDomainID

