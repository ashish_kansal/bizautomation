SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_DashboardTemplate_Delete')
DROP PROCEDURE USP_DashboardTemplate_Delete
GO
CREATE PROCEDURE [dbo].[USP_DashboardTemplate_Delete]
	@numDomainID NUMERIC(18,0)
	,@numTemplateID NUMERIC(18,0)
AS
BEGIN 
	BEGIN TRY
	BEGIN TRANSACTION
		DELETE FROM ReportDashboard WHERE numDomainID=@numDomainID AND ISNULL(numDashboardTemplateID,0)=@numTemplateID AND numReportID NOT IN (SELECT numReportID FROM ReportListMaster WHERE ISNULL(bitDefault,0)=1)
		UPDATE UserMaster SET numDashboardTemplateID=0 WHERE numDomainID=@numDomainID AND numDashboardTemplateID=@numTemplateID
		DELETE FROM DashboardTemplate WHERE numDomainID=@numDomainID AND numTemplateID=@numTemplateID
	COMMIT
	END TRY
	BEGIN CATCH
		DECLARE @ErrorMessage NVARCHAR(4000)
		DECLARE @ErrorNumber INT
		DECLARE @ErrorSeverity INT
		DECLARE @ErrorState INT
		DECLARE @ErrorLine INT
		DECLARE @ErrorProcedure NVARCHAR(200)

		IF @@TRANCOUNT > 0
			ROLLBACK TRANSACTION;

		SELECT 
			@ErrorMessage = ERROR_MESSAGE(),
			@ErrorNumber = ERROR_NUMBER(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE(),
			@ErrorLine = ERROR_LINE(),
			@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

		RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);
	END CATCH
END
GO