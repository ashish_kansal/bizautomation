/****** Object:  StoredProcedure [dbo].[USP_GetCustomerCreditCardInfo]    Script Date: 05/07/2009 22:00:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_ManageCustomerCreditCardInfo' ) 
    DROP PROCEDURE USP_ManageCustomerCreditCardInfo
GO
CREATE PROCEDURE [dbo].[USP_ManageCustomerCreditCardInfo]
  
               @numContactId   NUMERIC(18,0),
               @vcCardHolder   VARCHAR(500),
               @vcCreditCardNo VARCHAR(500),
               @vcCVV2         VARCHAR(200),
               @numCardTypeID   NUMERIC(9),
               @tintValidMonth TINYINT,
               @intValidYear   INT,
               @numUserCntId   NUMERIC(18,0),
               @numCCInfoID INT=0,
               @bitIsdefault BIT=0
    
   
AS 
    BEGIN
   
    IF @bitIsDefault <> 0
    BEGIN
		UPDATE dbo.CustomerCreditCardInfo SET dbo.CustomerCreditCardInfo.bitIsDefault = 0 
		WHERE numContactId=@numUserCntId
	END
    IF @numCCInfoID = 0
    BEGIN
		 INSERT INTO [CustomerCreditCardInfo]
                   ([numContactId],
                    [vcCardHolder],
                    [vcCreditCardNo],
                    [vcCVV2],
                    [numCardTypeID],
                    [tintValidMonth],
                    [intValidYear],
                    [numCreatedby],
                    [dtCreated],
                    [numModifiedby],
                    [dtModified],[bitIsDefault])
        VALUES     (@numContactId,
                    @vcCardHolder,
                    @vcCreditCardNo,
                    @vcCVV2,
                    @numCardTypeID,
                    @tintValidMonth,
                    @intValidYear,
                    @numUserCntId,
                    GETUTCDATE(),
                    @numUserCntId,
                    GETUTCDATE(),@bitIsdefault)
	END
	ELSE
	BEGIN
		
	
      UPDATE dbo.CustomerCreditCardInfo SET vcCardHolder=  @vcCardHolder,vcCreditCardNo=@vcCreditCardNo,vcCVV2=@vcCVV2  ,tintValidMonth= @tintValidMonth,
      intValidYear=@intValidYear,numCardTypeID=@numCardTypeID,bitIsDefault=@bitIsdefault
      WHERE numCCInfoID=@numCCInfoID
      END
    END
