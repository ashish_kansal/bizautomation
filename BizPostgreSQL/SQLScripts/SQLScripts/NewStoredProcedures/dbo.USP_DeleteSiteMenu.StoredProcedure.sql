
GO
/****** Object:  StoredProcedure [dbo].[USP_DeleteSiteMenu]    Script Date: 08/08/2009 16:21:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_DeleteSiteMenu')
DROP PROCEDURE USP_DeleteSiteMenu
GO
CREATE PROCEDURE [dbo].[USP_DeleteSiteMenu]
	@numMenuID numeric(9),
	@numSiteID NUMERIC(9)
AS

DELETE FROM SiteMenu
WHERE numMenuID = @numMenuID AND numSiteID =@numSiteID



