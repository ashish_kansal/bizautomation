GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetOrderItemsForAutoCalculatedShippingRate')
DROP PROCEDURE dbo.USP_GetOrderItemsForAutoCalculatedShippingRate
GO

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[USP_GetOrderItemsForAutoCalculatedShippingRate]
(
	@numDomainID NUMERIC(18,0)
	,@numOppID NUMERIC(18,0)
	,@numWarehouseID NUMERIC(18,0)
	,@dtShipByDate DATE
)
AS
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	-- GET FROM ADDRESS
	SELECT
		AD.vcStreet
		,AD.vcCity
		,ISNULL((SELECT vcStateCode FROM ShippingStateMaster WHERE numShipCompany=91 AND vcStateName=dbo.fn_GetState(AD.numState)),'') vcStateFedex
		,ISNULL((SELECT vcStateCode FROM ShippingStateMaster WHERE numShipCompany=88 AND vcStateName=dbo.fn_GetState(AD.numState)),'') vcStateUPS
		,ISNULL((SELECT vcStateCode FROM ShippingStateMaster WHERE numShipCompany=90 AND vcStateName=dbo.fn_GetState(AD.numState)),'') vcStateUSPS
		,AD.vcPostalCode AS vcZipCode
		,ISNULL((SELECT vcCountryCode FROM ShippingCountryMaster WHERE numShipCompany=91 AND vcCountryName=dbo.fn_GetListItemName(AD.numCountry)),'') vcCountryFedex
		,ISNULL((SELECT vcCountryCode FROM ShippingCountryMaster WHERE numShipCompany=91 AND vcCountryName=dbo.fn_GetListItemName(AD.numCountry)),'') vcCountryUPS
		,ISNULL((SELECT vcCountryCode FROM ShippingCountryMaster WHERE numShipCompany=91 AND vcCountryName=dbo.fn_GetListItemName(AD.numCountry)),'') vcCountryUSPS
	FROM
		Warehouses W
	LEFT JOIN
		AddressDetails AD
	ON
		W.numAddressID = AD.numAddressID
	WHERE
		W.numDomainID = @numDomainID
		AND W.numWareHouseID = @numWarehouseID

	-- GET TO ADDRESS
	SELECT
		vcStreet
		,vcCity
		,ISNULL((SELECT vcStateCode FROM ShippingStateMaster WHERE numShipCompany=91 AND vcStateName=dbo.fn_GetState(vcState)),'') vcStateFedex
		,ISNULL((SELECT vcStateCode FROM ShippingStateMaster WHERE numShipCompany=88 AND vcStateName=dbo.fn_GetState(vcState)),'') vcStateUPS
		,ISNULL((SELECT vcStateCode FROM ShippingStateMaster WHERE numShipCompany=90 AND vcStateName=dbo.fn_GetState(vcState)),'') vcStateUSPS
		,vcZipCode
		,ISNULL((SELECT vcCountryCode FROM ShippingCountryMaster WHERE numShipCompany=91 AND vcCountryName=dbo.fn_GetListItemName(vcCountry)),'') vcCountryFedex
		,ISNULL((SELECT vcCountryCode FROM ShippingCountryMaster WHERE numShipCompany=88 AND vcCountryName=dbo.fn_GetListItemName(vcCountry)),'') vcCountryUPS
		,ISNULL((SELECT vcCountryCode FROM ShippingCountryMaster WHERE numShipCompany=90 AND vcCountryName=dbo.fn_GetListItemName(vcCountry)),'') vcCountryUSPS
	FROM
		dbo.fn_GetShippingReportAddress(2,@numDomainId,@numOppID)

	-- GET CONTAINERS/PACKAGES
	DECLARE @TempContiner TABLE
	(
		ID INT IDENTITY(1,1)
		,fltContainerWeight FLOAT
		,fltContainerHeight FLOAT
		,fltContainerWidth FLOAT
		,fltContainerLength FLOAT
		,numQty FLOAT
	)

	DECLARE @TEMPItems TABLE
	(
		ID INT IDENTITY(1,1)
		,numContainer NUMERIC(18,0)
		,numNoItemIntoContainer FLOAT
		,fltItemWeight FLOAT
		,fltItemHeight FLOAT
		,fltItemWidth FLOAT
		,fltItemLength FLOAT
		,fltContainerWeight FLOAT
		,fltContainerHeight FLOAT
		,ltContainerWidth FLOAT
		,fltContainerLength FLOAT
		,numTotalQty FLOAT
	)

	INSERT INTO 
		@TEMPItems
	SELECT
		T1.numContainer
		,T1.numNoItemIntoContainer
		,T1.fltItemWeight
		,T1.fltItemHeight
		,T1.fltItemWidth
		,T1.fltItemLength
		,ISNULL(IContainer.fltWeight,0) fltContainerWeight
		,ISNULL(IContainer.fltHeight,0) fltContainerHeight
		,ISNULL(IContainer.fltWidth,0) fltContainerWidth
		,ISNULL(IContainer.fltLength,0) fltContainerLength
		,T1.numTotalQty
	FROM
	(
		SELECT 
			I.numContainer
			,(CASE WHEN ISNULL(I.numContainer,0) > 0 THEN I.numNoItemIntoContainer ELSE SUM(OI.numUnitHour) END) AS numNoItemIntoContainer
			,ISNULL(I.fltWeight,0) fltItemWeight
			,ISNULL(I.fltHeight,0) fltItemHeight
			,ISNULL(I.fltWidth,0) fltItemWidth
			,ISNULL(I.fltLength,0) fltItemLength
			,SUM(OI.numUnitHour) numTotalQty
		FROM 
			OpportunityMaster OM
		INNER JOIN
			OpportunityItems OI
		ON
			OM.numOppID = OI.numOppID
		INNER JOIN
			WareHouseItems WI
		ON
			OI.numWarehouseItmsID = WI.numWareHouseItemID
		INNER JOIN 
			Item AS I
		ON 
			OI.numItemCode = I.numItemCode
		WHERE
			OM.numOppId=@numOppID
			AND WI.numWareHouseID = @numWarehouseID
			AND ISNULL(OI.ItemReleaseDate,CAST(OM.dtReleaseDate AS DATE)) = @dtShipByDate
			AND ISNULL(I.bitContainer,0) = 0
		GROUP BY
			I.numContainer
			,I.numNoItemIntoContainer
			,I.fltWeight
			,I.fltHeight
			,I.fltWidth
			,I.fltLength
	) AS T1
	LEFT JOIN
		Item IContainer
	ON
		T1.numContainer = IContainer.numItemCode

	DECLARE @i AS INT = 1
	DECLARE @iCount AS INT
	DECLARE @numContainer NUMERIC(18,0)
	DECLARE @numNoItemIntoContainer FLOAT
	DECLARE @fltItemWeight FLOAT
	DECLARE @fltItemHeight FLOAT
	DECLARE @fltItemWidth FLOAT
	DECLARE @fltItemLength FLOAT
	DECLARE @fltContainerWeight FLOAT
	DECLARE @fltContainerHeight FLOAT
	DECLARE @ltContainerWidth FLOAT
	DECLARE @fltContainerLength FLOAT
	DECLARE @numTotalQty FLOAT

	SELECT @iCount = COUNT(*) FROM @TEMPItems

	WHILE @i <= @iCount
	BEGIN
		SELECT 
			@numContainer=numContainer
			,@numNoItemIntoContainer=numNoItemIntoContainer
			,@fltItemWeight=fltItemWeight
			,@fltItemHeight=fltItemHeight
			,@fltItemWidth=fltItemWidth
			,@fltItemLength=fltItemLength
			,@fltContainerWeight=fltContainerWeight
			,@fltContainerHeight=fltContainerHeight
			,@ltContainerWidth=ltContainerWidth
			,@fltContainerLength=fltContainerLength
			,@numTotalQty=numTotalQty
		FROM
			@TEMPItems
		WHERE
			ID=@i

		IF @numTotalQty <= @numNoItemIntoContainer
		BEGIN
			INSERT INTO @TempContiner
			(
				fltContainerWeight
				,fltContainerHeight
				,fltContainerWidth
				,fltContainerLength
				,numQty
			)
			VALUES
			(
				ISNULL(@fltContainerWeight,0) + (ISNULL(@numTotalQty,0) * ISNULL(@fltItemWeight,0))
				,@fltContainerHeight
				,@ltContainerWidth
				,@fltContainerLength
				,@numTotalQty
			)
		END
		ELSE
		BEGIN
			WHILE @numTotalQty > @numNoItemIntoContainer
			BEGIN
				INSERT INTO @TempContiner
				(
					fltContainerWeight
					,fltContainerHeight
					,fltContainerWidth
					,fltContainerLength
					,numQty
				)
				VALUES
				(
					ISNULL(@fltContainerWeight,0) + (ISNULL(@numNoItemIntoContainer,0) * ISNULL(@fltItemWeight,0))
					,@fltContainerHeight
					,@ltContainerWidth
					,@fltContainerLength
					,@numNoItemIntoContainer
				)
			
				SET @numTotalQty = @numTotalQty - @numNoItemIntoContainer
			END

			IF ISNULL(@numTotalQty,0) > 0
			BEGIN
				INSERT INTO @TempContiner
				(
					fltContainerWeight
					,fltContainerHeight
					,fltContainerWidth
					,fltContainerLength
					,numQty
				)
				VALUES
				(
					ISNULL(@fltContainerWeight,0) + (ISNULL(@numTotalQty,0) * ISNULL(@fltItemWeight,0))
					,@fltContainerHeight
					,@ltContainerWidth
					,@fltContainerLength
					,@numTotalQty
				)
			END
		END


		SET @i = @i + 1
	END

	SELECT * FROm @TempContiner
END
GO