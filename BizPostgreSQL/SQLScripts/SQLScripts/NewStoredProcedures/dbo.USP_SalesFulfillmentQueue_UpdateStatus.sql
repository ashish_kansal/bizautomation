SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_SalesFulfillmentQueue_UpdateStatus')
DROP PROCEDURE USP_SalesFulfillmentQueue_UpdateStatus
GO
CREATE PROCEDURE [dbo].[USP_SalesFulfillmentQueue_UpdateStatus]
	@numDomainID NUMERIC(18,0),  
	@numSFQID NUMERIC(18,0),
	@vcMessage VARCHAR(MAX),
	@tintRule AS TINYINT,
	@bitSuccess BIT
AS
BEGIN
	DECLARE @numUserCntID NUMERIC(18,0)
	DECLARE @numOppID AS NUMERIC(18,0)
	DECLARE @numRule2SuccessOrderStatus NUMERIC(18,0)
	DECLARE @numRule2FailOrderStatus NUMERIC(18,0)
	DECLARE @numRule3FailOrderStatus NUMERIC(18,0)
	DECLARE @numRule4FailOrderStatus NUMERIC(18,0)
	DECLARE @numRule5OrderStatus NUMERIC(18,0)
	DECLARE @numRule6OrderStatus NUMERIC(18,0)
	DECLARE @bitActive BIT
	DECLARE @bitRule5IsActive BIT
	DECLARE @bitRule6IsActive BIT
	DECLARE @numTempOppID NUMERIC(18,0)
	SET @numTempOppID = @numSFQID

	SELECT
		@bitActive=bitActive
		,@numRule2SuccessOrderStatus=numRule2SuccessOrderStatus
		,@numRule2FailOrderStatus=numRule2FailOrderStatus
		,@numRule3FailOrderStatus=numRule3FailOrderStatus
		,@numRule4FailOrderStatus=numRule4FailOrderStatus
		,@bitRule5IsActive=bitRule5IsActive
		,@numRule5OrderStatus=numRule5OrderStatus
		,@bitRule6IsActive=bitRule6IsActive
		,@numRule6OrderStatus=numRule6OrderStatus
	FROM
		SalesFulfillmentConfiguration
	WHERE
		numDomainID = @numDomainID

	SELECT
		@numOppID=numOppID
		,@numUserCntID=numUserCntID
	FROM
		SalesFulfillmentQueue
	WHERE
		numSFQID=@numSFQID
		AND @tintRule NOT IN (5,6)

	-- CHANGE ORDER STATUS BASED ON SALES FULFILLMENT CONFIGURATION
	IF @tintRule = 2
	BEGIN
		IF @bitSuccess = 1
		BEGIN
			UPDATE OpportunityMaster SET numStatus = @numRule2SuccessOrderStatus WHERE numOppId=@numOppID
			EXEC USP_SalesFulfillmentQueue_Save @numDomainID,@numUserCntID,@numOppID,@numRule2SuccessOrderStatus
		END
		ELSE
		BEGIN
			UPDATE OpportunityMaster SET numStatus = @numRule2FailOrderStatus WHERE numOppId=@numOppID
			EXEC USP_SalesFulfillmentQueue_Save @numDomainID,@numUserCntID,@numOppID,@numRule2FailOrderStatus
		END
	END
	ELSE IF @tintRule = 3 AND ISNULL(@bitSuccess,0) = 0
	BEGIN
		UPDATE OpportunityMaster SET numStatus = @numRule3FailOrderStatus WHERE numOppId=@numOppID
		EXEC USP_SalesFulfillmentQueue_Save @numDomainID,@numUserCntID,@numOppID,@numRule3FailOrderStatus
	END
	ELSE IF @tintRule = 4 AND ISNULL(@bitSuccess,0) = 0
	BEGIN
		UPDATE OpportunityMaster SET numStatus = @numRule4FailOrderStatus WHERE numOppId=@numOppID
		EXEC USP_SalesFulfillmentQueue_Save @numDomainID,@numUserCntID,@numOppID,@numRule4FailOrderStatus
	END
	ELSE IF @tintRule = 5 AND ISNULL(@bitRule5IsActive,0) = 1 AND ISNULL(@bitActive,0) = 1
	BEGIN
		UPDATE OpportunityMaster SET numStatus = @numRule5OrderStatus WHERE numOppId=@numSFQID

		IF (
			SELECT 
				COUNT(*) 
			FROM 
				SalesFulfillmentConfiguration 
			WHERE 
				numDomainID=@numDomainId 
				AND ISNULL(bitActive,0)=1
				ANd ISNULL(bitRule5IsActive,0) = 1
		) > 0
		AND NOT EXISTS (SELECT * FROM SalesFulfillmentQueue WHERE numOppID=@numOppID AND numOrderStatus=-1)
		BEGIN
			IF NOT EXISTS (SELECT numSFLID FROM SalesFulfillmentLog WHERE numOppID=@numSFQID AND vcMessage=@vcMessage)
			BEGIN
				INSERT INTO SalesFulfillmentQueue
				( 
					numDomainID
					,numUserCntID
					,numOppID
					,numOrderStatus
					,dtDate
					,bitExecuted
					,intNoOfTimesTried
				)
				VALUES
				(
					@numDomainId
					,0
					,@numSFQID
					,-1
					,GETUTCDATE()
					,1
					,1
				)

				SET @numSFQID = SCOPE_IDENTITY()
			END			
		END
	END
	ELSE IF @tintRule = 6 AND ISNULL(@bitRule6IsActive,0) = 1 AND ISNULL(@bitActive,0) = 1
	BEGIN
		UPDATE OpportunityMaster SET numStatus = @numRule6OrderStatus WHERE numOppId=@numSFQID

		IF (
			SELECT 
				COUNT(*) 
			FROM 
				SalesFulfillmentConfiguration 
			WHERE 
				numDomainID=@numDomainId 
				AND ISNULL(bitActive,0)=1
				ANd ISNULL(bitRule6IsActive,0) = 1
		) > 0
		AND NOT EXISTS (SELECT * FROM SalesFulfillmentQueue WHERE numOppID=@numOppID AND numOrderStatus=-2)
		BEGIN
			INSERT INTO SalesFulfillmentQueue
			( 
				numDomainID
				,numUserCntID
				,numOppID
				,numOrderStatus
				,dtDate
				,bitExecuted
				,intNoOfTimesTried
			)
			VALUES
			(
				@numDomainId
				,0
				,@numSFQID
				,-2
				,GETUTCDATE()
				,1
				,1
			)

			SET @numSFQID = SCOPE_IDENTITY()
		END
	END

	-- MARK RULE AS EXECUTED
	UPDATE
		SalesFulfillmentQueue
	SET
		bitExecuted = 1
	WHERE
		numSFQID=@numSFQID
		AND @tintRule NOT IN (5,6)

	-- MAKE ENTRY IN LOG TABLE
	IF @tintRule = 5
	BEGIN
		IF NOT EXISTS (SELECT numSFLID FROM SalesFulfillmentLog WHERE numSFQID=@numSFQID AND vcMessage=@vcMessage)
		BEGIN
			INSERT INTO SalesFulfillmentLog
			(
				numDomainID,
				numSFQID,
				numOPPID,
				vcMessage,
				dtDate,
				bitSuccess
			)
			SELECT
				numDomainID,
				numSFQID,
				numOppID,
				@vcMessage,
				GETUTCDATE(),
				@bitSuccess
			FROM
				SalesFulfillmentQueue
			WHERE
				numSFQID=@numSFQID
		END
	END
	ELSE IF @tintRule = 6
	BEGIN
		IF NOT EXISTS (SELECT numSFLID FROM SalesFulfillmentLog WHERE numOppID=@numTempOppID AND vcMessage=@vcMessage)
		BEGIN
			INSERT INTO SalesFulfillmentLog
			(
				numDomainID,
				numSFQID,
				numOPPID,
				vcMessage,
				dtDate,
				bitSuccess
			)
			SELECT
				numDomainID,
				numSFQID,
				numOppID,
				@vcMessage,
				GETUTCDATE(),
				@bitSuccess
			FROM
				SalesFulfillmentQueue
			WHERE
				numSFQID=@numSFQID
		END
	END
	ELSE
	BEGIN
		INSERT INTO SalesFulfillmentLog
		(
			numDomainID,
			numSFQID,
			numOPPID,
			vcMessage,
			dtDate,
			bitSuccess
		)
		SELECT
			numDomainID,
			numSFQID,
			numOppID,
			@vcMessage,
			GETUTCDATE(),
			@bitSuccess
		FROM
			SalesFulfillmentQueue
		WHERE
			numSFQID=@numSFQID
	END
END