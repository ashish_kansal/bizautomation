

GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_EmailHistory_DeleteArchived' ) 
    DROP PROCEDURE USP_EmailHistory_DeleteArchived
GO
CREATE PROCEDURE USP_EmailHistory_DeleteArchived
	@numDomainID NUMERIC(18,0),
	@numUserCntID NUMERIC(18,0),
    @dtFrom AS DATE,
	@dtTo AS DATE
AS 
BEGIN
	DECLARE @ErrorMessage NVARCHAR(4000);SET @ErrorMessage=''
	DECLARE @ErrorSeverity INT;
	DECLARE @ErrorState INT;
	DECLARE @numEmailArchiveNodeID NUMERIC(18,0)
	DECLARE @strSQL AS VARCHAR(MAX)

	BEGIN TRANSACTION
	BEGIN TRY
		--numFixID = 7 = Email Archive
		SELECT @numEmailArchiveNodeID=ISNULL(numNodeID,0) FROM InboxTreeSort WHERE numDomainID = @numDomainID AND numUserCntID = @numUserCntID AND numFixID = 2 AND ISNULL(bitSystem,0) = 1

		DELETE FROM dbo.Correspondence WHERE numEmailHistoryID IN (SELECT ISNULL(numEmailHstrID,0) FROM EmailHistory WHERE numDomainID = @numDomainID AND numUserCntId = @numUserCntID AND numNodeId = @numEmailArchiveNodeID AND CAST(dtReceivedOn AS DATE) >= @dtFrom AND CAST(dtReceivedOn AS DATE) <= @dtTo)

		DELETE FROM EmailHStrToBCCAndCC WHERE numEmailHstrID IN (SELECT ISNULL(numEmailHstrID,0) FROM EmailHistory WHERE numDomainID = @numDomainID AND numUserCntId = @numUserCntID AND numNodeId = @numEmailArchiveNodeID AND CAST(dtReceivedOn AS DATE) >= @dtFrom AND CAST(dtReceivedOn AS DATE) <= @dtTo)       
       
		DELETE FROM EmailHstrAttchDtls WHERE numEmailHstrID IN (SELECT ISNULL(numEmailHstrID,0) FROM EmailHistory WHERE numDomainID = @numDomainID AND numUserCntId = @numUserCntID AND numNodeId = @numEmailArchiveNodeID AND CAST(dtReceivedOn AS DATE) >= @dtFrom AND CAST(dtReceivedOn AS DATE) <= @dtTo)
	
		DELETE EmailHistory WHERE numEmailHstrID IN (SELECT ISNULL(numEmailHstrID,0) FROM EmailHistory WHERE numDomainID = @numDomainID AND numUserCntId = @numUserCntID AND numNodeId = @numEmailArchiveNodeID AND CAST(dtReceivedOn AS DATE) >= @dtFrom AND CAST(dtReceivedOn AS DATE) <= @dtTo)                   
	END TRY
	BEGIN CATCH
		SELECT @ErrorMessage = ERROR_MESSAGE(), @ErrorSeverity = ERROR_SEVERITY(), @ErrorState = ERROR_STATE();

		IF @@TRANCOUNT > 0
			ROLLBACK TRANSACTION;

		-- Use RAISERROR inside the CATCH block to return error
		-- information about the original error that caused
		-- execution to jump to the CATCH block.
		RAISERROR (@ErrorMessage,@ErrorSeverity,@ErrorState);
	END CATCH

	IF @@TRANCOUNT > 0
		COMMIT TRANSACTION
END



