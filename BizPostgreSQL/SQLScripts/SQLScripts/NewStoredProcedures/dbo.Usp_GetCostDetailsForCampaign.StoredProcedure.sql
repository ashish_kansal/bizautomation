SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- EXEC Usp_GetCostDetailsForCampaign 91,1
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'Usp_GetCostDetailsForCampaign' ) 
    DROP PROCEDURE Usp_GetCostDetailsForCampaign
GO
CREATE PROCEDURE [dbo].[Usp_GetCostDetailsForCampaign]
    (
      @numCampaignID NUMERIC(18, 0),
      @numDomainID NUMERIC(18, 0)
    )
AS 
    BEGIN
        SELECT  ( SELECT    vcAccountName
                  FROM      dbo.Chart_Of_Accounts
                  WHERE     numAccountID = BD.numExpenseAccountID
                ) AS [vcAccountName],
                BH.numBillID,
                BD.numClassID,
                LD.vcData AS [vcClassName],
                BD.numCampaignID,
                BD.vcDescription,
                BD.monAmount
        FROM    dbo.BillDetails BD
                JOIN dbo.BillHeader BH ON BD.numBillID = BH.numBillID
                LEFT JOIN dbo.ListDetails LD ON BD.numClassID = LD.numListItemID
                                                AND numListID = 381
        WHERE   BH.numDomainID = @numDomainID
                AND BD.numCampaignID = @numCampaignID
	
	
    END