
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetUserBankAccounts')
	DROP PROCEDURE USP_GetUserBankAccounts
GO

/****** Added By : Joseph ******/
/****** Gets All Bank Account Details from Table BankDetails  for a UserContactID******/


CREATE PROCEDURE [dbo].[USP_GetUserBankAccounts]
	@numDomainID numeric(18, 0),
	@numUserContactID numeric(18, 0)
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT DISTINCT ISNUll(BD.numBankDetailID,0) AS numBankDetailID,BD.numAccountID, BD.vcAccountNumber, BM.vcFIName,  BD.vcAccountType,
ISNUll((SELECT TOP 1 monAvailableBalance FROM BankStatementHeader A WHERE A.numBankDetailID = BD.numBankDetailID  
AND dtAvailableBalanceDate = (SELECT TOP 1 dtLedgerBalanceDate FROM BankStatementHeader 
WHERE numBankDetailID = BD.numBankDetailID ORDER BY dtCreatedDate DESC)ORDER BY dtCreatedDate DESC),0) AS monAvailableBalance,
dbo.FormatedDateTimeFromDate((SELECT TOP 1 dtAvailableBalanceDate FROM BankStatementHeader WHERE numBankDetailID = BD.numBankDetailID ORDER BY dtCreatedDate DESC),@numDomainID)AS dtAvailableBalanceDate,
ISNUll((SELECT TOP 1 monLedgerBalance FROM BankStatementHeader WHERE numBankDetailID = BD.numBankDetailID  
AND dtLedgerBalanceDate =  (SELECT TOP 1 dtLedgerBalanceDate FROM BankStatementHeader WHERE numBankDetailID = BD.numBankDetailID ORDER BY dtCreatedDate DESC) ORDER BY dtCreatedDate DESC),0) AS monLedgerBalance,
dbo.FormatedDateTimeFromDate((SELECT TOP 1 dtLedgerBalanceDate FROM BankStatementHeader WHERE numBankDetailID = BD.numBankDetailID ORDER BY dtCreatedDate DESC),@numDomainID) AS dtLedgerBalanceDate
FROM dbo.BankDetails BD 
INNER JOIN dbo.Chart_Of_Accounts COA ON BD.numBankDetailID = COA.numBankDetailID 
INNER JOIN dbo.BankMaster BM ON BD.numBankMasterID = BM.numBankMasterID 
LEFT JOIN dbo.BankStatementHeader BSH ON BD.numBankDetailID = BSH.numBankDetailID 
WHERE numUserContactID = @numUserContactID AND BD.numDomainID = @numDomainID AND BD.bitIsActive = 1
AND COA.IsConnected = 1  AND COA.bitActive = 1 
GROUP BY BD.vcAccountType, BM.vcFIName,BD.vcAccountNumber,BSH.dtAvailableBalanceDate,BSH.monAvailableBalance,
BSH.dtLedgerBalanceDate,BSH.monLedgerBalance,BD.numBankDetailID ,BD.numAccountID  ORDER BY BM.vcFIName,BD.vcAccountType DESC
	


	
GO