
GO
/****** Object:  StoredProcedure [dbo].[USP_ManageSiteTemplates]    Script Date: 08/08/2009 16:08:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ManageSiteTemplates')
DROP PROCEDURE USP_ManageSiteTemplates
GO
CREATE PROCEDURE [dbo].[USP_ManageSiteTemplates]
	@numTemplateID numeric(9)=0 output,
	@vcTemplateName varchar(50),
	@txtTemplateHTML text,
	@numSiteID numeric(9),
	@numDomainID numeric(9),
	@numUserCntID NUMERIC(9)

AS
IF @numTemplateID = 0 AND (NOT EXISTS(SELECT [vcTemplateName] FROM SiteTemplates WHERE [vcTemplateName] = @vcTemplateName
																					AND [numSiteID] = @numSiteID))
BEGIN
	INSERT INTO SiteTemplates (
		[vcTemplateName],
		[txtTemplateHTML],
		[numSiteID],
		[numDomainID],
		[numCreatedBy],
		[dtCreateDate],
		[numModifiedBy],
		[dtModifiedDate]
	)
	VALUES (
		@vcTemplateName,
		@txtTemplateHTML,
		@numSiteID,
		@numDomainID,
		@numUserCntID,
		GETUTCDATE(),
		@numUserCntID,
		GETUTCDATE()
	)
SELECT @numTemplateID = SCOPE_IDENTITY()
END
ELSE BEGIN
	UPDATE SiteTemplates SET 
		[vcTemplateName] = @vcTemplateName,
		[txtTemplateHTML] = @txtTemplateHTML,
		[numModifiedBy] = @numUserCntID,
		[dtModifiedDate] = GETUTCDATE()
	WHERE [numTemplateID] = @numTemplateID AND [numDomainID] = @numDomainID

END

