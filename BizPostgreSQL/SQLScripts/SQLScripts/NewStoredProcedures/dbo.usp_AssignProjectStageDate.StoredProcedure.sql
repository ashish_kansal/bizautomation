GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_AssignProjectStageDate')
DROP PROCEDURE usp_AssignProjectStageDate
GO
CREATE PROCEDURE [dbo].[usp_AssignProjectStageDate]
    @numDomainid NUMERIC = 0,
    @numProjectID NUMERIC = 0 ,
    @numOppID NUMERIC = 0 
AS 

SELECT  MAX(intDueDays) AS intDueDays,SD.numStageDetailId,ROW_NUMBER() OVER( order by MAX(intDueDays)) AS ROWNUMBER
INTO #TempTable
 FROM StageDependency SD join StagePercentageDetails SP on SD.numDependantOnID=SP.numStageDetailsId
WHERE SP.numdomainid = @numDomainid AND numProjectid=@numProjectID and numOppid=@numOppID GROUP BY SD.numStageDetailId
ORDER BY MAX(intDueDays)

--SELECT * FROM #TempTable T 

DECLARE @minROWNUMBER INT
DECLARE @maxROWNUMBER INT
DECLARE @intDueDays int
DECLARE @EndDate DATETIME
DECLARE @numStageDetailId NUMERIC(9)

SELECT  @minROWNUMBER = MIN(ROWNUMBER) , @maxROWNUMBER =MAX(ROWNUMBER) FROM #TempTable

WHILE  @minROWNUMBER <= @maxROWNUMBER
    BEGIN
   	    SELECT @intDueDays = intDueDays,@numStageDetailId=numStageDetailId FROM #TempTable WHERE ROWNUMBER=@minROWNUMBER
   	    
   	    SELECT @EndDate = MAX(dtEndDate) FROM StageDependency SD join StagePercentageDetails SP on SD.numDependantOnID=SP.numStageDetailsId
               where SP.numdomainid = @numDomainid AND numProjectid=@numProjectID and numOppid=@numOppID AND SD.numStageDetailId=@numStageDetailId AND SP.intDueDays=@intDueDays
   	        
   	       UPDATE StagePercentageDetails SET dtStartDate=@EndDate + 1 , dtEndDate=@EndDate + intDueDays 
   	       WHERE numStageDetailsId=@numStageDetailId AND numdomainid = @numDomainid AND numProjectid=@numProjectID and numOppid=@numOppID
   	        
        SELECT  @minROWNUMBER = MIN(ROWNUMBER) FROM  #TempTable WHERE  [ROWNUMBER] > @minROWNUMBER
    END	

DROP TABLE #TempTable
