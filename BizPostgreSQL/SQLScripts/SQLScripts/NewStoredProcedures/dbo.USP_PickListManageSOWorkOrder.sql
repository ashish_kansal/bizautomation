GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_PickListManageSOWorkOrder')
DROP PROCEDURE USP_PickListManageSOWorkOrder
GO
CREATE PROCEDURE [dbo].[USP_PickListManageSOWorkOrder]
	@numDomainID AS numeric(18,0),
	@numUserCntID AS NUMERIC(18,0),
	@numOppID AS NUMERIC(18,0),
	@numOppBizDocID AS NUMERIC(18,0)
AS
BEGIN
	DECLARE @numItemCode AS NUMERIC(18,0),@numUnitHour AS FLOAT,@numWarehouseItmsID AS NUMERIC(18,0),@vcItemDesc AS VARCHAR(1000)
	DECLARE @numWOId AS NUMERIC(18,0),@vcInstruction AS VARCHAR(1000),@bintCompliationDate AS DATETIME, @numWOAssignedTo NUMERIC(18,0)
	DECLARE @numoppitemtCode NUMERIC(18,0)
	DECLARE @numWarehouseID NUMERIC(18,0)
	DECLARE @numBuildProcessID NUMERIC(18,0)
  
	DECLARE @TEMP TABLE
	(
		ID INT IDENTITY(1,1)
		,numoppitemtCode NUMERIC(18,0)
		,numItemCode NUMERIC(18,0)
		,numUnitHour FLOAT
		,numWarehouseItmsID NUMERIC(18,0)
		,vcItemDesc VARCHAR(1000)
		,vcInstruction VARCHAR(1000)
		,bintCompliationDate DATETIME
		,numWOAssignedTo NUMERIC(18,0)
		,numBuildProcessID NUMERIC(18,0)
	)

	INSERT INTO @TEMP
	(
		numoppitemtCode
		,numItemCode
		,numUnitHour
		,numWarehouseItmsID
		,vcItemDesc
		,vcInstruction
		,bintCompliationDate
		,numBuildProcessID
	)
	SELECT 
		numoppitemtCode
		,OpportunityItems.numItemCode
		,OpportunityBizDocItems.numUnitHour
		,OpportunityItems.numWarehouseItmsID
		,OpportunityItems.vcItemDesc
		,vcInstruction
		,bintCompliationDate
		,ISNULL(numBusinessProcessId,0)
	FROM
		OpportunityBizDocItems
	INNER JOIN 
		OpportunityItems             
	ON
		OpportunityBizDocItems.numOppItemID = OpportunityItems.numoppitemtCode 
	INNER JOIN
		Item
	ON
		OpportunityItems.numItemCode = Item.numItemCode                                      
	WHERE 
		numOppID=@numOppID
		AND numOppBizDocID = @numOppBizDocID
		AND ISNULL(bitWorkOrder,0)=1
		AND ISNULL(OpportunityBizDocItems.numUnitHour,0) > 0

	DECLARE @i INT = 1
	DECLARE @iCount AS INT

	SELECT @iCount=COUNT(*) FROM @TEMP

	WHILE @i <= @iCount
	BEGIN
		SELECT 
			@numItemCode=numItemCode
			,@numUnitHour=numUnitHour
			,@numWarehouseItmsID=numWarehouseItmsID
			,@vcItemDesc=vcItemDesc
			,@vcInstruction=vcInstruction
			,@bintCompliationDate=bintCompliationDate
			,@numoppitemtCode=numoppitemtCode
			,@numBuildProcessID=numBuildProcessID
		FROM 
			@TEMP                                                                         
		WHERE 
			ID=@i

		SET @numWarehouseID = ISNULL((SELECT numWareHouseID FROM WareHouseItems WHERE numWareHouseItemID = @numWarehouseItmsID),0)

		IF EXISTS (SELECT 
						item.numItemCode
					FROM 
						item                                
					INNER JOIN 
						ItemDetails Dtl 
					ON 
						numChildItemID=numItemCode
					WHERE 
						numItemKitID=@numItemCode
						AND charItemType='P'
						AND NOT EXISTS (SELECT numWareHouseItemId FROM WareHouseItems WHERE numItemID=item.numItemCode AND numWareHouseID=@numWarehouseID))
		BEGIN
			RAISERROR('SELECTED_WAREHOUSE_IS_NOT_AVAILABLE_FOR_ALL_CHILD_ITEMS',16,1)
			RETURN
		END
		ELSE
		BEGIN
			INSERT INTO WorkOrder
			(
				numItemCode
				,numQtyItemsReq
				,numWareHouseItemId
				,numCreatedBy
				,bintCreatedDate
				,numDomainID
				,numWOStatus
				,numOppId
				,vcItemDesc
				,vcInstruction
				,bintCompliationDate
				,numAssignedTo
				,numOppItemID
			)
			VALUES
			(
				@numItemCode
				,@numUnitHour
				,@numWarehouseItmsID
				,@numUserCntID
				,GETUTCDATE()
				,@numDomainID
				,0
				,@numOppID
				,@vcItemDesc
				,@vcInstruction
				,@bintCompliationDate
				,ISNULL((SELECT numBuildManager FROM Sales_process_List_Master WHERE Slp_Id=@numBuildProcessId),0)
				,@numoppitemtCode
			)
	
			SET @numWOId = SCOPE_IDENTITY()
			EXEC dbo.USP_UpdateNameTemplateValueForWorkOrder 3,@numDomainID,@numWOId

			IF(@numBuildProcessId>0)
			BEGIN
					INSERT  INTO Sales_process_List_Master ( Slp_Name,
														 numdomainid,
														 pro_type,
														 numCreatedby,
														 dtCreatedon,
														 numModifedby,
														 dtModifiedOn,tintConfiguration,bitAutomaticStartTimer,bitAssigntoTeams,numOppId,numTaskValidatorId,
														 numProjectId,numWorkOrderId)
				 SELECT Slp_Name,
						numdomainid,
						pro_type,
						numCreatedby,
						dtCreatedon,
						numModifedby,
						dtModifiedOn,tintConfiguration,bitAutomaticStartTimer,bitAssigntoTeams,0,numTaskValidatorId,0,@numWOId FROM Sales_process_List_Master WHERE Slp_Id=@numBuildProcessId    
					DECLARE @numNewProcessId AS NUMERIC(18,0)=0
					SET @numNewProcessId=(SELECT SCOPE_IDENTITY())
					UPDATE WorkOrder SET numBuildProcessId=@numNewProcessId WHERE numWOId=@numWOId
					INSERT INTO StagePercentageDetails(
					numStagePercentageId, 
					tintConfiguration, 
					vcStageName, 
					numDomainId, 
					numCreatedBy, 
					bintCreatedDate, 
					numModifiedBy, 
					bintModifiedDate, 
					slp_id, 
					numAssignTo, 
					vcMileStoneName, 
					tintPercentage, 
					tinProgressPercentage, 
					dtStartDate, 
					numParentStageID, 
					intDueDays, 
					numProjectID, 
					numOppID, 
					vcDescription, 
					bitIsDueDaysUsed,
					numTeamId, 
					bitRunningDynamicMode,
					numStageOrder,
					numWorkOrderId
				)
				SELECT
					numStagePercentageId, 
					tintConfiguration, 
					vcStageName, 
					numDomainId, 
					@numUserCntID, 
					GETDATE(), 
					@numUserCntID, 
					GETDATE(), 
					@numNewProcessId, 
					numAssignTo, 
					vcMileStoneName, 
					tintPercentage, 
					tinProgressPercentage, 
					GETDATE(), 
					numParentStageID, 
					intDueDays, 
					0, 
					0, 
					vcDescription, 
					bitIsDueDaysUsed,
					numTeamId, 
					bitRunningDynamicMode,
					numStageOrder,
					@numWOId
				FROM
					StagePercentageDetails
				WHERE
					slp_id=@numBuildProcessId	

					INSERT INTO StagePercentageDetailsTask(
					numStageDetailsId, 
					vcTaskName, 
					numHours, 
					numMinutes, 
					numAssignTo, 
					numDomainID, 
					numCreatedBy, 
					dtmCreatedOn,
					numOppId,
					numProjectId,
					numParentTaskId,
					bitDefaultTask,
					bitSavedTask,
					numOrder,
					numReferenceTaskId,
					numWorkOrderId
				)
				SELECT 
					(SELECT TOP 1 SFD.numStageDetailsId FROM StagePercentageDetails AS SFD WHERE SFD.vcStageName=SP.vcStageName AND SFD.numWorkOrderId=@numWOId),
					vcTaskName,
					numHours,
					numMinutes,
					ST.numAssignTo,
					@numDomainID,
					@numUserCntID,
					GETDATE(),
					0,
					0,
					0,
					1,
					CASE WHEN SP.bitRunningDynamicMode=1 THEN 0 ELSE 1 END,
					numOrder,
					numTaskId,
					@numWOId
				FROM 
					StagePercentageDetailsTask AS ST
				LEFT JOIN
					StagePercentageDetails As SP
				ON
					ST.numStageDetailsId=SP.numStageDetailsId
				WHERE
					ISNULL(ST.numOppId,0)=0 AND ISNULL(ST.numProjectId,0)=0 AND
					ST.numStageDetailsId IN(SELECT numStageDetailsId FROM 
					StagePercentageDetails As ST
				WHERE
					ST.slp_id=@numBuildProcessId)
				ORDER BY ST.numOrder
			END


			INSERT INTO [WorkOrderDetails] 
			(
				numWOId,numItemKitID,numChildItemID,numQtyItemsReq,numWareHouseItemId,vcItemDesc,sintOrder,numQtyItemsReq_Orig,numUOMId
			)
			SELECT 
				@numWOId,numItemKitID,numItemCode,
				CAST(((DTL.numQtyItemsReq * ISNULL(dbo.fn_UOMConversion(Dtl.numUOMID,item.numItemCode,item.numDomainID,item.numBaseUnit),1)) * @numUnitHour)AS FLOAT),
				isnull((SELECT TOP 1 numWareHouseItemId FROM WareHouseItems WHERE numItemID=item.numItemCode AND numWareHouseID=@numWarehouseID ORDER BY ISNULL(numWLocationID,0),numWareHouseItemID),0),
				ISNULL(Dtl.vcItemDesc,txtItemDesc),
				ISNULL(sintOrder,0),
				DTL.numQtyItemsReq,
				Dtl.numUOMId 
			FROM 
				item                                
			INNER JOIN 
				ItemDetails Dtl 
			ON 
				numChildItemID=numItemCode
			WHERE 
				numItemKitID=@numItemCode
		END
				
		

		SET @i = @i + 1
	END	
END
GO