SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO


IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_OpportunityMaster_GetDataForEDI940')
DROP PROCEDURE dbo.USP_OpportunityMaster_GetDataForEDI940
GO
CREATE PROCEDURE [dbo].[USP_OpportunityMaster_GetDataForEDI940]
	@numDomainID NUMERIC(18,0)
	,@numOppID NUMERIC(18,0)
AS 
BEGIN
	SELECT
		OpportunityMaster.numOppID
		,OpportunityMaster.vcPOppName
		,CompanyInfo.vcCompanyName
		,Domain.vcDomainName
		,OpportunityMaster.numDomainID
		,OpportunityMaster.numDivisionID
		,CONCAT(OpportunityMaster.numDomainID,'-',OpportunityMaster.numDivisionID) AS vcDomainDivisionID
		,ISNULL(OpportunityMaster.txtComments,'') vcComments
		,ISNULL(AdditionalContactsInformation.vcFirstName,'') vcFirstName
		,ISNULL(AdditionalContactsInformation.vcLastName,'') vcLastName
		,ISNULL(AdditionalContactsInformation.numCell,'') vcPhone
		,ISNULL(OpportunityMaster.vcOppRefOrderNo,'') vcCustomerPO#
		,ISNULL((SELECT TOP 1 vcStreet FROM fn_getOPPAddressDetails(OpportunityMaster.numOppId,OpportunityMaster.numDomainID,1)),'') AS [vcBillStreet]
		,ISNULL((SELECT TOP 1 vcCity FROM fn_getOPPAddressDetails(OpportunityMaster.numOppId,OpportunityMaster.numDomainID,1)),'') AS [vcBillCity]
		,ISNULL((SELECT TOP 1 vcState FROM fn_getOPPAddressDetails(OpportunityMaster.numOppId,OpportunityMaster.numDomainID,1)),'') AS [vcBillState]
		,ISNULL((SELECT TOP 1 vcCountry FROM fn_getOPPAddressDetails(OpportunityMaster.numOppId,OpportunityMaster.numDomainID,1)),'') AS [vcBillCountry]
		,ISNULL((SELECT TOP 1 vcPostalCode FROM fn_getOPPAddressDetails(OpportunityMaster.numOppId,OpportunityMaster.numDomainID,1)),'') AS [vcBillPostalCode]
		,ISNULL((SELECT TOP 1 vcStreet FROM fn_getOPPAddressDetails(OpportunityMaster.numOppId,OpportunityMaster.numDomainID,2)),'') AS [vcShipStreet]
		,ISNULL((SELECT TOP 1 vcCity FROM fn_getOPPAddressDetails(OpportunityMaster.numOppId,OpportunityMaster.numDomainID,2)),'') AS [vcShipCity]
		,ISNULL((SELECT TOP 1 vcState FROM fn_getOPPAddressDetails(OpportunityMaster.numOppId,OpportunityMaster.numDomainID,2)),'') AS [vcShipState]
		,ISNULL((SELECT TOP 1 vcCountry FROM fn_getOPPAddressDetails(OpportunityMaster.numOppId,OpportunityMaster.numDomainID,2)),'') AS [vcShipCountry]
		,ISNULL((SELECT TOP 1 vcPostalCode FROM fn_getOPPAddressDetails(OpportunityMaster.numOppId,OpportunityMaster.numDomainID,2)),'') AS [vcShipPostalCode]
		,ISNULL((SELECT TOP 1 vcDescription FROM DivisionMasterShippingConfiguration WHERE DivisionMasterShippingConfiguration.numDivisionId=DivisionMaster.numDivisionID),'') vcShippingInstruction
		,(CASE WHEN ISNULL(DivisionMaster.bitShippingLabelRequired,0) = 1 THEN 'Yes' ELSE 'No' END) bitShippingLabelRequired
	FROM
		OpportunityMaster
	INNER JOIN
		Domain
	ON
		OpportunityMaster.numDomainID = Domain.numDomainId
	INNER JOIN
		DivisionMaster
	ON
		OpportunityMaster.numDivisionId = DivisionMaster.numDivisionID
	INNER JOIN
		CompanyInfo
	ON
		DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
	LEFT JOIN
		AdditionalContactsInformation
	ON
		OpportunityMaster.numContactId = AdditionalContactsInformation.numContactId
	WHERE
		OpportunityMaster.numDomainId=@numDomainID
		AND OpportunityMaster.numOppId = @numOppID


	SELECT
		Item.vcItemName
		,CASE 
			WHEN ISNULL(Item.numItemGroup,0) > 0 AND ISNULL(Item.bitMatrix,0) = 0
			THEN 
				(CASE WHEN LEN(ISNULL(WareHouseItems.vcWHSKU,'')) > 0 THEN WareHouseItems.vcWHSKU ELSE ISNULL(Item.vcSKU,'') END)
			ELSE
				(CASE WHEN LEN(ISNULL(OpportunityItems.vcSKU,'')) > 0 THEN OpportunityItems.vcSKU ELSE ISNULL(Item.vcSKU,'') END)
		END vcSKU
		,CAST((dbo.fn_UOMConversion(ISNULL(Item.numBaseUnit, 0),OpportunityItems.numItemCode,@numDomainID, ISNULL(OpportunityItems.numUOMId, 0))* OpportunityItems.numUnitHour) AS NUMERIC(18, 2)) AS numUnits
		,ISNULL(UOM.vcUnitName,'Units') vcUOM
		,(CASE 
			WHEN charItemType='P' THEN 'Inventory Item'
			WHEN charItemType='S' THEN 'Service' 
			WHEN charItemType='A' THEN 'Accessory' 
			WHEN charItemType='N' THEN 'Non-Inventory Item' 
		END) AS vcItemType
		,ISNULL(OpportunityItems.vcNotes,'') vcNotes
		,OpportunityItems.numoppitemtCode AS 'BizOppItemID'
	FROM
		OpportunityItems
	INNER JOIN
		Item
	ON
		OpportunityItems.numItemCode = Item.numItemCode
	LEFT JOIN
		WareHouseItems
	ON
		OpportunityItems.numWarehouseItmsID = WareHouseItems.numWareHouseItemID
	LEFT JOIN 
		UOM 
	ON
		OpportunityItems.numUOMId = UOM.numUOMId
	WHERE
		numOppId = @numOppID
		AND ISNULL(OpportunityItems.bitDropShip,0) = 0
END
GO