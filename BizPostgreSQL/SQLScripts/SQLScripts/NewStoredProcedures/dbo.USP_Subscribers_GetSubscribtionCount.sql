GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
       
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_Subscribers_GetSubscribtionCount')
DROP PROCEDURE USP_Subscribers_GetSubscribtionCount
GO
CREATE PROCEDURE [dbo].[USP_Subscribers_GetSubscribtionCount]
(       
	@numDomainID NUMERIC(18,0)
)
AS
BEGIN
	SELECT 
		CONCAT(ISNULL((SELECT COUNT(*) FROM UserMaster LEFT JOIN AuthenticationGroupMaster ON UserMaster.numGroupID=AuthenticationGroupMaster.numGroupID WHERE UserMaster.numDomainID=@numDomainID AND ISNULL(bitActivateFlag,0)=1 AND (AuthenticationGroupMaster.tintGroupType=1 OR AuthenticationGroupMaster.tintGroupType IS NULL)),0),' used / ',ISNULL(intNoofUsersSubscribed,0) - ISNULL((SELECT COUNT(*) FROM UserMaster LEFT JOIN AuthenticationGroupMaster ON UserMaster.numGroupID=AuthenticationGroupMaster.numGroupID WHERE UserMaster.numDomainID=@numDomainID AND ISNULL(bitActivateFlag,0)=1 AND (AuthenticationGroupMaster.tintGroupType=1 OR AuthenticationGroupMaster.tintGroupType IS NULL)),0),' remaining') vcFullUsers
		,CONCAT(ISNULL((SELECT COUNT(*) FROM UserMaster LEFT JOIN AuthenticationGroupMaster ON UserMaster.numGroupID=AuthenticationGroupMaster.numGroupID WHERE UserMaster.numDomainID=@numDomainID AND ISNULL(bitActivateFlag,0)=1 AND AuthenticationGroupMaster.tintGroupType=4),0),' used / ',ISNULL(intNoofLimitedAccessUsers,0) - ISNULL((SELECT COUNT(*) FROM UserMaster LEFT JOIN AuthenticationGroupMaster ON UserMaster.numGroupID=AuthenticationGroupMaster.numGroupID WHERE UserMaster.numDomainID=@numDomainID AND ISNULL(bitActivateFlag,0)=1 AND AuthenticationGroupMaster.tintGroupType=4),0),' remaining') vcLimitedAccessUsers
		,CONCAT(ISNULL((SELECT COUNT(*) FROM ExtranetAccountsDtl WHERE numDomainID=@numDomainID AND ISNULL(bitPartnerAccess,0)=1),0),' used / ',ISNULL(intNoofBusinessPortalUsers,0) - ISNULL((SELECT COUNT(*) FROM ExtranetAccountsDtl WHERE numDomainID=@numDomainID AND ISNULL(bitPartnerAccess,0)=1),0),' remaining') vcBusinessPortalUsers
	FROM
		Subscribers
	WHERE
		numTargetDomainID=@numDomainID
END
GO