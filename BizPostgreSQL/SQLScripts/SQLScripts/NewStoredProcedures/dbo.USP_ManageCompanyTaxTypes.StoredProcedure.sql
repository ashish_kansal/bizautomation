

--Created By Anoop Jayaraj
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ManageComapnyTaxTypes')
DROP PROCEDURE USP_ManageComapnyTaxTypes
GO
CREATE PROCEDURE USP_ManageComapnyTaxTypes
@numDivisionID as numeric(9),
@strCompTaxTypes as varchar(1000)
as

declare @hDoc  int    
delete from DivisionTaxTypes where numDivisionID=@numDivisionID    
EXEC sp_xml_preparedocument @hDoc OUTPUT, @strCompTaxTypes    
insert into DivisionTaxTypes (numDivisionID,numTaxItemID,bitApplicable)     
 select @numDivisionID,X.numTaxItemID,X.bitApplicable  from(SELECT *FROM OPENXML (@hDoc,'/NewDataSet/Table',2)    
 WITH ( numTaxItemID numeric(9),    
  bitApplicable bit    
  ))X    
    
 EXEC sp_xml_removedocument @hDoc  


