SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='Activity_GoogleEventId')
DROP PROCEDURE Activity_GoogleEventId
GO
CREATE PROCEDURE [dbo].[Activity_GoogleEventId]  
 @ActivityID int, 
 @GoogleEventId varchar(MAX) 
AS  
BEGIN  
    UPDATE Activity SET GoogleEventId=@GoogleEventId WHERE ActivityID=@ActivityID
END
GO
