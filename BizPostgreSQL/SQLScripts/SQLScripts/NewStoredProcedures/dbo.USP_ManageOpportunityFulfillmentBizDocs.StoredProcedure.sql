GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_ManageOpportunityFulfillmentBizDocs' ) 
    DROP PROCEDURE USP_ManageOpportunityFulfillmentBizDocs
GO
CREATE PROCEDURE USP_ManageOpportunityFulfillmentBizDocs
    @numDomainID AS NUMERIC,
    @strItems TEXT,
    @tintMode AS TINYINT
AS 
BEGIN
    IF @tintMode = 0 OR @tintMode = 2
        BEGIN
			--If Fulfillment Order bizdocs is not added
			IF NOT EXISTS (SELECT numBizDocTypeID FROM OpportunityFulfillmentBizDocs WHERE numDomainID=@numDomainID)
			BEGIN
				INSERT INTO dbo.OpportunityFulfillmentBizDocs (numBizDocTypeID,numDomainID,ItemType) 
					select 296,@numDomainID,'P' UNION
					select 296,@numDomainID,'N' UNION
					select 296,@numDomainID,'S' 
			END
			
				IF @tintMode = 0
				BEGIN
				--Fetch Fulfillment Order bizdocs and all bizdocs for domain
				SELECT LD.numListItemID AS numBizDocTypeID,vcData AS vcBizDocType
						FROM dbo.ListDetails LD 
						WHERE LD.numListID=27 and ((LD.constFlag=1 AND LD.numListItemID=296) or (LD.constFlag=0 AND LD.numDomainID=@numDomainID))
						ORDER BY LD.numListItemID
				END
				ELSE IF @tintMode = 2
				BEGIN
				SELECT LD.numListItemID AS numBizDocTypeID,vcData AS vcBizDocType,
					  CASE WHEN ISNULL(FO.numBizDocTypeID,0)>0 THEN Cast(1 AS BIT) ELSE Cast(0 AS BIT) END AS bitIsFulfillmentOrder,
					  ISNULL(FO.ItemType,'') AS ItemType
						FROM dbo.ListDetails LD JOIN OpportunityFulfillmentBizDocs FO ON LD.numListItemID=FO.numBizDocTypeID 
						AND FO.numDomainID=@numDomainID
						WHERE LD.numListID=27 and ((LD.constFlag=1 AND LD.numListItemID=296) or (LD.constFlag=0 AND LD.numDomainID=@numDomainID))
						ORDER BY LD.numListItemID
				END		
        END
	ELSE IF @tintMode = 1 
        BEGIN
            DELETE FROM OpportunityFulfillmentBizDocs WHERE numDomainID=@numDomainID

            DECLARE @hDocItem INT
            IF CONVERT(VARCHAR(10), @strItems) <> '' 
                BEGIN
                    EXEC sp_xml_preparedocument @hDocItem OUTPUT, @strItems
            
                    INSERT  INTO dbo.OpportunityFulfillmentBizDocs (numDomainID,numBizDocTypeID,ItemType)
                            SELECT  @numDomainID,
                                    X.numBizDocTypeID,
                                    X.ItemType
                            FROM    ( SELECT    *
                                      FROM      OPENXML (@hDocItem, '/NewDataSet/Table', 2)
                                                WITH ( numBizDocTypeID NUMERIC(18),ItemType CHAR(2))
                                    ) X
                    EXEC sp_xml_removedocument @hDocItem
                END
            SELECT  ''
        END	
 END



