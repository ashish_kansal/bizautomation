/****** Object:  StoredProcedure [dbo].[USP_GetInvoiceList]    Script Date: 07/26/2008 16:17:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by prasanta Pradhan                  
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetRMAListInEcommerce')
DROP PROCEDURE USP_GetRMAListInEcommerce
GO
CREATE PROCEDURE [dbo].[USP_GetRMAListInEcommerce]                            
 @numDivisionID as numeric(9)=0 ,
 @numDomainID AS NUMERIC(9),
 @CurrentPage INT,
 @PageSize INT,
 @UserId NUMERIC(18,0)
as                        
 
 DECLARE @firstRec INT=0
 DECLARE @lastRec INT=0

	SET @firstRec = ( @CurrentPage - 1 ) * @PageSize
    SET @lastRec = ( @CurrentPage * @PageSize + 1 )
	SELECT  * FROM (
	SELECT ROW_NUMBER() OVER(ORDER BY numReturnHeaderID) AS Rownumber,* FROM (
	SELECT distinct
		RH.vcRMA,
		(select TOP 1 vcData from ListDetails WHERE numListItemID=RH.numReturnReason) AS numReturnReason,
		(select TOP 1 vcData from ListDetails WHERE numListItemID=RH.numReturnStatus) AS numReturnStatus,
		(SELECT SUBSTRING((SELECT '$^$' + CAST(I.vcItemName AS VARCHAR(18)) +'#^#'+ CAST(RI.numUnitHour AS VARCHAR(18))
				FROM ReturnItems AS RI LEFT JOIN Item AS I ON RI.numItemCode=I.numItemCode WHERE numReturnHeaderID=RH.numReturnHeaderID FOR XML PATH('')),4,200000)) AS vcProducts,
		RH.numReturnHeaderID
	FROM 
		ReturnHeader AS RH
	WHERE 
		RH.numDivisionID=@numDivisionID AND 
		RH.numContactId=@UserId
		) AS K) AS T
		WHERE T.Rownumber>@firstRec AND T.Rownumber<@lastRec

	select 
		DISTINCT COUNT(RH.numReturnHeaderID)
	FROM 
		ReturnHeader AS RH
	WHERE 
		RH.numDivisionID=@numDivisionID AND 
		RH.numContactId=@UserId




