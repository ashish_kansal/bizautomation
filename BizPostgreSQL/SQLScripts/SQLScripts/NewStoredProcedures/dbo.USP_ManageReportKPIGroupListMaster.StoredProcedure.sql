GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ManageReportKPIGroupListMaster')
DROP PROCEDURE USP_ManageReportKPIGroupListMaster
GO
CREATE PROCEDURE [dbo].[USP_ManageReportKPIGroupListMaster] 
	@numReportKPIGroupID NUMERIC(18,0) OUTPUT,
	@numDomainID NUMERIC(18,0),	                                       
    @vcKPIGroupName varchar(200),
    @vcKPIGroupDescription varchar(500),
    @tintKPIGroupReportType tinyint,
    @strText TEXT = '',
    @tintMode AS TINYINT, --0:Insert/Update KPIGroupList 1:Insert KPIGroupDetail 2: Delete KPIGroupList 3: Delete KPIGroupDetail
    @numReportID AS NUMERIC(18,0)
as                 


IF @tintMode=0 --0:Insert/Update KPIGroupList
BEGIN
	IF @numReportKPIGroupID=0
	BEGIN
		INSERT INTO dbo.ReportKPIGroupListMaster (numDomainID,vcKPIGroupName,vcKPIGroupDescription,tintKPIGroupReportType
		) VALUES ( @numDomainID, @vcKPIGroupName, @vcKPIGroupDescription, @tintKPIGroupReportType) 
		    
		SET @numReportKPIGroupID=@@identity
	END
	ELSE
	BEGIN
		UPDATE [dbo].[ReportKPIGroupListMaster]
		SET  vcKPIGroupName=@vcKPIGroupName,vcKPIGroupDescription=@vcKPIGroupDescription
		WHERE  [numDomainID] = @numDomainID AND [numReportKPIGroupID] = @numReportKPIGroupID
	END
END
ELSE IF @tintMode=1 --1:Insert KPIGroupDetail
BEGIN
	IF NOT EXISTS(SELECT 1 FROM ReportKPIGroupDetailList WHERE numReportKPIGroupID=@numReportKPIGroupID AND numReportID=@numReportID)
	BEGIN
		INSERT INTO dbo.ReportKPIGroupDetailList (numReportKPIGroupID,numReportID) 
			SELECT @numReportKPIGroupID,@numReportID
	END		
END
ELSE IF @tintMode=2 --2: Delete KPIGroupList
BEGIN
	DELETE FROM ReportKPIGroupDetailList WHERE [numReportKPIGroupID] = @numReportKPIGroupID
	
	DELETE FROM ReportKPIGroupListMaster WHERE [numDomainID] = @numDomainID AND [numReportKPIGroupID] = @numReportKPIGroupID
END
ELSE IF @tintMode=3 --3: Delete KPIGroupDetail
BEGIN
	DELETE FROM ReportKPIGroupDetailList WHERE [numReportKPIGroupID] = @numReportKPIGroupID AND numReportID=@numReportID
END

	
	


	
