GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_BizFormWizardMasterConfiguration_Save' ) 
    DROP PROCEDURE USP_BizFormWizardMasterConfiguration_Save
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author: Sandeep Patel
-- Create date: 25 July 2014
-- Description: Saves form configuration
-- =============================================
CREATE PROCEDURE USP_BizFormWizardMasterConfiguration_Save
	@numDomainID NUMERIC(18,0),
	@numFormID NUMERIC(18,0),
	@numGroupID NUMERIC(18,0),
	@numRelCntType NUMERIC(18,0),
	@tintPageType TINYINT,
	@pageID NUMERIC(18,0),
	@bitGridConfiguration BIT = 0,
	@strFomFld as TEXT,
	@numFormFieldGroupId AS NUMERIC(18,0)=0
AS
BEGIN
	SET NOCOUNT ON;

	DELETE FROM 
		BizFormWizardMasterConfiguration 
	WHERE 
		numDomainID = @numDomainID AND
		numFormId = @numFormId AND 
		numGroupID = @numGroupID AND
		numRelCntType = @numRelCntType AND
		tintPageType = @tintPageType AND
		bitGridConfiguration = @bitGridConfiguration AND
		ISNULL(numFormFieldGroupId,0)=ISNULL(@numFormFieldGroupId,0)

	DELETE FROM 
		DycFormConfigurationDetails 
	WHERE 
		numFormId=@numFormId 
		AND numDomainID=@numDomainID 
		AND numUserCntID IN (SELECT numUserDetailID FROM UserMaster WHERE numDomainID =  @numDomainID AND numGroupID = @numGroupID)
		AND ISNULL(numRelCntType,0) = ISNULL(@numRelCntType,0)
		AND ISNULL(tintPageType,0) = ISNULL(@tintPageType,0) AND
		ISNULL(numFormFieldGroupId,0)=ISNULL(@numFormFieldGroupId,0)

    DECLARE @hDoc1 int                                                        
	EXEC sp_xml_preparedocument @hDoc1 OUTPUT, @strFomFld         
	
	SELECT numUserDetailID
		INTO #userDetails
	FROM 
		UserMaster
	WHERE 
		numDomainID =  @numDomainID AND numGroupID = @numGroupID

	ALTER TABLE #userDetails
    ADD [ID] integer identity not null;
	DECLARE @totalRecordCount AS INT=0
	SET @totalRecordCount=(Select Count(*) From #userDetails)
	DECLARE @SelectedId as INT = 1;
	While ( @SelectedId<= @totalRecordCount)
	BEGIN
		DECLARE @numUserCntId AS NUMERIC(18,0) = 0
		SET @numUserCntId = (Select TOP 1 numUserDetailID From #userDetails where [ID] =@SelectedId)
		INSERT INTO DycFormConfigurationDetails
			(
				numFormID,
				numFieldID,
				intColumnNum,
				intRowNum,
				numDomainID,
				numAuthGroupID,
				numRelCntType,
				tintPageType,
				bitCustom,
				numUserCntID,
				numFormFieldGroupId,
				bitDefaultByAdmin
			)
		SELECT
			@numFormId,
			replace(replace(X.numFormFieldId,'R',''),'C',''),                                                        
			X.intColumnNum,                                
			X.intRowNum,
			@numDomainId,
			@numGroupID,
			@numRelCntType,
			@tintPageType,
			(
			CASE 
				WHEN CHARINDEX('C',X.numFormFieldId) > 0 THEN 1 
				ELSE 0 
			END
			),
			@numUserCntId,
			@numFormFieldGroupId,
			1
		FROM
			(
				SELECT 
					* 
				FROM 
					OPENXML (@hDoc1,'/FormFields/FormField',2)                                                        
				WITH 
					(numFormFieldId varchar(20),                                                        
					vcFieldType char(1),                                                        
					intColumnNum int,                                
					intRowNum int,
					boolAOIField bit
					)
			)X  
		SET @SelectedId = @SelectedId +1
	END
	INSERT INTO BizFormWizardMasterConfiguration
		(
			numFormID,
			numFieldID,
			intColumnNum,
			intRowNum,
			numDomainID,
			numGroupID,
			numRelCntType,
			tintPageType,
			bitCustom,
			bitGridConfiguration,
			numFormFieldGroupId
		)
	SELECT
        @numFormId,
		replace(replace(X.numFormFieldId,'R',''),'C',''),                                                        
		X.intColumnNum,                                
		X.intRowNum,
		@numDomainId,
		@numGroupID,
		@numRelCntType,
		@tintPageType,
		(
		CASE 
			WHEN CHARINDEX('C',X.numFormFieldId) > 0 THEN 1 
			ELSE 0 
		END
		),
		@bitGridConfiguration,
		@numFormFieldGroupId
	FROM
		(
			SELECT 
				* 
			FROM 
				OPENXML (@hDoc1,'/FormFields/FormField',2)                                                        
			WITH 
				(numFormFieldId varchar(20),                                                        
				vcFieldType char(1),                                                        
				intColumnNum int,                                
				intRowNum int,
				boolAOIField bit
				)
		)X      
	
	EXEC sp_xml_removedocument @hDoc1
	
END
GO


