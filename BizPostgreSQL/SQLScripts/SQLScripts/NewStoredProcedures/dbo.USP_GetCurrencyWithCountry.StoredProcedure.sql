GO
/****** Object:  StoredProcedure [dbo].[USP_GetCurrencyWithCountry]    Script Date: 05/16/2011 09:07:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetCurrencyWithCountry')
DROP PROCEDURE USP_GetCurrencyWithCountry

GO
Create PROCEDURE [dbo].[USP_GetCurrencyWithCountry]
@numDomanID as numeric(9)
as


Select CAST(C.numCurrencyID AS VARCHAR(10)) + ':' + C.varCurrSymbol + ':' + CAST(C.fltExchangeRate AS VARCHAR(20)) AS vcCurrencDetail,C.numCurrencyID,
D.vcData + ' - ' + C.chrCurrency  AS vcCurrencyDesc
from Currency C JOIN ListDetails D ON C.numCountryId=D.numListItemID
where C.numDomainID=@numDomanID and C.bitEnabled=1 AND D.numDomainID=@numDomanID AND D.numListID=40
order by bitEnabled desc,vcCurrencyDesc Asc