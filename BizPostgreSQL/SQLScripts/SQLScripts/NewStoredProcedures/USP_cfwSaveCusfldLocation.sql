IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_cfwSaveCusfldLocation')
DROP PROCEDURE USP_cfwSaveCusfldLocation
GO
CREATE PROCEDURE [dbo].[USP_cfwSaveCusfldLocation]    
@numDomainID as numeric(9)=0 ,   
@strDetails as NVARCHAR(MAX)=''
AS
BEGIN
	CREATE TABLE #TempData
	(
		ItemsData Varchar(50)
	)
	CREATE TABLE #TempDataLocation
	(
		fld_id bigint,
		vcitemsLocation VARCHAR(500)
	)
	ALTER TABLE #TempData ADD ID INT IDENTITY
	ALTER TABLE #TempDataLocation ADD ID INT IDENTITY
	INSERT INTO #TempData(ItemsData)
	SELECT Items FROM Split(@strDetails,'_') WHERE Items<>''

	SELECT * FROM #TempData
	DECLARE @i INT =1
	DECLARE @RecordCount INT 
	DECLARE @vcTempData varchar(MAX) 
	SET @RecordCount = (SELECT COUNT(*) FROM #TempData)
	WHILE(@i<=@RecordCount)
	BEGIN
		SELECT @vcTempData=ItemsData FROM #TempData WHERE ID=@i
		UPDATE CFW_Fld_Master SET vcItemsLocation=(SELECT Items FROM Split(@vcTempData,'#') WHERE Items<>''  ORDER BY(SELECT NULL) OFFSET 1 ROWS
FETCH NEXT 1 ROWS ONLY) WHERE fld_id=(SELECT Items FROM Split(@vcTempData,'#') WHERE Items<>''  ORDER BY(SELECT NULL) OFFSET 0 ROWS
FETCH NEXT 1 ROWS ONLY)
		SET @i = @i+1
	END

	DROP TABLE #TempData
END