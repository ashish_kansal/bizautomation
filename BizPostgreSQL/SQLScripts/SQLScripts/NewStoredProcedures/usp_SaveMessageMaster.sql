
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_SaveMessageMaster')
DROP PROCEDURE usp_SaveMessageMaster
GO
CREATE PROCEDURE [dbo].[usp_SaveMessageMaster]                                   
(
@numMessageId as NUMERIC(18,0)=0 output, 
@numTopicId as NUMERIC(18,0)=0, 
@numParentMessageId as NUMERIC(18,0)=0, 
@intRecordType as int=0, 
@numRecordId as NUMERIC(18,0)=0, 
@vcMessage as varchar(MAX)='', 
@numCreatedBy as NUMERIC(18,0)=0, 
@numUpdatedBy as NUMERIC(18,0)=0, 
@bitIsInternal as bit=0, 
@numDomainId NUMERIC(18,0)=0,
@status AS VARCHAR(100) OUTPUT
)                                    
AS          
SET NOCOUNT ON
	IF @numMessageId = 0     
		BEGIN      
			INSERT INTO MessageMaster
			(
				numTopicId, numParentMessageId, intRecordType, numRecordId, vcMessage, numCreatedBy, dtmCreatedOn, numUpdatedBy, dtmUpdatedOn, bitIsInternal, numDomainId
			)
			VALUES
			(
				@numTopicId, @numParentMessageId, @intRecordType, @numRecordId, @vcMessage, @numCreatedBy, GETDATE(), @numUpdatedBy, GETDATE(), @bitIsInternal, @numDomainId
			)
			SELECT @numMessageId = SCOPE_IDENTITY()
			SET @status='1'
		END      
		ELSE      
		BEGIN      
			UPDATE 
				MessageMaster 
			SET 
				numTopicId=@numTopicId, 
				numParentMessageId=@numParentMessageId, 
				intRecordType=@intRecordType, 
				numRecordId=@numRecordId, 
				vcMessage=@vcMessage, 
				numUpdatedBy=@numUpdatedBy, 
				dtmUpdatedOn=GETDATE(), 
				bitIsInternal=@bitIsInternal, 
				numDomainId=@numDomainId
			WHERE 
				numMessageId=@numMessageId 
				AND numDomainID=@numDomainID  
         SET @status='3'
		END
GO