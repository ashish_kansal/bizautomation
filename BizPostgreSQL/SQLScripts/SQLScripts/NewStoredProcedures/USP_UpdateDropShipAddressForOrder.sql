GO
/****** Created by Priya (05 Oct 2018)
		Save Ship To Address on Drop Ship Check   **********/
						 
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_updatedropshipaddressfororder')
DROP PROCEDURE usp_updatedropshipaddressfororder
GO
CREATE PROCEDURE [dbo].[USP_UpdateDropShipAddressForOrder]        
@numOppID as numeric(9)=0,      
@tintShipToType as tinyint,                       
@vcShipStreet as varchar(50),                  
@vcShipCity as varchar(50),                  
@numShipState as numeric(9),                  
@vcShipPostCode as varchar(15),                  
@numShipCountry as numeric(9), 
@bitAltShippingContact BIT = 0,
@vcAltShippingContact VARCHAR(200) = '',
@numShipToAddressID AS NUMERIC(18,0) = 0
AS        
BEGIN     
	
	UPDATE 
		OpportunityMaster 
	SET 
		tintShipToType=@tintShipToType,
		numShipToAddressID = @numShipToAddressID
	WHERE 
		numOppID=@numOppID  	           
	        
	IF (@tintShipToType = 2  AND ISNULL(@numShipToAddressID,0) = 0)
	BEGIN
		IF (SELECT COUNT(*) FROM OpportunityAddress WHERE [numOppID]=@numOppID) = 0
		BEGIN
			INSERT  INTO OpportunityAddress ( numOppID )
			VALUES  ( @numOppID )      
		END
	END	
    
	IF @tintShipToType = 2 AND ISNULL(@numShipToAddressID,0) = 0
	BEGIN
		UPDATE  OpportunityAddress
		SET    	vcShipStreet = @vcShipStreet,
				vcShipCity = @vcShipCity,
				numShipState = @numShipState,
				vcShipPostCode = @vcShipPostCode,
				numShipCountry = @numShipCountry,
				bitAltShippingContact=@bitAltShippingContact,
				vcAltShippingContact=@vcAltShippingContact
		WHERE   numOppID = @numOppID
	END

	DECLARE @numDomainID NUMERIC(18,0)
	DECLARE @numDivisionID NUMERIC(18,0)
	DECLARE @tintOppType TINYINT

	SELECT 
		@numDomainID=numDomainID
		,@numDivisionID=numDivisionID
		,@tintOppType=tintOppType
	FROM 
		OpportunityMaster 
	WHERE 
		numOppID=@numOppId

	DELETE FROM OpportunityMasterTaxItems WHERE numOppId=@numOppID

	--INSERT TAX FOR DIVISION   
	IF @tintOppType=1 OR (@tintOppType=2 and (select isnull(bitPurchaseTaxCredit,0) from Domain where numDomainId=@numDomainId)=1)
	BEGIN
		INSERT INTO dbo.OpportunityMasterTaxItems 
		(
			numOppId,
			numTaxItemID,
			fltPercentage,
			tintTaxType,
			numTaxID
		) 
		SELECT 
			@numOppID,
			TI.numTaxItemID,
			TEMPTax.decTaxValue,
			TEMPTax.tintTaxType,
			0
		FROM 
			TaxItems TI 
		JOIN 
			DivisionTaxTypes DTT 
		ON 
			TI.numTaxItemID = DTT.numTaxItemID
		CROSS APPLY
		(
			SELECT
				decTaxValue,
				tintTaxType
			FROM
				dbo.fn_CalItemTaxAmt(@numDomainID,@numDivisionID,0,TI.numTaxItemID,@numOppId,0,NULL)
		) AS TEMPTax
		WHERE 
			DTT.numDivisionID=@numDivisionId 
			AND DTT.bitApplicable=1
		UNION 
		SELECT 
			@numOppID,
			0,
			TEMPTax.decTaxValue,
			TEMPTax.tintTaxType,
			0
		FROM 
			dbo.DivisionMaster 
		CROSS APPLY
		(
			SELECT
				decTaxValue,
				tintTaxType
			FROM
				dbo.fn_CalItemTaxAmt(@numDomainID,@numDivisionID,0,0,@numOppId,1,NULL)
		) AS TEMPTax
		WHERE 
			bitNoTax=0 
			AND numDivisionID=@numDivisionID	
		UNION 
		SELECT
			@numOppId
			,1
			,decTaxValue
			,tintTaxType
			,numTaxID
		FROM
			dbo.fn_CalItemTaxAmt(@numDomainID,@numDivisionID,0,1,@numOppId,1,NULL)		
	END	
	
END
GO


