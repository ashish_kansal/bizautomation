GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_AdvancedSearchItems')
DROP PROCEDURE USP_AdvancedSearchItems
GO
CREATE PROCEDURE USP_AdvancedSearchItems
@WhereCondition as varchar(4000)='',                            
@numDomainID as numeric(9)=0,
@numUserCntID NUMERIC(18,0),                         
@CurrentPage int,
@PageSize int,                                                                    
@TotRecs int output,                                                                                                             
@columnSortOrder as Varchar(10),                              
@ColumnSearch as varchar(100)='',                           
@ColumnName as varchar(50)='',                             
@GetAll as bit,                              
@SortCharacter as char(1),                        
@SortColumnName as varchar(50)='',        
@bitMassUpdate as bit,        
@LookTable as varchar(10)='' ,        
@strMassUpdate as varchar(2000)='',
@vcDisplayColumns VARCHAR(MAX)
AS 
BEGIN
 
 
   
IF (@SortCharacter <> '0' AND @SortCharacter <> '')
  SET @ColumnSearch = @SortCharacter
CREATE TABLE #temptable
  (
  id           INT   IDENTITY   PRIMARY KEY,
  numItemID VARCHAR(15))
  
------------Declaration---------------------  
DECLARE  @strSql           VARCHAR(8000),
         @numMaxFieldId NUMERIC(9),
         @from         NVARCHAR(1000),
         @Where         NVARCHAR(4000),
         @OrderBy       NVARCHAR(1000),
         @GroupBy       NVARCHAR(1000),
         @SelectFields   NVARCHAR(4000),
         @InneJoinOn    NVARCHAR(1000)
         
 DECLARE @tintOrder AS TINYINT,
		 @vcFormFieldName AS VARCHAR(50),
		 @vcListItemType AS VARCHAR(3),
		 @vcListItemType1 AS VARCHAR(3),
		 @vcAssociatedControlType VARCHAR(20),
		 @numListID AS NUMERIC(9),
		 @vcDbColumnName VARCHAR(30),
		 @vcLookBackTableName VARCHAR(50),
		 @WCondition VARCHAR(1000)
SET @InneJoinOn = ''
SET @OrderBy = ''
SET @Where = ''
SET @from = ' FROM 
				Item I 
			LEFT JOIN dbo.Vendor V ON I.numDomainID = V.numDomainID AND I.numItemCode = V.numItemCode AND I.numVendorID = V.numVendorID
			LEFT JOIN DivisionMaster DM ON V.numVendorID = DM.numDivisionID 
			LEFt JOIN AdditionalContactsinformation ADC on ADC.numDivisionID = DM.numDomainID AND ISNULL(ADC.bitPrimaryContact,0)=1
			LEFT JOIN CompanyInfo C ON DM.numCompanyID = C.numCompanyID
			OUTER APPLY
			(
				SELECT
					SUM(numOnHand) AS numOnHand,
					SUM(numBackOrder) AS numBackOrder,
					SUM(numOnOrder) AS numOnOrder,
					SUM(numAllocation) AS numAllocation,
					SUM(numReorder) AS numReorder
				FROM
					WareHouseItems
				WHERE
					numItemID = I.numItemCode
			) WI '
SET @GroupBy = ''
SET @SelectFields = ''  
SET @WCondition = ''


	
--	SELECT I.numItemCode,* FROM item I 
--	LEFT JOIN ItemGroups IG ON I.numDomainID = IG.numDomainID AND I.numItemGroup = IG.numItemGroupID
--	LEFT JOIN dbo.Vendor V ON I.numDomainID = V.numDomainID AND I.numItemCode = V.numItemCode --AND I.numVendorID = V.numVendorID
	
	
SET @Where = @Where + ' WHERE I.charItemType <>''A'' and I.numDomainID=' + convert(varchar(15),@numDomainID)
if (@ColumnSearch<>'') SET @Where = @Where + ' and I.vcItemName like ''' + @ColumnSearch + '%'''

CREATE TABLE #temp(ItemID INT PRIMARY KEY)
IF CHARINDEX('SplitString',@WhereCondition) > 0
	BEGIN
		DECLARE @strTemp AS NVARCHAR(MAX)
		SET @WhereCondition	= REPLACE(@WhereCondition,' AND I.numItemCode IN ','')
		SET @WhereCondition = RIGHT(@WhereCondition,LEN(@WhereCondition) - 1)
		SET @WhereCondition = LEFT(@WhereCondition,LEN(@WhereCondition) - 1)
		
		SET @strTemp = 'INSERT INTO #temp ' + @WhereCondition	
		PRINT @strTemp
		EXEC SP_EXECUTESQL @strTemp
		
		set @InneJoinOn= @InneJoinOn + ' JOIN #temp SP ON SP.ItemID = I.numItemCode '
	END
ELSE
	BEGIN
		SET @Where = @Where + @WhereCondition	
	END

	
	
	
set @tintOrder=0
set @WhereCondition =''
set @strSql='SELECT  I.numItemCode '

	DECLARE @TEMPSelectedColumns TABLE
	(
		numFieldID NUMERIC(18,0)
		,bitCustomField BIT
		,tintOrder INT
		,intColumnWidth FLOAT
	)
	DECLARE @vcLocationID VARCHAR(100) = '0'
	SELECT @vcLocationID =isnull(vcLocationID,'0') from dynamicFormMaster where numFormID=29   

	-- IF USER HAS SELECTED ITS OWN DISPLAY COLUMNS THEN USE IT
	IF LEN(ISNULL(@vcDisplayColumns,'')) > 0
	BEGIN
		DECLARE @TempIDs TABLE
		(
			ID INT IDENTITY(1,1),
			vcFieldID VARCHAR(300)
		)

		INSERT INTO @TempIDs
		(
			vcFieldID
		)
		SELECT 
			OutParam 
		FROM 
			SplitString(@vcDisplayColumns,',')

		INSERT INTO @TEMPSelectedColumns
		(
			numFieldID,
			bitCustomField,
			tintOrder,
			intColumnWidth
		)
		SELECT
			numFormFieldID
			,bitCustomField
			,(SELECT (ID-1) FROM @TempIDs T3 WHERE T3.vcFieldID = TEMP.vcFieldID)
			,0
		FROM
		(
			SELECT  
				numFieldID as numFormFieldID,
				0 bitCustomField,
				CONCAT(numFieldID,'~0') vcFieldID
			FROM    
				View_DynamicDefaultColumns
			WHERE   
				numFormID = 29
				AND bitInResults = 1
				AND bitDeleted = 0 
				AND numDomainID = @numDomainID
			UNION 
			SELECT  
				c.Fld_id AS numFormFieldID
				,1 bitCustomField
				,CONCAT(Fld_id,'~1') vcFieldID
			FROM    
				CFW_Fld_Master C 
			LEFT JOIN 
				CFW_Validation V ON V.numFieldID = C.Fld_id
			JOIN 
				CFW_Loc_Master L on C.GRP_ID=L.Loc_Id
			WHERE   
				C.numDomainID = @numDomainID
				AND GRP_ID IN (SELECT ID FROM dbo.SplitIDs(@vcLocationID,','))
		) TEMP
		WHERE
			vcFieldID IN (SELECT vcFieldID FROM @TempIDs)
	END

	-- IF USER SELECTED DISPLAY COLUMNS IS NOT AVAILABLE THAN GET COLUMNS CONFIGURED FROM SEARCH RESULT GRID
	IF (SELECT COUNT(*) FROM @TEMPSelectedColumns) = 0
	BEGIN
		INSERT INTO @TEMPSelectedColumns
		(
			numFieldID,
			bitCustomField,
			tintOrder,
			intColumnWidth
		)
		SELECT
			numFormFieldID
			,bitCustomField
			,tintOrder
			,intColumnWidth
		FROM
		(
			SELECT  
				A.numFormFieldID,
				0 bitCustomField,
				A.tintOrder,
				A.intColumnWidth
			FROM    AdvSerViewConf A
					JOIN View_DynamicDefaultColumns D ON D.numFieldID = A.numFormFieldID
			WHERE   A.numFormID = 29
					AND D.bitInResults = 1
					AND D.bitDeleted = 0
					AND D.numDomainID = @numDomainID 
					AND D.numFormID = 29
					AND A.numDomainID = @numDomainID
					AND A.numUserCntID=@numUserCntID
					AND ISNULL(A.bitCustom,0) = 0
			UNION
			SELECT  
				A.numFormFieldID,
				1 bitCustomField,
				A.tintOrder,
				A.intColumnWidth
			FROM    AdvSerViewConf A
					JOIN CFW_Fld_Master C ON C.Fld_id = A.numFormFieldID
			WHERE   A.numFormID = 29
					AND C.numDomainID = @numDomainID 
					AND A.numDomainID = @numDomainID
					AND A.numUserCntID=@numUserCntID
					AND ISNULL(A.bitCustom,0) = 1
		) T1
		ORDER BY
			 tintOrder
	END

	-- IF USER HASN'T CONFIGURED COLUMNS FROM SEARCH RESULT GRID THEN RETURN DEFAULT COLUMNS
	IF (SELECT COUNT(*) FROM @TEMPSelectedColumns) = 0
	BEGIN
		INSERT INTO @TEMPSelectedColumns
		(
			numFieldID,
			bitCustomField,
			tintOrder,
			intColumnWidth
		)
		SELECT  
			numFieldID,
			0,
			1,
			0
		FROM    
			View_DynamicDefaultColumns
		WHERE   
			numFormID = 29
			AND numDomainID = @numDomainID
			AND numFieldID = 189
	END

DECLARE @Table AS VARCHAR(200)
DECLARE @bitCustom BIT
DECLARE @numFieldGroupID AS INT
DECLARE @vcColumnName AS VARCHAR(200)
DECLARE @numFieldID AS NUMERIC(18,0)

	SELECT TOP 1
		@tintOrder=tintOrder,
		@numFieldID=numFormFieldID,
		@vcDbColumnName=vcDbColumnName,
		@vcFormFieldName=vcFieldName,
		@vcAssociatedControlType=vcAssociatedControlType,
		@vcListItemType=vcListItemType,
		@numListID=numListID,
		@vcLookBackTableName=vcLookBackTableName,
		@bitCustom=bitCustom,
		@numFieldGroupID=numGroupID
	FROM
	(
		SELECT
			T1.tintOrder+1 tintOrder,
			T1.numFieldID AS numFormFieldID,
			vcDbColumnName,
			vcFieldName,
			vcAssociatedControlType,
			vcListItemType,
			numListID,
			vcLookBackTableName,
			0 AS bitCustom,
			0 AS numGroupID
		FROM 
			View_DynamicDefaultColumns D
		INNER JOIN
			@TEMPSelectedColumns T1                         
		ON 
			D.numFieldId=T1.numFieldID                          
		WHERE 
			D.numDomainID = @numDomainID 
			AND D.numFormID = 29 
			AND ISNULL(T1.bitCustomField,0) = 0
		UNION
		SELECT
			T1.tintOrder+1 tintOrder,
			Fld_id AS numFormFieldID,
			CONCAT('Cust',Fld_id),
			Fld_label,
			c.Fld_type,
			'',
			c.numlistid,
			'',
			1 AS bitCustom,
			Grp_id
		FROM    
			CFW_Fld_Master C 
		INNER JOIN
			@TEMPSelectedColumns T1
		ON 
			C.Fld_id = T1.numFieldID
		WHERE   
			C.numDomainID = @numDomainID
			AND ISNULL(T1.bitCustomField,0) = 1
	) T1
	ORDER BY 
		tintOrder asc
while @tintOrder>0
begin

	IF @bitCustom = 0
	BEGIN
		if @vcAssociatedControlType='SelectBox'                              
        begin                              
		  if @vcListItemType='IG' --Item group
		  begin                              
			   set @SelectFields=@SelectFields+',IG.vcItemGroup ['+  @vcFormFieldName + '~' + @vcDbColumnName+']'                              
			   set @InneJoinOn= @InneJoinOn +' LEFT JOIN ItemGroups IG ON I.numDomainID = IG.numDomainID AND I.numItemGroup = IG.numItemGroupID '
			   SET @GroupBy = @GroupBy + 'IG.vcItemGroup,'
		  end
		  else if @vcListItemType='PP'--Item Type
		  begin
			   set @SelectFields=@SelectFields+', CASE I.charItemType WHEN ''p'' THEN ''Inventory Item'' WHEN ''n'' THEN  ''Non-Inventory Item'' WHEN ''s'' THEN ''Service'' ELSE '''' END   ['+  @vcFormFieldName + '~' + @vcDbColumnName+']'                              
			   SET @GroupBy = @GroupBy + 'I.charItemType,'
		  END
		  else if @vcListItemType='UOM'--Unit of Measure
		  BEGIN
		  		SET @SelectFields = @SelectFields + ' ,dbo.fn_GetUOMName(I.'+ @vcDbColumnName +') ['+  @vcFormFieldName + '~' + @vcDbColumnName+']'
				SET @GroupBy = @GroupBy + 'I.' + @vcDbColumnName + ','
		  END
		  else if @vcListItemType='LI'--Any List Item
		  BEGIN
			IF @vcDbColumnName='vcUnitofMeasure'
			BEGIN 		
				SET @SelectFields = @SelectFields + ' ,I.'+ @vcDbColumnName +' ['+  @vcFormFieldName + '~' + @vcDbColumnName+']'
				SET @GroupBy = @GroupBy + 'I.' + @vcDbColumnName + ','
			END 
			ELSE 
				BEGIN
					set @SelectFields=@SelectFields+',L'+ convert(varchar(3),@tintOrder)+'.vcData'+' ['+  @vcFormFieldName + '~' + @vcDbColumnName+']'                              
					set @InneJoinOn= @InneJoinOn +' left Join ListDetails L'+ convert(varchar(3),@tintOrder)+ ' on L'+convert(varchar(3),@tintOrder)+ '.numListItemID='+@vcDbColumnName                              
					SET @GroupBy = @GroupBy + 'L' + convert(varchar(3),@tintOrder)+'.vcData,'
					IF @SortColumnName ='numItemClassification'
						SET @SortColumnName = 'L'+ convert(varchar(3),@tintOrder)+'.vcData'
					
				END
			END                              
			
		END
		ELSE if @vcAssociatedControlType='ListBox'
		BEGIN
	 		if @vcListItemType='V'--Vendor
			BEGIN
				set @SelectFields=@SelectFields+', dbo.fn_GetComapnyName(I.numVendorID)  ['+  @vcFormFieldName + '~' + @vcDbColumnName+']'
				SET @GroupBy = @GroupBy + 'I.' + @vcDbColumnName + ','
			END
		END
		ELSE
		BEGIN
			IF @vcLookBackTableName ='WareHouseItems'
			BEGIN
				IF @vcDbColumnName = 'vcBarCode'
				BEGIN
					SET @SelectFields = @SelectFields + ', STUFF((SELECT N'', '' + vcBarCode FROM WareHouseItems WHERE numDomainID = ' + convert(varchar(15),@numDomainID) + ' AND numItemID = I.numItemCode FOR XML PATH ('''')), 1, 2, '''') ['+  @vcFormFieldName + '~' + @vcDbColumnName+']'
				END
				ELSE IF @vcDbColumnName = 'vcWHSKU'
				BEGIN
					SET @SelectFields = @SelectFields + ', STUFF((SELECT N'', '' + vcWHSKU FROM WareHouseItems WHERE numDomainID = ' + convert(varchar(15),@numDomainID) + ' AND numItemID = I.numItemCode FOR XML PATH ('''')), 1, 2, '''') ['+  @vcFormFieldName + '~' + @vcDbColumnName+']'
				END
				ELSE
				BEGIN
		 			SET @SelectFields = @SelectFields + ' ,WI.'+ @vcDbColumnName +' ['+  @vcFormFieldName + '~' + @vcDbColumnName+']'
		 		END
			END
			else IF @vcDbColumnName='vcSerialNo'
			BEGIN
				SET @SelectFields = @SelectFields + ' ,0 ['+  @vcFormFieldName + '~' + @vcDbColumnName+']'
			END
			ELSE IF @vcDbColumnName='vcPartNo'
			BEGIN
				SET @SelectFields = @SelectFields + ' ,(SELECT top 1 ISNULL(vcPartNo,'''') FROM dbo.Vendor V1 WHERE V1.numVendorID =I.numVendorID) as  ['+  @vcFormFieldName + '~' + @vcDbColumnName+']'
			END
			ELSE 
			BEGIN
		  		SET @SelectFields = @SelectFields + ' ,I.'+ @vcDbColumnName +' ['+ @vcFormFieldName + '~' + @vcDbColumnName+']'
		  		SET @GroupBy = @GroupBy + 'I.' + @vcDbColumnName + ','
			END
		   
		END
	END
	ELSE IF @bitCustom = 1
	BEGIN
		SET @vcColumnName= CONCAT(@vcFormFieldName,'~','Cust',@numFieldId)
			
		IF @vcAssociatedControlType = 'TextBox' or @vcAssociatedControlType = 'TextArea'            
		BEGIN
			SET @SelectFields= @SelectFields+',CFW'+ convert(VARCHAR(3),@tintOrder)+'.Fld_Value  ['+ @vcColumnName +']'                   
				
			SET @InneJoinOn= @InneJoinOn 
								+' left Join CFW_FLD_Values_Item CFW'+ convert(varchar(3),@tintOrder)
								+ ' on CFW' + convert(varchar(3),@tintOrder) 
								+ '.Fld_Id='+convert(varchar(10),@numFieldId) 
								+ 'and CFW' + convert(varchar(3),@tintOrder) 
								+ '.RecId=I.numItemCode '                                                         
		END   
		ELSE IF @vcAssociatedControlType = 'CheckBox'       
		BEGIN
			set @SelectFields= @SelectFields+',case when isnull(CFW' + CONVERT(VARCHAR(3), @tintOrder)+ '.Fld_Value,0)='''' then 0 else  isnull(CFW'
											+ CONVERT(VARCHAR(3), @tintOrder)+ '.Fld_Value,0) end ['+ @vcColumnName + ']'
 
			set @InneJoinOn= @InneJoinOn +' left Join CFW_FLD_Values_Item CFW'+ convert(varchar(3),@tintOrder)+ '             
			on CFW'+ convert(varchar(3),@tintOrder)+ '.Fld_Id='+convert(varchar(10),@numFieldId) +'and CFW'+ convert(varchar(3),@tintOrder)+ '.RecId=I.numItemCode '                                                     
		END                
		ELSE IF @vcAssociatedControlType = 'DateField'            
		BEGIN
			set @SelectFields= @SelectFields+',dbo.FormatedDateFromDate(CFW'+ convert(varchar(3),@tintOrder)+'.Fld_Value,'+convert(varchar(10),@numDomainId)+')  ['+ @vcColumnName +']'                   
			set @InneJoinOn= @InneJoinOn +' left Join CFW_FLD_Values_Item CFW'+ convert(varchar(3),@tintOrder)+ '                 
			on CFW'+ convert(varchar(3),@tintOrder)+ '.Fld_Id='+convert(varchar(10),@numFieldId) +'and CFW'+ convert(varchar(3),@tintOrder)+ '.RecId=I.numItemCode '
		END                
		ELSE IF @vcAssociatedControlType = 'SelectBox'             
		BEGIN
			set @vcDbColumnName = 'DCust'+Convert(varchar(10),@numFieldId)                
			set @SelectFields=@SelectFields+',L'+ convert(varchar(3),@tintOrder)+'.vcData'+' ['+ @vcColumnName +']'                                                          
			set @InneJoinOn= @InneJoinOn +' left Join CFW_FLD_Values_Item CFW'+ convert(varchar(3),@tintOrder)+ '                 
				on CFW'+ convert(varchar(3),@tintOrder)+ '.Fld_Id='+convert(varchar(10),@numFieldId) +'and CFW'+ convert(varchar(3),@tintOrder)+ '.RecId=I.numItemCode '
			set @InneJoinOn= @InneJoinOn +' left Join ListDetails L'+ convert(varchar(3),@tintOrder)+ ' on L'+convert(varchar(3),@tintOrder)+ '.numListItemID=CFW'+ convert(varchar(3),@tintOrder)+'.Fld_Value'                
		END         
		ELSE IF @vcAssociatedControlType = 'CheckBoxList'
		BEGIN
			SET @SelectFields= @SelectFields+',(SELECT STUFF((SELECT CONCAT('', '', vcData) FROM ListDetails WHERE numListID= ' + CAST(@numListID AS varchar) + ' AND numlistitemid IN (SELECT ISNULL(Id,0) FROM dbo.SplitIDs(CFW'+ convert(VARCHAR(3),@tintOrder)+'.Fld_Value,'','')) FOR XML PATH('''')), 1, 1, ''''))  ['+ @vcColumnName +']'   

			SET @InneJoinOn= @InneJoinOn 
								+' left Join CFW_FLD_Values_Item CFW'+ convert(varchar(3),@tintOrder)
								+ ' on CFW' + convert(varchar(3),@tintOrder) 
								+ '.Fld_Id='+convert(varchar(10),@numFieldId) 
								+ 'and CFW' + convert(varchar(3),@tintOrder) 
								+ '.RecId=I.numItemCode '
		END

	END
               
                                      
	SELECT TOP 1
		@tintOrder=tintOrder,
		@numFieldID=numFormFieldID,
		@vcDbColumnName=vcDbColumnName,
		@vcFormFieldName=vcFieldName,
		@vcAssociatedControlType=vcAssociatedControlType,
		@vcListItemType=vcListItemType,
		@numListID=numListID,
		@vcLookBackTableName=vcLookBackTableName,
		@bitCustom=bitCustom,
		@numFieldGroupID=numGroupID
	FROM
	(
		SELECT
			T1.tintOrder+1 tintOrder,
			T1.numFieldID AS numFormFieldID,
			vcDbColumnName,
			vcFieldName,
			vcAssociatedControlType,
			vcListItemType,
			numListID,
			vcLookBackTableName,
			0 AS bitCustom,
			0 AS numGroupID
		FROM 
			View_DynamicDefaultColumns D
		INNER JOIN
			@TEMPSelectedColumns T1                         
		ON 
			D.numFieldId=T1.numFieldID                          
		WHERE 
			D.numDomainID = @numDomainID 
			AND D.numFormID = 29 
			AND ISNULL(T1.bitCustomField,0) = 0
			AND T1.tintOrder > @tintOrder-1
		UNION
		SELECT
			T1.tintOrder+1 tintOrder,
			Fld_id AS numFormFieldID,
			CONCAT('Cust',Fld_id),
			Fld_label,
			c.Fld_type,
			'',
			c.numlistid,
			'',
			1 AS bitCustom,
			Grp_id
		FROM    
			CFW_Fld_Master C 
		INNER JOIN
			@TEMPSelectedColumns T1
		ON 
			C.Fld_id = T1.numFieldID
		WHERE   
			C.numDomainID = @numDomainID
			AND ISNULL(T1.bitCustomField,0) = 1
			AND T1.tintOrder > @tintOrder-1
	) T1
	ORDER BY 
		tintOrder asc     
                            
 if @@rowcount=0 set @tintOrder=0                              
end                              

/*--------------Sorting logic-----------*/
	
	IF @SortColumnName=''
		SET @SortColumnName = 'vcItemName'
	ELSE IF @SortColumnName ='numItemGroup'
		SET @SortColumnName = 'IG.vcItemGroup'
	ELSE IF @SortColumnName ='charItemType'
		SET @SortColumnName = 'I.charItemType'
	ELSE IF @SortColumnName = 'vcPartNo'
		SET @SortColumnName = 'vcItemName'
		
	 
SET @OrderBy = ' ORDER BY ' + @SortColumnName + ' '+ @columnSortOrder	
	
/*--------------Sorting logic ends-----------*/
	
	

	
	SET @strSql =  @strSql /*+ @SelectFields */+ @from + @InneJoinOn + @Where + @OrderBy
	PRINT @strSql
	INSERT INTO #temptable (
		numItemID
	) EXEC(@strSql)

/*-----------------------------Mass Update-----------------------------*/
if @bitMassUpdate=1
BEGIN
	IF CHARINDEX('USP_ItemCategory_MassUpdate',@strMassUpdate) >0
	BEGIN
		declare @vcItemCodes VARCHAR(MAX)
		SET @vcItemCodes = ''
		select @vcItemCodes = @vcItemCodes + numItemID + ', ' from #tempTable

		SET @strMassUpdate = REPLACE(@strMassUpdate,'##ItemCodes##',SUBSTRING(@vcItemCodes, 0, LEN(@vcItemCodes)))

		PRINT @strMassUpdate
		exec (@strMassUpdate)
	END
	ELSE
	BEGIN
		PRINT @strMassUpdate
		exec (@strMassUpdate)
	END
END

	
/*-----------------------------Paging Logic-----------------------------*/
	declare @firstRec as integer
	declare @lastRec as integer
	set @firstRec = ( @CurrentPage - 1 ) * @PageSize
	set @lastRec = ( @CurrentPage * @PageSize + 1 )
	set @TotRecs=(select count(*) from #tempTable)
	
	SET @InneJoinOn =@InneJoinOn+ ' join #temptable T on T.numItemID = I.numItemCode '
	SET @strSql = 'SELECT distinct I.numItemCode  [numItemCode],T.ID [ID~ID],isnull(I.bitSerialized,0) as [bitSerialized],isnull(I.bitLotNo,0) as [bitLotNo] ' +  @SelectFields + @from + @InneJoinOn + @Where 
	IF @GetAll=0
	BEGIN
		SET @strSql = @strSql +' and T.ID > '+convert(varchar(10),@firstRec)+ ' and T.ID <'+ convert(varchar(10),@lastRec) 
	END
	
	-- enable or disble group by based on search result view
	IF CHARINDEX('dbo.WareHouseItems WI',@InneJoinOn)=0
		SET @strSql = @strSql + ' order by ID; '
	ELSE 
		SET @strSql = @strSql + ' GROUP BY I.numItemCode,T.ID,I.bitSerialized,I.bitLotNo,I.numVendorID,' + SUBSTRING(@GroupBy, 1, LEN(@GroupBy)-1);
	
	
	EXEC (@strSql)
	PRINT @strSql
	
	DROP TABLE #temptable;
-------------------------------------------------	


	SELECT
		vcDbColumnName,
		vcFormFieldName,
		tintOrder AS tintOrder,
		bitAllowSorting
	FROM
	(
		SELECT
			D.vcDbColumnName,
			D.vcFieldName AS vcFormFieldName,
			T1.tintOrder AS tintOrder,
			isnull(D.bitAllowSorting,0) AS bitAllowSorting
		FROM 
			View_DynamicDefaultColumns D                               
		INNER JOIN
			@TEMPSelectedColumns T1                         
		ON 
			D.numFieldId=T1.numFieldID                         
		WHERE 
			D.numDomainID = @numDomainID 
			AND D.numFormID = 29 
			AND ISNULL(T1.bitCustomField,0) = 0
			AND d.vcDbColumnName not in ('numBillShipCountry','numBillShipState')
		UNION
		SELECT
			CONCAT('Cust',Fld_id),
			Fld_label,
			T1.tintOrder AS tintOrder,
			1 AS bitAllowSorting
		FROM
			CFW_Fld_Master C 
		INNER JOIN
			@TEMPSelectedColumns T1
		ON 
			C.Fld_id = T1.numFieldID
		WHERE   
			C.numDomainID = @numDomainID
			AND ISNULL(T1.bitCustomField,0) = 1
	) T1      
	UNION 
	SELECT 'ID','numItemCode',0 tintOrder,0 
	order by T1.tintOrder ASC




DROP TABLE #temp
END