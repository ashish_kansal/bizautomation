GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_TaxReports')
DROP PROCEDURE USP_TaxReports
GO
CREATE PROCEDURE [dbo].[USP_TaxReports]     
    @numDomainID numeric(18, 0),
    @dtFromDate DATETIME,
    @dtToDate DATETIME,
	@ClientTimeZoneOffset INT=0
AS                 
BEGIN
	DECLARE @bitDiscountOnUnitPrice BIT
	SELECT @bitDiscountOnUnitPrice=ISNULL(bitDiscountOnUnitPrice,0) FROM Domain WHERE numDomainID=@numDomainID

	SELECT 
		T.numOppBizDocsId,
		T.numReturnHeaderID,
		T.numOppId,
		vcBizDocID,
		TI.vcTaxName,
		T.fltPercentage,
		T.tintTaxType,
		SUM(Case When T.tintOppType=1 then TaxAmount else 0 end) SalesTax,
		SUM(Case When T.tintOppType=2 then TaxAmount else 0 end) PurchaseTax,
		T.INVCreatedDate,
		T.Organization,
		T.vcShipState,
		ISNULL((SELECT SUM(CASE WHEN @bitDiscountOnUnitPrice=1 THEN monTotAmount ELSE monTotAmtBefDiscount END) FROM OpportunityBizDocItems WHERE numOppBizDocID=T.numOppBizDocsId),0) AS SubTotal,
		T.GrandTotal
	FROM 
	(
		SELECT 
			OBD.numOppBizDocsId,
			0 AS numReturnHeaderID1,
		    OBD.vcBizDocID,
			OMTI.*,
			OM.tintOppType,
			ISNULL(dbo.fn_CalOppItemTotalTaxAmt(OM.numDomainId,OMTI.numTaxItemID,OM.numOppId,OBD.numOppBizDocsId),0) AS TaxAmount,
			[dbo].[FormatedDateFromDate](OBD.dtCreatedDate,CONVERT(VARCHAR(10), @numDomainId)) AS INVCreatedDate,
			CI.vcCompanyName AS Organization,
			ISNULL(dbo.fn_getOPPState(OM.numOppId,@numDomainId,2),'') AS vcShipState,			
			OBD.monDealAmount AS GrandTotal
			
		FROM OpportunityMaster OM 
		JOIN OpportunityBizDocs OBD 
		ON OM.numOppId=OBD.numOppId
		JOIN OpportunityMasterTaxItems OMTI 
		ON OMTI.numOppId=OBD.numOppId
		LEFT JOIN DivisionMaster DM
		ON OM.numDivisionId = DM.numDivisionID
		LEFT JOIN CompanyInfo CI
		ON DM.numCompanyID = CI.numCompanyId
		
		WHERE 
			OM.numDomainID=@numDomainID 
			AND OBD.bitAuthoritativeBizDocs=1 
			AND OMTI.fltPercentage>0
			AND OBD.dtFromDate BETWEEN  @dtFromDate AND @dtToDate
		UNION
		SELECT
			0 AS numOppBizDocsId,
			RH.numReturnHeaderID,
		    RH.vcRMA,
			OMTI.*,
			OM.tintOppType,
			RH.monTotalTax * -1 AS TaxAmount,
			[dbo].[FormatedDateFromDate](DATEADD(MINUTE,-1 * @ClientTimeZoneOffset,RH.dtCreatedDate),@numDomainID) AS INVCreatedDate,
			CI.vcCompanyName AS Organization,
			ISNULL(dbo.fn_getOPPState(OM.numOppId,212,2),'') AS vcShipState,			
			RH.monBizDocAmount AS GrandTotal
		FROM
			ReturnHeader RH
		INNER JOIN OpportunityMaster OM ON RH.numOppId=OM.numOppId
		JOIN OpportunityMasterTaxItems OMTI 
		ON OMTI.numOppId=RH.numOppId
		LEFT JOIN DivisionMaster DM
		ON RH.numDivisionId = DM.numDivisionID
		LEFT JOIN CompanyInfo CI
		ON DM.numCompanyID = CI.numCompanyId
		WHERE
			RH.numDomainId=@numDomainID
			AND DATEADD(MINUTE,-1 * @ClientTimeZoneOffset,RH.dtCreatedDate) BETWEEN  @dtFromDate AND @dtToDate
			AND RH.numReturnStatus=303
	) T 
	JOIN 
	(
		SELECT 
			numTaxItemId,
			vcTaxName 
		FROM 
			TaxItems 
		WHERE 
			numDomainID=@numDomainID 
		UNION 
		SELECT 
			0,
			'Sales Tax'
		UNION 
		SELECT 
			1,
			'CRV'
	) TI 
	ON T.numTaxItemId=TI.numTaxItemID 
	WHERE 
		TaxAmount <> 0
	GROUP BY 
		T.vcBizDocID,
		TI.vcTaxName,
		T.fltPercentage,
		T.tintTaxType,
		T.INVCreatedDate,
		T.Organization,
		T.vcShipState,
		T.GrandTotal,
		T.numOppBizDocsId,
		T.numReturnHeaderID,
		T.numOppId
END
GO