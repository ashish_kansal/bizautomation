SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_AssembledItem_GetByItemCode')
DROP PROCEDURE USP_AssembledItem_GetByItemCode
GO
CREATE PROCEDURE [dbo].[USP_AssembledItem_GetByItemCode]  
	@numDomainID NUMERIC(18,0),
	@numItemCode NUMERIC(18,0),
	@ClientTimeZoneOffset AS INT
AS
BEGIN
	SELECT
		ID,
		tintType,
		AssembledType,
		numAssembledQty,
		monAverageCost,
		numWarehouseItemID,
		vcWarehouse,
		dbo.FormatedDateTimeFromDate(DateAdd(minute,-@ClientTimeZoneOffset, dtCreatedDate),@numDomainID) As dtCreatedDate,
		vcUserName
	FROM
	(
		SELECT
			ID,
			1 AS tintType,
			'Build' AS AssembledType,
			(numAssembledQty - ISNULL(numDisassembledQty,0)) as numAssembledQty,
			ISNULL(AI.monAverageCost,0) monAverageCost,
			AI.numWarehouseItemID,
			CONCAT(vcWareHouse, CASE WHEN LEN(WI.vcLocation) > 0 THEN CONCAT('(',WL.vcLocation,')') ELSE '' END) AS vcWarehouse,
			dtCreatedDate,
			ISNULL((SELECT vcUserName FROM UserMaster WHERE numUserDetailId = AI.numCreatedBy),'') AS vcUserName
		FROM
			AssembledItem AI
		INNER JOIN
			WareHouseItems WI
		ON
			AI.numWarehouseItemID = WI.numWareHouseItemID
		INNER JOIN
			Warehouses W
		ON
			WI.numWareHouseID = W.numWareHouseID
		LEFT JOIN
			WarehouseLocation WL
		ON
			WI.numWLocationID = WL.numWLocationID
		WHERE
			AI.numDomainID=@numDomainID
			AND AI.numItemCode=@numItemCode
		UNION
		SELECT
			numWOId,
			2 AS tintType,
			'Work Order' AS AssembledType,
			WO.numQtyItemsReq as numAssembledQty,
			ISNULL(WO.monAverageCost,0) monAverageCost,
			WO.numWarehouseItemID,
			CONCAT(vcWareHouse, CASE WHEN LEN(WI.vcLocation) > 0 THEN CONCAT('(',WL.vcLocation,')') ELSE '' END) AS vcWarehouse,
			WO.bintCreatedDate as dtCreatedDate,
			ISNULL((SELECT vcUserName FROM UserMaster WHERE numUserDetailId = WO.numCreatedBy),'') AS vcUserName
		FROM
			WorkOrder WO
		INNER JOIN
			WareHouseItems WI
		ON
			WO.numWarehouseItemID = WI.numWareHouseItemID
		INNER JOIN
			Warehouses W
		ON
			WI.numWareHouseID = W.numWareHouseID
		LEFT JOIN
			WarehouseLocation WL
		ON
			WI.numWLocationID = WL.numWLocationID
		WHERE 
			ISNULL(numOppId,0)=0 
			AND ISNULL(numParentWOID,0)=0 
			AND numWOStatus = 23184 
			AND numItemCode=@numItemCode
	) AS TEMP
	ORDER BY
		dtCreatedDate DESC
END


