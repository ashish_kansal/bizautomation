GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_CFW_FLD_Values_Case_CT' ) 
    DROP PROCEDURE USP_CFW_FLD_Values_Case_CT
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Sachin Sadhu>
-- Create date: <Create Date,,29thJuly2014>
-- Description: This customField change Tracking SP is used in Projects
-- =============================================
Create PROCEDURE [dbo].[USP_CFW_FLD_Values_Case_CT]
-- Add the parameters for the stored procedure here
    @numDomainID numeric(18, 0)=0,
    @numUserCntID numeric(18, 0)=0,
    @numRecordID numeric(18, 0)=0 ,
	@numFormFieldId  numeric (18,0)=0

AS
BEGIN
-- SET NOCOUNT ON added to prevent extra result sets from
-- interfering with SELECT statements.
SET NOCOUNT ON;
 -- Insert statements for procedure here
 Declare @FldDTLID numeric
 
 Select @FldDTLID=FldDTLID from CFW_FLD_Values_Case where  RecId=@numRecordID and  Fld_ID=@numFormFieldId

 


 declare @tblFields as table (
 FieldName varchar(200),
 FieldValue varchar(200)
)

Declare @Type char(1)
Declare @ChangeVersion bigint
Declare @CreationVersion bigint
Declare @last_synchronization_version bigInt=null
DECLARE @Columns_Updated VARCHAR(1000)

		SELECT 			
			@ChangeVersion=Ct.Sys_Change_version,@CreationVersion=Ct.Sys_Change_Creation_version
		FROM 
			CHANGETABLE(CHANGES dbo.CFW_FLD_Values_Case, 0) AS CT
		WHERE FldDTLID=@FldDTLID

		SELECT 
			@Type = CT.SYS_CHANGE_OPERATION
		FROM 
			CHANGETABLE(CHANGES dbo.CFW_FLD_Values_Case, 0) AS CT
		WHERE  CT.FldDTLID=@FldDTLID


IF (@ChangeVersion=@CreationVersion)--Insert
Begin
        SELECT 
			@Type = CT.SYS_CHANGE_OPERATION
		FROM 
			CHANGETABLE(CHANGES dbo.CFW_FLD_Values_Case, 0) AS CT
		WHERE  CT.FldDTLID=@FldDTLID
END
ELSE
BEGIN




SET @last_synchronization_version=@ChangeVersion-1     
		
		SELECT 
			@Type = CT.SYS_CHANGE_OPERATION
		FROM 
			CHANGETABLE(CHANGES dbo.CFW_FLD_Values_Case, @last_synchronization_version) AS CT
		WHERE  CT.FldDTLID=@FldDTLID
	
		--Fields Up date & for Type :Insert or Update:
			Declare @UFFields as table 
			(
			FldDTLID varchar(70),
			Fld_ID varchar(70),
			Fld_Value varchar(70),
			RecId varchar(70)
			
			)

	INSERT into @UFFields(FldDTLID,Fld_ID,Fld_Value,RecId)
		SELECT
		CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.CFW_FLD_Values_Case'), 'FldDTLID', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS FldDTLID ,
		CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.CFW_FLD_Values_Case'), 'Fld_ID', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS Fld_ID, 
		CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.CFW_FLD_Values_Case'), 'Fld_Value', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS Fld_Value, 
		CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.CFW_FLD_Values_Case'), 'RecId', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS RecId   
		
		FROM 
		CHANGETABLE(CHANGES dbo.CFW_FLD_Values_Case, @last_synchronization_version) AS CT
		WHERE 
		ct.FldDTLID=@FldDTLID

    --sp_helptext USP_GetWorkFlowFormFieldMaster
	--exec USP_GetWorkFlowFormFieldMaster 1,73
INSERT INTO @tblFields(FieldName,FieldValue)
		SELECT
		FieldName,
		FieldValue
		FROM
			(
				SELECT  FldDTLID,Fld_ID,Fld_Value,RecId
				FROM @UFFields	
			) AS UP
		UNPIVOT
		(
		FieldValue FOR FieldName IN (FldDTLID,Fld_ID,Fld_Value,RecId)
		) AS upv
		where FieldValue<>0
 
END


--select @FldDTLID

DECLARE @tintWFTriggerOn AS TINYINT
DECLARE @fldLabel varchar(100)
SET @tintWFTriggerOn=CASE @Type WHEN 'I' THEN 1 WHEN 'U' THEN 2 WHEN 'D' THEN 5 END

--select * from @UFFields


SELECT @Columns_Updated = COALESCE(@Columns_Updated + ',', '') + FieldName FROM @tblFields 
SET @Columns_Updated=ISNULL(@Columns_Updated,'')
--select @Columns_Updated

--IF (@Columns_Updated='Fld_Value')
--BEGIN

 
 Select @fldLabel=Fld_label from CFW_Fld_Master where   Fld_id=@numFormFieldId

 SET @Columns_Updated=ISNULL(@fldLabel,'')
 SET @tintWFTriggerOn=2
--END



	EXEC dbo.USP_ManageWorkFlowQueueCF
	@numWFQueueID = 0, --  numeric(18, 0)
	@numDomainID = @numDomainID, --  numeric(18, 0)
	@numUserCntID = @numUserCntID, --  numeric(18, 0)
	@numRecordID = @numRecordID, --  numeric(18, 0)
	@numFormID = 72, --  numeric(18, 0)1
	@tintProcessStatus = 1, --  tinyint
	@tintWFTriggerOn = @tintWFTriggerOn, --  tinyint
	@tintMode = 1, --  tinyint
	@vcColumnsUpdated = @Columns_Updated

	
/***************************************************Commented SP for Checking RowVersion with Change Tracking:Date:23Apr||Sachin********************************************************/



--DECLARE @synchronization_version BIGINT 



--SET @synchronization_version = CHANGE_TRACKING_CURRENT_VERSION();











--SELECT 



--    CT.numOppID, CT.SYS_CHANGE_OPERATION, 



--    CT.SYS_CHANGE_COLUMNS, CT.SYS_CHANGE_CONTEXT, 



--    CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.OpportunityMaster'), 'numOppID', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS numOppID, 



--    CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.OpportunityMaster'), 'vcPOppName', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS vcPOppName 



--FROM 



--    CHANGETABLE(CHANGES dbo.OpportunityMaster, @synchronization_version) AS CT











--SET @synchronization_version = CHANGE_TRACKING_CURRENT_VERSION();


/***************************************************End of Comment||Sachin********************************************************/




END

/****** Object:  StoredProcedure [dbo].[USP_ReOpenOppertunity]    Script Date: 07/26/2008 16:20:22 ******/
SET ANSI_NULLS ON