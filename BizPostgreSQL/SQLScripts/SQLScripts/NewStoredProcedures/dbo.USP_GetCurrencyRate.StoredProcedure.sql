
--Created by Anoop Jayaraj
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetCurrencyRate')
DROP PROCEDURE USP_GetCurrencyRate
GO
CREATE PROCEDURE USP_GetCurrencyRate
@numDomanID AS NUMERIC(9),
@bitAll AS BIT,
@numCurrencyID NUMERIC

AS

IF NOT EXISTS (SELECT * FROM Currency WHERE numDomainID=@numDomanID)
INSERT INTO Currency (vcCurrencyDesc,chrCurrency,varCurrSymbol
      ,numDomainID,fltExchangeRate,bitEnabled)
SELECT vcCurrencyDesc, chrCurrency,varCurrSymbol, @numDomanID, fltExchangeRate, bitEnabled
FROM Currency WHERE numDomainID=0


IF @numCurrencyID >0 
BEGIN
	
    SELECT  numCurrencyID ,
            vcCurrencyDesc ,
            chrCurrency ,
            varCurrSymbol ,
            fltExchangeRate ,
            bitEnabled ,
            numCountryId
    FROM    dbo.Currency
    WHERE   numDomainID = @numDomanID
            AND numCurrencyID = @numCurrencyID
	
	RETURN
END


IF @bitAll=1
	SELECT numCurrencyID,vcCurrencyDesc,chrCurrency,varCurrSymbol
		  ,numDomainID,fltExchangeRate,bitEnabled,ISNULL(numCountryId,0) AS numCountryId
	FROM Currency WHERE numDomainID=@numDomanID
	ORDER BY bitEnabled DESC,vcCurrencyDesc ASC
ELSE
	SELECT numCurrencyID,vcCurrencyDesc,chrCurrency,varCurrSymbol
		  ,numDomainID,fltExchangeRate,bitEnabled,ISNULL(numCountryId,0) AS numCountryId
	FROM Currency WHERE numDomainID=@numDomanID AND bitEnabled=1
	ORDER BY bitEnabled DESC,vcCurrencyDesc ASC