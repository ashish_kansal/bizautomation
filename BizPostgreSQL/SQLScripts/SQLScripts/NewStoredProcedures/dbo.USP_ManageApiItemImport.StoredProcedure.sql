GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_ManageApiItemImport' ) 
    DROP PROCEDURE USP_ManageApiItemImport
GO

--created by Joseph
CREATE PROCEDURE [dbo].[USP_ManageApiItemImport]
	@numDomainID numeric(18, 0),
	@numWebApiID numeric(18, 0),
	@UserContactID numeric(18, 0),
	@FlagImport varchar(100)
	
AS

BEGIN

	UPDATE dbo.WebAPIDetail SET vcFourteenthFldValue = @FlagImport ,vcFifteenthFldValue = @UserContactId
	WHERE WebApiId = @numWebApiID AND numDomainId = @numDomainID

END
