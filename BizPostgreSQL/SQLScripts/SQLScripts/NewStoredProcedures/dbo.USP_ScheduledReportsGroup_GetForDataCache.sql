GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ScheduledReportsGroup_GetForDataCache')
DROP PROCEDURE dbo.USP_ScheduledReportsGroup_GetForDataCache
GO
CREATE PROCEDURE [dbo].[USP_ScheduledReportsGroup_GetForDataCache]
(
	@numDomainID NUMERIC(18,0)
	,@numSRGID NUMERIC(18,0)
	,@tintMode TINYINT
)
AS 
BEGIN
	IF @tintMode = 1 AND EXISTS (SELECT ID FROM ScheduledReportsGroup WHERE numDomainID=@numDomainID AND ID=@numSRGID)
	BEGIN
		SELECT * FROM ScheduledReportsGroup WHERE numDomainID=@numDomainID AND ID=@numSRGID
	END
	ELSE IF @tintMode = 2
	BEGIN
		SELECT 
			*
		FROM 
			ScheduledReportsGroup 
		WHERE 
			ISNULL(tintDataCacheStatus,0) <> 1 --0: NOT STARTED, 1: STARTED, 2: FINISHED
			AND (DATEPART(HOUR,GETDATE()) >= 0 AND DATEPART(HOUR,GETDATE()) <= 7) -- BETWEEN 0 AM TO 7 AM
			AND 1 = (CASE 
						WHEN dtNextDate IS NULL AND CAST(dtStartDate AS DATE) <= CAST(DATEADD(DAY,1,GETUTCDATE()) AS DATE) THEN 1
						WHEN dtNextDate IS NOT NULL AND CAST(dtNextDate AS DATE) <= CAST(DATEADD(DAY,1,GETUTCDATE()) AS DATE) THEN 1
						ELSE 0
					END)
	END
END
GO