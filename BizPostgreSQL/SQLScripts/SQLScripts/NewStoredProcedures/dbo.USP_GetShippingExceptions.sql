GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_GetShippingExceptions' ) 
    DROP PROCEDURE USP_GetShippingExceptions
GO
CREATE PROCEDURE USP_GetShippingExceptions
   
     @numDomainID NUMERIC(9)
	
AS 
BEGIN
    
	SELECT numShippingExceptionID,
		(SELECT vcData from ListDetails LD JOIN ShippingExceptions SE ON LD.numListItemID = SE.numClassificationID 
							WHERE LD.numListItemID = SE.numClassificationID) AS vcClassification
		,PercentAbove
		,FlatAmt
		
	 FROM ShippingExceptions WHERE numDomainID = @numDomainID

END