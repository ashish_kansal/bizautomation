SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
                    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_CheckFinancialyear')
DROP PROCEDURE USP_CheckFinancialyear
GO
CREATE PROCEDURE [dbo].[USP_CheckFinancialyear]    
	@numDomainID as numeric(9),
	@dtDate as DATETIME
as                            
BEGIN
SELECT COUNT(*) FROM Financialyear WHERE numDomainID=@numDomainID AND bitCloseStatus=0 AND bitCurrentYear=1 AND (CAST(dtPeriodFrom AS DATE) <= CAST(@dtDate AS DATE) AND CAST(dtPeriodTO AS DATE) >= CAST(@dtDate AS DATE))
END	
GO
 