GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_WorkSchedule_Save')
DROP PROCEDURE dbo.USP_WorkSchedule_Save
GO
CREATE PROCEDURE [dbo].[USP_WorkSchedule_Save]
(
	@numWorkScheduleID NUMERIC(18,0)
	,@numDomainID NUMERIC(18,0)
	,@numUserCntID NUMERIC(18,0)
	,@vcWorkDays VARCHAR(20)
	,@numWorkHours TINYINT
	,@numWorkMinutes TINYINT
	,@numProductiveHours TINYINT
	,@numProductiveMinutes TINYINT
	,@tmStartOfDay TIME
)
AS 
BEGIN
	IF EXISTS (SELECT ID FROM WorkSchedule WHERE numDomainID=@numDomainID AND ID=ISNULL(@numWorkScheduleID,0))
	BEGIN
		UPDATE
			WorkSchedule
		SET
			numWorkHours=@numWorkHours
			,numWorkMinutes=@numWorkMinutes
			,numProductiveHours=@numProductiveHours
			,numProductiveMinutes=@numProductiveMinutes
			,tmStartOfDay=@tmStartOfDay
			,vcWorkDays=@vcWorkDays
		WHERE	
			ID = @numWorkScheduleID
	END
	ELSE
	BEGIN
		INSERT INTO WorkSchedule
		(
			numDomainID
			,numUserCntID
			,numWorkHours
			,numWorkMinutes
			,numProductiveHours
			,numProductiveMinutes
			,tmStartOfDay
			,vcWorkDays
		)
		VALUES
		(
			@numDomainID
			,@numUserCntID
			,@numWorkHours
			,@numWorkMinutes
			,@numProductiveHours
			,@numProductiveMinutes
			,@tmStartOfDay
			,@vcWorkDays
		)
	END
END
GO