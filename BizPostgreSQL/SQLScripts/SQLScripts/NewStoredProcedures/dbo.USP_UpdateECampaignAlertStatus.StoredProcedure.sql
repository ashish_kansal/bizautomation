GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_UpdateECampaignAlertStatus')
DROP PROCEDURE USP_UpdateECampaignAlertStatus
GO
CREATE PROCEDURE [dbo].[USP_UpdateECampaignAlertStatus]
    @numConECampDTLID NUMERIC(9),
    @tintDeliveryStatus TINYINT,
    @vcEmailLog VARCHAR(500) = NULL,
    @tintMode TINYINT = 0
AS 
    BEGIN
		DECLARE @Date AS DATETIME 
		SET @Date = GETUTCDATE()

        IF @tintMode = 0 
        BEGIN
            UPDATE  [ConECampaignDTL]
            SET     [vcEmailLog] = @vcEmailLog,
                    [tintDeliveryStatus] = @tintDeliveryStatus,
                    bitSend = 1,
                    bintSentON = @Date
            WHERE   [numConECampDTLID] = @numConECampDTLID
        END

        IF @tintMode = 1 
        BEGIN
            UPDATE  [ConECampaignDTL]
            SET     bitFollowUpStatus = 1
            WHERE   [numConECampDTLID] = @numConECampDTLID      
        END
			
        IF @tintMode = 2 --For Action Item
        BEGIN
            UPDATE  [ConECampaignDTL]
            SET     [vcEmailLog] = @vcEmailLog,
                    bitSend = 1,
					[tintDeliveryStatus] = @tintDeliveryStatus,
                    bintSentON = GETUTCDATE()
            WHERE   [numConECampDTLID] = @numConECampDTLID
        END
    END

