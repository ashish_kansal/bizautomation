/****** Object:  StoredProcedure [dbo].[USP_DeleteSite]    Script Date: 08/08/2009 15:00:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_DeleteSite')
DROP PROCEDURE USP_DeleteSite
GO
CREATE PROCEDURE [dbo].[USP_DeleteSite]
          @numSiteID   NUMERIC(9),
          @numDomainID NUMERIC(9)
AS
  BEGIN

    DELETE FROM dbo.PromotionOfferContacts WHERE numProId in (select numRuleID from  dbo.ShippingRules where numSiteID = @numSiteID  and numDomainID = @numDomainID) AND tintRecordType=2
	DELETE FROM dbo.PromotionOfferItems WHERE numProId in (select numRuleID from  dbo.ShippingRules where numSiteID = @numSiteID  and numDomainID = @numDomainID) AND tintRecordType=2
    DELETE  FROM dbo.ShippingRules
    WHERE   
            numDomainID = @numDomainID
            AND numSiteID = @numSiteID


	DELETE FROM SiteSubscriberDetails WHERE [numSiteID] =@numSiteID
    DELETE FROM [SiteBreadCrumb]
    WHERE       [numSiteID] = @numSiteID
    
    DELETE FROM [StyleSheetDetails]
    WHERE       [numCssID] IN (SELECT numCssID
                               FROM   [StyleSheets]
                               WHERE  [numSiteID] = @numSiteID)
    DELETE FROM [StyleSheets]
    WHERE       [numSiteID] = @numSiteID
                AND [numDomainID] = @numDomainID
    DELETE FROM [SiteTemplates]
    WHERE       [numSiteID] = @numSiteID
                AND [numDomainID] = @numDomainID
    DELETE FROM [PageElementDetail]
    WHERE       [numSiteID] = @numSiteID
                AND [numDomainID] = @numDomainID
    DELETE FROM [SiteMenu]
    WHERE       [numSiteID] = @numSiteID
                AND [numDomainID] = @numDomainID
    DELETE FROM [SitePages]
    WHERE       [numSiteID] = @numSiteID
                AND [numDomainID] = @numDomainID
    DELETE FROM [SiteCategories]
    WHERE       [numSiteID] = @numSiteID
    DELETE FROM Sites
    WHERE       [numSiteID] = @numSiteID
                AND numDomainID = @numDomainID
    
    --Delete Shipping Rules 
    DELETE FROM dbo.ShippingRuleStateList WHERE 
           numRuleID IN(
           SELECT numRuleId FROM dbo.ShippingRules
           WHERE  numDomainID = @numDomainID   
           AND numSiteID = @numSiteID
           )   
           
    DELETE FROM dbo.ShippingServiceTypes WHERE
           numRuleID IN(
           SELECT numRuleId FROM dbo.ShippingRules
           WHERE  numDomainID = @numDomainID   
           AND numSiteID = @numSiteID
           )  
  
    DELETE  FROM dbo.ShippingRules
    WHERE   numDomainID = @numDomainID
            AND numSiteID = @numSiteID
    
    --Delete Review ,Rating  and ReviewDetail
    DELETE FROM Ratings
    WHERE       [numSiteID] = @numSiteID
                AND numDomainID = @numDomainID
    DELETE FROM ReviewDetail 
    WHERE       numReviewId IN
               (SELECT numReviewId FROM Review WHERE numSiteId = @numSiteID AND numDomainId = @numDomainID)
    DELETE FROM Review 
    WHERE       numSiteId = @numSiteID AND numDomainID = @numDomainID
                            
                            
  END
