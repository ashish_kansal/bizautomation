GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_Item_GetPriceBasedOnPricingMethod')
DROP PROCEDURE USP_Item_GetPriceBasedOnPricingMethod
GO
CREATE PROCEDURE [dbo].[USP_Item_GetPriceBasedOnPricingMethod]
	@numDomainID NUMERIC(18,0)
	,@numDivisionID NUMERIC(18,0)
	,@numItemCode NUMERIC(18,0)
	,@numWarehouseItemID NUMERIC(18,0) 
	,@numQty FLOAT
	,@vcChildKits VARCHAR(MAX)
	,@numCurrencyID NUMERIC(18,0)
AS
BEGIN
	DECLARE @fltExchangeRate FLOAT
	SET @fltExchangeRate = ISNULL((SELECT fltExchangeRate FROM Currency WHERE numDomainID=@numDomainID AND numCurrencyID=@numCurrencyID),0)

	IF ISNULL(@fltExchangeRate,0) = 0
	BEGIN
		SET @fltExchangeRate = 1
	END

	DECLARE @monPrice DECIMAL(20,5)
	SELECT @monPrice=(CASE 
							WHEN EXISTS (SELECT ID FROM ItemCurrencyPrice WHERE numDomainID=@numDomainID AND numItemCode=@numItemCode AND numCurrencyID=ISNULL(@numCurrencyID,0))  
							THEN ISNULL((SELECT monListPrice FROM ItemCurrencyPrice WHERE numDomainID=@numDomainID AND numItemCode=@numItemCode AND numCurrencyID=ISNULL(@numCurrencyID,0)),0)
							ELSE (CASE WHEN @fltExchangeRate <> 1 THEN CAST((ISNULL(Item.monListPrice,0) / @fltExchangeRate) AS INT) ELSE ISNULL(Item.monListPrice,0) END)
						END) FROM Item WHERE numItemCode=@numItemCode

	IF EXISTS (SELECT 
					numItemCode 
				FROM 
					Item 
				WHERE 
					numDomainID=@numDomainID 
					AND numItemCode=@numItemCode 
					AND ISNULL(bitCalAmtBasedonDepItems,0)=1 
					AND (ISNULL(bitKitParent,0) = 1 OR ISNULL(bitAssembly,0)=1))
	BEGIN
		SET @monPrice = dbo.GetCalculatedPriceForKitAssembly(@numDomainID,@numDivisionID,@numItemCode,@numQty,@numWarehouseItemID,1,0,0,@vcChildKits,@numCurrencyID,@fltExchangeRate)
	END
	ELSE
	BEGIN
		SELECT
			@monPrice=monFinalPrice
		FROM
			dbo.fn_GetItemPriceBasedOnMethod(@numDomainID,@numDivisionID,@numItemCode,@numQty,0,0,@numWarehouseItemID,@vcChildKits,@numCurrencyID)
	END

	SELECT @monPrice AS monPrice
END