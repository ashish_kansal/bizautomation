SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getFormFieldValidationDetails')
DROP PROCEDURE usp_getFormFieldValidationDetails
GO
CREATE PROCEDURE [dbo].[usp_getFormFieldValidationDetails]  
 @numDomainID NUMERIC(9),
 @numFormId NUMERIC(9),
 @numFormFieldId NUMERIC(9)
AS  
BEGIN  

 select DFV.numFormId
      ,DFV.numFormFieldId
      ,DFV.numDomainId
      ,DFV.vcNewFormFieldName
      ,DFV.bitIsRequired
      ,DFV.bitIsNumeric
      ,DFV.bitIsAlphaNumeric
      ,DFV.bitIsEmail
      ,DFV.bitIsLengthValidation
      ,DFV.intMaxLength
      ,DFV.intMinLength
      ,DFV.bitFieldMessage
      ,DFV.vcFieldMessage
      ,ISNULL(DFV.vcNewFormFieldName,isnull(DFFM.vcFieldName,DFM.vcFieldName)) AS vcFormFieldName,DFM.vcAssociatedControlType,
 ISNULL(DFV.vcToolTip,ISNULL(DFM.vcToolTip,'')) AS vcToolTip
 FROM DycFormField_Mapping DFFM 
JOIN DycFieldMaster DFM on DFFM.numModuleID=DFFM.numModuleID and DFFM.numFieldID=DFM.numFieldID
left join DynamicFormField_Validation DFV on DFV.numDomainID=@numDomainID AND DFM.numFieldID=DFV.numFormFieldId AND DFV.numFormId=DFFM.numFormId
LEFT JOIN DynamicFormMaster DFMMaster ON DFFM.numFormId=DFMMaster.numFormId
where DFFM.numFieldID=@numFormFieldId AND DFFM.numFormID=@numFormId AND ISNULL(DFM.bitDeleted,0)=0 ORDER BY vcFormFieldName
 
END
GO
