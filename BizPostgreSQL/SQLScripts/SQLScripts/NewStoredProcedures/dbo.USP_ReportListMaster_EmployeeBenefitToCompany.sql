SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ReportListMaster_EmployeeBenefitToCompany')
DROP PROCEDURE USP_ReportListMaster_EmployeeBenefitToCompany
GO
CREATE PROCEDURE [dbo].[USP_ReportListMaster_EmployeeBenefitToCompany]
	@numDomainID NUMERIC(18,0)
	,@ClientTimeZoneOffset INT
	,@vcTimeLine VARCHAR(50)
	,@vcFilterBy TINYINT  -- 1:Assign TO, 2: Record Owner, 3: Teams (Based on Assigned To), 4: Employees
	,@vcFilterValue VARCHAR(MAX)
AS
BEGIN 
	DECLARE @numShippingItemID NUMERIC(18,0)
	DECLARE @numDiscountItemID NUMERIC(18,0)
	SELECT @numShippingItemID = numShippingServiceItemID,@numDiscountItemID=numDiscountServiceItemID FROM Domain WHERE numDomainId=@numDomainID

	DECLARE @dtStartDate DATETIME --= DATEADD(MINUTE,-@ClientTimeZoneOffset,DATEADD(MONTH,-12,GETUTCDATE()))
	DECLARE @dtEndDate AS DATETIME = DATEADD(MINUTE,-@ClientTimeZoneOffset,GETUTCDATE())

	IF @vcTimeLine = 'Last12Months'
	BEGIN
		SET @dtStartDate = DATEADD(MONTH, DATEDIFF(MONTH, 0, @dtEndDate), 0)
	END
	IF @vcTimeLine = 'Last6Months'
	BEGIN
		SET @dtStartDate = DATEADD(MONTH, DATEDIFF(MONTH, 0, @dtEndDate), 0)
	END
	IF @vcTimeLine = 'Last3Months'
	BEGIN
		SET @dtStartDate = DATEADD(MONTH, DATEDIFF(MONTH, 0, @dtEndDate), 0)
	END
	IF @vcTimeLine = 'Last30Days'
	BEGIN
		SET @dtStartDate = DATEADD(DAY, DATEDIFF(DAY, -30, @dtEndDate), 0)
	END
	IF @vcTimeLine = 'Last7Days'
	BEGIN
		SET @dtStartDate = DATEADD(DAY, DATEDIFF(DAY, -7, @dtEndDate), 0)
	END
	
	-- FETCH ALLREADY PAID COMMISSION DATA AND STORE IN TEMPORARY TABLE
	SELECT * INTO #tempPayrollTracking FROM PayrollTracking WHERE numDomainId=@numDomainId

	DECLARE @TempCommissionPaidCreditMemoOrRefund TABLE
	(
		numUserCntID NUMERIC(18,0),
		numReturnHeaderID NUMERIC(18,0),
		tintReturnType TINYINT,
		numReturnItemID NUMERIC(18,0),
		monCommission DECIMAL(20,5),
		numComRuleID NUMERIC(18,0),
		tintComType TINYINT,
		tintComBasedOn TINYINT,
		decCommission FLOAT,
		tintAssignTo TINYINT
	)    

	INSERT INTO 
		@TempCommissionPaidCreditMemoOrRefund 
	SELECT 
		numUserCntID,
		numReturnHeaderID,
		tintReturnType,
		numReturnItemID,
		monCommission,
		numComRuleID,
		tintComType,
		tintComBasedOn,
		decCommission,
		tintAssignTo
	FROM 
		dbo.GetCommissionPaidCreditMemoOrRefund(@numDomainId,@ClientTimeZoneOffset,@dtStartDate,@dtEndDate)

	DECLARE @TEMP TABLE
	(
		ID INT IDENTITY(1,1)
		,numUserCntID NUMERIC(18,0)
		,vcUserName VARCHAR(300)
		,monHourlyRate DECIMAL(20,5)
		,TotalHrsWorked FLOAT
		,TotalPayroll DECIMAL(20,5)
		,ProfitMinusPayroll DECIMAL(20,5)
	)

	INSERT INTO @TEMP
	(
		numUserCntID
		,vcUserName
		,monHourlyRate
	)
	SELECT 
		UM.numUserDetailId
		,CONCAT(ADC.vcfirstname,' ',adc.vclastname) vcUserName
		,ISNULL(UM.monHourlyRate,0)
	FROM  
		dbo.UserMaster UM 
	JOIN 
		dbo.AdditionalContactsInformation adc 
	ON 
		adc.numDomainID=@numDomainId 
		AND adc.numcontactid = um.numuserdetailid
	JOIN
		userteams UT
	ON 
		UT.numDomainID=@numDomainId 
		AND UM.numUserDetailId=UT.numUserCntID
	WHERE 
		UM.numDomainId=@numDomainId
		AND UM.bitActivateFlag = 1
		AND 1 = (CASE WHEN @vcFilterBy = 4  THEN (CASE WHEN UT.numUserCntID IN (SELECT Id FROM dbo.SplitIDs((@vcFilterValue),',')) THEN 1 ELSE 0 END) 
					ELSE 
				(CASE WHEN LEN(ISNULL(@vcFilterValue,'')) > 0 THEN (CASE WHEN UT.numTeam IN (SELECT Id FROM dbo.SplitIDs((@vcFilterValue),',')) THEN 1 ELSE 0 END) ELSE 1 END)
					END)
		


	DECLARE @i INT = 1
	DECLARE @COUNT INT = 0
	DECLARE @numUserCntID NUMERIC(18,0)
	DECLARE @decTotalHrsWorked as decimal(10,2)
	DECLARE @decTotalPaidLeaveHrs AS DECIMAL(10,2)
	DECLARE @monExpense DECIMAL(20,5)
	DECLARE @monReimburse DECIMAL(20,5)
	DECLARE @monCommPaidInvoice DECIMAL(20,5)
	DECLARE @monCommPaidInvoiceDepositedToBank DECIMAL(20,5)
	DECLARE @monCommUNPaidInvoice DECIMAL(20,5)
	DECLARE @monCommPaidCreditMemoOrRefund DECIMAL(20,5)
	DECLARE @monCommUnPaidCreditMemoOrRefund DECIMAL(20,5)
	DECLARE @GrossProfit DECIMAL(20,5)
    
	SELECT @COUNT=COUNT(*) FROM @TEMP

	-- LOOP OVER EACH EMPLOYEE
	WHILE @i <= @COUNT
	BEGIN
		SELECT @decTotalHrsWorked=0,@decTotalPaidLeaveHrs=0,@monExpense=0,@monReimburse=0,@monCommPaidInvoice=0,@monCommPaidInvoiceDepositedToBank=0,@monCommUNPaidInvoice=0, @numUserCntID=0, @GrossProfit=0

		SELECT @numUserCntID = numUserCntID FROM @TEMP WHERE ID=@i

		SELECT 
			@decTotalHrsWorked=sum(x.Hrs) 
		FROM 
		(
			SELECT  
				ISNULL(SUM(CAST(DATEDIFF(MINUTE,dtfromdate,dttodate) AS FLOAT)/CAST(60 AS FLOAT)),0) AS Hrs            
			FROM 
				TimeAndExpense 
			WHERE 
				(numType=1 OR numType=2) 
				AND numCategory=1                 
				AND (
						(DATEADD(MINUTE,-@ClientTimeZoneOffset,dtFromDate) BETWEEN @dtStartDate AND @dtEndDate) 
						OR 
						(DATEADD(MINUTE,-@ClientTimeZoneOffset,dtToDate) BETWEEN @dtStartDate AND @dtEndDate)
					) 
				AND numUserCntID=@numUserCntID  
				AND numDomainID=@numDomainId
				AND numCategoryHDRID NOT IN (SELECT numCategoryHDRID FROM #tempPayrollTracking WHERE ISNULL(numCategoryHDRID,0)>0)
		) x

		-- CALCULATE PAID LEAVE HRS
		SELECT 
			@decTotalPaidLeaveHrs=ISNULL(
											SUM( 
													CASE 
													WHEN dtfromdate = dttodate 
													THEN 
														24 - (CASE WHEN bitfromfullday = 0 THEN 12 ELSE 0 END)
													ELSE 
														(Datediff(DAY,dtfromdate,dttodate) + 1) * 24 - (CASE WHEN bitfromfullday = 0 THEN 12 ELSE 0 END) - (CASE WHEN bittofullday = 0 THEN 12 ELSE 0 END)
														END
												)
											,0)
		FROM   
			TimeAndExpense
		WHERE  
			numtype = 3 
			AND numcategory = 3 
			AND numusercntid = @numUserCntID 
			AND numdomainid = @numDomainId 
			AND (
					(DATEADD(MINUTE,-@ClientTimeZoneOffset,dtFromDate) BETWEEN @dtStartDate AND @dtEndDate)
					Or 
					(DATEADD(MINUTE,-@ClientTimeZoneOffset,dtToDate) BETWEEN @dtStartDate AND @dtEndDate)
				)
			AND numCategoryHDRID NOT IN (SELECT numCategoryHDRID FROM #tempPayrollTracking WHERE ISNULL(numCategoryHDRID,0)>0)		

		-- CALCULATE EXPENSES
		SELECT 
			@monExpense=ISNULL((SUM(CAST(monAmount AS FLOAT))),0)    
		FROM 
			TimeAndExpense 
		WHERE 
			numCategory=2 
			AND numType in (1,2) 
			AND numUserCntID=@numUserCntID 
			AND numDomainID=@numDomainId 
			AND ((dtFromDate BETWEEN @dtStartDate And @dtEndDate) Or (dtToDate BETWEEN @dtStartDate And @dtEndDate)) 
			AND numCategoryHDRID NOT IN (SELECT numCategoryHDRID FROM #tempPayrollTracking WHERE ISNULL(numCategoryHDRID,0)>0)		

		-- CALCULATE REIMBURSABLE EXPENSES
		SELECT 
			@monReimburse=Isnull((SUM(CAST(monamount AS FLOAT))),0)
		FROM   
			TimeAndExpense 
		WHERE  
			bitreimburse = 1 
			AND numcategory = 2 
			AND numusercntid = @numUserCntID 
			AND numdomainid = @numDomainId
			AND dtfromdate BETWEEN @dtStartDate AND @dtEndDate
			AND numCategoryHDRID NOT IN (SELECT numCategoryHDRID FROM #tempPayrollTracking WHERE ISNULL(numCategoryHDRID,0)>0)        

		-- CALCULATE COMMISSION PAID INVOICE
		SELECT 
			@monCommPaidInvoice=ISNULL(SUM(Amount),0) 
		FROM 
			(
				SELECT 
					oppBiz.numOppID,
					oppBiz.numOppBizDocsID,
					ISNULL(SUM(BC.numComissionAmount - ISNULL(PT.monCommissionAmt,0)),0) AS Amount
				FROM 
					OpportunityMaster Opp 
				INNER JOIN OpportunityBizDocs oppBiz on oppBiz.numOppId=opp.numOppId 
				INNER JOIN BizDocComission BC on BC.numOppBizDocId=oppBiz.numOppBizDocsId 
				LEFT JOIN #tempPayrollTracking PT ON PT.numComissionID=BC.numComissionID
				CROSS APPLY
				(
					SELECT 
						MAX(DM.dtDepositDate) dtDepositDate
					FROM 
						DepositMaster DM 
					JOIN dbo.DepositeDetails DD	ON DM.numDepositId=DD.numDepositID 
					WHERE 
						DM.tintDepositePage = 2
						AND DM.numDomainId=@numDomainId
						AND DD.numOppID=oppBiz.numOppID 
						AND DD.numOppBizDocsID =oppBiz.numOppBizDocsID
				) TEMPDeposit
				WHERE 
					Opp.numDomainId=@numDomainId 
					AND (BC.numUserCntId=@numUserCntID AND ISNULL(BC.tintAssignTo,0) <> 3)
					AND BC.bitCommisionPaid=0 
					AND oppBiz.bitAuthoritativeBizDocs = 1 
					AND isnull(oppBiz.monAmountPaid,0) >=oppBiz.monDealAmount 
					AND CAST(DATEADD(MINUTE,-@ClientTimeZoneOffset,TEMPDeposit.dtDepositDate) AS DATE) BETWEEN CAST(@dtStartDate AS DATE) AND CAST(@dtEndDate AS DATE)
				GROUP BY 
					oppBiz.numOppID,
					oppBiz.numOppBizDocsID
			) A

		-- CALCULATE COMMISSION UNPAID INVOICE 
		SELECT 
			@monCommUNPaidInvoice=isnull(sum(BC.numComissionAmount - ISNULL(PT.monCommissionAmt,0)),0) 
		FROM 
			OpportunityMaster Opp 
		INNER JOIN OpportunityBizDocs oppBiz on oppBiz.numOppId=opp.numOppId 
		INNER JOIN BizDocComission BC on BC.numOppBizDocId=oppBiz.numOppBizDocsId
		LEFT JOIN #tempPayrollTracking PT ON PT.numComissionID=BC.numComissionID 
		WHERE 
			Opp.numDomainId=@numDomainId 
			AND (BC.numUserCntId=@numUserCntID AND ISNULL(BC.tintAssignTo,0) <> 3)
			AND BC.bitCommisionPaid=0 
			AND oppBiz.bitAuthoritativeBizDocs = 1 
			AND ISNULL(oppBiz.monAmountPaid,0) <oppBiz.monDealAmount
			AND ((DATEADD(MINUTE,-@ClientTimeZoneOffset,oppBiz.dtFromDate) BETWEEN @dtStartDate AND @dtEndDate))	

		SET @monCommPaidCreditMemoOrRefund = ISNULL((SELECT SUM(monCommission) FROM @TempCommissionPaidCreditMemoOrRefund WHERE (numUserCntId=@numUserCntID AND ISNULL(tintAssignTo,0) <> 3)),0)


		SET @GrossProfit = ISNULL((SELECT
										SUM(dbo.GetOrderItemProfitAmountOrMargin(@numDomainID,OM.numOppId,OI.numoppitemtCode,1)) Profit
									FROM
										OpportunityMaster OM
									INNER JOIN
										OpportunityItems OI
									ON
										OI.numOppId = OM.numOppID
									INNER JOIN
										Item I
									ON
										OI.numItemCode = I.numItemCode
									WHERE
										OM.numDomainId=@numDomainID
										AND OM.numAssignedTo = @numUserCntID
										AND ISNULL(OI.monTotAmount,0) <> 0
										AND ISNULL(OI.numUnitHour,0) <> 0
										AND ISNULL(OM.tintOppType,0)=1
										AND ISNULL(OM.tintOppStatus,0)=1
										AND DATEADD(MINUTE,-@ClientTimeZoneOffset,OM.bintCreatedDate) BETWEEN @dtStartDate AND @dtEndDate
										AND I.numItemCode NOT IN (@numShippingItemID,@numDiscountItemID)),0)

		UPDATE 
			@TEMP 
		SET    
			TotalHrsWorked=@decTotalHrsWorked,
			TotalPayroll= @decTotalHrsWorked * monHourlyRate + @monReimburse + @monCommPaidInvoice + @monCommUNPaidInvoice - ISNULL(@monCommPaidCreditMemoOrRefund,0),
			ProfitMinusPayroll = @GrossProfit - (@decTotalHrsWorked * monHourlyRate + @monReimburse + @monCommPaidInvoice + @monCommUNPaidInvoice - ISNULL(@monCommPaidCreditMemoOrRefund,0))
		WHERE 
			numUserCntID=@numUserCntID 

		SET @i = @i + 1
	END

	SELECT vcUserName,TotalHrsWorked FROM @TEMP ORDER BY TotalHrsWorked DESC
	SELECT vcUserName,TotalPayroll FROM @TEMP ORDER BY TotalPayroll DESC
	SELECT vcUserName,ProfitMinusPayroll FROM @TEMP ORDER BY ProfitMinusPayroll DESC
	DROP TABLE #tempPayrollTracking
END
GO

