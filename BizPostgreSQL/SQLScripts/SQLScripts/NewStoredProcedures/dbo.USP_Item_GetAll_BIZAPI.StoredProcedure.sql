GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_Item_GetAll_BIZAPI' ) 
    DROP PROCEDURE USP_Item_GetAll_BIZAPI
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author: Sandeep Patel
-- Create date: 9 April 2014
-- Description:	Gets list of items domain wise
-- =============================================
CREATE PROCEDURE USP_Item_GetAll_BIZAPI
	@numDomainID NUMERIC(18,0) = NULL,
	@numPageIndex AS INT = 0,
    @numPageSize AS INT = 0,
    @TotalRecords INT OUTPUT,
	@dtCreatedAfter DateTime = NULL,
	@dtModifiedAfter DateTime = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @TEMP TABLE
	(
		numItemCode NUMERIC(18,0)
	)

	INSERT INTO @TEMP
	(
		numItemCode
	)
	SELECT DISTINCT
		numItemCode
	FROM 
		Item 
	LEFT JOIN
		WareHouseItems
	ON
		Item.numItemCode=numItemID
	WHERE 
		Item.numDomainID = @numDomainID
		AND (@dtCreatedAfter IS NULL OR Item.bintCreatedDate > @dtCreatedAfter)
		AND (@dtModifiedAfter IS NULL OR Item.bintModifiedDate > @dtModifiedAfter OR DATEADD(hh, DATEDIFF(hh, GETDATE(), GETUTCDATE()), dtModified) > @dtModifiedAfter)

	SET @TotalRecords = ISNULL((SELECT COUNT(*) FROM @TEMP),0)

	SELECT
		ROW_NUMBER() OVER (ORDER BY Item.numItemCode asc) AS RowNumber,
		Item.numItemCode,
		Item.vcItemName,
		(SELECT TOP 1 vcPathForImage FROM ItemImages WHERE numItemCode = Item.numItemCode AND ISNULL(bitDefault,0) = 1) AS vcPathForImage,
		(SELECT TOP 1 vcPathForTImage FROM ItemImages WHERE numItemCode = Item.numItemCode AND ISNULL(bitDefault,0) = 1) AS vcPathForTImage,
		Item.txtItemDesc,
		Item.charItemType,
		CASE 
			WHEN Item.charItemType='P' THEN 'Inventory Item' 
			WHEN Item.charItemType='N' THEN 'Non Inventory Item' 
			WHEN Item.charItemType='S' THEN 'Service' 
			WHEN Item.charItemType='A' THEN 'Accessory' 
		END AS ItemType,
		Item.vcSKU,
		Item.vcModelID,
		Item.numBarCodeId,
		Item.vcManufacturer,
		Item.vcUnitofMeasure,
		Item.monListPrice,
		(CASE WHEN ISNULL(Item.bitVirtualInventory,0) = 1 THEN 0 ELSE ISNULL(Item.monAverageCost,0.00) END) AS monAverageCost,
		Item.monCampaignLabourCost,
		Item.numItemGroup,
		Item.numItemClass,
		Item.numShipClass,
		Item.numItemClassification,
		Item.fltWidth,
		Item.fltWeight,
		Item.fltHeight,
		Item.fltLength,
		Item.numBaseUnit,
		Item.numSaleUnit,
		Item.numPurchaseUnit,
		Item.bitKitParent,
		Item.bitLotNo,
		Item.bitAssembly,
		Item.bitSerialized,
		Item.bitTaxable,
		Item.bitAllowBackOrder,
		Item.bitFreeShipping,
		Item.numCOGsChartAcntId,
		Item.numAssetChartAcntId,
		Item.numIncomeChartAcntId,
		Item.numVendorID,
		WI.numOnHand,                      
		WI.numOnOrder,                      
		WI.numReorder,                      
		WI.numAllocation,                      
		WI.numBackOrder,
		bintCreatedDate,
		CASE WHEN DATEADD(hh, DATEDIFF(hh, GETDATE(), GETUTCDATE()), WI.dtModified) > bintModifiedDate THEN DATEADD(hh, DATEDIFF(hh, GETDATE(), GETUTCDATE()), WI.dtModified) ELSE bintModifiedDate END AS bintModifiedDate,
		STUFF((SELECT CONCAT(',',numCategoryID) FROM ItemCategory WHERE numItemID=Item.numItemCode GROUP BY numCategoryID FOR XML PATH('')),1,1,'') AS vcCategories
	INTO
		#Temp
	FROM
		Item WITH (NOLOCK)
	INNER JOIN
		@TEMP T1
	ON
		Item.numItemCode = T1.numItemCode
	OUTER APPLY
	(
		SELECT
			SUM(numOnHand) AS numOnHand,                      
			SUM(numOnOrder) AS numOnOrder,                      
			SUM(numReorder) AS numReorder,                      
			SUM(numAllocation) AS numAllocation,                      
			SUM(numBackOrder) AS numBackOrder,
			MAX(dtModified) dtModified
		FROM
			WarehouseItems
		WHERE
			WarehouseItems.numItemID=Item.numItemCode
	) WI
	ORDER BY
		Item.numItemCode
	OFFSET (CASE WHEN ISNULL(@numPageIndex,0) > 0 THEN (@numPageIndex - 1) ELSE 0 END) * @numPageSize ROWS FETCH NEXT (CASE WHEN ISNULL(@numPageSize,0) > 0 THEN @numPageSize ELSE 999999999 END) ROWS ONLY

	SELECT * FROM #Temp

	--GET CHILD ITEMS
	SELECT
		ItemDetails.numItemKitID,
		ItemDetails.numChildItemID,
		ItemDetails.numQtyItemsReq,
		(SELECT vcUnitName FROM UOM WHERE numUOMId = ItemDetails.numUOMId) AS vcUnitName
	FROM
		#Temp T1
	INNER JOIN
		ItemDetails WITH (NOLOCK)
	ON
		T1.numItemCode = ItemDetails.numItemKitID

	--GET CUSTOM FIELDS
	SELECT 
		I.numItemCode,
		TEMPCustFields.Fld_id AS Id,
		TEMPCustFields.Fld_label AS Name,
		TEMPCustFields.Fld_type AS [Type],
		TEMPCustFields.Fld_Value AS ValueId,
		CASE 
			WHEN TEMPCustFields.Fld_type='TextBox' or TEMPCustFields.Fld_type='TextArea' THEN
				(CASE WHEN TEMPCustFields.Fld_Value='0' OR TEMPCustFields.Fld_Value='' THEN '-' ELSE TEMPCustFields.Fld_Value END)
			WHEN TEMPCustFields.Fld_type='SelectBox' THEN
				(CASE WHEN TEMPCustFields.Fld_Value='' THEN '-' ELSE dbo.GetListIemName(TEMPCustFields.Fld_Value) END)
			WHEN TEMPCustFields.Fld_type='CheckBox' THEN
				(CASE WHEN TEMPCustFields.Fld_Value=0 THEN 'No' ELSE 'Yes' END)
			ELSE
				''
		END AS Value
	FROM 
		#Temp I
	OUTER APPLY
	(
		SELECT
			CFW_Fld_Master.Fld_id,
			CFW_Fld_Master.Fld_label,
			CFW_Fld_Master.Fld_type,
			CFW_FLD_Values_Item.Fld_Value
		FROM
			CFW_Fld_Master WITH (NOLOCK)
		LEFT JOIN
			CFW_FLD_Values_Item WITH (NOLOCK)
		ON
			CFW_FLD_Values_Item.Fld_ID = CFW_Fld_Master.Fld_id
			AND I.numItemCode = CFW_FLD_Values_Item.RecId
		WHERE 
			numDomainID = @numDomainID
			AND Grp_id = 5
	) TEMPCustFields

	DROp TABLE #Temp
END
GO


