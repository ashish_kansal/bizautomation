IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetApprovalConfig')
DROP PROCEDURE USP_GetApprovalConfig
GO
CREATE PROCEDURE [dbo].[USP_GetApprovalConfig]          
  @chrAction char(10)=NULL,
  @numDomainId Numeric(18,2)=0,
  @numUserId Numeric(18,2)=0,
  @numModuleId Numeric(18,2)=0,
  @numConfigId Numeric(18,2)=0      
AS                                          
BEGIN
	IF @chrAction='VU'
	BEGIN
		SELECT 
			U.numUserDetailId AS numUserId
			,CONCAT(ISNULL(vcFirstName,'-'),' ',ISNULL(vcLastName,'-')) vcUserName
		FROM 
			UserMaster AS U
		INNER JOIN
			AdditionalContactsInformation ACI
		ON
			U.numUserDetailId = ACI.numContactId
		WHERE
			U.numDomainID=@numDomainId AND U.bitActivateFlag=1
	END
	ELSE IF @chrAction='V'
	BEGIN
		SELECT 
			U.numUserDetailId AS numUserId,CONCAT(ISNULL(vcFirstName,'-'),' ',ISNULL(vcLastName,'-')) vcUserName,ISNULL(AP.numLevel1Authority,0) AS numLevel1Authority,ISNULL(AP.numLevel2Authority,0) AS numLevel2Authority,
			ISNULL(AP.numLevel3Authority,0) AS numLevel3Authority,ISNULL(AP.numLevel4Authority,0) AS numLevel4Authority,
			ISNULL(AP.numLevel5Authority,0) AS numLevel5Authority,AP.numConfigId
		FROM 
			UserMaster AS U
		INNER JOIN
			AdditionalContactsInformation ACI
		ON
			U.numUserDetailId = ACI.numContactId
		LEFT JOIN 
			ApprovalConfig AS AP
		ON
			U.numUserDetailId=AP.numUserId AND AP.numModule=@numModuleId
		WHERE
			U.numDomainID=@numDomainId AND U.bitActivateFlag=1
	END
	ELSE
	BEGIN
		SELECT 
			* 
		FROM 
			ApprovalConfig 
		WHERE 
			numDomainID=@numDomainId 
			AND numModule=@numModuleId
			AND (numUserId=@numUserId OR ISNULl(@numUserId,0) = 0) 
			AND (numConfigID=@numConfigId OR ISNULL(@numConfigId,0) = 0)
	END
END
GO