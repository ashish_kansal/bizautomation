
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetCOAShippingMapping')
DROP PROCEDURE USP_GetCOAShippingMapping
GO
CREATE PROCEDURE [dbo].[USP_GetCOAShippingMapping]
    @numShippingMethodID NUMERIC(9),
    @numDomainID NUMERIC(9)
AS 
    SET NOCOUNT ON

    SELECT  [numShippingMappingID],
            [numShippingMethodID],
            [numIncomeAccountID],
            COA.[vcAccountName],
             SM.[numDomainID]
    FROM    COAShippingMapping SM
            INNER JOIN [Chart_Of_Accounts] COA ON COA.[numAccountId] = SM.[numIncomeAccountID]
    WHERE   [numShippingMethodID] = @numShippingMethodID
            AND SM.[numDomainID] = @numDomainID