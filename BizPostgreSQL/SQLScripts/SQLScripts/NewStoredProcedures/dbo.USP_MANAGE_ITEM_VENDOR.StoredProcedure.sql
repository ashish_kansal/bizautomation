GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_MANAGE_ITEM_VENDOR')
DROP PROCEDURE USP_MANAGE_ITEM_VENDOR
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE dbo.USP_MANAGE_ITEM_VENDOR
(
	@numVendorTcode		NUMERIC(18,0) OUTPUT,
	@numItemKitID		NUMERIC(18,0),
	@numVendorID		NUMERIC(18,0),
	@vcPartNo			VARCHAR(100),
	@numDomainID		NUMERIC(18, 0),
	@monCost			DECIMAL(20,5) ,
	@intMinQty			INT,
	@bitIsPrimaryVendor	BIT,
	@intMode			BIT,
	@vcNotes VARCHAR(300) = ''
)
AS 
BEGIN
	IF (SELECT COUNT(*) FROM Vendor WHERE numDomainID = @numDomainID AND numItemCode = @numItemKitID) = 0
	BEGIN
		SET @bitIsPrimaryVendor = 1
	END

	IF @intMode = 0
	BEGIN		
		SELECT @numVendorTcode = numVendorTcode FROM dbo.Vendor  WHERE numDomainID = @numDomainID AND numItemCode = @numItemKitID AND numVendorID = @numVendorID
		
		IF ISNULL(@numVendorTcode,0) = 0    
			BEGIN
				INSERT INTO  dbo.Vendor ( numVendorID, vcPartNo, numDomainID, monCost, numItemCode, intMinQty, vcNotes ) 
								 VALUES ( @numVendorID,@vcPartNo,@numDomainID,@monCost,@numItemKitID,@intMinQty, @vcNotes) 	
			
				 PRINT @numVendorTcode 
				 SET @numVendorTcode = SCOPE_IDENTITY()		 
				 PRINT @numVendorTcode 
			END
		ELSE
			BEGIN
					SELECT  @numItemKitID = numItemCode, @numDomainID = numDomainID, @numVendorID = numVendorID 
					FROM dbo.Vendor WHERE numVendorTcode = @numVendorTcode
											
					UPDATE dbo.Vendor	SET	vcPartNo = @vcPartNo, monCost = @monCost, intMinQty = @intMinQty, vcNotes=@vcNotes
					WHERE 1=1
					AND numVendorTcode = @numVendorTcode
					AND numDomainID = @numDomainID						
			END 
		
		IF @bitIsPrimaryVendor = 1
			BEGIN
				UPDATE item SET numVendorID = @numVendorID WHERE numItemCode = @numItemKitID AND numDomainID = @numDomainID
			END			 					
	END
	
--	 PRINT @numVendorTcode 
--	 SET @numVendorTcode = SCOPE_IDENTITY()		 
--	 PRINT @numVendorTcode 
	 
	IF @intMode = 1
	BEGIN
		DELETE FROM dbo.Vendor WHERE 1=1 AND numVendorTcode = @numVendorTcode
	END
END