GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_AccountsReports')
DROP PROCEDURE USP_AccountsReports
GO
CREATE PROCEDURE [dbo].[USP_AccountsReports]     
    @numDomainID numeric(18, 0),
    @dtFromDate DATETIME,
    @dtToDate DATETIME,
    @tintReportType TINYINT,
    @numAccountClass AS NUMERIC(9)=0
as                 

IF @tintReportType=1 --Report Name: Check Register
BEGIN
	SELECT CH.numCheckNo,CH.dtCheckDate,CH.monAmount,CH.numDivisionID,C.vcCompanyName,COA.vcAccountName
	FROM dbo.CheckHeader CH 
	JOIN dbo.DivisionMaster D ON CH.numDivisionID=D.numDivisionID
	JOIN dbo.CompanyInfo C ON D.numCompanyID = C.numCompanyId
	JOIN dbo.Chart_Of_Accounts COA ON CH.numChartAcntId = COA.numAccountId AND COA.numDomainId=@numDomainID
	WHERE CH.numDomainID=@numDomainID --AND ISNULL(bitIsPrint,0)=0 
	AND CH.dtCheckDate BETWEEN  @dtFromDate AND @dtToDate
	AND (CH.numAccountClass=@numAccountClass OR @numAccountClass=0)
	order by dtCheckDate,numCheckNo
END

ELSE IF @tintReportType=2 --Report Name: Sales Journal Detail (By GL Account)
BEGIN
	SELECT vcAccountName,SUM(ISNULL(numCreditAmt,0)) AS numCreditAmt,SUM(ISNULL(numDebitAmt,0)) AS numDebitAmt 
	FROM (
	SELECT GJD.numChartAcntId,COA.vcAccountName,ISNULL(GJD.numCreditAmt,0) AS numCreditAmt,ISNULL(GJD.numDebitAmt,0) AS numDebitAmt 
	FROM dbo.General_Journal_Header GJH JOIN dbo.General_Journal_Details GJD ON GJH.numJournal_Id = GJD.numJournalId
	JOIN dbo.OpportunityMaster OM ON OM.numOppId=GJH.numOppId AND OM.tintOppType=1 AND OM.numDomainId=@numDomainID
	JOIN dbo.OpportunityBizDocs OBD ON OM.numOppId=OBD.numOppId AND OBD.numOppBizDocsId=GJH.numOppBizDocsId 
	AND OBD.bitAuthoritativeBizDocs=1
	JOIN dbo.Chart_Of_Accounts COA ON GJD.numChartAcntId = COA.numAccountId AND COA.numDomainId=@numDomainID
	WHERE GJH.numDomainId=@numDomainID
	AND GJH.datEntry_Date BETWEEN  @dtFromDate AND @dtToDate
	AND (OM.numAccountClass=@numAccountClass OR @numAccountClass=0)
	
	UNION ALL 
	
	SELECT GJD.numChartAcntId,COA.vcAccountName,ISNULL(GJD.numCreditAmt,0) AS numCreditAmt,ISNULL(GJD.numDebitAmt,0) AS numDebitAmt 
	FROM dbo.General_Journal_Header GJH JOIN dbo.General_Journal_Details GJD ON GJH.numJournal_Id = GJD.numJournalId
	JOIN dbo.ReturnHeader RH ON RH.numDomainID=@numDomainID AND RH.numReturnHeaderID=GJH.numReturnID
	JOIN dbo.Chart_Of_Accounts COA ON GJD.numChartAcntId = COA.numAccountId AND COA.numDomainId=@numDomainID
	WHERE GJH.numDomainId=@numDomainID AND RH.tintReceiveType=2 AND RH.tintReturnType IN(1,3) AND  RH.numDomainId=@numDomainID
	AND GJH.datEntry_Date BETWEEN  @dtFromDate AND @dtToDate
	AND (RH.numAccountClass=@numAccountClass OR @numAccountClass=0)) AS T
	GROUP BY numChartAcntId,vcAccountName ORDER BY vcAccountName
END

ELSE IF @tintReportType=3 --Report Name: Purchase Journal
BEGIN
SELECT vcAccountName,SUM(ISNULL(numCreditAmt,0)) AS numCreditAmt,SUM(ISNULL(numDebitAmt,0)) AS numDebitAmt
FROM (
	SELECT GJD.numChartAcntId,COA.vcAccountName,ISNULL(GJD.numCreditAmt,0) AS numCreditAmt,ISNULL(GJD.numDebitAmt,0) AS numDebitAmt 
	FROM dbo.General_Journal_Header GJH JOIN dbo.General_Journal_Details GJD ON GJH.numJournal_Id = GJD.numJournalId
	JOIN dbo.OpportunityMaster OM ON OM.numOppId=GJH.numOppId AND OM.tintOppType=2 AND OM.numDomainId=@numDomainID
	JOIN dbo.OpportunityBizDocs OBD ON OM.numOppId=OBD.numOppId AND OBD.numOppBizDocsId=GJH.numOppBizDocsId AND OBD.bitAuthoritativeBizDocs=1
	JOIN dbo.Chart_Of_Accounts COA ON GJD.numChartAcntId = COA.numAccountId AND COA.numDomainId=@numDomainID
	WHERE GJH.numDomainId=@numDomainID 
	AND GJH.datEntry_Date BETWEEN  @dtFromDate AND @dtToDate
	AND (OM.numAccountClass=@numAccountClass OR @numAccountClass=0)
	
	UNION ALL
	
	SELECT GJD.numChartAcntId,COA.vcAccountName,ISNULL(GJD.numCreditAmt,0) AS numCreditAmt,ISNULL(GJD.numDebitAmt,0) AS numDebitAmt 
	FROM dbo.General_Journal_Header GJH JOIN dbo.General_Journal_Details GJD ON GJH.numJournal_Id = GJD.numJournalId
	JOIN dbo.BillHeader BH ON BH.numBillID=GJH.numBillID AND BH.numDomainId=@numDomainID
	JOIN dbo.Chart_Of_Accounts COA ON GJD.numChartAcntId = COA.numAccountId AND COA.numDomainId=@numDomainID
	WHERE GJH.numDomainId=@numDomainID 
	AND GJH.datEntry_Date BETWEEN  @dtFromDate AND @dtToDate
	AND (BH.numAccountClass=@numAccountClass OR @numAccountClass=0)) AS T
	GROUP BY numChartAcntId,vcAccountName ORDER BY vcAccountName
END

ELSE IF @tintReportType=4 --Report Name: Invoice Register
BEGIN

	SELECT OM.vcPOppName,OBD.vcBizDocID,OBD.dtFromDate AS FromDate,C.vcCompanyName,OM.monDealAMount,OBD.monDealAMount AS monBizDocAmount,OBD.monAmountPaid
	FROM OpportunityMaster OM
	JOIN dbo.OpportunityBizDocs OBD ON OM.numOppId=OBD.numOppId AND OBD.bitAuthoritativeBizDocs=1 
	JOIN dbo.DivisionMaster D ON OM.numDivisionID=D.numDivisionID
	JOIN dbo.CompanyInfo C ON D.numCompanyID = C.numCompanyId
	WHERE OM.tintOppType=1 AND OM.numDomainId=@numDomainID
	AND OBD.dtFromDate BETWEEN  @dtFromDate AND @dtToDate
	AND (OM.numAccountClass=@numAccountClass OR @numAccountClass=0)

	UNION ALL

	SELECT RH.vcRMA,RH.vcBizDocName,RH.dtCreatedDate AS FromDate,C.vcCompanyName,DM.monDepositAmount * -1 AS monDealAMount,0 AS monBizDocAmount,0 AS monAmountPaid 
	FROM dbo.ReturnHeader RH
	JOIN dbo.DepositMaster DM ON RH.numReturnHeaderID=DM.numReturnHeaderID AND DM.numDomainID=@numDomainID
	JOIN dbo.DivisionMaster D ON RH.numDivisionID=D.numDivisionID
	JOIN dbo.CompanyInfo C ON D.numCompanyID = C.numCompanyId
	WHERE RH.tintReceiveType=2 AND  RH.numDomainId=@numDomainID
	AND RH.dtCreatedDate BETWEEN  @dtFromDate AND @dtToDate
	AND (RH.numAccountClass=@numAccountClass OR @numAccountClass=0)
	ORDER BY FromDate

END

ELSE IF @tintReportType=5 --Report Name: Receipt Journal (Summary format)
BEGIN
	
	SELECT COA.vcAccountName,SUM(ISNULL(GJD.numCreditAmt,0)) AS numCreditAmt,SUM(ISNULL(GJD.numDebitAmt,0)) AS numDebitAmt 
	FROM dbo.General_Journal_Header GJH JOIN dbo.General_Journal_Details GJD ON GJH.numJournal_Id = GJD.numJournalId
	JOIN dbo.Chart_Of_Accounts COA ON GJD.numChartAcntId = COA.numAccountId AND COA.numDomainId=@numDomainID
	WHERE ISNULL(GJH.numDepositId,0)>0 AND GJH.numDomainId=@numDomainID
	AND GJH.datEntry_Date BETWEEN  @dtFromDate AND @dtToDate
	AND (GJH.numAccountClass=@numAccountClass OR @numAccountClass=0)
	GROUP BY GJD.numChartAcntId,COA.vcAccountName 
	ORDER BY COA.vcAccountName

END

ELSE IF @tintReportType=6 --Report Name: Total Receipts (Detailed)
BEGIN

	SELECT DM.dtDepositDate AS datEntry_Date,COA.vcAccountName,GJD.varDescription AS Description,numDebitAmt,numCreditAmt,
	DM.numDepositId AS numReference,GJD.tintReferenceType
	FROM dbo.DepositMaster DM JOIN dbo.General_Journal_Header GJH ON DM.numDepositId=GJH.numDepositId
	JOIN dbo.General_Journal_Details GJD ON GJH.numJournal_Id = GJD.numJournalId AND GJD.tintReferenceType=6
	JOIN dbo.Chart_Of_Accounts COA ON GJD.numChartAcntId = COA.numAccountId AND COA.numDomainId=@numDomainID
	WHERE DM.numDomainID=@numDomainID /*AND DM.tintDepositeToType=2 AND DM.tintDepositePage=2*/
	AND DM.dtDepositDate BETWEEN  @dtFromDate AND @dtToDate
	AND (DM.numAccountClass=@numAccountClass OR @numAccountClass=0)
	
	UNION ALL
	SELECT DM.dtDepositDate AS datEntry_Date,COA.vcAccountName,C.vcCompanyName AS Description,numDebitAmt,numCreditAmt,
	DM.numDepositId AS numReference,GJD.tintReferenceType
	FROM dbo.DepositMaster DM JOIN dbo.General_Journal_Header GJH ON DM.numDepositId=GJH.numDepositId
	JOIN dbo.General_Journal_Details GJD ON GJH.numJournal_Id = GJD.numJournalId AND GJD.tintReferenceType=7
	JOIN dbo.Chart_Of_Accounts COA ON GJD.numChartAcntId = COA.numAccountId AND COA.numDomainId=@numDomainID
	LEFT JOIN dbo.DivisionMaster D ON GJD.numCustomerId=D.numDivisionID
	LEFT JOIN dbo.CompanyInfo C ON D.numCompanyID = C.numCompanyId
	WHERE DM.numDomainID=@numDomainID /*AND DM.tintDepositeToType=2 AND DM.tintDepositePage=2*/
	AND DM.dtDepositDate BETWEEN  @dtFromDate AND @dtToDate
	AND (DM.numAccountClass=@numAccountClass OR @numAccountClass=0)
	
	UNION ALL
	SELECT DM.dtDepositDate AS datEntry_Date,NULL,OM.vcPOppName + ' : ' + OBD.vcbizdocid + ' (' + CAST(DD.monAmountPaid AS VARCHAR(20)) +')',NULL,NULL,
	DM.numDepositId AS numReference,8 AS tintReferenceType
	FROM dbo.DepositMaster DM JOIN dbo.DepositeDetails DD ON DM.numDepositId = DD.numDepositID 
	JOIN dbo.OpportunityBizDocs OBD ON DD.numOppBizDocsID = OBD.numOppBizDocsId
	JOIN dbo.OpportunityMaster OM ON OBD.numOppId = OM.numOppId AND DD.numOppID=OM.numOppId
	WHERE DM.numDomainID=@numDomainID /*AND DM.tintDepositeToType=2 AND DM.tintDepositePage=2*/
	AND DM.dtDepositDate BETWEEN  @dtFromDate AND @dtToDate
	AND (DM.numAccountClass=@numAccountClass OR @numAccountClass=0)
	ORDER BY datEntry_Date,numReference,tintReferenceType	
	
END

ELSE IF @tintReportType=7 --Report Name: Total Disbursement Journal (Summary)
BEGIN

	SELECT COA.vcAccountName,SUM(ISNULL(GJD.numCreditAmt,0)) AS numCreditAmt,SUM(ISNULL(GJD.numDebitAmt,0)) AS numDebitAmt 
	FROM dbo.General_Journal_Header GJH JOIN dbo.General_Journal_Details GJD ON GJH.numJournal_Id = GJD.numJournalId
	JOIN dbo.Chart_Of_Accounts COA ON GJD.numChartAcntId = COA.numAccountId AND COA.numDomainId=@numDomainID
	WHERE (ISNULL(GJH.numBillPaymentID,0)>0 OR ISNULL(GJH.numCheckHeaderID,0)>0) AND GJH.numDomainId=@numDomainID 
	AND GJH.datEntry_Date BETWEEN  @dtFromDate AND @dtToDate
	AND (GJH.numAccountClass=@numAccountClass OR @numAccountClass=0)
	GROUP BY GJD.numChartAcntId,COA.vcAccountName 
	ORDER BY COA.vcAccountName
	
END

ELSE IF @tintReportType=8 --Report Name: Cash disbursement Journal (Detailed)
BEGIN

	SELECT ISNULL(GJH.numCheckHeaderID,0) AS numCheckHeaderID,ISNULL(GJH.numBillPaymentID,0) AS numBillPaymentID,
	GJH.datEntry_Date,COA.vcAccountName,numDebitAmt,numCreditAmt,GJD.tintReferenceType INTO #tempGJ 
	FROM dbo.General_Journal_Header GJH JOIN dbo.General_Journal_Details GJD ON GJH.numJournal_Id = GJD.numJournalId
	JOIN dbo.Chart_Of_Accounts COA ON GJD.numChartAcntId = COA.numAccountId AND COA.numDomainId=@numDomainID
	WHERE GJH.numDomainID=@numDomainID AND GJH.datEntry_Date BETWEEN  @dtFromDate AND @dtToDate
	AND (ISNULL(GJH.numCheckHeaderID,0)>0 OR ISNULL(GJH.numBillPaymentID,0)>0)
	AND (GJH.numAccountClass=@numAccountClass OR @numAccountClass=0)
	
	SELECT GJH.datEntry_Date,CH.numCheckNo,GJH.vcAccountName,'' AS Description,numDebitAmt,numCreditAmt,CH.numCheckHeaderID AS numReference,GJH.tintReferenceType
	FROM dbo.CheckHeader CH JOIN #tempGJ GJH ON CH.numCheckHeaderID=GJH.numCheckHeaderID AND GJH.tintReferenceType=1
	WHERE CH.numDomainID=@numDomainID AND CH.tintReferenceType=1
	AND (CH.numAccountClass=@numAccountClass OR @numAccountClass=0)
		
	UNION ALL
	SELECT GJH.datEntry_Date AS datEntry_Date,NULL,GJH.vcAccountName,C.vcCompanyName AS Description,numDebitAmt,numCreditAmt,CH.numCheckHeaderID AS numReference,GJH.tintReferenceType
	FROM dbo.CheckHeader CH JOIN #tempGJ GJH ON CH.numCheckHeaderID=GJH.numCheckHeaderID AND GJH.tintReferenceType=2
	JOIN dbo.DivisionMaster D ON CH.numDivisionID=D.numDivisionID
	JOIN dbo.CompanyInfo C ON D.numCompanyID = C.numCompanyId
	WHERE CH.numDomainID=@numDomainID AND CH.tintReferenceType=1
	AND (CH.numAccountClass=@numAccountClass OR @numAccountClass=0)
	
	UNION ALL
	SELECT BPH.dtPaymentDate AS datEntry_Date,CH.numCheckNo,GJH.vcAccountName,'' AS Description,numDebitAmt,numCreditAmt,BPH.numBillPaymentID,GJH.tintReferenceType
	FROM dbo.BillPaymentHeader BPH JOIN #tempGJ GJH ON BPH.numBillPaymentID=GJH.numBillPaymentID AND GJH.tintReferenceType=8
	LEFT JOIN dbo.CheckHeader CH ON CH.numReferenceID=BPH.numBillPaymentID AND CH.tintReferenceType=8
	WHERE BPH.numDomainID=@numDomainID 
	AND BPH.dtPaymentDate BETWEEN  @dtFromDate AND @dtToDate
	AND (BPH.numAccountClass=@numAccountClass OR @numAccountClass=0)
	
	UNION ALL
	SELECT BPH.dtPaymentDate AS datEntry_Date,NULL AS numCheckNo,GJH.vcAccountName,C.vcCompanyName AS Description,numDebitAmt,numCreditAmt,BPH.numBillPaymentID,GJH.tintReferenceType
	FROM dbo.BillPaymentHeader BPH JOIN #tempGJ GJH ON BPH.numBillPaymentID=GJH.numBillPaymentID AND GJH.tintReferenceType=9
	JOIN dbo.DivisionMaster D ON BPH.numDivisionID=D.numDivisionID
	JOIN dbo.CompanyInfo C ON D.numCompanyID = C.numCompanyId
	WHERE BPH.numDomainID=@numDomainID
	AND BPH.dtPaymentDate BETWEEN  @dtFromDate AND @dtToDate
	AND (BPH.numAccountClass=@numAccountClass OR @numAccountClass=0)
	
	UNION ALL
	SELECT BPH.dtPaymentDate AS datEntry_Date,NULL,NULL,OM.vcPOppName + ' : ' + OBD.vcbizdocid + ' (' + CAST(BPD.monAmount AS VARCHAR(20)) +')'
	,NULL,NULL,BPH.numBillPaymentID,10
	FROM dbo.BillPaymentHeader BPH JOIN dbo.BillPaymentDetails BPD ON BPH.numBillPaymentID=BPD.numBillPaymentID
	JOIN dbo.OpportunityBizDocs OBD ON BPD.numOppBizDocsID = OBD.numOppBizDocsId
	JOIN dbo.OpportunityMaster OM ON OBD.numOppId = OM.numOppId
	WHERE BPH.numDomainID=@numDomainID 
	AND BPH.dtPaymentDate BETWEEN  @dtFromDate AND @dtToDate
	AND (BPH.numAccountClass=@numAccountClass OR @numAccountClass=0)
	
	UNION ALL
	SELECT BPH.dtPaymentDate AS datEntry_Date,NULL,NULL,COA.vcAccountName + '(' + CAST(GJD.numDebitAmt AS VARCHAR(20)) + ' : Bill-' + CONVERT(VARCHAR(10),BH.numBillID)+')'
	,NULL,NULL,BPH.numBillPaymentID,10
	FROM dbo.BillPaymentHeader BPH JOIN dbo.BillPaymentDetails BPD ON BPH.numBillPaymentID=BPD.numBillPaymentID
	JOIN dbo.BillHeader BH ON BH.numBillID = BPD.numBillID
	JOIN dbo.General_Journal_Header GJH ON BH.numBillID=GJH.numBillID JOIN dbo.General_Journal_Details GJD ON GJH.numJournal_Id = GJD.numJournalId
	JOIN dbo.Chart_Of_Accounts COA ON GJD.numChartAcntId = COA.numAccountId AND COA.numDomainId=@numDomainID
	WHERE BPH.numDomainID=@numDomainID AND GJD.numDebitAmt>0
	AND BPH.dtPaymentDate BETWEEN  @dtFromDate AND @dtToDate
	AND (BH.numAccountClass=@numAccountClass OR @numAccountClass=0)
	
	ORDER BY datEntry_Date,numReference,tintReferenceType
	
	DROP TABLE #tempGJ
END