GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_MassSalesFulfillment_GetConfiguration')
DROP PROCEDURE dbo.USP_MassSalesFulfillment_GetConfiguration
GO

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[USP_MassSalesFulfillment_GetConfiguration]
(
	@numDomainID NUMERIC(18,0)
)
AS
BEGIN
	DECLARE @numDefaultSalesShippingDoc NUMERIC(18,0)

	SELECT @numDefaultSalesShippingDoc=ISNULL(numDefaultSalesShippingDoc,0) FROM Domain WHERE numDomainId=@numDomainID

	IF EXISTS (SELECT ID FROM MassSalesFulfillmentConfiguration WHERE numDomainID=@numDomainID)
	BEGIN
		SELECT 
			ISNULL(bitGroupByOrderForPick,0) bitGroupByOrderForPick
			,ISNULL(bitGroupByOrderForShip,0) bitGroupByOrderForShip
			,ISNULL(bitGroupByOrderForInvoice,0) bitGroupByOrderForInvoice
			,ISNULL(bitGroupByOrderForPay,0) bitGroupByOrderForPay
			,ISNULL(bitGroupByOrderForClose,0) bitGroupByOrderForClose
			,ISNULL(tintInvoicingType,1) tintInvoicingType
			,ISNULL(tintScanValue,1) tintScanValue
			,ISNULL(tintPackingMode,1) tintPackingMode
			,@numDefaultSalesShippingDoc numDefaultSalesShippingDoc
			,ISNULL(tintPendingCloseFilter,1) tintPendingCloseFilter
			,ISNULL(bitGeneratePickListByOrder,0) bitGeneratePickListByOrder
			,ISNULL(bitPickWithoutInventoryCheck,0) bitPickWithoutInventoryCheck
			,ISNULL(bitEnablePickListMapping,0) bitEnablePickListMapping
			,ISNULL(bitEnableFulfillmentBizDocMapping,0) bitEnableFulfillmentBizDocMapping
		FROM	
			MassSalesFulfillmentConfiguration
		WHERE
			numDomainID=@numDomainID
	END
	ELSE 
	BEGIN
		SELECT 
			0 bitGroupByOrderForPick
			,0 bitGroupByOrderForShip
			,0 bitGroupByOrderForInvoice
			,0 bitGroupByOrderForPay
			,0 bitGroupByOrderForClose
			,2 tintInvoicingType
			,1 tintScanValue
			,1 tintPackingMode
			,@numDefaultSalesShippingDoc numDefaultSalesShippingDoc
			,1 tintPendingCloseFilter
			,0 bitGeneratePickListByOrder
			,0 bitPickWithoutInventoryCheck
			,0 bitEnablePickListMapping
			,0 bitEnableFulfillmentBizDocMapping
	END
END
GO