GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetEmployeesWithCapacityLoad')
DROP PROCEDURE dbo.USP_GetEmployeesWithCapacityLoad
GO
CREATE PROCEDURE [dbo].[USP_GetEmployeesWithCapacityLoad]
(
	@numDomainID NUMERIC(18,0)
	,@numWorkOrderID NUMERIC(18,0)
	,@tintDateRange TINYINT -- 1:Today, 2:Week, 3:Month
	,@dtFromDate DATETIME
	,@ClientTimeZoneOffset INT
)
AS 
BEGIN
	 SELECT 
		A.numContactID numEmployeeID
		,CONCAT(A.vcFirstName,' ',A.vcLastName) as vcEmployeeName
		,[dbo].[GetCapacityLoad](@numDomainID,A.numContactID,0,@numWorkOrderID,0,@tintDateRange,@dtFromDate,@ClientTimeZoneOffset) numCapacityLoad
	FROM 
		UserMaster UM         
	INNER JOIN 
		AdditionalContactsInformation A        
	ON 
		UM.numUserDetailId=A.numContactID          
	WHERE 
		UM.numDomainID=@numDomainID 
		AND UM.numDomainID=A.numDomainID 
		AND UM.intAssociate=1  
END