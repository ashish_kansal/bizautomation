
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ReportListMaster_RunPrebuildReport')
DROP PROCEDURE USP_ReportListMaster_RunPrebuildReport
GO
CREATE PROCEDURE [dbo].[USP_ReportListMaster_RunPrebuildReport]         
@numDomainID AS NUMERIC(18,0),                     
@numUserCntID AS NUMERIC(18,0),      
@ClientTimeZoneOffset AS INT,      
@intDefaultReportID INT,      
@numDashBoardID NUMERIC(18,0)      
AS        
BEGIN      
 DECLARE @vcTimeLine VARCHAR(50)      
  ,@vcGroupBy VARCHAR(50)      
  ,@vcTeritorry VARCHAR(MAX)      
  ,@vcFilterBy TINYINT      
  ,@vcFilterValue VARCHAR(MAX)      
  ,@dtFromDate DATE      
  ,@dtToDate DATE      
  ,@numRecordCount INT      
  ,@tintControlField INT      
  ,@vcDealAmount VARCHAR(MAX)      
  ,@tintOppType INT      
  ,@lngPConclAnalysis VARCHAR(MAX)      
  ,@vcTeams VARCHAR(MAX)      
  ,@vcEmploees VARCHAR(MAX)      
  ,@bitTask VARCHAR(MAX)      
  ,@tintTotalProgress INT      
  ,@tintMinNumber INT      
  ,@tintMaxNumber INT      
  ,@tintQtyToDisplay INT      
  ,@vcDueDate VARCHAR(MAX)


        
      
 IF @intDefaultReportID = 1 --1 - A/R      
 BEGIN      
  EXEC USP_ReportListMaster_ARPreBuildReport @numDomainID      
 END      
 ELSE IF @intDefaultReportID = 2 -- A/P      
 BEGIN      
  EXEC USP_ReportListMaster_APPreBuildReport @numDomainID      
 END      
 ELSE IF @intDefaultReportID = 3 -- MONEY IN THE BANK      
 BEGIN      
  EXEC USP_ReportListMaster_BankPreBuildReport @numDomainID      
 END      
 ELSE IF @intDefaultReportID = 5 -- TOP 10 ITEMS BY PROFIT AMOUNT (LAST 12 MONTHS)      
 BEGIN      
      
  SELECT      
   @vcTimeLine=ISNULL(vcTimeLine,'Last12Months')      
   ,@vcGroupBy=vcGroupBy      
   ,@vcTeritorry=vcTeritorry      
   ,@vcFilterBy=vcFilterBy      
   ,@vcFilterValue=vcFilterValue      
   ,@dtFromDate=dtFromDate      
   ,@dtToDate=dtToDate      
   ,@numRecordCount = ISNULL(numRecordCount,10)      
   ,@tintControlField = ISNULL(tintControlField,1)      
  FROM      
   ReportDashboard      
  WHERE      
   numDashBoardID = @numDashBoardID      
      
  EXEC USP_ReportListMaster_Top10ItemByProfitPreBuildReport @numDomainID,@vcTimeLine,@vcGroupBy,@vcTeritorry,@vcFilterBy,@vcFilterValue,@dtFromDate,@dtToDate,@numRecordCount,@tintControlField      
       
  --EXEC USP_ReportListMaster_Top10ItemByProfitPreBuildReport @numDomainID      
 END      
 ELSE IF @intDefaultReportID = 6 -- TOP 10 ITEMS BY REVENUE SOLD (LAST 12 MONTHS)      
 BEGIN      
  EXEC USP_ReportListMaster_Top10ItemByRevenuePreBuildReport @numDomainID      
 END      
 ELSE IF @intDefaultReportID = 7  -- PROFIT MARGIN BY ITEM CLASSIFICATION (LAST 12 MONTHS)      
 BEGIN      
  EXEC USP_ReportListMaster_ItemClassificationProfitPreBuildReport @numDomainID      
 END      
 ELSE IF @intDefaultReportID = 8  -- TOP 10 CUSTOMERS BY PROFIT MARGIN      
 BEGIN      
      
  SELECT      
   @vcTimeLine=ISNULL(vcTimeLine,'Last12Months')      
   ,@tintControlField=ISNULL(tintControlField,1)      
   ,@numRecordCount=ISNULL(numRecordCount,10)      
  FROM      
   ReportDashboard      
  WHERE      
   numDashBoardID = @numDashBoardID      
      
  EXEC USP_ReportListMaster_Top10CustomerByProfitPercentPreBuildReport @numDomainID,@ClientTimeZoneOffset,@vcTimeLine,@tintControlField,@numRecordCount      
       
  --EXEC USP_ReportListMaster_Top10CustomerByProfitPercentPreBuildReport @numDomainID      
 END      
 ELSE IF @intDefaultReportID = 9  -- TOP 10 CUSTOMERS BY PROFIT AMOUNT      
 BEGIN      
      
 SELECT      
   @vcTimeLine=ISNULL(vcTimeLine,'Last12Months')      
   ,@tintControlField=ISNULL(tintControlField,1)      
   ,@numRecordCount=ISNULL(numRecordCount,10)      
  FROM      
   ReportDashboard      
  WHERE      
   numDashBoardID = @numDashBoardID      
      
  EXEC USP_ReportListMaster_Top10CustomerByProfitAmountPreBuildReport @numDomainID,@ClientTimeZoneOffset,@vcTimeLine,@tintControlField,@numRecordCount      
      
  --EXEC USP_ReportListMaster_Top10CustomerByProfitAmountPreBuildReport @numDomainID      
 END      
 ELSE IF @intDefaultReportID = 10  -- ACTION ITEMS & MEETINGS DUE TODAY      
 BEGIN      
  SELECT      
   @vcTimeLine=vcTimeLine      
   ,@vcTeritorry=vcTeritorry      
   ,@vcFilterBy=vcFilterBy      
   ,@vcFilterValue=vcFilterValue      
   ,@bitTask=bitTask      
  FROM      
   ReportDashboard      
  WHERE      
   numDashBoardID = @numDashBoardID      
      
  EXEC USP_ReportListMaster_ActionItemPreBuildReport @numDomainID,0,@ClientTimeZoneOffset,@vcTimeLine,@vcTeritorry,@vcFilterBy,@vcFilterValue,@bitTask      
        
  --EXEC USP_ReportListMaster_ActionItemPreBuildReport @numDomainID,0,@ClientTimeZoneOffset      
 END      
 ELSE IF @intDefaultReportID = 11 -- YTD VS SAME PERIOD LAST YEAR      
 BEGIN      
 select 
 @vcFilterBy=vcFilterBy --added 
 from 
 ReportDashboard
 where 
 numDashBoardID= @numDashBoardID
  EXEC USP_ReportListMaster_RPEYTDAndSamePeriodLastYear @numDomainID,@vcFilterBy      
 END      
 ELSE IF @intDefaultReportID = 12  -- ACTION ITEMS & MEETINGS DUE TODAY      
 BEGIN      
      
  SELECT      
   @vcFilterValue=vcFilterValue      
         
  FROM      
   ReportDashboard      
  WHERE      
   numDashBoardID = @numDashBoardID      
        
  EXEC USP_ReportListMaster_RPELastMonthAndSamePeriodLastYear @numDomainID,@ClientTimeZoneOffset,@vcFilterValue      
  --EXEC USP_ReportListMaster_RPELastMonthAndSamePeriodLastYear @numDomainID      
 END      
 ELSE IF @intDefaultReportID = 13  --  SALES VS EXPENSES (LAST 12 MONTHS)      
 BEGIN      
  SELECT      
   @vcTimeLine=vcTimeLine      
   ,@vcGroupBy=vcGroupBy      
   ,@vcTeritorry=vcTeritorry      
   ,@vcFilterBy=vcFilterBy      
   ,@vcFilterValue=vcFilterValue      
   ,@dtFromDate=dtFromDate      
   ,@dtToDate=dtToDate      
  FROM      
   ReportDashboard      
  WHERE      
   numDashBoardID = @numDashBoardID      
      
  EXEC USP_ReportListMaster_SalesVsExpenseLast12Months @numDomainID,@ClientTimeZoneOffset,@vcTimeLine,@vcGroupBy,@vcTeritorry,@vcFilterBy,@vcFilterValue,@dtFromDate,@dtToDate      
 END      
 ELSE IF @intDefaultReportID = 14  --  TOP SOURCES OF SALES ORDERS (LAST 12 MONTHS)      
 BEGIN      
      
  SELECT      
   @vcDealAmount=vcDealAmount      
  FROM      
   ReportDashboard      
  WHERE      
   numDashBoardID = @numDashBoardID      
      
  EXEC USP_ReportListMaster_TopSourceOfSalesOrder @numDomainID,@ClientTimeZoneOffset,@vcDealAmount      
       
  --EXEC USP_ReportListMaster_TopSourceOfSalesOrder @numDomainID      
 END      
 ELSE IF @intDefaultReportID = 15  --  TOP 10 ITEMS BY PROFIT MARGIN      
 BEGIN      
  EXEC USP_ReportListMaster_Top10ItemByProfitMargin @numDomainID      
 END      
 ELSE IF @intDefaultReportID = 16  --  TOP 10 SALES OPPORTUNITIES BY REVENUE (LAST 12 MONTHS)      
 BEGIN      
      
  SELECT      
   @numRecordCount=ISNULL(numRecordCount,10)      
   ,@vcTimeLine=ISNULL(vcTimeLine,'Last12Months')      
   ,@vcDealAmount=vcDealAmount      
  FROM      
   ReportDashboard      
  WHERE      
   numDashBoardID = @numDashBoardID      
      
  EXEC USP_ReportListMaster_Top10SalesOpportunityByRevenue @numDomainID,@ClientTimeZoneOffset, @numRecordCount, @vcTimeLine, @vcDealAmount      
  --EXEC USP_ReportListMaster_Top10SalesOpportunityByRevenue @numDomainID      
 END      
 ELSE IF @intDefaultReportID = 17  --  TOP 10 SALES OPPORTUNITIES BY TOTAL PROGRESS      
 BEGIN      
  EXEC USP_ReportListMaster_Top10SalesOpportunityByTotalProgress @numDomainID      
 END      
 ELSE IF @intDefaultReportID = 18  --  TOP 10 ITEMS RETURNED VS QTY SOLD      
 BEGIN      
      
  SELECT      
    @numRecordCount=ISNULL(numRecordCount,10)      
  FROM      
   ReportDashboard      
  WHERE      
   numDashBoardID = @numDashBoardID      
      
  EXEC USP_ReportListMaster_Top10ItemsByReturnedVsSold @numDomainID, @numRecordCount      
 END      
 ELSE IF @intDefaultReportID = 19  --  LARGEST 10 SALES OPPORTUNITIES PAST THEIR DUE DATE      
 BEGIN      
      
  SELECT      
   @vcTimeLine = vcTimeLine,      
   @vcDealAmount = vcDealAmount,      
   @vcTeritorry = vcTeritorry,      
   @tintOppType=tintOppType,      
   @vcFilterBy = vcFilterBy,      
   @vcFilterValue = vcFilterValue,      
   @tintTotalProgress = tintTotalProgress,      
   @tintMinNumber = tintMinNumber,      
   @tintMaxNumber = tintMaxNumber,      
   @tintQtyToDisplay = tintQtyToDisplay,      
   @vcDueDate = vcDueDate      
  ,@dtFromDate = dtFromDate      
   ,@dtToDate = dtToDate      
  FROM      
   ReportDashboard      
  WHERE      
   numDashBoardID = @numDashBoardID      
      
  EXEC USP_ReportListMaster_Top10SalesOpportunityByPastDue @numDomainID,@ClientTimeZoneOffset,@vcTimeLine,@vcTeritorry,@tintOppType,@vcDealAmount,@vcFilterBy,@vcFilterValue,@tintTotalProgress,@tintMinNumber,@tintMaxNumber,@tintQtyToDisplay,@vcDueDate,  
  @dtFromDate,@dtToDate      
       
  --EXEC USP_ReportListMaster_Top10SalesOpportunityByPastDue @numDomainID,@ClientTimeZoneOffset      
 END      
 ELSE IF @intDefaultReportID = 20  --  Top 10 Campaigns by ROI      
 BEGIN      
  EXEC USP_ReportListMaster_Top10CampaignsByROI @numDomainID      
 END      
 ELSE IF @intDefaultReportID = 21  --  MARKET FRAGMENTATION � TOP 10 CUSTOMERS, AS A PORTION TOTAL SALES      
 BEGIN      
  EXEC USP_ReportListMaster_TopCustomersByPortionOfTotalSales @numDomainID      
 END      
 ELSE IF @intDefaultReportID = 22  --  SCORE CARD      
 BEGIN      
  EXEC USP_ReportListMaster_PrebuildScoreCard @numDomainID      
 END      
 ELSE IF @intDefaultReportID = 23  --  TOP 10 LEAD SOURCES (NEW LEADS)      
 BEGIN      
      
  SELECT      
   @vcTimeLine=ISNULL(vcTimeLine,'Last12Months')      
   ,@vcGroupBy=vcGroupBy      
   ,@vcTeritorry=vcTeritorry      
   ,@vcFilterBy=vcFilterBy      
   ,@vcFilterValue=vcFilterValue      
   ,@dtFromDate=dtFromDate      
   ,@dtToDate=dtToDate      
   ,@numRecordCount=ISNULL(numRecordCount,10)      
  FROM      
   ReportDashboard      
  WHERE      
   numDashBoardID = @numDashBoardID      
      
  EXEC USP_ReportListMaster_Top10LeadSource @numDomainID,@ClientTimeZoneOffset,@vcTimeLine,@vcGroupBy,@vcTeritorry,@vcFilterBy,@vcFilterValue,@dtFromDate,@dtToDate,@numRecordCount      
       
  --EXEC USP_ReportListMaster_Top10LeadSource @numDomainID      
 END      
 ELSE IF @intDefaultReportID = 24  --  LAST 10 EMAIL MESSAGES FROM PEOPLE I DO BUSINESS WITH      
 BEGIN      
  EXEC USP_ReportListMaster_Top10Email @numDomainID,@numUserCntID      
 END      
 ELSE IF @intDefaultReportID = 25  --  Reminders      
 BEGIN      
  EXEC USP_ReportListMaster_RemindersPreBuildReport @numDomainID,@ClientTimeZoneOffset      
 END      
 ELSE IF @intDefaultReportID = 26  --  TOP REASONS WHY WE�RE WINING DEALS      
 BEGIN      
        
  SELECT      
   @tintOppType=tintOppType      
   ,@vcDealAmount=vcDealAmount      
  FROM      
   ReportDashboard      
  WHERE      
   numDashBoardID = @numDashBoardID      
      
  EXEC USP_ReportListMaster_Top10ReasonsDealWins @numDomainID,@ClientTimeZoneOffset, @tintOppType, @vcDealAmount      
  --EXEC USP_ReportListMaster_Top10ReasonsDealWins @numDomainID      
 END      
 ELSE IF @intDefaultReportID = 27  --  TOP REASONS WHY WE�RE LOOSING DEALS      
 BEGIN      
      
  SELECT      
   @tintOppType=tintOppType      
   ,@vcDealAmount=vcDealAmount      
   ,@lngPConclAnalysis= lngPConclAnalysis      
  FROM      
   ReportDashboard      
  WHERE      
   numDashBoardID = @numDashBoardID      
      
  EXEC USP_ReportListMaster_Top10ReasonsDealLost @numDomainID,@ClientTimeZoneOffset, @tintOppType, @vcDealAmount, @lngPConclAnalysis      
  --EXEC USP_ReportListMaster_Top10ReasonsDealLost @numDomainID      
 END      
 ELSE IF @intDefaultReportID = 28  --  TOP 10 REASONS FOR SALES RETURNS      
 BEGIN      
      
  SELECT      
    @numRecordCount=ISNULL(numRecordCount,10)      
  FROM      
   ReportDashboard      
  WHERE      
   numDashBoardID = @numDashBoardID      
  EXEC USP_ReportListMaster_Top10ReasonsForSalesReturns @numDomainID, @numDashBoardID      
 END      
 ELSE IF @intDefaultReportID = 29  --  EMPLOYEE SALES PERFORMANCE PANEL 1      
 BEGIN      
      
  SELECT      
   @vcTimeLine=vcTimeLine      
   ,@vcFilterBy=vcFilterBy      
   ,@vcFilterValue=vcFilterValue
     
  FROM      
   ReportDashboard      
  WHERE      
   numDashBoardID = @numDashBoardID      
      
  EXEC USP_ReportListMaster_EmployeeSalesPerformancePanel1 @numDomainID,@ClientTimeZoneOffset, @vcTimeLine, @vcFilterBy,@vcFilterValue    
  --EXEC USP_ReportListMaster_EmployeeSalesPerformancePanel1 @numDomainID   
    
 END      
 ELSE IF @intDefaultReportID = 30  --  PARTNER REVENUES, MARGINS & PROFITS (LAST 12 MONTHS)      
 BEGIN      
  EXEC USP_ReportListMaster_PartnerRMPLast12Months @numDomainID      
 END      
 ELSE IF @intDefaultReportID = 31  --  EMPLOYEE SALES PERFORMANCE PT 1 (LAST 12 MONTHS)      
 BEGIN      
        
  SELECT      
   @vcTimeLine=vcTimeLine      
   ,@vcFilterBy=vcFilterBy      
   ,@vcFilterValue=vcFilterValue      
  FROM      
   ReportDashboard      
  WHERE      
   numDashBoardID = @numDashBoardID      
      
  EXEC USP_ReportListMaster_EmployeeSalesPerformance1 @numDomainID,@ClientTimeZoneOffset, @vcTimeLine, @vcFilterBy,@vcFilterValue      
  --EXEC USP_ReportListMaster_EmployeeSalesPerformance1 @numDomainID      
 END      
 ELSE IF @intDefaultReportID = 32  --  EMPLOYEE BENEFIT TO COMPANY (LAST 12 MONTHS)      
 BEGIN      
      
  SELECT      
   @vcTimeLine=vcTimeLine      
   ,@vcFilterBy=vcFilterBy      
   ,@vcFilterValue=vcFilterValue      
  FROM      
   ReportDashboard      
  WHERE      
   numDashBoardID = @numDashBoardID      
      
  EXEC USP_ReportListMaster_EmployeeBenefitToCompany @numDomainID,@ClientTimeZoneOffset, @vcTimeLine, @vcFilterBy,@vcFilterValue      
 END      
 ELSE IF @intDefaultReportID = 33  --  FIRST 10 SAVED SEARCHES      
 BEGIN      
  EXEC USP_ReportListMaster_First10SavedSearch @numDomainID      
 END      
 ELSE IF @intDefaultReportID = 34  --  SALES OPPORTUNITY WON/LOST REPORT      
 BEGIN      
  SELECT      
   @vcTimeLine=vcTimeLine      
   ,@vcGroupBy=vcGroupBy      
   ,@vcTeritorry=vcTeritorry      
   ,@vcFilterBy=vcFilterBy      
   ,@vcFilterValue=vcFilterValue      
   ,@dtFromDate=dtFromDate      
   ,@dtToDate=dtToDate      
  FROM      
   ReportDashboard      
  WHERE      
   numDashBoardID = @numDashBoardID      
      
  EXEC USP_ReportListMaster_SalesOppWonLostPreBuildReport @numDomainID,@ClientTimeZoneOffset,@vcTimeLine,@vcGroupBy,@vcTeritorry,@vcFilterBy,@vcFilterValue,@dtFromDate,@dtToDate      
 END      
 ELSE IF @intDefaultReportID = 35  --  SALES OPPORTUNITY PIPELINE      
 BEGIN      
  SELECT      
   @vcTimeLine=vcTimeLine      
   ,@vcGroupBy=vcGroupBy      
   ,@vcTeritorry=vcTeritorry      
   ,@vcFilterBy=vcFilterBy      
   ,@vcFilterValue=vcFilterValue      
   ,@dtFromDate=dtFromDate      
   ,@dtToDate=dtToDate      
  FROM      
   ReportDashboard      
  WHERE      
   numDashBoardID = @numDashBoardID      
      
  EXEC USP_ReportListMaster_SalesOppPipeLine @numDomainID,@ClientTimeZoneOffset,@vcTimeLine,@vcGroupBy,@vcTeritorry,@vcFilterBy,@vcFilterValue,@dtFromDate,@dtToDate      
 END     
  ELSE IF @intDefaultReportID = 36 -- BackOrder Valuation    
  BEGIN    
   SELECT      
   @vcFilterBy=vcFilterBy      
   ,@vcFilterValue=vcFilterValue
     
  FROM      
   ReportDashboard      
  WHERE      
   numDashBoardID = @numDashBoardID   
   EXEC USP_ReportListMaster_TotalBackOrderValue @numDomainID, @vcFilterBy,@vcFilterValue   
  END    
    
  ELSE IF @intDefaultReportID = 37 -- Inventory Value by the WareHouse    
  BEGIN    
   EXEC USP_ReportListMaster_WarehouseInventoryValue @numDomainID    
  END    
      
   ELSE IF @intDefaultReportID = 38 -- Month To Date Revenue    
  BEGIN    
   EXEC  USP_ReportListMaster_MonthToDateRevenue @numDomainID    
  END 
  
   ELSE IF @intDefaultReportID = 39 -- Pending Report Details   
  BEGIN    
   EXEC  USP_ReportListMaster_PendingOrdersStatus @numDomainID   
  END     
END 
GO
