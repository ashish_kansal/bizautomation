GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_UpdateOppItemLabel')
DROP PROCEDURE USP_UpdateOppItemLabel
GO
CREATE PROCEDURE  USP_UpdateOppItemLabel
    @numOppID NUMERIC,
	@numoppitemtCode numeric(9),
    @vcItemName varchar(300),
    @vcModelID varchar(200),
    @vcItemDesc varchar(2000),
	@vcManufacturer varchar(250),
	@numProjectID numeric(9),
	@numClassID numeric(9),
	@vcNotes varchar(1000),
	@numCost decimal(30, 16)
AS 
    BEGIN

 UPDATE  OpportunityItems
                SET     [vcItemName] = @vcItemName,
                        [vcModelID] = @vcModelID,
                        [vcItemDesc] = @vcItemDesc,
                        vcManufacturer = @vcManufacturer,
                        numProjectID = @numProjectID,
                        numClassID = @numClassID,
						vcNotes=@vcNotes,
						numCost=@numCost,
						numProjectStageID=(Case when numProjectID<>@numProjectID then 0 else numProjectStageID end)
                WHERE   [numOppId] = @numOppID and numoppitemtCode = @numoppitemtCode
    END