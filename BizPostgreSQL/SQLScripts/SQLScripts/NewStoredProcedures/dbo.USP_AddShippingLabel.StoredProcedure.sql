GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_AddShippingLabel')
DROP PROCEDURE USP_AddShippingLabel
GO
/****** Object:  StoredProcedure [dbo].[USP_AddShippingLabel]    Script Date: 05/07/2009 17:39:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[USP_AddShippingLabel]
                @ShippingReportItemId NUMERIC(18,0),
                @vcShippingLabelImage VARCHAR(100),
                @vcTrackingNumber     VARCHAR(50),
                @numUserCntId         NUMERIC(18,0)
AS
  BEGIN
    IF (SELECT COUNT(*) FROM   [ShippingLabel] WHERE  ShippingReportItemId = @ShippingReportItemId) > 0
      BEGIN
        UPDATE [ShippingLabel]
        SET    [vcShippingLabelImage] = @vcShippingLabelImage,
               [vcTrackingNumber] = @vcTrackingNumber
        WHERE  [ShippingReportItemId] = @ShippingReportItemId
      END
    ELSE
      BEGIN
        INSERT INTO [ShippingLabel]
                   ([ShippingReportItemId],
                    [vcShippingLabelImage],
                    [vcTrackingNumber],
                    [dtCreateDate],
                    [numCreatedBy])
        VALUES     (@ShippingReportItemId,
                    @vcShippingLabelImage,
                    @vcTrackingNumber,
                    GETUTCDATE(),
                    @numUserCntId)
      END
  END
