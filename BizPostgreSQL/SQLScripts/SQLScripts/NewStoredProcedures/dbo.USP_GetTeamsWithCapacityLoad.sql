GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetTeamsWithCapacityLoad')
DROP PROCEDURE dbo.USP_GetTeamsWithCapacityLoad
GO
CREATE PROCEDURE [dbo].[USP_GetTeamsWithCapacityLoad]
(
	@numDomainID NUMERIC(18,0)
	,@numWorkOrderID NUMERIC(18,0)
	,@tintDateRange TINYINT -- 1:Today, 2:Week, 3:Month
	,@dtFromDate DATETIME
	,@ClientTimeZoneOffset INT
)
AS 
BEGIN
	 SELECT 
		LD.numListItemID numTeamID
		,ISNULL(vcData,'') as vcTeamName
		,[dbo].[GetCapacityLoad](@numDomainID,0,LD.numListItemID,@numWorkOrderID,0,@tintDateRange,@dtFromDate,@ClientTimeZoneOffset) numCapacityLoad
	FROM 
		ListDetails LD                 
	WHERE 
		LD.numDomainID=@numDomainID 
		AND LD.numListID=35
END