GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_UserMaster_UpdateEmailAlias')
DROP PROCEDURE dbo.USP_UserMaster_UpdateEmailAlias
GO
CREATE PROCEDURE [dbo].[USP_UserMaster_UpdateEmailAlias]
(
	@numDomainID NUMERIC(18,0),
	@numUserID NUMERIC(18,0),
    @vcEmailAlias VARCHAR(100),
	@vcSMTPPassword VARCHAR(100)
)
AS
BEGIN
	UPDATE
		UserMaster
	SET
		vcEmailAlias=@vcEmailAlias
		,vcEmailAliasPassword=@vcSMTPPassword
	WHERE
		numDomainID=@numDomainID
		AND numUserId=@numUserID
END
GO


