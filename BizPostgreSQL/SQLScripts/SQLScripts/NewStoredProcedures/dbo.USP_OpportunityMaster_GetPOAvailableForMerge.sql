SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_OpportunityMaster_GetPOAvailableForMerge')
DROP PROCEDURE USP_OpportunityMaster_GetPOAvailableForMerge
GO
CREATE PROCEDURE [dbo].[USP_OpportunityMaster_GetPOAvailableForMerge]  
	@numDomainID NUMERIC(18,0),
	@numLevel TINYINT,
	@numVendorID NUMERIC(18,0),
	@numPOID NUMERIC(18,0)
AS  
BEGIN  
	-- GET VENDORS FOR WHOM MORE THAN 1 POs ARE AVAILABLE IN WHICH AUTORATIVE BIZDOC IS NOT ADDED AND NO ITEMS ARE RECEIVED YET
	IF @numLevel = 0
	BEGIN
		SELECT 
			OpportunityMaster.numDivisionId AS VendorID,
			CompanyInfo.vcCompanyName AS Vendor
		FROM 
			OpportunityMaster
		INNER JOIN
			DivisionMaster
		ON
			OpportunityMaster.numDivisionId = DivisionMaster.numDivisionID
		INNER JOIN
			CompanyInfo
		ON
			DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
		WHERE 
			tintOppType=2 
			AND tintOppStatus=1 
			AND ISNULL(bitStockTransfer,0)=0 
			AND ISNULL(tintshipped,0)=0 
			AND OpportunityMaster.numDomainId=@numDomainID
			AND ISNULL(bitStopMerge,0) = 0
			AND (SELECT COUNT(*) FROM OpportunityItems WHERE numOppId=OpportunityMaster.numOppId) > 0
			AND (SELECT COUNT(*) FROM OpportunityItems WHERE numOppId=OpportunityMaster.numOppId AND ISNULL(numUnitHourReceived,0) > 0) = 0
			AND (SELECT COUNT(*) FROM OpportunityBizDocs WHERE numOppId=OpportunityMaster.numOppId AND ISNULL(bitAuthoritativeBizDocs,0) = 1) = 0
		GROUP BY
			OpportunityMaster.numDivisionId,
			CompanyInfo.vcCompanyName
		HAVING
			COUNT(OpportunityMaster.numOppId) > 1
	END
	--GET PO OF VENDOR
	ELSE IF @numLevel = 1 AND @numVendorID > 0
	BEGIN
		SELECT 
			@numVendorID AS VendorID,
			OpportunityMaster.numOppID AS POID,
			OpportunityMaster.vcPOppName AS POName,
			ISNULL(ListDetails.vcData,'') AS POStatus,
			dbo.FormatedDateTimeFromDate(OpportunityMaster.bintCreatedDate,@numDomainID) AS CreatedDate,
			dbo.FormatedDateTimeFromDate(OpportunityMaster.intPEstimatedCloseDate,@numDomainID) AS EstimatedCloseDate
		FROM 
			OpportunityMaster
		LEFT JOIN
			ListDetails
		ON
			numListID = 176 AND
			numListItemID = OpportunityMaster.numStatus
		WHERE 
			tintOppType=2 
			AND tintOppStatus=1 
			AND ISNULL(bitStockTransfer,0)=0 
			AND ISNULL(tintshipped,0)=0 
			AND OpportunityMaster.numDomainId=@numDomainID
			AND OpportunityMaster.numDivisionId = @numVendorID
			AND ISNULL(bitStopMerge,0) = 0
			AND (SELECT COUNT(*) FROM OpportunityItems WHERE numOppId=OpportunityMaster.numOppId) > 0
			AND (SELECT COUNT(*) FROM OpportunityItems WHERE numOppId=OpportunityMaster.numOppId AND ISNULL(numUnitHourReceived,0) > 0) = 0
			AND (SELECT COUNT(*) FROM OpportunityBizDocs WHERE numOppId=OpportunityMaster.numOppId AND ISNULL(bitAuthoritativeBizDocs,0) = 1) = 0
	END
	--GET ITEMS OF PO
	ELSE IF @numLevel = 2 AND @numPOID > 0
	BEGIN
		SELECT 
			@numPOID AS POID,
			OpportunityItems.numItemCode,
			ISNULL(OpportunityItems.vcItemName,'') AS vcItemName,
			ISNULL(OpportunityItems.vcModelID,'') AS vcModelID,
			ISNULL(OpportunityItems.numUnitHour,0) AS numUnitHour,
			ISNULL(Vendor.intMinQty,0) AS intMinQty
		FROM 
			OpportunityMaster
		INNER JOIN
			OpportunityItems
		ON
			OpportunityMaster.numOppId = OpportunityItems.numOppId
		LEFT JOIN
			Vendor
		ON
			OpportunityMaster.numDivisionId = Vendor.numVendorID AND
			OpportunityItems.numItemCode = Vendor.numItemCode
		WHERE 
			OpportunityMaster.numDomainId=@numDomainID
			AND OpportunityMaster.numOppId = @numPOID
	END

	
END
GO
