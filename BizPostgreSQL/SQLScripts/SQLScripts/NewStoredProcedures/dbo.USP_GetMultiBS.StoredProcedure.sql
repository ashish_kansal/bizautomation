--EXEC USP_GetMultiBS @numParentDomainID=0,@dtFromDate='01/Jan/2008',@dtToDate='31/Dec/2008',@numSubscriberId=103,@RollUp=1
-- Created by Sojan        
-- [USP_GetChartAcntDetails] @numParentDomainID=72,@dtFromDate='2007-01-01 00:00:00:000',@dtToDate='2009-09-25 00:00:00:000'                            
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetMultiBS')
DROP PROCEDURE USP_GetMultiBS
GO
CREATE PROCEDURE [dbo].[USP_GetMultiBS]                                   
@numParentDomainID as numeric(9),                        
@dtFromDate as datetime,                      
@dtToDate as datetime  ,
@numSubscriberId int ,
@RollUp int=0                                
As                                      
Begin    
DECLARE @numFinYear INT;
DECLARE @dtFinYearFrom datetime;

--set @numFinYear= (SELECT numFinYearId FROM FINANCIALYEAR WHERE dtPeriodFrom <= @dtFromDate AND  
--dtPeriodTo >=@dtFromDate AND numDomainId= @numParentDomainID);
--
--set @dtFinYearFrom = (SELECT dtPeriodFrom FROM FINANCIALYEAR WHERE dtPeriodFrom <= @dtFromDate AND  
--dtPeriodTo >=@dtFromDate AND numDomainId= @numParentDomainID);

CREATE TABLE #PLSummary (numDomainId numeric(9),vcDomainCode varchar(50),vcDomainName varchar(150), numAccountId numeric(9),vcAccountName varchar(250),
numParntAcntTypeID numeric(9),vcAccountDescription varchar(250),
vcAccountCode varchar(50),Opening DECIMAL(20,5),Debit DECIMAL(20,5),Credit DECIMAL(20,5));


CREATE TABLE #TLGroup (numDomainId numeric(9),vcDomainCode varchar(50),vcDomainName varchar(150), numAccountId numeric(9),vcAccountName varchar(250),
numParntAcntTypeID numeric(9),vcAccountDescription varchar(250),
vcAccountCode varchar(50),Opening DECIMAL(20,5),Debit DECIMAL(20,5),Credit DECIMAL(20,5));


INSERT INTO  #PLSummary


SELECT COA.numDomainId,dn.vcDomainCode,dn.vcDomainName,COA.numAccountId,vcAccountName,numParntAcntTypeID,vcAccountDescription,vcAccountCode,
isnull((SELECT sum(isnull(monOpening,0)) from CHARTACCOUNTOPENING CAO WHERE
numFinYearId in (SELECT numFinYearId FROM FINANCIALYEAR WHERE dtPeriodFrom <= @dtFromDate AND  
dtPeriodTo >=@dtFromDate AND numDomainId= COA.numDomainId) and 
numDomainID=@numParentDomainID and
CAO.numAccountId=COA.numAccountId),0) +

ISNULL((SELECT sum(Debit-Credit) FROM view_journal VJ,FINANCIALYEAR FN
WHERE VJ.numDomainId=COA.numDomainId AND
	FN.numDomainID=VJ.numDomainID AND
	VJ.numAccountId=COA.numAccountId AND
	FN.dtPeriodFrom <= @dtFromDate AND  
	FN.dtPeriodTo >=@dtFromDate and 
	datEntry_Date BETWEEN FN.dtPeriodFrom  AND  @dtFromDate-1),0) AS OPENING,

ISNULL((SELECT sum(Debit) FROM view_journal VJ
WHERE VJ.numDomainId=COA.numDomainId AND
	VJ.numAccountId=COA.numAccountId AND
	datEntry_Date BETWEEN  @dtFromDate AND @dtToDate ),0) as DEBIT,

ISNULL((SELECT sum(Credit) FROM view_journal VJ
WHERE VJ.numDomainId=COA.numDomainId AND
	VJ.numAccountId=COA.numAccountId AND
	datEntry_Date BETWEEN  @dtFromDate AND @dtToDate ),0) as CREDIT
FROM Chart_of_Accounts COA,
	Domain DN,
	(select * from Domain DNN where dnn.numSubscriberID=@numSubscriberID) DNN
WHERE COA.numDomainId=DNN.numDomainId AND 
	DNN.numDomainId =dn.numDomainId AND 
	DN.numDomainId =@numParentDomainID AND 
	DN.numSubscriberID = @numSubscriberID
	
union


SELECT COA.numDomainId,dn.vcDomainCode,dn.vcDomainName,COA.numAccountId,vcAccountName,numParntAcntTypeID,vcAccountDescription,vcAccountCode,
isnull((SELECT sum(isnull(monOpening,0)) from CHARTACCOUNTOPENING CAO WHERE
numFinYearId in (SELECT numFinYearId FROM FINANCIALYEAR WHERE dtPeriodFrom <= @dtFromDate AND  
dtPeriodTo >=@dtFromDate AND numDomainId= COA.numDomainId) and 
numDomainID=@numParentDomainID and
CAO.numAccountId=COA.numAccountId),0) +

ISNULL((SELECT sum(Debit-Credit) FROM view_journal VJ,FINANCIALYEAR FN
WHERE VJ.numDomainId=COA.numDomainId AND
	FN.numDomainID=VJ.numDomainID AND
	VJ.numAccountId=COA.numAccountId AND
	FN.dtPeriodFrom <= @dtFromDate AND  
	FN.dtPeriodTo >=@dtFromDate and 
	datEntry_Date BETWEEN FN.dtPeriodFrom  AND  @dtFromDate-1),0) AS OPENING,

ISNULL((SELECT sum(Debit) FROM view_journal VJ
WHERE VJ.numDomainId=COA.numDomainId AND
	VJ.numAccountId=COA.numAccountId AND
	datEntry_Date BETWEEN  @dtFromDate AND @dtToDate ),0) as DEBIT,

ISNULL((SELECT sum(Credit) FROM view_journal VJ
WHERE VJ.numDomainId=COA.numDomainId AND
	VJ.numAccountId=COA.numAccountId AND
	datEntry_Date BETWEEN  @dtFromDate AND @dtToDate ),0) as CREDIT
FROM Chart_of_Accounts COA,
	Domain DN,
	(select * from Domain DNN where dnn.numSubscriberID=@numSubscriberID) DNN
WHERE COA.numDomainId=DNN.numDomainId AND 
	DNN.vcDomainCode like DN.vcDomainCode + '%'  AND 
	DN.numParentDomainId =@numParentDomainID  AND 
	DN.numSubscriberID=@numSubscriberID


---------------------------------------------------------------------------------
---------------------------------------------------------------------------------
---------------------------------------------------------------------------------


INSERT INTO  #TLGroup


SELECT AD.numDomainId,dn.vcDomainCode,dn.vcDomainName,AD.numAccountTypeId,AD.vcAccountType,numParentID,'',AD.vcAccountCode,
isnull((SELECT SUM(isnull(monOpening,0)) from CHARTACCOUNTOPENING CAO,CHART_OF_ACCOUNTS COA WHERE
numFinYearId in (SELECT numFinYearId FROM FINANCIALYEAR WHERE dtPeriodFrom <= @dtFromDate AND  
dtPeriodTo >=@dtFromDate AND FINANCIALYEAR.numDomainId= AD.numDomainId)
 AND CAO.numDomainID=AD.numDomainID AND 
CAO.numAccountId=COA.numAccountId  AND 
COA.vcAccountCode like AD.vcAccountCode + '%' ),0) +

ISNULL((SELECT sum(Debit-Credit) FROM view_journal VJ,FINANCIALYEAR FN
WHERE VJ.numDomainId=AD.numDomainId AND
	FN.numDomainID=VJ.numDomainID AND
	VJ.coavcAccountCode like AD.vcAccountCode  + '%' AND
	FN.dtPeriodFrom <= @dtFromDate AND  
	FN.dtPeriodTo >=@dtFromDate and 
	datEntry_Date BETWEEN FN.dtPeriodFrom  AND  @dtFromDate-1),0) AS OPENING,

ISNULL((SELECT sum(Debit) FROM view_journal VJ
WHERE VJ.numDomainId=AD.numDomainId AND
	VJ.coavcAccountCode like AD.vcAccountCode  + '%' AND
	datEntry_Date BETWEEN  @dtFromDate AND @dtToDate ),0) as DEBIT,

ISNULL((SELECT sum(Credit) FROM view_journal VJ
WHERE VJ.numDomainId=AD.numDomainId AND
	VJ.coavcAccountCode like AD.vcAccountCode  + '%' AND
	datEntry_Date BETWEEN  @dtFromDate AND @dtToDate ),0) as CREDIT
FROM AccountTypeDetail AD,
	Domain DN,
	Domain DNN
WHERE AD.numDomainId=DNN.numDomainId AND 
	DNN.numDomainId =dn.numDomainId AND 
	DN.numDomainId =@numParentDomainID AND 
	DN.numSubscriberID= @numSubscriberID
	

UNION


SELECT AD.numDomainId,dn.vcDomainCode,dn.vcDomainName,AD.numAccountTypeId,AD.vcAccountType,numParentID,'',AD.vcAccountCode,
isnull((SELECT sum(isnull(monOpening,0)) from CHARTACCOUNTOPENING CAO,CHART_OF_ACCOUNTS COA WHERE
numFinYearId in (SELECT numFinYearId FROM FINANCIALYEAR WHERE dtPeriodFrom <= @dtFromDate AND  
dtPeriodTo >=@dtFromDate AND FINANCIALYEAR.numDomainId= AD.numDomainId)
 AND CAO.numDomainID=AD.numDomainID AND 
CAO.numAccountId=COA.numAccountId  AND 
COA.vcAccountCode like AD.vcAccountCode + '%' ),0) +

ISNULL((SELECT sum(Debit-Credit) FROM view_journal VJ,FINANCIALYEAR FN
WHERE VJ.numDomainId=AD.numDomainId AND
	FN.numDomainID=VJ.numDomainID AND
	VJ.coavcAccountCode like AD.vcAccountCode + '%' AND
	FN.dtPeriodFrom <= @dtFromDate AND  
	FN.dtPeriodTo >=@dtFromDate and 
	datEntry_Date BETWEEN FN.dtPeriodFrom  AND  @dtFromDate-1),0) AS OPENING,

ISNULL((SELECT sum(Debit) FROM view_journal VJ
WHERE VJ.numDomainId=AD.numDomainId AND
	VJ.coavcAccountCode like AD.vcAccountCode + '%' AND
	datEntry_Date BETWEEN  @dtFromDate AND @dtToDate ),0) as DEBIT,

ISNULL((SELECT sum(Credit) FROM view_journal VJ
WHERE VJ.numDomainId=AD.numDomainId AND
	VJ.coavcAccountCode like AD.vcAccountCode + '%' AND
	datEntry_Date BETWEEN  @dtFromDate AND @dtToDate ),0) as CREDIT
FROM AccountTypeDetail AD,
	Domain DN,
	( SELECT * FROM Domain DNN WHERE DNN.numSubscriberID=@numSubscriberID) DNN
WHERE AD.numDomainId=DNN.numDomainId AND 
	DNN.vcDomainCode like DN.vcDomainCode + '%'  AND 
	DN.numParentDomainId =@numParentDomainID  AND 
	DN.numSubscriberID= @numSubscriberID

--select * from #PLSummary;
--
--select * from #TLGroup



if @RollUp=0 
	begin
		select vcDomainCode, vcDomainName from #TLGroup
		where ([vcAccountCode] like '0101%'  or [vcAccountCode] like '0102%' or [vcAccountCode] like '0105%')
		group by vcDomainCode, vcDomainName
	end
else if @RollUp=1 
	begin
		select '00' AS vcDomainCode, 'Roll-Up' AS vcDomainName	
	end

	select
	CASE 
	 WHEN LEN([vcAccountCode])>4 THEN REPLICATE('&nbsp;', LEN([vcAccountCode])-4) + [vcAccountCode] 
	 ELSE '<b>' + [vcAccountCode] +  '</b>'
	 END AS vcAccountCode,
	 CASE 
	 WHEN LEN([vcAccountCode])>4 THEN REPLICATE('&nbsp;', LEN([vcAccountCode])-4) + [vcAccountName]
	 ELSE '<b>' + [vcAccountName] + '</b>'
	 END  as vcAccountName,
	vcAccountName as vcAccountNamePK,
	vcAccountCode as vcAccountCodePK
	from #TLGroup
	where len(vcAccountCode)>2 AND 
	([vcAccountCode] like '0101%'  or [vcAccountCode] like '0102%'  or [vcAccountCode] like '0105%')
	group by vcAccountCode,vcAccountName;


if @Rollup=0
	begin
		SELECT vcDomainCode,vcDomainName,vcAccountName,vcAccountCode,sum(Opening) + sum(Debit) - Sum(Credit) as Balance
		FROM #TLGroup where len(vcAccountCode)>2 AND 
		([vcAccountCode] like '0101%'  or [vcAccountCode] like '0102%'  or [vcAccountCode] like '0105%')
		GROUP BY vcDomainCode,vcDomainName,vcAccountName,vcAccountCode;
	end
else if @Rollup=1
	begin
		SELECT '00' AS vcDomainCode,'Roll-Up' AS vcDomainName,vcAccountName,vcAccountCode,sum(Opening) + sum(Debit) - Sum(Credit) as Balance
		FROM #TLGroup where len(vcAccountCode)>2 AND 
		([vcAccountCode] like '0101%'  or [vcAccountCode] like '0102%'  or [vcAccountCode] like '0105%')
		GROUP BY vcAccountName,vcAccountCode;
	end
if @Rollup=0
	begin
		SELECT vcDomainCode,vcDomainName, sum(Opening) + sum(Debit) - Sum(Credit) as Balance
		FROM #TLGroup where len(vcAccountCode)>2 AND 
		[vcAccountCode] IN ('0103','0104')
		GROUP BY vcDomainCode,vcDomainName;
	end
else if @Rollup=1
	begin
		SELECT '00' AS vcDomainCode,'Roll-Up' AS vcDomainName, sum(Opening) + sum(Debit) - Sum(Credit) as Balance
		FROM #TLGroup where len(vcAccountCode)>2 AND 
		[vcAccountCode] IN ('0103','0104');
	end
drop table #PLSummary;
drop table #TLGroup;

End


