GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ScheduledReportsGroup_GetForEmail')
DROP PROCEDURE dbo.USP_ScheduledReportsGroup_GetForEmail
GO
CREATE PROCEDURE [dbo].[USP_ScheduledReportsGroup_GetForEmail]
(
	@numDomainID NUMERIC(18,0)
	,@numSRGID NUMERIC(18,0)
)
AS 
BEGIN
	SELECT
		ScheduledReportsGroup.numDomainID
		,ScheduledReportsGroup.numCreatedBy
		,ScheduledReportsGroup.ID
		,ScheduledReportsGroup.numEmailTemplate
		,ISNULL(ScheduledReportsGroup.[vcSelectedTokens],'') vcSelectedTokens
		,ISNULL(ScheduledReportsGroup.vcRecipientsEmail,'') vcRecipientsEmail
		,ISNULL(ScheduledReportsGroup.vcReceipientsContactID,'') vcReceipientsContactID
		,ISNULL(ScheduledReportsGroup.tintDataCacheStatus,0) tintDataCacheStatus
		,ISNULL(Domain.vcPSMTPDisplayName,'') AS vcFromName
		,ISNULL(Domain.vcPSMTPUserName,'') AS vcFromEmail
		,ISNULL(Domain.bitPSMTPServer,0) AS bitPSMTPServer
		,ISNULL(Domain.vcLogoForBizTheme,'') vcLogoForBizTheme
		,ISNULL(Domain.vcThemeClass,'') vcThemeClass
	FROM
		ScheduledReportsGroup
	INNER JOIN
		Domain
	ON
		ScheduledReportsGroup.numDomainID = Domain.numDomainId
	WHERE
		1 = (CASE 
				WHEN ISNULL(@numDomainID,0) > 0 AND ISNULL(@numSRGID,0) > 0 
				THEN (CASE WHEN ScheduledReportsGroup.numDomainID=@numDomainID AND ScheduledReportsGroup.ID=@numSRGID THEN 1 ELSE 0 END)
				ELSE (CASE WHEN (dtNextDate <= GETUTCDATE() OR (dtNextDate IS NULL AND dtStartDate <= GETUTCDATE())) AND ISNULL(intNotOfTimeTried,0) < 5 THEN 1 ELSE 0 END)
			END)
END
GO