GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_Item_GetAssemblyMfgTimeCost')
DROP PROCEDURE USP_Item_GetAssemblyMfgTimeCost
GO
CREATE PROCEDURE [dbo].[USP_Item_GetAssemblyMfgTimeCost]
	@numDomainID NUMERIC(18,0)
	,@numItemCode NUMERIC(18,0)
	,@numWarehouseID NUMERIC(18,0)
	,@numQtyToBuild FLOAT
	,@numBuildProcessID NUMERIC(18,0)
	,@dtProjectedStartDate DATETIME
	,@ClientTimeZoneOffset INT
AS
BEGIN 
	DECLARE @dtProjectedFinishDate AS DATETIME = @dtProjectedStartDate
	DECLARE @monLaborCost DECIMAL(20,5) = 0
	DECLARE @numOverheadServiceItemID NUMERIC(18,0)

	SELECT 
		@numOverheadServiceItemID=ISNULL(numOverheadServiceItemID,0) 
	FROM 
		Domain 
	WHERE 
		numDomainID=@numDomainID

	IF ISNULL(@numBuildProcessID,0) = 0
	BEGIN
		SELECT @numBuildProcessID=ISNULL(numBusinessProcessId,0) FROM Item WHERE numDomainID=@numDomainID AND numItemCode = @numItemCode
	END

	IF ISNULL(@numQtyToBuild,0) = 0
	BEGIN
		SET @numQtyToBuild = 1
	END

	DECLARE @TempTaskAssignee TABLE
	(
		numAssignedTo NUMERIC(18,0)
		,dtLastTaskCompletionTime DATETIME
	)
	
	DECLARE @TempTasks TABLE
	(
		ID INT IDENTITY(1,1)
		,numTaskID NUMERIC(18,0)
		,numTaskTimeInMinutes NUMERIC(18,0)
		,numTaskAssignee NUMERIC(18,0)
		,monHourlyRate DECIMAL(20,5)
		,intTaskType INT --1:Parallel, 2:Sequential
		,dtPlannedStartDate DATETIME
		,dtFinishDate DATETIME
	)

	INSERT INTO @TempTasks
	(
		numTaskID
		,numTaskTimeInMinutes
		,intTaskType
		,numTaskAssignee
		,monHourlyRate
	)
	SELECT 
		SPDT.numTaskID
		,((ISNULL(SPDT.numHours,0) * 60) + ISNULL(SPDT.numMinutes,0))
		,ISNULL(SPDT.intTaskType,1)
		,SPDT.numAssignTo
		,ISNULL(UM.monHourlyRate,0)
	FROM 
		StagePercentageDetails SPD
	INNER JOIN
		StagePercentageDetailsTask SPDT
	ON
		SPD.numStageDetailsId=SPDT.numStageDetailsId
	INNER JOIN
		UserMaster UM
	ON
		SPDT.numAssignTo = UM.numUserDetailId
	WHERE 
		SPD.numDomainId=@numDomainID
		AND SPD.slp_id=@numBuildProcessID
		AND ISNULL(SPDT.numWorkOrderId,0) = 0

	
	SET @dtProjectedFinishDate= ISNULL(dbo.GetProjectedFinish(@numDomainID,1,@numItemCode,@numQtyToBuild,@numBuildProcessID,@dtProjectedStartDate,@ClientTimeZoneOffset,@numWarehouseID),@dtProjectedStartDate)

	SET @monLaborCost = ISNULL((SELECT SUM(numTaskTimeInMinutes * (monHourlyRate/60)) FROM @TempTasks),0)
	
	SELECT 
		DATEADD(MINUTE,-1 * @ClientTimeZoneOffset,@dtProjectedFinishDate) AS dtProjectedFinishDate
		,ISNULL((SELECT SUM(ISNULL(ID.numQtyItemsReq,0) * ISNULL(IChild.monAverageCost,0)) FROM ItemDetails ID INNER JOIN Item IChild ON ID.numChildItemID=IChild.numItemCode WHERE ID.numItemKitID=@numItemCode AND ID.numChildItemID <> @numOverheadServiceItemID),0) monMaterialCost 
		,ISNULL((SELECT SUM(ISNULL(ID.numQtyItemsReq,0) * ISNULL(IChild.monListPrice,0)) FROM ItemDetails ID INNER JOIN Item IChild ON ID.numChildItemID=IChild.numItemCode WHERE ID.numItemKitID=@numItemCode AND ID.numChildItemID = @numOverheadServiceItemID),0) monMFGOverhead
		,@monLaborCost AS monLaborCost
END
GO
