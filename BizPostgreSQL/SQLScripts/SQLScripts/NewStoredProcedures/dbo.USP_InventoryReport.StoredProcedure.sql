--exec USP_INVENTORYREPORT @numDomainID=72,@numUserCntID=1,@numDateRange=19,@numBasedOn=11,@numItemType=21,@numItemClass=22,@numItemClassId=0,@DateCondition=' OppDate between ''01/01/2008'' and ''11/23/2008'''
-- select * from financialyear
-- exec USP_InventoryReport @numDomainID=72,@numUserCntID=17,@numDateRange=19,@numBasedOn=9,@numItemType=21,@numItemClass=22,@numItemClassId=0,@DateCondition=' oppDate between ''01/01/2009 '' And '' 01/18/2010''',@numWareHouseId=0
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_INVENTORYREPORT')
DROP PROCEDURE USP_INVENTORYREPORT
GO
CREATE PROCEDURE [dbo].[USP_INVENTORYREPORT]
(@numDomainID AS NUMERIC(9),
@numUserCntID AS NUMERIC(9),
@numDateRange AS NUMERIC(9),
@numBasedOn AS NUMERIC(9),
@numItemType AS NUMERIC(9),
@numItemClass AS NUMERIC(9),
@numItemClassId AS NUMERIC(9),
@DateCondition AS VARCHAR(100),
@numWareHouseId AS NUMERIC,
@dtFromDate AS DATE,
@dtToDate AS DATE,
@CurrentPage INT = 0,                                                            
@PageSize INT,
@TotRecs NUMERIC(18,0) OUTPUT)
AS 
BEGIN

DELETE FROM InventroyReportDTL WHERE numUserID=@numUserCntID;

INSERT INTO InventroyReportDTL SELECT @numDomainID,@numUserCntID,@numDateRange,0;
INSERT INTO InventroyReportDTL SELECT @numDomainID,@numUserCntID,@numBasedOn,0; 
INSERT INTO InventroyReportDTL SELECT @numDomainID,@numUserCntID,@numItemType,0;
INSERT INTO InventroyReportDTL SELECT @numDomainID,@numUserCntID,@numItemClass,@numItemClassId;

DECLARE @FilterItemType VARCHAR(MAX);
DECLARE @FilterBasedOn VARCHAR(MAX);
DECLARE @FilterDateRange VARCHAR(MAX);

DECLARE @strSql VARCHAR(MAX);
DECLARE @strGroup VARCHAR(MAX);
DECLARE @ItemClassID VARCHAR(100);
DECLARE @strSort VARCHAR(MAX);

SELECT @FilterItemType= vcCondition from InventoryReportMaster a inner join InventroyReportDTL b ON 
a.numReportID=b.numReportID and b.numUserID=@numUserCntID and a.numTypeID=1;

SELECT @FilterBasedOn= vcCondition from InventoryReportMaster a inner join InventroyReportDTL b ON 
a.numReportID=b.numReportID and b.numUserID=@numUserCntID and a.numTypeID=2;

SELECT @FilterDateRange = vcDesc from InventoryReportMaster a inner join InventroyReportDTL b ON
a.numReportID=b.numReportID and b.numUserID=@numUserCntID and a.numTypeID=3;

SELECT @ItemClassID = (CASE numItemClassification WHEN 0 THEN '' ELSE ' AND a.numItemClassification=' + CAST(numItemClassification AS VARCHAR(50))  END) FROM  InventroyReportDTL WHERE numUserID = @numUserCntID 

PRINT '@FilterItemType:' + @FilterItemType
PRINT '@FilterBasedOn:' + @FilterBasedOn
PRINT '@FilterDateRange:' + @FilterDateRange
PRINT '@ItemClassID: ' + @ItemClassID

CREATE TABLE #ItemWareHouse 
(RowID NUMERIC(18,0),
numItemCode NUMERIC(18),
vcItemName VARCHAR(2000),
--vcModelID VARCHAR(150),
vcSKU VARCHAR(50),
ItemClass VARCHAR(150),
ItemDesc VARCHAR(MAX),
numOnHand NUMERIC(18,2),
AVGCOST DECIMAL(20,5),
numAllocation NUMERIC(18,2),
Amount DECIMAL(20,5),
SalesUnit numeric(18,2),
ReturnQty numeric(18,2),
ReturnPer numeric(18,2),
COGS DECIMAL(20,5),
PROFIT DECIMAL(20,5),
SALESRETURN DECIMAL(20,5));

DECLARE @strItemSQL AS NVARCHAR(MAX)
CREATE TABLE #tmpItems(RowID NUMERIC(18,0),numItemCode NUMERIC(18,0))


SET @strItemSQL = ' SELECT ROW_NUMBER() OVER (ORDER BY [numItemCode]) AS [RowID] ,[numItemCode] FROM Item a WHERE a.[numDomainID] = ' + CONVERT(VARCHAR(10),@numDomainID) + ' 
AND 1=1 ' + @FilterItemType + ' ' + @ItemClassID

PRINT @strItemSQL
INSERT INTO [#tmpItems]( [RowID], [numItemCode] )
EXEC (@strItemSQL);

SET @strSql='SELECT DISTINCT RowID, A.numItemCode,A.vcItemName--,a.vcModelID
	, A.vcSKU, A.numItemClass,isnull(txtItemDesc,'''') AS ItemDesc,
(CASE WHEN CONVERT(DATE,GETDATE()) = CONVERT(DATE, ''' + CONVERT(VARCHAR(50),@dtToDate) + ''') THEN SUM(ISNULL(c.[numOnHand],0))
	 ELSE
			(SELECT TOP 1 [WHIT].[numOnHand] 
					FROM [WareHouseItems_Tracking] WHIT 
					INNER JOIN WareHouseItems ON WHIT.numWareHouseItemID = WareHouseItems.numWareHouseItemID
					WHERE WareHouseItems.numWareHouseID =  (CASE ' + CAST(@numWareHouseId as varchar(10)) + '  WHEN 0 THEN WareHouseItems.numWareHouseID ELSE '  + cast(@numWareHouseId as varchar(10)) + ' END) 
					AND [WHIT].[dtCreatedDate] BETWEEN CONVERT(DATE, ''' + CONVERT(VARCHAR(50),@dtFromDate) + ''') And CONVERT(DATE, + ''' + CONVERT(VARCHAR(50),@dtToDate) + ''') 
					ORDER BY [WHIT].[numWareHouseItemTrackingID] DESC ) 
END) [numOnHand], 
(CASE WHEN CONVERT(DATE,GETDATE()) = CONVERT(DATE, ''' + CONVERT(VARCHAR(50),@dtToDate) + ''') THEN ISNULL(A.[monAverageCost],0)
	 ELSE
			(SELECT TOP 1 [WHIT].[monAverageCost] 
					FROM [WareHouseItems_Tracking] WHIT 
					INNER JOIN WareHouseItems ON WHIT.numWareHouseItemID = WareHouseItems.numWareHouseItemID
					WHERE WareHouseItems.numWareHouseID =  (CASE ' + CAST(@numWareHouseId as varchar(10)) + '  WHEN 0 THEN WareHouseItems.numWareHouseID ELSE '  + cast(@numWareHouseId as varchar(10)) + ' END) 
					AND [WHIT].[dtCreatedDate] BETWEEN CONVERT(DATE, ''' + CONVERT(VARCHAR(50),@dtFromDate) + ''') And CONVERT(DATE, + ''' + CONVERT(VARCHAR(50),@dtToDate) + ''')
					ORDER BY [WHIT].[numWareHouseItemTrackingID] DESC ) 
END) [AVGCOST],
(CASE WHEN CONVERT(DATE,GETDATE()) = CONVERT(DATE, ''' + CONVERT(VARCHAR(50),@dtFromDate) + ''') THEN SUM(ISNULL(c.[numAllocation],0))
	 ELSE
			(SELECT TOP 1 [WHIT].[numAllocation] 
					FROM [WareHouseItems_Tracking] WHIT 
					INNER JOIN WareHouseItems ON WHIT.numWareHouseItemID = WareHouseItems.numWareHouseItemID
					WHERE WareHouseItems.numWareHouseID =  (CASE ' + CAST(@numWareHouseId as varchar(10)) + '  WHEN 0 THEN WareHouseItems.numWareHouseID ELSE '  + cast(@numWareHouseId as varchar(10)) + ' END) 
					AND [WHIT].[dtCreatedDate] BETWEEN CONVERT(DATE, ''' + CONVERT(VARCHAR(50),@dtFromDate) + ''') And CONVERT(DATE, + ''' + CONVERT(VARCHAR(50),@dtToDate) + ''') 
					ORDER BY [WHIT].[numWareHouseItemTrackingID] DESC ) 
END) [numAllocation],
SUM(COGS) as Amount,
sum(SalesUnit) as SalesUnit,
sum(ReturnQty) as ReturnQty,
isnull((sum(ReturnQty)/NULLIF(sum(SalesUnit),0)*100),0) as ReturnPer,
SUM(COGS) AS COGS,
SUM(PROFIT) AS PROFIT,
SUM(SALESRETURN) AS SALESRETURN
FROM Item A
JOIN #tmpItems ON #tmpItems.numItemCode = A.numItemCode 
JOIN WareHouseItems C ON C.numItemID = A.numItemCode AND numWareHouseID = (CASE ' + CAST(@numWareHouseId as varchar(10)) + '  WHEN 0 THEN numWareHouseID ELSE '  + cast(@numWareHouseId as varchar(10)) + ' END) ' + @FilterItemType  +  @ItemClassID + '
LEFT JOIN 
	VIEW_INVENTORYOPPSALES B 
ON  
	b.numItemCode = A.numItemCode
	AND OppDate BETWEEN CONVERT(DATE, ''' + CONVERT(VARCHAR(50),@dtFromDate) + ''') And CONVERT(DATE, + ''' + CONVERT(VARCHAR(50),@dtToDate) + ''')
INNER JOIN 
	OpportunityBizDocItems
ON
	B.numOppBizDocItemID = OpportunityBizDocItems.numOppBizDocItemID
	AND OpportunityBizDocItems.numWarehouseItmsID = C.numWareHouseItemID
WHERE 1=1 AND A.[numDomainID] = ' + CONVERT(VARCHAR(10),@numDomainID)  

IF CONVERT(DATE,GETDATE()) <> CONVERT(DATE,@dtToDate)
	BEGIN
		SET @strsql = @strsql + ' AND CONVERT(DATE, bintModifiedDate) >= CONVERT(DATE, + ''' + CONVERT(VARCHAR(50),@dtFromDate) + ''') AND CONVERT(DATE, bintModifiedDate) <= CONVERT(DATE, + ''' + CONVERT(VARCHAR(50),@dtToDate) + ''')'
	END

DECLARE @firstRec AS INTEGER                                                            
DECLARE @lastRec AS INTEGER                                                            
SET @firstRec = ( @CurrentPage - 1 ) * @PageSize                                                            
SET @lastRec = ( @CurrentPage * @PageSize + 1 ) 

IF @CurrentPage > 0 
BEGIN
	SET @strsql = @strsql + ' AND RowID > ' + CONVERT(VARCHAR(10),@firstRec)+ ' AND RowID < '+ CONVERT(VARCHAR(10),@lastRec) + ' '
END

set @strGroup = ' GROUP BY A.numItemCode,A.vcItemName--,a.vcModelID
,A.vcSKU,A.numItemClass,ISNULL(txtItemDesc,''''),A.[monAverageCost], RowID '

SET @TotRecs = (SELECT COUNT(*) FROM [#tmpItems])
PRINT @TotRecs

--PRINT @DateCondition
--IF LEN(@DateCondition) > 0
--	SET @strsql = @strsql +' AND '+ @DateCondition + ' '+ @strGroup
--ELSE
--	SET @strsql = @strsql + ' ' + @strGroup + ';'

SET @strsql = @strsql + ' ' + @strGroup 
--SET @strsql = @strsql + ' ORDER BY ' + @FilterBasedOn + ' DESC '

PRINT @strSql
INSERT INTO #ItemWareHouse 
EXEC (@strSql);

--SELECT * FROM [#ItemWareHouse] AS IWH

--DECLARE @firstRec AS INTEGER                                                            
--DECLARE @lastRec AS INTEGER                                                            
--SET @firstRec = ( @CurrentPage - 1 ) * @PageSize                                                            
--SET @lastRec = ( @CurrentPage * @PageSize + 1 ) 

IF NOT EXISTS(SELECT * FROM #ItemWareHouse)
BEGIN
	SET @TotRecs = 0
END

SET @strSort = ' SELECT *, (ISNULL(numOnHand,0) + ISNULL(numAllocation,0)) [TotalInventory],  ((ISNULL(numOnHand,0) + ISNULL(numAllocation,0)) * AVGCOST) AS [InventoryValue] FROM #ItemWareHouse ORDER BY ' + @FilterBasedOn + ' DESC '

--WHERE 1=1 AND RowID > ' + CONVERT(VARCHAR(10),@firstRec)+ ' AND RowID < '+ CONVERT(VARCHAR(10),@lastRec) + ' 


--CREATE TABLE #InventoryReport 
--(RowID NUMERIC(18,0),
--numItemCode NUMERIC(18),
--vcItemName VARCHAR(2000),
--vcModelID VARCHAR(150),
--ItemClass VARCHAR(150),
--ItemDesc VARCHAR(MAX),
--numOnHand NUMERIC(18,2),
--AVGCOST DECIMAL(20,5),
--TotalInventory NUMERIC(18,0),
--InventoryValue DECIMAL(20,5),
--numAllocation NUMERIC(18,2));

PRINT @strSort
--INSERT INTO #InventoryReport 

EXEC (@strSort);

--SELECT * FROM #InventoryReport -- WHERE RowID > @firstRec AND RowID < @lastRec

--EXEC ('DROP TABLE #ItemWareHouse')
DROP TABLE #tmpItems
DROP TABLE #ItemWareHouse

END