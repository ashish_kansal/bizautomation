
GO
/****** Object:  StoredProcedure [dbo].[usp_UpdateStagePercentageDetails]    Script Date: 09/21/2010 12:00:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
----Created By Prasanta    
    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ManageProcesstoOpportunity')
DROP PROCEDURE USP_ManageProcesstoOpportunity
GO
CREATE PROCEDURE [dbo].[USP_ManageProcesstoOpportunity]
@numDomainID as numeric(9)=0,    
@numProcessId as numeric(18)=0,      
@numCreatedBy as numeric(18)=0,
@numOppId AS NUMERIC(18,0)=0,   
@numProjectId AS NUMERIC(18,0)=0
as    
BEGIN 
 INSERT  INTO Sales_process_List_Master ( Slp_Name,
                                                 numdomainid,
                                                 pro_type,
                                                 numCreatedby,
                                                 dtCreatedon,
                                                 numModifedby,
                                                 dtModifiedOn,tintConfiguration,bitAutomaticStartTimer,bitAssigntoTeams,numOppId,numTaskValidatorId,numProjectId)
     SELECT Slp_Name,
            numdomainid,
            pro_type,
            numCreatedby,
            dtCreatedon,
            numModifedby,
            dtModifiedOn,tintConfiguration,bitAutomaticStartTimer,bitAssigntoTeams,@numOppId,numTaskValidatorId,@numProjectId FROM Sales_process_List_Master WHERE Slp_Id=@numProcessId    
	DECLARE @numNewProcessId AS NUMERIC(18,0)=0
	SET @numNewProcessId=(SELECT SCOPE_IDENTITY())  
	IF(@numOppId>0)
	BEGIN
		UPDATE OpportunityMaster SET numBusinessProcessID=@numNewProcessId WHERE numOppId=@numOppId
	END
	IF(@numProjectId>0)
	BEGIN
		UPDATE ProjectsMaster SET numBusinessProcessID=@numNewProcessId WHERE numProId=@numProjectId
	END

	INSERT INTO StagePercentageDetails(
		numStagePercentageId, 
		tintConfiguration, 
		vcStageName, 
		numDomainId, 
		numCreatedBy, 
		bintCreatedDate, 
		numModifiedBy, 
		bintModifiedDate, 
		slp_id, 
		numAssignTo, 
		vcMileStoneName, 
		tintPercentage, 
		tinProgressPercentage, 
		dtStartDate, 
		numParentStageID, 
		intDueDays, 
		numProjectID, 
		numOppID, 
		vcDescription, 
		bitIsDueDaysUsed,
		numTeamId, 
		bitRunningDynamicMode,
		numStageOrder,
		numParentStageDetailsId
	)
	SELECT
		numStagePercentageId, 
		tintConfiguration, 
		vcStageName, 
		numDomainId, 
		@numCreatedBy, 
		GETDATE(), 
		@numCreatedBy, 
		GETDATE(), 
		@numNewProcessId, 
		numAssignTo, 
		vcMileStoneName, 
		tintPercentage, 
		tinProgressPercentage, 
		GETDATE(), 
		numParentStageID, 
		intDueDays, 
		@numProjectId, 
		@numOppId, 
		vcDescription, 
		bitIsDueDaysUsed,
		numTeamId, 
		bitRunningDynamicMode,
		numStageOrder,
		numStageDetailsId
	FROM
		StagePercentageDetails
	WHERE
		slp_id=@numProcessId	

	INSERT INTO StagePercentageDetailsTask(
		numStageDetailsId, 
		vcTaskName, 
		numHours, 
		numMinutes, 
		numAssignTo, 
		numDomainID, 
		numCreatedBy, 
		dtmCreatedOn,
		numOppId,
		numProjectId,
		numParentTaskId,
		bitDefaultTask,
		bitSavedTask,
		numOrder,
		numReferenceTaskId
	)
	SELECT 
		CASE WHEN @numOppId>0
		THEN
		(SELECT TOP 1 SFD.numStageDetailsId FROM StagePercentageDetails AS SFD WHERE SFD.vcStageName=SP.vcStageName AND SFD.numOppID=@numOppId AND SFD.numParentStageDetailsId=SP.numStageDetailsId)
		WHEN @numProjectId>0
		THEN
		(SELECT TOP 1 SFD.numStageDetailsId FROM StagePercentageDetails AS SFD WHERE SFD.vcStageName=SP.vcStageName AND SFD.numProjectID=@numProjectId AND SFD.numParentStageDetailsId=SP.numStageDetailsId) ELSE 0 END,
		vcTaskName,
		numHours,
		numMinutes,
		ST.numAssignTo,
		@numDomainID,
		@numCreatedBy,
		GETDATE(),
		@numOppId,
		@numProjectId,
		0,
		1,
		CASE WHEN SP.bitRunningDynamicMode=1 THEN 0 ELSE 1 END,
		numOrder,
		numTaskId
	FROM 
		StagePercentageDetailsTask AS ST
	LEFT JOIN
		StagePercentageDetails As SP
	ON
		ST.numStageDetailsId=SP.numStageDetailsId
	WHERE
		ISNULL(ST.numOppId,0)=0 AND ISNULL(ST.numProjectId,0)=0 AND
		ST.numStageDetailsId IN(SELECT numStageDetailsId FROM 
		StagePercentageDetails As ST
	WHERE
		ST.slp_id=@numProcessId)
	ORDER BY ST.numOrder

END 