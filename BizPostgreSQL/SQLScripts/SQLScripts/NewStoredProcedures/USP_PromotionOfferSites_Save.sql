SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_PromotionOfferSites_Save')
DROP PROCEDURE USP_PromotionOfferSites_Save
GO
CREATE PROCEDURE [dbo].[USP_PromotionOfferSites_Save]    
	@numProId AS NUMERIC(18,0),
	@numDomainID AS NUMERIC(18,0),
	@vcSelectedSites AS VARCHAR(MAX)
AS
BEGIN
	DECLARE @TEMP TABLE
	(
		numSiteID NUMERIC(18,0)
	)

	INSERT INTO
		@TEMP
	SELECT
		Id
	FROM
		dbo.SplitIDs(@vcSelectedSites,',')

	-- FIRST DELETE THE SITES WHICH ARE REMOVED FROM SELECTION
	DELETE FROM PromotionOfferSites WHERE numPromotionID = @numProId AND numSiteID NOT IN (SELECT Id FROM @TEMP)

	-- ADD NEW SITES
	INSERT INTO PromotionOfferSites (numPromotionID,numSiteID) SELECT @numProId,numSiteID FROM @TEMP WHERE numSiteID NOT IN (SELECT ISNULL(numSiteID,0) FROM PromotionOfferSites WHERE numPromotionID = @numProId)
END 
GO