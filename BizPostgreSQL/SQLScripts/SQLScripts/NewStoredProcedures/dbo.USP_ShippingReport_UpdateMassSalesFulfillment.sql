GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ShippingReport_UpdateMassSalesFulfillment')
DROP PROCEDURE USP_ShippingReport_UpdateMassSalesFulfillment
GO
CREATE PROCEDURE [dbo].[USP_ShippingReport_UpdateMassSalesFulfillment]
	@numDomainId NUMERIC(18,0)
	,@numUserCntID NUMERIC(18,0)
	,@numOppID NUMERIC(18,0)
    ,@numShippingReportID NUMERIC(18,0)
	,@vcShippingDetail VARCHAR(MAX)
AS
BEGIN

	
	DECLARE @vcFromName VARCHAR(1000)
    DECLARE @vcFromCompany VARCHAR(1000)
    DECLARE @vcFromPhone VARCHAR(100)
    DECLARE @vcFromAddressLine1 VARCHAR(50)
    DECLARE @vcFromAddressLine2 VARCHAR(50) = ''
    DECLARE @vcFromCity VARCHAR(50)
	DECLARE @vcFromState VARCHAR(50)
    DECLARE @vcFromZip VARCHAR(50)
    DECLARE @vcFromCountry VARCHAR(50)
	DECLARE @bitFromResidential BIT = 0

	DECLARE @vcToName VARCHAR(1000)
    DECLARE @vcToCompany VARCHAR(1000)
    DECLARE @vcToPhone VARCHAR(100)
    DECLARE @vcToAddressLine1 VARCHAR(50)
    DECLARE @vcToAddressLine2 VARCHAR(50) = ''
	DECLARE @vcToCity VARCHAR(50)
	DECLARE @vcToState VARCHAR(50)
    DECLARE @vcToZip VARCHAR(50)
    DECLARE @vcToCountry VARCHAR(50)
    DECLARE @bitToResidential BIT = 0

	DECLARE @IsCOD BIT,@IsDryIce BIT,@IsHoldSaturday BIT,@IsHomeDelivery BIT,@IsInsideDelevery BIT,@IsInsidePickup BIT,@IsReturnShipment BIT,@IsSaturdayDelivery BIT,@IsSaturdayPickup BIT,@IsAdditionalHandling BIT,@IsLargePackage BIT
	DECLARE @numCODAmount NUMERIC(18,2),@vcCODType VARCHAR(50),@vcDeliveryConfirmation VARCHAR(1000),@vcDescription VARCHAR(MAX)
	DECLARE @tintSignatureType TINYINT
	DECLARE @tintPayorType TINYINT=0
	DECLARE @vcPayorAccountNo VARCHAR(20)=''
	DECLARE @numPayorCountry NUMERIC(18,0)
	DECLARE @vcPayorZip VARCHAR(50)=''
	DECLARE @numTotalCustomsValue NUMERIC(18,2)
	DECLARE @numTotalInsuredValue NUMERIC(18,2)

	IF LEN(ISNULL(@vcShippingDetail,'')) > 0
	BEGIN
		DECLARE @hDocShippingDetail as INTEGER
		EXEC sp_xml_preparedocument @hDocShippingDetail OUTPUT, @vcShippingDetail

		SELECT 
			@vcFromName=FromContact
			,@vcFromCompany=FromCompany
			,@vcFromPhone=FromPhone
			,@vcFromAddressLine1=FromAddress1
			,@vcFromAddressLine2=FromAddress2
			,@vcFromCity=FromCity
			,@vcFromState=CAST(FromState AS VARCHAR(18))
			,@vcFromZip=FromZip
			,@vcFromCountry=CAST(FromCountry AS VARCHAR(18))
			,@bitFromResidential=IsFromResidential
			,@vcToName=ToContact
			,@vcToCompany=ToCompany
			,@vcToPhone=ToPhone
			,@vcToAddressLine1=ToAddress1
			,@vcToAddressLine2=ToAddress2
			,@vcToCity=ToCity
			,@vcToState=CAST(ToState AS VARCHAR(18))
			,@vcToZip=ToZip
			,@vcToCountry=CAST(ToCountry AS VARCHAR(18))
			,@bitToResidential=IsToResidential
			,@tintPayorType=PayerType
			,@vcPayorAccountNo=AccountNo
			,@numPayorCountry=Country
			,@vcPayorZip=ZipCode
			,@IsCOD = IsCOD
			,@IsHomeDelivery = IsHomeDelivery
			,@IsInsideDelevery = IsInsideDelivery
			,@IsInsidePickup = IsInsidePickup
			,@IsSaturdayDelivery = IsSaturdayDelivery
			,@IsSaturdayPickup = IsSaturdayPickup
			,@IsAdditionalHandling = IsAdditionalHandling
			,@IsLargePackage=IsLargePackage
			,@numTotalInsuredValue = TotalInsuredValue
            ,@numTotalCustomsValue = TotalCustomsValue
			,@vcCODType = CODType
			,@numCODAmount = CODAmount
			,@vcDescription = [Description]
			,@tintSignatureType = CAST(ISNULL(SignatureType,'0') AS TINYINT)
		FROM 
			OPENXML(@hDocShippingDetail,'/NewDataSet/Table1',2)                                                                          
		WITH                       
		(
			FromContact VARCHAR(1000)
            ,FromPhone VARCHAR(100)
            ,FromCompany VARCHAR(1000)
            ,FromAddress1 VARCHAR(50)
            ,FromAddress2 VARCHAR(50)
            ,FromCountry NUMERIC(18,0)
            ,FromState NUMERIC(18,0)
            ,FromCity VARCHAR(50)
            ,FromZip VARCHAR(50)
            ,IsFromResidential BIT
            ,ToContact VARCHAR(1000)
            ,ToPhone VARCHAR(100)
            ,ToCompany VARCHAR(1000)
            ,ToAddress1 VARCHAR(50)
            ,ToAddress2 VARCHAR(50)
            ,ToCountry NUMERIC(18,0)
            ,ToState NUMERIC(18,0)
            ,ToCity VARCHAR(50)
            ,ToZip VARCHAR(50)
            ,IsToResidential BIT
            ,PayerType TINYINT
            ,ReferenceNo VARCHAR(200)
            ,AccountNo VARCHAR(20)
            ,ZipCode VARCHAR(50)
            ,Country NUMERIC(18,0)
            ,IsAdditionalHandling BIT
            ,IsCOD BIT
            ,IsHomeDelivery BIT
            ,IsInsideDelivery BIT
            ,IsInsidePickup BIT
            ,IsLargePackage BIT
            ,IsSaturdayDelivery BIT
            ,IsSaturdayPickup BIT
            ,SignatureType TINYINT
            ,[Description] VARCHAR(MAX)
            ,TotalInsuredValue NUMERIC(18,2)
            ,TotalCustomsValue NUMERIC(18,2)
            ,CODType VARCHAR(50)
            ,CODAmount NUMERIC(18,2)
		)


		EXEC sp_xml_removedocument @hDocShippingDetail

		UPDATE
			ShippingReport
		SET
			IsCOD=@IsCOD
			,IsHomeDelivery=@IsHomeDelivery
			,IsInsideDelevery=@IsInsideDelevery
			,IsInsidePickup=@IsInsidePickup
			,IsSaturdayDelivery=@IsSaturdayDelivery
			,IsSaturdayPickup=@IsSaturdayPickup
			,IsAdditionalHandling=@IsAdditionalHandling
			,IsLargePackage=@IsLargePackage
			,tintSignatureType=@tintSignatureType
			,vcFromName=@vcFromName
			,vcFromCompany=@vcFromCompany
			,vcFromPhone=@vcFromPhone
			,vcFromAddressLine1=@vcFromAddressLine1
			,vcFromCity=@vcFromCity
			,vcFromState=CAST(@vcFromState AS VARCHAR(18))
			,vcFromZip=@vcFromZip
			,vcFromCountry=CAST(@vcFromCountry AS VARCHAR(18))
			,bitFromResidential=@bitFromResidential
			,vcToName=@vcToName
			,vcToCompany=@vcToCompany
			,vcToPhone=@vcToPhone
			,vcToAddressLine1=@vcToAddressLine1
			,vcToCity=@vcToCity
			,vcToState=CAST(@vcToState AS VARCHAR(18))
			,vcToZip=@vcToZip
			,vcToCountry=CAST(@vcToCountry AS VARCHAR(18))
			,bitToResidential=@bitToResidential
			,vcCODType=@vcCODType
			,vcDeliveryConfirmation=@vcDeliveryConfirmation
			,vcDescription=@vcDescription
			,numTotalInsuredValue=@numTotalInsuredValue
            ,numTotalCustomsValue=@numTotalInsuredValue
            ,numCODAmount=@numCODAmount
			,tintPayorType=@tintPayorType
			,vcPayorAccountNo=@vcPayorAccountNo
			,numPayorCountry=@numPayorCountry
			,vcPayorZip=@vcPayorZip
		WHERE
			numShippingReportId=@numShippingReportID
	END
END
GO