-- EXEC USP_GetEmbeddedCostItems 156,110
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetEmbeddedCostItems')
DROP PROCEDURE USP_GetEmbeddedCostItems
GO
CREATE PROCEDURE USP_GetEmbeddedCostItems
    @numEmbeddedCostID NUMERIC,
    @numDomainID NUMERIC
AS 
    BEGIN
		
        IF EXISTS ( SELECT  *
                    FROM    [EmbeddedCostItems]
                    WHERE   [numEmbeddedCostID] = @numEmbeddedCostID ) 
            BEGIN
                SELECT 
                        --ECI.[numEmbeddedCostID],
                        BDI.[numOppBizDocItemID],
                        ISNULL(ECI.[monAmount],0) monAmount,
                        I.[fltHeight],
                        I.[fltLength],
                        I.[fltWeight],
                        I.[fltWidth],
                        BDI.[numUnitHour],
                        I.[vcUnitofMeasure],
                        OI.[vcItemName],
                        OI.[vcModelID]
                FROM    [OpportunityBizDocItems] BDI
                        INNER JOIN [OpportunityItems] OI ON OI.[numoppitemtCode] = BDI.[numOppItemID]
                        INNER JOIN Item i ON i.[numItemCode] = BDI.[numItemCode]
                        LEFT JOIN [EmbeddedCostItems] ECI ON ECI.[numOppBizDocItemID] = BDI.[numOppBizDocItemID]
                        AND ECI.[numEmbeddedCostID]=@numEmbeddedCostID
                WHERE   
					BDI.[numOppBizDocID] IN (SELECT numOppBizDocID FROM [EmbeddedCost] WHERE [numEmbeddedCostID]=@numEmbeddedCostID)
					AND 
					I.[numDomainID] = @numDomainID
	
            END
            ELSE 
			BEGIN
				SELECT  
                        BDI.[numOppBizDocItemID],
                        0 AS [monAmount],
                        I.[fltHeight],
                        I.[fltLength],
                        I.[fltWeight],
                        I.[fltWidth],
                        BDI.[numUnitHour],
                        I.[vcUnitofMeasure],
                        OI.[vcItemName],
                        OI.[vcModelID]
                FROM    [OpportunityBizDocItems] BDI
                        INNER JOIN [OpportunityItems] OI ON OI.[numoppitemtCode] = BDI.[numOppItemID]
                        INNER JOIN Item i ON i.[numItemCode] = BDI.[numItemCode]
                WHERE   
						BDI.[numOppBizDocID] IN (SELECT [numOppBizDocID] FROM [EmbeddedCost] WHERE [numEmbeddedCostID] = @numEmbeddedCostID)
						AND I.[numDomainID] = @numDomainID
			END
        
    END