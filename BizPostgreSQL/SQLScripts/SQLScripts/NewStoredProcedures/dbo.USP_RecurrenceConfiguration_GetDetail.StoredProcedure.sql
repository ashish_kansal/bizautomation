GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_RecurrenceConfiguration_GetDetail')
DROP PROCEDURE USP_RecurrenceConfiguration_GetDetail
GO
  
-- =============================================  
-- Author:  Sandeep
-- Create date: 9 Oct 2014
-- Description: Inserts recurrence configuration
-- =============================================  
Create PROCEDURE [dbo].[USP_RecurrenceConfiguration_GetDetail]   
	@numRecConfigID NUMERIC(18,0)
AS  
BEGIN  
 
 SET NOCOUNT ON;  

	DECLARE @numType SMALLINT
	DECLARE @numDomainID NUMERIC(18,0)

	SELECT 
		@numType = numType,
		@numDomainID = numDomainID
	FROM 
		RecurrenceConfiguration 
	WHERE 
		numRecConfigID = @numRecConfigID

	SELECT
		numRecConfigID,
		numType,
		vcType,
		vcFrequency,
		dbo.FormatedDateFromDate(dtNextRecurrenceDate,@numDomainID) AS dtNextRecurrenceDate,
		dbo.FormatedDateFromDate(dtStartDate,@numDomainID) AS dtStartDate,
		dbo.FormatedDateFromDate(dtEndDate,@numDomainID) AS dtEndDate,
		numTransaction,
		bitCompleted,
		bitDisabled
	FROM
		RecurrenceConfiguration
	WHERE
		numRecConfigID = @numRecConfigID
 
 If @numType = 1 --Sales Order
 BEGIN
		SELECT
			OpportunityMaster.numOppId,
			OpportunityMaster.vcPOppName,
			dbo.FormatedDateFromDate(RecurrenceTransaction.dtCreatedDate,@numDomainID) AS dtCreatedDate,
			(CASE WHEN RecurrenceTransaction.numErrorID IS NULL THEN 0 ELSE 1 END) AS bitErrorOccured,
			RecurrenceErrorLog.vcMessage
		FROM
			RecurrenceTransaction
		INNER JOIN
			RecurrenceConfiguration
		ON
			RecurrenceTransaction.numRecConfigID = RecurrenceConfiguration.numRecConfigID
		LEFT JOIN
			OpportunityMaster
		ON
			RecurrenceTransaction.numRecurrOppID = OpportunityMaster.numOppId
		LEFT JOIN
			RecurrenceErrorLog
		ON
			RecurrenceErrorLog.numRecConfigID = RecurrenceConfiguration.numRecConfigID AND
			RecurrenceErrorLog.numErrorID = RecurrenceTransaction.numErrorID 
		WHERE
			RecurrenceTransaction.numRecConfigID = @numRecConfigID
 END
  
 IF @numType = 2 --Invoice
 BEGIN
	SELECT
		OpportunityBizDocs.numOppId,
		OpportunityBizDocs.numOppBizDocsId,
		OpportunityBizDocs.vcBizDocID AS vcBizDocName,
		dbo.FormatedDateFromDate(RecurrenceTransaction.dtCreatedDate,@numDomainID) AS dtCreatedDate,
		(CASE WHEN RecurrenceTransaction.numErrorID IS NULL THEN 0 ELSE 1 END) AS bitErrorOccured,
		RecurrenceErrorLog.vcMessage
	FROM
		RecurrenceTransaction
	INNER JOIN
		RecurrenceConfiguration
	ON
		RecurrenceTransaction.numRecConfigID = RecurrenceConfiguration.numRecConfigID
	LEFT JOIN
		OpportunityBizDocs
	ON
		RecurrenceTransaction.numRecurrOppBizDocID = OpportunityBizDocs.numOppBizDocsId
	LEFT JOIN
		RecurrenceErrorLog
	ON
		RecurrenceErrorLog.numRecConfigID = RecurrenceConfiguration.numRecConfigID AND
		RecurrenceErrorLog.numErrorID = RecurrenceTransaction.numErrorID 
	WHERE
		RecurrenceConfiguration.numRecConfigID = @numRecConfigID

 END

 


END  
