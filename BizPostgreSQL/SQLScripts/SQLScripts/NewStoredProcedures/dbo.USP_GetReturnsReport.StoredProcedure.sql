/****** Object:  StoredProcedure [dbo].[USP_GetReturnsReport]    Script Date: 01/22/2009 01:39:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- EXEC Usp_getreturnsreport 1,1,20,0
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetReturnsReport')
DROP PROCEDURE USP_GetReturnsReport
GO
CREATE PROCEDURE [dbo].[USP_GetReturnsReport](
               @numDomainId NUMERIC(9)  = 0,
               @CurrentPage INT,
               @PageSize    INT,
               @TotRecs     INT  OUTPUT,
               @tintMode TINYINT=0,
               @numDivisionID NUMERIC(9)=0)
AS
  BEGIN
    /*paging logic*/
    DECLARE  @firstRec  AS INTEGER
    DECLARE  @lastRec  AS INTEGER
    SET @firstRec = (@CurrentPage
                       - 1)
                      * @PageSize
    SET @lastRec = (@CurrentPage
                      * @PageSize
                      + 1)
                      
DECLARE  @numListItemID NUMERIC(9)                      
IF @tintMode = 0 
	SELECT @numListItemID = numListItemID
    FROM   [ListDetails]
    WHERE  vcData = 'Returned' AND numListID=49 AND constFlag=1;
ELSE IF @tintMode = 1 
    SELECT @numListItemID = numListItemID
    FROM   [ListDetails]
    WHERE  vcData = 'Pending' AND numListID=49 AND constFlag=1;
                      
    
    PRINT @numListItemID;
    WITH salesreturn
         AS (SELECT Row_number()
                      OVER(ORDER BY opp.numitemcode) AS row,
                    I.vcItemName,
                    opp.vcitemdesc AS itemdesc,
                    opp.numunithour quantity,
                    r.numqtytoreturn AS quantitytoreturn,
                    r.numqtyreturned quantityreturned,
                    r.numreturnstatus,
                    r.numreasonforreturnid,
                    (r.[monPrice] * r.numTotalQty) AmountSold,
                    (r.[monPrice] * r.numqtyreturned) AmountReturned,
                    (r.numqtyreturned * 100/ r.numtotalqty ) AS ReturnPercent,
                    opp.monprice,
                    opp.montotamount,
                    r.numtotalqty,
                    '' AS serial,
                    I.vcModelID
            FROM opportunityitems AS opp
                    INNER JOIN [returns] AS r
                      ON opp.numoppid = r.numoppid
                    INNER JOIN [opportunitymaster] om
                      ON om.[numoppid] = opp.[numoppid]
                    INNER JOIN [Item] I
                      ON I.numItemCode = opp.numitemcode
             WHERE  opp.numitemcode = r.numitemcode
                    AND om.[numdomainid] = @numDomainId
                    AND r.numreturnstatus = @numListItemID
                    AND (@numDivisionID =0 OR Om.numDivisionId =@numDivisionID)
                   )
    SELECT *,dbo.[getlistiemname](numreasonforreturnid) reasonforreturn,dbo.[getlistiemname](numreturnstatus) ReturnStatus
    FROM   salesreturn
    WHERE  row > @firstRec
           AND row < @lastRec;
  END
