GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetPriceLevelAndDiscount')
DROP PROCEDURE USP_GetPriceLevelAndDiscount
GO
CREATE PROCEDURE [dbo].[USP_GetPriceLevelAndDiscount]     
(    
    @numDomainID NUMERIC(18, 0),
    @numItemCode NUMERIC(9),
    @numWareHouseItemID NUMERIC(9)
)   
AS

	CREATE TABLE #tmpPriceLevel ( numItemCode NUMERIC(9),numWareHouseItemID NUMERIC(9), monHighestPrice NUMERIC(18), monHighestDiscount NUMERIC(18) )
	
	DECLARE @monHighestDiscount AS NUMERIC(18);
	DECLARE @monHighestPrice AS NUMERIC(18);
	
    DECLARE @monListPrice AS NUMERIC(18,2) ;
        SET @monListPrice = 0
    DECLARE @monVendorCost AS NUMERIC(18,2) ;
        SET @monVendorCost = 0

    IF ( ( @numWareHouseItemID > 0 )
         AND EXISTS ( SELECT    *
                      FROM      item
                      WHERE     numItemCode = @numItemCode
                                AND charItemType = 'P' )
       ) 
        BEGIN      
            SELECT  @monListPrice = ISNULL(monWListPrice, 0)
            FROM    WareHouseItems
            WHERE   numWareHouseItemID = @numWareHouseItemID      
            IF @monListPrice = 0 
                SELECT  @monListPrice = monListPrice
                FROM    Item
                WHERE   numItemCode = @numItemCode       
        END      
    ELSE 
        BEGIN      
            SELECT  @monListPrice = monListPrice
            FROM    Item
            WHERE   numItemCode = @numItemCode      
        END 

    SELECT  @monVendorCost = dbo.[fn_GetVendorCost](@numItemCode)
	
	SELECT TOP 1 @monHighestPrice = CASE WHEN tintRuleType = 1
							  AND tintDiscountType = 1 --Deduct from List price & Percentage
							  THEN @monListPrice - ( @monListPrice * ( decDiscount / 100 ) )
							 WHEN tintRuleType = 1
								  AND tintDiscountType = 2 --Deduct from List price & Flat discount
								  THEN @monListPrice - decDiscount
							 WHEN tintRuleType = 2
								  AND tintDiscountType = 1 --Add to Primary Vendor Cost & Percentage
								  THEN @monVendorCost + ( @monVendorCost * ( decDiscount / 100 ) )
							 WHEN tintRuleType = 2
								  AND tintDiscountType = 2 --Add to Primary Vendor Cost & Flat discount
								  THEN @monVendorCost + decDiscount
							 WHEN tintRuleType = 3 --Named Price
								  THEN decDiscount
							 END,
			@monHighestDiscount = CASE WHEN tintRuleType = 1 AND tintDiscountType = 1 --Deduct from List price & Percentage
										 THEN decDiscount 
										 WHEN tintRuleType = 1 AND tintDiscountType = 2 --Deduct from List price & Flat discount
										 THEN (decDiscount * 100) / (@monListPrice - decDiscount)
										 WHEN tintRuleType = 2 AND tintDiscountType = 1 --Add to Primary Vendor Cost & Percentage
										 THEN decDiscount
										 WHEN tintRuleType = 2 AND tintDiscountType = 2 --Add to Primary Vendor Cost & Flat discount
										 THEN (decDiscount * 100) / (@monVendorCost + decDiscount)
										 WHEN tintRuleType = 3 --Named Price 
										 THEN 0
							 END				 
    FROM    [PricingTable]
    WHERE   ISNULL(numItemCode, 0) = @numItemCode AND ISNULL(numCurrencyID,0) = 0
    ORDER BY [numPricingID] 

    
    INSERT INTO #tmpPriceLevel (numItemCode,numWareHouseItemID,monHighestPrice,monHighestDiscount) 
    VALUES (@numItemCode, @numWareHouseItemID, ISNULL(@monHighestPrice,0), ISNULL(@monHighestDiscount,0))
    
SELECT numItemCode,numWareHouseItemID, monHighestPrice, monHighestDiscount FROM #tmpPriceLevel

IF OBJECT_ID('tempdb..#tmpPriceLevel') IS NOT NULL
	DROP TABLE #tmpPriceLevel
GO