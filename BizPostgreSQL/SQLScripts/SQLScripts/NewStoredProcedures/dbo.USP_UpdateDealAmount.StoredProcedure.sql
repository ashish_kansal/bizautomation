
GO
/****** Object:  StoredProcedure [dbo].[USP_UpdateDealAmount]    Script Date: 09/01/2009 01:15:03 ******/
SET ANSI_NULLS  ON 
GO
SET QUOTED_IDENTIFIER  ON 
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_UpdateDealAmount')
DROP PROCEDURE USP_UpdateDealAmount
GO
CREATE PROCEDURE [dbo].[USP_UpdateDealAmount]
AS
  BEGIN
    UPDATE [OpportunityBizDocs]
    SET    monDealAmount = dbo.[GetDealAmount](numOppId,GETUTCDATE(),numOppBizDocsId)
--    WHERE  bitBillingTerms = 1 -- Caused problem in accuracy of report
    
    UPDATE [OpportunityMaster]
    SET    monDealAmount = dbo.[GetDealAmount](numOppId,GETUTCDATE(),0)
    
  END
