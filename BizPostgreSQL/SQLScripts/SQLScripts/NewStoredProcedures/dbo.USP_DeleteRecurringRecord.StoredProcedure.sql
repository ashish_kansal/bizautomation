GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_DeleteRecurringRecord')
DROP PROCEDURE USP_DeleteRecurringRecord
GO
CREATE PROCEDURE USP_DeleteRecurringRecord 
@numOppRecID NUMERIC,
@numDomainID NUMERIC
AS 
BEGIN
	
	DELETE FROM dbo.OpportunityRecurring WHERE numOppRecID =@numOppRecID AND numDomainID=@numDomainID
END