GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_UniversalSMTPIMAP_Save')
DROP PROCEDURE dbo.USP_UniversalSMTPIMAP_Save
GO
CREATE PROCEDURE [dbo].[USP_UniversalSMTPIMAP_Save]
(
	@numDomainID NUMERIC(18,0),
    @vcSMTPServer VARCHAR(100),
	@numSMTPPort NUMERIC(18,0),
	@bitSMTPSSL BIT,
	@bitSMTPAuth BIT,
	@vcIMAPServer VARCHAR(100),
	@numIMAPPort NUMERIC(18,0),
	@bitIMAPSSL BIT,
	@bitIMAPAuth BIT
)
AS
BEGIN
	IF EXISTS (SELECT ID FROM UniversalSMTPIMAP WHERE numDomainID=@numDomainID)
	BEGIN
		UPDATE
			UniversalSMTPIMAP
		SET
			vcSMTPServer=@vcSMTPServer
			,numSMTPPort=@numSMTPPort
			,bitSMTPSSL=@bitSMTPSSL
			,bitSMTPAuth=@bitSMTPAuth
			,vcIMAPServer=@vcIMAPServer
			,numIMAPPort=@numIMAPPort
			,bitIMAPSSL=@bitIMAPSSL
			,bitIMAPAuth=@bitIMAPAuth
		WHERE
			numDomainID=@numDomainID
	END
	ELSE
	BEGIN
		INSERT INTO UniversalSMTPIMAP
		(
			numDomainID
			,vcSMTPServer
			,numSMTPPort
			,bitSMTPSSL
			,bitSMTPAuth
			,vcIMAPServer
			,numIMAPPort
			,bitIMAPSSL
			,bitIMAPAuth
		)
		VALUES
		(
			@numDomainID
			,@vcSMTPServer
			,@numSMTPPort
			,@bitSMTPSSL
			,@bitSMTPAuth
			,@vcIMAPServer
			,@numIMAPPort
			,@bitIMAPSSL
			,@bitIMAPAuth
		)
	END
END
GO


