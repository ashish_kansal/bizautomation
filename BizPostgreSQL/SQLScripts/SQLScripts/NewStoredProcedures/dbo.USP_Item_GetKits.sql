GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_Item_GetKits')
DROP PROCEDURE USP_Item_GetKits
GO
CREATE PROCEDURE [dbo].[USP_Item_GetKits]
	@numDomainID NUMERIC(18,0)
AS  
BEGIN
	SELECT 
		I.numItemCode
		,I.vcItemName
	FROM
		Item I
	WHERE
		I.numDomainID=@numDomainID
		AND ISNULL(I.bitKitParent,0) = 1
		AND (SELECT COUNT(*) FROM ItemDetails ID INNER JOIN Item IInner ON ID.numChildItemID=IInner.numItemCode	WHERE numItemKitID=I.numItemCode AND ISNULL(IInner.bitKitParent,0)=1) = 0
END
GO
