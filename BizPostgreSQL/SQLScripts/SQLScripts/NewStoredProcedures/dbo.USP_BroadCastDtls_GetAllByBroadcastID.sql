
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_BroadCastDtls_GetAllByBroadcastID' ) 
    DROP PROCEDURE USP_BroadCastDtls_GetAllByBroadcastID
GO

CREATE PROCEDURE USP_BroadCastDtls_GetAllByBroadcastID  
(  
 @numDomainID NUMERIC(18,0),
 @numBroadCastID NUMERIC(18,0)
)  
AS  
BEGIN  
-- SET NOCOUNT ON added to prevent extra result sets from  
-- interfering with SELECT statements.  
SET NOCOUNT ON;  

	SELECT  
		BroadCastDtls.numBroadCastID,
		BroadCastDtls.numBroadCastDtlId,
		ACI.numContactId,
        [dbo].[GetListIemName](ACI.vcDepartment) ContactDepartment,
        [dbo].[GetListIemName](ACI.vcCategory) ContactCategory,
                    
        ACI.vcGivenName,
        ACI.vcFirstName ContactFirstName,
        ACI.vcLastName ContactLastName,
        ACI.numDivisionId,
        dbo.GetListIemName(ACI.numContactType) ContactType,
        [dbo].[GetListIemName](ACI.numTeam) ContactTeam,
        ACI.numPhone ContactPhone,
        ACI.numPhoneExtension ContactPhoneExt,
        ACI.numCell ContactCell,
        ACI.numHomePhone ContactHomePhone,
        ACI.vcFax ContactFax,
        ACI.vcEmail ContactEmail,
        ACI.VcAsstFirstName AssistantFirstName,
        ACI.vcAsstLastName AssistantLastName,
        ACI.numAsstPhone ContactAssistantPhone,
        ACI.numAsstExtn ContactAssistantPhoneExt,
        ACI.vcAsstEmail ContactAssistantEmail,
        CASE WHEN ACI.charSex = 'M' THEN 'Male'
                WHEN ACI.charSex = 'F' THEN 'Female'
                ELSE '-'
        END AS ContactGender,
        ACI.bintDOB,
        dbo.GetAge(ACI.bintDOB, GETUTCDATE()) AS ContactAge,
        [dbo].[GetListIemName](ACI.vcPosition) ContactPosition,
        ACI.txtNotes,
        ACI.numCreatedBy,
        ACI.bintCreatedDate,
        ACI.numModifiedBy,
        ACI.bintModifiedDate,
        ACI.numDomainID,
        ACI.bitOptOut,
        dbo.fn_GetContactName(ACI.numManagerId) ContactManager,
        ACI.numRecOwner,
        [dbo].[GetListIemName](ACI.numEmpStatus) ContactStatus,
        ACI.vcTitle ContactTitle,
        ACI.vcAltEmail,
        ACI.vcItemId,
        ACI.vcChangeKey,
        ISNULL(( SELECT vcECampName
                    FROM   [ECampaign]
                    WHERE  numECampaignID = ACI.numECampaignID
                ), '') ContactDripCampaign,
        dbo.getContactAddress(ACI.numContactID) AS ContactPrimaryAddress,
        C.numCompanyId,
        C.vcCompanyName OrganizationName,
        C.numCompanyType,
        C.numCompanyRating,
        C.numCompanyIndustry,
        C.numCompanyCredit,
        C.txtComments,
        C.vcWebSite,
        C.vcWebLabel1,
        C.vcWebLink1,
        C.vcWebLabel2,
        C.vcWebLink2,
        C.vcWebLabel3,
        C.vcWebLink3,
        C.vcWeblabel4,
        C.vcWebLink4,
        C.numAnnualRevID,
        dbo.GetListIemName(C.numNoOfEmployeesId) OrgNoOfEmployee,
        C.vcHow,
        C.vcProfile,
        C.numCreatedBy,
        C.bintCreatedDate,
        C.numModifiedBy,
        C.bintModifiedDate,
        C.bitPublicFlag,
        C.numDomainID,
                    
        DM.numDivisionID,
        DM.numCompanyID,
        DM.vcDivisionName,
        DM.numGrpId,
        DM.numFollowUpStatus,
        DM.bitPublicFlag,
        DM.numCreatedBy,
        DM.bintCreatedDate,
        DM.numModifiedBy,
        DM.bintModifiedDate,
        DM.tintCRMType,
        DM.numDomainID,
        DM.bitLeadBoxFlg,
        DM.numTerID,
        DM.numStatusID,
        DM.bintLeadProm,
        DM.bintLeadPromBy,
        DM.bintProsProm,
        DM.bintProsPromBy,
        DM.numRecOwner,
        DM.decTaxPercentage,
        DM.tintBillingTerms,
        DM.numBillingDays,
        DM.tintInterestType,
        DM.fltInterest,
        DM.vcComPhone,
        DM.vcComFax,
        DM.numCampaignID,
        DM.numAssignedBy,
        DM.numAssignedTo,
        DM.bitNoTax,
        vcCompanyName + ' ,<br>' + ISNULL(AD2.vcStreet,'') + ' <br>'
        + ISNULL(AD2.VcCity,'') + ' ,' + ISNULL(dbo.fn_GetState(AD2.numState),
                                        '') + ' ' + ISNULL(AD2.vcPostalCode,'')
        + ' <br>' + ISNULL(dbo.fn_GetListItemName(AD2.numCountry),
                            '') AS OrgShippingAddress ,								   
		(select U.txtSignature from UserMaster U where U.numUserDetailId=ACI.numRecOwner) as [Signature]
    FROM    
		BroadCastDtls
	INNER JOIN 
		dbo.AdditionalContactsInformation ACI 
	ON
		ACI.numContactId = BroadCastDtls.numContactID
	INNER JOIN
		dbo.DivisionMaster DM 
	ON
		ACI.numDivisionId = DM.numDivisionID
	INNER JOIN
		dbo.CompanyInfo C
	ON 
		DM.numCompanyId = C.numCompanyId
    LEFT JOIN 
		dbo.AddressDetails AD2 
	ON 
		AD2.numDomainID=DM.numDomainID 
		AND AD2.numRecordID= DM.numDivisionID 
		AND AD2.tintAddressOf=2 
		AND AD2.tintAddressType=2 
		AND AD2.bitIsPrimary=1
    WHERE   
		DM.numDomainId = @numDomainID AND
		BroadCastDtls.numBroadCastID = @numBroadCastID AND
		ACI.bitOptOut = 0

END  

