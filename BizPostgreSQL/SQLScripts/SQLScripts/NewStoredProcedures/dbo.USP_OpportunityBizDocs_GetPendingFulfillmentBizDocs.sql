SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON            
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_OpportunityBizDocs_GetPendingFulfillmentBizDocs')
DROP PROCEDURE USP_OpportunityBizDocs_GetPendingFulfillmentBizDocs
GO
CREATE PROCEDURE [dbo].[USP_OpportunityBizDocs_GetPendingFulfillmentBizDocs]          
@numOppID AS NUMERIC(18,0)
AS             
BEGIN
	SELECT numOppBizDocsId FROM OpportunityBizDocs WHERE numOppId=@numOppID AND numBizDocId=296 AND ISNULL(bitFulFilled,0) = 0
END



