GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetPrimaryListItemID')
DROP PROCEDURE USP_GetPrimaryListItemID 
GO
--exec USP_GetPrimaryListItemID 1,1,1593,5,7074 
CREATE PROCEDURE USP_GetPrimaryListItemID 
@numDomainID NUMERIC,
@tintMode TINYINT=0,
@numFormFieldId NUMERIC,
@numPrimaryListID NUMERIC,
@numRecordID NUMERIC,
@bitCustomField bit
AS 
BEGIN
	DECLARE @fld_id NUMERIC 
	DECLARE @numFormID NUMERIC
	DECLARE @vcDbColumnName VARCHAR(100)
	DECLARE @vcLookBackTableName VARCHAR(100)
	DECLARE @numCompanyID NUMERIC
	DECLARE @strSQL VARCHAR(4000)

	IF @tintMode=1 OR @tintMode=5 -- 1 for organization, 5 for contact
	BEGIN
	
		-- when primary list is custom field and secondary is regular(non-custom) field
			IF (SELECT ISNULL(numModuleID,0) FROM dbo.ListMaster WHERE numListID= @numPrimaryListID) = 8
			BEGIN
				DECLARE @strTemp VARCHAR(100)
				IF @tintMode =1 SET @strTemp ='CFW_FLD_Values'
				ELSE IF @tintMode =5 SET @strTemp ='CFW_FLD_Values_Cont'
			
				SELECT @fld_id = Fld_id FROM dbo.CFW_Fld_Master WHERE numlistid=@numPrimaryListID AND numDomainID= @numDomainID
				SET @strSQL = 'SELECT top 1 Fld_Value FROM dbo.'+ @strTemp + ' WHERE RecId= ' + CONVERT(VARCHAR(15),@numRecordID) + ' and fld_id = ' + CONVERT(VARCHAR(15),@fld_id)
			END
			ELSE
			BEGIN
				IF @numPrimaryListID = 834 -- 834: Shipping Zone
				BEGIN
					SELECT numShippingZone FROM dbo.AddressDetails AD2 INNER JOIN [State] ON Ad2.numState=[State].numStateID WHERE AD2.numDomainID=@numDomainID AND AD2.numRecordID= @numRecordID AND AD2.tintAddressOf=2 AND AD2.tintAddressType=2 AND AD2.bitIsPrimary=1
				END
				ELSE
				BEGIN
					SELECT @numFormID =numFormID FROM View_DynamicDefaultColumns WHERE numFieldId=@numFormFieldId AND numDomainID= @numDomainID
				
					SELECT TOP 1 @vcDbColumnName=vcDbColumnName,@vcLookBackTableName=vcLookBackTableName FROM View_DynamicDefaultColumns WHERE
					numFormID=@numFormID AND numListID= @numPrimaryListID AND numDomainID= @numDomainID
				
					IF @vcLookBackTableName = 'CompanyInfo'
					BEGIN
						SELECT @numCompanyID= numCompanyId FROM dbo.DivisionMaster WHERE numDivisionID=@numRecordID AND numDomainID = @numDomainID
						SET @strSQL = 'SELECT '+ @vcDbColumnName +  '  FROM dbo.CompanyInfo WHERE numDomainID= '+ CONVERT(VARCHAR(10),@numDomainID) +' and numCompanyId = ' + CONVERT(VARCHAR(15),@numCompanyID)
					END
					ELSE IF @vcLookBackTableName = 'DivisionMaster'
					BEGIN
						SET @strSQL = 'SELECT '+ @vcDbColumnName +  '  FROM dbo.DivisionMaster WHERE numDomainID= '+ CONVERT(VARCHAR(10),@numDomainID) +' and numDivisionID = ' + CONVERT(VARCHAR(15),@numRecordID)
					END
					ELSE IF @vcLookBackTableName = 'AdditionalContactsInformation'
					BEGIN
						SET @strSQL = 'SELECT '+ @vcDbColumnName +  '  FROM dbo.AdditionalContactsInformation WHERE numDomainID= '+ CONVERT(VARCHAR(10),@numDomainID) +' and numContactId = ' + CONVERT(VARCHAR(15),@numRecordID)
					END
					ELSE
						SELECT 0
				END
			END 
		
		EXEC(@strSQL);
	END		
	IF @tintMode=2 -- Project
	BEGIN
			-- when primary list is custom field and secondary is regular(non-custom) field
			IF (SELECT ISNULL(numModuleID,0) FROM dbo.ListMaster WHERE numListID= @numPrimaryListID) = 8
			BEGIN
				
				
				SELECT @fld_id = Fld_id FROM dbo.CFW_Fld_Master WHERE numlistid=@numPrimaryListID AND numDomainID= @numDomainID
				SET @strSQL = 'SELECT top 1 Fld_Value FROM dbo.CFW_FLD_Values_Pro WHERE RecId= ' + CONVERT(VARCHAR(15),@numRecordID) + ' and fld_id = ' + CONVERT(VARCHAR(15),@fld_id)
			END
			ELSE 
			BEGIN
				SELECT TOP 1 @vcDbColumnName=vcDbColumnName,@vcLookBackTableName=vcLookBackTableName FROM View_DynamicDefaultColumns WHERE
				numFormID=13 AND numListID= @numPrimaryListID AND numDomainID= @numDomainID
			
				SET @strSQL = 'SELECT '+ @vcDbColumnName +  '  FROM dbo.ProjectsMaster WHERE numDomainID= '+ CONVERT(VARCHAR(10),@numDomainID) +' and numProId = ' + CONVERT(VARCHAR(15),@numRecordID)
				
			END
			
			EXEC(@strSQL);
	END
	IF @tintMode=3 -- Opportunity
	BEGIN
		-- when primary list is custom field and secondary is regular(non-custom) field
			IF (SELECT ISNULL(numModuleID,0) FROM dbo.ListMaster WHERE numListID= @numPrimaryListID) = 8
			BEGIN
				
				SELECT @fld_id = Fld_id FROM dbo.CFW_Fld_Master WHERE numlistid=@numPrimaryListID AND numDomainID= @numDomainID
				SET @strSQL = 'SELECT top 1 Fld_Value FROM dbo.CFW_Fld_Values_Opp WHERE RecId= ' + CONVERT(VARCHAR(15),@numRecordID) + ' and fld_id = ' + CONVERT(VARCHAR(15),@fld_id)
			END
			ELSE 
			BEGIN
				SELECT @numFormID =numFormID FROM View_DynamicDefaultColumns WHERE numFieldId=@numFormFieldId AND numDomainID= @numDomainID
		
				SELECT TOP 1 @vcDbColumnName=vcDbColumnName,@vcLookBackTableName=vcLookBackTableName FROM View_DynamicDefaultColumns WHERE
				numFormID=@numFormID AND numListID= @numPrimaryListID AND numDomainID= @numDomainID
			
				SET @strSQL = 'SELECT '+ @vcDbColumnName +  '  FROM dbo.OpportunityMaster WHERE numDomainID= '+ CONVERT(VARCHAR(10),@numDomainID) +' and numOppID = ' + CONVERT(VARCHAR(15),@numRecordID)
				
			END
			EXEC(@strSQL);
		
	END
	IF @tintMode=4 -- Cases
	BEGIN
			-- when primary list is custom field and secondary is regular(non-custom) field
			IF (SELECT ISNULL(numModuleID,0) FROM dbo.ListMaster WHERE numListID= @numPrimaryListID) = 8
			BEGIN
				SELECT @fld_id = Fld_id FROM dbo.CFW_Fld_Master WHERE numlistid=@numPrimaryListID AND numDomainID= @numDomainID
				SET @strSQL = 'SELECT top 1 Fld_Value FROM dbo.CFW_FLD_Values_Case WHERE RecId= ' + CONVERT(VARCHAR(15),@numRecordID) + ' and fld_id = ' + CONVERT(VARCHAR(15),@fld_id)
			END
			ELSE 
			BEGIN
				SELECT TOP 1 @vcDbColumnName=vcDbColumnName,@vcLookBackTableName=vcLookBackTableName FROM View_DynamicDefaultColumns WHERE
				numFormID=12 AND numListID= @numPrimaryListID AND numDomainID= @numDomainID
			
				SET @strSQL = 'SELECT '+ @vcDbColumnName +  '  FROM dbo.Cases WHERE numDomainID= '+ CONVERT(VARCHAR(10),@numDomainID) +' and numCaseId = ' + CONVERT(VARCHAR(15),@numRecordID)
			END
			EXEC(@strSQL);
	END

		
	
END
