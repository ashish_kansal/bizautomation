GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ManageCommunicationAttendees')
DROP PROCEDURE USP_ManageCommunicationAttendees
GO
CREATE PROCEDURE [dbo].[USP_ManageCommunicationAttendees]
@numCommId AS numeric(9)=0,
@numContactId  AS numeric(9)=0,
@type as int=0
AS
BEGIN
	IF(@type=0)
	BEGIN
		IF EXISTS(SELECT * FROM CommunicationAttendees WHERE numCommId=@numCommId AND numContactId=@numContactId)
		BEGIN
			RAISERROR('Contact already exist for this Activity',16,1)
		END
		ELSE
		BEGIN
		INSERT INTO CommunicationAttendees(numCommId,numContactId)
		VALUES(@numCommId,@numContactId)
		END
	END
	ELSE IF(@type=1)
	BEGIN
		DELETE FROM CommunicationAttendees WHERE numCommId=@numCommId AND numContactId=@numContactId
	END
END