
GO
/****** Object:  StoredProcedure [dbo].[USP_GetShippingStateAbbreviation]    Script Date: 05/07/2009 17:35:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetShippingAbbreviation')
DROP PROCEDURE USP_GetShippingAbbreviation
GO
CREATE PROCEDURE [dbo].[USP_GetShippingAbbreviation]
@numShipFromState as numeric(9),
@numShipFromCountry  as numeric(9),
@numShipToState  as numeric(9),
@numShipToCountry  as numeric(9),
@vcFromState  as varchar(5)='' output,
@vcFromCountry  as varchar(5)='' output,
@vcToState  as varchar(5)='' output,
@vcToCountry  as varchar(5)='' output,
@numShipCompany  as numeric(9)

AS 


select @vcFromState=vcStateCode from ShippingStateMaster 
where vcStateName=(select top 1 vcState from [State] where numStateID=@numShipFromState) and  numShipCompany=@numShipCompany

select @vcFromCountry=vcCountryCode from ShippingCountryMaster 
where vcCountryName=(select top 1 vcData from ListDetails where numListItemID=@numShipFromCountry)  and  numShipCompany=@numShipCompany

select @vcToState=vcStateCode from ShippingStateMaster 
where vcStateName=(select top 1 vcState from [State] where numStateID=@numShipToState)  and  numShipCompany=@numShipCompany

select @vcToCountry=vcCountryCode from ShippingCountryMaster 
where vcCountryName=(select top 1 vcData from ListDetails where numListItemID=@numShipToCountry)  and  numShipCompany=@numShipCompany


