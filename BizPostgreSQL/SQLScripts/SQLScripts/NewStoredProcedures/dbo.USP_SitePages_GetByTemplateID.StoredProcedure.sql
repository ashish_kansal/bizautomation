SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_SitePages_GetByTemplateID')
DROP PROCEDURE dbo.USP_SitePages_GetByTemplateID
GO
CREATE PROCEDURE [dbo].[USP_SitePages_GetByTemplateID]
	@numDomainID NUMERIC (18,0),
	@numTemplateID  NUMERIC (18,0),
	@numSiteID NUMERIC(18,0)
AS 
BEGIN

	DECLARE @vcTemplateCode VARCHAR(500)
	SELECT @vcTemplateCode='{template:' + REPLACE(LOWER([vcTemplateName]),' ','_') + '}' FROM SiteTemplates WHERE numTemplateID = @numTemplateID AND numDomainID = @numDomainID


	SELECT 
		* 
	FROM 
		SitePages 
	WHERE 
		numTemplateID IN (
							SELECT 
								numTemplateID 
							FROM 
								SiteTemplates 
							WHERE 
								numDomainID = @numDomainID AND
								numSiteID = @numSiteID AND 
								CONTAINS(txtTemplateHTML,@vcTemplateCode)
							)
	
END



