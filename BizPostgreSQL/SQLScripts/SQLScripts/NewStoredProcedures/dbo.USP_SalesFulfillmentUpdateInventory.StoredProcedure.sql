GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_SalesFulfillmentUpdateInventory')
DROP PROCEDURE USP_SalesFulfillmentUpdateInventory
GO
CREATE PROCEDURE  USP_SalesFulfillmentUpdateInventory
    @numDomainID NUMERIC(9),
    @numOppID NUMERIC(9),
    @numOppBizDocsId NUMERIC(9),
    @numUserCntID NUMERIC(9),
    @tintMode TINYINT --1:Release 2:Revert
  AS 
BEGIN
BEGIN TRY
BEGIN TRANSACTION      
		DECLARE @numDomain AS NUMERIC(9)
		SET @numDomain = @numDomainID
        
		DECLARE @onAllocation AS FLOAT
		DECLARE @numWarehouseItemID AS NUMERIC
		DECLARE @numOppItemID AS NUMERIC
		DECLARE @numUnits FLOAT    
        DECLARE @numOldQtyShipped AS FLOAT
		DECLARE @numNewQtyShipped AS FLOAT
		      
        DECLARE @Kit AS BIT,@vcItemName AS VARCHAR(300)                   
        DECLARE @vcError VARCHAR(500) 
		DECLARE @description AS VARCHAR(100)
         
        DECLARE @minRowNumber NUMERIC(9),@maxRowNumber NUMERIC(9)
         
        DECLARE @minKitRowNumber NUMERIC(9)
		DECLARE @maxKitRowNumber NUMERIC(9)
		DECLARE @KitWareHouseItemID NUMERIC(9)
		DECLARE @KitonAllocation AS FLOAT
		DECLARE @KitQtyShipped AS FLOAT
		DECLARE @KiyQtyItemsReq_Orig AS FLOAT
		DECLARE @KiyQtyItemsReq as FLOAT  
		DECLARE @Kitdescription AS VARCHAR(100)
		DECLARE @KitNewQtyShipped AS FLOAT
		DECLARE @KitOppChildItemID AS NUMERIC(9)
						
        SELECT 
			OBDI.numOppItemID,
			ISNULL(OBDI.numWarehouseItmsID, 0) AS numWarehouseItemID,
			ISNULL(OI.numQtyShipped,0) AS numQtyShipped,
			OBDI.numUnitHour,
			I.vcItemName,
			(CASE WHEN bitKitParent=1 AND bitAssembly=1 THEN 0 WHEN bitKitParent=1 THEN 1 ELSE 0 END) AS KIT,
			ROW_NUMBER() OVER(ORDER BY OBDI.numOppItemID) AS RowNumber 
		INTO 
			#tempItems
        FROM  
			dbo.OpportunityBizDocs OBD 
		JOIN 
			dbo.OpportunityBizDocItems OBDI 
		ON 
			OBD.numOppBizDocsId=OBDI.numOppBizDocID
		JOIN 
			OpportunityItems OI 
		ON 
			OBDI.numOppItemID=OI.numoppitemtCode 
		JOIN 
			Item I 
		ON 
			OI.numItemCode=I.numItemCode
        WHERE 
			OBD.numOppID=@numOppID 
			AND OBD.numOppBizDocsId=@numOppBizDocsId 
			AND OI.numOppID=@numOppID 
        	AND (I.charitemtype='P' OR 1=(CASE WHEN ISNULL(I.bitKitParent,0) = 1 AND ISNULL(I.bitAssembly,0) = 1 THEN 0
											WHEN ISNULL(I.bitKitParent,0) = 1 THEN 1 ELSE 0 END))
			 and (OI.bitDropShip=0 or OI.bitDropShip is null) AND ISNULL(OI.bitWorkOrder,0)=0                                   
											
        SELECT  
			@minRowNumber = MIN(RowNumber),
			@maxRowNumber = MAX(RowNumber) 
		FROM 
			#tempItems
        
        
        WHILE  @minRowNumber <= @maxRowNumber
		BEGIN
			SELECT 
				@numWarehouseItemID=numWarehouseItemID,
				@numOppItemID=numOppItemID,
				@numOldQtyShipped=ISNULL(numQtyShipped,0),
				@numUnits=numUnitHour,
				@Kit=Kit 
			FROM 
				#tempItems 
			WHERE 
				RowNumber=@minRowNumber
			
			IF @tintMode=1 --1:Release
			BEGIN
				SET @numNewQtyShipped = @numUnits-@numOldQtyShipped;
					  		
				IF @numNewQtyShipped>0
				BEGIN
					SET @description=CONCAT('SO Qty Shipped (Qty:',@numUnits,' Shipped:',@numNewQtyShipped,')')
						 
					IF @Kit=1
					BEGIN
						SELECT 
							OKI.numOppChildItemID,
							OKI.numWareHouseItemId,
							numQtyItemsReq,
							numQtyItemsReq_Orig,
							numQtyShipped,
							ISNULL(numAllocation, 0) AS numAllocation,
							ROW_NUMBER() OVER(ORDER BY numOppChildItemID) AS RowNumber 
						INTO 
							#tempKits
						FROM 
							OpportunityKitItems OKI 
						JOIN 
							Item I 
						ON 
							OKI.numChildItemID=I.numItemCode  
						JOIN 
							WareHouseItems WI 
						ON 
							WI.numWareHouseItemID=OKI.numWareHouseItemID  
						WHERE 
							charitemtype='P' 
							AND OKI.numWareHouseItemId > 0 
							AND OKI.numWareHouseItemId IS NOT NULL
							AND numOppItemID=@numOppItemID 
					
						IF(SELECT COUNT(*) FROM #tempKits WHERE numAllocation - ((@numUnits * numQtyItemsReq_Orig) - numQtyShipped) < 0)>0
						BEGIN
							SET @vcError ='You do not have enough KIT inventory to support this shipment('+ @vcItemName +'). Modify your Qty-Shipped to an amount equal to or less than the Qty-On-Allocation and try again.' 
								
							RAISERROR ('NotSufficientQty_Allocation',16,1);
						END
						ELSE
						BEGIN
							SELECT  
								@minKitRowNumber = MIN(RowNumber),
								@maxKitRowNumber = MAX(RowNumber) 
							FROM 
								#tempKits

							WHILE @minKitRowNumber <= @maxKitRowNumber
							BEGIN
								SELECT 
									@KitOppChildItemID=numOppChildItemID,
									@KitWareHouseItemID=numWareHouseItemID,
									@KiyQtyItemsReq=numQtyItemsReq,
									@KitQtyShipped=numQtyShipped,
									@KiyQtyItemsReq_Orig=numQtyItemsReq_Orig,
									@KitonAllocation=numAllocation 
								FROM 
									#tempKits 
								WHERE 
									RowNumber=@minKitRowNumber
						
								SET @KitNewQtyShipped = (@numNewQtyShipped * @KiyQtyItemsReq_Orig) - @KitQtyShipped;
						
								SET @Kitdescription=CONCAT('SO KIT Qty Shipped (Qty:',@KiyQtyItemsReq,' Shipped:',@KitNewQtyShipped,')')

								SET @KitonAllocation = @KitonAllocation - @KitNewQtyShipped
			        
								IF (@KitonAllocation >= 0 )
								BEGIN
									UPDATE
										WareHouseItems
									SET
										numAllocation = @KitonAllocation,
										dtModified = GETDATE() 
									WHERE 
										numWareHouseItemID = @KitWareHouseItemID      	
									
									UPDATE  
										[OpportunityKitItems]
									SET 
										[numQtyShipped] = @numUnits * @KiyQtyItemsReq_Orig 
									WHERE 
										[numOppChildItemID] = @KitOppChildItemID
						                
									EXEC dbo.USP_ManageWareHouseItems_Tracking
											@numWareHouseItemID = @KitWareHouseItemID, --  numeric(9, 0)
											@numReferenceID = @numOppId, --  numeric(9, 0)
											@tintRefType = 3, --  tinyint
											@vcDescription = @Kitdescription, --  varchar(100)
											@numModifiedBy = @numUserCntID,
											@numDomainID = @numDomain 
								END
			            
								SET @minKitRowNumber=@minKitRowNumber + 1			 		
							END				
						   
							UPDATE  
								[OpportunityItems]
							SET 
								[numQtyShipped] = @numUnits
							WHERE 
								[numoppitemtCode] = @numOppItemID
				                
							EXEC dbo.USP_ManageWareHouseItems_Tracking
									@numWareHouseItemID = @numWareHouseItemID, --  numeric(9, 0)
									@numReferenceID = @numOppId, --  numeric(9, 0)
									@tintRefType = 3, --  tinyint
									@vcDescription = @description, --  varchar(100)
									@numModifiedBy = @numUserCntID,
									@numDomainID = @numDomain
						 END
					END
					ELSE
					BEGIN
						SELECT
							@onAllocation = ISNULL(numAllocation, 0) 
						FROM 
							WareHouseItems 
						WHERE 
							numWareHouseItemID = @numWareHouseItemID
				        
						SET @onAllocation = @onAllocation - @numNewQtyShipped	
				        
						IF (@onAllocation >= 0 )
						BEGIN
							UPDATE  
								WareHouseItems
							SET 
								numAllocation = @onAllocation,dtModified = GETDATE() 
							WHERE
								numWareHouseItemID = @numWareHouseItemID      	
							
							UPDATE  
								[OpportunityItems]
							SET 
								[numQtyShipped] = @numUnits
							WHERE
								[numoppitemtCode] = @numOppItemID AND numOppId=@numOppId
				                
							EXEC dbo.USP_ManageWareHouseItems_Tracking
									@numWareHouseItemID = @numWareHouseItemID, --  numeric(9, 0)
									@numReferenceID = @numOppId, --  numeric(9, 0)
									@tintRefType = 3, --  tinyint
									@vcDescription = @description, --  varchar(100)
									@numModifiedBy = @numUserCntID,
									@numDomainID = @numDomain 
						END
						ELSE
						BEGIN
							SET @vcError ='You do not have enough inventory to support this shipment('+ @vcItemName +'). Modify your Qty-Shipped to an amount equal to or less than the Qty-On-Allocation and try again.' 
							RAISERROR ('NotSufficientQty_Allocation',16,1);
						END
					END
				END	 
			END
			ELSE IF @tintMode=2 --2:Revert
			BEGIN
				DECLARE @numAuthInvoice NUMERIC(18,0)
				
				SELECT 
					@numAuthInvoice=ISNULL(numAuthoritativeSales,0) 
				FROM 
					dbo.AuthoritativeBizDocs 
				WHERE 
					AuthoritativeBizDocs.numDomainId=@numDomainId 
				
				IF EXISTS (SELECT numOppBizDocsId FROM OpportunityMaster OM JOIN OpportunityBizDocs OBD ON OM.numOppID=OBD.numOppID
					WHERE OM.numDomainId=@numDomainId AND OM.numOppID=@numOppID  AND OBD.numBizDocId IN (@numAuthInvoice))
				BEGIN
					RAISERROR ('AlreadyInvoice_DoNotReturn',16,1);
					RETURN -1
				END		
						
				IF @numOldQtyShipped>0
				BEGIN
					SET @description=CONCAT('SO Qty Shipped Return (Qty:',@numUnits,' Shipped Return:',@numOldQtyShipped,')')
					
					IF @Kit=1
					BEGIN
						SELECT 
							OKI.numOppChildItemID,
							OKI.numWareHouseItemId,
							numQtyItemsReq,
							numQtyItemsReq_Orig,
							numQtyShipped,
							ISNULL(numAllocation, 0) AS numAllocation,
							ROW_NUMBER() OVER(ORDER BY numOppChildItemID) AS RowNumber 
						INTO 
							#tempKitsReturn
						FROM 
							OpportunityKitItems OKI 
						JOIN 
							Item I 
						ON 
							OKI.numChildItemID=I.numItemCode  
						JOIN 
							WareHouseItems WI 
						ON 
							WI.numWareHouseItemID=OKI.numWareHouseItemID  
						WHERE 
							charitemtype='P' 
							AND OKI.numWareHouseItemId > 0 
							AND OKI.numWareHouseItemId IS NOT NULL 
							AND numOppItemID=@numOppItemID 
					
						SELECT  
							@minKitRowNumber = MIN(RowNumber),
							@maxKitRowNumber = MAX(RowNumber) 
						FROM 
							#tempKitsReturn

						WHILE  @minKitRowNumber <= @maxKitRowNumber
						BEGIN
							SELECT 
								@KitOppChildItemID=numOppChildItemID,
								@KitWareHouseItemID=numWareHouseItemID,
								@KiyQtyItemsReq=numQtyItemsReq,
								@KitQtyShipped=numQtyShipped,
								@KiyQtyItemsReq_Orig=numQtyItemsReq_Orig,
								@KitonAllocation=numAllocation 
							FROM 
								#tempKits 
							WHERE 
								RowNumber=@minKitRowNumber
						
							SET @KitNewQtyShipped = (@numOldQtyShipped * @KiyQtyItemsReq_Orig);
						
							SET @Kitdescription=CONCAT('SO KIT Qty Shipped Return (Qty:',@KiyQtyItemsReq,' Shipped:',@KitNewQtyShipped,')')

							SET @KitonAllocation = @KitonAllocation + @KitNewQtyShipped
			        
							UPDATE  
								WareHouseItems
							SET 
								numAllocation = @KitonAllocation,
								dtModified = GETDATE() 
							WHERE
								numWareHouseItemID = @KitWareHouseItemID      	
									
							UPDATE 
								[OpportunityKitItems]
							SET 
								[numQtyShipped] = 0
							WHERE 
								[numOppChildItemID] = @KitOppChildItemID
						                
							EXEC dbo.USP_ManageWareHouseItems_Tracking
									@numWareHouseItemID = @KitWareHouseItemID, --  numeric(9, 0)
									@numReferenceID = @numOppId, --  numeric(9, 0)
									@tintRefType = 3, --  tinyint
									@vcDescription = @Kitdescription, --  varchar(100)
									@numModifiedBy = @numUserCntID,
									@numDomainID = @numDomain 
			            
							SET @minKitRowNumber=@minKitRowNumber + 1			 		
						END				
						   
						UPDATE  
							[OpportunityItems]
						SET 
							[numQtyShipped] = @numUnits
						WHERE 
							[numoppitemtCode] = @numOppItemID
				                
						EXEC dbo.USP_ManageWareHouseItems_Tracking
								@numWareHouseItemID = @numWareHouseItemID, --  numeric(9, 0)
								@numReferenceID = @numOppId, --  numeric(9, 0)
								@tintRefType = 3, --  tinyint
								@vcDescription = @description, --  varchar(100)
								@numModifiedBy = @numUserCntID,
								@numDomainID = @numDomain 
					END
					ELSE
					BEGIN
						 SELECT @onAllocation = ISNULL(numAllocation, 0) FROM    WareHouseItems WHERE   numWareHouseItemID = @numWareHouseItemID
				        
						 SET @onAllocation = @onAllocation + @numOldQtyShipped	
				        
						IF (@onAllocation >= 0 )
						BEGIN
							UPDATE  
								WareHouseItems
							SET
								numAllocation = @onAllocation,
								dtModified = GETDATE() 
							WHERE 
								numWareHouseItemID = @numWareHouseItemID      	
							
							UPDATE
								[OpportunityItems]
							SET 
								[numQtyShipped] = 0
							WHERE 
								[numoppitemtCode] = @numOppItemID 
								AND numOppId=@numOppId
				                
							EXEC dbo.USP_ManageWareHouseItems_Tracking
									@numWareHouseItemID = @numWareHouseItemID, --  numeric(9, 0)
									@numReferenceID = @numOppId, --  numeric(9, 0)
									@tintRefType = 3, --  tinyint
									@vcDescription = @description, --  varchar(100)
									@numModifiedBy = @numUserCntID,
									@numDomainID = @numDomain 
						END
					END 			
				END
				ELSE
				BEGIN
					SET @vcError ='You have not shipped qty for ('+ @vcItemName +').' 
					RAISERROR ('NoQty_ForShipped',16,1);
				END
			END
				  		
			SET @minRowNumber=@minRowNumber + 1			 		
		END	

COMMIT
END TRY
BEGIN CATCH
  -- Whoops, there was an error
  IF @@TRANCOUNT > 0
     ROLLBACK

  -- Raise an error with the details of the exception
  DECLARE @ErrMsg nvarchar(4000), @ErrSeverity int
  SELECT @ErrMsg = ERROR_MESSAGE(),
         @ErrSeverity = ERROR_SEVERITY()

  RAISERROR(@ErrMsg, @ErrSeverity, 1)
END CATCH
END
GO
    