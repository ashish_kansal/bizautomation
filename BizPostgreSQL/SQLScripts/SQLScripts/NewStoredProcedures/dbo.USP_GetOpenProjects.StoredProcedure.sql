GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetOpenProjects')
DROP PROCEDURE USP_GetOpenProjects
GO
CREATE PROCEDURE USP_GetOpenProjects
    @numDomainID NUMERIC(9),
    @numProId NUMERIC(9),
    @tintMode TINYINT = 0,
    @numDivisionID NUMERIC(9)=0
AS 
    BEGIN
    
    IF @tintMode = 0 
    BEGIN
		IF @numProId > 0 
            BEGIN
                SELECT  numAccountID
                FROM    [ProjectsMaster]
                WHERE   [numProId] = @numProId
                        AND [numDomainId] = @numDomainID
            END
        ELSE 
            SELECT  [numProId],
                    [vcProjectName]
            FROM    [ProjectsMaster]
            WHERE   [numDomainId] = @numDomainID
                    AND [tintProStatus] <> 1
                    AND ([numDivisionId]=@numDivisionID OR @numDivisionID=0)
					AND isnull(numProjectStatus,0) <> 27492
            ORDER BY [vcProjectName],[numProId] desc
                    
	END
	ELSE IF @tintMode = 1
	BEGIN
		  SELECT  [numProId],
                    [vcProjectName]
            FROM    [ProjectsMaster]
            WHERE   [numDomainId] = @numDomainID
            ORDER BY [numProId] desc
            
                    
	END
        
    END 
