/****** Object:  StoredProcedure [dbo].[USP_GetPageElementAttributes]    Script Date: 08/08/2009 16:15:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--EXEC [USP_GetPageElementAttributes] @numSiteID = 3,@numElementID =4
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_StagePercentageDetails_CT')
DROP PROCEDURE USP_StagePercentageDetails_CT
GO
-- =============================================
-- Author:		<Author,,Sachin Sadhu>
-- Create date: <Create Date,, 18thMay2014>
-- Description:	<Description,,Get Changes in Stage Percentage details.used in WFA>
-- =============================================
Create PROCEDURE [dbo].[USP_StagePercentageDetails_CT]
	-- Add the parameters for the stored procedure here
    @numDomainID numeric(18, 0)=0,
    @numUserCntID numeric(18, 0)=0,
    @numRecordID numeric(18, 0)=0 
AS
BEGIN
-- SET NOCOUNT ON added to prevent extra result sets from
-- interfering with SELECT statements.
SET NOCOUNT ON;
 -- Insert statements for procedure here
 declare @tblFields as table (

 FieldName varchar(200),
 FieldValue varchar(200)
)

Declare @Type char(1)
Declare @ChangeVersion bigint
Declare @CreationVersion bigint
Declare @last_synchronization_version bigInt=null

SELECT 			
@ChangeVersion=Ct.Sys_Change_version,@CreationVersion=Ct.Sys_Change_Creation_version
FROM 
CHANGETABLE(CHANGES dbo.StagePercentageDetails, 0) AS CT
WHERE numStageDetailsId=@numRecordID

IF (@ChangeVersion=@CreationVersion)--Insert
Begin
        SELECT 
		@Type = CT.SYS_CHANGE_OPERATION
		FROM 
		CHANGETABLE(CHANGES dbo.StagePercentageDetails, @last_synchronization_version) AS CT
		WHERE  CT.numStageDetailsId=@numRecordID

End

Else
Begin

SET @last_synchronization_version=@ChangeVersion-1     
	
	  SELECT 
		@Type = CT.SYS_CHANGE_OPERATION
		FROM 
		CHANGETABLE(CHANGES dbo.StagePercentageDetails, @last_synchronization_version) AS CT
		WHERE  CT.numStageDetailsId=@numRecordID


		--Fields Update & for Type :Insert or Update:
			Declare @UFFields as table 
			(
			   	numStageDetailsId varchar(70),
				dtStartDate varchar(70),
				dtEndDate varchar(70),
				tintPercentage varchar(70),
				tinProgressPercentage varchar(70),
				numAssignTo varchar(70),
				intDueDays varchar(70),
				numModifiedBy varchar(70),
				numCreatedBy varchar(70)

			)


	INSERT into @UFFields(numStageDetailsId,dtStartDate,dtEndDate,tintPercentage,tinProgressPercentage,numAssignTo,intDueDays,numModifiedBy,numCreatedBy)
		SELECT
		CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.StagePercentageDetails'), 'numStageDetailsId', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS numStageDetailsId ,
		CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.StagePercentageDetails'), 'dtStartDate', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS dtStartDate, 
		CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.StagePercentageDetails'), 'dtEndDate', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS dtEndDate, 
		CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.StagePercentageDetails'), 'tintPercentage', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS tintPercentage, 
		CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.StagePercentageDetails'), 'tinProgressPercentage', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS tinProgressPercentage	,
		CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.StagePercentageDetails'), 'numAssignTo', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS  numAssignTo ,
		CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.StagePercentageDetails'), 'intDueDays', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS intDueDays,
		CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.StagePercentageDetails'), 'numModifiedBy', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS numModifiedBy, 
		CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.StagePercentageDetails'), 'numCreatedBy', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS numCreatedBy  
		FROM 
		CHANGETABLE(CHANGES dbo.StagePercentageDetails, @last_synchronization_version) AS CT
		WHERE 
		ct.numStageDetailsId=@numRecordID

		--sp_helptext USP_GetWorkFlowFormFieldMaster
	--exec USP_GetWorkFlowFormFieldMaster 1,90
INSERT INTO @tblFields(FieldName,FieldValue)
		SELECT
		FieldName,
		FieldValue
		FROM
			(
				SELECT numStageDetailsId,dtStartDate,dtEndDate,tintPercentage,tinProgressPercentage,numAssignTo,intDueDays,numModifiedBy,numCreatedBy
				FROM @UFFields	
			) AS UP
		UNPIVOT
		(
		FieldValue FOR FieldName IN (numStageDetailsId,dtStartDate,dtEndDate,tintPercentage,tinProgressPercentage,numAssignTo,intDueDays,numModifiedBy,numCreatedBy)
		) AS upv
		where FieldValue<>0
 

End

--select 'col1' from @UFFields where numCreatedBy='0' and numModifiedBy='0'
-- If Exists (select 'col1' from @UFFields where numCreatedBy='0' and numModifiedBy='0')
-- Begin
-- set @Type='I'
-- End
-- Else
-- Begin
-- set @Type='U'
-- End
-- select @Type
DECLARE @tintWFTriggerOn AS TINYINT
SET @tintWFTriggerOn=CASE @Type WHEN 'I' THEN 1 WHEN 'U' THEN 2 WHEN 'D' THEN 5 END

--DateField WorkFlow Logic
	
	Declare @TempnumStageDetailsId AS NUMERIC
	IF @tintWFTriggerOn = 1
	BEGIN		
		IF EXISTS (SELECT numStageDetailsId FROM StagePercentageDetails_TempDateFields WHERE numStageDetailsId=@numRecordID)
		BEGIN
			SET @TempnumStageDetailsId = (SELECT SP.numStageDetailsId FROM StagePercentageDetails SP WHERE  (			
				(SP.dtEndDate >= DATEADD(DAY,-30,GETUTCDATE()) AND SP.dtEndDate <= DATEADD(DAY,90,GETUTCDATE()) )
				OR	
				(SP.dtStartDate >= DATEADD(DAY,-30,GETUTCDATE()) AND SP.dtStartDate <= DATEADD(DAY,90,GETUTCDATE()) )
		    )		
			AND SP.numStageDetailsId=@numRecordID)

			IF(@TempnumStageDetailsId IS NOT NULL)
			BEGIN	
				UPDATE SPT 
				SET 
						SPT.dtEndDate  = SP.dtEndDate 
						,SPT.dtStartDate  =SP.dtStartDate 					
				FROM
					[StagePercentageDetails_TempDateFields] AS SPT
					INNER JOIN StagePercentageDetails AS SP
						ON SPT.numStageDetailsId = SP.numStageDetailsId
				WHERE  (
						(SP.dtEndDate >= DATEADD(DAY,-30,GETUTCDATE()) AND SP.dtEndDate <= DATEADD(DAY,90,GETUTCDATE()) )
						OR	
						(SP.dtStartDate >= DATEADD(DAY,-30,GETUTCDATE()) AND SP.dtStartDate <= DATEADD(DAY,90,GETUTCDATE()) )
						)			
					AND 
						SPT.numStageDetailsId = @numRecordID
			END
			ELSE
			BEGIN
				DELETE FROM StagePercentageDetails_TempDateFields WHERE numStageDetailsId = @numRecordID
			END	
		END
		ELSE
		BEGIN
			INSERT INTO [StagePercentageDetails_TempDateFields] (numStageDetailsId, numDomainID, dtEndDate, dtStartDate) 
			SELECT numStageDetailsId,numDomainID, dtEndDate, dtStartDate
			FROM StagePercentageDetails
			WHERE (
					(dtEndDate >= DATEADD(DAY,-30,GETUTCDATE()) AND dtEndDate <= DATEADD(DAY,90,GETUTCDATE()) )
					OR	
					(dtStartDate >= DATEADD(DAY,-30,GETUTCDATE()) AND dtStartDate <= DATEADD(DAY,90,GETUTCDATE()) )
				  )
				AND numStageDetailsId=@numRecordID
		END
	END
	ELSE IF @tintWFTriggerOn = 2
	BEGIN
		
		SET @TempnumStageDetailsId = (SELECT SP.numStageDetailsId FROM StagePercentageDetails SP WHERE  (			
				(SP.dtEndDate >= DATEADD(DAY,-30,GETUTCDATE()) AND SP.dtEndDate <= DATEADD(DAY,90,GETUTCDATE()) )
				OR	
				(SP.dtStartDate >= DATEADD(DAY,-30,GETUTCDATE()) AND SP.dtStartDate <= DATEADD(DAY,90,GETUTCDATE()) )
		    )		
			AND SP.numStageDetailsId=@numRecordID)

		IF(@TempnumStageDetailsId IS NOT NULL)
		BEGIN	
			UPDATE SPT 
			SET 
					SPT.dtEndDate  = SP.dtEndDate 
					,SPT.dtStartDate  =SP.dtStartDate 					
			FROM
				[StagePercentageDetails_TempDateFields] AS SPT
				INNER JOIN StagePercentageDetails AS SP
					ON SPT.numStageDetailsId = SP.numStageDetailsId
			WHERE  (
					(SP.dtEndDate >= DATEADD(DAY,-30,GETUTCDATE()) AND SP.dtEndDate <= DATEADD(DAY,90,GETUTCDATE()) )
					OR	
					(SP.dtStartDate >= DATEADD(DAY,-30,GETUTCDATE()) AND SP.dtStartDate <= DATEADD(DAY,90,GETUTCDATE()) )
					)			
				AND 
					SPT.numStageDetailsId = @numRecordID
		END
		ELSE
		BEGIN
			DELETE FROM StagePercentageDetails_TempDateFields WHERE numStageDetailsId = @numRecordID
		END	
	END
	ELSE IF @tintWFTriggerOn = 5
	BEGIN
		DELETE FROM StagePercentageDetails_TempDateFields WHERE numStageDetailsId = @numRecordID
	END	

	--DateField WorkFlow Logic


--For Fields Update Exection Point
DECLARE @Columns_Updated VARCHAR(1000)
SELECT @Columns_Updated = COALESCE(@Columns_Updated + ',', '') + FieldName FROM @tblFields 
SET @Columns_Updated=ISNULL(@Columns_Updated,'')

SELECT @Columns_Updated
select @tintWFTriggerOn

--Adding data As per Opertaion
	EXEC dbo.USP_ManageWorkFlowQueue
	@numWFQueueID = 0, --  numeric(18, 0)
	@numDomainID = @numDomainID, --  numeric(18, 0)
	@numUserCntID = @numUserCntID, --  numeric(18, 0)
	@numRecordID = @numRecordID, --  numeric(18, 0)
	@numFormID = 94, --  numeric(18, 0)
	@tintProcessStatus = 1, --  tinyint
	@tintWFTriggerOn = @tintWFTriggerOn, --  tinyint
	@tintMode = 1, --  tinyint
	@vcColumnsUpdated = @Columns_Updated

	
/***************************************************Commented SP for Checking RowVersion with Change Tracking:Date:23Apr||Sachin********************************************************/



--DECLARE @synchronization_version BIGINT 



--SET @synchronization_version = CHANGE_TRACKING_CURRENT_VERSION();











--SELECT 



--    CT.numOppID, CT.SYS_CHANGE_OPERATION, 



--    CT.SYS_CHANGE_COLUMNS, CT.SYS_CHANGE_CONTEXT, 



--    CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.OpportunityMaster'), 'numOppID', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS numOppID, 



--    CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.OpportunityMaster'), 'vcPOppName', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS vcPOppName 



--FROM 



--    CHANGETABLE(CHANGES dbo.OpportunityMaster, @synchronization_version) AS CT











--SET @synchronization_version = CHANGE_TRACKING_CURRENT_VERSION();



/***************************************************End of Comment||Sachin********************************************************/



END
	





