GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_DeleteShippingRuleStateList' ) 
    DROP PROCEDURE USP_DeleteShippingRuleStateList
GO
CREATE PROCEDURE USP_DeleteShippingRuleStateList
    @numDomainID NUMERIC,
    @numCountryID NUMERIC,
	@numStateID NUMERIC,
	@numRuleId NUMERIC
AS 
BEGIN
 	
		DELETE FROM ShippingRuleStateList 
		WHERE numDomainID = @numDomainID AND numCountryID = @numCountryID 
		AND numStateID = @numStateID AND numRuleID = @numRuleId
 	
END