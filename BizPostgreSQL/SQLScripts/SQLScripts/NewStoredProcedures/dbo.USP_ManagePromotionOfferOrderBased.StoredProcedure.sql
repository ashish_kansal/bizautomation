SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ManagePromotionOfferOrderBased')
DROP PROCEDURE USP_ManagePromotionOfferOrderBased
GO
CREATE PROCEDURE [dbo].[USP_ManagePromotionOfferOrderBased]
	@numProId AS NUMERIC(9) = 0,
	@vcProName AS VARCHAR(100),
	@numDomainId AS NUMERIC(18,0),
	@numUserCntID NUMERIC(18,0),
	@fltSubTotal FLOAT,
	@cSaleType CHAR,
	@byteMode TINYINT = 0,
	@fltOldSubTotal FLOAT = 0
	--@numProOrderBasedID AS NUMERIC(9) = 0
AS 
BEGIN

	IF @numProId = 0 
		BEGIN
			INSERT INTO PromotionOffer
			(
				vcProName, numDomainId, numCreatedBy, dtCreated, bitEnabled, IsOrderBasedPromotion
			)
			VALUES  
			(
				@vcProName, @numDomainId, @numUserCntID, GETUTCDATE(), 1, 1
			)
            
			SELECT SCOPE_IDENTITY()
		END
	ELSE IF @numProId > 0 
		BEGIN
			IF @byteMode = 0
			BEGIN			
				IF @cSaleType = 'I'
				BEGIN
					IF (SELECT COUNT(*) FROM PromotionOfferOrderBased WHERE numProId = @numProId AND fltSubTotal = @fltSubTotal) > 0
					BEGIN
						RAISERROR ( 'DUPLICATE_SUBTOTAL_AMOUNT',16, 1 )
 						RETURN;
					END

					IF (SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId = @numProId AND tintType = 1 AND tintRecordType = 5) > 0
					BEGIN
						IF ((SELECT COUNT(*) FROM PromotionOfferOrderBased WHERE numProId = @numProId AND numDomainId = @numDomainId AND cSaleType = 'I') > 0)
						BEGIN
							IF((SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId = @numProId AND tintType = 1 AND tintRecordType = 5) > 1)
							BEGIN
							
								DELETE FROM PromotionOfferOrderBased WHERE numProId = @numProId AND numDomainId = @numDomainId AND cSaleType = 'C'

								INSERT INTO PromotionOfferOrderBased
								(
									numProId, numProItemId, numDomainId, fltSubTotal, cSaleType, cItems, bitEnabled
								)
								SELECT numProId, numValue, @numDomainId, @fltSubTotal, @cSaleType, 'M', 1 FROM PromotionOfferItems WHERE numProId = @numProId AND tintType = 1 AND tintRecordType = 5
								AND numValue NOT IN(SELECT numProItemId FROM PromotionOfferOrderBased WHERE numProId = @numProId AND numDomainId = @numDomainId AND fltSubTotal = @fltSubTotal)
							END
							ELSE IF((SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId = @numProId AND tintType = 1 AND tintRecordType = 5) = 1)
							BEGIN

								DELETE FROM PromotionOfferOrderBased WHERE numProId = @numProId AND numDomainId = @numDomainId AND cSaleType = 'C'

								INSERT INTO PromotionOfferOrderBased
								(
									numProId, numProItemId, numDomainId, fltSubTotal, cSaleType, cItems, bitEnabled
								)
								SELECT numProId, numValue, @numDomainId, @fltSubTotal, @cSaleType, 'S', 1 FROM PromotionOfferItems WHERE numProId = @numProId AND tintType = 1 AND tintRecordType = 5
								AND numValue NOT IN(SELECT numProItemId FROM PromotionOfferOrderBased WHERE numProId = @numProId AND numDomainId = @numDomainId AND fltSubTotal = @fltSubTotal)
							END
						END
						ELSE
						BEGIN
							IF((SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId = @numProId AND tintType = 1 AND tintRecordType = 5) > 1)
							BEGIN

								DELETE FROM PromotionOfferOrderBased WHERE numProId = @numProId AND numDomainId = @numDomainId AND cSaleType = 'C'

								INSERT INTO PromotionOfferOrderBased
								(
									numProId, numProItemId, numDomainId, fltSubTotal, cSaleType, cItems, bitEnabled
								)
								SELECT numProId, numValue, @numDomainId, @fltSubTotal, @cSaleType, 'M', 1 FROM PromotionOfferItems WHERE numProId = @numProId AND tintType = 1 AND tintRecordType = 5
							END
							ELSE
							BEGIN
							
								DELETE FROM PromotionOfferOrderBased WHERE numProId = @numProId AND numDomainId = @numDomainId AND cSaleType = 'C'

								INSERT INTO PromotionOfferOrderBased
								(
									numProId, numProItemId, numDomainId, fltSubTotal, cSaleType, cItems, bitEnabled
								)
								SELECT numProId, numValue, @numDomainId, @fltSubTotal, @cSaleType, 'S', 1 FROM PromotionOfferItems WHERE numProId = @numProId AND tintType = 1 AND tintRecordType = 5
							END
						END
					END
					ELSE
					BEGIN
						IF (SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId = @numProId AND tintType = 1 AND tintRecordType = 5) = 0
						BEGIN

							RAISERROR ( 'SELECT_ITEMS_FOR_PROMOTIONS',16, 1 )
 							RETURN;
						END
					END
				END
				ELSE IF @cSaleType = 'C'
				BEGIN
				
					DELETE FROM PromotionOfferOrderBased WHERE numProId = @numProId AND numDomainId = @numDomainId
					DELETE FROM PromotionOfferItems WHERE numProId = @numProId AND tintType = 1 AND tintRecordType = 5

					INSERT INTO PromotionOfferOrderBased
					(
						numProId, cSaleType, numDomainId, bitEnabled
					)
					VALUES
					(	@numProId, @cSaleType, @numDomainId, 1	)
				END
				SELECT @numProId
			END
			ELSE IF @byteMode = 1
			BEGIN
				IF (SELECT COUNT(*) FROM PromotionOfferOrderBased WHERE numProId = @numProId AND fltSubTotal = @fltSubTotal) > 0
				BEGIN
					RAISERROR ( 'DUPLICATE_SUBTOTAL_AMOUNT',16, 1 )
 					RETURN;
				END

				CREATE TABLE #temp 
				(
					ID int IDENTITY (1, 1) NOT NULL ,
					numProOrderBasedID numeric(18,0)
				)
				INSERT INTO #temp(numProOrderBasedID)
					SELECT numProOrderBasedID FROM PromotionOfferOrderBased WHERE numProId = @numProId AND fltSubTotal = @fltOldSubTotal	

				UPDATE PromotionOfferOrderBased 
				SET fltSubTotal = @fltSubTotal 
				WHERE numProOrderBasedID IN ( SELECT numProOrderBasedID FROM #temp )
			END
		END
END
