/****** Object:  StoredProcedure [dbo].[usp_VerifyPartnerCode]    Script Date: 07/26/2008 16:15:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj                    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_VerifyPartnerCode')
DROP PROCEDURE usp_VerifyPartnerCode
GO
CREATE PROCEDURE [dbo].[usp_VerifyPartnerCode]                                         
 @vcPartnerCode  VARCHAR(200)=NULL,                                           
 @numDomainID  numeric=0                                           
AS                                                                       
BEGIN   
	SET @numDomainID=(SELECT TOP 1 numDomainID FROM Sites WHERE numSiteID=@numDomainID)                                       
  SELECT TOP 1 numDivisionID FROM DivisionMaster WHERE numDomainID=@numDomainID AND vcPartnerCode=@vcPartnerCode
END
