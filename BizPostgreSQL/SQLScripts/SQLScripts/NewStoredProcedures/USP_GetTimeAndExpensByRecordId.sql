     IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetTimeAndExpensByRecordId')
DROP PROCEDURE USP_GetTimeAndExpensByRecordId
GO
CREATE PROCEDURE [dbo].[USP_GetTimeAndExpensByRecordId]
    @numDomainID AS NUMERIC(9) = 0,
	@numRecordId AS NUMERIC(9)=0,
    @ClientTimeZoneOffset AS INT
AS 
BEGIN

	 SELECT TOP 1 CASE WHEN tintTEType = 0 THEN 'T & E'
                         WHEN tintTEType = 1
                         THEN CASE WHEN numCategory = 1
                                   THEN CASE WHEN ( SELECT  tintopptype
                                                    FROM    opportunitymaster
                                                    WHERE   numoppid = TE.numOppId
                                                  ) = 1 THEN 'Sales Time'
                                             ELSE 'Purch Time'
                                        END
                                   ELSE CASE WHEN ( SELECT  tintopptype
                                                    FROM    opportunitymaster
                                                    WHERE   numoppid = TE.numOppId
                                                  ) = 1 THEN 'Sales Exp'
                                             ELSE 'Purch Exp'
                                        END
                              END
                         WHEN tintTEType = 2
                         THEN CASE WHEN numCategory = 1 THEN 'Project Time'
                                   ELSE 'Project Exp'
                              END
                         WHEN tintTEType = 3
                         THEN CASE WHEN numCategory = 1 THEN 'Case Time'
                                   ELSE 'Case Exp'
                              END
                         WHEN tintTEType = 4
                         THEN 'Non-Paid Leave'
						  WHEN tintTEType = 5
                         THEN 'Expense'
                    END AS chrFrom,
                    numCategoryHDRID,
                    tintTEType,
                    CASE WHEN numCategory = 1
                         THEN 'Time (' + ( CONVERT(VARCHAR, DATEADD(minute, -@ClientTimeZoneOffset, dtFromDate))
                                           + '-'
                                           + CONVERT(VARCHAR, DATEADD(minute, -@ClientTimeZoneOffset, dtToDate)) )
                              + ')'
                         WHEN numCategory = 2 AND ISNULL(bitReimburse,0) = 1 AND numType = 2  THEN 'Reimbursable Expense'
                         WHEN numCategory = 2 AND ISNULL(bitReimburse,0) = 1 AND numType = 6 THEN 'Billable + Reimbursable Expense'
                         WHEN numCategory = 2 AND ISNULL(bitReimburse,0) = 0 AND numType = 1 THEN 'Billable Expense' 
                    END AS Category,
                    CASE WHEN numType = 1 THEN 'Billable'
                         WHEN numType = 2 THEN 'Non-Billable'
                         WHEN numType = 5 THEN 'Reimbursable Expense'
                         WHEN numType = 6 THEN 'Billable'
                         --WHEN numType = 3 THEN 'Paid Leave'
                         --WHEN numType = 4 THEN 'Non-Paid'
                    END AS [Type],
                    CASE WHEN numCategory = 1
                         THEN DATEADD(minute, -@ClientTimeZoneOffset,
                                      dtFromDate)
                         ELSE dtFromDate
                    END AS dtFromDate,
                    CASE WHEN numCategory = 1
                         THEN DATEADD(minute, -@ClientTimeZoneOffset, dtToDate)
                         ELSE dtToDate
                    END AS dtToDate,
                    bitFromFullDay,
                    bitToFullDay,
                    CONVERT(DECIMAL(10, 2), monAmount) AS [monAmount],
                    TE.numOppId,
                    OM.vcPOppName,
					CA.vcAccountName,
                    TE.numDivisionID,
                    txtDesc,
                    numUserCntID,
                    ISNULL(ACI.vcFirstName,'') + ' ' + ISNULL(ACI.vcLastName,'') AS [vcEmployee],
                    TE.numDomainID,
                    TE.numContractId,
                    numCaseid,
                    TE.numProid,
                    PM.vcProjectId,
                    numOppBizDocsId,
                    numStageId AS numStageID,
                    numCategory,
                    numtype,
                    CASE WHEN numCategory = 1
                         THEN CONVERT(VARCHAR, CONVERT(FLOAT, DATEDIFF(mi, dtFromDate, dtToDate)) / 60) 
                         WHEN numCategory = 2
                         THEN CONVERT(VARCHAR, CONVERT(DECIMAL(10, 2), monAmount))
                         WHEN numCategory = 3
                         THEN ( CASE WHEN bitFromFullDay = 0 THEN 'HDL' 
                                     WHEN bitFromFullDay = 1 THEN 'FDL'
                                END )
						 WHEN numCategory = 4
                         THEN ( CASE WHEN bitFromFullDay = 0 THEN 'HDL'
                                     WHEN bitFromFullDay = 1 THEN 'FDL'
                                END )
                    END AS Detail
                    ,CONVERT(VARCHAR, CONVERT(FLOAT, DATEDIFF(mi, dtFromDate, dtToDate)) / 60) [TimeValue]
                    ,CASE WHEN numCategory = 1 THEN CONVERT(DECIMAL(10, 2), (CONVERT(FLOAT, DATEDIFF(mi, dtFromDate, dtToDate)) / 60) * ISNULL(TE.monAmount,1)) 
						  WHEN numCategory = 2 THEN CONVERT(DECIMAL(10, 2),ISNULL(TE.monAmount,1))
						  ELSE 0
						  END AS [ExpenseValue]
                    ,(SELECT ISNULL(numItemCode,0) FROM item  WHERE numItemCode IN (SELECT numItemCode FROM dbo.OpportunityItems WHERE numoppitemtCode = TE.numOppItemID )) AS [numItemCode]
					,TE.numClassID
					,(SELECT ISNULL(vcItemDesc,'') FROM dbo.OpportunityItems WHERE numoppitemtCode = TE.numOppItemID ) AS [vcItemDesc]
					,CASE WHEN TE.numApprovalComplete=6 THEN 'Approved' WHEN TE.numApprovalComplete=-1 THEN 'Declined'  WHEN ISNULL(TE.numApprovalComplete,0)=0 THEN '' ELSE 'Waiting For Approve' END as ApprovalStatus
					,TE.numApprovalComplete,TE.numServiceItemID,TE.numClassID as numEClassId,TE.numExpId
			FROM    TimeAndExpense TE
            LEFT JOIN dbo.OpportunityMaster OM ON om.numoppId = TE.numOppId AND TE.numDomainID = OM.numDomainID
            LEFT JOIN dbo.ProjectsMaster PM ON PM.numProId = TE.numProID AND TE.numDomainID = PM.numDomainID
            LEFT JOIN dbo.Chart_Of_Accounts CA ON CA.numAccountId = TE.numExpId AND TE.numDomainID = CA.numDomainID
            LEFT JOIN AdditionalContactsInformation ACI ON ACI.numContactID = TE.numUserCntID
			LEFT JOIN ApprovalConfig AS A on TE.numUserCntID=A.numUserId AND A.numDomainID=@numDomainId AND A.numModule=1 WHERE TE.numCategoryHDRID=@numRecordId
	 

END