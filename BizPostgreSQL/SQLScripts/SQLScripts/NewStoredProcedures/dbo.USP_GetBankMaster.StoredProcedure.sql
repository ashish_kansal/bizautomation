GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetBankMaster')
	DROP PROCEDURE USP_GetBankMaster
GO

CREATE PROCEDURE [dbo].[USP_GetBankMaster]
	@numBankMasterID numeric(18, 0)
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[numBankMasterID],
	[tintBankType],
	[vcFIId],
	[vcFIName],
	[vcFIOrganization],
	[vcFIOFXServerUrl],
	[vcBankID],
	[vcOFXAccessKey],
	[dtCreatedDate],
	[dtModifiedDate],
	[vcBankPhone],
	[vcBankWebsite],
	[vcUserIdLabel]
FROM
	[dbo].[BankMaster]
WHERE
	(	[numBankMasterID] = @numBankMasterID
 or @numBankMasterID = 0 )


GO