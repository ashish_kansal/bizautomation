GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_DivisionMasterShippingConfiguration_Get')
DROP PROCEDURE USP_DivisionMasterShippingConfiguration_Get
GO
CREATE PROCEDURE [dbo].[USP_DivisionMasterShippingConfiguration_Get]        
	@numDomainID NUMERIC(18,0)
	,@numDivisionID NUMERIC(18,0)
AS        
BEGIN     
	SELECT
		numDomainID
        ,numDivisionID
        ,IsAdditionalHandling
        ,IsCOD
        ,IsHomeDelivery
        ,IsInsideDelevery
        ,IsInsidePickup
        ,IsSaturdayDelivery
        ,IsSaturdayPickup
        ,IsLargePackage
        ,vcDeliveryConfirmation
        ,vcDescription
        ,vcSignatureType
        ,vcCODType
	FROM
		DivisionMasterShippingConfiguration
	WHERE
		numDomainID=@numDomainID
		AND numDivisionID=@numDivisionID
END
GO


