GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_CopyEmbeddedCost')
DROP PROCEDURE USP_CopyEmbeddedCost
GO
-- EXEC USP_CopyEmbeddedCost 12003,12002,110
CREATE PROCEDURE USP_CopyEmbeddedCost
    @numFromBizDocID NUMERIC,
    @numToBizDocID NUMERIC,
    @numDomainID NUMERIC 
AS 
    BEGIN
    

	
        INSERT  INTO [EmbeddedCost]
                (
                  [numOppBizDocID],
                  [numCostCatID],
                  [numAccountID],
                  [numDivisionID],
                  [numPaymentMethod],
                  [monCost],
                  [monActualCost],
                  [vcMemo],
                  [dtDueDate],
                  [numDomainID],
                  [numCreatedBy],
                  [dtCreatedDate],
                  [numModifiedBy],
                  [dtModifiedDate],
                  [numCostCenterID],
                  [numBizDocsPaymentDetId]
	          )
                SELECT  @numToBizDocID,
                        [numCostCatID],
                        [numAccountID],
                        [numDivisionID],
                        [numPaymentMethod],
                        [monCost],
                        [monActualCost],
                        [vcMemo],
                        [dtDueDate],
                        [numDomainID],
                        [numCreatedBy],
                        GETUTCDATE(),
                        [numModifiedBy],
                        GETUTCDATE(),
                        [numCostCenterID],
                        null
                FROM    [EmbeddedCost]
                WHERE   [numOppBizDocID] = @numFromBizDocID
                
		
		
		DECLARE @numCostCatID NUMERIC
		DECLARE @numCostCenterID NUMERIC
		DECLARE @MaxID NUMERIC
		DECLARE @MinID NUMERIC
		
		SELECT @MaxID = MAX([numEmbeddedCostID]), @MinID = MIN([numEmbeddedCostID])
		FROM [EmbeddedCost] WHERE [numOppBizDocID] = @numToBizDocID
		
		PRINT @MinID
		PRINT @MaxID
		WHILE @MaxID>=@MinID
		BEGIN
			
			SELECT  @numCostCenterID=numCostCenterID,@numCostCatID=numCostCatID
			FROM [EmbeddedCost] EC
			WHERE [numEmbeddedCostID] =@MinID
			
			PRINT 'costcenter id'
			PRINT @numCostCenterID
			PRINT 'costcat id'
			PRINT @numCostCatID
			
			INSERT INTO [EmbeddedCostItems] (
				[numEmbeddedCostID],
				[numOppBizDocItemID],
				[monAmount],
				[numDomainID]
			) 
			SELECT 
				   @MinID,
				   (SELECT TOP 1 numOppBizDocItemID FROM [OpportunityBizDocItems] WHERE numOppItemID = OBI.[numOppItemID] AND [numOppBizDocID] = @numToBizDocID),
				   ECI.[monAmount],
				   ECI.[numDomainID] 
			FROM [EmbeddedCost] EC INNER JOIN [EmbeddedCostItems] ECI ON EC.[numEmbeddedCostID] = ECI.[numEmbeddedCostID]
			INNER JOIN [OpportunityBizDocItems] OBI ON OBI.numOppBizDocItemID = ECI.numOppBizDocItemID
			WHERE EC.[numCostCatID]=@numCostCatID
			AND EC.[numCostCenterID] = @numCostCenterID
			AND EC.[numOppBizDocID] = @numFromBizDocID
			
			SELECT @MinID = MIN(numEmbeddedCostID)
			FROM [EmbeddedCost] WHERE [numOppBizDocID]=@numToBizDocID
			AND [numEmbeddedCostID] > @MinID
		END

    END