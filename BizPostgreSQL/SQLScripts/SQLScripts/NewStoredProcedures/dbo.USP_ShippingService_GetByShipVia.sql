GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- created By Priya Sharma
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ShippingService_GetByShipVia')
DROP PROCEDURE USP_ShippingService_GetByShipVia
GO

CREATE PROCEDURE [dbo].[USP_ShippingService_GetByShipVia] 
@numDomainID AS numeric(9)=0  ,
@numShipViaID AS numeric(18,0)
AS      
BEGIN    
	SELECT 
		ShippingService.numDomainID
		,ShippingService.numShippingServiceID
		,ShippingService.vcShipmentService
		,ISNULL(ShippingServiceAbbreviations.vcAbbreviation,'') vcAbbreviation
	FROM 
		ShippingService
	LEFT JOIN
		ShippingServiceAbbreviations
	ON
		ShippingService.numShippingServiceID = ShippingServiceAbbreviations.numShippingServiceID
	WHERE 
		(ShippingService.numDomainId = @numDomainID OR ISNULL(ShippingService.numDomainID,0)=0)
		AND (numShipViaID=@numShipViaID OR ISNULL(@numShipViaID,0)=0)
END    
GO