/****** Object:  StoredProcedure [dbo].[USP_AssCntCase]    Script Date: 07/26/2008 16:14:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--modified by anoop jayaraj      
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_ProjectDefaultAssociateContacts')
DROP PROCEDURE usp_ProjectDefaultAssociateContacts
GO
CREATE PROCEDURE [dbo].[usp_ProjectDefaultAssociateContacts]            
@numProId numeric(9)=0 ,
@numDomainID as numeric(9)=0   
AS
BEGIN
DECLARE @numRecordOwnerRoleId AS NUMERIC(18,0)=0
DECLARE @numRecordAssigneeRoleId AS NUMERIC(18,0)=0
DECLARE @numRecordOrganizationContactsRoleId AS NUMERIC(18,0)=0
--Record Owner
IF((SELECT COUNT(*) FROM listdetails WHERE numListID=26  AND numDomainID=@numDomainID AND vcData='Record Owner')=0)
BEGIN
	INSERT INTO ListDetails(numListID,numDomainID,vcData)VALUES(26,@numDomainID,'Record Owner')
	SET @numRecordOwnerRoleId =(SELECT SCOPE_IDENTITY())
END
ELSE
BEGIN
	SELECT TOP 1 @numRecordOwnerRoleId=numListItemID FROM listdetails WHERE numListID=26  AND numDomainID=@numDomainID AND vcData='Record Owner'
END
--Assignee
IF((SELECT COUNT(*) FROM listdetails WHERE numListID=26  AND numDomainID=@numDomainID AND vcData='Assignee')=0)
BEGIN
	INSERT INTO ListDetails(numListID,numDomainID,vcData)VALUES(26,@numDomainID,'Assignee')
	SET @numRecordAssigneeRoleId =(SELECT SCOPE_IDENTITY())
END
ELSE
BEGIN
	SELECT TOP 1 @numRecordAssigneeRoleId=numListItemID FROM listdetails WHERE numListID=26  AND numDomainID=@numDomainID AND vcData='Assignee'
END
--Organization Contact
IF((SELECT COUNT(*) FROM listdetails WHERE numListID=26  AND numDomainID=@numDomainID AND vcData='Organization Contact')=0)
BEGIN
	INSERT INTO ListDetails(numListID,numDomainID,vcData)VALUES(26,@numDomainID,'Organization Contact')
	SET @numRecordOrganizationContactsRoleId =(SELECT SCOPE_IDENTITY())
END
ELSE
BEGIN
	SELECT TOP 1 @numRecordOrganizationContactsRoleId=numListItemID FROM listdetails WHERE numListID=26  AND numDomainID=@numDomainID AND vcData='Organization Contact'
END

IF((SELECT COUNT(*) FROM ProjectsContacts WHERE numProId=@numProId AND numRole=@numRecordOwnerRoleId)=0)
BEGIN
	DECLARE @numROwnerContactID AS NUMERIC(18,0)=0
	SET @numROwnerContactID = (SELECT TOP 1 numRecOwner FROM ProjectsMaster WHERE numProId=@numProId)
	if(isnull(@numROwnerContactID,0)>0)
	begin
	insert into ProjectsContacts(numProId, numContactId, numRole, bitPartner,bitSubscribedEmailAlert)
	values (@numProId,@numROwnerContactID,@numRecordOwnerRoleId,0,0)
	end
END
IF((SELECT COUNT(*) FROM ProjectsContacts WHERE numProId=@numProId AND numRole=@numRecordAssigneeRoleId)=0)
BEGIN
	DECLARE @numRAssigneContactID AS NUMERIC(18,0)=0
	SET @numRAssigneContactID = (SELECT TOP 1 numAssignedBy FROM ProjectsMaster WHERE numProId=@numProId)
	if(isnull(@numRAssigneContactID,0)>0)
	begin
	insert into ProjectsContacts(numProId, numContactId, numRole, bitPartner,bitSubscribedEmailAlert)
	values (@numProId,@numRAssigneContactID,@numRecordAssigneeRoleId,0,0)
	END
END
IF((SELECT COUNT(*) FROM ProjectsContacts WHERE numProId=@numProId AND numRole=@numRecordOrganizationContactsRoleId)=0)
BEGIN
	DECLARE @numROrgContactID AS NUMERIC(18,0)=0
	SET @numROrgContactID = (SELECT TOP 1 numCustPrjMgr FROM ProjectsMaster WHERE numProId=@numProId)
	if(isnull(@numROrgContactID,0)>0)
	begin
	insert into ProjectsContacts(numProId, numContactId, numRole, bitPartner,bitSubscribedEmailAlert)
	values (@numProId,@numROrgContactID,@numRecordOrganizationContactsRoleId,0,0)
	end
END
END