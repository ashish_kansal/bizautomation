GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GET_DATA_FOR_ROLLBACK')
DROP PROCEDURE USP_GET_DATA_FOR_ROLLBACK
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- USP_GET_DATA_FOR_ROLLBACK 
CREATE PROCEDURE USP_GET_DATA_FOR_ROLLBACK
AS 
BEGIN
	SELECT  HISTORY.numHistoryID
			,MASTER.intImportFileID
			,ISNULL(vcImportFileName,'') AS vcImportFileName
		    ,ISNULL(numMasterID,0) AS numMasterID
		    ,ISNULL(numRecordAdded,0) AS numRecordAdded
		    ,ISNULL(numRecordUpdated,0) numRecordUpdated
		    ,ISNULL(numErrors,0) numErrors
		    ,ISNULL(numDuplicates,0) numDuplicates
		    ,ISNULL(dtCreateDate,'') dtCreateDate
		    ,ISNULL(dtImportDate,'') dtImportDate
		    ,ISNULL(numDomainID,0)  numDomainID
		    ,ISNULL(numUserContactID,0) numUserContactID
		    ,ISNULL(tintStatus,0) tintStatus
		    ,btHasSendEmail
			,ISNULL(vcHistory_Added_Value,'') AS [ItemCodes]
			,dtHistoryDateTime 
	FROM dbo.Import_File_Master MASTER
	INNER JOIN dbo.Import_History HISTORY ON MASTER.intImportFileID = HISTORY.intImportFileID
										 AND MASTER.numHistoryID = HISTORY.numHistoryID
	WHERE 1=1
	--AND (tintStatus = 2 OR tintStatus = 3)
	AND tintStatus = 2
	AND (ISNULL(numRecordAdded,0) > 0 OR ISNULL(numRecordAdded,0) = 0)
	ORDER BY intImportFileID,dtImportDate
END

