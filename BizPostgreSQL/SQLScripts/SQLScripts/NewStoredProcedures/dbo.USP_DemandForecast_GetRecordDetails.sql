GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_DemandForecast_GetRecordDetails')
DROP PROCEDURE dbo.USP_DemandForecast_GetRecordDetails
GO
CREATE PROCEDURE [dbo].[USP_DemandForecast_GetRecordDetails]
	@numDomainID NUMERIC(18,0)
	,@tintType TINYINT
	,@numItemCode NUMERIC(18,0)
	,@numWarehouseItemID NUMERIC(18,0)
	,@numForecastDays INT
	,@numAnalysisDays INT
	,@bitBasedOnLastYear BIT
	,@numOpportunityPercentComplete NUMERIC(18,0)
	,@CurrentPage INT
    ,@PageSize INT
	,@ClientTimeZoneOffset INT
AS 
BEGIN
	DECLARE @TEMP TABLE
	(
		numOppID NUMERIC(18,0)
		,numWOID NUMERIC(18,0)
		,dtCreatedDate DATETIME
		,vcOppName VARCHAR(500)
		,numOrderedQty FLOAT
		,numQtyToShip FLOAT
		,numOpportunityForecastQty FLOAT
		,dtReleaseDate VARCHAR(50)
	)

	IF @tintType = 1 -- Release Dates
	BEGIN
		INSERT INTO @TEMP
		(
			numOppID
			,numWOID
			,dtCreatedDate
			,vcOppName
			,numOrderedQty
			,numQtyToShip
			,numOpportunityForecastQty
			,dtReleaseDate
		)
		SELECT
			OpportunityMaster.numOppId
			,0
			,OpportunityMaster.bintCreatedDate
			,OpportunityMaster.vcPOppName
			,OpportunityItems.numUnitHour
			,OpportunityItems.numUnitHour - ISNULL(numQtyShipped,0)
			,0
			,dbo.FormatedDateFromDate(ISNULL(OpportunityItems.ItemReleaseDate,OpportunityMaster.dtReleaseDate),@numDomainID)
		FROM
			OpportunityMaster
		INNER JOIN
			OpportunityItems
		ON
			OpportunityMaster.numOppId = OpportunityItems.numOppId
		INNER JOIN
			Item
		ON
			OpportunityItems.numItemCode = Item.numItemCode
		WHERE
			OpportunityMaster.numDomainId=@numDomainID
			AND OpportunityMaster.tintOppType=1
			AND OpportunityMaster.tintOppStatus=1
			AND ISNULL(OpportunityMaster.tintshipped,0)=0
			AND (ISNULL(OpportunityItems.numUnitHour,0)  - ISNULL(OpportunityItems.numQtyShipped,0)) > 0
			AND ISNULL(OpportunityItems.bitDropShip,0) = 0
			AND ISNULL(Item.IsArchieve,0) = 0
			AND ISNULL(Item.bitAssembly,0) = 0
			AND ISNULL(Item.bitKitParent,0) = 0
			AND (OpportunityItems.ItemReleaseDate IS NOT NULL OR OpportunityMaster.dtReleaseDate IS NOT NULL)
			AND OpportunityItems.numItemCode=@numItemCode
			AND OpportunityItems.numWarehouseItmsID=@numWarehouseItemID
			AND ISNULL(OpportunityItems.ItemReleaseDate,OpportunityMaster.dtReleaseDate) <= DATEADD(DAY,@numForecastDays,DATEADD(MINUTE,-@ClientTimeZoneOffset,GETUTCDATE()))

		-- USED IN KIT
		INSERT INTO @TEMP
		(
			numOppID
			,numWOID
			,dtCreatedDate
			,vcOppName
			,numOrderedQty
			,numQtyToShip
			,numOpportunityForecastQty
			,dtReleaseDate
		)
		SELECT
			OM.numOppId
			,0
			,OM.bintCreatedDate
			,CONCAT(OM.vcPOppName,' (Kit Memeber)')
			,OKI.numQtyItemsReq
			,OKI.numQtyItemsReq - ISNULL(OKI.numQtyShipped,0)
			,0
			,dbo.FormatedDateFromDate(ISNULL(OI.ItemReleaseDate,OM.dtReleaseDate),@numDomainID)
		FROM
			OpportunityMaster OM
		INNER JOIN
			OpportunityItems OI
		ON
			OM.numOppId = OI.numOppId
		INNER JOIN
			OpportunityKitItems OKI
		ON
			OI.numoppitemtCode=OKI.numOppItemID
		INNER JOIN
			Item
		ON
			OKI.numChildItemID = Item.numItemCode
		INNER JOIN
			WareHouseItems WI
		ON
			OKI.numWareHouseItemId = WI.numWareHouseItemID
		WHERE
			OM.numDomainId=@numDomainID
			AND OM.tintOppType=1
			AND OM.tintOppStatus=1
			AND ISNULL(OM.tintshipped,0)=0
			AND ISNULL(OI.bitDropShip,0) = 0
			AND ISNULL(OI.bitWorkOrder,0) = 0
			AND ISNULL(Item.IsArchieve,0) = 0
			AND ISNULL(Item.bitAssembly,0) = 0
			AND ISNULL(Item.bitKitParent,0) = 0
			AND (ISNULL(OKI.numQtyItemsReq,0) - ISNULL(OKI.numQtyShipped,0)) > 0
			AND (OM.dtReleaseDate IS NOT NULL OR OI.ItemReleaseDate IS NOT NULL)
			AND OKI.numChildItemID=@numItemCode
			AND OKI.numWareHouseItemId=@numWarehouseItemID
			AND ISNULL(OI.ItemReleaseDate,OM.dtReleaseDate) <= DATEADD(DAY,@numForecastDays,DATEADD(MINUTE,-@ClientTimeZoneOffset,GETUTCDATE()))

		-- USED IN KIT WITHIN KIT
		INSERT INTO @TEMP
		(
			numOppID
			,numWOID
			,dtCreatedDate
			,vcOppName
			,numOrderedQty
			,numQtyToShip
			,numOpportunityForecastQty
			,dtReleaseDate
		)
		SELECT
			OM.numOppId
			,0
			,OM.bintCreatedDate
			,CONCAT(OM.vcPOppName,' (Kit Memeber)')
			,OKCI.numQtyItemsReq
			,OKCI.numQtyItemsReq - ISNULL(OKCI.numQtyShipped,0)
			,0
			,dbo.FormatedDateFromDate(ISNULL(OI.ItemReleaseDate,OM.dtReleaseDate),@numDomainID)
		FROM
			OpportunityMaster OM
		INNER JOIN
			OpportunityItems OI
		ON
			OM.numOppId = OI.numOppId
		INNER JOIN
			OpportunityKitChildItems OKCI
		ON
			OI.numoppitemtCode = OKCI.numOppItemID
		INNER JOIN
			Item
		ON
			OKCI.numItemID = Item.numItemCode
		INNER JOIN
			WareHouseItems WI
		ON
			OKCI.numWareHouseItemId = WI.numWareHouseItemID
		WHERE
			OM.numDomainId=@numDomainID
			AND OM.tintOppType=1
			AND OM.tintOppStatus=1
			AND ISNULL(OM.tintshipped,0)=0
			AND ISNULL(OI.bitDropShip,0) = 0
			AND ISNULL(OI.bitWorkOrder,0) = 0
			AND ISNULL(Item.IsArchieve,0) = 0
			AND ISNULL(Item.bitAssembly,0) = 0
			AND ISNULL(Item.bitKitParent,0) = 0
			AND (ISNULL(OKCI.numQtyItemsReq,0) - ISNULL(OKCI.numQtyShipped,0)) > 0
			AND (OM.dtReleaseDate IS NOT NULL OR OI.ItemReleaseDate IS NOT NULL)
			AND OKCI.numItemID=@numItemCode
			AND OKCI.numWareHouseItemId=@numWarehouseItemID
			AND ISNULL(OI.ItemReleaseDate,OM.dtReleaseDate) <= DATEADD(DAY,@numForecastDays,DATEADD(MINUTE,-@ClientTimeZoneOffset,GETUTCDATE()))
		
		-- USED IN WORK ORDER
		INSERT INTO @TEMP
		(
			numOppID
			,numWOID
			,dtCreatedDate
			,vcOppName
			,numOrderedQty
			,numQtyToShip
			,numOpportunityForecastQty
			,dtReleaseDate
		)
		SELECT
			OM.numOppId
			,WO.numWOId
			,CASE WHEN OM.numOppId IS NOT NULL THEN OM.bintCreatedDate ELSE WO.bintCreatedDate END
			,CONCAT(CASE WHEN OM.numOppId IS NOT NULL THEN OM.vcPOppName ELSE 'Work Order' END,' (Assembly Memeber)') 
			,WOD.numQtyItemsReq
			,WOD.numQtyItemsReq 
			,0
			,CASE WHEN OM.numOppId IS NOT NULL THEN dbo.FormatedDateFromDate(ISNULL(OI.ItemReleaseDate,OM.dtReleaseDate),@numDomainID) ELSE dbo.FormatedDateFromDate(WO.bintCompliationDate,@numDomainID) END
		FROM
			WorkOrderDetails WOD
		INNER JOIN
			Item
		ON
			WOD.numChildItemID = Item.numItemCode
		INNER JOIN
			WareHouseItems WI
		ON
			WOD.numWareHouseItemId = WI.numWareHouseItemID
		INNER JOIN
			WorkOrder WO
		ON
			WOD.numWOId=WO.numWOId
		LEFT JOIN
			OpportunityItems OI
		ON
			WO.numOppItemID = OI.numoppitemtCode
		LEFT JOIN
			OpportunityMaster OM
		ON
			OI.numOppId=OM.numOppId
		WHERE
			WO.numDomainID=@numDomainID
			AND WO.numWOStatus <> 23184 -- NOT COMPLETED
			AND ISNULL(Item.IsArchieve,0) = 0
			AND ISNULL(Item.bitAssembly,0) = 0
			AND ISNULL(Item.bitKitParent,0) = 0
			AND ISNULL(WOD.numQtyItemsReq,0) > 0
			AND (OM.dtReleaseDate IS NOT NULL OR OI.ItemReleaseDate IS NOT NULL OR WO.bintCompliationDate IS NOT NULL)
			AND 1 = (CASE WHEN OM.numOppId IS NOT NULL THEN (CASE WHEN OM.tintOppStatus=1 THEN 1 ELSE 0 END) ELSE 1 END)
			AND WOD.numChildItemID = @numItemCode
			AND WOD.numWareHouseItemId=@numWarehouseItemID
			AND 1 = (CASE 
					WHEN OI.numoppitemtCode IS NOT NULL 
					THEN (CASE 
							WHEN ISNULL(OI.ItemReleaseDate,OM.dtReleaseDate) <= DATEADD(DAY,@numForecastDays,DATEADD(MINUTE,-@ClientTimeZoneOffset,GETUTCDATE())) 
							THEN 1 
							ELSE 0 
						END)
					ELSE 
						(CASE 
						WHEN WO.bintCompliationDate <= DATEADD(DAY,@numForecastDays,DATEADD(MINUTE,-@ClientTimeZoneOffset,GETUTCDATE())) 
						THEN 1 
						ELSE 0 
						END)
					END)
	END
	ELSE IF @tintType = 2 -- Historical Sales
	BEGIN
		DECLARE @dtFromDate DATETIME
		DECLARE @dtToDate DATETIME

		IF ISNULL(@bitBasedOnLastYear,0) = 1 --last week from last year
		BEGIN
			SET @dtToDate = DATEADD(yy,-1,DATEADD(d,-1,GETUTCDATE()))
			SET @dtFromDate = DATEADD(yy,-1,DATEADD(d,-(@numAnalysisDays),GETUTCDATE()))
		END
		ELSE
		BEGIN
			SET @dtToDate = DATEADD(d,-1,GETUTCDATE())
			SET @dtFromDate = DATEADD(d,-(@numAnalysisDays),GETUTCDATE())
		END

		INSERT INTO @TEMP
		(
			numOppID
			,numWOID
			,dtCreatedDate
			,vcOppName
			,numOrderedQty
			,numQtyToShip
			,numOpportunityForecastQty
			,dtReleaseDate
		)
		SELECT
			OM.numOppId
			,0
			,OM.bintCreatedDate
			,OM.vcPOppName
			,OI.numUnitHour
			,OI.numUnitHour - ISNULL(numQtyShipped,0)
			,0
			,dbo.FormatedDateFromDate(ISNULL(OI.ItemReleaseDate,OM.dtReleaseDate),@numDomainID)
		FROM
			OpportunityItems OI
		INNER JOIN
			OpportunityMaster OM
		ON
			OI.numOppId=OM.numOppId
		WHERE
			OM.numDomainId=@numDomainID
			AND OM.tintOppType=1
			AND OM.tintOppStatus=1
			AND numItemCode=@numItemCode
			AND numWarehouseItmsID=@numWarehouseItemID
			AND ISNULL(bitDropship,0) = 0
			AND CAST(OM.bintCreatedDate AS DATE) BETWEEN CONVERT(DATE,@dtFromDate) AND CONVERT(DATE,@dtToDate)

		-- USED IN KIT
		INSERT INTO @TEMP
		(
			numOppID
			,numWOID
			,dtCreatedDate
			,vcOppName
			,numOrderedQty
			,numQtyToShip
			,numOpportunityForecastQty
			,dtReleaseDate
		)
		SELECT
			OM.numOppId
			,0
			,OM.bintCreatedDate
			,CONCAT(OM.vcPOppName,' (Kit Memeber)')
			,OKI.numQtyItemsReq
			,OKI.numQtyItemsReq - ISNULL(OKI.numQtyShipped,0)
			,0
			,dbo.FormatedDateFromDate(ISNULL(OI.ItemReleaseDate,OM.dtReleaseDate),@numDomainID)
		FROM
			OpportunityKitItems OKI
		INNER JOIN
			OpportunityItems OI
		ON
			OKI.numOppItemID=OI.numoppitemtCode
		INNER JOIN
			OpportunityMaster OM
		ON
			OI.numOppId=OM.numOppId
		WHERE
			OM.numDomainId=@numDomainID
			AND OM.tintOppType=1
			AND OM.tintOppStatus=1
			AND OKI.numChildItemID=@numItemCode
			AND OKI.numWareHouseItemId=@numWarehouseItemID
			AND ISNULL(OI.bitWorkOrder,0) = 0
			AND CAST(OM.bintCreatedDate AS DATE) BETWEEN CONVERT(DATE,@dtFromDate) AND CONVERT(DATE,@dtToDate)

		-- USED IN KIT WITHIN KIT
		INSERT INTO @TEMP
		(
			numOppID
			,numWOID
			,dtCreatedDate
			,vcOppName
			,numOrderedQty
			,numQtyToShip
			,numOpportunityForecastQty
			,dtReleaseDate
		)
		SELECT
			OM.numOppId
			,0
			,OM.bintCreatedDate
			,CONCAT(OM.vcPOppName,' (Kit Memeber)')
			,OKCI.numQtyItemsReq
			,OKCI.numQtyItemsReq - ISNULL(OKCI.numQtyShipped,0)
			,0
			,dbo.FormatedDateFromDate(ISNULL(OI.ItemReleaseDate,OM.dtReleaseDate),@numDomainID)
		FROM
			OpportunityKitChildItems OKCI
		INNER JOIN
			OpportunityItems OI
		ON
			OKCI.numOppItemID=OI.numoppitemtCode
		INNER JOIN
			OpportunityMaster OM
		ON
			OI.numOppId=OM.numOppId
		WHERE
			OM.numDomainId=@numDomainID
			AND OM.tintOppType=1
			AND OM.tintOppStatus=1
			AND OKCI.numItemID=@numItemCode
			AND OKCI.numWareHouseItemId=@numWarehouseItemID
			AND ISNULL(OI.bitWorkOrder,0) = 0
			AND CAST(OM.bintCreatedDate AS DATE) BETWEEN CONVERT(DATE,@dtFromDate) AND CONVERT(DATE,@dtToDate)

		-- USED IN WORK ORDER
		INSERT INTO @TEMP
		(
			numOppID
			,numWOID
			,dtCreatedDate
			,vcOppName
			,numOrderedQty
			,numQtyToShip
			,numOpportunityForecastQty
			,dtReleaseDate
		)
		SELECT
			OM.numOppId
			,WO.numWOId
			,CASE WHEN OM.numOppId IS NOT NULL THEN OM.bintCreatedDate ELSE WO.bintCreatedDate END
			,CONCAT(CASE WHEN OM.numOppId IS NOT NULL THEN OM.vcPOppName ELSE 'Work Order' END,' (Assembly Memeber)') 
			,WOD.numQtyItemsReq
			,(CASE WHEN WO.numWOStatus = 23184 THEN 0 ELSE WOD.numQtyItemsReq END)
			,0
			,CASE WHEN OM.numOppId IS NOT NULL THEN dbo.FormatedDateFromDate(ISNULL(OI.ItemReleaseDate,OM.dtReleaseDate),@numDomainID) ELSE dbo.FormatedDateFromDate(WO.bintCompliationDate,@numDomainID) END
		FROM
			WorkOrderDetails WOD
		INNER JOIN
			WorkOrder WO
		ON
			WOD.numWOId=WO.numWOId
		LEFT JOIN
			OpportunityItems OI
		ON
			WO.numOppItemID = OI.numoppitemtCode
		LEFT JOIN
			OpportunityMaster OM
		ON
			OI.numOppId=OM.numOppId
		WHERE
			WO.numDomainID=@numDomainID
			AND WOD.numChildItemID = @numItemCode
			AND WOD.numWareHouseItemId=@numWarehouseItemID
			AND CAST(WO.bintCreatedDate AS DATE) BETWEEN CONVERT(DATE,@dtFromDate) AND CONVERT(DATE,@dtToDate)
	END
	ELSE IF @tintType = 3 -- Sales Opportunity
	BEGIN
		INSERT INTO @TEMP
		(
			numOppID
			,numWOID
			,dtCreatedDate
			,vcOppName
			,numOrderedQty
			,numQtyToShip
			,numOpportunityForecastQty
			,dtReleaseDate
		)
		SELECT
			OM.numOppId
			,0
			,OM.bintCreatedDate
			,OM.vcPOppName
			,OI.numUnitHour
			,OI.numUnitHour
			,OI.numUnitHour * (PP.intTotalProgress/100)
			,dbo.FormatedDateFromDate(ISNULL(OI.ItemReleaseDate,OM.dtReleaseDate),@numDomainID)
		FROM
			OpportunityItems OI
		INNER JOIN
			OpportunityMaster OM
		ON
			OI.numOppId=OM.numOppId
		INNER JOIN
			ProjectProgress PP
		ON
			OM.numOppId = PP.numOppId
		WHERE
			OM.numDomainId=@numDomainID
			AND OM.tintOppType=1
			AND OM.tintOppStatus=0
			AND numItemCode=@numItemCode
			AND numWarehouseItmsID=@numWarehouseItemID
			AND ISNULL(bitDropship,0) = 0
			AND (OM.dtReleaseDate IS NOT NULL OR OI.ItemReleaseDate IS NOT NULL)
			AND ISNULL(OI.ItemReleaseDate,OM.dtReleaseDate) <= DATEADD(DAY,@numForecastDays,DATEADD(MINUTE,-@ClientTimeZoneOffset,GETUTCDATE()))
			AND PP.intTotalProgress >= @numOpportunityPercentComplete

		-- USED IN KIT
		INSERT INTO @TEMP
		(
			numOppID
			,numWOID
			,dtCreatedDate
			,vcOppName
			,numOrderedQty
			,numQtyToShip
			,numOpportunityForecastQty
			,dtReleaseDate
		)
		SELECT
			OM.numOppId
			,0
			,OM.bintCreatedDate
			,CONCAT(OM.vcPOppName,' (Kit Memeber)')
			,OKI.numQtyItemsReq
			,OKI.numQtyItemsReq
			,OKI.numQtyItemsReq * (PP.intTotalProgress/100)
			,dbo.FormatedDateFromDate(ISNULL(OI.ItemReleaseDate,OM.dtReleaseDate),@numDomainID)
		FROM
			OpportunityKitItems OKI
		INNER JOIN
			OpportunityItems OI
		ON
			OKI.numOppItemID=OI.numoppitemtCode
		INNER JOIN
			OpportunityMaster OM
		ON
			OI.numOppId=OM.numOppId
		INNER JOIN
			ProjectProgress PP
		ON
			OM.numOppId = PP.numOppId
		WHERE
			OM.numDomainId=@numDomainID
			AND OM.tintOppType=1
			AND OM.tintOppStatus=0
			AND OKI.numChildItemID=@numItemCode
			AND OKI.numWareHouseItemId=@numWarehouseItemID
			AND ISNULL(OI.bitWorkOrder,0) = 0
			AND (OM.dtReleaseDate IS NOT NULL OR OI.ItemReleaseDate IS NOT NULL)
			AND ISNULL(OI.ItemReleaseDate,OM.dtReleaseDate) <= DATEADD(DAY,@numForecastDays,DATEADD(MINUTE,-@ClientTimeZoneOffset,GETUTCDATE()))
			AND PP.intTotalProgress >= @numOpportunityPercentComplete

		-- USED IN KIT WITHIN KIT
		INSERT INTO @TEMP
		(
			numOppID
			,numWOID
			,dtCreatedDate
			,vcOppName
			,numOrderedQty
			,numQtyToShip
			,numOpportunityForecastQty
			,dtReleaseDate
		)
		SELECT
			OM.numOppId
			,0
			,OM.bintCreatedDate
			,CONCAT(OM.vcPOppName,' (Kit Memeber)')
			,OKCI.numQtyItemsReq
			,OKCI.numQtyItemsReq
			,OKCI.numQtyItemsReq * (PP.intTotalProgress/100)
			,dbo.FormatedDateFromDate(ISNULL(OI.ItemReleaseDate,OM.dtReleaseDate),@numDomainID)
		FROM
			OpportunityKitChildItems OKCI
		INNER JOIN
			OpportunityItems OI
		ON
			OKCI.numOppItemID=OI.numoppitemtCode
		INNER JOIN
			OpportunityMaster OM
		ON
			OI.numOppId=OM.numOppId
		INNER JOIN
			ProjectProgress PP
		ON
			OM.numOppId = PP.numOppId
		WHERE
			OM.numDomainId=@numDomainID
			AND OM.tintOppType=1
			AND OM.tintOppStatus=0
			AND OKCI.numItemID=@numItemCode
			AND OKCI.numWareHouseItemId=@numWarehouseItemID
			AND ISNULL(OI.bitWorkOrder,0) = 0
			AND (OM.dtReleaseDate IS NOT NULL OR OI.ItemReleaseDate IS NOT NULL)
			AND ISNULL(OI.ItemReleaseDate,OM.dtReleaseDate) <= DATEADD(DAY,@numForecastDays,DATEADD(MINUTE,-@ClientTimeZoneOffset,GETUTCDATE()))
			AND PP.intTotalProgress >= @numOpportunityPercentComplete

		-- USED IN WORK ORDER
		INSERT INTO @TEMP
		(
			numOppID
			,numWOID
			,dtCreatedDate
			,vcOppName
			,numOrderedQty
			,numQtyToShip
			,numOpportunityForecastQty
			,dtReleaseDate
		)
		SELECT
			OM.numOppId
			,WO.numWOId
			,CASE WHEN OM.numOppId IS NOT NULL THEN OM.bintCreatedDate ELSE WO.bintCreatedDate END
			,CONCAT(CASE WHEN OM.numOppId IS NOT NULL THEN OM.vcPOppName ELSE 'Work Order' END,' (Assembly Memeber)') 
			,WOD.numQtyItemsReq
			,WOD.numQtyItemsReq
			,WOD.numQtyItemsReq * (PP.intTotalProgress/100)
			,CASE WHEN OM.numOppId IS NOT NULL THEN dbo.FormatedDateFromDate(ISNULL(OI.ItemReleaseDate,OM.dtReleaseDate),@numDomainID) ELSE dbo.FormatedDateFromDate(WO.bintCompliationDate,@numDomainID) END
		FROM
			OpportunityMaster OM
		INNER JOIN
			OpportunityItems OI
		ON
			OI.numOppId=OM.numOppId
		INNER JOIN
			WorkOrder WO
		ON
			OI.numoppitemtCode = WO.numOppItemID
		INNER JOIN
			WorkOrderDetails WOD
		ON
			WO.numWOId=WOD.numWOId
		INNER JOIN
			ProjectProgress PP
		ON
			OM.numOppId = PP.numOppId
		WHERE
			OM.tintOppType=1
			AND OM.tintOppStatus=0
			AND WO.numDomainID=@numDomainID
			AND WO.numWOStatus <> 23184 -- NOT COMPLETED
			AND WOD.numChildItemID = @numItemCode
			AND WOD.numWareHouseItemId=@numWarehouseItemID
			AND (OM.dtReleaseDate IS NOT NULL OR OI.ItemReleaseDate IS NOT NULL OR WO.bintCompliationDate IS NOT NULL)
			AND ISNULL(OI.ItemReleaseDate,OM.dtReleaseDate) <= DATEADD(DAY,@numForecastDays,DATEADD(MINUTE,-@ClientTimeZoneOffset,GETUTCDATE()))
			AND PP.intTotalProgress >= @numOpportunityPercentComplete
	END

	SELECT * FROM @TEMP ORDER BY dtCreatedDate
END
GO