GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'usp_ManageRMAInventory' ) 
    DROP PROCEDURE usp_ManageRMAInventory
GO
CREATE PROCEDURE [dbo].[usp_ManageRMAInventory]
    @numReturnHeaderID AS NUMERIC(9) = 0,
    @numDomainId NUMERIC(9),
    @numUserCntID AS NUMERIC(9)=0,
    @tintFlag AS TINYINT  --1: Receive 2: Revert
AS 
  BEGIN

	DECLARE @numOppItemID NUMERIC(18,0)
  	DECLARE @tintReturnType AS TINYINT
	DECLARE @tintReceiveType AS TINYINT
  	DECLARE @numReturnItemID AS NUMERIC
	DECLARE @itemcode AS NUMERIC
	DECLARE @numUnits as FLOAT
	DECLARE @numWareHouseItemID as numeric
	DECLARE @numWLocationID AS NUMERIC(18,0)
	DECLARE @monAmount as DECIMAL(20,5) 
	DECLARE @onHand AS FLOAT
	DECLARE @onOrder AS FLOAT
	DECLARE @onBackOrder AS FLOAT
	DECLARE @onAllocation AS FLOAT
	DECLARE @TotalOnHand FLOAT
	DECLARE @monItemAverageCost DECIMAL(20,5)
	DECLARE @monReturnAverageCost DECIMAL(20,5)
	DECLARE @monNewAverageCost DECIMAL(20,5)
	DECLARE @description AS VARCHAR(100)
	DECLARE @bitKitParent BIT
	DECLARE @bitKitChild BIT
    
  	SELECT 
		@tintReturnType=tintReturnType,
		@tintReceiveType=tintReceiveType
	FROM 
		dbo.ReturnHeader 
	WHERE 
		numReturnHeaderID=@numReturnHeaderID

	DECLARE @TEMPReturnItems TABLE
	(
		ID INT IDENTITY(1,1)
		,numReturnItemID NUMERIC(18,0)
		,numItemCode NUMERIC(18,0)
		,numUnitHourReceived FLOAT
		,numWareHouseItemID NUMERIC(18,0)
		,monTotAmount DECIMAL(20,5)
		,numOppItemID NUMERIC(18,0)
		,monAverageCost DECIMAL(20,5)
		,monReturnAverageCost DECIMAL(20,5)
		,bitKitParent BIT
		,bitKitChild BIT
	)

	INSERT INTO @TEMPReturnItems
	(
		numReturnItemID
		,numItemCode
		,numUnitHourReceived
		,numWareHouseItemID
		,monTotAmount
		,numOppItemID
		,monAverageCost
		,monReturnAverageCost
		,bitKitParent
		,bitKitChild
	)
  	SELECT
		numReturnItemID,
		RI.numItemCode,
		RI.numUnitHourReceived,
  		ISNULL(numWareHouseItemID,0),
		RI.monTotAmount,
		RI.numOppItemID
		,ISNULL(I.monAverageCost,0)
		,ISNULL(OBDI.monAverageCost,RI.monAverageCost)
		,ISNULL(I.bitKitParent,0)
		,0
	FROM 
		ReturnItems RI 
	JOIN 
		Item I 
	ON 
		RI.numItemCode=I.numItemCode
	LEFT JOIN
		OpportunityBizDocItems OBDI
	ON	
		RI.numOppBizDocItemID = OBDI.numOppBizDocItemID               
	WHERE 
		numReturnHeaderID=@numReturnHeaderID 
		AND (charitemtype='P' OR 1=(CASE 
									WHEN @tintReturnType=1 THEN 
										CASE 
											WHEN ISNULL(I.bitKitParent,0) = 1 AND ISNULL(I.bitAssembly,0) = 1 THEN 0
											WHEN ISNULL(I.bitKitParent,0) = 1 THEN 1
											ELSE 0 
										END 
									ELSE 
										0 
									END)) 
	ORDER BY 
		RI.numReturnItemID

	-- INSERT KIT CHILD ITEMS
	INSERT INTO @TEMPReturnItems
	(
		numReturnItemID
		,numItemCode
		,numUnitHourReceived
		,numWareHouseItemID
		,monTotAmount
		,numOppItemID
		,monAverageCost
		,monReturnAverageCost
		,bitKitParent
		,bitKitChild
	)
  	SELECT
		numReturnItemID,
		RI.numItemCode,
		RI.numUnitHourReceived * ISNULL(OKI.numQtyItemsReq_Orig,0),
  		ISNULL((SELECT TOP 1 WIInner.numWareHouseItemID FROM WareHouseItems WIInner WHERE WIInner.numItemID=I.numItemCode AND WIInner.numWareHouseID = WI.numWareHouseID AND ISNULL(WIInner.numWLocationID,0) = ISNULL(WI.numWLocationID,0)),0),
		RI.monTotAmount,
		RI.numOppItemID
		,ISNULL(I.monAverageCost,0)
		,ISNULL(OKI.monAvgCost,0)
		,ISNULL(I.bitKitParent,0)
		,1
	FROM
		ReturnItems RI
	INNER JOIN
		WareHouseItems WI
	ON
		RI.numWareHouseItemID = WI.numWareHouseItemID
	INNER JOIN
		OpportunityKitItems OKI
	ON
		RI.numOppItemID = OKI.numOppItemID
	INNER JOIN
		Item I
	ON
		OKI.numChildItemID = I.numItemCode
	WHERE
		RI.numReturnHeaderID=@numReturnHeaderID 
		AND ISNULL(RI.numOppItemID,0) > 0
		AND ISNULL(OKI.numWareHouseItemId,0) > 0

	INSERT INTO @TEMPReturnItems
	(
		numReturnItemID
		,numItemCode
		,numUnitHourReceived
		,numWareHouseItemID
		,monTotAmount
		,numOppItemID
		,monAverageCost
		,monReturnAverageCost
		,bitKitParent
		,bitKitChild
	)
	SELECT
		numReturnItemID,
		RI.numItemCode,
		RI.numUnitHourReceived * ISNULL(OKI.numQtyItemsReq_Orig,0) * ISNULL(OKCI.numQtyItemsReq_Orig,0),
  		ISNULL((SELECT TOP 1 WIInner.numWareHouseItemID FROM WareHouseItems WIInner WHERE WIInner.numItemID=I.numItemCode AND WIInner.numWareHouseID = WI.numWareHouseID AND ISNULL(WIInner.numWLocationID,0) = ISNULL(WI.numWLocationID,0)),0),
		RI.monTotAmount,
		RI.numOppItemID
		,ISNULL(I.monAverageCost,0)
		,ISNULL(OKCI.monAvgCost,0)
		,ISNULL(I.bitKitParent,0)
		,1
	FROM
		ReturnItems RI
	INNER JOIN
		WareHouseItems WI
	ON
		RI.numWareHouseItemID = WI.numWareHouseItemID
	INNER JOIN
		OpportunityKitChildItems OKCI
	ON
		RI.numOppItemID = OKCI.numOppItemID
	INNER JOIN
		OpportunityKitItems OKI
	ON
		OKCI.numOppChildItemID = OKI.numOppChildItemID
	INNER JOIN
		Item I
	ON
		OKCI.numItemID = I.numItemCode
	WHERE
		RI.numReturnHeaderID=@numReturnHeaderID 
		AND ISNULL(RI.numOppItemID,0) > 0
		AND ISNULL(OKCI.numWareHouseItemId,0) > 0

	DECLARE @k INT = 1
	DECLARE @kCount INT 

	SELECT @kCount = COUNT(*) FROM @TEMPReturnItems

	WHILE @k <= @kCount
	BEGIN
		SELECT TOP 1 
			@numReturnItemID=numReturnItemID,
			@itemcode=numItemCode,
			@numUnits=numUnitHourReceived,
  			@numWareHouseItemID=ISNULL(numWareHouseItemID,0),
			@monAmount=monTotAmount,
			@numOppItemID=numOppItemID
			,@monItemAverageCost=ISNULL(monAverageCost,0)
			,@monReturnAverageCost=ISNULL(monReturnAverageCost,0)
			,@bitKitParent=ISNULL(bitKitParent,0)
			,@bitKitChild=ISNULL(bitKitChild,0)
		FROM 
			@TEMPReturnItems
		WHERE
			ID = @k
		
		IF ISNULL(@bitKitParent,0) = 0
		BEGIN
			IF @numWareHouseItemID > 0
			BEGIN
				SELECT @TotalOnHand=SUM(ISNULL(numOnHand,0)) + SUM(ISNULL(numAllocation,0)) FROM dbo.WareHouseItems WHERE numItemID=@itemcode
		             
				SELECT  
					@onHand = ISNULL(numOnHand, 0),
					@onAllocation = ISNULL(numAllocation, 0),
					@onOrder = ISNULL(numOnOrder, 0),
					@onBackOrder = ISNULL(numBackOrder, 0),
					@numWLocationID=ISNULL(numWLocationID,0)
				FROM 
					WareHouseItems
				WHERE 
					numWareHouseItemID = @numWareHouseItemID 
			
				--Receive : SalesReturn 
				IF (@tintFlag=1 AND @tintReturnType=1)
				BEGIN
					SET @description=CONCAT('Sales Return Receive',(CASE WHEN ISNULL(@bitKitChild,0) = 1 THEN ' Kit Child ' ELSE ' ' END),'(Qty:',@numUnits,')')
										
					IF @onBackOrder >= @numUnits 
					BEGIN            
						SET @onBackOrder = @onBackOrder - @numUnits            
						SET @onAllocation = @onAllocation + @numUnits            
					END            
					ELSE 
					BEGIN            
						SET @onAllocation = @onAllocation + @onBackOrder            
						SET @numUnits = @numUnits - @onBackOrder            
						SET @onBackOrder = 0            
						SET @onHand = @onHand + @numUnits            
					END      
				
					IF @TotalOnHand + @numUnits > 0
					BEGIN
						SET @monNewAverageCost = ((@TotalOnHand * @monItemAverageCost) + (@numUnits * @monReturnAverageCost)) / (@TotalOnHand + @numUnits)
					END
					ELSE
					BEGIN
						SET @monNewAverageCost = @monItemAverageCost
					END
		                            
					UPDATE 
						Item
					SET 
						monAverageCost = (CASE WHEN ISNULL(bitVirtualInventory,0) = 1 THEN 0 ELSE @monNewAverageCost END)
					WHERE 
						numItemCode = @itemcode   
				END
				--Revert : SalesReturn
				ELSE IF (@tintFlag=2 AND @tintReturnType=1)
				BEGIN
					SET @description=CONCAT('Sales Return Delete',(CASE WHEN ISNULL(@bitKitChild,0) = 1 THEN ' Kit Child ' ELSE ' ' END),'(Qty:',@numUnits,')')
					
					IF @onHand  - @numUnits >= 0
					BEGIN						
						SET @onHand = @onHand - @numUnits
					END
					ELSE IF (@onHand + @onAllocation)  - @numUnits >= 0
					BEGIN						
						SET @numUnits = @numUnits - @onHand
						SET @onHand = 0 
                            
						SET @onBackOrder = @onBackOrder + @numUnits
						SET @onAllocation = @onAllocation - @numUnits  
					END	
					ELSE IF  (@onHand + @onBackOrder + @onAllocation) - @numUnits >= 0
					BEGIN
						SET @numUnits = @numUnits - @onHand
						SET @onHand = 0 
                            
						SET @numUnits = @numUnits - @onAllocation  
						SET @onAllocation = 0 
                            
						SET @onBackOrder = @onBackOrder + @numUnits
					END

					IF @TotalOnHand - @numUnits > 0
					BEGIN
						SET @monNewAverageCost = ((@TotalOnHand * @monItemAverageCost) - (@numUnits * @monReturnAverageCost)) / (@TotalOnHand - @numUnits)
					END
					ELSE
					BEGIN
						SET @monNewAverageCost = @monItemAverageCost
					END
				                    
					UPDATE 
						Item
					SET 
						monAverageCost = (CASE WHEN ISNULL(bitVirtualInventory,0) = 1 THEN 0 ELSE @monNewAverageCost END)
					WHERE 
						numItemCode = @itemcode
				END
				--Revert : PurchaseReturn
				ELSE IF (@tintFlag=2 AND @tintReturnType=2) 
				BEGIN
					DECLARE @numQtyReturnRemainingToDelete FLOAT
					SET @numQtyReturnRemainingToDelete = @numUnits
				
					IF EXISTS (SELECT ID FROM OpportunityItemsReceievedLocationReturn WHERE numReturnID=@numReturnHeaderID AND numReturnItemID = @numReturnItemID)
					BEGIN
						-- DELETE RETURN QTY
						DECLARE @TEMPReturn TABLE
						(
							ID INT,
							numWarehouseItemID NUMERIC(18,0),
							numReturnedQty FLOAT
						)

						INSERT INTO @TEMPReturn
						(
							ID,
							numWarehouseItemID,
							numReturnedQty
						)
						SELECT
							ROW_NUMBER() OVER(ORDER BY numWarehouseItemID)
							,numWarehouseItemID
							,SUM(numReturnedQty)
						FROM
							OpportunityItemsReceievedLocationReturn OITLR
						INNER JOIN
							OpportunityItemsReceievedLocation OIRL
						ON
							OITLR.numOIRLID = OIRL.ID
						WHERE
							numReturnID=@numReturnHeaderID
							AND numReturnItemID = @numReturnItemID
						GROUP BY
							numWarehouseItemID

						DECLARE @i AS INT = 1
						DECLARE @iCOUNT AS INT
						DECLARE @numQtyReturned FLOAT
						DECLARE @numTempReturnWarehouseItemID NUMERIC(18,0)

						SELECT @iCOUNT = COUNT(*) FROM @TEMPReturn
							
						WHILE @i <= @iCOUNT
																																			BEGIN
						SELECT 
							@numTempReturnWarehouseItemID=numWarehouseItemID,
							@numQtyReturned=numReturnedQty
						FROM 
							@TEMPReturn
						WHERE
							ID = @i
						
						-- INCREASE THE OnHand Of Destination Location
						UPDATE
							WareHouseItems
						SET
							numBackOrder = (CASE WHEN numBackOrder >= @numQtyReturned THEN ISNULL(numBackOrder,0) - @numQtyReturned ELSE 0 END),         
							numAllocation = (CASE WHEN numBackOrder >= @numQtyReturned THEN ISNULL(numAllocation,0) + @numQtyReturned ELSE ISNULL(numAllocation,0) + ISNULL(numBackOrder,0) END),
							numOnHand = (CASE WHEN numBackOrder >= @numQtyReturned THEN numOnHand ELSE ISNULL(numOnHand,0) + @numQtyReturned - ISNULL(numBackOrder,0) END),
							dtModified = GETDATE()
						WHERE
							numWareHouseItemID=@numTempReturnWarehouseItemID

						SET @numQtyReturnRemainingToDelete = ISNULL(@numQtyReturnRemainingToDelete,0) - ISNULL(@numQtyReturned,0)

						DECLARE @descriptionInner VARCHAR(MAX)=CONCAT('Purchase Return Delete (Qty:',@numQtyReturned,')')

						EXEC dbo.USP_ManageWareHouseItems_Tracking
							@numWareHouseItemID = @numTempReturnWarehouseItemID, --  numeric(9, 0)
							@numReferenceID = @numReturnHeaderID, --  numeric(9, 0)
							@tintRefType = 5, --  tinyint
							@vcDescription = @descriptionInner, --  varchar(100)
							@numModifiedBy = @numUserCntID,
							@numDomainId = @numDomainId

						SET @i = @i + 1
					END

						DELETE FROM OpportunityItemsReceievedLocationReturn WHERE numReturnID=@numReturnHeaderID AND numReturnItemID = @numReturnItemID
					END

					IF ISNULL(@numQtyReturnRemainingToDelete,0) > 0
					BEGIN
						SET @description=CONCAT('Purchase Return Delete',(CASE WHEN ISNULL(@bitKitChild,0) = 1 THEN ' Kit Child ' ELSE ' ' END),'(Qty:',@numQtyReturnRemainingToDelete,')')

						IF @onBackOrder >= @numQtyReturnRemainingToDelete 
						BEGIN            
							SET @onBackOrder = @onBackOrder - @numQtyReturnRemainingToDelete            
							SET @onAllocation = @onAllocation + @numQtyReturnRemainingToDelete            
						END            
						ELSE 
						BEGIN            
							SET @onAllocation = @onAllocation + @onBackOrder            
							SET @numQtyReturnRemainingToDelete = @numQtyReturnRemainingToDelete - @onBackOrder            
							SET @onBackOrder = 0            
							SET @onHand = @onHand + @numQtyReturnRemainingToDelete            
						END
					END

					IF @TotalOnHand + @numUnits > 0
					BEGIN
						SET @monNewAverageCost = ((@TotalOnHand * @monItemAverageCost) + (@numUnits * @monReturnAverageCost)) / (@TotalOnHand + @numUnits)
					END
					ELSE
					BEGIN
						SET @monNewAverageCost = @monItemAverageCost
					END
				               
					UPDATE 
						Item
					SET 
						monAverageCost = (CASE WHEN ISNULL(bitVirtualInventory,0) = 1 THEN 0 ELSE @monNewAverageCost END)
					WHERE 
						numItemCode = @itemcode
				END
				--Receive : PurchaseReturn 
				ELSE IF (@tintFlag=1 AND @tintReturnType=2) 
				BEGIN
					DECLARE @numRemainingQtyToReturn AS FLOAT = @numUnits

					-- FIRST TRY TO RETURN QTY FROM OTHER WAREHOUSES WHERE PURCHASE ORDER IS RECIEVED
					IF EXISTS (SELECT ID FROM OpportunityItemsReceievedLocation OIRL WHERE numOppItemID=@numOppItemID AND OIRL.numUnitReceieved - ISNULL((SELECT SUM(numReturnedQty) FROM OpportunityItemsReceievedLocationReturn WHERE numOIRLID = OIRL.ID),0) >= 0)
					BEGIN
						DECLARE @TEMP TABLE
						(
							ID INT,
							numOIRLID NUMERIC(18,0),
							numWarehouseItemID NUMERIC(18,0),
							numUnitReceieved FLOAT,
							numReturnedQty FLOAT
						)

						INSERT INTO 
							@TEMP
						SELECT 
							ROW_NUMBER() OVER(ORDER BY OIRL.ID),
							OIRL.ID,
							OIRL.numWarehouseItemID,
							OIRL.numUnitReceieved,
							ISNULL((SELECT SUM(numReturnedQty) FROM OpportunityItemsReceievedLocationReturn WHERE numOIRLID = OIRL.ID),0)
						FROM
							OpportunityItemsReceievedLocation OIRL
						WHERE 
							numOppItemID=@numOppItemID
							AND (OIRL.numUnitReceieved - ISNULL((SELECT SUM(numReturnedQty) FROM OpportunityItemsReceievedLocationReturn WHERE numOIRLID = OIRL.ID),0)) > 0
						ORDER BY
							(OIRL.numUnitReceieved - ISNULL((SELECT SUM(numReturnedQty) FROM OpportunityItemsReceievedLocationReturn WHERE numOIRLID = OIRL.ID),0)) DESC

						DECLARE @numTempOnHand AS FLOAT
						DECLARE @numTempOIRLID NUMERIC(18,0)
						DECLARE @numTempWarehouseItemID	NUMERIC(18,0)
						DECLARE @numRemaingUnits FLOAT

						DECLARE @j AS INT = 1
						DECLARE @jCOUNT AS INT

						SELECT @jCOUNT = COUNT(*) FROM @TEMP
							
						WHILE @j <= @jCOUNT AND @numRemainingQtyToReturn > 0
																																																																																							BEGIN
						SELECT 
							@numTempOIRLID=numOIRLID,
							@numTempWarehouseItemID=T1.numWarehouseItemID,
							@numTempOnHand = ISNULL(WI.numOnHand,0),
							@numRemaingUnits=ISNULL(numUnitReceieved,0)-ISNULL(numReturnedQty,0)
						FROM
							@TEMP T1
						INNER JOIN
							WareHouseItems WI
						ON
							T1.numWarehouseItemID=WI.numWareHouseItemID
						WHERE
							ID = @j

						IF @numTempOnHand >= @numRemaingUnits
						BEGIN
							UPDATE  
								WareHouseItems
							SET 
								numOnHand = ISNULL(numOnHand,0) - (CASE WHEN @numRemaingUnits >= @numRemainingQtyToReturn THEN @numRemainingQtyToReturn ELSE @numRemaingUnits END),
								dtModified = GETDATE() 
							WHERE 
								numWareHouseItemID = @numTempWarehouseItemID 

							INSERT INTO OpportunityItemsReceievedLocationReturn
							(
								numOIRLID
								,[numReturnID]
								,[numReturnItemID]
								,[numReturnedQty]
							)
							VALUES
							(
								@numTempOIRLID
								,@numReturnHeaderID
								,@numReturnItemID
								,(CASE WHEN @numRemaingUnits >= @numRemainingQtyToReturn THEN @numRemainingQtyToReturn ELSE @numRemaingUnits END)
							)

							SET @descriptionInner = CONCAT('Purchase Return Receive (Qty:',(CASE WHEN @numRemaingUnits >= @numRemainingQtyToReturn THEN @numRemainingQtyToReturn ELSE @numRemaingUnits END),')')

							EXEC dbo.USP_ManageWareHouseItems_Tracking
								@numWareHouseItemID = @numTempWarehouseItemID, --  numeric(9, 0)
								@numReferenceID = @numReturnHeaderID, --  numeric(9, 0)
								@tintRefType = 5, --  tinyint
								@vcDescription = @descriptionInner, --  varchar(100)
								@numModifiedBy = @numUserCntID,
								@numDomainId = @numDomainId

							SET @numRemainingQtyToReturn = @numRemainingQtyToReturn - (CASE WHEN @numRemaingUnits >= @numRemainingQtyToReturn THEN @numRemainingQtyToReturn ELSE @numRemaingUnits END)
						END
						ELSE IF @numTempOnHand > 0
						BEGIN
							UPDATE  
								WareHouseItems
							SET 
								numOnHand = numOnHand - (CASE WHEN @numTempOnHand >= @numRemainingQtyToReturn THEN @numRemainingQtyToReturn ELSE @numTempOnHand END),
								dtModified = GETDATE() 
							WHERE 
								numWareHouseItemID = @numTempWarehouseItemID 

							INSERT INTO OpportunityItemsReceievedLocationReturn
							(
								numOIRLID
								,[numReturnID]
								,[numReturnItemID]
								,[numReturnedQty]
							)
							VALUES
							(
								@numTempOIRLID
								,@numReturnHeaderID
								,@numReturnItemID
								,(CASE WHEN @numTempOnHand >= @numRemainingQtyToReturn THEN @numRemainingQtyToReturn ELSE @numTempOnHand END)
							)

							SET @descriptionInner =CONCAT('Purchase Return Receive (Qty:',(CASE WHEN @numTempOnHand >= @numRemainingQtyToReturn THEN @numRemainingQtyToReturn ELSE @numTempOnHand END),')')

							EXEC dbo.USP_ManageWareHouseItems_Tracking
								@numWareHouseItemID = @numTempWarehouseItemID, --  numeric(9, 0)
								@numReferenceID = @numReturnHeaderID, --  numeric(9, 0)
								@tintRefType = 5, --  tinyint
								@vcDescription = @descriptionInner, --  varchar(100)
								@numModifiedBy = @numUserCntID,
								@numDomainId = @numDomainId

							SET @numRemainingQtyToReturn = @numRemainingQtyToReturn - (CASE WHEN @numTempOnHand >= @numRemainingQtyToReturn THEN @numRemainingQtyToReturn ELSE @numTempOnHand END)
						END

						SET @j = @j + 1
					END
					END

					IF @numRemainingQtyToReturn > 0
																BEGIN
					SET @description=CONCAT('Purchase Return Receive',(CASE WHEN ISNULL(@bitKitChild,0) = 1 THEN ' Kit Child ' ELSE ' ' END),'(Qty:',@numRemainingQtyToReturn,')')

					IF @onHand  - @numRemainingQtyToReturn >= 0
					BEGIN						
						SET @onHand = @onHand - @numRemainingQtyToReturn
					END
					ELSE
					BEGIN
						RAISERROR ( 'PurchaseReturn_Qty', 16, 1 ) ;
						RETURN ;
					END
				END

					IF @TotalOnHand - @numUnits > 0
							BEGIN
					SET @monNewAverageCost = ((@TotalOnHand * @monItemAverageCost) - (@numUnits * @monReturnAverageCost)) / (@TotalOnHand - @numUnits)
		        END
					ELSE
					BEGIN
						SET @monNewAverageCost = @monItemAverageCost
					END
				              
					UPDATE 
						Item
					SET 
						monAverageCost = (CASE WHEN ISNULL(bitVirtualInventory,0) = 1 THEN 0 ELSE @monNewAverageCost END)
					WHERE 
						numItemCode = @itemcode
				END 

				IF @tintReturnType=1 OR (@tintReturnType=2 AND @numWareHouseItemID > 0 AND (ISNULL(@numRemainingQtyToReturn,0) > 0 OR ISNULL(@numQtyReturnRemainingToDelete,0) > 0))
				BEGIN
					UPDATE  
						WareHouseItems
					SET 
						numOnHand = @onHand,
						numAllocation = @onAllocation,
						numBackOrder = @onBackOrder,
						numOnOrder = @onOrder,
						dtModified = GETDATE() 
					WHERE 
						numWareHouseItemID = @numWareHouseItemID 

					DECLARE @tintRefType AS TINYINT;SET @tintRefType=5
					  
					EXEC dbo.USP_ManageWareHouseItems_Tracking
					@numWareHouseItemID = @numWareHouseItemID, --  numeric(9, 0)
					@numReferenceID = @numReturnHeaderID, --  numeric(9, 0)
					@tintRefType = @tintRefType, --  tinyint
					@vcDescription = @description, --  varchar(100)
					@numModifiedBy = @numUserCntID,
					@numDomainId = @numDomainId
				END         
			END
			ELSE
			BEGIN
				RAISERROR('MISSING_WAREHOUSE',16,1)
			END
		END

		SET @k = @k + 1
	END
END
GO
