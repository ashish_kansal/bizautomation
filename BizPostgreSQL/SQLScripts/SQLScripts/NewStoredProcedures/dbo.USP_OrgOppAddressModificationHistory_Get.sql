GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj      
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_OrgOppAddressModificationHistory_Get')
DROP PROCEDURE USP_OrgOppAddressModificationHistory_Get
GO
CREATE PROCEDURE [dbo].[USP_OrgOppAddressModificationHistory_Get] 
(
	@numDomainID NUMERIC(18,0)
	,@numRecordID NUMERIC(18,0)
	,@numAddressID NUMERIC(18,0)
	,@tintAddressOf TINYINT
	,@tintAddressType TINYINT
	,@ClientTimeZoneOffset INT
)
AS
BEGIN
	SELECT
		dbo.fn_GetContactName(numModifiedBy) AS vcModifiedBy
		,dbo.FormatedDateTimeFromDate(DATEADD(MINUTE,-@ClientTimeZoneOffset,dtModifiedDate),@numDomainID) dtModifiedDate
	FROM
		OrgOppAddressModificationHistory
	WHERE
		numRecordID=@numRecordID 
		AND tintAddressOf=@tintAddressOf 
		AND tintAddressType=@tintAddressType
		AND ISNULL(@numAddressID,0) = 0
END