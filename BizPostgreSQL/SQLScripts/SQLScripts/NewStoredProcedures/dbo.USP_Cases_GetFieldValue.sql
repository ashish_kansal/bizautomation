GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_Cases_GetFieldValue')
DROP PROCEDURE USP_Cases_GetFieldValue
GO
CREATE PROCEDURE [dbo].[USP_Cases_GetFieldValue]                                   
(                                                   
	@numDomainID NUMERIC(18,0),    
	@numCaseId NUMERIC(18,0),
	@vcFieldName VARCHAR(100)                           
)                                    
as
BEGIN
	IF @vcFieldName = 'numDivisionID'
	BEGIN
		SELECT numDivisionID FROM Cases WHERE numDomainId=@numDomainID AND numCaseId=@numCaseId
	END
	ELSE
	BEGIN
		RAISERROR('FIELD_NOT_FOUND',16,1)
	END
END
GO