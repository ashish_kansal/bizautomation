/****** Object:  StoredProcedure [dbo].[USP_GetTimeAndExpense]    Script Date: 07/26/2008 16:18:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj    
-- SELECT TOP 1 * FROM [TimeAndExpense] ORDER BY [numCategoryHDRID] DESC 
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ApprovalProcess_Transaction')
DROP PROCEDURE USP_ApprovalProcess_Transaction
GO
CREATE PROCEDURE [dbo].[USP_ApprovalProcess_Transaction]
	@chrAction char(10)=null,
	@numDomainID AS NUMERIC(9) = 0,
    @numUserCntID AS NUMERIC(9) = 0,
    @numModuleId AS NUMERIC(9) = 0,
    @numRecordId AS NUMERIC(9) = 0,
    @numApprovedBy AS NUMERIC(9) = 0,
	@numApprovalComplete AS NUMERIC(9)=0,
	
	@vchoutput varchar(100)=null OUTPUT
AS
BEGIN 
	DECLARE @numRUserId AS NUMERIC
	DECLARE @num1stApproval AS NUMERIC
	DECLARE @num2ndApproval AS NUMERIC
	DECLARE @num3rdApproval AS NUMERIC
	DECLARE @num4thApproval AS NUMERIC
	DECLARE @num5thApproval AS NUMERIC

	IF(@chrAction='UT')--Time Expense Approval
	BEGIN
		UPDATE
			TimeAndExpense
		SET
			numApprovalComplete=@numApprovalComplete
		WHERE
			numCategoryHDRID=@numRecordId
		INSERT INTO Approval_transaction_log
			(numDomainID,numModuleId,numRecordId,numApprovedBy,dtmApprovedOn)
		VALUES
			(@numDomainID,@numModuleId,@numRecordId,@numApprovedBy,GETDATE())
	END

	IF(@chrAction='UTP')--Promotion Approval
	BEGIN
		UPDATE
			OpportunityMaster
		SET
			intPromotionApprovalStatus=@numApprovalComplete
		WHERE
			numOppId=@numRecordId
		IF(@numApprovalComplete=0)
		BEGIN
				DECLARE @DealStatus AS NUMERIC(18,0)  
				DECLARE @tintOppStatus AS tinyint
				DECLARE @tintOppType AS tinyint
				SET @DealStatus=(SELECT TOP 1 intChangePromotionStatus FROM OpportunityMaster WHERE numOppId=@numRecordId)    
				UPDATE OpportunityMaster SET tintOppStatus = @DealStatus WHERE numOppId=@numRecordId
				declare @tintShipped as tinyint 
				DECLARE @numDivisionID AS NUMERIC(18,0)  
				DECLARE @tintCommitAllocation TINYINT
				      
				select @tintOppStatus=tintOppStatus,@tintOppType=tintOppType,@tintShipped=tintShipped,@tintCommitAllocation=ISNULL(tintCommitAllocation,1) from OpportunityMaster INNER JOIN Domain ON OpportunityMaster.numDomainId=Domain.numDomainId where numOppID=@numRecordId              
				if @tintOppStatus=1 AND @tintCommitAllocation=1         
				begin         
					exec USP_UpdatingInventoryonCloseDeal @numRecordId,0,@numUserCntID
				end  
				declare @tintCRMType as numeric(9)        
				declare @AccountClosingDate as datetime        

				select @numDivisionID=numDivisionID,@AccountClosingDate=bintAccountClosingDate from  OpportunityMaster where numOppID=@numRecordId         
				select @tintCRMType=tintCRMType from divisionmaster where numDivisionID =@numDivisionID 

				--When deal is won                                         
				if @DealStatus = 1 
				begin           
					 if @AccountClosingDate is null                   
					  update OpportunityMaster set bintAccountClosingDate=getutcdate() where numOppID=@numRecordId              
				end         
				--When Deal is Lost
				else if @DealStatus = 2
				begin         
					 select @AccountClosingDate=bintAccountClosingDate from  OpportunityMaster where numOppID=@numRecordId         
					 if @AccountClosingDate is null                   
					  update OpportunityMaster set bintAccountClosingDate=getutcdate() where numOppID=@numRecordId
				end                    

				/*Added by chintan -  Biz usually automatically promotes the record from Lead/Prospect to Account When a Sales Opp/Order is made against it*/  
				-- Promote Lead to Prospect when Sales/Purchase Opp is created against it
				if @tintCRMType=0 AND @DealStatus = 0 --Lead & Open Opp
				begin        
					update divisionmaster set tintCRMType=1,bintLeadProm=getutcdate(),bintLeadPromBy=@numUserCntID,bintProsProm=getutcdate(),bintProsPromBy=@numUserCntID        
					where numDivisionID=@numDivisionID        
				end        
				-- Promote Lead to Account when Sales/Purchase Order is created against it
				ELSE if @tintCRMType=0 AND @DealStatus = 1 --Lead & Order
				begin        
					update divisionmaster set tintCRMType=2,bintLeadProm=getutcdate(),bintLeadPromBy=@numUserCntID,bintProsProm=getutcdate(),bintProsPromBy=@numUserCntID        
					where numDivisionID=@numDivisionID        
				end
				--Promote Prospect to Account
				else if @tintCRMType=1 AND @DealStatus = 1 
				begin        
					update divisionmaster set tintCRMType=2,bintProsProm=getutcdate(),bintProsPromBy=@numUserCntID        
					where numDivisionID=@numDivisionID        
				end
		END
		INSERT INTO Approval_transaction_log
			(numDomainID,numModuleId,numRecordId,numApprovedBy,dtmApprovedOn)
		VALUES
			(@numDomainID,@numModuleId,@numRecordId,@numApprovedBy,GETDATE())
	END

	IF(@chrAction='CF')
	BEGIN
		IF(@numRecordId>0)
		BEGIN
			SET @numRUserId=(SELECT TOP 1 numUserCntID FROM TimeAndExpense WHERE numCategoryHDRID=@numRecordId)
		END
		ELSE
		BEGIN
			SET @numRUserId=@numUserCntID
		END
		
		SELECT TOP 1 
			@num1stApproval=numLevel1Authority,
			@num2ndApproval=numLevel2Authority,
			@num3rdApproval=numLevel3Authority,
			@num4thApproval=numLevel4Authority,
			@num5thApproval=numLevel5Authority 
		FROM 
			ApprovalConfig 
		WHERE 
			numUserId=@numRUserId AND numModule=1
		
		IF(ISNULL(@num1stApproval,0)=0 and ISNULL(@num2ndApproval,0)=0 and ISNULL(@num3rdApproval,0)=0 and ISNULL(@num4thApproval,0)=0 and ISNULL(@num5thApproval,0)=0)
		BEGIN
			SET @vchoutput='INVALID'
		END
		ELSE
		BEGIN
			SET @vchoutput='VALID'
		END
	END

	IF(@chrAction='CH')
	BEGIN
		IF(@numRecordId>0)
		BEGIN
			SET @numRUserId=(SELECT TOP 1 numUserCntID FROM TimeAndExpense WHERE numCategoryHDRID=@numRecordId)
		END
		ELSE
		BEGIN
			SET @numRUserId=@numUserCntID
		END
		SELECT TOP 1 
			@num1stApproval=numLevel1Authority,
			@num2ndApproval=numLevel2Authority,
			@num3rdApproval=numLevel3Authority,
			@num4thApproval=numLevel4Authority,
			@num5thApproval=numLevel5Authority 
		FROM 
			ApprovalConfig 
		WHERE 
			numUserId=@numRUserId AND numModule=1
		
		IF(ISNULL(@num1stApproval,0)=0 and @numApprovalComplete=1)
		BEGIN
			SET @vchoutput='INVALID'
		END
		ELSE IF(ISNULL(@num2ndApproval,0)=0 and @numApprovalComplete=2)
		BEGIN
			SET @vchoutput='INVALID'
		END
		ELSE IF(ISNULL(@num3rdApproval,0)=0 and @numApprovalComplete=3)
		BEGIN
			SET @vchoutput='INVALID'
		END
		ELSE IF(ISNULL(@num4thApproval,0)=0 and @numApprovalComplete=4)
		BEGIN
			SET @vchoutput='INVALID'
		END
		ELSE IF(ISNULL(@num5thApproval,0)=0 and @numApprovalComplete=5)
		BEGIN
			SET @vchoutput='INVALID'
		END
		ELSE
		BEGIN
			SET @vchoutput='VALID'
		END
	END

	IF(@chrAction='CHP')--Check Promotion Approver Authority
	BEGIN
		IF(@numRecordId>0)
		BEGIN
			SET @numRUserId=(SELECT TOP 1 numCreatedby FROM OpportunityMaster WHERE numOppId=@numRecordId)
		END
		ELSE
		BEGIN
			SET @numRUserId=@numUserCntID
		END
		SELECT TOP 1 
			@num1stApproval=numLevel1Authority,
			@num2ndApproval=numLevel2Authority,
			@num3rdApproval=numLevel3Authority,
			@num4thApproval=numLevel4Authority,
			@num5thApproval=numLevel5Authority 
		FROM 
			ApprovalConfig 
		WHERE 
			numUserId=@numRUserId AND numModule=2
		
		IF(ISNULL(@num1stApproval,0)=0 and @numApprovalComplete=1)
		BEGIN
			SET @vchoutput='INVALID'
		END
		ELSE IF(ISNULL(@num2ndApproval,0)=0 and @numApprovalComplete=2)
		BEGIN
			SET @vchoutput='INVALID'
		END
		ELSE IF(ISNULL(@num3rdApproval,0)=0 and @numApprovalComplete=3)
		BEGIN
			SET @vchoutput='INVALID'
		END
		ELSE IF(ISNULL(@num4thApproval,0)=0 and @numApprovalComplete=4)
		BEGIN
			SET @vchoutput='INVALID'
		END
		ELSE IF(ISNULL(@num5thApproval,0)=0 and @numApprovalComplete=5)
		BEGIN
			SET @vchoutput='INVALID'
		END
		ELSE
		BEGIN
			SET @vchoutput='VALID'
		END
	END



END
GO
