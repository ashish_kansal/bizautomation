GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ManageBizDocTemplate')
DROP PROCEDURE USP_ManageBizDocTemplate
GO
CREATE PROCEDURE [dbo].[USP_ManageBizDocTemplate]
    @numDomainID NUMERIC,
    @numBizDocID NUMERIC,
    @numOppType NUMERIC, 
    @txtBizDocTemplate TEXT,
    @txtCSS TEXT,
    @bitEnabled BIT,
    @tintTemplateType TINYINT,
	@numBizDocTempID NUMERIC,
	@vcTemplateName varchar(50)='',
	@bitDefault bit=0,
	@numOrientation int=1,
	@bitKeepFooterBottom bit = 0,
	@numRelationship NUMERIC(18,0) = 0,
	@numProfile NUMERIC(18,0) = 0,
	@bitDisplayKitChild AS BIT = 0,
	@numAccountClass AS NUMERIC(18,0) = 0
AS 
BEGIN TRY
	IF (ISNULL(@numRelationship,0) <> 0 OR ISNULL(@numProfile,0) <> 0 OR ISNULL(@numAccountClass,0) <> 0)
	BEGIN
		IF (SELECT 
				COUNT(numBizDocTempID) 
			FROM 
				BizDocTemplate 
			WHERE 
				[numDomainID] = @numDomainID 
				AND [numBizDocID] = @numBizDocID 
				AND [numOppType]=@numOppType 
				AND tintTemplateType=@tintTemplateType 
				AND (ISNULL(numProfile,0) > 0 OR ISNULL(numRelationship,0) > 0 OR ISNULL(numAccountClass,0) > 0) 
				AND ISNULL(numProfile,0)=ISNULL(@numProfile,0)
				AND numBizDocTempID <> @numBizDocTempID
				AND ISNULL(numRelationship,0)=ISNULL(@numRelationship,0)
				AND ISNULL(numAccountClass,0) = ISNULL(@numAccountClass,0)) > 0
		BEGIN
			RAISERROR('BIZDOC_RELATIONSHIP_PROFILE_CLASS_EXISTS',16,1)
		END
	END


	IF @tintTemplateType=0
	BEGIN
		IF @bitDefault=1
		BEGIN
			IF (select count(numBizDocTempID) from BizDocTemplate where [numDomainID] = @numDomainID AND [numBizDocID] = @numBizDocID AND [numOppType]=@numOppType AND tintTemplateType=@tintTemplateType and isnull(bitDefault,0)=1)>0
			BEGIN
				Update BizDocTemplate SET bitDefault=0 where [numDomainID] = @numDomainID AND [numBizDocID] = @numBizDocID AND [numOppType]=@numOppType AND tintTemplateType=@tintTemplateType and isnull(bitDefault,0)=1
			END
		END
		ELSE
		BEGIN
			IF (select count(numBizDocTempID) from BizDocTemplate where [numDomainID] = @numDomainID AND [numBizDocID] = @numBizDocID AND [numOppType]=@numOppType AND tintTemplateType=@tintTemplateType)=0
			BEGIN
				SET @bitDefault=1
			END
		END

		IF @numBizDocTempID>0
			BEGIN
				UPDATE  BizDocTemplate SET [txtBizDocTemplate] = @txtBizDocTemplate,[txtCSS] = @txtCSS,
					bitEnabled =@bitEnabled,vcTemplateName=@vcTemplateName,bitDefault=@bitDefault,numOrientation=@numOrientation,bitKeepFooterBottom=@bitKeepFooterBottom,
					[numBizDocID] = @numBizDocID,
					[numRelationship] = @numRelationship,
					[numProfile] = @numProfile,
					bitDisplayKitChild = @bitDisplayKitChild,
					numAccountClass=@numAccountClass
					WHERE numBizDocTempID = @numBizDocTempID 
			END
		ELSE
			BEGIN
				INSERT  INTO BizDocTemplate
                    ([numDomainID],[numBizDocID],[txtBizDocTemplate],[txtCSS],bitEnabled,[numOppType],tintTemplateType,vcTemplateName,bitDefault,numOrientation,bitKeepFooterBottom,numRelationship,numProfile,bitDisplayKitChild,numAccountClass)
				VALUES  (@numDomainID,@numBizDocID,@txtBizDocTemplate,@txtCSS,@bitEnabled,@numOppType,@tintTemplateType,@vcTemplateName,@bitDefault,@numOrientation,@bitKeepFooterBottom,@numRelationship,@numProfile,@bitDisplayKitChild,@numAccountClass)
            
				SELECT  SCOPE_IDENTITY() AS InsertedID
			END
	END
	ELSE
		BEGIN
		IF NOT EXISTS ( SELECT  * FROM    [BizDocTemplate] WHERE   numDomainID = @numDomainID AND numBizDocID = @numBizDocID AND [numOppType]=@numOppType AND tintTemplateType=@tintTemplateType) 
        BEGIN
            INSERT  INTO BizDocTemplate
                    (
                      [numDomainID],
                      [numBizDocID],
                      [txtBizDocTemplate],
                      [txtCSS],
					  bitEnabled,
					  [numOppType],tintTemplateType,numOrientation,bitKeepFooterBottom,numRelationship,numProfile,bitDisplayKitChild,numAccountClass
	              )
            VALUES  (
                      @numDomainID,
                      @numBizDocID,
                      @txtBizDocTemplate,
                      @txtCSS,
					  @bitEnabled,
					  @numOppType,@tintTemplateType,@numOrientation,@bitKeepFooterBottom,@numRelationship,@numProfile,@bitDisplayKitChild,@numAccountClass
	              )
            SELECT  SCOPE_IDENTITY() AS InsertedID
        END
    ELSE 
        BEGIN
            UPDATE  BizDocTemplate
            SET     
                    [txtBizDocTemplate] = @txtBizDocTemplate,
                    [txtCSS] = @txtCSS,
                    bitEnabled =@bitEnabled,
                    numOppType = @numOppType,tintTemplateType=@tintTemplateType,numOrientation=@numOrientation,bitKeepFooterBottom=@bitKeepFooterBottom,
					numRelationship = @numRelationship, numProfile = @numProfile, bitDisplayKitChild=@bitDisplayKitChild, numAccountClass=@numAccountClass
            WHERE   [numDomainID] = @numDomainID AND [numBizDocID] = @numBizDocID AND [numOppType]=@numOppType AND tintTemplateType=@tintTemplateType
        END
END
END TRY
BEGIN CATCH
	DECLARE @ErrorMessage NVARCHAR(4000)
	DECLARE @ErrorNumber INT
	DECLARE @ErrorSeverity INT
	DECLARE @ErrorState INT
	DECLARE @ErrorLine INT
	DECLARE @ErrorProcedure NVARCHAR(200)

	SELECT 
		@ErrorMessage = ERROR_MESSAGE(),
		@ErrorNumber = ERROR_NUMBER(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE(),
		@ErrorLine = ERROR_LINE(),
		@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

	RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);
END CATCH