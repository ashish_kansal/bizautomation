GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_BizAPIThrottleLog_Save' ) 
    DROP PROCEDURE USP_BizAPIThrottleLog_Save
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author: Sandeep Patel
-- Create date: 21 April 2014
-- Description:	Saves Biz API Throttle Log
-- =============================================
CREATE PROCEDURE USP_BizAPIThrottleLog_Save
	 @vcIP VARCHAR(50),
	 @vcBizAPIAccessKey VARCHAR(50),
	 @vcEndPoint VARCHAR(MAX),
	 @dtLogDate DATETIME
AS
BEGIN
	
	SET NOCOUNT ON;
	
	INSERT INTO BizAPIThrottleLog
	(
	 vcIP,
	 vcBizAPIAccessKey,
	 vcEndPoint,
	 dtLogDate
	)
	VALUES
	(
	 @vcIP,
	 @vcBizAPIAccessKey,
	 @vcEndPoint,
	 @dtLogDate
	)
	
	
END
GO