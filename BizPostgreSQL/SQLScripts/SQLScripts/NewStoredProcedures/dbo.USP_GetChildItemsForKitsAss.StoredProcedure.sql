/****** Object:  StoredProcedure [dbo].[USP_GetChildItemsForKitsAss]    Script Date: 07/26/2008 16:16:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
                    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetChildItemsForKitsAss')
DROP PROCEDURE USP_GetChildItemsForKitsAss
GO
CREATE PROCEDURE [dbo].[USP_GetChildItemsForKitsAss]                             
	@numKitId AS NUMERIC(18,0)                            
	,@numWarehouseItemID NUMERIC(18,0)
	,@bitApplyFilter BIT=0
AS                            
BEGIN

	WITH CTE
	(
		ID
		,numItemDetailID
		,numParentID
		,numItemKitID
		,numItemCode
		,vcItemName
		,vcSKU
		,bitKitParent
		,monAverageCost
		,numAssetChartAcntId
		,txtItemDesc
		,numQtyItemsReq
		,numOppChildItemID
		,charItemType
		,ItemType
		,StageLevel
		,monListPriceC
		,numBaseUnit
		,numCalculatedQty
		,numIDUOMId,sintOrder
		,numRowNumber
		,RStageLevel
		,numUOMQuantity
		,tintView
		,vcFields
		,bitUseInDynamicSKU
		,bitOrderEditable
	)
	AS
	(
		SELECT 
			CAST(CONCAT('#0#',ISNULL(Dtl.numItemDetailID,'0'),'#') AS VARCHAR(1000))
			,Dtl.numItemDetailID
			,CAST('' AS VARCHAR(1000))
			,convert(NUMERIC(18,0),0)
			,numItemCode
			,CONCAT(vcItemName,(CASE WHEN LEN(ISNULL(vcSKU,'')) > 0 THEN CONCAT(' (',vcSKU,')') ELSE '' END)) vcItemName
			,vcSKU
			,ISNULL(Item.bitKitParent,0)
			,(CASE WHEN ISNULL(bitVirtualInventory,0) = 1 THEN 0 ELSE monAverageCost END)
			,numAssetChartAcntId
			,ISNULL(Dtl.vcItemDesc,txtItemDesc) AS txtItemDesc
			,DTL.numQtyItemsReq
			,0 as numOppChildItemID
			,case when charItemType='P' then CONCAT('Inventory',CASE WHEN ISNULL(item.bitKitParent,0)=1 THEN ' (Kit)' WHEN ISNULL(item.bitAssembly,0)=1 THEN ' (Assembly)' WHEN ISNULL(item.numItemGroup,0) > 0 AND (ISNULL(item.bitMatrix,0)=1 OR EXISTS (SELECT numItemGroupDTL FROM ItemGroupsDTL WHERE numItemGroupID=item.numItemGroup AND tintType=2)) THEN ' (Matrix)' ELSE '' END) when charItemType='S' then 'Service' when charItemType='A' then 'Accessory' when charItemType='N' then 'Non-Inventory' end as charItemType
			,charItemType as ItemType                            
			,1
			,Item.monListPrice
			,ISNULL(numBaseUnit,0)
			,CAST(DTL.numQtyItemsReq AS FLOAT) AS numCalculatedQty
			,ISNULL(Dtl.numUOMId,ISNULL(numBaseUnit,0)) AS numIDUOMId
			,ISNULL(sintOrder,0) sintOrder
			,ROW_NUMBER() OVER (ORDER BY ISNULL(sintOrder,0)) AS numRowNumber
			,0
			,(DTL.numQtyItemsReq * ISNULL(dbo.fn_UOMConversion(DTL.numUOMID,item.numItemCode,item.numDomainID,item.numBaseUnit),1))
			,ISNULL(Dtl.tintView,1) tintView
			,ISNULL(Dtl.vcFields,'') vcFields
			,ISNULL(Dtl.bitUseInDynamicSKU,0) bitUseInDynamicSKU
			,ISNULL(Dtl.bitOrderEditable,0) bitOrderEditable
		FROM 
			Item                                
		INNER JOIN 
			ItemDetails Dtl 
		ON 
			numChildItemID=numItemCode
		WHERE 
			numItemKitID=@numKitId 
		UNION ALL
		SELECT 
			CAST(CONCAT(c.ID,'-#',(c.StageLevel + 1),'#',ISNULL(Dtl.numItemDetailID,'0'),'#') AS VARCHAR(1000))
			,Dtl.numItemDetailID
			,C.ID
			,Dtl.numItemKitID
			,i.numItemCode
			,CONCAT(i.vcItemName,(CASE WHEN LEN(ISNULL(i.vcSKU,'')) > 0 THEN CONCAT(' (',i.vcSKU,')') ELSE '' END)) vcItemName
			,i.vcSKU
			,ISNULL(i.bitKitParent,0)
			,(CASE WHEN ISNULL(i.bitVirtualInventory,0) = 1 THEN 0 ELSE i.monAverageCost END)
			,i.numAssetChartAcntId
			,ISNULL(Dtl.vcItemDesc,i.txtItemDesc) AS txtItemDesc
			,DTL.numQtyItemsReq
			,0 as numOppChildItemID
			,CASE WHEN i.charItemType='P' THEN CONCAT('Inventory',CASE WHEN ISNULL(i.bitKitParent,0)=1 THEN ' (Kit)' WHEN ISNULL(i.bitAssembly,0)=1 THEN ' (Assembly)' WHEN ISNULL(i.numItemGroup,0) > 0 AND (ISNULL(i.bitMatrix,0)=1 OR EXISTS (SELECT numItemGroupDTL FROM ItemGroupsDTL WHERE numItemGroupID=i.numItemGroup AND tintType=2)) THEN ' (Matrix)' ELSE '' END) when i.charItemType='S' then 'Service' when i.charItemType='A' then 'Accessory' when i.charItemType='N' then 'Non-Inventory' end as charItemType
			,i.charItemType as ItemType                            
			,c.StageLevel + 1
			,i.monListPrice,ISNULL(i.numBaseUnit,0),CAST((DTL.numQtyItemsReq * c.numCalculatedQty) AS FLOAT) AS numCalculatedQty
			,ISNULL(Dtl.numUOMId,ISNULL(i.numBaseUnit,0)) AS numIDUOMId
			,ISNULL(Dtl.sintOrder,0) sintOrder
			,ROW_NUMBER() OVER (ORDER BY ISNULL(Dtl.sintOrder,0)) AS numRowNumber
			,0
			,(DTL.numQtyItemsReq * ISNULL(dbo.fn_UOMConversion(DTL.numUOMID,i.numItemCode,i.numDomainID,i.numBaseUnit),1))
			,ISNULL(Dtl.tintView,1) tintView
			,ISNULL(Dtl.vcFields,'') vcFields
			,ISNULL(Dtl.bitUseInDynamicSKU,0) bitUseInDynamicSKU
			,ISNULL(Dtl.bitOrderEditable,0) bitOrderEditable
		FROM
			Item i                               
		INNER JOIN 
			ItemDetails Dtl
		ON 
			Dtl.numChildItemID=i.numItemCode
		INNER JOIN 
			CTE c 
		ON 
			Dtl.numItemKitID = c.numItemCode 
		WHERE 
			Dtl.numChildItemID!=@numKitId
	)

	SELECT * INTO #temp FROM CTE

	;WITH Final AS 
	(
		SELECT 
			*
			,1 AS RStageLevel1 
		FROM 
			#temp 
		WHERE 
			numitemcode NOT IN (SELECT numItemKitID FROM #temp)
		UNION ALL
		SELECT 
			t.*
			,c.RStageLevel1 + 1 AS RStageLevel1 
		FROM 
			#temp t 
		JOIN 
			Final c 
		ON 
			t.numitemcode=c.numItemKitID
	)


	UPDATE 
		t 
	SET 
		t.RStageLevel=f.RStageLevel 
	FROM 
		#temp t,(SELECT numitemcode,numItemKitID,MAX(RStageLevel1) AS RStageLevel FROM Final GROUP BY numitemcode,numItemKitID) f
	WHERE 
		t.numitemcode=f.numitemcode 
		AND t.numItemKitID=f.numItemKitID 


	IF ISNULL(@numWarehouseItemID,0) > 0
	BEGIN
		DECLARE @numWarehouseID NUMERIC(18,0)
		SELECT @numWarehouseID = numWareHouseID FROM WareHouseItems WHERE numWareHouseItemID=@numWarehouseItemID
		IF(@bitApplyFilter=0)
		BEGIN
		IF EXISTS 
		(
			SELECT
				c.numItemCode
			FROM
				#temp c 
			WHERE
				c.ItemType='P'
				AND (SELECT COUNT(*) FROM WareHouseItems WHERE numItemID = c.numItemCode AND numWareHouseID=@numWarehouseID) = 0
		)
		BEGIN
			RAISERROR('SELECTED_WAREHOUSE_IS_NOT_AVAILABLE_FOR_ALL_CHILD_ITEMS',16,1)
			RETURN
		END
		END

		SELECT DISTINCT 
			c.*
			,CASE WHEN isnull(WI.numWareHouseItemID,0)>0 THEN ISNULL(WI.[monWListPrice],0) ELSE c.monListPriceC END monListPrice
			,CONVERT(VARCHAR(30),CASE WHEN isnull(WI.numWareHouseItemID,0)>0 THEN ISNULL(WI.[monWListPrice],0) ELSE c.monListPriceC END)+'/'+isnull(UOM.vcUnitName,'Units') as UnitPrice
			,UOM.vcUnitName
			,ISNULL(UOM.numUOMId,0) numUOMId
			,WI.numItemID,vcWareHouse
			,IDUOM.vcUnitName AS vcIDUnitName
			,dbo.fn_UOMConversion(IDUOM.numUOMId,0,IDUOM.numDomainId,UOM.numUOMId) AS UOMConversionFactor
			,CAST(c.numQtyItemsReq * dbo.fn_UOMConversion(UOM.numUOMId,0,IDUOM.numDomainId,IDUOM.numUOMId) AS FLOAT) AS numConQty
			,(c.numQtyItemsReq * dbo.fn_UOMConversion(UOM.numUOMId,0,IDUOM.numDomainId,IDUOM.numUOMId)) * c.monAverageCost AS monAverageCostTotalForQtyRequired
			,ISNULL(WI.[numWareHouseItemID],0) AS numWarehouseItmsID
			,ISNULL(WI.[numWareHouseItemID],0) AS numWareHouseItemID
			,ISNULL(numOnHand,0) numOnHand
			,ISNULL(numOnHand,0) + isnull(numAllocation,0) AS numAvailable
			,isnull(numOnOrder,0) numOnOrder
			,isnull(numReorder,0) numReorder
			,isnull(numAllocation,0) numAllocation
			,isnull(numBackOrder,0) numBackOrder
		INTO
			#TEMPFinal
		FROM 
			#temp c  
		LEFT JOIN 
			UOM 
		ON 
			UOM.numUOMId=c.numBaseUnit
		LEFT JOIN 
			UOM IDUOM 
		ON 
			IDUOM.numUOMId=c.numIDUOMId
		OUTER APPLY
		(
			SELECT TOP 1
				numWareHouseItemID
				,numOnHand
				,numOnOrder
				,numReorder
				,numAllocation
				,numBackOrder
				,numItemID
				,vcWareHouse
				,monWListPrice
			FROM
				WareHouseItems
			LEFT JOIN
				Warehouses W 
			ON
				W.numWareHouseID=WareHouseItems.numWareHouseID
			WHERE 
				WareHouseItems.numItemID = c.numItemCode 
				AND WareHouseItems.numWareHouseID=@numWarehouseID
			ORDER BY
				WareHouseItems.numWareHouseID,numWareHouseItemID
		) WI
		ORDER BY 
			c.StageLevel

		UPDATE
			T
		SET
			T.numOnHand = ISNULL((SELECT MIN(FLOOR(TF.numOnHand/TF.numQtyItemsReq)) FROM #TEMPFinal TF WHERE TF.numParentID = T.ID),0)
			,T.numAvailable = ISNULL((SELECT MIN(FLOOR(TF.numAvailable/TF.numQtyItemsReq)) FROM #TEMPFinal TF WHERE TF.numParentID = T.ID),0)
			,T.numBackOrder = 0
			,T.numOnOrder=0
		FROM
			#TEMPFinal T
		WHERE
			ISNULL(T.bitKitParent,0) = 1

		SELECT * FROM #TEMPFinal

		DROP TABLE #TEMPFinal
	END
	ELSE IF @bitApplyFilter=1
	BEGIN
		SELECT DISTINCT 
			c.*
			,CASE WHEN isnull(WI.numWareHouseItemID,0)>0 THEN ISNULL(WI.[monWListPrice],0) ELSE c.monListPriceC END monListPrice
			,CONVERT(VARCHAR(30),CASE WHEN isnull(WI.numWareHouseItemID,0)>0 THEN ISNULL(WI.[monWListPrice],0) ELSE c.monListPriceC END)+'/'+isnull(UOM.vcUnitName,'Units') as UnitPrice
			,UOM.vcUnitName
			,ISNULL(UOM.numUOMId,0) numUOMId
			,WI.numItemID,vcWareHouse
			,IDUOM.vcUnitName AS vcIDUnitName
			,dbo.fn_UOMConversion(IDUOM.numUOMId,0,IDUOM.numDomainId,UOM.numUOMId) AS UOMConversionFactor
			,CAST(c.numQtyItemsReq * dbo.fn_UOMConversion(UOM.numUOMId,0,IDUOM.numDomainId,IDUOM.numUOMId) AS FLOAT) AS numConQty
			,(c.numQtyItemsReq * dbo.fn_UOMConversion(UOM.numUOMId,0,IDUOM.numDomainId,IDUOM.numUOMId)) * c.monAverageCost AS monAverageCostTotalForQtyRequired
			,ISNULL(WI.[numWareHouseItemID],0) AS numWarehouseItmsID
			,ISNULL(WI.[numWareHouseItemID],0) AS numWareHouseItemID
			,ISNULL(numOnHand,0) numOnHand
			,ISNULL(numOnHand,0) + isnull(numAllocation,0) AS numAvailable
			,isnull(numOnOrder,0) numOnOrder
			,isnull(numReorder,0) numReorder
			,isnull(numAllocation,0) numAllocation
			,isnull(numBackOrder,0) numBackOrder
		INTO
			#TEMPFinal1
		FROM 
			#temp c  
		LEFT JOIN 
			UOM 
		ON 
			UOM.numUOMId=c.numBaseUnit
		LEFT JOIN 
			UOM IDUOM 
		ON 
			IDUOM.numUOMId=c.numIDUOMId
		OUTER APPLY
		(
			SELECT TOP 1
				1 AS numWareHouseItemID
				,SUM(numOnHand) AS numOnHand
				,SUM(numOnOrder) AS numOnOrder
				,0 AS numReorder
				,0 AS numAllocation
				,0 AS numBackOrder
				,0 AS numItemID
				,'' AS vcWareHouse
				,0 AS monWListPrice
			FROM
				WareHouseItems
			LEFT JOIN
				Warehouses W 
			ON
				W.numWareHouseID=WareHouseItems.numWareHouseID
			WHERE 
				WareHouseItems.numItemID = c.numItemCode 
			ORDER BY
				numWareHouseItemID
			
		) WI
		ORDER BY 
			c.StageLevel

		UPDATE
			T
		SET
			T.numOnHand = ISNULL((SELECT MIN(FLOOR(TF.numOnHand/TF.numQtyItemsReq)) FROM #TEMPFinal1 TF WHERE TF.numParentID = T.ID),0)
			,T.numAvailable = ISNULL((SELECT MIN(FLOOR(TF.numAvailable/TF.numQtyItemsReq)) FROM #TEMPFinal1 TF WHERE TF.numParentID = T.ID),0)
			,T.numBackOrder = 0
			,T.numOnOrder=0
		FROM
			#TEMPFinal1 T
		WHERE
			ISNULL(T.bitKitParent,0) = 1

		SELECT * FROM #TEMPFinal1

		DROP TABLE #TEMPFinal1
	END
	ELSE
	BEGIN
		SELECT DISTINCT 
			c.*
			,UOM.vcUnitName
			,ISNULL(UOM.numUOMId,0) numUOMId
			,IDUOM.vcUnitName AS vcIDUnitName
			,dbo.fn_UOMConversion(IDUOM.numUOMId,0,IDUOM.numDomainId,UOM.numUOMId) AS UOMConversionFactor
			,c.numQtyItemsReq * dbo.fn_UOMConversion(UOM.numUOMId,0,IDUOM.numDomainId,IDUOM.numUOMId) AS numConQty
			,(c.numQtyItemsReq * dbo.fn_UOMConversion(UOM.numUOMId,0,IDUOM.numDomainId,IDUOM.numUOMId)) * c.monAverageCost AS monAverageCostTotalForQtyRequired
			,c.sintOrder
			,c.numUOMQuantity
		FROM 
			#temp c  
		LEFT JOIN 
			UOM 
		ON 
			UOM.numUOMId=c.numBaseUnit
		LEFT JOIN 
			UOM IDUOM 
		ON 
			IDUOM.numUOMId=c.numIDUOMId
		ORDER BY 
			c.StageLevel
	END
END
GO
