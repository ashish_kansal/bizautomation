SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetTrueCommerceUsers')
DROP PROCEDURE dbo.USP_GetTrueCommerceUsers
GO
CREATE PROCEDURE [dbo].[USP_GetTrueCommerceUsers] 

AS
BEGIN
	SELECT 
		DomainSFTPDetail.numDomainID
		,DomainSFTPDetail.vcUsername
		,DomainSFTPDetail.vcPassword
		,DomainSFTPDetail.vcExportPath
		,DomainSFTPDetail.vcImportPath 
	FROM 
		DomainSFTPDetail 
	INNER JOIN
		Domain
	ON
		DomainSFTPDetail.numDomainID = Domain.numDomainID
	WHERE 
		ISNULL(Domain.bitEDI,0) = 1
		AND tintType=2
		--AND vcExportPath IS NOT NULL
		--AND vcImportPath IS NOT NULL
END
GO


