GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_GetMaxJournalReferenceNo' ) 
    DROP PROCEDURE USP_GetMaxJournalReferenceNo
GO
CREATE PROCEDURE [dbo].[USP_GetMaxJournalReferenceNo]
    @numDomainId AS NUMERIC(9) = 0
AS 
       SELECT MAX(numJournalReferenceNo) + 1 AS numJournalReferenceNo FROM General_Journal_Header WHERE numDomainId=@numDomainId              
GO
