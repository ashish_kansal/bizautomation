
GO
/****** Object:  StoredProcedure [dbo].[USP_DeleteAlertConfig]    Script Date: 11/02/2011 12:42:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_DeleteAlertConfig')
DROP PROCEDURE USP_DeleteAlertConfig
GO

CREATE PROC [dbo].[USP_DeleteAlertConfig] 
    @numAlertConfigId numeric(18, 0),
@numDomainID numeric
AS 
 
 DELETE
 FROM   [dbo].[AlertConfig]
 WHERE  [numAlertConfigId] = @numAlertConfigId AND numDomainID = @numDomainID 
