GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_UpdateUserAccessDetailsDefault')
DROP PROCEDURE usp_UpdateUserAccessDetailsDefault
GO
/****** Object:  StoredProcedure [dbo].[usp_SetUsersWithDomains]    Script Date: 07/26/2008 16:21:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj                      
Create PROCEDURE [dbo].[usp_UpdateUserAccessDetailsDefault]
 @numUserID NUMERIC(9),
 @numDomainID as numeric(9),                              
 @byteMode as tinyint,  
 @tintDefaultRemStatus as tinyint,                                   
 @numDefaultTaskType as numeric(9),
 @bitOutlook AS BIT                            
AS                                    
BEGIN                       
        
    if  @byteMode=1     
	BEGIN
		UPDATE UserMaster SET tintDefaultRemStatus = @tintDefaultRemStatus  WHERE numUserID = @numUserID and numDomainID=@numDomainID
	END
     else if  @byteMode=2     
	BEGIN
		UPDATE UserMaster SET numDefaultTaskType = @numDefaultTaskType  WHERE numUserID = @numUserID and numDomainID=@numDomainID
	END 
	else if  @byteMode=3
	BEGIN
		UPDATE UserMaster SET bitOutlook = @bitOutlook  WHERE numUserID = @numUserID and numDomainID=@numDomainID
	END  
END
          
  
