GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_MassPurchaseFulfillment_GetOrderItems')
DROP PROCEDURE dbo.USP_MassPurchaseFulfillment_GetOrderItems
GO

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[USP_MassPurchaseFulfillment_GetOrderItems]
(
	@numDomainID NUMERIC(18,0)
	,@numUserCntID NUMERIC(18,0)
	,@numOppID NUMERIC(18,0)
	,@tintMode TINYINT
	,@vcSelectedRecords VARCHAR(MAX)
)
AS 
BEGIN
	DECLARE @tintBillType TINYINT = 1
	
	IF EXISTS (SELECT ID FROM MassPurchaseFulfillmentConfiguration WHERE numDomainID=@numDomainID AND numUserCntID=@numUserCntID)
	BEGIN
		SELECT 
			@tintBillType = ISNULL(tintBillType,1)
		FROM 
			MassPurchaseFulfillmentConfiguration 
		WHERE 
			numDomainID=@numDomainID 
			AND numUserCntID=@numUserCntID
	END

	IF ISNULL(@vcSelectedRecords,'') <> ''
	BEGIN
		DECLARE @TempItems TABLE
		(	
			numOppItemID NUMERIC(18,0),
			numUnitHour FLOAT
		)

		INSERT INTO @TempItems
		(
			numOppItemID,
			numUnitHour
		)
		SELECT
			CAST(SUBSTRING(OutParam,0,CHARINDEX('-',OutParam)) AS NUMERIC(18,0))
			,CAST(SUBSTRING(OutParam,CHARINDEX('-',OutParam) + 1,LEN(OutParam)) AS FLOAT)
		FROM
			dbo.SplitString(@vcSelectedRecords,',')

		IF @tintMode = 1 -- For bill orders
		BEGIN
			SELECT
				numoppitemtCode
				,(CASE WHEN ISNULL(T1.numUnitHour,0) > 0 
					THEN T1.numUnitHour
					ELSE (CASE 
							WHEN @tintBillType=2 
							THEN ISNULL(OpportunityItems.numQtyReceived,0) 
							WHEN @tintBillType=3 THEN ISNULL(OpportunityItems.numUnitHourReceived,0) 
							ELSE ISNULL(OpportunityItems.numUnitHour,0) 
						END) - ISNULL((SELECT 
											SUM(OpportunityBizDocItems.numUnitHour)
										FROM
											OpportunityBizDocs
										INNER JOIN
											OpportunityBizDocItems
										ON
											OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
										WHERE
											OpportunityBizDocs.numOppId = @numOppID
											AND OpportunityBizDocs.numBizDocId=644
											AND OpportunityBizDocItems.numOppItemID = OpportunityItems.numoppitemtcode),0) 
				END) numUnitHour
				,monPrice
			FROM
				@TempItems T1
			INNER JOIN
				OpportunityItems
			ON
				T1.numOppItemID=OpportunityItems.numoppitemtCode
			INNER JOIN
				OpportunityMaster
			ON
				OpportunityItems.numOppId = OpportunityMaster.numOppId
			WHERE
				OpportunityMaster.numDomainId=@numDomainID
				AND OpportunityMaster.numOppId=@numOppID
				AND OpportunityMaster.tintOppType = 2
				AND OpportunityMaster.tintOppStatus = 1
				AND ISNULL(OpportunityMaster.tintshipped,0) = 0
				AND (CASE 
							WHEN @tintBillType=2 
							THEN ISNULL(OpportunityItems.numQtyReceived,0) 
							WHEN @tintBillType=3 THEN ISNULL(OpportunityItems.numUnitHourReceived,0) 
							ELSE ISNULL(OpportunityItems.numUnitHour,0) 
						END) - ISNULL((SELECT 
											SUM(OpportunityBizDocItems.numUnitHour)
										FROM
											OpportunityBizDocs
										INNER JOIN
											OpportunityBizDocItems
										ON
											OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
										WHERE
											OpportunityBizDocs.numOppId = @numOppID
											AND OpportunityBizDocs.numBizDocId=644
											AND OpportunityBizDocItems.numOppItemID = OpportunityItems.numoppitemtcode),0) > 0
		END
	END
	ELSE
	BEGIN
		IF @tintMode = 1 -- For bill orders
		BEGIN
			SELECT
				numoppitemtCode
				,(CASE 
					WHEN @tintBillType=2 
					THEN ISNULL(OpportunityItems.numQtyReceived,0) 
					WHEN @tintBillType=3 THEN ISNULL(OpportunityItems.numUnitHourReceived,0) 
					ELSE ISNULL(OpportunityItems.numUnitHour,0) 
				END) - ISNULL((SELECT 
								SUM(OpportunityBizDocItems.numUnitHour)
							FROM
								OpportunityBizDocs
							INNER JOIN
								OpportunityBizDocItems
							ON
								OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
							WHERE
								OpportunityBizDocs.numOppId = @numOppID
								AND OpportunityBizDocs.numBizDocId=644
								AND OpportunityBizDocItems.numOppItemID = OpportunityItems.numoppitemtcode),0) numUnitHour
				,monPrice
			FROM
				OpportunityMaster
			INNER JOIN
				OpportunityItems
			ON
				OpportunityMaster.numOppId=OpportunityItems.numOppId
			WHERE
				OpportunityMaster.numDomainId=@numDomainID
				AND OpportunityMaster.numOppId=@numOppID
				AND OpportunityMaster.tintOppType = 2
				AND OpportunityMaster.tintOppStatus = 1
				AND ISNULL(OpportunityMaster.tintshipped,0) = 0
				AND (CASE 
						WHEN @tintBillType=2 
						THEN ISNULL(OpportunityItems.numQtyReceived,0) 
						WHEN @tintBillType=3 THEN ISNULL(OpportunityItems.numUnitHourReceived,0) 
						ELSE ISNULL(OpportunityItems.numUnitHour,0) 
					END) - ISNULL((SELECT 
										SUM(OpportunityBizDocItems.numUnitHour)
									FROM
										OpportunityBizDocs
									INNER JOIN
										OpportunityBizDocItems
									ON
										OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
									WHERE
										OpportunityBizDocs.numOppId = @numOppID
										AND OpportunityBizDocs.numBizDocId=644
										AND OpportunityBizDocItems.numOppItemID = OpportunityItems.numoppitemtcode),0) > 0
		END
		ELSE IF @tintMode = 2 -- For close order with auto receive and bill on
		BEGIN
			SELECT 
				OpportunityMaster.numOppID OppID
				,OpportunityMaster.vcPoppName OppName
				,OpportunityItems.numoppitemtCode OppItemID
				,(CASE WHEN ISNULL(OpportunityMaster.bitStockTransfer,0) = 1 THEN OpportunityItems.numToWarehouseItemID ELSE OpportunityItems.numWarehouseItmsID END) WarehouseItemID
				,ISNULL(OpportunityMaster.bitStockTransfer,0) IsStockTransfer
				,ISNULL(OpportunityItems.numUnitHour,0) - ISNULL(OpportunityItems.numUnitHourReceived,0) QtyToReceive
				,OpportunityItems.numItemCode ItemCode
				,Item.vcItemName ItemName
				,ISNULL(bitSerialized,0) IsSerial
				,ISNULL(bitLotNo,0) IsLot
				,'' SerialLot
				,ISNULL(OpportunityMaster.bitPPVariance,0) IsPPVariance
				,OpportunityMaster.numDivisionId DivisionID
				,ISNULL(OpportunityItems.monPrice,0) Price
				,ISNULL(OpportunityMaster.numCurrencyID,0) CurrencyID
				,ISNULL(OpportunityMaster.fltExchangeRate,0) ExchangeRate
				,Item.charItemType CharItemType
				,OpportunityItems.vcType ItemType
				,ISNULL(OpportunityItems.bitDropShip,0) DropShip
				,ISNULL(OpportunityItems.numProjectID,0) ProjectID
				,ISNULL(OpportunityItems.numClassID,0) ClassID
				,ISNULL(Item.numAssetChartAcntId,0) AssetChartAcntId
				,ISNULL(Item.numCOGsChartAcntId,0) COGsChartAcntId
				,ISNULL(Item.numIncomeChartAcntId,0) IncomeChartAcntId
			FROM
				OpportunityMaster
			INNER JOIN
				OpportunityItems
			ON
				OpportunityMaster.numOppId = OpportunityItems.numOppId
			INNER JOIN
				Item 
			ON
				OpportunityItems.numItemCode = Item.numItemCode
			WHERE
				OpportunityItems.numOppId=@numOppID
				AND (ISNULL(OpportunityItems.numUnitHour,0) - ISNULL(OpportunityItems.numUnitHourReceived,0)) > 0
		END
	END

END
GO