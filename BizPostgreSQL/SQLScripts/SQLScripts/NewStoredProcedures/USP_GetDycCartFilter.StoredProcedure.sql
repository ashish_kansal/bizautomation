SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'usp_GetDycCartFilter' ) 
    DROP PROCEDURE usp_GetDycCartFilter
GO

CREATE PROCEDURE dbo.usp_GetDycCartFilter
	@numFormID NUMERIC(18, 0) = 0,
	@numSiteID NUMERIC(18, 0) = 0,
	@numDomainID NUMERIC(18,0)  = 0
AS

BEGIN 

SELECT 	ISNULL(DCF.numFieldID,0) AS [numFieldID],
		ISNULL(DCF.numFormID,0) AS [numFormID],
		ISNULL(DCF.numSiteID,0) AS [numSiteID],
		ISNULL(DCF.numDomainID,0) AS [numDomainID],
		ISNULL(DCF.numFilterType,0) AS [numFilterType],
		ISNULL(DCF.bitCustomField,0) AS [bitCustomField],
		ISNULL(DCF.tintOrder,0) AS [tintOrder],
		ISNULL(vcSiteName,'') AS [vcSiteName],
		CASE WHEN ISNULL(DCF.bitCustomField,0) = 0 THEN ISNULL((SELECT vcFieldName FROM DycFieldMaster DM WHERE DCF.numFieldID = DM.numFieldId),'') 
			 WHEN ISNULL(DCF.bitCustomField,0) = 1 THEN ISNULL((SELECT CFM.Fld_label FROM CFW_Fld_Master CFM WHERE CFM.Fld_id =  DCF.numFieldId) ,'') 
		END AS [vcFieldName]
FROM dbo.DycCartFilters DCF
LEFT JOIN dbo.Sites S ON DCF.numSiteID = S.numSiteID AND DCF.numDomainID = S.numDomainID
--LEFT JOIN dbo.DycFieldMaster DM ON DCF.numFieldID = DM.numFieldId
--LEFT JOIN dbo.CFW_Fld_Master CFM ON CFM.Fld_id =  DCF.numFieldId AND ISNULL(DCF.bitCustomField,0) = 1
WHERE (numFormID = @numFormID  OR  @numFormID = 0)
AND (DCF.numSiteID = @numSiteID OR @numSiteID = 0)
AND (DCF.numDomainID = @numDomainID OR @numDomainID = 0)
ORDER BY bitCustomField
END 

GO
