SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_CheckRuleConditonAction')
DROP PROCEDURE USP_CheckRuleConditonAction
GO
CREATE PROCEDURE [dbo].[USP_CheckRuleConditonAction]
	@numDomainID AS NUMERIC(9),
	@numRecordID as numeric(18) ,   
	@numRuleID AS NUMERIC(9),
    @tintModuleType as TINYINT,
	@numRuleConditionID numeric(9)
AS 
   
declare @vcCondition as varchar(4000);SET @vcCondition=''
declare @sql as nvarchar(max)
declare @numROWCOUNT NUMERIC;SET @numROWCOUNT=0

select @vcCondition=vcCondition from RuleCondition where numRuleID=@numRuleID and numDomainID=@numDomainID and numRuleConditionID=@numRuleConditionID

IF @tintModuleType=49 --Biz Doc Approval
BEGIN
	SET @sql='select @numROWCOUNT=count(*) from OpportunityBizDocs where numOppBizDocsId=@numRecordID ' + Case WHEN @vcCondition ='(  )' then '' else + 'and' + @vcCondition  end 
print @sql
	
	EXECUTE sp_executeSQL @sql, N'@numRecordID numeric(9),@numROWCOUNT INT OUTPUT',@numRecordID, @numROWCOUNT OUTPUT

	IF @numROWCOUNT > 0
	BEGIN
		IF (select count(*) from ruleAction where numRuleID=@numRuleID and numDomainID=@numDomainID and numRuleConditionID=@numRuleConditionID and tintActionType=4) > 0
		BEGIN
			Create table #TempTable(ROWNUMBER  int,vcDbColumnName  varchar(100),vcFieldValue varchar(1000))

			insert into #TempTable select ROW_NUMBER() OVER( order by RA.tintActionOrder) AS ROWNUMBER,DFM.vcDbColumnName,RA.vcFieldValue
					from ruleAction RA join View_DynamicDefaultColumns DFM on RA.numActionTemplateID=DFM.numFieldID
					where DFM.numDomainID=@numDomainID and RA.numRuleID=@numRuleID and RA.numDomainID=@numDomainID and RA.numRuleConditionID=@numRuleConditionID and RA.tintActionType=4
						and DFM.numFormID=@tintModuleType 
	
			DECLARE @minROWNUMBER INT
			DECLARE @maxROWNUMBER INT
			DECLARE @vcDbColumnName varchar(100),@vcFieldValue as varchar(1000)
			
			SELECT  @minROWNUMBER = MIN(ROWNUMBER) , @maxROWNUMBER =MAX(ROWNUMBER) FROM #TempTable
		
			WHILE  @minROWNUMBER <= @maxROWNUMBER
			BEGIN
   				SELECT @vcDbColumnName = vcDbColumnName,@vcFieldValue = vcFieldValue FROM #TempTable WHERE ROWNUMBER=@minROWNUMBER
   	    
				declare @sqlUpdate as nvarchar(max)
   				SET @sqlUpdate='update OpportunityBizDocs set ' + @vcDbColumnName + '= @vcFieldValue where numOppBizDocsId=@numRecordID'
				EXECUTE sp_executeSQL @sqlUpdate, N'@numRecordID numeric(9),@vcFieldValue varchar(1000)',@numRecordID,@vcFieldValue
				
				SELECT  @minROWNUMBER = MIN(ROWNUMBER) FROM  #TempTable WHERE  [ROWNUMBER] > @minROWNUMBER
			END	

			DROP TABLE #TempTable	
		END
		select * from ruleAction where numRuleID=@numRuleID and numDomainID=@numDomainID and numRuleConditionID=@numRuleConditionID and tintActionType!=4 order by tintActionOrder

		select OppBizDocs.vcBizDocID,OPPMaster.numDivisionId,OPPMaster.numContactId from OpportunityBizDocs OppBizDocs join OpportunityMaster OPPMaster
		on OppBizDocs.numOppId=OPPMaster.numOppId
		where OppBizDocs.numOppBizDocsId=@numRecordID and OPPMaster.numDomainID=@numDomainID
	END
END
GO
