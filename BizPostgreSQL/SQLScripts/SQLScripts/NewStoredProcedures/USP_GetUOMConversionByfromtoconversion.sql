GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetUOMConversionByfromtoconversion')
DROP PROCEDURE USP_GetUOMConversionByfromtoconversion
GO
CREATE PROCEDURE [dbo].[USP_GetUOMConversionByfromtoconversion]           
@chrAction char(10),                                                               
 @numDomainID as numeric(18,0),
 @fromUOM numeric(18,0),   
 @toUOM numeric(18,0),
 @ItemCode numeric(18,0)               
AS       
      
   IF(@chrAction='G')
   BEGIN
		select * from UOMConversion where numDomainID=@numDomainID AND numUOM1=@fromUOM AND numUOM2=@toUOM
   END
	IF(@chrAction='IL')
   BEGIN
		select * from ItemUOMConversion where numDomainID=@numDomainID AND numItemCode=@ItemCode AND numSourceUOM=@fromUOM AND numTargetUOM=@toUOM
   END