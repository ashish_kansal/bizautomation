GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetCOAwithOpeningBalance_Temp')
DROP PROCEDURE USP_GetCOAwithOpeningBalance_Temp
GO
CREATE PROCEDURE [dbo].[USP_GetCOAwithOpeningBalance_Temp]
AS 
BEGIN

	SELECT numDomainId,numAccountId,vcAccountName,numOriginalOpeningBal,dtOpeningDate,(SELECT TOP 1 numUserDetailId FROM dbo.UserMaster WHERE numDomainID=COA.numDomainID AND bitActivateFlag=1) AS numUserCntID FROM dbo.Chart_Of_Accounts COA WHERE numOriginalOpeningBal >0 ORDER BY numDomainId
	
END

