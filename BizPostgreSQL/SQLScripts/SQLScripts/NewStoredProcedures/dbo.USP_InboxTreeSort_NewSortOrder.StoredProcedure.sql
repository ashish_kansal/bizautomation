-- =============================================
-- Author:		<Author,,Sachin Sadhu>
-- Create date: <Create Date,,07-Dec-13>
-- Description:	<Description,,Fetch New MailBox>
-- =============================================

GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_InboxTreeSort_NewSortOrder' ) 
    DROP PROCEDURE USP_InboxTreeSort_NewSortOrder
GO
CREATE PROCEDURE USP_InboxTreeSort_NewSortOrder
   	-- Add the parameters for the stored procedure here
	@numDomainID int,
	@numUserCntID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-- Insert statements for procedure here
	IF Not Exists (Select 'col1' from  InboxTreeSort where numdomainid=@numDomainID AND numUserCntId=@numUserCntID)
	BEGIN

		INSERT INTO InboxTreeSort
		(numParentID, vcNodeName, numDomainID, numUserCntID, numSortOrder, bitSystem)
		VALUES     (NULL,NULL,@numDomainID,@numUserCntID,1,1)
				
		DECLARE @lastParentid INT
		SELECT @lastParentid=@@identity FROM InboxTreeSort

		INSERT INTO InboxTreeSort
		(numParentID, vcNodeName, numDomainID, numUserCntID, numSortOrder, bitSystem, numFixID)
		VALUES     (@lastParentid,'Inbox',@numDomainID,@numUserCntID,2,1,1)
		INSERT INTO InboxTreeSort
		(numParentID, vcNodeName, numDomainID, numUserCntID, numSortOrder, bitSystem, numFixID)
		VALUES     (@lastParentid,'Email Archive',@numDomainID,@numUserCntID,3,1,2)
		INSERT INTO InboxTreeSort
		(numParentID, vcNodeName, numDomainID, numUserCntID, numSortOrder, bitSystem, numFixID)
		VALUES     (@lastParentid,'Sent Messages',@numDomainID,@numUserCntID,4,1,4)
		INSERT INTO InboxTreeSort
		(numParentID, vcNodeName, numDomainID, numUserCntID, numSortOrder, bitSystem, numFixID)
		VALUES     (@lastParentid,'Calendar',@numDomainID,@numUserCntID,5,1,5)
		INSERT INTO InboxTreeSort
		(numParentID, vcNodeName, numDomainID, numUserCntID, numSortOrder, bitSystem, numFixID)
		VALUES     (@lastParentid,'Custom Folders',@numDomainID,@numUserCntID,6,1,6)

		--ALL RECORDS ARE ALREADY REPLACED WITH MASTER SCRIPT TO APPROPROATE ID FOR USERCNTID BUT
		--THERE ARE SOME RECORDS IN EMAIL HISTORY TABLE FOR SOME USERS WHOSE CORRESPONDING TREE STRUCTURE IS NOT AVILABLE IN INBOXTREESORT TABLE
		--FOLLOWING SCRIPTS UPDATED NODEID IN EMAIL HISTORY TABLE FOR INBOX=0, SENT EMAILS=4 AND DELETED MAILS=2 INCASE EXISTS BEFORE TREE STRUCTURE CREATED
		UPDATE EmailHistory SET numNodeId = ISNULL((SELECT numNodeId FROM InboxTreeSort WHERE numDomainID = @numDomainID AND numUserCntID = @numUserCntID AND ISNULL(numFixID,0)=4),4) WHERE numDomainID = @numDomainID AND numUserCntId = @numUserCntID AND numNodeId = 4 AND (numNodeId <> (SELECT numNodeId FROM InboxTreeSort WHERE numDomainID = @numDomainID AND numUserCntID = @numUserCntID AND ISNULL(numFixID,0)=4))
		UPDATE EmailHistory SET numNodeId = ISNULL((SELECT numNodeId FROM InboxTreeSort WHERE numDomainID = @numDomainID AND numUserCntID = @numUserCntID AND ISNULL(numFixID,0)=2),2) WHERE numDomainID = @numDomainID AND numUserCntId = @numUserCntID AND numNodeId = 2 AND (numNodeId <> (SELECT numNodeId FROM InboxTreeSort WHERE numDomainID = @numDomainID AND numUserCntID = @numUserCntID AND ISNULL(numFixID,0)=2))
		UPDATE EmailHistory SET numNodeId = ISNULL((SELECT numNodeId FROM InboxTreeSort WHERE numDomainID = @numDomainID AND numUserCntID = @numUserCntID AND ISNULL(numFixID,0)=1),0) WHERE numDomainID = @numDomainID AND numUserCntId = @numUserCntID AND numNodeId = 0 AND (numNodeId <> (SELECT numNodeId FROM InboxTreeSort WHERE numDomainID = @numDomainID AND numUserCntID = @numUserCntID AND ISNULL(numFixID,0)=1))
	END

	DECLARE @TMEP TABLE
	(
		numNodeID int,
		numParentId int,
		vcNodeName varchar(100),
		numSortOrder int,
		bitSystem bit,
		numFixID numeric(18,0),
		bitHidden BIT
	)
	INSERT INTO 
		@TMEP
	SELECT 
		numNodeID,
		CASE WHEN numParentId=0 THEN NULL ELSE numParentID END AS numParentID,
		vcNodeName as vcNodeName,
		numSortOrder, 
		bitSystem,
		numFixID,
		ISNULL(bitHidden,0) AS bitHidden
	FROM 
		InboxTreeSort 
	WHERE 
		(numdomainid=@numDomainID) AND (numUserCntId=@numUserCntID )
	ORDER BY 
		numSortOrder asc

	DECLARE @numParentNodeID AS NUMERIC(18,0)
	SELECT @numParentNodeID = numNodeID FROM InboxTreeSort WHERE numDomainID=@numDomainID AND numUserCntID=@numUserCntID AND ISNULL(numParentID,0) = 0 AND ISNULL(vcNodeName,'')=''

	UPDATE @TMEP SET numParentId = NULL WHERE numParentID =@numParentNodeID
	UPDATE @TMEP SET numParentId = @numParentNodeID WHERE numParentID NOT IN (SELECT numNodeID FROM @TMEP)
	UPDATE @TMEP SET numParentId = @numParentNodeID WHERE numParentID IS NULL AND numNodeID<>@numParentNodeID


	--DELETE FROM @TMEP WHERE numNodeID = @numParentNodeID

	SELECT * FROM @TMEP ORDER BY numSortOrder ASC
END
