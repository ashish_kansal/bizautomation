/****** Object:  StoredProcedure [dbo].[USP_ManageWareHouseItems_Tracking]    Script Date: 07/26/2008 16:14:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_ManageWareHouseItems_Tracking' ) 
    DROP PROCEDURE USP_ManageWareHouseItems_Tracking
GO
CREATE PROCEDURE [dbo].[USP_ManageWareHouseItems_Tracking]
    @numWareHouseItemID AS NUMERIC(9) = 0,
    @numReferenceID AS NUMERIC(9)=0,
    @tintRefType AS TINYINT=0,
    @vcDescription AS VARCHAR(100)='',
    @tintMode AS TINYINT=0,
    @CurrentPage INT=0,
    @PageSize INT=0,
    @TotRecs INT=0  OUTPUT,
    @numModifiedBy NUMERIC(9)=0,
    @ClientTimeZoneOffset INT=0,
    @dtRecordDate AS DATETIME=NULL,
	@numDomainID AS NUMERIC(18,0) = 0
AS 

  DECLARE  @firstRec  AS INTEGER
  DECLARE  @lastRec  AS INTEGER


IF @tintMode=1 --Select based on WareHouseItemID
BEGIN

SELECT X.*,Row_number() over(order by dtCreatedDate1 DESC,numWareHouseItemTrackingID DESC) AS RowNumber INTO #tempTable FROM (SELECT        WHIT.numWareHouseItemID,
              WHIT.numOnHand,
              WHIT.numOnOrder,
              WHIT.numReorder,
              WHIT.numAllocation,
              WHIT.numBackOrder,
              WHIT.numDomainID,
              WHIT.vcDescription,
              WHIT.tintRefType,
              WHIT.numReferenceID,
              '' as vcPOppName,
              0 as tintOppType ,
              dbo.FormatedDateTimeFromDate(DateAdd(minute,-@ClientTimeZoneOffset, WHIT.dtCreatedDate),WHIT.numDomainID) dtCreatedDate
			  ,dtCreatedDate AS dtCreatedDate1,WHIT.numModifiedBy,ISNULL(WHIT.monAverageCost,0) AS monAverageCost,ISNULL(WHIT.numTotalOnHand,0) AS numTotalOnHand,dbo.FormatedDateFromDate(WHIT.dtRecordDate,WHIT.numDomainID) AS dtRecordDate,WHIT.numWareHouseItemTrackingID
              FROM WareHouseItems_Tracking WHIT
              join Item I  on I.numItemCode = WHIT.numReferenceID
              WHERE numWareHouseItemID=@numWareHouseItemID  
			  AND [WHIT].[numDomainID] = @numDomainID
			  AND WHIT.tintRefType=1
UNION  
SELECT    WHIT.numWareHouseItemID,
              WHIT.numOnHand,
              WHIT.numOnOrder,
              WHIT.numReorder,
              WHIT.numAllocation,
              WHIT.numBackOrder,
              WHIT.numDomainID,
              WHIT.vcDescription,
              WHIT.tintRefType,
              ISNULL(WHIT.numReferenceID,0) AS numReferenceID,
              ISNULL(ISNULL(OM.vcPOppName,''),'Order Deleted - ' + CONVERT(VARCHAR(10),ISNULL(WHIT.numReferenceID,0))) as vcPOppName,
              ISNULL(OM.tintOppType,0) as tintOppType,
              dbo.FormatedDateTimeFromDate(DateAdd(minute,-@ClientTimeZoneOffset, WHIT.dtCreatedDate),WHIT.numDomainID) dtCreatedDate
			  ,dtCreatedDate AS dtCreatedDate1,WHIT.numModifiedBy,ISNULL(WHIT.monAverageCost,0) AS monAverageCost,ISNULL(WHIT.numTotalOnHand,0) AS numTotalOnHand,dbo.FormatedDateFromDate(WHIT.dtRecordDate,WHIT.numDomainID) AS dtRecordDate,WHIT.numWareHouseItemTrackingID
              FROM WareHouseItems_Tracking WHIT
              LEFT join OpportunityMaster OM  on WHIT.numReferenceID = OM.numOppId
    WHERE numWareHouseItemID=@numWareHouseItemID AND WHIT.tintRefType IN (3,4)
	AND [WHIT].[numDomainID] = @numDomainID
    UNION
	SELECT    WHIT.numWareHouseItemID,
              WHIT.numOnHand,
              WHIT.numOnOrder,
              WHIT.numReorder,
              WHIT.numAllocation,
              WHIT.numBackOrder, 
              WHIT.numDomainID,
              WHIT.vcDescription,
              WHIT.tintRefType,
              WHIT.numReferenceID,
              ISNULL(WO.vcWorkOrderName,'') as vcPOppName,
              0 as tintOppType,
              dbo.FormatedDateTimeFromDate(DateAdd(minute,-@ClientTimeZoneOffset, WHIT.dtCreatedDate),WHIT.numDomainID) dtCreatedDate
			  ,dtCreatedDate AS dtCreatedDate1,WHIT.numModifiedBy,ISNULL(WHIT.monAverageCost,0) AS monAverageCost,ISNULL(WHIT.numTotalOnHand,0) AS numTotalOnHand,dbo.FormatedDateFromDate(WHIT.dtRecordDate,WHIT.numDomainID) AS dtRecordDate,WHIT.numWareHouseItemTrackingID
              FROM WareHouseItems_Tracking WHIT
              LEFT JOIN WorkOrder WO  on WHIT.numReferenceID = WO.numWOId
    WHERE  WHIT.numWareHouseItemID=@numWareHouseItemID AND  WHIT.tintRefType=2
	AND [WHIT].[numDomainID] = @numDomainID
    UNION
    SELECT    WHIT.numWareHouseItemID,
              WHIT.numOnHand,
              WHIT.numOnOrder,
              WHIT.numReorder,
              WHIT.numAllocation,
              WHIT.numBackOrder,
              WHIT.numDomainID,
              WHIT.vcDescription,
              WHIT.tintRefType,
              ISNULL(WHIT.numReferenceID,0) AS numReferenceID,
              ISNULL(ISNULL(RH.vcBizDocName,RH.vcRMA),'RMA Deleted - ' + CONVERT(VARCHAR(10),ISNULL(WHIT.numReferenceID,0))) as vcPOppName,
              5 as tintOppType,
              dbo.FormatedDateTimeFromDate(DateAdd(minute,-@ClientTimeZoneOffset, WHIT.dtCreatedDate),WHIT.numDomainID) dtCreatedDate
			  ,WHIT.dtCreatedDate AS dtCreatedDate1,WHIT.numModifiedBy,ISNULL(WHIT.monAverageCost,0) AS monAverageCost,ISNULL(WHIT.numTotalOnHand,0) AS numTotalOnHand,dbo.FormatedDateFromDate(WHIT.dtRecordDate,WHIT.numDomainID) AS dtRecordDate,WHIT.numWareHouseItemTrackingID
              FROM WareHouseItems_Tracking WHIT
              LEFT join ReturnHeader RH  on WHIT.numReferenceID = RH.numReturnHeaderID
    WHERE numWareHouseItemID=@numWareHouseItemID AND WHIT.tintRefType=5
	AND [WHIT].[numDomainID] = @numDomainID) X

  SET @firstRec = (@CurrentPage
                     - 1)
                    * @PageSize
  SET @lastRec = (@CurrentPage
                    * @PageSize
                    + 1)
  SET @TotRecs = (SELECT COUNT(* )
                  FROM   #tempTable)
                  
  SELECT *,dbo.fn_GetContactName(ISNULL(numModifiedBy,0)) AS vcUserName FROM #tempTable WHERE RowNumber > @firstRec and RowNumber < @lastRec order by RowNumber
  DROP TABLE #tempTable
END
ELSE IF @tintMode=2 --Only Biz Calculated Avg Cost
BEGIN

SELECT X.*,Row_number() over(order by dtCreatedDate1 DESC,numWareHouseItemTrackingID DESC) AS RowNumber INTO #tempTable1 FROM (
SELECT    WHIT.numWareHouseItemID,
              WHIT.numOnHand,
              WHIT.numOnOrder,
              WHIT.numReorder,
              WHIT.numAllocation,
              WHIT.numBackOrder,
              WHIT.numDomainID,
              WHIT.vcDescription,
              WHIT.tintRefType,
              ISNULL(WHIT.numReferenceID,0) AS numReferenceID,
              ISNULL(ISNULL(OM.vcPOppName,''),'Order Deleted - ' + CONVERT(VARCHAR(10),ISNULL(WHIT.numReferenceID,0))) as vcPOppName,
              ISNULL(OM.tintOppType,0) as tintOppType,
              dbo.FormatedDateTimeFromDate(DateAdd(minute,-@ClientTimeZoneOffset, WHIT.dtCreatedDate),WHIT.numDomainID) dtCreatedDate
			  ,dtCreatedDate AS dtCreatedDate1,WHIT.numModifiedBy,ISNULL(WHIT.monAverageCost,0) AS monAverageCost,ISNULL(WHIT.numTotalOnHand,0) AS numTotalOnHand,dbo.FormatedDateFromDate(WHIT.dtRecordDate,WHIT.numDomainID) AS dtRecordDate,WHIT.numWareHouseItemTrackingID
              FROM WareHouseItems_Tracking WHIT
              LEFT join OpportunityMaster OM  on WHIT.numReferenceID = OM.numOppId
    WHERE numWareHouseItemID=@numWareHouseItemID AND WHIT.tintRefType =6
	AND [WHIT].[numDomainID] = @numDomainID
    ) X

  SET @firstRec = (@CurrentPage
                     - 1)
                    * @PageSize
  SET @lastRec = (@CurrentPage
                    * @PageSize
                    + 1)
  SET @TotRecs = (SELECT COUNT(* )
                  FROM   #tempTable1)
                  
  SELECT *,dbo.fn_GetContactName(ISNULL(numModifiedBy,0)) AS vcUserName FROM #tempTable1 WHERE RowNumber > @firstRec and RowNumber < @lastRec order by RowNumber
  
  DROP TABLE #tempTable1
END
ELSE IF @tintMode=0 --Insert new record
BEGIN

    INSERT  INTO WareHouseItems_Tracking
            (
              numWareHouseItemID,
              numOnHand,
              numOnOrder,
              numReorder,
              numAllocation,
              numBackOrder,
              numDomainID,
              vcDescription,
              tintRefType,
              numReferenceID,
              dtCreatedDate,numModifiedBy,monAverageCost,numTotalOnHand,dtRecordDate
            )
            SELECT  WHI.numWareHouseItemID,
                    ISNULL(WHI.numOnHand,0),
                    ISNULL(WHI.numOnOrder,0),
                    ISNULL(WHI.numReorder,0),
                    ISNULL(WHI.numAllocation,0),
                    ISNULL(WHI.numBackOrder,0),
                    WHI.numDomainID,
                    @vcDescription,
                    @tintRefType,
                    @numReferenceID,
                    GETUTCDATE(),@numModifiedBy,(CASE WHEN ISNULL(I.bitVirtualInventory,0) = 1 THEN 0 ELSE ISNULL(I.monAverageCost,0) END),
                    (SELECT SUM(ISNULL(numOnHand, 0)) FROM dbo.WareHouseItems WHERE numItemID=I.numItemCode),@dtRecordDate
            FROM    Item I JOIN WareHouseItems WHI ON I.numItemCode=WHI.numItemID
			WHERE WHI.numWareHouseItemID = @numWareHouseItemID
			AND [WHI].[numDomainID] = @numDomainID

	UPDATE I SET numModifiedBy=@numModifiedBy,bintModifiedDate=GETUTCDATE() FROM WareHouseItems WI INNER JOIN Item I ON WI.numItemID=I.numItemCode WHERE WI.numDomainID=@numDomainID AND WI.numWareHouseItemID=@numWareHouseItemID
END
