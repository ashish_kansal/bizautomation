SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_CustomReportTest')
DROP PROCEDURE USP_CustomReportTest
GO
CREATE PROCEDURE [dbo].[USP_CustomReportTest]    
@ParamnumDomainID NUMERIC
as     

    create table #temp
    (numCustomReportID NUMERIC,
    vcReportName VARCHAR(100),
    numDomainID NUMERIC,
    numCreatedBy  NUMERIC,numROWCOUNT NUMERIC,vcError VARCHAR(MAX));
    
DECLARE @numCustomReportID NUMERIC,@vcReportName VARCHAR(100),@textQuery varchar(max),@numCreatedBy NUMERIC
DECLARE @numDomainID AS NUMERIC, @ClientTimeZoneOffset as NUMERIC,@vcError as varchar(max),@numROWCOUNT AS NUMERIC
SET @ClientTimeZoneOffset=-330
declare @sql1  nvarchar(max)  ,@sql as nvarchar(max)
		
	declare Cust_Reports CURSOR FOR 
			SELECT numCustomReportID,vcReportName,textQuery,numCreatedBy,numDomainID FROM CustomReport WHERE numDomainID=@ParamnumDomainID
							
			OPEN Cust_Reports

			FETCH NEXT FROM Cust_Reports into @numCustomReportID,@vcReportName,@textQuery,@numCreatedBy,@numDomainID
				WHILE @@FETCH_STATUS = 0
				BEGIN  
					SET @sql=@textQuery
					set @sql1 ='declare @numUserCntId numeric(9) set @numUserCntId ='+convert(varchar(10),@numCreatedBy)
					set @sql1 = @sql1+ ' declare @ClientTimeZoneOffset  numeric(9) set @ClientTimeZoneOffset='+convert(varchar(10),@ClientTimeZoneOffset )
					
					
					IF CHARINDEX('/*sum*/', @sql)>0
						set @sql1 = @sql1+' select @numROWCOUNT=count(*) from ('+ SUBSTRING(@sql,0,CHARINDEX('/*sum*/', @sql)) +') as temp'  
				    ELSE
						set @sql1 = @sql1+' select @numROWCOUNT=count(*) from ('+ @sql +') as temp'
				    
					set @sql1=replace(@sql1,'@numDomainId',  @numDomainID)

					print(@Sql1) 
				
					SET @vcError=''
					SET @numROWCOUNT=0
					
					BEGIN TRY 
						EXECUTE sp_executeSQL @Sql1, N'@numROWCOUNT INT OUTPUT', @numROWCOUNT OUTPUT

--						execute(@Sql1)
--						SET @numROWCOUNT=@@ROWCOUNT
					END TRY
					BEGIN CATCH
						SELECT @vcError=ERROR_MESSAGE()
					END CATCH;
					
					INSERT INTO #temp values(@numCustomReportID,@vcReportName,@numDomainID,@numCreatedBy,@numROWCOUNT,@vcError)
					FETCH NEXT FROM Cust_Reports into @numCustomReportID,@vcReportName,@textQuery,@numCreatedBy,@numDomainID
				END;

			CLOSE Cust_Reports;
			DEALLOCATE Cust_Reports;

	SELECT * FROM #temp
	DROP TABLE #temp
	

GO
