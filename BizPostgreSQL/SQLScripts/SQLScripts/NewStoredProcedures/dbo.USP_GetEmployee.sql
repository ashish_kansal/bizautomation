GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetEmployee')
DROP PROCEDURE USP_GetEmployee
GO
CREATE PROCEDURE [dbo].[USP_GetEmployee]       
	@numDomainID NUMERIC(18,0)
as    
BEGIN   
	SELECT 
		A.numContactId AS numContactID
		,CONCAT(A.vcFirstName,' ',A.vcLastName) as vcUserName
		,ISNULL(A.numTeam,0) numTeam
	FROM 
		UserMaster UM
	INNER JOIN
		AdditionalContactsInformation AS A  
	ON
		UM.numUserDetailId = A.numContactId
	WHERE 
		UM.numDomainID = @numDomainID
		AND A.numDomainID=@numDomainId 
		AND ISNULL(UM.bitActivateFlag,0) = 1
	ORDER BY
		CONCAT(A.vcFirstName,' ',A.vcLastName)
END
GO