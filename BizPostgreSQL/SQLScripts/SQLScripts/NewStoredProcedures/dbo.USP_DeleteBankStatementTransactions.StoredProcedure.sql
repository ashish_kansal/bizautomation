
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_DeleteBankStatementTransactions')
	DROP PROCEDURE USP_DeleteBankStatementTransactions
GO

/****** Added By : Joseph ******/
/****** Deletes the Transaction Detail from Table BankStatementTransaction if the Transaction is not Mapped******/


CREATE PROCEDURE [dbo].[USP_DeleteBankStatementTransactions]
	@numTransactionID numeric(18, 0),
	@numDomainID numeric(18, 0)
AS

SET NOCOUNT ON

DELETE FROM [dbo].[BankStatementTransactions]
WHERE
	[numTransactionID] = @numTransactionID
AND numReferenceID IS NULL OR numReferenceID = 0

GO
