
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created by Priya
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getuseddiscountcodes')
DROP PROCEDURE usp_getuseddiscountcodes
GO
CREATE PROCEDURE [dbo].[USP_GetUsedDiscountCodes] 

@numPromotionID AS NUMERIC(9)=0

AS
BEGIN
	

	SELECT  CodeUsageLimit, vcDiscountCode
                
	FROM DiscountCodes DC
	INNER JOIN DiscountCodeUsage DCU
	ON DC.numDiscountId = DCU.numDiscountId
	WHERE numPromotionID = @numPromotionID
	AND DCU.intCodeUsed > 0;
	
END