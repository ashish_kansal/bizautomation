
GO
/****** Object:  StoredProcedure [dbo].[USP_GetProjectStageHierarchy]    Script Date: 09/21/2010 11:36:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- Created By Anoop Jayaraj          
-- exec USP_ConEmpList @numDomainID=72,@bitPartner=1,@numContactID=0
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetProjectStageHierarchy')
DROP PROCEDURE USP_GetProjectStageHierarchy
GO
CREATE PROCEDURE [dbo].[USP_GetProjectStageHierarchy]         
@numDomainID as numeric(9)=0,
@numProId as numeric(9)=0,
@numContactId as numeric(9)=0,
@tintMode TINYINT=-1
as       
declare @numRights int;
set @numrights=1;

select @numrights=numRights from ProjectTeamRights where numProId=@numProId and numContactId=@numContactId

SET @numrights=(case when @tintMode=0 then 1 else @numrights end)

IF @tintMode=2 --tintMode=2 (from portal) show all stages
BEGIN 
	SET @numrights = 1
	SET @tintMode = 1 --reset to project
END

;WITH MileStone (numParentStageID, numStageDetailsId, numStage,bitClose,dtEndDate,
dtStartDate,vcStageName,tinProgressPercentage,numAssignTo,vcAssignTo,vcMileStoneName,slp_id,tintPercentage,numStagePercentageId,numStagePercentage,numExpense,numTime)
AS
(
-- Anchor member definition
    SELECT numParentStageID, numStageDetailsId ,
        0 AS numStage,bitClose,dtEndDate,dtStartDate
,vcStageName,tinProgressPercentage,isnull(numAssignTo,0) as numAssignTo,            
  isnull(dbo.fn_GetContactName(numAssignTo),'-') as vcAssignTo,vcMileStoneName,slp_id,tintPercentage,numStagePercentageId,
(select numStagePercentage from StagePercentageMaster where numStagePercentageId=SPD.numStagePercentageId) numStagePercentage,
  isnull(dbo.fn_GetExpenseDtlsbyProStgID(@numProId,numStageDetailsId,0),0) as numExpense,                        
  isnull(dbo.fn_GeTimeDtlsbyProStgID_BT_NBT(@numProId,numStageDetailsId,1),'Billable Time (0) Non Billable Time (0)') as numTime
    FROM StagePercentageDetails SPD
    WHERE numParentStageID =0 and @numProId=(case @tintMode when 0 then numOppID 
				when 1 then numProjectID end) 
    UNION ALL
-- Recursive member definition
    SELECT p.numParentStageID, p.numStageDetailsId ,
        m.numStage + 1,p.bitClose,p.dtEndDate,p.dtStartDate
,p.vcStageName,p.tinProgressPercentage,isnull(p.numAssignTo,0) as numAssignTo,            
  isnull(dbo.fn_GetContactName(p.numAssignTo),'-') as vcAssignTo,p.vcMileStoneName,p.slp_id,p.tintPercentage,p.numStagePercentageId,
(select numStagePercentage from StagePercentageMaster where numStagePercentageId=p.numStagePercentageId) numStagePercentage,
  isnull(dbo.fn_GetExpenseDtlsbyProStgID(@numProId, p.numStageDetailsId,0),0) as numExpense,                        
  isnull(dbo.fn_GeTimeDtlsbyProStgID_BT_NBT(@numProId, p.numStageDetailsId,1),'Billable Time (0) Non Billable Time (0)') as numTime
    FROM StagePercentageDetails AS p
    INNER JOIN MileStone AS m
        ON p.numParentStageID = m.numStageDetailsId where @numProId=(case @tintMode when 0 then p.numOppID 
				when 1 then p.numProjectID end) 
)
SELECT numParentStageID, numStageDetailsId, numStage,ISNULL(bitClose,0) bitClose,slp_id,
dbo.[FormatedDateFromDate](ISNULL(dtEndDate,GETDATE()),@numDomainId) dtEndDate,
dbo.[FormatedDateFromDate](ISNULL(dtStartDate,GETDATE()),@numDomainId) dtStartDate,vcStageName,
isnull(tinProgressPercentage,0) as tinProgressPercentage,numAssignTo,vcAssignTo,vcMileStoneName,tintPercentage,numStagePercentage,numExpense,numTime
FROM MileStone  where 1=(case when @numRights=1 Then 1
	  when @numRights= 0 Then case when numAssignTo=@numContactId then 1 else 0 end
      when @numRights !=-1 Then case when numAssignTo in 
		(select numContactId from ProjectTeam where numProjectTeamId=@numrights) then 1 else 0 end end) order by numStagePercentageId,numStageDetailsId

Declare @ProjectName as varchar(50);
DECLARE @TotalProgress AS INT 



if @tintMode=0
BEGIN
	SET @ProjectName= (select [vcPOppName] from OpportunityMaster opp
	where numOppId=@numProId     and opp.numdomainID=  @numDomainID     )
	
	SELECT @TotalProgress = ISNULL(intTotalProgress,0) FROM dbo.ProjectProgress WHERE numDomainId=@numDomainID AND numOppId=@numProId 
END
ELSE if @tintMode=1
BEGIN
	SET @ProjectName= (select [vcProjectName] from ProjectsMaster pro                                   
	where numProId=@numProId     and pro.numdomainID=  @numDomainID     )
	
	SELECT @TotalProgress = ISNULL(intTotalProgress,0) FROM dbo.ProjectProgress WHERE numDomainId=@numDomainID AND numProId=@numProId 
END
SELECT @ProjectName AS ProjectName,ISNULL(@TotalProgress,0) AS TotalProgress


