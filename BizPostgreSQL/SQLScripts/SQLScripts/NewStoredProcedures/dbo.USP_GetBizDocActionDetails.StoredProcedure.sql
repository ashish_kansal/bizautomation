GO
/****** Object:  StoredProcedure [dbo].[USP_GetBizDocActionDetails]    Script Date: 03/03/2010 13:15:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--exec USP_GetBizDocActionDetails @numUserCntID=17,@numDomainID=72,@numBizActionID=39
--select * from BizDocAction
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetBizDocActionDetails')
DROP PROCEDURE USP_GetBizDocActionDetails
GO
CREATE PROCEDURE [dbo].[USP_GetBizDocActionDetails]
(@numUserCntID numeric(8),
@numDomainID numeric(8), 
 @numBizActionID numeric(8))
as
begin
SELECT OrderType ,substring([Order Id],1,20) + '...' as OrderId,
OrderAmount ,
substring(cast(BizDocID as varchar(250)),1,20) + '...' as BizDocID,'' as FileType,
BizDocAmount,
BizDocPaidAmount,
(SELECT count(btStatus) from BizActionDetails C where c.numOppBizDocsId=a.numOppBizDocsId and btStatus=0) as Pending, 
(SELECT count(btStatus) from BizActionDetails C where c.numOppBizDocsId=a.numOppBizDocsId and btStatus=1) as Approved, 
(SELECT count(btStatus) from BizActionDetails C where c.numOppBizDocsId=a.numOppBizDocsId and btStatus=2) as Declined,
 0 as Accept, 0 as Decline,
numOppId,A.numOppBizDocsId,numBizDocId,'' as  VcFileName,'' as cUrlType,'' as vcDocumentSection,tintOppType,
'B' AS BizDocTypeID,
A.vcComment
FROM 
	BizActionDetails A
INNER JOIN 
	View_BizDoc B 
ON
	A.numOppBizDocsId=B.numOppBizDocsId
INNER JOIN
	BizDocAction D
ON
	A.numBizActionID=D.numBizActionID and 
	D.numDomainID=B.numDomainID
where
	D.numDomainID=@numDomainID and 
	D.numBizActionID=@numBizActionID and 
    D.numContactID=@numUserCntID and 
	A.btDocType=1 and a.btStatus=0

UNION
select
  dbo.fn_GetListItemName(numDocCategory) as OrderType, 
cast(numGenericDocID as varchar(250)) as OrderId,
  0 as OrderAmount,    
  vcDocName as BizDocID,
vcfiletype as FileType,
0 as BizDocAmount,
0 as BizDocPaidAmount,
 (select count(*) from DocumentWorkflow             
 where numDocID=numGenericDocID and cDocType=vcDocumentSection and tintApprove=0) as Pending,
(select count(*) from DocumentWorkflow             
 where numDocID=numGenericDocID and cDocType=vcDocumentSection and tintApprove=1) as Approved,
 (select count(*) from DocumentWorkflow             
 where numDocID=numGenericDocID and cDocType=vcDocumentSection and tintApprove=2) as Declined,
  0 as Accept, 0 as Declain,
  0 as numOppId,numGenericDocID as numOppBizDocsId, 0 as numBizDocId,  VcFileName ,  
cUrlType,vcDocumentSection,0 AS tintOppType,'S' AS BizDocTypeID,
B.vcComment
  from dbo.GenericDocuments A
INNER JOIN
	BizActionDetails B
ON
	a.numGenericDocID=B.numOppBizDocsId
INNER JOIN
	BizDocAction C
ON
	C.numBizActionId=B.numBizActionId
	AND C.numDomainID=A.numDomainID
WHERE 
		c.numContactID=@numUserCntID AND 
		B.numBizActionId=@numBizActionID AND
        a.numDomainID=@numDomainID    and
		B.btDocType=0 and
		b.btStatus=0

UNION
select
  dbo.fn_GetListItemName(numDocCategory) as OrderType, 
cast(numGenericDocID as varchar(250)) as OrderId,
  0 as OrderAmount,    
  vcDocName as BizDocID,
vcfiletype as FileType,
0 as BizDocAmount,
0 as BizDocPaidAmount,
 (select count(*) from DocumentWorkflow             
 where numDocID=numGenericDocID and cDocType='D' and tintApprove=0) as Pending,
(select count(*) from DocumentWorkflow             
 where numDocID=numGenericDocID and cDocType='D' and tintApprove=1) as Approved,
 (select count(*) from DocumentWorkflow             
 where numDocID=numGenericDocID and cDocType='D' and tintApprove=2) as Declined,
  0 as Accept, 0 as Declain,
  0 as numOppId,numGenericDocID as numOppBizDocsId, 0 as numBizDocId,  VcFileName , 
 cUrlType,'D' vcDocumentSection,0 AS tintOppType,'D' AS BizDocTypeID,B.vcComment
  from GenericDocuments A
	INNER JOIN 
		BizActionDetails B
	ON
		a.numGenericDocID=B.numOppBizDocsId
	INNER JOIN
		BizDocAction C
	ON
		C.numBizActionId=B.numBizActionId AND C.numDomainID=A.numDomainID
  where 
		c.numContactID=@numUserCntID AND 
		B.numBizActionId=@numBizActionID AND
        a.numDomainID=@numDomainID   AND 
		B.btDocType=0 and
		b.btStatus=0
end