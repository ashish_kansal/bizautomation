GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetInventoryValuationReport')
DROP PROCEDURE USP_GetInventoryValuationReport
GO
CREATE PROCEDURE [dbo].[USP_GetInventoryValuationReport]
(
    @numDomainId AS NUMERIC(9) = 0,
    @ClientTimeZoneOffset AS INT,
	@dtDate DATE,
	@tintCurrentPage INT
)
AS
BEGIN
	IF @dtDate IS NULL
		SET @dtDate = CAST(DATEADD(MINUTE,-@ClientTimeZoneOffset,GETUTCDATE()) AS DATE)

	SELECT
		COUNT(*) OVER() AS TotalRecords
		,Item.numItemCode
		,Item.vcItemName
		,Item.vcSKU
		,Item.numBarCodeId
		,SUM(ISNULL(TEMP.numOnHand,0) + ISNULL(TEMP.numAllocation,0)) numTotalOnHand
		,CAST((CASE WHEN SUM(ISNULL(TEMP.numOnHand,0) + ISNULL(TEMP.numAllocation,0)) > 0 THEN (SUM((ISNULL(TEMP.numOnHand,0) + ISNULL(TEMP.numAllocation,0)) * ISNULL(TEMP.monAverageCost,0))/SUM(ISNULL(TEMP.numOnHand,0) + ISNULL(TEMP.numAllocation,0))) ELSE SUM((ISNULL(TEMP.numOnHand,0) + ISNULL(TEMP.numAllocation,0)) * ISNULL(TEMP.monAverageCost,0)) END) AS DECIMAL(20,5)) AS monAverageCost
		,CAST(SUM((ISNULL(TEMP.numOnHand,0) + ISNULL(TEMP.numAllocation,0)) * ISNULL(TEMP.monAverageCost,0)) AS DECIMAL(20,5)) AS monTotalValuation
	FROM
		Item
	INNER JOIN
		WareHouseItems
	ON
		Item.numItemCode = WareHouseItems.numItemID
	CROSS APPLY
	(
		SELECT TOP 1
			*
		FROM
			WareHouseItems_Tracking
		WHERE
			WareHouseItems_Tracking.numWareHouseItemID = WareHouseItems.numWareHouseItemID
			AND CAST(DATEADD(MINUTE,-@ClientTimeZoneOffset,WareHouseItems_Tracking.dtCreatedDate) AS DATE) <= @dtDate
		ORDER BY 
			dtCreatedDate DESC
	) TEMP
	WHERE
		Item.numDomainID=@numDomainId
		AND ISNULL(Item.bitKitParent,0) = 0
		AND (TEMP.numOnHand > 0 OR TEMP.numAllocation > 0)
	GROUP BY
		Item.numItemCode
		,Item.vcItemName
		,Item.vcSKU
		,Item.numBarCodeId
	ORDER BY
		vcItemName
	OFFSET
		CAST((CASE WHEN @tintCurrentPage=-1 THEN 0 ELSE (@tintCurrentPage-1) * 100 END) AS INTEGER) ROWS
	FETCH NEXT 
		CAST((CASE WHEN @tintCurrentPage=-1 THEN 1000000000 ELSE 100 END) AS INTEGER) ROWS ONLY
END
GO