GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_ManageImportActionItemReference' ) 
    DROP PROCEDURE USP_ManageImportActionItemReference
GO
CREATE PROCEDURE USP_ManageImportActionItemReference
    @numDomainID NUMERIC,
    @numCompanyID NUMERIC OUTPUT,
    @numDivisionID NUMERIC OUTPUT,
    @numContactID NUMERIC OUTPUT,
    @numNonBizContactID VARCHAR(50),
    @tintMode AS TINYINT = 0
AS 
BEGIN
    IF @tintMode = 0 
        BEGIN
            INSERT  INTO dbo.ImportActionItemReference ( numDomainID,
                                                         numNonBizContactID,
                                                         numCompanyID,
                                                         numDivisionID,
                                                         numContactID )
            VALUES  (
                      @numDomainID,
                      @numNonBizContactID,
                      @numCompanyID,
                      @numDivisionID,
                      @numContactID 
                    ) 
        END
    IF @tintMode = 1 
        BEGIN
            SELECT TOP 1 @numCompanyID=numCompanyID,
                    @numDivisionID=numDivisionID,
                    @numContactID=numContactID
            FROM    dbo.ImportActionItemReference
            WHERE   numNonBizContactID = @numNonBizContactID
                    AND numDomainID = @numDomainID
        END
END
