USE [BizExch2007DB]
GO
/****** Object:  StoredProcedure [dbo].[USP_ManageSubTabs]    Script Date: 08/04/2009 22:42:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Mohan Joshi
-- Create date: 
-- Description:	
-- =============================================
ALTER PROCEDURE [dbo].[USP_ManageSubTabs] 
	-- Add the parameters for the stored procedure here
	@numDomainID as numeric(9)=0,
	@numModuleId numeric(9) =0
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
    select numTabId,
			 case when bitFixed=1 then
			case when exists(select numTabName from TabDefault where numDomainid=@numDomainID and numTabId=TM.numTabId and tintTabType=TM.tintTabType ) then
			(select Top 1 numTabName from TabDefault where numDomainid=@numDomainID and numTabId=TM.numTabId and tintTabType=TM.tintTabType)
			else
			Tm.numTabName
			end
			else Tm.numTabName end numTabname
			,dbo.fn_IsSubTabInGroupTabDtl(numModuleId,numTabId) IsVisible, numModuleId 
			from TabMaster Tm where tintTabType in (select numGroupID from AuthenticationGroupMaster where numDomainID=@numDomainID ) 
					and numModuleId=@numModuleId 
						and (numDomainID=@numDomainID or bitFixed=1)
    -- Insert statements for procedure here
	
END
