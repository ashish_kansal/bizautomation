IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getApproverPromotionApproval')
DROP PROCEDURE usp_getApproverPromotionApproval
GO
CREATE PROCEDURE [dbo].[usp_getApproverPromotionApproval]          
  @numDomainId Numeric(18,2)=0,
  @numoppId Numeric(18,2)=0    
AS                                          
BEGIN
DECLARE @ApprovalStaus AS INT
DECLARE @CreatedBy AS INT
DECLARE @listIds AS VARCHAR(MAX)
SELECT TOP 1 @ApprovalStaus=intPromotionApprovalStatus,@CreatedBy=numCreatedBy FROM OpportunityMaster WHERE numOppId=@numoppId
IF(@ApprovalStaus=1)
BEGIN
	SET @listIds=(SELECT TOP 1 vcUserName FROM UserMaster where numUserDetailId=(SELECT TOP 1 numLevel1Authority FROM ApprovalConfig WHERE numModule=2 AND numUserId=@CreatedBy))
END
IF(@ApprovalStaus=2)
BEGIN
	SET @listIds=(SELECT TOP 1 vcUserName FROM UserMaster where numUserDetailId=(SELECT TOP 1 numLevel2Authority FROM ApprovalConfig WHERE numModule=2 AND numUserId=@CreatedBy))
END
IF(@ApprovalStaus=3)
BEGIN
	SET @listIds=(SELECT TOP 1 vcUserName FROM UserMaster where numUserDetailId=(SELECT TOP 1 numLevel3Authority FROM ApprovalConfig WHERE numModule=2 AND numUserId=@CreatedBy))
END
IF(@ApprovalStaus=4)
BEGIN
	SET @listIds=(SELECT TOP 1 vcUserName FROM UserMaster where numUserDetailId=(SELECT TOP 1 numLevel4Authority FROM ApprovalConfig WHERE numModule=2 AND numUserId=@CreatedBy))
END
IF(@ApprovalStaus=5)
BEGIN
	SET @listIds=(SELECT TOP 1 vcUserName FROM UserMaster where numUserDetailId=(SELECT TOP 1 numLevel5Authority FROM ApprovalConfig WHERE numModule=2 AND numUserId=@CreatedBy))
END
IF(@ApprovalStaus=6)
BEGIN
	SET @listIds=
		(SELECT TOP 1 vcUserName FROM UserMaster where numUserDetailId=(SELECT TOP 1 numLevel1Authority FROM ApprovalConfig WHERE numModule=2 AND numUserId=@CreatedBy))
		+','+
		(SELECT TOP 1 vcUserName FROM UserMaster where numUserDetailId=(SELECT TOP 1 numLevel2Authority FROM ApprovalConfig WHERE numModule=2 AND numUserId=@CreatedBy))
		+','+
		(SELECT TOP 1 vcUserName FROM UserMaster where numUserDetailId=(SELECT TOP 1 numLevel3Authority FROM ApprovalConfig WHERE numModule=2 AND numUserId=@CreatedBy))
		+','+
		(SELECT TOP 1 vcUserName FROM UserMaster where numUserDetailId=(SELECT TOP 1 numLevel4Authority FROM ApprovalConfig WHERE numModule=2 AND numUserId=@CreatedBy))
		+','+
		(SELECT TOP 1 vcUserName FROM UserMaster where numUserDetailId=(SELECT TOP 1 numLevel5Authority FROM ApprovalConfig WHERE numModule=2 AND numUserId=@CreatedBy))
END
SELECT @listIds AS UserNames
END