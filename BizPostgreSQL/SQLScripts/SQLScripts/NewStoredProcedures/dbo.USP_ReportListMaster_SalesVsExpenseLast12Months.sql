SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ReportListMaster_SalesVsExpenseLast12Months')
DROP PROCEDURE USP_ReportListMaster_SalesVsExpenseLast12Months
GO
CREATE PROCEDURE [dbo].[USP_ReportListMaster_SalesVsExpenseLast12Months]
	@numDomainID NUMERIC(18,0)
	,@ClientTimeZoneOffset INT
	,@vcTimeLine VARCHAR(50)
	,@vcGroupBy VARCHAR(50)
	,@vcTeritorry VARCHAR(MAX)
	,@vcFilterBy TINYINT  -- 1:Assign TO, 2: Record Owner, 3: Teams (Based on Assigned To) 
	,@vcFilterValue VARCHAR(MAX)
	,@dtFromDate DATE
	,@dtToDate DATE
AS
BEGIN 
	DECLARE @StartDate DATE
	DECLARE @EndDate DATE
	DECLARE @TableInsertCount AS INT = 0, @j AS INT =0
	SET @EndDate = EOMONTH(DATEADD(MINUTE,-@ClientTimeZoneOffset,GETUTCDATE()))

	DECLARE @TABLE TABLE
	(
		ID INT IDENTITY(1,1)
		,MonthStartDate DATE
		,MonthEndDate DATE
		,MonthLabel VARCHAR(20)
		,MonthSales DECIMAL(20,5)
		,MonthExpense DECIMAL(20,5)
	)

	IF @vcTimeLine = 'Last12Months'
	BEGIN
		SET @StartDate = DATEADD(MONTH, DATEDIFF(MONTH, 0, @EndDate), 0)
		SET @TableInsertCount=12
	END
	IF @vcTimeLine = 'Last6Months'
	BEGIN
		SET @StartDate = DATEADD(MONTH, DATEDIFF(MONTH, 0, @EndDate), 0)
		SET @TableInsertCount=6
	END
	IF @vcTimeLine = 'Last3Months'
	BEGIN
		SET @StartDate = DATEADD(MONTH, DATEDIFF(MONTH, 0, @EndDate), 0)
		SET @TableInsertCount=3
	END
	IF @vcTimeLine = 'Last30Days'
	BEGIN
		SET @EndDate = (GETDATE()) 
		SET @StartDate = DATEADD(DAY, DATEDIFF(DAY, -30, @EndDate), 0)
		SET @TableInsertCount=30
	END
	IF @vcTimeLine = 'Last7Days'
	BEGIN
		SET @EndDate = (DATEADD(MINUTE,-@ClientTimeZoneOffset,GETUTCDATE())) 
		SET @StartDate = DATEADD(DAY, DATEDIFF(DAY, -7, @EndDate), 0)
		SET @TableInsertCount=7
	END
	

	WHILE @j < @TableInsertCount
	BEGIN

		IF(@vcTimeLine = 'Last30Days' OR @vcTimeLine = 'Last7Days')
		BEGIN 
			INSERT INTO @TABLE
			(
				MonthStartDate,
				MonthEndDate,
				MonthLabel
			)
			VALUES
			(
				DATEADD(DAY,-(@j+1),@EndDate),
				DATEADD(DAY,-@j,@EndDate),
				CONCAT(CONVERT(VARCHAR(6),DATEADD(DAY,-@j,@EndDate), 100),' ',YEAR(DATEADD(DAY,-@j,@EndDate)))
			);
		END
		ELSE
		BEGIN
			INSERT INTO @TABLE
			(
				MonthStartDate,
				MonthEndDate,
				MonthLabel
			)
			VALUES
			(
				DATEADD(MONTH,-@j,@StartDate),
				DATEADD(MONTH,-@j,@EndDate),
				CONCAT(CONVERT(VARCHAR(3),DATEADD(MONTH,-@j,@EndDate), 100),' ',YEAR(DATEADD(MONTH,-@j,@EndDate)))
			);
		END

		SET @j= @j + 1;
	END

	DECLARE @i AS INT = 1
	DECLARE @iCount AS INT
	DECLARE @TempSales DECIMAL(20,5) = 0
	DECLARE @TempExpense DECIMAL(20,5) = 0


	SELECT @iCount=COUNT(*) FROM @TABLE

	WHILE @i <= @iCount
	BEGIN
		SET @TempSales = 0
		SET @TempExpense = 0

		SELECT @StartDate=MonthStartDate,@EndDate=MonthEndDate FROM @TABLE WHERE ID=@i

		SELECT 
			@TempSales = SUM(monTotAmount)
		FROM
		(
			SELECT
				ISNULL(monTotAmount,0) monTotAmount
			FROM
				OpportunityItems OI
			INNER JOIN
				OpportunityMaster OM
			ON
				OI.numOppId = OM.numOppID
			INNER JOIN
				Item I
			ON
				OI.numItemCode = I.numItemCode
			WHERE
				OM.numDomainId=@numDomainID
				AND CAST(OM.bintCreatedDate AS DATE) BETWEEN  @StartDate AND @EndDate
				AND ISNULL(OM.tintOppType,0)=1
				AND ISNULL(OM.tintOppStatus,0)=1
		) TEMP
	
		SELECT @TempExpense = SUM(numDebitAmt) - SUM(numCreditAmt) FROM General_Journal_Details INNER JOIN General_Journal_Header ON General_Journal_Details.numJournalId=General_Journal_Header.numJournal_Id WHERE General_Journal_Details.numDomainId=@numDomainID AND CAST(datEntry_Date AS DATE) BETWEEN  @StartDate AND @EndDate AND General_Journal_Details.numChartAcntId IN (SELECT numAccountId FROM Chart_Of_Accounts WHERE numDomainId=@numDomainID AND bitActive = 1 AND numAcntTypeId IN (SELECT numAccountTypeID FROM AccountTypeDetail WHERE numDomainID=@numDomainID AND vcAccountCode LIKE '0104%'))

		UPDATE @TABLE SET MonthSales=ISNULL(@TempSales,0), MonthExpense=ISNULL(@TempExpense,0) WHERE ID=@i

		SET @i = @i + 1
	END

	SELECT * FROM @TABLE
END
GO