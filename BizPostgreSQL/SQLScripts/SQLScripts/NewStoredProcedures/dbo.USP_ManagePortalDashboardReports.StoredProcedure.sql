GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ManagePortalDashboardReports')
DROP PROCEDURE USP_ManagePortalDashboardReports
GO
CREATE PROCEDURE USP_ManagePortalDashboardReports
    @numDomainID NUMERIC,
    @strItems TEXT,
    @numRelationshipID NUMERIC(18, 0), 
    @numProfileID NUMERIC(18, 0),
    @numContactID NUMERIC=0
AS 
BEGIN
		
    DECLARE @hDocItem INT
    IF CONVERT(VARCHAR(10), @strItems) = 'Reset' 
    BEGIN
		DELETE FROM dbo.PortalDashboardDtl WHERE numDomainID=@numDomainID AND numRelationshipID=@numRelationshipID AND numProfileID=@numProfileID AND numContactID = @numContactID
		RETURN -- exit
	END
    
    
    IF CONVERT(VARCHAR(10), @strItems) <> '' 
        BEGIN
        
			DELETE FROM dbo.PortalDashboardDtl WHERE numDomainID=@numDomainID AND numRelationshipID=@numRelationshipID AND numProfileID=@numProfileID AND numContactID = @numContactID
            EXEC sp_xml_preparedocument @hDocItem OUTPUT, @strItems
            INSERT  INTO dbo.PortalDashboardDtl ( numDomainID,
                                                  numRelationshipID,
                                                  numProfileID,
                                                  numReportID,
                                                  tintColumnNumber,
                                                  tintRowOrder,
                                                  numContactID )
                    SELECT  @numDomainID,
                            X.numRelationshipID,
                            X.numProfileID,
                            X.numReportID,
                            X.tintColumnNumber,
                            X.tintRowOrder,
                            @numContactID
                    FROM    ( SELECT    *
                              FROM      OPENXML (@hDocItem, '/NewDataSet/Table1', 2)
                                        WITH ( numRelationshipID NUMERIC(18, 0), numProfileID NUMERIC(18, 0), numReportID NUMERIC(18, 0), tintColumnNumber TINYINT, tintRowOrder TINYINT )
                            ) X
            EXEC sp_xml_removedocument @hDocItem 
            
            IF @numContactID = 0
            BEGIN
				-- Remove User's customized portlet if admin disallow it to show
				DELETE FROM dbo.PortalDashboardDtl WHERE 
				numDomainID=@numDomainID  
				AND numRelationshipID = @numRelationshipID
				AND numProfileID = @numProfileID AND 
				numReportID IN ( SELECT  numReportID
								FROM    dbo.PortalDashboardReports
								WHERE numReportID NOT IN(SELECT numReportID FROM dbo.PortalDashboardDtl WHERE  numDomainID = @numDomainID
										AND numRelationshipID = @numRelationshipID
										AND numProfileID = @numProfileID AND numContactID = @numContactID)
								)
	            
            END
            
        END 
    
                                        
END