GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_Customer_Get_BIZAPI' ) 
    DROP PROCEDURE USP_Customer_Get_BIZAPI
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author: Sandeep Patel
-- Create date: 2 May 2014
-- Description:	Gets list of customers domain wise
-- =============================================
CREATE PROCEDURE USP_Customer_Get_BIZAPI
	@numDomainID NUMERIC(18,0) = NULL,
	@SearchText VARCHAR(100),
	@numPageIndex AS INT = 0,
    @numPageSize AS INT = 0,
    @TotalRecords INT OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT 
	    @TotalRecords = COUNT(*)
	FROM 
		dbo.CompanyInfo 
	INNER JOIN 
		dbo.DivisionMaster 
	ON
		dbo.CompanyInfo.numCompanyId = dbo.DivisionMaster.numCompanyID
	WHERE 
		dbo.CompanyInfo.numDomainID = @numDomainID AND (@SearchText = '' OR dbo.CompanyInfo.vcCompanyName LIKE '%'+ @SearchText +'%')

	SELECT
	*
    FROM
	(
		SELECT 
			ROW_NUMBER() OVER (ORDER BY CompanyInfo.vcCompanyName asc) AS RowNumber,
			DivisionMaster.numDivisionID,
			CompanyInfo.vcCompanyName,
			(
				-- Wrapped select result with another select to generate correct xml atrribute
				-- Do not remove wrapping
				SELECT 
					*
				FROM
				(
					SELECT  
						numContactId,
						vcFirstName+' '+vcLastName AS [Name],
						ISNULL(vcFirstName,'') AS vcFirstName,
						ISNULL(vcLastName,'') AS vcLastName,
						ISNULL(numPhoneExtension,'') AS numPhoneExtension,
						ISNULL(numPhone,'') AS numPhone,
						ISNULL(vcEmail,'') AS vcEmail,
						ISNULL(vcFax,'') AS vcFax,
						ISNULL(bitPrimaryContact,0) AS bitPrimaryContact,
						dbo.GetListIemName(ISNULL(numContactType,0)) AS numContactType,
						dbo.GetListIemName(ISNULL(vcDepartment,0)) AS vcDepartment,
						dbo.GetListIemName(ISNULL(vcPosition,0)) AS vcPosition
					FROM 
						AdditionalContactsInformation
					WHERE 
						AdditionalContactsInformation.numDomainID = @numDomainID AND
						AdditionalContactsInformation.numDivisionId = DivisionMaster.numDivisionID AND
						(ISNULL(vcFirstName,'')  <> '' OR ISNULL(vcLastName,'') <> '')
				) AS Contact
				FOR XML AUTO
			) AS Contacts
		FROM 
			CompanyInfo 
		INNER JOIN 
			DivisionMaster 
		ON
			CompanyInfo.numCompanyId = DivisionMaster.numCompanyID
		WHERE 
			CompanyInfo.numDomainID = @numDomainID AND 
			(@SearchText = '' OR CompanyInfo.vcCompanyName LIKE '%'+ @SearchText +'%')
	) AS I
	WHERE 
		(@numPageIndex = 0 OR @numPageSize = 0) OR
		(RowNumber > ((@numPageIndex - 1) * @numPageSize) and RowNumber < ((@numPageIndex * @numPageSize) + 1))
END
GO