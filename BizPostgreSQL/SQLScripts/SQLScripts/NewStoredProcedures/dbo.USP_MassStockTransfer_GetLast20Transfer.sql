GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_MassStockTransfer_GetLast20Transfer' ) 
    DROP PROCEDURE USP_MassStockTransfer_GetLast20Transfer
GO
CREATE PROCEDURE [dbo].[USP_MassStockTransfer_GetLast20Transfer]
	@numDomainID NUMERIC(18,0)
	,@ClientTimeZoneOffset INT
AS
BEGIN
	SELECT TOP 20
		dbo.FormatedDateFromDate(DateAdd(minute,-@ClientTimeZoneOffset, dtTransferredDate),@numDomainID) dtTransferredDate
		,Item.numItemCode
		,Item.vcItemName
		,MassStockTransfer.numQty
		,CONCAT(WFrom.vcWareHouse,CASE WHEN WLFrom.numWLocationID IS NOT NULL THEN CONCAT(' (',WLFrom.vcLocation,')') ELSE '' END) vcFrom
		,CONCAT(WTo.vcWareHouse,CASE WHEN WLTo.numWLocationID IS NOT NULL THEN CONCAT(' (',WLTo.vcLocation,')') ELSE '' END) vcTo
		,ISNULL(numFromQtyBefore,0) numFromQtyBefore
		,ISNULL(numFromQtyAfter,0) numFromQtyAfter
		,ISNULL(numToQtyBefore,0) numToQtyBefore
		,ISNULL(numToQtyAfter,0) numToQtyAfter
	FROM
		MassStockTransfer
	INNER JOIN
		Item
	ON
		MassStockTransfer.numItemCode = Item.numItemCode
	INNER JOIN
		WareHouseItems WIFrom
	ON
		MassStockTransfer.numFromWarehouseItemID=WIFrom.numWareHouseItemID
	INNER JOIN
		Warehouses WFrom
	ON
		WIFrom.numWareHouseID = WFrom.numWareHouseID
	LEFT JOIN
		WarehouseLocation WLFrom
	ON
		WIFrom.numWLocationID=WLFrom.numWLocationID
	INNER JOIN
		WareHouseItems WITo
	ON
		MassStockTransfer.numToWarehouseItemID=WITo.numWareHouseItemID
	INNER JOIN
		Warehouses WTo
	ON
		WITo.numWareHouseID = WTo.numWareHouseID
	LEFT JOIN
		WarehouseLocation WLTo
	ON
		WITo.numWLocationID=WLTo.numWLocationID
	WHERE
		MassStockTransfer.numDomainID = @numDomainID
	ORDER BY
		ID DESC

END