GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ScheduledReportsGroup_Save')
DROP PROCEDURE dbo.USP_ScheduledReportsGroup_Save
GO
CREATE PROCEDURE [dbo].[USP_ScheduledReportsGroup_Save]
(
	@numDomainID NUMERIC(18,0)
	,@numUserCntID NUMERIC(18,0)
	,@numSRGID NUMERIC(18,0)
	,@vcName NVARCHAR(200)
	,@tintFrequency TINYINT
	,@dtStartDate DATETIME
	,@numEmailTemplate NUMERIC(18,0)
	,@vcSelectedTokens NVARCHAR(MAX)
	,@vcRecipientsEmail NVARCHAR(MAX)
	,@vcReceipientsContactID NVARCHAR(MAX)
	,@ClientTimeZoneOffset INT
)
AS 
BEGIN
	IF ISNULL(@numSRGID,0) > 0
	BEGIN
		DECLARE @dtStartDateExisting DATETIME
		SELECT @dtStartDateExisting=dtStartDate FROM ScheduledReportsGroup WHERE numDomainID = @numDomainID AND ID = @numSRGID

		UPDATE
			ScheduledReportsGroup
		SET
			vcName=@vcName
			,tintFrequency=@tintFrequency
			,dtStartDate=@dtStartDate
			,numEmailTemplate=@numEmailTemplate
			,vcSelectedTokens=@vcSelectedTokens
			,vcRecipientsEmail=@vcRecipientsEmail
			,vcReceipientsContactID=@vcReceipientsContactID
			,dtNextDate=(CASE WHEN @dtStartDateExisting <> @dtStartDate THEN NULL ELSE dtNextDate END)
			,intNotOfTimeTried=(CASE WHEN @dtStartDateExisting <> @dtStartDate THEN 0 ELSE intNotOfTimeTried END)
		WHERE
			numDomainID = @numDomainID
			AND ID = @numSRGID
	END
	ELSE
	BEGIN
		IF (SELECT COUNT(*) FROM ScheduledReportsGroup WHERE numDomainID=@numDomainID) >= 19
		BEGIN
			RAISERROR('MAX_20_REPORT_GROUPS',16,1)
		END
		ELSE
		BEGIN
			INSERT INTO ScheduledReportsGroup
			(
				numDomainID
				,numCreatedBy
				,vcName
				,tintFrequency
				,dtStartDate
				,numEmailTemplate
				,vcSelectedTokens
				,vcRecipientsEmail
				,vcReceipientsContactID
				,intTimeZoneOffset
			)
			VALUES
			(
				@numDomainID
				,@numUserCntID
				,@vcName
				,@tintFrequency 
				,@dtStartDate
				,@numEmailTemplate
				,@vcSelectedTokens
				,@vcRecipientsEmail
				,@vcReceipientsContactID
				,@ClientTimeZoneOffset
			)

			SET @numSRGID = SCOPE_IDENTITY()
		END
	END

	SELECT @numSRGID
END
GO