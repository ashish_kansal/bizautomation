SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetReportDashboardDTL')
DROP PROCEDURE USP_GetReportDashboardDTL
GO
CREATE PROCEDURE [dbo].[USP_GetReportDashboardDTL]   
@numDomainID AS NUMERIC(18,0),               
@numDashBoardID AS NUMERIC(18,0),
@numReportID AS NUMERIC(18,0)             
AS  
BEGIN
	IF ISNULL(@numReportID,0) > 0 AND ISNULL(@numDashBoardID,0) = 0
	BEGIN
		SELECT 
			@numDomainID AS numDomainID
			,ReportListMaster.numReportID
			,0 AS [numUserCntID]
			,ReportListMaster.tintReportType
			,NULL [tintChartType]
			,ISNULL(ReportListMaster.vcReportName,'') [vcHeaderText]
			,NULL [vcFooterText]
			,NULL [tintRow]
			,NULL [tintColumn]
			,NULL [vcXAxis]
			,NULL [vcYAxis]
			,NULL [vcAggType]
			,NULL [tintReportCategory]
			,NULL [intHeight]
			,NULL [intWidth]
			,NULL [numDashboardTemplateID]
			,NULL [bitNewAdded]
			,NULL [vcTimeLine]
			,NULL [vcGroupBy]
			,NULL [vcTeritorry]
			,NULL [vcFilterBy]
			,NULL [vcFilterValue]
			,NULL dtFromDate
			,NULL dtToDate
			,NULL [numRecordCount]
			,NULL [tintControlField]
			,NULL [vcDealAmount]
			,NULL [tintOppType]
			,NULL [lngPConclAnalysis]
			,0 [bitTask]
			,0 [tintTotalProgress]
			,0 [tintMinNumber]
			,0 [tintMaxNumber]
			,0 [tintQtyToDisplay]
			,NULL [vcDueDate] 
			,0 bitDealWon
			,0 bitGrossRevenue
			,0 bitRevenue
			,0 bitGrossProfit
			,0 bitCustomTimeline
			,0 bitBilledButNotReceived
			,0 bitReceviedButNotBilled
			,0 bitSoldButNotShipped
			,ISNULL(ReportListMaster.bitDefault,0) bitDefault
			,ISNULL(ReportListMaster.intDefaultReportID,0) intDefaultReportID
		FROM 
			ReportListMaster
		WHERE 
			(ReportListMaster.numDomainID = @numDomainID OR ISNULL(ReportListMaster.numDomainID,0) = 0)
			AND ReportListMaster.numReportID = @numReportID

	END
	ELSE
	BEGIN
		SELECT 
			ReportDashboard.numDomainID
			,ReportDashboard.numReportID
			,[numUserCntID]
			,ReportDashboard.tintReportType
			,[tintChartType]
			,(CASE WHEN ISNULL(ReportDashboard.vcHeaderText,'') <> '' THEN ISNULL(ReportDashboard.vcHeaderText,'') ELSE ISNULL(ReportListMaster.vcReportName,'') END) [vcHeaderText]
			,[vcFooterText]
			,[tintRow]
			,[tintColumn]
			,[vcXAxis]
			,[vcYAxis]
			,[vcAggType]
			,[tintReportCategory]
			,[intHeight]
			,[intWidth]
			,[numDashboardTemplateID]
			,[bitNewAdded]
			,[vcTimeLine]
			,[vcGroupBy]
			,[vcTeritorry]
			,[vcFilterBy]
			,[vcFilterValue]
			,ReportDashboard.dtFromDate
			,ReportDashboard.dtToDate
			,[numRecordCount]
			,[tintControlField]
			,[vcDealAmount]
			,[tintOppType]
			,[lngPConclAnalysis]
			,[bitTask]
			,[tintTotalProgress]
			,[tintMinNumber]
			,[tintMaxNumber]
			,[tintQtyToDisplay]
			,[vcDueDate] 
			,ISNULL(bitDealWon,0) bitDealWon
			,ISNULL(bitGrossRevenue,0) bitGrossRevenue
			,ISNULL(bitRevenue,0) bitRevenue
			,ISNULL(bitGrossProfit,0) bitGrossProfit
			,ISNULL(bitCustomTimeline,0) bitCustomTimeline
			,ISNULL(bitBilledButNotReceived,0) bitBilledButNotReceived
			,ISNULL(bitReceviedButNotBilled,0) bitReceviedButNotBilled
			,ISNULL(bitSoldButNotShipped,0) bitSoldButNotShipped
			,ISNULL(ReportListMaster.bitDefault,0) bitDefault
			,ISNULL(ReportListMaster.intDefaultReportID,0) intDefaultReportID
		FROM 
			ReportDashboard 
		LEFT JOIN 
			ReportListMaster 
		ON 
			ReportDashboard.numReportID=ReportListMaster.numReportID 
		WHERE 
			ReportDashboard.numDomainID=@numDomainID 
			AND numDashBoardID=@numDashBoardID
			AND 1 = (CASE 
						WHEN ISNULL(@numDashBoardID,0) > 0 THEN (CASE WHEN numDashBoardID=@numDashBoardID THEN 1 ELSE 0 END)
						WHEN ISNULL(@numReportID,0) > 0 THEN (CASE WHEN ReportDashboard.numReportID=@numReportID THEN 1 ELSE 0 END)
						ELSE 0
					END)
	END
	
END
GO
