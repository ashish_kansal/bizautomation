GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ManageEmbeddedCostConfig')
DROP PROCEDURE USP_ManageEmbeddedCostConfig
GO
CREATE PROCEDURE USP_ManageEmbeddedCostConfig
    @numOppBizDocID NUMERIC,
    @numLastViewedCostCatID NUMERIC,
    @numDomainID NUMERIC
AS 
    BEGIN
    
        IF NOT EXISTS ( SELECT  *
                    FROM    [EmbeddedCostConfig]
                    WHERE   [numOppBizDocID] = @numOppBizDocID
                            AND [numDomainID] = @numDomainID ) 
            BEGIN
                INSERT  INTO [EmbeddedCostConfig]
                        (
                          [numOppBizDocID],
                          [numLastViewedCostCatID],
                          [numDomainID]
                        )
                VALUES  (
                          @numOppBizDocID,
                          @numLastViewedCostCatID,
                          @numDomainID 
                        ) 
            END
        ELSE 
            BEGIN
                UPDATE  [EmbeddedCostConfig]
                SET     [numLastViewedCostCatID] = @numLastViewedCostCatID
                WHERE   [numOppBizDocID] = @numOppBizDocID
            END
    END
