GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_MassSalesFulfillment_UpdateShippingRate')
DROP PROCEDURE dbo.USP_MassSalesFulfillment_UpdateShippingRate
GO

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[USP_MassSalesFulfillment_UpdateShippingRate]
(
	@numDomainID NUMERIC(18,0)
	,@numUserCntID NUMERIC(18,0)
	,@vcSelectedRecords VARCHAR(MAX)
)
AS
BEGIN
	DECLARE @numStatus NUMERIC(18,0) = 0
	IF EXISTS (SELECT ID FROM MassSalesFulfillmentConfiguration WHERE numDomainID=@numDomainID)
	BEGIN
		SELECT @numStatus = ISNULL(numOrderStatusPacked1,0) FROM MassSalesFulfillmentConfiguration WHERE numDomainID=@numDomainID
	END
	
	DECLARE @bitSuccessful BIT = 1
	DECLARE @hDocItem INT
	DECLARE @numShippingServiceItemID NUMERIC(18,0)

	SELECT 
		@numShippingServiceItemID=ISNULL(numShippingServiceItemID,0)
	FROM 
		Domain 
	WHERE 
		numDomainId=@numDomainID 

	IF NOT EXISTS (SELECT numItemCode FROM Item WHERE numDomainID=@numDomainID AND numItemCode=@numShippingServiceItemID)
	BEGIN
		RAISERROR('SHIPPING_ITEM_NOT_SET',16,1)
		RETURN
	END

	DECLARE @Temp TABLE
	(
		ID INT IDENTITY(1,1)
		,numOppID NUMERIC(18,0)
		,numShipVia NUMERIC(18,0)
		,numShipService NUMERIC(18,0)
		,monShipRate DECIMAL(20,5)
	)

	EXEC sp_xml_preparedocument @hDocItem OUTPUT, @vcSelectedRecords

	INSERT INTO @Temp
	(
		numOppID
		,numShipVia
		,numShipService
		,monShipRate
	)
	SELECT
		ISNULL(OppID,0)
		,ISNULL(PreferredShipVia,0)
		,ISNULL(PreferredShipService,0)
		,ISNULL(ShipRate,0)
	FROM
		OPENXML (@hDocItem,'/NewDataSet/Table1',2)
	WITH
	(
		OppID  NUMERIC(18,0)
		,PreferredShipVia NUMERIC(18,0)
		,PreferredShipService NUMERIC(18,0)
		,ShipRate DECIMAL(20,5)
	)

	EXEC sp_xml_removedocument @hDocItem 


	DECLARE @i INT = 1
	DECLARE @iCount INT
	DECLARE @numOppID NUMERIC(18,0)
	DECLARE @numShipVia NUMERIC(18,0)
	DECLARE @numShipService NUMERIC(18,0)
	DECLARE @vcShipService VARCHAR(300)
	DECLARE @monShipRate DECIMAL(20,5)

	SET @iCount = (SELECT COUNT(*) FROM @TEMP)

	WHILE @i <= @iCount
	BEGIN
		SELECT
			@numOppID=numOppID
			,@numShipVia=numShipVia
			,@numShipService=numShipService
			,@monShipRate=monShipRate
		FROM 
			@TEMP 
		WHERE 
			ID=@i

		SET @vcShipService = ISNULL((SELECT TOP 1 vcShipmentService FROM ShippingService WHERE (numDomainID=@numDomainID OR ISNULL(numDomainID,0)=0) AND numShippingServiceID = @numShipService),'')

		BEGIN TRY
		BEGIN TRANSACTION
			IF EXISTS(SELECT numoppitemtCode FROM OpportunityItems WHERE numOppId=@numOppID AND numItemCode=@numShippingServiceItemID)
			BEGIN
				UPDATE 
					OpportunityItems 
				SET 
					numUnitHour=1
					,monPrice=@monShipRate
					,monTotAmount=@monShipRate
					,monTotAmtBefDiscount=@monShipRate
					,fltDiscount=0
					,vcItemDesc = @vcShipService
				WHERE 
					numOppId=@numOppID 
					AND numItemCode=@numShippingServiceItemID
			END
			ELSE 
			BEGIN
				INSERT INTO OpportunityItems
				(
					numOppId
					,numItemCode
					,numUnitHour
					,monPrice
					,monTotAmount
					,monTotAmtBefDiscount
					,bitDiscountType
					,fltDiscount
					,vcItemName
					,vcItemDesc
					,vcSKU
					,vcManufacturer
					,vcModelID
				)
				SELECT 
					@numOppID
					,@numShippingServiceItemID
					,1
					,@monShipRate
					,@monShipRate
					,@monShipRate
					,1
					,0
					,vcItemName
					,@vcShipService
					,vcSKU
					,vcManufacturer
					,vcModelID
				FROM
					Item
				WHERE
					numDomainID=@numDomainID
					AND numItemCode = @numShippingServiceItemID
			END

			UPDATE 
				OpportunityMaster 
			SET 
				intUsedShippingCompany=@numShipVia
				,numShippingService=@numShipService
				,numModifiedBy=@numUserCntID
				,bintModifiedDate=GETUTCDATE()
				,monPAmount = (SELECT SUM(monTotAmount) FROM OpportunityItems WHERE numOppId=@numOppID)
				,numStatus = (CASE WHEN ISNULL(@numStatus,0) > 0 THEN @numStatus ELSE numStatus END)
			WHERE 
				numDomainID=@numDomainID 
				AND numOppID=@numOppID

		COMMIT
		END TRY
		BEGIN CATCH
			IF @@TRANCOUNT > 0
				ROLLBACK TRANSACTION;

			SET @bitSuccessful = 0
		END CATCH
			
		SET @i = @i + 1
	END

	SELECT @bitSuccessful
END
GO