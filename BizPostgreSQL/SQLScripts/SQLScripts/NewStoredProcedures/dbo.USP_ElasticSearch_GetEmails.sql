GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ElasticSearch_GetEmails')
DROP PROCEDURE dbo.USP_ElasticSearch_GetEmails
GO
CREATE PROCEDURE [dbo].[USP_ElasticSearch_GetEmails]
	@numDomainID NUMERIC(18,0)
	,@numCurrentPage INT
	,@numPageSize INT
	,@vcEmailHistoryIds VARCHAR(MAX)
AS 
BEGIN
	--DECLARE @TEMP TABLE
	--(
	--	numEmailHstrID NUMERIC(18,0)
	--	,vcSubject VARCHAR(MAX)
	--	,vcBodyText VARCHAR(MAX)
	--	,vcFromEmail VARCHAR(150)
	--	,vcToEmail VARCHAR(MAX)
	--	,numFromEmailID NUMERIC(18,0)
	--	,numToEmailID NUMERIC(18,0)
	--	,numCCEmailID NUMERIC(18,0)
	--	,numBCCEmailID NUMERIC(18,0)
	--	,dtReceivedOn DATETIME
	--)

	--INSERT INTO 
	--	@TEMP
	--SELECT
	--	numEmailHstrID
	--	,vcSubject
	--	,vcBodyText
	--	,vcFromEmail
	--	,CONCAT(vcToEmail,CASE WHEN LEN(vcCCEmail) > 0 THEN CONCAT(', CC:',vcCCEmail) ELSE '' END,CASE WHEN LEN(vcBCCEmail) > 0 THEN CONCAT(', BCC:',vcBCCEmail) ELSE '' END)
	--	,numFromEmailID
	--	,numToEmailID
	--	,numCCEmailID
	--	,numBCCEmailID
	--	,dtReceivedOn
	--FROM
	--(
	--	SELECT
	--		numEmailHstrID
	--		,vcSubject
	--		,vcBodyText
	--		,(CASE WHEN CHARINDEX('$^$',LTRIM(RTRIM(EmailHistory.vcFrom)), (CHARINDEX('$^$',LTRIM(RTRIM(EmailHistory.vcFrom)))+1)) > 0 THEN SUBSTRING(EmailHistory.vcFrom,CHARINDEX('$^$',LTRIM(RTRIM(EmailHistory.vcFrom)), (CHARINDEX('$^$',LTRIM(RTRIM(EmailHistory.vcFrom)))+1)) + 3,LEN(EmailHistory.vcFrom) - CHARINDEX('$^$',LTRIM(RTRIM(EmailHistory.vcFrom)), (CHARINDEX('$^$',LTRIM(RTRIM(EmailHistory.vcFrom)))+1)) + 3) ELSE '' END) AS vcFromEmail
	--		,(CASE WHEN CHARINDEX('$^$',LTRIM(RTRIM(TEMPTO.vcTo)), (CHARINDEX('$^$',LTRIM(RTRIM(TEMPTO.vcTo)))+1)) > 0 THEN SUBSTRING(TEMPTO.vcTo,CHARINDEX('$^$',LTRIM(RTRIM(TEMPTO.vcTo)), (CHARINDEX('$^$',LTRIM(RTRIM(TEMPTO.vcTo)))+1)) + 3,LEN(TEMPTO.vcTo) - CHARINDEX('$^$',LTRIM(RTRIM(TEMPTO.vcTo)), (CHARINDEX('$^$',LTRIM(RTRIM(TEMPTO.vcTo)))+1)) + 3) ELSE '' END) AS vcToEmail
	--		,(CASE WHEN CHARINDEX('$^$',LTRIM(RTRIM(TEMPCC.vcCC)), (CHARINDEX('$^$',LTRIM(RTRIM(TEMPCC.vcCC)))+1)) > 0 THEN SUBSTRING(TEMPCC.vcCC,CHARINDEX('$^$',LTRIM(RTRIM(TEMPCC.vcCC)), (CHARINDEX('$^$',LTRIM(RTRIM(TEMPCC.vcCC)))+1)) + 3,LEN(TEMPCC.vcCC)-CHARINDEX('$^$',LTRIM(RTRIM(TEMPCC.vcCC)), (CHARINDEX('$^$',LTRIM(RTRIM(TEMPCC.vcCC)))+1)) + 3) ELSE '' END) AS vcCCEmail
	--		,(CASE WHEN CHARINDEX('$^$',LTRIM(RTRIM(TEMPBCC.vcBCC)), (CHARINDEX('$^$',LTRIM(RTRIM(TEMPBCC.vcBCC)))+1)) > 0 THEN SUBSTRING(TEMPBCC.vcBCC,CHARINDEX('$^$',LTRIM(RTRIM(TEMPBCC.vcBCC)), (CHARINDEX('$^$',LTRIM(RTRIM(TEMPBCC.vcBCC)))+1)) + 3,LEN(TEMPBCC.vcBCC) - CHARINDEX('$^$',LTRIM(RTRIM(TEMPBCC.vcBCC)), (CHARINDEX('$^$',LTRIM(RTRIM(TEMPBCC.vcBCC)))+1)) + 3) ELSE '' END) AS vcBCCEmail
	--		,numEmailId AS numFromEmailID
	--		,(CASE WHEN LEN(ISNULL(TEMPTO.vcTo,'')) > 0 AND CHARINDEX('$^$',TEMPTO.vcTo,0) > 0 THEN SUBSTRING(TEMPTO.vcTo,0,CHARINDEX('$^$',TEMPTO.vcTo,0)) ELSE 0 END) numToEmailID
	--		,(CASE WHEN LEN(ISNULL(TEMPCC.vcCC,'')) > 0 AND CHARINDEX('$^$',TEMPCC.vcCC,0) > 0 THEN SUBSTRING(TEMPCC.vcCC,0,CHARINDEX('$^$',TEMPCC.vcCC,0)) ELSE 0 END) numCCEmailID 
	--		,(CASE WHEN LEN(ISNULL(TEMPBCC.vcBCC,'')) > 0 AND CHARINDEX('$^$',TEMPBCC.vcBCC,0) > 0 THEN SUBSTRING(TEMPBCC.vcBCC,0,CHARINDEX('$^$',TEMPBCC.vcBCC,0)) ELSE 0 END) numBCCEmailID 
	--		,dtReceivedOn 
	--	FROM
	--		EmailHistory
	--	OUTER APPLY
	--	(
	--		SELECT item AS vcTo FROM dbo.DelimitedSplit8K(EmailHistory.vcTo,'#^#')
	--	) AS TEMPTO
	--	OUTER APPLY
	--	(
	--	SELECT item AS vcCC FROM dbo.DelimitedSplit8K(EmailHistory.vcCC,'#^#')
	--	) AS TEMPCC
	--	OUTER APPLY
	--	(
	--	SELECT item AS vcBCC FROM dbo.DelimitedSplit8K(EmailHistory.vcBCC,'#^#')
	--	) AS TEMPBCC
	--	WHERE
	--		numDomainID=@numDomainID
	--		AND (@vcEmailHistoryIds IS NULL OR numEmailHstrID IN (SELECT Id FROM dbo.SplitIDs(ISNULL(@vcEmailHistoryIds,'0'),','))) 
	--) T1

	IF ISNULL(@numCurrentPage,0) > 0 AND ISNULL(@numPageSize,0) > 0
	BEGIN
		SELECT
			numEmailHstrID AS id
			,'email' AS module
			,'' AS url
			,vcSubject AS [text]
			,CONCAT('<b style="color:#efe129">'
					,'Email:</b> '
					,dbo.FormatedDateFromDate(dtReceivedOn,@numDomainID)
					,', From:'
					,ISNULL((CASE WHEN CHARINDEX('$^$',LTRIM(RTRIM(EmailHistory.vcFrom)), (CHARINDEX('$^$',LTRIM(RTRIM(EmailHistory.vcFrom)))+1)) > 0 THEN SUBSTRING(EmailHistory.vcFrom,CHARINDEX('$^$',LTRIM(RTRIM(EmailHistory.vcFrom)), (CHARINDEX('$^$',LTRIM(RTRIM(EmailHistory.vcFrom)))+1)) + 3,LEN(EmailHistory.vcFrom) - CHARINDEX('$^$',LTRIM(RTRIM(EmailHistory.vcFrom)), (CHARINDEX('$^$',LTRIM(RTRIM(EmailHistory.vcFrom)))+1)) + 3) ELSE '' END),'')
					,', To:'
					,ISNULL(TEMPTO.vcTo,'')
					,(CASE WHEN LEN(ISNULL(TEMPCC.vcCC,'')) > 0 THEN CONCAT(', CC:',ISNULL(TEMPCC.vcCC,'')) ELSE '' END)
					,(CASE WHEN LEN(ISNULL(TEMPBCC.vcBCC,'')) > 0 THEN CONCAT(', BCC:',ISNULL(TEMPBCC.vcBCC,'')) ELSE '' END)
					,', '
					,ISNULL(vcSubject,'')
			) AS displaytext
			,ISNULL(vcSubject,'') AS Search_vcSubject
			,ISNULL(vcBodyText,'') AS Search_vcBodyText
			,dtReceivedOn AS SearchDate_dtReceivedOn
			,(CASE WHEN CHARINDEX('$^$',LTRIM(RTRIM(EmailHistory.vcFrom)), (CHARINDEX('$^$',LTRIM(RTRIM(EmailHistory.vcFrom)))+1)) > 0 THEN SUBSTRING(EmailHistory.vcFrom,CHARINDEX('$^$',LTRIM(RTRIM(EmailHistory.vcFrom)), (CHARINDEX('$^$',LTRIM(RTRIM(EmailHistory.vcFrom)))+1)) + 3,LEN(EmailHistory.vcFrom) - CHARINDEX('$^$',LTRIM(RTRIM(EmailHistory.vcFrom)), (CHARINDEX('$^$',LTRIM(RTRIM(EmailHistory.vcFrom)))+1)) + 3) ELSE '' END) AS Search_vcFromEmail
			,ISNULL(TEMPTO.vcTo,'') AS Search_vcToEmail
			,ISNULL(TEMPCC.vcCC,'') AS Search_vcCCEmail
			,ISNULL(TEMPBCC.vcBCC,'') AS Search_vcBCCEmail
			,dtReceivedOn
			,numUserCntId
		FROM
			EmailHistory
		OUTER APPLY
		(
			Select STUFF((SELECT ', ' + (CASE WHEN CHARINDEX('$^$',LTRIM(RTRIM(item)), (CHARINDEX('$^$',LTRIM(RTRIM(item)))+1)) > 0 THEN SUBSTRING(item,CHARINDEX('$^$',LTRIM(RTRIM(item)), (CHARINDEX('$^$',LTRIM(RTRIM(item)))+1)) + 3,LEN(item) - CHARINDEX('$^$',LTRIM(RTRIM(item)), (CHARINDEX('$^$',LTRIM(RTRIM(item)))+1)) + 3) ELSE '' END) FROM dbo.DelimitedSplit8K(EmailHistory.vcTo,'#^#') FOR XML PATH(''), TYPE).value('.[1]', 'nvarchar(max)'), 1, 2, '') AS vcTo
		) AS TEMPTO
		OUTER APPLY
		(
			Select STUFF((SELECT ', ' + (CASE WHEN CHARINDEX('$^$',LTRIM(RTRIM(item)), (CHARINDEX('$^$',LTRIM(RTRIM(item)))+1)) > 0 THEN SUBSTRING(item,CHARINDEX('$^$',LTRIM(RTRIM(item)), (CHARINDEX('$^$',LTRIM(RTRIM(item)))+1)) + 3,LEN(item) - CHARINDEX('$^$',LTRIM(RTRIM(item)), (CHARINDEX('$^$',LTRIM(RTRIM(item)))+1)) + 3) ELSE '' END) FROM dbo.DelimitedSplit8K(EmailHistory.vcCC,'#^#') FOR XML PATH(''), TYPE).value('.[1]', 'nvarchar(max)'), 1, 2, '') AS vcCC
		) AS TEMPCC
		OUTER APPLY
		(
			Select STUFF((SELECT ', ' + (CASE WHEN CHARINDEX('$^$',LTRIM(RTRIM(item)), (CHARINDEX('$^$',LTRIM(RTRIM(item)))+1)) > 0 THEN SUBSTRING(item,CHARINDEX('$^$',LTRIM(RTRIM(item)), (CHARINDEX('$^$',LTRIM(RTRIM(item)))+1)) + 3,LEN(item) - CHARINDEX('$^$',LTRIM(RTRIM(item)), (CHARINDEX('$^$',LTRIM(RTRIM(item)))+1)) + 3) ELSE '' END) FROM dbo.DelimitedSplit8K(EmailHistory.vcBCC,'#^#') FOR XML PATH(''), TYPE).value('.[1]', 'nvarchar(max)'), 1, 2, '') AS vcBCC
		) AS TEMPBCC
		WHERE
			numDomainID=@numDomainID
			AND (@vcEmailHistoryIds IS NULL OR numEmailHstrID IN (SELECT Id FROM dbo.SplitIDs(ISNULL(@vcEmailHistoryIds,'0'),',')))
		ORDER BY
			dtReceivedOn DESC
		OFFSET (@numCurrentPage - 1) * @numPageSize ROWS FETCH NEXT @numPageSize ROWS ONLY; 
	END
	ELSE
	BEGIN
		SELECT
			numEmailHstrID AS id
			,'email' AS module
			,'' AS url
			,vcSubject AS [text]
			,CONCAT('<b style="color:#efe129">'
					,'Email:</b> '
					,dbo.FormatedDateFromDate(dtReceivedOn,@numDomainID)
					,', From:'
					,ISNULL((CASE WHEN CHARINDEX('$^$',LTRIM(RTRIM(EmailHistory.vcFrom)), (CHARINDEX('$^$',LTRIM(RTRIM(EmailHistory.vcFrom)))+1)) > 0 THEN SUBSTRING(EmailHistory.vcFrom,CHARINDEX('$^$',LTRIM(RTRIM(EmailHistory.vcFrom)), (CHARINDEX('$^$',LTRIM(RTRIM(EmailHistory.vcFrom)))+1)) + 3,LEN(EmailHistory.vcFrom) - CHARINDEX('$^$',LTRIM(RTRIM(EmailHistory.vcFrom)), (CHARINDEX('$^$',LTRIM(RTRIM(EmailHistory.vcFrom)))+1)) + 3) ELSE '' END),'')
					,', To:'
					,ISNULL(TEMPTO.vcTo,'')
					,(CASE WHEN LEN(ISNULL(TEMPCC.vcCC,'')) > 0 THEN CONCAT(', CC:',ISNULL(TEMPCC.vcCC,'')) ELSE '' END)
					,(CASE WHEN LEN(ISNULL(TEMPBCC.vcBCC,'')) > 0 THEN CONCAT(', BCC:',ISNULL(TEMPBCC.vcBCC,'')) ELSE '' END)
					,', '
					,ISNULL(vcSubject,'')
			) AS displaytext
			,ISNULL(vcSubject,'') AS Search_vcSubject
			,ISNULL(vcBodyText,'') AS Search_vcBodyText
			,dtReceivedOn AS SearchDate_dtReceivedOn
			,(CASE WHEN CHARINDEX('$^$',LTRIM(RTRIM(EmailHistory.vcFrom)), (CHARINDEX('$^$',LTRIM(RTRIM(EmailHistory.vcFrom)))+1)) > 0 THEN SUBSTRING(EmailHistory.vcFrom,CHARINDEX('$^$',LTRIM(RTRIM(EmailHistory.vcFrom)), (CHARINDEX('$^$',LTRIM(RTRIM(EmailHistory.vcFrom)))+1)) + 3,LEN(EmailHistory.vcFrom) - CHARINDEX('$^$',LTRIM(RTRIM(EmailHistory.vcFrom)), (CHARINDEX('$^$',LTRIM(RTRIM(EmailHistory.vcFrom)))+1)) + 3) ELSE '' END) AS Search_vcFromEmail
			,ISNULL(TEMPTO.vcTo,'') AS Search_vcToEmail
			,ISNULL(TEMPCC.vcCC,'') AS Search_vcCCEmail
			,ISNULL(TEMPBCC.vcBCC,'') AS Search_vcBCCEmail
			,dtReceivedOn
			,numUserCntId
		FROM
			EmailHistory
		OUTER APPLY
		(
			Select STUFF((SELECT ', ' + (CASE WHEN CHARINDEX('$^$',LTRIM(RTRIM(item)), (CHARINDEX('$^$',LTRIM(RTRIM(item)))+1)) > 0 THEN SUBSTRING(item,CHARINDEX('$^$',LTRIM(RTRIM(item)), (CHARINDEX('$^$',LTRIM(RTRIM(item)))+1)) + 3,LEN(item) - CHARINDEX('$^$',LTRIM(RTRIM(item)), (CHARINDEX('$^$',LTRIM(RTRIM(item)))+1)) + 3) ELSE '' END) FROM dbo.DelimitedSplit8K(EmailHistory.vcTo,'#^#') FOR XML PATH(''), TYPE).value('.[1]', 'nvarchar(max)'), 1, 2, '') AS vcTo
		) AS TEMPTO
		OUTER APPLY
		(
			Select STUFF((SELECT ', ' + (CASE WHEN CHARINDEX('$^$',LTRIM(RTRIM(item)), (CHARINDEX('$^$',LTRIM(RTRIM(item)))+1)) > 0 THEN SUBSTRING(item,CHARINDEX('$^$',LTRIM(RTRIM(item)), (CHARINDEX('$^$',LTRIM(RTRIM(item)))+1)) + 3,LEN(item) - CHARINDEX('$^$',LTRIM(RTRIM(item)), (CHARINDEX('$^$',LTRIM(RTRIM(item)))+1)) + 3) ELSE '' END) FROM dbo.DelimitedSplit8K(EmailHistory.vcCC,'#^#') FOR XML PATH(''), TYPE).value('.[1]', 'nvarchar(max)'), 1, 2, '') AS vcCC
		) AS TEMPCC
		OUTER APPLY
		(
			Select STUFF((SELECT ', ' + (CASE WHEN CHARINDEX('$^$',LTRIM(RTRIM(item)), (CHARINDEX('$^$',LTRIM(RTRIM(item)))+1)) > 0 THEN SUBSTRING(item,CHARINDEX('$^$',LTRIM(RTRIM(item)), (CHARINDEX('$^$',LTRIM(RTRIM(item)))+1)) + 3,LEN(item) - CHARINDEX('$^$',LTRIM(RTRIM(item)), (CHARINDEX('$^$',LTRIM(RTRIM(item)))+1)) + 3) ELSE '' END) FROM dbo.DelimitedSplit8K(EmailHistory.vcBCC,'#^#') FOR XML PATH(''), TYPE).value('.[1]', 'nvarchar(max)'), 1, 2, '') AS vcBCC
		) AS TEMPBCC
		WHERE
			numDomainID=@numDomainID
			AND (@vcEmailHistoryIds IS NULL OR numEmailHstrID IN (SELECT Id FROM dbo.SplitIDs(ISNULL(@vcEmailHistoryIds,'0'),',')))
		ORDER BY
			dtReceivedOn DESC
	END
END