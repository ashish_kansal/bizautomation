set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go

-- EXEC USP_GetInboxItemRange 6,1,7002
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetInboxItemRange')
DROP PROCEDURE USP_GetInboxItemRange
GO
CREATE PROCEDURE [dbo].[USP_GetInboxItemRange]
               @PageSize     INT,
               @numDomainId  AS NUMERIC(9),
               @numUserCntId AS NUMERIC(9),
			   @numNodeId As NUMERIC(9)
AS
  BEGIN
  
  --Added filter for not to fetch sent items
    WITH Emails
         AS (SELECT ROW_NUMBER()
                      OVER(ORDER BY EH.numEmailHstrID DESC) AS RowNumber,
                    EH.numEmailHstrID,
                    EH.numUid
             FROM   [EmailHistory] EH
             WHERE  [numUserCntId] = @numUserCntId
                    AND [numDomainID] = @numDomainId
                    AND [tintType]<>2
                    AND [numUid]>0
					AND [numNodeId]=@numNodeId)
    SELECT *
    FROM   Emails
    WHERE  RowNumber > 0
           AND RowNumber <= @PageSize

	IF((SELECT COUNT(EH.numEmailHstrID) FROM EmailHistory AS EH LEFT JOIN EmailMaster AS EM ON
EH.numEmailId=EM.numEmailId
 WHERE numUserCntId=@numUserCntId AND EH.numDomainID=@numDomainId AND [numNodeId]=@numNodeId AND ISNULL(EM.numContactId,0)=0)>1000)
 BEGIN
 DELETE FROM EmailHistory WHERE numEmailHstrID IN(SELECT TOP 1000 EH.numEmailHstrID FROM EmailHistory AS EH LEFT JOIN EmailMaster AS EM ON
EH.numEmailId=EM.numEmailId
 WHERE numUserCntId=@numUserCntId AND EH.numDomainID=@numDomainId AND [numNodeId]=@numNodeId AND ISNULL(EM.numContactId,0)=0 ORDER BY numEmailHstrID ASC
)
 END
  END

