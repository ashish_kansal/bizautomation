
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetMatchingCheckDetails')
	DROP PROCEDURE USP_GetMatchingCheckDetails
GO
/****** Added By : Joseph ******/
/****** Gets Matching Check Transactions Details From table Check Header and Check Details 
for the Given Transaction Amount and Period of DateTime******/


CREATE PROCEDURE [dbo].[USP_GetMatchingCheckDetails]
@monAmount DECIMAL(20,5),
@numDomainId numeric(18, 0),
@dtFromDate DATETIME,
@dtToDate DATETIME


AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

BEGIN  

SELECT CH.numCheckHeaderID,CD.numCheckDetailID,CH.numChartAcntId,CH.dtCheckDate,CH.vcMemo,ROUND(ABS(CD.monAmount),2) AS monAmount,
CD.numClassID,CD.numProjectID,CD.numCustomerId,CD.numChartAcntId,C.vcCompanyname FROM dbo.CheckHeader CH 
 INNER JOIN dbo.CheckDetails CD ON CH.numCheckHeaderID = CD.numCheckHeaderID
INNER JOIN dbo.DivisionMaster D on CD.numCustomerID = D.numDivisionID
INNER JOIN dbo.companyinfo C on D.numCompanyID=C.numCompanyID
WHERE CH.numDomainID = @numDomainId AND CD.monAmount = ROUND(ABS(@monAmount),2) OR  CD.monAmount < ROUND(ABS(@monAmount),2)  
AND CH.dtCheckDate BETWEEN @dtFromDate AND @dtToDate



 
END