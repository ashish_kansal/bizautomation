GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON

IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_GetCartItemFilters' ) 
                    DROP PROCEDURE USP_GetCartItemFilters
Go

CREATE PROCEDURE [dbo].[USP_GetCartItemFilters]
(
	@numFormID		NUMERIC(18,0),
	@numSiteID		NUMERIC(18,0),
    @numDomainId	NUMERIC(18,0)
)    
AS 
BEGIN
	
	DECLARE @strSQL AS NVARCHAR(MAX)
	DECLARE @strWhere AS NVARCHAR(MAX)
	DECLARE @JOIN AS NVARCHAR(MAX)
	DECLARE @Alias AS NVARCHAR(MAX)
	
	SET @strSQL = ''
	SET @strWhere = ' WHERE 1=1 '
	
	CREATE TABLE #tempFieldValues(RowID BIGINT IDENTITY(1,1),
								  numFieldID NUMERIC(18,0),
								  numListID NUMERIC(18,0),
								  vcText NVARCHAR(MAX),
								  vcValue NUMERIC(18,0))
	
	CREATE TABLE #tempFields(RowID BIGINT,
							 numFieldID NUMERIC(18,0),
							 vcFieldName VARCHAR(100),
							 vcAssociatedControlType VARCHAR(20),
							 bitCustomField BIT,
							 vcLookBackTableName VARCHAR(50),
							 numListID	NUMERIC(18,0),
							 vcOrigDbColumnName VARCHAR(30))
							 
	INSERT INTO #tempFields(RowID,numFieldID,vcFieldName,vcAssociatedControlType,bitCustomField,vcLookBackTableName,numListID,vcOrigDbColumnName) 
	SELECT ROW_NUMBER() OVER (ORDER BY DCF.bitCustomField,DCF.numFieldID) AS RowID,
		   DCF.numFieldID,
		   CASE WHEN DCF.bitCustomField = 1 THEN CFM.Fld_label ELSE DFM.vcFieldName END AS [vcFieldName],
		   CASE WHEN DCF.bitCustomField = 1 THEN CFM.Fld_type ELSE DFM.vcAssociatedControlType END AS [vcAssociatedControlType],
		   DCF.bitCustomField,
		   CASE WHEN DCF.bitCustomField = 1 THEN '' ELSE DFM.vcLookBackTableName END AS [vcFieldName],
		   ISNULL(DFM.numListID,0) AS [numListID],
		   CASE WHEN DCF.bitCustomField = 1 THEN CONVERT(VARCHAR(10),CFM.Fld_id) + 'C' ELSE DFM.vcOrigDbColumnName END AS [vcOrigDbColumnName]
	FROM dbo.DycCartFilters DCF 
	LEFT JOIN dbo.DycFieldMaster DFM ON DCF.numFieldID = DFM.numFieldId
	LEFT JOIN dbo.CFW_Fld_Master CFM ON DCF.numFieldID = CFM.Fld_id
	WHERE DCF.numFormID = @numFormID
	AND DCF.numSiteID = @numSiteID
	AND DCF.numDomainID = @numDomainId
	
	DECLARE @intRowCnt AS BIGINT
	DECLARE @intTableRowCnt AS BIGINT
	DECLARE @numFieldID AS NUMERIC(18,0)
	DECLARE @numListID AS NUMERIC(18,0)
	DECLARE @vcAssociatedControlType AS VARCHAR(20)
	DECLARE @vcLookBackTableName AS VARCHAR(20)
	DECLARE @bitCustom AS BIT
	
	SET @intRowCnt = 0
	SET @intTableRowCnt = (SELECT COUNT(*) FROM #tempFields)
	PRINT @intTableRowCnt 
	
	--SELECT * FROM #tempFields
	IF @intTableRowCnt > 0
	BEGIN
		WHILE(@intTableRowCnt > @intRowCnt)
		BEGIN
			SET @intRowCnt = @intRowCnt + 1
			SELECT  @numFieldID = numFieldID, 
					@vcAssociatedControlType = vcAssociatedControlType,
					@bitCustom = bitCustomField,
					@vcLookBackTableName = vcLookBackTableName,
					@numListID = numListID
			FROM #tempFields WHERE RowID = @intRowCnt
			
			IF @vcAssociatedControlType = 'SelectBox' 
			BEGIN
				IF @bitCustom = 1
				BEGIN
					SET @strSQL = @strSQL + ' INSERT INTO #tempFieldValues(numFieldID,numListID,vcText,vcValue)
											  SELECT ' + CONVERT(VARCHAR(20),@numFieldID) + ',numListID,vcData,numListItemID FROM dbo.ListDetails 
											  WHERE numListID = ( SELECT numListID FROM dbo.CFW_Fld_Master WHERE Fld_ID = ' 
											  + CONVERT(VARCHAR(20),@numFieldID) + ') AND numDomainID = ' + CONVERT(VARCHAR(10),@numDomainID)
					PRINT @strSQL 
					EXEC SP_EXECUTESQL @strSQL						  
					SET @strSQL = ''		
				END
				ELSE 
				BEGIN
					PRINT '@numListID' + CONVERT(VARCHAR(10),@numListID)
					
					IF @numListID > 0
					BEGIN
						SET @strSQL = @strSQL + ' INSERT INTO #tempFieldValues(numFieldID,numListID,vcText,vcValue)
												  SELECT ' + CONVERT(VARCHAR(20),@numFieldID) + ',numListID,vcData,numListItemID FROM dbo.ListDetails 
												  WHERE numListID = ( SELECT numListID FROM dbo.DycFieldMaster WHERE numFieldID = ' 
												  + CONVERT(VARCHAR(20),@numFieldID) + ') AND numDomainID = ' + CONVERT(VARCHAR(10),@numDomainID)
						PRINT @strSQL 
						EXEC SP_EXECUTESQL @strSQL						  
						SET @strSQL = ''		
					END
--					ELSE IF @vcLookBackTableName = 'Item'
--					BEGIN
--						SET @strSQL = @strSQL + ' INSERT INTO #tempFieldValues(numFieldID,numListID,vcValue)
--										  SELECT ' + CONVERT(VARCHAR(20),@numFieldID) + ',0,vcData FROM Item 
--										  WHERE numListID = ( SELECT numlistid FROM dbo.CFW_Fld_Master WHERE Fld_ID = ' + CONVERT(VARCHAR(20),@numFieldID) + ')'
--						PRINT @strSQL 
--						EXEC SP_EXECUTESQL @strSQL						  
--						SET @strSQL = ''		
--					END

					ELSE IF @vcLookBackTableName = 'category' AND @numListID = 0
					BEGIN
						SET @strSQL = @strSQL + ' INSERT INTO #tempFieldValues(numFieldID,numListID,vcText,vcValue)
										  SELECT ' + CONVERT(VARCHAR(20),@numFieldID) + ',0,vcCategoryName,numCategoryID FROM dbo.category 
										  WHERE numDomainID = ' + CONVERT(VARCHAR(10),@numDomainID)
						PRINT @strSQL 
						EXEC SP_EXECUTESQL @strSQL						  
						SET @strSQL = ''			
					END
					
--					SET @strSQL = @strSQL + ' INSERT INTO #tempFieldValues(numFieldID,numListID,vcValue)
--											  SELECT ' + CONVERT(VARCHAR(20),@numFieldID) + ',0,vcData FROM dbo.ListDetails 
--											  WHERE numListID = ( SELECT numlistid FROM dbo.CFW_Fld_Master WHERE Fld_ID = ' + CONVERT(VARCHAR(20),@numFieldID) + ')'
--					PRINT @strSQL 
--					EXEC SP_EXECUTESQL @strSQL						  
--					SET @strSQL = ''		
				END
					
			END
			
		END 
	END
	
	SELECT * FROM #tempFields
	SELECT * FROM #tempFieldValues
	
	DROP TABLE #tempFields
	DROP TABLE #tempFieldValues
END