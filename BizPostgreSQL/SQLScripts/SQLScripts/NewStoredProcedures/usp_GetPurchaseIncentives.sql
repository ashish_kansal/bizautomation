/****** Object:  StoredProcedure [dbo].[usp_GetCompanyInfoDtlPl]    Script Date: 07/26/2008 16:16:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--usp_GetCompanyInfoDtlPl 91099,72,-333
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_GetPurchaseIncentives')
DROP PROCEDURE usp_GetPurchaseIncentives
GO
CREATE PROCEDURE [dbo].[usp_GetPurchaseIncentives]   
@numDivisonID AS NUMERIC,
@numDomainID AS NUMERIC
AS 
BEGIN
	SELECT 'true' AS IsIncentives,vcBuyingQty,intType,vcIncentives,tintDiscountType FROM PurchaseIncentives WHERE numDomainId=@numDomainID AND numDivisionID=@numDivisonID
END