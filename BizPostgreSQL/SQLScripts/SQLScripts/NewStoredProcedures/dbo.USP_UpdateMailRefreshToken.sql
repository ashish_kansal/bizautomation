GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_UpdateMailRefreshToken')
DROP PROCEDURE USP_UpdateMailRefreshToken
GO
CREATE PROCEDURE USP_UpdateMailRefreshToken
(
	@numDomainId NUMERIC(18,0),
    @numUserCntId NUMERIC(18,0),
	@tintMailProvider TINYINT,
	@vcEmail VARCHAR(100),
	@vcMailAccessToken VARCHAR(MAX),
    @vcMailRefreshToken VARCHAR(MAX),
	@intExpireInSeconds INT
)
AS 
BEGIN
	IF ISNULL(@numDomainId,0) > 0 AND ISNULL(@numUserCntId,0) > 0
	BEGIN
		UPDATE  
			UserMaster
		SET 
			vcPSMTPUserName=(CASE WHEN LEN(ISNULL(@vcEmail,'')) > 0 THEN @vcEmail ELSE vcPSMTPUserName END)
			,tintMailProvider=@tintMailProvider
			,vcMailAccessToken=@vcMailAccessToken
			,vcMailRefreshToken = @vcMailRefreshToken
			,dtTokenExpiration=DATEADD(SECOND,ISNULL(@intExpireInSeconds,0),GETUTCDATE())
		WHERE   
			numDomainID = @numDomainId
			AND numUserDetailId = @numUserCntId

		UPDATE
			UserMaster
		SET
			tintMailProvider=@tintMailProvider
			,vcMailAccessToken=@vcMailAccessToken
			,vcMailRefreshToken = @vcMailRefreshToken
			,dtTokenExpiration=DATEADD(SECOND,ISNULL(@intExpireInSeconds,0),GETUTCDATE())
		WHERE   
			numUserDetailId <> @numUserCntId
			AND vcPSMTPUserName=@vcEmail
	END
	ELSE IF ISNULL(@numDomainId,0) > 0
	BEGIN
		UPDATE
			Domain
		SET
			vcPSMTPUserName=(CASE WHEN LEN(ISNULL(@vcEmail,'')) > 0 THEN @vcEmail ELSE vcPSMTPUserName END)
			,tintMailProvider=@tintMailProvider
			,vcMailAccessToken=@vcMailAccessToken
			,vcMailRefreshToken = @vcMailRefreshToken
			,dtTokenExpiration=DATEADD(SECOND,ISNULL(@intExpireInSeconds,0),GETUTCDATE())
		WHERE
			numDomainId=@numDomainId
	END
END
GO