SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_OpportunityMaster_CreateBackOrderPO')
DROP PROCEDURE dbo.USP_OpportunityMaster_CreateBackOrderPO
GO
CREATE PROCEDURE [dbo].[USP_OpportunityMaster_CreateBackOrderPO]
(
	@numDomainID NUMERIC(18,0),
	@numUserCntID NUMERIC(18,0),
	@numOppID NUMERIC(18,0)
)
AS 
BEGIN
	DECLARE @bitReOrderPoint AS BIT = 0
	DECLARE @tintUnitsRecommendationForAutoPOBackOrder AS TINYINT  = 0
	DECLARE @tintOppStautsForAutoPOBackOrder AS TINYINT = 0
	DECLARE @numReOrderPointOrderStatus AS NUMERIC(18,0) = 0
	DECLARE @numCurrencyID NUMERIC(18,0)
	DECLARE @tintOppStatus TINYINT
	DECLARE @bitIncludeRequisitions AS BIT

	SELECT 
		@bitReOrderPoint=ISNULL(bitReOrderPoint,0)
		,@numReOrderPointOrderStatus=ISNULL(numReOrderPointOrderStatus,0)
		,@tintOppStautsForAutoPOBackOrder=ISNULL(tintOppStautsForAutoPOBackOrder,0)
		,@tintUnitsRecommendationForAutoPOBackOrder=ISNULL(tintUnitsRecommendationForAutoPOBackOrder,0)
		,@numCurrencyID=ISNULL(numCurrencyID,0)
		,@bitIncludeRequisitions=ISNULL(bitIncludeRequisitions,0)
	FROM 
		Domain 
	WHERE 
		numDomainID = @numDomainID

	SET @tintOppStatus = (CASE WHEN @tintOppStautsForAutoPOBackOrder = 2 THEN 0 ELSE 1 END)


	IF @bitReOrderPoint=1 AND (@tintOppStautsForAutoPOBackOrder = 2 OR @tintOppStautsForAutoPOBackOrder=3) --2:Purchase Opportunity, 3: Purchase Order
	BEGIN
		DECLARE @TEMPBackOrderItems TABLE
		(
			ID INT IDENTITY(1,1)
			,numItemCode NUMERIC(18,0)
			,bitAssembly BIT
			,bitKit BIT
			,numVendorID NUMERIC(18,0)
			,numWarehouseItemID NUMERIC(18,0)
			,numVendorMinQty FLOAT
			,monCost Decimal(20,5)
			,numReorderQty FLOAT
			,numBackOrder FLOAT
			,numOnOrder FLOAT
			,numRequisitions FLOAT
		)

		INSERT INTO @TEMPBackOrderItems
		(
			numItemCode
			,bitAssembly
			,bitKit
			,numVendorID
			,numWarehouseItemID
			,numVendorMinQty
			,monCost
			,numReorderQty
			,numBackOrder
			,numRequisitions
		)
		SELECT
			OI.numItemCode
			,ISNULL(I.bitAssembly,0)
			,ISNULL(I.bitKitParent,0)
			,ISNULL(I.numVendorID,0)
			,OI.numWarehouseItmsID
			,ISNULL(v.intMinQty,0)
			,ISNULL(V.monCost,0) * dbo.fn_UOMConversion(I.numBaseUnit,I.numItemCode,I.numDomainID,I.numPurchaseUnit)
			,ISNULL(I.fltReorderQty,0)
			,ISNULL(WI.numBackOrder,0)
			,ISNULL((SELECT 
						SUM(numUnitHour) 
					FROM 
						OpportunityItems 
					INNER JOIN 
						OpportunityMaster 
					ON 
						OpportunityItems.numOppID=OpportunityMaster.numOppId 
					WHERE 
						OpportunityMaster.numDomainId=@numDomainID 
						AND OpportunityMaster.tintOppType=2 
						AND OpportunityMaster.tintOppStatus=0 
						AND OpportunityItems.numItemCode=OI.numItemCode 
						AND OpportunityItems.numWarehouseItmsID=OI.numWarehouseItmsID),0)
		FROM
			OpportunityItems OI
		INNER JOIN
			Item I
		ON
			OI.numItemCode = I.numItemCode
		INNER JOIN
			Vendor V
		ON
			I.numVendorID = V.numVendorID
			AND I.numItemCode = V.numItemCode
		INNER JOIN
			WareHouseItems WI
		ON
			OI.numWarehouseItmsID = WI.numWareHouseItemID
		WHERE
			numOppId=@numOppID
			AND ISNULL(numWarehouseItmsID,0) > 0
			AND ISNULL(bitDropShip,0) = 0
			AND ISNULL(bitWorkOrder,0) = 0
			AND ISNULL(WI.numReorder,0) > 0
			AND ISNULL(I.bitKitParent,0) = 0
			AND ISNULL(WI.numOnHand,0) + ISNULL(WI.numOnOrder,0) <= ISNULL(WI.numReorder,0)
		UNION
		SELECT
			IChild.numItemCode
			,ISNULL(IChild.bitAssembly,0)
			,ISNULL(IChild.bitKitParent,0)
			,ISNULL(IChild.numVendorID,0)
			,OKI.numWareHouseItemId
			,ISNULL(v.intMinQty,0)
			,ISNULL(V.monCost,0) * dbo.fn_UOMConversion(IChild.numBaseUnit,IChild.numItemCode,IChild.numDomainID,IChild.numPurchaseUnit)
			,ISNULL(IChild.fltReorderQty,0)
			,ISNULL(WI.numBackOrder,0)
			,ISNULL((SELECT 
						SUM(numUnitHour) 
					FROM 
						OpportunityItems 
					INNER JOIN 
						OpportunityMaster 
					ON 
						OpportunityItems.numOppID=OpportunityMaster.numOppId 
					WHERE 
						OpportunityMaster.numDomainId=@numDomainID 
						AND OpportunityMaster.tintOppType=2 
						AND OpportunityMaster.tintOppStatus=0 
						AND OpportunityItems.numItemCode=OKI.numChildItemID 
						AND OpportunityItems.numWarehouseItmsID=OKI.numWareHouseItemId),0)
		FROM
			OpportunityItems OI
		INNER JOIN
			Item I
		ON
			OI.numItemCode = I.numItemCode
		INNER JOIN
			OpportunityKitItems OKI
		ON
			OI.numoppitemtCode = OKI.numOppItemID
		INNER JOIN
			Item IChild
		ON
			OKI.numChildItemID = IChild.numItemCode
			AND ISNULL(IChild.bitKitParent,0) = 0
		INNER JOIN
			Vendor V
		ON
			IChild.numVendorID = V.numVendorID
			AND IChild.numItemCode = V.numItemCode
		INNER JOIN
			WareHouseItems WI
		ON
			OKI.numWareHouseItemId = WI.numWareHouseItemID
		WHERE
			OI.numOppId=@numOppID
			AND ISNULL(bitDropShip,0) = 0
			AND ISNULL(bitWorkOrder,0) = 0
			AND ISNULL(WI.numReorder,0) > 0
			AND ISNULL(I.bitKitParent,0) = 1
			AND ISNULL(WI.numOnHand,0) + ISNULL(WI.numOnOrder,0) <= ISNULL(WI.numReorder,0)
		UNION
		SELECT
			IChild.numItemCode
			,ISNULL(IChild.bitAssembly,0)
			,ISNULL(IChild.bitKitParent,0)
			,ISNULL(IChild.numVendorID,0)
			,OKCI.numWareHouseItemId
			,ISNULL(v.intMinQty,0)
			,ISNULL(V.monCost,0) * dbo.fn_UOMConversion(IChild.numBaseUnit,IChild.numItemCode,IChild.numDomainID,IChild.numPurchaseUnit)
			,ISNULL(IChild.fltReorderQty,0)
			,ISNULL(WI.numBackOrder,0)
			,ISNULL((SELECT 
						SUM(numUnitHour) 
					FROM 
						OpportunityItems 
					INNER JOIN 
						OpportunityMaster 
					ON 
						OpportunityItems.numOppID=OpportunityMaster.numOppId 
					WHERE 
						OpportunityMaster.numDomainId=@numDomainID 
						AND OpportunityMaster.tintOppType=2 
						AND OpportunityMaster.tintOppStatus=0 
						AND OpportunityItems.numItemCode=OKCI.numItemID 
						AND OpportunityItems.numWarehouseItmsID=OKCI.numWareHouseItemId),0)
		FROM
			OpportunityItems OI
		INNER JOIN
			Item I
		ON
			OI.numItemCode = I.numItemCode
		INNER JOIN
			OpportunityKitItems OKI
		ON
			OI.numoppitemtCode = OKI.numOppItemID
		INNER JOIN
			OpportunityKitChildItems OKCI
		ON
			OKCI.numOppChildItemID = OKI.numOppChildItemID
		INNER JOIN
			Item IChild
		ON
			OKCI.numItemID = IChild.numItemCode
			AND ISNULL(IChild.bitKitParent,0) = 0
		INNER JOIN
			Vendor V
		ON
			IChild.numVendorID = V.numVendorID
			AND IChild.numItemCode = V.numItemCode
		INNER JOIN
			WareHouseItems WI
		ON
			OKCI.numWareHouseItemId = WI.numWareHouseItemID
		WHERE
			OI.numOppId=@numOppID
			AND ISNULL(bitDropShip,0) = 0
			AND ISNULL(bitWorkOrder,0) = 0
			AND ISNULL(WI.numReorder,0) > 0
			AND ISNULL(I.bitKitParent,0) = 1
			AND ISNULL(WI.numOnHand,0) + ISNULL(WI.numOnOrder,0) <= ISNULL(WI.numReorder,0)

		DECLARE @TempVendors TABLE
		(
			ID INT IDENTITY(1,1)
			,numVendorID NUMERIC(18,0)
		)

		INSERT INTO @TempVendors
		(
			numVendorID
		)
		SELECT DISTINCT
			numVendorID
		FROM
			@TEMPBackOrderItems
		WHERE
			ISNULL(numVendorID,0) > 0

		DECLARE @TempVendorItems TABLE
		(
			ID INT
			,numItemCode NUMERIC(18,0)
			,bitAssembly BIT
			,bitKit BIT
			,numWarehouseItemID NUMERIC(18,0)
			,numUnitHour FLOAT
			,monCost Decimal(20,5)
		)

		DECLARE @i AS INT = 1
		DECLARE @iCount AS INT
		SELECT @iCount = COUNT(*) FROM @TempVendors

		DECLARE @numTempVendorID NUMERIC(18,0)
		DECLARE @numNewOppID NUMERIC(18,0)
		DECLARE @numContactID NUMERIC(18,0)
		DECLARE @strItems VARCHAR(MAX)
		DECLARE @bitBillingTerms BIT
		DECLARE @intBillingDays INTEGER
		DECLARE @bitInterestType BIT
		DECLARE @fltInterest FLOAT
		DECLARE @intShippingCompany NUMERIC(18,0)
		DECLARE @numAssignedTo NUMERIC(18,0)
		DECLARE @numDefaultShippingServiceID AS NUMERIC(18,0)
		DECLARE @dtEstimatedCloseDate DATETIME = GETUTCDATE()
		DECLARE @numTempItemCode NUMERIC(18,0)
		DECLARE @numTempWarehouseItemID NUMERIC(18,0)
		DECLARE @numTempUnitHour FLOAT

		DECLARE @k INT
		DECLARE @kCount INT

		WHILE @i <= @iCount
		BEGIN
			SELECT @numTempVendorID=numVendorID FROM @TempVendors WHERE ID=@i

			SELECT 
				@bitBillingTerms=(CASE WHEN ISNULL(tintBillingTerms,0) = 1 THEN 1 ELSE 0 END) 
				,@intBillingDays = ISNULL(numBillingDays,0)
				,@bitInterestType= (CASE WHEN ISNULL(tintInterestType,0) = 1 THEN 1 ELSE 0 END)
				,@fltInterest=ISNULL(fltInterest ,0)
				,@numAssignedTo=ISNULL(numAssignedTo,0)
				,@intShippingCompany=ISNULL(intShippingCompany,0)
				,@numDefaultShippingServiceID=ISNULL(numDefaultShippingServiceID,0)
			FROM 
				DivisionMaster
			WHERE 
				numDomainID=@numDomainID AND numDivisionID=@numTempVendorID

			SELECT @numContactID = numContactId FROM AdditionalContactsInformation WHERE numDivisionId = @numTempVendorID AND bitPrimaryContact = 1

			IF @numContactID IS NULL
			BEGIN
				SELECT TOP 1 @numContactID = numContactId FROM AdditionalContactsInformation WHERE numDivisionId = @numTempVendorID
			END

			-- NORMAL INVENTORY ITEMS
			DELETE FROM @TempVendorItems
			INSERT INTO @TempVendorItems
			(
				ID
				,numItemCode
				,bitAssembly
				,bitKit
				,numWarehouseItemID
				,numUnitHour
				,monCost
			)
			SELECT
				ROW_NUMBER() OVER(ORDER BY numItemCode ASC)
				,numItemCode
				,bitAssembly
				,bitKit
				,numWarehouseItemID
				,(CASE 
					WHEN @tintUnitsRecommendationForAutoPOBackOrder=1 --Add �Re-Order Qty� or �Vendor Min Order Qty� (whichever is greater) + �Back Order Qty� - �On Order + Requisitions�
					THEN 
						(CASE WHEN ISNULL(numReorderQty,0) > ISNULL(numVendorMinQty,0) THEN ISNULL(numReorderQty,0) ELSE ISNULL(numVendorMinQty,0) END) + ISNULL(numBackOrder,0) - (ISNULL(numOnOrder,0) + (CASE WHEN @bitIncludeRequisitions = 1 THEN ISNULL(numRequisitions,0) ELSE 0 END))
					WHEN @tintUnitsRecommendationForAutoPOBackOrder=2 --Add �Re-Order Qty� or �Vendor Min Order Qty� or �Back-Order Qty� (whichever is greater) - �On Order + Requisitions�
					THEN 
						(CASE
							WHEN ISNULL(numReorderQty,0) >= ISNULL(numVendorMinQty,0) AND ISNULL(numReorderQty,0) >= ISNULL(numBackOrder,0) THEN ISNULL(numReorderQty,0)
							WHEN ISNULL(numVendorMinQty,0) >= ISNULL(numReorderQty,0) AND ISNULL(numVendorMinQty,0) >= ISNULL(numBackOrder,0) THEN ISNULL(numVendorMinQty,0)
							WHEN ISNULL(numBackOrder,0) >= ISNULL(numReorderQty,0) AND ISNULL(numBackOrder,0) >= ISNULL(numVendorMinQty,0) THEN ISNULL(numBackOrder,0)
							ELSE ISNULL(numReorderQty,0)
						END) - (ISNULL(numOnOrder,0) + (CASE WHEN @bitIncludeRequisitions = 1 THEN ISNULL(numRequisitions,0) ELSE 0 END))
					WHEN @tintUnitsRecommendationForAutoPOBackOrder=3 --Add �On-Order Qty� � + Requisitions� - �Back Order� + �Re-Order Qty� or �Vendor Min Order Qty� (whichever is greater)
					THEN
						(ISNULL(numOnOrder,0) + (CASE WHEN @bitIncludeRequisitions = 1 THEN ISNULL(numRequisitions,0) ELSE 0 END)) - ISNULL(numBackOrder,0) + (CASE WHEN ISNULL(numReorderQty,0) >= ISNULL(numVendorMinQty,0) THEN ISNULL(numReorderQty,0) ELSE ISNULL(numVendorMinQty,0) END)
					ELSE 0 
				END)
				,monCost
			FROM
				@TEMPBackOrderItems
			WHERE
				numVendorID=@numTempVendorID
				AND ISNULL(bitAssembly,0)=0 
				AND ISNULL(bitKit,0)=0
				AND (CASE 
						WHEN @tintUnitsRecommendationForAutoPOBackOrder=1 --Add �Re-Order Qty� or �Vendor Min Order Qty� (whichever is greater) + �Back Order Qty� - �On Order + Requisitions�
						THEN 
							(CASE WHEN ISNULL(numReorderQty,0) > ISNULL(numVendorMinQty,0) THEN ISNULL(numReorderQty,0) ELSE ISNULL(numVendorMinQty,0) END) + ISNULL(numBackOrder,0) - (ISNULL(numOnOrder,0) + ISNULL(numRequisitions,0))
						WHEN @tintUnitsRecommendationForAutoPOBackOrder=2 --Add �Re-Order Qty� or �Vendor Min Order Qty� or �Back-Order Qty� (whichever is greater) - �On Order + Requisitions�
						THEN 
							(CASE
								WHEN ISNULL(numReorderQty,0) >= ISNULL(numVendorMinQty,0) AND ISNULL(numReorderQty,0) >= ISNULL(numBackOrder,0) THEN ISNULL(numReorderQty,0)
								WHEN ISNULL(numVendorMinQty,0) >= ISNULL(numReorderQty,0) AND ISNULL(numVendorMinQty,0) >= ISNULL(numBackOrder,0) THEN ISNULL(numVendorMinQty,0)
								WHEN ISNULL(numBackOrder,0) >= ISNULL(numReorderQty,0) AND ISNULL(numBackOrder,0) >= ISNULL(numVendorMinQty,0) THEN ISNULL(numBackOrder,0)
								ELSE ISNULL(numReorderQty,0)
							END) - (ISNULL(numOnOrder,0) + ISNULL(numRequisitions,0))
						WHEN @tintUnitsRecommendationForAutoPOBackOrder=3 --Add �On-Order Qty� � + Requisitions� - �Back Order� + �Re-Order Qty� or �Vendor Min Order Qty� (whichever is greater)
						THEN
							(ISNULL(numOnOrder,0) + (CASE WHEN @bitIncludeRequisitions = 1 THEN ISNULL(numRequisitions,0) ELSE 0 END)) - ISNULL(numBackOrder,0) + (CASE WHEN ISNULL(numReorderQty,0) >= ISNULL(numVendorMinQty,0) THEN ISNULL(numReorderQty,0) ELSE ISNULL(numVendorMinQty,0) END)
						ELSE 0 
					END) > 0

			IF (SELECT COUNT(*) FROM @TempVendorItems) > 0
			BEGIN
				SET @k = 1
				SELECT @kCount=COUNT(*) FROM @TempVendorItems

				SET @strItems = '<?xml version="1.0" encoding="iso-8859-1" ?><NewDataSet>'

				WHILE @k <= @kCount
				BEGIN
					SELECT 
						@strItems = CONCAT(@strItems
											,'<Item><Op_Flag>1</Op_Flag>'
											,'<numoppitemtCode>',ID,'</numoppitemtCode>'
											,'<numItemCode>',T1.numItemCode,'</numItemCode>'
											,'<numUnitHour>',numUnitHour,'</numUnitHour>'
											,'<monPrice>',monCost,'</monPrice>'
											,'<monTotAmount>',numUnitHour * monCost,'</monTotAmount>'
											,'<vcItemDesc>',txtItemDesc,'</vcItemDesc>'
											,'<numWarehouseItmsID>',numWarehouseItemID,'</numWarehouseItmsID>'
											,'<ItemType>',charItemType,'</ItemType>'
											,'<DropShip>',0,'</DropShip>'
											,'<bitDiscountType>',1,'</bitDiscountType>'
											,'<fltDiscount>',0,'</fltDiscount>'
											,'<monTotAmtBefDiscount>',numUnitHour * monCost,'</monTotAmtBefDiscount>'
											,'<vcItemName>',vcItemName,'</vcItemName>'
											,'<numUOM>',ISNULL(numBaseUnit,0),'</numUOM>'
											,'<bitWorkOrder>0</bitWorkOrder>'
											,'<numVendorWareHouse>0</numVendorWareHouse>'
											,'<numShipmentMethod>0</numShipmentMethod>'
											,'<numSOVendorId>0</numSOVendorId>'
											,'<numProjectID>0</numProjectID>'
											,'<numProjectStageID>0</numProjectStageID>'
											,'<numToWarehouseItemID>0</numToWarehouseItemID>'
											,'<Attributes></Attributes>'
											,'<AttributeIDs></AttributeIDs>'
											,'<vcSKU>',vcSKU,'</vcSKU>'
											,'<bitItemPriceApprovalRequired>0</bitItemPriceApprovalRequired>'
											,'<numPromotionID>0</numPromotionID>'
											,'<bitPromotionTriggered>0</bitPromotionTriggered>'
											,'<vcPromotionDetail></vcPromotionDetail>'
											,'<numSortOrder>',@k,'</numSortOrder>')
									
					FROM 
						@TempVendorItems T1
					INNER JOIN
						Item 
					ON
						T1.numItemCode=Item.numItemCode
					WHERE 
						ID=@k
					
					SET @strItems = CONCAT(@strItems,'</Item>')

					SET @k = @k + 1
				END

				SET @strItems = CONCAT(@strItems,'</NewDataSet>')

				BEGIN TRY
					SET @numNewOppID = 0

					EXEC USP_OppManage 
						@numNewOppID OUTPUT
						,@numContactID
						,@numTempVendorID
						,0
						,''
						,''
						,0
						,@numUserCntID
						,0
						,@numAssignedTo
						,@numDomainID
						,@strItems
						,''
						,@dtEstimatedCloseDate
						,0
						,0
						,2
						,0
						,0
						,0
						,@numCurrencyID
						,@tintOppStatus
						,@numReOrderPointOrderStatus
						,NULL
						,0
						,0
						,0
						,0
						,1
						,0
						,0
						,@bitBillingTerms
						,@intBillingDays
						,@bitInterestType
						,@fltInterest
						,0
						,0
						,NULL
						,''
						,NULL
						,NULL
						,NULL
						,NULL
						,0
						,0
						,0
						,@intShippingCompany
						,0
						,NULL
						,0
						,0
						,0
						,0
						,0
						,0
						,0
						,@numDefaultShippingServiceID
						,0
						,''

					EXEC USP_OpportunityMaster_CT @numDomainID,@numUserCntID,@numNewOppID
				END TRY
				BEGIN CATCH
					DECLARE @ErrorMessage NVARCHAR(4000)
					DECLARE @ErrorNumber INT
					DECLARE @ErrorSeverity INT
					DECLARE @ErrorState INT
					DECLARE @ErrorLine INT
					DECLARE @ErrorProcedure NVARCHAR(200)

					IF @@TRANCOUNT > 0
						ROLLBACK TRANSACTION;

					SET @numOppID = 0

					SELECT 
						@ErrorMessage = ERROR_MESSAGE(),
						@ErrorNumber = ERROR_NUMBER(),
						@ErrorSeverity = ERROR_SEVERITY(),
						@ErrorState = ERROR_STATE(),
						@ErrorLine = ERROR_LINE(),
						@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

					RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);
				END CATCH
			END

			-- ASSEMBLY ITEMS
			DELETE FROM @TempVendorItems
			INSERT INTO @TempVendorItems
			(
				ID
				,numItemCode
				,bitAssembly
				,bitKit
				,numWarehouseItemID
				,numUnitHour
				,monCost
			)
			SELECT
				ROW_NUMBER() OVER(ORDER BY numItemCode ASC)
				,numItemCode
				,bitAssembly
				,bitKit
				,numWarehouseItemID
				,(CASE 
					WHEN @tintUnitsRecommendationForAutoPOBackOrder=1 --Add �Re-Order Qty� or �Vendor Min Order Qty� (whichever is greater) + �Back Order Qty� - �On Order + Requisitions�
					THEN 
						(CASE WHEN ISNULL(numReorderQty,0) > ISNULL(numVendorMinQty,0) THEN ISNULL(numReorderQty,0) ELSE ISNULL(numVendorMinQty,0) END) + ISNULL(numBackOrder,0) - (ISNULL(numOnOrder,0) + (CASE WHEN @bitIncludeRequisitions = 1 THEN ISNULL(numRequisitions,0) ELSE 0 END))
					WHEN @tintUnitsRecommendationForAutoPOBackOrder=2 --Add �Re-Order Qty� or �Vendor Min Order Qty� or �Back-Order Qty� (whichever is greater) - �On Order + Requisitions�
					THEN 
						(CASE
							WHEN ISNULL(numReorderQty,0) >= ISNULL(numVendorMinQty,0) AND ISNULL(numReorderQty,0) >= ISNULL(numBackOrder,0) THEN ISNULL(numReorderQty,0)
							WHEN ISNULL(numVendorMinQty,0) >= ISNULL(numReorderQty,0) AND ISNULL(numVendorMinQty,0) >= ISNULL(numBackOrder,0) THEN ISNULL(numVendorMinQty,0)
							WHEN ISNULL(numBackOrder,0) >= ISNULL(numReorderQty,0) AND ISNULL(numBackOrder,0) >= ISNULL(numVendorMinQty,0) THEN ISNULL(numBackOrder,0)
							ELSE ISNULL(numReorderQty,0)
						END) - (ISNULL(numOnOrder,0) + (CASE WHEN @bitIncludeRequisitions = 1 THEN ISNULL(numRequisitions,0) ELSE 0 END))
					WHEN @tintUnitsRecommendationForAutoPOBackOrder=3 --Add �On-Order Qty� � + Requisitions� - �Back Order� + �Re-Order Qty� or �Vendor Min Order Qty� (whichever is greater)
					THEN
						(ISNULL(numOnOrder,0) + (CASE WHEN @bitIncludeRequisitions = 1 THEN ISNULL(numRequisitions,0) ELSE 0 END)) - ISNULL(numBackOrder,0) + (CASE WHEN ISNULL(numReorderQty,0) >= ISNULL(numVendorMinQty,0) THEN ISNULL(numReorderQty,0) ELSE ISNULL(numVendorMinQty,0) END)
					ELSE 0 
				END)
				,monCost
			FROM
				@TEMPBackOrderItems
			WHERE
				numVendorID=@numTempVendorID
				AND ISNULL(bitAssembly,0)=1 
				AND ISNULL(bitKit,0)=0
				AND (CASE 
						WHEN @tintUnitsRecommendationForAutoPOBackOrder=1 --Add �Re-Order Qty� or �Vendor Min Order Qty� (whichever is greater) + �Back Order Qty� - �On Order + Requisitions�
						THEN ISNULL(numReorderQty,0) + ISNULL(numBackOrder,0) - (ISNULL(numOnOrder,0) + ISNULL(numRequisitions,0))
						WHEN @tintUnitsRecommendationForAutoPOBackOrder=2 --Add �Re-Order Qty� or �Vendor Min Order Qty� or �Back-Order Qty� (whichever is greater) - �On Order + Requisitions�
						THEN ISNULL(numReorderQty,0) - (ISNULL(numOnOrder,0) + ISNULL(numRequisitions,0))
						WHEN @tintUnitsRecommendationForAutoPOBackOrder=3 --Add �On-Order Qty� � + Requisitions� - �Back Order� + �Re-Order Qty� or �Vendor Min Order Qty� (whichever is greater)
						THEN
							(ISNULL(numOnOrder,0) + (CASE WHEN @bitIncludeRequisitions = 1 THEN ISNULL(numRequisitions,0) ELSE 0 END)) - ISNULL(numBackOrder,0) + (CASE WHEN ISNULL(numReorderQty,0) >= ISNULL(numVendorMinQty,0) THEN ISNULL(numReorderQty,0) ELSE ISNULL(numVendorMinQty,0) END)
						ELSE 0 
					END) > 0

			IF (SELECT COUNT(*) FROM @TempVendorItems) > 0
			BEGIN
				SET @k = 1
				SELECT @kCount=COUNT(*) FROM @TempVendorItems

				WHILE @k <= @kCount
				BEGIN
					SELECT
						@numTempItemCode=numItemCode
						,@numTempWarehouseItemID=numWarehouseItemID
						,@numTempUnitHour=numUnitHour
					FROM 
						@TempVendorItems 
					WHERE 
						ID=@k

					BEGIN TRY
						EXEC USP_ManageWorkOrder 
							@numTempItemCode,
							@numTempWarehouseItemID,
							@numTempUnitHour,
							@numDomainID,
							@numUserCntID,
							'Auto created work order for back orders',
							NULL,
							@numAssignedTo
					END TRY
					BEGIN CATCH
						--DO NOT THROW ERROR
					END CATCH

					SET @k = @k + 1
				END
			END

			SET @i = @i + 1
		END       
	END
END
