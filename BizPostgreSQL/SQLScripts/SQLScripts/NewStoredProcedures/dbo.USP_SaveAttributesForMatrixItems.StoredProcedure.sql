GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_SaveAttributesForMatrixItems' ) 
    DROP PROCEDURE USP_SaveAttributesForMatrixItems
GO

--05052018 Change: Procedure Created to update Attrbutes for Matrix Items
CREATE PROCEDURE [dbo].[USP_SaveAttributesForMatrixItems]  
	@numDomainID AS NUMERIC(18,0),
	@numUserCntID AS NUMERIC(18,0),
	@numWareHouseItemID AS NUMERIC(18,0) OUTPUT,
	@numItemCode NUMERIC(18,0),
	@monWListPrice AS DECIMAL(20,5),
	@vcWHSKU AS VARCHAR(100),
	@vcBarCode AS VARCHAR(100),
	@strFieldList AS TEXT

AS
BEGIN
BEGIN TRY
BEGIN TRANSACTION
	DECLARE @bitMatrix AS BIT = 0
	DECLARE @numItemGroup NUMERIC(18,0)
	DECLARE @vcDescription AS VARCHAR(100)
	DECLARE @numGlobalWarehouseItemID NUMERIC(18,0) = 0
	DECLARE @monPrice AS DECIMAL(20,5)
	DECLARE @vcSKU AS VARCHAR(200) = ''
	DECLARE @vcUPC AS VARCHAR(200) = ''
	DECLARE @bitRemoveGlobalLocation BIT

	SELECT 
		@numItemGroup = ISNULL(Item.numItemGroup,0),
		@monPrice=ISNULL(Item.monListPrice,0),
		@bitMatrix = ISNULL(bitMatrix,0),
		@vcSKU = ISNULL(vcSKU,''),
		@vcUPC = ISNULL(numBarCodeId,'')

	FROM 
		Item 
	INNER JOIN
		Domain
	ON
		Item.numDomainID = Domain.numDomainId
	WHERE 
		numItemCode=@numItemCode 
		AND Item.numDomainID=@numDomainID

 
	BEGIN --UPDATE
		UPDATE 
			WareHouseItems  
		SET 
			vcWHSKU=(CASE WHEN @numItemGroup > 0 AND ISNULL(@bitMatrix,0) = 1 THEN @vcSKU ELSE @vcWHSKU END),
			vcBarCode=(CASE WHEN @numItemGroup > 0 AND ISNULL(@bitMatrix,0) = 1 THEN @vcUPC ELSE @vcBarCode END),
			dtModified=GETDATE()   
		WHERE 
			numItemID=@numItemCode 
			AND numDomainID=@numDomainID 
			AND numWareHouseItemID=@numWareHouseItemID


		 --WAREHOUSE LOCATION LEVEL DIFFERENT ITEM PRICE IS AVAILABLE FOR MATRIX ITEM ONLY
		IF @numItemGroup > 0 AND ISNULL(@bitMatrix,0) = 0
		BEGIN
			UPDATE WareHouseItems SET monWListPrice = @monWListPrice WHERE numDomainID=@numDomainID AND numItemID = @numItemCode AND numWareHouseItemID = @numWareHouseItemID
		END

		SET @vcDescription='UPDATE WareHouse'
	END

	-- SAVE MATRIX ITEM ATTRIBUTES
	IF DATALENGTH(@strFieldList)>2
	BEGIN
		--INSERT CUSTOM FIELDS BASE ON WAREHOUSEITEMS
		DECLARE @hDoc AS INT     
		EXEC sp_xml_preparedocument @hDoc OUTPUT, @strFieldList                                                                        

		
	    DECLARE @TempTable TABLE 
		(
			ID INT IDENTITY(1,1),
			Fld_ID NUMERIC(18,0),
			Fld_Value VARCHAR(300)
		)   
	                                      
		INSERT INTO @TempTable 
		(
			Fld_ID,
			Fld_Value
		)    
		SELECT
			*          
		FROM 
			OPENXML (@hDoc,'/NewDataSet/CusFlds',2)                                                  
		WITH 
			(Fld_ID NUMERIC(18,0),Fld_Value VARCHAR(300))                                        
	                                                                           

		IF (SELECT COUNT(*) FROM @TempTable) > 0                                        
		BEGIN                           
			DELETE 
				CFW_Fld_Values_Serialized_Items 
			FROM 
				CFW_Fld_Values_Serialized_Items C                                        
			INNER JOIN 
				@TempTable T 
			ON 
				C.Fld_ID=T.Fld_ID 
			WHERE 
				C.RecId= @numWareHouseItemID  
	      
			INSERT INTO CFW_Fld_Values_Serialized_Items
			(
				Fld_ID,
				Fld_Value,
				RecId,
				bitSerialized
			)                                                  
			SELECT 
				Fld_ID,
				ISNULL(Fld_Value,'') AS Fld_Value,
				@numWareHouseItemID,
				0 
			FROM 
				@TempTable  


			IF ISNULL(@bitMatrix,0) = 1
			BEGIN
				DELETE 
					CFW_Fld_Values_Serialized_Items 
				FROM 
					CFW_Fld_Values_Serialized_Items C                                        
				INNER JOIN 
					@TempTable T 
				ON 
					C.Fld_ID=T.Fld_ID 
				WHERE 
					C.RecId= @numGlobalWarehouseItemID  
	      
				INSERT INTO CFW_Fld_Values_Serialized_Items
				(
					Fld_ID,
					Fld_Value,
					RecId,
					bitSerialized
				)                                                  
				SELECT 
					Fld_ID,
					ISNULL(Fld_Value,'') AS Fld_Value,
					@numGlobalWarehouseItemID,
					0 
				FROM 
					@TempTable
			END
		END  

		EXEC sp_xml_removedocument @hDoc 
	END	  


 
	DECLARE @dtDATE AS DATETIME = GETUTCDATE()

	EXEC dbo.USP_ManageWareHouseItems_Tracking
		@numWareHouseItemID = @numWareHouseItemID,
		@numReferenceID = @numItemCode,
		@tintRefType = 1,
		@vcDescription = @vcDescription,
		@numModifiedBy = @numUserCntID,
		@ClientTimeZoneOffset = 0,
		@dtRecordDate = @dtDATE,
		@numDomainID = @numDomainID 

	IF ISNULL(@numGlobalWarehouseItemID,0) > 0
	BEGIN        
		EXEC dbo.USP_ManageWareHouseItems_Tracking
		@numWareHouseItemID = @numGlobalWarehouseItemID,
		@numReferenceID = @numItemCode,
		@tintRefType = 1,
		@vcDescription = @vcDescription,
		@numModifiedBy = @numUserCntID,
		@ClientTimeZoneOffset = 0,
		@dtRecordDate = @dtDATE,
		@numDomainID = @numDomainID 
	END

	UPDATE Item SET bintModifiedDate=@dtDATE,numModifiedBy=@numUserCntID WHERE numItemCode=@numItemCode AND numDomainID=@numDomainID
COMMIT
END TRY
BEGIN CATCH
	DECLARE @ErrorMessage NVARCHAR(4000)
	DECLARE @ErrorNumber INT
	DECLARE @ErrorSeverity INT
	DECLARE @ErrorState INT
	DECLARE @ErrorLine INT
	DECLARE @ErrorProcedure NVARCHAR(200)

	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;

	SET @numWareHouseItemID = 0

	SELECT 
		@ErrorMessage = ERROR_MESSAGE(),
		@ErrorNumber = ERROR_NUMBER(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE(),
		@ErrorLine = ERROR_LINE(),
		@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

	RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);
END CATCH
END

SET ANSI_NULLS ON
