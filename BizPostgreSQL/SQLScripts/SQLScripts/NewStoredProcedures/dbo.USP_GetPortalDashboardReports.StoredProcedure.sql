GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetPortalDashboardReports')
DROP PROCEDURE USP_GetPortalDashboardReports
GO
CREATE PROCEDURE USP_GetPortalDashboardReports
    @numDomainID NUMERIC(9),
    @numRelationshipID NUMERIC(9),
    @numProfileID NUMERIC(9),
    @numContactID NUMERIC=0
AS 
BEGIN
    SELECT  numReportID,
            vcReportName,
            tintDisplayOrder
    FROM    dbo.PortalDashboardReports
    WHERE numReportID NOT IN(SELECT numReportID FROM dbo.PortalDashboardDtl WHERE  numDomainID = @numDomainID
            AND numRelationshipID = @numRelationshipID
            AND numProfileID = @numProfileID AND numContactID = @numContactID)
    ORDER BY tintDisplayOrder
    
    SELECT  PD.numPortalDashboardDtlID,
            PD.numDomainID,
            PD.numRelationshipID,
            PD.numProfileID,
            PD.numReportID,
            PD.tintColumnNumber,
            PD.tintRowOrder,
            vcReportName
    FROM    dbo.PortalDashboardReports PDR
            INNER JOIN dbo.PortalDashboardDtl PD ON PD.numReportID = PDR.numReportID
    WHERE   numDomainID = @numDomainID
            AND numRelationshipID = @numRelationshipID
            AND numProfileID = @numProfileID
            AND tintColumnNumber=1
            AND numContactID = @numContactID
	ORDER BY PD.tintRowOrder
            
	SELECT  PD.numPortalDashboardDtlID,
            PD.numDomainID,
            PD.numRelationshipID,
            PD.numProfileID,
            PD.numReportID,
            PD.tintColumnNumber,
            PD.tintRowOrder,
            vcReportName
    FROM    dbo.PortalDashboardReports PDR
            INNER JOIN dbo.PortalDashboardDtl PD ON PD.numReportID = PDR.numReportID
    WHERE   numDomainID = @numDomainID
            AND numRelationshipID = @numRelationshipID
            AND numProfileID = @numProfileID
            AND tintColumnNumber=2
            AND numContactID = @numContactID
    ORDER BY PD.tintRowOrder
END