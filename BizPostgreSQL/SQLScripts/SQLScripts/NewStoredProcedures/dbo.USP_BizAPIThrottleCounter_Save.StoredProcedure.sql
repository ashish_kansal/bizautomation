GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_BizAPIThrottleCounter_Save' ) 
    DROP PROCEDURE USP_BizAPIThrottleCounter_Save
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author: Sandeep Patel
-- Create date: 16 April 2014
-- Description:	Insert or Update Throttle counter for id
-- =============================================
CREATE PROCEDURE USP_BizAPIThrottleCounter_Save
	@ID VARCHAR(100),
	@TimeSpan DATETIME,
	@TotalRequest NUMERIC(18,0),
	@ExpirationTime BIGINT,
	@vcBizAPIAcessKey VARCHAR(50)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	IF EXISTS (SELECT * FROM BizAPIThrottleCounter WHERE vcID = @ID)
		UPDATE 
			BizAPIThrottleCounter
		SET
			dtTimeSpan = @TimeSpan,
			numTotalRequest = @TotalRequest,
			bintExpirationTime = @ExpirationTime,
			vcBizAPIAccessKey = @vcBizAPIAcessKey
		WHERE
			vcID = @ID
	ELSE
		INSERT INTO BizAPIThrottleCounter (vcID,dtTimeSpan,numTotalRequest,bintExpirationTime,vcBizAPIAccessKey) VALUES (@ID,@TimeSpan,@TotalRequest,@ExpirationTime,@vcBizAPIAcessKey)
	
END
GO