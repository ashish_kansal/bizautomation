SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_DemandForecast_Get')
DROP PROCEDURE dbo.USP_DemandForecast_Get
GO
CREATE PROCEDURE [dbo].[USP_DemandForecast_Get]
	@numDomainID NUMERIC (18,0),
	@numPageIndex INTEGER,
	@numPageSize INTEGER
AS 
BEGIN

	DECLARE @TotalRowCount AS INTEGER = 0
	SELECT @TotalRowCount = COUNT(*) FROM DemandForecast WHERE numDomainID = @numDomainID

	SELECT
		*
	FROM
		(
			SELECT
				ROW_NUMBER() OVER(ORDER BY DemandForecast.numDFID) AS NUMBER,
				DemandForecast.numDomainID,
				DemandForecast.numDFID,
				DemandForecast.bitIncludeOpenReleaseDates,
				LEFT(TableItemClassification.list,LEN(TableItemClassification.list) - 1) AS vcItemClassification,
				LEFT(TableItemGroup.list,LEN(TableItemGroup.list) - 1) AS vcItemGroups,
				LEFT(TableWarehouse.list,LEN(TableWarehouse.list) - 1) AS vcWarehouse,
				DemandForecastAnalysisPattern.vcAnalysisPattern AS vcAnalysisPattern,
				DemandForecastDays.vcDays AS vcForecastDays,
				ISNULL(@TotalRowCount,0) AS TotalRowCount
			FROM
				DemandForecast
			LEFT JOIN
				DemandForecastAnalysisPattern
			ON
				DemandForecastAnalysisPattern.numDFAPID  = DemandForecast.numDFAPID
			LEFT JOIN
				DemandForecastDays
			ON
				DemandForecastDays.numDFDaysID = DemandForecast.numDFDaysID
			CROSS APPLY 
				( 
					SELECT 
						vcData + ', ' AS [text()] 
					FROM 
						ListDetails 
					WHERE 
						numListItemID IN (
											SELECT 
												numItemClassificationID 
											FROM 
												DemandForecastItemClassification 
											WHERE 
												numDFID = DemandForecast.numDFID)
					FOR XML PATH('') 
				) TableItemClassification (list)
				CROSS APPLY 
				( 
					SELECT 
						vcItemGroup + ', ' AS [text()] 
					FROM 
						ItemGroups 
					WHERE 
						numItemGroupID IN (
											SELECT 
												numItemGroupID 
											FROM 
												DemandForecastItemGroup
											WHERE 
												numDFID = DemandForecast.numDFID)
					FOR XML PATH('') 
				) TableItemGroup (list)
				CROSS APPLY 
				( 
					SELECT 
						vcWareHouse + ', ' AS [text()] 
					FROM 
						Warehouses 
					WHERE 
						numWareHouseID IN (
											SELECT 
												numWareHouseID 
											FROM 
												DemandForecastWarehouse
											WHERE 
												numDFID = DemandForecast.numDFID)
					FOR XML PATH('') 
				) TableWarehouse (list)
			WHERE
				DemandForecast.numDomainID = @numDomainID
		) AS TEMP
	WHERE
		NUMBER BETWEEN ((@numPageIndex - 1) * @numPageSize + 1) AND (@numPageIndex * @numPageSize)
END
