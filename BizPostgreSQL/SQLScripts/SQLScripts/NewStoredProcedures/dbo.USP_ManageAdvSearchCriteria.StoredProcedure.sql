GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_ManageAdvSearchCriteria' ) 
    DROP PROCEDURE USP_ManageAdvSearchCriteria
GO
CREATE PROCEDURE USP_ManageAdvSearchCriteria
    @numContactID AS NUMERIC,
    @numDomainID AS NUMERIC,
    @numFormID AS NUMERIC,
    @strItems TEXT,
    @tintMode AS TINYINT
AS 
BEGIN
    IF @tintMode = 0 
        BEGIN
            SELECT  vcFormFieldName,
                    intSearchOperator
            FROM    dbo.AdvSearchCriteria
            WHERE   numFormID = @numFormID
                    AND numContactID = @numContactID
                    AND numDomainID = @numDomainID
        END

    IF @tintMode = 1 
        BEGIN
	

            DELETE  FROM dbo.AdvSearchCriteria
            WHERE   numContactID = @numContactID
                    AND numFormID = @numFormID
                    AND numDomainID = @numDomainID 

            DECLARE @hDocItem INT
            IF CONVERT(VARCHAR(10), @strItems) <> '' 
                BEGIN
                    EXEC sp_xml_preparedocument @hDocItem OUTPUT, @strItems
            
                    INSERT  INTO dbo.AdvSearchCriteria ( numDomainID,
                                                         numFormID,
                                                         vcFormFieldName,
                                                         numContactID,
                                                         intSearchOperator )
                            SELECT  @numDomainID,
                                    @numFormID,
                                    X.vcFormFieldName,
                                    @numContactID,
                                    X.intSearchOperator
                            FROM    ( SELECT    *
                                      FROM      OPENXML (@hDocItem, '/NewDataSet/Table', 2)
                                                WITH ( vcFormFieldName VARCHAR(50), intSearchOperator INT )
                                    ) X
                    EXEC sp_xml_removedocument @hDocItem
                END
            SELECT  ''
        END	
END