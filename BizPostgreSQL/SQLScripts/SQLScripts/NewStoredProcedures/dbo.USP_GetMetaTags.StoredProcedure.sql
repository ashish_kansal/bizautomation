
GO
/****** Object:  StoredProcedure [dbo].[USP_GetMetaTags]    Script Date: 08/08/2009 16:14:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetMetaTags')
DROP PROCEDURE USP_GetMetaTags
GO
CREATE PROCEDURE [dbo].[USP_GetMetaTags]
                @numMetaID      NUMERIC(9)  = 0,
                @tintMetatagFor TINYINT, -- 1 for Page, 2 for Items,
                @numReferenceID NUMERIC(9)
AS
  BEGIN
    IF @numMetaID <> 0
    BEGIN
		SELECT 
			[vcPageTitle],
			[vcMetaKeywords],
			[vcMetaDescription],
			[numReferenceID]
		FROM 
			[MetaTags]
		WHERE 
			[tintMetatagFor] = @tintMetatagFor
    END
    ELSE
    BEGIN
        SELECT 
			[vcPageTitle],
			[vcMetaKeywords],
            [vcMetaDescription],
            [numMetaID]
        FROM 
			[MetaTags]
        WHERE 
			[numReferenceID] = @numReferenceID 
			AND tintMetatagFor =  @tintMetatagFor 
    END
  END

