GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj  
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetKitChildsFirstLevel')
DROP PROCEDURE USP_GetKitChildsFirstLevel
GO
CREATE PROCEDURE [dbo].[USP_GetKitChildsFirstLevel]        
	@numDomainID as numeric(9)=0,
	@strItem as varchar(1000),
	@numItemCode AS NUMERIC(18,0) = 0,
	@numPageIndex AS INT,
	@numPageSize AS INT,     
	@TotalCount AS INT OUTPUT
AS
BEGIN
	DECLARE @strSQL AS NVARCHAR(MAX) = ''
	DECLARE @vcWareHouseSearch VARCHAR(1000) = ''
	DECLARE @vcVendorSearch VARCHAR(1000) = ''
	DECLARE @TEMPitems TABLE 
	( 
		numItemCode NUMERIC(18,0)
	)
	DECLARE @tintDecimalPoints AS TINYINT

	SELECT  
		@tintDecimalPoints=ISNULL(tintDecimalPoints,4)
	FROM    
		domain
	WHERE  
		numDomainID = @numDomainID
               
	SELECT 
		*
	INTO 
		#Temp1
	FROM
	( 
		SELECT    
			numFieldId ,
			vcDbColumnName ,
			ISNULL(vcCultureFieldName, vcFieldName) AS vcFieldName ,
			tintRow AS tintOrder ,
			0 AS Custom
		FROM 
			View_DynamicColumns
		WHERE 
			numFormId = 22
			AND numDomainID = @numDomainID
			AND tintPageType = 1
			AND bitCustom = 0
			AND ISNULL(bitSettingField, 0) = 1
			AND numRelCntType = 0
		UNION
		SELECT 
			numFieldId ,
			vcFieldName ,
			vcFieldName ,
			tintRow AS tintOrder ,
			1 AS Custom
		FROM
			View_DynamicCustomColumns
		WHERE
			Grp_id = 5
			AND numFormId = 22
			AND numDomainID = @numDomainID
			AND tintPageType = 1
			AND bitCustom = 1
			AND numRelCntType = 0
	) X 
  
	IF NOT EXISTS (SELECT * FROM #Temp1)
	BEGIN
		INSERT INTO 
			#Temp1
		SELECT 
			numFieldId
			,vcDbColumnName
			,ISNULL(vcCultureFieldName, vcFieldName) AS vcFieldName
			,tintorder
			,0
		FROM
			View_DynamicDefaultColumns
		WHERE 
			numFormId = 22
			AND bitDefault = 1
			AND ISNULL(bitSettingField, 0) = 1
			AND numDomainID = @numDomainID 
	END

	DECLARE @tintOrder AS INT
    DECLARE @Fld_id AS NVARCHAR(20)
    DECLARE @Fld_Name AS VARCHAR(20)
        
		

    SELECT TOP 1
        @tintOrder = tintOrder + 1 ,
        @Fld_id = numFieldId ,
        @Fld_Name = vcFieldName
    FROM
		#Temp1
    WHERE
		Custom = 1
    ORDER BY 
		tintOrder

    WHILE @tintOrder > 0
    BEGIN
        SET @strSQL = @strSQL + ', dbo.GetCustFldValueItem('+ @Fld_id + ', I.numItemCode) as [' + @Fld_Name + ']'

        SELECT TOP 1
            @tintOrder = tintOrder + 1 ,
            @Fld_id = numFieldId ,
            @Fld_Name = vcFieldName
        FROM 
			#Temp1
        WHERE 
			Custom = 1
			AND tintOrder >= @tintOrder
        ORDER BY 
			tintOrder
            
		IF @@rowcount = 0
            SET @tintOrder = 0
    END

	--Temp table for Item Search Configuration

    SELECT 
		*
    INTO
		#tempSearch
    FROM
	(
		SELECT
			numFieldId,
            vcDbColumnName ,
            ISNULL(vcCultureFieldName, vcFieldName) AS vcFieldName ,
            tintRow AS tintOrder ,
            0 AS Custom
        FROM
			View_DynamicColumns
        WHERE
			numFormId = 22
            AND numDomainID = @numDomainID
            AND tintPageType = 1
            AND bitCustom = 0
            AND ISNULL(bitSettingField, 0) = 1
            AND numRelCntType = 1
        UNION
        SELECT
			numFieldId,
            vcFieldName ,
            vcFieldName ,
            tintRow AS tintOrder ,
            1 AS Custom
		FROM
			View_DynamicCustomColumns
        WHERE
			Grp_id = 5
            AND numFormId = 22
            AND numDomainID = @numDomainID
            AND tintPageType = 1
            AND bitCustom = 1
            AND numRelCntType = 1
    ) X 
  
    IF NOT EXISTS(SELECT * FROM #tempSearch)
    BEGIN
		INSERT INTO 
			#tempSearch
        SELECT 
			numFieldId ,
            vcDbColumnName ,
            ISNULL(vcCultureFieldName, vcFieldName) AS vcFieldName ,
            tintorder ,
            0
        FROM 
			View_DynamicDefaultColumns
        WHERE 
			numFormId = 22
            AND bitDefault = 1
            AND ISNULL(bitSettingField, 0) = 1
            AND numDomainID = @numDomainID 
    END

	DECLARE @strSearch AS NVARCHAR(MAX)
	,@CustomSearch AS NVARCHAR(MAX)
	,@numFieldId AS NUMERIC(18,0)

	SET @strSearch = ''
        
    SELECT TOP 1
        @tintOrder = tintOrder + 1 ,
        @Fld_Name = vcDbColumnName ,
        @numFieldId = numFieldId
    FROM
		#tempSearch
    WHERE
		Custom = 0
    ORDER BY
		tintOrder
        
	WHILE @tintOrder > -1
    BEGIN
        IF @Fld_Name = 'vcPartNo'
		BEGIN
            INSERT INTO @TEMPitems
            SELECT DISTINCT
                    Item.numItemCode
            FROM    Vendor
                    JOIN Item ON Item.numItemCode = Vendor.numItemCode
            WHERE   Item.numDomainID = @numDomainID
                    AND Vendor.numDomainID = @numDomainID
                    AND Vendor.vcPartNo IS NOT NULL
                    AND LEN(Vendor.vcPartNo) > 0
                    AND vcPartNo LIKE '%' + @strItem + '%'
							
			SET @vcVendorSearch = 'dbo.Vendor.vcPartNo LIKE ''%' + @strItem + '%'''
		END
        ELSE IF @Fld_Name = 'vcBarCode'
		BEGIN
            INSERT INTO @TEMPitems
            SELECT DISTINCT
                    Item.numItemCode
            FROM    Item
                    JOIN WareHouseItems WI ON WI.numDomainID = @numDomainID
                                    AND Item.numItemCode = WI.numItemID
                    JOIN Warehouses W ON W.numDomainID = @numDomainID
                                    AND W.numWareHouseID = WI.numWareHouseID
            WHERE   Item.numDomainID = @numDomainID
                    AND ISNULL(Item.numItemGroup,
                                0) > 0
                    AND WI.vcBarCode IS NOT NULL
                    AND LEN(WI.vcBarCode) > 0
                    AND WI.vcBarCode LIKE '%' + @strItem + '%'

			SET @vcWareHouseSearch = 'dbo.WareHouseItems.vcBarCode LIKE ''%' + @strItem + '%'''
		END
        ELSE IF @Fld_Name = 'vcWHSKU'
		BEGIN
            INSERT INTO @TEMPitems
            SELECT DISTINCT
                    Item.numItemCode
            FROM    Item
                    JOIN WareHouseItems WI ON WI.numDomainID = @numDomainID
                                AND Item.numItemCode = WI.numItemID
                    JOIN Warehouses W ON W.numDomainID = @numDomainID
                                AND W.numWareHouseID = WI.numWareHouseID
            WHERE   Item.numDomainID = @numDomainID
                    AND ISNULL(Item.numItemGroup,
                                0) > 0
                    AND WI.vcWHSKU IS NOT NULL
                    AND LEN(WI.vcWHSKU) > 0
                    AND WI.vcWHSKU LIKE '%'
                    + @strItem + '%'

			IF LEN(@vcWareHouseSearch) > 0
				SET @vcWareHouseSearch = @vcWareHouseSearch + ' OR dbo.WareHouseItems.vcWHSKU LIKE ''%' + @strItem + '%'''
			ELSE
				SET @vcWareHouseSearch = 'dbo.WareHouseItems.vcWHSKU LIKE ''%' + @strItem + '%'''
		END
        ELSE
		BEGIN
            SET @strSearch = @strSearch + (CASE @Fld_Name WHEN 'numItemCode' THEN ' I.[' + @Fld_Name ELSE ' [' + @Fld_Name END)  + '] LIKE ''%' + @strItem + '%'''
		END

        SELECT TOP 1
            @tintOrder = tintOrder + 1 ,
            @Fld_Name = vcDbColumnName ,
            @numFieldId = numFieldId
        FROM
			#tempSearch
        WHERE
			tintOrder >= @tintOrder
			AND Custom = 0
        ORDER BY 
			tintOrder
                        
		IF @@rowcount = 0
        BEGIN
            SET @tintOrder = -1
        END
        ELSE
		BEGIN
            IF @Fld_Name != 'vcPartNo' AND @Fld_Name != 'vcBarCode' AND @Fld_Name != 'vcWHSKU'
            BEGIN
                SET @strSearch = @strSearch + ' or '
            END
		END
    END

    IF (SELECT COUNT(*) FROM @TEMPitems) > 0
	BEGIN
        SET @strSearch = @strSearch + ' OR  I.numItemCode IN (select numItemCode from #tempItemCode)' 
	END

	--Custom Search
    SET @CustomSearch = ''
    SELECT TOP 1
        @tintOrder = tintOrder + 1 ,
        @Fld_Name = vcDbColumnName ,
        @numFieldId = numFieldId
    FROM 
		#tempSearch
    WHERE
		Custom = 1
    ORDER BY 
		tintOrder

    WHILE @tintOrder > -1
    BEGIN
        SET @CustomSearch = @CustomSearch + CAST(@numFieldId AS VARCHAR(10)) 

        SELECT TOP 1
            @tintOrder = tintOrder + 1 ,
            @Fld_Name = vcDbColumnName ,
            @numFieldId = numFieldId
        FROM 
			#tempSearch
        WHERE 
			tintOrder >= @tintOrder
			AND Custom = 1
        ORDER BY 
			tintOrder
            
		IF @@rowcount = 0
        BEGIN
            SET @tintOrder = -1
        END
        ELSE
        BEGIN
            SET @CustomSearch = @CustomSearch + ' , '
        END
    END
	
    IF LEN(@CustomSearch) > 0
    BEGIN
        SET @CustomSearch = ' I.numItemCode in (SELECT RecId FROM dbo.CFW_FLD_Values_Item WHERE Fld_ID IN ('
            + @CustomSearch + ') and dbo.GetCustFldValueItem(Fld_ID,RecId) like ''%'
            + @strItem + '%'')'

        IF LEN(@strSearch) > 0
            SET @strSearch = @strSearch + ' OR ' + @CustomSearch
        ELSE
            SET @strSearch = @CustomSearch  
    END

	DECLARE @strSQLCount NVARCHAR(MAX) = ''

	SET @strSQLCount = 'SET @TotalCount = ISNULL((SELECT 
										COUNT(I.numItemCode) 
									FROM 
										Item  I WITH (NOLOCK)
									LEFT JOIN 
										DivisionMaster D              
									ON 
										I.numVendorID=D.numDivisionID  
									LEFT JOIN 
										CompanyInfo C  
									ON 
										C.numCompanyID=D.numCompanyID
									LEFT JOIN 
										CustomerPartNumber  CPN  
									ON 
										I.numItemCode=CPN.numItemCode 
										AND CPN.numCompanyID=C.numCompanyID
									LEFT JOIN
										dbo.WareHouseItems WHT
									ON
										WHT.numItemID = I.numItemCode AND
										WHT.numWareHouseItemID = (' +
																		CASE LEN(@vcWareHouseSearch)
																		WHEN 0 
																		THEN 
																			'SELECT 
																				TOP 1 numWareHouseItemID 
																			FROM 
																				dbo.WareHouseItems 
																			WHERE 
																				dbo.WareHouseItems.numDomainID = ' + CAST(@numDomainID AS VARCHAR(20)) + ' 
																				AND dbo.WareHouseItems.numItemID = I.numItemCode '
																		ELSE
																			'CASE 
																				WHEN (SELECT 
																							COUNT(*) 
																						FROM 
																							dbo.WareHouseItems 
																						WHERE 
																							dbo.WareHouseItems.numDomainID = ' + CAST(@numDomainID AS VARCHAR(20)) + ' 
																							AND dbo.WareHouseItems.numItemID = I.numItemCode 
																							AND (' + @vcWareHouseSearch + ')) > 0 
																				THEN
																					(SELECT 
																						TOP 1 numWareHouseItemID 
																					FROM 
																						dbo.WareHouseItems 
																					WHERE 
																						dbo.WareHouseItems.numDomainID = ' + CAST(@numDomainID AS VARCHAR(20)) + ' 
																						AND dbo.WareHouseItems.numItemID = I.numItemCode 
																						AND (' + @vcWareHouseSearch + '))
																				ELSE
																					(SELECT 
																						TOP 1 numWareHouseItemID 
																					FROM 
																						dbo.WareHouseItems 
																					WHERE 
																						dbo.WareHouseItems.numDomainID = ' + CAST(@numDomainID AS VARCHAR(20)) + ' 
																						AND dbo.WareHouseItems.numItemID = I.numItemCode 
																					)
																				END' 
																		END
																	+
																') 
									WHERE ((I.charItemType <> ''P'') OR ((SELECT COUNT(*) FROM dbo.WareHouseItems WHERE numItemID = I.numItemCode AND numDomainID = '
									+ CONVERT(VARCHAR(20), @numDomainID)
									+ ') > 0)) AND ISNULL(I.bitKitParent,0) = 0 AND ISNULL(I.bitAssembly,0) = 0 AND ISNULL(I.Isarchieve,0) <> 1 And I.charItemType NOT IN (''A'') AND  I.numDomainID='
									+ CONVERT(VARCHAR(20), @numDomainID) + ' AND ' + @strSearch  
						    

					SET @strSQLCount = @strSQLCount + '),0);'

                    SET @strSQL = 'select I.numItemCode, ISNULL(CPN.CustomerPartNo,'''')  AS CustomerPartNo,
									(SELECT TOP 1 vcPathForImage from ItemImages WHERE numItemCode = I.numItemCode) as vcPathForImage,
									(SELECT TOP 1 vcPathForTImage from ItemImages WHERE numItemCode = I.numItemCode) as vcPathForTImage,
									isnull(vcItemName,'''') vcItemName,
									CAST(CAST(CASE 
									WHEN I.[charItemType]=''P''
									THEN ISNULL((SELECT TOP 1 monWListPrice FROM [WareHouseItems] WHERE [numItemID]=I.numItemCode),0) 
									ELSE 
									monListPrice 
									END AS DECIMAL(20,' + CAST(@tintDecimalPoints AS VARCHAR) +')) AS VARCHAR) AS monListPrice,
									isnull(vcSKU,'''') vcSKU,
									isnull(numBarCodeId,0) numBarCodeId,
									isnull(vcModelID,'''') vcModelID,
									isnull(txtItemDesc,'''') as txtItemDesc,
									isnull(C.vcCompanyName,'''') as vcCompanyName,
									D.numDivisionID,
									WHT.numWareHouseItemID,
									dbo.fn_GetAttributes(WHT.numWareHouseItemId,I.bitSerialized) AS [vcAttributes],
									(CASE 
										WHEN ISNULL(I.bitKitParent,0) = 1 
										THEN 
											(
												CASE
													WHEN (SELECT COUNT(*) FROM ItemDetails INNER JOIN Item ON ItemDetails.numChildItemID = Item.numItemCode WHERE numItemKitID=I.numItemCode AND ISNULL(Item.bitKitParent,0) = 1) > 0
													THEN 1
													ELSE 0
												END
											)
										ELSE 0 
									END) bitHasKitAsChild
									,ISNULL(I.bitMatrix,0) AS bitMatrix
									,ISNULL(I.numItemGroup,0) AS numItemGroup
									,I.charItemType
									,ISNULL(I.bitKitParent,0) bitKitParent
									,ISNULL(I.bitAssembly,0) bitAssembly '
									+ @strSQL
									+ ' INTO #TEMPItems FROM Item  I WITH (NOLOCK)
									Left join DivisionMaster D              
									on I.numVendorID=D.numDivisionID  
									left join CompanyInfo C  
									on C.numCompanyID=D.numCompanyID
									LEFT JOIN CustomerPartNumber  CPN  
									ON I.numItemCode=CPN.numItemCode AND CPN.numCompanyID=C.numCompanyID
									LEFT JOIN
										dbo.WareHouseItems WHT
									ON
										WHT.numItemID = I.numItemCode AND
										WHT.numWareHouseItemID = (' +
																		CASE LEN(@vcWareHouseSearch)
																		WHEN 0 
																		THEN 
																			'SELECT 
																				TOP 1 numWareHouseItemID 
																			FROM 
																				dbo.WareHouseItems 
																			WHERE 
																				dbo.WareHouseItems.numDomainID = ' + CAST(@numDomainID AS VARCHAR(20)) + ' 
																				AND dbo.WareHouseItems.numItemID = I.numItemCode '
																		ELSE
																			'CASE 
																				WHEN (SELECT 
																							COUNT(*) 
																						FROM 
																							dbo.WareHouseItems 
																						WHERE 
																							dbo.WareHouseItems.numDomainID = ' + CAST(@numDomainID AS VARCHAR(20)) + ' 
																							AND dbo.WareHouseItems.numItemID = I.numItemCode 
																							AND (' + @vcWareHouseSearch + ')) > 0 
																				THEN
																					(SELECT 
																						TOP 1 numWareHouseItemID 
																					FROM 
																						dbo.WareHouseItems 
																					WHERE 
																						dbo.WareHouseItems.numDomainID = ' + CAST(@numDomainID AS VARCHAR(20)) + ' 
																						AND dbo.WareHouseItems.numItemID = I.numItemCode 
																						AND (' + @vcWareHouseSearch + '))
																				ELSE
																					(SELECT 
																						TOP 1 numWareHouseItemID 
																					FROM 
																						dbo.WareHouseItems 
																					WHERE 
																						dbo.WareHouseItems.numDomainID = ' + CAST(@numDomainID AS VARCHAR(20)) + ' 
																						AND dbo.WareHouseItems.numItemID = I.numItemCode 
																					)
																				END' 
																		END
																	+
																') 
									where ((I.charItemType <> ''P'') OR ((SELECT COUNT(*) FROM dbo.WareHouseItems WHERE numItemID = I.numItemCode AND numDomainID = '
									+ CONVERT(VARCHAR(20), @numDomainID) + ') > 0)) AND ISNULL(I.bitKitParent,0) = 0 AND ISNULL(I.bitAssembly,0) = 0 AND ISNULL(I.Isarchieve,0) <> 1 And I.charItemType NOT IN(''A'') AND  I.numDomainID='
									+ CONVERT(VARCHAR(20), @numDomainID) + ' AND ' + @strSearch   
						     
						

                       

	SET @strSQL = @strSQL + CONCAT(' ORDER BY vcItemName OFFSET ', (@numPageIndex - 1) * @numPageSize,' ROWS FETCH NEXT ',@numPageSize,' ROWS ONLY ')


	SET @strSQL = @strSQL + ' SELECT SRNO=ROW_NUMBER() OVER( ORDER BY vcItemName),(CASE WHEN (vcSKU=''' + @strItem + ''' OR ' + CAST(ISNULL(@numDomainID,0) AS VARCHAR) + '=209) THEN 1 ELSE 0 END) bitSKUMatch,* INTO #TEMPItemsFinal FROM #TEMPItems;'

	SET @strSQL = @strSQL + @strSQLCount

	SET @strSQL = @strSQL + ' SELECT * FROM #TEMPItemsFinal ORDER BY SRNO; DROP TABLE #TEMPItemsFinal; DROP TABLE #TEMPItems;'
						
	PRINT @strSQL
	EXEC sp_executeSQL @strSQL, N'@TotalCount INT OUTPUT', @TotalCount OUTPUT

	SELECT  *
	FROM    #Temp1
	WHERE   vcDbColumnName NOT IN ( 'vcPartNo', 'vcBarCode', 'vcWHSKU' )ORDER BY tintOrder 

	IF OBJECT_ID('tempdb..#Temp1') IS NOT NULL
	BEGIN
		DROP TABLE #Temp1
	END

	IF OBJECT_ID('tempdb..#tempSearch') IS NOT NULL
	BEGIN
		DROP TABLE #tempSearch
	END

END
GO