SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_RecordHistory_Get')
DROP PROCEDURE dbo.USP_RecordHistory_Get
GO
CREATE PROCEDURE [dbo].[USP_RecordHistory_Get]
(
	@numDomainID NUMERIC(18,0),
	@numModuleID NUMERIC(18,0),
	@numRecordID NUMERIC(18,0),
	@PageSize INT,
	@PageNumber INT,
	@TotalRecords INT OUTPUT
)
AS
BEGIN
	SELECT 
		@TotalRecords = COUNT(*) 
	FROM 
		RecordHistory 
	WHERE 
		RecordHistory.numDomainID = @numDomainID
		AND RecordHistory.numRHModuleMasterID = @numModuleID
		AND RecordHistory.numRecordID = @numRecordID

	SELECT 
		ISNULL(RecordHistory.vcEvent,'') vcEvent,
		ISNULL(UserMaster.vcUserName,'') vcUserName,
		ISNULL(RecordHistory.vcFieldName,'') vcFieldName,
		ISNULL(RecordHistory.vcOldValue,'') vcOldValue,
		ISNULL(RecordHistory.vcNewValue,'') vcNewValue,
		ISNULL(RecordHistory.vcDescription,'') vcDescription
	FROM 
		RecordHistory 
	LEFT JOIN
		UserMaster
	ON
		RecordHistory.numUserCntID = UserMaster.numUserDetailId
	WHERE
		RecordHistory.numDomainID = @numDomainID
		AND RecordHistory.numRHModuleMasterID = @numModuleID
		AND RecordHistory.numRecordID = @numRecordID
	ORDER BY 
		RecordHistory.dtDate DESC
	OFFSET 
		@PageSize * (@PageNumber - 1) ROWS
    FETCH NEXT 
		@PageSize
	ROWS ONLY OPTION (RECOMPILE);
END