GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetRedirectConfig')
	DROP PROCEDURE USP_GetRedirectConfig
GO

CREATE PROCEDURE [dbo].[USP_GetRedirectConfig]
	@numDomainID numeric (18, 0),
	@numSiteID numeric(18, 0),
	@numRedirectConfigID numeric(18, 0) =0,
	@CurrentPage int,                                                            
	@PageSize int,
	@TotRecs int OUTPUT
	
AS

DECLARE @firstRec AS INTEGER                                                            
DECLARE @lastRec AS INTEGER                                                            
SET @firstRec = ( @CurrentPage - 1 ) * @PageSize                                                            
SET @lastRec = ( @CurrentPage * @PageSize + 1 )                  
  

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

declare @strSql as varchar(8000)   
declare @strSql1 as varchar(8000)   
                                                         
set @strSql='SELECT
	[numRedirectConfigID],
	[numDomainId],
	[numUserCntId],
	[numSiteId],
	[vcOldUrl],
	[vcNewUrl],
	[numRedirectType],
	[numReferenceType],
	[numReferenceNo],
	[dtCreated],
	[dtModified] 
FROM [dbo].[RedirectConfig]
WHERE  [numSiteId] = ' + CONVERT(VARCHAR(15), @numSiteId)  +  ' AND (numDomainId =  '
 + CONVERT(VARCHAR(15), @numDomainID)  +  ' or '
 + CONVERT(VARCHAR(15), @numDomainID)  +  ' = 0) AND
	([numRedirectConfigID] =  '+ CONVERT(VARCHAR(15), @numRedirectConfigID)  +'
 or  '
 + CONVERT(VARCHAR(15), @numRedirectConfigID)  + ' = 0) AND bitIsActive = 1'

Create table #tempTable (numRedirectConfigID numeric(18),
		numDomainId numeric(18),
		numUserCntId numeric(18),
		numSiteId  numeric(18),
		vcOldUrl VARCHAR(500),    
		vcNewUrl VARCHAR(500),    
		numRedirectType numeric(18),  
		numReferenceType numeric(18),   
		numReferenceNo numeric(18),   
		dtCreated DATETIME,    
		dtModified DATETIME                                           
 ) 

INSERT into #tempTable execute (@strSql )

IF @PageSize > 0
BEGIN
SET @strSql1 = 'WITH Paging
         AS (SELECT Row_number() OVER(ORDER BY numRedirectConfigID) AS row,* from #tempTable) select * from Paging where  row >'+ convert(varchar(15),@firstRec) +' and row < '+ convert(varchar(15),@lastRec) 

END
ELSE
BEGIN
SET @strSql1 = 'SELECT  * from #tempTable'

END

 PRINT @strSql1
 exec (@strSql1)

 (SELECT @TotRecs = COUNT(*) from #tempTable)
        
 DROP TABLE #tempTable
 
-- SELECT @TotRecs
PRINT @TotRecs

GO
