GO
/****** Object:  StoredProcedure [dbo].[usp_UpdateStagePercentageDetails]    Script Date: 09/21/2010 12:00:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
----Created By Prasanta    
    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_UpdateTaskOrder')
DROP PROCEDURE USP_UpdateTaskOrder
GO
CREATE PROCEDURE [dbo].[USP_UpdateTaskOrder]
@numDomainID as numeric(9)=0,    
@vcSortedTask as VARCHAR(500)=''
as    
BEGIN
UPDATE ST SET ST.numOrder=Row# FROM StagePercentageDetailsTask AS ST
LEFT JOIN
(SELECT Items,ROW_NUMBER()  OVER(ORDER BY (SELECT 1)) AS Row# FROM Split(@vcSortedTask,',')) AS T
ON ST.numTaskId=T.Items
WHERE ST.numDomainID=@numDomainID AND ST.numTaskId=T.Items

RETURN 1
END 