GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_TicklerActItemsV2TopCount')
DROP PROCEDURE USP_TicklerActItemsV2TopCount
GO
CREATE Proc [dbo].[USP_TicklerActItemsV2TopCount]     
@numUserCntID as numeric(9)=null,                                                                        
@numDomainID as numeric(9)=null,                                                                        
@startDate as datetime,                                                                        
@endDate as datetime,                                                                    
@ClientTimeZoneOffset Int,
@numViewID AS NUMERIC(18,0),
@numTotalRecords AS NUMERIC(18,0) OUTPUT
AS
BEGIN
	DECLARE @bitGroupByOrder AS BIT =0
	DECLARE @numBatchID AS NUMERIC(18,0)=0
	DECLARE @bitIncludeSearch AS BIT =1
	DECLARE @tintPrintBizDocViewMode AS INT =1
	DECLARE @tintPendingCloseFilter AS INT =1
	DECLARE @numShippingZone AS NUMERIC(18,0)=0
	DECLARE @vcOrderStauts VARCHAR(MAX) ='' 
	DECLARE @tintFilterBy TINYINT =0
	DECLARE @vcFilterValue VARCHAR(MAX)=''
	,@numPageIndex INT=1
	,@vcSortColumn VARCHAR(100)=''
	,@vcSortOrder VARCHAR(4)=''
	,@numWarehouseID VARCHAR(100)
	,@bitRemoveBO BIT =0


	IF @numViewID = 4 OR @numViewID = 5 OR @numViewID = 6
	BEGIN
		SET @bitGroupByOrder = 1
	END
	DECLARE @tintPackingViewMode AS INT =1
	DECLARE @tintPackingMode AS INT
	DECLARE @tintInvoicingType AS INT 
	SELECT
		@tintPackingMode=tintPackingMode
		,@tintInvoicingType=tintInvoicingType
	FROM
		MassSalesFulfillmentConfiguration
	WHERE
		numDomainID = @numDomainID 

	DECLARE @tintCommitAllocation TINYINT
	DECLARE @numShippingServiceItemID NUMERIC(18,0)
	DECLARE @numDefaultSalesShippingDoc NUMERIC(18,0)
	SELECT 
		@tintCommitAllocation=ISNULL(tintCommitAllocation,1) 
		,@numShippingServiceItemID=ISNULL(numShippingServiceItemID,0)
		,@numDefaultSalesShippingDoc=ISNULL(numDefaultSalesShippingDoc,0)
	FROM 
		Domain 
	WHERE 
		numDomainId=@numDomainID 

	DECLARE @vcCustomSearchValue AS VARCHAR(MAX) ='' 
	SET @vcCustomSearchValue = REPLACE(@vcCustomSearchValue,'OpportunityMaster.dtItemReceivedDate','(SELECT TOP 1 OMInner.dtItemReceivedDate FROM OpportunityMaster OMInner INNER JOIN OpportunityItems OIInner ON OMInner.numOppID=OIInner.numOppID WHERE OMInner.tintOppType=2 AND OMInner.tintOppStatus=1 AND OIInner.numItemCode=OpportunityItems.numItemCode ORDER BY OMInner.dtItemReceivedDate DESC)')

CREATE TABLE #TEMPMSRecords
	(
		numOppID NUMERIC(18,0)
		,numOppItemID NUMERIC(18,0)
		,numOppBizDocID NUMERIC(18,0)
	)
CREATE TABLE #TempWareHouse
(
	ID INT IDENTITY(1,1) PRIMARY KEY,
	numWareHouseId NUMERIC(18,0)
)

	DECLARE @vcSQL NVARCHAR(MAX)
	DECLARE @vcSQLFinal NVARCHAR(MAX)
	DECLARE @WarehouseCount AS INT = 0
	DECLARE @I AS INT = 1
INSERT INTO #TempWareHouse 	SELECT 
		[numWareHouseID]         
   FROM [dbo].[Warehouses] W 
   LEFT JOIN
		AddressDetails AD
	ON
        W.numAddressID = AD.numAddressID
   WHERE 
		W.numDomainID=@numDomainID

	SELECT @WarehouseCount = COUNT(*) FROM #TempWareHouse
	WHILE(@I<=@WarehouseCount)
	BEGIN
		SELECT @numWarehouseID = numWareHouseId FROM #TempWareHouse WHERE ID=@I
		
	SET @vcSQL = CONCAT('INSERT INTO #TEMPMSRecords
						(
							numOppID
							,numOppItemID
							,numOppBizDocID
						)
						SELECT
							OpportunityMaster.numOppId
							,',(CASE WHEN @bitGroupByOrder = 1 THEN '0' ELSE 'OpportunityItems.numoppitemtCode' END),'
							,',(CASE 
									WHEN @numViewID = 4 OR @numViewID = 6
									THEN 'OpportunityBizDocs.numOppBizDocsId'  
									ELSE '0' 
								END),'
						FROM
							OpportunityMaster
						INNER JOIN
							DivisionMaster
						ON
							OpportunityMaster.numDivisionId = DivisionMaster.numDivisionID
						INNER JOIN
							CompanyInfo
						ON
							DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
						INNER JOIN 
							OpportunityItems
						ON
							OpportunityMaster.numOppId = OpportunityItems.numOppId
						INNER JOIN
							Item
						ON
							OpportunityItems.numItemCode = Item.numItemCode
						LEFT JOIN
							WareHouseItems
						ON
							OpportunityItems.numWarehouseItmsID = WareHouseItems.numWareHouseItemID
						',
						(CASE 
							WHEN ISNULL(@numBatchID,0) > 0 
							THEN 
								' INNER JOIN 
										MassSalesFulfillmentBatchOrders
								ON
									OpportunityMaster.numOppId = MassSalesFulfillmentBatchOrders.numOppID
									AND (OpportunityItems.numOppItemtCode = MassSalesFulfillmentBatchOrders.numOppItemID OR ISNULL(MassSalesFulfillmentBatchOrders.numOppItemID,0) = 0)'
							ELSE 
								'' 
						END)	
						,
						(CASE 
							WHEN @numViewID = 4 
							THEN
								'INNER JOIN 
									OpportunityBizDocs
								ON
									OpportunityMaster.numOppId = OpportunityBizDocs.numOppId
									AND OpportunityBizDocs.numBizDocID=287'
							WHEN @numViewID = 6
							THEN
								CONCAT('INNER JOIN 
											OpportunityBizDocs
										ON
											OpportunityMaster.numOppId = OpportunityBizDocs.numOppId
											AND OpportunityBizDocs.numBizDocID=',(CASE @tintPrintBizDocViewMode WHEN 2 THEN 29397 WHEN 3 THEN 296 WHEN 4 THEN 287 ELSE 55206 END))
							ELSE ''

						END)
						,'
						WHERE
							OpportunityMaster.numDomainId=@numDomainID
							AND OpportunityMaster.tintOppType = 1
							AND OpportunityMaster.tintOppStatus = 1
							AND ISNULL(OpportunityMaster.tintshipped,0) = 0
							AND 1 = (CASE 
										WHEN @numViewID IN (1,2) AND ISNULL(@bitRemoveBO,0) = 1
										THEN (CASE 
												WHEN (Item.charItemType = ''p'' AND ISNULL(OpportunityItems.bitDropShip, 0) = 0 AND ISNULL(Item.bitAsset,0)=0)
												THEN 
													(CASE WHEN dbo.CheckOrderItemInventoryStatus(OpportunityItems.numItemCode,(CASE 
				WHEN @numViewID = 1
				THEN ISNULL(OpportunityItems.numUnitHour,0) - ISNULL(OpportunityItems.numQtyPicked,0)
				WHEN @numViewID = 2 AND @tintPackingViewMode = 2
				THEN (CASE WHEN ISNULL(@tintPackingMode,1) = 2 THEN ISNULL(OpportunityItems.numQtyPicked,0) - ISNULL(OpportunityItems.numQtyShipped,0) ELSE ISNULL(OpportunityItems.numUnitHour,0) - ISNULL(OpportunityItems.numQtyShipped,0) END)
				WHEN @numViewID = 2 AND @tintPackingViewMode = 3
				THEN (CASE WHEN ISNULL(@tintPackingMode,1) = 2 THEN ISNULL(OpportunityItems.numQtyPicked,0) ELSE ISNULL(OpportunityItems.numUnitHour,0) END) - ISNULL((SELECT 
																																											SUM(OpportunityBizDocItems.numUnitHour)
																																										FROM
																																											OpportunityBizDocs
																																										INNER JOIN
																																											OpportunityBizDocItems
																																										ON
																																											OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
																																										WHERE
																																											OpportunityBizDocs.numOppId = OpportunityMaster.numOppID
																																											AND OpportunityBizDocs.numBizDocId=@numDefaultSalesShippingDoc
					 																																					AND OpportunityBizDocItems.numOppItemID = OpportunityItems.numoppitemtcode),0)
				
				ELSE OpportunityItems.numUnitHour
			END),OpportunityItems.numoppitemtCode,OpportunityItems.numWarehouseItmsID,(CASE WHEN ISNULL(Item.bitKitParent,0) = 1 THEN 1 ELSE 0 END)) = 1 THEN 0 ELSE 1 END)
												ELSE 1
											END) 
										ELSE 1
									END)		
							AND 1 = (CASE WHEN LEN(ISNULL(@vcOrderStauts,'''')) > 0 THEN (CASE WHEN OpportunityMaster.numStatus ',(CASE WHEN ISNULL(@bitIncludeSearch,0) = 0 THEN 'NOT IN' ELSE 'IN' END),' (SELECT Id FROM dbo.SplitIDs(@vcOrderStauts,'','')) THEN 1 ELSE 0 END) ELSE 1 END)
							AND 1 = (CASE 
										WHEN LEN(ISNULL(@vcFilterValue,'''')) > 0
										THEN
											CASE 
												WHEN @tintFilterBy=1 --Item Classification
												THEN (CASE WHEN Item.numItemClassification ',(CASE WHEN ISNULL(@bitIncludeSearch,0) = 0 THEN 'NOT IN' ELSE 'IN' END),' (SELECT Id FROM dbo.SplitIDs(@vcFilterValue,'','')) THEN 1 ELSE 0 END)
												ELSE 1
											END
										ELSE 1
									END)
							AND 1 = (CASE
										WHEN @numViewID = 1 THEN (CASE WHEN ISNULL(OpportunityItems.numUnitHour,0) - ISNULL(OpportunityItems.numQtyPicked,0) > 0 THEN 1 ELSE 0 END)
										WHEN @numViewID = 2 THEN (CASE 
																		WHEN @tintPackingViewMode = 1 
																		THEN (CASE 
																				WHEN ISNULL((SELECT COUNT(*) FROM OpportunityItems OIInner WHERE OIInner.numOppID=OpportunityMaster.numOppId AND OIInner.numItemCode=ISNULL(@numShippingServiceItemID,0)),0) = 0 
																				THEN 1 
																				ELSE 0 
																			END)
																		WHEN @tintPackingViewMode = 2
																		THEN (CASE 
																				WHEN ',(CASE WHEN ISNULL(@tintPackingMode,1) = 2 THEN 'ISNULL(OpportunityItems.numQtyPicked,0)' ELSE 'ISNULL(OpportunityItems.numUnitHour,0)' END),' - ISNULL(OpportunityItems.numQtyShipped,0) > 0
																				THEN 1 
																				ELSE 0 
																			END)
																		WHEN @tintPackingViewMode = 3
																		THEN (CASE 
																				WHEN ',(CASE WHEN ISNULL(@tintPackingMode,1) = 2 THEN 'ISNULL(OpportunityItems.numQtyPicked,0)' ELSE 'ISNULL(OpportunityItems.numUnitHour,0)' END),' - ISNULL((SELECT 
																																																													SUM(OpportunityBizDocItems.numUnitHour)
																																																												FROM
																																																													OpportunityBizDocs
																																																												INNER JOIN
																																																													OpportunityBizDocItems
																																																												ON
																																																													OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
																																																												WHERE
																																																													OpportunityBizDocs.numOppId = OpportunityMaster.numOppID
																																																													AND OpportunityBizDocs.numBizDocId=@numDefaultSalesShippingDoc
																																																													AND OpportunityBizDocItems.numOppItemID = OpportunityItems.numoppitemtcode),0) > 0
																				THEN 1 
																				ELSE 0 
																			END)','
																		ELSE 0
																	END)
										WHEN @numViewID = 3
										THEN (CASE 
												WHEN (CASE 
														WHEN ISNULL(@tintInvoicingType,0)=1 
														THEN ISNULL(OpportunityItems.numQtyShipped,0) 
														ELSE ISNULL(OpportunityItems.numUnitHour,0) 
													END) - ISNULL((SELECT 
																		SUM(OpportunityBizDocItems.numUnitHour)
																	FROM
																		OpportunityBizDocs
																	INNER JOIN
																		OpportunityBizDocItems
																	ON
																		OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
																	WHERE
																		OpportunityBizDocs.numOppId = OpportunityMaster.numOppID
																		AND OpportunityBizDocs.numBizDocId=287
																		AND OpportunityBizDocItems.numOppItemID = OpportunityItems.numoppitemtcode),0) > 0 
												THEN 1
												ELSE 0 
											END)
										WHEN @numViewID = 4 THEN 1
										WHEN @numViewID = 5 THEN 1
										WHEN @numViewID = 6 THEN 1
									END)
							AND 1 = (CASE
										WHEN @numViewID = 1 THEN (CASE WHEN ISNULL(WareHouseItems.numWareHouseItemID,0) > 0 AND ISNULL(OpportunityItems.bitDropship,0) = 0 THEN 1 ELSE 0 END)
										WHEN @numViewID = 2 AND @tintPackingViewMode <> 2 THEN (CASE WHEN ISNULL(WareHouseItems.numWareHouseItemID,0) > 0 AND ISNULL(OpportunityItems.bitDropship,0) = 0 THEN 1 ELSE 0 END)
										WHEN @numViewID = 2 AND @tintPackingViewMode = 2 THEN 1
										WHEN @numViewID = 3 THEN 1
										WHEN @numViewID = 4 THEN 1
										WHEN @numViewID = 5 THEN 1
										WHEN @numViewID = 6 THEN 1
										ELSE 0
									END)',
							(CASE 
								WHEN @numViewID = 5 AND ISNULL(@tintPendingCloseFilter,0) = 2
								THEN ' AND (SELECT 
												COUNT(*) 
											FROM 
											(
												SELECT
													OI.numoppitemtCode,
													ISNULL(OI.numUnitHour,0) AS OrderedQty,
													ISNULL(TempFulFilled.FulFilledQty,0) AS FulFilledQty
												FROM
													OpportunityItems OI
												INNER JOIN
													Item I
												ON
													OI.numItemCode = I.numItemCode
												OUTER APPLY
												(
													SELECT
														SUM(OpportunityBizDocItems.numUnitHour) AS FulFilledQty
													FROM
														OpportunityBizDocs
													INNER JOIN
														OpportunityBizDocItems 
													ON
														OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
													WHERE
														OpportunityBizDocs.numOppId = OpportunityMaster.numOppID
														AND ISNULL(OpportunityBizDocs.numBizDocId,0) = 296
														AND OpportunityBizDocItems.numOppItemID = OI.numoppitemtCode
												) AS TempFulFilled
												WHERE
													OI.numOppID = OpportunityMaster.numOppID
													AND ISNULL(OI.numUnitHour,0) > 0
													AND ISNULL(OI.bitDropShip,0) = 0
											) X
											WHERE
												X.OrderedQty <> X.FulFilledQty) = 0
										AND (SELECT 
												COUNT(*) 
											FROM 
											(
												SELECT
													OI.numoppitemtCode,
													ISNULL(OI.numUnitHour,0) AS OrderedQty,
													ISNULL(TempInvoice.InvoicedQty,0) AS InvoicedQty
												FROM
													OpportunityItems OI
												INNER JOIN
													Item I
												ON
													OI.numItemCode = I.numItemCode
												OUTER APPLY
												(
													SELECT
														SUM(OpportunityBizDocItems.numUnitHour) AS InvoicedQty
													FROM
														OpportunityBizDocs
													INNER JOIN
														OpportunityBizDocItems 
													ON
														OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
													WHERE
														OpportunityBizDocs.numOppId = OpportunityMaster.numOppID
														AND OpportunityBizDocItems.numOppItemID = OI.numoppitemtCode
														AND ISNULL(OpportunityBizDocs.bitAuthoritativeBizDocs,0) = 1
												) AS TempInvoice
												WHERE
													OI.numOppID = OpportunityMaster.numOppID
													AND ISNULL(OI.numUnitHour,0) > 0
											) X
											WHERE
												X.OrderedQty > X.InvoicedQty) = 0'
								ELSE '' 
							END)
							,(CASE WHEN @numViewID = 4 THEN 'AND 1 = (CASE WHEN ISNULL(OpportunityBizDocs.monDealAmount,0) - ISNULL(OpportunityBizDocs.monAmountPaid,0) > 0 THEN 1 ELSE 0 END)' ELSE '' END)
							,' AND 1 = (CASE WHEN @numViewID = 2 AND @tintPackingViewMode = 3 THEN (CASE WHEN ISNULL(Item.bitContainer,0)=1 THEN 0 ELSE 1 END) ELSE 1 END)
							AND 1 = (CASE WHEN ISNULL(@numWarehouseID,'''') <>'''' AND ISNULL(OpportunityItems.numWarehouseItmsID,0) > 0 THEN (CASE WHEN WareHouseItems.numWarehouseID IN (SELECT Items FROM Split(@numWarehouseID,'','') WHERE Items<>'''') THEN 1 ELSE 0 END) ELSE 1 END)	'
								,(CASE WHEN ISNULL(@numShippingZone,0) > 0 THEN ' AND 1 = (CASE 
																				WHEN ISNULL((SELECT TOP 1 numState FROM fn_getOPPAddressDetails(OpportunityMaster.numOppId,OpportunityMaster.numDomainID,2)),0) > 0 
																				THEN
																					CASE 
																						WHEN (SELECT COUNT(*) FROM dbo.State WHERE numDomainID=@numDomainId AND numStateID =  (SELECT TOP 1 numState FROM fn_getOPPAddressDetails(OpportunityMaster.numOppId,OpportunityMaster.numDomainID,2)) AND numShippingZone=@numShippingZone) > 0 
																						THEN 1
																						ELSE 0
																					END
																				ELSE 0 
																			END)' ELSE '' END),
								@vcCustomSearchValue,(CASE WHEN @bitGroupByOrder = 1 THEN CONCAT('GROUP BY OpportunityMaster.numOppId',(CASE WHEN @numViewID = 4 THEN ',OpportunityBizDocs.numOppBizDocsId,OpportunityBizDocs.monAmountPaid' WHEN @numViewID = 6 THEN ',OpportunityBizDocs.numOppBizDocsId'  ELSE '' END)) ELSE '' END));
	EXEC sp_executesql @vcSQL, N'@numDomainID NUMERIC(18,0), @vcOrderStauts VARCHAR(MAX), @vcFilterValue VARCHAR(MAX), @tintFilterBy TINYINT, @numViewID NUMERIC(18,0), @tintPackingViewMode TINYINT, @numShippingServiceItemID NUMERIC(18,0), @numShippingZone NUMERIC(18,0), @numDefaultSalesShippingDoc NUMERIC(18,0), @tintInvoicingType TINYINT, @tintPackingMode TINYINT, @bitRemoveBO BIT,@numWarehouseID VARCHAR(100)', @numDomainID, @vcOrderStauts, @vcFilterValue, @tintFilterBy, @numViewID, @tintPackingViewMode, @numShippingServiceItemID, @numShippingZone, @numDefaultSalesShippingDoc, @tintInvoicingType, @tintPackingMode, @bitRemoveBO,@numWarehouseID;

	If @numViewID = 2 AND @tintPackingViewMode = 3
	BEGIN
		SET @vcSQL = CONCAT('INSERT INTO #TEMPMSRecords
						(
							numOppID
							,numOppItemID
							,numOppBizDocID
						)
						SELECT
							OpportunityMaster.numOppId
							,',(CASE WHEN @bitGroupByOrder = 1 THEN '0' ELSE 'OpportunityItems.numoppitemtCode' END),'
							,OpportunityBizDocs.numOppBizDocsId
						FROM
							OpportunityMaster
						INNER JOIN
							DivisionMaster
						ON
							OpportunityMaster.numDivisionId = DivisionMaster.numDivisionID
						INNER JOIN
							CompanyInfo
						ON
							DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
						INNER JOIN 
							OpportunityBizDocs
						ON
							OpportunityMaster.numOppId = OpportunityBizDocs.numOppId
							AND OpportunityBizDocs.numBizDocID=@numDefaultSalesShippingDoc
						INNER JOIN 
							OpportunityItems
						ON
							OpportunityMaster.numOppId = OpportunityItems.numOppId
						INNER JOIN
							Item
						ON
							OpportunityItems.numItemCode = Item.numItemCode
						LEFT JOIN
							WareHouseItems
						ON
							OpportunityItems.numWarehouseItmsID = WareHouseItems.numWareHouseItemID
						
						',
						(CASE 
							WHEN ISNULL(@numBatchID,0) > 0 
							THEN 
								' INNER JOIN 
										MassSalesFulfillmentBatchOrders
								ON
									OpportunityMaster.numOppId = MassSalesFulfillmentBatchOrders.numOppID
									AND (OpportunityItems.numOppItemtCode = MassSalesFulfillmentBatchOrders.numOppItemID OR ISNULL(MassSalesFulfillmentBatchOrders.numOppItemID,0) = 0)'
							ELSE 
								'' 
						END)	
						,'
						WHERE
							OpportunityMaster.numDomainId=@numDomainID
							AND OpportunityMaster.tintOppType = 1
							AND OpportunityMaster.tintOppStatus = 1
							AND ISNULL(OpportunityMaster.tintshipped,0) = 0		
							AND 1 = (CASE WHEN LEN(ISNULL(@vcOrderStauts,'''')) > 0 THEN (CASE WHEN OpportunityMaster.numStatus ',(CASE WHEN ISNULL(@bitIncludeSearch,0) = 0 THEN 'NOT IN' ELSE 'IN' END),' (SELECT Id FROM dbo.SplitIDs(@vcOrderStauts,'','')) THEN 1 ELSE 0 END) ELSE 1 END)
							AND 1 = (CASE 
										WHEN LEN(ISNULL(@vcFilterValue,'''')) > 0
										THEN
											CASE 
												WHEN @tintFilterBy=1 --Item Classification
												THEN (CASE WHEN Item.numItemClassification ',(CASE WHEN ISNULL(@bitIncludeSearch,0) = 0 THEN 'NOT IN' ELSE 'IN' END),' (SELECT Id FROM dbo.SplitIDs(@vcFilterValue,'','')) THEN 1 ELSE 0 END)
												ELSE 1
											END
										ELSE 1
									END)
							AND 1 = (CASE 
										WHEN ISNULL((SELECT COUNT(*) FROM ShippingBox WHERE ISNULL(vcShippingLabelImage,'''') <> '''' AND ISNULL(vcTrackingNumber,'''') <> '''' AND numShippingReportId IN (SELECT numShippingReportId FROM ShippingReport WHERE ShippingReport.numOppID=OpportunityMaster.numOppId AND ShippingReport.numOppBizDocId=OpportunityBizDocs.numOppBizDocsId)),0) = 0 
										THEN 1 
										ELSE 0 
									END)
										
							AND 1 = (CASE WHEN ISNULL(WareHouseItems.numWareHouseItemID,0) > 0 AND ISNULL(OpportunityItems.bitDropship,0) = 0 THEN 1 ELSE 0 END)',
							' AND 1 = (CASE WHEN @numViewID = 2 AND @tintPackingViewMode = 3 THEN (CASE WHEN ISNULL(Item.bitContainer,0)=1 THEN 0 ELSE 1 END) ELSE 1 END)
							AND 1 = (CASE WHEN ISNULL(@numWarehouseID,'''') <>'''' AND ISNULL(OpportunityItems.numWarehouseItmsID,0) > 0 THEN (CASE WHEN WareHouseItems.numWarehouseID IN (SELECT Items FROM Split(@numWarehouseID,'','') WHERE Items<>'''') THEN 1 ELSE 0 END) ELSE 1 END)	'
								,(CASE WHEN ISNULL(@numShippingZone,0) > 0 THEN ' AND 1 = (CASE 
																				WHEN ISNULL((SELECT TOP 1 numState FROM fn_getOPPAddressDetails(OpportunityMaster.numOppId,OpportunityMaster.numDomainID,2)),0) > 0 
																				THEN
																					CASE 
																						WHEN (SELECT COUNT(*) FROM dbo.State WHERE numDomainID=@numDomainId AND numStateID =  (SELECT TOP 1 numState FROM fn_getOPPAddressDetails(OpportunityMaster.numOppId,OpportunityMaster.numDomainID,2)) AND numShippingZone=@numShippingZone) > 0 
																						THEN 1
																						ELSE 0
																					END
																				ELSE 0 
																			END)' ELSE '' END),
								@vcCustomSearchValue,(CASE WHEN @bitGroupByOrder = 1 THEN ' GROUP BY OpportunityMaster.numOppId,OpportunityBizDocs.numOppBizDocsId,OpportunityBizDocs.monAmountPaid' ELSE '' END));

		EXEC sp_executesql @vcSQL, N'@numDomainID NUMERIC(18,0), @vcOrderStauts VARCHAR(MAX), @vcFilterValue VARCHAR(MAX), @tintFilterBy TINYINT, @numViewID NUMERIC(18,0), @tintPackingViewMode TINYINT, @numShippingServiceItemID NUMERIC(18,0), @numShippingZone NUMERIC(18,0), @numDefaultSalesShippingDoc NUMERIC(18,0), @tintInvoicingType TINYINT, @tintPackingMode TINYINT, @bitRemoveBO BIT,@numWarehouseID NUMERIC(18,0)', @numDomainID, @vcOrderStauts, @vcFilterValue, @tintFilterBy, @numViewID, @tintPackingViewMode, @numShippingServiceItemID, @numShippingZone, @numDefaultSalesShippingDoc, @tintInvoicingType, @tintPackingMode, @bitRemoveBO,@numWarehouseID;
	END

	--PRINT @numDomainID 
	--PRINT @vcOrderStauts 
	--PRINT  @vcFilterValue 
	--PRINT @tintFilterBy 
	--PRINT  @numViewID 
	--PRINT  @tintPackingViewMode 
	--PRINT  @numShippingServiceItemID 
	--PRINT @numShippingZone 
	--PRINT  @numDefaultSalesShippingDoc 
	--PRINT  @tintInvoicingType 
	--PRINT  @tintPackingMode 
	--PRINT @bitRemoveBO 
	PRINT @numWarehouseID
	--SELECT * FROM #TEMPMSRecords
	SET @I = @I +1
	END
	
	SET @numTotalRecords = (SELECT COUNT(*) FROM #TEMPMSRecords) 
END