
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_BroadCastDtls_UpdateSentStatus' ) 
    DROP PROCEDURE USP_BroadCastDtls_UpdateSentStatus
GO

CREATE PROCEDURE USP_BroadCastDtls_UpdateSentStatus  
(  
 @numBroadCastDtlID NUMERIC(18,0)
)  
AS  
BEGIN  
-- SET NOCOUNT ON added to prevent extra result sets from  
-- interfering with SELECT statements.  
SET NOCOUNT ON;  

UPDATE BroadCastDtls SET tintSucessfull = 1 WHERE numBroadCastDtlId = @numBroadCastDtlID
UPDATE Broadcast SET numTotalSussessfull = numTotalSussessfull + 1, bitBroadcasted=1 WHERE numBroadCastId = ISNULL((SELECT numBroadCastID FROM BroadCastDtls WHERE numBroadCastDtlId = @numBroadCastDtlID),0)

END  

