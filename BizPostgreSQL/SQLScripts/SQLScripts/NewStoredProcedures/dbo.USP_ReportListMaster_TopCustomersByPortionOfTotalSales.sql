SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ReportListMaster_TopCustomersByPortionOfTotalSales')
DROP PROCEDURE USP_ReportListMaster_TopCustomersByPortionOfTotalSales
GO
CREATE PROCEDURE [dbo].[USP_ReportListMaster_TopCustomersByPortionOfTotalSales]
	@numDomainID NUMERIC(18,0)
AS
BEGIN 
	DECLARE @TotalSalesOrderAmount AS DECIMAL(20,5)


	SELECT DISTINCT
		@TotalSalesOrderAmount = SUM(monDealAmount)
	FROM 
		OpportunityMaster
	WHERE 
		numDomainId=@numDomainID 
		AND tintOppType=1 
		AND tintOppStatus=1 

	SELECT TOP 10 
		DM.numDivisionID
		,vcCompanyName
		,(SUM(monDealAmount) * 100)/ (CASE WHEN @TotalSalesOrderAmount = 0 THEN 1 ELSE @TotalSalesOrderAmount END) AS TotalSalesPercent
	FROM
		OpportunityMaster OM
	INNER JOIN
		DivisionMaster DM
	ON
		OM.numDivisionId = DM.numDivisionID
	INNER JOIN
		CompanyInfo CI
	ON
		DM.numCompanyID = CI.numCompanyId
	WHERE
		OM.numDomainId = @numDomainID
		AND DM.numDomainID = @numDomainID
		AND CI.numDomainID=@numDomainID
		AND tintOppType=1 
		AND tintOppStatus=1
	GROUP BY
		DM.numDivisionID
		,vcCompanyName
	HAVING
		SUM(monDealAmount) > 0
	ORDER BY 
		SUM(monDealAmount) DESC
END
GO
