SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_SalesFulfillmentWorkflow')
DROP PROCEDURE dbo.USP_SalesFulfillmentWorkflow
GO
CREATE PROCEDURE [dbo].[USP_SalesFulfillmentWorkflow]
    (
	  @numDomainID NUMERIC(9),
      @numOppID NUMERIC(9),
      @numOppBizDocsId NUMERIC(9),
      @numUserCntID NUMERIC(9),
      @numBizDocStatus NUMERIC(9),
	  @numOppAuthBizDocsId as numeric(9)=0 OUTPUT,
	  @bitNotValidateBizDocFulfillment AS BIT
    )
AS 
    BEGIN
    DECLARE @ErrorMessage NVARCHAR(4000);SET @ErrorMessage=''
    DECLARE @ErrorSeverity INT;
    DECLARE @ErrorState INT;
    
BEGIN TRY		
		--DECLARE @numBizDocStatus AS NUMERIC(9);SET @numBizDocStatus=0
		DECLARE @numBizDocId AS NUMERIC(9);SET @numBizDocId=0
		DECLARE @numRuleID AS NUMERIC(9);SET @numRuleID=0
		DECLARE @numPackingSlipBizDocId NUMERIC(9),@numAuthInvoice NUMERIC(9)
		DECLARE @tintOppType NUMERIC(9);SET @tintOppType=0
	   
		SELECT /*@numBizDocStatus=ISNULL(OBD.numBizDocStatus,0),*/@tintOppType=tintOppType,@numBizDocId=OBD.numBizDocId FROM dbo.OpportunityMaster OM JOIN dbo.OpportunityBizDocs OBD ON OM.numOppID=OBD.numOppID
				WHERE OM.numDomainId=@numDomainId AND OM.numOppID=@numOppID AND OBD.numOppBizDocsId=@numOppBizDocsId 
				/*AND ISNULL(OBD.numBizDocStatus,0)!=0 AND ISNULL(OBD.numBizDocStatus,0)!=ISNULL(OBD.numBizDocStatusOLD,0)*/
		
		IF @tintOppType=1
		BEGIN
		IF @numBizDocStatus>0 AND @numBizDocId>0
		BEGIN
			IF @numBizDocId=296
			BEGIN
				SELECT @numRuleID=ISNULL(numRuleID,0) FROM dbo.OpportunityAutomationRules WHERE numDomainID=@numDomainID AND numBizDocStatus1=@numBizDocStatus AND numRuleID IN (1,2,3,4,5,6)
				
				IF @numRuleID=1 --create an invoice for all items within the fulfillment order
				BEGIN
					Select @numAuthInvoice=isnull(numAuthoritativeSales,0) From dbo.AuthoritativeBizDocs Where AuthoritativeBizDocs.numDomainId=@numDomainId 
					
					EXEC dbo.USP_CreateBizDocs
							@numOppId = @numOppID, --  numeric(9, 0)
							@numBizDocId = @numAuthInvoice, --  numeric(9, 0)
							@numUserCntID = @numUserCntID, --  numeric(9, 0)
							@numOppBizDocsId = @numOppAuthBizDocsId output,
							@vcComments = '', --  varchar(1000)
							@bitPartialFulfillment = 1, --  bit
							@strBizDocItems = '', --  text
							--@monShipCost = 0, --  DECIMAL(20,5)
							@numShipVia = 0, --  numeric(9, 0)
							@vcTrackingURL = '', --  varchar(1000)
							@dtFromDate = '2013-1-2 10:55:29.610', --  datetime
							@numBizDocStatus = 0, --  numeric(9, 0)
							@bitRecurringBizDoc = 0, --  bit
							@numSequenceId = '', --  varchar(50)
							@tintDeferred = 0, --  tinyint
							@monCreditAmount =0,
							@ClientTimeZoneOffset = 0, --  int
							@monDealAmount =0,
							@bitRentalBizDoc = 0, --  bit
							@numBizDocTempID = 0, --  numeric(9, 0)
							@vcTrackingNo = '', --  varchar(500)
							--@vcShippingMethod = '', --  varchar(100)
							@vcRefOrderNo = '', --  varchar(100)
							--@dtDeliveryDate = '2013-1-2 10:55:29.611', --  datetime
							@OMP_SalesTaxPercent = 0, --  float
							@numFromOppBizDocsId = @numOppBizDocsId, --  numeric(9, 0)
							@bitTakeSequenceId = 1,  --bit
							@bitNotValidateBizDocFulfillment=@bitNotValidateBizDocFulfillment
					
				END
				ELSE IF @numRuleID=2 --create a packing slip for its inventory items
				BEGIN
					SELECT @numPackingSlipBizDocId=numListItemID FROM dbo.ListDetails WHERE ListDetails.vcData='Packing Slip' AND ListDetails.constFlag=1 AND ListDetails.numListID=27
				
					EXEC dbo.USP_CreateBizDocs
								@numOppId = @numOppID, --  numeric(9, 0)
								@numBizDocId = @numPackingSlipBizDocId, --  numeric(9, 0)
								@numUserCntID = @numUserCntID, --  numeric(9, 0)
								@numOppBizDocsId =0,
								@vcComments = '', --  varchar(1000)
								@bitPartialFulfillment = 1, --  bit
								@strBizDocItems = '', --  text
								--@monShipCost = 0, --  DECIMAL(20,5)
								@numShipVia = 0, --  numeric(9, 0)
								@vcTrackingURL = '', --  varchar(1000)
								@dtFromDate = '2013-1-2 10:56:4.477', --  datetime
								@numBizDocStatus = 0, --  numeric(9, 0)
								@bitRecurringBizDoc = 0, --  bit
								@numSequenceId = '', --  varchar(50)
								@tintDeferred = 0, --  tinyint
								@monCreditAmount =0,
								@ClientTimeZoneOffset = 0, --  int
								@monDealAmount =0,
								@bitRentalBizDoc = 0, --  bit
								@numBizDocTempID = 0, --  numeric(9, 0)
								@vcTrackingNo = '', --  varchar(500)
								--@vcShippingMethod = '', --  varchar(100)
								@vcRefOrderNo = '', --  varchar(100)
								--@dtDeliveryDate = '2013-1-2 10:56:4.477', --  datetime
								@OMP_SalesTaxPercent = 0, --  float
								@numFromOppBizDocsId = @numOppBizDocsId, --  numeric(9, 0)
								@bitTakeSequenceId = 1,  --bit
								@bitNotValidateBizDocFulfillment=@bitNotValidateBizDocFulfillment
				END
				ELSE IF @numRuleID=3 --release its inventory items from allocation
				BEGIN
					BEGIN TRY		
						EXEC dbo.USP_SalesFulfillmentUpdateInventory
							@numDomainID = @numDomainID, --  numeric(9, 0)
							@numOppID = @numOppID, --  numeric(9, 0)
							@numOppBizDocsId = @numOppBizDocsId, --  numeric(9, 0)
							@numUserCntID = @numUserCntID,  --  numeric(9, 0) 
							@tintMode = 1 --  tinyint
					END TRY
					BEGIN CATCH
					SELECT @ErrorMessage = ERROR_MESSAGE(),
								       @ErrorSeverity = ERROR_SEVERITY(),
									   @ErrorState = ERROR_STATE();
									   
						IF @ErrorMessage='NotSufficientQty_Allocation'
						BEGIN
							SELECT @numBizDocStatus=ISNULL(numBizDocStatus1,0) FROM dbo.OpportunityAutomationRules 
								WHERE numDomainID=@numDomainID AND ISNULL(numRuleID,0)=6
							
							EXEC dbo.USP_UpdateSingleFieldValue
								@tintMode = 27, --  tinyint
								@numUpdateRecordID = @numOppBizDocsId, --  numeric(18, 0)
								@numUpdateValueID = @numBizDocStatus, --  numeric(18, 0)
								@vcText = '', --  text
								@numDomainID = @numDomainID --  numeric(18, 0)
								
								INSERT INTO dbo.OppFulfillmentBizDocsStatusHistory (
										numDomainId,numOppId,numOppBizDocsId,numBizDocStatus,numUserCntID,dtCreatedDate
										) VALUES ( 
										@numDomainID,@numOppID,@numOppBizDocsId,@numBizDocStatus,@numUserCntID,GETUTCDATE() ) 
						END	

						RAISERROR (@ErrorMessage,@ErrorSeverity,@ErrorState);
						
					END CATCH;		
				END
				ELSE IF @numRuleID=4 --return its inventory items back to allocation
				BEGIN
					Select @numAuthInvoice=isnull(numAuthoritativeSales,0) From dbo.AuthoritativeBizDocs Where AuthoritativeBizDocs.numDomainId=@numDomainId 
				
						IF EXISTS (SELECT numOppBizDocsId FROM OpportunityMaster OM JOIN OpportunityBizDocs OBD ON OM.numOppID=OBD.numOppID
							WHERE OM.numDomainId=@numDomainId AND OM.numOppID=@numOppID  AND OBD.numBizDocId IN (@numAuthInvoice))
						BEGIN
								RAISERROR ('AlreadyInvoice_DoNotReturn',16,1);
								RETURN -1
						END		
						ELSE
						BEGIN
								EXEC dbo.USP_SalesFulfillmentUpdateInventory
							@numDomainID = @numDomainID, --  numeric(9, 0)
							@numOppID = @numOppID, --  numeric(9, 0)
							@numOppBizDocsId = @numOppBizDocsId, --  numeric(9, 0)
							@numUserCntID = @numUserCntID,  --  numeric(9, 0) 
							@tintMode = 2 --  tinyint
						END
				END
				ELSE IF @numRuleID=5 --Remove the Fulfillment Order from the Sales Fulfillment queue
				BEGIN
					PRINT 'Rule-5'
				
				END
			END
		END
		
		IF @numBizDocStatus>0
		BEGIN
				INSERT INTO dbo.OppFulfillmentBizDocsStatusHistory (
					numDomainId,
					numOppId,
					numOppBizDocsId,
					numBizDocStatus,
					numUserCntID,
					dtCreatedDate
				) VALUES ( 
					/* numDomainId - numeric(18, 0) */ @numDomainID,
					/* numOppId - numeric(18, 0) */ @numOppID,
					/* numOppBizDocsId - numeric(18, 0) */ @numOppBizDocsId,
					/* numBizDocStatus - numeric(18, 0) */ @numBizDocStatus,
					/* numUserCntID - numeric(18, 0) */ @numUserCntID,
					/* dtCreatedDate - datetime */ GETUTCDATE() )  
		 END			
	
	END
			 END TRY
BEGIN CATCH

    SELECT 
        @ErrorMessage = ERROR_MESSAGE(),
        @ErrorSeverity = ERROR_SEVERITY(),
        @ErrorState = ERROR_STATE();

    -- Use RAISERROR inside the CATCH block to return error
    -- information about the original error that caused
    -- execution to jump to the CATCH block.
    RAISERROR (@ErrorMessage, -- Message text.
               @ErrorSeverity, -- Severity.
               @ErrorState -- State.
               );
END CATCH;
		
    END
