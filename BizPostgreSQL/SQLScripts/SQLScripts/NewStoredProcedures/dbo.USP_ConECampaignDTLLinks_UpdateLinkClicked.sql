GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ConECampaignDTLLinks_UpdateLinkClicked')
DROP PROCEDURE USP_ConECampaignDTLLinks_UpdateLinkClicked
GO
CREATE PROCEDURE [dbo].[USP_ConECampaignDTLLinks_UpdateLinkClicked]                            
 @numLinkID NUMERIC(9 )= 0
AS                            
BEGIN                            
	
	IF (SELECT ISNULL(bitClicked,0) FROM ConECampaignDTLLinks WHERE numLinkID = @numLinkID) = 0
	BEGIN
		UPDATE 
			ConECampaignDTLLinks
		SET
			bitClicked = 1
		WHERE
			numLinkID = @numLinkID
	END

	SELECT ISNULL(vcOriginalLink,'') FROM ConECampaignDTLLinks WHERE numLinkID = @numLinkID
END
GO