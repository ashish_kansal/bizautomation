
GO
/****** Object:  StoredProcedure [dbo].[USP_GetWebAPI]    Script Date: 05/07/2009 22:19:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- exec USP_GetWebAPI 2,1
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetWebAPI')
DROP PROCEDURE USP_GetWebAPI
GO
CREATE PROCEDURE [dbo].[USP_GetWebAPI](
               @WebApiID AS NUMERIC(9)=0,
               @numDomainID NUMERIC(9)=0,
               @tintMode TINYINT -- 1: get list of webAPI regardless of domain,2:get All WebAPI for all domain with WebAPI Details
               )
AS
  BEGIN
  SET @WebApiID = ISNULL(@WebApiID,0);
  
 IF @tintMode=1
 BEGIN
 		SELECT DISTINCT 
 			WebApiId,
		   vcProviderName,
		   vcFirstFldName,
		   vcSecondFldName,
		   vcThirdFldName,
		   vcFourthFldName,
		   vcFifthFldName,
		   vcSixthFldName,
		   vcSeventhFldName,
		   vcEighthFldName,
		   vcNinthFldName,
		   vcTenthFldName,
		   vcEleventhFldName,
           vcTwelfthFldName,
           vcThirteenthFldName,
           vcFourteenthFldName,
		   vcSettingURL,
		   dtCreateDate,
		   dtEndDate 	 
		 FROM dbo.WebAPI W
 END
 ELSE IF @tintMode=2
 BEGIN
 	SELECT W.vcProviderName,
           W.vcFirstFldName,
           W.vcSecondFldName,
           W.vcThirdFldName,
           W.vcFourthFldName,
           W.vcFifthFldName,
           W.vcSixthFldName,
           W.vcSeventhFldName,
           W.vcEighthFldName,
           W.vcNinthFldName,
           W.vcTenthFldName,
           W.vcThirteenthFldName,
           W.vcFourteenthFldName,
           W.vcFifteenthFldName,
           WD.vcFifthFldValue,
           WD.vcFourthFldValue,
           WD.vcThirdFldValue,
           WD.vcSecondFldValue,
           WD.vcFirstFldValue,
           WD.vcSixthFldValue,
           WD.vcSeventhFldValue,
           WD.vcEighthFldValue,
           WD.vcNinthFldValue,
           WD.vcTenthFldValue,
            WD.vcEleventhFldValue,
             WD.vcTwelfthFldValue,
             WD.vcThirteenthFldValue,
             WD.vcFourteenthFldValue,
             WD.vcFifteenthFldValue,
			 WD.vcSixteenthFldValue,
           ISNULL(vcSettingURL,'#') vcSettingURL,
           ISNULL(WD.bitEnableAPI,0) bitEnableAPI,
		   ISNULL(WD.bitEnableItemUpdate,0) bitEnableItemUpdate,
		   ISNULL(WD.bitEnableOrderImport,0) bitEnableOrderImport,
		   ISNULL(WD.bitEnableTrackingUpdate,0) bitEnableTrackingUpdate,
		   ISNULL(WD.bitEnableInventoryUpdate,0) bitEnableInventoryUpdate,
           W.WebApiId,
           WD.numDomainId,
           --ISNULL(ED.[numDefaultWareHouseID],0) numDefaultWareHouseID,
           --ISNULL(ED.[numDefaultIncomeChartAcntId],0) numDefaultIncomeChartAcntId,
           --ISNULL(ED.[numDefaultAssetChartAcntId],0) numDefaultAssetChartAcntId,
           --ISNULL(ED.[numDefaultCOGSChartAcntId],0) numDefaultCOGSChartAcntId,
           --ISNULL((SELECT MAX(CONVERT(NUMERIC(9),[vcAPIOppId])) FROM   [OpportunityMasterAPI] WHERE  [numdomainid] = WD.numDomainId AND [WebApiId] =W.WebApiId ),0) AS LastInsertedOrderID,
		    ISNULL(WD.numBizDocId,0) numBizDocId,
			ISNULL(WD.numBizDocStatusId,0) numBizDocStatusId,
		    ISNULL(WD.numOrderStatus,0) numOrderStatus,
			ISNULL(WD.numFBABizDocId,0) numFBABizDocId,
			ISNULL(WD.numFBABizDocStatusId,0) numFBABizDocStatusId,
			ISNULL(WD.numUnShipmentOrderStatus,0) numUnShipmentOrderStatus,
		    ISNULL(WD.numRecordOwner,0) numRecordOwner ,
		    ISNULL(WD.numAssignTo,0) numAssignTo,
		    ISNULL(WD.numWareHouseID,0) numWareHouseID,
		    ISNULL(WD.numRelationshipId,0) numRelationshipId,
		     ISNULL(WD.numProfileId,0) numProfileId,
		    ISNULL(WD.numExpenseAccountId,0) numExpenseAccountId,
		   --ISNULL(WD.numDiscountItemMapping,0) numDiscountItemMapping,
			ISNULL(WD.numSalesTaxItemMapping,0) numSalesTaxItemMapping,
		ISNULL((SELECT numShippingServiceItemID FROM Domain WHERE numDomainID = WD.numDomainID),0) AS numShippingServiceItemID,
		ISNULL((SELECT numDisCountServiceItemID FROM Domain WHERE numDomainID = WD.numDomainID),0) AS numDiscountItemMapping,
		ISNULL((SELECT vcShipToPhoneNo FROM Domain WHERE numDomainID = WD.numDomainID),0) AS vcShipToPhoneNo,
		ISNULL((SELECT numAdminID FROM Domain WHERE numDomainID = WD.numDomainID),0) AS numAdminID
		
		   
    FROM   WebAPI AS W
           left JOIN WebAPIDetail AS WD
             ON W.WebApiId = WD.WebApiId
     WHERE WD.bitEnableAPI=1--Amazon hard coded 
             --LEFT JOIN [eCommerceDTL] ED
             --ON WD.[numDomainId] = ED.[numDomainId]
--              AND WD.numDomainID = @numDomainID
--    WHERE  (W.WebApiId = @WebApiID OR @WebApiID =0)AND  (WD.numDomainId = @numDomainID OR @numDomainID =0)
 	
 END
    
     ELSE IF @tintMode=3
 BEGIN
 	SELECT W.vcProviderName,
           W.vcFirstFldName,
           W.vcSecondFldName,
           W.vcThirdFldName,
           W.vcFourthFldName,
           W.vcFifthFldName,
           W.vcSixthFldName,
           W.vcSeventhFldName,
           W.vcEighthFldName,
           W.vcNinthFldName,
           W.vcTenthFldName,
           W.vcThirteenthFldName,
           W.vcFourteenthFldName,
           W.vcFifteenthFldName,
           WD.vcFifthFldValue,
           WD.vcFourthFldValue,
           WD.vcThirdFldValue,
           WD.vcSecondFldValue,
           WD.vcFirstFldValue,
           WD.vcSixthFldValue,
           WD.vcSeventhFldValue,
           WD.vcEighthFldValue,
           WD.vcNinthFldValue,
           WD.vcTenthFldValue,
            WD.vcEleventhFldValue,
            WD.vcTwelfthFldValue,
             WD.vcThirteenthFldValue,
              WD.vcFourteenthFldValue,
                WD.vcFifteenthFldValue,
				WD.vcSixteenthFldValue,		
           ISNULL(vcSettingURL,'#') vcSettingURL,
           ISNULL(WD.bitEnableAPI,0) bitEnableAPI,
		   ISNULL(WD.bitEnableItemUpdate,0) bitEnableItemUpdate,
		   ISNULL(WD.bitEnableOrderImport,0) bitEnableOrderImport,
		   ISNULL(WD.bitEnableTrackingUpdate,0) bitEnableTrackingUpdate,
		   ISNULL(WD.bitEnableInventoryUpdate,0) bitEnableInventoryUpdate,
           W.WebApiId,
           WD.numDomainId,
           --ISNULL(ED.[numDefaultWareHouseID],0) numDefaultWareHouseID,
           --ISNULL(ED.[numDefaultIncomeChartAcntId],0) numDefaultIncomeChartAcntId,
           --ISNULL(ED.[numDefaultAssetChartAcntId],0) numDefaultAssetChartAcntId,
           --ISNULL(ED.[numDefaultCOGSChartAcntId],0) numDefaultCOGSChartAcntId,
          -- ISNULL((SELECT MAX(CONVERT(NUMERIC(9),[vcAPIOppId])) FROM   [OpportunityMasterAPI] WHERE  [numdomainid] = WD.numDomainId AND [WebApiId] =W.WebApiId ),0) AS LastInsertedOrderID,
		    ISNULL(WD.numBizDocId,0) numBizDocId,
			ISNULL(WD.numBizDocStatusId,0) numBizDocStatusId,
		    ISNULL(WD.numOrderStatus,0) numOrderStatus,
			ISNULL(WD.numFBABizDocId,0) numFBABizDocId,
			ISNULL(WD.numFBABizDocStatusId,0) numFBABizDocStatusId,
			ISNULL(WD.numUnShipmentOrderStatus,0) numUnShipmentOrderStatus,
		    ISNULL(WD.numRecordOwner,0) numRecordOwner ,
		    ISNULL(WD.numAssignTo,0) numAssignTo,
		    ISNULL(WD.numWareHouseID,0) numWareHouseID,
		    ISNULL(WD.numRelationshipId,0) numRelationshipId,
		     ISNULL(WD.numProfileId,0) numProfileId,
		    ISNULL(WD.numExpenseAccountId,0) numExpenseAccountId,
		    --ISNULL(WD.numDiscountItemMapping,0) numDiscountItemMapping,
			ISNULL(WD.numSalesTaxItemMapping,0) numSalesTaxItemMapping,
		ISNULL((SELECT numShippingServiceItemID FROM Domain WHERE numDomainID = @numDomainID),0) AS numShippingServiceItemID,
		ISNULL((SELECT numDisCountServiceItemID FROM Domain WHERE numDomainID = @numDomainID),0) AS numDiscountItemMapping,
		ISNULL((SELECT vcShipToPhoneNo FROM Domain WHERE numDomainID = @numDomainID),0) AS vcShipToPhoneNo,
		ISNULL((SELECT numAdminID FROM Domain WHERE numDomainID = @numDomainID),0) AS numAdminID
		
		   
    FROM   WebAPI AS W
           left JOIN WebAPIDetail AS WD
             ON W.WebApiId = WD.WebApiId
   
             --LEFT JOIN [eCommerceDTL] ED
             --ON WD.[numDomainId] = ED.[numDomainId]
              AND WD.numDomainID = @numDomainID
   WHERE  (W.WebApiId = @WebApiID OR @WebApiID =0)
    
    
 	
 END
 
 ELSE IF @tintMode=4
 BEGIN
 	SELECT W.vcProviderName,
           W.vcFirstFldName,
           W.vcSecondFldName,
           W.vcThirdFldName,
           W.vcFourthFldName,
           W.vcFifthFldName,
           W.vcSixthFldName,
           W.vcSeventhFldName,
           W.vcEighthFldName,
           W.vcNinthFldName,
           W.vcTenthFldName,
           W.vcThirteenthFldName,
           W.vcFourteenthFldName,
           W.vcFifteenthFldName,
           WD.vcFifthFldValue,
           WD.vcFourthFldValue,
           WD.vcThirdFldValue,
           WD.vcSecondFldValue,
           WD.vcFirstFldValue,
           WD.vcSixthFldValue,
           WD.vcSeventhFldValue,
           WD.vcEighthFldValue,
           WD.vcNinthFldValue,
           WD.vcTenthFldValue,
            WD.vcEleventhFldValue,
            WD.vcTwelfthFldValue,
             WD.vcThirteenthFldValue,
              WD.vcFourteenthFldValue,
                WD.vcFifteenthFldValue,
				WD.vcSixteenthFldValue,
           ISNULL(vcSettingURL,'#') vcSettingURL,
           ISNULL(WD.bitEnableAPI,0) bitEnableAPI,
		   ISNULL(WD.bitEnableItemUpdate,0) bitEnableItemUpdate,
		   ISNULL(WD.bitEnableOrderImport,0) bitEnableOrderImport,
		   ISNULL(WD.bitEnableTrackingUpdate,0) bitEnableTrackingUpdate,
		   ISNULL(WD.bitEnableInventoryUpdate,0) bitEnableInventoryUpdate,

           W.WebApiId,
           WD.numDomainId,
           --ISNULL(ED.[numDefaultWareHouseID],0) numDefaultWareHouseID,
           --ISNULL(ED.[numDefaultIncomeChartAcntId],0) numDefaultIncomeChartAcntId,
           --ISNULL(ED.[numDefaultAssetChartAcntId],0) numDefaultAssetChartAcntId,
           --ISNULL(ED.[numDefaultCOGSChartAcntId],0) numDefaultCOGSChartAcntId,
          -- ISNULL((SELECT MAX(CONVERT(NUMERIC(9),[vcAPIOppId])) FROM   [OpportunityMasterAPI] WHERE  [numdomainid] = WD.numDomainId AND [WebApiId] =W.WebApiId ),0) AS LastInsertedOrderID,
		    ISNULL(WD.numBizDocId,0) numBizDocId,
			ISNULL(WD.numBizDocStatusId,0) numBizDocStatusId,
		    ISNULL(WD.numOrderStatus,0) numOrderStatus,
			ISNULL(WD.numFBABizDocId,0) numFBABizDocId,
			ISNULL(WD.numFBABizDocStatusId,0) numFBABizDocStatusId,
			ISNULL(WD.numUnShipmentOrderStatus,0) numUnShipmentOrderStatus,
		    ISNULL(WD.numRecordOwner,0) numRecordOwner ,
		    ISNULL(WD.numAssignTo,0) numAssignTo,
		    ISNULL(WD.numWareHouseID,0) numWareHouseID,
		    ISNULL(WD.numRelationshipId,0) numRelationshipId,
		     ISNULL(WD.numProfileId,0) numProfileId,
		    ISNULL(WD.numExpenseAccountId,0) numExpenseAccountId,
		    --ISNULL(WD.numDiscountItemMapping,0) numDiscountItemMapping,
			ISNULL(WD.numSalesTaxItemMapping,0) numSalesTaxItemMapping,
			ISNULL((SELECT numShippingServiceItemID FROM Domain WHERE numDomainID = WD.numDomainID),0) AS numShippingServiceItemID,
			ISNULL((SELECT numDisCountServiceItemID FROM Domain WHERE numDomainID = WD.numDomainID),0) AS numDiscountItemMapping,
		ISNULL((SELECT vcShipToPhoneNo FROM Domain WHERE numDomainID = WD.numDomainID),0) AS vcShipToPhoneNo,
		ISNULL((SELECT numAdminID FROM Domain WHERE numDomainID = WD.numDomainID),0) AS numAdminID
		
		   
    FROM   WebAPI AS W
           left JOIN WebAPIDetail AS WD
             ON W.WebApiId = WD.WebApiId
   
             --LEFT JOIN [eCommerceDTL] ED
             --ON WD.[numDomainId] = ED.[numDomainId]
              AND WD.numDomainID = @numDomainID
   WHERE  (W.WebApiId = @WebApiID OR @WebApiID =0) AND WD.bitEnableAPI=1
    
    
 	
 END
  END


