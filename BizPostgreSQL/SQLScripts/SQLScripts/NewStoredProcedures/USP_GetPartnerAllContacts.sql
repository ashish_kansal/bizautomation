/****** Object:  StoredProcedure [dbo].[USP_ShoppingCart]    Script Date: 05/07/2009 22:09:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj                  
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetPartnerAllContacts')
DROP PROCEDURE USP_GetPartnerAllContacts
GO
CREATE PROCEDURE [dbo].[USP_GetPartnerAllContacts]                                                                                    
 @numDomainID  numeric=0                   
AS                                                                       
BEGIN   
	
	SELECT DISTINCT A.numContactId,A.vcFirstName+' '+A.vcLastName AS vcGivenName FROM OpportunityMaster AS O
	LEFT JOIN AdditionalContactsInformation AS A 
	ON O.numPartenerContact=A.numContactId
	WHERE A.numDomainID=@numDomainID AND ISNULL(O.numPartenerContact,0)>0 AND A.vcGivenName<>''

END

