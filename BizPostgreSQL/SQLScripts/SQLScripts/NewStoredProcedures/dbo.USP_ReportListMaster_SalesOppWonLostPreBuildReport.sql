SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ReportListMaster_SalesOppWonLostPreBuildReport')
DROP PROCEDURE USP_ReportListMaster_SalesOppWonLostPreBuildReport
GO
CREATE PROCEDURE [dbo].[USP_ReportListMaster_SalesOppWonLostPreBuildReport]
	@numDomainID NUMERIC(18,0)
	,@ClientTimeZoneOffset INT
	,@vcTimeLine VARCHAR(50)
	,@vcGroupBy VARCHAR(50)
	,@vcTeritorry VARCHAR(MAX)
	,@vcFilterBy TINYINT  -- 1:Assign TO, 2: Record Owner, 3: Teams (Based on Assigned To) 
	,@vcFilterValue VARCHAR(MAX)
	,@dtFromDate DATE
	,@dtToDate DATE
AS
BEGIN 
	DECLARE @tintFiscalStartMonth TINYINT

	SELECT @tintFiscalStartMonth=ISNULL(tintFiscalStartMonth,1) FROM Domain WHERE numDomainId=@numDomainID
	DECLARE @CurrentYearStartDate DATE 
	DECLARE @CurrentYearEndDate DATE

	SET @CurrentYearStartDate = DATEFROMPARTS(YEAR(DATEADD(MINUTE,-@ClientTimeZoneOffset,GETUTCDATE())),@tintFiscalStartMonth,1)
	IF @CurrentYearStartDate > DATEADD(MINUTE,-@ClientTimeZoneOffset,GETUTCDATE())
	BEGIN
		SET @CurrentYearStartDate = DATEADD(YEAR,-1,@CurrentYearStartDate)
	END	
	SET @CurrentYearEndDate = DATEADD(DAY,-1, DATEADD(YEAR,1,@CurrentYearStartDate))

	DECLARE @TABLEQuater TABLE
	(
		ID INT
		,QuaterStartDate DATE
		,QuaterEndDate DATE
	)

	INSERT INTO @TABLEQuater
	(
		ID
		,QuaterStartDate
		,QuaterEndDate
	)
	VALUES
	(
		1,DATEFROMPARTS(YEAR(@CurrentYearStartDate),@tintFiscalStartMonth,1),DATEADD(DAY,-1,DATEADD(MONTH,3,DATEFROMPARTS(YEAR(@CurrentYearStartDate),@tintFiscalStartMonth,1)))
	),
	(
		2,DATEADD(MONTH,3,DATEFROMPARTS(YEAR(@CurrentYearStartDate),@tintFiscalStartMonth,1)),DATEADD(DAY,-1,DATEADD(MONTH,6,DATEFROMPARTS(YEAR(@CurrentYearStartDate),@tintFiscalStartMonth,1)))
	),
	(
		3,DATEADD(MONTH,6,DATEFROMPARTS(YEAR(@CurrentYearStartDate),@tintFiscalStartMonth,1)),DATEADD(DAY,-1,DATEADD(MONTH,9,DATEFROMPARTS(YEAR(@CurrentYearStartDate),@tintFiscalStartMonth,1)))
	),
	(
		4,DATEADD(MONTH,9,DATEFROMPARTS(YEAR(@CurrentYearStartDate),@tintFiscalStartMonth,1)),DATEADD(DAY,-1,DATEADD(MONTH,12,DATEFROMPARTS(YEAR(@CurrentYearStartDate),@tintFiscalStartMonth,1)))
	)

	IF @vcTimeLine = 'CurYear'
	BEGIN
		SET @dtFromDate = @CurrentYearStartDate
		SET @dtToDate = @CurrentYearEndDate
	END
	ELSE IF @vcTimeLine = 'PreYear'
	BEGIN
		SET @dtFromDate =  DATEADD(YEAR,-1,@CurrentYearStartDate)
		SET @dtToDate = DATEADD(DAY,-1, DATEADD(YEAR,1,@dtFromDate))
	END
	ELSE IF @vcTimeLine = 'Pre2Year'
	BEGIN
		SET @dtFromDate = DATEADD(YEAR,-2,@CurrentYearStartDate)
		SET @dtToDate = DATEADD(DAY,-1,DATEADD(YEAR,2,@dtFromDate))
	END
	ELSE IF @vcTimeLine = 'Ago2Year'
	BEGIN
		SET @dtFromDate =  NULL
		SET @dtToDate = DATEADD(DAY,-1,DATEADD(YEAR,-2,@CurrentYearStartDate))
	END
	ELSE IF @vcTimeLine = 'CurPreYear'
	BEGIN
		SET @dtFromDate = DATEADD(YEAR,-1,@CurrentYearStartDate)
		SET @dtToDate = DATEADD(DAY,-1, DATEADD(YEAR,1,@dtFromDate))
	END
	ELSE IF @vcTimeLine = 'CurPre2Year'
	BEGIN
		SET @dtFromDate = DATEADD(YEAR,-2,@CurrentYearStartDate)
		SET @dtToDate = DATEADD(DAY,-1,DATEADD(YEAR,1, DATEFROMPARTS(YEAR(DATEADD(MINUTE,-@ClientTimeZoneOffset,GETUTCDATE())),@tintFiscalStartMonth,1)))
	END
	ELSE IF @vcTimeLine = 'CuQur'
	BEGIN
		SELECT
			@dtFromDate = QuaterStartDate
			,@dtToDate = QuaterEndDate
		FROM
			@TABLEQuater
		WHERE
			DATEADD(MINUTE,-@ClientTimeZoneOffset,GETUTCDATE()) BETWEEN QuaterStartDate AND QuaterEndDate
	END
	ELSE IF @vcTimeLine = 'PreQur'
	BEGIN
		SELECT
			@dtFromDate = DATEADD(MONTH,-3,QuaterStartDate)
			,@dtToDate = DATEADD(MONTH,-3,QuaterEndDate)
		FROM
			@TABLEQuater
		WHERE
			DATEADD(MINUTE,-@ClientTimeZoneOffset,GETUTCDATE()) BETWEEN QuaterStartDate AND QuaterEndDate
	END
	ELSE IF @vcTimeLine = 'CurPreQur'
	BEGIN
		SELECT
			@dtFromDate = DATEADD(MONTH,-3,QuaterStartDate)
			,@dtToDate = QuaterEndDate
		FROM
			@TABLEQuater
		WHERE
			DATEADD(MINUTE,-@ClientTimeZoneOffset,GETUTCDATE()) BETWEEN QuaterStartDate AND QuaterEndDate
	END
	ELSE IF @vcTimeLine = 'ThisMonth'
	BEGIN
		SET @dtFromDate = dbo.get_month_start(DATEADD(MINUTE,-@ClientTimeZoneOffset,GETUTCDATE()))
		SET @dtToDate = dbo.get_month_end(DATEADD(MINUTE,-@ClientTimeZoneOffset,GETUTCDATE()))
	END
	ELSE IF @vcTimeLine = 'LastMonth'
	BEGIN
		SET @dtFromDate = DATEADD(MONTH,-1,dbo.get_month_start(DATEADD(MINUTE,-@ClientTimeZoneOffset,GETUTCDATE())))
		SET @dtToDate = DATEADD(MONTH,-1,dbo.get_month_end(DATEADD(MINUTE,-@ClientTimeZoneOffset,GETUTCDATE())))
	END
	ELSE IF @vcTimeLine = 'CurPreMonth'
	BEGIN
		SET @dtFromDate = DATEADD(MONTH,-1,dbo.get_month_start(DATEADD(MINUTE,-@ClientTimeZoneOffset,GETUTCDATE())))
		SET @dtToDate = dbo.get_month_end(DATEADD(MINUTE,-@ClientTimeZoneOffset,GETUTCDATE()))
	END
	ELSE IF @vcTimeLine = 'LastWeek'
	BEGIN
		SET @dtFromDate = DATEADD(DAY,-7,dbo.get_week_start(DATEADD(MINUTE,-@ClientTimeZoneOffset,GETUTCDATE())))
		SET @dtToDate = DATEADD(DAY,-7,dbo.get_week_end(DATEADD(MINUTE,-@ClientTimeZoneOffset,GETUTCDATE())))
	END
	ELSE IF @vcTimeLine = 'ThisWeek'
	BEGIN
		SET @dtFromDate = dbo.get_week_start(DATEADD(MINUTE,-@ClientTimeZoneOffset,GETUTCDATE()))
		SET @dtToDate = dbo.get_week_end(DATEADD(MINUTE,-@ClientTimeZoneOffset,GETUTCDATE()))
	END
	ELSE IF @vcTimeLine = 'Yesterday'
	BEGIN
		SET @dtFromDate = DATEADD(DAY,-1,DATEADD(MINUTE,-@ClientTimeZoneOffset,GETUTCDATE()))
		SET @dtToDate = DATEADD(DAY,-1,DATEADD(MINUTE,-@ClientTimeZoneOffset,GETUTCDATE()))
	END
	ELSE IF @vcTimeLine = 'Today'
	BEGIN
		SET @dtFromDate = DATEADD(MINUTE,-@ClientTimeZoneOffset,GETUTCDATE())
		SET @dtToDate = DATEADD(MINUTE,-@ClientTimeZoneOffset,GETUTCDATE())
	END
	ELSE IF @vcTimeLine = 'Last7Day'
	BEGIN
		SET @dtFromDate = DATEADD(DAY,-7,DATEADD(MINUTE,-@ClientTimeZoneOffset,GETUTCDATE()))
		SET @dtToDate = DATEADD(MINUTE,-@ClientTimeZoneOffset,GETUTCDATE())
	END
	ELSE IF @vcTimeLine = 'Last30Day'
	BEGIN
		SET @dtFromDate = DATEADD(DAY,-30,DATEADD(MINUTE,-@ClientTimeZoneOffset,GETUTCDATE()))
		SET @dtToDate = DATEADD(MINUTE,-@ClientTimeZoneOffset,GETUTCDATE())
	END
	ELSE IF @vcTimeLine = 'Last60Day'
	BEGIN
		SET @dtFromDate = DATEADD(DAY,-60,DATEADD(MINUTE,-@ClientTimeZoneOffset,GETUTCDATE()))
		SET @dtToDate = DATEADD(MINUTE,-@ClientTimeZoneOffset,GETUTCDATE())
	END
	ELSE IF @vcTimeLine = 'Last90Day'
	BEGIN
		SET @dtFromDate = DATEADD(DAY,-90,DATEADD(MINUTE,-@ClientTimeZoneOffset,GETUTCDATE()))
		SET @dtToDate = DATEADD(MINUTE,-@ClientTimeZoneOffset,GETUTCDATE())
	END
	ELSE IF @vcTimeLine = 'Last120Day'
	BEGIN
		SET @dtFromDate = DATEADD(DAY,-120,DATEADD(MINUTE,-@ClientTimeZoneOffset,GETUTCDATE()))
		SET @dtToDate = DATEADD(MINUTE,-@ClientTimeZoneOffset,GETUTCDATE())
	END

	DECLARE @TempResult TABLE
	(
		ID INT IDENTITY(1,1)
		,GroupValue VARCHAR(100)
		,StartDate DATE
		,EndDate DATE
		,SalesOppAmount DECIMAL(20,5)
		,ClosedWonAmount DECIMAL(20,5)
		,CloseWonPercent DECIMAL(20,2)
		,CloseLostAmount DECIMAL(20,5)
		,CloseLostPercent DECIMAL(20,2)
	)


	IF @vcGroupBy = 'Quater'
	BEGIN
		;WITH CTEYear
		AS
		(
			SELECT  
				CASE WHEN @dtFromDate < DATEFROMPARTS(DATEPART(YEAR,@dtFromDate),@tintFiscalStartMonth,1) THEN DATEPART(YEAR,DATEADD(YEAR,-1,@dtFromDate)) ELSE DATEPART(YEAR,@dtFromDate) END  AS yr
			UNION ALL
			SELECT 
				yr + 1
			FROM 
				CTEYear
			WHERE 
				yr < DATEPART(YEAR, @dtToDate)
		)
		INSERT INTO @TempResult
		(
			GroupValue
			,StartDate
			,EndDate
		)
		SELECT 
			CONCAT(yr,'-',Quater)
			,QuaterStart
			,QuaterEnd
		FROM 
			CTEYear
		CROSS APPLY
		(
			SELECT
				'Q1' Quater,
				DATEFROMPARTS(yr,@tintFiscalStartMonth,1) QuaterStart
				,DATEADD(DAY,-1,DATEADD(MONTH,3,DATEFROMPARTS(yr,@tintFiscalStartMonth,1))) QuaterEnd
			UNION
			SELECT
				'Q2' Quater,
				DATEADD(MONTH,3,DATEFROMPARTS(yr,@tintFiscalStartMonth,1)) QuaterStart
				,DATEADD(DAY,-1,DATEADD(MONTH,6,DATEFROMPARTS(yr,@tintFiscalStartMonth,1))) QuaterEnd
			UNION
			SELECT
				'Q3' Quater,
				DATEADD(MONTH,6,DATEFROMPARTS(yr,@tintFiscalStartMonth,1)) QuaterStart
				,DATEADD(DAY,-1,DATEADD(MONTH,9,DATEFROMPARTS(yr,@tintFiscalStartMonth,1))) QuaterEnd
			UNION
			SELECT
				'Q4' Quater,
				DATEADD(MONTH,9,DATEFROMPARTS(yr,@tintFiscalStartMonth,1)) QuaterStart
				,DATEADD(DAY,-1,DATEADD(MONTH,12,DATEFROMPARTS(yr,@tintFiscalStartMonth,1))) QuaterEnd
		) Test
		WHERE
			(QuaterStart BETWEEN @dtFromDate AND @dtToDate) OR 
			(QuaterEnd BETWEEN @dtFromDate AND @dtToDate) OR 
			(QuaterStart <= @dtFromDate AND QuaterEnd >= @dtToDate)
			
	END
	ELSE IF @vcGroupBy = 'Year'
	BEGIN
		;WITH CTEYear
		AS
		(
			SELECT  
				DATEPART(YEAR,@dtFromDate) AS yr
			UNION ALL
			SELECT 
				yr + 1
			FROM 
				CTEYear
			WHERE 
				yr < DATEPART(YEAR, @dtToDate)
		)
		INSERT INTO @TempResult
		(
			GroupValue
			,StartDate
			,EndDate
		)
		SELECT 
			CAST(yr AS VARCHAR(100))
			,DATEFROMPARTS(yr,@tintFiscalStartMonth,1)
			,DATEADD(DAY,-1,DATEADD(YEAR,1,DATEFROMPARTS(yr,@tintFiscalStartMonth,1))) 
		FROM 
			CTEYear
	END
	ELSE -- MONTH
	BEGIN
		;with CTEMonth (monthDate,StartDate,EndDate)
		AS
		(
			SELECT 
				@dtFromDate
				,dbo.get_month_start(@dtFromDate)
				,dbo.get_month_end(@dtFromDate)
			UNION ALL
			SELECT 
				DATEADD(month,1,monthDate)
				,dbo.get_month_start(DATEADD(month,1,monthDate))
				,dbo.get_month_end(DATEADD(month,1,monthDate))
			FROM 
				CTEMonth
			WHERE 
				DATEADD(month,1,monthDate)<=@dtToDate
		)
		
		INSERT INTO @TempResult
		(
			GroupValue
			,StartDate
			,EndDate
		)
		SELECT 
			CONCAT(DATENAME(MONTH,monthDate),'-',DATENAME(YEAR,monthDate))
			,StartDate
			,EndDate 
		FROM 
			CTEMonth
	END

	DECLARE @TempOppData TABLE
	(
		TotalRows NUMERIC(18,0)
		,CreatedDate DATETIME
		,WonDate DATE
		,LostDate DATE
		,monDealAmount DECIMAL(20,5)
	)

	INSERT INTO @TempOppData
	(
		TotalRows
		,CreatedDate
		,WonDate
		,LostDate
		,monDealAmount
	)	
	SELECT
		COUNT(numOppId) OVER() AS TotalRows
		,DATEADD(MINUTE,-@ClientTimeZoneOffset,OpportunityMaster.bintCreatedDate) CreatedDate
		,(CASE WHEN ISNULL(tintOppStatus,0)=1 AND OpportunityMaster.bintOppToOrder IS NOT NULL THEN DATEADD(MINUTE,-@ClientTimeZoneOffset,OpportunityMaster.bintOppToOrder) ELSE NULL END) WonDate
		,(CASE WHEN ISNULL(tintOppStatus,0)=2 AND OpportunityMaster.dtDealLost IS NOT NULL THEN DATEADD(MINUTE,-@ClientTimeZoneOffset,OpportunityMaster.dtDealLost) ELSE NULL END) LostDate
		,monDealAmount
	FROM
		OpportunityMaster
	INNER JOIN
		DivisionMaster
	ON
		OpportunityMaster.numDivisionId = DivisionMaster.numDivisionID
	WHERE
		OpportunityMaster.numDomainId=@numDomainID
		AND ISNULL(tintOppType,0)=1
		AND (OpportunityMaster.bintOppToOrder IS NOT NULL OR OpportunityMaster.dtDealLost IS NOT NULL)
		AND 1 = (CASE WHEN LEN(ISNULL(@vcTeritorry,'')) > 0 THEN (CASE WHEN DivisionMaster.numTerID IN (SELECT Id FROM dbo.SplitIDs(@vcFilterValue,',')) THEN 1 ELSE 0 END) ELSE 1 END)
		AND 1 = (CASE 
					WHEN LEN(ISNULL(@vcFilterValue,0)) > 0 AND @vcFilterBy IN (1,2,3)
					THEN 
						(CASE @vcFilterBy
							WHEN 1  -- Assign To
							THEN (CASE WHEN OpportunityMaster.numAssignedTo IN (SELECT Id FROM dbo.SplitIDs(@vcFilterValue,',')) THEN 1 ELSE 0 END)
							WHEN 2  -- Record Owner
							THEN (CASE WHEN OpportunityMaster.numRecOwner IN (SELECT Id FROM dbo.SplitIDs(@vcFilterValue,',')) THEN 1 ELSE 0 END)
							WHEN 3  -- Teams (Based on Assigned To)
							THEN (CASE WHEN OpportunityMaster.numAssignedTo IN (SELECT numUserCntID FROM UserTeams WHERE numDomainID=@numDomainID AND numTeam IN (SELECT Id FROM dbo.SplitIDs(@vcFilterValue,','))) THEN 1 ELSE 0 END)
						END)
					ELSE 1 
				END)
		AND (CAST(DATEADD(MINUTE,-@ClientTimeZoneOffset,OpportunityMaster.bintCreatedDate) AS DATE) BETWEEN @dtFromDate AND @dtToDate 
			OR CAST(DATEADD(MINUTE,-@ClientTimeZoneOffset,OpportunityMaster.bintOppToOrder) AS DATE) BETWEEN @dtFromDate AND @dtToDate
			OR CAST(DATEADD(MINUTE,-@ClientTimeZoneOffset,OpportunityMaster.dtDealLost) AS DATE) BETWEEN @dtFromDate AND @dtToDate)

	DECLARE @i AS INT = 1
	DECLARE @iCount AS INT
	SELECT @iCount=COUNT(*) FROM @TempResult

	WHILE @i <= @iCount
	BEGIN

		UPDATE
			@TempResult
		SET
			SalesOppAmount = ISNULL((SELECT SUM(monDealAmount) FROM @TempOppData WHERE (CreatedDate BETWEEN StartDate AND EndDate OR LostDate BETWEEN StartDate AND EndDate OR WonDate BETWEEN StartDate AND EndDate)),0)
			,ClosedWonAmount = ISNULL((SELECT SUM(monDealAmount) FROM @TempOppData WHERE WonDate BETWEEN StartDate AND EndDate),0)
			,CloseWonPercent = (CASE WHEN ISNULL((SELECT COUNT(*) FROM @TempOppData WHERE (CreatedDate BETWEEN StartDate AND EndDate OR LostDate BETWEEN StartDate AND EndDate OR WonDate BETWEEN StartDate AND EndDate)),0) > 0 THEN (ISNULL((SELECT COUNT(*) FROM @TempOppData WHERE WonDate BETWEEN StartDate AND EndDate),0) * 100) / ISNULL((SELECT COUNT(*) FROM @TempOppData WHERE (CreatedDate BETWEEN StartDate AND EndDate OR LostDate BETWEEN StartDate AND EndDate OR WonDate BETWEEN StartDate AND EndDate)),0) ELSE 0 END)
			,CloseLostAmount = ISNULL((SELECT SUM(monDealAmount) FROM @TempOppData WHERE LostDate BETWEEN StartDate AND EndDate),0)
			,CloseLostPercent = (CASE WHEN ISNULL((SELECT COUNT(*) FROM @TempOppData WHERE (CreatedDate BETWEEN StartDate AND EndDate OR LostDate BETWEEN StartDate AND EndDate OR WonDate BETWEEN StartDate AND EndDate)),0) > 0 THEN (ISNULL((SELECT COUNT(*) FROM @TempOppData WHERE LostDate BETWEEN StartDate AND EndDate),0) * 100) / ISNULL((SELECT COUNT(*) FROM @TempOppData WHERE (CreatedDate BETWEEN StartDate AND EndDate OR LostDate BETWEEN StartDate AND EndDate OR WonDate BETWEEN StartDate AND EndDate)),0) ELSE 0 END)
		WHERE
			ID=@i


		SET @i = @i + 1
	END

	SELECT * FROM @TempResult
END
GO