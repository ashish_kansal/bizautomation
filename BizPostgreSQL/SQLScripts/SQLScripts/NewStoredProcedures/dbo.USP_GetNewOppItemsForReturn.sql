--created by anoop jayaraj      
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetNewOppItemsForReturn')
DROP PROCEDURE USP_GetNewOppItemsForReturn
GO
CREATE PROCEDURE [dbo].[USP_GetNewOppItemsForReturn]      
@tintOppType as tinyint,      
@numDomainID as numeric(9),      
@numDivisionID as numeric(9)=0,      
@str as varchar(20) ,
@numUserCntID as numeric(9),
@tintSearchOrderCustomerHistory as tinyint=0     
as   

--IF @tintOppType=0
--BEGIN
--	SELECT '' AS NoValue
--	SELECT '' AS NoValue
--	RETURN 
--END

 select * into #Temp1 from (select numFieldId,vcDbColumnName,ISNULL(vcCultureFieldName,vcFieldName) AS vcFieldName,tintRow AS tintOrder,0 as Custom
    from View_DynamicColumns
   where numFormId=22 and numUserCntID=@numUserCntID and numDomainID=@numDomainID AND tintPageType=1 
        AND bitCustom=0 AND ISNULL(bitSettingField,0)=1 AND numRelCntType=0
union   
 select numFieldId,vcFieldName,vcFieldName,tintRow AS tintOrder,1 as Custom
    from View_DynamicCustomColumns  
   where Grp_id=5 AND numFormId=22 and numUserCntID=@numUserCntID and numDomainID=@numDomainID 
   AND tintPageType=1 AND bitCustom=1 AND numRelCntType=0)X 
  
  if not exists(select * from #Temp1)
  begin
    insert into #Temp1
     select numFieldId,vcDbColumnName,ISNULL(vcCultureFieldName,vcFieldName) AS vcFieldName,tintorder,0 from View_DynamicDefaultColumns                                                  
     where numFormId=22 and bitDefault = 1 and ISNULL(bitSettingField,0)=1 and numDomainID=@numDomainID 
  end

Create table #tempItemCode (numItemCode numeric(9))

if @tintOppType>0       
begin 
DECLARE @bitRemoveVendorPOValidation AS BIT

SELECT @bitRemoveVendorPOValidation=ISNULL(bitRemoveVendorPOValidation,0) FROM domain WHERE numDomainID=@numDomainID

Declare @strSQL as varchar(8000)
declare @tintOrder as int
declare @Fld_id as varchar(20)
declare @Fld_Name as varchar(20)
set @strSQL=''
	select top 1 @tintOrder=tintOrder+1,@Fld_id=numFieldId,@Fld_Name=vcFieldName from #Temp1 where Custom=1 order by tintOrder
	while @tintOrder>0
    begin
	set @strSQL=@strSQL+', dbo.GetCustFldValueItem('+@Fld_id+', I.numItemCode) as ['+ @Fld_Name+']'

	select top 1 @tintOrder=tintOrder+1,@Fld_id=numFieldId,@Fld_Name=vcFieldName from #Temp1 where Custom=1 and tintOrder>=@tintOrder order by tintOrder
	if @@rowcount=0 set @tintOrder=0
	end


--Temp table for Item Search Configuration

 select * into #tempSearch from (select numFieldId,vcDbColumnName,ISNULL(vcCultureFieldName,vcFieldName) AS vcFieldName,tintRow AS tintOrder,0 as Custom
    from View_DynamicColumns
   where numFormId=22 and numUserCntID=@numUserCntID and numDomainID=@numDomainID AND tintPageType=1 
        AND bitCustom=0  AND ISNULL(bitSettingField,0)=1 AND numRelCntType=1
union   
  select numFieldId,vcFieldName,vcFieldName,tintRow AS tintOrder,1 as Custom
    from View_DynamicCustomColumns   
   where Grp_id=5 AND numFormId=22 and numUserCntID=@numUserCntID and numDomainID=@numDomainID 
   AND tintPageType=1 AND bitCustom=1 AND numRelCntType=1)X 
  
  if not exists(select * from #tempSearch)
  begin
    insert into #tempSearch
    select numFieldId,vcDbColumnName,ISNULL(vcCultureFieldName,vcFieldName) AS vcFieldName,tintorder,0 from View_DynamicDefaultColumns                                                  
     where numFormId=22 and bitDefault = 1 and ISNULL(bitSettingField,0)=1 and numDomainID=@numDomainID 
  END

--Regular Search
Declare @strSearch as varchar(8000),@CustomSearch AS VARCHAR(4000),@numFieldId AS NUMERIC(9)
SET @strSearch=''
	select top 1 @tintOrder=tintOrder+1,@Fld_Name=vcDbColumnName,@numFieldId=numFieldId from #tempSearch WHERE Custom=0 order by tintOrder
	while @tintOrder>-1
    BEGIN
	IF @Fld_Name='vcPartNo'
		insert into #tempItemCode SELECT distinct Item.numItemCode FROM Vendor join Item on Item.numItemCode=Vendor.numItemCode 
										where Item.numDomainID=@numDomainID and Vendor.numDomainID=@numDomainID 
										and Vendor.vcPartNo is not null and len(Vendor.vcPartNo)>0 and vcPartNo LIKE '%'+@str+'%'

-- 		set @strSearch=@strSearch+ ' I.numItemCode in (SELECT distinct Item.numItemCode FROM Vendor join Item on Item.numItemCode=Vendor.numItemCode 
--										where Item.numDomainID='+convert(varchar(15),@numDomainID) + ' and Vendor.numDomainID='+convert(varchar(15),@numDomainID) + ' 
--										and Vendor.vcPartNo is not null and len(Vendor.vcPartNo)>0 and vcPartNo LIKE ''%'+@str+'%'')'
	ELSE IF @Fld_Name='vcBarCode'
		insert into #tempItemCode SELECT distinct Item.numItemCode FROM Item join WareHouseItems WI on WI.numDomainID=@numDomainID and Item.numItemCode=WI.numItemID join Warehouses W on W.numDomainID=@numDomainID and W.numWareHouseID=WI.numWareHouseID  
									 where Item.numDomainID=@numDomainID and isnull(Item.numItemGroup,0)>0 and WI.vcBarCode is not null and len(WI.vcBarCode)>0 and WI.vcBarCode LIKE '%'+@str+'%'

-- 		set @strSearch=@strSearch+ ' I.numItemCode in (SELECT distinct Item.numItemCode FROM Item join WareHouseItems WI on WI.numDomainID='+convert(varchar(15),@numDomainID) + ' and Item.numItemCode=WI.numItemID join Warehouses W on W.numDomainID='+convert(varchar(15),@numDomainID) + ' and W.numWareHouseID=WI.numWareHouseID  
--									 where Item.numDomainID='+convert(varchar(15),@numDomainID) + ' and isnull(Item.numItemGroup,0)>0 and WI.vcBarCode is not null and len(WI.vcBarCode)>0 and WI.vcBarCode LIKE ''%'+@str+'%'')'
	ELSE IF @Fld_Name='vcWHSKU'
		insert into #tempItemCode SELECT distinct Item.numItemCode FROM Item join WareHouseItems WI on WI.numDomainID=@numDomainID and Item.numItemCode=WI.numItemID join Warehouses W on W.numDomainID=@numDomainID and W.numWareHouseID=WI.numWareHouseID  
									 where Item.numDomainID=@numDomainID and isnull(Item.numItemGroup,0)>0 and WI.vcWHSKU is not null and len(WI.vcWHSKU)>0 and WI.vcWHSKU LIKE '%'+@str+'%'

-- 		set @strSearch=@strSearch+ ' I.numItemCode in (SELECT distinct Item.numItemCode FROM Item join WareHouseItems WI on WI.numDomainID='+convert(varchar(15),@numDomainID) + ' and Item.numItemCode=WI.numItemID join Warehouses W on W.numDomainID='+convert(varchar(15),@numDomainID) + ' and W.numWareHouseID=WI.numWareHouseID  
--									 where Item.numDomainID='+convert(varchar(15),@numDomainID) + ' and isnull(Item.numItemGroup,0)>0 and WI.vcWHSKU is not null and len(WI.vcWHSKU)>0 and WI.vcWHSKU LIKE ''%'+@str+'%'')'
	else    
		set @strSearch=@strSearch+' ['+ @Fld_Name + '] LIKE ''%'+@str+'%'''

	select top 1 @tintOrder=tintOrder+1,@Fld_Name=vcDbColumnName,@numFieldId=numFieldId from #tempSearch where tintOrder>=@tintOrder AND Custom=0 order by tintOrder
		if @@rowcount=0 
		begin
			set @tintOrder=-1
		END
		ELSE IF @Fld_Name!='vcPartNo' and @Fld_Name!='vcBarCode' and @Fld_Name!='vcWHSKU'
		begin
			set @strSearch=@strSearch + ' or '
		END
	END

IF (select count(*) from #tempItemCode)>0
	set @strSearch=@strSearch + ' or  I.numItemCode in (select numItemCode from #tempItemCode)' 
--Custom Search
SET @CustomSearch=''
select top 1 @tintOrder=tintOrder+1,@Fld_Name=vcDbColumnName,@numFieldId=numFieldId from #tempSearch WHERE Custom=1 order by tintOrder
	while @tintOrder>-1
    BEGIN
    
	set @CustomSearch=@CustomSearch + CAST(@numFieldId AS VARCHAR(10)) 

	select top 1 @tintOrder=tintOrder+1,@Fld_Name=vcDbColumnName,@numFieldId=numFieldId from #tempSearch where tintOrder>=@tintOrder AND Custom=1 order by tintOrder
		if @@rowcount=0 
		begin
			set @tintOrder=-1
		END
		ELSE
		begin
			set @CustomSearch=@CustomSearch + ' , '
		END
	END
	
IF LEN(@CustomSearch)>0
BEGIN
 	SET @CustomSearch= ' I.numItemCode in (SELECT RecId FROM dbo.CFW_FLD_Values_Item WHERE Fld_ID IN (' + @CustomSearch  + ') and Fld_Value like ''%' + @str + '%'')'

IF LEN(@strSearch)>0
       SET @strSearch=@strSearch + ' OR ' +  @CustomSearch
ELSE
       SET @strSearch=  @CustomSearch  
END


if @tintOppType=1 OR (@bitRemoveVendorPOValidation=1 AND @tintOppType=2)       
begin      
  set @strSQL='select TOP 20 I.numItemCode,isnull(vcItemName,'''') vcItemName,CASE WHEN I.[charItemType]=''P'' AND ISNULL(I.bitSerialized,0) = 0 AND  ISNULL(I.bitLotNo,0) = 0 THEN convert(varchar(100),ISNULL((SELECT TOP 1 ROUND(monWListPrice,2) FROM [WareHouseItems] WHERE [numItemID]=I.numItemCode),0)) ELSE CONVERT(VARCHAR(100), ROUND(monListPrice, 2)) END AS monListPrice,isnull(vcSKU,'''') vcSKU,isnull(numBarCodeId,0) numBarCodeId,isnull(vcModelID,'''') vcModelID,isnull(txtItemDesc,'''') as txtItemDesc,isnull(C.vcCompanyName,'''') as vcCompanyName'+@strSQL+' from Item  I     
  Left join DivisionMaster D              
  on I.numVendorID=D.numDivisionID  
  left join CompanyInfo C  
  on C.numCompanyID=D.numCompanyID  
  where ISNULL(I.bitSerialized,0) = 0 AND ISNULL(I.bitLotNo,0) = 0 AND ISNULL(I.Isarchieve,0) <> 1 And I.charItemType NOT IN(''A'') AND  I.numDomainID='+convert(varchar(20),@numDomainID)+' and (' +@strSearch+ ') '         
--- added Asset validation  by sojan

IF @bitRemoveVendorPOValidation=1 AND @tintOppType=2
	set @strSQL=@strSQL + ' and ISNULL(I.bitKitParent,0) = 0 '

if @tintSearchOrderCustomerHistory=1 and @numDivisionID>0
BEGIN
	set @strSQL=@strSQL + ' and I.numItemCode in (select distinct oppI.numItemCode from 
							OpportunityMaster oppM join OpportunityItems oppI on oppM.numOppId=oppI.numOppId where oppM.numDomainID='+convert(varchar(20),@numDomainID)+' and oppM.numDivisionId='+convert(varchar(20),@numDivisionId)+')'
END

set @strSQL=@strSQL + ' order by  vcItemName'
 print @strSQL
 exec (@strSQL)
end      
else if @tintOppType=2       
begin      
 set @strSQL='SELECT TOP 20 I.numItemCode,isnull(vcItemName,'''') vcItemName,convert(varchar(200),round(monListPrice,2)) as monListPrice,isnull(vcSKU,'''') vcSKU,isnull(numBarCodeId,0) numBarCodeId,isnull(vcModelID,'''') vcModelID,isnull(txtItemDesc,'''') as txtItemDesc,isnull(C.vcCompanyName,'''') as vcCompanyName'+@strSQL+'  from item I              
 join Vendor V              
 on V.numItemCode=I.numItemCode   
 Left join DivisionMaster D              
 on V.numVendorID=D.numDivisionID  
 left join CompanyInfo C  
 on C.numCompanyID=D.numCompanyID             
 where ISNULL(I.bitSerialized,0) = 0 AND ISNULL(I.bitLotNo,0) = 0 AND ISNULL(I.Isarchieve,0) <> 1 and ISNULL(I.bitKitParent,0) = 0 And I.charItemType NOT IN(''A'') AND V.numVendorID='+convert(varchar(20),@numDivisionID)+' and (' +@strSearch+ ') ' 
--- added Asset validation  by sojan

if @tintSearchOrderCustomerHistory=1 and @numDivisionID>0
BEGIN
	set @strSQL=@strSQL + ' and I.numItemCode in (select distinct oppI.numItemCode from 
							OpportunityMaster oppM join OpportunityItems oppI on oppM.numOppId=oppI.numOppId where oppM.numDomainID='+convert(varchar(20),@numDomainID)+' and oppM.numDivisionId='+convert(varchar(20),@numDivisionId)+')'
END

set @strSQL=@strSQL + ' order by  vcItemName'
 print @strSQL
 exec (@strSQL)
end
else
select 0  
end
ELSE
	select 0  

select * from  #Temp1 where vcDbColumnName not in ('vcPartNo','vcBarCode','vcWHSKU')ORDER BY tintOrder 

drop table #Temp1
         
drop table #tempItemCode
  
