GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_BizAPI_ValidateSalesOrder' ) 
    DROP PROCEDURE USP_BizAPI_ValidateSalesOrder
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author: Sandeep Patel
-- Create date: 24 April 2014
-- Description:	Checks whether data supplied by user is valid or not
-- =============================================
CREATE PROCEDURE USP_BizAPI_ValidateSalesOrder
	@numDomainID NUMERIC(18,0),
	@numCustomerID NUMERIC(18,0) = NULL,
	@numContactID NUMERIC(18,0) = NULL,
	@vcItems VARCHAR(MAX)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	DECLARE @Item TABLE (ID BIGINT)
	DECLARE @ErrorCode int
	DECLARE @str VARCHAR(20)
    DECLARE @index INT
	
	DECLARE @TotalItem INT
	DECLARE @ItemFound INT
	
	SET @ErrorCode = 0
	SET @TotalItem = 0
	SET @ItemFound = 0
	
	IF @numCustomerID IS NOT NULL
	BEGIN
		IF NOT EXISTS (SELECT * FROM dbo.DivisionMaster WHERE numDivisionID = @numCustomerID AND numDomainID = @numDomainID )
			SET @ErrorCode = 60000;
	END
	
	IF @numContactID IS NOT NULL AND @ErrorCode = 0
	BEGIN
		IF NOT EXISTS (SELECT * FROM dbo.AdditionalContactsInformation WHERE numContactId =  @numContactID)
			SET @ErrorCode = 60001;
	END
	
	IF @vcItems IS NOT NULL AND @ErrorCode = 0
	BEGIN
		
		SET @index = CharIndex(',',@vcItems);
		WHILE @index > 0
		BEGIN
			  SET @str = SUBSTRING(@vcItems,1,@index-1)
			  SET @vcItems = SUBSTRING(@vcItems,@index+1,LEN(@vcItems)-@index)
			  INSERT INTO @Item (ID) values (@str)
			  SET @index = CharIndex(',',@vcItems)
		END
		SET @str = @vcItems
		INSERT INTO @Item (ID) values (@str)
		
		SELECT @TotalItem = COUNT(*) FROM @Item
		SELECT @ItemFound = COUNT(*) FROM Item WHERE numDomainID = @numDomainID AND numItemCode IN (SELECT ID FROM @Item)
		
		IF @TotalItem <> @ItemFound 
			SET @ErrorCode = 60002;
	END
	
	SELECT @ErrorCode;
END
GO