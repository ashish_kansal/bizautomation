/****** Object:  StoredProcedure [dbo].[USP_GetBizDocInventoryItems]    Script Date: 05/07/2009 17:36:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- EXEC USP_GetBizDocInventoryItems 2172,1
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetBizDocInventoryItems')
DROP PROCEDURE USP_GetBizDocInventoryItems
GO
CREATE PROCEDURE [dbo].[USP_GetBizDocInventoryItems]
--                @numOppId        AS NUMERIC(9)  = NULL,
                @numOppBizDocID AS NUMERIC(9)  ,
                @numDomainID     AS NUMERIC(9)  = 0
AS
  BEGIN
  
    SELECT 
			BI.[numOppBizDocItemID],
			I.[numItemCode],
			WI.numWareHouseID,
			I.vcItemName,
           I.vcModelID,
           ISNULL(I.fltWeight,0) fltWeight,
           ISNULL(I.fltHeight,0) fltHeight,
           ISNULL(I.fltWidth,0) fltWidth,
           ISNULL(I.fltLength,0) fltLength,
           0 intNoOfBox,
           BI.vcItemDesc,
           (ISNULL(BI.[numUnitHour],0) * ISNULL(fltWeight,0)) AS TotalWeight,
		   ISNULL(BI.[numUnitHour],0) AS numUnitHour,
		   ISNULL(I.bitContainer,0) AS bitContainer,ISNULL(I.numNoItemIntoContainer,0) AS numNoItemIntoContainer,
		   ISNULL(I.numContainer,0) AS numContainer
    FROM   
		OpportunityBizDocItems AS BI
    INNER JOIN 
		Item AS I
    ON 
		BI.numItemCode = I.numItemCode
	INNER JOIN
		WareHouseItems WI
	ON
		BI.numWarehouseItmsID = WI.numWareHouseItemID
    WHERE  
		BI.[numOppBizDocID] = @numOppBizDocID
        AND I.[charItemType] = 'P' --Only Inventory Items
        AND I.[numDomainID] = @numDomainID
        AND ISNULL((SELECT bitDropShip FROM dbo.OpportunityItems WHERE numoppitemtCode = BI.numOppItemID AND numItemCode = I.numItemCode),0) = 0
           
	-- GET CONTAINER BY WAREHOUSE AND NOOFITEMSCANBEADDED
	SELECT
		T1.numContainer
		,T1.numWareHouseID
		,T1.numNoItemIntoContainer
		,T2.numTotalContainer
		,0 AS bitItemAdded
		,T2.fltWeight
		,T2.fltHeight
		,T2.fltWidth
		,T2.fltLength
	FROM
	(
		SELECT 
			I.numContainer
			,WI.numWareHouseID
			,I.numNoItemIntoContainer
		FROM 
			OpportunityBizDocItems OBI
		INNER JOIN 
			Item AS I
		ON 
			OBI.numItemCode = I.numItemCode
		INNER JOIN
			WareHouseItems WI
		ON
			OBI.numWarehouseItmsID = WI.numWareHouseItemID
		WHERE
			numOppBizDocID=@numOppBizDocID
			AND I.charItemType ='P'
			AND ISNULL(numContainer,0) > 0
		GROUP BY
			I.numContainer
			,WI.numWareHouseID
			,I.numNoItemIntoContainer
	) AS T1
	INNER JOIN
	(
		SELECT
			I.numItemCode
			,WI.numWareHouseID
			,SUM(OBI.numUnitHour) numTotalContainer
			,ISNULL(I.fltWeight,0) fltWeight
			,ISNULL(I.fltHeight,0) fltHeight
			,ISNULL(I.fltWidth,0) fltWidth
			,ISNULL(I.fltLength,0) fltLength
		FROM 
			OpportunityBizDocItems OBI
		INNER JOIN 
			Item AS I
		ON 
			OBI.numItemCode = I.numItemCode
		INNER JOIN
			WareHouseItems WI
		ON
			OBI.numWarehouseItmsID = WI.numWareHouseItemID
		WHERE
			numOppBizDocID=@numOppBizDocID
			AND I.charItemType ='P'
			AND ISNULL(I.bitContainer,0) = 1
		GROUP BY
			I.numItemCode
			,WI.numWareHouseID
			,ISNULL(I.fltWeight,0)
			,ISNULL(I.fltHeight,0)
			,ISNULL(I.fltWidth,0)
			,ISNULL(I.fltLength,0)
	) AS T2
	ON
		T1.numContainer = T2.numItemCode
END
