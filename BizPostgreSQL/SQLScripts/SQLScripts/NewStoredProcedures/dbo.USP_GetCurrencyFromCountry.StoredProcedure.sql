/****** Object:  StoredProcedure [dbo].[USP_GetCurrencyFromCountry]    Script Date: 07/26/2008 16:19:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created by Siva                          
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetCurrencyFromCountry')
DROP PROCEDURE USP_GetCurrencyFromCountry
GO
CREATE PROCEDURE [dbo].[USP_GetCurrencyFromCountry]
@numDomanID as numeric(9), 
@numSiteID AS NUMERIC(9),                       
@numCountryId as numeric(9)                
As                                      
Begin   

IF(Select COUNT(*) from Currency where numDomainID=@numDomanID AND numCountryId=@numCountryId AND bitEnabled=1)>0 AND @numCountryId>0
BEGIN
	Select numCurrencyID,varCurrSymbol, CASE WHEN ISNULL(fltExchangeRate,0) = 0 THEN 1 ELSE fltExchangeRate END AS [fltExchangeRate]
	from Currency 
		where numDomainID=@numDomanID AND numCountryId=@numCountryId AND bitEnabled=1
END
ELSE IF(Select COUNT(*) from Currency C JOIN Sites S ON C.numDomainID=S.numDomainID where S.numDomainID=@numDomanID AND S.numSiteID=@numSiteID AND C.numCurrencyID=S.numCurrencyID AND C.bitEnabled=1)>0
BEGIN
	Select S.numCurrencyID,varCurrSymbol, CASE WHEN ISNULL(fltExchangeRate,0) = 0 THEN 1 ELSE fltExchangeRate END AS [fltExchangeRate]
	from Currency C JOIN Sites S ON C.numDomainID=S.numDomainID
		where S.numDomainID=@numDomanID AND S.numSiteID=@numSiteID AND C.numCurrencyID=S.numCurrencyID AND C.bitEnabled=1
END
ELSE
BEGIN
	Select D.numCurrencyID,varCurrSymbol, CASE WHEN ISNULL(fltExchangeRate,0) = 0 THEN 1 ELSE fltExchangeRate END AS [fltExchangeRate]
	from Currency C JOIN Domain D ON C.numDomainID=D.numDomainID
		where D.numDomainID=@numDomanID AND C.numCurrencyID=D.numCurrencyID AND C.bitEnabled=1
END

END
GO
