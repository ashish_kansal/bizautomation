GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ManageSiteMenu')
DROP PROCEDURE USP_ManageSiteMenu
GO
CREATE PROCEDURE [dbo].[USP_ManageSiteMenu]
	@numMenuID numeric(9)=0 OUTPUT,
	@vcTitle varchar(100),
	@vcNavigationURL varchar(1000),
	@tintLevel tinyint,
	@numParentID numeric(9),
	@numSiteID numeric(9),
	@numDomainID numeric(9),
	@numPageID numeric(9),
	@bitStatus BIT
AS
IF @numPageID = 0
	SET @numPageID = NULL;
IF @numMenuID = 0 AND (NOT EXISTS(SELECT [vcTitle] FROM SiteMenu WHERE [vcTitle]= @vcTitle
																	AND [numSiteID] = @numSiteID))
BEGIN
	DECLARE @intDisplayOrder INT
	SELECT @intDisplayOrder = (ISNULL(MAX(intDisplayOrder),0) + 1) FROM [SiteMenu] WHERE numSiteID = @numSiteID

	INSERT INTO SiteMenu (
		vcTitle,
		vcNavigationURL,
		numPageID,
		tintLevel,
		numParentID,
		numSiteID,
		numDomainID,
		bitStatus,
		intDisplayOrder
	)
	VALUES (
		@vcTitle,
		@vcNavigationURL,
		@numPageID,
		@tintLevel,
		@numParentID,
		@numSiteID,
		@numDomainID,
		@bitStatus,
		@intDisplayOrder
	)
	SELECT @numMenuID= SCOPE_IDENTITY();
END
ELSE BEGIN

	UPDATE SiteMenu SET 
		vcTitle = @vcTitle,
		vcNavigationURL = @vcNavigationURL,
		numPageID = @numPageID,
		tintLevel = @tintLevel,
		numParentID = @numParentID,
		bitStatus =@bitStatus
	WHERE numMenuID = @numMenuID 
END

