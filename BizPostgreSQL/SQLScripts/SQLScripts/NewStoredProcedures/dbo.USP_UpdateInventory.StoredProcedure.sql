---Created by anoop Jayaraj

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_UpdateInventory')
DROP PROCEDURE USP_UpdateInventory
GO
CREATE PROCEDURE  USP_UpdateInventory
@numDomainID as numeric(9),
@numItemCode as NUMERIC(9),
@numOnHand as FLOAT,
@numWarehouseID as numeric(9)
as


Update W set W.numOnHand=@numOnHand ,dtModified = GETDATE() 
from WareHouseItems W join
Item I
on W.numItemID=I.numItemCode
where I.numDomainID=@numDomainID AND W.numDomainID =@numDomainID and numWareHouseID=@numWarehouseID 
and I.numItemCode=@numItemCode