GO
SET ANSI_NULLS on
GO
SET QUOTED_IDENTIFIER on
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetUserEmailTemplate_ComposeEmail')
DROP PROCEDURE USP_GetUserEmailTemplate_ComposeEmail
GO

--exec [USP_GetUserEmailTemplate_ComposeEmail] 189640,72,0,''
CREATE PROCEDURE [dbo].[USP_GetUserEmailTemplate_ComposeEmail] ( @numUserID NUMERIC(9) = 0,
                                                    @numDomainID NUMERIC(9) = 0,
                                                    @isMarketing AS BIT =0,
													@Screen As VARCHAR(20),
													@numGroupId AS NUMERIC(18)=0,
													@numCategoryId AS NUMERIC(18)=0)
AS 
DECLARE @dynamicQuery AS VARCHAR(MAX)=''
IF @isMarketing = 1 
    BEGIN
        SET @dynamicQuery='SELECT  vcDocName,
                numGenericDocID
        FROM    GenericDocuments
        WHERE   numDocCategory = 369
                --AND vcDocumentSection = ''M''
                AND numDomainId = '+CAST(@numDomainID AS VARCHAR)+' AND ISNULL(VcFileName,'''') not LIKE ''#SYS#%''	
				AND numModuleID <> 8  --Exclude BizDoc Templates'
    END
    
IF @isMarketing=0 
BEGIN
	
	IF @Screen = 'Opp/Order'		 
		SET @dynamicQuery='SELECT  vcDocName,
						numGenericDocID
				FROM    GenericDocuments
				WHERE   numDocCategory = 369
						AND tintDocumentType =1 --1 =generic,2=specific
						--AND ISNULL(vcDocumentSection,'''') <> ''M''
						AND numDomainId = '+CAST(@numDomainID AS VARCHAR)+' AND ISNULL(VcFileName,'''') not LIKE ''#SYS#%''
						AND numModuleID = 2 -- Opp/Order Templates'
	ELSE IF @Screen = 'BizDoc'
		SET @dynamicQuery='SELECT  vcDocName,
						numGenericDocID
				FROM    GenericDocuments
				WHERE   numDocCategory = 369
						AND tintDocumentType =1 
						--AND ISNULL(vcDocumentSection,'''') <> ''M''
						AND numDomainId = '+CAST(@numDomainID AS VARCHAR)+' AND ISNULL(VcFileName,'''') not LIKE ''#SYS#%''
						AND  (numModuleID = 1 OR numModuleID = 45 OR numModuleID = 2 OR numModuleID = 8) '
	ELSE
		--AND  (numModuleID = 1 OR numModuleID = 45 or numModuleID = 0 or isnull(numModuleID, 0) =0)
		SET @dynamicQuery='SELECT  vcDocName,
					numGenericDocID
			FROM    GenericDocuments
			WHERE   numDocCategory = 369
					AND tintDocumentType =1 
					--AND ISNULL(vcDocumentSection,'''') <> ''M''
					AND numDomainId = '+CAST(@numDomainID AS VARCHAR)+' AND ISNULL(VcFileName,'''') not LIKE ''#SYS#%''
					 '
		
END
IF(@numCategoryId>0)
BEGIN
	SET @dynamicQuery=@dynamicQuery+' AND numCategoryId='+CAST(@numCategoryId AS VARCHAR)+' '
END
IF(@numGroupId>0)
BEGIN
	SET @dynamicQuery=@dynamicQuery+'  AND ('+CAST(@numGroupId AS VARCHAR)+' IN (SELECT Items FROM Split(ISNULL(vcGroupsPermission,''''),'','')) OR ISNULL(vcGroupsPermission,''0'')=''0'') '
END
PRINT @dynamicQuery
EXEC(@dynamicQuery)

GO
