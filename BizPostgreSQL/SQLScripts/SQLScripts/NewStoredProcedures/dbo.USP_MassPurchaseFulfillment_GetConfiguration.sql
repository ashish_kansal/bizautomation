GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_MassPurchaseFulfillment_GetConfiguration')
DROP PROCEDURE dbo.USP_MassPurchaseFulfillment_GetConfiguration
GO

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[USP_MassPurchaseFulfillment_GetConfiguration]
(
	@numDomainID NUMERIC(18,0)
	,@numUserCntID NUMERIC(18,0)
)
AS
BEGIN
	IF EXISTS (SELECT ID FROM MassPurchaseFulfillmentConfiguration WHERE numDomainID=@numDomainID AND numUserCntID=@numUserCntID)
	BEGIN
		SELECT 
			ISNULL(bitGroupByOrderForReceive,0) bitGroupByOrderForReceive
			,ISNULL(bitGroupByOrderForPutAway,0) bitGroupByOrderForPutAway
			,ISNULL(bitGroupByOrderForBill,0) bitGroupByOrderForBill
			,ISNULL(bitGroupByOrderForClose,0) bitGroupByOrderForClose
			,ISNULL(bitReceiveBillOnClose,0) bitReceiveBillOnClose
			,ISNULL(tintScanValue,1) tintScanValue
		FROM	
			MassPurchaseFulfillmentConfiguration
		WHERE
			numDomainID=@numDomainID
			AND numUserCntID=@numUserCntID
	END
	ELSE 
	BEGIN
		SELECT 
			0 bitGroupByOrderForReceive
			,0 bitGroupByOrderForPutAway
			,0 bitGroupByOrderForBill
			,0 bitGroupByOrderForClose
			,0 bitReceiveBillOnClose
			,1 tintScanValue
	END
END
GO