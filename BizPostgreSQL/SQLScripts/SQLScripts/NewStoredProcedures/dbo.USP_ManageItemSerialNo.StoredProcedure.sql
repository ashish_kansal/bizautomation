GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ManageItemSerialNo')
DROP PROCEDURE USP_ManageItemSerialNo
GO
CREATE PROCEDURE USP_ManageItemSerialNo
    @numDomainID NUMERIC(9),
    @strFieldList TEXT,
    @numUserCntID AS NUMERIC(9)=0
AS 
BEGIN
    DECLARE @hDoc AS INT                                            
    EXEC sp_xml_preparedocument @hDoc OUTPUT, @strFieldList
                                                     
     
    SELECT 
		X.*,
		ROW_NUMBER() OVER( order by X.NewQty) AS ROWNUMBER
	INTO 
		#TempTable
	FROM 
		( 
			SELECT  
				numWareHouseItmsDTLID,
				numWareHouseItemID,
				vcSerialNo,
				NewQty,
				OldQty, 
				(CASE WHEN CAST(dExpirationDate AS DATE) = cast('1753-1-1' as date) THEN NULL ELSE dExpirationDate END) AS dExpirationDate
			FROM      
				OPENXML(@hDoc, '/NewDataSet/SerializedItems',2)
            WITH 
				( 
					numWareHouseItmsDTLID NUMERIC(9), 
					numWareHouseItemID NUMERIC(9), 
					vcSerialNo VARCHAR(100), 
					NewQty NUMERIC(9),
					OldQty NUMERIC(9),
					dExpirationDate DATETIME
				)
        ) X

	DECLARE @minROWNUMBER INT
	DECLARE @maxROWNUMBER INT
	DECLARE @Diff INT
	DECLARE @numNewQty AS INT
	DECLARE @dtExpirationDate AS DATE
	DECLARE @Diff1 INT
	DECLARE @numWareHouseItemID NUMERIC(18,0)
	DECLARE @numWarehouseItmDTLID NUMERIC(18,0)
	DECLARE @vcSerialNo AS VARCHAR(100)

	SELECT  @minROWNUMBER=MIN(ROWNUMBER), @maxROWNUMBER=MAX(ROWNUMBER) FROM #TempTable

	DECLARE @vcDescription VARCHAR(100),@numItemCode NUMERIC(18)

	WHILE  @minROWNUMBER <= @maxROWNUMBER
    BEGIN

   	    SELECT 
			@numWarehouseItmDTLID = numWarehouseItmsDTLID,
			@numWareHouseItemID=numWareHouseItemID,
			@numNewQty = NewQty,
			@vcSerialNo = vcSerialNo,
			@dtExpirationDate = dExpirationDate,
			@Diff = NewQty - OldQty
		FROM 
			#TempTable 
		WHERE 
			ROWNUMBER=@minROWNUMBER

		SET @numItemCode = 0

		SELECT 
			@numItemCode=numItemID 
		FROM 
			WareHouseItems 
		WHERE 
			numWareHouseItemID = @numWareHouseItemID 
			AND [numDomainID] = @numDomainID

		IF ISNULL((SELECT bitSerialized FROM Item WHERE numItemCode = @numItemCode),0) = 1
		BEGIN
			IF EXISTS (SELECT 
							numWareHouseItmsDTLID 
						FROM 
							WareHouseItmsDTL 
						WHERE 
							numWareHouseItemID IN (SELECT ISNULL(numWareHouseItemID,0) FROM WareHouseItems WHERE numItemID = @numItemCode)
							AND WareHouseItmsDTL.numWareHouseItmsDTLID <> @numWarehouseItmDTLID
							AND WareHouseItmsDTL.vcSerialNo = @vcSerialNo)
			BEGIN
				RAISERROR('DUPLICATE_SERIAL_NUMBER',16,1)
				RETURN
			END
		END

		UPDATE 
			WareHouseItmsDTL 
		SET  
			numQty=@numNewQty,
			vcSerialNo=@vcSerialNo,
			dExpirationDate=@dtExpirationDate 
		WHERE
			numWareHouseItmsDTLID = @numWarehouseItmDTLID
			
                
        SET @vcDescription='UPDATE Lot/Serial#'
		
   	    UPDATE
			WareHouseItems 
		SET 
			numOnHand=ISNULL(numOnHand,0) + @Diff,
			dtModified = GETDATE()  
		WHERE 
			numWareHouseItemID = @numWareHouseItemID 
			AND (numOnHand + @Diff) >= 0

		DECLARE @numDomain AS NUMERIC(18,0)
		SET @numDomain = @numDomainID

		EXEC dbo.USP_ManageWareHouseItems_Tracking
			@numWareHouseItemID = @numWareHouseItemID,
			@numReferenceID = @numItemCode,
			@tintRefType = 1,
			@vcDescription = @vcDescription,
			@numModifiedBy = @numUserCntID,
			@numDomainID = @numDomain 

        SELECT  @minROWNUMBER = MIN(ROWNUMBER) FROM  #TempTable WHERE  [ROWNUMBER] > @minROWNUMBER
    END	           
     
	DROP TABLE #TempTable
    EXEC sp_xml_removedocument @hDoc
END
                      