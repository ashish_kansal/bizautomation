/****** Object:  StoredProcedure [dbo].[USP_GetInvoiceList]    Script Date: 07/26/2008 16:17:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by prasanta Pradhan                  
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetOrderItemListInEcommerce')
DROP PROCEDURE USP_GetOrderItemListInEcommerce
GO
CREATE PROCEDURE [dbo].[USP_GetOrderItemListInEcommerce]                            
 @numDivisionID as numeric(9)=0 ,
 @numDomainID AS NUMERIC(9),
 @CurrentPage INT,
 @PageSize INT,
 @UserId NUMERIC(18,0)
as                        
 
 DECLARE @firstRec INT=0
 DECLARE @lastRec INT=0

	SET @firstRec = ( @CurrentPage - 1 ) * @PageSize
    SET @lastRec = ( @CurrentPage * @PageSize + 1 )
	SELECT  * FROM (
	SELECT ROW_NUMBER() OVER(ORDER BY numOppID) AS Rownumber,* FROM (
	select 
		OI.numOppId,
		OI.numItemCode,
		OI.vcItemName,
		O.vcPOppName,
		[dbo].[FormatedDateFromDate](O.bintCreatedDate,O.numDomainId) AS bintCreatedDate,
		OI.numUnitHour,
		(OI.monPrice* O.fltExchangeRate) AS monPrice,
		(OI.monTotAmtBefDiscount* O.fltExchangeRate) AS monTotAmount,
		O.tintOppStatus
	from 
		OpportunityItems AS OI 
	LEFT JOIN 
		OpportunityMaster AS O ON O.numOppId=OI.numOppId
	WHERE
		O.numDivisionID=@numDivisionID AND O.tintOppType= 1 
		--AND O.tintOppStatus=1 AND O.tintShipped=1 
		AND O.numContactId=@UserId
		) AS K) AS T
		WHERE T.Rownumber>@firstRec AND T.Rownumber<@lastRec

	select 
		 COUNT(OI.numItemCode)
	from 
		OpportunityItems AS OI 
	LEFT JOIN 
		OpportunityMaster AS O ON O.numOppId=OI.numOppId
	WHERE
		O.numDivisionID=@numDivisionID AND O.tintOppType= 1 
		--AND O.tintOppStatus=1 AND O.tintShipped=1 
		AND O.numContactId=@UserId






