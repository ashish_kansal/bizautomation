SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_CategoryProfile_GetWithSites')
DROP PROCEDURE dbo.USP_CategoryProfile_GetWithSites
GO
CREATE PROCEDURE [dbo].[USP_CategoryProfile_GetWithSites]
(
	@ID NUMERIC(18,0),
	@numDomainID NUMERIC(18,0)
)
AS
BEGIN
	SELECT ID,vcName FROM CategoryProfile WHERE ID=@ID

	SELECT 
		CategoryProfileSites.* ,
		Sites.vcSiteName
	FROM 
		CategoryProfileSites 
	INNER JOIN
		Sites
	ON
		CategoryProfileSites.numSiteID = Sites.numSiteID
	WHERE 
		numCategoryProfileID=@ID
END