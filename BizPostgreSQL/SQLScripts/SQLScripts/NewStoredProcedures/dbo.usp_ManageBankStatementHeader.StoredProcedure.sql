GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_ManageBankStatementHeader')
	DROP PROCEDURE usp_ManageBankStatementHeader
GO
/****** Added By : Joseph ******/
/****** Add/Update Statement Detail to Table BankStatementHeader******/

CREATE PROCEDURE [dbo].[usp_ManageBankStatementHeader]
	@numStatementID numeric(18, 0),
	@numBankDetailID numeric(18, 0),
	@vcFIStatementID varchar(50),
	@monAvailableBalance DECIMAL(20,5),
	@dtAvailableBalanceDate datetime,
	@numCurrencyID numeric(18, 0),
	@monLedgerBalance DECIMAL(20,5),
	@dtLedgerBalanceDate datetime,
	@dtStartDate datetime,
	@dtEndDate DATETIME,
	@numDomainID numeric(18, 0)
AS

SET NOCOUNT ON

IF EXISTS(SELECT [numStatementID] FROM [dbo].[BankStatementHeader] WHERE [numStatementID] = @numStatementID)
BEGIN
	UPDATE [dbo].[BankStatementHeader] SET
		[numBankDetailID] = @numBankDetailID,
		[vcFIStatementID] = @vcFIStatementID,
		[monAvailableBalance] = @monAvailableBalance,
		[dtAvailableBalanceDate] = @dtAvailableBalanceDate,
		[numCurrencyID] = @numCurrencyID,
		[monLedgerBalance] = @monLedgerBalance,
		[dtLedgerBalanceDate] = @dtLedgerBalanceDate,
		[dtStartDate] = @dtStartDate,
		[dtEndDate] = @dtEndDate,
		[numDomainID] = @numDomainID,
		[dtModifiedDate] =GETDATE()
	WHERE
		[numStatementID] = @numStatementID
END
ELSE
BEGIN
	INSERT INTO [dbo].[BankStatementHeader] (
		[numBankDetailID],
		[vcFIStatementID],
		[monAvailableBalance],
		[dtAvailableBalanceDate],
		[numCurrencyID],
		[monLedgerBalance],
		[dtLedgerBalanceDate],
		[dtStartDate],
		[dtEndDate],
		[numDomainID],
		[dtCreatedDate]
	) VALUES (
		@numBankDetailID,
		@vcFIStatementID,
		@monAvailableBalance,
		@dtAvailableBalanceDate,
		@numCurrencyID,
		@monLedgerBalance,
		@dtLedgerBalanceDate,
		@dtStartDate,
		@dtEndDate,
		@numDomainID,
		GETDATE()
	)
   SET @numStatementID = SCOPE_IDENTITY()
END

SELECT @numStatementID
GO
GO