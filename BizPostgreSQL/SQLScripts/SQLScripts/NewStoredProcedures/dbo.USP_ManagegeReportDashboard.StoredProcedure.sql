SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ManagegeReportDashboard')
DROP PROCEDURE USP_ManagegeReportDashboard
GO
CREATE PROCEDURE [dbo].[USP_ManagegeReportDashboard]
@numDomainID AS NUMERIC(18,0),
@numDashBoardID AS NUMERIC(18,0),
@numReportID AS NUMERIC(18,0),
@numUserCntID AS NUMERIC(9)=0,
@tintReportType AS TINYINT,
@tintChartType AS TINYINT,
@vcHeaderText AS VARCHAR(100)='',
@vcFooterText AS VARCHAR(100)='',
@vcXAxis AS VARCHAR(50),
@vcYAxis AS VARCHAR(50),
@vcAggType AS VARCHAR(50),
@tintReportCategory AS TINYINT,
@numDashboardTemplateID NUMERIC(18,0),
@vcTimeLine VARCHAR(50)
,@vcGroupBy VARCHAR(50)
,@vcTeritorry VARCHAR(MAX)
,@vcFilterBy TINYINT
,@vcFilterValue VARCHAR(MAX)
,@dtFromDate DATE
,@dtToDate DATE
,@numRecordCount INT
,@tintControlField INT
,@vcDealAmount VARCHAR(MAX)
,@tintOppType INT
,@lngPConclAnalysis VARCHAR(MAX)
,@bitTask VARCHAR(MAX)
,@tintTotalProgress INT
,@tintMinNumber INT
,@tintMaxNumber INT
,@tintQtyToDisplay INT
,@vcDueDate VARCHAR(MAX)
,@bitDealWon bit
,@bitGrossRevenue bit
,@bitRevenue bit
,@bitGrossProfit bit
,@bitCustomTimeline bit
,@bitBilledButNotReceived bit 
,@bitReceviedButNotBilled bit
,@bitSoldButNotShipped bit
AS
BEGIN
	IF @numDashBoardID=0 
	BEGIN
		DECLARE @tintRow AS TINYINT;SET @tintRow=0
		SELECT @tintRow = ISNULL(MAX(tintRow),0) + 1 FROM ReportDashboard WHERE numDomainID=@numDomainID AND numUserCntID=@numUserCntID AND ISNULL(numDashboardTemplateID,0)=ISNULL(@numDashboardTemplateID,0)
		
		INSERT INTO ReportDashboard 
		(
			numDomainID
			,numReportID
			,numUserCntID
			,tintReportType
			,tintChartType
			,vcHeaderText
			,vcFooterText
			,tintRow
			,tintColumn
			,vcXAxis
			,vcYAxis
			,vcAggType
			,tintReportCategory
			,intHeight
			,intWidth
			,numDashboardTemplateID
			,bitNewAdded
			,vcTimeLine
			,vcGroupBy
			,vcTeritorry
			,vcFilterBy
			,vcFilterValue
			,dtFromDate
			,dtToDate
			,numRecordCount
			,tintControlField
			,vcDealAmount
			,tintOppType
			,lngPConclAnalysis
			,bitTask
			,tintTotalProgress
			,tintMinNumber
			,tintMaxNumber
			,tintQtyToDisplay
			,vcDueDate
			,bitDealWon
			,bitGrossRevenue
			,bitRevenue
			,bitGrossProfit
			,bitCustomTimeline
			,bitBilledButNotReceived  
			,bitReceviedButNotBilled
			,bitSoldButNotShipped
		)
		VALUES
		(
			@numDomainID
			,@numReportID
			,@numUserCntID
			,@tintReportType
			,@tintChartType
			,@vcHeaderText
			,@vcFooterText
			,@tintRow
			,1
			,@vcXAxis
			,@vcYAxis
			,@vcAggType
			,@tintReportCategory
			,7
			,8
			,@numDashboardTemplateID
			,1
			,@vcTimeLine
			,@vcGroupBy
			,@vcTeritorry
			,@vcFilterBy
			,@vcFilterValue
			,@dtFromDate
			,@dtToDate
			,@numRecordCount
			,@tintControlField
			,@vcDealAmount
			,@tintOppType
			,@lngPConclAnalysis
			,@bitTask
			,@tintTotalProgress
			,@tintMinNumber
			,@tintMaxNumber
			,@tintQtyToDisplay
			,@vcDueDate
			,@bitDealWon
			,@bitGrossRevenue
			,@bitRevenue
			,@bitGrossProfit
			,@bitCustomTimeline
			,@bitBilledButNotReceived 
			,@bitReceviedButNotBilled
			,@bitSoldButNotShipped
		)
	END
	ELSE IF @numDashBoardID>0
	BEGIN
		UPDATE 
			ReportDashboard 
		SET
			numReportID=@numReportID, 
			tintReportType=@tintReportType, 
			tintChartType=@tintChartType, 
			vcHeaderText=@vcHeaderText, 
			vcFooterText=@vcFooterText,
			vcXAxis=@vcXAxis,
			vcYAxis=@vcYAxis,
			vcAggType=@vcAggType,
			tintReportCategory=@tintReportCategory
			,vcTimeLine=@vcTimeLine
			,vcGroupBy=@vcGroupBy
			,vcTeritorry=@vcTeritorry
			,vcFilterBy=@vcFilterBy
			,vcFilterValue=@vcFilterValue	
			,dtFromDate=@dtFromDate
			,dtToDate=@dtToDate
			,numRecordCount=@numRecordCount
			,tintControlField=@tintControlField
			,vcDealAmount=@vcDealAmount
			,tintOppType = @tintOppType
			,lngPConclAnalysis= @lngPConclAnalysis
			,bitTask=@bitTask
			,tintTotalProgress=@tintTotalProgress
			,tintMinNumber=@tintMinNumber
			,tintMaxNumber=@tintMaxNumber
			,tintQtyToDisplay=@tintQtyToDisplay
			,vcDueDate=@vcDueDate
			,bitDealWon =@bitDealWon
			,bitGrossRevenue = @bitGrossRevenue
			,bitRevenue = @bitRevenue
			,bitGrossProfit = @bitGrossProfit
			,bitCustomTimeline = @bitCustomTimeline
			,bitBilledButNotReceived = @bitBilledButNotReceived 
			,bitReceviedButNotBilled = @bitReceviedButNotBilled
			,bitSoldButNotShipped = @bitSoldButNotShipped
		WHERE
			numDashBoardID=@numDashBoardID 
			AND numDomainID=@numDomainID
	END
END	
GO
