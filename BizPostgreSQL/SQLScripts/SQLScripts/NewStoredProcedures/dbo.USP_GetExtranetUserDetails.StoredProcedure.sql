GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_GetExtranetUserDetails' ) 
    DROP PROCEDURE USP_GetExtranetUserDetails
GO
CREATE PROCEDURE USP_GetExtranetUserDetails
@numDomainID NUMERIC,
@numContactID NUMERIC
as
BEGIN
    SELECT  numExtranetDtlID,
            numExtranetID,
            EAD.numContactID,
            vcPassword,
            tintAccessAllowed,
            bintLastLoggedIn,
            numNoOfTimes,
            bitPartnerAccess,
            ISNULL(ACI.vcEmail,'') vcEmail,
            ISNULL(D.vcPortalName,'') vcPortalName
    FROM    dbo.ExtranetAccountsDtl
    EAD INNER JOIN dbo.AdditionalContactsInformation ACI ON EAD.numDomainID = ACI.numDomainID
	AND EAD.numContactID =ACI.numContactId
	INNER JOIN domain D ON D.numDomainID = ACI.numDomainID
    WHERE   EAD.numDomainID = @numDomainID
            AND EAD.numContactID = @numContactID
		   
END