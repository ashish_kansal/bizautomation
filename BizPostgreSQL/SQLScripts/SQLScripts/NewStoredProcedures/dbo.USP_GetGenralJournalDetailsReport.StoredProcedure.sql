-- CREATED BY MANISH ANJARA : 28th OCT, 2014
/*
EXEC USP_GetGenralJournalDetailsReport @numDomainId = 169, 
    @dtFromDate = '2014-01-01 00:00:00', @dtToDate = '2014-01-01 23:59:59',
    @CurrentPage = 1,@PageSize = 100

*/

GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_GetGenralJournalDetailsReport' )
    DROP PROCEDURE USP_GetGenralJournalDetailsReport
GO
CREATE PROCEDURE [dbo].[USP_GetGenralJournalDetailsReport]
    (
      @numDomainID NUMERIC(18, 0) ,
      @dtFromDate DATETIME ,
      @dtToDate DATETIME ,
      @CurrentPage INT = 0 ,
      @PageSize INT = 0 ,
      @tintMode AS TINYINT = 0
    )
AS
    BEGIN
	
        --SELECT TOP 10
        --        *
        --FROM    [dbo].[General_Journal_Details] AS GJD
        --ORDER BY [GJD].[numJournalId] DESC

        DECLARE @first_id NUMERIC
        DECLARE @startRow INT

        DECLARE @CurrRecord AS INT
        IF @tintMode = 0
            BEGIN

                SELECT TOP 1
                        [GJH].[numJournal_Id] ,
                        [GJD].[numTransactionId] ,
                        dbo.[FormatedDateFromDate]([GJH].[datEntry_Date],
                                                   [GJD].numDomainID) [Date] ,
                        COA.[numAccountId] ,
                        ISNULL([COA].vcAccountName, '') AS [vcAccountName] ,
                        ISNULL([GJD].[numDebitAmt], 0) [numDebitAmt] ,
                        ISNULL([GJD].[numCreditAmt], 0) [numCreditAmt]
                FROM    [dbo].[General_Journal_Header] AS GJH
                        JOIN [dbo].[General_Journal_Details] AS GJD ON [GJD].[numJournalId] = [GJH].[numJournal_Id]
                                                              AND [GJD].[numDomainId] = [GJH].[numDomainId]
                        JOIN [dbo].[Chart_Of_Accounts] AS COA ON [GJD].[numChartAcntId] = [COA].[numAccountId]
                                                              AND [GJH].[numDomainId] = [COA].[numDomainId]
                WHERE   [GJH].[numDomainId] = @numDomainID
                        AND [GJH].[datEntry_Date] BETWEEN @dtFromDate
                                                  AND     @dtToDate
						AND [GJD].[tintReferenceType] = 5
                ORDER BY [GJD].numEntryDateSortOrder1 ASC
            END
        ELSE
            IF @tintMode = 1
                BEGIN
                    SET @CurrRecord = ( ( @CurrentPage - 1 ) * @PageSize ) + 1
                    PRINT @CurrRecord
                    SET ROWCOUNT @CurrRecord

                    SELECT  @first_id = [GJD].numEntryDateSortOrder1
                    FROM    [dbo].[General_Journal_Header] AS GJH
                            JOIN [dbo].[General_Journal_Details] AS GJD ON [GJD].[numJournalId] = [GJH].[numJournal_Id]
                                                              AND [GJD].[numDomainId] = [GJH].[numDomainId]
                            JOIN [dbo].[Chart_Of_Accounts] AS COA ON [GJD].[numChartAcntId] = [COA].[numAccountId]
                                                              AND [GJH].[numDomainId] = [COA].[numDomainId]
                    WHERE   [GJH].[numDomainId] = @numDomainID
                            AND [GJH].[datEntry_Date] BETWEEN @dtFromDate
                                                      AND     @dtToDate
							AND [GJD].[tintReferenceType] = 5
                    ORDER BY [GJD].numEntryDateSortOrder1 ASC

                    PRINT @first_id
                    PRINT ROWCOUNT_BIG()
                    SET ROWCOUNT @PageSize

                    SELECT  [GJD].[numJournalId] ,
                            [GJD].[numTransactionId] ,
                            dbo.[FormatedDateFromDate]([GJH].[datEntry_Date],
                                                       [GJD].numDomainID) [Date] ,
                            COA.[numAccountId] ,
                            ISNULL([COA].vcAccountName, '') AS [vcAccountName] ,
                            ISNULL([GJD].[numDebitAmt], 0) [numDebitAmt] ,
                            ISNULL([GJD].[numCreditAmt], 0) [numCreditAmt]
                    FROM    [dbo].[General_Journal_Header] AS GJH
                            JOIN [dbo].[General_Journal_Details] AS GJD ON [GJD].[numJournalId] = [GJH].[numJournal_Id]
                                                              AND [GJD].[numDomainId] = [GJH].[numDomainId]
                            JOIN [dbo].[Chart_Of_Accounts] AS COA ON [GJD].[numChartAcntId] = [COA].[numAccountId]
                                                              AND [GJH].[numDomainId] = [COA].[numDomainId]
                    WHERE   [GJH].[numDomainId] = @numDomainID
                            AND [GJH].[datEntry_Date] BETWEEN @dtFromDate
                                                      AND     @dtToDate
							AND [GJD].[tintReferenceType] = 5
                            AND [GJD].numEntryDateSortOrder1 >= @first_id
                    ORDER BY [GJH].[datEntry_Date],[GJD].[numJournalId] ASC
                
                    SET ROWCOUNT 0

                END
            ELSE
                IF @tintMode = 3
                    BEGIN
                        SELECT  dbo.[FormatedDateFromDate]([GJH].[datEntry_Date],
                                                           [GJD].numDomainID) [Date] ,
                                COA.[numAccountId] AS [AccountID],
                                CASE WHEN ISNULL([GJD].[numDebitAmt], 0) = 0 THEN ISNULL([COA].vcAccountName, '') ELSE '' END AS [Reference] ,
								CASE WHEN ISNULL([GJD].[numCreditAmt], 0) = 0 THEN ISNULL([COA].vcAccountName, '')  ELSE '' END AS [TransDesc] ,
                                ISNULL([GJD].[numDebitAmt], 0) [Debit] ,
                                ISNULL([GJD].[numCreditAmt], 0) [Credit]
                        FROM    [dbo].[General_Journal_Header] AS GJH
                                JOIN [dbo].[General_Journal_Details] AS GJD ON [GJD].[numJournalId] = [GJH].[numJournal_Id]
                                                              AND [GJD].[numDomainId] = [GJH].[numDomainId]
                                JOIN [dbo].[Chart_Of_Accounts] AS COA ON [GJD].[numChartAcntId] = [COA].[numAccountId]
                                                              AND [GJH].[numDomainId] = [COA].[numDomainId]
                        WHERE   [GJH].[numDomainId] = @numDomainID
                                AND [GJH].[datEntry_Date] BETWEEN @dtFromDate
                                                          AND @dtToDate
								AND [GJD].[tintReferenceType] = 5
                        ORDER BY [GJD].numEntryDateSortOrder1 ASC
                    END
    END
