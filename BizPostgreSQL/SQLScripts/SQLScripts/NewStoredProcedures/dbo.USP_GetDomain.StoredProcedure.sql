GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetDomain')
DROP PROCEDURE USP_GetDomain
GO
CREATE PROCEDURE [dbo].[USP_GetDomain]
    (
      @numDomainID INT,
      @numType INT,
      @vcDomainCode VARCHAR(50) = '',
      @numSubscriberID INT
    )
AS 
    BEGIN
        IF @numType = 1 
            BEGIN
                SELECT  numDomainID,
                        vcDomainName,
                        numParentDomainID,
                        vcDomainCode
                FROM    Domain
                WHERE   numDomainID = @numDomainID
                        AND numSubscriberID = @numSubscriberID ;
            END
        ELSE 
            IF @numType = 2 
                BEGIN
                    SELECT  DN.numDomainID,
                            DN.vcDomainName,
                            DN.numParentDomainID,
                            DN.vcDomainCode,
                            DP.vcDomainName AS ParentDomainName
                    FROM    Domain DN INNER JOIN Domain DP ON DP.numDomainID = DN.numParentDomainID
                    WHERE   
                            DN.vcDomainCode LIKE @vcDomainCode + '%'
                            AND DN.numSubscriberID = @numSubscriberID
                            AND DP.numSubscriberID = @numSubscriberID
                    ORDER BY DN.vcDomainCode ;
                END
		 ELSE 
            IF @numType = 3 
                BEGIN
                    SELECT  DN.numDomainID,
                            DN.vcDomainName,
                            DN.numParentDomainID,
                            DN.vcDomainCode,
                           '' AS ParentDomainName
                    FROM    Domain DN 
                END
    END