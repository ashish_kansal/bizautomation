GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_EmailHistory_GetByIDsString' ) 
    DROP PROCEDURE USP_EmailHistory_GetByIDsString
GO
CREATE PROCEDURE USP_EmailHistory_GetByIDsString
    @vcEmailHstrID AS VARCHAR(MAX),
	@numDomainID AS NUMERIC(18,0),
	@numUserCntID AS NUMERIC(18,0)
AS 
BEGIN
	DECLARE @strSQL AS VARCHAR(MAX)

	SET @strSQL = 'SELECT numEmailHstrID,numUid,vcNodeName,chrSource FROM EmailHistory LEFT JOIN InboxTreeSort ON EmailHistory.numNodeID = InboxTreeSort.numNodeID WHERE 
	EmailHistory.numDomainID=' + CAST(@numDomainID AS VARCHAR(9)) + ' AND EmailHistory.numUserCntId=' + CAST(@numUserCntID AS VARCHAR(9)) + ' AND 
	EmailHistory.numEmailHstrID IN (' + @vcEmailHstrID + ')'
	
	EXEC(@strSQL)
END



