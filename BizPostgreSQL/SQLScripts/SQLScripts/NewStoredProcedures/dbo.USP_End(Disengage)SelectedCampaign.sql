SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_End(Disengage)SelectedCampaign')
DROP PROCEDURE USP_End(Disengage)SelectedCampaign
GO
-- =============================================
-- Author:		<Priya Sharma>
-- Create date: <06/01/2018>
-- Description:	<Disengage selected Camapigns>
-- =============================================
CREATE PROCEDURE [dbo].[USP_End(Disengage)SelectedCampaign]
	@numContactID NUMERIC(9),
	@numECampaignID NUMERIC(9)
AS
  BEGIN
    DECLARE  @numConEmailCampID NUMERIC(9)
    
    SELECT @numConEmailCampID = [numConEmailCampID]
    FROM   [ConECampaign]
    WHERE  [numContactID] = @numContactID
           AND [numECampaignID] = @numECampaignID
        
	UPDATE [ConECampaign] SET [bitEngaged] = 0 WHERE [numConEmailCampID]=@numConEmailCampID
	UPDATE AdditionalContactsInformation SET numECampaignID = NULL WHERE numContactId = @numContactID

END
GO


