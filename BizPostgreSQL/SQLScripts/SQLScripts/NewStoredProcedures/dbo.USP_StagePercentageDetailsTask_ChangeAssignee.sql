GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_StagePercentageDetailsTask_ChangeAssignee')
DROP PROCEDURE dbo.USP_StagePercentageDetailsTask_ChangeAssignee
GO
CREATE PROCEDURE [dbo].[USP_StagePercentageDetailsTask_ChangeAssignee]
(
	@numDomainID NUMERIC(18,0)
	,@numTaskID NUMERIC(18,0)
	,@numAssignedTo NUMERIC(18,0)
)
AS 
BEGIN
	IF EXISTS (SELECT ID FROM StagePercentageDetailsTaskTimeLog WHERE numDomainID=@numDomainID AND numTaskID = @numTaskID AND tintAction=4)
	BEGIN
		RAISERROR('TASK_IS_MARKED_AS_FINISHED',16,1)
	END
	ELSE IF ISNULL((SELECT TOP 1 tintAction FROM StagePercentageDetailsTaskTimeLog WHERE numDomainID=@numDomainID AND numTaskID = @numTaskID ORDER BY tintAction),0) IN (1,3)
	BEGIN
		RAISERROR('TASK_IS_ALREADY_STARTED',16,1)
	END
	ELSE
	BEGIN
		UPDATE StagePercentageDetailsTask SET numAssignTo=@numAssignedTo WHERE numDomainID=@numDomainID AND numTaskId=@numTaskID
	END	
END
GO