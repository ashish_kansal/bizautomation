GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

GO
IF EXISTS (SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_TimeAndExpense_SaveUserClockInOut')
DROP PROCEDURE USP_TimeAndExpense_SaveUserClockInOut
GO

CREATE PROCEDURE USP_TimeAndExpense_SaveUserClockInOut
(
	@numDomainID NUMERIC(18,0)
	,@numUserCntID NUMERIC(18,0)
	,@numApprovalComplete INT
)
AS
BEGIN
	DECLARE @dtFromDate DATETIME = GETUTCDATE()

	EXEC dbo.USP_ValidateFinancialYearClosingDate @numDomainID,@dtFromDate

	IF EXISTS (SELECT numCategoryHDRID FROM TimeAndExpense WHERE numUserCntID=@numUserCntID AND numCategory=1 AND numType=7 AND dtToDate IS NULL)
	BEGIN
		DECLARE @monHourlyRate DECIMAL(20,5)

		SELECT 
			@monHourlyRate = ISNULL(monHourlyRate,0) 
		FROM 
			UserMaster 
		WHERE 
			numUserDetailId=@numUserCntID
		
		UPDATE 
			TimeAndExpense 
		SET 
			dtToDate = GETUTCDATE()
			,monAmount=@monHourlyRate 
		WHERE 
			numUserCntID=@numUserCntID 
			AND numCategory=1 
			AND numType=7 
			AND dtToDate IS NULL

		SELECT 0
	END
	ELSE
	BEGIN
		INSERT INTO TimeAndExpense
		(
			tintTEType,
			numCategory,
			dtFromDate,
			dtToDate,
			bitFromFullDay,
			bitToFullDay,
			monAmount,
			numOppId,
			numDivisionID,
			txtDesc,
			numType,
			numUserCntID,
			numDomainID,
			numCaseId,
			numProId,
			numContractId,
			numOppBizDocsId,
			numtCreatedBy,
			numtModifiedBy,
			dtTCreatedOn,
			dtTModifiedOn,
			numStageId,
			bitReimburse,
			numOppItemID,
			numExpId,
			numApprovalComplete,
			numServiceItemID,
			numClassID    
		)
		VALUES  
		(
			0,
			1,
			GETUTCDATE(),
			NULL,
			0,
			0,
			0,
			0,
			0,
			'',
			7,
			@numUserCntID,
			@numDomainID,
			0,
			0,
			0,
			0,
			@numUserCntID,
			@numUserCntID,
			GETUTCDATE(),
			GETUTCDATE(),
			0,
			0,
			0,
			0,
			@numApprovalComplete,
			0,
			0         
		)

		SELECT 1
	END
END
GO