
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetConnectedCOABankAccounts')
	DROP PROCEDURE USP_GetConnectedCOABankAccounts
GO

/****** Added By : Joseph ******/
/****** Gets All Chart Of Account Detail connected to Bank ******/


CREATE PROCEDURE [dbo].[USP_GetConnectedCOABankAccounts]
	@tintMode numeric(18, 0) = 0,
	@numDomainID numeric(18, 0),
	@numUserContactID numeric(18, 0)
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

IF @tintMode = 0
	BEGIN
		SELECT COA.numAccountId,COA.numBankDetailID,COA.vcAccountName,COA.numOpeningBal,dbo.FormatedDateTimeFromDate( COA.dtOpeningDate,@numDomainID) AS dtOpeningDate,
CASE dbo.fn_GetDateTimeDifference(GETDATE(),ISNULL((SELECT MAX(dtCreatedDate) 
	 FROM dbo.BankStatementHeader WHERE numBankDetailID=BD.numBankDetailID),GETDATE()))
	 WHEN '' THEN ''
	 ELSE (dbo.fn_GetDateTimeDifference(GETDATE(),ISNULL((SELECT MAX(dtCreatedDate) 
	 FROM dbo.BankStatementHeader WHERE numBankDetailID=BD.numBankDetailID),GETDATE())) + ' ago')
	 END   AS LastUpdated, BM.vcFIName,BSH.dtCreatedDate
		FROM dbo.Chart_Of_Accounts COA
		INNER JOIN dbo.BankDetails BD ON  COA.numBankDetailID = BD.numBankDetailID
		INNER JOIN BankMaster BM ON BM.numBankMasterID = BD.numBankMasterID
		LEFT JOIN dbo.BankStatementHeader BSH ON BD.numBankDetailID = BSH.numBankDetailID

		WHERE COA.numDomainId = @numDomainID 
		AND COA.numBankDetailID IS NOT NULL 
		AND COA.numBankDetailID <> 0
		AND COA.IsConnected = 1
		AND COA.bitActive = 1
		AND (BSH.dtCreatedDate IS NULL OR BSH.dtCreatedDate =
		(SELECT MAX(dtCreatedDate) FROM dbo.BankStatementHeader WHERE numBankDetailID=BD.numBankDetailID))

	END
	ELSE IF @tintMode = 1
	BEGIN
		SELECT COA.numAccountId,COA.numBankDetailID,COA.vcAccountName,COA.numOpeningBal,dbo.FormatedDateTimeFromDate( COA.dtOpeningDate,@numDomainID) AS dtOpeningDate, BM.vcFIName,BSH.dtCreatedDate
		FROM dbo.Chart_Of_Accounts COA
		INNER JOIN dbo.BankDetails BD ON  COA.numBankDetailID = BD.numBankDetailID
		INNER JOIN BankMaster BM ON BM.numBankMasterID = BD.numBankMasterID
		LEFT JOIN dbo.BankStatementHeader BSH ON BD.numBankDetailID = BSH.numBankDetailID

		WHERE COA.numDomainId = @numDomainID 
		AND COA.numBankDetailID IS NOT NULL 
		AND BD.vcAccountType <> '4'
		AND COA.numBankDetailID <> 0
		AND COA.IsConnected = 1
		AND COA.bitActive = 1
		AND (BSH.dtCreatedDate IS NULL OR BSH.dtCreatedDate =
		(SELECT MAX(dtCreatedDate) FROM dbo.BankStatementHeader WHERE numBankDetailID=BD.numBankDetailID))
	END
GO