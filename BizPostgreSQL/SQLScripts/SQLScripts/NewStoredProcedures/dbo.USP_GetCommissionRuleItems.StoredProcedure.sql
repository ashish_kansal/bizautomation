/****** Object:  StoredProcedure [dbo].[USP_GetCommissionRuleItems]    Script Date: 07/26/2008 16:17:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj    
-- USP_GetCommissionRuleItems 366,72,1
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetCommissionRuleItems')
DROP PROCEDURE USP_GetCommissionRuleItems
GO
CREATE PROCEDURE [dbo].[USP_GetCommissionRuleItems]    
@numRuleID as numeric(9)=0,    
@numDomainID as numeric(9)=0,    
@tintRuleAppType as tinyint    
as   
  
     
  
if @tintRuleAppType=1    
begin   

 select [numComRuleItemID],[numComRuleID], vcItemName,[txtItemDesc],[vcModelID], dbo.[GetListIemName]([numItemClassification]) ItemClassification from item   
 join [CommissionRuleItems]  
 on numValue=numItemCode  
 where numDomainID=@numDomainID and numComRuleID=@numRuleID and tintType=1
    
end    
if @tintRuleAppType=2    
begin    

  SELECT [numListItemID] numItemClassification,ISNULL([vcData],'-') ItemClassification  FROM [ListDetails] WHERE [numListID]=36 AND [numDomainID]=@numDomainID AND [numListItemID] NOT IN (select numValue from [CommissionRuleItems] where tintType=2 and numComRuleID=@numRuleID)
  
 select [numComRuleItemID],[numComRuleID],[numValue],dbo.[GetListIemName]([numValue]) ItemClassification,(SELECT COUNT(*) FROM item I1 WHERE I1.numItemClassification=[numValue]) AS ItemsCount from 
 [CommissionRuleItems]   
 where numComRuleID=@numRuleID and tintType=2
 
end
IF @tintRuleAppType=3
BEGIN
select numComRuleOrgID,[numComRuleID],vcCompanyName,dbo.[GetListIemName](dbo.CompanyInfo.numCompanyType) +',' + dbo.[GetListIemName](dbo.CompanyInfo.vcProfile) Relationship,
 (SELECT ISNULL(vcFirstName,'') + ' ' + ISNULL(vcLastName,'') + ', ' + ISNULL(vcEmail,'')  FROM [AdditionalContactsInformation] A WHERE A.[numDivisionID]=[DivisionMaster].[numDivisionID] AND ISNULL(A.bitPrimaryContact,0)=1) AS PrimaryContact  from DivisionMaster  
 join Companyinfo on DivisionMaster.numCompanyID=Companyinfo.numCompanyID  
 JOIN CommissionRuleOrganization ON numValue=DivisionMaster.numDivisionID  
 WHERE DivisionMaster.numDomainID=@numDomainID and numComRuleID=@numRuleID
END
IF @tintRuleAppType=4
BEGIN
 select numComRuleOrgID,numComRuleID,L1.vcData+' - '+L2.vcData as RelProfile from CommissionRuleOrganization DTL
 Join ListDetails L1  
 on L1.numListItemID=DTL.numValue  
 Join ListDetails L2  
 on L2.numListItemID=DTL.numProfile  
 where numComRuleID=@numRuleID
END  
GO
