GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_ManageWorkFlowQueue' ) 
    DROP PROCEDURE USP_ManageWorkFlowQueue
GO
CREATE PROCEDURE USP_ManageWorkFlowQueue
    @numWFQueueID numeric(18, 0)=0,
    @numDomainID numeric(18, 0)=0,
    @numUserCntID numeric(18, 0)=0,
    @numRecordID numeric(18, 0)=0,
    @numFormID numeric(18, 0)=0,
    @tintProcessStatus TINYINT=1,
    @tintWFTriggerOn TINYINT,
    @tintMode TINYINT,
    @vcColumnsUpdated VARCHAR(1000)='',
    @numWFID NUMERIC(18,0)=0,
    @bitSuccess BIT=0,
    @vcDescription VARCHAR(1000)=''
    
AS 
BEGIN
	IF @tintMode=1
	BEGIN
		IF @numWFQueueID>0
		   BEGIN
	  		 UPDATE [dbo].[WorkFlowQueue] SET [tintProcessStatus] = @tintProcessStatus WHERE  [numWFQueueID] = @numWFQueueID AND [numDomainID] = @numDomainID
		   END
		ELSE
		   BEGIN
	 		 IF EXISTS(SELECT 1 FROM dbo.WorkFlowMaster WHERE numDomainID=@numDomainID AND numFormID=@numFormID AND (tintWFTriggerOn=@tintWFTriggerOn OR (@tintWFTriggerOn IN (1,2) AND tintWFTriggerOn=3)) AND bitActive=1)
			 BEGIN
					INSERT INTO [dbo].[WorkFlowQueue] ([numDomainID], [numCreatedBy], [dtCreatedDate], [numRecordID], [numFormID], [tintProcessStatus], [tintWFTriggerOn], [numWFID])
					SELECT @numDomainID, @numUserCntID, GETUTCDATE(), @numRecordID, @numFormID, @tintProcessStatus, tintWFTriggerOn, numWFID
					FROM dbo.WorkFlowMaster WHERE numDomainID=@numDomainID AND numFormID=@numFormID AND (tintWFTriggerOn=@tintWFTriggerOn OR (@tintWFTriggerOn IN (1,2) AND tintWFTriggerOn=3)) AND bitActive=1
					AND numWFID NOT IN (SELECT numWFID FROM WorkFlowQueue WHERE numDomainID=@numDomainID AND numRecordID=@numRecordID AND numFormID=@numFormID AND (tintWFTriggerOn=@tintWFTriggerOn OR (@tintWFTriggerOn IN (1,2) AND tintWFTriggerOn=3)) AND tintProcessStatus IN(1,2))
					
					IF @tintWFTriggerOn=2 AND LEN(@vcColumnsUpdated)>0
					BEGIN
						SET @tintWFTriggerOn=4
						
						INSERT INTO [dbo].[WorkFlowQueue] ([numDomainID], [numCreatedBy], [dtCreatedDate], [numRecordID], [numFormID], [tintProcessStatus], [tintWFTriggerOn], [numWFID])
						SELECT DISTINCT @numDomainID, @numUserCntID, GETUTCDATE(), @numRecordID, @numFormID, @tintProcessStatus, @tintWFTriggerOn, WF.numWFID
						FROM dbo.WorkFlowMaster WF JOIN WorkFlowConditionList WFCL ON WF.numWFID=WFCL.numWFID
						JOIN dbo.DycFieldMaster DFM ON WFCL.numFieldID=DFM.numFieldId 
						WHERE WF.numDomainID=@numDomainID AND WF.numFormID=@numFormID AND WF.tintWFTriggerOn=4 AND WF.bitActive=1 AND WFCL.bitCustom=0
						AND DFM.vcOrigDbColumnName IN (SELECT OutParam FROM dbo.SplitString(@vcColumnsUpdated,','))					
						AND WF.numWFID NOT IN (SELECT numWFID FROM WorkFlowQueue WHERE numDomainID=@numDomainID AND numRecordID=@numRecordID AND numFormID=@numFormID AND tintWFTriggerOn=4 AND tintProcessStatus IN(1,2))
					END
			 END
			 --Added By:Sachin Sadhu||Date:10thFeb2014
			 --Description:@tintWFTriggerOn value is  coming from Trigger,I=1,U=2,D=3
			 --case of Field(s) Update
			 IF EXISTS(SELECT 1 FROM dbo.WorkFlowMaster WHERE numDomainID=@numDomainID AND numFormID=@numFormID AND (tintWFTriggerOn=@tintWFTriggerOn OR (@tintWFTriggerOn IN (1,2) AND tintWFTriggerOn=4)) AND bitActive=1)
			 BEGIN
					INSERT INTO [dbo].[WorkFlowQueue] ([numDomainID], [numCreatedBy], [dtCreatedDate], [numRecordID], [numFormID], [tintProcessStatus], [tintWFTriggerOn], [numWFID])
					SELECT @numDomainID, @numUserCntID, GETUTCDATE(), @numRecordID, @numFormID, @tintProcessStatus, tintWFTriggerOn, numWFID
					FROM dbo.WorkFlowMaster WHERE numDomainID=@numDomainID AND numFormID=@numFormID AND (tintWFTriggerOn=@tintWFTriggerOn OR (@tintWFTriggerOn IN (1,2) AND tintWFTriggerOn=3)) AND bitActive=1
					AND numWFID NOT IN (SELECT numWFID FROM WorkFlowQueue WHERE numDomainID=@numDomainID AND numRecordID=@numRecordID AND numFormID=@numFormID AND (tintWFTriggerOn=@tintWFTriggerOn OR (@tintWFTriggerOn IN (1,2) AND tintWFTriggerOn=3)) AND tintProcessStatus IN(1,2))
					
					IF @tintWFTriggerOn=2 AND LEN(@vcColumnsUpdated)>0
					BEGIN
						SET @tintWFTriggerOn=4
						
						INSERT INTO [dbo].[WorkFlowQueue] ([numDomainID], [numCreatedBy], [dtCreatedDate], [numRecordID], [numFormID], [tintProcessStatus], [tintWFTriggerOn], [numWFID])
						SELECT DISTINCT @numDomainID, @numUserCntID, GETUTCDATE(), @numRecordID, @numFormID, @tintProcessStatus, @tintWFTriggerOn, WF.numWFID
						FROM dbo.WorkFlowMaster WF JOIN WorkFlowConditionList WFCL ON WF.numWFID=WFCL.numWFID
						JOIN dbo.DycFieldMaster DFM ON WFCL.numFieldID=DFM.numFieldId 
						WHERE WF.numDomainID=@numDomainID AND WF.numFormID=@numFormID AND WF.tintWFTriggerOn=4 AND WF.bitActive=1 AND WFCL.bitCustom=0
						AND DFM.vcOrigDbColumnName IN (SELECT OutParam FROM dbo.SplitString(@vcColumnsUpdated,','))					
						AND WF.numWFID NOT IN (SELECT numWFID FROM WorkFlowQueue WHERE numDomainID=@numDomainID AND numRecordID=@numRecordID AND numFormID=@numFormID AND tintWFTriggerOn=4 AND tintProcessStatus IN(1,2))
					END
			 END
			 --End of Script:Sachin
		  END
	 END
	  ELSE IF @tintMode = 2
        BEGIN
			INSERT INTO [dbo].[WorkFlowQueueExecution] ([numWFQueueID], [numWFID], [dtExecutionDate], [bitSuccess], [vcDescription])
			SELECT @numWFQueueID, @numWFID, GETUTCDATE(), @bitSuccess, @vcDescription


			IF ISNULL(@bitSuccess,0) = 0
			BEGIN
				UPDATE WorkFlowQueue SET tintProcessStatus=6 WHERE numWFQueueID=@numWFQueueID
			END
        END    
END



