SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetReportDashBoard')
DROP PROCEDURE USP_GetReportDashBoard
GO
CREATE PROCEDURE [dbo].[USP_GetReportDashBoard]     
	@numDomainID AS NUMERIC(18,0),           
	@numUserCntID AS NUMERIC(18,0),
	@numDashboardTemplateID AS NUMERIC(18,0) --IF VALUE IS -1 THEN FETCH DEFAULT TEMPLATE SET FOR THAT USER           
AS            
BEGIN            
	IF ISNULL(@numDashboardTemplateID,0) = -1
	BEGIN
		SET @numDashboardTemplateID = ISNULL((SELECT numDashboardTemplateID FROM UserMaster WHERE numDomainID=@numDomainID AND numUserDetailId=@numUserCntID),0)
	END

	IF NOT EXISTS(select 1 from ReportDashBoardSize WHERE numDomainID=@numDomainID AND numUserCntID=@numUserCntID)            
	BEGIN            
		INSERT INTO ReportDashBoardSize
		(
			numDomainID,numUserCntID,tintColumn,tintSize
		)            
		SELECT @numDomainID,@numUserCntID,1,1 
		UNION ALL
		SELECT @numDomainID,@numUserCntID,2,1 
		UNION ALL
		SELECT @numDomainID,@numUserCntID,3,1
	END     

	SELECT 
		tintColumn
		,tintSize 
	FROM 
		ReportDashBoardSize 
	WHERE 
		numDomainID=@numDomainID 
		AND numUserCntID=@numUserCntID 
	ORDER BY 
		tintColumn            

	SELECT 
		RD.numDashBoardID
		,RD.numReportID
		,RD.tintColumn
		,RD.tintRow
		,RD.tintReportType
		,RD.tintChartType
		,RD.tintReportCategory
		,RD.intHeight
		,RD.intWidth
		,ISNULL(RLM.bitDefault,0) bitDefault
		,ISNULL(RLM.intDefaultReportID,0) intDefaultReportID
	FROM 
		ReportDashboard RD 
	LEFT JOIN
		DashboardTemplate DT
	ON
		RD.numDashboardTemplateID=DT.numTemplateID
	LEFT JOIN
		ReportListMaster RLM
	ON 
		RD.numReportID=RLM.numReportID
	WHERE 
		RD.numDomainID=@numDomainID 
		AND RD.numUserCntID=@numUserCntID 
		AND ISNULL(RD.numDashboardTemplateID,0)=ISNULL(@numDashboardTemplateID,0)
	ORDER BY 
		tintColumn,tintRow     
		
	SELECT
		numTemplateID
		,vcTemplateName
	FROM
		DashboardTemplate
	WHERE
		numDomainID=@numDomainID
		AND numTemplateID=@numDashboardTemplateID       
END
GO
