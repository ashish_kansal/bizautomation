GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ManageCorrespondence')
DROP PROCEDURE USP_ManageCorrespondence
GO
CREATE PROCEDURE [dbo].[USP_ManageCorrespondence]
    @numCorrespondenceID NUMERIC = 0 OUTPUT,
    @numCommID NUMERIC = 0,
    @numEmailHistoryID NUMERIC = 0,
    @tintCorrType TINYINT,
    @numDomainID NUMERIC,
    @numOpenRecordID NUMERIC,
	@monMRItemAmount DECIMAL(20,5)=0
AS 
    SET NOCOUNT ON

    IF @numCommID = 0 
        SET @numCommID = NULL
    IF @numEmailHistoryID = 0 
        SET @numEmailHistoryID = NULL
    IF @numCorrespondenceID = 0 
        BEGIN
	
            INSERT  INTO Correspondence
                    (
                      [numCommID],
                      [numEmailHistoryID],
                      [tintCorrType],
                      [numDomainID],
					  [numOpenRecordID],
					  [monMRItemAmount]
	              )
            VALUES  (
                      @numCommID,
                      @numEmailHistoryID,
                      @tintCorrType,
                      @numDomainID,
					  @numOpenRecordID,
					  @monMRItemAmount
	              )

            SELECT  @numCorrespondenceID = SCOPE_IDENTITY()
        END
    ELSE 
        BEGIN
            UPDATE  Correspondence
            SET     [numCommID] = @numCommID,
                    [numEmailHistoryID] = @numEmailHistoryID,
                    [tintCorrType] = @tintCorrType,
                    [numOpenRecordID] = @numOpenRecordID,
					[monMRItemAmount] = @monMRItemAmount
            WHERE   [numCorrespondenceID] = @numCorrespondenceID

        END		UPDATE OpportunityBizDocs SET [bitIsEmailSent] = 1 WHERE numOppId = @numOpenRecordID