SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_Contacts_CT')
DROP PROCEDURE USP_Contacts_CT
GO
-- =============================================
-- Author:		<Author,,Sachin Sadhu>
-- Create date: <Create Date,,23rdJuly2014>
-- Description:	<Description,, to Track Row changes in Contacts Table>
-- =============================================
Create PROCEDURE [dbo].[USP_Contacts_CT]
	-- Add the parameters for the stored procedure here
-- Add the parameters for the stored procedure here
    @numDomainID numeric(18, 0)=0,
    @numUserCntID numeric(18, 0)=0,
    @numRecordID numeric(18, 0)=0 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	 -- Insert statements for procedure here
	DECLARE @tblFields TABLE 
	(
		FieldName VARCHAR(200),
		FieldValue VARCHAR(200)
	)

	DECLARE @Type char(1)
	DECLARE @ChangeVersion bigint
	DECLARE @CreationVersion bigint
	DECLARE @last_synchronization_version bigInt=null
	DECLARE @Columns_Updated VARCHAR(1000)

	SELECT 			
		@ChangeVersion=Ct.Sys_Change_version
		,@CreationVersion=Ct.Sys_Change_Creation_version
	FROM 
		CHANGETABLE(CHANGES dbo.AdditionalContactsInformation, 0) AS CT
	WHERE 
		numContactId=@numRecordID


	IF (@ChangeVersion=@CreationVersion)--Insert
	BEGIN
		SELECT 
			@Type = CT.SYS_CHANGE_OPERATION
		FROM 
			CHANGETABLE(CHANGES dbo.AdditionalContactsInformation, 0) AS CT
		WHERE  
			CT.numContactId=@numRecordID
	END
	ELSE
	BEGIN
		SET @last_synchronization_version=@ChangeVersion-1     
		
		SELECT 
			@Type = CT.SYS_CHANGE_OPERATION
		FROM 
			CHANGETABLE(CHANGES dbo.AdditionalContactsInformation, @last_synchronization_version) AS CT
		WHERE  
			CT.numContactId=@numRecordID
	
		--Fields Up date & for Type :Insert or Update:
		Declare @UFFields as table 
		(
			Address varchar(70),
			numAge varchar(70),
			vcAsstEmail varchar(70),
			vcAsstFirstName varchar(70),
			vcAsstLastName varchar(70),
			numAsstPhone varchar(70),
			vcAltEmail varchar(70),
			vcCategory varchar(70),
			numCell varchar(70),
			vcEmail varchar(70),
			numPhone varchar(70),
			vcPosition varchar(70),
			bitPrimaryContact varchar(70),
			bitOptOut varchar(70),
			numManagerID varchar(70),
			vcFirstName varchar(70),
			charSex varchar(70),
			vcLastName varchar(70),
			bintDOB varchar(70),
			numContactType varchar(70),
			numTeam varchar(70),
			txtNotes varchar(70),
			bintCreatedDate varchar(70)
		)

		INSERT into @UFFields(Address,numAge,vcAsstEmail,vcAsstFirstName,vcAsstLastName,numAsstPhone,vcAltEmail,vcCategory,numCell,vcEmail,numPhone,vcPosition,bitPrimaryContact,bitOptOut,numManagerID,vcFirstName,charSex,vcLastName,bintDOB,numContactType,numTeam,txtNotes,bintCreatedDate)
		SELECT
		CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.AdditionalContactsInformation'), 'Address', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS Address ,
		CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.AdditionalContactsInformation'), 'numAge', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS numAge, 
		CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.AdditionalContactsInformation'), 'vcAsstEmail', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS vcAsstEmail, 
		CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.AdditionalContactsInformation'), 'vcAsstFirstName', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS vcAsstFirstName, 
		CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.AdditionalContactsInformation'), 'vcAsstLastName', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS vcAsstLastName	,
		CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.AdditionalContactsInformation'), 'numAsstPhone', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS  numAsstPhone ,
		CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.AdditionalContactsInformation'), 'vcAltEmail', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS vcAltEmail, 
        CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.AdditionalContactsInformation'), 'vcCategory', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS vcCategory,
        CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.AdditionalContactsInformation'), 'numCell', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS numCell,
		CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.AdditionalContactsInformation'), 'vcEmail', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS vcEmail,
		CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.AdditionalContactsInformation'), 'numPhone', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS numPhone,
		CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.AdditionalContactsInformation'), 'vcPosition', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS vcPosition,
		CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.AdditionalContactsInformation'), 'bitPrimaryContact', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS bitPrimaryContact,
		CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.AdditionalContactsInformation'), 'bitOptOut', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS bitOptOut,
		CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.AdditionalContactsInformation'), 'numManagerID', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS numManagerID,
		CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.AdditionalContactsInformation'), 'vcFirstName', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS vcFirstName,
		CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.AdditionalContactsInformation'), 'charSex', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS charSex,
		CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.AdditionalContactsInformation'), 'vcLastName', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS vcLastName,

		CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.AdditionalContactsInformation'), 'bintDOB', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS bintDOB,
		CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.AdditionalContactsInformation'), 'numContactType', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS numContactType,
		CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.AdditionalContactsInformation'), 'numTeam', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS numTeam,
		CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.AdditionalContactsInformation'), 'txtNotes', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS txtNotes,
		CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.AdditionalContactsInformation'), 'bintCreatedDate', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS bintCreatedDate
		FROM 
		CHANGETABLE(CHANGES dbo.AdditionalContactsInformation, @last_synchronization_version) AS CT
		WHERE 
		ct.numContactId=@numRecordID
    --sp_helptext USP_GetWorkFlowFormFieldMaster
	--exec USP_GetWorkFlowFormFieldMaster 1,69
		INSERT INTO @tblFields(FieldName,FieldValue)
		SELECT
		FieldName,
		FieldValue
		FROM
			(
				SELECT  Address,numAge,vcAsstEmail,vcAsstFirstName,vcAsstLastName,numAsstPhone,vcAltEmail,vcCategory,numCell,vcEmail,numPhone,vcPosition,bitPrimaryContact,bitOptOut,numManagerID,vcFirstName,charSex,vcLastName,bintDOB,numContactType,numTeam,txtNotes,bintCreatedDate
				FROM @UFFields	
			) AS UP
		UNPIVOT
		(
		FieldValue FOR FieldName IN (Address,numAge,vcAsstEmail,vcAsstFirstName,vcAsstLastName,numAsstPhone,vcAltEmail,vcCategory,numCell,vcEmail,numPhone,vcPosition,bitPrimaryContact,bitOptOut,numManagerID,vcFirstName,charSex,vcLastName,bintDOB,numContactType,numTeam,txtNotes,bintCreatedDate)
		) AS upv
		where FieldValue<>0
 
		 SELECT @Columns_Updated = COALESCE(@Columns_Updated + ',', '') + FieldName FROM @tblFields 
		 SET @Columns_Updated=ISNULL(@Columns_Updated,'')
		 select @Columns_Updated
	END

	DECLARE @tintWFTriggerOn AS TINYINT
	SET @tintWFTriggerOn=CASE @Type WHEN 'I' THEN 1 WHEN 'U' THEN 2 WHEN 'D' THEN 5 END
	
--DateField WorkFlow Logic
	
	Declare @TempnumContactId AS NUMERIC
	IF @tintWFTriggerOn = 1
	BEGIN
		IF EXISTS (SELECT numContactId FROM AdditionalContactsInformation_TempDateFields WHERE numContactId=@numRecordID)
		BEGIN
			SET @TempnumContactId = (SELECT ACI.numContactId FROM [AdditionalContactsInformation] ACI WHERE  (			
				(ACI.bintCreatedDate >= DATEADD(DAY,-30,GETUTCDATE()) AND ACI.bintCreatedDate <= DATEADD(DAY,90,GETUTCDATE()) )
				OR
				(ACI.bintDOB >= DATEADD(DAY,-30,GETUTCDATE()) AND ACI.bintDOB <= DATEADD(DAY,90,GETUTCDATE()) )
		    )		
			AND ACI.numContactId=@numRecordID)

			IF(@TempnumContactId IS NOT NULL)
			BEGIN	
				UPDATE ACIT 
				SET 
						ACIT.bintCreatedDate = ACI.bintCreatedDate
						,ACIT.bintDOB = ACI.bintDOB
					
				FROM
					[AdditionalContactsInformation_TempDateFields] AS ACIT
					INNER JOIN [AdditionalContactsInformation] AS ACI
						ON ACIT.numContactId = ACI.numContactId
				WHERE  (
						(ACI.bintCreatedDate >= DATEADD(DAY,-30,GETUTCDATE()) AND ACI.bintCreatedDate <= DATEADD(DAY,90,GETUTCDATE()) )
						OR
						(ACI.bintDOB >= DATEADD(DAY,-30,GETUTCDATE()) AND ACI.bintDOB <= DATEADD(DAY,90,GETUTCDATE()) )
					  )			
					AND 
						ACIT.numContactId = @numRecordID
			END
			ELSE
			BEGIN
				DELETE FROM AdditionalContactsInformation_TempDateFields WHERE numContactId = @numRecordID
			END	
		END
		ELSE
		BEGIN
		INSERT INTO [AdditionalContactsInformation_TempDateFields] (numContactId, numDomainID, bintCreatedDate, bintDOB) 
		SELECT numContactId,numDomainID, bintCreatedDate,bintDOB	
		FROM AdditionalContactsInformation
		WHERE (
				(bintCreatedDate >= DATEADD(DAY,-30,GETUTCDATE()) AND bintCreatedDate <= DATEADD(DAY,90,GETUTCDATE()) )
				OR
				(bintDOB >= DATEADD(DAY,-30,GETUTCDATE()) AND bintDOB <= DATEADD(DAY,90,GETUTCDATE()) )
		      )	
			AND numContactId=@numRecordID
		END
	END
	ELSE IF @tintWFTriggerOn = 2
	BEGIN
		
		SET @TempnumContactId = (SELECT ACI.numContactId FROM [AdditionalContactsInformation] ACI WHERE  (			
				(ACI.bintCreatedDate >= DATEADD(DAY,-30,GETUTCDATE()) AND ACI.bintCreatedDate <= DATEADD(DAY,90,GETUTCDATE()) )
				OR
				(ACI.bintDOB >= DATEADD(DAY,-30,GETUTCDATE()) AND ACI.bintDOB <= DATEADD(DAY,90,GETUTCDATE()) )
		    )		
			AND ACI.numContactId=@numRecordID)

		IF(@TempnumContactId IS NOT NULL)
		BEGIN	
			UPDATE ACIT 
			SET 
					ACIT.bintCreatedDate = ACI.bintCreatedDate
					,ACIT.bintDOB = ACI.bintDOB
					
			FROM
				[AdditionalContactsInformation_TempDateFields] AS ACIT
				INNER JOIN [AdditionalContactsInformation] AS ACI
					ON ACIT.numContactId = ACI.numContactId
			WHERE  (
					(ACI.bintCreatedDate >= DATEADD(DAY,-30,GETUTCDATE()) AND ACI.bintCreatedDate <= DATEADD(DAY,90,GETUTCDATE()) )
					OR
					(ACI.bintDOB >= DATEADD(DAY,-30,GETUTCDATE()) AND ACI.bintDOB <= DATEADD(DAY,90,GETUTCDATE()) )
				  )			
				AND 
					ACIT.numContactId = @numRecordID
		END
		ELSE
		BEGIN
			DELETE FROM AdditionalContactsInformation_TempDateFields WHERE numContactId = @numRecordID
		END	
	END
	ELSE IF @tintWFTriggerOn = 5
	BEGIN
		DELETE FROM AdditionalContactsInformation_TempDateFields WHERE numContactId = @numRecordID
	END	

	--DateField WorkFlow Logic

	EXEC dbo.USP_ManageWorkFlowQueue
	@numWFQueueID = 0, --  numeric(18, 0)
	@numDomainID = @numDomainID, --  numeric(18, 0)
	@numUserCntID = @numUserCntID, --  numeric(18, 0)
	@numRecordID = @numRecordID, --  numeric(18, 0)
	@numFormID = 69, --  numeric(18, 0)
	@tintProcessStatus = 1, --  tinyint
	@tintWFTriggerOn = @tintWFTriggerOn, --  tinyint
	@tintMode = 1, --  tinyint
	@vcColumnsUpdated = @Columns_Updated
END
GO
