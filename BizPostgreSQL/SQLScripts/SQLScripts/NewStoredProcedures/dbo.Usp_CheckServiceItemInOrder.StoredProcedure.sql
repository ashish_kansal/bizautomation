GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='Usp_CheckServiceItemInOrder')
DROP PROCEDURE Usp_CheckServiceItemInOrder
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE Usp_CheckServiceItemInOrder
(
	@numOppID		NUMERIC(18,0),
	@numDomainID	NUMERIC(18,0),
	@numItemCode	NUMERIC(18,0),
	@sMode			TINYINT = 0
)
AS 
BEGIN
	DECLARE @RecCount AS INT
	IF @sMode = 0
		BEGIN
			IF ISNULL(@numOppID,0) <> 0
				BEGIN
					
					SELECT @RecCount = COUNT(*) FROM 
					dbo.OpportunityMaster OM 
					JOIN dbo.OpportunityItems OI ON OM.numOppId = OI.numOppId
					WHERE OI.numOppId = @numOppID
					AND numDomainID = @numDomainID
					AND numItemCode = @numItemCode
					
					SELECT CASE WHEN @RecCount > 0 THEN 1 ELSE 0 END AS Result
				END
			ELSE
				BEGIN
					SELECT @RecCount = COUNT(*) FROM dbo.Item WHERE numItemCode = @numItemCode AND numDomainID = @numDomainID AND item.charItemType = 'S'
					SELECT CASE WHEN @RecCount > 0 THEN 1 ELSE 0 END AS Result
				END	
		 END		
	ELSE IF @sMode = 1
		BEGIN
			SELECT @RecCount = COUNT(*) FROM 
				dbo.OpportunityMaster OM 
				JOIN dbo.OpportunityBizDocs OI ON OM.numOppId = OI.numOppId AND bitAuthoritativeBizDocs = 1
				JOIN dbo.OpportunityBizDocItems OBI ON OI.numOppBizDocsId = OBI.numOppBizDocID AND OBI.numItemCode = @numItemCode
				WHERE OI.numOppId = @numOppID
				AND numDomainID = @numDomainID
			
			IF @RecCount = 0
			BEGIN
				SELECT @RecCount = COUNT(*) FROM 
				dbo.OpportunityMaster OM 
				JOIN dbo.OpportunityBizDocs OI ON OM.numOppId = OI.numOppId AND bitAuthoritativeBizDocs = 1
				WHERE OI.numOppId = @numOppID
				AND numDomainID = @numDomainID	
			END				
			
			SELECT CASE WHEN @RecCount > 0 THEN 1 ELSE 0 END AS Result
		END			
END

