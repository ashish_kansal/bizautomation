GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_DeleteDomain')
DROP PROCEDURE USP_DeleteDomain
GO
CREATE PROCEDURE USP_DeleteDomain
	@numDomainID NUMERIC(18,0)
AS 
BEGIN
	IF ISNULL(@numDomainID,0)=0
	BEGIN
		RAISERROR('Domain id required',16,1);
		RETURN ;
	END
	ELSE IF @numDomainID=-255 OR @numDomainID=-1
	BEGIN
		RAISERROR('Verry funny.. can''t delete system account',16,1);
		RETURN ;
	END
	ELSE IF @numDomainID=1
	BEGIN
		RAISERROR('Verry funny.. can''t delete yourself',16,1);
		RETURN ;
	END
	ELSE IF @numDomainID=72
	BEGIN
		raiserror('Verry funny.. can''t delete developers test account',16,1);
		RETURN ;
	END

	/*Allow deletion only for suspended subscription*/
	IF exists (SELECT * FROM [Subscribers] WHERE bitActive = 1 AND numTargetDomainID = @numDomainID)
	BEGIN
		raiserror('Can''t delete active subscription',16,1);
		RETURN ;
	END

	BEGIN TRY
	--BEGIN TRANSACTION
		DELETE FROM General_Journal_Details WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM General_Journal_Header WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM OpportunityRecurring WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM EmbeddedCostItems WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM EmbeddedCost WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM CaseOpportunities WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM CreditBalanceHistory WHERE ISNULL(numDomainID,0)=@numDomainID

		DELETE 
			SRI 
		FROM 
			ShippingReportItems SRI 
		INNER JOIN 
			ShippingBox SB 
		ON 
			SRI.numBoxID = SB.numBoxID 
		INNER JOIN 
			ShippingReport SR 
		ON 
			SB.numShippingReportID = SR.numShippingReportID 
		INNER JOIN
			OpportunityBizDocs OBD
		ON
			SR.[numOppBizDocId] = OBD.numOppBizDocsId
		INNER JOIN
			OpportunityMaster OM
		ON
			OBD.numOppID = OM.numOppID
		WHERE 
			ISNULL(OM.numDomainID,0) = @numDomainID

		DELETE 
			SB 
		FROM 
			ShippingBox SB 
		INNER JOIN 
			ShippingReport SR 
		ON 
			SB.numShippingReportID = SR.numShippingReportID 
		INNER JOIN
			OpportunityBizDocs OBD
		ON
			SR.[numOppBizDocId] = OBD.numOppBizDocsId
		INNER JOIN
			OpportunityMaster OM
		ON
			OBD.numOppID = OM.numOppID
		WHERE 
			ISNULL(OM.numDomainID,0) = @numDomainID

		DELETE 
			OBDI 
		FROM 
			OpportunityBizDocItems OBDI 
		INNER JOIN
			OpportunityBizDocs OBD
		ON
			OBDI.numOppBizDocID = OBD.numOppBizDocsId
		INNER JOIN
			OpportunityMaster OM
		ON
			OBD.numOppID = OM.numOppID
		WHERE 
			ISNULL(OM.numDomainID,0) = @numDomainID

		DELETE OIRLR FROM OpportunityItemsReceievedLocationReturn OIRLR INNER JOIN ReturnItems RI ON OIRLR.numReturnItemID=RI.numReturnItemID INNER JOIN ReturnHeader RH ON RI.numReturnHeaderID=RH.numReturnHeaderID WHERE RH.numDomainID=@numDomainID
		DELETE OIRLR FROM OpportunityItemsReceievedLocationReturn OIRLR INNER JOIN ReturnHeader RH ON OIRLR.numReturnID=RH.numReturnHeaderID WHERE RH.numDomainID=@numDomainID
		DELETE OKCI FROM OpportunityKitChildItems OKCI INNER JOIN OpportunityMaster OM ON OKCI.numOppID=OM.numOppId WHERE ISNULL(OM.numDomainID,0)=@numDomainID
	
		DELETE 
			RT 
		FROM 
			RecurrenceTransaction RT 
		INNER JOIN 
			OpportunityBizDocs OBD 
		ON 
			RT.numRecurrOppBizDocID=OBD.numOppBizDocsId 
		INNER JOIN
			OpportunityMaster OM
		ON
			OBD.numOppID = OM.numOppID
		WHERE 
			ISNULL(OM.numDomainID,0) = @numDomainID

		DELETE 
			OBTI 
		FROM 
			OpportunityBizDocTaxItems OBTI 
		INNER JOIN 
			OpportunityBizDocs OBD 
		ON 
			OBTI.numOppBizDocID=OBD.numOppBizDocsId 
		INNER JOIN
			OpportunityMaster OM
		ON
			OBD.numOppID = OM.numOppID
		WHERE 
			ISNULL(OM.numDomainID,0)=@numDomainID

		DELETE SAD FROM StageAccessDetail SAD INNER JOIN AdditionalContactsInformation ACI ON SAD.numContactId=ACI.numContactId WHERE ISNULL(ACI.numDomainID,0)=@numDomainID
		DELETE RTR FROM RecurringTransactionReport RTR INNER JOIN OpportunityBizDocs OBD ON RTR.numRecTranBizDocID=OBD.numOppBizDocsId INNER JOIN OpportunityMaster OM ON OBD.numOppId=OM.numOppId WHERE ISNULL(OM.numDomainID,0)=@numDomainID
		DELETE RTR FROM RecurringTransactionReport RTR INNER JOIN OpportunityMaster OM ON RTR.numRecTranOppID=OM.numOppId WHERE ISNULL(OM.numDomainID,0)=@numDomainID
		DELETE RTH FROM RecurrenceTransactionHistory RTH INNER JOIN OpportunityBizDocs OBD ON RTH.numRecurrOppBizDocID=OBD.numOppBizDocsId INNER JOIN OpportunityMaster OM ON OBD.numOppId=OM.numOppId WHERE ISNULL(OM.numDomainID,0)=@numDomainID
		DELETE RTH FROM RecurrenceTransactionHistory RTH INNER JOIN OpportunityMaster OM ON RTH.numRecurrOppID=OM.numOppId WHERE ISNULL(OM.numDomainID,0)=@numDomainID
		DELETE PT FROM ProjectTeam PT INNER JOIN AdditionalContactsInformation ACI ON PT.numContactId=ACI.numContactId WHERE ISNULL(ACI.numDomainID,0)=@numDomainID
		DELETE PTR FROM ProjectTeamRights PTR INNER JOIN ProjectsMaster PM ON PTR.numProId=PM.numProId WHERE ISNULL(PM.numDomainID,0)=@numDomainID
		DELETE PPSD FROM ProjectProcessStageDetails PPSD INNER JOIN ProjectsMaster PM ON PPSD.numProjectId=PM.numProId WHERE ISNULL(PM.numDomainID,0)=@numDomainID
		DELETE PPSD FROM ProjectProcessStageDetails PPSD INNER JOIN OpportunityMaster OM ON PPSD.numOppId=OM.numOppId WHERE ISNULL(OM.numDomainID,0)=@numDomainID
		DELETE PT FROM PricingTable PT INNER JOIN PriceBookRules PBR ON PT.numPriceRuleID=PBR.numPricRuleID WHERE ISNULL(PBR.numDomainID,0)=@numDomainID
		DELETE PT FROM PricingTable PT INNER JOIN Item I ON PT.numItemCode=I.numItemCode WHERE ISNULL(I.numDomainID,0)=@numDomainID
		DELETE PBRD FROM PriceBookRuleDTL PBRD INNER JOIN PriceBookRules PBR ON PBRD.numRuleID=PBR.numPricRuleID WHERE ISNULL(PBR.numDomainID,0)=@numDomainID
		DELETE PMGS FROM PageMasterGroupSetting PMGS INNER JOIN AuthenticationGroupMaster AGM ON PMGS.numGroupID=AGM.numGroupID WHERE ISNULL(AGM.numDomainID,0)=@numDomainID
		DELETE OA FROM OrgOppAddressModificationHistory OA INNER JOIN UserMaster UM ON OA.numModifiedBy=UM.numUserDetailId WHERE ISNULL(UM.numDomainID,0)=@numDomainID
		DELETE OMTI FROM OpportunityMasterTaxItems OMTI INNER JOIN OpportunityMaster OM ON OMTI.numOppID=OM.numOppID WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE OBDPD FROM OpportunityBizDocsPaymentDetails OBDPD INNER JOIn OpportunityBizDocsDetails OBD ON OBDPD.numBizDocsPaymentDetId=OBD.numBizDocsPaymentDetId WHERE ISNULL(OBD.numDomainID,0)=@numDomainID
		DELETE OIL FROM OpportunityItemLinking OIL INNER JOIN OpportunityMaster OM ON OIL.numNewOppID=OM.numOppId WHERE ISNULL(OM.numDomainID,0)=@numDomainID
		DELETE OITI FROM OpportunityItemsTaxItems OITI INNER JOIN OpportunityMaster OM ON OITI.numOppId=OM.numOppId WHERE ISNULL(OM.numDomainID,0)=@numDomainID
		DELETE OITI FROM OpportunityItemsTaxItems OITI INNER JOIN ReturnHeader RH ON OITI.numReturnHeaderID=RH.numReturnHeaderID WHERE ISNULL(RH.numDomainID,0)=@numDomainID
		DELETE OBDKCI FROM OpportunityBizDocKitChildItems OBDKCI INNER JOIN OpportunityBizDocs OBD ON OBDKCI.numOppBizDocID=OBD.numOppBizDocsId INNER JOIN OpportunityMaster OM ON OBD.numOppId=OM.numOppId WHERE ISNULL(OM.numDomainID,0)=@numDomainID
		DELETE OBDKI FROM OpportunityBizDocKitItems OBDKI INNER JOIN OpportunityBizDocs OBD ON OBDKI.numOppBizDocID=OBD.numOppBizDocsId INNER JOIN OpportunityMaster OM ON OBD.numOppId=OM.numOppId WHERE ISNULL(OM.numDomainID,0)=@numDomainID
		DELETE MSOL FROM MassSalesOrderLog MSOL INNER JOIN OpportunityMaster OM ON MSOL.numCreatedOrderID=OM.numOppId WHERE ISNULL(OM.numDomainID,0)=@numDomainID
		DELETE MSOL FROM MassSalesOrderQueue MSOL INNER JOIN OpportunityMaster OM ON MSOL.numOppId=OM.numOppId WHERE ISNULL(OM.numDomainID,0)=@numDomainID
		DELETE EHAD FROM EmailHstrAttchDtls EHAD INNER JOIN EmailHistory EH ON EHAD.numEmailHstrID=EH.numEmailHstrID WHERE ISNULL(EH.numDomainID,0)=@numDomainID
		DELETE EHCB FROM EmailHStrToBCCAndCC EHCB INNER JOIN EmailHistory EH ON EHCB.numEmailHstrID=EH.numEmailHstrID WHERE ISNULL(EH.numDomainID,0)=@numDomainID
		DELETE DW FROM DocumentWorkflow DW INNER JOIN AdditionalContactsInformation ACI ON DW.numContactID=ACI.numContactId WHERE ISNULL(ACI.numDomainID,0)=@numDomainID
		DELETE DTT FROM DivisionTaxTypes DTT INNER JOIN DivisionMaster DM ON DTT.numDivisionID=DM.numDivisionID WHERE ISNULL(DM.numDomainID,0)=@numDomainID
		DELETE DMSA FROM DivisionMasterShippingAccount DMSA INNER JOIN DivisionMaster DM ON DMSA.numDivisionID=DM.numDivisionID WHERE ISNULL(DM.numDomainID,0)=@numDomainID
		DELETE BCPD FROM BizDocComissionPaymentDifference BCPD INNER JOIN BizDocComission BDC ON BCPD.numComissionID=BDC.numComissionID WHERE ISNULL(BDC.numDomainID,0)=@numDomainID
		DELETE RT FROM RecurrenceTransaction RT INNER JOIN OpportunityMaster OM ON RT.numRecurrOppID=OM.numOppId WHERE ISNULL(OM.numDomainID,0)=@numDomainID
		DELETE BAD FROM BizActionDetails BAD INNER JOIN OpportunityBizDocs OBD ON BAD.numOppBizDocsId=OBD.numOppBizDocsId INNER JOIN OpportunityMaster OM ON OBD.numOppId=OM.numOppId WHERE ISNULL(OM.numDomainID,0)=@numDomainID
		DELETE OA FROM OpportunityAddress OA INNER JOIN OpportunityMaster OM ON OA.numOppID=OM.numOppID WHERE ISNULL(OM.numDomainID,0)=@numDomainID
		DELETE OBD FROM OpportunityBizDocs OBD INNER JOIN OpportunityMaster OM ON OBD.numOppId=OM.numOppId WHERE ISNULL(OM.numDomainID,0)=@numDomainID
		DELETE OC FROM OpportunityContact OC INNER JOIN OpportunityMaster OM ON OC.numOppId=OM.numOppId WHERE ISNULL(OM.numDomainID,0)=@numDomainID
		DELETE OD FROM OpportunityDependency OD INNER JOIN OpportunityMaster OM ON OD.numOpportunityId=OM.numOppId WHERE ISNULL(OM.numDomainID,0)=@numDomainID
		DELETE OKI FROM OpportunityKitItems OKI INNER JOIN OpportunityMaster OM ON OKI.numOppId=OM.numOppId WHERE ISNULL(OM.numDomainID,0)=@numDomainID
		DELETE OL FROM OpportunityLinking OL INNER JOIN OpportunityMaster OM ON OL.numChildOppID=OM.numOppId WHERE ISNULL(OM.numDomainID,0)=@numDomainID
		DELETE OL FROM OpportunityLinking OL INNER JOIN OpportunityMaster OM ON OL.numParentOppID=OM.numOppId WHERE ISNULL(OM.numDomainID,0)=@numDomainID
		DELETE OSSD FROM OpportunitySubStageDetails OSSD INNER JOIN OpportunityMaster OM ON OSSD.numOppId=OM.numOppId WHERE ISNULL(OM.numDomainID,0)=@numDomainID
		DELETE OWSI FROM OppWarehouseSerializedItem OWSI INNER JOIN OpportunityMaster OM ON OWSI.numOppId=OM.numOppId WHERE ISNULL(OM.numDomainID,0)=@numDomainID
		DELETE R FROM Returns R INNER JOIN OpportunityMaster OM ON R.numOppId=OM.numOppId WHERE ISNULL(OM.numDomainID,0)=@numDomainID
		DELETE AA FROM ActivityAttendees AA INNER JOIN ActivityResource AR ON AA.ActivityID=AR.ActivityID INNER JOIN Resource R ON AR.ResourceID=R.ResourceID WHERE ISNULL(R.numDomainID,0)=@numDomainID
		DELETE AR FROM ActivityResource AR INNER JOIN Resource R ON AR.ResourceID=R.ResourceID WHERE ISNULL(R.numDomainID,0)=@numDomainID
		DELETE BEL FROM BroadcastErrorLog BEL INNER JOIN BroadCastDtls BD ON BEL.numBroadcastDtlID=BD.numBroadCastDtlId INNER JOIN Broadcast B ON BD.numBroadCastID=B.numBroadCastId WHERE ISNULL(B.numDomainID,0)=@numDomainID
		DELETE CFD FROM CFW_Fld_Dtl CFD INNER JOIN CFW_Fld_Master CFM ON CFD.numFieldId=CFM.Fld_id WHERE ISNULL(CFM.numDomainID,0)=@numDomainID
		DELETE CFV FROM CFW_FLD_Values CFV INNER JOIN CFW_Fld_Master CFM ON CFV.Fld_ID=CFM.Fld_id WHERE ISNULL(CFM.numDomainID,0)=@numDomainID
		DELETE CFVA FROM CFW_Fld_Values_Attributes CFVA INNER JOIN CFW_Fld_Master CFM ON CFVA.Fld_ID=CFM.Fld_id WHERE ISNULL(CFM.numDomainID,0)=@numDomainID
		DELETE CFVC FROM CFW_FLD_Values_Case CFVC INNER JOIN CFW_Fld_Master CFM ON CFVC.Fld_ID=CFM.Fld_id WHERE ISNULL(CFM.numDomainID,0)=@numDomainID
		DELETE CFVC FROM CFW_FLD_Values_Cont CFVC INNER JOIN CFW_Fld_Master CFM ON CFVC.Fld_ID=CFM.Fld_id WHERE ISNULL(CFM.numDomainID,0)=@numDomainID
		DELETE CFVI FROM CFW_FLD_Values_Item CFVI INNER JOIN CFW_Fld_Master CFM ON CFVI.Fld_ID=CFM.Fld_id WHERE ISNULL(CFM.numDomainID,0)=@numDomainID
		DELETE CFVIE FROM CFW_FLD_Values_Item_EDI CFVIE INNER JOIN CFW_Fld_Master CFM ON CFVIE.Fld_ID=CFM.Fld_id WHERE ISNULL(CFM.numDomainID,0)=@numDomainID
		DELETE CFVO FROM CFW_Fld_Values_Opp CFVO INNER JOIN CFW_Fld_Master CFM ON CFVO.Fld_ID=CFM.Fld_id WHERE ISNULL(CFM.numDomainID,0)=@numDomainID
		DELETE CFVOE FROM CFW_Fld_Values_Opp_EDI CFVOE INNER JOIN CFW_Fld_Master CFM ON CFVOE.Fld_ID=CFM.Fld_id WHERE ISNULL(CFM.numDomainID,0)=@numDomainID
		DELETE CFVOI FROM CFW_Fld_Values_OppItems CFVOI INNER JOIN CFW_Fld_Master CFM ON CFVOI.Fld_ID=CFM.Fld_id WHERE ISNULL(CFM.numDomainID,0)=@numDomainID
		DELETE CFVOIE FROM CFW_Fld_Values_OppItems_EDI CFVOIE INNER JOIN CFW_Fld_Master CFM ON CFVOIE.Fld_ID=CFM.Fld_id WHERE ISNULL(CFM.numDomainID,0)=@numDomainID
		DELETE CFVP FROM CFW_Fld_Values_Product CFVP INNER JOIN CFW_Fld_Master CFM ON CFVP.Fld_ID=CFM.Fld_id WHERE ISNULL(CFM.numDomainID,0)=@numDomainID
		DELETE CFVP FROM CFW_FLD_Values_Pro CFVP INNER JOIN CFW_Fld_Master CFM ON CFVP.Fld_ID=CFM.Fld_id WHERE ISNULL(CFM.numDomainID,0)=@numDomainID
		DELETE CFVSI FROM CFW_Fld_Values_Serialized_Items CFVSI INNER JOIN CFW_Fld_Master CFM ON CFVSI.Fld_ID=CFM.Fld_id WHERE ISNULL(CFM.numDomainID,0)=@numDomainID
		DELETE CRD FROM ChecksRecurringDetails CRD INNER JOIN CheckHeader CH ON CRD.numCheckId=CH.numCheckHeaderID WHERE ISNULL(CH.numDomainID,0)=@numDomainID
		DELETE C FROM Comments C INNER JOIN StagePercentageDetails SPD ON C.numStageID=SPD.numStageDetailsId WHERE ISNULL(SPD.numDomainID,0)=@numDomainID
		DELETE C FROM Comments C INNER JOIN Cases SPD ON C.numCaseID=SPD.numCaseId WHERE ISNULL(SPD.numDomainID,0)=@numDomainID
		DELETE CED FROM ConECampaignDTL CED INNER JOIN ECampaignDTLs ED ON CED.numECampDTLID=ED.numECampDTLId INNER JOIN ECampaign E ON ED.numECampID=E.numECampaignID WHERE ISNULL(E.numDomainID,0)=@numDomainID
		DELETE DD FROM DepositeDetails DD INNER JOIN DepositMaster DM ON DD.numDepositID=DM.numDepositId WHERE ISNULL(DM.numDomainID,0)=@numDomainID
		DELETE EIC FROM ECommerceItemClassification EIC INNER JOIN EcommerceRelationshipProfile ERP ON EIC.numECommRelatiionshipProfileID=ERP.ID INNER JOIN Sites S ON ERP.numSiteID=S.numSiteID WHERE ISNULL(S.numDomainID,0)=@numDomainID
		DELETE OI FROM OpportunityItems OI INNER JOIN OpportunityMaster OM ON OI.numOppId=OM.numOppId WHERE ISNULL(OM.numDomainID,0)=@numDomainID
		DELETE PC FROM ProjectsContacts PC INNER JOIN ProjectsMaster PM ON PC.numProId=PM.numProId WHERE ISNULL(PM.numDomainID,0)=@numDomainID
		DELETE PD FROM ProjectsDependency PD INNER JOIN ProjectsMaster PM ON PD.numProId=PM.numProId WHERE ISNULL(PM.numDomainID,0)=@numDomainID
		DELETE PO FROM ProjectsOpportunities PO INNER JOIN ProjectsMaster PM ON PO.numProId=PM.numProId WHERE ISNULL(PM.numDomainID,0)=@numDomainID
		DELETE PSSD FROM ProjectsSubStageDetails PSSD INNER JOIN ProjectsMaster PM ON PSSD.numProId=PM.numProId WHERE ISNULL(PM.numDomainID,0)=@numDomainID
		DELETE RI FROM ReturnItems RI INNER JOIN ReturnHeader RH ON RI.numReturnHeaderID=RH.numReturnHeaderID WHERE ISNULL(RH.numDomainID,0)=@numDomainID
		DELETE SD FROM StageDependency SD INNER JOIN StagePercentageDetails SPD ON SD.numStageDetailID=SPD.numStageDetailsId WHERE ISNULL(SPD.numDomainID,0)=@numDomainID
		DELETE SD FROM StageDependency SD INNER JOIN StagePercentageDetails SPD ON SD.numDependantOnID=SPD.numStageDetailsId WHERE ISNULL(SPD.numDomainID,0)=@numDomainID
		DELETE SSD FROM StyleSheetDetails SSD INNER JOIN SitePages SP ON SSD.numPageID=SP.numPageID WHERE ISNULL(SP.numDomainID,0)=@numDomainID
		DELETE SSD FROM StyleSheetDetails SSD INNER JOIN StyleSheets SS ON SSD.numCssID=SS.numCssID WHERE ISNULL(SS.numDomainID,0)=@numDomainID
		DELETE SRC FROM SurveyRespondentsChild SRC INNER JOIN SurveyRespondentsMaster SRM ON SRC.numRespondantID=SRM.numRespondantID WHERE ISNULL(SRM.numDomainID,0)=@numDomainID
		DELETE WID FROM WareHouseItmsDTL WID INNER JOIN WareHouseItems WI ON WID.numWareHouseItemID=WI.numWareHouseItemID WHERE ISNULL(WI.numDomainID,0)=@numDomainID
		DELETE WFAUF FROM WorkFlowActionUpdateFields WFAUF INNER JOIN WorkFlowActionList WFASL ON WFAUF.numWFActionID = WFASL.numWFActionID INNER JOIN WorkFlowMaster WFM ON WFASL.numWFID=WFM.numWFID WHERE ISNULL(WFM.numDomainID,0)=@numDomainID
		DELETE A FROM Activity A INNER JOIN ActivityResource AR ON A.ActivityID=AR.ActivityID INNER JOIN Resource R ON AR.ResourceID=R.ResourceID WHERE ISNULL(R.numDomainID,0)=@numDomainID
		DELETE AIC FROM AssembledItemChilds AIC INNER JOIN Item I ON AIC.numAssembledItemID=I.numItemCode WHERE ISNULL(I.numDomainID,0)=@numDomainID
		DELETE AIC FROM AssembledItemChilds AIC INNER JOIN Item I ON AIC.numItemCode=I.numItemCode WHERE ISNULL(I.numDomainID,0)=@numDomainID
		DELETE BRMRC FROM BankReconcileMatchRuleCondition BRMRC INNER JOIN BankReconcileMatchRule BRMR ON BRMRC.numRuleID=BRMR.ID WHERE ISNULL(BRMR.numDomainID,0)=@numDomainID
		DELETE BD FROM BillDetails BD INNER JOIN BillHeader BH ON BD.numBillID=BH.numBillID WHERE ISNULL(BH.numDomainID,0)=@numDomainID
		DELETE BCD FROM BroadCastDtls BCD INNER JOIN Broadcast B ON BCD.numBroadCastID=B.numBroadCastId  WHERE ISNULL(B.numDomainID,0)=@numDomainID
		DELETE CC FROM CaseContacts CC INNER JOIN Cases C ON CC.numCaseID=C.numCaseId WHERE ISNULL(C.numDomainID,0)=@numDomainID
		DELETE CS FROM CaseSolutions CS  INNER JOIN Cases C ON CS.numCaseID=C.numCaseId WHERE ISNULL(C.numDomainID,0)=@numDomainID
		DELETE CPS FROM CategoryProfileSites CPS INNER JOIN Sites S ON CPS.numSiteID=S.numSiteID WHERE ISNULL(S.numDomainID,0)=@numDomainID
		DELETE CRD FROM CommissionRuleDtl CRD INNER JOIN CommissionRules CR ON CRD.numComRuleID=CR.numComRuleID WHERE ISNULL(CR.numDomainID,0)=@numDomainID
		DELETE CRI FROM CommissionRuleItems CRI INNER JOIN CommissionRules CR ON CRI.numComRuleID=CR.numComRuleID WHERE ISNULL(CR.numDomainID,0)=@numDomainID
		DELETE CRO FROM CommissionRuleOrganization CRO INNER JOIN CommissionRules CR ON CRO.numComRuleID=CR.numComRuleID WHERE ISNULL(CR.numDomainID,0)=@numDomainID
		DELETE CLO FROM CommunicationLinkedOrganization CLO INNER JOIN Communication C ON CLO.numCommID=C.numCommId WHERE ISNULL(C.numDomainID,0)=@numDomainID
		DELETE CEC FROM ConECampaign CEC INNER JOIN ECampaign EC ON CEC.numECampaignID=EC.numECampaignID WHERE ISNULL(EC.numDomainID,0)=@numDomainID
		DELETE CCCI FROM CustomerCreditCardInfo CCCI INNER JOIN AdditionalContactsInformation ACI ON CCCI.numContactId=ACI.numContactId WHERE ISNULL(ACI.numDomainID,0)=@numDomainID
		DELETE CQRE FROM CustomQueryReportEmail CQRE INNER JOIN ReportListMaster RLM ON CQRE.numReportID=RLM.numReportID WHERE ISNULL(RLM.numDomainID,0)=@numDomainID
		DELETE DFIC FROM DemandForecastItemClassification DFIC INNER JOIN DemandForecast DF ON DFIC.numDFID=DF.numDFID WHERE ISNULL(DF.numDomainID,0)=@numDomainID
		DELETE DFIG FROM DemandForecastItemGroup DFIG INNER JOIN DemandForecast DF ON DFIG.numDFID=DF.numDFID WHERE ISNULL(DF.numDomainID,0)=@numDomainID
		DELETE DFW FROM DemandForecastWarehouse DFW INNER JOIN DemandForecast DF ON DFW.numDFID=DF.numDFID WHERE ISNULL(DF.numDomainID,0)=@numDomainID
		DELETE ECD FROM ECampaignDTLs ECD INNER JOIN ECampaign EC ON ECD.numECampID=EC.numECampaignID WHERE ISNULL(EC.numDomainID,0)=@numDomainID
		DELETE ERP FROM EcommerceRelationshipProfile ERP INNER JOIN Sites S ON ERP.numSiteID=S.numSiteID WHERE ISNULL(S.numDomainID,0)=@numDomainID
		DELETE FRD FROM FieldRelationshipDTL FRD INNER JOIN FieldRelationship FR ON FRD.numFieldRelID=FR.numFieldRelID WHERE ISNULL(FR.numDomainID,0)=@numDomainID
		DELETE FRD FROM FinancialReportDetail FRD INNER JOIN FinancialReport FR ON FRD.numFRID=FR.numFRID WHERE ISNULL(FR.numDomainID,0)=@numDomainID
		DELETE IFR FROM Import_File_Report IFR INNER JOIN Import_File_Master IFM ON IFR.intImportFileID=IFM.intImportFileID WHERE ISNULL(IFM.numDomainID,0)=@numDomainID
		DELETE MBD FROM MarketBudgetDetails MBD INNER JOIN MarketBudgetMaster MBM ON MBD.numMarketId=MBM.numMarketBudgetId WHERE ISNULL(MBM.numDomainID,0)=@numDomainID
		DELETE MSFWMS FROM MassSalesFulfillmentWMState MSFWMS INNER JOIN MassSalesFulfillmentWM MSFWM ON MSFWMS.numMSFWMID=MSFWM.ID  WHERE ISNULL(MSFWM.numDomainID,0)=@numDomainID
		DELETE MSFWP FROM MassSalesFulfillmentWP MSFWP INNER JOIN MassSalesFulfillmentWM MSFWM ON MSFWP.numMSFWMID=MSFWM.ID  WHERE ISNULL(MSFWM.numDomainID,0)=@numDomainID
		DELETE OBD FROM OperationBudgetDetails OBD INNER JOIN OperationBudgetMaster OBM ON OBD.numBudgetID=OBM.numBudgetID WHERE ISNULL(OBM.numDomainID,0)=@numDomainID
		DELETE OARD FROM OrderAutoRuleDetails OARD INNER JOIN OrderAutoRule OAR ON OARD.numRuleID=OAR.numRuleID WHERE ISNULL(OAR.numDomainID,0)=@numDomainID	
		DELETE PBRI FROM PriceBookRuleItems PBRI INNER JOIN PriceBookRules PBR ON PBRI.numRuleID=PBR.numPricRuleID WHERE ISNULL(PBR.numDomainID,0)=@numDomainID
		DELETE PGD FROM ProfileEGroupDTL PGD INNER JOIN ProfileEmailGroup PEG ON PGD.numEmailGroupID=PEG.numEmailGroupID  WHERE ISNULL(PEG.numDomainID,0)=@numDomainID
		DELETE POO FROM PromotionOfferOrganizations POO INNER JOIN PromotionOffer PO ON POO.numProId=PO.numProId WHERE ISNULL(PO.numDomainID,0)=@numDomainID
		DELETE POS FROM PromotionOfferSites POS INNER JOIN PromotionOffer PO ON POS.numPromotionID=PO.numProId WHERE ISNULL(PO.numDomainID,0)=@numDomainID
		DELETE RP FROM ResourcePreference RP INNER JOIN Resource R ON RP.ResourceID=R.ResourceID WHERE ISNULL(R.numDomainID,0)=@numDomainID
		DELETE RD FROM ReviewDetail RD INNER JOIN Review R ON RD.numReviewId=R.numReviewId WHERE ISNULL(R.numDomainID,0)=@numDomainID
		DELETE RLD FROM RoutingLeadDetails RLD INNER JOIN RoutingLeads RL ON RLD.numRoutID=RL.numRoutID WHERE ISNULL(RL.numDomainID,0)=@numDomainID
		DELETE RLV FROM RoutinLeadsValues RLV INNER JOIN RoutingLeads RL ON RLV.numRoutID=RL.numRoutID WHERE ISNULL(RL.numDomainID,0)=@numDomainID
		DELETE SLRC FROM ShippingLabelRuleChild SLRC INNER JOIN ShippingLabelRuleMaster SLRM ON SLRC.numShippingRuleID=SLRM.numShippingRuleID WHERE ISNULL(SLRM.numDomainID,0)=@numDomainID
		DELETE SC FROM SiteCategories SC INNER JOIN Sites S ON SC.numSiteID=S.numSiteID WHERE ISNULL(S.numDomainID,0)=@numDomainID
		DELETE SSD FROM SiteSubscriberDetails SSD INNER JOIN Sites S ON SSD.numSiteID=S.numSiteID WHERE ISNULL(S.numDomainID,0)=@numDomainID
		DELETE SWC FROM SiteWiseCategories SWC INNER JOIN Sites S ON SWC.numSiteID=S.numSiteID WHERE ISNULL(S.numDomainID,0)=@numDomainID
		DELETE SPDTN FROM StagePercentageDetailsTaskNotes SPDTN INNER JOIN StagePercentageDetailsTask SPDT ON SPDTN.numTaskID=SPDT.numTaskId WHERE ISNULL(SPDT.numDomainID,0)=@numDomainID
		DELETE SAM FROM SurveyAnsMaster SAM INNER JOIN SurveyMaster SM ON SAM.numSurID=SM.numSurID WHERE ISNULL(SM.numDomainID,0)=@numDomainID
		DELETE SQM FROM SurveyQuestionMaster SQM INNER JOIN SurveyMaster SM ON SQM.numSurID=SM.numSurID WHERE ISNULL(SM.numDomainID,0)=@numDomainID
		DELETE SR FROM SurveyResponse SR INNER JOIN SurveyMaster SM ON SR.numSurID=SM.numSurID WHERE ISNULL(SM.numDomainID,0)=@numDomainID
		DELETE SWR FROM SurveyWorkflowRules SWR INNER JOIN SurveyMaster SM ON SWR.numSurID=SM.numSurID WHERE ISNULL(SM.numDomainID,0)=@numDomainID
		DELETE TVD FROM TrackingVisitorsDTL TVD INNER JOIN TrackingVisitorsHDR TVH ON TVD.numTracVisitorsHDRID=TVH.numTrackingID WHERE ISNULL(TVH.numDomainID,0)=@numDomainID
		DELETE WAL FROM WorkFlowActionList WAL INNER JOIN WorkFlowMaster WFM ON WAL.numWFID=WFM.numWFID WHERE ISNULL(WFM.numDomainID,0)=@numDomainID
		DELETE WCL FROM WorkFlowConditionList WCL INNER JOIN WorkFlowMaster WFM ON WCL.numWFID=WFM.numWFID WHERE ISNULL(WFM.numDomainID,0)=@numDomainID
		DELETE WTFL FROM WorkFlowTriggerFieldList WTFL INNER JOIN WorkFlowMaster WFM ON WTFL.numWFID=WFM.numWFID WHERE ISNULL(WFM.numDomainID,0)=@numDomainID
		DELETE WOPI FROM WorkOrderPickedItems WOPI INNER JOIN WorkOrderDetails WOD ON WOPI.numWODetailId=WOD.numWODetailId INNER JOIN WorkOrder WO ON WOD.numWOId=WO.numWOId WHERE ISNULL(WO.numDomainID,0)=@numDomainID
		DELETE WSDO FROM WorkScheduleDaysOff WSDO INNER JOIN WorkSchedule WS ON WSDO.numWorkScheduleID=WS.ID WHERE ISNULL(WS.numDomainID,0)=@numDomainID
		DELETE ACL FROM AOIContactLink ACL INNER JOIN AdditionalContactsInformation ACI ON ACL.numContactID=ACI.numContactId WHERE ISNULL(ACI.numDomainID,0)=@numDomainID
		DELETE BPD FROM BillPaymentDetails BPD INNER JOIN BillPaymentHeader BPH ON BPD.numBillPaymentID=BPH.numBillPaymentID WHERE ISNULL(BPH.numDomainID,0)=@numDomainID
		DELETE BL FROM BroadcastingLink BL INNER JOIN Broadcast B ON BL.numBroadcastId=B.numBroadCastId WHERE ISNULL(B.numDomainID,0)=@numDomainID
		DELETE CD FROM CampaignDetails CD INNER JOIN CampaignMaster CM ON CD.numCampaignId=CM.numCampaignId WHERE ISNULL(CM.numDomainID,0)=@numDomainID
		DELETE CD FROM CheckDetails CD INNER JOIN CheckHeader CH ON CD.numCheckHeaderID=CH.numCheckHeaderID WHERE ISNULL(CH.numDomainID,0)=@numDomainID
		DELETE CKR FROM ChildKitsRelation CKR INNER JOIN Item I ON CKR.numItemCode=I.numItemCode WHERE ISNULL(I.numDomainID,0)=@numDomainID
		DELETE CRC FROM CommissionRuleContacts CRC INNER JOIN CommissionRules CR ON CRC.numComRuleID=CR.numComRuleID WHERE ISNULL(CR.numDomainID,0)=@numDomainID
		DELETE CA FROM CommunicationAttendees CA INNER JOIN Communication C ON CA.numCommId=C.numCommId WHERE ISNULL(C.numDomainID,0)=@numDomainID
		DELETE CL FROM ContractsLog CL INNER JOIN Contracts C ON CL.numContractId=C.numContractId WHERE ISNULL(C.numDomainID,0)=@numDomainID
		DELETE CRF FROM CustReportFields CRF INNER JOIN CustomReport CR ON CRF.numCustomReportId=CR.numCustomReportID WHERE ISNULL(CR.numDomainID,0)=@numDomainID
		DELETE CRFL FROM CustReportFilterlist CRFL INNER JOIN CustomReport CR ON CRFL.numCustomReportId=CR.numCustomReportID WHERE ISNULL(CR.numDomainID,0)=@numDomainID
		DELETE CRK FROM CustReportKPI CRK INNER JOIN CustomReport CR ON CRK.numCustomReportId=CR.numCustomReportID WHERE ISNULL(CR.numDomainID,0)=@numDomainID
		DELETE CROL FROM CustReportOrderlist CROL INNER JOIN CustomReport CR ON CROL.numCustomReportId=CR.numCustomReportID WHERE ISNULL(CR.numDomainID,0)=@numDomainID
		DELETE CRSL FROM CustReportSummationlist CRSL INNER JOIN CustomReport CR ON CRSL.numCustomReportId=CR.numCustomReportID WHERE ISNULL(CR.numDomainID,0)=@numDomainID
		DELETE D FROM Dashboard D INNER JOIN CustomReport CR ON D.numReportID=CR.numCustomReportID WHERE ISNULL(CR.numDomainID,0)=@numDomainID
		DELETE DAR FROM DashboardAllowedReports DAR INNER JOIn AuthenticationGroupMaster AGM ON DAR.numGrpId=AGM.numGroupID WHERE ISNULL(AGM.numDomainID,0)=@numDomainID
		DELETE DS FROM DashBoardSize DS INNER JOIN UserMaster UM ON DS.numGroupUserCntID=UM.numUserDetailId WHERE ISNULL(UM.numDomainID,0)=@numDomainID
		DELETE DCU FROM DiscountCodeUsage DCU INNER JOIN DiscountCodes DC ON DCU.numDiscountId=DC.numDiscountId INNER JOIN PromotionOffer PO ON DC.numPromotionID=PO.numProId WHERE ISNULL(PO.numDomainID,0)=@numDomainID
		DELETE DC FROM DiscountCodes DC INNER JOIN PromotionOffer PO ON DC.numPromotionID=PO.numProId WHERE ISNULL(PO.numDomainID,0)=@numDomainID
		DELETE F FROM Favorites F INNER JOIN UserMaster UM ON F.numUserCntID=UM.numUserDetailId WHERE ISNULL(UM.numDomainID,0)=@numDomainID
		DELETE FRC FROM ForReportsByCampaign FRC INNER JOIN UserMaster UM ON FRC.numUserCntID=UM.numUserDetailId WHERE ISNULL(UM.numDomainID,0)=@numDomainID
		DELETE GTD FROM GroupTabDetails GTD INNER JOIN AuthenticationGroupMaster AGM ON GTD.numGroupId=AGM.numGroupID WHERE ISNULL(AGM.numDomainID,0)=@numDomainID
		DELETE IFFM FROM Import_File_Field_Mapping IFFM INNER JOIN Import_File_Master IFM ON IFFM.intImportFileID=IFM.intImportFileID WHERE ISNULL(IFM.numDomainID,0)=@numDomainID
		DELETE IH FROM Import_History IH INNER JOIN Import_File_Master IFM ON IH.intImportFileID=IFM.intImportFileID WHERE ISNULL(IFM.numDomainID,0)=@numDomainID
		DELETE IC FROM ItemCategory IC INNER JOIN Category C ON IC.numCategoryID=C.numCategoryID WHERE ISNULL(C.numDomainID,0)=@numDomainID
		DELETE IC FROM ItemCategory IC INNER JOIN Item I ON IC.numItemID=I.numItemCode WHERE ISNULL(I.numDomainID,0)=@numDomainID
		DELETE ID FROM ItemDetails ID INNER JOIN Item I ON ID.numChildItemID=I.numItemCode WHERE ISNULL(I.numDomainID,0)=@numDomainID
		DELETE ID FROM ItemDetails ID INNER JOIN Item I ON ID.numItemKitID=I.numItemCode WHERE ISNULL(I.numDomainID,0)=@numDomainID	
		DELETE IED FROM ItemExtendedDetails IED INNER JOIN Item I ON IED.numItemCode=I.numItemCode WHERE ISNULL(I.numDomainID,0)=@numDomainID
		DELETE IGD FROM ItemGroupsDTL IGD INNER JOIN ItemGroups IG ON IGD.numItemGroupID=IG.numItemGroupID WHERE ISNULL(IG.numDomainID,0)=@numDomainID
		DELETE IT FROM ItemTax IT INNER JOIN Item I ON IT.numItemCode=I.numItemCode WHERE ISNULL(I.numDomainID,0)=@numDomainID	
		DELETE MSFBO FROM MassSalesFulfillmentBatchOrders MSFBO INNER JOIN MassSalesFulfillmentBatch MSFB ON MSFBO.numBatchID=MSFB.ID WHERE ISNULL(MSFB.numDomainID,0)=@numDomainID
		DELETE MST FROM MassStockTransfer MST INNER JOIN Item I ON MST.numItemCode=I.numItemCode WHERE ISNULL(I.numDomainID,0)=@numDomainID
		DELETE MRL FROM MyReportList MRL INNER JOIN UserMaster UM ON MRL.numUserCntID=UM.numUserDetailId WHERE ISNULL(UM.numDomainID,0)=@numDomainID
		DELETE OAQE FROM OpportunityAutomationQueueExecution OAQE INNER JOIN OpportunityAutomationQueue OAQ ON OAQE.numOppQueueID=OAQ.numOppQueueID WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE PBD FROM ProcurementBudgetDetails PBD INNER JOIN ProcurementBudgetMaster PBM ON PBD.numProcurementId=PBM.numProcurementBudgetId WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE POC FROM PromotionOfferContacts POC INNER JOIN PromotionOffer PO ON POC.numProId=PO.numProId WHERE ISNULL(PO.numDomainID,0)=@numDomainID
		DELETE POI FROM PromotionOfferItems POI INNER JOIN PromotionOffer PO ON POI.numProId=PO.numProId WHERE ISNULL(PO.numDomainID,0)=@numDomainID
		DELETE POOB FROM PromotionOfferOrderBased POOB INNER JOIN PromotionOffer PO ON POOB.numProID=PO.numProId WHERE ISNULL(PO.numDomainID,0)=@numDomainID
		DELETE RI FROM RecentItems RI INNER JOIN UserMaster UM ON RI.numUserCntID=UM.numUserDetailId WHERE ISNULL(UM.numDomainID,0)=@numDomainID
		DELETE RD FROM RemarketDTL RD INNER JOIN Item I ON RD.numItemID=I.numItemCode WHERE ISNULL(I.numDomainID,0)=@numDomainID
		DELETE RSGL FROM ReportSummaryGroupList RSGL INNER JOIN ReportListMaster RLM ON RSGL.numReportID=RLM.numReportID WHERE ISNULL(RLM.numDomainID,0)=@numDomainID
		DELETE RMAL FROM ReportMatrixAggregateList RMAL INNER JOIN ReportListMaster RLM ON RMAL.numReportID=RLM.numReportID WHERE ISNULL(RLM.numDomainID,0)=@numDomainID
		DELETE RKT FROM ReportKPIThresold RKT INNER JOIN ReportListMaster RLM ON RKT.numReportID=RLM.numReportID WHERE ISNULL(RLM.numDomainID,0)=@numDomainID
		DELETE RFL FROM ReportFieldsList RFL INNER JOIN ReportListMaster RLM ON RFL.numReportID=RLM.numReportID WHERE ISNULL(RLM.numDomainID,0)=@numDomainID
		DELETE RFL FROM ReportFilterList RFL INNER JOIN ReportListMaster RLM ON RFL.numReportID=RLM.numReportID WHERE ISNULL(RLM.numDomainID,0)=@numDomainID
		DELETE RKGDL FROM ReportKPIGroupDetailList RKGDL INNER JOIN ReportListMaster RLM ON RKGDL.numReportID=RLM.numReportID WHERE ISNULL(RLM.numDomainID,0)=@numDomainID
		DELETE SRGL FROM ScheduledReportsGroupLog SRGL INNER JOIN ScheduledReportsGroup SRG ON SRGL.numSRGID=SRG.ID WHERE ISNULL(SRG.numDomainID,0)=@numDomainID
		DELETE SRGR FROM ScheduledReportsGroupReports SRGR INNER JOIN ScheduledReportsGroup SRG ON SRGR.numSRGID=SRG.ID WHERE ISNULL(SRG.numDomainID,0)=@numDomainID
		DELETE SMAM FROM SurveyMatrixAnsMaster SMAM INNER JOIN SurveyMaster SM ON SMAM.numSurID=SM.numSurID WHERE ISNULL(SM.numDomainID,0)=@numDomainID
		DELETE SCR FROM SurveyCreateRecord SCR INNER JOIN SurveyMaster SM ON SCR.numSurID=SM.numSurID WHERE ISNULL(SM.numDomainID,0)=@numDomainID
		DELETE UR FROM UserRoles UR INNER JOIN UserMaster UM ON UR.numUserCntID=UM.numUserDetailId WHERE ISNULL(UM.numDomainID,0)=@numDomainID
		DELETE WFQE FROM WorkFlowQueueExecution WFQE INNER JOIN WorkFlowMaster WFM ON WFQE.numWFID=WFM.numWFID WHERE ISNULL(WFM.numDomainID,0)=@numDomainID
		DELETE WOD FROM WorkOrderDetails WOD INNER JOIN WorkOrder WO ON WOD.numWOId=WO.numWOId WHERE ISNULL(WO.numDomainID,0)=@numDomainID
		
				
		DELETE FROM SalesTemplateItems WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM ShippingReport WHERE ISNULL(numDomainID,0)=@numDomainID
		
		DELETE FROM EDIQueue WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM ExtranetAccountsDtl WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM OpportunityItemsReceievedLocation WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM OpportunityItemsReleaseDates WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM OpportunitySalesTemplate WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM OpportunityStageDetails WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM ProjectProgress WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM AlertEmailDTL WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM CommissionContacts WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM CustomerPartNumber WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM DivisionMasterPromotionHistory WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM EmbeddedCostDefaults WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE EA FROM ExtarnetAccounts EA INNER JOIN DivisionMaster DM ON EA.numDivisionID=DM.numDivisionID WHERE ISNULL(DM.numDomainID,0)=@numDomainID
		DELETE EA FROM ExtarnetAccounts EA INNER JOIN CompanyInfo CI ON EA.numCompanyId=CI.numCompanyId WHERE ISNULL(CI.numDomainID,0)=@numDomainID
		DELETE FROM ExtarnetAccounts WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM ImportActionItemReference WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM ImportOrganizationContactReference WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM PageElementDetail WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM ProjectsStageDetails WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM SiteBreadCrumb WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM SiteMenu WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM Vendor WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM VendorShipmentMethod WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM AccountingCharges WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM AccountTypeDetail WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM AlertConfig WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM ApprovalProcessItemsClassification WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM BankReconcileFileData WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM BizDocAlerts WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM CashCreditCardDetails WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM CFW_Fld_Master WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM ChartAccountOpening WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM CheckDetails_Old WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM COAShippingMapping WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM CommissionPayPeriod WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM CompanyAssociations WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM ContactTypeMapping WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM Correspondence WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM CustomReport WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM DashboardTemplateReports WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM DepositDetails_Old WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM DepositMaster WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM ErrorDetail WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM ExportDataSettings WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM GroupAuthorization WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM ImapUserDetails WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM ItemUOMConversion WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM ListDetailsName WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM PortalDashboardDtl WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM ProjectsMaster WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM PurchaseOrderFulfillmentSettings WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM RecordHistory WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM ReturnHeader WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM ShippingServiceAbbreviations WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM SitePages WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM SiteTemplates WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM StagePercentageDetails WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM StagePercentageDetailsTaskTimeLog WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM StyleSheets WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM SubscriberHstr WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM SurveyRespondentsMaster WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM TaxCountryConfi WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM TrackingCampaignData WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM AccountingIntegration WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM AccountTypeDetail_delete WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM ActivityDisplayConfiguration WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM ActivityFormAuthentication WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM AdditionalContactsInformation WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM AdditionalContactsInformation_TempDateFields WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM AddressDetails WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM AdvSearchCriteria WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM AdvSerViewConf WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM AlertDomainDtl WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM AlertHDR WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM Approval_transaction_log WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM ApprovalConfig WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM AssembledItem WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM Audit WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM AuthenticationGroupBackOrder WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM AuthenticationGroupMaster WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM AuthoritativeBizDocs WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM BankDetails WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM BankReconcileMaster WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM BankReconcileMatchRule WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM BankStatementHeader WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM BankStatementTransactions WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM BillHeader WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM BillingTerms WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM BillPaymentHeader WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM BizAPIErrorLog WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM BizDocAction WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM BizDocApprovalRule WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM BizDocAttachments WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM BizDocComission WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM BizDocFilter WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM BizDocStatusApprove WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM BizDocTemplate WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM BizFormWizardMasterConfiguration WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM Broadcast WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM CampaignMaster WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM CartItems WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM Cases WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM Cases_TempDateFields WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM Category WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM CategoryProfile WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM CFw_Grp_Master WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM CheckCompanyDetails WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM CheckHeader WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM ClassDetails WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM COACreditCardCharge WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM COARelationships WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM CommissionReports WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM CommissionRules WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM Communication WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM Communication_TempDateFields WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM CommunicationBackUP WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM CompanyAssets WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM Competitor WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM ContractManagement WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM Contracts WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM ContractsContact WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM Currency WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM CustomQueryReport WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM CustomReport26032011 WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM CustRptScheduler WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM DashboardTemplate WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM DefaultTabs WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM DeleteThis WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM DemandForecast WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM DemandForecastAnalysisPattern WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM DemandForecastDays WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM DivisionMaster_TempDateFields WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM DivisionMasterShippingConfiguration WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM DomainMarketplace WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM DomainSFTPDetail WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM DycCartFilters WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM DycField_Globalization WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM DycField_Validation WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM DycFieldColorScheme WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM DycFieldMaster WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM DycFieldMasterSynonym WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM DycFormConfigurationDetails WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM DycFormField_Mapping WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM DycFrmConfigBizDocsSumm WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM DycModuleMaster WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM DynamicFormAOIConf WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM DynamicFormConfigurationDetails WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM DynamicFormField_Validation WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM DynamicFormFieldMaster WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM DynamicFormMasterParam WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM DynamicFrmConfigBizDocsSumm WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM ECampaign WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM ECampaignAssignee WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM ECommerceCreditCardTransactionLog WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM eCommerceDTL WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM eCommercePaymentConfig WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM EDIHistory WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM EDIQueueLog WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM ElasticSearchBizCart WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM ElasticSearchModifiedRecords WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM EmailBroadcastConfiguration WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM EmailBroadcastConfiguration_old WHERE ISNULL(numDomainID,0)=@numDomainID

		deleteMoreEmailHistory:
		DELETE TOP (1000) FROM EmailHistory WHERE ISNULL(numDomainID,0)=@numDomainID
		IF @@ROWCOUNT != 0
			goto deleteMoreEmailHistory

		DELETE FROM EmailMaster WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM EmbeddedCostConfig WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM ExceptionLog WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM FieldRelationship WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM FinancialReport WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM FinancialYear WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM FixedAssetDetails WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM FollowUpHistory WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM Forecast WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM FormFieldGroupConfigurarion WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM ForReportsByAsItsType WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM ForReportsByTeam WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM ForReportsByTerritory WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM ForReportsByUser WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM GenealEntryAudit WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM GenericDocuments WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM GoogleContactGroups WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM HTMLFormURL WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM Import_File_Master WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM ImportApiOrder WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM InboxTree WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM InboxTreeSort WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM InitialListColumnConf WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM InventroyReportDTL WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM ItemAPI WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM ItemAttributes WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM ItemCurrencyPrice WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM ItemGroups WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM ItemImages WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM ItemMarketplaceMapping WHERE ISNULL(numDomainID,0)=@numDomainID	
		DELETE FROM ItemOptionAccAttr WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM LandedCostExpenseAccount WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM LandedCostVendors WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM LeadsubForms WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM ListDetails WHERE ISNULL(numDomainID,0)=@numDomainID AND ISNULL(constFlag,0) = 0
		DELETE FROM ListMaster WHERE ISNULL(numDomainID,0)=@numDomainID AND ISNULL(bitFixed,0) = 0
		DELETE FROM ListOrder WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM LuceneItemsIndex WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM MarketBudgetMaster WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM MassPurchaseFulfillmentConfiguration WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM MassSalesFulfillmentBatch WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM MassSalesFulfillmentConfiguration WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM MassSalesFulfillmentShippingConfig WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM MassSalesFulfillmentWM WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM MessageMaster WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM NameTemplate WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM OperationBudgetMaster WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM OppFulfillmentBizDocsStatusHistory WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM OpportunityAutomationQueue WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM OpportunityAutomationRules WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM OpportunityBizDocsDetails WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM OpportunityMaster_TempDateFields WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM OpportunityMasterAPI WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM OpportunityMasterShippingRateError WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM OpportunityOrderStatus WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM OrderAutoRule WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM OrganizationRatingRule WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM OverrideAssignTo WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM PackagingRules WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM PageLayoutDTL WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM ParentChildCustomFieldMap WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM PaymentGatewayDTLID WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM PayrollDetail WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM PayrollHeader WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM PayrollTracking WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM PortalBizDocs WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM PortalWorkflow WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM PriceBookRules WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM PricingNamesTable WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM ProcurementBudgetMaster WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM ProfileEmailGroup WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM ProjectsMaster_TempDateFields WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM PromotionOffer WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM PromotionOfferOrder WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM PurchaseIncentives WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM Ratings WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM RecImportConfg WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM RecordOrganizationChangeHistory WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM RecurrenceConfiguration WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM RecurrenceErrorLog WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM RecurringTemplate WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM RedirectConfig WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM RelationsDefaultFilter WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM ReportDashboard WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM ReportDashboardAllowedReports WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM ReportDashBoardSize WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM ReportKPIGroupListMaster WHERE ISNULL(numDomainID,0)=@numDomainID	
		DELETE FROM ReportListMaster WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM Resource WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM Review WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM RoutingLeads WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM RuleAction WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM RuleCondition WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM RuleMaster WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM Sales_process_List_Master WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM SalesFulfillmentConfiguration WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM SalesFulfillmentLog WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM SalesFulfillmentQueue WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM SalesOrderConfiguration WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM SalesOrderLineItemsPOLinking WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM SalesOrderRule WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM SalesReturnCommission WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM SavedSearch WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM ScheduledReportsGroup WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM ShareRecord WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM ShippingExceptions WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM ShippingFieldValues WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM ShippingLabelRuleMaster WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM ShippingPromotions WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM ShippingProviderForEComerce WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM ShippingRules WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM ShippingRuleStateList WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM ShippingService WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM ShippingServiceTypes WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM ShortCutBar WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM ShortCutGrpConf WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM ShortCutUsrCnf WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM SignatureDetail WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM SimilarItems WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM Sites WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM SMTPUserDetails WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM SolutionMaster WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM SpecificDocuments WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM StagePercentageDetails_TempDateFields WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM StagePercentageDetailsTask WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM StagePercentageDetailsTaskCommission WHERE ISNULL(numDomainID,0)=@numDomainID	
		DELETE FROM State WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM SurveyMaster WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM SurveyTemplate WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM TabDefault WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM TabMaster WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM TaxDetails WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM TaxItems WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM tblActionItemData WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM tblStageGradeDetails WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM TicklerListFilterConfiguration WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM TimeAndExpense WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM TimeAndExpenseCommission WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM TopicMaster WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM TopicMessageAttachments WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM TrackingVisitorsHDR WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM TrackNotification WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM TransactionHistory WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM TreeNavigationAuthorization WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM TreeNodeOrder WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM TrueCommerceLog WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM UnitPriceApprover WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM UniversalSMTPIMAP WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM UOM WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM UOMConversion WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM UserAccessedDTL WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM UserReportList WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM UserTeams WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM UserTerritory WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM Vendortemp WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM WareHouseDetails WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM WareHouseForSalesFulfillment WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM WareHouseItems_Tracking WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM WebAPIDetail WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM WebApiOppItemDetails WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM WebApiOrderDetails WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM WebApiOrderReports WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM WebService WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM WorkFlowAlertList WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM WorkFlowARAgingExecutionHistory WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM WorkflowAutomation WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM WorkFlowDateFieldExecutionHistory WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM WorkFlowQueue WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM WorkFlowMaster WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM WorkOrder WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM WorkSchedule WHERE ISNULL(numDomainID,0)=@numDomainID

		deleteMoreOpportunityMaster:
		DELETE TOP (1000) FROM OpportunityMaster WHERE ISNULL(numDomainID,0)=@numDomainID
		IF @@ROWCOUNT != 0
			goto deleteMoreOpportunityMaster

		deleteMoreWareHouseItems:
		DELETE TOP (1000) FROM WareHouseItems WHERE ISNULL(numDomainID,0)=@numDomainID
		IF @@ROWCOUNT != 0
			goto deleteMoreWareHouseItems

		deleteMoreItem:
		DELETE TOP (1000) FROM Item WHERE ISNULL(numDomainID,0)=@numDomainID
		IF @@ROWCOUNT != 0
			goto deleteMoreItem

		DELETE FROM Warehouses WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM WarehouseLocation WHERE ISNULL(numDomainID,0)=@numDomainID

		deleteMoreDivisionMaster:
		DELETE TOP (1000) FROM DivisionMaster WHERE ISNULL(numDomainID,0)=@numDomainID
		IF @@ROWCOUNT != 0
			goto deleteMoreDivisionMaster

		deleteMoreCompanyInfo:
		DELETE TOP (1000) CompanyInfo WHERE ISNULL(numDomainID,0)=@numDomainID
		IF @@ROWCOUNT != 0
			goto deleteMoreCompanyInfo

		DELETE FROM Chart_Of_Accounts WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM DivisionMasterBizDocEmail WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM UserMaster WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM Domain WHERE ISNULL(numDomainID,0)=@numDomainID
		DELETE FROM Subscribers WHERE numTargetDomainID=@numDomainID
	--COMMIT
	END TRY
	BEGIN CATCH
		DECLARE @ErrorMessage NVARCHAR(4000)
		DECLARE @ErrorNumber INT
		DECLARE @ErrorSeverity INT
		DECLARE @ErrorState INT
		DECLARE @ErrorLine INT
		DECLARE @ErrorProcedure NVARCHAR(200)

		--IF @@TRANCOUNT > 0
		--	ROLLBACK TRANSACTION;

		SELECT 
			@ErrorMessage = ERROR_MESSAGE(),
			@ErrorNumber = ERROR_NUMBER(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE(),
			@ErrorLine = ERROR_LINE(),
			@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

		RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);
	END CATCH
END
GO

