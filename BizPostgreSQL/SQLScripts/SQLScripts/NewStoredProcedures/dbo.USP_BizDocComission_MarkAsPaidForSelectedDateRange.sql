GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_BizDocComission_MarkAsPaidForSelectedDateRange')
DROP PROCEDURE USP_BizDocComission_MarkAsPaidForSelectedDateRange
GO
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[USP_BizDocComission_MarkAsPaidForSelectedDateRange]
(
    @numDomainId AS NUMERIC(9) = 0,
	@dtFromDate AS DATETIME,
	@dtToDate AS DATETIME
)
AS 
BEGIN 
	SET @dtFromDate = DATEADD(SECOND, DATEDIFF(SECOND, GETDATE(), GETUTCDATE()), @dtFromDate);
	SET @dtToDate =  DATEADD(SECOND, DATEDIFF(SECOND, GETDATE(), GETUTCDATE()), @dtToDate);

	UPDATE
		BDC
	SET 
		bitCommisionPaid = 1
    FROM    
		BizDocComission BDC
    INNER JOIN 
		OpportunityBizDocs OBD 
	ON 
		BDC.numOppBizDocId = OBD.numOppBizDocsId
    INNER JOIN 
		OpportunityMaster Opp 
	ON 
		OPP.numOppId = OBD.numOppId
    WHERE 
		OPP.numDomainID = @numDomainId
        AND BDC.numDomainID = @numDomainId
		AND OBD.[dtCreatedDate]  BETWEEN @dtFromDate AND @dtToDate
		AND ISNULL(BDC.bitCommisionPaid,0)=0
        AND OBD.bitAuthoritativeBizDocs = 1
END