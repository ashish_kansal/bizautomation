
GO
/****** Object:  StoredProcedure [dbo].[usp_UpdateStagePercentageDetails]    Script Date: 09/21/2010 12:00:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
----Created By Gangadhar    
    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ManageStageTask')
DROP PROCEDURE USP_ManageStageTask
GO
CREATE PROCEDURE [dbo].[USP_ManageStageTask]
@numDomainID as numeric(9)=0,    
@numStageDetailsId as numeric(18)=0,           
@vcTaskName as VARCHAR(500)='',           
@numHours as numeric(9)=0,           
@numMinutes as numeric(9)=0,           
@numAssignTo as numeric(18)=0,           
@numCreatedBy as numeric(18)=0,
@numOppId AS NUMERIC(18,0)=0,   
@numProjectId AS NUMERIC(18,0)=0,   
@numParentTaskId AS NUMERIC(18,0)=0,
@bitTaskClosed AS BIT=0,
@numTaskId AS NUMERIC=0,
@bitSavedTask AS BIT=0,
@bitAutoClosedTaskConfirmed As BIT=0,
@intTaskType AS INT=0,
@numWorkOrderID NUMERIC(18,0) = 0
as    
BEGIN   
	DECLARE @bitDefaultTask AS BIT=0
	IF(@numTaskId=0)
	BEGIN
		IF(@numOppId>0 OR @numProjectId>0 OR @numWorkOrderID > 0)
		BEGIN
			SET @bitDefaultTask=0
			SET @bitSavedTask=1
		END
		SET @bitTaskClosed =0
		INSERT INTO StagePercentageDetailsTask(
			numStageDetailsId, 
			vcTaskName, 
			numHours, 
			numMinutes, 
			numAssignTo, 
			numDomainID, 
			numCreatedBy, 
			dtmCreatedOn,
			numOppId,
			numProjectId,
			numParentTaskId,
			bitDefaultTask,
			bitTaskClosed,
			bitSavedTask,
			intTaskType,
			numWorkOrderId
		)
		VALUES(
			@numStageDetailsId,
			@vcTaskName,
			@numHours,
			@numMinutes,
			@numAssignTo,
			@numDomainId,
			@numCreatedBy,
			GETDATE(),
			@numOppId,
			@numProjectId,
			@numParentTaskId,
			@bitDefaultTask,
			@bitTaskClosed,
			@bitSavedTask,
			@intTaskType,
			@numWorkOrderID
		)
		UPDATE 
				StagePercentageDetails
			SET 
				intTaskType=@intTaskType
			WHERE
				numStageDetailsId=@numStageDetailsId
	END
	ELSE
	BEGIN
		DECLARE @vcRunningTaskName AS VARCHAR(500)=0
		DECLARE @numTaskValidatorId AS NUMERIC(18,0)=0
		DECLARE @numTaskSlNo AS NUMERIC(18,0)=0
		DECLARE @numStageOrder AS NUMERIC(18,0)=0
		DECLARE @numStagePercentageId AS NUMERIC(18,0)=0
		SELECT TOP 1 @numStageDetailsId=numStageDetailsId,@vcRunningTaskName=vcTaskName,@bitDefaultTask=bitDefaultTask,@numTaskSlNo=numOrder
		 FROM StagePercentageDetailsTask WHERE numTaskId=@numTaskId
		SELECT @numTaskValidatorId=numTaskValidatorId FROM Sales_process_List_Master WHERE Slp_Id=(SELECT TOP 1 slp_id FROM StagePercentageDetails WHERE numStageDetailsId=@numStageDetailsId)

		DECLARE @bitRunningDynamicMode AS BIT=0
		SELECT TOP 1 
			@bitRunningDynamicMode=bitRunningDynamicMode,
			@numStageOrder=numStageOrder ,
			@numStagePercentageId=numStagePercentageId
		FROM 
			StagePercentageDetails 
		WHERE 
			numStageDetailsId=@numStageDetailsId
		PRINT @bitRunningDynamicMode

		IF(@bitRunningDynamicMode=1 AND (@numOppId>0 OR @numProjectId>0 OR @numWorkOrderID > 0))
		BEGIN

			DECLARE @recordCount AS INT=1
			IF(@bitDefaultTask=1)
			BEGIN
				SET @recordCount=(SELECT COUNT(*) FROM StagePercentageDetailsTask WHERE numParentTaskId=@numTaskId AND numStageDetailsId=@numStageDetailsId)
				UPDATE 
					StagePercentageDetailsTask
				SET 
					bitSavedTask=0
				WHERE
					bitDefaultTask=1 AND numStageDetailsId=@numStageDetailsId
			
			END
			IF(@recordCount=0)
			BEGIN
				DELETE FROM 
					StagePercentageDetailsTask
				WHERE
					numParentTaskId IN(SELECT numTaskId FROM StagePercentageDetailsTask WHERE 
					bitDefaultTask=1 AND numStageDetailsId=@numStageDetailsId)
				DECLARE @numReferenceTaskId AS NUMERIC=0
				DECLARE @numReferenceStageDetailsId AS NUMERIC=0
				SELECT TOP 1 @numReferenceTaskId=numReferenceTaskId FROM StagePercentageDetailsTask WHERE numTaskId=@numTaskId
				SELECT TOP 1 @numReferenceStageDetailsId=numStageDetailsId FROM StagePercentageDetailsTask WHERE numTaskId=@numReferenceTaskId
				INSERT INTO StagePercentageDetailsTask(
				numStageDetailsId, 
				vcTaskName, 
				numHours, 
				numMinutes, 
				numAssignTo, 
				numDomainID, 
				numCreatedBy, 
				dtmCreatedOn,
				numOppId,
				numProjectId,
				numParentTaskId,
				bitDefaultTask,
				bitTaskClosed,
				bitSavedTask,
				numReferenceTaskId,
				numWorkOrderId
			)
			SELECT 
				@numStageDetailsId, 
				vcTaskName, 
				numHours, 
				numMinutes, 
				numAssignTo, 
				@numDomainID, 
				@numCreatedBy, 
				GETDATE(),
				@numOppId,
				@numProjectId,
				@numTaskId,
				0,
				0,
				1, 
				numTaskId,
				@numWorkOrderID
			FROM
				StagePercentageDetailsTask AS ST
			WHERE
				ST.numTaskId IN(
				SELECT numSecondaryListItemID FROM FieldRelationshipDTL WHERE 
				numPrimaryListItemID=@numReferenceTaskId AND
				numFieldRelID=(SELECT TOP 1 numFieldRelID FROM FieldRelationship WHERE numPrimaryListID=@numReferenceStageDetailsId AND ISNULL(bitTaskRelation,0)=1)
				) AND ST.numOppId=0 AND ST.numProjectId=0 AND ST.numDomainId=@numDomainId 					
			END

	
					
		END
		IF((@bitAutoClosedTaskConfirmed=1 AND (@numOppId>0 OR @numProjectId>0 OR @numWorkOrderID>0) AND @numTaskValidatorId=2 AND @bitTaskClosed=1) OR ((@numOppId>0 OR @numProjectId>0 OR @numWorkOrderID>0) AND @numTaskValidatorId=1 AND @bitTaskClosed=1))
		BEGIN
			IF(ISNULL(@numTaskSlNo,0)>0)
			BEGIN
				UPDATE
					StagePercentageDetailsTask
				SET
					bitTaskClosed=1,
					numCreatedBy=@numCreatedBy,
					dtmUpdatedOn=GETDATE()
				WHERE
					numStageDetailsId=@numStageDetailsId AND numOrder<@numTaskSlNo
			END
			ELSE
			BEGIN
				UPDATE
					StagePercentageDetailsTask
				SET
					bitTaskClosed=1,
					numCreatedBy=@numCreatedBy,
					dtmUpdatedOn=GETDATE()
				WHERE
					numStageDetailsId=@numStageDetailsId AND numTaskId<@numTaskId
			END
		END
		IF(((@numOppId>0 OR @numProjectId>0 OR @numWorkOrderID>0) AND @numTaskValidatorId=3 AND @bitTaskClosed=1) OR (@bitAutoClosedTaskConfirmed=1 AND (@numOppId>0 OR @numProjectId>0 OR @numWorkOrderID>0) AND @numTaskValidatorId=4 AND @bitTaskClosed=1))
		BEGIN
			IF(ISNULL(@numTaskSlNo,0)>0)
			BEGIN
				UPDATE
					StagePercentageDetailsTask
				SET
					bitTaskClosed=1,
					numCreatedBy=@numCreatedBy,
					dtmUpdatedOn=GETDATE()
				WHERE
					numStageDetailsId=@numStageDetailsId AND numOrder<@numTaskSlNo
			END
			ELSE
			BEGIN
				UPDATE
					StagePercentageDetailsTask
				SET
					bitTaskClosed=1,
					numCreatedBy=@numCreatedBy,
					dtmUpdatedOn=GETDATE()
				WHERE
					numStageDetailsId=@numStageDetailsId AND numTaskId<@numTaskId
			END
			IF(ISNULL(@numStageOrder,0)>0)
			BEGIN
				UPDATE
					StagePercentageDetailsTask
				SET
					bitTaskClosed=1,
					numCreatedBy=@numCreatedBy,
					dtmUpdatedOn=GETDATE()
				WHERE	
					numOppId=@numOppId AND
					numProjectId=@numProjectId AND
					ISNULL(numWorkOrderId,0)=@numWorkOrderID AND
					numStageDetailsId IN(SELECT numStageDetailsId FROM StagePercentageDetails WHERE numStageOrder<@numStageOrder AND numStagePercentageId=@numStagePercentageId AND (numOppId=@numOppId AND numProjectId=@numProjectId AND ISNULL(numWorkOrderId,0)=@numWorkOrderID))
			END
			ELSE
			BEGIN
				UPDATE
					StagePercentageDetailsTask
				SET
					bitTaskClosed=1,
					numCreatedBy=@numCreatedBy,
					dtmUpdatedOn=GETDATE()
				WHERE	
					numOppId=@numOppId AND
					numProjectId=@numProjectId AND
					ISNULL(numWorkOrderId,0)=@numWorkOrderID AND
					numStageDetailsId IN(SELECT numStageDetailsId FROM StagePercentageDetails WHERE numStageDetailsId<@numStageDetailsId AND numStagePercentageId=@numStagePercentageId AND numOppId=@numOppId AND numProjectId=@numProjectId AND ISNULL(numWorkOrderId,0)=@numWorkOrderID)
			END
			UPDATE
					StagePercentageDetailsTask
				SET
					bitTaskClosed=1,
					numCreatedBy=@numCreatedBy,
					dtmUpdatedOn=GETDATE()
				WHERE	
					numOppId=@numOppId AND
					numProjectId=@numProjectId AND
					ISNULL(numWorkOrderId,0)=@numWorkOrderID AND
					numStageDetailsId IN(SELECT numStageDetailsId FROM StagePercentageDetails WHERE numStagePercentageId<@numStagePercentageId AND numOppId=@numOppId AND numProjectId=@numProjectId AND ISNULL(numWorkOrderId,0)=@numWorkOrderID)
		END
	
		UPDATE 
				StagePercentageDetailsTask
			SET 
				numAssignTo=@numAssignTo
			WHERE
				numTaskId=@numTaskId
		IF(@numHours>0)
		BEGIN
			UPDATE 
				StagePercentageDetailsTask
			SET 
				numHours=@numHours
			WHERE
				numTaskId=@numTaskId
		END
		IF(@numMinutes>0)
		BEGIN
			UPDATE 
				StagePercentageDetailsTask
			SET 
				numMinutes=@numMinutes
			WHERE
				numTaskId=@numTaskId
		END
		IF(LEN(@vcTaskName)>0)
		BEGIN
			UPDATE 
				StagePercentageDetailsTask
			SET 
				vcTaskName=@vcTaskName
			WHERE
				numTaskId=@numTaskId
		END
		IF(@intTaskType>0)
		BEGIN
			
			UPDATE 
				StagePercentageDetails
			SET 
				intTaskType=@intTaskType
			WHERE
				numStageDetailsId=(SELECT TOP 1 numStageDetailsId FROM StagePercentageDetailsTask WHERE numTaskId=@numTaskId)
		END
		UPDATE 
			StagePercentageDetailsTask
		SET
			bitSavedTask=@bitSavedTask,
			bitTaskClosed=@bitTaskClosed,
			numCreatedBy=@numCreatedBy,
			dtmUpdatedOn=GETDATE()
		WHERE
			numTaskId=@numTaskId
	END
	IF(@numOppId>0 OR @numProjectId>0 OR @numWorkOrderID > 0)
	BEGIN
		DECLARE @intTotalProgress AS INT =0
		DECLARE @intTotalTaskCount AS INT=0
		DECLARE @intTotalTaskClosed AS INT =0
		SET @intTotalTaskCount=(SELECT COUNT(*) FROM StagePercentageDetailsTask WHERE (numOppId=@numOppId AND numProjectId=@numProjectId AND ISNULL(numWorkOrderId,0)=@numWorkOrderID) AND bitSavedTask=1)
		SET @intTotalTaskClosed=(SELECT COUNT(*) FROM StagePercentageDetailsTask WHERE (numOppId=@numOppId AND numProjectId=@numProjectId AND ISNULL(numWorkOrderId,0)=@numWorkOrderID) AND bitSavedTask=1 AND bitTaskClosed=1)
		IF(@intTotalTaskCount>0 AND @intTotalTaskClosed>0)
		BEGIN
			SET @intTotalProgress=ROUND(((@intTotalTaskClosed*100)/@intTotalTaskCount),0)
		END
		IF((SELECT COUNT(*) FROM ProjectProgress WHERE (ISNULL(numOppId,0)=@numOppId AND ISNULL(numProId,0)=@numProjectId AND ISNULL(numWorkOrderId,0)=@numWorkOrderID)AND numDomainId=@numDomainID)>0)
		BEGIN
			UPDATE ProjectProgress SET intTotalProgress=@intTotalProgress WHERE (ISNULL(numOppId,0)=@numOppId AND ISNULL(numProId,0)=@numProjectId AND ISNULL(numWorkOrderId,0)=@numWorkOrderID) AND numDomainId=@numDomainID
		END
		ELSE
		BEGIN
			IF(@numOppId=0)
			BEGIN
				SET @numOppId = NULL
			END
			IF(@numProjectId=0)
			BEGIN
				SET @numProjectId = NULL
			END
			IF (@numWorkOrderID = 0)
			BEGIN
				SET @numWorkOrderID = NULL
			END
			INSERT INTO ProjectProgress
			(
				numOppId,
				numProId,
				numWorkOrderID,
				numDomainId,
				intTotalProgress
			)VALUES(
				@numOppId,
				@numProjectId,
				@numWorkOrderID,
				@numDomainID,
				@intTotalProgress
			)
		END
	END
END 
