GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='Usp_GetBillingTerms')
	DROP PROCEDURE Usp_GetBillingTerms
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[Usp_GetBillingTerms]
(
	@numTermsID	INT
	,@numDomainID NUMERIC(18,0)
	,@ClientTimeZoneOffset INT
	,@tintApplyTo TINYINT
)
AS
BEGIN
	SELECT numTermsID,vcTerms,tintApplyTo,numNetDueInDays,numDiscount,numDiscountPaidInDays,numListItemID,numDomainID, 
	(CAST(numTermsID AS VARCHAR(10)) + '~' + CAST(numNetDueInDays AS VARCHAR(10))) AS [vcValue],
	CASE WHEN (SELECT COUNT(*) FROM dbo.OpportunityMaster WHERE dbo.OpportunityMaster.intBillingDays = numTermsID AND [OpportunityMaster].[numDomainId] = @numDomainID ) > 0 THEN 1 ELSE 0 END AS [IsUsed],
	ISNULL(numInterestPercentage,0) AS [numInterestPercentage],
	ISNULL(numInterestPercentageIfPaidAfterDays,0) AS [numInterestPercentageIfPaidAfterDays],
	dbo.fn_GetContactName(numCreatedBy) + ' ' + CONVERT(VARCHAR(20),dateadd(MINUTE,-@ClientTimeZoneOffset,[dtCreatedDate])) AS CreatedBy,
    dbo.fn_GetContactName(numModifiedBy) + ' ' + CONVERT(VARCHAR(20),dateadd(MINUTE,-@ClientTimeZoneOffset,[dtModifiedDate])) AS ModifiedBy
	FROM dbo.BillingTerms
	WHERE (numTermsID = @numTermsID OR @numTermsID = 0)
	AND numDomainID = @numDomainID
	AND ISNULL(bitActive,0) = 1
	AND (ISNULL(@tintApplyTo,0) = 0 OR tintApplyTo = @tintApplyTo)
END
GO