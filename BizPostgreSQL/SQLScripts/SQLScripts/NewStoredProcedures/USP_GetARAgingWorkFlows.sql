GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_GetARAgingWorkFlows' ) 
    DROP PROCEDURE USP_GetARAgingWorkFlows
GO
CREATE PROCEDURE USP_GetARAgingWorkFlows
AS 
BEGIN 
		
		SELECT * FROM WorkFlowMaster WFM		
		WHERE WFM.intDays <> 0 AND ISNULL(WFM.vcDateField,'') = '' 
			AND WFM.bitActive = 1 AND WFM.tintWFTriggerOn = 6

END


