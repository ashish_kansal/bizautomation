-- Created by Sojan        
-- [USP_GetChartAcntDetails] @numParentDomainID=72,@dtFromDate='2007-01-01 00:00:00:000',@dtToDate='2009-09-25 00:00:00:000'                            
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetMultiStock')
DROP PROCEDURE USP_GetMultiStock
GO
CREATE PROCEDURE [dbo].[USP_GetMultiStock]                                   
@numParentDomainID as numeric(9),                        
@dtFromDate as datetime,                      
@dtToDate as datetime,
@numSubscriberID as int,
@RollUp as bit=0
                                   
As                                      
Begin    
DECLARE @numFinYear INT;
DECLARE @dtFinYearFrom datetime;


CREATE TABLE #TLGroup (numDomainId numeric(9),vcDomainCode varchar(50),vcDomainName varchar(150), numAccountId numeric(9),vcAccountName varchar(250),
numParntAcntTypeID numeric(9),vcAccountDescription varchar(250),
vcAccountCode varchar(50),Opening DECIMAL(20,5),Debit DECIMAL(20,5),Credit DECIMAL(20,5));


INSERT INTO  #TLGroup


SELECT AD.numDomainId,dn.vcDomainCode,dn.vcDomainName,AD.numAccountTypeId,AD.vcAccountType,numParentID,'',AD.vcAccountCode,
isnull((SELECT sum(isnull(monOpening,0)) from CHARTACCOUNTOPENING CAO,CHART_OF_ACCOUNTS COA WHERE
numFinYearId in (SELECT numFinYearId FROM FINANCIALYEAR WHERE dtPeriodFrom <= @dtFromDate AND  
dtPeriodTo >=@dtFromDate AND FINANCIALYEAR.numDomainId= AD.numDomainId)
 AND CAO.numDomainID=AD.numDomainID AND 
CAO.numAccountId=COA.numAccountId  AND 
COA.vcAccountCode like AD.vcAccountCode + '%' ),0) +

ISNULL((SELECT sum(Debit-Credit) FROM view_journal VJ,FINANCIALYEAR FN
WHERE VJ.numDomainId=AD.numDomainId AND
	FN.numDomainID=VJ.numDomainID AND
	VJ.coavcAccountCode like AD.vcAccountCode  + '%' AND
	FN.dtPeriodFrom <= @dtFromDate AND  
	FN.dtPeriodTo >=@dtFromDate and 
	datEntry_Date BETWEEN FN.dtPeriodFrom  AND  @dtFromDate-1),0) AS OPENING,

ISNULL((SELECT sum(Debit) FROM view_journal VJ
WHERE VJ.numDomainId=AD.numDomainId AND
	VJ.coavcAccountCode like AD.vcAccountCode  + '%' AND
	datEntry_Date BETWEEN  @dtFromDate AND @dtToDate ),0) as DEBIT,

ISNULL((SELECT sum(Credit) FROM view_journal VJ
WHERE VJ.numDomainId=AD.numDomainId AND
	VJ.coavcAccountCode like AD.vcAccountCode  + '%' AND
	datEntry_Date BETWEEN  @dtFromDate AND @dtToDate ),0) as CREDIT
FROM AccountTypeDetail AD,
	(select * from Domain DN where dn.numSubscriberID=@numSubscriberID)DN,
	(select * from Domain DNN where dnn.numSubscriberID=@numSubscriberID) DNN
WHERE AD.numDomainId=DNN.numDomainId AND 
	DNN.numDomainId =dn.numDomainId AND 
	DN.numDomainId =@numParentDomainID AND 
	(AD.vcAccountCode like '01010104%')
	

UNION


SELECT AD.numDomainId,dn.vcDomainCode,dn.vcDomainName,AD.numAccountTypeId,AD.vcAccountType,numParentID,'',AD.vcAccountCode,
isnull((SELECT sum(isnull(monOpening,0)) from CHARTACCOUNTOPENING CAO,CHART_OF_ACCOUNTS COA WHERE
numFinYearId in (SELECT numFinYearId FROM FINANCIALYEAR WHERE dtPeriodFrom <= @dtFromDate AND  
dtPeriodTo >=@dtFromDate AND FINANCIALYEAR.numDomainId= AD.numDomainId)
 AND CAO.numDomainID=AD.numDomainID AND 
CAO.numAccountId=COA.numAccountId  AND 
COA.vcAccountCode like AD.vcAccountCode + '%' ),0) +

ISNULL((SELECT sum(Debit-Credit) FROM view_journal VJ,FINANCIALYEAR FN
WHERE VJ.numDomainId=AD.numDomainId AND
	FN.numDomainID=VJ.numDomainID AND
	VJ.coavcAccountCode like AD.vcAccountCode + '%' AND
	FN.dtPeriodFrom <= @dtFromDate AND  
	FN.dtPeriodTo >=@dtFromDate and 
	datEntry_Date BETWEEN FN.dtPeriodFrom  AND  @dtFromDate-1),0) AS OPENING,

ISNULL((SELECT sum(Debit) FROM view_journal VJ
WHERE VJ.numDomainId=AD.numDomainId AND
	VJ.coavcAccountCode like AD.vcAccountCode + '%' AND
	datEntry_Date BETWEEN  @dtFromDate AND @dtToDate ),0) as DEBIT,

ISNULL((SELECT sum(Credit) FROM view_journal VJ
WHERE VJ.numDomainId=AD.numDomainId AND
	VJ.coavcAccountCode like AD.vcAccountCode + '%' AND
	datEntry_Date BETWEEN  @dtFromDate AND @dtToDate ),0) as CREDIT
FROM AccountTypeDetail AD,
	(select * from Domain DN where dn.numSubscriberID=@numSubscriberID)DN,
	(select * from Domain DNN where dnn.numSubscriberID=@numSubscriberID) DNN
WHERE AD.numDomainId=DNN.numDomainId AND 
	DNN.vcDomainCode like DN.vcDomainCode + '%'  AND 
	DN.numParentDomainId =@numParentDomainID   AND 
	(AD.vcAccountCode like '01010104%')

if @Rollup=0
	begin
		select vcDomainName as Company, sum(isnull(OPENING,0)) + SUM(isnull(DEBIT,0)) - SUM(ISNULL(CREDIT,0)) as Amount from #TLGroup
		group by vcDomainName
	end
else if @Rollup=1
	begin
		select 'Stock Roll-up' as Company, sum(isnull(OPENING,0)) + SUM(isnull(DEBIT,0)) - SUM(ISNULL(CREDIT,0)) as Amount from #TLGroup		
	end
drop table #TLGroup;

End


