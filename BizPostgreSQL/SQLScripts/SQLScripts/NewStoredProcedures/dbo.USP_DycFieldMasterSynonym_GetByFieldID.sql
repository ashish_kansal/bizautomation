GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_DycFieldMasterSynonym_GetByFieldID')
DROP PROCEDURE dbo.USP_DycFieldMasterSynonym_GetByFieldID
GO
CREATE PROCEDURE [dbo].[USP_DycFieldMasterSynonym_GetByFieldID]
(
	@numDomainID NUMERIC(18,0)
	,@numFieldID NUMERIC(18,0)
	,@bitCustomField BIT
)
AS 
BEGIN
	SELECT
		ID
		,numFieldID
		,vcSynonym
		,bitDefault
	FROM
		DycFieldMasterSynonym
	WHERE
		(numDomainID=@numDomainID OR bitDefault=1)
		AND numFieldID=@numFieldID
		AND ISNULL(bitCustomField,0) = @bitCustomField
	ORDER BY
		vcSynonym
END
GO