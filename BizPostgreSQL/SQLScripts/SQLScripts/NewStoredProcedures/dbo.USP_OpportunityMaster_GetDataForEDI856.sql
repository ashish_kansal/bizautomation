SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_OpportunityMaster_GetDataForEDI856')
DROP PROCEDURE dbo.USP_OpportunityMaster_GetDataForEDI856
GO
CREATE PROCEDURE [dbo].[USP_OpportunityMaster_GetDataForEDI856]
	@numDomainID NUMERIC(18,0)
	,@numOppID NUMERIC(18,0)
AS 
BEGIN
	DECLARE @TableShippingReport TABLE
	(
		numShippingReportId NUMERIC(18,0)
	)

	DECLARE @TableShippingBox TABLE
	(
		numBoxID NUMERIC(18,0)
		,vcTrackingNumber VARCHAR(300)
	)

	INSERT INTO @TableShippingReport
	(
		numShippingReportId
	)
	SELECT
		numShippingReportId
	FROM
		ShippingReport
	WHERE
		ShippingReport.numDomainID=@numDomainID
		AND ShippingReport.numOppID=@numOppID
		AND ISNULL(ShippingReport.bitSendToTP2,0) = 0
		AND 1 = (CASE WHEN (SELECT COUNT(*) FROM ShippingBox WHERE numShippingReportId=ShippingReport.numShippingReportId AND vcShippingLabelImage IS NOT NULL AND vcTrackingNumber IS NOT NULL) > 0 THEN 1 ELSE 0 END)

	INSERT INTO @TableShippingBox
	(
		numBoxID
		,vcTrackingNumber
	)
	SELECT 
		numBoxID
		,vcTrackingNumber
	FROM 
		ShippingBox 
	WHERE 
		numShippingReportId IN (SELECT numShippingReportId FROM @TableShippingReport)
		AND ISNULL(vcTrackingNumber,'') <> ''

	SELECT
		numShippingReportId
		,CONVERT(CHAR(8), ShippingReport.dtCreateDate, 112) vcShipDate
		,ISNULL(ShippingReport.vcToName,'') vcToName
		,ISNULL(ShippingReport.vcToAddressLine1,'') vcToAddressLine1
		,ISNULL(ShippingReport.vcToAddressLine2,'') vcToAddressLine2
		,ISNULL(ShippingReport.vcToCity,'') vcToCity
		,ISNULL(dbo.fn_GetState(ShippingReport.vcToState),'') vcToState
		,ISNULL(ShippingReport.vcToZip,'') vcToZip
		,ISNULL(dbo.fn_GetListName(ShippingReport.vcToCountry,0),'') vcToCountry
		,ISNULL(ShippingReport.vcFromName,'') vcFromName
		,ISNULL(ShippingReport.vcFromAddressLine1,'') vcFromAddressLine1
		,ISNULL(ShippingReport.vcFromAddressLine2,'') vcFromAddressLine2
		,ISNULL(ShippingReport.vcFromCity,'') vcFromCity
		,ISNULL(dbo.fn_GetState(ShippingReport.vcFromState),'') vcFromState
		,ISNULL(ShippingReport.vcFromZip,'') vcFromZip
		,ISNULL(dbo.fn_GetListName(ShippingReport.vcFromCountry,0),'') vcFromCountry
		,(CASE numShippingCompany WHEN 88 THEN 'UPS' WHEN 90 THEN 'USPS' WHEN 91 THEN 'FedEx' ELSE '' END) vcShipVia
		,CAST(ROUND(((SELECT SUM(ISNULL(fltTotalWeight,0) * ISNULL(intBoxQty,0)) FROM dbo.ShippingReportItems WHERE numShippingReportId = ShippingReport.numShippingReportId)),2) AS DECIMAL(9, 2)) AS fltTotalRegularWeight
		,CAST(ROUND((SELECT SUM(ISNULL(fltDimensionalWeight,0)) FROM dbo.ShippingBox WHERE numShippingReportId = ShippingReport.numShippingReportId), 2) AS DECIMAL(9, 2)) AS fltTotalDimensionalWeight
		,ISNULL(vcCustomerPO#,'') vcCustomerPO#
	FROM
		ShippingReport
	INNER JOIN
		OpportunityMaster
	ON
		ShippingReport.numOppID = OpportunityMaster.numOppId
	WHERE
		numShippingReportId IN (SELECT numShippingReportId FROM @TableShippingReport)


	SELECT 
		numBoxID
		,ISNULL(vcTrackingNumber,'')
		,numShippingReportId
		,fltHeight
		,fltLength
		,fltWidth
		,fltTotalWeight
		,BoxItems.Qty
	FROM 
		ShippingBox 
	OUTER APPLY
	(
		SELECT 
			SUM(ISNULL(ShippingReportItems.intBoxQty,0) * dbo.fn_UOMConversion(Item.numBaseUnit,OpportunityItems.numItemCode,@numDomainID,OpportunityItems.numUOMId)) AS Qty
		FROM 
			ShippingReportItems
		INNER JOIN
			OpportunityBizDocItems 
		ON
			ShippingReportItems.numOppBizDocItemID = OpportunityBizDocItems.numOppBizDocItemID
		INNER JOIN
			OpportunityItems
		ON
			OpportunityBizDocItems.numOppItemID = OpportunityItems.numoppitemtCode
		INNER JOIN
			Item
		ON
			OpportunityItems.numItemCode = Item.numItemCode
		WHERE 
			numBoxID=ShippingBox.numBoxID
	) BoxItems
	WHERE 
		numBoxID IN (SELECT numBoxID FROM @TableShippingBox)
		
	SELECT 
		numBoxID
		,CASE 
			WHEN ISNULL(Item.numItemGroup,0) > 0 AND ISNULL(Item.bitMatrix,0) = 0
			THEN 
				(CASE WHEN LEN(ISNULL(WareHouseItems.vcWHSKU,'')) > 0 THEN WareHouseItems.vcWHSKU ELSE ISNULL(Item.vcSKU,'') END)
			ELSE
				(CASE WHEN LEN(ISNULL(OpportunityItems.vcSKU,'')) > 0 THEN OpportunityItems.vcSKU ELSE ISNULL(Item.vcSKU,'') END)
		END vcSKU
		,CASE 
			WHEN ISNULL(Item.numItemGroup,0) > 0 AND ISNULL(Item.bitMatrix,0) = 0
			THEN 
				(CASE WHEN LEN(ISNULL(WareHouseItems.vcBarCode,'')) > 0 THEN WareHouseItems.vcBarCode ELSE ISNULL(Item.numBarCodeId,'') END)
			ELSE
				ISNULL(Item.numBarCodeId,'')
		END vcUPC
		,CAST((dbo.fn_UOMConversion(ISNULL(Item.numBaseUnit,0),OpportunityItems.numItemCode,@numDomainID, ISNULL(OpportunityItems.numUOMId, 0))* OpportunityItems.numUnitHour) AS NUMERIC(18, 2)) AS numOrderedUnits
		,ISNULL(UOM.vcUnitName,'Units') vcUOM
		,(CASE 
			WHEN charItemType='P' THEN 'Inventory Item'
			WHEN charItemType='S' THEN 'Service' 
			WHEN charItemType='A' THEN 'Accessory' 
			WHEN charItemType='N' THEN 'Non-Inventory Item' 
		END) AS vcItemType
		,CAST((dbo.fn_UOMConversion(ISNULL(Item.numBaseUnit,0),OpportunityItems.numItemCode,@numDomainID, ISNULL(OpportunityItems.numUOMId, 0))* ISNULL(ShippingReportItems.intBoxQty,0)) AS NUMERIC(18, 2)) AS numShippedUnits
		,OpportunityItems.numoppitemtCode 
	FROM 
		ShippingReportItems
	INNER JOIN
		OpportunityBizDocItems 
	ON
		ShippingReportItems.numOppBizDocItemID = OpportunityBizDocItems.numOppBizDocItemID
	INNER JOIN
		OpportunityItems
	ON
		OpportunityBizDocItems.numOppItemID = OpportunityItems.numoppitemtCode
	INNER JOIN
		Item
	ON
		OpportunityItems.numItemCode = Item.numItemCode
	LEFT JOIN
		WareHouseItems
	ON
		OpportunityItems.numWarehouseItmsID = WareHouseItems.numWareHouseItemID
	LEFT JOIN 
		UOM 
	ON
		OpportunityItems.numUOMId = UOM.numUOMId
	WHERE 
		numBoxID IN (SELECT numBoxID FROM @TableShippingBox)

END
GO