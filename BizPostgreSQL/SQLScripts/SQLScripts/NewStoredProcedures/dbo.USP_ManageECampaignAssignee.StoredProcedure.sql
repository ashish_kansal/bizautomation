
GO
/****** Object:  StoredProcedure [dbo].[USP_ManageECampaignAssignee]    Script Date: 06/04/2009 15:14:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by chintan
-- exec USP_ManageECampaignAssignee @numECampaignAssigneeID=2,@numECampaignID=0,@numEmailGroupID=0,@dtStartDate=NULL,@numDomainID=0,@numUserCntID=0,@bitMode=1
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ManageECampaignAssignee')
DROP PROCEDURE USP_ManageECampaignAssignee
GO
CREATE PROCEDURE [dbo].[USP_ManageECampaignAssignee]
          @numECampaignAssigneeID NUMERIC(9)  = 0,
          @numECampaignID         NUMERIC(9),
          @numEmailGroupID        NUMERIC(9),
          @dtStartDate            DATETIME=null,
          @numDomainID            AS NUMERIC(9),
          @numUserCntID           NUMERIC(9),
          @bitMode BIT = 0
AS

IF @bitMode = 1 --delete record
BEGIN
PRINT 'inside'
	DELETE FROM [ECampaignAssignee] WHERE 
	[numECampaignAssigneeID] = @numECampaignAssigneeID AND [numDomainID]= @numDomainID;
	SELECT 0;
	RETURN 
END 
ELSE 
BEGIN
PRINT 'insidef'
  IF @numECampaignAssigneeID = 0
    BEGIN
      INSERT INTO [ECampaignAssignee]
                 ([numECampaignID],
                  [numEmailGroupID],
                  [dtStartDate],
                  [numDomainID],
                  [numCreatedBy],
                  [dtCreateDate],
                  [numModifiedBy],
                  [dtModifiedDate])
      VALUES     (@numECampaignID,
                  @numEmailGroupID,
                  @dtStartDate,
                  @numDomainID,
                  @numUserCntID,
                  GETUTCDATE(),
                  @numUserCntID,
                  GETUTCDATE())
      SET @numECampaignAssigneeID = SCOPE_IDENTITY()
    END
  ELSE
    BEGIN
      UPDATE [ECampaignAssignee]
      SET    [numECampaignID] = @numECampaignID,
             [numEmailGroupID] = @numEmailGroupID,
             [dtStartDate] = @dtStartDate,
             [numModifiedBy] = @numUserCntID,
             [dtModifiedDate] = GETUTCDATE()
      WHERE  numECampaignAssigneeID = @numECampaignAssigneeID
    END
  SELECT @numECampaignAssigneeID
END