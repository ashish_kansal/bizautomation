SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON            
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_OpportunityBizDocs_CheckIfItemsPendingToAddByBizDocID')
DROP PROCEDURE USP_OpportunityBizDocs_CheckIfItemsPendingToAddByBizDocID
GO
CREATE PROCEDURE [dbo].[USP_OpportunityBizDocs_CheckIfItemsPendingToAddByBizDocID]              
	@numOppID NUMERIC(18,0)
	,@numBizDocID NUMERIC(18,0)
AS             
BEGIN
	IF (SELECT 
			COUNT(*) 
		FROM 
		(
			SELECT
				OI.numoppitemtCode,
				ISNULL(OI.numUnitHour,0) AS OrderedQty,
				ISNULL(TempInvoice.AddedQty,0) AS AddedQty
			FROM
				OpportunityItems OI
			INNER JOIN
				Item I
			ON
				OI.numItemCode = I.numItemCode
			OUTER APPLY
			(
				SELECT
					SUM(OpportunityBizDocItems.numUnitHour) AS AddedQty
				FROM
					OpportunityBizDocs
				INNER JOIN
					OpportunityBizDocItems 
				ON
					OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
				WHERE
					OpportunityBizDocs.numOppId = @numOppID
					AND OpportunityBizDocItems.numOppItemID = OI.numoppitemtCode
					AND OpportunityBizDocs.numBizDocId = @numBizDocID
			) AS TempInvoice
			WHERE
				OI.numOppID = @numOppID
		) X
		WHERE
			X.OrderedQty <> X.AddedQty) > 0
	BEGIN
		SELECT 1
	END
	ELSE
	BEGIN
		SELECT 0
	END
END

	