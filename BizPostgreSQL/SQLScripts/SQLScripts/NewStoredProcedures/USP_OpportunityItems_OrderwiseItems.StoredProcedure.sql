GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_OpportunityItems_OrderwiseItems' ) 
    DROP PROCEDURE USP_OpportunityItems_OrderwiseItems
GO
-- =============================================        
-- Author:  <Author,,Sachin Sadhu>        
-- Create date: <Create Date,,30Dec2013>        
-- Description: <Description,,To get Items Customer wise n order wise>        
-- Module:Accounts/Items tab        
-- ============================================= 
CREATE PROCEDURE [dbo].[USP_OpportunityItems_OrderwiseItems]         
 -- Add the parameters for the stored procedure here        
    @numDomainId NUMERIC(9) ,    
    @numDivisionId NUMERIC(9) ,    
    @keyword VARCHAR(50) ,    
    @startDate AS DATETIME =NULL,    
    @endDate AS DATETIME =NULL,    
    @imode AS INT ,    
    @CurrentPage INT = 0 ,    
    @PageSize INT = 0 ,    
    @TotRecs INT = 0 OUTPUT    
AS     
    BEGIN        
 -- SET NOCOUNT ON added to prevent extra result sets from        
 -- interfering with SELECT statements.        
        SET NOCOUNT ON;        
                
	DECLARE @numCompanyID NUMERIC(18,0)
	SELECT @numCompanyID = numCompanyID FROM DivisionMaster where numDivisionID=@numDivisionId and numDomainID = @numDomainId 

    -- Insert statements for procedure here        
        IF ( @imode = 1 )     
            BEGIN        
                   
                CREATE TABLE #tempItems    
                    (    
                      Units FLOAT,    
                      BasePrice DECIMAL(18,2) ,    
                      ItemName VARCHAR(300) ,
					  vcSKU VARCHAR(300),
					  vcUPC VARCHAR(300),    
                      base DECIMAL(18,2) ,    
                      ChargedAmount DECIMAL(18,2) ,    
                      ORDERID NUMERIC(9) ,    
                      ModelId VARCHAR(200) ,    
                      OrderDate VARCHAR(30) ,    
                      tintOppType TINYINT ,    
                      tintOppStatus TINYINT ,    
                      numContactId NUMERIC ,    
                      numDivisionId NUMERIC ,    
                      OppId NUMERIC ,    
                      OrderNo VARCHAR(300)   ,
					  CustomerPartNo  VARCHAR(300),
					  numItemCode NUMERIc(18,0)
                    )        
                                
                INSERT  INTO #tempItems    
                        SELECT  OI.numUnitHour AS Units ,    
                                OI.monPrice AS BasePrice ,    
                                OI.vcItemName AS ItemName ,
								CASE WHEN ISNULL(I.numItemGroup,0) > 0 AND ISNULL(bitMatrix,0)=0 THEN ISNULL(WI.vcWHSKU,ISNULL(I.vcSKU,''))ELSE I.vcSKU END,
								CASE WHEN ISNULL(I.numItemGroup,0) > 0 AND ISNULL(bitMatrix,0)=0 THEN ISNULL(WI.vcBarCode,ISNULL(I.numBarCodeId,''))ELSE I.numBarCodeId END,
                                OI.monAvgCost AS base ,    
                                OI.monTotAmount AS ChargedAmount ,    
                                Oi.numItemCode AS ORDERID ,    
                                ISNULL(oi.vcModelID, 'NA') AS ModelId ,    
                                dbo.FormatedDateFromDate(OM.bintCreatedDate,OM.numDomainId)   AS OrderDate,     
                                om.tintOppType ,    
                                om.tintOppStatus ,    
                                Om.numContactId ,    
                                ACI.numDivisionId ,    
                                Om.numOppId AS OppId ,    
								 Om.vcPOppName AS OrderNo ,
								 ISNULL(CPN.CustomerPartNo,'') AS  CustomerPartNo,
								 OI.numItemCode
                        FROM    dbo.OpportunityItems AS OI    
								INNER JOIN Item I ON OI.numItemCode = I.numItemCode
                                INNER JOIN dbo.OpportunityMaster OM ON OI.numOppId = OM.numOppId    
                                INNER JOIN dbo.AdditionalContactsInformation ACI ON ACI.numContactId = om.numContactId   
								LEFT JOIN WareHouseItems WI ON OI.numWarehouseItmsID = WI.numWareHouseItemID
								LEFT JOIN  CustomerPartNumber CPN  ON OI.numItemCode = CPN.numItemCode AND numCompanyId = @numCompanyID and CPN.numDomainId = @numDomainId 
                        WHERE   Om.numDomainId = @numDomainId    
                                AND ACI.numDivisionId = @numDivisionId    
                                AND ( OI.vcItemName LIKE @keyword + '%'    
                                      OR OI.vcModelID LIKE @keyword + '%'    
									  OR CPN.CustomerPartNo LIKE '%' + @keyword + '%'    
      )    
                                AND ( OM.bintCreatedDate >= ISNULL(@startDate,OM.bintCreatedDate)    
                                      AND Om.bintCreatedDate <=ISNULL(@endDate,OM.bintCreatedDate)     
                                    )         
                                            
                DECLARE @firstRec AS INTEGER          
                DECLARE @lastRec AS INTEGER          
          
                SET @firstRec = ( @CurrentPage - 1 ) * @PageSize          
                SET @lastRec = ( @CurrentPage * @PageSize + 1 )          
                SET @TotRecs = ( SELECT COUNT(*)    
                                 FROM   #tempItems    
                               )          
                   
                SELECT  *    
                FROM    ( SELECT    * ,    
                                    ROW_NUMBER() OVER ( ORDER BY ItemName ) AS RowNumber    
                          FROM      #tempItems    
                        ) a    
                WHERE   RowNumber > @firstRec    
                        AND RowNumber < @lastRec    
                ORDER BY ItemName         
                            
                     
                DROP TABLE #tempItems          
            END        
                   
        ELSE     
            BEGIN       
                   
                CREATE TABLE #tempVItems    
                    (    
					  numDivisionID NUMERIC(18,0),
                      ItemName VARCHAR(300) ,   
					  vcSKU VARCHAR(300),
					  vcUPC VARCHAR(300), 
                      MinOrderQty INT ,    
                      MinCost DECIMAL(18,2) ,    
                      ModelId VARCHAR(200) ,    
                      PartNo VARCHAR(100) ,    
                      numItemCode NUMERIC    ,
					  CustomerPartNo  VARCHAR(100)  
                    )        
                INSERT  INTO #tempVItems    
                        SELECT  @numDivisionId,
								i.vcItemName AS ItemName,
								ISNULL(I.vcSKU,''),
								ISNULL(I.numBarCodeId,''),
                                v.intMinQty AS MinOrderQty,
                                v.monCost AS MinCost,
                                I.vcModelID AS ModelId,
                                v.vcPartNo AS PartNo,
                                v.numItemCode AS numItemCode,
								ISNULL(CPN.CustomerPartNo,'') AS  CustomerPartNo     
                        FROM    dbo.Item AS I    
                                INNER JOIN dbo.Vendor AS V ON I.numVendorID = V.numVendorID    
								LEFT JOIN  CustomerPartNumber CPN  ON I.numItemCode = CPN.numItemCode AND numCompanyId = @numCompanyID and CPN.numDomainId = @numDomainId 
                        WHERE   i.numVendorID = @numDivisionId    
                                AND v.numItemCode = i.numItemCode    
                                AND v.numDomainID = @numDomainId        
                              AND ( I.vcItemName LIKE @keyword + '%'    
                                      OR I.vcModelID LIKE @keyword + '%'    
                                    )    
                 
                 
                DECLARE @VfirstRec AS INTEGER          
                DECLARE @VlastRec AS INTEGER          
          
                SET @VfirstRec = ( @CurrentPage - 1 ) * @PageSize          
                SET @VlastRec = ( @CurrentPage * @PageSize + 1 )          
                SET @TotRecs = ( SELECT COUNT(*)    
                                 FROM   #tempVItems    
                               )          
                     
                            
                SELECT  *    
                FROM    ( SELECT    * ,    
                                    ROW_NUMBER() OVER ( ORDER BY ItemName ) AS RowNumber    
                          FROM      #tempVItems    
                        ) a    
                WHERE   RowNumber > @VfirstRec    
                        AND RowNumber < @VlastRec    
                ORDER BY ItemName               
                            
                    
                DROP TABLE #tempVItems          
                 
                 
            END           
                    
                    
                      
    END 


