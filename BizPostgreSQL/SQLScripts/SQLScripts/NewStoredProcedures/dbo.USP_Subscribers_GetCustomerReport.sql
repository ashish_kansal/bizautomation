GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
       
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_Subscribers_GetCustomerReport')
DROP PROCEDURE USP_Subscribers_GetCustomerReport
GO
CREATE PROCEDURE [dbo].[USP_Subscribers_GetCustomerReport]
(
	@bitPayingCustomerOnly BIT
	,@SortChar CHAR(1)
	,@tintSortOrder TINYINT
	,@vcSearchWord VARCHAR(100)
	,@ClientTimeZoneOffset INT
	,@tintLastLoggedIn TINYINT
	,@numPageIndex INT
	,@numPageSize INT
	,@vcSortColumn VARCHAR(100)
	,@vcSortOrder VARCHAR(10)
)
AS
BEGIN
	CREATE TABLE #TEMPCutomer
	(
		numDivisionID NUMERIC(18,0)
		,vcCompanyName VARCHAR(300)
		,vcCompanyNameURL VARCHAR(500)
		,dtSubEndDate DATETIME
		,vcSubEndDate VARCHAR(50)
		,dtLastLoggedIn DATETIME
		,vcLastLoggedIn VARCHAR(50)
		,intSubscribedFullUsers INT
		,intFullUsersUniqueLogin INT
		,intFullUsersAllLogin INT
		,intUsedFullUsers INT
		,monFullUserCost DECIMAL(20,5)
		,intSubscribedLimitedAccessUsers INT
		,intUsedLimitedAccessUsers INT
		,intSubscribedBusinessPortalUsers INT
		,intUsedBusinessPortalUsers INT
		,monSiteCost DECIMAL(20,5)
		,monTotalBiz DECIMAL(20,5)
		,numTotalSalesOrder INT
		,monTotalGrossProfit DECIMAL(20,5)
		,numTotalBizPercent INT
		,numTotalSize FLOAT
		,numTotalIncome DECIMAL(20,5)
	)

	INSERT INTO #TEMPCutomer
	(
		numDivisionID
		,vcCompanyName
		,vcCompanyNameURL
		,dtSubEndDate
		,vcSubEndDate
		,dtLastLoggedIn
		,vcLastLoggedIn
		,intSubscribedFullUsers
		,intFullUsersUniqueLogin
		,intFullUsersAllLogin
		,intUsedFullUsers
		,monFullUserCost
		,intSubscribedLimitedAccessUsers
		,intUsedLimitedAccessUsers
		,intSubscribedBusinessPortalUsers
		,intUsedBusinessPortalUsers
		,monSiteCost
		,monTotalBiz
		,numTotalSalesOrder
		,monTotalGrossProfit
		,numTotalBizPercent
		,numTotalSize
	)
	SELECT
		DivisionMaster.numDivisionID
		,ISNULL(vcCompanyName,'-')
		,CONCAT('<a href=',(CASE 
							WHEN DivisionMaster.tintCRMType=2 
							THEN CONCAT('../account/frmAccounts.aspx?DivID=',DivisionMaster.numDivisionID,'') 
							WHEN tintCRMType=1 
							THEN CONCAT('../prospects/frmProspects.aspx?DivID=',DivisionMaster.numDivisionID,'') 
							ELSE CONCAT('../Leads/frmLeads.aspx?DivID=',DivisionMaster.numDivisionID,'') 
							END),'>',ISNULL(vcCompanyName,'-'),'</a>') vcCompanyNameURL
		,dtSubEndDate
		,dbo.FormatedDateFromDate(dtSubEndDate,1) as dtSubEndDate
		,dtLastLoggedIn
		,dbo.FormatedDateTimeFromDate(DATEADD(MINUTE,-@ClientTimeZoneOffset,dtLastLoggedIn),1) dtLastLoggedIn
		,ISNULL(intNoofUsersSubscribed,0) intSubscribedFullUsers
		,ISNULL((SELECT  
					COUNT(DISTINCT numContactID)
				FROM 
					UserAccessedDTL 
				INNER JOIN
					UserMaster
				ON
					UserAccessedDTL.numContactID = UserMaster.numUserDetailId
				LEFT JOIN 
					AuthenticationGroupMaster 
				ON 
					UserMaster.numGroupID = AuthenticationGroupMaster.numGroupID
				WHERE 
					UserAccessedDTL.numDomainID=UserAccessedDTL.numDomainID
					AND UserAccessedDTL.numDivisionID = DivisionMaster.numDivisionID
					AND (AuthenticationGroupMaster.tintGroupType=1 OR ISNULL(AuthenticationGroupMaster.tintGroupType,0) = 0)
					AND 1 = (CASE 
								WHEN @tintLastLoggedIn = 1 THEN (CASE WHEN dtLoggedInTime BETWEEN DATEADD(DAY,-7,GETUTCDATE()) AND GETUTCDATE() THEN 1 ELSE 0 END)                
								WHEN @tintLastLoggedIn = 2 THEN( CASE WHEN dtLoggedInTime BETWEEN DATEADD(DAY,-14,GETUTCDATE()) AND GETUTCDATE() THEN 1 ELSE 0 END)
								WHEN @tintLastLoggedIn = 3 THEN (CASE WHEN dtLoggedInTime BETWEEN DATEADD(DAY,-30,GETUTCDATE()) AND GETUTCDATE() THEN 1 ELSE 0 END)
								ELSE 1
							END)
			),0) intFullUsersUniqueLogin
		,ISNULL((SELECT 
					COUNT(*)
				FROM 
					UserAccessedDTL 
				INNER JOIN
					UserMaster
				ON
					UserAccessedDTL.numContactID = UserMaster.numUserDetailId
				LEFT JOIN 
					AuthenticationGroupMaster 
				ON 
					UserMaster.numGroupID = AuthenticationGroupMaster.numGroupID
				WHERE 
					UserAccessedDTL.numDomainID=UserAccessedDTL.numDomainID
					AND UserAccessedDTL.numDivisionID = DivisionMaster.numDivisionID
					AND (AuthenticationGroupMaster.tintGroupType=1 OR ISNULL(AuthenticationGroupMaster.tintGroupType,0) = 0)
					AND 1 = (CASE 
								WHEN @tintLastLoggedIn = 1 THEN (CASE WHEN dtLoggedInTime BETWEEN DATEADD(DAY,-7,GETUTCDATE()) AND GETUTCDATE() THEN 1 ELSE 0 END)                
								WHEN @tintLastLoggedIn = 2 THEN( CASE WHEN dtLoggedInTime BETWEEN DATEADD(DAY,-14,GETUTCDATE()) AND GETUTCDATE() THEN 1 ELSE 0 END)
								WHEN @tintLastLoggedIn = 3 THEN (CASE WHEN dtLoggedInTime BETWEEN DATEADD(DAY,-30,GETUTCDATE()) AND GETUTCDATE() THEN 1 ELSE 0 END)
								ELSE 1
							END)
			),0) intFullUsersAllLogin
		,(CASE 
			WHEN ISNULL(intNoofUsersSubscribed,0) > 0
			THEN (ISNULL((SELECT  
								COUNT(DISTINCT numContactID)
							FROM 
								UserAccessedDTL 
							INNER JOIN
								UserMaster
							ON
								UserAccessedDTL.numContactID = UserMaster.numUserDetailId
							LEFT JOIN 
								AuthenticationGroupMaster 
							ON 
								UserMaster.numGroupID = AuthenticationGroupMaster.numGroupID
							WHERE 
								UserAccessedDTL.numDomainID=UserAccessedDTL.numDomainID
								AND UserAccessedDTL.numDivisionID = DivisionMaster.numDivisionID
								AND (AuthenticationGroupMaster.tintGroupType=1 OR ISNULL(AuthenticationGroupMaster.tintGroupType,0) = 0)
								AND 1 = (CASE 
											WHEN @tintLastLoggedIn = 1 THEN (CASE WHEN dtLoggedInTime BETWEEN DATEADD(DAY,-7,GETUTCDATE()) AND GETUTCDATE() THEN 1 ELSE 0 END)                
											WHEN @tintLastLoggedIn = 2 THEN( CASE WHEN dtLoggedInTime BETWEEN DATEADD(DAY,-14,GETUTCDATE()) AND GETUTCDATE() THEN 1 ELSE 0 END)
											WHEN @tintLastLoggedIn = 3 THEN (CASE WHEN dtLoggedInTime BETWEEN DATEADD(DAY,-30,GETUTCDATE()) AND GETUTCDATE() THEN 1 ELSE 0 END)
											ELSE 1
										END)
						),0) * 100) / ISNULL(intNoofUsersSubscribed,0)
			ELSE 0
		END) intUsedFullUsers
		,ISNULL(monFullUserCost,79.95) monFullUserCost
		,ISNULL(intNoofLimitedAccessUsers,0) intSubscribedLimitedAccessUsers
		,(CASE 
			WHEN ISNULL(intNoofLimitedAccessUsers,0) > 0
			THEN (ISNULL((SELECT 
								COUNT(DISTINCT numContactID)
							FROM 
								UserAccessedDTL 
							INNER JOIN
								UserMaster
							ON
								UserAccessedDTL.numContactID = UserMaster.numUserDetailId
							INNER JOIN 
								AuthenticationGroupMaster 
							ON 
								UserMaster.numGroupID = AuthenticationGroupMaster.numGroupID
							WHERE 
								UserAccessedDTL.numDomainID=UserAccessedDTL.numDomainID
								AND UserAccessedDTL.numDivisionID = DivisionMaster.numDivisionID
								AND AuthenticationGroupMaster.tintGroupType=4
								AND 1 = (CASE 
											WHEN @tintLastLoggedIn = 1 THEN (CASE WHEN dtLoggedInTime BETWEEN DATEADD(DAY,-7,GETUTCDATE()) AND GETUTCDATE() THEN 1 ELSE 0 END)                
											WHEN @tintLastLoggedIn = 2 THEN( CASE WHEN dtLoggedInTime BETWEEN DATEADD(DAY,-14,GETUTCDATE()) AND GETUTCDATE() THEN 1 ELSE 0 END)
											WHEN @tintLastLoggedIn = 3 THEN (CASE WHEN dtLoggedInTime BETWEEN DATEADD(DAY,-30,GETUTCDATE()) AND GETUTCDATE() THEN 1 ELSE 0 END)
											ELSE 1
										END)
						),0) * 100) / ISNULL(intNoofLimitedAccessUsers,0)
			ELSE 0
		END) intUsedLimitedAccessUsers
		,ISNULL(intNoofBusinessPortalUsers,0) intSubscribedBusinessPortalUsers
		,(CASE 
			WHEN ISNULL(intNoofBusinessPortalUsers,0) > 0
			THEN (ISNULL((SELECT 
							COUNT(DISTINCT ExtranetAccountsDtl.numContactID)
						FROM 
							ExtranetAccountsDtl
						INNER JOIN
							ExtarnetAccounts
						ON
							ExtranetAccountsDtl.numExtranetID = ExtarnetAccounts.numExtranetID
						INNER JOIN
							UserAccessedDTL
						ON
							ExtranetAccountsDtl.numDomainID = UserAccessedDTL.numDomainID
							AND ExtarnetAccounts.numDivisionID = UserAccessedDTL.numDivisionID
							AND ExtranetAccountsDtl.numContactID = UserAccessedDTL.numContactID
						WHERE 
							ExtranetAccountsDtl.numDomainID=Subscribers.numTargetDomainID
							AND 1 = (CASE 
										WHEN @tintLastLoggedIn = 1 THEN (CASE WHEN dtLoggedInTime BETWEEN DATEADD(DAY,-7,GETUTCDATE()) AND GETUTCDATE() THEN 1 ELSE 0 END)                
										WHEN @tintLastLoggedIn = 2 THEN( CASE WHEN dtLoggedInTime BETWEEN DATEADD(DAY,-14,GETUTCDATE()) AND GETUTCDATE() THEN 1 ELSE 0 END)
										WHEN @tintLastLoggedIn = 3 THEN (CASE WHEN dtLoggedInTime BETWEEN DATEADD(DAY,-30,GETUTCDATE()) AND GETUTCDATE() THEN 1 ELSE 0 END)
										ELSE 1
									END)
					),0) * 100) / ISNULL(intNoofBusinessPortalUsers,0)
			ELSE 0
		END) intUsedBusinessPortalUsers
		,ISNULL((SELECT COUNT(*) FROM Sites SI WHERE SI.numDomainID=Subscribers.numTargetDomainID AND ISNULL(SI.bitIsActive,0)=1 AND LEN(ISNULL(SI.vcLiveURL,'')) > 0),0) * ISNULL(monSiteCost,0) monSiteCost
		,((ISNULL(intNoofUsersSubscribed,0) * ISNULL(monFullUserCost,79.95))
		 + (ISNULL(intNoofLimitedAccessUsers,0) * ISNULL(monLimitedAccessUserCost,24.95))
		 + (ISNULL(intNoofBusinessPortalUsers,0) * ISNULL(monBusinessPortalUserCost,9.95))
		 + (ISNULL((SELECT COUNT(*) FROM Sites SI WHERE SI.numDomainID=Subscribers.numTargetDomainID AND ISNULL(SI.bitIsActive,0)=1 AND LEN(ISNULL(SI.vcLiveURL,'')) > 0),0) * ISNULL(monSiteCost,0))) AS monTotalBiz
		,ISNULL((SELECT COUNT(*) FROM OpportunityMaster OM WHERE OM.numDomainID=Subscribers.numTargetDomainID AND OM.tintOppType=1 AND OM.tintOppStatus=1 AND OM.bintCreatedDate BETWEEN DATEADD(DAY,-30,GETUTCDATE()) AND GETUTCDATE()),0) numTotalSalesOrder
		,ISNULL((SELECT SUM(dbo.GetOrderProfitAmountOrMargin(OM.numDomainId,OM.numOppId,1)) FROM OpportunityMaster OM WHERE OM.numDomainID=Subscribers.numTargetDomainID AND OM.tintOppType=1 AND OM.tintOppStatus=1 AND OM.bintCreatedDate BETWEEN DATEADD(DAY,-30,GETUTCDATE()) AND GETUTCDATE()),0) monTotalGrossProfit
		,0 numTotalBizPercent
		,ISNULL((SELECT SUM(ISNULL(SpaceOccupied,0)) FROM dbo.View_InboxCount VI WHERE VI.numDomainID=Subscribers.numTargetDomainID),0) AS numTotalSize
	FROM
		Subscribers
	INNER JOIN
		Domain
	ON
		Subscribers.numTargetDomainID = Domain.numDomainId
	INNER JOIN
		DivisionMaster
	ON
		Domain.numDivisionID = DivisionMaster.numDivisionID
	INNER JOIN
		CompanyInfo
	ON
		DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
	WHERE
		(@SortChar = '0' OR vcCompanyName LIKE CONCAT(@SortChar,'%'))
		AND 1 = (CASE 
					WHEN LEN(ISNULL(@vcSearchWord,'')) > 0 AND @tintSortOrder=1 THEN (CASE WHEN vcCompanyName LIKE CONCAT('%',@vcSearchWord,'%') THEN 1 ELSE 0 END)
					WHEN LEN(ISNULL(@vcSearchWord,'')) > 0 AND @tintSortOrder=2 THEN (CASE WHEN DivisionMaster.numDivisionID IN (SELECT numDivisionID FROM AdditionalContactsInformation A WHERE vcEmail LIKE CONCAT('%',@vcSearchWord,'%')) THEN 1 ELSE 0 END)
					WHEN @tintSortOrder=3 THEN (CASE WHEN Subscribers.bitTrial=1 THEN 1 ELSE 0 END)
					WHEN @tintSortOrder=4 THEN (CASE WHEN Subscribers.bitActive=1 THEN 1 ELSE 0 END)                  
					WHEN @tintSortOrder=5 THEN (CASE WHEN Subscribers.bitActive=0 THEN 1 ELSE 0 END)                   
					WHEN @tintSortOrder=6 THEN (CASE WHEN Subscribers.bitActive=0 AND Subscribers.dtSuspendedDate <= DATEADD(DAY,30,GETUTCDATE()) THEN 1 ELSE 0 END)                  
					WHEN @tintSortOrder=7 THEN (CASE WHEN Subscribers.bitActive=0 AND Subscribers.dtSuspendedDate >= DATEADD(DAY,30,GETUTCDATE()) THEN 1 ELSE 0 END)
					ELSE 1
				END)     
		AND 1 = (CASE 
					WHEN @bitPayingCustomerOnly=1 
					THEN (CASE WHEN ISNULL(bitTrial,0) = 0 THEN 1 ELSE 0 END) 
					ELSE 1
				END)
		AND 1 = (CASE 
					WHEN @tintLastLoggedIn = 1 THEN (CASE WHEN ISNULL((SELECT COUNT(*) FROM UserAccessedDTL UAD WHERE UAD.numDivisionID=DivisionMaster.numDivisionID AND UAD.dtLoggedInTime BETWEEN DATEADD(DAY,-7,GETUTCDATE()) AND GETUTCDATE()),0) > 0 THEN 1 ELSE 0 END)
					WHEN @tintLastLoggedIn = 2 THEN (CASE WHEN ISNULL((SELECT COUNT(*) FROM UserAccessedDTL UAD WHERE UAD.numDivisionID=DivisionMaster.numDivisionID AND UAD.dtLoggedInTime BETWEEN DATEADD(DAY,-14,GETUTCDATE()) AND GETUTCDATE()),0) > 0 THEN 1 ELSE 0 END)
					WHEN @tintLastLoggedIn = 3 THEN (CASE WHEN ISNULL((SELECT COUNT(*) FROM UserAccessedDTL UAD WHERE UAD.numDivisionID=DivisionMaster.numDivisionID AND UAD.dtLoggedInTime BETWEEN DATEADD(DAY,-30,GETUTCDATE()) AND GETUTCDATE()),0) > 0 THEN 1 ELSE 0 END)
					ELSE 1
				END)

	UPDATE #TEMPCutomer SET numTotalBizPercent = (CASE WHEN ISNULL(monTotalGrossProfit,0) > 0 THEN  ((monTotalBiz * 100) / monTotalGrossProfit) ELSE 0 END)
	UPDATE #TEMPCutomer SET numTotalIncome = ISNULL((SELECT SUM(monTotalBiz) FROM #TEMPCutomer),0)

	DECLARE @vcSQL VARCHAR(MAX)
	SET @vcSQL = 'SELECT COUNT(*) OVER() numTotalRecords,* FROM #TEMPCutomer'
	
	IF LEN(ISNULL(@vcSortColumn,'')) > 0
	BEGIN
		SET @vcSQL = CONCAT(@vcSQL,' ORDER BY ',@vcSortColumn,' ',@vcSortOrder)
	END
	ELSE
	BEGIN
		SET @vcSQL = CONCAT(@vcSQL,' ORDER BY monTotalBiz DESC')
	END

	SET @vcSQL = CONCAT(@vcSQL,' OFFSET ',(@numPageIndex-1) * @numPageSize,' ROWS FETCH NEXT ',@numPageSize,' ROWS ONLY')
		
	EXEC (@vcSQL)

	DROP TABLE #TEMPCutomer
END
GO