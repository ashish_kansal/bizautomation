
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_UpdateWorkOrderDetailQuantity')
DROP PROCEDURE USP_UpdateWorkOrderDetailQuantity
GO
CREATE PROCEDURE [dbo].[USP_UpdateWorkOrderDetailQuantity]
	@numDomainID NUMERIC(18,0),
	@numUserCntID NUMERIC(18,0),
	@numItemCode AS numeric(18,0),
	@numUnitHour AS FLOAT,
	@numWOId AS NUMERIC(18,0)
AS
BEGIN
	DECLARE @numPickedQty FLOAT = 0
				
	SET @numPickedQty = ISNULL((SELECT
									MAX(numPickedQty)
								FROM
								(
									SELECT 
										WorkOrderDetails.numWODetailId
										,CEILING(SUM(WorkOrderPickedItems.numPickedQty)/numQtyItemsReq_Orig) numPickedQty
									FROM
										WorkOrderDetails
									INNER JOIN
										WorkOrderPickedItems
									ON
										WorkOrderDetails.numWODetailId=WorkOrderPickedItems.numWODetailId
									WHERE
										WorkOrderDetails.numWOId=@numWOID
										AND ISNULL(WorkOrderDetails.numWareHouseItemId,0) > 0
									GROUP BY
										WorkOrderDetails.numWODetailId
										,WorkOrderDetails.numQtyItemsReq_Orig
								) TEMP),0)

	IF @numUnitHour < ISNULL((@numPickedQty),0)
	BEGIN
		RAISERROR('WORKORDR_QUANITY_CAN_NOT_BE_LESS_THEN_PICKED_QTY',16,1)
		RETURN
	END

	IF EXISTS (SELECT 
					SPDTTL.ID 
				FROM 
					StagePercentageDetailsTask SPDT
				INNER JOIN
					StagePercentageDetailsTaskTimeLog SPDTTL
				ON
					SPDT.numTaskId = SPDTTL.numTaskID 
				WHERE 
					SPDT.numDomainID=@numDomainID 
					AND SPDT.numWorkOrderId = @numWOId)
	BEGIN
		RAISERROR('TASKS_ARE_ALREADY_STARTED',16,1)
		RETURN
	END

	IF EXISTS (SELECT numWOId FROM WorkOrder WHERE numDomainID=@numDomainID AND numWOId=@numWOId AND numWOStatus=23184)
	BEGIN
		RAISERROR('WORKORDR_COMPLETED',16,1)
		RETURN
	END

	BEGIN TRY
	BEGIN TRANSACTION
		EXEC USP_RevertInventoryWorkOrder @numDomainID,@numUserCntID,@numWOID

		DECLARE @numOppID NUMERIC(18,0)
		DECLARE @numCurrentQty FLOAT
		DECLARE @numWarehouseItmsID NUMERIC(18,0)

		SELECT
			@numOppID=WorkOrder.numOppID
			,@numCurrentQty = numQtyItemsReq
			,@numWarehouseItmsID=numWareHouseItemId
		FROM
			WorkOrder
		WHERE
			numWOId=@numWOId

		;WITH CTE(numWODetailId,numItemKitID,numItemCode,numCalculatedQty)
		AS
		(
			SELECT 
				dtl.numWODetailId
				,CAST(@numItemCode AS NUMERIC(18,0))
				,numItemCode
				,CAST((DTL.numQtyItemsReq_Orig * @numUnitHour) AS FLOAT) AS numCalculatedQty
			FROM 
				Item                                
			INNER JOIN 
				WorkOrderDetails Dtl 
			ON 
				numChildItemID=numItemCode
			WHERE 
				numItemKitID=@numItemCode 
				AND dtl.numWOId=@numWOId
			UNION ALL
			SELECT 
				dtl.numWODetailId
				,CAST(Dtl.numItemKitID AS NUMERIC(18,0))
				,i.numItemCode
				,CAST((DTL.numQtyItemsReq_Orig * c.numCalculatedQty) AS FLOAT) AS numCalculatedQty
			FROM 
				Item i                               
			INNER JOIN 
				WorkOrderDetails Dtl 
			ON 
				Dtl.numChildItemID=i.numItemCode
			INNER JOIN 
				CTE c 
			ON 
				Dtl.numItemKitID = c.numItemCode 
			WHERE 
				dtl.numWOId=@numWOId
		)

		UPDATE 
			WOD 
		SET 
			numQtyItemsReq=numCalculatedQty 
		FROM 
			WorkOrderDetails WOD 
		JOIN 
			CTE c 
		ON 
			WOD.numWODetailId=c.numWODetailId 
		WHERE 
			WOD.numWOId=@numWOId 

		EXEC USP_ManageInventoryWorkOrder @numWOId,@numDomainID,@numUserCntID

		--UPDATE ON ORDER OF ASSEMBLY
		UPDATE 
			WareHouseItems
		SET    
			numOnOrder= (ISNULL(numOnOrder,0) - @numCurrentQty) + @numUnitHour,
			dtModified = GETDATE() 
		WHERE   
			numWareHouseItemID = @numWarehouseItmsID 
	
		--MAKE ENTRY IN WAREHOUSE TRACKING TABLE
		DECLARE @Description VARCHAR(300)
		SET @Description=CONCAT('Work Order Edited (Qty:',@numUnitHour,')')

		EXEC dbo.USP_ManageWareHouseItems_Tracking
		@numWareHouseItemID = @numWarehouseItmsID, --  numeric(9, 0)
		@numReferenceID = @numWOId, --  numeric(9, 0)
		@tintRefType = 2, --  tinyint
		@vcDescription = @Description, --  varchar(100)
		@numModifiedBy = @numUserCntID,
		@numDomainID = @numDomainID

	COMMIT
	END TRY
	BEGIN CATCH
		DECLARE @ErrorMessage NVARCHAR(4000)
		DECLARE @ErrorNumber INT
		DECLARE @ErrorSeverity INT
		DECLARE @ErrorState INT
		DECLARE @ErrorLine INT
		DECLARE @ErrorProcedure NVARCHAR(200)

		IF @@TRANCOUNT > 0
			ROLLBACK TRANSACTION;

		SELECT 
			@ErrorMessage = ERROR_MESSAGE(),
			@ErrorNumber = ERROR_NUMBER(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE(),
			@ErrorLine = ERROR_LINE(),
			@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

		RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);
	END CATCH
END