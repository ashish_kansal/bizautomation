GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetMailRefreshToken')
DROP PROCEDURE USP_GetMailRefreshToken
GO
CREATE PROCEDURE USP_GetMailRefreshToken
(
	@numDomainID NUMERIC(18,0),
    @numUserCntID NUMERIC(18,0)
)
AS 
BEGIN
	IF ISNULL(@numDomainId,0) > 0 AND ISNULL(@numUserCntId,0) > 0
	BEGIN
		SELECT
			tintMailProvider
			,vcMailAccessToken
			,vcMailRefreshToken
			,vcPSMTPUserName
			,(CASE WHEN DATEDIFF(MINUTE,GETUTCDATE(),dtTokenExpiration) > 5 THEN 0 ELSE 1 END) bitExpired
		FROM  
			UserMaster
		WHERE   
			numDomainID = @numDomainID
			AND numUserDetailId = @numUserCntID
	END
	ELSE
	BEGIN
		SELECT
			tintMailProvider
			,vcMailAccessToken
			,vcMailRefreshToken
			,vcPSMTPUserName
			,(CASE WHEN DATEDIFF(MINUTE,GETUTCDATE(),dtTokenExpiration) > 5 THEN 0 ELSE 1 END) bitExpired
		FROM  
			Domain
		WHERE   
			numDomainID = @numDomainID
	END
END
GO