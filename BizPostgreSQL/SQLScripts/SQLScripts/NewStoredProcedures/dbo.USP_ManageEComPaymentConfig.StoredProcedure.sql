GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_ManageEComPaymentConfig' ) 
    DROP PROCEDURE USP_ManageEComPaymentConfig
GO
CREATE PROCEDURE USP_ManageEComPaymentConfig
    @numDomainID NUMERIC,
    @numSiteID NUMERIC,
    @strItems TEXT,
    @tintMode AS TINYINT
AS 
BEGIN

    IF @tintMode = 1 
        BEGIN
			   
                            SELECT  ISNULL(PC.numId, 0) as numId,
                            ISNULL(PC.numPaymentMethodId, 0) numPaymentMethodId,
                            dbo.GetListIemName(PC.numPaymentMethodId) [PaymentMethod],
                            ISNULL(PC.bitEnable, 0) bitEnable,
                            ISNULL(PC.numBizDocId, 0) numBizDocId,
                            ISNULL(PC.numBizDocStatus, 0) numBizDocStatus,
                            ISNULL(PC.numMirrorBizDocTemplateId,0) numMirrorBizDocTemplateId, 
                            ISNULL(PC.numOrderStatus, 0) numOrderStatus,
                            ISNULL(PC.numFailedOrderStatus, 0)  numFailedOrderStatus ,
                            ISNULL(PC.numRecordOwner, 0) numRecordOwner,
                            ISNULL(PC.numAssignTo, 0) numAssignTo,
                            CASE WHEN PC.numPaymentMethodId = 84 THEN 'Sales Opportunity' 
                                 ELSE 'Sales Order'
                            END AS [strType]
							FROM    
									dbo.eCommercePaymentConfig PC
							WHERE   
									 PC.numDomainID = @numDomainID
									AND numSiteId = @numSiteID
                            
                            UNION 
                            
							SELECT   0 as numId,
                            LD.numListItemId numPaymentMethodId,
                            LD.vcData AS PaymentMethod,
							0 bitEnable,
                            0 numBizDocId,
                            0 numBizDocStaus,
                            0 numMirrorBizDocTemplateId,
                            0 numOrderStatus,
                            0 numFailedOrderStatus,
                            0 numRecordOwner,
                            0 numAssignTo,
                            CASE WHEN LD.numListItemId = 84 THEN 'Sales Opportunity' 
                                 ELSE 'Sales Order'
                            END AS [strType]
							FROM    dbo.ListDetails LD
							WHERE   LD.numListID = 31
									AND LD.constFlag = 1
									AND LD.numListItemId NOT IN
									(
									  SELECT ISNULL(PC.numPaymentMethodId, 0) numPaymentMethodId FROM dbo.eCommercePaymentConfig PC
									  WHERE PC.numDomainID = @numDomainID AND numSiteId = @numSiteID
									 ) 

                            ORDER BY numPaymentMethodId
                            		
        END
    ELSE 
        IF @tintMode = 2 
            BEGIN
                DECLARE @hDocItem INT
                IF CONVERT(VARCHAR(10), @strItems) <> '' 
                    BEGIN
                        EXEC sp_xml_preparedocument @hDocItem OUTPUT,
                            @strItems
            
                        DELETE  FROM dbo.eCommercePaymentConfig
                        WHERE   numSiteId = @numSiteID
                                AND numDomainID = @numDomainID
            
          
                        INSERT  INTO dbo.eCommercePaymentConfig ( numSiteId,
                                                                  numPaymentMethodId,
                                                                  bitEnable,
                                                                  numBizDocId,
															      numBizDocStatus,
                                                                  numMirrorBizDocTemplateId ,
                                                                  numOrderStatus,
                                                                  numFailedOrderStatus,
                                                                  numRecordOwner,
                                                                  numAssignTo,
                                                                  numDomainID )
                                SELECT  @numSiteID,
                                        X.numPaymentMethodId,
                                        X.bitEnable,
                                        X.numBizDocId,
                                        X.numBizdocStatus,
                                        X.numMirrorBizDocTemplateId,
                                        X.numOrderStatus,
                                        X.numFailedOrderStatus,
                                        X.numRecordOwner,
                                        X.numAssignTo,
                                        @numDomainID
                                FROM    ( SELECT    *
                                          FROM      OPENXML (@hDocItem, '/NewDataSet/Item', 2)
                                                    WITH ( numPaymentMethodId NUMERIC(18, 0), bitEnable NUMERIC(18, 0), numBizDocId NUMERIC(18, 0),numBizdocStatus Numeric(18,0),numMirrorBizDocTemplateId NUMERIC(18, 0), numOrderStatus NUMERIC(18, 0),numFailedOrderStatus NUMERIC(18, 0), numRecordOwner NUMERIC(18, 0), numAssignTo NUMERIC(18, 0) )
                                        ) X
                        EXEC sp_xml_removedocument @hDocItem
                    END
                    SELECT 1
            END
	
END
