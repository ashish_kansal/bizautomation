GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_GetAssetSerial')
DROP PROCEDURE usp_GetAssetSerial
GO
CREATE PROCEDURE usp_GetAssetSerial
@numItemCode Numeric(18)
as
begin
	select numAssetItemDTLID, numAssetItemID,isnull(vcSerialNo,'') as vcSerialNo,isnull(numBarCodeId,0) as numBarCodeID,
isnull(vcModelId,'') as vcModelID, dtPurchase,dtWarrante, isnull(vcLocation,'') as vcLocation,0 as Op_Flag from CompanyAssetSerial
where numAssetItemId=@numItemCode
end


