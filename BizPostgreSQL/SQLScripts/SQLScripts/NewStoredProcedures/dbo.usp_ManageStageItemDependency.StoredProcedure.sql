
GO
/****** Object:  StoredProcedure [dbo].[usp_ManageStageItemDependency]    Script Date: 09/24/2010 18:11:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_ManageStageItemDependency')
DROP PROCEDURE usp_ManageStageItemDependency
GO
CREATE PROCEDURE [dbo].[usp_ManageStageItemDependency]
    @numStageDetailID NUMERIC = 0,
    @numDependantOnID NUMERIC = 0 ,
    @tintMode TINYINT=0
AS 

IF @tintMode = 0 
BEGIN   
insert into StageDependency (numStageDetailID,numDependantOnID) values (@numStageDetailID,@numDependantOnID)
END
ELSE IF @tintMode = 1
delete from StageDependency where numStageDetailID=@numStageDetailID and numDependantOnID=@numDependantOnID
