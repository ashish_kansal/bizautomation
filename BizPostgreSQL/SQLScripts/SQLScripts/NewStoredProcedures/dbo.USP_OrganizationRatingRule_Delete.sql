SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_OrganizationRatingRule_Delete')
DROP PROCEDURE dbo.USP_OrganizationRatingRule_Delete
GO
CREATE PROCEDURE [dbo].[USP_OrganizationRatingRule_Delete]
(
	@numDomainID NUMERIC(18,0)
	,@numORRID NUMERIC(18,0)
)
AS 
BEGIN
	DELETE FROM OrganizationRatingRule WHERE numDomainID = @numDomainID AND numORRID = @numORRID
END