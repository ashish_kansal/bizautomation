GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_OpportunityItems_UpdateWarehouseMapping')
DROP PROCEDURE USP_OpportunityItems_UpdateWarehouseMapping
GO
CREATE PROCEDURE [dbo].[USP_OpportunityItems_UpdateWarehouseMapping]
(
	@numDomainID NUMERIC(18,0)
	,@numOppID NUMERIC(18,0)
)
AS
BEGIN
	DECLARE @numOrderSource NUMERIC(18,0)
	DECLARE @tintSourceType TINYINT
	DECLARE @numShipToState NUMERIC(18,0)
	DECLARE @numShipToCountry NUMERIC(18,0)

	SELECT
		@numOrderSource = ISNULL(OpportunityMaster.tintSource,0)
		,@tintSourceType = ISNULL(OpportunityMaster.tintSourceType,0)
	FROM
		OpportunityMaster
	WHERE
		numDomainId=@numDomainID
		AND numOppId=@numOppID

	SELECT
		@numShipToState=ISNULL(numState,0)
		,@numShipToCountry=ISNULL(numCountry,0)
	FROM
		fn_getOPPAddressDetails(@numOppID,@numDomainID,2)

	DECLARE @TEMPWarehouse TABLE
	(
		ID INT IDENTITY(1,1)
		,numWarehouseID NUMERIC(18,0)
	)

	INSERT INTO @TEMPWarehouse
	(	
		numWarehouseID
	)
	SELECT  
		MSFWP.numWarehouseID
	FROM 
		MassSalesFulfillmentWM MSFWM
	INNER JOIN
		MassSalesFulfillmentWMState MSFWMS
	ON
		MSFWM.ID = MSFWMS.numMSFWMID
	INNER JOIN
		MassSalesFulfillmentWP MSFWP
	ON
		MSFWM.ID = MSFWP.numMSFWMID
	WHERE 
		numDomainID=@numDomainID 
		AND numOrderSource=@numOrderSource 
		AND tintSourceType=@tintSourceType 
		AND numCountryID=@numShipToCountry
		AND numStateID=@numShipToState
	ORDER BY
		intOrder

	IF EXISTS (SELECT ID FROM @TEMPWarehouse)
	BEGIN
		DECLARE @TEMPItems TABLE
		(
			ID INT IDENTITY(1,1)
			,numOppItemID NUMERIC(18,0)
			,numItemCode NUMERIC(18,0)
			,bitKitParent BIT
			,numWarehouseItemID NUMERIC(18,0)
			,numUnitHour FLOAT
		)

		INSERT INTO @TEMPItems
		(
			numOppItemID
			,numItemCode
			,bitKitParent
			,numWarehouseItemID
			,numUnitHour
		)
		SELECT
			OI.numoppitemtCode
			,OI.numItemCode
			,ISNULL(I.bitKitParent,0)
			,OI.numWarehouseItmsID
			,ISNULL(OI.numUnitHour,0) - ISNULL(OI.numWOQty,0)
		FROM
			OpportunityItems OI
		INNER JOIN
			Item I
		ON
			OI.numItemCode = I.numItemCode
		WHERE
			OI.numOppId = @numOppID
			AND ISNULL(OI.numWarehouseItmsID,0) > 0
			AND ISNULL(OI.bitDropShip,0) = 0

		DECLARE @i INT = 1
		DECLARE @iCount INT
		DECLARE @numOppItemID NUMERIC(18,0)
		DECLARE @numItemCode NUMERIC(18,0)
		DECLARE @bitKitParent BIT
		DECLARE @numUnitHour FLOAT
		DECLARE @numWarehouseItemID  NUMERIC(18,0)
		
		SELECT @iCount = COUNT(*) FROM @TEMPItems

		WHILE @i <= @iCount
		BEGIN
			SELECT  
				@numOppItemID=numOppItemID
				,@numItemCode=numItemCode
				,@bitKitParent=bitKitParent
				,@numUnitHour=numUnitHour
				,@numWarehouseItemID=numWarehouseItemID
			FROM 
				@TEMPItems 
			WHERE 
				ID = @i

			SELECT TOP 1 
				@numWarehouseItemID = numWareHouseItemID
			FROM
			(
				SELECT 
					ROW_NUMBER() OVER(PARTITION BY titOnHandOrder ORDER BY titOnHandOrder ASC) numRowIndex	
					,*
				FROM
				(
					SELECT
						WI.numWareHouseItemID
						,(CASE 
							WHEN @bitKitParent=1 
							THEN (CASE 
									WHEN NOT EXISTS (SELECT OKI.numOppChildItemID FROM OpportunityKitItems OKI INNER JOIN WareHouseItems WIInner ON OKI.numChildItemID=WIInner.numItemID WHERE OKI.numOppId=@numOppID AND OKI.numOppItemID=@numOppItemID AND WIInner.numWareHouseID = TW.numWarehouseID GROUP BY OKI.numOppChildItemID,OKI.numQtyItemsReq,WIInner.numWareHouseID HAVING SUM(numOnHand) < OKI.numQtyItemsReq)
										AND  NOT EXISTS (SELECT OKCI.numOppKitChildItemID FROM OpportunityKitChildItems OKCI INNER JOIN WareHouseItems WIInner ON OKCI.numItemID=WIInner.numItemID WHERE OKCI.numOppId=@numOppID AND OKCI.numOppItemID=@numOppItemID AND WIInner.numWareHouseID = TW.numWarehouseID GROUP BY OKCI.numOppKitChildItemID,OKCI.numQtyItemsReq,WIInner.numWareHouseID HAVING SUM(numOnHand) < OKCI.numQtyItemsReq)
									THEN 1
									ELSE 0 
								END)
							ELSE (CASE WHEN (SELECT SUM(WIInner.numOnHand) FROM WareHouseItems WIInner WHERE WIInner.numItemID = WI.numItemID AND WIInner.numWareHouseID=WI.numWareHouseID) >= @numUnitHour THEN 1 ELSE 0 END) 
						END) titOnHandOrder
						,TW.ID intOrder
					FROM
						WareHouseItems WI
					INNER JOIN
						@TEMPWarehouse TW
					ON
						WI.numWareHouseID = TW.numWarehouseID
					WHERE
						WI.numItemID = @numItemCode
						AND ISNULL(WI.numWareHouseID,0) = 0
				) TEMP
			) T1
			ORDER BY
				titOnHandOrder DESC, intOrder ASC			

			IF ISNULL(@numWarehouseItemID,0) > 0
			BEGIN
				UPDATE OpportunityItems SET numWarehouseItmsID=@numWarehouseItemID WHERE numOppId=@numOppID AND numoppitemtCode=@numOppItemID
			END

			SET @i = @i + 1
		END
	END
END