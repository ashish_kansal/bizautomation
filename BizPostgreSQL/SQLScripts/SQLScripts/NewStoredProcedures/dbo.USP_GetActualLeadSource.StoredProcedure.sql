GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetActualLeadSource')
DROP PROCEDURE USP_GetActualLeadSource
GO
CREATE PROCEDURE [dbo].[USP_GetActualLeadSource]  
(
	@numDomainId		NUMERIC(9,0),
	@FromDate	DATETIME	
)                      
AS
BEGIN
	
SELECT dbo.GetDomainNameFromURL(TV.vcReferrer) AS SourceDomain,
COUNT(*) AS TotalCount,
COUNT(CASE WHEN (DM.tintCRMType = 0 or DM.tintCRMType = 1)  THEN DM.numDivisionID END) AS LeadsCount,
COUNT(CASE WHEN DM.tintCRMType =2 THEN DM.numDivisionID END) AS AccountsCount,
COUNT(CASE WHEN DM.numDivisionID = OM.numDivisionID THEN OM.numOppId END) AS OrderCount,
ISNULL(SUM(CASE WHEN DM.numDivisionID = OM.numDivisionID THEN OM.monDealAmount END),0) AS AmountGenerated

FROM TrackingVisitorsHDR TV
INNER JOIN DivisionMaster DM ON TV.numDivisionID = DM.numDivisionID
LEFT JOIN OpportunityMaster OM ON DM.numDivisionID = OM.numDivisionID 

WHERE TV.numDomainId = @numDomainId 
AND DM.bitActiveInActive = 1 
AND TV.dtcreated > @FromDate 
AND TV.vcReferrer IS NOT NULL 
AND TV.vcReferrer <>''
GROUP BY dbo.GetDomainNameFromURL(vcReferrer)
ORDER BY LeadsCount DESC
	
END
