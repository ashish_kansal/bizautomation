GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_GetTransactionHistory' ) 
    DROP PROCEDURE USP_GetTransactionHistory
GO

CREATE PROCEDURE [dbo].[USP_GetTransactionHistory]
    @numTransHistoryID NUMERIC(18, 0),
    @numDomainID NUMERIC(18, 0),
    @numDivisionID NUMERIC(18, 0),
    @numOppID NUMERIC(18, 0)
    
AS 
    SET NOCOUNT ON
    SET TRANSACTION ISOLATION LEVEL READ COMMITTED

    DECLARE @strSQL AS NVARCHAR(MAX)
    DECLARE @strWHERE AS VARCHAR(MAX)
    
    SET @strWHERE = ' WHERE 1=1 AND TH.numDomainID = '
        + CONVERT(VARCHAR(10), @numDomainID) 
    
    IF ( @numOppID > 0 ) 
        BEGIN
            SET @strWHERE = @strWHERE + ' AND TH.numOppID = '
                + CONVERT(VARCHAR(10), @numOppID)
        END
    
    IF ( @numTransHistoryID > 0 ) 
        BEGIN
            SET @strWHERE = @strWHERE + ' AND numTransHistoryID = '
                + CONVERT(VARCHAR(10), @numTransHistoryID)
        END
    
    IF ( @numDivisionID > 0 ) 
        BEGIN
            SET @strWHERE = @strWHERE + ' AND TH.numDivisionID = '
                + CONVERT(VARCHAR(10), @numDivisionID)
        END
    

    SET @strSQL = ' 
SELECT
	[numTransHistoryID],
	TH.[numDivisionID],
	TH.[numContactID],
	TH.[numOppID],
	TH.[numOppBizDocsID],
	[vcTransactionID],
	[tintTransactionStatus],
	(CASE WHEN tintTransactionStatus = 1 THEN ''Authorized/Pending Capture'' 
		 WHEN tintTransactionStatus = 2 THEN ''Captured''
		 WHEN tintTransactionStatus = 3 THEN ''Void''
		 WHEN tintTransactionStatus = 4 THEN ''Failed''
		 WHEN tintTransactionStatus = 5 THEN ''Credited''
		 ELSE ''Not Authorized''
	END) AS vcTransactionStatus,	 
	[vcPGResponse],
	[tintType],
	[monAuthorizedAmt],
	[monCapturedAmt],
	[monRefundAmt],
	[vcCardHolder],
	[vcCreditCardNo],
	isnull((SELECT vcData FROM dbo.ListDetails WHERE numListItemID = isnull([numCardType],0)),''-'') as vcCardType,
	[vcCVV2],
	[tintValidMonth],
	[intValidYear],
	TH.dtCreatedDate,
	ISNULL(OBD.vcBizDocID, ISNULL(OM.vcPOppName,'''')) AS [vcBizOrderID],
	vcSignatureFile,
	ISNULL(tintOppType,0) AS [tintOppType],
	ISNULL(vcResponseCode,'''') vcResponseCode
FROM
	[dbo].[TransactionHistory] TH
LEFT JOIN dbo.OpportunityMaster OM ON OM.numoppID = TH.numOppID AND TH.numDomainID = OM.numDomainId
LEFT JOIN dbo.OpportunityBizDocs OBD ON OM.numOppId = OBD.numOppId AND OBD.numOppBizDocsId = TH.numOppBizDocsID '

    SET @strSQL = @strSQL + @strWHERE
    PRINT @strSQL
    EXEC SP_EXECUTESQL @strSQL
GO

--SELECT * FROM OpportunityMaster
--SELECT vcData FROM dbo.ListDetails WHERE numListItemID = 14883