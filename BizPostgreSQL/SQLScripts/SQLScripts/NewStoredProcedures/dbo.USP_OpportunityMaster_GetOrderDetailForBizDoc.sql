GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_OpportunityMaster_GetOrderDetailForBizDoc')
DROP PROCEDURE dbo.USP_OpportunityMaster_GetOrderDetailForBizDoc
GO

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[USP_OpportunityMaster_GetOrderDetailForBizDoc]
(
	@numDomainID NUMERIC(18,0)
	,@numOppID NUMERIC(18,0)
)
AS
BEGIN
	SELECT
		OpportunityMaster.numOppId
		,OpportunityMaster.tintOppType
		,OpportunityMaster.numContactID
		,DivisionMaster.numCurrencyID AS numBaseCurrencyID
		,ISNULL(OpportunityMaster.numCurrencyID,0) AS numCurrencyID
		,DivisionMaster.numDivisionID
		,CompanyInfo.numCompanyType
		,CompanyInfo.vcProfile
		,DivisionMaster.numDivisionID
		,CompanyInfo.vcCompanyName
		,ISNULL(OpportunityMaster.fltExchangeRate,1) AS fltExchangeRate
		,ISNULL(OpportunityMaster.intUsedShippingCompany,DivisionMaster.intShippingCompany) AS intUsedShippingCompany
	FROM
		OpportunityMaster
	INNER JOIN
		DivisionMaster
	ON
		OpportunityMaster.numDivisionId = DivisionMaster.numDivisionID
	INNER JOIN
		CompanyInfo
	ON
		DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
	WHERE
		OpportunityMaster.numDomainId = @numDomainID
		AND numOppId=@numOppID
END
GO