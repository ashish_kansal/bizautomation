SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_OpportunityMaster_GetDataForEDI856Acknowledgement')
DROP PROCEDURE dbo.USP_OpportunityMaster_GetDataForEDI856Acknowledgement
GO
CREATE PROCEDURE [dbo].[USP_OpportunityMaster_GetDataForEDI856Acknowledgement]
	@numDomainID NUMERIC(18,0)
	,@numOppID NUMERIC(18,0)
AS 
BEGIN
	SELECT
		numOppId
		,vcPOppName
	FROM
		OpportunityMaster
	WHERE
		numDomainId=@numDomainID
		AND numOppId=@numOppID
END
GO