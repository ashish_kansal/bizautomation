SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'usp_ManageDycCartFilter' ) 
    DROP PROCEDURE usp_ManageDycCartFilter
GO

CREATE PROCEDURE [dbo].[usp_ManageDycCartFilter]
    @numFieldID numeric(18, 0),
	@numFormID numeric(18, 0),
	@numSiteID numeric(18, 0),
	@numDomainID numeric(18, 0),
	@numFilterType int,
	@bitCustomField bit,
	@tintOrder tinyint
AS

SET NOCOUNT ON

IF EXISTS(SELECT [numFieldID], [numFormID], [numSiteID] FROM [dbo].[DycCartFilters] WHERE [numFieldID] = @numFieldID AND [numFormID] = @numFormID AND [numSiteID] = @numSiteID)
BEGIN
	UPDATE [dbo].[DycCartFilters] SET
		[numDomainID] = @numDomainID,
		[numFilterType] = @numFilterType,
		[bitCustomField] = @bitCustomField,
		[tintOrder] = @tintOrder
	WHERE
		[numFieldID] = @numFieldID
		AND [numFormID] = @numFormID
		AND [numSiteID] = @numSiteID
END
ELSE
BEGIN
	INSERT INTO [dbo].[DycCartFilters] (
		[numFieldID],
		[numFormID],
		[numSiteID],
		[numDomainID],
		[numFilterType],
		[bitCustomField],
		[tintOrder]
	) VALUES (
		@numFieldID,
		@numFormID,
		@numSiteID,
		@numDomainID,
		@numFilterType,
		@bitCustomField,
		@tintOrder
	)
END