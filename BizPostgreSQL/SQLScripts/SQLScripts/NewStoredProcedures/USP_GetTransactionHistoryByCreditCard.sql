GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_GetTransactionHistoryByCreditCard' ) 
    DROP PROCEDURE USP_GetTransactionHistoryByCreditCard
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Sachin Sadhu>
-- Create date: <Create Date,,10Oct2014>
-- Description:	<Description,,Work Flow Action :Update fields >
-- =============================================
Create PROCEDURE [dbo].[USP_GetTransactionHistoryByCreditCard]
	-- Add the parameters for the stored procedure here
	@numDomainId numeric(18, 0),
	@numContactId numeric(18, 0),
	@vcCreditCardNo VARCHAR(MAX)=''

AS
BEGIN
	select 
		TOP 1 * 
	from 
		TransactionHistory
	WHERE
		numDomainId=@numDomainId AND
		vcCreditCardNo=@vcCreditCardNo
	ORDER BY
		dtCreatedDate desc
END