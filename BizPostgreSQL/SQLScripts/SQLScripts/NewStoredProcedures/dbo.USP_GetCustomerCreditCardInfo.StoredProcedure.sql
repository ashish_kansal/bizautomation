/****** Object:  StoredProcedure [dbo].[USP_GetCustomerCreditCardInfo]    Script Date: 05/07/2009 22:00:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_GetCustomerCreditCardInfo' ) 
    DROP PROCEDURE USP_GetCustomerCreditCardInfo
GO
CREATE PROCEDURE [dbo].[USP_GetCustomerCreditCardInfo]
    @numOppBizDocId NUMERIC(9)=0,
    @numDomainId NUMERIC(9)=0,
    @numUserCntId NUMERIC(9) = 0,
    @bitFlag BIT = 0,
    @numCCInfoID numeric(9),
    @IsDefault BIT =0,
    @numDivisionID numeric(9)=0
AS 
    BEGIN
        IF @bitFlag = 0  AND @IsDefault = 0
            BEGIN
   
                DECLARE @numContactId NUMERIC(18, 0)
    
                SELECT  @numContactId = OM.numContactId
                FROM    [OpportunityMaster] OM
                        INNER JOIN [OpportunityBizDocs] BD ON OM.[numOppId] = BD.[numOppId]
                WHERE   BD.[numOppBizDocsId] = @numOppBizDocId
    
                SELECT  [numCCInfoID],
                        CC.[numContactId],
                        [vcCardHolder],
                        [vcCreditCardNo],
                        [vcCVV2],
                        ISNULL([numCardTypeID], 0) numCardTypeID,
                        [tintValidMonth],
                        [intValidYear]
                FROM    [CustomerCreditCardInfo] CC
                        INNER JOIN [AdditionalContactsInformation] ADC ON CC.[numContactId] = ADC.[numContactId]
                        INNER JOIN [Domain] D ON D.[numDomainId] = ADC.[numDomainID]
                WHERE   CC.[numContactId] = @numContactId
                        AND D.[bitSaveCreditCardInfo] = 1
            END
        ELSE 
            BEGIN
            IF @numDivisionID >0 
            BEGIN
            	SELECT   CC.*,dbo.ListDetails.vcData
					FROM    dbo.CustomerCreditCardInfo  CC
					INNER JOIN [AdditionalContactsInformation] ADC ON CC.[numContactId] = ADC.[numContactId]
					LEFT  JOIN dbo.ListDetails ON dbo.ListDetails.numListItemID= CC .numCardTypeID
					WHERE   CC.numContactId IN (SELECT numContactId FROM dbo.AdditionalContactsInformation WHERE numDivisionId=@numDivisionID) AND adc.numDomainID= @numDomainId /*AND CC.bitIsDefault = 1 */
					AND (CC.numCCInfoID = @numCCInfoID OR @numCCInfoID = 0)
					ORDER BY CC.bitIsDefault DESC 
            END
            ELSE IF @numCCInfoID = 0 AND @IsDefault=0
            BEGIN
                SELECT   CC.*,dbo.ListDetails.vcData
                FROM    dbo.CustomerCreditCardInfo  CC
                INNER JOIN [AdditionalContactsInformation] ADC ON CC.[numContactId] = ADC.[numContactId]
                LEFT  JOIN dbo.ListDetails ON dbo.ListDetails.numListItemID= CC.numCardTypeID
                WHERE   CC.numContactId = @numUserCntId AND adc.numDomainID= @numDomainId
            END
            ELSE
            BEGIN
				IF @IsDefault = 0 
				BEGIN
					SELECT   CC.*,dbo.ListDetails.vcData
					FROM    dbo.CustomerCreditCardInfo  CC
					INNER JOIN [AdditionalContactsInformation] ADC ON CC.[numContactId] = ADC.[numContactId]
					LEFT  JOIN dbo.ListDetails ON dbo.ListDetails.numListItemID= CC .numCardTypeID
					WHERE  adc.numDomainID= @numDomainId AND cc.numCCInfoID=@numCCInfoID	
				END
				ELSE
				BEGIN
					SELECT   CC.*,dbo.ListDetails.vcData
					FROM    dbo.CustomerCreditCardInfo  CC
					INNER JOIN [AdditionalContactsInformation] ADC ON CC.[numContactId] = ADC.[numContactId]
					LEFT  JOIN dbo.ListDetails ON dbo.ListDetails.numListItemID= CC .numCardTypeID
					WHERE   CC.numContactId = @numUserCntId AND adc.numDomainID= @numDomainId AND CC.bitIsDefault = 1	
				END
                
            END
            END
    END
