--USP_GetPriceTable 360
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetPriceTable')
DROP PROCEDURE USP_GetPriceTable
GO
CREATE PROCEDURE USP_GetPriceTable
   @numPriceRuleID NUMERIC(18,0),
    @numItemCode AS NUMERIC(18,0) 
	,@numDomainID AS NUMERIC(18,0)
	,@numCurrencyID NUMERIC(18,0)
AS 
BEGIN

	WITH PricingCte AS 
	(
		SELECT  pt.[numPricingID],
		pt.[numPriceRuleID],
		pt.[intFromQty],
		pt.[intToQty],
		pt.[tintRuleType],
		pt.[tintDiscountType],
		CAST(CONVERT(DECIMAL(18, 4), pt.[decDiscount]) AS VARCHAR) decDiscount,
		ROW_NUMBER() OVER(ORDER BY [numPricingID]) as rowId
		FROM    [PricingTable] pt
		WHERE   ISNULL([numPriceRuleID],0) = @numPriceRuleID AND ISNULL(numItemCode,0)=@numItemCode AND ISNULL(numCurrencyID,0) = @numCurrencyID
	)
	SELECT  [numPricingID]
	,[numPriceRuleID]
	,[intFromQty]
	,[intToQty]
	,[tintRuleType]
	,[tintDiscountType]
	,decDiscount
	,ISNULL(NULLIF(pnt.vcPriceLevelName, ''), CONCAT('Price Level ',rowId)) AS [vcName]
	FROM PricingCte
	LEFT JOIN PricingNamesTable pnt ON pnt.tintPriceLevel = PricingCte.rowId AND pnt.numDomainID=@numDomainID
	WHERE rowId IS NOT NULL OR (pnt.vcPriceLevelName IS NOT NULL AND pnt.vcPriceLevelName <> '')
	ORDER BY [numPricingID] 

END