GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_StagePercentageDetailsTaskTimeLog_Save')
DROP PROCEDURE dbo.USP_StagePercentageDetailsTaskTimeLog_Save
GO
CREATE PROCEDURE [dbo].[USP_StagePercentageDetailsTaskTimeLog_Save]
(
	@ID NUMERIC(18,0)
	,@numDomainID NUMERIC(18,0)
	,@numUserCntID NUMERIC(18,0)
	,@numTaskID NUMERIC(18,0)
	,@tintAction TINYINT
	,@dtActionTime DATETIME
	,@numProcessedQty FLOAT
	,@numReasonForPause NUMERIC(18,0)
	,@vcNotes VARCHAR(1000)
	,@bitManualEntry BIT
)
AS 
BEGIN

	IF NOT EXISTS (SELECT ID FROM StagePercentageDetailsTaskTimeLog WHERE numDomainID=@numDomainID AND numTaskID=@numTaskID AND tintAction=4)
	BEGIN
		IF @tintAction=1 AND EXISTS (SELECT ID FROM StagePercentageDetailsTaskTimeLog WHERE numDomainID=@numDomainID AND numTaskID=@numTaskID AND tintAction=1 AND ID <> ISNULL(@ID,0))
		BEGIN
			RAISERROR('TASK_IS_ALREADY_STARTED',16,1)
		END
		ELSE
		BEGIN
			IF EXISTS (SELECT ID FROM StagePercentageDetailsTaskTimeLog WHERE ID=@ID)
			BEGIN
				UPDATE
					StagePercentageDetailsTaskTimeLog
				SET
					tintAction = @tintAction
					,dtActionTime =  DATEADD(MINUTE, DATEDIFF(MINUTE,0,@dtActionTime),0)
					,numProcessedQty=@numProcessedQty
					,vcNotes = @vcNotes
					,bitManualEntry=@bitManualEntry
					,numModifiedBy = @numUserCntID
					,numReasonForPause = @numReasonForPause
					,dtModifiedDate = GETUTCDATE()
				WHERE
					ID = @ID			
			END
			ELSE
			BEGIN
				IF EXISTS (SELECT numTaskId FROM StagePercentageDetailsTask WHERE numDomainID=@numDomainID AND numTaskId=@numTaskID)
				BEGIN
					DECLARE @numTaskAssignee AS NUMERIC(18,0)
					SELECT @numTaskAssignee=numAssignTo FROM StagePercentageDetailsTask WHERE numDomainID=@numDomainID AND numTaskId=@numTaskID

					INSERT INTO StagePercentageDetailsTaskTimeLog
					(
						numDomainID
						,numUserCntID
						,numTaskID
						,tintAction
						,dtActionTime
						,numProcessedQty
						,numReasonForPause
						,vcNotes
						,bitManualEntry
					)
					VALUES
					(
						@numDomainID
						,@numTaskAssignee
						,@numTaskID
						,@tintAction
						,DATEADD(MINUTE, DATEDIFF(MINUTE,0,@dtActionTime),0)
						,@numProcessedQty
						,@numReasonForPause
						,@vcNotes
						,@bitManualEntry
					)
				END
				ELSE
				BEGIN
					RAISERROR('TASK_DOES_NOT_EXISTS',16,1)
				END
			END
		END
	END
	ELSE
	BEGIN
		RAISERROR('TASK_IS_MARKED_AS_FINISHED',16,1)
	END
	DECLARE @numProjectId NUMERIC=0
	SET @numProjectId = ISNULL((SELECT TOP 1 numProjectId FROM StagePercentageDetailsTask where numTaskId=@numTaskID),0)
	IF(@numProjectId>0)
	BEGIN
		DECLARE @dtmActualStartDate AS DATETIME
		DECLARE @dtmActualFinishDate AS DATETIME
		SET @dtmActualStartDate = (SELECT MIN(dtActionTime) FROM StagePercentageDetailsTaskTimeLog where numDomainID=@numDomainID AND numTaskID IN(SELECT numTaskId FROM StagePercentageDetailsTask WHERE numDomainID=@numDomainID AND numProjectId=@numProjectId))
		
		DECLARE @intTotalProgress AS INT =0
		DECLARE @intTotalTaskCount AS INT=0
		DECLARE @intTotalTaskClosed AS INT =0
		SET @intTotalTaskCount=(SELECT COUNT(*) FROM StagePercentageDetailsTask WHERE numProjectId=@numProjectId AND numDomainID=@numDomainID)
		SET @intTotalTaskClosed=(SELECT COUNT(distinct numTaskID) FROM StagePercentageDetailsTaskTimeLog WHERE numTaskId IN (SELECT numTaskId FROM StagePercentageDetailsTask WHERE numProjectId=@numProjectId AND numDomainID=@numDomainID) AND tintAction=4)
		IF(@intTotalTaskCount=@intTotalTaskClosed)
		BEGIN
			SET @dtmActualFinishDate = (SELECT MAX(dtActionTime) FROM StagePercentageDetailsTaskTimeLog where numDomainID=@numDomainID AND numTaskID IN(SELECT numTaskId FROM StagePercentageDetailsTask WHERE numDomainID=@numDomainID AND numProjectId=@numProjectId))
			UPDATE ProjectsMaster SET dtCompletionDate=@dtmActualFinishDate WHERE numProId=@numProjectId
		END
		IF(@intTotalTaskCount>0 AND @intTotalTaskClosed>0)
		BEGIN
			SET @intTotalProgress=ROUND(((@intTotalTaskClosed*100)/@intTotalTaskCount),0)
		END
		IF((SELECT COUNT(*) FROM ProjectProgress WHERE ISNULL(numProId,0)=@numProjectId AND numDomainId=@numDomainID)>0)
		BEGIN
			UPDATE ProjectProgress SET intTotalProgress=@intTotalProgress WHERE ISNULL(numProId,0)=@numProjectId AND numDomainId=@numDomainID
		END
		ELSE
		BEGIN
			INSERT INTO ProjectProgress(
				numProId,
				numOppId,
				numDomainId,
				intTotalProgress,
				numWorkOrderId
			)VALUES(
				@numProjectId,
				NULL,
				@numDomainID,
				@intTotalProgress,
				NULL
			)
		END

		DECLARE @numContractsLogId NUMERIC(18,0)
		DECLARE @numContractID NUMERIC(18,0)
		DECLARE @numUsedTime INT
		DECLARE @balanceContractTime INT

		-- REMOVE EXISTING CONTRACT LOG ENTRY
		IF EXISTS (SELECT 
					CL.numContractId  
				FROM 
					ContractsLog CL 
				WHERE
					CL.numDivisionID = @numDomainID
					AND CL.numReferenceId = @numTaskID
					AND vcDetails='1')
		BEGIN
			SELECT 
				@numContractsLogId=numContractsLogId
				,@numContractID = CL.numContractId
				,@numUsedTime = numUsedTime
			FROM
				ContractsLog CL 
			INNER JOIN
				Contracts C
			ON
				CL.numContractId = C.numContractId
			WHERE
				C.numDomainId = @numDomainID
				AND CL.numReferenceId = @numTaskID
				AND vcDetails='1'

			UPDATE Contracts SET timeUsed=ISNULL(timeUsed,0)-ISNULL(@numUsedTime,0),dtmModifiedOn=GETUTCDATE() WHERE numContractId=@numContractID
			DELETE FROM ContractsLog WHERE numContractsLogId=@numContractsLogId
		END

		IF EXISTS (SELECT 
						C.numContractId 
					FROM 
						ProjectsMaster PM 
					INNER JOIN 
						Contracts C 
					ON 
						PM.numDivisionId=C.numDivisonId 
					WHERE 
						PM.numDomainID = @numDomainID
						AND PM.numProID = @numProjectId
						AND C.numDomainId=@numDomainID
						AND C.numDivisonId=PM.numDivisionId 
						AND C.intType=1)
		BEGIN
			IF EXISTS (SELECT ID FROM StagePercentageDetailsTaskTimeLog WHERE numDomainID = @numDomainID AND numTaskId = @numTaskID AND tintAction = 4)
			BEGIN
				SELECT TOP 1
					@numContractID =  C.numContractId
				FROM
					ProjectsMaster PM
				INNER JOIN 
					Contracts C 
				ON 
					PM.numDivisionId=C.numDivisonId 
				WHERE 
					PM.numDomainID = @numDomainID
					AND PM.numProID = @numProjectId
					AND C.numDomainId=@numDomainID
					AND C.numDivisonId=PM.numDivisionId 
					AND C.intType=1
				ORDER BY
					C.numContractId DESC

				-- GET UPDATED TIME
				SET @numUsedTime = CAST(dbo.GetTimeSpendOnTaskByProject(@numDomainID,@numTaskID,2) AS INT)

				-- CREATE NEW ENTRY FOR
				UPDATE Contracts SET timeUsed=timeUsed+(ISNULL(@numUsedTime,0) * 60),dtmModifiedOn=GETUTCDATE() WHERE numContractId=@numContractID
				SET @balanceContractTime = (SELECT ISNULL((ISNULL(timeLeft,0)-ISNULL(timeUsed,0)),0) FROM Contracts WHERE numContractId=@numContractID)
				INSERT INTO ContractsLog 
				(
					intType, numDivisionID, numReferenceId, dtmCreatedOn, numCreatedBy,vcDetails,numUsedTime,numBalance,numContractId,tintRecordType
				)
				SELECT 
					1,Contracts.numDivisonId,@numTaskID,GETUTCDATE(),0,1,(ISNULL(@numUsedTime,0) * 60),@balanceContractTime,numContractId,1
				FROM 
					Contracts
				WHERE 
					numContractId = @numContractID
			END
		END
	END

	SELECT 
		ISNULL(numWorkOrderId,0) AS numWorkOrderId 
	FROM 
		StagePercentageDetailsTask 
	WHERE 
		numDomainID=@numDomainID 
		AND numTaskId=@numTaskID
END
GO