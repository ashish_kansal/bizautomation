--Created by Anoop Jayaraj
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetMonyTrailDetails')
DROP PROCEDURE USP_GetMonyTrailDetails
GO
CREATE PROCEDURE USP_GetMonyTrailDetails
@numDomainID as numeric(9),
@numDivisionID as numeric(9),
@numAccountID as numeric(9),
@dtFrom as datetime,
@dtTo as datetime,
@tintMode as tinyint,
@strSearch as varchar(100)
as

declare @strSQL as varchar(4000)

if @dtFrom='1753-01-01 00:00:00:000' set @dtFrom=null 
if @dtTo='1753-01-01 00:00:00:000' set @dtTo=null 



set @strSQL='Select numTransactionId,numJournalId,numDepositId,numCashCreditCardId,numCheckId,OppB.numBizDocsPaymentDetId,OppBDTL.numBizDocsPaymentDetailsId,
OppBiz1.numOppBizDocsId,OppM.numOppID,numCheckNo,OppBDTL.monAmount,dtDueDate,varCurrSymbol,vcCatgyName,vcBizDocID,
dbo.fn_GetListItemName(OppB.numPaymentMethod) PaymentMethod,
OppB.vcReference,vcMemo
from OpportunityBizDocsDetails OppB
Join OpportunityBizDocsPaymentDetails OppBDTL
on OppBDTL.numBizDocsPaymentDetId=OppB.numBizDocsPaymentDetId
Join OpportunityBizDocs OppBiz1 
on OppB.numBizDocsId=OppBiz1.numOppBizDocsId
Join OpportunityMaster OppM
on OppM.numOppID=OppBiz1.numOppID
left join Currency C
on C.numCurrencyID =OppB.numCurrencyID
left Join General_Journal_Details G
on G.numBizDocsPaymentDetailsId=OppBDTL.numBizDocsPaymentDetailsId
left join General_Journal_Header GH
on GH.numJournal_Id=G.numJournalId
Left join Chart_Of_Accounts CA
on CA.numAccountId=G.numChartAcntId
where bitIntegrated=1 
and OppB.numDomainID='+ convert(varchar(20),@numDomainID) +'
and OppM.tintOppType='+ convert(varchar(1),@tintMode)

if @numDivisionID>0  set @strSQL=@strSQL+ ' and numCustomerId=' + convert(varchar(20),@numDivisionID)

if @numAccountID>0  set @strSQL=@strSQL+ ' and G.numChartAcntId=' + convert(varchar(20),@numAccountID)

if @dtFrom is not null  set @strSQL=@strSQL+ ' and dtDueDate>=''' + convert(varchar(30),@dtFrom)+''''

if @dtTo is not null  set @strSQL=@strSQL+ ' and dtDueDate<=''' + convert(varchar(30),@dtTo)+''''

if @strSearch >''  set @strSQL=@strSQL+ ' and (OppB.vcReference like ''%' + @strSearch +'%''' 
+ ' or vcMemo like ''%' + @strSearch +'%''' 

if isnumeric(@strSearch)=1  and @strSearch<>''
begin
	set @strSQL=@strSQL+ ' or numCheckNo = ' + @strSearch  +')'
end
else if isnumeric(@strSearch)=0  and @strSearch<>''
begin
	set @strSQL=@strSQL +')'
end

if @tintMode=2
begin
set @strSQL=@strSQL+ ' union  Select numTransactionId,numJournalId,numDepositId,numCashCreditCardId,numCheckId,OppB.numBizDocsPaymentDetId,OppBDTL.numBizDocsPaymentDetailsId,
0 as numOppBizDocsId,0 as numOppID,numCheckNo,OppBDTL.monAmount,dtDueDate,varCurrSymbol,vcCatgyName,'''' vcBizDocID,
dbo.fn_GetListItemName(OppB.numPaymentMethod) PaymentMethod,
OppB.vcReference,vcMemo
from OpportunityBizDocsDetails OppB
Join OpportunityBizDocsPaymentDetails OppBDTL
on OppBDTL.numBizDocsPaymentDetId=OppB.numBizDocsPaymentDetId
left join Currency C
on C.numCurrencyID =OppB.numCurrencyID
left Join General_Journal_Details G
on G.numBizDocsPaymentDetailsId=OppBDTL.numBizDocsPaymentDetailsId
left join General_Journal_Header GH
on GH.numJournal_Id=G.numJournalId
Left join Chart_Of_Accounts CA
on CA.numAccountId=G.numChartAcntId
where  ([tintPaymentType] =2 or [tintPaymentType]=3)
and OppB.numDomainID='+ convert(varchar(20),@numDomainID) 

if @numDivisionID>0  set @strSQL=@strSQL+ ' and numCustomerId=' + convert(varchar(20),@numDivisionID)

if @numAccountID>0  set @strSQL=@strSQL+ ' and G.numChartAcntId=' + convert(varchar(20),@numAccountID)

if @dtFrom is not null  set @strSQL=@strSQL+ ' and dtDueDate>=''' + convert(varchar(30),@dtFrom)+''''

if @dtTo is not null  set @strSQL=@strSQL+ ' and dtDueDate<=''' + convert(varchar(30),@dtTo)+''''

if @strSearch >''  set @strSQL=@strSQL+ ' and (OppB.vcReference like ''%' + @strSearch +'%''' 
+ ' or vcMemo like ''%' + @strSearch +'%''' 

if isnumeric(@strSearch)=1  and @strSearch<>''
begin
	set @strSQL=@strSQL+ ' or numCheckNo = ' + @strSearch  +')'
end
else if isnumeric(@strSearch)=0  and @strSearch<>''
begin
	set @strSQL=@strSQL +')'
end
end

--print @strSQL
exec (@strSQL)
