GO
/****** Object:  StoredProcedure [dbo].[USP_DocumentWorkFlow]    Script Date: 02/11/2010 21:33:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj      
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_DocumentWorkFlow')
DROP PROCEDURE USP_DocumentWorkFlow
GO
CREATE PROCEDURE [dbo].[USP_DocumentWorkFlow]      
@numDocID as numeric(9)=0,      
@numContactID as numeric(9)=0,      
@cDocType as varchar(10)='',      
@byteMode as tinyint,
@vcComment as text='',
@numUserCntID AS NUMERIC(9)=0
as      
      
if @byteMode=1      
begin

/*validate if given contact is internal/external user*/
IF NOT EXISTS (SELECT * FROM dbo.UserMaster WHERE numUserDetailID = @numContactID )
BEGIN
	IF NOT EXISTS(SELECT * FROM dbo.ExtranetAccountsDtl EAD INNER JOIN dbo.AdditionalContactsInformation ACI ON EAD.numDomainID = ACI.numDomainID
	AND EAD.numContactID =ACI.numContactId WHERE EAD.numContactID=@numContactID)
	BEGIN
		raiserror('No_EXTRANET',16,1);
		RETURN ;		
	END
END



 delete from DocumentWorkflow where numDocID=@numDocID and numContactID=@numContactID and cDocType=@cDocType      
 insert into DocumentWorkflow(numDocID,numContactID,cDocType,dtCreatedDate)      
 values (@numDocID,@numContactID,@cDocType,GETUTCDATE()) 
 --Add to Tickler,bizdoc action item
 IF @cDocType='B'OR @cDocType='D'--bizdoc,generic docs & specific
 BEGIN
		DECLARE @numDivisionID NUMERIC
		DECLARE @numDomainID NUMERIC
		DECLARE @numBizActionId NUMERIC
		SELECT  @numDivisionID = numDivisionId,@numDomainID=numDomainID FROM dbo.AdditionalContactsInformation WHERE numContactId=@numContactID
	IF NOT EXISTS(SELECT * FROM BizDocAction B JOIN dbo.BizActionDetails BA
	ON BA.numBizActionId = B.numBizActionId WHERE B.numDomainID=@numDomainID AND B.numContactId = @numContactID AND BA.numOppBizDocsId=@numDocID)
	BEGIN
		insert into dbo.BizDocAction(numContactId ,numDivisionId,numStatus,numDomainID,dtCreatedDate,numAssign,numCreatedBy,bitTask,numBizDocAppId)
		values (@numContactID,@numDivisionID,0,@numDomainID,getdate(),@numContactID,@numUserCntID,972,0);
		set @numBizActionId=@@IDENTITY							
		
--		PRINT @numBizActionId
		-- btDocType =2 for Document approval request, =1 for bizdoc
		INSERT INTO BizActionDetails (numBizActionId,numOppBizDocsId,btStatus,btDocType)
		VALUES (@numBizActionId,@numDocID,0,CASE WHEN @cDocType='D' THEN 2 ELSE 1 END)
	END
		 
		

 END
 
end
else if @byteMode=2      
begin      
 delete from DocumentWorkflow where numDocID=@numDocID and numContactID=@numContactID and cDocType=@cDocType      
  IF @cDocType='B'OR @cDocType='D'--bizdoc,generic docs & specific
 BEGIN
	DELETE FROM dbo.BizDocAction WHERE numBizActionId IN (SELECT numBizActionId FROM dbo.BizActionDetails WHERE numOppBizDocsId=@numDocID)
	DELETE FROM dbo.BizActionDetails WHERE numOppBizDocsId=@numDocID
 END
 
end      
else if @byteMode=3      
begin      
	 update DocumentWorkflow set dtApprovedOn=getutcdate(),tintApprove=1,vcComment =@vcComment  where numDocID=@numDocID and numContactID=@numContactID and cDocType=@cDocType
	 
	 IF @cDocType='B'OR @cDocType='D'--bizdoc,generic docs & specific
	 BEGIN
	 
		UPDATE dbo.BizActionDetails SET btStatus=1 WHERE numOppBizDocsId=@numDocID AND btDocType = CASE @cDocType WHEN 'B' THEN 1 WHEN 'D' THEN 2 END 
		UPDATE dbo.BizDocAction SET numStatus=1 WHERE numBizActionId IN (SELECT numBizActionId FROM dbo.BizActionDetails WHERE numOppBizDocsId=@numDocID)
	 END
end    
else if @byteMode=4     
begin      
 update DocumentWorkflow set dtApprovedOn=getutcdate(),tintApprove=2,vcComment =@vcComment where numDocID=@numDocID and numContactID=@numContactID and cDocType=@cDocType      
end
