SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON            
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ListDetailsName_Save')
DROP PROCEDURE USP_ListDetailsName_Save
GO
CREATE PROCEDURE [dbo].[USP_ListDetailsName_Save]          
@numDomainID AS NUMERIC(18,0),
@numListID AS NUMERIC(18,0),
@numListItemID AS NUMERIC(18,0),
@vcName AS VARCHAR(300)
AS             
BEGIN
	IF EXISTS (SELECT * FROM ListDetailsName WHERE numDomainID=@numDomainID AND numListID = @numListID AND numListItemID=@numListItemID)
	BEGIN
		UPDATE 
			ListDetailsName
		SET 
			vcName = ISNULL(@vcName,'')
		WHERE
			numDomainID=@numDomainID 
			AND numListID = @numListID 
			AND numListItemID=@numListItemID
	END
	ELSE
	BEGIN
		INSERT INTO ListDetailsName
		(
			numDomainID
			,numListID
			,numListItemID
			,vcName
		)
		VALUES
		(
			@numDomainID
			,@numListID
			,@numListItemID
			,ISNULL(@vcName,'')
		)

	END
END