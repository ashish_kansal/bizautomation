GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetPackingDetail')
DROP PROCEDURE USP_GetPackingDetail
GO
CREATE PROCEDURE USP_GetPackingDetail
    @numDomainID AS NUMERIC(9),
	@vcBizDocsIds VARCHAR(MAX),
	@tintMode AS TINYINT 
AS
BEGIN
	-- PICK LIST
	IF @tintMode = 1
	BEGIN
		SELECT
			ISNULL(vcItemName,'') AS vcItemName,
			ISNULL((SELECT TOP 1 vcPathForTImage FROM ItemImages WHERE numDomainId=@numDomainID AND ItemImages.numItemCode=Item.numItemCode AND bitDefault=1 AND bitIsImage=1),'') AS vcImage,
			ISNULL(vcModelID,'') AS vcModelID,
			ISNULL(txtItemDesc,'') AS txtItemDesc,
			ISNULL(vcWareHouse,'') AS vcWareHouse,
			ISNULL(vcLocation,'') AS vcLocation,
			(CASE WHEN ISNULL(Item.numItemGroup,0) > 0 THEN WareHouseItems.vcWHSKU ELSE Item.vcSKU END) AS vcSKU,
			(CASE WHEN ISNULL(Item.numItemGroup,0) > 0 THEN WareHouseItems.vcBarCode ELSE Item.numBarCodeId END) AS vcUPC,
			CAST(SUM(numUnitHour) AS FLOAT) AS numUnitHour
		FROM
			OpportunityBizDocItems
		INNER JOIN
			WareHouseItems
		ON
			OpportunityBizDocItems.numWarehouseItmsID = WareHouseItems.numWareHouseItemID
		INNER JOIN
			Warehouses
		ON
			WareHouseItems.numWareHouseID = Warehouses.numWareHouseID
		INNER JOIN
			Item
		ON
			OpportunityBizDocItems.numItemCode = Item.numItemCode
		WHERE
			numOppBizDocID IN ( SELECT * FROM dbo.splitIds(@vcBizDocsIds,','))
		GROUP BY
			Item.numItemCode,
			vcItemName,
			vcModelID,
			txtItemDesc,
			vcWareHouse,
			vcLocation,
			(CASE WHEN ISNULL(Item.numItemGroup,0) > 0 THEN WareHouseItems.vcWHSKU ELSE Item.vcSKU END),
			(CASE WHEN ISNULL(Item.numItemGroup,0) > 0 THEN WareHouseItems.vcBarCode ELSE Item.numBarCodeId END)
		ORDER BY
			vcWareHouse ASC,
			vcLocation ASC,
			vcItemName ASC
	END
	-- GROUP LIST
	ELSE IF @tintMode = 2
	BEGIN
		SELECT
			ISNULL(vcItemName,'') AS vcItemName,
			ISNULL((SELECT TOP 1 vcPathForTImage FROM ItemImages WHERE numDomainId=@numDomainID AND ItemImages.numItemCode=Item.numItemCode AND bitDefault=1 AND bitIsImage=1),'') AS vcImage,
			ISNULL(vcModelID,'') AS vcModelID,
			ISNULL(txtItemDesc,'') AS txtItemDesc,
			OpportunityBizDocs.numOppBizDocsId,
			(CASE WHEN ISNULL(Item.numItemGroup,0) > 0 THEN WareHouseItems.vcWHSKU ELSE Item.vcSKU END) AS vcSKU,
			CAST(numUnitHour AS FLOAT) AS numUnitHour
		FROM
			OpportunityBizDocs
		INNER JOIN
			OpportunityBizDocItems
		ON
			OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
		INNER JOIN
			WareHouseItems
		ON
			OpportunityBizDocItems.numWarehouseItmsID = WareHouseItems.numWareHouseItemID
		INNER JOIN
			Warehouses
		ON
			WareHouseItems.numWareHouseID = Warehouses.numWareHouseID
		INNER JOIN
			Item
		ON
			OpportunityBizDocItems.numItemCode = Item.numItemCode
		WHERE
			numOppBizDocID IN ( SELECT * FROM dbo.splitIds(@vcBizDocsIds,','))
		ORDER BY
			vcWareHouse ASC,
			vcLocation ASC,
			vcItemName ASC
	END
END