set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go

    
-- =============================================  
-- Author:  <Ajit Kumar Singh>  
-- Create date: <Monday, September 29, 2008>  
-- Description: <This procedure is used for Save the existing address in DivisionMaster Table>  
-- =============================================  

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_SaveExistingAddress')
DROP PROCEDURE USP_SaveExistingAddress
GO
CREATE PROCEDURE [dbo].[USP_SaveExistingAddress]
@numNewDomainId as numeric(9),        
@numOldDomainId as numeric(9),        

@numNewUserId as numeric(9),        
@numOldUserId as numeric(9),        

@numNewDivisionId as numeric(9),
@numOldDivisionId as numeric(9),

@vcAddressType as varchar(15),
@numPrimaryContactCheck as varchar(10)
as    

declare @vcBillStreet	varchar(50)
declare @vcBillCity	varchar(50)
declare @vcBilState	numeric(18, 0)
declare @vcBillPostCode	varchar(15)
declare @vcBillCountry	numeric(18, 0)

declare @numContactId numeric(18, 0)

Begin      
	-- Fetching Existing Company Address.
	if (@numPrimaryContactCheck ='Yes')
		begin
			select @numContactId=numcontactid from AdditionalContactsInformation 
			where numDomainID=@numNewDomainId and numDivisionID=@numNewDivisionId;

			SELECT @vcBillStreet=vcStreet, @vcBillCity=vcCity, @vcBillPostCode=vcPostalCode, 
			@vcBilState=numState, @vcBillCountry=numCountry 
			FROM dbo.AddressDetails WHERE numDomainID=@numNewDomainId AND numRecordID=@numContactId AND tintAddressOf=1 AND tintAddressType=0 AND bitIsPrimary=1;
	
		end
	if (@numPrimaryContactCheck ='No')
	begin	
		select  @vcBillStreet=vcStreet, @vcBillCity=vcCity,
				@vcBilState=numState, @vcBillPostCode=vcPostalCode,
				@vcBillCountry=numCountry
		 FROM     dbo.AddressDetails
		   WHERE    numDomainID = @numNewDomainId 
					AND numRecordID = @numNewDivisionId
					AND tintAddressOf = 2
					AND tintAddressType = 1
					AND bitIsPrimary=1
	end
	-- End of the Code.
	
	-- Updating Existing Company Address.
	if (@vcAddressType ='BillTo')
	begin
		update AddressDetails set vcStreet=@vcBillStreet, vcCity=@vcBillCity,
				numState=@vcBilState, vcPostalCode=@vcBillPostCode,numCountry=@vcBillCountry
		 FROM     dbo.AddressDetails
		   WHERE    numDomainID = @numOldDomainId 
					AND numRecordID = @numOldDivisionId
					AND tintAddressOf = 2
					AND tintAddressType = 1
					AND bitIsPrimary=1
	end
	if (@vcAddressType ='ShipTo')
	begin
		update AddressDetails set vcStreet=@vcBillStreet,vcCity=@vcBillCity,
				numState=@vcBilState,vcPostalCode=@vcBillPostCode,numCountry=@vcBillCountry
		from AddressDetails where numDomainID = @numOldDomainId 
					AND numRecordID = @numOldDivisionId
					AND tintAddressOf = 2
					AND tintAddressType = 2
					AND bitIsPrimary=1
	end
	-- End of the Code.
End  

