SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_AssembledItem_GetByID')
DROP PROCEDURE USP_AssembledItem_GetByID
GO
CREATE PROCEDURE [dbo].[USP_AssembledItem_GetByID]  
	@numDomainID NUMERIC(18,0),
	@ID NUMERIC(18,0)
AS
BEGIN

	SELECT 
		AI.ID,
		AI.numAssembledQty,
		AI.monAverageCost,
		I.vcItemName,
		I.numAssetChartAcntId
	FROM
		AssembledItem AI
	INNER JOIN
		Item I
	ON
		AI.numItemCode = I.numItemCode
	WHERE
		AI.numDomainID=@numDomainID
		AND AI.ID = @ID

	SELECT
		AIC.*,
		I.vcItemName,
		I.numAssetChartAcntId
	FROM 
		AssembledItemChilds AIC
	INNER JOIN
		Item I
	ON
		AIC.numItemCode = I.numItemCode
	WHERE
		numAssembledItemID=@ID

END

