SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
         
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_WareHouseItmsDTL_TransferSerialLotNo')
DROP PROCEDURE USP_WareHouseItmsDTL_TransferSerialLotNo
GO
CREATE PROCEDURE [dbo].[USP_WareHouseItmsDTL_TransferSerialLotNo]            
@numOppID AS NUMERIC(18,0),
@numOppItemID AS numeric(18,0),
@numFormWarehouseID AS NUMERIC(18,0),
@numToWarehouseID AS NUMERIC(18,0),
@bitSerialized AS BIT,
@bitLotNo AS BIT     
AS
BEGIN TRY
BEGIN TRANSACTION 
	
	IF @bitSerialized = 1
	BEGIN
		--TRANSFER SERIAL NUMBERS TO SELECTED WAREHOUSE
		UPDATE 
			WareHouseItmsDTL 
		SET 
			numWareHouseItemID=@numToWarehouseID,
			numQty=1
		WHERE 
			numWareHouseItmsDTLID IN 
			(
				SELECT 
					t1.numWarehouseItmsDTLID
				FROM 
					OppWarehouseSerializedItem t1
				WHERE 
					t1.numOppID=@numOppID AND
					t1.numOppItemID=@numOppItemID AND
					t1.numWarehouseItmsID = @numFormWarehouseID)
	END

	IF @bitLotNo = 1
	BEGIN
		DECLARE @TableLotNoSelected TABLE
		(
			ID INT IDENTITY(1,1),
			numWarehouseItmsDTLID NUMERIC(18,0),
			numOppID NUMERIC(18,0),
			numOppItemID NUMERIC(18,0),
			numWarehouseItmsID NUMERIC(18,0),
			numQty NUMERIC(18,0)
		)
		
		INSERT INTO 
			@TableLotNoSelected 
		SELECT 
			numWarehouseItmsDTLID,
			numOppID,
			numOppItemID,
			numWarehouseItmsID,
			numQty
		FROM 
			OppWarehouseSerializedItem 
		WHERE 
			numOppID=@numOppID 
			AND numOppItemID=@numOppItemID 
			AND numWarehouseItmsID = @numFormWarehouseID

		DECLARE @i INT = 1
		DECLARE @Count INT
		SELECT @Count=COUNT(*) FROM @TableLotNoSelected

		WHILE @i <= @Count
		BEGIN
			DECLARE @numWarehouseItmsDTLID AS NUMERIC(18,0)
			DECLARE @vcSerialNo AS VARCHAR(100)
			DECLARE @dExpiryDate AS DATETIME
			DECLARE @bitAddedFromPO AS BIT
			DECLARE @numQty AS INT

			SELECT @numQty=numQty,@numWarehouseItmsDTLID=numWarehouseItmsDTLID FROM @TableLotNoSelected WHERE ID= @i
			SELECT @vcSerialNo=vcSerialNo,@dExpiryDate=dExpirationDate,@bitAddedFromPO=ISNULL(bitAddedFromPO,0) FROM WareHouseItmsDTL WHERE numWareHouseItmsDTLID=@numWarehouseItmsDTLID

			--IF SAME LOT NUMBER IS ALREADY AVAILABLE IN DESTINATION WAREHOUSE THEN UPDATE QUANITY ELSE CREATE NEW ENTRY
			IF EXISTS (SELECT * FROM WareHouseItmsDTL WHERE numWareHouseItemID = @numToWarehouseID AND vcSerialNo=@vcSerialNo)
			BEGIN
				UPDATE
					WareHouseItmsDTL
				SET
					numQty = ISNULL(numQty,0) + ISNULL(@numQty,0)
				WHERE
					numWareHouseItemID = @numToWarehouseID AND vcSerialNo=@vcSerialNo
			END
			ELSE
			BEGIN
				INSERT INTO WareHouseItmsDTL
				(
					numWareHouseItemID,
					vcSerialNo,
					numQty,
					dExpirationDate,
					bitAddedFromPO
				)
				VALUES
				(
					@numToWarehouseID,
					@vcSerialNo,
					@numQty,
					@dExpiryDate,
					@bitAddedFromPO
				)
			END

			SET @i = @i + 1
		END 
	END


	--UPDATE TRANSFER COMPLETES SO USERS WILL BE ABLE TO USE SERIAL/LOT NUMBR OTHERWISE USER WILL NOT BE ABLE TO SELECT SERIAL/LOT NUMBR
	UPDATE OppWarehouseSerializedItem SET bitTransferComplete=1 WHERE numOppID=@numOppID AND numOppItemID=@numOppItemID AND numWarehouseItmsID = @numFormWarehouseID

COMMIT
END TRY
BEGIN CATCH
  -- Whoops, there was an error
  IF @@TRANCOUNT > 0
     ROLLBACK

  -- Raise an error with the details of the exception
  DECLARE @ErrMsg nvarchar(4000), @ErrSeverity int
  SELECT @ErrMsg = ERROR_MESSAGE(),
         @ErrSeverity = ERROR_SEVERITY()

  RAISERROR(@ErrMsg, @ErrSeverity, 1)
END CATCH

GO
