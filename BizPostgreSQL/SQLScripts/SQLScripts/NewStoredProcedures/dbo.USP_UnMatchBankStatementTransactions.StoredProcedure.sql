
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_UnMatchBankStatementTransactions')
	DROP PROCEDURE USP_UnMatchBankStatementTransactions
GO

/****** Added By : Joseph ******/
/****** Deletes the Transaction Detail from Table BankStatementTransaction if the Transaction is not Mapped******/


CREATE PROCEDURE [dbo].[USP_UnMatchBankStatementTransactions]
	@numTransactionID numeric(18, 0)
AS

SET NOCOUNT ON

--UPDATE [dbo].[BankStatementTransactions] SET numReferenceID = 0 , tintReferenceType = 0 
DELETE FROM BankTransactionMapping  

WHERE
	[numTransactionID] = @numTransactionID


GO
