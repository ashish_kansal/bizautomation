/****** Object:  StoredProcedure [dbo].[USP_UpdatedomainDetails]    Script Date: 05/07/2009 21:56:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj                                      
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_UpdatedomainDefaultContactAddress')
DROP PROCEDURE USP_UpdatedomainDefaultContactAddress
GO
CREATE PROCEDURE [dbo].[USP_UpdatedomainDefaultContactAddress]                                      
@numDomainID as numeric(9)=0,                                      
@bitAutoPopulateAddress as bit=null,
@tintPoulateAddressTo as tinyint=null
as                                      
BEGIN                                      
	UPDATE 
		Domain                                       
	SET      
		bitAutoPopulateAddress = @bitAutoPopulateAddress,
		tintPoulateAddressTo = @tintPoulateAddressTo
	WHERE 
		numDomainId=@numDomainID
END
GO