/****** Object:  StoredProcedure [USP_ManageReturnHeaderItems]    ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ManageReturnHeaderItems')
DROP PROCEDURE USP_ManageReturnHeaderItems
GO
CREATE PROCEDURE [USP_ManageReturnHeaderItems]                              
(
	@numReturnHeaderID numeric(18, 0),
    @vcRMA varchar(100),
    @vcBizDocName varchar(100),
    @numDomainId numeric(18, 0),
    @numUserCntID AS NUMERIC(9)=0,
    @numDivisionId numeric(18, 0),
    @numContactId numeric(18, 0),
    @numOppId numeric(18, 0),
    @tintReturnType tinyint,
    @numReturnReason numeric(18, 0),
    @numReturnStatus numeric(18, 0),
    @monAmount money,
    @monAmountUsed money,
    @monTotalTax money,
    @monTotalDiscount money,
    @tintReceiveType tinyint,
    @vcComments text,
    @strItems TEXT=''
)            
AS                                           
BEGIN 

DECLARE  @hDocItem INT
 
 IF @numReturnHeaderID=0                                  
  BEGIN                              
     
    INSERT INTO .[ReturnHeader] ([vcRMA], [vcBizDocName], [numDomainId], [numDivisionId], [numContactId], [numOppId], [tintReturnType], [numReturnReason], [numReturnStatus], [monAmount], [monAmountUsed], [monTotalTax], [monTotalDiscount], [tintReceiveType], [vcComments], [numCreatedBy], [dtCreatedDate])
		SELECT @vcRMA, @vcBizDocName, @numDomainId, @numDivisionId, @numContactId, @numOppId, @tintReturnType, @numReturnReason, @numReturnStatus, @monAmount, @monAmountUsed, @monTotalTax, @monTotalDiscount, @tintReceiveType, @vcComments, @numUserCntID,GETUTCDATE()
                    
	SET @numReturnHeaderID = @@IDENTITY                                        
	SELECT @numReturnHeaderID
  END                            
 ELSE IF @numReturnHeaderID<>0                        
  BEGIN                        
   	UPDATE [ReturnHeader]
	SET    [vcRMA] = @vcRMA, [vcBizDocName] = @vcBizDocName, [numDivisionId] = @numDivisionId, [numContactId] = @numContactId,
		   [numOppId] = @numOppId, [tintReturnType] = @tintReturnType, [numReturnReason] = @numReturnReason, [numReturnStatus] = @numReturnStatus,
		   [monAmount] = @monAmount, [monAmountUsed] = @monAmountUsed, [monTotalTax] = @monTotalTax, [monTotalDiscount] = @monTotalDiscount,
		   [tintReceiveType] = @tintReceiveType, [vcComments] = @vcComments, [numModifiedBy] = @numUserCntID, [dtModifiedDate] = GETUTCDATE()
	WHERE  [numDomainId] = @numDomainId AND [numReturnHeaderID] = @numReturnHeaderID
  END            
  
  
  IF CONVERT(VARCHAR(10),@strItems) <> ''
        BEGIN
           DECLARE @hDoc3 INT                                                                                                                                                                
                EXEC sp_xml_preparedocument @hDoc3 OUTPUT, @strItems                                                                                                             
                    SELECT * INTO #temp FROM      OPENXML (@hdoc3,'/NewDataSet/Item',2)
												WITH (
												numReturnItemID NUMERIC, 				
								numItemCode NUMERIC,
								numUnitHour NUMERIC,
								numUnitHourReceived NUMERIC,
								monPrice MONEY,
								monTotAmount MONEY,
								vcItemDesc VARCHAR(1000),
								numWareHouseItemID	NUMERIC,	
								vcModelID	VARCHAR(200),
								vcManufacturer	VARCHAR(250),
								numUOMId NUMERIC,
								bitMarkupDiscount BIT)

                 EXEC sp_xml_removedocument @hDoc3 
                 
            
              DELETE FROM ReturnItems WHERE numReturnHeaderID = @numReturnHeaderID
              AND numReturnItemID NOT IN (SELECT numReturnItemID FROM #temp WHERE numReturnItemID>0)
			
			
			  INSERT INTO [ReturnItems] ([numReturnHeaderID], [numItemCode], [numUnitHour], [numUnitHourReceived], 
				[monPrice], [monTotAmount],[vcItemDesc], [numWareHouseItemID], [vcModelID], [vcManufacturer], [numUOMId], [bitMarkupDiscount])
			  SELECT @numReturnHeaderID,
					 numItemCode ,
					 numUnitHour ,
					 numUnitHourReceived,
					 monPrice,
					 monTotAmount,
					 vcItemDesc,
					 numWareHouseItemID,
					 vcModelID,
					 vcManufacturer,
					 numUOMId,
					 bitMarkupDiscount
			  FROM  #temp WHERE numReturnItemID=0
					
					
				UPDATE [ReturnItems]
					SET    [numItemCode] = X.numItemCode, [numUnitHour] = X.numUnitHour, [numUnitHourReceived] = X.numUnitHourReceived, 
						   [monPrice] = X.monPrice, [monTotAmount] = X.monTotAmount, [vcItemDesc] = X.vcItemDesc, [numWareHouseItemID] = X.numWareHouseItemID,
						   [vcModelID] = X.vcModelID, [vcManufacturer] = X.vcManufacturer, [numUOMId] = X.numUOMId, [bitMarkupDiscount] = X.bitMarkupDiscount
				 FROM    #temp as X 
					  WHERE X.numReturnItemID =ReturnItems.numReturnItemID AND ReturnItems.numReturnHeaderID = @numReturnHeaderID

          drop table #temp
  END 
END
GO
