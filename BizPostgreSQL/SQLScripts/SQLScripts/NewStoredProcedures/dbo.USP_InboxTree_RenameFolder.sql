SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_InboxTree_RenameFolder')
DROP PROCEDURE USP_InboxTree_RenameFolder
GO
CREATE PROCEDURE [dbo].[USP_InboxTree_RenameFolder]                                       
@numDomainID AS NUMERIC(18,0),              
@numUserCntID AS NUMERIC(18,0),
@numNodeID AS NUMERIC(18,0),
@vcName AS VARCHAR(100)
AS
BEGIN
	UPDATE InboxTreeSort SET vcNodeName=@vcName WHERE numNodeID=@numNodeID AND numDomainID=@numDomainID AND numUserCntID=@numUserCntID
END
