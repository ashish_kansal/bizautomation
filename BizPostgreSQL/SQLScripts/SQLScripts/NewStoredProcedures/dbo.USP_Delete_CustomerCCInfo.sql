
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_Delete_CustomerCCInfo')
DROP PROCEDURE USP_Delete_CustomerCCInfo
GO
CREATE PROCEDURE [dbo].[USP_Delete_CustomerCCInfo]  
 @CCInfoId as bigint,
@ContactId as bigint
as
begin
	delete from CustomerCreditCardInfo where numCCInfoID=@CCInfoId and numContactId=@ContactId
end