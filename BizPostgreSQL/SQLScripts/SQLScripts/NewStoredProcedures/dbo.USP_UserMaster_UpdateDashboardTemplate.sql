GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_UserMaster_UpdateDashboardTemplate')
DROP PROCEDURE USP_UserMaster_UpdateDashboardTemplate
GO
CREATE PROCEDURE [dbo].[USP_UserMaster_UpdateDashboardTemplate]
	@numDomainID NUMERIC(18,0)
	,@numUserCntID AS NUMERIC(18,0)
	,@numTemplateID AS NUMERIC(18,0)
AS
BEGIN 

	BEGIN
		UPDATE 
			UserMaster
		SET
			numDashboardTemplateID = @numTemplateID
		WHERE
			numDomainID = @numDomainID	AND  numUserDetailId = @numUserCntID
	END

	
END

GO


