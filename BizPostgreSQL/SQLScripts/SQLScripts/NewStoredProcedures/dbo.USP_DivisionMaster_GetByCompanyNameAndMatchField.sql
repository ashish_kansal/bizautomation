GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_DivisionMaster_GetByCompanyNameAndMatchField' )
    DROP PROCEDURE USP_DivisionMaster_GetByCompanyNameAndMatchField
GO
CREATE PROCEDURE [dbo].[USP_DivisionMaster_GetByCompanyNameAndMatchField]
	@numDomainID NUMERIC(18,0)
	,@vcCompanyName VARCHAR(300)
	,@numMatchField NUMERIC(18,0)
	,@vcMatchValue VARCHAR(300)
AS
BEGIN
	IF @numMatchField = 14 -- Assigned To
	BEGIN
		SELECT TOP 1 
			DM.numDivisionID
			,ADC.numContactID 
		FROM 
			CompanyInfo CI 
		INNER JOIN 
			DivisionMaster DM 
		ON 
			CI.numCompanyId=DM.numCompanyID 
		LEFT JOIN
			AdditionalContactsInformation ADC
		ON
			ADC.numDivisionId = DM.numDivisionID
			AND ISNULL(bitPrimaryContact,0) = 1
		WHERE 
			CI.numDomainID=@numDomainID
			AND LOWER(vcCompanyName) = LOWER(@vcCompanyName) 
			AND numAssignedTo=CAST(@vcMatchValue AS NUMERIC)
	END
	ELSE IF @numMatchField = 28 -- Industry
	BEGIN
		SELECT TOP 1 
			DM.numDivisionID
			,ADC.numContactID 
		FROM 
			CompanyInfo CI 
		INNER JOIN 
			DivisionMaster DM 
		ON 
			CI.numCompanyId=DM.numCompanyID 
		LEFT JOIN
			AdditionalContactsInformation ADC
		ON
			ADC.numDivisionId = DM.numDivisionID
			AND ISNULL(bitPrimaryContact,0) = 1
		WHERE 
			CI.numDomainID=@numDomainID
			AND LOWER(vcCompanyName) = LOWER(@vcCompanyName)
			AND numCompanyIndustry=CAST(@vcMatchValue AS NUMERIC)
	END
	ELSE IF @numMatchField = 539 -- Organization ID
	BEGIN
		SELECT TOP 1 
			DM.numDivisionID
			,ADC.numContactID 
		FROM 
			CompanyInfo CI 
		INNER JOIN 
			DivisionMaster DM 
		ON 
			CI.numCompanyId=DM.numCompanyID 
		LEFT JOIN
			AdditionalContactsInformation ADC
		ON
			ADC.numDivisionId = DM.numDivisionID
			AND ISNULL(bitPrimaryContact,0) = 1
		WHERE 
			CI.numDomainID=@numDomainID
			AND LOWER(vcCompanyName) = LOWER(@vcCompanyName)
			AND DM.numDivisionID=CAST(@vcMatchValue AS NUMERIC)
	END
	ELSE IF @numMatchField = 3 OR @numMatchField = 535 -- Organization Name OR Customer PO#
	BEGIN
		SELECT TOP 1 
			DM.numDivisionID
			,ADC.numContactID 
		FROM 
			CompanyInfo CI 
		INNER JOIN 
			DivisionMaster DM 
		ON 
			CI.numCompanyId=DM.numCompanyID 
		LEFT JOIN
			AdditionalContactsInformation ADC
		ON
			ADC.numDivisionId = DM.numDivisionID
			AND ISNULL(bitPrimaryContact,0) = 1
		WHERE 
			CI.numDomainID=@numDomainID
			AND LOWER(vcCompanyName) = LOWER(@vcCompanyName)
	END
	ELSE IF @numMatchField = 10 -- Organization phone
	BEGIN
		SELECT TOP 1 
			DM.numDivisionID
			,ADC.numContactID 
		FROM 
			CompanyInfo CI 
		INNER JOIN 
			DivisionMaster DM 
		ON 
			CI.numCompanyId=DM.numCompanyID 
		LEFT JOIN
			AdditionalContactsInformation ADC
		ON
			ADC.numDivisionId = DM.numDivisionID
			AND ISNULL(bitPrimaryContact,0) = 1
		WHERE 
			CI.numDomainID=@numDomainID
			AND LOWER(vcCompanyName) = LOWER(@vcCompanyName)
			AND vcComPhone=@vcMatchValue
	END
	ELSE IF @numMatchField = 5 -- Organization profile
	BEGIN
		SELECT TOP 1 
			DM.numDivisionID
			,ADC.numContactID 
		FROM 
			CompanyInfo CI 
		INNER JOIN 
			DivisionMaster DM 
		ON 
			CI.numCompanyId=DM.numCompanyID 
		LEFT JOIN
			AdditionalContactsInformation ADC
		ON
			ADC.numDivisionId = DM.numDivisionID
			AND ISNULL(bitPrimaryContact,0) = 1
		WHERE 
			CI.numDomainID=@numDomainID
			AND LOWER(vcCompanyName) = LOWER(@vcCompanyName)
			AND vcProfile=CAST(@vcMatchValue AS NUMERIC)
	END
	ELSE IF @numMatchField = 30 --Organization Status
	BEGIN
		SELECT TOP 1 
			DM.numDivisionID
			,ADC.numContactID 
		FROM 
			CompanyInfo CI 
		INNER JOIN 
			DivisionMaster DM 
		ON 
			CI.numCompanyId=DM.numCompanyID 
		LEFT JOIN
			AdditionalContactsInformation ADC
		ON
			ADC.numDivisionId = DM.numDivisionID
			AND ISNULL(bitPrimaryContact,0) = 1
		WHERE 
			CI.numDomainID=@numDomainID
			AND LOWER(vcCompanyName) = LOWER(@vcCompanyName)
			AND numStatusID=CAST(@vcMatchValue AS NUMERIC)
	END
	ELSE IF @numMatchField = 6 -- Relationship
	BEGIN
		SELECT TOP 1 
			DM.numDivisionID
			,ADC.numContactID 
		FROM 
			CompanyInfo CI 
		INNER JOIN 
			DivisionMaster DM 
		ON 
			CI.numCompanyId=DM.numCompanyID 
		LEFT JOIN
			AdditionalContactsInformation ADC
		ON
			ADC.numDivisionId = DM.numDivisionID
			AND ISNULL(bitPrimaryContact,0) = 1
		WHERE 
			CI.numDomainID=@numDomainID
			AND LOWER(vcCompanyName) = LOWER(@vcCompanyName)
			AND numCompanyType=CAST(@vcMatchValue AS NUMERIC)
	END
	ELSE IF @numMatchField = 451 -- Relationship Type
	BEGIN
		SELECT TOP 1 
			DM.numDivisionID
			,ADC.numContactID 
		FROM 
			CompanyInfo CI 
		INNER JOIN 
			DivisionMaster DM 
		ON 
			CI.numCompanyId=DM.numCompanyID 
		LEFT JOIN
			AdditionalContactsInformation ADC
		ON
			ADC.numDivisionId = DM.numDivisionID
			AND ISNULL(bitPrimaryContact,0) = 1
		WHERE 
			CI.numDomainID=@numDomainID
			AND LOWER(vcCompanyName) = LOWER(@vcCompanyName)
			AND tintCRMType=CAST(@vcMatchValue AS NUMERIC)
	END
	ELSE IF @numMatchField = 21 -- Territory
	BEGIN
		SELECT TOP 1 
			DM.numDivisionID
			,ADC.numContactID 
		FROM 
			CompanyInfo CI 
		INNER JOIN 
			DivisionMaster DM 
		ON 
			CI.numCompanyId=DM.numCompanyID 
		LEFT JOIN
			AdditionalContactsInformation ADC
		ON
			ADC.numDivisionId = DM.numDivisionID
			AND ISNULL(bitPrimaryContact,0) = 1
		WHERE 
			CI.numDomainID=@numDomainID
			AND LOWER(vcCompanyName) = LOWER(@vcCompanyName)
			AND numTerID=CAST(@vcMatchValue AS NUMERIC)
	END
END
GO