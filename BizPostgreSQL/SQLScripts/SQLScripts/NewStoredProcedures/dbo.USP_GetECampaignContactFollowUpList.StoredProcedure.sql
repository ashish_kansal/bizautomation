GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetECampaignContactFollowUpList')
DROP PROCEDURE USP_GetECampaignContactFollowUpList
GO
CREATE PROCEDURE USP_GetECampaignContactFollowUpList
AS 
    BEGIN
	DECLARE @LastDateTime DATETIME
	DECLARE @CurrentDateTime DATETIME
	
	SELECT @LastDateTime = [dtLastDateTimeECampaign] FROM [WindowsServiceHistory]
	SET @CurrentDateTime = DATEADD(minute, DATEpart(minute,GETUTCDATE()) ,DATEADD(hour,DATEpart(hour,GETUTCDATE()),dbo.[GetUTCDateWithoutTime]()))

    SELECT  
			E.[numECampaignID],
			ED.[numECampDTLId],
            C.numContactID,
            ED.[numFollowUpID],
            DTL.[numConECampDTLID],
            C.[numConEmailCampID],
                DATEADD(MINUTE, ISNULL(E.fltTimeZone,0) * 60, DTL.dtExecutionDate) AS StartDate,
                DATEADD(MINUTE,(ISNULL(E.fltTimeZone,0)-2)*60,@LastDateTime) Between1,
                DATEADD(MINUTE,ISNULL(E.fltTimeZone,0) * 60,@CurrentDateTime) Between2,
                E.fltTimeZone
    FROM    ConECampaign C
            JOIN ConECampaignDTL DTL ON numConEmailCampID = numConECampID
            JOIN ECampaignDTLs ED ON ED.numECampDTLId = DTL.numECampDTLID
            JOIN [ECampaign] E ON ED.[numECampID] = E.[numECampaignID]
            JOIN AdditionalContactsInformation A ON A.numContactID = C.[numContactID]
    WHERE   ISNULL(bitFollowUpStatus,0) = 0
			AND 
            [bitEngaged] = 1
            AND [numFollowUpID] > 0
           AND DATEADD(MINUTE, ISNULL(E.fltTimeZone,0) * 60, DTL.dtExecutionDate)
            BETWEEN DATEADD(MINUTE,(ISNULL(E.fltTimeZone,0)-2)*60,@LastDateTime) AND DATEADD(MINUTE,ISNULL(E.fltTimeZone,0) * 60,@CurrentDateTime)
END