
/****** Object:  StoredProcedure [dbo].[usp_VerifyPartnerCode]    Script Date: 07/26/2008 16:15:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj                    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetPartnerSource')
DROP PROCEDURE USP_GetPartnerSource
GO
CREATE PROCEDURE [dbo].[USP_GetPartnerSource]                                                                                    
 @numDomainID  numeric=0                                           
AS                                                                       
BEGIN   
	SELECT 
		D.numDivisionID
		,D.vcPartnerCode + '-' + C.vcCompanyName AS vcPartner
	FROM 
		DivisionMaster AS D 
	LEFT JOIN 
		CompanyInfo AS C
	ON 
		D.numCompanyID=C.numCompanyID
	WHERE 
		D.numDomainID=@numDomainID 
		AND D.vcPartnerCode <> ''
	ORDER BY
		vcCompanyName
END
