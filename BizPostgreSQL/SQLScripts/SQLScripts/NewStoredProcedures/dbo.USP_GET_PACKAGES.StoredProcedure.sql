GO
/****** Object:  StoredProcedure [dbo].[USP_GET_PACKAGES]    Script Date: 30/07/2012 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO

IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_GET_PACKAGES' ) 
    DROP PROCEDURE USP_GET_PACKAGES
GO
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE USP_GET_PACKAGES
@DomainId NUMERIC(18,0)
AS 
BEGIN
	SELECT
		0 AS numCustomPackageID,
		0 AS numPackageTypeID,
		'None' AS vcPackageName,
		0.00 AS fltWidth,
		0.00 AS fltHeight,
		0.00 AS fltLength,
		0.00 AS fltTotalWeight,
		0 AS numShippingCompanyID,
		'' AS vcShippingCompany,
		CONCAT(0,',',0,',',0,',',0,',',0,',',0)  AS [ValueDimension]
	UNION 
	SELECT
		numItemCode AS numCustomPackageID,
		0 AS numPackageTypeID,
		vcItemName AS vcPackageName,
		fltWidth,
		fltHeight,
		fltLength,
		fltWeight AS fltTotalWeight,
		0 AS numShippingCompanyID,
		'Custom Box' AS vcShippingCompany,
		CONCAT(0,',',fltWidth,',',fltHeight,',',fltLength,',',fltWeight,',',numItemCode)  AS [ValueDimension]
	FROM 
		Item WITH (NOLOCK)
	where 
		numDomainID=@DomainId
		AND ISNULL(bitContainer,0)=1
		
END
GO

