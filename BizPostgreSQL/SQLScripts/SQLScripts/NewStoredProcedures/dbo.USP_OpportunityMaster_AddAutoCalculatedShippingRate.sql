GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_OpportunityMaster_AddAutoCalculatedShippingRate')
DROP PROCEDURE dbo.USP_OpportunityMaster_AddAutoCalculatedShippingRate
GO

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[USP_OpportunityMaster_AddAutoCalculatedShippingRate]
(
	@numDomainID NUMERIC(18,0)
	,@numOppID NUMERIC(18,0)
	,@dtItemReleaseDate DATE
	,@numShipFromWarehouse NUMERIC(18,0)
	,@monShipRate DECIMAL(20,5)
	,@vcDescription VARCHAR(300)
)
AS
BEGIN
	DECLARE @numShippingServiceItemID NUMERIC(18,0)

	SELECT 
		@numShippingServiceItemID=ISNULL(numShippingServiceItemID,0)
	FROM 
		 Domain
	WHERE 
		numDomainId=@numDomainID 

	IF NOT EXISTS (SELECT numItemCode FROM  Item WHERE numDomainID=@numDomainID AND numItemCode=@numShippingServiceItemID)
	BEGIN
		RAISERROR('SHIPPING_ITEM_NOT_SET',16,1)
		RETURN
	END

	DECLARE @numOppItemID NUMERIC(18,0)

	SET @numOppItemID = ISNULL((SELECT 
									OI.numoppitemtCode 
								FROM 
									OpportunityItems OI 
								INNER JOIN 
									OpportunityMaster OM 
								ON 
									OI.numOppId=OM.numOppId 
								WHERE 
									OI.numOppId=@numOppID 
									AND OI.numItemCode=@numShippingServiceItemID 
									AND ISNULL(OI.numShipFromWarehouse,0)=@numShipFromWarehouse 
									AND ISNULL(OI.ItemReleaseDate,OM.dtReleaseDate)=@dtItemReleaseDate),0)

	IF ISNULL(@numOppItemID,0) > 0
	BEGIN
		IF NOT EXISTS (SELECT OBI.numOppBizDocItemID FROM OpportunityBizDocs OB INNER JOIN OpportunityBizDocItems OBI ON OB.numOppBizDocsId=OBI.numOppBizDocID WHERE OB.numOppId=@numOppID AND OBI.numOppItemID=@numOppItemID)
		BEGIN
			UPDATE 
				OpportunityItems
			SET
				numUnitHour = 1
				,monPrice = @monShipRate
				,monTotAmount = @monShipRate
				,monTotAmtBefDiscount = @monShipRate
			FROM
				OpportunityItems OI 
			WHERE 
				numoppitemtCode=@numOppItemID

			UPDATE 
				OpportunityBizDocItems 
			SET 
				numUnitHour=1
				,monPrice=@monShipRate
				,monTotAmount=@monShipRate
				,monTotAmtBefDiscount=@monShipRate 
			WHERE 
				numOppItemID=@numOppItemID
		END
	END
	ELSE
	BEGIN
		INSERT INTO OpportunityItems
		(
			numOppId
			,numItemCode
			,numUnitHour
			,monPrice
			,monTotAmount
			,monTotAmtBefDiscount
			,bitDiscountType
			,fltDiscount
			,vcItemName
			,vcItemDesc
			,vcSKU
			,vcManufacturer
			,vcModelID
			,numShipFromWarehouse
			,ItemReleaseDate
		)
		SELECT 
			@numOppID
			,@numShippingServiceItemID
			,1
			,@monShipRate
			,@monShipRate
			,@monShipRate
			,1
			,0
			,vcItemName
			,@vcDescription
			,vcSKU
			,vcManufacturer
			,vcModelID
			,@numShipFromWarehouse
			,@dtItemReleaseDate
		FROM
			Item
		WHERE
			numDomainID=@numDomainID
			AND numItemCode = @numShippingServiceItemID

		SET @numOppItemID = SCOPE_IDENTITY()

		--Insert Tax for Opportunity Items
		INSERT INTO dbo.OpportunityItemsTaxItems 
		(
			numOppId,
			numOppItemID,
			numTaxItemID,
			numTaxID
		) 
		SELECT 
			@numOppId,
			OI.numoppitemtCode,
			TI.numTaxItemID,
			0
		FROM 
			dbo.OpportunityItems OI 
		JOIN 
			dbo.ItemTax IT 
		ON 
			OI.numItemCode=IT.numItemCode 
		JOIN
			TaxItems TI 
		ON 
			TI.numTaxItemID = IT.numTaxItemID 
		WHERE 
			OI.numOppId=@numOppID 
			AND OI.numoppitemtCode = @numOppItemID
			AND IT.bitApplicable=1 
		UNION
		SELECT 
			@numOppId,
			OI.numoppitemtCode,
			0,
			0
		FROM 
			dbo.OpportunityItems OI 
		JOIN 
			dbo.Item I 
		ON 
			OI.numItemCode=I.numItemCode
		WHERE 
			OI.numOppId=@numOppID 
			AND OI.numoppitemtCode = @numOppItemID
			AND I.bitTaxable=1 
		UNION
		SELECT
			@numOppId,
			OI.numoppitemtCode,
			1,
			TD.numTaxID
		FROM
			dbo.OpportunityItems OI 
		INNER JOIN
			ItemTax IT
		ON
			IT.numItemCode = OI.numItemCode
		INNER JOIN
			TaxDetails TD
		ON
			TD.numTaxID = IT.numTaxID
			AND TD.numDomainId = @numDomainId
		WHERE
			OI.numOppId = @numOppID
			AND OI.numoppitemtCode = @numOppItemID
	END
	
	UPDATE 
		OpportunityMaster 
	SET 
		monDealAmount=dbo.GetDealAmount(@numOppID,getdate(),0)
	WHERE 
		numDomainID=@numDomainID 
		AND numOppID=@numOppID
END
GO