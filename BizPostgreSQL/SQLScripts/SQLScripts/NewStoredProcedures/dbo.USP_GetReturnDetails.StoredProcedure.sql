
GO
/****** Object:  StoredProcedure [dbo].[USP_GetReturnDetails]    Script Date: 05/07/2009 21:20:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- exec USP_GetReturnDetails 1134
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetReturnDetails')
DROP PROCEDURE USP_GetReturnDetails
GO
CREATE PROCEDURE [dbo].[USP_GetReturnDetails]
               @numOppId NUMERIC(9)
AS
  BEGIN
    SELECT I.numItemCode,
           I.vcItemName Item,
           OI.vcItemDesc [Desc],
           I.vcModelID,
           OI.monPrice,
           (OI.monPrice
              * R.numQtyToReturn) AS ReturnAmount,
           R.numQtyToReturn Unit
    FROM   RETURNS R
           INNER JOIN OpportunityItems OI
             ON R.numOppItemCode = OI.numoppitemtCode
           INNER JOIN Item I
             ON I.numItemCode = OI.numItemCode
    WHERE  R.numOppId = @numOppId
    
    SELECT OM.txtComments,
           D.vcBizDocImagePath,
           OM.vcPOppName,
           OM.numContactId,
           ISNULL(ACI.vcEmail,'') vcEmail
    FROM   OpportunityMaster OM
           INNER JOIN Domain D
             ON D.numDomainID = OM.numDomainID
           INNER JOIN [AdditionalContactsInformation] ACI
             ON ACI.[numContactId] = OM.[numContactId]
    WHERE  OM.numOppId = @numOppId
  END

  
