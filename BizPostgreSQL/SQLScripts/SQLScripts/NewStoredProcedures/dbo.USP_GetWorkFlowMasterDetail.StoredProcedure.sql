GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetWorkFlowMasterDetail')
DROP PROCEDURE USP_GetWorkFlowMasterDetail
GO
Create PROCEDURE [dbo].[USP_GetWorkFlowMasterDetail]     
    @numDomainID numeric(18, 0),
	@numWFID numeric(18, 0),
	@ClientTimeZoneOffset Int 
as                 

SELECT 
		isnull(dbo.fn_GetContactName(WF.numCreatedBy),'-')+' '+convert(varchar(20),DateAdd(minute, -@ClientTimeZoneOffset,WF.dtCreatedDate )) as CreatedBy                         
		,isnull(dbo.fn_GetContactName(WF.numModifiedBy),'-')+' '+convert(varchar(20),DateAdd(minute, -@ClientTimeZoneOffset,WF.dtModifiedDate )) as ModifiedBy 
		,*
 FROM WorkFlowMaster WF WHERE WF.numDomainID=@numDomainID AND WF.numWFID=@numWFID
--please

CREATE TABLE #tempField(numFieldID NUMERIC,  
vcFieldName NVARCHAR(50),vcDbColumnName NVARCHAR(50),vcOrigDbColumnName NVARCHAR(50),vcFieldDataType CHAR(1),  
vcAssociatedControlType NVARCHAR(50),vcListItemType VARCHAR(3),numListID NUMERIC,bitCustom BIT,vcLookBackTableName NVARCHAR(50),bitAllowFiltering BIT,bitAllowEdit BIT)  
DECLARE @numFormId AS NUMERIC(18,0)
SELECT @numFormId=numFormID from dbo.WorkFlowMaster WHERE numDomainID=@numDomainID  AND numWFID=@numWFID

IF (@numFormId=71)
	BEGIN

	SET @numFormId=49
	
	END
--Regular Fields  
INSERT INTO #tempField  
 SELECT numFieldID,vcFieldName,  
     vcDbColumnName,vcOrigDbColumnName,vcFieldDataType,vcAssociatedControlType,  
     vcListItemType,numListID,CAST(0 AS BIT) AS bitCustom,vcLookBackTableName,bitAllowFiltering,bitAllowEdit  
  FROM View_DynamicDefaultColumns  
  where numFormId=@numFormId and numDomainID=@numDomainID   
  AND ISNULL(bitWorkFlowField,0)=1 AND ISNULL(bitCustom,0)=0  
  
  
DECLARE @vcLocationID VARCHAR(100);SET @vcLocationID='0'  
Select @vcLocationID =isnull(vcLocationID,'0') from dynamicFormMaster where numFormID=@numFormId  
  
IF @numFormID IN (69,70,49,72,73,124)
BEGIN
	IF LEN(ISNULL(@vcLocationID ,'')) > 0
	BEGIN
		SET @vcLocationID = CONCAT(@vcLocationID,',1,12,13,14')
	END
	ELSE
	BEGIN
		SET @vcLocationID = '1,12,13,14'
	END
END


--Custom Fields     
INSERT INTO #tempField  
SELECT ISNULL(CFM.Fld_Id,0) as numFieldID,CFM.Fld_label as vcFieldName,  
    'Cust'+Convert(varchar(10),Fld_Id) as vcDbColumnName,'Cust'+Convert(varchar(10),Fld_Id) AS vcOrigDbColumnName,CASE CFM.Fld_type WHEN 'SelectBox' THEN 'V' WHEN 'DateField' THEN 'D' ELSE 'V' END AS vcFieldDataType,  
    CFM.Fld_type AS vcAssociatedControlType,CASE WHEN CFM.Fld_type='SelectBox' THEN 'LI' ELSE '' END AS vcListItemType,isnull(CFM.numListID,0) as numListID,  
    CAST(1 AS BIT) AS bitCustom,L.vcCustomLookBackTableName AS vcLookBackTableName,0 AS bitAllowFiltering,0 AS bitAllowEdit  
FROM CFW_Fld_Master CFM LEFT JOIN CFW_Validation V ON V.numFieldID = CFM.Fld_id  
     JOIN CFW_Loc_Master L on CFM.GRP_ID=L.Loc_Id  
            WHERE   CFM.numDomainID = @numDomainId  
     AND GRP_ID IN( SELECT ID FROM dbo.SplitIDs(@vcLocationID,','))  



--Fire Trigger Events on Fields
SELECT numFieldID,bitCustom,numWFID,numWFTriggerFieldID,CONVERT(NVARCHAR(80),CONVERT(NVARCHAR(20),numFieldId) +'_' + CASE WHEN bitCustom=0 THEN 'False' ELSE 'True' End) AS FieldID,(SELECT #tempField.vcFieldName FROM #tempField WHERE #tempField.numFieldID=
WorkFlowTriggerFieldList.numFieldID)AS FieldName FROM WorkFlowTriggerFieldList WHERE numWFID=@numWFID

SELECT 
	*
	, CONVERT(NVARCHAR(80),CONVERT(NVARCHAR(20),WorkFlowConditionList.numFieldId) +'_' + CASE WHEN WorkFlowConditionList.bitCustom=0 THEN 'False' ELSE 'True' End) AS FieldID
	,t1.vcFieldName AS FieldName
	,t1.vcLookBackTableName
	,CASE WHEN  vcFilterOperator='eq' THEN 'equals' WHEN vcFilterOperator='ne' THEN 'not equal to' WHEN vcFilterOperator='gt' THEN 'greater than' WHEN vcFilterOperator='ge' THEN 'greater or equal' WHEN vcFilterOperator='lt' THEN 'less than' WHEN vcFilterOperator='le' THEN 'less or equal' WHEN (vcFilterOperator = 'Before' OR vcFilterOperator = 'After') THEN 'is' ELSE 'NA' END AS FilterOperator  
FROM 
	WorkFlowConditionList 
LEFT JOIN
	#tempField t1
ON
	 t1.numFieldID=WorkFlowConditionList.numFieldID
WHERE 
	numWFID=@numWFID



--Declare @vcApprovalName varchar(Max)
--Set @vcApprovalName=''

--IF(@numFormId=49)
--BEGIN
--	Declare @test varchar(100)
--	SELECT @test= vcApproval from WorkFlowActionList where numWFID=@numWFID

--	SELECT @vcApprovalName=@vcApprovalName+Coalesce (isnull(vcFirstname,'')+' '+isnull(vcLastName,'')+ ', ','' )from AdditionalContactsInformation  where numContactID in (SELECT items FROM dbo.Split(@test,','))	
--END






SELECT *,CASE WHEN tintActionType=1 THEN 
				   (SELECT  vcDocName              
					FROM    GenericDocuments  
					WHERE   numDocCategory = 369  
					AND tintDocumentType =1 --1 =generic,2=specific  
					AND ISNULL(vcDocumentSection,'') <> 'M'  
					AND numDomainId = @numDomainID  AND ISNULL(VcFileName,'') not LIKE '#SYS#%' AND numGenericDocID=numTemplateID)                
             WHEN	tintActionType=2 then (Select TemplateName From tblActionItemData  where numdomainId=@numDomainID AND RowID=numTemplateID) 
              ELSE '0'  END EmailTemplate ,
         CASE WHEN vcEmailToType='1' THEN 'Owner of trigger record' WHEN vcEmailToType='2' THEN 'Assignee of trigger record'  WHEN vcEmailToType='1,2' THEN 'Owner of trigger record,Assignee of trigger record'  WHEN   vcEmailToType='3' THEN 'Primary contact of trigger record' WHEN   vcEmailToType='1,3' THEN 'Owner of trigger record,Primary contact of trigger record' WHEN vcEmailToType='2,3' THEN 'Primary contact of trigger record,Assignee of trigger record'  WHEN vcEmailToType='1,2,3' THEN  'Owner of trigger record,Primary contact of trigger record,Assignee of trigger record' ELSE NULL END AS EmailToType,
         CASE WHEN tintTicklerActionAssignedTo=1 THEN 'Owner of trigger record' ELSE 'Assignee of trigger record' END AS TicklerAssignedTo,
		 (select vcTemplateName from BizDocTemplate where numBizDocTempID=numBizDocTemplateID ) as  vcBizDocTemplate, 
		 (SELECT isnull(vcRenamedListName,vcData) as vcData FROM listdetails Ld        
						left join listorder LO 
						on Ld.numListItemID= LO.numListItemID and Lo.numDomainId = @numDomainID 
						WHERE Ld.numListID=27 AND (constFlag=1 or Ld.numDomainID=@numDomainID )    AND  Ld.numListItemID=numBizDocTypeID) as vcBizDoc ,							
		 (SELECT Case When numOpptype=2 then 'Purchase' else 'Sales' end As BizDocType from BizDocTemplate WHERE numBizDocTempID=numBizDocTemplateID) as vcBizDocType ,
		 (SELECT numOpptype from BizDocTemplate where numBizDocTempID=numBizDocTemplateID) as numOppType,
		 ISNULL(vcSMSText,'') as vcSMSText    ,
		 ISNULL(vcEmailSendTo,'') as vcEmailSendTo ,
		 ISNULL(vcMailBody,'') as vcMailBody ,
		 ISNULL(vcMailSubject,'') as vcMailSubject,
		 ISNULL( vcApproval,'') as vcApprovalName
FROM dbo.WorkFlowActionList WHERE numWFID=@numWFID

 
SELECT *,CONVERT(NVARCHAR(80),CONVERT(NVARCHAR(20),numFieldId) +'_' + CASE WHEN bitCustom=0 THEN 'False' ELSE 'True' End) AS FieldID,(SELECT #tempField.vcFieldName FROM #tempField WHERE #tempField.numFieldID=WorkFlowActionUpdateFields.numFieldID)AS FieldName  FROM WorkFlowActionUpdateFields WHERE numWFActionID IN(SELECT numWFActionID FROM WorkFlowActionList WHERE numWFID=@numWFID)
DROP TABLE #tempField