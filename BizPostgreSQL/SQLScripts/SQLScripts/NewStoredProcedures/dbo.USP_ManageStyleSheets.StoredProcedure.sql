/****** Object:  StoredProcedure [dbo].[USP_ManageStyleSheets]    Script Date: 08/08/2009 15:03:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ManageStyleSheets')
DROP PROCEDURE USP_ManageStyleSheets
GO
CREATE PROCEDURE [dbo].[USP_ManageStyleSheets]
 @numCssID NUMERIC(9)=0,
 @StyleName varchar(50),
 @StyleFileName varchar(50),
 @numSiteID NUMERIC(9),
 @numDomainID NUMERIC(9),
 @tintType TINYINT=0 -- 0- css , 1- javascript
AS
 
DECLARE @intDisplayOrder AS INT 

IF @numCssID = 0 AND (NOT EXISTS (SELECT [StyleName] FROM StyleSheets WHERE [StyleName] = @StyleName 
																		AND [numSiteID] = @numSiteID
																		AND [tintType] = @tintType))
BEGIN
IF @numSiteID = 0
	SET @numSiteID = NULL;
   IF @tintType = 0	
      BEGIN
			INSERT INTO StyleSheets (
                        [StyleName],
                        [StyleFileName],
                        [numSiteID],
                        [numDomainID],
                        [tintType] 
                                     )
           VALUES (
                        @StyleName,
                        @StyleFileName,
                        @numSiteID,
                        @numDomainID,
                        @tintType
                   )	  	
	 END
   
   ELSE IF @tintType = 1
     BEGIN
            SELECT @intDisplayOrder = (ISNULL(MAX(intDisplayOrder),0) + 1)  FROM dbo.StyleSheets WHERE  numSiteID = @numSiteID AND numDomainID =  @numDomainID
            
	 	  	INSERT INTO StyleSheets (
                        [StyleName],
                        [StyleFileName],
                        [numSiteID],
                        [numDomainID],
                        [tintType],
                        [intDisplayOrder]
                         
                                     )
           VALUES (
                        @StyleName,
                        @StyleFileName,
                        @numSiteID,
                        @numDomainID,
                        @tintType,
                        @intDisplayOrder 
                   )
	 END 
   ELSE IF @tintType = 3
	 BEGIN
			INSERT INTO StyleSheets (
                        [StyleName],
                        [StyleFileName],
                        [numSiteID],
                        [numDomainID],
                        [tintType] 
                                     )
           VALUES (
                        @StyleName,
                        @StyleFileName,
                        @numSiteID,
                        @numDomainID,
                        @tintType
                   )	  	
	 END
	 ELSE IF @tintType = 4
     BEGIN
            SELECT @intDisplayOrder = (ISNULL(MAX(intDisplayOrder),0) + 1)  FROM dbo.StyleSheets WHERE  numSiteID = @numSiteID AND numDomainID =  @numDomainID
            
	 	  	INSERT INTO StyleSheets (
                        [StyleName],
                        [StyleFileName],
                        [numSiteID],
                        [numDomainID],
                        [tintType],
                        [intDisplayOrder]
                         
                                     )
           VALUES (
                        @StyleName,
                        @StyleFileName,
                        @numSiteID,
                        @numDomainID,
                        @tintType,
                        @intDisplayOrder 
                   )
	 END 
   
 SELECT SCOPE_IDENTITY()
END
ELSE BEGIN
 UPDATE StyleSheets SET 
  [StyleName] = @StyleName
--  [StyleFileName] = @StyleFileName
 WHERE [numCssID] = @numCssID AND numDomainID =@numDomainID
 SELECT @numCssID ;
END


