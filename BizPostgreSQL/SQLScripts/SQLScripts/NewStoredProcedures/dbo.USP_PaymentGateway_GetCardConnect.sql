GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_PaymentGateway_GetCardConnect')
DROP PROCEDURE USP_PaymentGateway_GetCardConnect
GO
CREATE PROCEDURE [dbo].[USP_PaymentGateway_GetCardConnect]  
	@numDomainID NUMERIC(18,0)
	,@numSiteID NUMERIC(18,0)
AS
BEGIN
	SELECT
		PaymentGatewayDTLID.intPaymentGateWay
		,PaymentGatewayDTLID.vcFirstFldValue
		,PaymentGatewayDTLID.vcSecndFldValue
		,(CASE WHEN ISNULL(PaymentGatewayDTLID.vcThirdFldValue,'') = '' THEN PaymentGateway.vcGateWayName ELSE PaymentGatewayDTLID.vcThirdFldValue END) vcThirdFldValue
		,PaymentGatewayDTLID.bitTest
	FROM
		PaymentGatewayDTLID
	INNER JOIN
		PaymentGateway
	ON
		PaymentGatewayDTLID.intPaymentGateWay = PaymentGateway.intPaymentGateWay
	WHERE
		numDomainID = @numDomainID
		AND numSiteId = @numSiteID
		AND PaymentGatewayDTLID.intPaymentGateWay IN (9,10)
END