GO
SET ANSI_NULLS on
GO
SET QUOTED_IDENTIFIER on
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_UpdateFollowUpDatesByContactId')
DROP PROCEDURE usp_UpdateFollowUpDatesByContactId
GO
CREATE PROCEDURE [dbo].[usp_UpdateFollowUpDatesByContactId]          
(    
	 @numDomainID as numeric(9)=null,          
	 @strContactIds as text='',
	 @bitUpdateFollowUpStatus AS BIT=0,
	 @numFollowUpStatusId AS NUMERIC(18,0)=0
)
AS
BEGIN
	UPDATE 
		DivisionMaster
	SET 
		dtLastFollowUp=getutcdate()
	WHERE	
		numDomainID=@numDomainID AND numDivisionID IN(
	SELECT numDivisionId FROM AdditionalContactsInformation WHERE numContactId IN(
	SELECT Items FROM Split(@strContactIds,',')))
	IF(@bitUpdateFollowUpStatus=1 AND @numFollowUpStatusId>0)
	BEGIN
		UPDATE 
			DivisionMaster
		SET 
			numFollowUpStatus=@numFollowUpStatusId
		WHERE	
			numDomainID=@numDomainID AND numDivisionID IN(
		SELECT numDivisionId FROM AdditionalContactsInformation WHERE numContactId IN(
		SELECT Items FROM Split(@strContactIds,',')))
	END
END     