GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_DeleteBroadcastHistory')
DROP PROCEDURE USP_DeleteBroadcastHistory
GO
CREATE PROCEDURE USP_DeleteBroadcastHistory
@numBroadCastId NUMERIC(9)=0
AS 
BEGIN
IF EXISTS(select * from Broadcast WHERE [numBroadCastId] = @numBroadCastId and [bitBroadcasted] = 0)
 BEGIN
  DELETE FROM [BroadCastDtls] WHERE [numBroadCastID]=@numBroadCastId	 
  DELETE FROM [Broadcast] WHERE [numBroadCastId] = @numBroadCastId
 END		
END