
GO
/****** Object:  StoredProcedure [dbo].[USP_ManageMetaTags]    Script Date: 08/08/2009 16:12:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ManageMetaTags')
DROP PROCEDURE USP_ManageMetaTags
GO
CREATE PROCEDURE [dbo].[USP_ManageMetaTags]
	@numMetaID NUMERIC(9)=0,
	@tintMetatagFor tinyint, -- 1 for Page, 2 for Items,
	@numReferenceID NUMERIC(9),
	@vcMetaKeywords VARCHAR(4000),
	@vcMetaDescription VARCHAR(4000),
	@vcPageTitle VARCHAR(4000)
AS
BEGIN
	SET NOCOUNT ON
	IF @numMetaID = 0 
	BEGIN
		IF NOT EXISTS (SELECT numMetaID FROM MetaTags WHERE numReferenceID=@numReferenceID AND tintMetatagFor=@tintMetatagFor)
		BEGIN
			INSERT INTO MetaTags 
			(
				[vcPageTitle],
	   			[vcMetaKeywords],
				[vcMetaDescription],
				[tintMetatagFor],
				[numReferenceID]
			)
			VALUES 
			(
				@vcPageTitle,
	   			@vcMetaKeywords,
				@vcMetaDescription,
				@tintMetatagFor,
				@numReferenceID
			)
		END
		ELSE
		BEGIN
			UPDATE 
				MetaTags 
			SET 
				[vcPageTitle] = @vcPageTitle,
	   			[vcMetaKeywords] = @vcMetaKeywords,
				[vcMetaDescription] = @vcMetaDescription,
				[tintMetatagFor] = @tintMetatagFor,
				[numReferenceID] = @numReferenceID
			WHERE 
				numReferenceID=@numReferenceID 
				AND tintMetatagFor=@tintMetatagFor
		END
	END
	ELSE 
	BEGIN
		UPDATE 
			MetaTags 
		SET 
			[vcPageTitle] = @vcPageTitle,
	   		[vcMetaKeywords] = @vcMetaKeywords,
			[vcMetaDescription] = @vcMetaDescription,
			[tintMetatagFor] = @tintMetatagFor,
			[numReferenceID] = @numReferenceID
		WHERE 
			[numMetaID] = @numMetaID
	END
END
GO

