GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_DivisionMasterShippingConfiguration_Save')
DROP PROCEDURE USP_DivisionMasterShippingConfiguration_Save
GO
CREATE PROCEDURE [dbo].[USP_DivisionMasterShippingConfiguration_Save]        
	@numDomainID NUMERIC(18,0)
	,@numDivisionID NUMERIC(18,0)
	,@IsAdditionalHandling BIT
    ,@IsCOD BIT
    ,@IsHomeDelivery BIT
    ,@IsInsideDelevery BIT
    ,@IsInsidePickup BIT
    ,@IsSaturdayDelivery BIT
    ,@IsSaturdayPickup BIT
    ,@IsLargePackage BIT
    ,@vcDeliveryConfirmation VARCHAR(1000)
    ,@vcDescription VARCHAR(MAX)
    ,@vcSignatureType VARCHAR(300)
    ,@vcCODType VARCHAR(50)
AS        
BEGIN     
	IF EXISTS (SELECT ID FROM DivisionMasterShippingConfiguration WHERE numDomainID=@numDomainID AND numDivisionID=@numDivisionID)
	BEGIN
		UPDATE
			DivisionMasterShippingConfiguration
		SET 
			numDomainID = @numDomainID
			,numDivisionID = @numDivisionID
			,IsAdditionalHandling = @IsAdditionalHandling 
			,IsCOD = @IsCOD 
			,IsHomeDelivery = @IsHomeDelivery 
			,IsInsideDelevery = @IsInsideDelevery 
			,IsInsidePickup = @IsInsidePickup 
			,IsSaturdayDelivery = @IsSaturdayDelivery 
			,IsSaturdayPickup = @IsSaturdayPickup 
			,IsLargePackage = @IsLargePackage 
			,vcDeliveryConfirmation = @vcDeliveryConfirmation
			,vcDescription = @vcDescription
			,vcSignatureType = @vcSignatureType
			,vcCODType = @vcCODType
		WHERE
			numDomainID=@numDomainID 
			AND numDivisionID=@numDivisionID
	END
	ELSE
	BEGIN
		INSERT INTO [dbo].[DivisionMasterShippingConfiguration]
		(
			numDomainID
           ,numDivisionID
           ,IsAdditionalHandling
           ,IsCOD
           ,IsHomeDelivery
           ,IsInsideDelevery
           ,IsInsidePickup
           ,IsSaturdayDelivery
           ,IsSaturdayPickup
           ,IsLargePackage
           ,vcDeliveryConfirmation
           ,vcDescription
           ,vcSignatureType
           ,vcCODType
		)
		VALUES
        (
			@numDomainID
           ,@numDivisionID
           ,@IsAdditionalHandling
           ,@IsCOD
           ,@IsHomeDelivery
           ,@IsInsideDelevery
           ,@IsInsidePickup
           ,@IsSaturdayDelivery
           ,@IsSaturdayPickup
           ,@IsLargePackage
           ,@vcDeliveryConfirmation
           ,@vcDescription
           ,@vcSignatureType
           ,@vcCODType
		)
	END
END
GO


