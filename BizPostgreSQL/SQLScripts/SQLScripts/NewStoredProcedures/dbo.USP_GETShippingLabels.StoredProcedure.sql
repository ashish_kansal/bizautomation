
/****** Object:  StoredProcedure [dbo].[USP_GETShippingLabels]    Script Date: 05/07/2009 17:43:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_GETShippingLabels' ) 
    DROP PROCEDURE USP_GETShippingLabels
GO
--  EXEC USP_GETShippingLabels 0,'21993,21993,21993,21993,38382,38382',135,1
-- exec USP_GETShippingLabels @numBoxID=0,@vcOrderIds='1758,2304,2344,2344,2346,2346,2347,2347,2347,2347,2653,2653,2883,15152,17257,17257,17260,17752,18139,18597,18597,18597',@numDomainID=1,@tintMode=1
CREATE PROCEDURE [dbo].[USP_GETShippingLabels]
    (
      @numBoxID NUMERIC(9),
      @vcOrderIds VARCHAR(MAX),
      @numDomainID NUMERIC(10, 0),
      @tintMode TINYINT = 0,
      @vcOppBizDocIds AS VARCHAR(MAX) = ''
    )
AS 
    BEGIN
        IF @tintMode = 0 
            BEGIN
                SELECT  [vcShippingLabelImage]
		--		   [vcTrackingNumber]
                FROM    [ShippingBox]
                WHERE   [numBoxID] = @numBoxID
            END
	
        IF @tintMode = 1 
            BEGIN
				
				DECLARE @numBizDocType AS VARCHAR(MAX)
				SELECT @numBizDocType  = ISNULL(numBizDocType,0) FROM dbo.OpportunityOrderStatus WHERE numDomainID = @numDomainID
				PRINT @numBizDocType
	
				SELECT ROW_NUMBER() OVER (ORDER BY numOppBizDocsId) AS [ID], numOppBizDocsId INTO #tempBizDoc
				FROM dbo.OpportunityBizDocs OBD 
				INNER JOIN dbo.OpportunityMaster OM ON OM.numOppId = OBD.numOppId
				WHERE 1 = 1
				AND OM.numDomainID = @numDomainID
		        AND OBD.numOppId IN (SELECT ID FROM dbo.SplitIDs(@vcOrderIDs, ','))
		        AND ISNULL(OBD.numBizDocId,0) =  @numBizDocType 
				
				DECLARE @intRow AS INT
				DECLARE @intCnt AS INT
				SET @intRow = 0
				SELECT @intCnt = COUNT(numOppBizDocsId) FROM #tempBizDoc
				
				PRINT @intCnt
				
				CREATE TABLE #tempImages
				(
					numShippingReportId		NUMERIC(18,0),
					numBoxID				NUMERIC(18,0),
					vcBoxName				VARCHAR(100),
					vcShippingLabelImage	VARCHAR(MAX),
					numShippingCompany		NUMERIC(18,0)
				)
				
				WHILE(@intRow < @intCnt)
					BEGIN
						SET @intRow = @intRow + 1
--						SELECT numOppBizDocsId FROM #tempBizDoc WHERE ID = @intRow
--						SELECT TOP 1 numShippingReportId FROM dbo.ShippingReport 
--														WHERE 1=1
--														AND numDomainID = 135
--														AND numOppBizDocId = (SELECT numOppBizDocsId FROM #tempBizDoc WHERE ID = @intRow)
--														ORDER BY numShippingReportId DESC
						
						INSERT INTO #tempImages 
						SELECT SB.numShippingReportId,numBoxID,vcBoxName,ISNULL(vcShippingLabelImage,''),numShippingCompany AS [numShippingCompany] FROM dbo.ShippingBox SB
						INNER JOIN dbo.ShippingReport SR ON SR.numShippingReportId = SB.numShippingReportId 
						WHERE 1=1 
						AND  SB.numShippingReportId = ( 
														SELECT TOP 1 numShippingReportId FROM dbo.ShippingReport 
														WHERE 1=1
														AND numDomainID = @numDomainID
														AND numOppBizDocId = (SELECT numOppBizDocsId FROM #tempBizDoc WHERE ID = @intRow)
														ORDER BY numShippingReportId DESC
													  )
					END
				
				--SELECT * FROM #tempBizDoc
				SELECT * FROM #tempImages
				
				DROP TABLE #tempImages	
				DROP TABLE #tempBizDoc
            END
		
		 IF @tintMode = 2
        BEGIN        
			SELECT  SB.numShippingReportId,numOppBizDocId,
					numBoxID,
					vcBoxName,
					ISNULL(vcShippingLabelImage, '') AS vcShippingLabelImage,
					numShippingCompany AS [numShippingCompany]
			FROM    dbo.ShippingBox SB
					INNER JOIN dbo.ShippingReport SR ON SR.numShippingReportId = SB.numShippingReportId
			WHERE   1 = 1
			AND SR.numOppBizDocId IN ( SELECT * FROM dbo.split(@vcOppBizDocIds,','))       

        END    


		IF @tintMode = 3
        BEGIN        
			SELECT  
				SB.numShippingReportId,
				OM.numOppId,
				ISNULL(vcPOppName,'') vcPOppName,
				numBoxID,
				vcBoxName,
				ISNULL(vcShippingLabelImage, '') AS vcShippingLabelImage,
				numShippingCompany AS [numShippingCompany]
			FROM    
				dbo.ShippingBox SB
			INNER JOIN 
				dbo.ShippingReport SR 
			ON 
				SR.numShippingReportId = SB.numShippingReportId
			INNER JOIN
				OpportunityMaster OM
			ON
				SR.numOppID=OM.numOppId
			WHERE 
				SR.numDomainId=@numDomainID
				AND SR.numOppID IN (SELECT Distinct Items FROM dbo.split(@vcOrderIds,',')) 
				AND CHARINDEX('.zpl',vcShippingLabelImage) = 0

			SELECT  
				SB.numShippingReportId,
				OM.numOppId,
				ISNULL(vcPOppName,'') vcPOppName,
				numBoxID,
				vcBoxName,
				ISNULL(vcShippingLabelImage, '') AS vcShippingLabelImage,
				numShippingCompany AS [numShippingCompany]
			FROM    
				dbo.ShippingBox SB
			INNER JOIN 
				dbo.ShippingReport SR 
			ON 
				SR.numShippingReportId = SB.numShippingReportId
			INNER JOIN
				OpportunityMaster OM
			ON
				SR.numOppID=OM.numOppId
			WHERE 
				SR.numDomainId=@numDomainID
				AND SR.numOppID IN (SELECT Distinct Items FROM dbo.split(@vcOrderIds,',')) 
				AND CHARINDEX('.zpl',vcShippingLabelImage) > 0

        END    
    END
    
