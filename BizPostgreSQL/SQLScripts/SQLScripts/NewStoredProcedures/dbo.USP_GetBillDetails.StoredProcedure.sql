GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_GetBillDetails' ) 
    DROP PROCEDURE USP_GetBillDetails
GO
-- exec USP_GetBillPaymentDetails 3,1,0
CREATE PROCEDURE [dbo].[USP_GetBillDetails]
    @numBillID NUMERIC(18, 0),
    @numDomainID NUMERIC
AS 
    SET NOCOUNT ON
    SET TRANSACTION ISOLATION LEVEL READ COMMITTED
    
	DECLARE @numCurrencyID NUMERIC(18,0)
	SELECT @numCurrencyID=numCurrencyID FROM Domain WHERE numDomainID = @numDomainID

    SELECT  BH.[numBillID],
            [numDivisionID],
            dbo.fn_GetComapnyName(numDivisionID) AS [vcCompanyName],
            BH.[numAccountID],
            BH.[dtBillDate],
            BH.[numTermsID],
            BH.[monAmountDue],
            BH.[dtDueDate],
            BH.[vcMemo],
            BH.[vcReference],
            BH.[monAmtPaid],
            BH.[bitIsPaid],
            BH.[numDomainID],
            BH.[numCreatedBy],
            BH.[dtCreatedDate],
            BH.[numModifiedBy],
            BH.[dtModifiedDate],ISNULL(GJD.numTransactionId,0) AS numTransactionId,
            ISNULL(PO.numProId,0) AS numProjectID,ISNULL(PO.numStageID,0) AS numStageID,
            (SELECT vcTerms FROM BillingTerms WHERE numTermsID = BH.[numTermsID] AND numDomainId = BH.numDomainID)  AS [vcTermName],
			ISNULL(BH.numCurrencyID,@numCurrencyID) numCurrencyID,
			ISNULL(BH.fltExchangeRate,1) fltExchangeRate
    FROM    [dbo].[BillHeader] BH
            LEFT OUTER JOIN General_Journal_Details GJD ON GJD.tintReferenceType=3 
						AND GJD.numReferenceID=BH.numBillID
			LEFT OUTER JOIN ProjectsOpportunities PO ON PO.numBillId=BH.numBillId			
    WHERE   BH.[numBillID] = @numBillID
            AND BH.numDomainID = @numDomainID
    
    
    SELECT  BD.[numBillDetailID],
            BD.[numExpenseAccountID],
            BD.[monAmount],
            BD.[vcDescription],
            BD.[numProjectID],
            BD.[numClassID],
            BD.[numCampaignID],
            BD.numBillID
			,ISNULL(BD.numOppItemID,0) numOppItemID
			,ISNULL(BD.numOppID,0) numOppID
			,ISNULL(GJD.numTransactionId,0) AS numTransactionId
    FROM    dbo.BillDetails BD
            INNER JOIN BillHeader BH ON BD.numBillID = BH.numBillID
           	LEFT OUTER JOIN General_Journal_Details GJD ON GJD.tintReferenceType=4 AND GJD.numReferenceID=BD.numBillDetailID
    WHERE   BH.[numBillID] = @numBillID
            AND BH.numDomainID = @numDomainID
            
GO


