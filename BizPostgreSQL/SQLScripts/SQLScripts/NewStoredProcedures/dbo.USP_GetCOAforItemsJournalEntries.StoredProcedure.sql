--Created by Joseph
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetCOAforItemsJournalEntries')
DROP PROCEDURE USP_GetCOAforItemsJournalEntries
GO
CREATE PROCEDURE [dbo].[USP_GetCOAforItemsJournalEntries]
               @numDomainId      AS NUMERIC(9)  = 0,
               @numItemId		 AS NUMERIC(9)  = 0,
               @tintMode         TINYINT  = 0
AS
  BEGIN
    IF @tintMode = 0 -- select all account Ids for  Item's Journal Entries
      BEGIN
			SELECT GJD.numChartAcntID as numAccountId FROM General_Journal_Details GJD 
			WHERE GJD.numItemID = @numItemId and GJD.numDomainID= @numDomainId
			GROUP by GJD.numChartAcntID
      END
    
  END


