
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ReportListMaster_First10SavedSearch')
DROP PROCEDURE USP_ReportListMaster_First10SavedSearch
GO
CREATE PROCEDURE [dbo].[USP_ReportListMaster_First10SavedSearch]   
@numDomainID AS NUMERIC(18,0)         
AS  
BEGIN
	SELECT TOP 10  
		numSearchID
		,vcSearchName
		,S.numFormID                   
	FROM 
		dbo.SavedSearch S
	INNER JOIN 
		dbo.DynamicFormMaster DFM 
	ON 
		DFM.numFormId = S.numFormId
	WHERE  
		numDomainID = @numDomainID
	ORDER BY
		numSearchID ASC
END
GO
