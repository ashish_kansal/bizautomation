SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'usp_GetTopItemSales' ) 
    DROP PROCEDURE usp_GetTopItemSales
GO
CREATE PROCEDURE [dbo].[USP_GetTopItemSales]
    (
      @numDomainId NUMERIC(18, 0),
      @numDivisionId NUMERIC(18, 0),
      @IsUnit BIT = 0
    )
AS 
    BEGIN 
    CREATE TABLE #temp(ItemName VARCHAR(2000),TotalUnits numeric(18,2),Revenue numeric(18,2))
        DECLARE @strSql VARCHAR(8000) 
        SET @strSql = ' Insert Into #temp SELECT TOP 10 I.vcItemName As ItemName,SUM(opp.numUnitHour) AS TotalUnits
        ,convert(decimal(10,2),SUM(opp.numUnitHour * monPrice)) AS Revenue
			   FROM  dbo.OpportunityItems opp
			   INNER JOIN dbo.OpportunityMaster om ON Opp.numOppID = oM.numOppID
			   JOIN item I ON Opp.numItemCode = i.numItemcode
			   WHERE   om.numDomainId = ' + CONVERT(VARCHAR, @numDomainId)
            + ' --and Opp.numOppId=12772 order by numoppitemtCode ASC 
			   AND OM.numDivisionID = ' + CONVERT(VARCHAR, @numDivisionId) + ' 
			   GROUP BY opp.numitemcode,I.vcItemName'
	PRINT @strSql
    EXECUTE(@strSql)
    	   
        IF @IsUnit = 0 
            BEGIN
			SELECT * FROM #temp ORDER BY TotalUnits ASC              
            END
        ELSE 
            BEGIN
		    SELECT * FROM #temp ORDER BY Revenue ASC 
            END
    
    DROP TABLE #temp 
    END
			
			 