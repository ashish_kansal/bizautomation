GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ManageCOAShippingMapping')
DROP PROCEDURE USP_ManageCOAShippingMapping
GO
CREATE PROCEDURE [dbo].[USP_ManageCOAShippingMapping]
	@numShippingMappingID numeric(9) =0 output,
	@numShippingMethodID numeric(9),
	@numIncomeAccountID numeric(9),
	@numDomainID numeric(9)
AS
SET NOCOUNT ON
IF @numShippingMappingID = 0 BEGIN
	INSERT INTO COAShippingMapping (
		[numShippingMethodID],
		[numIncomeAccountID],
		[numDomainID]
	)
	VALUES (
		@numShippingMethodID,
		@numIncomeAccountID,
		@numDomainID
	)
	SELECT @numShippingMappingID =SCOPE_IDENTITY() 
END
ELSE BEGIN
	UPDATE COAShippingMapping SET 
		[numShippingMethodID] = @numShippingMethodID,
		[numIncomeAccountID] = @numIncomeAccountID,
		[numDomainID] = @numDomainID
	WHERE [numShippingMappingID] = @numShippingMappingID
	AND numDomainID =@numDomainID

END