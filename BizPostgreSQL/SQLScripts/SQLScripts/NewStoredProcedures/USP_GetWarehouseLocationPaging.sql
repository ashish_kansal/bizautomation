GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_GetWarehouseLocationPaging' ) 
    DROP PROCEDURE USP_GetWarehouseLocationPaging
GO
-- EXEC USP_GetWarehouseLocation 1 , 58 , 11
CREATE PROCEDURE [dbo].USP_GetWarehouseLocationPaging
    @numDomainID AS NUMERIC(9) ,
    @numWarehouseID AS NUMERIC(8),
    @numWLocationID AS numeric(9) = 0,
	@vcSortChar VARCHAR(100) = '',
    @CurrentPage INT,
    @PageSize INT,
	@vcAisle VARCHAR(500)='',
	@vcRack VARCHAR(500)='',
	@vcShelf VARCHAR(500)='',
	@vcBin VARCHAR(500)='',
	@vcLocation VARCHAR(500)='',
    @intTotalRecordCount INT =0 OUTPUT 
AS 
    BEGIN
	 DECLARE @firstRec AS INTEGER                                        
		DECLARE @lastRec AS INTEGER                                        
		DECLARE @TotalRecordCount AS INTEGER                                        
		DECLARE @dynamicQuery AS NVARCHAR(MAX)=''
		DECLARE @dynamicQueryCount AS NVARCHAR(MAX)=''
		DECLARE @dycFilterQuery AS VARCHAR(MAX)=''
		SET @dynamicQueryCount=' SELECT  
				@TotalRecordCount = COUNT(numWLocationID)
        FROM    dbo.WarehouseLocation
        WHERE   numDomainID = '''+CAST(@numDomainID AS VARCHAR(500))+''' '
		IF(@numWarehouseID>0)
		BEGIN   
			SET @dycFilterQuery=@dycFilterQuery +' AND numWarehouseID='''+CAST(@numWarehouseID AS VARCHAR(500))+''' '
		END
		IF(@numWLocationID>0)
		BEGIN   
			SET @dycFilterQuery=@dycFilterQuery +' AND numWLocationID='''+CAST(@numWLocationID AS VARCHAR(500))+''' '
		END
		IF(@vcAisle<>'')
		BEGIN   
			SET @dycFilterQuery=@dycFilterQuery +' AND vcAisle='''+CAST(@vcAisle AS VARCHAR(500))+''' '
		END
		IF(@vcRack<>'')
		BEGIN   
			SET @dycFilterQuery=@dycFilterQuery +' AND vcRack='''+CAST(@vcRack AS VARCHAR(500))+''' '
		END
		IF(@vcShelf<>'')
		BEGIN   
			SET @dycFilterQuery=@dycFilterQuery +' AND vcShelf='''+CAST(@vcShelf AS VARCHAR(500))+''' '
		END
		IF(@vcBin<>'')
		BEGIN   
			SET @dycFilterQuery=@dycFilterQuery +' AND vcBin='''+CAST(@vcBin AS VARCHAR(500))+''' '
		END
		IF LEN(ISNULL(@vcLocation,'')) > 0
		BEGIN
			SET @dycFilterQuery=@dycFilterQuery +' AND vcLocation LIKE ''%' + CAST(@vcLocation AS VARCHAR(500)) + '%'' '
		END
		SET @dynamicQueryCount = @dynamicQueryCount+ @dycFilterQuery
 		EXEC sp_executesql @dynamicQueryCount
				,N'@TotalRecordCount int out'
				,@TotalRecordCount OUT
		SET @intTotalRecordCount=@TotalRecordCount
		SET @CurrentPage=(( @CurrentPage - 1 )*@PageSize)
        SET @dynamicQuery = 'SELECT  
				numWLocationID ,
                numWarehouseID ,
                vcAisle ,
                vcRack ,
                vcShelf ,
                vcBin ,
                vcLocation ,
                bitDefault ,
                bitSystem,
                intQty 
               
        FROM    dbo.WarehouseLocation
        WHERE   numDomainID = '''+CAST(@numDomainID AS VARCHAR(500))+''' '
		
		SET @dynamicQuery = @dynamicQuery+ @dycFilterQuery
		SET @dynamicQuery = @dynamicQuery + ' ORDER BY vcLocation '
 		SET @dynamicQuery=@dynamicQuery+' OFFSET CAST('''+CAST(@CurrentPage AS varchar(100))+'''  AS INT) ROWS
				FETCH NEXT CAST('''+CAST(@PageSize AS varchar(100))+''' AS INT) ROWS ONLY;'
		PRINT @dynamicQuery
		
		exec sp_executesql @dynamicQuery, N'@intTotalRecordCount INT OUT', @intTotalRecordCount OUT
		--EXEC(@dynamicQuery)
    END
 
 GO 