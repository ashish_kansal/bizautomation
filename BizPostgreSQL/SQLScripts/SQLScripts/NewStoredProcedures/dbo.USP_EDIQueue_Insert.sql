SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_EDIQueue_Insert')
DROP PROCEDURE dbo.USP_EDIQueue_Insert
GO
CREATE PROCEDURE [dbo].[USP_EDIQueue_Insert]
(
	@numDomainID NUMERIC(18,0),
	@numUserCntID NUMERIC(18,0),
    @numOppID NUMERIC(18,0),
	@numOrderStatus NUMERIC(18,0)
)
AS 
BEGIN
	IF ISNULL((SELECT bit3PL FROM Domain WHERE numDomainID=@numDomainID),0) = 1 
		AND ISNULL(@numOrderStatus,0) = 15445 -- 15445: SEND SHIPMENT REQUEST (940)  
		AND NOT EXISTS (SELECT ID FROM EDIQueue WHERE numDomainID=@numDomainID AND numOppID=@numOppID AND EDIType=940 AND (ISNULL(bitExecuted,0) = 0)) --Changed By Richa so that 940 can be send multiple times
		--AND NOT EXISTS (SELECT ID FROM EDIQueue WHERE numDomainID=@numDomainID AND numOppID=@numOppID AND EDIType=940 AND ((ISNULL(bitSuccess,0)=1 AND ISNULL(bitExecuted,0)= 1) OR ISNULL(bitExecuted,0) = 0))
	BEGIN
		INSERT INTO EDIQueue
		(
			numDomainID
			,numOppID
			,bitExecuted
			,bitSuccess
			,dtCreatedDate
			,numCreatedBy
			,EDIType
		)
		VALUES
		(
			@numDomainID
			,@numOppID
			,0
			,0
			,GETUTCDATE()
			,@numUserCntID
			,940
		)
	END
	ELSE IF ISNULL((SELECT bitEDI FROM Domain WHERE numDomainID=@numDomainID),0) = 1
			AND ISNULL(@numOrderStatus,0) = 15447  -- 15445: SEND 856
			AND NOT EXISTS (SELECT ID FROM EDIQueue WHERE numDomainID=@numDomainID AND numOppID=@numOppID AND EDIType=856 AND ((ISNULL(bitSuccess,0)=1 AND ISNULL(bitExecuted,0)= 1) OR ISNULL(bitExecuted,0) = 0))
	BEGIN
		INSERT INTO EDIQueue
		(
			numDomainID
			,numOppID
			,bitExecuted
			,bitSuccess
			,dtCreatedDate
			,numCreatedBy
			,EDIType
		)
		VALUES
		(
			@numDomainID
			,@numOppID
			,0
			,0
			,GETUTCDATE()
			,@numUserCntID
			,856
		)
	END
END
GO