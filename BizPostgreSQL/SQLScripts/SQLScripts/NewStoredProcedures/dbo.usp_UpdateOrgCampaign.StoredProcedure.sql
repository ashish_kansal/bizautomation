GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_UpdateOrgCampaign' ) 
    DROP PROCEDURE USP_UpdateOrgCampaign
GO
CREATE PROCEDURE USP_UpdateOrgCampaign
    @vcContactIDList VARCHAR(8000),
    @numCampaignID NUMERIC,
    @numUserCntID NUMERIC,
    @numDomainID NUMERIC
AS 
BEGIN
    UPDATE  dbo.DivisionMaster
    SET     numCampaignID = @numCampaignID,
            numModifiedBy = @numUserCntID,
            bintModifiedDate = GETUTCDATE()
    WHERE   numDomainID =@numDomainID 
			AND numDivisionID IN (
            SELECT DISTINCT
                    numDivisionID
            FROM    dbo.AdditionalContactsInformation
            WHERE   numContactId IN (
                    SELECT  id
                    FROM    dbo.splitIDs(@vcContactIDList, ',') ) )
END