/****** Object:  StoredProcedure [dbo].[USP_UpdatedomainRentalSetting]    Script Date: 05/07/2009 21:56:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj                                      
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_UpdatedomainRentalSetting')
DROP PROCEDURE USP_UpdatedomainRentalSetting
GO
CREATE PROCEDURE [dbo].[USP_UpdatedomainRentalSetting]    
@numDomainID as numeric(9)=0,
@bitRentalItem as bit=0,
@numRentalItemClass as NUMERIC(9)=NULL,
@numRentalHourlyUOM as NUMERIC(9)=NULL,
@numRentalDailyUOM as NUMERIC(9)=NULL,
@tintRentalPriceBasedOn as tinyint=NULL
AS
BEGIN
BEGIN TRY
	UPDATE 
		Domain                                       
	SET
		bitRentalItem=@bitRentalItem,
		numRentalItemClass=@numRentalItemClass,
		numRentalHourlyUOM=@numRentalHourlyUOM,
		numRentalDailyUOM=@numRentalDailyUOM,
		tintRentalPriceBasedOn=@tintRentalPriceBasedOn
	WHERE
		numDomainId=@numDomainId
END TRY
BEGIN CATCH
	DECLARE @ErrorMessage NVARCHAR(4000)
	DECLARE @ErrorNumber INT
	DECLARE @ErrorSeverity INT
	DECLARE @ErrorState INT
	DECLARE @ErrorLine INT
	DECLARE @ErrorProcedure NVARCHAR(200)

	SELECT 
		@ErrorMessage = ERROR_MESSAGE(),
		@ErrorNumber = ERROR_NUMBER(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE(),
		@ErrorLine = ERROR_LINE(),
		@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

	RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);
END CATCH
END