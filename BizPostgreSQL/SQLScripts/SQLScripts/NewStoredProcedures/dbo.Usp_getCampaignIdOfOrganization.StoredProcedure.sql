GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'Usp_getCampaignIdOfOrganization' ) 
    DROP PROCEDURE Usp_getCampaignIdOfOrganization
GO
CREATE PROCEDURE Usp_getCampaignIdOfOrganization
(
	@numDivisionID	NUMERIC(18),
	@numDomainID	NUMERIC(10)
)
AS 
BEGIN
	SELECT ISNULL(numCampaignID,0) AS [numCampaignID] FROM dbo.DivisionMaster 
	WHERE numDivisionID = @numDivisionID
	AND numDomainID = @numDomainID
END    