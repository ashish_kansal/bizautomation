SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
                    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_WareHouseItems_ManageInventoryKitWithinKit')
DROP PROCEDURE USP_WareHouseItems_ManageInventoryKitWithinKit
GO
CREATE PROCEDURE [dbo].[USP_WareHouseItems_ManageInventoryKitWithinKit]    
	@numOppID AS NUMERIC(18,0),
	@numOppItemID AS NUMERIC(18,0),
	@numOppChildItemID AS NUMERIC(18,0),
	@Mode as TINYINT,
	@tintMode AS TINYINT=0, -- 0:Add/Edit 1:Delete
	@numDomainID AS NUMERIC(18,0),
	@numUserCntID AS NUMERIC(18,0),
	@tintOppType AS TINYINT
AS
BEGIN	
	DECLARE @TableKitChildItem TABLE
	(
		RowNo INT,
		numOppKitChildItemID NUMERIC(18,0),
		numItemCode NUMERIC(18,0),
		bitAssembly BIT,
		numWarehouseItemID NUMERIC(18,0),
		numUnits FLOAT,
		numQtyShipped FLOAT
	)

	INSERT INTO @TableKitChildItem
	(
		RowNo,
		numOppKitChildItemID,
		numItemCode,
		bitAssembly,
		numWarehouseItemID,
		numUnits,
		numQtyShipped
	)
	SELECT
		ROW_NUMBER() OVER(ORDER BY numOppKitChildItemID),
		numOppKitChildItemID,
		numItemID,
		ISNULL(bitAssembly,0),
		numWareHouseItemId,
		numQtyItemsReq,
		numQtyShipped
	FROM
		OpportunityKitChildItems
	JOIN 
		Item I 
	ON 
		OpportunityKitChildItems.numItemID=I.numItemCode    
	WHERE 
		charitemtype = 'P' 
		AND ISNULL(numWareHouseItemId,0) > 0 
		AND numOppID = @numOppID
		AND numOppItemID = @numOppItemID
		AND numOppChildItemID = @numOppChildItemID
  
	DECLARE @numOppKitChildItemID NUMERIC(18,0)
	DECLARE @numItemCode NUMERIC(18,0)
	DECLARE @bitAssembly BIT
	DECLARE @numWareHouseItemID NUMERIC(18,0)
	DECLARE @onHand AS FLOAT
	DECLARE @onAllocation AS FLOAT
	DECLARE @onOrder AS FLOAT
	DECLARE @onBackOrder AS FLOAT
	DECLARE @onReOrder AS FLOAT
	DECLARE @numUnits as FLOAT
	DECLARE @QtyShipped AS FLOAT

	DECLARE @i AS INT = 1
	DECLARE @COUNT AS INT

	SELECT @COUNT = COUNT(*) FROM @TableKitChildItem

	WHILE @i <= @COUNT
	BEGIN
		SELECT 
			@numOppKitChildItemID=numOppKitChildItemID,
			@numItemCode=numItemCode,
			@bitAssembly=ISNULL(bitAssembly,0),
			@numWareHouseItemID=numWarehouseItemID,
			@numUnits=ISNULL(numUnits,0),
			@QtyShipped=ISNULL(numQtyShipped,0)
		FROM 
			@TableKitChildItem 
		WHERE 
			RowNo=@i
		
		DECLARE @description AS VARCHAR(100)

		IF @Mode=0
			SET @description=CONCAT('SO CHILD KIT insert/edit (Qty:',@numUnits,' Shipped:',@QtyShipped,')')
		ELSE IF @Mode=1
			SET @description=CONCAT('SO CHILD KIT Deleted (Qty:',@numUnits,' Shipped:',@QtyShipped,')')
		ELSE IF @Mode=2
			SET @description=CONCAT('SO CHILD KIT Close (Qty:',@numUnits,' Shipped:',@QtyShipped,')')
		ELSE IF @Mode=3
			SET @description=CONCAT('SO CHILD KIT Re-Open (Qty:',@numUnits,' Shipped:',@QtyShipped,')')
			
    	SELECT 
			@onHand=ISNULL(numOnHand,0),
			@onAllocation=ISNULL(numAllocation,0),                                    
			@onOrder=ISNULL(numOnOrder,0),
			@onBackOrder=ISNULL(numBackOrder,0),
			@onReOrder = ISNULL(numReorder,0)                                     
		FROM 
			WareHouseItems 
		WHERE 
			numWareHouseItemID=@numWareHouseItemID 
		
    	IF @Mode=0 --insert/edit
    	BEGIN
    		SET @numUnits = @numUnits - @QtyShipped
    	
			IF @onHand >= @numUnits                                    
			BEGIN                                    
				SET @onHand=@onHand-@numUnits                            
				SET @onAllocation=@onAllocation+@numUnits                                    
			END                                    
			ELSE IF @onHand < @numUnits                                    
			BEGIN                                    
				SET @onAllocation=@onAllocation+@onHand                                    
				SET @onBackOrder=@onBackOrder+@numUnits-@onHand                                    
				SET @onHand=0                                    
			END                         
    	END
		ELSE IF @Mode=1 --Revert
		BEGIN
			IF @QtyShipped>0
			BEGIN
				IF @tintMode=0
					SET @numUnits = @numUnits - @QtyShipped
				ELSE IF @tintmode=1
					SET @onAllocation = @onAllocation + @QtyShipped 
			END 
								                    
			IF @numUnits >= @onBackOrder 
			BEGIN
				SET @numUnits = @numUnits - @onBackOrder
				SET @onBackOrder = 0
                            
				IF (@onAllocation - @numUnits >= 0)
					SET @onAllocation = @onAllocation - @numUnits

				SET @onHand = @onHand + @numUnits                                            
			END                                            
			ELSE IF @numUnits < @onBackOrder 
			BEGIN                  
				IF (@onBackOrder - @numUnits >0)
					SET @onBackOrder = @onBackOrder - @numUnits					
			END 
		END       
		ELSE IF @Mode=2 --Close
		BEGIN
			SET @numUnits = @numUnits - @QtyShipped
			SET @onAllocation = @onAllocation - @numUnits            
		END
		ELSE IF @Mode=3 --Re-Open
		BEGIN
			SET @numUnits = @numUnits - @QtyShipped
			SET @onAllocation = @onAllocation + @numUnits            
		END

		UPDATE 
			WareHouseItems 
		SET 
			numOnHand=@onHand,
			numAllocation=@onAllocation,                                    
			numBackOrder=@onBackOrder,
			dtModified = GETDATE()  
		WHERE 
			numWareHouseItemID=@numWareHouseItemID 
	
		EXEC dbo.USP_ManageWareHouseItems_Tracking
			@numWareHouseItemID = @numWareHouseItemID, --  numeric(9, 0)
			@numReferenceID = @numOppID, --  numeric(9, 0)
			@tintRefType = 3, --  tinyint
			@vcDescription = @description, --  varchar(100)
			@numModifiedBy = @numUserCntID,
			@numDomainID = @numDomainID

		SET @i = @i + 1
	END
END
GO
