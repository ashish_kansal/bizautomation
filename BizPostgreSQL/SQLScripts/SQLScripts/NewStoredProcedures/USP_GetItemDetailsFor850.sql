SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetItemDetailsFor850')
DROP PROCEDURE dbo.USP_GetItemDetailsFor850
GO

CREATE PROCEDURE [dbo].[USP_GetItemDetailsFor850]
	@Inbound850PickItem INT
	,@vcItemIdentification VARCHAR(100)
	,@numDivisionId NUMERIC(18,0)
	,@numCompanyId NUMERIC(18,0)
	,@numDomainId NUMERIC(18,0)
	,@numOrderSource NUMERIC(18,0)
AS 
BEGIN

	If EXISTS (SELECT ID FROM ItemMarketplaceMapping WHERE numDomainID=@numDomainId AND numMarketplaceID=@numOrderSource AND ISNULL(vcMarketplaceUniqueID,'')=ISNULL(@vcItemIdentification,''))
	BEGIN
		SELECT
			Item.* 
		FROM 
			ItemMarketplaceMapping 
		INNER JOIN
			Item
		ON
			ItemMarketplaceMapping.numItemCode = Item.numItemCode
		WHERE 
			ItemMarketplaceMapping.numDomainID=@numDomainId 
			AND ItemMarketplaceMapping.numMarketplaceID=@numOrderSource 
			AND ISNULL(ItemMarketplaceMapping.vcMarketplaceUniqueID,'')=ISNULL(@vcItemIdentification,'')
	END
	ELSE
	BEGIN
		IF @Inbound850PickItem = 1 --SKU
		BEGIN
			IF @numDomainID = 209 AND EXISTS (SELECT numItemCode FROM Item WHERE numDomainID = @numDomainId AND vcItemName = @vcItemIdentification)
			BEGIN
				SELECT * FROM Item WHERE numDomainID = @numDomainId AND vcItemName = @vcItemIdentification
			END
			ELSE
			BEGIN
				SELECT * FROM Item WHERE numDomainID = @numDomainId AND vcSKU = @vcItemIdentification
			END
		END
		ELSE IF @Inbound850PickItem = 2  -- UPC
		BEGIN
			SELECT * FROM ITEM WHERE numDomainID = @numDomainId AND numBarCodeId = @vcItemIdentification
		END
		ELSE IF @Inbound850PickItem = 3  -- ItemName
		BEGIN
			SELECT * FROM ITEM WHERE numDomainID = @numDomainId AND vcItemName = @vcItemIdentification
		END
		ELSE IF @Inbound850PickItem = 4   -- BizItemID
		BEGIN
			SELECT * FROM ITEM WHERE numDomainID = @numDomainId AND numItemCode = @vcItemIdentification
		END
		ELSE IF @Inbound850PickItem = 5   -- ASIN
		BEGIN
			SELECT * FROM ITEM WHERE numDomainID = @numDomainId AND vcASIN = @vcItemIdentification
		END
		ELSE IF @Inbound850PickItem = 6  -- CustomerPart#
		BEGIN
			SELECT * FROM ITEM I
			INNER JOIN CustomerPartNumber CPN ON I.numItemCode = CPN.numItemCode
			WHERE CPN.numDomainId = @numDomainId AND CPN.numCompanyId = @numCompanyId AND CPN.CustomerPartNo = @vcItemIdentification
		END
		ELSE IF @Inbound850PickItem = 7   -- VendorPart#
		BEGIN
			SELECT * FROM ITEM I
			INNER JOIN Vendor V ON I.numItemCode = V.numItemCode
			WHERE V.numDomainID = @numDomainId AND V.numVendorID = @numDivisionId AND V.vcPartNo = @vcItemIdentification
		END
	END
END

GO


