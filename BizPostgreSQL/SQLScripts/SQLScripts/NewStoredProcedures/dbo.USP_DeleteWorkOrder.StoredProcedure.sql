
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_DeleteWorkOrder')
DROP PROCEDURE USP_DeleteWorkOrder
GO
CREATE PROCEDURE [dbo].[USP_DeleteWorkOrder]
@numWOId AS NUMERIC(18,0),
@numUserCntID AS NUMERIC(18,0),
@numDomainID AS NUMERIC(18,0),
@bitOrderDelete AS BIT,
@tintMode TINYINT --1: Order/Item, 2: Pick List
AS
BEGIN
BEGIN TRY
BEGIN TRANSACTION
	DECLARE @WorkOrder TABLE
	(
		ID INT IDENTITY(1,1),
		numWOID NUMERIC(18,0)
	)

	--GET ALL WORK ORDER CREATED FOR ORDER ASSEMBLY ITEM IN BOTTOM TO TOP ORDER
	;WITH CTE (numLevel,numWOID) AS
	(
		SELECT 
			1,
			numWOId
		FROM
			WorkOrder
		WHERE
			numParentWOID = @numWOID
			AND numWOStatus <> 23184
		UNION ALL
		SELECT 
			c.numLevel + 1,
			WorkOrder.numWOId		
		FROM
			WorkOrder
		INNER JOIN
			CTE c
		ON
			WorkOrder.numParentWOID = c.numWOID AND
			WorkOrder.numWOStatus <> 23184
	)

	INSERT INTO @WorkOrder SELECT numWOID FROM CTE ORDER BY numLevel DESC

	DECLARE @COUNT AS INT
	SELECT @COUNT=COUNT(*) FROM @WorkOrder

	IF @COUNT > 0
	BEGIN
		RAISERROR('CHILD_WORK_ORDER_EXISTS',16,1)
		RETURN
	END

	IF EXISTS (SELECT 
					WorkOrderPickedItems.ID
				FROM
					WorkOrderDetails
				INNER JOIN
					WorkOrderPickedItems
				ON
					WorkOrderDetails.numWODetailId=WorkOrderPickedItems.numWODetailId
				WHERE
					WorkOrderDetails.numWOId=@numWOID)
	BEGIN
		RAISERROR('WORK_ORDER_BOM_PICKED',16,1)
		RETURN
	END

	IF ISNULL((SELECT numQtyBuilt FROM WorkOrder WHERE numDomainID=@numDomainID AND numWOID=@numWOId),0) > 0
	BEGIN
		RAISERROR('WORK_ORDER_QTY_BUILT',16,1)
		RETURN
	END

	DECLARE @numWareHouseItemID AS NUMERIC(18,0),@numQtyItemsReq AS FLOAT,@numOppId AS NUMERIC(18,0),@numItemCode AS NUMERIC(18,0),@numWOStatus AS NUMERIC(18,0)
	DECLARE @onHand FLOAT
	DECLARE @onAllocation FLOAT
	DECLARE @onOrder FLOAT
	DECLARE @onBackOrder FLOAT
	DECLARE @Description VARCHAR(1000)

	DECLARE @numReferenceID AS NUMERIC(18,0)
	DECLARE @tintRefType TINYINT

	SELECT @numOppId=ISNULL(numOppId,0) FROM WorkOrder WHERE numWOId=@numWOId AND [WorkOrder].[numDomainID] = @numDomainID

	IF ISNULL(@numOppId,0) > 0
	BEGIN
		SET @numReferenceID = @numOppId
		SET @tintRefType = 3
	END
	ELSE
	BEGIN
		SET @numReferenceID = @numWOId
		SET @tintRefType = 2
	END

	--IF WORK ORDER IS CREATED FROM SALES ORDER AND WORK ORDER IS DELETED NOT WHOLE ORDER THEN EXECUTE FOLLOWING IF CODE
	UPDATE 
		opp 
	SET 
		bitWorkOrder=0
		,numWOQty=0 
	FROM 
		OpportunityItems opp 
	JOIN 
		WorkOrder wo 
	ON 
		ISNULL(wo.numOppId,0)=opp.numOppId
		AND ISNULL(wo.numOppItemID,0) = opp.numoppitemtCode
		AND ISNULL(numOppChildItemID,0) = 0   
		AND ISNULL(numOppKitChildItemID,0) = 0
	WHERE 
		wo.numWOId=@numWOId
			AND WO.[numDomainID] = @numDomainID

	SELECT 
		@numWareHouseItemID=numWareHouseItemID,
		@numQtyItemsReq=numQtyItemsReq,
		@numOppId=ISNULL(numOppId,0),
		@numItemCode=numItemCode,
		@numWOStatus=numWOStatus
	FROM 
		WorkOrder 
	WHERE 
		numWOId=@numWOId
		AND [WorkOrder].[numDomainID] = @numDomainID

	--IF NOT COMPLETED THEN DELETE
	IF @numWOStatus <> 23184
	BEGIN
		IF @numOppId>0
		BEGIN
			IF @tintMode = 2
			BEGIN
				SET @Description = CONCAT('SO-WO Pick List Deleted (Qty:',@numQtyItemsReq,')')
			END
			ELSE
			BEGIN
				SET @Description = CONCAT('SO-WO Work Order Deleted (Qty:',@numQtyItemsReq,')')
			END
		END
		ELSE
		BEGIN
			SET @Description = CONCAT('Work Order Deleted (Qty:',@numQtyItemsReq,')')
		END

		--GET CURRENT INEVENTORY STATUS
		SELECT
			@onHand = ISNULL(numOnHand,0),
			@onOrder = ISNULL(numOnOrder,0),
			@onAllocation = ISNULL(numAllocation,0),
			@onBackOrder = ISNULL(numBackOrder,0)
		FROM
			WareHouseItems
		WHERE
			numWareHouseItemID = @numWareHouseItemID

		--CHANGE INVENTORY
		IF @onOrder < @numQtyItemsReq
			SET @onOrder = 0
		ELSE
			SET @onOrder = @onOrder - @numQtyItemsReq

		EXEC USP_UpdateInventoryAndTracking 
		@numOnHand=@onHand,
		@numOnAllocation=@onAllocation,
		@numOnBackOrder=@onBackOrder,
		@numOnOrder=@onOrder,
		@numWarehouseItemID=@numWareHouseItemID,
		@numReferenceID=@numReferenceID,
		@tintRefType=@tintRefType,
		@numDomainID=@numDomainID,
		@numUserCntID=@numUserCntID,
		@Description=@Description 


		DECLARE @TEMP TABLE
		(
			ID INT IDENTITY(1,1),
			numWarehouseItemID NUMERIC(18,0),
			numQtyItemsReq FLOAT
		)

		INSERT INTO @TEMP SELECT numWareHouseItemId,numQtyItemsReq FROM WorkOrderDetails WHERE numWOId=@numWOId AND ISNULL(numWareHouseItemId,0) > 0

		DECLARE @i INT = 1
		DECLARE @iCount INT

		SELECT @iCount = COUNT(*) FROM @TEMP

		WHILE @i <= @iCount
		BEGIN
			SELECT @numWareHouseItemID=ISNULL(numWarehouseItemID,0),@numQtyItemsReq=ISNULL(numQtyItemsReq,0) FROM @TEMP WHERE ID = @i

			IF @numOppId>0
			BEGIN
				IF @tintMode = 2
				BEGIN
					SET @Description = CONCAT('Items Allocation Reverted O-WO Pick List Deleted (Qty:',@numQtyItemsReq,')')
				END
				ELSE
				BEGIN
					SET @Description = CONCAT('Items Allocation Reverted SO-WO Work Order Deleted (Qty:',@numQtyItemsReq,')')
				END
			END
			ELSE
			BEGIN
				SET @Description = CONCAT('Items Allocation Reverted Work Order Deleted (Qty:',@numQtyItemsReq,')')
			END

			--GET CURRENT INEVENTORY STATUS
			SELECT
				@onHand = ISNULL(numOnHand,0),
				@onOrder = ISNULL(numOnOrder,0),
				@onAllocation = ISNULL(numAllocation,0),
				@onBackOrder = ISNULL(numBackOrder,0)
			FROM
				WareHouseItems
			WHERE
				numWareHouseItemID = @numWareHouseItemID

			--CHANGE INVENTORY
			IF @numQtyItemsReq >= @onBackOrder 			BEGIN				SET @numQtyItemsReq = @numQtyItemsReq - @onBackOrder				SET @onBackOrder = 0                            				IF (@onAllocation - @numQtyItemsReq >= 0)						SET @onAllocation = @onAllocation - @numQtyItemsReq										SET @onHand = @onHand + @numQtyItemsReq                                                          			END                                            			ELSE IF @numQtyItemsReq < @onBackOrder 			BEGIN				SET @onBackOrder = @onBackOrder - @numQtyItemsReq			END

			--UPDATE INVENTORY AND WAREHOUSE TRACKING
			EXEC USP_UpdateInventoryAndTracking 
			@numOnHand=@onHand,
			@numOnAllocation=@onAllocation,
			@numOnBackOrder=@onBackOrder,
			@numOnOrder=@onOrder,
			@numWarehouseItemID=@numWareHouseItemID,
			@numReferenceID=@numReferenceID,
			@tintRefType=@tintRefType,
			@numDomainID=@numDomainID,
			@numUserCntID=@numUserCntID,
			@Description=@Description 

			SET @i = @i + 1
		END

		DELETE FROM WorkOrderDetails WHERE numWOId=@numWOId 

		DELETE FROM WorkOrder WHERE numWOId=@numWOId AND [numDomainID] = @numDomainID
	END
COMMIT TRANSACTION
END TRY
BEGIN CATCH
  -- Whoops, there was an error
  IF @@TRANCOUNT > 0
     ROLLBACK TRANSACTION

  -- Raise an error with the details of the exception
  DECLARE @ErrMsg nvarchar(4000), @ErrSeverity int
  SELECT @ErrMsg = ERROR_MESSAGE(),
         @ErrSeverity = ERROR_SEVERITY()

  RAISERROR(@ErrMsg, @ErrSeverity, 1)
END CATCH
END