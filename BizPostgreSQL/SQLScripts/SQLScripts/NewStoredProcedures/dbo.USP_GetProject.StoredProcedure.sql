GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetProject')
DROP PROCEDURE USP_GetProject
GO
CREATE PROCEDURE [dbo].[USP_GetProject]                                   
(                                    
@numProId numeric(9)=null  ,                  
@numDomainID numeric(9),    
@ClientTimeZoneOffset as int                                     
)                                    
as                                    
                                    
begin                                    
                    
 declare @strOppName as varchar(200)              
              
set @strOppName = ''              
select @strOppName= @strOppName +' <br> '+ vcpoppname from projectsOpportunities CO              
 join opportunitymaster OM on om.numoppid=co.numoppid and numProId=@numProId              
and co.numDomainid = @numdomainid                                   
                                    
 select  pro.numProId,                                    
  pro.vcProjectName,tintCRMType,                                   
  dbo.fn_GetContactName(pro.numintPrjMgr) as numintPrjMgr,                                    
@strOppName as numOppId,                              
 [dbo].[FormatedDateFromDate]( pro.intDueDate,@numDomainID) as intDueDate,                                    
    dbo.fn_GetContactName(pro.numCustPrjMgr) as numCustPrjMgr,                                                           
  pro.numDivisionId,                                    
  pro.txtComments,                                    
  div.vcDivisionName,                                    
  com.vcCompanyName,                                   
  dbo.fn_GetContactName(pro.numCreatedBy)+' '+convert(varchar(20),dateadd(minute,-@ClientTimeZoneOffset,pro.bintCreatedDate)) as vcCreatedBy ,                                    
  dbo.fn_GetContactName(pro.numModifiedBy)+' '+convert(varchar(20),dateadd(minute,-@ClientTimeZoneOffset,pro.bintModifiedDate)) as vcModifiedby,                                  
  pro.numDomainId,                                    
  dbo.fn_GetContactName(pro.numRecOwner) as vcRecOwner,                      
  pro.numAssignedby,     
pro.numContractId              ,    
pro.numAssignedTo ,                    
(select  count(*) from GenericDocuments   where numRecID=@numProId and  vcDocumentSection='P') as DocumentCount,                    
(select sum(tintPercentage) from ProjectsStageDetails where numProId=@numProId and bitStageCompleted=1) as TProgress            
  from ProjectsMaster pro                                    
  left join DivisionMaster div                                    
  on pro.numDivisionID=div.numDivisionID                                    
  left join CompanyInfo com                                    
  on div.numCompanyID=com.numCompanyId                                    
  where numProId=@numProId     and pro.numdomainID=  @numDomainID                             
                                    
end  