SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetAdminUserForDomain')
DROP PROCEDURE dbo.USP_GetAdminUserForDomain
GO

CREATE PROCEDURE [dbo].[USP_GetAdminUserForDomain]
	@numDomainID NUMERIC
AS 
BEGIN
		SELECT numAdminID FROM Domain WHERE numDomainID=@numDomainID

	END

GO