/****** Object:  StoredProcedure [dbo].[USP_DeleteShippingReportItem]    Script Date: 05/07/2009 17:40:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_DeleteShippingBox')
DROP PROCEDURE USP_DeleteShippingBox
GO
CREATE PROCEDURE [dbo].USP_DeleteShippingBox
                @numBoxID NUMERIC(9)
AS
  BEGIN
	

    DELETE FROM [ShippingReportItems]
    WHERE numBoxID=@numBoxID

	DELETE FROM dbo.ShippingBox WHERE numBoxID= @numBoxID 
  END
