SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_OpportunityItemsReleaseDates_GetByOppItemID')
DROP PROCEDURE USP_OpportunityItemsReleaseDates_GetByOppItemID
GO
CREATE PROCEDURE [dbo].[USP_OpportunityItemsReleaseDates_GetByOppItemID]
(
	@numDomainID NUMERIC(18,0),
	@numOppID NUMERIC(18,0),
	@numOppItemID NUMERIC(18,0)
)
AS 
BEGIN
	SELECT
		numID,
		CONCAT(dbo.FormatedDateFromDate(dtReleaseDate,@numDomainID),'(',numQty,'),',CASE tintStatus WHEN 1 THEN 'Open' ELSE 'Purchased' END) AS vcDesctiption,
		dtReleaseDate,
		numQty,
		tintStatus
	FROM
		OpportunityItemsReleaseDates
	WHERE
		numOppID=@numOppID
		AND numOppItemID=@numOppItemID
END