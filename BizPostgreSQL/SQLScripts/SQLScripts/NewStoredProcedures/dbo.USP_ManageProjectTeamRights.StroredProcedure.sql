
GO
/****** Object:  StoredProcedure [dbo].[USP_ManageProjectTeamRights]    Script Date: 09/21/2010 11:29:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ManageProjectTeamRights')
DROP PROCEDURE USP_ManageProjectTeamRights
GO
CREATE PROCEDURE [dbo].[USP_ManageProjectTeamRights]
@strContactId as TEXT,
@numDomainId as numeric(9)=0,
@numProId as numeric(9)=0,
@tintUserType as tinyint,--1:Internal User 2:External User
@tintMode as tinyint, --1: ADD/DELETE/UPDATE 2:ADD 3:Delete
@numUserCntID as numeric(9)
as

DECLARE @hDocItem INT
IF CONVERT(VARCHAR(10), @strContactId) <> '' 
BEGIN
	EXEC sp_xml_preparedocument @hDocItem OUTPUT,@strContactId


 SELECT  X.numContactId,X.numRights,X.numProjectRole into #tempUser
                                FROM    ( SELECT    *
                                          FROM      OPENXML (@hDocItem, '/NewDataSet/Table1', 2)
                                                    WITH ( numContactId NUMERIC, numRights NUMERIC, numProjectRole NUMERIC)
                                        ) X

IF @tintMode=1 or @tintMode=2
BEGIN
      
if @tintMode=1
BEGIN
	delete from ProjectTeamRights where numProId=@numProId and tintUserType=@tintUserType 
		and numContactId not in (select numContactId from #tempUser)

	update PTR set numRights=X.numRights,numProjectRole=X.numProjectRole from ProjectTeamRights PTR join #tempUser X
		on PTR.numContactId=X.numContactId where PTR.numProId=@numProId and tintUserType=@tintUserType
END

INSERT INTo ProjectTeamRights(numContactId, numRights,numProjectRole,numProId,tintUserType)
                    SELECT  X.numContactId,X.numRights,X.numProjectRole,@numProId,@tintUserType
							from #tempUser X where numContactId not in(select numContactId from ProjectTeamRights
								where numProId=@numProId and tintUserType=@tintUserType)	
END

IF @tintMode=3
BEGIN
	Update StagePercentageDetails set numAssignTo=@numUserCntID where numProjectId=@numProId 
			and numAssignTo in (select numContactId from #tempUser)

	delete from ProjectTeamRights where numProId=@numProId and tintUserType=@tintUserType 
		and numContactId in (select numContactId from #tempUser)
END    
                     
     EXEC sp_xml_removedocument @hDocItem
END
              








