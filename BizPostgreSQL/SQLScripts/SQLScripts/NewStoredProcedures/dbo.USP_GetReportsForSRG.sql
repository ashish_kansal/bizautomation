GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetReportsForSRG')
DROP PROCEDURE dbo.USP_GetReportsForSRG
GO
CREATE PROCEDURE [dbo].[USP_GetReportsForSRG]
(
	@numDomainID NUMERIC(18,0)
	,@numUserCntID NUMERIC(18,0)
	,@tintMode TINYINT
	,@tintReportType TINYINT
	,@numDashboardTemplateID NUMERIC(18,0)
	,@vcSearchText VARCHAR(300)
	,@numCurrentPage INT
	,@numPageSize INT
	,@numSRGID NUMERIC(18,0)	
)
AS 
BEGIN
	IF @tintMode = 1
	BEGIN
		IF @tintReportType = 2 -- Pre defined reports
		BEGIN
			SELECT 
				COUNT(*) OVER() AS TotalRecords
				,ReportListMaster.numReportID
				,(CASE WHEN  ISNULL(bitDefault,0) = 1 THEN 2 ELSE 1 END) AS tintReportType
				,ISNULL(vcReportName,'') vcReportName
				,'' AS vcReportType
				,'' vcModuleName
				,'' AS vcPerspective
				,'' AS vcTimeline
			FROM 
				ReportListMaster 
			LEFT JOIN 
				ReportModuleMaster RMM 
			ON 
				ReportListMaster.numReportModuleID=RMM.numReportModuleID
			WHERE
				ISNULL(ReportListMaster.bitDefault,0) = 1
				AND (ISNULL(@vcSearchText,'')='' OR vcReportName LIKE CONCAT('%',@vcSearchText,'%'))
			ORDER BY
				vcReportName
			OFFSET 
				(@numCurrentPage - 1) * @numPageSize ROWS 
			FETCH NEXT @numPageSize ROWS ONLY
		END
		ELSE IF @tintReportType = 3 -- My Reports
		BEGIN
			SELECT 
				COUNT(*) OVER() AS TotalRecords
				,URL.numReportID
				,1 AS tintReportType
				,ISNULL(vcReportName,'') vcReportName
				,(CASE RLM.tintReportType WHEN 0 THEN 'Tabular' WHEN 1 THEN 'Summary' WHEN 2 THEN 'Matrix' WHEN 3 THEN 'KPI' WHEN 4 THEN 'ScoreCard' END) AS vcReportType
				,ISNULL(RMM.vcModuleName,'') vcModuleName
				,(CASE RLM.tintRecordFilter WHEN 5 THEN 'User logged in (Record Owner)' WHEN 4 THEN 'User logged in (Assigned-To)' WHEN 3 THEN 'User logged in (Territory Scope)' WHEN 2 THEN 'User logged in (Team Scope)' WHEN 1 THEN 'User logged in (Assigned-To/Record Owner)' ELSE 'All Records' END) AS vcPerspective
				,(CASE 
					WHEN ISNULL(RLM.numDateFieldID,0) > 0 AND LEN(ISNULL(RLM.vcDateFieldValue,'')) > 0
					THEN 
						CONCAT((CASE WHEN bitDateFieldColumnCustom = 1 THEN (SELECT Fld_label FROM CFW_Fld_Master WHERE Fld_Id = numDateFieldID) ELSE (SELECT vcFieldName FROM DycFieldMaster WHERE numFieldId=numDateFieldID) END),' (',
						(CASE RLM.vcDateFieldValue
							WHEN 'AllTime' THEN 'All Time'
							WHEN 'Custom' THEN 'Custom'
							WHEN 'CurYear' THEN 'Current CY'
							WHEN 'PreYear' THEN 'Previous CY'
							WHEN 'Pre2Year' THEN 'Previous 2 CY'
							WHEN 'Ago2Year' THEN '2 CY Ago'
							WHEN 'NextYear' THEN 'Next CY'
							WHEN 'CurPreYear' THEN 'Current and Previous CY'
							WHEN 'CurPre2Year' THEN 'Current and Previous 2 CY'
							WHEN 'CurNextYear' THEN 'Current and Next CY'
							WHEN 'CuQur' THEN 'Current CQ'
							WHEN 'CurNextQur' THEN 'Current and Next CQ'
							WHEN 'CurPreQur' THEN 'Current and Previous CQ'
							WHEN 'NextQur' THEN 'Next CQ'
							WHEN 'PreQur' THEN 'Previous CQ'
							WHEN 'LastMonth' THEN 'Last Month'
							WHEN 'ThisMonth' THEN 'This Month'
							WHEN 'NextMonth' THEN 'Next Month'
							WHEN 'CurPreMonth' THEN 'Current and Previous Month'
							WHEN 'CurNextMonth' THEN 'Current and Next Month'
							WHEN 'LastWeek' THEN 'Last Week'
							WHEN 'ThisWeek' THEN 'This Week'
							WHEN 'NextWeek' THEN 'Next Week'
							WHEN 'Yesterday' THEN 'Yesterday'
							WHEN 'Today' THEN 'Today'
							WHEN 'Tomorrow' THEN 'Tomorrow'
							WHEN 'Last7Day' THEN 'Last 7 Days'
							WHEN 'Last30Day' THEN 'Last 30 Days'
							WHEN 'Last60Day' THEN 'Last 60 Days'
							WHEN 'Last90Day' THEN 'Last 90 Days'
							WHEN 'Last120Day' THEN 'Last 120 Days'
							WHEN 'Next7Day' THEN 'Next 7 Days'
							WHEN 'Next30Day' THEN 'Next 30 Days'
							WHEN 'Next60Day' THEN 'Next 60 Days'
							WHEN 'Next90Day' THEN 'Next 90 Days'
							WHEN 'Next120Day' THEN 'Next 120 Days'
							ELSE '-'
						END),')')
					ELSE 
						'' 
				END) AS vcTimeline
			FROM 
				UserReportList URL
			INNER JOIN 
				ReportListMaster RLM
			ON 
				URL.numReportID=RLM.numReportID
			INNER JOIN 
				ReportModuleMaster RMM 
			ON 
				RLM.numReportModuleID=RMM.numReportModuleID
			WHERE 
				URL.numDomainID=@numDomainID 
				AND URL.numUserCntID=@numUserCntID 
				AND URL.tintReportType=1
				AND RLM.numDomainID=@numDomainID
				AND (ISNULL(@vcSearchText,'')='' OR vcReportName LIKE CONCAT('%',@vcSearchText,'%'))
			ORDER BY
				vcReportName
			OFFSET 
				(@numCurrentPage - 1) * @numPageSize ROWS 
			FETCH NEXT @numPageSize ROWS ONLY
		END
		ELSE IF @tintReportType = 4 -- Saved searches
		BEGIN
			SELECT  
				COUNT(*) OVER() AS TotalRecords,
				numSearchID AS numReportID,
				4 AS tintReportType,
				ISNULL(vcSearchName,'') AS vcReportName,
				'' AS vcReportType,
				(CASE DFM.numFormId WHEN 1 THEN 'Companies & Contacts Search' ELSE DFM.vcFormName END) AS vcModuleName,
				'' AS vcPerspective,
				'' AS vcTimeline
			FROM
				dbo.SavedSearch S
			INNER JOIN
				dbo.DynamicFormMaster DFM 
			ON 
				DFM.numFormId = S.numFormId
			WHERE 
				numDomainID = @numDomainID
				AND numUserCntID = @numUserCntID
				AND ISNULL(numSharedFromSearchID,0) = 0
				AND (ISNULL(@vcSearchText,'')='' OR vcSearchName LIKE CONCAT('%',@vcSearchText,'%'))
			ORDER BY
				vcSearchName
			OFFSET 
				(@numCurrentPage - 1) * @numPageSize ROWS 
			FETCH NEXT @numPageSize ROWS ONLY
		END
		ELSE IF @tintReportType = 5 -- Shared saved searched
		BEGIN
			SELECT  
				COUNT(*) OVER() AS TotalRecords,
				numSearchID AS numReportID,
				4 AS tintReportType,
				ISNULL(vcSearchName,'') AS vcReportName,
				'' AS vcReportType,
				(CASE DFM.numFormId WHEN 1 THEN 'Companies & Contacts Search' ELSE DFM.vcFormName END) AS vcModuleName,
				'' AS vcPerspective,
				'' AS vcTimeline
			FROM
				dbo.SavedSearch S
			INNER JOIN
				dbo.DynamicFormMaster DFM 
			ON 
				DFM.numFormId = S.numFormId
			WHERE 
				numDomainID = @numDomainID
				AND numUserCntID = @numUserCntID
				AND ISNULL(numSharedFromSearchID,0) > 0
				AND (ISNULL(@vcSearchText,'')='' OR vcSearchName LIKE CONCAT('%',@vcSearchText,'%'))
			ORDER BY
				vcSearchName
			OFFSET 
				(@numCurrentPage - 1) * @numPageSize ROWS 
			FETCH NEXT @numPageSize ROWS ONLY
		END
		ELSE IF @tintReportType = 6 -- Dashboar templates
		BEGIN
			SELECT 
				COUNT(*) OVER() AS TotalRecords
				,ReportDashboard.numDashBoardID AS numReportID
				,6 AS tintReportType
				,ISNULL(vcReportName,'') vcReportName
				,'' AS vcReportType
				,'' vcModuleName
				,'' AS vcPerspective
				,'' AS vcTimeline
			FROM 
				DashboardTemplate
			INNER JOIN
				ReportDashboard 
			ON
				DashboardTemplate.numTemplateID = ReportDashboard.numDashboardTemplateID
			INNER JOIN
				ReportListMaster 
			ON
				ReportDashboard.numReportID = ReportListMaster.numReportID
			LEFT JOIN 
				ReportModuleMaster RMM 
			ON 
				ReportListMaster.numReportModuleID=RMM.numReportModuleID
			WHERE
				DashboardTemplate.numDomainID = @numDomainID
				AND DashboardTemplate.numTemplateID = @numDashboardTemplateID
				AND ReportDashboard.numDomainID = @numDomainID
				AND ReportDashboard.numUserCntID = @numUserCntID
				AND (ReportListMaster.numDomainID = @numDomainID OR ReportListMaster.bitDefault = 1)
				AND (ISNULL(@vcSearchText,'')='' OR vcReportName LIKE CONCAT('%',@vcSearchText,'%'))
			ORDER BY
				vcReportName
			OFFSET 
				(@numCurrentPage - 1) * @numPageSize ROWS 
			FETCH NEXT @numPageSize ROWS ONLY
		END
		ELSE
		BEGIN
			SELECT 
				COUNT(*) OVER() AS TotalRecords
				,RLM.numReportID
				,1 AS tintReportType
				,ISNULL(vcReportName,'') vcReportName
				,(CASE RLM.tintReportType WHEN 0 THEN 'Tabular' WHEN 1 THEN 'Summary' WHEN 2 THEN 'Matrix' WHEN 3 THEN 'KPI' WHEN 4 THEN 'ScoreCard' END) AS vcReportType
				,ISNULL(RMM.vcModuleName,'') vcModuleName
				,(CASE RLM.tintRecordFilter WHEN 5 THEN 'User logged in (Record Owner)' WHEN 4 THEN 'User logged in (Assigned-To)' WHEN 3 THEN 'User logged in (Territory Scope)' WHEN 2 THEN 'User logged in (Team Scope)' WHEN 1 THEN 'User logged in (Assigned-To/Record Owner)' ELSE 'All Records' END) AS vcPerspective
				,(CASE 
					WHEN ISNULL(RLM.numDateFieldID,0) > 0 AND LEN(ISNULL(RLM.vcDateFieldValue,'')) > 0
					THEN 
						CONCAT((CASE WHEN bitDateFieldColumnCustom = 1 THEN (SELECT Fld_label FROM CFW_Fld_Master WHERE Fld_Id = numDateFieldID) ELSE (SELECT vcFieldName FROM DycFieldMaster WHERE numFieldId=numDateFieldID) END),' (',
						(CASE RLM.vcDateFieldValue
							WHEN 'AllTime' THEN 'All Time'
							WHEN 'Custom' THEN 'Custom'
							WHEN 'CurYear' THEN 'Current CY'
							WHEN 'PreYear' THEN 'Previous CY'
							WHEN 'Pre2Year' THEN 'Previous 2 CY'
							WHEN 'Ago2Year' THEN '2 CY Ago'
							WHEN 'NextYear' THEN 'Next CY'
							WHEN 'CurPreYear' THEN 'Current and Previous CY'
							WHEN 'CurPre2Year' THEN 'Current and Previous 2 CY'
							WHEN 'CurNextYear' THEN 'Current and Next CY'
							WHEN 'CuQur' THEN 'Current CQ'
							WHEN 'CurNextQur' THEN 'Current and Next CQ'
							WHEN 'CurPreQur' THEN 'Current and Previous CQ'
							WHEN 'NextQur' THEN 'Next CQ'
							WHEN 'PreQur' THEN 'Previous CQ'
							WHEN 'LastMonth' THEN 'Last Month'
							WHEN 'ThisMonth' THEN 'This Month'
							WHEN 'NextMonth' THEN 'Next Month'
							WHEN 'CurPreMonth' THEN 'Current and Previous Month'
							WHEN 'CurNextMonth' THEN 'Current and Next Month'
							WHEN 'LastWeek' THEN 'Last Week'
							WHEN 'ThisWeek' THEN 'This Week'
							WHEN 'NextWeek' THEN 'Next Week'
							WHEN 'Yesterday' THEN 'Yesterday'
							WHEN 'Today' THEN 'Today'
							WHEN 'Tomorrow' THEN 'Tomorrow'
							WHEN 'Last7Day' THEN 'Last 7 Days'
							WHEN 'Last30Day' THEN 'Last 30 Days'
							WHEN 'Last60Day' THEN 'Last 60 Days'
							WHEN 'Last90Day' THEN 'Last 90 Days'
							WHEN 'Last120Day' THEN 'Last 120 Days'
							WHEN 'Next7Day' THEN 'Next 7 Days'
							WHEN 'Next30Day' THEN 'Next 30 Days'
							WHEN 'Next60Day' THEN 'Next 60 Days'
							WHEN 'Next90Day' THEN 'Next 90 Days'
							WHEN 'Next120Day' THEN 'Next 120 Days'
							ELSE '-'
						END),')')
					ELSE 
						'' 
				END) AS vcTimeline
			FROM 
				ReportListMaster RLM
			INNER JOIN 
				ReportModuleMaster RMM 
			ON 
				RLM.numReportModuleID=RMM.numReportModuleID
			WHERE 
				RLM.numDomainID=@numDomainID 
				AND (ISNULL(@vcSearchText,'')='' OR vcReportName LIKE CONCAT('%',@vcSearchText,'%'))
			ORDER BY
				vcReportName
			OFFSET 
				(@numCurrentPage - 1) * @numPageSize ROWS 
			FETCH NEXT @numPageSize ROWS ONLY
		END
	END
	ELSE IF @tintMode = 2
	BEGIN
		DECLARE @TEMP TABLE
		(
			ID NUMERIC(18,0)
			,numReportID NUMERIC(18,0)
			,tintReportType TINYINT
			,vcReportName VARCHAR(MAX)
			,vcReportType VARCHAR(100)
			,vcModuleName VARCHAR(100)
			,vcPerspective VARCHAR(100)
			,vcTimeline VARCHAR(100)
			,intSortOrder INT
		)

		INSERT INTO @TEMP
		(
			ID
			,numReportID
			,tintReportType
			,vcReportName
			,vcReportType
			,vcModuleName
			,vcPerspective
			,vcTimeline
			,intSortOrder
		)
		SELECT 
			SRGR.ID
			,ReportListMaster.numReportID
			,(CASE WHEN ISNULL(bitDefault,0) = 1 THEN 2 ELSE 1 END) AS tintReportType
			,(CASE WHEN ISNULL(bitDefault,0) = 1 THEN ISNULL(vcReportName,'') ELSE CONCAT('<a target="_blank" href="../reports/frmCustomReportRun.aspx?ReptID=',ReportListMaster.numReportID,'">',ISNULL(vcReportName,''),'</a>') END) vcReportName
			,(CASE WHEN ISNULL(ReportListMaster.bitDefault,0) = 1 THEN '' ELSE (CASE ReportListMaster.tintReportType WHEN 0 THEN 'Tabular' WHEN 1 THEN 'Summary' WHEN 2 THEN 'Matrix' WHEN 3 THEN 'KPI' WHEN 4 THEN 'ScoreCard' END) END) AS vcReportType
			,(CASE WHEN ISNULL(ReportListMaster.bitDefault,0) = 1 THEN '' ELSE ISNULL(RMM.vcModuleName,'') END) vcModuleName
			,(CASE WHEN ISNULL(ReportListMaster.bitDefault,0) = 1 THEN '' ELSE (CASE ReportListMaster.tintRecordFilter WHEN 5 THEN 'User logged in (Record Owner)' WHEN 4 THEN 'User logged in (Assigned-To)' WHEN 3 THEN 'User logged in (Territory Scope)' WHEN 2 THEN 'User logged in (Team Scope)' WHEN 1 THEN 'User logged in (Assigned-To/Record Owner)' ELSE 'All Records' END) END) AS vcPerspective
			,(CASE 
				WHEN ISNULL(ReportListMaster.bitDefault,0) = 1 
				THEN '' 
				ELSE (CASE 
						WHEN ISNULL(ReportListMaster.numDateFieldID,0) > 0 AND LEN(ISNULL(ReportListMaster.vcDateFieldValue,'')) > 0
						THEN 
							CONCAT((CASE WHEN bitDateFieldColumnCustom = 1 THEN (SELECT Fld_label FROM CFW_Fld_Master WHERE Fld_Id = numDateFieldID) ELSE (SELECT vcFieldName FROM DycFieldMaster WHERE numFieldId=numDateFieldID) END),' (',
							(CASE ReportListMaster.vcDateFieldValue
								WHEN 'AllTime' THEN 'All Time'
								WHEN 'Custom' THEN 'Custom'
								WHEN 'CurYear' THEN 'Current CY'
								WHEN 'PreYear' THEN 'Previous CY'
								WHEN 'Pre2Year' THEN 'Previous 2 CY'
								WHEN 'Ago2Year' THEN '2 CY Ago'
								WHEN 'NextYear' THEN 'Next CY'
								WHEN 'CurPreYear' THEN 'Current and Previous CY'
								WHEN 'CurPre2Year' THEN 'Current and Previous 2 CY'
								WHEN 'CurNextYear' THEN 'Current and Next CY'
								WHEN 'CuQur' THEN 'Current CQ'
								WHEN 'CurNextQur' THEN 'Current and Next CQ'
								WHEN 'CurPreQur' THEN 'Current and Previous CQ'
								WHEN 'NextQur' THEN 'Next CQ'
								WHEN 'PreQur' THEN 'Previous CQ'
								WHEN 'LastMonth' THEN 'Last Month'
								WHEN 'ThisMonth' THEN 'This Month'
								WHEN 'NextMonth' THEN 'Next Month'
								WHEN 'CurPreMonth' THEN 'Current and Previous Month'
								WHEN 'CurNextMonth' THEN 'Current and Next Month'
								WHEN 'LastWeek' THEN 'Last Week'
								WHEN 'ThisWeek' THEN 'This Week'
								WHEN 'NextWeek' THEN 'Next Week'
								WHEN 'Yesterday' THEN 'Yesterday'
								WHEN 'Today' THEN 'Today'
								WHEN 'Tomorrow' THEN 'Tomorrow'
								WHEN 'Last7Day' THEN 'Last 7 Days'
								WHEN 'Last30Day' THEN 'Last 30 Days'
								WHEN 'Last60Day' THEN 'Last 60 Days'
								WHEN 'Last90Day' THEN 'Last 90 Days'
								WHEN 'Last120Day' THEN 'Last 120 Days'
								WHEN 'Next7Day' THEN 'Next 7 Days'
								WHEN 'Next30Day' THEN 'Next 30 Days'
								WHEN 'Next60Day' THEN 'Next 60 Days'
								WHEN 'Next90Day' THEN 'Next 90 Days'
								WHEN 'Next120Day' THEN 'Next 120 Days'
								ELSE '-'
							END),')')
						ELSE 
							'' 
					END)
			END) AS vcTimeline
			,SRGR.intSortOrder
		FROM
			ScheduledReportsGroupReports SRGR
		INNER JOIN
			ReportListMaster 
		ON
			SRGR.numReportID = ReportListMaster.numReportID
			AND SRGR.tintReportType IN (1,2)
		LEFT JOIN 
			ReportModuleMaster RMM 
		ON 
			ReportListMaster.numReportModuleID=RMM.numReportModuleID
		WHERE
			SRGR.numSRGID = @numSRGID
			AND (ReportListMaster.numDomainID = @numDomainID OR ISNULL(ReportListMaster.bitDefault,0) = 1)
			AND SRGR.tintReportType IN (1,2)

		INSERT INTO @TEMP
		(
			ID
			,numReportID
			,tintReportType
			,vcReportName
			,vcReportType
			,vcModuleName
			,vcPerspective
			,vcTimeline
			,intSortOrder
		)
		SELECT 
			SRGR.ID
			,ReportListMaster.numReportID
			,(CASE WHEN ISNULL(bitDefault,0) = 1 THEN 2 ELSE 1 END) AS tintReportType
			,(CASE WHEN ISNULL(bitDefault,0) = 1 THEN ISNULL(vcReportName,'') ELSE CONCAT('<a target="_blank" href="../reports/frmCustomReportRun.aspx?ReptID=',ReportListMaster.numReportID,'">',ISNULL(vcReportName,''),'</a>') END) vcReportName
			,(CASE WHEN ISNULL(ReportListMaster.bitDefault,0) = 1 THEN '' ELSE (CASE ReportListMaster.tintReportType WHEN 0 THEN 'Tabular' WHEN 1 THEN 'Summary' WHEN 2 THEN 'Matrix' WHEN 3 THEN 'KPI' WHEN 4 THEN 'ScoreCard' END) END) AS vcReportType
			,(CASE WHEN ISNULL(ReportListMaster.bitDefault,0) = 1 THEN '' ELSE ISNULL(RMM.vcModuleName,'') END) vcModuleName
			,(CASE WHEN ISNULL(ReportListMaster.bitDefault,0) = 1 THEN '' ELSE (CASE ReportListMaster.tintRecordFilter WHEN 5 THEN 'User logged in (Record Owner)' WHEN 4 THEN 'User logged in (Assigned-To)' WHEN 3 THEN 'User logged in (Territory Scope)' WHEN 2 THEN 'User logged in (Team Scope)' WHEN 1 THEN 'User logged in (Assigned-To/Record Owner)' ELSE 'All Records' END) END) AS vcPerspective
			,(CASE 
				WHEN ISNULL(ReportListMaster.bitDefault,0) = 1 
				THEN '' 
				ELSE (CASE 
						WHEN ISNULL(ReportListMaster.numDateFieldID,0) > 0 AND LEN(ISNULL(ReportListMaster.vcDateFieldValue,'')) > 0
						THEN 
							CONCAT((CASE WHEN bitDateFieldColumnCustom = 1 THEN (SELECT Fld_label FROM CFW_Fld_Master WHERE Fld_Id = numDateFieldID) ELSE (SELECT vcFieldName FROM DycFieldMaster WHERE numFieldId=numDateFieldID) END),' (',
							(CASE ReportListMaster.vcDateFieldValue
								WHEN 'AllTime' THEN 'All Time'
								WHEN 'Custom' THEN 'Custom'
								WHEN 'CurYear' THEN 'Current CY'
								WHEN 'PreYear' THEN 'Previous CY'
								WHEN 'Pre2Year' THEN 'Previous 2 CY'
								WHEN 'Ago2Year' THEN '2 CY Ago'
								WHEN 'NextYear' THEN 'Next CY'
								WHEN 'CurPreYear' THEN 'Current and Previous CY'
								WHEN 'CurPre2Year' THEN 'Current and Previous 2 CY'
								WHEN 'CurNextYear' THEN 'Current and Next CY'
								WHEN 'CuQur' THEN 'Current CQ'
								WHEN 'CurNextQur' THEN 'Current and Next CQ'
								WHEN 'CurPreQur' THEN 'Current and Previous CQ'
								WHEN 'NextQur' THEN 'Next CQ'
								WHEN 'PreQur' THEN 'Previous CQ'
								WHEN 'LastMonth' THEN 'Last Month'
								WHEN 'ThisMonth' THEN 'This Month'
								WHEN 'NextMonth' THEN 'Next Month'
								WHEN 'CurPreMonth' THEN 'Current and Previous Month'
								WHEN 'CurNextMonth' THEN 'Current and Next Month'
								WHEN 'LastWeek' THEN 'Last Week'
								WHEN 'ThisWeek' THEN 'This Week'
								WHEN 'NextWeek' THEN 'Next Week'
								WHEN 'Yesterday' THEN 'Yesterday'
								WHEN 'Today' THEN 'Today'
								WHEN 'Tomorrow' THEN 'Tomorrow'
								WHEN 'Last7Day' THEN 'Last 7 Days'
								WHEN 'Last30Day' THEN 'Last 30 Days'
								WHEN 'Last60Day' THEN 'Last 60 Days'
								WHEN 'Last90Day' THEN 'Last 90 Days'
								WHEN 'Last120Day' THEN 'Last 120 Days'
								WHEN 'Next7Day' THEN 'Next 7 Days'
								WHEN 'Next30Day' THEN 'Next 30 Days'
								WHEN 'Next60Day' THEN 'Next 60 Days'
								WHEN 'Next90Day' THEN 'Next 90 Days'
								WHEN 'Next120Day' THEN 'Next 120 Days'
								ELSE '-'
							END),')')
						ELSE 
							'' 
					END)
			END) AS vcTimeline
			,SRGR.intSortOrder
		FROM
			ScheduledReportsGroupReports SRGR
		INNER JOIN
			ReportDashboard RD
		ON
			SRGR.numReportID = RD.numDashBoardID
		INNER JOIN
			ReportListMaster 
		ON
			RD.numReportID = ReportListMaster.numReportID
		LEFT JOIN 
			ReportModuleMaster RMM 
		ON 
			ReportListMaster.numReportModuleID=RMM.numReportModuleID
		WHERE
			SRGR.numSRGID = @numSRGID
			AND (ReportListMaster.numDomainID = @numDomainID OR ISNULL(ReportListMaster.bitDefault,0) = 1)
			AND SRGR.tintReportType = 6

		INSERT INTO @TEMP
		(
			ID
			,numReportID
			,tintReportType
			,vcReportName
			,vcReportType
			,vcModuleName
			,vcPerspective
			,vcTimeline
			,intSortOrder
		)
		SELECT
			SRGR.ID
			,numSearchID AS numReportID,
			(CASE WHEN ISNULL(numSharedFromSearchID,0) > 0 THEN 4 ELSE 3 END) AS tintReportType,
			(CASE WHEN ISNULL(S.vcSearchConditionJson,'') = '' THEN CONCAT('<a href="javascript:OpenSearchResult(',S.numSearchID,',',ISNULL(S.numFormID,0),')">',ISNULL(vcSearchName,''),'</a>') ELSE ISNULL(vcSearchName,'') END) AS vcReportName,
			'' AS vcReportType,
			(CASE DFM.numFormId WHEN 1 THEN 'Companies & Contacts Search' ELSE DFM.vcFormName END) AS vcModuleName,
			'' AS vcPerspective,
			'' AS vcTimeline,
			SRGR.intSortOrder
		FROM
			ScheduledReportsGroupReports SRGR
		INNER JOIN
			dbo.SavedSearch S
		ON
			SRGR.numReportID = S.numSearchID
		LEFT JOIN
			dbo.DynamicFormMaster DFM 
		ON 
			DFM.numFormId = S.numFormId
		WHERE 
			numDomainID = @numDomainID
			AND SRGR.numSRGID = @numSRGID
			AND SRGR.tintReportType IN (4,5)

		SELECT 0 AS TotalRecords,* FROM @TEMP ORDER BY intSortOrder
	END
	ELSE IF @tintMode = 3
	BEGIN
		UPDATE ScheduledReportsGroup SET tintDataCacheStatus = 1 WHERE numDomainID=@numDomainID AND ID=@numSRGID

		SELECT 
			SRGR.ID
			,SRGR.numSRGID
			,(CASE 
				WHEN SRGR.tintReportType = 6 THEN RLMDashboard.numReportID
				ELSE SRGR.numReportID
			END) numReportID
			,(CASE 
				WHEN SRGR.tintReportType = 6 THEN (CASE WHEN ISNULL(RLMDashboard.bitDefault,0) = 1 THEN 2 ELSE 1 END)
				ELSE SRGR.tintReportType
			END) tintReportType
			,SRGR.intSortOrder
			,(CASE 
				WHEN SRGR.tintReportType IN (1,2) THEN ISNULL(RLM.vcReportName,'') 
				WHEN SRGR.tintReportType IN (4,5) THEN ISNULL(SS.vcSearchName,'') 
				WHEN SRGR.tintReportType = 6 THEN (CASE WHEN ISNULL(RD.vcHeaderText,'') <> '' THEN ISNULL(RD.vcHeaderText,'') ELSE ISNULL(RLMDashboard.vcReportName,'') END)
				ELSE ''
			END) vcReportName
			,(CASE 
				WHEN SRGR.tintReportType = 6 THEN (CASE WHEN ISNULL(RLMDashboard.bitDefault,0) = 1 THEN ISNULL(RLMDashboard.intDefaultReportID,0) ELSE 0 END)
				ELSE ISNULL(RLM.intDefaultReportID,0)
			END) intDefaultReportID
			,ISNULL(SS.numFormID,0) numFormID
			,ISNULL(SS.numUserCntID,0) numSearchUserCntID
			,ISNULL(SS.vcSearchConditionJson,'') vcSearchConditionJson
			,ISNULL(SS.vcDisplayColumns,'') vcDisplayColumns
			,ISNULL(RD.numDashBoardID,0) numDashBoardID
		FROM 
			ScheduledReportsGroup SRG 
		INNER JOIN 
			ScheduledReportsGroupReports SRGR 
		ON 
			SRG.ID=SRGR.numSRGID 
		LEFT JOIN
			ReportListMaster RLM
		ON
			SRGR.numReportID = RLM.numReportID
			AND SRGR.tintReportType IN (1,2)
		LEFT JOIN
			ReportDashboard RD
		ON
			SRGR.numReportID = RD.numDashBoardID
			AND SRGR.tintReportType = 6
		LEFT JOIN
			ReportListMaster RLMDashboard
		ON
			RD.numReportID = RLMDashboard.numReportID
		LEFT JOIN
			SavedSearch SS
		ON
			SRGR.numReportID = SS.numSearchID
			AND SRGR.tintReportType IN (4,5)
		WHERE 
			SRG.numDomainID = @numDomainID 
			AND SRG.ID=@numSRGID 
		ORDER BY 
			intSortOrder
	END
END
GO