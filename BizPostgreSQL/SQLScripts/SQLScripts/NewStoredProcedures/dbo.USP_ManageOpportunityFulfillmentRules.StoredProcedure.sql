GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_ManageOpportunityFulfillmentRules' ) 
    DROP PROCEDURE USP_ManageOpportunityFulfillmentRules
GO
CREATE PROCEDURE USP_ManageOpportunityFulfillmentRules
    @numDomainID AS NUMERIC,
    @numBizDocTypeID AS NUMERIC,
    @strItems TEXT,
    @tintMode AS TINYINT
AS 
BEGIN
    IF @tintMode = 0
        BEGIN
			SELECT * FROM OpportunityFulfillmentRules WHERE numDomainID=@numDomainID AND (numBizDocTypeID=@numBizDocTypeID OR @numBizDocTypeID=0)
        END
	ELSE IF @tintMode = 1 
        BEGIN
            DELETE FROM OpportunityFulfillmentRules WHERE numDomainID=@numDomainID AND numBizDocTypeID=@numBizDocTypeID

            DECLARE @hDocItem INT
            IF CONVERT(VARCHAR(10), @strItems) <> '' 
                BEGIN
                    EXEC sp_xml_preparedocument @hDocItem OUTPUT, @strItems
            
                    INSERT  INTO dbo.OpportunityFulfillmentRules ( numDomainID,numRuleID,numBizDocTypeID,numBizDocStatus)
                            SELECT  @numDomainID,
                                    X.numRuleID,
                                    @numBizDocTypeID,
                                    X.numBizDocStatus
                            FROM    ( SELECT    *
                                      FROM      OPENXML (@hDocItem, '/NewDataSet/Table', 2)
                                                WITH ( numRuleID NUMERIC(18),numBizDocStatus NUMERIC(18))
                                    ) X
                    EXEC sp_xml_removedocument @hDocItem
                END
            SELECT  ''
        END	
 END



