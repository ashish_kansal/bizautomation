GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ElasticSearch_GetCases')
DROP PROCEDURE dbo.USP_ElasticSearch_GetCases
GO
CREATE PROCEDURE [dbo].[USP_ElasticSearch_GetCases]
	@numDomainID NUMERIC(18,0)
	,@vcCaseIds VARCHAR(MAX)
AS 
BEGIN
	SELECT 
		numCaseId AS id
		,'case' AS module
		,ISNULL(Cases.numRecOwner,0) numRecOwner
		,ISNULL(Cases.numAssignedTo,0) numAssignedTo
		,ISNULL(DivisionMaster.numTerID,0) numTerID
		,CONCAT('/cases/frmCases.aspx?frm=Caselist&CaseID=',numCaseId) AS url
		,CONCAT('<b style="color:#7f6000">','Case:</b> ',ISNULL(vcCaseNumber,''),', ',vcCompanyName,', ',ISNULL(textSubject,'')) AS displaytext
		,vcCaseNumber AS [text]
		,ISNULL(vcCaseNumber,'') AS Search_vcCaseNumber
		,ISNULL(textSubject,'') AS Search_textSubject
	FROM
		Cases
	INNER JOIN
		DivisionMaster
	ON
		Cases.numDivisionId = DivisionMaster.numDivisionID
	INNER JOIN
		CompanyInfo
	ON
		DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
	WHERE
		Cases.numDomainID=@numDomainID
		AND (@vcCaseIds IS NULL OR numCaseId IN (SELECT Id FROM dbo.SplitIDs(ISNULL(@vcCaseIds,'0'),',')))
END