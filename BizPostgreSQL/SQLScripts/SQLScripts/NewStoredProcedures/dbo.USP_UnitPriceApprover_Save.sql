
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_UnitPriceApprover_Save')
DROP PROCEDURE USP_UnitPriceApprover_Save
GO
CREATE PROCEDURE [dbo].[USP_UnitPriceApprover_Save]
(
	@numDomainID NUMERIC(18,0)
	,@vcUnitPriceApprovers VARCHAR(1000)
)
AS
BEGIN
	DELETE FROM UnitPriceApprover WHERE numDomainID = @numDomainID

	INSERT INTO UnitPriceApprover
		(
			numDomainID,
			numUserID
		)
	SELECT
		 @numDomainID,
		 Split.a.value('.', 'VARCHAR(100)') AS String
	FROM  
	(
	SELECT 
			CAST ('<M>' + REPLACE(@vcUnitPriceApprovers, ',', '</M><M>') + '</M>' AS XML) AS String  
	) AS A 
	CROSS APPLY 
		String.nodes ('/M') AS Split(a); 
END