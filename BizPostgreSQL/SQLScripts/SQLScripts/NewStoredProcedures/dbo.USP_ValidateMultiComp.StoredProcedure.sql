-- EXEC USP_ValidateMultiComp 78
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ValidateMultiComp')
DROP PROCEDURE USP_ValidateMultiComp
GO
CREATE PROCEDURE  USP_ValidateMultiComp
    @numDomainID NUMERIC(9)
AS 
    BEGIN
        DECLARE @DomainSubscriberID NUMERIC(9)
        DECLARE @MultiCompSubscriberID NUMERIC(9)
        /* Only Top patent company should be able to see multicomp data */
    
		SELECT  @DomainSubscriberID = ISNULL([numSubscriberID],0) FROM [Subscribers] WHERE [numTargetDomainID]= @numDomainID
        
        SELECT COUNT(*) FROM [Domain] WHERE [numSubscriberID]=@DomainSubscriberID
        
        /*
        SELECT  @MultiCompSubscriberID = ISNULL([numSubscriberID],0)
        FROM    [Domain]
        WHERE   [numDomainId] = @numDomainID
 
        SELECT  @DomainSubscriberID = ISNULL([numSubscriberID],0)
        FROM    [Subscribers]
        WHERE   [numTargetDomainID] = @numDomainID	
        
        
        PRINT @MultiCompSubscriberID
        PRINT @DomainSubscriberID
        
        
        IF @DomainSubscriberID = 0
            OR @MultiCompSubscriberID = 0 
            BEGIN
                SELECT  0 
                RETURN	
            END
			
        
        IF @DomainSubscriberID = @MultiCompSubscriberID 
            SELECT  1
        ELSE 
            SELECT  0 
	*/
    END
