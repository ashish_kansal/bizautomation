IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_OpportunityMaster_GetPurchaseOrderForFulfillment')
DROP PROCEDURE USP_OpportunityMaster_GetPurchaseOrderForFulfillment
GO

CREATE PROCEDURE USP_OpportunityMaster_GetPurchaseOrderForFulfillment
	@numDomainID NUMERIC(18,0)
	,@tintSearchType TINYINT
	,@vcSearchText VARCHAR(300)
	,@intOffset INT
	,@intPageSize INT
	,@intTotalRecords INT OUTPUT
AS 
BEGIN
	DECLARE @TEMP TABLE
	(
		ID INT IDENTITY(1,1)
		,RecordID NUMERIC(18,0)
		,RecordValue VARCHAR(300)
	)

	IF @tintSearchType = 1 -- Item
	BEGIN
		INSERT INTO @TEMP
		(
			RecordID
			,RecordValue
		)
		SELECT
			numItemCode
			,CONCAT(vcItemName,CASE WHEN LEN(ISNULL(Item.vcSKU,'')) > 0 THEN CONCAT(' (SKU:',Item.vcSKU,')') ELSE '' END)
		FROM
			Item
		WHERE
			numDomainID=@numDomainID
			AND vcItemName LIKE '%' + @vcSearchText + '%' 
	END
	ELSE IF @tintSearchType = 2 -- Warehouse
	BEGIN
		INSERT INTO @TEMP
		(
			RecordID
			,RecordValue
		)
		SELECT
			numWareHouseID
			,vcWareHouse
		FROM 
			Warehouses
		WHERE
			numDomainID=@numDomainID
			AND vcWareHouse LIKE '%' + @vcSearchText + '%' 
	END
	ELSE IF @tintSearchType = 3 -- Purchase Order
	BEGIN
		INSERT INTO @TEMP
		(
			RecordID
			,RecordValue
		)
		SELECT
			numOppId
			,vcPOppName
		FROM
			OpportunityMaster
		WHERE
			numDomainId=@numDomainID
			AND tintOppType=2
			AND tintOppStatus=1
			AND ISNULL(bitStockTransfer,0) = 0
			AND vcPOppName LIKE '%' + @vcSearchText + '%'
	END
	ELSE IF @tintSearchType = 4 -- Vendor
	BEGIN
		INSERT INTO @TEMP
		(
			RecordID
			,RecordValue
		)
		SELECT
			numDivisionID
			,vcCompanyName
		FROM 
			CompanyInfo
		INNER JOIN 
			DivisionMaster
		ON
			CompanyInfo.numCompanyId = DivisionMaster.numCompanyID
		WHERE
			CompanyInfo.numDomainID=@numDomainID
			AND vcCompanyName LIKE '%' + @vcSearchText + '%'

	END
	ELSE IF @tintSearchType = 5 -- BizDoc
	BEGIN
		INSERT INTO @TEMP
		(
			RecordID
			,RecordValue
		)
		SELECT
			numOppBizDocsId
			,vcBizDocID
		FROM
			OpportunityBizDocs
		INNER JOIN
			OpportunityMaster
		ON
			OpportunityBizDocs.numOppId = OpportunityMaster.numOppId
		WHERE
			OpportunityMaster.numDomainId = @numDomainID
			AND tintOppType=2
			AND tintOppStatus=1
			AND ISNULL(bitStockTransfer,0) = 0
			AND OpportunityBizDocs.vcBizDocID LIKE '%' + @vcSearchText + '%'
	END
	ELSE IF @tintSearchType = 6 -- SKU
	BEGIN
		INSERT INTO @TEMP
		(
			RecordID
			,RecordValue
		)
		SELECT DISTINCT
			numItemCode
			,CONCAT(vcItemName,CASE WHEN LEN(ISNULL(Item.vcSKU,'')) > 0 THEN CONCAT(' (SKU:',Item.vcSKU,')') ELSE '' END)
		FROM
			Item
		LEFT JOIN
			WareHouseItems
		ON
			Item.numItemCode = WareHouseItems.numItemID
		WHERE
			Item.numDomainID=@numDomainID
			AND (vcSKU LIKE '%' + @vcSearchText + '%' OR vcWHSKU LIKE '%' + @vcSearchText + '%')

	END

	SELECT * FROM @TEMP WHERE ID BETWEEN @intOffset+1 AND @intOffset+@intPageSize
	
	SET @intTotalRecords = (SELECT COUNT(*) FROM @TEMP)
END
GO