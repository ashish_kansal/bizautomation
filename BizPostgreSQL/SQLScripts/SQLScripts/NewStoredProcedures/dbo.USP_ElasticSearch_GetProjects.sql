GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ElasticSearch_GetProjects')
DROP PROCEDURE dbo.USP_ElasticSearch_GetProjects
GO
CREATE PROCEDURE [dbo].[USP_ElasticSearch_GetProjects]
	@numDomainID NUMERIC(18,0)
	,@vcProjectIds VARCHAR(MAX)
AS 
BEGIN
	SELECT
		numProId AS id
		,'project' AS module
		,ISNULL(ProjectsMaster.numRecOwner,0) numRecOwner
		,ISNULL(ProjectsMaster.numAssignedTo,0) numAssignedTo
		,ISNULL(DivisionMaster.numTerID,0) numTerID
		,CONCAT('<b style="color:#7030a0">Project:</b> ',vcProjectName,', ', vcCompanyName) AS displaytext
		,vcProjectName AS [text]
		,CONCAT('/projects/frmProjects.aspx?frm=ProjectList&ProId=',numProId) AS url
		,ISNULL(vcProjectID,'') AS Search_vcProjectID
		,ISNULL(vcProjectName,'') AS Search_vcProjectName
	FROM
		ProjectsMaster
	INNER JOIN
		DivisionMaster
	ON
		ProjectsMaster.numDivisionId = DivisionMaster.numDivisionID
	INNER JOIN
		CompanyInfo
	ON
		DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
	WHERE
		ProjectsMaster.numDomainId=@numDomainID
		AND (@vcProjectIds IS NULL OR numProId IN (SELECT Id FROM dbo.SplitIDs(ISNULL(@vcProjectIds,'0'),',')))
END