GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created By Anoop Jayaraj                                                          
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_Item_ImportPriceLevel')
DROP PROCEDURE USP_Item_ImportPriceLevel
GO
CREATE PROCEDURE [dbo].[USP_Item_ImportPriceLevel]
(
	@numItemCode nUMERIC(18,0)
	,@tintRuleType INT
	,@tintDiscountType INT
	,@monPriceLevel1 DECIMAL(20,5)
	,@monPriceLevel2 DECIMAL(20,5)
	,@monPriceLevel3 DECIMAL(20,5)
	,@monPriceLevel4 DECIMAL(20,5)
	,@monPriceLevel5 DECIMAL(20,5)
	,@monPriceLevel6 DECIMAL(20,5)
	,@monPriceLevel7 DECIMAL(20,5)
	,@monPriceLevel8 DECIMAL(20,5)
	,@monPriceLevel9 DECIMAL(20,5)
	,@monPriceLevel10 DECIMAL(20,5)
	,@monPriceLevel11 DECIMAL(20,5)
	,@monPriceLevel12 DECIMAL(20,5)
	,@monPriceLevel13 DECIMAL(20,5)
	,@monPriceLevel14 DECIMAL(20,5)
	,@monPriceLevel15 DECIMAL(20,5)
	,@monPriceLevel16 DECIMAL(20,5)
	,@monPriceLevel17 DECIMAL(20,5)
	,@monPriceLevel18 DECIMAL(20,5)
	,@monPriceLevel19 DECIMAL(20,5)
	,@monPriceLevel20 DECIMAL(20,5)
	,@intPriceLevel1FromQty INT
	,@intPriceLevel1ToQty INT
	,@intPriceLevel2FromQty INT
	,@intPriceLevel2ToQty INT
	,@intPriceLevel3FromQty INT
	,@intPriceLevel3ToQty INT
	,@intPriceLevel4FromQty INT
	,@intPriceLevel4ToQty INT
	,@intPriceLevel5FromQty INT
	,@intPriceLevel5ToQty INT
	,@intPriceLevel6FromQty INT
	,@intPriceLevel6ToQty INT
	,@intPriceLevel7FromQty INT
	,@intPriceLevel7ToQty INT
	,@intPriceLevel8FromQty INT
	,@intPriceLevel8ToQty INT
	,@intPriceLevel9FromQty INT
	,@intPriceLevel9ToQty INT
	,@intPriceLevel10FromQty INT
	,@intPriceLevel10ToQty INT
	,@intPriceLevel11FromQty INT
	,@intPriceLevel11ToQty INT
	,@intPriceLevel12FromQty INT
	,@intPriceLevel12ToQty INT
	,@intPriceLevel13FromQty INT
	,@intPriceLevel13ToQty INT
	,@intPriceLevel14FromQty INT
	,@intPriceLevel14ToQty INT
	,@intPriceLevel15FromQty INT
	,@intPriceLevel15ToQty INT
	,@intPriceLevel16FromQty INT
	,@intPriceLevel16ToQty INT
	,@intPriceLevel17FromQty INT
	,@intPriceLevel17ToQty INT
	,@intPriceLevel18FromQty INT
	,@intPriceLevel18ToQty INT
	,@intPriceLevel19FromQty INT
	,@intPriceLevel19ToQty INT
	,@intPriceLevel20FromQty INT
	,@intPriceLevel20ToQty INT
	,@vcPriceLevel1Name VARCHAR(300)
	,@vcPriceLevel2Name VARCHAR(300)
	,@vcPriceLevel3Name VARCHAR(300)
	,@vcPriceLevel4Name VARCHAR(300)
	,@vcPriceLevel5Name VARCHAR(300)
	,@vcPriceLevel6Name VARCHAR(300)
	,@vcPriceLevel7Name VARCHAR(300)
	,@vcPriceLevel8Name VARCHAR(300)
	,@vcPriceLevel9Name VARCHAR(300)
	,@vcPriceLevel10Name VARCHAR(300)
	,@vcPriceLevel11Name VARCHAR(300)
	,@vcPriceLevel12Name VARCHAR(300)
	,@vcPriceLevel13Name VARCHAR(300)
	,@vcPriceLevel14Name VARCHAR(300)
	,@vcPriceLevel15Name VARCHAR(300)
	,@vcPriceLevel16Name VARCHAR(300)
	,@vcPriceLevel17Name VARCHAR(300)
	,@vcPriceLevel18Name VARCHAR(300)
	,@vcPriceLevel19Name VARCHAR(300)
	,@vcPriceLevel20Name VARCHAR(300)
) 
AS
BEGIN
	IF @monPriceLevel1 = -1
		AND @monPriceLevel2 = -1
		AND @monPriceLevel3 = -1
		AND @monPriceLevel4 = -1
		AND @monPriceLevel5 = -1
		AND @monPriceLevel6 = -1
		AND @monPriceLevel7 = -1
		AND @monPriceLevel8 = -1
		AND @monPriceLevel9 = -1
		AND @monPriceLevel10 = -1
		AND @monPriceLevel11 = -1
		AND @monPriceLevel12 = -1
		AND @monPriceLevel13 = -1
		AND @monPriceLevel14 = -1
		AND @monPriceLevel15 = -1
		AND @monPriceLevel16 = -1
		AND @monPriceLevel17 = -1
		AND @monPriceLevel18 = -1
		AND @monPriceLevel19 = -1
		AND @monPriceLevel20 = -1
	BEGIN
		RETURN
	END

	IF ISNULL(@intPriceLevel1FromQty,0) = 0 AND ISNULL(@intPriceLevel1ToQty,0) = 0
	BEGIN
		SET @intPriceLevel1FromQty = -1
		SET @intPriceLevel1ToQty = -1
	END
	IF ISNULL(@intPriceLevel2FromQty,0) = 0 AND ISNULL(@intPriceLevel2ToQty,0) = 0
	BEGIN
		SET @intPriceLevel2FromQty = -1
		SET @intPriceLevel2ToQty = -1
	END
	IF ISNULL(@intPriceLevel3FromQty,0) = 0 AND ISNULL(@intPriceLevel3ToQty,0) = 0
	BEGIN
		SET @intPriceLevel3FromQty = -1
		SET @intPriceLevel3ToQty = -1
	END
	IF ISNULL(@intPriceLevel4FromQty,0) = 0 AND ISNULL(@intPriceLevel4ToQty,0) = 0
	BEGIN
		SET @intPriceLevel4FromQty = -1
		SET @intPriceLevel4ToQty = -1
	END
	IF ISNULL(@intPriceLevel5FromQty,0) = 0 AND ISNULL(@intPriceLevel5ToQty,0) = 0
	BEGIN
		SET @intPriceLevel5FromQty = -1
		SET @intPriceLevel5ToQty = -1
	END
	IF ISNULL(@intPriceLevel6FromQty,0) = 0 AND ISNULL(@intPriceLevel6ToQty,0) = 0
	BEGIN
		SET @intPriceLevel6FromQty = -1
		SET @intPriceLevel6ToQty = -1
	END
	IF ISNULL(@intPriceLevel7FromQty,0) = 0 AND ISNULL(@intPriceLevel7ToQty,0) = 0
	BEGIN
		SET @intPriceLevel7FromQty = -1
		SET @intPriceLevel7ToQty = -1
	END
	IF ISNULL(@intPriceLevel8FromQty,0) = 0 AND ISNULL(@intPriceLevel8ToQty,0) = 0
	BEGIN
		SET @intPriceLevel8FromQty = -1
		SET @intPriceLevel8ToQty = -1
	END
	IF ISNULL(@intPriceLevel9FromQty,0) = 0 AND ISNULL(@intPriceLevel9ToQty,0) = 0
	BEGIN
		SET @intPriceLevel9FromQty = -1
		SET @intPriceLevel9ToQty = -1
	END
	IF ISNULL(@intPriceLevel10FromQty,0) = 0 AND ISNULL(@intPriceLevel10ToQty,0) = 0
	BEGIN
		SET @intPriceLevel10FromQty = -1
		SET @intPriceLevel10ToQty = -1
	END
	IF ISNULL(@intPriceLevel11FromQty,0) = 0 AND ISNULL(@intPriceLevel11ToQty,0) = 0
	BEGIN
		SET @intPriceLevel11FromQty = -1
		SET @intPriceLevel11ToQty = -1
	END
	IF ISNULL(@intPriceLevel12FromQty,0) = 0 AND ISNULL(@intPriceLevel12ToQty,0) = 0
	BEGIN
		SET @intPriceLevel12FromQty = -1
		SET @intPriceLevel12ToQty = -1
	END
	IF ISNULL(@intPriceLevel13FromQty,0) = 0 AND ISNULL(@intPriceLevel13ToQty,0) = 0
	BEGIN
		SET @intPriceLevel13FromQty = -1
		SET @intPriceLevel13ToQty = -1
	END
	IF ISNULL(@intPriceLevel14FromQty,0) = 0 AND ISNULL(@intPriceLevel14ToQty,0) = 0
	BEGIN
		SET @intPriceLevel14FromQty = -1
		SET @intPriceLevel14ToQty = -1
	END
	IF ISNULL(@intPriceLevel15FromQty,0) = 0 AND ISNULL(@intPriceLevel15ToQty,0) = 0
	BEGIN
		SET @intPriceLevel15FromQty = -1
		SET @intPriceLevel15ToQty = -1
	END
	IF ISNULL(@intPriceLevel16FromQty,0) = 0 AND ISNULL(@intPriceLevel16ToQty,0) = 0
	BEGIN
		SET @intPriceLevel16FromQty = -1
		SET @intPriceLevel16ToQty = -1
	END
	IF ISNULL(@intPriceLevel17FromQty,0) = 0 AND ISNULL(@intPriceLevel17ToQty,0) = 0
	BEGIN
		SET @intPriceLevel17FromQty = -1
		SET @intPriceLevel17ToQty = -1
	END
	IF ISNULL(@intPriceLevel18FromQty,0) = 0 AND ISNULL(@intPriceLevel18ToQty,0) = 0
	BEGIN
		SET @intPriceLevel18FromQty = -1
		SET @intPriceLevel18ToQty = -1
	END
	IF ISNULL(@intPriceLevel19FromQty,0) = 0 AND ISNULL(@intPriceLevel19ToQty,0) = 0
	BEGIN
		SET @intPriceLevel19FromQty = -1
		SET @intPriceLevel19ToQty = -1
	END
	IF ISNULL(@intPriceLevel20FromQty,0) = 0 AND ISNULL(@intPriceLevel20ToQty,0) = 0
	BEGIN
		SET @intPriceLevel20FromQty = -1
		SET @intPriceLevel20ToQty = -1
	END



	IF ISNULL(@intPriceLevel1FromQty,0) = 0 OR ISNULL(@intPriceLevel1ToQty,0) = 0
	BEGIN
		RAISERROR('QTY_FROM_AND_TO_MUST_BE_GREATER_THEN_0_FOR_PRICE_LEVEL_1',16,1)
		RETURN
	END
	IF ISNULL(@intPriceLevel2FromQty,0) = 0 OR ISNULL(@intPriceLevel2ToQty,0) = 0
	BEGIN
		RAISERROR('QTY_FROM_AND_TO_MUST_BE_GREATER_THEN_0_FOR_PRICE_LEVEL_2',16,1)
		RETURN
	END
	IF ISNULL(@intPriceLevel3FromQty,0) = 0 OR ISNULL(@intPriceLevel3ToQty,0) = 0
	BEGIN
		RAISERROR('QTY_FROM_AND_TO_MUST_BE_GREATER_THEN_0_FOR_PRICE_LEVEL_3',16,1)
		RETURN
	END
	IF ISNULL(@intPriceLevel4FromQty,0) = 0 OR ISNULL(@intPriceLevel4ToQty,0) = 0
	BEGIN
		RAISERROR('QTY_FROM_AND_TO_MUST_BE_GREATER_THEN_0_FOR_PRICE_LEVEL_4',16,1)
		RETURN
	END
	IF ISNULL(@intPriceLevel5FromQty,0) = 0 OR ISNULL(@intPriceLevel5ToQty,0) = 0
	BEGIN
		RAISERROR('QTY_FROM_AND_TO_MUST_BE_GREATER_THEN_0_FOR_PRICE_LEVEL_5',16,1)
		RETURN
	END
	IF ISNULL(@intPriceLevel6FromQty,0) = 0 OR ISNULL(@intPriceLevel6ToQty,0) = 0
	BEGIN
		RAISERROR('QTY_FROM_AND_TO_MUST_BE_GREATER_THEN_0_FOR_PRICE_LEVEL_6',16,1)
		RETURN
	END
	IF ISNULL(@intPriceLevel7FromQty,0) = 0 OR ISNULL(@intPriceLevel7ToQty,0) = 0
	BEGIN
		RAISERROR('QTY_FROM_AND_TO_MUST_BE_GREATER_THEN_0_FOR_PRICE_LEVEL_7',16,1)
		RETURN
	END
	IF ISNULL(@intPriceLevel8FromQty,0) = 0 OR ISNULL(@intPriceLevel8ToQty,0) = 0
	BEGIN
		RAISERROR('QTY_FROM_AND_TO_MUST_BE_GREATER_THEN_0_FOR_PRICE_LEVEL_8',16,1)
		RETURN
	END
	IF ISNULL(@intPriceLevel9FromQty,0) = 0 OR ISNULL(@intPriceLevel9ToQty,0) = 0
	BEGIN
		RAISERROR('QTY_FROM_AND_TO_MUST_BE_GREATER_THEN_0_FOR_PRICE_LEVEL_9',16,1)
		RETURN
	END
	IF ISNULL(@intPriceLevel10FromQty,0) = 0 OR ISNULL(@intPriceLevel10ToQty,0) = 0
	BEGIN
		RAISERROR('QTY_FROM_AND_TO_MUST_BE_GREATER_THEN_0_FOR_PRICE_LEVEL_10',16,1)
		RETURN
	END
	IF ISNULL(@intPriceLevel11FromQty,0) = 0 OR ISNULL(@intPriceLevel11ToQty,0) = 0
	BEGIN
		RAISERROR('QTY_FROM_AND_TO_MUST_BE_GREATER_THEN_0_FOR_PRICE_LEVEL_11',16,1)
		RETURN
	END
	IF ISNULL(@intPriceLevel12FromQty,0) = 0 OR ISNULL(@intPriceLevel12ToQty,0) = 0
	BEGIN
		RAISERROR('QTY_FROM_AND_TO_MUST_BE_GREATER_THEN_0_FOR_PRICE_LEVEL_12',16,1)
		RETURN
	END
	IF ISNULL(@intPriceLevel13FromQty,0) = 0 OR ISNULL(@intPriceLevel13ToQty,0) = 0
	BEGIN
		RAISERROR('QTY_FROM_AND_TO_MUST_BE_GREATER_THEN_0_FOR_PRICE_LEVEL_13',16,1)
		RETURN
	END
	IF ISNULL(@intPriceLevel14FromQty,0) = 0 OR ISNULL(@intPriceLevel14ToQty,0) = 0
	BEGIN
		RAISERROR('QTY_FROM_AND_TO_MUST_BE_GREATER_THEN_0_FOR_PRICE_LEVEL_14',16,1)
		RETURN
	END
	IF ISNULL(@intPriceLevel15FromQty,0) = 0 OR ISNULL(@intPriceLevel15ToQty,0) = 0
	BEGIN
		RAISERROR('QTY_FROM_AND_TO_MUST_BE_GREATER_THEN_0_FOR_PRICE_LEVEL_15',16,1)
		RETURN
	END
	IF ISNULL(@intPriceLevel16FromQty,0) = 0 OR ISNULL(@intPriceLevel16ToQty,0) = 0
	BEGIN
		RAISERROR('QTY_FROM_AND_TO_MUST_BE_GREATER_THEN_0_FOR_PRICE_LEVEL_16',16,1)
		RETURN
	END
	IF ISNULL(@intPriceLevel17FromQty,0) = 0 OR ISNULL(@intPriceLevel17ToQty,0) = 0
	BEGIN
		RAISERROR('QTY_FROM_AND_TO_MUST_BE_GREATER_THEN_0_FOR_PRICE_LEVEL_17',16,1)
		RETURN
	END
	IF ISNULL(@intPriceLevel18FromQty,0) = 0 OR ISNULL(@intPriceLevel18ToQty,0) = 0
	BEGIN
		RAISERROR('QTY_FROM_AND_TO_MUST_BE_GREATER_THEN_0_FOR_PRICE_LEVEL_18',16,1)
		RETURN
	END
	IF ISNULL(@intPriceLevel19FromQty,0) = 0 OR ISNULL(@intPriceLevel19ToQty,0) = 0
	BEGIN
		RAISERROR('QTY_FROM_AND_TO_MUST_BE_GREATER_THEN_0_FOR_PRICE_LEVEL_19',16,1)
		RETURN
	END
	IF ISNULL(@intPriceLevel20FromQty,0) = 0 OR ISNULL(@intPriceLevel20ToQty,0) = 0
	BEGIN
		RAISERROR('QTY_FROM_AND_TO_MUST_BE_GREATER_THEN_0_FOR_PRICE_LEVEL_20',16,1)
		RETURN
	END


	DECLARE @Defualt TABLE
	(
		PriceLevelID INT
		,tintRuleType TINYINT
		,tintDiscountType TINYINT
		,monPrice DECIMAL(20,5)
		,intFromQty INT
		,intToQty INT
		,vcName VARCHAR(300)
	)

	INSERT INTO @Defualt
	(
		PriceLevelID
		,monPrice
		,intFromQty
		,intToQty
		,vcName
	)
	VALUES
	(1,-1,-1,-1,'-1'),
	(2,-1,-1,-1,'-1'),
	(3,-1,-1,-1,'-1'),
	(4,-1,-1,-1,'-1'),
	(5,-1,-1,-1,'-1'),
	(6,-1,-1,-1,'-1'),
	(7,-1,-1,-1,'-1'),
	(8,-1,-1,-1,'-1'),
	(9,-1,-1,-1,'-1'),
	(10,-1,-1,-1,'-1'),
	(11,-1,-1,-1,'-1'),
	(12,-1,-1,-1,'-1'),
	(13,-1,-1,-1,'-1'),
	(14,-1,-1,-1,'-1'),
	(15,-1,-1,-1,'-1'),
	(16,-1,-1,-1,'-1'),
	(17,-1,-1,-1,'-1'),
	(18,-1,-1,-1,'-1'),
	(19,-1,-1,-1,'-1'),
	(20,-1,-1,-1,'-1')

	-- FIRST REPLACE -1 WITH EXISTING ITEM PRICES, REPLEACE -1 IN FROM AND TO QTY ONLY WHEN RuleType is Deduct From List Price or Add to vendor Cost
	UPDATE
		t1
	SET
		t1.monPrice = ISNULL(t2.decDiscount,t1.monPrice)
		,tintRuleType = (CASE WHEN ISNULL(@tintRuleType,0) <> -1 THEN @tintRuleType ELSE ISNULL(t2.tintRuleType,t1.tintRuleType) END)
		,tintDiscountType = (CASE WHEN ISNULL(@tintDiscountType,0) <> -1 THEN @tintDiscountType ELSE ISNULL(t2.tintDiscountType,t1.tintDiscountType) END)
		,t1.intFromQty = (CASE WHEN t2.tintRuleType = 1 OR t2.tintRuleType = 2 THEN t2.intFromQty ELSE t1.intFromQty END)
		,t1.intToQty = (CASE WHEN t2.tintRuleType = 1 OR t2.tintRuleType = 2 THEN t2.intToQty ELSE t1.intToQty END)
		,t1.vcName = ISNULL(t2.vcName,'')
	FROM
		@Defualt t1
	LEFT JOIN
	(
		SELECT
			ROW_NUMBER() OVER(ORDER BY numPricingID) AS RowID 
			,decDiscount
			,intFromQty
			,intToQty
			,tintRuleType
			,tintDiscountType
			,vcName
		FROM 
			PricingTable WHERE numItemCode=@numItemCode 
			AND ISNULL(numCurrencyID,0) = 0
			AND ISNULL(numPriceRuleID,0)=0
	) t2
	ON
		t1.PriceLevelID=t2.RowID

	DECLARE @localRuleType TINYINT
	DECLARE @localDiscountType TINYINT

	SELECT TOP 1
		@localRuleType = tintRuleType
		,@localDiscountType = tintDiscountType
	FROM
		@Defualt AS t2
		

	IF ISNULL(@localRuleType,0) = 0
	BEGIN
		RAISERROR('INVALID_PRICE_RULE_TYPE',16,1)
		RETURN
	END
	ELSE IF (@localRuleType = 1 OR @localRuleType = 2) AND @localDiscountType NOT IN (1,2,3)
	BEGIN
		RAISERROR('INVALID_PRICE_LEVEL_DISCOUNT_TYPE',16,1)
		RETURN
	END
	ELSE IF @localRuleType = 3
	BEGIN
		SET @tintDiscountType = 0
	END

	-- REPLACE -1 WITH NEW PRICES IF PROVIDED FROM ITEM IMPORT (IF COLUMN IF NOT AVAILABLE IN IMPORT FILE THAN VALUE WILL BE -1)
	-- REPLACE -1 WITH NEW QTY FROM IF PROVIDED FROM ITEM IMPORT (IF COLUMN IF NOT AVAILABLE IN IMPORT FILE THAN VALUE WILL BE -1)
	-- REPLACE -1 WITH NEW QTY TO IF PROVIDED FROM ITEM IMPORT (IF COLUMN IF NOT AVAILABLE IN IMPORT FILE THAN VALUE WILL BE -1)
	UPDATE 
		@Defualt 
	SET 
		monPrice = (CASE WHEN ISNULL(@monPriceLevel1,0) <> -1 THEN ISNULL(@monPriceLevel1,0) ELSE monPrice END)
		,intFromQty = (CASE WHEN ISNULL(@intPriceLevel1FromQty,0) <> -1 THEN ISNULL(@intPriceLevel1FromQty,0) ELSE intFromQty END)
		,intToQty = (CASE WHEN ISNULL(@intPriceLevel1ToQty,0) <> -1 THEN ISNULL(@intPriceLevel1ToQty,0) ELSE intToQty END)
		,vcName = (CASE WHEN ISNULL(@vcPriceLevel1Name,'') <> '-1' THEN ISNULL(@vcPriceLevel1Name,0) ELSE vcName END)
	WHERE 
		PriceLevelID = 1

	UPDATE 
		@Defualt 
	SET 
		monPrice = (CASE WHEN ISNULL(@monPriceLevel2,0) <> -1 THEN ISNULL(@monPriceLevel2,0) ELSE monPrice END) 
		,intFromQty = (CASE WHEN ISNULL(@intPriceLevel2FromQty,0) <> -1 THEN ISNULL(@intPriceLevel2FromQty,0) ELSE intFromQty END)
		,intToQty = (CASE WHEN ISNULL(@intPriceLevel2ToQty,0) <> -1 THEN ISNULL(@intPriceLevel2ToQty,0) ELSE intToQty END)
		,vcName = (CASE WHEN ISNULL(@vcPriceLevel2Name,'') <> '-1' THEN ISNULL(@vcPriceLevel2Name,0) ELSE vcName END)
	WHERE 
		PriceLevelID = 2

	UPDATE 
		@Defualt 
	SET 
		monPrice = (CASE WHEN ISNULL(@monPriceLevel3,0) <> -1 THEN ISNULL(@monPriceLevel3,0) ELSE monPrice END) 
		,intFromQty = (CASE WHEN ISNULL(@intPriceLevel3FromQty,0) <> -1 THEN ISNULL(@intPriceLevel3FromQty,0) ELSE intFromQty END)
		,intToQty = (CASE WHEN ISNULL(@intPriceLevel3ToQty,0) <> -1 THEN ISNULL(@intPriceLevel3ToQty,0) ELSE intToQty END)
		,vcName = (CASE WHEN ISNULL(@vcPriceLevel3Name,'') <> '-1' THEN ISNULL(@vcPriceLevel3Name,0) ELSE vcName END)
	WHERE 
		PriceLevelID = 3

	UPDATE 
		@Defualt 
	SET 
		monPrice = (CASE WHEN ISNULL(@monPriceLevel4,0) <> -1 THEN ISNULL(@monPriceLevel4,0) ELSE monPrice END)
		,intFromQty = (CASE WHEN ISNULL(@intPriceLevel4FromQty,0) <> -1 THEN ISNULL(@intPriceLevel4FromQty,0) ELSE intFromQty END)
		,intToQty = (CASE WHEN ISNULL(@intPriceLevel4ToQty,0) <> -1 THEN ISNULL(@intPriceLevel4ToQty,0) ELSE intToQty END)
		,vcName = (CASE WHEN ISNULL(@vcPriceLevel4Name,'') <> '-1' THEN ISNULL(@vcPriceLevel4Name,0) ELSE vcName END)
	WHERE 
		PriceLevelID = 4

	UPDATE 
		@Defualt
	SET 
		monPrice = (CASE WHEN ISNULL(@monPriceLevel5,0) <> -1 THEN ISNULL(@monPriceLevel5,0) ELSE monPrice END)
		,intFromQty = (CASE WHEN ISNULL(@intPriceLevel5FromQty,0) <> -1 THEN ISNULL(@intPriceLevel5FromQty,0) ELSE intFromQty END)
		,intToQty = (CASE WHEN ISNULL(@intPriceLevel5ToQty,0) <> -1 THEN ISNULL(@intPriceLevel5ToQty,0) ELSE intToQty END)
		,vcName = (CASE WHEN ISNULL(@vcPriceLevel5Name,'') <> '-1' THEN ISNULL(@vcPriceLevel5Name,0) ELSE vcName END)
	WHERE 
		PriceLevelID = 5
	
	UPDATE
		@Defualt 
	SET 
		monPrice = (CASE WHEN ISNULL(@monPriceLevel6,0) <> -1 THEN ISNULL(@monPriceLevel6,0) ELSE monPrice END)
		,intFromQty = (CASE WHEN ISNULL(@intPriceLevel6FromQty,0) <> -1 THEN ISNULL(@intPriceLevel6FromQty,0) ELSE intFromQty END)
		,intToQty = (CASE WHEN ISNULL(@intPriceLevel6ToQty,0) <> -1 THEN ISNULL(@intPriceLevel6ToQty,0) ELSE intToQty END)
		,vcName = (CASE WHEN ISNULL(@vcPriceLevel6Name,'') <> '-1' THEN ISNULL(@vcPriceLevel6Name,0) ELSE vcName END)
	WHERE 
		PriceLevelID = 6

	UPDATE 
		@Defualt 
	SET 
		monPrice = (CASE WHEN ISNULL(@monPriceLevel7,0) <> -1 THEN ISNULL(@monPriceLevel7,0) ELSE monPrice END)
		,intFromQty = (CASE WHEN ISNULL(@intPriceLevel7FromQty,0) <> -1 THEN ISNULL(@intPriceLevel7FromQty,0) ELSE intFromQty END)
		,intToQty = (CASE WHEN ISNULL(@intPriceLevel7ToQty,0) <> -1 THEN ISNULL(@intPriceLevel7ToQty,0) ELSE intToQty END)
		,vcName = (CASE WHEN ISNULL(@vcPriceLevel7Name,'') <> '-1' THEN ISNULL(@vcPriceLevel7Name,0) ELSE vcName END)
	WHERE 
		PriceLevelID = 7

	UPDATE 
		@Defualt 
	SET 
		monPrice = (CASE WHEN ISNULL(@monPriceLevel8,0) <> -1 THEN ISNULL(@monPriceLevel8,0) ELSE monPrice END)
		,intFromQty = (CASE WHEN ISNULL(@intPriceLevel8FromQty,0) <> -1 THEN ISNULL(@intPriceLevel8FromQty,0) ELSE intFromQty END)
		,intToQty = (CASE WHEN ISNULL(@intPriceLevel8ToQty,0) <> -1 THEN ISNULL(@intPriceLevel8ToQty,0) ELSE intToQty END)
		,vcName = (CASE WHEN ISNULL(@vcPriceLevel8Name,'') <> '-1' THEN ISNULL(@vcPriceLevel8Name,0) ELSE vcName END)
	WHERE
		 PriceLevelID = 8
	
	UPDATE 
		@Defualt 
	SET 
		monPrice = (CASE WHEN ISNULL(@monPriceLevel9,0) <> -1 THEN ISNULL(@monPriceLevel9,0) ELSE monPrice END)
		,intFromQty = (CASE WHEN ISNULL(@intPriceLevel9FromQty,0) <> -1 THEN ISNULL(@intPriceLevel9FromQty,0) ELSE intFromQty END)
		,intToQty = (CASE WHEN ISNULL(@intPriceLevel9ToQty,0) <> -1 THEN ISNULL(@intPriceLevel9ToQty,0) ELSE intToQty END)
		,vcName = (CASE WHEN ISNULL(@vcPriceLevel9Name,'') <> '-1' THEN ISNULL(@vcPriceLevel9Name,0) ELSE vcName END)
	WHERE 
		PriceLevelID = 9
	
	UPDATE 
		@Defualt 
	SET 
		monPrice = (CASE WHEN ISNULL(@monPriceLevel10,0) <> -1 THEN ISNULL(@monPriceLevel10,0) ELSE monPrice END)
		,intFromQty = (CASE WHEN ISNULL(@intPriceLevel10FromQty,0) <> -1 THEN ISNULL(@intPriceLevel10FromQty,0) ELSE intFromQty END)
		,intToQty = (CASE WHEN ISNULL(@intPriceLevel10ToQty,0) <> -1 THEN ISNULL(@intPriceLevel10ToQty,0) ELSE intToQty END)
		,vcName = (CASE WHEN ISNULL(@vcPriceLevel10Name,'') <> '-1' THEN ISNULL(@vcPriceLevel10Name,0) ELSE vcName END)
	WHERE 
		PriceLevelID = 10
	
	UPDATE 
		@Defualt 
	SET 
		monPrice = (CASE WHEN ISNULL(@monPriceLevel11,0) <> -1 THEN ISNULL(@monPriceLevel11,0) ELSE monPrice END)
		,intFromQty = (CASE WHEN ISNULL(@intPriceLevel11FromQty,0) <> -1 THEN ISNULL(@intPriceLevel11FromQty,0) ELSE intFromQty END)
		,intToQty = (CASE WHEN ISNULL(@intPriceLevel11ToQty,0) <> -1 THEN ISNULL(@intPriceLevel11ToQty,0) ELSE intToQty END)
		,vcName = (CASE WHEN ISNULL(@vcPriceLevel11Name,'') <> '-1' THEN ISNULL(@vcPriceLevel11Name,0) ELSE vcName END)
	WHERE 
		PriceLevelID = 11
	
	UPDATE
		@Defualt 
	SET 
		monPrice = (CASE WHEN ISNULL(@monPriceLevel12,0) <> -1 THEN ISNULL(@monPriceLevel12,0) ELSE monPrice END)
		,intFromQty = (CASE WHEN ISNULL(@intPriceLevel12FromQty,0) <> -1 THEN ISNULL(@intPriceLevel12FromQty,0) ELSE intFromQty END)
		,intToQty = (CASE WHEN ISNULL(@intPriceLevel12ToQty,0) <> -1 THEN ISNULL(@intPriceLevel12ToQty,0) ELSE intToQty END)
		,vcName = (CASE WHEN ISNULL(@vcPriceLevel12Name,'') <> '-1' THEN ISNULL(@vcPriceLevel12Name,0) ELSE vcName END)
	WHERE
		PriceLevelID = 12
	
	UPDATE 
		@Defualt 
	SET 
		monPrice = (CASE WHEN ISNULL(@monPriceLevel13,0) <> -1 THEN ISNULL(@monPriceLevel13,0) ELSE monPrice END)
		,intFromQty = (CASE WHEN ISNULL(@intPriceLevel13FromQty,0) <> -1 THEN ISNULL(@intPriceLevel13FromQty,0) ELSE intFromQty END)
		,intToQty = (CASE WHEN ISNULL(@intPriceLevel13ToQty,0) <> -1 THEN ISNULL(@intPriceLevel13ToQty,0) ELSE intToQty END)
		,vcName = (CASE WHEN ISNULL(@vcPriceLevel13Name,'') <> '-1' THEN ISNULL(@vcPriceLevel13Name,0) ELSE vcName END)
	WHERE 
		PriceLevelID = 13
	
	UPDATE 
		@Defualt 
	SET 
		monPrice = (CASE WHEN ISNULL(@monPriceLevel14,0) <> -1 THEN ISNULL(@monPriceLevel14,0) ELSE monPrice END) 
		,intFromQty = (CASE WHEN ISNULL(@intPriceLevel14FromQty,0) <> -1 THEN ISNULL(@intPriceLevel14FromQty,0) ELSE intFromQty END)
		,intToQty = (CASE WHEN ISNULL(@intPriceLevel14ToQty,0) <> -1 THEN ISNULL(@intPriceLevel14ToQty,0) ELSE intToQty END)
		,vcName = (CASE WHEN ISNULL(@vcPriceLevel14Name,'') <> '-1' THEN ISNULL(@vcPriceLevel14Name,0) ELSE vcName END)
	WHERE 
		PriceLevelID = 14
	
	UPDATE 
		@Defualt 
	SET 
		monPrice = (CASE WHEN ISNULL(@monPriceLevel15,0) <> -1 THEN ISNULL(@monPriceLevel15,0) ELSE monPrice END)
		,intFromQty = (CASE WHEN ISNULL(@intPriceLevel15FromQty,0) <> -1 THEN ISNULL(@intPriceLevel15FromQty,0) ELSE intFromQty END)
		,intToQty = (CASE WHEN ISNULL(@intPriceLevel15ToQty,0) <> -1 THEN ISNULL(@intPriceLevel15ToQty,0) ELSE intToQty END)
		,vcName = (CASE WHEN ISNULL(@vcPriceLevel15Name,'') <> '-1' THEN ISNULL(@vcPriceLevel15Name,0) ELSE vcName END)
	WHERE 
		PriceLevelID = 15
	
	UPDATE 
		@Defualt 
	SET 
		monPrice = (CASE WHEN ISNULL(@monPriceLevel16,0) <> -1 THEN ISNULL(@monPriceLevel16,0) ELSE monPrice END)
		,intFromQty = (CASE WHEN ISNULL(@intPriceLevel16FromQty,0) <> -1 THEN ISNULL(@intPriceLevel16FromQty,0) ELSE intFromQty END)
		,intToQty = (CASE WHEN ISNULL(@intPriceLevel16ToQty,0) <> -1 THEN ISNULL(@intPriceLevel16ToQty,0) ELSE intToQty END)
		,vcName = (CASE WHEN ISNULL(@vcPriceLevel16Name,'') <> '-1' THEN ISNULL(@vcPriceLevel16Name,0) ELSE vcName END)
	WHERE 
		PriceLevelID = 16
	
	UPDATE 
		@Defualt 
	SET 
		monPrice = (CASE WHEN ISNULL(@monPriceLevel17,0) <> -1 THEN ISNULL(@monPriceLevel17,0) ELSE monPrice END)
		,intFromQty = (CASE WHEN ISNULL(@intPriceLevel17FromQty,0) <> -1 THEN ISNULL(@intPriceLevel17FromQty,0) ELSE intFromQty END)
		,intToQty = (CASE WHEN ISNULL(@intPriceLevel17ToQty,0) <> -1 THEN ISNULL(@intPriceLevel17ToQty,0) ELSE intToQty END)
		,vcName = (CASE WHEN ISNULL(@vcPriceLevel17Name,'') <> '-1' THEN ISNULL(@vcPriceLevel17Name,0) ELSE vcName END)
	WHERE 
		PriceLevelID = 17
	
	UPDATE 
		@Defualt 
	SET 
		monPrice = (CASE WHEN ISNULL(@monPriceLevel18,0) <> -1 THEN ISNULL(@monPriceLevel18,0) ELSE monPrice END)
		,intFromQty = (CASE WHEN ISNULL(@intPriceLevel18FromQty,0) <> -1 THEN ISNULL(@intPriceLevel18FromQty,0) ELSE intFromQty END)
		,intToQty = (CASE WHEN ISNULL(@intPriceLevel18ToQty,0) <> -1 THEN ISNULL(@intPriceLevel18ToQty,0) ELSE intToQty END)
		,vcName = (CASE WHEN ISNULL(@vcPriceLevel18Name,'') <> '-1' THEN ISNULL(@vcPriceLevel18Name,0) ELSE vcName END)
	WHERE 
		PriceLevelID = 18
	
	UPDATE 
		@Defualt 
	SET 
		monPrice = (CASE WHEN ISNULL(@monPriceLevel19,0) <> -1 THEN ISNULL(@monPriceLevel19,0) ELSE monPrice END)
		,intFromQty = (CASE WHEN ISNULL(@intPriceLevel19FromQty,0) <> -1 THEN ISNULL(@intPriceLevel19FromQty,0) ELSE intFromQty END)
		,intToQty = (CASE WHEN ISNULL(@intPriceLevel19ToQty,0) <> -1 THEN ISNULL(@intPriceLevel19ToQty,0) ELSE intToQty END)
		,vcName = (CASE WHEN ISNULL(@vcPriceLevel19Name,'') <> '-1' THEN ISNULL(@vcPriceLevel19Name,0) ELSE vcName END)
	WHERE 
		PriceLevelID = 19
	
	UPDATE 
		@Defualt 
	SET 
		monPrice = (CASE WHEN ISNULL(@monPriceLevel20,0) <> -1 THEN ISNULL(@monPriceLevel20,0) ELSE monPrice END)
		,intFromQty = (CASE WHEN ISNULL(@intPriceLevel20FromQty,0) <> -1 THEN ISNULL(@intPriceLevel20FromQty,0) ELSE intFromQty END)
		,intToQty = (CASE WHEN ISNULL(@intPriceLevel20ToQty,0) <> -1 THEN ISNULL(@intPriceLevel20ToQty,0) ELSE intToQty END)
		,vcName = (CASE WHEN ISNULL(@vcPriceLevel20Name,'') <> '-1' THEN ISNULL(@vcPriceLevel20Name,0) ELSE vcName END)
	WHERE 
		PriceLevelID = 20
	
	DECLARE @MaxPriceLevelIDWithSomeValue AS INT
	
	SELECT @MaxPriceLevelIDWithSomeValue=MAX(PriceLevelID) FROM @Defualt WHERE (monPrice <> -1 OR intFromQty <> -1 OR intToQty <> -1)

	-- NOW REPLACE PRICE -1 TO 0 IN ALL PRICE LEVELS WHICH ARE LESS THAN @MaxPriceLevelIDWithSomeValue 
	-- (THIS REQUIRED BECAUSE IF USER HAS PROVIDED PRICE LEVEL 1 AND PRICE LEVEL 6 COLUMN IN IMPORT FILE THEN WE HAVE TO ADD 0 FROM PRICE LEVEL 2,3,4,5)
	UPDATE
		@Defualt
	SET
		monPrice = (CASE WHEN monPrice = -1 THEN 0 ELSE monPrice END)
		,intFromQty = (CASE WHEN intFromQty = -1 THEN 0 ELSE intFromQty END)
		,intToQty = (CASE WHEN intToQty = -1 THEN 0 ELSE intToQty END)
		,vcName = (CASE WHEN vcName = '-1' THEN '' ELSE vcName END)
	WHERE
		PriceLevelID <= @MaxPriceLevelIDWithSomeValue
		AND (monPrice = -1 OR intFromQty=-1 OR intToQty=-1)

	IF EXISTS(SELECT * FROM @Defualt WHERE PriceLevelID <= @MaxPriceLevelIDWithSomeValue AND tintRuleType IN (1,2) AND (ISNULL(intFromQty,0) = 0 OR ISNULL(intToQty,0) = 0))
	BEGIN
		RAISERROR('QTY_FROM_AND_TO_MUST_BE_GREATER_THEN_0',16,1)
		RETURN
	END

	IF EXISTS(SELECT * FROM @Defualt WHERE PriceLevelID <= @MaxPriceLevelIDWithSomeValue AND tintRuleType IN (1,2) AND ISNULL(tintDiscountType,0) = 0)
	BEGIN
		RAISERROR('INVALID_PRICE_LEVEL_DISCOUNT_TYPE',16,1)
		RETURN
	END

	BEGIN TRY
	BEGIN TRANSACTION
		DELETE FROM PricingTable WHERE numItemCode=@numItemCode AND ISNULL(numCurrencyID,0) = 0

		INSERT INTO PricingTable
		(
			numPriceRuleID,numItemCode,vcName,decDiscount,tintRuleType,tintDiscountType,intFromQty,intToQty
		)
		SELECT
			0,@numItemCode,vcName,monPrice,tintRuleType,(CASE WHEN tintRuleType = 3 THEN 0 ELSE tintDiscountType END),(CASE WHEN tintRuleType = 3 THEN 0 ELSE intFromQty END),(CASE WHEN tintRuleType = 3 THEN 0 ELSE intToQty END)
		FROM
			@Defualt
		WHERE
			PriceLevelID <= @MaxPriceLevelIDWithSomeValue

	COMMIT
	END TRY
	BEGIN CATCH
		DECLARE @ErrorMessage NVARCHAR(4000)
		DECLARE @ErrorNumber INT
		DECLARE @ErrorSeverity INT
		DECLARE @ErrorState INT
		DECLARE @ErrorLine INT
		DECLARE @ErrorProcedure NVARCHAR(200)

		IF @@TRANCOUNT > 0
			ROLLBACK TRANSACTION;

		SELECT 
			@ErrorMessage = ERROR_MESSAGE(),
			@ErrorNumber = ERROR_NUMBER(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE(),
			@ErrorLine = ERROR_LINE(),
			@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

		RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);
	END CATCH

END