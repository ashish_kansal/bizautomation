/****** Object:  StoredProcedure [dbo].[USP_ManageCreditBalanceHistory]    Script Date: 01/22/2009 01:41:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ManageCreditBalanceHistory')
DROP PROCEDURE USP_ManageCreditBalanceHistory
GO
CREATE PROCEDURE [dbo].[USP_ManageCreditBalanceHistory]
@numDomainId	numeric(18, 0),
@numCreatedBy	numeric(18, 0),
@numDivisionID	numeric(18, 0)=NULL,
@numReturnID NUMERIC(9),
@monAmount	DECIMAL(20,5),
@vcReference	nvarchar(200)=null,
@vcMemo	nvarchar(50)=null,
@tintPaymentAction as tinyint,
@tintOppType as TINYINT 
AS 
BEGIN

IF ISNULL(@numDivisionID,0)=0 AND ISNULL(@numReturnID,0)>0
BEGIN
	SELECT @numDivisionID = numDivisionID FROM OpportunityMaster OM join [Returns] R on OM.numOppId=R.numOppId where R.numReturnID=@numReturnID
END

IF @numReturnID = 0 SET @numReturnID = NULL

	INSERT INTO [CreditBalanceHistory]
           ([numReturnID]
           ,[monAmount]
           ,[vcReference]
           ,[vcMemo]
           ,[dtCreateDate]
           ,[dtCreatedBy]
           ,[numDomainId],numDivisionID)
     VALUES
           (@numReturnID
           ,Case When @tintPaymentAction=1 then @monAmount else @monAmount * -1 end
           ,@vcReference
           ,@vcMemo
           ,GETDATE()
           ,@numCreatedBy
           ,@numDomainId,@numDivisionID)

    IF @tintOppType=1 --Sales
		update DivisionMaster set monSCreditBalance=isnull(monSCreditBalance,0) + Case When @tintPaymentAction=1 then @monAmount else @monAmount * -1 end where numDivisionID=@numDivisionID and numDomainID=@numDomainID
	ELSE IF @tintOppType=2 --Purchase
		update DivisionMaster set monPCreditBalance=isnull(monPCreditBalance,0) + Case When @tintPaymentAction=1 then @monAmount else @monAmount * -1 end where numDivisionID=@numDivisionID and numDomainID=@numDomainID

END