SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetItemFromUPCSKU')
DROP PROCEDURE USP_GetItemFromUPCSKU
GO
CREATE PROCEDURE [dbo].[USP_GetItemFromUPCSKU]                              
@numDomainID as numeric(9)=0,
@vcUPCSKU as varchar(50)=''    
as                
     
declare @sql as nvarchar(max)
 
set @sql ='select I.numItemCode,I.vcItemName,0 as numWareHouseID,0 as numWareHouseItemID from Item I 
where I.numDomainID=@numDomainID and isnull(numItemGroup,0)=0 
and ((I.numBarCodeId is not null and len(I.numBarCodeId)>1 and I.numBarCodeId=@vcUPCSKU)
	OR (I.vcSKU is not null and len(I.vcSKU)>0 and I.vcSKU=@vcUPCSKU))
 
Union

select I.numItemCode,I.vcItemName,W.numWareHouseID,WI.numWareHouseItemID from Item I 
	join WareHouseItems WI on WI.numDomainID=@numDomainID and I.numItemCode=WI.numItemID  
	join Warehouses W on W.numDomainID=@numDomainID and W.numWareHouseID=WI.numWareHouseID  
	where I.numDomainID=@numDomainID and isnull(numItemGroup,0)>0 
			and ((WI.vcBarCode is not null and len(WI.vcBarCode)>0 and WI.vcBarCode=@vcUPCSKU)
				OR (WI.vcWHSKU is not null and len(WI.vcWHSKU)>0 and WI.vcWHSKU =@vcUPCSKU))

UNION

select I.numItemCode,I.vcItemName,0 as numWareHouseID,0 as numWareHouseItemID
from Vendor join Item I on I.numItemCode= Vendor.numItemCode
WHERE I.numDomainID=@numDomainID and Vendor.numDomainID=@numDomainID 
and Vendor.vcPartNo is not null and len(Vendor.vcPartNo)>0 and Vendor.vcPartNo=@vcUPCSKU

UNION

select I.numItemCode,I.vcItemName,W.numWareHouseID,WI.numWareHouseItemID
from Item I 
join WareHouseItems WI on WI.numDomainID=@numDomainID and I.numItemCode=WI.numItemID  
join Warehouses W on W.numDomainID=@numDomainID and W.numWareHouseID=WI.numWareHouseID  
join WareHouseItmsDTL WDTL on WDTL.numWareHouseItemID=WI.numWareHouseItemID    
where I.numDomainID=@numDomainID and (I.bitSerialized=1 or I.bitLotNo=1) 
and WDTL.vcSerialNo is not null and len(WDTL.vcSerialNo)>0 and WDTL.vcSerialNo=@vcUPCSKU
and ISNULL(WDTL.numQty,0) > 0 '


print @sql
EXECUTE sp_executeSQL @sql, N'@numDomainID numeric(9),@vcUPCSKU varchar(50)',@numDomainID, @vcUPCSKU

GO
