/****** Object:  StoredProcedure [dbo].[USP_DeleteOpportunitySalesTemplate]    Script Date: 09/01/2009 01:16:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ManageSalesSpecificGenericTemplateItems')
DROP PROCEDURE USP_ManageSalesSpecificGenericTemplateItems
GO
CREATE PROCEDURE [dbo].[USP_ManageSalesSpecificGenericTemplateItems]
	@numSalesTemplateItemID NUMERIC(18),
	@numSalesTemplateID NUMERIC(9),
	@numDomainID NUMERIC(9),
	@numItemCode NUMERIC(18),
	@numWarehouseItmsID NUMERIC(18) NULL,
	@numUOM NUMERIC(18)  NULL,
	@vcUOMName VARCHAR (50) NULL,
	@Attributes VARCHAR (1000) NULL,
	@AttrValues VARCHAR (1000) NULL
AS

		DECLARE @monListPrice DECIMAL(20,5), @numWareHouseID NUMERIC(18), @ItemType VARCHAR(100), @numVendorId NUMERIC(18) 
		--, @numUOM NUMERIC(18), @vcUOMName VARCHAR(100),@numWarehouseItmsID NUMERIC(18)
			
		--SELECT @monListPrice = monListPrice FROM Item WHERE numitemcode = @numitemcode

		/*SELECT @monListPrice = (CASE WHEN charItemType='P' THEN ISNULL(W.monWListPrice,0) ELSE ISNULL(monListPrice,0) END),
			@numWareHouseID = ISNULL(W.numWareHouseID,0),
			@numWarehouseItmsID = ISNULL(W.numWareHouseItemID,0),
			@ItemType = (CASE WHEN charItemType='P' THEN 'Inventory Item' 
			WHEN charItemType='N' THEN 'Non-Inventory Item' 
			WHEN charItemType='S' THEN 'Service Item' 
			ELSE '' END),
			@numUOM = numSaleUnit,
			@vcUOMName = dbo.fn_GetUOMName(ISNULL(I.numBaseUnit,0)), 
			@numVendorId = I.numVendorID
		FROM Item I       
			LEFT JOIN WareHouseItems W  ON  W.numItemID = I.numItemCode AND W.numWareHouseItemID = @numWarehouseItmsID           
			LEFT JOIN ItemExtendedDetails IED ON I.numItemCode = IED.numItemCode
			LEFT JOIN Vendor ON I.numVendorID = Vendor.numVendorID AND Vendor.numItemCode = I.numItemCode            
		WHERE I.numItemCode=@numItemCode 
		
		IF @monListPrice = 0
			SELECT @monListPrice = monListPrice FROM Item WHERE numitemcode = @numitemcode
		*/

		IF EXISTS(SELECT * FROM PricingTable WHERE numitemcode = @numitemcode)
		BEGIN
			SELECT TOP 1 @monListPrice = decDiscount FROM PricingTable WHERE numitemcode = @numitemcode
		END
		ELSE
		BEGIN
			SELECT @monListPrice = monListPrice FROM Item WHERE numitemcode = @numitemcode
		END
	IF @numSalesTemplateItemID > 0
	BEGIN
		UPDATE SalesTemplateItems
		SET numSalesTemplateID = @numSalesTemplateID,
			numDomainID = @numDomainID,
			numItemCode =@numItemCode,
			numWarehouseItmsID = @numWarehouseItmsID,
			numUOM = @numUOM,
			vcUOMName = @vcUOMName,
			Attributes = @Attributes,
			AttrValues = @AttrValues
		WHERE numSalesTemplateItemID = @numSalesTemplateItemID
	END
	ELSE
	BEGIN		

		INSERT INTO SalesTemplateItems
		(
			numSalesTemplateID,
			numDomainID,
			numItemCode,
			numWarehouseItmsID,
			numUOM,
			vcUOMName,
			Attributes,
			AttrValues,
			numUnitHour,
			monPrice,
			numWarehouseID,
			ItemType,
			monTotAmount,
			monTotAmtBefDiscount,
			numSOVendorId   
		)
		VALUES
		(
			@numSalesTemplateID,
			@numDomainID,
			@numItemCode,
			@numWarehouseItmsID,
			@numUOM,
			@vcUOMName,
			@Attributes,
			@AttrValues,
			1,
			@monListPrice,
			@numWareHouseID,
			@ItemType,
			@monListPrice,
			@monListPrice,
			@numVendorId
		)
	END