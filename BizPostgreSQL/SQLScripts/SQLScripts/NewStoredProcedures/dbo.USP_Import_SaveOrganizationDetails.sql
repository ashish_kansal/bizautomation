SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_Import_SaveOrganizationDetails')
DROP PROCEDURE USP_Import_SaveOrganizationDetails
GO
CREATE PROCEDURE [dbo].[USP_Import_SaveOrganizationDetails]  
	@numDomainID NUMERIC(18,0)
	,@numUserCntID NUMERIC(18,0)
	,@numDivisionID NUMERIC(18,0)
	,@vcCompanyName VARCHAR(100)
	,@vcProfile NUMERIC(18,0)
	,@numCompanyType NUMERIC(18,0)
	,@numNoOfEmployeesId NUMERIC(18,0)
	,@numAnnualRevID NUMERIC(18,0)
	,@vcWebSite VARCHAR(255)
	,@vcHow NUMERIC(18,0)
	,@numCompanyIndustry NUMERIC(18,0)
	,@numCompanyRating NUMERIC(18,0)
	,@numCompanyCredit NUMERIC(18,0)
	,@txtComments NVARCHAR(MAX)
	,@vcWebLink1 VARCHAR(255)
	,@vcWebLink2 VARCHAR(255)
	,@vcWebLink3 VARCHAR(255)
	,@vcWebLink4 VARCHAR(255)
	,@numCompanyDiff NUMERIC(18,0)
	,@vcCompanyDiff VARCHAR(100)
	,@numCampaignID NUMERIC(18,0)
	,@vcComPhone VARCHAR(50)
	,@numAssignedTo NUMERIC(18,0)
	,@numFollowUpStatus NUMERIC(18,0)
	,@numTerID NUMERIC(18,0)
	,@numStatusID NUMERIC(18,0)
	,@vcComFax VARCHAR(50)
	,@bitPublicFlag BIT
	,@bitActiveInActive BIT
	,@numGrpID NUMERIC(18,0)
	,@tintCRMType TINYINT
	,@numCurrencyID NUMERIC(18,0)
	,@bitNoTax BIT
	,@numBillingDays NUMERIC(18,0)
	,@bitOnCreditHold BIT
	,@numDefaultExpenseAccountID NUMERIC(18,0)
AS  
BEGIN
BEGIN TRY
	-- IMPORTANT: PLEASE MAKE SURE NEW FIELD WHICH YOUR ARE ADDING IN STORE PROCEDURE FOR UPDATE ARE ADDED IN SELECT IN PROCEDURE USP_Import_GetOrganizationDetails

	IF ISNULL(@numDivisionID,0) > 0 AND EXISTS (SELECT numDivisionID FROM DivisionMaster WHERE numDomainID=@numDomainID AND numDivisionID=@numDivisionID)
	BEGIN
		BEGIN TRANSACTION
			DECLARE @numCompanyID NUMERIC(18,0)

			SELECT @numCompanyID = numCompanyID FROM DivisionMaster WHERE numDomainID=@numDomainID AND numDivisionID=@numDivisionID

			IF ISNULL(@numCompanyID,0) > 0
			BEGIN
				UPDATE
					CompanyInfo
				SET
					vcCompanyName=@vcCompanyName
					,vcProfile=@vcProfile
					,numCompanyType=@numCompanyType
					,numNoOfEmployeesId=@numNoOfEmployeesId
					,numAnnualRevID=@numAnnualRevID
					,vcWebSite=@vcWebSite
					,vcHow=@vcHow
					,numCompanyIndustry=@numCompanyIndustry
					,numCompanyRating=@numCompanyRating
					,numCompanyCredit=@numCompanyCredit
					,txtComments=@txtComments
					,vcWebLink1=@vcWebLink1
					,vcWebLink2=@vcWebLink2
					,vcWebLink3=@vcWebLink3
					,vcWebLink4=@vcWebLink4
					,numModifiedBy=@numUserCntID              
					,bintModifiedDate=GETUTCDATE()
				WHERE
					numDomainID=@numDomainID
					AND numCompanyId=@numCompanyID
			END

			DECLARE @numFollow NUMERIC(18,0)                     

			SELECT 
				@numFollow=ISNULL(numFollowUpStatus,0)
			FROM 
				DivisionMaster 
			WHERE 
				numDivisionID=@numDivisionID 
				AND numDomainId= @numDomainID                    
			
			IF @numFollow <> @numFollowUpStatus                          
			BEGIN                          
				SELECT TOP 1  numFollowUpStatus from FollowUpHistory where numDivisionID= @numDivisionID AND numDomainId= @numDomainID ORDER BY numFollowUpStatusID DESC                          
			                    
				INSERT INTO FollowUpHistory
				(numFollowUpstatus,numDivisionID,bintAddedDate,numDomainId)                          
				VALUES
				(@numFollow,@numDivisionID,GETUTCDATE(),@numDomainID)                                  
			END                          
			
			---Updating if organization is assigned to someone            
			DECLARE @tempAssignedTo NUMERIC(18,0)          
			SELECT @tempAssignedTo=ISNULL(numAssignedTo,0) FROM DivisionMaster WHERE numDivisionID = @numDivisionID AND numDomainId= @numDomainID           
			 
			IF @tempAssignedTo <> @numAssignedTo AND  @numAssignedTo<>'0'        
			BEGIN            
				UPDATE DivisionMaster SET numAssignedTo=@numAssignedTo,numAssignedBy=@numUserCntID WHERE numDivisionID = @numDivisionID AND numDomainId= @numDomainID           
			END           
			ELSE IF ISNULL(@numAssignedTo,0) = 0
			BEGIN          
				UPDATE DivisionMaster SET numAssignedTo=0,numAssignedBy=0 WHERE numDivisionID = @numDivisionID AND numDomainId= @numDomainID        
			END  
			                                      
			UPDATE 
				DivisionMaster 
			SET                                                                               
				numModifiedBy = @numUserCntID,                                            
				bintModifiedDate = GETUTCDATE()                                        
				,numCompanyDiff=@numCompanyDiff
				,vcCompanyDiff=@vcCompanyDiff
				,numCampaignID=@numCampaignID
				,vcComPhone=@vcComPhone
				,numFollowUpStatus=@numFollowUpStatus
				,numTerID=@numTerID
				,numStatusID=@numStatusID
				,vcComFax=@vcComFax
				,bitPublicFlag=@bitPublicFlag
				,bitActiveInActive=@bitActiveInActive
				,numGrpID=@numGrpID
				,tintCRMType=@tintCRMType
				,numCurrencyID=@numCurrencyID
				,bitNoTax=@bitNoTax
				,numBillingDays=@numBillingDays
				,bitOnCreditHold=@bitOnCreditHold
				,numDefaultExpenseAccountID=@numDefaultExpenseAccountID
			WHERE 
				numDivisionID = @numDivisionID 
				AND numDomainId= @numDomainID                                       
		COMMIT
	END
END TRY
BEGIN CATCH
	DECLARE @ErrorMessage NVARCHAR(4000)
	DECLARE @ErrorNumber INT
	DECLARE @ErrorSeverity INT
	DECLARE @ErrorState INT
	DECLARE @ErrorLine INT
	DECLARE @ErrorProcedure NVARCHAR(200)

	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;

	SELECT 
		@ErrorMessage = ERROR_MESSAGE(),
		@ErrorNumber = ERROR_NUMBER(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE(),
		@ErrorLine = ERROR_LINE(),
		@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

	RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);
END CATCH

END