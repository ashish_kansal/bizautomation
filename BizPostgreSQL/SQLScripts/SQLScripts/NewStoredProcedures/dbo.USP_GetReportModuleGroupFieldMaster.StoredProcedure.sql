GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetReportModuleGroupFieldMaster')
DROP PROCEDURE USP_GetReportModuleGroupFieldMaster
GO
CREATE PROCEDURE [dbo].[USP_GetReportModuleGroupFieldMaster]     
    @numDomainID numeric(18, 0),
	@numReportID numeric(18, 0),
	@bitIsReportRun BIT
as                 

--Get numReportModuleGroupID
DECLARE @numReportModuleGroupID AS NUMERIC(18,0);SET @numReportModuleGroupID=0
SELECT @numReportModuleGroupID=numReportModuleGroupID FROM dbo.ReportListMaster WHERE numDomainID=@numDomainID AND numReportID=@numReportID


IF @numReportModuleGroupID>0
BEGIN
CREATE TABLE #tempField(numReportFieldGroupID NUMERIC,vcFieldGroupName VARCHAR(100),numFieldID NUMERIC,
vcFieldName NVARCHAR(50),vcDbColumnName NVARCHAR(50),vcOrigDbColumnName NVARCHAR(50),vcFieldDataType CHAR(1),
vcAssociatedControlType NVARCHAR(50),vcListItemType VARCHAR(3),numListID NUMERIC,bitCustom BIT,
bitAllowSorting BIT,bitAllowGrouping BIT,bitAllowAggregate BIT,bitAllowFiltering BIT,vcLookBackTableName NVARCHAR(50))

--Regular Fields
INSERT INTO #tempField
	SELECT RFGM.numReportFieldGroupID,RFGM.vcFieldGroupName,RGMM.numFieldID,RGMM.vcFieldName,
		   DFM.vcDbColumnName,DFM.vcOrigDbColumnName,RGMM.vcFieldDataType,RGMM.vcAssociatedControlType,
		   ISNULL(DFM.vcListItemType,''),ISNULL(DFM.numListID,0),CAST(0 AS BIT) AS bitCustom,RGMM.bitAllowSorting,RGMM.bitAllowGrouping,
		   RGMM.bitAllowAggregate,RGMM.bitAllowFiltering,DFM.vcLookBackTableName
		FROM ReportFieldGroupMaster RFGM 
			JOIN ReportModuleGroupFieldMappingMaster RFMM ON RFGM.numReportFieldGroupID=RFMM.numReportFieldGroupID
			JOIN ReportFieldGroupMappingMaster RGMM ON RGMM.numReportFieldGroupID=RFGM.numReportFieldGroupID
			JOIN dbo.DycFieldMaster DFM ON DFM.numFieldId=RGMM.numFieldID
		WHERE RFMM.numReportModuleGroupID=@numReportModuleGroupID AND RFGM.bitActive=1
			AND ISNULL(RFGM.bitCustomFieldGroup,0)=0

IF @numReportModuleGroupID=13
BEGIN
INSERT INTO #tempField
SELECT 3 AS numReportFieldGroupID,'Milestone Stages Field' AS vcFieldGroupName,0 AS numStageDetailsId,'Last Milestone Completed' as vcStageName,
		   'Last Milestone Completed' AS vcDbColumnName,'OppMileStoneCompleted' AS vcOrigDbColumnName,'M' AS vcFieldDataType,'TextBox' AS vcAssociatedControlType,
		   '' AS vcListItemType,0 AS numListID,CAST(0 AS BIT) AS bitCustom,0 AS bitAllowSorting,0 AS bitAllowGrouping,
		   1 AS bitAllowAggregate,1 AS bitAllowFiltering,'StagePercentageDetails' AS vcLookBackTableName

UNION
SELECT 3 AS numReportFieldGroupID,'Milestone Stages Field' AS vcFieldGroupName,1 AS numStageDetailsId,'Last Stage Completed' AS vcStageName,
		   'Last Stage Completed' AS vcDbColumnName,'OppStageCompleted' AS vcOrigDbColumnName,'M' AS vcFieldDataType,'TextBox' AS vcAssociatedControlType,
		   '' AS vcListItemType,0 AS numListID,CAST(0 AS BIT) AS bitCustom,0 AS bitAllowSorting,0 AS bitAllowGrouping,
		   1 AS bitAllowAggregate,1 AS bitAllowFiltering,'StagePercentageDetails' AS vcLookBackTableName
UNION
SELECT 3 AS numReportFieldGroupID,'Milestone Stages Field' AS vcFieldGroupName,2 AS numStageDetailsId,'Total Progress' AS vcStageName,
		   'Total Progress' AS vcDbColumnName,'intTotalProgress' AS vcOrigDbColumnName,'M' AS vcFieldDataType,'TextBox' AS vcAssociatedControlType,
		   '' AS vcListItemType,0 AS numListID,CAST(0 AS BIT) AS bitCustom,0 AS bitAllowSorting,0 AS bitAllowGrouping,
		   1 AS bitAllowAggregate,1 AS bitAllowFiltering,'ProjectProgress' AS vcLookBackTableName
END

IF @numReportModuleGroupID=9
BEGIN
INSERT INTO #tempField
SELECT 25 AS numReportFieldGroupID,'BizDocs Items Tax Field' AS vcFieldGroupName,TI.numTaxItemID,TI.vcTaxName,
		   vcTaxName AS vcDbColumnName,vcTaxName AS vcOrigDbColumnName,'M' AS vcFieldDataType,'TextBox' AS vcAssociatedControlType,
		   '' AS vcListItemType,0 AS numListID,CAST(0 AS BIT) AS bitCustom,0 AS bitAllowSorting,0 AS bitAllowGrouping,
		   1 AS bitAllowAggregate,1 AS bitAllowFiltering,'TaxItems' AS vcLookBackTableName
from TaxItems TI 
where TI.numDomainID =@numDomainID
UNION
SELECT 25 AS numReportFieldGroupID,'BizDocs Items Tax Field' AS vcFieldGroupName,0 AS numTaxItemID,'Sales Tax' AS vcTaxName,
		   'Sales Tax' AS vcDbColumnName,'Sales Tax' AS vcOrigDbColumnName,'M' AS vcFieldDataType,'TextBox' AS vcAssociatedControlType,
		   '' AS vcListItemType,0 AS numListID,CAST(0 AS BIT) AS bitCustom,0 AS bitAllowSorting,0 AS bitAllowGrouping,
		   1 AS bitAllowAggregate,1 AS bitAllowFiltering,'TaxItems' AS vcLookBackTableName
END

IF @numReportModuleGroupID=8 OR @numReportModuleGroupID=9
BEGIN
INSERT INTO #tempField
SELECT 26 AS numReportFieldGroupID,'BizDocs Tax Field' AS vcFieldGroupName,TI.numTaxItemID,'Total ' + TI.vcTaxName,
		   'Total ' + vcTaxName AS vcDbColumnName,'Total ' +vcTaxName AS vcOrigDbColumnName,'M' AS vcFieldDataType,'TextBox' AS vcAssociatedControlType,
		   '' AS vcListItemType,0 AS numListID,CAST(0 AS BIT) AS bitCustom,0 AS bitAllowSorting,0 AS bitAllowGrouping,
		   1 AS bitAllowAggregate,1 AS bitAllowFiltering,'TaxItems' AS vcLookBackTableName
from TaxItems TI 
where TI.numDomainID =@numDomainID
UNION
SELECT 26 AS numReportFieldGroupID,'BizDocs Tax Field' AS vcFieldGroupName,0 AS numTaxItemID,'Total Sales Tax' AS vcTaxName,
		   'Total Sales Tax' AS vcDbColumnName,'Total Sales Tax' AS vcOrigDbColumnName,'M' AS vcFieldDataType,'TextBox' AS vcAssociatedControlType,
		   '' AS vcListItemType,0 AS numListID,CAST(0 AS BIT) AS bitCustom,0 AS bitAllowSorting,0 AS bitAllowGrouping,
		   1 AS bitAllowAggregate,1 AS bitAllowFiltering,'TaxItems' AS vcLookBackTableName
END

--Custom Fields			
INSERT INTO #tempField
SELECT RFGM.numReportFieldGroupID,RFGM.vcFieldGroupName,ISNULL(CFM.Fld_Id,0) as numFieldID,CFM.Fld_label as vcFieldName,
	   '' as vcDbColumnName,'' AS vcOrigDbColumnName,CASE CFM.Fld_type WHEN 'SelectBox' THEN 'V' WHEN 'DateField' THEN 'D' ELSE 'V' END AS vcFieldDataType,
	   CFM.Fld_type AS vcAssociatedControlType,CASE WHEN CFM.Fld_type='SelectBox' OR CFM.Fld_type='CheckBoxList' THEN 'LI' ELSE '' END AS vcListItemType,isnull(CFM.numListID,0) as numListID,
	   CAST(1 AS BIT) AS bitCustom,1 AS bitAllowSorting,CASE WHEN CFM.Fld_type='SelectBox' THEN 1 ELSE 0 END AS bitAllowGrouping,
	   0 AS bitAllowAggregate,1 AS bitAllowFiltering,RFGM.vcCustomTableName AS vcLookBackTableName
FROM ReportFieldGroupMaster RFGM 
	 JOIN ReportModuleGroupFieldMappingMaster RFMM ON RFGM.numReportFieldGroupID=RFMM.numReportFieldGroupID
	 JOIN CFW_Fld_Master CFM ON CFM.Grp_id=RFGM.numGroupID 
	 LEFT JOIN CFw_Grp_Master CGM ON CFM.subgrp=CGM.Grp_id 
	 --JOIN CFW_Loc_Master L on CFM.GRP_ID=L.Loc_Id
	 WHERE RFMM.numReportModuleGroupID=@numReportModuleGroupID AND RFGM.bitActive=1
			AND ISNULL(RFGM.bitCustomFieldGroup,0)=1 AND ISNULL(RFGM.numGroupID,0)>0
			AND CFM.numDomainID=@numDomainID  


	IF ISNULL(@bitIsReportRun,0) = 1 AND (SELECT COUNT(*) FROM #tempField WHERE vcOrigDbColumnName IN ('monPriceLevel1','monPriceLevel2','monPriceLevel3','monPriceLevel4','monPriceLevel5','monPriceLevel6','monPriceLevel7','monPriceLevel8','monPriceLevel9','monPriceLevel10','monPriceLevel11','monPriceLevel12','monPriceLevel13','monPriceLevel14','monPriceLevel15','monPriceLevel16','monPriceLevel17','monPriceLevel18','monPriceLevel19','monPriceLevel20')) > 0
	BEGIN
		UPDATE
			#tempField
		SET
			vcFieldName = (CASE WHEN LEN(ISNULL((SELECT vcPriceLevelName FROm PricingNamesTable WHERE numDomainID=@numDomainID AND tintPriceLevel=1),'')) > 0 THEN ISNULL((SELECT vcPriceLevelName FROm PricingNamesTable WHERE numDomainID=@numDomainID AND tintPriceLevel=1),'') ELSE 'Price Level 1' END)
		WHERE
			vcOrigDbColumnName='monPriceLevel1'

		UPDATE
			#tempField
		SET
			vcFieldName = (CASE WHEN LEN(ISNULL((SELECT vcPriceLevelName FROm PricingNamesTable WHERE numDomainID=@numDomainID AND tintPriceLevel=2),'')) > 0 THEN ISNULL((SELECT vcPriceLevelName FROm PricingNamesTable WHERE numDomainID=@numDomainID AND tintPriceLevel=2),'') ELSE 'Price Level 2' END)
		WHERE
			vcOrigDbColumnName='monPriceLevel2'

		UPDATE
			#tempField
		SET
			vcFieldName = (CASE WHEN LEN(ISNULL((SELECT vcPriceLevelName FROm PricingNamesTable WHERE numDomainID=@numDomainID AND tintPriceLevel=3),'')) > 0 THEN ISNULL((SELECT vcPriceLevelName FROm PricingNamesTable WHERE numDomainID=@numDomainID AND tintPriceLevel=3),'') ELSE 'Price Level 3' END)
		WHERE
			vcOrigDbColumnName='monPriceLevel3'

		UPDATE
			#tempField
		SET
			vcFieldName = (CASE WHEN LEN(ISNULL((SELECT vcPriceLevelName FROm PricingNamesTable WHERE numDomainID=@numDomainID AND tintPriceLevel=4),'')) > 0 THEN ISNULL((SELECT vcPriceLevelName FROm PricingNamesTable WHERE numDomainID=@numDomainID AND tintPriceLevel=4),'') ELSE 'Price Level 4' END)
		WHERE
			vcOrigDbColumnName='monPriceLevel4'

		UPDATE
			#tempField
		SET
			vcFieldName = (CASE WHEN LEN(ISNULL((SELECT vcPriceLevelName FROm PricingNamesTable WHERE numDomainID=@numDomainID AND tintPriceLevel=5),'')) > 0 THEN ISNULL((SELECT vcPriceLevelName FROm PricingNamesTable WHERE numDomainID=@numDomainID AND tintPriceLevel=5),'') ELSE 'Price Level 5' END)
		WHERE
			vcOrigDbColumnName='monPriceLevel5'

		UPDATE
			#tempField
		SET
			vcFieldName = (CASE WHEN LEN(ISNULL((SELECT vcPriceLevelName FROm PricingNamesTable WHERE numDomainID=@numDomainID AND tintPriceLevel=6),'')) > 0 THEN ISNULL((SELECT vcPriceLevelName FROm PricingNamesTable WHERE numDomainID=@numDomainID AND tintPriceLevel=6),'') ELSE 'Price Level 6' END)
		WHERE
			vcOrigDbColumnName='monPriceLevel6'

		UPDATE
			#tempField
		SET
			vcFieldName = (CASE WHEN LEN(ISNULL((SELECT vcPriceLevelName FROm PricingNamesTable WHERE numDomainID=@numDomainID AND tintPriceLevel=7),'')) > 0 THEN ISNULL((SELECT vcPriceLevelName FROm PricingNamesTable WHERE numDomainID=@numDomainID AND tintPriceLevel=7),'') ELSE 'Price Level 7' END)
		WHERE
			vcOrigDbColumnName='monPriceLevel7'

		UPDATE
			#tempField
		SET
			vcFieldName = (CASE WHEN LEN(ISNULL((SELECT vcPriceLevelName FROm PricingNamesTable WHERE numDomainID=@numDomainID AND tintPriceLevel=8),'')) > 0 THEN ISNULL((SELECT vcPriceLevelName FROm PricingNamesTable WHERE numDomainID=@numDomainID AND tintPriceLevel=8),'') ELSE 'Price Level 8' END)
		WHERE
			vcOrigDbColumnName='monPriceLevel8'

		UPDATE
			#tempField
		SET
			vcFieldName = (CASE WHEN LEN(ISNULL((SELECT vcPriceLevelName FROm PricingNamesTable WHERE numDomainID=@numDomainID AND tintPriceLevel=9),'')) > 0 THEN ISNULL((SELECT vcPriceLevelName FROm PricingNamesTable WHERE numDomainID=@numDomainID AND tintPriceLevel=9),'') ELSE 'Price Level 9' END)
		WHERE
			vcOrigDbColumnName='monPriceLevel9'

		UPDATE
			#tempField
		SET
			vcFieldName = (CASE WHEN LEN(ISNULL((SELECT vcPriceLevelName FROm PricingNamesTable WHERE numDomainID=@numDomainID AND tintPriceLevel=10),'')) > 0 THEN ISNULL((SELECT vcPriceLevelName FROm PricingNamesTable WHERE numDomainID=@numDomainID AND tintPriceLevel=10),'') ELSE 'Price Level 10' END)
		WHERE
			vcOrigDbColumnName='monPriceLevel10'

		UPDATE
			#tempField
		SET
			vcFieldName = (CASE WHEN LEN(ISNULL((SELECT vcPriceLevelName FROm PricingNamesTable WHERE numDomainID=@numDomainID AND tintPriceLevel=11),'')) > 0 THEN ISNULL((SELECT vcPriceLevelName FROm PricingNamesTable WHERE numDomainID=@numDomainID AND tintPriceLevel=11),'') ELSE 'Price Level 11' END)
		WHERE
			vcOrigDbColumnName='monPriceLevel11'

		UPDATE
			#tempField
		SET
			vcFieldName = (CASE WHEN LEN(ISNULL((SELECT vcPriceLevelName FROm PricingNamesTable WHERE numDomainID=@numDomainID AND tintPriceLevel=12),'')) > 0 THEN ISNULL((SELECT vcPriceLevelName FROm PricingNamesTable WHERE numDomainID=@numDomainID AND tintPriceLevel=12),'') ELSE 'Price Level 12' END)
		WHERE
			vcOrigDbColumnName='monPriceLevel12'

		UPDATE
			#tempField
		SET
			vcFieldName = (CASE WHEN LEN(ISNULL((SELECT vcPriceLevelName FROm PricingNamesTable WHERE numDomainID=@numDomainID AND tintPriceLevel=13),'')) > 0 THEN ISNULL((SELECT vcPriceLevelName FROm PricingNamesTable WHERE numDomainID=@numDomainID AND tintPriceLevel=13),'') ELSE 'Price Level 13' END)
		WHERE
			vcOrigDbColumnName='monPriceLevel13'

		UPDATE
			#tempField
		SET
			vcFieldName = (CASE WHEN LEN(ISNULL((SELECT vcPriceLevelName FROm PricingNamesTable WHERE numDomainID=@numDomainID AND tintPriceLevel=14),'')) > 0 THEN ISNULL((SELECT vcPriceLevelName FROm PricingNamesTable WHERE numDomainID=@numDomainID AND tintPriceLevel=14),'') ELSE 'Price Level 14' END)
		WHERE
			vcOrigDbColumnName='monPriceLevel14'

		UPDATE
			#tempField
		SET
			vcFieldName = (CASE WHEN LEN(ISNULL((SELECT vcPriceLevelName FROm PricingNamesTable WHERE numDomainID=@numDomainID AND tintPriceLevel=15),'')) > 0 THEN ISNULL((SELECT vcPriceLevelName FROm PricingNamesTable WHERE numDomainID=@numDomainID AND tintPriceLevel=15),'') ELSE 'Price Level 15' END)
		WHERE
			vcOrigDbColumnName='monPriceLevel15'

		UPDATE
			#tempField
		SET
			vcFieldName = (CASE WHEN LEN(ISNULL((SELECT vcPriceLevelName FROm PricingNamesTable WHERE numDomainID=@numDomainID AND tintPriceLevel=16),'')) > 0 THEN ISNULL((SELECT vcPriceLevelName FROm PricingNamesTable WHERE numDomainID=@numDomainID AND tintPriceLevel=16),'') ELSE 'Price Level 16' END)
		WHERE
			vcOrigDbColumnName='monPriceLevel16'

		UPDATE
			#tempField
		SET
			vcFieldName = (CASE WHEN LEN(ISNULL((SELECT vcPriceLevelName FROm PricingNamesTable WHERE numDomainID=@numDomainID AND tintPriceLevel=17),'')) > 0 THEN ISNULL((SELECT vcPriceLevelName FROm PricingNamesTable WHERE numDomainID=@numDomainID AND tintPriceLevel=17),'') ELSE 'Price Level 17' END)
		WHERE
			vcOrigDbColumnName='monPriceLevel17'

		UPDATE
			#tempField
		SET
			vcFieldName = (CASE WHEN LEN(ISNULL((SELECT vcPriceLevelName FROm PricingNamesTable WHERE numDomainID=@numDomainID AND tintPriceLevel=18),'')) > 0 THEN ISNULL((SELECT vcPriceLevelName FROm PricingNamesTable WHERE numDomainID=@numDomainID AND tintPriceLevel=18),'') ELSE 'Price Level 18' END)
		WHERE
			vcOrigDbColumnName='monPriceLevel18'

		UPDATE
			#tempField
		SET
			vcFieldName = (CASE WHEN LEN(ISNULL((SELECT vcPriceLevelName FROm PricingNamesTable WHERE numDomainID=@numDomainID AND tintPriceLevel=19),'')) > 0 THEN ISNULL((SELECT vcPriceLevelName FROm PricingNamesTable WHERE numDomainID=@numDomainID AND tintPriceLevel=19),'') ELSE 'Price Level 19' END)
		WHERE
			vcOrigDbColumnName='monPriceLevel19'

		UPDATE
			#tempField
		SET
			vcFieldName = (CASE WHEN LEN(ISNULL((SELECT vcPriceLevelName FROm PricingNamesTable WHERE numDomainID=@numDomainID AND tintPriceLevel=20),'')) > 0 THEN ISNULL((SELECT vcPriceLevelName FROm PricingNamesTable WHERE numDomainID=@numDomainID AND tintPriceLevel=20),'') ELSE 'Price Level 20' END)
		WHERE
			vcOrigDbColumnName='monPriceLevel20'
	END


SELECT * FROM #tempField ORDER BY numReportFieldGroupID,vcFieldName

DROP TABLE #tempField
		
END
