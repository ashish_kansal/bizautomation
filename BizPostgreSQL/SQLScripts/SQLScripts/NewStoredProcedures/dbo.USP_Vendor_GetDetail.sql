GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_Vendor_GetDetail')
DROP PROCEDURE USP_Vendor_GetDetail
GO
CREATE PROCEDURE [dbo].[USP_Vendor_GetDetail]
(
	@numDomainID NUMERIC(18,0)
	,@numDivisionID NUMERIC(18,0)
	,@numItemCode NUMERIC(18,0)
	,@vcAttrValues VARCHAR(500)
)
AS
BEGIN
	SELECT TOP 10
		ISNULL(OI.numUnitHour,0) - ISNULL(OI.numUnitHourReceived,0) numQty
		,dbo.FormatedDateFromDate(OI.ItemRequiredDate,@numDomainID) vcDueDate
	FROM
		OpportunityMaster OM
	INNER JOIN
		OpportunityItems OI
	ON
		OM.numOppId = OI.numOppId
	WHERE
		OM.numDomainId = @numDomainID
		AND OM.numDivisionId = @numDivisionID
		AND OM.tintOppType = 2
		AND OM.tintOppType = 1
		AND ISNULL(OM.tintshipped,0) = 0
		AND OI.numItemCode = @numItemCode
		AND ISNULL(OI.vcAttrValues,'') = ISNULL(@vcAttrValues,'')
		AND ISNULL(OI.numUnitHour,0) - ISNULL(OI.numUnitHourReceived,0) > 0

	SELECT 
		SUM(ISNULL(OI.numUnitHour,0) - ISNULL(OI.numUnitHourReceived,0) * (monTotAmount/(CASE WHEN ISNULL(OI.numUnitHour,0) = 0 THEN 1 ELSE OI.numUnitHour END))) AS TotalPOAmount
		,SUM(ISNULL(OI.numUnitHour,0) - ISNULL(OI.numUnitHourReceived,0)) AS TotalQty
		,SUM(ISNULL(I.fltWeight,0) * (ISNULL(OI.numUnitHour,0) - ISNULL(OI.numUnitHourReceived,0))) AS TotalWeight
		,STUFF((SELECT 
					' <b>Buy</b> '+ CASE WHEN u.intType=1 THEN '$'+CAST(u.vcBuyingQty AS VARCHAR(MAX)) ELSE CAST(u.vcBuyingQty AS VARCHAR(MAX)) END+' ' + CASE WHEN u.intType=1 THEN 'Amount' WHEN u.intType=2 THEN 'Quantity' WHEN u.intType=3 THEN 'Lbs' ELSE '' END+' <b>Get</b> ' + (CASE WHEN u.tintDiscountType = 1 THEN CONCAT('$',u.vcIncentives,' off') ELSE CONCAT(u.vcIncentives,'% off') END) + '<br/>'
				FROM 
					PurchaseIncentives u
				WHERE 
					u.numPurchaseIncentiveId = numPurchaseIncentiveId 
					AND numDivisionId=@numDivisionID
				ORDER BY 
					u.numPurchaseIncentiveId
				FOR XML PATH(''), TYPE).value('.', 'varchar(max)'),1,1,'')  AS Purchaseincentives
	FROM 
		OpportunityMaster OM
	INNER JOIN
		OpportunityItems OI
	ON
		OM.numOppId = OI.numOppId 
	INNER JOIN 
		Item AS I 
	ON 
		OI.numItemCode=I.numItemCode 
	WHERE 
		OM.numDomainId = @numDomainID
		AND OM.numDivisionId = @numDivisionID
		AND OM.tintOppType = 2
		AND OM.tintOppType = 1
		AND ISNULL(OM.tintshipped,0) = 0
END
GO