
GO
/****** Object:  StoredProcedure [dbo].[USP_ManageSitePages]    Script Date: 08/08/2009 16:11:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ManageSitePages')
DROP PROCEDURE USP_ManageSitePages
GO
CREATE PROCEDURE [dbo].[USP_ManageSitePages]
          @numPageID     NUMERIC(9)  = 0,
          @vcPageName    VARCHAR(50),
          @vcPageURL     VARCHAR(1000),
          @tintPageType  TINYINT,
          @vcPageTitle   VARCHAR(1000),
          @numTemplateID NUMERIC(9),
          @numSiteID     NUMERIC(9),
          @numDomainID   NUMERIC(9),
          @numUserCntID  NUMERIC(9),
          @strItems      TEXT  = NULL,
          @bitIsActive   BIT,
          @IsMaintainScroll Bit
          
AS
  SET NOCOUNT  ON
  IF @numPageID = 0
     AND (NOT EXISTS (SELECT [vcPageName]
                      FROM   SitePages
                      WHERE  [numSiteID] = @numSiteID
                             AND ([vcPageName] = @vcPageName
                                   OR [vcPageURL] = @vcPageURL)))
    BEGIN
      INSERT INTO SitePages
                 ([vcPageName],
                  [vcPageURL],
                  [tintPageType],
                  [vcPageTitle],
                  [numTemplateID],
                  [numSiteID],
                  [numDomainID],
                  [numCreatedBy],
                  [dtCreateDate],
                  [numModifiedBy],
                  [dtModifiedDate],
                  [bitIsActive],
                  [bitIsMaintainScroll]
                   )
      VALUES     (@vcPageName,
                  @vcPageURL,
                  @tintPageType,
                  @vcPageTitle,
                  @numTemplateID,
                  @numSiteID,
                  @numDomainID,
                  @numUserCntID,
                  GETUTCDATE(),
                  @numUserCntID,
                  GETUTCDATE(),
                  @bitIsActive,
                  @IsMaintainScroll
                  )
                  
      SET @numPageID = SCOPE_IDENTITY()
      SELECT @numPageID
    END
  ELSE
    BEGIN
      UPDATE SitePages
      SET    [vcPageName] = @vcPageName,
             [vcPageURL] = @vcPageURL,
             [tintPageType] = @tintPageType,
             [vcPageTitle] = @vcPageTitle,
             [numTemplateID] = @numTemplateID,
             [numModifiedBy] = @numUserCntID,
             [dtModifiedDate] = GETUTCDATE(),
             [bitIsActive] = @bitIsActive,
             [bitIsMaintainScroll] = @IsMaintainScroll
      WHERE  [numPageID] = @numPageID
             AND numSiteID = @numSiteID
             AND numDomainID = @numDomainID
      SELECT @numPageID
    END
  DELETE FROM [StyleSheetDetails]
  WHERE       [numPageID] = @numPageID
  DECLARE  @hDocItem INT
  IF CONVERT(VARCHAR(10),@strItems) <> ''
    BEGIN
      EXEC sp_xml_preparedocument
        @hDocItem OUTPUT ,
        @strItems
      INSERT INTO [StyleSheetDetails]
                 ([numPageID],
                  [numCssID])
      SELECT @numPageID,
             X.numCssID
      FROM   (SELECT *
              FROM   OPENXML (@hDocItem, '/NewDataSet/Item', 2)
                        WITH (numCssID NUMERIC(9))) X
      EXEC sp_xml_removedocument
        @hDocItem
    END

