---Created By Anoop Jayaraj

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ManageFieldRelationship')
DROP PROCEDURE USP_ManageFieldRelationship
GO
CREATE PROCEDURE USP_ManageFieldRelationship
@byteMode as tinyint,
@numFieldRelID as numeric(9),
@numModuleID NUMERIC(18,0),
@numPrimaryListID as numeric(9),
@numSecondaryListID as numeric(9),
@numDomainID as numeric(9),
@numFieldRelDTLID as numeric(9),
@numPrimaryListItemID as numeric(9),
@numSecondaryListItemID as numeric(9),
@bitTaskRelation AS BIT=0
as
BEGIN
if @byteMode =1
BEGIN
	IF(@bitTaskRelation=1)
	BEGIN
		IF NOT EXISTS(SELECT * FROM dbo.FieldRelationship WHERE numDomainID=@numDomainID AND numPrimaryListID=@numPrimaryListID AND bitTaskRelation=1)
		BEGIN
			insert into FieldRelationship (numPrimaryListID,numSecondaryListID,numDomainID,bitTaskRelation,numModuleID)
			values (@numPrimaryListID,@numSecondaryListID,@numDomainID,@bitTaskRelation,@numModuleID)
			set @numFieldRelID=@@identity 
		END
		ELSE 
			set @numFieldRelID=0
	END
	ELSE
	BEGIN
	IF NOT EXISTS(SELECT * FROM dbo.FieldRelationship WHERE numDomainID=@numDomainID AND numPrimaryListID=@numPrimaryListID AND numSecondaryListID=@numSecondaryListID AND (numModuleID IS NULL OR numModuleID=@numModuleID))
	BEGIN
		insert into FieldRelationship (numPrimaryListID,numSecondaryListID,numDomainID,numModuleID,bitTaskRelation)
		values (@numPrimaryListID,@numSecondaryListID,@numDomainID,@numModuleID,@bitTaskRelation)
		set @numFieldRelID=@@identity 
	END
	ELSE
	BEGIN
		SET @numFieldRelID=0
	END
END
END
else if @byteMode =2
begin

	 if not exists(select * from FieldRelationshipDTL where numFieldRelID=@numFieldRelID 
	and numPrimaryListItemID=@numPrimaryListItemID and numSecondaryListItemID=@numSecondaryListItemID)
		insert into FieldRelationshipDTL (numFieldRelID,numPrimaryListItemID,numSecondaryListItemID)
		values (@numFieldRelID,@numPrimaryListItemID,@numSecondaryListItemID)
	else
	set @numFieldRelID=0 -- to identify that there is already one

END
	else if @byteMode =3
	begin
		Delete from FieldRelationshipDTL where numFieldRelDTLID=@numFieldRelDTLID
	end
	else if @byteMode =4
	begin
		Delete from FieldRelationshipDTL where numFieldRelID=@numFieldRelID
		Delete from FieldRelationship where numFieldRelID=@numFieldRelID
	end

	select @numFieldRelID
END