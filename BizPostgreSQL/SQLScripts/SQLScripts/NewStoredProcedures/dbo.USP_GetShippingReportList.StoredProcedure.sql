
GO
/****** Object:  StoredProcedure [dbo].[USP_GetShippingReportList]    Script Date: 05/07/2009 17:34:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--SELECT *FROM   ShippingReport
--SELECT *FROM   ShippingReportItems
-- USP_GetShippingReportList 2921,2184
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetShippingReportList')
DROP PROCEDURE USP_GetShippingReportList
GO
CREATE PROCEDURE [dbo].[USP_GetShippingReportList] 
               @numOppId    NUMERIC(9),
               @numOppBizDocsId NUMERIC(9)
AS
  BEGIN
  
    SELECT numShippingReportId,
		   ('Shipping Report-' + CONVERT(VARCHAR(10),numShippingReportId)) AS vcReportName,
           numOppBizDocId,
           vcBizDocID,
           LD.vcData AS ShippingCompany,
           numShippingCompany,
           (SELECT SUM(ISNULL(monShippingRate,0)) FROM dbo.ShippingBox WHERE [numShippingReportId] = SR.numShippingReportId) AS SumTotal,
           SR.numDomainId,
           SR.numOppId
    FROM   ShippingReport SR
           LEFT JOIN ListDetails LD
             ON LD.numListItemID = SR.numShippingCompany
           INNER JOIN OpportunityBizDocs BD 
           ON BD.numOppBizDocsId = SR.numOppBizDocId
    WHERE  numOppBizDocId  = @numOppBizDocsId
		AND SR.numOppId = @numOppId
     
  END
