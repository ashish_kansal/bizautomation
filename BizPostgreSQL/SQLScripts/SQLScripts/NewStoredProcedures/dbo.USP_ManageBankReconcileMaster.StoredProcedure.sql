GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ManageBankReconcileMaster')
DROP PROCEDURE USP_ManageBankReconcileMaster
GO
CREATE PROCEDURE USP_ManageBankReconcileMaster
	@tintMode AS TINYINT, 
    @numDomainID numeric(18, 0),
    @numReconcileID numeric(18, 0),
    @numCreatedBy numeric(18, 0),
    @dtStatementDate datetime,
    @monBeginBalance DECIMAL(20,5),
    @monEndBalance DECIMAL(20,5),
    @numChartAcntId numeric(18, 0),
    @monServiceChargeAmount DECIMAL(20,5),
    @numServiceChargeChartAcntId numeric(18, 0),
    @dtServiceChargeDate datetime,
    @monInterestEarnedAmount DECIMAL(20,5),
    @numInterestEarnedChartAcntId numeric(18, 0),
    @dtInterestEarnedDate DATETIME,
    @bitReconcileComplete BIT=0,
	@vcFileName VARCHAR(300)='',
	@vcFileData VARCHAR(MAX) = ''
AS

--Validation of closed financial year
EXEC dbo.USP_ValidateFinancialYearClosingDate @numDomainID,@dtStatementDate
	
	IF @tintMode=1 --SELECT
	BEGIN
		DECLARE @numCreditAmt AS DECIMAL(20,5),@numDebitAmt AS DECIMAL(20,5) 
		
		SELECT 
			@numCreditAmt=ISNULL(SUM(numCreditAmt),0)
			,@numDebitAmt=ISNULL(SUM(numDebitAmt),0) 
		FROM 
			General_Journal_Details 
		WHERE 
			numDomainID=@numDomainID 
			AND [numReconcileID] = @numReconcileID 
			AND numChartAcntId=(SELECT numChartAcntId FROM [BankReconcileMaster] WHERE  numDomainID=@numDomainID AND [numReconcileID] = @numReconcileID)
			AND numJournalId NOT IN (SELECT numJournal_Id FROM General_Journal_Header WHERE numDomainId=@numDomainID AND numReconcileID=@numReconcileID AND ISNULL(bitReconcileInterest,0) = 0)
		OPTION (OPTIMIZE FOR (@numDomainID UNKNOWN))
	
		SELECT 
			[numReconcileID]
			,[numDomainID]
			,[numCreatedBy]
			,[dtCreatedDate]
			,[dtStatementDate]
			,[monBeginBalance]
			,[monEndBalance]
			,[numChartAcntId]
			,[monServiceChargeAmount]
			,[numServiceChargeChartAcntId]
			,[dtServiceChargeDate]
			,[monInterestEarnedAmount]
			,[numInterestEarnedChartAcntId]
			,[dtInterestEarnedDate]
			,[bitReconcileComplete]
			,[dtReconcileDate]
			,@numCreditAmt AS Payment
			,@numDebitAmt AS Deposit
			,dbo.fn_GetContactName(ISNULL(numCreatedBy,0)) AS vcReconciledby
			,dbo.fn_GetChart_Of_AccountsName(ISNULL(numChartAcntId,0)) AS vcAccountName,ISNULL(vcFileName,'') AS vcFileName
		FROM 
			[BankReconcileMaster] 
		WHERE 
			numDomainID=@numDomainID 
			AND [numReconcileID] = @numReconcileID
	END
	ELSE IF @tintMode=2 --Insert/Update
	BEGIN
		IF @numReconcileID>0 --Update
			BEGIN
				UPDATE [BankReconcileMaster]
					SET    [dtStatementDate] = @dtStatementDate, [monBeginBalance]=@monBeginBalance, [monEndBalance]=@monEndBalance, [numChartAcntId] = @numChartAcntId, [monServiceChargeAmount] = @monServiceChargeAmount, [numServiceChargeChartAcntId] = @numServiceChargeChartAcntId,dtServiceChargeDate=@dtServiceChargeDate, [monInterestEarnedAmount] = @monInterestEarnedAmount, [numInterestEarnedChartAcntId] = @numInterestEarnedChartAcntId,dtInterestEarnedDate=@dtInterestEarnedDate
					WHERE  [numDomainID] = @numDomainID AND [numReconcileID] = @numReconcileID
			END
		ELSE --Insert
			BEGIN
				INSERT INTO [BankReconcileMaster] ([numDomainID], [numCreatedBy], [dtCreatedDate], [dtStatementDate], [monBeginBalance], [monEndBalance], [numChartAcntId], [monServiceChargeAmount], [numServiceChargeChartAcntId],dtServiceChargeDate,[monInterestEarnedAmount], [numInterestEarnedChartAcntId],dtInterestEarnedDate,vcFileName)
				SELECT @numDomainID, @numCreatedBy, GETUTCDATE(), @dtStatementDate, @monBeginBalance, @monEndBalance, @numChartAcntId, @monServiceChargeAmount, @numServiceChargeChartAcntId,@dtServiceChargeDate,@monInterestEarnedAmount, @numInterestEarnedChartAcntId,@dtInterestEarnedDate,@vcFileName
			
				SET @numReconcileID=SCOPE_IDENTITY()


				IF LEN(@vcFileName) > 0
				BEGIN
					DECLARE @hDocItem int
					EXEC sp_xml_preparedocument @hDocItem OUTPUT, @vcFileData

					INSERT INTO BankReconcileFileData
					(
						[numDOmainID],
						[numReconcileID],
						[dtEntryDate],
						[vcReference],
						[fltAmount],
						[vcPayee],
						[vcDescription]
					)
					SELECT 
						@numDomainID,
						@numReconcileID,
						dtEntryDate,
						vcReference,
						fltAmount,
						vcPayee,
						vcDescription
					FROM 
						OPENXML (@hDocItem,'/BankStatement/Trasactions',2)                                                                          
					WITH                       
					(                                                                          
						dtEntryDate Date, vcReference VARCHAR(500), fltAmount FLOAT, vcPayee VARCHAR(1000),vcDescription VARCHAR(1000)
					)
				END
			END	
			
			SELECT [numReconcileID], [numDomainID], [numCreatedBy], [dtCreatedDate], [dtStatementDate], [monBeginBalance], [monEndBalance], [numChartAcntId], [monServiceChargeAmount], [numServiceChargeChartAcntId], [monInterestEarnedAmount], [numInterestEarnedChartAcntId] 
			FROM   [BankReconcileMaster] WHERE  numDomainID=@numDomainID AND [numReconcileID] = @numReconcileID
	   END
	 ELSE IF @tintMode=3 --Delete
	 BEGIN
	    --Delete Service Charge,Interest Earned and Adjustment entry
	    DELETE FROM General_Journal_Details WHERE numJournalId IN (SELECT numJournal_Id FROM General_Journal_Header WHERE numDomainID=@numDomainID AND ISNULL(numReconcileID,0) = @numReconcileID)
	    DELETE FROM General_Journal_Header WHERE numDomainID=@numDomainID AND ISNULL(numReconcileID,0) = @numReconcileID
	 
	    --Set Journal_Details entry to bitReconcile=0 & bitCleared=0
        UPDATE dbo.General_Journal_Details SET bitReconcile=0,bitCleared=0,numReconcileID=NULL WHERE numDomainID=@numDomainID AND [numReconcileID] = @numReconcileID
        
		DELETE FROM [BankReconcileFileData] WHERE numDomainID=@numDomainID AND [numReconcileID] = @numReconcileID

	 	DELETE FROM [BankReconcileMaster] WHERE numDomainID=@numDomainID AND [numReconcileID] = @numReconcileID
	 END
	 ELSE IF @tintMode=4 -- Select based on numChartAcntId and bitReconcileComplete
	 BEGIN
		SELECT *,(SELECT ISNULL(SUM(numCreditAmt),0) FROM General_Journal_Details WHERE numDomainID=@numDomainID AND [numReconcileID] = BRM.numReconcileID /*AND ISNULL(bitReconcile,0)=1*/ AND numChartAcntId=BRM.numChartAcntId) AS monPayment
		,(SELECT ISNULL(SUM(numDebitAmt),0) FROM General_Journal_Details WHERE numDomainID=@numDomainID AND [numReconcileID] = BRM.numReconcileID /*AND ISNULL(bitReconcile,0)=1*/ AND numChartAcntId=BRM.numChartAcntId) AS monDeposit
	      FROM BankReconcileMaster BRM WHERE  numDomainID=@numDomainID AND numChartAcntId=@numChartAcntId 
			AND ISNULL(bitReconcileComplete,0)=@bitReconcileComplete ORDER BY numReconcileID DESC 
	 END
	 ELSE IF @tintMode=5 -- Complete Bank Reconcile
	 BEGIN
	    UPDATE General_Journal_Details SET bitReconcile=1,bitCleared=0 WHERE numDomainID=@numDomainID AND [numReconcileID] = @numReconcileID

		UPDATE BankReconcileFileData SET bitReconcile=1,bitCleared=0 WHERE numDomainID=@numDomainID AND [numReconcileID] = @numReconcileID AND ISNULL(bitCleared,0)=1
	 
		UPDATE BankReconcileMaster SET bitReconcileComplete=1,dtReconcileDate=GETUTCDATE(),numCreatedBy=@numCreatedBy 
			WHERE numDomainID=@numDomainID AND [numReconcileID] = @numReconcileID
	 END
	ELSE IF @tintMode=6 -- Get Last added entry for numChartAcntId
	BEGIN
	SELECT TOP 1 *
	    FROM BankReconcileMaster BRM WHERE  numDomainID=@numDomainID AND numChartAcntId=@numChartAcntId 
		AND ISNULL(bitReconcileComplete,0)=1 ORDER BY numReconcileID DESC 
	END
	ELSE IF @tintMode = 7 -- Select based on numChartAcntId
	BEGIN
		SELECT *,(SELECT ISNULL(SUM(numCreditAmt),0) FROM General_Journal_Details WHERE numDomainID=@numDomainID AND [numReconcileID] = BRM.numReconcileID /*AND ISNULL(bitReconcile,0)=1*/ AND numChartAcntId=BRM.numChartAcntId) AS monPayment
		,(SELECT ISNULL(SUM(numDebitAmt),0) FROM General_Journal_Details WHERE numDomainID=@numDomainID AND [numReconcileID] = BRM.numReconcileID /*AND ISNULL(bitReconcile,0)=1*/ AND numChartAcntId=BRM.numChartAcntId) AS monDeposit
	      FROM BankReconcileMaster BRM WHERE  numDomainID=@numDomainID AND numChartAcntId=@numChartAcntId 
			ORDER BY numReconcileID DESC 
	END
    
