/****** Object:  StoredProcedure [dbo].[USP_GetPageElementAttributes]    Script Date: 08/08/2009 16:15:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--EXEC [USP_GetPageElementAttributes] @numSiteID = 3,@numElementID =4
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_Projects_CT')
DROP PROCEDURE USP_Projects_CT
GO
-- =============================================
-- Author:		<Author,,Sachin Sadhu>
-- Create date: <Create Date,,29thJuly2014>
-- Description:	<Description,, to Track Row changes in Projects Table>
-- =============================================
Create PROCEDURE [dbo].[USP_Projects_CT]
-- Add the parameters for the stored procedure here
    @numDomainID numeric(18, 0)=0,
    @numUserCntID numeric(18, 0)=0,
    @numRecordID numeric(18, 0)=0 
AS
BEGIN
-- SET NOCOUNT ON added to prevent extra result sets from
-- interfering with SELECT statements.
SET NOCOUNT ON;
 -- Insert statements for procedure here
 declare @tblFields as table (

 FieldName varchar(200),
 FieldValue varchar(200)
)

Declare @Type char(1)
Declare @ChangeVersion bigint
Declare @CreationVersion bigint
Declare @last_synchronization_version bigInt=null
DECLARE @Columns_Updated VARCHAR(1000)

		SELECT 			
			@ChangeVersion=Ct.Sys_Change_version,@CreationVersion=Ct.Sys_Change_Creation_version
		FROM 
			CHANGETABLE(CHANGES dbo.ProjectsMaster, 0) AS CT
		WHERE numProId=@numRecordID

		SELECT 
			@Type = CT.SYS_CHANGE_OPERATION
		FROM 
			CHANGETABLE(CHANGES dbo.ProjectsMaster, 0) AS CT
		WHERE  CT.numProId=@numRecordID


IF (@ChangeVersion=@CreationVersion)--Insert
Begin
        SELECT 
			@Type = CT.SYS_CHANGE_OPERATION
		FROM 
			CHANGETABLE(CHANGES dbo.ProjectsMaster, 0) AS CT
		WHERE  CT.numProId=@numRecordID
END
ELSE
BEGIN




SET @last_synchronization_version=@ChangeVersion-1     
		
		SELECT 
			@Type = CT.SYS_CHANGE_OPERATION
		FROM 
			CHANGETABLE(CHANGES dbo.ProjectsMaster, @last_synchronization_version) AS CT
		WHERE  CT.numProId=@numRecordID
	
		--Fields Up date & for Type :Insert or Update:
			Declare @UFFields as table 
			(
			numAssignedBy varchar(70),
			numAssignedTo varchar(70),
			bintProClosingDate varchar(70),
			txtComments varchar(70),
			numContractId varchar(70),
			numCreatedBy varchar(70),
			bintCreatedDate varchar(70),
			numCustPrjMgr varchar(70),
			intDueDate varchar(70),
			numintPrjMgr varchar(70),
			numOppId varchar(70),
			vcProjectID varchar(70),
			vcProjectName varchar(70),
			numProjectStatus varchar(70),
			numProjectType varchar(70),
			numRecOwner varchar(70),
			numAccountID varchar(70)
			
			)

	INSERT into @UFFields(numAssignedBy,numAssignedTo,bintProClosingDate,txtComments,numContractId,numCreatedBy,bintCreatedDate,numCustPrjMgr,intDueDate,numintPrjMgr,numOppId,vcProjectID,vcProjectName,numProjectStatus,numProjectType,numRecOwner,numAccountID)
		SELECT
		CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.ProjectsMaster'), 'numAssignedBy', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS numAssignedBy ,
		CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.ProjectsMaster'), 'numAssignedTo', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS numAssignedTo, 
		CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.ProjectsMaster'), 'bintProClosingDate', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS bintProClosingDate, 
		CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.ProjectsMaster'), 'txtComments', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS txtComments, 
		CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.ProjectsMaster'), 'numContractId', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS numContractId	,
		CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.ProjectsMaster'), 'numCreatedBy', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS  numCreatedBy ,
		CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.ProjectsMaster'), 'bintCreatedDate', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS bintCreatedDate, 
        CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.ProjectsMaster'), 'numCustPrjMgr', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS numCustPrjMgr,
        CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.ProjectsMaster'), 'intDueDate', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS intDueDate,
		CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.ProjectsMaster'), 'numintPrjMgr', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS numintPrjMgr,
		CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.ProjectsMaster'), 'numOppId', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS numOppId,
		CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.ProjectsMaster'), 'vcProjectID', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS vcProjectID,
		CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.ProjectsMaster'), 'vcProjectName', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS vcProjectName,
		CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.ProjectsMaster'), 'numProjectStatus', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS numProjectStatus,
		CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.ProjectsMaster'), 'numProjectType', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS numProjectType,
		CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.ProjectsMaster'), 'numRecOwner', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS numRecOwner,
		CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.ProjectsMaster'), 'numAccountID', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS numAccountID
		
		FROM 
		CHANGETABLE(CHANGES dbo.ProjectsMaster, @last_synchronization_version) AS CT
		WHERE 
		ct.numProId=@numRecordID
    --sp_helptext USP_GetWorkFlowFormFieldMaster
	--exec USP_GetWorkFlowFormFieldMaster 1,73
INSERT INTO @tblFields(FieldName,FieldValue)
		SELECT
		FieldName,
		FieldValue
		FROM
			(
				SELECT  numAssignedBy,numAssignedTo,bintProClosingDate,txtComments,numContractId,numCreatedBy,bintCreatedDate,numCustPrjMgr,intDueDate,numintPrjMgr,numOppId,vcProjectID,vcProjectName,numProjectStatus,numProjectType,numRecOwner,numAccountID
				FROM @UFFields	
			) AS UP
		UNPIVOT
		(
		FieldValue FOR FieldName IN (numAssignedBy,numAssignedTo,bintProClosingDate,txtComments,numContractId,numCreatedBy,bintCreatedDate,numCustPrjMgr,intDueDate,numintPrjMgr,numOppId,vcProjectID,vcProjectName,numProjectStatus,numProjectType,numRecOwner,numAccountID)
		) AS upv
		where FieldValue<>0
 
END


SELECT @Columns_Updated = COALESCE(@Columns_Updated + ',', '') + FieldName FROM @tblFields 
SET @Columns_Updated=ISNULL(@Columns_Updated,'')
select @Columns_Updated

DECLARE @tintWFTriggerOn AS TINYINT
SET @tintWFTriggerOn=CASE @Type WHEN 'I' THEN 1 WHEN 'U' THEN 2 WHEN 'D' THEN 5 END

--DateField WorkFlow Logic
	
	Declare @TempnumProId AS NUMERIC
	IF @tintWFTriggerOn = 1
	BEGIN		
		IF EXISTS (SELECT numProId FROM ProjectsMaster_TempDateFields WHERE numProId = @numRecordID)
		BEGIN
			SET @TempnumProId = (SELECT PM.numProId FROM ProjectsMaster PM WHERE  (			
				(PM.bintCreatedDate >= DATEADD(DAY,-30,GETUTCDATE()) AND PM.bintCreatedDate <= DATEADD(DAY,90,GETUTCDATE()) )
				OR	
				(PM.bintProClosingDate >= DATEADD(DAY,-30,GETUTCDATE()) AND PM.bintProClosingDate <= DATEADD(DAY,90,GETUTCDATE()) )
				OR
				(PM.intDueDate >= DATEADD(DAY,-30,GETUTCDATE()) AND PM.intDueDate <= DATEADD(DAY,90,GETUTCDATE()) )
		    )		
			AND PM.numProId=@numRecordID)

			IF(@TempnumProId IS NOT NULL)
			BEGIN	
				UPDATE PMT 
				SET 
						PMT.bintCreatedDate = PM.bintCreatedDate 
						,PMT.bintProClosingDate =PM.bintProClosingDate
						,PMT.intDueDate = PM.intDueDate
					
				FROM
					[ProjectsMaster_TempDateFields] AS PMT
					INNER JOIN ProjectsMaster AS PM
						ON PMT.numProId = PM.numProId
				WHERE  (
						(PM.bintCreatedDate >= DATEADD(DAY,-30,GETUTCDATE()) AND PM.bintCreatedDate <= DATEADD(DAY,90,GETUTCDATE()) )
						OR	
						(PM.bintProClosingDate >= DATEADD(DAY,-30,GETUTCDATE()) AND PM.bintProClosingDate <= DATEADD(DAY,90,GETUTCDATE()) )
						OR
						(PM.intDueDate >= DATEADD(DAY,-30,GETUTCDATE()) AND PM.intDueDate <= DATEADD(DAY,90,GETUTCDATE()) )
						)			
					AND 
						PMT.numProId = @numRecordID
			END
			ELSE
			BEGIN
				DELETE FROM ProjectsMaster_TempDateFields WHERE numProId = @numRecordID
			END	
		END
		ELSE
		BEGIN
			INSERT INTO [ProjectsMaster_TempDateFields] (numProId, numDomainID, bintCreatedDate, bintProClosingDate,intDueDate) 
			SELECT numProId,numDomainID, bintCreatedDate, bintProClosingDate, intDueDate
			FROM ProjectsMaster
			WHERE (
					(bintCreatedDate >= DATEADD(DAY,-30,GETUTCDATE()) AND bintCreatedDate <= DATEADD(DAY,90,GETUTCDATE()) )
					OR	
					(bintProClosingDate >= DATEADD(DAY,-30,GETUTCDATE()) AND bintProClosingDate <= DATEADD(DAY,90,GETUTCDATE()) )
					OR
					(intDueDate >= DATEADD(DAY,-30,GETUTCDATE()) AND intDueDate <= DATEADD(DAY,90,GETUTCDATE()) )
				  )

				AND numProId=@numRecordID
		END
	END
	ELSE IF @tintWFTriggerOn = 2
	BEGIN
		
		SET @TempnumProId = (SELECT PM.numProId FROM ProjectsMaster PM WHERE  (			
				(PM.bintCreatedDate >= DATEADD(DAY,-30,GETUTCDATE()) AND PM.bintCreatedDate <= DATEADD(DAY,90,GETUTCDATE()) )
				OR	
				(PM.bintProClosingDate >= DATEADD(DAY,-30,GETUTCDATE()) AND PM.bintProClosingDate <= DATEADD(DAY,90,GETUTCDATE()) )
				OR
				(PM.intDueDate >= DATEADD(DAY,-30,GETUTCDATE()) AND PM.intDueDate <= DATEADD(DAY,90,GETUTCDATE()) )
		    )		
			AND PM.numProId=@numRecordID)

		IF(@TempnumProId IS NOT NULL)
		BEGIN	
			UPDATE PMT 
			SET 
					PMT.bintCreatedDate = PM.bintCreatedDate 
					,PMT.bintProClosingDate =PM.bintProClosingDate
					,PMT.intDueDate = PM.intDueDate
					
			FROM
				[ProjectsMaster_TempDateFields] AS PMT
				INNER JOIN ProjectsMaster AS PM
					ON PMT.numProId = PM.numProId
			WHERE  (
					(PM.bintCreatedDate >= DATEADD(DAY,-30,GETUTCDATE()) AND PM.bintCreatedDate <= DATEADD(DAY,90,GETUTCDATE()) )
					OR	
					(PM.bintProClosingDate >= DATEADD(DAY,-30,GETUTCDATE()) AND PM.bintProClosingDate <= DATEADD(DAY,90,GETUTCDATE()) )
					OR
					(PM.intDueDate >= DATEADD(DAY,-30,GETUTCDATE()) AND PM.intDueDate <= DATEADD(DAY,90,GETUTCDATE()) )
					)			
				AND 
					PMT.numProId = @numRecordID
		END
		ELSE
		BEGIN
			DELETE FROM ProjectsMaster_TempDateFields WHERE numProId = @numRecordID
		END	
	END
	ELSE IF @tintWFTriggerOn = 5
	BEGIN
		DELETE FROM ProjectsMaster_TempDateFields WHERE numProId = @numRecordID
	END	

	--DateField WorkFlow Logic
	
	EXEC dbo.USP_ManageWorkFlowQueue
	@numWFQueueID = 0, --  numeric(18, 0)
	@numDomainID = @numDomainID, --  numeric(18, 0)
	@numUserCntID = @numUserCntID, --  numeric(18, 0)
	@numRecordID = @numRecordID, --  numeric(18, 0)
	@numFormID = 73, --  numeric(18, 0)
	@tintProcessStatus = 1, --  tinyint
	@tintWFTriggerOn = @tintWFTriggerOn, --  tinyint
	@tintMode = 1, --  tinyint
	@vcColumnsUpdated = @Columns_Updated
END
GO

