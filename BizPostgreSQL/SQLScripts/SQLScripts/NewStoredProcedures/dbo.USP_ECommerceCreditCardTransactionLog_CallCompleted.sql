GO
IF EXISTS (SELECT * FROM sysobjects WHERE xtype = 'p' AND NAME = 'USP_ECommerceCreditCardTransactionLog_CallCompleted' ) 
    DROP PROCEDURE USP_ECommerceCreditCardTransactionLog_CallCompleted
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[USP_ECommerceCreditCardTransactionLog_CallCompleted]
(
    @numID NUMERIC(18,0)
	,@bitSuccess BIT
	,@vcMessage VARCHAR(MAX)
	,@vcExceptionMessage VARCHAR(MAX)
	,@vcStackStrace VARCHAR(MAX)
)
AS 
BEGIN
	UPDATE
		ECommerceCreditCardTransactionLog
	SET
		bitNSoftwareCallCompleted = 1
		,bitSuccess = @bitSuccess
		,vcMessage=@vcMessage
		,vcExceptionMessage=@vcExceptionMessage
		,vcStackStrace=@vcStackStrace
	WHERE
		ID=@numID

	SELECT SCOPE_IDENTITY()

END
GO