SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_CalculateItemProfit')
DROP PROCEDURE USP_CalculateItemProfit
GO
CREATE PROCEDURE [dbo].[USP_CalculateItemProfit] 
( 
	@numDomainID AS NUMERIC(18,0),
	@numOppID AS NUMERIC(18,0),
	@numUnitHour AS FLOAT,
	@monPrice AS DECIMAL(20,5),
	@numCost AS DECIMAL(20,5),
	@numVendorID AS NUMERIC(18,0),
	@numItemCode AS NUMERIC(18,0)
)
AS
BEGIN
	DECLARE @fltExchangeRate FLOAT
	SELECT @fltExchangeRate=ISNULL(fltExchangeRate,1) FROM OpportunityMaster WHERE numOppId=@numOppID

	DECLARE @avgCost int
	SET @avgCost =(select TOP 1 numCost from Domain where numDOmainId=@numDomainID)

	DECLARE @VendorCost AS DECIMAL(20,5)
	SELECT @VendorCost=monCost FROM Vendor WHERE numVendorID = @numVendorID AND numItemCode = @numItemCode

	SELECT 
		ISNULL(@VendorCost,0) AS numCost,
		dbo.fn_UOMConversion(numPurchaseUnit,numItemCode,@numDomainID,numBaseUnit) as Factor,
		ISNULL(@numUnitHour,0) * (CAST((ISNULL(@monPrice,0) * @fltExchangeRate) AS INT) - (CASE @avgCost WHEN 3 THEN CAST(@VendorCost / dbo.fn_UOMConversion(numPurchaseUnit,numItemCode,@numDomainID,numBaseUnit) AS FLOAT) WHEN 2 THEN @numCost ELSE monAverageCost END)) AS Profit
	FROM
		Item
	WHERE
		numDomainID = @numDomainID
		AND numItemCode=@numItemCode
END