GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_InsertJournalEntryForTime')
DROP PROCEDURE USP_InsertJournalEntryForTime
GO
CREATE PROCEDURE [dbo].[USP_InsertJournalEntryForTime]
    @datEntry_Date AS DATETIME,
    @numAmount AS DECIMAL(20,5),
    @numJournal_Id AS NUMERIC(9) = 0,
    @numDomainId AS NUMERIC(9) = 0,
    @numUserCntID AS NUMERIC(9) = 0,
    @numCategoryHDRID AS NUMERIC(9) = 0,
    @strRow AS TEXT = '',
    @numAccountID AS  NUMERIC(9),
    @numProjectID AS NUMERIC(9)=0,
    @numClassID AS NUMERIC=0
--    @Mode AS TINYINT = 0
AS 
    BEGIN    
    
    IF @numProjectID = 0  SET @numProjectID=NULL
    IF @numClassID = 0  SET @numClassID=NULL
    
   IF @numJournal_Id = 0 
   IF @numAccountID > 0
   BEGIN
      Select @numJournal_Id=numJournal_Id From General_Journal_Header Where numCategoryHDRID=@numCategoryHDRID And numDomainId=@numDomainId
	  Delete from General_Journal_Details where numJournalId=@numJournal_Id and numDomainId=@numDomainId
	  Delete from General_Journal_Header Where numJournal_Id=@numJournal_Id and numDomainId=@numDomainId
   END
	IF @numAccountID = 0 
		SET @numAccountID = NULL
        
            BEGIN                        
                INSERT  INTO General_Journal_Header
                        (
                          datEntry_Date,
                          numAmount,
                          numDomainId,
                          numCategoryHDRID,
                          numChartAcntId,
                          numCreatedBy,
                          datCreatedDate,
                          numProjectID,
                          numClassID
                        )
                VALUES  (
                          @datEntry_Date,
                          @numAmount,
                          @numDomainId,
                          @numCategoryHDRID,
                          @numAccountID,
                          @numUserCntID,
                          GETUTCDATE(),
                          @numProjectID,
                          @numClassID
                        )                              
                SET @numJournal_Id = SCOPE_IDENTITY()                              
                
           
                SELECT  @numJournal_Id     

-- Insert Journal entry details
                DECLARE @hDoc3 INT                                                                                                                                                                
                EXEC sp_xml_preparedocument @hDoc3 OUTPUT, @strRow                                                                                                             
                BEGIN                                                                                   
--                    IF @Mode = 0 
--                        BEGIN
                            INSERT  INTO General_Journal_Details
                                    (
                                      numJournalId,
                                      numDebitAmt,
                                      numCreditAmt,
                                      numChartAcntId,
                                      numCustomerId,
                                      numDomainId
                                    )
                                    SELECT  @numJournal_Id,numDebitAmt,numCreditAmt,numChartAcntId,numCustomerId,numDomainId
                                    FROM    ( SELECT    *	
                                              FROM      OPENXML (@hdoc3, '/NewDataSet/Table1 [TransactionId=0]',2)
                                                        WITH ( numJournalId NUMERIC(9),numDebitAmt DECIMAL(20,5), numCreditAmt DECIMAL(20,5), numChartAcntId NUMERIC(9),  numCustomerId NUMERIC(9), numDomainId NUMERIC(9))
                                            ) X 
            
                            
                        END
--                END                     
            END                        
     
    END