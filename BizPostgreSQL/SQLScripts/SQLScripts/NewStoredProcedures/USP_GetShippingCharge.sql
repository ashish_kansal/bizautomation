SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetShippingCharge')
DROP PROCEDURE USP_GetShippingCharge
GO
CREATE PROCEDURE [dbo].[USP_GetShippingCharge]
	@numDomainID NUMERIC(18,0),
	@numShippingCountry AS NUMERIC(18,0),
	@cookieId VARCHAR(MAX)=NULL,
	@numUserCntId numeric=0,
	@monShippingAmount DECIMAL(20,5) OUTPUT
AS
BEGIN
	SET @monShippingAmount=-1
	DECLARE @numPromotionID AS NUMERIC(18,0)=0

	DECLARE @decmPrice DECIMAL(18,2)
	SET @decmPrice = (
						SELECT 
							ISNULL(SUM(monTotAmount),0) 
						FROM 
							CartItems 
						WHERE 
							numDomainId=@numDomainID 
							AND vcCookieId=@cookieId 
							AND numUserCntId=@numUserCntId
					)


	SET @numPromotionID= (
							SELECT TOP 1 
								PromotionID 
							FROM 
								CartItems AS C 
							LEFT JOIN 
								PromotionOffer AS P 
							ON 
								C.PromotionID=P.numProId
								LEFT JOIN ShippingPromotions SP ON C.numDomainId = SP.numDomainId
							WHERE
								C.numDomainId=@numDomainID 
								AND C.vcCookieId=@cookieId 
								AND C.numUserCntId=@numUserCntId
								AND (ISNULL(bitFixShipping1,0)=1 OR ISNULL(bitFixShipping2,0)=1 OR ISNULL(bitFreeShiping,0)=1) 
							ORDER BY 
								numCartId 
							)

	IF(@numPromotionID>0)
	BEGIN
		DECLARE @bitFreeShiping BIT
		DECLARE @monFreeShippingOrderAmount DECIMAL(20,5)
		DECLARE @numFreeShippingCountry DECIMAL(20,5)
		DECLARE @bitFixShipping1 BIT
		DECLARE @monFixShipping1OrderAmount DECIMAL(20,5)
		DECLARE @monFixShipping1Charge DECIMAL(20,5)
		DECLARE @bitFixShipping2 BIT
		DECLARE @monFixShipping2OrderAmount DECIMAL(20,5)
		DECLARE @monFixShipping2Charge DECIMAL(20,5)
		SELECT 
			@bitFreeShiping=bitFreeShiping
			,@monFreeShippingOrderAmount=monFreeShippingOrderAmount
			,@numFreeShippingCountry=numFreeShippingCountry
			,@bitFixShipping1=bitFixShipping1
			,@monFixShipping1OrderAmount=monFixShipping1OrderAmount
			,@monFixShipping1Charge=monFixShipping1Charge
			,@bitFixShipping2=bitFixShipping2
			,@monFixShipping2OrderAmount=monFixShipping2OrderAmount
			,@monFixShipping2Charge=monFixShipping2Charge
		FROM
			PromotionOffer PO
			LEFT JOIN ShippingPromotions SP ON PO.numDomainId = SP.numDomainId
		WHERE
			PO.numDomainId=@numDomainID 
			AND numProId = @numPromotionID

		IF(ISNULL(@bitFixShipping1,0)=1 AND @decmPrice > @monFixShipping1OrderAmount)
		BEGIN
			SET @monShippingAmount=@monFixShipping1Charge
		END

		IF(ISNULL(@bitFixShipping2,0)=1 AND @decmPrice > @monFixShipping2OrderAmount)
		BEGIN
			SET @monShippingAmount=@monFixShipping2Charge
		END

		IF((ISNULL(@bitFreeShiping,0)=1 AND @numFreeShippingCountry=@numShippingCountry AND @decmPrice>@monFreeShippingOrderAmount))
		BEGIN
			SET @monShippingAmount=0
		END
	END
	
	SELECT ISNULL(@monShippingAmount,0)
END