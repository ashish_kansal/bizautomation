GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_GetBizDocTemplate' ) 
    DROP PROCEDURE USP_GetBizDocTemplate
GO
-- USP_GetBizDocTemplate 72,0,0
-- =============================================  
-- Modified by: <Author,,Sachin Sadhu>  
-- Create date: <Create Date,,24thDec2013>  
-- Description: <Description,,add 3 fields:vcBizDocImagePath,vcBizDocFooter,vcPurBizDocFooter>  
-- =============================================  
CREATE PROCEDURE [dbo].[USP_GetBizDocTemplate]
    @numDomainID NUMERIC ,
    @tintTemplateType TINYINT ,
    @numBizDocTempID NUMERIC
AS 
BEGIN
    SET NOCOUNT ON    
    
    SELECT  
		BizDocTemplate.numDomainID,
        [numBizDocID] ,
        [txtBizDocTemplate] ,
        [txtCSS] ,
        ISNULL(bitEnabled, 0) AS bitEnabled ,
        [numOppType] ,
        ISNULL(vcTemplateName, '') AS vcTemplateName ,
        ISNULL(bitDefault, 0) AS bitDefault ,
        BizDocTemplate.vcBizDocFooter ,
        BizDocTemplate.vcPurBizDocFooter,
		numOrientation,
		bitKeepFooterBottom,
		ISNULL(numRelationship,0) AS numRelationship,
		ISNULL(numProfile,0) AS numProfile,
		ISNULL(bitDisplayKitChild,0) AS bitDisplayKitChild,
		ISNULL(numAccountClass,0) AS numAccountClass,
		ISNULL(BizDocTemplate.vcBizDocImagePath,Domain.vcBizDocImagePath) As vcBizDocImagePath
    FROM
		BizDocTemplate
	INNER JOIN
		Domain
	ON
		BizDocTemplate.numDOmainID=Domain.numDomainID
    WHERE 
		BizDocTemplate.numDomainID = @numDomainID
        AND tintTemplateType = @tintTemplateType
        AND (numBizDocTempID = @numBizDocTempID OR (@numBizDocTempID = 0 AND @tintTemplateType != 0))
END
GO