GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetOnlineBillPayeeDetail')
	DROP PROCEDURE USP_GetOnlineBillPayeeDetail
GO

CREATE PROCEDURE [dbo].[USP_GetOnlineBillPayeeDetail]
	@numPayeeDetailId numeric(18, 0)
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[numPayeeDetailId],
	[vcPayeeFIId],
	[numBankDetailId],
	[vcPayeeFIListID],
	[vcPayeeName],
	[vcAccount],
	[vcAddress1],
	[vcAddress2],
	[vcAddress3],
	[vcCity],
	[vcState],
	[vcPostalCode],
	[vcCountry] ,
	[vcPhone]
FROM
	[dbo].[OnlineBillPayeeDetails]
WHERE
	[numPayeeDetailId] = @numPayeeDetailId AND [bitIsActive] = 1
 
GO
