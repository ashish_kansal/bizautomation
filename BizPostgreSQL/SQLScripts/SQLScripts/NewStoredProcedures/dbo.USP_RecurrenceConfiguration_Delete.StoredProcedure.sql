GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_RecurrenceConfiguration_Delete')
DROP PROCEDURE USP_RecurrenceConfiguration_Delete
GO
  
-- =============================================  
-- Author:  Sandeep
-- Create date: 9 Oct 2014
-- Description: Deletes recurrence configuration
-- =============================================  
Create PROCEDURE [dbo].[USP_RecurrenceConfiguration_Delete]   
	@numRecConfigID NUMERIC(18,0),
	@numDomainID NUMERIC(18,0),
	@numUserCntID NUMERIC(18,0)
AS  
BEGIN  

BEGIN TRANSACTION;

BEGIN TRY

	UPDATE RecurrenceConfiguration SET bitMarkForDelete = 1 WHERE numRecConfigID = @numRecConfigID

	DECLARE @numType SMALLINT = 0
	SELECT @numType = numType FROM RecurrenceConfiguration WHERE numRecConfigID = @numRecConfigID

	IF @numType > 0
	BEGIN
		IF @numType = 1  --Sales Order
		BEGIN
			IF (SELECT COUNT(*) FROM RecurrenceTransaction WHERE numRecConfigID = @numRecConfigID AND ISNULL(numRecurrOppID,0) > 0) > 0
			BEGIN 
				RAISERROR('RECURRENCE_TRANSACTIOS_EXISTS',16,1)
				RETURN
			END
		END
		ELSE IF @numType = 2 --BizDoc
		BEGIN
			IF (SELECT COUNT(*) FROM RecurrenceTransaction WHERE numRecConfigID = @numRecConfigID AND ISNULL(numRecurrOppBizDocID,0) > 0) > 0
			BEGIN 
				RAISERROR('RECURRENCE_TRANSACTIOS_EXISTS',16,1)
				RETURN
			END
		END
	END

	UPDATE OpportunityMaster SET vcRecurrenceType = NULL WHERE numOppId IN (SELECT numOppId FROM RecurrenceConfiguration WHERE numRecConfigID = @numRecConfigID)
	
	DELETE FROM RecurrenceErrorLog WHERE numRecConfigID = @numRecConfigID
	DELETE FROM RecurrenceTransaction WHERE numRecConfigID = @numRecConfigID
	DELETE FROM RecurrenceConfiguration WHERE numRecConfigID = @numRecConfigID
END TRY
BEGIN CATCH
	DECLARE @ErrorMessage NVARCHAR(4000);
    DECLARE @ErrorSeverity INT;
    DECLARE @ErrorState INT;

    SELECT @ErrorMessage = ERROR_MESSAGE(), @ErrorSeverity = ERROR_SEVERITY(), @ErrorState = ERROR_STATE();

    RAISERROR (@ErrorMessage, @ErrorSeverity, @ErrorState);

	IF @@TRANCOUNT > 0
        ROLLBACK TRANSACTION;
END CATCH

IF @@TRANCOUNT > 0
    COMMIT TRANSACTION;


END  
