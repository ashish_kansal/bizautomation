GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='Usp_DeleteBillingTerms')
	DROP PROCEDURE Usp_DeleteBillingTerms
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[Usp_DeleteBillingTerms]
(
	@numTermsID		BIGINT,
	@numDomainID	NUMERIC(18,0)
)
AS

BEGIN
	IF EXISTS(SELECT * FROM dbo.DivisionMaster WHERE numBillingDays = @numTermsID AND numDomainID = @numDomainID)
	BEGIN
	    RAISERROR ('DEPENDANT', -- Message text.
				   16, -- Severity.
				   1 -- State.
				   );
        RETURN
	END	
	
	DELETE FROM dbo.BillingTerms WHERE numTermsID = @numTermsID AND numDomainID = @numDomainID
END