SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ReportListMaster_Top10LeadSource')
DROP PROCEDURE USP_ReportListMaster_Top10LeadSource
GO
CREATE PROCEDURE [dbo].[USP_ReportListMaster_Top10LeadSource]
	@numDomainID NUMERIC(18,0)
	,@ClientTimeZoneOffset INT
	,@vcTimeLine VARCHAR(50)
	,@vcGroupBy VARCHAR(50)
	,@vcTeritorry VARCHAR(MAX)
	,@vcFilterBy TINYINT  -- 1:Assign TO, 2: Record Owner, 3: Teams (Based on Assigned To) 
	,@vcFilterValue VARCHAR(MAX)
	,@dtFromDate DATE
	,@dtToDate DATE
	,@numRecordCount INT
AS
BEGIN 
	DECLARE @TEMP TABLE
	(
		numDivisionID NUMERIC(18,0)
		,RowID NUMERIC(18,0)
	)

	INSERT INTO @TEMP
	(
		numDivisionID
		,RowID
	)
	SELECT 
		numDivisionID
		,MIN(ID) 
	FROM 
		DivisionMasterPromotionHistory
	WHERE
		numDomainID = @numDomainID
	GROUP BY
		numDivisionID


	DECLARE @TotalLeads NUMERIC(18,2) 

	SELECT
		@TotalLeads = COUNT(DM.numDivisionID)
	FROM
		DivisionMaster DM
	INNER JOIN	
		CompanyInfo CI
	ON
		DM.numCompanyID = CI.numCompanyId
	LEFT JOIN
		ListDetails LD
	ON
		LD.numListID=18
		AND CI.vcHow = LD.numListItemID
		AND (LD.constFlag=1 OR LD.numDomainID=@numDomainID)
	LEFT JOIN
		@TEMP T1
	ON
		DM.numDivisionID = T1.numDivisionID
	LEFT JOIN
		DivisionMasterPromotionHistory DMPH
	ON
		T1.RowID = DMPH.ID
	WHERE
		DM.numDomainID = @numDomainID
		AND ((ISNULL(DM.tintCRMType,0) = 0 AND DMPH.ID IS NULL) OR ISNULL(DMPH.tintPreviousCRMType,0) = 0)

	SELECT TOP (@numRecordCount)
		LD.vcData
		,CAST((COUNT(*) * 100) / @TotalLeads AS NUMERIC(18,2)) LeadSourcePercent
	FROM
		DivisionMaster DM
	INNER JOIN	
		CompanyInfo CI
	ON
		DM.numCompanyID = CI.numCompanyId
	LEFT JOIN
		ListDetails LD
	ON
		LD.numListID=18
		AND CI.vcHow = LD.numListItemID
		AND (LD.constFlag=1 OR LD.numDomainID=@numDomainID)
	LEFT JOIN
		@TEMP T1
	ON
		DM.numDivisionID = T1.numDivisionID
	LEFT JOIN
		DivisionMasterPromotionHistory DMPH
	ON
		T1.RowID = DMPH.ID
	WHERE
		DM.numDomainID = @numDomainID
		AND ((ISNULL(DM.tintCRMType,0) = 0 AND DMPH.ID IS NULL) OR ISNULL(DMPH.tintPreviousCRMType,0) = 0)
		AND LD.numListItemID IS NOT NULL
	GROUP BY
		LD.numListItemID
		,LD.vcData
	ORDER BY
		(COUNT(*) * 100) / @TotalLeads DESC
END
GO
