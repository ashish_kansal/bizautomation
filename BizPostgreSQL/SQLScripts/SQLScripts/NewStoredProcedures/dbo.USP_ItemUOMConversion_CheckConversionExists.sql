SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ItemUOMConversion_CheckConversionExists')
DROP PROCEDURE dbo.USP_ItemUOMConversion_CheckConversionExists
GO
CREATE PROCEDURE [dbo].[USP_ItemUOMConversion_CheckConversionExists]
(
	@numDomainID NUMERIC,
	@numItemCode NUMERIC,
	@numSourceUnit NUMERIC
)
AS 
BEGIN  
	DECLARE @numBaseUnit AS NUMERIC(18,0) = 0

	SELECT @numBaseUnit=ISNULL(numBaseUnit,0) FROM Item WHERE numDomainID=@numDomainID AND numItemCode=@numItemCode

	IF ISNULL(@numBaseUnit,0) = 0
	BEGIN
		RAISERROR('ITEM_BASE_UOM_MISSING',16,1)
	END
	ELSE 
	BEGIN
		IF @numBaseUnit = @numSourceUnit
		BEGIN 
			SELECT 1 --NO NEED TO CHECK CONVERSION BECAUSE IT IS SAME AS BASE UOM
		END
		ELSE
		BEGIN
			SELECT dbo.fn_CheckIfItemUOMConversionExists(@numDomainID,@numItemCode,@numSourceUnit)
		END
	END
END