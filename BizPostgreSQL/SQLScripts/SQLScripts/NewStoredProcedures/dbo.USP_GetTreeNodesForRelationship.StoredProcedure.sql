GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_GetTreeNodesForRelationship' ) 
    DROP PROCEDURE USP_GetTreeNodesForRelationship
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- EXEC USP_GetPageNavigationAuthorizationDetails 1,1
CREATE PROCEDURE USP_GetTreeNodesForRelationship
    (
	  @numGroupID NUMERIC(18,0),
	  @numTabID NUMERIC(18,0),
      @numDomainID NUMERIC(18, 0),
	  @bitVisibleOnly bit
    )
AS 
    SET NOCOUNT ON
    SET TRANSACTION ISOLATION LEVEL READ COMMITTED
  
    BEGIN		
        DECLARE @numModuleID AS NUMERIC(18, 0)

        SELECT TOP 1
                @numModuleID = numModuleID
        FROM    dbo.PageNavigationDTL
        WHERE   numTabID = @numTabID 
 

SELECT
	ROW_NUMBER() OVER (ORDER BY numOrder) AS ID,
	*
INTO
	#RelationshipTree 
FROM
	(
	SELECT    
		PND.numPageNavID AS numPageNavID,
		NULL AS numListItemID,
		ISNULL(TNA.bitVisible,1) AS bitVisible,
		ISNULL(PND.vcPageNavName, '') AS [vcNodeName],
		ISNULL(vcNavURL, '') AS vcNavURL,
        vcImageURL,
		ISNULL(TNA.tintType,1) AS tintType,
		PND.numParentID AS numParentID,
		PND.numParentID AS numOriginalParentID,
		ISNULL(TreeNodeOrder.numOrder,1000) AS numOrder,
		0 AS isUpdated
	FROM      
		PageNavigationDTL PND
	LEFT JOIN 
		dbo.TreeNavigationAuthorization TNA 
	ON 
		TNA.numPageNavID = PND.numPageNavID AND
		TNA.numDomainID = @numDomainID AND 
		TNA.numGroupID = @numGroupID
	LEFT JOIN
		dbo.TreeNodeOrder 
	ON
		dbo.TreeNodeOrder.numTabID = @numTabID AND
		dbo.TreeNodeOrder.numDomainID = @numDomainID AND
		dbo.TreeNodeOrder.numGroupID = @numGroupID AND
		dbo.TreeNodeOrder.numPageNavID = PND.numPageNavID
	WHERE     
		1 = 1
		AND ISNULL(PND.bitVisible, 0) = 1
		AND numModuleID = @numModuleID
	UNION
	SELECT
		NULL AS numPageNavID,
		ListDetails.numListItemID,
		ISNULL(dbo.TreeNodeOrder.bitVisible,1) AS bitVisible,
		ListDetails.vcData AS [vcNodeName],
		'../prospects/frmCompanyList.aspx?RelId='+ convert(varchar(10),ListDetails.numListItemID) vcNavURL,
		NULL as vcImageURL,
		1 AS tintType,
		6 AS numParentID,
		6 AS numOriginalParentID,
		ISNULL(TreeNodeOrder.numOrder,2000) AS numOrder,
		0 AS isUpdated
	FROM
		ListDetails
	LEFT JOIN 
		dbo.TreeNodeOrder 
	ON
		dbo.TreeNodeOrder.numListItemID = ListDetails.numListItemID AND
		dbo.TreeNodeOrder.numParentID = 6 AND
		dbo.TreeNodeOrder.numTabID = @numTabID AND
		dbo.TreeNodeOrder.numDomainID = @numDomainID AND
		dbo.TreeNodeOrder.numGroupID = @numGroupID AND
		dbo.TreeNodeOrder.numPageNavID IS NULL
	WHERE
		numListID = 5 AND
		(ListDetails.numDomainID = @numDomainID  OR constFlag = 1) AND
		(ISNULL(bitDelete,0) = 0 OR constFlag = 1) AND
		ListDetails.numListItemID <> 46 
	UNION       
	SELECT 
		NULL AS numPageNavID,
		L2.numListItemID,   
		ISNULL(dbo.TreeNodeOrder.bitVisible,1) AS bitVisible,
		L2.vcData AS [vcNodeName],
		'../prospects/frmCompanyList.aspx?RelId=' 
		+ convert(varchar(10), FRDTL.numPrimaryListItemID) + '&profileid='
        + convert(varchar(10), numSecondaryListItemID) as vcNavURL,
        NULL as vcImageURL,
		1 AS tintType,
		FRDTL.numPrimaryListItemID AS numParentID,
		FRDTL.numPrimaryListItemID AS numOriginalParentID,
		ISNULL(TreeNodeOrder.numOrder,3000) AS numOrder,
		0 AS isUpdated
	FROM      
		FieldRelationship FR
    join 
		FieldRelationshipDTL FRDTL on FRDTL.numFieldRelID = FR.numFieldRelID
    join 
		ListDetails L1 on numPrimaryListItemID = L1.numListItemID
    join 
		ListDetails L2 on numSecondaryListItemID = L2.numListItemID
	LEFT JOIN 
		dbo.TreeNodeOrder 
	ON
		dbo.TreeNodeOrder.numListItemID = L2.numListItemID AND
		dbo.TreeNodeOrder.numParentID = FRDTL.numPrimaryListItemID AND
		dbo.TreeNodeOrder.numTabID = @numTabID AND
		dbo.TreeNodeOrder.numDomainID = @numDomainID AND
		dbo.TreeNodeOrder.numGroupID = @numGroupID AND
		dbo.TreeNodeOrder.numPageNavID IS NULL
	WHERE     
		numPrimaryListItemID <> 46
        and FR.numPrimaryListID = 5
		and FR.numSecondaryListID = 21
		and FR.numDomainID = @numDomainID
        and L2.numDomainID = @numDomainID
	UNION
	SELECT 
		NULL AS numPageNavID,
		L2.numListItemID,
		ISNULL(dbo.TreeNodeOrder.bitVisible,1) AS bitVisible,
		L2.vcData AS [vcNodeName],
		'../prospects/frmProspectList.aspx?numProfile=' + CONVERT(VARCHAR(10), L2.numListItemID) AS vcNavURL,
        NULL AS vcImageURL,
		1 AS tintType,
		11 AS numParentID,
		11 AS numOriginalParentID,
		ISNULL(TreeNodeOrder.numOrder,4000) AS numOrder,
		0 AS isUpdated
	FROM    
		FieldRelationship AS FR
	INNER JOIN 
		FieldRelationshipDTL AS FRD ON FRD.numFieldRelID = FR.numFieldRelID
	INNER JOIN 
		ListDetails AS L1 ON L1.numListItemID = FRD.numPrimaryListItemID
	INNER JOIN 
		ListDetails AS L2 ON L2.numListItemID = FRD.numSecondaryListItemID
	LEFT JOIN 
		dbo.TreeNodeOrder 
	ON
		dbo.TreeNodeOrder.numListItemID = L2.numListItemID AND
		dbo.TreeNodeOrder.numParentID = 11 AND
		dbo.TreeNodeOrder.numTabID = @numTabID AND
		dbo.TreeNodeOrder.numDomainID = @numDomainID AND
		dbo.TreeNodeOrder.numGroupID = @numGroupID AND
		dbo.TreeNodeOrder.numPageNavID IS NULL
	WHERE   ( FRD.numPrimaryListItemID = 46 )
			AND ( FR.numDomainID = @numDomainID )
	UNION
	SELECT 
		NULL AS numPageNavID,
		L2.numListItemID,
		ISNULL(dbo.TreeNodeOrder.bitVisible,1) AS bitVisible,
		L2.vcData AS [vcNodeName],
		'../account/frmAccountList.aspx?numProfile=' + CONVERT(VARCHAR(10), L2.numListItemID) AS vcNavURL,
        NULL AS vcImageURL,
		1 AS tintType,
		12 AS numParentID,
		12 AS numOriginalParentID,
		ISNULL(TreeNodeOrder.numOrder,5000) AS numOrder,
		0 AS isUpdated
	FROM    
		FieldRelationship AS FR
	INNER JOIN 
		FieldRelationshipDTL AS FRD ON FRD.numFieldRelID = FR.numFieldRelID
	INNER JOIN 
		ListDetails AS L1 ON L1.numListItemID = FRD.numPrimaryListItemID
	INNER JOIN 
		ListDetails AS L2 ON L2.numListItemID = FRD.numSecondaryListItemID
	LEFT JOIN 
		dbo.TreeNodeOrder 
	ON
		dbo.TreeNodeOrder.numListItemID = L2.numListItemID AND
		dbo.TreeNodeOrder.numParentID = 12 AND
		dbo.TreeNodeOrder.numTabID = @numTabID AND
		dbo.TreeNodeOrder.numDomainID = @numDomainID AND
		dbo.TreeNodeOrder.numGroupID = @numGroupID AND
		dbo.TreeNodeOrder.numPageNavID IS NULL
	WHERE   ( FRD.numPrimaryListItemID = 46 )
			AND ( FR.numDomainID = @numDomainID )
	UNION    
	--Select Contact
	SELECT 
		NULL AS numPageNavID,
		listdetails.numListItemID,
		ISNULL(dbo.TreeNodeOrder.bitVisible,1) AS bitVisible,
		listdetails.vcData AS [vcNodeName],
		'../contact/frmContactList.aspx?ContactType=' + CONVERT(VARCHAR(10), listdetails.numListItemID) AS vcNavURL,
        NULL AS vcImageURL,
		1 AS tintType,
		13 AS numParentID,
		13 AS numOriginalParentID,
		ISNULL(TreeNodeOrder.numOrder,6000) AS numOrder,
		0 AS isUpdated
	FROM    
		listdetails
	LEFT JOIN 
		dbo.TreeNodeOrder 
	ON
		dbo.TreeNodeOrder.numListItemID = listdetails.numListItemID AND
		dbo.TreeNodeOrder.numParentID = 13 AND
		dbo.TreeNodeOrder.numTabID = @numTabID AND
		dbo.TreeNodeOrder.numDomainID = @numDomainID AND
		dbo.TreeNodeOrder.numGroupID = @numGroupID AND
		dbo.TreeNodeOrder.numPageNavID IS NULL
	WHERE   numListID = 8
			AND ( constFlag = 1
				  OR listdetails.numDomainID = @numDomainID
				)
	UNION
	SELECT 
		NULL AS numPageNavID,
		101 AS numListItemID,
		ISNULL((
					SELECT 
						bitVisible 
					FROM 
						dbo.TreeNodeOrder 
					WHERE
						dbo.TreeNodeOrder.numListItemID = 101 AND
						dbo.TreeNodeOrder.numParentID = 13 AND
						dbo.TreeNodeOrder.numTabID = @numTabID AND
						dbo.TreeNodeOrder.numDomainID = @numDomainID AND
						dbo.TreeNodeOrder.numGroupID = @numGroupID AND
						dbo.TreeNodeOrder.numPageNavID IS NULL
				),1) AS bitVisible,
		'Primary Contact' AS [vcNodeName],
		'../Contact/frmcontactList.aspx?ContactType=101',
        NULL AS vcImageURL,
		1 AS tintType,
		13 AS numParentID,
		13 AS numOriginalParentID,
		ISNULL((
					SELECT 
						numOrder 
					FROM 
						dbo.TreeNodeOrder 
					WHERE
						dbo.TreeNodeOrder.numListItemID = 101 AND
						dbo.TreeNodeOrder.numParentID = 13 AND
						dbo.TreeNodeOrder.numTabID = @numTabID AND
						dbo.TreeNodeOrder.numDomainID = @numDomainID AND
						dbo.TreeNodeOrder.numGroupID = @numGroupID AND
						dbo.TreeNodeOrder.numPageNavID IS NULL
				),7000) AS numOrder,
		 0 AS isUpdated
	) AS TEMP
	ORDER BY
		TEMP.numOrder

	DECLARE @i AS INT = 1
	DECLARE @Count AS INT
	DECLARE @oldParentID AS INT
	SELECT @COUNT=COUNT(ID) FROM #RelationshipTree

	WHILE @i <= @COUNT
	BEGIN
		IF (SELECT ISNULL(numPageNavID,0) FROM #RelationshipTree WHERE ID = @i) <> 0
			SELECT @oldParentID = numPageNavID FROM #RelationshipTree WHERE ID = @i
		ELSE
			SELECT @oldParentID = numListItemID FROM #RelationshipTree WHERE ID = @i


		UPDATE #RelationshipTree SET numParentID = @i, isUpdated=1 WHERE numParentID = @oldParentID AND isUpdated = 0

		SET @i = @i + 1
	END


	IF @bitVisibleOnly = 1
		SELECT 
			CAST(ID AS INT) As ID,
			numPageNavID,
			numListItemID,
			bitVisible,
			vcNodeName, 
			vcNavURL, 
			vcImageURL,
			tintType,
			(CASE numParentID WHEN 0 THEN NULL ELSE CAST(numParentID AS INT) END) AS numParentID,
			numOriginalParentID, 
			numOrder 
		FROM
			#RelationshipTree 
		WHERE 
			bitVisible = 1
			AND (numParentID IN (SELECT ID FROM #RelationshipTree WHERE bitVisible = 1) OR numParentID = 0)
	ELSE
		SELECT CAST(ID AS INT) As ID,numPageNavID,numListItemID,bitVisible,vcNodeName,tintType,(CASE numParentID WHEN 0 THEN NULL ELSE CAST(numParentID AS INT) END) AS numParentID,numOriginalParentID, numOrder FROM #RelationshipTree WHERE numParentID IN (SELECT ID FROM #RelationshipTree) OR ISNULL(numParentID,0) = 0
	
	DROP TABLE #RelationshipTree
      
    END
