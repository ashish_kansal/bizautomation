GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ManageEmbeddedCostDefaults')
DROP PROCEDURE USP_ManageEmbeddedCostDefaults
GO
CREATE PROCEDURE [dbo].[USP_ManageEmbeddedCostDefaults]
    @numCostCatID NUMERIC,
    @numDomainID NUMERIC,
    @numUserCntID NUMERIC,
    @strItems TEXT
AS 
    BEGIN
	

        IF @numCostCatID > 0 
            BEGIN

				DELETE FROM [EmbeddedCostDefaults] WHERE [numCostCatID] =@numCostCatID AND [numDomainID]=@numDomainID

                DECLARE @hDocItem INT
                IF CONVERT(VARCHAR(10), @strItems) <> '' 
                    BEGIN
                        EXEC sp_xml_preparedocument @hDocItem OUTPUT,
                            @strItems
                        INSERT  INTO EmbeddedCostDefaults
                                (
                                  [numCostCatID],
                                  [numCostCenterID],
                                  [numAccountID],
                                  [numDivisionID],
                                  [numPaymentMethod],
                                  [numDomainID],
                                  [numCreatedBy],
                                  [dtCreatedDate],
                                  [numModifiedBy],
                                  [dtModifiedDate]
	                          )
                                SELECT  @numCostCatID,
										X.numCostCenterID,
                                        X.numAccountID,
                                        X.numDivisionID,
                                        X.numPaymentMethod,
                                        @numDomainID,
                                        @numUserCntID,
                                        GETUTCDATE(),
                                        @numUserCntID,
                                        GETUTCDATE()
                                FROM    ( SELECT    *
                                          FROM      OPENXML (@hDocItem, '/NewDataSet/Table1', 2)
                                                    WITH ( numCostCenterID NUMERIC,numAccountID NUMERIC, numDivisionID NUMERIC, numPaymentMethod NUMERIC )
                                        ) X
                        EXEC sp_xml_removedocument @hDocItem
                    END
            END
              
      
    END