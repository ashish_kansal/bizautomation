GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_MANAGE_ORDER_STATUS')
DROP PROCEDURE USP_MANAGE_ORDER_STATUS
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE dbo.USP_MANAGE_ORDER_STATUS
(
	@vcOrderStatus			VARCHAR(100)='',
	@numErrorStatus			NUMERIC(10,0)=0,
	@numSuccessStatus		NUMERIC(10,0)=0,
	@numReadyToShipStatus	NUMERIC(10,0)=0,
	@numDomainID			NUMERIC(18,0),
	@intMode				INT,
	@numBizDocType			NUMERIC(10,0)
)
AS
BEGIN
	IF @intMode = 0
		BEGIN
			DELETE FROM dbo.OpportunityOrderStatus WHERE numDomainID = @numDomainID
			
			INSERT INTO dbo.OpportunityOrderStatus (
				vcOrderStatus,
				numErrorStatus,
				numSuccessStatus,
				numReadyToShipStatus,
				numDomainID,
				numBizDocType
			) VALUES ( 
				/* vcOrderStatus - varchar(100) */ @vcOrderStatus,
				/* numErrorStatus - numeric(10, 0) */ @numErrorStatus,
				/* numSuccessStatus - numeric(10, 0) */ @numSuccessStatus,
				/* numReadyToShipStatus - numeric(10, 0) */ @numReadyToShipStatus,
				/* numDomainID - numeric(18, 0) */ @numDomainID,
				/* numBizDocType - numeric(10, 0) */ @numBizDocType ) 			
		END
	
	IF @intMode = 1	
		BEGIN
			SELECT  vcOrderStatus,
					ISNULL(numErrorStatus,0) AS numErrorStatus,
					ISNULL(numSuccessStatus,0) AS numSuccessStatus,
					ISNULL(numReadyToShipStatus,0) AS numReadyToShipStatus,
					ISNULL(numDomainID,0) AS numDomainID,
					ISNULL(numBizDocType,0) AS numBizDocType
			FROM dbo.OpportunityOrderStatus WHERE numDomainID = @numDomainID				
		END
	
	
END
