GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_ManageDocumentRepositary' ) 
    DROP PROCEDURE USP_ManageDocumentRepositary
GO
CREATE PROCEDURE USP_ManageDocumentRepositary
    @tintMode AS TINYINT,
    @numGenericDocID NUMERIC,
    @numUserCntID NUMERIC,
    @numDomainID NUMERIC
AS 
BEGIN
    IF @tintMode = 1 --CheckOut
        BEGIN
            UPDATE  dbo.GenericDocuments
            SET     numLastCheckedOutBy = @numUserCntID,
                    dtCheckOutDate = GETUTCDATE(),
                    tintCheckOutStatus=1,
                    numModifiedBy=@numUserCntID,
                    bintModifiedDate=GETUTCDATE()
--                    intDocumentVersion = ISNULL(intDocumentVersion, 0) + 1
            WHERE   numGenericDocID = @numGenericDocID
                    AND numDomainID = @numDomainID
        END
        
    IF @tintMode = 2 --Check in 
        BEGIN
            UPDATE  dbo.GenericDocuments
            SET     
                    dtCheckInDate = GETUTCDATE(),
					intDocumentVersion = ISNULL(intDocumentVersion, 0) + 1,
					tintCheckOutStatus=0,
					numModifiedBy=@numUserCntID,
                    bintModifiedDate=GETUTCDATE()
            WHERE   numGenericDocID = @numGenericDocID
                    AND numDomainID = @numDomainID
        END
        

--SELECT * FROM dbo.GenericDocuments
END