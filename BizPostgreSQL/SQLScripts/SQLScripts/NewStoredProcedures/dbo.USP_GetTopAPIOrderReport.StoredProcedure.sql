GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_GetTopAPIOrderReport' ) 
    DROP PROCEDURE USP_GetTopAPIOrderReport
GO
--created by Joseph
set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go


CREATE PROC [dbo].[USP_GetTopAPIOrderReport]
          @numDomainID AS NUMERIC(9),
          @numWebApiId AS NUMERIC(9),
		@Mode AS TINYINT = 0
AS
  BEGIN
IF @Mode = 0
BEGIN

SELECT TOP 1 ISNULL(vcWebApiOrderReportId,0) as ReportId ,ISNULL(tintReportTypeId,0) as tintReportTypeId FROM WebApiOrderReports 
WHERE numDomainId = @numDomainID AND WebApiId = @numWebApiId AND tintStatus = 0

END

IF @Mode = 1
BEGIN

SELECT TOP 1 IAO.numImportApiOrderReqId as RequestId,ISNULL(IAO.tintRequestType,0) AS tintRequestType,ISNULL(IAO.vcApiOrderId,0) AS vcApiOrderId,
ISNULL(IAO.dtFromDate,'') as dtFromDate,ISNULL(IAO.dtToDate,'') AS dtToDate,ISNULL(IAO.dtCreatedDate,'') AS dtCreatedDate,IAO.numOppId,OM.vcMarketplaceOrderReportId,IAO.numOppId
FROM ImportApiOrder IAO JOIN OpportunityMaster OM on IAO.numOppId = OM.numOppId
WHERE IAO.numDomainId = @numDomainID AND IAO.numWebApiId = @numWebApiId AND IAO.bitIsActive = 1
ORDER BY IAO.numImportApiOrderReqId DESC

END


END 
