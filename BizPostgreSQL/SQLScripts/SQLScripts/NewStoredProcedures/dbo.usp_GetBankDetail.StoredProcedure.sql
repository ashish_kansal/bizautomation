
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_GetBankDetail')
	DROP PROCEDURE usp_GetBankDetail
GO
/****** Added By : Joseph ******/
/****** Gets Bank Account Details with Account Balance and LastUpdated Details from Table BankDetails******/


CREATE PROCEDURE [dbo].[usp_GetBankDetail]
@tintMode as numeric(9)=0,  
@numBankDetailID numeric(18, 0)=0,
@numAccountID numeric(18, 0)=0,
@numDomainID numeric(18, 0)
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

BEGIN  
	IF @tintMode = 0
		BEGIN
			SELECT BD.numBankDetailID,BM.numBankMasterID,BM.vcFIName,BM.vcFIId,BM.vcFIOrganization,BM.vcFIOFXServerUrl, BM.tintBankType,BM.vcBankPhone,BM.vcBankWebsite,
			BM.vcOFXAccessKey, BD.vcBankID,BD.numAccountID, BD.vcAccountNumber, BD.vcAccountType,BD.vcUserName,BD.vcPassword,
			BD.numDomainID,BD.numAccountID,bd.numUserContactID,BSH.dtCreatedDate 
			FROM dbo.BankDetails BD INNER JOIN dbo.BankMaster BM ON BM.numBankMasterID = BD.numBankMasterID
			LEFT JOIN dbo.BankStatementHeader BSH ON  BD.numBankDetailID = BSH.numBankDetailID
			WHERE BD.numBankDetailID = @numBankDetailID AND  BD.bitIsActive = 1 AND BSH.dtCreatedDate IS NULL OR BSH.dtCreatedDate =
			(SELECT MAX(dtCreatedDate) FROM dbo.BankStatementHeader WHERE numBankDetailID=@numBankDetailID)
		END
	IF @tintMode = 1
		BEGIN
			SELECT BD.numBankDetailID,BM.numBankMasterID,BM.vcFIName,BM.vcFIId,BM.vcFIOrganization,BM.vcFIOFXServerUrl, 
			BM.tintBankType,BM.vcBankPhone,BM.vcBankWebsite,
			BM.vcOFXAccessKey, BD.vcBankID,BD.numAccountID, BD.vcAccountNumber, BD.vcAccountType,BD.vcUserName,BD.vcPassword,
			BD.numDomainID,BD.numAccountID,bd.numUserContactID,BSH.dtCreatedDate 
			FROM dbo.BankDetails BD INNER JOIN dbo.BankMaster BM ON BM.numBankMasterID = BD.numBankMasterID
			LEFT JOIN dbo.BankStatementHeader BSH ON  BD.numBankDetailID = BSH.numBankDetailID
			WHERE  BD.vcAccountNumber IS NOT NULL AND BD.vcAccountType IS NOT NULL AND BD.vcUserName IS NOT NULL AND BD.vcPassword IS NOT NULL AND  BD.bitIsActive = 1 AND BSH.dtCreatedDate IS NULL OR BSH.dtCreatedDate =
			(SELECT MAX(dtCreatedDate) FROM dbo.BankStatementHeader WHERE numBankDetailID= BD.numBankDetailID)
		END
	IF @tintMode = 2
		BEGIN
			SELECT BD.numBankDetailID,BM.numBankMasterID,BM.vcFIName,BM.vcFIId,BM.vcFIOrganization,BM.vcFIOFXServerUrl, BM.tintBankType,BM.vcBankPhone,BM.vcBankWebsite,
			BM.vcOFXAccessKey, BD.vcBankID,BD.numAccountID, BD.vcAccountNumber, BD.vcAccountType,BD.vcUserName,BD.vcPassword,
			BD.numDomainID,BD.numAccountID,bd.numUserContactID FROM dbo.BankDetails BD 
			INNER JOIN dbo.BankMaster BM ON BM.numBankMasterID = BD.numBankMasterID
			INNER JOIN dbo.Chart_Of_Accounts COA ON bd.numAccountID = COA.numAccountID
			WHERE COA.numDomainId = @numDomainID AND COA.numAccountID = @numAccountID AND BD.bitIsActive = 1
		END
END
