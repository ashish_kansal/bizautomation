-- USP_HelpCategory '',1,0
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_HelpCategory')
DROP PROCEDURE USP_HelpCategory
GO
CREATE PROCEDURE [dbo].[USP_HelpCategory]
    (
      @Search VARCHAR(500),
      @intSearchType INT,
      @numHelpId INT = 0
    )
AS 
    BEGIN
        IF @intSearchType = 1 
            BEGIN
                SELECT  helpcategory,
                        helpheader,
                        numHelpId, 
                        helpMetatag,
                        ISNULL(C.vcCatecoryName,'') AS vcCatecoryName,
                        CASE WHEN C.[bitLinkPage] = 1 THEN ISNULL(C.[numPageID],0)
                        ELSE 0 END  AS CatHelpID
                FROM    helpmaster h
                        LEFT OUTER JOIN helpCategories c ON h.[helpcategory] = c.numHelpCategoryID
--                WHERE   helpMetatag LIKE +'%' + @Search + '%'
WHERE helpcategory<>0
                ORDER BY helpcategory,intDisplayOrder
            END
        ELSE 
            IF @intSearchType = 2 
                BEGIN
                    SELECT  helpcategory,
                            helpheader,
                            numHelpId,
                            helpMetatag,
                            ISNULL(helpshortdesc, '') AS helpshortdesc,
                            ISNULL(C.vcCatecoryName,'') AS vcCatecoryName
                    FROM    helpmaster h
                            LEFT OUTER JOIN helpCategories c ON h.[helpcategory] = c.numHelpCategoryID
                    WHERE    
							CONTAINS(helpDescription, @Search)
							OR CONTAINS(helpheader, @Search)
							OR CONTAINS(helpMetatag, @Search)
--                            OR helpshortdesc LIKE +'%' + @Search + '%'
--                            OR helpMetatag LIKE +'%' + @Search + '%'
                    ORDER BY helpheader
                END
            ELSE 
                IF @intSearchType = 3 
                    BEGIN
                        SELECT  ISNULL(helpDescription, '') AS helpDescription,
                                ISNULL(C.vcCatecoryName,'') AS vcCatecoryName
                        FROM    helpmaster h
                                LEFT OUTER JOIN helpCategories c ON h.[helpcategory] = c.numHelpCategoryID
                        WHERE   numHelpId = @numHelpId
                    END
                    
            ELSE   IF @intSearchType = 4 
            BEGIN
                    set @Search = '"' + @Search + '"'

                SELECT ISNULL(helpDescription, '') AS helpDescription,
                numhelpID
                FROM    helpmaster h
                        LEFT OUTER JOIN helpCategories c ON h.[helpcategory] = c.numHelpCategoryID
				WHERE   CONTAINS(helpLinkingPageURL,@Search)
                ORDER BY helpcategory,intDisplayOrder
            END
    END