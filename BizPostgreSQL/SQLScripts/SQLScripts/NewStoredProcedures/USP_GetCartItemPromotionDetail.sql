SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetCartItemPromotionDetail')
DROP PROCEDURE USP_GetCartItemPromotionDetail
GO
CREATE PROCEDURE [dbo].[USP_GetCartItemPromotionDetail]
	@numDomainID NUMERIC(18,0),
	@numItemCode NUMERIC(18,0),
	@numUOMID NUMERIC(18,0),
	@numShippingCountry AS NUMERIC(18,0),
	@cookieId VARCHAR(MAX)=NULL,
	@numUserCntId numeric=0,
	@vcSendCoupon VARCHAR(200)=0
AS
BEGIN
	DECLARE @vPromotionDesc VARCHAR(MAX)
	IF(@vcSendCoupon='0')
	BEGIN

	DECLARE @vcItemName AS VARCHAR(500)
	DECLARE @chrItemType AS CHAR(1)
	DECLARE @numItemGroup AS NUMERIC(18,0)
	DECLARE @ItemDesc AS VARCHAR(1000)
	DECLARE @numItemClassification AS NUMERIC(18,0)

	DECLARE @numWarehouseItemID NUMERIC(18,0)

	
	DECLARE @decmPrice DECIMAL(18,2)
	SET @decmPrice=(select ISNULL(SUM(monTotAmount),0) from CartItems where numDomainId=@numDomainID AND vcCookieId=@cookieId AND numUserCntId=@numUserCntId)

	DECLARE @numShippingPromotionID AS NUMERIC(18,0)=0
	DECLARE @numPromotionID AS NUMERIC(18,0)=0

	SET @numShippingPromotionID=(SELECT TOP 1 PromotionID from CartItems AS C 
	LEFT JOIN PromotionOffer AS P ON C.PromotionID=P.numProId
	LEFT JOIN ShippingPromotions SP ON C.numDomainId = SP.numDomainId
	where C.numDomainId=@numDomainID AND C.vcCookieId=@cookieId AND C.numUserCntId=@numUserCntId
	AND (ISNULL(SP.bitFixShipping1,0)=1 OR ISNULL(SP.bitFixShipping2,0)=1 OR ISNULL(SP.bitFreeShiping,0)=1)  AND ISNULL(C.bitParentPromotion,0)=1
	AND 1=(CASE WHEN P.tintOfferTriggerValueType=1 AND C.numUnitHour>=fltOfferTriggerValue THEN 1 WHEN P.tintOfferTriggerValueType=2 AND monTotAmount>=fltOfferTriggerValue THEN 1 ELSE 0 END))
	PRINT(@decmPrice)

	IF(ISNULL(@numPromotionID,0)=0)
	BEGIN
	-- GET ITEM DETAILS
	SELECT
		@vcItemName = vcItemName,
		@ItemDesc = Item.txtItemDesc,
		@chrItemType = charItemType,
		@numItemGroup = numItemGroup,
		@numItemClassification = numItemClassification,
		@numWarehouseItemID = (CASE WHEN ISNULL(@numWarehouseItemID,0) > 0 THEN @numWarehouseItemID ELSE WareHouseItems.numWareHouseItemID END)
	FROM
		Item
	OUTER APPLY
	(
		SELECT TOP 1 numWareHouseItemID,monWListPrice FROM WareHouseItems WHERE numItemID=@numItemCode AND (numWareHouseItemID = @numWarehouseItemID OR ISNULL(@numWarehouseItemID,0) = 0)
	) AS WareHouseItems
	WHERE
		Item.numItemCode = @numItemCode


	--IF @chrItemType = 'P' AND ISNULL(@numWarehouseItemID,0) = 0
	--BEGIN
	--	RAISERROR('WAREHOUSE_DOES_NOT_EXISTS',16,1)
	--END

	

	-- CHECK IF PROMOTION CAN BE TRIGGERED FROM THIS ITEM
	
	DECLARE @bitCoupon bit
	DECLARE @vcCouponCode VARCHAR(200)
	SELECT TOP 1
		@numPromotionID = numProId 
	FROM 
		PromotionOffer PO
	WHERE 
		numDomainId=@numDomainID 
		AND ISNULL(bitEnabled,0)=1
		AND ISNULL(bitAppliesToSite,0)=1 
		AND 1 = (CASE WHEN bitRequireCouponCode = 1 THEN CASE WHEN ISNULL(intCouponCodeUsed,0) < ISNULL(tintUsageLimit,0) OR ISNULL(tintUsageLimit,0) = 0  THEN 1 ELSE 0 END ELSE 1 END)
		AND 1 = (CASE WHEN bitNeverExpires = 1 THEN 1 ELSE (CASE WHEN CAST(GETDATE() AS DATE)>=dtValidFrom AND CAST(GETDATE() AS DATE)<=dtValidTo THEN 1 ELSE 0 END) END)
		AND (1 = (CASE 
					WHEN tintOfferBasedOn = 1 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId=PO.numProId AND tintRecordType=5 AND tintType=1 AND numValue=@numItemCode) > 0 THEN 1 ELSE 0 END)
					WHEN tintOfferBasedOn = 2 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId=PO.numProId AND tintRecordType=5 AND tintType=2 AND numValue=@numItemClassification) > 0 THEN 1 ELSE 0 END)
					ELSE 0
				END)
			OR
			1 = (CASE 
					WHEN tintDiscoutBaseOn = 1 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId=PO.numProId AND tintRecordType=6 AND tintType=1 AND numValue=@numItemCode) > 0 THEN 1 ELSE 0 END)
					WHEN tintDiscoutBaseOn = 2 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId=PO.numProId AND tintRecordType=6 AND tintType=2 AND numValue=@numItemClassification) > 0 THEN 1 ELSE 0 END)
					WHEN tintDiscoutBaseOn = 3 AND tintOfferBasedOn = 1 THEN (CASE WHEN (SELECT COUNT(SimilarItems.numItemCode) FROM SimilarItems WHERE numParentItemCode IN (SELECT numValue FROM PromotionOfferItems WHERE numProId=PO.numProId AND tintRecordType=5 AND tintType=1 AND SimilarItems.numItemCode=@numItemCode))>0 THEN 1 ELSE 0 END)
					WHEN tintDiscoutBaseOn = 3 AND tintOfferBasedOn = 2 THEN (CASE WHEN (SELECT COUNT(SimilarItems.numItemCode) FROM SimilarItems WHERE SimilarItems.numItemCode IN(@numItemCode) AND numParentItemCode IN (SELECT numItemCode FROM Item WHERE numItemClassification IN (SELECT numValue FROM PromotionOfferItems WHERE numProId=PO.numProId AND tintRecordType=5 AND tintType=2)))>0 THEN 1 ELSE 0 END)
					ELSE 0
				END)
			)
	ORDER BY
		CASE 
			WHEN PO.txtCouponCode IN(SELECT vcCoupon FROM CartItems WHERE numUserCntId=@numUserCntId AND vcCookieId=@cookieId) THEN 1
			WHEN PO.tintOfferBasedOn = 1 AND PO.tintDiscoutBaseOn = 1 THEN 2
			WHEN PO.tintOfferBasedOn = 2 AND PO.tintDiscoutBaseOn = 1 THEN 3
			WHEN PO.tintOfferBasedOn = 1 AND PO.tintDiscoutBaseOn = 2 THEN 4
			WHEN PO.tintOfferBasedOn = 2 AND PO.tintDiscoutBaseOn = 2 THEN 5
			WHEN PO.tintOfferBasedOn = 1 AND PO.tintDiscoutBaseOn = 3 THEN 6
			WHEN PO.tintOfferBasedOn = 2 AND PO.tintDiscoutBaseOn = 3 THEN 7
		END

		END

	
	--IF ISNULL(@numPromotionID,0) > 0
	--BEGIN
		-- Get Top 1 Promotion Based on priority
		

		DECLARE @bitRequireCouponCode VARCHAR(200)

		DECLARE @tintOfferTriggerValueType INT
		DECLARE @fltOfferTriggerValue DECIMAL
		
		SET @vcCouponCode=(SELECT  STUFF((SELECT 
			','+txtCouponCode
		FROM 
			PromotionOffer PO
		WHERE 
			numDomainId=@numDomainID 
			AND ISNULL(bitRequireCouponCode,0)=1
			AND ISNULL(bitEnabled,0)=1
			AND ISNULL(bitAppliesToSite,0)=1 
			AND 1 = (CASE WHEN bitRequireCouponCode = 1 THEN CASE WHEN ISNULL(intCouponCodeUsed,0) < ISNULL(tintUsageLimit,0) OR ISNULL(tintUsageLimit,0) = 0  THEN 1 ELSE 0 END ELSE 1 END)
			AND 1 = (CASE WHEN bitNeverExpires = 1 THEN 1 ELSE (CASE WHEN CAST(GETDATE() AS DATE)>=dtValidFrom AND CAST(GETDATE() AS DATE)<=dtValidTo THEN 1 ELSE 0 END) END)
			AND (1 = (CASE 
						WHEN tintOfferBasedOn = 1 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId=PO.numProId AND tintRecordType=5 AND tintType=1 AND numValue=@numItemCode) > 0 THEN 1 ELSE 0 END)
						WHEN tintOfferBasedOn = 2 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId=PO.numProId AND tintRecordType=5 AND tintType=2 AND numValue=@numItemClassification) > 0 THEN 1 ELSE 0 END)
						ELSE 0
					END)
				)
			AND (SELECT COUNT(numCartId) FROM CartItems WHERE vcCoupon=PO.txtCouponCode AND numDomainId=@numDomainID AND numUserCntId=@numUserCntId AND vcCookieId=@cookieId)=0
		ORDER BY
			CASE 
				WHEN PO.tintOfferBasedOn = 1 AND PO.tintDiscoutBaseOn = 1 THEN 1
				WHEN PO.tintOfferBasedOn = 2 AND PO.tintDiscoutBaseOn = 1 THEN 2
				WHEN PO.tintOfferBasedOn = 1 AND PO.tintDiscoutBaseOn = 2 THEN 3
				WHEN PO.tintOfferBasedOn = 2 AND PO.tintDiscoutBaseOn = 2 THEN 4
				WHEN PO.tintOfferBasedOn = 1 AND PO.tintDiscoutBaseOn = 3 THEN 5
				WHEN PO.tintOfferBasedOn = 2 AND PO.tintDiscoutBaseOn = 3 THEN 6
			END
		FOR
                XML PATH('')
              ), 1, 0, ''))
		SET @vcCouponCode= (SELECT SUBSTRING(@vcCouponCode,2,(LEN(@vcCouponCode))))

		

		IF(LEN(@vcCouponCode)>0)
		BEGIN
			SET @bitRequireCouponCode=1
		END

		SELECT @vPromotionDesc=( 
			CASE WHEN PO.bitRequireCouponCode=0 OR
			(PO.bitRequireCouponCode=1 AND (SELECT COUNT(*) FROM CartItems WHERE PromotionID=PO.numProId AND bitParentPromotion=1 AND vcCookieId=@cookieId AND numUserCntId=@numUserCntId)>0)
			
			THEN CONCAT('Buy '
					,CASE WHEN tintOfferTriggerValueType=1 THEN CAST(fltOfferTriggerValue AS VARCHAR) ELSE CONCAT('$',fltOfferTriggerValue) END
					,CASE tintOfferBasedOn
							WHEN 1 THEN CONCAT(' of "',(SELECT Stuff((SELECT CONCAT(', ',Item.vcItemName) FROM PromotionOfferItems INNER JOIN Item ON PromotionOfferItems.numValue = Item.numItemCode WHERE numProId=PO.numProId AND tintRecordType=5 AND tintType=1 FOR XML PATH('')), 1, 2, '')), '"')
							WHEN 2 THEN CONCAT(' of items belonging to classification(s) "',(SELECT Stuff((SELECT CONCAT(', ',ListDetails.vcData) FROM PromotionOfferItems INNER JOIN ListDetails ON PromotionOfferItems.numValue = ListDetails.numListItemID WHERE numListID=36 AND numProId=PO.numProId AND tintRecordType=5 AND tintType=2 FOR XML PATH('')), 1, 2, '')), '"')
						END
					,' & get '
					, CASE tintDiscountType WHEN 1 THEN CONCAT(fltDiscountValue,'% off on') WHEN 2 THEN CONCAT('$',fltDiscountValue, ' off on') WHEN 3 THEN CONCAT(fltDiscountValue,' of') END
					, CASE 
						WHEN tintDiscoutBaseOn = 1 THEN 
							CONCAT
							(
								CONCAT(' "',(SELECT Stuff((SELECT CONCAT(',',Item.vcItemName) FROM PromotionOfferItems INNER JOIN Item ON PromotionOfferItems.numValue = Item.numItemCode WHERE numProId=PO.numProId AND tintRecordType=6 AND tintType=1 FOR XML PATH('')), 1, 1, '')), '"')
								,(CASE WHEN PO.tintDiscountType = 3 THEN ' free' ELSE '' END)
								,'.'
							)
						WHEN tintDiscoutBaseOn = 2 THEN 
							CONCAT
							(
								CONCAT(' items belonging to classification(s) "',(SELECT Stuff((SELECT CONCAT(',',ListDetails.vcData) FROM PromotionOfferItems INNER JOIN ListDetails ON PromotionOfferItems.numValue = ListDetails.numListItemID WHERE numListID=36 AND numProId=PO.numProId AND tintRecordType=6 AND tintType=2 FOR XML PATH('')), 1, 1, '')), '"')
								,(CASE WHEN PO.tintDiscountType = 3 THEN ' free' ELSE '' END)
								,'.'
							)
						WHEN tintDiscoutBaseOn = 3 THEN ' related items.'
						ELSE '' 
					END
				)  ELSE '' END) 
				
		FROM
			PromotionOffer PO
		WHERE
			numDomainId=@numDomainID 
			AND numProId =@numPromotionID

		IF(LEN(@vcCouponCode)>0)
		BEGIN
			SET @bitRequireCouponCode=1
			SET @vPromotionDesc=''
		END


		IF (@numShippingPromotionID=0)
		BEGIN
			SET @numShippingPromotionID=@numPromotionID
		END
		
		DECLARE @vcShippingDescription VARCHAR(MAX)
		DECLARE @vcQualifiedShipping VARCHAR(MAX)


		SELECT @vcShippingDescription=(CONCAT
				(
					(CASE WHEN ISNULL(bitFixShipping1,0)=1 AND @decmPrice < monFixShipping1OrderAmount THEN CONCAT('Spend $',(monFixShipping1OrderAmount-@decmPrice), CASE WHEN @decmPrice>0 THEN ' more ' ELSE '' END+' and get $',monFixShipping1Charge,' shipping. ') ELSE '' END)
					,(CASE WHEN ISNULL(bitFixShipping2,0)=1 AND @decmPrice < monFixShipping2OrderAmount THEN CONCAT('Spend $',monFixShipping2OrderAmount-@decmPrice, CASE WHEN @decmPrice>0 THEN ' more ' ELSE '' END+' and get $',monFixShipping2Charge,' shipping. ') ELSE '' END)
					,(CASE WHEN (bitFreeShiping=1 AND numFreeShippingCountry=@numShippingCountry) AND @decmPrice<monFreeShippingOrderAmount THEN CONCAT('Spend $',monFreeShippingOrderAmount-@decmPrice, CASE WHEN @decmPrice>0 THEN ' more ' ELSE '' END+' and shipping is FREE! ') ELSE '' END) 
					
				)),
				@vcQualifiedShipping=((CASE WHEN ISNULL(bitFixShipping1,0)=1 AND @decmPrice > monFixShipping1OrderAmount AND 1=(CASE WHEN ISNULL(bitFixShipping2,0)=1 AND @decmPrice<monFixShipping2OrderAmount THEN 1 WHEN ISNULL(bitFixShipping2,0)=0 THEN 1 ELSE 0 END) THEN 'You have qualified for $'+CAST(monFixShipping1Charge AS varchar)+' shipping'
						   WHEN ISNULL(bitFixShipping2,0)=1 AND @decmPrice > monFixShipping2OrderAmount  AND 1=(CASE WHEN ISNULL(bitFreeShiping,0)=1 AND @decmPrice<monFreeShippingOrderAmount THEN 1 WHEN ISNULL(bitFreeShiping,0)=0 THEN 1 ELSE 0 END) THEN 'You have qualified for $'+CAST(monFixShipping2Charge AS varchar)+' shipping'
						   WHEN (ISNULL(bitFreeShiping,0)=1 AND numFreeShippingCountry=@numShippingCountry AND @decmPrice>monFreeShippingOrderAmount) THEN 'You have qualified for Free Shipping' END
					))
		FROM
			PromotionOffer PO
			LEFT JOIN ShippingPromotions SP ON PO.numDomainId = SP.numDomainId
		WHERE
			PO.numDomainId=@numDomainID 
			AND numProId = @numShippingPromotionID

		IF((SELECT COUNT(*) FROM CartItems WHERE numItemCode=@numItemCode AND numDomainId=@numDomainID AND numUserCntId=@numUserCntId AND vcCookieId=@cookieId AND PromotionID>0 AND @vcCouponCode<>'')>0)
		BEGIN
			SET @bitRequireCouponCode='APPLIED';
			SET @vcCouponCode=(SELECT TOP 1 vcCoupon FROM CartItems WHERE numItemCode=@numItemCode AND vcCookieId=@cookieId AND numUserCntId=@numUserCntId AND numDomainId=@numDomainID AND PromotionID>0 AND @vcCouponCode<>'')

			SELECT @vPromotionDesc=( 
				CONCAT('Buy '
						,CASE WHEN tintOfferTriggerValueType=1 THEN CAST(fltOfferTriggerValue AS VARCHAR) ELSE CONCAT('$',fltOfferTriggerValue) END
						,CASE tintOfferBasedOn
								WHEN 1 THEN CONCAT(' of "',(SELECT Stuff((SELECT CONCAT(', ',Item.vcItemName) FROM PromotionOfferItems INNER JOIN Item ON PromotionOfferItems.numValue = Item.numItemCode WHERE numProId=PO.numProId AND tintRecordType=5 AND tintType=1 FOR XML PATH('')), 1, 2, '')), '"')
								WHEN 2 THEN CONCAT(' of items belonging to classification(s) "',(SELECT Stuff((SELECT CONCAT(', ',ListDetails.vcData) FROM PromotionOfferItems INNER JOIN ListDetails ON PromotionOfferItems.numValue = ListDetails.numListItemID WHERE numListID=36 AND numProId=PO.numProId AND tintRecordType=5 AND tintType=2 FOR XML PATH('')), 1, 2, '')), '"')
							END
						,' & get '
						, CASE tintDiscountType WHEN 1 THEN CONCAT(fltDiscountValue,'% off on') WHEN 2 THEN CONCAT('$',fltDiscountValue, ' off on') WHEN 3 THEN CONCAT(fltDiscountValue,' of') END
						, CASE 
							WHEN tintDiscoutBaseOn = 1 THEN 
								CONCAT
								(
									CONCAT(' "',(SELECT Stuff((SELECT CONCAT(',',Item.vcItemName) FROM PromotionOfferItems INNER JOIN Item ON PromotionOfferItems.numValue = Item.numItemCode WHERE numProId=PO.numProId AND tintRecordType=6 AND tintType=1 FOR XML PATH('')), 1, 1, '')), '"')
									,(CASE WHEN PO.tintDiscountType = 3 THEN ' free' ELSE '' END)
									,'.'
								)
							WHEN tintDiscoutBaseOn = 2 THEN 
								CONCAT
								(
									CONCAT(' items belonging to classification(s) "',(SELECT Stuff((SELECT CONCAT(',',ListDetails.vcData) FROM PromotionOfferItems INNER JOIN ListDetails ON PromotionOfferItems.numValue = ListDetails.numListItemID WHERE numListID=36 AND numProId=PO.numProId AND tintRecordType=6 AND tintType=2 FOR XML PATH('')), 1, 1, '')), '"')
									,(CASE WHEN PO.tintDiscountType = 3 THEN ' free' ELSE '' END)
									,'.'
								)
							WHEN tintDiscoutBaseOn = 3 THEN ' related items.'
							ELSE '' 
						END
					))
				
			FROM
				PromotionOffer PO
			WHERE
				numDomainId=@numDomainID 
				AND txtCouponCode=@vcCouponCode
			PRINT @vPromotionDesc
		END

		SELECT @vPromotionDesc AS vcPromotionDescription,@vcShippingDescription AS vcShippingDescription,@vcQualifiedShipping AS vcQualifiedShipping,ISNULL(@bitRequireCouponCode,'False') AS bitRequireCouponCode,@vcCouponCode AS vcCouponCode,
		ISNULL(@tintOfferTriggerValueType,0) as tintOfferTriggerValueType,ISNULL(@fltOfferTriggerValue,0) as fltOfferTriggerValue

			PRINT @decmPrice
	END
	ELSE
	BEGIN
		SELECT 
			@bitRequireCouponCode=PO.bitRequireCouponCode,
			@tintOfferTriggerValueType=PO.tintOfferTriggerValueType,
			@fltOfferTriggerValue=PO.fltOfferTriggerValue

		FROM 
			PromotionOffer PO
		WHERE 
			numDomainId=@numDomainID 
			AND ISNULL(bitEnabled,0)=1
			AND ISNULL(bitAppliesToSite,0)=1 
			AND txtCouponCode=@vcSendCoupon

		SELECT @vPromotionDesc=( 
			CONCAT('Buy '
					,CASE WHEN tintOfferTriggerValueType=1 THEN CAST(fltOfferTriggerValue AS VARCHAR) ELSE CONCAT('$',fltOfferTriggerValue) END
					,CASE tintOfferBasedOn
							WHEN 1 THEN CONCAT(' of "',(SELECT Stuff((SELECT CONCAT(', ',Item.vcItemName) FROM PromotionOfferItems INNER JOIN Item ON PromotionOfferItems.numValue = Item.numItemCode WHERE numProId=PO.numProId AND tintRecordType=5 AND tintType=1 FOR XML PATH('')), 1, 2, '')), '"')
							WHEN 2 THEN CONCAT(' of items belonging to classification(s) "',(SELECT Stuff((SELECT CONCAT(', ',ListDetails.vcData) FROM PromotionOfferItems INNER JOIN ListDetails ON PromotionOfferItems.numValue = ListDetails.numListItemID WHERE numListID=36 AND numProId=PO.numProId AND tintRecordType=5 AND tintType=2 FOR XML PATH('')), 1, 2, '')), '"')
						END
					,' & get '
					, CASE tintDiscountType WHEN 1 THEN CONCAT(fltDiscountValue,'% off on') WHEN 2 THEN CONCAT('$',fltDiscountValue, ' off on') WHEN 3 THEN CONCAT(fltDiscountValue,' of') END
					, CASE 
						WHEN tintDiscoutBaseOn = 1 THEN 
							CONCAT
							(
								CONCAT(' "',(SELECT Stuff((SELECT CONCAT(',',Item.vcItemName) FROM PromotionOfferItems INNER JOIN Item ON PromotionOfferItems.numValue = Item.numItemCode WHERE numProId=PO.numProId AND tintRecordType=6 AND tintType=1 FOR XML PATH('')), 1, 1, '')), '"')
								,(CASE WHEN PO.tintDiscountType = 3 THEN ' free' ELSE '' END)
								,'.'
							)
						WHEN tintDiscoutBaseOn = 2 THEN 
							CONCAT
							(
								CONCAT(' items belonging to classification(s) "',(SELECT Stuff((SELECT CONCAT(',',ListDetails.vcData) FROM PromotionOfferItems INNER JOIN ListDetails ON PromotionOfferItems.numValue = ListDetails.numListItemID WHERE numListID=36 AND numProId=PO.numProId AND tintRecordType=6 AND tintType=2 FOR XML PATH('')), 1, 1, '')), '"')
								,(CASE WHEN PO.tintDiscountType = 3 THEN ' free' ELSE '' END)
								,'.'
							)
						WHEN tintDiscoutBaseOn = 3 THEN ' related items.'
						ELSE '' 
					END
				))
				
		FROM
			PromotionOffer PO
		WHERE
			numDomainId=@numDomainID 
			AND txtCouponCode=@vcSendCoupon

		SELECT @bitRequireCouponCode AS bitRequireCouponCode,@tintOfferTriggerValueType AS tintOfferTriggerValueType,@vPromotionDesc AS vPromotionDesc,
		@fltOfferTriggerValue AS fltOfferTriggerValue
	END
	--END
END