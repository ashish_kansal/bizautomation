SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_DeleteEmailBroadcastConfiguration')
DROP PROCEDURE USP_DeleteEmailBroadcastConfiguration
GO
CREATE PROCEDURE [dbo].[USP_DeleteEmailBroadcastConfiguration]  
  
  @numConfigurationID AS NUMERIC(18,0) ,
  @numDomainID AS NUMERIC(18,0)
  
AS  

IF EXISTS(SELECT * FROM dbo.Broadcast WHERE numConfigurationID=@numConfigurationID)
BEGIN
	raiserror('PRIMARY',16,1);
		RETURN ;
END
ELSE
BEGIN
	DELETE FROM EmailBroadcastConfiguration where numConfigurationID = @numConfigurationID AND numDomainID = @numDomainID       
END	