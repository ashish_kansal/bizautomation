--exec USP_GetMutltiIncomeExpenses @numParentDomainID=72,@dtFromDate='01/Jun/2009',@dtToDate='07/Nov/2009'

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetMultiIncomeExpenses')
DROP PROCEDURE USP_GetMultiIncomeExpenses
GO
CREATE PROCEDURE [dbo].[USP_GetMultiIncomeExpenses]
(@numParentDomainID  int,
 @dtFromDate datetime,
 @dtToDate datetime,
 @numSubscriberID INT,
 @Rollup bit=0)
as
begin

create table #TempIncomeExpense
(numDomainID int,
vcDomainName varchar(200),
vcDomainCode varchar(50),
Debit DECIMAL(20,5),
Credit DECIMAL(20,5),	
Expense DECIMAL(20,5),
Income DECIMAL(20,5));

INSERT INTO #TempIncomeExpense

select DN.numDomainID,DN.vcDomainName,DN.vcDomainCode, Sum(Debit) as Debit, Sum(Credit) as Credit, Sum(Total) as Total,0
from VIEW_EXPENSEDAILYSUMMARY VARD
INNER JOIN 
(select * from DOMAIN DN where dn.numSubscriberID=@numSubscriberID) DN ON VARD.numDomainID  =  DN.numDomainID 
INNER JOIN (select * from DOMAIN DNV where dnv.numSubscriberID=@numSubscriberID) DNV ON VARD.numDomainID = DNV.numDomainID INNER JOIN 
financialyear FY ON FY.numDomainID=DN.numDomainID AND 
FY.dtPeriodFrom <= @dtFromDate and dtPeriodTo >= @dtFromDate 
AND (DN.numDomainID=@numParentDomainID)
AND datEntry_Date between FY.dtPeriodFrom and @dtFromDate - 1
GROUP BY DN.numDomainID,DN.vcDomainName,DN.vcDomainCode
union
select DN.numDomainID,DN.vcDomainName,DN.vcDomainCode, Sum(Debit) as Debit, Sum(Credit) as Credit, Sum(Total) as Total,0
from VIEW_EXPENSEDAILYSUMMARY VARD
INNER JOIN 
(select * from DOMAIN DN where dn.numSubscriberID=@numSubscriberID) DN ON VARD.numDomainID  =  DN.numDomainID 
INNER JOIN (select * from DOMAIN DNV where dnv.numSubscriberID=@numSubscriberID) DNV  ON VARD.numDomainID = DNV.numDomainID
AND (DN.numDomainID = @numParentDomainID  )
AND datEntry_Date between @dtFromDate and @dtToDate  
GROUP BY DN.numDomainID,DN.vcDomainName,DN.vcDomainCode

----------------------------------------
union 
--------------------------
select DN.numDomainID,DN.vcDomainName,DN.vcDomainCode, Sum(Debit) as Debit, Sum(Credit) as Credit, Sum(Total) as Total,0
from VIEW_EXPENSEDAILYSUMMARY VARD
INNER JOIN 
(select * from DOMAIN DN where dn.numSubscriberID=@numSubscriberID) DN ON VARD.vcDomainCode  like  DN.vcDomainCode + '%'
INNER JOIN (select * from DOMAIN DNV where dnv.numSubscriberID=@numSubscriberID) DNV  ON VARD.numDomainID = DNV.numDomainID INNER JOIN 
financialyear FY ON FY.numDomainID=DN.numDomainID AND 
FY.dtPeriodFrom <= @dtFromDate and dtPeriodTo >= @dtFromDate 
AND (DN.numParentDomainID = @numParentDomainID )
AND datEntry_Date between FY.dtPeriodFrom and @dtFromDate - 1
GROUP BY DN.numDomainID,DN.vcDomainName,DN.vcDomainCode
union
select DN.numDomainID,DN.vcDomainName,DN.vcDomainCode, Sum(Debit) as Debit, Sum(Credit) as Credit, Sum(Total) as Total,0
from VIEW_EXPENSEDAILYSUMMARY VARD
INNER JOIN 
(select * from DOMAIN DN where dn.numSubscriberID=@numSubscriberID) DN ON VARD.vcDomainCode  like  DN.vcDomainCode + '%'
INNER JOIN (select * from DOMAIN DNV where dnv.numSubscriberID=@numSubscriberID) DNV  ON VARD.numDomainID = DNV.numDomainID
AND (DN.numParentDomainID = @numParentDomainID  )
AND datEntry_Date between @dtFromDate and @dtToDate  
GROUP BY DN.numDomainID,DN.vcDomainName,DN.vcDomainCode;

------------------------
--getting income
-------------------------

INSERT INTO #TempIncomeExpense

select DN.numDomainID,DN.vcDomainName,DN.vcDomainCode, Sum(Debit) as Debit, Sum(Credit) as Credit, 0,Sum(Total) as Total
from VIEW_INCOMEDAILYSUMMARY VARD
INNER JOIN 
(select * from DOMAIN DN where dn.numSubscriberID=@numSubscriberID) DN  ON VARD.numDomainID  =  DN.numDomainID 
INNER JOIN (select * from DOMAIN DNV where dnv.numSubscriberID=@numSubscriberID) DNV  ON VARD.numDomainID = DNV.numDomainID INNER JOIN 
financialyear FY ON FY.numDomainID=DN.numDomainID AND 
FY.dtPeriodFrom <= @dtFromDate and dtPeriodTo >= @dtFromDate 
AND ( DN.numDomainID=@numParentDomainID)
AND datEntry_Date between FY.dtPeriodFrom and @dtFromDate - 1
GROUP BY DN.numDomainID,DN.vcDomainName,DN.vcDomainCode
union
select DN.numDomainID,DN.vcDomainName,DN.vcDomainCode, Sum(Debit) as Debit, Sum(Credit) as Credit,0, Sum(Total) as Total
from VIEW_INCOMEDAILYSUMMARY VARD
INNER JOIN 
(select * from DOMAIN DN where dn.numSubscriberID=@numSubscriberID) DN  ON VARD.numDomainID  =  DN.numDomainID 
INNER JOIN (select * from DOMAIN DNV where dnv.numSubscriberID=@numSubscriberID) DNV  ON VARD.numDomainID = DNV.numDomainID
AND (DN.numDomainID = @numParentDomainID  )
AND datEntry_Date between @dtFromDate and @dtToDate  
GROUP BY DN.numDomainID,DN.vcDomainName,DN.vcDomainCode

----------------------------------------
union 
--------------------------
select DN.numDomainID,DN.vcDomainName,DN.vcDomainCode, Sum(Debit) as Debit, Sum(Credit) as Credit,0, Sum(Total) as Total
from VIEW_INCOMEDAILYSUMMARY VARD
INNER JOIN 
(select * from DOMAIN DN where dn.numSubscriberID=@numSubscriberID) DN  ON VARD.vcDomainCode  like  DN.vcDomainCode + '%'
INNER JOIN (select * from DOMAIN DNV where dnv.numSubscriberID=@numSubscriberID) DNV  ON VARD.numDomainID = DNV.numDomainID INNER JOIN 
financialyear FY ON FY.numDomainID=DN.numDomainID AND 
FY.dtPeriodFrom <= @dtFromDate and dtPeriodTo >= @dtFromDate 
AND (DN.numDomainID = @numParentDomainID )
AND datEntry_Date between FY.dtPeriodFrom and @dtFromDate - 1
GROUP BY DN.numDomainID,DN.vcDomainName,DN.vcDomainCode
union
select DN.numDomainID,DN.vcDomainName,DN.vcDomainCode, Sum(Debit) as Debit, Sum(Credit) as Credit,0, Sum(Total) as Total
from VIEW_INCOMEDAILYSUMMARY VARD
INNER JOIN 
(select * from DOMAIN DN where dn.numSubscriberID=@numSubscriberID) DN  ON VARD.vcDomainCode  like  DN.vcDomainCode + '%'
INNER JOIN (select * from DOMAIN DNV where dnv.numSubscriberID=@numSubscriberID) DNV ON VARD.numDomainID = DNV.numDomainID
AND (DN.numParentDomainID = @numParentDomainID  )
AND datEntry_Date between @dtFromDate and @dtToDate  
GROUP BY DN.numDomainID,DN.vcDomainName,DN.vcDomainCode;

if @Rollup=0
	begin
		select 
		vcDomainName,
		isnull(Sum(isnull(Expense,0)),0) as Expense ,
		isnull(sum(isnull(Income,0)),0) *(-1)  as Income 
		from #TempIncomeExpense
		group by
		vcDomainName;
	END
ELSE IF @Rollup=1
	BEGIN
		select 
		'Income & Expenses Roll-Up' as vcDomainName,
		isnull(Sum(isnull(Expense,0)),0) as Expense ,
		isnull(sum(isnull(Income,0)),0) *(-1)  as Income 
		from #TempIncomeExpense
	END
drop table #TempIncomeExpense;

end
