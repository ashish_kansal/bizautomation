GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_GetDetailForImport' ) 
    DROP PROCEDURE USP_GetDetailForImport
GO
CREATE PROCEDURE USP_GetDetailForImport
    (
      @numDomainID NUMERIC(18),
      @numMasterID NUMERIC(9)
    )
AS 
    BEGIN
    
		DECLARE @strSQL AS NVARCHAR(MAX)
		DECLARE @vcColumns AS VARCHAR(MAX)
                
        IF ISNULL(@numMasterID, 0) = 31 --AND ISNULL(@vcColumns,'') <> '' 
            BEGIN
				
				SELECT  @vcColumns = COALESCE(@vcColumns + ',', '')
                + (vcLookBackTableName + '.' + vcDbColumnName + ' AS [' + MAPPING.vcFieldName + ']')
				FROM    dycFormField_Mapping MAPPING
						JOIN dbo.DycFieldMaster MAST ON MAPPING.numFieldID = MAST.numFieldId
				WHERE   numFormID = ISNULL(@numMasterID, 0)
				ORDER BY MAPPING.[order]
				
				SET @vcColumns = REPLACE(@vcColumns,'ItemDetails.numItemDetailID AS [Item Detail ID],','')
				SET @vcColumns = REPLACE(@vcColumns,'ItemDetails.numItemKitID AS [Item Name],','')
				SET @vcColumns = REPLACE(@vcColumns,'ItemDetails.numChildItemID AS [Child Item Name],','')
				SET @vcColumns = REPLACE(@vcColumns,'ItemDetails.numWareHouseItemID','(SELECT vcWareHouse FROM dbo.Warehouses W
																					   JOIN dbo.WareHouseItems WI ON W.numWareHouseID = WI.numWareHouseID
																					   JOIN dbo.ItemDetails IDS ON IDS.numWareHouseItemId = WI.numWareHouseItemID
																					   WHERE IDS.numItemKitID = ItemDetails.numItemKitID
																					   AND IDS.numItemDetailID = ItemDetails.numItemDetailID)')
				SET @vcColumns = REPLACE(@vcColumns,'ItemDetails.numUOMId','(SELECT vcUnitName FROM dbo.UOM WHERE numUOMId = dbo.ItemDetails.numUOMId)')
				
				PRINT @vcColumns
        
                -- SELECT  * FROM dycFormField_Mapping WHERE numFormID = 31
                SET @strSQL = ''
                SET @strSQL = ' SELECT numItemDetailID AS [Item Detail ID],Item.numItemCode AS [Item Code],Item.vcItemName AS [Item Name],Item1.vcItemName AS [Child Item Name],
								' + @vcColumns + ' FROM dbo.ItemDetails 
								LEFT JOIN dbo.Item ON dbo.ItemDetails.numItemKitID = dbo.Item.numItemCode
								LEFT JOIN dbo.Item Item1 ON dbo.ItemDetails.numChildItemID = Item1.numItemCode
							    WHERE Item.numDomainID = ' + CONVERT(VARCHAR(10), @numDomainID)
							    + ' AND ISNULL(dbo.Item.bitAssembly,0) = 1 
							        AND ISNULL(dbo.Item.bitKitParent,0) = 1 
							        ORDER BY Item.numItemCode '
                PRINT @strSQL
                EXEC SP_EXECUTESQL @strSQL
            END
	
        IF ISNULL(@numMasterID, 0) = 54 --AND ISNULL(@vcColumns,'')  <> '' 
            BEGIN
            
				SELECT  @vcColumns = COALESCE(@vcColumns + ',', '') + (vcLookBackTableName + '.' + vcDbColumnName + ' AS [' + MAPPING.vcFieldName + ']')
				FROM    dycFormField_Mapping MAPPING
						JOIN dbo.DycFieldMaster MAST ON MAPPING.numFieldID = MAST.numFieldId
				WHERE   numFormID = ISNULL(@numMasterID, 0)
				ORDER BY MAPPING.[order]
				PRINT @vcColumns
				
				-- SELECT * FROM dycFormField_Mapping WHERE numFormID = 54
				SET @strSQL = ''
                SET @strSQL = 'SELECT Item.vcItemName AS [Item Name],' + @vcColumns + ' FROM Item 
							   LEFT JOIN dbo.ItemImages ON Item.numItemCode = ItemImages.numItemCode AND Item.numDomainID = ItemImages.numDomainID
							   WHERE Item.numDomainID = ' + CONVERT(VARCHAR(10), @numDomainID)
							   + ' ORDER BY Item.numItemCode'
                PRINT @strSQL
                
                EXEC SP_EXECUTESQL @strSQL
				
            END

    END