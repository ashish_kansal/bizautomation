GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_EcommerceRelationshipProfile_GetBySiteID')
DROP PROCEDURE dbo.USP_EcommerceRelationshipProfile_GetBySiteID
GO
CREATE PROCEDURE [dbo].[USP_EcommerceRelationshipProfile_GetBySiteID]
(
	@numSiteID NUMERIC(18,0)
)
AS 
BEGIN
	SELECT
		ID
		,dbo.GetListIemName(numRelationship) vcRelationship
		,dbo.GetListIemName(numProfile) vcProfile
		,STUFF((SELECT 
					', ' + vcData
				FROM 
					ECommerceItemClassification EIC 
				INNER JOIN 
					ListDetails 
				ON 
					EIC.numItemClassification = ListDetails.numListItemID 
				WHERE 
					EIC.numECommRelatiionshipProfileID=ERP.ID
				FOR XML PATH(''),TYPE 
				).value('.','VARCHAR(MAX)') 
			  ,1,2,'') vcItemClassifications
	FROM
		EcommerceRelationshipProfile ERP
	WHERE
		numSiteID=@numSiteID
END