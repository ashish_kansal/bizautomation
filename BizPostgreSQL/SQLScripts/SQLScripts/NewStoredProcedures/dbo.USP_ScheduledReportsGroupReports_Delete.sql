GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ScheduledReportsGroupReports_Delete')
DROP PROCEDURE dbo.USP_ScheduledReportsGroupReports_Delete
GO
CREATE PROCEDURE [dbo].[USP_ScheduledReportsGroupReports_Delete]
(
	@numDomainID NUMERIC(18,0)
	,@numSRGID NUMERIC(18,0)
	,@vcReports VARCHAR(MAX)
)
AS 
BEGIN
	IF ISNULL(@vcReports,'') <> ''
	BEGIN
		DELETE 
			SRGR 
		FROM 
			ScheduledReportsGroup SRG 
		INNER JOIN 
			ScheduledReportsGroupReports SRGR 
		ON 
			SRG.ID = SRGR.numSRGID 
		WHERE 
			SRG.numDomainID = @numDomainID 
			AND numSRGID=@numSRGID
			AND SRGR.ID IN (SELECT Id FROM dbo.SplitIDs(@vcReports,','))
	END
END
GO