GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_OpportunityMaster_AddVendor')
DROP PROCEDURE dbo.USP_OpportunityMaster_AddVendor
GO
CREATE PROCEDURE [dbo].[USP_OpportunityMaster_AddVendor]
(
	@numDomainID NUMERIC(18,0)
	,@numOppID NUMERIC(18,0)
)
AS 
BEGIN
	DECLARE @TEMP TABLE
	(
		ID INT IDENTITY(1,1)
		,numDivisionId NUMERIC(18,0)
		,numItemCode NUMERIC(18,0)
		,monPrice DECIMAL(20,5)
	)

	INSERT INTO @TEMP
	(
		numDivisionId
		,numItemCode
		,monPrice
	)
	SELECT 
		numDivisionId
		,numItemCode
		,monPrice
	FROM 
		OpportunityMaster OM
	INNER JOIN
		OpportunityItems OI
	ON
		OM.numOppId = OI.numOppId
	WHERE 
		OM.numDomainId=@numDomainID 
		AND OM.numOppId=@numOppID 
		AND OM.tintOppType=2 
		AND OM.tintOppStatus=1

	DECLARE @i INT = 1
	DECLARE @iCount INT
	DECLARE @numDivisionId NUMERIC(18,0)
	DECLARE @numItemCode NUMERIC(18,0)
	DECLARE @monPrice DECIMAL(20,5)

	SELECT @iCount=COUNT(*) FROM @TEMP

	WHILE @i <= @iCount
	BEGIN
		SELECT 
			@numDivisionId=numDivisionId
			,@numItemCode=numItemCode
			,@monPrice=ISNULL(monPrice,0)
		FROM 
			@TEMP 
		WHERE ID=@i

		EXEC USP_AddVendor @numDivisionId,@numDomainID,@numItemCode,0,'',0,@monPrice
			 

		SET @i = @i + 1
	END
END
GO