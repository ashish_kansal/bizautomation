SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ShortCutGrpConf_SetDefaultURL')
DROP PROCEDURE dbo.USP_ShortCutGrpConf_SetDefaultURL
GO
CREATE PROCEDURE [dbo].[USP_ShortCutGrpConf_SetDefaultURL]
(
	@numGroupID NUMERIC(18,0),
	@numDomainID NUMERIC(18,0),
	@numTabId NUMERIC(18,0),
	@numLinkID NUMERIC(18,0)
)
AS 
BEGIN
	IF EXISTS (SELECT * FROM ShortCutGrpConf WHERE numDomainId=@numDomainID AND numGroupId=@numGroupID AND numTabId = @numTabId AND numLinkId=@numLinkID)
	BEGIN
		UPDATE ShortCutGrpConf SET bitInitialPage=0 WHERE numDomainId=@numDomainID AND numGroupId=@numGroupID AND numTabId = @numTabId AND bitInitialPage=1
		UPDATE ShortCutGrpConf SET bitInitialPage=1 WHERE numDomainId=@numDomainID AND numGroupId=@numGroupID AND numTabId = @numTabId AND numLinkId=@numLinkID
	END
END

