GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetBizDocsByOrderType')
DROP PROCEDURE USP_GetBizDocsByOrderType
GO
CREATE PROCEDURE [dbo].[USP_GetBizDocsByOrderType]
	@ListID AS NUMERIC(9)=0,                
	@numDomainID AS NUMERIC(9)=0, 
	@tintOppType AS TINYINT,
	@tintOppStatus AS TINYINT
AS
BEGIN
	SET NOCOUNT ON;

	Declare @numAuthoritativeId AS NUMERIC(9) 

	If @tintOppType=1           
	 SELECT @numAuthoritativeId=numAuthoritativeSales FROM AuthoritativeBizDocs WHERE numDomainId=@numDomainID           
	Else IF @tintOppType=2
	 SELECT @numAuthoritativeId=numAuthoritativePurchase FROM AuthoritativeBizDocs WHERE numDomainId=@numDomainID            
 

	IF @tintOppStatus=1         
	BEGIN        
		IF ((SELECT COUNT(*) FROM BizDocFilter WHERE numDomainID=@numDomainID)=0)
		BEGIN
			SELECT 
				numListItemID,
				case when @numAuthoritativeId=numListItemID then  vcData +' - Authoritative' Else vcData   End as  vcData 
			FROM 
				listdetails               
			WHERE 
				numListID=@ListID AND 
				(constFlag=1 OR numDomainID=@numDomainID)  
		END
		ELSE
		BEGIN
			SELECT 
				Ld.numListItemID,
				case when @numAuthoritativeId=Ld.numListItemID then  isnull(vcRenamedListName,vcData) +' - Authoritative' Else isnull(vcRenamedListName,vcData)   End as  vcData 
			FROM 
				BizDocFilter BDF 
			INNER JOIN 
				listdetails Ld 
			ON 
				BDF.numBizDoc=Ld.numListItemID
			LEFT JOIN 
				listorder LO 
			ON 
				Ld.numListItemID= LO.numListItemID AND 
				Lo.numDomainId = @numDomainID
			WHERE 
				Ld.numListID=@ListID AND 
				BDF.tintBizocType=@tintOppType AND 
				BDF.numDomainID=@numDomainID AND 
				(constFlag=1 OR Ld.numDomainID=@numDomainID)            
		END        
	END               
	Else        
	Begin        
		IF ((SELECT COUNT(*) FROM BizDocFilter WHERE numDomainID=@numDomainID)=0)
		BEGIN
			SELECT 
				numListItemID,
				vcData as  vcData 
			FROM 
				listdetails               
			WHERE 
				numListID=@ListID and 
				(constFlag=1 or numDomainID=@numDomainID) And 
				numListItemID<>@numAuthoritativeId          
		END
		ELSE
		BEGIN
			SELECT 
				Ld.numListItemID, 
				isnull(vcRenamedListName,vcData) as vcData 
			FROM 
				BizDocFilter BDF 
			INNER JOIN 
				listdetails Ld 
			ON 
				BDF.numBizDoc=Ld.numListItemID
			LEFT JOIN 
				listorder LO 
			ON 
				Ld.numListItemID= LO.numListItemID AND 
				Lo.numDomainId = @numDomainID
			WHERE 
				Ld.numListID=@ListID AND 
				BDF.tintBizocType=@tintOppType AND 
				BDF.numDomainID=@numDomainID AND 
				(constFlag=1 or Ld.numDomainID=@numDomainID) AND 
				Ld.numListItemID<>@numAuthoritativeId AND 
				Ld.numListItemID NOT IN (296)
		  END    
	END        
END







