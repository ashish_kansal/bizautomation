GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GET_RULE_AS_PER_ORDER')
	DROP PROCEDURE USP_GET_RULE_AS_PER_ORDER
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[USP_GET_RULE_AS_PER_ORDER]
(
	@numOrderID			NUMERIC(18,0),
	@numBizDocItemID	NUMERIC(18,0),
	@numShipClass		NUMERIC(18,0),
	@numDomainID		NUMERIC(18,0),
	@numQty				FLOAT
)
AS 
BEGIN

--SET @OrderIDs = '26171,26172,26173'
--SET @numDomainID = 72
--SET @intShipBasedOn = 1
--SET @tintType = 1

-- WANT TO FETCH DATA
DECLARE @numPriceValue As DECIMAL(20,5)
DECLARE @numWeightValue As NUMERIC(18,2)
DECLARE @numQtyValue As NUMERIC(18,0)
DECLARE @numCountryID AS NUMERIC(18)
DECLARE @numStateID AS NUMERIC(18)
DECLARE @Zip AS VARCHAR(50)
DECLARE @numZip AS NUMERIC(10)
DECLARE @numItemID AS NUMERIC(10)
DECLARE @numSourceCompanyID AS NUMERIC(10)
--DECLARE @numShipClass AS NUMERIC(10)

SET @numSourceCompanyID = 0
SELECT @numSourceCompanyID = ISNULL(numWebApiId,0) FROM dbo.OpportunityLinking
WHERE numChildOppID = @numOrderID

SELECT  @numPriceValue = ISNULL(OBDI.monPrice,ISNULL(OI.monPrice,0)), 
		@numWeightValue = ISNULL(I.fltWeight,0),
		@numQtyValue = @numQty,
		@Zip = AD.vcPostalCode ,
        @numCountryID = CASE WHEN ISNULL(OM.tintShipToType,0) IN (0,1) THEN AD.numCountry ELSE OppAD.numShipCountry END, 
        @numStateID = CASE WHEN ISNULL(OM.tintShipToType,0) IN (0,1) THEN AD.numState ELSE OppAD.numShipState END,
        @numItemID = OBDI.numItemCode
         --,@numShipClass=I.numShipClass
      FROM OpportunityMaster OM 
      INNER JOIN OpportunityItems OI ON OM.numOppId = OI.numOppId
      LEFT JOIN dbo.OpportunityBizDocs OBD ON OBD.numOppId = OM.numOppId
      LEFT JOIN dbo.OpportunityBizDocItems OBDI ON OBD.numOppBizDocsId = OBDI.numOppBizDocID  AND OI.numoppitemtCode = OBDI.numOppItemID
      INNER JOIN Item I ON I.numItemCode = OI.numItemCode AND I.numDomainID = OM.numDomainID
      LEFT JOIN AddressDetails AD ON AD.numDomainID = OM.numDomainId AND AD.numRecordID = OM.numDivisionId AND AD.tintAddressOf = 2 AND AD.tintAddressType = 2 AND AD.bitIsPrimary=1
      LEFT JOIN OpportunityAddress OppAD ON OppAD.numOppID = OM.numOppID 
      WHERE 1=1
      AND OM.numOppId = @numOrderID
      AND (OBDI.numOppBizDocItemID = @numBizDocItemID OR ISNULL(@numBizDocItemID,0) = 0)
      AND I.numDomainID = @numDomainID

PRINT @numWeightValue             
PRINT 'PRICE : ' + CONVERT(VARCHAR(100),@numPriceValue)
PRINT 'Weight : ' + CONVERT(VARCHAR(100),@numWeightValue)
PRINT 'Quantity : ' + CONVERT(VARCHAR(100),@numQtyValue)
PRINT 'numCountryID : ' + CONVERT(VARCHAR(100),@numCountryID)
PRINT 'numStateID : ' + CONVERT(VARCHAR(100),@numStateID)
PRINT 'numItemCode : ' + CONVERT(VARCHAR(100),@numItemID)
PRINT '@numSourceCompanyID : ' + CONVERT(VARCHAR(100),@numSourceCompanyID)

PRINT @Zip
IF ISNUMERIC(@Zip) = 1 
BEGIN
	SET @numZip = CONVERT(NUMERIC(10),@Zip)
END
ELSE
BEGIN
	SET @numZip = 0
END
PRINT @numZip

      
SELECT DISTINCT @numZip,(CASE WHEN ISNUMERIC(SRSL.vcFromZip) = 1 THEN CONVERT(NUMERIC(18),SRSL.vcFromZip) ELSE 0 END),(CASE WHEN ISNUMERIC(SRSL.vcToZip) = 1 THEN CONVERT(NUMERIC(18),SRSL.vcToZip) ELSE 0 END),SR.*,SRC.*,
				ISNULL(SST1.vcServiceName,'') AS [Domestic],
				ISNULL(SST2.vcServiceName,'') AS [International],
				SST1.numShippingCompanyID AS [numShippingCompanyID1],
				SST2.numShippingCompanyID AS [numShippingCompanyID2]
FROM dbo.ShippingLabelRuleMaster SR 
INNER JOIN dbo.ShippingLabelRuleChild SRC ON SR.numShippingRuleID = SRC.numShippingRuleID                                                     
LEFT JOIN dbo.ShippingRuleStateList SRSL ON SR.numShippingRuleID = SRSL.numRuleID AND SR.numDomainID = SRSL.numDomainID
LEFT JOIN dbo.PromotionOfferItems POI ON SR.numShippingRuleID = POI.numProId
INNER JOIN ShippingServiceTypes SST1 ON SST1.intNsoftEnum = SRC.numDomesticShipID
INNER JOIN ShippingServiceTypes SST2 ON SST2.intNsoftEnum = SRC.numInternationalShipID
WHERE 1=1
AND SR.numDomainID = @numDomainID
AND  
(
	-- Check MarketPlace is Available or not for shipping
	(
		(SR.numSourceCompanyID = @numSourceCompanyID OR ISNULL(SR.numSourceCompanyID,0) = 0 OR ISNULL(@numSourceCompanyID,0) = 0)
	)
	AND	
	--Items Affected
	(
		(intItemAffected = 1 AND POI.numValue = @numItemID ) -- Priority 1
		OR 
		(intItemAffected = 2 AND POI.numValue = @numShipClass ) -- Priority 2
		OR 
		(intItemAffected = 3) -- Priority 3
	)
	
	--Shipping Based On
    AND 
    (
    1 = (CASE WHEN SR.tintShipBasedOn = 1 THEN CASE WHEN @numWeightValue BETWEEN SRC.numFromValue AND SRC.numToValue THEN 1 ELSE 0 END 
    	 	  WHEN SR.tintShipBasedOn = 2 THEN CASE WHEN @numPriceValue BETWEEN SRC.numFromValue AND SRC.numToValue THEN 1 ELSE 0 END 
    	 	  WHEN SR.tintShipBasedOn = 3 THEN CASE WHEN @numQtyValue BETWEEN SRC.numFromValue AND SRC.numToValue THEN 1 ELSE 0 END 
				 ELSE 0
            END)
    )
    
  AND
    (
	1 = (CASE WHEN SR.tintType = 1 AND SRSL.numRuleStateID IS NOT NULL THEN 
								CASE WHEN SR.numShippingRuleID  IN 
									  ( SELECT TOP 1 numRuleID FROM dbo.ShippingRuleStateList SRSL WHERE numDomainID = @numDomainID AND SRSL.numRuleID=SR.numShippingRuleID
										AND (SRSL.numCountryID = @numCountryID OR @numCountryID = 0)
										AND (SRSL.numStateID = @numStateID OR @numStateID = 0 )
										AND 
											(
											(
											 @numZip >= (CASE WHEN ISNUMERIC(SRSL.vcFromZip) = 1 THEN CONVERT(NUMERIC(18),SRSL.vcFromZip) ELSE 0 END)             
											 AND 
											 @numZip <= (CASE WHEN ISNUMERIC(SRSL.vcToZip) = 1 THEN CONVERT(NUMERIC(18),SRSL.vcToZip) ELSE 0 END) ) 
											 OR 
											 ISNULL(@numZip,0) = 0
											 )
										ORDER BY (CASE WHEN LEN(ISNULL(SRSL.vcToZip,0))>=5 THEN 1 WHEN ISNULL(SRSL.numStateID,0) > 0 THEN 2 WHEN ISNULL(SRSL.numCountryID,0) > 0 THEN 3 END )
										) THEN 1 ELSE 0 END
			  WHEN SR.tintType = 2 AND SRSL.numRuleStateID IS NOT NULL  THEN 
								CASE WHEN SR.numShippingRuleID not IN 
									  ( SELECT TOP 1 numRuleID FROM dbo.ShippingRuleStateList SRSL WHERE numDomainID = @numDomainID AND SRSL.numRuleID=SR.numShippingRuleID
										AND (SRSL.numCountryID = @numCountryID OR @numCountryID = 0)
										AND (SRSL.numStateID = @numStateID OR @numStateID = 0 )
										AND 
										(
											(
											 @numZip >= (CASE WHEN ISNUMERIC(SRSL.vcFromZip) = 1 THEN CONVERT(NUMERIC(18),SRSL.vcFromZip) ELSE 0 END)             
											 AND 
											 @numZip <= (CASE WHEN ISNUMERIC(SRSL.vcToZip) = 1 THEN CONVERT(NUMERIC(18),SRSL.vcToZip) ELSE 0 END) ) 
											 OR 
											  ISNULL(@numZip,0) = 0
											 )
										ORDER BY (CASE WHEN LEN(ISNULL(SRSL.vcToZip,0))>=5 THEN 1 
													   WHEN ISNULL(SRSL.numStateID,0) > 0 THEN 2 WHEN ISNULL(SRSL.numCountryID,0) > 0 THEN 3 END )
										) THEN 1 ELSE 0 END
			  ELSE 1
		END																	
    )       
)
) 
ORDER BY SR.numSourceCompanyID DESC ,intItemAffected,SR.numShippingRuleID 

END 