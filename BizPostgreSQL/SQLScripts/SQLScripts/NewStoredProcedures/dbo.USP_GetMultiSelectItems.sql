GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetMultiSelectItems')
DROP PROCEDURE USP_GetMultiSelectItems
GO
CREATE PROCEDURE [dbo].[USP_GetMultiSelectItems]    
@numDomainID NUMERIC(18,0),
	@numItemCodes VARCHAR(8000) = '',
	@tintOppType TINYINT
AS    
BEGIN
 
	SELECT 
		I.numItemCode
		,I.vcItemName AS ItemName
		,I.charItemType
		,dbo.fn_GetItemAttributes(@numDomainID,I.numItemCode,0) AS Attr
		,(SELECT SUM(numOnHand)  FROM WareHouseItems WHERE numItemID = I.numItemCode)AS numOnHand
		,(SELECT SUM(numAllocation) FROM WareHouseItems WHERE numItemID = I.numItemCode)AS numAllocation
		,ISNULL(I.numSaleUnit,0) AS numSaleUnit
		,dbo.fn_UOMConversion((CASE WHEN  @tintOppType = 1 THEN ISNULL(I.numSaleUnit,0) ELSE ISNULL(I.numPurchaseUnit,0) END),I.numItemCode,I.numDomainID,ISNULL(I.numBaseUnit,0)) fltUOMConversionFactor
FROM 
	Item I
	WHERE 
		I.numItemCode IN (SELECT  Id FROM dbo.SplitIDs(@numItemCodes, ','))
END
GO