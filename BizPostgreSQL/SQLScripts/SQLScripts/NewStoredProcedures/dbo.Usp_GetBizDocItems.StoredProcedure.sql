GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='Usp_GetBizDocItems')
DROP PROCEDURE Usp_GetBizDocItems
GO
CREATE PROCEDURE [dbo].[Usp_GetBizDocItems]  
(
	@numOppID			NUMERIC(9,0),
	@numOppBizDocID		NUMERIC(9,0),
	@numDomainID		NUMERIC(9,0)
)                      
AS
BEGIN
	
	  SELECT    OBDI.numOppBizDocID,
				OBDI.numOppItemID,
				OBDI.numItemCode,
				OBDI.numUnitHour,
				OBDI.monPrice,
				OBDI.monTotAmount,
				OBDI.vcItemDesc,
				OBDI.numWarehouseItmsID,
				OBDI.vcType,
				OBDI.vcAttributes,
				OBDI.bitDropShip,
				OBDI.bitDiscountType,
				OBDI.fltDiscount,
				OBDI.monTotAmtBefDiscount,
				OBDI.vcNotes,
				OBDI.vcTrackingNo,
				OBDI.bitEmbeddedCost,
				OBDI.monEmbeddedCost,
				OBDI.monShipCost,
				OBDI.vcShippingMethod,
				OBDI.dtDeliveryDate,
				OBDI.dtRentalStartDate,
				OBDI.dtRentalReturnDate,
				OBDI.tintTrackingStatus,
				ISNULL(OI.vcItemDesc,'') AS [vcItemDesc],
				ISNULL(OI.vcItemName,'') AS [vcItemName]
	  FROM dbo.OpportunityBizDocItems OBDI
	  JOIN dbo.OpportunityItems OI ON OI.numoppitemtCode = OBDI.numOppItemID
	  JOIN dbo.OpportunityMaster OM ON OM.numOppId = OI.numOppID
	  WHERE 1=1
	  AND OM.numOppId = @numOppID
	  AND numOppBizDocID = @numOppBizDocID		
END
