SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getpromotiondrulefororder')
DROP PROCEDURE usp_getpromotiondrulefororder
GO
CREATE PROCEDURE [dbo].[USP_GetPromotiondRuleForOrder]
	 @numDomainID NUMERIC(18,0)
	,@numSubTotal FLOAT = 0
	,@numDivisionID NUMERIC(18,0)
	,@numSiteID NUMERIC(18,0)
	,@numPromotionID NUMERIC(18,0)
AS
BEGIN	
	DECLARE @numRelationship NUMERIC(18,0) = 0
	DECLARE @numProfile NUMERIC(18,0) = 0

	IF ISNULL(@numDivisionID,0) > 0
	BEGIN
		SELECT 
			@numRelationship=ISNULL(numCompanyType,0)
			,@numProfile=ISNULL(vcProfile,0) 
		FROM 
			DivisionMaster 
		INNER JOIN 
			CompanyInfo 
		ON 
			DivisionMaster.numCompanyID=CompanyInfo.numCompanyId 
		WHERE 
			numDivisionID=@numDivisionID
	END
	ELSE IF ISNULL(@numSiteID,0) > 0
	BEGIN
		SELECT
			@numRelationship=ISNULL(numRelationshipId,0)
			,@numProfile=ISNULL(numProfileId,0)
		FROM
			eCommerceDTL
		WHERE
			numSiteID=@numSiteID
	END



	DECLARE @numProID AS NUMERIC

	SET @numProID = (SELECT TOP 1 
						PO.numProId		 
					FROM 
						PromotionOffer PO	
					INNER JOIN 
						PromotionOfferOrganizations PORG
					ON 
						PO.numProId = PORG.numProId
					WHERE 
						PO.numDomainId = @numDomainID 
						AND ISNULL(PO.IsOrderBasedPromotion,0) = 1 
						AND (PO.numProId=@numPromotionID OR ISNULL(bitRequireCouponCode,0) = 0)
						AND ISNULL(bitEnabled,0) = 1
						AND numRelationship=@numRelationship
						AND ISNULL(bitUseForCouponManagement,0) = 0
						AND numProfile=@numProfile
						AND  1 = (CASE 
									WHEN ISNULL(PO.bitNeverExpires,0) = 1
									THEN 1
									WHEN ISNULL(PO.bitNeverExpires,0) = 0 
									THEN 
										CASE WHEN (PO.dtValidFrom <= GETUTCDATE() AND PO.dtValidTo >= GETUTCDATE())
									THEN 1	
									ELSE 0 END
								ELSE 0 END )
					ORDER BY
						(CASE WHEN PO.numProId=ISNULL(@numPromotionID,0) THEN 1 ELSE 0 END) DESC
				)


	SELECT * FROM PromotionOffer WHERE numProId = @numProID 
	 	
	SELECT ROW_NUMBER() OVER(ORDER BY numProOfferOrderID ASC) AS RowNum, numOrderAmount,fltDiscountValue FROM PromotionOfferOrder WHERE numPromotionID = @numProID

END 
GO