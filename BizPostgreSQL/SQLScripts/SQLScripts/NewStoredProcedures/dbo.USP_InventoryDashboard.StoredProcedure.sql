--exec USP_INVENTORYDASHBOARD @numDomainID=72,@PeriodFrom='01/Jan/2008',@PeriodTo='31/Dec/2008',@Type=2,@Top=2
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_InventoryDashboard')
DROP PROCEDURE USP_InventoryDashboard
GO
CREATE PROCEDURE USP_InventoryDashboard
(@numDomainID int,
@PeriodFrom datetime,
@PeriodTo datetime,
@Type int,
@Top int)
as
begin
declare @Sql varchar(500);

create table #MostSold		
		(Item varchar(250),SalesUnit FLOAT);

if @type=1 -- Most Sold
	begin
		insert into #MostSold		
		select  vcItemName + ',' + ItemClass as Item, sum(numUnitHour) as SalesUnit 
		from VIEW_INVENTORYOPPSALES where numDomainID=@numDomainID
		 and OppDate between @PeriodFrom and @PeriodTo
		 group by vcItemName + ',' + ItemClass 

		set @Sql ='select top ' +  cast( @Top as varchar) + ' * from #MostSold order by SalesUnit Desc'
		exec (@SQl)
	end
else if @type=2 --- Highest Avg Cost
	begin				
		insert into #MostSold		
		select  vcItemName + ',' + ItemClass as Item, Max(AvgCost) as AvgCost 
		from VIEW_INVENTORYOPPSALES where numDomainID=@numDomainID
		 and OppDate between @PeriodFrom and @PeriodTo
		 group by vcItemName + ',' + ItemClass 

		set @Sql ='select top ' +  cast( @Top as varchar) + ' * from #MostSold order by SalesUnit Desc'
		exec (@SQl)
	end
else if @type=3 --- Highest Cogs
	begin
				
		insert into #MostSold		
		select  vcItemName + ',' + ItemClass as Item, sum(COGS) as COGS 
		from VIEW_INVENTORYOPPSALES where numDomainID=@numDomainID
		 and OppDate between @PeriodFrom and @PeriodTo
		 group by vcItemName + ',' + ItemClass 

		set @Sql ='select top ' +  cast( @Top as varchar) + ' * from #MostSold order by SalesUnit Desc'
		exec (@SQl)
	end
else if @type=4 --- Highest Profit
	begin
				
		insert into #MostSold		
		select  vcItemName + ',' + ItemClass as Item, sum(Profit) as Profit 
		from VIEW_INVENTORYOPPSALES where numDomainID=@numDomainID
		 and OppDate between @PeriodFrom and @PeriodTo
		 group by vcItemName + ',' + ItemClass 

		set @Sql ='select top ' +  cast( @Top as varchar) + ' * from #MostSold order by SalesUnit Desc'
		exec (@SQl)
	end
else if @type=5 --- Highest Return
	begin
				
		insert into #MostSold		
		select  vcItemName + ',' + ItemClass as Item, sum(SalesReturn) as [Return ]
		from VIEW_INVENTORYOPPSALES where numDomainID=@numDomainID
		 and OppDate between @PeriodFrom and @PeriodTo
		 group by vcItemName + ',' + ItemClass 

		set @Sql ='select top ' +  cast( @Top as varchar) + ' * from #MostSold order by SalesUnit Desc'
		exec (@SQl)
	end

	drop table #MostSold;
end