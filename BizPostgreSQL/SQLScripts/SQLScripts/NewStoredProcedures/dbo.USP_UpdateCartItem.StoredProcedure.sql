
/****** Object:  StoredProcedure [dbo].[USP_UpdateCartItem]    Script Date: 11/08/2011 17:49:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_UpdateCartItem' ) 
                    DROP PROCEDURE USP_UpdateCartItem
                    Go
CREATE  PROCEDURE [dbo].[USP_UpdateCartItem]
(
	@numUserCntId numeric(9,0),
	@numDomainId numeric(9,0),
	@vcCookieId varchar(100),
	@strXML text,
	@postselldiscount INT=0,
	@numSiteID NUMERIC(18,0),
	@vcPreferredPromotions VARCHAR(MAX)=''
)
AS
BEGIN
	IF convert(varchar(10),@strXML) <>''
	BEGIN
		DECLARE @i INT
		EXEC sp_xml_preparedocument @i OUTPUT, @strXML 
		
		IF @numUserCntId <> 0
		BEGIN
			UPDATE 
				CartItems
			SET 
				CartItems.monPrice = ox.monPrice
				,CartItems.numUnitHour = ox.numUnitHour
				,CartItems.monTotAmtBefDiscount = ox.monTotAmtBefDiscount
				,CartItems.monTotAmount = ox.monTotAmount
				,CartItems.bitDiscountType=ISNULL(ox.bitDiscountType,0)
				,CartItems.vcShippingMethod=ox.vcShippingMethod
				,CartItems.numServiceTypeID=ox.numServiceTypeID
				,CartItems.decShippingCharge=ox.decShippingCharge
				,CartItems.tintServiceType=ox.tintServiceType
				,CartItems.numShippingCompany =ox.numShippingCompany
				,CartItems.dtDeliveryDate =ox.dtDeliveryDate
				,CartItems.fltDiscount =ox.fltDiscount
				,CartItems.vcCookieId = ox.vcCookieId
				,CartItems.vcCoupon=(CASE WHEN CartItems.vcCoupon<>'' THEN CartItems.vcCoupon ELSE ox.vcCoupon END)
			FROM 
				OPENXML(@i, '/NewDataSet/Table1',2)
			WITH 
			(
				numDomainId NUMERIC(9,0),numUserCntId NUMERIC(9,0),vcCookieId VARCHAR(200), numCartID NUMERIC(18,0),numItemCode NUMERIC(9,0), monPrice DECIMAL(20,5), numUnitHour FLOAT,
				monTotAmtBefDiscount DECIMAL(20,5),monTotAmount DECIMAL(20,5) ,bitDiscountType BIT,vcShippingMethod VARCHAR(200),numServiceTypeID NUMERIC(9,0),decShippingCharge DECIMAL(20,5),
				tintServiceType TINYINT,numShippingCompany NUMERIC(9,0),dtDeliveryDate DATETIME,fltDiscount DECIMAL(18,2),vcCoupon VARCHAR(200) 
			) ox
			WHERE 
				CartItems.numDomainId = ox.numDomainId
				AND CartItems.numUserCntId = ox.numUserCntId
				AND CartItems.numItemCode = ox.numItemCode
				AND CartItems.numCartId = ox.numCartID
		END 
		ELSE
		BEGIN
			UPDATE 
				CartItems
			SET 
				CartItems.monPrice = ox.monPrice
				,CartItems.numUnitHour = ox.numUnitHour
				,CartItems.monTotAmtBefDiscount = ISNULL(ox.numUnitHour,0) * ISNULL(decUOMConversionFactor,1) * ISNULL(ox.monPrice,0)
				,CartItems.monTotAmount = (CASE 
												WHEN ISNULL(ox.fltDiscount,0) > 0
												THEN (CASE 
														WHEN ISNULL(ox.bitDiscountType,0) = 1 
														THEN (CASE 
																WHEN (ISNULL(ox.numUnitHour,0) * ISNULL(decUOMConversionFactor,1) * ISNULL(ox.monPrice,0)) - ox.fltDiscount >= 0
																THEN (ISNULL(ox.numUnitHour,0) * ISNULL(decUOMConversionFactor,1) * ISNULL(ox.monPrice,0)) - ox.fltDiscount
																ELSE 0
															END)
														ELSE (ISNULL(ox.numUnitHour,0) * ISNULL(decUOMConversionFactor,1)) * (ISNULL(ox.monPrice,0) - (ISNULL(ox.monPrice,0) * (ox.fltDiscount/100)))
													END)
												ELSE ISNULL(ox.numUnitHour,0) * ISNULL(decUOMConversionFactor,1) * ISNULL(ox.monPrice,0)
											END)
				,CartItems.bitDiscountType =ISNULL(ox.bitDiscountType,0)
				,CartItems.vcShippingMethod =ox.vcShippingMethod
				,CartItems.numServiceTypeID =ox.numServiceTypeID
				,CartItems.decShippingCharge =ox.decShippingCharge
				,CartItems.tintServiceType =ox.tintServiceType
				,CartItems.numShippingCompany =ox.numShippingCompany
				,CartItems.dtDeliveryDate =ox.dtDeliveryDate
				,CartItems.fltDiscount =ox.fltDiscount
				,CartItems.vcCoupon=(CASE WHEN CartItems.vcCoupon<>'' THEN CartItems.vcCoupon ELSE ox.vcCoupon END)
			FROM 
				OpenXml(@i, '/NewDataSet/Table1',2)
			WITH 
			(
				numDomainId NUMERIC(9,0),numUserCntId NUMERIC(9,0),vcCookieId VARCHAR(200), numCartID NUMERIC(18,0),numItemCode NUMERIC(9,0), monPrice DECIMAL(20,5), numUnitHour FLOAT,
				monTotAmtBefDiscount DECIMAL(20,5),monTotAmount DECIMAL(20,5) ,bitDiscountType BIT,vcShippingMethod VARCHAR(200),numServiceTypeID NUMERIC(9,0),decShippingCharge DECIMAL(20,5),
				tintServiceType TINYINT,numShippingCompany NUMERIC(9,0),dtDeliveryDate DATETIME,fltDiscount DECIMAL(18,2),vcCoupon VARCHAR(200) 
			) ox
			WHERE 
				CartItems.numDomainId = ox.numDomainId 
				AND CartItems.numUserCntId = 0 
				AND CartItems.numItemCode = ox.numItemCode
				AND CartItems.vcCookieId = ox.vcCookieId
				AND CartItems.numCartId = ox.numCartID
		END

		exec sp_xml_removedocument @i


		IF (SELECT COUNT(*) FROM PromotionOffer WHERE numDomainId=@numDomainId AND ISNULL(bitEnabled,0)=1) > 0
		BEGIN
			EXEC USP_PromotionOffer_ApplyItemPromotionToECommerce @numDomainID,@numUserCntId,@numSiteID,@vcCookieId,0,''
		END
	END
END
