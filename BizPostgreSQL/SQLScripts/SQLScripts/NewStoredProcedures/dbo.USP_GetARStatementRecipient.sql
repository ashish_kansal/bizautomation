GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
       
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetARStatementRecipient')
DROP PROCEDURE USP_GetARStatementRecipient
GO
CREATE PROCEDURE [dbo].[USP_GetARStatementRecipient]
(
	@numDomainID NUMERIC(18,0)
	,@vcDivIDs VARCHAR(MAX)
)
AS
BEGIN
	DECLARE @numARContactPosition NUMERIC(18,0)

	SELECT 
		@numARContactPosition=ISNULL(numARContactPosition,0) 
	FROM 
		Domain 
	WHERE 
		numDomainID=@numDomainID

	SELECT
		DM.numDivisionID
		,ISNULL(CI.vcCompanyName,'') vcCompanyName
		,numContactID
		,CONCAT(ISNULL(ACI.vcFirstName,'-'),' ',ISNULL(ACI.vcFirstName,'-')) vcContactName
		,ISNULL(ACI.vcEmail,'') vcContactEmail
	FROM
		DivisionMaster DM
	INNER JOIN
		CompanyInfo CI
	ON
		DM.numCompanyID = CI.numCompanyID
	LEFT JOIN
		AdditionalContactsInformation ACI
	ON
		DM.numDivisionID = ACI.numDivisionId
	WHERE
		DM.numDomainID = @numDomainID
		AND DM.numDivisionID IN (SELECT ID FROM dbo.SplitIDs(@vcDivIDs,','))
		AND 1 = (CASE 
					WHEN ISNULL(@numARContactPosition,0) > 0 THEN (CASE WHEN ISNULL(ACI.vcPosition,0) = @numARContactPosition THEN 1 ELSE 0 END)
					ELSE (CASE WHEN ISNULL(ACI.bitPrimaryContact,0) = 1 THEN 1 ELSE 0 END)
				END)
END
GO