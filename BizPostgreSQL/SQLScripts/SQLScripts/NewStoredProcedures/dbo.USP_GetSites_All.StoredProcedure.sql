
GO
/****** Object:  StoredProcedure [dbo].[USP_GetSites_All]    Script Date: 08/08/2009 12:43:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetSites_All')
DROP PROCEDURE USP_GetSites_All
GO
CREATE PROCEDURE [dbo].[USP_GetSites_All]
AS
  SELECT [numSiteID],
         [vcSiteName],
         [vcDescription],
         [vcHostName],
         [numDomainID],
         numCreatedBy,
         vcLiveURL
  FROM   Sites
  WHERE numDomainID>0

