GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_EmailBroadcastConfiguration_Save' ) 
    DROP PROCEDURE USP_EmailBroadcastConfiguration_Save
GO

CREATE PROCEDURE USP_EmailBroadcastConfiguration_Save  
(  
 @numDomainID NUMERIC,  
 @vcAWSAccessKey VARCHAR(100),  
 @vcAWSSecretKey VARCHAR(100),
 @vcAWSDomain VARCHAR(200),
 @vcFrom VARCHAR(100)
   
)  
AS  
BEGIN  
-- SET NOCOUNT ON added to prevent extra result sets from  
-- interfering with SELECT statements.  
SET NOCOUNT ON;  

IF EXISTS (SELECT * FROM EmailBroadcastConfiguration WHERE numDomainId = @numDomainID)
BEGIN
	UPDATE
		EmailBroadcastConfiguration
	SET
		vcAWSAccessKey=@vcAWSAccessKey,
		vcAWSSecretKey=@vcAWSSecretKey,
		vcAWSDomain=@vcAWSDomain,
		vcFrom= @vcFrom
	WHERE
		numDomainId = @numDomainID
END
ELSE
BEGIN
	INSERT INTO EmailBroadcastConfiguration
	(
		numDomainId,
		vcFrom,
		vcAWSDomain,
		vcAWSAccessKey,
		vcAWSSecretKey
	)
	VALUES
	(
		@numDomainID,
		@vcFrom,
		@vcAWSDomain,
		@vcAWSAccessKey,
		@vcAWSSecretKey
	)
END

END  

