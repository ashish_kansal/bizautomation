
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetPayrollHistory')
DROP PROCEDURE USP_GetPayrollHistory
GO
CREATE PROCEDURE [dbo].[USP_GetPayrollHistory]
(
    @numDomainId AS NUMERIC(9) = 0,
    @numPayrollHeaderID AS NUMERIC(9)=0,
    @ClientTimeZoneOffset INT
)
AS
  BEGIN

 SELECT PH.numPayrollHeaderID,numPayrolllReferenceNo,dtFromDate,dtToDate,dtPaydate,
  PD.numPayrollDetailID,UM.numUserId,Isnull(ADC.vcfirstname + ' ' + adc.vclastname,'-') vcUserName,
   adc.numcontactid AS numUserCntID,Isnull(um.vcEmployeeId,'') AS vcEmployeeId,
   ISNULL(PD.monTotalAmt,0) AS monTotalAmt,ISNULL(numCheckStatus,0) AS numCheckStatus,
   CASE ISNULL(numCheckStatus,0) WHEN 0 THEN 'Not Approved'
								 WHEN 1 THEN 'Approved'
								 WHEN 2 THEN 'Paid' END AS vcCheckStatus,
   ISNULL(PD.monDeductions,0) AS monDeductions,adc.numDivisionID,PD.numUserCntID,
   ISNULL(CH.numCheckNo,0) AS numCheckNo
 FROM  PayrollHeader PH JOIN dbo.PayrollDetail PD ON PH.numPayrollHeaderID=PD.numPayrollHeaderID
 JOIN dbo.UserMaster UM ON UM.numUserDetailId=PD.numUserCntID
 JOIN dbo.AdditionalContactsInformation adc ON adc.numcontactid = um.numuserdetailid
 LEFT JOIN CheckHeader CH ON CH.numReferenceID=PD.numPayrollDetailID AND CH.tintReferenceType=11
 WHERE UM.numDomainId=@numDomainId 

END
GO