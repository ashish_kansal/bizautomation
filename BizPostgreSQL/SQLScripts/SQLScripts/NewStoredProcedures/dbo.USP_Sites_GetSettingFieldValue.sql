GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
            
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_Sites_GetSettingFieldValue')
DROP PROCEDURE USP_Sites_GetSettingFieldValue
GO
CREATE PROCEDURE [dbo].[USP_Sites_GetSettingFieldValue]    
(
	@numDomainID NUMERIC(18,0)
	,@numSiteID NUMERIC(18,0)
	,@vcFieldName VARCHAR(300)
)
AS
BEGIN
	IF @vcFieldName = 'tintWarehouseAvailability'
	BEGIN
		SELECT ISNULL(tintWarehouseAvailability,1)  FROM eCommerceDTL WHERE numDomainId=@numDomainID AND numSiteId = @numSiteID
	END
	ELSE IF @vcFieldName='bitDisplayQtyAvailable'
	BEGIN
		SELECT ISNULL(bitDisplayQtyAvailable,0)  FROM eCommerceDTL WHERE numDomainId=@numDomainID AND numSiteId = @numSiteID
	END
	ELSE IF @vcFieldName='bitElasticSearch'
	BEGIN
		SELECT ISNULL(bitElasticSearch,0)  FROM eCommerceDTL WHERE numDomainId=@numDomainID AND numSiteId = @numSiteID
	END
	ELSE
	BEGIN
		RAISERROR('FIELD_NOT_FOUND',16,1)
	END
END
