/****** Object:  StoredProcedure [dbo].[USP_UpdateMessageFlags]    Script Date: 07/26/2008 16:17:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_UpdateMessageFlags')
DROP PROCEDURE USP_UpdateMessageFlags
GO
CREATE PROCEDURE [dbo].[USP_UpdateMessageFlags]                
@vchEmailHstrID VARCHAR(MAX),
@bitRead AS BIT=0
as   
BEGIN
update  emailHistory set bitIsRead= @bitRead where numEmailHstrID IN(SELECT Items FROM Split(@vchEmailHstrID,',') WHERE Items<>'') 
END