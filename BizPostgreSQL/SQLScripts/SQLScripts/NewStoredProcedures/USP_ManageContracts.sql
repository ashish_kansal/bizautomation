
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ManageContracts')
DROP PROCEDURE USP_ManageContracts
GO
CREATE PROCEDURE [dbo].[USP_ManageContracts]
@numContractId AS NUMERIC(18,0),
@intType AS INT,
@numDomainID AS NUMERIC(18,0),
@numUserCntID AS NUMERIC(18,0),
@numDivisonId AS NUMERIC(18,0),
@numIncidents AS NUMERIC(18,0),
@numIncidentLeft AS NUMERIC(18,0),
@numHours AS NUMERIC(18,0)=0,
@numMinutes AS NUMERIC(18,0)=0,
@vcItemClassification AS VARCHAR(MAX)=NULL,
@numWarrantyDays AS NUMERIC(18,0)=NULL,
@vcNotes AS VARCHAR(MAX)=NULL
AS
BEGIN
BEGIN TRY
BEGIN TRAN
	
	DECLARE @timeLeft AS NUMERIC(18,0) = 0
	DECLARE @timeUsed AS NUMERIC(18,0) = 0
	IF(@numContractId>0)
	BEGIN
		IF(@intType=1)
		BEGIN
			SET @timeUsed = (SELECT TOP 1 timeUsed FROM Contracts WHERE numContractId=@numContractId)
			SET @timeLeft = ((@numHours*60*60) +(@numMinutes*60)) - @timeUsed
		END
		UPDATE 
			Contracts
		SET
			numIncidents=@numIncidents,
			numHours = @numHours,
			numMinutes = @numMinutes,
			vcItemClassification = @vcItemClassification,
			numWarrantyDays = @numWarrantyDays,
			vcNotes = @vcNotes,
			timeUsed=@timeUsed,
			timeLeft=@timeLeft
		WHERE
			numContractId=@numContractId
	END
	ELSE
	BEGIN
		IF((@intType=1 OR @intType=3) AND (SELECT COUNT(*) FROM Contracts WHERE numDivisonId=@numDivisonId AND intType=@intType)>0)
		BEGIN
			RAISERROR('Organization contract is already exist',0,0);
			RETURN
		END
		ELSE
		BEGIN
			SET @timeLeft = ((@numHours*60*60) +(@numMinutes*60))
			INSERT INTO Contracts(
				intType, 
				numDomainId, 
				numDivisonId, 
				numIncidents, 
				numIncidentLeft, 
				numHours, 
				numMinutes, 
				timeLeft, 
				vcItemClassification, 
				numWarrantyDays, 
				vcNotes, 
				numCreatedBy, 
				dtmCreatedOn, 
				timeUsed
			)VALUES(
				@intType,
				@numDomainId,
				@numDivisonId,
				@numIncidents,
				@numIncidents,
				@numHours,
				@numMinutes,
				@timeLeft,
				@vcItemClassification,
				@numWarrantyDays,
				@vcNotes,
				@numUserCntID,
				GETDATE(),
				@timeUsed
			)
			SET @numContractId=@@IDENTITY
			IF(@intType=1)
			BEGIN
				EXEC dbo.USP_UpdateNameTemplateValueForContracts 4,@numDomainID,@numContractId
			END
			IF(@intType=2)
			BEGIN
				EXEC dbo.USP_UpdateNameTemplateValueForContracts 5,@numDomainID,@numContractId
			END
			IF(@intType=3)
			BEGIN
				EXEC dbo.USP_UpdateNameTemplateValueForContracts 6,@numDomainID,@numContractId
			END
		END
	END
COMMIT TRAN 
END TRY 
BEGIN CATCH		
    IF ( @@TRANCOUNT > 0 ) 
    BEGIN
        ROLLBACK TRAN
    END

	DECLARE @ErrorMessage NVARCHAR(4000);
    DECLARE @ErrorSeverity INT;
    DECLARE @ErrorState INT;

    SELECT @ErrorMessage = ERROR_MESSAGE(), @ErrorSeverity = ERROR_SEVERITY(), @ErrorState = ERROR_STATE();

    RAISERROR (@ErrorMessage, @ErrorSeverity, @ErrorState);
END CATCH
END