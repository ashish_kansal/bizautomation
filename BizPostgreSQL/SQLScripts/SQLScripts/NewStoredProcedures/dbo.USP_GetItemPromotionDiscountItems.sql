SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetItemPromotionDiscountItems')
DROP PROCEDURE USP_GetItemPromotionDiscountItems
GO
CREATE PROCEDURE [dbo].[USP_GetItemPromotionDiscountItems]
	@numDomainID NUMERIC(18,0), 
	@numDivisionID NUMERIC(18,0),
	@numPromotionID NUMERIC(18,0),
	@numItemCode NUMERIC(18,0),
	@numWarehouseItemID NUMERIC(18,0),
	@numPageIndex INT,
	@numPageSize INT
AS
BEGIN
	DECLARE @tintDiscountBasedOn TINYINT
	DECLARE @tintDiscountType TINYINT
	DECLARE @fltDiscountValue FLOAT
	DECLARE @vcCurrency VARCHAR(10)
	DECLARE @numWarehouseID NUMERIC(18,0)
	DECLARE @tintItemCalDiscount TINYINT
	DECLARE @monDiscountedItemPrice DECIMAL(20,5)
	
	SELECT @vcCurrency=ISNULL(vcCurrency,'$') FROm Domain WHERE numDomainId=@numDomainID

	SELECT @numWarehouseID=ISNULL(numWareHouseID,0) FROM WarehouseItems WHERE numWarehouseItemID = @numWarehouseItemID
	
	SELECT 
		@tintDiscountBasedOn=tintDiscoutBaseOn
		,@tintDiscountType=tintDiscountType 
		,@fltDiscountValue=ISNULL(fltDiscountValue,0)
		,@tintItemCalDiscount=ISNULL(tintItemCalDiscount,0)
		,@monDiscountedItemPrice=ISNULL(monDiscountedItemPrice,0)
	FROM 
		PromotionOffer 
	WHERE 
		numProId = @numPromotionID 

	DECLARE @TempItem TABLE
	(
		numItemCode NUMERIC(18,0)
	)

	PRINT @tintDiscountBasedOn

	IF @tintDiscountBasedOn = 1 -- Based on individual item(s)
	BEGIN
		INSERT INTO @TempItem
		(
			numItemCode
		)
		SELECT DISTINCT
			numValue
		FROM
			PromotionOfferItems
		WHERE
			numProId=@numPromotionID
			AND tintRecordType = 6
			AND tintType = 1
	END
	ELSE IF @tintDiscountBasedOn = 2 -- Based on individual item classification(s)
	BEGIN
		INSERT INTO @TempItem
		(
			numItemCode
		)
		SELECT DISTINCT
			numItemCode
		FROM
			Item
		WHERE
			numDomainID=@numDomainID
			AND numItemClassification IN (SELECT
												numValue
											FROM
												PromotionOfferItems
											WHERE
												numProId=@numPromotionID
												AND tintRecordType = 6
												AND tintType = 2)
	END
	ELSE IF @tintDiscountBasedOn = 3  -- Based on related item(s)
	BEGIN
		INSERT INTO @TempItem
		(
			numItemCode
		)
		SELECT DISTINCT
			numItemCode
		FROM
			SimilarItems
		WHERE
			numParentItemCode =@numItemCode
	END
	ELSE IF @tintDiscountBasedOn = 6 
	BEGIN
		INSERT INTO @TempItem
		(
			numItemCode
		)
		SELECT DISTINCT
			numValue
		FROM
			PromotionOfferItems
		WHERE
			numProId=@numPromotionID
			AND tintRecordType = 6
			AND tintType = 4
	END

	SELECT
		COUNT(Item.numItemCode) OVER() AS TotalRecords
		,Item.numItemCode
		,ISNULL(Item.vcItemName,'') vcItemName
		,ISNULL(Item.txtItemDesc,'') txtItemDesc
		,(CASE 
			WHEN @tintDiscountBasedOn = 6
			THEN @monDiscountedItemPrice
			ELSE
				(CASE 
					WHEN (ISNULL(bitKitParent,0)=1 OR ISNULL(bitAssembly,0) = 1) AND ISNULL(bitCalAmtBasedonDepItems,0) = 1
					THEN [dbo].[GetCalculatedPriceForKitAssembly](@numDomainID,@numDivisionID,Item.numItemCode,1,@numWarehouseItemID,ISNULL(tintKitAssemblyPriceBasedOn,1),0,0,'',0,1)
					ELSE ISNULL(monListPrice,0)
				END)
		END) monListPrice
		,(CASE @tintDiscountType
			WHEN 1 THEN 0
			WHEN 2 THEN 1
			WHEN 3 THEN 0
		END) tintDiscountType
		,(CASE @tintDiscountType
			WHEN 1 THEN CONCAT(@fltDiscountValue,'%')
			WHEN 2 THEN CONCAT(@vcCurrency,@fltDiscountValue)
			WHEN 3 THEN '100%'
		END) vcDiscountType
		,(CASE @tintDiscountType
			WHEN 1 THEN @fltDiscountValue
			WHEN 2 THEN @fltDiscountValue
			WHEN 3 THEN 100
		END) fltDiscount
		,(CASE @tintDiscountType
			WHEN 1 THEN (CASE WHEN @fltDiscountValue > 0 THEN (CASE 
																	WHEN (ISNULL(bitKitParent,0)=1 OR ISNULL(bitAssembly,0) = 1) AND ISNULL(bitCalAmtBasedonDepItems,0) = 1
																	THEN [dbo].[GetCalculatedPriceForKitAssembly](@numDomainID,@numDivisionID,Item.numItemCode,1,@numWarehouseItemID,ISNULL(tintKitAssemblyPriceBasedOn,1),0,0,'',0,1)
																	ELSE ISNULL(monListPrice,0)
																END) - ((CASE 
																			WHEN (ISNULL(bitKitParent,0)=1 OR ISNULL(bitAssembly,0) = 1) AND ISNULL(bitCalAmtBasedonDepItems,0) = 1
																			THEN [dbo].[GetCalculatedPriceForKitAssembly](@numDomainID,@numDivisionID,Item.numItemCode,1,@numWarehouseItemID,ISNULL(tintKitAssemblyPriceBasedOn,1),0,0,'',0,1)
																			ELSE ISNULL(monListPrice,0)
																		END) * (@fltDiscountValue/100))  ELSE (CASE 
																													WHEN (ISNULL(bitKitParent,0)=1 OR ISNULL(bitAssembly,0) = 1) AND ISNULL(bitCalAmtBasedonDepItems,0) = 1
																													THEN [dbo].[GetCalculatedPriceForKitAssembly](@numDomainID,@numDivisionID,Item.numItemCode,1,@numWarehouseItemID,ISNULL(tintKitAssemblyPriceBasedOn,1),0,0,'',0,1)
																													ELSE ISNULL(monListPrice,0)
																												END) END)
			WHEN 2 THEN (CASE WHEN @fltDiscountValue > 0 THEN (CASE WHEN (CASE 
																				WHEN (ISNULL(bitKitParent,0)=1 OR ISNULL(bitAssembly,0) = 1) AND ISNULL(bitCalAmtBasedonDepItems,0) = 1
																				THEN [dbo].[GetCalculatedPriceForKitAssembly](@numDomainID,@numDivisionID,Item.numItemCode,1,@numWarehouseItemID,ISNULL(tintKitAssemblyPriceBasedOn,1),0,0,'',0,1)
																				ELSE ISNULL(monListPrice,0)
																			END) > @fltDiscountValue THEN ((CASE 
																												WHEN (ISNULL(bitKitParent,0)=1 OR ISNULL(bitAssembly,0) = 1) AND ISNULL(bitCalAmtBasedonDepItems,0) = 1
																												THEN [dbo].[GetCalculatedPriceForKitAssembly](@numDomainID,@numDivisionID,Item.numItemCode,1,@numWarehouseItemID,ISNULL(tintKitAssemblyPriceBasedOn,1),0,0,'',0,1)
																												ELSE ISNULL(monListPrice,0)
																											END) - @fltDiscountValue) ELSE 0 END)  ELSE (CASE 
																																							WHEN (ISNULL(bitKitParent,0)=1 OR ISNULL(bitAssembly,0) = 1) AND ISNULL(bitCalAmtBasedonDepItems,0) = 1
																																							THEN [dbo].[GetCalculatedPriceForKitAssembly](@numDomainID,@numDivisionID,Item.numItemCode,1,@numWarehouseItemID,ISNULL(tintKitAssemblyPriceBasedOn,1),0,0,'',0,1)
																																							ELSE ISNULL(monListPrice,0)
																																						END) END)
			WHEN 3 THEN 0
		END) monSalePrice
		,(CASE @tintDiscountType
			WHEN 1 THEN (CASE WHEN @fltDiscountValue > 0 THEN (CASE 
																	WHEN (ISNULL(bitKitParent,0)=1 OR ISNULL(bitAssembly,0) = 1) AND ISNULL(bitCalAmtBasedonDepItems,0) = 1
																	THEN [dbo].[GetCalculatedPriceForKitAssembly](@numDomainID,@numDivisionID,Item.numItemCode,1,@numWarehouseItemID,ISNULL(tintKitAssemblyPriceBasedOn,1),0,0,'',0,1)
																	ELSE ISNULL(monListPrice,0)
																END) - ((CASE 
																			WHEN (ISNULL(bitKitParent,0)=1 OR ISNULL(bitAssembly,0) = 1) AND ISNULL(bitCalAmtBasedonDepItems,0) = 1
																			THEN [dbo].[GetCalculatedPriceForKitAssembly](@numDomainID,@numDivisionID,Item.numItemCode,1,@numWarehouseItemID,ISNULL(tintKitAssemblyPriceBasedOn,1),0,0,'',0,1)
																			ELSE ISNULL(monListPrice,0)
																		END) * (@fltDiscountValue/100))  ELSE (CASE 
																													WHEN (ISNULL(bitKitParent,0)=1 OR ISNULL(bitAssembly,0) = 1) AND ISNULL(bitCalAmtBasedonDepItems,0) = 1
																													THEN [dbo].[GetCalculatedPriceForKitAssembly](@numDomainID,@numDivisionID,Item.numItemCode,1,@numWarehouseItemID,ISNULL(tintKitAssemblyPriceBasedOn,1),0,0,'',0,1)
																													ELSE ISNULL(monListPrice,0)
																												END) END)
			WHEN 2 THEN (CASE WHEN @fltDiscountValue > 0 THEN (CASE WHEN (CASE 
																				WHEN (ISNULL(bitKitParent,0)=1 OR ISNULL(bitAssembly,0) = 1) AND ISNULL(bitCalAmtBasedonDepItems,0) = 1
																				THEN [dbo].[GetCalculatedPriceForKitAssembly](@numDomainID,@numDivisionID,Item.numItemCode,1,@numWarehouseItemID,ISNULL(tintKitAssemblyPriceBasedOn,1),0,0,'',0,1)
																				ELSE ISNULL(monListPrice,0)
																			END) > @fltDiscountValue THEN ((CASE 
																												WHEN (ISNULL(bitKitParent,0)=1 OR ISNULL(bitAssembly,0) = 1) AND ISNULL(bitCalAmtBasedonDepItems,0) = 1
																												THEN [dbo].[GetCalculatedPriceForKitAssembly](@numDomainID,@numDivisionID,Item.numItemCode,1,@numWarehouseItemID,ISNULL(tintKitAssemblyPriceBasedOn,1),0,0,'',0,1)
																												ELSE ISNULL(monListPrice,0)
																											END) - @fltDiscountValue) ELSE 0 END)  ELSE (CASE 
																																							WHEN (ISNULL(bitKitParent,0)=1 OR ISNULL(bitAssembly,0) = 1) AND ISNULL(bitCalAmtBasedonDepItems,0) = 1
																																							THEN [dbo].[GetCalculatedPriceForKitAssembly](@numDomainID,@numDivisionID,Item.numItemCode,1,@numWarehouseItemID,ISNULL(tintKitAssemblyPriceBasedOn,1),0,0,'',0,1)
																																							ELSE ISNULL(monListPrice,0)
																																						END) END)
			WHEN 3 THEN 0
		END) monTotalAmount
		,numWareHouseItemID
		,ISNULL(Item.bitMatrix,0) bitMatrix
		,ISNULL(numItemGroup,0) numItemGroup
		,(CASE WHEN ISNULL(bitKitParent,0) = 1 THEN (CASE WHEN ISNULL((SELECT COUNT(*) FROM ItemDetails INNER JOIN Item IInner ON ItemDetails.numChildItemID=IInner.numItemCode WHERE numItemKitID=Item.numItemCode AND ISNULL(IInner.bitKitParent,0) = 1),0) > 0 THEN 1 ELSE 0 END) ELSE 0 END) bitHasChildKits
		,(CASE WHEN ISNULL(bitKitParent,0) = 1 OR ISNULL(bitAssembly,0) = 1 THEN ISNULL(bitCalAmtBasedonDepItems,0) ELSE 0 END) bitCalAmtBasedonDepItems
	FROM
		Item
	INNER JOIN
		@TempItem T1
	ON
		Item.numItemCode = T1.numItemCode
	OUTER APPLY
	(
		SELECT TOP 1
			numWareHouseItemID
		FROM
			WareHouseItems
		WHERE
			numItemID=Item.numItemCode
			AND (numWareHouseID=@numWarehouseID OR @numWarehouseItemID=0)
	) T2
	WHERE
		numDomainID=@numDomainID
	ORDER BY
		vcItemName
	OFFSET
		((@numPageIndex - 1) * @numPageSize) ROWS
	FETCH NEXT
		@numPageSize ROWS ONLY
END