GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_ManageNameTemplate' ) 
    DROP PROCEDURE USP_ManageNameTemplate
GO
CREATE PROCEDURE USP_ManageNameTemplate
    @numDomainID AS NUMERIC,
    @strItems TEXT,
    @tintMode AS TINYINT,
    @tintModuleID AS TINYINT 
AS 
BEGIN
    IF @tintMode = 0 
        BEGIN
			IF @tintModuleID=1 --SO/PO
			BEGIN
				/*insert if doesn't exist*/
            IF NOT EXISTS ( SELECT  * FROM    dbo.NameTemplate WHERE   numDomainID = @numDomainID AND tintModuleID=@tintModuleID) 
                BEGIN
                    INSERT  INTO dbo.NameTemplate ( numDomainID,vcModuleName,vcNameTemplate,tintModuleID,numSequenceId,numMinLength,numRecordID)
                            SELECT  @numDomainID,vcModuleName,vcNameTemplate,tintModuleID,numSequenceId,numMinLength,numRecordID
                            FROM    dbo.NameTemplate WHERE   numDomainID = 0 AND tintModuleID=@tintModuleID 	
                END
                
				SELECT numDomainID,vcModuleName,vcNameTemplate,tintModuleID,ISNULL(numSequenceId,1) AS numSequenceId,ISNULL(numMinLength,4) AS numMinLength,numRecordID 
						FROM dbo.NameTemplate WHERE numDomainID = @numDomainID AND tintModuleID=@tintModuleID 
			END
			ELSE IF @tintModuleID=2 --BizDocs
			BEGIN
			        SELECT NT.numDomainID,LD.numListItemID AS numRecordID,vcData AS vcModuleName,ISNULL(NT.vcNameTemplate,CASE WHEN LD.numListItemID=290 THEN 'PROI' ELSE UPPER(SUBSTRING(dbo.GetListIemName(LD.numListItemID),0,4)) END + '-') AS vcNameTemplate,ISNULL(NT.numSequenceId,1) AS numSequenceId,ISNULL(NT.numMinLength,4) AS numMinLength 
					FROM dbo.ListDetails LD LEFT JOIN NameTemplate NT ON LD.numListItemID=NT.numRecordID AND NT.numDomainID=@numDomainID AND NT.tintModuleID=@tintModuleID 
					WHERE LD.numListID=27 and (LD.constFlag=1 or LD.numDomainID=@numDomainID)
			END
        END

    IF @tintMode = 1 
        BEGIN
            DELETE  FROM dbo.NameTemplate WHERE numDomainID=@numDomainID AND tintModuleID=@tintModuleID 

            DECLARE @hDocItem INT
            IF CONVERT(VARCHAR(10), @strItems) <> '' 
                BEGIN
                    EXEC sp_xml_preparedocument @hDocItem OUTPUT, @strItems
            
                    INSERT  INTO dbo.NameTemplate ( numDomainID,vcModuleName,vcNameTemplate,tintModuleID,numSequenceId,numMinLength,numRecordID,dtCreatedDate)
                    
                            SELECT  @numDomainID,
                                    X.vcModuleName,
                                    X.vcNameTemplate,
                                    @tintModuleID,X.numSequenceId,
                                    X.numMinLength,X.numRecordID,GETUTCDATE()
                            FROM    ( SELECT    *
                                      FROM      OPENXML (@hDocItem, '/NewDataSet/Table', 2)
                                                WITH ( vcModuleName VARCHAR(50),vcNameTemplate VARCHAR(200),numSequenceId NUMERIC(9),numMinLength NUMERIC(9),numRecordID NUMERIC(9))
                                    ) X
                    EXEC sp_xml_removedocument @hDocItem
                END
            SELECT  ''
        END	
END



