SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ItemUOMConversion_Save')
DROP PROCEDURE dbo.USP_ItemUOMConversion_Save
GO
CREATE PROCEDURE [dbo].[USP_ItemUOMConversion_Save]
(
	@numDomainID NUMERIC(18,0),
	@numUserCntID NUMERIC(18,0),
	@numItemCode NUMERIC(18,0),
	@numSourceUOM NUMERIC(18,0),
	@numTargetUOM NUMERIC(18,0),
	@numTargetUnit FLOAT
)
AS 
BEGIN  
	INSERT INTO ItemUOMConversion
	(
		numDomainID,
		numItemCode,
		numSourceUOM,
		numTargetUOM,
		numTargetUnit,
		numCreatedBy,
		dtCreated
	)
	VALUES
	(
		@numDomainID,
		@numItemCode,
		@numSourceUOM,
		@numTargetUOM,
		@numTargetUnit,
		@numUserCntID,
		GETDATE()
	)
END