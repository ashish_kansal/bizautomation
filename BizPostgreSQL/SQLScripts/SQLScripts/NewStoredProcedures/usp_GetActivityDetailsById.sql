/****** Object:  StoredProcedure [dbo].[USP_CaseDetails]    Script Date: 07/26/2008 16:15:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON                     
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_GetActivityDetailsById')
DROP PROCEDURE usp_GetActivityDetailsById
GO
CREATE PROCEDURE [dbo].[usp_GetActivityDetailsById]  
@ActivityID AS NUMERIC(18,0)=0
AS
BEGIN
	SELECT ac.Duration AS timeInSecounds,ac.ActivityID,ac.StartDateTimeUtc,DATEADD(ss,ac.Duration,ac.StartDateTimeUtc) AS EndDateTime,
	ACI.numContactId,ac.Subject,isnull(dbo.fn_GetContactEmail(1,0,ACI.numContactId),'') AS AssignEmail,ACI.numDivisionId,ISNULL(bitTimeAddedToContract,0) AS bitTimeAddedToContract,
isnull(dbo.fn_GetContactName(ACI.numContactId),'') AS AssignName,
dbo.fn_SecondsConversion(ISNULL((SELECT TOP 1 ISNULL(CAST(timeLeft AS INT),0) FROM Contracts WHERE numDomainId=ACI.numDomainID AND numDivisonId=ACI.numDivisionId AND intType=1),0)) AS timeLeft,
ISNULL((SELECT TOP 1 ISNULL(CAST(timeLeft AS INT),0) FROM Contracts WHERE numDomainId=ACI.numDomainID AND numDivisonId=ACI.numDivisionId AND intType=1),0) AS timeLeftinSec
	From activity ac join             
activityresource ar on  ac.activityid=ar.activityid             
join resource res on ar.resourceid=res.resourceid                         
join AdditionalContactsInformation ACI on ACI.numContactId = res.numUserCntId                
 JOIN DivisionMaster Div                                                          
 ON ACI.numDivisionId = Div.numDivisionID                                   
 JOIN CompanyInfo  Comp                             
 ON Div.numCompanyID = Comp.numCompanyId
WHERE ac.ActivityID=@ActivityID
END