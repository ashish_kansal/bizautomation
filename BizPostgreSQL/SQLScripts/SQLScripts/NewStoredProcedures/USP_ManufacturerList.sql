GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ManufacturerList')
DROP PROCEDURE USP_ManufacturerList
GO
-- =============================================  
-- Modified by: <Author,,Sachin Sadhu>  
-- Create date: <Create Date,,24thDec2013>  
-- Description: <Description,,add 3 fields:vcBizDocImagePath,vcBizDocFooter,vcPurBizDocFooter>  
-- =============================================  
CREATE PROCEDURE [dbo].[USP_ManufacturerList]
(
          @numDomainID     AS NUMERIC(9)  = 0,
          @numRelationshipID   AS NUMERIC(9)  = 0,
		  @numShowOnWebField AS NUMERIC(18,0)=0,
		  @numRecordID AS NUMERIC(18,0)=0
)
AS
BEGIN
IF(@numRecordID=0)
BEGIN
	SELECT 
		C.vcCompanyName,C.txtComments,
		(select TOP 1
		  VcFileName
		  from dbo.GenericDocuments          
		  where numRecID=D.numDivisionID       
		  and  vcDocumentSection='A'   and numDomainID=@numDomainID    and vcfiletype IN ('.jpg','.jpeg','.png') 
		 )  AS VcFileName,
		 D.numDivisionID
	FROM 
		CompanyInfo AS C 
	LEFT JOIN
		DivisionMaster AS D
	ON
		C.numCompanyId=D.numCompanyID
	WHERE
		C.numCompanyType=@numRelationshipID AND C.numDomainID=@numDomainID AND
		ISNULL(( select TOP 1 Fld_Value from CFW_FLD_Values where  RecId=D.numDivisionID AND Fld_ID=@numShowOnWebField),0)=1
    ORDER BY C.vcCompanyName
END
ELSE
BEGIN
		SELECT 
		C.vcCompanyName,C.txtComments,
		(select TOP 1
		  VcFileName
		  from dbo.GenericDocuments          
		  where numRecID=@numRecordID
		  and  vcDocumentSection='A'   and numDomainID=@numDomainID    and vcfiletype IN ('.jpg','.jpeg','.png') 
		 )  AS VcFileName,
		 D.numDivisionID
	FROM 
		CompanyInfo AS C 
	LEFT JOIN
		DivisionMaster AS D
	ON
		C.numCompanyId=D.numCompanyID
	WHERE
		C.numDomainID=@numDomainID AND D.numDivisionID=@numRecordID
END
END
