GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetCustomerARStatementHtml')
DROP PROCEDURE USP_GetCustomerARStatementHtml
GO
CREATE PROCEDURE [dbo].[USP_GetCustomerARStatementHtml]
(
	@numDomainID NUMERIC(18,0)
	,@numDivisionID NUMERIC(18,0)
	,@dtToDate DATE
)
AS
BEGIN
	DECLARE @vcBluePayFormName VARCHAR(500)
	DECLARE @bitShowCardConnectLink BIT
	DECLARE @vcBluePayID VARCHAR(1000)
	DECLARE @vcBluePayPassword VARCHAR(1000)
	DECLARE @AuthoritativeSalesBizDocId INT
	DECLARE @vcCompanyName VARCHAR(300)
	DECLARE @bitTest BIT

	SELECT 
		@vcBluePayID=ISNULL(vcFirstFldValue,'')
		,@vcBluePayPassword=ISNULL(vcSecndFldValue ,'')
		,@bitTest = ISNULL(bitTest,0)
	FROM 
		PaymentGatewayDTLID 
	WHERE 
		numDomainID=@numDomainID 
		AND intPaymentGateWay=9 
		AND ISNULL(numSiteId,0) = 0

	SET @vcCompanyName = ISNULL((SELECT CompanyInfo.vcCompanyName FROM DivisionMaster INNER JOIN CompanyInfo ON DivisionMaster.numCompanyID=CompanyInfo.numCompanyId WHERE numDivisionID=@numDivisionID),'-')

	SELECT 
		@bitShowCardConnectLink= (CASE WHEN ISNULL(bitShowCardConnectLink,0)=1 AND ISNULL(vcBluePayFormName,'') <> '' THEN 1 ELSE 0 END)
		,@vcBluePayFormName=ISNULL(vcBluePayFormName,'')
	FROM 
		Domain 
	WHERE 
		numDomainId=@numDomainID
    
    SELECT 
		@AuthoritativeSalesBizDocId = ISNULL(numAuthoritativeSales,0)
    FROM 
		AuthoritativeBizDocs
    WHERE 
		numDomainId = @numDomainId

	IF @dtToDate IS NULL
		SET @dtToDate = GETUTCDATE()
    

	DECLARE @TEMP TABLE
	(
		ID INT IDENTITY(1,1)
		,numOppID NUMERIC(18,0)
		,vcPOppName VARCHAR(200)
		,numOppBizDocsId NUMERIC(18,0)
		,vcBizDocID VARCHAR(300)
		,monAmountPaid DECIMAL(20,2)
		,monAmountDue DECIMAL(20,2)
		,dtDueDate VARCHAR(50)
		,numPastDays NUMERIC(18,0)
		,vcCustomerPO# VARCHAR(100)
	)

	INSERT INTO @TEMP
	(
		numOppID
		,vcPOppName
		,numOppBizDocsId
		,vcBizDocID
		,monAmountPaid
		,monAmountDue
		,dtDueDate
		,numPastDays
		,vcCustomerPO#
	)
    SELECT 
		OB.numOppID,
		OM.vcPOppName,
        OB.[numOppBizDocsId],
        OB.[vcBizDocID],
        FORMAT(ISNULL(TablePayments.monPaidAmount,0) * OM.fltExchangeRate,'N2') monAmountPaid,
		FORMAT(ISNULL((OB.monDealAmount * OM.fltExchangeRate) - (ISNULL(TablePayments.monPaidAmount,0) * OM.fltExchangeRate),0),'N2') monAmountDue,
        CASE ISNULL(bitBillingTerms,0) 
            WHEN 1 THEN [dbo].[FormatedDateFromDate](DATEADD(DAY,CONVERT(INT,ISNULL((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(intBillingDays,0)), 0)),OB.dtFromDate),@numDomainId)
            WHEN 0 THEN [dbo].[FormatedDateFromDate](OB.[dtFromDate],@numDomainId)
        END AS dtDueDate,
		DATEDIFF(DAY,[dbo].[FormatedDateFromDate]((CASE ISNULL(bitBillingTerms,0) 
														WHEN 1 THEN [dbo].[FormatedDateFromDate](DATEADD(DAY,CONVERT(INT,ISNULL((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(intBillingDays,0)), 0)),OB.dtFromDate),@numDomainId)
														WHEN 0 THEN [dbo].[FormatedDateFromDate](OB.[dtFromDate],@numDomainId)
													END),@numDomainID),GETDATE()),
		ISNULL(OM.vcCustomerPO#,'') vcCustomerPO#
    FROM   
		OpportunityMaster OM
    INNER JOIN 
		DivisionMaster DM
    ON 
		OM.numDivisionId = DM.numDivisionID
	INNER JOIN 
		OpportunityBizDocs OB 
	ON 
		OB.numOppId = OM.numOppID
	OUTER APPLY
	(
		SELECT 
			SUM(monAmountPaid) AS monPaidAmount
		FROM
			DepositeDetails DD
		INNER JOIN
			DepositMaster DM
		ON
			DD.numDepositID=DM.numDepositId
		WHERE 
			numOppID = OM.numOppID
			AND numOppBizDocsID=OB.numOppBizDocsID
			AND CONVERT(DATE,DM.dtDepositDate) <= @dtToDate
	) TablePayments
    WHERE  
		OM.numDomainId = @numDomainId
		AND OM.[tintOppType] = 1
        AND OM.[tintOppStatus] = 1
        AND OM.numDivisionId = @numDivisionId       
        AND OB.[numBizDocId] = @AuthoritativeSalesBizDocId 	   
        AND OB.bitAuthoritativeBizDocs=1
        AND ISNULL(OB.tintDeferred,0) <> 1
        AND ISNULL((OB.monDealAmount * OM.fltExchangeRate) - (ISNULL(TablePayments.monPaidAmount,0) * OM.fltExchangeRate),0) > 0


	SELECT 
		@vcCompanyName vcCompanyName
		,dbo.FormatedDateFromDate(@dtToDate,@numDomainID) dtStatementDate
		,FORMAT(ISNULL((SELECT SUM(monAmountDue) FROM @Temp WHERE numPastDays<=0),0),'N2') monCurrent
		,FORMAT(ISNULL((SELECT SUM(monAmountDue) FROM @Temp WHERE numPastDays>0),0),'N2') monPastDue
		,@vcBluePayID vcBluePayID
		,@vcBluePayPassword vcBluePayPassword
		,@bitTest bitTest
		,@vcBluePayFormName vcBluePayFormName
		,@bitShowCardConnectLink bitShowCardConnectLink

	SELECT * FROM @TEMP	
END
GO