GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_GetBillPaymentDetails' ) 
    DROP PROCEDURE USP_GetBillPaymentDetails
GO
-- exec USP_GetBillPaymentDetails 0,1,0,0
CREATE PROCEDURE [dbo].[USP_GetBillPaymentDetails]
    @numBillPaymentID NUMERIC(18, 0) ,
    @numDomainID NUMERIC ,
    @ClientTimeZoneOffset INT,
    @numDivisionID NUMERIC=0,
    @numCurrencyID NUMERIC=0,
    @CurrentPage INT,
    @PageSize INT,
    @TotRecs INT OUTPUT,
    @numAccountClass NUMERIC(18,0)=0,
	@SortColumn AS VARCHAR(50),
    @columnSortOrder AS VARCHAR(10),
	@strCondition AS VARCHAR(MAX)=''
AS 
    SET NOCOUNT ON
    SET TRANSACTION ISOLATION LEVEL READ COMMITTED
		DECLARE @numBaseCurrencyID NUMERIC
	    DECLARE @BaseCurrencySymbol nVARCHAR(3)
	    
        SELECT @numBaseCurrencyID = ISNULL(numCurrencyID,0) FROM dbo.Domain WHERE numDomainId=@numDomainId
        SELECT @BaseCurrencySymbol = ISNULL(varCurrSymbol,'') FROM dbo.Currency WHERE numCurrencyID = @numBaseCurrencyID
    
		DECLARE @firstRec AS INTEGER
		DECLARE @lastRec AS INTEGER
		
		SET @firstRec = ( @CurrentPage - 1 ) * @PageSize
		SET @lastRec = ( @CurrentPage * @PageSize + 1 )
		
				  
		IF @numBillPaymentID>0
		BEGIN
			SELECT @numDivisionID=numDivisionID FROM [dbo].[BillPaymentHeader] BPH WHERE BPH.numBillPaymentID=@numBillPaymentID
		END

		IF LEN(@strCondition) = 0
		BEGIN
			SET @strCondition = '1=1'
		END

		DECLARE @strSQLTable1 AS NVARCHAR(MAX)
		DECLARE @strSQLTable2 AS NVARCHAR(MAX)
		DECLARE @strFinal AS NVARCHAR(MAX)

SET @strSQLTable1 = CONCAT(' SELECT BPH.numBillPaymentID,BPH.dtPaymentDate,BPH.numAccountID,BPH.numDivisionID,BPH.numReturnHeaderID,BPH.numPaymentMethod
						,ISNULL(BPH.monPaymentAmount,0) AS monPaymentAmount,ISNULL(BPH.monAppliedAmount,0) AS monAppliedAmount,
					 ISNULL(CH.numCheckHeaderID, 0) numCheckHeaderID,
					 ISNULL(CH.numCheckNo,0) AS numCheckNo,ISNULL(BPH.numReturnHeaderID,0) AS numReturnHeaderID,
					 BPH.numCurrencyID,ISNULL(NULLIF(BPH.fltExchangeRate,0),1) fltExchangeRateBillPayment
					,ISNULL(C.varCurrSymbol,'''') varCurrSymbol
					,''',ISNULL(@BaseCurrencySymbol,''),''' AS BaseCurrencySymbol					
					,ISNULL(monRefundAmount,0) AS monRefundAmount,ISNULL(BPH.numAccountClass,0) AS numAccountClass
					  FROM  [dbo].[BillPaymentHeader] BPH
							LEFT JOIN dbo.CheckHeader CH ON CH.numReferenceID = BPH.numBillPaymentID AND CH.tintReferenceType = 8
							LEFT JOIN dbo.Currency C ON C.numCurrencyID = BPH.numCurrencyID
							WHERE BPH.numBillPaymentID =',ISNULL(@numBillPaymentID,0),' ')
		
		IF LEN(@SortColumn) = 0 AND LEN(@columnSortOrder) = 0
		BEGIN
			SET @SortColumn = 'dtDueDate'
			SET	@columnSortOrder = 'ASC'
		END 

print @strSQLTable1
print @SortColumn
print @columnSortOrder

		--For Edit Bill Payment Get Details
SET @strSQLTable2 = ' (
						SELECT  Case When isnull(BH.bitLandedCost,0)=1 then  isnull(BH.numOppId,0) else OBD.numOppId end AS numoppid,
							  BPH.numDivisionID,BPD.[numOppBizDocsID],BPD.[numBillID],
							  CASE WHEN(BPD.numBillID>0) THEN  Case When isnull(BH.bitLandedCost,0)=1 THEN ''Laned Cost-'' ELSE ''Bill-'' END + CONVERT(VARCHAR(10),BH.numBillID)
							  else OBD.vcbizdocid END AS [Name],
							  CASE BPD.[tintBillType] WHEN 1 THEN OBD.monDealAmount WHEN 2 THEN BH.monAmountDue END monOriginalAmount,
							  CASE BPD.[tintBillType] WHEN 1 THEN ( OBD.monDealAmount - OBD.monAmountPaid + BPD.[monAmount]) WHEN 2 THEN BH.monAmountDue - BH.monAmtPaid + BPD.[monAmount] END monAmountDue,
							  CASE BPD.[tintBillType] WHEN 1 THEN '''' WHEN 2 THEN BH.vcReference END Reference,
							  CASE BPD.[tintBillType] WHEN 1 THEN dbo.fn_GetComapnyName(OM.numDivisionId) WHEN 2 THEN dbo.fn_GetComapnyName(BH.numDivisionID) END vcCompanyName,
							  BPD.[tintBillType],
							  CASE BPD.[tintBillType]
								  WHEN 1
								  THEN CASE ISNULL(OM.bitBillingTerms, 0)
										 WHEN 1
										 
										 THEN [dbo].[FormatedDateFromDate](DATEADD(dd,CONVERT(INT, ISNULL((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(OM.intBillingDays,0)), 0)),OBD.dtFromDate),'+ CONVERT(VARCHAR(10),@numDomainId) +')
										 WHEN 0
										 THEN [dbo].[FormatedDateFromDate](OBD.[dtFromDate],
																		 '+ CONVERT(VARCHAR(10), @numDomainId) +')
									   END
								  WHEN 2
								  THEN [dbo].[FormatedDateFromDate](BH.dtDueDate, '+ CONVERT(VARCHAR(10),@numDomainID) +') END AS DueDate,
								  BPD.[monAmount] AS monAmountPaid,1 bitIsPaid
								  ,ISNULL(NULLIF(OM.fltExchangeRate,0),1) fltExchangeRateOfOrder
								  ,BH.dtDueDate 
						FROM    [dbo].[BillPaymentHeader] BPH
								INNER JOIN dbo.BillPaymentDetails BPD ON BPH.numBillPaymentID = BPD.numBillPaymentID
								LEFT JOIN dbo.OpportunityBizDocs OBD ON BPD.numOppBizDocsID = OBD.numOppBizDocsId
								LEFT JOIN dbo.OpportunityMaster OM ON OBD.numOppId = OM.numOppId
								LEFT JOIN dbo.BillHeader BH ON BH.numBillID = BPD.numBillID
								LEFT JOIN dbo.CheckHeader CH ON CH.numReferenceID = BPH.numBillPaymentID
																AND CH.tintReferenceType = 8
						                                                 
						WHERE   BPH.[numBillPaymentID] = '+ CONVERT(VARCHAR(10),@numBillPaymentID) +'
						AND BPH.numDomainID = '+ CONVERT(VARCHAR(10),@numDomainID) +'
						AND (OM.numCurrencyID = '+ CONVERT(VARCHAR(10),@numCurrencyID) +' OR '+ CONVERT(VARCHAR(10),@numCurrencyID) +' = 0 OR ISNULL(BPD.numOppBizDocsID,0) =0)
						AND (ISNULL(BPH.numAccountClass,0)='+ CONVERT(VARCHAR(10),@numAccountClass) +' OR '+ CONVERT(VARCHAR(10),@numAccountClass) +'=0)
	            
						UNION
				
						SELECT  opp.numoppid AS numoppid ,
								opp.numdivisionid AS numDivisionID ,
								OBD.numOppBizDocsId AS numOppBizDocsId ,
								0 AS numBillID ,
								OBD.vcbizdocid AS [name] ,
								OBD.monDealAmount AS monOriginalAmount ,
								( OBD.monDealAmount - OBD.monAmountPaid ) AS monAmountDue ,
			
								OBD.vcRefOrderNo AS reference ,
			
								c.vccompanyname AS vccompanyname ,
								1 AS [tintBillType] , 
								CASE ISNULL(Opp.bitBillingTerms, 0)
								  WHEN 1
								  
								  THEN [dbo].[FormatedDateFromDate](DATEADD(dd,CONVERT(INT, ISNULL((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(intBillingDays,0)), 0)),OBD.dtFromDate),'+ CONVERT(VARCHAR(10),@numDomainId) +')
								  WHEN 0
								  THEN [dbo].[FormatedDateFromDate](OBD.[dtFromDate],
																	'+ CONVERT(VARCHAR(10),@numDomainId) +')
								END AS DueDate ,
								0 AS monAmountPaid,
								0 bitIsPaid
								,ISNULL(NULLIF(Opp.fltExchangeRate,0),1) fltExchangeRateOfOrder
								,OBD.[dtFromDate] AS dtDueDate
						FROM    OpportunityBizDocs OBD
								INNER JOIN OpportunityMaster Opp ON OBD.numOppId = Opp.numOppId
								INNER JOIN AdditionalContactsInformation ADC ON Opp.numContactId = ADC.numContactId
								INNER JOIN DivisionMaster Div ON Opp.numDivisionId = Div.numDivisionID
																 AND ADC.numDivisionId = Div.numDivisionID
								INNER JOIN CompanyInfo C ON Div.numCompanyID = C.numCompanyId
						WHERE   Opp.numDomainId = '+ CONVERT(VARCHAR(10),@numDomainId) +'
								AND OBD.bitAuthoritativeBizDocs = 1
								AND Opp.tintOppType = 2
								AND ( OBD.monDealAmount - OBD.monAmountPaid ) > 0
								AND (Opp.numDivisionId = '+ CONVERT(VARCHAR(10),@numDivisionID) +' OR ISNULL('+ CONVERT(VARCHAR(10),@numDivisionID) +',0)=0)
								AND OBD.numOppBizDocsID NOT IN (SELECT numOppBizDocsID FROM BillPaymentDetails WHERE numBillPaymentID = '+ CONVERT(VARCHAR(10),@numBillPaymentID) +')
								AND (Opp.numCurrencyID = '+ CONVERT(VARCHAR(10),@numCurrencyID)+' OR '+ CONVERT(VARCHAR(10),@numCurrencyID) +' = 0)
								AND (ISNULL(Opp.numAccountClass,0)='+ CONVERT(VARCHAR(10),@numAccountClass)+' OR '+ CONVERT(VARCHAR(10),@numAccountClass) +'=0)
						 UNION       
						
						SELECT  Case When isnull(BH.bitLandedCost,0)=1 then  isnull(BH.numOppId,0) else 0 end AS numoppid ,
								numDivisionID AS numDivisionID ,
								0 AS numOppBizDocsId ,
								BH.numBillID ,
								Case When isnull(BH.bitLandedCost,0)=1 THEN ''Laned Cost-'' ELSE ''Bill-'' END + CONVERT(VARCHAR(10),BH.numBillID) AS [name] ,
								BH.monAmountDue AS monOriginalAmount ,
								(BH.monAmountDue - BH.monAmtPaid) AS monAmountDue ,
			
								BH.vcReference AS reference ,
			
								dbo.fn_GetComapnyName(BH.numDivisionID) AS vccompanyname ,
								2 AS [tintBillType] ,
								[dbo].[FormatedDateFromDate](BH.dtDueDate,'+ CONVERT(VARCHAR(10),@numDomainId) +') AS DueDate ,
								0 AS monAmountPaid,
								0 bitIsPaid
								,ISNULL(BH.fltExchangeRate,1) fltExchangeRateOfOrder
								,BH.dtDueDate
						FROM    dbo.BillHeader BH
						WHERE   BH.numDomainId = '+ CONVERT(VARCHAR(10),@numDomainId) +'
								AND BH.bitIsPaid = 0
								AND (BH.numDivisionId = '+ CONVERT(VARCHAR(10),@numDivisionID) +' OR ISNULL('+ CONVERT(VARCHAR(10),@numDivisionID) +',0)=0) 
								AND BH.numBillID NOT IN (SELECT numBillID FROM BillPaymentDetails WHERE numBillPaymentID = '+ CONVERT(VARCHAR(10),@numBillPaymentID) +')      
								AND (ISNULL(BH.numCurrencyID,'+ CONVERT(VARCHAR(10),@numBaseCurrencyID) +') = '+ CONVERT(VARCHAR(10),@numCurrencyID) +' OR '+ CONVERT(VARCHAR(10),@numCurrencyID) +' = 0)
								AND (ISNULL((SELECT TOP 1 numClassID FROM BillDetails BD WHERE BD.numBillID=BH.numBillID),0)='+ CONVERT(VARCHAR(10),@numAccountClass) +' OR '+ CONVERT(VARCHAR(10),@numAccountClass) +'=0)
					) [BillPayment]  WHERE '+ convert(varchar(MAX),@strCondition) 

		SET @strFinal = CONCAT(@strSQLTable1,'SELECT Row_number() OVER ( ORDER BY ',@SortColumn,' ',@columnSortOrder,') AS row,* INTO #tempBillPayment FROM ', @strSQLTable2,'; SELECT @TotalRecords = COUNT(*) FROM #tempBillPayment; SELECT * FROM #tempBillPayment  WHERE row > ',@firstRec,' and row <',@lastRec,'; DROP TABLE #tempBillPayment ;')   

PRINT @strFinal

print  @TotRecs
exec sp_executesql @strFinal, N'@TotalRecords INT OUT', @TotRecs OUT


GO


