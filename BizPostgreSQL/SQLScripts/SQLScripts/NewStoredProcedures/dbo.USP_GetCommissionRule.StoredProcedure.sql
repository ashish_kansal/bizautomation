SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetCommissionRule')
DROP PROCEDURE USP_GetCommissionRule
GO
CREATE PROCEDURE [dbo].[USP_GetCommissionRule]
    @numComRuleID AS NUMERIC(9) = 0,
    @byteMode AS TINYINT,
    @numDomainID AS NUMERIC(9) = 0
AS 
   IF @byteMode = 0 
        BEGIN    
            SELECT  
				[numDomainID],
				[numComRuleID],
                [vcCommissionName],
                ISNULL([tinComDuration], 0) tinComDuration,
                ISNULL([tintComBasedOn], 0) tintComBasedOn,
                ISNULL([tintComType], 0) tintComType,
				ISNULL([tintComAppliesTo], 0) tintComAppliesTo,
				CASE ISNULL([tintComAppliesTo], 0)
					WHEN 1 THEN (SELECT COUNT(*) FROM [CommissionRuleItems] CI WHERE CI.[numComRuleID] = CR.numComRuleID AND CI.tintType=1)
					WHEN 2 THEN (SELECT COUNT(*) FROM [CommissionRuleItems] CI WHERE CI.[numComRuleID] = CR.numComRuleID AND CI.tintType=2)
                END AS ItemCount,
				ISNULL([tintComOrgType],0) tintComOrgType,
                CASE WHEN (ISNULL([tintComOrgType], 0) = 1 OR ISNULL([tintComOrgType], 0) = 2) THEN
					(SELECT COUNT(*) FROM CommissionRuleOrganization WHERE numComRuleID= @numComRuleID)
                END AS OrganizationCount,
				ISNULL(tintAssignTo,0) AS tintAssignTo
            FROM    CommissionRules CR
            WHERE   numComRuleID = @numComRuleID    
        END    
    ELSE IF @byteMode = 1
            BEGIN    
                SELECT  
					PB.numComRuleID,
                    PB.vcCommissionName,
					ISNULL(PP.Priority,'') AS [Priority],
					PB.tintComBasedOn,
					PB.tinComDuration,
					PB.tintComType,
                    CASE 
						WHEN tintComAppliesTo = 3 THEN 'All Items'
						WHEN tintComAppliesTo IN (1,2) THEN dbo.GetCommissionContactItemList(PB.numComRuleID,PB.tintComAppliesTo)
						ELSE ''
                    END AS ItemList,
                    CASE WHEN tintComOrgType = 3 THEN 'All Customers'
                            ELSE dbo.GetCommissionContactItemList(PB.numComRuleID, CASE [tintComOrgType]
                                                                            WHEN 1 THEN 3
                                                                            WHEN 2 THEN 4
                                                                        END)
					END CustomerList,
                    dbo.GetCommissionContactItemList(PB.[numComRuleID],0) AS ContactList
                FROM    CommissionRules PB 
				LEFT OUTER JOIN [PriceBookPriorities] PP ON PP.[Step2Value] = PB.tintComAppliesTo AND PP.[Step3Value] = PB.[tintComOrgType]
                WHERE   PB.numDomainID = @numDomainID
                ORDER BY vcCommissionName ASC
            END
     
GO
