set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_HelpDisplayOrder')
DROP PROCEDURE USP_HelpDisplayOrder
GO
CREATE PROCEDURE [dbo].[USP_HelpDisplayOrder]
	@strItems TEXT=null
AS
BEGIN

	
	DECLARE  @hDocItem INT
      IF CONVERT(VARCHAR(10),@strItems) <> ''
        BEGIN
          EXEC sp_xml_preparedocument
            @hDocItem OUTPUT ,@strItems
            
          UPDATE [HelpMaster] SET HelpMaster.[intDisplayOrder] = X.intDisplayOrder
          FROM   OPENXML (@hDocItem, '/NewDataSet/Item', 2)
                            WITH (numHelpID NUMERIC(9),intDisplayOrder int) X
		  WHERE HelpMaster.[numHelpID] = X.numHelpID
          EXEC sp_xml_removedocument
            @hDocItem
        END
END

