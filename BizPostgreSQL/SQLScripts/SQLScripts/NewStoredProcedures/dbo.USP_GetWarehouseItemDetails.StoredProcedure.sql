GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetWarehouseItemDetails')
DROP PROCEDURE USP_GetWarehouseItemDetails
GO
CREATE PROCEDURE USP_GetWarehouseItemDetails
(
	@numWareHouseItemID		BIGINT,
	@numDomainID			NUMERIC(18)
)
AS 
BEGIN
	SELECT ISNULL( WI.numWareHouseItemID,0) AS [numWareHouseItemID],
		ISNULL(numItemID,0) AS [numItemID],
		ISNULL(WI.numWareHouseID,0) AS [numWareHouseID],
		ISNULL(numOnHand,0) AS [numOnHand],
		ISNULL(numOnOrder,0) AS [numOnOrder],
		ISNULL(numReorder,0) AS [numReorder],
		ISNULL(numAllocation,0) AS [numAllocation],
		ISNULL(numBackOrder,0) AS [numBackOrder],
		ISNULL(monWListPrice,0) AS [monWListPrice],
		ISNULL(vcLocation,0) AS [vcLocation],
		ISNULL(vcWHSKU,0) AS [vcWHSKU],
		ISNULL(vcBarCode,0) AS [vcBarCode],
		ISNULL(I.numDomainID,0) AS [numDomainID],
		ISNULL(dtModified,0) AS [dtModified],
		ISNULL(numWLocationID,0) AS [numWLocationID]
	FROM dbo.WareHouseItems WI 
	JOIN dbo.Warehouses W ON WI.numWareHouseID = W.numWareHouseID
	JOIN dbo.Item I ON WI.numItemID = I.numItemCode AND WI.numDomainID = I.numDomainID
	WHERE WI.numWareHouseItemID = @numWareHouseItemID
	AND WI.numDomainID = @numDomainID
	
END