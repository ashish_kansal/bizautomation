GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_PromotionOffer_ApplyItemPromotionToOrder')
DROP PROCEDURE USP_PromotionOffer_ApplyItemPromotionToOrder
GO
CREATE PROCEDURE [dbo].[USP_PromotionOffer_ApplyItemPromotionToOrder]
	@numDomainID NUMERIC(18,0),
	@numDivisionID NUMERIC(18,0),
	@vcItems VARCHAR(MAX),
	@bitBasedOnDiscountCode BIT,
	@vcDiscountCode VARCHAR(100),
	@numCurrencyID NUMERIC(18,0)
AS
BEGIN
	DECLARE @hDocItem INT
	DECLARE @numRelationship NUMERIC(18,0)
	DECLARE @numProfile NUMERIC(18,0)
	DECLARE @fltExchangeRate FLOAT

	SET @fltExchangeRate = ISNULL((SELECT fltExchangeRate FROM Currency WHERE numDomainID=@numDomainID AND numCurrencyID=@numCurrencyID),1)

	IF ISNULL(@fltExchangeRate,0) = 0
	BEGIN
		SET @fltExchangeRate = 1
	END

	SELECT 
		@numRelationship=numCompanyType,
		@numProfile=vcProfile
	FROM 
		DivisionMaster D                  
	JOIN 
		CompanyInfo C 
	ON 
		C.numCompanyId=D.numCompanyID                  
	WHERE 
		numDivisionID =@numDivisionID 		

	DECLARE @TEMP TABLE
	(
		ID INT IDENTITY(1,1)
		,numOppItemID NUMERIC(18,0)
        ,numItemCode NUMERIC(18,0)
        ,numUnits FLOAT
		,numWarehouseItemID NUMERIC(18,0)
        ,monUnitPrice DECIMAL(20,5)
        ,fltDiscount DECIMAL(20,5)
        ,bitDiscountType BIT
        ,monTotalAmount DECIMAL(20,5)
        ,numPromotionID NUMERIC(18,0)
        ,bitPromotionTriggered BIT
        ,vcPromotionDescription VARCHAR(MAX)
		,bitPromotionDiscount BIT
		,vcKitChildItems VARCHAR(MAX)
		,vcInclusionDetail VARCHAR(MAX)
		,numSelectedPromotionID NUMERIC(18,0)
		,bitChanged BIT		
	)

	IF ISNULL(@vcItems,'') <> ''
	BEGIN
		EXEC sp_xml_preparedocument @hDocItem OUTPUT, @vcItems

		INSERT INTO @TEMP
		(
			numOppItemID
			,numItemCode
			,numUnits
			,numWarehouseItemID
			,monUnitPrice
			,fltDiscount
			,bitDiscountType
			,monTotalAmount
			,numPromotionID
			,bitPromotionTriggered
			,vcPromotionDescription
			,bitPromotionDiscount
			,vcKitChildItems
			,vcInclusionDetail
			,numSelectedPromotionID
			,bitChanged
		)

		SELECT 
			numOppItemID
			,numItemCode
			,numUnits
			,numWarehouseItemID
			,monUnitPrice * @fltExchangeRate
			,fltDiscount
			,bitDiscountType
			,monTotalAmount
			,numPromotionID
			,bitPromotionTriggered
			,vcPromotionDescription
			,bitPromotionDiscount
			,vcKitChildItems
			,vcInclusionDetail
			,numSelectedPromotionID
			,0
		FROM 
			OPENXML (@hDocItem,'/NewDataSet/Item',2)                                                                          
		WITH                       
		(                                                                          
			numOppItemID NUMERIC(18,0)
			,numItemCode NUMERIC(18,0)
			,numUnits FLOAT
			,numWarehouseItemID NUMERIC(18,0)
			,monUnitPrice DECIMAL(20,5)
			,fltDiscount DECIMAL(20,5)
			,bitDiscountType BIT
			,monTotalAmount DECIMAL(20,5)
			,numPromotionID NUMERIC(18,0)
			,bitPromotionTriggered BIT
			,bitPromotionDiscount BIT
			,vcKitChildItems VARCHAR(MAX)
			,vcInclusionDetail VARCHAR(MAX)
			,vcPromotionDescription VARCHAR(MAX)
			,numSelectedPromotionID NUMERIC(18,0)
		)

		EXEC sp_xml_removedocument @hDocItem

		DECLARE @i INT = 1
		DECLARE @iCount INT 
		SET @iCount = (SELECT COUNT(*) FROM @TEMP)

		DECLARE @l INT = 1
		DECLARE @lCount INT
		SET @lCount = (SELECT COUNT(*) FROM @TEMP)

		DECLARE @numTempPromotionID NUMERIC(18,0)
		DECLARE @tintOfferTriggerValueType TINYINT
		DECLARE @tintOfferBasedOn TINYINT
		DECLARE @tintOfferTriggerValueTypeRange TINYINT
		DECLARE @fltOfferTriggerValue FLOAT
		DECLARE @fltOfferTriggerValueRange FLOAT
		DECLARE @vcShortDesc VARCHAR(500)
		DECLARE @tintItemCalDiscount TINYINT
		DECLARE @monDiscountedItemPrice DECIMAL(20,5)
		DECLARE @numItemCode NUMERIC(18,0)
		DECLARE @numTempWarehouseItemID NUMERIC(18,0)
		DECLARE @numUnits FLOAT
		DECLARE @monTotalAmount DECIMAL(20,5)
		DECLARE @numItemClassification AS NUMERIC(18,0)

		DECLARE @bitRemainingCheckRquired AS BIT
		DECLARE @numRemainingPromotion FLOAT
		DECLARE @fltDiscountValue FLOAT
		DECLARE @tintDiscountType TINYINT
		DECLARE @tintDiscoutBaseOn TINYINT

		DECLARE @TEMPUsedPromotion TABLE
		(
			ID INT
			,numPromotionID NUMERIC(18,0)
			,numUnits FLOAT
			,monTotalAmount DECIMAL(20,5)
		)

		INSERT INTO @TEMPUsedPromotion
		(
			ID
			,numPromotionID
			,numUnits
			,monTotalAmount
		)
		SELECT
			ROW_NUMBER() OVER(ORDER BY T1.numPromotionID ASC)
			,numPromotionID
			,SUM(numUnits)
			,SUM(monTotalAmount)
		FROM
			@TEMP T1
		WHERE
			ISNULL(numPromotionID,0) > 0
			AND ISNULL(bitPromotionTriggered,0) = 1
		GROUP BY
			numPromotionID

		DECLARE @j INT = 1
		DECLARE @jCount INT
		SET @jCount = ISNULL((SELECT COUNT(*) FROM @TEMPUsedPromotion),0)

		-- FIRST REMOVE ALL PROMOTIONS WHERE ITEM WHICH TRIGERRED PROMOTION IS DELETED
		UPDATE 
			T1
		SET 
			numPromotionID=0
			,bitPromotionTriggered=0
			,vcPromotionDescription=''
			,bitPromotionDiscount=0
			,bitChanged=1
			,monUnitPrice=ISNULL(T2.monPrice,0)
			,fltDiscount=ISNULL(T2.decDiscount,0)
			,bitDiscountType=ISNULL(T2.tintDisountType,0)
		FROM
			@TEMP T1
		CROSS APPLY
			dbo.fn_GetItemPriceBasedOnMethod(@numDomainID,@numDivisionID,T1.numItemCode,T1.numUnits,0,0,T1.numWarehouseItemID,T1.vcKitChildItems,@numCurrencyID) T2
		WHERE 
			numPromotionID > 0
			AND ISNULL(bitPromotionTriggered,0) = 0
			AND (SELECT COUNT(*) FROM @TEMP T3 WHERE T3.numPromotionID=T1.numPromotionID AND ISNULL(T3.bitPromotionTriggered,0) = 1) = 0

		-- FIRST REMOVE APPLIED PROMOTION IF THEY ARE NOT VALIES
		WHILE @j <= @jCount
		BEGIN
			SELECT 
				@numTempPromotionID=numPromotionID
				,@numUnits = numUnits
				,@monTotalAmount=monTotalAmount
			FROM 
				@TEMPUsedPromotion 
			WHERE 
				ID = @j

			IF NOT EXISTS (SELECT 
								PO.numProId 
							FROM 
								PromotionOffer PO
							LEFT JOIN	
								PromotionOffer POOrder
							ON
								PO.numOrderPromotionID = POOrder.numProId
							LEFT JOIN	
								DiscountCodes DC
							ON
								PO.numOrderPromotionID = DC.numPromotionID
							WHERE 
								PO.numDomainId=@numDomainID 
								AND PO.numProId=@numTempPromotionID
								AND ISNULL(PO.bitEnabled,0)=1 
								AND ISNULL(PO.IsOrderBasedPromotion,0) = 0
								AND 1 = (CASE 
											WHEN ISNULL(PO.numOrderPromotionID,0) > 0 
											THEN (CASE WHEN POOrder.bitNeverExpires = 1 THEN 1 ELSE (CASE WHEN GETUTCDATE()>=POOrder.dtValidFrom AND GETUTCDATE()<=POOrder.dtValidTo THEN 1 ELSE 0 END) END)
											ELSE (CASE WHEN PO.bitNeverExpires = 1 THEN 1 ELSE (CASE WHEN GETUTCDATE()>=PO.dtValidFrom AND GETUTCDATE()<=PO.dtValidTo THEN 1 ELSE 0 END) END)
										END)
								AND 1 = (CASE 
											WHEN ISNULL(PO.numOrderPromotionID,0) > 0 
											THEN
												(CASE WHEN (SELECT COUNT(*) FROM PromotionOfferOrganizations WHERE numProId=PO.numOrderPromotionID AND tintType=2 AND numRelationship=@numRelationship AND numProfile=@numProfile) > 0 THEN 1 ELSE 0 END)
											ELSE
												(CASE PO.tintCustomersBasedOn 
													WHEN 1 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferOrganizations WHERE numProId=PO.numProId AND tintType=1 AND numDivisionID=@numDivisionID) > 0 THEN 1 ELSE 0 END)
													WHEN 2 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferOrganizations WHERE numProId=PO.numProId AND tintType=2 AND numRelationship=@numRelationship AND numProfile=@numProfile) > 0 THEN 1 ELSE 0 END)
													WHEN 3 THEN 1
													ELSE 0
												END)
										END)
								AND 1 = (CASE 
											WHEN ISNULL(PO.tintOfferTriggerValueType,1) = 1 --Based on quantity
											THEN
												CASE
													WHEN ISNULL(PO.tintOfferTriggerValueTypeRange,1) = 2
													THEN
														(CASE WHEN ISNULL(@numUnits,0) BETWEEN ISNULL(PO.fltOfferTriggerValue,0) AND ISNULL(PO.fltOfferTriggerValueRange,0) THEN 1 ELSE 0 END)
													ELSE
														(CASE WHEN ISNULL(@numUnits,0) >= PO.fltOfferTriggerValue THEN 1 ELSE 0 END)
												END
											WHEN ISNULL(PO.tintOfferTriggerValueType,1) = 2 --Based on amount
											THEN
												CASE
													WHEN ISNULL(PO.tintOfferTriggerValueTypeRange,1) = 2
													THEN
														(CASE WHEN ISNULL(@monTotalAmount,0) BETWEEN ISNULL(PO.fltOfferTriggerValue,0) AND ISNULL(PO.fltOfferTriggerValueRange,0) THEN 1 ELSE 0 END)
													ELSE
														(CASE WHEN ISNULL(@monTotalAmount,0) >= PO.fltOfferTriggerValue THEN 1 ELSE 0 END)
												END
										END)
								AND 1 = (CASE 
											WHEN ISNULL(PO.numOrderPromotionID,0) > 0 
											THEN 
												(CASE WHEN ISNULL(DC.CodeUsageLimit,0) <> 0 THEN (CASE WHEN ISNULL((SELECT intCodeUsed FROM DiscountCodeUsage WHERE numDiscountId=DC.numDiscountId AND numDivisionId=@numDivisionId),0) >= DC.CodeUsageLimit THEN 0 ELSE 1 END) ELSE 1 END)
											ELSE 1 
										END)
					)
			BEGIN
				-- IF Promotion is not valid than revert line item price to based on default pricing
				UPDATE 
					T1
				SET 
					numPromotionID=0
					,bitPromotionTriggered=0
					,vcPromotionDescription=''
					,bitPromotionDiscount=0
					,bitChanged=1
					,monUnitPrice=ISNULL(T2.monPrice,0)
					,fltDiscount=ISNULL(T2.decDiscount,0)
					,bitDiscountType=ISNULL(T2.tintDisountType,0)
				FROM
					@TEMP T1
				CROSS APPLY
					dbo.fn_GetItemPriceBasedOnMethod(@numDomainID,@numDivisionID,T1.numItemCode,T1.numUnits,0,0,T1.numWarehouseItemID,T1.vcKitChildItems,@numCurrencyID) T2
				WHERE 
					numPromotionID=@numTempPromotionID
			END

			SET @j = @j + 1
		END

		UPDATE
			T1
		SET 
			T1.bitPromotionDiscount = 0
			,T1.bitChanged=1
			,T1.monUnitPrice=ISNULL(T2.monPrice,0)
			,T1.fltDiscount=ISNULL(T2.decDiscount,0)
			,T1.bitDiscountType=ISNULL(T2.tintDisountType,0)
		FROM
			@TEMP T1
		INNER JOIN
			PromotionOffer PO
		ON
			T1.numPromotionID=PO.numProId
		CROSS APPLY
			dbo.fn_GetItemPriceBasedOnMethod(@numDomainID,@numDivisionID,T1.numItemCode,T1.numUnits,0,0,T1.numWarehouseItemID,T1.vcKitChildItems,@numCurrencyID) T2
		WHERE
			ISNULL(numPromotionID,0) > 0
			AND ISNULL(bitPromotionDiscount,0) = 1
			AND ISNULL(PO.tintDiscountType,0) = 3

		IF ISNULL(@bitBasedOnDiscountCode,0) = 1
		BEGIN
			DECLARE @TableItemCouponPromotion TABLE
			(
				ID INT IDENTITY(1,1)
				,numPromotionID NUMERIC(18,0)
				,tintOfferTriggerValueType TINYINT
				,tintOfferTriggerValueTypeRange TINYINT
				,fltOfferTriggerValue FLOAT
				,fltOfferTriggerValueRange FLOAT
				,tintOfferBasedOn TINYINT
				,vcShortDesc VARCHAR(300)
			)

			INSERT INTO @TableItemCouponPromotion
			(
				numPromotionID
				,tintOfferTriggerValueType
				,tintOfferTriggerValueTypeRange
				,fltOfferTriggerValue
				,fltOfferTriggerValueRange
				,tintOfferBasedOn
				,vcShortDesc
			)
			SELECT
				PO.numProId
				,PO.tintOfferTriggerValueType
				,PO.tintOfferTriggerValueTypeRange
				,PO.fltOfferTriggerValue
				,PO.fltOfferTriggerValueRange
				,PO.tintOfferBasedOn
				,ISNULL(PO.vcShortDesc,'-')
			FROM
				PromotionOffer PO
			INNER JOIN
				PromotionOffer POOrder
			ON
				PO.numOrderPromotionID = POOrder.numProId
			INNER JOIN 
				DiscountCodes DC
			ON 
				POOrder.numProId = DC.numPromotionID
			WHERE
				PO.numDomainId = @numDomainID
				AND POOrder.numDomainId=@numDomainID 
				AND ISNULL(PO.bitEnabled,0)=1 
				AND ISNULL(POOrder.bitEnabled,0)=1 
				AND ISNULL(PO.bitUseOrderPromotion,0)=1 
				AND ISNULL(POOrder.IsOrderBasedPromotion,0) = 1
				AND ISNULL(POOrder.bitRequireCouponCode,0) = 1
				AND 1 = (CASE WHEN POOrder.bitNeverExpires = 1 THEN 1 ELSE (CASE WHEN GETUTCDATE()>=POOrder.dtValidFrom AND GETUTCDATE()<=POOrder.dtValidTo THEN 1 ELSE 0 END) END)
				AND 1 = (CASE WHEN ISNULL(DC.CodeUsageLimit,0) <> 0 THEN (CASE WHEN ISNULL((SELECT intCodeUsed FROM DiscountCodeUsage WHERE numDiscountId=DC.numDiscountId AND numDivisionId=@numDivisionId),0) >= DC.CodeUsageLimit THEN 0 ELSE 1 END) ELSE 1 END)
				AND 1 = (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferOrganizations WHERE numProId=POOrder.numProId AND tintType=2 AND numRelationship=@numRelationship AND numProfile=@numProfile) > 0 THEN 1 ELSE 0 END)
				AND DC.vcDiscountCode = @vcDiscountCode
			
			SET @l = 1
			DECLARE @k INT = 1
			DECLARE @kCount INT 
			SET @kCount = (SELECT COUNT(*) FROM @TableItemCouponPromotion)

			WHILE @k <= @kCount
			BEGIN
				SELECT
					@numTempPromotionID=numPromotionID
					,@tintOfferTriggerValueType=tintOfferTriggerValueType
					,@tintOfferTriggerValueTypeRange = tintOfferTriggerValueTypeRange
					,@fltOfferTriggerValue=fltOfferTriggerValue
					,@fltOfferTriggerValueRange=fltOfferTriggerValueRange
					,@tintOfferBasedOn=tintOfferBasedOn
					,@vcShortDesc=vcShortDesc
				FROM
					@TableItemCouponPromotion
				WHERE
					ID=@k

				IF (SELECT COUNT(*) FROM @TEMP WHERE numPromotionID=@numTempPromotionID AND ISNULL(bitPromotionTriggered,0)=1) = 0
				BEGIN
					IF ISNULL((SELECT 
								(CASE WHEN @tintOfferTriggerValueType = 2 THEN SUM(ISNULL(monTotalAmount,0)) ELSE SUM(ISNULL(numUnits,0)) END)
							FROM
								@TEMP T1
							INNER JOIN
								Item I
							ON
								T1.numItemCode = I.numItemCode
							WHERE
								ISNULL(numPromotionID,0) = 0
								AND 1 = (CASE 
											WHEN @tintOfferBasedOn = 1 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId=@numTempPromotionID AND tintRecordType=5 AND tintType=1 AND numValue=T1.numItemCode) > 0 THEN 1 ELSE 0 END)
											WHEN @tintOfferBasedOn = 2 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId=@numTempPromotionID AND tintRecordType=5 AND tintType=2 AND numValue=ISNULL(I.numItemClassification,0)) > 0 THEN 1 ELSE 0 END)
											WHEN @tintOfferBasedOn = 4 THEN 1 
											ELSE 0
										END)
							),0) >= ISNULL(@fltOfferTriggerValue,0)
					BEGIN
						SET @i = 1

						WHILE @i <= @iCount
						BEGIN

							IF ISNULL((SELECT 
										(CASE WHEN @tintOfferTriggerValueType = 2 THEN SUM(ISNULL(monTotalAmount,0)) ELSE SUM(ISNULL(numUnits,0)) END)
									FROM
										@TEMP T1
									WHERE
										ISNULL(numPromotionID,0) = @numTempPromotionID
										AND ISNULL(bitPromotionTriggered,0) = 1
									),0) < ISNULL(@fltOfferTriggerValue,0)
							BEGIN
								UPDATE
									T1
								SET
									bitChanged = 1
									,numPromotionID=@numTempPromotionID
									,bitPromotionTriggered=1
									,vcPromotionDescription=ISNULL(@vcShortDesc,'')
								FROM
									@TEMP T1
								INNER JOIN
									Item I
								ON
									T1.numItemCode = I.numItemCode
								WHERE
									T1.ID = @i
									AND ISNULL(numPromotionID,0) = 0
									AND 1 = (CASE 
												WHEN ISNULL(@tintOfferTriggerValueType,1) = 1 --Based on quantity
												THEN
													CASE
														WHEN ISNULL(@tintOfferTriggerValueTypeRange,1) = 2
														THEN
															(CASE WHEN ISNULL(T1.numUnits,0) + ISNULL((SELECT SUM(ISNULL(numUnits,0)) FROM @TEMP WHERE ISNULL(numPromotionID,0) = @numTempPromotionID AND ISNULL(bitPromotionTriggered,0) = 1),0) BETWEEN ISNULL(@fltOfferTriggerValue,0) AND ISNULL(@fltOfferTriggerValueRange,0) THEN 1 ELSE 0 END)
														ELSE
															1
													END
												WHEN ISNULL(@tintOfferTriggerValueType,1) = 2 --Based on amount
												THEN
													CASE
														WHEN ISNULL(@tintOfferTriggerValueTypeRange,1) = 2
														THEN
															(CASE WHEN ISNULL(T1.monTotalAmount,0) + ISNULL((SELECT SUM(ISNULL(monTotalAmount,0)) FROM @TEMP WHERE ISNULL(numPromotionID,0) = @numTempPromotionID AND ISNULL(bitPromotionTriggered,0) = 1),0) BETWEEN ISNULL(@fltOfferTriggerValue,0) AND ISNULL(@fltOfferTriggerValueRange,0) THEN 1 ELSE 0 END)
														ELSE
															1
													END
											END)
									AND 1 = (CASE 
												WHEN @tintOfferBasedOn = 1 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId=@numTempPromotionID AND tintRecordType=5 AND tintType=1 AND numValue=T1.numItemCode) > 0 THEN 1 ELSE 0 END)
												WHEN @tintOfferBasedOn = 2 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId=@numTempPromotionID AND tintRecordType=5 AND tintType=2 AND numValue=ISNULL(I.numItemClassification,0)) > 0 THEN 1 ELSE 0 END)
												WHEN @tintOfferBasedOn = 4 THEN 1 
												ELSE 0
											END)
							END

							SET @i = @i + 1
						END

					END
				END

				SET @k = @k + 1
			END
		END

		-- TRIGGER PROMOTION FOR ITEM WHICH ARE ELIGIBLE
		SET @i = 1
		WHILE @i <= @iCount
		BEGIN
			SET @numTempPromotionID = NULL

			SELECT
				@numTempPromotionID=numProId
				,@tintOfferTriggerValueType=tintOfferTriggerValueType
				,@tintOfferTriggerValueTypeRange = tintOfferTriggerValueTypeRange
				,@fltOfferTriggerValue=fltOfferTriggerValue
				,@fltOfferTriggerValueRange=fltOfferTriggerValueRange
				,@tintOfferBasedOn=tintOfferBasedOn
				,@vcShortDesc=vcShortDesc
			FROM
				@TEMP T1
			INNER JOIN
				Item I
			ON
				T1.numItemCode=I.numItemCode
			CROSS APPLY
			(
				SELECT TOP 1
					PO.numProId
					,PO.tintOfferBasedOn
					,PO.tintOfferTriggerValueType
					,PO.tintOfferTriggerValueTypeRange
					,PO.fltOfferTriggerValue
					,PO.fltOfferTriggerValueRange
					,ISNULL(PO.vcShortDesc,'') vcShortDesc
				FROM 
					PromotionOffer PO
				WHERE
					PO.numDomainId=@numDomainID
					AND 1 = (CASE WHEN ISNULL(T1.numSelectedPromotionID,0) > 0 THEN (CASE WHEN PO.numProId=T1.numSelectedPromotionID THEN 1 ELSE 0 END) ELSE 1 END) 
					AND ISNULL(PO.bitEnabled,0)=1 
					AND ISNULL(PO.IsOrderBasedPromotion,0) = 0
					AND ISNULL(PO.bitUseOrderPromotion,0) = 0
					AND ISNULL(PO.bitRequireCouponCode,0) = 0
					AND 1 = (CASE WHEN PO.bitNeverExpires = 1 THEN 1 ELSE (CASE WHEN GETUTCDATE()>=PO.dtValidFrom AND GETUTCDATE()<=PO.dtValidTo THEN 1 ELSE 0 END) END)
					AND 1 = (CASE 
								WHEN ISNULL(numOrderPromotionID,0) > 0
								THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferOrganizations WHERE numProId=numOrderPromotionID AND tintType=2 AND numRelationship=@numRelationship AND numProfile=@numProfile) > 0 THEN 1 ELSE 0 END)
								ELSE
									(CASE tintCustomersBasedOn 
										WHEN 1 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferOrganizations WHERE numProId=PO.numProId AND tintType=1 AND numDivisionID=@numDivisionID) > 0 THEN 1 ELSE 0 END)
										WHEN 2 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferOrganizations WHERE numProId=PO.numProId AND tintType=2 AND numRelationship=@numRelationship AND numProfile=@numProfile) > 0 THEN 1 ELSE 0 END)
										WHEN 3 THEN 1
										ELSE 0
									END)
							END)
					AND 1 = (CASE 
								WHEN PO.tintOfferBasedOn = 1 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId=PO.numProId AND tintRecordType=5 AND tintType=1 AND numValue=T1.numItemCode) > 0 THEN 1 ELSE 0 END)
								WHEN PO.tintOfferBasedOn = 2 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId=PO.numProId AND tintRecordType=5 AND tintType=2 AND numValue=ISNULL(I.numItemClassification,0)) > 0 THEN 1 ELSE 0 END)
								WHEN PO.tintOfferBasedOn = 4 THEN 1 
								ELSE 0
							END)
				ORDER BY
					(CASE 
						WHEN PO.tintOfferBasedOn = 1 AND PO.tintDiscoutBaseOn = 1 THEN 1
						WHEN PO.tintOfferBasedOn = 2 AND PO.tintDiscoutBaseOn = 1 THEN 2
						WHEN PO.tintOfferBasedOn = 1 AND PO.tintDiscoutBaseOn = 2 THEN 3
						WHEN PO.tintOfferBasedOn = 2 AND PO.tintDiscoutBaseOn = 2 THEN 4
						WHEN PO.tintOfferBasedOn = 1 AND PO.tintDiscoutBaseOn = 3 THEN 5
						WHEN PO.tintOfferBasedOn = 2 AND PO.tintDiscoutBaseOn = 3 THEN 6
					END) ASC,ISNULL(PO.fltOfferTriggerValue,0) DESC

			) T2
			WHERE
				ISNULL(T1.numPromotionID,0)  = 0
				AND T1.ID = @i
				AND ISNULL((SELECT COUNT(*) FROM @TEMP T3 WHERE T3.numPromotionID=T2.numProId AND T3.bitPromotionTriggered=1),0) = 0

			IF ISNULL(@numTempPromotionID,0) > 0 AND (SELECT COUNT(*) FROM @TEMP WHERE numPromotionID=@numTempPromotionID AND ISNULL(bitPromotionTriggered,0)=1) = 0
			BEGIN
				IF ISNULL((SELECT 
								(CASE WHEN @tintOfferTriggerValueType = 2 THEN SUM(ISNULL(monTotalAmount,0)) ELSE SUM(ISNULL(numUnits,0)) END)
							FROM
								@TEMP T1
							INNER JOIN
								Item I
							ON
								T1.numItemCode = I.numItemCode
							WHERE
								ISNULL(numPromotionID,0) = 0
								AND 1 = (CASE 
											WHEN @tintOfferBasedOn = 1 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId=@numTempPromotionID AND tintRecordType=5 AND tintType=1 AND numValue=T1.numItemCode) > 0 THEN 1 ELSE 0 END)
											WHEN @tintOfferBasedOn = 2 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId=@numTempPromotionID AND tintRecordType=5 AND tintType=2 AND numValue=ISNULL(I.numItemClassification,0)) > 0 THEN 1 ELSE 0 END)
											WHEN @tintOfferBasedOn = 4 THEN 1 
											ELSE 0
										END)
							),0) >= ISNULL(@fltOfferTriggerValue,0)
				BEGIN
					SET @l = 1

					WHILE @l <= @lCount
					BEGIN
						
						IF ISNULL((SELECT 
										(CASE WHEN @tintOfferTriggerValueType = 2 THEN SUM(ISNULL(monTotalAmount,0)) ELSE SUM(ISNULL(numUnits,0)) END)
									FROM
										@TEMP T1
									WHERE
										ISNULL(numPromotionID,0) = @numTempPromotionID
										AND ISNULL(bitPromotionTriggered,0) = 1
									),0) < ISNULL(@fltOfferTriggerValue,0)
						BEGIN
							UPDATE
								T1
							SET
								bitChanged = 1
								,numPromotionID=@numTempPromotionID
								,bitPromotionTriggered=1
								,vcPromotionDescription=ISNULL(@vcShortDesc,'')
							FROM
								@TEMP T1
							INNER JOIN
								Item I
							ON
								T1.numItemCode = I.numItemCode
							WHERE
								T1.ID = @l
								AND ISNULL(numPromotionID,0) = 0
								AND 1 = (CASE 
											WHEN ISNULL(@tintOfferTriggerValueType,1) = 1 --Based on quantity
											THEN
												CASE
													WHEN ISNULL(@tintOfferTriggerValueTypeRange,1) = 2
													THEN
														(CASE WHEN ISNULL(T1.numUnits,0) + ISNULL((SELECT SUM(ISNULL(numUnits,0)) FROM @TEMP WHERE ISNULL(numPromotionID,0) = @numTempPromotionID AND ISNULL(bitPromotionTriggered,0) = 1),0) BETWEEN ISNULL(@fltOfferTriggerValue,0) AND ISNULL(@fltOfferTriggerValueRange,0) THEN 1 ELSE 0 END)
													ELSE
														1
												END
											WHEN ISNULL(@tintOfferTriggerValueType,1) = 2 --Based on amount
											THEN
												CASE
													WHEN ISNULL(@tintOfferTriggerValueTypeRange,1) = 2
													THEN
														(CASE WHEN ISNULL(T1.monTotalAmount,0) + ISNULL((SELECT SUM(ISNULL(monTotalAmount,0)) FROM @TEMP WHERE ISNULL(numPromotionID,0) = @numTempPromotionID AND ISNULL(bitPromotionTriggered,0) = 1),0) BETWEEN ISNULL(@fltOfferTriggerValue,0) AND ISNULL(@fltOfferTriggerValueRange,0) THEN 1 ELSE 0 END)
													ELSE
														1
												END
										END)
								AND 1 = (CASE 
											WHEN @tintOfferBasedOn = 1 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId=@numTempPromotionID AND tintRecordType=5 AND tintType=1 AND numValue=T1.numItemCode) > 0 THEN 1 ELSE 0 END)
											WHEN @tintOfferBasedOn = 2 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId=@numTempPromotionID AND tintRecordType=5 AND tintType=2 AND numValue=ISNULL(I.numItemClassification,0)) > 0 THEN 1 ELSE 0 END)
											WHEN @tintOfferBasedOn = 4 THEN 1 
											ELSE 0
										END)
						END

						SET @l = @l + 1
					END

				END
			END

			SET @i = @i + 1
		END
		

		DECLARE @TEMPPromotion TABLE
		(
			ID INT
			,numPromotionID NUMERIC(18,0)
			,vcShortDesc VARCHAR(500)
			,numTriggerItemCode NUMERIC(18,0)
			,numWarehouseItemID NUMERIC(18,0)
			,numUnits FLOAT
			,monTotalAmount DECIMAL(20,5)
		)

		INSERT INTO @TEMPPromotion
		(
			ID
			,numPromotionID
			,numTriggerItemCode
			,numWarehouseItemID
			,numUnits
			,monTotalAmount
		)
		SELECT
			ROW_NUMBER() OVER(ORDER BY T1.numOppItemID ASC)
			,numPromotionID
			,numItemCode
			,numWarehouseItemID
			,numUnits
			,monTotalAmount
		FROM
			@TEMP T1
		INNER JOIN
			PromotionOffer PO
		ON
			T1.numPromotionID = PO.numProId
		WHERE
			ISNULL(numPromotionID,0) > 0
			AND ISNULL(bitPromotionTriggered,0) = 1
			AND 1 = (CASE WHEN @bitBasedOnDiscountCode=0 THEN (CASE WHEN ISNULL(PO.numOrderPromotionID,0) = 0 THEN 1 ELSE 0 END) ELSE 1 END)

		SET @j = 1
		SET @jCount = ISNULL((SELECT COUNT(*) FROM @TEMPPromotion),0)

		WHILE @j <= @jCount
		BEGIN
			SELECT 
				@numTempPromotionID=numPromotionID
				,@numItemCode=numTriggerItemCode
				,@numTempWarehouseItemID=numWarehouseItemID
				,@numUnits = numUnits
				,@monTotalAmount=monTotalAmount
			FROM 
				@TEMPPromotion 
			WHERE 
				ID = @j

			SELECT
				@fltDiscountValue=ISNULL(fltDiscountValue,0)
				,@tintDiscountType=ISNULL(tintDiscountType,0)
				,@tintDiscoutBaseOn=tintDiscoutBaseOn
				,@vcShortDesc=ISNULL(vcShortDesc,'')
				,@tintItemCalDiscount=ISNULL(tintItemCalDiscount,1)
				,@monDiscountedItemPrice=ISNULL(monDiscountedItemPrice,0)
			FROM
				PromotionOffer
			WHERE
				numProId=@numTempPromotionID

			-- If discount is based on amount or quantity than we have to checked remaining amount or qty left to give as discount 
			IF @tintDiscountType = 2 OR @tintDiscountType = 3
			BEGIN
				SET @bitRemainingCheckRquired = 1
			END
			ELSE
			BEGIN
				SET @bitRemainingCheckRquired = 0
				SET @numRemainingPromotion = 0
			END

			-- If promotion is valid than check whether any item left to apply promotion
			SET @i = 1

			WHILE @i <= @iCount
			BEGIN
				IF @bitRemainingCheckRquired=1
				BEGIN
					IF @tintDiscountType = 2 -- Discount by amount
					BEGIN
						SET @numRemainingPromotion = @fltDiscountValue - ISNULL((SELECT SUM(fltDiscount) FROM @TEMP WHERE numPromotionID = @numTempPromotionID	AND ISNULL(bitDiscountType,0)=1 AND bitPromotionDiscount = 1),0)
					END
					ELSE IF @tintDiscountType = 3 -- Discount by quantity
					BEGIN
						IF @tintDiscoutBaseOn IN (5,6)
						BEGIN
							SET @numRemainingPromotion = @fltDiscountValue - ISNULL((SELECT SUM(numUnits) FROM @TEMP WHERE numPromotionID = @numTempPromotionID	AND ISNULL(bitDiscountType,0)=1 AND bitPromotionDiscount = 1),0)
						END
						ELSE
						BEGIN
							SET @numRemainingPromotion = @fltDiscountValue - ISNULL((SELECT SUM(fltDiscount/monUnitPrice) FROM @TEMP WHERE numPromotionID = @numTempPromotionID	AND ISNULL(bitDiscountType,0)=1 AND bitPromotionDiscount = 1),0)
						END
					END
				END

				IF @bitRemainingCheckRquired=0 OR (@bitRemainingCheckRquired=1 AND @numRemainingPromotion > 0)
				BEGIN
					UPDATE
						T1
					SET
						monUnitPrice= CASE 
											WHEN (ISNULL(bitKitParent,0)=1 OR ISNULL(bitAssembly,0) = 1) AND ISNULL(bitCalAmtBasedonDepItems,0) = 1
											THEN (dbo.GetCalculatedPriceForKitAssembly(@numDomainID,@numDivisionID,I.numItemCode,T1.numUnits,@numTempWarehouseItemID,ISNULL(tintKitAssemblyPriceBasedOn,1),0,0,ISNULL(T1.vcKitChildItems,''),@numCurrencyID,@fltExchangeRate) * @fltExchangeRate)
											ELSE (CASE WHEN @tintItemCalDiscount = 1 THEN (ISNULL(T2.monFinalPrice,0) * @fltExchangeRate) ELSE ISNULL(I.monListPrice,0) END)
										END
						,fltDiscount = (CASE 
											WHEN @bitRemainingCheckRquired=0 
											THEN 
												@fltDiscountValue
											ELSE 
												(CASE 
													WHEN @tintDiscountType = 2 -- Discount by amount
													THEN
														(CASE 
															WHEN (T1.numUnits * (CASE 
																					WHEN (ISNULL(bitKitParent,0)=1 OR ISNULL(bitAssembly,0) = 1) AND ISNULL(bitCalAmtBasedonDepItems,0) = 1
																					THEN (dbo.GetCalculatedPriceForKitAssembly(@numDomainID,@numDivisionID,I.numItemCode,T1.numUnits,@numTempWarehouseItemID,ISNULL(tintKitAssemblyPriceBasedOn,1),0,0,ISNULL(T1.vcKitChildItems,''),@numCurrencyID,@fltExchangeRate) * @fltExchangeRate)
																					ELSE (CASE WHEN @tintItemCalDiscount = 1 THEN (ISNULL(T2.monFinalPrice,0) * @fltExchangeRate) ELSE ISNULL(I.monListPrice,0) END)
																				END)) >= @numRemainingPromotion 
															THEN @numRemainingPromotion 
															ELSE (T1.numUnits * (CASE 
																					WHEN (ISNULL(bitKitParent,0)=1 OR ISNULL(bitAssembly,0) = 1) AND ISNULL(bitCalAmtBasedonDepItems,0) = 1
																					THEN (dbo.GetCalculatedPriceForKitAssembly(@numDomainID,@numDivisionID,I.numItemCode,T1.numUnits,@numTempWarehouseItemID,ISNULL(tintKitAssemblyPriceBasedOn,1),0,0,ISNULL(T1.vcKitChildItems,''),@numCurrencyID,@fltExchangeRate) * @fltExchangeRate)
																					ELSE (CASE WHEN @tintItemCalDiscount = 1 THEN (ISNULL(T2.monFinalPrice,0) * @fltExchangeRate) ELSE ISNULL(I.monListPrice,0) END)
																				END)) 
														END)
													WHEN @tintDiscountType = 3 -- Discount by quantity
													THEN
														(CASE 
															WHEN @tintDiscoutBaseOn IN (5,6)
															THEN
																(CASE 
																	WHEN (CASE WHEN (ISNULL(T1.numPromotionID,0) = @numTempPromotionID AND ISNULL(bitPromotionTriggered,0)=1 AND ISNULL(bitPromotionDiscount,0)=0) 
																		THEN T1.numUnits - ISNULL(PO.fltOfferTriggerValue,0) 
																		ELSE T1.numUnits
																	END) >= @numRemainingPromotion 
																	THEN @numRemainingPromotion 
																	ELSE (CASE WHEN (ISNULL(T1.numPromotionID,0) = @numTempPromotionID AND ISNULL(bitPromotionTriggered,0)=1 AND ISNULL(bitPromotionDiscount,0)=0) 
																		THEN T1.numUnits - ISNULL(PO.fltOfferTriggerValue,0) 
																		ELSE T1.numUnits
																	END)
																END) * (CASE 
																			WHEN (CASE WHEN @tintItemCalDiscount = 1 THEN (ISNULL(T2.monFinalPrice,0) * @fltExchangeRate) ELSE ISNULL(I.monListPrice,0) END) > @monDiscountedItemPrice
																			THEN ((CASE WHEN @tintItemCalDiscount = 1 THEN (ISNULL(T2.monFinalPrice,0) * @fltExchangeRate) ELSE ISNULL(I.monListPrice,0) END) - @monDiscountedItemPrice)
																			ELSE 
																				(CASE WHEN @tintItemCalDiscount = 1 THEN (ISNULL(T2.monFinalPrice,0) * @fltExchangeRate) ELSE ISNULL(I.monListPrice,0) END) 
																		END)
															ELSE
																(CASE 
																	WHEN (CASE WHEN (ISNULL(T1.numPromotionID,0) = @numTempPromotionID AND ISNULL(bitPromotionTriggered,0)=1 AND ISNULL(bitPromotionDiscount,0)=0) 
																			THEN T1.numUnits - ISNULL(PO.fltOfferTriggerValue,0) 
																			ELSE T1.numUnits
																		END) >= @numRemainingPromotion 
																	THEN (@numRemainingPromotion * (CASE 
																										WHEN (ISNULL(bitKitParent,0)=1 OR ISNULL(bitAssembly,0) = 1) AND ISNULL(bitCalAmtBasedonDepItems,0) = 1
																										THEN (dbo.GetCalculatedPriceForKitAssembly(@numDomainID,@numDivisionID,I.numItemCode,T1.numUnits,@numTempWarehouseItemID,ISNULL(tintKitAssemblyPriceBasedOn,1),0,0,ISNULL(T1.vcKitChildItems,''),@numCurrencyID,@fltExchangeRate) * @fltExchangeRate)
																										ELSE (CASE WHEN @tintItemCalDiscount = 1 THEN (ISNULL(T2.monFinalPrice,0) * @fltExchangeRate) ELSE ISNULL(I.monListPrice,0) END)
																									END)) 
																	ELSE ((CASE WHEN (ISNULL(T1.numPromotionID,0) = @numTempPromotionID AND ISNULL(bitPromotionTriggered,0)=1 AND ISNULL(bitPromotionDiscount,0)=0) 
																				THEN T1.numUnits - ISNULL(PO.fltOfferTriggerValue,0) 
																				ELSE T1.numUnits
																			END) * (CASE 
																						WHEN (ISNULL(bitKitParent,0)=1 OR ISNULL(bitAssembly,0) = 1) AND ISNULL(bitCalAmtBasedonDepItems,0) = 1
																						THEN (dbo.GetCalculatedPriceForKitAssembly(@numDomainID,@numDivisionID,I.numItemCode,T1.numUnits,@numTempWarehouseItemID,ISNULL(tintKitAssemblyPriceBasedOn,1),0,0,ISNULL(T1.vcKitChildItems,''),@numCurrencyID,@fltExchangeRate) * @fltExchangeRate)
																						ELSE (CASE WHEN @tintItemCalDiscount = 1 THEN (ISNULL(T2.monFinalPrice,0) * @fltExchangeRate) ELSE ISNULL(I.monListPrice,0) END)
																					END)) 
																END)
														END)
													ELSE 0
												END)
											END
										)
						,bitDiscountType = (CASE WHEN @bitRemainingCheckRquired=0 THEN 0 ELSE 1 END)
						,numPromotionID=@numTempPromotionID
						,bitPromotionTriggered=(CASE WHEN bitPromotionTriggered=1 THEN 1 ELSE 0 END)
						,vcPromotionDescription=@vcShortDesc
						,bitChanged=1
						,bitPromotionDiscount=1
					FROM
						@TEMP T1
					INNER JOIN
						Item I
					ON
						T1.numItemCode = I.numItemCode
					LEFT JOIN
						PromotionOffer PO
					ON
						T1.numPromotionID = PO.numProId
					CROSS APPLY
						dbo.fn_GetItemPriceBasedOnMethod(@numDomainID,@numDivisionID,T1.numItemCode,T1.numUnits,0,0,T1.numWarehouseItemID,T1.vcKitChildItems,@numCurrencyID) T2
					WHERE
						ID=@i
						AND (ISNULL(T1.numPromotionID,0) = 0 OR (ISNULL(T1.numPromotionID,0) = @numTempPromotionID AND ISNULL(bitPromotionDiscount,0)=0))
						AND 1 = (CASE 
										WHEN (ISNULL(T1.numPromotionID,0) = @numTempPromotionID AND ISNULL(bitPromotionTriggered,0)=1 AND ISNULL(bitPromotionDiscount,0)=0) 
										THEN (CASE WHEN (CASE WHEN PO.tintOfferTriggerValueType = 2 THEN ISNULL(monTotalAmount,0) ELSE ISNULL(numUnits,0) END) >= ISNULL(PO.fltOfferTriggerValue,0) THEN 1 ELSE 0 END) 
										ELSE 1 
									END)
						AND 1 = (CASE 
									WHEN @tintDiscoutBaseOn = 1 -- Selected Items
									THEN
										(CASE WHEN (SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId=@numTempPromotionID AND tintRecordType=6 AND tintType=1 AND numValue=I.numItemCode) > 0 THEN 1 ELSE 0 END)
									WHEN @tintDiscoutBaseOn = 2 -- Selected Item Classification
									THEN 
										(CASE WHEN (SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId=@numTempPromotionID AND tintRecordType=6 AND tintType=2 AND numValue=ISNULL(I.numItemClassification,0)) > 0 THEN 1 ELSE 0 END)
									WHEN @tintDiscoutBaseOn = 3 -- Related Items
									THEN
										(CASE WHEN (SELECT COUNT(*) FROM SimilarItems WHERE numDomainId=@numDomainID AND numParentItemCode=@numItemCode AND numItemCode=I.numItemCode) > 0 THEN 1 ELSE 0 END)
									WHEN @tintDiscoutBaseOn IN  (4,5) -- Item with list price lesser or equal
									THEN
										(CASE WHEN ISNULL(I.monListPrice,0) <= ISNULL((SELECT monListPrice FROM Item WHERE numItemCode=@numItemCode),0) AND ISNULL(T1.bitPromotionTriggered,0)=0 THEN 1 ELSE 0 END)
									WHEN @tintDiscoutBaseOn = 6 -- Selected Items
									THEN
										(CASE WHEN (SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId=@numTempPromotionID AND tintRecordType=6 AND tintType=4 AND numValue=I.numItemCode) > 0 THEN 1 ELSE 0 END)
									ELSE 0
								END)
				END

				SET @i = @i + 1
			END

			SET @j = @j + 1
		END

		-- IF ANY COUPON BASE PROMOTION IS TRIGERRED BUT THERE ARE NO ITEMS TO APPLY PROMOTION DISCOUNT THEN CLEAR PROMOTIO TRIGGER
		IF (SELECT 
				COUNT(*) 
			FROM 
				@TEMP T1 
			INNER JOIN
				PromotionOffer
			ON
				T1.numPromotionID=PromotionOffer.numProId
				AND ISNULL(PromotionOffer.bitUseOrderPromotion,0) = 1
			WHERE 
				ISNULL(T1.numPromotionID,0) > 0
				AND ISNULL(T1.bitPromotionTriggered,0)=1 
				AND ISNULL(T1.bitPromotionDiscount,0)=0 
				AND (SELECT COUNT(*) FROM @TEMP T2 WHERE T2.numPromotionID=T1.numPromotionID AND ISNULL(bitPromotionTriggered,0) = 0) = 0) > 0
		BEGIN
			UPDATE
				T1
			SET
				T1.numPromotionID = 0
				,T1.bitPromotionTriggered=0
				,T1.bitPromotionDiscount=0
				,T1.vcPromotionDescription=''
				,T1.bitChanged=1
				,T1.monUnitPrice=ISNULL(T2.monPrice,0)
				,T1.fltDiscount=ISNULL(T2.decDiscount,0)
				,T1.bitDiscountType=ISNULL(T2.tintDisountType,0)
			FROM 
				@TEMP T1 
			INNER JOIN
				PromotionOffer
			ON
				T1.numPromotionID=PromotionOffer.numProId
				AND ISNULL(PromotionOffer.bitUseOrderPromotion,0) = 1
			CROSS APPLY
				dbo.fn_GetItemPriceBasedOnMethod(@numDomainID,@numDivisionID,T1.numItemCode,T1.numUnits,0,0,T1.numWarehouseItemID,T1.vcKitChildItems,@numCurrencyID) T2
			WHERE 
				ISNULL(T1.numPromotionID,0) > 0
				AND ISNULL(T1.bitPromotionTriggered,0)=1 
				AND ISNULL(T1.bitPromotionDiscount,0)=0 
				AND (SELECT COUNT(*) FROM @TEMP T2 WHERE T2.numPromotionID=T1.numPromotionID AND ISNULL(bitPromotionTriggered,0) = 0) = 0

			SET @j = 1
			SET @jCount = ISNULL((SELECT COUNT(*) FROM @TEMPPromotion),0)

			WHILE @j <= @jCount
			BEGIN
				SELECT 
					@numTempPromotionID=numPromotionID
					,@numItemCode=numTriggerItemCode
					,@numTempWarehouseItemID=numWarehouseItemID
					,@numUnits = numUnits
					,@monTotalAmount=monTotalAmount
				FROM 
					@TEMPPromotion 
				WHERE 
					ID = @j

				SELECT
					@fltDiscountValue=ISNULL(fltDiscountValue,0)
					,@tintDiscountType=ISNULL(tintDiscountType,0)
					,@tintDiscoutBaseOn=tintDiscoutBaseOn
					,@vcShortDesc=ISNULL(vcShortDesc,'')
					,@tintItemCalDiscount=ISNULL(tintItemCalDiscount,1)
				FROM
					PromotionOffer
				WHERE
					numProId=@numTempPromotionID

				-- If discount is based on amount or quantity than we have to checked remaining amount or qty left to give as discount 
				IF @tintDiscountType = 2 OR @tintDiscountType = 3
				BEGIN
					SET @bitRemainingCheckRquired = 1
				END
				ELSE
				BEGIN
					SET @bitRemainingCheckRquired = 0
					SET @numRemainingPromotion = 0
				END

				-- If promotion is valid than check whether any item left to apply promotion
				SET @i = 1

				WHILE @i <= @iCount
				BEGIN
					IF @bitRemainingCheckRquired=1
					BEGIN
						IF @tintDiscountType = 2 -- Discount by amount
						BEGIN
							SET @numRemainingPromotion = @fltDiscountValue - ISNULL((SELECT SUM(fltDiscount) FROM @TEMP WHERE numPromotionID = @numTempPromotionID	AND ISNULL(bitDiscountType,0)=1 AND bitPromotionDiscount = 1),0)
						END
						ELSE IF @tintDiscountType = 3 -- Discount by quantity
						BEGIN
							IF @tintDiscoutBaseOn IN (5,6)
							BEGIN
								SET @numRemainingPromotion = @fltDiscountValue - ISNULL((SELECT SUM(numUnits) FROM @TEMP WHERE numPromotionID = @numTempPromotionID	AND ISNULL(bitDiscountType,0)=1 AND bitPromotionDiscount = 1),0)
							END
							ELSE
							BEGIN
								SET @numRemainingPromotion = @fltDiscountValue - ISNULL((SELECT SUM(fltDiscount/monUnitPrice) FROM @TEMP WHERE numPromotionID = @numTempPromotionID	AND ISNULL(bitDiscountType,0)=1 AND bitPromotionDiscount = 1),0)
							END
						END
					END

					IF @bitRemainingCheckRquired=0 OR (@bitRemainingCheckRquired=1 AND @numRemainingPromotion > 0)
					BEGIN
						UPDATE
							T1
						SET
							monUnitPrice= CASE 
												WHEN (ISNULL(bitKitParent,0)=1 OR ISNULL(bitAssembly,0) = 1) AND ISNULL(bitCalAmtBasedonDepItems,0) = 1
												THEN (dbo.GetCalculatedPriceForKitAssembly(@numDomainID,@numDivisionID,I.numItemCode,T1.numUnits,@numTempWarehouseItemID,ISNULL(tintKitAssemblyPriceBasedOn,1),0,0,ISNULL(T1.vcKitChildItems,''),@numCurrencyID,@fltExchangeRate) * @fltExchangeRate)
												ELSE (CASE WHEN @tintItemCalDiscount = 1 THEN (ISNULL(T2.monFinalPrice,0) * @fltExchangeRate) ELSE ISNULL(I.monListPrice,0) END)
											END
							,fltDiscount = (CASE 
												WHEN @bitRemainingCheckRquired=0 
												THEN 
													@fltDiscountValue
												ELSE 
													(CASE 
														WHEN @tintDiscountType = 2 -- Discount by amount
														THEN
															(CASE 
																WHEN (T1.numUnits * (CASE 
																						WHEN (ISNULL(bitKitParent,0)=1 OR ISNULL(bitAssembly,0) = 1) AND ISNULL(bitCalAmtBasedonDepItems,0) = 1
																						THEN (dbo.GetCalculatedPriceForKitAssembly(@numDomainID,@numDivisionID,I.numItemCode,T1.numUnits,@numTempWarehouseItemID,ISNULL(tintKitAssemblyPriceBasedOn,1),0,0,ISNULL(T1.vcKitChildItems,''),@numCurrencyID,@fltExchangeRate) * @fltExchangeRate)
																						ELSE (CASE WHEN @tintItemCalDiscount = 1 THEN (ISNULL(T2.monFinalPrice,0) * @fltExchangeRate) ELSE ISNULL(I.monListPrice,0) END)
																					END)) >= @numRemainingPromotion 
																THEN @numRemainingPromotion 
																ELSE (T1.numUnits * (CASE 
																									WHEN (ISNULL(bitKitParent,0)=1 OR ISNULL(bitAssembly,0) = 1) AND ISNULL(bitCalAmtBasedonDepItems,0) = 1
																									THEN (dbo.GetCalculatedPriceForKitAssembly(@numDomainID,@numDivisionID,I.numItemCode,T1.numUnits,@numTempWarehouseItemID,ISNULL(tintKitAssemblyPriceBasedOn,1),0,0,ISNULL(T1.vcKitChildItems,''),@numCurrencyID,@fltExchangeRate) * @fltExchangeRate)
																									ELSE (CASE WHEN @tintItemCalDiscount = 1 THEN (ISNULL(T2.monFinalPrice,0) * @fltExchangeRate) ELSE ISNULL(I.monListPrice,0) END)
																								END)) 
															END)
														WHEN @tintDiscountType = 3 -- Discount by quantity
														THEN
															(CASE 
																WHEN @tintDiscoutBaseOn IN (5,6)
																THEN
																	(CASE 
																		WHEN (CASE WHEN (ISNULL(T1.numPromotionID,0) = @numTempPromotionID AND ISNULL(bitPromotionTriggered,0)=1 AND ISNULL(bitPromotionDiscount,0)=0) 
																				THEN T1.numUnits - ISNULL(PO.fltOfferTriggerValue,0) 
																				ELSE T1.numUnits
																			END) >= @numRemainingPromotion 
																		THEN @numRemainingPromotion 
																		ELSE (CASE WHEN (ISNULL(T1.numPromotionID,0) = @numTempPromotionID AND ISNULL(bitPromotionTriggered,0)=1 AND ISNULL(bitPromotionDiscount,0)=0) 
																				THEN T1.numUnits - ISNULL(PO.fltOfferTriggerValue,0) 
																				ELSE T1.numUnits
																			END )
																	END) * (CASE 
																				WHEN (CASE WHEN @tintItemCalDiscount = 1 THEN (ISNULL(T2.monFinalPrice,0) * @fltExchangeRate) ELSE ISNULL(I.monListPrice,0) END) > @monDiscountedItemPrice
																				THEN ((CASE WHEN @tintItemCalDiscount = 1 THEN (ISNULL(T2.monFinalPrice,0) * @fltExchangeRate) ELSE ISNULL(I.monListPrice,0) END) - @monDiscountedItemPrice)
																				ELSE 
																					(CASE WHEN @tintItemCalDiscount = 1 THEN (ISNULL(T2.monFinalPrice,0) * @fltExchangeRate) ELSE ISNULL(I.monListPrice,0) END) 
																			END)
																ELSE
																	(CASE 
																		WHEN (CASE WHEN (ISNULL(T1.numPromotionID,0) = @numTempPromotionID AND ISNULL(bitPromotionTriggered,0)=1 AND ISNULL(bitPromotionDiscount,0)=0) 
																		THEN T1.numUnits - ISNULL(PO.fltOfferTriggerValue,0) 
																		ELSE T1.numUnits
																	END) >= @numRemainingPromotion 
																		THEN (@numRemainingPromotion * (CASE 
																											WHEN (ISNULL(bitKitParent,0)=1 OR ISNULL(bitAssembly,0) = 1) AND ISNULL(bitCalAmtBasedonDepItems,0) = 1
																											THEN (dbo.GetCalculatedPriceForKitAssembly(@numDomainID,@numDivisionID,I.numItemCode,T1.numUnits,@numTempWarehouseItemID,ISNULL(tintKitAssemblyPriceBasedOn,1),0,0,ISNULL(T1.vcKitChildItems,''),@numCurrencyID,@fltExchangeRate) * @fltExchangeRate)
																											ELSE (CASE WHEN @tintItemCalDiscount = 1 THEN (ISNULL(T2.monFinalPrice,0) * @fltExchangeRate) ELSE ISNULL(I.monListPrice,0) END)
																										END)) 
																		ELSE ((CASE WHEN (ISNULL(T1.numPromotionID,0) = @numTempPromotionID AND ISNULL(bitPromotionTriggered,0)=1 AND ISNULL(bitPromotionDiscount,0)=0) 
																		THEN T1.numUnits - ISNULL(PO.fltOfferTriggerValue,0) 
																		ELSE T1.numUnits
																	END) * (CASE 
																								WHEN (ISNULL(bitKitParent,0)=1 OR ISNULL(bitAssembly,0) = 1) AND ISNULL(bitCalAmtBasedonDepItems,0) = 1
																								THEN (dbo.GetCalculatedPriceForKitAssembly(@numDomainID,@numDivisionID,I.numItemCode,T1.numUnits,@numTempWarehouseItemID,ISNULL(tintKitAssemblyPriceBasedOn,1),0,0,ISNULL(T1.vcKitChildItems,''),@numCurrencyID,@fltExchangeRate) * @fltExchangeRate)
																								ELSE (CASE WHEN @tintItemCalDiscount = 1 THEN (ISNULL(T2.monFinalPrice,0) * @fltExchangeRate) ELSE ISNULL(I.monListPrice,0) END)
																							END)) 
																	END)
															END)
														ELSE 0
													END)
												END
											)
							,bitDiscountType = (CASE WHEN @bitRemainingCheckRquired=0 THEN 0 ELSE 1 END)
							,numPromotionID=@numTempPromotionID
							,bitPromotionTriggered=(CASE WHEN bitPromotionTriggered=1 THEN 1 ELSE 0 END)
							,vcPromotionDescription=@vcShortDesc
							,bitChanged=1
							,bitPromotionDiscount=1
						FROM
							@TEMP T1
						INNER JOIN
							Item I
						ON
							T1.numItemCode = I.numItemCode
						LEFT JOIN
							PromotionOffer PO
						ON
							T1.numPromotionID = PO.numProId
						CROSS APPLY
							dbo.fn_GetItemPriceBasedOnMethod(@numDomainID,@numDivisionID,T1.numItemCode,T1.numUnits,0,0,T1.numWarehouseItemID,T1.vcKitChildItems,@numCurrencyID) T2
						WHERE
							ID=@i
							AND (ISNULL(T1.numPromotionID,0) = 0 OR (ISNULL(T1.numPromotionID,0) = @numTempPromotionID AND ISNULL(bitPromotionDiscount,0)=0))
							AND 1 = (CASE 
										WHEN (ISNULL(T1.numPromotionID,0) = @numTempPromotionID AND ISNULL(bitPromotionDiscount,0)=0) 
										THEN (CASE WHEN (CASE WHEN PO.tintOfferTriggerValueType = 2 THEN ISNULL(monTotalAmount,0) ELSE ISNULL(numUnits,0) END) >= ISNULL(PO.fltOfferTriggerValue,0) THEN 1 ELSE 0 END) 
										ELSE 1 
									END)
							AND 1 = (CASE 
										WHEN @tintDiscoutBaseOn = 1 -- Selected Items
										THEN
											(CASE WHEN (SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId=@numTempPromotionID AND tintRecordType=6 AND tintType=1 AND numValue=I.numItemCode) > 0 THEN 1 ELSE 0 END)
										WHEN @tintDiscoutBaseOn = 2 -- Selected Item Classification
										THEN 
											(CASE WHEN (SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId=@numTempPromotionID AND tintRecordType=6 AND tintType=2 AND numValue=ISNULL(I.numItemClassification,0)) > 0 THEN 1 ELSE 0 END)
										WHEN @tintDiscoutBaseOn = 3 -- Related Items
										THEN
											(CASE WHEN (SELECT COUNT(*) FROM SimilarItems WHERE numDomainId=@numDomainID AND numParentItemCode=@numItemCode AND numItemCode=I.numItemCode) > 0 THEN 1 ELSE 0 END)
										WHEN @tintDiscoutBaseOn IN (4,5) -- Item with list price lesser or equal
										THEN
											(CASE WHEN ISNULL(I.monListPrice,0) <= ISNULL((SELECT monListPrice FROM Item WHERE numItemCode=@numItemCode),0) AND ISNULL(T1.bitPromotionTriggered,0)=0 THEN 1 ELSE 0 END)
										WHEN @tintDiscoutBaseOn = 6 -- Selected Items
										THEN
											(CASE WHEN (SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId=@numTempPromotionID AND tintRecordType=6 AND tintType=4 AND numValue=I.numItemCode) > 0 THEN 1 ELSE 0 END)
										ELSE 0
									END)
					END

					SET @i = @i + 1
				END

				SET @j = @j + 1
			END
		END
	END

	UPDATE 
		@TEMP 
	SET 
		monUnitPrice = (CASE WHEN @fltExchangeRate <> 1 THEN CAST((ISNULL(monUnitPrice,0) / @fltExchangeRate) AS INT) ELSE ISNULL(monUnitPrice,0) END) 
		,fltDiscount = (CASE WHEN ISNULL(bitDiscountType,0) = 0 THEN (CASE WHEN @fltExchangeRate <> 1 THEN CAST((ISNULL(fltDiscount,0) / @fltExchangeRate) AS INT) ELSE fltDiscount END) ELSE fltDiscount END)
	WHERE 
		bitChanged=1

	SELECT * FROM @TEMP WHERE bitChanged=1
END
GO