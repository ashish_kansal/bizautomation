
GO
/****** Object:  StoredProcedure [dbo].[USP_ManageTaxItem]    Script Date: 09/25/2009 16:24:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created By Anoop Jayaraj
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ManageTaxItem')
DROP PROCEDURE USP_ManageTaxItem
GO
CREATE PROCEDURE [dbo].[USP_ManageTaxItem]
@numTaxItemID as numeric(9),
@vcTaxName as varchar(100),
@numDomainID as numeric(9),
@numAccountID as numeric(9)
as

declare @numSalesBizDocFormFieldID as numeric(9)
declare @numPurBizDocFormFieldID as numeric(9)
declare @numBizDocSummaryFormFieldID as numeric(9)
--declare @numChartOfAcntID as numeric(9)
declare @numFieldID as numeric(9)

if @numTaxItemID =0 
begin
insert into DycFieldMaster
(numModuleID, vcFieldName, vcFieldType, vcAssociatedControlType, vcDbColumnName, vcListItemType, numListID, bitDeleted, vcFieldDataType, vcOrigDbColumnName, bitAllowEdit, vcLookBackTableName, bitDefault, [order], bitInResults, numDomainID)
values
(3,@vcTaxName,'R','EditBox',@vcTaxName,'',0,'False','M','vcTax','True','Item','False','0','True',@numDomainID)
set @numFieldID=@@identity

insert into DycFormField_Mapping
(numModuleID,numFormID,numFieldID, numDomainID)
values
(3,7,@numFieldID,@numDomainID)

insert into DycFormField_Mapping
(numModuleID,numFormID,numFieldID, numDomainID)
values
(3,8,@numFieldID,@numDomainID)

insert into DycFormField_Mapping
(numModuleID,numFormID,numFieldID, numDomainID)
values
(3,16,@numFieldID,@numDomainID)

--Commented by chintan -Account ID Will be provided form frontend
--declare @numAccounid as numeric(9)
--select top 1 @numAccounid=numAccountId from  Chart_Of_Accounts where numParntAcntId is null and numDomainId=@numDomainId
--Insert into Chart_Of_Accounts(numParntAcntId,numAcntType,vcCatgyName,vcCatgyDescription,numOriginalOpeningBal,numOpeningBal,dtOpeningDate,bitActive,bitFixed,chBizDocItems,numDomainId)                                                      
--Values(@numAccounid,827,@vcTaxName,@vcTaxName,Null,Null,getutcdate(),1,0,'OT',@numDomainId) 
--set @numChartOfAcntID=@@identity

insert into TaxItems(vcTaxName, numSalesBizDocFormFieldID,numPurBizDocFormFieldID, numBizDocSummaryFormFieldID, numDomainID,numChartOfAcntID)
values
(@vcTaxName,@numFieldID,@numFieldID,@numFieldID,@numDomainID,@numAccountid)

end
else
begin

update  TaxItems set vcTaxName=@vcTaxName,@numSalesBizDocFormFieldID=numSalesBizDocFormFieldID,
@numPurBizDocFormFieldID=numPurBizDocFormFieldID,
@numBizDocSummaryFormFieldID=numBizDocSummaryFormFieldID,
@numAccountid=numChartOfAcntID
where numTaxItemID=@numTaxItemID

update DycFieldMaster set vcFieldName=@vcTaxName,vcDbColumnName=@vcTaxName where numFieldID=@numSalesBizDocFormFieldID
--update Chart_Of_Accounts set vcCatgyName=@vcTaxName where numAccountId=@numChartOfAcntID

end
