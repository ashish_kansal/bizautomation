SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
                    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_WorkOrder_InsertRecursive')
DROP PROCEDURE USP_WorkOrder_InsertRecursive
GO
CREATE PROCEDURE [dbo].[USP_WorkOrder_InsertRecursive]
	@numOppID AS NUMERIC(18,0),
	@numOppItemID NUMERIC(18,0),
	@numOppChildItemID NUMERIC(18,0),
	@numOppKitChildItemID NUMERIC(18,0),
	@numItemCode AS NUMERIC(18,0),
	@numQty AS FLOAT,
	@numWarehouseItemID AS NUMERIC(18,0),
	@numDomainID AS NUMERIC(18,0),
	@numUserCntID AS NUMERIC(18,0)=0,
	@numQtyShipped AS FLOAT,
	@numParentWOID AS NUMERIC(18,0),
	@bitFromWorkOrderScreen AS BIT
AS
BEGIN
	DECLARE @numWOID NUMERIC(18,0)
	DECLARE @vcInstruction VARCHAR(2000)
	DECLARE @numAssignedTo NUMERIC(18,0)
	DECLARE @bintCompletionDate DATETIME
	DECLARE @numWarehouseID NUMERIC(18,0)
	DECLARE @numBuildProcessId NUMERIC(18,0)
	DECLARE @dtmStartDate DATETIME
	DECLARE @dtmEndDate DATETIME

	IF NOT EXISTS (SELECT 
						numWOId
					FROM 
						WorkOrder 
					WHERE 
						numDomainID=@numDomainID 
						AND numItemCode=@numItemCode
						AND numParentWOID=@numParentWOID)
	BEGIN
		SELECT @numWarehouseID=numWareHouseID FROM WareHouseItems WHERE numWareHouseItemID = @numWarehouseItemID

		IF (SELECT 
				COUNT(*)
			FROM 
				item                                
			INNER JOIN 
				ItemDetails Dtl 
			ON 
				numChildItemID=numItemCode
			WHERE 
				numItemKitID=@numItemCode
				AND item.charItemType = 'P'
				AND NOT EXISTS (SELECT numWareHouseItemId FROM WarehouseItems WHERE numItemID=Dtl.numChildItemID AND numWareHouseID=@numWarehouseID)) > 0
		BEGIN
			RAISERROR('SELECTED_WAREHOUSE_IS_NOT_AVAILABLE_FOR_ALL_CHILD_ITEMS',16,1)
			RETURN
		END

		SELECT 
			@numBuildProcessId = ISNULL(Item.numBusinessProcessId,0) 
			,@numAssignedTo=ISNULL(Sales_process_List_Master.numBuildManager,0)
		FROM 
			Item 
		INNER JOIN 
			Sales_process_List_Master 
		ON 
			Item.numBusinessProcessId=Sales_process_List_Master.Slp_Id 
		WHERE 
			numItemCode=@numItemCode

		SELECT 
			@vcInstruction=vcInstruction
			,@bintCompletionDate=bintCompliationDate 
			,@dtmStartDate=dtmStartDate
			,@dtmEndDate=dtmEndDate
		FROM 
			WorkOrder 
		WHERE 
			numWOId=@numParentWOID

		INSERT INTO WorkOrder
		(
			numItemCode,numQtyItemsReq,numWareHouseItemId,numCreatedBy,bintCreatedDate,numDomainID,numWOStatus,numOppId,numOppItemID,numOppChildItemID,numOppKitChildItemID,numParentWOID,vcInstruction,numAssignedTo,bintCompliationDate,numBuildProcessId,dtmStartDate,dtmEndDate
		)
		VALUES
		(
			@numItemCode,@numQty,@numWarehouseItemID,@numUserCntID,getutcdate(),@numDomainID,0,@numOppID,@numOppItemID,@numOppChildItemID,@numOppKitChildItemID,@numParentWOID,@vcInstruction,@numAssignedTo,@bintCompletionDate,@numBuildProcessId,@dtmStartDate,@dtmEndDate
		)

		SELECT @numWOID = SCOPE_IDENTITY()

		EXEC dbo.USP_UpdateNameTemplateValueForWorkOrder 3,@numDomainID,@numWOId
		IF(@numBuildProcessId>0)
		BEGIN
				INSERT  INTO Sales_process_List_Master ( Slp_Name,
													 numdomainid,
													 pro_type,
													 numCreatedby,
													 dtCreatedon,
													 numModifedby,
													 dtModifiedOn,tintConfiguration,bitAutomaticStartTimer,bitAssigntoTeams,numOppId,numTaskValidatorId,
													 numProjectId,numWorkOrderId)
			 SELECT Slp_Name,
					numdomainid,
					pro_type,
					numCreatedby,
					dtCreatedon,
					numModifedby,
					dtModifiedOn,tintConfiguration,bitAutomaticStartTimer,bitAssigntoTeams,0,numTaskValidatorId,0,@numWOId FROM Sales_process_List_Master WHERE Slp_Id=@numBuildProcessId    
				DECLARE @numNewProcessId AS NUMERIC(18,0)=0
				SET @numNewProcessId=(SELECT SCOPE_IDENTITY())  
				UPDATE WorkOrder SET numBuildProcessId=@numNewProcessId WHERE numWOId=@numWOId
				INSERT INTO StagePercentageDetails(
				numStagePercentageId, 
				tintConfiguration, 
				vcStageName, 
				numDomainId, 
				numCreatedBy, 
				bintCreatedDate, 
				numModifiedBy, 
				bintModifiedDate, 
				slp_id, 
				numAssignTo, 
				vcMileStoneName, 
				tintPercentage, 
				tinProgressPercentage, 
				dtStartDate, 
				numParentStageID, 
				intDueDays, 
				numProjectID, 
				numOppID, 
				vcDescription, 
				bitIsDueDaysUsed,
				numTeamId, 
				bitRunningDynamicMode,
				numStageOrder,
				numWorkOrderId
			)
			SELECT
				numStagePercentageId, 
				tintConfiguration, 
				vcStageName, 
				numDomainId, 
				@numUserCntID, 
				GETDATE(), 
				@numUserCntID, 
				GETDATE(), 
				@numNewProcessId, 
				numAssignTo, 
				vcMileStoneName, 
				tintPercentage, 
				tinProgressPercentage, 
				GETDATE(), 
				numParentStageID, 
				intDueDays, 
				0, 
				0, 
				vcDescription, 
				bitIsDueDaysUsed,
				numTeamId, 
				bitRunningDynamicMode,
				numStageOrder,
				@numWOId
			FROM
				StagePercentageDetails
			WHERE
				slp_id=@numBuildProcessId	

				INSERT INTO StagePercentageDetailsTask(
				numStageDetailsId, 
				vcTaskName, 
				numHours, 
				numMinutes, 
				numAssignTo, 
				numDomainID, 
				numCreatedBy, 
				dtmCreatedOn,
				numOppId,
				numProjectId,
				numParentTaskId,
				bitDefaultTask,
				bitSavedTask,
				numOrder,
				numReferenceTaskId,
				numWorkOrderId
			)
			SELECT 
				(SELECT TOP 1 SFD.numStageDetailsId FROM StagePercentageDetails AS SFD WHERE SFD.vcStageName=SP.vcStageName AND SFD.numWorkOrderId=@numWOId),
				vcTaskName,
				numHours,
				numMinutes,
				ST.numAssignTo,
				@numDomainID,
				@numUserCntID,
				GETDATE(),
				0,
				0,
				0,
				1,
				CASE WHEN SP.bitRunningDynamicMode=1 THEN 0 ELSE 1 END,
				numOrder,
				numTaskId,
				@numWOId
			FROM 
				StagePercentageDetailsTask AS ST
			LEFT JOIN
				StagePercentageDetails As SP
			ON
				ST.numStageDetailsId=SP.numStageDetailsId
			WHERE
				ISNULL(ST.numOppId,0)=0 AND ISNULL(ST.numProjectId,0)=0 AND
				ST.numStageDetailsId IN(SELECT numStageDetailsId FROM 
				StagePercentageDetails As ST
			WHERE
				ST.slp_id=@numBuildProcessId)
			ORDER BY ST.numOrder
		END

		INSERT INTO [WorkOrderDetails] 
		(
			numWOId,numItemKitID,numChildItemID,numQtyItemsReq,numWareHouseItemId,vcItemDesc,sintOrder,numQtyItemsReq_Orig,numUOMId
		)
		SELECT 
			@numWOId,numItemKitID,numItemCode,
			CAST(((DTL.numQtyItemsReq * ISNULL(dbo.fn_UOMConversion(Dtl.numUOMId,item.numItemCode,item.numDomainID,item.numBaseUnit),1)) * @numQty)AS FLOAT),
			isnull((SELECT TOP 1 numWareHouseItemId FROM WarehouseItems WHERE numItemID=item.numItemCode AND numWareHouseID=@numWarehouseID ORDER BY ISNULL(numWLocationID,0),numWareHouseItemID),0),
			ISNULL(Dtl.vcItemDesc,txtItemDesc),
			ISNULL(sintOrder,0),
			DTL.numQtyItemsReq,
			Dtl.numUOMId 
		FROM 
			item                                
		INNER JOIN 
			ItemDetails Dtl 
		ON 
			numChildItemID=numItemCode
		WHERE 
			numItemKitID=@numItemCode

		DECLARE @Description AS VARCHAR(1000)

		--UPDATE ON ORDER OF ASSEMBLY
		UPDATE 
			WareHouseItems
		SET    
			numOnOrder= ISNULL(numOnOrder,0) + @numQty,
			dtModified = GETDATE() 
		WHERE   
			numWareHouseItemID = @numWareHouseItemID 

		SET @Description=CONCAT('Work Order Created (Qty:',@numQty,')')

		--MAKE ENTRY IN WAREHOUSE TRACKING TABLE
		EXEC dbo.USP_ManageWareHouseItems_Tracking
		@numWareHouseItemID = @numWareHouseItemID, --  numeric(9, 0)
		@numReferenceID = @numWOId, --  numeric(9, 0)
		@tintRefType = 2, --  tinyint
		@vcDescription = @Description, --  varchar(100)
		@numModifiedBy = @numUserCntID,
		@numDomainID = @numDomainID

		EXEC USP_ManageInventoryWorkOrder @numWOID,@numDomainID,@numUserCntID
	END
END