
GO
/****** Object:  StoredProcedure [dbo].[USP_GetBizDocStatusApprove]    Script Date: 02/10/2010 08:35:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--exec USP_GetBizDocStatusApprove @numDomainID=72
--
--select * from BizDocStatusApprove

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetBizDocStatusApprove')
DROP PROCEDURE USP_GetBizDocStatusApprove
GO
CREATE PROCEDURE [dbo].[USP_GetBizDocStatusApprove]
@numDomainID int
as

select numBizDocStatusAppId as numBizDocStatusID,
 numBizDocTypeID AS BizDocTypeID,
BDT.vcData as BizDocTypeValue,
numBizDocStatusID AS BizDocStatusID,
BDS.vcData as BizDocStatusValue,
numEmployeeID AS EmployeeID, 
vcFirstName + ' ' + vcLastName as EmployeeValue,				
numActionTypeID AS ActionTypeID,
ACT.vcData as ActionTypeValue
				from (SELECT * FROM BizDocStatusApprove WHERE numDomainID=@numDomainID) BZA 
				INNER JOIN 
				(SELECT * FROM View_GetMasterListItems WHERE (constFlag=1 or  numDomainID=@numDomainID)) BDT ON BDT.numListItemID=BZA.numBizDocTypeID
				INNER JOIN 
				(SELECT * FROM View_GetMasterListItems WHERE (constFlag=1 or  numDomainID=@numDomainID)) BDS on BDS.numListItemID=BZA.numBizDocStatusID
				INNER JOIN 
				(SELECT * FROM View_GetMasterListItems WHERE (constFlag=1 or  numDomainID=@numDomainID)) ACT ON ACT.numListItemID=BZA.numActionTypeID 
				INNER JOIN 
				(SELECT * FROM AdditionalContactsInformation  WHERE numDomainID=@numDomainID) ACI ON ACI.numContactID=BZA.numEMployeeID

--select * from BizDocStatusApprove
--SELECT * FROM VIEW_MASTERDETAILS WHERE numDomainID=72 and numListItemID=287
--select * from VIEW_MASTERDETAILS where numListItemID=287
--
--select * from View_GetMasterListItems
--exec USP_GetMasterListItems @ListID=27,@numDomainID=72

--SELECT * FROM View_GetMasterListItems WHERE (constFlag=0 or  numDomainID=72) and numListItemID=287

--SELECT * FROM View_GetMasterListItems  where numListItemID=287

