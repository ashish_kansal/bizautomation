--exec USP_ListDomain @numType=2,@numDomainId=1,@numSubscriberID=103
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ListDomain')
DROP PROCEDURE USP_ListDomain
GO
CREATE PROCEDURE [dbo].[USP_ListDomain]
(@numType int,
@numDomainId int,
@numSubscriberID int,
@vcDomainCode varchar(50)='')
as
begin
if @numType=1
	begin
		select vcDomainName ,numDomainID from Domain where numParentDomainID is null and numSubscriberID is  null
	end
else if @numType=2
	begin
		select vcCompanyName+ '-Subscriber' as vcDomainName ,a.numSubscriberID as numDomainID  from Subscribers A inner join DivisionMaster b on a.numDivisionID=b.numDivisionID
		inner join CompanyInfo C ON c.numCompanyID=b.numCompanyId AND a.numSubscriberID=@numSubscriberID
		UNION
		select vcDomainName + '-Domain' as vcDomainName ,numDomainID from Domain 
		where numDomainID not in ( @numDomainId) and 
		numDomainID in (select DN.numDomainID from Domain DN OUTER APPLY(SELECT * FROM Domain) DNA where DN.vcDomainCode like DNA.vcDomainCode + '%' 
		and  DNA.numSubscriberID=@numSubscriberID) and 
		numSubscriberID is not null and numParentDomainID is not null
	end
else if  @numType=3
	begin
		select vcDomainName ,numDomainID,vcDomainCode from Domain where numSubscriberID =@numSubscriberID and numParentDomainID=0;
	end
else if @numType=4
	begin
		select DN.vcDomainName ,DN.numDomainID,DN.vcDomainCode,DNA.vcDomainName as ParentDomainName,DNA.numDomainID as numParentDomainID from Domain DN OUTER APPLY(SELECT * FROM Domain) DNA
			 where  DNA.numDomainID=DN.numParentDomainID and DNA.numSubscriberID=@numSubscriberID and DN.numSubscriberID =@numSubscriberID and DN.vcDomainCode like @vcDomainCode + '%'
			order by DN.vcDomainCode;
	end
end
