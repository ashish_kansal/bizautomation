GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_MassPurchaseFulfillmentConfiguration_Save')
DROP PROCEDURE dbo.USP_MassPurchaseFulfillmentConfiguration_Save
GO
CREATE PROCEDURE [dbo].[USP_MassPurchaseFulfillmentConfiguration_Save]
(
	@numDomainID NUMERIC(18,0)
    ,@numUserCntID NUMERIC(18,0)
	,@bitGroupByOrderForReceive BIT
	,@bitGroupByOrderForPutAway BIT
	,@bitGroupByOrderForBill BIT
	,@bitGroupByOrderForClose BIT
	,@bitShowOnlyFullyReceived BIT
	,@bitReceiveBillOnClose BIT
	,@tintScanValue TINYINT
	,@tintBillType TINYINT
)
AS 
BEGIN
	IF EXISTS (SELECT ID FROM MassPurchaseFulfillmentConfiguration WHERE numDomainID=@numDomainID AND numUserCntID=@numUserCntID)
	BEGIN
		UPDATE
			MassPurchaseFulfillmentConfiguration
		SET
			bitGroupByOrderForReceive=@bitGroupByOrderForReceive
			,bitGroupByOrderForPutAway=@bitGroupByOrderForPutAway
			,bitGroupByOrderForBill=@bitGroupByOrderForBill
			,bitGroupByOrderForClose=@bitGroupByOrderForClose
			,bitShowOnlyFullyReceived=@bitShowOnlyFullyReceived
			,bitReceiveBillOnClose=@bitReceiveBillOnClose
			,tintScanValue=@tintScanValue
			,tintBillType=@tintBillType
		WHERE
			numDomainID=@numDomainID 
			AND numUserCntID=@numUserCntID
	END
	ELSE
	BEGIN
		INSERT INTO MassPurchaseFulfillmentConfiguration
		(
			numDomainID 
			,numUserCntID
			,bitGroupByOrderForReceive
			,bitGroupByOrderForPutAway
			,bitGroupByOrderForBill
			,bitGroupByOrderForClose
			,bitShowOnlyFullyReceived
			,bitReceiveBillOnClose
			,tintScanValue
			,tintBillType
		)
		VALUES
		(
			@numDomainID 
			,@numUserCntID
			,@bitGroupByOrderForReceive
			,@bitGroupByOrderForPutAway
			,@bitGroupByOrderForBill
			,@bitGroupByOrderForClose
			,@bitShowOnlyFullyReceived
			,@bitReceiveBillOnClose
			,@tintScanValue
			,@tintBillType
		)
	END
END
GO