SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetUsedAssetAccountsByDomain')
DROP PROCEDURE dbo.USP_GetUsedAssetAccountsByDomain
GO
CREATE PROCEDURE USP_GetUsedAssetAccountsByDomain
	@numDomainID NUMERIC(18,0)
AS
BEGIN

	SET NOCOUNT ON;

    SELECT
		DISTINCT numAssetChartAcntId
	FROM
		Item
	WHERE
		numDomainID = @numDomainID
		AND ISNULL(numAssetChartAcntId,0) > 0
END
GO
