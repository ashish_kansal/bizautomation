SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_WareHouseItems_CheckForDuplicateAttr')
DROP PROCEDURE USP_WareHouseItems_CheckForDuplicateAttr
GO
CREATE PROCEDURE [dbo].[USP_WareHouseItems_CheckForDuplicateAttr]  
	@numDomainID AS NUMERIC(18,0),
	@numWareHouseID AS NUMERIC(18,0),
	@numWLocationID AS NUMERIC(18,0),
	@numItemCode AS NUMERIC(18,0),
	@strFieldList AS TEXT
AS
BEGIN
	DECLARE @bitDuplicate AS BIT = 0
	
	IF DATALENGTH(@strFieldList)>2
	BEGIN
		--INSERT CUSTOM FIELDS BASE ON WAREHOUSEITEMS
		DECLARE @hDoc AS INT     
		EXEC sp_xml_preparedocument @hDoc OUTPUT, @strFieldList                                                                        

		
	    DECLARE @TempTable TABLE 
		(
			ID INT IDENTITY(1,1),
			Fld_ID NUMERIC(18,0),
			Fld_Value VARCHAR(300)
		)   
	                                      
		INSERT INTO @TempTable 
		(
			Fld_ID,
			Fld_Value
		)    
		SELECT
			*          
		FROM 
			OPENXML (@hDoc,'/NewDataSet/CusFlds',2)                                                  
		WITH 
			(Fld_ID NUMERIC(18,0),Fld_Value VARCHAR(300))        

		DECLARE @strAttribute VARCHAR(300) = ''
		DECLARE @i AS INT = 1
		DECLARE @COUNT AS INT 
		DECLARE @numFldID AS NUMERIC(18,0)
		DECLARE @numFldValue AS VARCHAR(300) 
		SELECT @COUNT = COUNT(*) FROM @TempTable

		WHILE @i <= @COUNT
		BEGIN
			SELECT @numFldID=Fld_ID,@numFldValue=Fld_Value FROM @TempTable WHERE ID=@i

			IF ISNUMERIC(@numFldValue)=1
			BEGIN
				SELECT @strAttribute=@strAttribute+Fld_label FROM CFW_Fld_Master WHERE Fld_id=@numFldID AND Grp_id = 9
                
				IF LEN(@strAttribute) > 0
				BEGIN
					SELECT @strAttribute=@strAttribute+'  - '+ vcData +',' FROM ListDetails WHERE numListItemID=@numFldValue
				END
			END
			ELSE
			BEGIN
				SELECT @strAttribute=@strAttribute+Fld_label from  CFW_Fld_Master where Fld_id=@numFldID AND Grp_id = 9
				IF LEN(@strAttribute) > 0
				BEGIN
					SELECT @strAttribute=@strAttribute+'  - -,' 
				END
			END

			SET @i = @i + 1
		END

		IF                                                                 
			(SELECT
				COUNT(*)
			FROM
				WareHouseItems WI                               
			WHERE
				WI.numWarehouseID = @numWareHouseID
				AND ISNULL(WI.numWLocationID,0) = ISNULL(@numWLocationID,0)
				AND WI.numItemID = @numItemCode
				AND WI.numDomainID = @numDomainID
				AND dbo.fn_GetAttributes(WI.numWarehouseItemID,0) = @strAttribute
			) > 0
		BEGIN
			SET @bitDuplicate = 1
		END

		EXEC sp_xml_removedocument @hDoc 
	END	  

	SELECT @bitDuplicate
END

