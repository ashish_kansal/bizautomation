/****** Object:  StoredProcedure [dbo].[USP_GetImportWareHouseItems]    Script Date: 07/26/2008 16:18:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj                              
GO
-- exec USP_GetImportWareHouseItems @numDomainID=1,@numItemGroupID=0,@byteMode=1,@bitSerialized=0,@bitLotNo=0
-- exec USP_GetImportWareHouseItems @numDomainID=1,@numItemGroupID=125,@byteMode=1,@bitSerialized=0,@bitLotNo=0
-- exec USP_GetImportWareHouseItems @numDomainID=1,@numItemGroupID=125,@byteMode=2,@bitSerialized=1,@bitLotNo=0
-- exec USP_GetImportWareHouseItems @numDomainID=1,@numItemGroupID=125,@byteMode=2,@bitSerialized=0,@bitLotNo=1
-- exec USP_GetImportWareHouseItems @numDomainID=150,@numItemGroupID=384,@byteMode=2,@bitSerialized=1,@bitLotNo=0
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_GetImportWareHouseItems' ) 
    DROP PROCEDURE USP_GetImportWareHouseItems
GO
CREATE PROCEDURE [dbo].[USP_GetImportWareHouseItems]
    @numDomainID AS NUMERIC(9) = 0,
    @numItemGroupID AS NUMERIC(9) = 0,
    @byteMode AS TINYINT = 0,
    @bitSerialized AS BIT = 0,
    @bitLotNo AS BIT = 0
AS 
    DECLARE @str AS VARCHAR(2000) ;
    SET @str = ''               
    DECLARE @str1 AS VARCHAR(2000)               
    DECLARE @ColName AS VARCHAR(25)                      
                        
--Create a Temporary table to hold data                                                            
    CREATE TABLE #tempTable
        (
          ID INT IDENTITY
                 PRIMARY KEY,
          vcFormFieldName NVARCHAR(50),
          numFormFieldId NUMERIC(18),
          tintOrder TINYINT,
          cCtype CHAR(1),
          vcAssociatedControlType NVARCHAR(50),
          vcDbColumnName NVARCHAR(50),
          vcLookBackTableName VARCHAR(50)
        )                         
	
    DECLARE @strSQL AS NVARCHAR(MAX)
    DECLARE @strWHERE AS NVARCHAR(MAX)
	
    SET @strWHERE = ' WHERE 1=1 AND numFormID = 48 AND ISNULL(DFFM.bitImport,0) <> 0 '
    SET @strSQL = '                        
    INSERT INTO #tempTable ( vcFormFieldName,numFormFieldId,tintOrder,cCtype,vcAssociatedControlType,vcDbColumnName,vcLookBackTableName )
            SELECT  DFM.vcFieldName AS vcFormFieldName,
                    DFM.numFieldID AS numFormFieldId,
                    ISNULL(DFFM.[order], 0) as tintOrder,
                    ''R'' as cCtype,
                    DFM.vcAssociatedControlType AS vcAssociatedControlType,
                    DFM.vcDbColumnName AS vcDbColumnName,
                    ISNULL(DFM.vcLookBackTableName, '''') AS vcLookBackTableName
            FROM    dbo.DycFieldMaster DFM
            INNER JOIN dycFormField_Mapping DFFM ON DFM.numFieldId = DFFM.numFieldID ' 
            
    IF ISNULL(@numItemGroupID, 0) > 0 
        BEGIN
            SET @strWHERE = @strWHERE
                + ' AND DFM.numFieldID IN (SELECT numFieldId FROM dycFormField_Mapping WHERE numFormID = 48 )  '								      
			
            SET @strSQL = @strSQL + @strWHERE + ' 
            UNION
            SELECT  Fld_label AS vcFormFieldName,
                    Fld_id AS numFormFieldId,
                    100 AS tintOrder,
                    ''C'' AS cCtype,
                    Fld_Type AS vcAssociatedControlType,
                    '''' AS vcDbColumnName,
                    '''' AS vcLookBackTableName
            FROM    CFW_Fld_Master
                    JOIN ItemGroupsDTL ON numOppAccAttrID = Fld_id
            WHERE   numItemGroupID = ' + CAST(@numItemGroupID AS VARCHAR(100))
                + ' AND tintType = 2
            ORDER BY tintOrder ' 
            
        END
    ELSE 
        BEGIN
            SET @strWHERE = @strWHERE
                + ' AND DFM.numFieldID NOT IN (SELECT numFieldId FROM dycFormField_Mapping WHERE numFormID = 48 AND vcFieldName IN (''UPC (M)'',''SKU (M)'',''Serial No'',''Comments'',''Lot Qty''))'
			
            SET @strSQL = @strSQL + @strWHERE + ' ORDER BY tintOrder ' 
            
        END
                  
    PRINT @strSQL
    EXEC SP_EXECUTESQL @strSQL
	
    --SELECT  * FROM    #tempTable
        
    DECLARE @ID AS INT,
        @vcFormFieldName AS NVARCHAR(50),
        @numFormFieldId AS NUMERIC(18),
        @tintOrder AS TINYINT,
        @cCtype AS CHAR(1),
        @vcAssociatedControlType AS NVARCHAR(50),
        @vcDbColumnName AS NVARCHAR(50),
        @vcLookBackTableName VARCHAR(20)

    IF @byteMode = 1 
        BEGIN     
            SELECT TOP 1
                    @ID = ID,
                    @vcFormFieldName = vcFormFieldName,
                    @numFormFieldId = numFormFieldId,
                    @tintOrder = tintOrder,
                    @cCtype = cCtype,
                    @vcAssociatedControlType = vcAssociatedControlType,
                    @vcDbColumnName = vcDbColumnName,
                    @vcLookBackTableName = vcLookBackTableName
            FROM    #tempTable                         
                         
            WHILE @ID > 0                        
                BEGIN 
                    IF @ID > 1 
                        SET @str = @str + ','

                    IF @vcDbColumnName = 'numWareHouseID' 
                        SET @vcDbColumnName = 'vcWareHouse'

                    IF @cCtype = 'R' 
                        IF @vcDbColumnName = 'vcLocation' 
                            BEGIN
                                SET @str = @str + 'WL.' + @vcDbColumnName
                                    + ' as [' + @vcFormFieldName + ']'    
                            END
                        ELSE 
                            BEGIN
                                SET @str = @str + @vcLookBackTableName + '.'
                                    + @vcDbColumnName + ' as ['
                                    + @vcFormFieldName + ']'                                        
                            END

                    ELSE 
                        SET @str = @str + ' dbo.GetCustFldItemsValue('
                            + CONVERT(VARCHAR(15), @numFormFieldId)
                            + ',9,WareHouseItems.numWareHouseItemID,0,'''
                            + @vcAssociatedControlType + ''') as ['
                            + @vcFormFieldName + ']'      
                                                              
                    SELECT TOP 1
                            @ID = ID,
                            @vcFormFieldName = vcFormFieldName,
                            @numFormFieldId = numFormFieldId,
                            @tintOrder = tintOrder,
                            @cCtype = cCtype,
                            @vcAssociatedControlType = vcAssociatedControlType,
                            @vcDbColumnName = vcDbColumnName,
                            @vcLookBackTableName = vcLookBackTableName
                    FROM    #tempTable
                    WHERE   ID > @ID 
   
                    IF @@rowcount = 0 
                        SET @ID = 0                        
                END                  
                      
            SET @str = RTRIM(@str)       
            PRINT 'SQL :' + @str                       
            SET @str1 = ' select ' + @str
                + ' from Item 
						LEFT join WareHouseItems on Item.numItemCode=WareHouseItems.numItemID
						LEFT join Warehouses on Warehouses.numWareHouseID=WareHouseItems.numWareHouseID                                    
						LEFT JOIN WareHouseItmsDTL ON WareHouseItmsDTL.numWareHouseItemID = dbo.WareHouseItems.numWareHouseItemID
						LEFT JOIN dbo.WarehouseLocation WL ON WareHouseItems.numWLocationID = WL.numWLocationID and WareHouseItems.numDomainID = WL.numDomainID 
						where Item.numDomainID='
                + CONVERT(VARCHAR(15), @numDomainID)
                + ' AND Item.numItemGroup='
                + CONVERT(VARCHAR(15), @numItemGroupID)
                + ' ORDER BY dbo.WareHouseItems.numWareHouseItemID DESC'
				   
            PRINT ( @str1 )
            EXEC ( @str1
                ) 
                           
        END

    ELSE 
        IF @byteMode = 2 
            BEGIN
                SELECT TOP 1
                        @ID = ID,
                        @vcFormFieldName = vcFormFieldName,
                        @numFormFieldId = numFormFieldId,
                        @tintOrder = tintOrder,
                        @cCtype = cCtype,
                        @vcAssociatedControlType = vcAssociatedControlType,
                        @vcDbColumnName = vcDbColumnName,
                        @vcLookBackTableName = vcLookBackTableName
                FROM    #tempTable                         
                         
                WHILE @ID > 0                        
                    BEGIN 
                        IF @ID > 1 
                            SET @str = @str + ','

                        IF @vcDbColumnName = 'numWareHouseID' 
                            SET @vcDbColumnName = 'vcWareHouse'

                        IF @cCtype = 'R' 
                            IF @vcDbColumnName = 'vcLocation' 
                                BEGIN
                                    SET @str = @str + 'WL.' + @vcDbColumnName
                                        + ' as [' + @vcFormFieldName + ']'    
                                END
                            ELSE 
                                BEGIN
                                    SET @str = @str + @vcLookBackTableName
                                        + '.' + @vcDbColumnName + ' as ['
                                        + @vcFormFieldName + ']'                                        	
                                END
								
                        ELSE 
                            SET @str = @str + ' dbo.GetCustFldItemsValue('
                                + CONVERT(VARCHAR(15), @numFormFieldId)
                                + ',9,numWareHouseItmsDTLID,0,'''
                                + @vcAssociatedControlType + ''') as ['
                                + @vcFormFieldName + ']'                                        
							
                        SELECT TOP 1
                                @ID = ID,
                                @vcFormFieldName = vcFormFieldName,
                                @numFormFieldId = numFormFieldId,
                                @tintOrder = tintOrder,
                                @cCtype = cCtype,
                                @vcAssociatedControlType = vcAssociatedControlType,
                                @vcDbColumnName = vcDbColumnName,
                                @vcLookBackTableName = vcLookBackTableName
                        FROM    #tempTable
                        WHERE   ID > @ID                        
   
                        IF @@rowcount = 0 
                            SET @ID = 0                        
                    END                        
                
                PRINT @str1       
                SET @str1 = ' SELECT   DISTINCT ' + @str 
							--+ ',vcSerialNo AS ' + CASE WHEN @bitSerialized = 1 THEN '[Serial No]' WHEN @bitLotNo = 1 THEN '[LOT No]' END 
                    + ',WareHouseItmsDTL.vcComments AS Comments'

                IF @bitLotNo = 1 
                    SET @str1 = @str1 + ',WareHouseItmsDTL.numQty AS Qty'

                SET @str1 = @str1
                    + ' FROM ITEM 
						LEFT JOIN WareHouseItems  ON Item.numItemCode=WareHouseItems.numItemID
						LEFT JOIN Warehouses  ON Warehouses.numWareHouseID=WareHouseItems.numWareHouseID      
						LEFT JOIN WareHouseItmsDTL ON WareHouseItmsDTL.numWareHouseItemID = dbo.WareHouseItems.numWareHouseItemID
						LEFT JOIN dbo.WarehouseLocation WL ON WareHouseItems.numWLocationID = WL.numWLocationID and WareHouseItems.numDomainID = WL.numDomainID 
						WHERE Item.numDomainID='
                    + CONVERT(VARCHAR(15), @numDomainID)
                    + ' and Item.numItemGroup='
                    + CONVERT(VARCHAR(15), @numItemGroupID)
                    + '
						AND ISNULL(WareHouseItmsDTL.numQty,0)>0 AND (Item.bitSerialized='
                    + CONVERT(VARCHAR(15), @bitSerialized)
                    + ' AND Item.bitLotNo=' + CONVERT(VARCHAR(15), @bitLotNo)
                    + ')'
                    + ' ORDER BY dbo.WareHouseItems.numWareHouseItemID DESC'
                  
                SET @str1 = REPLACE(@str1, '[Serial No]', '[Serial/Lot #s]')
                SET @str1 = REPLACE(@str1, 'WareHouseItmsDTL.vcSerialNo',
                                    'NULL')
                SET @str1 = REPLACE(@str1, 'WareHouseItmsDTL.vcComments',
                                    'NULL')
                SET @str1 = REPLACE(@str1, 'WareHouseItmsDTL.vcSerialNo',
                                    'NULL')
                SET @str1 = REPLACE(@str1, 'WareHouseItmsDTL.numQty', 'NULL')
                SET @str1 = REPLACE(@str1, 'WareHouseItmsDTL.numQty', 'NULL')
				
                PRINT ( @str1 )           
                EXEC ( @str1
                    )
            END

        ELSE 
            BEGIN	
                SELECT  vcFormFieldName,
                        numFormFieldId,
                        tintOrder,
                        cCtype,
                        vcAssociatedControlType,
                        vcDbColumnName,
                        vcLookBackTableName
                FROM    #tempTable
                ORDER BY tintOrder
            END

    DROP TABLE #tempTable
GO
