SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_OpportunityMaster_QtyAddedToBizDocType')
DROP PROCEDURE USP_OpportunityMaster_QtyAddedToBizDocType
GO
CREATE PROCEDURE [dbo].[USP_OpportunityMaster_QtyAddedToBizDocType]
(                        
 @numDomainID AS NUMERIC(18,0),
 @numOppId AS NUMERIC(18,0),
 @numBizDocId AS NUMERIC(18,0)
)                        
AS 
BEGIN
	DECLARE @bitAllQtyAddedToBizDocType AS BIT = 1

	IF (SELECT 
			COUNT(*) 
		FROM 
		(
			SELECT
				OI.numoppitemtCode,
				ISNULL(OI.numUnitHour,0) AS OrderedQty,
				ISNULL(TempInvoice.QtyAddedToBizDocType,0) AS QtyAddedToBizDocType
			FROM
				OpportunityItems OI
			INNER JOIN
				Item I
			ON
				OI.numItemCode = I.numItemCode
			OUTER APPLY
			(
				SELECT
					SUM(OpportunityBizDocItems.numUnitHour) AS QtyAddedToBizDocType
				FROM
					OpportunityBizDocs
				INNER JOIN
					OpportunityBizDocItems 
				ON
					OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
				WHERE
					OpportunityBizDocs.numOppId = @numOppID
					AND OpportunityBizDocItems.numOppItemID = OI.numoppitemtCode
					AND ISNULL(OpportunityBizDocs.numBizDocId,0) = @numBizDocId
			) AS TempInvoice
			WHERE
				OI.numOppID = @numOppID
		) X
		WHERE

			X.OrderedQty <> X.QtyAddedToBizDocType) > 0
	BEGIN
		SET @bitAllQtyAddedToBizDocType = 0
	END

	SELECT @bitAllQtyAddedToBizDocType
END