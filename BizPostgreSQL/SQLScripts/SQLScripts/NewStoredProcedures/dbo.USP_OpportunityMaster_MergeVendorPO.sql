SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_OpportunityMaster_MergeVendorPO')
DROP PROCEDURE USP_OpportunityMaster_MergeVendorPO
GO
CREATE PROCEDURE [dbo].[USP_OpportunityMaster_MergeVendorPO]  
	@numDomainID NUMERIC(18,0),
	@numUserCntID NUMERIC(18,0),
	@vcXML VARCHAR(4000)
AS  
BEGIN  
BEGIN TRY
BEGIN TRANSACTION
	DECLARE @docHandle int;

	DECLARE @Vendor TABLE
	(
		ID INT IDENTITY(1,1),
		numVendorID NUMERIC(18,0),
		numMasterPOID NUMERIC(18,0)
	)

	DECLARE @VendorPOToMerger TABLE
	(
		numVendorID NUMERIC(18,0),
		numPOID NUMERIC(18,0)
	)

	DECLARE @TEMPMasterItems TABLE
	(
		numoppitemtCode NUMERIC(18,0),numItemCode NUMERIC(18,0),numUnitHour FLOAT,monPrice DECIMAL(20,5),
		monTotAmount DECIMAL(20,5),vcItemDesc VARCHAR(1000),numWarehouseItmsID NUMERIC(18,0),numToWarehouseItemID NUMERIC(18,0),
		Op_Flag INT, ItemType VARCHAR(50),DropShip BIT,bitDiscountType BIT, fltDiscount FLOAT, monTotAmtBefDiscount DECIMAL(20,5),
		vcItemName VARCHAR(300),numUOM NUMERIC(18,0), bitWorkOrder BIT,numVendorWareHouse NUMERIC(18,0),
		numShipmentMethod NUMERIC(18,0),numSOVendorId NUMERIC(18,0),numProjectID NUMERIC(18,0),
		numProjectStageID NUMERIC(18,0),Attributes VARCHAR(200),bitItemPriceApprovalRequired BIT
	)

	DECLARE @TEMPChildItems TABLE
	(
		numRowID INT, numoppitemtCode NUMERIC(18,0),numItemCode NUMERIC(18,0),numUnitHour FLOAT,monPrice DECIMAL(20,5),
		monTotAmount DECIMAL(20,5),vcItemDesc VARCHAR(1000),numWarehouseItmsID NUMERIC(18,0),numToWarehouseItemID NUMERIC(18,0),
		Op_Flag INT, ItemType VARCHAR(50),DropShip BIT,bitDiscountType BIT, fltDiscount FLOAT, monTotAmtBefDiscount DECIMAL(20,5),
		vcItemName VARCHAR(300),numUOM NUMERIC(18,0), bitWorkOrder BIT,numVendorWareHouse NUMERIC(18,0),
		numShipmentMethod NUMERIC(18,0),numSOVendorId NUMERIC(18,0),numProjectID NUMERIC(18,0),
		numProjectStageID NUMERIC(18,0),Attributes VARCHAR(200),bitItemPriceApprovalRequired BIT
	)

	DECLARE @TEMPPO TABLE
	(
		ID INT,
		numPOID NUMERIC(18,0)
	)

	EXEC sp_xml_preparedocument @docHandle OUTPUT, @vcXML;

	--RETRIVE VENDOR AND MASTER PURCHASE ORDER IDS
	INSERT INTO 
		@Vendor
	SELECT 
		VendorID,
		MasterPOID
	FROM 
		OPENXML(@docHandle, N'/Vendors/Vendor',2) 
	WITH 
		(
			VendorID NUMERIC(18,0),
			MasterPOID NUMERIC(18,0)
		);  
	
	--RETIRIVE ORDERS OF EACH VENDOR
	INSERT INTO 
		@VendorPOToMerger
	SELECT 
		VendorID,
		OrderID
	FROM 
		OPENXML(@docHandle, N'/Vendors/Vendor/Orders/OrderID',2) 
	WITH 
		(
			VendorID NUMERIC(18,0) '../../VendorID',
			OrderID NUMERIC(18,0) '.'
		);  

	DECLARE @i AS INT = 1
	DECLARE @iCOUNT AS INT
	SELECT @iCOUNT=COUNT(*) FROM @Vendor

	--LOOP ALL VENDORS
	WHILE @i <= @iCOUNT
	BEGIN
		DECLARE @VendorID AS NUMERIC(18,0) = 0
		DECLARE @MasterPOID AS NUMERIC(18,0) = 0
		DECLARE @numOppID  AS NUMERIC(18,0) = NULL                                                                        
		DECLARE @numContactId NUMERIC(18,0)=null                                                                        
		DECLARE @numDivisionId numeric(9)=null                                                                          
		DECLARE @tintSource numeric(9)=null                                                                          
		DECLARE @vcPOppName Varchar(100)=''                                                                 
		DECLARE @Comments varchar(1000)=''                                                                          
		DECLARE @bitPublicFlag bit=0                                                                                                                                                         
		DECLARE @monPAmount DECIMAL(20,5) =0                                                 
		DECLARE @numAssignedTo as numeric(9)=0                                                                                                                                                                                                                                                                                         
		DECLARE @strItems as VARCHAR(MAX)=null                                                                          
		DECLARE @strMilestone as VARCHAR(100)=null                                                                          
		DECLARE @dtEstimatedCloseDate datetime                                                                          
		DECLARE @CampaignID as numeric(9)=null                                                                          
		DECLARE @lngPConclAnalysis as numeric(9)=null                                                                         
		DECLARE @tintOppType as tinyint                                                                                                                                             
		DECLARE @tintActive as tinyint                                                              
		DECLARE @numSalesOrPurType as numeric(9)              
		DECLARE @numRecurringId as numeric(9)
		DECLARE @numCurrencyID as numeric(9)=0
		DECLARE @DealStatus as tinyint =0
		DECLARE @numStatus AS NUMERIC(9)
		DECLARE @vcOppRefOrderNo VARCHAR(100)
		DECLARE @numBillAddressId numeric(9)=0
		DECLARE @numShipAddressId numeric(9)=0
		DECLARE @bitStockTransfer BIT=0
		DECLARE @WebApiId INT = 0
		DECLARE @tintSourceType TINYINT=0
		DECLARE @bitDiscountType as bit
		DECLARE @fltDiscount  as float
		DECLARE @bitBillingTerms as bit
		DECLARE @intBillingDays as integer
		DECLARE @bitInterestType as bit
		DECLARE @fltInterest as float
		DECLARE @tintTaxOperator AS TINYINT
		DECLARE @numDiscountAcntType AS NUMERIC(9)
		DECLARE @vcWebApiOrderNo VARCHAR(100)=NULL
		DECLARE @vcCouponCode		VARCHAR(100) = ''
		DECLARE @vcMarketplaceOrderID VARCHAR(100)=NULL
		DECLARE @vcMarketplaceOrderDate  datetime=NULL
		DECLARE @vcMarketplaceOrderReportId VARCHAR(100)=NULL
		DECLARE @numPercentageComplete NUMERIC(9)
		DECLARE @bitUseShippersAccountNo BIT = 0
		DECLARE @bitUseMarkupShippingRate BIT = 0
		DECLARE @numMarkupShippingRate NUMERIC(19,2) = 0
		DECLARE @intUsedShippingCompany INT = 0
		DECLARE @numShipmentMethod NUMERIC(18,0)=0
		DECLARE @dtReleaseDate DATE = NULL
		DECLARE @numPartner NUMERIC(18,0)=0
		DECLARE @tintClickBtn INT=0
		DECLARE @numPartenerContactId NUMERIC(18,0)=0
		DECLARE @numAccountClass NUMERIC(18,0) = 0
		DECLARE @numWillCallWarehouseID NUMERIC(18,0) = 0
		DECLARE @numVendorAddressID NUMERIC(18,0) = 0

		--GET VENDOR AND MASTER PO ID
		SELECT @VendorID=numVendorID, @MasterPOID=numMasterPOID FROM @Vendor WHERE ID = @i

		--GET MASTER PO FIELDS 
		SELECT
			@numOppID=numOppId,@numContactId =numContactId,@numDivisionId=numDivisionId,@tintSource=tintSource,@vcPOppName=vcPOppName,@Comments=txtComments,
			@bitPublicFlag=bitPublicFlag,@monPAmount=monPAmount,@numAssignedTo=numAssignedTo,@strMilestone=null,@dtEstimatedCloseDate=intPEstimatedCloseDate,
			@CampaignID=numCampainID,@lngPConclAnalysis=lngPConclAnalysis,@tintOppType=tintOppType,@tintActive=tintActive,@numSalesOrPurType=numSalesOrPurType,
			@numRecurringId=null,@numCurrencyID=numCurrencyID,@DealStatus=tintOppStatus,@numStatus=numStatus,@vcOppRefOrderNo=vcOppRefOrderNo,@numBillAddressId=0,
			@numShipAddressId=0,@bitStockTransfer=0,@WebApiId=0,@tintSourceType=tintSourceType,@bitDiscountType=bitDiscountType,@fltDiscount=fltDiscount,
			@bitBillingTerms=bitBillingTerms,@intBillingDays=intBillingDays,@bitInterestType=bitInterestType,@fltInterest=fltInterest,@tintTaxOperator=tintTaxOperator,
			@numDiscountAcntType=numDiscountAcntType,@vcWebApiOrderNo=vcWebApiOrderNo,@vcCouponCode=vcCouponCode,@vcMarketplaceOrderID=vcMarketplaceOrderID,
			@vcMarketplaceOrderDate=bintCreatedDate,@vcMarketplaceOrderReportId=vcMarketplaceOrderReportId,@numPercentageComplete= numPercentageComplete,
			@bitUseShippersAccountNo=bitUseShippersAccountNo,@bitUseMarkupShippingRate=bitUseMarkupShippingRate,@numMarkupShippingRate=numMarkupShippingRate,
			@intUsedShippingCompany= intUsedShippingCompany,@numShipmentMethod=numShipmentMethod,@dtReleaseDate=dtReleaseDate,@numPartner=numPartner,@numPartenerContactId=numPartenerContact
			,@numAccountClass=numAccountClass,@numVendorAddressID=numVendorAddressID
		FROM
			OpportunityMaster
		WHERE
			numDomainId = @numDomainID
			AND numOppId = @MasterPOID 

		--CLEAR DATA OF PREVIOUS ITERATION
		DELETE FROM @TEMPMasterItems
		DELETE FROM @TEMPChildItems

		
		--GET ITEMS OF MASTER PO
		INSERT INTO 
			@TEMPMasterItems
		SELECT 
			numoppitemtCode,numItemCode,numUnitHour,monPrice,monTotAmount,vcItemDesc,numWarehouseItmsID,numToWarehouseItemID,
			2,vcType,bitDropShip,bitDiscountType,fltDiscount,monTotAmtBefDiscount,vcItemName,numUOMId,bitWorkOrder,numVendorWareHouse,
			numShipmentMethod,numSOVendorId,numProjectID,numProjectStageID,vcAttributes,bitItemPriceApprovalRequired
		FROM 
			OpportunityItems 
		WHERE 
			numOppId = @MasterPOID

		--GET ITEMS OF PO NEEDS TO BE MERGED TO MASTER PO
		INSERT INTO 
			@TEMPChildItems
		SELECT 
			ROW_NUMBER() OVER(ORDER BY numoppitemtCode),numoppitemtCode,numItemCode,numUnitHour,monPrice,monTotAmount,vcItemDesc,numWarehouseItmsID,numToWarehouseItemID,
			1,vcType,bitDropShip,bitDiscountType,fltDiscount,monTotAmtBefDiscount,vcItemName,numUOMId,bitWorkOrder,numVendorWareHouse,
			numShipmentMethod,numSOVendorId,numProjectID,numProjectStageID,vcAttributes,bitItemPriceApprovalRequired
		FROM 
			OpportunityItems 
		WHERE 
			numOppId IN (SELECT numPOID FROM @VendorPOToMerger WHERE numVendorID=@VendorID AND numPOID <> @MasterPOID)

		DECLARE @numChildItemCode NUMERIC(18,0) = 0
		DECLARE @numChildWaerehouseItemID NUMERIC(18,0) = 0
		DECLARE @numChildUnitHour FLOAT = 0
		DECLARE @monChildPrice DECIMAL(20,5) = 0
		DECLARE @K AS INT = 1
		DECLARE @KCOUNT AS INT
		SELECT @KCOUNT=COUNT(*) FROM @TEMPChildItems

		-- LOOP ITEMS OF EACH PO NEEDS TO MERGED
		WHILE @K <= @KCOUNT
		BEGIN
			SELECT 
				@numChildItemCode=numItemCode,
				@numChildWaerehouseItemID=ISNULL(numWarehouseItmsID,0),
				@numChildUnitHour=ISNULL(numUnitHour,0), 
				@monChildPrice=ISNULL(monPrice,0) 
			FROM 
				@TEMPChildItems 
			WHERE 
				numRowID = @K

			-- IF ITEM WITH SAME ITEM ID AND WAREHOUSE IS EXIST IN MASTER ORDER THEN UPDATE QTY IN MASTER PO AND SET PRICE TO WHICHEVER LOWER
			-- ELSE ADD ITEMS MASTER PO ITEMS
			IF EXISTS(SELECT numoppitemtCode FROM @TEMPMasterItems WHERE numItemCode=@numChildItemCode AND ISNULL(numWarehouseItmsID,0)=@numChildWaerehouseItemID)
			BEGIN
				UPDATE 
					@TEMPMasterItems
				SET 
					numUnitHour = ISNULL(numUnitHour,0) + @numChildUnitHour,
					monPrice = (CASE WHEN ISNULL(monPrice,0) > @monChildPrice THEN @monChildPrice ELSE monPrice END),
					monTotAmount = (ISNULL(numUnitHour,0) + @numChildUnitHour) * (CASE WHEN ISNULL(monPrice,0) > @monChildPrice THEN @monChildPrice ELSE monPrice END),
					monTotAmtBefDiscount = (ISNULL(numUnitHour,0) + @numChildUnitHour) * (CASE WHEN ISNULL(monPrice,0) > @monChildPrice THEN @monChildPrice ELSE monPrice END)
				WHERE
					numItemCode=@numChildItemCode 
					AND ISNULL(numWarehouseItmsID,0)=@numChildWaerehouseItemID
			END
			ELSE
			BEGIN
				INSERT INTO 
					@TEMPMasterItems
				SELECT 
					numoppitemtCode,numItemCode,numUnitHour,monPrice,monTotAmount,vcItemDesc,numWarehouseItmsID,numToWarehouseItemID,
					1,ItemType,DropShip,bitDiscountType,fltDiscount,monTotAmtBefDiscount,vcItemName,numUOM,bitWorkOrder,numVendorWareHouse,
					numShipmentMethod,numSOVendorId,numProjectID,numProjectStageID,Attributes,bitItemPriceApprovalRequired
				FROM 
					@TEMPChildItems 
				WHERE 
					numRowID = @K

			END

			SET @K = @K + 1
		END
		
		-- Op_Flag is 2(UPDATE) FOR MASTER PO AND 1(INSERT) FOR OTHER
		SET @strItems =(
							SELECT 
								numoppitemtCode,numItemCode,CAST(numUnitHour AS DECIMAL(18,8)) AS numUnitHour,monPrice,monTotAmount,vcItemDesc,numWarehouseItmsID,numToWarehouseItemID,
								Op_Flag, ItemType,DropShip,	bitDiscountType, CAST(fltDiscount AS DECIMAL(18,8)) AS fltDiscount, monTotAmtBefDiscount,	vcItemName,numUOM, bitWorkOrder,
								numVendorWareHouse,numShipmentMethod,numSOVendorId,numProjectID,numProjectStageID,Attributes,bitItemPriceApprovalRequired
							FROM 
								@TEMPMasterItems
							FOR XML RAW ('Item'), ROOT ('NewDataSet'), ELEMENTS
						)
		
		--UPDATE MASTER PO AND ADD MERGE PO ITEMS
		EXEC USP_OppManage @numOppID=@numOppID OUTPUT,@numContactId=@numContactId,@numDivisionId=@numDivisionId,@tintSource=@tintSource,@vcPOppName=@vcPOppName output,
							@Comments=@Comments,@bitPublicFlag=@bitPublicFlag,@numUserCntID=@numUserCntID,@monPAmount=@monPAmount,@numAssignedTo=@numAssignedTo,
							@numDomainId=@numDomainId,@strItems=@strItems,@strMilestone=@strMilestone,@dtEstimatedCloseDate=@dtEstimatedCloseDate,@CampaignID=@CampaignID,
							@lngPConclAnalysis=@lngPConclAnalysis,@tintOppType=@tintOppType,@tintActive=@tintActive,@numSalesOrPurType=@numSalesOrPurType,
							@numRecurringId=@numRecurringId,@numCurrencyID=@numCurrencyID,@DealStatus=@DealStatus,@numStatus=@numStatus,@vcOppRefOrderNo=@vcOppRefOrderNo,
							@numBillAddressId=@numBillAddressId,@numShipAddressId=@numShipAddressId,@bitStockTransfer=@bitStockTransfer,@WebApiId=@WebApiId,
							@tintSourceType=@tintSourceType,@bitDiscountType=@bitDiscountType,@fltDiscount=@fltDiscount,@bitBillingTerms=@bitBillingTerms,
							@intBillingDays=@intBillingDays,@bitInterestType=@bitInterestType,@fltInterest=@fltInterest,@tintTaxOperator=@tintTaxOperator,
							@numDiscountAcntType=@numDiscountAcntType,@vcWebApiOrderNo=@vcWebApiOrderNo,@vcCouponCode=@vcCouponCode,@vcMarketplaceOrderID=@vcMarketplaceOrderID,
							@vcMarketplaceOrderDate=@vcMarketplaceOrderDate,@vcMarketplaceOrderReportId=@vcMarketplaceOrderReportId,@numPercentageComplete=@numPercentageComplete,
							@bitUseShippersAccountNo=@bitUseShippersAccountNo,@bitUseMarkupShippingRate=@bitUseMarkupShippingRate,
							@numMarkupShippingRate=@numMarkupShippingRate,@intUsedShippingCompany=@intUsedShippingCompany,@numShipmentMethod=@numShipmentMethod,@dtReleaseDate=@dtReleaseDate,@numPartner=@numPartner,@numPartenerContactId=@numPartenerContactId
							,@numAccountClass=@numAccountClass,@numVendorAddressID=@numVendorAddressID
		
		-- DELETE MERGED PO

		---- CLEAR DATA OF PREVIOUS ITERATION
		DELETE FROM @TEMPPO

		---- GET ALL POS EXCEPT MASTER PO
		INSERT INTO @TEMPPO SELECT Row_Number() OVER(ORDER BY numPOID), numPOID FROM @VendorPOToMerger WHERE numVendorID=@VendorID AND numPOID <> @MasterPOID

		DECLARE @j AS INT = 1
		DECLARE @jCOUNT AS INT
		SELECT @jCOUNT=COUNT(*) FROM @TEMPPO

		WHILE @j <= @jCOUNT
		BEGIN
			DECLARE @POID AS INT = 0
			SELECT @POID=numPOID FROM @TEMPPO WHERE ID = @j
			
			EXEC USP_DeleteOppurtunity @numOppId=@POID,@numDomainID=@numDomainID,@numUserCntID=@numUserCntID

			SET @j = @j + 1
		END

		SET @i = @i + 1
	END
COMMIT
END TRY
BEGIN CATCH
	DECLARE @ErrorMessage NVARCHAR(4000)
	DECLARE @ErrorNumber INT
	DECLARE @ErrorSeverity INT
	DECLARE @ErrorState INT
	DECLARE @ErrorLine INT
	DECLARE @ErrorProcedure NVARCHAR(200)

	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;

	SELECT 
		@ErrorMessage = ERROR_MESSAGE(),
		@ErrorNumber = ERROR_NUMBER(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE(),
		@ErrorLine = ERROR_LINE(),
		@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

	RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);
END CATCH	
END
GO
