SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ReportListMaster_Top10ItemByProfitMargin')
DROP PROCEDURE USP_ReportListMaster_Top10ItemByProfitMargin
GO
CREATE PROCEDURE [dbo].[USP_ReportListMaster_Top10ItemByProfitMargin]
	@numDomainID NUMERIC(18,0)
AS
BEGIN 
	DECLARE @numShippingItemID NUMERIC(18,0)
	DECLARE @numDiscountItemID NUMERIC(18,0)
	SELECT @numShippingItemID=numShippingServiceItemID,@numDiscountItemID=numDiscountServiceItemID FROM Domain WHERE numDomainId=@numDomainID


	SELECT TOP 10
		numItemCode
		,vcItemName
		,CONCAT('~/Items/frmKitDetails.aspx?ItemCode=',numItemCode,'&frm=All Items') AS URL
		,(SUM(Profit)/SUM(monTotAmount)) * 100 BlendedProfit
	FROM
	(
		SELECT
			I.numItemCode
			,I.vcItemName
			,ISNULL(monTotAmount,0) monTotAmount
			,ISNULL(dbo.GetOrderItemProfitAmountOrMargin(@numDomainID,OM.numOppId,OI.numoppitemtCode,1),0) Profit
		FROM
			OpportunityMaster OM
		INNER JOIN
			OpportunityItems OI
		ON
			OM.numOppID = OI.numOppId
		INNER JOIN
			Item I
		ON
			OI.numItemCode = I.numItemCode
		WHERE
			OM.numDomainId=@numDomainID
			AND ISNULL(monTotAmount,0) <> 0
			AND ISNULL(numUnitHour,0) <> 0
			AND ISNULL(OM.tintOppType,0)=1
			AND ISNULL(OM.tintOppStatus,0)=1
			AND I.numItemCode NOT IN (@numShippingItemID,@numDiscountItemID)
	) TEMP
	GROUP BY
		numItemCode
		,vcItemName
	HAVING
		(SUM(Profit)/SUM(monTotAmount)) * 100 > 0
	ORDER BY
		(SUM(Profit)/SUM(monTotAmount)) * 100 DESC
END
GO