GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
            
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ChartOfAccounts_SaveAccountTypeOrder')
DROP PROCEDURE USP_ChartOfAccounts_SaveAccountTypeOrder
GO
CREATE PROCEDURE [dbo].[USP_ChartOfAccounts_SaveAccountTypeOrder]    
(
	@numDomainID NUMERIC(18,0)
	,@vcAccountTypes VARCHAR(MAX)
)
AS
BEGIN
	DECLARE @hDocItem int

	IF LEN(ISNULL(@vcAccountTypes,'')) > 0
	BEGIN
		EXEC sp_xml_preparedocument @hDocItem OUTPUT, @vcAccountTypes

		UPDATE
			ATD
		SET
			ATD.tintSortOrder = T1.tintSortOrder
		FROM
			AccountTypeDetail ATD
		INNER JOIN
		(
			SELECT
				numAccountTypeID,
				tintSortOrder
			FROM
				OPENXML (@hDocItem,'/NewDataSet/AccountType',2)
			WITH
			(
				numAccountTypeID NUMERIC(18,0),
				tintSortOrder INT
			)
		) AS T1
		ON 
			ATD.numAccountTypeID = T1.numAccountTypeID
		WHERE
			ATD.numDomainID = @numDomainID

		EXEC sp_xml_removedocument @hDocItem 
	END

	
END