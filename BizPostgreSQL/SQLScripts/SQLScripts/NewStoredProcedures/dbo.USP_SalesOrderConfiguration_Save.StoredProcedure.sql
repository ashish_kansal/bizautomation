GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_SalesOrderConfiguration_Save' ) 
    DROP PROCEDURE USP_SalesOrderConfiguration_Save
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author: Sandeep Patel
-- Create date: 18 Feb 2014
-- Description:	Saves new sales order form configuration
-- =============================================
CREATE PROCEDURE USP_SalesOrderConfiguration_Save
	@numSOCID NUMERIC(18,0) = NULL,
	@numDomainID NUMERIC(18,0) = NULL,
	@bitAutoFocusCustomer BIT,
	@bitAutoFocusItem BIT,
	@bitDisplayRootLocation BIT,
	@bitAutoAssignOrder BIT,
	@bitDisplayLocation BIT,
	@bitDisplayFinancialStamp BIT,
	@bitDisplayItemDetails BIT,
	@bitDisplayUnitCost BIT,
	@bitDisplayProfitTotal BIT,
	@bitDisplayShippingRates BIT,
	@bitDisplayPaymentMethods BIT,
	@bitCreateOpenBizDoc BIT,
	@numListItemID NUMERIC(18,0),
	@vcPaymentMethodIDs VARCHAR(100),
	@bitDisplayCurrency BIT,
	@bitDisplayAssignTo BIT,
	@bitDisplayTemplate BIT,
	@bitDisplayCouponDiscount BIT,
	@bitDisplayShippingCharges BIT,
	@bitDisplayDiscount BIT,
	@bitDisplaySaveNew BIT,
	@bitDisplayAddToCart BIT,
	@bitDisplayCreateInvoice BIT,
	@bitDisplayShipVia BIT,
	@bitDisplayComments BIT,
	@bitDisplayReleaseDate BIT,
	@bitDisplayCustomerPart#Entry BIT,
	@bitDisplayItemGridOrderAZ BIT,
	@bitDisplayItemGridItemID BIT,
	@bitDisplayItemGridSKU BIT,
	@bitDisplayItemGridUnitListPrice BIT,
	@bitDisplayItemGridUnitSalePrice BIT,
	@bitDisplayItemGridDiscount BIT,
	@bitDisplayItemGridItemRelaseDate BIT,
	@bitDisplayItemGridLocation BIT,
	@bitDisplayItemGridShipTo BIT,
	@bitDisplayItemGridOnHandAllocation BIT,
	@bitDisplayItemGridDescription BIT,
	@bitDisplayItemGridNotes BIT,
	@bitDisplayItemGridAttributes BIT,
	@bitDisplayItemGridInclusionDetail BIT,
	@bitDisplayItemGridItemClassification BIT,
	@bitDisplayExpectedDate BIT,
	@bitDisplayItemGridItemPromotion BIT,
	@bitDisplayApplyPromotionCode BIT,
	@bitDisplayPayButton BIT,
	@bitDisplayPOSPay BIT,
	@vcPOHiddenColumns VARCHAR(200)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	IF (SELECT COUNT(*) FROM SalesOrderConfiguration WHERE numDomainID = @numDomainID) = 0
	BEGIN
		INSERT INTO dbo.SalesOrderConfiguration
		(
			numDomainID,
			bitAutoFocusCustomer,
			bitAutoFocusItem,
			bitDisplayRootLocation,
			bitAutoAssignOrder,
			bitDisplayLocation,
			bitDisplayFinancialStamp,
			bitDisplayItemDetails,
			bitDisplayUnitCost,
			bitDisplayProfitTotal,
			bitDisplayShippingRates,
			bitDisplayPaymentMethods,
			bitCreateOpenBizDoc,
			numListItemID,
			vcPaymentMethodIDs,
			bitDisplayCurrency,
			bitDisplayAssignTo,
			bitDisplayTemplate,
			bitDisplayCouponDiscount,
			bitDisplayShippingCharges,
			bitDisplayDiscount,
			bitDisplaySaveNew,
			bitDisplayAddToCart,
			bitDisplayCreateInvoice,
			bitDisplayShipVia,
			bitDisplayComments,
			bitDisplayReleaseDate,
			bitDisplayCustomerPart#Entry,
			bitDisplayItemGridOrderAZ,
			bitDisplayItemGridItemID,
			bitDisplayItemGridSKU,
			bitDisplayItemGridUnitListPrice,
			bitDisplayItemGridUnitSalePrice,
			bitDisplayItemGridDiscount,
			bitDisplayItemGridItemRelaseDate,
			bitDisplayItemGridLocation,
			bitDisplayItemGridShipTo,
			bitDisplayItemGridOnHandAllocation,
			bitDisplayItemGridDescription,
			bitDisplayItemGridNotes,
			bitDisplayItemGridAttributes,
			bitDisplayItemGridInclusionDetail,
			bitDisplayItemGridItemClassification,
			bitDisplayExpectedDate,
			bitDisplayItemGridItemPromotion,
			bitDisplayApplyPromotionCode,
			bitDisplayPayButton,
			bitDisplayPOSPay,
			vcPOHiddenColumns
			)
		VALUES
		(
			@numDomainID,
			@bitAutoFocusCustomer,
			@bitAutoFocusItem,
			@bitDisplayRootLocation,
			@bitAutoAssignOrder,
			@bitDisplayLocation,
			@bitDisplayFinancialStamp,
			@bitDisplayItemDetails,
			@bitDisplayUnitCost,
			@bitDisplayProfitTotal,
			@bitDisplayShippingRates,
			@bitDisplayPaymentMethods,
			@bitCreateOpenBizDoc,
			@numListItemID,
			@vcPaymentMethodIDs,
			@bitDisplayCurrency,
			@bitDisplayAssignTo,
			@bitDisplayTemplate,
			@bitDisplayCouponDiscount,
			@bitDisplayShippingCharges,
			@bitDisplayDiscount,
			@bitDisplaySaveNew,
			@bitDisplayAddToCart,
			@bitDisplayCreateInvoice,
			@bitDisplayShipVia,
			@bitDisplayComments,
			@bitDisplayReleaseDate,
			@bitDisplayCustomerPart#Entry,
			@bitDisplayItemGridOrderAZ,
			@bitDisplayItemGridItemID,
			@bitDisplayItemGridSKU,
			@bitDisplayItemGridUnitListPrice,
			@bitDisplayItemGridUnitSalePrice,
			@bitDisplayItemGridDiscount,
			@bitDisplayItemGridItemRelaseDate,
			@bitDisplayItemGridLocation,
			@bitDisplayItemGridShipTo,
			@bitDisplayItemGridOnHandAllocation,
			@bitDisplayItemGridDescription,
			@bitDisplayItemGridNotes,
			@bitDisplayItemGridAttributes,
			@bitDisplayItemGridInclusionDetail,
			@bitDisplayItemGridItemClassification
			,@bitDisplayExpectedDate
			,@bitDisplayItemGridItemPromotion
			,@bitDisplayApplyPromotionCode
			,@bitDisplayPayButton
			,@bitDisplayPOSPay
			,@vcPOHiddenColumns
		)
	END
	ELSE
	BEGIN
		UPDATE
			dbo.SalesOrderConfiguration
		SET
			bitAutoFocusCustomer = @bitAutoFocusCustomer,
			bitAutoFocusItem = @bitAutoFocusItem,
			bitDisplayRootLocation = @bitDisplayRootLocation,
			bitAutoAssignOrder = @bitAutoAssignOrder,
			bitDisplayLocation = @bitDisplayLocation,
			bitDisplayFinancialStamp = @bitDisplayFinancialStamp,
			bitDisplayItemDetails = @bitDisplayItemDetails,
			bitDisplayUnitCost = @bitDisplayUnitCost,
			bitDisplayProfitTotal = @bitDisplayProfitTotal,
			bitDisplayShippingRates = @bitDisplayShippingRates,
			bitDisplayPaymentMethods = @bitDisplayPaymentMethods,
			bitCreateOpenBizDoc = @bitCreateOpenBizDoc,
			numListItemID = @numListItemID,
			vcPaymentMethodIDs  = @vcPaymentMethodIDs,
			bitDisplayCurrency = @bitDisplayCurrency,
			bitDisplayAssignTo = @bitDisplayAssignTo,
			bitDisplayTemplate = @bitDisplayTemplate,
			bitDisplayCouponDiscount = @bitDisplayCouponDiscount,
			bitDisplayShippingCharges = @bitDisplayShippingCharges,
			bitDisplayDiscount = @bitDisplayDiscount,
			bitDisplaySaveNew = @bitDisplaySaveNew,
			bitDisplayAddToCart = @bitDisplayAddToCart,
			bitDisplayCreateInvoice = @bitDisplayCreateInvoice,
			bitDisplayShipVia = @bitDisplayShipVia,
			bitDisplayComments = @bitDisplayComments,
			bitDisplayReleaseDate = @bitDisplayReleaseDate,
			bitDisplayCustomerPart#Entry = @bitDisplayCustomerPart#Entry,
			bitDisplayItemGridOrderAZ = @bitDisplayItemGridOrderAZ,
			bitDisplayItemGridItemID = @bitDisplayItemGridItemID,
			bitDisplayItemGridSKU = @bitDisplayItemGridSKU,
			bitDisplayItemGridUnitListPrice = @bitDisplayItemGridUnitListPrice,
			bitDisplayItemGridUnitSalePrice = @bitDisplayItemGridUnitSalePrice,
			bitDisplayItemGridDiscount = @bitDisplayItemGridDiscount,
			bitDisplayItemGridItemRelaseDate = @bitDisplayItemGridItemRelaseDate,
			bitDisplayItemGridLocation = @bitDisplayItemGridLocation,
			bitDisplayItemGridShipTo = @bitDisplayItemGridShipTo,
			bitDisplayItemGridOnHandAllocation = @bitDisplayItemGridOnHandAllocation,
			bitDisplayItemGridDescription = @bitDisplayItemGridDescription,
			bitDisplayItemGridNotes = @bitDisplayItemGridNotes,
			bitDisplayItemGridAttributes = @bitDisplayItemGridAttributes,
			bitDisplayItemGridInclusionDetail = @bitDisplayItemGridInclusionDetail,
			bitDisplayItemGridItemClassification = @bitDisplayItemGridItemClassification,
			bitDisplayExpectedDate=@bitDisplayExpectedDate
			,bitDisplayItemGridItemPromotion=@bitDisplayItemGridItemPromotion
			,bitDisplayApplyPromotionCode=@bitDisplayApplyPromotionCode
			,bitDisplayPayButton=@bitDisplayPayButton
			,bitDisplayPOSPay=@bitDisplayPOSPay
			,vcPOHiddenColumns=@vcPOHiddenColumns
		WHERE
			numDomainID = @numDomainID
	END
    
END
GO


