GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ElasticSearch_GetOrganizations')
DROP PROCEDURE dbo.USP_ElasticSearch_GetOrganizations
GO
CREATE PROCEDURE [dbo].[USP_ElasticSearch_GetOrganizations]
	@numDomainID NUMERIC(18,0)
	,@vcCompanyIds VARCHAR(MAX)
AS 
BEGIN
	DECLARE @CustomerSearchFields AS VARCHAR(MAX) = ''
	DECLARE @CustomerSearchCustomFields AS VARCHAR(MAX) = ''

	DECLARE @CustomerDisplayFields AS VARCHAR(MAX) = ''

	DECLARE @query AS NVARCHAR(MAX);

	----------------------------   Search Fields ---------------------------

	DECLARE @TEMPSearchFields TABLE
	(
		numFieldID NUMERIC(18,0)
		,vcDbColumnName VARCHAR(200)
		,tintOrder TINYINT
		,bitCustom BIT
	)

	INSERT INTO @TEMPSearchFields
	(
		numFieldID
		,vcDbColumnName
		,tintOrder
		,bitCustom
	)
	SELECT 
		numFieldId,
		vcDbColumnName,
		tintRow AS tintOrder,
		0 as Custom
    FROM 
		View_DynamicColumns
	WHERE 
		numFormId=97 AND numDomainID=@numDomainID AND 
		tintPageType=1 AND bitCustom=0  AND ISNULL(bitSettingField,0)=1 AND numRelCntType=0
	UNION   
	SELECT 
		numFieldId,
		vcFieldName,
		tintRow AS tintOrder,
		1 as Custom
    FROM 
		View_DynamicCustomColumns   
	WHERE 
		Grp_id=1 AND numFormId=97 AND numDomainID=@numDomainID AND 
		tintPageType=1 AND bitCustom=1 AND numRelCntType=0

	IF EXISTS (SELECT * FROM @TEMPSearchFields)
	BEGIN
		SELECT 
			@CustomerSearchFields = COALESCE (@CustomerSearchFields,'') + CAST(vcDbColumnName AS VARCHAR(100)) + ' AS Search_' + CAST(vcDbColumnName AS VARCHAR(100)) + ', '	
		FROM 
			@TEMPSearchFields 
		WHERE
			ISNULL(bitCustom,0) = 0
		ORDER BY
			tintOrder ASC
		
		-- Removes last , from string
		IF DATALENGTH(@CustomerSearchFields) > 0
			SET @CustomerSearchFields = LEFT(@CustomerSearchFields, LEN(@CustomerSearchFields) - 1)

		IF EXISTS(SELECT * FROM @TEMPSearchFields WHERE ISNULL(bitCustom,0) = 1)
		BEGIN
			SELECT 
				@CustomerSearchCustomFields = COALESCE (@CustomerSearchCustomFields,'') + 'ISNULL(CFW' + CAST(numFieldId AS VARCHAR(100)) + ','''') AS ' + 'Search_CFW' + CAST(numFieldId AS VARCHAR(100)) + ','
			FROM 
				@TEMPSearchFields 
			WHERE
				ISNULL(bitCustom,0) = 1
			ORDER BY
				tintOrder ASC
		END

		-- Removes last , from string
		IF DATALENGTH(@CustomerSearchCustomFields) > 0
			SET @CustomerSearchCustomFields = LEFT(@CustomerSearchCustomFields, LEN(@CustomerSearchCustomFields) - 1)
	END

	IF  LEN(@CustomerSearchFields) = 0 AND LEN(@CustomerSearchCustomFields) = 0
	BEGIN
		SET @CustomerSearchFields = 'vcCompanyName AS Search_vcCompanyName'
	END

	----------------------------   Display Fields ---------------------------
	DECLARE @TEMPDisplayFields TABLE
	(
		numFieldID NUMERIC(18,0)
		,vcDbColumnName VARCHAR(200)
		,tintOrder TINYINT
		,bitCustom BIT
	)


	INSERT INTO @TEMPDisplayFields
	(
		numFieldID
		,vcDbColumnName
		,tintOrder
		,bitCustom
	)
	SELECT 
		numFieldId,
		vcDbColumnName,
		tintRow AS tintOrder,
		0 as Custom
    FROM 
		View_DynamicColumns
	WHERE 
		numFormId=96 AND numDomainID=@numDomainID AND 
		tintPageType=1 AND bitCustom=0  AND ISNULL(bitSettingField,0)=1 AND numRelCntType=0
	UNION   
	SELECT 
		numFieldId,
		vcFieldName,
		tintRow AS tintOrder,
		1 as Custom
    FROM 
		View_DynamicCustomColumns   
	WHERE 
		Grp_id=1 AND numFormId=96 AND numDomainID=@numDomainID AND 
		tintPageType=1 AND bitCustom=1 AND numRelCntType=0


	IF EXISTS (SELECT * FROM @TEMPDisplayFields)
	BEGIN
		SELECT 
			@CustomerDisplayFields = COALESCE (@CustomerDisplayFields,'') + CONCAT('(CASE WHEN LEN(',vcDbColumnName,') > 0 THEN CONCAT(',(CASE WHEN LEN(@CustomerDisplayFields) > 0 THEN ''', '',' ELSE ''''',' END),vcDbColumnName, ') ELSE '''' END)') + ', '	
		FROM 
			@TEMPDisplayFields 
		WHERE
			ISNULL(bitCustom,0) = 0
		ORDER BY
			tintOrder ASC
		
		IF EXISTS(SELECT * FROM @TEMPDisplayFields WHERE ISNULL(bitCustom,0) = 1)
		BEGIN
			SELECT 
				@CustomerDisplayFields = COALESCE (@CustomerDisplayFields,'') + CONCAT('(CASE WHEN LEN(',CONCAT('CFW',numFieldId),') > 0 THEN CONCAT(',(CASE WHEN LEN(@CustomerDisplayFields) > 0 THEN ''', '',' ELSE ''''',' END),CONCAT('CFW',numFieldId), ') ELSE '''' END)') + ', '	
			FROM 
				@TEMPDisplayFields 
			WHERE
				ISNULL(bitCustom,0) = 1
			ORDER BY
				tintOrder ASC
		END

		-- Removes last , from string
		IF DATALENGTH(@CustomerDisplayFields) > 0
			SET @CustomerDisplayFields = LEFT(@CustomerDisplayFields, LEN(@CustomerDisplayFields) - 1)
	END

	IF  LEN(@CustomerDisplayFields) = 0
	BEGIN
		SET @CustomerDisplayFields = 'vcCompanyName'
	END

	--IF LEN(@CustomerDisplayFields) > 0 AND CHARINDEX(',',@CustomerDisplayFields) > 0
	--BEGIN
	--	SET @CustomerDisplayFields = REPLACE(@CustomerDisplayFields,',',','', '',')
	--END

	--IF LEN(@CustomerDisplayCustomFields) > 0 AND CHARINDEX(',',@CustomerDisplayCustomFields) > 0
	--BEGIN
	--	SET @CustomerDisplayCustomFields = REPLACE(@CustomerDisplayCustomFields,',',','', '',')
	--END

	DECLARE @CustomFields AS VARCHAR(1000)

	SELECT @CustomFields = COALESCE (@CustomFields,'') + 'CFW' + CAST(FLD_ID AS VARCHAR) + ', ' FROM CFW_Fld_Master WHERE numDomainID=@numDomainID AND Grp_id=1
	-- Removes last , from string
	IF DATALENGTH(@CustomFields) > 0
		SET @CustomFields = LEFT(@CustomFields, LEN(@CustomFields) - 1)

	SELECT @query = CONCAT('SELECT numDivisionID as id,''organization'' As module, ISNULL(tintCRMType,0) AS tintOrgCRMType, numOrgAssignedTo, numOrgRecOwner, numOrgTerID, numOrgCompanyType, (CASE tintCRMType WHEN 1 THEN CONCAT(''/prospects/frmProspects.aspx?DivID='',numDivisionID) WHEN 2 THEN CONCAT(''/Account/frmAccounts.aspx?DivID='',numDivisionID) ELSE CONCAT(''/Leads/frmLeads.aspx?DivID='',numDivisionID) END) url,CONCAT(''<b style="color:#0070C0">'',numCompanyType,(CASE WHEN numOrgCompanyType=46 THEN CONCAT('' ('',(CASE tintCRMType WHEN 1 THEN ''Prospect'' WHEN 2 THEN ''Account'' ELSE ''Lead'' END),'')'') ELSE '''' END),'':</b> '',', @CustomerDisplayFields,') AS displaytext, CONCAT(numCompanyType,(CASE WHEN numOrgCompanyType=46 THEN CONCAT('' ('',(CASE tintCRMType WHEN 1 THEN ''Prospect'' WHEN 2 THEN ''Account'' ELSE ''Lead'' END),'')'') ELSE '''' END),'': '',', @CustomerDisplayFields,') AS text',
	CASE @CustomerSearchFields
	WHEN '' THEN ''
	ELSE ',' + @CustomerSearchFields
	END
	,
	CASE @CustomerSearchCustomFields
	WHEN '' THEN ''
	ELSE ',' + @CustomerSearchCustomFields
	END
	 ,' FROM (SELECT
		DivisionMaster.numDivisionID
		,ISNULL(DivisionMaster.numAssignedTo,0) numOrgAssignedTo
		,ISNULL(DivisionMaster.numRecOwner,0) numOrgRecOwner
		,ISNULL(DivisionMaster.numTerID,0) numOrgTerID
		,CompanyInfo.vcCompanyName
		,CompanyInfo.numCompanyType AS numOrgCompanyType
		,DivisionMaster.tintCRMType
		,ISNULL(LDComDiff.vcData,'''') numCompanyDiff
		,ISNULL(DivisionMaster.vcCompanyDiff,'''') vcCompanyDiff
		,ISNULL(LDCMPProfile.vcData,'''') vcProfile
		,ISNULL(LDCMPType.vcData,'''') numCompanyType
		,ISNULL(vcComPhone,'''') vcComPhone
		,ISNULL(LDCMPNoOfEmployees.vcData,'''') numNoOfEmployeesId
		,ISNULL(LDCMPAnnualRevenue.vcData,'''') numAnnualRevID
		,ISNULL(vcWebSite,'''') vcWebSite 
		,ISNULL(LDTerritory.vcData,'''') numTerID
		,ISNULL(LDLeadSource.vcData,'''') vcHow
		,ISNULL(LDCMPIndustry.vcData,'''') numCompanyIndustry
		,ISNULL(LDCMPRating.vcData,'''') numCompanyRating
		,ISNULL(LDDMStatus.vcData,'''') numStatusID
		,ISNULL(LDCMPCreditLimit.vcData,'''') numCompanyCredit
		,ISNULL(vcComFax,'''') vcComFax
		,ISNULL(txtComments,'''') txtComments
		,ISNULL(ADC.vcFirstName,'''') vcFirstName
		,ISNULL(ADC.vcLastName,'''') vcLastName
		,ISNULL(ShippingAddress.vcStreet,'''') vcShipStreet
		,ISNULL(ShippingAddress.vcCity,'''') vcShipCity
		,ISNULL(ShippingAddress.vcPostalCode,'''') vcShipPostCode
		,ISNULL(LDShipCountry.vcData,'''') numShipCountry 
		,ISNULL(ShipState.vcState,'''') numShipState
		,ISNULL(BillingAddress.vcStreet,'''') vcBillStreet
		,ISNULL(BillingAddress.vcCity,'''') vcBillCity
		,ISNULL(BillingAddress.vcPostalCode,'''') vcBillPostCode
		,ISNULL(LDBillCountry.vcData,'''') numBillCountry 
		,ISNULL(BillState.vcState,'''') numBillState 
		,ISNULL(LDDMFollowup.vcData,'''') numFollowUpStatus
		,ISNULL(BillingTerms.vcTerms,'''') numBillingDays
		,DivisionMaster.vcPartnerCode
		' + (CASE WHEN DATALENGTH(@CustomFields) > 0 THEN ',CustomFields.*' ELSE '' END) + '
	FROM 
		CompanyInfo
	INNER JOIN
		DivisionMaster
	ON
		CompanyInfo.numCompanyId = DivisionMaster.numCompanyID
	LEFT JOIN
		ListDetails LDCMPRating
	ON
		CompanyInfo.numCompanyCredit = LDCMPRating.numListItemID
		AND LDCMPRating.numListID = 2
	LEFT JOIN
		ListDetails LDCMPCreditLimit
	ON
		CompanyInfo.numCompanyRating = LDCMPCreditLimit.numListItemID
		AND LDCMPCreditLimit.numListID = 3
	LEFT JOIN
		ListDetails LDCMPIndustry
	ON
		CompanyInfo.numCompanyIndustry = LDCMPIndustry.numListItemID
		AND LDCMPIndustry.numListID = 4
	LEFT JOIN
		ListDetails LDCMPType
	ON
		CompanyInfo.numCompanyType = LDCMPType.numListItemID
		AND LDCMPType.numListID = 5
	LEFT JOIN
		ListDetails LDCMPAnnualRevenue
	ON
		CompanyInfo.numAnnualRevID = LDCMPAnnualRevenue.numListItemID
		AND LDCMPAnnualRevenue.numListID = 6
	LEFT JOIN
		ListDetails LDCMPNoOfEmployees
	ON
		CompanyInfo.numNoOfEmployeesId = LDCMPNoOfEmployees.numListItemID
		AND LDCMPNoOfEmployees.numListID = 7
	LEFT JOIN
		ListDetails LDCMPProfile
	ON
		CompanyInfo.vcProfile = LDCMPProfile.numListItemID
		AND LDCMPProfile.numListID = 21
	LEFT JOIN
		ListDetails LDDMFollowup
	ON
		DivisionMaster.numFollowUpStatus = LDDMFollowup.numListItemID
		AND LDDMFollowup.numListID = 30
	LEFT JOIN
		ListDetails LDDMStatus
	ON
		DivisionMaster.numStatusID = LDDMStatus.numListItemID
		AND LDDMStatus.numListID = 1
	LEFT JOIN
		BillingTerms
	ON
		DivisionMaster.numBillingDays = BillingTerms.numTermsID
	LEFT JOIN
		ListDetails LDComDiff
	ON 
		DivisionMaster.numCompanyDiff = LDComDiff.numListItemID
		AND LDComDiff.numListID = 438
	LEFT JOIN
		ListDetails LDTerritory
	ON 
		DivisionMaster.numTerID = LDTerritory.numListItemID
		AND LDTerritory.numListID = 78
	LEFT JOIN
		ListDetails LDLeadSource
	ON 
		CompanyInfo.vcHow = LDLeadSource.numListItemID
		AND LDLeadSource.numListID = 18
	OUTER APPLY
	(
		SELECT TOP 1 
			vcFirstName
			,vcLastName
		FROM
			AdditionalContactsInformation
		WHERE
			AdditionalContactsInformation.numDomainID = DivisionMaster.numDomainID
			AND AdditionalContactsInformation.numDivisionID=DivisionMaster.numDivisionID
			AND ISNULL(bitPrimaryContact,0) = 1 
	) ADC
	LEFT JOIN
		AddressDetails BillingAddress
	ON
		BillingAddress.numRecordID = DivisionMaster.numDivisionID 
		AND BillingAddress.tintAddressOf = 2 
		AND BillingAddress.tintAddressType = 1 
		AND BillingAddress.bitIsPrimary = 1
		AND BillingAddress.numDomainID = DivisionMaster.numDomainID
	LEFT JOIN
		ListDetails LDBillCountry
	ON 
		BillingAddress.numCountry = LDBillCountry.numListItemID
		AND LDBillCountry.numListID = 40
	LEFT JOIN
		[State] BillState
	ON 
		BillingAddress.numState = BillState.numStateID
		AND BillState.numDomainID = DivisionMaster.numDomainID
	LEFT JOIN
		AddressDetails ShippingAddress
	ON
		ShippingAddress.numRecordID = DivisionMaster.numDivisionID 
		AND ShippingAddress.tintAddressOf = 2 
		AND ShippingAddress.tintAddressType = 2 
		AND ShippingAddress.bitIsPrimary = 1
		AND ShippingAddress.numDomainID = DivisionMaster.numDomainID
	LEFT JOIN
		ListDetails LDShipCountry
	ON 
		ShippingAddress.numCountry = LDShipCountry.numListItemID
		AND LDShipCountry.numListID = 40
	LEFT JOIN
		[State] ShipState
	ON 
		ShippingAddress.numState = ShipState.numStateID
		AND ShipState.numDomainID = DivisionMaster.numDomainID
	' + (CASE WHEN DATALENGTH(@CustomFields) > 0 THEN CONCAT('OUTER APPLY
				(
					SELECT 
					* 
					FROM 
					(
						SELECT  
							CONCAT(''CFW'',CFW_Fld_Values.Fld_ID) Fld_ID,
							dbo.fn_GetCustFldStringValue(CFW_FLD_Values.Fld_ID, CFW_FLD_Values.RecId, CFW_FLD_Values.Fld_Value) Fld_Value 
						FROM 
							CFW_Fld_Master
						LEFT JOIN 
							CFW_Fld_Values
						ON
							CFW_Fld_Values.Fld_ID = CFW_Fld_Master.Fld_id
						WHERE 
							RecId = DivisionMaster.numDivisionID
					) p PIVOT (MAX([Fld_Value]) FOR Fld_ID  IN (',@CustomFields,') ) AS pvt
				 ) CustomFields') ELSE '' END) + '
	WHERE
		CompanyInfo.numDomainID =',@numDomainID,' AND (CompanyInfo.numCompanyId IN (',ISNULL(@vcCompanyIds,'0'),') OR LEN(''',@vcCompanyIds,''') = 0)) TEMP1')

	PRINT @query

	EXEC SP_EXECUTESQL @query
END