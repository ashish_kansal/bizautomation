SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_ValidateeWorkOrderInventory')
DROP PROCEDURE usp_ValidateeWorkOrderInventory
GO
CREATE PROCEDURE [dbo].[usp_ValidateeWorkOrderInventory]
    (
      @numWOId as numeric(9)=0,
	  @numWOStatus as numeric(9)
    )
AS 
BEGIN
   
	IF @numWOStatus=23184 
	BEGIN
		DECLARE @numOppID NUMERIC(18,0)
		DECLARE @tintCommitAllocation TINYINT
		SELECT @tintCommitAllocation=ISNULL(tintCommitAllocation,1),@numOppID=ISNULL(numOppId,0) FROM WorkOrder INNER JOIN Domain ON WorkOrder.numDomainID=Domain.numDomainID WHERE numWOId=@numWOId

		SELECT 
			WD.numQtyItemsReq
			,i.vcItemName
			,isnull(WHI.numOnHand,0) numOnHand
			,isnull(WHI.numAllocation,0) numAllocation 
		FROM 
			WorkOrderDetails WD 
		JOIN 
			Item I 
		ON
			WD.numChildItemID=i.numItemCode
		JOIN 
			WareHouseItems WHI 
		ON 
			WHI.numWareHouseItemID=WD.numWareHouseItemID  
		WHERE 
			numWOId=@numWOId 
			AND i.charItemType IN ('P') 
			AND 1 = (CASE 
						WHEN @numOppID > 0 AND @tintCommitAllocation = 2 
						THEN (CASE WHEN WD.numQtyItemsReq > ISNULL(WHI.numOnHand,0) THEN 1 ELSE 0 END) 
						ELSE (CASE WHEN WD.numQtyItemsReq > ISNULL(WHI.numAllocation,0) THEN 1 ELSE 0 END) 
					END)
	END

END
GO
