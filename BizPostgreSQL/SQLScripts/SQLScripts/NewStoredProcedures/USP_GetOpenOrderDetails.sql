/****** Object:  StoredProcedure [dbo].[usp_GetDuplicateCompany]    Script Date: 07/26/2008 16:17:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetOpenOrderDetails')
DROP PROCEDURE USP_GetOpenOrderDetails
GO
CREATE PROCEDURE [dbo].[USP_GetOpenOrderDetails]                      
@numDivisionID numeric(9)=0                      
as                    
BEGIN
SELECT SUM(monTotAmount) AS TotalPOAmount,SUM(numUnitHOur) AS TotalQty,SUM(ISNULL(I.fltWeight,0) * ISNULL(numUnitHOur,0)) AS TotalWeight,
 stuff((
        select '<br/>' +' <b>Buy</b> '+CASE WHEN u.intType=1 THEN '$'+CAST(u.vcBuyingQty AS VARCHAR(MAX)) ELSE CAST(u.vcBuyingQty AS VARCHAR(MAX)) END+' ' + CASE WHEN u.intType=1 THEN 'Amount' WHEN u.intType=2 THEN 'Quantity' WHEN u.intType=3 THEN 'Lbs' ELSE '' END+' <b>Get</b> ' + (CASE WHEN u.tintDiscountType = 1 THEN CONCAT('$',u.vcIncentives,' off') ELSE CONCAT(u.vcIncentives,'% off') END)
        from PurchaseIncentives u
        where u.numPurchaseIncentiveId = numPurchaseIncentiveId  AND numDivisionId=@numDivisionID
        order by u.numPurchaseIncentiveId
        for xml path('')
    ),6,6,'') AS Purchaseincentives
FROM OpportunityItems as OPI left join Item AS I ON OPI.numItemCode=I.numItemCode WHERE 
numOppId IN (SELECT numOppId FROM OpportunityMaster WHERE numDivisionId=@numDivisionID AND bintClosedDate IS NULL)
END