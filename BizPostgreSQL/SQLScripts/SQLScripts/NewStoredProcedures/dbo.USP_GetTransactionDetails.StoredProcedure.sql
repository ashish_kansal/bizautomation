
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetTransactionDetails')
	DROP PROCEDURE USP_GetTransactionDetails
GO
/****** Added By : Joseph ******/
/****** Gets Transactions Details for the the Given TransactionID******/


CREATE PROCEDURE [dbo].[USP_GetTransactionDetails]
@numTransactionID numeric(18, 0)
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

BEGIN  

SELECT BST.numTransactionId,BST.monAmount,BST.vcCheckNumber,BST.dtDatePosted,BST.vcFITransactionID, BST.vcPayeeName, BST.vcMemo,
BST.vcTxType,BST.numAccountID,BTM.tintReferenceType,BTM.numReferenceID, BD.vcAccountNumber,BD.vcAccountType,BM.vcFIName,
BST.vcPayeeName + BST.vcMemo AS vcDescription,
C.vcAccountName, 
LEFT(CONVERT(VARCHAR, BST.dtDatePosted, 120), 10) + ' CREDIT to (' +ISNULL(C.[vcAccountName],'') +  ')' + CI.vcCompanyname + ' for $' + CONVERT(VARCHAR, BST.monAmount) AS TransMatchDetails

FROM bankStatementTransactions BST 
LEFT JOIN BankTransactionMapping BTM ON BST.numTransactionID = BTM.numTransactionID

INNER JOIN dbo.BankStatementHeader BSH ON BST.numStatementId = BSH.numStatementID 
INNER JOIN BankDetails BD ON BSH.numBankDetailId = BD.numBankDetailId 
INNER JOIN BankMaster BM ON BD.numbankMasterId = BM.numBankMasterID

LEFT JOIN dbo.DepositMaster DM  ON BTM.numReferenceID = DM.numDepositId
LEFT JOIN dbo.DepositeDetails DD ON DD.numDepositId = DM.numDepositId
LEFT JOIN dbo.Chart_Of_Accounts C ON DD.numAccountID = C.numAccountId
LEFT JOIN dbo.DivisionMaster D ON DD.numReceivedFrom	= D.numDivisionID
LEFT JOIN dbo.companyinfo CI ON D.numCompanyID= CI.numCompanyID 

WHERE BST.numTransactionId = @numTransactionID AND
EXISTS (SELECT * FROM BankTransactionMapping BTM WHERE BTM.numTransactionID = BST.numTransactionID AND BTM.numreferenceId > 0 AND  BTM.tintReferenceType = 6 AND BTM.bitIsActive = 1)

UNION 


 
SELECT BST.numTransactionId,BST.monAmount,BST.vcCheckNumber,BST.dtDatePosted,BST.vcFITransactionID, BST.vcPayeeName, BST.vcMemo,
BST.vcTxType,BST.numAccountID,BTM.tintReferenceType,BTM.numReferenceID, BD.vcAccountNumber,BD.vcAccountType,BM.vcFIName,
BST.vcPayeeName + BST.vcMemo AS vcDescription,
C.vcAccountName, 
LEFT(CONVERT(VARCHAR, BST.dtDatePosted, 120), 10) + ' DEBIT to (' +ISNULL(C.[vcAccountName],'') +  ')' + CI.vcCompanyname + ' for $' + CONVERT(VARCHAR, ABS(BST.monAmount)) AS TransMatchDetails

FROM bankStatementTransactions BST 
LEFT JOIN BankTransactionMapping BTM ON BST.numTransactionID = BTM.numTransactionID

INNER JOIN dbo.BankStatementHeader BSH ON BST.numStatementId = BSH.numStatementID 
INNER JOIN BankDetails BD ON BSH.numBankDetailId = BD.numBankDetailId 
INNER JOIN BankMaster BM ON BD.numbankMasterId = BM.numBankMasterID

LEFT JOIN dbo.CheckHeader CH ON BTM.numReferenceID = CH.numCheckHeaderID
LEFT JOIN dbo.CheckDetails CD ON CH.numCheckHeaderID = CD.numCheckHeaderID
LEFT JOIN dbo.Chart_Of_Accounts C ON CD.numChartAcntId = C.numAccountId
LEFT JOIN dbo.DivisionMaster D ON CH.numDivisionID	= D.numDivisionID
LEFT JOIN dbo.companyinfo CI ON D.numCompanyID= CI.numCompanyID 

WHERE BST.numTransactionId = @numTransactionID AND  
EXISTS (SELECT * FROM BankTransactionMapping BTM WHERE BTM.numTransactionID = BST.numTransactionID AND BTM.numreferenceId > 0 AND  BTM.tintReferenceType = 1 AND BTM.bitIsActive = 1)

UNION 
 
SELECT BST.numTransactionId,BST.monAmount,BST.vcCheckNumber,BST.dtDatePosted,BST.vcFITransactionID, BST.vcPayeeName, BST.vcMemo,
BST.vcTxType,BST.numAccountID,BTM.tintReferenceType,BTM.numReferenceID, BD.vcAccountNumber,BD.vcAccountType,BM.vcFIName,
BST.vcPayeeName + BST.vcMemo AS vcDescription,
'' AS vcAccountName, 
'UnMatched' AS TransMatchDetails

FROM bankStatementTransactions BST 
LEFT JOIN BankTransactionMapping BTM ON BST.numTransactionID = BTM.numTransactionID

INNER JOIN dbo.BankStatementHeader BSH ON BST.numStatementId = BSH.numStatementID 
INNER JOIN BankDetails BD ON BSH.numBankDetailId = BD.numBankDetailId 
INNER JOIN BankMaster BM ON BD.numbankMasterId = BM.numBankMasterID

WHERE BST.numTransactionId = @numTransactionID AND
 NOT EXISTS (SELECT * FROM BankTransactionMapping BTM WHERE BTM.numTransactionID = BST.numTransactionID AND BTM.bitIsActive = 1)

--SELECT BST.numTransactionId,BST.monAmount,BST.vcCheckNumber,BST.dtDatePosted,BST.vcFITransactionID, BST.vcPayeeName, BST.vcMemo,
--BST.vcTxType,BST.numAccountID,BST.tintReferenceType,BST.numReferenceID, BD.vcAccountNumber,BD.vcAccountType,BM.vcFIName
--FROM bankStatementTransactions BST 
--INNER JOIN dbo.BankStatementHeader BSH ON BST.numStatementId = BSH.numStatementID 
--INNER JOIN BankDetails BD ON BSH.numBankDetailId = BD.numBankDetailId 
--INNER JOIN BankMaster BM ON BD.numbankMasterId = BM.numBankMasterID
--WHERE BST.numTransactionId = @numTransactionID
 
END
	
