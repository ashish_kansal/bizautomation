GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GeteCommercePaymentConfig')
DROP PROCEDURE USP_GeteCommercePaymentConfig
GO
CREATE PROCEDURE [dbo].[USP_GeteCommercePaymentConfig]
@numDomainID as NUMERIC(9),
@numSiteId as NUMERIC(9),
@numPaymentMethodId AS NUMERIC(9)
as

SELECT [numId], [numSiteId], [numPaymentMethodId], [bitEnable], [numBizDocId], [numOrderStatus], [numRecordOwner], [numAssignTo], [numDomainID], [numMirrorBizDocTemplateId], [numFailedOrderStatus], [numBizDocStatus] 
	FROM   [dbo].[eCommercePaymentConfig] 
	WHERE  numDomainID=@numDomainID AND numSiteId=@numSiteId AND numPaymentMethodId=@numPaymentMethodId

GO
