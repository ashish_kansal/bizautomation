
GO
/****** Object:  StoredProcedure [dbo].[USP_GetSiteBreadCrumb]    Script Date: 08/08/2009 16:29:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Anoop G Krishnan
-- Create date: 24-Jul-2009
-- Description:	To fetch the BreadCrumb details
-- =============================================
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetSiteBreadCrumb')
DROP PROCEDURE USP_GetSiteBreadCrumb
GO
CREATE PROCEDURE [dbo].[USP_GetSiteBreadCrumb] 
	@numSiteID numeric(18), 
	@numDomainID numeric(18),
	@numBreadCrumbID numeric(18)
AS
BEGIN
	SET NOCOUNT ON;
	
	SELECT SBC.[numBreadCrumbID]
		  ,SBC.[numPageID]
		  ,SP.[vcPageName]
		  ,SP.[vcPageURL]
		  ,SBC.[numSiteID]
		  ,SBC.[tintLevel]
		  ,SBC.[vcDisplayName]
	FROM [dbo].[SiteBreadCrumb] SBC
	INNER JOIN  [dbo].[SitePages] SP
	ON SBC.[numPageID] = SP.[numPageID]
	WHERE SBC.[numSiteID] = @numSiteID
		AND SBC.[numDomainID] = @numDomainID
		AND([numBreadCrumbID] = @numBreadCrumbID 
			OR @numBreadCrumbID = 0)
	ORDER BY [tintLevel]
END
