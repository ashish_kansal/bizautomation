GO
SET ANSI_NULLS ON

GO
SET QUOTED_IDENTIFIER ON
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetCampaignReport')
DROP PROCEDURE USP_GetCampaignReport
GO
CREATE PROCEDURE [dbo].[USP_GetCampaignReport]                                 
	@numDomainID NUMERIC(18,0)
	,@ClientTimezoneOffset INT
	,@numLeadSource NUMERIC(18,0)
	,@tintReportView TINYINT
	,@dtFromDate DATETIME
	,@dtToDate DATETIME                                            
AS                                                                
BEGIN  
	DECLARE @columns VARCHAR(8000) = ''
	DECLARE @PivotColumns VARCHAR(8000) = ''
	DECLARE @sql VARCHAR(MAX) = ''

	IF @tintReportView IN (1,2)
	BEGIN
		CREATE TABLE #TempYearMonth
		(
			intYear INT,
			intMonth INT,
			MonthYear VARCHAR(50),
			StartDate DATETIME,
			EndDate DATETIME,
			numConversionPercent NUMERIC(18,2),
			monGrossProfit DECIMAL(20,5)
		)

		IF @tintReportView = 2
		BEGIN
			;WITH CTE AS 
			(
				SELECT
					YEAR(@dtFromDate) AS 'yr',
					MONTH(@dtFromDate) AS 'mm',
					DATENAME(mm, @dtFromDate) AS 'mon',
					(FORMAT(@dtFromDate,'MMM') + '_' + CAST(YEAR(@dtFromDate) AS VARCHAR)) AS MonthYear,
					CAST(DATEFROMPARTS(YEAR(@dtFromDate),MONTH(@dtFromDate),1) AS DATETIME) dtStartDate,
					DATEADD(MS,-2,CAST(DATEADD(MONTH,1,DATEFROMPARTS(YEAR(@dtFromDate),MONTH(@dtFromDate),1)) AS DATETIME)) dtEndDate,
					@dtFromDate 'new_date'
				UNION ALL
				SELECT
					YEAR(DATEADD(d,1,new_date)) AS 'yr',
					MONTH(DATEADD(d,1,new_date)) AS 'mm',
					DATENAME(mm, DATEADD(d,1,new_date)) AS 'mon',
					(FORMAT(DATEADD(d,1,new_date),'MMM') + '_' + CAST(YEAR(DATEADD(d,1,new_date)) AS VARCHAR)) AS MonthYear,
					CAST(DATEFROMPARTS(YEAR(DATEADD(d,1,new_date)),MONTH(DATEADD(d,1,new_date)),1) AS DATETIME) dtStartDate,
					DATEADD(MS,-2,CAST(DATEADD(MONTH,1,DATEFROMPARTS(YEAR(DATEADD(d,1,new_date)),MONTH(DATEADD(d,1,new_date)),1)) AS DATETIME)) dtEndDate,
					DATEADD(d,1,new_date) 'new_date'
				FROM CTE
				WHERE DATEADD(d,1,new_date) < @dtToDate
			)
	
			INSERT INTO #tempYearMonth
			(
				intYear,
				intMonth,
				MonthYear,
				StartDate,
				EndDate
			)
			SELECT 
				yr,mm,MonthYear,dtStartDate,dtEndDate
			FROM 
				CTE
			GROUP BY 
				yr, mm, MonthYear, dtStartDate, dtEndDate
			ORDER BY 
				yr, mm
			OPTION (MAXRECURSION 5000)

			UPDATE 
				#tempYearMonth
			SET
				EndDate = (CASE WHEN EndDate < @dtToDate THEN EndDate ELSE @dtToDate END)
			END
		ELSE
		BEGIN
			;WITH CTE AS 
			(
				SELECT
					dbo.GetFiscalyear(@dtFromDate,@numDomainId) AS 'yr',
					dbo.GetFiscalQuarter(@dtFromDate,@numDomainId) AS 'qq',
					('Q' + cast(dbo.GetFiscalQuarter(@dtFromDate,@numDomainId) AS VARCHAR) + '_' + cast(dbo.GetFiscalyear(@dtFromDate,@numDomainId) AS VARCHAR)) AS MonthYear,
					CASE dbo.GetFiscalQuarter(@dtFromDate,@numDomainId) 
					WHEN 1 THEN dateadd(month,0,dbo.GetFiscalStartDate(dbo.GetFiscalyear(@dtFromDate,@numDomainId),@numDomainId))
					WHEN 2 THEN dateadd(month,3,dbo.GetFiscalStartDate(dbo.GetFiscalyear(@dtFromDate,@numDomainId),@numDomainId)) 
					WHEN 3 THEN dateadd(month,6,dbo.GetFiscalStartDate(dbo.GetFiscalyear(@dtFromDate,@numDomainId),@numDomainId))
					WHEN 4 THEN dateadd(month,9,dbo.GetFiscalStartDate(dbo.GetFiscalyear(@dtFromDate,@numDomainId),@numDomainId)) 
					END AS dtStartDate,
					CASE dbo.GetFiscalQuarter(@dtFromDate,@numDomainId) 
					WHEN 1 THEN dateadd(MS,-3,dateadd(month,3,dbo.GetFiscalStartDate(dbo.GetFiscalyear(@dtFromDate,@numDomainId),@numDomainId)))
					WHEN 2 THEN dateadd(MS,-3,dateadd(month,6,dbo.GetFiscalStartDate(dbo.GetFiscalyear(@dtFromDate,@numDomainId),@numDomainId)))
					WHEN 3 THEN dateadd(MS,-3,dateadd(month,9,dbo.GetFiscalStartDate(dbo.GetFiscalyear(@dtFromDate,@numDomainId),@numDomainId)))
					WHEN 4 THEN dateadd(MS,-3,dateadd(month,12,dbo.GetFiscalStartDate(dbo.GetFiscalyear(@dtFromDate,@numDomainId),@numDomainId)))
					END AS dtEndDate,
					@dtFromDate 'new_date'
				UNION ALL
				SELECT
					dbo.GetFiscalyear(DATEADD(d,1,new_date),@numDomainId) AS 'yr',
					dbo.GetFiscalQuarter(DATEADD(d,1,new_date),@numDomainId) AS 'qq',
					('Q' + cast(dbo.GetFiscalQuarter(DATEADD(d,1,new_date),@numDomainId) AS VARCHAR) + '_' + cast(dbo.GetFiscalyear(DATEADD(d,1,new_date),@numDomainId) AS VARCHAR)) AS MonthYear,
					CASE dbo.GetFiscalQuarter(DATEADD(d,1,new_date),@numDomainId) 
					WHEN 1 THEN dateadd(month,0,dbo.GetFiscalStartDate(dbo.GetFiscalyear(DATEADD(d,1,new_date),@numDomainId),@numDomainId))
					WHEN 2 THEN dateadd(month,3,dbo.GetFiscalStartDate(dbo.GetFiscalyear(DATEADD(d,1,new_date),@numDomainId),@numDomainId)) 
					WHEN 3 THEN dateadd(month,6,dbo.GetFiscalStartDate(dbo.GetFiscalyear(DATEADD(d,1,new_date),@numDomainId),@numDomainId))
					WHEN 4 THEN dateadd(month,9,dbo.GetFiscalStartDate(dbo.GetFiscalyear(DATEADD(d,1,new_date),@numDomainId),@numDomainId)) 
					END AS dtStartDate,
					CASE dbo.GetFiscalQuarter(DATEADD(d,1,new_date),@numDomainId) 
					WHEN 1 THEN dateadd(MS,-3,dateadd(month,3,dbo.GetFiscalStartDate(dbo.GetFiscalyear(DATEADD(d,1,new_date),@numDomainId),@numDomainId)))
					WHEN 2 THEN dateadd(MS,-3,dateadd(month,6,dbo.GetFiscalStartDate(dbo.GetFiscalyear(DATEADD(d,1,new_date),@numDomainId),@numDomainId)))
					WHEN 3 THEN dateadd(MS,-3,dateadd(month,9,dbo.GetFiscalStartDate(dbo.GetFiscalyear(DATEADD(d,1,new_date),@numDomainId),@numDomainId)))
					WHEN 4 THEN dateadd(MS,-3,dateadd(month,12,dbo.GetFiscalStartDate(dbo.GetFiscalyear(DATEADD(d,1,new_date),@numDomainId),@numDomainId)))
					END AS dtEndDate,
					DATEADD(d,1,new_date) 'new_date'
				FROM CTE
				WHERE DATEADD(d,1,new_date) < @dtToDate
			)
	
			INSERT INTO #tempYearMonth
			(
				intYear,
				intMonth,
				MonthYear,
				StartDate,
				EndDate
			)
			SELECT 
				yr,qq,MonthYear,dtStartDate,dtEndDate
			FROM 
				CTE
			GROUP BY 
				yr, qq, MonthYear, dtStartDate, dtEndDate
			ORDER BY 
				yr, qq
			OPTION
				(MAXRECURSION 5000)

			UPDATE 
				#tempYearMonth
			SET
				EndDate = (CASE WHEN EndDate < @dtToDate THEN EndDate ELSE @dtToDate END)
		END
		
		SELECT @columns = STUFF((SELECT ', ISNULL(' + MonthYear  + ',0) AS [' + REPLACE(MonthYear,'_',' ') + ']' FROM #tempYearMonth ORDER BY intYear,intMonth FOR XML PATH('')), 1, 2, ''); 
		SELECT @PivotColumns = STUFF((SELECT ', ' + MonthYear FROM #tempYearMonth ORDER BY intYear,intMonth FOR XML PATH('')), 1, 2, ''); 

		SELECT 
			ISNULL(vcData,'') AS Campaign
			,ISNULL((SELECT
						SUM(monDealAmount)
					FROM
						CompanyInfo CI 
					INNER JOIN 
						DivisionMaster DM 
					ON 
						CI.numCompanyId = DM.numCompanyID
					INNER JOIN
						OpportunityMaster OM
					ON
						DM.numDivisionID = OM.numDivisionId
					WHERE 
						CI.numDomainID=@numDomainID 
						AND DM.numDomainID = @numDomainID
						AND OM.numDomainId=@numDomainID
						AND CI.vcHow=LD.numListItemID
						AND OM.tintOppType=1
						AND OM.tintOppStatus = 1
						AND DATEADD(MINUTE,-1 * @ClientTimezoneOffset,OM.bintCreatedDate) BETWEEN TYM.StartDate AND TYM.EndDate),0) monTotalIncome
			,ISNULL((SELECT 
						COUNT(DM.numDivisionID) 
					FROM 
						CompanyInfo CI 
					INNER JOIN 
						DivisionMaster DM 
					ON 
						CI.numCompanyId = DM.numCompanyID 
					WHERE 
						CI.numDomainID=@numDomainID 
						AND CI.vcHow=LD.numListItemID 
						AND DATEADD(MINUTE,-1 * @ClientTimezoneOffset,DM.bintCreatedDate) BETWEEN TYM.StartDate AND TYM.EndDate),0) numLeadCount
			,ISNULL((SELECT 
						COUNT(numDivisionID)
					FROM
					(
						SELECT
							DM.numDivisionID
							,MIN(OM.bintCreatedDate) dtFirstSalesOrderDate
						FROM
							CompanyInfo CI 
						INNER JOIN 
							DivisionMaster DM 
						ON 
							CI.numCompanyId = DM.numCompanyID
						INNER JOIN
							OpportunityMaster OM
						ON
							DM.numDivisionID = OM.numDivisionId
						WHERE 
							CI.numDomainID=@numDomainID 
							AND DM.numDomainID = @numDomainID
							AND OM.numDomainId=@numDomainID
							AND CI.vcHow=LD.numListItemID
							AND OM.tintOppType=1
							AND OM.tintOppStatus = 1
						GROUP BY
							DM.numDivisionID
					) TEMP
					WHERE
						DATEADD(MINUTE,-1 * @ClientTimezoneOffset,dtFirstSalesOrderDate) BETWEEN TYM.StartDate AND TYM.EndDate),0) numCustomerCount
			,Stuff((SELECT 
						CONCAT(N', ',numDivisionID)
					FROM
					(
						SELECT
							DM.numDivisionID
							,MIN(OM.bintCreatedDate) dtFirstSalesOrderDate
						FROM
							CompanyInfo CI 
						INNER JOIN 
							DivisionMaster DM 
						ON 
							CI.numCompanyId = DM.numCompanyID
						INNER JOIN
							OpportunityMaster OM
						ON
							DM.numDivisionID = OM.numDivisionId
						WHERE 
							CI.numDomainID=@numDomainID 
							AND DM.numDomainID = @numDomainID
							AND OM.numDomainId=@numDomainID
							AND CI.vcHow=LD.numListItemID
							AND OM.tintOppType=1
							AND OM.tintOppStatus = 1
						GROUP BY
							DM.numDivisionID
					) TEMP
					WHERE
						DATEADD(MINUTE,-1 * @ClientTimezoneOffset,dtFirstSalesOrderDate) BETWEEN TYM.StartDate AND TYM.EndDate 
				FOR XML PATH(''),TYPE).value('text()[1]','nvarchar(max)'),1,2,N'') AS DivisionIDs
			,ISNULL((SELECT 
						AVG(DATEDIFF(DAY,dtOrgCreatedDate,dtFirstSalesOrderDate))
					FROM
					(
						SELECT
							DM.numDivisionID
							,DM.bintCreatedDate AS dtOrgCreatedDate
							,MIN(OM.bintCreatedDate) dtFirstSalesOrderDate
						FROM
							CompanyInfo CI 
						INNER JOIN 
							DivisionMaster DM 
						ON 
							CI.numCompanyId = DM.numCompanyID
						INNER JOIN
							OpportunityMaster OM
						ON
							DM.numDivisionID = OM.numDivisionId
						WHERE 
							CI.numDomainID=@numDomainID 
							AND DM.numDomainID = @numDomainID
							AND OM.numDomainId=@numDomainID
							AND CI.vcHow=LD.numListItemID
							AND OM.tintOppType=1
							AND OM.tintOppStatus = 1
						GROUP BY
							DM.numDivisionID
							,DM.bintCreatedDate
					) TEMP
					WHERE
						DATEADD(MINUTE,-1 * @ClientTimezoneOffset,dtFirstSalesOrderDate) BETWEEN TYM.StartDate AND TYM.EndDate),0) intAverageDaysToConvert
			,ISNULL((SELECT 
						SUM(monDealAmount)
					FROM
						OpportunityMaster 
					WHERE
						numDomainId=@numDomainID
						AND tintOppType=1
						AND tintOppStatus=1
						AND numDivisionID IN (SELECT
													numDivisionID
												FROM
												(
													SELECT
														DM.numDivisionID
														,MIN(OM.bintCreatedDate) dtFirstSalesOrderDate
													FROM
														CompanyInfo CI 
													INNER JOIN 
														DivisionMaster DM 
													ON 
														CI.numCompanyId = DM.numCompanyID
													INNER JOIN
														OpportunityMaster OM
													ON
														DM.numDivisionID = OM.numDivisionId
													WHERE 
														CI.numDomainID=@numDomainID 
														AND DM.numDomainID = @numDomainID
														AND OM.numDomainId=@numDomainID
														AND CI.vcHow=LD.numListItemID
														AND OM.tintOppType=1
														AND OM.tintOppStatus = 1
													GROUP BY
														DM.numDivisionID
												) TEMP
												WHERE
													DATEADD(MINUTE,-1 * @ClientTimezoneOffset,dtFirstSalesOrderDate) BETWEEN TYM.StartDate AND TYM.EndDate)),0) monCampaignIncome
			,ISNULL((SELECT SUM(BD.monAmount) FROM BillHeader BH INNER JOIN BillDetails BD ON BH.numBillID=BD.numBillID WHERE BH.numDomainID=@numDomainID AND ISNULL(BD.numCampaignID,0) = LD.numListItemID AND BH.dtBillDate BETWEEN TYM.StartDate AND TYM.EndDate),0) AS monCampaignExpense
			,TYM.*
		INTO
			#TEMPYearFinal
		FROM 
			ListDetails LD
		OUTER APPLY
			#TempYearMonth TYM
		WHERE
			LD.numDomainID = @numDomainID
			AND LD.numListID = 18
			AND (ISNULL(@numLeadSource,0)=0 OR LD.numListItemID=@numLeadSource)
	
		UPDATE
			#TEMPYearFinal
		SET
			numConversionPercent=(CASE WHEN numLeadCount > 0 THEN (ISNULL(numCustomerCount,0) * 100)/numLeadCount ELSE 0.00 END)
			,monGrossProfit = ISNULL(monCampaignIncome,0) - ISNULL(monCampaignExpense,0) 


		SET @sql = 'SELECT
						Campaign,' + @columns + '
					FROM
					(
						SELECT
							Campaign
							,MonthYear
							,CONCAT(''{''
							,''TotalIncome:'',ISNULL(monTotalIncome,0),'',''
							,''LeadCount:'',ISNULL(numLeadCount,0),'',''
							,''CostPerLead:'',(CASE WHEN ISNULL(numLeadCount,0) > 0 THEN ISNULL(monCampaignExpense,0) / ISNULL(numLeadCount,0) ELSE 0 END),'',''
							,''CustomerCount:'',ISNULL(numCustomerCount,0),'',''
							,''CostPerCustomer:'',(CASE WHEN ISNULL(numCustomerCount,0) > 0 THEN ISNULL(monCampaignExpense,0) / ISNULL(numCustomerCount,0) ELSE 0 END),'',''
							,''DivisionIDs:'''''',ISNULL(DivisionIDs,''''),'''''',''
							,''ConversionPercent:'',(CASE WHEN numLeadCount > 0 THEN (ISNULL(numCustomerCount,0) * 100)/numLeadCount ELSE 0.00 END),'',''
							,''AverageDaysToConvert:'',ISNULL(intAverageDaysToConvert,0),'',''
							,''CampaignIncome:'',ISNULL(monCampaignIncome,0),'',''
							,''CampaignExpense:'',ISNULL(monCampaignExpense,0),'',''
							,''GrossProfit:'',ISNULL(monCampaignIncome,0) - ISNULL(monCampaignExpense,0),''}'') AS MonthTotal
						FROM
							#TEMPYearFinal
					) AS SourceTable
					pivot
					(
						MIN(MonthTotal)
						FOR [MonthYear] IN ( ' + @pivotcolumns + ' )
					) AS P ORDER BY Campaign'

			PRINT @sql
			EXECUTE (@sql)
	

		DROP TABLE #TEMPYearFinal
		DROP TABLE #TempYearMonth
	END
	ELSE
	BEGIN
		SELECT 
			LD.numListItemID
			,LD.vcData 
			,ISNULL((SELECT
						SUM(monDealAmount)
					FROM
						CompanyInfo CI 
					INNER JOIN 
						DivisionMaster DM 
					ON 
						CI.numCompanyId = DM.numCompanyID
					INNER JOIN
						OpportunityMaster OM
					ON
						DM.numDivisionID = OM.numDivisionId
					WHERE 
						CI.numDomainID=@numDomainID 
						AND DM.numDomainID = @numDomainID
						AND OM.numDomainId=@numDomainID
						AND CI.vcHow=LD.numListItemID
						AND OM.tintOppType=1
						AND OM.tintOppStatus = 1
						AND DATEADD(MINUTE,-1 * @ClientTimezoneOffset,OM.bintCreatedDate) BETWEEN @dtFromDate AND @dtToDate),0) monTotalIncome
			,ISNULL((SELECT 
						COUNT(DM.numDivisionID) 
					FROM 
						CompanyInfo CI 
					INNER JOIN 
						DivisionMaster DM 
					ON 
						CI.numCompanyId = DM.numCompanyID 
					WHERE 
						CI.numDomainID=@numDomainID 
						AND CI.vcHow=LD.numListItemID 
						AND DATEADD(MINUTE,-1 * @ClientTimezoneOffset,DM.bintCreatedDate) BETWEEN @dtFromDate AND @dtToDate),0) numLeadCount
			,ISNULL((SELECT 
						COUNT(numDivisionID)
					FROM
					(
						SELECT
							DM.numDivisionID
							,MIN(OM.bintCreatedDate) dtFirstSalesOrderDate
						FROM
							CompanyInfo CI 
						INNER JOIN 
							DivisionMaster DM 
						ON 
							CI.numCompanyId = DM.numCompanyID
						INNER JOIN
							OpportunityMaster OM
						ON
							DM.numDivisionID = OM.numDivisionId
						WHERE 
							CI.numDomainID=@numDomainID 
							AND DM.numDomainID = @numDomainID
							AND OM.numDomainId=@numDomainID
							AND CI.vcHow=LD.numListItemID
							AND OM.tintOppType=1
							AND OM.tintOppStatus = 1
						GROUP BY
							DM.numDivisionID
					) TEMP
					WHERE
						DATEADD(MINUTE,-1 * @ClientTimezoneOffset,dtFirstSalesOrderDate) BETWEEN @dtFromDate AND @dtToDate),0) numCustomerCount
			,Stuff((SELECT 
						CONCAT(N', ',numDivisionID)
					FROM
					(
						SELECT
							DM.numDivisionID
							,MIN(OM.bintCreatedDate) dtFirstSalesOrderDate
						FROM
							CompanyInfo CI 
						INNER JOIN 
							DivisionMaster DM 
						ON 
							CI.numCompanyId = DM.numCompanyID
						INNER JOIN
							OpportunityMaster OM
						ON
							DM.numDivisionID = OM.numDivisionId
						WHERE 
							CI.numDomainID=@numDomainID 
							AND DM.numDomainID = @numDomainID
							AND OM.numDomainId=@numDomainID
							AND CI.vcHow=LD.numListItemID
							AND OM.tintOppType=1
							AND OM.tintOppStatus = 1
						GROUP BY
							DM.numDivisionID
					) TEMP
					WHERE
						DATEADD(MINUTE,-1 * @ClientTimezoneOffset,dtFirstSalesOrderDate) BETWEEN @dtFromDate AND @dtToDate 
				FOR XML PATH(''),TYPE).value('text()[1]','nvarchar(max)'),1,2,N'') AS DivisionIDs
			,ISNULL((SELECT 
						AVG(DATEDIFF(DAY,dtOrgCreatedDate,dtFirstSalesOrderDate))
					FROM
					(
						SELECT
							DM.numDivisionID
							,DM.bintCreatedDate AS dtOrgCreatedDate
							,MIN(OM.bintCreatedDate) dtFirstSalesOrderDate
						FROM
							CompanyInfo CI 
						INNER JOIN 
							DivisionMaster DM 
						ON 
							CI.numCompanyId = DM.numCompanyID
						INNER JOIN
							OpportunityMaster OM
						ON
							DM.numDivisionID = OM.numDivisionId
						WHERE 
							CI.numDomainID=@numDomainID 
							AND DM.numDomainID = @numDomainID
							AND OM.numDomainId=@numDomainID
							AND CI.vcHow=LD.numListItemID
							AND OM.tintOppType=1
							AND OM.tintOppStatus = 1
						GROUP BY
							DM.numDivisionID
							,DM.bintCreatedDate
					) TEMP
					WHERE
						DATEADD(MINUTE,-1 * @ClientTimezoneOffset,dtFirstSalesOrderDate) BETWEEN @dtFromDate AND @dtToDate),0) intAverageDaysToConvert
			,ISNULL((SELECT 
						SUM(monDealAmount)
					FROM
						OpportunityMaster 
					WHERE
						numDomainId=@numDomainID
						AND tintOppType=1
						AND tintOppStatus=1
						AND numDivisionID IN (SELECT
													numDivisionID
												FROM
												(
													SELECT
														DM.numDivisionID
														,MIN(OM.bintCreatedDate) dtFirstSalesOrderDate
													FROM
														CompanyInfo CI 
													INNER JOIN 
														DivisionMaster DM 
													ON 
														CI.numCompanyId = DM.numCompanyID
													INNER JOIN
														OpportunityMaster OM
													ON
														DM.numDivisionID = OM.numDivisionId
													WHERE 
														CI.numDomainID=@numDomainID 
														AND DM.numDomainID = @numDomainID
														AND OM.numDomainId=@numDomainID
														AND CI.vcHow=LD.numListItemID
														AND OM.tintOppType=1
														AND OM.tintOppStatus = 1
													GROUP BY
														DM.numDivisionID
												) TEMP
												WHERE
													DATEADD(MINUTE,-1 * @ClientTimezoneOffset,dtFirstSalesOrderDate) BETWEEN @dtFromDate AND @dtToDate)),0) monCampaignIncome
			,ISNULL((SELECT SUM(BD.monAmount) FROM BillHeader BH INNER JOIN BillDetails BD ON BH.numBillID=BD.numBillID WHERE BH.numDomainID=@numDomainID AND ISNULL(BD.numCampaignID,0) = LD.numListItemID AND BH.dtBillDate BETWEEN @dtFromDate AND @dtToDate),0) AS monCampaignExpense
		INTO
			#TEMPFinal
		FROM 
			ListDetails LD
		WHERE
			LD.numDomainID = @numDomainID
			AND LD.numListID = 18
			AND (ISNULL(@numLeadSource,0)=0 OR LD.numListItemID=@numLeadSource)
	
		SELECT
			ISNULL(vcData,'') AS Campaign
			,CONCAT('{'
					,'TotalIncome:',ISNULL(monTotalIncome,0),','
					,'LeadCount:',ISNULL(numLeadCount,0),','
					,'CostPerLead:',(CASE WHEN ISNULL(numLeadCount,0) > 0 THEN ISNULL(monCampaignExpense,0) / ISNULL(numLeadCount,0) ELSE 0 END),','
					,'CustomerCount:',ISNULL(numCustomerCount,0),','
					,'CostPerCustomer:',(CASE WHEN ISNULL(numCustomerCount,0) > 0 THEN ISNULL(monCampaignExpense,0) / ISNULL(numCustomerCount,0) ELSE 0 END),','
					,'DivisionIDs:''',ISNULL(DivisionIDs,''),''','
					,'ConversionPercent:',(CASE WHEN numLeadCount > 0 THEN (ISNULL(numCustomerCount,0) * 100)/numLeadCount ELSE 0.00 END),','
					,'AverageDaysToConvert:',ISNULL(intAverageDaysToConvert,0),','
					,'CampaignIncome:',ISNULL(monCampaignIncome,0),','
					,'CampaignExpense:',ISNULL(monCampaignExpense,0),','
					,'GrossProfit:',ISNULL(monCampaignIncome,0) - ISNULL(monCampaignExpense,0),'}') AS Totals
		FROM
			#TEMPFinal
		ORDER BY 
			vcData

		DROP TABLE #TEMPFinal
	END
END
GO





