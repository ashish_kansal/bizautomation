GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_OpportunityMaster_SetAsWinningBid')
DROP PROCEDURE dbo.USP_OpportunityMaster_SetAsWinningBid
GO
CREATE PROCEDURE [dbo].[USP_OpportunityMaster_SetAsWinningBid]
(
	@numDomainID NUMERIC(18,0),
    @numOppID NUMERIC(18,0),
    @numOppItemID NUMERIC(18,0)
)
AS
BEGIN
	UPDATE OpportunityItems SET bitWinningBid=0 WHERE numOppId=@numOppID
	UPDATE OpportunityItems SET bitWinningBid=1 WHERE numOppId=@numOppID AND numoppitemtCode=@numOppItemID
END
GO