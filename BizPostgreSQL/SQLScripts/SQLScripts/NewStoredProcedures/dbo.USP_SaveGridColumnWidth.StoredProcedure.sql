GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj  
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_SaveGridColumnWidth')
DROP PROCEDURE USP_SaveGridColumnWidth
GO
CREATE PROCEDURE [dbo].[USP_SaveGridColumnWidth]  
	@numDomainID AS NUMERIC(18,0)=0,  
	@numUserCntID AS NUMERIC(18,0)=0,
	@str AS TEXT
AS  
BEGIN
	declare @hDoc as int     
	EXEC sp_xml_preparedocument @hDoc OUTPUT, @str                                                                        

	CREATE TABLE #tempTable
	(
		numFormId NUMERIC(18,0)
		,numFieldId NUMERIC(9)
		,bitCustom BIT
		,intColumnWidth INT
	)

	INSERT INTO #tempTable 
	(
		numFormId
		,numFieldId
		,bitCustom
		,intColumnWidth
	)                                        
	SELECT 
		numFormId
		,numFieldId
		,bitCustom
		,CAST(intColumnWidth AS INT) 
	FROM 
		OPENXML (@hDoc,'/NewDataSet/GridColumnWidth',2)                                                  
	WITH 
	(
		numFormId numeric(9)
		,numFieldId numeric(9)
		,bitCustom bit
		,intColumnWidth FLOAT
	)                                           
     
	EXEC sp_xml_removedocument @hDoc  

	UPDATE 
		DFCD 
	SET 
		intColumnWidth=temp.intColumnWidth
	FROM 
		DycFormConfigurationDetails DFCD 
	JOIN 
		#tempTable temp
	ON 
		DFCD.numFormId=temp.numFormId 
		AND DFCD.numFieldId=temp.numFieldId 
		AND ISNULL(DFCD.bitCustom,0)=temp.bitCustom
	WHERE 
		DFCD.numDomainID=@numDomainID 
		AND DFCD.numUserCntID=@numUserCntID 
		AND DFCD.tintPagetype=1                                                         

	INSERT INTO DycFormConfigurationDetails
	(
		numDomainId
		,numUserCntID
		,numFormId
		,numFieldId
		,bitCustom
		,intColumnWidth
		,tintPagetype
		,intColumnNum
		,intRowNum
	)
	SELECT 
		@numDomainID
		,@numUserCntID
		,numFormId
		,numFieldId
		,bitCustom
		,intColumnWidth
		,1
		,1
		,1
	FROM
		#tempTable
	WHERE
		numFormId=145
		AND numFieldId NOT IN (SELECT numFieldId FROM DycFormConfigurationDetails WHERE numDomainID=@numDomainID AND numUserCntID=@numUserCntID AND numFormId=145)

INSERT INTO DycFormConfigurationDetails
	(
		numDomainId
		,numUserCntID
		,numFormId
		,numFieldId
		,bitCustom
		,intColumnWidth
		,tintPagetype
		,intColumnNum
		,intRowNum
	)
	SELECT 
		@numDomainID
		,@numUserCntID
		,numFormId
		,numFieldId
		,bitCustom
		,intColumnWidth
		,1
		,1
		,1
	FROM
		#tempTable
	WHERE
		numFormId=43
		AND numFieldId NOT IN (SELECT numFieldId FROM DycFormConfigurationDetails WHERE numDomainID=@numDomainID AND numUserCntID=@numUserCntID AND numFormId=43)

	DROP TABLE #tempTable                                        

END
GO


