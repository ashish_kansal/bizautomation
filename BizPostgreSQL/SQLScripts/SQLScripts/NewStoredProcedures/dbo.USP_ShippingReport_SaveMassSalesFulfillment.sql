SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ShippingReport_SaveMassSalesFulfillment')
DROP PROCEDURE USP_ShippingReport_SaveMassSalesFulfillment
GO
CREATE PROCEDURE [dbo].[USP_ShippingReport_SaveMassSalesFulfillment]
	@numDomainId NUMERIC(18,0)
	,@numUserCntID NUMERIC(18,0)
	,@numOppID NUMERIC(18,0)
    ,@numOppBizDocId NUMERIC(18,0)
	,@numShippingCompany NUMERIC(18,0)
	,@numShippingService NUMERIC(18,0)
	,@vcBoxes VARCHAR(MAX)
	,@vcBoxItems VARCHAR(MAX)
	,@vcShippingDetail VARCHAR(MAX)
AS
BEGIN
BEGIN TRY
BEGIN TRANSACTION
	
	DECLARE @numShippingReportID NUMERIC(18,0)
	DECLARE @numOrderShippingCompany NUMERIC(18,0)
	DECLARE @numOrderShippingService NUMERIC(18,0)
	DECLARE @numDivisionShippingCompany NUMERIC(18,0)
    DECLARE @numDivisionShippingService NUMERIC(18,0)

	IF ISNULL(@numShippingCompany,0) = 0 OR ISNULL(@numShippingService,0) = 0
	BEGIN
		SELECT
			@numOrderShippingCompany = ISNULL(intUsedShippingCompany,0)
			,@numOrderShippingService = ISNULL(numShippingService,0)
			,@numDivisionShippingCompany = ISNULL(intShippingCompany,0)
			,@numDivisionShippingService = ISNULL(numDefaultShippingServiceID,0)
		FROM
			OpportunityMaster
		INNER JOIN
			DivisionMaster
		ON
			OpportunityMaster.numDivisionId = DivisionMaster.numDivisionID
		WHERE
			OpportunityMaster.numDomainId = @numDomainId
			AND DivisionMaster.numDomainID=@numDomainId
			AND OpportunityMaster.numOppId = @numOppID

	
		-- FIRST CHECK IF ORDER LEVEL SHIPPING FIELDS HAVE VALUES
		IF ISNULL(@numOrderShippingCompany,0) > 0
		BEGIN
			IF ISNULL(@numOrderShippingService,0) = 0
			BEGIN
				SET @numOrderShippingService = CASE @numOrderShippingCompany 
										WHEN 91 THEN 15 --FEDEX
										WHEN 88 THEN 43 --UPS
										WHEN 90 THEN 70 --USPS
									 END
			END

			SET @numShippingCompany = @numOrderShippingCompany
			SET @numShippingService = @numOrderShippingService
		END
		ELSE IF ISNULL(@numDivisionShippingCompany,0) > 0 -- IF DIVISION SHIPPIGN SETTING AVAILABLE
		BEGIN
			IF ISNULL(@numDivisionShippingService,0) = 0
			BEGIN
				SET @numDivisionShippingService = CASE @numDivisionShippingCompany 
										WHEN 91 THEN 15 --FEDEX
										WHEN 88 THEN 43 --UPS
										WHEN 90 THEN 70 --USPS
									 END
			END

			SET @numShippingCompany = @numDivisionShippingCompany
			SET @numShippingService = @numDivisionShippingService
		END
		ELSE -- USE DOMAIN DEFAULT
		BEGIN
			SELECT @numShippingCompany=ISNULL(numShipCompany,0) FROM Domain WHERE numDomainId=@numDomainId
		
			IF @numShippingCompany <> 91 OR @numShippingCompany <> 88 OR @numShippingCompany <> 90
			BEGIN
				SET @numShippingCompany = 91
			END

			SET @numShippingService = CASE @numShippingCompany 
										WHEN 91 THEN 15 --FEDEX
										WHEN 88 THEN 43 --UPS
										WHEN 90 THEN 70 --USPS
									 END
		END
	END

	

	DECLARE @vcFromName VARCHAR(1000)
    DECLARE @vcFromCompany VARCHAR(1000)
    DECLARE @vcFromPhone VARCHAR(100)
    DECLARE @vcFromAddressLine1 VARCHAR(50)
    DECLARE @vcFromAddressLine2 VARCHAR(50) = ''
    DECLARE @vcFromCity VARCHAR(50)
	DECLARE @vcFromState VARCHAR(50)
    DECLARE @vcFromZip VARCHAR(50)
    DECLARE @vcFromCountry VARCHAR(50)
	DECLARE @bitFromResidential BIT = 0

	DECLARE @vcToName VARCHAR(1000)
    DECLARE @vcToCompany VARCHAR(1000)
    DECLARE @vcToPhone VARCHAR(100)
    DECLARE @vcToAddressLine1 VARCHAR(50)
    DECLARE @vcToAddressLine2 VARCHAR(50) = ''
	DECLARE @vcToCity VARCHAR(50)
	DECLARE @vcToState VARCHAR(50)
    DECLARE @vcToZip VARCHAR(50)
    DECLARE @vcToCountry VARCHAR(50)
    DECLARE @bitToResidential BIT = 0

	DECLARE @IsCOD BIT,@IsDryIce BIT,@IsHoldSaturday BIT,@IsHomeDelivery BIT,@IsInsideDelevery BIT,@IsInsidePickup BIT,@IsReturnShipment BIT,@IsSaturdayDelivery BIT,@IsSaturdayPickup BIT,@IsAdditionalHandling BIT,@IsLargePackage BIT
	DECLARE @numCODAmount NUMERIC(18,2),@vcCODType VARCHAR(50),@vcDeliveryConfirmation VARCHAR(1000),@vcDescription VARCHAR(MAX)
	DECLARE @tintSignatureType TINYINT
	DECLARE @tintPayorType TINYINT=0
	DECLARE @vcPayorAccountNo VARCHAR(20)=''
	DECLARE @numPayorCountry NUMERIC(18,0)
	DECLARE @vcPayorZip VARCHAR(50)=''
	DECLARE @numTotalCustomsValue NUMERIC(18,2)
	DECLARE @numTotalInsuredValue NUMERIC(18,2)

	IF LEN(ISNULL(@vcShippingDetail,'')) > 0
	BEGIN
		DECLARE @hDocShippingDetail as INTEGER
		EXEC sp_xml_preparedocument @hDocShippingDetail OUTPUT, @vcShippingDetail

		SELECT 
			@vcFromName=FromContact
			,@vcFromCompany=FromCompany
			,@vcFromPhone=FromPhone
			,@vcFromAddressLine1=FromAddress1
			,@vcFromAddressLine2=FromAddress2
			,@vcFromCity=FromCity
			,@vcFromState=CAST(FromState AS VARCHAR(18))
			,@vcFromZip=FromZip
			,@vcFromCountry=CAST(FromCountry AS VARCHAR(18))
			,@bitFromResidential=IsFromResidential
			,@vcToName=ToContact
			,@vcToCompany=ToCompany
			,@vcToPhone=ToPhone
			,@vcToAddressLine1=ToAddress1
			,@vcToAddressLine2=ToAddress2
			,@vcToCity=ToCity
			,@vcToState=CAST(ToState AS VARCHAR(18))
			,@vcToZip=ToZip
			,@vcToCountry=CAST(ToCountry AS VARCHAR(18))
			,@bitToResidential=IsToResidential
			,@tintPayorType=PayerType
			,@vcPayorAccountNo=AccountNo
			,@numPayorCountry=Country
			,@vcPayorZip=ZipCode
			,@IsCOD = IsCOD
			,@IsHomeDelivery = IsHomeDelivery
			,@IsInsideDelevery = IsInsideDelivery
			,@IsInsidePickup = IsInsidePickup
			,@IsSaturdayDelivery = IsSaturdayDelivery
			,@IsSaturdayPickup = IsSaturdayPickup
			,@IsAdditionalHandling = IsAdditionalHandling
			,@IsLargePackage=IsLargePackage
			,@numTotalInsuredValue = TotalInsuredValue
            ,@numTotalCustomsValue = TotalCustomsValue
			,@vcCODType = CODType
			,@numCODAmount = CODAmount
			,@vcDescription = [Description]
			,@tintSignatureType = CAST(ISNULL(SignatureType,'0') AS TINYINT)
		FROM 
			OPENXML(@hDocShippingDetail,'/NewDataSet/Table1',2)                                                                          
		WITH                       
		(
			FromContact VARCHAR(1000)
            ,FromPhone VARCHAR(100)
            ,FromCompany VARCHAR(1000)
            ,FromAddress1 VARCHAR(50)
            ,FromAddress2 VARCHAR(50)
            ,FromCountry NUMERIC(18,0)
            ,FromState NUMERIC(18,0)
            ,FromCity VARCHAR(50)
            ,FromZip VARCHAR(50)
            ,IsFromResidential BIT
            ,ToContact VARCHAR(1000)
            ,ToPhone VARCHAR(100)
            ,ToCompany VARCHAR(1000)
            ,ToAddress1 VARCHAR(50)
            ,ToAddress2 VARCHAR(50)
            ,ToCountry NUMERIC(18,0)
            ,ToState NUMERIC(18,0)
            ,ToCity VARCHAR(50)
            ,ToZip VARCHAR(50)
            ,IsToResidential BIT
            ,PayerType TINYINT
            ,ReferenceNo VARCHAR(200)
            ,AccountNo VARCHAR(20)
            ,ZipCode VARCHAR(50)
            ,Country NUMERIC(18,0)
            ,IsAdditionalHandling BIT
            ,IsCOD BIT
            ,IsHomeDelivery BIT
            ,IsInsideDelivery BIT
            ,IsInsidePickup BIT
            ,IsLargePackage BIT
            ,IsSaturdayDelivery BIT
            ,IsSaturdayPickup BIT
            ,SignatureType TINYINT
            ,[Description] VARCHAR(MAX)
            ,TotalInsuredValue NUMERIC(18,2)
            ,TotalCustomsValue NUMERIC(18,2)
            ,CODType VARCHAR(50)
            ,CODAmount NUMERIC(18,2)
		)


		EXEC sp_xml_removedocument @hDocShippingDetail
	END
	ELSE
	BEGIN
		-- GET FROM ADDRESS
		SELECT
			@vcFromName=vcName
			,@vcFromCompany=vcCompanyName
			,@vcFromPhone=vcPhone
			,@vcFromAddressLine1=vcStreet
			,@vcFromCity=vcCity
			,@vcFromState=CAST(vcState AS VARCHAR(18))
			,@vcFromZip=vcZipCode
			,@vcFromCountry=CAST(vcCountry AS VARCHAR(18))
			,@bitFromResidential=bitResidential
		FROM
			dbo.fn_GetShippingReportAddress(1,@numDomainId,@numOppID)

		-- GET TO ADDRESS
		SELECT
			@vcToName=vcName
			,@vcToCompany=vcCompanyName
			,@vcToPhone=vcPhone
			,@vcToAddressLine1=vcStreet
			,@vcToCity=vcCity
			,@vcToState=CAST(vcState AS VARCHAR(18))
			,@vcToZip=vcZipCode
			,@vcToCountry=CAST(vcCountry AS VARCHAR(18))
			,@bitToResidential=bitResidential
		FROM
			dbo.fn_GetShippingReportAddress(2,@numDomainId,@numOppID)

		SELECT
			@IsCOD = IsCOD
			,@IsHomeDelivery = IsHomeDelivery
			,@IsInsideDelevery = IsInsideDelevery
			,@IsInsidePickup = IsInsidePickup
			,@IsSaturdayDelivery = IsSaturdayDelivery
			,@IsSaturdayPickup = IsSaturdayPickup
			,@IsAdditionalHandling = IsAdditionalHandling
			,@IsLargePackage=IsLargePackage
			,@vcCODType = vcCODType
			,@vcDeliveryConfirmation = vcDeliveryConfirmation
			,@vcDescription = vcDescription
			,@tintSignatureType = CAST(ISNULL(vcSignatureType,'0') AS TINYINT)
		FROM
			OpportunityMaster
		INNER JOIN
			DivisionMaster
		ON
			OpportunityMaster.numDivisionId = DivisionMaster.numDivisionID
		LEFT JOIN
			DivisionMasterShippingConfiguration
		ON
			DivisionMasterShippingConfiguration.numDomainID=@numDomainId
			AND DivisionMaster.numDivisionID = DivisionMasterShippingConfiguration.numDivisionID
		WHERE
			OpportunityMaster.numDomainId = @numDomainId
			AND DivisionMaster.numDomainID=@numDomainId
			AND OpportunityMaster.numOppId = @numOppID

		DECLARE @bitUseBizdocAmount BIT

		SELECT @bitUseBizdocAmount=bitUseBizdocAmount,@numTotalCustomsValue=ISNULL(numTotalInsuredValue,0),@numTotalInsuredValue=ISNULL(numTotalCustomsValue,0) FROM Domain WHERE numDomainId=@numDomainId

		IF @bitUseBizdocAmount = 1
		BEGIN
			SELECT @numTotalCustomsValue=ISNULL(monDealAmount,0),@numTotalInsuredValue=ISNULL(monDealAmount,0),@numCODAmount=ISNULL(monDealAmount,0) FROM OpportunityBizDocs WHERE numOppBizDocsId=@numOppBizDocId 
		END
		ELSE
		BEGIN
			SELECT @numCODAmount=ISNULL(monDealAmount,0) FROM OpportunityBizDocs WHERE numOppBizDocsId=@numOppBizDocId 
		END
	END

	-- MAKE ENTRY IN ShippingReport TABLE
	INSERT INTO ShippingReport
	(
		numDomainId,numCreatedBy,dtCreateDate,numOppID,numOppBizDocId,numShippingCompany,vcValue2
		,vcFromName,vcFromCompany,vcFromPhone,vcFromAddressLine1,vcFromAddressLine2,vcFromCity,vcFromState,vcFromZip,vcFromCountry,bitFromResidential
		,vcToName,vcToCompany,vcToPhone,vcToAddressLine1,vcToAddressLine2,vcToCity,vcToState,vcToZip,vcToCountry,bitToResidential
		,IsCOD,IsDryIce,IsHoldSaturday,IsHomeDelivery,IsInsideDelevery,IsInsidePickup,IsReturnShipment,IsSaturdayDelivery,IsSaturdayPickup
		,numCODAmount,vcCODType,numTotalInsuredValue,IsAdditionalHandling,IsLargePackage,vcDeliveryConfirmation,vcDescription,numTotalCustomsValue,tintSignatureType
		,tintPayorType,vcPayorAccountNo,numPayorCountry,vcPayorZip
	)
	VALUES
	(
		@numDomainId,@numUserCntID,GETUTCDATE(),@numOppID,@numOppBizDocId,@numShippingCompany,CAST(@numShippingService AS VARCHAR)
		,@vcFromName,@vcFromCompany,@vcFromPhone,@vcFromAddressLine1,@vcFromAddressLine2,@vcFromCity,@vcFromState,@vcFromZip,@vcFromCountry,@bitFromResidential
		,@vcToName,@vcToCompany,@vcToPhone,@vcToAddressLine1,@vcToAddressLine2,@vcToCity,@vcToState,@vcToZip,@vcToCountry,@bitToResidential
		,@IsCOD,@IsDryIce,@IsHoldSaturday,@IsHomeDelivery,@IsInsideDelevery,@IsInsidePickup,@IsReturnShipment,@IsSaturdayDelivery,@IsSaturdayPickup
		,@numCODAmount,@vcCODType,@numTotalInsuredValue,@IsAdditionalHandling,@IsLargePackage,@vcDeliveryConfirmation,@vcDescription,@numTotalCustomsValue,@tintSignatureType
		,@tintPayorType,@vcPayorAccountNo,@numPayorCountry,@vcPayorZip
	)

	SET @numShippingReportID = SCOPE_IDENTITY()


	DECLARE @TableShippingBox TABLE
	(
		ID INT IDENTITY(1,1)
		,numBoxID INT
		,vcBoxName VARCHAR(300)
		,fltHeight FLOAT
		,fltWidth FLOAT
		,fltLength FLOAT
		,fltWeight FLOAT
		,bitUserProvidedBoxWeight BIT
		,numPackageType NUMERIC(18,0)
	)

	DECLARE @hDocBox as INTEGER
	EXEC sp_xml_preparedocument @hDocBox OUTPUT, @vcBoxes

	INSERT INTO @TableShippingBox
	(
		numBoxID
		,vcBoxName
		,fltHeight
		,fltWidth
		,fltLength
		,fltWeight
		,bitUserProvidedBoxWeight
		,numPackageType
	)
	SELECT 
		ID 
		,ISNULL(Name,'')
		,ISNULL(Height,0)
		,ISNULL(Width,0)
		,ISNULL([Length],0)                             
		,ISNULL([Weight],0)
		,ISNULL(IsUserProvidedBoxWeight,0)
		,PackageType
	FROM 
		OPENXML(@hDocBox,'/NewDataSet/Table1',2)                                                                          
	WITH                       
	(
		ID NUMERIC(18,0)
		,Name VARCHAR(100)
		,Height FLOAT
		,Width FLOAT
		,[Length] FLOAT                              
		,[Weight] FLOAT
		,IsUserProvidedBoxWeight BIT
		,PackageType NUMERIC(18,0)
	)

	EXEC sp_xml_removedocument @hDocBox

	DECLARE @TableShippingBoxItems TABLE
	(
		numBoxID NUMERIC(18,0)
		,numOppItemID NUMERIC(18,0)
		,numUnitHour FLOAT
	)

	DECLARE @hDocBoxItem as INTEGER
	EXEC sp_xml_preparedocument @hDocBoxItem OUTPUT, @vcBoxItems

	INSERT INTO @TableShippingBoxItems
	(
		numBoxID
		,numOppItemID
		,numUnitHour
	)
	SELECT 
		BoxID 
		,OppItemID
		,ISNULL(Quantity,0)
	FROM 
		OPENXML(@hDocBoxItem,'/NewDataSet/Table1',2)                                                                          
	WITH                       
	(
		BoxID NUMERIC(18,0)
		,OppItemID NUMERIC(18,0)
		,Quantity FLOAT
	)

	EXEC sp_xml_removedocument @hDocBoxItem

	DECLARE @iBox INT = 1
	DECLARE @iBoxCount INT 
	DECLARE @numUserBoxID NUMERIC(18,0)
	DECLARE @numTempBoxID NUMERIC(18,0)
	DECLARE @bitUserProvidedBoxWeight BIT
	SELECT @iBoxCount = COUNT(*) FROM @TableShippingBox

	WHILE @iBox <= @iBoxCount
	BEGIN
		SELECT 
			@numUserBoxID=numBoxID
			,@bitUserProvidedBoxWeight=ISNULL(bitUserProvidedBoxWeight,0)
		FROM 
			@TableShippingBox
		WHERE
			ID=@iBox

		INSERT INTO ShippingBox
		(
			vcBoxName
			,numShippingReportId
			,fltTotalWeight
			,fltHeight
			,fltWidth
			,fltLength
			,dtCreateDate
			,numCreatedBy
			,numPackageTypeID
			,numServiceTypeID
			,fltDimensionalWeight
			,numShipCompany
		)
		SELECT 
			ISNULL(vcBoxName,'')
			,@numShippingReportID
			,(CASE WHEN ISNULL(fltWeight,0) = 0 THEN CEILING(CAST((CASE WHEN ISNULL(fltHeight,0)=0 THEN 1 ELSE fltHeight END) * (CASE WHEN ISNULL(fltWidth,0)=0 THEN 1 ELSE fltWidth END) * (CASE WHEN ISNULL(fltLength,0)=0 THEN 1 ELSE fltLength END) AS FLOAT)/166) ELSE fltWeight END)
			,(CASE WHEN ISNULL(fltHeight,0)=0 THEN 1 ELSE fltHeight END)
			,(CASE WHEN ISNULL(fltWidth,0)=0 THEN 1 ELSE fltWidth END)
			,(CASE WHEN ISNULL(fltLength,0)=0 THEN 1 ELSE fltLength END)
			,GETUTCDATE()
			,@numUserCntID
			,(CASE 
				WHEN ISNULL(numPackageType,0) > 0 AND EXISTS (SELECT 
																	ID 
																FROM 
																	ShippingPackageType 
																WHERE 
																	numShippingCompanyID=ISNULL(@numShippingCompany,0)
																	AND numNsoftwarePackageTypeID=numPackageType
																	AND 1 = (CASE 
																				WHEN (numShippingCompanyID = 88 OR numShippingCompanyID=91) THEN 1 
																				ELSE (CASE WHEN ISNULL(bitEndicia,0) = ISNULL((SELECT vcShipFieldValue FROM ShippingFieldValues WHERE numDomainID=@numDomainID AND numListItemID=90 AND intShipFieldID=21),0) THEN 1 ELSE 0 END) 
																			END))
				THEN (SELECT TOP 1
						ID 
					FROM 
						ShippingPackageType 
					WHERE 
						numShippingCompanyID=ISNULL(@numShippingCompany,0)
						AND numNsoftwarePackageTypeID=numPackageType
						AND 1 = (CASE 
									WHEN (numShippingCompanyID = 88 OR numShippingCompanyID=91) THEN 1 
									ELSE (CASE WHEN ISNULL(bitEndicia,0) = ISNULL((SELECT vcShipFieldValue FROM ShippingFieldValues WHERE numDomainID=@numDomainID AND numListItemID=90 AND intShipFieldID=21),0) THEN 1 ELSE 0 END) 
								END))
				ELSE (CASE 
							WHEN @numShippingCompany = 88 THEN 19
							WHEN @numShippingCompany = 90 THEN (CASE WHEN ISNULL((SELECT TOP 1 vcShipFieldValue FROM ShippingFieldValues WHERE numDomainID=@numDomainId AND intShipFieldID=21 AND numListItemID=90),0) = '1' THEN 69 ELSE 50 END)
							WHEN @numShippingCompany = 91 THEN 7
						END) 
			END)
			,@numShippingService
			,(CASE WHEN ISNULL(fltWeight,0) = 0 THEN CEILING(CAST((CASE WHEN ISNULL(fltHeight,0)=0 THEN 1 ELSE fltHeight END) * (CASE WHEN ISNULL(fltWidth,0)=0 THEN 1 ELSE fltWidth END) * (CASE WHEN ISNULL(fltLength,0)=0 THEN 1 ELSE fltLength END) AS FLOAT)/166) ELSE fltWeight END)
			,@numShippingCompany
		FROM
			@TableShippingBox
		WHERE
			ID = @iBox

		SET @numTempBoxID = SCOPE_IDENTITY()

		-- MAKE ENTRY IN ShippingReportItems Table
		INSERT INTO ShippingReportItems
		(
			numShippingReportId
			,numItemCode
			,tintServiceType
			,monShippingRate
			,fltTotalWeight
			,intNoOfBox
			,fltHeight
			,fltWidth
			,fltLength
			,dtCreateDate
			,numCreatedBy
			,numBoxID
			,numOppBizDocItemID
			,intBoxQty
		)
		SELECT 
			@numShippingReportID
			,OpportunityBizDocItems.numItemCode
			,@numShippingService
			,0
			,(CASE 
				WHEN ISNULL(bitKitParent,0)=1 
						AND (CASE 
								WHEN EXISTS (SELECT ID.numItemDetailID FROM ItemDetails ID INNER JOIN Item IInner ON ID.numChildItemID=IInner.numItemCode WHERE ID.numItemKitID=I.numItemCode AND ISNULL(IInner.bitKitParent,0)=1)
								THEN 1
								ELSE 0
							END) = 0
				THEN 
					dbo.GetKitWeightBasedOnItemSelection(I.numItemCode,OI.vcChildKitSelectedItems)
				ELSE 
					ISNULL(fltWeight,0) 
			END)
			,1
			,ISNULL(I.fltHeight,0)
			,ISNULL(I.fltWidth,0)
			,ISNULL(I.fltLength,0)
			,GETUTCDATE()
			,@numUserCntID
			,@numTempBoxID
			,OpportunityBizDocItems.numOppBizDocItemID
			,TSBI.numUnitHour
		FROM
			OpportunityBizDocs
		INNER JOIN
			OpportunityBizDocItems
		ON
			OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
		INNER JOIN
			OpportunityItems OI
		ON
			OpportunityBizDocItems.numOppItemID=OI.numoppitemtCode
		INNER JOIN
			WareHouseItems WI
		ON
			OpportunityBizDocItems.numWarehouseItmsID=WI.numWareHouseItemID
		INNER JOIN
			Item I
		ON
			OpportunityBizDocItems.numItemCode=I.numItemCode
		INNER JOIN
			@TableShippingBoxItems TSBI
		ON
			OpportunityBizDocItems.numOppItemID = TSBI.numOppItemID
			AND OI.numoppitemtCode = TSBI.numOppItemID
		WHERE
			OpportunityBizDocs.numOppBizDocsId = @numOppBizDocId
			AND TSBI.numBoxID = @numUserBoxID

		IF ISNULL(@bitUserProvidedBoxWeight,0) <> 1
		BEGIN
			UPDATE
				ShippingBox 
			SET 
				fltTotalWeight = ISNULL(fltTotalWeight,0) + ISNULL((SELECT SUM((ISNULL(fltTotalWeight,0) * ISNULL(intBoxQty,0))) FROM ShippingReportItems WHERE numShippingReportId=@numShippingReportID AND numBoxID=@numTempBoxID),0) 
			WHERE 
				numBoxID=@numTempBoxID
		END
	
		SET @iBox = @iBox + 1
	END

	SELECT @numShippingReportID
COMMIT
END TRY
BEGIN CATCH
	DECLARE @ErrorMessage NVARCHAR(4000)
	DECLARE @ErrorNumber INT
	DECLARE @ErrorSeverity INT
	DECLARE @ErrorState INT
	DECLARE @ErrorLine INT
	DECLARE @ErrorProcedure NVARCHAR(200)

	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;

	SELECT 
		@ErrorMessage = ERROR_MESSAGE(),
		@ErrorNumber = ERROR_NUMBER(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE(),
		@ErrorLine = ERROR_LINE(),
		@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

	RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);

END CATCH
END