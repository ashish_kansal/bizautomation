
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_SaveTopicMaster')
DROP PROCEDURE usp_SaveTopicMaster
GO
CREATE PROCEDURE [dbo].[usp_SaveTopicMaster]                                   
(                         
@numTopicId as NUMERIC(18,0)=0  output , 
@intRecordType as int=0 , 
@numRecordId as NUMERIC(18,0)=0, 
@vcTopicTitle as varchar(MAX)='', 
@numCreatedBy as NUMERIC(18,0)=0, 
@numUpdatedBy as NUMERIC(18,0)=0, 
@bitIsInternal as bit=0, 
@numDomainId as NUMERIC(18,0)=0,
@status AS VARCHAR(100) OUTPUT 
)                                    
AS   
SET NOCOUNT ON                                                        
	IF @numTopicId = 0     
		BEGIN      
			INSERT INTO TopicMaster
			(
				intRecordType, numRecordId, vcTopicTitle, numCreateBy, dtmCreatedOn, numUpdatedBy, dtmUpdatedOn, bitIsInternal, numDomainId
			)
			VALUES
			(
				@intRecordType, @numRecordId, @vcTopicTitle, @numCreatedBy, GETDATE(), @numUpdatedBy, GETDATE(), @bitIsInternal, @numDomainId
			)
			SELECT @numTopicId=(SELECT SCOPE_IDENTITY())
			SET @status='1'
		END      
		ELSE      
		BEGIN      
			UPDATE 
				TopicMaster 
			SET 
				intRecordType=@intRecordType, 
				numRecordId=@numRecordId, 
				vcTopicTitle=@vcTopicTitle, 
				numUpdatedBy=@numUpdatedBy, 
				dtmUpdatedOn=GETDATE(), 
				bitIsInternal=@bitIsInternal, 
				numDomainId=@numDomainId
			WHERE 
				numTopicId=@numTopicId 
				AND numDomainID=@numDomainID  
			SET @status='3'
		END
GO