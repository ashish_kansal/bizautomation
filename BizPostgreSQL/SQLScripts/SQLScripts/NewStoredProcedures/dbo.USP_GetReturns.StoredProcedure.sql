/****** Object:  StoredProcedure [dbo].[USP_GetReturns]    Script Date: 01/22/2009 01:39:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_GetReturns' ) 
    DROP PROCEDURE USP_GetReturns
GO
CREATE PROCEDURE [dbo].[USP_GetReturns]
    @numReturnId NUMERIC(18,0) = NULL,
    @numDomainId NUMERIC(18,0) = 0,
	@numUserCntID NUMERIC(18,0)=0,
    @numReturnStatus NUMERIC(9) = NULL,
    @numReturnReason NUMERIC(9) = NULL,
    @vcOrgSearchText VARCHAR(50) = NULL,
    @CurrentPage INT,
    @PageSize INT,
    @TotRecs INT OUTPUT,
    @ClientTimeZoneOffset AS INT,
    @tintReturnType AS TINYINT,
	@vcRMASearchText VARCHAR(50) = NULL,
	@vcSortColumn AS VARCHAR(50) = NULL,
    @tintSortDirection AS TINYINT = 1 --1 = ASC, 2 = DESC
AS 
BEGIN
	DECLARE @firstRec AS INTEGER
    DECLARE @lastRec AS INTEGER
    
	SET @firstRec = ( @CurrentPage - 1 ) * @PageSize
    SET @lastRec = ( @CurrentPage * @PageSize + 1 )
       
	CREATE TABLE #TempSalesReturn
	(
		numOppId NUMERIC(18,0),
        numReturnHeaderID NUMERIC(18,0),
        vcRMA VARCHAR(300),
        numReturnStatus NUMERIC(18,0),
        numReturnReason NUMERIC(18,0),
        numDivisionId NUMERIC(18,0),
		dtCreateDate VARCHAR(50),
		dtOriginal DATETIME,
        tintReturnType TINYINT,
		tintReceiveType TINYINT,
		vcCompanyName VARCHAR(300),
		[numAccountID] NUMERIC(18,0),
		reasonforreturn VARCHAR(200),
		[status] VARCHAR(200),
		vcCreditFromAccount VARCHAR(300),
		IsEditable BIT,
		vcBizDocName VARCHAR(300)
	)
	
		INSERT INTO 
			#TempSalesReturn
		SELECT
			RH.numoppid,
			RH.numReturnHeaderID,
			vcRMA,
			RH.numReturnStatus,
			RH.numReturnReason,
			RH.numDivisionId,
			CASE WHEN CONVERT(VARCHAR(11), DATEADD(minute, -@ClientTimeZoneOffset, RH.dtCreatedDate)) = CONVERT(VARCHAR(11), GETDATE())
					THEN '<b><font color=red>Today</font></b>'
					WHEN CONVERT(VARCHAR(11), DATEADD(minute, -@ClientTimeZoneOffset, RH.dtCreatedDate)) = CONVERT(VARCHAR(11), DATEADD(day, -1, GETDATE()))
					THEN '<b><font color=purple>YesterDay</font></b>'
					WHEN CONVERT(VARCHAR(11), DATEADD(minute, -@ClientTimeZoneOffset, RH.dtCreatedDate)) = CONVERT(VARCHAR(11), DATEADD(day, 1, GETDATE()))
					THEN '<b><font color=orange>Tommorow</font></b>'
					ELSE dbo.FormatedDateFromDate(RH.dtCreatedDate, RH.[numdomainid])
			END dtCreateDate,
			RH.dtCreatedDate,
			RH.tintReturnType, 
			RH.tintReceiveType,
			SUBSTRING(com.vcCompanyName, 0, 30) AS vcCompanyName
			,RH.[numAccountID]
			,dbo.[getlistiemname](RH.numReturnReason) reasonforreturn
			,dbo.[getlistiemname](RH.numReturnStatus) AS [status]
			,COA.vcAccountName AS vcCreditFromAccount,
			CASE dbo.[getlistiemname](numreturnstatus)
				WHEN 'Pending' THEN 'True'
				WHEN 'Confirmed' THEN 'True'
				ELSE 'True'
			END
			,RH.vcBizDocName
		FROM    	
			dbo.ReturnHeader AS RH 
		JOIN divisionMaster div ON div.numdivisionid = RH.numdivisionid
		JOIN companyInfo com ON com.numCompanyID = div.numCompanyID
		LEFT JOIN Chart_Of_Accounts COA ON RH.numAccountID = COA.numAccountId AND COA.numDomainId = RH.numDomainId
		WHERE 
			(RH.numReturnReason = @numReturnReason OR @numReturnReason IS NULL OR @numReturnReason = 0)
			AND (RH.numReturnStatus = @numReturnStatus OR @numReturnStatus IS NULL OR @numReturnStatus = 0)
			AND ( RH.numReturnHeaderID = @numReturnId OR @numReturnId IS NULL)
			AND RH.[numdomainid] = @numDomainId
			AND 1 = (CASE 
						WHEN NOT EXISTS (SELECT numUserDetailId FROM UserMaster WHERE numUserDetailId=@numUserCntID)
						THEN (CASE 
								WHEN EXISTS (SELECT EAD.numExtranetDtlID FROM ExtranetAccountsDtl EAD INNER JOIN ExtarnetAccounts EA ON EAD.numExtranetID=EA.numExtranetID WHERE EAD.numDomainID=@numDomainID AND EAD.numContactID=@numUserCntID AND EA.numDivisionID=div.numDivisionID)
								THEN 1 
								ELSE 0 
							END)
						ELSE 1
					END)
			AND 1 = (CASE @tintReturnType
						WHEN 3 THEN (CASE WHEN (RH.tintReturnType =1 AND tintReceiveType=2) OR RH.tintReturnType=3 THEN 1 ELSE 0 END) 
						WHEN 4 THEN (CASE WHEN (RH.tintReturnType =1 AND tintReceiveType=1) OR RH.tintReturnType=4 THEN 1 ELSE 0 END) 
						ELSE (CASE WHEN RH.tintReturnType = @tintReturnType THEN 1 ELSE 0 END) 
					END)
			AND ( ( RH.vcRMA LIKE '%' + @vcRMASearchText+ '%') OR @vcRMASearchText IS NULL OR LEN(@vcRMASearchText)=0) 
			AND (com.vcCompanyName LIKE ('%' + @vcOrgSearchText + '%') OR @vcOrgSearchText IS NULL OR LEN(@vcOrgSearchText)=0)

		--SELECT * FROM  #TempSalesReturn ORDER BY vcCreditFromAccount DESC OFFSET ((1 - 1) * 20) ROWS FETCH NEXT 20 ROWS ONLY
		SELECT  @TotRecs = COUNT(*) FROM #TempSalesReturn
      
		DECLARE @strSQL AS VARCHAR(500) = ''
		DECLARE @vcSort AS VARCHAR(500) = ''

		SET @vcSort = (CASE
						WHEN @vcSortColumn = 'vcCompanyName' THEN
							'vcCompanyName ' + (CASE WHEN @tintSortDirection = 1 THEN 'ASC' ELSE 'DESC' END)
						WHEN @vcSortColumn = 'vcRMA' THEN
							'vcRMA ' + (CASE WHEN @tintSortDirection = 1 THEN 'ASC' ELSE 'DESC' END)
						WHEN @vcSortColumn = 'vcCreditFromAccount' THEN
							'vcCreditFromAccount ' + (CASE WHEN @tintSortDirection = 1 THEN 'ASC' ELSE 'DESC' END)
						WHEN @vcSortColumn = 'ReasonforReturn' THEN
							'reasonforreturn ' + (CASE WHEN @tintSortDirection = 1 THEN 'ASC' ELSE 'DESC' END)
						WHEN @vcSortColumn = 'Status' THEN
							'[status] ' + (CASE WHEN @tintSortDirection = 1 THEN 'ASC' ELSE 'DESC' END)
						ELSE
							'dtOriginal ' + (CASE WHEN @tintSortDirection = 1 THEN 'ASC' ELSE 'DESC' END)
					END) 
	                   
		SET  @strSQL = 'SELECT * FROM (SELECT Row_Number() OVER (ORDER BY ' + @vcSort + ',numReturnHeaderID ASC) AS row,* FROM  #TempSalesReturn) t WHERE row > ' + CAST(@firstRec AS VARCHAR(10)) + ' AND row < ' + CAST(@lastRec AS VARCHAR(10))

		EXEC(@strSQL)
	
		DROP TABLE #TempSalesReturn
END
  
