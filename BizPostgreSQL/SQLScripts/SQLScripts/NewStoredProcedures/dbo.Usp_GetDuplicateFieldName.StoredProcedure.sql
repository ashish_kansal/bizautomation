
GO
/****** Object:  StoredProcedure [dbo].[Usp_getduplicatefieldname]    Script Date: 05/07/2009 22:26:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='Usp_GetDuplicateFieldName')
DROP PROCEDURE Usp_GetDuplicateFieldName
GO
CREATE PROCEDURE [dbo].[Usp_GetDuplicateFieldName]
                @numFormId   NUMERIC(9)  = 24,
                @numDomainId NUMERIC(9)
AS
  BEGIN
    SELECT vcFieldName as vcformfieldname,
           vcdbcolumnname,
           vclookbacktablename,
           numFieldID as numformfieldid
    from View_DynamicColumns 
 where numFormId=@numFormID and numDomainID=@numDomainID AND tintPageType=1  
 AND ISNULL(bitSettingField,0)=1

  END
