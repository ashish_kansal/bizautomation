GO
/****** Object:  StoredProcedure [dbo].[USP_GetEmailBroadcastConfiguration]    Script Date: 30/07/2012 17:47:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetEmailBroadcastConfiguration')
	DROP PROCEDURE usp_GetEmailBroadcastConfiguration
GO

CREATE PROCEDURE [dbo].[usp_GetEmailBroadcastConfiguration]
	@numDomainID NUMERIC(18,0),
	@numConfigurationID NUMERIC(18,0) , 
	@intMode INT 
AS
 IF @intMode = 1
	BEGIN
	    SELECT * FROM [dbo].[EmailBroadcastConfiguration] WHERE numDomainId = @numDomainID
	END
 ELSE IF @intMode = 2
    BEGIN
 	    SELECT * FROM [dbo].[EmailBroadcastConfiguration] WHERE numConfigurationID = @numConfigurationID 
	END	
 ELSE IF @intMode = 3
    BEGIN
 	    SELECT * FROM [dbo].[EmailBroadcastConfiguration] WHERE numDomainId = @numDomainID And numConfigurationID <> @numConfigurationID 
	END	
	
