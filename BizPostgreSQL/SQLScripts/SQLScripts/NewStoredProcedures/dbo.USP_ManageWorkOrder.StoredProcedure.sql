
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ManageWorkOrder')
DROP PROCEDURE USP_ManageWorkOrder
GO
CREATE PROCEDURE [dbo].[USP_ManageWorkOrder]
@numItemCode AS NUMERIC(18,0),
@numWareHouseItemID AS NUMERIC(18,0),
@Units AS INT,
@numDomainID AS NUMERIC(18,0),
@numUserCntID AS NUMERIC(18,0),
@vcInstruction AS VARCHAR(1000),
@bintCompliationDate AS DATETIME,
@numAssignedTo AS NUMERIC(9),
@numSalesOrderId AS NUMERIC(18,0)=0,
@numBuildProcessId AS NUMERIC(18,0)=0,
@dtmProjectFinishDate AS DATETIME=NULL,
@dtmEndDate AS DATETIME=NULL,
@dtmStartDate AS DATETIME=NULL
AS
BEGIN
BEGIN TRY
BEGIN TRAN
	DECLARE @numWarehouseID NUMERIC(18,0)
	SET @numWarehouseID = ISNULL((SELECT numWareHouseID FROM WareHouseItems WHERE numWareHouseItemID = @numWareHouseItemID),0)

	IF EXISTS (SELECT 
					item.numItemCode
				FROM 
					item                                
				INNER JOIN 
					ItemDetails Dtl 
				ON 
					numChildItemID=numItemCode
				WHERE 
					numItemKitID=@numItemCode
					AND charItemType='P'
					AND NOT EXISTS (SELECT numWareHouseItemId FROM WareHouseItems WHERE numItemID=item.numItemCode AND numWareHouseID=@numWarehouseID))
	BEGIN
		RAISERROR('SELECTED_WAREHOUSE_IS_NOT_AVAILABLE_FOR_ALL_CHILD_ITEMS',16,1)
		RETURN
	END

	DECLARE @numWOId AS NUMERIC(9),@numDivisionId AS NUMERIC(9) 
	DECLARE @txtItemDesc AS varchar(1000),@vcItemName AS VARCHAR(300)
	
	SELECT @txtItemDesc=ISNULL(txtItemDesc,''),@vcItemName=ISNULL(vcItemName,'') FROM Item WHERE numItemCode=@numItemCode

	IF ISNULL(@numAssignedTo,0) = 0
	BEGIN
		SET @numAssignedTo = ISNULL((SELECT numBuildManager FROM Sales_process_List_Master WHERE Slp_Id=@numBuildProcessId),0)
	END

	INSERT INTO WorkOrder
	(
		numItemCode,numQtyItemsReq,numWareHouseItemId,numCreatedBy,
		bintCreatedDate,numDomainID,numWOStatus,vcItemDesc,vcInstruction,
		bintCompliationDate,numAssignedTo,numOppId,numBuildProcessId,dtmStartDate,dtmEndDate
	)
	VALUES
	(
		@numItemCode,@Units,@numWareHouseItemID,@numUserCntID,
		getutcdate(),@numDomainID,0,@txtItemDesc,@vcInstruction,
		@bintCompliationDate,@numAssignedTo,@numSalesOrderId,@numBuildProcessId,@dtmStartDate,@dtmEndDate
	)
	
	SET @numWOId=@@IDENTITY

	EXEC dbo.USP_UpdateNameTemplateValueForWorkOrder 3,@numDomainID,@numWOId
	IF(@numBuildProcessId>0)
	BEGIN
			INSERT  INTO Sales_process_List_Master ( Slp_Name,
                                                 numdomainid,
                                                 pro_type,
                                                 numCreatedby,
                                                 dtCreatedon,
                                                 numModifedby,
                                                 dtModifiedOn,tintConfiguration,bitAutomaticStartTimer,bitAssigntoTeams,numOppId,numTaskValidatorId,
												 numProjectId,numWorkOrderId)
		 SELECT Slp_Name,
				numdomainid,
				pro_type,
				numCreatedby,
				dtCreatedon,
				numModifedby,
				dtModifiedOn,tintConfiguration,bitAutomaticStartTimer,bitAssigntoTeams,0,numTaskValidatorId,0,@numWOId FROM Sales_process_List_Master WHERE Slp_Id=@numBuildProcessId    
			DECLARE @numNewProcessId AS NUMERIC(18,0)=0
			SET @numNewProcessId=(SELECT SCOPE_IDENTITY())
			UPDATE WorkOrder SET numBuildProcessId=@numNewProcessId WHERE numWOId=@numWOId
			INSERT INTO StagePercentageDetails(
			numStagePercentageId, 
			tintConfiguration, 
			vcStageName, 
			numDomainId, 
			numCreatedBy, 
			bintCreatedDate, 
			numModifiedBy, 
			bintModifiedDate, 
			slp_id, 
			numAssignTo, 
			vcMileStoneName, 
			tintPercentage, 
			tinProgressPercentage, 
			dtStartDate, 
			numParentStageID, 
			intDueDays, 
			numProjectID, 
			numOppID, 
			vcDescription, 
			bitIsDueDaysUsed,
			numTeamId, 
			bitRunningDynamicMode,
			numStageOrder,
			numWorkOrderId
		)
		SELECT
			numStagePercentageId, 
			tintConfiguration, 
			vcStageName, 
			numDomainId, 
			@numUserCntID, 
			GETDATE(), 
			@numUserCntID, 
			GETDATE(), 
			@numNewProcessId, 
			numAssignTo, 
			vcMileStoneName, 
			tintPercentage, 
			tinProgressPercentage, 
			GETDATE(), 
			numParentStageID, 
			intDueDays, 
			0, 
			0, 
			vcDescription, 
			bitIsDueDaysUsed,
			numTeamId, 
			bitRunningDynamicMode,
			numStageOrder,
			@numWOId
		FROM
			StagePercentageDetails
		WHERE
			slp_id=@numBuildProcessId	

			INSERT INTO StagePercentageDetailsTask(
			numStageDetailsId, 
			vcTaskName, 
			numHours, 
			numMinutes, 
			numAssignTo, 
			numDomainID, 
			numCreatedBy, 
			dtmCreatedOn,
			numOppId,
			numProjectId,
			numParentTaskId,
			bitDefaultTask,
			bitSavedTask,
			numOrder,
			numReferenceTaskId,
			numWorkOrderId
		)
		SELECT 
			(SELECT TOP 1 SFD.numStageDetailsId FROM StagePercentageDetails AS SFD WHERE SFD.vcStageName=SP.vcStageName AND SFD.numWorkOrderId=@numWOId),
			vcTaskName,
			numHours,
			numMinutes,
			ST.numAssignTo,
			@numDomainID,
			@numUserCntID,
			GETDATE(),
			0,
			0,
			0,
			1,
			CASE WHEN SP.bitRunningDynamicMode=1 THEN 0 ELSE 1 END,
			numOrder,
			numTaskId,
			@numWOId
		FROM 
			StagePercentageDetailsTask AS ST
		LEFT JOIN
			StagePercentageDetails As SP
		ON
			ST.numStageDetailsId=SP.numStageDetailsId
		WHERE
			ISNULL(ST.numOppId,0)=0 AND ISNULL(ST.numProjectId,0)=0 AND
			ST.numStageDetailsId IN(SELECT numStageDetailsId FROM 
			StagePercentageDetails As ST
		WHERE
			ST.slp_id=@numBuildProcessId)
		ORDER BY ST.numOrder
	END

	INSERT INTO [WorkOrderDetails] 
	(
		numWOId,numItemKitID,numChildItemID,numQtyItemsReq,numWareHouseItemId,vcItemDesc,sintOrder,numQtyItemsReq_Orig,numUOMId
	)
	SELECT 
		@numWOId,numItemKitID,numItemCode,
		CAST(((DTL.numQtyItemsReq * ISNULL(dbo.fn_UOMConversion(Dtl.numUOMID,item.numItemCode,item.numDomainID,item.numBaseUnit),1)) * @Units) AS FLOAT),
		isnull((SELECT TOP 1 numWareHouseItemId FROM WareHouseItems WHERE numItemID=item.numItemCode AND numWareHouseID=@numWarehouseID ORDER BY ISNULL(numWLocationID,0),numWareHouseItemID),0),
		ISNULL(Dtl.vcItemDesc,txtItemDesc),
		ISNULL(sintOrder,0),
		DTL.numQtyItemsReq,
		Dtl.numUOMId 
	FROM 
		item                                
	INNER JOIN 
		ItemDetails Dtl 
	ON 
		numChildItemID=numItemCode
	WHERE 
		numItemKitID=@numItemCode

	--UPDATE ON ORDER OF ASSEMBLY
	UPDATE 
		WareHouseItems
	SET    
		numOnOrder= ISNULL(numOnOrder,0) + @Units,
		dtModified = GETDATE() 
	WHERE   
		numWareHouseItemID = @numWareHouseItemID 
	
	--MAKE ENTRY IN WAREHOUSE TRACKING TABLE
	DECLARE @Description AS VARCHAR(1000)
	SET @Description=CONCAT('Work Order Created (Qty:',@Units,')')

	EXEC dbo.USP_ManageWareHouseItems_Tracking
	@numWareHouseItemID = @numWareHouseItemID, --  numeric(9, 0)
	@numReferenceID = @numWOId, --  numeric(9, 0)
	@tintRefType = 2, --  tinyint
	@vcDescription = @Description, --  varchar(100)
	@numModifiedBy = @numUserCntID,
	@numDomainID = @numDomainID

	EXEC USP_ManageInventoryWorkOrder @numWOId,@numDomainID,@numUserCntID
COMMIT TRAN 
END TRY 
BEGIN CATCH		
    IF ( @@TRANCOUNT > 0 ) 
    BEGIN
        ROLLBACK TRAN
    END

	DECLARE @ErrorMessage NVARCHAR(4000);
    DECLARE @ErrorSeverity INT;
    DECLARE @ErrorState INT;

    SELECT @ErrorMessage = ERROR_MESSAGE(), @ErrorSeverity = ERROR_SEVERITY(), @ErrorState = ERROR_STATE();

    RAISERROR (@ErrorMessage, @ErrorSeverity, @ErrorState);
END CATCH
END