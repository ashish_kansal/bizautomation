GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_CalculateCommission')
DROP PROCEDURE USP_CalculateCommission
GO
CREATE PROCEDURE [dbo].[USP_CalculateCommission]
	-- Add the parameters for the stored procedure here
	 @numComPayPeriodID AS NUMERIC(18,0),
	 @numDomainID AS NUMERIC(18,0),
	 @ClientTimeZoneOffset INT
AS
BEGIN
	-- NOTE: CALLED FROM USP_CommissionPayPeriod_Save WITH TRANSACTION

	SET NOCOUNT ON;

	DECLARE @dtPayStart DATE
	DECLARE @dtPayEnd DATE

	DECLARE @TempBizCommission TABLE
	(
		numUserCntID NUMERIC(18,0),
		numOppID NUMERIC(18,0),
		numOppBizDocID NUMERIC(18,0),
		numOppBizDocItemID NUMERIC(18,0),
		monCommission DECIMAL(20,5),
		numComRuleID NUMERIC(18,0),
		tintComType TINYINT,
		tintComBasedOn TINYINT,
		decCommission FLOAT,
		tintAssignTo TINYINT,
		numOppItemID NUMERIC(18,0),
		monInvoiceSubTotal DECIMAL(20,5),
		monOrderSubTotal DECIMAL(20,5)
	)

	DECLARE @TABLEPAID TABLE
	(
		ID INT IDENTITY(1,1),
		numDivisionID NUMERIC(18,0),
		numRelationship NUMERIC(18,0),
		numProfile NUMERIC(18,0),
		numAssignedTo NUMERIC(18,0),
		numRecordOwner NUMERIC(18,0),
		numItemCode NUMERIC(18,0),
		numItemClassification NUMERIC(18,0),
		numOppID NUMERIC(18,0),
		numOppItemID NUMERIC(18,0),
		numOppBizDocID NUMERIC(18,0),
		numOppBizDocItemID NUMERIC(18,0),
		numUnitHour FLOAT,
		monTotAmount DECIMAL(20,5),
		monInvoiceSubTotal DECIMAL(20,5),
		monOrderSubTotal DECIMAL(20,5),
		monVendorCost DECIMAL(20,5),
		monAvgCost DECIMAL(20,5),
		numPartner NUMERIC(18,0),
		numPOID NUMERIC(18,0),
		numPOItemID NUMERIC(18,0),
		monPOCost DECIMAL(20,5)
	)

	SELECT 
		@dtPayStart=dtStart
		,@dtPayEnd=dtEnd 
	FROM 
		CommissionPayPeriod 
	WHERE 
		numComPayPeriodID = @numComPayPeriodID

	--GET COMMISSION GLOBAL SETTINGS
	DECLARE @numDiscountServiceItemID NUMERIC(18,0)
	DECLARE @numShippingServiceItemID NUMERIC(18,0)
	DECLARE @tintCommissionType TINYINT
	DECLARE @bitCommissionBasedOn BIT
	DECLARE @tintCommissionBasedOn TINYINT
	DECLARE @bitIncludeTaxAndShippingInCommission BIT

	SELECT 
		@numDiscountServiceItemID=numDiscountServiceItemID, 
		@numShippingServiceItemID = numShippingServiceItemID, 
		@tintCommissionType=tintCommissionType, 
		@bitIncludeTaxAndShippingInCommission=bitIncludeTaxAndShippingInCommission
		,@tintCommissionBasedOn=ISNULL(tintCommissionBasedOn,1)
		,@bitCommissionBasedOn=ISNULL(bitCommissionBasedOn,0)
	FROM 
		Domain 
	WHERE 
		numDomainID=@numDomainID

	DELETE 
		BC 
	FROM 
		BizDocComission BC
	LEFT JOIN
		OpportunityMaster OM
	ON
		BC.numOppID = OM.numOppId
	WHERE 
		BC.numDomainId=@numDomainID
		AND ISNULL(BC.numOppID,0) > 0
		AND ISNULL(BC.bitCommisionPaid,0)=0
		AND OM.numOppId IS NULL

	DELETE 
		BC 
	FROM 
		BizDocComission BC
	LEFT JOIN
		OpportunityItems OI
	ON
		BC.numOppItemID = OI.numoppitemtCode
	WHERE 
		BC.numDomainId=@numDomainID 
		AND ISNULL(BC.numOppItemID,0) > 0
		AND ISNULL(BC.bitCommisionPaid,0)=0
		AND OI.numoppitemtCode IS NULL

	DELETE 
		BC 
	FROM 
		BizDocComission BC
	LEFT JOIN
		OpportunityBizDocs OBD
	ON
		BC.numOppBizDocId = OBD.numOppBizDocsId
	WHERE 
		BC.numDomainId=@numDomainID
		AND ISNULL(BC.numOppBizDocId,0) > 0
		AND ISNULL(BC.bitCommisionPaid,0)=0
		AND OBD.numOppBizDocsId IS NULL

	DELETE 
		BC 
	FROM 
		BizDocComission BC
	LEFT JOIN
		OpportunityBizDocItems OBDI
	ON
		BC.numOppBizDocItemID = OBDI.numOppBizDocItemID
	WHERE 
		BC.numDomainId=@numDomainID 
		AND ISNULL(BC.numOppBizDocItemID,0) > 0
		AND ISNULL(BC.bitCommisionPaid,0)=0
		AND OBDI.numOppBizDocItemID IS NULL

	-- DELETE UNPAID COMMISSION ENTRIES BETWEEN PAY PERIOD DATE RANGE	
	IF @tintCommissionType = 3 --Sales Order Sub-Total Amount
	BEGIN
		DELETE FROM 
			BizDocComission 
		WHERE 
			numDomainId=@numDomainID 
			AND ISNULL(bitCommisionPaid,0)=0 
			AND numOppItemID IN (SELECT 
									OI.numoppitemtCode
								FROM 
									OpportunityMaster OM
								INNER JOIN
									OpportunityItems OI
								ON
									OM.numOppId = OI.numOppId
								WHERE
									OM.numDomainId = @numDomainID 
									AND OM.tintOppType = 1
									AND OM.tintOppStatus = 1
									AND CAST(DATEADD(MINUTE,@ClientTimeZoneOffset * -1,OM.bintCreatedDate) AS DATE) BETWEEN @dtPayStart AND @dtPayEnd)

		INSERT INTO 
			@TABLEPAID
		SELECT 
			OM.numDivisionID,
			ISNULL(CompanyInfo.numCompanyType,0),
			ISNULL(CompanyInfo.vcProfile,0),
			OM.numAssignedTo,
			OM.numRecOwner,
			OI.numItemCode,
			Item.numItemClassification,
			OM.numOppID,
			OI.numoppitemtCode,
			0,
			0,
			ISNULL(OI.numUnitHour,0),
			(CASE WHEN ISNULL(OM.fltExchangeRate,0)=0 OR ISNULL(OM.fltExchangeRate,0)=1 THEN ISNULL(OI.monTotAmount,0) ELSE ROUND(ISNULL(OI.monTotAmount,0) * OM.fltExchangeRate,2) END),
			0,
			(CASE WHEN ISNULL(OM.fltExchangeRate,0)=0 OR ISNULL(OM.fltExchangeRate,0)=1 THEN ISNULL(OI.monTotAmount,0) ELSE ROUND(ISNULL(OI.monTotAmount,0) * OM.fltExchangeRate,2) END),
			(ISNULL(OI.monVendorCost,0) * dbo.fn_UOMConversion(Item.numPurchaseUnit,Item.numItemCode,@numDomainID,Item.numBaseUnit) * ISNULL(OI.numUnitHour,0)),
			(ISNULL(OI.monAvgCost,0) * ISNULL(OI.numUnitHour,0)),
			OM.numPartner,
			ISNULL(TEMPMatchedPO.numOppId,TEMPPO.numOppId),
			ISNULL(TEMPMatchedPO.numoppitemtCode,TEMPPO.numoppitemtCode),
			ISNULL(ISNULL(TEMPMatchedPO.monPrice,TEMPPO.monPrice),0)
		FROM 
			OpportunityMaster OM
		INNER JOIN
			DivisionMaster 
		ON
			OM.numDivisionID = DivisionMaster.numDivisionID
		INNER JOIN
			CompanyInfo
		ON
			DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
		INNER JOIN
			OpportunityItems OI 
		ON
			OM.numOppId = OI.numOppId
		INNER JOIN
			Item
		ON
			OI.numItemCode = Item.numItemCode
		OUTER APPLY
		(
			SELECT TOP 1
				OMInner.numOppID
				,OMInner.vcPOppName
				,OIInner.numoppitemtCode
				,OIInner.monPrice
			FROM
				SalesOrderLineItemsPOLinking SOLIPL
			INNER JOIN
				OpportunityMaster OMInner
			ON
				SOLIPL.numPurchaseOrderID = OMInner.numOppId
			INNER JOIN
				OpportunityItems OIInner
			ON
				OMInner.numOppId = OIInner.numOppId
				AND SOLIPL.numPurchaseOrderItemID = OIInner.numoppitemtCode
			WHERE
				SOLIPL.numSalesOrderID = OM.numOppId
				AND SOLIPL.numSalesOrderItemID = OI.numoppitemtCode
		) TEMPMatchedPO
		OUTER APPLY
		(
			SELECT TOP 1
				OMInner.numOppID
				,OMInner.vcPOppName
				,OIInner.numoppitemtCode
				,OIInner.monPrice
			FROM
				OpportunityLinking OL
			INNER JOIN
				OpportunityMaster OMInner
			ON
				OL.numChildOppID = OMInner.numOppId
			INNER JOIN
				OpportunityItems OIInner
			ON
				OMInner.numOppId = OIInner.numOppId
			WHERE
				OL.numParentOppID = OM.numOppId
				AND OIInner.numItemCode = OI.numItemCode
				AND ISNULL(OIInner.vcAttrValues,'') = ISNULL(OI.vcAttrValues,'')
		) TEMPPO
		LEFT JOIN
			BizDocComission
		ON
			OM.numOppID = BizDocComission.numOppID
			AND OI.numoppitemtCode = BizDocComission.numOppItemID
			AND ISNULL(bitCommisionPaid,0) = 1
		WHERE
			OM.numDomainId = @numDomainID 
			AND OM.tintOppType = 1
			AND OM.tintOppStatus = 1
			AND CAST(DATEADD(MINUTE,@ClientTimeZoneOffset * -1,OM.bintCreatedDate) AS DATE) BETWEEN @dtPayStart AND @dtPayEnd
			AND BizDocComission.numComissionID IS NULL
			AND OI.numItemCode NOT IN ( @numDiscountServiceItemID )  /*DO NOT INCLUDE DISCOUNT ITEM*/
			AND 1 = (
						CASE 
							WHEN OI.numItemCode = @numShippingServiceItemID 
							THEN 
								CASE 
									WHEN ISNULL(@bitIncludeTaxAndShippingInCommission,0) = 1
									THEN	
										1
									ELSE	
										0
								END
							ELSE 
								1 
						END
					)/*CONDITION TO INCLUDE/EXCLUDE INCLUDE SHIPPING ITEM */
	END
	ELSE IF @tintCommissionType = 2 --Invoice Sub-Total Amount (Paid or Unpaid)
	BEGIN
		DELETE FROM 
			BizDocComission 
		WHERE 
			numDomainId=@numDomainID
			AND ISNULL(numOppBizDocID,0) = 0
			AND ISNULL(bitCommisionPaid,0)=0 
			AND numOppItemID IN (SELECT 
									OI.numoppitemtCode
								FROM 
									OpportunityMaster OM
								INNER JOIN
									OpportunityItems OI
								ON
									OM.numOppId = OI.numOppId
								WHERE
									OM.numDomainId = @numDomainID 
									AND OM.tintOppType = 1
									AND OM.tintOppStatus = 1
									AND CAST(DATEADD(MINUTE,@ClientTimeZoneOffset * -1,OM.bintCreatedDate) AS DATE) BETWEEN @dtPayStart AND @dtPayEnd)

		DELETE FROM 
			BizDocComission 
		WHERE 
			numDomainId=@numDomainID 
			AND ISNULL(bitCommisionPaid,0)=0 
			AND numOppBizDocItemID IN (SELECT 
											ISNULL(numOppBizDocItemID,0)
										FROM 
											OpportunityBizDocs OppBiz
										INNER JOIN
											OpportunityMaster OppM
										ON
											OppBiz.numOppID = OppM.numOppID
										INNER JOIN
											OpportunityBizDocItems OppBizItems
										ON
											OppBiz.numOppBizDocsId = OppBizItems.numOppBizDocID
										OUTER APPLY
										(
											SELECT 
												MAX(DM.dtDepositDate) dtDepositDate
											FROM 
												DepositMaster DM 
											JOIN 
												dbo.DepositeDetails DD
											ON 
												DM.numDepositId=DD.numDepositID 
											WHERE 
												DM.tintDepositePage=2 
												AND DM.numDomainId=@numDomainId
												AND DD.numOppID=OppBiz.numOppID 
												AND DD.numOppBizDocsID =OppBiz.numOppBizDocsID
										) TempDepositMaster
										WHERE
											OppM.numDomainId = @numDomainID 
											AND OppM.tintOppType = 1
											AND OppM.tintOppStatus = 1
											AND OppBiz.bitAuthoritativeBizDocs = 1
											AND ((TempDepositMaster.dtDepositDate IS NOT NULL AND (CAST(TempDepositMaster.dtDepositDate AS DATE) BETWEEN @dtPayStart AND @dtPayEnd)) OR CAST(DATEADD(MINUTE,@ClientTimeZoneOffset * -1,OppBiz.dtCreatedDate) AS DATE) BETWEEN @dtPayStart AND @dtPayEnd))

		INSERT INTO 
			@TABLEPAID
		SELECT 
			OM.numDivisionID,
			ISNULL(CompanyInfo.numCompanyType,0),
			ISNULL(CompanyInfo.vcProfile,0),
			OM.numAssignedTo,
			OM.numRecOwner,
			OppBizItems.numItemCode,
			Item.numItemClassification,
			OM.numOppID,
			OppMItems.numoppitemtCode,
			OppBizItems.numOppBizDocID,
			OppBizItems.numOppBizDocItemID,
			ISNULL(OppBizItems.numUnitHour,0),
			(CASE WHEN ISNULL(OM.fltExchangeRate,0)=0 OR ISNULL(OM.fltExchangeRate,0)=1 THEN ISNULL(OppBizItems.monTotAmount,0) ELSE ROUND(ISNULL(OppBizItems.monTotAmount,0) * OM.fltExchangeRate,2) END),
			(CASE WHEN ISNULL(OM.fltExchangeRate,0)=0 OR ISNULL(OM.fltExchangeRate,0)=1 THEN ISNULL(OppBizItems.monTotAmount,0) ELSE ROUND(ISNULL(OppBizItems.monTotAmount,0) * OM.fltExchangeRate,2) END),
			(CASE WHEN ISNULL(OM.fltExchangeRate,0)=0 OR ISNULL(OM.fltExchangeRate,0)=1 THEN ISNULL(OppMItems.monTotAmount,0) ELSE ROUND(ISNULL(OppMItems.monTotAmount,0) * OM.fltExchangeRate,2) END),
			(ISNULL(OppMItems.monVendorCost,0) * dbo.fn_UOMConversion(Item.numPurchaseUnit,Item.numItemCode,@numDomainID,Item.numBaseUnit) * ISNULL(OppBizItems.numUnitHour,0)),
			(ISNULL(OppMItems.monAvgCost,0) * ISNULL(OppBizItems.numUnitHour,0)),
			OM.numPartner,
			ISNULL(TEMPMatchedPO.numOppId,TEMPPO.numOppId),
			ISNULL(TEMPMatchedPO.numoppitemtCode,TEMPPO.numoppitemtCode),
			ISNULL(ISNULL(TEMPMatchedPO.monPrice,TEMPPO.monPrice),0)
		FROM 
			OpportunityMaster OM
		INNER JOIN	
			OpportunityBizDocs OBD
		ON
			OM.numOppId = OBD.numOppId
		INNER JOIN
			DivisionMaster 
		ON
			OM.numDivisionID = DivisionMaster.numDivisionID
		INNER JOIN
			CompanyInfo
		ON
			DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
		INNER JOIN
			OpportunityBizDocItems OppBizItems
		ON
			OBD.numOppBizDocsId = OppBizItems.numOppBizDocID
		INNER JOIN
			OpportunityItems OppMItems 
		ON
			OppBizItems.numOppItemID = OppMItems.numoppitemtCode
		INNER JOIN
			Item
		ON
			OppBizItems.numItemCode = Item.numItemCode
		OUTER APPLY
		(
			SELECT TOP 1
				OMInner.numOppID
				,OMInner.vcPOppName
				,OIInner.numoppitemtCode
				,OIInner.monPrice
			FROM
				SalesOrderLineItemsPOLinking SOLIPL
			INNER JOIN
				OpportunityMaster OMInner
			ON
				SOLIPL.numPurchaseOrderID = OMInner.numOppId
			INNER JOIN
				OpportunityItems OIInner
			ON
				OMInner.numOppId = OIInner.numOppId
				AND SOLIPL.numPurchaseOrderItemID = OIInner.numoppitemtCode
			WHERE
				SOLIPL.numSalesOrderID = OM.numOppId
				AND SOLIPL.numSalesOrderItemID = OppMItems.numoppitemtCode
		) TEMPMatchedPO
		OUTER APPLY
		(
			SELECT TOP 1
				OMInner.numOppID
				,OMInner.vcPOppName
				,OIInner.numoppitemtCode
				,OIInner.monPrice
			FROM
				OpportunityLinking OL
			INNER JOIN
				OpportunityMaster OMInner
			ON
				OL.numChildOppID = OMInner.numOppId
			INNER JOIN
				OpportunityItems OIInner
			ON
				OMInner.numOppId = OIInner.numOppId
			WHERE
				OL.numParentOppID = OM.numOppId
				AND OIInner.numItemCode = OppMItems.numItemCode
				AND ISNULL(OIInner.vcAttrValues,'') = ISNULL(OppMItems.vcAttrValues,'')
		) TEMPPO
		LEFT JOIN
			BizDocComission BDC1
		ON
			OM.numOppID = BDC1.numOppID
			AND OppMItems.numoppitemtCode = BDC1.numOppItemID
			AND ISNULL(bitCommisionPaid,0) = 1
		LEFT JOIN
			BizDocComission BDC2
		ON
			OM.numOppID = BDC2.numOppID
			AND OppMItems.numoppitemtCode = BDC2.numOppItemID
			AND ISNULL(BDC2.bitCommisionPaid,0) = 1
			AND OppBizItems.numOppBizDocID = BDC2.numOppBizDocId 
			AND OppBizItems.numOppBizDocItemID =  BDC2.numOppBizDocItemID
		WHERE
			OM.numDomainId = @numDomainID 
			AND OM.tintOppType = 1
			AND OM.tintOppStatus = 1
			AND OBD.bitAuthoritativeBizDocs = 1
			AND CAST(DATEADD(MINUTE,@ClientTimeZoneOffset * -1,OBD.dtCreatedDate) AS DATE) BETWEEN @dtPayStart AND @dtPayEnd
			AND OppBizItems.numItemCode NOT IN ( @numDiscountServiceItemID )  /*DO NOT INCLUDE DISCOUNT ITEM*/
			AND BDC1.numComissionID IS NULL
			AND BDC2.numComissionID IS NULL
			AND 1 = (
						CASE 
							WHEN OppBizItems.numItemCode = @numShippingServiceItemID 
							THEN 
								CASE 
									WHEN ISNULL(@bitIncludeTaxAndShippingInCommission,0) = 1
									THEN	
										1
									ELSE	
										0
								END
							ELSE 
								1 
						END
					)/*CONDITION TO INCLUDE/EXCLUDE INCLUDE SHIPPING ITEM */
	END
	ELSE
	BEGIN
		DELETE FROM 
			BizDocComission 
		WHERE 
			numDomainId=@numDomainID
			AND ISNULL(numOppBizDocID,0) = 0
			AND ISNULL(bitCommisionPaid,0)=0 
			AND numOppItemID IN (SELECT 
									OI.numoppitemtCode
								FROM 
									OpportunityMaster OM
								INNER JOIN
									OpportunityItems OI
								ON
									OM.numOppId = OI.numOppId
								WHERE
									OM.numDomainId = @numDomainID 
									AND OM.tintOppType = 1
									AND OM.tintOppStatus = 1
									AND CAST(DATEADD(MINUTE,@ClientTimeZoneOffset * -1,OM.bintCreatedDate) AS DATE) BETWEEN @dtPayStart AND @dtPayEnd)

		DELETE FROM 
			BizDocComission 
		WHERE 
			numDomainId=@numDomainID 
			AND ISNULL(bitCommisionPaid,0)=0 
			AND numOppBizDocItemID IN (SELECT 
											ISNULL(numOppBizDocItemID,0)
										FROM 
											OpportunityBizDocs OppBiz
										INNER JOIN
											OpportunityMaster OppM
										ON
											OppBiz.numOppID = OppM.numOppID
										INNER JOIN
											OpportunityBizDocItems OppBizItems
										ON
											OppBiz.numOppBizDocsId = OppBizItems.numOppBizDocID
										OUTER APPLY
										(
											SELECT 
												MAX(DM.dtDepositDate) dtDepositDate
											FROM 
												DepositMaster DM 
											JOIN 
												dbo.DepositeDetails DD
											ON 
												DM.numDepositId=DD.numDepositID 
											WHERE 
												DM.tintDepositePage=2 
												AND DM.numDomainId=@numDomainId
												AND DD.numOppID=OppBiz.numOppID 
												AND DD.numOppBizDocsID =OppBiz.numOppBizDocsID
										) TempDepositMaster
										WHERE
											OppM.numDomainId = @numDomainID 
											AND OppM.tintOppType = 1
											AND OppM.tintOppStatus = 1
											AND OppBiz.bitAuthoritativeBizDocs = 1
											AND ((TempDepositMaster.dtDepositDate IS NOT NULL AND (CAST(TempDepositMaster.dtDepositDate AS DATE) BETWEEN @dtPayStart AND @dtPayEnd)) OR CAST(DATEADD(MINUTE,@ClientTimeZoneOffset * -1,OppBiz.dtCreatedDate) AS DATE) BETWEEN @dtPayStart AND @dtPayEnd))

		INSERT INTO 
			@TABLEPAID
		SELECT 
			OppM.numDivisionID,
			ISNULL(CompanyInfo.numCompanyType,0),
			ISNULL(CompanyInfo.vcProfile,0),
			OppM.numAssignedTo,
			OppM.numRecOwner,
			OppBizItems.numItemCode,
			Item.numItemClassification,
			OppM.numOppID,
			OppMItems.numoppitemtCode,
			OppBizItems.numOppBizDocID,
			OppBizItems.numOppBizDocItemID,
			ISNULL(OppBizItems.numUnitHour,0),
			(CASE WHEN ISNULL(OppM.fltExchangeRate,0)=0 OR ISNULL(OppM.fltExchangeRate,0)=1 THEN ISNULL(OppBizItems.monTotAmount,0) ELSE ROUND(ISNULL(OppBizItems.monTotAmount,0) * OppM.fltExchangeRate,2) END),
			(CASE WHEN ISNULL(OppM.fltExchangeRate,0)=0 OR ISNULL(OppM.fltExchangeRate,0)=1 THEN ISNULL(OppBizItems.monTotAmount,0) ELSE ROUND(ISNULL(OppBizItems.monTotAmount,0) * OppM.fltExchangeRate,2) END),
			(CASE WHEN ISNULL(OppM.fltExchangeRate,0)=0 OR ISNULL(OppM.fltExchangeRate,0)=1 THEN ISNULL(OppMItems.monTotAmount,0) ELSE ROUND(ISNULL(OppMItems.monTotAmount,0) * OppM.fltExchangeRate,2) END),
			(ISNULL(OppMItems.monVendorCost,0) * dbo.fn_UOMConversion(Item.numPurchaseUnit,Item.numItemCode,@numDomainID,Item.numBaseUnit) * ISNULL(OppBizItems.numUnitHour,0)),
			(ISNULL(OppMItems.monAvgCost,0) * ISNULL(OppBizItems.numUnitHour,0)),
			OppM.numPartner,
			ISNULL(TEMPMatchedPO.numOppId,TEMPPO.numOppId),
			ISNULL(TEMPMatchedPO.numoppitemtCode,TEMPPO.numoppitemtCode),
			ISNULL(ISNULL(TEMPMatchedPO.monPrice,TEMPPO.monPrice),0)
		FROM 
			OpportunityBizDocs OppBiz
		INNER JOIN
			OpportunityMaster OppM
		ON
			OppBiz.numOppID = OppM.numOppID
		INNER JOIN
			DivisionMaster 
		ON
			OppM.numDivisionID = DivisionMaster.numDivisionID
		INNER JOIN
			CompanyInfo
		ON
			DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
		INNER JOIN
			OpportunityBizDocItems OppBizItems
		ON
			OppBiz.numOppBizDocsId = OppBizItems.numOppBizDocID
		INNER JOIN
			OpportunityItems OppMItems 
		ON
			OppBizItems.numOppItemID = OppMItems.numoppitemtCode
		INNER JOIN
			Item
		ON
			OppBizItems.numItemCode = Item.numItemCode
		OUTER APPLY
		(
			SELECT TOP 1
				OMInner.numOppID
				,OMInner.vcPOppName
				,OIInner.numoppitemtCode
				,OIInner.monPrice
			FROM
				SalesOrderLineItemsPOLinking SOLIPL
			INNER JOIN
				OpportunityMaster OMInner
			ON
				SOLIPL.numPurchaseOrderID = OMInner.numOppId
			INNER JOIN
				OpportunityItems OIInner
			ON
				OMInner.numOppId = OIInner.numOppId
				AND SOLIPL.numPurchaseOrderItemID = OIInner.numoppitemtCode
			WHERE
				SOLIPL.numSalesOrderID = OppM.numOppId
				AND SOLIPL.numSalesOrderItemID = OppMItems.numoppitemtCode
		) TEMPMatchedPO
		OUTER APPLY
		(
			SELECT TOP 1
				OMInner.numOppID
				,OMInner.vcPOppName
				,OIInner.numoppitemtCode
				,OIInner.monPrice
			FROM
				OpportunityLinking OL
			INNER JOIN
				OpportunityMaster OMInner
			ON
				OL.numChildOppID = OMInner.numOppId
			INNER JOIN
				OpportunityItems OIInner
			ON
				OMInner.numOppId = OIInner.numOppId
			WHERE
				OL.numParentOppID = OppM.numOppId
				AND OIInner.numItemCode = OppMItems.numItemCode
				AND ISNULL(OIInner.vcAttrValues,'') = ISNULL(OppMItems.vcAttrValues,'')
		) TEMPPO
		LEFT JOIN
			BizDocComission BDC1
		ON
			OppM.numOppID = BDC1.numOppID
			AND OppMItems.numoppitemtCode = BDC1.numOppItemID
			AND ISNULL(bitCommisionPaid,0) = 1
		LEFT JOIN
			BizDocComission BDC2
		ON
			OppM.numOppID = BDC2.numOppID
			AND OppMItems.numoppitemtCode = BDC2.numOppItemID
			AND ISNULL(BDC2.bitCommisionPaid,0) = 1
			AND OppBizItems.numOppBizDocID = BDC2.numOppBizDocId 
			AND OppBizItems.numOppBizDocItemID =  BDC2.numOppBizDocItemID
		OUTER APPLY
		(
			SELECT 
				MAX(DM.dtDepositDate) dtDepositDate
			FROM 
				DepositMaster DM 
			JOIN 
				dbo.DepositeDetails DD
			ON 
				DM.numDepositId=DD.numDepositID 
			WHERE 
				DM.tintDepositePage=2 
				AND DM.numDomainId=@numDomainId
				AND DD.numOppID=OppBiz.numOppID 
				AND DD.numOppBizDocsID =OppBiz.numOppBizDocsID
		) TempDepositMaster
		WHERE
			OppM.numDomainId = @numDomainID 
			AND OppM.tintOppType = 1
			AND OppM.tintOppStatus = 1
			AND OppBiz.bitAuthoritativeBizDocs = 1
			AND BDC1.numComissionID IS NULL
			AND BDC2.numComissionID IS NULL
			AND ISNULL(OppBiz.monAmountPaid,0) >= OppBiz.monDealAmount
			AND (TempDepositMaster.dtDepositDate IS NOT NULL AND (CAST(TempDepositMaster.dtDepositDate AS DATE) BETWEEN @dtPayStart AND @dtPayEnd))
			AND OppBizItems.numItemCode NOT IN ( @numDiscountServiceItemID )  /*DO NOT INCLUDE DISCOUNT ITEM*/
			AND 1 = (
						CASE 
							WHEN OppBizItems.numItemCode = @numShippingServiceItemID 
							THEN 
								CASE 
									WHEN ISNULL(@bitIncludeTaxAndShippingInCommission,0) = 1
									THEN	
										1
									ELSE	
										0
								END
							ELSE 
								1 
						END
					)/*CONDITION TO INCLUDE/EXCLUDE INCLUDE SHIPPING ITEM */
	END	

	--GET COMMISSION RULE OF DOMAIN
	DECLARE @TempCommissionRule TABLE
	(
		ID INT IDENTITY(1,1),
		numComRuleID NUMERIC(18,0),
		tintComBasedOn TINYINT,
		tintComType TINYINT,
		tintComAppliesTo TINYINT,
		tintComOrgType TINYINT,
		tintAssignTo TINYINT
	)

	INSERT INTO
		@TempCommissionRule
	SELECT
		CR.numComRuleID,
		CR.tintComBasedOn,
		CR.tintComType,
		CR.tintComAppliesTo,
		CR.tintComOrgType,
		CR.tintAssignTo
	FROM
		CommissionRules CR
	LEFT JOIN
		PriceBookPriorities PBP
	ON	
		CR.tintComAppliesTo = PBP.Step2Value 
		AND CR.tintComOrgType = PBP.Step3Value
	WHERE
		CR.numDomainID = @numDomainID
		AND ISNULL(PBP.[Priority],0) > 0
	ORDER BY
		ISNULL(PBP.[Priority],0) ASC

	-- LOOP ALL COMMISSION RULES
	DECLARE @i INT = 1
	DECLARE @COUNT INT = 0

	SELECT @COUNT=COUNT(*) FROM @TempCommissionRule

	DECLARE @TEMPITEM TABLE
	(
		ID INT,
		UniqueID INT,
		numUserCntID NUMERIC(18,0),
		numOppID NUMERIC(18,0),
		numOppBizDocID NUMERIC(18,0),
		numOppBizDocItemID NUMERIC(18,0),
		numItemCode NUMERIC(18,0),
		numUnitHour FLOAT,
		monTotAmount DECIMAL(20,5),
		monVendorCost DECIMAL(20,5),
		monAvgCost DECIMAL(20,5),
		numOppItemID NUMERIC(18,0),
		monInvoiceSubTotal DECIMAL(20,5),
		monOrderSubTotal DECIMAL(20,5),
		numPOID NUMERIC(18,0),
		numPOItemID NUMERIC(18,0),
		monPOCost DECIMAL(20,5)
	)

	WHILE @i <= @COUNT
	BEGIN
		DECLARE @numComRuleID NUMERIC(18,0)
		DECLARE @tintComBasedOn TINYINT
		DECLARE @tintComAppliesTo TINYINT
		DECLARE @tintComOrgType TINYINT
		DECLARE @tintComType TINYINT
		DECLARE @tintAssignTo TINYINT
		DECLARE @numTotalAmount AS DECIMAL(20,5)
		DECLARE @numTotalUnit AS FLOAT

		--CLEAR PREVIOUS VALUES FROM TEMP TABLE
		DELETE FROM @TEMPITEM

		--FETCH COMMISSION RULE
		SELECT @numComRuleID=numComRuleID,@tintComBasedOn=tintComBasedOn,@tintComType=tintComType,@tintComAppliesTo=tintComAppliesTo,@tintComOrgType=tintComOrgType,@tintAssignTo=tintAssignTo FROM @TempCommissionRule WHERE ID=@i

		--FIND ALL BIZ DOC ITEMS WHERE COMMISSION IS NOT PAID AND CURRENT RULE ITEMS, ORGANIZATION AND ASSINGED TO FILTER CAN BE APPLIED
		INSERT INTO
			@TEMPITEM
		SELECT
			ROW_NUMBER() OVER (ORDER BY ID),
			ID,
			(CASE @tintAssignTo WHEN 1 THEN numAssignedTo WHEN 2 THEN numRecordOwner ELSE numPartner END),
			numOppID,
			numOppBizDocID,
			numOppBizDocItemID,
			numItemCode,
			numUnitHour,
			monTotAmount,
			monVendorCost,
			monAvgCost,
			numOppItemID,
			monInvoiceSubTotal,
			monOrderSubTotal,
			numPOID,
			numPOItemID,
			monPOCost
		FROM
			@TABLEPAID
		WHERE
			1 = (CASE @tintComAppliesTo
					--SPECIFIC ITEMS
					WHEN 1 THEN CASE WHEN (SELECT COUNT(*) FROM CommissionRuleItems WHERE numComRuleID = @numComRuleID AND ISNULL(numValue,0) = numItemCode) > 0 THEN 1 ELSE 0 END
					-- ITEM WITH SPECIFIC CLASSIFICATIONS
					WHEN 2 THEN CASE WHEN (SELECT COUNT(*) FROM CommissionRuleItems WHERE numComRuleID = @numComRuleID AND ISNULL(numValue,0) = numItemClassification) > 0 THEN 1 ELSE 0 END
					-- ALL ITEMS
					ELSE 1
				END)
			AND 1 = (CASE @tintComOrgType
						-- SPECIFIC ORGANIZATION
						WHEN 1 THEN CASE WHEN (SELECT COUNT(*) FROM CommissionRuleOrganization WHERE numComRuleID = @numComRuleID AND ISNULL(numValue,0) = numDivisionID) > 0 THEN 1 ELSE 0 END
						-- ORGANIZATION WITH SPECIFIC RELATIONSHIP AND PROFILE
						WHEN 2 THEN CASE WHEN (SELECT COUNT(*) FROM CommissionRuleOrganization WHERE numComRuleID = @numComRuleID AND ISNULL(numValue,0) = numRelationship AND ISNULL(numProfile,0) = numProfile) > 0 THEN 1 ELSE 0 END
						-- ALL ORGANIZATIONS
						ELSE 1
					END)
			AND 1 = (CASE @tintAssignTo 
						-- ORDER ASSIGNED TO
						WHEN 1 THEN CASE WHEN (SELECT COUNT(*) FROM CommissionRuleContacts WHERE numComRuleID = @numComRuleID AND numValue=numAssignedTo) > 0 THEN 1 ELSE 0 END
						-- ORDER OWNER
						WHEN 2 THEN CASE WHEN (SELECT COUNT(*) FROM CommissionRuleContacts WHERE numComRuleID = @numComRuleID AND numValue=numRecordOwner) > 0 THEN 1 ELSE 0 END
						-- Order Partner
						WHEN 3 THEN CASE WHEN (SELECT COUNT(*) FROM CommissionRuleContacts WHERE numComRuleID = @numComRuleID AND numValue=numPartner) > 0 THEN 1 ELSE 0 END
						-- NO OPTION SELECTED IN RULE
						ELSE 0
					END)


		--CALCULATE COMMISSION AND STORE IN TEMPARORY TABLE
		INSERT INTO @TempBizCommission 
		(
			numUserCntID,
			numOppID,
			numOppBizDocID,
			numOppBizDocItemID,
			monCommission,
			numComRuleID,
			tintComType,
			tintComBasedOn,
			decCommission,
			tintAssignTo,
			numOppItemID,
			monInvoiceSubTotal,
			monOrderSubTotal
		)
		SELECT
			numUserCntID,
			numOppID,
			numOppBizDocID,
			numOppBizDocItemID,
			CASE @tintComType 
				WHEN 1 --PERCENT
					THEN 
						CASE 
							WHEN ISNULL(@bitCommissionBasedOn,0) = 1
							THEN
								(CASE 
									WHEN numPOItemID IS NOT NULL
									THEN (ISNULL(monTotAmount,0) - ISNULL(monPOCost,0)) * (T2.decCommission / CAST(100 AS FLOAT))
									ELSE 
										CASE @tintCommissionBasedOn
											--ITEM GROSS PROFIT (VENDOR COST)
											WHEN 1 THEN (ISNULL(monTotAmount,0) - ISNULL(monVendorCost,0)) * (T2.decCommission / CAST(100 AS FLOAT))
											--ITEM GROSS PROFIT (AVERAGE COST)
											WHEN 2 THEN(ISNULL(monTotAmount,0) - ISNULL(monAvgCost,0)) * (T2.decCommission / CAST(100 AS FLOAT))
										END
								END)
							ELSE
								monTotAmount * (T2.decCommission / CAST(100 AS FLOAT))
						END
				ELSE  --FLAT
					T2.decCommission
			END,
			@numComRuleID,
			@tintComType,
			@tintComBasedOn,
			T2.decCommission,
			@tintAssignTo,
			numOppItemID,
			monInvoiceSubTotal,
			monOrderSubTotal
		FROM 
			@TEMPITEM AS T1
		CROSS APPLY
		(
			SELECT TOP 1 
				ISNULL(decCommission,0) AS decCommission
			FROM 
				CommissionRuleDtl 
			WHERE 
				numComRuleID=@numComRuleID 
				AND ISNULL(decCommission,0) > 0
				AND 1 = (CASE 
						WHEN @tintComBasedOn = 1 --BASED ON AMOUNT SOLD 
						THEN (CASE WHEN (T1.monTotAmount BETWEEN intFrom AND intTo) THEN 1 ELSE 0 END)
						WHEN @tintComBasedOn = 2 --BASED ON UNITS SOLD
						THEN (CASE WHEN (T1.numUnitHour BETWEEN intFrom AND intTo) THEN 1 ELSE 0 END)
						ELSE 0
						END)
		) AS T2
		WHERE
			(CASE 
				WHEN ISNULL(@bitCommissionBasedOn,0) = 1
				THEN
					(CASE 
						WHEN numPOItemID IS NOT NULL
						THEN (ISNULL(monTotAmount,0) - ISNULL(monPOCost,0)) * (T2.decCommission / CAST(100 AS FLOAT))
						ELSE
							CASE @tintCommissionBasedOn
								--ITEM GROSS PROFIT (VENDOR COST)
								WHEN 1 THEN (ISNULL(monTotAmount,0) - ISNULL(monVendorCost,0)) * (T2.decCommission / CAST(100 AS FLOAT))
								--ITEM GROSS PROFIT (AVERAGE COST)
								WHEN 2 THEN(ISNULL(monTotAmount,0) - ISNULL(monAvgCost,0)) * (T2.decCommission / CAST(100 AS FLOAT))
							END
					END)
				ELSE
					monTotAmount * (T2.decCommission / CAST(100 AS FLOAT))
			END) > 0 
			AND (
					SELECT 
						COUNT(*) 
					FROM 
						@TempBizCommission TempBDC
					WHERE 
						TempBDC.numUserCntID=T1.numUserCntID 
						AND TempBDC.numOppID=T1.numOppID 
						AND TempBDC.numOppBizDocID = T1.numOppBizDocID 
						AND TempBDC.numOppBizDocItemID = T1.numOppBizDocItemID
						AND TempBDC.numOppItemID = T1.numOppItemID 
						AND tintAssignTo = @tintAssignTo
				) = 0

		SET @i = @i + 1

		SET @numComRuleID = 0
		SET @tintComBasedOn = 0
		SET @tintComAppliesTo = 0
		SET @tintComOrgType = 0
		SET @tintComType = 0
		SET @tintAssignTo = 0
		SET @numTotalAmount = 0
		SET @numTotalUnit = 0
	END

	--INSERT CALCULATED ENTERIES IN BIZDOCCOMMISSION TABLE
	INSERT INTO BizDocComission
	(	
		numDomainID,
		numUserCntID,
		numOppBizDocId,
		numComissionAmount,
		numOppBizDocItemID,
		numComRuleID,
		tintComType,
		tintComBasedOn,
		decCommission,
		numOppID,
		bitFullPaidBiz,
		numComPayPeriodID,
		tintAssignTo,
		numOppItemID,
		monInvoiceSubTotal,
		monOrderSubTotal,
		bitDomainCommissionBasedOn,
		tintDomainCommissionBasedOn
	)
	SELECT
		@numDomainID,
		numUserCntID,
		numOppBizDocID,
		monCommission,
		numOppBizDocItemID,
		numComRuleID,
		tintComType,
		tintComBasedOn,
		decCommission,
		numOppID,
		1,
		@numComPayPeriodID,
		tintAssignTo,
		numOppItemID,
		monInvoiceSubTotal,
		monOrderSubTotal,
		@bitCommissionBasedOn,
		@tintCommissionBasedOn
	FROM
		@TempBizCommission
END
GO