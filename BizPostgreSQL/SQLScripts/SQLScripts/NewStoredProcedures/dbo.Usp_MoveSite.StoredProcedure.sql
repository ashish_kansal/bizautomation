SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_MoveSite')
DROP PROCEDURE USP_MoveSite
GO
CREATE PROCEDURE [dbo].[USP_MoveSite]
    @numSiteID NUMERIC(9) ,
    @numFromDomainID NUMERIC(9) ,
    @numToDomainID NUMERIC(9)
AS
    BEGIN

        BEGIN TRY 
            BEGIN TRAN

    
            UPDATE  Category
            SET     numDomainID = @numToDomainID
            WHERE   numDomainID = @numFromDomainID
            UPDATE  RedirectConfig
            SET     numDomainID = @numToDomainID
            WHERE   [numSiteID] = @numSiteID
                    AND numDomainID = @numFromDomainID
            UPDATE  [SiteBreadCrumb]
            SET     numDomainID = @numToDomainID
            WHERE   [numSiteID] = @numSiteID
                    AND numDomainID = @numFromDomainID
            UPDATE  [StyleSheets]
            SET     numDomainID = @numToDomainID
            WHERE   [numSiteID] = @numSiteID
                    AND numDomainID = @numFromDomainID
 --update [StyleSheetDetails] set numDomainID
            UPDATE  [SiteTemplates]
            SET     numDomainID = @numToDomainID
            WHERE   [numSiteID] = @numSiteID
                    AND numDomainID = @numFromDomainID
            UPDATE  [PageElementDetail]
            SET     numDomainID = @numToDomainID
            WHERE   [numSiteID] = @numSiteID
                    AND numDomainID = @numFromDomainID
            UPDATE  [SiteMenu]
            SET     numDomainID = @numToDomainID
            WHERE   [numSiteID] = @numSiteID
                    AND numDomainID = @numFromDomainID
            UPDATE  [SitePages]
            SET     numDomainID = @numToDomainID
            WHERE   [numSiteID] = @numSiteID
                    AND numDomainID = @numFromDomainID
 
 --update [SiteCategories] set numDomainID= @numToDomainID    WHERE       [numSiteID] = @numSiteID and numDomainID=@numFromDomainID
 
            UPDATE  Sites
            SET     numDomainID = @numToDomainID
            WHERE   [numSiteID] = @numSiteID
                    AND numDomainID = @numFromDomainID
 
 --update Ratings set numDomainID= @numToDomainID    WHERE       [numSiteID] = @numSiteID and numDomainID=@numFromDomainID
 --update Review set numDomainID= @numToDomainID    WHERE       [numSiteID] = @numSiteID and numDomainID=@numFromDomainID
 
  DELETE FROM [dbo].[eCommerceDTL] WHERE [eCommerceDTL].[numDomainId] = @numFromDomainID
  AND [eCommerceDTL].[numSiteId] = @numSiteID
 
            COMMIT TRAN
        END TRY 
        BEGIN CATCH  
            IF ( @@TRANCOUNT > 0 )
                BEGIN
                    ROLLBACK TRAN
                    DECLARE @error VARCHAR(1000)
                    SET @error = ERROR_MESSAGE();
                    RAISERROR (@error, 16, 1 );
                    RETURN 1
                END
        END CATCH 
    
                     
    END