/****** Object:  StoredProcedure [dbo].[USP_GetDepositDetails]    Script Date: 07/26/2008 16:17:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created By Siva    
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_GetDepositDetailsWithClass' ) 
    DROP PROCEDURE USP_GetDepositDetailsWithClass
GO
CREATE PROCEDURE [dbo].[USP_GetDepositDetailsWithClass]
    @numDepositId AS NUMERIC(9) = 0 ,
    @numDomainId AS NUMERIC(9) = 0 ,
    @tintMode AS TINYINT = 0, -- 1= Get Undeposited Payments 
    @numCurrencyID numeric(9) =0,
    @CurrentPage INT,
    @PageSize INT,
    @TotRecs INT OUTPUT,
    @numAccountClass NUMERIC(18,0)=0,
	@SortColumn AS VARCHAR(50),
    @columnSortOrder AS VARCHAR(10),
	@strCondition AS VARCHAR(MAX)=''
AS 
    BEGIN  
      				
	    DECLARE @numBaseCurrencyID NUMERIC
	    DECLARE @BaseCurrencySymbol nVARCHAR(3)
	    
        SELECT @numBaseCurrencyID = ISNULL(numCurrencyID,0) FROM dbo.Domain WHERE numDomainId=@numDomainId
        SELECT @BaseCurrencySymbol = ISNULL(varCurrSymbol,'') FROM dbo.Currency WHERE numCurrencyID = @numBaseCurrencyID
      
		DECLARE @firstRec AS INTEGER
		DECLARE @lastRec AS INTEGER
		SET @firstRec = ( @CurrentPage - 1 ) * @PageSize
		SET @lastRec = ( @CurrentPage * @PageSize + 1 )

		IF LEN(@SortColumn) = 0 AND LEN(@columnSortOrder) = 0
		BEGIN
			SET @SortColumn = 'DepositDate'
			SET @columnSortOrder = 'ASC'
		END

		IF LEN(@strCondition) = 0
		BEGIN
			SET @strCondition = '1=1'
		END

DECLARE @strSQLTintMode0 AS NVARCHAR(MAX)
DECLARE @strSQLTintMode1 AS NVARCHAR(MAX)
DECLARE @strFinal AS NVARCHAR(MAX)
			  
 SET @strSQLTintMode0 = '    SELECT 
						numDepositId ,
                        dbo.fn_GetComapnyName(numDivisionID) vcCompanyName ,
                        numDivisionID ,
                        numChartAcntId ,
                        dbo.FormatedDateFromDate(dtDepositDate, DM.numDomainId) dtDepositDate ,
                        dtDepositDate AS [dtOrigDepositDate],
                        monDepositAmount ,
                        numPaymentMethod ,
                        vcReference ,
                        vcMemo ,
                        tintDepositeToType,tintDepositePage,bitDepositedToAcnt
                        ,DM.numCurrencyID
						,ISNULL(C.varCurrSymbol,'''') varCurrSymbol
						,ISNULL(NULLIF(DM.fltExchangeRate,0),1) fltExchangeRateReceivedPayment
						,'''+CONVERT(VARCHAR(10),@BaseCurrencySymbol)+''' BaseCurrencySymbol
						,ISNULL(monRefundAmount,0) AS monRefundAmount
						,ISNULL(DM.numAccountClass,0) AS numAccountClass
                FROM    dbo.DepositMaster DM
						LEFT JOIN dbo.Currency C ON C.numCurrencyID = DM.numCurrencyID
                WHERE   numDepositId = '+ CONVERT(VARCHAR(10),@numDepositId) +'
                        AND DM.numDomainId = '+ CONVERT(VARCHAR(10),@numDomainId) +'
        
                SELECT  DD.numDepositeDetailID ,
                        DD.numDepositID ,
                        DD.numOppBizDocsID ,
                        DD.numOppID ,
                        DD.monAmountPaid ,
                        DD.numChildDepositID ,
                        DD.numPaymentMethod ,
                        DD.vcMemo ,
                        DD.vcReference ,
                        DD.numClassID ,
                        DD.numProjectID ,
                        DD.numReceivedFrom ,
                        DD.numAccountID,
                        GJD.numTransactionId AS numTransactionID,
                        GJD1.numTransactionId AS numTransactionIDHeader
                        ,ISNULL(DM.numCurrencyID,0) numCurrencyID
						,ISNULL(NULLIF(DM.fltExchangeRate,0),1) fltExchangeRateReceivedPayment
						
                FROM    dbo.DepositeDetails DD
                LEFT JOIN dbo.DepositMaster DM ON DM.numDepositId = DD.numChildDepositID
                LEFT JOIN dbo.General_Journal_Details GJD ON GJD.numReferenceID = DD.numDepositeDetailID AND GJD.tintReferenceType = 7 
                LEFT JOIN dbo.General_Journal_Details GJD1 ON GJD1.numReferenceID = DD.numDepositID AND GJD1.tintReferenceType = 6 
                WHERE   DD.numDepositID = '+CONVERT(VARCHAR(10),@numDepositId) +'  '
				
				     
SET @strSQLTintMode1 = ' (
								--Get Saved deposit entries for edit mode
							SELECT  --Row_number() OVER ( ORDER BY dtDepositDate ASC ) AS row,												
					
									DD.numDepositeDetailID ,
									DM.numDepositId ,
									DM.numChartAcntId ,
									dbo.FormatedDateFromDate(dtDepositDate, DM.numDomainId) dtDepositDate ,
									DD.monAmountPaid AS monDepositAmount,
									DD.numPaymentMethod ,
									DD.vcReference ,
									DD.vcMemo ,
									DD.numReceivedFrom AS numDivisionID,
									dbo.fn_GetComapnyName(DM.numDivisionID) ReceivedFrom,
									bitDepositedToAcnt,
										(SELECT 
									CASE tintSourceType 
									WHEN 1 THEN dbo.GetListIemName(tintSource) 
									WHEN 2 THEN (SELECT TOP 1 vcSiteName FROM dbo.Sites WHERE numSiteID = tintSource) 
									WHEN 3 THEN (SELECT TOP 1 vcProviderName FROM dbo.WebAPI WHERE WebApiId = tintSource)  
									ELSE '''' END 
									FROM dbo.OpportunityMaster WHERE numOppId IN ( SELECT TOP 1 numOppID FROM dbo.DepositeDetails WHERE numDepositID = DM.numDepositId AND numOppID>0) ) AS vcSource,
									CASE WHEN DM.numTransHistoryID >0 THEN dbo.GetListIemName(ISNULL(TH.numCardType,0)) ELSE '''' END  AS vcCardType
									,DM.numCurrencyID
									,ISNULL(C.varCurrSymbol,'''') varCurrSymbol
									,ISNULL(NULLIF(DM.fltExchangeRate,0),1) fltExchangeRateReceivedPayment
									,'''+CONVERT(VARCHAR(10),@BaseCurrencySymbol) +''' BaseCurrencySymbol
									,dtDepositDate AS DepositDate
							FROM    dbo.DepositeDetails DD
									INNER JOIN dbo.DepositMaster DM ON DD.numChildDepositID = DM.numDepositId
									LEFT JOIN dbo.TransactionHistory TH ON TH.numTransHistoryID= DM.numTransHistoryID
									LEFT JOIN dbo.Currency C ON C.numCurrencyID = DM.numCurrencyID
							WHERE   DD.numDepositID = '+CONVERT(VARCHAR(10),@numDepositId) +' AND (ISNULL(DM.numAccountClass,0)='+CONVERT(VARCHAR(10),@numAccountClass) +' OR ISNULL('+CONVERT(VARCHAR(10),@numAccountClass) +',0)=0)
							UNION
							SELECT --Row_number() OVER ( ORDER BY DM.dtDepositDate DESC ) AS row,
									0 numDepositeDetailID ,
									numDepositId ,
									numChartAcntId ,
									dbo.FormatedDateFromDate(dtDepositDate, DM.numDomainId) dtDepositDate ,
									monDepositAmount ,
									numPaymentMethod ,
									vcReference ,
									vcMemo ,
									DM.numDivisionID,
									dbo.fn_GetComapnyName(DM.numDivisionID) ReceivedFrom,
									bitDepositedToAcnt,
									(SELECT 
									CASE tintSourceType 
									WHEN 1 THEN dbo.GetListIemName(tintSource) 
									WHEN 2 THEN (SELECT TOP 1 vcSiteName FROM dbo.Sites WHERE numSiteID = tintSource) 
									WHEN 3 THEN (SELECT TOP 1 vcProviderName FROM dbo.WebAPI WHERE WebApiId = tintSource)  
									ELSE '''' END 
									FROM dbo.OpportunityMaster WHERE numOppId IN ( SELECT TOP 1 numOppID FROM dbo.DepositeDetails WHERE numDepositID = DM.numDepositId AND numOppID>0) ) AS vcSource,
									CASE WHEN DM.numTransHistoryID >0 THEN dbo.GetListIemName(ISNULL(TH.numCardType,0)) ELSE '''' END  AS vcCardType
									,DM.numCurrencyID
									,ISNULL(C.varCurrSymbol,'''') varCurrSymbol
									,ISNULL(NULLIF(DM.fltExchangeRate,0),1) fltExchangeRateReceivedPayment
									,'''+CONVERT(VARCHAR(10),@BaseCurrencySymbol) +''' BaseCurrencySymbol
									,dtDepositDate AS DepositDate
							FROM    dbo.DepositMaster DM
									INNER JOIN dbo.Chart_Of_Accounts COA ON DM.numChartAcntId = COA.numAccountId
									LEFT JOIN dbo.TransactionHistory TH ON DM.numTransHistoryID = TH.numTransHistoryID
									LEFT JOIN dbo.Currency C ON C.numCurrencyID = DM.numCurrencyID
							WHERE   DM.numDomainId = '+CONVERT(VARCHAR(10),@numDomainID) +'
									AND tintDepositeToType = 2
									AND ISNULL(bitDepositedToAcnt,0) = 0 
									AND (ISNULL(DM.numCurrencyID,'+ CONVERT(VARCHAR(10),@numBaseCurrencyID) +') = '+ CONVERT(VARCHAR(10),@numCurrencyID) +' OR '+CONVERT(VARCHAR(10),@numCurrencyID) +' =0)
									AND (ISNULL(DM.numAccountClass,0)='+ CONVERT(VARCHAR(10),@numAccountClass) +'  OR ISNULL('+CONVERT(VARCHAR(10),@numAccountClass) +',0)=0)
						)  [Deposits]  WHERE '+ convert(varchar(MAX),@strCondition) 



		SET @strFinal = CONCAT('IF '+ CONVERT(VARCHAR(10),@tintMode) +' = 0 BEGIN ', @strSQLTintMode0, 'END IF '+ CONVERT(VARCHAR(10),@tintMode) +' = 1 BEGIN ','SELECT Row_number() OVER ( ORDER BY ',@SortColumn,' ',@columnSortOrder,') AS row,* INTO #tempDeposits FROM ', @strSQLTintMode1,';  SELECT @TotalRecords = COUNT(*) FROM #tempDeposits; SELECT * FROM #tempDeposits  WHERE row > ',@firstRec,' and row <',@lastRec,'; DROP TABLE #tempDeposits ;        
					
			-- get new deposite Entries
					SELECT  DD.numDepositeDetailID ,
							DM.numDepositId ,
							dbo.FormatedDateFromDate(dtDepositDate, DM.numDomainId) dtDepositDate ,
							DD.monAmountPaid ,
							DD.numPaymentMethod ,
							DD.vcReference ,
							DD.vcMemo ,
							DD.numAccountID,
--							 COA.vcAccountCode numAcntType,
							DD.numClassID,
							DD.numProjectID,
							DD.numReceivedFrom,
							dbo.fn_GetComapnyName(DD.numReceivedFrom) ReceivedFrom,
							bitDepositedToAcnt
					FROM    dbo.DepositeDetails DD
							INNER JOIN dbo.DepositMaster DM ON DD.numDepositID = DM.numDepositId
--							LEFT JOIN dbo.Chart_Of_Accounts COA ON DD.numAccountID = COA.numAccountId
					WHERE   DD.numDepositID = '+ CONVERT(VARCHAR(10),@numDepositId) +' AND ISNULL(numChildDepositID,0) = 0
					AND (ISNULL(DM.numAccountClass,0)='+ CONVERT(VARCHAR(10),@numAccountClass) +'  OR ISNULL('+ CONVERT(VARCHAR(10),@numAccountClass) +',0)=0)

					 END')  


PRINT @strFinal

print  @TotRecs
exec sp_executesql @strFinal, N'@TotalRecords INT OUT', @TotRecs OUT
						
                END
    
GO
