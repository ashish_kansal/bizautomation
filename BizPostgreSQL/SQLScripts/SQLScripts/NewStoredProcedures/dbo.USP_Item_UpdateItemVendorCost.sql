GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_Item_UpdateItemVendorCost')
DROP PROCEDURE USP_Item_UpdateItemVendorCost
GO
CREATE PROCEDURE [dbo].[USP_Item_UpdateItemVendorCost]  
	@numDomainID NUMERIC(18,0),
	@numUserCntID NUMERIC(18,0),
	@vcSelectedRecords VARCHAR(MAX)
AS  
BEGIN
	DECLARE @hDocItem INT

	DECLARE @TEMP TABLE
	(
		numVendorID NUMERIC(18,0)
		,numItemCode NUMERIC(18,0)
		,monUnitCost DECIMAL(20,5)
	)

	EXEC sp_xml_preparedocument @hDocItem OUTPUT, @vcSelectedRecords

	INSERT INTO @TEMP
	(
		numVendorID
		,numItemCode
		,monUnitCost
	)
	SELECT
		ISNULL(VendorID,0)
		,ISNULL(ItemCode,0)
		,ISNULL(UnitCost,0)
	FROM
		OPENXML (@hDocItem,'/NewDataSet/Table1',2)
	WITH
	(
		VendorID NUMERIC(18,0)
		,ItemCode NUMERIC(18,0)
		,UnitCost DECIMAL(20,5)
	)

	EXEC sp_xml_removedocument @hDocItem

	UPDATE
		V
	SET
		V.monCost = T.monUnitCost
	FROM
		@TEMP T
	INNER JOIN
		Vendor V
	ON
		V.numDomainID = @numDomainID
		AND T.numVendorID = V.numVendorID
		AND T.numItemCode = V.numItemCode
END
GO