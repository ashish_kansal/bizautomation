
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ManageSOWorkOrder')
DROP PROCEDURE USP_ManageSOWorkOrder
GO
CREATE PROCEDURE [dbo].[USP_ManageSOWorkOrder]
@numDomainID AS numeric(18,0)=0,
@numOppID AS numeric(18,0)=0,
@numUserCntID AS numeric(9)=0
AS
BEGIN
	DECLARE @numItemCode AS NUMERIC(18,0)
	DECLARE @numUnitHour AS FLOAT
	DECLARE @numWarehouseItmsID AS NUMERIC(18,0)
	DECLARE @vcItemDesc AS VARCHAR(1000)
  
	DECLARE @numWOId AS NUMERIC(18,0)
	DECLARE @vcInstruction AS VARCHAR(1000)
	DECLARE @dtPlannedStart AS DATETIME
	DECLARE @dtRequestedFinish AS DATETIME
	DECLARE @numWOAssignedTo NUMERIC(18,0) 
	DECLARE @numWarehouseID NUMERIC(18,0)

	DECLARE @numBuildProcessID NUMERIC(18,0)
	DECLARE @ItemReleaseDate DATE

	DECLARE @numoppitemtCode NUMERIC(18,0)
	DECLARE @numOppChildItemID NUMERIC(18,0)
	DECLARE @numOppKitChildItemID NUMERIC(18,0)

	DECLARE @TEMPItems TABLE
	(
		ID INT IDENTITY(1,1)
		,numoppitemtCode NUMERIC(18,0)
		,numOppChildItemID NUMERIC(18,0)
		,numOppKitChildItemID NUMERIC(18,0)
		,numItemCode NUMERIC(18,0)
		,numUnitHour FLOAT
		,numWarehouseItmsID NUMERIC(18,0)
		,vcItemDesc VARCHAR(3000)
		,vcInstruction VARCHAR(3000)
		,dtPlannedStart DATETIME
		,bintCompliationDate DATETIME
		,ItemReleaseDate DATETIME
		,numBusinessProcessId NUMERIC(18,0)
	)
  
	INSERT INTO @TEMPItems
	(
		numoppitemtCode
		,numOppChildItemID
		,numOppKitChildItemID
		,numItemCode
		,numUnitHour
		,numWarehouseItmsID
		,vcItemDesc
		,vcInstruction
		,dtPlannedStart
		,bintCompliationDate
		,ItemReleaseDate
		,numBusinessProcessId
	)
	SELECT 
		numoppitemtCode
		,0 AS numOppChildItemID
		,0 AS numOppKitChildItemID
		,OpportunityItems.numItemCode
		,ISNULL(numWOQty,0) numUnitHour
		,numWarehouseItmsID
		,vcItemDesc
		,vcInstruction
		,dtPlannedStart
		,bintCompliationDate
		,ItemReleaseDate
		,ISNULL(Item.numBusinessProcessId,0)
	FROM 
		OpportunityItems
	INNER JOIN
		Item
	ON
		OpportunityItems.numItemCode = Item.numItemCode
	WHERE 
		numOppID=@numOppID
		AND ISNULL(bitWorkOrder,0)=1
		AND ISNULL(numWOQty,0) > 0


	DECLARE @i INT = 1
	DECLARE @iCount INT

	SELECT @iCount=COUNT(*) FROM @TEMPItems

	WHILE @i <= @iCount
	BEGIN
		SELECT
			@numoppitemtCode=numoppitemtCode
			,@numOppChildItemID=numOppChildItemID
			,@numOppKitChildItemID=numOppKitChildItemID
			,@numItemCode=numItemCode
			,@numUnitHour=numUnitHour
			,@numWarehouseItmsID=numWarehouseItmsID
			,@vcItemDesc=vcItemDesc
			,@vcInstruction=vcInstruction
			,@dtPlannedStart=dtPlannedStart
			,@dtRequestedFinish=bintCompliationDate
			,@numWOAssignedTo=ISNULL((SELECT numBuildManager FROM Sales_process_List_Master WHERE Slp_Id=numBusinessProcessId),0)
			,@ItemReleaseDate=ItemReleaseDate
			,@numBuildProcessID=numBusinessProcessId
		FROM
			@TEMPItems
		WHERE
			ID = @i 

		IF NOT EXISTS (SELECT numWOId FROM WorkOrder WHERE numDomainID=@numDomainID AND numOppId=@numOppID AND numOppItemID=@numoppitemtCode AND ISNULl(numOppChildItemID,0) = 0 AND ISNULL(numOppKitChildItemID,0) = 0)
		BEGIN
			SET @numWarehouseID = ISNULL((SELECT numWareHouseID FROM WareHouseItems WHERE numWareHouseItemID = @numWarehouseItmsID),0)

			IF EXISTS (SELECT 
						item.numItemCode
					FROM 
						item                                
					INNER JOIN 
						ItemDetails Dtl 
					ON 
						numChildItemID=numItemCode
					WHERE 
						numItemKitID=@numItemCode
						AND charItemType='P'
						AND NOT EXISTS (SELECT numWareHouseItemId FROM WareHouseItems WHERE numItemID=item.numItemCode AND numWareHouseID=@numWarehouseID))
			BEGIN
				RAISERROR('SELECTED_WAREHOUSE_IS_NOT_AVAILABLE_FOR_ALL_CHILD_ITEMS',16,1)
				RETURN
			END

			INSERT INTO WorkOrder
			(
				numItemCode
				,numQtyItemsReq
				,numWareHouseItemId
				,numCreatedBy
				,bintCreatedDate
				,numDomainID
				,numWOStatus
				,numOppId
				,vcItemDesc
				,vcInstruction
				,bintCompliationDate
				,numAssignedTo
				,numOppItemID
				,numOppChildItemID
				,numOppKitChildItemID
				,numBuildProcessId
				,dtmStartDate
				,dtmEndDate
			)
			VALUES
			(
				@numItemCode
				,@numUnitHour
				,@numWarehouseItmsID
				,@numUserCntID
				,GETUTCDATE()
				,@numDomainID
				,0
				,@numOppID
				,@vcItemDesc
				,@vcInstruction
				,@dtRequestedFinish
				,@numWOAssignedTo
				,@numoppitemtCode
				,@numOppChildItemID
				,@numOppKitChildItemID
				,@numBuildProcessID
				,@dtPlannedStart
				,@dtRequestedFinish
			)
	
			SET @numWOId = SCOPE_IDENTITY()

			EXEC dbo.USP_UpdateNameTemplateValueForWorkOrder 3,@numDomainID,@numWOId

			IF(@numBuildProcessId>0)
			BEGIN
				INSERT INTO Sales_process_List_Master
				(
					Slp_Name,
					numdomainid,
					pro_type,
					numCreatedby,
					dtCreatedon,
					numModifedby,
					dtModifiedOn,
					tintConfiguration,
					bitAutomaticStartTimer,
					bitAssigntoTeams,
					numOppId,
					numTaskValidatorId,
					numProjectId,
					numWorkOrderId
				)
					SELECT 
					Slp_Name,
					numdomainid,
					pro_type,
					numCreatedby,
					dtCreatedon,
					numModifedby,
					dtModifiedOn,
					tintConfiguration,
					bitAutomaticStartTimer,
					bitAssigntoTeams,
					0,
					numTaskValidatorId,
					0,
					@numWOId 
				FROM 
					Sales_process_List_Master 
				WHERE 
					Slp_Id=@numBuildProcessId    
					
				DECLARE @numNewProcessId AS NUMERIC(18,0)=0
				SET @numNewProcessId=(SELECT SCOPE_IDENTITY())  
					
				UPDATE WorkOrder SET numBuildProcessId=@numNewProcessId WHERE numWOId=@numWOId

				INSERT INTO StagePercentageDetails
				(
					numStagePercentageId, 
					tintConfiguration, 
					vcStageName, 
					numDomainId, 
					numCreatedBy, 
					bintCreatedDate, 
					numModifiedBy, 
					bintModifiedDate, 
					slp_id, 
					numAssignTo, 
					vcMileStoneName, 
					tintPercentage, 
					tinProgressPercentage, 
					dtStartDate, 
					numParentStageID, 
					intDueDays, 
					numProjectID, 
					numOppID, 
					vcDescription, 
					bitIsDueDaysUsed,
					numTeamId, 
					bitRunningDynamicMode,
					numStageOrder,
					numWorkOrderId
				)
				SELECT
					numStagePercentageId, 
					tintConfiguration, 
					vcStageName, 
					numDomainId, 
					@numUserCntID, 
					GETDATE(), 
					@numUserCntID, 
					GETDATE(), 
					@numNewProcessId, 
					numAssignTo, 
					vcMileStoneName, 
					tintPercentage, 
					tinProgressPercentage, 
					GETDATE(), 
					numParentStageID, 
					intDueDays, 
					0, 
					0, 
					vcDescription, 
					bitIsDueDaysUsed,
					numTeamId, 
					bitRunningDynamicMode,
					numStageOrder,
					@numWOId
				FROM
					StagePercentageDetails
				WHERE
					slp_id=@numBuildProcessId	

				INSERT INTO StagePercentageDetailsTask
				(
					numStageDetailsId, 
					vcTaskName, 
					numHours, 
					numMinutes, 
					numAssignTo, 
					numDomainID, 
					numCreatedBy, 
					dtmCreatedOn,
					numOppId,
					numProjectId,
					numParentTaskId,
					bitDefaultTask,
					bitSavedTask,
					numOrder,
					numReferenceTaskId,
					numWorkOrderId
				)
				SELECT 
					(SELECT TOP 1 SFD.numStageDetailsId FROM StagePercentageDetails AS SFD WHERE SFD.vcStageName=SP.vcStageName AND SFD.numWorkOrderId=@numWOId),
					vcTaskName,
					numHours,
					numMinutes,
					ST.numAssignTo,
					@numDomainID,
					@numUserCntID,
					GETDATE(),
					0,
					0,
					0,
					1,
					CASE WHEN SP.bitRunningDynamicMode=1 THEN 0 ELSE 1 END,
					numOrder,
					numTaskId,
					@numWOId
				FROM 
					StagePercentageDetailsTask AS ST
				LEFT JOIN
					StagePercentageDetails As SP
				ON
					ST.numStageDetailsId=SP.numStageDetailsId
				WHERE
					ISNULL(ST.numOppId,0)=0 AND ISNULL(ST.numProjectId,0)=0 AND
					ST.numStageDetailsId IN(SELECT numStageDetailsId FROM 
					StagePercentageDetails As ST
				WHERE
					ST.slp_id=@numBuildProcessId)
				ORDER BY ST.numOrder
			END

			INSERT INTO [WorkOrderDetails] 
			(
				numWOId,numItemKitID,numChildItemID,numQtyItemsReq,numWareHouseItemId,vcItemDesc,sintOrder,numQtyItemsReq_Orig,numUOMId
			)
			SELECT 
				@numWOId,numItemKitID,numItemCode,
				CAST(((DTL.numQtyItemsReq * ISNULL(dbo.fn_UOMConversion(Dtl.numUOMID,item.numItemCode,item.numDomainID,item.numBaseUnit),1)) * @numUnitHour)AS FLOAT),
				isnull((SELECT TOP 1 numWareHouseItemId FROM WareHouseItems WHERE numItemID=item.numItemCode AND numWareHouseID=@numWarehouseID ORDER BY ISNULL(numWLocationID,0),numWareHouseItemID),0),
				ISNULL(Dtl.vcItemDesc,txtItemDesc),
				ISNULL(sintOrder,0),
				DTL.numQtyItemsReq,
				Dtl.numUOMId 
			FROM 
				item                                
			INNER JOIN 
				ItemDetails Dtl 
			ON 
				numChildItemID=numItemCode
			WHERE 
				numItemKitID=@numItemCode

			--UPDATE ON ORDER OF ASSEMBLY
			UPDATE 
				WareHouseItems
			SET    
				numOnOrder= ISNULL(numOnOrder,0) + @numUnitHour,
				dtModified = GETDATE() 
			WHERE   
				numWareHouseItemID = @numWarehouseItmsID 
	
			--MAKE ENTRY IN WAREHOUSE TRACKING TABLE
			DECLARE @Description AS VARCHAR(1000)
			SET @Description=CONCAT('Work Order Created (Qty:',@numUnitHour,')')

			EXEC dbo.USP_ManageWareHouseItems_Tracking
			@numWareHouseItemID = @numWarehouseItmsID, --  numeric(9, 0)
			@numReferenceID = @numWOId, --  numeric(9, 0)
			@tintRefType = 2, --  tinyint
			@vcDescription = @Description, --  varchar(100)
			@numModifiedBy = @numUserCntID,
			@numDomainID = @numDomainID

			EXEC USP_ManageInventoryWorkOrder @numWOId,@numDomainID,@numUserCntID
		END

		SET @i = @i + 1
	END
END