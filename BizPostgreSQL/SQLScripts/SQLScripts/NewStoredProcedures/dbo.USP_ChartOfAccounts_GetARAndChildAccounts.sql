GO
IF EXISTS (SELECT * FROM sysobjects WHERE xtype = 'p' AND NAME = 'USP_ChartOfAccounts_GetARAndChildAccounts' ) 
    DROP PROCEDURE USP_ChartOfAccounts_GetARAndChildAccounts
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[USP_ChartOfAccounts_GetARAndChildAccounts]
(
    @numDomainID NUMERIC(18,0)
)
AS 
BEGIN
	DECLARE @TEMP TABLE
	(
		numAccountID NUMERIC(18,0)
		,vcAccountName VARCHAR(300)
		,numDefaultARAccountID NUMERIC(18,0)
	)

	DECLARE @numAccountID NUMERIC(18,0)
	DECLARE @vcAccountCode VARCHAR(300)

	SELECT 
		@numAccountID=Chart_Of_Accounts.numAccountID
		,@vcAccountCode=Chart_Of_Accounts.vcAccountCode
	FROM 
		AccountingCharges 
	INNER JOIN 
		Chart_Of_Accounts 
	ON 
		AccountingCharges.numAccountID = Chart_Of_Accounts.numAccountId 
	WHERE 
		AccountingCharges.numDomainID=@numDomainID 
		AND Chart_Of_Accounts.numDomainId=@numDomainID 
		AND numChargeTypeId=4

	IF ISNULL(@numAccountID,0) > 0
	BEGIN
		INSERT INTO @TEMP
		(
			numAccountID
			,vcAccountName
			,numDefaultARAccountID
		)
		SELECT
			numAccountId
			,vcAccountName
			,@numAccountID
		FROM
			Chart_Of_Accounts
		WHERE
			numDomainId=@numDomainID
			AND ISNULL(bitActive,0) = 1
			AND vcAccountCode LIKE ISNULL(@vcAccountCode,'-') + '%'
	END

	SELECT * FROM @TEMP
END
GO


