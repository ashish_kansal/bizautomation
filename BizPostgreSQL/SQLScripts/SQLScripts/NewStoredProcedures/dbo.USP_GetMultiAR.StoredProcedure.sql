GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetMultiAR')
DROP PROCEDURE USP_GetMultiAR
GO
CREATE PROCEDURE [dbo].[USP_GetMultiAR]
(@numParentDomainID  int,
 @dtFromDate datetime,
 @dtToDate datetime,
@numSubscriberID int,
@Rollup int=0)
as
begin

CREATE TABLE #ARSUMMARY
(vcDomainName VARCHAR(150),
Total  DECIMAL(20,5))

INSERT INTO #ARSUMMARY

select DN.vcDomainName,Sum(Total) as Total
from VIEW_ARDAILYSUMMARY VARD
INNER JOIN 
(SELECT * FROM DOMAIN DN WHERE DN.numSubscriberID=@numSubscriberID) DN ON VARD.numDomainID  =  DN.numDomainID 
INNER JOIN (SELECT * FROM DOMAIN DNV WHERE DNV.numSubscriberID=@numSubscriberID) DNV ON VARD.numDomainID = DNV.numDomainID INNER JOIN 
financialyear FY ON FY.numDomainID=DN.numDomainID AND 
FY.dtPeriodFrom <= @dtFromDate and dtPeriodTo >= @dtFromDate 
AND (DN.numParentDomainID = @numParentDomainID or DN.numDomainID=@numParentDomainID)
AND datEntry_Date between FY.dtPeriodFrom and @dtFromDate - 1
GROUP BY DN.vcDomainName
union
select DN.vcDomainName,Sum(Total) as Total
from VIEW_ARDAILYSUMMARY VARD
INNER JOIN 
(SELECT * FROM DOMAIN DN WHERE DN.numSubscriberID=@numSubscriberID) DN ON VARD.numDomainID  =  DN.numDomainID 
INNER JOIN (SELECT * FROM DOMAIN DNV WHERE DNV.numSubscriberID=@numSubscriberID) DNV ON VARD.numDomainID = DNV.numDomainID
AND (DN.numDomainID = @numParentDomainID  )
AND datEntry_Date between @dtFromDate and @dtToDate  
GROUP BY DN.vcDomainName

----------------------------------------
union 
--------------------------
select DN.vcDomainName,Sum(Total) as Total
from VIEW_ARDAILYSUMMARY VARD
INNER JOIN 
(select * from DOMAIN DN where dn.numSubscriberID=@numSubscriberID)DN ON VARD.vcDomainCode  like  DN.vcDomainCode + '%'
INNER JOIN (SELECT * FROM DOMAIN DNV  WHERE DNV.numSubscriberID=@numSubscriberID) DNV ON VARD.numDomainID = DNV.numDomainID INNER JOIN 
financialyear FY ON FY.numDomainID=DN.numDomainID AND 
FY.dtPeriodFrom <= @dtFromDate and dtPeriodTo >= @dtFromDate 
AND (DN.numParentDomainID = @numParentDomainID or DN.numDomainID=@numParentDomainID)
AND datEntry_Date between FY.dtPeriodFrom and @dtFromDate - 1
GROUP BY DN.vcDomainName
union
select DN.vcDomainName,Sum(Total) as Total
from VIEW_ARDAILYSUMMARY VARD
INNER JOIN 
(SELECT * FROM DOMAIN DN WHERE DN.numSubscriberID=@numSubscriberID) DN  ON VARD.vcDomainCode  like  DN.vcDomainCode + '%'
INNER JOIN (SELECT * FROM DOMAIN DNV WHERE DNV.numSubscriberID=@numSubscriberID) DNV  ON VARD.numDomainID = DNV.numDomainID
AND (DN.numParentDomainID = @numParentDomainID  )
AND datEntry_Date between @dtFromDate and @dtToDate  
GROUP BY DN.vcDomainName

if @Rollup=0
	begin
		SELECT vcDomainName, Sum(Total) as Total FROM #ARSUMMARY GROUP BY vcDomainName;
	end
else if @Rollup=1
	begin
		SELECT 'AR Roll-Up' AS  vcDomainName, ISNULL(Sum(Total),0) as Total FROM #ARSUMMARY;
	end
DROP TABLE #ARSUMMARY;
end


