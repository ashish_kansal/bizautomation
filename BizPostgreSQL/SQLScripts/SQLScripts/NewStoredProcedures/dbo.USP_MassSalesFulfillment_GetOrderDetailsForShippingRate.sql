GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_MassSalesFulfillment_GetOrderDetailsForShippingRate')
DROP PROCEDURE dbo.USP_MassSalesFulfillment_GetOrderDetailsForShippingRate
GO

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[USP_MassSalesFulfillment_GetOrderDetailsForShippingRate]
(
	@numDomainID NUMERIC(18,0)
	,@numOppID NUMERIC(18,0)
	,@numShipVia NUMERIC(18,0)
)
AS
BEGIN
	-- GET FROM ADDRESS
	SELECT
		vcStreet
		,vcCity
		,ISNULL((SELECT vcStateCode FROM ShippingStateMaster WHERE numShipCompany=@numShipVia AND vcStateName=dbo.fn_GetState(vcState)),'') vcState
		,vcZipCode
		,ISNULL((SELECT vcCountryCode FROM ShippingCountryMaster WHERE numShipCompany=@numShipVia AND vcCountryName=dbo.fn_GetListItemName(vcCountry)),'') vcCountry
	FROM
		dbo.fn_GetShippingReportAddress(1,@numDomainId,@numOppID)
		  

	-- GET TO ADDRESS
	SELECT
		vcStreet
		,vcCity
		,ISNULL((SELECT vcStateCode FROM ShippingStateMaster WHERE numShipCompany=@numShipVia AND vcStateName=dbo.fn_GetState(vcState)),'') vcState
		,vcZipCode
		,ISNULL((SELECT vcCountryCode FROM ShippingCountryMaster WHERE numShipCompany=@numShipVia AND vcCountryName=dbo.fn_GetListItemName(vcCountry)),'') vcCountry
	FROM
		dbo.fn_GetShippingReportAddress(2,@numDomainId,@numOppID)

	-- GET CONTAINERS/PACKAGES
	DECLARE @TempContiner TABLE
	(
		ID INT IDENTITY(1,1)
		,fltContainerWeight FLOAT
		,fltContainerHeight FLOAT
		,fltContainerWidth FLOAT
		,fltContainerLength FLOAT
		,numQty FLOAT
	)

	DECLARE @TEMPItems TABLE
	(
		ID INT IDENTITY(1,1)
		,numContainer NUMERIC(18,0)
		,numNoItemIntoContainer FLOAT
		,fltItemWeight FLOAT
		,fltItemHeight FLOAT
		,fltItemWidth FLOAT
		,fltItemLength FLOAT
		,fltContainerWeight FLOAT
		,fltContainerHeight FLOAT
		,ltContainerWidth FLOAT
		,fltContainerLength FLOAT
		,numTotalQty FLOAT
	)

	INSERT INTO 
		@TEMPItems
	SELECT
		T1.numContainer
		,T1.numNoItemIntoContainer
		,T1.fltItemWeight
		,T1.fltItemHeight
		,T1.fltItemWidth
		,T1.fltItemLength
		,ISNULL(IContainer.fltWeight,0) fltContainerWeight
		,ISNULL(IContainer.fltHeight,0) fltContainerHeight
		,ISNULL(IContainer.fltWidth,0) fltContainerWidth
		,ISNULL(IContainer.fltLength,0) fltContainerLength
		,T1.numTotalQty
	FROM
	(
		SELECT 
			I.numContainer
			,(CASE WHEN ISNULL(I.numContainer,0) > 0 THEN I.numNoItemIntoContainer ELSE SUM(OI.numUnitHour) END) AS numNoItemIntoContainer
			,ISNULL(I.fltWeight,0) fltItemWeight
			,ISNULL(I.fltHeight,0) fltItemHeight
			,ISNULL(I.fltWidth,0) fltItemWidth
			,ISNULL(I.fltLength,0) fltItemLength
			,SUM(OI.numUnitHour) numTotalQty
		FROM 
			OpportunityItems OI
		INNER JOIN 
			Item AS I
		ON 
			OI.numItemCode = I.numItemCode
		WHERE
			numOppId=@numOppID
			AND I.charItemType ='P'
			AND ISNULL(I.bitContainer,0) = 0
		GROUP BY
			I.numContainer
			,I.numNoItemIntoContainer
			,I.fltWeight
			,I.fltHeight
			,I.fltWidth
			,I.fltLength
	) AS T1
	LEFT JOIN
		Item IContainer
	ON
		T1.numContainer = IContainer.numItemCode

	DECLARE @i AS INT = 1
	DECLARE @iCount AS INT
	DECLARE @numContainer NUMERIC(18,0)
	DECLARE @numNoItemIntoContainer FLOAT
	DECLARE @fltItemWeight FLOAT
	DECLARE @fltItemHeight FLOAT
	DECLARE @fltItemWidth FLOAT
	DECLARE @fltItemLength FLOAT
	DECLARE @fltContainerWeight FLOAT
	DECLARE @fltContainerHeight FLOAT
	DECLARE @ltContainerWidth FLOAT
	DECLARE @fltContainerLength FLOAT
	DECLARE @numTotalQty FLOAT

	SELECT @iCount = COUNT(*) FROM @TEMPItems

	WHILE @i <= @iCount
	BEGIN
		SELECT 
			@numContainer=numContainer
			,@numNoItemIntoContainer=numNoItemIntoContainer
			,@fltItemWeight=fltItemWeight
			,@fltItemHeight=fltItemHeight
			,@fltItemWidth=fltItemWidth
			,@fltItemLength=fltItemLength
			,@fltContainerWeight=fltContainerWeight
			,@fltContainerHeight=fltContainerHeight
			,@ltContainerWidth=ltContainerWidth
			,@fltContainerLength=fltContainerLength
			,@numTotalQty=numTotalQty
		FROM
			@TEMPItems
		WHERE
			ID=@i

		IF @numTotalQty <= @numNoItemIntoContainer
		BEGIN
			INSERT INTO @TempContiner
			(
				fltContainerWeight
				,fltContainerHeight
				,fltContainerWidth
				,fltContainerLength
				,numQty
			)
			VALUES
			(
				ISNULL(@fltContainerWeight,0) + (ISNULL(@numTotalQty,0) * ISNULL(@fltItemWeight,0))
				,@fltContainerHeight
				,@ltContainerWidth
				,@fltContainerLength
				,@numTotalQty
			)
		END
		ELSE
		BEGIN
			WHILE @numTotalQty > @numNoItemIntoContainer
			BEGIN
				INSERT INTO @TempContiner
				(
					fltContainerWeight
					,fltContainerHeight
					,fltContainerWidth
					,fltContainerLength
					,numQty
				)
				VALUES
				(
					ISNULL(@fltContainerWeight,0) + (ISNULL(@numNoItemIntoContainer,0) * ISNULL(@fltItemWeight,0))
					,@fltContainerHeight
					,@ltContainerWidth
					,@fltContainerLength
					,@numNoItemIntoContainer
				)
			
				SET @numTotalQty = @numTotalQty - @numNoItemIntoContainer
			END

			IF ISNULL(@numTotalQty,0) > 0
			BEGIN
				INSERT INTO @TempContiner
				(
					fltContainerWeight
					,fltContainerHeight
					,fltContainerWidth
					,fltContainerLength
					,numQty
				)
				VALUES
				(
					ISNULL(@fltContainerWeight,0) + (ISNULL(@numTotalQty,0) * ISNULL(@fltItemWeight,0))
					,@fltContainerHeight
					,@ltContainerWidth
					,@fltContainerLength
					,@numTotalQty
				)
			END
		END


		SET @i = @i + 1
	END

	SELECT * FROm @TempContiner
END
GO