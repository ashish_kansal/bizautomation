SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_UpdateEDICustomFields')
DROP PROCEDURE dbo.USP_UpdateEDICustomFields
GO

/****** Author :  Priya Sharma
		Date : 19 June 2018
 ******/
CREATE PROCEDURE [dbo].[USP_UpdateEDICustomFields]  
  
	-- @numDomainID NUMERIC(18,0)
	 @Grp_id AS NUMERIC(5)
	,@RecID AS NUMERIC(18,0)
	,@Fld_id AS NUMERIC(9)
	,@Fld_Value AS VARCHAR(MAX)
	,@FldDTLID AS NUMERIC(9)
	,@numModifiedBy AS NUMERIC(9)
	,@bintModifiedBy datetime
  AS

  BEGIN

  /****** Insert Update Opp/Order  ******/
	IF @FldDTLID > 0 AND @Grp_id = 19
	BEGIN

		UPDATE CFW_Fld_Values_Opp_EDI
		SET	
			 Fld_ID = @Fld_id
			,Fld_Value = @Fld_Value
			,RecID = @RecID
			,numModifiedBy = @numModifiedBy
			,bintModifiedDate = @bintModifiedBy
		WHERE FldDTLID = @FldDTLID AND RecID = @RecID

	END

	 ELSE IF @FldDTLID <= 0 AND @Grp_id = 19
	 BEGIN

		INSERT INTO CFW_Fld_Values_Opp_EDI
		(
			 Fld_ID
			,Fld_Value
			,RecID 
			,numModifiedBy
			,bintModifiedDate 
		)
		VALUES
		(
			 @Fld_id
			,@Fld_Value
			,@RecID
			,@numModifiedBy
			,@bintModifiedBy
		)

	END	
	

	  /****** Insert Update Item  ******/

	IF @FldDTLID > 0 AND @Grp_id = 20
	BEGIN
		UPDATE CFW_FLD_Values_Item_EDI
		SET	
			 Fld_ID = @Fld_id
			,Fld_Value = @Fld_Value
			,RecID = @RecID			
		WHERE FldDTLID = @FldDTLID AND RecID = @RecID

	END

	ELSE IF @FldDTLID <= 0 AND @Grp_id = 20
	BEGIN	

		INSERT INTO CFW_FLD_Values_Item_EDI
		(
			 Fld_ID
			,Fld_Value
			,RecID 			
		)
		VALUES
		(
			 @Fld_id
			,@Fld_Value
			,@RecID			
		)

	END

  /****** Insert Update Opp/Order Item  ******/
	IF @FldDTLID > 0 AND @Grp_id = 21
	BEGIN
		UPDATE CFW_Fld_Values_OppItems_EDI
		SET	
			 Fld_ID = @Fld_id
			,Fld_Value = @Fld_Value
			,RecID = @RecID			
		WHERE FldDTLID = @FldDTLID AND RecID = @RecID

	END

	ELSE IF @FldDTLID <= 0 AND @Grp_id = 21
	BEGIN	

		INSERT INTO CFW_Fld_Values_OppItems_EDI
		(
			 Fld_ID
			,Fld_Value
			,RecID 			
		)
		VALUES
		(
			 @Fld_id
			,@Fld_Value
			,@RecID			
		)

	END

  /****** Insert Update Opp/Order Item  ******/


  END

GO


