GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GET_ITEM_VENDOR_FOR_UPDATE')
DROP PROCEDURE USP_GET_ITEM_VENDOR_FOR_UPDATE
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE dbo.USP_GET_ITEM_VENDOR_FOR_UPDATE
(
	@numDomainID		NUMERIC(18,0)	
)
AS 
BEGIN
	SELECT IT.numItemCode AS [Item ID],
		   IT.vcItemName AS [Item],
		   (SELECT vcCompanyName FROM dbo.CompanyInfo 
			WHERE numCompanyID = (SELECT numCompanyID FROM dbo.DivisionMaster WHERE numDivisionID = VEN.numVendorID))	AS [Vendor],
		   VEN.vcPartNo AS [Vendor Part#],
		   VEN.monCost AS [Vendor Cost],
		   VEN.intMinQty AS [Vendor Minimum Qty],
		   (CASE WHEN IT.numVendorID = VEN.numVendorID
				THEN 'TRUE'
				ELSE 'FALSE'
		   END) AS [Is Primary Vendor]	
	FROM dbo.Vendor VEN
	INNER JOIN dbo.Item IT ON VEN.numItemCode = IT.numItemCode AND VEN.numDomainID = IT.numDomainID 
	WHERE  IT.numDomainID = @numDomainID
END