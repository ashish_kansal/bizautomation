/****** Object:  StoredProcedure [dbo].[USP_OPPCheckWOStatus]    Script Date: 07/26/2008 16:20:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--exec USP_OPPCheckWOStatus 5853,1  
--created by anoop jayaraj              
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_OPPCheckWOStatus')
DROP PROCEDURE USP_OPPCheckWOStatus
GO
CREATE PROCEDURE [dbo].[USP_OPPCheckWOStatus]              
@intOpportunityId as numeric(9)
as              
              
DECLARE @OppType AS VARCHAR(2)   
DECLARE @Sel AS TINYINT   

set @Sel=0          
select @OppType=tintOppType from OpportunityMaster where numOppId=@intOpportunityId
                                           
  if @OppType=1                                              
  begin          
   SET @Sel = (SELECT COUNT(*) FROM WorkOrder WHERE ISNULL(numOppId,0)=@intOpportunityId AND numWOStatus!=23184)
  end    
  
select @Sel
GO
