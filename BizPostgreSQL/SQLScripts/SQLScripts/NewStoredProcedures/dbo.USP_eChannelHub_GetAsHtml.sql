GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_eChannelHub_GetAsHtml')
DROP PROCEDURE USP_eChannelHub_GetAsHtml
GO
CREATE PROCEDURE [dbo].[USP_eChannelHub_GetAsHtml]
	@numDomainID NUMERIC(18,0)
AS
BEGIN
	DECLARE @vchtml VARCHAR(MAX) = '<div class="row">'
	SET @vchtml = CONCAT(@vchtml,'<div class="col-xs-12 col-md-6"><b>Marketplaces</b><hr style="margin-top:6px;margin-bottom:10px;border-color:#ddd;"/>')
	SET @vchtml = CONCAT(@vchtml,'<div class="row">')
	SET @vchtml = CONCAT(@vchtml,STUFF((SELECT
												CONCAT('<div class="col-sm-12 col-md-4"><div class="checkbox-inline"><input type="checkbox"',(CASE WHEN ISNULL(TEMP.numMarketplaceID,0) > 0 THEN ' checked' ELSE '' END),' id="chkMarketplace',eChannelHub.ID,'" onchange="MarketplaceSelectionChanged();" /><label style="font-weight:normal;" class="lblMarketplaceName">',vcMarketplace,(CASE 
																																																																																																WHEN numDivisionID IS NOT NULL 
																																																																																																THEN CONCAT(' <i>',(CASE 
																																																																																																						WHEN ISNULL(tintCRMType,0)= 1 THEN CONCAT('<a href="../prospects/frmProspects.aspx?DivID=',numDivisionID,'" target="_blank">')
																																																																																																						WHEN ISNULL(tintCRMType,0)= 2 THEN CONCAT('<a href="../account/frmAccounts.aspx?DivID=',numDivisionID,'" target="_blank">')
																																																																																																						ELSE CONCAT('<a href="../Leads/frmLeads.aspx?DivID=',numDivisionID,'" target="_blank">')
																																																																																																					END),vcCompanyName,'</a></i>') 
																																																																																																ELSE '' 
																																																																																															END),'</label></div></div>')
											FROM 
												eChannelHub 
											LEFT JOIN
											(
												SELECT
													numMarketplaceID
													,DivisionMaster.numDivisionID
													,DivisionMaster.tintCRMType
													,CompanyInfo.vcCompanyName
												FROM
													DomainMarketplace
												LEFT JOIN
													DivisionMaster
												ON
													DomainMarketplace.numDivisionID = DivisionMaster.numDivisionID
												LEFT JOIN
													CompanyInfo
												ON
													DivisionMaster.numCompanyID = CompanyInfo.numCompanyID
												WHERE
													DomainMarketplace.numDomainID = @numDomainID
											) TEMP
											ON
												eChannelHub.ID = TEMP.numMarketplaceID
											WHERE 
												vcType='Marketplaces'
											ORDER BY
												vcMarketplace
											FOR XML PATH(''), TYPE).value('.', 'varchar(max)'),1,0,''))
	SET @vchtml = CONCAT(@vchtml,'</div>')
	SET @vchtml = CONCAT(@vchtml,'</div>')
	SET @vchtml = CONCAT(@vchtml,'<div class="col-xs-12 col-md-3"><b>Comparison Shopping</b><hr style="margin-top:6px;margin-bottom:10px;border-color:#ddd;"/>')
	SET @vchtml = CONCAT(@vchtml,'<div class="row">')
	SET @vchtml = CONCAT(@vchtml,STUFF((SELECT
												CONCAT('<div class="col-sm-12"><div class="checkbox-inline"><input type="checkbox"',(CASE WHEN ISNULL(TEMP.numMarketplaceID,0) > 0 THEN ' checked="checked"' ELSE '' END),' id="chkMarketplace',eChannelHub.ID,'" onchange="MarketplaceSelectionChanged();" /><label style="font-weight:normal;" class="lblMarketplaceName">',vcMarketplace,(CASE 
																																																																																																WHEN numDivisionID IS NOT NULL 
																																																																																																THEN CONCAT(' <i>',(CASE 
																																																																																																						WHEN ISNULL(tintCRMType,0)= 1 THEN CONCAT('<a href="../prospects/frmProspects.aspx?DivID=',numDivisionID,'" target="_blank">')
																																																																																																						WHEN ISNULL(tintCRMType,0)= 2 THEN CONCAT('<a href="../account/frmAccounts.aspx?DivID=',numDivisionID,'" target="_blank">')
																																																																																																						ELSE CONCAT('<a href="../Leads/frmLeads.aspx?DivID=',numDivisionID,'" target="_blank">')
																																																																																																					END),vcCompanyName,'</a></i>') 
																																																																																																ELSE '' 
																																																																																															END),'</label></div></div>')
											FROM 
												eChannelHub 
											LEFT JOIN
											(
												SELECT
													numMarketplaceID
													,DivisionMaster.numDivisionID
													,DivisionMaster.tintCRMType
													,CompanyInfo.vcCompanyName
												FROM
													DomainMarketplace
												LEFT JOIN
													DivisionMaster
												ON
													DomainMarketplace.numDivisionID = DivisionMaster.numDivisionID
												LEFT JOIN
													CompanyInfo
												ON
													DivisionMaster.numCompanyID = CompanyInfo.numCompanyID
												WHERE
													DomainMarketplace.numDomainID = @numDomainID
											) TEMP
											ON
												eChannelHub.ID = TEMP.numMarketplaceID
											WHERE 
												vcType='Comparison Shopping'
											ORDER BY
												vcMarketplace
											FOR XML PATH(''), TYPE).value('.', 'varchar(max)'),1,0,''))
	SET @vchtml = CONCAT(@vchtml,'</div>')
	SET @vchtml = CONCAT(@vchtml,'</div>')
	SET @vchtml = CONCAT(@vchtml,'<div class="col-xs-12 col-md-3"><b>3rd Party Shopping Carts</b><hr style="margin-top:6px;margin-bottom:10px;border-color:#ddd;"/>')
	SET @vchtml = CONCAT(@vchtml,'<div class="row">')
	SET @vchtml = CONCAT(@vchtml,STUFF((SELECT
												CONCAT('<div class="col-sm-12"><div class="checkbox-inline"><input type="checkbox"',(CASE WHEN ISNULL(TEMP.numMarketplaceID,0) > 0 THEN ' checked="checked"' ELSE '' END),' id="chkMarketplace',eChannelHub.ID,'" onchange="MarketplaceSelectionChanged();" /><label style="font-weight:normal;" class="lblMarketplaceName">',vcMarketplace,(CASE 
																																																																																																WHEN numDivisionID IS NOT NULL 
																																																																																																THEN CONCAT(' <i>',(CASE 
																																																																																																						WHEN ISNULL(tintCRMType,0)= 1 THEN CONCAT('<a href="../prospects/frmProspects.aspx?DivID=',numDivisionID,'" target="_blank">')
																																																																																																						WHEN ISNULL(tintCRMType,0)= 2 THEN CONCAT('<a href="../account/frmAccounts.aspx?DivID=',numDivisionID,'" target="_blank">')
																																																																																																						ELSE CONCAT('<a href="../Leads/frmLeads.aspx?DivID=',numDivisionID,'" target="_blank">')
																																																																																																					END),vcCompanyName,'</a></i>') 
																																																																																																ELSE '' 
																																																																																															END),'</label></div></div>')
											FROM 
												eChannelHub 
											LEFT JOIN
											(
												SELECT
													numMarketplaceID
													,DivisionMaster.numDivisionID
													,DivisionMaster.tintCRMType
													,CompanyInfo.vcCompanyName
												FROM
													DomainMarketplace
												LEFT JOIN
													DivisionMaster
												ON
													DomainMarketplace.numDivisionID = DivisionMaster.numDivisionID
												LEFT JOIN
													CompanyInfo
												ON
													DivisionMaster.numCompanyID = CompanyInfo.numCompanyID
												WHERE
													DomainMarketplace.numDomainID = @numDomainID
											) TEMP
											ON
												eChannelHub.ID = TEMP.numMarketplaceID
											WHERE 
												vcType='3rd Party Shopping Carts'
											ORDER BY
												vcMarketplace
											FOR XML PATH(''), TYPE).value('.', 'varchar(max)'),1,0,''))
	SET @vchtml = CONCAT(@vchtml,'</div>')
	SET @vchtml = CONCAT(@vchtml,'</div>')
	SET @vchtml = CONCAT(@vchtml,'</div>')

	SELECT @vchtml
END
GO