SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_PromotionOffer_EnableDisable')
DROP PROCEDURE USP_PromotionOffer_EnableDisable
GO
CREATE PROCEDURE [dbo].[USP_PromotionOffer_EnableDisable]
	@numDomainID NUMERIC(18,0),
	@numProId NUMERIC(18,0)
AS
BEGIN
	IF EXISTS (SELECT numProId FROM PromotionOffer WHERE numDomainId=@numDomainID AND numProId=@numProId AND ISNULL(bitEnabled,0) = 0 AND ISNULL(bitError,0) = 1)
	BEGIN
		RAISERROR('PROMOTION_CONFIGURATION_INVALID',16,1)
		RETURN
	END
	ELSE
	BEGIN
		UPDATE
			PromotionOffer
		SET
			bitEnabled = (CASE WHEN ISNULL(bitEnabled,0)=1 THEN 0 ELSE 1 END)
		WHERE	
			numDomainId=@numDomainID
			AND numProId=@numProId
	END
END