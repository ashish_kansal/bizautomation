SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ManageRuleMaster')
DROP PROCEDURE USP_ManageRuleMaster
GO
CREATE PROCEDURE [dbo].[USP_ManageRuleMaster]
    @numRuleID AS NUMERIC(9) = 0,
    @numDomainID AS NUMERIC(9) = 0,
    @tintModuleType AS TINYINT, 
    @vcRuleName AS VARCHAR(100),
    @vcRuleDescription AS VARCHAR(500),
    @tintBasedOn AS TINYINT,
    @tintStatus AS TINYINT,
	@numFormFieldIdChange as numeric(9)=0,
	@byteMode as tinyint=0
AS 
IF @byteMode=1
BEGIN
     Delete from RuleAction  WHERE   numRuleID=@numRuleID and numDomainId=@numDomainId
	 Delete from RuleCondition  WHERE   numRuleID=@numRuleID and numDomainId=@numDomainId
	 Delete from RuleMaster  WHERE   numRuleID=@numRuleID and numDomainId=@numDomainId

	SELECT  @numRuleID
END
ELSE
BEGIN
    IF @numRuleID = 0 
        BEGIN  
            INSERT  INTO RuleMaster
                    (
                      numDomainId,tintModuleType,vcRuleName,vcRuleDescription,tintBasedOn,tintStatus,bintCreatedDate,numFormFieldIdChange
                    )
            VALUES  (
                      @numDomainId,@tintModuleType,@vcRuleName,@vcRuleDescription,@tintBasedOn,@tintStatus,getutcdate(),@numFormFieldIdChange
                    )

            SELECT  SCOPE_IDENTITY()  
        END  
    ELSE 
        BEGIN  
            UPDATE  RuleMaster
            SET     tintModuleType=@tintModuleType,vcRuleName=@vcRuleName,vcRuleDescription=@vcRuleDescription,
					tintBasedOn=@tintBasedOn,tintStatus=@tintStatus,bintModifiedDate=getutcdate(),numFormFieldIdChange=@numFormFieldIdChange
            WHERE   numRuleID=@numRuleID and numDomainId=@numDomainId

            SELECT  @numRuleID
        END
END
GO
