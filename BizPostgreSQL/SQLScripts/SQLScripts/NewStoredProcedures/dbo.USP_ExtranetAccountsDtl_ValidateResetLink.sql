GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ExtranetAccountsDtl_ValidateResetLink')
DROP PROCEDURE USP_ExtranetAccountsDtl_ValidateResetLink
GO
CREATE PROCEDURE [dbo].[USP_ExtranetAccountsDtl_ValidateResetLink]
@numDomainID NUMERIC(18,0),
@vcResetID VARCHAR(500)
as
BEGIN
	DECLARE @bitValidResetID BIT = 0

	IF EXISTS (SELECT numExtranetDtlID FROM ExtranetAccountsDtl WHERE vcResetLinkID=@vcResetID AND DATEDIFF(MINUTE, vcResetLinkCreatedTime, GETUTCDATE()) <= 10)
	BEGIN
		SET @bitValidResetID = 1
	END

	SELECT @bitValidResetID
END
GO

