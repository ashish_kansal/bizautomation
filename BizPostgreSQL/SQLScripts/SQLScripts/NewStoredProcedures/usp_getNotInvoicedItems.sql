/****** Object:  StoredProcedure [dbo].[usp_getNotInvoicedItems]    Script Date: 05/07/2009 22:12:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getNotInvoicedItems')
DROP PROCEDURE usp_getNotInvoicedItems
GO
CREATE PROCEDURE [dbo].[usp_getNotInvoicedItems]
	@numDomainID Numeric(18,2)=0,
	@numoppId Numeric(18,0)=0

AS
BEGIN
	SELECT (SELECT SUBSTRING((SELECT ',' + cast(I.vcItemName as varchar)
                     FROM 
													 OpportunityItems AS t LEFT JOIN  Item as I ON 
													 t.numItemCode=I.numItemCode WHERE 
													 t.numOppId=@numoppId AND t.numoppItemtCode NOT IN 
													 (SELECT numOppItemId FROM OpportunityBizDocs JOIN 
													 OpportunityBizDocItems ON OpportunityBizDocs.numOppBizDocsId=OpportunityBizDocItems.numOppBizDocID  
													 WHERE OpportunityBizDocs.numOppId=@numoppId AND ISNULL(bitAuthoritativeBizDocs,0)=1)
                     FOR XML PATH('')), 2, 200000)  )[List_Item_Approval_UNIT] 
END