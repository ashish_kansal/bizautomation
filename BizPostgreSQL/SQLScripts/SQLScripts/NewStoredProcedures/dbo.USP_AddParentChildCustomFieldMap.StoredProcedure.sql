        
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_AddParentChildCustomFieldMap')
	DROP PROCEDURE USP_AddParentChildCustomFieldMap
GO

CREATE PROCEDURE [dbo].[USP_AddParentChildCustomFieldMap]
	@numDomainID numeric(18, 0),
	@numRecordID numeric(18, 0),
	@numParentRecId numeric(18, 0),
	@tintPageID TINYINT 
AS
BEGIN
	SELECT 
		tintParentModule
		,numParentFieldID
		,tintChildModule
		,numChildFieldID
		,PFM.Fld_type AS ParentFld_type
		,ISNULL(PFM.numlistid,0) AS Parentlistid
		,CFM.Fld_type AS ChildFld_type
		,ISNULL(CFM.numlistid,0) AS Childlistid
		,ROW_NUMBER() OVER(ORDER BY numParentFieldID) RowNO 
	INTO 
		#temp
	FROM 
		ParentChildCustomFieldMap PCFM 
	JOIN CFW_Loc_Master PM ON PCFM.tintParentModule=PM.Loc_id
	JOIN CFW_Fld_Master PFM ON PCFM.numParentFieldID=PFM.Fld_id AND PCFM.numDomainID=PFM.numDomainID AND PCFM.tintParentModule=PFM.Grp_id
	JOIN CFW_Loc_Master CM ON PCFM.tintChildModule=CM.Loc_id
	JOIN CFW_Fld_Master CFM ON PCFM.numChildFieldID=CFM.Fld_id AND PCFM.numDomainID=CFM.numDomainID AND PCFM.tintChildModule=CFM.Grp_id
	WHERE 
		PCFM.numDomainID=@numDomainID 
		AND PCFM.tintChildModule=@tintPageID 
		AND PFM.Fld_type=CFM.Fld_type

	DECLARE @minNo AS NUMERIC,@maxNo AS NUMERIC;
	SELECT @minNo=MIN(RowNO),@maxNo=MAX(RowNO) FROM #temp

	DECLARE @tintParentModule AS TINYINT,@numParentFieldID AS NUMERIC,@ParentFld_type AS VARCHAR(20),@Parentlistid AS NUMERIC
	DECLARE @tintChildModule AS TINYINT,@numChildFieldID AS NUMERIC,@ChildFld_type AS VARCHAR(20),@Childlistid AS NUMERIC
	DECLARE @ParentRecId AS NUMERIC,@Fld_Value AS VARCHAR(1000),@vcData VARCHAR(50)

	WHILE @minNo <= @maxNo
	BEGIN
		SELECT 
			@tintParentModule=tintParentModule
			,@numParentFieldID=numParentFieldID
			,@ParentFld_type=ParentFld_type
			,@Parentlistid=Parentlistid
			,@tintChildModule=tintChildModule
			,@numChildFieldID=numChildFieldID
			,@ChildFld_type=ChildFld_type
			,@Childlistid=Childlistid 
		FROM 
			#temp 
		WHERE 
			RowNO=@minNo 	

		IF @ParentFld_type='SelectBox' AND @ChildFld_type='SelectBox'
		BEGIN
			SET @Fld_Value = ''
			
			IF @tintParentModule=1 --Organizations
			BEGIN
				SELECT @Fld_Value=Fld_Value FROM CFW_FLD_Values WHERE RecId=@numParentRecId AND Fld_ID=@numParentFieldID
			END
			ELSE IF (@tintParentModule=6 OR @tintParentModule = 2)   --Sales Opportunities / Orders
			BEGIN
				SELECT @Fld_Value=Fld_Value FROM CFW_Fld_Values_Opp WHERE RecId=@numParentRecId AND Fld_ID=@numParentFieldID
			END

			IF @Parentlistid>0 AND @Childlistid>0 AND @Fld_Value <> ''
			BEGIN
				SELECT @vcData=vcData FROM dbo.ListDetails WHERE numDomainID=@numDomainID AND numListID=@Parentlistid AND numListItemID=CAST(@Fld_Value AS NUMERIC(18))
				
				SET @Fld_Value=NULL
				
				SELECT @Fld_Value=numListItemID FROM dbo.ListDetails WHERE numDomainID=@numDomainID AND numListID=@Childlistid AND LOWER(vcData)=LOWER(ISNULL(@vcData,''))
			END
			ELSE
			BEGIN
				SET @Fld_Value=NULL
			END

			IF @Fld_Value IS NOT NULL 
			BEGIN
				IF @tintChildModule=2 OR @tintChildModule=6 --Sales Opportunities / Orders
				BEGIN
					DELETE FROM CFW_Fld_Values_Opp WHERE RecId = @numRecordID AND Fld_ID=@numChildFieldID

					INSERT INTO CFW_Fld_Values_Opp 
					(Fld_ID,Fld_Value,RecId) 
					SELECT 
					@numChildFieldID,@Fld_Value,@numRecordID
				END
				ELSE IF @tintChildModule=3 --Case
				BEGIN
					DELETE FROM CFW_FLD_Values_Case WHERE RecId = @numRecordID AND Fld_ID=@numChildFieldID

					INSERT INTO CFW_FLD_Values_Case 
					(Fld_ID,Fld_Value,RecId)          
					SELECT 
					@numChildFieldID,@Fld_Value,@numRecordID
				END
				ELSE IF @tintChildModule=11 --Projects
				BEGIN
					DELETE FROM CFW_Fld_Values_Pro WHERE RecId = @numRecordID AND Fld_ID=@numChildFieldID

					INSERT INTO CFW_Fld_Values_Pro 
					(Fld_ID,Fld_Value,RecId)          
					SELECT 
					@numChildFieldID,@Fld_Value,@numRecordID
				END
			END
		END
		ELSE IF @ParentFld_type='CheckBoxList' AND @ChildFld_type='CheckBoxList'
		BEGIN
			IF @Parentlistid>0 AND @Childlistid>0
			BEGIN
				DECLARE @TEMP TABLE
				(
					ID INT IDENTITY(1,1)
					,ListItemID NUMERIC(18,0)
				)

				IF @tintParentModule=1 --Organizations
				BEGIN
					INSERT INTO @TEMP
					(
						ListItemID
					)
					SELECT * FROM dbo.SplitIDs(ISNULL((SELECT Fld_Value FROM CFW_FLD_Values WHERE RecId=@numParentRecId AND Fld_ID=@numParentFieldID),''),',')
				END
				ELSE IF (@tintParentModule = 6 OR @tintParentModule = 2) --Sales Opportunities / Orders
				BEGIN
					INSERT INTO @TEMP
					(
						ListItemID
					)
					SELECT Id FROM dbo.SplitIDs(ISNULL((SELECT Fld_Value FROM CFW_Fld_Values_Opp WHERE RecId=@numParentRecId AND Fld_ID=@numParentFieldID),''),',')
				END

				DECLARE @i INT = 1
				DECLARE @iCount INT 
				DECLARE @Fld_VlaueTemp NUMERIC(18,0)
				DECLARE @Fld_ValueCheckBoxList VARCHAR(MAX) = ''
				SET @Fld_ValueCheckBoxList=NULL

				SELECT @iCount = COUNT(*) FROM @TEMP
				
				WHILE @i <= @iCount
				BEGIN
					SELECT @Fld_VlaueTemp = ListItemID FROM @TEMP WHERE ID = @i
		
					SELECT @vcData=vcData FROM dbo.ListDetails WHERE numDomainID=@numDomainID AND numListID=@Parentlistid AND numListItemID=@Fld_VlaueTemp

					IF EXISTS (SELECT numListItemID FROM dbo.ListDetails WHERE numDomainID=@numDomainID AND numListID=@Childlistid AND LOWER(vcData)=LOWER(ISNULL(@vcData,'')))
					BEGIN
						SET @Fld_ValueCheckBoxList = CONCAT(@Fld_ValueCheckBoxList,(CASE WHEN LEN(ISNULL(@Fld_ValueCheckBoxList,'')) > 0 THEN CONCAT(',',(SELECT numListItemID FROM dbo.ListDetails WHERE numDomainID=@numDomainID AND numListID=@Childlistid AND LOWER(vcData)=LOWER(ISNULL(@vcData,'')))) ELSE CAST((SELECT numListItemID FROM dbo.ListDetails WHERE numDomainID=@numDomainID AND numListID=@Childlistid AND LOWER(vcData)=LOWER(ISNULL(@vcData,''))) AS VARCHAR) END))
					END

					SET @i = @i + 1
				END

				IF @Fld_ValueCheckBoxList IS NOT NULL 
				BEGIN
					IF @tintChildModule=2 OR @tintChildModule=6 --Sales Opportunities / Orders
					BEGIN
						DELETE FROM CFW_Fld_Values_Opp WHERE RecId = @numRecordID AND Fld_ID=@numChildFieldID

						INSERT INTO CFW_Fld_Values_Opp 
						(Fld_ID,Fld_Value,RecId) 
						SELECT 
						@numChildFieldID,@Fld_ValueCheckBoxList,@numRecordID
					END
					ELSE IF @tintChildModule=3 --Case
					BEGIN
						DELETE FROM CFW_FLD_Values_Case WHERE RecId = @numRecordID AND Fld_ID=@numChildFieldID

						INSERT INTO CFW_FLD_Values_Case 
						(Fld_ID,Fld_Value,RecId)          
						SELECT 
						@numChildFieldID,@Fld_ValueCheckBoxList,@numRecordID
					END
					ELSE IF @tintChildModule=11 --Projects
					BEGIN
						DELETE FROM CFW_Fld_Values_Pro WHERE RecId = @numRecordID AND Fld_ID=@numChildFieldID

						INSERT INTO CFW_Fld_Values_Pro 
						(Fld_ID,Fld_Value,RecId)          
						SELECT 
						@numChildFieldID,@Fld_ValueCheckBoxList,@numRecordID
					END
				END
			END
		END
		ELSE
		BEGIN
			IF @tintParentModule=1 --Organizations
			BEGIN
				SELECT @Fld_Value=Fld_Value FROM CFW_FLD_Values WHERE RecId=@numParentRecId AND Fld_ID=@numParentFieldID
			END
			ELSE IF (@tintParentModule = 6 OR @tintParentModule = 2) --Sales Opportunities / Orders
			BEGIN
				SELECT @Fld_Value=Fld_Value FROM CFW_Fld_Values_Opp WHERE RecId=@numParentRecId AND Fld_ID=@numParentFieldID
			END

			IF @Fld_Value IS NOT NULL 
			BEGIN
				IF @tintChildModule=2 OR @tintChildModule=6 --Sales Opportunities / Orders
				BEGIN
					DELETE FROM CFW_Fld_Values_Opp WHERE RecId = @numRecordID AND Fld_ID=@numChildFieldID
				
					INSERT INTO CFW_Fld_Values_Opp 
					(Fld_ID,Fld_Value,RecId) 
					SELECT 
					@numChildFieldID,@Fld_Value,@numRecordID
				END
				ELSE IF @tintChildModule=3 --Case
				BEGIN
					DELETE FROM CFW_FLD_Values_Case WHERE RecId = @numRecordID AND Fld_ID=@numChildFieldID

					INSERT INTO CFW_FLD_Values_Case 
					(Fld_ID,Fld_Value,RecId)          
					SELECT 
					@numChildFieldID,@Fld_Value,@numRecordID
				END
				ELSE IF @tintChildModule=11 --Projects
				BEGIN
					DELETE FROM CFW_Fld_Values_Pro WHERE RecId = @numRecordID AND Fld_ID=@numChildFieldID

					INSERT INTO CFW_Fld_Values_Pro 
					(Fld_ID,Fld_Value,RecId)          
					SELECT 
					@numChildFieldID,@Fld_Value,@numRecordID
				END
			END
		END
	
		SET @minNo = @minNo + 1
	END	
END
