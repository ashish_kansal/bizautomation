
GO
/****** Object:  StoredProcedure [dbo].[usp_UpdateStagePercentageDetails]    Script Date: 09/21/2010 12:00:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
----Created By Gangadhar    
    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetTaskByStageDetailsId')
DROP PROCEDURE USP_GetTaskByStageDetailsId
GO
CREATE PROCEDURE [dbo].[USP_GetTaskByStageDetailsId]
@numDomainID as numeric(9)=0,    
@numStageDetailsId as numeric(18)=0,
@numOppId AS NUMERIC(18,0)=0,   
@numProjectId AS NUMERIC(18,0)=0,
@vcSearchText AS VARCHAR(MAX)='',
@bitAlphabetical AS BIT=0
as    
BEGIN   
	DECLARE @dynamicQuery AS NVARCHAR(MAX)=NULL
	SET @dynamicQuery = 'SELECT 
		ST.numTaskId,
		ST.vcTaskName,
		ST.numHours,
		ST.numMinutes,
		ST.numAssignTo,
		ISNULL(AC.vcFirstName+'' ''+AC.vcLastName,''-'') As vcContactName,
		ISNULL(ST.bitDefaultTask,0) AS bitDefaultTask,
		ISNULL(ST.bitTaskClosed,0) AS bitTaskClosed,
		ISNULL(ST.bitSavedTask,0) AS bitSavedTask,
		ISNULL(ST.numParentTaskId,0) AS numParentTaskId,
		ISNULL(AU.vcFirstName+'' ''+AU.vcLastName,''-'') As vcUpdatedByContactName,
		convert(varchar(10),ST.dtmUpdatedOn, 105) + right(convert(varchar(32),ST.dtmUpdatedOn,100),8) As dtmUpdatedOn,
		 convert(varchar(10),PPD.dtmStartDate, 101) + right(convert(varchar(32),PPD.dtmStartDate,100),8) AS StageStartDate,
		ST.numStageDetailsId,
		AC.numTeam,
		ISNULL(S.numTeamId,0) AS numTeamId,
		ISNULL(ST.numOrder,0) AS numOrder, 
		ISNULL(S.numStageOrder,0) AS numStageOrder,
		M.numStagePercentage,
		ISNULL(S.intTaskType,0) AS intTaskType
	FROM
		StagePercentageDetailsTask AS ST
	LEFT JOIN 
		StagePercentageDetails As S
	ON
		ST.numStageDetailsId=S.numStageDetailsId
	LEFT JOIN
		StagePercentageMaster AS M
	ON
		M.numStagePercentageId=S.numStagePercentageId
	LEFT JOIN
		AdditionalContactsInformation AS AC
	ON
		ST.numAssignTo=AC.numContactId
	LEFT JOIN 
		AdditionalContactsInformation AS AU
	ON
		ST.numCreatedBy=AU.numContactId
	LEFT JOIN
		ProjectProcessStageDetails AS PPD
	ON
		ST.numStageDetailsId=PPD.numStageDetailsId AND ST.numOppId=PPD.numOppId
	WHERE
		ST.numDomainID='+CAST(@numDomainID AS VARCHAR(100))+' '
	IF(@numStageDetailsId>0)
	BEGIN
		SET @dynamicQuery=@dynamicQuery+' AND ST.numStageDetailsId='+CAST(@numStageDetailsId AS VARCHAR(100))+' '
	END
	SET @dynamicQuery=@dynamicQuery+' AND ST.numOppId='+CAST(@numOppId AS VARCHAR(100))+' '
	SET @dynamicQuery=@dynamicQuery+' AND ST.numProjectId='+CAST(@numProjectId AS VARCHAR(100))+' '
	
	IF(LEN(@vcSearchText)>0)
	BEGIN
		SET @dynamicQuery=@dynamicQuery+' AND ST.vcTaskName LIKE ''%'+CAST(@vcSearchText AS VARCHAR(100))+'%'' '
	END
	IF(@bitAlphabetical=1)
	BEGIN
		SET @dynamicQuery=@dynamicQuery+' ORDER BY ST.vcTaskName ASC '
	END
	ELSE
	BEGIN
		SET @dynamicQuery=@dynamicQuery+' ORDER BY ST.numOrder ASC '
	END
	EXEC(@dynamicQuery)
END 