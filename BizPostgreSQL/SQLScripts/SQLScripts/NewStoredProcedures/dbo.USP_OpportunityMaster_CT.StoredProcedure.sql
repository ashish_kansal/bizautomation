/****** Object:  StoredProcedure [dbo].[USP_GetPageElementAttributes]    Script Date: 08/08/2009 16:15:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--EXEC [USP_GetPageElementAttributes] @numSiteID = 3,@numElementID =4
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_OpportunityMaster_CT')
DROP PROCEDURE USP_OpportunityMaster_CT
GO
-- =============================================
-- Author:		<Author,,Sachin Sadhu>
-- Create date: <Create Date,,23April2014>
-- Description:	<Description,,Change Tracking>
-- =============================================
Create PROCEDURE [dbo].[USP_OpportunityMaster_CT]
	-- Add the parameters for the stored procedure here
    @numDomainID numeric(18, 0)=0,
    @numUserCntID numeric(18, 0)=0,
    @numRecordID numeric(18, 0)=0 
AS
BEGIN
-- SET NOCOUNT ON added to prevent extra result sets from
-- interfering with SELECT statements.
SET NOCOUNT ON;
 -- Insert statements for procedure here
 declare @tblFields as table (

 FieldName varchar(200),
 FieldValue varchar(200)
)

Declare @Type char(1)
Declare @ChangeVersion bigint
Declare @CreationVersion bigint
Declare @last_synchronization_version bigInt=null

SELECT 			
@ChangeVersion=Ct.Sys_Change_version,@CreationVersion=Ct.Sys_Change_Creation_version
FROM 
CHANGETABLE(CHANGES dbo.OpportunityMaster, 0) AS CT
WHERE numOppId=@numRecordID

IF (@ChangeVersion=@CreationVersion)--Insert
Begin
        SELECT 
		@Type = CT.SYS_CHANGE_OPERATION
		FROM 
		CHANGETABLE(CHANGES dbo.OpportunityMaster, @last_synchronization_version) AS CT
		WHERE  CT.numOppID=@numRecordID

End
Else--Update
Begin

SET @last_synchronization_version=@ChangeVersion-1
        

        SELECT 
		@Type = CT.SYS_CHANGE_OPERATION
		FROM 
		CHANGETABLE(CHANGES dbo.OpportunityMaster, @last_synchronization_version) AS CT
		WHERE  CT.numOppID=@numRecordID

		
		
		--Fields Update:
			Declare @UFFields as table 
			(
					tintActive varchar(70),
					monPAmount varchar(70),
					numAssignedBy varchar(70),
					numAssignedTo varchar(70),
					numCampainID varchar(70),
					bintAccountClosingDate varchar(70),
					txtComments varchar(70),
					lngPConclAnalysis varchar(70),
					bintCreatedDate varchar(70),
					vcOppRefOrderNo varchar(70),
					tintOppStatus varchar(70),
					intpEstimatedCloseDate varchar(70),
					--monDealAmount varchar(70),
					vcMarketplaceOrderID varchar(70),
					tintOppType varchar(70),
					numStatus varchar(70),
					monDealAmount varchar(70),
					numRecOwner varchar(70),
					vcPoppName varchar(70),
					numSalesOrPurType varchar(70),
					tintSource varchar(70),
					bitRecurred varchar(70),
					numPartner varchar(70),
					numPartenerContact varchar(70),
					dtReleaseDate varchar(70),
					numReleaseStatus varchar(70),
					intUsedShippingCompany varchar(70),
					numShipmentMethod varchar(70),
					numShippingService varchar(70),
					numPercentageComplete varchar(70),
					bintClosedDate varchar(70)
			)

		INSERT into @UFFields
		(
			tintActive,monPAmount,numAssignedBy,numAssignedTo,numCampainID,bintAccountClosingDate,txtComments,lngPConclAnalysis,bintCreatedDate,vcOppRefOrderNo,tintOppStatus
			,intpEstimatedCloseDate,vcMarketplaceOrderID,tintOppType,numStatus,monDealAmount,numRecOwner,vcPoppName,numSalesOrPurType,tintSource,bitRecurred,
			numPartner,numPartenerContact,dtReleaseDate,numReleaseStatus,intUsedShippingCompany,numShipmentMethod,numShippingService,numPercentageComplete,bintClosedDate
		)
		SELECT
			CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.OpportunityMaster'), 'tintActive', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS tintActive, 
			CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.OpportunityMaster'), 'monPAmount', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS monPAmount, 
			CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.OpportunityMaster'), 'numAssignedBy', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS numAssignedBy, 
			CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.OpportunityMaster'), 'numAssignedTo', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS numAssignedTo,
			CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.OpportunityMaster'), 'numCampainID', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS numCampainID, 
			CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.OpportunityMaster'), 'bintAccountClosingDate', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS bintAccountClosingDate, 
			CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.OpportunityMaster'), 'txtComments', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS txtComments, 
			CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.OpportunityMaster'), 'lngPConclAnalysis', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS lngPConclAnalysis, 
			CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.OpportunityMaster'), 'bintCreatedDate', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS bintCreatedDate, 
			CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.OpportunityMaster'), 'vcOppRefOrderNo', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS vcOppRefOrderNo, 
			CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.OpportunityMaster'), 'tintOppStatus', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS tintOppStatus, 
			CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.OpportunityMaster'), 'intpEstimatedCloseDate', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS intpEstimatedCloseDate, 
			CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.OpportunityMaster'), 'vcMarketplaceOrderID', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS vcMarketplaceOrderID, 
			CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.OpportunityMaster'), 'tintOppType', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS tintOppType, 
			CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.OpportunityMaster'), 'numStatus', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS numStatus,
			CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.OpportunityMaster'), 'monDealAmount', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS monDealAmount,
			CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.OpportunityMaster'), 'numRecOwner', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS numRecOwner, 
			CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.OpportunityMaster'), 'vcPoppName', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS vcPoppName, 
			CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.OpportunityMaster'), 'numSalesOrPurType', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS numSalesOrPurType, 
			CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.OpportunityMaster'), 'tintSource', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS tintSource ,
			CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.OpportunityMaster'), 'bitRecurred', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS bitRecurred,
			CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.OpportunityMaster'), 'numPartner', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS numPartner,
			CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.OpportunityMaster'), 'numPartenerContact', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS numPartenerContact,
			CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.OpportunityMaster'), 'dtReleaseDate', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS dtReleaseDate,
			CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.OpportunityMaster'), 'numReleaseStatus', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS numReleaseStatus,
			CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.OpportunityMaster'), 'intUsedShippingCompany', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS intUsedShippingCompany,
			CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.OpportunityMaster'), 'numShipmentMethod', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS numShipmentMethod,
			CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.OpportunityMaster'), 'numShippingService', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS numShippingService,
			CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.OpportunityMaster'), 'numPercentageComplete', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS numPercentageComplete,
			CHANGE_TRACKING_IS_COLUMN_IN_MASK (COLUMNPROPERTY(OBJECT_ID('dbo.OpportunityMaster'), 'bintClosedDate', 'ColumnId'),CT.SYS_CHANGE_COLUMNS) AS bintClosedDate
		FROM 
			CHANGETABLE(CHANGES dbo.OpportunityMaster, @last_synchronization_version) AS CT
		WHERE 
			ct.numOppId=@numRecordID

		--exec USP_GetWorkFlowFormFieldMaster 1,70
INSERT INTO @tblFields(FieldName,FieldValue)
		SELECT
		FieldName,
		FieldValue
		FROM
			(
				SELECT 
					tintActive,monPAmount,numAssignedBy,numAssignedTo,numCampainID,bintAccountClosingDate,txtComments,lngPConclAnalysis,bintCreatedDate,vcOppRefOrderNo,tintOppStatus
					,intpEstimatedCloseDate,vcMarketplaceOrderID,tintOppType,numStatus,monDealAmount,numRecOwner,vcPoppName,numSalesOrPurType,tintSource,bitRecurred
					,numPartner,numPartenerContact,dtReleaseDate,numReleaseStatus,intUsedShippingCompany,numShipmentMethod,numShippingService,numPercentageComplete
				FROM @UFFields	
			) AS UP
		UNPIVOT
		(
		FieldValue FOR FieldName IN (tintActive,monPAmount,numAssignedBy,numAssignedTo,numCampainID,bintAccountClosingDate,txtComments,lngPConclAnalysis,bintCreatedDate,vcOppRefOrderNo,tintOppStatus,intpEstimatedCloseDate,vcMarketplaceOrderID,tintOppType,numStatus,monDealAmount,numRecOwner,vcPoppName,numSalesOrPurType,tintSource,bitRecurred,numPartner,numPartenerContact,dtReleaseDate,numReleaseStatus,intUsedShippingCompany,numShipmentMethod,numShippingService,numPercentageComplete)
		) AS upv
		where FieldValue<>0
 

End

DECLARE @tintWFTriggerOn AS TINYINT
SET @tintWFTriggerOn=CASE @Type WHEN 'I' THEN 1 WHEN 'U' THEN 2 WHEN 'D' THEN 5 END


--DateField WorkFlow Logic
	
	Declare @TempnumOppId AS NUMERIC
	IF @tintWFTriggerOn = 1
	BEGIN
		IF EXISTS (SELECT numOppId FROM OpportunityMaster_TempDateFields WHERE numOppId = @numRecordID)
		BEGIN
			SET @TempnumOppId = (SELECT numOppId FROM [OpportunityMaster] OM WHERE  (
					(OM.bintClosedDate >= DATEADD(DAY,-30,GETUTCDATE()) AND OM.bintClosedDate <= DATEADD(DAY,90,GETUTCDATE()) )
					OR
					(OM.bintCreatedDate >= DATEADD(DAY,-30,GETUTCDATE()) AND OM.bintCreatedDate <= DATEADD(DAY,90,GETUTCDATE()) )
					OR
					(OM.intpEstimatedCloseDate >= DATEADD(DAY,-30,GETUTCDATE()) AND OM.intpEstimatedCloseDate <= DATEADD(DAY,90,GETUTCDATE()) )
					 OR
					(OM.dtReleaseDate >= DATEADD(DAY,-30,GETUTCDATE()) AND OM.dtReleaseDate <= DATEADD(DAY,90,GETUTCDATE()) )
				  )			
				AND numOppId=@numRecordID)

			IF(@TempnumOppId IS NOT NULL)
			BEGIN	
				UPDATE OMT 
				SET 
						OMT.bintCreatedDate = OM.bintCreatedDate
						,OMT.bintClosedDate = OM.bintClosedDate
						,OMT.intPEstimatedCloseDate = OM.intPEstimatedCloseDate
						,OMT.dtReleaseDate = OM.dtReleaseDate
				FROM
					OpportunityMaster_TempDateFields AS OMT
					INNER JOIN OpportunityMaster AS OM
						ON OMT.numOppId = OM.numOppId
				WHERE  (
					(OM.bintClosedDate >= DATEADD(DAY,-30,GETUTCDATE()) AND OM.bintClosedDate <= DATEADD(DAY,90,GETUTCDATE()) )
					OR
					(OM.bintCreatedDate >= DATEADD(DAY,-30,GETUTCDATE()) AND OM.bintCreatedDate <= DATEADD(DAY,90,GETUTCDATE()) )
					OR
					(OM.intpEstimatedCloseDate >= DATEADD(DAY,-30,GETUTCDATE()) AND OM.intpEstimatedCloseDate <= DATEADD(DAY,90,GETUTCDATE()) )
					 OR
					(OM.dtReleaseDate >= DATEADD(DAY,-30,GETUTCDATE()) AND OM.dtReleaseDate <= DATEADD(DAY,90,GETUTCDATE()) )
				  )			
				AND 
					OMT.numOppId = @numRecordID
			END
			ELSE
			BEGIN
				DELETE FROM OpportunityMaster_TempDateFields WHERE numOppId = @numRecordID
			END	
		END
		ELSE
		BEGIN
			INSERT INTO [OpportunityMaster_TempDateFields] (numOppId, numDomainID, bintClosedDate, bintCreatedDate,intpEstimatedCloseDate,dtReleaseDate) 
			SELECT numOppId,numDomainID, bintClosedDate, bintCreatedDate,intpEstimatedCloseDate,dtReleaseDate
			FROM OpportunityMaster
			WHERE (
					(bintClosedDate >= DATEADD(DAY,-30,GETUTCDATE()) AND bintClosedDate <= DATEADD(DAY,90,GETUTCDATE()) )
					OR
					(bintCreatedDate >= DATEADD(DAY,-30,GETUTCDATE()) AND bintCreatedDate <= DATEADD(DAY,90,GETUTCDATE()) )
					OR
					(intpEstimatedCloseDate >= DATEADD(DAY,-30,GETUTCDATE()) AND intpEstimatedCloseDate <= DATEADD(DAY,90,GETUTCDATE()) )
					 OR
					(dtReleaseDate >= DATEADD(DAY,-30,GETUTCDATE()) AND dtReleaseDate <= DATEADD(DAY,90,GETUTCDATE()) )
				  )			
				AND numOppId=@numRecordID
		END
	END
	ELSE IF @tintWFTriggerOn = 2
	BEGIN
		
		SET @TempnumOppId = (SELECT numOppId FROM [OpportunityMaster] OM WHERE  (
				(OM.bintClosedDate >= DATEADD(DAY,-30,GETUTCDATE()) AND OM.bintClosedDate <= DATEADD(DAY,90,GETUTCDATE()) )
				OR
				(OM.bintCreatedDate >= DATEADD(DAY,-30,GETUTCDATE()) AND OM.bintCreatedDate <= DATEADD(DAY,90,GETUTCDATE()) )
				OR
				(OM.intpEstimatedCloseDate >= DATEADD(DAY,-30,GETUTCDATE()) AND OM.intpEstimatedCloseDate <= DATEADD(DAY,90,GETUTCDATE()) )
				 OR
				(OM.dtReleaseDate >= DATEADD(DAY,-30,GETUTCDATE()) AND OM.dtReleaseDate <= DATEADD(DAY,90,GETUTCDATE()) )
			  )			
			AND numOppId=@numRecordID)

		IF(@TempnumOppId IS NOT NULL)
		BEGIN	
			UPDATE OMT 
			SET 
					OMT.bintCreatedDate = OM.bintCreatedDate
					,OMT.bintClosedDate = OM.bintClosedDate
					,OMT.intPEstimatedCloseDate = OM.intPEstimatedCloseDate
					,OMT.dtReleaseDate = OM.dtReleaseDate
			FROM
				OpportunityMaster_TempDateFields AS OMT
				INNER JOIN OpportunityMaster AS OM
					ON OMT.numOppId = OM.numOppId
			WHERE  (
				(OM.bintClosedDate >= DATEADD(DAY,-30,GETUTCDATE()) AND OM.bintClosedDate <= DATEADD(DAY,90,GETUTCDATE()) )
				OR
				(OM.bintCreatedDate >= DATEADD(DAY,-30,GETUTCDATE()) AND OM.bintCreatedDate <= DATEADD(DAY,90,GETUTCDATE()) )
				OR
				(OM.intpEstimatedCloseDate >= DATEADD(DAY,-30,GETUTCDATE()) AND OM.intpEstimatedCloseDate <= DATEADD(DAY,90,GETUTCDATE()) )
				 OR
				(OM.dtReleaseDate >= DATEADD(DAY,-30,GETUTCDATE()) AND OM.dtReleaseDate <= DATEADD(DAY,90,GETUTCDATE()) )
			  )			
			AND 
				OMT.numOppId = @numRecordID
		END
		ELSE
		BEGIN
			DELETE FROM OpportunityMaster_TempDateFields WHERE numOppId = @numRecordID
		END	
	END
	ELSE IF @tintWFTriggerOn = 5
	BEGIN
		DELETE FROM OpportunityMaster_TempDateFields WHERE numOppId = @numRecordID
	END	

	--DateField WorkFlow Logic


--For Fields Update Exection Point
DECLARE @Columns_Updated VARCHAR(1000)
SELECT @Columns_Updated = COALESCE(@Columns_Updated + ',', '') + FieldName FROM @tblFields 
SET @Columns_Updated=ISNULL(@Columns_Updated,'')
IF(	@Columns_Updated='monDealAmount')
BEGIN

set @tintWFTriggerOn=1
END

--select  @Columns_Updated
--select @tintWFTriggerOn
--select * from @UFFields
--Adding data As per Opertaion
	EXEC dbo.USP_ManageWorkFlowQueue
	@numWFQueueID = 0, --  numeric(18, 0)
	@numDomainID = @numDomainID, --  numeric(18, 0)
	@numUserCntID = @numUserCntID, --  numeric(18, 0)
	@numRecordID = @numRecordID, --  numeric(18, 0)
	@numFormID = 70, --  numeric(18, 0)
	@tintProcessStatus = 1, --  tinyint
	@tintWFTriggerOn = @tintWFTriggerOn, --  tinyint
	@tintMode = 1, --  tinyint
	@vcColumnsUpdated = @Columns_Updated
END
GO

