-- exec USP_GetEmbeddedCost @numEmbeddedCostID=0,@numOppBizDocID=11945,@numCostCatID=0,@numDomainID=110
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetEmbeddedCost')
DROP PROCEDURE USP_GetEmbeddedCost
GO
CREATE PROCEDURE [dbo].[USP_GetEmbeddedCost]
    @numEmbeddedCostID NUMERIC = 0,
    @numOppBizDocID NUMERIC,
    @numCostCatID NUMERIC = 0,
    @numDomainID NUMERIC
AS 
    BEGIN
    
    IF EXISTS(SELECT * FROM [EmbeddedCost] WHERE [numOppBizDocID]=@numOppBizDocID and [numCostCatID]=@numCostCatID AND [numDomainID]=@numDomainID)
    BEGIN
		SELECT  numEmbeddedCostID AS numDefaultEmbeddedCostID,
				[numEmbeddedCostID],
                [numOppBizDocID],
                numCostCatID,
                numCostCenterID,
                [numAccountID],
                [numDivisionID],
                dbo.[fn_GetComapnyName]([numDivisionID]) vcCompanyName,
                [numPaymentMethod],
                [numDomainID],
                [vcMemo],
                [monCost],
                monActualCost,
                dbo.[FormatedDateFromDate]([dtDueDate],[numDomainID]) dtDueDate,
                CASE WHEN ISNULL(EmbeddedCost.[numBizDocsPaymentDetId],0)>0 THEN (SELECT TOP 1 bitIntegrated FROM [OpportunityBizDocsPaymentDetails] WHERE [numBizDocsPaymentDetId] = EmbeddedCost.[numBizDocsPaymentDetId]) ELSE 0 END AS IsDisabled
        FROM    EmbeddedCost
        WHERE   [numOppBizDocID] = @numOppBizDocID
                AND numCostCatID = @numCostCatID
                AND numDomainID = @numDomainID
	END
	ELSE 
	BEGIN
		
		IF @numCostCatID>0 
		BEGIN
			SELECT
					0 AS numEmbeddedCostID,
					[numEmbeddedCostID] AS numDefaultEmbeddedCostID,
					numCostCatID,
					numCostCenterID,
					[numAccountID],
					[numDivisionID],
					dbo.[fn_GetComapnyName]([numDivisionID]) vcCompanyName,
					[numPaymentMethod],
					[numDomainID],
					'' [vcMemo],
					0 [monCost],
					0 monActualCost,
					dbo.[FormatedDateFromDate](GETUTCDATE(),[numDomainID]) dtDueDate,
					0 AS IsDisabled
			FROM    EmbeddedCostDefaults
			WHERE   
					numCostCatID=@numCostCatID
					AND numDomainID = @numDomainID	
		END
		ELSE 
		BEGIN
			SELECT
					EC.[numEmbeddedCostID],
					EC.numCostCatID,
					EC.numCostCenterID,
					EC.[numAccountID],
					CA.[vcAccountName],
					EC.[numDivisionID],
					dbo.[fn_GetComapnyName](EC.[numDivisionID]) vcCompanyName,
					EC.[numPaymentMethod],
					EC.[numDomainID],
					EC.[vcMemo] [vcMemo],
					EC.[monCost],
					EC.monActualCost,
					ISNULL(EC.[numBizDocsPaymentDetId],0) numBizDocsPaymentDetId,
					dbo.[FormatedDateFromDate](GETUTCDATE(),EC.[numDomainID]) dtDueDate,
					dtDueDate AS dtDueDate1,
					EC.[numBizDocsPaymentDetId],
					CASE WHEN ISNULL(EC.[numBizDocsPaymentDetId],0)>0 THEN (SELECT TOP 1 bitIntegrated FROM [OpportunityBizDocsPaymentDetails] WHERE [numBizDocsPaymentDetId] = EC.[numBizDocsPaymentDetId]) ELSE 0 END AS IsDisabled
			FROM    [EmbeddedCost] EC LEFT OUTER JOIN [Chart_Of_Accounts] CA ON CA.[numAccountId] = EC.[numAccountID]
			WHERE   
					numOppBizDocID = @numOppBizDocID
					AND EC.numDomainID = @numDomainID	
		END
	END
        
	
    END