GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_OpportunityBizDocItems_ChangePackingSlipQty')
DROP PROCEDURE USP_OpportunityBizDocItems_ChangePackingSlipQty
GO
CREATE PROCEDURE [dbo].[USP_OpportunityBizDocItems_ChangePackingSlipQty]                        
(              
	@numDomainID NUMERIC(18,0)                                
	,@numUserCntID NUMERIC(18,0)
	,@numOppId NUMERIC(18,0)
	,@numOppBizDocsId NUMERIC(18,0)
)
AS 
BEGIN
	-- CODE LEVEL TRANSACTION
	IF EXISTS (SELECT numOppBizDocsId FROM OpportunityBizDocs WHERE numOppId=@numOppId AND numBizDocId=296 AND numSourceBizDocId=@numOppBizDocsId)
	BEGIN
		DECLARE @TEMP TABLE
		(
			numOppItemID NUMERIC(18,0)
			,numOldUnitHour FLOAT
			,numNewUnitHour FLOAT
		)

		INSERT INTO @TEMP
		(
			numOppItemID
			,numOldUnitHour
			,numNewUnitHour
		)
		SELECT
			OBDI.numOppItemID
			,OBDI.numUnitHour
			,SUM(OBDIFulfillment.numUnitHour)
		FROM 
			OpportunityBizDocs OBD
		INNER JOIN
			OpportunityBizDocItems OBDI
		ON
			OBDI.numOppBizDocID=OBD.numOppBizDocsId
		INNER JOIN 
			OpportunityBizDocs OBDFulfillment 
		ON  
			OBDFulfillment.numSourceBizDocId = OBD.numOppBizDocsId
		INNER JOIN 
			OpportunityBizDocItems OBDIFulfillment 
		ON  
			OBDIFulfillment.numOppBizDocID = OBDFulfillment.numOppBizDocsId
			AND OBDIFulfillment.numOppItemID = OBDI.numOppItemID
		WHERE 
			OBD.numOppId=@numOppId
			AND OBD.numOppBizDocsId=@numOppBizDocsId
		GROUP BY
			OBDI.numOppItemID
			,OBDI.numUnitHour

		IF EXISTS (SELECT 
					OBDI.numOppItemID
				FROM 
					OpportunityBizDocs OBD
				INNER JOIN
					OpportunityBizDocItems OBDI
				ON
					OBDI.numOppBizDocID=OBD.numOppBizDocsId
				INNER JOIN 
					OpportunityBizDocs OBDFulfillment 
				ON  
					OBDFulfillment.numSourceBizDocId = OBD.numOppBizDocsId
				INNER JOIN 
					OpportunityBizDocItems OBDIFulfillment 
				ON  
					OBDIFulfillment.numOppBizDocID = OBDFulfillment.numOppBizDocsId
					AND OBDIFulfillment.numOppItemID = OBDI.numOppItemID
				WHERE 
					OBD.numOppId=@numOppId
					AND OBD.numOppBizDocsId=@numOppBizDocsId
				GROUP BY
					OBDI.numOppItemID
					,OBDI.numUnitHour
				HAVING
					OBDI.numUnitHour > ISNULL(SUM(OBDIFulfillment.numUnitHour),0))
		BEGIN
			DECLARE @tintCommitAllocation AS TINYINT
			DECLARE @bitAllocateInventoryOnPickList AS BIT

			SELECT @tintCommitAllocation=ISNULL(tintCommitAllocation,1) FROM Domain WHERE numDomainId=@numDomainID
			SELECT @bitAllocateInventoryOnPickList=ISNULL(bitAllocateInventoryOnPickList,0) FROM OpportunityMaster INNER JOIN DivisionMaster ON OpportunityMaster.numDivisionId = DivisionMaster.numDivisionID WHERE numOppID=@numOppID

			IF @numOppBizDocsId > 0 AND @tintCommitAllocation=2 AND @bitAllocateInventoryOnPickList=1
			BEGIN
				-- Revert Allocation
				EXEC USP_RevertAllocationPickList @numDomainID,@numOppID,@numOppBizDocsId,@numUserCntID
			END

			UPDATE 
				OpportunityBizDocs
			SET    
				numModifiedBy = @numUserCntID,
				dtModifiedDate = GETUTCDATE()
			WHERE  
				numOppBizDocsId = @numOppBizDocsId

			IF EXISTS (SELECT 1 FROM dbo.OpportunityMaster WHERE numOppId = @numOppID AND tintOppType=1 AND isnull(tintOppStatus,0)=1 )
			BEGIN
				EXEC 
				USP_ManageOpportunityAutomationQueue			
				@numOppQueueID = 0, -- numeric(18, 0)							
				@numDomainID = @numDomainID, -- numeric(18, 0)							
				@numOppId = @numOppID, -- numeric(18, 0)							
				@numOppBizDocsId = @numOppBizDocsId, -- numeric(18, 0)							
				@numOrderStatus = 0, -- numeric(18, 0)					
				@numUserCntID = @numUserCntID, -- numeric(18, 0)					
				@tintProcessStatus = 1, -- tinyint						
				@tintMode = 1 -- TINYINT
			END 

			UPDATE 
				OBI
			SET 
				OBI.numUnitHour = T1.numNewUnitHour
				,OBI.monPrice = OI.monPrice
				,OBI.monTotAmount = (OI.monTotAmount/OI.numUnitHour) * T1.numNewUnitHour
				,OBI.fltDiscount = (CASE WHEN OI.bitDiscountType=0 THEN OI.fltDiscount WHEN OI.bitDiscountType=1 THEN (OI.fltDiscount/OI.numUnitHour) * T1.numNewUnitHour ELSE OI.fltDiscount END)
				,OBI.monTotAmtBefDiscount = (OI.monTotAmtBefDiscount/OI.numUnitHour) * T1.numNewUnitHour 
			FROM 
				OpportunityBizDocItems OBI
			JOIN 
				OpportunityItems OI
			ON 
				OBI.numOppItemID=OI.numoppitemtCode
			INNER JOIN
				@TEMP T1
			ON
				OBI.numOppItemID = T1.numOppItemID
			WHERE 
				OI.numOppId=@numOppId 
				AND OBI.numOppBizDocID=@numOppBizDocsId
				AND OBI.numUnitHour > T1.numNewUnitHour

			DECLARE @monDealAmount as DECIMAL(20,5)
			SELECT @monDealAmount=isnull(dbo.[GetDealAmount](@numOppId,GETUTCDATE(),@numOppBizDocsId),0)

			UPDATE 
				OpportunityBizDocs 
			SET 
				monDealAmount=@monDealAmount
			WHERE  
				numOppBizDocsId = @numOppBizDocsId

			--IF @numOppBizDocsId > 0 AND @tintCommitAllocation=2 AND @bitAllocateInventoryOnPickList=1
			--BEGIN
			--	EXEC USP_PickListManageSOWorkOrder @numDomainID,@numUserCntID,@numOppID,@numOppBizDocsId
			--	-- Allocate Inventory
			--	EXEC USP_ManageInventoryPickList @numDomainID,@numOppID,@numOppBizDocsId,@numUserCntID
			--END
		END

		UPDATE 
			OBI
		SET 
			OBI.numUnitHour = 0
			,OBI.monPrice = OI.monPrice
			,OBI.monTotAmount = 0
			,OBI.fltDiscount = 0
			,OBI.monTotAmtBefDiscount = 0 
		FROM 
			OpportunityBizDocItems OBI
		JOIN 
			OpportunityItems OI
		ON 
			OBI.numOppItemID=OI.numoppitemtCode
		WHERE 
			OI.numOppId=@numOppId 
			AND OBI.numOppBizDocID=@numOppBizDocsId
			AND OBI.numOppItemID NOT IN (SELECT OBIInner.numOppItemID FROM OpportunityBizDocs OB INNER JOIN OpportunityBizDocItems OBIInner ON OB.numOppBizDocsId=OBIInner.numOppBizDocID WHERE OB.numOppId=@numOppId AND OB.numSourceBizDocId=@numOppBizDocsId)
			AND OBI.numOppItemID NOT IN (SELECT numOppItemID FROM @TEMP)

		UPDATE
			OI
		SET
			numQtyPicked = ISNULL((SELECT
										SUM(numUnitHour) 
									FROM 
										OpportunityBizDocs OB 
									INNER JOIN 
										OpportunityBizDocItems OBI 
									ON 
										OB.numOppBizDocsId=OBI.numOppBizDocID 
									WHERE 
										OB.numOppID=@numOppID 
										AND OB.numBizDocId=29397
										AND OBI.numOppItemID=OI.numOppItemtCode),0)
		FROM
			OpportunityItems OI
		WHERE
			numOppId=@numOppId
	END
END
GO

