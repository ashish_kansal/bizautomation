GO
/****** Object:  StoredProcedure [dbo].[USP_OppManage_API]    Script Date: 06/01/2009 23:49:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_OppManage_API')
DROP PROCEDURE USP_OppManage_API
GO
CREATE PROCEDURE [dbo].[USP_OppManage_API]
(                                                                          
  @numOppID as numeric=0 OUTPUT,                                                                          
  @numContactId numeric(9)=null,                                                                          
  @numDivisionId numeric(9)=null,                                                                          
  @tintSource numeric(9)=null,                                                                          
  @vcPOppName Varchar(100)='' OUTPUT,                                                                          
  @Comments varchar(1000)='',                                                                          
  @bitPublicFlag bit=0,                                                                          
  @numUserCntID numeric(9)=0,                                                                                 
  @monPAmount DECIMAL(20,5) =0,                                                 
  @numAssignedTo as numeric(9)=0,                                                                                                        
  @numDomainId numeric(9)=0,                                                                                                                                                                                   
  @strItems as text=null,                                                                          
  @strMilestone as text=null,                                                                          
  @dtEstimatedCloseDate datetime,                                                                          
  @CampaignID as numeric(9)=null,                                                                          
  @lngPConclAnalysis as numeric(9)=null,                                                                         
  @tintOppType as tinyint,                                                                                                                                             
  @tintActive as tinyint,                                                              
  @numSalesOrPurType as numeric(9),              
  @numRecurringId as numeric(9),
  @numCurrencyID as numeric(9)=0,
  @vcSource VARCHAR(50)=NULL                                                                   
)                                                                          
                                                                          
as  
 Declare @fltExchangeRate float                                 
 DECLARE @hDocItem int                                                                                                                            
 declare @TotalAmount as DECIMAL(20,5)  
 DECLARE @WebApiId AS int                                                                        
 if @numOppID = 0                                                                          
 begin                                                                          
  declare @intOppTcode as numeric(9)                           
  Select @intOppTcode=max(numOppId)from OpportunityMaster                
  set @intOppTcode =isnull(@intOppTcode,0) + 1                                                
  set @vcPOppName=@vcPOppName+ '-'+convert(varchar(4),year(getutcdate()))  +'-A'+ convert(varchar(10),@intOppTcode)                                  
  
  if @numCurrencyID=0
  BEGIN
  	 set   @fltExchangeRate=1
  	 SELECT @numCurrencyID = CASE [bitMultiCurrency] WHEN 1 THEN numCurrencyID ELSE 0 END  FROM [Domain] WHERE [numDomainId] = @numDomainID 
  END
  else set @fltExchangeRate=dbo.GetExchangeRate(@numDomainID,@numCurrencyID)

  insert into OpportunityMaster                                                                 
  (                                                                             
  numContactId,                                                                   
  numDivisionId,                                                                          
  txtComments,                                             
  numCampainID,                                                                          
  bitPublicFlag,                                                                          
  tintSource,                                                             
  vcPOppName,                                                                          
  intPEstimatedCloseDate,                                                                          
  monPAmount,                                                                           
  numCreatedBy,                                                                          
  bintCreatedDate,                                                                           
  numModifiedBy,                                                                          
  bintModifiedDate,                                                                          
  numDomainId,                                                                                                             
  numRecOwner,                                                                          
  lngPConclAnalysis,                                                                         
  tintOppType,                                                              
  numSalesOrPurType,              
  numCurrencyID,
  fltExchangeRate                                                                   
  )                                                                          
  Values                                                                          
  (                                                                          
  @numContactId,                                                                          
  @numDivisionId,                                                                          
  ISNULL(@Comments,''),
  @CampaignID,                                                                          
  @bitPublicFlag,                     
  @tintSource,                         
  @vcPOppName,                                                                          
  @dtEstimatedCloseDate,                                                                          
  @monPAmount,                                                                                
  @numUserCntID,                                                                          
  getutcdate(),                                                                          
  @numUserCntID,                                                           
  getutcdate(),                                                                          
  @numDomainId,                                                                                   
  @numUserCntID,                                                                          
  @lngPConclAnalysis,                                                                                                                                                                      
  @tintOppType,                                                                                                                                      
  @numSalesOrPurType,              
  @numCurrencyID, 
  @fltExchangeRate                                                                 
  )                                                                                                                      
  set @numOppID=scope_identity()  
  ---- insert recurring id 
  if (@numRecurringId >0)
  BEGIN
  	EXEC [dbo].[USP_RecurringAddOpportunity] @numOppID, @numRecurringId , null, 0, null,null
  END                                              
  ---- inserting Items                                                                           
                                                         
  if convert(varchar(10),@strItems) <>''                                                                          
  begin                      
   EXEC sp_xml_preparedocument @hDocItem OUTPUT, @strItems                       
      insert into                       
   OpportunityItems                                                                          
   (numOppId,numItemCode,numUnitHour,monPrice,monTotAmount,vcItemDesc,numWarehouseItmsID,vcType,bitDropShip,[vcItemName],[vcModelID],[vcManufacturer],vcPathForTImage,monVendorCost)                      
   select 
   @numOppID,
   X.numItemCode AS numItemCode,
   x.numUnitHour,x.monPrice,x.monTotAmount,X.vcItemDesc,
   CASE X.numWarehouseItmsID WHEN -1 THEN (SELECT [numWareHouseItemID] FROM [WareHouseItems] WHERE [numItemID] = X.numItemCode AND numWareHouseID IN (SELECT numDefaultWareHouseID FROM [eCommerceDTL] WHERE numDomainID =@numDomainId))  WHEN 0 THEN (SELECT TOP 1 [numWareHouseItemID] FROM [WareHouseItems] WHERE [numItemID] = X.numItemCode) ELSE  X.numWarehouseItmsID END AS numWarehouseItmsID,
   X.ItemType,X.DropShip,(SELECT vcItemName FROM item WHERE numItemCode = X.numItemCode),(SELECT vcModelID FROM item WHERE numItemCode = X.numItemCode),(SELECT vcManufacturer FROM item WHERE numItemCode = X.numItemCode),(SELECT TOP 1 vcPathForTImage FROM dbo.ItemImages II WHERE II.numItemCode = X.numItemCode AND II.numDomainId= @numDomainID AND II.bitDefault=1),
   (select isnull(VN.monCost,0) from Vendor VN INNER JOIN Item IT ON IT.numItemCode=VN.numItemCode AND VN.numVendorID=IT.numVendorID WHERE VN.numItemCode=X.numItemCode )
    from(                                                                          
   SELECT *FROM OPENXML (@hDocItem,'/NewDataSet/Item[Op_Flag=1]',2)                                                                          
   WITH                       
   (                                                                          
   numoppitemtCode numeric(9),                                     
   numItemCode numeric(9),                                                                          
   numUnitHour FLOAT,                                                                          
   monPrice DECIMAL(20,5),                                                                       
   monTotAmount DECIMAL(20,5),                                                                          
   vcItemDesc varchar(1000),                                    
   numWarehouseItmsID numeric(9),                          
   ItemType varchar(30),      
   DropShip BIT,
   WebApiId int
   ))X    
    
 SELECT TOP 1 @WebApiId=WebApiId FROM OPENXML (@hDocItem,'/NewDataSet/Item',2)                                                        
 WITH  
 (                                                        
  WebApiId int                                
 )
		IF(@WebApiId =1)  
		BEGIN
			UPDATE [WebAPIDetail] SET [vcNinthFldValue] = GETUTCDATE() WHERE [numDomainId]=@numDomainID AND [WebApiId]=@WebApiId
		END
		IF(@vcSource IS NOT NULL)
		BEGIN
			insert into OpportunityLinking(	[numParentOppID],[numChildOppID],[vcSource],[numParentOppBizDocID]) values(null,@numOppID,@vcSource,null);
		END
	
    
   Update OpportunityItems                       
   set numItemCode=X.numItemCode,    
   numOppId=@numOppID,                     
   numUnitHour=x.numUnitHour,                      
   monPrice=x.monPrice,                      
   monTotAmount=x.monTotAmount,                                  
   vcItemDesc=X.vcItemDesc,                      
   numWarehouseItmsID=X.numWarehouseItmsID,                 
   bitDropShip=X.DropShip                 
   from (SELECT numoppitemtCode as numOppItemID,numItemCode,numUnitHour,monPrice,monTotAmount,vcItemDesc,numWarehouseItmsID,DropShip  FROM OPENXML (@hDocItem,'/NewDataSet/Item[Op_Flag=2]',2)                                                                
  
    
   WITH  (                      
    numoppitemtCode numeric(9),                                     
    numItemCode numeric(9),                                                                          
    numUnitHour FLOAT,                                                                          
    monPrice DECIMAL(20,5),                                         
    monTotAmount DECIMAL(20,5),                                                                          
    vcItemDesc varchar(1000),                                    
    numWarehouseItmsID numeric(9),      
    DropShip bit                                                      
   ))X where numoppitemtCode=X.numOppItemID                                          
                                     
   insert into OppWarehouseSerializedItem                                                                          
   (numOppID,numWarehouseItmsDTLID,numOppItemID,numWarehouseItmsID)                      
   select @numOppID,X.numWarehouseItmsDTLID,X.numoppitemtCode,X.numWItmsID from(                                                                          
   SELECT * FROM OPENXML (@hDocItem,'/NewDataSet/SerialNo',2)                                                                          
   WITH                        
   (                                               
   numWarehouseItmsDTLID numeric(9),                      
   numoppitemtCode numeric(9),                      
   numWItmsID numeric(9)                                                     
   ))X                                    
               
                                 
   update OppWarehouseSerializedItem                       
   set numOppItemID=X.numoppitemtCode                                
   from                       
   (SELECT numWarehouseItmsDTLID as DTLID,O.numoppitemtCode FROM OPENXML (@hDocItem,'/NewDataSet/SerialNo',2)                                                                         
   WITH(numWarehouseItmsDTLID numeric(9),numoppitemtCode numeric(9),numWItmsID numeric(9))                                
   join OpportunityItems O on O.numWarehouseItmsID=numWItmsID and O.numOppID=@numOppID)X                 
   where numOppID=@numOppID and numWarehouseItmsDTLID=X.DTLID      
    
--   insert into OpportunityKitItems(numOppKitItem,numChildItem,intQuantity,numWareHouseItemId)    
--   SELECT * FROM OPENXML (@hDocItem,'/NewDataSet/ChildItems',2)                                                                          
--   WITH                        
--   (                                               
--   numoppitemtCode numeric(9),                      
--   numItemCode numeric(9),    
--   numQtyItemsReq integer,  
--   numWarehouseItmsID numeric(9)                                                       
--   )    
      
    
    
    
    
                              
                          
   EXEC sp_xml_removedocument @hDocItem                                     
        end          
   set @TotalAmount=0                                                           
   select @TotalAmount=sum(monTotAmount)from OpportunityItems where numOppId=@numOppID                                                                          
  update OpportunityMaster set  monPamount=@TotalAmount                                                          
  where numOppId=@numOppID                                                 
                       
                
                                                                   
  insert into OpportunityStageDetails                                                  
  (                                                                    
  numOppId,                                                                    
  numstagepercentage,                                                                    
  vcStageDetail,                                                                    
  vcComments,                            
  numDomainId,                                                                    
  numCreatedBy,                                                                    
  bintCreatedDate,                                                                    
  numModifiedBy,                                                                    
  bintModifiedDate,                                                                    
  numAssignTo,                                                                    
  bintStageComDate,                                                                    
  bintDueDate,                                                                    
  bitAlert,                                                                    
  bitStageCompleted                                                                    
  )                                                                          
  values                      
  (                      
  @numOppID,                                                                    
  100,                      
  'Deal Closed',                      
  '',                                                                    
  @numDomainId,                                                                    
  @numUserCntID,                                                                    
  getutcdate(),                                   
  @numUserCntID,                                                                    
  getutcdate(),                                                                    
  0,                                                                    
  0,                                                                    
  0,                                                                    
  0,                                                                    
  0                                                                    
  )                                                                     
  insert into OpportunityStageDetails                                  
  (                                                             
  numOppId,                                                                    
  numstagepercentage,                                                                    
  vcStageDetail,                                                                    
  vcComments,                                                                
  numDomainId,                                                                    
  numCreatedBy,                                                                    
  bintCreatedDate,                                                                    
  numModifiedBy,                                                            
  bintModifiedDate,                                                                    
  numAssignTo,                                                                  
  bintStageComDate,                                                                    
  bintDueDate,                                                                    
  bitAlert,                                                                    
  bitStageCompleted                                                                    
  )                                 
  values                      
  (                  
  @numOppID,                                                                    
  0,                                                                    
  'Deal Lost',                                 
  '',                                                                    
 @numDomainId,                                                                    
  @numUserCntID,                                                                    
  getutcdate(),                                                                    
  @numUserCntID,                                                                    
  getutcdate(),                        
   0,                                                                    
   0,                                                                
  0,                                                                    
  0,                                                                    
  0                                                                    
  )                                                                                                     
 end                       
 else                                                                                                                          
 begin                  
  --Reverting back the warehouse items                  
  declare @tintOppStatus as tinyint                   
  select @tintOppStatus=tintOppStatus from OpportunityMaster where numOppID=@numOppID                  
  if @tintOppStatus=1                   
        begin                  
   exec USP_RevertDetailsOpp @numOppID,0,0,@numUserCntID        
  end                  
                     
  update OpportunityMaster                      
  set                       
  vcPOppName=@vcPOppName,                                              
  txtComments=ISNULL(@Comments,''),                                                           
  bitPublicFlag=@bitPublicFlag,                                                                          
  numCampainID=@CampaignID,                                                                          
  tintSource=@tintSource,                                                                          
  intPEstimatedCloseDate=@dtEstimatedCloseDate,                                                                           
  numModifiedBy=@numUserCntID,                                                          
   bintModifiedDate=getutcdate(),                                                                                                                            
  lngPConclAnalysis=@lngPConclAnalysis,          
  monPAmount=@monPAmount,                                                                                                                                                                                           
  tintActive=@tintActive,                                                              
  numSalesOrPurType=@numSalesOrPurType              
  where numOppId=@numOppID               
        ---Updating if organization is assigned to someone                                                      
  declare @tempAssignedTo as numeric(9)                                                    
  set @tempAssignedTo=null                                      
  select @tempAssignedTo=isnull(numAssignedTo,0) from OpportunityMaster where numOppId=@numOppID                                                                          
  if (@tempAssignedTo<>@numAssignedTo and  @numAssignedTo<>'0')                                                    
  begin                                   
   update OpportunityMaster set numAssignedTo=@numAssignedTo ,numAssignedBy=@numUserCntID where numOppId=@numOppID                                                    
  end                                                     
  else if  (@numAssignedTo =0)                                                    
  begin                
   update OpportunityMaster set numAssignedTo=0 ,numAssignedBy=0 where numOppId = @numOppID                                                    
  end       
  ---- update recurring id 
  if (@numRecurringId >0)
  BEGIN
  	EXEC [dbo].[USP_RecurringAddOpportunity] @numOppID, @numRecurringId , null, 0, null,null
  END                                                               
  ---- Updating Items                                                                     
  if convert(varchar(10),@strItems) <>''                                                                          
  begin                                                                          
   EXEC sp_xml_preparedocument @hDocItem OUTPUT, @strItems                                      
   Delete From OppWarehouseSerializedItem WHERE numOppID=@numOppID                                           
 delete from OpportunityItems where numOppID=@numOppID and numoppitemtCode not in                       
   (SELECT numoppitemtCode FROM OPENXML (@hDocItem,'/NewDataSet/Item',2)                                                                          
   WITH  (numoppitemtCode numeric(9)))                                   
   insert into OpportunityItems                                                                          
   (numOppId,numItemCode,numUnitHour,monPrice,monTotAmount,vcItemDesc,numWarehouseItmsID,vcType,bitDropShip)                                                                          
   select 
	   @numOppID,
	   X.numItemCode AS numItemCode,
	   x.numUnitHour,x.monPrice,x.monTotAmount,X.vcItemDesc,
	   CASE X.numWarehouseItmsID WHEN 0 THEN (SELECT TOP 1 [numWareHouseItemID] FROM [WareHouseItems] WHERE [numItemID] = X.numItemCode) ELSE  X.numWarehouseItmsID END AS numWarehouseItmsID,
	   X.ItemType,X.DropShip from(                                                                          
   SELECT *FROM OPENXML (@hDocItem,'/NewDataSet/Item[Op_Flag=1]',2)                                                                          
   WITH                       
   (                                                                          
   numoppitemtCode numeric(9),                                     
   numItemCode numeric(9),                                                                          
   numUnitHour FLOAT,                                                                          
   monPrice DECIMAL(20,5),                                                                       
   monTotAmount DECIMAL(20,5),                                                                          
   vcItemDesc varchar(1000),                                    
   numWarehouseItmsID numeric(9),                          
   ItemType varchar(30),      
   DropShip BIT,
   WebApiId int
   ))X  
--   select @numOppID,X.numItemCode,x.numUnitHour,x.monPrice,x.monTotAmount,X.vcItemDesc,X.numWarehouseItmsID,X.ItemType,X.DropShip from(                                                                          
--   SELECT *FROM OPENXML (@hDocItem,'/NewDataSet/Item[Op_Flag=1]',2)                                                                          
--   WITH  (                      
--    numoppitemtCode numeric(9),                                     
--    numItemCode numeric(9),                                                                          
--    numUnitHour numeric(9),                                                                          
--    monPrice DECIMAL(20,5),                                                                       
--    monTotAmount DECIMAL(20,5),                                                                          
--    vcItemDesc varchar(1000),                                    
--    numWarehouseItmsID numeric(9),                              
--    Op_Flag tinyint,                            
--    ItemType varchar(30),      
-- DropShip bit                  
--     ))X                                     
                                  
   Update OpportunityItems                       
   set numItemCode=X.numItemCode,    
   numOppId=@numOppID,                       
   numUnitHour=x.numUnitHour,                      
   monPrice=x.monPrice,                      
   monTotAmount=x.monTotAmount,                                  
   vcItemDesc=X.vcItemDesc,                      
   numWarehouseItmsID=X.numWarehouseItmsID,                 
   bitDropShip=X.DropShip                 
   from (SELECT numoppitemtCode as numOppItemID,numItemCode,numUnitHour,monPrice,monTotAmount,vcItemDesc,numWarehouseItmsID,DropShip  FROM OPENXML (@hDocItem,'/NewDataSet/Item[Op_Flag=2]',2)                                                                
  
    
      
              
                
   WITH  (                      
    numoppitemtCode numeric(9),                                     
    numItemCode numeric(9),                                                                          
    numUnitHour FLOAT,                                                                          
    monPrice DECIMAL(20,5),                                         
    monTotAmount DECIMAL(20,5),                                                                          
    vcItemDesc varchar(1000),                                    
    numWarehouseItmsID numeric(9),      
    DropShip bit                                                      
   ))X where numoppitemtCode=X.numOppItemID                                  
                                    
         
-- delete from OpportunityKitItems where numOppKitItem in (select numoppitemtCode from OpportunityItems where numOppId=@numOppID)     
-- and numOppChildItemID not in                       
-- (SELECT numOppChildItemID FROM OPENXML (@hDocItem,'/NewDataSet/ChildItems',2)                                                                          
-- WITH  (numOppChildItemID numeric(9)))    
                             
   insert into OppWarehouseSerializedItem                                                                          
   (numOppID,numWarehouseItmsDTLID,numOppItemID,numWarehouseItmsID)                                                                   
   select @numOppID,X.numWarehouseItmsDTLID,X.numoppitemtCode,X.numWItmsID from(                                                                          
   SELECT * FROM OPENXML (@hDocItem,'/NewDataSet/SerialNo',2)                                                                          
   WITH  (                                                                          
   numWarehouseItmsDTLID numeric(9),numoppitemtCode numeric(9),numWItmsID numeric(9)                                                     
   ))X                                    
                     
   update OppWarehouseSerializedItem set numOppItemID=X.numoppitemtCode                                
   from (SELECT numWarehouseItmsDTLID as DTLID,O.numoppitemtCode FROM OPENXML (@hDocItem,'/NewDataSet/SerialNo',2)                                                                         
   WITH(numWarehouseItmsDTLID numeric(9),numoppitemtCode numeric(9),numWItmsID numeric(9))                                
   join OpportunityItems O on O.numWarehouseItmsID=numWItmsID and O.numOppID=@numOppID)X                                
   where numOppID=@numOppID and numWarehouseItmsDTLID=X.DTLID       
    
--   insert into OpportunityKitItems(numOppKitItem,numChildItem,intQuantity,numWareHouseItemId)    
--   SELECT * FROM OPENXML (@hDocItem,'/NewDataSet/ChildItems[Op_Flag=1]',2)                                                                          
--   WITH                        
--   (                                               
--   numoppitemtCode numeric(9),                      
--   numItemCode numeric(9),    
--   numQtyItemsReq integer,  
--   numWarehouseItmsID numeric(9)                                                       
--   )    
    
--   update OpportunityKitItems set numOppKitItem=X.numoppitemtCode,numWareHouseItemId=X.numWarehouseItmsID,numChildItem=X.numItemCode, intQuantity=X.numQtyItemsReq                               
--   from (SELECT numoppitemtCode,numItemCode,numQtyItemsReq,numOppChildItemID as OppChildItemID,numWarehouseItmsID FROM OPENXML (@hDocItem,'/NewDataSet/ChildItems[Op_Flag=2]',2)                                                                         
--   WITH(numOppChildItemID numeric(9),numoppitemtCode numeric(9),numItemCode numeric(9),numQtyItemsReq numeric(9),numWarehouseItmsID numeric(9)))X                                
--   where numOppChildItemID=X.OppChildItemID     
       
                                                                      
   EXEC sp_xml_removedocument @hDocItem                                               
end                                                                  
                                                                                                                               
                                             
 end
