GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetUserSMTPDetails')
DROP PROCEDURE USP_GetUserSMTPDetails
GO
CREATE PROCEDURE [dbo].[USP_GetUserSMTPDetails]                            
 @numUserID NUMERIC(9 )= 0,
 @numDomainID NUMERIC(9)
--                          
AS                            
BEGIN                            
  SELECT       
 S.[numDomainId]
      ,S.[numUserId]
      ,S.[vcSMTPUserName]
      ,S.[vcSMTPPassword]
      ,S.[vcSMTPServer]
      ,S.[numSMTPPort]
      ,S.[bitSMTPSSL]
      ,S.[bitSMTPServer]
      ,S.[bitSMTPAuth]
FROM SMTPUserDetails S JOIN Domain D on D.numDomainID =  S.numDomainID      
   WHERE  S.numUserId=(CASE WHEN ISNULL(@numUserID,0)=0 THEN -1 ELSE @numUserID END) AND S.numDomainID=@numDomainID                            
END
GO
