GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_OppWarehouseSerializedItem_Insert')
DROP PROCEDURE USP_OppWarehouseSerializedItem_Insert
GO
CREATE PROCEDURE [dbo].[USP_OppWarehouseSerializedItem_Insert]
	@numOppID AS NUMERIC(18,0),
	@numOppItemID AS NUMERIC(18,0),
	@vcSelectedItems AS VARCHAR(MAX),
	@tintItemType AS TINYINT -- 1 = Serial, 2 = Lot
AS
BEGIN
SET NOCOUNT ON;
BEGIN TRY
BEGIN TRANSACTION	

	IF @tintItemType = 1 OR @tintItemType = 2
	BEGIN

		DECLARE @TEMPSERIALLOT TABLE
		(
			ID INT IDENTITY(1,1),
			numWareHouseItmsDTLID NUMERIC(18,0),
			numWarehouseItemID NUMERIC(18,0),
			numQty INT
		)

		UPDATE 
			WIDL
		SET
			WIDL.numQty = (CASE WHEN @tintItemType = 2 THEN (ISNULL(WIDL.numQty,0) + ISNULL(OWSI.numQty,0)) ELSE 1 END)
		FROM 
			WarehouseItmsDTL WIDL
		INNER JOIN
			OppWarehouseSerializedItem OWSI
		ON
			WIDL.numWareHouseItmsDTLID = OWSI.numWarehouseItmsDTLID
			AND WIDL.numWareHouseItemID = OWSI.numWarehouseItmsID
		WHERE 
			OWSI.numOppID=@numOppID 
			AND OWSI.numOppItemID=@numOppItemID			

		DELETE FROM OppWarehouseSerializedItem WHERE numOppID=@numOppID AND numOppItemID=@numOppItemID

		IF LEN(@vcSelectedItems) > 0
		BEGIN
			INSERT INTO @TEMPSERIALLOT
			(
				numWareHouseItmsDTLID
				,numQty
			)
			SELECT 
				PARSENAME(items,2),
				PARSENAME(items,1) 
			FROM 
				dbo.Split(Replace(@vcSelectedItems, '-', '.'),',')

			UPDATE 
				TSL
			SET
				TSL.numWarehouseItemID = WID.numWareHouseItemID
			FROM
				@TEMPSERIALLOT TSL
			INNER JOIN
				WareHouseItmsDTL WID
			ON
				TSL.numWareHouseItmsDTLID = WID.numWareHouseItmsDTLID


			DECLARE @i AS INT = 1
			DECLARE @COUNT AS INT = 0
			DECLARE @numWareHouseItmsDTLID NUMERIC(18,0)
			DECLARE @numWarehouseItemID AS NUMERIC(18,0)
			DECLARE @numQty INT

			SELECT @COUNT = COUNT(*) FROM @TEMPSERIALLOT

			WHILE @i <= @COUNT
			BEGIN
				SELECT @numWareHouseItmsDTLID=numWareHouseItmsDTLID,@numQty=numQty,@numWarehouseItemID=numWarehouseItemID FROM @TEMPSERIALLOT WHERE ID = @i

				UPDATE WareHouseItmsDTL SET numQty = (CASE WHEN @tintItemType = 2 THEN (ISNULL(numQty,0) - ISNULL(@numQty,0)) ELSE 0 END) WHERE numWareHouseItmsDTLID=@numWareHouseItmsDTLID

				INSERT INTO OppWarehouseSerializedItem
				(
					numWarehouseItmsDTLID,
					numOppID,
					numOppItemID,
					numWarehouseItmsID,
					numQty
				)
				SELECT 
					@numWareHouseItmsDTLID,
					@numOppID,
					@numOppItemID,
					@numWarehouseItemID,
					@numQty 

				SET @i = @i + 1
			END
		END
	END
COMMIT
END TRY
BEGIN CATCH
	DECLARE @ErrorMessage NVARCHAR(4000)
	DECLARE @ErrorNumber INT
	DECLARE @ErrorSeverity INT
	DECLARE @ErrorState INT
	DECLARE @ErrorLine INT
	DECLARE @ErrorProcedure NVARCHAR(200)

	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;

	SELECT 
		@ErrorMessage = ERROR_MESSAGE(),
		@ErrorNumber = ERROR_NUMBER(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE(),
		@ErrorLine = ERROR_LINE(),
		@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

	RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);
END CATCH
END







