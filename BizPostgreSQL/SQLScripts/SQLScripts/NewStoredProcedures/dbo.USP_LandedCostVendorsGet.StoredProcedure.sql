GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_LandedCostVendorsGet')
DROP PROCEDURE USP_LandedCostVendorsGet
GO
CREATE PROC [dbo].[USP_LandedCostVendorsGet] 
    @numDomainId NUMERIC(18, 0)
AS 
	SELECT [numLCVendorId], LCV.[numDomainId], [numVendorId], CI.[vcCompanyName]
	FROM   [dbo].[LandedCostVendors] LCV JOIN [dbo].[DivisionMaster] AS DM
	ON lcv.[numVendorId] = DM.[numDivisionID]
	JOIN [dbo].[CompanyInfo] CI ON CI.[numCompanyId] = DM.[numCompanyID]
	WHERE LCV.numDomainId=@numDomainId


