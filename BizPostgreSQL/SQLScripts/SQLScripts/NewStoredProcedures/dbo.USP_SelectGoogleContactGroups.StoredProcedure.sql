GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_SelectGoogleContactGroups')
DROP PROCEDURE USP_SelectGoogleContactGroups
GO
CREATE PROCEDURE [dbo].[USP_SelectGoogleContactGroups]
@numDomainID AS numeric(9)=0,
@numContactId AS numeric(9)=0,
@tintType AS numeric(9)=0
AS
BEGIN

SELECT * FROM GoogleContactGroups WHERE numDomainID=@numDomainID AND numContactId=@numContactId AND tintType=@tintType
END