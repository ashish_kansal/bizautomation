GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_AddReturn')
DROP PROCEDURE USP_AddReturn
GO
/****** Object:  StoredProcedure [dbo].[USP_AddReturn]    Script Date: 01/22/2009 00:09:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[USP_AddReturn](
               @numOppItemCode       NUMERIC(18,0),
               @numOppId             NUMERIC(18,0),
               @numItemCode          NUMERIC(18,0),
               @numTotalQty          NUMERIC(18,0),
               @numQtyToReturn       NUMERIC(18,0),
               @numQtyReturned       NUMERIC(18,0)  = 0,
               @monPrice             DECIMAL(20,5),
               @numReasonForReturnID NUMERIC(18,0),
               @numReturnStatus      NUMERIC(18,0),
               @numCreateBy          NUMERIC(18,0),
			@vcReferencePO varchar(50)='')
AS
  BEGIN
    DECLARE  @numReturnID NUMERIC(18,0);
    SELECT @numReturnID = [numReturnID]
    FROM   [Returns]
    WHERE  numOppID = @numOppId
           AND numOppItemCode = @numOppItemCode --Bug fix ID 2138
    IF @numReturnID IS NULL
      BEGIN
        --insert
        PRINT 'insert'
        INSERT INTO [returns]
                   ([numoppid],
                    numOppItemCode,
                    [numitemcode],
                    [numtotalqty],
                    [numQtyToReturn],
                    [numqtyreturned],
                    [monprice],
                    [numreasonforreturnid],
                    [numreturnstatus],
                    [numcreateby],
                    [numModifiedBy],
                    [dtModifiedDate])
        VALUES     (@numOppId,
                    @numOppItemCode,
                    @numItemCode,
                    @numTotalQty,
                    @numQtyToReturn,
                    @numQtyReturned,
                    @monPrice,
                    @numReasonForReturnID,
                    @numReturnStatus,
                    @numCreateBy,
                    @numCreateBy,
                    Getdate())
        SELECT @numReturnID = SCOPE_IDENTITY()
      --        UPDATE [OpportunityItems]
      --        SET    numReturnID = @numReturnID
      --        --numUnitHour = (@numTotalQty - @numQtyReturned)
      --        WHERE numOppID = @numOppId
      --               AND [numItemCode] = @numItemCode
      END
    ELSE
      BEGIN
        --update
        PRINT 'update'
        DECLARE  @temp NUMERIC(9)
        DECLARE  @AllowedQty NUMERIC(9)
        SELECT @AllowedQty = Isnull([numTotalQty],0),
               @temp = (ISNULL([numQtyToReturn],0)+ @numQtyToReturn)
        FROM   [Returns]
        WHERE  [numReturnID] = @numReturnID
        
        IF (@temp > @AllowedQty)
          BEGIN
			RAISERROR ('NOT_ALLOWED',16,1);
            RETURN -1
          END
        ELSE
          BEGIN
            UPDATE [Returns]
            SET    [numQtyToReturn] = [numQtyToReturn]
                                        + @numQtyToReturn,
                   [numModifiedBy] = @numCreateBy,
                   [dtModifiedDate] = Getdate(),vcReferencePO=@vcReferencePO
            WHERE  [numReturnID] = @numReturnID
          END
      END
  END
