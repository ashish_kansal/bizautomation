GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ReportListMaster_PendingOrdersStatus')
DROP PROCEDURE USP_ReportListMaster_PendingOrdersStatus
GO

CREATE PROCEDURE [dbo].[USP_ReportListMaster_PendingOrdersStatus]
 @numDomainID NUMERIC(18,0)
AS
BEGIN
-- Pending to bill ---------------------------
select 
sum(Temp.Pendingtobill*Temp.AverageCost) as PendingToBill
from 
(
select 
ISNULL((isnull(OpportunityItems.numUnitHourReceived,0)) - (ISNULL((SELECT SUM(numUnitHour) FROM OpportunityBizDocItems INNER JOIN OpportunityBizDocs ON OpportunityBizDocItems.numOppBizDocID=OpportunityBizDocs.numOppBizDocsId WHERE OpportunityBizDocs.numOppId= OpportunityItems.numOppId AND OpportunityBizDocItems.numOppItemID=OpportunityItems.numoppitemtCode   AND ISNULL(bitAuthoritativeBizDocs,0)=1), 0)),0) as Pendingtobill
,isnull(Item.monAverageCost,'0') as AverageCost
FROM OpportunityMaster 
inner join DivisionMaster on OpportunityMaster.numDivisionId=DivisionMaster.numDivisionId  
inner join CompanyInfo on CompanyInfo.numCompanyid = DivisionMaster.numCompanyid  
inner join AdditionalContactsInformation on OpportunityMaster.numContactId=AdditionalContactsInformation.numContactId  
inner Join OpportunityItems on OpportunityMaster.numOppId = OpportunityItems.numOppId 
Left Join Item on OpportunityItems.numItemcode = Item.numItemcode 
Left Join Vendor on Vendor.numDomainId = @numDomainID and Vendor.numVendorID = Item.numVendorID and  Item.numItemCode= Vendor.numItemCode  
Left Join WareHouseItems  on WareHouseItems.numDomainId = @numDomainID and WareHouseItems.numWareHouseItemID = OpportunityItems.numWarehouseItmsID 
LEFT JOIN WarehouseLocation ON WarehouseLocation.numWLocationID = WareHouseItems.numWLocationID  
Left Join Warehouses on Warehouses.numDomainId = @numDomainID and Warehouses.numWareHouseID = WareHouseItems.numWareHouseID   
WHERE 1=1 
AND OpportunityMaster.numDomainId = @numDomainID 
AND (OpportunityMaster.tintshipped  in('0') AND ((OpportunityMaster.tintOppType=2 and OpportunityMaster.tintOppStatus=1)) AND OpportunityItems.numUnitHourReceived >0 ) 
) Temp
--------------------- Billed but not received-----------------------------------------

select 
--OBDI.numItemCode,OBDI.numUnitHour ,OI.numUnitHourReceived, I.monAverageCost, 
sum(
--(OBDI.numUnitHour * I.monAverageCost) - (ISNULL( OI.numUnitHourReceived,0) * I.monAverageCost) 
--(OBDI.numUnitHour - OI.numUnitHourReceived) * I.monAverageCost
(OBDI.numUnitHour - OI.numUnitHourReceived) * OI.monPrice
) 
as BilledButNotReceived

 from OpportunityMaster OM 
inner join OpportunityBizDocs OBD on OBD.numOppId = OM.numOppId
  inner join OpportunityBizDocItems OBDI on OBDI.numOppBizDocID = OBD.numOppBizDocsId 
    inner join OpportunityItems OI on OI.numOppId = OM.numOppId and OBDI.numOppItemID = OI.numoppitemtCode
left join Item I on OBDI.numItemCode = I.numItemCode
--left join WareHouseItems WI on WI.numItemID = I.numItemCode
--left join Warehouses W on W.numWareHouseID = WI.numWareHouseID
where OM.tintOppType = 2
AND OM.numdomainid = @numDomainID 
               AND  OM.tintshipped IN( '0' ) 
                     AND  OM.tintopptype = 2 
                            AND OM.tintoppstatus = 1 
					and OBD.bitAuthoritativeBizDocs = 1 
					and I.monAverageCost>0
					and (OBDI.numUnitHour - OI.numUnitHourReceived) > 0
					--and (OI.numUnitHour - isnull(OI.numQtyReceived,0))>0 -- this condition was exist in PO fulfillment SP.
					--and (OBDI.numUnitHour * isnull(I.monAverageCost,0)) - (ISNULL( OI.numUnitHourReceived,0) * isnull(I.monAverageCost,0)) > 0 
					--and W.numWareHouseID = 1115 --select for all warehouse 



------------------------------Sold But Not Shipped----------------------------------------------
select
sum(Temp3.Pendingsales) as PendingSales
from 
(
Select
isnull(OpportunityItems.numUnitHour,0) as units, 
ISNULL(ISNULL(dbo.fn_UOMConversion(ISNULL(Item.numBaseUnit, 0), OpportunityItems.numItemCode,Item.numDomainId, ISNULL(OpportunityItems.numUOMId, 0)),1) * OpportunityItems.numUnitHour,0)as ordered,
isnull(OpportunityItems.numQtyShipped,0) as shipped,
isnull(OpportunityItems.monPrice,'0') as unitprice, 
ISNULL((ISNULL(ISNULL(dbo.fn_UOMConversion(ISNULL(Item.numBaseUnit, 0), OpportunityItems.numItemCode,Item.numDomainId, ISNULL(OpportunityItems.numUOMId, 0)),1) * OpportunityItems.numUnitHour,0) -isnull(OpportunityItems.numQtyShipped,0) ) * (isnull(OpportunityItems.monPrice,'0')),0) as Pendingsales
FROM OpportunityMaster
 inner join DivisionMaster on OpportunityMaster.numDivisionId=DivisionMaster.numDivisionId  
 inner join CompanyInfo on CompanyInfo.numCompanyid = DivisionMaster.numCompanyid  
 inner join AdditionalContactsInformation on OpportunityMaster.numContactId=AdditionalContactsInformation.numContactId  
 inner Join OpportunityItems on OpportunityMaster.numOppId = OpportunityItems.numOppId 
 Left Join Item on OpportunityItems.numItemcode = Item.numItemcode 
 Left Join Vendor on Vendor.numDomainId = @numDomainID and Vendor.numVendorID = Item.numVendorID and  Item.numItemCode= Vendor.numItemCode  
 Left Join WareHouseItems  on WareHouseItems.numDomainId = @numDomainID and WareHouseItems.numWareHouseItemID = OpportunityItems.numWarehouseItmsID 
 LEFT JOIN WarehouseLocation ON WarehouseLocation.numWLocationID = WareHouseItems.numWLocationID  
 Left Join Warehouses on Warehouses.numDomainId = @numDomainID and Warehouses.numWareHouseID = WareHouseItems.numWareHouseID   
 WHERE 1=1 
 AND OpportunityMaster.numDomainId = @numDomainID AND (((OpportunityMaster.tintOppType=1 and OpportunityMaster.tintOppStatus=1)) AND Item.charItemType  in('P') AND OpportunityMaster.tintshipped  in('0') ) 
)Temp3
where Pendingsales > 0
 --ORDER BY Pendingsales desc

END
GO