SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_CustomQueryReport_LogEmailNotification')
DROP PROCEDURE dbo.USP_CustomQueryReport_LogEmailNotification
GO
CREATE PROCEDURE [dbo].[USP_CustomQueryReport_LogEmailNotification]
	@numReportID NUMERIC(18,0),
	@tintEmailFrequency TINYINT,
	@dtDateToSend DATE
AS 
BEGIN
	INSERT INTO CustomQueryReportEmail
	(
		numReportID,
		tintEmailFrequency,
		dtDateToSend,
		dtSentDate
	)
	VALUES
	(
		@numReportID,
		@tintEmailFrequency,
		@dtDateToSend,
		CAST(GETDATE() AS DATE)
	)
END