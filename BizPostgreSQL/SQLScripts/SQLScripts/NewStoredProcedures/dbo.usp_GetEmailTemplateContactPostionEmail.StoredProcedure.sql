SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_GetEmailTemplateContactPostionEmail')
DROP PROCEDURE usp_GetEmailTemplateContactPostionEmail
GO
CREATE PROCEDURE [dbo].[usp_GetEmailTemplateContactPostionEmail]                          
(                          
@GenDocId as numeric(9)=null,                
@numDomainID as numeric(9),
@numDivID AS numeric(9)                      
)                          
as       

DECLARE @vcContactPosition AS VARCHAR(200)
SELECT @vcContactPosition = ISNULL(vcContactPosition ,'')
FROM GenericDocuments where numGenericDocId=@GenDocId and numDomainID = @numDomainID
	
SELECT [A].[vcEmail],[A].[numContactId],[A].[vcFirstName],[A].[vcLastName] FROM [dbo].[DivisionMaster] AS DM JOIN AdditionalContactsInformation A ON A.numDivisionId = DM.numDivisionID  
WHERE DM.[numDomainID]=@numDomainID AND dm.[numDivisionID]=@numDivID AND A.[vcPosition] IN(SELECT * FROM dbo.Split(@vcContactPosition ,','))
AND isnull([A].[vcEmail],'')!='' AND isnull([A].[vcFirstName],'')!='' AND isnull([A].[vcLastName],'')!=''

GO



