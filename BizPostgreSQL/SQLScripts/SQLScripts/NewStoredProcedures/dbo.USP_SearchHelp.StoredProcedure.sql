GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_SearchHelp')
DROP PROCEDURE USP_SearchHelp
GO
CREATE PROCEDURE USP_SearchHelp
 @strsearch varchar(500)      
AS      
BEGIN      
 SET NOCOUNT ON;      
      
    declare @strqry varchar(2000)    
    set @strqry='select * from helpmaster where helpDescription like ''%' + @strsearch + '%'' or helpMetatag like ''%' + @strsearch + '%'''    
        
    exec (@strqry)    
    
END