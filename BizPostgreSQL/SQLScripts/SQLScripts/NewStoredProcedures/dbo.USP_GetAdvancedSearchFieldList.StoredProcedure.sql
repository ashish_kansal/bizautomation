GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_GetAdvancedSearchFieldList' ) 
    DROP PROCEDURE USP_GetAdvancedSearchFieldList
GO
CREATE PROCEDURE [dbo].[USP_GetAdvancedSearchFieldList]
    @numDomainId NUMERIC(9),
    @numFormId INT,
    @numAuthGroupId NUMERIC(9)
AS  
	--added by chintan to select Custom field form multiple location
	DECLARE @vcLocationID VARCHAR(100);SET @vcLocationID='0'
	--DECLARE @vcSectionName VARCHAR(500);SET @vcSectionName=''

	Select @vcLocationID =isnull(vcLocationID,'0') from dynamicFormMaster where numFormID=@numFormId

	--select ItemNumber as intSectionID,Item as vcSectionName from dbo.DelimitedSplit8K(@vcSectionName,',')
	select intSectionID,vcSectionName,Loc_id from DycFormSectionDetail where @numFormId=numFormId

    CREATE TABLE #tempFieldsList(ID int IDENTITY(1,1) NOT NULL,RowNo int,intSectionID int,numModuleID numeric(9),numFormFieldId  NVARCHAR(20),vcFormFieldName NVARCHAR(100),
        vcFieldType CHAR(1),vcAssociatedControlType NVARCHAR(50),numListID NUMERIC(18),vcDbColumnName NVARCHAR(100),vcPropertyName NVARCHAR(100),vcToolTip NVARCHAR(1000),
        vcListItemType CHAR(3),intColumnNum INT,intRowNum INT,boolRequired tinyint, numAuthGroupID NUMERIC(18),
          vcFieldDataType CHAR(1),boolAOIField tinyint,numFormID NUMERIC(18),vcLookBackTableName NVARCHAR(100),GRP_ID int,intFieldMaxLength int)

DECLARE @Nocolumns AS TINYINT;SET @Nocolumns = 0                

select @Nocolumns=isnull(sum(TotalRow),0)  from(            
Select count(*) TotalRow from View_DynamicColumns where numFormId=@numFormId and numAuthGroupID = @numAuthGroupId and numDomainID=@numDomainID 
Union 
Select count(*) TotalRow from View_DynamicCustomColumns where numFormId=@numFormId and numAuthGroupID = @numAuthGroupId and numDomainID=@numDomainID ) TotalRows

if @Nocolumns>0 
BEGIN

  INSERT INTO #tempFieldsList (intSectionID,numModuleID,RowNo,numFormFieldId,vcFormFieldName,vcFieldType,vcAssociatedControlType
        ,numListID,vcDbColumnName,vcPropertyName,vcToolTip,vcListItemType,intColumnNum,intRowNum,boolRequired,numAuthGroupID,
          vcFieldDataType,boolAOIField,numFormID,vcLookBackTableName,GRP_ID,intFieldMaxLength)                         
SELECT intSectionID,numModuleID,row_number() over (PARTITION BY intSectionID order by intSectionID), CONVERT(VARCHAR(15),numFieldId) + vcFieldType AS numFormFieldId,
                    ISNULL(vcCultureFieldName,vcFieldName) AS vcFormFieldName,
                    'R' as vcFieldType,
                    vcAssociatedControlType,
                    numListID,
                    vcDbColumnName,
                    vcPropertyName,
                    vcToolTip,
                    vcListItemType,
                    tintColumn as intColumnNum,
                    tintRow as intRowNum,
                    ISNULL(bitIsRequired,0) boolRequired,
                    0 numAuthGroupID,
                    vcFieldDataType,
                    0 boolAOIField,
                    numFormID,
                    vcLookBackTableName,0 as GRP_ID, intFieldMaxLength
 FROM View_DynamicColumns 
 where numFormId=@numFormId and numAuthGroupID = @numAuthGroupId and numDomainID=@numDomainID AND 1 = (CASE WHEN numFormId=29 AND numFieldID=311 THEN 0 ELSE 1 END)
       
       UNION
    
    SELECT 0 as intSectionID,0 as numModuleID,row_number() over (PARTITION BY GRP_ID order by GRP_ID),CONVERT(VARCHAR(15), numFieldId) + 'C' AS numFormFieldId,
                    vcFieldName AS vcFormFieldName,
                    vcFieldType AS vcFieldType,
                    vcAssociatedControlType,
                    numListID,
                    CONVERT(VARCHAR(15), numFieldId) + 'C' vcDbColumnName,
                    '' AS vcPropertyName,
                    '' AS vcToolTip,
                    CASE WHEN numListID > 0 THEN 'LI' ELSE '' END vcListItemType,
                   tintColumn as intColumnNum,
                    tintRow as intRowNum,
                    ISNULL(bitIsRequired,0) as boolRequired,
                    0 numAuthGroupID,
                    CASE WHEN numListID > 0 THEN 'N' ELSE 'V' END vcFieldDataType,
                    0 boolAOIField,
                    @numFormId AS numFormID,
                    '' AS vcLookBackTableName,GRP_ID,0 as intFieldMaxLength
 from View_DynamicCustomColumns
 where numFormId=@numFormId and numAuthGroupID = @numAuthGroupId and numDomainID=@numDomainID  AND GRP_ID IN(	SELECT ID FROM dbo.SplitIDs(@vcLocationID,','))

END
ELSE
BEGIN 
   INSERT INTO #tempFieldsList (intSectionID,numModuleID,RowNo,numFormFieldId,vcFormFieldName,vcFieldType,vcAssociatedControlType
        ,numListID,vcDbColumnName,vcPropertyName,vcToolTip,vcListItemType,intColumnNum,intRowNum,boolRequired,numAuthGroupID,
          vcFieldDataType,boolAOIField,numFormID,vcLookBackTableName,GRP_ID,intFieldMaxLength)                         
            SELECT intSectionID,numModuleID,row_number() over (PARTITION BY intSectionID order by intSectionID), CONVERT(VARCHAR(15),numFieldId) + vcFieldType AS numFormFieldId,
                    ISNULL(vcCultureFieldName,vcFieldName) AS vcFormFieldName,
                    'R' as vcFieldType,
                    vcAssociatedControlType,
                    numListID,
                    vcDbColumnName,
                    vcPropertyName,
                    vcToolTip,
                    vcListItemType,
                    0 intColumnNum,
                    0 intRowNum,
                    ISNULL(bitIsRequired,0) boolRequired,
                    0 numAuthGroupID,
                    vcFieldDataType,
                    0 boolAOIField,
                    numFormID,
                    vcLookBackTableName,0 as GRP_ID,intFieldMaxLength
            FROM    View_DynamicDefaultColumns
            WHERE   numFormID = @numFormId 
					AND bitDeleted=0
					AND numDomainID = @numDomainId and intSectionID!=0
					 AND 1 = (CASE WHEN numFormId=29 AND numFieldID=311 THEN 0 ELSE 1 END)
			UNION
            
			SELECT 0 as intSectionID,0 as numModuleID,row_number() over (PARTITION BY GRP_ID order by GRP_ID),CONVERT(VARCHAR(15), Fld_id) + 'C' AS numFormFieldId,
                    Fld_label AS vcFormFieldName,
                    L.vcFieldType AS vcFieldType,
                    fld_type as vcAssociatedControlType,
                    ISNULL(C.numListID, 0) numListID,
                    CONVERT(VARCHAR(15), Fld_id) + 'C' vcDbColumnName,
                    '' AS vcPropertyName,
                    '' AS vcToolTip,
                    CASE WHEN C.numListID > 0 THEN 'LI' ELSE '' END vcListItemType,
                    0 intColumnNum,
                    0 intRowNum,
                    ISNULL(V.bitIsRequired,0) as boolRequired,
                    0 numAuthGroupID,
                    CASE WHEN C.numListID > 0 THEN 'N' ELSE 'V' END vcFieldDataType,
                    0 boolAOIField,
                    @numFormId AS numFormID,
                    '' AS vcLookBackTableName,GRP_ID,0 as intFieldMaxLength
            FROM    CFW_Fld_Master C LEFT JOIN CFW_Validation V ON V.numFieldID = C.Fld_id
					JOIN CFW_Loc_Master L on C.GRP_ID=L.Loc_Id
            WHERE   C.numDomainID = @numDomainId
					AND GRP_ID IN(	SELECT ID FROM dbo.SplitIDs(@vcLocationID,','))
   
END
Update #tempFieldsList set RowNO=RowNO-1
Update #tempFieldsList set  intRowNum=(RowNo/3)+1 ,intColumnNum= (RowNo%3)+1 

SELECT * FROM #tempFieldsList ORDER BY intRowNum,intColumnNum
        
DROP TABLE #tempFieldsList
        
GO
