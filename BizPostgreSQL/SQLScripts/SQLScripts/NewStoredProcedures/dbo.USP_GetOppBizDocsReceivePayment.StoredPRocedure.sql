        
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_GetOppBizDocsReceivePayment' ) 
    DROP PROCEDURE USP_GetOppBizDocsReceivePayment
GO

CREATE PROCEDURE [dbo].[USP_GetOppBizDocsReceivePayment]
    @numDomainID NUMERIC(18, 0) ,
	@numOppID NUMERIC(18,0),
	@numOppBizDocsID NUMERIC(18,0),
	@numBillID AS NUMERIC(18,0),
	@numReturnHeaderID AS NUMERIC(18,0),
    @tintClientTimeZoneOffset as INT,
	@numLandedCostOppId AS NUMERIC(18,0)
AS 
		DECLARE @tintOppType AS TINYINT

   IF @numBillID>0
   BEGIN
   		SELECT 1 AS tintRefType,BPH.numBillPaymentID AS numReferenceID,BPH.monPaymentAmount AS monPaymentTotal,BPD.monAmount AS monApplied,/*dbo.fn_GetComapnyName(BH.numDivisionID) AS vcCompanyName,*/
   			dbo.FormatedDateFromDate(BPD.dtAppliedDate,@numDomainId ) AS dtAppliedDate
			,dbo.FormatedDateFromDate(BPH.[dtPaymentDate],@numDomainId ) AS dtPaymentDate			
   			,CASE WHEN ISNULL(numReturnHeaderID,0)>0 THEN 'Purchase Credit Applied' ELSE 'Bill Payment' END vcReference
			,CONVERT(varchar(15),CAST(BPH.[dtPaymentDate] AS TIME),100) AS dtPaymentTime
			,'-' AS vcPaymentReceiver
   		FROM dbo.BillPaymentHeader BPH JOIN dbo.BillPaymentDetails BPD ON BPH.numBillPaymentID = BPD.numBillPaymentID
   		JOIN dbo.BillHeader BH ON BPD.numBillID=BH.numBillID
   		WHERE BH.numBillID=@numBillID ORDER BY BPD.dtAppliedDate
   		
   		SELECT ISNULL(monAmountDue,0) AS monAmountTotal,ISNULL(monAmtPaid,0) AS monAmtPaid FROM BillHeader 
   		WHERE numBillID=@numBillID
   END
   ELSE IF @numReturnHeaderID>0
   BEGIN
		DECLARE @tintReturnType AS TINYINT,@tintReceiveType AS TINYINT 
		
		SELECT @tintReturnType=tintReturnType,@tintReceiveType=ISNULL(tintReceiveType,0) FROM ReturnHeader WHERE numReturnHeaderID=@numReturnHeaderID
		
		IF (@tintReturnType=1 AND @tintReceiveType=2) OR @tintReturnType=3
		BEGIN
			SELECT 2 AS tintRefType,DM.numDepositID AS numReferenceID,DM.monDepositAmount AS monPaymentTotal,DD.monAmountPaid AS monApplied,/*dbo.fn_GetComapnyName(BPH.numDivisionID) AS vcCompanyName,*/
   			dbo.FormatedDateFromDate(DD.dtCreatedDate,@numDomainId ) AS dtAppliedDate
			,dbo.FormatedDateFromDate(DD.dtCreatedDate,@numDomainId ) AS dtPaymentDate			
   			,OM.vcPOppName AS vcReference
			,CONVERT(varchar(15),CAST(DD.dtCreatedDate AS TIME),100) AS dtPaymentTime
			,isnull(dbo.fn_GetContactName(DM.numCreatedBy ),'-') AS vcPaymentReceiver
   			FROM dbo.DepositMaster DM JOIN dbo.DepositeDetails DD ON DM.numDepositID = DD.numDepositID
   			JOIN dbo.OpportunityMaster OM ON DD.numOppID=OM.numOppID
   			WHERE DM.numReturnHeaderID=@numReturnHeaderID ORDER BY DD.dtCreatedDate	
   			
   			SELECT 2 AS tintRefType,ISNULL(monDepositAmount,0) AS monAmountTotal,ISNULL(monAppliedAmount,0) AS monAmtPaid FROM DepositMaster 
   			WHERE numReturnHeaderID=@numReturnHeaderID
		END
		ELSE IF @tintReturnType=2 AND @tintReceiveType=2
		BEGIN
			SELECT 1 AS tintRefType,BPH.numBillPaymentID AS numReferenceID,BPH.monPaymentAmount AS monPaymentTotal,BPD.monAmount AS monApplied,/*dbo.fn_GetComapnyName(BPH.numDivisionID) AS vcCompanyName,*/
   			dbo.FormatedDateFromDate(BPD.dtAppliedDate,@numDomainId ) AS dtAppliedDate
			,dbo.FormatedDateFromDate(BPH.[dtPaymentDate],@numDomainId ) AS dtPaymentDate
			,CONVERT(varchar(15),CAST(BPH.[dtPaymentDate] AS TIME),100) AS dtPaymentTime
   			,CASE WHEN(BPD.numBillID>0) THEN  'Bill-' + CONVERT(VARCHAR(10),BH.numBillID) ELSE OM.vcPOppName END AS vcReference
			,'-' AS vcPaymentReceiver
   			FROM dbo.BillPaymentHeader BPH JOIN dbo.BillPaymentDetails BPD ON BPH.numBillPaymentID = BPD.numBillPaymentID
   			LEFT JOIN dbo.OpportunityBizDocs OBD ON BPD.numOppBizDocsID = OBD.numOppBizDocsId
            LEFT JOIN dbo.OpportunityMaster OM ON OBD.numOppId = OM.numOppId
            LEFT JOIN dbo.BillHeader BH ON BH.numBillID = BPD.numBillID
   			WHERE BPH.numReturnHeaderID=@numReturnHeaderID	ORDER BY dtAppliedDate
   			
   			SELECT 1 AS tintRefType,ISNULL(monPaymentAmount,0) AS monAmountTotal,ISNULL(monAppliedAmount,0) AS monAmtPaid FROM BillPaymentHeader 
   			WHERE numReturnHeaderID=@numReturnHeaderID
		END
   END
   ELSE IF @numOppID>0 AND @numOppBizDocsID>0
   BEGIN
		
		SELECT @tintOppType=tintOppType FROM OpportunityMaster WHERE numOppId=@numOppId
		
		IF @tintOppType=1
		BEGIN
			SELECT 2 AS tintRefType,DM.numDepositID AS numReferenceID,DM.monDepositAmount AS monPaymentTotal,DD.monAmountPaid AS monApplied,/*dbo.fn_GetComapnyName(BPH.numDivisionID) AS vcCompanyName,*/
   			dbo.FormatedDateFromDate(DD.dtCreatedDate,@numDomainId ) AS dtAppliedDate
			,dbo.FormatedDateFromDate(DD.dtCreatedDate,@numDomainId ) AS dtPaymentDate
				,CONVERT(varchar(15),CAST(DD.dtCreatedDate AS TIME),100) AS dtPaymentTime
   			,CASE WHEN tintDepositePage=3 THEN 'Credit Applied' ELSE 'Payment Received' END vcReference
			,isnull(dbo.fn_GetContactName(DM.numCreatedBy ),'-') AS vcPaymentReceiver
			,DD.numReceivedFrom 
   			FROM dbo.DepositMaster DM JOIN dbo.DepositeDetails DD ON DM.numDepositID = DD.numDepositID
   			WHERE DD.numOppID=@numOppID AND DD.numOppBizDocsID=@numOppBizDocsID ORDER BY DD.dtCreatedDate	

			--DECLARE @TEMPTable TABLE
			--(
			--	ID INT IDENTITY(1,1)
			--	,tintRefType TINYINT
			--	,numReferenceID NUMERIC(18,0)
			--	,monAmountPaid DECIMAL(20,5)
			--	,monTotalAmount DECIMAL(20,5)
			--	,dtPaymentDate DATETIME
			--	,vcReference VARCHAR(20)
			--	,numTempOrigDepositID UNIQUEIDENTIFIER
			--	,vcCredit VARChAR(2000)
			--)

			--INSERT INTO 
			--	@TEMPTable
			--SELECT 
			--	2 AS tintRefType
			--	,DM.numDepositID
			--	,DD.monAmountPaid
			--	,DD.monAmountPaid
			--	,DD.dtCreatedDate
			--	,CASE WHEN tintDepositePage=3 THEN 'Credit Applied' ELSE 'Payment Received' END vcReference
			--	,DD.numTempOrigDepositID
			--	,''
			--FROM 
			--	dbo.DepositMaster DM 
			--JOIN 
			--	dbo.DepositeDetails DD 
			--ON 
			--	DM.numDepositID = DD.numDepositID
			--WHERE 
			--	DD.numOppID=@numOppID 
			--	AND DD.numOppBizDocsID=@numOppBizDocsID 
			--	AND tintDepositePage <> 3
			--ORDER BY 
			--	DD.dtCreatedDate	

			--UPDATE
			--	t1
			--SET
			--	t1.vcCredit = t2.vcCredit
			--	,t1.monTotalAmount = ISNULL(monTotalAmount,0) + monTotalCreditAmount
			--FROM
			--	@TEMPTable t1
			--INNER JOIN
			--(
			--	SELECT
			--		t3.numReferenceID
			--		,SUM(DepositeDetails.monAmountPaid) monTotalCreditAmount
			--		,stuff((
			--				SELECT 
			--					CONCAT(', <a href="javascript:void(0)" onclick="return OpenPaymentAmtPaid(''2'',', DepositeDetails.numDepositID ,');">',DepositeDetails.monAmountPaid,(CASE WHEN ReturnHeader.numReturnHeaderID IS NOT NULL AND LEN(vcRMA ) > 0 THEN CONCAT(' (',ReturnHeader.vcRMA,')') ELSE '' END),'</a>')
			--				FROM 
			--					@TEMPTable t3
			--				INNER JOIN
			--					DepositeDetails
			--				ON
			--					t3.numTempOrigDepositID = DepositeDetails.numTempOrigDepositID
			--				INNER JOIN
			--					DepositMaster
			--				ON
			--					DepositeDetails.numDepositID = DepositMaster.numDepositId
			--				LEFT JOIN 
			--					ReturnHeader 
			--				ON 
			--					DepositMaster.numReturnHeaderID=ReturnHeader.numReturnHeaderID
			--				WHERE
			--					DepositMaster.tintDepositePage = 3
			--				FOR XML PATH('')
			--	   ),1,1,'') as vcCredit
			--	FROM
			--		@TEMPTable t3
			--	INNER JOIN
			--		DepositeDetails
			--	ON
			--		t3.numTempOrigDepositID = DepositeDetails.numTempOrigDepositID
			--	INNER JOIN
			--		DepositMaster
			--	ON
			--		DepositeDetails.numDepositID = DepositMaster.numDepositId
			--	WHERE
			--		DepositMaster.tintDepositePage = 3
			--	GROUP BY
			--		t3.numReferenceID
			--) t2
			--ON
			--	t1.numReferenceID = t2.numReferenceID



			--INSERT INTO 
			--	@TEMPTable
			--SELECT 
			--	2 AS tintRefType
			--	,DM.numDepositID
			--	,0
			--	,DD.monAmountPaid
			--	,DD.dtCreatedDate
			--	,CASE WHEN tintDepositePage=3 THEN 'Credit Applied' ELSE 'Payment Received' END vcReference
			--	,DD.numTempOrigDepositID
			--	,CONCAT(DD.monAmountPaid,(CASE WHEN RH.numReturnHeaderID IS NOT NULL AND LEN(vcRMA ) > 0 THEN CONCAT(' (',RH.vcRMA,')') ELSE '' END))
			--FROM 
			--	dbo.DepositMaster DM 
			--JOIN 
			--	dbo.DepositeDetails DD 
			--ON 
			--	DM.numDepositID = DD.numDepositID
			--LEFT JOIN 
			--	ReturnHeader RH
			--ON 
			--	DM.numReturnHeaderID=RH.numReturnHeaderID
			--WHERE 
			--	DD.numOppID=@numOppID 
			--	AND DD.numOppBizDocsID=@numOppBizDocsID 
			--	AND tintDepositePage = 3
			--	AND (numTempOrigDepositID IS NULL OR numTempOrigDepositID NOT IN (SELECT numTempOrigDepositID FROM @TEMPTable))
			--ORDER BY 
			--	DD.dtCreatedDate

			--SELECT 
			--	tintRefType
			--	,numReferenceID
			--	,monAmountPaid
			--	,monTotalAmount
			--	,dbo.FormatedDateFromDate(dtPaymentDate,@numDomainID) AS dtPaymentDate
			--	,vcReference
			--	,numTempOrigDepositID
			--	,vcCredit 
			--FROM 
			--	@TEMPTable
			--ORDER BY
			--	dtPaymentDate	
   		END
		ELSE
		BEGIN
			SELECT 1 AS tintRefType,BPH.numBillPaymentID AS numReferenceID,BPH.monPaymentAmount AS monPaymentTotal,BPD.monAmount AS monApplied,/*dbo.fn_GetComapnyName(BPH.numDivisionID) AS vcCompanyName,*/
   			dbo.FormatedDateFromDate(BPD.dtAppliedDate,@numDomainId ) AS dtAppliedDate
			,dbo.FormatedDateFromDate(BPH.[dtPaymentDate],@numDomainId ) AS dtPaymentDate
			,CONVERT(varchar(15),CAST(BPH.[dtPaymentDate] AS TIME),100) AS dtPaymentTime
   			,CASE WHEN ISNULL(numReturnHeaderID,0)>0 THEN 'Purchase Credit Applied' ELSE 'Bill Payment' END + 
   			CASE WHEN ISNULL(CH.numCheckNo,0)>0 THEN ' (Check # : ' + CAST(CH.numCheckNo AS VARCHAR(18)) + ')'  ELSE '' END AS vcReference
			,'-' AS vcPaymentReceiver
   			FROM dbo.BillPaymentHeader BPH JOIN dbo.BillPaymentDetails BPD ON BPH.numBillPaymentID = BPD.numBillPaymentID
   			LEFT JOIN dbo.CheckHeader CH ON BPH.numBillPaymentID=CH.numReferenceID AND CH.tintReferenceType=8 AND CH.numDomainID=@numDomainID
   			WHERE BPD.numOppBizDocsID=@numOppBizDocsID ORDER BY BPD.dtAppliedDate
   		END
   		 
   		 SELECT tintOppType AS tintRefType,ISNULL(OBD.monDealAmount,0) AS monAmountTotal,ISNULL(OBD.monAmountPaid,0) AS monAmtPaid,numDivisionId 
   		 FROM OpportunityBizDocs OBD JOIN OpportunityMaster OM ON OBD.numOppID=OM.numOppID
   			WHERE OM.numOppID=@numOppID AND numOppBizDocsID=@numOppBizDocsID --AND ISNULL(OBD.bitAuthoritativeBizDocs,0)=1
	END	
	ELSE IF @numOppID>0 
    BEGIN
		SELECT @tintOppType=tintOppType FROM OpportunityMaster WHERE numOppId=@numOppId
		
		IF @tintOppType=1
		BEGIN
		SELECT 2 AS tintRefType,DM.numDepositID AS numReferenceID,DM.monDepositAmount AS monPaymentTotal,DD.monAmountPaid AS monApplied,/*dbo.fn_GetComapnyName(BPH.numDivisionID) AS vcCompanyName,*/
   			dbo.FormatedDateFromDate(DD.dtCreatedDate,@numDomainId ) AS dtAppliedDate
			,dbo.FormatedDateFromDate(DD.dtCreatedDate,@numDomainId ) AS dtPaymentDate
				,CONVERT(varchar(15),CAST(DD.dtCreatedDate AS TIME),100) AS dtPaymentTime
   			,CASE WHEN tintDepositePage=3 THEN 'Credit Applied' ELSE 'Payment Received' END vcReference
			,isnull(dbo.fn_GetContactName(DM.numCreatedBy ),'-') AS vcPaymentReceiver
   			FROM dbo.DepositMaster DM JOIN dbo.DepositeDetails DD ON DM.numDepositID = DD.numDepositID
   			WHERE DD.numOppID=@numOppID ORDER BY DD.dtCreatedDate	
   		END
		ELSE
		BEGIN
			SELECT 1 AS tintRefType,BPH.numBillPaymentID AS numReferenceID,BPH.monPaymentAmount AS monPaymentTotal,BPD.monAmount AS monApplied,/*dbo.fn_GetComapnyName(BPH.numDivisionID) AS vcCompanyName,*/
   			dbo.FormatedDateFromDate(BPD.dtAppliedDate,@numDomainId ) AS dtAppliedDate
			,dbo.FormatedDateFromDate(BPH.[dtPaymentDate],@numDomainId ) AS dtPaymentDate
			,CONVERT(varchar(15),CAST(BPH.[dtPaymentDate] AS TIME),100) AS dtPaymentTime
			,CASE WHEN ISNULL(numReturnHeaderID,0)>0 THEN 'Purchase Credit Applied' ELSE 'Bill Payment' END + 
   			CASE WHEN ISNULL(CH.numCheckNo,0)>0 THEN ' (Check # : ' + CAST(CH.numCheckNo AS VARCHAR(18)) + ')'  ELSE '' END  AS vcReference
			,'-' AS vcPaymentReceiver
   			FROM dbo.BillPaymentHeader BPH JOIN dbo.BillPaymentDetails BPD ON BPH.numBillPaymentID = BPD.numBillPaymentID
   			LEFT JOIN dbo.CheckHeader CH ON BPH.numBillPaymentID=CH.numReferenceID AND CH.tintReferenceType=8 AND CH.numDomainID=@numDomainID
   			WHERE BPD.numOppBizDocsID IN (SELECT numOppBizDocsID FROM dbo.OpportunityBizDocs WHERE numOppID=@numOppID) ORDER BY BPD.dtAppliedDate
   		END
   		 
   		 SELECT tintOppType AS tintRefType,ISNULL(OBD.monDealAmount,0) AS monAmountTotal,ISNULL(OBD.monAmountPaid,0) AS monAmtPaid,numDivisionId 
   		 FROM OpportunityBizDocs OBD JOIN OpportunityMaster OM ON OBD.numOppID=OM.numOppID
   			WHERE OM.numOppID=@numOppID AND ISNULL(OBD.bitAuthoritativeBizDocs,0)=1
	END
	ELSE IF @numLandedCostOppId >0
	BEGIN
	SELECT 1 AS tintRefType,BPH.numBillPaymentID AS numReferenceID,BPH.monPaymentAmount AS monPaymentTotal,BPD.monAmount AS monApplied,/*dbo.fn_GetComapnyName(BH.numDivisionID) AS vcCompanyName,*/
   			dbo.FormatedDateFromDate(BPD.dtAppliedDate,@numDomainId ) AS dtAppliedDate
			,dbo.FormatedDateFromDate(BPH.[dtPaymentDate],@numDomainId ) AS dtPaymentDate
			,CONVERT(varchar(15),CAST(BPH.[dtPaymentDate] AS TIME),100) AS dtPaymentTime
   			,CASE WHEN ISNULL(numReturnHeaderID,0)>0 THEN 'Purchase Credit Applied' ELSE 'Bill Payment' END vcReference
			,'-' AS vcPaymentReceiver
   		FROM dbo.BillPaymentHeader BPH JOIN dbo.BillPaymentDetails BPD ON BPH.numBillPaymentID = BPD.numBillPaymentID
   		JOIN dbo.BillHeader BH ON BPD.numBillID=BH.numBillID
   		WHERE BH.[numOppId]=@numLandedCostOppId ORDER BY BPD.dtAppliedDate
   		
   		SELECT SUM(ISNULL(monAmountDue,0)) AS monAmountTotal,SUM(ISNULL(monAmtPaid,0)) AS monAmtPaid FROM BillHeader 
   		WHERE [BillHeader].[numOppId]=@numLandedCostOppId
	END
				
				