GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_GetEmailTemplateByCode' ) 
    DROP PROCEDURE USP_GetEmailTemplateByCode
GO
CREATE PROCEDURE USP_GetEmailTemplateByCode
    @vcTemplateCode VARCHAR(50),
    @numDomainID NUMERIC
AS 
BEGIN
    SELECT  numGenericDocID,
            VcFileName,
            vcDocName,
            numDocCategory,
            numDocStatus,
            vcDocDesc,
            vcSubject,
            tintDocumentType,
            numModuleID
    FROM    dbo.GenericDocuments
    WHERE   numDomainID = @numDomainID
            AND numDocCategory = 369
            AND VcFileName = @vcTemplateCode
	
END