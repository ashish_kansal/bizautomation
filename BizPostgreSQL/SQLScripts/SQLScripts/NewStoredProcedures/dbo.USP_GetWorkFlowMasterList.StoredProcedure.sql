SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetWorkFlowMasterList')
DROP PROCEDURE USP_GetWorkFlowMasterList
GO
CREATE PROCEDURE [dbo].[USP_GetWorkFlowMasterList]      
@numDomainId as numeric(9),  
@numFormID NUMERIC(18,0), 
@CurrentPage int,                                                        
@PageSize int,                                                        
@TotRecs int output,     
@SortChar char(1)='0' ,                                                       
@columnName as Varchar(50),                                                        
@columnSortOrder as Varchar(50)  ,
@SearchStr  as Varchar(50)   
    
as       
    SET NOCOUNT ON
     
Create table #tempTable ( ID INT IDENTITY PRIMARY KEY,                                                         
      numWFID NUMERIC(18,0)                                                       
 )     
declare @strSql as varchar(8000)                                                  
    
set  @strSql='Select numWFID from WorkFlowMaster where numdomainid='+ convert(varchar(15),@numDomainID) 
    
if @SortChar<>'0' set @strSql=@strSql + ' And vcWFName like '''+@SortChar+'%'''     

if @numFormID <> 0 set @strSql=@strSql + ' And numFormID = '+ convert(varchar(15),@numFormID) +''   

if @SearchStr<>'' set @strSql=@strSql + ' And (vcWFName like ''%'+@SearchStr+'%'' or 
vcWFDescription like ''%'+@SearchStr+'%'') ' 
    
set  @strSql=@strSql +'ORDER BY ' + @columnName +' '+ @columnSortOrder    
    
    PRINT @strSql
insert into #tempTable(numWFID) exec(@strSql)    
    
 declare @firstRec as integer                                                        
 declare @lastRec as integer                                                        
 set @firstRec= (@CurrentPage-1) * @PageSize                                                        
 set @lastRec= (@CurrentPage*@PageSize+1)                                                         

 SELECT  @TotRecs = COUNT(*)  FROM #tempTable	
	
Select ROW_NUMBER()  OVER (ORDER BY ID ASC) AS 'RowNo', WM.numWFID,WM.numDomainID,WM.vcWFName,WM.vcWFDescription,dbo.fn_StripHTML(WM.vcWFAction) AS vcWFAction ,CASE WHEN wm.bitActive=1 THEN 'Active' ELSE 'InActive' END AS Status,WM.numCreatedBy,
		WM.vcDateField,WM.intDays,WM.intActionOn,WM.numModifiedBy,WM.dtCreatedDate,WM.dtModifiedDate,WM.bitActive,WM.numFormID,WM.tintWFTriggerOn,DFM.vcFormName,CASE WHEN (WM.tintWFTriggerOn=1 AND wm.intDays=0)THEN 'Create' WHEN WM.tintWFTriggerOn=2 THEN 'Edit' WHEN WM.tintWFTriggerOn=3 THEN 'Date Field' WHEN WM.tintWFTriggerOn=4 THEN 'Fields Update' WHEN WM.tintWFTriggerOn=5 THEN 'Delete' WHEN WM.tintWFTriggerOn=6 THEN 'A/R Aging' ELSE 'NA' end AS TriggeredOn
from WorkFlowMaster WM join #tempTable T on T.numWFID=WM.numWFID
JOIN DynamicFormMaster DFM ON WM.numFormID=DFM.numFormID AND DFM.tintFlag=3
   WHERE ID > @firstRec and ID < @lastRec order by ID
   
   DROP TABLE #tempTable
GO
