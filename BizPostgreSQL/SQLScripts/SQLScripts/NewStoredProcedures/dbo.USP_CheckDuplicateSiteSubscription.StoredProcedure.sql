GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_CheckDuplicateSiteSubscription')
DROP PROCEDURE USP_CheckDuplicateSiteSubscription
GO
/****** Object:  StoredProcedure [dbo].[USP_CheckDuplicateSiteSubscription]    Script Date: 08/08/2009 16:34:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Anoop Krishnan
-- Create date: 28-Jul-2009
-- Description:	To check duplicate subscriptions
-- =============================================
CREATE PROCEDURE [dbo].[USP_CheckDuplicateSiteSubscription] 
	@numSiteID numeric(18) = 0, 
	@numDomainID numeric(18) = 0,
	@vcEmail VARCHAR(50)
AS
BEGIN
	SET NOCOUNT ON;
	
	SELECT SSD.[numSubscriberID] 
	FROM [SiteSubscriberDetails] SSD
	INNER JOIN [AdditionalContactsInformation] ACI
	ON SSD.[numContactId] = ACI.[numContactId]
	WHERE ACI.[vcEmail] = @vcEmail
		AND ACI.[numDomainID] = @numDomainID
		AND SSD.[numSiteID] = @numSiteID
END
