
GO
/****** Object:  StoredProcedure [dbo].[usp_RemoveStagePercentageDetails]    Script Date: 09/24/2010 15:19:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_ManageStageAccessDetail')
DROP PROCEDURE usp_ManageStageAccessDetail
GO
CREATE PROCEDURE [dbo].[usp_ManageStageAccessDetail]
    @numProjectID NUMERIC = 0,
    @numOppID NUMERIC = 0,
	@numStageDetailsId NUMERIC = 0,
    @numContactId NUMERIC = 0,
	@tintMode TINYINT=-1 
AS 

if @tintMode=0 
BEGIN
	if EXISTS(select 1 from StageAccessDetail where numContactId=@numContactId and
			numProjectID=@numProjectID and numOppID=@numOppID)
	Begin
		Update StageAccessDetail set numStageDetailsId=@numStageDetailsId
	END
	Else
		Begin
		 insert into StageAccessDetail(numProjectID,numOppID,numStageDetailsId
      ,numContactId) values (@numProjectID,@numOppID,@numStageDetailsId,@numContactId)
		END
END
Else
BEGIN
   select numStageDetailsId from StageAccessDetail where numContactId=@numContactId and
	 numProjectID=@numProjectID and numOppID=@numOppID
END