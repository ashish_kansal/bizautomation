


GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetOnlineBillPayeeList')
	DROP PROCEDURE USP_GetOnlineBillPayeeList
GO

CREATE PROCEDURE [dbo].[USP_GetOnlineBillPayeeList]
	@numDomainID numeric(18, 0),
	@numBankDetailId numeric(18, 0)
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[numPayeeDetailId],
	[vcPayeeFIId],
	[vcPayeeFIListID],
	[vcPayeeName],
	[vcAccount],
	[vcAddress1],
	[vcAddress2],
	[vcAddress3],
	[vcCity],
	[vcState],
	[vcPostalCode],
	[vcPhone],
	[vcCountry] 
FROM
	[dbo].[OnlineBillPayeeDetails] OBP 
	INNER JOIN dbo.BankDetails BD ON BD.numBankDetailID = OBP.numBankDetailID
WHERE
	 BD.numDomainID = @numDomainID AND BD.numBankDetailID = @numBankDetailId 
	AND BD.bitIsActive = 1 


GO
