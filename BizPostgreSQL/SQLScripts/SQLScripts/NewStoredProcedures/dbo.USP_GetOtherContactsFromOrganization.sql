GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--modified by chintan
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetOtherContactsFromOrganization')
DROP PROCEDURE USP_GetOtherContactsFromOrganization
GO
CREATE PROCEDURE [dbo].[USP_GetOtherContactsFromOrganization]
(
	@numDomainID NUMERIC(18,0)
	,@tintMode TINYINT
	,@vcContactIds VARCHAR(MAX)
)
AS
BEGIN
	SELECT STUFF((SELECT 
					CONCAT(' <li><input type="checkbox" onchange="return OtherContactSelectionChanged(',@tintMode,',this);" id="', CONCAT(ADC.vcEmail,'~',ADC.numContactID,'~',ADC.vcFirstName,'~',ADC.vcLastName),'" />&nbsp;&nbsp;<label style="font-weight:normal;">',CONCAT(ISNULL(ADC.vcFirstName,'-'),' ',ISNULL(ADC.vcLastName,'-'),', ',ADC.vcEmail,', ',C.vcCompanyName,(CASE WHEN LD.vcData IS NOT NULL THEN CONCAT(' (',LD.vcData,')') ELSE '' END)),'</label></li>')
				FROM
					AdditionalContactsInformation ADC
				INNER JOIN 
					dbo.DivisionMaster DM 
				ON 
					ADC.numDivisionId = DM.numDivisionID 
				INNER JOIN 
					dbo.CompanyInfo C 
				ON
					DM.numCompanyID = C.numCompanyId 
				LEFT JOIN 
					dbo.ListDetails LD 
				ON 
					LD.numListItemID=numCompanyType
				WHERE
					ADC.numDomainID = @numDomainID
					AND ADC.numDivisionId IN (SELECT numDivisionId FROM AdditionalContactsInformation ACI WHERE ACI.numDomainID=@numDomainID AND ACI.numContactId IN (SELECT Id FROM SplitIDs(@vcContactIds,',')))
					AND ADC.numContactId NOT IN (SELECT Id FROM SplitIDs(@vcContactIds,','))
					AND LEN(ISNULL(ADC.vcEmail,'')) > 0
				ORDER BY
					ADC.vcFirstName
				FOR XML PATH(''), TYPE).value('.', 'varchar(max)'),1,1,'')
END
GO