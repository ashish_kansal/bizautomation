
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetOppSerialLotNumber')
DROP PROCEDURE USP_GetOppSerialLotNumber
GO
CREATE PROCEDURE [dbo].[USP_GetOppSerialLotNumber]
	@numOppID AS NUMERIC(18,0),
	@numOppItemID AS NUMERIC(18,0),
	@numWarehouseItemID AS NUMERIC(18,0)
AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @numDomainID AS NUMERIC(18,0)
	DECLARE @numItemID NUMERIC(18,0)
	DECLARE @numWarehouseID NUMERIC(18,0)
	SELECT @numDomainID= numDomainId FROM OpportunityMaster WHERE numOppId = @numOppID
	SELECT @numItemID=numItemID,@numWarehouseID=numWarehouseID FROM WarehouseItems WHERE numWarehouseItemID=@numWarehouseItemID

	-- Available Serail Lot Numbers
	SELECT
		ROW_NUMBER() OVER(ORDER BY dExpirationDate DESC) AS OrderNo,
		numWareHouseItmsDTLID,
		vcSerialNo,
		numQty,
		dbo.FormatedDateFromDate(dExpirationDate,@numDomainID) AS dExpirationDate
	FROM
		WareHouseItmsDTL
	WHERE
		numWareHouseItemID IN (SELECT numWareHouseItemID FROM WareHouseItems WHERE numItemID=@numItemID AND numWareHouseID=@numWarehouseID)
		AND ISNULL(numQty,0) > 0 
	ORDER BY
		dExpirationDate DESC

    -- Selected Serail Lot Numbers
	SELECT
		ROW_NUMBER() OVER(ORDER BY WareHouseItmsDTL.dExpirationDate DESC) AS OrderNo,
		WareHouseItmsDTL.numWareHouseItmsDTLID,
		WareHouseItmsDTL.vcSerialNo,
		dbo.FormatedDateFromDate(WareHouseItmsDTL.dExpirationDate,@numDomainID) AS dExpirationDate,
		OppWarehouseSerializedItem.numQty,
		OppWarehouseSerializedItem.numOppBizDocsId
	FROM
		OppWarehouseSerializedItem
	JOIN
		WareHouseItmsDTL
	ON
		OppWarehouseSerializedItem.numWarehouseItmsDTLID = WareHouseItmsDTL.numWareHouseItmsDTLID
	WHERE
		numOppID = @numOppID AND
		numOppItemID = @numOppItemID
	ORDER BY
		WareHouseItmsDTL.dExpirationDate DESC

END



