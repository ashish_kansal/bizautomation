SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ManageReviewDetail')
DROP PROCEDURE USP_ManageReviewDetail
GO
CREATE PROCEDURE [dbo].[USP_ManageReviewDetail]   

@numReviewId as numeric(9)=0,    
@numContactId AS NUMERIC(18,0) ,
@bitAbuse AS BIT ,
@bitHelpful AS BIT ,
@Mode AS INT 

AS

BEGIN
		IF NOT EXISTS(SELECT * FROM ReviewDetail WHERE numReviewId = @numReviewId AND numContactId = @numContactId)
			BEGIN
					INSERT INTO ReviewDetail
					VALUES
					(
					  @numReviewId ,
					  @bitAbuse ,
					  @bitHelpful ,
					  @numContactId
					)			
			END
		ELSE
		    BEGIN
				IF @Mode = 0
				   BEGIN
				   	     UPDATE ReviewDetail SET  bitAbuse = @bitAbuse WHERE numContactId = @numContactId AND numReviewId = @numReviewId
				   END	
				ELSE IF @Mode = 1
				   BEGIN
				   	     UPDATE ReviewDetail SET  bitHelpful = @bitHelpful WHERE numContactId = @numContactId AND numReviewId = @numReviewId
				   END
			END	
END

	
--SELECT AVG(CONVERT(NUMERIC (9,0) , intRatingCount)) FROM Ratings
--UPDATE Ratings SET intRatingCount = 4 WHERE numRatingId = 1
--SELECT * FROM ReviewDetail

--INSERT INTO Ratings VALUES(2,'19017',1,20,'255.124.13.1',92,1 ,2012-10-03)