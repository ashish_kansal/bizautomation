SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_OpportunityItemsReleaseDates_UpdateItemReleaseStatus')
DROP PROCEDURE USP_OpportunityItemsReleaseDates_UpdateItemReleaseStatus
GO
CREATE PROCEDURE [dbo].[USP_OpportunityItemsReleaseDates_UpdateItemReleaseStatus] 
(
	@ID NUMERIC(18,0),
	@numDomainID NUMERIC(18,0),
	@numOppID NUMERIC(18,0),
	@numOppItemID NUMERIC(18,0),
	@dtReleaseDate DATE,
	@numQuantity NUMERIC(18,0),
	@tintReleaseStatus TINYINT,
	@numPurchasedOrderID NUMERIC(18,0)
)
AS 
BEGIN
BEGIN TRY	
BEGIN TRANSACTION

	IF @ID = -1
	BEGIN
		INSERT INTO OpportunityItemsReleaseDates
		(
			numDomainID,
			numOppID,
			numOppItemID,
			dtReleaseDate,
			numQty,
			tintStatus,
			numPurchasedOrderID
		)
		VALUES
		(
			@numDomainID,
			@numOppID,
			@numOppItemID,
			GETDATE(),
			@numQuantity,
			@tintReleaseStatus,
			@numPurchasedOrderID
		)
	END
	ELSE
	BEGIN
		UPDATE
			OpportunityItemsReleaseDates
		SET
			tintStatus=@tintReleaseStatus
			,numPurchasedOrderID=@numPurchasedOrderID
		WHERE
			numID=@ID
	END

	IF (SELECT
			SUM(OIRD.numQty) - (OI.numUnitHour * dbo.fn_UOMConversion(ISNULL(OI.numUOMId, 0), Item.numItemCode, Item.numDomainId, ISNULL(Item.numBaseUnit, 0)))
		FROM
			OpportunityItems OI
		INNER JOIN
			Item 
		ON
			OI.numItemCode = Item.numItemCode
		INNER JOIN
			OpportunityItemsReleaseDates OIRD
		ON
			OI.numOppId = OIRD.numOppID
			AND OI.numoppitemtCode = OIRD.numOppItemID
		WHERE
			OI.numOppId = @numOppID
			AND OI.numoppitemtCode = @numOppItemID
		GROUP BY
			OI.numoppitemtCode
			,Item.numBaseUnit
			,Item.numItemCode
			,Item.numDomainID
			,Item.numSaleUnit
			,OI.numUnitHour
			,OI.numUOMId) > 0
	BEGIN
		RAISERROR('Release qty is more than ordered qty.',16,1)
	END

COMMIT
END TRY
BEGIN CATCH
	DECLARE @ErrorMessage NVARCHAR(4000)
	DECLARE @ErrorNumber INT
	DECLARE @ErrorSeverity INT
	DECLARE @ErrorState INT
	DECLARE @ErrorLine INT
	DECLARE @ErrorProcedure NVARCHAR(200)

	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;

	SELECT 
		@ErrorMessage = ERROR_MESSAGE(),
		@ErrorNumber = ERROR_NUMBER(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE(),
		@ErrorLine = ERROR_LINE(),
		@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

	RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);

END CATCH
END
