SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ManageRuleAction')
DROP PROCEDURE USP_ManageRuleAction
GO
CREATE PROCEDURE [dbo].USP_ManageRuleAction
    @numRuleID AS NUMERIC(9),
    @numDomainID AS NUMERIC(9),
    @numRuleConditionID AS NUMERIC(9),
    @tintActionType AS TINYINT, 
    @numActionTemplateID AS NUMERIC(9),
	@vcFieldValue as varchar(1000),
	@tintActionFrom as tinyint,
	@tintActionTo as tinyint,
    @vcActionFrom as varchar(1000),
	@vcActionTo as varchar(1000),
	@tintActionOrder as tinyint
AS 
			INSERT  INTO RuleAction
                    (
                      numDomainId,numRuleID,numRuleConditionID,tintActionType,numActionTemplateID,vcFieldValue,
					  tintActionFrom,tintActionTo,vcActionFrom,vcActionTo,tintActionOrder	
                    )
            VALUES  (
                      @numDomainId,@numRuleID,@numRuleConditionID,@tintActionType,@numActionTemplateID,@vcFieldValue,
					  @tintActionFrom,@tintActionTo,@vcActionFrom,@vcActionTo,@tintActionOrder
                    )

            SELECT  SCOPE_IDENTITY()  
GO
