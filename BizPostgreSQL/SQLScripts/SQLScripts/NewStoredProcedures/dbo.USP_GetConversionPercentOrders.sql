GO
SET ANSI_NULLS ON

GO
SET QUOTED_IDENTIFIER ON
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetConversionPercentOrders')
DROP PROCEDURE USP_GetConversionPercentOrders
GO
CREATE PROCEDURE [dbo].[USP_GetConversionPercentOrders]                                 
	@numDomainID NUMERIC(18,0)
	,@ClientTimezoneOffset INT
	,@vcDivisionIds VARCHAR(MAX)                                          
AS                                                                
BEGIN  
	SELECT
		CONCAT(CompanyInfo.vcCompanyName,' (',dbo.FormatedDateFromDate(DivisionMaster.bintCreatedDate,@numDomainID),')') vcCompanyName
		,vcPOppName
		,monDealAmount
		,dbo.fn_GetContactName(DivisionMaster.numRecOwner) vcRecordOwner
		,dbo.fn_GetContactName(DivisionMaster.numAssignedTo) vcAssignee
		,DATEDIFF(DAY,DivisionMaster.bintCreatedDate,TableFirstOrder.bintCreatedDate) intDaysTookToConvert
	FROM
		DivisionMaster
	INNER JOIN
		CompanyInfo 
	ON
		DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
	CROSS APPLY
	(
		SELECT TOP 1 
			CONCAT(vcPOppName,' (',dbo.FormatedDateFromDate(OpportunityMaster.bintCreatedDate,@numDomainID),')') vcPOppName
			,OpportunityMaster.bintCreatedDate
			,ISNULL(monDealAmount,0) monDealAmount
		FROM
			OpportunityMaster
		WHERE
			numDomainID=@numDomainID
			AND OpportunityMaster.numDivisionId=DivisionMaster.numDivisionID
		ORDER BY
			OpportunityMaster.bintCreatedDate ASC
	) TableFirstOrder
	WHERE
		DivisionMaster.numDomainID = @numDomainID
		AND LEN(ISNULL(@vcDivisionIds,'')) > 0 AND DivisionMaster.numDivisionID IN (SELECT Id FROM dbo.SplitIDs(@vcDivisionIds,','))
	ORDER BY
		vcCompanyName
END