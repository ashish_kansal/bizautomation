GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ShippingFieldValues_GetAllByDomain')
DROP PROCEDURE dbo.USP_ShippingFieldValues_GetAllByDomain
GO

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[USP_ShippingFieldValues_GetAllByDomain]
(
	@numDomainID NUMERIC(18,0)
)
AS
BEGIN
	SELECT 
		ShippingFieldValues.numListItemID AS numShipVia
		,ShippingFieldValues.intShipFieldID
		,ShippingFields.vcFieldName
		,ISNULL(ShippingFieldValues.vcShipFieldValue,'') vcShipFieldValue
	FROM	
		ShippingFieldValues
	INNER JOIN
		ShippingFields
	ON
		ShippingFieldValues.intShipFieldID = ShippingFields.intShipFieldID
	WHERE
		ShippingFieldValues.numDomainID=@numDomainID
	ORDER BY
		ShippingFieldValues.numListItemID
END
GO