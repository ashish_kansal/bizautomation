
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ManagePayrollTracking')
DROP PROCEDURE USP_ManagePayrollTracking
GO
CREATE PROCEDURE [dbo].[USP_ManagePayrollTracking]
    @numDomainId numeric(18, 0),
    @numPayrollHeaderID numeric(18, 0),
    @numUserCntID numeric(18, 0),
    @strItems TEXT,
    @tintMode TINYINT 
AS
BEGIN

IF NOT EXISTS (SELECT numPayrollDetailID FROM dbo.PayrollDetail WHERE numDomainId=@numDomainId AND numPayrollHeaderID=@numPayrollHeaderID
AND numUserCntID=@numUserCntID)
BEGIN
	INSERT INTO PayrollDetail(numPayrollHeaderID,numDomainId,numUserCntID,monHourlyRate,bitOverTime,bitOverTimeHrsDailyOrWeekly,numLimDailHrs,monOverTimeRate,numCheckStatus)
		SELECT @numPayrollHeaderID,@numDomainId,@numUserCntID,monHourlyRate,bitOverTime,bitOverTimeHrsDailyOrWeekly,numLimDailHrs,monOverTimeRate,0
		FROM dbo.UserMaster WHERE numDomainId=@numDomainId AND numUserDetailId=@numUserCntID
END

DECLARE @numPayrollDetailID NUMERIC(18,0)

SELECT @numPayrollDetailID=numPayrollDetailID FROM PayrollDetail WHERE numDomainId=@numDomainId AND numPayrollHeaderID=@numPayrollHeaderID
AND numUserCntID=@numUserCntID

DECLARE @hDocItem INT
EXEC sp_xml_preparedocument @hDocItem OUTPUT, @strItems

SELECT CASE WHEN ISNULL(X.numCategoryHDRID,0)=0 THEN NULL ELSE X.numCategoryHDRID END AS numCategoryHDRID,
		CASE WHEN ISNULL(X.numComissionID,0)=0 THEN NULL ELSE X.numComissionID END AS numComissionID,
		X.monCommissionAmt INTO #temp
	FROM    (SELECT * FROM  OPENXML (@hDocItem, '/NewDataSet/Table1', 2)
				WITH (numCategoryHDRID NUMERIC,numComissionID NUMERIC,monCommissionAmt DECIMAL(20,5))) X
					
EXEC sp_xml_removedocument @hDocItem
					
IF @tintMode=1 --RegularHrs
BEGIN
	DELETE FROM PayrollTracking WHERE numPayrollHeaderID=@numPayrollHeaderID AND numPayrollDetailID=@numPayrollDetailID  
	AND numCategoryHDRID IN(SELECT TE.numCategoryHDRID FROM TimeAndExpense TE 
		JOIN PayrollTracking PT ON  ISNULL(TE.numCategoryHDRID,0)=ISNULL(PT.numCategoryHDRID,0)
		WHERE TE.numUserCntID=@numUserCntID  And TE.numDomainID=@numDomainId 
		And numPayrollHeaderID=@numPayrollHeaderID AND numPayrollDetailID=@numPayrollDetailID  
		AND (numType=1 Or numType=2)  And numCategory=1)
END
ELSE IF @tintMode=3 --PaidLeaveHrs
BEGIN
	DELETE FROM PayrollTracking WHERE numPayrollHeaderID=@numPayrollHeaderID AND numPayrollDetailID=@numPayrollDetailID  
	AND numCategoryHDRID IN(SELECT TE.numCategoryHDRID FROM TimeAndExpense TE 
		JOIN PayrollTracking PT ON  ISNULL(TE.numCategoryHDRID,0)=ISNULL(PT.numCategoryHDRID,0)
		WHERE TE.numUserCntID=@numUserCntID  And TE.numDomainID=@numDomainId 
		And numPayrollHeaderID=@numPayrollHeaderID AND numPayrollDetailID=@numPayrollDetailID  
		AND numType=3 And numCategory=3)
END
ELSE IF @tintMode=4 --PaidInvoice
BEGIN
	DELETE FROM PayrollTracking WHERE numPayrollHeaderID=@numPayrollHeaderID AND numPayrollDetailID=@numPayrollDetailID
	AND numComissionID IN (SELECT  BC.numComissionID FROM OpportunityMaster Opp 
		INNER JOIN OpportunityBizDocs oppBiz on oppBiz.numOppId=opp.numOppId 
        INNER JOIN BizDocComission BC on BC.numOppBizDocId=oppBiz.numOppBizDocsId
        JOIN PayrollTracking PT ON PT.numComissionID=BC.numComissionID 
		where Opp.numDomainId=@numDomainId AND BC.numUserCntId=@numUserCntID 
		And numPayrollHeaderID=@numPayrollHeaderID AND numPayrollDetailID=@numPayrollDetailID  
		AND BC.bitCommisionPaid=0 AND oppBiz.bitAuthoritativeBizDocs = 1 AND isnull(oppBiz.monAmountPaid,0) >=oppBiz.monDealAmount)

END
ELSE IF @tintMode=5 --UNPaidInvoice
BEGIN
	DELETE FROM PayrollTracking WHERE numPayrollHeaderID=@numPayrollHeaderID AND numPayrollDetailID=@numPayrollDetailID
	AND numComissionID IN (SELECT  BC.numComissionID FROM OpportunityMaster Opp 
		INNER JOIN OpportunityBizDocs oppBiz on oppBiz.numOppId=opp.numOppId 
        INNER JOIN BizDocComission BC on BC.numOppBizDocId=oppBiz.numOppBizDocsId
        JOIN PayrollTracking PT ON PT.numComissionID=BC.numComissionID 
		where Opp.numDomainId=@numDomainId AND BC.numUserCntId=@numUserCntID 
		And numPayrollHeaderID=@numPayrollHeaderID AND numPayrollDetailID=@numPayrollDetailID  
		AND BC.bitCommisionPaid=0 AND oppBiz.bitAuthoritativeBizDocs = 1 AND isnull(oppBiz.monAmountPaid,0) <oppBiz.monDealAmount)

END
ELSE IF @tintMode=6 --Expense
BEGIN
	DELETE FROM PayrollTracking WHERE numPayrollHeaderID=@numPayrollHeaderID AND numPayrollDetailID=@numPayrollDetailID  
	AND numCategoryHDRID IN(SELECT TE.numCategoryHDRID FROM TimeAndExpense TE 
		JOIN PayrollTracking PT ON  ISNULL(TE.numCategoryHDRID,0)=ISNULL(PT.numCategoryHDRID,0)
		WHERE TE.numUserCntID=@numUserCntID  And TE.numDomainID=@numDomainId 
		And numPayrollHeaderID=@numPayrollHeaderID AND numPayrollDetailID=@numPayrollDetailID  
		AND numCategory=2 And numType in (1,2))
END
ELSE IF @tintMode=7 --Reimburse
BEGIN
	DELETE FROM PayrollTracking WHERE numPayrollHeaderID=@numPayrollHeaderID AND numPayrollDetailID=@numPayrollDetailID  
	AND numCategoryHDRID IN(SELECT TE.numCategoryHDRID FROM TimeAndExpense TE 
		JOIN PayrollTracking PT ON  ISNULL(TE.numCategoryHDRID,0)=ISNULL(PT.numCategoryHDRID,0)
		WHERE TE.numUserCntID=@numUserCntID  And TE.numDomainID=@numDomainId 
		And numPayrollHeaderID=@numPayrollHeaderID AND numPayrollDetailID=@numPayrollDetailID  
		AND bitreimburse = 1 AND numcategory = 2)
END

ELSE IF @tintMode=8 --Project Commission
BEGIN
	DELETE FROM PayrollTracking WHERE numPayrollHeaderID=@numPayrollHeaderID AND numPayrollDetailID=@numPayrollDetailID
	AND numComissionID IN (SELECT  BC.numComissionID FROM ProjectsMaster PM
        INNER JOIN BizDocComission BC on BC.numProId=PM.numProId 
        JOIN PayrollTracking PT ON PT.numComissionID=BC.numComissionID 
		where PM.numDomainId=@numDomainId /*AND PM.numProjectStatus=27492*/ AND BC.numUserCntId=@numUserCntID 
		And numPayrollHeaderID=@numPayrollHeaderID AND numPayrollDetailID=@numPayrollDetailID  
		AND BC.bitCommisionPaid=0)
END		

INSERT INTO PayrollTracking (numDomainId,numPayrollHeaderID,numPayrollDetailID,numCategoryHDRID,numComissionID,monCommissionAmt)
SELECT @numDomainId,@numPayrollHeaderID,@numPayrollDetailID,numCategoryHDRID,numComissionID,monCommissionAmt
FROM #temp
		
DROP TABLE #temp
					 
END