
GO
/****** Object:  StoredProcedure [dbo].[usp_UpdateStagePercentageDetails]    Script Date: 09/21/2010 12:00:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
----Created By Gangadhar    
    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ConEmpListByTeamId')
DROP PROCEDURE USP_ConEmpListByTeamId
GO
CREATE PROCEDURE [dbo].[USP_ConEmpListByTeamId]       
@numDomainID as numeric(9)=0,    
@numTeamId as numeric(9)=0            
as    
BEGIN   
SELECT 
		A.numContactId AS numContactID,A.vcFirstName+' '+A.vcLastName as vcUserName
	FROM 
		AdditionalContactsInformation AS A  
	WHERE 
		A.numDomainID=@numDomainId AND
		A.numTeam=@numTeamId
	ORDER BY
		vcUserName 

END 