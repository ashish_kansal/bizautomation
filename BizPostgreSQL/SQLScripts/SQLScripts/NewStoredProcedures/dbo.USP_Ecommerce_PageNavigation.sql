GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_Ecommerce_PageNavigation' ) 
    DROP PROCEDURE USP_Ecommerce_PageNavigation
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE USP_Ecommerce_PageNavigation
   @numModuleID as numeric(9),      
		@numDomainID as numeric(9),
		@numGroupID NUMERIC(18, 0) ,
		@numParentID numeric(18,0)  ,
		@Mode int 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	WITH CTE (ID, ParentID, PageNavName, NavURL, ImageURL, SortOrder)
	AS
	(
		SELECT  
			PND.numPageNavID AS ID,
			numParentID AS ParentID,
			vcPageNavName AS PageNavName,
			isnull(vcNavURL, '') AS NavURL,
			vcImageURL AS ImageURL,
			intSortOrder AS SortOrder
		FROM    
			PageNavigationDTL PND
		JOIN 
			dbo.TreeNavigationAuthorization TNA 
		ON 
			PND.numPageNavID = TNA.numPageNavID   
		WHERE  
			numModuleID = @numModuleID
			AND TNA.numDomainID = @numDomainID
			AND numGroupID = @numGroupID
			AND PND.numParentID in (@numParentID)
			AND PND.numPageNavID <> 119
			AND TNA.numTabID = -1
		UNION ALL
		SELECT  
			PND.numPageNavID AS ID,
			numParentID AS ParentID,
			vcPageNavName AS PageNavName,
			isnull(vcNavURL, '') AS NavURL,
			vcImageURL AS ImageURL,
			ISNULL(intSortOrder,1000) AS SortOrder
		FROM    
			PageNavigationDTL PND
		JOIN 
			dbo.TreeNavigationAuthorization TNA 
		ON 
			PND.numPageNavID = TNA.numPageNavID  
		JOIN
			CTE 
		ON
			PND.numParentID = CTE.ID		
		WHERE  
			numModuleID = @numModuleID
			AND TNA.numDomainID = @numDomainID
			AND numGroupID = @numGroupID
			AND PND.numPageNavID != CTE.ID
			AND TNA.numTabID = -1
	)

	SELECT 
		ID AS numPageNavID,
		(CASE ParentID WHEN 73 THEN NULL ELSE ParentID END) AS numParentID ,
		PageNavName AS vcPageNavName,
		NavURL AS vcNavURL,
		ImageURL AS vcImageURL,
		SortOrder AS intSortOrder
	FROM 
		CTE 
	ORDER BY 
		SortOrder

END