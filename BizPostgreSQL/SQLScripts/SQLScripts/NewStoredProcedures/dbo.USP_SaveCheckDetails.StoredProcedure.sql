SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_SaveCheckDetails')
DROP PROCEDURE USP_SaveCheckDetails
GO
CREATE PROCEDURE [dbo].[USP_SaveCheckDetails]  
@numDomainId as numeric(9)=0,                       
@numCheckHeaderID as numeric(9)=0,                                                                                          
@strRow as text=''                                                                        
As                                                
Begin                                          
If convert(varchar(100),@strRow) <> ''                                                                                                            
Begin                                                                                                             
  Declare @hDoc3 int                                                                                                                                                                
   EXEC sp_xml_preparedocument @hDoc3 OUTPUT, @strRow                                                                                                             
	
   DELETE FROM CheckDetails WHERE numCheckHeaderID=@numCheckHeaderID 
	AND numCheckDetailID NOT IN (SELECT X.numCheckDetailID 
			 FROM OPENXML(@hDoc3,'/NewDataSet/Table1 [numCheckDetailID>0]',2)                                                                                                              
			With(numCheckDetailID numeric(9))X)
			                                                           
			--Update transactions
			Update CheckDetails  Set numChartAcntId=X.numChartAcntId,monAmount=X.monAmount,                                                                                              
			vcDescription=X.vcDescription,numCustomerId=X.numCustomerId
			,numClassID=X.numClassID,numProjectID=X.numProjectID
			From (SELECT * FROM OPENXML(@hDoc3,'/NewDataSet/Table1 [numCheckDetailID>0]',2)                                                                                                              
			With(                                                                                       
			numCheckDetailID numeric(9),  
			numChartAcntId numeric(9),                                                                                        
			monAmount DECIMAL(20,5),                                                                                                             
			vcDescription varchar(1000),                                                                                                            
			numCustomerId numeric(9),numClassID NUMERIC(9),numProjectID NUMERIC(9)                                                                                                  
			))X                                                                                                  
			Where CheckDetails.numCheckDetailID=X.numCheckDetailID AND numCheckHeaderID=@numCheckHeaderID                                                                                              
			               
			--Insert New transactions if any
			Insert Into CheckDetails(numCheckHeaderID,numChartAcntId,monAmount,vcDescription,numCustomerId,numClassID,numProjectID)                                                                     
			Select @numCheckHeaderID,numChartAcntId ,monAmount ,vcDescription ,numCustomerId ,numClassID ,numProjectID from (                                           
			Select * from OPenXml (@hdoc3,'/NewDataSet/Table1 [numCheckDetailID=0]',2)                                                                                
			With(                                                                                                            
			numCheckDetailID numeric(9),  
			numChartAcntId numeric(9),                                                                                        
			monAmount DECIMAL(20,5),                                                                                                             
			vcDescription varchar(1000),                                                                                                            
			numCustomerId numeric(9),numClassID NUMERIC(9),numProjectID NUMERIC(9)                 
			))X                                                                     
			
			                                                                       
	EXEC sp_xml_removedocument @hDoc3 
	END
	END                                   
	                                                                                      
