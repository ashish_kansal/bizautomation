
GO
/****** Object:  StoredProcedure [dbo].[USP_ManageBizDocActionItem]    Script Date: 02/11/2010 21:00:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--EXEC USP_ManageBizDocActionItem @numDomainID=72,@dtFromDate='29/Jan/2010',@dtTodate='30/Jan/2010'
--select * from BizDocAction
--select * from BizActionDetails
--
--delete from BizDocAction
--delete from BizActionDetails

--select * from Documentworkflow
--select * from SpecificDocuments where numSpecificDocid=127
--
--SELECT * FROM GenericDocuments where numGenericDocid=127
---select * from ListDetails where numListItemid=14463

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ManageBizDocActionItem')
DROP PROCEDURE USP_ManageBizDocActionItem
GO
CREATE PROCEDURE [dbo].[USP_ManageBizDocActionItem]
(@numDomainID numeric,
@dtFromDate datetime,
@dtTodate datetime)

as

begin


declare @numEmployeeID numeric(18);
declare @numDivisionID numeric(18);
declare @numOppBizDocsId numeric(18);
declare @numTempEmployeeId numeric(18);
declare @numBizActionId numeric(18);
declare @numContactID numeric(18);
declare @numActionTypeID  numeric(18);
declare @btDocType bit;
declare @numBizDocAppId numeric(18);

create table #temp_BizDocActionItem
(numBizDocAppId numeric(18),
	numOppBizDocsId numeric(18),
 numEmployeeID numeric(18),
 numDivisionID numeric(18),
numContactID numeric(18),
numActionTypeID numeric(18),
btDocType bit )


--select * from  View_BizDocForApproval where BizDocId like 'ABC inc-SO-January-2010-A6056-BD-5450%'
--select * from BizDocApprovalRule
--update BizDocApprovalRule set btBizDocPaidFull=0

insert into #temp_BizDocActionItem

select BA.numBizDocAppId,numOppBizDocsId,numEmployeeID,AC.numDivisionID,AC.numContactID,BA.numActionTypeID,1 as DocType from 
View_BizDocForApproval VB
INNER JOIN BizDocApprovalRule BA ON  VB.numBizDocId=BA.numBizDocTypeId and BA.btBizDocPaidFull = 1 AND BA.numDomainID=VB.numDomainID
INNER JOIN BizDocApprovalRuleEmployee BE ON VB.BizDocAmount=BizDocPaidAmount AND BE.numBizDocAppID = BA.numBizDocAppID
INNER JOIN AdditionalContactsInformation AC ON AC.numContactId=BE.numEmployeeID AND AC.numDomainID=BA.numDomainID
where   
	  dtCreatedDate BETWEEN @dtFromDate and @dtTodate and 
	VB.BizDocAmount between BA.numRangeFrom and BA.numRangeTo and 
	VB.numDomainID=@numDomainID
UNION
	select  BA.numBizDocAppId,numOppBizDocsId,numEmployeeID,AC.numDivisionID,AC.numContactID,BA.numActionTypeID,1 as DocType from 
	View_BizDocForApproval VB
	INNER JOIN BizDocApprovalRule BA ON BA.numBizDocTypeId=VB.numBizDocId and BA.btBizDocPaidFull = 0 AND BA.numDomainID = @numDomainID
	INNER JOIN BizDocApprovalRuleEmployee BE ON BE.numBizDocAppID = BA.numBizDocAppID
	INNER JOIN AdditionalContactsInformation AC ON AC.numContactId=BE.numEmployeeID AND AC.numDomainID = @numDomainID
	where   
			dtCreatedDate BETWEEN @dtFromDate and @dtTodate and 
		  VB.BizDocAmount between BA.numRangeFrom and BA.numRangeTo and 
			VB.numDomainID=@numDomainID
UNION
--select * from View_BizDoc
select  0,numOppBizDocsId,BizDocCreatedBy,AC.numDivisionID,AC.numContactID,0 numActionTypeID,1 as DocType from 
	View_BizDoc VB
	INNER JOIN AdditionalContactsInformation AC ON VB.numDomainID=AC.numDomainID
	INNER JOIN (SELECT * FROM Documentworkflow  DF WHERE DF.cDocType='B') DF ON AC.numContactId=DF.numContactId AND VB.numOppBizDocsId=DF.numDocID
	where DF.dtCreatedDate BETWEEN @dtFromDate and @dtTodate and 	   
			VB.numDomainID=@numDomainID
UNION
select 0,
  numGenericDocID,
c.numContactID,   	
  numDivisionId,
	 numRecID,	
  0 as numActionTypeID, 0 as DocType
  from dbo.GenericDocuments A INNER JOIN Documentworkflow B ON A.numGenericDocID = B.numDocID INNER JOIN AdditionalContactsInformation C ON B.numContactID=C.numContactID    
  where  a.numGenericDocID=numDocID and 
		 a.numDomainID=@numDomainID  AND 
		 B.dtCreatedDate between @dtFromDate and @dtTodate 
--and 
--		 A.numSpecificDocID not in 
--		(select numOppBizDocsId from BizActionDetails where btDocType=0) 

UNION

select 0,
  numGenericDocID,
c.numContactID,   	
  numDivisionId,
	 A.numCreatedBy,	
  0 as numActionTypeID, 0 as DocType
  from GenericDocuments A INNER JOIN Documentworkflow B ON a.numGenericDocID=B.numDocID INNER JOIN AdditionalContactsInformation C ON B.numContactID=C.numContactID
  where  a.numDomainID=@numDomainID    AND 
		 B.dtCreatedDate between @dtFromDate and @dtTodate 
--and 
--		 A.numGenericDocID not in 
--		(select numGenericDocID from BizActionDetails where btDocType=0)

--select * from GenericDocuments
--select * from #temp_BizDocActionItem order by numEmployeeID


declare BizDocActionItem CURSOR FOR
select * from #temp_BizDocActionItem order by numEmployeeID


OPEN BizDocActionItem

set @numTempEmployeeId=0

FETCH NEXT FROM BizDocActionItem into @numBizDocAppId,@numOppBizDocsId,@numEmployeeID,@numDivisionID,@numContactID,@numActionTypeID,@btDocType
WHILE @@FETCH_STATUS = 0
   BEGIN
		
		if @numTempEmployeeId<> @numEmployeeID
			begin
				insert into BizDocAction (numContactId ,numDivisionId,numStatus,numDomainID,dtCreatedDate,numAssign,numCreatedBy,bitTask,numBizDocAppId)
				values (@numEmployeeID,@numDivisionID,0,@numDomainID,getdate(),@numContactID,1,@numActionTypeID,@numBizDocAppId);
				set @numBizActionId=@@IDENTITY							
			end
			
			INSERT INTO BizActionDetails (numBizActionId,numOppBizDocsId,btStatus,btDocType)
			VALUES (@numBizActionId,@numOppBizDocsId,0,@btDocType)			
			
			set @numTempEmployeeId=@numEmployeeID

      FETCH NEXT FROM BizDocActionItem into @numBizDocAppId,@numOppBizDocsId,@numEmployeeID,@numDivisionID,@numContactID,@numActionTypeID,@btDocType
   END;

CLOSE BizDocActionItem;
DEALLOCATE BizDocActionItem;
--select * from #temp_BizDocActionItem;
drop table #temp_BizDocActionItem;
END;

