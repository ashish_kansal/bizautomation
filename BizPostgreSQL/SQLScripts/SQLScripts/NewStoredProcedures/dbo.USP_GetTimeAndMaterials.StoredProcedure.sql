set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go

-- exec USP_GetTimeAndMaterials @numProId=204,@numDomainID=72
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetTimeAndMaterials')
DROP PROCEDURE USP_GetTimeAndMaterials
GO
CREATE PROCEDURE [dbo].[USP_GetTimeAndMaterials]
    @numProId NUMERIC(9),
    @numDomainID NUMERIC(9)
AS 
    BEGIN
        DECLARE @ProjName VARCHAR(100)
        SELECT  @ProjName = [vcProjectName]
        FROM    [ProjectsMaster]
        WHERE   [numProId] = @numProId
        
        SELECT  TE.[numCategoryHDRID],
                CASE numType
                  WHEN 1 THEN 'Billable Time'
                  ELSE 'Non-Billable Time'
                END AS TimeType,
                TE.[numOppItemID],
                I.[vcItemName],
                OB.[vcBizDocID],
                CASE numType
                  WHEN 1 THEN ISNULL(numUnitHour,0)
                  ELSE ISNULL(CONVERT(VARCHAR(100), CONVERT(DECIMAL(10, 1), ( CONVERT(DECIMAL(10, 1), DATEDIFF(minute, TE.dtFromDate, TE.dtToDate)) / 60 ))),0)
                END AS [numUnitHour],
                ISNULL([monPrice],0) monPrice,
                OB.[numOppBizDocsId],
                dbo.[FormatedDateFromDate](TE.[dtTCreatedOn], @numDomainID) AS dtDateEntered,
                dbo.fn_GetContactName(TE.[numUserCntID]) AS ContactName,
                @ProjName AS vcProjectName
        FROM    [TimeAndExpense] TE
                LEFT OUTER JOIN [OpportunityBizDocs] OB ON TE.[numOppBizDocsId] = OB.[numOppBizDocsId]
                LEFT OUTER JOIN [OpportunityBizDocItems] OBI ON OBI.numOppBizDocID = TE.[numOppBizDocsId]
                LEFT OUTER JOIN Item I ON OBI.[numItemCode] = I.[numItemCode]
        WHERE   [numProId] = @numProId
                AND TE.[numDomainID] = @numDomainID
        UNION
        SELECT  0,
                CASE OM.[tintOppType]
                WHEN 1 THEN 'S.O'
                WHEN 2 THEN 'P.O'
                END AS TimeType,
                TE.[numOppItemID],
                I.[vcItemName],
                OB.[vcBizDocID],
                ISNULL(OBI.numUnitHour,0) numUnitHour,
                ISNULL(OBI.[monPrice],0) monPrice,
                OB.[numOppBizDocsId],
                dbo.[FormatedDateFromDate](OB.[dtCreatedDate], @numDomainID) AS dtDateEntered,
                dbo.fn_GetContactName(OB.[numCreatedBy]) AS ContactName,
                @ProjName AS vcProjectName
        FROM    [ProjectsOpportunities] PO
				LEFT OUTER JOIN [OpportunityMaster] OM ON PO.[numOppId] = OM.[numOppId]
                LEFT OUTER JOIN [OpportunityBizDocs] OB ON PO.[numOppBizDocID] = OB.[numOppBizDocsId]
                LEFT OUTER JOIN [OpportunityBizDocItems] OBI ON OBI.numOppBizDocID = PO.[numOppBizDocID]
                LEFT OUTER JOIN Item I ON OBI.[numItemCode] = I.[numItemCode]
                LEFT OUTER JOIN [TimeAndExpense] TE ON TE.[numProId]=PO.[numProId]
                AND TE.[numOppBizDocsId] = PO.[numOppBizDocID]
        WHERE   PO.[numProId] = @numProId
                AND PO.[numDomainID] = @numDomainID
                AND OBI.[numOppItemID] > 0
                AND TE.[numCategoryHDRID] IS NULL --added because qury was getting duplicate of billable time .. as it is also linked with project

--                AND OM.[tintOppType] = 2


        
    END	
    
    
--    exec USP_ProMilestone @ProcessID=0,@ContactID=17,@DomianID=72,@numProId=191
