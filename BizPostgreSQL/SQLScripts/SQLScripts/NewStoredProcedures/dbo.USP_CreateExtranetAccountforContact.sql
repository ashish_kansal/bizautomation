SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj                                      
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_CreateExtranetAccountforContact')
DROP PROCEDURE USP_CreateExtranetAccountforContact
GO
CREATE PROCEDURE [dbo].[USP_CreateExtranetAccountforContact]  
(
	@numDomainID as numeric(9),
	@vcPassword as varchar(100),
	@numContactID as numeric(9),
	@tintAccessAllowed TINYINT
) AS
BEGIN
	
	declare @numDivisionID as numeric(9)
	declare @numExtranetID as numeric(9)

	select @numDivisionID=numDivisionID from AdditionalContactsInformation where numContactID=@numContactID

	IF NOT EXISTS (SELECT * FROM ExtarnetAccounts where numDivisionID=@numDivisionID) 
	BEGIN
		DECLARE @numGroupID NUMERIC
		DECLARE @numCompanyID NUMERIC(18,0)
		SELECT @numCompanyID=numCompanyID FROM DivisionMaster WHERE numDivisionID=@numDivisionID

		SELECT TOP 1 @numGroupID=numGroupID FROM dbo.AuthenticationGroupMaster WHERE numDomainID=@numDomainID AND tintGroupType=2
		IF @numGroupID IS NULL SET @numGroupID = 0 
		INSERT INTO ExtarnetAccounts (numCompanyID,numDivisionID,numGroupID,numDomainID)                                                
		  values(@numCompanyID,@numDivisionID,@numGroupID,@numDomainID)            
	END

	select @numExtranetID=numExtranetID from ExtarnetAccounts where numDivisionID=@numDivisionID
	IF NOT EXISTS(SELECT * FROM dbo.ExtranetAccountsDtl WHERE numDomainID=@numDomainID AND numContactID=@numContactID)
	BEGIN
		insert into ExtranetAccountsDtl (numExtranetID,numContactID,vcPassword,tintAccessAllowed,numDomainID)
		values (@numExtranetID,@numContactID,@vcPassword,@tintAccessAllowed,@numDomainID)	
	END
	--PRINT ISNULL(@numExtranetID,0)
	select ISNULL(@numExtranetID,0)
END