GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ActivityAttendees')
DROP PROCEDURE USP_ActivityAttendees
GO

CREATE PROCEDURE [dbo].[USP_ActivityAttendees]
    @strRow AS TEXT = '' ,
	@numActivityID AS NUMERIC(9) = 0
AS 
BEGIN                   
BEGIN TRY 
	
	IF ISNULL(CAST(@strRow AS VARCHAR),'') <> '' AND CHARINDEX('<?xml',@strRow) = 0
	BEGIN
		SET @strRow = CONCAT('<?xml version="1.0" encoding="ISO-8859-1"?>',@strRow)
	END

      
    IF CONVERT(VARCHAR(100), @strRow) <> '' 
    BEGIN
        DECLARE @hDoc3 INT                                                                                                                                                                
        EXEC sp_xml_preparedocument @hDoc3 OUTPUT, @strRow                                                                                                             

		CREATE TABLE #temp(EmailId varchar(50),ResponseStatus varchar(20))


		INSERT INTO #temp(EmailId,ResponseStatus)
        SELECT 
			EmailId,
			ResponseStatus
		FROM 
			OPENXML (@hdoc3,'/ArrayOfAttendee/Attendee',2)
		WITH 
		(
			EmailId varchar(50)
			,ResponseStatus varchar(20)
		)

        EXEC sp_xml_removedocument @hDoc3 
                                    

/*-----------------------Validation of balancing entry*/
Delete From ActivityAttendees where ActivityID= @numActivityID
		IF EXISTS(SELECT * FROM #temp)
		BEGIN
			DECLARE @numDomainID NUMERIC(18,0)

			SELECT
				@numDomainID=res.numDomainId
			FROM 
				Activity ac 
			INNER JOIN             
				activityresource ar 
			ON 
				ac.activityid=ar.activityid             
			INNER JOIN 
				[resource] res 
			ON 
				ar.resourceid=res.resourceid
			WHERE
				ac.ActivityID=@numActivityID

		
			INSERT INTO ActivityAttendees
			(
				ActivityID,
				ResponseStatus,
				AttendeeEmail,
				AttendeeFirstName,
				AttendeeLastName,
				AttendeePhone,
				AttendeePhoneExtension,
				CompanyNameinBiz,
				DivisionID,
				tintCRMType,
				ContactID
			)
			SELECT 
				@numActivityID
				,T.ResponseStatus
				,T.EmailId
				,Ac.vcFirstName
				,Ac.vcLastName
				,Ac.numPhone
				,Ac.numPhoneExtension
				,CI.numCompanyId
				,AC.numDivisionId
				,Div.tintCRMType
				,AC.numContactId
			From 
				#temp T
			left Join 
				AdditionalContactsInformation AC 
			on 
				numContactId = (select MAX(numContactId) From AdditionalContactsInformation where vcEmail=T.EmailId AND numDomainID=@numDomainID)
			left join 
				DivisionMaster Div 
			on 
				AC.numDivisionId=Div.numDivisionId
			left join 
				CompanyInfo CI 
			on 
				Div.numCompanyID=CI.numCompanyID
		END
	
END
END TRY 
BEGIN CATCH		
   RollBack
END CATCH	

            
END
GO