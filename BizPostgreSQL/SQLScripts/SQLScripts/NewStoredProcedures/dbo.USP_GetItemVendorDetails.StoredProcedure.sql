GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetItemVendorDetails')
DROP PROCEDURE USP_GetItemVendorDetails
GO
CREATE PROCEDURE USP_GetItemVendorDetails
(
	@numVendorID BIGINT,
	@numDomainID NUMERIC(18),
	@numItemCode NUMERIC(18) = 0
)
AS 
BEGIN

	SELECT  
		ISNULL(V.numVendorTcode,0) AS [numVendorTcode],
		ISNULL(V.numVendorID,0) AS [numVendorID],
		ISNULL(V.vcPartNo,'') AS [vcPartNo],
		ISNULL(V.monCost,0) AS [monCost],
		ISNULL(V.numItemCode,0) AS [numItemCode],
		ISNULL(V.intMinQty,0) AS [intMinQty]
	FROM  
		dbo.Vendor V	
	WHERE 
		V.numVendorID = @numVendorID 
		AND V.numDomainID = @numDomainID
		AND V.numItemCode = @numItemCode
END