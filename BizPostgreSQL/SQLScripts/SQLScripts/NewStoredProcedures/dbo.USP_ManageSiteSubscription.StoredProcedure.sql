
GO
/****** Object:  StoredProcedure [dbo].[USP_ManageSiteSubscription]    Script Date: 08/08/2009 16:27:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Anoop Krishnan
-- Create date: 21-Jul-2009
-- Description:	To insert and delete subscriber details.
-- =============================================
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ManageSiteSubscription')
DROP PROCEDURE USP_ManageSiteSubscription
GO
CREATE PROCEDURE [dbo].[USP_ManageSiteSubscription] 
	@numDomainID NUMERIC(18) = 0,
	@numSiteID NUMERIC(18) = 0,
	@numContactId NUMERIC(18) = 0,
	@vcEmail VARCHAR(50) = null,
	@bitToSubscribe BIT = 0

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @numSubscriberID NUMERIC(18)
	DECLARE @numDivisionID NUMERIC(18)
	DECLARE @numCompanyID NUMERIC(18)
	
	IF (@bitToSubscribe = 1 AND (NOT EXISTS(SELECT [numSubscriberID] 
											FROM [SiteSubscriberDetails] 
											WHERE [numContactId] = @numContactId
												AND [numSiteID] = @numSiteID)))
	BEGIN
		INSERT INTO [dbo].[SiteSubscriberDetails]
			   ([numContactId],
				[numSiteID]
			   ,[bitIsSubscribed]
			   ,[dtSubscribedDate])
		 VALUES
			   (@numContactId
			   ,@numSiteID
			   ,@bitToSubscribe
			   ,GETDATE())

		SELECT SCOPE_IDENTITY()
	END
	ELSE IF(@bitToSubscribe = 0 AND (EXISTS(SELECT SSD.[numSubscriberID] FROM [SiteSubscriberDetails] SSD
											INNER JOIN [AdditionalContactsInformation] ACI
											ON SSD.[numContactId] = ACI.[numContactId]
											WHERE ACI.[vcEmail] = @vcEmail
												AND ACI.[numDomainID] = @numDomainID
												AND SSD.[numSiteID] = @numSiteID)))
	BEGIN
		SELECT @numSubscriberID = SSD.[numSubscriberID]
		FROM [SiteSubscriberDetails] SSD
		INNER JOIN [AdditionalContactsInformation] ACI
		ON SSD.[numContactId] = ACI.[numContactId]
		WHERE ACI.[vcEmail] = @vcEmail
			AND ACI.[numDomainID] = @numDomainID
			AND SSD.[numSiteID] = @numSiteID
		
		UPDATE [SiteSubscriberDetails]
		SET [bitIsSubscribed] = @bitToSubscribe
		  ,[dtUnsubscribedDate] = GETDATE()
		WHERE [numSubscriberID] = @numSubscriberID

		RETURN 1
	END
	ELSE
	BEGIN
		RETURN 0
	END
END
