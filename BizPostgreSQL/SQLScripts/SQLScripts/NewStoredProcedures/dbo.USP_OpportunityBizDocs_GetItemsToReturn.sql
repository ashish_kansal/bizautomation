SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by Siva                                                                                                            
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_OpportunityBizDocs_GetItemsToReturn')
DROP PROCEDURE USP_OpportunityBizDocs_GetItemsToReturn
GO
CREATE PROCEDURE  [dbo].[USP_OpportunityBizDocs_GetItemsToReturn]                                                                                                                                                
(           
	@numDomainID NUMERIC(18,0),                                                                                                                                     
	@numOppID NUMERIC(18,0),
	@numOppBizDocID NUMERIC(18,0)                                                              
)                                                                                                                                        
AS                           
BEGIN
	SELECT 
		OBDI.numOppBizDocItemID,
		OI.numoppitemtCode,
        OI.numOppId,
		OI.vcNotes as txtNotes,
        OI.numItemCode,
		OI.bitItemPriceApprovalRequired,
		OBDI.numUnitHour AS numOrigUnitHour,
        (OBDI.numUnitHour-ISNULL((SELECT SUM(numUnitHour) FROM ReturnItems where (ReturnItems.numOppBizDocItemID=OBDI.numOppBizDocItemID OR ReturnItems.numOppBizDocItemID IS NULL) AND 
		numReturnHeaderID IN (SELECT numReturnHeaderID FROM ReturnHeader 
		WHERE numOppId=@numOppID) AND numOppItemID=OI.numoppitemtCode),0))
		AS numUnitHour,
        OI.monPrice,
        OI.monTotAmount,
        OI.numSourceID,
        OI.vcItemDesc,
		OI.numCost,
        OI.numWarehouseItmsID,
        OI.vcType ItemType,
        OI.vcAttributes Attributes,
        OI.bitDropShip DropShip,
        OI.numUnitHourReceived,
        OI.bitDiscountType,
        OI.fltDiscount,
		OI.bitMarkupDiscount,
        OI.monTotAmtBefDiscount,
        OI.numQtyShipped,
        OI.vcItemName,
        OI.vcModelID,
        OI.vcPathForTImage,
        OI.vcManufacturer,
        OI.monVendorCost,
        ISNULL(OI.numUOMId, 0) AS numUOM,
        ISNULL(U.vcUnitName, '') vcUOMName,
        ISNULL(dbo.fn_UOMConversion(OI.numUOMId, OI.numItemCode,
                                OM.numDomainId, NULL),1) AS UOMConversionFactor,
        OI.bitWorkOrder,
        isnull(OI.numVendorWareHouse,0) as numVendorWareHouse,
        OI.numShipmentMethod,
        OI.numSOVendorId,
        OI.numProjectID,
        OI.numClassID,
        OI.numProjectStageID,
        OI.numToWarehouseItemID,
        UPPER(I.charItemType) charItemType,
        WItems.numWarehouseID,
        W.vcWareHouse AS Warehouse,
        0 as Op_Flag,
		CAST(( dbo.fn_UOMConversion(ISNULL(I.numBaseUnit, 0), OI.numItemCode,
                        OM.numDomainId, ISNULL(OI.numUOMId, 0))
			* OBDI.numUnitHour ) AS NUMERIC(18, 2)) AS numUnitHour,
		isnull(OI.numClassID,0) as numClassID,isnull(OI.numProjectID,0) as numProjectID,
		CASE WHEN (SELECT COUNT(*) FROM OpportunityBizDocs OB JOIN dbo.OpportunityBizDocItems OBI ON ob.numOppBizDocsId=obi.numOppBizDocID
		AND (ob.bitAuthoritativeBizDocs=1 OR ob.numBizDocId=304) AND obi.numOppItemID=oi.numoppitemtCode AND ob.numOppId=OM.numOppId)>0 THEN 1 ELSE 0 END  AS bitIsAuthBizDoc,
		ISNULL(OI.numUnitHourReceived,0) AS numUnitHourReceived,ISNULL(numQtyShipped,0) AS numQtyShipped,ISNULL(dbo.fn_GetUOMName(I.numBaseUnit),'') AS vcBaseUOMName,ISNULL(I.fltWeight,0) AS [fltWeight],ISNULL(I.bitFreeShipping,0) AS [bitFreeShipping],
		ISNULL(I.fltLength,0) AS [fltLength],
		ISNULL(I.fltWidth,0) AS [fltWidth],
		ISNULL(I.fltHeight,0) AS [fltHeight],
		ISNULL(WItems.vcWHSKU,ISNULL(I.vcSKU,'')) AS [vcSKU],
		(CASE WHEN ISNULL(I.bitVirtualInventory,0) = 1 THEN 0 ELSE ISNULL(I.monAverageCost,0) END) AS monAverageCost,
		ISNULL(I.bitFreeShipping,0) AS [bitFreeShipping],
		ISNULL(I.txtItemDesc,'') txtItemDesc,
		ISNULL(I.vcModelID,'') vcModelID,
		ISNULL(I.vcManufacturer,'') vcManufacturer,
		ISNULL(I.numBarCodeId,'') numBarCodeId,
		ISNULL(I.bitSerialized,0) bitSerialized,
		ISNULL(I.bitKitParent,0) bitKitParent,
		ISNULL(I.bitAssembly,0) bitAssembly,
		ISNULL(I.IsArchieve,0) IsArchieve,
		ISNULL(I.bitLotNo,0) bitLotNo,
		ISNULL(WItems.monWListPrice,0) monWListPrice,
		ISNULL(I.bitTaxable, 0) bitTaxable,
		CASE WHEN I.charItemType = 'P' THEN (SELECT CAST(ISNULL(SUM(numOnHand),0) AS VARCHAR(30)) FROM WareHouseItems WHERE WareHouseItems.numItemID = OI.numItemCode) ELSE '' END AS numOnHand,
		CASE WHEN I.charItemType = 'P' THEN (SELECT CAST(ISNULL(SUM(numOnOrder),0) AS VARCHAR(30)) FROM WareHouseItems WHERE WareHouseItems.numItemID = OI.numItemCode) ELSE '' END AS numOnOrder,
		CASE WHEN I.charItemType = 'P' THEN (SELECT CAST(ISNULL(SUM(numAllocation),0) AS VARCHAR(30)) FROM WareHouseItems WHERE WareHouseItems.numItemID = OI.numItemCode) ELSE '' END AS numAllocation,
		CASE WHEN I.charItemType = 'P' THEN (SELECT CAST(ISNULL(SUM(numBackOrder),0) AS VARCHAR(30)) FROM WareHouseItems WHERE WareHouseItems.numItemID = OI.numItemCode) ELSE '' END AS numBackOrder,
		case when ISNULL(I.bitAssembly,0)=1 then dbo.fn_GetAssemblyPossibleWOQty(I.numItemCode,WItems.numWareHouseID) else 0 end AS numMaxWorkOrderQty,
		ISNULL((SELECT COUNT(*) FROM OppWarehouseSerializedItem WHERE numOppId=@numOppID AND numOppItemID=OI.numoppitemtCode),0) AS numSerialNoAssigned,
		(CASE WHEN (SELECT COUNT(*) FROM OpportunityBizDocs OB JOIN dbo.OpportunityBizDocItems OBI ON OB.numOppBizDocsId=OBI.numOppBizDocID WHERE OB.numOppId = OI.numOppId AND OBI.numOppItemID=OI.numoppitemtCode AND OB.numBizDocId=296) > 0 THEN 1 ELSE 0 END) AS bitAddedFulFillmentBizDoc,
		ISNULL(OI.vcAttrValues,'') AS AttributeIDs
		,(CASE WHEN OM.tintOppType=2 THEN ISNULL(OI.vcNotes,'') ELSE '' END) AS vcVendorNotes
    FROM
		OpportunityBizDocItems OBDI
	INNER JOIN   
		dbo.OpportunityItems OI
	ON
		OBDI.numOppItemID = OI.numoppitemtCode
    INNER JOIN dbo.OpportunityMaster OM ON OM.numOppId = OI.numOppId
    INNER JOIN dbo.Item I ON I.numItemCode = OI.numItemCode
    LEFT JOIN UOM U ON U.numUOMId = OI.numUOMId
    LEFT JOIN WareHouseItems WItems ON WItems.numWareHouseItemID = OI.numWarehouseItmsID
    LEFT JOIN Warehouses W ON W.numWarehouseID = WItems.numWarehouseID
	WHERE
		OI.numOppId = @numOppID
		AND OBDI.numOppBizDocID=@numOppBizDocID
		AND OM.numDomainId = @numDomainId
		AND (OBDI.numUnitHour-ISNULL((SELECT SUM(numUnitHour) FROM ReturnItems where (ReturnItems.numOppBizDocItemID=OBDI.numOppBizDocItemID OR ReturnItems.numOppBizDocItemID IS NULL) AND numReturnHeaderID IN (SELECT numReturnHeaderID FROM ReturnHeader WHERE numOppId=@numOppID) AND numOppItemID=OI.numoppitemtCode),0))>0
                    
	SELECT  vcSerialNo,
			vcComments AS Comments,
			numOppItemID AS numoppitemtCode,
			W.numWarehouseItmsDTLID,
			numWarehouseItmsID AS numWItmsID,
			dbo.Fn_getattributes(W.numWarehouseItmsDTLID, 1) Attributes
	FROM    WareHouseItmsDTL W
			JOIN OppWarehouseSerializedItem O ON O.numWarehouseItmsDTLID = W.numWareHouseItmsDTLID
	WHERE   
		numOppID = @numOppID 
		AND numOppItemID IN (SELECT 
								OI.numoppitemtCode 
							FROM 
								OpportunityBizDocItems OBDI
							INNER JOIN   
								dbo.OpportunityItems OI
							ON
								OBDI.numOppItemID = OI.numoppitemtCode 
							WHERE 
								OI.numOppId = @numOppID 
								AND OBDI.numOppBizDocID=@numOppBizDocID
								AND (OBDI.numUnitHour-ISNULL((SELECT 
																SUM(numUnitHour) 
															FROM 
																ReturnItems 
															WHERE 
																numReturnHeaderID IN (SELECT numReturnHeaderID FROM ReturnHeader WHERE numOppId=@numOppID) 
																AND numOppItemID=OI.numoppitemtCode),0))>0
							)
END