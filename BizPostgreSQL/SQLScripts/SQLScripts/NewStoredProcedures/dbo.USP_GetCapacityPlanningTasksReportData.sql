GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
                    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetCapacityPlanningTasksReportData')
DROP PROCEDURE USP_GetCapacityPlanningTasksReportData
GO
CREATE PROCEDURE [dbo].[USP_GetCapacityPlanningTasksReportData]                             
	@numDomainID AS NUMERIC(18,0)                            
	,@tintProcessType TINYINT
	,@numProcessID NUMERIC(18,0)
	,@vcMilestone VARCHAR(300)
	,@numStageDetailsId NUMERIC(18,0)
	,@vcTaskIDs VARCHAR(200)
	,@vcGradeIDs VARCHAR(200)
	,@dtFromDate DATETIME
	,@dtToDate DATETIME
	,@tintView TINYINT --1:Day, 2:Week, 3:Month
	,@ClientTimeZoneOffset INT
AS                            
BEGIN
	SELECT 
		vcMileStoneName
	FROM
		StagePercentageDetails
	WHERE
		numDomainId = @numDomainID
		AND slp_id = @numProcessID
		AND (ISNULL(@vcMilestone,'') = '' OR vcMileStoneName = @vcMilestone)
	GROUP BY
		vcMileStoneName

	SELECT 
		numStageDetailsId
		,vcMileStoneName
		,vcStageName
	FROM
		StagePercentageDetails
	WHERE
		numDomainId = @numDomainID
		AND slp_id = @numProcessID
		AND (ISNULL(@vcMilestone,'') = '' OR vcMileStoneName = @vcMilestone)
		AND (ISNULL(@numStageDetailsId,0) = 0 OR numStageDetailsId=@numStageDetailsId)


	DECLARE @TEMP TABLE
	(
		numStageDetailsId NUMERIC(18,0)
		,numTaskID NUMERIC(18,0)
		,vcTaskName VARCHAR(300)
		,numTaskTimeInMinutes NUMERIC(18,0)
	)

	INSERT INTO 
		@TEMP
	SELECT
		SPDT.numStageDetailsId
		,SPDT.numTaskId
		,SPDT.vcTaskName
		,(ISNULL(SPDT.numHours,0) * 60) + ISNULL(SPDT.numMinutes,0)
	FROM
		StagePercentageDetails SPD
	INNER JOIN
		StagePercentageDetailsTask SPDT
	ON
		SPD.numStageDetailsId = SPDT.numStageDetailsId
	WHERE
		SPD.numDomainID=@numDomainID
		AND SPD.slp_id = @numProcessID
		AND (ISNULL(@vcMilestone,'') = '' OR SPD.vcMileStoneName = @vcMilestone)
		AND (ISNULL(@numStageDetailsId,0) = 0 OR SPD.numStageDetailsId=@numStageDetailsId)
		AND (ISNULL(@vcTaskIDs,'') = '' OR SPDT.numTaskId IN (SELECT ID FROM dbo.SplitIDs(@vcTaskIDs,',')))

	SELECT * FROM @TEMP

	DECLARE @TempTaskAssignee TABLE
	(
		numWorkOrderID NUMERIC(18,0)
		,numAssignedTo NUMERIC(18,0)			
		,dtLastTaskCompletionTime DATETIME
	)

	DECLARE @TempTimeLog TABLE
	(
		numWrokOrder NUMERIC(18,0)
		,numTaskID NUMERIC(18,0)
		,numReferenceTaskId NUMERIC(18,0)
		,numQtyItemsReq FLOAT
		,numAssignTo NUMERIC(18,0)
		,numProductiveMinutes NUMERIC(18,0)
		,dtDate DATETIME
		,numTotalMinutes NUMERIC(18,0)
	)

	DECLARE @TempTasks TABLE
	(
		ID INT IDENTITY(1,1)
		,numTaskID NUMERIC(18,0)
		,numReferenceTaskId NUMERIC(18,0)
		,numWorkOrderID NUMERIC(18,0)
		,numAssignTo NUMERIC(18,0)
		,numTaskTimeInMinutes NUMERIC(18,0)
		,intTaskType INT
		,dtStartDate DATETIME
		,dtFinishDate DATETIME
		,numQtyItemsReq FLOAT
		,numProcessedQty FLOAT
	)

	INSERT INTO @TempTasks
	(
		numTaskID
		,numReferenceTaskId
		,numWorkOrderID
		,numAssignTo
		,numTaskTimeInMinutes
		,intTaskType
		,dtStartDate
		,numQtyItemsReq
		,numProcessedQty
	)
	SELECT
		SPDT.numTaskId
		,SPDT.numReferenceTaskId
		,WorkOrder.numWOId
		,SPDT.numAssignTo
		,((ISNULL(SPDT.numHours,0) * 60) + ISNULL(SPDT.numMinutes,0)) * (ISNULL(numQtyItemsReq,0) - ISNULL((SELECT SUM(SPDTTL.numProcessedQty) FROM StagePercentageDetailsTaskTimeLog SPDTTL WHERE SPDTTL.numTaskId = SPDT.numTaskId),0))
		,SPDT.intTaskType
		,(CASE	
			WHEN (SELECT COUNT(*) FROM StagePercentageDetailsTaskTimeLog SPDTTL WHERE SPDTTL.numTaskID = SPDT.numTaskId) > 0
			THEN (SELECT MAX(dtActionTime) FROM StagePercentageDetailsTaskTimeLog SPDTTL WHERE SPDTTL.numTaskID = SPDT.numTaskId)
			ELSE ISNULL(WorkOrder.dtmStartDate,WorkOrder.bintCreatedDate)
		END)
		,WorkOrder.numQtyItemsReq
		,ISNULL((SELECT SUM(numProcessedQty) FROM StagePercentageDetailsTaskTimeLog SPDTTL WHERE SPDTTL.numTaskID = SPDT.numTaskId),0)
	FROM
		StagePercentageDetailsTask SPDT
	INNER JOIN
		WorkOrder
	ON
		SPDT.numWorkOrderId = WorkOrder.numWOId
	WHERE
		SPDT.numDomainID=@numDomainID
		AND (SELECT COUNT(*) FROM StagePercentageDetailsTaskTimeLog SPDTTL WHERE SPDTTL.numTaskID = SPDT.numTaskId AND tintAction=4) = 0 -- Task in not finished
		--TODO:uncomment if we have performance hit on live 
		--AND (ISNULL(WorkOrder.dtmStartDate,WorkOrder.bintCreatedDate) BETWEEN @dtFromDate AND @dtToDate OR (SELECT COUNT(*) FROM StagePercentageDetailsTaskTimeLog SPDTTL WHERE SPDTTL.numTaskID = SPDT.numTaskId) > 0)
		AND (ISNULL(numQtyItemsReq,0) - ISNULL((SELECT SUM(SPDTTL.numProcessedQty) FROM StagePercentageDetailsTaskTimeLog SPDTTL WHERE SPDTTL.numTaskId = SPDT.numTaskId),0)) > 0

	INSERT INTO @TempTaskAssignee
	(
		numWorkOrderID
		,numAssignedTo
	)
	SELECT
		numWorkOrderID
		,numAssignTo
	FROM
		@TempTasks
	GROUP BY
		numWorkOrderID
		,numAssignTo

	DECLARE @i INT = 1
	DECLARE @iCount INT 	
	SELECT @iCount = COUNT(*) FROM @TempTasks

	DECLARE @numTempTaskID NUMERIC(18,0)
	DECLARE @numTempParentTaskID NUMERIC(18,0)
	DECLARE @numTaskAssignee NUMERIC(18,0)
	DECLARE @intTaskType INT
	DECLARE @numTotalTaskInMinutes NUMERIC(18,0)
	DECLARE @numWorkOrderID NUMERIC(18,0)
	DECLARE @numQtyItemsReq FLOAT

	DECLARE @numWorkScheduleID NUMERIC(18,0)
	DECLARE @numProductiveTimeInMinutes NUMERIC(18,0)
	DECLARE @tmStartOfDay TIME(7)
	DECLARE @numTimeLeftForDay NUMERIC(18,0)
	DECLARE @vcWorkDays VARCHAR(20)
	DECLARE @dtStartDate DATETIME
		
	WHILE @i <= @iCount
	BEGIN
		SELECT
			@numTempTaskID=numTaskID
			,@numTempParentTaskID=numReferenceTaskId
			,@numTaskAssignee=numAssignTo
			,@numTotalTaskInMinutes=numTaskTimeInMinutes
			,@intTaskType = intTaskType
			,@dtStartDate=dtStartDate
			,@numWorkOrderID=numWorkOrderID
			,@numQtyItemsReq=numQtyItemsReq
		FROM
			@TempTasks 
		WHERE
			ID=@i

		-- GET TASK COMPLETION TIME WHICH WILL BE START DATETIME FOR NEXT TASK
		SELECT
			@numWorkScheduleID = WS.ID
			,@numProductiveTimeInMinutes=(ISNULL(WS.numProductiveHours,0) * 60) + ISNULL(WS.numProductiveMinutes,0)
			,@tmStartOfDay = tmStartOfDay
			,@vcWorkDays=CONCAT(',',vcWorkDays,',')
			,@dtStartDate = DATEADD(MINUTE,@ClientTimeZoneOffset,CONVERT(DATETIME,CONVERT(VARCHAR(8),DATEADD(MINUTE,-@ClientTimeZoneOffset,@dtStartDate),112)) + tmStartOfDay)
		FROM
			WorkSchedule WS
		INNER JOIN
			UserMaster
		ON
			WS.numUserCntID = UserMaster.numUserDetailId
		WHERE 
			WS.numUserCntID = @numTaskAssignee

		IF @intTaskType=1 AND EXISTS (SELECT numAssignedTo FROM @TempTaskAssignee WHERE numWorkOrderID=@numWorkOrderID AND numAssignedTo = @numTaskAssignee AND dtLastTaskCompletionTime IS NOT NULL)
		BEGIN
			SELECT @dtStartDate = dtLastTaskCompletionTime FROM @TempTaskAssignee WHERE numWorkOrderID=@numWorkOrderID AND numAssignedTo = @numTaskAssignee
		END
		ELSE IF EXISTS (SELECT numAssignedTo FROM @TempTaskAssignee WHERE numWorkOrderID=@numWorkOrderID AND numAssignedTo = @numTaskAssignee AND dtLastTaskCompletionTime IS NOT NULL)
		BEGIN
			SELECT @dtStartDate = dtLastTaskCompletionTime FROM @TempTaskAssignee WHERE numWorkOrderID=@numWorkOrderID AND numAssignedTo = @numTaskAssignee
		END

		UPDATE @TempTasks SET dtStartDate=@dtStartDate WHERE ID=@i 

		IF @numProductiveTimeInMinutes > 0 AND @numTotalTaskInMinutes > 0
		BEGIN
			WHILE @numTotalTaskInMinutes > 0
			BEGIN
				-- IF ITS WORKING DAY AND ALSO NOT HOLIDAY THEN PROCEED OR INCRESE DATE TO NEXT DAY
				IF CHARINDEX(CONCAT(',',DATEPART(WEEKDAY,@dtStartDate),','),@vcWorkDays) > 0 AND NOT EXISTS (SELECT ID FROM WorkScheduleDaysOff WHERE numWorkScheduleID=@numWorkScheduleID AND CAST(DATEADD(MINUTE,@ClientTimeZoneOffset * -1,@dtStartDate) AS DATE) BETWEEN dtDayOffFrom AND dtDayOffTo)
				BEGIN
					-- CHECK TIME LEFT FOR DAY BASED
					SET @numTimeLeftForDay = DATEDIFF(MINUTE,@dtStartDate,DATEADD(MINUTE,@numProductiveTimeInMinutes,DATEADD(MINUTE,@ClientTimeZoneOffset,CONVERT(DATETIME,CONVERT(VARCHAR(8),DATEADD(MINUTE,-@ClientTimeZoneOffset,@dtStartDate),112)) + @tmStartOfDay)))

					IF @numTimeLeftForDay > 0
					BEGIN
						INSERT INTO @TempTimeLog
						(
							numWrokOrder
							,numTaskID
							,numReferenceTaskId
							,numQtyItemsReq
							,numAssignTo
							,numProductiveMinutes
							,dtDate
							,numTotalMinutes
						) 
						VALUES 
						(	
							@numWorkOrderID
							,@numTempTaskID
							,@numTempParentTaskID
							,@numQtyItemsReq
							,@numTaskAssignee
							,@numProductiveTimeInMinutes
							,@dtStartDate
							,(CASE WHEN @numTimeLeftForDay > @numTotalTaskInMinutes THEN @numTotalTaskInMinutes ELSE @numTimeLeftForDay END)
						)

						IF @numTimeLeftForDay > @numTotalTaskInMinutes
						BEGIN
							SET @dtStartDate = DATEADD(MINUTE,@numTotalTaskInMinutes,@dtStartDate)
						END
						ELSE
						BEGIN
							SET @dtStartDate = DATEADD(MINUTE,@ClientTimeZoneOffset,CONVERT(DATETIME,CONVERT(VARCHAR(8),DATEADD(MINUTE,-@ClientTimeZoneOffset,DATEADD(DAY,1,@dtStartDate)),112)) + @tmStartOfDay)
						END
							
						SET @numTotalTaskInMinutes = @numTotalTaskInMinutes - @numTimeLeftForDay
					END
					ELSE
					BEGIN
						SET @dtStartDate = DATEADD(MINUTE,@ClientTimeZoneOffset,CONVERT(DATETIME,CONVERT(VARCHAR(8),DATEADD(MINUTE,-@ClientTimeZoneOffset,DATEADD(DAY,1,@dtStartDate)),112)) + @tmStartOfDay)
					END
				END
				ELSE
				BEGIN
					SET @dtStartDate = DATEADD(MINUTE,@ClientTimeZoneOffset,CONVERT(DATETIME,CONVERT(VARCHAR(8),DATEADD(MINUTE,-@ClientTimeZoneOffset,DATEADD(DAY,1,@dtStartDate)),112)) + @tmStartOfDay)					
				END				
			END
		END	

		UPDATE @TempTasks SET dtFinishDate=@dtStartDate WHERE ID=@i 
		UPDATE @TempTaskAssignee SET dtLastTaskCompletionTime=@dtStartDate WHERE numWorkOrderID=@numWorkOrderID AND numAssignedTo=@numTaskAssignee

		SET @i = @i + 1
	END

	IF @tintView = 1
	BEGIN
		SELECT
			numReferenceTaskId AS numTaskID
			,TT.numTaskID AS numActualTaskID
			,WO.numWOId
			,ISNULL(WO.vcWorkOrderName,'-') vcWorkOrderName
			,I.numItemCode
			,ISNULL(I.vcItemName,'-') vcItemName
			,TT.numAssignTo
			,ISNULL(AdditionalContactsInformation.numTeam,0) numTeam
			,ISNULL((SELECT vcGradeId FROM tblStageGradeDetails WHERE tblStageGradeDetails.numTaskID=numReferenceTaskId AND tblStageGradeDetails.numAssigneId=TT.numAssignTo),'') vcGrade
			,ISNULL(TT.numProcessedQty,0) numProcessedQty
			,ISNULL(WO.numQtyItemsReq,0) - ISNULL(TT.numProcessedQty,0) numRemainingQty
			,CONCAT(FORMAT(ISNULL(TT.numTaskTimeInMinutes,0) / 60,'00'),':',FORMAT(ISNULL(TT.numTaskTimeInMinutes,0) % 60,'00')) vcTimeRequired
			,(CASE	
				WHEN (SELECT COUNT(*) FROM StagePercentageDetailsTaskTimeLog SPDTTL WHERE SPDTTL.numTaskID = TT.numTaskId) > 0
				THEN dbo.FormatedDateFromDate(DATEADD(MINUTE,-@ClientTimeZoneOffset,TT.dtStartDate),@numDomainID) + ' ' + CONVERT(VARCHAR(5),DATEADD(MINUTE,-@ClientTimeZoneOffset,TT.dtStartDate), 108) + ' ' + RIGHT(CONVERT(VARCHAR(30),DATEADD(MINUTE,-@ClientTimeZoneOffset,TT.dtStartDate),9),2) 
				ELSE ''
			END) dtStartDate
			,(CASE 
				WHEN TT.dtFinishDate IS NOT NULL
				THEN dbo.FormatedDateFromDate(DATEADD(MINUTE,-@ClientTimeZoneOffset,TT.dtFinishDate),@numDomainID) + ' ' + CONVERT(VARCHAR(5),DATEADD(MINUTE,-@ClientTimeZoneOffset,TT.dtFinishDate), 108) + ' ' + RIGHT(CONVERT(VARCHAR(30),DATEADD(MINUTE,-@ClientTimeZoneOffset,TT.dtFinishDate),9),2)
				ELSE ''
			END) dtFinishDate
			,(CASE 
				WHEN OI.numoppitemtCode > 0 
				THEN (CASE 
						WHEN OI.ItemReleaseDate IS NOT NULL
						THEN dbo.FormatedDateFromDate(CAST(OI.ItemReleaseDate AS DATETIME),@numDomainID) + ' ' + CONVERT(VARCHAR(5),CAST(OI.ItemReleaseDate AS DATETIME), 108) + ' ' + RIGHT(CONVERT(VARCHAR(30),CAST(OI.ItemReleaseDate AS DATETIME),9),2)
						ELSE ''
					END)
				ELSE (CASE 
						WHEN WO.dtmEndDate IS NOT NULL
						THEN dbo.FormatedDateFromDate(DATEADD(MINUTE,-@ClientTimeZoneOffset,WO.dtmEndDate),@numDomainID) + ' ' + CONVERT(VARCHAR(5),DATEADD(MINUTE,-@ClientTimeZoneOffset,WO.dtmEndDate), 108) + ' ' + RIGHT(CONVERT(VARCHAR(30),DATEADD(MINUTE,-@ClientTimeZoneOffset,WO.dtmEndDate),9),2)
						ELSE ''
					END) 
			END) dtReleaseDate
		FROM
			@TEMP T
		INNER JOIN
			@TempTasks TT
		ON
			T.numTaskID = TT.numReferenceTaskId
		INNER JOIN
			AdditionalContactsInformation
		ON
			TT.numAssignTo = AdditionalContactsInformation.numContactId
		INNER JOIN
			WorkOrder WO
		ON
			TT.numWorkOrderID = WO.numWOId
		LEFT JOIN
			OpportunityItems OI
		ON	
			WO.numOppItemID = OI.numoppitemtCode
		INNER JOIN
			Item I
		ON
			WO.numItemCode = I.numItemCode
		WHERE
			TT.numTaskID IN (SELECT TTL.numTaskID FROM @TempTimeLog TTL WHERE dtDate BETWEEN @dtFromDate AND @dtToDate)

		SELECT
			numUserDetailId
			,CONCAT(ISNULL(vcFirstName,'-'),' ',ISNULL(vcLastName,'-')) vcUserName
			,ISNULL(numTeam,0) numTeam
			,CONCAT('[',STUFF((SELECT 
						CONCAT(',{"numTaskID":',numTaskId,',','"vcGradeId":"',vcGradeId,'"}')
					FROM 
						tblStageGradeDetails 
					WHERE 
						numAssigneId=UserMaster.numUserDetailId
					FOR XML PATH('')),1,1,''),']') vcTaskGrades
			,ISNULL((SELECT 
						(CASE WHEN ISNULL(TTL.numProductiveMinutes,0) > 0 THEN (SUM(TTL.numTotalMinutes) * 100) / TTL.numProductiveMinutes ELSE 0 END) numCapacityLoad 
					FROM 
						@TempTimeLog TTL 
					WHERE 
						ISNULL(TTL.numProductiveMinutes,0) > 0
						AND TTL.numAssignTo = UserMaster.numUserDetailId
						AND TTL.dtDate BETWEEN @dtFromDate AND @dtToDate
					GROUP BY 
						TTL.numAssignTo
						,TTL.numProductiveMinutes),0) numCapacityLoad
		FROM
			UserMaster
		INNER JOIN
			AdditionalContactsInformation
		ON
			UserMaster.numUserDetailId = AdditionalContactsInformation.numContactId
		WHERE
			UserMaster.numDomainID=@numDomainID
			AND AdditionalContactsInformation.numDomainID = @numDomainID
			AND bitActivateFlag = 1

		SELECT
			numListItemID numTeam
			,ISNULL(vcData,'-') vcTeamName
			,ISNULL((SELECT 
						AVG(numCapacityLoad) 
					FROM 
					(
						SELECT 
							(CASE WHEN ISNULL(TTL.numProductiveMinutes,0) > 0 THEN (SUM(TTL.numTotalMinutes) * 100) / TTL.numProductiveMinutes ELSE 0 END) numCapacityLoad 
						FROM 
							@TempTimeLog TTL 
						INNER JOIN
							AdditionalContactsInformation ADC
						ON
							TTL.numAssignTo = ADC.numContactId
						WHERE 
							ADC.numTeam	= ListDetails.numListItemID
							AND ISNULL(TTL.numProductiveMinutes,0) > 0
							AND TTL.numAssignTo = ADC.numContactId
							AND TTL.dtDate BETWEEN @dtFromDate AND @dtToDate
						GROUP BY 
							TTL.numAssignTo
							,TTL.numProductiveMinutes
					) TEMP),0) numCapacityLoad
		FROM
			ListDetails
		WHERE
			numListID = 35
			AND numDomainID=@numDomainID

		SELECT
			T.numTaskID
			,ISNULL((SELECT AVG(numCapacityLoad) FROM (SELECT (CASE WHEN ISNULL(TTL.numProductiveMinutes,0) > 0 THEN (SUM(TTL.numTotalMinutes) * 100) / TTL.numProductiveMinutes ELSE 0 END) numCapacityLoad FROM @TempTimeLog TTL INNER JOIN tblStageGradeDetails ON TTL.numAssignTo = tblStageGradeDetails.numAssigneId WHERE tblStageGradeDetails.numTaskID = T.numTaskID AND tblStageGradeDetails.vcGradeId = 'A+' AND TTL.dtDate BETWEEN @dtFromDate AND @dtToDate AND ISNULL(TTL.numProductiveMinutes,0) > 0 GROUP BY TTL.numAssignTo,TTL.numProductiveMinutes) TEMP),0) AS [APlus]
			,ISNULL((SELECT AVG(numCapacityLoad) FROM (SELECT (CASE WHEN ISNULL(TTL.numProductiveMinutes,0) > 0 THEN (SUM(TTL.numTotalMinutes) * 100) / TTL.numProductiveMinutes ELSE 0 END) numCapacityLoad FROM @TempTimeLog TTL INNER JOIN tblStageGradeDetails ON TTL.numAssignTo = tblStageGradeDetails.numAssigneId WHERE tblStageGradeDetails.numTaskID = T.numTaskID AND tblStageGradeDetails.vcGradeId = 'A' AND TTL.dtDate BETWEEN @dtFromDate AND @dtToDate AND ISNULL(TTL.numProductiveMinutes,0) > 0 GROUP BY TTL.numAssignTo,TTL.numProductiveMinutes) TEMP),0) AS [A]
			,ISNULL((SELECT AVG(numCapacityLoad) FROM (SELECT (CASE WHEN ISNULL(TTL.numProductiveMinutes,0) > 0 THEN (SUM(TTL.numTotalMinutes) * 100) / TTL.numProductiveMinutes ELSE 0 END) numCapacityLoad FROM @TempTimeLog TTL INNER JOIN tblStageGradeDetails ON TTL.numAssignTo = tblStageGradeDetails.numAssigneId WHERE tblStageGradeDetails.numTaskID = T.numTaskID AND tblStageGradeDetails.vcGradeId = 'A-' AND TTL.dtDate BETWEEN @dtFromDate AND @dtToDate AND ISNULL(TTL.numProductiveMinutes,0) > 0 GROUP BY TTL.numAssignTo,TTL.numProductiveMinutes) TEMP),0) AS [AMinus]
			,ISNULL((SELECT AVG(numCapacityLoad) FROM (SELECT (CASE WHEN ISNULL(TTL.numProductiveMinutes,0) > 0 THEN (SUM(TTL.numTotalMinutes) * 100) / TTL.numProductiveMinutes ELSE 0 END) numCapacityLoad FROM @TempTimeLog TTL INNER JOIN tblStageGradeDetails ON TTL.numAssignTo = tblStageGradeDetails.numAssigneId WHERE tblStageGradeDetails.numTaskID = T.numTaskID AND tblStageGradeDetails.vcGradeId = 'B+' AND TTL.dtDate BETWEEN @dtFromDate AND @dtToDate AND ISNULL(TTL.numProductiveMinutes,0) > 0 GROUP BY TTL.numAssignTo,TTL.numProductiveMinutes) TEMP),0) AS [BPlus]
			,ISNULL((SELECT AVG(numCapacityLoad) FROM (SELECT (CASE WHEN ISNULL(TTL.numProductiveMinutes,0) > 0 THEN (SUM(TTL.numTotalMinutes) * 100) / TTL.numProductiveMinutes ELSE 0 END) numCapacityLoad FROM @TempTimeLog TTL INNER JOIN tblStageGradeDetails ON TTL.numAssignTo = tblStageGradeDetails.numAssigneId WHERE tblStageGradeDetails.numTaskID = T.numTaskID AND tblStageGradeDetails.vcGradeId = 'B' AND TTL.dtDate BETWEEN @dtFromDate AND @dtToDate AND ISNULL(TTL.numProductiveMinutes,0) > 0 GROUP BY TTL.numAssignTo,TTL.numProductiveMinutes) TEMP),0) AS [B]
			,ISNULL((SELECT AVG(numCapacityLoad) FROM (SELECT (CASE WHEN ISNULL(TTL.numProductiveMinutes,0) > 0 THEN (SUM(TTL.numTotalMinutes) * 100) / TTL.numProductiveMinutes ELSE 0 END) numCapacityLoad FROM @TempTimeLog TTL INNER JOIN tblStageGradeDetails ON TTL.numAssignTo = tblStageGradeDetails.numAssigneId WHERE tblStageGradeDetails.numTaskID = T.numTaskID AND tblStageGradeDetails.vcGradeId = 'B-' AND TTL.dtDate BETWEEN @dtFromDate AND @dtToDate AND ISNULL(TTL.numProductiveMinutes,0) > 0 GROUP BY TTL.numAssignTo,TTL.numProductiveMinutes) TEMP),0) AS [BMinus]
			,ISNULL((SELECT AVG(numCapacityLoad) FROM (SELECT (CASE WHEN ISNULL(TTL.numProductiveMinutes,0) > 0 THEN (SUM(TTL.numTotalMinutes) * 100) / TTL.numProductiveMinutes ELSE 0 END) numCapacityLoad FROM @TempTimeLog TTL INNER JOIN tblStageGradeDetails ON TTL.numAssignTo = tblStageGradeDetails.numAssigneId WHERE tblStageGradeDetails.numTaskID = T.numTaskID AND tblStageGradeDetails.vcGradeId = 'C+' AND TTL.dtDate BETWEEN @dtFromDate AND @dtToDate AND ISNULL(TTL.numProductiveMinutes,0) > 0 GROUP BY TTL.numAssignTo,TTL.numProductiveMinutes) TEMP),0) AS [CPlus]
			,ISNULL((SELECT AVG(numCapacityLoad) FROM (SELECT (CASE WHEN ISNULL(TTL.numProductiveMinutes,0) > 0 THEN (SUM(TTL.numTotalMinutes) * 100) / TTL.numProductiveMinutes ELSE 0 END) numCapacityLoad FROM @TempTimeLog TTL INNER JOIN tblStageGradeDetails ON TTL.numAssignTo = tblStageGradeDetails.numAssigneId WHERE tblStageGradeDetails.numTaskID = T.numTaskID AND tblStageGradeDetails.vcGradeId = 'C' AND TTL.dtDate BETWEEN @dtFromDate AND @dtToDate AND ISNULL(TTL.numProductiveMinutes,0) > 0 GROUP BY TTL.numAssignTo,TTL.numProductiveMinutes) TEMP),0) AS [C]
			,ISNULL((SELECT AVG(numCapacityLoad) FROM (SELECT (CASE WHEN ISNULL(TTL.numProductiveMinutes,0) > 0 THEN (SUM(TTL.numTotalMinutes) * 100) / TTL.numProductiveMinutes ELSE 0 END) numCapacityLoad FROM @TempTimeLog TTL INNER JOIN tblStageGradeDetails ON TTL.numAssignTo = tblStageGradeDetails.numAssigneId WHERE tblStageGradeDetails.numTaskID = T.numTaskID AND tblStageGradeDetails.vcGradeId = 'C-' AND TTL.dtDate BETWEEN @dtFromDate AND @dtToDate AND ISNULL(TTL.numProductiveMinutes,0) > 0 GROUP BY TTL.numAssignTo,TTL.numProductiveMinutes) TEMP),0) AS [CMinus]
		FROM
			@TEMP T
	END
	ELSE 
	IF @tintView = 2 OR @tintView = 3
	BEGIN
		DECLARE @TEMPResult TABLE
		(
			dtDate DATE
			,numTaskID NUMERIC(18,0)
			,numTotalTasks NUMERIC(18,0)
			,numMaxTasks NUMERIC(18,0)
			,numAssigneeCapacityLoad NUMERIC(18,0)
			,numGradeCapacity NUMERIC(18,0)
			,numGradeCapacityLoad NUMERIC(18,0)
		)

		WHILE @dtFromDate <= @dtToDate
		BEGIN
			INSERT INTO @TEMPResult
			(
				dtDate
				,numTaskID
				,numTotalTasks
				,numMaxTasks
				,numAssigneeCapacityLoad
				,numGradeCapacity
				,numGradeCapacityLoad
			)
			SELECT
				@dtFromDate
				,numTaskID
				,ISNULL((SELECT 
							SUM(TTL.numQtyItemsReq) 
						FROM 
							@TempTimeLog TTL 
						WHERE 
							TTL.numReferenceTaskId=T.numTaskID 
							AND CAST(dtDate AS DATE) = CAST(@dtFromDate AS DATE)
						GROUP BY
							TTL.numReferenceTaskId),0)
				,ISNULL((SELECT 
							SUM(numMaxTask) 
						FROM
						(
							SELECT 
								(CASE 
									WHEN ISNULL(TTL.numProductiveMinutes,0) > ISNULL(SUM(TTL.numTotalMinutes),0)
									THEN ((ISNULL(TTL.numProductiveMinutes,0) - ISNULL(SUM(TTL.numTotalMinutes),0)) / ISNULL(T.numTaskTimeInMinutes,0))
									ELSE ISNULL(TTL.numProductiveMinutes,0) / ISNULL(T.numTaskTimeInMinutes,0)
								END) numMaxTask 
							FROM 
								@TempTimeLog TTL
							WHERE 
								ISNULL(TTL.numProductiveMinutes,0) > 0
								AND CAST(dtDate AS DATE) = CAST(@dtFromDate AS DATE)
							GROUP BY 
								TTL.numAssignTo
								,TTL.numProductiveMinutes
							) TEMP),0)
				,ISNULL((SELECT 
							AVG(numCapacityLoad) 
						FROM 
						(
							SELECT 
								(CASE WHEN ISNULL(TTL.numProductiveMinutes,0) > 0 THEN (SUM(TTL.numTotalMinutes) * 100) / TTL.numProductiveMinutes ELSE 0 END) numCapacityLoad 
							FROM 
								@TempTimeLog TTL 
							WHERE 
								ISNULL(TTL.numProductiveMinutes,0) > 0
								AND CAST(dtDate AS DATE) = CAST(@dtFromDate AS DATE) 
							GROUP BY 
								TTL.numAssignTo
								,TTL.numProductiveMinutes
						) TEMP),0)
				,ISNULL((SELECT 
							SUM(numMaxTask) 
						FROM
						(
							SELECT
								(CASE 
									WHEN (ISNULL(WS.numProductiveHours,0) * 60 + ISNULL(WS.numProductiveMinutes,0)) > ISNULL(TTL.numTotalMinutes,0)
									THEN (((ISNULL(WS.numProductiveHours,0) * 60 + ISNULL(WS.numProductiveMinutes,0)) - ISNULL(TTL.numTotalMinutes,0)) / ISNULL(T.numTaskTimeInMinutes,0))
									ELSE (ISNULL(WS.numProductiveHours,0) * 60 + ISNULL(WS.numProductiveMinutes,0)) / ISNULL(T.numTaskTimeInMinutes,0)
								END) numMaxTask
							FROM
								tblStageGradeDetails TSGD 
							INNER JOIN
								WorkSchedule WS
							ON
								TSGD.numAssigneId = WS.numUserCntID
							OUTER APPLY
							(
								SELECT
									SUM(numTotalMinutes) numTotalMinutes
								FROM 
									@TempTimeLog TTL
								WHERE 
									CAST(dtDate AS DATE) = CAST(@dtFromDate AS DATE) 
									AND TTL.numAssignTo = TSGD.numAssigneId
							) TTL
							WHERE 
								TSGD.numTaskId=T.numTaskID 
								AND (ISNULL(WS.numProductiveHours,0) * 60 + ISNULL(WS.numProductiveMinutes,0)) > 0
								AND (ISNULL(@vcGradeIDs,'') = '' OR vcGradeId IN (SELECT OutParam FROM dbo.SplitString(@vcGradeIDs,',')))
							GROUP BY 
								TSGD.numAssigneId
								,WS.numProductiveHours
								,WS.numProductiveMinutes
								,TTL.numTotalMinutes) TEMP),0)
				,ISNULL((SELECT 
							AVG(numCapacityLoad) 
						FROM
						(
							SELECT
								(CASE WHEN (ISNULL(WS.numProductiveHours,0) * 60 + ISNULL(WS.numProductiveMinutes,0)) > 0 THEN (ISNULL(TTL.numTotalMinutes,0) * 100) / (ISNULL(WS.numProductiveHours,0) * 60 + ISNULL(WS.numProductiveMinutes,0)) ELSE 0 END) numCapacityLoad
							FROM
								tblStageGradeDetails TSGD 
							INNER JOIN
								WorkSchedule WS
							ON
								TSGD.numAssigneId = WS.numUserCntID
							OUTER APPLY
							(
								SELECT
									SUM(numTotalMinutes) numTotalMinutes
								FROM 
									@TempTimeLog TTL
								WHERE 
									CAST(dtDate AS DATE) = CAST(@dtFromDate AS DATE) 
									AND TTL.numAssignTo = TSGD.numAssigneId
							) TTL
							WHERE 
								TSGD.numTaskId = T.numTaskId
								AND (ISNULL(WS.numProductiveHours,0) * 60 + ISNULL(WS.numProductiveMinutes,0)) > 0
								AND (ISNULL(@vcGradeIDs,'') = '' OR vcGradeId IN (SELECT OutParam FROM dbo.SplitString(@vcGradeIDs,',')))
							GROUP BY 
								TSGD.numAssigneId
								,WS.numProductiveHours
								,WS.numProductiveMinutes
								,TTL.numTotalMinutes) TEMP),0)
			FROM
				@TEMP T
			
			SET @dtFromDate = DATEADD(DAY,1,@dtFromDate)
		END

		--SELECT * FROM @TempTimeLog ORDER BY numWrokOrder,numTaskID,dtDate
		SELECT * FROM @TEMPResult
	END
END
GO