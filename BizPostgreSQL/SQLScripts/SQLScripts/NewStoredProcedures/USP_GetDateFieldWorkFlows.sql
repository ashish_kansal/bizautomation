GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_GetDateFieldWorkFlows' ) 
    DROP PROCEDURE USP_GetDateFieldWorkFlows
GO
CREATE PROCEDURE USP_GetDateFieldWorkFlows
AS 
BEGIN 
		
		SELECT * FROM WorkFlowMaster WFM		
		WHERE ISNULL(WFM.vcDateField,'') <> '' AND WFM.bitActive = 1

END


