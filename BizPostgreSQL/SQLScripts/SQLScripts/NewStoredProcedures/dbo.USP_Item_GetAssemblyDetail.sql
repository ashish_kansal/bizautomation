GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
                                    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_Item_GetAssemblyDetail')
DROP PROCEDURE USP_Item_GetAssemblyDetail
GO
CREATE PROCEDURE [dbo].[USP_Item_GetAssemblyDetail]
(
	@numDomainID NUMERIC(18,0)
	,@numItemCode NUMERIC(18,0)
	,@numWarehouseItemID NUMERIC(18,0)
	,@ClientTimeZoneOffset INT
)
AS
BEGIN
	DECLARE @numTempWarehouseID NUMERIC(18,0)
	SELECT @numTempWarehouseID=numWareHouseID FROM WareHouseItems WHERE numWareHouseItemID=@numWarehouseItemID

	SELECT
		dbo.FormatedDateFromDate(DATEADD(MINUTE,-1*@ClientTimeZoneOffset,GETUTCDATE()),@numDomainID) AS dtShipDate
		,dbo.fn_GetAssemblyPossibleWOQty(@numItemCode,@numWarehouseItemID) AS numMaxWOQty
		,CASE WHEN ISNULL(Item.bitCalAmtBasedonDepItems,0) = 1 THEN ISNULL((SELECT monMSRPPrice FROM dbo.fn_GetKitAssemblyCalculatedPrice(@numDomainID,0,@numItemCode,1,@numWarehouseItemID,ISNULL(tintKitAssemblyPriceBasedOn,1),0,0,'',0,1)),0) ELSE ISNULL(monListPrice,0) END AS monPrice 
	FROM
		Item
	WHERE
		numDomainID=@numDomainID
		AND numItemCode = @numItemCode

	SELECT 
		ISNULL(ItemDetails.sintOrder,0)
		,Item.numItemCode
		,CONCAT(Item.vcItemName,(CASE WHEN LEN(ISNULL(Item.vcSKU,'')) > 0 THEN CONCAT(' (',Item.vcSKU,')')  ELSE '' END)) vcItemName
		,CAST((1 * ISNULL(ItemDetails.numQtyItemsReq,0) * ISNULL(dbo.fn_UOMConversion(ISNULL(ItemDetails.numUOMID,0), Item.numItemCode, @numDomainID, ISNULL(Item.numBaseUnit, 0)),1)) AS FLOAT) numQuantity
		,ISNULL(Item.monListPrice,0) monListPrice
	FROM 
		ItemDetails
	INNER JOIN
		Item
	ON
		ItemDetails.numChildItemID = Item.numItemCode
	WHERE
		numItemKitID=@numItemCode
END
GO