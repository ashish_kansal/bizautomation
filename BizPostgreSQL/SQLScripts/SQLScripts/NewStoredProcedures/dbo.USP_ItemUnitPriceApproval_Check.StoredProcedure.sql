GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_ItemUnitPriceApproval_Check' ) 
    DROP PROCEDURE USP_ItemUnitPriceApproval_Check
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author: Sandeep Patel
-- Create date: 1 July 2014
-- Description:	Checks wheather unit price approval is required or not for item
-- =============================================
CREATE PROCEDURE USP_ItemUnitPriceApproval_Check
	@numDomainID numeric(18,0),
	@numDivisionID numeric(18,0),
	@numItemCode numeric(18,0),
	@numQuantity FLOAT,
	@numUnitPrice float,
	@numTotalAmount float,
	@numCost FLOAT,
	@numWarehouseItemID numeric(18,0),
	@numCurrencyID NUMERIC(18,0)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	DECLARE @numItemClassification NUMERIC(18,0)
	DECLARE @numAbovePercent FLOAT
	DECLARE @numAboveField FLOAT
	DECLARE @numBelowPercent FLOAT
	DECLARE @numBelowField FLOAT
	DECLARE @numDefaultCost TINYINT
	DECLARE @bitCostApproval BIT
	DECLARE @bitListPriceApproval BIT
	DECLARE @numCostDomain INT
	DECLARE @tintKitAssemblyPriceBasedOn TINYINT
	DECLARE @bitCalAmtBasedonDepItems BIT
	DECLARE @numBaseCurrency NUMERIC(18,0)
	DECLARE @fltExchangeRate FLOAT
	
	SELECT 
		@numCostDomain = numCost
		,@numBaseCurrency=ISNULL(numCurrencyID,0) 
	FROM 
		Domain 
	WHERE 
		numDomainId = @numDomainID

	IF @numBaseCurrency = @numCurrencyID
	BEGIN
		SET @fltExchangeRate = 1
	END
	ELSE
	BEGIN
		SET @fltExchangeRate = ISNULL((SELECT fltExchangeRate FROM Currency WHERE numDomainID=@numDomainID AND numCurrencyID=@numCurrencyID),1)

		IF ISNULL(@fltExchangeRate,0) = 0
		BEGIN
			SET @fltExchangeRate = 1
		END
	END

	SET @numUnitPrice = @numUnitPrice * @fltExchangeRate
	
	SELECT 
		@numItemClassification=ISNULL(numItemClassification,0)
		,@tintKitAssemblyPriceBasedOn=ISNULL(tintKitAssemblyPriceBasedOn,1)
		,@bitCalAmtBasedonDepItems=ISNULL(bitCalAmtBasedonDepItems,0) 
	FROM 
		Item 
	WHERE 
		numItemCode=@numItemCode

	SET @fltExchangeRate = ISNULL((SELECT fltExchangeRate FROM Currency WHERE numDomainID=@numDomainID AND numCurrencyID=@numCurrencyID),1)

	IF ISNULL(@fltExchangeRate,0) = 0
	BEGIN
		SET @fltExchangeRate = 1
	END

	IF ISNULL(@numItemClassification,0) > 0 AND EXISTS (SELECT numApprovalRuleID FROM ApprovalProcessItemsClassification WHERE numDomainID=@numDomainID AND numListItemID=@numItemClassification)
	BEGIN
		SELECT
			@numAbovePercent = ISNULL(numAbovePercent,0)
			,@numBelowPercent = ISNULL(numBelowPercent,0)
			,@numBelowField = ISNULL(numBelowPriceField,0)
			,@numAboveField=ISNULL(@numCostDomain,1)
			,@bitCostApproval=ISNULL(bitCostApproval,0)
			,@bitListPriceApproval=ISNULL(bitListPriceApproval,0)
		FROM
			ApprovalProcessItemsClassification
		WHERE
			numDomainId=@numDomainID
			AND ISNULL(numListItemID,0)=@numItemClassification
	END
	ELSE
	BEGIN
		SELECT
			@numAbovePercent = ISNULL(numAbovePercent,0)
			,@numBelowPercent = ISNULL(numBelowPercent,0)
			,@numBelowField = ISNULL(numBelowPriceField,0)
			,@numAboveField=ISNULL(@numCostDomain,1)
			,@bitCostApproval=ISNULL(bitCostApproval,0)
			,@bitListPriceApproval=ISNULL(bitListPriceApproval,0)
		FROM
			ApprovalProcessItemsClassification
		WHERE
			numDomainId=@numDomainID
			AND ISNULL(numListItemID,0)=0
	END
	/*SELECT
		@numAbovePercent = ISNULL(numAbovePercent,0)
		,@numBelowPercent = ISNULL(numBelowPercent,0)
		,@numBelowField = ISNULL(numBelowPriceField,0)
		,@numAboveField=ISNULL(numCost,1)
		,@bitCostApproval=ISNULL(bitCostApproval,0)
		,@bitListPriceApproval=ISNULL(bitListPriceApproval,0)
	FROM
		Domain
	WHERE
		numDomainId=@numDomainID*/


	DECLARE @IsApprovalRequired BIT
	DECLARE @ItemAbovePrice FLOAT
	DECLARE @ItemBelowPrice FLOAT

	SET @IsApprovalRequired = 0
	SET @ItemAbovePrice = 0
	SET @ItemBelowPrice = 0

	IF @numQuantity > 0
		SET @numUnitPrice = (@numTotalAmount / @numQuantity)
	ELSE
		SET @numUnitPrice = 0

	DECLARE @TEMPPrice TABLE
	(
		bitSuccess BIT
		,monPrice DECIMAL(20,5)
	)


	IF @numAboveField > 0 AND ISNULL(@bitCostApproval,0) = 1
	BEGIN
		IF (SELECT COUNT(*) FROM Item WHERE numDomainID = @numDomainID AND numItemCode = @numItemCode  AND (bitKitParent = 1 OR bitAssembly = 1)) > 0 AND @bitCalAmtBasedonDepItems=1
		BEGIN
			DELETE FROM @TEMPPrice

			INSERT INTO @TEMPPrice
			(
				bitSuccess
				,monPrice
			)
			SELECT
				bitSuccess
				,monPrice
			FROM
				dbo.fn_GetKitAssemblyCalculatedPrice(@numDomainID,@numDivisionID,@numItemCode,@numQuantity,@numWarehouseItemID,@tintKitAssemblyPriceBasedOn,0,0,'',0,1)

			IF (SELECT bitSuccess FROM @TEMPPrice) = 1
			BEGIN
				SET @ItemAbovePrice = (SELECT monPrice FROM @TEMPPrice)
			END
			ELSE
			BEGIN
				RAISERROR('SELECTED_WAREHOUSE_IS_NOT_AVAILABLE_FOR_ALL_CHILD_ITEMS',16,1)
			END
		END
		ELSE
		BEGIN
			IF @numAboveField = 3 -- Primaty Vendor Cost
				SELECT @ItemAbovePrice = ISNULL(monCost,0) FROM Vendor WHERE numItemCode = @numItemCode AND numVendorID = (SELECT numVendorID FROM Item WHERE numItemCode = @numItemCode)
			ELSE IF @numAboveField = 2 -- Product & Sevices Cost
				SELECT @ItemAbovePrice = ISNULL(@numCost,0)
			ELSE IF @numAboveField = 1 -- Average Cost
				SELECT @ItemAbovePrice = (CASE WHEN ISNULL(bitVirtualInventory,0) = 1 THEN 0 ELSE ISNULL(monAverageCost,0) END) FROM Item WHERE numDomainID = @numDomainID AND numItemCode = @numItemCode
		END

		IF @ItemAbovePrice > 0
		BEGIN
			IF @numUnitPrice < (@ItemAbovePrice + (@ItemAbovePrice * (@numAbovePercent/100)))
				SET @IsApprovalRequired = 1
		END
		ELSE IF @ItemAbovePrice = 0
		BEGIN
			SET @IsApprovalRequired = 1
		END
	END

	IF @IsApprovalRequired = 0 AND @numBelowField > 0  AND ISNULL(@bitListPriceApproval,0) = 1
	BEGIN
		IF @numBelowField = 1 -- List Price
		BEGIN
			IF (SELECT ISNULL(charItemType,'') AS charItemType FROM Item WHERE numDomainID = @numDomainID AND numItemCode = @numItemCode) = 'P' -- Inventory Item
				IF (SELECT COUNT(*) FROM Item WHERE numDomainID = @numDomainID AND numItemCode = @numItemCode  AND (bitKitParent = 1 OR bitAssembly = 1)) > 0 AND @bitCalAmtBasedonDepItems=1
				BEGIN
					DELETE FROM @TEMPPrice

					INSERT INTO @TEMPPrice
					(
						bitSuccess
						,monPrice
					)
					SELECT
						bitSuccess
						,monPrice
					FROM
						dbo.fn_GetKitAssemblyCalculatedPrice(@numDomainID,@numDivisionID,@numItemCode,@numQuantity,@numWarehouseItemID,@tintKitAssemblyPriceBasedOn,0,0,'',0,1)

					IF (SELECT bitSuccess FROM @TEMPPrice) = 1
					BEGIN
						SET @ItemBelowPrice = (SELECT monPrice FROM @TEMPPrice)
					END
					ELSE
					BEGIN
						RAISERROR('SELECTED_WAREHOUSE_IS_NOT_AVAILABLE_FOR_ALL_CHILD_ITEMS',16,1)
					END
				END
				ELSE
				BEGIN
					SELECT @ItemBelowPrice = ISNULL(monWListPrice,0) FROM WareHouseItems WHERE numDomainID = @numDomainID AND numItemID = @numItemCode AND numWareHouseItemID = @numWarehouseItemID
				END
			ELSE
				SELECT @ItemBelowPrice = ISNULL(monListPrice,0) FROM Item WHERE numDomainID = @numDomainID AND numItemCode = @numItemCode
		END
		ELSE IF @numBelowField = 2 -- Price Rule
		BEGIN
			/* Check if valid price book rules exists for sales in domain */
			IF (SELECT COUNT(*) FROM PriceBookRules WHERE numDomainId = @numDomainID AND tintRuleFor = 1 AND tintStep2 > 0 AND tintStep3 > 0) > 0
			BEGIN
				DECLARE @i INT = 0
				DECLARE @Count int = 0
				DECLARE @tempPriority INT
				DECLARE @tempNumPriceRuleID NUMERIC(18,0)
				DECLARE @numPriceRuleIDApplied NUMERIC(18,0) = 0
				DECLARE @TempTable TABLE (numID INT, numRuleID numeric(18,0), numPriority INT)

				INSERT INTO 
					@TempTable
				SELECT 
					ROW_NUMBER() OVER (ORDER BY PriceBookPriorities.Priority) AS id,
					numPricRuleID,
					PriceBookPriorities.Priority 
				FROM 
					PriceBookRules
				INNER JOIN
					PriceBookPriorities
				ON
					PriceBookRules.tintStep2 = PriceBookPriorities.Step2Value AND
					PriceBookRules.tintStep3 = PriceBookPriorities.Step3Value
				WHERE 
					PriceBookRules.numDomainId = @numDomainID AND 
					PriceBookRules.tintRuleFor = 1 AND 
					PriceBookRules.tintStep2 > 0 AND PriceBookRules.tintStep3 > 0
				ORDER BY
					PriceBookPriorities.Priority

				SELECT @Count = COUNT(*) FROM @TempTable

				/* Loop all price rule with priority */
				WHILE (@i < @count)
				BEGIN
					SELECT @tempNumPriceRuleID = numRuleID, @tempPriority = numPriority FROM @TempTable WHERE numID = (@i + 1)
					
					/* IF proprity is 9 then price rule is applied to all items and all customers. 
					So price rule must be applied to item or not.*/
					IF @tempPriority = 9
					BEGIN
						SET @numPriceRuleIDApplied = @tempNumPriceRuleID
						BREAK
					END
					/* Check if current item exists in rule. if eixts then exit loop with rule id else continie loop. if item not exist in any rule then nothing to check */
					ELSE
					BEGIN
						DECLARE @isRuleApplicable BIT = 0
						EXEC @isRuleApplicable = dbo.CheckIfPriceRuleApplicableToItem @numRuleID = @tempNumPriceRuleID, @numItemID = @numItemCode, @numDivisionID = @numDivisionID 

						IF @isRuleApplicable = 1
						BEGIN
							SET @numPriceRuleIDApplied = @tempNumPriceRuleID
							BREAK
						END
					END

					SET @i = @i + 1
				END

				/* If @numPriceRuleIDApplied > 0 Get final unit price limit after applying below rule */
				PRINT @numPriceRuleIDApplied
				IF @numPriceRuleIDApplied > 0
				BEGIN
					EXEC @ItemBelowPrice = GetUnitPriceAfterPriceRuleApplication @numRuleID = @numPriceRuleIDApplied, @numDomainID = @numDomainID, @numItemCode = @numItemCode, @numQuantity = @numQuantity, @numWarehouseItemID = @numWarehouseItemID, @numDivisionID = @numDivisionID
				END
			END
		END
		ELSE IF @numBelowField = 3 -- Price Level
		BEGIN
			EXEC @ItemBelowPrice = GetUnitPriceAfterPriceLevelApplication @numDomainID = @numDomainID, @numItemCode = @numItemCode, @numQuantity = @numQuantity, @numWarehouseItemID = @numWarehouseItemID, @isPriceRule = 0, @numPriceRuleID = 0, @numDivisionID = @numDivisionID
		END
		
		IF @numUnitPrice < (@ItemBelowPrice - (@ItemBelowPrice * (@numBelowPercent/100)))
				SET @IsApprovalRequired = 1
	END

	SELECT @IsApprovalRequired
END
GO