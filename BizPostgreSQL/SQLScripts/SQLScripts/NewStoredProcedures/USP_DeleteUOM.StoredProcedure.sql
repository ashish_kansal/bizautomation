GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_DeleteUOM')
DROP PROCEDURE USP_DeleteUOM
GO
-- =============================================
-- Author:		<Author,,Sachin Sadhu>
-- Create date: <Create Date,,8thJuly2014>
-- Description:	<Description,, Delete UOM records>
-- =============================================
Create PROCEDURE [dbo].[USP_DeleteUOM]
	-- Add the parameters for the stored procedure here
	 @numDomainID as numeric(9),
	 @numUOMId as  numeric(18)          
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
SET NOCOUNT ON;

IF Exists(Select 'col1' From Item c Where (c.numSaleUnit =@numUOMId) or (c.numSaleUnit =@numUOMId) ) 				  
  Begin

    RaisError('Child Record Exists,Can''t perform delete operation', 16, 1);
  
  End
ELSE IF Exists(Select 'col1' From OpportunityItems c Where (c.numUOMId =@numUOMId)  ) 				  
  Begin

    RaisError('Child Record Exists,Can''t perform delete operation', 16, 1);
  
  End
ELSE
  BEGIN
   -- Insert statements for procedure here
		Delete  from UOM where numDomainId=@numDomainID and numUOMId=@numUOMId
  END

  
   --154/2798
END
GO

