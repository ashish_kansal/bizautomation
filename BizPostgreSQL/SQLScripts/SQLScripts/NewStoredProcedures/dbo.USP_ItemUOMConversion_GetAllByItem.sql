SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ItemUOMConversion_GetAllByItem')
DROP PROCEDURE dbo.USP_ItemUOMConversion_GetAllByItem
GO
CREATE PROCEDURE [dbo].[USP_ItemUOMConversion_GetAllByItem]
(
	@numDomainID NUMERIC(18,0),
	@numItemCode NUMERIC(18,0)
)
AS 
BEGIN
	SELECT numBaseUnit FROM Item WHERE numDomainID = @numDomainID AND numItemCode = @numItemCode

	SELECT 
		ItemUOMConversion.numItemUOMConvID,
		ItemUOMConversion.numDomainID,
		ItemUOMConversion.numItemCode,
		ItemUOMConversion.numSourceUOM,
		SourceUOM.vcUnitName AS SourceUOM,
		ItemUOMConversion.numTargetUOM,
		TargetUOM.vcUnitName AS TargetUOM,
		ItemUOMConversion.numTargetUnit
	FROM
		ItemUOMConversion 
	INNER JOIN
		UOM AS SourceUOM
	ON
		ItemUOMConversion.numSourceUOM = SourceUOM.numUOMId
	INNER JOIN
		UOM AS TargetUOM
	ON
		ItemUOMConversion.numTargetUOM = TargetUOM.numUOMId
	WHERE
		ItemUOMConversion.numDomainID = @numDomainID
		AND ItemUOMConversion.numItemCode = @numItemCode
END