GO
/****** Object:  StoredProcedure [dbo].[USP_GET_PACKAGING_RULE_AS_PER_ORDER_ITEMS]    Script Date: 30/07/2012 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_GET_PACKAGING_RULE_AS_PER_ORDER_ITEMS' ) 
    DROP PROCEDURE USP_GET_PACKAGING_RULE_AS_PER_ORDER_ITEMS
GO
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

/*
EXEC dbo.USP_GET_PACKAGING_RULE_AS_PER_ORDER_ITEMS
	@numOppID = 18597, -- NUMERIC(18)
	@numItemCode = 4626, -- NUMERIC(18,0)
	@numQty = 10, -- NUMERIC(18,0)
	@numPackageTypeID = 2, -- NUMERIC(18,0)
	@numDomainID = 1 -- NUMERIC(18,0)
*/
--- exec USP_GET_PACKAGING_RULE_AS_PER_ORDER_ITEMS @numShipClass=29691,@numPackageTypeID=4,@numQty=1,@numDomainID=1
--- exec USP_GET_PACKAGING_RULE_AS_PER_ORDER_ITEMS @numShipClass=29691,@numPackageTypeID=2,@numQty=15,@numDomainID=1
CREATE PROCEDURE USP_GET_PACKAGING_RULE_AS_PER_ORDER_ITEMS
(
	@numShipClass		NUMERIC(18),
	@numPackageTypeID	NUMERIC(18,0),
	@numQty				FLOAT,
	@numDomainID		NUMERIC(18,0)
)
AS 
BEGIN

--DECLARE @numShipClass AS NUMERIC(18)
--
--SELECT  @numShipClass = numShipClass FROM ITEM I
--INNER JOIN dbo.OpportunityItems OI ON OI.numItemCode = I.numItemCode
--INNER JOIN dbo.OpportunityMaster OM ON OM.numOppId = OI.numOppId AND OM.numDomainId = I.numDomainID
--INNER JOIN dbo.OpportunityBizDocs OBD ON OM.numOppId = OBD.numOppId 
--INNER JOIN dbo.OpportunityBizDocItems OBDI ON OBDI.numOppBizDocID = OBD.numOppBizDocsId
--WHERE I.numItemCode = @numItemCode 
--AND OM.numOppId = @numOppID
--AND I.numDomainID = @numDomainID
--AND ISNULL(OBDI.numUnitHour,OI.numUnitHour) >= @numQty

DECLARE @strSQL AS NVARCHAR(MAX)
DECLARE @strWhere AS NVARCHAR(MAX)
DECLARE @numQtyToCheck AS NUMERIC(18,0)

SELECT @strWhere =  CASE WHEN  numFromQty < @numQty THEN ' ORDER BY numFromQty DESC'  
						 WHEN  numFromQty > @numQty THEN ' ORDER BY numFromQty ASC' 
						 WHEN  numFromQty = @numQty THEN ' ORDER BY numFromQty ASC'  
						 ELSE ' AND 1=1 ' 
					END
				FROM PackagingRules PR    
				LEFT JOIN dbo.CustomPackages CP ON CP.numCustomPackageID = PR.numPackagingTypeID 
				WHERE 1=1 
				AND PR.numDomainID = @numDomainID
				AND PR.numShipClassId = @numShipClass
				AND CP.numCustomPackageID = @numPackageTypeID 
PRINT @strWhere 				

SET @strSQL = '
				SELECT DISTINCT numPackagingRuleID,vcRuleName,numPackagingTypeID,numFromQty,numShipClassID,numDomainID,
								CP.numCustomPackageID ,CP.fltHeight,CP.fltLength,CP.fltTotalWeight,CP.fltWidth,CP.numPackageTypeID,CP.vcPackageName 
				FROM PackagingRules PR    
				LEFT JOIN dbo.CustomPackages CP ON CP.numCustomPackageID = PR.numPackagingTypeID 
				WHERE 1=1 
				AND PR.numDomainID = ' +  CONVERT(VARCHAR(10),@numDomainID) + 
				' AND PR.numShipClassId = ' + CONVERT(VARCHAR(10),@numShipClass) + 
				' AND CP.numCustomPackageID = ' + CONVERT(VARCHAR(10),@numPackageTypeID) 
				--+ ' AND ( numFromQty > ' + CONVERT(VARCHAR(10),@numQty) + ' OR numFromQty = ' + CONVERT(VARCHAR(10),@numQty) + ')'

SET @strSQL = @strSQL + ' ' + ISNULL(@strWhere,' AND 1=1 ') 
PRINT @strSQL
EXEC SP_EXECUTESQL @strSQL 

--(CASE WHEN numFromQty <= @numQty) THEN  
--ORDER BY numFromQty DESC

END