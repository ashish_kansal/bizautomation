SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetReminders')
DROP PROCEDURE USP_GetReminders
GO
CREATE PROCEDURE [dbo].[USP_GetReminders]  
 @StartDateTimeUtc datetime, -- the start date before which no activities are retrieved  
 @EndDateTimeUtc datetime, -- the end date, any activities starting after this date are excluded  
 @OrganizerName nvarchar(64) -- resource name (will be used to lookup ID)  
AS  
BEGIN  
 --  
 -- First step is to get the ResourceID for the primary resource name supplied.  
 --  
	DECLARE @numDomaindID NUMERIC(18,0) = 0
	DECLARE @numUserCntID NUMERIC(18,0) = 0
	DECLARE @ResourceID AS integer;

	

	if isnumeric(@OrganizerName) =1
	begin
		set @ResourceID=convert(numeric(9), @OrganizerName)
		SELECT @numDomaindID=numDomainId,@numUserCntID=numUserCntId FROM Resource WHERE ISNULL(ResourceID,0) = @OrganizerName
	end
	else
	begin 
		set @ResourceID =-999	
	end 

	DECLARE @TEMP TABLE
	(
		ActivityID INT,
		numType INT, --1-Calendar 2-Action Item
		RecurrenceID NUMERIC(18,0),
		Subject VARCHAR(500),
		Location VARCHAR(500),
		EnableReminder BIT,
		AllDayEvent BIT,
		StartDateTimeUtc DATETIME, 
		VarianceID UNIQUEIDENTIFIER,
		LastSnoozDateTimeUtc DATETIME,
		ReminderInterval INT,
		Organization VARCHAR(500),
		ReminderDate VARCHAR(500)
	)


	--FETCH CALENDAR REMINDER
	INSERT INTO
		@TEMP
	SELECT   
		[Activity].[ActivityID],
		1,
		[Activity].[RecurrenceID], 
		[Activity].[Subject], 
		[Activity].[Location],
		[Activity].[EnableReminder],
	    [Activity].[AllDayEvent],   
		[Activity].[StartDateTimeUtc],   
		[Activity].[VarianceID],
		ISNULL(LastSnoozDateTimeUtc,'1900-01-01 00:00:00') AS LastSnoozDateTimeUtc,
		[Activity].ReminderInterval,
		'',
		CASE WHEN convert(varchar(11),[Activity].[StartDateTimeUtc])=convert(varchar(11),DateAdd(minute, 330,getdate())) then '<b><font color=red>Today</font></b>'
WHEN convert(varchar(11),[Activity].[StartDateTimeUtc])=convert(varchar(11),DateAdd(day,-1,DateAdd(minute, 330,getdate()))) then '<b><font color=purple>Yesterday</font></b>'
WHEN convert(varchar(11),[Activity].[StartDateTimeUtc])=convert(varchar(11),DateAdd(day,1,DateAdd(minute, 330,getdate()))) then '<b><font color=orange>Tommorow</font></b>'
ELSE  dbo.FormatedDateFromDate([Activity].[StartDateTimeUtc],@numDomaindID) END AS ReminderDate
	FROM   
		[Activity] 
	INNER JOIN 
		[ActivityResource] 
	ON 
		[Activity].[ActivityID] = [ActivityResource].[ActivityID]  
	WHERE   
		ISNULL([Activity].[EnableReminder],0) = 1
		AND (([Activity].[ActivityID] = [ActivityResource].[ActivityID]) AND ([ActivityResource].[ResourceID] = @ResourceID)) 
		AND ((([Activity].[StartDateTimeUtc] >= @StartDateTimeUtc AND [Activity].[StartDateTimeUtc] < @EndDateTimeUtc) OR ([Activity].[RecurrenceID] <> -999 AND CAST([Activity].[StartDateTimeUtc] AS TIME) >= CAST(@StartDateTimeUtc AS TIME))) AND ([Activity].[OriginalStartDateTimeUtc] IS NULL));  
		

	--FETCH ACTION ITEMS
	INSERT INTO
		@TEMP
	SELECT   
		Communication.numCommId,
		2,
		0,
		'',
		ISNULL(ListDetails.vcData,''),
		1,
		bitFollowUpAnyTime,
		Communication.dtStartTime,
		NULL,
		ISNULL(LastSnoozDateTimeUtc,'1900-01-01 00:00:00') AS LastSnoozDateTimeUtc,
		ISNULL(Communication.intRemainderMins,0) * 60 AS ReminderInterval,
		CompanyInfo.vcCompanyName,
		CASE WHEN convert(varchar(11),Communication.dtStartTime)=convert(varchar(11),DateAdd(minute, 330,getdate())) then '<b><font color=red>Today</font></b>'
WHEN convert(varchar(11),Communication.dtStartTime)=convert(varchar(11),DateAdd(day,-1,DateAdd(minute, 330,getdate()))) then '<b><font color=purple>Yesterday</font></b>'
WHEN convert(varchar(11),Communication.dtStartTime)=convert(varchar(11),DateAdd(day,1,DateAdd(minute, 330,getdate()))) then '<b><font color=orange>Tommorow</font></b>'
ELSE  dbo.FormatedDateFromDate(Communication.dtStartTime,@numDomaindID) END AS ReminderDate
	FROM   
		Communication
	INNER JOIN
		DivisionMaster
	ON
		Communication.numDivisionId = DivisionMaster.numDivisionID
	INNER JOIN
		CompanyInfo
	ON
		DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
	LEFT JOIN
		ListDetails
	ON
		ListDetails.numListItemID = Communication.bitTask
	WHERE	
		Communication.numDomainID = @numDomaindID 
		AND numAssign =@numUserCntID 
		AND ISNULL(tintRemStatus,0) > 0 
		AND bitclosedflag=0   
		AND ISNULL(intRemainderMins,0) <> 0          
		AND CAST(Communication.dtStartTime AS DATE) = CAST(GETUTCDATE() AS DATE)
		AND Communication.dtStartTime > @StartDateTimeUtc

	SELECT * FROM @TEMP ORDER BY StartDateTimeUtc ASC
END
GO
