GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON


GO

IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetAttributesForSelectedItemGroup')
DROP PROCEDURE USP_GetAttributesForSelectedItemGroup
GO

CREATE PROCEDURE [dbo].[USP_GetAttributesForSelectedItemGroup]
	@numDomainID AS NUMERIC(18,0),
	@numListID AS NUMERIC(9)=0,  
	@numItemGroupID AS NUMERIC(9),     
	@numWareHouseID AS NUMERIC(9)=0    
AS
BEGIN
	SELECT numListItemID,vcData 
				FROM 
					ListDetails 
				WHERE 
					numlistitemid IN (
										SELECT 
											DISTINCT(fld_value) 
										FROM 
											Item I
										JOIN
											WareHouseItems W      
										ON
											I.numItemCode=W.numItemID
										JOIN 
											ItemAttributes
										ON 
											I.numItemCode=ItemAttributes.numItemCode      
										JOIN 
											CFW_Fld_Master M 
										ON 
											ItemAttributes.Fld_ID=M.Fld_ID                             
										WHERE 
											I.numDomainID = @numDomainID
											AND W.numDomainID = @numDomainID
											AND M.numListID=@numListID 
											--AND bitMatrix = 0
											AND numItemGroup = @numItemGroupID
											AND (W.numWareHouseID=@numWareHouseID OR ISNULL(@numWareHouseID,0)=0)
									)      
END
