GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_WarehouseItems_UpdateReorder' ) 
    DROP PROCEDURE USP_WarehouseItems_UpdateReorder
GO
CREATE PROCEDURE [dbo].[USP_WarehouseItems_UpdateReorder]  
	@numDomainID AS NUMERIC(18,0),
	@numUserCntID AS NUMERIC(18,0),
	@numItemCode NUMERIC(18,0),
	@numWarehouseItemID NUMERIC(18,0),
	@numReorder AS FLOAT
AS
BEGIN
	IF ISNULL((SELECT numReorder FROM WareHouseItems WHERE numWareHouseItemID=@numWarehouseItemID),0) <> @numReorder
	BEGIN
		UPDATE 
			WareHouseItems  
		SET 
			numReorder=@numReorder,
			dtModified=GETDATE()   
		WHERE 
			numWareHouseItemID=@numWarehouseItemID

		UPDATE Item SET bintModifiedDate=GETUTCDATE(),numModifiedBy=@numUserCntID WHERE numItemCode=@numItemCode AND numDomainID=@numDomainID
	END
END
GO
