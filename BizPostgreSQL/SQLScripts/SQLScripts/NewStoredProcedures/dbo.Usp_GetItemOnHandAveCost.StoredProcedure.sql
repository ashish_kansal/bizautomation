GO
/****** Object:  StoredProcedure [dbo].[Usp_GetItemOnHandAveCost]    Script Date: 05/25/2013 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO                                            
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='Usp_GetItemOnHandAveCost')
DROP PROCEDURE Usp_GetItemOnHandAveCost
GO
CREATE PROCEDURE [dbo].[Usp_GetItemOnHandAveCost]   
(
	@strItemIDs		VARCHAR(MAX),
	@numDomainID	NUMERIC(18)
)
AS

BEGIN
	IF ISNULL(@strItemIDs,'') <> ''
	BEGIN
	
		CREATE TABLE #temp(ID INT PRIMARY KEY)
		INSERT INTO #temp SELECT DISTINCT ID FROM dbo.SplitIDs(@strItemIDs,',')
		
		SELECT numItemCode, numWareHouseItemID ,(CASE WHEN ISNULL(I.bitVirtualInventory,0) = 1 THEN 0 ELSE monAverageCost END) ,numOnHand
		FROM dbo.Item I
		JOIN dbo.WareHouseItems WI ON I.numItemCode = WI.numItemID AND I.numDomainID = WI.numDomainID
		JOIN #temp SP ON SP.Id=I.numItemCode
		WHERE I.numDomainID = @numDomainID
		ORDER BY numItemCode	
		
		DROP TABLE #temp
	END
	
END