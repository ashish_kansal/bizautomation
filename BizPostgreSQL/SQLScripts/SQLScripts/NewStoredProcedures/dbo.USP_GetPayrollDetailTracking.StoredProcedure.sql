
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetPayrollDetailTracking')
DROP PROCEDURE USP_GetPayrollDetailTracking
GO
CREATE PROCEDURE [dbo].[USP_GetPayrollDetailTracking]
(
    @numDomainId AS NUMERIC(9),
    @numUserCntID AS NUMERIC(9),
    @numPayrollHeaderID AS NUMERIC(9),
    @numPayrollDetailID AS NUMERIC(9),
    @tintMode AS TINYINT,
    @ClientTimeZoneOffset INT
)
AS
  BEGIN
  	DECLARE @dtStartDate AS DATETIME
DECLARE @dtEndDate AS DATETIME
 
SELECT @dtStartDate=dtFromDate,@dtEndDate=dtToDate FROM dbo.PayrollHeader WHERE numPayrollHeaderID=@numPayrollHeaderID AND numDomainId=@numDomainId

IF @tintMode=1 --RegularHrs
BEGIN
  Select TE.numCategoryHDRID,ISNULL(numPayrollDetailID,0) AS numPayrollDetailID
  ,CASE WHEN ISNULL(numPayrollDetailID,0)>0 THEN 1 ELSE 0 END AS bitPaid
  ,dateadd(minute,-@ClientTimeZoneOffset,dtFromDate) AS dtFromDate,
  dateadd(minute,-@ClientTimeZoneOffset,dtToDate) AS dtToDate,
  isnull((Cast(datediff(minute,dtfromdate,dttodate) as float)/Cast(60 as float)),0) as Hrs,0 AS monamount            
  from TimeAndExpense TE 
  LEFT JOIN PayrollTracking PT ON  ISNULL(TE.numCategoryHDRID,0)=ISNULL(PT.numCategoryHDRID,0)
  AND numPayrollHeaderID=@numPayrollHeaderID AND numPayrollDetailID=@numPayrollDetailID  
   Where (numType=1 Or numType=2)  And numCategory=1                 
--  And ((dateadd(minute,-@ClientTimeZoneOffset,dtFromDate) between @dtStartDate And 	@dtEndDate )
--	Or (dateadd(minute,-@ClientTimeZoneOffset,dtToDate) between @dtStartDate And @dtEndDate)) 
	And             
  TE.numUserCntID=@numUserCntID  And TE.numDomainID=@numDomainId 
  AND TE.numCategoryHDRID NOT IN (SELECT numCategoryHDRID FROM PayrollTracking WHERE numDomainId=@numDomainId AND numPayrollHeaderID!=@numPayrollHeaderID AND numCategoryHDRID IS NOT NULL)

END
--ELSE IF @tintMode=2 --OvertimeHrs
--BEGIN
--	PRINT @tintMode
--END
ELSE IF @tintMode=3 --PaidLeaveHrs
BEGIN
	 Select TE.numCategoryHDRID,ISNULL(numPayrollDetailID,0) AS numPayrollDetailID
  ,CASE WHEN ISNULL(numPayrollDetailID,0)>0 THEN 1 ELSE 0 END AS bitPaid
  ,dateadd(minute,-@ClientTimeZoneOffset,dtFromDate) AS dtFromDate,
  dateadd(minute,-@ClientTimeZoneOffset,dtToDate) AS dtToDate,
  ISNULL(CASE WHEN dtfromdate = dttodate THEN 24 - (CASE WHEN bitfromfullday = 0 THEN 12 ELSE 0 END)
           ELSE (Datediff(DAY,dtfromdate,dttodate) + 1) * 24 
                                - (CASE WHEN bitfromfullday = 0 THEN 12 ELSE 0 END)
                                - (CASE WHEN bittofullday = 0 THEN 12 ELSE 0 END) END,0) as Hrs,0 AS monamount           
  from TimeAndExpense TE 
  LEFT JOIN PayrollTracking PT ON  ISNULL(TE.numCategoryHDRID,0)=ISNULL(PT.numCategoryHDRID,0)
  AND numPayrollHeaderID=@numPayrollHeaderID AND numPayrollDetailID=@numPayrollDetailID  
   Where numType=3 And numCategory=3               
--  And ((dateadd(minute,-@ClientTimeZoneOffset,dtFromDate) between @dtStartDate And 	@dtEndDate )
--	Or (dateadd(minute,-@ClientTimeZoneOffset,dtToDate) between @dtStartDate And @dtEndDate)) 
	And             
  TE.numUserCntID=@numUserCntID  And TE.numDomainID=@numDomainId
  AND TE.numCategoryHDRID NOT IN (SELECT numCategoryHDRID FROM PayrollTracking WHERE numDomainId=@numDomainId AND numPayrollHeaderID!=@numPayrollHeaderID AND numCategoryHDRID IS NOT NULL)
END
ELSE IF @tintMode=4 --PaidInvoice
BEGIN
	SELECT * FROM (
	SELECT BC.numComissionID,Opp.vcPOppName,BC.numComissionAmount,
	ISNULL((SELECT SUM(ISNULL(monCommissionAmt,0))
	FROM PayrollTracking WHERE ISNULL(BC.numComissionID,0)=ISNULL(numComissionID,0) AND numPayrollHeaderID!=@numPayrollHeaderID),0) monPaidAmount
	,isnull(PT1.monCommissionAmt,0) AS monUsedAmount ,CASE WHEN ISNULL(PT1.numPayrollDetailID,0)>0 THEN 1 ELSE 0 END AS bitPaid
   FROM OpportunityMaster Opp 
		INNER JOIN OpportunityBizDocs oppBiz on oppBiz.numOppId=opp.numOppId 
        INNER JOIN BizDocComission BC on BC.numOppBizDocId=oppBiz.numOppBizDocsId 
        LEFT JOIN PayrollTracking PT1 ON  ISNULL(BC.numComissionID,0)=ISNULL(PT1.numComissionID,0)
			AND PT1.numPayrollHeaderID=@numPayrollHeaderID AND PT1.numPayrollDetailID=@numPayrollDetailID	 
   where Opp.numDomainId=@numDomainId and
   BC.numUserCntId=@numUserCntID AND BC.bitCommisionPaid=0 
   AND oppBiz.bitAuthoritativeBizDocs = 1 AND isnull(oppBiz.monAmountPaid,0) >=oppBiz.monDealAmount
  -- AND ((dateadd(minute,-@ClientTimeZoneOffset,oppBiz.dtCreatedDate) between @dtStartDate And @dtEndDate))
   ) T
   WHERE (numComissionAmount - monPaidAmount) > 0
   
END
ELSE IF @tintMode=5 --UNPaidInvoice
BEGIN
SELECT * FROM (
SELECT BC.numComissionID,Opp.vcPOppName,BC.numComissionAmount,
	ISNULL((SELECT SUM(ISNULL(monCommissionAmt,0))
	FROM PayrollTracking WHERE ISNULL(BC.numComissionID,0)=ISNULL(numComissionID,0) AND numPayrollHeaderID!=@numPayrollHeaderID),0) monPaidAmount
	,isnull(PT1.monCommissionAmt,0) AS monUsedAmount ,CASE WHEN ISNULL(PT1.numPayrollDetailID,0)>0 THEN 1 ELSE 0 END AS bitPaid
   FROM OpportunityMaster Opp 
		INNER JOIN OpportunityBizDocs oppBiz on oppBiz.numOppId=opp.numOppId 
        INNER JOIN BizDocComission BC on BC.numOppBizDocId=oppBiz.numOppBizDocsId 
        LEFT JOIN PayrollTracking PT1 ON  ISNULL(BC.numComissionID,0)=ISNULL(PT1.numComissionID,0)
			AND PT1.numPayrollHeaderID=@numPayrollHeaderID AND PT1.numPayrollDetailID=@numPayrollDetailID	 
   where Opp.numDomainId=@numDomainId and
   BC.numUserCntId=@numUserCntID AND BC.bitCommisionPaid=0 
   AND oppBiz.bitAuthoritativeBizDocs = 1 AND isnull(oppBiz.monAmountPaid,0) <oppBiz.monDealAmount
   --AND ((dateadd(minute,-@ClientTimeZoneOffset,oppBiz.dtCreatedDate) between @dtStartDate And @dtEndDate))
   ) T
   WHERE (numComissionAmount - monPaidAmount) > 0
   
END
ELSE IF @tintMode=6 --Expense
BEGIN
	SELECT TE.numCategoryHDRID,ISNULL(numPayrollDetailID,0) AS numPayrollDetailID
  ,CASE WHEN ISNULL(numPayrollDetailID,0)>0 THEN 1 ELSE 0 END AS bitPaid
  ,dateadd(minute,-@ClientTimeZoneOffset,dtFromDate) AS dtFromDate,
  dateadd(minute,-@ClientTimeZoneOffset,dtToDate) AS dtToDate,
  Isnull(monamount,0) AS monamount,0 AS Hrs   
  from TimeAndExpense TE 
  LEFT JOIN PayrollTracking PT ON  ISNULL(TE.numCategoryHDRID,0)=ISNULL(PT.numCategoryHDRID,0)
  AND numPayrollHeaderID=@numPayrollHeaderID AND numPayrollDetailID=@numPayrollDetailID  
   Where numCategory=2 And numType in (1,2)               
--  And ((dateadd(minute,-@ClientTimeZoneOffset,dtFromDate) between @dtStartDate And 	@dtEndDate )
--	Or (dateadd(minute,-@ClientTimeZoneOffset,dtToDate) between @dtStartDate And @dtEndDate)) 
	And             
  TE.numUserCntID=@numUserCntID  And TE.numDomainID=@numDomainId
  AND TE.numCategoryHDRID NOT IN (SELECT numCategoryHDRID FROM PayrollTracking WHERE numDomainId=@numDomainId AND numPayrollHeaderID!=@numPayrollHeaderID AND numCategoryHDRID IS NOT NULL)

END
ELSE IF @tintMode=7 --Reimburse
BEGIN
	SELECT TE.numCategoryHDRID,ISNULL(numPayrollDetailID,0) AS numPayrollDetailID
  ,CASE WHEN ISNULL(numPayrollDetailID,0)>0 THEN 1 ELSE 0 END AS bitPaid
  ,dateadd(minute,-@ClientTimeZoneOffset,dtFromDate) AS dtFromDate,
  dateadd(minute,-@ClientTimeZoneOffset,dtToDate) AS dtToDate,
  Isnull(monamount,0) AS monamount,0 AS Hrs
	 from TimeAndExpense TE 
  LEFT JOIN PayrollTracking PT ON  ISNULL(TE.numCategoryHDRID,0)=ISNULL(PT.numCategoryHDRID,0)
  AND numPayrollHeaderID=@numPayrollHeaderID AND numPayrollDetailID=@numPayrollDetailID  
   Where bitreimburse = 1 AND numcategory = 2              
--  And ((dateadd(minute,-@ClientTimeZoneOffset,dtFromDate) between @dtStartDate And 	@dtEndDate )
--	Or (dateadd(minute,-@ClientTimeZoneOffset,dtToDate) between @dtStartDate And @dtEndDate)) 
	And             
  TE.numUserCntID=@numUserCntID  And TE.numDomainID=@numDomainId
  AND TE.numCategoryHDRID NOT IN (SELECT numCategoryHDRID FROM PayrollTracking WHERE numDomainId=@numDomainId AND numPayrollHeaderID!=@numPayrollHeaderID AND numCategoryHDRID IS NOT NULL)

END
ELSE IF @tintMode=8 --Project Commission
BEGIN
	SELECT * FROM (
	SELECT BC.numComissionID,PM.vcProjectID,PM.monTotalIncome,PM.monTotalExpense,PM.monTotalGrossProfit,
	BC.monProTotalIncome,BC.monProTotalExpense,BC.monProTotalGrossProfit,BC.numComissionAmount,
	ISNULL((SELECT SUM(ISNULL(monCommissionAmt,0))
	FROM PayrollTracking WHERE ISNULL(BC.numComissionID,0)=ISNULL(numComissionID,0) AND numPayrollHeaderID!=@numPayrollHeaderID),0) monPaidAmount
	,isnull(PT1.monCommissionAmt,0) AS monUsedAmount ,CASE WHEN ISNULL(PT1.numPayrollDetailID,0)>0 THEN 1 ELSE 0 END AS bitPaid,
	dbo.FormatedDateFromDate(DateAdd(minute,-@ClientTimeZoneOffset, BC.dtProCompletionDate),PM.numDomainID) AS dtCompletionDate
   FROM ProjectsMaster PM
        INNER JOIN BizDocComission BC on BC.numProId=PM.numProId 
        LEFT JOIN PayrollTracking PT1 ON  ISNULL(BC.numComissionID,0)=ISNULL(PT1.numComissionID,0)
			AND PT1.numPayrollHeaderID=@numPayrollHeaderID AND PT1.numPayrollDetailID=@numPayrollDetailID	 
   where PM.numDomainId=@numDomainId AND --PM.numProjectStatus=27492 and
   BC.numUserCntId=@numUserCntID AND BC.bitCommisionPaid=0 
   ) T
   WHERE (numComissionAmount - monPaidAmount) > 0
   
END

END
GO