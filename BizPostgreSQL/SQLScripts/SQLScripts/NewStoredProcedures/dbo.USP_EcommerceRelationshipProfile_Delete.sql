GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_EcommerceRelationshipProfile_Delete')
DROP PROCEDURE dbo.USP_EcommerceRelationshipProfile_Delete
GO
CREATE PROCEDURE [dbo].[USP_EcommerceRelationshipProfile_Delete]
(
	@numSiteID NUMERIC(18,0)
	,@vcIds VARCHAR(1000)
)
AS 
BEGIN
	DELETE FROM EcommerceRelationshipProfile WHERE numSiteID=@numSiteID AND ID IN (SELECT Id FROM dbo.SplitIDs(@vcIds,','))
END