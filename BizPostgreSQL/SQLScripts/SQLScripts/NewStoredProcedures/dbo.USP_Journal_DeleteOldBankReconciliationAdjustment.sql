SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created By Chintan
-- EXEC USP_Journal_EntryListForBankReconciliation 2233,'','',0,1
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_Journal_DeleteOldBankReconciliationAdjustment')
DROP PROCEDURE USP_Journal_DeleteOldBankReconciliationAdjustment
GO
CREATE PROCEDURE [dbo].[USP_Journal_DeleteOldBankReconciliationAdjustment]
	@numDomainID NUMERIC(18,0)
	,@numReconcileID NUMERIC(18,0)
AS
BEGIN
	BEGIN TRY
	BEGIN TRANSACTION
		IF ISNULL(@numReconcileID,0) > 0
		BEGIN
			DELETE FROM General_Journal_Details WHERE numDomainId=@numDomainID AND numJournalId IN (SELECT ISNULL(numJournal_Id,0) FROM General_Journal_Header WHERE numReconcileID=@numReconcileID AND ISNULL(bitReconcileInterest,0) = 0)
			DELETE FROM General_Journal_Header WHERE numDomainId=@numDomainID AND numReconcileID=@numReconcileID AND ISNULL(bitReconcileInterest,0) = 0
		END
	COMMIT
	END TRY
	BEGIN CATCH
		DECLARE @ErrorMessage NVARCHAR(4000)
		DECLARE @ErrorNumber INT
		DECLARE @ErrorSeverity INT
		DECLARE @ErrorState INT
		DECLARE @ErrorLine INT
		DECLARE @ErrorProcedure NVARCHAR(200)

		IF @@TRANCOUNT > 0
			ROLLBACK TRANSACTION;

		SELECT 
			@ErrorMessage = ERROR_MESSAGE(),
			@ErrorNumber = ERROR_NUMBER(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE(),
			@ErrorLine = ERROR_LINE(),
			@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

		RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);
	END CATCH
END
GO