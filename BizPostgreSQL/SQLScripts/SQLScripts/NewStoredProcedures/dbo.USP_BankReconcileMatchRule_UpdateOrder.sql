SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_BankReconcileMatchRule_UpdateOrder')
DROP PROCEDURE dbo.USP_BankReconcileMatchRule_UpdateOrder
GO
CREATE PROCEDURE [dbo].[USP_BankReconcileMatchRule_UpdateOrder]
(
	@numDomainID NUMERIC(18,0),
	@vcRules Text
)
AS 
BEGIN
BEGIN TRY
BEGIN TRANSACTION
	DECLARE @hDocItem INT
	EXEC sp_xml_preparedocument @hDocItem OUTPUT, @vcRules

	DECLARE @TEMP TABLE
	(
		ID INT IDENTITY(1,1)
		,numRuleID NUMERIC(18,0)
		,tintOrder TINYINT
	)

	INSERT INTO @TEMP
	(
		numRuleID
		,tintOrder
	)
	SELECT 
		numRuleID
		,tintOrder
	FROM
		OPENXML (@hDocItem,'/NewDataSet/Rule',2)
	WITH
	(
		numRuleID NUMERIC(18,0)
		,tintOrder TINYINT
	)

	DECLARE @i AS INT = 1
	DECLARE @COUNT AS INT
	DECLARE @numRuleID NUMERIC(18,0)
	DECLARE @tintOrder TINYINT
	SELECT @COUNT = COUNT(*) FROM @TEMP

	WHILE @i <= @COUNT
	BEGIN
		SELECT @numRuleID=numRuleID,@tintOrder=tintOrder FROM @TEMP WHERE ID = @i

		IF NOT EXISTS (SELECT * FROM @TEMP WHERE numRuleID <> @numRuleID AND tintOrder = @tintOrder)
		BEGIN
			UPDATE BankReconcileMatchRule SET tintOrder = @tintOrder WHERE numDomainID=@numDomainID AND ID=@numRuleID
		END
		ELSE 
		BEGIN
			RAISERROR('DUPLICATE_ORDER_NUMBER',16,1)
		END

		SET @i = @i + 1
	END
COMMIT
END TRY
BEGIN CATCH
	DECLARE @ErrorMessage NVARCHAR(4000)
	DECLARE @ErrorNumber INT
	DECLARE @ErrorSeverity INT
	DECLARE @ErrorState INT
	DECLARE @ErrorLine INT
	DECLARE @ErrorProcedure NVARCHAR(200)

	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;

	SELECT 
		@ErrorMessage = ERROR_MESSAGE(),
		@ErrorNumber = ERROR_NUMBER(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE(),
		@ErrorLine = ERROR_LINE(),
		@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

	RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);

END CATCH
END