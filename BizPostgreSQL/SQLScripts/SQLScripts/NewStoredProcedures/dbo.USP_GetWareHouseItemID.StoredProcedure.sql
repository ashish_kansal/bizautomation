SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
  
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetWareHouseItemID')
DROP PROCEDURE USP_GetWareHouseItemID
GO
CREATE PROCEDURE [dbo].[USP_GetWareHouseItemID]  
@numItemKitID as numeric(9),  
@vcText as varchar(100)='',
@numDomainID as numeric(9)  
as  
  
  DECLARE @numWareHouseItemID AS NUMERIC(9)

  select top 1 @numWareHouseItemID=WI.numWareHouseItemID from warehouses W JOIN  WareHouseItems WI ON W.numWareHouseID=WI.numWareHouseID 
  where W.numDomainID=@numDomainID AND WI.numDomainID=@numDomainID AND WI.numItemID=@numItemKitID and W.vcWareHouse=@vcText

IF @numWareHouseItemID IS NULL
BEGIN
	SELECT TOP 1 @numWareHouseItemID=WI.numWareHouseItemID from WareHouseItems WI 
           where WI.numDomainID=@numDomainID AND WI.numItemID=@numItemKitID 
END

SELECT ISNULL(@numWareHouseItemID,0)