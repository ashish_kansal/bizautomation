SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_TicklerListFilterConfiguration_Get')
DROP PROCEDURE dbo.USP_TicklerListFilterConfiguration_Get
GO
CREATE PROCEDURE [dbo].[USP_TicklerListFilterConfiguration_Get]
    (
	  @numDomainID NUMERIC(18,0),
      @numUserCntID NUMERIC(18,0)
    )
AS 
BEGIN
   
	SELECT
		*
	FROM
		TicklerListFilterConfiguration
	WHERE
		numDomainID = @numDomainID AND
		numUserCntID = @numUserCntID
END
