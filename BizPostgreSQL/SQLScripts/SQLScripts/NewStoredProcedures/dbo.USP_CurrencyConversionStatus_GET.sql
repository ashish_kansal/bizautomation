SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_CurrencyConversionStatus_GET')
DROP PROCEDURE USP_CurrencyConversionStatus_GET
GO
CREATE PROCEDURE [dbo].[USP_CurrencyConversionStatus_GET]  
	
AS  
BEGIN 
	SELECT
		Id,
		dtLastExecuted,
		bitSucceed,
		intNoOfTimesTried,
		bitFailureNotificationSent
	FROM
		CurrencyConversionStatus
END