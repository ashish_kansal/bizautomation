GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_PromotionOffer_ClearCouponCodeItemPromotion')
DROP PROCEDURE USP_PromotionOffer_ClearCouponCodeItemPromotion
GO
CREATE PROCEDURE [dbo].[USP_PromotionOffer_ClearCouponCodeItemPromotion]
	@numDomainID NUMERIC(18,0),
	@numDivisionID NUMERIC(18,0),
	@numPromotionID NUMERIC(18,0),
	@vcItems VARCHAR(MAX),
	@numCurrencyID NUMERIC(18,0)
AS
BEGIN
	DECLARE @hDocItem INT

	DECLARE @TEMP TABLE
	(
		ID INT IDENTITY(1,1)
		,numOppItemID NUMERIC(18,0)
        ,numItemCode NUMERIC(18,0)
        ,numUnits FLOAT
		,numWarehouseItemID NUMERIC(18,0)
        ,monUnitPrice DECIMAL(20,5)
        ,fltDiscount DECIMAL(20,5)
        ,bitDiscountType BIT
        ,monTotalAmount DECIMAL(20,5)
        ,numPromotionID NUMERIC(18,0)
        ,bitPromotionTriggered BIT
		,bitPromotionDiscount BIT
        ,vcPromotionDescription VARCHAR(MAX)
		,vcKitChildItems VARCHAR(MAX)
		,vcInclusionDetail VARCHAR(MAX)
		,bitChanged BIT		
	)

	IF ISNULL(@vcItems,'') <> ''
	BEGIN
		EXEC sp_xml_preparedocument @hDocItem OUTPUT, @vcItems

		INSERT INTO @TEMP
		(
			numOppItemID
			,numItemCode
			,numUnits
			,numWarehouseItemID
			,monUnitPrice
			,fltDiscount
			,bitDiscountType
			,monTotalAmount
			,numPromotionID
			,bitPromotionTriggered
			,bitPromotionDiscount
			,vcPromotionDescription
			,vcKitChildItems
			,vcInclusionDetail
			,bitChanged
		)

		SELECT 
			numOppItemID
			,numItemCode
			,numUnits
			,numWarehouseItemID
			,monUnitPrice
			,fltDiscount
			,bitDiscountType
			,monTotalAmount
			,numPromotionID
			,bitPromotionTriggered
			,bitPromotionDiscount
			,vcPromotionDescription
			,vcKitChildItems
			,vcInclusionDetail
			,0
		FROM 
			OPENXML (@hDocItem,'/NewDataSet/Item',2)                                                                          
		WITH                       
		(                                                                          
			numOppItemID NUMERIC(18,0)
			,numItemCode NUMERIC(18,0)
			,numUnits FLOAT
			,numWarehouseItemID NUMERIC(18,0)
			,monUnitPrice DECIMAL(20,5)
			,fltDiscount DECIMAL(20,5)
			,bitDiscountType BIT
			,monTotalAmount DECIMAL(20,5)
			,numPromotionID NUMERIC(18,0)
			,bitPromotionTriggered BIT
			,bitPromotionDiscount BIT
			,vcKitChildItems VARCHAR(MAX)
			,vcInclusionDetail VARCHAR(MAX)
			,vcPromotionDescription VARCHAR(MAX)
		)

		EXEC sp_xml_removedocument @hDocItem

		UPDATE 
			T1
		SET 
			numPromotionID=0
			,bitPromotionTriggered=0
			,vcPromotionDescription=''
			,bitChanged=1
			,monUnitPrice=ISNULL(T2.monPrice,0)
			,fltDiscount=ISNULL(T2.decDiscount,0)
			,bitDiscountType=ISNULL(T2.tintDisountType,0)
			,bitPromotionDiscount=0
		FROM
			@TEMP T1
		CROSS APPLY
			dbo.fn_GetItemPriceBasedOnMethod(@numDomainID,@numDivisionID,T1.numItemCode,T1.numUnits,0,0,T1.numWarehouseItemID,T1.vcKitChildItems,@numCurrencyID) T2
		WHERE 
			ISNULL(numPromotionID,0)=@numPromotionID
	END

	SELECT * FROM @TEMP WHERE bitChanged=1
END