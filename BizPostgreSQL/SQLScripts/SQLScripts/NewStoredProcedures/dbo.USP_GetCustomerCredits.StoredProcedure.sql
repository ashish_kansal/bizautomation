/****** Object:  StoredProcedure [USP_GetCustomerCredits]    ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetCustomerCredits')
DROP PROCEDURE USP_GetCustomerCredits
GO
CREATE PROCEDURE [USP_GetCustomerCredits]                              
(
    @numDomainId numeric(18, 0),
    @numDivisionId numeric(18, 0),
    @numReferenceId NUMERIC(18,0),
    @tintMode TINYINT,
    @numCurrencyID NUMERIC(18,0)=0,
    @numAccountClass NUMERIC(18,0)=0
)            
AS                                           
BEGIN 

    DECLARE @numBaseCurrencyID NUMERIC
	    DECLARE @BaseCurrencySymbol nVARCHAR(3);SET @BaseCurrencySymbol='$'
	    
        SELECT @numBaseCurrencyID = ISNULL(numCurrencyID,0) FROM dbo.Domain WHERE numDomainId=@numDomainId
        SELECT @BaseCurrencySymbol = ISNULL(varCurrSymbol,'$') FROM dbo.Currency WHERE numCurrencyID = @numBaseCurrencyID
    
    
IF @tintMode=1
BEGIN
	
	DECLARE @tintDepositePage AS TINYINT;SET @tintDepositePage=0
	
	IF @numReferenceId>0
	BEGIN
		SELECT @tintDepositePage=tintDepositePage FROM depositMaster WHERE numDepositID=@numReferenceId
	END
	
	PRINT @tintDepositePage
	
	IF @tintDepositePage=3
	BEGIN
		SELECT tintDepositePage AS tintRefType,
		CASE WHEN tintDepositePage=3 THEN 'Credit Memo #' + CAST((SELECT TOP 1 [ReturnHeader].[vcRMA] FROM [dbo].[ReturnHeader] WHERE [ReturnHeader].[numReturnHeaderID] = DM.[numReturnHeaderID]) AS VARCHAR(20)) ELSE 'Unapplied Payment' END AS vcReference,
		numDepositId AS numReferenceID,CAST(monDepositAmount AS DECIMAL(19,2)) AS monAmount,
		ISNULL(CAST(monDepositAmount AS DECIMAL(19,2)),0) - ISNULL(CAST(monRefundAmount AS DECIMAL(19,2)),0) AS monNonAppliedAmount,
		ISNULL(numReturnHeaderID,0) AS numReturnHeaderID,CAST (1 AS BIT) AS bitIsPaid,ISNULL(CAST(monAppliedAmount AS DECIMAL(19,2)),0) AS monAmountPaid,
		DM.numCurrencyID,ISNULL(C.varCurrSymbol,'') varCurrSymbol,ISNULL(NULLIF(DM.fltExchangeRate,0),1) fltExchangeRateReceivedPayment
		,@BaseCurrencySymbol BaseCurrencySymbol,CASE WHEN tintDepositePage=3 THEN '' ELSE ISNULL(DM.vcReference,'') END  AS vcDepositReference 
		FROM DepositMaster DM LEFT JOIN dbo.Currency C ON C.numCurrencyID = DM.numCurrencyID
		WHERE DM.numDomainId=@numDomainId AND numDivisionID=@numDivisionID AND tintDepositePage IN(3)
		AND numDepositId=@numReferenceId AND (ISNULL(DM.numCurrencyID,@numBaseCurrencyID) = @numCurrencyID OR @numCurrencyID =0)
		AND (ISNULL(DM.numAccountClass,0)=@numAccountClass OR @numAccountClass=0)
		 
		UNION
		
		SELECT tintDepositePage AS tintRefType,CASE WHEN tintDepositePage=3 THEN 'Credit Memo #' + CAST((SELECT TOP 1 [ReturnHeader].[vcRMA] FROM [dbo].[ReturnHeader] WHERE [ReturnHeader].[numReturnHeaderID] = DM.[numReturnHeaderID]) AS VARCHAR(20)) ELSE 'Unapplied Payment' END AS vcReference,
		numDepositId AS numReferenceID,CAST(monDepositAmount AS DECIMAL(19,2)) AS monAmount,
		ISNULL(CAST(monDepositAmount AS DECIMAL(19,2)),0) - ISNULL(CAST(monAppliedAmount AS DECIMAL(19,2)),0)  /*- ISNULL(CAST(monRefundAmount AS DECIMAL(19,2)),0)*/ AS monNonAppliedAmount,
		ISNULL(numReturnHeaderID,0) AS numReturnHeaderID,CAST (0 AS BIT) AS bitIsPaid,0 AS monAmountPaid,DM.numCurrencyID,ISNULL(C.varCurrSymbol,'') varCurrSymbol,
		ISNULL(NULLIF(DM.fltExchangeRate,0),1) fltExchangeRateReceivedPayment,@BaseCurrencySymbol BaseCurrencySymbol,
		CASE WHEN tintDepositePage=3 THEN '' ELSE ISNULL(DM.vcReference,'') END AS vcDepositReference  
		FROM DepositMaster DM LEFT JOIN dbo.Currency C ON C.numCurrencyID = DM.numCurrencyID
		WHERE DM.numDomainId=@numDomainId AND numDivisionID=@numDivisionID AND tintDepositePage IN(2,3)
		AND (ISNULL(CAST(monDepositAmount AS DECIMAL(19,2)),0) - ISNULL(CAST(monAppliedAmount AS DECIMAL(19,2)),0)) > 0 AND numDepositId!=@numReferenceId
		AND (ISNULL(DM.numCurrencyID,@numBaseCurrencyID) = @numCurrencyID OR @numCurrencyID =0)
		AND (ISNULL(DM.numAccountClass,0)=@numAccountClass OR @numAccountClass=0)
	END
	ELSE
	BEGIN
		SELECT tintDepositePage AS tintRefType,CASE WHEN tintDepositePage=3 THEN 'Credit Memo #' + CAST((SELECT TOP 1 [ReturnHeader].[vcRMA] FROM [dbo].[ReturnHeader] WHERE [ReturnHeader].[numReturnHeaderID] = DM.[numReturnHeaderID]) AS VARCHAR(20)) ELSE 'Unapplied Payment' END AS vcReference,
		numDepositId AS numReferenceID,CAST(monDepositAmount AS DECIMAL(19,2)) AS monAmount,
		ISNULL(CAST(monDepositAmount AS DECIMAL(19,2)),0) - ISNULL(CAST(monAppliedAmount AS DECIMAL(19,2)),0) /*- ISNULL(CAST(monRefundAmount AS DECIMAL(19,2)),0)*/ AS monNonAppliedAmount,
		ISNULL(numReturnHeaderID,0) AS numReturnHeaderID,CAST (0 AS BIT) AS bitIsPaid,0 AS monAmountPaid,DM.numCurrencyID,ISNULL(C.varCurrSymbol,'') varCurrSymbol
		,ISNULL(NULLIF(DM.fltExchangeRate,0),1) fltExchangeRateReceivedPayment,@BaseCurrencySymbol BaseCurrencySymbol,CASE WHEN tintDepositePage=3 THEN '' ELSE ISNULL(DM.vcReference,'') END  AS vcDepositReference 
		FROM DepositMaster DM LEFT JOIN dbo.Currency C ON C.numCurrencyID = DM.numCurrencyID
		WHERE DM.numDomainId=@numDomainId AND numDivisionID=@numDivisionID AND tintDepositePage IN(2,3)
		AND (ISNULL(CAST(monDepositAmount AS DECIMAL(19,2)),0) - ISNULL(CAST(monAppliedAmount AS DECIMAL(19,2)),0)) > 0 AND numDepositId!=@numReferenceId
		AND (ISNULL(DM.numCurrencyID,@numBaseCurrencyID) = @numCurrencyID OR @numCurrencyID =0)
		AND (ISNULL(DM.numAccountClass,0)=@numAccountClass OR @numAccountClass=0)
		ORDER BY numDepositId DESC
	END		
	
--UNION 
--
--SELECT 2 AS tintRefType,numReturnHeaderID AS numReferenceID,monAmount,monAmountUsed AS monAppliedAmount FROM ReturnHeader WHERE numDomainId=@numDomainId AND numDivisionID=@numDivisionID AND tintReturnType IN (1,4)
END

ELSE IF @tintMode=2
BEGIN

	DECLARE @numReturnHeaderID AS NUMERIC(18,0);SET @numReturnHeaderID=0
	
	IF @numReferenceId>0
	BEGIN
		SELECT @numReturnHeaderID=numReturnHeaderID FROM BillPaymentHeader WHERE numBillPaymentID=@numReferenceId
	END
	
	IF @numReturnHeaderID>0
	BEGIN
		SELECT 1 AS tintRefType,RH.vcRMA AS vcReference
		,BPH.numBillPaymentID AS numReferenceID,monPaymentAmount AS monAmount,ISNULL(monPaymentAmount,0) - ISNULL(monRefundAmount,0) AS monNonAppliedAmount,CAST (1 AS BIT) AS bitIsPaid,ISNULL(monAppliedAmount,0) AS monAmountPaid,ISNULL(BPH.numReturnHeaderID,0) AS numReturnHeaderID,
		BPH.numCurrencyID
						,ISNULL(C.varCurrSymbol,'') varCurrSymbol
						,ISNULL(NULLIF(BPH.fltExchangeRate,0),1) fltExchangeRateReceivedPayment
						,@BaseCurrencySymbol BaseCurrencySymbol
		FROM BillPaymentHeader BPH LEFT JOIN dbo.Currency C ON C.numCurrencyID = BPH.numCurrencyID 
		LEFT JOIN ReturnHeader RH ON RH.numReturnHeaderID=ISNULL(BPH.numReturnHeaderID,0)
		WHERE BPH.numDomainId=@numDomainId AND BPH.numDivisionID=@numDivisionID 
		 AND numBillPaymentID=@numReferenceId AND (ISNULL(BPH.numCurrencyID,@numBaseCurrencyID) = @numCurrencyID OR @numCurrencyID =0)
		 AND (ISNULL(BPH.numAccountClass,0)=@numAccountClass OR @numAccountClass=0)
		 
		UNION
		
		SELECT CASE WHEN ISNULL(BPH.numReturnHeaderID,0)>0 THEN 1 ELSE 0 END AS tintRefType,
			  CASE WHEN ISNULL(BPH.numReturnHeaderID,0)>0 THEN RH.vcRMA ELSE 'Unapplied Bill Payment' END AS vcReference
		,BPH.numBillPaymentID AS numReferenceID,monPaymentAmount AS monAmount,ISNULL(monPaymentAmount,0) - ISNULL(monAppliedAmount,0) - ISNULL(monRefundAmount,0) AS monNonAppliedAmount,CAST (0 AS BIT) AS bitIsPaid,0 AS monAmountPaid,ISNULL(BPH.numReturnHeaderID,0) AS numReturnHeaderID,
		BPH.numCurrencyID
						,ISNULL(C.varCurrSymbol,'') varCurrSymbol
						,ISNULL(NULLIF(BPH.fltExchangeRate,0),1) fltExchangeRateReceivedPayment
						,@BaseCurrencySymbol BaseCurrencySymbol
		FROM BillPaymentHeader BPH LEFT JOIN dbo.Currency C ON C.numCurrencyID = BPH.numCurrencyID 
		LEFT JOIN ReturnHeader RH ON RH.numReturnHeaderID=ISNULL(BPH.numReturnHeaderID,0)
		WHERE BPH.numDomainId=@numDomainId AND BPH.numDivisionID=@numDivisionID 
		AND (ISNULL(CAST(monPaymentAmount AS DECIMAL(19,2)),0) - ISNULL(CAST(monAppliedAmount AS DECIMAL(19,2)),0)) > 0 AND numBillPaymentID!=@numReferenceId
		AND (ISNULL(BPH.numCurrencyID,@numBaseCurrencyID) = @numCurrencyID OR @numCurrencyID =0)
		AND (ISNULL(BPH.numAccountClass,0)=@numAccountClass OR @numAccountClass=0)
		
	END
	ELSE
	BEGIN
		SELECT CASE WHEN ISNULL(BPH.numReturnHeaderID,0)>0 THEN 1 ELSE 0 END AS tintRefType,
			  CASE WHEN ISNULL(BPH.numReturnHeaderID,0)>0 THEN RH.vcRMA ELSE 'Unapplied Bill Payment' END AS vcReference
		,BPH.numBillPaymentID AS numReferenceID,monPaymentAmount AS monAmount,ISNULL(monPaymentAmount,0) - ISNULL(monAppliedAmount,0) - ISNULL(monRefundAmount,0) AS monNonAppliedAmount,CAST (0 AS BIT) AS bitIsPaid,0 AS monAmountPaid  ,ISNULL(BPH.numReturnHeaderID,0) AS numReturnHeaderID,
		BPH.numCurrencyID
						,ISNULL(C.varCurrSymbol,'') varCurrSymbol
						,ISNULL(NULLIF(BPH.fltExchangeRate,0),1) fltExchangeRateReceivedPayment
						,@BaseCurrencySymbol BaseCurrencySymbol
		FROM BillPaymentHeader BPH LEFT JOIN dbo.Currency C ON C.numCurrencyID = BPH.numCurrencyID 
		LEFT JOIN ReturnHeader RH ON RH.numReturnHeaderID=ISNULL(BPH.numReturnHeaderID,0)
		WHERE BPH.numDomainId=@numDomainId AND BPH.numDivisionID=@numDivisionID 
		AND (ISNULL(CAST(monPaymentAmount AS DECIMAL(19,2)),0) - ISNULL(CAST(monAppliedAmount AS DECIMAL(19,2)),0)) > 0.01 AND numBillPaymentID!=@numReferenceId
		AND (ISNULL(BPH.numCurrencyID,@numBaseCurrencyID) = @numCurrencyID OR @numCurrencyID =0)
		AND (ISNULL(BPH.numAccountClass,0)=@numAccountClass OR @numAccountClass=0)
	END		
	
--	SELECT tintReturnType AS tintRefType,'Purchase Return Credit' AS vcReference,numReturnHeaderID AS numReferenceID,monBizDocAmount AS monAmount, ISNULL(monBizDocAmount,0) - ISNULL(monBizDocUsedAmount,0) AS monNonAppliedAmount,CAST (0 AS BIT) AS bitIsPaid,0 AS monAmountPaid  
--		FROM ReturnHeader WHERE numDomainId=@numDomainId AND numDivisionID=@numDivisionID
--		AND numReturnHeaderID NOT IN (SELECT numReturnHeaderID FROM ReturnPaymentHistory WHERE numReferenceID=@numReferenceId AND tintReferenceType=2)
--		AND (ISNULL(monBizDocAmount,0) - ISNULL(monBizDocUsedAmount,0)) >0 AND tintReturnType=2 AND tintReceiveType=2
--		
--	UNION
--	
--	SELECT tintReturnType AS tintRefType,'Purchase Return Credit' AS vcReference,RH.numReturnHeaderID AS numReferenceID,RH.monBizDocAmount, ISNULL(RH.monBizDocAmount,0) - ISNULL(RH.monBizDocUsedAmount,0) + ISNULL(SUM(ISNULL(RPH.monAmount,0)),0) AS monNonAppliedAmount,CAST (1 AS BIT) AS bitIsPaid,ISNULL(SUM(ISNULL(RPH.monAmount,0)),0) AS monAmountPaid  
--		FROM ReturnHeader RH JOIN ReturnPaymentHistory RPH ON RH.numReturnHeaderID=RPH.numReturnHeaderID  
--		WHERE RH.numDomainId=@numDomainId AND RH.numDivisionID=@numDivisionID 
--		AND RPH.numReferenceID=@numReferenceId AND RPH.tintReferenceType=2 AND tintReturnType=2 AND tintReceiveType=2
--		GROUP BY RH.numReturnHeaderID,RH.monBizDocAmount,RH.monBizDocUsedAmount,RH.tintReturnType
--	
	
END
  
END
GO
