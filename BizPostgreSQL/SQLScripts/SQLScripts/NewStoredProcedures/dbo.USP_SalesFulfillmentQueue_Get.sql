SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_SalesFulfillmentQueue_Get')
DROP PROCEDURE USP_SalesFulfillmentQueue_Get
GO
CREATE PROCEDURE [dbo].[USP_SalesFulfillmentQueue_Get]  
	
AS
BEGIN
	DELETE FROM SalesFulfillmentQueue WHERE numOppID NOT IN (SELECT numOppID FROM OpportunityMaster)


	-- FIRST DELETE ENTRIES FROM QUEUE WHICH ARE NOT VALID BECAUSE MASS FULFILLMENT RULE MAY BE CHANGED
	DELETE FROM 
		SalesFulfillmentQueue
	WHERE
		ISNULL(bitExecuted,0) = 0
		AND numSFQID NOT IN (
								SELECT 
									SFQ.numSFQID 
								FROM 
									SalesFulfillmentQueue SFQ
								INNER JOIN
									SalesFulfillmentConfiguration SFC
								ON
									SFQ.numDomainID = SFC.numDomainID
								WHERE
									(SFC.bitRule1IsActive = 1 AND SFC.tintRule1Type = 2 AND SFQ.numOrderStatus=SFC.numRule1OrderStatus)
									OR (SFC.bitRule2IsActive = 1 AND SFQ.numOrderStatus=SFC.numRule2OrderStatus)
									OR (SFC.bitRule3IsActive = 1 AND SFQ.numOrderStatus=SFC.numRule3OrderStatus)
									OR (SFC.bitRule4IsActive = 1 AND SFQ.numOrderStatus=SFC.numRule4OrderStatus)
							)
	
	-- GET TOP 25 entries
	SELECT TOP 25 
		SFQ.numSFQID,
		SFQ.numDomainID,
		SFQ.numUserCntID,
		SFQ.numOppID,
		OM.numDivisionId,
		OM.numContactID,
		D.numCurrencyID,
		D.bitMinUnitPriceRule,
		OM.tintOppType,
		OM.tintOppStatus,
		OM.tintshipped,
		D.numDefaultSalesShippingDoc,
		D.IsEnableDeferredIncome,
		D.bitAutolinkUnappliedPayment,
		ISNULL(OM.intUsedShippingCompany,ISNULL(D.numShipCompany,91)) numShipCompany,
		CASE 
			WHEN (SFC.bitRule1IsActive = 1 AND SFC.tintRule1Type = 2 AND SFQ.numOrderStatus=SFC.numRule1OrderStatus) THEN 1
			WHEN (SFC.bitRule2IsActive = 1 AND SFQ.numOrderStatus=SFC.numRule2OrderStatus) THEN 2
			WHEN (SFC.bitRule3IsActive = 1 AND SFQ.numOrderStatus=SFC.numRule3OrderStatus) THEN 3
			WHEN (SFC.bitRule4IsActive = 1 AND SFQ.numOrderStatus=SFC.numRule4OrderStatus) THEN 4
			ELSE 0
		END	AS tintRule
	FROM 
		SalesFulfillmentQueue SFQ
	INNER JOIN
		SalesFulfillmentConfiguration SFC
	ON
		SFQ.numDomainID = SFC.numDomainID
	INNER JOIN
		OpportunityMaster OM
	ON
		SFQ.numOppID = OM.numOppId
	INNER JOIN
		Domain D
	ON
		SFQ.numDomainID = D.numDomainID
	WHERE
		ISNULL(SFQ.bitExecuted,0) = 0
		AND ((SFC.bitRule1IsActive = 1 AND SFC.tintRule1Type = 2 AND SFQ.numOrderStatus=SFC.numRule1OrderStatus)
		OR (SFC.bitRule2IsActive = 1 AND SFQ.numOrderStatus=SFC.numRule2OrderStatus)
		OR (SFC.bitRule3IsActive = 1 AND SFQ.numOrderStatus=SFC.numRule3OrderStatus)
		OR (SFC.bitRule4IsActive = 1 AND SFQ.numOrderStatus=SFC.numRule4OrderStatus))
	
END