SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ManageDashboardOpe')
DROP PROCEDURE USP_ManageDashboardOpe
GO
CREATE PROCEDURE [dbo].[USP_ManageDashboardOpe]
@numDomainID AS NUMERIC(9),
@numDashBoardID as NUMERIC(9),
@charOpe as char(1),
@strText TEXT
AS

IF @charOpe='X'
begin
	delete ReportDashboard where numDomainID=@numDomainID AND numDashBoardID=@numDashBoardID
END
else if @charOpe='A'
BEGIN
	DECLARE  @hDocItem INT
      IF CONVERT(VARCHAR(10),@strText) <> ''
        BEGIN
          EXEC sp_xml_preparedocument
            @hDocItem OUTPUT ,
            @strText
            
		  UPDATE ReportDashboard SET tintColumn=X.tintColumn,tintRow=X.tintRow,intHeight=X.intHeight,intWidth=X.intWidth
		  FROM  (SELECT *
                  FROM   OPENXML (@hDocItem, '/NewDataSet/Table1', 2)
                            WITH (numDashBoardID NUMERIC(9),
								  tintColumn tinyint,
								  tintRow TINYINT,intHeight INT,intWidth INT)) X
		  WHERE ReportDashboard.numDomainID=@numDomainID AND ReportDashboard.numDashBoardID=x.numDashBoardID
          
          EXEC sp_xml_removedocument
            @hDocItem
        END
	
END

GO
