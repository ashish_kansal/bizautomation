SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetItemAttributes')
DROP PROCEDURE dbo.USP_GetItemAttributes
GO
CREATE PROCEDURE [dbo].[USP_GetItemAttributes]
(
	@numDomainID NUMERIC(9),
	@numWareHouseID AS NUMERIC(9) = 0,
    @FilterQuery AS VARCHAR(2000) = '',
    @numCategoryID AS NUMERIC(18) = 0
)
AS 
BEGIN
	SELECT
		Fld_id 
		,Fld_type 
		,Fld_label 
		,numlistid
		,bitAutoComplete
	FROM
		CFW_Fld_Master
	WHERE
		numDomainID=@numDomainID
		AND Grp_id=9	
	ORDER BY bitAutoComplete
END
