GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_StagePercentageDetailsTaskNotes_GetByTaskID')
DROP PROCEDURE dbo.USP_StagePercentageDetailsTaskNotes_GetByTaskID
GO
CREATE PROCEDURE [dbo].[USP_StagePercentageDetailsTaskNotes_GetByTaskID]
(
	@numTaskID NUMERIC(18,0)
)
AS 
BEGIN
	SELECT * FROM StagePercentageDetailsTaskNotes WHERE numTaskID = @numTaskID
END
GO