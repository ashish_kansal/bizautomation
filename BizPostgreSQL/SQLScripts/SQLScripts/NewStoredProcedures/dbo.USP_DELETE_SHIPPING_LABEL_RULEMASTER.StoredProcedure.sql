GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_DELETE_SHIPPING_LABEL_RULEMASTER')
	DROP PROCEDURE USP_DELETE_SHIPPING_LABEL_RULEMASTER
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[USP_DELETE_SHIPPING_LABEL_RULEMASTER]
	@numShippingRuleID numeric(18, 0),
	@numDomainID		NUMERIC(18,0)	
AS

DELETE FROM PackagingRules where numDomainID=@numDomainID and numShippingRuleID=@numShippingRuleID--added by  sachin(31stJuly2014) || To Delete Packaging rules on the Basis of Shipping Rule ID
DELETE FROM dbo.ShippingLabelRuleChild WHERE numShippingRuleID = @numShippingRuleID
DELETE FROM dbo.ShippingRuleStateList WHERE numRuleID = @numShippingRuleID AND numDomainID = @numDomainID
DELETE FROM [dbo].[ShippingLabelRuleMaster] WHERE [numShippingRuleID] = @numShippingRuleID AND numDomainID = @numDomainID

GO
