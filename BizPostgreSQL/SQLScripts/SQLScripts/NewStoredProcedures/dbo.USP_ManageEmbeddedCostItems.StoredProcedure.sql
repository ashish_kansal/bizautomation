GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ManageEmbeddedCostItems')
DROP PROCEDURE USP_ManageEmbeddedCostItems
GO
CREATE PROCEDURE [USP_ManageEmbeddedCostItems]
    @numEmbeddedCostID NUMERIC,
    @numDomainID NUMERIC,
    @strItems TEXT
AS 
    SET NOCOUNT ON

    DELETE  FROM [EmbeddedCostItems]
    WHERE   numEmbeddedCostID = @numEmbeddedCostID
            AND [numDomainID] = @numDomainID

    DECLARE @hDocItem INT
    IF CONVERT(VARCHAR(10), @strItems) <> '' 
        BEGIN
        
			DECLARE @sumTotal DECIMAL(20,5)
        
            EXEC sp_xml_preparedocument @hDocItem OUTPUT, @strItems
            INSERT  INTO EmbeddedCostItems
                    (
                      [numEmbeddedCostID],
                      [numOppBizDocItemID],
                      [monAmount],
                      [numDomainID]
	              )
                    SELECT  @numEmbeddedCostID,
                            X.[numOppBizDocItemID],
                            X.[monAmount],
                            @numDomainID
                    FROM    ( SELECT    *
                              FROM      OPENXML (@hDocItem, '/NewDataSet/Table1', 2)
                                        WITH (numOppBizDocItemID NUMERIC, monAmount DECIMAL(20,5) )
                            ) X
                            
					SELECT  
                            @sumTotal = SUM(ISNULL(X.[monAmount],0))
                    FROM    ( SELECT    *
                              FROM      OPENXML (@hDocItem, '/NewDataSet/Table1', 2)
                                        WITH (numOppBizDocItemID NUMERIC, monAmount DECIMAL(20,5))
                            ) X
                            
                    -- update estimated cost        
					UPDATE [EmbeddedCost] SET [monCost] = ISNULL(@sumTotal,0) WHERE [numEmbeddedCostID] =@numEmbeddedCostID
					
            EXEC sp_xml_removedocument @hDocItem
        END

	