GO
SET ANSI_NULLS on
GO
SET QUOTED_IDENTIFIER on
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_SaveEmailTemplates')
DROP PROCEDURE USP_SaveEmailTemplates
GO
CREATE PROCEDURE [dbo].[USP_SaveEmailTemplates]          
(          
	 @numDomainID as numeric(9)=null,          
	 @vcDocDesc as text='',          
	 @VcFileName as varchar(100)='',          
	 @vcDocName as varchar(100)='',          
	 @numDocCategory as numeric(9)=null,          
	 @vcfiletype as varchar(10)='',          
	 @numDocStatus as numeric(9)=null,          
	 @numUserCntID as numeric(9)=null,          
	 @numGenDocId as numeric(9)=0 output,          
	 @cUrlType as varchar(1)='',        
	 @vcSubject as varchar(500)='' ,  
	 @vcDocumentSection VARCHAR(10),
	 @numRecID as numeric(9)=0,
	 @tintDocumentType TINYINT=1,-- 1 = generic, 2= specific document
	 @numModuleID NUMERIC =0,
	 @vcContactPosition VARCHAR(200)='',
	 @BizDocOppType VARCHAR(10),
	 @BizDocType VARCHAR(10),
	 @BizDocTemplate VARCHAR(10),
	 @numCategoryId NUMERIC(18,0),
	 @bitLastFollowupUpdate BIT,
	 @vcGroupsPermission VARCHAR(500),
	 @numFollowUpStatusId NUMERIC(18,0),
	 @bitUpdateFollowupStatus BIT
)          
AS          

          
IF @numGenDocId=0           
BEGIN          

		IF @numModuleID=0 SET @numModuleID=null

		INSERT INTO GenericDocuments  
		 (          
			 numDomainID,          
			 vcDocDesc,          
			 VcFileName,          
			 vcDocName,          
			 numDocCategory,          
			 vcfiletype,          
			 numDocStatus,          
			 numCreatedBy,          
			 bintCreatedDate,          
			 numModifiedBy,          
			 bintModifiedDate,          
			 cUrlType,        
			 vcSubject,
			 vcDocumentSection,
			 numRecID,
		 tintDocumentType,
		 numModuleID,vcContactPosition,
		 BizDocOppType,
		 BizDocType,
		 BizDocTemplate,
		 numCategoryId,
		 bitLastFollowupUpdate,
		 vcGroupsPermission,
		 numFollowUpStatusId,
		 bitUpdateFollowupStatus
	 )          
	VALUES          
	 (          
		 @numDomainID,          
		 @vcDocDesc,          
		 @VcFileName,          
		 @vcDocName,          
		 @numDocCategory,          
		 @vcfiletype,          
		 @numDocStatus,          
		 @numUserCntID,           
		 getutcdate(),          
		 @numUserCntID,          
		 getutcdate(),          
		 @cUrlType,        
		 @vcSubject,
		 @vcDocumentSection,
		 @numRecID,
		 @tintDocumentType,
		 @numModuleID,@vcContactPosition,
		 @BizDocOppType,
		 @BizDocType,
		 @BizDocTemplate,
		 @numCategoryId,
		 @bitLastFollowupUpdate,
		 @vcGroupsPermission,
		 @numFollowUpStatusId,
		 @bitUpdateFollowupStatus
	 )          
          
	SET @numGenDocId=@@identity           
          
END          
          
ELSE          
          
BEGIN          
          
	 IF @numModuleID = 0  SET @numModuleID = NULL;
 
	 UPDATE  GenericDocuments          
           
	 SET           
			vcDocDesc=@vcDocDesc,          
			VcFileName=@VcFileName,          
			vcDocName=@vcDocName,          
			numDocCategory=@numDocCategory,          
			vcfiletype=@vcfiletype,
			numDocStatus=@numDocStatus,          
			numModifiedBy=@numUserCntID,          
			bintModifiedDate=getutcdate(),          
			cUrlType=@cUrlType,        
			vcSubject=@vcSubject,
			vcDocumentSection = @vcDocumentSection,
			numModuleID= @numModuleID,vcContactPosition=@vcContactPosition,
			BizDocOppType =@BizDocOppType,
			BizDocType = @BizDocType,
			BizDocTemplate = @BizDocTemplate,
			numCategoryId = @numCategoryId,
			bitLastFollowupUpdate = @bitLastFollowupUpdate,
			vcGroupsPermission=@vcGroupsPermission,
			numFollowUpStatusId=@numFollowUpStatusId,
			bitUpdateFollowupStatus=@bitUpdateFollowupStatus
           
	 WHERE numGenericDocID = @numGenDocId          
          
END

GO


