GO
IF EXISTS (SELECT * FROM sysobjects WHERE xtype='p' AND NAME = 'USP_WarehouseItems_BulkRemove') 
	DROP PROCEDURE USP_WarehouseItems_BulkRemove

GO
CREATE PROCEDURE [dbo].USP_WarehouseItems_BulkRemove
(
	@numDomainID NUMERIC(18,0)
)
AS
BEGIN
	DECLARE @TableWarehouseLocation TABLE
	(
		ID INT IDENTITY(1,1)
		,numWarehouseItemID NUMERIC(18,0)
	)

	INSERT INTO @TableWarehouseLocation
	(
		numWarehouseItemID
	)
	SELECT
		numWareHouseItemID
	FROM
		WareHouseItems
	WHERE
		numDomainID=@numDomainID
		AND ISNULL(numWLocationID,0) > 0
		AND ISNULL(numOnHand,0) = 0
		AND ISNULL(numAllocation,0) = 0
		AND ISNULL(numBackOrder,0) = 0
		AND ISNULL(numOnOrder,0) = 0
		AND NOT EXISTS (SELECT OI.numoppitemtCode FROM [OpportunityMaster] OM INNER JOIN [OpportunityItems] OI ON OM.[numOppId] = OI.[numOppId] WHERE OM.[numDomainId]=@numDomainID AND OI.[numWarehouseItmsID]=WareHouseItems.numWareHouseItemID)
		AND NOT EXISTS (SELECT OIRL.ID FROM OpportunityItemsReceievedLocation OIRL WHERE OIRL.[numDomainId]=@numDomainID AND OIRL.numWarehouseItemID=WareHouseItems.numWareHouseItemID)
		AND NOT EXISTS (SELECT OKI.numOppChildItemID FROM OpportunityKitItems OKI WHERE OKI.numWarehouseItemID=WareHouseItems.numWareHouseItemID)
		AND NOT EXISTS (SELECT OKCI.numOppKitChildItemID FROM OpportunityKitChildItems OKCI WHERE OKCI.numWarehouseItemID=WareHouseItems.numWareHouseItemID)

		
	DECLARE @i INT = 1
	DECLARE @iCount INT
	DECLARE @numWareHouseItemID NUMERIC(18,0)

	SELECT @iCount=COUNT(*) FROM @TableWarehouseLocation

	WHILE @i <= @iCount
	BEGIN
		SELECT @numWareHouseItemID=numWarehouseItemID FROM @TableWarehouseLocation WHERE ID=@i 

		DELETE from WareHouseItmsDTL WHERE numWareHouseItemID=@numWareHouseItemID 
		DELETE FROM WareHouseItems_Tracking WHERE numDomainID=@numDomainID AND numWareHouseItemID=@numWareHouseItemID 
		DELETE from WareHouseItems WHERE numDomainID=@numDomainID AND numWareHouseItemID=@numWareHouseItemID

		SET @i = @i + 1
	END		
END

