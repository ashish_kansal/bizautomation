GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_DeleteBreadCrumb')
DROP PROCEDURE USP_DeleteBreadCrumb
GO

/****** Object:  StoredProcedure [dbo].[USP_DeleteBreadCrumb]    Script Date: 08/08/2009 16:31:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[USP_DeleteBreadCrumb]
	@numBreadCrumbID numeric(9),
	@numSiteID NUMERIC(9)
AS

DELETE FROM [SiteBreadCrumb]
WHERE numBreadCrumbID = @numBreadCrumbID AND numSiteID =@numSiteID