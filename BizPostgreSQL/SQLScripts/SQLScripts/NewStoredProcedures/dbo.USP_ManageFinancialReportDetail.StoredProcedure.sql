GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_ManageFinancialReportDetail' ) 
    DROP PROCEDURE USP_ManageFinancialReportDetail
GO
CREATE PROC [dbo].[USP_ManageFinancialReportDetail]
    @numFRDtlID NUMERIC(18, 0) = 0 OUTPUT,
    @numFRID NUMERIC(18, 0),
    @numValue1 NUMERIC(18, 0)=null,
    @numValue2 NUMERIC(18, 0)=null,
    @vcValue1 VARCHAR(50)=NULL
AS 
IF @numFRDtlID > 0 
    BEGIN
        UPDATE  [dbo].[FinancialReportDetail]
        SET     [numFRID] = @numFRID,
                [numValue1] = @numValue1,
                [numValue2] = @numValue2,
                [vcValue1] = @vcValue1
        WHERE   [numFRDtlID] = @numFRDtlID
    END 
ELSE 
    BEGIN
        INSERT  INTO [dbo].[FinancialReportDetail] ( [numFRID],
                                                     [numValue1],
                                                     [numValue2],
                                                     [vcValue1] )
                SELECT  @numFRID,
                        @numValue1,
                        @numValue2,
                        @vcValue1
	
        SET @numFRDtlID = SCOPE_IDENTITY()
    END	
	
GO