GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_FeaturedItem' ) 
    DROP PROCEDURE USP_FeaturedItem
GO
CREATE PROCEDURE USP_FeaturedItem
    @numNewDomainID NUMERIC(9) ,
    @numReportId NUMERIC(9) ,
    @numSiteId NUMERIC(9)
AS 
    BEGIN

        DECLARE @strsql VARCHAR(MAX) 
        DECLARE @DefaultWarehouseID NUMERIC(9)
       
        SELECT  @DefaultWarehouseID = numDefaultWareHouseID
        FROM    dbo.eCommerceDTL
        WHERE   numDomainId = @numNewDomainID
        SET @strsql = 'DECLARE @numDomainId NUMERIC;
       SET @numDomainid =' + CONVERT(VARCHAR, @numNewDomainID)
            + ';
     select X.##Items## from ( '
        DECLARE @query AS VARCHAR(MAX)
        SELECT  @query = textQuery
        FROM    dbo.CustomReport
        WHERE   dbo.CustomReport.numCustomReportID = @numReportId 
        SET @strsql = @strsql + ' ' + @query + ') X'
        PRINT @strSql

 
        CREATE TABLE #temptable ( ItemCode NUMERIC(9) ) 
        INSERT  INTO #temptable
                EXEC ( @strsql)
SELECT X.*, ( SELECT TOP 1
                            vcCategoryName
                  FROM      dbo.Category
                  WHERE     numCategoryID = X.numCategoryID
                ) AS vcCategoryName
                
  FROM (        
  SELECT  I.numItemCode ,
                I.vcItemName ,
                I.txtItemDesc ,
                ( SELECT TOP 1
                            numCategoryID
                  FROM      ItemCategory
                  WHERE     numItemID = I.numItemCode
                ) AS numCategoryID ,
                CASE WHEN EXISTS ( SELECT TOP 1
                                            vcPathForTImage
                                   FROM     dbo.ItemImages II
                                   WHERE    II.bitDefault = 1
                                            AND II.numItemCode = I.numItemCode )
                     THEN ( SELECT TOP 1  vcPathForTImage
                            FROM    dbo.ItemImages II
                            WHERE   II.bitDefault = 1
                                    AND II.numItemCode = I.numItemCode
                          )
                     WHEN EXISTS ( SELECT TOP 1
                                            vcPathForTImage
                                   FROM     dbo.ItemImages II
                                   WHERE    II.numItemCode = I.numItemCode )
                     THEN ( SELECT TOP 1  vcPathForTImage
                            FROM    dbo.ItemImages II
                            WHERE   II.numItemCode = I.numItemCode
                          )
                     ELSE ''
                END AS vcPathForTImage ,
                ISNULL(CASE WHEN I.[charItemType] = 'P'
                            THEN CASE WHEN I.bitSerialized = 1
                                      THEN 1.00000 * monListPrice
                                      ELSE 1.00000
                                           * ISNULL(( SELECT TOP 1
                                                              W.[monWListPrice]
                                                      FROM    WarehouseItems W
                                                      WHERE   W.numDomainID = @numNewDomainID
															  AND W.numItemID = I.numItemCode
                                                              AND W.numWarehouseID = @DefaultWarehouseID
                                                    ), NULL)
                                 END
                            ELSE 1.00000 * monListPrice
                       END, 0) AS monListPrice ,
                dbo.fn_UOMConversion(ISNULL(I.numSaleUnit, 0), I.numItemCode,
                                     I.numDomainID, ISNULL(I.numBaseUnit, 0)) AS UOMConversionFactor ,
                I.vcSKU ,
                I.vcManufacturer,
                ISNULL(I.vcModelID,'') AS vcModelID
        FROM    Item AS I
        WHERE   I.numItemCode IN ( SELECT   ItemCode
                                   FROM     #tempTable )
                AND I.numDomainId = @numNewDomainID
                
) AS X
    DROP TABLE #tempTable
    END
--EXEC USP_FeaturedItem @numNewDomainID= 1 , @numReportId = 0 ,@numSiteId = 18
--exec USP_FeaturedItem @numNewDomainID=1,@numReportId=6774,@numSiteId=106