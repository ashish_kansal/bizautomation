/****** Object:  StoredProcedure [dbo].[USP_CheckCanbeReOpenOppertunity]    Script Date: 07/26/2008 16:20:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_CheckCanbeReOpenOppertunity')
DROP PROCEDURE USP_CheckCanbeReOpenOppertunity
GO
CREATE PROCEDURE [dbo].[USP_CheckCanbeReOpenOppertunity]              
@intOpportunityId AS NUMERIC(18,0),
@bitCheck AS BIT=0
AS              
BEGIN         
	DECLARE @numDomainID NUMERIC(18,0)
	DECLARE @OppType AS VARCHAR(2)   
	DECLARE @Sel AS int   
	DECLARE @itemcode AS NUMERIC     
	DECLARE @numWareHouseItemID AS NUMERIC(18,0)
	DECLARE @numWLocationID AS NUMERIC(18,0)                                
	DECLARE @numUnits AS FLOAT                                            
	DECLARE @numQtyShipped AS FLOAT
	DECLARE @onHand AS FLOAT                                                                                 
	DECLARE @numoppitemtCode AS NUMERIC(9)
	DECLARE @Kit AS BIT                   
	DECLARE @LotSerial AS BIT                   
	DECLARE @numQtyReceived AS FLOAT

	SET @Sel=0          
	SELECT 
		@OppType=tintOppType,
		@numDomainID=numDomainId
	FROM 
		OpportunityMaster 
	WHERE 
		numOppId=@intOpportunityId
 
	IF @OppType=2
	BEGIN
		SELECT TOP 1 
			@numoppitemtCode=numoppitemtCode,
			@itemcode=OI.numItemCode,
			@numUnits=ISNULL(numUnitHour,0),
			@numQtyShipped=ISNULL(numQtyShipped,0),
			@numQtyReceived=ISNULL(numUnitHourReceived,0),
			@numWareHouseItemID=numWarehouseItmsID,
			@numWLocationID=ISNULL(numWLocationID,0),
			@Kit= (CASE WHEN bitKitParent=1 and bitAssembly=1 THEN 0 WHEN bitKitParent=1 THEN 1 ELSE 0 END),
			@LotSerial=(CASE WHEN I.bitLotNo=1 OR I.bitSerialized=1 THEN 1 ELSE 0 END) 
		FROM 
			OpportunityItems OI
		LEFT JOIN
			WareHouseItems WI
		ON
			OI.numWarehouseItmsID=WI.numWareHouseItemID                                            
		JOIN 
			Item I                                            
		ON 
			OI.numItemCode=I.numItemCode 
			AND numOppId=@intOpportunityId 
			AND (bitDropShip=0 OR bitDropShip IS NULL)                                        
		WHERE 
			charitemtype='P' 
		ORDER BY 
			OI.numoppitemtCode

		WHILE @numoppitemtCode>0                               
		BEGIN  
			-- Selected Global Warehouse Location 
			IF EXISTS (SELECT ID FROM OpportunityItemsReceievedLocation WHERE numDomainID=@numDomainID AND numOppID=@intOpportunityId AND numOppItemID=@numoppitemtCode)
			BEGIN
				IF (SELECT 
						COUNT(*)
					FROM
						OpportunityItemsReceievedLocation OIRL
					INNER JOIN
						WareHouseItems WI
					ON
						OIRL.numWarehouseItemID=WI.numWareHouseItemID
					WHERE
						OIRL.numDomainID=@numDomainID
						AND numOppID=@intOpportunityId
						AND numOppItemID=@numoppitemtCode
					GROUP BY
						OIRL.numWarehouseItemID,
						WI.numOnHand
					HAVING 
						WI.numOnHand < SUM(OIRL.numUnitReceieved)) > 0
				BEGIN
					IF @Sel=0 SET @Sel=1
				END
				ELSE
				BEGIN
					SET @numUnits = @numUnits - (SELECT SUM(numUnitReceieved) FROM OpportunityItemsReceievedLocation WHERE numDomainID=@numDomainID AND numOppID=@intOpportunityId AND numOppItemID=@numoppitemtCode)
				END
			END
			
			IF @Sel = 0
			BEGIN
				SELECT 
					@onHand = ISNULL(numOnHand, 0) 
				FROM
					WareHouseItems 
				WHERE 
					numWareHouseItemID=@numWareHouseItemID

				IF @onHand < @numUnits
				BEGIN  
					IF @Sel=0 SET @Sel=1
				END
			END

			SELECT TOP 1 
				@numoppitemtCode=numoppitemtCode,
				@itemcode=OI.numItemCode,
				@numUnits=ISNULL(numUnitHour,0),
				@numQtyShipped=ISNULL(numQtyShipped,0),
				@numQtyReceived=ISNULL(numUnitHourReceived,0),
				@numWareHouseItemID=numWarehouseItmsID,
				@numWLocationID=ISNULL(numWLocationID,0),
				@Kit= (CASE WHEN bitKitParent=1 AND bitAssembly=1 THEN 0 WHEN bitKitParent=1 THEN 1 ELSE 0 END),
				@LotSerial=(CASE WHEN I.bitLotNo=1 OR I.bitSerialized=1 THEN 1 ELSE 0 END) 
			FROM 
				OpportunityItems OI                                            
			LEFT JOIN
				WareHouseItems WI
			ON
				OI.numWarehouseItmsID=WI.numWareHouseItemID
			JOIN 
				Item I                                            
			ON 
				OI.numItemCode=I.numItemCode and numOppId=@intOpportunityId                                          
			WHERE 
				charitemtype='P' 
				AND OI.numoppitemtCode>@numoppitemtCode 
				AND (bitDropShip=0 OR bitDropShip IS NULL) 
			ORDER BY 
				OI.numoppitemtCode                                            
    
			IF @@rowcount=0 
				SET @numoppitemtCode=0    
		END  
	END      


	IF EXISTS (SELECT * FROM dbo.ReturnHeader WHERE numOppId = @intOpportunityId)         
	BEGIN
		SET @Sel = -1 * @OppType;
	END	
                                    
  
	SELECT @Sel
END
GO
