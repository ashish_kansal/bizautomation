--EXEC USP_GetSalesTemplateItems 6,72
--SELECT * FROM SalesTemplateItems
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetSalesTemplateItems')
DROP PROCEDURE USP_GetSalesTemplateItems
GO
CREATE PROCEDURE [dbo].[USP_GetSalesTemplateItems]
    @numSalesTemplateID NUMERIC(18,0),
    @numDomainID NUMERIC(18,0),
	@numDivisionID NUMERIC(18,0)
AS
	DECLARE @tintPriceLevel TINYINT
	DECLARE @tintDefaultSalesPricing AS TINYINT

	SELECT @tintDefaultSalesPricing=numDefaultSalesPricing FROM Domain WHERE numDomainId=@numDomainID

	SELECT 
		@tintPriceLevel = ISNULL(D.tintPriceLevel,0)
	FROM 
		DivisionMaster D                
	WHERE 
		numDivisionID =@numDivisionID       

    SELECT  
		X.*
		,(SELECT TOP 1 vcCategoryName FROM [Category] WHERE numCategoryID IN (SELECT numCategoryID FROM [ItemCategory] IC WHERE X.numItemCode = IC.[numItemID])) AS vcCategoryName
    FROM    
	( 
		SELECT DISTINCT
            STI.[numSalesTemplateItemID],
            STI.[numSalesTemplateID],
            STI.[numSalesTemplateItemID] AS numoppitemtCode,
            STI.[numItemCode],
            STI.[numUnitHour],
            CASE WHEN ISNULL(@numDivisionID,0) > 0 AND ISNULL(@tintPriceLevel,0) > 0 AND @tintDefaultSalesPricing = 1 AND (SELECT COUNT(*) FROM PricingTable WHERE numItemCode=STI.[numItemCode] AND ISNULL(numPriceRuleID,0) = 0 AND ISNULL(numCurrencyID,0) = 0) > 0 THEN 
			ISNULL((SELECT 
						ISNULL((CASE 
											WHEN TEMP.tintRuleType = 1 AND TEMP.tintDiscountType = 1
											THEN ISNULL(I.monListPrice,0) - (ISNULL(I.monListPrice,0) * (TEMP.decDiscount / 100 ) )
											WHEN TEMP.tintRuleType = 1 AND TEMP.tintDiscountType = 2
											THEN ISNULL(I.monListPrice,0) - TEMP.decDiscount
											WHEN TEMP.tintRuleType = 2 AND TEMP.tintDiscountType = 1
											THEN ISNULL(Vendor.monCost,0) + (ISNULL(Vendor.monCost,0) * (TEMP.decDiscount / 100 ) )
											WHEN TEMP.tintRuleType = 2 AND TEMP.tintDiscountType = 2
											THEN ISNULL(Vendor.monCost,0) + TEMP.decDiscount
											WHEN TEMP.tintRuleType = 3
											THEN TEMP.decDiscount
										END),STI.[monPrice])
					FROM
					(
						SELECT 
							ROW_NUMBER() OVER(PARTITION BY PricingTable.numItemCode ORDER BY numPricingID) Id,
							*
						FROM
							PricingTable
						WHERE 
							PricingTable.numItemCode = STI.[numItemCode]
							AND ISNULL(numCurrencyID,0) = 0
					) TEMP
					WHERE
						numItemCode=STI.[numItemCode] 
						AND (tintRuleType = 3 AND Id=@tintPriceLevel)),STI.[monPrice]) ELSE STI.[monPrice] END monPrice,
            STI.[monTotAmount],
            STI.[numSourceID],
            STI.[numWarehouseID],
            STI.[Warehouse],
            STI.[numWarehouseItmsID],
            STI.[ItemType],
            STI.[ItemType] as vcType,
            STI.[Attributes],
			STI.[Attributes] as vcAttributes,
            STI.[AttrValues],
            I.[bitFreeShipping] FreeShipping, --Use from Item table
            STI.[Weight],
            STI.[Op_Flag],
            STI.[bitDiscountType],
            STI.[fltDiscount],
            STI.[monTotAmtBefDiscount],
            STI.[ItemURL],
            STI.[numDomainID],
            '' vcItemDesc,
            (SELECT TOP 1 vcPathForTImage FROM dbo.ItemImages II WHERE II.numItemCode = I.numItemCode AND II.numDomainId= I.numDomainId AND II.bitDefault=1) vcPathForTImage,
            I.vcItemName,
            'False' AS DropShip,
			0 as bitDropShip,
            I.[bitTaxable],
            ISNULL(STI.numUOM,0) numUOM,
			ISNULL(STI.numUOM,0) as numUOMId,
            ISNULL(STI.vcUOMName,'') vcUOMName,
            ISNULL(STI.UOMConversionFactor,1) UOMConversionFactor,
			ISNULL(STI.numVendorWareHouse,0) as numVendorWareHouse
			,ISNULL(STI.numShipmentMethod,0) as numShipmentMethod
			,ISNULL(STI.numSOVendorId,0) as numSOVendorId
			,0 as bitWorkOrder
			,I.charItemType
			,dbo.fn_GetUOMName(I.numBaseUnit) AS vcBaseUOMName
			,0 AS numSortOrder
        FROM 
			SalesTemplateItems STI
        INNER JOIN item I ON I.numItemCode = STI.numItemCode
		LEFT JOIN Vendor ON I.numItemCode=Vendor.numItemCode AND I.numVendorID=Vendor.numVendorID
        LEFT JOIN dbo.UOM U ON U.numUOMID = I.numSaleUnit
        WHERE 
			STI.[numSalesTemplateID] = @numSalesTemplateID
            AND STI.[numDomainID] = @numDomainID
) X