GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ShippingReport_UpdateTrackingDetail')
DROP PROCEDURE USP_ShippingReport_UpdateTrackingDetail
GO
CREATE PROCEDURE [dbo].[USP_ShippingReport_UpdateTrackingDetail]
	@numShippingReportID NUMERIC(18,0)
	,@bitDelivered BIT
	,@vcTrackingDetail VARCHAR(300)
AS
BEGIN
	DECLARE @vcCurrentTrackingDetail AS VARCHAR(300)
	DECLARE @dtLastTrackingUpdate AS DATETIME

	IF EXISTS (SELECT numShippingReportId FROM ShippingReport WHERE numShippingReportId=@numShippingReportID)
	BEGIN
		SELECT
			@vcCurrentTrackingDetail=ISNULL(vcTrackingDetail,'')
			,@dtLastTrackingUpdate=ISNULL(dtLastTrackingUpdate,GETUTCDATE())
		FROM
			ShippingReport
		WHERE
			numShippingReportId=@numShippingReportID

		IF @vcCurrentTrackingDetail = ISNULL(@vcTrackingDetail,'') AND DATEDIFF(DAY, @dtLastTrackingUpdate,GETUTCDATE()) > 3
		BEGIN
			SET @bitDelivered  = 1
			SET @vcTrackingDetail = ''
		END
	
		UPDATE
			ShippingReport 
		SET
			bitDelivered=@bitDelivered
			,vcTrackingDetail=@vcTrackingDetail
			,dtLastTrackingUpdate = GETUTCDATE()
		WHERE
			numShippingReportId=@numShippingReportID
	END
END