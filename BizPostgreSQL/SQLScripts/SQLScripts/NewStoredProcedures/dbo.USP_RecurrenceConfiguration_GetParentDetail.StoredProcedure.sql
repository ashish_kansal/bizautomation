GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_RecurrenceConfiguration_GetParentDetail')
DROP PROCEDURE USP_RecurrenceConfiguration_GetParentDetail
GO
  
-- =============================================  
-- Author:  Sandeep
-- Create date: 9 Oct 2014
-- Description: Gets parent detail of recurred order or bizdoc
-- =============================================  
Create PROCEDURE [dbo].[USP_RecurrenceConfiguration_GetParentDetail]   
	@numID NUMERIC(18,0),
	@numType SMALLINT
AS  
BEGIN  
 
 SET NOCOUNT ON;  

	DECLARE @numRecConfigID AS NUMERIC(18,0)

	IF @numType = 1 --Order
		SELECT @numRecConfigID=numRecConfigID FROM RecurrenceTransaction WHERE numRecurrOppID = @numID
	ELSE IF @numType = 2 --BizDoc
		SELECT @numRecConfigID=numRecConfigID FROM RecurrenceTransaction WHERE numRecurrOppBizDocID = @numID
	
	IF ISNULL(@numRecConfigID,0) > 0
	BEGIN
		IF @numType = 1 --Order
			SELECT
				RowNo,
				numParentOppID,
				vcPOppName AS Name,
				numParentBizDocID
			FROM
				(
					SELECT
						ROW_NUMBER() OVER (ORDER BY RecurrenceTransaction.numRecTranID) AS RowNo,
						numRecurrOppID,
						RecurrenceConfiguration.numOppID AS numParentOppID,
						OpportunityMaster.vcPOppName,
						RecurrenceConfiguration.numOppBizDocID AS numParentBizDocID
					FROM
						RecurrenceTransaction
					INNER JOIN
						RecurrenceConfiguration
					ON
						RecurrenceTransaction.numRecConfigID = RecurrenceConfiguration.numRecConfigID
					INNER JOIN
						OpportunityMaster
					ON
						RecurrenceConfiguration.numOppID = OpportunityMaster.numOppId
					WHERE
						RecurrenceTransaction.numRecConfigID = @numRecConfigID
				) AS TEMP
			WHERE
				TEMP.numRecurrOppID = @numID
		ELSE IF @numType = 2 --BizDoc
			SELECT
				RowNo,
				numParentOppID,
				vcBizDocID AS Name,
				numParentBizDocID
			FROM
				(
					SELECT
						ROW_NUMBER() OVER (ORDER BY RecurrenceTransaction.numRecTranID ASC) AS RowNo,
						numRecurrOppBizDocID,
						RecurrenceConfiguration.numOppID AS numParentOppID,
						OpportunityBizDocs.vcBizDocID,
						RecurrenceConfiguration.numOppBizDocID AS numParentBizDocID
					FROM
						RecurrenceTransaction
					INNER JOIN
						RecurrenceConfiguration
					ON
						RecurrenceTransaction.numRecConfigID = RecurrenceConfiguration.numRecConfigID
					INNER JOIN	
						OpportunityBizDocs
					ON
						RecurrenceConfiguration.numOppBizDocID = OpportunityBizDocs.numOppBizDocsId
					WHERE
						RecurrenceTransaction.numRecConfigID = @numRecConfigID
				) AS TEMP
			WHERE
				TEMP.numRecurrOppBizDocID = @numID
	END
	ELSE
	BEGIN
		SELECT 0 AS RowNo, NULL AS numParentOppID, NULL AS numParentBizDocID
	END
END  
