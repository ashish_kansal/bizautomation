      IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetApprovalForTickler')
DROP PROCEDURE USP_GetApprovalForTickler
GO
CREATE PROCEDURE [dbo].[USP_GetApprovalForTickler]
    @numDomainID AS NUMERIC(9) = 0,
	@startDate datetime =null,
	@endDate datetime=null,
    @numUserCntID AS NUMERIC(9) = 0,
    @ClientTimeZoneOffset AS INT=0
AS 
BEGIN
 IF(@startDate<>'')
 BEGIN
 SELECT  CAST(numCategoryHDRID AS varchar) as RecordId,
                    CASE WHEN numCategory = 1 AND numType=1 THEN 'Billable Time'
						 WHEN numCategory = 1 AND numType=2 THEN 'Non-Billable Time'
                         WHEN numCategory = 2 AND ISNULL(bitReimburse,0) = 1 AND numType = 2  THEN 'Reimbursable Expense'
                         WHEN numCategory = 2 AND ISNULL(bitReimburse,0) = 1 AND numType = 6 THEN 'Billable + Reimbursable Expense'
                         WHEN numCategory = 2 AND ISNULL(bitReimburse,0) = 0 AND numType = 1 THEN 'Billable Expense'
                    END AS [ApprovalRequests], ISNULL(ACIC.vcFirstName,'') + ' ' + ISNULL(ACIC.vcLastName,'') +','+ CONVERT(VARCHAR,(DateAdd(minute, -@ClientTimeZoneOffset, TE.dtTCreatedOn))) as CreatedDate,
					CASE WHEN TE.numApprovalComplete=-1 THEN 'Declined' ELSE 'Pending Approval' END as [status],TE.numApprovalComplete AS ApprovalStatus,
					TE.txtDesc as Notes,
					 OM.vcPOppName as vcPOppName,
					 CASE WHEN I.vcItemName='' THEN '' ELSE '('+I.vcItemName+')' END as vcItemName,
					 ISNULL(ACI.vcFirstName,'') + ' ' + ISNULL(ACI.vcLastName,'') AS [vcEmployee],
					 Case WHEN numCategory = 1 AND numType=1 AND OM.numOppId>0 
						THEN ((ISNULL(CAST(DATEDIFF(MINUTE,TE.dtfromdate,TE.dttodate) AS FLOAT)/CAST(60 AS FLOAT),0))*TE.monAmount)
					 WHEN  OM.numOppId>0 
						THEN TE.monAmount 
					ELSE '0'
						END [ClientCharge],
					Case WHEN numCategory = 1 AND (numType=1 or numType=2)
						THEN ((ISNULL(CAST(DATEDIFF(MINUTE,TE.dtfromdate,TE.dttodate) AS FLOAT)/CAST(60 AS FLOAT),0))*
						(select ISNULL(monHourlyRate,0) from UserMaster where numUserDetailId=TE.numUserCntID    
						and UserMaster.numDomainID=@numDomainID))
					 WHEN numCategory = 2
						THEN TE.monAmount 
					ELSE  0
						END [EmployeeCharge],
					ISNULL(OM.numOppId,0) AS numOppId,
					'1' AS TypeApproval
			FROM    TimeAndExpense TE
            LEFT JOIN dbo.OpportunityMaster OM ON om.numoppId = TE.numOppId AND TE.numDomainID = OM.numDomainID AND OM.tintOppType = 1
			LEFT JOIN Item as I ON TE.numServiceItemID=I.numItemCode
            LEFT JOIN dbo.ProjectsMaster PM ON PM.numProId = TE.numProID AND TE.numDomainID = PM.numDomainID
            LEFT JOIN dbo.Chart_Of_Accounts CA ON CA.numAccountId = TE.numExpId AND TE.numDomainID = CA.numDomainID
            LEFT JOIN AdditionalContactsInformation ACI ON ACI.numContactID = TE.numUserCntID
            LEFT JOIN AdditionalContactsInformation ACIC ON ACIC.numContactID = TE.numTCreatedBy
			LEFT JOIN ApprovalConfig AS A on TE.numUserCntID=A.numUserId AND A.numDomainID=@numDomainId
            WHERE  A.numModule=1 AND (A.numLevel1Authority=@numUserCntID OR A.numLevel2Authority=@numUserCntID OR A.numLevel3Authority=@numUserCntID
			OR A.numLevel4Authority=@numUserCntID OR A.numLevel5Authority=@numUserCntID) AND TE.numApprovalComplete NOT IN(0)
			AND dateadd(minute,-@ClientTimeZoneOffset,TE.dtTCreatedOn)>= @startDate AND dateadd(minute,-@ClientTimeZoneOffset,TE.dtTCreatedOn)<=@endDate 
			
	UNION ALL
		SELECT M.numOppId as RecordId,'Price Margin Approval' AS [ApprovalRequests],
				 ISNULL(ACIC.vcFirstName,'') + ' ' + ISNULL(ACIC.vcLastName,'') +','+CONVERT(VARCHAR,(DateAdd(minute, -@ClientTimeZoneOffset, M.bintCreatedDate))) as CreatedDate
				,'Pending Approval' as [status],0 as ApprovalStatus,''  as Notes,M.vcPOppName,
				'('+(SELECT SUBSTRING((SELECT ',' + CAST(OpportunityItems.vcItemName AS VARCHAR) FROM OpportunityItems 
				WHERE OpportunityItems.bitItemPriceApprovalRequired=1 AND OpportunityItems.numOppId=M.numOppId FOR XML PATH('')), 2,10000) )+')'
				 AS vcItemName,  
				 '' AS [vcEmployee],0 AS  [ClientCharge],0 AS [EmployeeCharge],M.numOppId
				 ,'2' AS TypeApproval
				 FROM  OpportunityMaster AS M
					LEFT JOIN AdditionalContactsInformation ACIC ON ACIC.numContactID = M.numCreatedBy
					WHERE M.numDomainId=@numDomainID
					AND (SELECT COUNT(OpportunityItems.numoppitemtCode) FROM OpportunityItems 
						WHERE OpportunityItems.bitItemPriceApprovalRequired=1 AND OpportunityItems.numOppId=M.numOppId) >0
					AND @numUserCntID IN (SELECT numUserID FROM UnitPriceApprover WHERE numDomainId=@numDomainID)
					AND dateadd(minute,-@ClientTimeZoneOffset,M.bintCreatedDate)>= @startDate AND dateadd(minute,-@ClientTimeZoneOffset,M.bintCreatedDate)<=@endDate 
					AND M.tintOppType = 1
	UNION ALL
		SELECT M.numOppId as RecordId,'Promotion Approval' AS [ApprovalRequests],
				 ISNULL(ACIC.vcFirstName,'') + ' ' + ISNULL(ACIC.vcLastName,'') +','+CONVERT(VARCHAR,(DateAdd(minute, -@ClientTimeZoneOffset, M.bintCreatedDate))) as CreatedDate
				,'Pending Approval' as [status],M.intPromotionApprovalStatus as ApprovalStatus,''  as Notes,M.vcPOppName,
				'('+(SELECT SUBSTRING((SELECT ',' + CAST(OpportunityItems.vcItemName AS VARCHAR) FROM OpportunityItems 
				WHERE OpportunityItems.bitItemPriceApprovalRequired=1 AND OpportunityItems.numOppId=M.numOppId FOR XML PATH('')), 2,10000) )+')'
				 AS vcItemName,  
				 '' AS [vcEmployee],0 AS  [ClientCharge],0 AS [EmployeeCharge],M.numOppId
				 ,'3' AS TypeApproval
				 FROM  OpportunityMaster AS M
					LEFT JOIN AdditionalContactsInformation ACIC ON ACIC.numContactID = M.numCreatedBy
					LEFT JOIN ApprovalConfig AS A on M.numCreatedBy=A.numUserId WHERE  A.numModule=2 AND
					M.intPromotionApprovalStatus NOT IN(0,-1) AND A.numDomainID=@numDomainId
					AND (A.numLevel1Authority=@numUserCntID OR A.numLevel2Authority=@numUserCntID OR A.numLevel3Authority=@numUserCntID
					OR A.numLevel4Authority=@numUserCntID OR A.numLevel5Authority=@numUserCntID)
					AND dateadd(minute,-@ClientTimeZoneOffset,M.bintCreatedDate)>= @startDate AND dateadd(minute,-@ClientTimeZoneOffset,M.bintCreatedDate)<=@endDate 
					AND M.tintOppType = 1
END
ELSE
BEGIN
	SELECT  CAST(numCategoryHDRID AS varchar) as RecordId,
                    CASE WHEN numCategory = 1 AND numType=1 THEN 'Billable Time'
						 WHEN numCategory = 1 AND numType=2 THEN 'Non-Billable Time'
                         WHEN numCategory = 2 AND ISNULL(bitReimburse,0) = 1 AND numType = 2  THEN 'Reimbursable Expense'
                         WHEN numCategory = 2 AND ISNULL(bitReimburse,0) = 1 AND numType = 6 THEN 'Billable + Reimbursable Expense'
                         WHEN numCategory = 2 AND ISNULL(bitReimburse,0) = 0 AND numType = 1 THEN 'Billable Expense'
                    END AS [ApprovalRequests], ISNULL(ACIC.vcFirstName,'') + ' ' + ISNULL(ACIC.vcLastName,'') +','+ CONVERT(VARCHAR,(DateAdd(minute, -@ClientTimeZoneOffset, TE.dtTCreatedOn))) as CreatedDate,
					CASE WHEN TE.numApprovalComplete=-1 THEN 'Declined' ELSE 'Pending Approval' END as [status],TE.numApprovalComplete AS ApprovalStatus,
					TE.txtDesc as Notes,
					 OM.vcPOppName as vcPOppName,
					 CASE WHEN I.vcItemName='' THEN '' ELSE '('+I.vcItemName+')' END as vcItemName,
					 ISNULL(ACI.vcFirstName,'') + ' ' + ISNULL(ACI.vcLastName,'') AS [vcEmployee],
					 Case WHEN numCategory = 1 AND numType=1 AND OM.numOppId>0 
						THEN ((ISNULL(CAST(DATEDIFF(MINUTE,TE.dtfromdate,TE.dttodate) AS FLOAT)/CAST(60 AS FLOAT),0))*TE.monAmount)
					 WHEN  OM.numOppId>0 
						THEN TE.monAmount 
					ELSE '0'
						END [ClientCharge],
					Case WHEN numCategory = 1 AND (numType=1 or numType=2)
						THEN ((ISNULL(CAST(DATEDIFF(MINUTE,TE.dtfromdate,TE.dttodate) AS FLOAT)/CAST(60 AS FLOAT),0))*
						(select ISNULL(monHourlyRate,0) from UserMaster where numUserDetailId=TE.numUserCntID    
						and UserMaster.numDomainID=@numDomainID))
					 WHEN numCategory = 2
						THEN TE.monAmount 
					ELSE  0
						END [EmployeeCharge],
					ISNULL(OM.numOppId,0) AS numOppId
			FROM    TimeAndExpense TE
            LEFT JOIN dbo.OpportunityMaster OM ON om.numoppId = TE.numOppId AND TE.numDomainID = OM.numDomainID  AND OM.tintOppType = 1
			LEFT JOIN Item as I ON TE.numServiceItemID=I.numItemCode
            LEFT JOIN dbo.ProjectsMaster PM ON PM.numProId = TE.numProID AND TE.numDomainID = PM.numDomainID
            LEFT JOIN dbo.Chart_Of_Accounts CA ON CA.numAccountId = TE.numExpId AND TE.numDomainID = CA.numDomainID
            LEFT JOIN AdditionalContactsInformation ACI ON ACI.numContactID = TE.numUserCntID
            LEFT JOIN AdditionalContactsInformation ACIC ON ACIC.numContactID = TE.numTCreatedBy
			LEFT JOIN ApprovalConfig AS A on TE.numUserCntID=A.numUserId AND A.numDomainID=@numDomainId AND A.numModule=1
            WHERE 
				TE.numDomainID = @numDomainID 
				AND @numUserCntID IN (A.numLevel1Authority,A.numLevel2Authority,A.numLevel3Authority,A.numLevel4Authority,A.numLevel5Authority) 
				AND TE.numApprovalComplete NOT IN (0)
END
END