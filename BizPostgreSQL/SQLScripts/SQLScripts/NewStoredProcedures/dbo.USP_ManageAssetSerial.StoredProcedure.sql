--
GO
/****** Object:  StoredProcedure [dbo].[USP_ManageItemSerialNo]    Script Date: 02/06/2010 13:14:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ManageAssetSerial')
DROP PROCEDURE USP_ManageAssetSerial
GO
CREATE PROCEDURE [dbo].[USP_ManageAssetSerial]
    @numDomainID NUMERIC(9),
    @strFieldList TEXT
AS 
    BEGIN
        DECLARE @hDoc AS INT                                            
       
	 EXEC sp_xml_preparedocument @hDoc OUTPUT, @strFieldList
                     
--                     SELECT * FROM WareHouseItmsDTL
        INSERT  INTO CompanyAssetSerial
                (
                  numAssetItemID,
                  vcSerialNo,
                  numBarCodeId,
				  vcModelId,
				  dtPurchase,
				  dtWarrante,
				  vcLocation
                )
                ( SELECT    numAssetItemID,
                            vcSerialNo,
                            numBarCodeId,
							vcModelId,
							dtPurchase,
							dtWarrante,
							vcLocation
                  FROM      OPENXML(@hDoc, '/NewDataSet/SerializedAsset[Op_Flag=1]',2)
                            WITH ( numAssetItemID NUMERIC(18), vcSerialNo VARCHAR(100),numBarCodeId NUMERIC(18),
							vcModelId varchar(100),dtPurchase datetime,dtWarrante datetime,vcLocation varchar(250), Op_Flag TINYINT ) WHERE  Op_Flag=1  )
                
                                                                       
                                              
       UPDATE  CompanyAssetSerial
        SET     numAssetItemID = X.numAssetItemID,
                vcSerialNo = X.vcSerialNo,
                numBarCodeId = X.numBarCodeId,
				vcModelId=X.vcModelId,
				dtPurchase=X.dtPurchase,
				dtWarrante=X.dtWarrante,
				vcLocation=X.vcLocation
        FROM    ( SELECT    numAssetItemDTLID,
							numAssetItemID,
							vcSerialNo,
                            numBarCodeId,
							vcModelId,
							dtPurchase,
							dtWarrante,
							vcLocation,
                            Op_Flag                            
                  FROM      OPENXML(@hDoc, '/NewDataSet/SerializedAsset[Op_Flag=0]',2)
                            WITH (numAssetItemDTLID numeric(18),  numAssetItemID NUMERIC(18), vcSerialNo VARCHAR(100),numBarCodeId NUMERIC(18),
							vcModelId varchar(100),dtPurchase datetime,dtWarrante datetime,vcLocation varchar(250), Op_Flag TINYINT) where Op_Flag=0
                ) X
        WHERE   CompanyAssetSerial.numAssetItemDTLID = X.numAssetItemDTLID

       EXEC sp_xml_removedocument @hDoc
    END
                      
