SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetAcivitybyGoogleEventId')
DROP PROCEDURE USP_GetAcivitybyGoogleEventId
GO
CREATE PROCEDURE [dbo].[USP_GetAcivitybyGoogleEventId]      
 @GoogleEventId varchar(MAX),
 @ResourceID AS int 
as       
      
 SELECT       
  [Activity].[AllDayEvent],       
  [Activity].[ActivityDescription],       
  [Activity].[Duration],       
  [Activity].[Location],       
  [Activity].[ActivityID],       
  [Activity].[StartDateTimeUtc],       
  [Activity].[Subject],       
  [Activity].[EnableReminder],       
  [Activity].[ReminderInterval],      
  [Activity].[ShowTimeAs],      
  [Activity].[Importance],      
  [Activity].[Status],      
  [Activity].[RecurrenceID],      
  [Activity].[VarianceID],      
[ActivityResource].[ResourceID],      
[Activity].[ItemID],    
[Activity].[ChangeKey],     
[Activity].[ItemIDOccur],  
  [Activity].[_ts] ,  [Activity].GoogleEventId   
 FROM       
  [Activity] INNER JOIN [ActivityResource] ON [Activity].[ActivityID] = [ActivityResource].[ActivityID]      
 WHERE       
  ISNULL(Activity.GoogleEventId,'') = @GoogleEventId AND ISNULL(ResourceID,0)=@ResourceID;   
GO
