GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_PageElementDetail_GetHtml')
DROP PROCEDURE dbo.USP_PageElementDetail_GetHtml
GO
CREATE PROCEDURE [dbo].[USP_PageElementDetail_GetHtml]
(
	@numDomainID NUMERIC(18,0)
	,@numSiteID NUMERIC(18,0)
	,@numElementID NUMERIC(18,0)
)
AS 
BEGIN
	SELECT
		ISNULL(PageElementDetail.vcHtml,'') vcHtml
	FROM
		PageElementDetail
	INNER JOIN
		PageElementAttributes
	ON
		PageElementDetail.numAttributeID = PageElementAttributes.numAttributeID
	WHERE
		PageElementDetail.numDomainID = @numDomainID
		AND PageElementDetail.numSiteID = @numSiteID
		AND PageElementDetail.numElementID = @numElementID
		AND PageElementAttributes.vcAttributeName='Html Customize'
END
GO