-- EXEC USP_GetGeneralLedger 72,89,'2008-10-05 15:13:47.873','2009-10-05 15:13:47.873','','','','','','','',''

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetGeneralLedger')
DROP PROCEDURE USP_GetGeneralLedger
GO
CREATE PROCEDURE [dbo].[USP_GetGeneralLedger]
    (
      @numDomainID INT,
      @vcAccountId VARCHAR(4000),
      @dtFromDate DATETIME,
      @dtToDate DATETIME,
      @vcTranType VARCHAR(50) = '',
      @varDescription VARCHAR(50) = '',
      @CompanyName VARCHAR(50) = '',
      @vcBizPayment VARCHAR(50) = '',
      @vcCheqNo VARCHAR(50) = '',
      @vcBizDocID VARCHAR(50) = '',
      @vcTranRef VARCHAR(50) = '',
      @vcTranDesc VARCHAR(50) = '',
      @numDivisionID NUMERIC(9)=0,
      @ClientTimeZoneOffset INT,  --Added by Chintan to enable calculation of date according to client machine
      @charReconFilter CHAR(1)='',
      @tintMode AS TINYINT=0,
      @numItemID AS NUMERIC(9)=0,
      @CurrentPage INT=0,
	  @PageSize INT=0,
      @TotRecs INT=0  OUTPUT,
	  @TransactionID NUMERIC(9)=0
    )
WITH Recompile -- Added by Chandan 26 Feb 2013 to avoid parameter sniffing
AS 
    BEGIN
    
		/*RollUp of Sub Accounts */
		SELECT  COA2.numAccountId INTO #Temp /*,COA1.vcAccountCode ,COA2.vcAccountCode*/
		FROM    dbo.Chart_Of_Accounts COA1
				INNER JOIN dbo.Chart_Of_Accounts COA2 
				ON COA1.numDomainId = COA2.numDomainId
				AND COA2.vcAccountCode LIKE COA1.vcAccountCode  + '%'
		WHERE   COA1.numAccountId IN(SELECT  cast(ID AS NUMERIC(9)) FROM dbo.SplitIDs(@vcAccountId, ','))
				AND COA1.numDomainId = @numDomainID
		SELECT @vcAccountId = ISNULL( STUFF((SELECT ',' + CAST(numAccountID AS VARCHAR(10)) FROM #Temp FOR XML PATH('')),1, 1, '') , '') 
    
    	SELECT @numDomainID AS numDomainID,dbo.[FormatedDateFromDate](dtAsOnDate,@numDomainID) AS Date,* FROM dbo.fn_GetOpeningBalance(@vcAccountID,@numDomainID,@dtFromDate,@ClientTimeZoneOffset)
    	--following is commented as jeff demoed date GL report date issue in derek's account
--		SET @dtFromDate = DateAdd(minute, @ClientTimeZoneOffset, @dtFromDate);
--		SET @dtToDate = DateAdd(minute, @ClientTimeZoneOffset, @dtToDate);

        SELECT  numDomainId,
                numAccountId,
                TransactionType,
                CompanyName,
                dbo.[FormatedDateFromDate](datEntry_Date,numDomainID) Date,
                varDescription,
                ISNULL(BizPayment,'') + ' ' + ISNULL(CheqNo,'') AS Narration,
                BizDocID,
                TranRef,
                TranDesc,
                CONVERT(VARCHAR(20),ISNULL(numDebitAmt,0)) numDebitAmt,
                CONVERT(VARCHAR(20),ISNULL(numCreditAmt,0)) numCreditAmt,
                vcAccountName,
                '' balance,
                numJournal_Id AS JournalId,
				numCheckHeaderID AS CheckId,
				numCashCreditCardId AS CashCreditCardId,
				numTransactionId,
				numOppId,
				numOppBizDocsId,
				numDepositId,
				numCategoryHDRID,
				tintTEType,
				numCategory,
				dtFromDate,
				numUserCntID,bitReconcile,bitCleared,numBillID,numBillPaymentID,numReturnID,
				STUFF((SELECT ', ' + CAST([COA].[vcAccountName] AS VARCHAR(4000))
				FROM [dbo].[General_Journal_Details] AS GJD JOIN [dbo].[Chart_Of_Accounts] AS COA ON GJD.[numChartAcntId] = COA.[numAccountId]
				WHERE [GJD].[numJournalId] = VIEW_GENERALLEDGER.numJournal_Id 
				AND [COA].[numDomainId] = VIEW_GENERALLEDGER.[numDomainId]
				AND [COA].[numAccountId] <> VIEW_GENERALLEDGER.numAccountId FOR XML PATH('')) ,1,2, '') AS [vcSplitAccountName],
				Row_number() over(order by datEntry_Date DESC) AS RowNumber INTO #tempTable
        FROM    VIEW_GENERALLEDGER
        WHERE   datEntry_Date BETWEEN  @dtFromDate AND @dtToDate -- Removed Tmezone bug id 1029 
        --instead of minus for datEntry_Date i have used it for From and To parameter .. DateAdd(minute, -@ClientTimeZoneOffset, datEntry_Date)
                AND (varDescription LIKE @varDescription + '%' OR LEN(@varDescription)=0)
                AND (BizPayment LIKE @vcBizPayment + '%' OR LEN(@vcBizPayment)=0)
                AND (CheqNo LIKE @vcCheqNo + '%' OR LEN(@vcCheqNo)=0)
                AND numDomainId = @numDomainID 
                AND (BizDocID LIKE @vcBizDocID OR LEN(@vcBizDocID)=0)
                AND (TranRef LIKE @vcTranRef OR LEN(@vcTranRef)=0)
                AND (TranDesc LIKE @vcTranDesc OR LEN(@vcTranDesc)=0)
                AND (TransactionType LIKE @vcTranType + '%' OR LEN(@vcTranType)=0)
                AND (bitCleared = CASE @charReconFilter WHEN 'C' THEN 1 ELSE 0 END  OR RTRIM(@charReconFilter)='A')
                AND (bitReconcile = CASE @charReconFilter WHEN 'R' THEN 1 ELSE 0 END  OR RTRIM(@charReconFilter)='A') 
                AND (CompanyName LIKE @CompanyName + '%' OR LEN(@CompanyName)=0)
                AND (numDivisionID = @numDivisionID OR @numDivisionID = 0)
                AND (numAccountId IN ( SELECT numAccountId FROM #Temp) OR  @tintMode=1)
                AND (numItemID = @numItemID OR @numItemID = 0)
                
         ORDER BY datEntry_Date ASC 
         
         IF @tintMode=1
         BEGIN
		  DECLARE  @firstRec  AS INTEGER
		  DECLARE  @lastRec  AS INTEGER

          SET @firstRec = (@CurrentPage - 1) * @PageSize
		  SET @lastRec = (@CurrentPage * @PageSize + 1)
		  SET @TotRecs = (SELECT COUNT(*) FROM   #tempTable)
         
          SELECT * FROM #tempTable WHERE RowNumber > @firstRec and RowNumber < @lastRec order by RowNumber
		 END
		 ELSE IF @tintMode=2
		 BEGIN
		 	SELECT * FROM #tempTable where numTransactionId = @TransactionID 
		 END
		ELSE
		 BEGIN
		 	SELECT * FROM #tempTable
		 END
         
		  DROP TABLE #tempTable
		  
         DROP TABLE #Temp
    END
