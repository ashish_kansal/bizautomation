GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ItemMarketplaceMapping_Save')
DROP PROCEDURE USP_ItemMarketplaceMapping_Save
GO
CREATE PROCEDURE [dbo].[USP_ItemMarketplaceMapping_Save]  
	@numDomainID NUMERIC(18,0)
	,@numItemCode NUMERIC(18,0)
	,@numMarketplaceID NUMERIC(18,0)
	,@vcMarketplaceUniqueID VARCHAR(300)
AS
BEGIN
	IF EXISTS (SELECT ID FROM ItemMarketplaceMapping WHERE numDomainID=@numDomainID AND numItemCode=@numItemCode AND numMarketplaceID=@numMarketplaceID)
	BEGIN
		UPDATE
			ItemMarketplaceMapping
		SET
			vcMarketplaceUniqueID=@vcMarketplaceUniqueID
		WHERE
			numDomainID=@numDomainID 
			AND numItemCode=@numItemCode 
			AND numMarketplaceID=@numMarketplaceID
	END
	ELSE
	BEGIN
		INSERT INTO ItemMarketplaceMapping
		(
			numDomainID
			,numItemCode
			,numMarketplaceID
			,vcMarketplaceUniqueID
		)
		VALUES
		(
			@numDomainID
			,@numItemCode
			,@numMarketplaceID
			,@vcMarketplaceUniqueID
		)
	END
END