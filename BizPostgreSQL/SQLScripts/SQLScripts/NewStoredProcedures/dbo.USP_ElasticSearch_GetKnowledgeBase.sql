GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ElasticSearch_GetKnowledgeBase')
DROP PROCEDURE dbo.USP_ElasticSearch_GetKnowledgeBase
GO
CREATE PROCEDURE [dbo].[USP_ElasticSearch_GetKnowledgeBase]
	@numDomainID NUMERIC(18,0)
	,@vcSolnID VARCHAR(MAX)
AS 
BEGIN
	SELECT 
		numSolnID AS id
		,'knowledgebase' AS module
		,CONCAT('/cases/frmSolution.aspx?SolId=',numSolnID) AS url
		,CONCAT('<b style="color:#7f6000">','KB Article:</b> ',ISNULL(vcSolnTitle,'')) AS displaytext
		,vcSolnTitle AS [text]
		,ISNULL(vcSolnTitle,'') AS Search_vcSolnTitle
	FROM
		SolutionMaster
	WHERE
		SolutionMaster.numDomainID=@numDomainID
		AND (@vcSolnID IS NULL OR numSolnID IN (SELECT Id FROM dbo.SplitIDs(ISNULL(@vcSolnID,'0'),',')))
END