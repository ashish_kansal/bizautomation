GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_OpportunityBizDocs_GetARAccountID')
DROP PROCEDURE USP_OpportunityBizDocs_GetARAccountID

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[USP_OpportunityBizDocs_GetARAccountID]
    @numOppBizDocsId AS NUMERIC(18,0)
AS 
BEGIN
	DECLARE @numARAccountID NUMERIC(18,0)


	SELECT
		@numARAccountID=COA.numAccountId
	FROM
		OpportunityBizDocs OBD
	INNER JOIN
		Chart_Of_Accounts COA
	ON
		OBD.numARAccountID=COA.numAccountId
	WHERE
		numOppBizDocsId=@numOppBizDocsId

	SELECT ISNULL(@numARAccountID,0) AS numARAccountID
END
GO