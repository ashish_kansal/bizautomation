GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_UpdateGoogleRefreshToken')
DROP PROCEDURE USP_UpdateGoogleRefreshToken
GO
CREATE PROCEDURE USP_UpdateGoogleRefreshToken
    @numUserCntId NUMERIC(18,0),
    @numDomainId NUMERIC(18,0),
	@vcGoogleRefreshToken as varchar(MAX)
AS 
    BEGIN

                UPDATE  ImapUserDetails
                SET     vcGoogleRefreshToken = @vcGoogleRefreshToken
                WHERE   numUserCntId = @numUserCntId
                        AND numDomainID = @numDomainId
	
    END