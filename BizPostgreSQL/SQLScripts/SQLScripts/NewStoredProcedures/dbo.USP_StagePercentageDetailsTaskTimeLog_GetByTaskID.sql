GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_StagePercentageDetailsTaskTimeLog_GetByTaskID')
DROP PROCEDURE dbo.USP_StagePercentageDetailsTaskTimeLog_GetByTaskID
GO
CREATE PROCEDURE [dbo].[USP_StagePercentageDetailsTaskTimeLog_GetByTaskID]
(
	@numDomainID NUMERIC(18,0)
	,@numTaskID NUMERIC(18,0)
	,@ClientTimeZoneOffset INT
	,@bitForProject BIT=0
	,@vcTotalTimeSpendOnTask VARCHAR(50) OUTPUT
)
AS 
BEGIN
	IF(@bitForProject=1)
	BEGIN
		SET @vcTotalTimeSpendOnTask = dbo.GetTimeSpendOnTaskByProject(@numDomainID,@numTaskID,0)
	END
	ELSE
	BEGIN
		SET @vcTotalTimeSpendOnTask = dbo.GetTimeSpendOnTask(@numDomainID,@numTaskID,0)
	END

	SELECT
		SPDTTL.ID
		,SPDTTL.numTaskID
		,SPDTTL.tintAction
		,ISNULL(SPDTTL.numProcessedQty,0) numProcessedQty
		,ISNULL(SPDTTL.numReasonForPause,0) numReasonForPause
		,dbo.fn_GetContactName(SPDTTL.numUserCntID) vcEmployee
		,CASE SPDTTL.tintAction
			WHEN 1 THEN 'Started'
			WHEN 2 THEN 'Paused'
			WHEN 3 THEN 'Resumed'
			WHEN 4 THEN 'Finished'
			ELSE ''
		END vcAction
		,CONCAT(dbo.FormatedDateFromDate(DATEADD(MINUTE,-1 * @ClientTimeZoneOffset,SPDTTL.dtActionTime),@numDomainID),' ',CONVERT(VARCHAR(5),DATEADD(MINUTE,-1 * @ClientTimeZoneOffset,SPDTTL.dtActionTime), 108),' ',RIGHT(CONVERT(VARCHAR(30),DATEADD(MINUTE,-1 * @ClientTimeZoneOffset,SPDTTL.dtActionTime),9),2)) vcActionTime
		,DATEADD(MINUTE,-1 * @ClientTimeZoneOffset,SPDTTL.dtActionTime) dtActionTime
		,CASE WHEN tintAction = 2 THEN CAST(numProcessedQty AS VARCHAR) ELSE '' END vcProcessedQty
		,ISNULL(LD.vcData,'') vcReasonForPause
		,ISNULL(SPDTTL.vcNotes,'') vcNotes
		,(CASE WHEN EXISTS (SELECT ID FROM StagePercentageDetailsTaskTimeLog S WHERE S.numTaskID = @numTaskID AND tintAction=4) THEN 1 ELSE 0 END) bitTaskFinished
	FROM
		StagePercentageDetailsTaskTimeLog SPDTTL
	LEFT JOIN
		ListDetails LD
	ON
		LD.numListID = 52
		AND SPDTTL.numReasonForPause = LD.numListItemID
	WHERE
		SPDTTL.numDomainID=@numDomainID
		AND SPDTTL.numTaskID=@numTaskID
	ORDER BY
		ID 
END
GO