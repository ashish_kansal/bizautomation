set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go

-- EXEC USP_GetInboxItemRange 6,1,7002
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_DelInboxItemsByUid')
DROP PROCEDURE USP_DelInboxItemsByUid
GO
CREATE PROCEDURE [dbo].[USP_DelInboxItemsByUid]
           @numDomainID  AS NUMERIC(9),
           @numUserCntID AS NUMERIC(9),
           @numUid       AS NUMERIC(9),
		   @numNodeId AS NUMERIC(9)
AS
  BEGIN
    DECLARE  @numEmailHstrID NUMERIC(9)
    SELECT @numEmailHstrID = [numEmailHstrID]
    FROM   [EmailHistory]
    WHERE  [numDomainID] = @numDomainID
           AND [numUserCntId] = @numUserCntID
           AND [numUid] = @numUId
		   AND [numNodeId]=@numNodeId
    DELETE emailHistory
    WHERE  [numDomainID] = @numDomainID
           AND [numUserCntId] = @numUserCntID
           AND [numUid] = @numUId
		   AND [numNodeId]=@numNodeId
    DELETE EmailHStrToBCCAndCC
    WHERE  numEmailHstrID = @numEmailHstrID
    DELETE EmailHstrAttchDtls
    WHERE  numEmailHstrID = @numEmailHstrID
  END
