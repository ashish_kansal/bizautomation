GO
/****** Object:  StoredProcedure [dbo].[USP_GET_SHIPPING_LABEL_RULEMASTER]    Script Date: 30/07/2012 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO

IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p' AND NAME ='USP_GET_SHIPPING_LABEL_RULEMASTER')
	DROP PROCEDURE USP_GET_SHIPPING_LABEL_RULEMASTER
GO
-- EXEC USP_GET_SHIPPING_LABEL_RULEMASTER 53,135
CREATE PROCEDURE USP_GET_SHIPPING_LABEL_RULEMASTER
	@numShippingRuleID  NUMERIC(18,0) = 0,
	@numDomainID		NUMERIC(18,0) = 1
AS
BEGIN
	
	DECLARE @SQL nvarchar(MAX)
	DECLARE	@strWhere AS VARCHAR(MAX)

	SET @strWhere = ' AND 1=1 '

	IF ISNULL(@numShippingRuleID,0) <> 0
	BEGIN
		SET @strWhere = @strWhere +  ' AND SR.numShippingRuleID = ' + CAST(@numShippingRuleID AS VARCHAR(10))
	END
		
	SET @SQL =' SELECT DISTINCT
						SR.numShippingRuleID,
						[vcRuleName],
						[tintShipBasedOn],
						[numSourceCompanyID],
						[numSourceShipID],
						[intItemAffected],
						 CASE ISNULL([intItemAffected], 0)
							WHEN 1 THEN (SELECT COUNT(*) FROM [PromotionOfferItems] POI WHERE POI.[numProId] = SR.numShippingRuleID AND POI.tintType=1 AND POI.tintRecordType=3 )
							WHEN 2 THEN (SELECT COUNT(*) FROM [PromotionOfferItems] POI WHERE POI.[numProId] = SR.numShippingRuleID AND POI.tintType=3 AND POI.tintRecordType=3 )
						END AS ItemCount,
						SR.[tintType],
						SR.[numDomainID],
						CASE ISNULL([intItemAffected], 0)
							WHEN 1 THEN (dbo.GetItemAffectedList(SR.numShippingRuleID,1,SR.numDomainID))
							WHEN 2 THEN (dbo.GetItemAffectedList(SR.numShippingRuleID,2,SR.numDomainID))
							ELSE ''ALL''
						END AS [ItemsAffected],
						''Item '' + CASE ISNULL([tintShipBasedOn], 0)
									  WHEN 1 THEN ''Weight''
									  WHEN 2 THEN ''Price''
									  WHEN 3 THEN ''Quantity''
								 END AS [ShipBasedOn],
						ISNULL(WD.vcProviderName,''ALL'') AS [SourceMarketplace],
						CASE WHEN numSourceShipID = 91 THEN ''Fedex'' 
							 WHEN numSourceShipID = 88 THEN ''UPS'' 
							 WHEN numSourceShipID = 90 THEN ''USPS'' 
							 WHEN numSourceShipID = 101 THEN ''Amazon Standard'' 
							 WHEN numSourceShipID = 102 THEN ''ShippingMethodStandard'' 
							 WHEN numSourceShipID = 103 THEN ''Other [E-Bay]'' 
							 ELSE ''Other'' 
						END	 
						AS [SourceShipMethod]		 
				FROM [dbo].[ShippingLabelRuleMaster] SR 
				LEFT JOIN ShippingLabelRuleChild SRC ON SR.numShippingRuleID = SRC.numShippingRuleID
				LEFT JOIN dbo.WebAPI WD ON WD.WebApiId = SR.numSourceCompanyID
				WHERE SR.numDomainID = ' + CAST(@numDomainID AS VARCHAR(10)) + ' ' + @strWhere
				

	PRINT @SQL
	SET @SQL = @SQL 
	EXEC SP_EXECUTESQL @SQL					

END

