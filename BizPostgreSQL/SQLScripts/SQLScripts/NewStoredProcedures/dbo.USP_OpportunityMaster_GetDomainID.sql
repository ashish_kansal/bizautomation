SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_OpportunityMaster_GetDomainID')
DROP PROCEDURE dbo.USP_OpportunityMaster_GetDomainID
GO
CREATE PROCEDURE [dbo].[USP_OpportunityMaster_GetDomainID]
	@numOppID NUMERIC(18,0)
AS 
BEGIN
	SELECT
		numDomainID
	FROM
		OpportunityMaster
	WHERE
		numOppId=@numOppID
END
GO