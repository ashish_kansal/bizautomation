
GO
/****** Object:  StoredProcedure [dbo].[USP_GetGanttChart]    Script Date: 10/08/2010 10:25:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--EXEC USP_GetGanttChart 35,1
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetGanttChart')
DROP PROCEDURE USP_GetGanttChart
GO
CREATE PROCEDURE [dbo].[USP_GetGanttChart]
@numOppID AS NUMERIC,
@numProjectID AS NUMERIC,
@numDomainID AS NUMERIC,
@numContactId AS NUMERIC,
@dtStartDate as datetime,
@dtEndDate as datetime
AS 

--SELECT  vcMileStoneName AS GanttSeries,
--        vcStageName AS GanttTask,
--        dtStartDate AS GanttStart,
--        dtEndDate AS GanttEnd,
--        numStageDetailsId AS GanttID,
--        numParentStageID AS GanttLinkTo,
--        tinProgressPercentage AS GanttPercentComplete,
--        dbo.fn_GetContactName(numAssignTo) +' (' +CONVERT(VARCHAR(4),tinProgressPercentage) + ' %)' AS GanttOwner
--FROM    dbo.StagePercentageDetails
--WHERE   ISNULL(numProjectID,0) = @numProjectID 
--AND ISNULL(numOppID,0) = @numOppID
--AND numDomainId = @numDomainID


declare @numRights int;
set @numrights=0;

select @numrights=numRights from ProjectTeamRights where numProId=@numProjectID and numContactId=@numContactId

--SET @numrights=(case when @tintMode=0 then 1 else @numrights end)

;WITH MileStone (numParentStageID, numStageDetailsId, numStage,bitClose,dtEndDate,
dtStartDate,vcStageName,tinProgressPercentage,numAssignTo,vcAssignTo,vcMileStoneName,slp_id)
AS
(
-- Anchor member definition
    SELECT numParentStageID, numStageDetailsId ,
        0 AS numStage,bitClose,dtEndDate,dtStartDate
,vcStageName,tinProgressPercentage,isnull(numAssignTo,0) as numAssignTo,            
  isnull(dbo.fn_GetContactName(numAssignTo),'-') as vcAssignTo,vcMileStoneName,slp_id
    FROM StagePercentageDetails
    WHERE numParentStageID =0 and ISNULL(numProjectID,0) = @numProjectID 
AND ISNULL(numOppID,0) = @numOppID  AND numDomainId = @numDomainID
    UNION ALL
-- Recursive member definition
    SELECT p.numParentStageID, p.numStageDetailsId ,
        m.numStage + 1,p.bitClose,p.dtEndDate,p.dtStartDate
,p.vcStageName,p.tinProgressPercentage,isnull(p.numAssignTo,0) as numAssignTo,            
  isnull(dbo.fn_GetContactName(p.numAssignTo),'-') as vcAssignTo,p.vcMileStoneName,p.slp_id
    FROM StagePercentageDetails AS p
    INNER JOIN MileStone AS m
        ON p.numParentStageID = m.numStageDetailsId where ISNULL(numProjectID,0) = @numProjectID 
AND ISNULL(numOppID,0) = @numOppID
)
SELECT vcMileStoneName AS GanttSerie,
		vcStageName AS GanttTask, dtStartDate AS GanttStart,
        dtEndDate AS GanttEnd, numStageDetailsId AS GanttID,
        isnull(s.numStageDetailID,0) AS GanttLinkTo,
        tinProgressPercentage AS GanttPercentComplete,
        dbo.fn_GetContactName(numAssignTo) +' (' +CONVERT(VARCHAR(4),tinProgressPercentage) + ' %)' AS GanttOwner
FROM MileStone M LEFT JOIN dbo.StageDependency S ON (M.numStageDetailsId=s.numDependantOnID)   where 
--1=(case when @numRights=1 Then 1
--	  when @numRights= 0 Then case when numAssignTo=@numContactId then 1 else 0 end
--      when @numRights !=-1 Then case when numAssignTo in 
--		(select numContactId from ProjectTeam where numProjectTeamId=@numrights) then 1 else 0 end end)
--       and 
       dtStartDate between @dtStartDate and @dtEndDate 
	   and dtEndDate between @dtStartDate and @dtEndDate


