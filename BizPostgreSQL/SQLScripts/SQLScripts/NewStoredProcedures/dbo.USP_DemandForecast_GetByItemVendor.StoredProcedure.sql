SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_DemandForecast_GetByItemVendor')
DROP PROCEDURE dbo.USP_DemandForecast_GetByItemVendor
GO
CREATE PROCEDURE [dbo].[USP_DemandForecast_GetByItemVendor]
	 @numDomainID AS NUMERIC(9),
	 @numItemCode AS NUMERIC(9),
	 @numVendorID AS NUMERIC(18,0)
AS 
BEGIN
	

	SELECT 
		ISNULL((SELECT TOP 1 monCost FROM Vendor WHERE numVendorID = @numVendorID AND numItemCode = @numItemCode),0) AS monCost
		,(SELECT TOP 1 numContactId FROM AdditionalContactsInformation WHERE numDivisionId = @numVendorID ORDER BY ISNULL(bitPrimaryContact,0) DESC) AS numContactID
	SELECT 
		LD.numListItemID
		,LD.vcData
		,ISNULL(numListValue,0) AS numListValue
		,CONCAT('(',ISNULL(numListValue,'-'),' Days)') AS vcLeadDays
	FROM 
		ListDetails  LD
	OUTER APPLY
	(
		SELECT TOP 1
			*
		FROM
			VendorShipmentMethod VSM
		WHERE
			VSM.numListItemID = LD.numListItemID
			AND VSM.numVendorID = @numVendorID
		ORDER BY
			ISNULL(bitPrimary,0) DESC, ISNULL(bitPreferredMethod,0) DESC
	) VSMO
	WHERE 
		(LD.numDomainID=@numDomainID OR ISNULL(LD.constFlag,0) = 1)
		AND LD.numListID = 338
	ORDER BY
		bitPreferredMethod DESC,numListItemID ASC
END

