/****** Object:  StoredProcedure [dbo].[USP_SubmitSalseReturnPayment]    Script Date: 01/22/2009 01:41:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_SubmitSalseReturnPayment')
DROP PROCEDURE USP_SubmitSalseReturnPayment
GO
CREATE PROCEDURE [dbo].[USP_SubmitSalseReturnPayment]
@numBizDocsId	numeric(18, 0)=null,
@numPaymentMethod	TINYINT,
@numPaymentType	TINYINT,
@monAmount	DECIMAL(20,5),
@numDepoistToChartAcntId	numeric(18, 0)=null,
@numDomainId	numeric(18, 0),
@vcReference	nvarchar(200)=null,
@vcMemo	nvarchar(50)=null,
@bitAuthoritativeBizDocs	BIT = 1,
@bitIntegratedToAcnt	BIT = null,
@bitDeferredIncome	BIT=null,
@sintDeferredIncomePeriod	SMALLINT =null,
@dtDeferredIncomeStartDate	DATETIME=null,
@NoofDeferredIncomeTransaction	SMALLINT=null,
@bitSalesDeferredIncome	BIT=null,
@numContractId	numeric(18, 0)=null,
@numCreatedBy	numeric(18, 0),
@numModifiedBy	numeric(18, 0)=null,
@dtModifiedDate	DATETIME=null,
@numExpenseAccount	numeric(18, 0)=null,
@numDivisionID	numeric(18, 0)=NULL,
@dtReturnDate DATETIME = null,
@numReturnID NUMERIC(9),
@tintPaymentAction as tinyint,
@numQtyReturned NUMERIC(9),
@tintOppType as TINYINT 
	
AS 
BEGIN

declare @numCurrencyID as numeric(9),
@fltExchangeRate as float

Select @numCurrencyID=ISNULL(numCurrencyID,0) from [Returns] 
Join OpportunityMaster 
on OpportunityMaster.numOppID=[Returns].numReturnID
where numReturnID=@numReturnID


 if @numCurrencyID=0 set   @fltExchangeRate=1
  else set @fltExchangeRate=dbo.GetExchangeRate(@numDomainID,@numCurrencyID)
   
SELECT @numDivisionID = numDivisionID FROM OpportunityMaster OM join [Returns] R on OM.numOppId=R.numOppId where R.numReturnID=@numReturnID

IF @tintPaymentAction=3 --Create a sales return in DECIMAL(20,5)-out 
BEGIN
--Payment Type
--1- Purchase Order(While BizDoc) 
--2- Bill(while Entered from Create Bill)  
--3-Sales Returns(wihle comes from Sales return Queue)
INSERT INTO OpportunityBizDocsDetails (

	numBizDocsId,
	numPaymentMethod,
	tintPaymentType,
	monAmount,
	numDepoistToChartAcntId,
	numDomainId,
	vcReference,
	vcMemo,
	bitAuthoritativeBizDocs,
	bitIntegratedToAcnt,
	bitDeferredIncome,
	sintDeferredIncomePeriod,
	dtDeferredIncomeStartDate,
	NoofDeferredIncomeTransaction,
	bitSalesDeferredIncome,
	numContractId,
	numCreatedBy,
	dtCreationDate,
	numModifiedBy,
	dtModifiedDate,
	numExpenseAccount,
	numDivisionID,
	numReturnID,
	numCurrencyID,
	fltExchangeRate
) VALUES ( 
	
	/* numBizDocsId - numeric(18, 0) */ @numBizDocsId,
	/* numPaymentMethod - tinyint */ @numPaymentMethod,
	/* numPaymentType - tinyint */ @numPaymentType,
	/* monAmount - DECIMAL(20,5) */ @monAmount,
	/* numDepoistToChartAcntId - numeric(18, 0) */ @numDepoistToChartAcntId,
	/* numDomainId - numeric(18, 0) */ @numDomainId,
	/* vcReference - nvarchar(200) */ @vcReference,
	/* vcMemo - nvarchar(50) */ @vcMemo,
	/* bitAuthoritativeBizDocs - bit */ @bitAuthoritativeBizDocs,
	/* bitIntegratedToAcnt - bit */ @bitIntegratedToAcnt,
	/* bitDeferredIncome - bit */ @bitDeferredIncome,
	/* sintDeferredIncomePeriod - smallint */ @sintDeferredIncomePeriod,
	/* dtDeferredIncomeStartDate - datetime */ @dtDeferredIncomeStartDate,
	/* NoofDeferredIncomeTransaction - smallint */ @NoofDeferredIncomeTransaction,
	/* bitSalesDeferredIncome - bit */ @bitSalesDeferredIncome,
	/* numContractId - numeric(18, 0) */ @numContractId,
	/* numCreatedBy - numeric(18, 0) */ @numCreatedBy,
	/* dtCreationDate - datetime */ GETDATE(),
	/* numModifiedBy - numeric(18, 0) */ @numModifiedBy,
	/* dtModifiedDate - datetime */ @dtModifiedDate,
	/* numExpenseAccount - numeric(18, 0) */ @numExpenseAccount,
	/* numDivisionID - numeric(18, 0) */ @numDivisionID,
	@numReturnID,
	@numCurrencyID,
	@fltExchangeRate ) 
END

--DECLARE @numReturnSatus NUMERIC(9)
--
--SELECT @numReturnSatus = [numListItemID] FROM [ListDetails] WHERE UPPER([vcData]) = UPPER('Under Payment');
	
UPDATE [Returns] SET 
dtReturnDate = @dtReturnDate , 
--[numReturnStatus] = @numReturnSatus,
[numQtyReturned] =numQtyReturned + @numQtyReturned,
[numModifiedBy] = @numCreatedBy,
[dtModifiedDate] = GETDATE()
WHERE [numReturnID] = @numReturnID;

END