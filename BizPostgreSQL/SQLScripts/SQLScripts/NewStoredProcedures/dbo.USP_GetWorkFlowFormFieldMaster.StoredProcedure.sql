SET ANSI_NULLS ON
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetWorkFlowFormFieldMaster')
DROP PROCEDURE USP_GetWorkFlowFormFieldMaster
GO
CREATE PROCEDURE [dbo].[USP_GetWorkFlowFormFieldMaster]     
    @numDomainID numeric(18, 0),
	@numFormID numeric(18, 0)
as                 
BEGIN
CREATE TABLE #tempField(numFieldID NUMERIC,
vcFieldName NVARCHAR(50),vcDbColumnName NVARCHAR(50),vcOrigDbColumnName NVARCHAR(50),vcFieldDataType CHAR(1),
vcAssociatedControlType NVARCHAR(50),vcListItemType VARCHAR(3),numListID NUMERIC,bitCustom BIT,vcLookBackTableName NVARCHAR(50),bitAllowFiltering BIT,bitAllowEdit BIT,vcGroup varchar(100),intWFCompare int)

--Regular Fields
INSERT INTO #tempField
	SELECT numFieldID,vcFieldName,
		   vcDbColumnName,vcOrigDbColumnName,vcFieldDataType,vcAssociatedControlType,
		   vcListItemType,numListID,CAST(0 AS BIT) AS bitCustom,
		   (CASE WHEN vcAssociatedControlType = 'DateField' THEN CONCAT(vcLookBackTableName, '_TempDateFields') 
		   ELSE vcLookBackTableName
		   END) AS vcLookBackTableName,
		   bitAllowFiltering,bitAllowEdit,ISNULL(vcGroup,'Custom Fields')as vcGroup,ISNULL(intWFCompare,0) as intWFCompare
		FROM View_DynamicDefaultColumns
		where numFormId=@numFormId and numDomainID=@numDomainID 
		AND ISNULL(bitWorkFlowField,0)=1 AND ISNULL(bitCustom,0)=0 AND 1 = (CASE 
																				WHEN @numFormId = 68
																				THEN (CASE WHEN vcGroup='Contact Fields' THEN 0 ELSE 1 END)
																				WHEN @numFormId = 70
																				THEN (CASE WHEN (vcGroup='BizDoc Fields' AND vcDbColumnName <> 'monAmountPaid') OR vcGroup='Contact Fields' OR vcGroup='Milestone/Stage Fields' THEN 0 ELSE 1 END)
																				WHEN @numFormId = 94
																				THEN (CASE WHEN vcGroup='Project Fields' THEN 0 ELSE 1 END)
																				ELSE 1
																			END)


DECLARE @vcLocationID VARCHAR(100);SET @vcLocationID='0'
Select @vcLocationID =isnull(vcLocationID,'0') from dynamicFormMaster where numFormID=@numFormId

IF @numFormID = 49
BEGIN
	IF LEN(ISNULL(@vcLocationID,'')) > 0
	BEGIN
		Select @vcLocationID = @vcLocationID + ',' + isnull(vcLocationID,'0') from dynamicFormMaster where numFormID=70
	END
	ELSE
	BEGIN
		Select @vcLocationID =isnull(vcLocationID,'0') from dynamicFormMaster where numFormID=70
	END
END
ELSE IF @numFormID = 94
BEGIN
	-- DO NOT SHOW CUSTOM FIELDS
	SET @vcLocationID = ''
END

IF @numFormID IN (69,70,49,72,73,124)
BEGIN
	IF LEN(ISNULL(@vcLocationID ,'')) > 0
	BEGIN
		SET @vcLocationID = CONCAT(@vcLocationID,',1,12,13,14')
	END
	ELSE
	BEGIN
		SET @vcLocationID = '1,12,13,14'
	END
END

--Custom Fields			
INSERT INTO #tempField
SELECT ISNULL(CFM.Fld_Id,0) as numFieldID,CFM.Fld_label as vcFieldName,
	   'Cust'+Convert(varchar(10),Fld_Id) as vcDbColumnName,'Cust'+Convert(varchar(10),Fld_Id) AS vcOrigDbColumnName,CASE CFM.Fld_type WHEN 'SelectBox' THEN 'V' WHEN 'DateField' THEN 'D' ELSE 'V' END AS vcFieldDataType,
	   CFM.Fld_type AS vcAssociatedControlType,CASE WHEN CFM.Fld_type='SelectBox' THEN 'LI' ELSE '' END AS vcListItemType,isnull(CFM.numListID,0) as numListID,
	   CAST(1 AS BIT) AS bitCustom,L.vcCustomLookBackTableName AS vcLookBackTableName,0 AS bitAllowFiltering,1 AS bitAllowEdit,(CASE WHEN Grp_id IN (1,12,13,14) THEN 'Organization Custom Fields' WHEN Grp_id=4 THEN 'Contact Custom Fields' WHEN Grp_id IN (2,6) THEN 'Opportunity & Order Custom Fields' WHEN Grp_id = 3 THEN 'Case Custom Fields' WHEN Grp_id=11 THEN 'Project Custom Fields' ELSE 'Custom Fields' END)  as vcGroup,0 as intWFCompare
FROM CFW_Fld_Master CFM LEFT JOIN CFW_Validation V ON V.numFieldID = CFM.Fld_id
					JOIN CFW_Loc_Master L on CFM.GRP_ID=L.Loc_Id
            WHERE   CFM.numDomainID = @numDomainId
					AND GRP_ID IN(	SELECT ID FROM dbo.SplitIDs(@vcLocationID,','))
 

SELECT *,/*vcOrigDbColumnName + '~' +*/ CAST(numFieldID AS VARCHAR(18)) + '_' + CAST(CASE WHEN bitCustom=1 THEN 'True' ELSE 'False' END AS VARCHAR(10)) AS ID  FROM #tempField
ORDER BY vcGroup,vcFieldName,bitCustom

DROP TABLE #tempField
END
GO