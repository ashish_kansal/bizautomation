/****** Object:  StoredProcedure [dbo].[Usp_UpdateCaseStatus]    Script Date: 7 Jul,2013 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--  By Manish Anjara
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='Usp_UpdateCaseStatus')
DROP PROCEDURE Usp_UpdateCaseStatus
GO
CREATE PROCEDURE [dbo].[Usp_UpdateCaseStatus]  
AS 
BEGIN
	
	DECLARE @intCnt AS INT
	DECLARE @intRows AS INT
	DECLARE @dtResolveDate AS DATETIME
	DECLARE @dtTodayDate AS DATETIME
	DECLARE @numCaseID AS NUMERIC(18,0)
	DECLARE @numDomainID AS NUMERIC(18,0)
	
	SET @dtTodayDate = dbo.GetUTCDateWithoutTime()
--	1008  > Open
--	134   > Pending
--	135   > Overdue
--	136   > Closed
--	22931 > test

	UPDATE dbo.Cases SET numStatus = 135  WHERE numStatus = 134 AND @dtTodayDate > intTargetResolveDate	
END