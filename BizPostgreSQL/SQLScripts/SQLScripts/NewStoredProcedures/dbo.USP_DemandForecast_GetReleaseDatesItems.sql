SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_DemandForecast_GetReleaseDatesItems')
DROP PROCEDURE USP_DemandForecast_GetReleaseDatesItems
GO
CREATE PROCEDURE [dbo].[USP_DemandForecast_GetReleaseDatesItems]  
(
	@numDomainID NUMERIC(18,0)
	,@numDFID NUMERIC(18,0)
)
AS  
BEGIN  
	DECLARE @numDays AS INT

	SELECT
		@numDays = ISNULL(DemandForecastDays.numDays,0)
	FROM
		DemandForecast
	INNER JOIN
		DemandForecastDays
	ON
		DemandForecast.numDFDaysID = DemandForecastDays.numDFDaysID
	WHERE
		numDFID = @numDFID


	DECLARE @dtTo AS DATE = CAST(DATEADD(D,@numDays,GETDATE()) AS DATE)


	DECLARE @bitWarehouseFilter BIT = 0
	DECLARE @bitItemClassificationFilter BIT = 0
	DECLARE @bitItemGroupFilter BIT = 0

	IF (SELECT COUNT(*) FROM DemandForecastWarehouse WHERE numDFID = @numDFID) > 0
	BEGIN
		SET @bitWarehouseFilter = 1
	END

	IF (SELECT COUNT(*) FROM DemandForecastItemClassification WHERE numDFID = @numDFID) > 0
	BEGIN
		SET @bitItemClassificationFilter = 1
	END

	IF (SELECT COUNT(*) FROM DemandForecastItemGroup WHERE numDFID = @numDFID) > 0
	BEGIN
		SET @bitItemGroupFilter = 1
	END

	DECLARE @TEMP TABLE
	(
		numOppID NUMERIC(18,0)
		,numOppItemID NUMERIC(18,0)
		,numItemCode NUMERIC(18,0)
		,numWarehouseItemID NUMERIC(18,0)
		,bitKit BIT
		,bitAssembly BIT
		,vcItemName VARCHAR(300)
		,vcCustomer VARCHAR(300)
		,vcOppName VARCHAR(300)
		,vcUOM VARCHAR(50)
		,vcOrderReleaseDate VARCHAR(15)
		,numOrderReleaseQty FLOAT
		,vcItemReleaseDates VARCHAR(1000)
		,fltUOMConversionFactor FLOAT
	)


	-- GET ITEMS ORDER RELEASE DATES
	INSERT INTO 
		@TEMP
	SELECT
		OM.numOppID
		,OI.numoppitemtCode
		,I.numItemCode
		,(CASE
			WHEN ISNULL(I.numItemGroup,0) > 0 AND LEN(dbo.fn_GetAttributes(WI.numWareHouseItemID,0)) > 0
			THEN
				(CASE 
					WHEN EXISTS (SELECT numWareHouseItemID FROM WareHouseItems WIInner WHERE WIInner.numDomainID=@numDomainID AND  WIInner.numItemID=I.numItemCode AND WIInner.numWareHouseID=WI.numWareHouseID AND WIInner.numWareHouseID=-1 AND  dbo.fn_getattributes(WIInner.numWareHouseItemID,0) = dbo.fn_getattributes(WI.numWareHouseItemID,0)) 
					THEN
						(SELECT TOP 1 numWareHouseItemID FROM WareHouseItems WIInner WHERE WIInner.numDomainID=@numDomainID AND  WIInner.numItemID=I.numItemCode AND WIInner.numWareHouseID=WI.numWareHouseID AND WIInner.numWareHouseID=-1 AND  dbo.fn_getattributes(WIInner.numWareHouseItemID,0) = dbo.fn_getattributes(WI.numWareHouseItemID,0))
					ELSE
						WI.numWareHouseItemID
				END)
			ELSE
				(SELECT TOP 1 numWareHouseItemID FROM WareHouseItems WHERE numDomainID=@numDomainID AND numItemID=I.numItemCode AND numWareHouseID=WI.numWareHouseID AND numWLocationID = -1)
		END) numWarehouseItemID
		,ISNULL(I.bitKitParent,0)
		,ISNULL(I.bitAssembly,0)
		,ISNULL(I.vcItemName,'')
		,ISNULL(CI.vcCompanyName,'')
		,CONCAT(ISNULL(OM.vcPOppName,''),' (',(CASE WHEN OM.tintOppStatus = 1 THEN 'Order' ELSE 'Opportunity' END),')')
		,ISNULL((SELECT vcUnitName FROM UOM WHERE numUOMId = OI.numUOMId),'-')
		,dbo.FormatedDateFromDate(OM.dtReleaseDate,@numDomainID) vcOrderReleaseDate
		,(OI.numUnitHour - ISNULL(TEMPItemRelease.numItemReleaseQty,0))
		,ISNULL(TEMPRelease.vcReleaseDates,'')
		--,ISNULL(TEMPRelease.numItemReleaseQty,0)
		--,ISNULL((TEMPRelease.numItemReleaseQty * dbo.fn_UOMConversion(ISNULL(OI.numUOMId, 0), I.numItemCode, I.numDomainId, ISNULL(I.numBaseUnit, 0))),0)
		,dbo.fn_UOMConversion(ISNULL(OI.numUOMId, 0), I.numItemCode, I.numDomainId, ISNULL(I.numBaseUnit, 0))
	FROM 
		OpportunityMaster OM
	INNER JOIN
		DivisionMaster DM
	ON
		DM.numDivisionID = OM.numDivisionId
	INNER JOIN
		CompanyInfo CI
	ON
		DM.numCompanyID = CI.numCompanyId
	INNER JOIN 
		OpportunityItems OI
	ON
		OM.numOppID = OI.numOppId
	INNER JOIN
		Item I
	ON
		OI.numItemCode = I.numItemCode
	INNER JOIN
		WareHouseItems WI
	ON
		OI.numWarehouseItmsID = WI.numWareHouseItemID
	OUTER APPLY
	(
		SELECT
			SUM(numQty) AS numItemReleaseQty
		FROM
			OpportunityItemsReleaseDates OIRD
		WHERE
			OIRD.numOppID = OM.numOppId
			AND OIRD.numOppItemID = OI.numoppitemtCode
			AND ((OIRD.dtReleaseDate <= @dtTo AND ISNULL(OIRD.tintStatus,0) = 1) OR ISNULL(OIRD.tintStatus,0) = 2)
	) TEMPItemRelease
	OUTER APPLY
	(
		SELECT
			SUM(numQty) AS numItemReleaseQty
			,STUFF((SELECT DISTINCT 
						',' + CONCAT(OIRDInner.numID,'#',dbo.FormatedDateFromDate(OIRDInner.dtReleaseDate,@numDOmainID),' (', numQty ,')')
					FROM 
						OpportunityItemsReleaseDates OIRDInner
					WHERE 
						OIRDInner.numOppID = OM.numOppId
						AND OIRDInner.numOppItemID = OI.numoppitemtCode
						AND (OIRDInner.dtReleaseDate <= @dtTo OR ISNULL(OIRDInner.tintStatus,0) = 1)
					FOR XML PATH ('')), 1, 1, '') AS vcReleaseDates
		FROM
			OpportunityItemsReleaseDates OIRD
		WHERE
			OIRD.numOppID = OM.numOppId
			AND OIRD.numOppItemID = OI.numoppitemtCode
			AND (OIRD.dtReleaseDate <= @dtTo OR ISNULL(OIRD.tintStatus,0) = 1)
	) TEMPRelease
	WHERE
		OM.numDomainId = @numDomainID
		AND OM.tintOppType = 1
		AND (OM.dtReleaseDate IS NOT NULL AND CAST(OM.dtReleaseDate AS DATE) <= @dtTo)
		AND ISNULL(numReleaseStatus,1) = 1
		AND
		(
			(SELECT COUNT(*) FROM DemandForecastWarehouse WHERE numDFID = @numDFID) = 0 OR
			WI.numWareHouseID IN (SELECT numWareHouseID FROM DemandForecastWarehouse WHERE numDFID = @numDFID)
		)
		AND
		(
			(SELECT COUNT(*) FROM DemandForecastItemClassification WHERE numDFID = @numDFID) = 0 OR
			I.numItemClassification IN (SELECT numItemClassificationID FROM DemandForecastItemClassification WHERE numDFID = @numDFID)
		)
		AND
		(
			(SELECT COUNT(*) FROM DemandForecastItemGroup WHERE numDFID = @numDFID) = 0 OR
			I.numItemGroup IN (SELECT numItemGroupID FROM DemandForecastItemGroup WHERE numDFID = @numDFID)
		)


	-- GET ITEM RELEASE DATES
	INSERT INTO 
		@TEMP
	SELECT
		TEMPItemRelease.numOppID
		,TEMPItemRelease.numOppItemID
		,I.numItemCode
		,(CASE
			WHEN ISNULL(I.numItemGroup,0) > 0 AND LEN(dbo.fn_GetAttributes(WI.numWareHouseItemID,0)) > 0 
			THEN
				(CASE 
					WHEN EXISTS (SELECT numWareHouseItemID FROM WareHouseItems WIInner WHERE WIInner.numDomainID=@numDomainID AND  WIInner.numItemID=I.numItemCode AND WIInner.numWareHouseID=WI.numWareHouseID AND WIInner.numWareHouseID=-1 AND  dbo.fn_getattributes(WIInner.numWareHouseItemID,0) = dbo.fn_getattributes(WI.numWareHouseItemID,0)) 
					THEN
						(SELECT TOP 1 numWareHouseItemID FROM WareHouseItems WIInner WHERE WIInner.numWareHouseID=WI.numWareHouseID AND WIInner.numWareHouseID=-1 AND  dbo.fn_getattributes(WIInner.numWareHouseItemID,0) = dbo.fn_getattributes(WI.numWareHouseItemID,0))
					ELSE
						WI.numWareHouseItemID
				END)
			ELSE
				(SELECT TOP 1 numWareHouseItemID FROM WareHouseItems WHERE numDomainID=@numDomainID AND numItemID=I.numItemCode AND numWareHouseID=WI.numWareHouseID AND numWLocationID = -1)
		END) numWarehouseItemID
		,ISNULL(I.bitKitParent,0)
		,ISNULL(I.bitAssembly,0)
		,ISNULL(I.vcItemName,'')
		,ISNULL(CI.vcCompanyName,'')
		,CONCAT(ISNULL(OM.vcPOppName,''),' (',(CASE WHEN OM.tintOppStatus = 1 THEN 'Order' ELSE 'Opportunity' END),')')
		,ISNULL((SELECT vcUnitName FROM UOM WHERE numUOMId = OI.numUOMId),'-')
		,''
		,0
		,ISNULL(TEMPItemRelease.vcReleaseDates,'')
		,dbo.fn_UOMConversion(ISNULL(OI.numUOMId, 0), I.numItemCode, I.numDomainId, ISNULL(I.numBaseUnit, 0))
	FROM
	(
		SELECT
			OIRD.numOppID
			,OIRD.numOppItemID
			,SUM(numQty) AS numItemReleaseQty
			,STUFF((SELECT DISTINCT 
						',' + CONCAT(OIRDInner.numID,'#',dbo.FormatedDateFromDate(OIRDInner.dtReleaseDate,@numDOmainID),' (', numQty ,')')
					FROM 
						OpportunityItemsReleaseDates OIRDInner
					INNER JOIN
						OpportunityMaster OMInner
					ON
						OIRDInner.numOppID = OMInner.numOppID
					WHERE 
						OIRDInner.numOppID = OIRD.numOppId
						AND OIRDInner.numOppItemID = OIRD.numOppItemID
						AND OIRDInner.dtReleaseDate <=  @dtTo
						AND ISNULL(OIRDInner.tintStatus,0) = 1
						AND (OMInner.dtReleaseDate IS NULL OR OMInner.dtReleaseDate > @dtTo)
					FOR XML PATH ('')), 1, 1, '') AS vcReleaseDates
		FROM
			OpportunityItemsReleaseDates OIRD
		INNER JOIN
			OpportunityMaster OM
		ON
			OIRD.numOppID = OM.numOppID
		WHERE
			OIRD.dtReleaseDate <=  @dtTo
			AND ISNULL(OIRD.tintStatus,0) = 1
			AND (OM.dtReleaseDate IS NULL OR OM.dtReleaseDate > @dtTo)
		GROUP BY
			OIRD.numOppID
			,OIRD.numOppItemID
	) TEMPItemRelease
	INNER JOIN
		OpportunityItems OI
	ON
		TEMPItemRelease.numOppID = OI.numOppID
		AND TEMPItemRelease.numOppItemID = OI.numOppitemtcode
	INNER JOIN
		OpportunityMaster OM
	ON
		TEMPItemRelease.numOppID = OM.numOppID
	INNER JOIN
		DivisionMaster DM
	ON
		DM.numDivisionID = OM.numDivisionId
	INNER JOIN
		CompanyInfo CI
	ON
		DM.numCompanyID = CI.numCompanyId
	INNER JOIN
		Item I
	ON
		OI.numItemCode = I.numItemCode
	INNER JOIN
		WareHouseItems WI
	ON
		OI.numWarehouseItmsID = WI.numWareHouseItemID
	WHERE
		(
			(SELECT COUNT(*) FROM DemandForecastWarehouse WHERE numDFID = @numDFID) = 0 OR
			WI.numWareHouseID IN (SELECT numWareHouseID FROM DemandForecastWarehouse WHERE numDFID = @numDFID)
		)
		AND
		(
			(SELECT COUNT(*) FROM DemandForecastItemClassification WHERE numDFID = @numDFID) = 0 OR
			I.numItemClassification IN (SELECT numItemClassificationID FROM DemandForecastItemClassification WHERE numDFID = @numDFID)
		)
		AND
		(
			(SELECT COUNT(*) FROM DemandForecastItemGroup WHERE numDFID = @numDFID) = 0 OR
			I.numItemGroup IN (SELECT numItemGroupID FROM DemandForecastItemGroup WHERE numDFID = @numDFID)
		)

	SELECT * FROM @TEMP WHERE (ISNULL(numOrderReleaseQty,0) > 0 OR LEN(ISNULL(vcItemReleaseDates,'')) > 0) ORDER BY numItemCode, numOppID, numOppItemID

END