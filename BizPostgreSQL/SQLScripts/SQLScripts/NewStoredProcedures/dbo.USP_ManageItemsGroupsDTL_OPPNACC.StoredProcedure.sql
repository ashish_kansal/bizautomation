
GO
/****** Object:  StoredProcedure [dbo].[USP_ManageItemsGroupsDTL_OPPNACC]    Script Date: 11/03/2010 11:51:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--create by anoop jayaraj  
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ManageItemsGroupsDTL_OPPNACC')
DROP PROCEDURE USP_ManageItemsGroupsDTL_OPPNACC
GO
CREATE PROCEDURE [dbo].[USP_ManageItemsGroupsDTL_OPPNACC]  
@numItemGroupID as numeric(9)=0,  
@numOppAccAttrID as numeric(9)=0,  
@numQtyItemsReq as numeric(9)=0, 
@numDefaultSelect as numeric(9)=0, 
@byteMode as TINYINT,
@numDomainID as numeric(9)=0,
@vcName as varchar(100),
@bitCombineAssemblies AS BIT,
@numMapToDropdownFld AS NUMERIC(18)=0,
@numProfileItem AS NUMERIC(18)
as  
if @byteMode=0  
begin   
 
if @numItemGroupID=0       
 BEGIN     
   insert into ItemGroups 
   (vcItemGroup,numDomainID,bitCombineAssemblies,numMapToDropdownFld,numProfileItem) 
   values 
   (@vcName,@numDomainID,@bitCombineAssemblies,@numMapToDropdownFld,@numProfileItem)      
    set @numItemGroupID=@@identity    
 END
  
delete from ItemGroupsDTL where numItemGroupID=@numItemGroupID and numOppAccAttrID=@numOppAccAttrID  
insert into ItemGroupsDTL (numItemGroupID,numOppAccAttrID,numQtyItemsReq,numDefaultSelect,tintType)  
values(@numItemGroupID,@numOppAccAttrID,@numQtyItemsReq,@numDefaultSelect,1)  


end  
  
if @byteMode=1  
begin  
delete from ItemGroupsDTL where numItemGroupID=@numItemGroupID AND numOppAccAttrID=@numOppAccAttrID AND tintType=1

end

select @numItemGroupID
