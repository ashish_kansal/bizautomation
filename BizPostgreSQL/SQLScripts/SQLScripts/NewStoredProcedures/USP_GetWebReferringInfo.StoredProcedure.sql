set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go 
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetWebReferringInfo')
DROP PROCEDURE USP_GetWebReferringInfo
GO
CREATE PROCEDURE [dbo].[USP_GetWebReferringInfo] 
	@numDomainID AS NUMERIC(9) = 0
AS  
BEGIN
	SELECT *
	FROM TrackingVisitorsHDR 
	WHERE numDomainID = @numDomainID AND ISNULL(numListItemID,0) > 0
END