GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_ManageAPIImportOrderRequest' ) 
    DROP PROCEDURE USP_ManageAPIImportOrderRequest
GO



--created by Joseph
CREATE PROC [dbo].[USP_ManageAPIImportOrderRequest]
		  @numImportApiOrderReqId AS NUMERIC(9),
          @numDomainID AS NUMERIC(9),
		  @numWebApiId AS NUMERIC(9),         
		  @vcWebApiOrderId AS VARCHAR(25),
          @bitIsActive AS BIT,
		  @tintRequestType as TINYINT,
		  @dtFromDate DATETIME = null,
		  @dtToDate DATETIME = null,
		  @numOppId AS NUMERIC(9)



AS
  BEGIN
  DECLARE @Check as integer
  IF @numImportApiOrderReqId =0
  BEGIN
	IF @tintRequestType = 1
		BEGIN
			SELECT @Check = COUNT(*) FROM   [ImportApiOrder] WHERE  [numDomainId] = @numDomainID AND [numWebApiId] = @numWebApiId AND [vcApiOrderId] = @vcWebApiOrderId AND bitIsActive = 1
		END
--	ELSE IF @tintRequestType = 1
--		BEGIN
--			--SELECT @Check = COUNT(*) FROM   [ImportApiOrder] WHERE  [numDomainId] = @numDomainID AND [numWebApiId] = @numWebApiId AND bitIsActive = 1
--		END
  	IF @Check =0    
	 BEGIN
  	 INSERT INTO [ImportApiOrder]
                   ([numWebApiId],
                    [numDomainId],
                    [vcApiOrderId],
                    [bitIsActive],
				    tintRequestType,
					dtFromDate,
					dtToDate,
                    [dtCreatedDate],
					numOppId)
        VALUES     (@numWebApiId,
                    @numDomainID,
                    @vcWebApiOrderId,
                    @bitIsActive,
				    @tintRequestType,
					@dtFromDate,
					@dtToDate,
                    GETDATE(),
					@numOppId)
      
      END  
        END
  ELSE 
  IF @numImportApiOrderReqId <> 0
  BEGIN
  		UPDATE [ImportApiOrder]
        SET    [bitIsActive] = @bitIsActive,[dtModifiedDate] = GETDATE()
        WHERE  [numDomainId] = @numDomainID AND [vcApiOrderId] = @vcWebApiOrderId
				AND [numWebApiId] = @numWebApiId
  END
  END








