GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ManageShippingRule')
DROP PROCEDURE USP_ManageShippingRule
GO
CREATE PROCEDURE USP_ManageShippingRule
	@numRuleID NUMERIC(9),
	@vcRuleName VARCHAR(50),
	@vcDescription VARCHAR(1000),
	@tintBasedOn TINYINT,
	@tinShippingMethod TINYINT,
	@tintType TINYINT,
	@numDomainID NUMERIC(9),
	@tintTaxMode TINYINT,
	@tintFixedShippingQuotesMode TINYINT,
	@bitFreeshipping BIT,
	@FreeShippingAmt INT = NULL,
	@numRelationship NUMERIC(9),
	@numProfile NUMERIC(9),
	@bitItemClassification BIT,
	@numSiteId NUMERIC(18,0),
	@strWarehouseIds VARCHAR(1000)
AS 
BEGIN
	IF(SELECT COUNT(*) FROM dbo.ShippingRules WHERE vcRuleName = @vcRuleName AND numDomainId=@numDomainId AND numRuleID<>@numRuleID)>0
	BEGIN
 		RAISERROR ( 'DUPLICATE',16, 1 )
 		RETURN ;
	END

    IF ISNULL(@numRuleID,0) = 0
    BEGIN  
        INSERT INTO dbo.ShippingRules 
		(	
			vcRuleName
            ,vcDescription
            ,tintBasedOn
            ,tinShippingMethod
            ,tintIncludeType
            ,numDomainID
            ,tintTaxMode
            ,tintFixedShippingQuotesMode 
			,bitFreeShipping
			,FreeShippingOrderAmt
			,numRelationship
			,numProfile
			,numWareHouseID
			,bitItemClassification
			,numSiteId
		)
        VALUES 
		(
            @vcRuleName
            ,@vcDescription
            ,@tintBasedOn
            ,@tinShippingMethod
            ,@tintType
            ,@numDomainID 
            ,@tintTaxMode 
            ,@tintFixedShippingQuotesMode
			,@bitFreeshipping
			,CASE WHEN @bitFreeshipping = 0 THEN NULL ELSE @FreeShippingAmt END
			,@numRelationship
			,@numProfile
			,@strWarehouseIds
			,@bitItemClassification
			,@numSiteId
		)
		   
        SELECT numRuleID = SCOPE_IDENTITY()
    END  
    ELSE 
    BEGIN  
		IF (SELECT 
				COUNT(*) 
			FROM 
				dbo.ShippingRules SR
			WHERE 
				numDomainId=@numDomainId 
				AND numRuleID <> ISNULL(@numRuleID,0) 
				AND ISNULL(numRelationship,0) = ISNULL(@numRelationship,0)
				AND ISNULL(numProfile,0) = ISNULL(@numProfile,0)
				AND EXISTS (SELECT 
								SRSL1.numRuleID
							FROM 
								ShippingRuleStateList SRSL1 
							INNER JOIN
								ShippingRuleStateList SRSL2
							ON
								ISNULL(SRSL1.numStateID,0) = ISNULL(SRSL2.numStateID,0)
								AND ISNULL(SRSL1.vcZipPostal,'') = ISNULL(SRSL2.vcZipPostal,'')
								AND ISNULL(SRSL1.numCountryID,0) = ISNULL(SRSL2.numCountryID,0)
							WHERE 
								SRSL1.numDomainID=@numDomainID 
								AND SRSL1.numRuleID = SR.numRuleID
								AND SRSL2.numRuleID = @numRuleID)
			) > 0
		BEGIN
 			RAISERROR ( 'DUPLICATE RELATIONSHIP, PROFILE AND STATE-ZIP',16, 1 )
 			RETURN ;
		END

		UPDATE 
			dbo.ShippingRules
		SET 
			vcRuleName = @vcRuleName
			,vcDescription = @vcDescription
			,tintBasedOn = @tintBasedOn
			,tinShippingMethod = @tinShippingMethod
			,tintIncludeType = @tintType 
			,tintTaxMode = @tintTaxMode
            ,tintFixedShippingQuotesMode = @tintFixedShippingQuotesMode
			,bitFreeShipping = @bitFreeshipping
			,FreeShippingOrderAmt = (CASE WHEN @bitFreeshipping = 0 THEN NULL Else @FreeShippingAmt End)
			,numRelationship = @numRelationship
			,numProfile = @numProfile
			,numWareHouseID = @strWarehouseIds
			,bitItemClassification=@bitItemClassification
			,numSiteId=@numSiteId
		WHERE 
			numRuleID = @numRuleID
			AND numDomainID = @numDomainID			          

        SELECT  @numRuleID
    END
END