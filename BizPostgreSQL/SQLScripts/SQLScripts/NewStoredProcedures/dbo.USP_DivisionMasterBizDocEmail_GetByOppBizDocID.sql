GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_DivisionMasterBizDocEmail_GetByOppBizDocID')
DROP PROCEDURE dbo.USP_DivisionMasterBizDocEmail_GetByOppBizDocID
GO
CREATE PROCEDURE [dbo].[USP_DivisionMasterBizDocEmail_GetByOppBizDocID]
(
	@numDomainID NUMERIC(18,0)
	,@numOppBizDocID NUMERIC(18,0)
)
AS 
BEGIN
	SELECT 
		(CASE WHEN LEN(ISNULL(DMBE.vcTo,'')) > 0 THEN dbo.fn_GetEmailStringAsJson(DMBE.vcTo) ELSE '' END) vcTo
		,(CASE WHEN LEN(ISNULL(DMBE.vcCC,'')) > 0 THEN dbo.fn_GetEmailStringAsJson(DMBE.vcCC) ELSE '' END) vcCC
		,(CASE WHEN LEN(ISNULL(DMBE.vcBCC,'')) > 0 THEN dbo.fn_GetEmailStringAsJson(DMBE.vcBCC) ELSE '' END) vcBCC
	FROM	
		OpportunityBizDocs OBD
	INNER JOIN
		OpportunityMaster OM
	ON
		OBD.numOppId = OM.numOppId
	INNER JOIN
		DivisionMasterBizDocEmail DMBE
	ON
		OM.numDivisionId = DMBE.numDivisionID
		AND OBD.numBizDocId = DMBE.numBizDocID
	INNER JOIN
		Domain D
	ON
		DMBE.numDomainID = D.numDomainId
	WHERE
		OBD.numOppBizDocsId = @numOppBizDocID
		AND ISNULL(D.bitUsePreviousEmailBizDoc,0) = 1
END
GO

