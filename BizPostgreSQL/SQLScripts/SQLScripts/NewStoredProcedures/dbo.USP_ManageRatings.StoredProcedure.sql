SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ManageRatings')
DROP PROCEDURE USP_ManageRatings
GO
CREATE PROCEDURE [dbo].[USP_ManageRatings]   

@numRatingId as numeric(9)=0,    
@tintTypeId as INT = 0,       
@vcReferenceId	 as varchar(100)='', 
@intRatingCount AS INT , 
@vcIpAddress AS VARCHAR(100) ,
@numContactId AS NUMERIC(18,0) ,
@numSiteId AS NUMERIC(18,0) ,
@numDomainId AS NUMERIC(18,0) ,
@vcCreatedDate AS VARCHAR(100)

AS

BEGIN

	IF @numRatingId = 0      
      BEGIN   
        
     IF NOT EXISTS(SELECT * FROM  Ratings  WHERE numContactId = @numContactId AND vcReferenceId = @vcReferenceId AND numSiteId = @numSiteId AND numDomainId = @numDomainId) 
			BEGIN
			      INSERT INTO Ratings(
					tintTypeId ,
					vcReferenceId,
					intRatingCount,
					numContactId,
					vcIpAddress,
					numSiteId ,
					numDomainId ,
					dtCreatedDate 
        
			 )      
			values(
					@tintTypeId ,
					@vcReferenceId ,
					@intRatingCount ,
					@numContactId ,
					@vcIpAddress,
					@numSiteId ,
					@numDomainId ,
					CONVERT(DATETIME , @vcCreatedDate)
					)	
			END
			ELSE
			BEGIN      
				UPDATE Ratings set 
					intRatingCount = @intRatingCount
				    where  numContactId = @numContactId AND vcReferenceId = @vcReferenceId AND numSiteId = @numSiteId AND numDomainId = @numDomainId 
			END
			
		END      

		ELSE      
			BEGIN      
				UPDATE Ratings set 
					intRatingCount = @intRatingCount
				where numRatingId = @numRatingId  
			END
END


--SELECT * FROM Ratings
