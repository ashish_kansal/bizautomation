SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_checkcodeforpromotion')
DROP PROCEDURE usp_checkcodeforpromotion
GO
CREATE PROCEDURE [dbo].[USP_CheckCodeForPromotion]
	 @numDomainID AS NUMERIC(18,0)
	,@txtCouponCode AS VARCHAR(100)
	,@numProdId AS NUMERIC(18,0) = 0
	,@numDivisionId AS NUMERIC(18,0)
AS
BEGIN	

	DECLARE @numCouponCode NUMERIC(18,0)
	DECLARE @intCodeUsageLimit NUMERIC(18,0)
	DECLARE @intCodeUsed INT
	DECLARE @ERRORMESSAGE AS VARCHAR(50)
	DECLARE @intUsedCountForDiv INT

	SET @numCouponCode = (SELECT COUNT(DC.vcDiscountCode) FROM PromotionOffer PO
							INNER JOIN DiscountCodes DC
							ON PO.numProId = DC.numPromotionID
							WHERE numProId = @numProdId AND numDomainId = @numDomainID AND bitRequireCouponCode = 1 
							AND DC.vcDiscountCode = @txtCouponCode)

	IF @numCouponCode = 1
	BEGIN 

		SET @intCodeUsageLimit = (SELECT DC.CodeUsageLimit FROM PromotionOffer PO
								INNER JOIN DiscountCodes DC
								ON PO.numProId = DC.numPromotionID
								WHERE numProId = @numProdId AND numDomainId = @numDomainID AND bitRequireCouponCode = 1 
								AND DC.vcDiscountCode = @txtCouponCode)

		IF @intCodeUsageLimit = 0
		BEGIN 
			SELECT 1
		END
		ELSE IF @intCodeUsageLimit > 0
		BEGIN 

			SET @intUsedCountForDiv = (SELECT COUNT(DCU.numDivisionId) FROM DiscountCodeUsage DCU
										INNER JOIN DiscountCodes DC
										ON  DCU.numDiscountId = DC.numDiscountId
										WHERE DCU.numDivisionId = @numDivisionId AND DC.vcDiscountCode = @txtCouponCode)

			IF @intUsedCountForDiv = 0
			BEGIN 
				SELECT 1
			END
			ELSE IF @intUsedCountForDiv > 0
			BEGIN 

				SET @intCodeUsed = (SELECT intCodeUsed FROM DiscountCodeUsage DCU
										INNER JOIN DiscountCodes DC
										ON  DCU.numDiscountId = DC.numDiscountId
										WHERE DCU.numDivisionId = @numDivisionId AND DC.vcDiscountCode = @txtCouponCode)				
								
				IF @intCodeUsed < @intCodeUsageLimit
				BEGIN
					SELECT 1
				END
				ELSE
				BEGIN				
					SET @ERRORMESSAGE = 'CODE_USAGE_LIMIT_EXCEEDS' 

					RAISERROR(@ERRORMESSAGE,16,1)
					RETURN
				END
			END
		END
	END	

	IF @numCouponCode = 0
	BEGIN
		
		SET @ERRORMESSAGE = 'INVALID_CODE' 

		RAISERROR(@ERRORMESSAGE,16,1)
		RETURN

	END

END 
GO