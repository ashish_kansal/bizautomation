GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_OpportunityItems_UnReceiveQty')
DROP PROCEDURE USP_OpportunityItems_UnReceiveQty
GO
CREATE PROCEDURE [dbo].[USP_OpportunityItems_UnReceiveQty] 
	@numDomainID NUMERIC(18,0),
	@numUserCntID AS NUMERIC(18,0),
	@numOppId NUMERIC(18,0),
	@numOppItemID NUMERIC(18,0),
	@numUnitHourToUnreceive FLOAT
AS
BEGIN
	-- CODE LEVE TRANSACTION SCOPE IS USED

	DECLARE @description AS VARCHAR(500)
	DECLARE @fltExchangeRate FLOAT 
	SELECT @fltExchangeRate=(CASE WHEN ISNULL(fltExchangeRate,0)=0 THEN 1 ELSE fltExchangeRate END) FROM OpportunityMaster WHERE numOppId=@numOppID

	DECLARE @TEMPReceievedItems TABLE
	(
		ID INT,
		numOIRLID NUMERIC(18,0),
		numWarehouseItemID NUMERIC(18,0),
		numUnitReceieved FLOAT,
		numDeletedReceievedQty FLOAT
	)
		
	DECLARE @numItemCode NUMERIC(18,0)
	DECLARE @numUnits FLOAT
	DECLARE @onHand AS FLOAT                                            
	DECLARE @onOrder AS FLOAT                                            
	DECLARE @onBackOrder AS FLOAT                                              
	DECLARE @onAllocation AS FLOAT
	DECLARE @numUnitHourReceived FLOAT
	DECLARE @numDeletedReceievedQty FLOAT
	DECLARE @bitDropship BIT
	DECLARE @numWareHouseItemID AS NUMERIC(18,0)
	DECLARE @numWLocationID NUMERIC(18,0)
	DECLARE @TotalOnHand AS NUMERIC(18,0)
	DECLARE @monAvgCost AS DECIMAL(20,5)
	DECLARE @monPrice AS DECIMAL(20,5) 

	IF EXISTS (SELECT numoppitemtCode FROM OpportunityItems WHERE numoppitemtCode=@numOppItemID AND ISNULL(numUnitHourReceived,0) = @numUnitHourToUnreceive)
	BEGIN
		IF EXISTS (SELECT numoppitemtCode FROM OpportunityItems OI INNER JOIN OpportunityMaster OM ON OI.numOppId=OM.numOppId WHERE numoppitemtCode=@numOppItemID AND ISNULL(numUnitHourReceived,0) > 0 AND ISNULL(OM.tintshipped,0)=0)
		BEGIN
			SELECT 
				@numItemCode=OI.numItemCode
				,@numUnits=numUnitHour
				,@numUnitHourReceived=numUnitHourReceived
				,@numDeletedReceievedQty=ISNULL(numDeletedReceievedQty,0)
				,@numWareHouseItemID = ISNULL(numWarehouseItmsID,0)
				,@numWLocationID = ISNULL(numWLocationID,0)
				,@monPrice=monTotAmount * @fltExchangeRate
				,@monAvgCost = I.monAverageCost
				,@bitDropship=ISNULL(OI.bitDropship,0)
			FROM
				OpportunityItems OI
			INNER JOIN 
				WareHouseItems WI 
			ON 
				OI.numWarehouseItmsID=WI.numWareHouseItemID
			INNER JOIN
				Item I
			ON
				OI.numItemCode = I.numItemCode
			WHERE 
				numoppitemtCode=@numOppItemID


			IF @numWareHouseItemID > 0 AND @bitDropship=0
			BEGIN
				SET @TotalOnHand=0  
				SELECT @TotalOnHand=(SUM(ISNULL(numOnHand,0)) + SUM(ISNULL(numAllocation,0))) FROM dbo.WareHouseItems WHERE numItemID=@numItemCode

				SELECT  
					@onHand = ISNULL(numOnHand, 0),
					@onAllocation = ISNULL(numAllocation, 0),
					@onOrder = ISNULL(numOnOrder, 0),
					@onBackOrder = ISNULL(numBackOrder, 0)
				FROM 
					WareHouseItems
				WHERE 
					numWareHouseItemID = @numWareHouseItemID 


				IF (SELECT 
						COUNT(*)
					FROM 
						OppWarehouseSerializedItem OWSI 
					INNER JOIN 
						OpportunityMaster OM 
					ON 
						OWSI.numOppID=OM.numOppId 
						AND tintOppType=1 
					WHERE 
						OWSI.numWarehouseItmsDTLID IN (SELECT numWarehouseItmsDTLID FROM OppWarehouseSerializedItem OWSIInner WHERE OWSIInner.numOppID = @numOppID AND numOppItemID=@numOppItemID)
					) > 0
				BEGIN
					RAISERROR ('SERIAL/LOT#_USED', 16, 1 ) ;
				END
				ELSE
				BEGIN
					-- REMOVE SERIAL/LOT# NUMBER FROM INVENTORY BECAUSE PURCHASE ORDER IS DELETED
					UPDATE WHIDL
						SET WHIDL.numQty = (CASE WHEN Item.bitLotNo = 1 THEN ISNULL(WHIDL.numQty,0) - ISNULL(OWSI.numQty,0) ELSE 0 END)
					FROM 
						WareHouseItmsDTL WHIDL
					INNER JOIN
						OppWarehouseSerializedItem OWSI
					ON
						WHIDL.numWareHouseItmsDTLID = OWSI.numWarehouseItmsDTLID
						AND WHIDL.numWareHouseItemID = OWSI.numWarehouseItmsID
					INNER JOIN
						WareHouseItems
					ON
						WHIDL.numWareHouseItemID = WareHouseItems.numWareHouseItemID
					INNER JOIN
						Item
					ON
						WareHouseItems.numItemID = Item.numItemCode
					WHERE
						OWSI.numOppID = @numOppId
						AND numOppItemID=@numOppItemID
				END

				INSERT INTO
					@TEMPReceievedItems
				SELECT
					ROW_NUMBER() OVER(ORDER BY ID),
					ID,
					numWarehouseItemID,
					numUnitReceieved,
					ISNULL(numDeletedReceievedQty,0)
				FROM
					OpportunityItemsReceievedLocation 
				WHERE
					numDomainID=@numDomainID
					AND numOppID=@numOppId
					AND numOppItemID=@numOppItemID

				DECLARE @j AS INT = 1
				DECLARE @COUNT AS INT
				DECLARE @numTempOIRLID NUMERIC(18,0)
				DECLARE @numTempOnHand FLOAT
				DECLARE @numTempWarehouseItemID NUMERIC(18,0)
				DECLARE @numTempUnitReceieved FLOAT
				DECLARE @numTempDeletedReceievedQty FLOAT				
				DECLARE @vcFromLocation AS VARCHAR(300)

				SELECT @COUNT=COUNT(*) FROM @TEMPReceievedItems

				WHILE @j <= @COUNT
				BEGIN
					SELECT 
						@numTempOIRLID=TRI.numOIRLID,
						@numTempOnHand= ISNULL(numOnHand,0),
						@numTempWarehouseItemID=ISNULL(TRI.numWarehouseItemID,0),
						@numTempUnitReceieved=ISNULL(numUnitReceieved,0),
						@numTempDeletedReceievedQty=ISNULL(numDeletedReceievedQty,0)
					FROM 
						@TEMPReceievedItems TRI
					INNER JOIN
						WareHouseItems WI
					ON
						TRI.numWarehouseItemID=WI.numWareHouseItemID
					WHERE 
						ID=@j

					
					SELECT @vcFromLocation=ISNULL(WL.vcLocation,'') FROM WareHouseItems WI LEFT JOIN WarehouseLocation WL ON WI.numWLocationID=WL.numWLocationID WHERE WI.numWareHouseItemID=@numTempWarehouseItemID

					IF @numTempOnHand >= (@numTempUnitReceieved - @numTempDeletedReceievedQty)
					BEGIN
						UPDATE
							WareHouseItems
						SET
							numOnHand = ISNULL(numOnHand,0) - (ISNULL(@numTempUnitReceieved,0) - ISNULL(@numTempDeletedReceievedQty,0)),
							numOnOrder = ISNULL(numOnOrder,0),
							dtModified = GETDATE()
						WHERE
							numWareHouseItemID=@numTempWarehouseItemID

						UPDATE
							WareHouseItems
						SET 
							numOnOrder = ISNULL(numOnOrder,0) + (ISNULL(@numTempUnitReceieved,0) - ISNULL(@numTempDeletedReceievedQty,0)),
							dtModified = GETDATE()
						WHERE 
							numWareHouseItemID = @numWareHouseItemID

						UPDATE
							OpportunityItems
						SET
							numDeletedReceievedQty = ISNULL(numDeletedReceievedQty,0) + (ISNULL(@numTempUnitReceieved,0) - ISNULL(@numTempDeletedReceievedQty,0))
						WHERE
							numoppitemtCode=@numOppItemID
					END
					ELSE
					BEGIN
						RAISERROR('INSUFFICIENT_ONHAND_QTY',16,1)
						RETURN
					END 

					SET @description = CONCAT('PO Qty Unreceived (Qty: ',CAST((@numTempUnitReceieved - @numTempDeletedReceievedQty) AS VARCHAR(10)),')')

					EXEC dbo.USP_ManageWareHouseItems_Tracking
						@numWareHouseItemID = @numTempWarehouseItemID,
						@numReferenceID = @numOppId,
						@tintRefType = 4,
						@vcDescription = @description,
						@numModifiedBy = @numUserCntID,
						@numDomainID = @numDomainID

					SET @numUnitHourReceived = ISNULL(@numUnitHourReceived,0) - (@numTempUnitReceieved - @numTempDeletedReceievedQty)

					SET @description= CONCAT('PO Qty Unreceived From Internal Location ',ISNULL(@vcFromLocation,''),' (Qty:',CAST((@numTempUnitReceieved - @numTempDeletedReceievedQty) AS VARCHAR(10)),')')

					EXEC dbo.USP_ManageWareHouseItems_Tracking
						@numWareHouseItemID = @numWareHouseItemID,
						@numReferenceID = @numOppId,
						@tintRefType = 4,
						@vcDescription = @description,
						@numModifiedBy = @numUserCntID,
						@numDomainID = @numDomainID

					DELETE FROM OpportunityItemsReceievedLocation WHERE ID=@numTempOIRLID

					SET @j = @j + 1
				END

				--WE ARE FETCHING VALUE AGAIN BECAUSE IF ITEMS ARE RECEIVED TO DIFFERENT LOCATION THAN IT'S CHANGE VALUE FROM CODE ABOVE
				SELECT @numDeletedReceievedQty=ISNULL(numDeletedReceievedQty,0) FROM OpportunityItems WHERE	numoppitemtCode=@numOppItemID
				
				SET @description= CONCAT('PO Qty Unreceived (Qty:',@numUnitHourReceived,')')

				IF @onHand >= @numUnitHourReceived
				BEGIN
					IF @TotalOnHand - @numUnitHourReceived <= 0
					BEGIN
						SET @monAvgCost = 0
					END
					ELSE
					BEGIN
						SET @monAvgCost = ((@TotalOnHand * @monAvgCost) - (@numUnitHourReceived * (@monPrice/@numUnits))) / ( @TotalOnHand - @numUnitHourReceived )
					END        
				
					PRINT @monAvgCost
					PRINT @numItemCode

					UPDATE  
						Item
					SET 
						monAverageCost = (CASE WHEN ISNULL(bitVirtualInventory,0) = 1 THEN 0 ELSE @monAvgCost END)
					WHERE 
						numItemCode = @numItemCode

					UPDATE
						WareHouseItems
					SET 
						numOnHand = ISNULL(numOnHand,0) - ISNULL(@numUnitHourReceived,0),
						numOnOrder = ISNULL(numOnOrder,0) + ISNULL(@numUnitHourReceived,0),
						dtModified = GETDATE()
					WHERE 
						numWareHouseItemID = @numWareHouseItemID

					UPDATE OpportunityItems SET numUnitHourReceived=0,numDeletedReceievedQty=0 WHERE numOppitemtcode = @numOppItemID

					EXEC dbo.USP_ManageWareHouseItems_Tracking
						@numWareHouseItemID = @numWareHouseItemID,
						@numReferenceID = @numOppId,
						@tintRefType = 4,
						@vcDescription = @description,
						@numModifiedBy = @numUserCntID,
						@numDomainID = @numDomainID
				END
				ELSE
				BEGIN
					RAISERROR('INSUFFICIENT_ONHAND_QTY',16,1)
					RETURN
				END
			END
			ELSE
			BEGIN
				UPDATE OpportunityItems SET numUnitHourReceived=0 WHERE numOppitemtcode = @numOppItemID
			END
		END
	END
	ELSE 
	BEGIN
		RAISERROR('RECEIVED_QTY_IS_CHANGED_BY_OTHER_USER',16,1)
	END
END
GO