SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_CreateSalesFulfillmentOrder')
DROP PROCEDURE dbo.USP_CreateSalesFulfillmentOrder
GO
CREATE PROCEDURE [dbo].[USP_CreateSalesFulfillmentOrder]
    (
	  @numDomainID NUMERIC(9),
      @numOppID NUMERIC(9),
      @numUserCntID NUMERIC(9),
      @numBizDocStatus NUMERIC(9),
      @tintMode AS TINYINT,
	  @numOppAuthBizDocsId as numeric(9)=NULL OUTPUT
    )
AS 
    BEGIN
BEGIN TRY		
        DECLARE @numOppBizDocsId NUMERIC(9);SET @numOppBizDocsId=0
		DECLARE @numPackingSlipBizDocId NUMERIC(9),@numAuthInvoice NUMERIC(9)
		DECLARE @UTCDATE AS DATETIME;SET @UTCDATE=GETUTCDATE()
		
		--Create FulfillmentBizDocs
		DECLARE @numInventoryInvoice NUMERIC(9),@numNonInventoryFOInvoice NUMERIC(9),@numServiceFOInvoice NUMERIC(9)
		
		SELECT @numInventoryInvoice=ISNULL(numBizDocTypeID,0) FROM OpportunityFulfillmentBizDocs WHERE numDomainID=@numDomainID AND ItemType='P'
		SELECT @numNonInventoryFOInvoice=ISNULL(numBizDocTypeID,0) FROM OpportunityFulfillmentBizDocs WHERE numDomainID=@numDomainID AND ItemType='N'
		SELECT @numServiceFOInvoice=ISNULL(numBizDocTypeID,0) FROM OpportunityFulfillmentBizDocs WHERE numDomainID=@numDomainID AND ItemType='S'
		
		--Create Auth BizDocs
		SELECT @numAuthInvoice=isnull(AuthoritativeBizDocs.numAuthoritativeSales,0) From dbo.AuthoritativeBizDocs Where AuthoritativeBizDocs.numDomainId=@numDomainId 
			
		--Create Packing Slip
		SELECT @numPackingSlipBizDocId=ListDetails.numListItemID FROM dbo.ListDetails WHERE ListDetails.vcData='Packing Slip' AND ListDetails.constFlag=1 AND ListDetails.numListID=27
								
		IF @numInventoryInvoice>0
		BEGIN
				EXEC dbo.USP_CreateBizDocs
							@numOppId = @numOppID, --  numeric(9, 0)
							@numBizDocId = @numInventoryInvoice, --  numeric(9, 0)
							@numUserCntID = @numUserCntID, --  numeric(9, 0)
							@numOppBizDocsId =@numOppBizDocsId output,
							@vcComments = '', --  varchar(1000)
							@bitPartialFulfillment = 1, --  bit
							@strBizDocItems = '', --  text
							--@monShipCost = 0, --  DECIMAL(20,5)
							@numShipVia = 0, --  numeric(9, 0)
							@vcTrackingURL = '', --  varchar(1000)
							@dtFromDate = @UTCDATE, --  datetime
							@numBizDocStatus = 0, --  numeric(9, 0)
							@bitRecurringBizDoc = 0, --  bit
							@numSequenceId = '', --  varchar(50)
							@tintDeferred = 0, --  tinyint
							@monCreditAmount =0,
							@ClientTimeZoneOffset = 0, --  int
							@monDealAmount =0,
							@bitRentalBizDoc = 0, --  bit
							@numBizDocTempID = 0, --  numeric(9, 0)
							@vcTrackingNo = '', --  varchar(500)
							--@vcShippingMethod = '', --  varchar(100)
							@vcRefOrderNo = '', --  varchar(100)
							--@dtDeliveryDate = @UTCDATE, --  datetime
							@OMP_SalesTaxPercent = 0, --  float
							@numFromOppBizDocsId = 0, --  numeric(9, 0)
							@bitTakeSequenceId = 1  --bit
			END
			
			IF @numInventoryInvoice != @numNonInventoryFOInvoice AND @numNonInventoryFOInvoice>0
			BEGIN
				EXEC dbo.USP_CreateBizDocs
							@numOppId = @numOppID, --  numeric(9, 0)
							@numBizDocId = @numNonInventoryFOInvoice, --  numeric(9, 0)
							@numUserCntID = @numUserCntID, --  numeric(9, 0)
							@numOppBizDocsId =0,
							@vcComments = '', --  varchar(1000)
							@bitPartialFulfillment = 1, --  bit
							@strBizDocItems = '', --  text
							--@monShipCost = 0, --  DECIMAL(20,5)
							@numShipVia = 0, --  numeric(9, 0)
							@vcTrackingURL = '', --  varchar(1000)
							@dtFromDate = @UTCDATE, --  datetime
							@numBizDocStatus = 0, --  numeric(9, 0)
							@bitRecurringBizDoc = 0, --  bit
							@numSequenceId = '', --  varchar(50)
							@tintDeferred = 0, --  tinyint
							@monCreditAmount =0,
							@ClientTimeZoneOffset = 0, --  int
							@monDealAmount =0,
							@bitRentalBizDoc = 0, --  bit
							@numBizDocTempID = 0, --  numeric(9, 0)
							@vcTrackingNo = '', --  varchar(500)
							--@vcShippingMethod = '', --  varchar(100)
							@vcRefOrderNo = '', --  varchar(100)
							--@dtDeliveryDate = @UTCDATE, --  datetime
							@OMP_SalesTaxPercent = 0, --  float
							@numFromOppBizDocsId = 0, --  numeric(9, 0)
							@bitTakeSequenceId = 1  --bit
				END
				
				IF @numInventoryInvoice != @numServiceFOInvoice AND @numNonInventoryFOInvoice != @numServiceFOInvoice AND @numServiceFOInvoice>0
				BEGIN			
						EXEC dbo.USP_CreateBizDocs
							@numOppId = @numOppID, --  numeric(9, 0)
							@numBizDocId = @numServiceFOInvoice, --  numeric(9, 0)
							@numUserCntID = @numUserCntID, --  numeric(9, 0)
							@numOppBizDocsId =0,
							@vcComments = '', --  varchar(1000)
							@bitPartialFulfillment = 1, --  bit
							@strBizDocItems = '', --  text
							--@monShipCost = 0, --  DECIMAL(20,5)
							@numShipVia = 0, --  numeric(9, 0)
							@vcTrackingURL = '', --  varchar(1000)
							@dtFromDate = @UTCDATE, --  datetime
							@numBizDocStatus = 0, --  numeric(9, 0)
							@bitRecurringBizDoc = 0, --  bit
							@numSequenceId = '', --  varchar(50)
							@tintDeferred = 0, --  tinyint
							@monCreditAmount =0,
							@ClientTimeZoneOffset = 0, --  int
							@monDealAmount =0,
							@bitRentalBizDoc = 0, --  bit
							@numBizDocTempID = 0, --  numeric(9, 0)
							@vcTrackingNo = '', --  varchar(500)
							--@vcShippingMethod = '', --  varchar(100)
							@vcRefOrderNo = '', --  varchar(100)
							--@dtDeliveryDate = @UTCDATE, --  datetime
							@OMP_SalesTaxPercent = 0, --  float
							@numFromOppBizDocsId = 0, --  numeric(9, 0)
							@bitTakeSequenceId = 1  --bit
					END		
							
					IF @tintMode=1
					BEGIN
						--Upadte FulfillmentBizDocs Status ro Release Allocation
						DECLARE @numBizDocStatusReleaseInventory NUMERIC(9);SET @numBizDocStatusReleaseInventory=0
						SELECT @numBizDocStatusReleaseInventory=ISNULL(numBizDocStatus,0) FROM dbo.OpportunityFulfillmentRules WHERE numDomainID=@numDomainID AND numRuleID=3 
					 			
				 		IF ISNULL(@numBizDocStatusReleaseInventory,0)>0
				 		BEGIN
							EXEC dbo.USP_SalesFulfillmentWorkflow
									@numDomainID = @numDomainID, --  numeric(9, 0)
									@numOppID = @numOppID, --  numeric(9, 0)
									@numOppBizDocsId = @numOppBizDocsId, --  numeric(9, 0)
									@numUserCntID = @numUserCntID, --  numeric(9, 0)
									@numBizDocStatus = @numBizDocStatusReleaseInventory --  numeric(9, 0)
							
							EXEC dbo.USP_UpdateSingleFieldValue
									@tintMode = 27, --  tinyint
									@numUpdateRecordID = @numOppBizDocsId, --  numeric(18, 0)
									@numUpdateValueID = @numBizDocStatusReleaseInventory, --  numeric(18, 0)
									@vcText = '', --  text
									@numDomainID = @numDomainID --  numeric(18, 0)
						END	
					END
					ELSE IF @tintMode=2
					BEGIN
							EXEC dbo.USP_SalesFulfillmentWorkflow
									@numDomainID = @numDomainID, --  numeric(9, 0)
									@numOppID = @numOppID, --  numeric(9, 0)
									@numOppBizDocsId = @numOppBizDocsId, --  numeric(9, 0)
									@numUserCntID = @numUserCntID, --  numeric(9, 0)
									@numBizDocStatus = @numBizDocStatus --  numeric(9, 0)
							
							EXEC dbo.USP_UpdateSingleFieldValue
									@tintMode = 27, --  tinyint
									@numUpdateRecordID = @numOppBizDocsId, --  numeric(18, 0)
									@numUpdateValueID = @numBizDocStatus, --  numeric(18, 0)
									@vcText = '', --  text
									@numDomainID = @numDomainID --  numeric(18, 0)
					END
					
					
					EXEC dbo.USP_CreateBizDocs
							@numOppId = @numOppID, --  numeric(9, 0)
							@numBizDocId = @numAuthInvoice, --  numeric(9, 0)
							@numUserCntID = @numUserCntID, --  numeric(9, 0)
							@numOppBizDocsId = @numOppAuthBizDocsId OUTPUT,
							@vcComments = '', --  varchar(1000)
							@bitPartialFulfillment = 1, --  bit
							@strBizDocItems = '', --  text
							--@monShipCost = 0, --  DECIMAL(20,5)
							@numShipVia = 0, --  numeric(9, 0)
							@vcTrackingURL = '', --  varchar(1000)
							@dtFromDate = '2013-1-2 10:55:29.610', --  datetime
							@numBizDocStatus = 0, --  numeric(9, 0)
							@bitRecurringBizDoc = 0, --  bit
							@numSequenceId = '', --  varchar(50)
							@tintDeferred = 0, --  tinyint
							@monCreditAmount =0,
							@ClientTimeZoneOffset = 0, --  int
							@monDealAmount =0,
							@bitRentalBizDoc = 0, --  bit
							@numBizDocTempID = 0, --  numeric(9, 0)
							@vcTrackingNo = '', --  varchar(500)
							--@vcShippingMethod = '', --  varchar(100)
							@vcRefOrderNo = '', --  varchar(100)
							--@dtDeliveryDate = '2013-1-2 10:55:29.611', --  datetime
							@OMP_SalesTaxPercent = 0, --  float
							@numFromOppBizDocsId = @numOppBizDocsId, --  numeric(9, 0)
							@bitTakeSequenceId = 1,  --bit
							@bitAllItems=1 --bit
					
					EXEC dbo.USP_CreateBizDocs
								@numOppId = @numOppID, --  numeric(9, 0)
								@numBizDocId = @numPackingSlipBizDocId, --  numeric(9, 0)
								@numUserCntID = @numUserCntID, --  numeric(9, 0)
								@numOppBizDocsId =0,
								@vcComments = '', --  varchar(1000)
								@bitPartialFulfillment = 1, --  bit
								@strBizDocItems = '', --  text
								--@monShipCost = 0, --  DECIMAL(20,5)
								@numShipVia = 0, --  numeric(9, 0)
								@vcTrackingURL = '', --  varchar(1000)
								@dtFromDate = '2013-1-2 10:56:4.477', --  datetime
								@numBizDocStatus = 0, --  numeric(9, 0)
								@bitRecurringBizDoc = 0, --  bit
								@numSequenceId = '', --  varchar(50)
								@tintDeferred = 0, --  tinyint
								@monCreditAmount =0,
								@ClientTimeZoneOffset = 0, --  int
								@monDealAmount =0,
								@bitRentalBizDoc = 0, --  bit
								@numBizDocTempID = 0, --  numeric(9, 0)
								@vcTrackingNo = '', --  varchar(500)
								--@vcShippingMethod = '', --  varchar(100)
								@vcRefOrderNo = '', --  varchar(100)
								--@dtDeliveryDate = '2013-1-2 10:56:4.477', --  datetime
								@OMP_SalesTaxPercent = 0, --  float
								@numFromOppBizDocsId = @numOppBizDocsId, --  numeric(9, 0)
								@bitTakeSequenceId = 1,  --bit
								@bitAllItems=0 --bit
				
		
			 END TRY
BEGIN CATCH
    DECLARE @ErrorMessage NVARCHAR(4000);
    DECLARE @ErrorSeverity INT;
    DECLARE @ErrorState INT;

    SELECT 
        @ErrorMessage = ERROR_MESSAGE(),
        @ErrorSeverity = ERROR_SEVERITY(),
        @ErrorState = ERROR_STATE();

    -- Use RAISERROR inside the CATCH block to return error
    -- information about the original error that caused
    -- execution to jump to the CATCH block.
    RAISERROR (@ErrorMessage, -- Message text.
               @ErrorSeverity, -- Severity.
               @ErrorState -- State.
               );
END CATCH;
		
    END
