GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GenericDocuments_Search')
DROP PROCEDURE USP_GenericDocuments_Search
GO
CREATE PROCEDURE [dbo].[USP_GenericDocuments_Search]                          
(                          
	@numDomainID NUMERIC(18,0)
	,@numDocCategory NUMERIC(18,0)
	,@tintDocType TINYINT
	,@vcSearchText VARCHAR(100)
	,@numPageIndex INT
	,@numPageSize INT
)                          
as 
BEGIN
	SELECT
		COUNT(*) OVER() AS numTotalRecords
		,numGenericDocID AS id
		,vcDocName AS [text]
		,ISNULL(EMM.vcModuleName,'-') vcModuleName
	FROM
		GenericDocuments
	LEFT JOIN 
		EmailMergeModule EMM 
	ON 
		EMM.numModuleID = GenericDocuments.numModuleID 
		AND EMM.tintModuleType=0
	WHERE 
		numDomainID = @numDomainID
		AND tintDocumentType=1
		AND (ISNULL(@numDocCategory,0) = 0 OR numDocCategory=@numDocCategory)
		AND 1 = (CASE @tintDocType 
					WHEN 2 THEN (CASE WHEN ISNULL(VcFileName,'') LIKE '#SYS#%' THEN 1 ELSE 0 END)
					WHEN 1 THEN (CASE WHEN ISNULL(VcFileName,'') NOT LIKE '#SYS#%' THEN 1 ELSE 0 END) 
					ELSE 1 
				END)
		AND vcDocName LIKE CONCAT('%',@vcSearchText,'%')
	ORDER BY
		vcDocName
	OFFSET 
		((@numPageIndex -1) * @numPageSize) ROWS 
	FETCH NEXT @numPageSize ROWS ONLY
END
GO