SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ManageBizDocFilter')
DROP PROCEDURE USP_ManageBizDocFilter
GO
CREATE PROCEDURE [dbo].[USP_ManageBizDocFilter]                
@strAOI as text,                  
@numDomainID as numeric(9)=0                   
as                    
declare @hDoc as int     

   DELETE FROM  BizDocFilter WHERE numDomainId=@numDomainId
    
    EXEC sp_xml_preparedocument @hDoc OUTPUT, @strAOI      

    insert into BizDocFilter (numBizDoc, tintBizocType,numDomainId)  
	SELECT numBizDoc,tintBizocType,@numDomainID
	FROM OPENXML(@hDoc,'/NewDataSet/TableBizDoc',2)                                                    
    with(numBizDoc numeric(9),tintBizocType TINYINT)   
    
    EXEC sp_xml_removedocument @hDoc  
  
GO
   