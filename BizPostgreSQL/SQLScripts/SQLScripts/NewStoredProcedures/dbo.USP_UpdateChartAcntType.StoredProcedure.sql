
-- =============================================  
-- Author:  <Ajit Kumar Singh>  
-- Create date: <Friday, July 28, 2008>  
-- Description: <This procedure is use for Update the Account Type against Account Id>  
-- =============================================  
--Testing
alter PROCEDURE [dbo].[USP_UpdateChartAcntType]   
@numDomainId as numeric(9)=0,        
@numAccountId as numeric(9)=0,  
@numAcntType as  numeric(9)=0,  
@vcCatgyName varchar(100)                                                                                                                                         
as              
Begin            
	
	update listdetails set vcData=@vcCatgyName 
	where numListItemID = @numAcntType and numDomainId =@numDomainId

	Update Chart_Of_Accounts Set vcCatgyName=@vcCatgyName
	Where numAccountId=@numAccountId And numDomainId =@numDomainId

End  
GO
  