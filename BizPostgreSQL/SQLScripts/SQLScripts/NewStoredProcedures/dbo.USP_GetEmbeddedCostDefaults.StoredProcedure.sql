-- exec USP_GetEmbeddedCostDefaults @numEmbeddedCostID=0,@numCostCatID=16572,@numDomainID=1
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetEmbeddedCostDefaults')
DROP PROCEDURE USP_GetEmbeddedCostDefaults
GO
CREATE PROCEDURE [dbo].[USP_GetEmbeddedCostDefaults]
    @numEmbeddedCostID NUMERIC=0,
    @numCostCatID NUMERIC=0,
    @numDomainID NUMERIC
AS 
    BEGIN
        SELECT  [numEmbeddedCostID],
				numCostCatID,
				[numCostCenterID],
                [numAccountID],
                [numDivisionID],
                dbo.[fn_GetComapnyName]([numDivisionID]) vcCompanyName,
                [numPaymentMethod],
                [numDomainID]
        FROM    EmbeddedCostDefaults
        WHERE   
				--[numEmbeddedCostID] = @numEmbeddedCostID
				--AND
				 numCostCatID=@numCostCatID
                AND numDomainID = @numDomainID
	
    END