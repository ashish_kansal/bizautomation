SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetVendorCostTable')
DROP PROCEDURE USP_GetVendorCostTable
GO
CREATE PROCEDURE [dbo].[USP_GetVendorCostTable] 
( 
@numDomainID as numeric(9)=0,    
@numItemcode AS NUMERIC(9)=0
)
AS 

Create table #tempVendors(numItemCode numeric(9),numCompanyId numeric(9),numVendorId numeric(9),Vendor varchar(100),monCost DECIMAL(20,5),intRowNum int)

insert into #tempVendors
select I.numItemCode,div.numCompanyID,Vendor.numVendorID,substring(vcCompanyName,0,30) as Vendor,        
ISNULL(monCost,0) monCost,row_number() Over(order by Vendor.numVendorID) as intRowNum
from Vendor          
join divisionMaster div        
on div.numdivisionid=numVendorid        
join companyInfo com        
on com.numCompanyID=div.numCompanyID        
join Item I  
on I.numItemCode= Vendor.numItemCode      
WHERE Vendor.numDomainID=@numDomainID and Vendor.numItemCode= @numItemCode


SELECT t.numItemCode,t.numVendorid,PT.intFromQty,PT.intToQty,PT.tintDiscountType, CASE PT.[tintDiscountType]
                  WHEN 1
                  THEN CAST(CONVERT(DECIMAL(18, 2),PT.[decDiscount]) AS VARCHAR)
                  ELSE CAST(CONVERT(INT, PT.[decDiscount]) AS VARCHAR)
                END AS decDiscount,
		Case when t.monCost>0 then
	    Case PT.[tintDiscountType] when 1 then t.monCost - (PT.[decDiscount]*t.monCost /100) 
			                       when 2 then t.monCost - PT.[decDiscount] 
									else 0 end 		
        else 0 end as monCost,row_number() Over(partition by t.numItemCode,t.numVendorid order by PT.intFromQty,PT.intToQty) as Tier
		into #tempData
		FROM  Item I 
        LEFT JOIN PriceBookRules P ON I.numDomainID = P.numDomainID
        LEFT JOIN PriceBookRuleDTL PDTL ON P.numPricRuleID = PDTL.numRuleID
        LEFT JOIN PriceBookRuleItems PBI ON P.numPricRuleID = PBI.numRuleID
        LEFT JOIN [PriceBookPriorities] PP ON PP.[Step2Value] = P.[tintStep2] 
                                              AND PP.[Step3Value] = P.[tintStep3]
	    Left join PricingTable PT on PT.numPriceRuleID = P.numPricRuleID AND ISNULL(PT.numCurrencyID,0) = 0,#tempVendors t
WHERE   I.numItemCode = t.numItemCode AND tintRuleFor=2
AND ((tintStep2 = 1 AND PBI.numValue = t.numItemCode) 
AND (tintStep3 = 1 AND PDTL.numValue = t.numVendorid))


declare @intRowNum as int
declare @Vendor as varchar(100)
declare @ItemCode as numeric(9),@numVendorId as numeric(9)                                                 
declare @strSQLUpdate as varchar(2000)

Create table #tempVT (iTier int,[0~Tier] varchar(20))

declare @MaxTier as int
select @MaxTier = max(Tier) from #tempData
;WITH cte AS
(
SELECT 0 x
UNION ALL
SELECT x + 1
FROM cte
WHERE x < @MaxTier
)

insert into #tempVT(iTier) select x from cte

select top 1 @intRowNum=intRowNum,@Vendor=Vendor,@ItemCode=numItemCode,@numVendorId=numVendorId
 from #tempVendors order by intRowNum                                                                                          
while @intRowNum>0                                                                                  
 begin    
		exec('alter table #tempVT add [' + @numVendorId + '~' + @Vendor +'] varchar(100)')
		
        set @strSQLUpdate= 'update VT set [' + Cast(@numVendorId as varchar(20)) + '~' + @Vendor +']= 
							(CAST(CONVERT(DECIMAL(9, 2), V.monCost) AS VARCHAR)) from #tempVT VT join #tempVendors V on VT.iTier=0 
							where V.numItemCode=' + Cast(@ItemCode as varchar(20)) + ' and V.numVendorId=' + Cast(@numVendorId as varchar(20))
		exec (@strSQLUpdate)
	        
        
        set @strSQLUpdate= 'update VT set [' + Cast(@numVendorId as varchar(20)) + '~' + @Vendor +']= 
							(Cast(D.intFromQty as varchar(20)) + '' - '' + Cast(D.intToQty as varchar(20)) + '' ('' + CAST(CONVERT(DECIMAL(9, 2), D.monCost) AS VARCHAR) + '')'')
							from #tempVT VT join #tempData D on VT.iTier=D.Tier 
							where D.numItemCode=' + Cast(@ItemCode as varchar(20)) + ' and D.numVendorId=' + Cast(@numVendorId as varchar(20))
		exec (@strSQLUpdate)
	                                                                                    
		select top 1 @intRowNum=intRowNum,@Vendor=Vendor,@ItemCode=numItemCode,@numVendorId=numVendorId
		from #tempVendors where intRowNum>@intRowNum order by intRowNum
		           
		if @@rowcount=0 set @intRowNum=0                                                                                          
 end

update #tempVT set [0~Tier]='Tier ' + Cast(iTier as varchar(20)) where iTier>0
update #tempVT set [0~Tier]='Base'  where iTier=0

ALTER TABLE #tempVT DROP COLUMN iTier

--select * from #tempVendors
--select * from #tempData
select * from #tempVT

drop table #tempVendors
drop table #tempData
drop table #tempVT
