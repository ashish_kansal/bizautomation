SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_GetCommissionsContacts')
DROP PROCEDURE usp_GetCommissionsContacts
GO
CREATE PROCEDURE [dbo].[usp_GetCommissionsContacts]
@numDomainID AS NUMERIC,
@byteMode AS TINYINT

AS

  IF @byteMode=1
  BEGIN
  		 select  CC.numCommContactID,D.numDivisionID,A.numContactID,A.vcFirstName +' '+ A.vcLastName + '(' + C.vcCompanyName +')'as vcUserName,C.vcCompanyName  
		from CommissionContacts CC JOIN DivisionMaster D ON CC.numDivisionID=D.numDivisionID
		JOIN AdditionalContactsInformation A ON D.numDivisionID=A.numDivisionID
		join CompanyInfo C on C.numCompanyID=D.numCompanyID  
		where CC.numDomainID=@numDomainID AND numCompanyType <> 93 
  END
  ELSE
  BEGIN
  	select  CC.numCommContactID,D.numDivisionID,C.vcCompanyName  
	from CommissionContacts CC JOIN DivisionMaster D ON CC.numDivisionID=D.numDivisionID
	join CompanyInfo C on C.numCompanyID=D.numCompanyID  
	where CC.numDomainID=@numDomainID
  END
GO
