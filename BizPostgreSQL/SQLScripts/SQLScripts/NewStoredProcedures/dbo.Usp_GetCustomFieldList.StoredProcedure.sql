/****** Object:  StoredProcedure [dbo].[Usp_GetCustomFieldList]    Script Date: 13/12/2013 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'Usp_GetCustomFieldList' ) 
    DROP PROCEDURE Usp_GetCustomFieldList
GO
CREATE PROCEDURE [dbo].[Usp_GetCustomFieldList]
    @numDomainID AS NUMERIC(9) = 0 ,
    @locId AS NUMERIC(9) ,
    @numListID AS NUMERIC(9) = 0
AS 
    SELECT  fld_id ,
            LOWER(Fld_label) [Fld_label] ,
            fld_type ,
            CASE WHEN subgrp = 0 THEN 'Detail Section'
                 ELSE grp_name
            END AS TabName ,
            loc_name ,
            ISNULL(CFV.bitIsRequired, 0) AS bitIsRequired ,
            ISNULL(CFV.bitIsEmail, 0) AS bitIsEmail ,
            ISNULL(CFV.bitIsAlphaNumeric, 0) AS bitIsAlphaNumeric ,
            ISNULL(CFV.bitIsNumeric, 0) AS bitIsNumeric ,
            ISNULL(CFV.bitIsLengthValidation, 0) AS bitIsLengthValidation ,
            ISNULL(fmst.numlistid, 0) AS [ListID]
    FROM    CFW_Fld_Master fmst
            LEFT JOIN CFw_Grp_Master Tmst ON Tmst.Grp_id = fmst.subgrp
            JOIN CFW_Loc_Master Lmst ON Lmst.Loc_id = fmst.Grp_id
            LEFT JOIN CFW_Validation CFV ON CFV.numFieldID = fld_id
    WHERE   fmst.numDomainID = @numDomainID
            AND Lmst.Loc_id = CASE WHEN @locId = 0 THEN Lmst.Loc_id
                                   ELSE @locId
                              END

    SELECT  Ld.numListItemID ,
            LOWER(vcData) [vcData] ,
            Ld.constFlag ,
            bitDelete ,
            ISNULL(intSortOrder, 0) intSortOrder,
            Ld.numListID AS [ListID]
    FROM    ListDetails LD
            LEFT JOIN listorder LO ON Ld.numListItemID = LO.numListItemID
                                      AND lo.numDomainId = @numDomainID
    WHERE   ( Ld.numDomainID = @numDomainID
              OR Ld.constFlag = 1
            )
            AND Ld.numListID IN (SELECT numListID FROM CFW_Fld_Master CFM
								LEFT JOIN CFw_Grp_Master Tmst ON Tmst.Grp_id = CFM.subgrp
								JOIN CFW_Loc_Master Lmst ON Lmst.Loc_id = CFM.Grp_id
								--LEFT JOIN CFW_Validation CFV ON CFV.numFieldID = fld_id
								WHERE CFM.numDomainID =  @numDomainID )
    ORDER BY intSortOrder                              
GO

--EXEC Usp_GetCustomFieldList	1,9,0