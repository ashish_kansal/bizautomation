GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ExtranetAccountsDtl_ResetPassword')
DROP PROCEDURE USP_ExtranetAccountsDtl_ResetPassword
GO
CREATE PROCEDURE [dbo].[USP_ExtranetAccountsDtl_ResetPassword]
	@numDomainID AS NUMERIC(18,0),
    @vcResetID AS VARCHAR(500),
    @vcNewPassword AS VARCHAR(100)
AS 
BEGIN
	IF EXISTS (SELECT numExtranetDtlID FROM ExtranetAccountsDtl WHERE vcResetLinkID=@vcResetID AND DATEDIFF(MINUTE, vcResetLinkCreatedTime, GETUTCDATE()) <= 10)
	BEGIN
		UPDATE  
			ExtranetAccountsDtl
        SET
			vcPassword = @vcNewPassword
        WHERE 
			vcResetLinkID = @vcResetID

		UPDATE
			ExtranetAccountsDtl
		SET
			vcResetLinkID = ''
		WHERE 
			vcResetLinkID = @vcResetID
	END
	ELSE
	BEGIN
		RAISERROR('INVALID_RESET_LINK',16,1)
	END
END
GO

