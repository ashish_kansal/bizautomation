GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
                    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_StagePercentageDetails_GetMilestonesByProcess')
DROP PROCEDURE USP_StagePercentageDetails_GetMilestonesByProcess
GO
CREATE PROCEDURE [dbo].[USP_StagePercentageDetails_GetMilestonesByProcess]                             
	@numDomainID AS NUMERIC(18,0)                            
	,@numProcessID NUMERIC(18,0)
AS                            
BEGIN
	SELECT
		vcMileStoneName
	FROM
		StagePercentageDetails
	WHERE
		numDomainId=@numDomainID
		AND slp_id = @numProcessID
	GROUP BY
		vcMileStoneName
END
GO