SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ReportListMaster_RemindersPreBuildReport')
DROP PROCEDURE USP_ReportListMaster_RemindersPreBuildReport
GO
CREATE PROCEDURE [dbo].[USP_ReportListMaster_RemindersPreBuildReport]
	@numDomainID NUMERIC(18,0)
	,@ClientTimeZoneOffset INT
AS
BEGIN 
	DECLARE @bitReOrderPoint AS BIT
	DECLARE @numDefaultSalesShippingDoc AS NUMERIC(18,0)

	DECLARE @TimeAndExpenseToReview INT
	DECLARE @PriceMarginToReview INT
	DECLARE @PromotionApproval INT
	DECLARE @SalesOppStagesToComplete INT
	DECLARE @PurchaseOppStagesToComplete INT
	DECLARE @SalesOrderStagesToComplete INT
	DECLARE @ProjectStagesToComplete INT
	DECLARE @CasesToSolve INT
	DECLARE @PendingSalesRMA INT
	DECLARE @PaymentsToDeposite INT
	DECLARE @BillsToPay INT
	DECLARE @PurchaseOrderToBill INT
	DECLARE @SalesOrderToPickAndPack INT
	DECLARE @SalesOrderToShip INT
	DECLARE @SalesOrderToInvoice INT
	DECLARE @SalesOrderWithItemsToPurchase INT
	DECLARE @SalesOrderPastTheirReleaseDate INT
	DECLARE @ItemOnHandLessThenReorder INT

	SELECT 
		@bitReOrderPoint=ISNULL(bitReOrderPoint,0)
		,@numDefaultSalesShippingDoc = ISNULL(numDefaultSalesShippingDoc,0)
	FROM 
		Domain 
	WHERE 
		numDomainId = @numDomainID

	DECLARE @TEMP TABLE
	(
		ID INT
		,Value INT
	)

	---------------------------- 1 Billable time & expense entries to review -----------------------------------

	SELECT 
		@TimeAndExpenseToReview = COUNT(numCategoryHDRID)
	FROM 
		TimeAndExpense TE
	WHERE 
		TE.numDomainID = @numDOmainID
		AND TE.numApprovalComplete NOT IN (0)

	---------------------------- 2 Minimum price margin violations to review -----------------------------------

	SELECT 
		@PriceMarginToReview = COUNT(M.numOppID)
	FROM 
		OpportunityMaster AS M
	WHERE 
		M.numDomainId=@numDomainID
		AND (SELECT 
				COUNT(OpportunityItems.numoppitemtCode) 
			FROM 
				OpportunityItems 
			WHERE 
				OpportunityItems.bitItemPriceApprovalRequired=1 
				AND OpportunityItems.numOppId=M.numOppId
			) > 0

	---------------------------- 3 Promotions in sales orders to approve -----------------------------------

	SELECT 
		@PromotionApproval = COUNT(numOppID)
	FROM 
		OpportunityMaster
	WHERE 
		numDomainID=@numDomainId
		AND tintOppType = 1
		AND ISNULL(intPromotionApprovalStatus,0) NOT IN (0,-1) 

	---------------------------- 4 Sales OpportunityOrder Stages to complete -----------------------------------

	SELECT 
		@SalesOppStagesToComplete = COUNT(*) 
	FROM 
		StagePercentageDetails SPD 
	INNER JOIN 
		OpportunityMaster OM 
	ON 
		SPD.numOppID=OM.numOppId 
	WHERE 
		SPD.numDomainId=@numDOmainID 
		AND OM.numDomainId=@numDOmainID 
		AND OM.tintOppType = 1
		AND ISNULL(OM.tintOppStatus,0) = 0
		AND tinProgressPercentage <> 100

	---------------------------- 18 Purchase Opportunities Stages to complete -----------------------------------

	SELECT 
		@PurchaseOppStagesToComplete = COUNT(*) 
	FROM 
		StagePercentageDetails SPD 
	INNER JOIN 
		OpportunityMaster OM 
	ON 
		SPD.numOppID=OM.numOppId 
	WHERE 
		SPD.numDomainId=@numDOmainID 
		AND OM.numDomainId=@numDOmainID 
		AND OM.tintOppType = 2
		AND ISNULL(OM.tintOppStatus,0) = 0
		AND tinProgressPercentage <> 100

	---------------------------- 19 Sales Order Stages to complete -----------------------------------

	SELECT 
		@SalesOrderStagesToComplete = COUNT(*) 
	FROM 
		StagePercentageDetails SPD 
	INNER JOIN 
		OpportunityMaster OM 
	ON 
		SPD.numOppID=OM.numOppId
	INNER JOIN
		DivisionMaster DM
	ON
		OM.numDivisionID = DM.numDivisionID
	INNER JOIN
		AdditionalContactsInformation ACI
	ON
		OM.numContactId = ACI.numContactId
	WHERE 
		SPD.numDomainId=@numDOmainID 
		AND OM.numDomainId=@numDOmainID 
		AND OM.tintOppType = 1
		AND ISNULL(OM.tintOppStatus,0) = 1
		AND tinProgressPercentage <> 100

	---------------------------- 5 Project Stages to complete -----------------------------------

	SELECT 
		@ProjectStagesToComplete = COUNT(*) 
	FROM 
		StagePercentageDetails SPD 
	INNER JOIN 
		ProjectsMaster 
	ON 
		SPD.numProjectID=ProjectsMaster.numProId 
	WHERE 
		SPD.numDomainId=@numDOmainID 
		AND ProjectsMaster.numDomainId=@numDOmainID 
		AND tinProgressPercentage <> 100

	---------------------------- 6 Cases to solve -----------------------------------

	SELECT 
		@CasesToSolve = COUNT(*) 
	FROM 
		Cases 
	INNER JOIN
		AdditionalContactsInformation
	ON
		Cases.numContactId = AdditionalContactsInformation.numContactId
	WHERE 
		Cases.numDomainID=@numDOmainID 
		AND numStatus <> 136
	
	---------------------------- 7 Pending Sales RMAs -----------------------------------

	SELECT 
		@PendingSalesRMA = COUNT(*) 
	FROM 
		ReturnHeader 
	WHERE 
		numDomainId=@numDomainID 
		AND tintReturnType = 1 
		AND numReturnStatus = 301

	---------------------------- 8 Payments to deposit -----------------------------------

	SELECT 
		@PaymentsToDeposite = COUNT(numDepositId)
	FROM 
		dbo.DepositMaster DM
	WHERE 
		DM.numDomainId = @numDomainID
		AND tintDepositeToType = 2
		AND ISNULL(bitDepositedToAcnt,0) = 0 

	---------------------------- 9 Bills to pay -----------------------------------

	SELECT 
		@BillsToPay = COUNT(*)
	FROM
	(
		SELECT  
			OBD.numOppBizDocsId
		FROM    
			OpportunityBizDocs OBD
		INNER JOIN 
			OpportunityMaster Opp 
		ON 
			OBD.numOppId = Opp.numOppId
		INNER JOIN 
			AdditionalContactsInformation ADC 
		ON 
			Opp.numContactId = ADC.numContactId
		INNER JOIN 
			DivisionMaster Div 
		ON 
			Opp.numDivisionId = Div.numDivisionID 
			AND ADC.numDivisionId = Div.numDivisionID
		INNER JOIN 
			CompanyInfo C 
		ON 
			Div.numCompanyID = C.numCompanyId
		WHERE 
			Opp.numDomainId = @numDomainId
			AND OBD.bitAuthoritativeBizDocs = 1
			AND Opp.tintOppType = 2
			AND ( OBD.monDealAmount - OBD.monAmountPaid ) > 0
			UNION       
			--Regular Bills For Add Bill Payment
		SELECT  BH.numBillID FROM dbo.BillHeader BH	WHERE BH.numDomainId = @numDomainId AND BH.bitIsPaid = 0
	) TEMP

	---------------------------- 10 Purchase Orders to bill -----------------------------------

	--SELECT
	--	@PurchaseOrderToBill = COUNT(numOppID)
	--FROM
	--(
	--	SELECT DISTINCT
	--		OM.numOppId
	--	FROM
	--		OpportunityMaster OM
	--	INNER JOIN
	--		OpportunityItems OI
	--	ON
	--		OI.numOppId = OM.numOppID
	--	INNER JOIN
	--		Item I
	--	ON
	--		OI.numItemCode = I.numItemCode
	--	OUTER APPLY
	--	(
	--		SELECT
	--			SUM(OpportunityBizDocItems.numUnitHour) AS BilledQty
	--		FROM
	--			OpportunityBizDocs
	--		INNER JOIN
	--			OpportunityBizDocItems 
	--		ON
	--			OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
	--		WHERE
	--			OpportunityBizDocs.numOppId = OM.numOppID
	--			AND OpportunityBizDocItems.numOppItemID = OI.numoppitemtCode
	--			AND OpportunityBizDocs.numBizDocId = (SELECT ISNULL(numAuthoritativePurchase,0) FROM AuthoritativeBizDocs WHERE numDomainId=@numDomainID)
	--	) AS TempBilled
	--	WHERE
	--		OM.numDomainId = @numDomainID
	--		AND ISNULL(OM.bitStockTransfer,0) = 0
	--		AND OM.tintOppType = 2
	--		AND OM.tintOppStatus  = 1
	--		AND ISNULL(OI.numUnitHour,0) <> ISNULL(TempBilled.BilledQty,0)
	--) TEMP


	---------------------------- 11 Sales Orders to Pick & Pack -----------------------------------

	--IF @numDefaultSalesShippingDoc > 0
	--BEGIN
	--	SELECT
	--		@SalesOrderToPickAndPack = COUNT(numOppId)
	--	FROM
	--	(
	--		SELECT DISTINCT
	--			OM.numOppId
	--		FROM
	--			OpportunityMaster OM
	--		INNER JOIN
	--			OpportunityItems OI
	--		ON
	--			OI.numOppId = OM.numOppId
	--		INNER JOIN
	--			Item I
	--		ON
	--			OI.numItemCode = I.numItemCode
	--		OUTER APPLY
	--		(
	--			SELECT
	--				SUM(OpportunityBizDocItems.numUnitHour) AS PickPacked
	--			FROM
	--				OpportunityBizDocs
	--			INNER JOIN
	--				OpportunityBizDocItems 
	--			ON
	--				OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
	--			WHERE
	--				OpportunityBizDocs.numOppId = OM.numOppId
	--				AND ISNULL(OpportunityBizDocs.numBizDocId,0) = @numDefaultSalesShippingDoc
	--				AND OpportunityBizDocItems.numOppItemID = OI.numoppitemtCode
	--		) AS TempFulFilled
	--		WHERE
	--			OM.numDomainId = @numDomainID
	--			AND OM.tintOppType = 1
	--			AND OM.tintOppStatus = 1
	--			AND ISNULL(OM.tintshipped,0) = 0
	--			AND UPPER(I.charItemType) = 'P'
	--			AND ISNULL(OI.bitDropShip,0) = 0
	--			AND ISNULL(OI.numUnitHour,0) <> ISNULL(TempFulFilled.PickPacked,0)
	--	) TEMP
	--END

	---------------------------- 12 Sales Orders to ship -----------------------------------

	--SELECT
	--	@SalesOrderToShip = COUNT(numOppId)
	--FROM
	--(
	--	SELECT DISTINCT
	--		OM.numOppId
	--	FROM
	--		OpportunityMaster OM
	--	INNER JOIN
	--		OpportunityItems OI
	--	ON
	--		OI.numOppId = OM.numOppId
	--	INNER JOIN
	--		Item I
	--	ON
	--		OI.numItemCode = I.numItemCode
	--	OUTER APPLY
	--	(
	--		SELECT
	--			SUM(OpportunityBizDocItems.numUnitHour) AS FulFilledQty
	--		FROM
	--			OpportunityBizDocs
	--		INNER JOIN
	--			OpportunityBizDocItems 
	--		ON
	--			OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
	--		WHERE
	--			OpportunityBizDocs.numOppId = OM.numOppId
	--			AND ISNULL(OpportunityBizDocs.numBizDocId,0) = 296
	--			AND ISNULL(OpportunityBizDocs.bitFulFilled,0) = 1
	--			AND OpportunityBizDocItems.numOppItemID = OI.numoppitemtCode
	--	) AS TempFulFilled
	--	WHERE
	--		OM.numDomainId = @numDomainID
	--		AND OM.tintOppType = 1
	--		AND OM.tintOppStatus = 1
	--		AND ISNULL(OM.tintshipped,0) = 0
	--		AND UPPER(I.charItemType) = 'P'
	--		AND ISNULL(OI.bitDropShip,0) = 0
	--		AND ISNULL(OI.numUnitHour,0) <> ISNULL(TempFulFilled.FulFilledQty,0)
	--) TEMP

	---------------------------- 13 Sales Orders to invoice -----------------------------------

	--SELECT
	--	@SalesOrderToInvoice = COUNT(numOppID)
	--FROM
	--(
	--	SELECT DISTINCT
	--		OM.numOppId
	--	FROM
	--		OpportunityMaster OM
	--	INNER JOIN
	--		OpportunityItems OI
	--	ON
	--		OI.numOppId = OM.numOppID
	--	INNER JOIN
	--		Item I
	--	ON
	--		OI.numItemCode = I.numItemCode
	--	OUTER APPLY
	--	(
	--		SELECT
	--			SUM(OpportunityBizDocItems.numUnitHour) AS InvoicedQty
	--		FROM
	--			OpportunityBizDocs
	--		INNER JOIN
	--			OpportunityBizDocItems 
	--		ON
	--			OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
	--		WHERE
	--			OpportunityBizDocs.numOppId = OM.numOppID
	--			AND OpportunityBizDocItems.numOppItemID = OI.numoppitemtCode
	--			AND OpportunityBizDocs.numBizDocId = (SELECT ISNULL(numAuthoritativeSales,0) FROM AuthoritativeBizDocs WHERE numDomainId=@numDomainID)
	--	) AS TempInvoice
	--	WHERE
	--		OM.numDomainId = @numDomainID
	--		AND OM.tintOppType = 1
	--		AND OM.tintOppStatus  = 1
	--		AND ISNULL(OI.numUnitHour,0) <> ISNULL(TempInvoice.InvoicedQty,0)
	--) TEMP

	---------------------------- 14 Sales Orders with items to purchase -----------------------------------

	DECLARE @TEMPOppID TABLE
	(
		numOppID NUMERIC(18,0)
	)

	INSERT INTO @TEMPOppID
	(
		numOppID
	)
	SELECT DISTINCT
		OM.numOppID
	FROM
		OpportunityMaster OM
	INNER JOIN
		OpportunityItems OI
	ON
		OM.numOppId = OI.numOppId
	LEFT JOIN
		OpportunityLinking OMInner
	ON
		OMInner.numParentOppID = OM.numOppID
	LEFT JOIN
		OpportunityItems OIInner 
	ON 
		OMInner.numChildOppID=OIInner.numOppId
		 AND OIInner.numItemCode=OI.numItemCode
		 AND ISNULL(OIInner.numWarehouseItmsID,0)=ISNULL(OI.numWarehouseItmsID,0)
	WHERE   
		OM.numDomainId = @numDomainId
		AND OM.tintOppType = 1
		AND OM.tintOppStatus=1
		AND ISNULL(OI.bitDropShip, 0) = 1
		AND OIInner.numoppitemtCode IS NULL


	If ISNULL(@bitReOrderPoint,0) = 1
	BEGIN
		INSERT INTO @TEMPOppID
		(
			numOppID
		)
		SELECT DISTINCT
			OM.numOppID
		FROM
			OpportunityMaster OM
		INNER JOIN
			OpportunityItems OI
		ON
			OI.numOppId = OM.numOppId
		INNER JOIN 
			Item I 
		ON 
			I.numItemCode = OI.numItemCode
		INNER JOIN 
			WarehouseItems WI 
		ON 
			OI.numWarehouseItmsID = WI.numWareHouseItemID
		WHERE   
			OM.numDomainId = @numDomainId
			AND ISNULL(@bitReOrderPoint,0) = 1
			AND (ISNULL(WI.numOnHand,0) + ISNULL(WI.numOnOrder,0) <= ISNULL(WI.numReOrder,0))
			AND OM.tintOppType = 1
			AND OM.tintOppStatus=1
			AND ISNULL(I.bitAssembly, 0) = 0
			AND ISNULL(I.bitKitParent, 0) = 0
			AND ISNULL(OI.bitDropship,0) = 0
			AND OM.numOppId NOT IN (SELECT numOppID FROM @TEMPOppID)
	END
	
	---------------------------- 15 Sales Orders past their release date -----------------------------------

	SELECT
		@SalesOrderPastTheirReleaseDate = COUNT(*)
	FROM
		OpportunityMaster OM
	WHERE
		numDomainId=@numDomainID
		AND tintOppType=1
		AND tintOppStatus=1
		AND ISNULL(OM.tintshipped,0) = 0
		AND dtReleaseDate IS NOT NULL
		AND dtReleaseDate < DateAdd(minute, -@ClientTimeZoneOffset,GETUTCDATE())
		AND (SELECT 
				COUNT(*) 
			FROM 
			(
				SELECT
					OI.numoppitemtCode,
					ISNULL(OI.numUnitHour,0) AS OrderedQty,
					ISNULL(TempFulFilled.FulFilledQty,0) AS FulFilledQty
				FROM
					OpportunityItems OI
				INNER JOIN
					Item I
				ON
					OI.numItemCode = I.numItemCode
				OUTER APPLY
				(
					SELECT
						SUM(OpportunityBizDocItems.numUnitHour) AS FulFilledQty
					FROM
						OpportunityBizDocs
					INNER JOIN
						OpportunityBizDocItems 
					ON
						OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
					WHERE
						OpportunityBizDocs.numOppId = OM.numOppId
						AND ISNULL(OpportunityBizDocs.numBizDocId,0) = 296
						AND ISNULL(OpportunityBizDocs.bitFulFilled,0) = 1
						AND OpportunityBizDocItems.numOppItemID = OI.numoppitemtCode
				) AS TempFulFilled
				WHERE
					OI.numOppID = OM.numOppID
					AND UPPER(I.charItemType) = 'P'
					AND ISNULL(OI.bitDropShip,0) = 0
			) X
			WHERE
				X.OrderedQty <> X.FulFilledQty) > 0

	---------------------------- 17 Items to buy where qty-on-hand is =< the reorder qty -----------------------------------
	 
	SELECT 
		@ItemOnHandLessThenReorder = COUNT(numItemID)
	FROM
	(
		SELECT DISTINCT
			numItemID
		FROM
			WareHouseItems
		INNER JOIN
			Item
		ON
			WareHouseItems.numItemID = Item.numItemCode
		WHERE
			WareHouseItems.numDomainID=@numDomainID
			AND Item.numDomainID = @numDomainID
			AND ISNULL(Item.bitArchiveItem,0) = 0
			AND ISNULL(Item.IsArchieve,0) = 0
			AND ISNULL(numReorder,0) > 0 
			AND ISNULL(numOnHand,0) < ISNULL(numReorder,0)
	) TEMP


	INSERT INTO @TEMP (ID,Value) VALUES (1,ISNULL(@TimeAndExpenseToReview,0))
	INSERT INTO @TEMP (ID,Value) VALUES (2,ISNULL(@PriceMarginToReview,0))
	INSERT INTO @TEMP (ID,Value) VALUES (3,ISNULL(@PromotionApproval,0))
	INSERT INTO @TEMP (ID,Value) VALUES (4,ISNULL(@SalesOppStagesToComplete,0))
	INSERT INTO @TEMP (ID,Value) VALUES (5,ISNULL(@ProjectStagesToComplete,0))
	INSERT INTO @TEMP (ID,Value) VALUES (6,ISNULL(@CasesToSolve,0))
	INSERT INTO @TEMP (ID,Value) VALUES (7,ISNULL(@PendingSalesRMA,0))
	INSERT INTO @TEMP (ID,Value) VALUES (8,ISNULL(@PaymentsToDeposite,0))
	INSERT INTO @TEMP (ID,Value) VALUES (9,ISNULL(@BillsToPay,0))
	INSERT INTO @TEMP (ID,Value) VALUES (10,ISNULL(@PurchaseOrderToBill,0))
	INSERT INTO @TEMP (ID,Value) VALUES (11,ISNULL(@SalesOrderToPickAndPack,0))
	INSERT INTO @TEMP (ID,Value) VALUES (12,ISNULL(@SalesOrderToShip,0))
	INSERT INTO @TEMP (ID,Value) VALUES (13,ISNULL(@SalesOrderToInvoice,0))
	INSERT INTO @TEMP (ID,Value) VALUES (14,ISNULL((SELECT COUNT(*) FROM @TEMPOppID),0))
	INSERT INTO @TEMP (ID,Value) VALUES (15,ISNULL(@SalesOrderPastTheirReleaseDate,0))
	INSERT INTO @TEMP (ID,Value) VALUES (16,0)
	INSERT INTO @TEMP (ID,Value) VALUES (17,ISNULL(@ItemOnHandLessThenReorder,0))
	INSERT INTO @TEMP (ID,Value) VALUES (18,ISNULL(@PurchaseOppStagesToComplete,0))
	INSERT INTO @TEMP (ID,Value) VALUES (19,ISNULL(@SalesOrderStagesToComplete,0))

	SELECT * FROM @TEMP
END
GO