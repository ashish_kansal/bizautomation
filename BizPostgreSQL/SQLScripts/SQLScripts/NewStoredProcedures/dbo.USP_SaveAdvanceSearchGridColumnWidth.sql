/****** Object:  StoredProcedure [dbo].[USP_SaveGridColumnWidth]    Script Date: 07/26/2008 16:14:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj  
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_SaveAdvanceSearchGridColumnWidth')
DROP PROCEDURE USP_SaveAdvanceSearchGridColumnWidth
GO
CREATE PROCEDURE [dbo].[USP_SaveAdvanceSearchGridColumnWidth]  
@numDomainID as numeric(9)=0,  
@numUserCntID as numeric(9)=0,
@str as text,
@numViewID int
as  
BEGIN
DECLARE @numGroupID AS NUMERIC(18,0)
SELECT @numGroupID=numGroupID FROM UserMaster WHERE numUserDetailId=@numUserCntID

declare @hDoc as int     
EXEC sp_xml_preparedocument @hDoc OUTPUT, @str                                                                        

Create table #tempTable(numFormId numeric(9),numFieldId numeric(9),bitCustom bit,intColumnWidth int)

  insert into #tempTable (numFormId,numFieldId,bitCustom,intColumnWidth)                                        
  SELECT * FROM OPENXML (@hDoc,'/NewDataSet/GridColumnWidth',2)                                                  
  WITH (numFormId numeric(9),numFieldId numeric(9),bitCustom bit,intColumnWidth int)                                           
     
UPDATE
	t1
SET 
	intColumnWidth=temp.intColumnWidth
FROM
	AdvSerViewConf t1
JOIN
	#tempTable temp
ON
	t1.numFormFieldID = temp.numFieldId 
WHERE
	t1.numFormId = 1
	AND temp.numFormId = 1
	AND t1.tintViewID = @numViewID
	AND t1.numDomainID = @numDomainID
	AND t1.numGroupID = @numGroupID      
	AND ISNULL(t1.bitCustom,0) = temp.bitCustom -- RIGHT NOW CUSTOM FIELDS ARE NOT AVAILABLE FOR SELECTION IN ADVANCE SEARCH GRID
	
	UPDATE 
		DFCD 
	SET 
		intColumnWidth=temp.intColumnWidth
	FROM 
		DycFormConfigurationDetails  DFCD 
	JOIN 
		#tempTable temp
	ON 
		DFCD.numFormId=temp.numFormId 
		AND DFCD.numFieldId=temp.numFieldId 
		AND isnull(DFCD.bitCustom,0)=temp.bitCustom
	WHERE 
		DFCD.numDomainID=@numDomainID 
		AND DFCD.numUserCntID=@numUserCntID 
		AND DFCD.tintPagetype=1
		AND temp.numFormId <> 1                                               

  drop table #tempTable                                        
 EXEC sp_xml_removedocument @hDoc  
END
GO