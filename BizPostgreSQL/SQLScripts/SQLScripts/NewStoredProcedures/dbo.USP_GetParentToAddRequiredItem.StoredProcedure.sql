SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetParentToAddRequiredItem')
DROP PROCEDURE USP_GetParentToAddRequiredItem
GO
CREATE PROCEDURE [dbo].[USP_GetParentToAddRequiredItem]
	@numDomainId NUMERIC(18,0)
AS
BEGIN
	DECLARE @numItemCode NUMERIC(18,0)

	SELECT TOP 1 @numItemCode = numItemCode FROM CartItems WHERE numDomainId = @numDomainId ORDER BY numCartId DESC
	--SELECT numItemCode FROM SimilarItems WHERE numParentItemCode = @numItemCode AND bitRequired = 1
	
	SELECT I.numItemCode, vcItemName FROM [Item] I
	LEFT JOIN [dbo].SimilarItems  AS SI ON I.[numItemCode] = SI.[numItemCode]
	WHERE numParentItemCode = @numItemCode AND bitRequired = 1 AND I.numDomainId = @numDomainId
END