GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_Domain_GetColumnValue')
DROP PROCEDURE USP_Domain_GetColumnValue
GO
CREATE PROCEDURE [dbo].[USP_Domain_GetColumnValue]
(
    @numDomainID AS NUMERIC(18,0),
	@vcDbColumnName AS VARCHAR(300)
)
AS 
BEGIN
	IF @vcDbColumnName = 'tintCommitAllocation'
	BEGIN
		SELECT ISNULL(tintCommitAllocation,1) AS tintCommitAllocation FROM Domain WHERE numDomainId=@numDomainID
	END
	ELSE IF @vcDbColumnName = 'bitEDI'
	BEGIN
		SELECT ISNULL(bitEDI,0) AS bitEDI FROM Domain WHERE numDomainId=@numDomainID
	END
	ELSE IF @vcDbColumnName = 'bitReOrderPoint'
	BEGIN
		SELECT ISNULL(bitReOrderPoint,0) AS bitReOrderPoint FROM Domain WHERE numDomainId=@numDomainID
	END
	ELSE IF @vcDbColumnName = 'tintMarkupDiscountOption'
	BEGIN
		SELECT ISNULL(tintMarkupDiscountOption,0) AS tintMarkupDiscountOption FROM Domain WHERE numDomainId=@numDomainID
	END
	ELSE IF @vcDbColumnName = 'tintMarkupDiscountValue'
	BEGIN
		SELECT ISNULL(tintMarkupDiscountValue,0) AS tintMarkupDiscountValue FROM Domain WHERE numDomainId=@numDomainID
	END
	ELSE IF @vcDbColumnName = 'bitDoNotShowDropshipPOWindow'
	BEGIN
		SELECT ISNULL(bitDoNotShowDropshipPOWindow,0) AS bitDoNotShowDropshipPOWindow FROM Domain WHERE numDomainId=@numDomainID
	END
	ELSE IF @vcDbColumnName = 'tintReceivePaymentTo'
	BEGIN
		SELECT ISNULL(tintReceivePaymentTo,0) AS tintReceivePaymentTo FROM Domain WHERE numDomainId=@numDomainID
	END
	ELSE IF @vcDbColumnName = 'numReceivePaymentBankAccount'
	BEGIN
		SELECT ISNULL(numReceivePaymentBankAccount,0) AS numReceivePaymentBankAccount FROM Domain WHERE numDomainId=@numDomainID
	END
	ELSE IF @vcDbColumnName = 'bitCloneLocationWithItem'
	BEGIN
		SELECT ISNULL(bitCloneLocationWithItem,0) AS bitCloneLocationWithItem FROM Domain WHERE numDomainId=@numDomainID
	END
	ELSE IF @vcDbColumnName = 'vcBluePaySuccessURL'
	BEGIN
		SELECT ISNULL(vcBluePaySuccessURL,0) AS vcBluePaySuccessURL FROM Domain WHERE numDomainId=@numDomainID
	END
	ELSE IF @vcDbColumnName = 'vcBluePayDeclineURL'
	BEGIN
		SELECT ISNULL(vcBluePayDeclineURL,0) AS vcBluePayDeclineURL FROM Domain WHERE numDomainId=@numDomainID
	END
	ELSE IF @vcDbColumnName = 'bitUseDeluxeCheckStock'
	BEGIN
		SELECT ISNULL(bitUseDeluxeCheckStock,0) AS bitUseDeluxeCheckStock FROM Domain WHERE numDomainId=@numDomainID
	END
	ELSE IF @vcDbColumnName = 'vcPOHiddenColumns'
	BEGIN
		SELECT ISNULL(vcPOHiddenColumns,0) AS vcPOHiddenColumns FROM SalesOrderConfiguration WHERE numDomainId=@numDomainID
	END
	ELSE IF @vcDbColumnName = 'tintReorderPointBasedOn'
	BEGIN
		SELECT ISNULL(tintReorderPointBasedOn,1) AS tintReorderPointBasedOn FROM Domain WHERE numDomainId=@numDomainID
	END
	ELSE IF @vcDbColumnName = 'intReorderPointDays'
	BEGIN
		SELECT ISNULL(intReorderPointDays,30) AS intReorderPointDays FROM Domain WHERE numDomainId=@numDomainID
	END
	ELSE IF @vcDbColumnName = 'intReorderPointPercent'
	BEGIN
		SELECT ISNULL(intReorderPointPercent,0) AS intReorderPointPercent FROM Domain WHERE numDomainId=@numDomainID
	END
	ELSE IF @vcDbColumnName = 'bitPurchaseTaxCredit'
	BEGIN
		SELECT ISNULL(bitPurchaseTaxCredit,0) AS intReorderPointPercent FROM Domain WHERE numDomainId=@numDomainID
	END
END
GO