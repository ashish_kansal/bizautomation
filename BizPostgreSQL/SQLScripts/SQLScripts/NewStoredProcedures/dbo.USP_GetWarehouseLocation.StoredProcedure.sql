GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_GetWarehouseLocation' ) 
    DROP PROCEDURE USP_GetWarehouseLocation
GO
-- EXEC USP_GetWarehouseLocation 1 , 58 , 11
CREATE PROCEDURE [dbo].USP_GetWarehouseLocation
    @numDomainID AS NUMERIC(9) ,
    @numWarehouseID AS NUMERIC(8),
    @numWLocationID AS numeric(9) = 0
AS 
    BEGIN
 
        SELECT  numWLocationID ,
                numWarehouseID ,
                vcAisle ,
                vcRack ,
                vcShelf ,
                vcBin ,
                vcLocation ,
                bitDefault ,
                bitSystem,
                intQty 
               
        FROM    dbo.WarehouseLocation
        WHERE   (numWarehouseID = @numWarehouseID   OR ISNULL(@numWarehouseID,0) = 0)
                AND numDomainID = @numDomainID
                AND (numWLocationID=@numWLocationID OR ISNULL(@numWLocationID,0) = 0)
 	
 	
    END
 
 GO 