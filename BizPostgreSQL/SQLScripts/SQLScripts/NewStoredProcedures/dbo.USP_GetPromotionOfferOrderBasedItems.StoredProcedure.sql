SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetPromotionOfferOrderBasedItems')
	DROP PROCEDURE USP_GetPromotionOfferOrderBasedItems
GO
CREATE PROCEDURE [dbo].[USP_GetPromotionOfferOrderBasedItems]  
	@numProId AS NUMERIC(18,0),
	@numDomainID AS NUMERIC(18,0),
	@numSiteId AS NUMERIC(18,0)  
AS
BEGIN
	DECLARE @vcItemName VARCHAR(200), @numBaseUnit VARCHAR(100), @monListPrice VARCHAR(100)

	SELECT vcProName FROM PromotionOffer WHERE numProId = @numProId AND numDomainId = @numDomainID AND ISNULL(bitEnabled,1) = 1

	CREATE TABLE #temp 
	(
		ID int IDENTITY (1, 1) NOT NULL ,
		numProOrderBasedID numeric(18,0),
		numProId numeric(18,0),
		numDomainID NUMERIC(18,0),
		cSaleType  CHAR,
		cItems  CHAR,
		fltSubTotal  FLOAT
	)

	CREATE TABLE #temptbl 
	(
		ID int IDENTITY (1, 1) NOT NULL ,
		numProOrderBasedID numeric(18,0),
		numProId numeric(18,0),
		numDomainID NUMERIC(18,0),
		cSaleType  CHAR,
		cItems  CHAR,
		fltSubTotal  FLOAT,
		Items VARCHAR(200),
		numBaseUnit VARCHAR(100),
		monListPrice  VARCHAR(100)
	)

	Declare @Id int, @tot float

	insert into #temp
		(numProOrderBasedID, numProId, numDomainID, cSaleType, cItems, fltSubTotal)
	(select numProOrderBasedID, numProId, numDomainID, cSaleType, cItems, fltSubTotal FROM PromotionOfferOrderBased WHERE numProId = @numProId AND numDomainId = @numDomainID AND ISNULL(bitEnabled,1) = 1)

	insert into #temptbl
		(numProOrderBasedID, numProId, numDomainID, cSaleType, cItems, fltSubTotal)
	(select numProOrderBasedID, numProId, numDomainID, cSaleType, cItems, fltSubTotal FROM PromotionOfferOrderBased WHERE numProId = @numProId AND numDomainId = @numDomainID AND ISNULL(bitEnabled,1) = 1)
	
		While (Select Count(*) FROM #temp WHERE cSaleType = 'I' AND cItems = 'M') > 0
		BEGIN
			
			Select Top 1  @Id = Id, @tot = fltSubTotal From #temp WHERE cSaleType = 'I' AND cItems = 'M'

			SET @vcItemName = CONCAT('',(SELECT Stuff((SELECT CONCAT(', ',Item.vcItemName) FROM PromotionOfferOrderBased INNER JOIN Item ON PromotionOfferOrderBased.numProItemId = Item.numItemCode WHERE numProId = @numProId AND PromotionOfferOrderBased.numDomainID = @numDomainID AND cItems = 'M' AND fltSubTotal = @tot FOR XML PATH('')), 1, 2, '')), '') 
			SET @numBaseUnit = CONCAT('',(SELECT Stuff((SELECT CONCAT(', ',ISNULL(Item.numBaseUnit,0)) FROM PromotionOfferOrderBased INNER JOIN Item ON PromotionOfferOrderBased.numProItemId = Item.numItemCode WHERE numProId = @numProId AND PromotionOfferOrderBased.numDomainID = @numDomainID AND cItems = 'M' AND fltSubTotal = @tot FOR XML PATH('')), 1, 2, '')), '') 
			SET @monListPrice = CONCAT('',(SELECT Stuff((SELECT CONCAT(', ',ISNULL(Item.monListPrice,0)) FROM PromotionOfferOrderBased INNER JOIN Item ON PromotionOfferOrderBased.numProItemId = Item.numItemCode WHERE numProId = @numProId AND PromotionOfferOrderBased.numDomainID = @numDomainID AND cItems = 'M' AND fltSubTotal = @tot FOR XML PATH('')), 1, 2, '')), '') 
	
			UPDATE #temptbl
				SET Items = @vcItemName, numBaseUnit = @numBaseUnit, monListPrice = @monListPrice
			WHERE Id = @Id

			Delete #temp Where Id = @Id
		END
		While (Select Count(*) FROM #temp WHERE cSaleType = 'I' AND cItems = 'S') > 0
		BEGIN
			Select Top 1  @Id = Id,  @tot = fltSubTotal From #temp WHERE cSaleType = 'I' AND cItems = 'S'

			SELECT @vcItemName = Item.vcItemName FROM PromotionOfferOrderBased INNER JOIN Item ON PromotionOfferOrderBased.numProItemId = Item.numItemCode WHERE numProId = @numProId AND PromotionOfferOrderBased.numDomainID = @numDomainID AND cItems = 'S' AND fltSubTotal = @tot
			SELECT @numBaseUnit = ISNULL(Item.numBaseUnit,0) FROM PromotionOfferOrderBased INNER JOIN Item ON PromotionOfferOrderBased.numProItemId = Item.numItemCode WHERE numProId = @numProId AND PromotionOfferOrderBased.numDomainID = @numDomainID AND cItems = 'S' AND fltSubTotal = @tot
			SELECT @monListPrice = ISNULL(Item.monListPrice,0) FROM PromotionOfferOrderBased INNER JOIN Item ON PromotionOfferOrderBased.numProItemId = Item.numItemCode WHERE numProId = @numProId AND PromotionOfferOrderBased.numDomainID = @numDomainID AND cItems = 'S' AND fltSubTotal = @tot
	
			UPDATE #temptbl
				SET Items = @vcItemName, numBaseUnit = @numBaseUnit, monListPrice = @monListPrice
			WHERE Id = @Id

			Delete #temp Where Id = @Id
		END

		SELECT DISTINCT(fltSubTotal), Items, numBaseUnit, monListPrice, cSaleType FROM #temptbl ORDER BY fltSubTotal

		--Table 3
		DECLARE @numWareHouseID AS NUMERIC(9), @UOMConversionFactor AS DECIMAL(18,5)
		SELECT @numWareHouseID = ISNULL(numDefaultWareHouseID,0) FROM [eCommerceDTL]  WHERE [numDomainID] = @numDomainID AND numSiteId = @numSiteId
		 
      
		SELECT @UOMConversionFactor = ISNULL(dbo.fn_UOMConversion(ISNULL(I.numSaleUnit,0),I.numItemCode,I.numDomainId,ISNULL(I.numBaseUnit,0)),0)
		FROM Item I 
		JOIN PromotionOfferOrderBased POB ON POB.numProItemId = I.numItemCode 
		WHERE numProId = @numProId AND POB.numDomainID = @numDomainID AND ISNULL(POB.bitEnabled,1) = 1

		SELECT I.vcItemName, numItemCode, fltSubTotal
			,ISNULL(CASE WHEN I.[charItemType]='P'
			THEN @UOMConversionFactor * W.[monWListPrice]  
			ELSE @UOMConversionFactor * monListPrice 
	   END,0) AS monListPrice
		FROM PromotionOfferOrderBased 
			INNER JOIN Item I ON PromotionOfferOrderBased.numProItemId = I.numItemCode 
			LEFT JOIN  WareHouseItems W  ON W.numItemID = I.numItemCode AND numWareHouseID = @numWareHouseID
		WHERE numProId = @numProId AND PromotionOfferOrderBased.numDomainID = @numDomainID AND ISNULL(PromotionOfferOrderBased.bitEnabled,1) = 1
END 
GO










/*
	IF ((SELECT COUNT(*) FROM PromotionOfferOrderBased WHERE numProId = @numProId AND numDomainId = @numDomainID ) > 1)
	BEGIN
		SET @vcItemName = CONCAT('',(SELECT Stuff((SELECT CONCAT(', ',Item.vcItemName) FROM PromotionOfferOrderBased INNER JOIN Item ON PromotionOfferOrderBased.numProItemId = Item.numItemCode WHERE numProId = @numProId AND PromotionOfferOrderBased.numDomainID = @numDomainID FOR XML PATH('')), 1, 2, '')), '') 
		SET @numBaseUnit = CONCAT('',(SELECT Stuff((SELECT CONCAT(', ',ISNULL(Item.numBaseUnit,0)) FROM PromotionOfferOrderBased INNER JOIN Item ON PromotionOfferOrderBased.numProItemId = Item.numItemCode WHERE numProId = @numProId AND PromotionOfferOrderBased.numDomainID = @numDomainID FOR XML PATH('')), 1, 2, '')), '') 
		SET @monListPrice = CONCAT('',(SELECT Stuff((SELECT CONCAT(', ',ISNULL(Item.monListPrice,0)) FROM PromotionOfferOrderBased INNER JOIN Item ON PromotionOfferOrderBased.numProItemId = Item.numItemCode WHERE numProId = @numProId AND PromotionOfferOrderBased.numDomainID = @numDomainID FOR XML PATH('')), 1, 2, '')), '') 
	END
	ELSE IF EXISTS(SELECT * FROM PromotionOfferOrderBased WHERE numProId = @numProId AND numDomainId = @numDomainID AND cSaleType = 'I' AND cItems = 'S' ) 
	BEGIN
		SELECT @vcItemName = Item.vcItemName FROM PromotionOfferOrderBased INNER JOIN Item ON PromotionOfferOrderBased.numProItemId = Item.numItemCode WHERE numProId = @numProId AND PromotionOfferOrderBased.numDomainID = @numDomainID
		SELECT @numBaseUnit = ISNULL(Item.numBaseUnit,0) FROM PromotionOfferOrderBased INNER JOIN Item ON PromotionOfferOrderBased.numProItemId = Item.numItemCode WHERE numProId = @numProId AND PromotionOfferOrderBased.numDomainID = @numDomainID
		SELECT @monListPrice = ISNULL(Item.monListPrice,0) FROM PromotionOfferOrderBased INNER JOIN Item ON PromotionOfferOrderBased.numProItemId = Item.numItemCode WHERE numProId = @numProId AND PromotionOfferOrderBased.numDomainID = @numDomainID
	END

	SELECT numProOrderBasedID
		,fltSubTotal
		,cSaleType
		,@vcItemName AS Items
		,@numBaseUnit AS numBaseUnit
		,@monListPrice AS monListPrice
	FROM PromotionOfferOrderBased  
	WHERE numProId = @numProId
		AND numDomainId = @numDomainID 
END 
GO

*/


/*
SET @vcItemName = (SELECT CASE 
							WHEN numProItemId <> NULL AND ((SELECT COUNT(*) FROM PromotionOfferOrderBased WHERE numProId = @numProId AND numDomainId = @numDomainID ) > 1)
								THEN CONCAT('',(SELECT Stuff((SELECT CONCAT(', ',Item.vcItemName) FROM PromotionOfferOrderBased INNER JOIN Item ON PromotionOfferOrderBased.numProItemId = Item.numItemCode WHERE numProId = @numProId AND PromotionOfferOrderBased.numDomainID = @numDomainID FOR XML PATH('')), 1, 2, '')), '') 
							ELSE
								(SELECT Item.vcItemName FROM PromotionOfferOrderBased INNER JOIN Item ON PromotionOfferOrderBased.numProItemId = Item.numItemCode WHERE numProId = @numProId AND PromotionOfferOrderBased.numDomainID = @numDomainID)
							END
								FROM PromotionOfferOrderBased  
								WHERE numProId = @numProId
									AND numDomainId = @numDomainID )

	SET @numBaseUnit = (SELECT CASE 
							WHEN numProItemId <> NULL AND ((SELECT COUNT(*) FROM PromotionOfferOrderBased WHERE numProId = @numProId AND numDomainId = @numDomainID ) > 1)
								THEN CONCAT('',(SELECT Stuff((SELECT CONCAT(', ',Item.numBaseUnit) FROM PromotionOfferOrderBased INNER JOIN Item ON PromotionOfferOrderBased.numProItemId = Item.numItemCode WHERE numProId = @numProId AND PromotionOfferOrderBased.numDomainID = @numDomainID FOR XML PATH('')), 1, 2, '')), '') 
							ELSE
								(SELECT Item.numBaseUnit FROM PromotionOfferOrderBased INNER JOIN Item ON PromotionOfferOrderBased.numProItemId = Item.numItemCode WHERE numProId = @numProId AND PromotionOfferOrderBased.numDomainID = @numDomainID)
							END
								FROM PromotionOfferOrderBased  
								WHERE numProId = @numProId
									AND numDomainId = @numDomainID )

	SET @monListPrice = (SELECT CASE 
							WHEN numProItemId <> NULL AND ((SELECT COUNT(*) FROM PromotionOfferOrderBased WHERE numProId = @numProId AND numDomainId = @numDomainID ) > 1)
								THEN CONCAT('',(SELECT Stuff((SELECT CONCAT(', ',Item.monListPrice) FROM PromotionOfferOrderBased INNER JOIN Item ON PromotionOfferOrderBased.numProItemId = Item.numItemCode WHERE numProId = @numProId AND PromotionOfferOrderBased.numDomainID = @numDomainID FOR XML PATH('')), 1, 2, '')), '') 
							ELSE
								(SELECT Item.monListPrice FROM PromotionOfferOrderBased INNER JOIN Item ON PromotionOfferOrderBased.numProItemId = Item.numItemCode WHERE numProId = @numProId AND PromotionOfferOrderBased.numDomainID = @numDomainID)
			
							END
								FROM PromotionOfferOrderBased  
								WHERE numProId = @numProId
									AND numDomainId = @numDomainID )
*/