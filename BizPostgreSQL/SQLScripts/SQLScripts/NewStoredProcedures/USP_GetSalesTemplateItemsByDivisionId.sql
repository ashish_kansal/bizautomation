--EXEC USP_GetSalesTemplateItems 6,72
--SELECT * FROM SalesTemplateItems
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetSalesTemplateItemsByDivisionId')
DROP PROCEDURE USP_GetSalesTemplateItemsByDivisionId
GO
CREATE PROCEDURE [dbo].[USP_GetSalesTemplateItemsByDivisionId]
    @numDivisionID NUMERIC,
    @numDomainID NUMERIC
AS 
    SELECT  X.*, (SELECT TOP 1 vcCategoryName FROM [Category] WHERE numCategoryID IN (SELECT numCategoryID FROM [ItemCategory] IC WHERE X.numItemCode = IC.[numItemID])) AS vcCategoryName
    FROM    ( SELECT  DISTINCT
                        STI.[numSalesTemplateItemID],
                        STI.[numSalesTemplateID],
                        STI.[numSalesTemplateItemID] AS numoppitemtCode,
						ST.vcTemplateName AS vcTemplateName,
                        STI.[numItemCode],
                        STI.[numUnitHour],
                        STI.[monPrice],
                        STI.[monTotAmount],
                        STI.[numSourceID],
                        STI.[numWarehouseID],
                        STI.[Warehouse],
                        STI.[numWarehouseItmsID],
                        STI.[ItemType],
                        STI.[ItemType] as vcType,
                        STI.[Attributes],
						STI.[Attributes] as vcAttributes,
                        STI.[AttrValues],
                        I.[bitFreeShipping] FreeShipping, --Use from Item table
                        STI.[Weight],
                        STI.[Op_Flag],
                        STI.[bitDiscountType],
                        STI.[fltDiscount],
                        STI.[monTotAmtBefDiscount],
                        STI.[ItemURL],
                        STI.[numDomainID],
                        I.txtItemDesc AS vcItemDesc,
                       (SELECT TOP 1 vcPathForTImage FROM dbo.ItemImages II WHERE II.numItemCode = I.numItemCode AND II.numDomainId= I.numDomainId AND II.bitDefault=1) vcPathForTImage,
                        I.vcItemName,
                        'False' AS DropShip,
					    0 as bitDropShip,
                        I.[bitTaxable],
                        ISNULL(STI.numUOM,0) numUOM,
						ISNULL(STI.numUOM,0) as numUOMId,
                        ISNULL(STI.vcUOMName,'') vcUOMName,
                        ISNULL(STI.UOMConversionFactor,1) UOMConversionFactor,
						ISNULL(STI.numVendorWareHouse,0) as numVendorWareHouse,ISNULL(STI.numShipmentMethod,0) as numShipmentMethod,ISNULL(STI.numSOVendorId,0) as numSOVendorId,
						0 as bitWorkOrder,I.charItemType,dbo.fn_GetUOMName(I.numBaseUnit) AS vcBaseUOMName,0 AS numSortOrder
--                        ISNULL(dbo.fn_UOMConversion(I.numBaseUnit,I.numItemCode,I.numdomainid,0),1) UOMConversionFactor
              FROM      SalesTemplateItems STI
						LEFT JOIN OpportunitySalesTemplate AS ST ON STI.numSalesTemplateID=ST.numSalesTemplateID
                        INNER JOIN item I ON I.numItemCode = STI.numItemCode
                        LEFT JOIN dbo.UOM U ON U.numUOMID = I.numSaleUnit
              WHERE     ST.numDivisionID=@numDivisionID
                        AND STI.[numDomainID] = @numDomainID
            ) X