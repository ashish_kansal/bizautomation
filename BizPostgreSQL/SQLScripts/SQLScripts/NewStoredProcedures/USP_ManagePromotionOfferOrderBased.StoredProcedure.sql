SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ManagePromotionOfferOrderBased')
DROP PROCEDURE USP_ManagePromotionOfferOrderBased
GO
CREATE PROCEDURE [dbo].[USP_ManagePromotionOfferOrderBased]
	@numProId AS NUMERIC(9) = 0,
	@vcProName AS VARCHAR(100),
	@numDomainId AS NUMERIC(18,0),
	@numUserCntID NUMERIC(18,0)
AS 
BEGIN
	IF @numProId = 0 
	BEGIN
		INSERT INTO PromotionOffer
        (
            vcProName, numDomainId, numCreatedBy, dtCreated, bitEnabled
        )
        VALUES  
		(
			@vcProName, @numDomainId, @numUserCntID, GETUTCDATE(), 1
        )
            
		SELECT SCOPE_IDENTITY()
	END
END
