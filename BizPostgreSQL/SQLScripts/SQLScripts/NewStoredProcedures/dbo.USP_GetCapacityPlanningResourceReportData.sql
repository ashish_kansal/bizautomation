GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
                    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetCapacityPlanningResourceReportData')
DROP PROCEDURE USP_GetCapacityPlanningResourceReportData
GO
CREATE PROCEDURE [dbo].[USP_GetCapacityPlanningResourceReportData]                             
	@numDomainID AS NUMERIC(18,0)
	,@vcTeams VARCHAR(200)
	,@vcEmployees VARCHAR(200)
	,@tintProcessType TINYINT
	,@numProcessID NUMERIC(18,0)
	,@vcMilestone VARCHAR(300)
	,@numStageDetailsId NUMERIC(18,0)
	,@vcTaskIDs VARCHAR(200)
	,@dtFromDate DATETIME
	,@dtToDate DATETIME
	,@tintView TINYINT --1:Day, 2:Week, 3:Month
	,@ClientTimeZoneOffset INT
AS                            
BEGIN
	SELECT
		numListItemID
		,ISNULL(vcData,'-') vcData
	FROM
		ListDetails 
	WHERE
		numDomainID = @numDomainID
		AND numListID = 35
		AND (ISNULL(@vcTeams,'') = '' OR numListItemID IN (SELECT Id FROM dbo.SplitIDs(@vcTeams,','))) 

	SELECT 
		ACI.numContactId
		,CONCAT(ACI.vcFirstName,' ',ACI.vcLastName) vcContactName
		,ISNULL(ACI.numTeam,0) numTeam 
	FROM
		UserMaster UM
	INNER JOIN
		AdditionalContactsInformation ACI
	ON
		UM.numUserDetailId = ACI.numContactId
	WHERE
		UM.numDomainID = @numDomainID
		AND ACI.numDomainID = @numDomainID
		AND ISNULL(UM.bitActivateFlag,0) = 1
		AND (ISNULL(@vcTeams,'') = '' OR numTeam IN (SELECT Id FROM dbo.SplitIDs(@vcTeams,','))) 
		AND (ISNULL(@vcEmployees,'') = '' OR numContactId IN (SELECT Id FROM dbo.SplitIDs(@vcEmployees,',')))  

	DECLARE @EmployeeTime TABLE
	(	
		numUserCntID NUMERIC(18,0)
		,numRecordID NUMERIC(18,0)
		,tintRecordType TINYINT
		,vcSource VARCHAR(200)
		,dtDate DATE
		,vcDate VARCHAR(50)
		,numType TINYINT
		,vcType VARCHAR(500)
		,vcHours VARCHAR(20)
		,numMinutes INT
		,vcDetails VARCHAR(MAX)
	)

	-- Time & Expense
	INSERT INTO @EmployeeTime
	(
		numUserCntID
		,numRecordID
		,tintRecordType
		,vcSource
		,dtDate
		,vcDate
		,numType
		,vcType
		,vcHours
		,numMinutes
		,vcDetails
	)
	SELECT
		TimeAndExpense.numUserCntID
		,TimeAndExpense.numCategoryHDRID
		,1
		,'Time & Expense' AS vcSource
		,dtFromDate
		,dbo.FormatedDateFromDate(DATEADD(MINUTE,@ClientTimeZoneOffset * -1,dtFromDate),@numDomainID) AS vcDate
		,TimeAndExpense.numType
		,(CASE 
			WHEN TimeAndExpense.numType = 4 THEN 'Absence (Un-Paid)' 
			WHEN TimeAndExpense.numType = 3 THEN 'Absence (Paid)' 
			WHEN TimeAndExpense.numType = 2 Then 'Non-Billable Time' 
			WHEN TimeAndExpense.numType = 1 Then 'Billable' 
			ELSE '' 
		END) AS vcType
		,CONVERT(varchar(5),DATEADD(MINUTE,DATEDIFF(MINUTE,TimeAndExpense.dtFromDate,TimeAndExpense.dtToDate),0),108) vcHours
		,DATEDIFF(MINUTE,TimeAndExpense.dtFromDate,TimeAndExpense.dtToDate) numMinutes
		,ISNULL(TimeAndExpense.txtDesc,'') vcDetails
	FROM
		TimeAndExpense 
	LEFT JOIN
		OpportunityMaster
	ON
		TimeAndExpense.numOppID=OpportunityMaster.numOppID
	WHERE
		TimeAndExpense.numDomainID = @numDomainID
		AND ISNULL(numCategory,0) = 1
		AND numType NOT IN (4,7) -- DO NOT INCLUDE UNPAID LEAVE
		AND dtFromDate BETWEEN @dtFromDate AND @dtToDate
	ORDER BY
		dtFromDate

	-- Action Item
	INSERT INTO @EmployeeTime
	(
		numUserCntID
		,numRecordID
		,tintRecordType
		,vcSource
		,dtDate
		,vcDate
		,numType
		,vcType
		,vcHours
		,numMinutes
		,vcDetails
	)
	SELECT
		Communication.numAssign
		,Communication.numCommId
		,2
		,CONCAT('<a target="_blank" href="../admin/actionitemdetails.aspx?CommId=',Communication.numCommId,'&lngType=0">'
				,dbo.GetListIemName(bitTask),'</a>',(CASE WHEN DM.numDivisionID > 0 THEN CONCAT(' (<a target="_blank" href="',(CASE WHEN DM.tintCRMType = 2 THEN '../account/frmAccounts.aspx?DivID=' WHEN DM.tintCRMType = 1 THEN '../prospects/frmProspects.aspx?DivID=' ELSE '../Leads/frmLeads.aspx?DivID=' END),Dm.numDivisionID,'">',ISNULL(CI.vcCompanyName,'-'),'</a>)') ELSE '' END)
		) AS vcSource
		,dtStartTime
		,dbo.FormatedDateFromDate(DATEADD(MINUTE,@ClientTimeZoneOffset * -1,Communication.dtStartTime),@numDomainID) AS vcDate
		,0
		,dbo.GetListIemName(bitTask) AS vcType
		,CONVERT(varchar(5),DATEADD(MINUTE,DATEDIFF(MINUTE,Communication.dtStartTime,Communication.dtEndTime),0),108) vcHours
		,DATEDIFF(MINUTE,Communication.dtStartTime,Communication.dtEndTime) numMinutes
		,ISNULL(Communication.textDetails,'') vcDetails
	FROM
		Communication 
	LEFT JOIN
		DivisionMaster DM
	ON
		Communication.numDivisionId = DM.numDivisionID
	LEFT JOIN
		CompanyInfo CI
	ON
		DM.numCompanyID = CI.numCompanyId
	WHERE
		Communication.numDomainID = @numDomainID
		AND Communication.dtStartTime BETWEEN @dtFromDate AND @dtToDate
		AND ISNULL(bitClosedFlag,0) = 1
	ORDER BY
		dtStartTime

	-- Calendar
	INSERT INTO @EmployeeTime
	(
		numUserCntID
		,numRecordID
		,tintRecordType
		,vcSource
		,dtDate
		,vcDate
		,numType
		,vcType
		,vcHours
		,numMinutes
		,vcDetails
	)
	SELECT
		[Resource].numUserCntId
		,Activity.ActivityID
		,3
		,CONCAT('<a target="_blank" href="../admin/actionitemdetails.aspx?CommId=',Activity.ActivityID,'&lngType=1">','Action Item','</a>') AS vcSource
		,Activity.StartDateTimeUtc
		,dbo.FormatedDateFromDate(DATEADD(MINUTE,@ClientTimeZoneOffset * -1,Activity.StartDateTimeUtc),@numDomainID) AS vcDate
		,0
		,'Calendar' AS vcType
		,CONVERT(varchar(5),DATEADD(MINUTE,DATEDIFF(MINUTE,Activity.StartDateTimeUtc,DATEADD(SECOND,Activity.Duration,Activity.StartDateTimeUtc)),0),108) vcHours
		,DATEDIFF(MINUTE,Activity.StartDateTimeUtc,DATEADD(SECOND,Activity.Duration,Activity.StartDateTimeUtc)) numMinutes
		,ISNULL(Activity.ActivityDescription,'') vcDetails
	FROM
		[Resource]
	INNER JOIN
		ActivityResource 
	ON
		[Resource].ResourceID = ActivityResource.ResourceID
	INNER JOIN
		Activity 
	ON
		ActivityResource.ActivityID=Activity.ActivityID
	WHERE
		Activity.StartDateTimeUtc BETWEEN @dtFromDate AND @dtToDate
	ORDER BY
		Activity.StartDateTimeUtc


	DECLARE @TempTaskAssignee TABLE
	(
		numWorkOrderID NUMERIC(18,0)
		,numProjectID NUMERIC(18,0)
		,numAssignedTo NUMERIC(18,0)			
		,dtLastTaskCompletionTime DATETIME
	)

	DECLARE @TempTimeLog TABLE
	(
		numWrokOrder NUMERIC(18,0)
		,numProjectID NUMERIC(18,0)
		,numTaskID NUMERIC(18,0)
		,numReferenceTaskId NUMERIC(18,0)
		,numQtyItemsReq FLOAT
		,numAssignTo NUMERIC(18,0)
		,numProductiveMinutes NUMERIC(18,0)
		,dtDate DATETIME
		,numTotalMinutes NUMERIC(18,0)
	)

	DECLARE @TempTasks TABLE
	(
		ID INT IDENTITY(1,1)
		,numTaskID NUMERIC(18,0)
		,numReferenceTaskId NUMERIC(18,0)
		,numWorkOrderID NUMERIC(18,0)
		,numProjectID NUMERIC(18,0)
		,numAssignTo NUMERIC(18,0)
		,numTaskTimeInMinutes NUMERIC(18,0)
		,intTaskType INT
		,dtStartDate DATETIME
		,dtFinishDate DATETIME
		,numQtyItemsReq FLOAT
		,numProcessedQty FLOAT
	)

	INSERT INTO @TempTasks
	(
		numTaskID
		,numReferenceTaskId
		,numWorkOrderID
		,numProjectID
		,numAssignTo
		,numTaskTimeInMinutes
		,intTaskType
		,dtStartDate
		,numQtyItemsReq
		,numProcessedQty
	)
	SELECT
		SPDT.numTaskId
		,SPDT.numReferenceTaskId
		,WorkOrder.numWOId
		,ProjectsMaster.numProId
		,SPDT.numAssignTo
		,((ISNULL(SPDT.numHours,0) * 60) + ISNULL(SPDT.numMinutes,0)) * (ISNULL(numQtyItemsReq,0) - ISNULL((SELECT SUM(SPDTTL.numProcessedQty) FROM StagePercentageDetailsTaskTimeLog SPDTTL WHERE SPDTTL.numTaskId = SPDT.numTaskId),0))
		,SPDT.intTaskType
		,(CASE	
			WHEN (SELECT COUNT(*) FROM StagePercentageDetailsTaskTimeLog SPDTTL WHERE SPDTTL.numTaskID = SPDT.numTaskId) > 0
			THEN (SELECT MAX(dtActionTime) FROM StagePercentageDetailsTaskTimeLog SPDTTL WHERE SPDTTL.numTaskID = SPDT.numTaskId)
			ELSE ISNULL(WorkOrder.dtmStartDate,WorkOrder.bintCreatedDate)
		END)
		,(CASE WHEN WorkOrder.numWOId IS NOT NULL THEN WorkOrder.numQtyItemsReq ELSE 1 END)
		,ISNULL((SELECT SUM(numProcessedQty) FROM StagePercentageDetailsTaskTimeLog SPDTTL WHERE SPDTTL.numTaskID = SPDT.numTaskId),0)
	FROM
		StagePercentageDetailsTask SPDT
	LEFT JOIN
		WorkOrder
	ON
		SPDT.numWorkOrderId = WorkOrder.numWOId
	LEFT JOIN
		ProjectsMaster
	ON
		SPDT.numProjectId = ProjectsMaster.numProId
	WHERE
		SPDT.numDomainID=@numDomainID
		AND (SELECT COUNT(*) FROM StagePercentageDetailsTaskTimeLog SPDTTL WHERE SPDTTL.numTaskID = SPDT.numTaskId AND tintAction=4) = 0 -- Task in not finished
		AND (ISNULL(numWorkOrderId,0) > 0 OR ISNULL(numProId,0) > 0)
		--TODO: Uncomment if there is performance issue on production
		--AND ((ISNULL(WorkOrder.dtmStartDate,WorkOrder.bintCreatedDate) BETWEEN @dtFromDate AND @dtToDate AND (ISNULL(numQtyItemsReq,0) - ISNULL((SELECT SUM(SPDTTL.numProcessedQty) FROM StagePercentageDetailsTaskTimeLog SPDTTL WHERE SPDTTL.numTaskId = SPDT.numTaskId),0)) > 0) 
		--		OR (ISNULL(ProjectsMaster.dtmStartDate,ProjectsMaster.bintCreatedDate) BETWEEN @dtFromDate AND @dtToDate)
		--		OR (SELECT COUNT(*) FROM StagePercentageDetailsTaskTimeLog SPDTTL WHERE SPDTTL.numTaskID = SPDT.numTaskId) > 0)

	INSERT INTO @TempTaskAssignee
	(
		numWorkOrderID
		,numProjectID
		,numAssignedTo
	)
	SELECT
		numWorkOrderID
		,numProjectID
		,numAssignTo
	FROM
		@TempTasks
	GROUP BY
		numWorkOrderID
		,numProjectID
		,numAssignTo

	DECLARE @i INT = 1
	DECLARE @iCount INT 	
	SELECT @iCount = COUNT(*) FROM @TempTasks

	DECLARE @numTempTaskID NUMERIC(18,0)
	DECLARE @numTempParentTaskID NUMERIC(18,0)
	DECLARE @numTaskAssignee NUMERIC(18,0)
	DECLARE @intTaskType INT
	DECLARE @numTotalTaskInMinutes NUMERIC(18,0)
	DECLARE @numWorkOrderID NUMERIC(18,0)
	DECLARE @numProjectID NUMERIC(18,0)
	DECLARE @numQtyItemsReq FLOAT

	DECLARE @numWorkScheduleID NUMERIC(18,0)
	DECLARE @numProductiveTimeInMinutes NUMERIC(18,0)
	DECLARE @tmStartOfDay TIME(7)
	DECLARE @numTimeLeftForDay NUMERIC(18,0)
	DECLARE @vcWorkDays VARCHAR(20)
	DECLARE @dtStartDate DATETIME
		
	WHILE @i <= @iCount
	BEGIN
		SELECT
			@numTempTaskID=numTaskID
			,@numTempParentTaskID=numReferenceTaskId
			,@numTaskAssignee=numAssignTo
			,@numTotalTaskInMinutes=numTaskTimeInMinutes
			,@intTaskType = intTaskType
			,@dtStartDate=dtStartDate
			,@numWorkOrderID=numWorkOrderID
			,@numProjectID=numProjectID
			,@numQtyItemsReq=numQtyItemsReq
		FROM
			@TempTasks 
		WHERE
			ID=@i

		-- GET TASK COMPLETION TIME WHICH WILL BE START DATETIME FOR NEXT TASK
		SELECT
			@numWorkScheduleID = WS.ID
			,@numProductiveTimeInMinutes=(ISNULL(WS.numProductiveHours,0) * 60) + ISNULL(WS.numProductiveMinutes,0)
			,@tmStartOfDay = tmStartOfDay
			,@vcWorkDays=CONCAT(',',vcWorkDays,',')
			,@dtStartDate = DATEADD(MINUTE,@ClientTimeZoneOffset,CONVERT(DATETIME,CONVERT(VARCHAR(8),DATEADD(MINUTE,-@ClientTimeZoneOffset,@dtStartDate),112)) + tmStartOfDay)
		FROM
			WorkSchedule WS
		INNER JOIN
			UserMaster
		ON
			WS.numUserCntID = UserMaster.numUserDetailId
		WHERE 
			WS.numUserCntID = @numTaskAssignee

		IF @intTaskType=1 AND EXISTS (SELECT numAssignedTo FROM @TempTaskAssignee WHERE ISNULL(numWorkOrderID,0)=ISNULL(@numWorkOrderID,0) AND ISNULL(numProjectID,0) = ISNULL(@numProjectID,0) AND numAssignedTo = @numTaskAssignee AND dtLastTaskCompletionTime IS NOT NULL)
		BEGIN
			SELECT @dtStartDate = dtLastTaskCompletionTime FROM @TempTaskAssignee WHERE ISNULL(numWorkOrderID,0)=ISNULL(@numWorkOrderID,0) AND ISNULL(numProjectID,0) = ISNULL(@numProjectID,0) AND numAssignedTo = @numTaskAssignee
		END
		ELSE IF EXISTS (SELECT numAssignedTo FROM @TempTaskAssignee WHERE ISNULL(numWorkOrderID,0)=ISNULL(@numWorkOrderID,0) AND ISNULL(numProjectID,0) = ISNULL(@numProjectID,0) AND numAssignedTo = @numTaskAssignee AND dtLastTaskCompletionTime IS NOT NULL)
		BEGIN
			SELECT @dtStartDate = dtLastTaskCompletionTime FROM @TempTaskAssignee WHERE ISNULL(numWorkOrderID,0)=ISNULL(@numWorkOrderID,0) AND ISNULL(numProjectID,0) = ISNULL(@numProjectID,0) AND numAssignedTo = @numTaskAssignee
		END

		UPDATE @TempTasks SET dtStartDate=@dtStartDate WHERE ID=@i 

		IF @numProductiveTimeInMinutes > 0 AND @numTotalTaskInMinutes > 0
		BEGIN
			WHILE @numTotalTaskInMinutes > 0
			BEGIN
				-- IF ITS WORKING DAY AND ALSO NOT HOLIDAY THEN PROCEED OR INCRESE DATE TO NEXT DAY
				IF CHARINDEX(CONCAT(',',DATEPART(WEEKDAY,@dtStartDate),','),@vcWorkDays) > 0 AND NOT EXISTS (SELECT ID FROM WorkScheduleDaysOff WHERE numWorkScheduleID=@numWorkScheduleID AND CAST(DATEADD(MINUTE,@ClientTimeZoneOffset * -1,@dtStartDate) AS DATE) BETWEEN dtDayOffFrom AND dtDayOffTo)
				BEGIN
					-- CHECK TIME LEFT FOR DAY BASED
					SET @numTimeLeftForDay = DATEDIFF(MINUTE,@dtStartDate,DATEADD(MINUTE,@numProductiveTimeInMinutes,DATEADD(MINUTE,@ClientTimeZoneOffset,CONVERT(DATETIME,CONVERT(VARCHAR(8),DATEADD(MINUTE,-@ClientTimeZoneOffset,@dtStartDate),112)) + @tmStartOfDay)))

					IF @numTimeLeftForDay > 0
					BEGIN
						INSERT INTO @TempTimeLog
						(
							numWrokOrder
							,numProjectID
							,numTaskID
							,numReferenceTaskId
							,numQtyItemsReq
							,numAssignTo
							,numProductiveMinutes
							,dtDate
							,numTotalMinutes
						) 
						VALUES 
						(	
							@numWorkOrderID
							,@numProjectID
							,@numTempTaskID
							,@numTempParentTaskID
							,@numQtyItemsReq
							,@numTaskAssignee
							,@numProductiveTimeInMinutes
							,@dtStartDate
							,(CASE WHEN @numTimeLeftForDay > @numTotalTaskInMinutes THEN @numTotalTaskInMinutes ELSE @numTimeLeftForDay END)
						)

						IF @numTimeLeftForDay > @numTotalTaskInMinutes
						BEGIN
							SET @dtStartDate = DATEADD(MINUTE,@numTotalTaskInMinutes,@dtStartDate)
						END
						ELSE
						BEGIN
							SET @dtStartDate = DATEADD(MINUTE,@ClientTimeZoneOffset,CONVERT(DATETIME,CONVERT(VARCHAR(8),DATEADD(MINUTE,-@ClientTimeZoneOffset,DATEADD(DAY,1,@dtStartDate)),112)) + @tmStartOfDay)
						END
							
						SET @numTotalTaskInMinutes = @numTotalTaskInMinutes - @numTimeLeftForDay
					END
					ELSE
					BEGIN
						SET @dtStartDate = DATEADD(MINUTE,@ClientTimeZoneOffset,CONVERT(DATETIME,CONVERT(VARCHAR(8),DATEADD(MINUTE,-@ClientTimeZoneOffset,DATEADD(DAY,1,@dtStartDate)),112)) + @tmStartOfDay)
					END
				END
				ELSE
				BEGIN
					SET @dtStartDate = DATEADD(MINUTE,@ClientTimeZoneOffset,CONVERT(DATETIME,CONVERT(VARCHAR(8),DATEADD(MINUTE,-@ClientTimeZoneOffset,DATEADD(DAY,1,@dtStartDate)),112)) + @tmStartOfDay)					
				END				
			END
		END	

		UPDATE @TempTasks SET dtFinishDate=@dtStartDate WHERE ID=@i 
		UPDATE @TempTaskAssignee SET dtLastTaskCompletionTime=@dtStartDate WHERE ISNULL(numWorkOrderID,0)=ISNULL(@numWorkOrderID,0) AND ISNULL(numProjectID,0) = ISNULL(@numProjectID,0) AND numAssignedTo=@numTaskAssignee

		SET @i = @i + 1
	END

	IF @tintView = 1
	BEGIN
		INSERT INTO @EmployeeTime
		(
			numUserCntID
			,numRecordID
			,tintRecordType
			,vcSource
			,dtDate
			,vcDate
			,numType
			,vcType
			,vcHours
			,numMinutes
			,vcDetails
		)
		SELECT
			TTL.numAssignTo
			,(CASE WHEN ISNULL(TTL.numWrokOrder,0) > 0 THEN TTL.numWrokOrder ELSE TTL.numProjectID END)
			,(CASE WHEN ISNULL(TTL.numWrokOrder,0) > 0 THEN 4 ELSE 5 END)
			,(CASE 
				WHEN ISNULL(TTL.numWrokOrder,0) > 0 
				THEN CONCAT('<a target="_blank" href="../Items/frmWorkOrder.aspx?WOID=',WO.numWOId,'">',ISNULL(WO.vcWorkOrderName,'-'),'</a>') 
				ELSE CONCAT('<a target="_blank" href="../projects/frmProjects.aspx?ProId=',PM.numProId,'">',ISNULL(PM.vcProjectName,'-'),'</a>')
			END) 
			,TTL.dtDate
			,dbo.FormatedDateFromDate(DATEADD(MINUTE,@ClientTimeZoneOffset * -1,TTL.dtDate),@numDomainID)
			,0
			,'Work Order'
			,CONCAT(FORMAT(ISNULL(TTL.numTotalMinutes,0) / 60,'00'),':',FORMAT(ISNULL(TTL.numTotalMinutes,0) % 60,'00'))
			,ISNULL(TTL.numTotalMinutes,0)
			,ISNULL(SPDT.vcTaskName,'') vcDetails
		FROM
			@TempTimeLog TTL
		INNER JOIN
			StagePercentageDetailsTask SPDT
		ON
			TTL.numTaskID = SPDT.numTaskId
		LEFT JOIN
			WorkOrder WO
		ON
			TTL.numWrokOrder = WO.numWOId
		LEFT JOIN
			ProjectsMaster PM
		ON
			TTL.numProjectID = PM.numProId
		WHERE
			dtDate BETWEEN @dtFromDate AND @dtToDate

		SELECT * FROM @EmployeeTime ORDER BY dtDate
	END
	ELSE IF @tintView = 2 OR @tintView = 3
	BEGIN
		DECLARE @TEMPResult TABLE
		(
			dtDate DATE
			,numUserCntID NUMERIC(18,0)
			,numTotalMinutes NUMERIC(18,0)
			,vcTotalHours VARCHAR(20)
			,numProductiveMinutes NUMERIC(18,0)
			,vcMaxHours VARCHAR(20)
			,numTotalBuildTasks NUMERIC(18,0)
			,numTotalProjectTasks NUMERIC(18,0)
			,numCapacityLoad NUMERIC(18,0)
		)

		WHILE @dtFromDate <= @dtToDate
		BEGIN
			INSERT INTO @TEMPResult
			(
				dtDate
				,numUserCntID
				,numTotalMinutes
				,numProductiveMinutes
				,vcMaxHours
				,numTotalBuildTasks
				,numTotalProjectTasks
			)	
			SELECT
				@dtFromDate
				,TTL.numAssignTo
				,ISNULL((SELECT SUM(numMinutes) FROM @EmployeeTime WHERE numUserCntID=TTL.numAssignTo AND CAST(dtDate AS DATE) = CAST(@dtFromDate AS DATE)),0) + ISNULL((SELECT SUM(numTotalMinutes) FROM @TempTimeLog TTLInner WHERE TTLInner.numAssignTo = TTL.numAssignTo AND CAST(TTLInner.dtdate AS DATE)  = CAST(@dtFromDate AS DATE)),0)
				,ISNULL(numProductiveMinutes,0)
				,CONCAT(FORMAT(ISNULL(numProductiveMinutes,0) / 60,'00'),':',FORMAT(ISNULL(numProductiveMinutes,0) % 60,'00'))
				,ISNULL((SELECT SUM(T.numQtyItemsReq) FROM (SELECT numQtyItemsReq FROM @TempTimeLog TTLInner WHERE TTLInner.numAssignTo = TTL.numAssignTo AND ISNULL(TTLInner.numWrokOrder,0) > 0 AND CAST(TTLInner.dtdate AS DATE)  = CAST(@dtFromDate AS DATE) GROUP BY TTLInner.numAssignTo,TTLInner.numTaskID,TTLInner.numQtyItemsReq) T),0)
				,ISNULL((SELECT SUM(T.numQtyItemsReq) FROM (SELECT numQtyItemsReq FROM @TempTimeLog TTLInner WHERE TTLInner.numAssignTo = TTL.numAssignTo AND ISNULL(TTLInner.numProjectID,0) > 0 AND CAST(TTLInner.dtdate AS DATE)  = CAST(@dtFromDate AS DATE) GROUP BY TTLInner.numAssignTo,TTLInner.numTaskID,TTLInner.numQtyItemsReq) T),0)				
			FROM
				@TempTimeLog TTL
			WHERE
				CAST(dtdate AS DATE)  = CAST(@dtFromDate AS DATE)
			GROUP BY
				numAssignTo
				,numProductiveMinutes
			
			SET @dtFromDate = DATEADD(DAY,1,@dtFromDate)
		END

		UPDATE
			@TEMPResult 
		SET 
			vcTotalHours = CONCAT(FORMAT(ISNULL(numTotalMinutes,0) / 60,'00'),':',FORMAT(ISNULL(numTotalMinutes,0) % 60,'00'))
			,numCapacityLoad = (CASE WHEN ISNULL(numProductiveMinutes,0) > 0 THEN (numTotalMinutes * 100) / numProductiveMinutes ELSE 0 END)

		SELECT * FROM @TEMPResult
	END
END
GO