GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by Priya Sharma
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ShippingService_Save')
DROP PROCEDURE USP_ShippingService_Save
GO
CREATE PROCEDURE [dbo].[USP_ShippingService_Save]   
	@numShippingServiceID NUMERIC(18,0),   
	@numDomainID NUMERIC(9),   
	@numShipViaID NUMERIC(18,0),
	@vcShippingService VARCHAR(300),
	@vcAbbreviation VARCHAR(300)
AS
BEGIN
	IF ISNULL(@numShippingServiceID,0) = 0     
	BEGIN     
		INSERT INTO ShippingService
		(
			numDomainID,numShipViaID,vcShipmentService
		)
		VALUES
		(
			@numDomainID,@numShipViaID,@vcShippingService
		)
	 
		IF LEN(ISNULL(@vcAbbreviation,'')) > 0
		BEGIN
			INSERT INTO ShippingServiceAbbreviations
			(
				numDomainID,numShippingServiceID,vcAbbreviation
			)
			VALUES
			(
				@numDomainID,SCOPE_IDENTITY(),@vcAbbreviation
			)      
		END
	END      
	ELSE      
	BEGIN      
		UPDATE 
			ShippingService 
		SET 
			vcShipmentService=@vcShippingService   
		WHERE 
			numShippingServiceID=@numShippingServiceID 
			AND numDomainID=@numDomainID    

		IF LEN(ISNULL(@vcAbbreviation,'')) > 0
		BEGIN
			IF EXISTS (SELECT ID FROM ShippingServiceAbbreviations WHERE numDomainID=@numDomainID AND numShippingServiceID=@numShippingServiceID)
			BEGIN
				UPDATE
					ShippingServiceAbbreviations
				SET 
					vcAbbreviation=@vcAbbreviation
				WHERE
					numDomainID=@numDomainID
					AND numShippingServiceID=@numShippingServiceID
			END
			ELSE
			BEGIN
				INSERT INTO ShippingServiceAbbreviations
				(
					numDomainID,numShippingServiceID,vcAbbreviation
				)
				VALUES
				(
					@numDomainID,@numShippingServiceID,@vcAbbreviation
				)      
			END
		END
		ELSE
		BEGIN
			DELETE FROM ShippingServiceAbbreviations WHERE numDomainID=@numDomainID AND numShippingServiceID=@numShippingServiceID
		END
	END
END
GO