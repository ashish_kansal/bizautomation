GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_ManageWarehouseLocation' ) 
    DROP PROCEDURE USP_ManageWarehouseLocation
GO
-- EXEC USP_GetWarehouseLocation 1 , 58 , 11
CREATE PROCEDURE [dbo].USP_ManageWarehouseLocation
    @numDomainID AS NUMERIC(9) ,
    @numWarehouseID AS NUMERIC(8),
    @numWLocationID AS numeric(9) = 0,
    @strItems TEXT='',
    @tintMode AS tinyint=0
    
AS 
    BEGIN
 
 IF @tintMode = 0
 BEGIN
		DECLARE  @hDocItem INT
      IF CONVERT(VARCHAR(10),@strItems) <> ''
        BEGIN
          EXEC sp_xml_preparedocument
            @hDocItem OUTPUT ,
            @strItems
            
        INSERT INTO dbo.WarehouseLocation
				( numWarehouseID ,
				  vcAisle ,
				  vcRack ,
				  vcShelf ,
				  vcBin ,
				  vcLocation ,
				  bitDefault ,
				  bitSystem ,
				  intQty ,
				  numDomainID
				)
          SELECT
			@numWarehouseID,
			X.vcAisle,X.vcRack,X.vcShelf,X.vcBin,X.vcLocation,0,0,0,@numDomainID
          FROM   (SELECT *
                  FROM   OPENXML (@hDocItem, '/NewDataSet/Item', 2)
                            WITH (
						   vcAisle varchar(50),
						   vcRack varchar(50),
						   vcShelf varchar(50),
						   vcBin varchar(50),
						   vcLocation varchar(100),
						   bitDefault bit,
						   bitSystem bit,
						   intQty int
                            )) X
          EXEC sp_xml_removedocument
            @hDocItem
        END
 	
 END
 
-- IF @tintMode = 2
-- BEGIN
-- 		INSERT INTO dbo.WarehouseLocation
--         ( numWarehouseID ,
--           vcAisle ,
--           vcRack ,
--           vcShelf ,
--           vcBin ,
--           vcLocation ,
--           bitDefault ,
--           bitSystem ,
--           intQty ,
--           numDomainID
--         )
--		VALUES  ( NULL , -- numWarehouseID - numeric
--           '' , -- vcAisle - varchar(50)
--           '' , -- vcRack - varchar(50)
--           '' , -- vcShelf - varchar(50)
--           '' , -- vcBin - varchar(50)
--           '' , -- vcLocation - varchar(100)
--           NULL , -- bitDefault - bit
--           NULL , -- bitSystem - bit
--           0 , -- intQty - int
--           NULL  -- numDomainID - numeric
--         )
--
-- END
  
 
 
 


 	
    END
 
 GO 