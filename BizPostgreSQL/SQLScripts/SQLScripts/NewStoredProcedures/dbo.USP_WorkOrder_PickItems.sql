GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
                    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_WorkOrder_PickItems')
DROP PROCEDURE USP_WorkOrder_PickItems
GO
CREATE PROCEDURE [dbo].[USP_WorkOrder_PickItems]                             
	@numDomainID NUMERIC(18,0)
	,@numUserCntID NUMERIC(18,0)
	,@numWOID NUMERIC(18,0)
	,@vcPickItems VARCHAR(MAX)                   
AS                            
BEGIN
	DECLARE @ErrorMessage NVARCHAR(4000)
	DECLARE @ErrorNumber INT
	DECLARE @ErrorSeverity INT
	DECLARE @ErrorState INT
	DECLARE @ErrorLine INT
	DECLARE @ErrorProcedure NVARCHAR(200)

	DECLARE @vcWorkOrder VARCHAR(100)
	DECLARE @vcItemName VARCHAR(300)
	DECLARE @numQtyToBuild FLOAT
	DECLARE @numBuildManager NUMERIC(18,0)
	DECLARE @numWarehouseID NUMERIC(18,0)
	DECLARE @numBuildPlan NUMERIC(18,0)

	DECLARE @numOldPickedQty FLOAT = 0
	DECLARE @numNewPickedQty FLOAT = 0

	IF EXISTS (SELECT numWOId FROM WorkOrder WHERE numDomainID=@numDomainID AND numWOId=@numWOID AND numWOStatus=23184)
	BEGIN
		RAISERROR('WORKORDER_COMPLETED',16,1)
		RETURN
	END
	
	SET @numOldPickedQty = ISNULL((SELECT
									MIN(numPickedQty)
								FROM
								(
									SELECT 
										WorkOrderDetails.numWODetailId
										,FLOOR(SUM(WorkOrderPickedItems.numPickedQty)/numQtyItemsReq_Orig) numPickedQty
									FROM
										WorkOrderDetails
									LEFT JOIN
										WorkOrderPickedItems
									ON
										WorkOrderDetails.numWODetailId=WorkOrderPickedItems.numWODetailId
									WHERE
										WorkOrderDetails.numWOId=@numWOID
										AND ISNULL(WorkOrderDetails.numWareHouseItemId,0) > 0
									GROUP BY
										WorkOrderDetails.numWODetailId
										,WorkOrderDetails.numQtyItemsReq_Orig
								) TEMP),0)

	SELECT
		@vcWorkOrder=ISNULL(vcWorkOrderName,'-')
		,@vcItemName=vcItemName
		,@numQtyToBuild=numQtyItemsReq
		,@numBuildManager=ISNULL(numAssignedTo,0)
		,@numWarehouseID = WareHouseItems.numWareHouseID
		,@numBuildPlan=ISNULL(numBuildPlan,0)
	FROM 
		WorkOrder 
	INNER JOIN
		Item
	ON
		WorkOrder.numItemCode = Item.numItemCode
	INNER JOIN 
		WareHouseItems 
	ON 
		WorkOrder.numWareHouseItemId = WareHouseItems.numWareHouseItemID 
	WHERE 
		WorkOrder.numDomainID=@numDomainID 
		AND WorkOrder.numWOId=@numWOID

	DECLARE @TableWOItems TABLE
	(
		ID INT IDENTITY(1,1)
		,numWOID NUMERIC(18,0)
		,numWODetailID NUMERIC(18,0)
		,numItemCode NUMERIC(18,0)
		,numWarehouseItemID NUMERIC(18,0)
		,numQtyItemsReq FLOAT
		,numQtyItemsReq_Orig FLOAT
		,numQtyLeftToPick FLOAT
		,numPickedQty FLOAT
		,numFromWarehouseItemID NUMERIC(18,0)
		,bitSuccess BIT
		,vcErrorMessage VARCHAR(300)
	)

	DECLARE @numWODetailID NUMERIC(18,0)
	DECLARE @numItemCode NUMERIC(18,0)
	DECLARE @numWarehouseItemID NUMERIC(18,0)
	DECLARE @numFromWarehouseItemID NUMERIC(18,0)
	DECLARE @numQtyItemsReq FLOAT
	DECLARE @numQtyItemsReq_Orig FLOAT
	DECLARE @numQtyLeftToPicked FLOAT
	DECLARE @numQtyPicked FLOAT
	DECLARE @numOnHand AS FLOAT
	DECLARE @numOnOrder AS FLOAT
	DECLARE @numOnAllocation AS FLOAT
	DECLARE @numOnBackOrder AS FLOAT
	DECLARE @Description AS VARCHAR(1000)
	DECLARE @i INT = 1
	DECLARE @iCount INT

	IF LEN(ISNULL(@vcPickItems,'')) = 0
	BEGIN
		INSERT INTO @TableWOItems
		(
			numWOID
			,numWODetailID
			,numItemCode
			,numWareHouseItemId
			,numQtyItemsReq
			,numQtyItemsReq_Orig
			,numQtyLeftToPick
		)
		SELECT
			numWOId
			,numWODetailID
			,numChildItemID
			,numWareHouseItemId
			,numQtyItemsReq
			,numQtyItemsReq_Orig
			,numQtyItemsReq - ISNULL((SELECT SUM(numPickedQty) FROM WorkOrderPickedItems WHERE WorkOrderPickedItems.numWODetailId=WorkOrderDetails.numWODetailID),0)
		FROM
			WorkOrderDetails
		WHERE
			numWOId=@numWOID
			AND ISNULL(numWareHouseItemId,0) > 0

		SELECT @iCount = COUNT(*) FROM @TableWOItems

		BEGIN TRY
		BEGIN TRANSACTION

			WHILE @i <= @iCount
			BEGIN
				SELECT
					@numWODetailID=numWODetailID
					,@numItemCode=numItemCode
					,@numWarehouseItemID=numWarehouseItemID
					,@numQtyItemsReq=numQtyItemsReq
					,@numQtyItemsReq_Orig=numQtyItemsReq_Orig
					,@numQtyLeftToPicked=numQtyLeftToPick
				FROM
					@TableWOItems 
				WHERE 
					ID = @i

				--GET CHILD ITEM CURRENT INVENTORY DETAIL
				SELECT  
					@numOnHand = ISNULL(numOnHand, 0),
					@numOnAllocation = ISNULL(numAllocation, 0),
					@numOnOrder = ISNULL(numOnOrder, 0),
					@numOnBackOrder = ISNULL(numBackOrder, 0)
				FROM    
					WareHouseItems
				WHERE   
					numWareHouseItemID = @numWarehouseItemID	

				IF @numOnAllocation > 0 AND @numQtyLeftToPicked > 0
				BEGIN
					IF @numOnAllocation >= @numQtyLeftToPicked
					BEGIN
						SET @Description=CONCAT('Items Picked For Work Order (Qty:',@numQtyLeftToPicked,')')
						SET @numQtyPicked = @numQtyLeftToPicked
						SET @numOnAllocation = @numOnAllocation - @numQtyLeftToPicked
					END
					ELSE
					BEGIN
						SET @numQtyPicked = FLOOR(@numOnAllocation/@numQtyItemsReq_Orig) * @numQtyItemsReq_Orig
						SET @Description=CONCAT('Items Picked For Work Order (Qty:',@numQtyPicked,')')
						SET @numOnAllocation = @numOnAllocation - (FLOOR(@numOnAllocation/@numQtyItemsReq_Orig) * @numQtyItemsReq_Orig)
					END
				
					EXEC USP_UpdateInventoryAndTracking 
						@numOnHand=@numOnHand,
						@numOnAllocation=@numOnAllocation,
						@numOnBackOrder=@numOnBackOrder,
						@numOnOrder=@numOnOrder,
						@numWarehouseItemID=@numWarehouseItemID,
						@numReferenceID=@numWOId,
						@tintRefType=2,
						@numDomainID=@numDomainID,
						@numUserCntID=@numUserCntID,
						@Description=@Description

					INSERT INTO WorkOrderPickedItems
					(
						[numWODetailId]
						,[numPickedQty]
						,[numFromWarehouseItemID]
						,[numToWarehouseItemID]
						,[dtCreatedDate]
						,[numCreatedBy]
					)
					VALUES
					(
						@numWODetailID
						,@numQtyPicked
						,@numWarehouseItemID
						,@numWarehouseItemID
						,GETUTCDATE()
						,@numUserCntID
					)
				END

				SET @i = @i + 1
			END

		COMMIT
		END TRY
		BEGIN CATCH
			IF @@TRANCOUNT > 0
				ROLLBACK TRANSACTION;

			SELECT 
				@ErrorMessage = ERROR_MESSAGE(),
				@ErrorNumber = ERROR_NUMBER(),
				@ErrorSeverity = ERROR_SEVERITY(),
				@ErrorState = ERROR_STATE(),
				@ErrorLine = ERROR_LINE(),
				@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

			RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);
		END CATCH
	END
	ELSE
	BEGIN
		DECLARE @hDocItem int
		EXEC sp_xml_preparedocument @hDocItem OUTPUT, @vcPickItems

		INSERT INTO @TableWOItems
		(
			numWOId
			,numWODetailID
			,numItemCode
			,numWareHouseItemId
			,numQtyItemsReq
			,numQtyItemsReq_Orig
			,numPickedQty
			,numFromWarehouseItemID
			,bitSuccess
			,vcErrorMessage
		)
		SELECT
			WorkOrderDetails.numWOId
			,TEMP.numWODetailID
			,TEMP.numItemCode
			,TEMP.numWareHouseItemId
			,WorkOrderDetails.numQtyItemsReq
			,WorkOrderDetails.numQtyItemsReq_Orig
			,TEMP.numPickedQty
			,TEMP.numFromWarehouseItemID
			,1
			,''
		FROM
		(
			SELECT
				numWODetailID
				,numItemCode
				,numWareHouseItemId
				,numPickedQty
				,numFromWarehouseItemID
			FROM
				OPENXML (@hDocItem,'/NewDataSet/Table1',2)
			WITH
			(
				numWODetailID NUMERIC(18,0)
				,numItemCode NUMERIC(18,0)
				,numWarehouseItemID NUMERIC(18,0)
				,numPickedQty FLOAT
				,numFromWarehouseItemID NUMERIC(18,0)
			)
		) TEMP
		INNER JOIN
			WorkOrderDetails 
		ON
			TEMP.numWODetailID = WorkOrderDetails.numWODetailId

		EXEC sp_xml_removedocument @hDocItem

		SELECT @iCount = COUNT(*) FROM @TableWOItems

		BEGIN TRY
		BEGIN TRANSACTION

			WHILE @i <= @iCount
			BEGIN
				SELECT
					@numWOID=numWOID
					,@numWODetailID=numWODetailID
					,@numItemCode=numItemCode
					,@numWarehouseItemID=numWarehouseItemID
					,@numQtyItemsReq=numQtyItemsReq
					,@numQtyItemsReq_Orig=numQtyItemsReq_Orig
					,@numFromWarehouseItemID=numFromWarehouseItemID
					,@numQtyPicked=numPickedQty
				FROM
					@TableWOItems 
				WHERE 
					ID = @i

				IF (@numQtyPicked + ISNULL((SELECT SUM(numPickedQty) FROM WorkOrderPickedItems WHERE WorkOrderPickedItems.numWODetailId=@numWODetailID),0)) > @numQtyItemsReq
				BEGIN
					UPDATE @TableWOItems SET bitSuccess=0,vcErrorMessage='Picked quantity can not be greater then quantity left to be picked.' WHERE ID=@i
				END
				ELSE
				BEGIN
					--GET CHILD ITEM CURRENT INVENTORY DETAIL
					SELECT  
						@numOnHand = ISNULL(numOnHand, 0),
						@numOnAllocation = ISNULL(numAllocation, 0),
						@numOnOrder = ISNULL(numOnOrder, 0),
						@numOnBackOrder = ISNULL(numBackOrder, 0)
					FROM    
						WareHouseItems
					WHERE   
						numWareHouseItemID = @numFromWarehouseItemID
				
					IF @numFromWarehouseItemID = @numWarehouseItemID
					BEGIN
						IF @numOnAllocation > 0 AND @numOnAllocation >= @numQtyPicked
						BEGIN
							SET @Description=CONCAT('Items Picked For Work Order (Qty:',@numQtyPicked,')')
							SET @numOnAllocation = @numOnAllocation - @numQtyPicked

							EXEC USP_UpdateInventoryAndTracking 
								@numOnHand=@numOnHand,
								@numOnAllocation=@numOnAllocation,
								@numOnBackOrder=@numOnBackOrder,
								@numOnOrder=@numOnOrder,
								@numWarehouseItemID=@numFromWarehouseItemID,
								@numReferenceID=@numWOId,
								@tintRefType=2,
								@numDomainID=@numDomainID,
								@numUserCntID=@numUserCntID,
								@Description=@Description

							INSERT INTO WorkOrderPickedItems
							(
								[numWODetailId]
								,[numPickedQty]
								,[numFromWarehouseItemID]
								,[numToWarehouseItemID]
								,[dtCreatedDate]
								,[numCreatedBy]
							)
							VALUES
							(
								@numWODetailID
								,@numQtyPicked
								,@numWarehouseItemID
								,@numWarehouseItemID
								,GETUTCDATE()
								,@numUserCntID
							)
						END
						ELSE
						BEGIN
							UPDATE @TableWOItems SET bitSuccess=0,vcErrorMessage='Picked quantity can not be greater then quantity on allocation.' WHERE ID=@i
						END
					END
					ELSE
					BEGIN
						IF (@numOnHand + @numOnAllocation) > 0 AND (@numOnHand + @numOnAllocation) >= @numQtyPicked
						BEGIN
							SET @Description=CONCAT('Items Transferred For Work Order (Qty:',@numQtyPicked,')')
							IF @numOnHand >= @numQtyPicked
							BEGIN
								SET @numOnHand = @numOnHand - @numQtyPicked
							END
							ELSE
							BEGIN
								SET @numOnBackOrder = @numOnBackOrder + (@numQtyPicked - @numOnHand)
								SET @numOnAllocation = @numOnAllocation - (@numQtyPicked - @numOnHand)
								SET @numOnHand = 0
							END
							

							EXEC USP_UpdateInventoryAndTracking 
								@numOnHand=@numOnHand,
								@numOnAllocation=@numOnAllocation,
								@numOnBackOrder=@numOnBackOrder,
								@numOnOrder=@numOnOrder,
								@numWarehouseItemID=@numFromWarehouseItemID,
								@numReferenceID=@numWOId,
								@tintRefType=2,
								@numDomainID=@numDomainID,
								@numUserCntID=@numUserCntID,
								@Description=@Description

							SELECT  
								@numOnHand = ISNULL(numOnHand, 0),
								@numOnAllocation = ISNULL(numAllocation, 0),
								@numOnOrder = ISNULL(numOnOrder, 0),
								@numOnBackOrder = ISNULL(numBackOrder, 0)
							FROM    
								WareHouseItems
							WHERE   
								numWareHouseItemID = @numWarehouseItemID

							IF @numOnBackOrder > 0
							BEGIN
								IF @numOnBackOrder > @numQtyPicked
								BEGIN
									SET @numOnBackOrder = @numOnBackOrder - @numQtyPicked
								END
								ELSE
								BEGIN
									SET @numOnBackOrder = 0
								END
							END

							SET @numOnAllocation = @numOnAllocation + @numQtyPicked
							SET @Description=CONCAT('Items Received For Work Order (Qty:',@numQtyPicked,')')

							EXEC USP_UpdateInventoryAndTracking 
								@numOnHand=@numOnHand,
								@numOnAllocation=@numOnAllocation,
								@numOnBackOrder=@numOnBackOrder,
								@numOnOrder=@numOnOrder,
								@numWarehouseItemID=@numWarehouseItemID,
								@numReferenceID=@numWOId,
								@tintRefType=2,
								@numDomainID=@numDomainID,
								@numUserCntID=@numUserCntID,
								@Description=@Description


							SET @numOnAllocation = @numOnAllocation - @numQtyPicked
							SET @Description=CONCAT('Items Picked For Work Order (Qty:',@numQtyPicked,')')

							EXEC USP_UpdateInventoryAndTracking 
								@numOnHand=@numOnHand,
								@numOnAllocation=@numOnAllocation,
								@numOnBackOrder=@numOnBackOrder,
								@numOnOrder=@numOnOrder,
								@numWarehouseItemID=@numWarehouseItemID,
								@numReferenceID=@numWOId,
								@tintRefType=2,
								@numDomainID=@numDomainID,
								@numUserCntID=@numUserCntID,
								@Description=@Description

							INSERT INTO WorkOrderPickedItems
							(
								[numWODetailId]
								,[numPickedQty]
								,[numFromWarehouseItemID]
								,[numToWarehouseItemID]
								,[dtCreatedDate]
								,[numCreatedBy]
							)
							VALUES
							(
								@numWODetailID
								,@numQtyPicked
								,@numFromWarehouseItemID
								,@numWarehouseItemID
								,GETUTCDATE()
								,@numUserCntID
							)
						END
						ELSE
						BEGIN
							UPDATE @TableWOItems SET bitSuccess=0,vcErrorMessage='Picked quantity can not be greater then available quantity.' WHERE ID=@i
						END
					END	
				END

				SET @i = @i + 1
			END

		COMMIT
		END TRY
		BEGIN CATCH
			IF @@TRANCOUNT > 0
				ROLLBACK TRANSACTION;

			SELECT 
				@ErrorMessage = ERROR_MESSAGE(),
				@ErrorNumber = ERROR_NUMBER(),
				@ErrorSeverity = ERROR_SEVERITY(),
				@ErrorState = ERROR_STATE(),
				@ErrorLine = ERROR_LINE(),
				@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

			RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);
		END CATCH
	END

	--SET @numNewPickedQty = ISNULL((SELECT
	--								MIN(numPickedQty)
	--							FROM
	--							(
	--								SELECT 
	--									WorkOrderDetails.numWODetailId
	--									,FLOOR(SUM(WorkOrderPickedItems.numPickedQty)/numQtyItemsReq_Orig) numPickedQty
	--								FROM
	--									WorkOrderDetails
	--								LEFT JOIN
	--									WorkOrderPickedItems
	--								ON
	--									WorkOrderDetails.numWODetailId=WorkOrderPickedItems.numWODetailId
	--								WHERE
	--									WorkOrderDetails.numWOId=@numWOID
	--									AND ISNULL(WorkOrderDetails.numWareHouseItemId,0) > 0
	--								GROUP BY
	--									WorkOrderDetails.numWODetailId
	--									,WorkOrderDetails.numQtyItemsReq_Orig
	--							) TEMP),0)


	--IF (@numBuildPlan = 202 AND @numNewPickedQty = @numQtyToBuild) OR (@numBuildPlan = 201 AND @numNewPickedQty <> @numOldPickedQty)
	--BEGIN
	--	DECLARE @dtActionDate DATETIME = GETUTCDATE()
	--	DECLARE @vcDescription VARCHAR(300) = CONCAT('Work Order BOM Picked: ',@vcWorkOrder,' (',@vcItemName,')')

	--	IF @numBuildManager > 0
	--	BEGIN
	--		DECLARE @numDivisionId NUMERIC(18,0)
			
	--		SELECT @numDivisionId=numDivisionId FROM AdditionalContactsInformation WHERE numContactId=@numBuildManager

	--		EXEC dbo.usp_InsertCommunication
	--		@numCommId = 0, --  numeric(18, 0)
	--		@bitTask = 972, --  numeric(18, 0)
	--		@numContactId = @numUserCntID, --  numeric(18, 0)
	--		@numDivisionId = @numDivisionId, --  numeric(18, 0)
	--		@txtDetails = @vcDescription, --  text
	--		@numOppId = 0, --  numeric(18, 0)
	--		@numAssigned = @numBuildManager, --  numeric(18, 0)
	--		@numUserCntID = @numUserCntID, --  numeric(18, 0)
	--		@numDomainId = @numDomainID, --  numeric(18, 0)
	--		@bitClosed = 0, --  tinyint
	--		@vcCalendarName = '', --  varchar(100)
	--		@dtStartTime = @dtActionDate, --  datetime
	--		@dtEndtime =@dtActionDate, --  datetime
	--		@numActivity = 0, --  numeric(9, 0)
	--		@numStatus = 0, --  numeric(9, 0)
	--		@intSnoozeMins = 0, --  int
	--		@tintSnoozeStatus = 0, --  tinyint
	--		@intRemainderMins = 0, --  int
	--		@tintRemStaus = 0, --  tinyint
	--		@ClientTimeZoneOffset = 0, --  int
	--		@bitOutLook = 0, --  tinyint
	--		@bitSendEmailTemplate = 0, --  bit
	--		@bitAlert = 0, --  bit
	--		@numEmailTemplate = 0, --  numeric(9, 0)
	--		@tintHours = 0, --  tinyint
	--		@CaseID = 0, --  numeric(18, 0)
	--		@CaseTimeId = 0, --  numeric(18, 0)
	--		@CaseExpId = 0, --  numeric(18, 0)
	--		@ActivityId = 0, --  numeric(18, 0)
	--		@bitFollowUpAnyTime = 0, --  bit
	--		@strAttendee='',
	--		@numWOId=@numWOId,
	--		@numTaskID=0
	--	END

	--	IF EXISTS (SELECT numTaskId FROM StagePercentageDetailsTask WHERE numWorkOrderId=@numWOID AND ISNULL(numAssignTo,0) > 0)
	--	BEGIN
	--		DECLARE @TEMPTasks TABLE
	--		(
	--			ID INT IDENTITY(1,1)
	--			,numTaskID NUMERIC(18,0)
	--			,numAssignedTo NUMERIC
	--		)
	--		INSERT INTO @TEMPTasks
	--		(
	--			numTaskID
	--			,numAssignedTo
	--		)
	--		SELECT DISTINCT
	--			numTaskID
	--			,numAssignTo
	--		FROM
	--			StagePercentageDetailsTask 
	--		WHERE 
	--			numWorkOrderId=@numWOID 
	--			AND ISNULL(numAssignTo,0) > 0

	--		DECLARE @j INT = 1
	--		DECLARE @jCount INT
	--		DECLARE @numTaskID NUMERIC(18,0)
	--		SELECT @jCount=COUNT(*) FROM @TEMPTasks

	--		WHILE @j <= @jCount
	--		BEGIN
	--			SELECT @numBuildManager = numAssignedTo,@numTaskID=numTaskID FROM @TEMPTasks WHERE ID=@j

	--			SELECT @numDivisionId=numDivisionId FROM AdditionalContactsInformation WHERE numContactId=@numBuildManager

	--			EXEC dbo.usp_InsertCommunication
	--			@numCommId = 0, --  numeric(18, 0)
	--			@bitTask = 972, --  numeric(18, 0)
	--			@numContactId = @numUserCntID, --  numeric(18, 0)
	--			@numDivisionId = @numDivisionId, --  numeric(18, 0)
	--			@txtDetails = @vcDescription, --  text
	--			@numOppId = 0, --  numeric(18, 0)
	--			@numAssigned = @numBuildManager, --  numeric(18, 0)
	--			@numUserCntID = @numUserCntID, --  numeric(18, 0)
	--			@numDomainId = @numDomainID, --  numeric(18, 0)
	--			@bitClosed = 0, --  tinyint
	--			@vcCalendarName = '', --  varchar(100)
	--			@dtStartTime = @dtActionDate, --  datetime
	--			@dtEndtime =@dtActionDate, --  datetime
	--			@numActivity = 0, --  numeric(9, 0)
	--			@numStatus = 0, --  numeric(9, 0)
	--			@intSnoozeMins = 0, --  int
	--			@tintSnoozeStatus = 0, --  tinyint
	--			@intRemainderMins = 0, --  int
	--			@tintRemStaus = 0, --  tinyint
	--			@ClientTimeZoneOffset = 0, --  int
	--			@bitOutLook = 0, --  tinyint
	--			@bitSendEmailTemplate = 0, --  bit
	--			@bitAlert = 0, --  bit
	--			@numEmailTemplate = 0, --  numeric(9, 0)
	--			@tintHours = 0, --  tinyint
	--			@CaseID = 0, --  numeric(18, 0)
	--			@CaseTimeId = 0, --  numeric(18, 0)
	--			@CaseExpId = 0, --  numeric(18, 0)
	--			@ActivityId = 0, --  numeric(18, 0)
	--			@bitFollowUpAnyTime = 0, --  bit
	--			@strAttendee='',
	--			@numWOId=@numWOId,
	--			@numTaskID=@numTaskID

	--			SET @j = @j + 1
	--		END
	--	END

	--END


	SELECT * FROM @TableWOItems
END
GO