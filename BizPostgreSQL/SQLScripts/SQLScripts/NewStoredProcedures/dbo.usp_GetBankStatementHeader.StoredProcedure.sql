
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_GetBankStatementHeader')
	DROP PROCEDURE usp_GetBankStatementHeader
GO
/****** Added By : Joseph ******/
/****** Gets the particular Bank Statement Detail from Table BankStatementHeader ******/



CREATE PROCEDURE [dbo].[usp_GetBankStatementHeader]
	@numStatementID numeric(18, 0)
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[numStatementID],
	[numBankDetailID],
	[vcFIStatementID],
	[monAvailableBalance],
	[dtAvailableBalanceDate],
	[numCurrencyID],
	[monLedgerBalance],
	[dtLedgerBalanceDate],
	[dtStartDate],
	[dtEndDate],
	[dtCreatedDate],
	[dtModifiedDate]
FROM
	[dbo].[BankStatementHeader]
WHERE
	(	[numStatementID] = @numStatementID
 or @numStatementID = 0 )


GO