SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_OpportunityItemsReleaseDates_UpdateOrderReleaseStatus')
DROP PROCEDURE USP_OpportunityItemsReleaseDates_UpdateOrderReleaseStatus
GO
CREATE PROCEDURE [dbo].[USP_OpportunityItemsReleaseDates_UpdateOrderReleaseStatus] 
(
	@numDomainID NUMERIC(18,0)
)
AS 
BEGIN
	DECLARE @TEMP TABLE
	(
		ID INT IDENTITY(1,1),
		numOppID NUMERIC(18,0)
	)

	INSERT INTO 
		@TEMP
	SELECT
		numOppId
	FROM
		OpportunityMaster
	WHERE
		numDomainId = @numDomainID
		AND numOppId IN (SELECT DISTINCT numOppId FROM OpportunityItemsReleaseDates WHERE numDomainId=@numDomainID)
		AND dtReleaseDate IS NOT NULL 
		AND ISNULL(numReleaseStatus,0) <> 2

	DECLARE @i AS INT = 1
	DECLARE @COUNT AS INT
	DECLARE @numOppID NUMERIC(18,0)

	SELECT @COUNT = COUNT(*) FROM @TEMP

	WHILE @i <= @COUNT
	BEGIN
		SELECT @numOppID=numOppID FROM @TEMP WHERE ID=@i

		IF NOT EXISTS 
		(
			SELECT
				numOppID
			FROM
			(
				SELECT
					OIRD.numOppID,
					OIRD.numOppItemID,
					OI.numUnitHour * dbo.fn_UOMConversion(OI.numUOMId,I.numItemCode,OIRD.numDomainID,I.numBaseUnit) AS numOrderedQty,
					SUM(OIRD.numQty) AS numReleaseQty
				FROM
					OpportunityItemsReleaseDates OIRD
				INNER JOIN
					OpportunityItems OI
				ON
					OIRD.numOppID = OI.numOppId
					AND OIRD.numOppItemID = OI.numoppitemtCode
				INNER JOIN
					WareHouseItems WI
				ON
					OI.numWarehouseItmsID = WI.numWareHouseItemID
				INNER JOIN
					Item  I
				ON
					OI.numItemCode = I.numItemCode
				WHERE
					OIRD.numDomainID = @numDomainID
					AND OIRD.numOppID = @numOppID
				GROUP BY
					OIRD.numDomainID,
					OIRD.numOppID,
					OIRD.numOppItemID,
					OI.numUnitHour,
					OI.numUOMId,
					I.numItemCode,
					I.numBaseUnit
			) T1
			WHERE
				numOrderedQty > numReleaseQty
		)
		BEGIN
			UPDATE OpportunityMaster SET numReleaseStatus=2 WHERE numDomainId=@numDomainID AND numOppId=@numOppID
		END

		SET @i = @i + 1
	END
END
