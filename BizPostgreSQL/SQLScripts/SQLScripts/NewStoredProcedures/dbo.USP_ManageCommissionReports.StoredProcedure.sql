SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ManageCommissionReports')
DROP PROCEDURE USP_ManageCommissionReports
GO
CREATE PROCEDURE [dbo].[USP_ManageCommissionReports]
@numItemCode as numeric(9)=0,
@numContactID as numeric(9)=0,
@tintAssignTo AS TINYINT,
@bitCommContact AS BIT,
@numDomainID AS NUMERIC
AS

  
if not exists(select * from CommissionReports where numItemCode=@numItemCode and numContactID=@numContactID AND tintAssignTo=@tintAssignTo AND bitCommContact=@bitCommContact AND numDomainID=@numDomainID)
BEGIN
DECLARE @numComReports AS NUMERIC(9)

    insert into CommissionReports(numItemCode,numContactID,tintAssignTo,bitCommContact,numDomainID)
	values(@numItemCode,@numContactID,@tintAssignTo,@bitCommContact,@numDomainID)
	
	set @numComReports=@@IDENTITY
	
	IF @bitCommContact=0
	BEGIN
		insert into Dashboard (numReportID, numGroupUserCntID, tintRow, tintColumn, tintReportType, vcHeader, vcFooter, tintChartType, bitGroup,tintReportCategory)
		values (@numComReports, @numContactID, 1, 1, 2, '','', 2, 0,2)
	END	

END
	
GO
