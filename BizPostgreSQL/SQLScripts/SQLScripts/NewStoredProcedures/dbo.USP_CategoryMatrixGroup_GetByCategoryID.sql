GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_CategoryMatrixGroup_GetByCategoryID')
DROP PROCEDURE dbo.USP_CategoryMatrixGroup_GetByCategoryID
GO
CREATE PROCEDURE [dbo].[USP_CategoryMatrixGroup_GetByCategoryID]
(
	@numDomainID NUMERIC(18,0)
	,@numCategoryID NUMERIC(18,0)
)
AS 
BEGIN
	SELECT * FROM CategoryMatrixGroup WHERE numDomainID=@numDomainID AND numCategoryID=@numCategoryID
END
GO
