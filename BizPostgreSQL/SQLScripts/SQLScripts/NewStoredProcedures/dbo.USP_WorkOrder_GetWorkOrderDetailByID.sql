SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_WorkOrder_GetWorkOrderDetailByID')
DROP PROCEDURE USP_WorkOrder_GetWorkOrderDetailByID
GO
CREATE PROCEDURE [dbo].[USP_WorkOrder_GetWorkOrderDetailByID]  
	@numDomainID NUMERIC(18,0),
	@numWOID NUMERIC(18,0),
	@ClientTimeZoneOffset INT
AS
BEGIN
	SELECT 
		WO.numWOId,
		Opp.numDivisionId,
		ISNULL(WO.numAssignedTo,0) numRecOwner,
		CONCAT(',',STUFF((
					SELECT 
						CONCAT(',',SPDT.numAssignTo)
					FROM 
						StagePercentageDetailsTask SPDT
					WHERE 
						SPDT.numDomainID = @numDomainID
						AND SPDT.numWorkOrderId=WO.numWOId
					FOR XML PATH, TYPE).value(N'.[1]', N'nvarchar(max)'),1,1,''),',') vcTasksAssignee,
		ISNULL(WO.monAverageCost,0) monAverageCost,
		I.numItemCode,
		ISNULL((SELECT TOP 1 vcPathForTImage FROM ItemImages WHERE numItemCode = I.numItemCode AND bitDefault = 1 AND bitIsImage = 1),'') AS vcPathForTImage,
		ISNULL(WO.numOppId,0) AS numOppId,
		I.vcItemName,
		I.numAssetChartAcntId,
		WO.numQtyItemsReq,
		WO.vcWorkOrderName,
		WO.numBuildProcessId,
		ISNULL(Slp_Name,'') vcProcessName,
		WO.numWOStatus,
		dbo.GetTotalProgress(@numDomainID,WO.numWOId,1,1,'',0) numTotalProgress,
		(CASE WHEN EXISTS (SELECT ID FROM StagePercentageDetailsTaskTimeLog SPDTTL WHERE SPDTTL.numTaskID IN (SELECT SPDT.numTaskID FROM StagePercentageDetailsTask SPDT WHERE SPDT.numWorkOrderId=WO.numWOId)) THEN 1 ELSE 0 END) bitTaskStarted,
		CONCAT(dbo.fn_GetContactName(WO.numCreatedby),' ',dbo.FormatedDateTimeFromDate(DATEADD(MINUTE,-@ClientTimeZoneOffset,WO.bintCreatedDate),@numDomainID)) AS CreatedBy,
        CONCAT(dbo.fn_GetContactName(WO.numModifiedBy),' ',dbo.FormatedDateTimeFromDate(DATEADD(MINUTE,-@ClientTimeZoneOffset,WO.bintModifiedDate),@numDomainID)) AS ModifiedBy,
		CONCAT(dbo.fn_GetContactName(WO.numCompletedBy),' ',dbo.FormatedDateTimeFromDate(DATEADD(MINUTE,-@ClientTimeZoneOffset,WO.bintCompletedDate),@numDomainID)) AS CompletedBy,
		ISNULL(A.vcFirstname + ' ' + A.vcLastName,'') AS vcContactName,
         isnull(A.vcEmail,'') AS vcEmail,
         isnull(A.numPhone,'') AS Phone,
         isnull(A.numPhoneExtension,'') AS PhoneExtension,
         CONCAT(C2.vcCompanyName,Case when isnull(D2.numCompanyDiff,0)>0 then  '  ' + dbo.fn_getlistitemname(D2.numCompanyDiff) + ':' + isnull(D2.vcCompanyDiff,'') else '' end) as vcCompanyname,
		ISNULL((SELECT TOP 1 ISNULL(vcData,'') FROM ListDetails WHERE numListID=5 AND numListItemID=C2.numCompanyType AND numDomainId=@numDomainID),'') AS vcCompanyType,
		CONCAT(dbo.fn_GetContactName(WO.numCompletedBy),' ',dbo.FormatedDateTimeFromDate(DATEADD(MINUTE,-@ClientTimeZoneOffset,WO.bintCompletedDate),@numDomainID)) AS CompletedBy,
		CONCAT(ISNULL(WO.numQtyBuilt,0),' (',WO.numQtyItemsReq,')') vcBuiltQty,
		(CASE 
			WHEN EXISTS (SELECT 
						WorkOrderDetails.numWODetailId
					FROM
						WorkOrderDetails
					OUTER APPLY
					(
						SELECT
							SUM(numPickedQty) numPickedQty
						FROM
							WorkOrderPickedItems
						WHERE
							WorkOrderPickedItems.numWODetailId = WorkOrderDetails.numWODetailId
					) TEMP
					WHERE
						WorkOrderDetails.numWOId=@numWOId
						AND ISNULL(numWareHouseItemId,0) > 0
						AND ISNULL(WorkOrderDetails.numQtyItemsReq,0) <> ISNULL(TEMP.numPickedQty,0)) 
			THEN 0 
			ELSE 1 
		END) bitBOMPicked
	FROM
		WorkOrder WO
	LEFT JOIN OpportunityMaster Opp 
	ON
		WO.numOppId=Opp.numoppid
    LEFT JOIN divisionMaster D2
           ON Opp.numDivisionID = D2.numDivisionID
	LEFT JOIN CompanyInfo C2 ON C2.numCompanyID = D2.numCompanyID
	LEFT JOIN AdditionalContactsInformation A ON Opp.numContactId = A.numContactId
	LEFT JOIN
		Sales_process_List_Master SPLFM
	ON
		WO.numBuildProcessId = SPLFM.Slp_Id
	INNER JOIN
		Item I
	ON
		WO.numItemCode = I.numItemCode
	WHERE
		WO.numDomainID=@numDomainID
		AND WO.numWOId = @numWOID

	SELECT
		WOD.numQtyItemsReq AS RequiredQty,
		WOD.monAverageCost,
		I.numItemCode,
		I.vcItemName,
		I.numAssetChartAcntId
	FROM 
		WorkOrderDetails WOD
	INNER JOIN
		Item I
	ON
		WOD.numChildItemID = I.numItemCode
	WHERE
		WOD.numWOId=@numWOID
		AND I.charItemType = 'P'

END