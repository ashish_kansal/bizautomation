GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ListDetails_Search')
DROP PROCEDURE USP_ListDetails_Search
GO

CREATE PROCEDURE USP_ListDetails_Search
	@numDomainID NUMERIC(18,0)
	,@numListID NUMERIC(18,0)
	,@vcSearchText VARCHAR(300)
	,@intOffset INT
	,@intPageSize INT
	,@intTotalRecords INT OUTPUT
AS 
BEGIN
	SELECT
		numListItemID
		,vcData
	FROM
		ListDetails
	WHERE
		numListID=@numListID
		AND (numDomainID=@numDomainID OR ISNULL(constFlag,0)=1)
		AND vcData LIKE CONCAT('%',@vcSearchText,'%')
	 ORDER BY 
		vcData
	OFFSET @intOffset ROWS FETCH NEXT @intPageSize ROWS ONLY;

	SET @intTotalRecords = (SELECT COUNT(*) FROM ListDetails WHERE numListID=@numListID AND (numDomainID=@numDomainID OR ISNULL(constFlag,0)=1) AND vcData LIKE CONCAT(@vcSearchText,'%'))
END
GO