GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_WarehouseItems_ClearInternalLocation')
DROP PROCEDURE USP_WarehouseItems_ClearInternalLocation
GO
CREATE PROCEDURE [dbo].[USP_WarehouseItems_ClearInternalLocation]  
	@numDomainID NUMERIC(18,0)
	,@numWarehouseItemID NUMERIC(18,0)
AS  
BEGIN  
	UPDATE
		WareHouseItems
	SET
		numWLocationID = 0
	WHERE
		numDomainID = @numDomainID
		AND numWareHouseItemID=@numWarehouseItemID
END
GO
