/****** Object:  StoredProcedure [dbo].[USP_UpdateOppSerializedItem_BizDoc_Comma]    Script Date: 07/26/2008 16:21:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_UpdateOppSerializedItem_BizDoc_Comma')
DROP PROCEDURE USP_UpdateOppSerializedItem_BizDoc_Comma
GO
CREATE PROCEDURE [dbo].[USP_UpdateOppSerializedItem_BizDoc_Comma]  
    @numOppItemTcode as numeric(9)=0,  
    @numOppID as numeric(9)=0,  
    @strItems as VARCHAR(1000)='',
    @numBizDocID as numeric(9)=0,
    @numWarehouseItmsID as numeric(9)=0
as  
-- STORE PROCEDURE LOGIC IS COMMENTED BECAUSE IT IS NOT USED NOW AND CODE IS NOT RELEVANT TO BIZ NOW.
--SELECT vcSerialNo,  isnull(numQty,0) 
--  - isnull((select sum(w.numQty) from OppWarehouseSerializedItem w INNER JOIN OpportunityMaster 
--  opp ON (w.numOppId=opp.numOppId AND opp.tintOppType=1) where isnull(opp.tintshipped,0)=0 and 
--  w.numWarehouseItmsDTLID=WareHouseItmsDTL.numWarehouseItmsDTLID),0)
--  
--  + isnull((select sum(w.numQty) from OppWarehouseSerializedItem w INNER JOIN OpportunityMaster 
--  opp ON (w.numOppId=opp.numOppId AND opp.tintOppType=1) where isnull(opp.tintshipped,0)=0 and 
--  w.numWarehouseItmsDTLID=WareHouseItmsDTL.numWarehouseItmsDTLID AND numOppBizDocsId=@numBizDocID),0)
--   as TotalQty,numWarehouseItmsDTLID
--INTO #tempTable
-- from   WareHouseItmsDTL   
--    where (tintStatus is null or tintStatus=0) and  numWareHouseItemID=@numWarehouseItmsID

--Create table #tempTable (                                                                    
-- vcSerialNo VARCHAR(100),TotalQty [numeric](18, 0),numWarehouseItmsDTLID [numeric](18, 0)                                             
-- )                       

--INSERT INTO #tempTable (vcSerialNo,numWarehouseItmsDTLID,TotalQty)  
--            SELECT vcSerialNo,WareHouseItmsDTL.numWareHouseItmsDTLID,
--  isnull(WareHouseItmsDTL.numQty,0) - isnull((select sum(w.numQty) from OppWarehouseSerializedItem w INNER JOIN OpportunityMaster opp ON (w.numOppId=opp.numOppId AND opp.tintOppType=1) where isnull(opp.tintshipped,0)=0 and w.numWarehouseItmsDTLID=WareHouseItmsDTL.numWarehouseItmsDTLID),0) + isnull(OppWarehouseSerializedItem.numQty,0) as TotalQty
-- from   OppWarehouseSerializedItem      
-- join WareHouseItmsDTL      
-- on WareHouseItmsDTL.numWareHouseItmsDTLID= OppWarehouseSerializedItem.numWarehouseItmsDTLID                           
-- where numOppID=@numOppID and  numWareHouseItemID=@numWarehouseItmsID and numOppBizDocsId=@numBizDocID ORDER BY vcSerialNo,TotalQty desc


--INSERT INTO #tempTable (vcSerialNo,numWarehouseItmsDTLID,TotalQty)  
--		 SELECT vcSerialNo,numWareHouseItmsDTLID,
--  isnull(numQty,0) - isnull((select sum(w.numQty) from OppWarehouseSerializedItem w INNER JOIN OpportunityMaster opp ON (w.numOppId=opp.numOppId AND opp.tintOppType=1) where isnull(opp.tintshipped,0)=0 and w.numWarehouseItmsDTLID=WareHouseItmsDTL.numWarehouseItmsDTLID),0) as TotalQty
-- from   WareHouseItmsDTL   
--    where (tintStatus is null or tintStatus=0) and  numWareHouseItemID=@numWarehouseItmsID  
--    and numWareHouseItmsDTLID not in(select numWarehouseItmsDTLID from OppWarehouseSerializedItem where numOppID=@numOppID and  numWarehouseItmsID=@numWarehouseItmsID and numOppBizDocsId=@numBizDocID) ORDER BY vcSerialNo,TotalQty desc

    
--Create table #tempLotSerial (                                                                    
-- vcSerialNo VARCHAR(100),UsedQty [numeric](18, 0),TotalQty [numeric](18, 0),numWarehouseItmsDTLID [numeric](18, 0)                                              
-- )                       

  
--DECLARE @posComma int, @strKeyVal varchar(20)

--SET @strItems=RTRIM(@strItems)
--IF RIGHT(@strItems, 1)!=',' SET @strItems=@strItems+','

--SET @posComma=PatIndex('%,%', @strItems)
--WHILE @posComma>1
--BEGIN
--	SET @strKeyVal=ltrim(rtrim(substring(@strItems, 1, @posComma-1)))
	
--	DECLARE @posBStart INT,@posBEnd int, @strQty varchar(20),@strName varchar(20)

--    SET @posBStart=PatIndex('%(%', @strKeyVal)
--    SET @posBEnd=PatIndex('%)%', @strKeyVal)
--	IF( @posBStart>1 AND @posBEnd>1)
--	BEGIN
--		    SET @strName=ltrim(rtrim(substring(@strKeyVal, 1, @posBStart-1)))
--			SET	@strQty=ltrim(rtrim(substring(@strKeyVal, @posBStart+1,len(@strKeyVal)-@posBStart-1)))
--	END		
			
--	ELSE
--	BEGIN
--			SET @strName=@strKeyVal
--			SET	@strQty=1
--	END
	  
--	PRINT  'Name->' + @strName + ':Qty->' + @strQty
	  
--	  DECLARE @TotalQty NUMERIC(9)
	  
--	 IF EXISTS(SELECT 1 FROM #tempTable WHERE vcSerialNo=@strName AND TotalQty>=@strQty)
--	 BEGIN
--	     INSERT INTO #tempLotSerial (vcSerialNo,UsedQty,TotalQty,numWarehouseItmsDTLID)  
--		 SELECT TOP 1 vcSerialNo,@strQty,TotalQty,numWarehouseItmsDTLID FROM #tempTable WHERE vcSerialNo=@strName AND TotalQty>=@strQty 
--	 END
	  
--	 SET @strItems=substring(@strItems, @posComma +1, len(@strItems)-@posComma)
--	 SET @posComma=PatIndex('%,%',@strItems)
	
--END

--update WareHouseItmsDTL set tintStatus=0 where numWareHouseItmsDTLID in 
--(select numWarehouseItmsDTLID from OppWarehouseSerializedItem where numOppItemID=@numOppItemTcode 
--AND numOppBizDocsId=@numBizDocID)

--delete from OppWarehouseSerializedItem where numOppItemID=@numOppItemTcode AND numOppBizDocsId=@numBizDocID

-- Insert into OppWarehouseSerializedItem(numWarehouseItmsDTLID,numOppID,numOppItemID,numWarehouseItmsID,numQty,numOppBizDocsId)                
--  (SELECT numWarehouseItmsDTLID,@numOppID,@numOppItemTcode,@numWarehouseItmsID,SUM(UsedQty),@numBizDocID                                            
--     FROM #tempLotSerial GROUP BY numWarehouseItmsDTLID)
      
--update WareHouseItmsDTL set tintStatus=1 where numWareHouseItmsDTLID in 
--(SELECT numWareHouseItmsDTLID FROM #tempLotSerial GROUP BY numWarehouseItmsDTLID having (SUM(TotalQty)-SUM(UsedQty)=0)
----SELECT numWareHouseItmsDTLID FROM #tempLotSerial WHERE (TotalQty-UsedQty)=0
--)


----SELECT * FROM #tempTable
----SELECT * FROM #tempLotSerial
--DROP TABLE #tempLotSerial
--DROP TABLE #tempTable

GO
