
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ManageUOM')
DROP PROCEDURE USP_ManageUOM
GO
CREATE PROCEDURE [dbo].[USP_ManageUOM]
	@numUOMId NUMERIC(9) OUTPUT,
	@numDomainID NUMERIC(9),
	@vcUnitName VARCHAR(100),
	@tintUnitType TINYINT,
	@bitEnabled BIT
AS

IF @numUOMId = 0 AND (EXISTS(SELECT [vcUnitName] from UOM Where [vcUnitName] = @vcUnitName AND [UOM].[numDomainId] = @numDomainID AND [UOM].[tintUnitType] = @tintUnitType))
BEGIN
		RAISERROR ('UOM_ALREADY_EXISTS',16,1);
		RETURN -1
END
IF @numUOMId = 0
BEGIN
	INSERT INTO UOM 
	(
	  [numDomainId],[vcUnitName],[tintUnitType],[bitEnabled]
	)
	VALUES 
	(
	   @numDomainId,@vcUnitName,@tintUnitType,@bitEnabled
	)
	
	SELECT @numUOMId = SCOPE_IDENTITY();
END
ELSE 
BEGIN
	UPDATE UOM SET 
		[vcUnitName] = @vcUnitName,
		[tintUnitType] = @tintUnitType,
		bitEnabled = @bitEnabled
	WHERE [numUOMId] = @numUOMId
END

