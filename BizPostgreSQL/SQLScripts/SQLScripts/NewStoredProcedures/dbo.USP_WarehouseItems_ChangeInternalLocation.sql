GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_WarehouseItems_ChangeInternalLocation')
DROP PROCEDURE USP_WarehouseItems_ChangeInternalLocation
GO
CREATE PROCEDURE [dbo].[USP_WarehouseItems_ChangeInternalLocation]  
	@numDomainID NUMERIC(18,0)
	,@numItemCode NUMERIC(18,0)
	,@numWarehouseItemID NUMERIC(18,0)
	,@numWarehouseID NUMERIC(18,0)
	,@numWLocationID NUMERIC(18,0)
AS  
BEGIN  
	IF NOT EXISTS (SELECT numWareHouseItemID FROM WareHouseItems WHERE numDomainID=@numDomainID AND numItemID=@numItemCode AND numWareHouseID=@numWarehouseID AND numWLocationID=@numWLocationID AND numWareHouseItemID <> @numWarehouseItemID )
	BEGIN
		UPDATE
			WareHouseItems
		SET
			numWLocationID = @numWLocationID
		WHERE
			numWareHouseItemID=@numWarehouseItemID
	END
	ELSE
	BEGIN
		RAISERROR('DUPLICATE_EXTERNAL_INTERNARL_COMBINATION',16,1)
	END
END
GO
