GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_MassSalesFulfillment_GetOrderItems')
DROP PROCEDURE dbo.USP_MassSalesFulfillment_GetOrderItems
GO

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[USP_MassSalesFulfillment_GetOrderItems]
(
	@numDomainID NUMERIC(18,0)
	,@numUserCntID NUMERIC(18,0)
	,@numOppID NUMERIC(18,0)
	,@tintMode TINYINT
	,@vcSelectedRecords VARCHAR(MAX)
)
AS 
BEGIN
	DECLARE @tintPackingMode TINYINT
	DECLARE @tintInvoicingType TINYINT
	DECLARE @numDefaultShippingBizDoc NUMERIC(18,0)

	SELECT
		@numDefaultShippingBizDoc = ISNULL(numDefaultSalesShippingDoc,0)
	FROM
		Domain
	WHERE
		numDomainID=@numDomainID

	SELECT
		@tintPackingMode=tintPackingMode
		,@tintInvoicingType=tintInvoicingType
	FROM
		MassSalesFulfillmentConfiguration
	WHERE
		MassSalesFulfillmentConfiguration.numDomainID = @numDomainID

	IF ISNULL(@vcSelectedRecords,'') <> ''
	BEGIN
		DECLARE @TempItems TABLE
		(	
			numOppItemID NUMERIC(18,0),
			numUnitHour FLOAT
		)

		INSERT INTO @TempItems
		(
			numOppItemID,
			numUnitHour
		)
		SELECT
			CAST(SUBSTRING(OutParam,0,CHARINDEX('-',OutParam)) AS NUMERIC(18,0))
			,CAST(SUBSTRING(OutParam,CHARINDEX('-',OutParam) + 1,LEN(OutParam)) AS FLOAT)
		FROM
			dbo.SplitString(@vcSelectedRecords,',')

		IF @tintMode = 1 -- For ship orders
		BEGIN
			SELECT
				numoppitemtCode
				,ISNULL(T1.numUnitHour,0) numUnitHour
				,monPrice
			FROM
				OpportunityItems
			INNER JOIN
				@TempItems T1
			ON
				OpportunityItems.numoppitemtCode=T1.numOppItemID
			WHERE
				numOppId = @numOppID
				AND ISNULL(bitMappingRequired,0) = 0
				AND (CASE 
						WHEN ISNULL(@tintPackingMode,1) = 3 
						THEN ISNULL(OpportunityItems.numQtyPicked,0) - ISNULL((SELECT 
																					SUM(SRI.intBoxQty)
																				FROM 
																					ShippingReport SR 
																				INNER JOIN 
																					ShippingBox SB 
																				ON 
																					SR.numShippingReportId=SB.numShippingReportId 
																				INNER JOIN 
																					ShippingReportItems SRI 
																				ON 
																					SB.numBoxID = SRI.numBoxID 
																				INNER JOIN
																					OpportunityBizDocItems OBDI
																				ON
																					 SRI.numOppBizDocItemID = OBDI.numOppBizDocItemID
																				WHERE 
																					SR.numDomainId = @numDomainID
																					AND SR.numOppID = @numOppID
																					AND LEN(ISNULL(SB.vcTrackingNumber,'')) > 0
																					AND OBDI.numOppItemID = OpportunityItems.numoppitemtCode),0) 
						WHEN ISNULL(@tintPackingMode,1) = 2 
						THEN ISNULL(OpportunityItems.numQtyPicked,0) - ISNULL(OpportunityItems.numQtyShipped,0) 
						ELSE ISNULL(OpportunityItems.numUnitHour,0) - ISNULL(OpportunityItems.numQtyShipped,0) 
					END) > 0				
		END
		ELSE IF @tintMode = 2 -- For invoicing orders
		BEGIN
			SELECT
				numoppitemtCode
				,ISNULL(T1.numUnitHour,0) numUnitHour
				,monPrice
			FROM
				OpportunityItems
			INNER JOIN
				@TempItems T1
			ON
				OpportunityItems.numoppitemtCode=T1.numOppItemID
			WHERE
				numOppId = @numOppID
				AND ISNULL(bitMappingRequired,0) = 0
				AND (CASE 
							WHEN ISNULL(@tintInvoicingType,0)=1 
							THEN ISNULL(OpportunityItems.numQtyShipped,0) 
							ELSE ISNULL(OpportunityItems.numUnitHour,0) 
						END) - ISNULL((SELECT 
											SUM(OpportunityBizDocItems.numUnitHour)
										FROM
											OpportunityBizDocs
										INNER JOIN
											OpportunityBizDocItems
										ON
											OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
										WHERE
											OpportunityBizDocs.numOppId = @numOppID
											AND OpportunityBizDocs.numBizDocId=287
											AND OpportunityBizDocItems.numOppItemID = OpportunityItems.numoppitemtcode),0) > 0
		END
		ELSE IF @tintMode = 3 -- For deffault shipping bizdoc
		BEGIN
			IF ISNULL(@numDefaultShippingBizDoc,0) > 0
			BEGIN
				SELECT
					numoppitemtCode
					,ISNULL(T1.numUnitHour,0) numUnitHour
					,monPrice
				FROM
					OpportunityItems
				INNER JOIN
					@TempItems T1
				ON
					OpportunityItems.numoppitemtCode=T1.numOppItemID
				WHERE
					numOppId = @numOppID
					AND ISNULL(bitMappingRequired,0) = 0
					AND ISNULL(OpportunityItems.numUnitHour,0)  - ISNULL((SELECT 
																		SUM(OpportunityBizDocItems.numUnitHour)
																	FROM
																		OpportunityBizDocs
																	INNER JOIN
																		OpportunityBizDocItems
																	ON
																		OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
																	WHERE
																		OpportunityBizDocs.numOppId = @numOppID
																		AND OpportunityBizDocs.numBizDocId=@numDefaultShippingBizDoc
																		AND OpportunityBizDocItems.numOppItemID = OpportunityItems.numoppitemtcode),0) > 0
			END
			ELSE
			BEGIN
				RAISERROR('DEFAULT_SHIPPING_BIZDOC_NOT_CONFIGURED',16,1)
			END
			
		END
	END
	ELSE
	BEGIN
		IF @tintMode = 1 -- For ship orders
		BEGIN
			SELECT
				numoppitemtCode
				,(CASE WHEN ISNULL(@tintPackingMode,1) = 2 THEN ISNULL(OpportunityItems.numQtyPicked,0) - ISNULL(OpportunityItems.numQtyShipped,0) ELSE ISNULL(OpportunityItems.numUnitHour,0) - ISNULL(OpportunityItems.numQtyShipped,0) END) numUnitHour
				,monPrice
			FROM
				OpportunityItems
			WHERE
				numOppId = @numOppID
				AND ISNULL(bitMappingRequired,0) = 0
				AND (CASE WHEN ISNULL(@tintPackingMode,1) = 2 THEN ISNULL(OpportunityItems.numQtyPicked,0) - ISNULL(OpportunityItems.numQtyShipped,0) ELSE ISNULL(OpportunityItems.numUnitHour,0) - ISNULL(OpportunityItems.numQtyShipped,0) END) > 0
		END
		ELSE IF @tintMode = 2 -- For invoicing orders
		BEGIN
			SELECT
				numoppitemtCode
				,(CASE 
						WHEN ISNULL(@tintInvoicingType,0)=1 
						THEN ISNULL(OpportunityItems.numQtyShipped,0) 
						ELSE ISNULL(OpportunityItems.numUnitHour,0) 
					END) - ISNULL((SELECT 
										SUM(OpportunityBizDocItems.numUnitHour)
									FROM
										OpportunityBizDocs
									INNER JOIN
										OpportunityBizDocItems
									ON
										OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
									WHERE
										OpportunityBizDocs.numOppId = @numOppID
										AND OpportunityBizDocs.numBizDocId=287
										AND OpportunityBizDocItems.numOppItemID = OpportunityItems.numoppitemtcode),0) numUnitHour
				,monPrice
			FROM
				OpportunityItems
			WHERE
				numOppId = @numOppID
				AND ISNULL(bitMappingRequired,0) = 0
				AND (CASE 
							WHEN ISNULL(@tintInvoicingType,0)=1 
							THEN ISNULL(OpportunityItems.numQtyShipped,0) 
							ELSE ISNULL(OpportunityItems.numUnitHour,0) 
						END) - ISNULL((SELECT 
											SUM(OpportunityBizDocItems.numUnitHour)
										FROM
											OpportunityBizDocs
										INNER JOIN
											OpportunityBizDocItems
										ON
											OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
										WHERE
											OpportunityBizDocs.numOppId = @numOppID
											AND OpportunityBizDocs.numBizDocId=287
											AND OpportunityBizDocItems.numOppItemID = OpportunityItems.numoppitemtcode),0) > 0
		END
		ELSE IF @tintMode = 3 -- For deffault shipping bizdoc
		BEGIN
			IF ISNULL(@numDefaultShippingBizDoc,0) > 0
			BEGIN
				SELECT
					numoppitemtCode
					,ISNULL(OpportunityItems.numUnitHour,0) - ISNULL((SELECT 
																		SUM(OpportunityBizDocItems.numUnitHour)
																	FROM
																		OpportunityBizDocs
																	INNER JOIN
																		OpportunityBizDocItems
																	ON
																		OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
																	WHERE
																		OpportunityBizDocs.numOppId = @numOppID
																		AND OpportunityBizDocs.numBizDocId=@numDefaultShippingBizDoc
																		AND OpportunityBizDocItems.numOppItemID = OpportunityItems.numoppitemtcode),0) numUnitHour
					,monPrice
				FROM
					OpportunityItems
				WHERE
					numOppId = @numOppID
					AND ISNULL(bitMappingRequired,0) = 0
					AND (CASE 
								WHEN ISNULL(@tintInvoicingType,0)=1 
								THEN ISNULL(OpportunityItems.numQtyShipped,0) 
								ELSE ISNULL(OpportunityItems.numUnitHour,0) 
							END) - ISNULL((SELECT 
												SUM(OpportunityBizDocItems.numUnitHour)
											FROM
												OpportunityBizDocs
											INNER JOIN
												OpportunityBizDocItems
											ON
												OpportunityBizDocs.numOppBizDocsId = OpportunityBizDocItems.numOppBizDocID
											WHERE
												OpportunityBizDocs.numOppId = @numOppID
												AND OpportunityBizDocs.numBizDocId=@numDefaultShippingBizDoc
												AND OpportunityBizDocItems.numOppItemID = OpportunityItems.numoppitemtcode),0) > 0
			END
			ELSE
			BEGIN
				RAISERROR('DEFAULT_SHIPPING_BIZDOC_NOT_CONFIGURED',16,1)
			END
		END
	END

END
GO