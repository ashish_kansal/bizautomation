GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created By Siva
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetCommissionCreditMemoOrRefundDetail')
DROP PROCEDURE USP_GetCommissionCreditMemoOrRefundDetail
GO
CREATE PROCEDURE [dbo].[USP_GetCommissionCreditMemoOrRefundDetail]
(
    @numUserId AS NUMERIC(18,0) = 0,
    @numUserCntID AS NUMERIC(18,0) = 0,
    @numDomainId AS NUMERIC(18,0) = 0,
    @ClientTimeZoneOffset AS INT,
    @numComPayPeriodID AS NUMERIC(18,0),
	@numComRuleID AS NUMERIC(18,0) = NULL
)
AS
BEGIN	
	SELECT
		RH.numReturnHeaderID
		,CI.vcCompanyName
		,RH.vcRMA
		,RH.vcBizDocName
		,RH.tintReturnType
		,RH.tintReceiveType
		,RH.monBizDocAmount
		,SUM(decCommission) decCommission
	FROM
		SalesReturnCommission SRC
	INNER JOIN
		ReturnHeader RH
	ON
		SRC.numReturnHeaderID = RH.numReturnHeaderID
	INNER JOIN
		DivisionMaster DM
	ON
		RH.numDivisionId=DM.numDivisionID
	INNER JOIN
		CompanyInfo CI
	ON
		DM.numCompanyID = CI.numCompanyId
	WHERE
		SRC.numComPayPeriodID = @numComPayPeriodID
		AND SRC.numUserCntID=@numUserCntID
		AND (ISNULL(@numComRuleID,0) = 0 OR SRC.numComRuleID = @numComRuleID)
		AND RH.numDomainId=@numDomainId
	GROUP BY
		RH.numReturnHeaderID
		,CI.vcCompanyName
		,RH.vcRMA
		,RH.vcBizDocName
		,RH.tintReturnType
		,RH.tintReceiveType
		,RH.monBizDocAmount
END
GO