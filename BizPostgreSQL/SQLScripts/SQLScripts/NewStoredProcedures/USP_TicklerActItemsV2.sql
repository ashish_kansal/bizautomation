GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_TicklerActItemsV2')
DROP PROCEDURE USP_TicklerActItemsV2
GO
CREATE Proc [dbo].[USP_TicklerActItemsV2]
@numUserCntID as numeric(9)=null,                                                                        
@numDomainID as numeric(9)=null,                                                                        
@startDate as datetime,                                                                        
@endDate as datetime,                                                                    
@ClientTimeZoneOffset Int,  --Added by Debasish to enable calculation of date according to client machine             
@bitFilterRecord bit=0,
@columnName varchar(50), 
@columnSortOrder varchar(50),
@vcBProcessValue varchar(500),
@RegularSearchCriteria VARCHAR(MAX)='',
@CustomSearchCriteria VARCHAR(MAX)='', 
@OppStatus int,
@CurrentPage int,                                                              
@PageSize int
AS
BEGIN
	SET @endDate = DATEADD(MILLISECOND,-2,DATEADD(DAY,1,@endDate))

--SET @PageSize=10
DECLARE @tintActionItemsViewRights TINYINT  = 3
DECLARE @bitREQPOApproval AS BIT
DECLARE @bitARInvoiceDue AS BIT
DECLARE @bitAPBillsDue AS BIT
DECLARE @vchARInvoiceDue AS VARCHAR(500)
DECLARE @vchAPBillsDue AS VARCHAR(500)
DECLARE @decReqPOMinValue AS DECIMAL(18,2)
DECLARE @vchREQPOApprovalEmp AS VARCHAR(500)=''
DECLARE @numDomainDivisionID NUMERIC(18,0)
DECLARE @dynamicPartQuery AS NVARCHAR(MAX)=''
DECLARE @ActivityRegularSearch AS VARCHAR(MAX)=''
DECLARE @OtherRegularSearch AS VARCHAR(MAX)=''
DECLARE @IndivRecordPaging AS NVARCHAR(MAX)=''
--SET @IndivRecordPaging=' '
DECLARE @TotalRecordCount INT =0
SET @IndivRecordPaging=  ' ORDER BY dtDueDate DESC OFFSET '+CAST(((@CurrentPage - 1 ) * @PageSize) AS VARCHAR) +' ROWS FETCH NEXT '+CAST(@PageSize AS VARCHAR)+' ROWS ONLY'

SELECT TOP 1 
	@bitREQPOApproval=bitREQPOApproval,
	@vchREQPOApprovalEmp=vchREQPOApprovalEmp,
	@decReqPOMinValue=ISNULL(decReqPOMinValue,0),
	@bitARInvoiceDue=bitARInvoiceDue,
	@bitAPBillsDue = bitAPBillsDue,
	@vchARInvoiceDue = vchARInvoiceDue,
	@vchAPBillsDue = vchAPBillsDue,
	@numDomainDivisionID=numDivisionID
FROM 
	Domain 
WHERE 
	numDomainId=@numDomainID

IF EXISTS (SELECT * FROM GroupAuthorization WHERE numDomainID=@numDomainID AND numModuleID=1 AND numPageID=1 AND numGroupID IN (SELECT ISNULL(numGroupID,0) FROM UserMaster WHERE numDomainID=@numDomainID AND numUserDetailId=@numUserCntID))
BEGIN
	SELECT @tintActionItemsViewRights=ISNULL(intViewAllowed,0) FROM GroupAuthorization WHERE numDomainID=@numDomainID AND numModuleID=1 AND numPageID=1 AND numGroupID IN (SELECT ISNULL(numGroupID,0) FROM UserMaster WHERE numDomainID=@numDomainID AND numUserDetailId=@numUserCntID)
END
--12 - Communication task
--1 - Case Task
--2 - Project Task
--3 - Opportunity Task
--4 - Sales Order Task
--5 - Work Order Management
--6 - Work Center Task
--7 - Requisition Approval
--8 - PO Approval
--9 - Price Margin Approval
--10 - A/R Invoice Due
--11 - A/P Bill Due

DECLARE @dynamicQuery AS NVARCHAR(MAX)=''
DECLARE @dynamicQueryCount AS NVARCHAR(MAX)=''

Create table #tempRecords (RecordCount NUMERIC(9),RecordId NUMERIC(9),numTaskId numeric(9),IsTaskClosed bit,TaskTypeName VARCHAR(MAX),TaskType NUMERIC(9),
OrigDescription NVARCHAR(MAX),Description NVARCHAR(MAX),TotalProgress INT,numAssignToId NUMERIC(9),
Priority NVARCHAR(MAX),PriorityId  NUMERIC(9),ActivityId NUMERIC(9),OrgName NVARCHAR(MAX),CompanyRating NVARCHAR(MAX),
numContactID numeric(9),numDivisionID numeric(9),tintCRMType tinyint,
Id numeric(9),bitTask varchar(100),Startdate datetime,EndTime datetime,
itemDesc text,[Name] varchar(200),vcFirstName varchar(100),vcLastName varchar(100),numPhone varchar(15),numPhoneExtension varchar(7),
[Phone] varchar(30),vcCompanyName varchar(100),vcProfile VARCHAR(100), vcEmail varchar(50),Task varchar(100),Activity varchar(MAX),
Status varchar(100),numCreatedBy numeric(9),numRecOwner VARCHAR(500), numModifiedBy VARCHAR(500), numOrgTerId NUMERIC(18,0), numTerId VARCHAR(200),numAssignedTo varchar(1000),numAssignedBy varchar(50),
 caseid numeric(9),vcCasenumber varchar(50),casetimeId numeric(9),caseExpId numeric(9),type int,itemid varchar(50),
changekey varchar(50),dtDueDate DATETIME, DueDate varchar(500),bitFollowUpAnyTime bit,numNoOfEmployeesId varchar(50),vcComPhone varchar(50),numFollowUpStatus varchar(200),dtLastFollowUp varchar(500),vcLastSalesOrderDate VARCHAR(100),monDealAmount DECIMAL(20,5), vcPerformance VARCHAR(100),vcColorScheme VARCHAR(50),
Slp_Name varchar(100),intTotalProgress INT,vcPoppName varchar(100), numOppId NUMERIC(18,0),numBusinessProcessID numeric(18,0),
numCompanyRating varchar(500),numStatusID varchar(500),numCampaignID varchar(500),[Action-Item Participants] NVARCHAR(MAX),HtmlLink varchar(500),
Location nvarchar(150),OrignalDescription text,numTaskEstimationInMinutes INT,numTimeSpentInMinutes INT,dtLastStartDate DATETIME,numRemainingQty FLOAT)                                                         
 

declare @strSql as varchar(MAX)                                                            
declare @strSql1 as varchar(MAX)                         
declare @strSql2 as varchar(MAX)                                                            
declare @vcCustomColumnName AS VARCHAR(8000)
declare @vcnullCustomColumnName AS VARCHAR(8000)
   -------Change Row Color-------
Declare @vcCSOrigDbCOlumnName as varchar(50),@vcCSLookBackTableName as varchar(50),@vcCSAssociatedControlType as varchar(50)
SET @vcCSOrigDbCOlumnName=''
SET @vcCSLookBackTableName=''

declare @tintOrder as tinyint                                                
declare @vcFieldName as varchar(50)                                                
declare @vcListItemType as varchar(3)                                           
declare @vcAssociatedControlType varchar(20)                                                
declare @numListID AS numeric(9)                                                
declare @vcDbColumnName varchar(20)                    
declare @WhereCondition varchar(2000)                     
declare @vcLookBackTableName varchar(2000)              
Declare @bitCustomField as BIT
DECLARE @bitAllowEdit AS CHAR(1)                 
Declare @numFieldId as numeric  
DECLARE @bitAllowSorting AS CHAR(1)   
declare @vcColumnName AS VARCHAR(500)       
declare @vcCustomColumnNameOnly AS VARCHAR(500)       


IF(@RegularSearchCriteria='')
BEGIN
	SET @RegularSearchCriteria =@RegularSearchCriteria + ' 1=1'
END
if(@OppStatus=0)
BEGIN
SET @RegularSearchCriteria =@RegularSearchCriteria + ' AND IsTaskClosed = 0'
END
if(@OppStatus=1)
BEGIN
SET @RegularSearchCriteria = @RegularSearchCriteria + ' AND IsTaskClosed = 1'
END
 SET @RegularSearchCriteria  = REPLACE(@RegularSearchCriteria,'TaskTypeName','TaskType')

 SET @ActivityRegularSearch = ''
 SET @OtherRegularSearch = ''

 SET @RegularSearchCriteria = REPLACE(@RegularSearchCriteria,'DueDate','dtDueDate')

 SET @ActivityRegularSearch = ISNULL((SELECT STUFF( 
(
	SELECT  'AND ' + [Items] FROM 
	(
	  SELECT [Items] FROM SplitByString(@RegularSearchCriteria,'AND') WHERE Items<>'' AND 1=
	  (CASE WHEN Items LIKE '%IsTaskClosed%' THEN 0
	 WHEN Items LIKE '%TaskType%'  THEN 0
	 WHEN  Items LIKE '%numAssignToId%'  THEN 0
	 WHEN  Items LIKE '%PriorityId%'  THEN 0
	 WHEN  Items LIKE '%ActivityId%'  THEN 0
	 WHEN  Items LIKE '%Description%'   THEN 0
	 WHEN Items LIKE '%dtDueDate%' THEN 0
	 WHEN  Items LIKE '%OrgName%' THEN 0 ELSE 1 END)
	) AS T FOR XML PATH('')
)
,1,3,'') AS [Items]),'')
SET @ActivityRegularSearch = REPLACE(@ActivityRegularSearch,'&gt;','>')
SET @ActivityRegularSearch = REPLACE(@ActivityRegularSearch,'&lt;','<')
SET @ActivityRegularSearch = REPLACE(@ActivityRegularSearch,'dtDueDate','DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +', dtStartTime)')
 SET @OtherRegularSearch = ISNULL((SELECT STUFF( 
(
	SELECT  'AND ' + [Items] FROM 
	(
	  SELECT [Items] FROM SplitByString(@RegularSearchCriteria,'AND') WHERE Items<>'' AND 1=
	  (CASE WHEN Items LIKE '%IsTaskClosed%' THEN 1
	 WHEN Items LIKE '%TaskType%'  THEN 1
	 WHEN  Items LIKE '%numAssignToId%'  THEN 1
	 WHEN  Items LIKE '%PriorityId%'  THEN 1
	 WHEN  Items LIKE '%ActivityId%'  THEN 1
	 WHEN  Items LIKE '%Description%'   THEN 1
	 WHEN  Items LIKE '%dtDueDate%'   THEN 1
	 WHEN  Items LIKE '%OrgName%'   THEN 1 ELSE 0 END)
	) AS T FOR XML PATH('')
)
,1,3,'') AS [Items]),'')
SET @OtherRegularSearch = ISNULL(REPLACE(@OtherRegularSearch,'T.',''),'')
SET @OtherRegularSearch = REPLACE(@OtherRegularSearch,'&gt;','>')
SET @OtherRegularSearch = REPLACE(@OtherRegularSearch,'&lt;','<')
IF(LEN(@OtherRegularSearch)=0)
BEGIN
	SET @OtherRegularSearch = ' 1=1 '
END
Declare @ListRelID as numeric(9)             
set @tintOrder=0   

CREATE TABLE #tempForm 
	(
		tintOrder INT,vcDbColumnName nvarchar(50),vcFieldDataType nvarchar(50),vcOrigDbColumnName nvarchar(50),bitAllowFiltering BIT,vcFieldName nvarchar(50)
		,vcAssociatedControlType nvarchar(50),vcListItemType char(3),numListID numeric(9),vcLookBackTableName varchar(50),bitCustomField BIT,numFieldId NUMERIC
		,bitAllowSorting BIT,bitAllowEdit BIT,bitIsRequired BIT,bitIsEmail BIT,bitIsAlphaNumeric BIT,bitIsNumeric BIT,bitIsLengthValidation BIT,intMaxLength int
		,intMinLength int,bitFieldMessage BIT,vcFieldMessage VARCHAR(500),ListRelID numeric(9),intColumnWidth NUMERIC(18,2)
	)
                
	--Custom Fields related to Organization
	INSERT INTO 
		#tempForm
	SELECT  
		tintRow + 1 AS tintOrder,vcDbColumnName,'',vcDbColumnName,1,vcFieldName,vcAssociatedControlType,'' as vcListItemType,numListID,'' as vcLookBackTableName                                                 
		,bitCustom,numFieldId,bitAllowSorting,0,bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,intMaxLength,intMinLength
		,bitFieldMessage,vcFieldMessage vcFieldMessage,ListRelID,intColumnWidth
	FROM 
		View_DynamicCustomColumns
	WHERE 
		numFormId=43 
		AND numUserCntID=@numUserCntID 
		AND numDomainID=@numDomainID 
		AND tintPageType=1
		AND ISNULL(bitCustom,0)=1
 
	SELECT TOP 1 
		@tintOrder = tintOrder + 1
		,@vcDbColumnName=vcDbColumnName
		,@vcFieldName=vcFieldName
		,@vcAssociatedControlType=vcAssociatedControlType
		,@vcListItemType=vcListItemType
		,@numListID=numListID
		,@vcLookBackTableName=vcLookBackTableName                                               
		,@bitCustomField=bitCustomField
		,@numFieldId=numFieldId
		,@bitAllowSorting=bitAllowSorting
		,@bitAllowEdit=0
		,@ListRelID=ListRelID
	FROM 
		#tempForm 
	ORDER BY 
		tintOrder ASC     
  
	SET @vcCustomColumnName=''
	SET @vcnullCustomColumnName=''
	SET @vcCustomColumnNameOnly = ''
	WHILE @tintOrder>0      
	BEGIN  
		SET @vcColumnName= @vcDbColumnName
		SET @strSql = 'ALTER TABLE #tempRecords ADD [' + @vcColumnName + '] NVARCHAR(MAX)'
		SET @vcCustomColumnNameOnly = @vcCustomColumnNameOnly+','+@vcColumnName
		EXEC(@strSql)
		
		IF @bitCustomField = 1
		BEGIN 
			IF @vcAssociatedControlType = 'TextBox' OR @vcAssociatedControlType = 'TextArea'            
			BEGIN            
				SET @vcCustomColumnName= @vcCustomColumnName+', ISNULL((SELECT TOP 1 CFW.Fld_Value FROM CFW_FLD_Values AS CFW WHERE CFW.Fld_ID='+CAST(@numFieldId AS VARCHAR(50))+' AND CFW.RecId=Div.numDivisionID),'''') ['+ @vcColumnName +']'
				SET @vcnullCustomColumnName= @vcnullCustomColumnName+','''' AS  ['+ @vcColumnName +']'
			END  
			ELSE IF @vcAssociatedControlType = 'CheckBox'         
			BEGIN            
				SET @vcCustomColumnName= @vcCustomColumnName+',ISNULL((SELECT TOP 1 CFW.Fld_Value FROM CFW_FLD_Values AS CFW WHERE CFW.Fld_ID='+CAST(@numFieldId AS VARCHAR(50))+' AND CFW.RecId=Div.numDivisionID),0)  ['+ @vcColumnName + ']'
				SET @vcnullCustomColumnName= @vcnullCustomColumnName+',0 AS ['+ @vcColumnName + ']'
			END            
			ELSE IF @vcAssociatedControlType = 'DateField'        
			BEGIN            
				SET @vcCustomColumnName= @vcCustomColumnName+',dbo.FormatedDateFromDate((SELECT TOP 1 CFW.Fld_Value FROM CFW_FLD_Values AS CFW WHERE CFW.Fld_ID='+CAST(@numFieldId AS VARCHAR(50))+' AND CFW.RecId=Div.numDivisionID),'+convert(varchar(10),@numDomainId)+')  ['+ @vcColumnName +']'              
				SET @vcnullCustomColumnName= @vcnullCustomColumnName+','''' AS  ['+ @vcColumnName +']'
			END            
			ELSE IF @vcAssociatedControlType = 'SelectBox'           
			BEGIN            
				SET @vcCustomColumnName=@vcCustomColumnName+',(SELECT TOP 1 ISNULL(L.vcData,'''') FROM CFW_FLD_Values CFW left Join ListDetails L on L.numListItemID=CFW.Fld_Value WHERE  CFW.Fld_ID='+CAST(@numFieldId AS VARCHAR(50))+' AND CFW.RecId=Div.numDivisionID)'+' ['+ @vcColumnName +']'                                                         
				SET @vcnullCustomColumnName=@vcnullCustomColumnName+','''''+'AS  ['+ @vcColumnName +']'                                                         
			END
			ELSE IF @vcAssociatedControlType = 'CheckBoxList'
			BEGIN
				SET @vcCustomColumnName=@vcCustomColumnName+',(SELECT STUFF((SELECT CONCAT('', '', vcData) FROM ListDetails WHERE numListID= ' + CAST(@numListID AS varchar) + ' AND numlistitemid IN (SELECT ISNULL(Id,0) FROM dbo.SplitIDs(ISNULL((SELECT CFWInner.Fld_Value FROM CFW_FLD_Values CFWInner WHERE CFWInner.Fld_ID='+CAST(@numFieldId AS VARCHAR(50))+' AND CFWInner.RecId=Div.numDivisionID),''''),'','')) FOR XML PATH('''')), 1, 1, ''''))'+' ['+ @vcColumnName +']'
				SET @vcnullCustomColumnName=@vcnullCustomColumnName+','''''+'AS  ['+ @vcColumnName +']'
			END
			ELSE
			BEGIN
				SET @vcCustomColumnName= CONCAT(@vcCustomColumnName,',(SELECT dbo.fn_GetCustFldStringValue(',@numFieldId,',CFW.RecId,CFW.Fld_Value',') FROM CFW_FLD_Values CFW left Join ListDetails L on L.numListItemID=CFW.Fld_Value WHERE  CFW.Fld_ID=',@numFieldId,' AND CFW.RecId=Div.numDivisionID)',' [', @vcColumnName ,']')
				SET @vcnullCustomColumnName=@vcnullCustomColumnName+','''''+'AS  ['+ @vcColumnName +']' 
			END
		END
		  
		SELECT TOP 1 
			@tintOrder=tintOrder + 1
			,@vcDbColumnName=vcDbColumnName
			,@vcFieldName=vcFieldName
			,@vcAssociatedControlType=vcAssociatedControlType
			,@vcListItemType=vcListItemType
			,@numListID=numListID
			,@vcLookBackTableName=vcLookBackTableName                                               
			,@bitCustomField=bitCustomField
			,@numFieldId=numFieldId
			,@bitAllowSorting=bitAllowSorting
			,@bitAllowEdit=0
			,@ListRelID=ListRelID
		FROM 
			#tempForm 
		WHERE 
			tintOrder > @tintOrder-1 
		ORDER BY 
			tintOrder asc            
 
		IF @@rowcount=0 set @tintOrder=0 
	END
SET @strSql=''

Create table #tempColorScheme(vcOrigDbCOlumnName varchar(50),vcLookBackTableName varchar(50),vcFieldValue varchar(50),vcFieldValue1 varchar(50),vcColorScheme varchar(50),vcAssociatedControlType varchar(50))

insert into #tempColorScheme select DFM.vcOrigDbCOlumnName,DFM.vcLookBackTableName,DFCS.vcFieldValue,DFCS.vcFieldValue1,DFCS.vcColorScheme,DFFM.vcAssociatedControlType 
from DycFieldColorScheme DFCS join DycFormField_Mapping DFFM on DFCS.numFieldID=DFFM.numFieldID
join DycFieldMaster DFM on DFM.numModuleID=DFFM.numModuleID and DFM.numFieldID=DFFM.numFieldID
where DFCS.numDomainID=@numDomainID and DFFM.numFormID=43 AND DFCS.numFormID=43 and isnull(DFFM.bitAllowGridColor,0)=1

IF(SELECT COUNT(*) FROM #tempColorScheme)>0
BEGIN
   select top 1 @vcCSOrigDbCOlumnName=vcOrigDbCOlumnName,@vcCSLookBackTableName=vcLookBackTableName,@vcCSAssociatedControlType=vcAssociatedControlType from #tempColorScheme
END   
-------------------- 0 - TASK----------------------------

IF(LEN(@CustomSearchCriteria)>0)
BEGIN
SET @CustomSearchCriteria = ' AND '+@CustomSearchCriteria
END
IF(LEN(@ActivityRegularSearch)=0)
BEGIN
	SET @ActivityRegularSearch = '1=1'
END
DECLARE @StaticColumns AS NVARCHAR(MAX)=''

SET @StaticColumns = ' RecordCount,RecordId,
numCreatedBy,
numOrgTerId,
numTaskId,
IsTaskClosed,
TaskTypeName,
TaskType,
OrigDescription,
Description,
dtDueDate,
DueDate,
TotalProgress,
numAssignedTo,
numAssignToId,
Priority,
Activity,
PriorityId,
ActivityId,
OrgName,
CompanyRating,
numContactID,numDivisionID,tintCRMType,Id,bitTask,Startdate,EndTime,itemDesc,
[Name],vcFirstName,vcLastName,numPhone,numPhoneExtension,[Phone],vcCompanyName,vcProfile,vcEmail,Task,Status,numRecOwner,numModifiedBy,numTerId,numAssignedBy,caseid,vcCasenumber,
casetimeId,caseExpId,type,itemid,bitFollowUpAnyTime,numNoOfEmployeesId,vcComPhone,numFollowUpStatus,dtLastFollowUp,vcLastSalesOrderDate,monDealAmount,vcPerformance'
SET @dynamicQuery = ' INSERT INTO #tempRecords(
 '+@StaticColumns +@vcCustomColumnNameOnly+'
) SELECT COUNT(*) OVER() RecordCount,* FROM (SELECT 
Comm.numCommId AS RecordId,
Comm.numCreatedBy AS numCreatedBy, 
ISNULL(Div.numTerId,0) AS numOrgTerId,
-2 AS numTaskId,
bitClosedFlag AS IsTaskClosed,
CONCAT(''<div><div class="pull-left"><a onclick="openCommTask('',Comm.numCommId,'',0)" href="javascript:void(0)">'',dbo.GetListIemName(Comm.bitTask),''</a><br/>'',ISNULL(Comp.vcCompanyName,''''),(CASE WHEN AddC.numContactID IS NOT NULL THEN CONCAT(''&nbsp;<a class="text-green" href="javascript:OpenContact('',AddC.numContactID,'',1)">('',ISNULL(AddC.vcFirstName,''-''),'' '',ISNULL(AddC.vcLastName,''-''),'')</a>'') ELSE '''' END),''<br/>'',(CASE WHEN ISNULL(AddC.numPhone,'''') <> '''' THEN AddC.numPhone ELSE '''' END),(CASE WHEN ISNULL(AddC.numPhoneExtension,'''') <> '''' THEN CONCAT(''&nbsp;('',AddC.numPhoneExtension,'')'') ELSE '''' END),(CASE WHEN ISNULL(AddC.vcEmail,'''') <> '''' THEN CONCAT(''&nbsp;<img src="../images/msg_unread_small.gif" style="cursor:pointer" onclick="return OpemEmail('''''',AddC.vcEmail,'''''','',AddC.numcontactID,'')" />'') ELSE '''' END),''</div><div class="pull-right"><ul class="list-inline"><li><button class="btn btn-flat btn-task-finish" onclick="return ActionItemFinished('',Comm.numCommId,'');"><img src="../images/comflag.png" />&nbsp;Finish</button></li><li><button class="btn btn-comm-followup btn-flat btn-warning" onclick="return ActionItemFollowup('',Comm.numCommId,'','',AddC.numcontactID,'');">Follow-up</button></li></ul></div></div>'') AS TaskTypeName,
Comm.bitTask AS TaskType,
'''' AS OrigDescription,
comm.textDetails AS Description,
DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +', dtStartTime) AS dtDueDate,
DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +', dtStartTime) as DueDate,
''-'' As TotalProgress,
dbo.fn_GetContactName(numAssign) As numAssignedTo,
numAssign As numAssignToId,
listdetailsStatus.VcData As Priority, -- Priority column   
 listdetailsActivity.VcData As Activity ,
listdetailsStatus.NumlistItemID As PriorityId, -- Priority column   
 listdetailsActivity.NumlistItemID As ActivityId ,
 ''<a href="../account/frmAccounts.aspx?DivID=''+CAST(Div.numDivisionID AS VARCHAR)+''">''+Comp.vcCompanyName+ CASE WHEN  ISNULL(listCompanyRating.VcData,'''')<>'''' THEN ''(''+listCompanyRating.VcData+'')'' ELSE '''' END +''</a>''  AS OrgName,
 listCompanyRating.VcData As CompanyRating,
 Addc.numContactID,Addc.numDivisionID,Div.tintCRMType,Comm.numCommId AS Id,convert(varchar(15),bitTask) as bitTask,
  DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +', dtStartTime) as Startdate,                        
 DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +', dtEndTime) as EndTime ,                                                                        
 Comm.textDetails AS itemDesc,  
 AddC.vcFirstName + '' '' + AddC.vcLastName  as [Name], 
 AddC.vcFirstName,AddC.vcLastName,Addc.numPhone,AddC.numPhoneExtension,                               
 case when Addc.numPhone<>'''' then + AddC.numPhone +case when AddC.numPhoneExtension<>'''' then '' - '' + AddC.numPhoneExtension else '''' end  else '''' end as [Phone], 
  Comp.vcCompanyName As vcCompanyName , ISNULL((SELECT vcData FROM ListDetails WHERE numListID =21 AND numDomainID='+ CAST(@numDomainID AS VARCHAR) +' AND  numListItemID=Comp.vcProfile ) ,'''') vcProfile ,                                                        
 AddC.vcEmail As vcEmail ,    
 dbo.GetListIemName(Comm.bitTask)AS Task,   
 listdetailsStatus.VcData As Status,dbo.fn_GetContactName(Comm.numCreatedBy) AS numRecOwner,dbo.fn_GetContactName(Comm.numModifiedBy) AS numModifiedBy,  
  (select TOP 1 vcData from ListDetails where numListItemID=Div.numTerId) AS numTerId,dbo.fn_GetContactName(Comm.numAssignedBy) AS numAssignedBy,
  convert(varchar(50),comm.caseid) as caseid,(select top 1 vcCaseNumber from cases where cases.numcaseid= comm.caseid )as vcCasenumber,
  ISNULL(comm.casetimeId,0) casetimeId,ISNULL(comm.caseExpId,0) caseExpId,0 as type,'''' as itemid,ISNULL(bitFollowUpAnyTime,0) bitFollowUpAnyTime,  
  dbo.GetListIemName(Comp.numNoOfEmployeesId) AS numNoOfEmployeesId,Div.vcComPhone,
dbo.GetListIemName(Div.numFollowUpStatus) AS numFollowUpStatus,' + 
CONCAT('ISNULL(dbo.FormatedDateFromDate(Div.dtLastFollowUp,',@numDomainID,'),'''')') + ' AS dtLastFollowUp,
' + CONCAT('ISNULL(dbo.FormatedDateFromDate(DateAdd(minute, ',-@ClientTimeZoneOffset,',TempLastOrder.bintCreatedDate),',@numDomainID,'),'''')') + ' AS vcLastSalesOrderDate,ISNULL(TempPerformance.monDealAmount,0) monDealAmount,
(CASE WHEN ISNULL(TempPerformance.monDealAmount,0) > 0 THEN CONCAT(FORMAT(TempPerformance.monDealAmount,''##,####.00''),'' ('',CAST(TempPerformance.fltProfitPer AS DECIMAL(18,2)),''%)'') ELSE '''' END) AS vcPerformance
  '+@vcCustomColumnName+'
 FROM  Communication Comm                              
 JOIN AdditionalContactsInformation AddC                                                  
 ON Comm.numContactId = AddC.numContactId                                  
 JOIN DivisionMaster Div                                                                         
 ON AddC.numDivisionId = Div.numDivisionID                                   
 JOIN CompanyInfo  Comp                                                                         
 ON Div.numCompanyID = Comp.numCompanyId  
 OUTER APPLY(' + CONCAT('SELECT MAX(bintCreatedDate) AS bintCreatedDate FROM OpportunityMaster WHERE numDomainID = ',@numDomainID,' AND numDivisionID=Div.numDivisionID AND tintOppType = 1 AND tintOppStatus = 1') + ' ) AS TempLastOrder
 OUTER APPLY(' + CONCAT('SELECT
																	SUM(monDealAmount) AS monDealAmount
																	,AVG(ProfitPer) AS fltProfitPer
																FROM
																(
																	SELECT
																		OM.numDivisionId
																		,monDealAmount
																		,AVG(((ISNULL(OI.monPrice,0) - (CASE D.numCost WHEN 3 THEN ISNULL(V.monCost,0) * dbo.fn_UOMConversion(I.numBaseUnit,I.numItemCode,I.numDomainID,I.numPurchaseUnit) WHEN 2 THEN ISNULL(OI.numCost,0) ELSE ISNULL(I.monAverageCost,0) END)) / ISNULL(NULLIF(OI.monPrice,0),1)) * 100) ProfitPer
																	FROM
																		OpportunityMaster OM
																	INNER JOIN
																		Domain D
																	ON
																		OM.numDomainID = D.numDomainID
																	INNER JOIN
																		OpportunityItems OI
																	ON
																		OM.numOppId = OI.numOppId
																	INNER JOIN
																		Item I
																	ON
																		OI.numItemCode = I.numItemCode
																	LEFT JOIN
																		Vendor V
																	ON
																		V.numVendorID = I.numVendorID
																		AND V.numItemCode = I.numItemCode
																	WHERE
																		OM.numDomainId = ',@numDomainID,'
																		AND OM.numDivisionId = Div.numDivisionID
																		AND ISNULL(I.bitContainer,0) = 0
																		AND tintOppType = 1
																		AND tintOppStatus=1
																	GROUP BY
																		OM.numDivisionId
																		,OM.numOppId
																		,monDealAmount
																) T1
																GROUP BY
																	T1.numDivisionId )') + ' AS TempPerformance
 LEFT JOIN OpportunityMaster Opp ON Opp.numDivisionId = Div.numDivisionID AND Opp.numContactId = AddC.numContactId  AND Opp.numOppID = Comm.numOppID 
 LEFT JOIN Sales_process_List_Master splm ON splm.Slp_Id = Opp.numBusinessProcessID 
 LEFT JOIN ProjectProgress PP On PP.numOppId = Opp.numOppId
  Left Join listdetails listdetailsStatus                                                 
 On Comm.numStatus = listdetailsStatus.NumlistItemID
  Left Join listdetails listdetailsActivity                                                  
 On Comm.numActivity = listdetailsActivity.NumlistItemID
 LEFT JOIN ListDetails AS listCompanyRating
 ON Comp.numCompanyRating=listCompanyRating.NumlistItemID
WHERE '+@ActivityRegularSearch+' AND
	Comm.numDomainID = ' + Cast(@numDomainID as varchar(10))  + ' AND 1 = (CASE ' + CAST( @tintActionItemsViewRights AS VARCHAR) + 
' WHEN 3 THEN 1 
WHEN 2 THEN (CASE WHEN Div.numTerID in (select numTerritoryID from  UserTerritory where numUserCntID= '+ CONVERT(VARCHAR(15),@numUserCntID) + ' ) THEN 1 ELSE 0 END)
WHEN 1 THEN (CASE WHEN (Comm.numCreatedBy= ' + CONVERT(VARCHAR(15),@numUserCntID) + ' or Comm.numAssign= '+ CONVERT(VARCHAR(15),@numUserCntID) + ') THEN 1 ELSE 0 END) 
ELSE 0 END)  AND (DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',dtStartTime) >= '''+Cast(@startDate as varchar(30))+''' and DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',dtStartTime) <= '''+Cast(@endDate as varchar(30))+''')
'+@CustomSearchCriteria+'
 )   Q WHERE '+@OtherRegularSearch+'
  '+@IndivRecordPaging+'
'


EXEC (@dynamicQuery)

-------------------- 0 - Email Communication----------------------------
--''<a onclick="openCommTask(''+CAST(ac.activityid AS VARCHAR)+'',1)" href="javascript:void(0)">Calendar</a>'' As TaskTypeName,
IF(LEN(@ActivityRegularSearch)=0 OR RTRIM(LTRIM(@ActivityRegularSearch))='1=1')
BEGIN
DECLARE @FormattedItems As VARCHAR(MAX)
SET @FormattedItems ='
RecordCount,RecordId,
numCreatedBy,
numOrgTerId,
numTaskId,
IsTaskClosed,
TaskTypeName,
TaskType,
OrigDescription,
Description,
dtDueDate,
DueDate,
TotalProgress,
numAssignedTo,
numAssignToId,
Priority,
Activity,
PriorityId,
ActivityId,
OrgName,
CompanyRating,Startdate,EndTime,type'
SET @dynamicQuery =  ' INSERT INTO #tempRecords(
'+@FormattedItems+'
) SELECT  COUNT(*) OVER() RecordCount,* FROM (  SELECT 
ac.activityid AS RecordId,
0 AS numCreatedBy,
ISNULL(Div.numTerId,0) AS numOrgTerId,
-2 AS numTaskId,
case when alldayevent = 1 then ''1'' else ''0'' end AS IsTaskClosed,
CONCAT(''<div><div class="pull-left"><a href="javascript:void(0)">Calendar</a><br/>'',ISNULL(Comp.vcCompanyName,''''),(CASE WHEN ACI.numContactID IS NOT NULL THEN CONCAT(''&nbsp;<a class="text-green" href="javascript:OpenContact('',ACI.numContactID,'',1)">('',ISNULL(ACI.vcFirstName,''-''),'' '',ISNULL(ACI.vcLastName,''-''),'')</a>'') ELSE '''' END),''<br/>'',(CASE WHEN ISNULL(ACI.numPhone,'''') <> '''' THEN ACI.numPhone ELSE '''' END),(CASE WHEN ISNULL(ACI.numPhoneExtension,'''') <> '''' THEN CONCAT(''&nbsp;('',ACI.numPhoneExtension,'')'') ELSE '''' END),(CASE WHEN ISNULL(ACI.vcEmail,'''') <> '''' THEN CONCAT(''&nbsp;<img src="../images/msg_unread_small.gif" style="cursor:pointer" onclick="return OpemEmail('''''',ACI.vcEmail,'''''','',ACI.numcontactID,'')" />'') ELSE '''' END),''</div><div class="pull-right"><button class="btn btn-flat btn-task-finish" onclick="return CalendarFinished('',ac.activityid,'');"><img src="../images/comflag.png" />&nbsp;Finish</button></div></div>'') AS TaskTypeName,
12 AS TaskType,
CASE WHEN ISNULL(ac.Comments,'''') ='''' THEN ac.ActivityDescription ELSE ISNULL(ac.Comments,'''') END AS OrigDescription,
(SELECT DISTINCT  
replace(replace((SELECT 
CASE(ISNULL(bitpartycalendarTitle,1)) WHEN 1 THEN ''<b><font color=red>Title</font></b>'' + CASE(ISNULL(ac.Subject,'''')) WHEN '''' THEN '''' else ac.Subject END + ''<br>'' else ''''  END  
+ CASE(ISNULL(bitpartycalendarLocation,1)) WHEN 1 THEN ''<b><font color=green>Location</font></b>'' + CASE(ISNULL(ac.location,'''')) WHEN '''' THEN '''' else ac.location END + ''<br>'' else ''''  END  
+ CASE ISNULL(bitpartycalendarDescription,1) 
	WHEN 1 
	THEN CONCAT(''<b><font color=#3c8dbc>Description</font></b>'',
				(CASE 
					WHEN LEN(CASE WHEN ISNULL(ac.Comments,'''') ='''' THEN ac.ActivityDescription ELSE ISNULL(ac.Comments,'''') END) > 100 AND NOT ((CASE WHEN ISNULL(ac.Comments,'''') ='''' THEN ac.ActivityDescription ELSE ISNULL(ac.Comments,'''') END) like ''%<[A-Za-z0-9]%'' OR (CASE WHEN ISNULL(ac.Comments,'''') ='''' THEN ac.ActivityDescription ELSE ISNULL(ac.Comments,'''') END) like ''%</[A-Za-z0-9]%'')
					THEN CONCAT(CAST((CASE WHEN ISNULL(ac.Comments,'''') ='''' THEN ac.ActivityDescription ELSE ISNULL(ac.Comments,'''') END) AS VARCHAR(100)),CONCAT(''...'',''<a href="#" role="button" title="Description" data-toggle="popover" data-content="'',(CASE WHEN ISNULL(ac.Comments,'''') ='''' THEN ac.ActivityDescription ELSE ISNULL(ac.Comments,'''') END) ,''"> more</a>'')) 
					ELSE (CASE WHEN ISNULL(ac.Comments,'''') ='''' THEN ac.ActivityDescription ELSE ISNULL(ac.Comments,'''') END) 
				END)) 
	ELSE ''''  
	END  
FROM Domain where numDomainId='+ CAST(@numDomainID AS VARCHAR) +' 
FOR XML PATH('''')
),''&lt;'', ''<''), ''&gt;'', ''>''))   AS Description,
DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +', dateadd(second,duration,startdatetimeutc)) AS dtDueDate,
DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +', dateadd(second,duration,startdatetimeutc)) as DueDate,
''0'' As TotalProgress,
''-'' As numAssignedTo,
''0'' As numAssignToId,
'''' As Priority, -- Priority column   
'''' As Activity ,
0 As PriorityId, -- Priority column   
 0 As ActivityId ,
 ''<a href="../account/frmAccounts.aspx?DivID=''+CAST(Div.numDivisionID AS VARCHAR)+''">''+Comp.vcCompanyName+ CASE WHEN  ISNULL(listCompanyRating.VcData,'''')<>'''' THEN ''(''+listCompanyRating.VcData+'')'' ELSE '''' END +''</a>''  AS OrgName,
 listCompanyRating.VcData As CompanyRating,DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',startdatetimeutc) AS Startdate,DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +', dateadd(second,ISNULL(duration,0),startdatetimeutc)) AS EndTime,0 AS type
 From activity ac join             
activityresource ar on  ac.activityid=ar.activityid             
join resource res on ar.resourceid=res.resourceid                         
LEFT JOIN AdditionalContactsInformation ACI on ACI.numContactId =  (SELECT TOP 1 ContactId FROM ActivityAttendees where ActivityID=ac.activityid)           
 LEFT JOIN DivisionMaster Div                                                          
 ON ACI.numDivisionId = Div.numDivisionID                                   
 LEFT JOIN CompanyInfo  Comp                             
 ON Div.numCompanyID = Comp.numCompanyId
  LEFT JOIN ListDetails AS listCompanyRating
 ON Comp.numCompanyRating=listCompanyRating.NumlistItemID
 OUTER APPLY(' + CONCAT('SELECT MAX(bintCreatedDate) AS bintCreatedDate FROM OpportunityMaster WHERE numDomainID = ',@numDomainID,' AND numDivisionID=Div.numDivisionID AND tintOppType = 1 AND tintOppStatus = 1') + ' ) AS TempLastOrder                 
 OUTER APPLY(' + CONCAT('SELECT
																	SUM(monDealAmount) AS monDealAmount
																	,AVG(ProfitPer) AS fltProfitPer
																FROM
																(
																	SELECT
																		OM.numDivisionId
																		,monDealAmount
																		,AVG(((ISNULL(OI.monPrice,0) - (CASE D.numCost WHEN 3 THEN ISNULL(V.monCost,0) * dbo.fn_UOMConversion(I.numBaseUnit,I.numItemCode,I.numDomainID,I.numPurchaseUnit) WHEN 2 THEN ISNULL(OI.numCost,0) ELSE ISNULL(I.monAverageCost,0) END)) / ISNULL(NULLIF(OI.monPrice,0),1)) * 100) ProfitPer
																	FROM
																		OpportunityMaster OM
																	INNER JOIN
																		Domain D
																	ON
																		OM.numDomainID = D.numDomainID
																	INNER JOIN
																		OpportunityItems OI
																	ON
																		OM.numOppId = OI.numOppId
																	INNER JOIN
																		Item I
																	ON
																		OI.numItemCode = I.numItemCode
																	LEFT JOIN
																		Vendor V
																	ON
																		V.numVendorID = I.numVendorID
																		AND V.numItemCode = I.numItemCode
																	WHERE
																		OM.numDomainId = ',@numDomainID,'
																		AND OM.numDivisionId = Div.numDivisionID
																		AND ISNULL(I.bitContainer,0) = 0
																		AND tintOppType = 1
																		AND tintOppStatus=1
																		
																	GROUP BY
																		OM.numDivisionId
																		,OM.numOppId
																		,monDealAmount
																) T1
																GROUP BY
																	T1.numDivisionId )') + ' AS TempPerformance

 where  res.numUserCntId = ' + Cast(@numUserCntID as varchar(10)) +'             
and ac.OriginalStartDateTimeUtc IS NULL            
and res.numDomainID = ' + Cast(@numDomainID as varchar(10)) +'  and 
(DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +', startdatetimeutc) >= '''+Cast(@startDate as varchar(30))+''' and DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +', startdatetimeutc) <= '''+Cast(@endDate as varchar(30))+''')
and [status] <> -1                                                     
and ac.activityid not in 
(select distinct(numActivityId) from communication Comm where Comm.numDomainID = ' + Cast(@numDomainID as varchar(10)) +'  
AND 1=(Case ' + Cast(@bitFilterRecord as varchar(10)) +' when 1 then Case when Comm.numAssign =' + Cast(@numUserCntID as varchar(10)) +'  then 1 else 0 end 
		else  Case when (Comm.numAssign =' + Cast(@numUserCntID as varchar(10)) +'  or Comm.numCreatedBy=' + Cast(@numUserCntID as varchar(10)) +' ) then 1 else 0 end end)   
		AND 1 = (CASE ' + CAST( @tintActionItemsViewRights AS VARCHAR) + 
' WHEN 3 THEN 1 
WHEN 2 THEN (CASE WHEN Div.numTerID in (select numTerritoryID from  UserTerritory where numUserCntID= '+ CONVERT(VARCHAR(15),@numUserCntID) + ' ) THEN 1 ELSE 0 END)
WHEN 1 THEN (CASE WHEN (Comm.numCreatedBy= ' + CONVERT(VARCHAR(15),@numUserCntID) + ' or Comm.numAssign= '+ CONVERT(VARCHAR(15),@numUserCntID) + ') THEN 1 ELSE 0 END) 
ELSE 0 END)                                                                   

)) Q WHERE '+@OtherRegularSearch+'  '+@IndivRecordPaging+'
'
EXEC (@dynamicQuery)
-------------------- 1 - CASE----------------------------

SET @dynamicQuery =  ' INSERT INTO #tempRecords(
RecordCount,
RecordId,
numCreatedBy,
numOrgTerId,
numTaskId,
IsTaskClosed,
TaskTypeName,
TaskType,
OrigDescription,
Description,
dtDueDate,
DueDate,
TotalProgress,
numAssignedTo,
numAssignToId,
Priority,
Activity,
PriorityId,
ActivityId,
OrgName,
CompanyRating,Startdate,type
) SELECT  COUNT(*) OVER() RecordCount,* FROM (  SELECT 
C.numCaseId AS RecordId,
C.numCreatedBy AS numCreatedBy,
ISNULL(Div.numTerId,0) AS numOrgTerId,
0 AS numTaskId,
CASE WHEN C.numStatus=1008 THEN 1 ELSE 0 END AS IsTaskClosed,
''<a onclick="openCaseDetails(''+CAST(C.numCaseId AS VARCHAR)+'')" href="javascript:void(0)">Case</a>'' As TaskTypeName,
1 AS TaskType,
'''' AS OrigDescription,
C.textSubject AS Description,
cast(C.intTargetResolveDate as datetime) AS dtDueDate,
cast(C.intTargetResolveDate as datetime) as DueDate,
''0'' As TotalProgress,
dbo.fn_GetContactName(C.numAssignedTo) As numAssignedTo,
C.numAssignedTo As numAssignToId,
listdetailsStatus.VcData As Priority, -- Priority column   
'''' As Activity ,
listdetailsStatus.NumlistItemID As PriorityId, -- Priority column   
 0 As ActivityId ,
 ''<a href="../account/frmAccounts.aspx?DivID=''+CAST(Div.numDivisionID AS VARCHAR)+''">''+Com.vcCompanyName+ CASE WHEN  ISNULL(listCompanyRating.VcData,'''')<>'''' THEN ''(''+listCompanyRating.VcData+'')'' ELSE '''' END +''</a>''  AS OrgName,
 listCompanyRating.VcData As CompanyRating,cast(C.intTargetResolveDate as datetime) AS Startdate,0 AS type

 FROM 
	Cases  C                          
INNER JOIN  
	AdditionalContactsInformation  AddC                          
ON 
	C.numContactId = AddC.numContactId                           
INNER JOIN 
	DivisionMaster Div                           
ON 
	AddC.numDivisionId = Div.numDivisionID AND  
	AddC.numDivisionId = Div.numDivisionID                           
INNER JOIN  
	CompanyInfo  Com                          
ON 
	Div.numCompanyID = Com.numCompanyId    
 LEFT JOIN ListDetails AS listCompanyRating
 ON Com.numCompanyRating=listCompanyRating.NumlistItemID
  Left Join listdetails listdetailsStatus                                                 
 On C.numPriority = listdetailsStatus.NumlistItemID
WHERE
	C.numDomainID = ' + Cast(@numDomainID as varchar(10))  + ' AND C.numStatus <> 136  AND 1 = (CASE ' + CAST( @tintActionItemsViewRights AS VARCHAR) + 
' WHEN 3 THEN 1 
WHEN 2 THEN (CASE WHEN Div.numTerID in (select numTerritoryID from  UserTerritory where numUserCntID= '+ CONVERT(VARCHAR(15),@numUserCntID) + ' ) THEN 1 ELSE 0 END)
WHEN 1 THEN (CASE WHEN (C.numCreatedBy= ' + CONVERT(VARCHAR(15),@numUserCntID) + ' or C.numAssignedTo= '+ CONVERT(VARCHAR(15),@numUserCntID) + ') THEN 1 ELSE 0 END) 
ELSE 0 END)  AND (DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',intTargetResolveDate) >= '''+Cast(@startDate as varchar(30))+''' and DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',intTargetResolveDate) <= '''+Cast(@endDate as varchar(30))+''') 
) Q WHERE '+@OtherRegularSearch+' '+@IndivRecordPaging+'
'

PRINT CAST(@dynamicQuery AS NTEXT)
EXEC (@dynamicQuery)

-------------------- 2 - Project Task ----------------------------

DECLARE @dynamicQuery1 VARCHAR(MAX) = ''

SET @dynamicQuery =  ' INSERT INTO #tempRecords(
RecordCount,
RecordId,
numCreatedBy,
numOrgTerId,
numTaskId,
IsTaskClosed,
TaskTypeName,
TaskType,
OrigDescription,
Description,
dtDueDate,
DueDate,
TotalProgress,
numAssignedTo,
numAssignToId,
Priority,
Activity,
PriorityId,
ActivityId,
OrgName,
CompanyRating,
numTaskEstimationInMinutes
,numTimeSpentInMinutes
,dtLastStartDate,Startdate,type
)  SELECT  COUNT(*) OVER() RecordCount,* FROM (SELECT
T.numProjectId AS RecordId,
T.numCreatedBy AS numCreatedBy,
ISNULL(Div.numTerId,0) AS numOrgTerId,
T.numTaskId AS numTaskId,
T.bitTaskClosed AS IsTaskClosed,
CONCAT(''<div><div class="pull-left">'',''<a onclick="openProjectTask(''+CAST(T.numProjectId AS VARCHAR)+'')" href="javascript:void(0)">Project Task</a><br/><span>'',ISNULL(OP.vcProjectID,''''),''</span></div><div id="divTaskControlsProject" class="pull-right">'',(CASE ISNULL((SELECT TOP 1 tintAction FROM StagePercentageDetailsTaskTimeLog WHERE numTaskID=T.numTaskId ORDER BY dtActionTime DESC,ID DESC),0) 
	WHEN 4 THEN (CASE WHEN EXISTS (SELECT C.numContractId FROM Contracts C WHERE C.numDivisonId=Div.numDivisionID) THEN CONCAT(''<ul class="list-inline">
	<li><button class="btn btn-xs btn-primary" onclick="return TaskClosed('',T.numTaskId,'');">Close</button></li>
    <li style="padding-left: 0px; padding-right: 0px">'',dbo.GetTimeSpendOnTaskByProject(T.numDomainID,T.numTaskId,1),''</li>
    <li style="padding-left: 0px; padding-right: 0px"><img src="../images/timeIconnnnn.png" runat="server" id="imgTimeIconn" style="height: 25px" /></li>
    <li style="padding-left: 0px; padding-right: 0px"><input type="checkbox" onclick="addRemoveTimeProject('',T.numDomainID,'','',Div.numDivisionID,'','',dbo.GetTimeSpendOnTaskByProject(T.numDomainID,T.numTaskId,2),'','',T.numTaskId,'','''''',DATEADD(MINUTE,'+Cast(-@ClientTimeZoneOffset as varchar(10)) +',(SELECT dtActionTime FROM StagePercentageDetailsTaskTimeLog SPDTTL WHERE SPDTTL.numTaskID=T.numTaskId AND tintAction=1)),'''''','''''',DATEADD(MINUTE,'+Cast(-@ClientTimeZoneOffset as varchar(10)) +',(SELECT dtActionTime FROM StagePercentageDetailsTaskTimeLog SPDTTL WHERE SPDTTL.numTaskID=T.numTaskId AND tintAction=4)),'''''','',T.numAssignTo,'','''''',T.vcTaskName,'''''','''''',(CASE WHEN dbo.GetTimeSpendOnTaskByProject(T.numDomainID,T.numTaskId,0) = ''Invalid time sequence'' THEN ''00:00'' ELSE dbo.GetTimeSpendOnTaskByProject(T.numDomainID,T.numTaskId,0) END),'''''','''''',ISNULL(ACustomer.vcEmail,''''),'''''','''''',dbo.fn_GetContactName(ACustomer.numContactId),'''''')"'',(CASE WHEN ISNULL(T.bitTimeAddedToContract,0)=1 THEN '' checked=''''checked'''''' ELSE '''' END),'' class="chk_'',T.numTaskId,''" /></li>
</ul>'') ELSE ''<img src="../images/comflag.png" />'' END)
	WHEN 3 THEN CONCAT(''<ul class="list-inline" style="margin-bottom:0px;"><li style="vertical-align:middle;padding-left:0px;padding-right:0px;"><button class="btn btn-xs btn-info" onclick="return ShowTaskTimeLogWindowProject('',T.numTaskId,'');"><i class="fa fa-clock-o"></i></button></li><li style="vertical-align:middle;padding-right:0px;"><button class="btn btn-xs btn-flat btn-warning" onclick="return ShowTaskPausedWindowProject(this,'',T.numTaskId,'');">Pause</button></li><li style="vertical-align:middle;padding-right:0px;"><button class="btn btn-flat btn-task-finish" onclick="return TaskFinishedProject(this,'',T.numTaskId,'',4);"><img src="../images/comflag.png" />&nbsp;Finish</button></li><li style="vertical-align:middle;padding-right:0px;"><label class="taskTimerInitial"></label></li></ul>'')
	WHEN 2 THEN CONCAT(''<ul class="list-inline" style="margin-bottom:0px;"><li style="vertical-align:middle;padding-left:0px;padding-right:0px;"><button class="btn btn-xs btn-info" onclick="return ShowTaskTimeLogWindowProject('',T.numTaskId,'');"><i class="fa fa-clock-o"></i></button></li><li style="vertical-align:middle;padding-right:0px;"><button class="btn btn-xs btn-flat bg-purple" onclick="return TaskStartedProject(this,'',T.numTaskId,'',1);">Resume</button></li><li style="vertical-align:middle;padding-right:0px;"><button class="btn btn-flat btn-task-finish" onclick="return TaskFinishedProject(this,'',T.numTaskId,'',4);"><img src="../images/comflag.png" />&nbsp;Finish</button></li></ul>'')
	WHEN 1 THEN CONCAT(''<ul class="list-inline" style="margin-bottom:0px;"><li style="vertical-align:middle;padding-left:0px;padding-right:0px;"><button class="btn btn-xs btn-info" onclick="return ShowTaskTimeLogWindowProject('',T.numTaskId,'');"><i class="fa fa-clock-o"></i></button></li><li style="vertical-align:middle;padding-right:0px;"><button class="btn btn-xs btn-flat btn-warning" onclick="return ShowTaskPausedWindowProject(this,'',T.numTaskId,'');">Pause</button></li><li style="vertical-align:middle;padding-right:0px;"><button class="btn btn-flat btn-task-finish" onclick="return TaskFinishedProject(this,'',T.numTaskId,'',4);"><img src="../images/comflag.png" />&nbsp;Finish</button></li><li style="vertical-align:middle;padding-right:0px;"><label class="taskTimerInitial"></label></li></ul>'')
	ELSE CONCAT(''<ul class="list-inline" style="margin-bottom:0px;"><li style="vertical-align:middle;padding-left:0px;padding-right:0px;"><button class="btn btn-xs btn-info" onclick="return ShowTaskTimeLogWindowProject('',T.numTaskId,'');"><i class="fa fa-clock-o"></i></button></li><li style="vertical-align:middle;padding-right:0px;"><button class="btn btn-xs btn-flat btn-success btn-task-start" onclick="return TaskStartedProject(this,'',T.numTaskId,'',0);">Start</button></li><li style="vertical-align:middle;padding-right:0px;"><button class="btn btn-flat btn-task-finish" onclick="return TaskFinishedProject(this,'',T.numTaskId,'',4);"><img src="../images/comflag.png" />&nbsp;Finish</button></li></ul>'')
END),''</div></div>'') As TaskTypeName,
2 AS TaskType,
'''' AS OrigDescription,
T.vcTaskName AS Description,
DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',OP.dtmEndDate) AS dtDueDate,
DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',OP.dtmEndDate) as DueDate,
ISNULL((SELECT TOP 1 intTotalProgress FROM ProjectProgress WHERE numProId = T.numProjectId),0) As TotalProgress,
dbo.fn_GetContactName(T.numAssignTo) As numAssignedTo,
T.numAssignTo As numAssignToId,
'''' As Priority, -- Priority column   
'''' As Activity ,
0 As PriorityId, -- Priority column   
 0 As ActivityId ,
 ''<a href="../account/frmAccounts.aspx?DivID=''+CAST(Div.numDivisionID AS VARCHAR)+''">''+Com.vcCompanyName+ CASE WHEN  ISNULL(listCompanyRating.VcData,'''')<>'''' THEN ''(''+listCompanyRating.VcData+'')'' ELSE '''' END +''</a>''  AS OrgName,
 listCompanyRating.VcData As CompanyRating
,((ISNULL(T.numHours,0) * 60) + ISNULL(T.numMinutes,0)) numTaskEstimationInMinutes
,(CASE ISNULL((SELECT TOP 1 tintAction FROM StagePercentageDetailsTaskTimeLog SPDTTL WHERE SPDTTL.numTaskID=T.numTaskId ORDER BY SPDTTL.dtActionTime DESC,SPDTTL.ID DESC),0) 
	WHEN 4 THEN 0
	WHEN 3 THEN dbo.GetTimeSpendOnTaskByProject(T.numDomainID,T.numTaskId,2)
	WHEN 2 THEN dbo.GetTimeSpendOnTaskByProject(T.numDomainID,T.numTaskId,2)
	WHEN 1 THEN 0
END) numTimeSpentInMinutes
,(CASE ISNULL((SELECT TOP 1 tintAction FROM StagePercentageDetailsTaskTimeLog WHERE numTaskID=T.numTaskId ORDER BY dtActionTime DESC,ID DESC),0) 
	WHEN 4 THEN NULL
	WHEN 3 THEN DATEADD(MINUTE,'+Cast(-@ClientTimeZoneOffset as varchar(10)) +',(SELECT TOP 1 dtActionTime FROM StagePercentageDetailsTaskTimeLog SPDTTL WHERE SPDTTL.numTaskID=T.numTaskId ORDER BY SPDTTL.dtActionTime DESC,SPDTTL.ID DESC))
	WHEN 2 THEN NULL
	WHEN 1 THEN DATEADD(MINUTE,'+Cast(-@ClientTimeZoneOffset as varchar(10)) +',(SELECT TOP 1 dtActionTime FROM StagePercentageDetailsTaskTimeLog SPDTTL WHERE SPDTTL.numTaskID=T.numTaskId ORDER BY SPDTTL.dtActionTime DESC,SPDTTL.ID DESC))
END) dtLastStartDate,CASE WHEN S.bitIsDueDaysUsed=1 THEN CONVERT(VARCHAR(10),DATEADD(DAY,S.intDueDays,CAST(S.dtStartDate AS DATE)), 101)
		ELSE CONVERT(VARCHAR(10),DATEADD(DAY,(SELECT TOP 1 CAST(Items AS DECIMAL(18,2)) FROM Split(dbo.fn_calculateTotalHoursMinutesByStageDetails(T.numStageDetailsId,T.numOppId,T.numProjectId),''_'')),CAST(S.dtStartDate AS DATE)), 101) END AS Startdate,0 AS type'
SET @dynamicQuery1 = ' FROM 
	StagePercentageDetailsTask AS T
	LEFT JOIN StagePercentageDetails AS S
	ON S.numStageDetailsId=T.numStageDetailsId AND S.numProjectID=T.numProjectID
	INNER JOIN ProjectsMaster AS OP 
	ON T.numProjectID=OP.numProId
	LEFT JOIN StagePercentageMaster AS SM
	ON S.numStagePercentageId=SM.numStagePercentageId                        
INNER JOIN 
	DivisionMaster Div                           
ON 
	OP.numDivisionId = Div.numDivisionID                       
INNER JOIN  
	CompanyInfo  Com                          
ON 
	Div.numCompanyID = Com.numCompanyId    
LEFT JOIN ListDetails AS listCompanyRating ON Com.numCompanyRating=listCompanyRating.NumlistItemID
LEFT JOIN AdditionalContactsInformation ACustomer ON OP.numCustPrjMgr = ACustomer.numContactId
WHERE
	T.numDomainID = ' + Cast(@numDomainID as varchar(10))  + ' 
	AND (NOT EXISTS (SELECT * FROM StagePercentageDetailsTaskTimeLog SPDTTL WHERE SPDTTL.numTaskID=T.numTaskID AND SPDTTL.tintAction=4) OR (ISNULL(T.bitTaskClosed,0) = 0 AND EXISTS (SELECT C.numContractId FROM Contracts C WHERE C.numDivisonId=Div.numDivisionID)))
	AND ((CAST(DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',OP.dtmStartDate) AS DATE) >= '''+Cast(CAST(@startDate AS DATE) as varchar(30))+''' and CAST(DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',OP.dtmStartDate) AS DATE) <= '''+Cast(CAST(@endDate AS DATE) as varchar(30))+''') OR (DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',OP.dtmEndDate) >= '''+Cast(@startDate as varchar(30))+''' and DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',OP.dtmEndDate) <= '''+Cast(@endDate as varchar(30))+''')) 
	AND 1 = (CASE ' + CAST( @tintActionItemsViewRights AS VARCHAR) + 
' WHEN 3 THEN 1 
WHEN 2 THEN (CASE WHEN Div.numTerID in (select numTerritoryID from  UserTerritory where numUserCntID= '+ CONVERT(VARCHAR(15),@numUserCntID) + ' ) THEN 1 ELSE 0 END)
WHEN 1 THEN (CASE WHEN (T.numCreatedBy= ' + CONVERT(VARCHAR(15),@numUserCntID) + ' or T.numAssignTo= '+ CONVERT(VARCHAR(15),@numUserCntID) + ') THEN 1 ELSE 0 END) 
ELSE 0 END) ) Q WHERE '+@OtherRegularSearch + @IndivRecordPaging

--PRINT CAST(@dynamicQuery AS NTEXT)

EXEC (@dynamicQuery + @dynamicQuery1)

------------------ 3 - Opportunity  Task ----------------------------

SET @dynamicQuery =  ' INSERT INTO #tempRecords(
RecordCount,
RecordId,
numCreatedBy,
numOrgTerId,
numTaskId,
IsTaskClosed,
TaskTypeName,
TaskType,
OrigDescription,
Description,
dtDueDate,
DueDate,
TotalProgress,
numAssignedTo,
numAssignToId,
Priority,
Activity,
PriorityId,
ActivityId,
OrgName,
CompanyRating,Startdate,type
) SELECT  COUNT(*) OVER() RecordCount,* FROM ( SELECT 
T.numOppId AS RecordId,
T.numCreatedBy AS numCreatedBy,
ISNULL(Div.numTerId,0) AS numOrgTerId,
T.numTaskId AS numTaskId,
T.bitTaskClosed AS IsTaskClosed,
CONCAT(''<div><div class="pull-left"><a onclick="openTask('',CAST(T.numOppId AS VARCHAR),'','',CAST(S.numStagePercentageId AS VARCHAR),'','',CAST(S.tinProgressPercentage AS VARCHAR),'','',CAST(S.tintConfiguration AS VARCHAR)+'')" href="javascript:void(0)">Opportunity Task</a><br/>'',ISNULL(Com.vcCompanyName,''''),(CASE WHEN ADC.numContactID IS NOT NULL THEN CONCAT(''&nbsp;<a class="text-green" href="javascript:OpenContact('',ADC.numContactID,'',1)">('',ISNULL(ADC.vcFirstName,''-''),'' '',ISNULL(ADC.vcLastName,''-''),'')</a>'') ELSE '''' END),''<br/>'',(CASE WHEN ISNULL(ADC.numPhone,'''') <> '''' THEN ADC.numPhone ELSE '''' END),(CASE WHEN ISNULL(ADC.numPhoneExtension,'''') <> '''' THEN CONCAT(''&nbsp;('',ADC.numPhoneExtension,'')'') ELSE '''' END),(CASE WHEN ISNULL(ADC.vcEmail,'''') <> '''' THEN CONCAT(''&nbsp;<img src="../images/msg_unread_small.gif" style="cursor:pointer" onclick="return OpemEmail('''''',ADC.vcEmail,'''''','',ADC.numcontactID,'')" />'') ELSE '''' END),''</div><div class="pull-right"><button class="btn btn-flat btn-task-finish" onclick="return TaskClosed('',T.numTaskId,'');"><img src="../images/comflag.png" />&nbsp;Finish</button></div></div>'') As TaskTypeName,
3 AS TaskType,
'''' AS OrigDescription,
T.vcTaskName AS Description,
CASE WHEN S.bitIsDueDaysUsed=1 THEN CONVERT(VARCHAR(10),DATEADD(DAY,S.intDueDays,CAST(S.dtStartDate AS DATE)), 101)
		ELSE CONVERT(VARCHAR(10),DATEADD(DAY,(SELECT TOP 1 CAST(Items AS DECIMAL(18,2)) FROM Split(dbo.fn_calculateTotalHoursMinutesByStageDetails(T.numStageDetailsId,T.numOppId,T.numProjectId),''_'')),CAST(S.dtStartDate AS DATE)), 101) END AS dtDueDate,
CASE WHEN S.bitIsDueDaysUsed=1 THEN CONVERT(VARCHAR(10),DATEADD(DAY,S.intDueDays,CAST(S.dtStartDate AS DATE)), 101)
		ELSE CONVERT(VARCHAR(10),DATEADD(DAY,(SELECT TOP 1 CAST(Items AS DECIMAL(18,2)) FROM Split(dbo.fn_calculateTotalHoursMinutesByStageDetails(T.numStageDetailsId,T.numOppId,T.numProjectId),''_'')),CAST(S.dtStartDate AS DATE)), 101) END as DueDate,
ISNULL((SELECT TOP 1 intTotalProgress FROM ProjectProgress WHERE numOppId = T.numOppId),0) As TotalProgress,
dbo.fn_GetContactName(T.numAssignTo) As numAssignedTo,
T.numAssignTo As numAssignToId,
'''' As Priority, -- Priority column   
'''' As Activity ,
0 As PriorityId, -- Priority column   
 0 As ActivityId ,
 ''<a href="../account/frmAccounts.aspx?DivID=''+CAST(Div.numDivisionID AS VARCHAR)+''">''+Com.vcCompanyName+ CASE WHEN  ISNULL(listCompanyRating.VcData,'''')<>'''' THEN ''(''+listCompanyRating.VcData+'')'' ELSE '''' END +''</a>''  AS OrgName,
 listCompanyRating.VcData As CompanyRating,CASE WHEN S.bitIsDueDaysUsed=1 THEN CONVERT(VARCHAR(10),DATEADD(DAY,S.intDueDays,CAST(S.dtStartDate AS DATE)), 101)
		ELSE CONVERT(VARCHAR(10),DATEADD(DAY,(SELECT TOP 1 CAST(Items AS DECIMAL(18,2)) FROM Split(dbo.fn_calculateTotalHoursMinutesByStageDetails(T.numStageDetailsId,T.numOppId,T.numProjectId),''_'')),CAST(S.dtStartDate AS DATE)), 101) END  AS Startdate,0 AS type

 FROM 
	StagePercentageDetailsTask AS T
	LEFT JOIN StagePercentageDetails AS S
	ON S.numStageDetailsId=T.numStageDetailsId AND S.numOppId=T.numOppId
	LEFT JOIN OpportunityMaster AS OP 
	ON T.numOppId=OP.numOppId
	LEFT JOIN StagePercentageMaster AS SM
	ON S.numStagePercentageId=SM.numStagePercentageId                        
INNER JOIN 
	DivisionMaster Div                           
ON 
	OP.numDivisionId = Div.numDivisionID                       
INNER JOIN  
	CompanyInfo  Com                          
ON 
	Div.numCompanyID = Com.numCompanyId    
LEFT JOIN AdditionalContactsInformation ADC ON OP.numContactId = ADC.numContactId
LEFT JOIN ListDetails AS listCompanyRating
 ON Com.numCompanyRating=listCompanyRating.NumlistItemID
WHERE
	T.numDomainID = ' + Cast(@numDomainID as varchar(10))  + ' AND OP.tintOppType=1 AND OP.tintOppStatus=0 AND T.numOppId>0 AND s.numOppId>0 AND T.bitSavedTask=1 AND T.bitTaskClosed=0 AND 1 = (CASE ' + CAST( @tintActionItemsViewRights AS VARCHAR) + 
' WHEN 3 THEN 1 
WHEN 2 THEN (CASE WHEN Div.numTerID in (select numTerritoryID from  UserTerritory where numUserCntID= '+ CONVERT(VARCHAR(15),@numUserCntID) + ' ) THEN 1 ELSE 0 END)
WHEN 1 THEN (CASE WHEN (T.numCreatedBy= ' + CONVERT(VARCHAR(15),@numUserCntID) + ' or T.numAssignTo= '+ CONVERT(VARCHAR(15),@numUserCntID) + ') THEN 1 ELSE 0 END) 
ELSE 0 END) ) Q WHERE '+@OtherRegularSearch+' AND (DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',Q.DueDate) >= '''+Cast(@startDate as varchar(30))+''' and DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',Q.DueDate) <= '''+Cast(@endDate as varchar(30))+''')
'+@IndivRecordPaging+' 
 '
 
 EXEC (@dynamicQuery)

------------------ 4 -  Sales Order Task ----------------------------

SET @dynamicQuery =  ' INSERT INTO #tempRecords(
RecordCount,
RecordId,
numCreatedBy,
numOrgTerId,
numTaskId,
IsTaskClosed,
TaskTypeName,
TaskType,
OrigDescription,
Description,
dtDueDate,
DueDate,
TotalProgress,
numAssignedTo,
numAssignToId,
Priority,
Activity,
PriorityId,
ActivityId,
OrgName,
CompanyRating,Startdate,type
)SELECT  COUNT(*) OVER() RecordCount,* FROM (  SELECT 
T.numOppId AS RecordId,
T.numCreatedBy AS numCreatedBy,
ISNULL(Div.numTerId,0) AS numOrgTerId,
T.numTaskId AS numTaskId,
T.bitTaskClosed AS IsTaskClosed,
CONCAT(''<div><div class="pull-left"><a onclick="openTask('',CAST(T.numOppId AS VARCHAR),'','',CAST(S.numStagePercentageId AS VARCHAR),'','',CAST(S.tinProgressPercentage AS VARCHAR),'','',CAST(S.tintConfiguration AS VARCHAR)+'')" href="javascript:void(0)">Sales Order Task</a><br/>'',ISNULL(Com.vcCompanyName,''''),(CASE WHEN ADC.numContactID IS NOT NULL THEN CONCAT(''&nbsp;<a class="text-green" href="javascript:OpenContact('',ADC.numContactID,'',1)">('',ISNULL(ADC.vcFirstName,''-''),'' '',ISNULL(ADC.vcLastName,''-''),'')</a>'') ELSE '''' END),''<br/>'',(CASE WHEN ISNULL(ADC.numPhone,'''') <> '''' THEN ADC.numPhone ELSE '''' END),(CASE WHEN ISNULL(ADC.numPhoneExtension,'''') <> '''' THEN CONCAT(''&nbsp;('',ADC.numPhoneExtension,'')'') ELSE '''' END),(CASE WHEN ISNULL(ADC.vcEmail,'''') <> '''' THEN CONCAT(''&nbsp;<img src="../images/msg_unread_small.gif" style="cursor:pointer" onclick="return OpemEmail('''''',ADC.vcEmail,'''''','',ADC.numcontactID,'')" />'') ELSE '''' END),''</div><div class="pull-right"><button class="btn btn-flat btn-task-finish" onclick="return TaskClosed('',T.numTaskId,'');"><img src="../images/comflag.png" />&nbsp;Finish</button></div></div>'') As TaskTypeName,
4 AS TaskType,
'''' AS OrigDescription,
T.vcTaskName AS Description,
CASE WHEN S.bitIsDueDaysUsed=1 THEN CONVERT(VARCHAR(10),DATEADD(DAY,S.intDueDays,CAST(S.dtStartDate AS DATE)), 101)
		ELSE CONVERT(VARCHAR(10),DATEADD(DAY,(SELECT TOP 1 CAST(Items AS DECIMAL(18,2)) FROM Split(dbo.fn_calculateTotalHoursMinutesByStageDetails(T.numStageDetailsId,T.numOppId,T.numProjectId),''_'')),CAST(S.dtStartDate AS DATE)), 101) END AS dtDueDate,
CASE WHEN S.bitIsDueDaysUsed=1 THEN CONVERT(VARCHAR(10),DATEADD(DAY,S.intDueDays,CAST(S.dtStartDate AS DATE)), 101)
		ELSE CONVERT(VARCHAR(10),DATEADD(DAY,(SELECT TOP 1 CAST(Items AS DECIMAL(18,2)) FROM Split(dbo.fn_calculateTotalHoursMinutesByStageDetails(T.numStageDetailsId,T.numOppId,T.numProjectId),''_'')),CAST(S.dtStartDate AS DATE)), 101) END as DueDate,
ISNULL((SELECT TOP 1 intTotalProgress FROM ProjectProgress WHERE numOppId = T.numOppId),0) As TotalProgress,
dbo.fn_GetContactName(T.numAssignTo) As numAssignedTo,
T.numAssignTo As numAssignToId,
'''' As Priority, -- Priority column   
'''' As Activity ,
0 As PriorityId, -- Priority column   
 0 As ActivityId ,
 ''<a href="../account/frmAccounts.aspx?DivID=''+CAST(Div.numDivisionID AS VARCHAR)+''">''+Com.vcCompanyName+ CASE WHEN  ISNULL(listCompanyRating.VcData,'''')<>'''' THEN ''(''+listCompanyRating.VcData+'')'' ELSE '''' END +''</a>''  AS OrgName,
 listCompanyRating.VcData As CompanyRating,CASE WHEN S.bitIsDueDaysUsed=1 THEN CONVERT(VARCHAR(10),DATEADD(DAY,S.intDueDays,CAST(S.dtStartDate AS DATE)), 101)
		ELSE CONVERT(VARCHAR(10),DATEADD(DAY,(SELECT TOP 1 CAST(Items AS DECIMAL(18,2)) FROM Split(dbo.fn_calculateTotalHoursMinutesByStageDetails(T.numStageDetailsId,T.numOppId,T.numProjectId),''_'')),CAST(S.dtStartDate AS DATE)), 101) END AS Startdate,0 AS type

 FROM 
	StagePercentageDetailsTask AS T
	LEFT JOIN StagePercentageDetails AS S
	ON S.numStageDetailsId=T.numStageDetailsId AND S.numOppId=T.numOppId
	LEFT JOIN OpportunityMaster AS OP 
	ON T.numOppId=OP.numOppId
	LEFT JOIN StagePercentageMaster AS SM
	ON S.numStagePercentageId=SM.numStagePercentageId                        
INNER JOIN 
	DivisionMaster Div                           
ON 
	OP.numDivisionId = Div.numDivisionID                       
INNER JOIN  
	CompanyInfo  Com                          
ON 
	Div.numCompanyID = Com.numCompanyId    
LEFT JOIN AdditionalContactsInformation ADC ON OP.numContactId = ADC.numContactId 
LEFT JOIN ListDetails AS listCompanyRating
 ON Com.numCompanyRating=listCompanyRating.NumlistItemID
WHERE
	T.numDomainID = ' + Cast(@numDomainID as varchar(10))  + ' AND OP.tintOppType=1 AND OP.tintOppStatus=1 AND T.numOppId>0 AND s.numOppId>0 AND T.bitSavedTask=1 AND T.bitTaskClosed=0 AND 1 = (CASE ' + CAST( @tintActionItemsViewRights AS VARCHAR) + 
' WHEN 3 THEN 1 
WHEN 2 THEN (CASE WHEN Div.numTerID in (select numTerritoryID from  UserTerritory where numUserCntID= '+ CONVERT(VARCHAR(15),@numUserCntID) + ' ) THEN 1 ELSE 0 END)
WHEN 1 THEN (CASE WHEN (T.numCreatedBy= ' + CONVERT(VARCHAR(15),@numUserCntID) + ' or T.numAssignTo= '+ CONVERT(VARCHAR(15),@numUserCntID) + ') THEN 1 ELSE 0 END) 
ELSE 0 END) ) Q WHERE '+@OtherRegularSearch+' AND (DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',Q.DueDate) >= '''+Cast(@startDate as varchar(30))+''' and DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',Q.DueDate) <= '''+Cast(@endDate as varchar(30))+''')
'+@IndivRecordPaging+'
 '
 EXEC (@dynamicQuery)
  
------------------ 5 -  Work Order Task ----------------------------

SET @dynamicQuery =  ' INSERT INTO #tempRecords(
RecordCount,
RecordId,
numCreatedBy,
numOrgTerId,
numTaskId,
IsTaskClosed,
TaskTypeName,
TaskType,
OrigDescription,
Description,
dtDueDate,
DueDate,
TotalProgress,
numAssignedTo,
numAssignToId,
Priority,
Activity,
PriorityId,
ActivityId,
OrgName,
CompanyRating,Startdate,type
)SELECT  COUNT(*) OVER() RecordCount,* FROM ( SELECT 
WorkOrder.numWOID AS RecordId,
WorkOrder.numCreatedBy AS numCreatedBy,
ISNULL(Div.numTerId,0) AS numOrgTerId,
WorkOrder.numWOID AS numTaskId,
(CASE WHEN WorkOrder.numWOStatus = 23184 THEN 1 ELSE 0 END) AS IsTaskClosed,
CONCAT(''<a href="../items/frmWorkOrder.aspx?WOID='',WorkOrder.numWOID,''" target="_blank">Work Order Management</a>'') As TaskTypeName,
5 AS TaskType,
'''' AS OrigDescription,
CONCAT(''<b>'',Item.vcItemName,''</b>'','' ('',WorkOrder.numQtyItemsReq,'')'','' <b>Work Order Status :'', ''</b>'',''<label class="lblWorkOrderStatus" id="'',WorkOrder.numWOID,''"><i class="fa fa-refresh fa-spin"></i></lable>'') AS Description,
DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',WorkOrder.dtmEndDate) AS dtDueDate,
DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',WorkOrder.dtmEndDate) as DueDate,
dbo.GetTotalProgress(WorkOrder.numDomainID,WorkOrder.numWOID,1,1,'''',0) As TotalProgress,
dbo.fn_GetContactName(WorkOrder.numAssignedTo) As numAssignedTo,
WorkOrder.numAssignedTo As numAssignToId,
'''' As Priority, -- Priority column   
'''' As Activity ,
0 As PriorityId, -- Priority column   
 0 As ActivityId ,
 ''<a href="../account/frmAccounts.aspx?DivID=''+CAST(Div.numDivisionID AS VARCHAR)+''">''+Com.vcCompanyName+ CASE WHEN  ISNULL(listCompanyRating.VcData,'''')<>'''' THEN ''(''+listCompanyRating.VcData+'')'' ELSE '''' END +''</a>''  AS OrgName,
 listCompanyRating.VcData As CompanyRating,DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',WorkOrder.dtmEndDate) AS Startdate,0 AS type

 FROM 
	WorkOrder
INNER JOIN 
	Item
ON
	WorkOrder.numItemCode = Item.numItemCode
LEFT JOIN 
	OpportunityMaster
ON 
	WorkOrder.numOppId=OpportunityMaster.numOppId
INNER JOIN 
	DivisionMaster Div                           
ON 
	Div.numDivisionId = (CASE WHEN OpportunityMaster.numOppID IS NOT NULL THEN OpportunityMaster.numDivisionID ELSE ' + CAST(@numDomainDivisionID AS VARCHAR) + ' END)                        
INNER JOIN  
	CompanyInfo  Com                          
ON 
	Div.numCompanyID = Com.numCompanyId    
LEFT JOIN 
	ListDetails AS listCompanyRating
ON 
	Com.numCompanyRating=listCompanyRating.NumlistItemID
WHERE
	WorkOrder.numDomainID = ' + Cast(@numDomainID as varchar(10))  + ' 
	AND WorkOrder.numWOStatus <> 23184
	AND WorkOrder.numAssignedTo= '+ CONVERT(VARCHAR(15),@numUserCntID) + '
	AND ((DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',WorkOrder.dtmStartDate) >= '''+Cast(@startDate as varchar(30))+''' and DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',WorkOrder.dtmStartDate) <= '''+Cast(@endDate as varchar(30))+''') OR (DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',WorkOrder.dtmEndDate) >= '''+Cast(@startDate as varchar(30))+''' and DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',WorkOrder.dtmEndDate) <= '''+Cast(@endDate as varchar(30))+'''))
) Q WHERE '+@OtherRegularSearch+' '+@IndivRecordPaging+'
 '
 EXEC (@dynamicQuery)
------------------ 6 -  Work Center Task ----------------------------

SET @dynamicQuery =  ' INSERT INTO #tempRecords(
RecordCount,
RecordId,
numCreatedBy,
numOrgTerId,
numTaskId,
IsTaskClosed,
TaskTypeName,
TaskType,
OrigDescription,
Description,
dtDueDate,
DueDate,
TotalProgress,
numAssignedTo,
numAssignToId,
Priority,
Activity,
PriorityId,
ActivityId,
OrgName,
CompanyRating,
numTaskEstimationInMinutes
,numTimeSpentInMinutes
,dtLastStartDate
,numRemainingQty
,Startdate,type
) SELECT  COUNT(*) OVER() RecordCount,* FROM ( SELECT 
T.numTaskId AS RecordId,
T.numCreatedBy AS numCreatedBy,
ISNULL(Div.numTerId,0) AS numOrgTerId,
T.numTaskId AS numTaskId,
(CASE WHEN EXISTS (SELECT * FROM StagePercentageDetailsTaskTimeLog SPDTTL WHERE SPDTTL.numTaskID=T.numTaskID AND SPDTTL.tintAction=4) THEN 1 ELSE 0 END) AS IsTaskClosed,
CONCAT(''<div><div class="pull-left"><a href="../items/frmWorkOrder.aspx?WOID='',W.numWOID,''" target="_blank">Work Center Task</a><br/><span>'',CONCAT(I.vcItemName,'' ('',W.numQtyItemsReq,'')''),''</span></div><div class="pull-right" id="divTaskControlsWorkOrder">'',(CASE ISNULL((SELECT TOP 1 tintAction FROM StagePercentageDetailsTaskTimeLog WHERE numTaskID=T.numTaskId ORDER BY dtActionTime DESC, ID DESC),0) 
	WHEN 4 THEN ''<img src="../images/comflag.png" />''
	WHEN 3 THEN CONCAT(''<ul class="list-inline" style="margin-bottom:0px;"><li style="vertical-align:middle;padding-left:0px;padding-right:0px;"><button class="btn btn-xs btn-info" onclick="return ShowTaskTimeLogWindowWorkOrder('',T.numTaskId,'');"><i class="fa fa-clock-o"></i></button></li><li style="vertical-align:middle;padding-right:0px;"><button class="btn btn-xs btn-flat btn-success btn-task-start" onclick="return TaskStartedWorkOrder(this,'',T.numTaskId,'',0);">Start</button></li><li style="vertical-align:middle;padding-right:0px;"><label class="taskTimerInitial"></label></li></ul>'')
	WHEN 2 THEN CONCAT(''<ul class="list-inline" style="margin-bottom:0px;"><li style="vertical-align:middle;padding-left:0px;padding-right:0px;"><button class="btn btn-xs btn-info" onclick="return ShowTaskTimeLogWindowWorkOrder('',T.numTaskId,'');"><i class="fa fa-clock-o"></i></button></li><li style="vertical-align:middle;padding-right:0px;"><button class="btn btn-xs btn-flat bg-purple" onclick="return TaskStartedWorkOrder(this,'',T.numTaskId,'',1);">Resume</button></li></ul>'')
	WHEN 1 THEN CONCAT(''<ul class="list-inline" style="margin-bottom:0px;"><li style="vertical-align:middle;padding-left:0px;padding-right:0px;"><button class="btn btn-xs btn-info" onclick="return ShowTaskTimeLogWindowWorkOrder('',T.numTaskId,'');"><i class="fa fa-clock-o"></i></button></li><li style="vertical-align:middle;padding-right:0px;"><button class="btn btn-xs btn-flat btn-warning" onclick="return ShowTaskPausedWindowWorkOrder(this,'',T.numTaskId,'');">Pause</button></li><li style="vertical-align:middle;padding-right:0px;"><button class="btn btn-flat btn-task-finish" onclick="return TaskFinishedWorkOrder(this,'',T.numTaskId,'',4);"><img src="../images/comflag.png" />&nbsp;Finish</button></li><li style="vertical-align:middle;padding-right:0px;"><input type="text" onkeydown="return ProcessedUnitsChangedWorkOrder(this,'',T.numTaskId,'',event);" class="form-control txtUnitsProcessed" style="width:50px;padding:1px 2px 1px 2px;height:23px;" /></li><li style="vertical-align:middle;padding-right:0px;"><label class="taskTimerInitial"></label></li></ul>'')
	ELSE CONCAT(''<ul class="list-inline" style="margin-bottom:0px;"><li style="vertical-align:middle;padding-left:0px;padding-right:0px;"><button class="btn btn-xs btn-info" onclick="return ShowTaskTimeLogWindowWorkOrder('',T.numTaskId,'');"><i class="fa fa-clock-o"></i></button></li><li style="vertical-align:middle;padding-right:0px;"><button class="btn btn-xs btn-flat btn-success btn-task-start" onclick="return TaskStartedWorkOrder(this,'',T.numTaskId,'',0);">Start</button></li></ul>'')
END),''</div></div>'') As TaskTypeName,
6 AS TaskType,
'''' AS OrigDescription,
T.vcTaskName AS Description,
DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',W.dtmEndDate) AS dtDueDate,
DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',W.dtmEndDate) as DueDate,
(CASE 
	WHEN EXISTS (SELECT * FROM StagePercentageDetailsTaskTimeLog SPDTTL WHERE SPDTTL.numTaskID=T.numTaskID AND SPDTTL.tintAction=4) THEN CAST(100 AS INT)
	ELSE CAST(((ISNULL((SELECT SUM(numProcessedQty) FROM StagePercentageDetailsTaskTimeLog SPDTTL WHERE SPDTTL.numTaskID=T.numTaskID),0) / W.numQtyItemsReq) * 100) AS INT)
END) As TotalProgress,
dbo.fn_GetContactName(T.numAssignTo) As numAssignedTo,
T.numAssignTo As numAssignToId,
'''' As Priority, -- Priority column   
'''' As Activity ,
0 As PriorityId, -- Priority column   
 0 As ActivityId ,
 ''<a href="../account/frmAccounts.aspx?DivID=''+CAST(Div.numDivisionID AS VARCHAR)+''">''+Com.vcCompanyName+ CASE WHEN  ISNULL(listCompanyRating.VcData,'''')<>'''' THEN ''(''+listCompanyRating.VcData+'')'' ELSE '''' END +''</a>''  AS OrgName,
 listCompanyRating.VcData As CompanyRating
 ,(((ISNULL(T.numHours,0) * 60) + ISNULL(T.numMinutes,0)) * ISNULL(W.numQtyItemsReq,0)) numTaskEstimationInMinutes
,(CASE ISNULL((SELECT TOP 1 tintAction FROM StagePercentageDetailsTaskTimeLog SPDTTL WHERE SPDTTL.numTaskID=T.numTaskId ORDER BY SPDTTL.dtActionTime DESC,SPDTTL.ID DESC),0) 
	WHEN 4 THEN 0
	WHEN 3 THEN dbo.GetTimeSpendOnTask(T.numDomainID,T.numTaskId,2)
	WHEN 2 THEN dbo.GetTimeSpendOnTask(T.numDomainID,T.numTaskId,2)
	WHEN 1 THEN 0
END) numTimeSpentInMinutes
,(CASE ISNULL((SELECT TOP 1 tintAction FROM StagePercentageDetailsTaskTimeLog WHERE numTaskID=T.numTaskId ORDER BY dtActionTime DESC,ID DESC),0) 
	WHEN 4 THEN NULL
	WHEN 3 THEN DATEADD(MINUTE,'+Cast(-@ClientTimeZoneOffset as varchar(10)) +',(SELECT TOP 1 dtActionTime FROM StagePercentageDetailsTaskTimeLog SPDTTL WHERE SPDTTL.numTaskID=T.numTaskId ORDER BY SPDTTL.dtActionTime DESC,SPDTTL.ID DESC))
	WHEN 2 THEN NULL
	WHEN 1 THEN DATEADD(MINUTE,'+Cast(-@ClientTimeZoneOffset as varchar(10)) +',(SELECT TOP 1 dtActionTime FROM StagePercentageDetailsTaskTimeLog SPDTTL WHERE SPDTTL.numTaskID=T.numTaskId ORDER BY SPDTTL.dtActionTime DESC,SPDTTL.ID DESC))
END) dtLastStartDate
,(CASE
	WHEN EXISTS (SELECT ID FROM StagePercentageDetailsTaskTimeLog SPDTTL WHERE SPDTTL.numTaskID=T.numTaskId AND tintAction=4) THEN 0
	ELSE ISNULL(W.numQtyItemsReq,0) - ISNULL((SELECT SUM(numProcessedQty) FROM StagePercentageDetailsTaskTimeLog SPDTTL WHERE SPDTTL.numTaskID=T.numTaskId),0)
END) AS numRemainingQty
,DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',W.dtmEndDate) AS Startdate,0 AS type
 FROM 
	StagePercentageDetailsTask AS T
	INNER JOIN WorkOrder AS W 
	ON T.numWorkOrderId=W.numWOId     
	INNER JOIN Item I ON W.numItemCode=I.numItemCode
LEFT JOIN OpportunityMaster AS OP 
	ON W.numOppId=OP.numOppId
INNER JOIN 
	DivisionMaster Div                           
ON 
	Div.numDivisionId = (CASE WHEN OP.numOppID IS NOT NULL THEN OP.numDivisionID ELSE ' + CAST(@numDomainDivisionID AS VARCHAR) + ' END)                         
INNER JOIN  
	CompanyInfo  Com                          
ON 
	Div.numCompanyID = Com.numCompanyId    
LEFT JOIN ListDetails AS listCompanyRating
 ON Com.numCompanyRating=listCompanyRating.NumlistItemID
WHERE
	W.numDomainID = ' + Cast(@numDomainID as varchar(10))  + '
	AND W.numWOStatus <> 23184 
	AND NOT EXISTS (SELECT * FROM StagePercentageDetailsTaskTimeLog SPDTTL WHERE SPDTTL.numTaskID=T.numTaskID AND SPDTTL.tintAction=4) 
	AND T.numAssignTo = ' + CAST(@numUserCntID AS VARCHAR) + '
	AND ((DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',W.dtmStartDate) >= '''+Cast(@startDate as varchar(30))+''' and DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',W.dtmStartDate) <= '''+Cast(@endDate as varchar(30))+''') OR (DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',W.dtmEndDate) >= '''+Cast(@startDate as varchar(30))+''' and DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',W.dtmEndDate) <= '''+Cast(@endDate as varchar(30))+''')) 
) Q WHERE '+@OtherRegularSearch+' '+@IndivRecordPaging+'
'
EXEC (@dynamicQuery)

------------------ 7 -  Requisition Approval ----------------------------

IF(@bitREQPOApproval=1 AND @numUserCntID IN(SELECT * FROM Split(@vchREQPOApprovalEmp,',') WHERE Items<>''))
BEGIN
SET @dynamicQuery =  ' INSERT INTO #tempRecords(
RecordCount,
RecordId,
numCreatedBy,
numOrgTerId,
numTaskId,
IsTaskClosed,
TaskTypeName,
TaskType,
OrigDescription,
Description,
dtDueDate,
DueDate,
TotalProgress,
numAssignedTo,
numAssignToId,
Priority,
Activity,
PriorityId,
ActivityId,
OrgName,
CompanyRating,Startdate,type
) SELECT  COUNT(*) OVER() RecordCount,* FROM( SELECT 
OP.numOppId AS RecordId,
OP.numCreatedBy AS numCreatedBy,
ISNULL(Div.numTerId,0) AS numOrgTerId,
0 AS numTaskId,
CASE WHEN ISNULL(OP.intReqPOApproved,0)=0 THEN 0 ELSE 1 END AS IsTaskClosed,
''<a  onclick="openSalesOrder(''+CAST(OP.numOppId AS VARCHAR)+'')" href="javascript:void(0)">Requisition Approval</a>'' As TaskTypeName,
7 AS TaskType,
'''' AS OrigDescription,
''<b>For :</b> <a class="underLineHyperLink" onclick="openSalesOrder(''+CAST(OP.numOppId AS VARCHAR)+'')" href="javascript:void(0)">''+OP.vcPOppName+''</a> &nbsp;<i onclick="ApproveRejectPORequistion(''+CAST(OP.numOppId AS VARCHAR)+'',1)"  class="fa fa-thumbs-up cursor"></i>  &nbsp;<i onclick="ApproveRejectPORequistion(''+CAST(OP.numOppId AS VARCHAR)+'',2)"  class="fa fa-thumbs-down cursor"></i>'' AS Description,
OP.intPEstimatedCloseDate AS dtDueDate,
OP.intPEstimatedCloseDate as DueDate,
0 As TotalProgress,
dbo.fn_GetContactName('+CAST(@numUserCntID AS VARCHAR(200))+') As numAssignedTo,
'+CAST(@numUserCntID AS VARCHAR(200))+' As numAssignToId,
'''' As Priority, -- Priority column   
'''' As Activity ,
0 As PriorityId, -- Priority column   
 0 As ActivityId ,
 ''<a href="../account/frmAccounts.aspx?DivID=''+CAST(Div.numDivisionID AS VARCHAR)+''">''+Com.vcCompanyName+ CASE WHEN  ISNULL(listCompanyRating.VcData,'''')<>'''' THEN ''(''+listCompanyRating.VcData+'')'' ELSE '''' END +''</a>''  AS OrgName,
 listCompanyRating.VcData As CompanyRating,OP.intPEstimatedCloseDate  AS Startdate,0 AS type

 FROM  
 OpportunityMaster AS OP 
INNER JOIN 
	DivisionMaster Div                           
ON 
	OP.numDivisionId = Div.numDivisionID                       
INNER JOIN  
	CompanyInfo  Com                          
ON 
	Div.numCompanyID = Com.numCompanyId    
LEFT JOIN ListDetails AS listCompanyRating
 ON Com.numCompanyRating=listCompanyRating.NumlistItemID
WHERE
	OP.numDomainID = ' + Cast(@numDomainID as varchar(10))  + ' AND OP.tintOppType = 2 AND OP.tintOppStatus=0 
	AND 1 = (CASE WHEN '+CAST(@decReqPOMinValue AS VARCHAR(100))+'>0 AND Op.monPAmount>'+CAST(@decReqPOMinValue AS VARCHAR(100))+' THEN 1  WHEN '+CAST(@decReqPOMinValue AS VARCHAR(100))+'=0 THEN 1  ELSE 0 END ) ) AS Q
WHERE '+@OtherRegularSearch+' AND (DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',Q.DueDate) >= '''+Cast(@startDate as varchar(30))+''' and DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',Q.DueDate) <= '''+Cast(@endDate as varchar(30))+''') 
'+@IndivRecordPaging+'
'
EXEC (@dynamicQuery)

END
------------------ 8 -  PO Approval ----------------------------
IF(@bitREQPOApproval=1 AND @numUserCntID IN(SELECT * FROM Split(@vchREQPOApprovalEmp,',') WHERE Items<>''))
BEGIN
SET @dynamicQuery =  ' INSERT INTO #tempRecords(
RecordCount,
RecordId,
numCreatedBy,
numOrgTerId,
numTaskId,
IsTaskClosed,
TaskTypeName,
TaskType,
OrigDescription,
Description,
dtDueDate,
DueDate,
TotalProgress,
numAssignedTo,
numAssignToId,
Priority,
Activity,
PriorityId,
ActivityId,
OrgName,
CompanyRating,Startdate,type
) SELECT  COUNT(*) OVER() RecordCount,* FROM ( SELECT
OP.numOppId AS RecordId,
OP.numCreatedBy AS numCreatedBy,
ISNULL(Div.numTerId,0) AS numOrgTerId,
0 AS numTaskId,
CASE WHEN ISNULL(OP.intReqPOApproved,0)=0 THEN 0 ELSE 1 END AS IsTaskClosed,
''<a onclick="openSalesOrder(''+CAST(OP.numOppId AS VARCHAR)+'')" href="javascript:void(0)">PO Approval</a>'' As TaskTypeName,
8 AS TaskType,
'''' AS OrigDescription,
''<b>For:</b> <a href="javascript:void(0)"  class="underLineHyperLink" onclick="openSalesOrder(''+CAST(OP.numOppId AS VARCHAR)+'')">''+OP.vcPOppName+''</a> &nbsp;<i onclick="ApproveRejectPORequistion(''+CAST(OP.numOppId AS VARCHAR)+'',1)"  class="fa fa-thumbs-up cursor"></i>  &nbsp;<i onclick="ApproveRejectPORequistion(''+CAST(OP.numOppId AS VARCHAR)+'',2)"  class="fa fa-thumbs-down cursor"></i> '' AS Description,
OP.intPEstimatedCloseDate AS dtDueDate,
OP.intPEstimatedCloseDate as DueDate,
0 As TotalProgress,
dbo.fn_GetContactName('+CAST(@numUserCntID AS VARCHAR(200))+') As numAssignedTo,
'+CAST(@numUserCntID AS VARCHAR(200))+' As numAssignToId,
'''' As Priority, -- Priority column   
'''' As Activity ,
0 As PriorityId, -- Priority column   
 0 As ActivityId ,
 ''<a href="../account/frmAccounts.aspx?DivID=''+CAST(Div.numDivisionID AS VARCHAR)+''">''+Com.vcCompanyName+ CASE WHEN  ISNULL(listCompanyRating.VcData,'''')<>'''' THEN ''(''+listCompanyRating.VcData+'')'' ELSE '''' END +''</a>''  AS OrgName,
 listCompanyRating.VcData As CompanyRating,OP.intPEstimatedCloseDate AS Startdate,0 AS type

 FROM  
 OpportunityMaster AS OP 
INNER JOIN 
	DivisionMaster Div                           
ON 
	OP.numDivisionId = Div.numDivisionID                       
INNER JOIN  
	CompanyInfo  Com                          
ON 
	Div.numCompanyID = Com.numCompanyId    
LEFT JOIN ListDetails AS listCompanyRating
 ON Com.numCompanyRating=listCompanyRating.NumlistItemID
WHERE
	OP.numDomainID = ' + Cast(@numDomainID as varchar(10))  + ' AND OP.tintOppType = 2 AND OP.tintOppStatus=1 
	AND 1 = (CASE WHEN '+CAST(@decReqPOMinValue AS VARCHAR(100))+'>0 AND Op.monPAmount>'+CAST(@decReqPOMinValue AS VARCHAR(100))+' THEN 1  WHEN '+CAST(@decReqPOMinValue AS VARCHAR(100))+'=0 THEN 1  ELSE 0 END )) AS Q
	WHERE  '+@OtherRegularSearch+' AND  (DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',Q.DueDate) >= '''+Cast(@startDate as varchar(30))+''' and DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',Q.DueDate) <= '''+Cast(@endDate as varchar(30))+''') 
'+@IndivRecordPaging+'
'
EXEC (@dynamicQuery)

END

------------------ 9 -  Price Margin Approval ----------------------------

SET @dynamicQuery =  ' INSERT INTO #tempRecords(
RecordCount,
RecordId,
numCreatedBy,
numOrgTerId,
numTaskId,
IsTaskClosed,
TaskTypeName,
TaskType,
OrigDescription,
Description,
dtDueDate,
DueDate,
TotalProgress,
numAssignedTo,
numAssignToId,
Priority,
Activity,
PriorityId,
ActivityId,
OrgName,
CompanyRating,Startdate,type
) SELECT COUNT(*) OVER() RecordCount,* FROM (SELECT
OP.numOppId AS RecordId,
OP.numCreatedBy AS numCreatedBy,
ISNULL(Div.numTerId,0) AS numOrgTerId,
0 AS numTaskId,
ISNULL(OP.bitReqPOApproved,0) AS IsTaskClosed,
''<a onclick="OpenMarginApproval(''+CAST(OP.numOppId AS VARCHAR)+'')" href="javascript:void(0)">Price Margin Approval</a>'' As TaskTypeName,
9 AS TaskType,
'''' AS OrigDescription,
''<b>For : </b> <a href="javascript:void(0)"  onclick="openSalesOrder(''+CAST(OP.numOppId AS VARCHAR)+'')">''+OP.vcPOppName+''</a> : '' +dbo.ItemUnitPriceApproval_CheckAndGetItemDetails(OP.numOppId,OP.numDomainId)+''  &nbsp;<i onclick="ApproveRejectPriceMarginApproval(''+CAST(OP.numOppId AS VARCHAR)+'',1)" class="fa fa-thumbs-up cursor"></i>  &nbsp;<i onclick="ApproveRejectPriceMarginApproval(''+CAST(OP.numOppId AS VARCHAR)+'',2)" class="fa fa-thumbs-down cursor"></i>''  AS Description,
OP.bintCreatedDate AS dtDueDate,
OP.bintCreatedDate as DueDate,
0 As TotalProgress,
dbo.fn_GetContactName('+CAST(@numUserCntID AS VARCHAR(200))+') As numAssignedTo,
'+CAST(@numUserCntID AS VARCHAR(200))+' As numAssignToId,
'''' As Priority, -- Priority column   
'''' As Activity ,
0 As PriorityId, -- Priority column   
 0 As ActivityId ,
 ''<a href="../account/frmAccounts.aspx?DivID=''+CAST(Div.numDivisionID AS VARCHAR)+''">''+Com.vcCompanyName+ CASE WHEN  ISNULL(listCompanyRating.VcData,'''')<>'''' THEN ''(''+listCompanyRating.VcData+'')'' ELSE '''' END +''</a>''  AS OrgName,
 listCompanyRating.VcData As CompanyRating,OP.bintCreatedDate AS Startdate,0 AS type

 FROM  
 OpportunityMaster AS OP 
INNER JOIN 
	DivisionMaster Div                           
ON 
	OP.numDivisionId = Div.numDivisionID                       
INNER JOIN  
	CompanyInfo  Com                          
ON 
	Div.numCompanyID = Com.numCompanyId    
LEFT JOIN ListDetails AS listCompanyRating
 ON Com.numCompanyRating=listCompanyRating.NumlistItemID
WHERE
	OP.numDomainID = ' + Cast(@numDomainID as varchar(10))  + ' AND OP.tintOppType = 1 AND (SELECT COUNT(OpportunityItems.numoppitemtCode) FROM OpportunityItems 
						WHERE OpportunityItems.bitItemPriceApprovalRequired=1 AND OpportunityItems.numOppId=OP.numOppId) >0
	AND '+CAST(@numUserCntID AS VARCHAR(100))+' IN (SELECT numUserID FROM UnitPriceApprover WHERE numDomainId= ' + Cast(@numDomainID as varchar(10))  + ' )) AS Q
	WHERE  '+@OtherRegularSearch+' AND  (DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',Q.DueDate) >= '''+Cast(@startDate as varchar(30))+''' and DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',Q.DueDate) <= '''+Cast(@endDate as varchar(30))+''') 
'+@IndivRecordPaging+'

'
EXEC (@dynamicQuery)

------------------------10 - A/R Invoice Due------------------------------------
IF(@bitARInvoiceDue=1 AND @numUserCntID IN(SELECT * FROM Split(@vchARInvoiceDue,',') WHERE Items<>''))
BEGIN
 DECLARE  @AuthoritativeSalesBizDocId  AS INTEGER
    SELECT @AuthoritativeSalesBizDocId = isnull(numAuthoritativeSales,0)
    FROM   AuthoritativeBizDocs
    WHERE  numDomainId = @numDomainId;
SET @dynamicQuery=  ' INSERT INTO #tempRecords(
RecordCount,
RecordId,
numCreatedBy,
numOrgTerId,
numTaskId,
IsTaskClosed,
TaskTypeName,
TaskType,
OrigDescription,
Description,
dtDueDate,
DueDate,
TotalProgress,
numAssignedTo,
numAssignToId,
Priority,
Activity,
PriorityId,
ActivityId,
OrgName,
CompanyRating,Startdate,type
) SELECT  COUNT(*) OVER() RecordCount,* FROM ( SELECT
OM.numOppId AS RecordId,
OM.numCreatedBy AS numCreatedBy,
ISNULL(DM.numTerId,0) AS numOrgTerId,
OB.numOppBizDocsId AS numTaskId,
0 AS IsTaskClosed,
''<a href="javascript:void(0)"  onclick="openBizDoc(''+CAST(OM.numOppId AS VARCHAR)+'',''+CAST(OB.numOppBizDocsId AS VARCHAR)+'')">A/R Invoice Due</a>'' As TaskTypeName,
10 AS TaskType,
'''' AS OrigDescription,
''<b>For : </b> <a href="javascript:void(0)" class="underLineHyperLink" onclick="openBizDoc(''+CAST(OM.numOppId AS VARCHAR)+'',''+CAST(OB.numOppBizDocsId AS VARCHAR)+'')">''+CAST(OB.[vcBizDocID] AS VARCHAR)+''</a> of <a href="javascript:void(0)" class="underLineHyperLink" onclick="openSalesOrder(''+CAST(OM.numOppId AS VARCHAR)+'')">''+CAST(OM.[vcPOppName] AS VARCHAR)+''</a> <b>Balance Due</b> <a href="javascript:void(0)" onclick="OpenAmtPaid(''+CAST(OM.numOppId AS VARCHAR)+'',''+CAST(OB.numOppBizDocsId AS VARCHAR)+'',''+CAST(OM.[numDivisionId] AS VARCHAR)+'')"   class="underLineHyperLink">'' + CAST(ISNULL(C.[varCurrSymbol],'''') AS VARCHAR) + '''' + CAST(CAST(isnull(OB.monDealAmount  - ISNULL(TablePayments.monPaidAmount,0),0) AS DECIMAL(18,2)) AS VARCHAR)+''</a>''  AS Description,
 CASE ISNULL(bitBillingTerms,0) 
                          WHEN 1 THEN DATEADD(dd,convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(intBillingDays,0)), 0)),OB.dtFromDate)
                          WHEN 0 THEN ob.[dtFromDate]
                        END AS dtDueDate,
 CASE ISNULL(bitBillingTerms,0) 
                          WHEN 1 THEN [dbo].[FormatedDateFromDate](DATEADD(dd,convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(intBillingDays,0)), 0)),OB.dtFromDate),
                                                                   1)
                          WHEN 0 THEN [dbo].[FormatedDateFromDate](ob.[dtFromDate],1)
                        END as DueDate,
0 As TotalProgress,
'''' As numAssignedTo,
0 As numAssignToId,
'''' As Priority, -- Priority column   
'''' As Activity ,
0 As PriorityId, -- Priority column   
 0 As ActivityId ,
 ''<a href="../account/frmAccounts.aspx?DivID=''+CAST(DM.numDivisionID AS VARCHAR)+''">''+Com.vcCompanyName+ CASE WHEN  ISNULL(listCompanyRating.VcData,'''')<>'''' THEN ''(''+listCompanyRating.VcData+'')'' ELSE '''' END +''</a>''  AS OrgName,
 listCompanyRating.VcData As CompanyRating,
  (CASE ISNULL(bitBillingTerms,0) 
                          WHEN 1 THEN DATEADD(dd,convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(intBillingDays,0)), 0)),OB.dtFromDate)
                          WHEN 0 THEN ob.[dtFromDate]
                        END) AS StartDate,0 AS type

 FROM   [OpportunityMaster] OM INNER JOIN [DivisionMaster] DM ON OM.[numDivisionId] = DM.[numDivisionID]
				LEFT JOIN CompanyInfo  Com ON DM.numCompanyID = Com.numCompanyId    
				LEFT JOIN ListDetails AS listCompanyRating
 ON Com.numCompanyRating=listCompanyRating.NumlistItemID
               LEFT OUTER JOIN OpportunityBizDocs OB ON OB.numOppId = OM.numOppID
               LEFT OUTER JOIN [Currency] C ON OM.[numCurrencyID] = C.[numCurrencyID]
			   OUTER APPLY
			   (
					SELECT 
						SUM(monAmountPaid) AS monPaidAmount
					FROM
						DepositeDetails DD
					INNER JOIN
						DepositMaster DM
					ON
						DD.numDepositID=DM.numDepositId
					WHERE 
						numOppID = OM.numOppID
						AND numOppBizDocsID=OB.numOppBizDocsID
						AND CONVERT(DATE,DM.dtDepositDate) <= DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +','''+Cast(@endDate as varchar(100))+''')
				) TablePayments
        WHERE  OM.[tintOppType] = 1
               AND OM.[tintOppStatus] = 1
               AND om.[numDomainId] = ' + Cast(@numDomainId as varchar(10))  + '
               AND OB.[numBizDocId] = '+CAST(@AuthoritativeSalesBizDocId As VARCHAR)+'
               AND OB.bitAuthoritativeBizDocs=1
               AND ISNULL(OB.tintDeferred,0) <> 1
			   AND (OM.numAccountClass= 0 OR 0=0)
			   AND CONVERT(DATE,OB.[dtCreatedDate]) <= DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +','''+Cast(@endDate as varchar(100))+''')
               AND isnull(OB.monDealAmount
                            ,0) > 0  AND isnull(OB.monDealAmount * OM.fltExchangeRate
                        - ISNULL(TablePayments.monPaidAmount,0) * OM.fltExchangeRate,0) != 0
	) AS Q
	WHERE  '+@OtherRegularSearch+' AND  (DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',Q.DueDate) >= '''+Cast(@startDate as varchar(30))+''' and DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',Q.DueDate) <= '''+Cast(@endDate as varchar(30))+''') 
'+@IndivRecordPaging+'
'
EXEC (@dynamicQuery)
END
------------------------11 - A/P Bill Due------------------------------------
IF(@bitAPBillsDue=1 AND @numUserCntID IN(SELECT * FROM Split(@vchAPBillsDue,',') WHERE Items<>''))
BEGIN   
   DECLARE  @AuthoritativePurchaseBizDocId  AS INTEGER
    SELECT  @AuthoritativePurchaseBizDocId= isnull(numAuthoritativePurchase,0)
    FROM   AuthoritativeBizDocs
    WHERE  numDomainId =  @numDomainId
	

SET @dynamicQuery = '  INSERT INTO #tempRecords(
RecordCount,
RecordId,
numCreatedBy,
numOrgTerId,
numTaskId,
IsTaskClosed,
TaskTypeName,
TaskType,
OrigDescription,
Description,
dtDueDate,
DueDate,
TotalProgress,
numAssignedTo,
numAssignToId,
Priority,
Activity,
PriorityId,
ActivityId,
OrgName,
CompanyRating,Startdate,type
) SELECT  COUNT(*) OVER() RecordCount,* FROM (SELECT
OM.numOppId AS RecordId,
OM.numCreatedBy AS numCreatedBy,
ISNULL(DM.numTerId,0) AS numOrgTerId,
OB.numOppBizDocsId AS numTaskId,
0 AS IsTaskClosed,
''<a href="javascript:void(0)" onclick="openPurchaseBill(''+CAST(DM.[numDivisionID] As VARCHAR)+'')">A/P Bill Due<a/>'' As TaskTypeName,
11 AS TaskType,
'''' AS OrigDescription,
''<b>For : </b> <a href="javascript:void(0)" class="underLineHyperLink" onclick="openBizDoc(''+CAST(OM.numOppId AS VARCHAR)+'',''+CAST(OB.numOppBizDocsId AS VARCHAR)+'')">''+CAST(OB.[vcBizDocID] AS VARCHAR)+''</a> of <a href="javascript:void(0)" class="underLineHyperLink" onclick="openSalesOrder(''+CAST(OM.numOppId AS VARCHAR)+'')">''+CAST(OM.[vcPOppName] AS VARCHAR)+''</a> <b>Balance Due : </b> <a href="javascript:void(0)" onclick="openBizDoc(''+CAST(OM.numOppId AS VARCHAR)+'',''+CAST(OB.numOppBizDocsId AS VARCHAR)+'')"   class="underLineHyperLink">'' + CAST(ISNULL(C.[varCurrSymbol],'''') AS VARCHAR) + '''' + CAST(CAST(isnull((OB.monDealAmount * OM.fltExchangeRate)  - (ISNULL(TablePayment.monPaidAmount,0)* OM.fltExchangeRate),0) AS DECIMAL(18,2))  AS VARCHAR)+''</a>''  AS Description,
 CASE ISNULL(bitBillingTerms,0) 
                          WHEN 1 THEN DATEADD(dd,convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(intBillingDays,0)), 0)),OB.dtFromDate)
                          WHEN 0 THEN ob.[dtFromDate]
                        END AS dtDueDate,
 CASE ISNULL(bitBillingTerms,0) 
                          WHEN 1 THEN [dbo].[FormatedDateFromDate](DATEADD(dd,convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(intBillingDays,0)), 0)),OB.dtFromDate),
                                                                   1)
                          WHEN 0 THEN [dbo].[FormatedDateFromDate](ob.[dtFromDate],1)
                        END as DueDate,
0 As TotalProgress,
'''' As numAssignedTo,
0 As numAssignToId,
'''' As Priority, -- Priority column   
'''' As Activity ,
0 As PriorityId, -- Priority column   
 0 As ActivityId ,
 ''<a href="../account/frmAccounts.aspx?DivID=''+CAST(DM.numDivisionID AS VARCHAR)+''">''+Com.vcCompanyName+ CASE WHEN  ISNULL(listCompanyRating.VcData,'''')<>'''' THEN ''(''+listCompanyRating.VcData+'')'' ELSE '''' END +''</a>''  AS OrgName,
 listCompanyRating.VcData As CompanyRating,
  (CASE ISNULL(bitBillingTerms,0) 
                          WHEN 1 THEN DATEADD(dd,convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(intBillingDays,0)), 0)),OB.dtFromDate)
                          WHEN 0 THEN ob.[dtFromDate]
                        END) AS StartDate,0 AS type

   FROM   [OpportunityMaster] OM
               INNER JOIN [DivisionMaster] DM
                 ON OM.[numDivisionId] = DM.[numDivisionID]
				 LEFT JOIN CompanyInfo  Com ON DM.numCompanyID = Com.numCompanyId    
				LEFT JOIN ListDetails AS listCompanyRating
 ON Com.numCompanyRating=listCompanyRating.NumlistItemID
               LEFT OUTER JOIN OpportunityBizDocs OB
                 ON OB.numOppId = OM.numOppID
               LEFT OUTER JOIN [Currency] C
                 ON OM.[numCurrencyID] = C.[numCurrencyID]
				 OUTER APPLY
				(
					SELECT
						SUm(BPD.monAmount) monPaidAmount
					FROM	
						BillPaymentDetails BPD
					INNER JOIN
						BillPaymentHeader BPH
					ON
						BPD.numBillPaymentID=BPH.numBillPaymentID
					WHERE
						BPD.numOppBizDocsID = OB.numOppBizDocsId
						AND CAST(BPH.dtPaymentDate AS DATE) <= DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +','''+Cast(@endDate as varchar(100))+''')
				) TablePayment
        WHERE  OM.[tintOppType] = 2
               AND OM.[tintOppStatus] = 1
               AND om.[numDomainId] = 1
               AND OB.[numBizDocId] = '+CAST(@AuthoritativePurchaseBizDocId AS VARCHAR)+' 
               AND OB.bitAuthoritativeBizDocs=1
               AND ISNULL(OB.tintDeferred,0) <> 1
			   AND (OM.numAccountClass= 0 OR 0=0)
			   AND CONVERT(DATE,OB.[dtCreatedDate]) <= DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +','''+Cast(@endDate as varchar(100))+''')
               AND  isnull(OB.monDealAmount * OM.fltExchangeRate,0) > 0 
			   AND isnull(OB.monDealAmount * OM.fltExchangeRate
                        - ISNULL(TablePayment.monPaidAmount,0) * OM.fltExchangeRate,0) > 0
	) AS Q
	WHERE  '+@OtherRegularSearch+' AND  (DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',Q.DueDate) >= '''+Cast(@startDate as varchar(30))+''' and DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',Q.DueDate) <= '''+Cast(@endDate as varchar(30))+''') 
'+@IndivRecordPaging+'
'

EXEC (@dynamicQuery)
END

END

DECLARE @strDueDateUpdate1 NVARCHAR(MAX) = ''
DECLARE @strDueDateUpdate2 NVARCHAR(MAX) = ''

SET @strDueDateUpdate1=' UPDATE #tempRecords SET DueDate = 
CASE 
WHEN convert(varchar(11),DueDate)=convert(varchar(11),DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+',getutcdate())) 
then CONCAT(''<b><font color="#FF0000" style="font-size:14px">Today''
	,CASE 
		WHEN ISNULL(bitFollowUpAnyTime,0)=1 
		THEN '' <i>Any time</i>'' 
		WHEN Startdate IS NOT NULL AND EndTime IS NOT NULL 
		THEN  '' @ ''+LEFT(RIGHT(''0''+LTRIM(RIGHT(CONVERT(varchar,Startdate,100),8)),7),PATINDEX(''%[a,p]m'',RIGHT(''0''+LTRIM(RIGHT(CONVERT(varchar,Startdate,100),8)),7))-1) 
		WHEN Startdate IS NOT NULL AND EndTime IS NULL THEN  '' @ ''+RIGHT(''0''+LTRIM(RIGHT(CONVERT(varchar,Startdate,100),8)),7) 
		ELSE '''' 
	END
	,CASE 
		WHEN ISNULL(bitFollowUpAnyTime,0)=0 AND Startdate IS NOT NULL AND EndTime IS NOT NULL 
		THEN  ''<Span style="font-size:14px;font-weight:normal;font-style: italic;"><br/>to ''+RIGHT(''0''+LTRIM(RIGHT(CONVERT(varchar,EndTime,100),8)),7)+''</Span>'' 
		ELSE '''' 
	END ,''</font></b>'') 
WHEN convert(varchar(11),DueDate)=convert(varchar(11),DateAdd(day,1,DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+',getutcdate()))) 
THEN CONCAT(''<b><font color=#ED8F11 style="font-size:14px">Tommorow''
	,CASE 
		WHEN ISNULL(bitFollowUpAnyTime,0)=1 
		THEN '' <i>Any time</i>''  
		WHEN Startdate IS NOT NULL AND EndTime IS NOT NULL 
		THEN  '' @ ''+LEFT(RIGHT(''0''+LTRIM(RIGHT(CONVERT(varchar,Startdate,100),8)),7),PATINDEX(''%[a,p]m'',RIGHT(''0''+LTRIM(RIGHT(CONVERT(varchar,Startdate,100),8)),7))-1) 
		WHEN Startdate IS NOT NULL AND EndTime IS NULL 
		THEN  '' @ ''+RIGHT(''0''+LTRIM(RIGHT(CONVERT(varchar,Startdate,100),8)),7) 
		ELSE '''' 
	END
	,CASE 
		WHEN ISNULL(bitFollowUpAnyTime,0)=0 AND Startdate IS NOT NULL AND EndTime IS NOT NULL 
		THEN  ''<Span style="font-size:14px;font-weight:normal;font-style: italic;"><br/>to ''+RIGHT(''0''+LTRIM(RIGHT(CONVERT(varchar,EndTime,100),8)),7)+''</Span>'' 
		ELSE '''' 
	END ,''</font></b>'') 
WHEN convert(varchar(11),DueDate)=convert(varchar(11),DateAdd(day,2,DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+',getutcdate()))) then CONCAT(''<b><font color=#8FAADC style="font-size:14px">'',datename(dw,DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+',dtDueDate)),''<span style="color:#000;font-weight:500"> - '',CAST(DAY(Startdate) AS VARCHAR(10)),CASE
	WHEN DAY(Startdate) % 100 IN (11, 12, 13) THEN ''th''
	WHEN DAY(Startdate) % 10 = 1 THEN ''st''
	WHEN DAY(Startdate) % 10 = 2 THEN ''nd''
	WHEN DAY(Startdate) % 10 = 3 THEN ''rd''
ELSE ''th'' END,''</span>'',''</font></b>'',''<b>'',''<br/>''
,''<Span style="font-size:14px;font-weight:normal;font-style: italic;">''
,CASE 
	WHEN ISNULL(bitFollowUpAnyTime,0)=1 
	THEN '' <i>Any time</i>'' 
	WHEN Startdate IS NOT NULL AND EndTime IS NOT NULL 
	THEN  LEFT(RIGHT(''0''+LTRIM(RIGHT(CONVERT(varchar,Startdate,100),8)),7),PATINDEX(''%[a,p]m'',RIGHT(''0''+LTRIM(RIGHT(CONVERT(varchar,Startdate,100),8)),7))-1) 
	WHEN Startdate IS NOT NULL AND EndTime IS NULL 
	THEN  RIGHT(''0''+LTRIM(RIGHT(CONVERT(varchar,Startdate,100),8)),7) 
	ELSE '''' 
END,''</span>''
,CASE WHEN ISNULL(bitFollowUpAnyTime,0)=0 AND Startdate IS NOT NULL AND EndTime IS NOT NULL THEN  ''<Span style="font-size:14px;font-weight:normal;font-style: italic;"> to ''+RIGHT(''0''+LTRIM(RIGHT(CONVERT(varchar,EndTime,100),8)),7)+''</Span>'' ELSE '''' END,''</b>'')
WHEN convert(varchar(11),DueDate)=convert(varchar(11),DateAdd(day,3,DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+',getutcdate()))) then CONCAT(''<b><font color=#CC99FF style="font-size:14px">'',datename(dw,DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+',dtDueDate)),''<span style="color:#000;font-weight:500"> - '',CAST(DAY(Startdate) AS VARCHAR(10)),CASE
	WHEN DAY(Startdate) % 100 IN (11, 12, 13) THEN ''th''
	WHEN DAY(Startdate) % 10 = 1 THEN ''st''
	WHEN DAY(Startdate) % 10 = 2 THEN ''nd''
	WHEN DAY(Startdate) % 10 = 3 THEN ''rd''
ELSE ''th'' END,''</span>'',''</font></b>'',''<b>'',''<br/>''
,''<Span style="font-size:14px;font-weight:normal;font-style: italic;">''
,CASE 
	WHEN ISNULL(bitFollowUpAnyTime,0)=1 
	THEN '' <i>Any time</i>'' 
	WHEN Startdate IS NOT NULL AND EndTime IS NOT NULL 
	THEN  LEFT(RIGHT(''0''+LTRIM(RIGHT(CONVERT(varchar,Startdate,100),8)),7),PATINDEX(''%[a,p]m'',RIGHT(''0''+LTRIM(RIGHT(CONVERT(varchar,Startdate,100),8)),7))-1) 
	WHEN Startdate IS NOT NULL AND EndTime IS NULL THEN  RIGHT(''0''+LTRIM(RIGHT(CONVERT(varchar,Startdate,100),8)),7) 
	ELSE '''' 
END,''</span>''
,CASE WHEN ISNULL(bitFollowUpAnyTime,0)=0 AND Startdate IS NOT NULL AND EndTime IS NOT NULL THEN  ''<Span style="font-size:14px;font-weight:normal;font-style: italic;"> to ''+RIGHT(''0''+LTRIM(RIGHT(CONVERT(varchar,EndTime,100),8)),7)+''</Span>'' ELSE '''' END ,''</b>'')
WHEN convert(varchar(11),DueDate)=convert(varchar(11),DateAdd(day,4,DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+',getutcdate()))) then CONCAT(''<b><font color=#AED495 style="font-size:14px">'',datename(dw,DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+',dtDueDate)),''<span style="color:#000;font-weight:500"> - '',CAST(DAY(Startdate) AS VARCHAR(10)),CASE
	WHEN DAY(Startdate) % 100 IN (11, 12, 13) THEN ''th''
	WHEN DAY(Startdate) % 10 = 1 THEN ''st''
	WHEN DAY(Startdate) % 10 = 2 THEN ''nd''
	WHEN DAY(Startdate) % 10 = 3 THEN ''rd''
ELSE ''th'' END,''</span>'',''</font></b>'',''<b>'',''<br/>'''

SET @strDueDateUpdate2 = ',''<Span style="font-size:14px;font-weight:normal;font-style: italic;">''
,CASE 
	WHEN ISNULL(bitFollowUpAnyTime,0)=1 
	THEN '' <i>Any time</i>'' 
	WHEN Startdate IS NOT NULL AND EndTime IS NOT NULL 
	THEN  LEFT(RIGHT(''0''+LTRIM(RIGHT(CONVERT(varchar,Startdate,100),8)),7),PATINDEX(''%[a,p]m'',RIGHT(''0''+LTRIM(RIGHT(CONVERT(varchar,Startdate,100),8)),7))-1) 
	WHEN Startdate IS NOT NULL AND EndTime IS NULL 
	THEN  RIGHT(''0''+LTRIM(RIGHT(CONVERT(varchar,Startdate,100),8)),7) 
	ELSE '''' 
END,''</span>''
,CASE WHEN ISNULL(bitFollowUpAnyTime,0)=0 AND Startdate IS NOT NULL AND EndTime IS NOT NULL THEN ''<Span style="font-size:14px;font-weight:normal;font-style: italic;"> to ''+RIGHT(''0''+LTRIM(RIGHT(CONVERT(varchar,EndTime,100),8)),7)+''</Span>'' ELSE '''' END ,''</b>'')
WHEN convert(varchar(11),DueDate)=convert(varchar(11),DateAdd(day,5,DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+',getutcdate()))) then CONCAT(''<b><font color=#72DFDC style="font-size:14px">'',datename(dw,DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+',dtDueDate)),''<span style="color:#000;font-weight:500"> - '',CAST(DAY(Startdate) AS VARCHAR(10)),CASE
	WHEN DAY(Startdate) % 100 IN (11, 12, 13) THEN ''th''
	WHEN DAY(Startdate) % 10 = 1 THEN ''st''
	WHEN DAY(Startdate) % 10 = 2 THEN ''nd''
	WHEN DAY(Startdate) % 10 = 3 THEN ''rd''
ELSE ''th'' END,''</span>'',''</font></b>'',''<b>'',''<br/>''
,''<Span style="font-size:14px;font-weight:normal;font-style: italic;">''
,CASE 
	WHEN ISNULL(bitFollowUpAnyTime,0)=1 THEN '' <i>Any time</i>'' 
	WHEN Startdate IS NOT NULL AND EndTime IS NOT NULL 
	THEN  LEFT(RIGHT(''0''+LTRIM(RIGHT(CONVERT(varchar,Startdate,100),8)),7),PATINDEX(''%[a,p]m'',RIGHT(''0''+LTRIM(RIGHT(CONVERT(varchar,Startdate,100),8)),7))-1) 
	WHEN Startdate IS NOT NULL AND EndTime IS NULL 
	THEN  RIGHT(''0''+LTRIM(RIGHT(CONVERT(varchar,Startdate,100),8)),7) 
	ELSE '''' 
END,''</span>''
,CASE WHEN ISNULL(bitFollowUpAnyTime,0)=0 AND Startdate IS NOT NULL AND EndTime IS NOT NULL THEN ''<Span style="font-size:14px;font-weight:normal;font-style: italic;"> to ''+RIGHT(''0''+LTRIM(RIGHT(CONVERT(varchar,EndTime,100),8)),7)+''</Span>'' ELSE '''' END,''</b>'')
WHEN convert(varchar(11),DueDate)=convert(varchar(11),DateAdd(day,6,DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+',getutcdate()))) then CONCAT(''<b><font color=#FF9999 style="font-size:14px">'',datename(dw,DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+',dtDueDate)),''<span style="color:#000;font-weight:500"> - '',CAST(DAY(Startdate) AS VARCHAR(10)),CASE
	WHEN DAY(Startdate) % 100 IN (11, 12, 13) THEN ''th''
	WHEN DAY(Startdate) % 10 = 1 THEN ''st''
	WHEN DAY(Startdate) % 10 = 2 THEN ''nd''
	WHEN DAY(Startdate) % 10 = 3 THEN ''rd''
ELSE ''th'' END,''</span>'',''</font></b>'',''<b>'',''<br/>''
,''<Span style="font-size:14px;font-weight:normal;font-style: italic;">''
,CASE 
	WHEN ISNULL(bitFollowUpAnyTime,0)=1 THEN '' <i>Any time</i>'' 
	WHEN Startdate IS NOT NULL AND EndTime IS NOT NULL 
	THEN  LEFT(RIGHT(''0''+LTRIM(RIGHT(CONVERT(varchar,Startdate,100),8)),7),PATINDEX(''%[a,p]m'',RIGHT(''0''+LTRIM(RIGHT(CONVERT(varchar,Startdate,100),8)),7))-1) 
	WHEN Startdate IS NOT NULL AND EndTime IS NULL 
	THEN  RIGHT(''0''+LTRIM(RIGHT(CONVERT(varchar,Startdate,100),8)),7) 
	ELSE '''' 
END,''</span>''
,CASE WHEN ISNULL(bitFollowUpAnyTime,0)=0 AND Startdate IS NOT NULL AND EndTime IS NOT NULL THEN  ''<Span style="font-size:14px;font-weight:normal;font-style: italic;"> to ''+RIGHT(''0''+LTRIM(RIGHT(CONVERT(varchar,EndTime,100),8)),7)+''</Span>'' ELSE '''' END ,''</b>'') 
ELSE CONCAT(''<span style="color:#909090;font-size:14px;font-weight:bold">'',(SELECT FORMAT(CAST(DueDate AS DATE), ''MMM-dd-yyyy''))
,CASE 
	WHEN ISNULL(bitFollowUpAnyTime,0)=1 
	THEN '' <i>Any time</i>''
	WHEN Startdate IS NOT NULL AND EndTime IS NOT NULL 
	THEN  '' @ ''+LEFT(RIGHT(''0''+LTRIM(RIGHT(CONVERT(varchar,Startdate,100),8)),7),PATINDEX(''%[a,p]m'',RIGHT(''0''+LTRIM(RIGHT(CONVERT(varchar,Startdate,100),8)),7))-1) 
	WHEN Startdate IS NOT NULL AND EndTime IS NULL THEN  '' @ ''+RIGHT(''0''+LTRIM(RIGHT(CONVERT(varchar,Startdate,100),8)),7) 
	ELSE '''' 
END,CASE WHEN ISNULL(bitFollowUpAnyTime,0)=0 AND Startdate IS NOT NULL AND EndTime IS NOT NULL 
THEN  ''<Span style="font-size:14px;font-weight:normal;font-style: italic;"><br/>to ''+RIGHT(''0''+LTRIM(RIGHT(CONVERT(varchar,EndTime,100),8)),7)+''</Span>'' 
ELSE '''' 
END,''</span>'') END  '
 EXEC (@strDueDateUpdate1 + @strDueDateUpdate2)
 
 DECLARE @strSql3 VARCHAR(MAX)=''
 IF LEN(@columnName) > 0
BEGIN
	SET @strSql3= CONCAT(@strSql3,' order by ',CASE WHEN @columnName = 'Action-Item Participants' THEN CONCAT('[','Action-Item Participants',']') WHEN @columnName='DueDate' THEN 'dtDueDate' ELSE @columnName END,' ',@columnSortOrder)
END
ELSE
BEGIN
	SET @strSql3= CONCAT(@strSql3,' ORDER BY dtDueDate ASC  ')
END
SET @TotalRecordCount=(SELECT ISNULL(SUM(DISTINCT RecordCount),0) FROM #tempRecords)

SET @dynamicQuery = 'SELECT '+CAST(@TotalRecordCount AS varchar(400))+' AS TotalRecords,T.*
 FROM #tempRecords T WHERE 1=1 '+@strSql3
--SET @dynamicQuery = 'SELECT '+CAST(@TotalRecordCount AS varchar(400))+' AS TotalRecords,T.*
-- FROM #tempRecords T WHERE 1=1 '+@strSql3

 PRINT @dynamicQuery
EXEC(@dynamicQuery)
DECLARE @TempColumns TABLE
	(
        vcFieldName VARCHAR(200)
        ,vcDbColumnName VARCHAR(200)
        ,vcOrigDbColumnName VARCHAR(200)
        ,bitAllowSorting BIT
        ,numFieldId INT
        ,bitAllowEdit BIT
        ,bitCustomField BIT
        ,vcAssociatedControlType VARCHAR(50)
        ,vcListItemType  VARCHAR(200)
        ,numListID INT
        ,ListRelID INT
        ,vcLookBackTableName VARCHAR(200)
        ,bitAllowFiltering BIT
        ,vcFieldDataType  VARCHAR(20)
		,intColumnWidth INT
		,bitClosedColumn BIT
		,bitFieldMessage BIT DEFAULT 0
		,vcFieldMessage VARCHAR(500)
		,bitIsRequired BIT DEFAULT 0
		,bitIsNumeric BIT DEFAULT 0
		,bitIsAlphaNumeric BIT DEFAULT 0
		,bitIsEmail BIT DEFAULT 0
		,bitIsLengthValidation  BIT DEFAULT 0
		,intMaxLength INT DEFAULT 0
		,intMinLength INT DEFAULT 0
	)



	DECLARE @Nocolumns TINYINT
	SET @Nocolumns=0       
         
	SELECT 
		@Nocolumns=COUNT(*) 
	FROM 
		View_DynamicColumns 
	WHERE 
		numFormId=43 
		AND numUserCntID=@numUserCntID 
		AND numDomainID=@numDomainID 
		AND tintPageType=1 
  
  
if @Nocolumns > 0            
begin      
INSERT INTO #tempForm
select tintRow+1 as tintOrder,vcDbColumnName,vcFieldDataType,vcOrigDbColumnName,bitAllowFiltering,ISNULL(vcCultureFieldName,vcFieldName),                  
 vcAssociatedControlType,vcListItemType,numListID,vcLookBackTableName                                                 
,bitCustom,numFieldId,bitAllowSorting,bitAllowEdit,
bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage vcFieldMessage,ListRelID,intColumnWidth
 FROM View_DynamicColumns 
 where numFormId=43 and numUserCntID=@numUserCntID and numDomainID=@numDomainID AND tintPageType=1 
  AND ISNULL(bitSettingField,0)=1 AND ISNULL(bitCustom,0)=0
  order by tintOrder asc  
  
end            
else            
begin 

	--Check if Master Form Configuration is created by administrator if NoColumns=0
	DECLARE @IsMasterConfAvailable BIT = 0
	DECLARE @numUserGroup NUMERIC(18,0)

	SELECT @numUserGroup = numGroupID FROM UserMaster WHERE numUserDetailId = @numUserCntID 

	IF (SELECT COUNT(*) FROM BizFormWizardMasterConfiguration WHERE numDomainID = @numDomainID AND numFormID = 43 AND bitGridConfiguration = 1 AND numGroupID = @numUserGroup) > 0
	BEGIN
		SET @IsMasterConfAvailable = 1
	END

	--If MasterConfiguration is available then load it otherwise load default columns
	IF @IsMasterConfAvailable = 1
	BEGIN
		INSERT INTO DycFormConfigurationDetails 
		(
			numFormID,numFieldID,intColumnNum,intRowNum,numDomainID,numUserCntID,numRelCntType,tintPageType,bitCustom,intColumnWidth
		)
		SELECT 
			43,numFieldId,intColumnNum,intRowNum,@numDomainID,@numUserCntID,NULL,1,0,intColumnWidth
		FROM 
			View_DynamicColumnsMasterConfig
		WHERE
			View_DynamicColumnsMasterConfig.numFormId=43 AND 
			View_DynamicColumnsMasterConfig.numDomainID=@numDomainID AND 
			View_DynamicColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
			View_DynamicColumnsMasterConfig.bitGridConfiguration = 1 AND
			ISNULL(View_DynamicColumnsMasterConfig.bitSettingField,0)=1 AND 
			ISNULL(View_DynamicColumnsMasterConfig.bitCustom,0)=0

		INSERT INTO #tempForm
		SELECT 
			(intRowNum + 1) as tintOrder, vcDbColumnName,vcFieldDataType,vcOrigDbColumnName,bitAllowFiltering, ISNULL(vcCultureFieldName,vcFieldName), vcAssociatedControlType,
			vcListItemType,numListID,vcLookBackTableName, bitCustom,numFieldId,bitAllowSorting,bitAllowEdit,
			bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
			intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage vcFieldMessage,ListRelID,intColumnWidth
		FROM 
			View_DynamicColumnsMasterConfig
		WHERE
			View_DynamicColumnsMasterConfig.numFormId=43 AND 
			View_DynamicColumnsMasterConfig.numDomainID=@numDomainID AND 
			View_DynamicColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
			View_DynamicColumnsMasterConfig.bitGridConfiguration = 1 AND
			ISNULL(View_DynamicColumnsMasterConfig.bitSettingField,0)=1 AND 
			ISNULL(View_DynamicColumnsMasterConfig.bitCustom,0)=0
	ORDER BY 
		tintOrder ASC  
	END
	ELSE                                        
	BEGIN
		 INSERT INTO #tempForm
		select tintOrder,vcDbColumnName,vcFieldDataType
		,vcOrigDbColumnName,bitAllowFiltering,ISNULL(vcCultureFieldName,vcFieldName),                  
		 vcAssociatedControlType,vcListItemType,numListID,vcLookBackTableName                                                 
		,bitCustom,numFieldId,bitAllowSorting,bitAllowEdit,
		bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
		intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage vcFieldMessage,ListRelID,intColumnWidth
		 FROM View_DynamicDefaultColumns
		 where numFormId=43 and bitDefault=1 AND ISNULL(bitSettingField,0)=1 AND numDomainID=@numDomainID
		order by tintOrder asc  
	END 
enD

INSERT INTO #tempForm
	SELECT 
		tintOrder,vcDbColumnName,vcFieldDataType
		,vcOrigDbColumnName,bitAllowFiltering,ISNULL(vcCultureFieldName,vcFieldName),                  
		 vcAssociatedControlType,vcListItemType,numListID,vcLookBackTableName                                                 
		,bitCustom,numFieldId,bitAllowSorting,bitAllowEdit,
		bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
		intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage vcFieldMessage,ListRelID,intColumnWidth
	FROM 
		View_DynamicDefaultColumns
	WHERE 
		numFormId=43 
		AND bitDefault=1 
		AND ISNULL(bitSettingField,0)=1 
		AND numDomainID=@numDomainID
		AND numFieldID NOT IN (SELECT numFieldId FROM #tempForm)
	ORDER BY 
		tintOrder asc  
			
UPDATE
		TC
	SET
		TC.intColumnWidth = (CASE WHEN TC.vcOrigDbColumnName = 'DueDate' AND ISNULL(DFCD.intColumnWidth,0) < 180 THEN 180 ELSE ISNULL(DFCD.intColumnWidth,0) END)
	FROM
		#tempForm TC
	INNER JOIN
		DycFormConfigurationDetails DFCD
	ON
		TC.numFieldId = DFCD.numFieldId
	WHERE
		DFCD.numDomainId = @numDomainID
		AND DFCD.numUserCntID = @numUserCntID
		AND DFCD.numFormId = 43
		AND DFCD.tintPageType = 1 


	SELECT * FROM #tempForm order by tintOrder asc  
END
