GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
                    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_WorkOrder_GetBOMDetail')
DROP PROCEDURE USP_WorkOrder_GetBOMDetail
GO
CREATE PROCEDURE [dbo].[USP_WorkOrder_GetBOMDetail]                             
	@numDomainID AS NUMERIC(18,0)                            
	,@numWOID NUMERIC(18,0)
AS                            
BEGIN
	DECLARE @tintCommitAllocation AS TINYINT
	SELECT @tintCommitAllocation=ISNULL(tintCommitAllocation,1) FROM Domain WHERE numDomainId=@numDomainID

	DECLARE @TEMP TABLE
	(
		numParentID NUMERIC(18,0)
		,ID NUMERIC(18,0)
		,numOppId NUMERIC(18,0)
		,numItemCode NUMERIC(18,0)
		,vcItemName VARCHAR(500)
		,numRequiredQty FLOAT
		,numWarehouseItemID NUMERIC(18,0)
		,vcWarehouse VARCHAR(200)
		,numOnHand FLOAT
		,numAvailable FLOAT
		,numOnOrder FLOAT
		,numAllocation FLOAT
		,numBackOrder FLOAT
		,monCost DECIMAL(20,5)
		,bitWorkOrder BIT
		,bitReadyToBuild BIT
	)

	;WITH CTEWorkOrder (numParentWOID,numWOId,numOppId,numWODetailId,numItemCode,vcItemName,numQtyItemsReq,numWarehouseItemID,monCost,bitReadyToBuild) AS
	(
	
		SELECT
			CAST(0 AS NUMERIC),
			WOD.numWOId,
			WorkOrder.numOppId,
			WOD.numWODetailId,
			Item.numItemCode,
			CONCAT(Item.vcItemName,(CASE WHEN ISNULL(Item.vcSKU,'') <> '' THEN CONCAT(' (',Item.vcSKU,')') ELSE '' END)),
			WOD.numQtyItemsReq,
			WOD.numWarehouseItemID,
			ISNULL(WOD.monAverageCost,ISNULL(Item.monAverageCost,0)) * ISNULL(WOD.numQtyItemsReq,0),
			CAST((CASE WHEN WorkOrder.numWOStatus = 23184 THEN 1 ELSE 0 END) AS BIT)
		FROM 
			WorkOrder 
		INNER JOIN
			WorkOrderDetails WOD
		ON
			WorkOrder.numWOId = WOD.numWOId
		INNER JOIN 
			Item
		ON
			WOD.numChildItemID = Item.numItemCode
		WHERE
			WorkOrder.numDomainID = @numDomainID
			AND WorkOrder.numWOId = @numWOID
		UNION ALL
		SELECT
			CTEWorkOrder.numWODetailId
			,WorkOrder.numWOId
			,WorkOrder.numOppId
			,WorkOrderDetails.numWODetailId
			,Item.numItemCode,
			CONCAT(Item.vcItemName,(CASE WHEN ISNULL(Item.vcSKU,'') <> '' THEN CONCAT(' (',Item.vcSKU,')') ELSE '' END)),
			WorkOrderDetails.numQtyItemsReq,
			WorkOrderDetails.numWarehouseItemID,
			ISNULL(WorkOrderDetails.monAverageCost,ISNULL(Item.monAverageCost,0)) * ISNULL(WorkOrderDetails.numQtyItemsReq,0),
			CAST((CASE WHEN WorkOrder.numWOStatus = 23184 THEN 1 ELSE 0 END) AS BIT)
		FROM
			WorkOrder
		INNER JOIN
			CTEWorkOrder
		ON
			WorkOrder.numParentWOID = CTEWorkOrder.numWOID
			AND WorkOrder.numItemCode = CTEWorkOrder.numItemCode
		INNER JOIN
			WorkOrderDetails
		ON
			WorkOrder.numWOId = WorkOrderDetails.numWOId
		INNER JOIN 
			Item
		ON
			WorkOrderDetails.numChildItemID = Item.numItemCode
	)

	INSERT INTO @TEMP
	(
		numParentID
		,ID
		,numItemCode
		,vcItemName
		,numRequiredQty
		,numWarehouseItemID
		,vcWarehouse
		,numOnHand
		,numAvailable
		,numOnOrder
		,numAllocation
		,numBackOrder
		,monCost
		,bitWorkOrder
		,bitReadyToBuild
	)
	SELECT
		CTEWorkOrder.numParentWOID
		,CTEWorkOrder.numWODetailId
		,numItemCode
		,vcItemName
		,numQtyItemsReq
		,CTEWorkOrder.numWarehouseItemID
		,Warehouses.vcWareHouse
		,ISNULL(WareHouseItems.numOnHand,0) + ISNULL(WareHouseItems.numAllocation,0)
		,WareHouseItems.numOnHand
		,WareHouseItems.numOnOrder
		,WareHouseItems.numAllocation
		,WareHouseItems.numBackOrder
		,ISNULL(CTEWorkOrder.monCost,0)
		,(CASE WHEN (SELECT COUNT(*) FROM WorkOrder WOInner WHERE WOInner.numParentWOID=CTEWorkOrder.numWOId AND WOInner.numItemCode = CTEWorkOrder.numItemCode) > 0 THEN 1 ELSE 0 END)
		,CASE 
			WHEN ISNULL(WareHouseItems.numWareHouseItemID,0) > 0 AND ISNULL(CTEWorkOrder.bitReadyToBuild,0) = 0
			THEN
				CASE 
					WHEN (CASE WHEN ISNULL(CTEWorkOrder.numOppId,0) > 0 THEN 1 ELSE 0 END)=1 AND @tintCommitAllocation=2 
					THEN (CASE WHEN ISNULL(numQtyItemsReq,0) > (ISNULL(WareHouseItems.numOnHand,0)  + ISNULL((SELECT 
																												SUM(ISNULL(WIInner.numOnHand,0) + ISNULL(WIInner.numAllocation,0))
																											FROM 
																												WareHouseItems WIInner
																											WHERE 
																												WIInner.numItemID=WareHouseItems.numItemID 
																												AND WIInner.numWareHouseID = WareHouseItems.numWareHouseID
																												AND WIInner.numWareHouseItemID <> WareHouseItems.numWareHouseItemID),0)) THEN 0 ELSE 1 END)
					ELSE (CASE WHEN ISNULL(numQtyItemsReq,0) > (ISNULL(WareHouseItems.numAllocation,0) + ISNULL((SELECT 
																													SUM(ISNULL(WIInner.numOnHand,0) + ISNULL(WIInner.numAllocation,0))
																												FROM 
																													WareHouseItems WIInner
																												WHERE 
																													WIInner.numItemID=WareHouseItems.numItemID 
																													AND WIInner.numWareHouseID = WareHouseItems.numWareHouseID
																													AND WIInner.numWareHouseItemID <> WareHouseItems.numWareHouseItemID),0)) THEN 0 ELSE 1 END) 
				END
			ELSE 1
		END
	FROM
		CTEWorkOrder
	LEFT JOIN
		WareHouseItems
	ON
		CTEWorkOrder.numWarehouseItemID = WareHouseItems.numWareHouseItemID
	LEFT JOIN
		Warehouses
	ON
		WareHouseItems.numWareHouseID = Warehouses.numWareHouseID

	SELECT * FROM @TEMP
END
GO