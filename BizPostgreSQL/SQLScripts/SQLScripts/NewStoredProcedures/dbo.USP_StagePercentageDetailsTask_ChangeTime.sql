GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_StagePercentageDetailsTask_ChangeTime')
DROP PROCEDURE dbo.USP_StagePercentageDetailsTask_ChangeTime
GO
CREATE PROCEDURE [dbo].[USP_StagePercentageDetailsTask_ChangeTime]
(
	@numDomainID NUMERIC(18,0)
	,@numTaskID NUMERIC(18,0)
	,@numHours NUMERIC(18,0)
	,@numMinutes NUMERIC(18,0)
	,@bitMasterUpdateAlso BIT
)
AS 
BEGIN
	IF EXISTS (SELECT ID FROM StagePercentageDetailsTaskTimeLog WHERE numDomainID=@numDomainID AND numTaskID = @numTaskID AND tintAction=4)
	BEGIN
		RAISERROR('TASK_IS_MARKED_AS_FINISHED',16,1)
		RETURN
	END
	ELSE IF ISNULL((SELECT TOP 1 tintAction FROM StagePercentageDetailsTaskTimeLog WHERE numDomainID=@numDomainID AND numTaskID = @numTaskID ORDER BY tintAction),0) IN (1,3)
	BEGIN
		RAISERROR('TASK_IS_ALREADY_STARTED',16,1)
		RETURN
	END
	ELSE
	BEGIN
		DECLARE @numToTalTimeSpendOnTask NUMERIC(18,0)
		DECLARE @vcTimeSpend VARCHAR(20) = ''
		SET @vcTimeSpend = dbo.GetTimeSpendOnTask(@numDomainID,@numTaskID,0)

		IF @vcTimeSpend = 'Invalid time sequence'
		BEGIN
			RAISERROR('INVALID_TIME_ENTRY_SEQUENCE',16,1)
			RETURN
		END

		SELECT @numToTalTimeSpendOnTask = (CAST(SUBSTRING(@vcTimeSpend,0,CHARINDEX(':',@vcTimeSpend)) AS INT) * 60) + CAST(SUBSTRING(@vcTimeSpend,CHARINDEX(':',@vcTimeSpend) + 1,LEN(@vcTimeSpend)) AS INT)

		If @numToTalTimeSpendOnTask > ((ISNULL(@numHours,0) * 60) + ISNULL(@numMinutes,0))
		BEGIN
			RAISERROR('MORE_TIME_SPEND_ON_TASK',16,1)
			RETURN
		END

		UPDATE StagePercentageDetailsTask SET numHours=@numHours,numMinutes=@numMinutes WHERE numDomainID=@numDomainID AND numTaskId=@numTaskID

		IF @bitMasterUpdateAlso = 1
		BEGIN
			DECLARE @numReferenceTaskId NUMERIC(18,0)
			SELECT @numReferenceTaskId=ISNULL(numReferenceTaskId,0) FROM StagePercentageDetailsTask WHERE numDomainID=@numDomainID AND numTaskId=@numTaskID

			UPDATE StagePercentageDetailsTask SET numHours=@numHours,numMinutes=@numMinutes WHERE numDomainID=@numDomainID AND numTaskId=@numReferenceTaskId 
		END
	END	
END
GO