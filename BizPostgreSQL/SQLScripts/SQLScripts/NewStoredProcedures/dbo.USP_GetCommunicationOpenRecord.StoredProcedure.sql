
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetCommunicationOpenRecord')
DROP PROCEDURE USP_GetCommunicationOpenRecord
GO
CREATE PROCEDURE [dbo].[USP_GetCommunicationOpenRecord]
@numDomainID AS numeric(9),
@numOpenRecordID AS numeric(9),
@ClientTimeZoneOffset AS INT
AS
BEGIN

SELECT Comm.numCommId,dbo.fn_GetContactName(Comm.numContactId) AS vcContactName,
dbo.fn_GetComapnyName(comm.numDivisionId) AS vcDivisionName
,dbo.GetListIemName(Comm.bitTask) AS vcTask,dbo.GetListIemName(Comm.numActivity) AS vcActivity,dbo.GetListIemName(Comm.numStatus) AS vcStatus,
dbo.FormatedDateTimeFromDate(DateAdd(minute,-@ClientTimeZoneOffset, dtStartTime),Comm.numDomainID) as dtStartTime,
dbo.FormatedDateTimeFromDate(DateAdd(minute, -@ClientTimeZoneOffset, CASE WHEN [bitTask]=974 THEN dtStartTime ELSE dtEndTime END),Comm.numDomainID) as dtEndTime,
dbo.fn_GetContactName(Comm.numAssign) AS vcAssignTo
FROM  dbo.Communication Comm JOIN Correspondence Corr ON Comm.numCommId=Corr.numCommID
WHERE Comm.numDomainID=@numDomainID AND Corr.numDomainID=Comm.numDomainID 
AND Corr.tintCorrType IN (1,2,3,4) AND Corr.numOpenRecordID=@numOpenRecordID

END