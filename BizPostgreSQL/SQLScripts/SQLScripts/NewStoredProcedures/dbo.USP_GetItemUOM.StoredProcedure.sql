GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetItemUOM')
DROP PROCEDURE USP_GetItemUOM
GO
CREATE PROCEDURE [dbo].[USP_GetItemUOM]                                                                          
 @numDomainID as numeric(9),    
 @numItemCode as numeric(9),
 @bUnitAll AS bit              
AS       
   
--   IF(@bUnitAll=1 AND (SELECT COUNT(*) FROM ItemUOM WHERE numItemCode=@numItemCode AND numDomainID=@numDomainID)=0)
--   BEGIN
--       EXEC USP_GetUOM @numDomainID
--   END
--   ELSE
--   BEGIN
--        SELECT u.numUOMId,u.vcUnitName FROM 
--        ItemUOM iu INNER JOIN UOM u ON (iu.numUOMId = u.numUOMId AND iu.numItemCode=@numItemCode)
--        WHERE u.numDomainID=@numDomainID AND iu.numDomainID=@numDomainID
--   END
--   

-- IF((SELECT COUNT(*) FROM UOM WHERE numDomainID=@numDomainID)=0)
--		BEGIN
--			INSERT INTO UOM (numDomainId,vcUnitName,tintUnitType,bitEnabled)
--			SELECT @numDomainID, vcUnitName,tintUnitType,bitEnabled FROM UOM WHERE numDomainId=0
--		END
		
 SELECT u.numUOMId,u.vcUnitName FROM 
      UOM u INNER JOIN Domain d ON u.numDomainID=d.numDomainID
        WHERE u.numDomainID=@numDomainID AND d.numDomainID=@numDomainID AND 
        u.tintUnitType=(CASE WHEN ISNULL(d.charUnitSystem,'E')='E' THEN 1 WHEN d.charUnitSystem='M' THEN 2 END)