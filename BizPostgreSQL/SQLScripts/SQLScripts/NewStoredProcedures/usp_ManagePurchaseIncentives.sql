GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_ManagePurchaseIncentives')
DROP PROCEDURE usp_ManagePurchaseIncentives
GO
CREATE Proc [dbo].[usp_ManagePurchaseIncentives]   
@numDivisionID as numeric(9),
@numDomainID as numeric(9),
@strXML as varchar(1000)
AS
BEGIN
	DECLARE @hDoc INT    
	DELETE FROM PurchaseIncentives WHERE numDivisionID=@numDivisionID    
	
	EXEC sp_xml_preparedocument @hDoc OUTPUT, @strXML    

	INSERT INTO PurchaseIncentives 
	(
		numDivisionID
		,numDomainID
		,vcBuyingQty
		,intType
		,vcIncentives
		,tintDiscountType
	)     
	SELECT 
		@numDivisionID
		,@numDomainID
		,X.BuyIncentives
		,X.IncentivesType
		,X.IncentiveDesc
		,X.DiscountType
	FROM
	(
		SELECT 
			*
		FROM 
			OPENXML (@hDoc,'/NewDataSet/PurchaseIncentives',2)    
		WITH
		(BuyIncentives FLOAT,IncentivesType INT,IncentiveDesc FLOAT, DiscountType TINYINT)
	)X    
    
	EXEC sp_xml_removedocument @hDoc  

END