/****** Object:  StoredProcedure [dbo].[usp_getlistType]    Script Date: 07/26/2008 16:17:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
---created by anoop jayaraj  
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getlistDataType')
DROP PROCEDURE usp_getlistDataType
GO
CREATE PROCEDURE [dbo].[usp_getlistDataType]  
@numDomainID as numeric(9),
@numListID AS NUMERIC(9) 
as  
  
select numListID,vcListName,vcDataType from listmaster where (numdomainID=@numDomainID or bitFlag=1) AND numListID=@numListID
GO
