GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ManageFinancialYear')
DROP PROCEDURE USP_ManageFinancialYear
GO
CREATE PROCEDURE [dbo].[USP_ManageFinancialYear]
    @numFinYearId NUMERIC(9) = 0 OUTPUT,
    @numDomainId NUMERIC(9),
    @dtPeriodFrom DATETIME,
    @dtPeriodTo DATETIME,
    @vcFinYearDesc VARCHAR(50),
    @bitCloseStatus BIT,
    @bitAuditStatus BIT,
    @bitCurrentYear BIT
AS 
    SET NOCOUNT ON
    IF @bitCurrentYear = 1 
        BEGIN
            UPDATE  [FinancialYear]
            SET     [bitCurrentYear] = 0
            WHERE   [numDomainId] = @numDomainID	
        END
    IF @numFinYearId = 0 
        BEGIN
            
            INSERT  INTO FinancialYear
                    (
                      [numDomainId],
                      [dtPeriodFrom],
                      [dtPeriodTo],
                      [vcFinYearDesc],
                      [bitCloseStatus],
                      [bitAuditStatus],
                      [bitCurrentYear]
	              )
            VALUES  (
                      @numDomainId,
                      @dtPeriodFrom,
                      @dtPeriodTo,
                      @vcFinYearDesc,
                      @bitCloseStatus,
                      @bitAuditStatus,
                      @bitCurrentYear
	              )
            SELECT  @numFinYearId = SCOPE_IDENTITY() 
	
        END
    ELSE 
        BEGIN
            UPDATE  FinancialYear
            SET     [numDomainId] = @numDomainId,
                    [dtPeriodFrom] = @dtPeriodFrom,
                    [dtPeriodTo] = @dtPeriodTo,
                    [vcFinYearDesc] = @vcFinYearDesc,
--                    [bitCloseStatus] = @bitCloseStatus,
--                    [bitAuditStatus] = @bitAuditStatus,
                    [bitCurrentYear] = @bitCurrentYear
            WHERE   [numFinYearId] = @numFinYearId
            SELECT  @numFinYearId
        END