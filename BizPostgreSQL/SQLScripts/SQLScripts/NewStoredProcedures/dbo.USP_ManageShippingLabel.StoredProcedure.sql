/****** Object:  StoredProcedure [dbo].[USP_ManageShippingLabel]    Script Date: 05/07/2009 17:41:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--SELECT * FROM [ShippingLabel]
--DECLARE @list VARCHAR(8000)
-- SELECT @list = COALESCE(@list + ',', '') + [vcTrackingNumber]
--            FROM [ShippingLabel]
--SELECT @list



GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ManageShippingLabel')
DROP PROCEDURE USP_ManageShippingLabel
GO
CREATE PROCEDURE [dbo].[USP_ManageShippingLabel]
    @numUserCntId NUMERIC(18, 0),
    @strItems TEXT
AS 
    BEGIN
  
    
        DECLARE @hDocItem INT
        EXEC sp_xml_preparedocument @hDocItem OUTPUT, @strItems
    
        UPDATE  [ShippingBox]
        SET     vcShippingLabelImage = X.vcShippingLabelImage,
                vcTrackingNumber = X.vcTrackingNumber,
                bitIsMasterTrackingNo = X.bitIsMasterTrackingNo,
                dtCreateDate = GETUTCDATE(),
                numCreatedBy = @numUserCntId
        FROM    ( SELECT    *
                  FROM      OPENXML (@hDocItem, '/NewDataSet/Table1', 2) WITH ( numBoxID NUMERIC(18, 0), vcShippingLabelImage VARCHAR(100), vcTrackingNumber VARCHAR(50), bitIsMasterTrackingNo BIT )
                ) X
        WHERE   X.[numBoxID] = [ShippingBox].[numBoxID]


-- Copy tracking number to each line item of BizDoc on generation Shipping Lable

	CREATE TABLE #temp_TrackingNo 
	( numBoxID NUMERIC(18, 0),vcTrackingNumber VARCHAR(50) )

	INSERT INTO #temp_TrackingNo
	SELECT    numBoxID,vcTrackingNumber
	FROM      OPENXML (@hDocItem, '/NewDataSet/Table1', 2) WITH ( numBoxID NUMERIC(18, 0),vcTrackingNumber VARCHAR(50))
	

	UPDATE [OpportunityBizDocItems] 
		SET [vcTrackingNo] = X.[vcTrackingNumber]
	FROM (SELECT SR.[numOppBizDocItemID],vcTrackingNumber FROM  #temp_TrackingNo t INNER JOIN [ShippingReportItems] SR ON SR.[numBoxID] = t.numBoxID )X
	WHERE X.numOppBizDocItemID = [OpportunityBizDocItems].[numOppBizDocItemID]	


	DROP TABLE #temp_TrackingNo

	EXEC sp_xml_removedocument @hDocItem
        
END
