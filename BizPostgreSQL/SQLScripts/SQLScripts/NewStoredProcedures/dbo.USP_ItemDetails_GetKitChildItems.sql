GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_ItemDetails_GetKitChildItems' ) 
    DROP PROCEDURE USP_ItemDetails_GetKitChildItems
GO
CREATE PROCEDURE USP_ItemDetails_GetKitChildItems  
(  
	@numDomainID NUMERIC(18,0),  
	@numKitItemID NUMERIC(18,0),
	@numItemCode NUMERIC(18,0)
)  
AS  
BEGIN  
	-- SET NOCOUNT ON added to prevent extra result sets from  
	-- interfering with SELECT statements.  
	SET NOCOUNT ON;  

	SELECT 
		@numItemCode AS numKitItemCode,
		Item.numItemCode,
		ISNULL(Item.vcItemName,'') AS vcItemName,
		dbo.GetKitChildItemsDisplay(@numKitItemID,@numItemCode,Item.numItemCode) AS vcDisplayText,
		ISNULL(UOM.vcUnitName,'') AS UnitName,
		ISNULL(ItemDetails.numQtyItemsReq,0) as Qty,
		dbo.fn_GetUOMName(Item.numSaleUnit) AS vcSaleUOMName,
		ISNULL(dbo.fn_UOMConversion(Item.numSaleUnit,@numItemCode,@numDomainId,Item.numBaseUnit),1) fltUOMConversion,
		ISNULL((SELECT tintView FROM ItemDetails ID WHERE ID.numItemKitID=@numKitItemID AND ID.numChildItemID=@numItemCode),1) tintView
	FROM 
		ItemDetails
	LEFT JOIN 
		UOM
	ON 
		ItemDetails.numUOMId = UOM.numUOMId
	INNER JOIN
		Item
	ON
		ItemDetails.numChildItemID = Item.numItemCode
	WHERE
		ItemDetails.numItemKitID = @numItemCode 
	ORDER BY 
		sintOrder
END  
GO

