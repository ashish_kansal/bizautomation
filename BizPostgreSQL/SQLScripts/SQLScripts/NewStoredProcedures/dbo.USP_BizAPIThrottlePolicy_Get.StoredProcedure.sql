GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_BizAPIThrottlePolicy_Get' ) 
    DROP PROCEDURE USP_BizAPIThrottlePolicy_Get
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author: Sandeep Patel
-- Create date: 16 March 2014
-- Description:	Gets ThrottlePolicy of BIZAPIAccessKey or IPAddress
-- =============================================
CREATE PROCEDURE USP_BizAPIThrottlePolicy_Get
	@BizAPIPublicKey VARCHAR(100),
	@IpAddress VARCHAR(100)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	SELECT * FROM dbo.BizAPIThrottlePolicy WHERE RateLimitKey = @BizAPIPublicKey OR RateLimitKey = @IpAddress
END
GO