
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by Priya(10 Oct 2018)

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getbackorderpermission')
DROP PROCEDURE usp_getbackorderpermission
GO
CREATE PROCEDURE [dbo].[USP_GetBackOrderPermission]    
	@numDomainID AS NUMERIC(9)=0,
	@UserAuthGroupId AS NUMERIC(9),
	@numitemcode AS NUMERIC(9),
	@numQuantity AS FLOAT,
	@numWareHouseItemid AS NUMERIC(9),
	@kitChildItem AS VARCHAR(MAX)
AS
BEGIN
	DECLARE @UserGroupId AS NUMERIC(18,0)
	DECLARE @numBackOrder AS NUMERIC(18,0)

	SET @UserGroupId = (SELECT COUNT(*) FROM AuthenticationGroupBackOrder WHERE numGroupID = @UserAuthGroupId AND numDomainID = @numDomainID)

	IF @UserGroupId > 0
	BEGIN
		DECLARE @bitKitParent BIT
		SELECT @bitKitParent=ISNULL(bitKitParent,0) FROM Item WHERE numItemCode=@numitemcode

		IF @bitKitParent = 1
		BEGIN
			DECLARE @numWarehouseID NUMERIC(18,0)
			SELECT @numWarehouseID=numWareHouseID FROM WareHouseItems WHERE numWareHouseItemID=@numWareHouseItemid

			DECLARE @TEMPitems TABLE
			(
				numItemCode NUMERIC(18,0)
				,numWarehouseItemID NUMERIC(18,0)
				,numQtyItemsReq FLOAT
			)

			DECLARE @TempExistingItems TABLE
			(
				vcItem VARCHAR(100)
			)

			INSERT INTO @TempExistingItems
			(
				vcItem
			)
			SELECT 
				OutParam 
			FROM 
				SplitString(@kitChildItem,',')

			DECLARE @TEMPSelectedKitChilds TABLE
			(
				ChildKitItemID NUMERIC(18,0),
				ChildKitWarehouseItemID NUMERIC(18,0),
				ChildKitChildItemID NUMERIC(18,0),
				ChildKitChildWarehouseItemID NUMERIC(18,0),
				ChildQty FLOAT
			)

			INSERT INTO @TEMPSelectedKitChilds
			(
				ChildKitItemID
				,ChildKitChildItemID
				,ChildQty
			)
			SELECT 
				Reverse(ParseName(Replace(Reverse(vcItem), '-', '.'), 1))
				,Reverse(ParseName(Replace(Reverse(vcItem), '-', '.'), 2))
				,Reverse(ParseName(Replace(Reverse(vcItem), '-', '.'), 3))
			FROM  
			(
				SELECT 
					vcItem 
				FROM 
					@TempExistingItems
			) As [x]

			UPDATE 
				@TEMPSelectedKitChilds 
			SET 
				ChildKitWarehouseItemID=(SELECT TOP 1 numWareHouseItemID FROM WareHouseItems WHERE numItemID=ChildKitItemID AND numWareHouseID=@numWarehouseID)
				,ChildKitChildWarehouseItemID=(SELECT TOP 1 numWareHouseItemID FROM WareHouseItems WHERE numItemID=ChildKitChildItemID AND numWareHouseID=@numWarehouseID)

			IF (SELECT COUNT(*) FROM @TEMPSelectedKitChilds WHERE ISNULL(ChildKitItemID,0)=0) > 0
			BEGIN
				INSERT INTO @TEMPitems
				(
					numItemCode
					,numWarehouseItemID
					,numQtyItemsReq
				)
				SELECT
					I.numItemCode
					,T1.ChildKitChildWarehouseItemID
					,@numQuantity * ChildQty
				FROM
					@TEMPSelectedKitChilds T1
				INNER JOIN
					Item I
				ON
					T1.ChildKitChildItemID = I.numItemCOde
				WHERE
					ISNULL(ChildKitItemID,0)=0
					AND ISNULL(bitKitParent,0) = 0
			END
			ELSE
			BEGIN
				;WITH CTE (numItemCode, bitKitParent, numQtyItemsReq) AS
				(
					SELECT
						ID.numChildItemID,
						ISNULL(I.bitKitParent,0),
						CAST(@numQuantity * (ISNULL(ID.numQtyItemsReq,0) * ISNULL(dbo.fn_UOMConversion(ISNULL(ID.numUOMID, 0), I.numItemCode, @numDomainID, ISNULL(I.numBaseUnit, 0)),1)) AS FLOAT)
					FROM 
						[ItemDetails] ID
					INNER JOIN 
						[Item] I 
					ON 
						ID.[numChildItemID] = I.[numItemCode]
					WHERE   
						[numItemKitID] = @numItemCode
						AND 1 = (CASE 
									WHEN ISNULL(I.bitKitParent,0) = 1 AND LEN(ISNULL(@kitChildItem,'')) > 0 THEN 
										(CASE 
											WHEN numChildItemID IN (SELECT ChildKitItemID FROM @TEMPSelectedKitChilds) 
											THEN 1 
											ELSE 0 
										END) 
									ELSE (CASE 
											WHEN (SELECT COUNT(*) FROM @TEMPSelectedKitChilds WHERE ISNULL(ChildKitItemID,0)=0) > 0
											THEN	
												(CASE 
													WHEN numChildItemID IN (SELECT ChildKitChildItemID FROM @TEMPSelectedKitChilds WHERE ISNULL(ChildKitItemID,0)=0) 
													THEN 1 
													ELSE 0 
												END) 
											ELSE 1
										END)
								END)
					UNION ALL
					SELECT
						ID.numChildItemID,
						ISNULL(I.bitKitParent,0),
						CAST((ISNULL(Temp1.numQtyItemsReq,0) * ISNULL(ID.numQtyItemsReq,0) * ISNULL(dbo.fn_UOMConversion(ISNULL(ID.numUOMID, 0), I.numItemCode, @numDomainID, ISNULL(I.numBaseUnit, 0)),1)) AS FLOAT)
					FROM 
						CTE As Temp1
					INNER JOIN
						[ItemDetails] ID
					ON
						ID.numItemKitID = Temp1.numItemCode
					INNER JOIN 
						[Item] I 
					ON 
						ID.[numChildItemID] = I.[numItemCode]
					WHERE
						Temp1.bitKitParent = 1
						AND EXISTS (SELECT ChildKitItemID FROM @TEMPSelectedKitChilds T2 WHERE ISNULL(Temp1.bitKitParent,0) = 1 AND t2.ChildKitItemID=ID.numItemKitID AND ID.numChildItemID = T2.ChildKitChildItemID)
				)

				INSERT INTO @TEMPitems
				(
					numItemCode
					,numWarehouseItemID
					,numQtyItemsReq
				)
				SELECT
					numItemCode
					,(SELECT TOP 1 numWareHouseItemID FROM WareHouseItems WHERE numItemID=numItemCode AND numWareHouseID=@numWarehouseID)
					,numQtyItemsReq
				FROM
					CTE
				WHERE
					ISNULL(bitKitParent,0) = 0
			END

		
			IF EXISTS (SELECT 
							T1.numItemCode 
						FROM 
							@TEMPitems T1 
						INNER JOIN 
							WareHouseItems WI 
						ON 
							T1.numWarehouseItemID=WI.numWareHouseItemID 
							WHERE ISNULL(numOnHand,0) < numQtyItemsReq)
			BEGIN
				SELECT 0
			END
			ELSE
			BEGIN
				SELECT 1
			END
		END
		ELSE
		BEGIN
			SET @numBackOrder =	(SELECT 
									COUNT(*) 
								FROM 
									WareHouseItems						
								WHERE 
									numItemID = @numitemcode
									AND ISNULL(numOnHand,0) < @numQuantity
									AND numWareHouseItemID = @numWareHouseItemid)

			IF @numBackOrder > 0
				SELECT 0
			ELSE
				SELECT 1
		END
	END
	ELSE
	BEGIN
		SELECT 1
	END
END
GO
