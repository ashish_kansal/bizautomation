/****** Object:  StoredProcedure [dbo].[USP_GetTeamForUsrMst]    Script Date: 07/26/2008 16:18:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj  
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetUserHourlyRate')
DROP PROCEDURE USP_GetUserHourlyRate
GO
CREATE PROCEDURE [dbo].[USP_GetUserHourlyRate]    
@numUserCntID as numeric(9),    
@numDomainID as numeric(9)    
as    
select ISNULL(monHourlyRate,0) from UserMaster      
where numUserDetailId=@numUserCntID    
and UserMaster.numDomainID=@numDomainID
GO