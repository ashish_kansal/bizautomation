GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_CalculateTimeAndExpense')
DROP PROCEDURE USP_CalculateTimeAndExpense
GO
CREATE PROCEDURE [dbo].[USP_CalculateTimeAndExpense]
	 @numComPayPeriodID AS NUMERIC(18,0),
	 @numDomainID AS NUMERIC(18,0),
	 @ClientTimeZoneOffset INT
AS
BEGIN
	DECLARE @dtPayStart DATE
	DECLARE @dtPayEnd DATE

	SELECT 
		@dtPayStart=dtStart
		,@dtPayEnd=dtEnd 
	FROM 
		CommissionPayPeriod 
	WHERE 
		numComPayPeriodID = @numComPayPeriodID

	DELETE FROM
		TimeAndExpenseCommission
	WHERE
		numDomainID=@numDomainID
		AND ISNULL(bitCommissionPaid,0) = 0
		AND ((CAST(DATEADD(MINUTE,-@ClientTimeZoneOffset,dtFromDate) AS DATE) BETWEEN @dtPayStart AND @dtPayEnd) OR (CAST(DATEADD(MINUTE,-@ClientTimeZoneOffset,dtToDate) AS DATE) BETWEEN @dtPayStart AND @dtPayEnd))

	DELETE FROM
		StagePercentageDetailsTaskCommission
	WHERE
		numDomainID=@numDomainID
		AND ISNULL(bitCommissionPaid,0) = 0
		AND numTaskID IN (SELECT numTaskId FROM StagePercentageDetailsTask WHERE numDomainID=@numDomainID AND CAST(DATEADD(MINUTE,-@ClientTimeZoneOffset,DATEADD(ss,-1 *DATEDIFF(ss,GETUTCDATE(),GETDATE()),dtmCreatedOn)) AS DATE) BETWEEN @dtPayStart AND @dtPayEnd)

	DELETE 
		TEC
	FROM 
		TimeAndExpenseCommission TEC
	LEFT JOIN
		TimeAndExpense TE
	ON
		TEC.numTimeAndExpenseID = TE.numCategoryHDRID
	WHERE
		TEC.numDomainID=@numDomainID
		AND ISNULL(TEC.bitCommissionPaid,0) = 0
		AND TE.numCategoryHDRID IS NULL

	DELETE 
		SPDTC
	FROM 
		StagePercentageDetailsTaskCommission SPDTC
	LEFT JOIN
		StagePercentageDetailsTask SPDT
	ON
		SPDTC.numTaskID = SPDT.numTaskId
	WHERE
		SPDTC.numDomainID=@numDomainID
		AND ISNULL(SPDTC.bitCommissionPaid,0) = 0
		AND SPDT.numTaskId IS NULL

	INSERT INTO TimeAndExpenseCommission
	(
		numDomainID
		,numComPayPeriodID
		,numTimeAndExpenseID
		,numUserCntID
		,dtFromDate
		,dtToDate
		,numType
		,numCategory
		,bitReimburse
		,numHours
		,monHourlyRate
		,monTotalAmount
		,bitCommissionPaid
	)
	SELECT
		@numDomainID
		,@numComPayPeriodID
		,numCategoryHDRID
		,TimeAndExpense.numUserCntID
		,TimeAndExpense.dtFromDate
		,TimeAndExpense.dtToDate
		,TimeAndExpense.numType
		,TimeAndExpense.numCategory
		,ISNULL(TimeAndExpense.bitReimburse,0)
		,ISNULL(CAST(DATEDIFF(MINUTE,TimeAndExpense.dtfromdate,TimeAndExpense.dttodate) AS FLOAT)/CAST(60 AS FLOAT),0)
		,ISNULL(UserMaster.monHourlyRate,0)
		,(CASE WHEN ISNULL(TimeAndExpense.bitReimburse,0) = 1 AND TimeAndExpense.numCategory = 2 THEN ISNULL(monAmount,0) ELSE ISNULL(CAST(DATEDIFF(MINUTE,TimeAndExpense.dtfromdate,TimeAndExpense.dttodate) AS FLOAT)/CAST(60 AS FLOAT),0) * ISNULL(UserMaster.monHourlyRate,0) END)
		,0
	FROM
		TimeAndExpense
	INNER JOIN
		UserMaster
	ON
		TimeAndExpense.numUserCntID = UserMaster.numUserDetailId
	LEFT JOIN
		TimeAndExpenseCommission
	ON
		TimeAndExpense.numCategoryHDRID = TimeAndExpenseCommission.numTimeAndExpenseID
	WHERE
		TimeAndExpense.numDomainID=@numDomainID
		AND TimeAndExpenseCommission.ID IS NULL
		AND (TimeAndExpense.numType=1 OR TimeAndExpense.numType=2) 
		AND 1 = (CASE WHEN TimeAndExpense.numCategory=2 THEN (CASE WHEN ISNULL(TimeAndExpense.bitReimburse,0)=1 THEN 1 ELSE 0 END) WHEN TimeAndExpense.numCategory=1 THEN 1 ELSE 0 END)
		AND (
				(CAST(DATEADD(MINUTE,-@ClientTimeZoneOffset,TimeAndExpense.dtFromDate) AS DATE) BETWEEN @dtPayStart AND @dtPayEnd) 
				OR 
				(CAST(DATEADD(MINUTE,-@ClientTimeZoneOffset,TimeAndExpense.dtToDate) AS DATE) BETWEEN @dtPayStart AND @dtPayEnd)
			) 


	INSERT INTO StagePercentageDetailsTaskCommission
	(
		numDomainID
		,numComPayPeriodID
		,numTaskID
		,numUserCntID
		,numHours
		,monHourlyRate
		,monTotalAmount
		,bitCommissionPaid
		,monAmountPaid
	)
	SELECT 
		@numDomainID
		,@numComPayPeriodID
		,numTaskId
		,SPDT.numAssignTo
		,ISNULL(CAST(((ISNULL(numHours,0) * 60) + ISNULL(numMinutes,0)) AS FLOAT)/CAST(60 AS FLOAT),0) numHours
		,ISNULL(UserMaster.monHourlyRate,0)
		,ISNULL(CAST(((ISNULL(numHours,0) * 60) + ISNULL(numMinutes,0)) AS FLOAT)/CAST(60 AS FLOAT),0) * ISNULL(UserMaster.monHourlyRate,0)
		,0
		,0
	FROM 
		StagePercentageDetailsTask SPDT 
	INNER JOIN 
		StagePercentageDetails SPD 
	ON 
		SPDT.numStageDetailsId=SPD.numStageDetailsId
	INNER JOIN
		UserMaster
	ON
		SPDT.numAssignTo = UserMaster.numUserDetailId
	WHERE 
		ISNULL(bitTaskClosed,0)=1
		AND CAST(DATEADD(MINUTE,-@ClientTimeZoneOffset,DATEADD(ss,-1 *DATEDIFF(ss,GETUTCDATE(),GETDATE()),dtmCreatedOn)) AS DATE) BETWEEN @dtPayStart AND @dtPayEnd
END
GO