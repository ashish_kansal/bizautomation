GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_PaymentGateway_GetDefault')
DROP PROCEDURE USP_PaymentGateway_GetDefault
GO
CREATE PROCEDURE [dbo].[USP_PaymentGateway_GetDefault]  
	@numDomainID NUMERIC(18,0),
	@numSiteID NUMERIC(18,0)
AS
BEGIN
	IF ISNULL(@numSiteId,0) > 0 AND (SELECT ISNULL(intPaymentGateWay,0) FROM Sites WHERE numDomainId=@numDomainID AND numSiteID=@numSiteID) > 0
	BEGIN
		SELECT 
			ISNULL(S.intPaymentGateWay,0) intPaymentGateWay
		FROM 
			Sites S
		WHERE 
			S.numDomainID = @numDomainID
  			AND ISNULL(S.numSiteId,0)=@numSiteId
	END
	ELSE
	BEGIN
		SELECT 
			ISNULL(D.intPaymentGateWay,0) intPaymentGateWay
		FROM 
			Domain D
		WHERE 
			D.numDomainID = @numDomainID
	END
END
GO