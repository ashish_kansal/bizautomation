/****** Object:  StoredProcedure [dbo].[USP_GetWorkOrder]    Script Date: 07/26/2008 16:16:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
                    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_SitePages_GetByPageElementID')
DROP PROCEDURE USP_SitePages_GetByPageElementID
GO
CREATE PROCEDURE [dbo].[USP_SitePages_GetByPageElementID]                             
	@numDomainID NUMERIC (18,0),
	@numPageElementID NUMERIC (18,0),
	@numSiteID NUMERIC(18,0)                                                                     
AS                           
	DECLARE @vcPageElement VARCHAR(500) = ''
	SELECT @vcPageElement=ISNULL(vcTagName,'') FROM PageElementMaster WHERE numElementID =  @numPageElementID

	SELECT 
		* 
	FROM 
		SitePages 
	WHERE 
		numTemplateID IN (
							SELECT 
								numTemplateID 
							FROM 
								SiteTemplates 
							WHERE 
								numDomainID = @numDomainID AND
								numSiteID = @numSiteID AND 
								CONTAINS(txtTemplateHTML,@vcPageElement)
							)

GO