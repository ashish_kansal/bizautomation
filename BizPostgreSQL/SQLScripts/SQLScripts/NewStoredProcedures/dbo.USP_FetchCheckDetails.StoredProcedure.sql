GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_FetchCheckDetails')
DROP PROCEDURE USP_FetchCheckDetails
GO
CREATE PROCEDURE USP_FetchCheckDetails
    @numDomainID numeric(18, 0),
    @numCheckHeaderID numeric(18, 0)
AS
	SELECT CD.*,ISNULL(GJD.numTransactionId,0) AS numTransactionId,CH.numCheckHeaderID, 
	dbo.FormatedDateFromDate(CH.dtCheckDate, @numDomainID) dtCheckDate , dbo.fn_GetComapnyName(CH.numDivisionID) AS CheckTo
	FROM CheckHeader CH JOIN CheckDetails CD ON CH.numCheckHeaderID=CD.numCheckHeaderID
	LEFT OUTER JOIN General_Journal_Details GJD ON GJD.tintReferenceType=2 AND GJD.numReferenceID=CD.numCheckDetailID 
			WHERE CH.numDomainID=@numDomainID AND CH.numCheckHeaderID=@numCheckHeaderID 