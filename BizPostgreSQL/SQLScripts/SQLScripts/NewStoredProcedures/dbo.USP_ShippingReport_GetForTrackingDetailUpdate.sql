GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ShippingReport_GetForTrackingDetailUpdate')
DROP PROCEDURE USP_ShippingReport_GetForTrackingDetailUpdate
GO
CREATE PROCEDURE [dbo].[USP_ShippingReport_GetForTrackingDetailUpdate]
AS
BEGIN
	SELECT 
		ShippingReport.numShippingReportId
		,ShippingReport.numShippingCompany
		,ShippingBox.vcTrackingNumber
		,ISNULL((SELECT vcShipFieldValue FROM ShippingFieldValues WHERE numDomainID=ShippingReport.numDomainId AND numListItemID=91 AND intShipFieldID=9),'') FedexServerURL
		,ISNULL((SELECT vcShipFieldValue FROM ShippingFieldValues WHERE numDomainID=ShippingReport.numDomainId AND numListItemID=91 AND intShipFieldID=6),'') FedexAccountNumber
		,ISNULL((SELECT vcShipFieldValue FROM ShippingFieldValues WHERE numDomainID=ShippingReport.numDomainId AND numListItemID=91 AND intShipFieldID=7),'') FedexMeterNumber
		,ISNULL((SELECT vcShipFieldValue FROM ShippingFieldValues WHERE numDomainID=ShippingReport.numDomainId AND numListItemID=91 AND intShipFieldID=11),'') FedexDeveloperKey
		,ISNULL((SELECT vcShipFieldValue FROM ShippingFieldValues WHERE numDomainID=ShippingReport.numDomainId AND numListItemID=91 AND intShipFieldID=12),'') FedexPassword
		,ISNULL((SELECT vcShipFieldValue FROM ShippingFieldValues WHERE numDomainID=ShippingReport.numDomainId AND numListItemID=88 AND intShipFieldID=14),'') UPSServerURL
		,ISNULL((SELECT vcShipFieldValue FROM ShippingFieldValues WHERE numDomainID=ShippingReport.numDomainId AND numListItemID=88 AND intShipFieldID=1),'') UPSAccessKey
		,ISNULL((SELECT vcShipFieldValue FROM ShippingFieldValues WHERE numDomainID=ShippingReport.numDomainId AND numListItemID=88 AND intShipFieldID=2),'') UPSUserID
		,ISNULL((SELECT vcShipFieldValue FROM ShippingFieldValues WHERE numDomainID=ShippingReport.numDomainId AND numListItemID=88 AND intShipFieldID=3),'') UPSPassword
		,ISNULL((SELECT vcShipFieldValue FROM ShippingFieldValues WHERE numDomainID=ShippingReport.numDomainId AND numListItemID=90 AND intShipFieldID=17),'') USPSServerURL
		,ISNULL((SELECT vcShipFieldValue FROM ShippingFieldValues WHERE numDomainID=ShippingReport.numDomainId AND numListItemID=90 AND intShipFieldID=15),'') USPSUserID
		,ISNULL((SELECT vcShipFieldValue FROM ShippingFieldValues WHERE numDomainID=ShippingReport.numDomainId AND numListItemID=90 AND intShipFieldID=16),'') USPSPassword
	FROM 
		ShippingReport 
	INNER JOIN 
		OpportunityMaster 
	ON 
		ShippingReport.numOppID=OpportunityMaster.numOppId
		AND ISNULL(OpportunityMaster.tintshipped,0) = 0
	INNER JOIN 
		ShippingBox 
	ON 
		ShippingReport.numShippingReportId=ShippingBox.numShippingReportId 
		AND ISNULL(ShippingBox.bitIsMasterTrackingNo,0) = 1
	WHERE
		ShippingReport.numDomainId NOT IN (1,163,166,184,186)
		AND ISNULL(ShippingBox.vcTrackingNumber,'') <> ''
		AND ISNULL(ShippingReport.numShippingCompany,0) IN (88,90,91)
		AND ISNULL(ShippingReport.bitDelivered,0) = 0
END