GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetAccountTransactionInfo')
DROP PROCEDURE USP_GetAccountTransactionInfo
GO
CREATE PROCEDURE USP_GetAccountTransactionInfo
    @numDomainId NUMERIC(18,0),
    @tintReferenceType TINYINT, --1:Write Check(numCheckHeaderID)
    @numReferenceID numeric(18, 0)
AS
    Declare @numJournal_Id NUMERIC(18,0);SET @numJournal_Id=0
    Declare @numTransactionId NUMERIC(18,0);SET @numTransactionId=0
    Declare @bitReconcile BIT;SET @bitReconcile=0

IF @numReferenceID>0
BEGIN
	IF @tintReferenceType=1 --Write Check
	BEGIN
		SELECT @numJournal_Id=numJournal_Id FROM General_Journal_Header WHERE numDomainId=@numDomainId AND numCheckHeaderID=@numReferenceID	
	END
	ELSE IF @tintReferenceType=5 --Direct Journal
	BEGIN
		SELECT @numJournal_Id=numJournal_Id FROM General_Journal_Header WHERE numDomainId=@numDomainId AND numJournal_Id=@numReferenceID	
	END
	ELSE IF @tintReferenceType=3 --Add Bill
	BEGIN
		SELECT @numJournal_Id=numJournal_Id FROM General_Journal_Header WHERE numDomainId=@numDomainId AND numBillID=@numReferenceID	
	END
	ELSE IF @tintReferenceType=8 --Bill Payment Header
	BEGIN
		SELECT @numJournal_Id=numJournal_Id FROM General_Journal_Header WHERE numDomainId=@numDomainId AND numBillPaymentID=@numReferenceID	
	END
	ELSE IF @tintReferenceType=6 --Deposit Header
	BEGIN
		SELECT @numJournal_Id=numJournal_Id FROM General_Journal_Header WHERE numDomainId=@numDomainId AND numDepositId=@numReferenceID	
	END
	ELSE IF @tintReferenceType=12 --TimeAndExpense
	BEGIN
		SELECT @numJournal_Id=numJournal_Id FROM General_Journal_Header WHERE numDomainId=@numDomainId AND numCategoryHDRID=@numReferenceID	
	END
	ELSE IF @tintReferenceType=10 --RMA
	BEGIN
		SELECT @numJournal_Id=numJournal_Id FROM General_Journal_Header WHERE numDomainId=@numDomainId AND numReturnID=@numReferenceID	
	END
	
	IF @numJournal_Id>0
	BEGIN
		SELECT @numTransactionId=numTransactionId FROM General_Journal_Details WHERE numJournalId=@numJournal_Id AND numReferenceID=@numReferenceID AND tintReferenceType=@tintReferenceType	
	
		IF(SELECT COUNT(numTransactionId) FROM General_Journal_Details WHERE numJournalId=@numJournal_Id AND ISNULL(bitReconcile,0)=1)>0
		BEGIN
			SET @bitReconcile=1
		END
	END
END
	 
	SELECT @numJournal_Id AS numJournal_Id,@bitReconcile AS bitReconcile,@numTransactionId AS numTransactionId