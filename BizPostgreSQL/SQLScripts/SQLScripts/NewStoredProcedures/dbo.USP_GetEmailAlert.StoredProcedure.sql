
GO
/****** Object:  StoredProcedure [dbo].[USP_GetEmailAlert]    Script Date: 11/02/2011 12:40:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetEmailAlert')
DROP PROCEDURE USP_GetEmailAlert
GO
CREATE  PROCEDURE [dbo].[USP_GetEmailAlert]   
@numDomainId  NUMERIC, 
@numContactId  NUMERIC, 
@numUserContactId  NUMERIC 
As
--SET @numDomainId = 1
--SET @numContactId = 97540
--SET @numUserContactId = 96872

DECLARE @bitOpenActionItem BIT
DECLARE @vcOpenActionMsg VARCHAR(50)
DECLARE @bitOpencases BIT
DECLARE @vcOpenCasesMsg VARCHAR(50)
DECLARE @bitOpenProject BIT
DECLARE @vcOpenProjectMsg VARCHAR(50)
DECLARE @bitOpenSalesOpp BIT
DECLARE @vcOpenSalesOppMsg VARCHAR(50)
DECLARE @bitOpenPurchaseOpp BIT
DECLARE @vcOpenPurchaseOppMsg VARCHAR(50)
DECLARE @bitBalancedue BIT
DECLARE @vcBalancedueMsg VARCHAR(50)
DECLARE @monBalancedue DECIMAL(20,5) 
DECLARE @bitARAccount BIT
DECLARE @vcARAccountMsg VARCHAR(50)
DECLARE @bitAPAccount BIT
DECLARE @vcAPAccountMsg VARCHAR(50)
DECLARE @bitUnreadEMail BIT
DECLARE @intUnreadEmail INT
DECLARE @vcUnreadEmailMsg VARCHAR(50)
DECLARE @bitPurchasedPast BIT
DECLARE @monPurchasedPast DECIMAL(20,5) 
DECLARE @vcPurchasedPastMsg VARCHAR(50)
DECLARE @bitSoldPast BIT
DECLARE @monSoldPast DECIMAL(20,5) 
DECLARE @vcSoldPastMsg VARCHAR(50)
DECLARE @bitCustomField BIT
DECLARE @numCustomFieldId NUMERIC 
DECLARE @vcCustomFieldValue VARCHAR(50)
DECLARE @vcCustomfieldMsg VARCHAR(50)
DECLARE @numCount NUMERIC 
DECLARE @strMsg VARCHAR(50)
SET @numCount = 0
DECLARE @numDivisionID NUMERIC 
SELECT  @bitOpenActionItem = [bitOpenActionItem],
        @vcOpenActionMsg = [vcOpenActionMsg],
        @bitOpencases = [bitOpenCases],
        @vcOpenCasesMsg = [vcOpenCasesMsg],
        @bitOpenProject = [bitOpenProject],
        @vcOpenProjectMsg = [vcOpenProjectMsg],
        @bitOpenSalesOpp = [bitOpenSalesOpp],
        @vcOpenSalesOppMsg = [vcOpenSalesOppMsg],
        @bitOpenPurchaseOpp = [bitOpenPurchaseOpp],
        @vcOpenPurchaseOppMsg = [vcOpenPurchaseOppMsg],
        @bitBalancedue = [bitBalancedue],
        @monBalancedue = [monBalancedue],
        @vcBalancedueMsg = [vcBalancedueMsg],
        @bitARAccount = [bitARAccount],
        @vcARAccountMsg = [vcARAccountMsg],
        @bitAPAccount = [bitAPAccount],
        @vcAPAccountMsg = [vcAPAccountMsg],
        @bitUnreadEmail = [bitUnreadEmail],
        @intUnreadEmail = [intUnreadEmail],
        @vcUnreadEmailMsg = [vcUnreadEmailMsg],
        @bitPurchasedPast = [bitPurchasedPast],
        @monPurchasedPast = [monPurchasedPast],
        @vcPurchasedPastMsg = [vcPurchasedPastMsg],
        @bitSoldPast = [bitSoldPast],
        @monSoldPast = [monSoldPast],
        @vcSoldPastMsg = [vcSoldPastMsg],
        @bitCustomField = [bitCustomField],
        @numCustomFieldId = [numCustomFieldId],
        @vcCustomFieldValue = [vcCustomFieldValue],
        @vcCustomFieldMsg = [vcCustomFieldMsg]
FROM    AlertConfig
WHERE   AlertConfig.numDomainId = @numDomainId
        AND AlertConfig.numContactId = @numUserContactId
      
CREATE TABLE #temp
    (
      [bitOpenActionItem] [bit] NULL,
      [numOpenActionCount] [numeric](18,0) NULL,
      [bitOpenCases] [bit] NULL,
      [numOpenCasesCount] [numeric](18,0) NULL,
      [bitOpenProject] [bit] NULL,
      [numOpenProjectCount] [numeric](18,0) NULL,
      [bitOpenSalesOpp] [bit] NULL,
      [numOpenSalesOppCount] [numeric](15) NULL,
      [bitOpenPurchaseOpp] [bit] NULL,
      [numOpenPurchaseOppCount] [numeric](15) NULL,
      [bitBalancedue] [bit] NULL,
      [monBalancedue] DECIMAL(20,5) NULL,
      [monBalancedueCount]DECIMAL(20,5) NULL,
      [bitUnreadEmail] [bit] NULL,
      [intUnreadEmail] [int] NULL,
      [intUnreadEmailCount] [int] NULL,
      [bitPurchasedPast] [bit] NULL,
      [monPurchasedPast] DECIMAL(20,5) NULL,
      [monPurchasedPastCount] DECIMAL(20,5) NULL,
      [bitSoldPast] [bit] NULL,
      [monSoldPast] DECIMAL(20,5) NULL,
      [monSoldPastCount] DECIMAL(20,5) NULL,
      numDivisionID numeric(9) NULL
      )
      --TAKE divisionID
      INSERT INTO #temp VALUES(NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL)
SET @numDivisionID = ( SELECT   numDivisionId
                       FROM     dbo.AdditionalContactsInformation
                       WHERE    numContactId = @numContactId
                     )
PRINT 'DivisionId :' + CAST(@numDivisionID AS VARCHAR)
PRINT 'bitOpenActionItem :' + CAST(@bitOpenActionItem AS VARCHAR)
IF @bitOpenActionItem = 1
    AND ( SELECT    SUM(OpenActionItemCount)
          FROM      VIEW_Email_Alert_Config v
          WHERE     v.numDomainid = @numDomainId
                    AND v.numDivisionID = @numDivisionID and v.numContactId=@numContactId
        ) > 0 
    BEGIN 
        UPDATE #temp SET bitOpenActionItem = 1,numOpenActionCount = (SELECT SUM(OpenActionItemCount)
          FROM      VIEW_Email_Alert_Config v
          WHERE     v.numDomainid = @numDomainId
                    AND v.numDivisionID = @numDivisionID and v.numContactId=@numContactId)
    END 
    ELSE
    BEGIN
		UPDATE #temp SET bitOpenActionItem = 0,numOpenActionCount = 0
	END
IF @bitOpencases = 1
    AND ( SELECT    SUM(OpenCaseCount)
          FROM      VIEW_Email_Alert_Config v
          WHERE     v.numDomainid = @numDomainId
                    AND v.numDivisionID = @numDivisionID
        ) > 0 
    BEGIN 
      UPDATE #temp SET bitOpenCases = 1,numOpenCasesCount =(SELECT SUM(OpenCaseCount)
          FROM      VIEW_Email_Alert_Config v
          WHERE     v.numDomainid = @numDomainId
                    AND v.numDivisionID = @numDivisionID)
		
    END 
    ELSE
    BEGIN
		UPDATE #temp SET bitOpenCases = 0,numOpenCasesCount = 0
	END
IF @bitOpenProject = 1
    AND ( SELECT    SUM(OpenProjectCount)
          FROM      VIEW_Email_Alert_Config v
          WHERE     v.numDomainid = @numDomainId
                    AND v.numDivisionID = @numDivisionID
        ) > 0 
    BEGIN 
        UPDATE #temp SET bitOpenProject = 1,numOpenProjectCount = (SELECT SUM(OpenProjectCount)
          FROM      VIEW_Email_Alert_Config v
          WHERE     v.numDomainid = @numDomainId
                    AND v.numDivisionID = @numDivisionID)
    END 
    ELSE
    BEGIN
        UPDATE #temp SET bitOpenProject = 0,numOpenProjectCount = 0
    END 
IF @bitOpenSalesOpp = 1
    AND ( SELECT    SUM(OpenSalesOppCount)
          FROM      VIEW_Email_Alert_Config v
          WHERE     v.numDomainid = @numDomainId
                    AND v.numDivisionID = @numDivisionID
        ) > 0 
    BEGIN 
       UPDATE #temp SET bitOpenSalesOpp = 1,numOpenSalesOppCount = (SELECT SUM(OpenSalesOppCount)
          FROM      VIEW_Email_Alert_Config v
          WHERE     v.numDomainid = @numDomainId
                    AND v.numDivisionID = @numDivisionID)
    END 
    ELSE
    BEGIN
		UPDATE #temp SET bitOpenSalesOpp = 0,numOpenSalesOppCount = 0
	END
IF @bitOpenPurchaseOpp = 1
    AND ( SELECT    SUM(OpenPurchaseOppCount)
          FROM      VIEW_Email_Alert_Config v
          WHERE     v.numDomainid = @numDomainId
                    AND v.numDivisionID = @numDivisionID
        ) > 0 
    BEGIN 
          UPDATE #temp SET bitOpenPurchaseOpp = 1,numOpenPurchaseOppCount =(SELECT SUM(OpenPurchaseOppCount)
          FROM      VIEW_Email_Alert_Config v
          WHERE     v.numDomainid = @numDomainId
                    AND v.numDivisionID = @numDivisionID)
    END 
    ELSE
    BEGIN
		  UPDATE #temp SET bitOpenPurchaseOpp = 0,numOpenPurchaseOppCount = 0
	END
IF @bitBalancedue = 1
    AND ( SELECT    SUM(TotalBalanceDue)
          FROM      VIEW_Email_Alert_Config v
          WHERE     v.numDomainid = @numDomainId
                    AND v.numDivisionID = @numDivisionID
        ) > @monBalancedue 
    BEGIN 
          UPDATE #temp SET bitBalancedue = 1,monBalancedueCount = (SELECT SUM(TotalBalanceDue)
          FROM      VIEW_Email_Alert_Config v
          WHERE     v.numDomainid = @numDomainId
                    AND v.numDivisionID = @numDivisionID),monBalancedue=@monBalancedue
    END 
    ELSE
    BEGIN
		  UPDATE #temp SET bitBalancedue = 0,monBalancedueCount = 0,monBalancedue=@monBalancedue
	END
IF @bitUnreadEmail = 1
    AND ( SELECT    SUM(UnreadEmailCount)
          FROM      VIEW_Email_Alert_Config v
          WHERE     v.numDomainid = @numDomainId
                    AND v.numDivisionID = @numDivisionID
        ) >= @intUnreadEmail 
    BEGIN 
        UPDATE #temp SET bitUnreadEmail = 1,intUnreadEmailCount = (SELECT SUM(UnreadEmailCount)
          FROM      VIEW_Email_Alert_Config v
          WHERE     v.numDomainid = @numDomainId
                    AND v.numDivisionID = @numDivisionID),intUnreadEmail=@intUnreadEmail
    END 
    ELSE
    BEGIN
		UPDATE #temp SET bitUnreadEmail = 0,intUnreadEmailCount =0,intUnreadEmail=@intUnreadEmail
	END
IF @bitPurchasedPast = 1
    AND ( SELECT    SUM(PastPurchaseOrderValue)
          FROM      VIEW_Email_Alert_Config v
          WHERE     v.numDomainid = @numDomainId
                    AND v.numDivisionID = @numDivisionID
        ) >= @monPurchasedPast 
    BEGIN 
        UPDATE #temp SET bitPurchasedPast = 1,monPurchasedPastCount =(SELECT SUM(PastPurchaseOrderValue)
          FROM      VIEW_Email_Alert_Config v
          WHERE     v.numDomainid = @numDomainId
                    AND v.numDivisionID = @numDivisionID),monPurchasedPast=@monPurchasedPast
    END 
    ELSE
    BEGIN
		UPDATE #temp SET bitPurchasedPast = 0,monPurchasedPastCount =0,monPurchasedPast=@monPurchasedPast
	END
IF @bitSoldPast = 1
    AND ( SELECT    SUM(PastSalesOrderValue)
          FROM      VIEW_Email_Alert_Config v
          WHERE     v.numDomainid = @numDomainId
                    AND v.numDivisionID = @numDivisionID
        ) >= @monSoldPast 
    BEGIN 
         UPDATE #temp SET bitSoldPast = 1,monSoldPastCount =(SELECT SUM(PastSalesOrderValue)
          FROM      VIEW_Email_Alert_Config v
          WHERE     v.numDomainid = @numDomainId
                    AND v.numDivisionID = @numDivisionID),monSoldPast=@monSoldPast
    END 
    ELSE
    BEGIN
		 UPDATE #temp SET bitSoldPast = 0,monSoldPastCount =0,monSoldPast=@monSoldPast
	END

UPDATE #temp SET numDivisionID =@numDivisionID

SELECT * FROM #temp 
DROP TABLE #temp 
      