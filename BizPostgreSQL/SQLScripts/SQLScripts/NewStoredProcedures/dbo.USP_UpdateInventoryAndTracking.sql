GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_UpdateInventoryAndTracking')
DROP PROCEDURE USP_UpdateInventoryAndTracking
GO
CREATE PROCEDURE [dbo].[USP_UpdateInventoryAndTracking]
@numOnHand AS FLOAT,
@numOnAllocation AS FLOAT,
@numOnBackOrder AS FLOAT,
@numOnOrder AS FLOAT,
@numWarehouseItemID AS NUMERIC(18,0),
@numReferenceID AS NUMERIC(18,0),
@tintRefType AS TINYINT,
@numDomainID AS NUMERIC(18,0),
@numUserCntID AS NUMERIC(18,0),
@Description AS VARCHAR(1000)
AS
BEGIN
BEGIN TRY 
BEGIN TRAN
	--UPDATE WAREHOUSE INVENTORY
	UPDATE  
		WareHouseItems
	SET     
		numOnHand = @numOnHand,
		numAllocation = @numOnAllocation,
		numBackOrder = @numOnBackOrder,
		numOnOrder = @numOnOrder,
		dtModified = GETDATE() 
	WHERE   
		numWareHouseItemID = @numWarehouseItemID 
			
	--UPDATE WAREHOUSE TRACKING
	EXEC dbo.USP_ManageWareHouseItems_Tracking
		@numWareHouseItemID = @numWarehouseItemID,
		@numReferenceID = @numReferenceID,
		@tintRefType = @tintRefType,
		@vcDescription = @Description, 
		@numModifiedBy = @numUserCntID,
		@numDomainID = @numDomainID           
COMMIT TRAN 
END TRY 
BEGIN CATCH		
    IF ( @@TRANCOUNT > 0 ) 
    BEGIN
        ROLLBACK TRAN
    END

	DECLARE @ErrorMessage NVARCHAR(4000);
    DECLARE @ErrorSeverity INT;
    DECLARE @ErrorState INT;

    SELECT @ErrorMessage = ERROR_MESSAGE(), @ErrorSeverity = ERROR_SEVERITY(), @ErrorState = ERROR_STATE();

    RAISERROR (@ErrorMessage, @ErrorSeverity, @ErrorState);
END CATCH	
END
