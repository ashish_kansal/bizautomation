GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
                    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_Item_GetWareHousesForPick')
DROP PROCEDURE USP_Item_GetWareHousesForPick
GO
CREATE PROCEDURE [dbo].[USP_Item_GetWareHousesForPick]                             
	@numDomainID NUMERIC(18,0)
	,@numItemCode NUMERIC(18,0)
	,@numWarehouseID NUMERIC(18,0)
	,@numWarehouseItemID NUMERIC(18,0)
	,@vcWarehouseLocation VARCHAR(100)
	,@numPageIndex INT
	,@numPageSize INT
AS                            
BEGIN
	SELECT
		COUNT(*) OVER() AS numTotalRecords
		,WareHouseItems.numWareHouseItemID
		,Warehouses.vcWareHouse
		,ISNULL(WarehouseLocation.vcLocation,'') vcLocation
		,ISNULL(WareHouseItems.numOnHand,0) + ISNULL(WareHouseItems.numAllocation,0) numAvailable
	FROM
		WareHouseItems
	INNER JOIN
		Warehouses
	ON
		WareHouseItems.numWareHouseID = Warehouses.numWareHouseID
	LEFT JOIN
		WarehouseLocation
	ON
		WareHouseItems.numWLocationID = WarehouseLocation.numWLocationID
	WHERE
		WareHouseItems.numDomainID=@numDomainID
		AND WareHouseItems.numItemID = @numItemCode
		AND (ISNULL(@numWarehouseItemID,0) = 0 OR WareHouseItems.numWareHouseItemID <> @numWarehouseItemID)
		AND (ISNULL(@numWarehouseID,0) = 0 OR WareHouseItems.numWareHouseID=@numWarehouseID)
		AND (ISNULL(@vcWarehouseLocation,'') = '' OR WarehouseLocation.vcLocation LIKE CONCAT('%',@vcWarehouseLocation,'%'))
		AND ISNULL(WareHouseItems.numOnHand,0) + ISNULL(WareHouseItems.numAllocation,0) > 0
	ORDER BY
		ISNULL(WarehouseLocation.vcLocation,'') ASC
	OFFSET
		(@numPageIndex - 1) * @numPageSize ROWS
	FETCH NEXT @numPageSize ROWS ONLY
END
GO