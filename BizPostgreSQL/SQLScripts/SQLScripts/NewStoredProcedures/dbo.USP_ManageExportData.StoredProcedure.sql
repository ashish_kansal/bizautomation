GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ManageExportData')
DROP PROCEDURE USP_ManageExportData
GO
CREATE PROCEDURE USP_ManageExportData
@numDomainID NUMERIC(9),
@vcExportTables VARCHAR(100)
AS 
BEGIN
IF EXISTS( SELECT * FROM [ExportDataSettings] WHERE [numDomainID]=@numDomainID)
	BEGIN
		UPDATE [ExportDataSettings] SET [vcExportTables]=@vcExportTables , [dtLastExportedOn] = GETDATE()		
		WHERE [numDomainID]=@numDomainID
	END
	ELSE
	BEGIN
		INSERT INTO [ExportDataSettings] (
			[numDomainID],
			[vcExportTables],
			[dtLastExportedOn]
		) VALUES (  
			/* numDomainID - numeric(18, 0) */ @numDomainID,
			/* vcExportTables - varchar(100) */ @vcExportTables,
			/* dtLastExportedOn - datetime */ GETDATE()) 
	END
END