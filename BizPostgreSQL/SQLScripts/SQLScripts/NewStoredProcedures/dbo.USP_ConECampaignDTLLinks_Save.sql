GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ConECampaignDTLLinks_Save')
DROP PROCEDURE USP_ConECampaignDTLLinks_Save
GO
CREATE PROCEDURE [dbo].[USP_ConECampaignDTLLinks_Save]                            
 @numConECampDTLID NUMERIC(9 )= 0,
 @strBroadcastingLink AS TEXT = ''
AS                            
BEGIN                            
	DECLARE @hDoc4 INT                                                
    EXEC sp_xml_preparedocument @hDoc4 OUTPUT, @strBroadcastingLink

    INSERT INTO ConECampaignDTLLinks
		(
			numConECampaignDTLID,
			vcOriginalLink
		)
    SELECT  
			@numConECampDTLID,
            X.vcOriginalLink
    FROM    
		( 
			SELECT    
				*
            FROM      
				OPENXML (@hDoc4, '/LinkRecords/Table',2)
    WITH 
        ( 
            vcOriginalLink VARCHAR(2000)
        )
        ) X        

	SELECT * FROM ConECampaignDTLLinks WHERE numConECampaignDTLID = @numConECampDTLID
END
GO