
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
     
GO

IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getwarehouseidfor850')
DROP PROCEDURE usp_getwarehouseidfor850
GO
CREATE PROCEDURE [dbo].[USP_GetWarehouseIdFor850]        
@numDomainID as numeric(9)=0,        
@numWareHouseID as numeric(9)=0        
as
BEGIN         
		SELECT TOP 1 numWareHouseID

	   FROM [dbo].[Warehouses] W 
	   LEFT JOIN
			AddressDetails AD
		ON
			W.numAddressID = AD.numAddressID
  
	   WHERE 
			W.numDomainID=@numDomainID
			AND (numWareHouseID=@numWareHouseID  OR ISNULL(@numWareHouseID,0) = 0)
END
GO
