/****** Object:  StoredProcedure [dbo].[usp_GetRMASerializedindItems]    Script Date: 07/26/2008 16:18:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
                           
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetRMASerializedindItems')
DROP PROCEDURE USP_GetRMASerializedindItems
GO
CREATE PROCEDURE [dbo].[USP_GetRMASerializedindItems]                            
@numDomainId as numeric(9)=0,      
@numReturnHeaderID as numeric(9)=0,  
@numReturnItemID  as numeric(9)=0
                         
as      
BEGIN
	
DECLARE @tintReturnType TINYINT,@numOppId NUMERIC(18)
SELECT @tintReturnType=tintReturnType,@numOppId=ISNULL(numOppId,0) FROM ReturnHeader WHERE numDomainId=@numDomainId AND numReturnHeaderID=@numReturnHeaderID
	
declare @numWareHouseItemID as numeric(9),@numItemID as numeric(9),@numOppItemCode AS NUMERIC(9),@numUnitHour FLOAT
declare @bitSerialize as BIT,@bitLotNo as BIT                     

declare @str as varchar(2000),@strSQL as varchar(2000),@ColName as varchar(50)  
set @str=''        
      
select @numWareHouseItemID=OI.numWarehouseItmsID,@numItemID=WI.numItemID,@numOppItemCode=OI.numoppitemtCode,@numUnitHour=RI.numUnitHour from 
ReturnItems RI JOIN OpportunityItems OI ON RI.numOppItemID=OI.numoppitemtCode
join WareHouseItems WI on OI.numWarehouseItmsID= WI.numWareHouseItemID      
where  RI.numReturnHeaderID=@numReturnHeaderID AND RI.numReturnItemID=@numReturnItemID    
AND OI.numOppId=@numOppId    
                      
declare @numItemGroupID as numeric(9)                      
                      
select @numItemGroupID=numItemGroup,@bitSerialize=ISNULL(bitSerialized,0),@bitLotNo=ISNULL(bitLotNo,0) 
from Item where numItemCode=@numItemID                      

set @ColName='numWareHouseItemID,0'                  
            
--Create a Temporary table to hold data                                                          
Create table #tempTable ( ID INT IDENTITY PRIMARY KEY,                                                                    
 numCusFlDItemID numeric(9)                                                       
 )                       
                      
insert into #tempTable                       
(numCusFlDItemID)                                                          
select numOppAccAttrID from ItemGroupsDTL where numItemGroupID=@numItemGroupID and tintType=2     
              
 declare @ID as numeric(9)                      
 declare @numCusFlDItemID as varchar(20)                      
 declare @fld_label as varchar(100),@fld_type as varchar(100)                              
 set @ID=0                      
 select top 1 @ID=ID,@numCusFlDItemID=numCusFlDItemID,@fld_label=fld_label,@fld_type=fld_type from #tempTable                       
 join CFW_Fld_Master on numCusFlDItemID=Fld_ID                                   
 while @ID>0                      
 begin                                    
   set @str=@str+',  dbo.GetCustFldItems('+@numCusFlDItemID+',9,'+@ColName+') as ['+ @fld_label+']'                                      
    
   set @str=@str+',  dbo.GetCustFldItemsValue('+@numCusFlDItemID+',9,'+@ColName+','''+@fld_type+''') as ['+ @fld_label+'Value]'                                        
                    
   select top 1 @ID=ID,@numCusFlDItemID=numCusFlDItemID,@fld_label=fld_label from #tempTable                       
   join CFW_Fld_Master on numCusFlDItemID=Fld_ID and ID >@ID                      
   if @@rowcount=0 set @ID=0                      
                        
 end        
      
select numWareHouseItemID,vcWareHouse,
ISNULL(u.vcUnitName,'') vcBaseUOMName
,@bitSerialize AS bitSerialize,@bitLotNo AS bitLotNo,I.vcItemName,@numUnitHour AS numUnitHour
from OpportunityItems Opp     
join WareHouseItems on Opp.numWarehouseItmsID=WareHouseItems.numWareHouseItemID      
join Warehouses on Warehouses.numWareHouseID=WareHouseItems.numWareHouseID 
 join item I on Opp.numItemCode=I.numItemcode  
 LEFT JOIN  UOM u ON u.numUOMId = I.numBaseUnit       
where Opp.numOppItemTcode=@numOppItemCode    
      
      
      
 set @strSQL=CONCAT('select WID.numWareHouseItmsDTLID,numWareHouseItemID,vcSerialNo,0 as Op_Flag,
  isnull(OSI.numQty,0) 
  as TotalQty,0 as UsedQty
  ',@str,',0 as bitAdded
 from OppWarehouseSerializedItem OSI join WareHouseItmsDTL WID     
 on WID.numWareHouseItmsDTLID= OSI.numWarehouseItmsDTLID                           
 where numOppID=',@numOppID,' and numWareHouseItemID=',ISNULL(@numWareHouseItemID,0),' AND OSI.numWarehouseItmsDTLID NOT IN (SELECT OISInner.numWarehouseItmsDTLID FROM OppWarehouseSerializedItem OISInner INNER JOIN ReturnHeader RH ON OISInner.numReturnHeaderID=RH.numReturnHeaderID WHERE RH.numOppId=',@numOppID,')')
 print @strSQL                     
 exec (@strSQL)     
  
  
select Fld_label,fld_id,fld_type,numlistid,vcURL from #tempTable                       
 join CFW_Fld_Master on numCusFlDItemID=Fld_ID                    
drop table #tempTable   
  
END
GO
