GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ManageClass')
DROP PROCEDURE USP_ManageClass
GO
CREATE PROCEDURE USP_ManageClass 
@numDomainID NUMERIC,
@strItems TEXT
AS 
BEGIN
	
	 DECLARE  @hDocItem INT
      IF CONVERT(VARCHAR(10),@strItems) <> ''
        BEGIN
        DELETE FROM dbo.ClassDetails WHERE numDomainID=@numDomainID
        
          EXEC sp_xml_preparedocument
            @hDocItem OUTPUT ,
            @strItems
        
        INSERT INTO dbo.ClassDetails (
			numParentClassID,
			numChildClassID,
			numDomainID
		) 
          SELECT X.numParentClassID,
                 X.numChildClassID,
                 @numDomainID
          FROM   (SELECT *
                  FROM   OPENXML (@hDocItem, '/NewDataSet/Item', 2)
                            WITH (numParentClassID NUMERIC(9),
								  numChildClassID NUMERIC(9)
									)) X
          EXEC sp_xml_removedocument
            @hDocItem
        END
	
END