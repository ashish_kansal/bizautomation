/****** Object:  StoredProcedure [dbo].[USP_GetInvoiceList]    Script Date: 07/26/2008 16:17:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by prasanta Pradhan                  
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetCaseLisInEcommerce')
DROP PROCEDURE USP_GetCaseLisInEcommerce
GO
CREATE PROCEDURE [dbo].[USP_GetCaseLisInEcommerce]                            
 @numDivisionID as numeric(9)=0 ,
 @numDomainID AS NUMERIC(9),
 @CurrentPage INT,
 @PageSize INT,
 @UserId NUMERIC(18,0),
 @CaseStatus INT=0
as                        
 
 DECLARE @firstRec INT=0
 DECLARE @lastRec INT=0

	SET @firstRec = ( @CurrentPage - 1 ) * @PageSize
    SET @lastRec = ( @CurrentPage * @PageSize + 1 )
	SELECT  * FROM (
	SELECT ROW_NUMBER() OVER(ORDER BY numCaseId) AS Rownumber,* FROM (
	select 
		C.numCaseId,
		C.vcCaseNumber,
		[dbo].[FormatedDateFromDate](C.bintCreatedDate,C.numDomainID) AS bintCreatedDate,
		C.textSubject,
		(select TOP 1 vcUserName from UserMaster where numUserDetailId=C.numAssignedTo) AS AssignTo,
		(select TOP 1 vcData from ListDetails WHERE numListItemID=C.numStatus) AS [Status],
		C.textDesc 
	from 
		Cases AS C 
	WHERE 
		1=(CASE WHEN @CaseStatus=0 AND C.numStatus<>136 THEN 1 WHEN C.numStatus=@CaseStatus THEN 1 ELSE 0 END) AND
		C.numDivisionID=@numDivisionID AND 
		C.numContactId=@UserId
		) AS K) AS T
		WHERE T.Rownumber>@firstRec AND T.Rownumber<@lastRec

	select 
		COUNT(C.numCaseId)
	from 
		Cases AS C 
	WHERE 
		1=(CASE WHEN @CaseStatus=0 AND C.numStatus<>136 THEN 1 WHEN C.numStatus=@CaseStatus THEN 1 ELSE 0 END) AND
		C.numDivisionID=@numDivisionID AND 
		C.numContactId=@UserId




