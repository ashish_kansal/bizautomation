GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_OpportunityMasterShippingRateError_Save')
DROP PROCEDURE USP_OpportunityMasterShippingRateError_Save
GO
CREATE PROCEDURE [dbo].[USP_OpportunityMasterShippingRateError_Save]
(
	@numDomainID NUMERIC(18,0)
	,@numOppID NUMERIC(18,0)
	,@vcErrorMessage VARCHAR(MAX)
)
AS
BEGIN
	IF EXISTS (SELECT ID FROM OpportunityMasterShippingRateError WHERE numDomainID=@numDomainID AND numOppID=@numOppID)
	BEGIN
		UPDATE
			OpportunityMasterShippingRateError
		SET
			intNoOfTimeFailed = ISNULL(intNoOfTimeFailed,0) + 1
			,dtLastExecutedDate = GETUTCDATE()
		WHERE
			numDomainID=@numDomainID 
			AND numOppID=@numOppID
	END
	ELSE
	BEGIN
		INSERT INTO OpportunityMasterShippingRateError
		(
			numDomainID
			,numOppID
			,intNoOfTimeFailed
			,vcErrorMessage
			,dtLastExecutedDate
		)
		VALUES
		(
			@numDomainID
			,@numOppID
			,1
			,@vcErrorMessage
			,GETUTCDATE()
		)
	END

END
GO