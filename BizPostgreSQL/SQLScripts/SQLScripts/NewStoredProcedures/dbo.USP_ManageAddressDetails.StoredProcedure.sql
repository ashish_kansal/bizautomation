GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ManageAddressDetails')
DROP PROCEDURE USP_ManageAddressDetails
GO
CREATE PROCEDURE USP_ManageAddressDetails
@numAddressID NUMERIC(18,0)=0 output,
@vcAddressName  varchar(50) ,
@vcStreet  varchar(100) ,
@vcCity  varchar(50) ,
@vcPostalCode  varchar(15) ,
@numState  numeric(18, 0) ,
@numCountry  numeric(18, 0) ,
@bitIsPrimary  bit ,
@tintAddressOf  tinyint ,
@tintAddressType  tinyint ,
@numRecordID  numeric(18, 0),
@numDomainID  numeric(18, 0),
@bitResidential BIT,
@vcPhone VARCHAR(20) = '',
@bitFromEcommerce BIT = 0,
@numContact NUMERIC(18,0) = 0,
@bitAltContact BIT = 0,
@vcAltContact VARCHAR(200) = '',
@numUserCntID NUMERIC(18,0) = 0
AS 
BEGIN

	IF @tintAddressOf = 2 AND ISNULL(@numContact,0) = 0 AND ISNULL(@bitAltContact,0) = 0
	BEGIN
		SELECT TOP 1
			@numContact = numContactId
		FROM
			AdditionalContactsInformation
		WHERE
			numDomainID=@numDomainID
			AND numDivisionId = @numRecordID
			AND ISNULL(bitPrimaryContact,0) = 1
	END

IF ISNULL(@bitFromEcommerce,0) = 1
BEGIN
	UPDATE 
		DivisionMaster 
	SET 
		vcComPhone=CASE WHEN ISNULL(@vcPhone,'') <> '' THEN ISNULL(@vcPhone,'')  ELSE  vcComPhone END
	WHERE 
		numDomainID=@numDomainID AND numDivisionID=@numRecordID
END

IF @numAddressID = 0
BEGIN
	IF @bitIsPrimary=1
	BEGIN
		UPDATE dbo.AddressDetails SET bitIsPrimary=0 WHERE numDomainID=@numDomainID AND numRecordID=@numRecordID AND tintAddressOf=@tintAddressOf 
		AND tintAddressType = @tintAddressType
	END
	
	IF NOT EXISTS (SELECT 
						numAddressID
					FROM 
						AddressDetails 
					WHERE 
						numRecordID=@numRecordID 
						AND numDomainID=@numDomainID 
						AND tintAddressOf=@tintAddressOf 
						AND tintAddressType=@tintAddressType
						AND vcStreet=@vcStreet
						AND vcCity=@vcCity
						AND vcPostalCode=@vcPostalCode
						AND numState=@numState
						AND numCountry=@numCountry)
	BEGIN
			INSERT INTO dbo.AddressDetails 
			(
				vcAddressName,
				vcStreet,
				vcCity,
				vcPostalCode,
				numState,
				numCountry,
				bitIsPrimary,
				tintAddressOf,
				tintAddressType,
				numRecordID,
				numDomainID,
				bitResidential,
				numContact,
				bitAltContact,
				vcAltContact
			) 
			VALUES 
			( 
				@vcAddressName,
				@vcStreet,
				@vcCity,
				@vcPostalCode,
				@numState,
				@numCountry,
				@bitIsPrimary,
				@tintAddressOf,
				@tintAddressType,
				@numRecordID,
				@numDomainID,
				@bitResidential,
				@numContact,
				@bitAltContact,
				@vcAltContact
			) 

			SET @numAddressID = SCOPE_IDENTITY()	
	END
	ELSE
	BEGIN
		SELECT TOP 1
			@numAddressID = numAddressID
		FROM 
			AddressDetails 
		WHERE 
			numRecordID=@numRecordID 
			AND numDomainID=@numDomainID 
			AND tintAddressOf=@tintAddressOf 
			AND tintAddressType=@tintAddressType
			AND vcStreet=@vcStreet
			AND vcCity=@vcCity
			AND vcPostalCode=@vcPostalCode
			AND numState=@numState
			AND numCountry=@numCountry

		UPDATE
			AddressDetails
		SET
			bitIsPrimary=@bitIsPrimary
			,bitResidential=@bitResidential
			,vcAddressName=@vcAddressName
			,numContact=@numContact
			,bitAltContact=@bitAltContact
			,vcAltContact=@vcAltContact
		WHERE 
			numRecordID=@numRecordID 
			AND numDomainID=@numDomainID 
			AND tintAddressOf=@tintAddressOf 
			AND tintAddressType=@tintAddressType
			AND vcStreet=@vcStreet
			AND vcCity=@vcCity
			AND vcPostalCode=@vcPostalCode
			AND numState=@numState
			AND numCountry=@numCountry
	END



	
	
END
ELSE IF @numAddressID>0
BEGIN
	
	
	SELECT  @tintAddressOf=tintAddressOf,@tintAddressType=tintAddressType,@numRecordID=numRecordID FROM dbo.AddressDetails WHERE numAddressID=@numAddressID

	IF @bitIsPrimary=1
	BEGIN
		UPDATE dbo.AddressDetails SET bitIsPrimary=0 WHERE numDomainID=@numDomainID 
		AND numRecordID=@numRecordID AND tintAddressOf=@tintAddressOf AND tintAddressType=@tintAddressType
	END


	UPDATE dbo.AddressDetails
	SET 
		vcAddressName=@vcAddressName,
		vcStreet =@vcStreet,
		vcCity =@vcCity,
		vcPostalCode =@vcPostalCode,
		numState =@numState,
		numCountry =@numCountry,
		bitIsPrimary =@bitIsPrimary,
		bitResidential=@bitResidential
		,numContact=@numContact
		,bitAltContact=@bitAltContact
		,vcAltContact=@vcAltContact
	WHERE 
	numDomainID =@numDomainID
	AND numAddressID=@numAddressID

	If @tintAddressOf = 2 --Organization Address
	BEGIN
		IF ISNULL(@numUserCntID,0) > 0
		BEGIN
			IF NOT EXISTS (SELECT ID FROM OrgOppAddressModificationHistory WHERE numRecordID=@numRecordID AND tintAddressOf=1 AND tintAddressType=@tintAddressType AND numAddressID=@numAddressID)
			BEGIN
				INSERT INTO OrgOppAddressModificationHistory
				(
					tintAddressOf
					,tintAddressType
					,numRecordID
					,numAddressID
					,numModifiedBy
					,dtModifiedDate
				)
				VALUES
				(
					1
					,@tintAddressType
					,@numRecordID
					,@numAddressID
					,@numUserCntID
					,GETUTCDATE()
				)
			END
			ELSE
			BEGIN
				UPDATE
					OrgOppAddressModificationHistory
				SET
					numModifiedBy=@numUserCntID
					,dtModifiedDate=GETUTCDATE()
				WHERE
					numRecordID=@numRecordID
					AND numAddressID=@numAddressID
					AND tintAddressOf=1
					AND tintAddressType=@tintAddressType
			END
		END
	END

END


	
END