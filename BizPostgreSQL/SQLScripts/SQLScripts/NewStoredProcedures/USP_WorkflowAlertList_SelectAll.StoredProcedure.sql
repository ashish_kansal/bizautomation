GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_WorkflowAlertList_SelectAll')
DROP PROCEDURE USP_WorkflowAlertList_SelectAll
GO
  
 
-- =============================================  
-- Author:  <Author,,Sachin Sadhu>  
-- Create date: <Create Date,,1stApr2014>  
-- Description: <Description,,Get WorkFlow Alerts as per domain>  
-- =============================================  
CREATE PROCEDURE [dbo].[USP_WorkflowAlertList_SelectAll] 
 -- Add the parameters for the stored procedure here  
 @numDomainId numeric(18,0)  
AS  
BEGIN  
 -- SET NOCOUNT ON added to prevent extra result sets from  
 -- interfering with SELECT statements.  
 SET NOCOUNT ON;  
  
    -- Insert statements for procedure here  
 SELECT  Row_Number() over ( order by a.numWFAlertID) as Number, a.numWFAlertID, a.vcAlertMessage,a.numDomainId ,a.intAlertStatus,a.numWFID  FROM WorkflowAlertList AS a   WHERE a.numDomainId=@numDomainId AND a.intAlertStatus=1  
END  
  