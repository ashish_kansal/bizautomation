GO
/****** Object:  StoredProcedure [dbo].[USP_DELETE_MESSAGEMASTER]    Script Date: 18/07/2020 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO

IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_DELETE_MESSAGEMASTER' ) 
    DROP PROCEDURE USP_DELETE_MESSAGEMASTER
GO



SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE USP_DELETE_MESSAGEMASTER 
(
	@numMessageId AS	NUMERIC(18,0)=0,
	@numDomainID AS NUMERIC(18,0)=0
)	
AS 
BEGIN
IF(@numMessageId != 0)
	BEGIN
	DELETE FROM dbo.MessageMaster WHERE numMessageId = @numMessageId AND numDomainID=@numDomainID	
	END
END

	