GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_DeleteWarehouseItem')
DROP PROCEDURE USP_DeleteWarehouseItem
GO
CREATE PROCEDURE USP_DeleteWarehouseItem
  @numDomainID NUMERIC(18,0),
  @numUserCntID NUMERIC(18,0),
  @numWareHouseItmsDTLID NUMERIC(18,0),
  @numWareHouseItemID NUMERIC(18,0)
AS 
BEGIN
BEGIn TRY
BEGIN TRANSACTION
	DECLARE @numItemCode AS NUMERIC(18,0)
	SELECT @numItemCode=numItemID FROM WareHouseItems WHERE numDomainID=@numDomainID AND numWareHouseItemID=@numWareHouseItemID

	DECLARE @Total FLOAT

	SELECT 
		@Total = numQty - ISNULL((SELECT 
									SUM(w.numQty) 
								FROM 
									OppWarehouseSerializedItem w 
								INNER JOIN 
									OpportunityMaster opp 
								ON 
									w.numOppId=opp.numOppId 
								WHERE 
									ISNULL(opp.tintOppType,0) <> 2 
									AND 1 = (CASE WHEN ISNULL(opp.bitStockTransfer,0) = 1 THEN CASE WHEN ISNULL(w.bitTransferComplete,0) = 0 THEN 1 ELSE 0 END ELSE 1 END) 
									AND w.numWarehouseItmsDTLID=WareHouseItmsDTL.numWarehouseItmsDTLID)
							,0) 
	FROM 
		WareHouseItmsDTL 
    WHERE  
		numWareHouseItmsDTLID = @numWareHouseItmsDTLID AND numWareHouseItemID=@numWareHouseItemID
     
    
	DECLARE @numOnHand FLOAT
	DECLARE @numOnAllocation FLOAT
	DECLARE @numBackOrder FLOAT

	SELECT
		@numOnHand = ISNULL(numOnHand,0)
		,@numOnAllocation = ISNULL(numAllocation,0)
		,@numBackOrder = ISNULL(numBackOrder,0)
	FROM
		WareHouseItems
	WHERE
		numWareHouseItemID = @numWareHouseItemID


	IF @numOnHand >= @Total
	BEGIN
		UPDATE 
			WareHouseItems 
		SET 
			numOnHand = ISNULL(numOnHand,0) - @Total,
			dtModified = GETDATE()  
		WHERE 
			numWareHouseItemID = @numWareHouseItemID
	END
	ELSE 
	BEGIN
		UPDATE 
			WareHouseItems 
		SET 
			numOnHand = 0,
			numAllocation = (CASE WHEN @numOnAllocation > (@Total - @numOnHand) THEN @numOnAllocation - (@Total - @numOnHand) ELSE 0 END),
			numBackOrder = (CASE WHEN @numOnAllocation > (@Total - @numOnHand) THEN @numBackOrder + (@Total - @numOnHand) ELSE @numBackOrder + @numOnAllocation END),
			dtModified = GETDATE()  
		WHERE 
			numWareHouseItemID = @numWareHouseItemID
	END
	
	UPDATE WareHouseItmsDTL SET numQty = ISNULL(numQty,0) - @Total  WHERE  numWareHouseItmsDTLID = @numWareHouseItmsDTLID AND numWareHouseItemID=@numWareHouseItemID

	DECLARE @numOPPID NUMERIC(18,0)
	DECLARE @numOppItemID NUMERIC(18,0)
	SELECT @numOPPID=OM.numOppID,@numOppItemID=OWSI.numOppItemID FROM OppWarehouseSerializedItem OWSI INNER JOIN OpportunityMaster OM ON OWSI.numOppID=OM.numOppId AND tintOppType=2 WHERE numWarehouseItmsDTLID=@numWareHouseItmsDTLID

	DECLARE @numReferenceID NUMERIC(18,0)
	DECLARE @tintReference TINYINT
	DECLARE @description AS VARCHAR(100)
		
	SET @numReferenceID = (CASE WHEN ISNULL(@numOPPID,0) > 0 THEN @numOPPID ELSE @numItemCode END)
	SET @tintReference = (CASE WHEN ISNULL(@numOPPID,0) > 0 THEN 4 ELSE 1 END)
	SET @description = CONCAT('Serial/Lot# Deleted(Qty:',@Total,')')

	IF ISNULL(@numOPPID,0) > 0
	BEGIN
		UPDATE
			OI
		SET
			OI.numDeletedReceievedQty = ISNULL(OI.numDeletedReceievedQty,0) + @Total
		FROM
			OpportunityItems OI
		INNER JOIN
			OppWarehouseSerializedItem OWSI
		ON
			OI.numOppId = OWSI.numOppID
			AND OI.numoppitemtCode = OWSI.numOppItemID
		WHERE
			OWSI.numWarehouseItmsDTLID = @numWareHouseItmsDTLID


		IF EXISTS (SELECT ID FROM OpportunityItemsReceievedLocation WHERE numOppID=@numOPPID AND numOppItemID=@numOppItemID)
		BEGIN
			DECLARE @TEMP TABLE
			(
				ID INT IDENTITY(1,1),
				numOIRLID NUMERIC(18,0),
				numReceievedQty FLOAT,
				numDeletedReceievedQty FLOAT
			)

			INSERT INTO @TEMP
			(
				numOIRLID
				,numReceievedQty
				,numDeletedReceievedQty
			)
			SELECT
				ID,
				ISNULL(numUnitReceieved,0),
				ISNULL(numDeletedReceievedQty,0)
			FROM
				OpportunityItemsReceievedLocation
			WHERE
				numOppID=@numOPPID 
				AND numOppItemID=@numOppItemID
				AND numWarehouseItemID = @numWareHouseItemID
				AND (ISNULL(numUnitReceieved,0) - ISNULL(numDeletedReceievedQty,0)) > 0

			DECLARE @i AS INT = 1
			DECLARE @numOIRLID NUMERIC(18,0)
			DECLARE @numRemainingQty AS FLOAT
			DECLARE @Count AS INT

			SELECT @Count=COUNT(*) FROM @TEMP
				
			WHILE @i <= @Count AND @Total > 0
			BEGIN
				SELECT @numOIRLID=numOIRLID,@numRemainingQty=numReceievedQty-numDeletedReceievedQty FROM @TEMP WHERE ID=@i

				IF @numRemainingQty >= @Total
				BEGIN
					UPDATE OpportunityItemsReceievedLocation SET numDeletedReceievedQty = ISNULL(numDeletedReceievedQty,0) + @Total WHERE ID=@numOIRLID
					SET @Total = 0
				END	
				ELSE
				BEGIN
					UPDATE OpportunityItemsReceievedLocation SET numDeletedReceievedQty = ISNULL(numDeletedReceievedQty,0) + (@Total - @numRemainingQty)  WHERE ID=@numOIRLID
					SET @Total = @Total - @numRemainingQty
				END

				SET @i = @i + 1
			END

			IF @Total > 0
			BEGIN
				RAISERROR('DELETED_RECEIEVED_QTY_MISMATCH',16,1)
			END
		END
	END

		

	EXEC dbo.USP_ManageWareHouseItems_Tracking
				@numWareHouseItemID = @numWareHouseItemID, --  numeric(9, 0)
				@numReferenceID = @numReferenceID, --  numeric(9, 0)
				@tintRefType = @tintReference, --  tinyint
				@vcDescription = @description, --  varchar(100)
				@numModifiedBy = @numUserCntID,
				@numDomainID = @numDomainID
                     
    SELECT 1

	COMMIT
END TRY
BEGIN CATCH
	DECLARE @ErrorMessage NVARCHAR(4000)
	DECLARE @ErrorNumber INT
	DECLARE @ErrorSeverity INT
	DECLARE @ErrorState INT
	DECLARE @ErrorLine INT
	DECLARE @ErrorProcedure NVARCHAR(200)

	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;

	SELECT 
		@ErrorMessage = ERROR_MESSAGE(),
		@ErrorNumber = ERROR_NUMBER(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE(),
		@ErrorLine = ERROR_LINE(),
		@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

	RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);
END CATCH
END

