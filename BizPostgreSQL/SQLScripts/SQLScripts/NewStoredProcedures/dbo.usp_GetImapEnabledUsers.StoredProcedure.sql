/****** Object:  StoredProcedure [dbo].[usp_GetImapEnabledUsers]    Script Date: 06/04/2009 15:11:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_GetImapEnabledUsers')
DROP PROCEDURE usp_GetImapEnabledUsers
GO
CREATE PROCEDURE [dbo].[usp_GetImapEnabledUsers]
@tinyMode AS TINYINT=0,
@numDomainId AS NUMERIC(9)=0 
AS 
IF @tinyMode=0
	SELECT DISTINCT 
		I.[numDomainId]
		,I.[numUserCntId]
		,I.vcImapServerUrl
		,I.vcImapPassword
		,I.bitSSl
		,I.numPort
		,I.bitUseUserName
		,I.tintRetryLeft
		,I.numLastUID
		,I.bitNotified
		,I.vcGoogleRefreshToken
		,I.vcImapUserName
	FROM 
		[ImapUserDetails] I 
	INNER JOIN 
		Subscribers S 
	ON 
		S.numTargetDomainId=I.numDomainId 
	WHERE 
		[bitImap] = 1
		AND S.bitActive=1 
	UNION
	SELECT
		U.numDomainID
		,U.numUserDetailId AS numUserCntId
		,'' AS vcImapServerUrl
		,'' AS vcImapPassword
		,0 AS bitSSl
		,0 AS numPort
		,0 AS bitUseUserName
		,0 AS tintRetryLeft
		,0 AS numLastUID
		,0 AS bitNotified
		,'' AS vcGoogleRefreshToken
		,'' AS vcImapUserName
	FROM
		UserMaster U
	INNER JOIN 
		Subscribers S 
	ON 
		S.numTargetDomainId=U.numDomainId 
	WHERE
		(U.tintMailProvider=1 OR U.tintMailProvider=2) 
		AND ISNULL(vcMailAccessToken,'') <> '' 
		AND ISNULL(vcMailRefreshToken,'') <> ''
		AND ISNULL(bitOauthImap,0) = 1
		AND S.bitActive=1 
ELSE IF @tinyMode=1 --Get users to whom we need to send notification mail about error in last 10 attempt.
  SELECT DISTINCT I.[numDomainId],[numUserCntId],ISNULL(UM.vcEmailID,'') vcEmailID FROM [ImapUserDetails] I
	JOIN dbo.UserMaster UM ON UM.numUserDetailId = I.numUserCntId
	join Subscribers S on S.numTargetDomainId=I.numDomainId
  WHERE [bitImap] = 0 AND bitNotified=0 AND tintRetryLeft=0 and S.bitActive=1

ELSE IF @tinyMode=2  --Google to Biz Contact Distinct Domain 
  SELECT DISTINCT I.[numDomainId] FROM [ImapUserDetails] I JOIN Domain D ON I.numDomainId=D.numDomainId
	join Subscribers S on S.numTargetDomainId=I.numDomainId
   WHERE I.[bitImap] = 1 AND ISNULL(D.bitGtoBContact,0)=1 and S.bitActive=1

ELSE IF @tinyMode=3 --Google to Biz Calendar
   SELECT DISTINCT I.[numDomainId],I.[numUserCntId],I.* FROM [ImapUserDetails] I JOIN Domain D ON I.numDomainId=D.numDomainId
	join Subscribers S on S.numTargetDomainId=I.numDomainId
    join UserAccessedDTL UAD on UAD.numContactId=I.numUserCntId
   WHERE I.[bitImap] = 1 AND ISNULL(D.bitGtoBCalendar,0)=1 and S.bitActive=1 and len(isnull(vcGoogleRefreshToken,''))>0

ELSE IF @tinyMode=4 --Biz to Google Calendar
  SELECT DISTINCT I.[numDomainId],I.[numUserCntId],I.* FROM [ImapUserDetails] I JOIN Domain D ON I.numDomainId=D.numDomainId
	join Subscribers S on S.numTargetDomainId=I.numDomainId
    join UserAccessedDTL UAD on UAD.numContactId=I.numUserCntId
   WHERE I.[bitImap] = 1 AND ISNULL(D.bitBtoGCalendar,0)=1 and S.bitActive=1 and len(isnull(vcGoogleRefreshToken,''))>0

ELSE IF @tinyMode=5  --Google to Biz Contact Domain 
  SELECT DISTINCT I.[numDomainId],I.[numUserCntId],I.* FROM [ImapUserDetails] I JOIN Domain D ON I.numDomainId=D.numDomainId
	join Subscribers S on S.numTargetDomainId=I.numDomainId
   WHERE I.[bitImap] = 1 AND ISNULL(D.bitGtoBContact,0)=1 AND I.[numDomainId]=@numDomainId and S.bitActive=1

ELSE IF @tinyMode=6  --Biz to Google Email
  SELECT DISTINCT I.[numDomainId],I.[numUserCntId],I.* FROM [ImapUserDetails] I JOIN Domain D ON I.numDomainId=D.numDomainId
	join Subscribers S on S.numTargetDomainId=I.numDomainId
   WHERE I.[bitImap] = 1 AND ISNULL(D.bitGtoBContact,0)=1 AND I.[numDomainId]=@numDomainId and S.bitActive=1

  
  
  

 
  

  

