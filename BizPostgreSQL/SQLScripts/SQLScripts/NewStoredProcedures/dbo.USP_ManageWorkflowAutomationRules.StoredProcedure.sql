--- Created by Joseph        
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ManageWorkflowAutomationRules')
	DROP PROCEDURE USP_ManageWorkflowAutomationRules
GO

--exec USP_ManageWorkflowAutomationRules 
--@tintMode=1,@numRuleID=15,@numBizDocTypeId=291,@numBizDocStatus1=32042,@numBizDocStatus2=0,@tintOppType=1,
--@numOrderStatus=0,@numDomainID=1,@numUserCntID=1,@numEmailTemplate=598,@numBizDocCreatedFrom=1,@numOpenRecievePayment=1,@numCreditCardOption=0

CREATE PROCEDURE [dbo].[USP_ManageWorkflowAutomationRules]
	@tintMode TINYINT = 0,
	@numRuleID numeric(18, 0),
	@numBizDocTypeId numeric(18, 0) =0,
	@numBizDocStatus1 numeric(18, 0) =0,
	@numBizDocStatus2 numeric(18, 0) =0,
	@tintOppType numeric(18, 0),
	@numOrderStatus numeric(18, 0)=0,
	@numDomainID numeric(18, 0),
	@numUserCntID numeric(18, 0),
	@numEmailTemplate numeric(18, 0),
	@numBizDocCreatedFrom numeric(18, 0),
	@numOpenRecievePayment bit =0,
	@numCreditCardOption int,
	@numBizDocTemplate numeric(18, 0),
	@numOppSource numeric(18, 0),
	@bitUpdatedTrackingNo bit =0
AS

SET NOCOUNT ON

DECLARE @numAutomationID AS NUMERIC(18,0)
SET @numAutomationID = 0
 
IF EXISTS(SELECT [numAutomationID] FROM [dbo].[WorkflowAutomation] 
WHERE [numRuleID] = @numRuleID AND [numDomainID] = @numDomainID AND [tintOppType] = @tintOppType 
AND [numBizDocTypeId] = @numBizDocTypeId AND [numBizDocCreatedFrom] = @numBizDocCreatedFrom)
BEGIN

        SELECT @numAutomationID = [numAutomationID]  FROM [dbo].[WorkflowAutomation] 
		WHERE [numRuleID] = @numRuleID AND [numDomainID] = @numDomainID AND [tintOppType] = @tintOppType 
		AND [numBizDocTypeId] = @numBizDocTypeId AND [numBizDocCreatedFrom] = @numBizDocCreatedFrom	
SELECT @numAutomationID
	UPDATE [dbo].[WorkflowAutomation] SET
		[numRuleID] = @numRuleID,
		[numBizDocTypeId] = @numBizDocTypeId,
		[numBizDocStatus1] = @numBizDocStatus1,
		[numBizDocStatus2] = @numBizDocStatus2,
		[tintOppType] = @tintOppType,
		[numOrderStatus] = @numOrderStatus,
		[numDomainID] = @numDomainID,
		[numUserCntID] = @numUserCntID,
		[numEmailTemplate] = @numEmailTemplate,
		[numBizDocCreatedFrom] = @numBizDocCreatedFrom,
		[numOpenRecievePayment] = @numOpenRecievePayment,
		[numCreditCardOption] = @numCreditCardOption,
		[numBizDocTemplate] = @numBizDocTemplate,
		[numOppSource] = @numOppSource,
		[bitUpdatedTrackingNo] = @bitUpdatedTrackingNo,
		[dtModified] = GETDATE()
	WHERE
		[numAutomationID] = @numAutomationID
END
ELSE
BEGIN
	INSERT INTO [dbo].[WorkflowAutomation] (
		[numRuleID],
		[numBizDocTypeId],
		[numBizDocStatus1],
		[numBizDocStatus2],
		[tintOppType],
		[numOrderStatus],
		[numDomainID],
		[numUserCntID],
		[numEmailTemplate],
		[numBizDocCreatedFrom],
		[numOpenRecievePayment],
		[numCreditCardOption],
		[numBizDocTemplate],
		[numOppSource],
		[bitUpdatedTrackingNo],
		[dtCreated]
	) VALUES (
		@numRuleID,
		@numBizDocTypeId,
		@numBizDocStatus1,
		@numBizDocStatus2,
		@tintOppType,
		@numOrderStatus,
		@numDomainID,
		@numUserCntID,
		@numEmailTemplate,
		@numBizDocCreatedFrom,
		@numOpenRecievePayment,
		@numCreditCardOption,
		@numBizDocTemplate,
		@numOppSource,
		@bitUpdatedTrackingNo,
		GETDATE()
	)
END

