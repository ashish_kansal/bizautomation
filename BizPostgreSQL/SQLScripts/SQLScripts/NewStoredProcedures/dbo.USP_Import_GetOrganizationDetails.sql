SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_Import_GetOrganizationDetails')
DROP PROCEDURE USP_Import_GetOrganizationDetails
GO
CREATE PROCEDURE [dbo].[USP_Import_GetOrganizationDetails]  
	@numDomainID NUMERIC(18,0)
	,@numDivisionID NUMERIC(18,0)
AS  
BEGIN
	SELECT
		CI.vcCompanyName
		,CI.vcProfile
		,CI.numCompanyType
		,CI.numNoOfEmployeesId
		,CI.numAnnualRevID
		,CI.vcWebSite
		,CI.vcHow
		,CI.numCompanyIndustry
		,CI.numCompanyRating
		,CI.numCompanyCredit
		,CI.txtComments
		,CI.vcWebLink1
		,CI.vcWebLink2
		,CI.vcWebLink3
		,CI.vcWebLink4
		,DM.numCompanyDiff
		,DM.vcCompanyDiff
		,DM.numCampaignID
		,DM.vcComPhone
		,DM.numAssignedTo
		,DM.numFollowUpStatus
		,DM.numTerID
		,DM.numStatusID
		,DM.vcComFax
		,DM.bitPublicFlag
		,DM.bitActiveInActive
		,DM.numDivisionID
		,DM.numGrpID
		,DM.tintCRMType
		,DM.numCurrencyID
		,DM.bitNoTax
		,DM.numBillingDays
		,DM.bitOnCreditHold
		,DM.numDefaultExpenseAccountID
		,ISNULL((SELECT TOP 1 numNonBizCompanyID FROM ImportOrganizationContactReference WHERE numDomainID=@numDomainID AND numDivisionID=@numDivisionID),'') vcNonBizCompanyID
	FROM
		DivisionMaster DM
	INNER JOIN 
		CompanyInfo CI
	ON
		DM.numCompanyID=CI.numCompanyId
	WHERE
		DM.numDomainID=@numDomainID
		AND DM.numDivisionID=@numDivisionID
END