SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_GetCommissionReports')
DROP PROCEDURE usp_GetCommissionReports
GO
CREATE PROCEDURE [dbo].[usp_GetCommissionReports]
@numContactID AS NUMERIC,
@numDomainID AS NUMERIC,
@bitCommContact BIT
AS

DECLARE @tintComAppliesTo TINYINT
SELECT  @tintComAppliesTo = ISNULL(tintComAppliesTo,0) FROM domain WHERE numDomainID = @numDomainID

if @tintComAppliesTo=1    
begin   

 select CR.numComReports,CR.numContactID,I.vcItemName,CASE tintAssignTo WHEN 1 THEN 'Assign To' WHEN 2 THEN 'Record Owner' END ReportOwner
 ,CASE WHEN  @bitCommContact=1 THEN dbo.fn_GetComapnyName(CR.numContactID) ELSE '' END vcCompanyName
 from CommissionReports CR JOIN item I ON CR.numItemCode=I.numItemCode
 WHERE CR.numContactID=(CASE WHEN @bitCommContact=1 THEN CR.numContactID ELSE @numContactID END) AND CR.numDomainID=@numDomainID AND CR.bitCommContact=@bitCommContact
    
end    
ELSE if @tintComAppliesTo=2    
begin    

 select CR.numComReports,CR.numContactID,dbo.[GetListIemName]([numItemCode]) as vcItemName,CASE tintAssignTo WHEN 1 THEN 'Assign To' WHEN 2 THEN 'Record Owner' END ReportOwner
 ,CASE WHEN  @bitCommContact=1 THEN dbo.fn_GetComapnyName(CR.numContactID) ELSE '' END vcCompanyName
 from CommissionReports CR where CR.numContactID=(CASE WHEN @bitCommContact=1 THEN CR.numContactID ELSE @numContactID END)  AND CR.numDomainID=@numDomainID  AND CR.bitCommContact=@bitCommContact
 
end  
ELSE if @tintComAppliesTo=3    
begin    

 select CR.numComReports,CR.numContactID,'All Items' as vcItemName,CASE tintAssignTo WHEN 1 THEN 'Assign To' WHEN 2 THEN 'Record Owner' END ReportOwner
 ,CASE WHEN  @bitCommContact=1 THEN dbo.fn_GetComapnyName(CR.numContactID) ELSE '' END vcCompanyName
 from CommissionReports CR where CR.numContactID=(CASE WHEN @bitCommContact=1 THEN CR.numContactID ELSE @numContactID END)  AND CR.numDomainID=@numDomainID  AND CR.bitCommContact=@bitCommContact
 
end  

GO
