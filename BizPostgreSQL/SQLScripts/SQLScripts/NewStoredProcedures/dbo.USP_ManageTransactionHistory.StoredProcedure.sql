GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ManageTransactionHistory')
	DROP PROCEDURE USP_ManageTransactionHistory
GO

CREATE PROCEDURE [dbo].[USP_ManageTransactionHistory]
	@numTransHistoryID numeric(18, 0) OUTPUT,
	@numDomainID numeric(18, 0),
	@numDivisionID numeric(18, 0),
	@numContactID numeric(18, 0),
	@numOppID numeric(18, 0),
	@numOppBizDocsID numeric(18, 0),
	@vcTransactionID varchar(100),
	@tintTransactionStatus tinyint,
	@vcPGResponse varchar(200),
	@tintType tinyint,
	@monAuthorizedAmt DECIMAL(20,5),
	@monCapturedAmt DECIMAL(20,5),
	@monRefundAmt DECIMAL(20,5),
	@vcCardHolder varchar(500),
	@vcCreditCardNo varchar(500),
	@vcCVV2 varchar(200),
	@tintValidMonth tinyint,
	@intValidYear int,
	@vcSignatureFile VARCHAR(100),
	@numUserCntID numeric(18, 0),
	@numCardType NUMERIC(9,0),
	@vcResponseCode VARCHAR(500) = '',
	@intPaymentGateWay INT = 0
AS

 SET NOCOUNT ON
 SET TRANSACTION ISOLATION LEVEL READ COMMITTED


IF EXISTS(SELECT [numTransHistoryID] FROM [dbo].[TransactionHistory] WHERE [numTransHistoryID] = @numTransHistoryID)
BEGIN
	DECLARE @OldCapturedAmt AS DECIMAL(20,5)
	DECLARE @OldRefundAmt AS DECIMAL(20,5)
	
	SELECT 
		@OldCapturedAmt = ISNULL(monCapturedAmt,0)
		,@OldRefundAmt = ISNULL(monRefundAmt,0) 
	FROM 
		[dbo].[TransactionHistory] 
	WHERE 
		[numTransHistoryID] = @numTransHistoryID
	
	IF @tintTransactionStatus =2 -- captured
	BEGIN
		SET @OldCapturedAmt = @OldCapturedAmt + ISNULL(@monCapturedAmt,0)
		SET @OldRefundAmt = ISNULL(@monRefundAmt,0) + ISNULL(@OldRefundAmt,0)
	END
	ELSE IF @tintTransactionStatus =5 -- Credited
	BEGIN
		SET @OldRefundAmt = ISNULL(@monRefundAmt,0) + ISNULL(@OldRefundAmt,0)
	END
	
	UPDATE [dbo].[TransactionHistory] SET
		[tintTransactionStatus] = @tintTransactionStatus,
		[monCapturedAmt] = @OldCapturedAmt,
		[monRefundAmt] = @OldRefundAmt,
		[numModifiedBy] = @numUserCntID,
		[vcPGResponse] = (CASE WHEN @tintTransactionStatus = 2 OR @tintTransactionStatus = 4 THEN @vcPGResponse ELSE [vcPGResponse] END),
		[dtModifiedDate] = GETUTCDATE()
	WHERE
		[numTransHistoryID] = @numTransHistoryID AND numDomainID=@numDomainID
END
ELSE
BEGIN
	INSERT INTO [dbo].[TransactionHistory] (
		[numDomainID],
		[numDivisionID],
		[numContactID],
		[numOppID],
		[numOppBizDocsID],
		[vcTransactionID],
		[tintTransactionStatus],
		[vcPGResponse],
		[tintType],
		[monAuthorizedAmt],
		[monCapturedAmt],
		[monRefundAmt],
		[vcCardHolder],
		[vcCreditCardNo],
		[vcCVV2],
		[tintValidMonth],
		[intValidYear],
		vcSignatureFile,
		[numCreatedBy],
		[dtCreatedDate],
		[numModifiedBy],
		[dtModifiedDate],
		[numCardType],
		[vcResponseCode],
		intPaymentGateWay
	) VALUES (
		@numDomainID,
		@numDivisionID,
		@numContactID,
		@numOppID,
		@numOppBizDocsID,
		@vcTransactionID,
		@tintTransactionStatus,
		@vcPGResponse,
		@tintType,
		@monAuthorizedAmt,
		@monCapturedAmt,
		@monRefundAmt,
		@vcCardHolder,
		@vcCreditCardNo,
		@vcCVV2,
		@tintValidMonth,
		@intValidYear,
		@vcSignatureFile,
		@numUserCntID,
		GETUTCDATE(),
		@numUserCntID,
		GETUTCDATE(),
		@numCardType,
		@vcResponseCode,
		ISNULL(@intPaymentGateWay,0)
	)
	
	SET @numTransHistoryID = SCOPE_IDENTITY()
END
