GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_DeleteFromCampaign')
DROP PROCEDURE USP_DeleteFromCampaign
GO
/****** Object:  StoredProcedure [dbo].[USP_DeleteFromCampaign]    Script Date: 02/18/2009 15:17:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create PROCEDURE [dbo].[USP_DeleteFromCampaign]
@numContactID NUMERIC(9),
@numECampaignID NUMERIC(9)
AS
  BEGIN
    DECLARE  @numConEmailCampID NUMERIC(9)
    
    SELECT @numConEmailCampID = [numConEmailCampID]
    FROM   [ConECampaign]
    WHERE  [numContactID] = @numContactID
           AND [numECampaignID] = @numECampaignID
        
	UPDATE [ConECampaign] SET [bitEngaged] = 0 WHERE [numConEmailCampID]=@numConEmailCampID
	UPDATE AdditionalContactsInformation SET numECampaignID = NULL WHERE numContactId = @numContactID
---- Delete child record                 
--    DELETE FROM [ConECampaignDTL]
--    WHERE       [numConECampID] = @numConEmailCampID
---- Delete parent Record           
--    DELETE FROM [ConECampaign]
--    WHERE       [numContactID] = @numContactID
--                AND [numECampaignID] = @numECampaignID
                

  END
