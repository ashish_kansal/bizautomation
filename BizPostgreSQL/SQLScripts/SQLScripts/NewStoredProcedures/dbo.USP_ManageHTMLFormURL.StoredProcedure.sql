SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ManageHTMLFormURL')
DROP PROCEDURE USP_ManageHTMLFormURL
GO
CREATE PROCEDURE [dbo].[USP_ManageHTMLFormURL]
    @numFormId AS NUMERIC(9) = 0,
    @numDomainId AS NUMERIC(9) = 0,
    @numGroupID AS NUMERIC(9) = 0,
    @vcURL AS VARCHAR(200)='',
    @numURLId AS NUMERIC(9)=0,
    @tintType AS TINYINT,
    @vcSuccessURL AS VARCHAR(200)='',
    @vcFailURL AS VARCHAR(200)='',
	@numSubFormId AS NUMERIC(18)=0
AS 

IF @tintType=1 --Insert/Edit
BEGIN
    IF @numURLId>0
    BEGIN
    	Update HTMLFormURL SET vcURL=@vcURL,vcSuccessURL=@vcSuccessURL,vcFailURL=@vcFailURL WHERE numURLId=@numURLId
    	SELECT @numURLId
	END
	ELSE
	BEGIN
    	INSERT INTO HTMLFormURL (numFormId,numDomainId,numGroupID,vcURL,vcSuccessURL,vcFailURL,numSubFormId) VALUES (@numFormId,@numDomainId,@numGroupID,@vcURL,@vcSuccessURL,@vcFailURL,@numSubFormId)
	    SELECT SCOPE_IDENTITY()
	END
END

ELSE IF @tintType=2 --Select
BEGIN
	SELECT * FROM HTMLFormURL WHERE numFormId=@numFormId AND ISNULL(numSubFormId,0)=ISNULL(@numSubFormId,0) AND numDomainId=@numDomainId AND numGroupID=@numGroupID
END

ELSE IF @tintType=3 --Delete
BEGIN
	DELETE FROM HTMLFormURL WHERE numURLId=@numURLId
    SELECT @numURLId
END

ELSE IF @tintType=4 --Select Single Record
BEGIN
	SELECT * FROM HTMLFormURL WHERE numURLId=@numURLId
END  