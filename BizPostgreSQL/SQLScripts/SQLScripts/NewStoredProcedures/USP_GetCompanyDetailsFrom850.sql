SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetCompanyDetailsFrom850')
DROP PROCEDURE dbo.USP_GetCompanyDetailsFrom850
GO

CREATE PROCEDURE [dbo].[USP_GetCompanyDetailsFrom850]
	@numDomainID NUMERIC(18,0)
	,@numDivisionID NUMERIC(18,0)
	,@vcFirstName VARCHAR(300)
	,@vcLastName VARCHAR(300)
	,@vcEmail VARCHAR(300)
	,@vcMarketplace VARCHAR(300)
AS 
BEGIN
	--1: SKU
	--2: UPC
	--3: ItemName
	--4: BizItemID
	--5: ASIN
	--6: CustomerPart
	--7: VendorPart

	DECLARE @numMarketplaceID NUMERIC(18,0) = 0

	IF EXISTS (SELECT ID FROM eChannelHub WHERE vcMarketplace=ISNULL(@vcMarketplace,''))
	BEGIN
		SELECT 
			@numMarketplaceID=ID
		FROM 
			eChannelHub 
		WHERE 
			vcMarketplace=ISNULL(@vcMarketplace,'')
	END

	DECLARE @bitUsePredefinedCustomer BIT

	SELECT @bitUsePredefinedCustomer = ISNULL(bitUsePredefinedCustomer,0) FROM Domain WHERE numDomainId=@numDomainID

	IF @bitUsePredefinedCustomer = 1
	BEGIN
		SET @numDivisionID = ISNULL((SELECT numDivisionID FROM DomainMarketplace WHERE numDomainID = @numDomainID AND numMarketplaceID = @numMarketplaceID),0)
	END
	ELSE 
	BEGIN
		IF ISNULL(@vcEmail,'') = ''
		BEGIN
			SET @numDivisionID = 0
		END
		ELSE
		BEGIN
			SET @numDivisionID = ISNULL((SELECT numDivisionId FROM AdditionalContactsInformation WHERE numDomainID=@numDomainID AND vcEmail=ISNULL(@vcEmail,'')),0)
		END
	END

	SELECT 
		TEMP.numContactId AS numContactID
		,TEMP.numAssignedTo AS numAssignedTo
		,TEMP.numDivisionID
		,ISNULL(DMEmployer.tintInbound850PickItem,0) AS tintInbound850PickItem
		,TEMP.numCompanyId
		,TEMP.vcCompanyName
		,@numMarketplaceID numMarketplaceID
		,ISNULL(bitUsePredefinedCustomer,0) bitUsePredefinedCustomer
		,ISNULL(bitReceiveOrderWithNonMappedItem,0) bitReceiveOrderWithNonMappedItem
		,ISNULL(numItemToUseForNonMappedItem,0) numItemToUseForNonMappedItem
	FROM
		Domain
	INNER JOIN
		DivisionMaster DMEmployer
	ON
		Domain.numDivisionID = DMEmployer.numDivisionID
	OUTER APPLY
	(
		SELECT TOP 1
			DM.numDivisionID
			,CI.numCompanyId
			,CI.vcCompanyName
			,DM.numAssignedTo
			,ACI.numContactId
		FROM
			DivisionMaster DM
		INNER JOIN 
			CompanyInfo CI
		ON 
			DM.numCompanyID = CI.numCompanyId
		INNER JOIN
			AdditionalContactsInformation ACI 
		ON
			DM.numDivisionID = ACI.numDivisionId
		
		WHERE
			DM.numDomainID = @numDomainID
			AND DM.numDivisionID = @numDivisionId
			AND ISNULL(ACI.bitPrimaryContact,0)=1
	) TEMP
	WHERE
		Domain.numDomainID = @numDomainID
END
GO


