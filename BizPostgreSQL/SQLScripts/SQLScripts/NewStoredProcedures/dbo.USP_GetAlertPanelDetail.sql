SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetAlertPanelDetail')
DROP PROCEDURE dbo.USP_GetAlertPanelDetail
GO
CREATE PROCEDURE [dbo].[USP_GetAlertPanelDetail]
    (
	  @numAlertType INTEGER,
      @numDivisionID NUMERIC(18,0),
      @numContactID NUMERIC(18,0),
	  @numDomainID NUMERIC(18,0)
    )
AS 
BEGIN
	-- A/R BALANCE
	IF @numAlertType = 1
	BEGIN
		DECLARE  @AuthoritativePurchaseBizDocId  AS INTEGER
		SELECT  @AuthoritativePurchaseBizDocId= isnull(numAuthoritativePurchase,0) FROM AuthoritativeBizDocs WHERE  numDomainId =  @numDomainId

		DECLARE  @AuthoritativeSalesBizDocId  AS INTEGER
		SELECT @AuthoritativeSalesBizDocId = isnull(numAuthoritativeSales,0) FROM AuthoritativeBizDocs WHERE  numDomainId = @numDomainId

		SELECT
			TOP 10 
			vcPOppName AS [Order],
			vcBizDocID AS [Authoritive BizDoc],
			TotalAmount AS [Total Amount],
			AmountPaid AS [Amount Paid],
			BalanceDue AS [Balance Due],
			[dbo].[FormatedDateFromDate](DueDate,1) AS [Due Date]
		FROM
		(
		SELECT DISTINCT 
			OM.[numOppId], 
			OM.[vcPOppName],
			OB.[numOppBizDocsId],
			OB.[vcBizDocID],
			isnull(OB.monDealAmount,0) TotalAmount,--OM.[monPAmount]
			(OB.[monAmountPaid]) AmountPaid,
			isnull(OB.monDealAmount - OB.[monAmountPaid],0) BalanceDue,
			CASE ISNULL(bitBillingTerms,0)        
				WHEN 1 THEN DATEADD(dd,convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(intBillingDays,0)), 0)),OB.dtFromDate)
				WHEN 0 THEN ob.[dtFromDate]
			END AS DueDate       
		FROM   
			[OpportunityMaster] OM 
		INNER JOIN [DivisionMaster] DM ON OM.[numDivisionId] = DM.[numDivisionID]
		LEFT OUTER JOIN OpportunityBizDocs OB ON OB.numOppId = OM.numOppID
		LEFT OUTER JOIN [Currency] C ON OM.[numCurrencyID] = C.[numCurrencyID]
		WHERE  
			OM.[tintOppType] = 1
			AND OM.[tintOppStatus] = 1
			AND om.[numDomainId] = @numDomainID
			AND OB.[numBizDocId] = ISNULL(@AuthoritativeSalesBizDocId,0)
			AND OB.bitAuthoritativeBizDocs=1
			AND ISNULL(OB.tintDeferred,0) <> 1
			AND DM.[numDivisionID] = @numDivisionID
			AND isnull(OB.monDealAmount,0) > 0  AND isnull(OB.monDealAmount * OM.fltExchangeRate - OB.[monAmountPaid] * OM.fltExchangeRate,0) != 0    	
		UNION 
		SELECT 
				-1 AS [numOppId],
				'Journal' [vcPOppName],
				0 [numOppBizDocsId],
				'' [vcBizDocID],
				CASE WHEN GJD.numDebitAmt > 0 THEN GJD.numDebitAmt ELSE -1*GJD.numCreditAmt END TotalAmount,
				0 AmountPaid,
				0 BalanceDue,
				GJH.datEntry_Date AS DueDate
		FROM   
			dbo.General_Journal_Header GJH
		INNER JOIN dbo.General_Journal_Details GJD ON GJH.numJournal_Id = GJD.numJournalId														  AND GJH.numDomainId = GJD.numDomainId
		INNER JOIN dbo.Chart_Of_Accounts COA ON COA.numAccountId = GJD.numChartAcntId
		LEFT OUTER JOIN [Currency] C ON GJD.[numCurrencyID] = C.[numCurrencyID]
		WHERE  
			GJH.numDomainId = @numDomainID
			AND GJD.numCustomerID =@numDivisionID
			AND COA.vcAccountCode LIKE '01010105%' 
			AND ISNULL(numOppId,0)=0 AND ISNULL(numOppBizDocsId,0)=0 
			AND ISNULL(GJH.numDepositID,0)=0 AND ISNULL(GJH.numReturnID,0)=0 
		UNION 
			SELECT 
				-1 AS [numOppId],
				CASE WHEN tintDepositePage=2 THEN 'Unapplied Payment' ELSE 'Credit Memo' END AS [vcPOppName],
				0 [numOppBizDocsId],
				'' [vcBizDocID],
				(ISNULL(monDepositAmount,0) - ISNULL(monAppliedAmount,0)) * -1 AS TotalAmount,
				0 AmountPaid,
				(ISNULL(monDepositAmount,0) - ISNULL(monAppliedAmount,0)) * -1 AS BalanceDue,
				DM.dtDepositDate AS DueDate
		FROM  
			DepositMaster DM 
		WHERE 
			DM.numDomainId=@numDomainID
			AND tintDepositePage IN(2,3)  
			AND DM.numDivisionId = @numDivisionID
			AND ((ISNULL(monDepositAmount,0) - ISNULL(monAppliedAmount,0)) > 0)
		UNION
		SELECT  
			Case When isnull(BH.bitLandedCost,0)=1 then  isnull(BH.numOppId,0) else 0 end AS numOppId,
			'Bill' vcPOppName,	   
			0 numOppBizDocsId,
			CASE WHEN len(BH.vcReference)=0 THEN '' ELSE BH.vcReference END AS [vcBizDocID],
			BH.monAmountDue as TotalAmount,
			ISNULL(BH.monAmtPaid, 0) as AmountPaid,
			ISNULL(BH.monAmountDue, 0) - ISNULL(BH.monAmtPaid, 0) as BalanceDue,
			BH.dtDueDate AS DueDate
		FROM    
			BillHeader BH 
		WHERE   
			BH.numDomainID = @numDomainID
			AND BH.numDivisionId = @numDivisionID
			AND ISNULL(BH.monAmountDue, 0) - ISNULL(BH.monAmtPaid, 0) > 0 
		UNION
		SELECT  
			0 AS numOppId,
			'Check' vcPOppName,
			0 numOppBizDocsId,
			'' AS [vcBizDocID],
			ISNULL(CD.monAmount, 0) as TotalAmount,
			ISNULL(CD.monAmount, 0) as AmountPaid,
			0 as BalanceDue,
			CH.dtCheckDate AS DueDate
		FROM   
			CheckHeader CH
			JOIN  dbo.CheckDetails CD ON CH.numCheckHeaderID=CD.numCheckHeaderID
			INNER JOIN dbo.Chart_Of_Accounts COA ON COA.numAccountId = CD.numChartAcntId
		WHERE 
			CH.numDomainID = @numDomainID 
			AND CH.tintReferenceType=1
			AND COA.vcAccountCode LIKE '01020102%'
			AND CD.numCustomerId = @numDivisionID
		UNION 
		SELECT 
			OM.[numOppId],
			OM.[vcPOppName],
			OB.[numOppBizDocsId],
			OB.[vcBizDocID],
			isnull(OB.monDealAmount * OM.fltExchangeRate,0) TotalAmount,--OM.[monPAmount]
			(OB.[monAmountPaid] * OM.fltExchangeRate) AmountPaid,
			isnull(OB.monDealAmount * OM.fltExchangeRate - OB.[monAmountPaid] * OM.fltExchangeRate,0)  BalanceDue,
			CASE ISNULL(OM.bitBillingTerms,0)  
				WHEN 1 THEN DATEADD(dd,convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(OM.intBillingDays,0)), 0)),OB.dtFromDate)
				WHEN 0 THEN ob.[dtFromDate]
			END AS DueDate
		FROM   
			[OpportunityMaster] OM
		INNER JOIN [DivisionMaster] DM ON OM.[numDivisionId] = DM.[numDivisionID]
		LEFT OUTER JOIN OpportunityBizDocs OB ON OB.numOppId = OM.numOppID
		LEFT OUTER JOIN [Currency] C ON OM.[numCurrencyID] = C.[numCurrencyID]
		WHERE  
			OM.[tintOppType] = 2
			AND OM.[tintOppStatus] = 1
			AND om.[numDomainId] = @numDomainID
			AND OB.[numBizDocId] = ISNULL(@AuthoritativePurchaseBizDocId,0) 
			AND DM.[numDivisionID] = @numDivisionID
			AND OB.bitAuthoritativeBizDocs=1
			AND ISNULL(OB.tintDeferred,0) <> 1
			AND  isnull(OB.monDealAmount * OM.fltExchangeRate,0) > 0 
			AND isnull(OB.monDealAmount * OM.fltExchangeRate - OB.[monAmountPaid] * OM.fltExchangeRate,0) > 0 
		UNION 
		SELECT  
			OM.[numOppId],
			OM.[vcPOppName],
			OB.[numOppBizDocsId],
			OB.[vcBizDocID],
			BDC.numComissionAmount,
			0 AmountPaid,
			BDC.numComissionAmount BalanceDue,
			CASE ISNULL(OM.bitBillingTerms,0) 
				WHEN 1 THEN DATEADD(dd,convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(OM.intBillingDays,0)), 0)),OB.dtFromDate)
				WHEN 0 THEN ob.[dtFromDate]
			END AS DueDate
		FROM    
			BizDocComission BDC 
		JOIN OpportunityBizDocs OB ON BDC.numOppBizDocId = OB.numOppBizDocsId
		join OpportunityMaster OM on OM.numOppId=OB.numOppId
		JOIN Domain D on D.numDomainID=BDC.numDomainID
		LEFT JOIN OpportunityBizDocsDetails OBDD ON BDC.numBizDocsPaymentDetId=isnull(OBDD.numBizDocsPaymentDetId,0)        
		WHERE   
			OM.numDomainID = @numDomainID
			AND BDC.numDomainID = @numDomainID
			AND OB.bitAuthoritativeBizDocs = 1
			AND isnull(OBDD.bitIntegratedToAcnt,0) = 0
			AND D.numDivisionId = @numDivisionID    	
		UNION 
		SELECT 
			GJH.numJournal_Id [numOppId],
			'Journal' [vcPOppName],
			0 [numOppBizDocsId],
			'' [vcBizDocID],
			CASE WHEN GJD.numDebitAmt > 0 THEN GJD.numDebitAmt ELSE -1*GJD.numCreditAmt END TotalAmount,
			0 AmountPaid,
			1 BalanceDue,
			GJH.datEntry_Date AS DueDate
		FROM   
			dbo.General_Journal_Header GJH
			INNER JOIN dbo.General_Journal_Details GJD ON GJH.numJournal_Id = GJD.numJournalId AND GJH.numDomainId = GJD.numDomainId
			INNER JOIN dbo.Chart_Of_Accounts COA ON COA.numAccountId = GJD.numChartAcntId
		WHERE  
			GJH.numDomainId = @numDomainID
			AND GJD.numCustomerID =@numDivisionID
			AND COA.vcAccountCode LIKE '01020102%'
			AND ISNULL(numOppId,0)=0 AND ISNULL(numOppBizDocsId,0)=0 
			AND ISNULL(GJH.numBillID,0)=0 AND ISNULL(GJH.numBillPaymentID,0)=0 AND ISNULL(GJH.numCheckHeaderID,0)=0 AND ISNULL(GJH.numReturnID,0)=0
			AND GJH.numJournal_Id NOT IN (
			--Exclude Bill and Bill payment entries
			SELECT GH.numJournal_Id FROM dbo.General_Journal_Header GH INNER JOIN dbo.OpportunityBizDocsDetails OBD ON GH.numBizDocsPaymentDetId = OBD.numBizDocsPaymentDetId 
			WHERE GH.numDomainId=@numDivisionID and OBD.tintPaymentType IN (2,4,6) and GH.numBizDocsPaymentDetId>0
			) AND ISNULL(GJH.numBillID,0)=0 AND ISNULL(GJH.numBillPaymentID,0)=0 AND ISNULL(GJH.numCheckHeaderID,0)=0) TEMP
		ORDER BY
			TEMP.DueDate DESC
	END
	-- OPEN ORDERS
	ELSE IF @numAlertType = 2
	BEGIN
		SELECT
			TOP 20 
			OM.vcPOppName [Order],
			ISNULL((SELECT vcData FROM ListDetails WHERE numListItemID = ISNULL(OM.numStatus,0)),'') AS [Order Status],
			ISNULL(OM.intPEstimatedCloseDate,'') AS [Due Date],
			CAST(ISNULL(numPercentageComplete,0) AS VARCHAR(3)) + ' %' As [Total Progress]
		FROM
			OpportunityMaster OM
		WHERE
			OM.numDomainID = @numDomainID
			AND OM.numDivisionID = @numDivisionID
			AND (OM.tintOppType = 1 OR OM.tintOppType = 2)
			AND OM.tintOppStatus = 1
			AND ISNULL(tintshipped,0)=0
		ORDER BY
			OM.intPEstimatedCloseDate DESC
	END	
	-- OPEN CASES
	ELSE IF @numAlertType = 3
	BEGIN
		SELECT    
			TOP 20 
			vcCaseNumber AS [Case #],
			textSubject AS [Subject],
			textDesc AS [Description],
			ISNULL((SELECT vcData FROM ListDetails WHERE numListItemID = ISNULL(Cases.numStatus,0)),'') AS [Status] 
		FROM            
			dbo.Cases
		WHERE        
			Cases.numstatus <> 136
			AND numDomainID = @numDomainID
			AND numDivisionID = @numDivisionID
		ORDER BY
			Cases.bintCreatedDate DESC
	END
	-- OPEN PROJECT
	ELSE IF @numAlertType = 4
	BEGIN
		SELECT        
			TOP 20
			vcProjectID AS [Project],
			ISNULL((SELECT vcData FROM ListDetails WHERE numListItemID = ISNULL(ProjectsMaster.numProjectStatus,0)),'') AS [Status],
			CAST(ISNULL((SELECT TOP 1 intTotalProgress FROM ProjectProgress WHERE numProId=numProId),0) AS VARCHAR(3)) + ' %' As [Total Progress],
			ISNULL(intDueDate,'') AS [Due Date],
			ISNULL(txtComments,'') AS [Comments]
		FROM            
			dbo.ProjectsMaster
		WHERE        
			numDomainId=@numDomainID
			AND numDivisionId = @numDivisionID
		ORDER BY
			bintCreatedDate DESC
			END
	-- OPEN ACTION ITEM
	ELSE IF @numAlertType = 5
	BEGIN
		SELECT     
			TOP 20 
			ISNULL((SELECT vcData FROM ListDetails WHERE numListItemID = ISNULL(bitTask,0)),'') AS [Type],
			CONCAT(CAST(dtStartTime AS DATE), ' ' ,FORMAT(dtStartTime,'hh:mm tt'),'-',FORMAT(dtEndTime,'hh:mm tt')) AS [Date, From, To],
			ISNULL(textDetails,'') AS [Comments]
		FROM            
			dbo.Communication
		WHERE        
			bitClosedFlag = 0
			AND numDomainID = @numDomainID
			AND numDivisionId = @numDivisionID
			AND numContactId = @numContactID
		ORDER BY
			Communication.dtCreatedDate DESC
	END
END
