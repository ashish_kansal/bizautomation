GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO

IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_WorkOrder_PutAway')
DROP PROCEDURE USP_WorkOrder_PutAway
GO
CREATE PROCEDURE [dbo].[USP_WorkOrder_PutAway]
(
	@numDomainID NUMERIC(18,0)
	,@numUserCntID NUMERIC(18,0)
	,@numWOID NUMERIC(18,0)
	,@numQtyReceived FLOAT
	,@dtItemReceivedDate DATETIME
	,@vcWarehouses VARCHAR(MAX)
	,@tintMode TINYINT
)
AS
BEGIN
	IF ISNULL(@numQtyReceived,0) <= 0 
	BEGIN
		RAISERROR('QTY_TO_RECEIVE_NOT_PROVIDED',16,1)
		RETURN
	END 

	IF LEN(ISNULL(@vcWarehouses,'')) = 0
	BEGIN
		RAISERROR('LOCATION_NOT_SELECTED',16,1)
		RETURN
	END

	DECLARE @TEMPWarehouseLot TABLE
	(
		ID INT
		,numWareHouseItmsDTLID NUMERIC(18,0)
		,vcSerialNo VARCHAR(300)
		,numQty FLOAT
	)

	DECLARE @TEMPSerialLot TABLE
	(
		ID INT
		,vcSerialNo VARCHAR(300)
		,numQty FLOAT
	)

	DECLARE @TEMPWarehouseLocations TABLE
	(
		ID INT IDENTITY(1,1)
		,numWarehouseLocationID NUMERIC(18,0)
		,numWarehouseItemID NUMERIC(18,0)
		,numReceivedQty FLOAT
		,vcSerialLotNo VARCHAR(MAX)
	)

	DECLARE @hDocItem int
	EXEC sp_xml_preparedocument @hDocItem OUTPUT, @vcWarehouses

	INSERT INTO @TEMPWarehouseLocations
	(
		numWarehouseLocationID
		,numWarehouseItemID
		,numReceivedQty
		,vcSerialLotNo
	)
	SELECT 
		(CASE WHEN ISNULL(bitNewLocation,0) = 1 THEN ID ELSE 0 END)
		,(CASE WHEN ISNULL(bitNewLocation,0) = 1 THEN 0 ELSE ID END)
		,ISNULL(Qty,0)
		,ISNULL(SerialLotNo,'') 
	FROM 
		OPENXML (@hDocItem, '/NewDataSet/Table1',2)
	WITH 
	(
		bitNewLocation BIT, 
		ID NUMERIC(18,0),
		Qty FLOAT,
		SerialLotNo VARCHAR(MAX)
	)

	EXEC sp_xml_removedocument @hDocItem

	IF @numQtyReceived <> ISNULL((SELECT SUM(numReceivedQty) FROM @TEMPWarehouseLocations),0)
	BEGIN
		RAISERROR('INALID_QTY_RECEIVED',16,1)
		RETURN
	END

	DECLARE @numItemCode NUMERIC(18,0)
	DECLARE @monListPrice DECIMAL(20,5)
	DECLARE @numWarehouseID NUMERIC(18,0)
	DECLARE @numWarehouseItemID NUMERIC(18,0)
	DECLARE @numOverheadServiceItemID NUMERIC(18,0)
	DECLARE @monAverageCost AS DECIMAL(20,5)
	DECLARE @newAverageCost AS DECIMAL(20,5)
	DECLARE @monOverheadCost DECIMAL(20,5) 
	DECLARE @monLabourCost DECIMAL(20,5)
	DECLARE @CurrentAverageCost DECIMAL(20,5)
	DECLARE @TotalCurrentOnHand FLOAT
	DECLARE @bitLot BIT
	DECLARE @bitSerial BIT
	DECLARE @numOldQtyReceived FLOAT
	DECLARE @numUnits FLOAT
	DECLARE @description VARCHAR(300)

	IF EXISTS (SELECT numWOId FROM WorkOrder WHERE numDomainID=@numDomainID AND numWOId=@numWOID)
	BEGIN
		SELECT 
			@numItemCode=Item.numItemCode
			,@monListPrice=ISNULL(Item.monListPrice,0)
			,@numWarehouseID=WareHouseItems.numWareHouseID
			,@numWarehouseItemID=WareHouseItems.numWareHouseItemID
			,@bitLot=ISNULL(bitLotNo,0)
			,@bitSerial=ISNULL(bitSerialized,0)
			,@numUnits=ISNULL(numQtyItemsReq,0)
			,@numOldQtyReceived=ISNULL(numUnitHourReceived,0)
		FROM 
			WorkOrder 
		INNER JOIN
			Item
		ON
			WorkOrder.numItemCode = Item.numItemCode
		INNER JOIN
			WareHouseItems
		ON
			WareHouseItems.numWareHouseItemID = WorkOrder.numWareHouseItemId
		WHERE 
			WorkOrder.numDomainID=@numDomainID 
			AND WorkOrder.numWOId=@numWOID

		IF (@numQtyReceived + @numOldQtyReceived) > @numUnits
		BEGIN
			RAISERROR('RECEIVED_QTY_MUST_BE_LESS_THEN_OR_EQUAL_TO_ORDERED_QTY',16,1)
			RETURN
		END

		SELECT @numOverheadServiceItemID=ISNULL(numOverheadServiceItemID,0) FROM Domain WHERE numDomainId=@numDomainID
	
		SELECT @CurrentAverageCost = (CASE WHEN ISNULL(bitVirtualInventory,0) = 1 THEN 0 ELSE monAverageCost END) FROM dbo.Item WHERE numItemCode =@numItemCode 
		SELECT @TotalCurrentOnHand = ISNULL((SUM(numOnHand) + SUM(numAllocation)),0) FROM dbo.WareHouseItems WHERE numItemID =@numItemCode 
		PRINT @CurrentAverageCost
		PRINT @TotalCurrentOnHand

		-- Store hourly rate for all task assignee
		UPDATE 
			SPDT
		SET
			SPDT.monHourlyRate = ISNULL(UM.monHourlyRate,0)
		FROM
			StagePercentageDetailsTask SPDT
		INNER JOIN
			UserMaster UM
		ON
			SPDT.numAssignTo = UM.numUserDetailId
		WHERE
			SPDT.numDomainID = @numDomainID
			AND SPDT.numWorkOrderId = @numWOID

		SELECT 
			@newAverageCost = SUM(numQtyItemsReq * ISNULL(I.monAverageCost,0))
		FROM 
			WorkOrderDetails WOD
		LEFT JOIN 
			dbo.Item I 
		ON 
			I.numItemCode = WOD.numChildItemID
		WHERE 
			WOD.numWOId = @numWOId
			AND I.charItemType = 'P'

		IF @numOverheadServiceItemID > 0 AND EXISTS (SELECT numWODetailId FROM WorkOrderDetails WHERE numWOId=@numWOId AND numChildItemID=@numOverheadServiceItemID)
		BEGIN
			SET @monOverheadCost = (SELECT SUM(numQtyItemsReq * ISNULL(Item.monListPrice,0)) FROM WorkOrderDetails WOD INNER JOIN Item ON WOD.numChildItemID=Item.numItemCode WHERE WOD.numWOId=@numWOId AND WOD.numChildItemID=@numOverheadServiceItemID)
		END

		SET @monLabourCost = dbo.GetWorkOrderLabourCost(@numDomainID,@numWOId)			

		UPDATE WorkOrder SET monAverageCost=@newAverageCost,monLabourCost=ISNULL(@monLabourCost,0),monOverheadCost=ISNULL(@monOverheadCost,0) WHERE numWOId=@numWOId

		SET @newAverageCost = ((@CurrentAverageCost * @TotalCurrentOnHand)  + (@newAverageCost + ISNULL(@monOverheadCost,0) + ISNULL(@monLabourCost,0))) / (@TotalCurrentOnHand + @numQtyReceived)

		UPDATE
			WareHouseItems
		SET 
			numOnOrder = ISNULL(numOnOrder,0) - @numQtyReceived,
			dtModified = GETDATE()
		WHERE
			numWareHouseItemID=@numWareHouseItemID
		
		SET @description = CONCAT('Work Order Received (Qty:',@numQtyReceived,')')

		EXEC dbo.USP_ManageWareHouseItems_Tracking
			@numWareHouseItemID = @numWareHouseItemID, --  numeric(9, 0)
			@numReferenceID = @numWOID, --  numeric(9, 0)
			@tintRefType = 2, --  tinyint
			@vcDescription = @description,
			@numModifiedBy = @numUserCntID,
			@dtRecordDate =  @dtItemReceivedDate,
			@numDomainID = @numDomainID

		DECLARE @i INT
		DECLARE @j INT
		DECLARE @k INT
		DECLARE @iCount INT = 0
		DECLARE @jCount INT = 0
		DECLARE @kCount INT = 0
		DECLARE @vcSerialLot# VARCHAR(MAX)
		DECLARE @numWareHouseItmsDTLID NUMERIC(18,0)
		DECLARE @vcSerailLotNumber VARCHAR(300)
		DECLARE @numSerialLotQty FLOAT
		DECLARE @numTempWLocationID NUMERIC(18,0)
		DECLARE @numTempWarehouseItemID NUMERIC(18,0)
		DECLARE @numTempReceivedQty FLOAT
		DECLARE @numLotWareHouseItmsDTLID NUMERIC(18,0)
		DECLARE @numLotQty FLOAT
	
		SET @i = 1
		SET @iCount = (SELECT COUNT(*) FROM @TEMPWarehouseLocations)

		WHILE @i <= @iCount
		BEGIN
			SELECT 
				@numTempWLocationID=ISNULL(numWarehouseLocationID,0)
				,@numTempWarehouseItemID=ISNULL(numWarehouseItemID,0)
				,@numTempReceivedQty=ISNULL(numReceivedQty,0)
				,@vcSerialLot# = ISNULL(vcSerialLotNo,'')
			FROM 
				@TEMPWarehouseLocations 
			WHERE 
				ID = @i

			IF (ISNULL(@bitLot,0)=1 OR ISNULL(@bitSerial,0)=1)
			BEGIN
				IF LEN(ISNULL(@vcSerialLot#,'')) = 0
				BEGIN
					RAISERROR('SERIALLOT_REQUIRED',16,1)
					RETURN
				END 
				ELSE
				BEGIN	
					DELETE FROM @TEMPSerialLot

					IF ISNULL(@bitSerial,0) = 1
					BEGIN
						INSERT INTO @TEMPSerialLot
						(
							ID
							,vcSerialNo
							,numQty
						)
						SELECT
							ROW_NUMBER() OVER(ORDER BY OutParam)
							,RTRIM(LTRIM(OutParam))
							,1
						FROM
							dbo.SplitString(@vcSerialLot#,',')
					END
					ELSE IF @bitLot = 1
					BEGIN
						INSERT INTO @TEMPSerialLot
						(
							ID
							,vcSerialNo
							,numQty
						)
						SELECT
							ROW_NUMBER() OVER(ORDER BY OutParam)
							,(CASE 
								WHEN PATINDEX('%(%', OutParam) > 0 AND PATINDEX('%)%', OutParam) > 0 AND PATINDEX('%)%',OutParam) > (PATINDEX('%(%',OutParam) + 1)
								THEN RTRIM(LTRIM(SUBSTRING(OutParam,0,PATINDEX('%(%', OutParam))))
								ELSE ''
							END)
							,(CASE 
								WHEN PATINDEX('%(%', OutParam) > 0 AND PATINDEX('%)%', OutParam) > 0 AND PATINDEX('%)%',OutParam) > (PATINDEX('%(%',OutParam) + 1)
								THEN (CASE WHEN ISNUMERIC(SUBSTRING(OutParam,(PATINDEX('%(%', OutParam) + 1),(PATINDEX('%)%', OutParam) - PATINDEX('%(%', OutParam) - 1))) = 1 
										THEN CAST(SUBSTRING(OutParam,(PATINDEX('%(%', OutParam) + 1),(PATINDEX('%)%', OutParam) - PATINDEX('%(%', OutParam) - 1)) AS FLOAT)  
										ELSE -1 
										END) 
								ELSE -1
							END)	
	
						FROM
							dbo.SplitString(@vcSerialLot#,',')
					END

					IF EXISTS (SELECT vcSerialNo FROM @TEMPSerialLot WHERE numQty = -1)
					BEGIN
						RAISERROR('INVALID_SERIAL_NO',16,1)
						RETURN
					END
					ELSE IF @numTempReceivedQty <> ISNULL((SELECT SUM(numQty) FROM @TEMPSerialLot),0)
					BEGIN
						RAISERROR('INALID_SERIALLOT',16,1)
						RETURN
					END
				END
			END

			IF ISNULL(@numTempWarehouseItemID,0) = 0
			BEGIN
				IF EXISTS (SELECT numWLocationID FROM WarehouseLocation WHERE numDomainID=@numDomainID AND numWLocationID=ISNULL(@numTempWLocationID,0))
				BEGIN
					IF EXISTS (SELECT numWareHouseItemID FROM WareHouseItems WHERE numDomainID=@numDomainID AND numItemID=@numItemCode AND numWareHouseID=@numWarehouseID AND numWLocationID=@numTempWLocationID)
					BEGIN
						SET @numTempWarehouseItemID = (SELECT TOP 1 numWareHouseItemID FROM WareHouseItems WHERE numDomainID=@numDomainID AND numItemID=@numItemCode AND numWareHouseID=@numWarehouseID AND numWLocationID=@numTempWLocationID)
					END
					ELSE 
					BEGIN
						INSERT INTO WareHouseItems 
						(
							numItemID, 
							numWareHouseID,
							numWLocationID,
							numOnHand,
							numAllocation,
							numOnOrder,
							numBackOrder,
							numReorder,
							monWListPrice,
							vcLocation,
							vcWHSKU,
							vcBarCode,
							numDomainID,
							dtModified
						)  
						VALUES
						(
							@numItemCode,
							@numWareHouseID,
							@numTempWLocationID,
							0,
							0,
							0,
							0,
							0,
							@monListPrice,
							ISNULL((SELECT vcLocation FROM WarehouseLocation WHERE numDomainID=@numDomainID AND numWLocationID=ISNULL(@numTempWLocationID,0)),''),
							'',
							'',
							@numDomainID,
							GETDATE()
						)  

						SELECT @numTempWarehouseItemID = SCOPE_IDENTITY()

						DECLARE @dtDATE AS DATETIME = GETUTCDATE()
						DECLARE @vcDescription VARCHAR(100)='INSERT WareHouse'

						EXEC dbo.USP_ManageWareHouseItems_Tracking
							@numWareHouseItemID = @numTempWarehouseItemID,
							@numReferenceID = @numItemCode,
							@tintRefType = 1,
							@vcDescription = @vcDescription,
							@numModifiedBy = @numUserCntID,
							@ClientTimeZoneOffset = 0,
							@dtRecordDate = @dtDATE,
							@numDomainID = @numDomainID 
					END
				END
				ELSE 
				BEGIN
					RAISERROR('INVALID_WAREHOUSE_LOCATION',16,1)
					RETURN
				END
			END

			IF NOT EXISTS (SELECT numWareHouseItemID FROM WareHouseItems WHERE numDomainID=@numDomainID AND numItemID=@numItemCode AND numWareHouseItemID=@numTempWarehouseItemID)
			BEGIN
				RAISERROR('INVALID_WAREHOUSE',16,1)
				RETURN
			END
			ELSE
			BEGIN
				-- INCREASE THE OnHand Of Destination Location
				UPDATE
					WareHouseItems
				SET
					numBackOrder = (CASE WHEN numBackOrder > @numTempReceivedQty THEN ISNULL(numBackOrder,0) - @numTempReceivedQty ELSE 0 END),         
					numAllocation = (CASE WHEN numBackOrder > @numTempReceivedQty THEN ISNULL(numAllocation,0) + @numTempReceivedQty ELSE ISNULL(numAllocation,0) + ISNULL(numBackOrder,0) END),
					numOnHand = (CASE WHEN numBackOrder > @numTempReceivedQty THEN numOnHand ELSE ISNULL(numOnHand,0) + (@numTempReceivedQty - ISNULL(numBackOrder,0)) END),
					dtModified = GETDATE()
				WHERE
					numWareHouseItemID=@numTempWarehouseItemID

				SET @description = CONCAT('Work Order Qty Put-Away (Qty:',@numTempReceivedQty,')')

				EXEC dbo.USP_ManageWareHouseItems_Tracking
					@numWareHouseItemID = @numTempWarehouseItemID, --  numeric(9, 0)
					@numReferenceID = @numWOID, --  numeric(9, 0)
					@tintRefType = 2, --  tinyint
					@vcDescription = @description,
					@numModifiedBy = @numUserCntID,
					@dtRecordDate =  @dtItemReceivedDate,
					@numDomainID = @numDomainID

				IF (ISNULL(@bitLot,0)=1 OR ISNULL(@bitSerial,0)=1)
				BEGIN
					SET @j = 1
					SET @jCount = (SELECT COUNT(*) FROM @TEMPSerialLot)

					WHILE (@j <= @jCount AND @numTempReceivedQty > 0)
					BEGIN
						SELECT 
							@vcSerailLotNumber=ISNULL(vcSerialNo,'')
							,@numSerialLotQty=ISNULL(numQty,0)
						FROM 
							@TEMPSerialLot 
						WHERE 
							ID=@j

						IF @numSerialLotQty > 0
						BEGIN
							IF @bitSerial = 1 AND (SELECT COUNT(*) FROM WareHouseItems INNER JOIN WareHouseItmsDTL ON WareHouseItems.numWareHouseItemID = WareHouseItmsDTL.numWareHouseItemID WHERE WareHouseItems.numItemID=@numItemCode AND LOWER(vcSerialNo)=LOWER(@vcSerailLotNumber)) > 0
							BEGIN
								RAISERROR('DUPLICATE_SERIAL_NO',16,1)
								RETURN
							END

							INSERT INTO WareHouseItmsDTL
							(
								numWareHouseItemID
								,vcSerialNo
								,numQty
								,bitAddedFromPO
							)  
							VALUES
							( 
								@numTempWarehouseItemID
								,@vcSerailLotNumber
								,(CASE WHEN @numTempReceivedQty >= @numSerialLotQty THEN @numSerialLotQty ELSE @numTempReceivedQty END)
								,1 
							)

							SET @numWareHouseItmsDTLID=SCOPE_IDENTITY()
		
							INSERT INTO OppWarehouseSerializedItem
							(
								numWarehouseItmsDTLID
								,numWOID
								,numWarehouseItmsID
								,numQty
							)  
							VALUES
							( 
								@numWareHouseItmsDTLID
								,@numWOID
								,@numTempWarehouseItemID
								,(CASE WHEN @numTempReceivedQty >= @numSerialLotQty THEN @numSerialLotQty ELSE @numTempReceivedQty END)
							)

							IF @numTempReceivedQty >= @numSerialLotQty
							BEGIN
								SET @numTempReceivedQty = @numTempReceivedQty - @numSerialLotQty
								UPDATE @TEMPSerialLot SET numQty = 0  WHERE ID = @j
							END
							ELSE
							BEGIN
								UPDATE @TEMPSerialLot SET numQty = numQty - @numTempReceivedQty  WHERE ID = @j
								SET @numTempReceivedQty = 0
							END
						END

						SET @j = @j + 1
					END

					IF @numTempReceivedQty > 0
					BEGIN
						RAISERROR('INALID_SERIALLOT',16,1)
						RETURN
					END
				END
			END


			SET @i = @i + 1
		END

		UPDATE  
			WorkOrder
		SET 
			numUnitHourReceived = ISNULL(numUnitHourReceived,0) + ISNULL(@numQtyReceived,0)
			,numQtyReceived = (CASE WHEN ISNULL(@tintMode,0)=1 THEN ISNULL(numQtyReceived,0) + ISNULL(@numQtyReceived,0) ELSE ISNULL(numQtyReceived,0) END)
		WHERE
			numDomainID = @numDomainID
			AND numWOID = @numWOID
	END
	ELSE
	BEGIN
		RAISERROR('WORKORDER_DOES_NOT_EXISTS',16,1)
	END

	
END
GO