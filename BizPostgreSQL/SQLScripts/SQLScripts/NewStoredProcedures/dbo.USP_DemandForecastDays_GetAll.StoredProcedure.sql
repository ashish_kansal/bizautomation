SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_DemandForecastDays_GetAll')
DROP PROCEDURE dbo.USP_DemandForecastDays_GetAll
GO
CREATE PROCEDURE [dbo].[USP_DemandForecastDays_GetAll]
	@numDomainID NUMERIC (18,0)
AS 
BEGIN
    IF EXISTS (SELECT numDFDaysID FROM DemandForecastDays WHERE ISNULL(numDomainID,0) = @numDomainID) 
		SELECT * FROM DemandForecastDays WHERE ISNULL(numDomainID,0) = @numDomainID
	ELSE
		SELECT * FROM DemandForecastDays WHERE ISNULL(numDomainID,0) = 0 
END
