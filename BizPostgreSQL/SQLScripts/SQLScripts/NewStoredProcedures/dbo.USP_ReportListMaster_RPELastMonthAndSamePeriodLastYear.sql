SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ReportListMaster_RPELastMonthAndSamePeriodLastYear')
DROP PROCEDURE USP_ReportListMaster_RPELastMonthAndSamePeriodLastYear
GO
CREATE PROCEDURE [dbo].[USP_ReportListMaster_RPELastMonthAndSamePeriodLastYear]
	@numDomainID NUMERIC(18,0)
	,@ClientTimeZoneOffset INT
	,@vcFilterValue VARCHAR(MAX)
AS
BEGIN 
	DECLARE @ExpenseLastMonth DECIMAL(20,5)
	DECLARE @ExpenseSamePeriodLastYear DECIMAL(20,5)
	DECLARE @ProfitLastMonth DECIMAL(20,5)
	DECLARE @ProfitSamePeriodLastYear DECIMAL(20,5)
	DECLARE @RevenueLastMonth DECIMAL(20,5)
	DECLARE @RevenueSamePeriodLastYear DECIMAL(20,5)
	DECLARE @numShippingItemID NUMERIC(18,0)
	DECLARE @numDiscountItemID NUMERIC(18,0)
	DECLARE @dtLastMonthFromDate AS DATETIME                                       
	DECLARE @dtLastMonthToDate AS DATETIME
	DECLARE @dtSameMonthLYFromDate AS DATETIME                                       
	DECLARE @dtSameMonthLYToDate AS DATETIME

	DECLARE @FinancialYearStartDate AS DATETIME
	SELECT @FinancialYearStartDate = dbo.GetFiscalStartDate(YEAR(GETDATE()),@numDomainID)

	--SELECT @dtLastMonthFromDate = DATEADD(mm,-1,DATEADD(mm,DATEDIFF(mm,0,GETDATE()),0)), @dtLastMonthToDate = DATEADD(ms,-3,DATEADD(mm,0,DATEADD(mm,DATEDIFF(mm,0,GETDATE()),0)));
	--SELECT @dtSameMonthLYFromDate = DATEADD(YEAR,-1,@dtLastMonthFromDate),@dtSameMonthLYToDate = DATEADD(YEAR,-1,@dtLastMonthToDate);

	SELECT @dtSameMonthLYFromDate = DATEADD(YEAR,-1,@dtLastMonthFromDate),@dtSameMonthLYToDate = DATEADD(YEAR,-1,@dtLastMonthToDate);
	DECLARE @StartDate AS DATETIME 
	SET @StartDate= DATEADD(mm,DATEDIFF(mm,0,GETDATE()),0)

	IF @vcFilterValue = 'ThisWeekVSLastWeek'
	BEGIN
		SET @dtLastMonthFromDate = DATEADD(WEEK,0,@StartDate)
		SET @dtLastMonthToDate= DATEADD(WEEK,-1,DATEADD(mm,0,@StartDate))
	END
	IF @vcFilterValue = 'ThisMonthVSLastMonth'
	BEGIN
		SET @dtLastMonthFromDate = DATEADD(MM,0,@StartDate)
		SET @dtLastMonthToDate= DATEADD(MM,-1,DATEADD(mm,0,@StartDate))
	END
	IF @vcFilterValue = 'ThisQTDVSLastQTD'
	BEGIN
		SET @dtLastMonthFromDate = DATEADD(QQ,0,@StartDate)
		SET @dtLastMonthToDate= DATEADD(QQ,-1,DATEADD(mm,0,@StartDate))
	END
	IF @vcFilterValue = 'ThisYTDVSLastYTD'
	BEGIN
		SET @dtLastMonthFromDate = DATEADD(YY,0,@StartDate)
		SET @dtLastMonthToDate= DATEADD(YY,-1,DATEADD(mm,0,@StartDate))
	END
	


	SELECT 
		@numShippingItemID=numShippingServiceItemID
		,@numDiscountItemID=numDiscountServiceItemID
	FROM 
		Domain 
	WHERE 
		numDomainId=@numDomainID


	-- GET EXPENSE YTD VS SAME PERIOD LAST YEAR
	SELECT @ExpenseLastMonth = SUM(numDebitAmt) - SUM(numCreditAmt) FROM General_Journal_Details INNER JOIN General_Journal_Header ON General_Journal_Details.numJournalId=General_Journal_Header.numJournal_Id WHERE General_Journal_Details.numDomainId=@numDomainID AND datEntry_Date BETWEEN  @dtLastMonthFromDate AND @dtLastMonthToDate AND General_Journal_Details.numChartAcntId IN (SELECT numAccountId FROM Chart_Of_Accounts WHERE numDomainId=@numDomainID AND bitActive = 1 AND numAcntTypeId IN (SELECT numAccountTypeID FROM AccountTypeDetail WHERE numDomainID=@numDomainID AND vcAccountCode LIKE '0104%'))
	SELECT @ExpenseSamePeriodLastYear = SUM(numDebitAmt) - SUM(numCreditAmt) FROM General_Journal_Details INNER JOIN General_Journal_Header ON General_Journal_Details.numJournalId=General_Journal_Header.numJournal_Id WHERE General_Journal_Details.numDomainId=@numDomainID AND datEntry_Date BETWEEN  @dtSameMonthLYFromDate AND @dtSameMonthLYToDate AND General_Journal_Details.numChartAcntId IN (SELECT numAccountId FROM Chart_Of_Accounts WHERE numDomainId=@numDomainID AND bitActive = 1 AND numAcntTypeId IN (SELECT numAccountTypeID FROM AccountTypeDetail WHERE numDomainID=@numDomainID AND vcAccountCode LIKE '0104%'))

	-- GET PROFIT YTD VS SAME PERIOD LAST YEAR
	SELECT 
		@ProfitLastMonth = SUM(Profit)
	FROM
	(
		SELECT
			ISNULL(dbo.GetOrderItemProfitAmountOrMargin(@numDomainID,OM.numOppId,OI.numoppitemtCode,1),0) Profit
		FROM
			OpportunityMaster OM
		INNER JOIN
			OpportunityItems OI
		ON
			OM.numOppID=OI.numOppId
		INNER JOIN
			Item I
		ON
			OI.numItemCode = I.numItemCode
		WHERE
			OM.numDomainId=@numDomainID
			AND OM.bintCreatedDate BETWEEN  @dtLastMonthFromDate AND @dtLastMonthToDate
			AND ISNULL(OM.tintOppType,0)=1
			AND ISNULL(OM.tintOppStatus,0)=1
			AND I.numItemCode NOT IN (@numShippingItemID,@numDiscountItemID)
	) TEMP

	SELECT 
		@ProfitSamePeriodLastYear = SUM(Profit)
	FROM
	(
		SELECT
			ISNULL(dbo.GetOrderItemProfitAmountOrMargin(@numDomainID,OM.numOppId,OI.numoppitemtCode,1),0) Profit
		FROM
			OpportunityMaster OM
		INNER JOIN
			OpportunityItems OI
		ON
			OM.numOppID = OI.numOppId
		INNER JOIN
			Item I
		ON
			OI.numItemCode = I.numItemCode
		WHERE
			OM.numDomainId=@numDomainID
			AND OM.bintCreatedDate BETWEEN  @dtSameMonthLYFromDate AND @dtSameMonthLYToDate
			AND ISNULL(OM.tintOppType,0)=1
			AND ISNULL(OM.tintOppStatus,0)=1
			AND I.numItemCode NOT IN (@numShippingItemID,@numDiscountItemID)
	) TEMP

	-- GET REVENUE YTD VS SAME PERIOD LAST YEAR
	SELECT 
		@RevenueLastMonth = SUM(monTotAmount)
	FROM
	(
		SELECT
			ISNULL(monTotAmount,0) monTotAmount
		FROM
			OpportunityItems OI
		INNER JOIN
			OpportunityMaster OM
		ON
			OI.numOppId = OM.numOppID
		INNER JOIN
			DivisionMaster DM
		ON
			OM.numDivisionId = DM.numDivisionID
		INNER JOIN
			CompanyInfo CI
		ON
			DM.numCompanyID = CI.numCompanyId
		INNER JOIN
			Item I
		ON
			OI.numItemCode = I.numItemCode
		Left JOIN 
			Vendor V 
		ON 
			V.numVendorID=I.numVendorID 
			AND V.numItemCode=I.numItemCode
		WHERE
			OM.numDomainId=@numDomainID
			AND OM.bintCreatedDate BETWEEN  @dtLastMonthFromDate AND @dtLastMonthToDate
			AND ISNULL(OM.tintOppType,0)=1
			AND ISNULL(OM.tintOppStatus,0)=1
	) TEMP

	SELECT 
		@RevenueSamePeriodLastYear = SUM(monTotAmount)
	FROM
	(
		SELECT
			ISNULL(monTotAmount,0) monTotAmount
		FROM
			OpportunityItems OI
		INNER JOIN
			OpportunityMaster OM
		ON
			OI.numOppId = OM.numOppID
		INNER JOIN
			DivisionMaster DM
		ON
			OM.numDivisionId = DM.numDivisionID
		INNER JOIN
			CompanyInfo CI
		ON
			DM.numCompanyID = CI.numCompanyId
		INNER JOIN
			Item I
		ON
			OI.numItemCode = I.numItemCode
		Left JOIN 
			Vendor V 
		ON 
			V.numVendorID=I.numVendorID 
			AND V.numItemCode=I.numItemCode
		WHERE
			OM.numDomainId=@numDomainID
			AND OM.bintCreatedDate BETWEEN  @dtSameMonthLYFromDate AND @dtSameMonthLYToDate
			AND ISNULL(OM.tintOppType,0)=1
			AND ISNULL(OM.tintOppStatus,0)=1
	) TEMP

	SELECT
		100.0 * (ISNULL(@RevenueLastMonth,0) - ISNULL(@RevenueSamePeriodLastYear,0)) / CASE WHEN ISNULL(@RevenueSamePeriodLastYear,0) = 0 THEN 1 ELSE @RevenueSamePeriodLastYear END As RevenueDifference
		,100.0 * (ISNULL(@ProfitLastMonth,0) - ISNULL(@ProfitSamePeriodLastYear,0)) / CASE WHEN ISNULL(@ProfitSamePeriodLastYear,0) = 0 THEN 1 ELSE @ProfitSamePeriodLastYear END As ProfitDifference
		,100.0 * (ISNULL(@ExpenseLastMonth,0) - ISNULL(@ExpenseSamePeriodLastYear,0)) / CASE WHEN ISNULL(@ExpenseSamePeriodLastYear,0) = 0 THEN 1 ELSE @ExpenseSamePeriodLastYear END As ExpenseDifference
END
GO