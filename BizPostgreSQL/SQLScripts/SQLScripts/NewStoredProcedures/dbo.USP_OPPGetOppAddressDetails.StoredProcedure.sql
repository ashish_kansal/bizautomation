GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_OPPGetOppAddressDetails')
DROP PROCEDURE USP_OPPGetOppAddressDetails
GO
CREATE PROCEDURE [dbo].[USP_OPPGetOppAddressDetails]
(
    @numOppId AS NUMERIC(18,0) = NULL,
    @numDomainID AS NUMERIC(18,0)  = 0,
	@numOppBizDocID AS NUMERIC(18,0) = 0
)
AS

  DECLARE  @tintOppType  AS TINYINT, @tintBillType  AS TINYINT, @tintShipType  AS TINYINT
 
  DECLARE @numParentOppID AS NUMERIC,@numDivisionID AS NUMERIC,@numContactID AS NUMERIC, @numBillToAddressID NUMERIC(18,0), @numShipToAddressID NUMERIC(18,0),@numVendorAddressID NUMERIC(18,0)
      
  SELECT  @tintOppType = tintOppType,
		  @tintBillType = tintBillToType,
		  @tintShipType = tintShipToType,
          @numDivisionID = numDivisionID,
          @numContactID = numContactID,
		  @numBillToAddressID = ISNULL(numBillToAddressID,0),
		  @numShipToAddressID = ISNULL(numShipToAddressID,0),
		  @numVendorAddressID= ISNULL(numVendorAddressID,0)
      FROM   OpportunityMaster WHERE  numOppId = @numOppId


	 IF @tintOppType=2
		BEGIN
			--************Opp/Order Billing Address************
			IF @numBillToAddressID > 0
			BEGIN
				SELECT  
					'<pre>' + isnull(AD.vcStreet,'') + '</pre>' + isnull(AD.vcCity,'') + ', ' + isnull(dbo.fn_GetState(AD.numState),'') + ' ' + isnull(AD.vcPostalCode,'') + ' <br>' + isnull(dbo.fn_GetListItemName(AD.numCountry),'') AS vcFullAddress,
					isnull(AD.vcStreet,'') AS vcStreet,
					isnull(AD.vcCity,'') AS vcCity,
					isnull(dbo.fn_GetState(AD.numState),'') AS vcState,
					isnull(AD.vcPostalCode,'') AS vcPostalCode,
					isnull(dbo.fn_GetListItemName(AD.numCountry),'') AS vcCountry,
					ISNULL(AD.numCountry,0) numCountry,
					ISNULL(AD.numState,0) numState,
					ISNULL(AD.vcAddressName,'') AS vcAddressName,
            		(SELECT CMP.vcCompanyName FROM DivisionMaster DM JOIN CompanyInfo CMP ON DM.numCompanyId = CMP.numCompanyId and CMP.numDomainID = AD.numDomainID WHERE DM.numDivisionID = AD.numRecordID) AS vcCompanyName
					,(CASE 
						WHEN ISNULL(bitAltContact,0) = 1 
						THEN ISNULL(vcAltContact,'') 
						ELSE 
							(CASE 
								WHEN ISNULL(numContact,0) > 0 
								THEN 
									ISNULL((SELECT TOP 1 CONCAT(vcFirstName,' ',vcLastName) FROM AdditionalContactsInformation WHERE numDomainID=@numDomainID AND numContactId=numContact),'')
								ELSE 
									ISNULL((SELECT TOP 1 CONCAT(vcFirstName,' ',vcLastName) FROM AdditionalContactsInformation WHERE numDomainID=@numDomainID AND numDivisionId=AD.numRecordID AND ISNULL(bitPrimaryContact,0)=1),'')
								END
							)  END) AS vcContact
				 FROM 
					AddressDetails AD 
				 WHERE	
					AD.numDomainID=@numDomainID 
					AND AD.numAddressID = @numBillToAddressID
			END
			ELSE IF EXISTS (SELECT * FROM OpportunityAddress WHERE numOppID=@numOppId)
				SELECT TOP 1
					'<pre>' + isnull(AD.vcBillStreet,'') + '</pre>' + isnull(AD.vcBillCity,'') + ', ' + isnull(dbo.fn_GetState(AD.numBillState),'') + ' ' + isnull(AD.vcBillPostCode,'') + ' <br>' + isnull(dbo.fn_GetListItemName(AD.numBillCountry),'') AS vcFullAddress,
					isnull(AD.vcBillStreet,'') AS vcStreet,
					isnull(AD.vcBillCity,'') AS vcCity,
					isnull(dbo.fn_GetState(AD.numBillState),'') AS vcState,
					isnull(AD.vcBillPostCode,'') AS vcPostalCode,
					isnull(dbo.fn_GetListItemName(AD.numBillCountry),'') AS vcCountry,
					ISNULL(AD.vcAddressName,'') AS vcAddressName,
            		CASE WHEN LEN(AD.vcBillCompanyName) > 0 THEN AD.vcBillCompanyName ELSE ISNULL((SELECT CMP.vcCompanyName FROM CompanyInfo CMP WHERE CMP.numCompanyId = AD.numBillCompanyId and CMP.numDomainID = @numDomainID),'') END AS vcCompanyName
					,(CASE 
						WHEN ISNULL(bitAltBillingContact,0) = 1 
						THEN ISNULL(vcAltBillingContact,'') 
						ELSE 
							(CASE 
								WHEN ISNULL(numBillingContact,0) > 0 
								THEN 
									ISNULL((SELECT TOP 1 CONCAT(vcFirstName,' ',vcLastName) FROM AdditionalContactsInformation WHERE numDomainID=@numDomainID AND numContactId=numBillingContact),'')
								ELSE 
									ISNULL((SELECT TOP 1 CONCAT(vcFirstName,' ',vcLastName) FROM AdditionalContactsInformation INNER JOIN DivisionMaster ON AdditionalContactsInformation.numDivisionId=DivisionMaster.numDivisionID WHERE AdditionalContactsInformation.numDomainID=@numDomainID AND DivisionMaster.numCompanyID=AD.numBillCompanyId AND ISNULL(bitPrimaryContact,0)=1),'')
								END
							)  END) AS vcContact
				 FROM 
					OpportunityAddress AD 
				 WHERE	
					AD.numOppID = @numOppId
			ELSE
			BEGIN
				SELECT  '<pre>' + isnull(AD.vcStreet,'') + '</pre>' + isnull(AD.vcCity,'') + ', ' + isnull(dbo.fn_GetState(AD.numState),'') + ' ' + isnull(AD.vcPostalCode,'') + ' <br>' + isnull(dbo.fn_GetListItemName(AD.numCountry),'') AS vcFullAddress,
						isnull(AD.vcStreet,'') AS vcStreet,
						isnull(AD.vcCity,'') AS vcCity,
						isnull(dbo.fn_GetState(AD.numState),'') AS vcState,
						isnull(AD.vcPostalCode,'') AS vcPostalCode,
						isnull(dbo.fn_GetListItemName(AD.numCountry),'') AS vcCountry,
						ISNULL(AD.numCountry,0) numCountry,
						ISNULL(AD.numState,0) numState,
						ISNULL(AD.vcAddressName,'') AS vcAddressName,
            			CMP.vcCompanyName
						,(CASE 
							WHEN ISNULL(bitAltContact,0) = 1 
							THEN ISNULL(vcAltContact,'') 
							ELSE 
								(CASE 
									WHEN ISNULL(numContact,0) > 0 
									THEN 
										ISNULL((SELECT TOP 1 CONCAT(vcFirstName,' ',vcLastName) FROM AdditionalContactsInformation WHERE numDomainID=@numDomainID AND numContactId=numContact),'')
									ELSE 
										ISNULL((SELECT TOP 1 CONCAT(vcFirstName,' ',vcLastName) FROM AdditionalContactsInformation WHERE numDomainID=@numDomainID AND numDivisionId=AD.numRecordID AND ISNULL(bitPrimaryContact,0)=1),'')
									END
								)  END) AS vcContact
				 FROM AddressDetails AD 
				 JOIN [DivisionMaster] DM ON DM.numDivisionID = AD.numRecordID   
				 JOIN CompanyInfo CMP ON DM.numCompanyId = CMP.numCompanyId and CMP.numDomainID = AD.numDomainID  
				 WHERE AD.numDomainID=@numDomainID AND AD.numRecordID=@numDivisionID 
				 AND AD.tintAddressOf = 2 AND AD.tintAddressType = 1 AND AD.bitIsPrimary=1
			END

			--************Opp/Order Shipping Address************
			IF @numShipToAddressID > 0
			BEGIN
				SELECT  
					'<pre>' + isnull(AD.vcStreet,'') + '</pre>' + isnull(AD.vcCity,'') + ', ' + isnull(dbo.fn_GetState(AD.numState),'') + ' ' + isnull(AD.vcPostalCode,'') + ' <br>' + isnull(dbo.fn_GetListItemName(AD.numCountry),'') AS vcFullAddress,
					isnull(AD.vcStreet,'') AS vcStreet,
					isnull(AD.vcCity,'') AS vcCity,
					isnull(dbo.fn_GetState(AD.numState),'') AS vcState,
					isnull(AD.vcPostalCode,'') AS vcPostalCode,
					isnull(dbo.fn_GetListItemName(AD.numCountry),'') AS vcCountry,
					ISNULL(AD.numCountry,0) numCountry,
						ISNULL(AD.numState,0) numState,
					ISNULL(AD.vcAddressName,'') AS vcAddressName,
            		(SELECT CMP.vcCompanyName FROM DivisionMaster DM JOIN CompanyInfo CMP ON DM.numCompanyId = CMP.numCompanyId and CMP.numDomainID = AD.numDomainID WHERE DM.numDivisionID = AD.numRecordID) AS vcCompanyName,
					(CASE 
						WHEN ISNULL(bitAltContact,0) = 1 
						THEN ISNULL(vcAltContact,'') 
						ELSE 
							(CASE 
								WHEN ISNULL(numContact,0) > 0 
								THEN 
									ISNULL((SELECT TOP 1 CONCAT(vcFirstName,' ',vcLastName) FROM AdditionalContactsInformation WHERE numDomainID=@numDomainID AND numContactId=numContact),'')
								ELSE 
									ISNULL((SELECT TOP 1 CONCAT(vcFirstName,' ',vcLastName) FROM AdditionalContactsInformation WHERE numDomainID=@numDomainID AND numDivisionId=AD.numRecordID AND ISNULL(bitPrimaryContact,0)=1),'')
								END
							)  END) AS vcContact
				 FROM 
					AddressDetails AD 
				 WHERE	
					AD.numDomainID=@numDomainID 
					AND AD.numAddressID = @numShipToAddressID
			END
			ELSE IF EXISTS (SELECT * FROM OpportunityAddress WHERE numOppID=@numOppId)
				SELECT TOP 1
					'<pre>' + isnull(AD.vcShipStreet,'') + '</pre>' + isnull(AD.vcShipCity,'') + ', ' + isnull(dbo.fn_GetState(AD.numShipState),'') + ' ' + isnull(AD.vcShipPostCode,'') + ' <br>' + isnull(dbo.fn_GetListItemName(AD.numShipCountry),'') AS vcFullAddress,
					isnull(AD.vcShipStreet,'') AS vcStreet,
					isnull(AD.vcShipCity,'') AS vcCity,
					isnull(dbo.fn_GetState(AD.numShipState),'') AS vcState,
					isnull(AD.vcShipPostCode,'') AS vcPostalCode,
					isnull(dbo.fn_GetListItemName(AD.numShipCountry),'') AS vcCountry,
					ISNULL(AD.vcAddressName,'') AS vcAddressName,
            		CASE WHEN LEN(AD.vcShipCompanyName) > 0 THEN AD.vcShipCompanyName ELSE ISNULL((SELECT CMP.vcCompanyName FROM CompanyInfo CMP WHERE CMP.numCompanyId = AD.numShipCompanyId and CMP.numDomainID = @numDomainID),'') END AS vcCompanyName
					,(CASE 
						WHEN ISNULL(bitAltShippingContact,0) = 1 
						THEN ISNULL(vcAltShippingContact,'') 
						ELSE 
							(CASE 
								WHEN ISNULL(numShippingContact,0) > 0 
								THEN 
									ISNULL((SELECT TOP 1 CONCAT(vcFirstName,' ',vcLastName) FROM AdditionalContactsInformation WHERE numDomainID=@numDomainID AND numContactId=numShippingContact),'')
								ELSE 
									ISNULL((SELECT TOP 1 CONCAT(vcFirstName,' ',vcLastName) FROM AdditionalContactsInformation WHERE numDomainID=@numDomainID AND numDivisionId=AD.numShipCompanyID AND ISNULL(bitPrimaryContact,0)=1),'')
								END
							)  END) AS vcContact
				 FROM 
					OpportunityAddress AD 
				 WHERE	
					AD.numOppID = @numOppId
			ELSE
			BEGIN
				SELECT  '<pre>' + isnull(AD.vcStreet,'') + '</pre>' + isnull(AD.vcCity,'') + ', ' + isnull(dbo.fn_GetState(AD.numState),'') + ' ' + isnull(AD.vcPostalCode,'') + ' <br>' + isnull(dbo.fn_GetListItemName(AD.numCountry),'') AS vcFullAddress,
					isnull(AD.vcStreet,'') AS vcStreet,
					isnull(AD.vcCity,'') AS vcCity,
					isnull(dbo.fn_GetState(AD.numState),'') AS vcState,
					isnull(AD.vcPostalCode,'') AS vcPostalCode,
					isnull(dbo.fn_GetListItemName(AD.numCountry),'') AS vcCountry,
					ISNULL(AD.numCountry,0) numCountry,
						ISNULL(AD.numState,0) numState,
					ISNULL(AD.vcAddressName,'') AS vcAddressName,
					CMP.vcCompanyName
					,(CASE 
						WHEN ISNULL(bitAltContact,0) = 1 
						THEN ISNULL(vcAltContact,'') 
						ELSE 
							(CASE 
								WHEN ISNULL(numContact,0) > 0 
								THEN 
									ISNULL((SELECT TOP 1 CONCAT(vcFirstName,' ',vcLastName) FROM AdditionalContactsInformation WHERE numDomainID=@numDomainID AND numContactId=numContact),'')
								ELSE 
									ISNULL((SELECT TOP 1 CONCAT(vcFirstName,' ',vcLastName) FROM AdditionalContactsInformation WHERE numDomainID=@numDomainID AND numDivisionId=AD.numRecordID AND ISNULL(bitPrimaryContact,0)=1),'')
								END
							)  END) AS vcContact
				FROM AddressDetails AD 
				JOIN [DivisionMaster] DM ON DM.numDivisionID = AD.numRecordID   
				JOIN CompanyInfo CMP ON DM.numCompanyId = CMP.numCompanyId and CMP.numDomainID = AD.numDomainID  
				WHERE AD.numDomainID=@numDomainID AND AD.numRecordID=@numDivisionID 
				AND AD.tintAddressOf = 2 AND AD.tintAddressType = 2 AND AD.bitIsPrimary=1
			END
			
			 --************Employer Billing Address************
			SELECT  '<pre>' + isnull(AD.vcStreet,'') + '</pre>' + isnull(AD.vcCity,'') + ', ' + isnull(dbo.fn_GetState(AD.numState),'') + ' ' + isnull(AD.vcPostalCode,'') + ' <br>' + isnull(dbo.fn_GetListItemName(AD.numCountry),'') AS vcFullAddress,
					isnull(AD.vcStreet,'') AS vcStreet,
					isnull(AD.vcCity,'') AS vcCity,
					isnull(dbo.fn_GetState(AD.numState),'') AS vcState,
					isnull(AD.vcPostalCode,'') AS vcPostalCode,
					isnull(dbo.fn_GetListItemName(AD.numCountry),'') AS vcCountry,
					ISNULL(AD.numCountry,0) numCountry,
						ISNULL(AD.numState,0) numState,
					ISNULL(AD.vcAddressName,'') AS vcAddressName,vcCompanyName
					,(CASE 
						WHEN ISNULL(bitAltContact,0) = 1 
						THEN ISNULL(vcAltContact,'') 
						ELSE 
							(CASE 
								WHEN ISNULL(numContact,0) > 0 
								THEN 
									ISNULL((SELECT TOP 1 CONCAT(vcFirstName,' ',vcLastName) FROM AdditionalContactsInformation WHERE numDomainID=@numDomainID AND numContactId=numContact),'')
								ELSE 
									ISNULL((SELECT TOP 1 CONCAT(vcFirstName,' ',vcLastName) FROM AdditionalContactsInformation WHERE numDomainID=@numDomainID AND numDivisionId=AD.numRecordID AND ISNULL(bitPrimaryContact,0)=1),'')
								END
							)  
					END) AS vcContact
             FROM Domain D1 JOIN divisionmaster div1 ON D1.numDivisionID = div1.numDivisionID
				  JOIN companyinfo [Com1] ON com1.numCompanyID = div1.numCompanyID
                  JOIN dbo.AddressDetails AD ON AD.numDomainID = div1.numDomainID AND AD.numRecordID = div1.numDivisionID 
              WHERE  D1.numDomainID = @numDomainID
				AND AD.tintAddressOf = 2 AND AD.tintAddressType = 1 AND AD.bitIsPrimary=1


			--************Employer Shipping Address************
			SELECT  '<pre>' + isnull(AD.vcStreet,'') + '</pre>' + isnull(AD.vcCity,'') + ', ' + isnull(dbo.fn_GetState(AD.numState),'') + ' ' + isnull(AD.vcPostalCode,'') + ' <br>' + isnull(dbo.fn_GetListItemName(AD.numCountry),'') AS vcFullAddress,
					isnull(AD.vcStreet,'') AS vcStreet,
					isnull(AD.vcCity,'') AS vcCity,
					isnull(dbo.fn_GetState(AD.numState),'') AS vcState,
					isnull(AD.vcPostalCode,'') AS vcPostalCode,
					isnull(dbo.fn_GetListItemName(AD.numCountry),'') AS vcCountry,
					ISNULL(AD.numCountry,0) numCountry,
						ISNULL(AD.numState,0) numState,
					ISNULL(AD.vcAddressName,'') AS vcAddressName,vcCompanyName
					,(CASE 
						WHEN ISNULL(bitAltContact,0) = 1 
						THEN ISNULL(vcAltContact,'') 
						ELSE 
							(CASE 
								WHEN ISNULL(numContact,0) > 0 
								THEN 
									ISNULL((SELECT TOP 1 CONCAT(vcFirstName,' ',vcLastName) FROM AdditionalContactsInformation WHERE numDomainID=@numDomainID AND numContactId=numContact),'')
								ELSE 
									ISNULL((SELECT TOP 1 CONCAT(vcFirstName,' ',vcLastName) FROM AdditionalContactsInformation WHERE numDomainID=@numDomainID AND numDivisionId=AD.numRecordID AND ISNULL(bitPrimaryContact,0)=1),'')
								END
							)  
					END) AS vcContact
             FROM Domain D1 JOIN divisionmaster div1 ON D1.numDivisionID = div1.numDivisionID
				  JOIN companyinfo [Com1] ON com1.numCompanyID = div1.numCompanyID
                  JOIN dbo.AddressDetails AD ON AD.numDomainID = div1.numDomainID AND AD.numRecordID = div1.numDivisionID 
			 WHERE AD.numDomainID=@numDomainID 
			 AND AD.tintAddressOf = 2 AND AD.tintAddressType = 2 AND AD.bitIsPrimary=1 


			--************Vendor Billing Address************
			SELECT  '<pre>' + isnull(AD.vcStreet,'') + '</pre>' + isnull(AD.vcCity,'') + ', ' + isnull(dbo.fn_GetState(AD.numState),'') + ' ' + isnull(AD.vcPostalCode,'') + ' <br>' + isnull(dbo.fn_GetListItemName(AD.numCountry),'') AS vcFullAddress,
						isnull(AD.vcStreet,'') AS vcStreet,
						isnull(AD.vcCity,'') AS vcCity,
						isnull(dbo.fn_GetState(AD.numState),'') AS vcState,
						isnull(AD.vcPostalCode,'') AS vcPostalCode,
						isnull(dbo.fn_GetListItemName(AD.numCountry),'') AS vcCountry,
						ISNULL(AD.numCountry,0) numCountry,
						ISNULL(AD.numState,0) numState,
						ISNULL(AD.vcAddressName,'') AS vcAddressName,
            			CMP.vcCompanyName
						,(CASE 
							WHEN ISNULL(bitAltContact,0) = 1 
							THEN ISNULL(vcAltContact,'') 
							ELSE 
								(CASE 
									WHEN ISNULL(numContact,0) > 0 
									THEN 
										ISNULL((SELECT TOP 1 CONCAT(vcFirstName,' ',vcLastName) FROM AdditionalContactsInformation WHERE numDomainID=@numDomainID AND numContactId=numContact),'')
									ELSE 
										ISNULL((SELECT TOP 1 CONCAT(vcFirstName,' ',vcLastName) FROM AdditionalContactsInformation WHERE numDomainID=@numDomainID AND numDivisionId=AD.numRecordID AND ISNULL(bitPrimaryContact,0)=1),'')
									END
								)  END) AS vcContact
				 FROM AddressDetails AD 
				 JOIN [DivisionMaster] DM ON DM.numDivisionID = AD.numRecordID   
				 JOIN CompanyInfo CMP ON DM.numCompanyId = CMP.numCompanyId and CMP.numDomainID = AD.numDomainID  
				 WHERE AD.numDomainID=@numDomainID AND AD.numRecordID=@numDivisionID 
				 AND AD.tintAddressOf = 2 AND AD.tintAddressType = 1 AND AD.bitIsPrimary=1


			--************Vendor Shipping Address************
			IF @numVendorAddressID > 0
			BEGIN
				SELECT  
					'<pre>' + isnull(AD.vcStreet,'') + '</pre>' + isnull(AD.vcCity,'') + ', ' + isnull(dbo.fn_GetState(AD.numState),'') + ' ' + isnull(AD.vcPostalCode,'') + ' <br>' + isnull(dbo.fn_GetListItemName(AD.numCountry),'') AS vcFullAddress,
					isnull(AD.vcStreet,'') AS vcStreet,
					isnull(AD.vcCity,'') AS vcCity,
					isnull(dbo.fn_GetState(AD.numState),'') AS vcState,
					isnull(AD.vcPostalCode,'') AS vcPostalCode,
					isnull(dbo.fn_GetListItemName(AD.numCountry),'') AS vcCountry,
					ISNULL(AD.numCountry,0) numCountry,
						ISNULL(AD.numState,0) numState,
					ISNULL(AD.vcAddressName,'') AS vcAddressName,
            		(SELECT CMP.vcCompanyName FROM DivisionMaster DM JOIN CompanyInfo CMP ON DM.numCompanyId = CMP.numCompanyId and CMP.numDomainID = AD.numDomainID WHERE DM.numDivisionID = AD.numRecordID) AS vcCompanyName,
					(CASE 
						WHEN ISNULL(bitAltContact,0) = 1 
						THEN ISNULL(vcAltContact,'') 
						ELSE 
							(CASE 
								WHEN ISNULL(numContact,0) > 0 
								THEN 
									ISNULL((SELECT TOP 1 CONCAT(vcFirstName,' ',vcLastName) FROM AdditionalContactsInformation WHERE numDomainID=@numDomainID AND numContactId=numContact),'')
								ELSE 
									ISNULL((SELECT TOP 1 CONCAT(vcFirstName,' ',vcLastName) FROM AdditionalContactsInformation WHERE numDomainID=@numDomainID AND numDivisionId=AD.numRecordID AND ISNULL(bitPrimaryContact,0)=1),'')
								END
							)  END) AS vcContact
				 FROM 
					AddressDetails AD 
				 WHERE	
					AD.numDomainID=@numDomainID 
					AND AD.numAddressID = @numVendorAddressID
			END
			ELSE
			BEGIN
				SELECT  '<pre>' + isnull(AD.vcStreet,'') + '</pre>' + isnull(AD.vcCity,'') + ', ' + isnull(dbo.fn_GetState(AD.numState),'') + ' ' + isnull(AD.vcPostalCode,'') + ' <br>' + isnull(dbo.fn_GetListItemName(AD.numCountry),'') AS vcFullAddress,
					isnull(AD.vcStreet,'') AS vcStreet,
					isnull(AD.vcCity,'') AS vcCity,
					isnull(dbo.fn_GetState(AD.numState),'') AS vcState,
					isnull(AD.vcPostalCode,'') AS vcPostalCode,
					isnull(dbo.fn_GetListItemName(AD.numCountry),'') AS vcCountry,
					ISNULL(AD.numCountry,0) numCountry,
						ISNULL(AD.numState,0) numState,
					ISNULL(AD.vcAddressName,'') AS vcAddressName,
					CMP.vcCompanyName
					,(CASE 
						WHEN ISNULL(bitAltContact,0) = 1 
						THEN ISNULL(vcAltContact,'') 
						ELSE 
							(CASE 
								WHEN ISNULL(numContact,0) > 0 
								THEN 
									ISNULL((SELECT TOP 1 CONCAT(vcFirstName,' ',vcLastName) FROM AdditionalContactsInformation WHERE numDomainID=@numDomainID AND numContactId=numContact),'')
								ELSE 
									ISNULL((SELECT TOP 1 CONCAT(vcFirstName,' ',vcLastName) FROM AdditionalContactsInformation WHERE numDomainID=@numDomainID AND numDivisionId=AD.numRecordID AND ISNULL(bitPrimaryContact,0)=1),'')
								END
							)  END) AS vcContact
				FROM AddressDetails AD 
				JOIN [DivisionMaster] DM ON DM.numDivisionID = AD.numRecordID   
				JOIN CompanyInfo CMP ON DM.numCompanyId = CMP.numCompanyId and CMP.numDomainID = AD.numDomainID  
				WHERE AD.numDomainID=@numDomainID AND AD.numRecordID=@numDivisionID 
				AND AD.tintAddressOf = 2 AND AD.tintAddressType = 2 AND AD.bitIsPrimary=1
			END

	  END

	IF @tintOppType=1 
		BEGIN
			--************Opp/Order Billing Address************
			IF @numBillToAddressID > 0
			BEGIN
				SELECT  
					'<pre>' + isnull(AD.vcStreet,'') + '</pre>' + isnull(AD.vcCity,'') + ', ' + isnull(dbo.fn_GetState(AD.numState),'') + ' ' + isnull(AD.vcPostalCode,'') + ' <br>' + isnull(dbo.fn_GetListItemName(AD.numCountry),'') AS vcFullAddress,
					isnull(AD.vcStreet,'') AS vcStreet,
					isnull(AD.vcCity,'') AS vcCity,
					isnull(dbo.fn_GetState(AD.numState),'') AS vcState,
					isnull(AD.vcPostalCode,'') AS vcPostalCode,
					isnull(dbo.fn_GetListItemName(AD.numCountry),'') AS vcCountry,
					ISNULL(AD.numCountry,0) numCountry,
					ISNULL(AD.numState,0) numState,
					ISNULL(AD.vcAddressName,'') AS vcAddressName,
            		(SELECT CMP.vcCompanyName FROM DivisionMaster DM JOIN CompanyInfo CMP ON DM.numCompanyId = CMP.numCompanyId and CMP.numDomainID = AD.numDomainID WHERE DM.numDivisionID = AD.numRecordID) AS vcCompanyName
					,(CASE 
						WHEN ISNULL(bitAltContact,0) = 1 
						THEN ISNULL(vcAltContact,'') 
						ELSE 
							(CASE 
								WHEN ISNULL(numContact,0) > 0 
								THEN 
									ISNULL((SELECT TOP 1 CONCAT(vcFirstName,' ',vcLastName) FROM AdditionalContactsInformation WHERE numDomainID=@numDomainID AND numContactId=numContact),'')
								ELSE 
									ISNULL((SELECT TOP 1 CONCAT(vcFirstName,' ',vcLastName) FROM AdditionalContactsInformation WHERE numDomainID=@numDomainID AND numDivisionId=AD.numRecordID AND ISNULL(bitPrimaryContact,0)=1),'')
								END
							)  
					END) AS vcContact
				 FROM 
					AddressDetails AD 
				 WHERE	
					AD.numDomainID=@numDomainID 
					AND AD.numAddressID = @numBillToAddressID
			END
			ELSE
			BEGIN
				SELECT 
					'<pre>' + isnull(vcStreet,'') + '</pre>' + isnull(vcCity,'') + ', ' + isnull(vcState,'') + ' ' + isnull(vcPostalCode,'') + ' <br>' + isnull(vcCountry,'') AS vcFullAddress,
					isnull(vcStreet,'') AS vcStreet,
					isnull(vcCity,'') AS vcCity,
					isnull(vcState,'') AS vcState,
					isnull(vcPostalCode,'') AS vcPostalCode,
					isnull(vcCountry,'') AS vcCountry,
					ISNULL(vcAddressName,'') AS vcAddressName,isnull(vcCompanyName,'') AS vcCompanyName
					,vcContact
					,ISNULL(numCountry,0) numCountry
					,ISNULL(numState,0) numState
				FROM 
					fn_getOPPAddressDetails(@numOppId,@numDomainID,1) 
			END
		
			--************Opp/Order Shipping Address************
			IF (SELECT ISNULL(bitDropShipAddress,0)  FROM OpportunityMaster WHERE numOppId = @numOppId AND numDomainId = @numDomainID) = 1
			BEGIN
				SELECT 
					'<pre>' + isnull(vcStreet,'') + '</pre>' + ISNULL(vcCity,'') + ', ' + isnull(vcState,'') + ' ' + isnull(vcPostalCode,'') + ' <br>' + isnull(vcCountry,'') AS vcFullAddress,
					isnull(vcStreet,'') AS vcStreet,
					isnull(vcCity,'') AS vcCity,
					isnull(vcState,'') AS vcState,
					isnull(vcPostalCode,'') AS vcPostalCode,
					isnull(vcCountry,'') AS vcCountry,
					ISNULL(vcAddressName,'') AS vcAddressName,isnull(vcCompanyName,'') AS vcCompanyName
					,vcContact
					,ISNULL(numState,0) numState
					,ISNULL(numCountry,0) numCountry
				FROM 
					fn_getOPPAddressDetails(@numOppId,@numDomainID,2) 
			END
			ELSE IF ISNULL(@numOppBizDocID,0) > 0 AND (SELECT COUNT(*) FROM (SELECT DISTINCT numShipToAddressID FROM OpportunityBizDocItems OBDI INNER JOIN OpportunityItems OI ON OBDI.numOppItemID=OI.numoppitemtCode WHERE OBDI.numOppBizDocID=@numOppBizDocID AND ISNULL(OI.numShipToAddressID,0) > 0) TEMP) = 1 AND @tintShipType <> 2
			BEGIN
				SET @numShipToAddressID = (SELECT TOP 1 numShipToAddressID FROM OpportunityBizDocItems OBDI INNER JOIN OpportunityItems OI ON OBDI.numOppItemID=OI.numoppitemtCode WHERE OBDI.numOppBizDocID=@numOppBizDocID AND ISNULL(OI.numShipToAddressID,0) > 0)

				SELECT  
					'<pre>' + isnull(AD.vcStreet,'') + '</pre>' + isnull(AD.vcCity,'') + ', ' + isnull(dbo.fn_GetState(AD.numState),'') + ' ' + isnull(AD.vcPostalCode,'') + ' <br>' + isnull(dbo.fn_GetListItemName(AD.numCountry),'') AS vcFullAddress,
					isnull(AD.vcStreet,'') AS vcStreet,
					isnull(AD.vcCity,'') AS vcCity,
					isnull(dbo.fn_GetState(AD.numState),'') AS vcState,
					isnull(AD.vcPostalCode,'') AS vcPostalCode,
					isnull(dbo.fn_GetListItemName(AD.numCountry),'') AS vcCountry,
					ISNULL(AD.numCountry,0) numCountry,
						ISNULL(AD.numState,0) numState,
					ISNULL(AD.vcAddressName,'') AS vcAddressName,
            		(SELECT CMP.vcCompanyName FROM DivisionMaster DM JOIN CompanyInfo CMP ON DM.numCompanyId = CMP.numCompanyId and CMP.numDomainID = AD.numDomainID WHERE DM.numDivisionID = AD.numRecordID) AS vcCompanyName
					,(CASE 
						WHEN ISNULL(bitAltContact,0) = 1 
						THEN ISNULL(vcAltContact,'') 
						ELSE 
							(CASE 
								WHEN ISNULL(numContact,0) > 0 
								THEN 
									ISNULL((SELECT TOP 1 CONCAT(vcFirstName,' ',vcLastName) FROM AdditionalContactsInformation WHERE numDomainID=@numDomainID AND numContactId=numContact),'')
								ELSE 
									ISNULL((SELECT TOP 1 CONCAT(vcFirstName,' ',vcLastName) FROM AdditionalContactsInformation WHERE numDomainID=@numDomainID AND numDivisionId=AD.numRecordID AND ISNULL(bitPrimaryContact,0)=1),'')
								END
							)  
					END) AS vcContact
				 FROM 
					AddressDetails AD 
				 WHERE	
					AD.numDomainID=@numDomainID 
					AND AD.numAddressID = @numShipToAddressID
			END
			ELSE IF @numShipToAddressID > 0
			BEGIN
				SELECT  
					'<pre>' + isnull(AD.vcStreet,'') + '</pre>' + isnull(AD.vcCity,'') + ', ' + isnull(dbo.fn_GetState(AD.numState),'') + ' ' + isnull(AD.vcPostalCode,'') + ' <br>' + isnull(dbo.fn_GetListItemName(AD.numCountry),'') AS vcFullAddress,
					isnull(AD.vcStreet,'') AS vcStreet,
					isnull(AD.vcCity,'') AS vcCity,
					isnull(dbo.fn_GetState(AD.numState),'') AS vcState,
					isnull(AD.vcPostalCode,'') AS vcPostalCode,
					isnull(dbo.fn_GetListItemName(AD.numCountry),'') AS vcCountry,
					ISNULL(AD.numCountry,0) numCountry,
						ISNULL(AD.numState,0) numState,
					ISNULL(AD.vcAddressName,'') AS vcAddressName,
            		(SELECT CMP.vcCompanyName FROM DivisionMaster DM JOIN CompanyInfo CMP ON DM.numCompanyId = CMP.numCompanyId and CMP.numDomainID = AD.numDomainID WHERE DM.numDivisionID = AD.numRecordID) AS vcCompanyName
					,(CASE 
						WHEN ISNULL(bitAltContact,0) = 1 
						THEN ISNULL(vcAltContact,'') 
						ELSE 
							(CASE 
								WHEN ISNULL(numContact,0) > 0 
								THEN 
									ISNULL((SELECT TOP 1 CONCAT(vcFirstName,' ',vcLastName) FROM AdditionalContactsInformation WHERE numDomainID=@numDomainID AND numContactId=numContact),'')
								ELSE 
									ISNULL((SELECT TOP 1 CONCAT(vcFirstName,' ',vcLastName) FROM AdditionalContactsInformation WHERE numDomainID=@numDomainID AND numDivisionId=AD.numRecordID AND ISNULL(bitPrimaryContact,0)=1),'')
								END
							)  
					END) AS vcContact
				 FROM 
					AddressDetails AD 
				 WHERE	
					AD.numDomainID=@numDomainID 
					AND AD.numAddressID = @numShipToAddressID
			END
			ELSE
			BEGIN
				SELECT 
					'<pre>' + isnull(vcStreet,'') + '</pre>' + ISNULL(vcCity,'') + ', ' + isnull(vcState,'') + ' ' + isnull(vcPostalCode,'') + ' <br>' + isnull(vcCountry,'') AS vcFullAddress,
					isnull(vcStreet,'') AS vcStreet,
					isnull(vcCity,'') AS vcCity,
					isnull(vcState,'') AS vcState,
					isnull(vcPostalCode,'') AS vcPostalCode,
					isnull(vcCountry,'') AS vcCountry,
					ISNULL(vcAddressName,'') AS vcAddressName,isnull(vcCompanyName,'') AS vcCompanyName
					,vcContact
					,ISNULL(numCountry,0) numCountry
					,ISNULL(numState,0) numState
				FROM 
					fn_getOPPAddressDetails(@numOppId,@numDomainID,2) 
			END
	
			--************Employer Billing Address************ --(For SO take Domain Address)
			SELECT  '<pre>' + isnull(AD.vcStreet,'') + '</pre>' + isnull(AD.vcCity,'') + ', ' + isnull(dbo.fn_GetState(AD.numState),'') + ' ' + isnull(AD.vcPostalCode,'') + ' <br>' + isnull(dbo.fn_GetListItemName(AD.numCountry),'') AS vcFullAddress,
					isnull(AD.vcStreet,'') AS vcStreet,
					isnull(AD.vcCity,'') AS vcCity,
					isnull(dbo.fn_GetState(AD.numState),'') AS vcState,
					isnull(AD.vcPostalCode,'') AS vcPostalCode,
					isnull(dbo.fn_GetListItemName(AD.numCountry),'') AS vcCountry,
					ISNULL(AD.numCountry,0) numCountry,
						ISNULL(AD.numState,0) numState,
					ISNULL(AD.vcAddressName,'') AS vcAddressName,vcCompanyName
					,(CASE 
						WHEN ISNULL(bitAltContact,0) = 1 
						THEN ISNULL(vcAltContact,'') 
						ELSE 
							(CASE 
								WHEN ISNULL(numContact,0) > 0 
								THEN 
									ISNULL((SELECT TOP 1 CONCAT(vcFirstName,' ',vcLastName) FROM AdditionalContactsInformation WHERE numDomainID=@numDomainID AND numContactId=numContact),'')
								ELSE 
									ISNULL((SELECT TOP 1 CONCAT(vcFirstName,' ',vcLastName) FROM AdditionalContactsInformation WHERE numDomainID=@numDomainID AND numDivisionId=AD.numRecordID AND ISNULL(bitPrimaryContact,0)=1),'')
								END
							)  
					END) AS vcContact
             FROM Domain D1 JOIN divisionmaster div1 ON D1.numDivisionID = div1.numDivisionID
				  JOIN companyinfo [Com1] ON com1.numCompanyID = div1.numCompanyID
                  JOIN dbo.AddressDetails AD ON AD.numDomainID = div1.numDomainID AND AD.numRecordID = div1.numDivisionID 
              WHERE  D1.numDomainID = @numDomainID
				AND AD.tintAddressOf = 2 AND AD.tintAddressType = 1 AND AD.bitIsPrimary=1


			--************Employer Shipping Address************ --(For SO take Domain Address)
			SELECT  '<pre>' + isnull(AD.vcStreet,'') + '</pre>' + isnull(AD.vcCity,'') + ', ' + isnull(dbo.fn_GetState(AD.numState),'') + ' ' + isnull(AD.vcPostalCode,'') + ' <br>' + isnull(dbo.fn_GetListItemName(AD.numCountry),'') AS vcFullAddress,
					isnull(AD.vcStreet,'') AS vcStreet,
					isnull(AD.vcCity,'') AS vcCity,
					isnull(dbo.fn_GetState(AD.numState),'') AS vcState,
					isnull(AD.vcPostalCode,'') AS vcPostalCode,
					isnull(dbo.fn_GetListItemName(AD.numCountry),'') AS vcCountry,
					ISNULL(AD.numCountry,0) numCountry,
						ISNULL(AD.numState,0) numState,
					ISNULL(AD.vcAddressName,'') AS vcAddressName,vcCompanyName
					,(CASE 
						WHEN ISNULL(bitAltContact,0) = 1 
						THEN ISNULL(vcAltContact,'') 
						ELSE 
							(CASE 
								WHEN ISNULL(numContact,0) > 0 
								THEN 
									ISNULL((SELECT TOP 1 CONCAT(vcFirstName,' ',vcLastName) FROM AdditionalContactsInformation WHERE numDomainID=@numDomainID AND numContactId=numContact),'')
								ELSE 
									ISNULL((SELECT TOP 1 CONCAT(vcFirstName,' ',vcLastName) FROM AdditionalContactsInformation WHERE numDomainID=@numDomainID AND numDivisionId=AD.numRecordID AND ISNULL(bitPrimaryContact,0)=1),'')
								END
							)  
					END) AS vcContact
             FROM Domain D1 JOIN divisionmaster div1 ON D1.numDivisionID = div1.numDivisionID
				  JOIN companyinfo [Com1] ON com1.numCompanyID = div1.numCompanyID
                  JOIN dbo.AddressDetails AD ON AD.numDomainID = div1.numDomainID AND AD.numRecordID = div1.numDivisionID 
			 WHERE AD.numDomainID=@numDomainID 
			 AND AD.tintAddressOf = 2 AND AD.tintAddressType = 2 AND AD.bitIsPrimary=1


			 --************Customer Billing Address************
			SELECT  '<pre>' + isnull(AD.vcStreet,'') + '</pre>' + isnull(AD.vcCity,'') + ', ' + isnull(dbo.fn_GetState(AD.numState),'') + ' ' + isnull(AD.vcPostalCode,'') + ' <br>' + isnull(dbo.fn_GetListItemName(AD.numCountry),'') AS vcFullAddress,
						isnull(AD.vcStreet,'') AS vcStreet,
						isnull(AD.vcCity,'') AS vcCity,
						isnull(dbo.fn_GetState(AD.numState),'') AS vcState,
						isnull(AD.vcPostalCode,'') AS vcPostalCode,
						isnull(dbo.fn_GetListItemName(AD.numCountry),'') AS vcCountry,
						ISNULL(AD.numCountry,0) numCountry,
						ISNULL(AD.numState,0) numState,
						ISNULL(AD.vcAddressName,'') AS vcAddressName,
            			CMP.vcCompanyName
						,(CASE 
							WHEN ISNULL(bitAltContact,0) = 1 
							THEN ISNULL(vcAltContact,'') 
							ELSE 
								(CASE 
									WHEN ISNULL(numContact,0) > 0 
									THEN 
										ISNULL((SELECT TOP 1 CONCAT(vcFirstName,' ',vcLastName) FROM AdditionalContactsInformation WHERE numDomainID=@numDomainID AND numContactId=numContact),'')
									ELSE 
										ISNULL((SELECT TOP 1 CONCAT(vcFirstName,' ',vcLastName) FROM AdditionalContactsInformation WHERE numDomainID=@numDomainID AND numDivisionId=AD.numRecordID AND ISNULL(bitPrimaryContact,0)=1),'')
									END
								)  END) AS vcContact
				 FROM AddressDetails AD 
				 JOIN [DivisionMaster] DM ON DM.numDivisionID = AD.numRecordID   
				 JOIN CompanyInfo CMP ON DM.numCompanyId = CMP.numCompanyId and CMP.numDomainID = AD.numDomainID  
				 WHERE AD.numDomainID=@numDomainID AND AD.numRecordID=@numDivisionID 
				 AND AD.tintAddressOf = 2 AND AD.tintAddressType = 1 AND AD.bitIsPrimary=1


			--************Customer Shipping Address************
			SELECT  '<pre>' + isnull(AD.vcStreet,'') + '</pre>' + isnull(AD.vcCity,'') + ', ' + isnull(dbo.fn_GetState(AD.numState),'') + ' ' + isnull(AD.vcPostalCode,'') + ' <br>' + isnull(dbo.fn_GetListItemName(AD.numCountry),'') AS vcFullAddress,
					isnull(AD.vcStreet,'') AS vcStreet,
					isnull(AD.vcCity,'') AS vcCity,
					isnull(dbo.fn_GetState(AD.numState),'') AS vcState,
					isnull(AD.vcPostalCode,'') AS vcPostalCode,
					isnull(dbo.fn_GetListItemName(AD.numCountry),'') AS vcCountry,
					ISNULL(AD.numCountry,0) numCountry,
						ISNULL(AD.numState,0) numState,
					ISNULL(AD.vcAddressName,'') AS vcAddressName,
					CMP.vcCompanyName
					,(CASE 
						WHEN ISNULL(bitAltContact,0) = 1 
						THEN ISNULL(vcAltContact,'') 
						ELSE 
							(CASE 
								WHEN ISNULL(numContact,0) > 0 
								THEN 
									ISNULL((SELECT TOP 1 CONCAT(vcFirstName,' ',vcLastName) FROM AdditionalContactsInformation WHERE numDomainID=@numDomainID AND numContactId=numContact),'')
								ELSE 
									ISNULL((SELECT TOP 1 CONCAT(vcFirstName,' ',vcLastName) FROM AdditionalContactsInformation WHERE numDomainID=@numDomainID AND numDivisionId=AD.numRecordID AND ISNULL(bitPrimaryContact,0)=1),'')
								END
							)  END) AS vcContact
				FROM AddressDetails AD 
				JOIN [DivisionMaster] DM ON DM.numDivisionID = AD.numRecordID   
				JOIN CompanyInfo CMP ON DM.numCompanyId = CMP.numCompanyId and CMP.numDomainID = AD.numDomainID  
				WHERE AD.numDomainID=@numDomainID AND AD.numRecordID=@numDivisionID 
				AND AD.tintAddressOf = 2 AND AD.tintAddressType = 2 AND AD.bitIsPrimary=1
	  END