/****** Object:  StoredProcedure [dbo].[USP_GetOpportunitySalesTemplate]    Script Date: 09/01/2009 01:16:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- EXEC USP_GetSalesTemplateItemDtl 699851,72
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetSalesTemplateItemDtl')
DROP PROCEDURE USP_GetSalesTemplateItemDtl
GO
CREATE PROCEDURE [dbo].[USP_GetSalesTemplateItemDtl]
	--@numSalesTemplateItemID NUMERIC(18) = 0,
    --@numItemCode NUMERIC(9)  = 0,
	@numSalesTemplateID NUMERIC(18) = 0,
    @numDomainID NUMERIC(9)
AS
BEGIN
	SELECT numSalesTemplateItemID, STI.numItemCode, vcItemName, vcSKU, txtItemDesc 
	FROM SalesTemplateItems STI
	JOIN Item I ON sti.numItemCode = i.numItemCode AND sti.numDomainID = I.numDomainID 
	WHERE STI.numSalesTemplateID = @numSalesTemplateID AND STI.numDomainID = @numDomainID
END