GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_DeleteShippingRule' ) 
    DROP PROCEDURE USP_DeleteShippingRule
GO
CREATE PROCEDURE USP_DeleteShippingRule
    @numRuleID NUMERIC(9),
    @numDomainID NUMERIC(9)
AS 
BEGIN

	DELETE FROM dbo.PromotionOfferContacts WHERE numProId=@numRuleID AND tintRecordType=2
	DELETE FROM dbo.PromotionOfferItems WHERE numProId=@numRuleID AND tintRecordType=2
    
    DELETE FROM dbo.ShippingRuleStateList WHERE numRuleID = @numRuleID 
    DELETE FROM dbo.ShippingServiceTypes WHERE numRuleID =  @numRuleID
  
    DELETE  FROM dbo.ShippingRules
    WHERE   numRuleID = @numRuleID
            AND numDomainID = @numDomainID
END