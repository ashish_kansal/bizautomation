SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetUserReportList')
DROP PROCEDURE USP_GetUserReportList
GO
CREATE PROCEDURE [dbo].[USP_GetUserReportList]
@numDomainID as numeric(9)=0,         
@numUserCntID as numeric(9)=0          
as          
select URL.numReportID,vcFileName,RPtHeading,RptDesc,tintReportType from PageMaster           
join ReportList on numPageID=NumID          
join UserReportList URL on RptID=URL.numReportID          
where numModuleID=8 AND URL.numDomainID=@numDomainID AND URL.numUserCntID=@numUserCntID and URL.tintReportType =0 AND [bitEnable]=1
    
UNION
    
select URL.numReportID,'frmCustomReportRun.aspx?ReptID='+convert(varchar(10),URL.numReportID) as vcFileName ,    
vcReportName as RPtHeading,vcReportDescription as RptDesc,URL.tintReportType    
 from ReportListMaster RLM    
join UserReportList URL on RLM.numReportID=URL.numReportID          
WHERE URL.numDomainID=@numDomainID AND URL.numUserCntID=@numUserCntID and URL.tintReportType =1

GO
