GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_MassSalesFulfillmentConfiguration_GetColumnValue')
DROP PROCEDURE USP_MassSalesFulfillmentConfiguration_GetColumnValue
GO
CREATE PROCEDURE [dbo].[USP_MassSalesFulfillmentConfiguration_GetColumnValue]
(
    @numDomainID AS NUMERIC(18,0),
	@vcDbColumnName AS VARCHAR(300)
)
AS 
BEGIN
	IF @vcDbColumnName = 'bitAutoWarehouseSelection'
	BEGIN
		SELECT ISNULL(bitAutoWarehouseSelection,0) AS bitAutoWarehouseSelection FROM MassSalesFulfillmentConfiguration WHERE numDomainId=@numDomainID
	END
	ELSE IF @vcDbColumnName = 'bitBOOrderStatus'
	BEGIN
		SELECT ISNULL(bitBOOrderStatus,0) AS bitBOOrderStatus FROM MassSalesFulfillmentConfiguration WHERE numDomainId=@numDomainID
	END
END
GO