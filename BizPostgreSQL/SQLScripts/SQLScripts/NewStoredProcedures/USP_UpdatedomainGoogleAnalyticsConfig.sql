/****** Object:  StoredProcedure [dbo].[USP_UpdatedomainDetails]    Script Date: 05/07/2009 21:56:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj                                      
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_UpdatedomainGoogleAnalyticsConfig')
DROP PROCEDURE USP_UpdatedomainGoogleAnalyticsConfig
GO
CREATE PROCEDURE [dbo].[USP_UpdatedomainGoogleAnalyticsConfig]                                      
@numDomainID as numeric(9)=0,                                      
@vcGAUserEmailId as VARCHAR(50)='',
@vcGAUserPassword as VARCHAR(50)='',
@vcGAUserProfileId as VARCHAR(50)=''
as                                      
                                      
 update Domain                                       
   set        
vcGAUserEMail = @vcGAUserEmailId,
vcGAUserPassword = @vcGAUserPassword,
vcGAUserProfileId = @vcGAUserProfileId
 where numDomainId=@numDomainID

