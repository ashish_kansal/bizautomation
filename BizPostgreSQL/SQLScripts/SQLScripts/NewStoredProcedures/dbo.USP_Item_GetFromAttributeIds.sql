GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_Item_GetFromAttributeIds')
DROP PROCEDURE USP_Item_GetFromAttributeIds
GO
CREATE PROCEDURE [dbo].[USP_Item_GetFromAttributeIds]
(
	@numDomainID NUMERIC(18,0)
	,@numItemCode AS NUMERIC(18,0)=0    
	,@vcAttributeIDs VARCHAR(500)    
)
AS
BEGIN  
	DECLARE @vcItemName VARCHAR(300) = ''
	DECLARE @numItemGroup NUMERIC(18,0)
	SELECT @vcItemName=ISNULL(vcItemName,''),@numItemGroup=ISNULL(numItemGroup,0) FROM Item WHERE numItemCode=@numItemCode AND numDomainID=@numDomainID


	SELECT TOP 1
		numItemCode
		,(SELECT TOP 1 numWarehouseItemID FROM WareHouseItems WHERE numDomainID=@numDomainID AND numItemID=Item.numItemCode AND ISNULL(numWLocationID,0) <> -1) numWarehouseItemID
	FROM
		Item
	WHERE
		numDomainID=@numDomainID
		AND vcItemName = @vcItemName
		AND bitMatrix=1
		AND numItemGroup=@numItemGroup
		AND Stuff((SELECT N',' + CONCAT(FLD_ID,':',FLD_Value) FROM ItemAttributes WHERE numDomainID=@numDomainID AND numItemCode=Item.numItemCode FOR XML PATH(''),TYPE).value('text()[1]','nvarchar(max)'),1,1,N'') = @vcAttributeIDs
END