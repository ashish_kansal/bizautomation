GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_ValidateBizDocSequenceId' ) 
    DROP PROCEDURE USP_ValidateBizDocSequenceId
GO

CREATE PROCEDURE USP_ValidateBizDocSequenceId
@numDomainID NUMERIC(9),
@numOppId NUMERIC(9),
@numOppBizDocsId NUMERIC(9),
@numBizDocId NUMERIC(9),
@numSequenceId VARCHAR(50)
AS 
BEGIN
	SELECT OBD.numOppBizDocsId,OBD.numOppId FROM dbo.OpportunityMaster OM JOIN dbo.OpportunityBizDocs OBD ON OM.numOppId=obd.numOppId
	WHERE om.numDomainId = @numDomainID 
	AND obd.numBizDocId = @numBizDocId 
	--AND om.numOppId != @numOppId 
	AND (obd.numOppBizDocsId = ISNULL(@numOppBizDocsId,0) OR @numOppBizDocsId = 0)
	AND obd.numSequenceId=@numSequenceId
END