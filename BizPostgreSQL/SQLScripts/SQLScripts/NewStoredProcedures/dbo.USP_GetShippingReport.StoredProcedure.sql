/****** Object:  StoredProcedure [dbo].[USP_GetShippingReport]    Script Date: 05/07/2009 17:34:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--SELECT * FROM ShippingReport
--SELECT * FROM ShippingReportItems
-- USP_GetShippingReport 1,60754,792
-- exec USP_GetShippingReport @numDomainID=72,@numOppBizDocId=5465,@ShippingReportId=4
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_GetShippingReport' ) 
    DROP PROCEDURE USP_GetShippingReport
GO
CREATE PROCEDURE [dbo].[USP_GetShippingReport]
    @numDomainID NUMERIC(9),
    @numOppBizDocId NUMERIC(9),
    @ShippingReportId NUMERIC(9)
AS 
    BEGIN
  
        SELECT  SRI.numItemCode,
                I.vcItemName,
                I.vcModelID,
                CASE WHEN LEN(BDI.vcItemDesc) > 100
                     THEN CONVERT(VARCHAR(100), BDI.vcItemDesc) + '..'
                     ELSE BDI.vcItemDesc
                END vcItemDesc,
                dbo.FormatedDateFromDate(SRI.dtDeliveryDate, @numDomainID) dtDeliveryDate,
                SRI.tintServiceType,
                ISNULL(I.fltWeight, 0) AS [fltTotalWeight],
                ISNULL(I.[fltHeight], 0) AS [fltHeight],
                ISNULL(I.[fltLength], 0) AS [fltLength],
                ISNULL(I.[fltWidth], 0) AS [fltWidth],
                ISNULL(SRI.monShippingRate,(SELECT monTotAmount FROM dbo.OpportunityItems 
											WHERE SR.numOppID = dbo.OpportunityItems.numOppId
											AND numItemCode = (SELECT numShippingServiceITemID FROM dbo.Domain WHERE numDomainId = @numDomainId))) [monShippingRate],
                SB.vcShippingLabelImage,
                SB.vcTrackingNumber,
                SRI.ShippingReportItemId,
                SRI.intNoOfBox,
                SR.[vcValue3],
                SR.[vcValue4],
                CASE WHEN ISNUMERIC(SR.vcFromState) = 1
                     THEN ( SELECT  vcStateCode
                            FROM    ShippingStateMaster
                            WHERE   vcStateName = ( SELECT TOP 1
                                                            vcState
                                                    FROM    State
                                                    WHERE   numStateID = SR.vcFromState
                                                  )
                                    AND [numShipCompany] = SR.[numShippingCompany]
                          )
                     ELSE ''
                END vcFromState,
                CASE WHEN ISNUMERIC(SR.vcFromCountry) = 1
                     THEN ( SELECT  vcCountryCode
                            FROM    ShippingCountryMaster
                            WHERE   vcCountryName = ( SELECT TOP 1
                                                                vcData
                                                      FROM      ListDetails
                                                      WHERE     numListItemID = SR.vcFromCountry
                                                    )
                                    AND [numShipCompany] = SR.[numShippingCompany]
                          )
                     ELSE ''
                END vcFromCountry,
                CASE WHEN ISNUMERIC(SR.vcToState) = 1
                     THEN ( SELECT  vcStateCode
                            FROM    ShippingStateMaster
                            WHERE   vcStateName = ( SELECT TOP 1
                                                            vcState
                                                    FROM    State
                                                    WHERE   numStateID = SR.vcToState
                                                  )
                                    AND [numShipCompany] = SR.[numShippingCompany]
                          )
                     ELSE ''
                END vcToState,
                CASE WHEN ISNUMERIC(SR.vcToCountry) = 1
                     THEN ( SELECT  vcCountryCode
                            FROM    ShippingCountryMaster
                            WHERE   vcCountryName = ( SELECT TOP 1
                                                                vcData
                                                      FROM      ListDetails
                                                      WHERE     numListItemID = SR.vcToCountry
                                                    )
                                    AND [numShipCompany] = SR.[numShippingCompany]
                          )
                     ELSE ''
                END vcToCountry,
                SR.vcToZip,
                SR.vcFromZip,
                SR.[numShippingCompany],
                SB.[numBoxID],
                SB.[vcBoxName],
                BDI.numOppBizDocItemID,
                ISNULL(SR.tintPayorType,0) tintPayorType,
                ISNULL(SR.vcPayorAccountNo, '') vcPayorAccountNo,
				SR.vcPayerStreet,
				SR.vcPayerCity,
                ISNULL(SR.numPayorCountry, 0) numPayorCountry,
				ISNULL(SR.numPayerState, 0) numPayerState,
                ( SELECT    vcCountryCode
                  FROM      ShippingCountryMaster
                  WHERE     vcCountryName = ( SELECT TOP 1
                                                        vcData
                                              FROM      ListDetails
                                              WHERE     numListItemID = SR.numPayorCountry
                                            )
                            AND [numShipCompany] = SR.[numShippingCompany]
                ) vcPayorCountryCode,
				( SELECT  vcStateCode
                            FROM    ShippingStateMaster
                            WHERE   vcStateName = ( SELECT TOP 1
                                                            vcState
                                                    FROM    State
                                                    WHERE   numStateID = SR.numPayerState
                                                  )
                                    AND [numShipCompany] = SR.[numShippingCompany]
                ) vcPayorStateCode,
                SR.vcPayorZip,
                SR.vcFromCity,
                SR.vcFromAddressLine1,
                SR.vcFromAddressLine2,
                ISNULL(SR.vcFromCountry,0) [numFromCountry],
                ISNULL(SR.vcFromState,0) [numFromState],
                SR.vcToCity,
                SR.vcToAddressLine1,
                SR.vcToAddressLine2,
                ISNULL(SR.vcToCountry,0) [numToCountry],
                ISNULL(SR.vcToState,0) [numToState],
                SRI.intBoxQty,
                ISNULL(SB.numServiceTypeID, 0) AS [numServiceTypeID],
                ISNULL(SR.vcFromCompany, '') AS vcFromCompany,
                ISNULL(SR.vcFromName, '') AS vcFromName,
                ISNULL(SR.vcFromPhone, '') AS vcFromPhone,
                ISNULL(SR.vcToCompany, '') AS vcToCompany,
                ISNULL(SR.vcToName, '') AS vcToName,
                ISNULL(SR.vcToPhone, '') AS vcToPhone,
                ISNULL(SB.fltDimensionalWeight, 0) AS [fltDimensionalWeight],
                dbo.fn_GetContactName(SR.numCreatedBy) + ' : '
                + CONVERT(VARCHAR(20), SR.dtCreateDate) AS [CreatedDetails],
                CASE WHEN SR.numShippingCompany = 91 THEN 'Fedex'
                     WHEN SR.numShippingCompany = 88 THEN 'UPS'
                     WHEN SR.numShippingCompany = 90 THEN 'USPS'
                     ELSE 'Other'
                END AS [vcShipCompany],
                CAST(ROUND(( ISNULL(SRI.fltTotalWeight,0) * intBoxQty ), 2) AS DECIMAL(9, 2)) AS [fltTotalRegularWeight],
                CAST(ROUND(ISNULL(fltDimensionalWeight,0), 2) AS DECIMAL(9, 2)) AS [fltTotalDimensionalWeight],
                ISNULL(monTotAmount, 0) AS [monTotAmount],
                ISNULL(bitFromResidential,0) [bitFromResidential],
                ISNULL(bitToResidential,0) [bitToResidential],
                ISNULL(IsCOD,0) [IsCOD],
				ISNULL(IsDryIce,0) [IsDryIce],
				ISNULL(IsHoldSaturday,0) [IsHoldSaturday],
				ISNULL(IsHomeDelivery,0) [IsHomeDelivery],
				ISNULL(IsInsideDelevery,0) [IsInsideDelevery],
				ISNULL(IsInsidePickup,0) [IsInsidePickup],
				ISNULL(IsReturnShipment,0) [IsReturnShipment],
				ISNULL(IsSaturdayDelivery,0) [IsSaturdayDelivery],
				ISNULL(IsSaturdayPickup,0) [IsSaturdayPickup],
				ISNULL(IsAdditionalHandling,0) [IsAdditionalHandling],
				ISNULL(IsLargePackage,0) [IsLargePackage],
				ISNULL(vcCODType,0) [vcCODType],
				SR.numOppID,
				ISNULL(numCODAmount,0) [numCODAmount],
				ISNULL([SR].[numTotalInsuredValue],0) AS [numTotalInsuredValue],
				ISNULL([SR].[numTotalCustomsValue],0) AS [numTotalCustomsValue],
				ISNULL(tintSignatureType,0) AS tintSignatureType,
				OpportunityMaster.numShipFromWarehouse,
				ISNULL(vcDeliveryConfirmation,0) AS vcDeliveryConfirmation,
				ISNULL(SR.vcDescription,'') AS vcDescription
        FROM    ShippingReport AS SR
				INNER JOIN OpportunityMaster ON SR.numOppID = OpportunityMaster.numOppId
                INNER JOIN ShippingBox SB ON SB.numShippingReportId = SR.numShippingReportId
                INNER JOIN ShippingReportItems AS SRI ON SR.numShippingReportId = SRI.numShippingReportId
                                                         AND SRI.numBoxID = SB.numBoxID
                INNER JOIN OpportunityBizDocItems BDI ON SRI.numItemCode = BDI.numItemCode
                                                         AND SR.numOppBizDocId = BDI.numOppBizDocID
                INNER JOIN Item I ON BDI.numItemCode = I.numItemCode
        WHERE   SR.[numOppBizDocId] = @numOppBizDocId
                AND SR.numDomainId = @numDomainID
                AND SR.[numShippingReportId] = @ShippingReportId
--           AND (SB.bitIsMasterTrackingNo = 1 OR SB.bitIsMasterTrackingNo IS NULL)
           
        SELECT  vcBizDocID
        FROM    [OpportunityBizDocs]
        WHERE   [numOppBizDocsId] = @numOppBizDocId

    END
