
GO
/****** Object:  StoredProcedure [dbo].[usp_DeleteMilestoneStage]    Script Date: 09/21/2010 11:43:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_DeleteMilestoneStage')
DROP PROCEDURE usp_DeleteMilestoneStage
GO
CREATE PROCEDURE [dbo].[usp_DeleteMilestoneStage]
  @numDomainid NUMERIC = 0,
    @numProjectID NUMERIC = 0 ,
    @numStageDetailsID NUMERIC = 0,
   @tintModeType TINYINT=-1   

as

IF EXISTS ( SELECT  *
            FROM    dbo.ProjectsOpportunities
            WHERE   numDomainId = @numDomainid
                    AND @numProjectID = ( CASE @tintModeType
                                            WHEN 0 THEN numOppId
                                            WHEN 1 THEN numProId
                                          END ) AND numStageID= @numStageDetailsID ) 
    BEGIN
        RAISERROR ( 'DEPENDANT', 16, 1 );
        RETURN;
    END	
    
DELETE FROM dbo.Comments WHERE numStageID =@numStageDetailsID

delete from StageDependency where numStageDetailID=@numStageDetailsID or 
numDependantOnID=@numStageDetailsID


DELETE FROM dbo.StageAccessDetail WHERE numStageDetailsId=@numStageDetailsID and 
						@numProjectID=(case @tintModeType when 0 then numOppID 
										when 1 then numProjectID end)

DELETE FROM dbo.DocumentWorkflow WHERE numDocID IN 
	(SELECT numGenericDocID FROM dbo.GenericDocuments WHERE vcDocumentSection='PS' AND
	numRecID IN (SELECT numStageDetailsId FROM stagepercentagedetails WHERE numStageDetailsId=@numStageDetailsID and 
						@numProjectID=(case @tintModeType when 0 then numOppID 
										when 1 then numProjectID end) AND numDomainID = @numDomainID) AND numDomainID=@numDomainid) 

DELETE FROM dbo.GenericDocuments WHERE vcDocumentSection='PS' AND 
	numRecID IN (SELECT numStageDetailsId FROM stagepercentagedetails WHERE numStageDetailsId=@numStageDetailsID and 
						@numProjectID=(case @tintModeType when 0 then numOppID 
										when 1 then numProjectID end) AND numDomainID = @numDomainID) AND numDomainID=@numDomainid
	
delete from stagepercentagedetails where  
@numProjectID=(case @tintModeType when 0 then numOppID 
				when 1 then numProjectID end)
	 and numStageDetailsID=@numStageDetailsID
and numDomainId=@numDomainId 

