GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ChartofAccountsDetail')
DROP PROCEDURE USP_ChartofAccountsDetail
GO
-- EXEC USP_ChartofAccountsDetail 169,'2015-10-24 11:03:12.857','6020'
CREATE PROCEDURE [dbo].[USP_ChartofAccountsDetail]
	@numDomainID NUMERIC(9),
    @dtToDate DATETIME,
    @vcAccountId AS VARCHAR(500)
    
AS 
    BEGIN
    
    	/*RollUp of Sub Accounts */
		SELECT  COA2.numAccountId INTO #Temp /*,COA1.vcAccountCode ,COA2.vcAccountCode*/
		FROM    dbo.Chart_Of_Accounts COA1
				INNER JOIN dbo.Chart_Of_Accounts COA2 
				ON COA1.numDomainId = COA2.numDomainId
				AND COA2.vcAccountCode LIKE COA1.vcAccountCode  + '%'
		WHERE   COA1.numAccountId IN(SELECT  cast(ID AS NUMERIC(9)) FROM dbo.SplitIDs(@vcAccountId, ','))
				AND COA1.numDomainId = @numDomainID
		SELECT @vcAccountId = ISNULL( STUFF((SELECT ',' + CAST(numAccountID AS VARCHAR(10)) FROM #Temp FOR XML PATH('')),1, 1, '') , '') 

		create table #Accounts(ID NUMERIC(9));
		insert into #Accounts 
		SELECT CAST(ID AS NUMERIC(9))FROM dbo.SplitIDs(@vcAccountId, ',')

		SELECT     
			h.numDomainId,  
			coa.numAccountId,  
			CompanyName = ci.vcCompanyName,  
			dm.numDivisionID,
			0 AS Opening,
			CONVERT(VARCHAR(20), SUM(ISNULL(d.numDebitAmt, 0))) TotalDebit,
			CONVERT(VARCHAR(20), SUM(ISNULL(d.numCreditAmt, 0))) TotalCredit,
			(SUM(ISNULL(d.numDebitAmt, 0)) -  SUM(ISNULL(d.numCreditAmt, 0)))  AS Closing,
			COA.numParntAcntTypeId
			
		FROM dbo.General_Journal_Header h 
		INNER JOIN dbo.General_Journal_Details d 
			ON h.numJournal_Id = d.numJournalId 
		LEFT JOIN dbo.Chart_Of_Accounts coa
			ON d.numChartAcntId = coa.numAccountId 
		LEFT JOIN dbo.DivisionMaster dm 
		LEFT JOIN dbo.CompanyInfo ci 
			ON dm.numCompanyID = ci.numCompanyId 
			ON d.numCustomerId = dm.numDivisionID 
		WHERE
			h.numDomainId = @numDomainID 
			AND ISNULL(dm.numDivisionID,0) > 0
			AND coa.numAccountId IN (SELECT ID FROM #Accounts)

		GROUP BY h.[numDomainId],  
						coa.[numAccountId],  
						ci.vcCompanyName,  
						dm.numDivisionID,  
						COA.numParntAcntTypeId  
		HAVING
			ROUND((SUM(ISNULL(d.numDebitAmt, 0)) -  SUM(ISNULL(d.numCreditAmt, 0))),2) <> 0
		order by  ci.vcCompanyName

		drop table #accounts



    END
