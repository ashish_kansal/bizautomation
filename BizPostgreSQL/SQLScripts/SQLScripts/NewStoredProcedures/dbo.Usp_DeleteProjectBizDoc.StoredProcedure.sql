
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='Usp_DeleteProjectBizDoc')
DROP PROCEDURE Usp_DeleteProjectBizDoc
GO
CREATE PROCEDURE Usp_DeleteProjectBizDoc
	@numProjOppID NUMERIC(9)
AS 
BEGIN
	DELETE FROM [ProjectsOpportunities] WHERE numProjOppID = @numProjOppID
END