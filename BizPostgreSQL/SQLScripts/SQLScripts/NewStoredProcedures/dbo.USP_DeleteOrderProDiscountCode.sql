SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
------- Created by Priya 

IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_deleteorderprodiscountcode')
DROP PROCEDURE usp_deleteorderprodiscountcode
GO
CREATE PROCEDURE [dbo].[USP_DeleteOrderProDiscountCode]
    @numProId AS NUMERIC(9) = 0,   
    @numDomainID AS NUMERIC(9) = 0,
	@numDiscountId AS NUMERIC(9),
	@numOppId AS NUMERIC(9),
	@numDivisionId AS NUMERIC(9)
AS 
BEGIN TRY
BEGIN TRANSACTION	
   
	   DELETE FROM DiscountCodes
	   WHERE numDiscountId = @numDiscountId AND numPromotionID = @numProId

	   DECLARE @intDiscountUsage AS INTEGER

	   SET @intDiscountUsage = (SELECT COUNT(intCodeUsed) FROM DiscountCodeUsage WHERE numDivisionId = @numDivisionId AND numDiscountId = @numDiscountId)

	   IF @intDiscountUsage > 0
	   BEGIN

		 UPDATE DiscountCodeUsage
			SET intCodeUsed = @intDiscountUsage - 1
		 WHERE numDivisionId = @numDivisionId AND numDiscountId = @numDiscountId

	   END

	   UPDATE OpportunityMaster 
			SET numDiscountID = NULL
		WHERE numOppId = @numOppId AND numDivisionId = @numDivisionId AND numDiscountId = @numDiscountId AND numDomainId = @numDomainID

		DECLARE @numDiscountLineItem AS NUMERIC
		SET @numDiscountLineItem = (SELECT ISNULL(numShippingServiceItemID,0) FROM Domain WHERE numDomainId = @numDomainID)

		DELETE FROM OpportunityItems		
		WHERE numItemCode = @numDiscountLineItem AND numOppId = @numOppId
   	
		
COMMIT
END TRY
BEGIN CATCH
	DECLARE @ErrorMessage NVARCHAR(4000)
	DECLARE @ErrorNumber INT
	DECLARE @ErrorSeverity INT
	DECLARE @ErrorState INT
	DECLARE @ErrorLine INT
	DECLARE @ErrorProcedure NVARCHAR(200)

	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;

	SELECT 
		@ErrorMessage = ERROR_MESSAGE(),
		@ErrorNumber = ERROR_NUMBER(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE(),
		@ErrorLine = ERROR_LINE(),
		@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

	RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);
END CATCH
      
  
GO
