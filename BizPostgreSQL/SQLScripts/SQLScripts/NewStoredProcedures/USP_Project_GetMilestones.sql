
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_Project_GetMilestones')
DROP PROCEDURE USP_Project_GetMilestones
GO
CREATE PROCEDURE [dbo].[USP_Project_GetMilestones]
(
	@numDomainID NUMERIC(18,0)
	,@numProId NUMERIC(18,0)
)
AS 
BEGIN
	SELECT 
		ROW_NUMBER() OVER(ORDER BY vcMileStoneName ASC) numMileStoneID
		,vcMileStoneName
		,CONCAT(StagePercentageDetails.vcMileStoneName,' (',ISNULL(StagePercentageMaster.numStagePercentage,0),'% of total)') vcMileStone
		,dbo.GetTotalProgress(@numDomainID,@numProId,2,2,vcMileStoneName,0) numTotalProgress
		,(SELECT TOP 1 intTotalProgress FROM ProjectProgress where numProId=@numProId AND numDomainID=@numDomainID) AS numTotalCompletedProgress
	FROM 
		ProjectsMaster
	INNER JOIN
		StagePercentageDetails
	ON
		ProjectsMaster.numBusinessProcessId = StagePercentageDetails.slp_id
	LEFT JOIN
		StagePercentageMaster
	ON
		StagePercentageDetails.numStagePercentageId = StagePercentageMaster.numStagePercentageId
	WHERE
		ProjectsMaster.numDomainID = @numDomainID
		AND ProjectsMaster.numProId = @numProId
	GROUP BY
		vcMileStoneName
		,StagePercentageMaster.numStagePercentage
END
