        
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_ManageBankMaster')
	DROP PROCEDURE usp_ManageBankMaster
GO

/****** Added By : Joseph ******/
/****** Add/Update New Financial Institution Detail to Table BankMaster******/

CREATE PROCEDURE [dbo].[usp_ManageBankMaster]
	@numBankMasterID numeric(18, 0),
	@tintBankType tinyint,
	@vcFIId varchar(50),
	@vcFIName varchar(50),
	@vcFIOrganization varchar(50),
	@vcFIOFXServerUrl varchar(350),
	@vcBankID varchar(50),
	@vcOFXAccessKey varchar(50),
	@dtCreatedDate datetime,
	@dtModifiedDate datetime,
	@vcBankPhone varchar(50),
	@vcBankWebsite varchar(350),
	@vcUserIdLabel varchar(50)
AS

SET NOCOUNT ON

IF EXISTS(SELECT [numBankMasterID] FROM [dbo].[BankMaster] WHERE [numBankMasterID] = @numBankMasterID)
BEGIN
	UPDATE [dbo].[BankMaster] SET
		[tintBankType] = @tintBankType,
		[vcFIId] = @vcFIId,
		[vcFIName] = @vcFIName,
		[vcFIOrganization] = @vcFIOrganization,
		[vcFIOFXServerUrl] = @vcFIOFXServerUrl,
		[vcBankID] = @vcBankID,
		[vcOFXAccessKey] = @vcOFXAccessKey,
		[dtModifiedDate] = GETDATE(),
		[vcBankPhone] = @vcBankPhone,
		[vcBankWebsite] = @vcBankWebsite,
		[vcUserIdLabel] = @vcUserIdLabel
	WHERE
		[numBankMasterID] = @numBankMasterID
END
ELSE
BEGIN
	INSERT INTO [dbo].[BankMaster] (
		[tintBankType],
		[vcFIId],
		[vcFIName],
		[vcFIOrganization],
		[vcFIOFXServerUrl],
		[vcBankID],
		[vcOFXAccessKey],
		[dtCreatedDate],
		[vcBankPhone],
		[vcBankWebsite],
		[vcUserIdLabel]
	) VALUES (
		@tintBankType,
		@vcFIId,
		@vcFIName,
		@vcFIOrganization,
		@vcFIOFXServerUrl,
		@vcBankID,
		@vcOFXAccessKey,
		GETDATE(),
		@vcBankPhone,
		@vcBankWebsite,
		@vcUserIdLabel
	)
	SET @numBankMasterID = SCOPE_IDENTITY()	
	
END

SELECT @numBankMasterID
GO
