
GO
/****** Object:  StoredProcedure [dbo].[USP_GetTaskForUserActivity]    Script Date: 09/21/2010 12:00:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
----Created By Prasanta    
    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetTaskForUserActivity')
DROP PROCEDURE USP_GetTaskForUserActivity
GO
CREATE PROCEDURE [dbo].[USP_GetTaskForUserActivity]
@numUserCntId as numeric(9)=0,    
@numDomainID as numeric(18)=0
as    
BEGIN   
	SELECT T.dtmDueDate,T.vcMileStoneName,T.OrderType,T.vcStageName,T.vcPOppName,T.vcTaskName,T.numOppID,
	T.numStageDetailsId,T.numOppID,T.numStagePercentageId,T.numStagePercentage AS tintPercentage,T.tintConfiguration FROM (
	SELECT 
		T.vcTaskName,
		CASE WHEN S.bitIsDueDaysUsed=1 THEN CONVERT(VARCHAR(10),DATEADD(DAY,S.intDueDays,CAST(S.dtStartDate AS DATE)), 101)
		ELSE CONVERT(VARCHAR(10),DATEADD(DAY,(SELECT TOP 1 CAST(Items AS DECIMAL(18,2)) FROM Split(dbo.fn_calculateTotalHoursMinutesByStageDetails(T.numStageDetailsId,T.numOppId,T.numProjectId),'_')),CAST(S.dtStartDate AS DATE)), 101) END AS dtmDueDate,
		CASE WHEN OP.tintOppType=0 THEN 'Sales Opportunity' 
			 WHEN OP.tintOppType=1 THEN 'Sales Order' WHEN OP.tintOppType=2 THEN 'Purchase Order' ELSE '' END AS OrderType,
			 OP.vcPOppName,
			 S.vcMileStoneName,
			 S.vcStageName,
			 T.numStageDetailsId,
			 S.numOppID,
			 S.numStagePercentageId,
			 SM.numStagePercentage,
			 S.tintConfiguration
	FROM StagePercentageDetailsTask AS T
	LEFT JOIN StagePercentageDetails AS S
	ON S.numStageDetailsId=T.numStageDetailsId AND S.numOppID=T.numOppId
	LEFT JOIN OpportunityMaster AS OP 
	ON T.numOppId=OP.numOppId
	LEFT JOIN StagePercentageMaster AS SM
	ON S.numStagePercentageId=SM.numStagePercentageId
	WHERE T.numOppId>0 AND s.numOppID>0 AND T.bitSavedTask=1 AND T.bitTaskClosed=0 AND T.numDomainID=@numDomainID AND
	1=(CASE WHEN T.numAssignTo=@numUserCntId OR (S.numTeamId>0 
	AND @numUserCntId IN(SELECT numContactId FROM AdditionalContactsInformation WHERE numTeam=S.numTeamId)) THEN 1 ELSE 0 END)) AS T 
END
