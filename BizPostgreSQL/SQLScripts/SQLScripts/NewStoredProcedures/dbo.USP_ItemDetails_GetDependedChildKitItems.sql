GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ItemDetails_GetDependedChildKitItems')
DROP PROCEDURE USP_ItemDetails_GetDependedChildKitItems
GO
CREATE PROCEDURE [dbo].[USP_ItemDetails_GetDependedChildKitItems]  
	@numDomainID NUMERIC(18,0)
	,@numMainKitItemCode NUMERIC(18,0)
	,@numChildKitItemCode NUMERIC(18,0)
	,@numChildKitSelectedItem VARCHAR(MAX)
AS  
BEGIN  
	DECLARE @TEMP TABLE
	(
		numPrimaryListID NUMERIC(18,0)
		,numSelectedValue NUMERIC(18,0)
	)

	INSERT INTO @TEMP
	(
		numPrimaryListID
		,numSelectedValue
	)
	SELECT 
		CAST(SUBSTRING(OutParam,0,CHARINDEX('-',OutParam)) AS NUMERIC)
		,CAST(SUBSTRING(OutParam,CHARINDEX('-',OutParam) + 1,LEN(OutParam)) AS NUMERIC)
	FROM 
		dbo.SplitString(@numChildKitSelectedItem,',')

	SELECT 
		numSecondaryListID AS numChildKitItemCode
		,ISNULL(I.bitKitSingleSelect,1) bitKitSingleSelect
		,ISNULL((SELECT tintView FROM ItemDetails ID WHERE ID.numItemKitID=@numMainKitItemCode AND ID.numChildItemID=numSecondaryListID),1) tintView
		,ISNULL((SELECT bitOrderEditable FROM ItemDetails ID WHERE ID.numItemKitID=@numMainKitItemCode AND ID.numChildItemID=numSecondaryListID),0) bitOrderEditable
	FROM
		FieldRelationship FR
	INNER JOIN
		Item I
	ON
		I.numItemCode = FR.numSecondaryListID
	WHERE
		FR.numDomainID=@numDomainID
		AND FR.numModuleID = 14
		AND FR.numPrimaryListID=@numChildKitItemCode
		AND (SELECT COUNT(*) FROM FieldRelationshipDTL FRD WHERE FRD.numFieldRelID=FR.numFieldRelID) > 0
	ORDER BY
		FR.numSecondaryListID

	DECLARE @TEMPItems TABLE
	(
		numPrimaryListID NUMERIC(18,0)
		,numChildKitItemCode NUMERIC(18,0)
		,numItemCode NUMERIC(18,0)
		,vcItemName VARCHAR(300)
		,vcDisplayText VARCHAR(500)
		,UnitName VARCHAR(100)
		,Qty FLOAT
		,vcSaleUOMName VARCHAR(100)
		,fltUOMConversion FLOAT
	)

	INSERT INTO @TEMPItems
	(
		numPrimaryListID
		,numChildKitItemCode
		,numItemCode
		,vcItemName
		,vcDisplayText
		,UnitName
		,Qty
		,vcSaleUOMName
		,fltUOMConversion
	)
	SELECT 
		FR.numPrimaryListID,
		FR.numSecondaryListID AS numChildKitItemCode,
		Item.numItemCode,
		ISNULL(Item.vcItemName,'') AS vcItemName,
		dbo.GetKitChildItemsDisplay(@numMainKitItemCode,FR.numSecondaryListID,Item.numItemCode) AS vcDisplayText,
		ISNULL(UOM.vcUnitName,'') AS UnitName,
		ItemDetails.numQtyItemsReq as Qty,
		dbo.fn_GetUOMName(Item.numSaleUnit) AS vcSaleUOMName,
		ISNULL(dbo.fn_UOMConversion(Item.numSaleUnit,Item.numItemCode,@numDomainId,Item.numBaseUnit),1) fltUOMConversion
	FROM
		FieldRelationshipDTL FRD
	INNER JOIN
		FieldRelationship FR
	ON
		FRD.numFieldRelID = FR.numFieldRelID
	INNER JOIN
		@TEMP T
	ON
		T.numPrimaryListID = FR.numPrimaryListID
		AND FRD.numPrimaryListItemID = T.numSelectedValue
	INNER JOIN
		ItemDetails
	ON
		FR.numSecondaryListID = ItemDetails.numItemKitID
	INNER JOIN
		Item
	ON
		ItemDetails.numChildItemID = Item.numItemCode
		AND Item.numItemCode = FRD.numSecondaryListItemID
	LEFT JOIN 
		UOM
	ON 
		ItemDetails.numUOMId = UOM.numUOMId
	WHERE
		FR.numDomainID=@numDomainID
		AND FR.numModuleID = 14
		AND FRD.numSecondaryListItemID <> -1
		AND FR.numSecondaryListID IN (SELECT 
											FR.numSecondaryListID
										FROM
											FieldRelationship FR
										WHERE
											FR.numDomainID=@numDomainID
											AND FR.numModuleID = 14
											AND FR.numPrimaryListID=@numChildKitItemCode
											AND (SELECT COUNT(*) FROM FieldRelationshipDTL FRD WHERE FRD.numFieldRelID=FR.numFieldRelID) > 0)
	UNION
	SELECT 
		FR.numPrimaryListID,
		FR.numSecondaryListID AS numChildKitItemCode,
		-1,
		'' AS vcItemName,
		'' AS vcDisplayText,
		'' AS UnitName,
		0 as Qty,
		'',
		1
	FROM
		FieldRelationshipDTL FRD
	INNER JOIN
		FieldRelationship FR
	ON
		FRD.numFieldRelID = FR.numFieldRelID
	INNER JOIN
		@TEMP T
	ON
		T.numPrimaryListID = FR.numPrimaryListID
		AND FRD.numPrimaryListItemID = T.numSelectedValue
	INNER JOIN
		ItemDetails
	ON
		FR.numSecondaryListID = ItemDetails.numItemKitID
	WHERE
		FR.numDomainID=@numDomainID
		AND FR.numModuleID = 14
		AND FRD.numSecondaryListItemID = -1
		AND FR.numSecondaryListID IN (SELECT 
											FR.numSecondaryListID
										FROM
											FieldRelationship FR
										WHERE
											FR.numDomainID=@numDomainID
											AND FR.numModuleID = 14
											AND FR.numPrimaryListID=@numChildKitItemCode
											AND (SELECT COUNT(*) FROM FieldRelationshipDTL FRD WHERE FRD.numFieldRelID=FR.numFieldRelID) > 0)

	
	SELECT 
		* 
	FROM
	(
		SELECT 
			numChildKitItemCode
			,numItemCode
			,vcItemName
			,vcDisplayText
			,UnitName
			,Qty
			,vcSaleUOMName
			,fltUOMConversion
		FROM 
			@TEMPItems TI
		GROUP BY
			numChildKitItemCode
			,numItemCode
			,vcItemName
			,vcDisplayText
			,UnitName
			,Qty
			,vcSaleUOMName
			,fltUOMConversion
		HAVING 
			1 = (CASE 
					WHEN ISNULL((SELECT COUNT(DISTINCT TIInner.numPrimaryListID) FROM @TEMPItems TIInner WHERE TIInner.numChildKitItemCode = TI.numChildKitItemCode),0) > 1
					THEN (CASE WHEN COUNT(*) = (SELECT COUNT(*) FROM (SELECT numPrimaryListID FROM @TEMPItems TI1 WHERE TI1.numChildKitItemCode=TI.numChildKitItemCode GROUP BY TI1.numPrimaryListID,TI1.numChildKitItemCode) TEMP2) THEN 1 ELSE 0 END)
					ELSE 1 
				END)
	) T
	WHERE
		numItemCode <> -1
END
GO
