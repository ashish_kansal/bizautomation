SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ReportListMaster_PartnerRMPLast12Months')
DROP PROCEDURE USP_ReportListMaster_PartnerRMPLast12Months
GO
CREATE PROCEDURE [dbo].[USP_ReportListMaster_PartnerRMPLast12Months]
	@numDomainID NUMERIC(18,0)
AS
BEGIN 
	DECLARE @numShippingItemID NUMERIC(18,0)
	DECLARE @numDiscountItemID NUMERIC(18,0)
	DECLARE @ProfitCost INT
	SELECT @numShippingItemID=numShippingServiceItemID,@numDiscountItemID=numDiscountServiceItemID,@ProfitCost=numCost FROM Domain WHERE numDomainId=@numDomainID

	SELECT
		CI.vcCompanyName
		,SUM(OM.monDealAmount) AS monDealAmount
	FROM
		OpportunityMaster OM
	INNER JOIN
		OpportunityItems OI
	ON
		OM.numOppID = OI.numOppId
	INNER JOIN
		DivisionMaster DM
	ON
		OM.numPartner = DM.numDivisionID
	INNER JOIN
		CompanyInfo CI
	ON
		DM.numCompanyID = CI.numCompanyId
	WHERE
		OM.numDomainId=@numDomainID
		AND ISNULL(OM.tintOppType,0)=1
		AND ISNULL(OM.tintOppStatus,0)=1
		AND OM.bintCreatedDate >= DATEADD(MONTH, -12, GETUTCDATE())
	GROUP BY
		CI.vcCompanyName
	HAVING
		SUM(monDealAmount) > 0
	ORDER BY
		SUM(monDealAmount) DESC
		
	SELECT
		vcCompanyName
		,(SUM(Profit)/SUM(monTotAmount)) * 100 BlendedProfit
	FROM
	(
		SELECT
			CI.vcCompanyName
			,ISNULL(monTotAmount,0) monTotAmount
			,ISNULL(dbo.GetOrderItemProfitAmountOrMargin(@numDomainID,OM.numOppId,OI.numoppitemtCode,1),0) Profit
		FROM
			OpportunityMaster OM
		INNER JOIN
			OpportunityItems OI
		ON
			OI.numOppId = OM.numOppID
		INNER JOIN
			DivisionMaster DM
		ON
			OM.numPartner = DM.numDivisionID
		INNER JOIN
			CompanyInfo CI
		ON
			DM.numCompanyID = CI.numCompanyId
		INNER JOIN
			Item I
		ON
			OI.numItemCode = I.numItemCode
		WHERE
			OM.numDomainId=@numDomainID
			AND ISNULL(OI.monTotAmount,0) <> 0
			AND ISNULL(OI.numUnitHour,0) <> 0
			AND ISNULL(OM.tintOppType,0)=1
			AND ISNULL(OM.tintOppStatus,0)=1
			AND OM.bintCreatedDate >= DATEADD(MONTH, -12, GETUTCDATE())
			AND I.numItemCode NOT IN (@numShippingItemID,@numDiscountItemID)
	) TEMP2
	GROUP BY
		vcCompanyName
	HAVING
		(SUM(Profit)/SUM(monTotAmount)) * 100 > 0
	ORDER BY
		(SUM(Profit)/SUM(monTotAmount)) * 100 DESC

	SELECT
		*
	FROM
	(
		SELECT
			CI.vcCompanyName
			,SUM(dbo.GetOrderItemProfitAmountOrMargin(@numDomainID,OM.numOppId,OI.numoppitemtCode,1)) Profit
		FROM
			OpportunityMaster OM
		INNER JOIN
			OpportunityItems OI
		ON
			OI.numOppId = OM.numOppID
		INNER JOIN
			DivisionMaster DM
		ON
			OM.numPartner = DM.numDivisionID
		INNER JOIN
			CompanyInfo CI
		ON
			DM.numCompanyID = CI.numCompanyId
		INNER JOIN
			Item I
		ON
			OI.numItemCode = I.numItemCode
		Left JOIN 
			Vendor V 
		ON 
			V.numVendorID=I.numVendorID 
			AND V.numItemCode=I.numItemCode
		WHERE
			OM.numDomainId=@numDomainID
			AND ISNULL(OI.monTotAmount,0) <> 0
			AND ISNULL(OI.numUnitHour,0) <> 0
			AND ISNULL(OM.tintOppType,0)=1
			AND ISNULL(OM.tintOppStatus,0)=1
			AND OM.bintCreatedDate >= DATEADD(MONTH, -12, GETUTCDATE())
			AND I.numItemCode NOT IN (@numShippingItemID,@numDiscountItemID)
		GROUP BY
			CI.vcCompanyName
	) TEMP2
	ORDER BY
		Profit DESC
END
GO

