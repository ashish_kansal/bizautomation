GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_EmailHistory_MarkAsSpam' ) 
    DROP PROCEDURE USP_EmailHistory_MarkAsSpam
GO
CREATE PROCEDURE USP_EmailHistory_MarkAsSpam
    @vcEmailHstrID AS VARCHAR(MAX)
AS 
BEGIN
	DECLARE @strSQL AS VARCHAR(MAX)

	SELECT numUserCntId FROM EmailHistory

	SET @strSQL = 'DELETE FROM EmailHStrToBCCAndCC WHERE numEmailHstrID IN (' + @vcEmailHstrID + ')'
	EXEC(@strSQL)
    
	SET @strSQL = 'DELETE FROM EmailHstrAttchDtls WHERE numEmailHstrID IN (' + @vcEmailHstrID + ')'
	EXEC(@strSQL)   

	SET @strSQL = 'DELETE FROM EmailHistory WHERE numEmailHstrID IN (' + @vcEmailHstrID + ')'
	EXEC(@strSQL)
END