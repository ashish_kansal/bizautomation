GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_GetOpportunityAutomationQueueExecutionLog' ) 
    DROP PROCEDURE USP_GetOpportunityAutomationQueueExecutionLog
GO
CREATE PROCEDURE USP_GetOpportunityAutomationQueueExecutionLog
    @numDomainID numeric(18, 0)=0,
    @numOppId numeric(18, 0)=0,
    @ClientTimeZoneOffset INT=0,
     @CurrentPage INT=0,
    @PageSize INT=0,
    @TotRecs INT=0  OUTPUT
AS 
BEGIN

	SELECT
		dbo.FormatedDateFromDate(DateAdd(minute,-@ClientTimeZoneOffset, dtDate),@numDomainID) AS dtExecutionDate,
		DateAdd(minute,-@ClientTimeZoneOffset, dtDate) AS dtExecutionTime,
		ISNULL(vcMessage,'') vcMessage
	FROM
		SalesFulfillmentLog
	WHERE
		numOppID=@numOppId
	ORDER BY
		numSFLID
	OFFSET 
		(@CurrentPage - 1) * @PageSize ROWS
	FETCH NEXT @PageSize ROWS ONLY

END



