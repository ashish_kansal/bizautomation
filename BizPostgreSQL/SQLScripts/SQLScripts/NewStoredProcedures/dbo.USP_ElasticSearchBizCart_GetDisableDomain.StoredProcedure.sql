GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p' AND NAME ='USP_ElasticSearchBizCart_GetDisableDomain')
DROP PROCEDURE USP_ElasticSearchBizCart_GetDisableDomain
GO

/****** Object:  StoredProcedure [dbo].[USP_ElasticSearchBizCart_GetDisableDomain]    Script Date: 27/04/2020 14:10:01 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[USP_ElasticSearchBizCart_GetDisableDomain]
AS
BEGIN
	SELECT DISTINCT 
		EC.numDomainID
    FROM 
		eCommerceDTL EC
	LEFT JOIN 
		eCommerceDTL OtherSite 
	ON 
		EC.numDomainId = OtherSite.numDomainId 
		AND (DATEDIFF(DAY,ec.dtElasticSearchDisableDatetime,GETUTCDATE()) <= 30 OR ec.bitElasticSearch = 1)
    WHERE 
		ec.dtElasticSearchDisableDatetime Is not null 
		AND DATEDIFF(DAY,ec.dtElasticSearchDisableDatetime,GETUTCDATE()) > 30
		AND ec.bitElasticSearch = 0 AND OtherSite.numDomainId IS NULL
END
GO


