SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ValidPromotionCode')
DROP PROCEDURE USP_ValidPromotionCode
GO
CREATE PROCEDURE [dbo].[USP_ValidPromotionCode]
    @vcPromoCode AS VARCHAR(100) = 0,
    @numDomainID AS NUMERIC(9) = 0,
    @numSiteID AS NUMERIC(9) = 0
AS 
    IF EXISTS(SELECT numProId FROM PromotionOffer WHERE txtCouponCode=@vcPromoCode AND bitEnabled=1)
       BEGIN
			DECLARE @validFrom DATETIME
			DECLARE @validTo DATETIME
			DECLARE @bitAppliesToSite BIT
			DECLARE @bitRequireCouponCode BIT
			DECLARE @tintUsageLimit INT
			DECLARE @tintOfferTriggerValueType INT -- Based Offer On 1-Quantiy 2-Amount 
			DECLARE @fltOfferTriggerValue INT-- Based Offer Amount/Quanity
			DECLARE @tintOfferBasedOn INT-- Based On Classification 1: individual item 2:item classification
			DECLARE @fltDiscountValue INT -- Applied Offer Amount/Quanity
	   END
GO
