GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_AdvancedSearchGetModuleAndFields')
DROP PROCEDURE dbo.USP_AdvancedSearchGetModuleAndFields
GO
CREATE PROCEDURE [dbo].[USP_AdvancedSearchGetModuleAndFields]
(
	@numDomainID NUMERIC(18,0)
	,@numUserCntID NUMERIC(18,0)
)
AS
BEGIN
	DECLARE @numAuthGroupID NUMERIC(18,0)
	SELECT @numAuthGroupID=numGroupID FROM UserMaster WHERE numUserDetailId = @numUserCntID

	DECLARE @TEMP TABLE
	(
		numFormID NUMERIC(18,0)
		,vcFormName VARCHAR(300)
	)

	INSERT INTO @TEMP (numFormID,vcFormName) VALUES (1,'Organizations & Contacts')

	IF EXISTS (SELECT numGroupID FROM GroupAuthorization WHERE numModuleID=9 AND numPageID=7 AND numGroupID=@numAuthGroupID AND ISNULL(intViewAllowed,0) <> 0)
	BEGIN
		INSERT INTO @TEMP (numFormID,vcFormName) VALUES (15,'Opportunities & Orders (with items)')
	END

	IF EXISTS (SELECT numGroupID FROM GroupAuthorization WHERE numModuleID=9 AND numPageID=11 AND numGroupID=@numAuthGroupID AND ISNULL(intViewAllowed,0) <> 0)
	BEGIN
		INSERT INTO @TEMP (numFormID,vcFormName) VALUES (29,'Items')
	END

	IF EXISTS (SELECT numGroupID FROM GroupAuthorization WHERE numModuleID=9 AND numPageID=8 AND numGroupID=@numAuthGroupID AND ISNULL(intViewAllowed,0) <> 0)
	BEGIN
		INSERT INTO @TEMP (numFormID,vcFormName) VALUES (17,'Cases')
	END

	IF EXISTS (SELECT numGroupID FROM GroupAuthorization WHERE numModuleID=9 AND numPageID=8 AND numGroupID=@numAuthGroupID AND ISNULL(intViewAllowed,0) <> 0)
	BEGIN
		INSERT INTO @TEMP (numFormID,vcFormName) VALUES (18,'Projects')
	END

	IF EXISTS (SELECT numGroupID FROM GroupAuthorization WHERE numModuleID=9 AND numPageID=12 AND numGroupID=@numAuthGroupID AND ISNULL(intViewAllowed,0) <> 0)
	BEGIN
		INSERT INTO @TEMP (numFormID,vcFormName) VALUES (59,'Financial Transactions')
	END

	SELECT * FROM @TEMP
	
	SELECT
		*
	FROM
	(
		SELECT
			DFCD.numFormId
			,DFCD.numFieldId
			,DFFm.vcFieldName
			,DFFM.vcAssociatedControlType
			,0 AS bitCustomField
			,DFM.numListID
			,DFM.vcListItemType
			,DFM.vcLookBackTableName
			,DFM.vcDbColumnName
			,'R' AS vcFieldType
		FROM
			DycFormConfigurationDetails DFCD
		INNER JOIN 
			DycFormField_Mapping DFFM
		ON
			DFCD.numFieldId = DFFM.numFieldId
			AND DFCD.numFormId=DFFM.numFormID
		INNER JOIN
			DycFieldMaster DFM
		ON
			DFCD.numFieldId = DFM.numFieldId
		WHERE
			DFCD.numDomainId=@numDomainID
			AND numAuthGroupID=@numAuthGroupID
			AND DFCD.numFormId IN (SELECT numFormID FROm @TEMP)
		UNION
		SELECT
			DFCD.numFormId
			,DFCD.numFieldId
			,CFW.Fld_label AS vcFieldName
			,CFW.Fld_type AS vcAssociatedControlType
			,1 AS bitCustomField
			,CFW.numlistid
			,(CASE WHEN ISNULL(CFW.numlistid,0) > 0 THEN 'LI' ELSE '' END) vcListItemType
			,'' AS vcLookBackTableName
			,CONVERT(VARCHAR(15), DFCD.numFieldId) + '_C' vcDbColumnName
			,CASE CLM.Loc_id WHEN 3 THEN 'CA' WHEN 5 THEN 'I' WHEN 9 THEN 'IA' WHEN 11 THEN 'P' ELSE CLM.vcFieldType END vcFieldType
		FROM
			DycFormConfigurationDetails DFCD
		INNER JOIN 
			CFW_Fld_Master CFW
		ON
			DFCD.numFieldId = CFW.Fld_id
		INNER JOIN 
			CFW_Loc_Master CLM
		ON
			CFW.GRP_ID=CLM.Loc_Id
		WHERE
			DFCD.numDomainId=@numDomainID
			AND numAuthGroupID=@numAuthGroupID
			AND DFCD.bitCustom=1
			AND DFCD.numFormId IN (SELECT numFormID FROm @TEMP)
	) TEMP
	ORDER BY
		numFormId
		,vcFieldName
END
GO