SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ReportListMaster_ItemClassificationProfitPreBuildReport')
DROP PROCEDURE USP_ReportListMaster_ItemClassificationProfitPreBuildReport
GO
CREATE PROCEDURE [dbo].[USP_ReportListMaster_ItemClassificationProfitPreBuildReport]
	@numDomainID NUMERIC(18,0)
AS
BEGIN 
	DECLARE @numShippingItemID NUMERIC(18,0)
	DECLARE @numDiscountItemID NUMERIC(18,0)
	SELECT @numShippingItemID=ISNULL(numShippingServiceItemID,0),@numDiscountItemID=ISNULL(numDiscountServiceItemID,0) FROM Domain WHERE numDomainId=@numDomainID


	SELECT 
		vcItemClassification
		,(SUM(Profit)/SUM(monTotAmount)) * 100 BlendedProfit
	FROM
	(
		SELECT
			LD.vcData AS vcItemClassification
			,ISNULL(monTotAmount,0) monTotAmount
			,ISNULL(dbo.GetOrderItemProfitAmountOrMargin(@numDomainID,OM.numOppId,OI.numoppitemtCode,1),0) Profit
		FROM
			OpportunityMaster OM
		INNER JOIN
			OpportunityItems OI
		ON
			OM.numOppID = OI.numOppId
		INNER JOIN
			Item I
		ON
			OI.numItemCode = I.numItemCode
		INNER JOIN
			ListDetails LD
		ON
			I.numItemClassification = LD.numListItemID
			AND LD.numListID=36
			AND LD.numDomainID=@numDomainID
		WHERE
			OM.numDomainId=@numDomainID
			AND ISNULL(OI.monTotAmount,0) <> 0
			AND ISNULL(OI.numUnitHour,0) <> 0
			AND ISNULL(OM.tintOppType,0)=1
			AND ISNULL(OM.tintOppStatus,0)=1
			AND OM.bintCreatedDate >= DATEADD(MONTH, -12, GETUTCDATE())
			AND I.numItemCode NOT IN (@numShippingItemID,@numDiscountItemID)
	) TEMP
	GROUP BY
		vcItemClassification
	HAVING 
		(SUM(Profit)/SUM(monTotAmount)) * 100 > 0
	ORDER BY
		(SUM(Profit)/SUM(monTotAmount)) * 100 DESC
END
GO