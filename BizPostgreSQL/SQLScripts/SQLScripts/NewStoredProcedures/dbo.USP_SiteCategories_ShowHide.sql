GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_SiteCategories_ShowHide')
DROP PROCEDURE USP_SiteCategories_ShowHide
GO
CREATE PROCEDURE [dbo].[USP_SiteCategories_ShowHide]
	@numDomainID NUMERIC(18,0)
	,@numCategoryProfileID NUMERIC(18,0)               
	,@numCategoryID NUMERIC(18,0)                     
	,@bitShow BIT           
AS 
BEGIN
	DECLARE @TableSites TABLE
	(
		numSiteID NUMERIC(18,0)
	)

	INSERT INTO @TableSites
	(
		numSiteID
	)
	SELECT
		numSiteID
	FROM
		CategoryProfileSites
	WHERE
		numCategoryProfileID=@numCategoryProfileID

	DECLARE @TableCategories TABLE
	(
		numCategoryID NUMERIC(18,0)
	)

	;WITH CTE (numCategoryID) AS
	(
		SELECT
			numCategoryID
		FROM
			Category
		WHERE
			numDomainID=@numDomainID
			AND numCategoryProfileID=@numCategoryProfileID
			AND numCategoryID=@numCategoryID
		 UNION ALL
		 SELECT
			C1.numCategoryID
		 FROM 
			dbo.Category C1 
		 INNER JOIN 
			CTE C
		 ON 
			C1.numDepCategory = C.numCategoryID 
		 WHERE 
			C1.numDomainID=@numDomainID 
			AND C1.numCategoryProfileID=@numCategoryProfileID
	)
	INSERT INTO @TableCategories
	(
		numCategoryID
	)
	SELECT
		numCategoryID
	FROM
		CTE

	DECLARE @TableSiteCategories TABLE
	(
		numSiteID NUMERIC(18,0)
		,numCategoryID NUMERIC(18,0)
	)

	INSERT INTO @TableSiteCategories
	(
		numSiteID
		,numCategoryID
	)
	SELECT 
		numSiteID
		,numCategoryID 
	FROM 
		@TableSites,@TableCategories

	IF ISNULL(@bitShow,0) = 0
	BEGIN
		DELETE 
			SC
		FROM 
			SiteCategories SC
		INNER JOIN
			@TableSiteCategories TSC
		ON
			SC.numSiteID=TSC.numSiteID
			AND SC.numCategoryID=TSC.numCategoryID

		UPDATE Category SET bitVisible=0 WHERE numDomainID=@numDomainID AND numCategoryID IN (SELECT numCategoryID FROM @TableCategories)
	END	
	ELSE
	BEGIN
		INSERT INTO SiteCategories
		(
			numSiteID
			,numCategoryID
		)
		SELECT
			TSC.numSiteID
			,TSC.numCategoryID
		FROM
			@TableSiteCategories TSC
		LEFT JOIN
			SiteCategories SC
		ON
			TSC.numSiteID=SC.numSiteID
			AND TSC.numCategoryID=SC.numCategoryID
		WHERE
			SC.numSiteID IS NULL

		UPDATE Category SET bitVisible=1 WHERE numDomainID=@numDomainID AND numCategoryID IN (SELECT numCategoryID FROM @TableCategories)
	END
END
GO

