SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ManagePromotionOffer')
DROP PROCEDURE USP_ManagePromotionOffer
GO
CREATE PROCEDURE [dbo].[USP_ManagePromotionOffer]
    @numProId AS NUMERIC(9) = 0,
    @vcProName AS VARCHAR(100),
    @numDomainId AS NUMERIC(18,0),
    @dtValidFrom AS DATE,
    @dtValidTo AS DATE,
	@bitNeverExpires BIT,
	@bitUseOrderPromotion BIT,
	@numOrderPromotionID NUMERIC(18,0),
	@tintOfferTriggerValueType TINYINT,
	@fltOfferTriggerValue INT,
	@tintOfferBasedOn TINYINT,
	@tintDiscountType TINYINT,
	@fltDiscountValue FLOAT,
	@tintDiscoutBaseOn TINYINT,
	@numUserCntID NUMERIC(18,0),
	@tintCustomersBasedOn TINYINT,
	@tintOfferTriggerValueTypeRange TINYINT,
	@fltOfferTriggerValueRange FLOAT,
	@vcShortDesc VARCHAR(100),
	@vcLongDesc VARCHAR(500),
	@tintItemCalDiscount TINYINT,
	@monDiscountedItemPrice DECIMAL(20,5)
AS 
BEGIN
	IF @numProId = 0 
	BEGIN
		INSERT INTO PromotionOffer
        (
            vcProName
			,numDomainId
			, numCreatedBy
			,dtCreated
			,bitEnabled
			,IsOrderBasedPromotion
        )
        VALUES  
		(
			@vcProName
           ,@numDomainId
           ,@numUserCntID
		   ,GETUTCDATE()
		   ,0
		   ,0
        )
            
		SELECT SCOPE_IDENTITY()
	END
	ELSE
	BEGIN
		BEGIN TRY
		BEGIN TRANSACTION
			IF @tintOfferBasedOn <> 4
			BEGIN
				IF (SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId=@numProId AND tintType=ISNULL(@tintOfferBasedOn,0) AND tintRecordType=5) = 0
				BEGIN
					RAISERROR ( 'SELECT_ITEMS_FOR_PROMOTIONS',16, 1 )
 					RETURN;
				END
			END

			IF ISNULL(@tintDiscoutBaseOn,0) = 1 AND (SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId=@numProId AND tintType=1 AND tintRecordType=6) = 0
			BEGIN
				RAISERROR ( 'SELECT_DISCOUNTED_ITEMS',16, 1 )
 				RETURN;
			END
			ELSE IF ISNULL(@tintDiscoutBaseOn,0) = 2  AND (SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId=@numProId AND tintType=2 AND tintRecordType=6) = 0
			BEGIN
				RAISERROR ( 'SELECT_DISCOUNTED_ITEMS',16, 1 )
 				RETURN;
			END
			ELSE IF ISNULL(@tintDiscoutBaseOn,0) = 6  AND (SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId=@numProId AND tintType=4 AND tintRecordType=6) = 0
			BEGIN
				RAISERROR ( 'SELECT_DISCOUNTED_ITEMS',16, 1 )
 				RETURN;
			END

			IF ISNULL(@bitUseOrderPromotion,0) = 0
			BEGIN
				/*Check For Duplicate Customer-Item relationship 
					1- The customer is specific (level 1), and the item is specific (also level 1). If the user tries to create another rule that targets the customer by name, or that same item by name, validation would say �You�ve already created a promotion rule that targets this customer and item by name�*/
				IF @tintCustomersBasedOn = 1
				BEGIN
					IF ISNULL(@tintCustomersBasedOn,0) = 1 AND (SELECT COUNT(*) FROM PromotionOfferOrganizations WHERE numProId = @numProId AND tintType = 1) = 0
					BEGIN
						RAISERROR ( 'SELECT_INDIVIDUAL_CUSTOMER',16, 1 )
 						RETURN;
					END
				END
			
					/* 2- The customer is targeted at the group level (level 2), and the item is specific (level 1). If the user tries to create another rule that targets this same Relationship / Profile combination and that same item in level 1 (L2 customer and L1 item both match), validation would say �You�ve already created a promotion rule that targets this relationship / profile and one or more item specifically�*/
				ELSE IF @tintCustomersBasedOn = 2
				BEGIN
					IF ISNULL(@tintCustomersBasedOn,0) = 2 AND (SELECT COUNT(*) FROM PromotionOfferOrganizations WHERE numProId = @numProId AND tintType = 2) = 0
					BEGIN
						RAISERROR ( 'SELECT_RELATIONSHIP_PROFILE',16, 1 )
 						RETURN;
					END
				END

				ELSE IF @tintCustomersBasedOn = 0
				BEGIN
					RAISERROR ( 'SELECT_CUSTOMERS',16, 1 )
 					RETURN;
				END

				IF @tintCustomersBasedOn = 3
				BEGIN
					DELETE FROM PromotionOfferorganizations 
						WHERE tintType IN (1, 2) 
							AND numProId = @numProId
				END

				IF (
					SELECT
						COUNT(*)
					FROM
						PromotionOffer
					LEFT JOIN
						PromotionOfferOrganizations
					ON
						PromotionOffer.numProId = PromotionOfferOrganizations.numProId
						AND 1 = (CASE 
									WHEN @tintCustomersBasedOn=1 THEN (CASE WHEN PromotionOfferOrganizations.tintType=1 THEN 1 ELSE 0 END) 
									WHEN @tintCustomersBasedOn=2 THEN (CASE WHEN PromotionOfferOrganizations.tintType=2 THEN 1 ELSE 0 END) 
									ELSE 1
								END)
					LEFT JOIN
						PromotionOfferItems
					ON
						PromotionOffer.numProId = PromotionOfferItems.numProId
						AND 1 = (CASE 
									WHEN @tintOfferBasedOn=1 THEN (CASE WHEN PromotionOfferItems.tintType=1 THEN 1 ELSE 0 END) 
									WHEN @tintOfferBasedOn=2 THEN (CASE WHEN PromotionOfferItems.tintType=2 THEN 1 ELSE 0 END) 
									ELSE 1
								END)
						AND PromotionOfferItems.tintRecordType = 5
					LEFT JOIN
						PromotionOfferItems PromotionOfferItemsDiscount
					ON
						PromotionOffer.numProId = PromotionOfferItemsDiscount.numProId
						AND 1 = (CASE 
									WHEN @tintDiscoutBaseOn=1 THEN (CASE WHEN PromotionOfferItemsDiscount.tintType=1 THEN 1 ELSE 0 END) 
									WHEN @tintDiscoutBaseOn=2 THEN (CASE WHEN PromotionOfferItemsDiscount.tintType=2 THEN 1 ELSE 0 END) 
									ELSE 1
								END)
						AND PromotionOfferItemsDiscount.tintRecordType = 6
					WHERE
						PromotionOffer.numDomainId=@numDomainID
						AND PromotionOffer.numProId <> @numProId
						AND tintCustomersBasedOn = @tintCustomersBasedOn
						AND tintDiscoutBaseOn = @tintDiscoutBaseOn
						AND tintOfferBasedOn = @tintOfferBasedOn
						AND tintOfferTriggerValueType = @tintOfferTriggerValueType
						AND 1 = (CASE 
									WHEN tintOfferTriggerValueTypeRange = 1 --FIXED
									THEN
										(CASE 
											WHEN @tintOfferTriggerValueTypeRange = 1 THEN 
												(CASE WHEN fltOfferTriggerValue = @fltOfferTriggerValue THEN 1 ELSE 0 END)
											WHEN @tintOfferTriggerValueTypeRange = 2 THEN 
												(CASE WHEN fltOfferTriggerValue BETWEEN @fltOfferTriggerValue AND @fltOfferTriggerValueRange THEN 1 ELSE 0 END)
											ELSE 0 
										END)
									WHEN tintOfferTriggerValueTypeRange = 2 --BETWEEN
									THEN
										(CASE 
											WHEN @tintOfferTriggerValueTypeRange = 1 THEN 
												(CASE WHEN @fltOfferTriggerValue BETWEEN fltOfferTriggerValue AND fltOfferTriggerValueRange THEN 1 ELSE 0 END)
											WHEN @tintOfferTriggerValueTypeRange = 2 THEN 
												(CASE 
													WHEN (@fltOfferTriggerValue BETWEEN fltOfferTriggerValue AND fltOfferTriggerValueRange) OR (@fltOfferTriggerValueRange BETWEEN fltOfferTriggerValue AND fltOfferTriggerValueRange) THEN 1 ELSE 0 END)
											ELSE 0 
										END)
									ELSE 0
								END)
						AND 1 = (CASE 
									WHEN @tintCustomersBasedOn=1 AND @tintOfferBasedOn=1 AND @tintDiscoutBaseOn=1 THEN
										(CASE 
											WHEN 
												(SELECT COUNT(*) FROM PromotionOfferOrganizations POOInner WHERE POOInner.numProId=@numProId AND POOInner.tintType=1 AND POOInner.numDivisionID=PromotionOfferOrganizations.numDivisionID) > 0 
												AND (SELECT COUNT(*) FROM PromotionOfferItems POIInner WHERE POIInner.numProId=@numProId AND POIInner.tintRecordType=5 AND POIInner.tintType=1 AND POIInner.numValue=PromotionOfferItems.numValue) > 0 
												AND (SELECT COUNT(*) FROM PromotionOfferItems POIInner WHERE POIInner.numProId=@numProId AND POIInner.tintRecordType=6 AND POIInner.tintType=1 AND POIInner.numValue=PromotionOfferItemsDiscount.numValue) > 0 
											THEN 
												1 
											ELSE 
												0 
										END)
									WHEN @tintCustomersBasedOn=1 AND @tintOfferBasedOn=1 AND @tintDiscoutBaseOn=2 THEN
										(CASE 
											WHEN 
												(SELECT COUNT(*) FROM PromotionOfferOrganizations POOInner WHERE POOInner.numProId=@numProId AND POOInner.tintType=1 AND POOInner.numDivisionID=PromotionOfferOrganizations.numDivisionID) > 0 
												AND (SELECT COUNT(*) FROM PromotionOfferItems POIInner WHERE POIInner.numProId=@numProId AND POIInner.tintRecordType=5 AND POIInner.tintType=1 AND POIInner.numValue=PromotionOfferItems.numValue) > 0 
												AND (SELECT COUNT(*) FROM PromotionOfferItems POIInner WHERE POIInner.numProId=@numProId AND POIInner.tintRecordType=6 AND POIInner.tintType=2 AND POIInner.numValue=PromotionOfferItemsDiscount.numValue) > 0 
											THEN 
												1 
											ELSE 
												0 
										END)
									WHEN @tintCustomersBasedOn=1 AND @tintOfferBasedOn=1 AND (@tintDiscoutBaseOn=3 OR @tintDiscoutBaseOn=4)  THEN
										(CASE 
											WHEN 
												(SELECT COUNT(*) FROM PromotionOfferOrganizations POOInner WHERE POOInner.numProId=@numProId AND POOInner.tintType=1 AND POOInner.numDivisionID=PromotionOfferOrganizations.numDivisionID) > 0 
												AND (SELECT COUNT(*) FROM PromotionOfferItems POIInner WHERE POIInner.numProId=@numProId AND POIInner.tintRecordType=5 AND POIInner.tintType=1 AND POIInner.numValue=PromotionOfferItems.numValue) > 0 
											THEN 
												1 
											ELSE 
												0 
										END)
									WHEN @tintCustomersBasedOn=1 AND @tintOfferBasedOn=2  AND @tintDiscoutBaseOn=1 THEN
										(CASE 
											WHEN 
												(SELECT COUNT(*) FROM PromotionOfferOrganizations POOInner WHERE POOInner.numProId=@numProId AND POOInner.tintType=1 AND POOInner.numDivisionID=PromotionOfferOrganizations.numDivisionID) > 0 
												AND (SELECT COUNT(*) FROM PromotionOfferItems POIInner WHERE POIInner.numProId=@numProId AND POIInner.tintRecordType=5 AND POIInner.tintType=2 AND POIInner.numValue=PromotionOfferItems.numValue) > 0 
												AND (SELECT COUNT(*) FROM PromotionOfferItems POIInner WHERE POIInner.numProId=@numProId AND POIInner.tintRecordType=6 AND POIInner.tintType=1 AND POIInner.numValue=PromotionOfferItemsDiscount.numValue) > 0  
											THEN 
												1 
											ELSE 
												0 
										END)
									WHEN @tintCustomersBasedOn=1 AND @tintOfferBasedOn=2  AND @tintDiscoutBaseOn=2 THEN
										(CASE 
											WHEN 
												(SELECT COUNT(*) FROM PromotionOfferOrganizations POOInner WHERE POOInner.numProId=@numProId AND POOInner.tintType=1 AND POOInner.numDivisionID=PromotionOfferOrganizations.numDivisionID) > 0 
												AND (SELECT COUNT(*) FROM PromotionOfferItems POIInner WHERE POIInner.numProId=@numProId AND POIInner.tintRecordType=5 AND POIInner.tintType=2 AND POIInner.numValue=PromotionOfferItems.numValue) > 0 
												AND (SELECT COUNT(*) FROM PromotionOfferItems POIInner WHERE POIInner.numProId=@numProId AND POIInner.tintRecordType=6 AND POIInner.tintType=2 AND POIInner.numValue=PromotionOfferItemsDiscount.numValue) > 0  
											THEN 
												1 
											ELSE 
												0 
										END)
									WHEN @tintCustomersBasedOn=1 AND @tintOfferBasedOn=2 AND (@tintDiscoutBaseOn=3 OR @tintDiscoutBaseOn=4) THEN
										(CASE 
											WHEN 
												(SELECT COUNT(*) FROM PromotionOfferOrganizations POOInner WHERE POOInner.numProId=@numProId AND POOInner.tintType=1 AND POOInner.numDivisionID=PromotionOfferOrganizations.numDivisionID) > 0 
												AND (SELECT COUNT(*) FROM PromotionOfferItems POIInner WHERE POIInner.numProId=@numProId AND POIInner.tintRecordType=5 AND POIInner.tintType=2 AND POIInner.numValue=PromotionOfferItems.numValue) > 0 
											THEN 
												1 
											ELSE 
												0 
										END)
									WHEN @tintCustomersBasedOn=1 AND @tintOfferBasedOn=4 AND @tintDiscoutBaseOn=1 THEN
										(CASE 
											WHEN 
												(SELECT COUNT(*) FROM PromotionOfferOrganizations POOInner WHERE POOInner.numProId=@numProId AND POOInner.tintType=1 AND POOInner.numDivisionID=PromotionOfferOrganizations.numDivisionID) > 0
												AND (SELECT COUNT(*) FROM PromotionOfferItems POIInner WHERE POIInner.numProId=@numProId AND POIInner.tintRecordType=6 AND POIInner.tintType=1 AND POIInner.numValue=PromotionOfferItemsDiscount.numValue) > 0
											THEN 
												1 
											ELSE 
												0 
										END) 
									WHEN @tintCustomersBasedOn=1 AND @tintOfferBasedOn=4 AND @tintDiscoutBaseOn=2 THEN
										(CASE 
											WHEN 
												(SELECT COUNT(*) FROM PromotionOfferOrganizations POOInner WHERE POOInner.numProId=@numProId AND POOInner.tintType=1 AND POOInner.numDivisionID=PromotionOfferOrganizations.numDivisionID) > 0
												AND (SELECT COUNT(*) FROM PromotionOfferItems POIInner WHERE POIInner.numProId=@numProId AND POIInner.tintRecordType=6 AND POIInner.tintType=2 AND POIInner.numValue=PromotionOfferItemsDiscount.numValue) > 0
											THEN 
												1 
											ELSE 
												0 
										END) 
									WHEN @tintCustomersBasedOn=1 AND @tintOfferBasedOn=4 AND (@tintDiscoutBaseOn=3 OR @tintDiscoutBaseOn=4)  THEN
										(CASE 
											WHEN 
												(SELECT COUNT(*) FROM PromotionOfferOrganizations POOInner WHERE POOInner.numProId=@numProId AND POOInner.tintType=1 AND POOInner.numDivisionID=PromotionOfferOrganizations.numDivisionID) > 0 
											THEN 
												1 
											ELSE 
												0 
										END) 
									WHEN @tintCustomersBasedOn=2 AND @tintOfferBasedOn=1 AND @tintDiscoutBaseOn=1 THEN
										(CASE 
											WHEN 
												(SELECT COUNT(*) FROM PromotionOfferOrganizations POOInner WHERE POOInner.numProId=@numProId AND POOInner.tintType=2 AND POOInner.numRelationship=PromotionOfferOrganizations.numRelationship AND POOInner.numProfile=PromotionOfferOrganizations.numProfile) > 0 
												AND (SELECT COUNT(*) FROM PromotionOfferItems POIInner WHERE POIInner.numProId=@numProId AND POIInner.tintRecordType=5 AND POIInner.tintType=1 AND POIInner.numValue=PromotionOfferItems.numValue) > 0 
												AND (SELECT COUNT(*) FROM PromotionOfferItems POIInner WHERE POIInner.numProId=@numProId AND POIInner.tintRecordType=6 AND POIInner.tintType=1 AND POIInner.numValue=PromotionOfferItemsDiscount.numValue) > 0
											THEN 
												1 
											ELSE 
												0 
										END)
									WHEN @tintCustomersBasedOn=2 AND @tintOfferBasedOn=1 AND @tintDiscoutBaseOn=2 THEN
										(CASE 
											WHEN 
												(SELECT COUNT(*) FROM PromotionOfferOrganizations POOInner WHERE POOInner.numProId=@numProId AND POOInner.tintType=2 AND POOInner.numRelationship=PromotionOfferOrganizations.numRelationship AND POOInner.numProfile=PromotionOfferOrganizations.numProfile) > 0 
												AND (SELECT COUNT(*) FROM PromotionOfferItems POIInner WHERE POIInner.numProId=@numProId AND POIInner.tintRecordType=5 AND POIInner.tintType=1 AND POIInner.numValue=PromotionOfferItems.numValue) > 0 
												AND (SELECT COUNT(*) FROM PromotionOfferItems POIInner WHERE POIInner.numProId=@numProId AND POIInner.tintRecordType=6 AND POIInner.tintType=2 AND POIInner.numValue=PromotionOfferItemsDiscount.numValue) > 0
											THEN 
												1 
											ELSE 
												0 
										END)
									WHEN @tintCustomersBasedOn=2 AND @tintOfferBasedOn=1 AND (@tintDiscoutBaseOn=3 OR @tintDiscoutBaseOn=4)  THEN
										(CASE 
											WHEN 
												(SELECT COUNT(*) FROM PromotionOfferOrganizations POOInner WHERE POOInner.numProId=@numProId AND POOInner.tintType=2 AND POOInner.numRelationship=PromotionOfferOrganizations.numRelationship AND POOInner.numProfile=PromotionOfferOrganizations.numProfile) > 0 
												AND (SELECT COUNT(*) FROM PromotionOfferItems POIInner WHERE POIInner.numProId=@numProId AND POIInner.tintRecordType=5 AND POIInner.tintType=1 AND POIInner.numValue=PromotionOfferItems.numValue) > 0 
											THEN 
												1 
											ELSE 
												0 
										END)
									WHEN @tintCustomersBasedOn=2 AND @tintOfferBasedOn=2 AND @tintDiscoutBaseOn=1 THEN
										(CASE 
											WHEN 
												(SELECT COUNT(*) FROM PromotionOfferOrganizations POOInner WHERE POOInner.numProId=@numProId AND POOInner.tintType=2 AND POOInner.numRelationship=PromotionOfferOrganizations.numRelationship AND POOInner.numProfile=PromotionOfferOrganizations.numProfile) > 0 
												AND (SELECT COUNT(*) FROM PromotionOfferItems POIInner WHERE POIInner.numProId=@numProId AND POIInner.tintRecordType=5 AND POIInner.tintType=2 AND POIInner.numValue=PromotionOfferItems.numValue) > 0
												AND (SELECT COUNT(*) FROM PromotionOfferItems POIInner WHERE POIInner.numProId=@numProId AND POIInner.tintRecordType=6 AND POIInner.tintType=1 AND POIInner.numValue=PromotionOfferItemsDiscount.numValue) > 0
											THEN 
												1 
											ELSE 
												0 
										END)
									WHEN @tintCustomersBasedOn=2 AND @tintOfferBasedOn=2 AND @tintDiscoutBaseOn=2 THEN
										(CASE 
											WHEN 
												(SELECT COUNT(*) FROM PromotionOfferOrganizations POOInner WHERE POOInner.numProId=@numProId AND POOInner.tintType=2 AND POOInner.numRelationship=PromotionOfferOrganizations.numRelationship AND POOInner.numProfile=PromotionOfferOrganizations.numProfile) > 0 
												AND (SELECT COUNT(*) FROM PromotionOfferItems POIInner WHERE POIInner.numProId=@numProId AND POIInner.tintRecordType=5 AND POIInner.tintType=2 AND POIInner.numValue=PromotionOfferItems.numValue) > 0 
												AND (SELECT COUNT(*) FROM PromotionOfferItems POIInner WHERE POIInner.numProId=@numProId AND POIInner.tintRecordType=6 AND POIInner.tintType=2 AND POIInner.numValue=PromotionOfferItemsDiscount.numValue) > 0
											THEN 
												1 
											ELSE 
												0 
										END)
									WHEN @tintCustomersBasedOn=2 AND @tintOfferBasedOn=2 AND (@tintDiscoutBaseOn=3 OR @tintDiscoutBaseOn=4)  THEN
										(CASE 
											WHEN 
												(SELECT COUNT(*) FROM PromotionOfferOrganizations POOInner WHERE POOInner.numProId=@numProId AND POOInner.tintType=2 AND POOInner.numRelationship=PromotionOfferOrganizations.numRelationship AND POOInner.numProfile=PromotionOfferOrganizations.numProfile) > 0 
												AND (SELECT COUNT(*) FROM PromotionOfferItems POIInner WHERE POIInner.numProId=@numProId AND POIInner.tintRecordType=5 AND POIInner.tintType=2 AND POIInner.numValue=PromotionOfferItems.numValue) > 0 
											THEN 
												1 
											ELSE 
												0 
										END)
									WHEN @tintCustomersBasedOn=2 AND @tintOfferBasedOn=4 AND @tintDiscoutBaseOn=1 THEN
										(CASE 
											WHEN 
												(SELECT COUNT(*) FROM PromotionOfferOrganizations POOInner WHERE POOInner.numProId=@numProId AND POOInner.tintType=2 AND POOInner.numRelationship=PromotionOfferOrganizations.numRelationship AND POOInner.numProfile=PromotionOfferOrganizations.numProfile) > 0 
												AND (SELECT COUNT(*) FROM PromotionOfferItems POIInner WHERE POIInner.numProId=@numProId AND POIInner.tintRecordType=6 AND POIInner.tintType=1 AND POIInner.numValue=PromotionOfferItemsDiscount.numValue) > 0
											THEN 
												1 
											ELSE 
												0 
										END)
									WHEN @tintCustomersBasedOn=2 AND @tintOfferBasedOn=4 AND @tintDiscoutBaseOn=2 THEN
										(CASE 
											WHEN 
												(SELECT COUNT(*) FROM PromotionOfferOrganizations POOInner WHERE POOInner.numProId=@numProId AND POOInner.tintType=2 AND POOInner.numRelationship=PromotionOfferOrganizations.numRelationship AND POOInner.numProfile=PromotionOfferOrganizations.numProfile) > 0
												AND (SELECT COUNT(*) FROM PromotionOfferItems POIInner WHERE POIInner.numProId=@numProId AND POIInner.tintRecordType=6 AND POIInner.tintType=2 AND POIInner.numValue=PromotionOfferItemsDiscount.numValue) > 0
											THEN 
												1 
											ELSE 
												0 
										END)
									WHEN @tintCustomersBasedOn=2 AND @tintOfferBasedOn=4 AND (@tintDiscoutBaseOn=3 OR @tintDiscoutBaseOn=4)  THEN
										(CASE 
											WHEN 
												(SELECT COUNT(*) FROM PromotionOfferOrganizations POOInner WHERE POOInner.numProId=@numProId AND POOInner.tintType=2 AND POOInner.numRelationship=PromotionOfferOrganizations.numRelationship AND POOInner.numProfile=PromotionOfferOrganizations.numProfile) > 0 
											THEN 
												1 
											ELSE 
												0 
										END)
									WHEN @tintCustomersBasedOn=3 AND @tintOfferBasedOn=1 AND @tintDiscoutBaseOn=1 THEN
										(CASE 
											WHEN 
												(SELECT COUNT(*) FROM PromotionOfferItems POIInner WHERE POIInner.numProId=@numProId AND POIInner.tintRecordType=5 AND POIInner.tintType=1 AND POIInner.numValue=PromotionOfferItems.numValue) > 0
												AND (SELECT COUNT(*) FROM PromotionOfferItems POIInner WHERE POIInner.numProId=@numProId AND POIInner.tintRecordType=6 AND POIInner.tintType=1 AND POIInner.numValue=PromotionOfferItemsDiscount.numValue) > 0
											THEN 
												1 
											ELSE 
												0 
										END)
									WHEN @tintCustomersBasedOn=3 AND @tintOfferBasedOn=1 AND @tintDiscoutBaseOn=2 THEN
										(CASE 
											WHEN 
												(SELECT COUNT(*) FROM PromotionOfferItems POIInner WHERE POIInner.numProId=@numProId AND POIInner.tintRecordType=5 AND POIInner.tintType=1 AND POIInner.numValue=PromotionOfferItems.numValue) > 0 
												AND (SELECT COUNT(*) FROM PromotionOfferItems POIInner WHERE POIInner.numProId=@numProId AND POIInner.tintRecordType=6 AND POIInner.tintType=2 AND POIInner.numValue=PromotionOfferItemsDiscount.numValue) > 0
											THEN 
												1 
											ELSE 
												0 
										END)
									WHEN @tintCustomersBasedOn=3 AND @tintOfferBasedOn=1 AND (@tintDiscoutBaseOn=3 OR @tintDiscoutBaseOn=4)  THEN
										(CASE 
											WHEN 
												(SELECT COUNT(*) FROM PromotionOfferItems POIInner WHERE POIInner.numProId=@numProId AND POIInner.tintRecordType=5 AND POIInner.tintType=1 AND POIInner.numValue=PromotionOfferItems.numValue) > 0 
											THEN 
												1 
											ELSE 
												0 
										END)
									WHEN @tintCustomersBasedOn=3 AND @tintOfferBasedOn=2 AND @tintDiscoutBaseOn=1 THEN
										(CASE 
											WHEN 
												(SELECT COUNT(*) FROM PromotionOfferItems POIInner WHERE POIInner.numProId=@numProId AND POIInner.tintRecordType=5 AND POIInner.tintType=2 AND POIInner.numValue=PromotionOfferItems.numValue) > 0
												AND (SELECT COUNT(*) FROM PromotionOfferItems POIInner WHERE POIInner.numProId=@numProId AND POIInner.tintRecordType=6 AND POIInner.tintType=1 AND POIInner.numValue=PromotionOfferItemsDiscount.numValue) > 0
											THEN 
												1 
											ELSE 
												0 
										END)
									WHEN @tintCustomersBasedOn=3 AND @tintOfferBasedOn=2 AND @tintDiscoutBaseOn=2 THEN
										(CASE 
											WHEN 
												(SELECT COUNT(*) FROM PromotionOfferItems POIInner WHERE POIInner.numProId=@numProId AND POIInner.tintRecordType=5 AND POIInner.tintType=2 AND POIInner.numValue=PromotionOfferItems.numValue) > 0 
												AND (SELECT COUNT(*) FROM PromotionOfferItems POIInner WHERE POIInner.numProId=@numProId AND POIInner.tintRecordType=6 AND POIInner.tintType=2 AND POIInner.numValue=PromotionOfferItemsDiscount.numValue) > 0
											THEN 
												1 
											ELSE 
												0 
										END)
									WHEN @tintCustomersBasedOn=3 AND @tintOfferBasedOn=2 AND (@tintDiscoutBaseOn=3 OR @tintDiscoutBaseOn=4) THEN
										(CASE 
											WHEN 
												(SELECT COUNT(*) FROM PromotionOfferItems POIInner WHERE POIInner.numProId=@numProId AND POIInner.tintRecordType=5 AND POIInner.tintType=2 AND POIInner.numValue=PromotionOfferItems.numValue) > 0 
											THEN 
												1 
											ELSE 
												0 
										END)
									WHEN @tintCustomersBasedOn=3 AND @tintOfferBasedOn=4 AND @tintDiscoutBaseOn=1 THEN
										(CASE 
											WHEN 
												(SELECT COUNT(*) FROM PromotionOfferItems POIInner WHERE POIInner.numProId=@numProId AND POIInner.tintRecordType=6 AND POIInner.tintType=1 AND POIInner.numValue=PromotionOfferItemsDiscount.numValue) > 0
											THEN 
												1 
											ELSE 
												0 
										END)
									WHEN @tintCustomersBasedOn=3 AND @tintOfferBasedOn=4 AND @tintDiscoutBaseOn=2 THEN
										(CASE 
											WHEN 
												(SELECT COUNT(*) FROM PromotionOfferItems POIInner WHERE POIInner.numProId=@numProId AND POIInner.tintRecordType=6 AND POIInner.tintType=2 AND POIInner.numValue=PromotionOfferItemsDiscount.numValue) > 0
											THEN 
												1 
											ELSE 
												0 
										END)
									WHEN @tintCustomersBasedOn=3 AND @tintOfferBasedOn=4 AND (@tintDiscoutBaseOn=3 OR @tintDiscoutBaseOn=4)  THEN 3
								END)
					) > 0
				BEGIN
					DECLARE @ERRORMESSAGEVALIDATION AS VARCHAR(50)
					SET @ERRORMESSAGEVALIDATION = (CASE 
									WHEN @tintCustomersBasedOn=1 AND @tintOfferBasedOn=1 THEN 'ORGANIZATION-ITEM'
									WHEN @tintCustomersBasedOn=1 AND @tintOfferBasedOn=2 THEN 'ORGANIZATION-ITEMCLASSIFICATIONS'
									WHEN @tintCustomersBasedOn=1 AND @tintOfferBasedOn=4 THEN 'ORGANIZATION-ALLITEMS'
									WHEN @tintCustomersBasedOn=2 AND @tintOfferBasedOn=1 THEN 'RELATIONSHIPPROFILE-ITEMS'
									WHEN @tintCustomersBasedOn=2 AND @tintOfferBasedOn=2 THEN 'RELATIONSHIPPROFILE-ITEMCLASSIFICATIONS'
									WHEN @tintCustomersBasedOn=2 AND @tintOfferBasedOn=4 THEN 'RELATIONSHIPPROFILE-ALLITEMS'
									WHEN @tintCustomersBasedOn=3 AND @tintOfferBasedOn=1 THEN 'ALLORGANIZATIONS-ITEMS'
									WHEN @tintCustomersBasedOn=3 AND @tintOfferBasedOn=2 THEN 'ALLORGANIZATIONS-ITEMCLASSIFICATIONS'
									WHEN @tintCustomersBasedOn=3 AND @tintOfferBasedOn=4 THEN 'ALLORGANIZATIONS-ALLITEMS'
							END)
					RAISERROR(@ERRORMESSAGEVALIDATION,16,1)
					RETURN
				END
			END
			ELSE
			BEGIN
				DELETE FROM PromotionOfferorganizations WHERE numProId=@numProId
			END

			UPDATE 
				PromotionOffer
			SET 
				vcProName = @vcProName
				,numDomainId = @numDomainId
				,dtValidFrom = @dtValidFrom
				,dtValidTo = @dtValidTo
				,bitNeverExpires = @bitNeverExpires
				,bitUseOrderPromotion=@bitUseOrderPromotion
				,numOrderPromotionID=(CASE WHEN ISNULL(@bitUseOrderPromotion,0)=1 THEN @numOrderPromotionID ELSE 0 END)
				,tintOfferTriggerValueType = @tintOfferTriggerValueType
				,fltOfferTriggerValue = @fltOfferTriggerValue
				,tintOfferBasedOn = @tintOfferBasedOn
				,tintDiscountType = @tintDiscountType
				,fltDiscountValue = @fltDiscountValue
				,tintDiscoutBaseOn = @tintDiscoutBaseOn
				,numModifiedBy = @numUserCntID
				,dtModified = GETUTCDATE()
				,tintCustomersBasedOn = @tintCustomersBasedOn
				,tintOfferTriggerValueTypeRange = @tintOfferTriggerValueTypeRange
				,fltOfferTriggerValueRange = @fltOfferTriggerValueRange
				,vcShortDesc = @vcShortDesc
				,vcLongDesc = @vcLongDesc
				,tintItemCalDiscount = @tintItemCalDiscount
				,bitError=0
				,monDiscountedItemPrice=@monDiscountedItemPrice
			WHERE 
				numProId=@numProId
		COMMIT
		END TRY
		BEGIN CATCH
			DECLARE @ErrorMessage NVARCHAR(4000)
			DECLARE @ErrorNumber INT
			DECLARE @ErrorSeverity INT
			DECLARE @ErrorState INT
			DECLARE @ErrorLine INT
			DECLARE @ErrorProcedure NVARCHAR(200)

			IF @@TRANCOUNT > 0
				ROLLBACK TRANSACTION;

			UPDATE PromotionOffer SET bitEnabled=0,bitError=1 WHERE numProId=@numProId 

			SELECT 
				@ErrorMessage = ERROR_MESSAGE(),
				@ErrorNumber = ERROR_NUMBER(),
				@ErrorSeverity = ERROR_SEVERITY(),
				@ErrorState = ERROR_STATE(),
				@ErrorLine = ERROR_LINE(),
				@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

			RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);
		END CATCH
        
		SELECT  @numProId
	END
END
