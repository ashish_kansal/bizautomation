GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_PromotionOffer_ClearCouponCodeECommerce')
DROP PROCEDURE USP_PromotionOffer_ClearCouponCodeECommerce
GO
CREATE PROCEDURE [dbo].[USP_PromotionOffer_ClearCouponCodeECommerce]
	@numDomainID NUMERIC(18,0),
	@numUserCntId NUMERIC(18,0),
	@numSiteID NUMERIC(18,0),
	@vcCookieId VARCHAR(100),
	@numPromotionID NUMERIC(18,0)
AS
BEGIN
	UPDATE 
		CartItems
	SET 
		PromotionID=0
		,bitPromotionTrigerred=0
		,PromotionDesc=''
		,bitPromotionDiscount=0
		,monPrice=ISNULL(monInsertPrice,0)
		,fltDiscount=ISNULL(fltInsertDiscount,0)
		,bitDiscountType=ISNULL(bitInsertDiscountType,0)
		,monTotAmtBefDiscount = ISNULL(numUnitHour,0) * ISNULL(monInsertPrice,0)
		,monTotAmount = (CASE 
							WHEN ISNULL(fltInsertDiscount,0) > 0
							THEN (CASE 
									WHEN ISNULL(bitInsertDiscountType,0) = 1 
									THEN (CASE 
											WHEN (ISNULL(numUnitHour,0) * ISNULL(monInsertPrice,0)) - fltInsertDiscount >= 0
											THEN (ISNULL(numUnitHour,0) * ISNULL(monInsertPrice,0)) - fltInsertDiscount
											ELSE 0
										END)
									ELSE ISNULL(numUnitHour,0) * (ISNULL(monInsertPrice,0) - (ISNULL(monInsertPrice,0) * (fltInsertDiscount/100)))
								END)
							ELSE ISNULL(numUnitHour,0) * ISNULL(monInsertPrice,0)
						END)
	WHERE 
		numDomainId=@numDomainID
		AND vcCookieId=@vcCookieId  
		AND numUserCntId=@numUserCntId 
		AND ISNULL(PromotionID,0)=@numPromotionID
END