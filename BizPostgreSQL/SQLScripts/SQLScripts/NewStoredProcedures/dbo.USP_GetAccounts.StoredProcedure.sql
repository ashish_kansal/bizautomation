GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_GetAccounts' ) 
    DROP PROCEDURE USP_GetAccounts
GO
/****** Object:  StoredProcedure [dbo].[USP_ChartofChildAccounts]    Script Date: 09/25/2009 16:21:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[USP_GetAccounts]
    (
      @numAccountID NUMERIC(9, 0),
      @numDomainID NUMERIC(9, 0),
      @numParntAcntTypeId NUMERIC(9, 0)
    )
AS 
    BEGIN

        DECLARE @vcAccountCode VARCHAR(50)
        
        SELECT  @vcAccountCode = vcAccountCode
        FROM    dbo.AccountTypeDetail
        WHERE   numAccountTypeID = @numParntAcntTypeId
				
        IF LEN(@vcAccountCode) > 0 
            BEGIN
                IF ( @numAccountID = 0 ) 
                    BEGIN
                        SELECT DISTINCT Chart_Of_Accounts.numAccountId,
                                Chart_Of_Accounts.vcAccountCode,
                                Chart_Of_Accounts.vcAccountName,
                                Chart_Of_Accounts.vcAccountDescription,
                                Chart_Of_Accounts.numAcntTypeId,
                                Chart_Of_Accounts.numParentAccId
                        FROM    dbo.Chart_Of_Accounts
                                LEFT JOIN Chart_Of_Accounts C ON C.numParentAccId = dbo.Chart_Of_Accounts.numAccountId
                        WHERE   Chart_Of_Accounts.numDomainId = @numDomainID
                                AND Chart_Of_Accounts.vcAccountCode LIKE @vcAccountCode
                                + '%'
                                AND ( Chart_Of_Accounts.numAccountId <> @numAccountID
                                      OR ISNULL(@numAccountID, 0) = 0
                                    )
--                                AND ( SELECT    COUNT(*)
--                                      FROM      dbo.Chart_Of_Accounts COA
--                                      WHERE     COA.numAccountId = C.numParentAccId
--                                    ) = 0     	
                    END
                ELSE 
                    BEGIN
                        SELECT  numAccountId,
                                vcAccountCode,
                                vcAccountName,
                                vcAccountDescription,
                                numAcntTypeId
                        FROM    dbo.Chart_Of_Accounts
                        WHERE   numDomainId = @numDomainID
                                AND vcAccountCode LIKE @vcAccountCode + '%'
                                AND numAccountId <> @numAccountID
                    END
                
            END

    END    