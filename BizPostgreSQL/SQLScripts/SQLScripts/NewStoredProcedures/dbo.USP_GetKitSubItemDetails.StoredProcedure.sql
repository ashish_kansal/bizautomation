GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetKitSubItemDetails')
DROP PROCEDURE USP_GetKitSubItemDetails
GO
CREATE PROCEDURE USP_GetKitSubItemDetails
(
	@numItemDetailID		BIGINT,
	@numDomainID			NUMERIC(18)
)
AS 
BEGIN
	
	SELECT  ISNULL(ID.numItemDetailID,0) AS [numItemDetailID],
			ISNULL(ID.numItemKitID,0) AS [numItemKitID],
			ISNULL(ID.numChildItemID,'') AS [numChildItemID],
			ISNULL(ID.numQtyItemsReq,'') AS [numQtyItemsReq],
			ISNULL(ID.numWareHouseItemId,0) AS [numWareHouseItemId],
			ISNULL(ID.numUOMId,0) AS [numUOMId],
			ISNULL(ID.vcItemDesc,0) AS [vcItemDesc],
			ISNULL(ID.sintOrder,0) AS [sintOrder]
	FROM  dbo.ItemDetails ID	
	JOIN dbo.Item I ON ID.numItemKitID = I.numItemCode
	WHERE ID.numItemDetailID = @numItemDetailID 
	AND I.numDomainID = @numDomainID
	
END