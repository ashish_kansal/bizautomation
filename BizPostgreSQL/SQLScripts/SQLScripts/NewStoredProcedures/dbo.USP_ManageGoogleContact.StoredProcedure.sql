SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
  
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ManageGoogleContact')
DROP PROCEDURE USP_ManageGoogleContact
GO
CREATE PROCEDURE [dbo].[USP_ManageGoogleContact]  
--@numUserCntId as numeric(9),
@numDomainID as numeric(9),  
--@vcName as varchar(100)='',
@vcEmail as varchar(50)='',
@vcPhone as varchar(15)=''
--@txtNotes as text=''
as  
 
 DECLARE @vcdomain AS VARCHAR(20)
 DECLARE @numCompanyID AS NUMERIC(9)
 DECLARE @numDivisionId AS NUMERIC(9)
 DECLARE @numContactId AS NUMERIC(9)

 SELECT @vcdomain=case when charindex('@',@vcEmail,1) > 0 then substring(@vcEmail, charindex('@',@vcEmail,1)+1,LEN(@vcEmail)-CHARINDEX('@', @vcEmail)) ELSE '' END
 SET @vcPhone=REPLACE(REPLACE(REPLACE(ISNULL(@vcPhone,''),'(',''),')',''),'-','')
 
 --Check with Email Domain and Phone No

 
 SELECT TOP 1 C.numCompanyID,D.numDivisionID from AdditionalContactsInformation A  
		join DivisionMaster D  on D.numDivisionID=A.numDivisionID  join CompanyInfo C  on C.numCompanyID=D.numCompanyID  
		where (ISNULL(vcEmail,'') LIKE '%' + @vcdomain OR 1=(CASE WHEN LEN(@vcPhone)>0 then CASE WHEN REPLACE(REPLACE(REPLACE(ISNULL(numPhone,0),'(',''),')',''),'-','')=@vcPhone THEN 1 ELSE 0 END ELSE 0 END))
		AND A.[numDomainID] = @numDomainId 
		AND @vcdomain NOT IN (SELECT isnull(vcRenamedListName,vcData) as vcData FROM listdetails Ld        
							  left join listorder LO on Ld.numListItemID= LO.numListItemID and LO.numDomainId = @numDomainID 
							  WHERE Ld.numListID=321 and (constFlag=1 or Ld.numDomainID=@numDomainID ) )
