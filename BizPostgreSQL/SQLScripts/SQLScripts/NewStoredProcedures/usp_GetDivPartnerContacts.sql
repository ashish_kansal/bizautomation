USE [Production.2014]
GO
/****** Object:  StoredProcedure [dbo].[usp_GetDivPartnerContacts]    Script Date: 11/10/2016 11:57:05 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_GetDivPartnerContacts')
DROP PROCEDURE usp_GetDivPartnerContacts
GO
CREATE PROCEDURE [dbo].[usp_GetDivPartnerContacts]                                                                                    
 @numDomainID  numeric=0,
 @numOppID numeric=0    ,
 @DivisionID numeric=0                                 
AS                                                                       
BEGIN   
	IF(@numOppID>0 AND @DivisionID=0)
	BEGIN
		SET @DivisionID=(SELECT TOP 1 numPartenerSource FROM DivisionMaster where numDivisionID=@numOppID and numDomainId=@numDomainID)
	END
	SELECT A.numContactId,A.vcFirstName+' '+A.vcLastName AS vcGivenName FROM AdditionalContactsInformation AS A 
	LEFT JOIN DivisionMaster AS D 
	ON D.numDivisionID=A.numDivisionId
	WHERE D.numDomainID=@numDomainID AND D.numDivisionID=@DivisionID AND A.vcGivenName<>''

END

