GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ProjectsMaster_GetFieldValue')
DROP PROCEDURE USP_ProjectsMaster_GetFieldValue
GO
CREATE PROCEDURE [dbo].[USP_ProjectsMaster_GetFieldValue]                                   
(                                                   
	@numDomainID NUMERIC(18,0),    
	@numProId NUMERIC(18,0),
	@vcFieldName VARCHAR(100)                           
)                                    
as
BEGIN
	IF @vcFieldName = 'numAddressID'
	BEGIN
		SELECT numAddressID FROM ProjectsMaster WHERE numDomainId=@numDomainID AND numProId=@numProId
	END
	ELSE IF @vcFieldName = 'numDivisionID'
	BEGIN
		SELECT numDivisionID FROM ProjectsMaster WHERE numDomainId=@numDomainID AND numProId=@numProId
	END
	ELSE IF @vcFieldName = 'numIntPrjMgr'
	BEGIN
		SELECT numIntPrjMgr FROM ProjectsMaster WHERE numDomainId=@numDomainID AND numProId=@numProId
	END
	ELSE IF @vcFieldName = 'vcCompanyName'
	BEGIN
		SELECT 
			CompanyInfo.vcCompanyName
		FROM 
			ProjectsMaster WITH(NOLOCK)
		INNER JOIN DivisionMaster WITH(NOLOCK) ON ProjectsMaster.numDivisionId=DivisionMaster.numDivisionID 
		INNER JOIN CompanyInfo WITH(NOLOCK) ON DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
		WHERE 
			ProjectsMaster.numDomainId=@numDomainID 
			AND ProjectsMaster.numProId=@numProId
	END
	ELSE
	BEGIN
		RAISERROR('FIELD_NOT_FOUND',16,1)
	END
END
GO