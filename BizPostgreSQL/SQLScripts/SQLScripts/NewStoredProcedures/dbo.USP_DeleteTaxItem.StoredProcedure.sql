

--Created By Anoop Jayaraj
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_DeleteTaxItem')
DROP PROCEDURE USP_DeleteTaxItem
GO
CREATE PROCEDURE USP_DeleteTaxItem
@numTaxItemID as numeric(9),
@numDomainID as numeric(9)
as

declare @numSalesBizDocFormFieldID as numeric(9)
declare @numPurBizDocFormFieldID as numeric(9)
declare @numBizDocSummaryFormFieldID as numeric(9)
declare @numChartActID as numeric(9)

select   @numSalesBizDocFormFieldID=numSalesBizDocFormFieldID,
@numPurBizDocFormFieldID=numPurBizDocFormFieldID,
@numBizDocSummaryFormFieldID=numBizDocSummaryFormFieldID,
@numChartActID=numChartOfAcntID
from TaxItems
where numTaxItemID=@numTaxItemID

delete from DycFormConfigurationDetails where numFieldID=@numSalesBizDocFormFieldID and numDomainID=@numDomainID

delete from DycFormField_Mapping where numFieldID=@numSalesBizDocFormFieldID and numDomainID=@numDomainID

delete from DycFieldMaster where numFieldID=@numSalesBizDocFormFieldID and numDomainID=@numDomainID

--Commented By chintan Now deleting Account is not required
--exec USP_DeleteChartCategory @numChartActID,@numDomainId

delete from TaxDetails where numTaxItemID=@numTaxItemID
delete from TaxItems Where numTaxItemID=@numTaxItemID

