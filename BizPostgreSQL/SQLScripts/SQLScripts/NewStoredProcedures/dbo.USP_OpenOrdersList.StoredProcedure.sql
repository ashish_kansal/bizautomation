SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_OpenOrdersList')
DROP PROCEDURE USP_OpenOrdersList
GO
CREATE PROCEDURE [dbo].[USP_OpenOrdersList]
    @numUserCntID              NUMERIC(9)  = 0,
    @numDomainID               NUMERIC(9)  = 0,
    @SortChar                  CHAR(1)  = '0',
    @CurrentPage               INT,
    @PageSize                  INT,
    @TotRecs                   INT  OUTPUT,
    @columnName                AS VARCHAR(50),
    @columnSortOrder           AS VARCHAR(10),
    @ClientTimeZoneOffset      AS INT,
	@vcRegularSearchCriteria varchar(1000)='',
	@vcCustomSearchCriteria varchar(1000)='',
	@numViewID as numeric(9) 
AS
  DECLARE  @PageId  AS TINYINT
  DECLARE  @numFormId  AS INT;SET @numFormId=57
  
  DECLARE  @strSql  AS VARCHAR(8000)
  DECLARE  @strFrom  AS VARCHAR(2000)

  DECLARE  @tintOrder  AS TINYINT
  DECLARE  @vcFieldName  AS VARCHAR(50)
  DECLARE  @vcListItemType  AS VARCHAR(3)
  DECLARE  @vcListItemType1  AS VARCHAR(1)
  DECLARE  @vcAssociatedControlType VARCHAR(30)
  DECLARE  @numListID  AS NUMERIC(9)
  DECLARE  @vcDbColumnName VARCHAR(40)
  DECLARE  @WhereCondition VARCHAR(2000)
  DECLARE  @vcLookBackTableName VARCHAR(2000)
  DECLARE  @bitCustom  AS BIT
  Declare @numFieldId as numeric  
  DECLARE @bitAllowSorting AS CHAR(1)   
DECLARE @bitAllowEdit AS CHAR(1)                 
declare @vcColumnName AS VARCHAR(500)                  

  SET @tintOrder = 0
  SET @WhereCondition = ''
  DECLARE  @Nocolumns  AS TINYINT
  SET @Nocolumns = 0


CREATE TABLE #tempForm (tintOrder TINYINT,vcDbColumnName nvarchar(50),vcOrigDbColumnName nvarchar(50),vcFieldName nvarchar(50),vcAssociatedControlType nvarchar(50),
  vcListItemType varchar(3),numListID numeric(9),vcLookBackTableName varchar(50),bitCustomField BIT,
  numFieldId NUMERIC,bitAllowSorting BIT,bitAllowEdit BIT,bitIsRequired BIT,bitIsEmail BIT,bitIsAlphaNumeric BIT,bitIsNumeric BIT,
  bitIsLengthValidation BIT,intMaxLength int,intMinLength int,bitFieldMessage BIT,vcFieldMessage VARCHAR(500),ListRelID numeric(9),intColumnWidth int,bitAllowFiltering BIT,vcFieldDataType CHAR(1))
  
DECLARE @numViewDomainID AS NUMERIC(9),@numViewUserCntID AS NUMERIC(9)
IF @numViewID=1 OR @numViewID=2
BEGIN
	SET @numViewDomainID=0
	SET @numViewUserCntID=0
END 
ELSE 
BEGIN
	SET @numViewDomainID=@numDomainID
	SET @numViewUserCntID=@numUserCntID
END
 
select @Nocolumns=isnull(sum(TotalRow),0)  from(            
Select count(*) TotalRow from View_DynamicColumns where numFormId=@numFormId and numUserCntID=@numViewUserCntID and numDomainID=@numViewDomainID AND tintPageType=1 and numViewID=@numViewID
Union 
Select count(*) TotalRow from View_DynamicCustomColumns where numFormId=@numFormId and numUserCntID=@numViewUserCntID and numDomainID=@numViewDomainID AND tintPageType=1 and numViewID=@numViewID) TotalRows
 
if @Nocolumns=0
BEGIN
	INSERT INTO DycFormConfigurationDetails (numFormID,numFieldID,intColumnNum,intRowNum,numDomainID,numUserCntID,tintPageType,bitCustom,intColumnWidth,numViewID)
	select @numFormId,numFieldId,0,Row_number() over(order by tintRow desc),@numDomainID,@numUserCntID,1,0,intColumnWidth,@numViewID
	 FROM View_DynamicDefaultColumns
	 where numFormId=@numFormId and bitDefault=1 AND ISNULL(bitSettingField,0)=1 and numDomainID=@numDomainID
	order by tintOrder asc   
END

    
    INSERT INTO #tempForm
select tintRow+1 as tintOrder,vcDbColumnName,vcOrigDbColumnName,ISNULL(vcCultureFieldName,vcFieldName),                  
 vcAssociatedControlType,vcListItemType,numListID,vcLookBackTableName                                                 
,bitCustom,numFieldId,bitAllowSorting,bitInlineEdit,
bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage vcFieldMessage,ListRelID,intColumnWidth,bitAllowFiltering,vcFieldDataType 
 FROM View_DynamicColumns 
 where numFormId=@numFormId and numUserCntID=@numViewUserCntID and numDomainID=@numViewDomainID AND tintPageType=1 
  AND ISNULL(bitSettingField,0)=1 AND ISNULL(bitCustom,0)=0 and numViewID=@numViewID 
 
 UNION
    
     select tintRow+1 as tintOrder,vcDbColumnName,vcDbColumnName,vcFieldName,                  
 vcAssociatedControlType,'' as vcListItemType,numListID,''                                                 
,bitCustom,numFieldId,bitAllowSorting,bitAllowEdit,
bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage,ListRelID,intColumnWidth,0,'' 
 from View_DynamicCustomColumns
 where numFormId=@numFormId and numUserCntID=@numViewUserCntID and numDomainID=@numViewDomainID AND tintPageType=1 
 AND ISNULL(bitCustom,0)=1  and numViewID=@numViewID
 
  order by tintOrder asc  



Declare @ListRelID as numeric(9) 

 SET @strSql = ''
 SET @strSql = ' select ADC.numContactId,DM.numDivisionID,isnull(DM.numTerID,0) numTerID,Opp.numRecOwner as numRecOwner,DM.tintCRMType,Opp.numOppId,OBD.numOppBizDocsID'


SET @strFrom ='FROM OpportunityMaster Opp 
							 JOIN OpportunityBizDocs OBD ON Opp.numOppId = OBD.numOppId --and (OBD.bitAuthoritativeBizDocs = 1 OR OBD.bitAuthoritativeBizDocs = 0)
						     JOIN AdditionalContactsInformation ADC ON Opp.numContactId = ADC.numContactId                                                               
                             JOIN DivisionMaster DM ON Opp.numDivisionId = DM.numDivisionID AND ADC.numDivisionId = DM.numDivisionID                                                               
                             JOIN CompanyInfo cmp ON DM.numCompanyID = cmp.numCompanyId 
                             left JOIN OpportunityLinking OL on OL.numChildOppID=Opp.numOppId left join WebAPI on WebAPI.WebApiId=isnull(OL.numWebApiId,0)
							left join Sites on isnull(OL.numSiteID,0)=Sites.numSIteID and Sites.numDomainID=' + convert(varchar(15),@numDomainID)
							

							
  select top 1 @tintOrder=tintOrder+1,@vcDbColumnName=vcDbColumnName,@vcFieldName=vcFieldName,                
  @vcAssociatedControlType=vcAssociatedControlType,@vcListItemType=vcListItemType,@numListID=numListID,@vcLookBackTableName=vcLookBackTableName                                               
,@bitCustom=bitCustomField,@numFieldId=numFieldId,@bitAllowSorting=bitAllowSorting,@bitAllowEdit=bitAllowEdit,
@ListRelID=ListRelID
  from  #tempForm order by tintOrder asc            

CREATE TABLE #tempOpp(
	numOppID [numeric](18, 0),
	numDivisionID [numeric](18, 0),
	tintBillToType tinyint,
	tintShipToType tinyint,
) 

CREATE TABLE #tempOppAddress(
	numOppID [numeric](18, 0) NULL,
	vcBillStreet [varchar](100) NULL,
	vcBillCity [varchar](50) NULL,
	numBillState [numeric](18, 0) NULL,
	vcBillPostCode [varchar](15) NULL,
	numBillCountry [numeric](18, 0) NULL,
	vcShipStreet [varchar](100) NULL,
	vcShipCity [varchar](50) NULL,
	numShipState [numeric](18, 0) NULL,
	vcShipPostCode [varchar](15) NULL,
	numShipCountry [numeric](18, 0) NULL
) 

  WHILE @tintOrder > 0
    BEGIN
      IF @bitCustom = 0
        BEGIN
          DECLARE  @Prefix  AS VARCHAR(5)
          IF @vcLookBackTableName = 'AdditionalContactsInformation'
            SET @Prefix = 'ADC.'
		  IF @vcLookBackTableName = 'DivisionMaster'
            SET @Prefix = 'DM.'
          IF @vcLookBackTableName = 'CompanyInfo'
            SET @Prefix = 'CMP.'
          IF @vcLookBackTableName = 'OpportunityMaster'
            SET @PreFix = 'Opp.'
          IF @vcLookBackTableName = 'OpportunityBizDocs'
            SET @PreFix = 'OBD.'
		  IF @vcLookBackTableName = 'Item'
            SET @PreFix = 'I.'
		  IF @vcLookBackTableName = 'OpportunityBizDocItems'
            SET @PreFix = 'OBDI.'
		  IF @vcLookBackTableName = 'OpportunityItems'
            SET @PreFix = 'OI.'
		  IF @vcLookBackTableName = 'WareHouseItems'
            SET @PreFix = 'WHI.'
		  IF @vcLookBackTableName = 'Warehouses'
            SET @PreFix = 'WH.'
          IF @vcLookBackTableName = 'AddressDetails'
            SET @PreFix = 'AD.'
		  IF @vcLookBackTableName = 'OpportunityLinking'
            SET @PreFix = 'OL.'

		 IF @vcLookBackTableName='Item' or @vcLookBackTableName='OpportunityBizDocItems' or @vcLookBackTableName='OpportunityItems'	or @vcLookBackTableName='Warehouses' or @vcLookBackTableName='WareHouseItems' OR @vcLookBackTableName = 'ShippingBox'
			BEGIN
				IF CHARINDEX('Item I', @strFrom)=0 AND CHARINDEX('OpportunityBizDocItems OBDI', @strFrom)=0 AND CHARINDEX('OpportunityItems OI', @strFrom)=0 AND CHARINDEX('ShippingBox SB',@strFrom) = 0
					BEGIN
					SET @strFrom=@strFrom + ' JOIN [OpportunityItems] OI ON Opp.[numOppId] = OI.[numOppId]
                      					   JOIN [OpportunityBizDocItems] OBDI ON OBDI.numOppItemID= OI.numoppitemtCode and  OBD.numOppBizDocsID=OBDI.numOppBizDocID 
										   join Item I on OI.numItemCode=I.numItemCode '

					SET @strSql=@strSql +',OBDI.numOppBizDocItemID'
					IF ISNULL(@numViewID,0) = 2
						BEGIN
							SET @strSql=@strSql +',OI.numoppitemtCode '
						END
					END
			END
--		 IF @vcLookBackTableName = 'ShippingBox'
--			BEGIN
--				IF CHARINDEX('ShippingBox SB') = 0
--				BEGIN
--					SET @strFrom = @strFrom + ' '
--				END
--			END
		 IF @vcLookBackTableName='Warehouses' or @vcLookBackTableName='WareHouseItems'	
			BEGIN
				IF CHARINDEX('Warehouses WH', @strFrom)=0 AND CHARINDEX('WareHouseItems WHI', @strFrom)=0 
					SET @strFrom=@strFrom + ' LEFT JOIN WareHouseItems WHI ON OI.numWarehouseItmsID = WHI.numWareHouseItemID AND WHI.numDomainID= '+convert(varchar(15),@numDomainID)+'
											  LEFT JOIN dbo.Warehouses WH ON WHI.numDomainID = WH.numDomainID AND WHI.numWareHouseID = WH.numWareHouseID '
			END
	
		IF @vcLookBackTableName='AddressDetails'	
			BEGIN
				IF CHARINDEX('#tempOppAddress AD', @strFrom)=0
				BEGIN
					insert into #tempOpp
						select distinct Opp.numOppId,Opp.numDivisionID,isnull(Opp.tintBillToType,0) as tintBillToType,isnull(Opp.tintShipToType,0) as tintShipToType
						FROM OpportunityMaster Opp JOIN OpportunityBizDocs OBD ON Opp.numOppId = OBD.numOppId and OBD.bitAuthoritativeBizDocs = 1
							--LEFT join OpportunityLinking OL on OL.numChildOppID=Opp.numOppId left join WebAPI on WebAPI.WebApiId=isnull(OL.numWebApiId,0)
						where Opp.numDomainID=@numDomainID 

					insert into #tempOppAddress(numOppID) select numOppId from #tempOpp

					--Billing Address from Address Detils
					Update tempOppAdd set vcBillStreet=AD.vcStreet,vcBillCity=AD.vcCity,numBillState=AD.numState,
							vcBillPostCode=AD.vcPostalCode,numBillCountry=AD.numCountry from #tempOpp Opp join #tempOppAddress tempOppAdd on Opp.numOppId=tempOppAdd.numOppId
							join AddressDetails AD on AD.numRecordID=Opp.numDivisionID and Opp.tintBillToType in (0,1)
							where AD.tintAddressOf=2 AND AD.tintAddressType=1 AND AD.bitIsPrimary=1 and AD.numDomainID=@numDomainID

					--Billing Address from OpportunityAddress
					Update tempOppAdd set vcBillStreet=OppAdd.vcBillStreet,vcBillCity=OppAdd.vcBillCity,numBillState=OppAdd.numBillState,
							vcBillPostCode=OppAdd.vcBillPostCode,numBillCountry=OppAdd.numBillCountry from #tempOpp Opp join #tempOppAddress tempOppAdd on Opp.numOppId=tempOppAdd.numOppId
							join OpportunityAddress OppAdd on OppAdd.numOppID=Opp.numOppId and Opp.tintBillToType in (2,3)

					--Shipping Address from Address Detils
					Update tempOppAdd set vcShipStreet=AD.vcStreet,vcShipCity=AD.vcCity,numShipState=AD.numState,
							vcShipPostCode=AD.vcPostalCode,numShipCountry=AD.numCountry from #tempOpp Opp join #tempOppAddress tempOppAdd on Opp.numOppId=tempOppAdd.numOppId
							join AddressDetails AD on AD.numRecordID=Opp.numDivisionID and Opp.tintShipToType in (0,1)
							where AD.tintAddressOf=2 AND AD.tintAddressType=2 AND AD.bitIsPrimary=1 and AD.numDomainID=@numDomainID

					--Shipping Address from OpportunityAddress
					Update tempOppAdd set vcShipStreet=OppAdd.vcShipStreet,vcShipCity=OppAdd.vcShipCity,numShipState=OppAdd.numShipState,
							vcShipPostCode=OppAdd.vcShipPostCode,numShipCountry=OppAdd.numShipCountry from #tempOpp Opp join #tempOppAddress tempOppAdd on Opp.numOppId=tempOppAdd.numOppId
							join OpportunityAddress OppAdd on OppAdd.numOppID=Opp.numOppId and Opp.tintShipToType in (2,3)

					SET @strFrom=@strFrom + ' LEFT JOIN #tempOppAddress AD ON AD.numOppID = Opp.numOppID '
	
				END
			END


		 SET @vcColumnName= @vcDbColumnName+'~'+ convert(varchar(10),@numFieldId) +'~0'
            
          IF @vcAssociatedControlType = 'SelectBox'
            BEGIN
              IF @vcListItemType = 'LI'
                BEGIN
                  SET @strSql = @strSql + ',L' + CONVERT(VARCHAR(3),@tintOrder) + '.vcData[' + @vcColumnName + ']'
                  SET @WhereCondition = @WhereCondition + ' left Join ListDetails L' + CONVERT(VARCHAR(3),@tintOrder)
                                          + ' on L' + CONVERT(VARCHAR(3),@tintOrder) + '.numListItemID='+ @vcDbColumnName
                END
              ELSE IF @vcListItemType = 'S'
                  BEGIN
                    SET @strSql = @strSql + ',S' + CONVERT(VARCHAR(3),@tintOrder)+ '.vcState[' + @vcColumnName + ']'
                    SET @WhereCondition = @WhereCondition + ' left join State S' + CONVERT(VARCHAR(3),@tintOrder) 
                                            + ' on S' + CONVERT(VARCHAR(3),@tintOrder) + '.numStateID='+ @vcDbColumnName
                  END
              ELSE IF @vcListItemType = 'T'
                    BEGIN
                      SET @strSql = @strSql + ',L'+ CONVERT(VARCHAR(3),@tintOrder)+ '.vcData['+ @vcColumnName + ']'
                      SET @WhereCondition = @WhereCondition + ' left Join ListDetails L' + CONVERT(VARCHAR(3),@tintOrder)
                                              + ' on L'+ CONVERT(VARCHAR(3),@tintOrder)+ '.numListItemID='
                                              + @vcDbColumnName
                    END
			  ELSE IF @vcListItemType = 'UOM'
                  BEGIN
                    SET @strSql = @strSql + ',UOM' + CONVERT(VARCHAR(3),@tintOrder)+ '.vcUnitName[' + @vcColumnName + ']'
                    SET @WhereCondition = @WhereCondition + ' left join UOM UOM' + CONVERT(VARCHAR(3),@tintOrder) 
                                            + ' on UOM' + CONVERT(VARCHAR(3),@tintOrder) + '.numUOMId=OI.numUOMId'
                  END
               ELSE IF @vcListItemType = 'U'
                      BEGIN
                        SET @strSql = @strSql+ ',dbo.fn_GetContactName('+ @Prefix+ @vcDbColumnName+ ') ['+ @vcColumnName + ']'
                      END
				ELSE IF @vcListItemType = 'WI'
                BEGIN
                  SET @strSql = @strSql + ', Case when isnull(OL.numSiteID,0)>0 then Sites.vcSiteName else WebAPI.vcProviderName end [' + @vcColumnName + ']'
			
                END
            END
          ELSE IF @vcAssociatedControlType = 'DateField'
              BEGIN
					SET @strSql = @strSql
                                + ',case when convert(varchar(11),DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+','
                                + @Prefix
                                + @vcDbColumnName
                                + ') )= convert(varchar(11),getdate()) then ''<b><font color=red>Today</font></b>'''
					SET @strSql = @strSql
									+ 'when convert(varchar(11),DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+','
									+ @Prefix
									+ @vcDbColumnName
									+ ') )= convert(varchar(11),dateadd(day,-1,getdate())) then ''<b><font color=purple>YesterDay</font></b>'''
					SET @strSql = @strSql
									+ 'when convert(varchar(11),DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+','
									+ @Prefix
									+ @vcDbColumnName
									+ ') )= convert(varchar(11),dateadd(day,1,getdate())) then''<b><font color=orange>Tommorow</font></b>'' '
					SET @strSql = @strSql
									+ 'else dbo.FormatedDateFromDate('
									+ @Prefix
									+ @vcDbColumnName
									+ ','
									+ CONVERT(VARCHAR(10),@numDomainId)
									+ ') end  ['
									+ @vcColumnName + ']' 	
              END
            ELSE IF @vcAssociatedControlType = 'TextBox'
                BEGIN
				  IF @vcDbColumnName='DiscAmt'
					 SET @strSql = @strSql + ',(isnull(OBDI.monTotAmtBefDiscount,OBDI.monTotAmount)-OBDI.monTotAmount) ['+ @vcColumnName + ']'
				  ELSE	
					 SET @strSql = @strSql + ',' + @Prefix + @vcDbColumnName+ ' ['+ @vcColumnName + ']'
                END
            ELSE
                IF @vcAssociatedControlType = 'TextArea'
                  BEGIN
                    IF @vcDbColumnName = 'numShippingReportID'
					BEGIN
						SET @strSql = @strSql + ', (
													SELECT TOP 1 numShippingReportID FROM ShippingReport
													WHERE numOppBizDocID = OBD.numOppBizDocsID
													AND numDomainID = Opp.numDomainID
													ORDER BY numShippingReportID DESC
													) AS [' + @vcColumnName + ']'	 
					END
					ELSE
						set @strSql=@strSql+','+  @Prefix + @vcDbColumnName +' ['+ @vcColumnName+']'  
                  END
        END
      
     
       select top 1 @tintOrder=tintOrder+1,@vcDbColumnName=vcDbColumnName,@vcFieldName=vcFieldName,                
  @vcAssociatedControlType=vcAssociatedControlType,@vcListItemType=vcListItemType,@numListID=numListID,@vcLookBackTableName=vcLookBackTableName                                               
,@bitCustom=bitCustomField,@numFieldId=numFieldId,@bitAllowSorting=bitAllowSorting,@bitAllowEdit=bitAllowEdit,
@ListRelID=ListRelID
  from #tempForm WHERE tintOrder > @tintOrder-1 order by tintOrder asc            
 
  if @@rowcount=0 set @tintOrder=0 
 END 
  
  

CREATE TABLE #tempTable (
    RowNumber int,TotalRowCount int,
    numOppId        NUMERIC(9))

Declare @column as varchar(50)              
set @column = @columnName              

-- ADDED BY MANISH ANJARA - 15th Aug,2012
DECLARE @vcStatus AS VARCHAR(MAX)
DECLARE @strWHERE AS VARCHAR(MAX)
SELECT @vcStatus  = ISNULL(vcOrderStatus,'') FROM dbo.OpportunityOrderStatus WHERE numDomainID = @numDomainID
IF ISNULL(@vcStatus,'') <> ''
	SET @strWHERE = ' AND ISNULL(numStatus,0) IN ( ' + @vcStatus + ' ) '
ELSE
	SET @strWHERE = ' AND 1=1 '

DECLARE @numBizDocType AS VARCHAR(MAX)
SELECT @numBizDocType  = ISNULL(numBizDocType,0) FROM dbo.OpportunityOrderStatus WHERE numDomainID = @numDomainID
IF ISNULL(@numBizDocType,'') <> ''
	SET @strWHERE = ' AND ISNULL(numBizDocId,0) = ' + @numBizDocType 
ELSE
	SET @strWHERE = ' AND 1=1 '	
-----------------------------------------

declare @strSql1 as varchar(8000)                                                            
set @strSql1='insert into #tempTable
SELECT ROW_NUMBER() OVER (ORDER BY '+ CASE WHEN PATINDEX('%txt%',@column)>0 THEN ' CONVERT(VARCHAR(Max),' + @column + ')'  ELSE @column end +' '+ @columnSortOrder+') AS RowNumber,                                                             
	COUNT(*) OVER(PARTITION BY NULL) AS TotalRowCount,Opp.numOppId ' + @strFrom  + ' where Opp.numDomainID='+convert(varchar(15),@numDomainID) +' AND tintOppType=1 AND tintOppStatus=1 ' + @strWHERE                                               

IF @vcRegularSearchCriteria<>'' 
BEGIN
	set @strSql1=@strSql1+' and ' + @vcRegularSearchCriteria 
END

if @SortChar<>'0' set @strSql1=@strSql1 + ' And Opp.vcPoppName like '''+@SortChar+'%'''                    

print @strSql1   
exec (@strSql1) 




DELETE FROM #tempTable WHERE RowNumber NOT IN (SELECT Min(RowNumber) FROM #tempTable GROUP BY numOppId)

SET @TotRecs = (SELECT COUNT(* )
                  FROM   #tempTable)

DECLARE @firstRec AS INTEGER                                                            
DECLARE @lastRec AS INTEGER                                                            
SET @firstRec = ( @CurrentPage - 1 ) * @PageSize                                                            
SET @lastRec = ( @CurrentPage * @PageSize + 1 )   

	IF CHARINDEX('OBDI.numOppBizDocItemID', @strSql)=0 
		SET @strSql=@strSql +',0 as numOppBizDocItemID '
		
	IF CHARINDEX('OI.numoppitemtCode', @strSql)=0 
		SET @strSql = @strSql +',0 as numoppitemtCode '


SET @strSql = @strSql + @strFrom + @WhereCondition + ' join #tempTable temp on temp.numOppID=Opp.numOppID  where Opp.numDomainID='+convert(varchar(15),@numDomainID) +
'and RowNumber > '+convert(varchar(10),@firstRec)+ ' and RowNumber <'+ convert(varchar(10),@lastRec) + ' ' + @strWHERE

IF @vcRegularSearchCriteria<>'' 
BEGIN
	set @strSql=@strSql+' and ' + @vcRegularSearchCriteria 
END
 
SET @strSql = @strSql + ' order by RowNumber'

                                             
print @strSql                      
              
exec (@strSql)  

SELECT * FROM #tempForm
 

DROP TABLE #tempTable
DROP TABLE #tempForm

drop table #tempOpp
drop table #tempOppAddress