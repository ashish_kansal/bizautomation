GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_GetEmailMergeData' ) 
    DROP PROCEDURE USP_GetEmailMergeData 
GO
CREATE PROCEDURE USP_GetEmailMergeData
    @numModuleID NUMERIC,
    @vcRecordIDs VARCHAR(8000) = '', -- Seperated by comma
    @numDomainID NUMERIC,
    @tintMode TINYINT = 0,
    @ClientTimeZoneOffset Int,
	@numOppBizDocId NUMERIC = 0
AS 
BEGIN
	
    IF (@numModuleID = 1  or @numModuleID = 0)
        BEGIN
            SELECT  ACI.numContactId,
                    [dbo].[GetListIemName](ACI.vcDepartment) ContactDepartment,
                    [dbo].[GetListIemName](ACI.vcCategory) ContactCategory,
                    
                    ACI.vcGivenName,
                    ACI.vcFirstName ContactFirstName,
                    ACI.vcLastName ContactLastName,
                    ACI.numDivisionId,
                    dbo.GetListIemName(ACI.numContactType) ContactType,
                    [dbo].[GetListIemName](ACI.numTeam) ContactTeam,
                    ACI.numPhone ContactPhone,
                    ACI.numPhoneExtension ContactPhoneExt,
                    ACI.numCell ContactCell,
                    ACI.numHomePhone ContactHomePhone,
                    ACI.vcFax ContactFax,
                    ACI.vcEmail ContactEmail,
                    ACI.VcAsstFirstName AssistantFirstName,
                    ACI.vcAsstLastName AssistantLastName,
                    ACI.numAsstPhone ContactAssistantPhone,
                    ACI.numAsstExtn ContactAssistantPhoneExt,
                    ACI.vcAsstEmail ContactAssistantEmail,
                    CASE WHEN ACI.charSex = 'M' THEN 'Male'
                         WHEN ACI.charSex = 'F' THEN 'Female'
                         ELSE '-'
                    END AS ContactGender,
                    ACI.bintDOB,
                    dbo.GetAge(ACI.bintDOB, GETUTCDATE()) AS ContactAge,
                    [dbo].[GetListIemName](ACI.vcPosition) ContactPosition,
                    ACI.txtNotes,
                    ACI.numCreatedBy,
                    ACI.bintCreatedDate,
                    ACI.numModifiedBy,
                    ACI.bintModifiedDate,
                    ACI.numDomainID,
                    ACI.bitOptOut,
                    dbo.fn_GetContactName(ACI.numManagerId) ContactManager,
                    ACI.numRecOwner,
                    [dbo].[GetListIemName](ACI.numEmpStatus) ContactStatus,
                    ACI.vcTitle ContactTitle,
                    ACI.vcAltEmail,
                    ACI.vcItemId,
                    ACI.vcChangeKey,
                    ISNULL(( SELECT TOP 1 vcECampName
                             FROM   [ECampaign]
                             WHERE  numECampaignID = ACI.numECampaignID
                           ), '') ContactDripCampaign,
                    dbo.getContactAddress(ACI.numContactID) AS ContactPrimaryAddress,
                    C.numCompanyId,
                    C.vcCompanyName OrganizationName,
                    C.numCompanyType,
                    C.numCompanyRating,
                    C.numCompanyIndustry,
                    C.numCompanyCredit,
                    C.txtComments,
                    C.vcWebSite,
                    C.vcWebLabel1,
                    C.vcWebLink1,
                    C.vcWebLabel2,
                    C.vcWebLink2,
                    C.vcWebLabel3,
                    C.vcWebLink3,
                    C.vcWeblabel4,
                    C.vcWebLink4,
                    C.numAnnualRevID,
                    dbo.GetListIemName(C.numNoOfEmployeesId) OrgNoOfEmployee,
                    C.vcHow,
                    C.numCreatedBy,
                    C.bintCreatedDate,
                    C.numModifiedBy,
                    C.bintModifiedDate,
                    C.bitPublicFlag,
                    C.numDomainID,
                    
                    DM.numDivisionID,
                    DM.numCompanyID,
                    DM.vcDivisionName,
                    DM.numGrpId,
                    DM.numFollowUpStatus,
                    DM.bitPublicFlag,
                    DM.numCreatedBy,
                    DM.bintCreatedDate,
                    DM.numModifiedBy,
                    DM.bintModifiedDate,
                    DM.tintCRMType,
                    DM.numDomainID,
                    DM.bitLeadBoxFlg,
                    DM.numTerID,
                    DM.numStatusID,
                    DM.bintLeadProm,
                    DM.bintLeadPromBy,
                    DM.bintProsProm,
                    DM.bintProsPromBy,
                    DM.numRecOwner,
                    DM.decTaxPercentage,
                    DM.tintBillingTerms,
                    DM.numBillingDays,
                    DM.tintInterestType,
                    DM.fltInterest,
                    DM.vcComPhone,
                    DM.vcComFax,
                    DM.numCampaignID,
                    DM.numAssignedBy,
                    DM.numAssignedTo,
                    DM.bitNoTax,
                    --vcCompanyName + ' ,<br>' + ISNULL(AD2.vcStreet,'') + ' <br>'
                    --+ ISNULL(AD2.VcCity,'') + ' ,' + ISNULL(dbo.fn_GetState(AD2.numState),
                    --                             '') + ' ' + ISNULL(AD2.vcPostalCode,'')
                    --+ ' <br>' + ISNULL(dbo.fn_GetListItemName(AD2.numCountry),
                    --                   '') AS OrgShippingAddress ,	
									   
					(SELECT TOP 1 vcCreditCardNo FROM CustomerCreditCardInfo CC
						LEFT JOIN ListDetails ON ListDetails.numListItemID=CC.numCardTypeID
					WHERE numContactId=ACI.numContactId AND bitIsDefault = 1 
					ORDER BY CC.bitIsDefault DESC) AS PrimaryCreditCardNo,												   		
                  
				   (select TOP 1 U.txtSignature from UserMaster U where U.numUserDetailId=ACI.numRecOwner) as [Signature],
				   (CASE WHEN @numDomainID=1 THEN ISNULL((SELECT TOP 1 LD.vcData FROM CFW_FLD_Values CFV INNER JOIN ListDetails LD ON CFV.Fld_Value=LD.numListItemID WHERE CFV.RecId=DM.numDivisionID AND Fld_ID=12882),'') ELSE '' END) Cust12882
            FROM    dbo.CompanyInfo C
                    INNER JOIN dbo.DivisionMaster DM ON DM.numCompanyId = C.numCompanyId
                    INNER JOIN dbo.AdditionalContactsInformation ACI ON ACI.numDivisionId = DM.numDivisionID
                    LEFT JOIN dbo.AddressDetails AD2 ON AD2.numDomainID=DM.numDomainID 
					 AND AD2.numRecordID= DM.numDivisionID AND AD2.tintAddressOf=2 AND AD2.tintAddressType=2 AND AD2.bitIsPrimary=1
					 ---- Added by Priya(9 Feb 2018)----
					LEFT JOIN dbo.AddressDetails AD1 ON AD1.numDomainID=DM.numDomainID 
					AND AD1.numRecordID= DM.numDivisionID AND AD1.tintAddressOf=2 AND AD1.tintAddressType=1 AND AD1.bitIsPrimary=1
					LEFT JOIN dbo.BillingTerms BT ON  DM.numBillingDays = BT.numTermsID
								AND ISNULL(BT.bitActive,0) = 1
				
            WHERE   DM.numDomainId = @numDomainID
                    AND numContactId IN (
                    SELECT  Id
                    FROM    dbo.SplitIDs(@vcRecordIDs, ',') )
        END
    IF @numModuleID = 2 
        BEGIN
             SELECT 
                    dbo.fn_getContactName(OM.numAssignedTo) OpportunityAssigneeName,
					(ISNULL(C.varCurrSymbol,'') + ' ' + (convert(varchar(30), ([dbo].[getdealamount](OM.numOppId,Getutcdate(),0)), 1) )) AS OppOrderSubTotal,
					vcPOppName OpportunityName,
					dbo.Fn_getcontactname(OM.numContactId) OppOrderContact,					
					ISNULL(OM.vcOppRefOrderNo,'') AS OppOrderCustomerPO,
					(ISNULL(C.varCurrSymbol,'') + ' ' + (convert(varchar(30),ISNULL((SELECT SUM(ISNULL(monDealAmount,0)) FROM OpportunityBizDocs BD WHERE  BD.numOppId = OM.numOppId AND ISNULL(bitAuthoritativeBizDocs,0)=1),0), 1) )) AS OppOrderInvoiceGrandtotal,
					(ISNULL(C.varCurrSymbol,'') + ' ' + (convert(varchar(30), ISNULL((SELECT SUM(ISNULL(monAmountPaid,0)) FROM OpportunityBizDocs BD WHERE  BD.numOppId = OM.numOppId AND ISNULL(bitAuthoritativeBizDocs,0)=1),0), 1) )) AS OppOrderTotalAmountPaid,
					ISNULL((SELECT TOP 1 vcAddressName FROM fn_getOPPAddressDetails(OM.numOppId,OM.numDomainID,1)),0) AS OppOrderBillingAddressName,
					ISNULL((SELECT TOP 1 vcStreet FROM fn_getOPPAddressDetails(OM.numOppId,OM.numDomainID,1)),0) AS OppOrderBillingStreet,
					ISNULL((SELECT TOP 1 vcCity FROM fn_getOPPAddressDetails(OM.numOppId,OM.numDomainID,1)),0) AS OppOrderBillingCity,
					ISNULL((SELECT TOP 1 vcState FROM fn_getOPPAddressDetails(OM.numOppId,OM.numDomainID,1)),0) AS OppOrderBillingState,
					ISNULL((SELECT TOP 1 vcPostalCode FROM fn_getOPPAddressDetails(OM.numOppId,OM.numDomainID,1)),0) AS OppOrderBillingPostal,
					ISNULL((SELECT TOP 1 vcCountry FROM fn_getOPPAddressDetails(OM.numOppId,OM.numDomainID,1)),0) AS OppOrderBillingCountry,
					ISNULL((SELECT TOP 1 vcAddressName FROM fn_getOPPAddressDetails(OM.numOppId,OM.numDomainID,2)),'') AS OppOrderShippingAddressName,
					ISNULL((SELECT TOP 1 vcStreet FROM fn_getOPPAddressDetails(OM.numOppId,OM.numDomainID,2)),'') AS OppOrderShippingStreet,
					ISNULL((SELECT TOP 1 vcCity FROM fn_getOPPAddressDetails(OM.numOppId,OM.numDomainID,2)),0) AS OppOrderShippingCity,
					ISNULL((SELECT TOP 1 vcState FROM fn_getOPPAddressDetails(OM.numOppId,OM.numDomainID,2)),0) AS OppOrderShippingState,
					ISNULL((SELECT TOP 1 vcPostalCode FROM fn_getOPPAddressDetails(OM.numOppId,OM.numDomainID,2)),0) AS OppOrderShippingPostal,
					ISNULL((SELECT TOP 1 vcCountry FROM fn_getOPPAddressDetails(OM.numOppId,OM.numDomainID,2)),0) AS OppOrderShippingCountry,
					(CASE WHEN ISNULL(OM.intUsedShippingCompany,0) = 0 THEN '' 
					ELSE ISNULL((SELECT vcData FROM ListDetails WHERE numListID = 82 AND numListItemID=ISNULL(OM.intUsedShippingCompany,0)),'') END) AS OppOrderShipVia,
					ISNULL((SELECT vcShipmentService FROM ShippingService WHERE (numDomainID=@numDomainID OR ISNULL(numDomainID,0)=0) AND numShippingServiceID = OM.numShippingService),'') AS OppOrderShippingService,
					ISNULL(OM.txtComments, '') AS OppOrderComments,
					ISNULL(OM.dtReleaseDate,'') AS OppOrderReleaseDate,
					ISNULL(OM.intpEstimatedCloseDate,'') AS OppOrderEstimatedCloseDate,
					dbo.fn_GetOpportunitySourceValue(ISNULL(OM.tintSource,0),ISNULL(OM.tintSourceType,0),OM.numDomainID) AS OppOrderSource,
					--ISNULL((SELECT SUBSTRING((SELECT '$^$' + ISNULL( (SELECT vcShipFieldValue FROM dbo.ShippingFieldValues WHERE numDomainID=OM.numDomainID AND intShipFieldID IN ( SELECT intShipFieldID FROM dbo.ShippingFields WHERE numListItemID=OBD.numShipVia  AND vcFieldName='Tracking URL')),'') +'#^#'+ RTRIM(LTRIM(vcTrackingNo) )  FROM dbo.OpportunityBizDocs OBD WHERE OBD.numOppId=OM.numOppID FOR XML PATH('')),4,200000)),'') AS OppOrderTrackingNo,				

					 (SELECT  
						SUBSTRING(
									(SELECT '$^$' + 
									(SELECT vcShipFieldValue FROM dbo.ShippingFieldValues 
										WHERE numDomainID=OM.numDomainID AND intShipFieldID IN ( SELECT intShipFieldID FROM dbo.ShippingFields WHERE numListItemID=OBD.numShipVia  AND vcFieldName='Tracking URL')
									) +'#^#'+ RTRIM(LTRIM(vcTrackingNo))						
									FROM dbo.OpportunityBizDocs OBD WHERE OBD.numOppId= OM.numOppID and ISNULL(OBD.numShipVia,0) <> 0
						FOR XML PATH('')),4,200000)
						
					 ) AS  OppOrderTrackingNo,					
					
					(SELECT U.txtSignature FROM UserMaster U where U.numUserDetailId=OM.numContactId) as [Signature],

					--(SELECT TOP 1 vcCreditCardNo FROM CustomerCreditCardInfo CC
					--	INNER JOIN [AdditionalContactsInformation] ADC ON CC.[numContactId] = ADC.[numContactId]
					--	LEFT JOIN ListDetails ON ListDetails.numListItemID=CC.numCardTypeID
					--	WHERE CC.numContactId=OM.numContactId AND bitIsDefault = 1 
					--	ORDER BY CC.bitIsDefault DESC
					--) AS PrimaryCreditCardNo

					(CASE WHEN @numOppBizDocId > 0 AND EXISTS (SELECT numTransHistoryID FROM TransactionHistory TH WHERE TH.numOppBizDocsID = @numOppBizDocId AND TH.numOppID = OM.numOppId) THEN (SELECT vcCreditCardNo FROM TransactionHistory TH WHERE TH.numOppBizDocsID = @numOppBizDocId AND TH.numOppID = OM.numOppId)
							ELSE ISNULL((SELECT TOP 1 vcCreditCardNo FROM TransactionHistory TH WHERE TH.numOppID = OM.numOppId),'')
						END
					) AS PrimaryCreditCardNo,
					
					ISNULL(CMP.vcCompanyName , '') AS OppOrderAccountName

            FROM    dbo.OpportunityMaster OM
					LEFT OUTER JOIN [Currency] C
					ON C.numCurrencyID = OM.numCurrencyID
					LEFT JOIN DivisionMaster DM
					ON  OM.numDivisionId = DM.numDivisionID
					LEFT JOIN CompanyInfo CMP
					ON DM.numCompanyID = CMP.numCompanyId					
            WHERE   OM.numDomainID = @numDomainID
                   AND OM.numOppId IN (
                    SELECT  Id
                    FROM    dbo.SplitIDs(@vcRecordIDs, ',') )

        END
    IF @numModuleID = 4 
        BEGIN        
            SELECT  vcProjectID ProjectID,
                    vcProjectName ProjectName,
                    dbo.fn_getContactName(numAssignedTo) ProjectAssigneeName,
                    dbo.FormatedDateFromDate(intDueDate, numDomainId) ProjectDueDate
            FROM    dbo.ProjectsMaster
            WHERE   numDomainID = @numDomainID
                    AND numProID IN (
                    SELECT  Id
                    FROM    dbo.SplitIDs(@vcRecordIDs, ',') )
        END 
    IF @numModuleID = 5 
        BEGIN        
            SELECT  vcCaseNumber CaseNo,
                    textSubject AS CaseSubject,
                    dbo.fn_getContactName(numAssignedTo) CaseAssigneeName,
                    dbo.FormatedDateFromDate(intTargetResolveDate, numDomainID) CaseResoveDate
            FROM    dbo.Cases
            WHERE   numDomainID = @numDomainID
                    AND numCaseId IN (
                    SELECT  Id
                    FROM    dbo.SplitIDs(@vcRecordIDs, ',') )
        END 
    IF @numModuleID = 6 
        BEGIN        
            SELECT  textDetails TicklerComment,
                    dbo.GetListIemName(bitTask) TicklerType,
                    dbo.fn_getContactName(numAssign) TicklerAssigneeName,
                    dbo.FormatedDateFromDate(dtStartTime, C.numDomainID) TicklerDueDate,
                    dbo.GetListIemName(numStatus) TicklerPriority,
                    dbo.FormatedDateTimeFromDate(DateAdd(minute, -@ClientTimeZoneOffset,dtStartTime ), C.numDomainID) TicklerStartTime,
                    dbo.FormatedDateTimeFromDate(DateAdd(minute, -@ClientTimeZoneOffset,dtEndTime ), C.numDomainID) TicklerEndTime,
                    dbo.GetListIemName(bitTask) + ' - ' + dbo.GetListIemName(numStatus) AS TicklerTitle,
                    dbo.fn_getContactName(C.numCreatedBy) TicklerCreatedBy,
					CMP.vcCompanyName OrganizationName
                   
                    
            FROM    dbo.Communication C
			LEFT JOIN DivisionMaster DM
			ON C.numDivisionId = DM.numDivisionID
			LEFT JOIN CompanyInfo CMP
			ON DM.numCompanyId = CMP.numCompanyId
            WHERE   C.numDomainID = @numDomainID
                    AND numCommId IN (
                    SELECT  Id
                    FROM    dbo.SplitIDs(@vcRecordIDs, ',') )
        END 
    IF @numModuleID = 8 
        BEGIN
            SELECT  ACI.vcFirstName ContactFirstName,
                    ACI.vcLastName ContactLastName,
					C.vcCompanyName OrganizationName,
					OB.vcBizDocID BizDocID,
					dbo.GetListIemName(OB.numBizDocId) BizDoc,
                   OB.monDealAmount BizDocAmount,
                    OB.monAmountPaid BizDocAmountPaid,
					OB.monAmountPaid AS AmountPaid,
                    ( OB.monDealAmount - OB.monAmountPaid ) BizDocBalanceDue,
                    dbo.GetListIemName(ISNULL(OB.numBizDocStatus, 0)) AS BizDocStatus,
                    C.vcCompanyName OrderOrganizationName,
                    ACI.vcFirstName OrderContactFirstName,
                    ACI.vcLastName OrderContactLastName,
                    ISNULL((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(om.intBillingDays,0)), 0)  AS BizDocBillingTermsNetDays,
                    dbo.FormatedDateFromDate(OB.dtFromDate, OM.numDomainID) AS BizDocBillingTermsFromDate,
                    CASE ISNULL(OM.bitInterestType, 0)
                      WHEN 1 THEN '+   OB.vcBizDocID BizDocID,
                  '
                      ELSE '-'
                    END + CAST(ISNULL(OM.fltInterest, 0) AS VARCHAR(10))
                    + ' %' AS BizDocBillingTermsInterest,
                    OM.vcPOppName OrderName,
                    CASE WHEN   isnull(OM.bitBillingTerms ,0) <> 0		
                          THEN 'Net ' + 
                                CASE WHEN 	                                   
                                      ISNUMERIC(ISNULL((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(intBillingDays,0)), 0))=1 	
                                THEN 	
                                     CONVERT(VARCHAR(20) ,ISNULL((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(intBillingDays,0)), 0)) 
                                     ELSE
                                     CONVERT(VARCHAR(20) ,0) 
                                     END + ',' +
                                     CASE WHEN  tintInterestType = 0 THEN '-' ELSE '+' + CONVERT(VARCHAR(20) , OM.fltInterest)   + ' %' END ELSE '-' 
                           END         
                                     AS BizDocBillingTerms,

						  DATEADD(DAY,+ CASE WHEN ISNUMERIC(ISNULL((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(intBillingDays,0)), 0))=1 
											 THEN ISNULL((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(intBillingDays,0)), 0)
											 ELSE 0 
										END,OB.dtCreatedDate) AS BizDocDueDate,										
					ACI.vcFirstName ContactFirstName,
                    ACI.vcLastName ContactLastName,
					ACI.vcEmail ContactEmail,OB.numOppBizDocsID,
					isnull(OB.vcTrackingNo,'') AS  vcTrackingNo,
					ISNULL( (SELECT vcShipFieldValue FROM dbo.ShippingFieldValues WHERE numDomainID=OM.numDomainID AND intShipFieldID IN ( SELECT intShipFieldID FROM dbo.ShippingFields WHERE numListItemID=OB.numShipVia  AND vcFieldName='Tracking URL')),'') AS vcTrackingURL,
					'' As BizDocTemplateFromGlobalSettings

					--OM.vcPOppName OpportunityName,
     --               dbo.fn_getContactName(OM.numAssignedTo) OpportunityAssigneeName
            FROM    dbo.OpportunityBizDocs OB
                    INNER JOIN dbo.OpportunityMaster OM ON OM.numOppID = OB.numOppID
                    INNER JOIN dbo.DivisionMaster DM ON DM.numDivisionID = OM.numDivisionID
                    INNER JOIN dbo.AdditionalContactsInformation ACI ON ACI.numContactID = OM.numContactID
                    INNER JOIN dbo.CompanyInfo C ON C.numCompanyID = DM.numCompanyID
            WHERE   OM.numDomainID = @numDomainID
                    AND OB.numOppBizDocsID IN (
                    SELECT  Id
                    FROM    dbo.SplitIDs(@vcRecordIDs, ',') )
			
        END 	
        IF @numModuleID = 9 
        BEGIN
            SELECT  
                   ISNULL(TempBizDoc.vcBizDocID,'') BizDocID,
                   ISNULL(TempBizDoc.monDealAmount,0) BizDocAmount,
                   ISNULL(TempBizDoc.monAmountPaid,0) BizDocAmountPaid,
                   ISNULL(TempBizDoc.BizDocBalanceDue,0) BizDocBalanceDue,
				   ISNULL(TempBizDoc.BizDocStatus,'') BizDocStatus,
                    C.vcCompanyName OrderOrganizationName,
                    ACI.vcFirstName OrderContactFirstName,
                    ACI.vcLastName OrderContactLastName,
                    --ISNULL(dbo.fn_GetListItemName(ISNULL(om.intBillingDays, 0)),0) AS BizDocBillingTermsNetDays,
                    ISNULL((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(om.intBillingDays,0)), 0) AS BizDocBillingTermsNetDays,
                   -- dbo.FormatedDateFromDate(OB.dtFromDate, OM.numDomainID) AS BizDocBillingTermsFromDate,
                    CASE ISNULL(OM.bitInterestType, 0)
                      WHEN 1 THEN '+'
                      ELSE '-'
                    END + CAST(ISNULL(OM.fltInterest, 0) AS VARCHAR(10))
                    + ' %' AS BizDocBillingTermsInterest,
                    OM.vcPOppName OrderName,
                    CASE WHEN   isnull(OM.bitBillingTerms ,0) <> 0		
                          THEN 'Net ' + 
                                CASE WHEN 		
                                      --ISNUMERIC(ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))=1
                                      ISNUMERIC(ISNULL((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(intBillingDays,0)), 0))=1  		
                                THEN 	
                                     --CONVERT(VARCHAR(20) ,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0)) 
                                     CONVERT(VARCHAR(20) ,ISNULL((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(intBillingDays,0)), 0)) 
                                     ELSE
                                     CONVERT(VARCHAR(20) ,0) 
                                     END + ',' +
                                     CASE WHEN  tintInterestType = 0 THEN '-' ELSE '+' + CONVERT(VARCHAR(20) , OM.fltInterest)   + ' %' END ELSE '-' 
                           END         
                                     AS BizDocBillingTerms       		
                                    ,
                        --  DATEADD(DAY,+ CASE WHEN ISNUMERIC(ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))=1 THEN ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0) ELSE 0 END,OB.dtCreatedDate) AS BizDocDueDate,
					ACI.vcFirstName ContactFirstName,
                    ACI.vcLastName ContactLastName,
					ACI.vcEmail ContactEmail
            FROM    dbo.OpportunityMaster OM 
                    INNER JOIN dbo.DivisionMaster DM ON DM.numDivisionID = OM.numDivisionID
                    INNER JOIN dbo.AdditionalContactsInformation ACI ON ACI.numContactID = OM.numContactID
                    INNER JOIN dbo.CompanyInfo C ON C.numCompanyID = DM.numCompanyID
					OUTER APPLY
					(
						SELECT TOP 1
							vcBizDocID,
							monDealAmount,
							monAmountPaid,
							(monDealAmount - monAmountPaid) AS BizDocBalanceDue,
							dbo.GetListIemName(ISNULL(numBizDocStatus, 0)) AS BizDocStatus
						FROM
							OpportunityBizDocs 
						WHERE 
							numOppId=OM.numOppID ANd ISNULL(bitAuthoritativeBizDocs,0) = 1
					) AS TempBizDoc
            WHERE   OM.numDomainID = @numDomainID
                    AND OM.numOppID IN (
                    SELECT  Id
                    FROM    dbo.SplitIDs(@vcRecordIDs, ',') )
			
        END 
        
        IF @numModuleID = 11 
        BEGIN
            SELECT  ACI.numContactId,
                    [dbo].[GetListIemName](ACI.vcDepartment) ContactDepartment,
                    [dbo].[GetListIemName](ACI.vcCategory) ContactCategory,
                    
                    ACI.vcGivenName,
                    ACI.vcFirstName ContactFirstName,
                    ACI.vcLastName ContactLastName,
                    ACI.numDivisionId,
                    dbo.GetListIemName(ACI.numContactType) ContactType,
                    [dbo].[GetListIemName](ACI.numTeam) ContactTeam,
                    ACI.numPhone ContactPhone,
                    ACI.numPhoneExtension ContactPhoneExt,
                    ACI.numCell ContactCell,
                    ACI.numHomePhone ContactHomePhone,
                    ACI.vcFax ContactFax,
                    ACI.vcEmail ContactEmail,
                    ACI.VcAsstFirstName AssistantFirstName,
                    ACI.vcAsstLastName AssistantLastName,
                    ACI.numAsstPhone ContactAssistantPhone,
                    ACI.numAsstExtn ContactAssistantPhoneExt,
                    ACI.vcAsstEmail ContactAssistantEmail,
                    CASE WHEN ACI.charSex = 'M' THEN 'Male'
                         WHEN ACI.charSex = 'F' THEN 'Female'
                         ELSE '-'
                    END AS ContactGender,
                    ACI.bintDOB,
                    dbo.GetAge(ACI.bintDOB, GETUTCDATE()) AS ContactAge,
                    [dbo].[GetListIemName](ACI.vcPosition) ContactPosition,
                    ACI.txtNotes,
                    ACI.numCreatedBy,
                    ACI.bintCreatedDate,
                    ACI.numModifiedBy,
                    ACI.bintModifiedDate,
                    ACI.numDomainID,
                    ACI.bitOptOut,
                    dbo.fn_GetContactName(ACI.numManagerId) ContactManager,
                    ACI.numRecOwner,
                    [dbo].[GetListIemName](ACI.numEmpStatus) ContactStatus,
                    ACI.vcTitle ContactTitle,
                    ACI.vcAltEmail,
                    ACI.vcItemId,
                    ACI.vcChangeKey,
                    ISNULL(( SELECT vcECampName
                             FROM   [ECampaign]
                             WHERE  numECampaignID = ACI.numECampaignID
                           ), '') ContactDripCampaign,
                    dbo.getContactAddress(ACI.numContactID) AS ContactPrimaryAddress,
                    C.numCompanyId,
                    C.vcCompanyName OrganizationName,
                    C.numCompanyType,
                    C.numCompanyRating,
                    C.numCompanyIndustry,
                    C.numCompanyCredit,
                    C.txtComments,
                    C.vcWebSite,
                    C.vcWebLabel1,
                    C.vcWebLink1,
                    C.vcWebLabel2,
                    C.vcWebLink2,
                    C.vcWebLabel3,
                    C.vcWebLink3,
                    C.vcWeblabel4,
                    C.vcWebLink4,
                    C.numAnnualRevID,
                    dbo.GetListIemName(C.numNoOfEmployeesId) OrgNoOfEmployee,
                    C.vcHow,
                    C.vcProfile,
                    C.numCreatedBy,
                    C.bintCreatedDate,
                    C.numModifiedBy,
                    C.bintModifiedDate,
                    C.bitPublicFlag,
                    C.numDomainID,
                    
                    DM.numDivisionID,
                    DM.numCompanyID,
                    DM.vcDivisionName,
                    DM.numGrpId,
                    DM.numFollowUpStatus,
                    DM.bitPublicFlag,
                    DM.numCreatedBy,
                    DM.bintCreatedDate,
                    DM.numModifiedBy,
                    DM.bintModifiedDate,
                    DM.tintCRMType,
                    DM.numDomainID,
                    DM.bitLeadBoxFlg,
                    DM.numTerID,
                    DM.numStatusID,
                    DM.bintLeadProm,
                    DM.bintLeadPromBy,
                    DM.bintProsProm,
                    DM.bintProsPromBy,
                    DM.numRecOwner,
                    DM.decTaxPercentage,
                    DM.tintBillingTerms,
                    DM.numBillingDays,
                    DM.tintInterestType,
                    DM.fltInterest,
                    DM.vcComPhone,
                    DM.vcComFax,
                    DM.numCampaignID,
                    DM.numAssignedBy,
                    DM.numAssignedTo,
                    DM.bitNoTax,
                    vcCompanyName + ' ,<br>' + ISNULL(AD2.vcStreet,'') + ' <br>'
                    + ISNULL(AD2.VcCity,'') + ' ,' + ISNULL(dbo.fn_GetState(AD2.numState),
                                                 '') + ' ' + ISNULL(AD2.vcPostalCode,'')
                    + ' <br>' + ISNULL(dbo.fn_GetListItemName(AD2.numCountry),
                                       '') AS OrgShippingAddress 
            FROM    dbo.CompanyInfo C
                    INNER JOIN dbo.DivisionMaster DM ON DM.numCompanyId = C.numCompanyId
                    INNER JOIN dbo.AdditionalContactsInformation ACI ON ACI.numDivisionId = DM.numDivisionID
                    LEFT JOIN dbo.AddressDetails AD2 ON AD2.numDomainID=DM.numDomainID 
					 AND AD2.numRecordID= DM.numDivisionID AND AD2.tintAddressOf=1 AND AD2.tintAddressType=2 AND AD2.bitIsPrimary=1
            WHERE   DM.numDomainId = @numDomainID
                    AND numContactId IN (
                    SELECT  Id
                    FROM    dbo.SplitIDs(@vcRecordIDs, ',') )
        END

		 IF @numModuleID = 45
        BEGIN        
            SELECT  C.vcCompanyName OrganizationName,
					(select vcdata from listdetails where numListItemID = C.vcProfile) AS OrgProfile, 		   
					(select A.vcFirstName+' '+A.vcLastName                       
					 from AdditionalContactsInformation A                  
					 join DivisionMaster D                        
					 on D.numDivisionID=A.numDivisionID
					 where A.numContactID=DM.numAssignedTo) AS OrgAssignto,	
					 ISNULL(AD1.vcAddressName,'') AS OrgBillingName,
					 ISNULL(AD1.vcStreet,'') AS OrgBillingStreet,
					 ISNULL(AD1.VcCity,'') AS OrgBillingCity,
					 ISNULL(dbo.fn_GetState(AD1.numState),'') AS OrgBillingState,
					 ISNULL(AD1.vcPostalCode,'') AS OrgBillingPostal,
					 ISNULL(dbo.fn_GetListItemName(AD1.numCountry),'') AS OrgBillingCountry,
					 ISNULL(AD2.vcAddressName,'') AS OrgShippingName,
					 ISNULL(AD2.vcStreet,'') AS OrgShippingStreet,
					 ISNULL(AD2.VcCity,'') AS OrgShippingCity,
					 ISNULL(dbo.fn_GetState(AD2.numState),'') AS OrgShippingState,
					 ISNULL(AD2.vcPostalCode,'') AS OrgShippingPostal,
					 ISNULL(dbo.fn_GetListItemName(AD2.numCountry),'') AS OrgShippingCountry,
					 (select vcdata from listdetails where numListItemID = C.numCompanyCredit) AS  OrgCreditLimit,
					 (select (CAST(BT.vcTerms AS VARCHAR(100) ) + ' (' + CAST(BT.numNetDueInDays AS VARCHAR(10)) + ',' + CAST(BT.numDiscount AS VARCHAR(10)) + ',' +  CAST(BT.numDiscountPaidInDays AS VARCHAR(10)) + ')' )) AS OrgNetTerms,
					 (select vcdata from listdetails where numListItemID = DM.numDefaultPaymentMethod) AS  OrgPaymentMethod,
					 (select vcdata from listdetails where numListItemID = DM.intShippingCompany) AS  OrgPreferredShipVia,
					 ISNULL((SELECT vcShipmentService FROM ShippingService WHERE (numDomainID=@numDomainID OR ISNULL(numDomainID,0)=0) AND numShippingServiceID = DM.numDefaultShippingServiceID),'') AS OrgPreferredParcelShippingService,
					(SELECT TOP 1 vcCreditCardNo FROM CustomerCreditCardInfo CC
						LEFT JOIN ListDetails ON ListDetails.numListItemID=CC.numCardTypeID
						WHERE numContactId=ACI.numContactId AND bitIsDefault = 1
						ORDER BY CC.bitIsDefault DESC
					) AS PrimaryCreditCardNo,	
			
					ACI.vcFirstName ContactFirstName,
                    ACI.vcLastName ContactLastName,
					ACI.vcEmail ContactEmail,
					ACI.numPhone ContactPhone,
					ACI.numPhoneExtension ContactPhoneExt,
					ACI.numCell ContactCellPhone,					
					 ISNULL((SELECT vcPassword FROM ExtranetAccountsDtl WHERE numContactId=ACI.numContactId),'') AS ContactEcommercepassword,					
					(select U.txtSignature from UserMaster U where U.numUserDetailId=ACI.numRecOwner) AS [Signature],
					dbo.GetListIemName(C.numNoOfEmployeesId) OrgNoOfEmployee,
					(CASE WHEN @numDomainID=1 THEN ISNULL((SELECT TOP 1 LD.vcData FROM CFW_FLD_Values CFV INNER JOIN ListDetails LD ON CFV.Fld_Value=LD.numListItemID WHERE CFV.RecId=DM.numDivisionID AND Fld_ID=12882),'') ELSE '' END) Cust12882
            FROM    dbo.CompanyInfo C
                    INNER JOIN dbo.DivisionMaster DM ON DM.numCompanyId = C.numCompanyId
                    INNER JOIN dbo.AdditionalContactsInformation ACI ON ACI.numDivisionId = DM.numDivisionID
					LEFT JOIN dbo.AddressDetails AD2 ON AD2.numDomainID=DM.numDomainID 
					AND AD2.numRecordID= DM.numDivisionID AND AD2.tintAddressOf=2 AND AD2.tintAddressType=2 AND AD2.bitIsPrimary=1
					 ---- Added by Priya(9 Feb 2018)----
					LEFT JOIN dbo.AddressDetails AD1 ON AD1.numDomainID=DM.numDomainID 
					AND AD1.numRecordID= DM.numDivisionID AND AD1.tintAddressOf=2 AND AD1.tintAddressType=1 AND AD1.bitIsPrimary=1
					LEFT JOIN dbo.BillingTerms BT ON  DM.numBillingDays = BT.numTermsID
								AND ISNULL(BT.bitActive,0) = 1
            WHERE   DM.numDomainId = @numDomainID
                    AND numContactId IN (
                    SELECT  Id
                    FROM    dbo.SplitIDs(@vcRecordIDs, ',') )
        END 
        	
--select * from dbo.OpportunityBizDocs	
--    SELECT TOP 1 bitInterestType,
--            *
--    FROM    dbo.OpportunityBizDocs ORDER BY numoppbizdocsid DESC 
END
--exec USP_GetEmailMergeData @numModuleID=6,@vcRecordIDs='18463',@numDomainID=1,@tintMode=0,@ClientTimeZoneOffset=0
--exec USP_GetEmailMergeData @numModuleID=1,@vcRecordIDs='1',@numDomainID=1,@tintMode=0,@ClientTimeZoneOffset=0