GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getshippingrulewarehouseslist')
DROP PROCEDURE usp_getshippingrulewarehouseslist
GO
CREATE PROCEDURE USP_GetShippingRuleWarehousesList			
	@numDomainID NUMERIC(9)
AS 
BEGIN
	
	SELECT vcWareHouse, numWareHouseID FROM Warehouses W
	WHERE numDomainID = @numDomainID

END