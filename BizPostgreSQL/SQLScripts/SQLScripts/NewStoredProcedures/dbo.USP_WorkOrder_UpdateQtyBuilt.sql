GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_WorkOrder_UpdateQtyBuilt')
DROP PROCEDURE USP_WorkOrder_UpdateQtyBuilt
GO
CREATE PROCEDURE [dbo].[USP_WorkOrder_UpdateQtyBuilt]  
	@numDomainID NUMERIC(18,0),
	@numUserCntID NUMERIC(18,0),
	@numWOID NUMERIC(18,0)
AS
BEGIN
	-- CODE LEVEL TRANSACTION IS USED

	DECLARE @numQtyBuilt FLOAT
	DECLARE @numQtyItemsReq FLOAT 
	DECLARE @numProcessedQty FLOAT = 0
	DECLARE @numWareHouseItemID NUMERIC(18,0)
	
	SELECT 
		@numQtyItemsReq = ISNULL(numQtyItemsReq,0)
		,@numQtyBuilt = ISNULL(numQtyBuilt,0)
		,@numWareHouseItemID=numWareHouseItemId
	FROM 
		WorkOrder 
	WHERE 
		WorkOrder.numDomainID = @numDomainID 
		AND WorkOrder.numWOId = @numWOID

	SET @numProcessedQty = ISNULL((SELECT
										MIN(numQtyBuilt) numQtyBuilt
									FROM
									(SELECT	
										numTaskId
										,(CASE 
											WHEN EXISTS (SELECT ID FROM StagePercentageDetailsTaskTimeLog SPDTTL WHERE SPDTTL.numTaskID = SPDT.numTaskId AND tintAction = 4) 
											THEN ISNULL(WorkOrder.numQtyItemsReq,0)
											ELSE ISNULL((SELECT SUM(numProcessedQty) FROM StagePercentageDetailsTaskTimeLog SPDTTL WHERE SPDTTL.numTaskID = SPDT.numTaskId),0)
										END) numQtyBuilt
									FROM
										StagePercentageDetailsTask SPDT
									INNER JOIN
										WorkOrder
									ON
										SPDT.numWorkOrderId = WorkOrder.numWOId
									WHERE
										numWorkOrderId=@numWOID) TEMP
									),0)

	SELECT
		Item.numItemCode
		,ISNULL(Item.numAssetChartAcntId,0) numAssetChartAcntId
		,ISNULL(Item.monAverageCost,0) monAverageCost
		,(CASE 
			WHEN ISNULL(@numProcessedQty,0) > @numQtyBuilt AND ISNULL(@numProcessedQty,0) <= @numQtyItemsReq  THEN ISNULL(@numProcessedQty,0) - @numQtyBuilt
			ELSE 0 
		END) numQtyBuilt
	FROM
		WorkOrder
	INNER JOIN
		Item
	ON
		WorkOrder.numItemCode = Item.numItemCode
	WHERE
		WorkOrder.numDomainID = @numDomainID
		AND WorkOrder.numWOId = @numWOID

	UPDATE
		WorkOrder
	SET 
		numQtyBuilt = ISNULL(numQtyBuilt,0) + (CASE 
													WHEN ISNULL(@numProcessedQty,0) > @numQtyBuilt AND ISNULL(@numProcessedQty,0) <= @numQtyItemsReq THEN (ISNULL(@numProcessedQty,0) - @numQtyBuilt)
													ELSE 0 
												END)
	WHERE
		WorkOrder.numDomainID = @numDomainID
		AND WorkOrder.numWOId = @numWOID	
END
GO