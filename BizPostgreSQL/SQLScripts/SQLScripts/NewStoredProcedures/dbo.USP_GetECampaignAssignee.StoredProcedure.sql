
GO
/****** Object:  StoredProcedure [dbo].[USP_GetECampaignAssignee]    Script Date: 06/04/2009 15:15:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- exec USP_GetECampaignAssignee 0,1
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetECampaignAssignee')
DROP PROCEDURE USP_GetECampaignAssignee
GO
CREATE PROCEDURE [dbo].[USP_GetECampaignAssignee]
               @numECampaignAssigneeID NUMERIC(9)  = 0,
               @numDomainID            NUMERIC(9)
AS
  SELECT numECampaignAssigneeID,
         EA.numECampaignID,
         EA.numEmailGroupID,
         [dbo].[FormatedDateFromDate](dtStartDate,EA.numDomainID) dtStartDate,
         EA.numDomainID,
         E.vcECampName,
         EG.vcEmailGroupName
  FROM   ECampaignAssignee EA
  JOIN ECampaign E 
  ON EA.numECampaignID = E.numECampaignID
  JOIN ProfileEmailGroup EG ON EA.numEmailGroupID = EG.numEmailGroupID
  WHERE  (numECampaignAssigneeID = @numECampaignAssigneeID
           OR @numECampaignAssigneeID = 0)
         AND EA.numDomainID = @numDomainID
