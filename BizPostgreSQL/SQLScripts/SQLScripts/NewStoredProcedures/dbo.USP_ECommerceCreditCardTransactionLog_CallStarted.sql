GO
IF EXISTS (SELECT * FROM sysobjects WHERE xtype = 'p' AND NAME = 'USP_ECommerceCreditCardTransactionLog_CallStarted' ) 
    DROP PROCEDURE USP_ECommerceCreditCardTransactionLog_CallStarted
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[USP_ECommerceCreditCardTransactionLog_CallStarted]
(
    @numDomainID NUMERIC(18,0)
	,@numOppID NUMERIC(18,0)
	,@monAmount DECIMAL(20,5)
)
AS 
BEGIN
	INSERT INTO ECommerceCreditCardTransactionLog
	(
		numDomainID
		,numOppID
		,monAmount
		,bitNsoftwareCallStarted
		,bitNSoftwareCallCompleted
	)
	VALUES
	(
		@numDomainID
		,@numOppID
		,@monAmount
		,1
		,0
	)

	SELECT SCOPE_IDENTITY()

END
GO