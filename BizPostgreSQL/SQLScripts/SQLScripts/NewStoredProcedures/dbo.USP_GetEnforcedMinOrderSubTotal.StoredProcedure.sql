SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetEnforcedMinOrderSubTotal')
DROP PROCEDURE USP_GetEnforcedMinOrderSubTotal
GO
CREATE PROCEDURE [dbo].[USP_GetEnforcedMinOrderSubTotal]  
	@numDomainID AS NUMERIC(9) = 0, 
	@numDivisionID NUMERIC(18,0),
	@numTotalAmount FLOAT          
	      
AS
BEGIN            
	DECLARE @numCompanyId NUMERIC(18,0), @vcProfile NUMERIC(18,0), @fltMinOrderAmount FLOAT = 0
	SELECT @numCompanyId = numCompanyId FROM DivisionMaster WHERE numDivisionID = @numDivisionID AND numDomainID = @numDomainID
	--SELECT @numCompanyId

	IF(SELECT COUNT(*) FROM CompanyInfo WHERE numCompanyId = @numCompanyId AND vcProfile <> 0 AND numDomainID = @numDomainID) > 0
	BEGIN
		SELECT @vcProfile = vcProfile FROM CompanyInfo WHERE numCompanyId = @numCompanyId AND numDomainID = @numDomainID
		--SELECT @vcProfile 
		--SELECT * FROM ListDetails WHERE numlistitemid = @vcProfile AND numDomainID = @numDomainID
		IF(SELECT COUNT(*) FROM ListDetails WHERE numlistitemid = @vcProfile AND numDomainID = @numDomainID) > 0
		BEGIN
			--SELECT * FROM ListDetails WHERE numlistitemid = @vcProfile AND numDomainID = @numDomainID
		
			SELECT @fltMinOrderAmount = fltMinOrderAmount 
			FROM SalesOrderRule 
			WHERE numlistitemid = @vcProfile
				AND bitEnforceMinOrderAmount <> 0
				AND numDomainID = @numDomainID

			IF @numTotalAmount >= @fltMinOrderAmount
			BEGIN
				SELECT 1 AS returnVal
				RETURN
			END
			ELSE
			BEGIN
				SELECT 0 AS returnVal, @fltMinOrderAmount AS MinOrderAmount
				RETURN
			END
		END
		ELSE
		BEGIN
			SELECT 1 AS returnVal
			RETURN
		END
	END
	ELSE
	BEGIN
		SELECT 1 AS returnVal
		RETURN
	END
END
GO
