GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_MassSalesFulfillmentShippingConfig_Save')
DROP PROCEDURE USP_MassSalesFulfillmentShippingConfig_Save
GO
CREATE PROCEDURE [dbo].[USP_MassSalesFulfillmentShippingConfig_Save]
(
	@numDomainID NUMERIC(18,0)
	,@numOrderSource NUMERIC(18,0)
	,@tintSourceType TINYINT
	,@tintType TINYINT
	,@vcPriorities VARCHAR(50)
	,@numShipVia NUMERIC(18,0)
	,@numShipService NUMERIC(18,0)
	,@bitOverride BIT
	,@fltWeight FLOAT
	,@numShipViaOverride NUMERIC(18,0)
)
AS
BEGIN
	IF EXISTS (SELECT ID FROM MassSalesFulfillmentShippingConfig WHERE numDomainID=@numDomainID AND numOrderSource=@numOrderSource AND tintSourceType=@tintSourceType)
	BEGIN
		UPDATE
			MassSalesFulfillmentShippingConfig
		SET
			tintType=@tintType
			,vcPriorities=@vcPriorities
			,numShipVia=@numShipVia
			,numShipService=@numShipService
			,bitOverride=@bitOverride
			,fltWeight=@fltWeight
			,numShipViaOverride=@numShipViaOverride
		WHERE
			numDomainID=@numDomainID 
			AND numOrderSource=@numOrderSource 
			AND tintSourceType=@tintSourceType
	END
	ELSE
	BEGIN
		INSERT INTO MassSalesFulfillmentShippingConfig
		(
			numDomainID
			,numOrderSource
			,tintSourceType
			,tintType
			,vcPriorities
			,numShipVia
			,numShipService
			,bitOverride
			,fltWeight
			,numShipViaOverride
		)
		VALUES
		(
			@numDomainID
			,@numOrderSource
			,@tintSourceType
			,@tintType
			,@vcPriorities
			,@numShipVia
			,@numShipService
			,@bitOverride
			,@fltWeight
			,@numShipViaOverride
		)
	END
END
GO