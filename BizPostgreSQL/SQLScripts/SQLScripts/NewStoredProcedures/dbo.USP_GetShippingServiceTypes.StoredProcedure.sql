GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_GetShippingServiceTypes' ) 
    DROP PROCEDURE USP_GetShippingServiceTypes
GO
CREATE PROCEDURE USP_GetShippingServiceTypes
    @numDomainID NUMERIC,
    @numRuleID NUMERIC,
    @numServiceTypeID NUMERIC = 0,
    @tintMode TINYINT = 0
AS 
BEGIN
    IF @tintMode = 2 
    BEGIN
		IF NOT EXISTS (SELECT 
							* 
						FROM 
							dbo.ShippingServiceTypes
						WHERE 
							numDomainID = @numDomainID
							AND intNsoftEnum > 0 
							AND ISNULL(numRuleID,0) = 0
						) 
		BEGIN
			INSERT INTO dbo.ShippingServiceTypes
			(	numShippingCompanyID,
				vcServiceName,
				intNsoftEnum,
				intFrom,
				intTo,
				fltMarkup,
				bitMarkupType,
				monRate,
				bitEnabled,
				numRuleID,
				numDomainID 
			)
			SELECT  numShippingCompanyID,
							vcServiceName,
							intNsoftEnum,
							intFrom,
							intTo,
							fltMarkup,
							bitMarkupType,
							monRate,
							bitEnabled,
							NULL,
							@numDomainID
			FROM    dbo.ShippingServiceTypes
			WHERE   numDomainID = 0 AND intNsoftEnum > 0 AND ISNULL(numRuleID,0) = 0
		END

        SELECT DISTINCT 
			* 
		FROM 
		(
			SELECT	
				numServiceTypeID,
				vcServiceName,
				intNsoftEnum,
				intFrom,
				intTo,
				fltMarkup,
				bitMarkupType,
				monRate,
				bitEnabled,
				numRuleID,
				numDomainID,
				numShippingCompanyID,
				CASE WHEN numShippingCompanyID = 91 THEN 'Fedex' 
						WHEN numShippingCompanyID = 88 THEN 'UPS'
						WHEN numShippingCompanyID = 90 THEN 'USPS'
						ELSE 'Other'
				END AS vcShippingCompany	 	
			FROM 
				dbo.ShippingServiceTypes
			WHERE 
				numDomainID = @numDomainID
				AND ISNULL(numRuleID,0) = 0
				AND (numServiceTypeID = @numServiceTypeID OR @numServiceTypeID = 0)
				AND intNsoftEnum > 0							
			UNION ALL	
			SELECT	101,'Amazon Standard',101,0,0,0,1,0,1,0,0,0,'Amazon' vcShippingCompany	 						
			UNION ALL
			SELECT	102,'ShippingMethodStandard',102,0,0,0,1,0,1,0,0,0,'E-Bay' vcShippingCompany	 							
			UNION ALL
			SELECT	103,'Other',103,0,0,0,1,0,1,0,0,0,'E-Bay' vcShippingCompany								
		) TABLE1 
		WHERE 
			(numServiceTypeID = @numServiceTypeID OR @numServiceTypeID = 0) 
		ORDER BY 
			vcShippingCompany
    END
    IF @tintMode=1
    BEGIN
	  	SELECT 
			numServiceTypeID,
            vcServiceName,
            intNsoftEnum,
            intFrom,
            intTo,
            fltMarkup,
            bitMarkupType,
            CAST(monRate AS DECIMAL(20,2)) AS monRate,
            bitEnabled,
            numRuleID,
            numDomainID,
			(select
   distinct  
    stuff((
        select ',' + LDI.vcData
        from ListDetails LDI
        where LDI.vcData = vcData AND LDI.numListID=36 and LDI.numDomainID=@numDomainID  AND numListItemID IN(SELECT Items FROM dbo.Split(vcItemClassification,',') WHERE Items<>'')
        order by LDI.vcData
        for xml path('')
    ),1,1,'') as vcData
from ListDetails AS LD WHERE LD.numListID=36 and LD.numDomainID=@numDomainID AND numListItemID IN(SELECT Items FROM dbo.Split(vcItemClassification,',') WHERE Items<>'')
group by vcData) As vcItemClassificationName
			,vcItemClassification
        FROM
			dbo.ShippingServiceTypes
        WHERE 
			numDomainID = @numDomainID
            AND numRuleID = @numRuleID
            AND (numServiceTypeID = @numServiceTypeID OR @numServiceTypeID = 0)
            AND ISNULL(intNsoftEnum,0)=0
	END
END
GO