SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ReportListMaster_Top10ReasonsDealWins')
DROP PROCEDURE USP_ReportListMaster_Top10ReasonsDealWins
GO
CREATE PROCEDURE [dbo].[USP_ReportListMaster_Top10ReasonsDealWins]
	@numDomainID NUMERIC(18,0)
	,@ClientTimeZoneOffset INT
	,@tintOppType NUMERIC(18,0)
	,@vcDealAmount VARCHAR(MAX)
AS
BEGIN 
	DECLARE @TotalDealWon FLOAT = 0.0

	SELECT 
		@TotalDealWon = COUNT(numOppId)
	FROM 
		OpportunityMaster
	WHERE 
		numDomainId=@numDomainID 
		AND tintOppType=(@tintOppType)
		AND tintOppStatus=1 
		AND bintOppToOrder IS NOT NULL
	
	SELECT TOP 10
		Reason
		,(NoOfOppToOrder * 100)/ (CASE WHEN ISNULL(@TotalDealWon,0) = 0 THEN 1 ELSE @TotalDealWon END) AS TotalDealWonPercent
	FROM
	(
		SELECT 
			ISNULL(ListDetails.vcData,'-') AS Reason
			,COUNT(numOppID) AS NoOfOppToOrder 
		FROM 
			OpportunityMaster 
		LEFT JOIN
			ListDetails
		ON
			OpportunityMaster.lngPConclAnalysis = ListDetails.numListItemID
			AND ListDetails.numListID = 12
			AND (ListDetails.numDomainID =@numDomainID OR ISNULL(ListDetails.constFlag,0) = 1)
		WHERE 
			OpportunityMaster.numDomainId=@numDomainID 
			AND tintOppType=(@tintOppType)
			AND tintOppStatus=1 
			AND bintOppToOrder IS NOT NULL
			
			AND ( 1 = (CASE WHEN CHARINDEX('1-5K',@vcDealAmount) > 0 THEN (CASE WHEN OpportunityMaster.monDealAmount BETWEEN 1000 AND 5000 THEN 1 ELSE 0 END) ELSE 0 END)
				OR 1 = (CASE WHEN CHARINDEX('5-10K',@vcDealAmount) > 0 THEN (CASE WHEN OpportunityMaster.monDealAmount BETWEEN 5000 AND 10000 THEN 1 ELSE 0 END) ELSE 0 END)
				OR 1 = (CASE WHEN CHARINDEX('10-20K',@vcDealAmount) > 0 THEN (CASE WHEN OpportunityMaster.monDealAmount BETWEEN 10000 AND 20000 THEN 1 ELSE 0 END) ELSE 0 END)
				OR 1 = (CASE WHEN CHARINDEX('20-50K',@vcDealAmount) > 0 THEN (CASE WHEN OpportunityMaster.monDealAmount BETWEEN 20000 AND 50000 THEN 1 ELSE 0 END) ELSE 0 END)
				OR 1 = (CASE WHEN CHARINDEX('>50K',@vcDealAmount) > 0 THEN (CASE WHEN OpportunityMaster.monDealAmount > 50000 THEN 1 ELSE 0 END) ELSE 0 END))
		GROUP BY
			ListDetails.vcData
	) TEMP
	ORDER BY
		NoOfOppToOrder DESC
		
END
GO

