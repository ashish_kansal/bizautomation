-- EXEC USP_ManageChartAccountOpening 72,84,100,'2009-10-07 16:34:28.373'
-- SELECT * FROM [ChartAccountOpening]
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ManageChartAccountOpening')
DROP PROCEDURE USP_ManageChartAccountOpening
GO
CREATE PROCEDURE [dbo].[USP_ManageChartAccountOpening]
    @numDomainId NUMERIC(9),
    @numAccountId NUMERIC(9),
    --@numFinYearId numeric(9),
    @monOpening DECIMAL(20,5),
    @dtOpenDate DATETIME
AS 
    DECLARE @numFinYearId NUMERIC(9)
    SELECT  @numFinYearId = [numFinYearId]
    FROM    [FinancialYear]
    WHERE   [numDomainId] = @numDomainId
            AND [bitCurrentYear] = 1
--    PRINT @numFinYearId
    IF @numFinYearId > 0 
        BEGIN
	

            IF ( SELECT COUNT(*)
                 FROM   [ChartAccountOpening]
                 WHERE  numDomainID = @numDomainID
                        AND numAccountID = @numAccountID AND numFinYearId = @numFinYearId
               ) > 0 
                BEGIN
                    UPDATE  ChartAccountOpening
                    SET     [numFinYearId] = @numFinYearId,
                            [monOpening] = @monOpening,
                            [dtOpenDate] = @dtOpenDate
                    WHERE   [numDomainId] = @numDomainId
                            AND [numAccountId] = @numAccountId
                END
            ELSE 
                BEGIN
                    INSERT  INTO ChartAccountOpening
                            (
                              [numDomainId],
                              [numAccountId],
                              [numFinYearId],
                              [monOpening],
                              [dtOpenDate]
	                      )
                    VALUES  (
                              @numDomainId,
                              @numAccountId,
                              @numFinYearId,
                              @monOpening,
                              @dtOpenDate
	                      )
           -- SELECT  SCOPE_IDENTITY() 

                END
        END