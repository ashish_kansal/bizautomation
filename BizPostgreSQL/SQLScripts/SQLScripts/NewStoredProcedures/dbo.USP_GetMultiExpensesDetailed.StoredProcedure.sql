GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetMultiExpensesDetailed')
DROP PROCEDURE USP_GetMultiExpensesDetailed
GO
CREATE PROCEDURE [dbo].[USP_GetMultiExpensesDetailed]
(@numParentDomainID  int,
 @dtFromDate datetime,
 @dtToDate datetime,
@numSubscriberID int)
as
begin

create table #TempExpenseDtl
(numDomainID int,
vcDomainName varchar(200),
vcDomainCode varchar(50),
vcAccountName VARCHAR(200),
Debit DECIMAL(20,5),
Credit DECIMAL(20,5),
Total DECIMAL(20,5));

INSERT INTO #TempExpenseDtl


select DN.numDomainID,DN.vcDomainName,DN.vcDomainCode,vcAccountName, Sum(Debit) as Debit, Sum(Credit) as Credit, Sum(Total) as Total
from VIEW_EXPENSEDAILYSUMMARY VARD
INNER JOIN 
(select * from DOMAIN DN where numSubscriberID=@numSubscriberID) DN ON VARD.numDomainID  =  DN.numDomainID 
INNER JOIN (select * from DOMAIN DNV where numSubscriberID=@numSubscriberID) DNV ON VARD.numDomainID = DNV.numDomainID INNER JOIN 
financialyear FY ON FY.numDomainID=DN.numDomainID AND 
FY.dtPeriodFrom <= @dtFromDate and dtPeriodTo >= @dtFromDate 
AND (DN.numDomainID = @numParentDomainID )
AND datEntry_Date between FY.dtPeriodFrom and @dtFromDate - 1
GROUP BY DN.numDomainID,DN.vcDomainName,DN.vcDomainCode,vcAccountName
union
select DN.numDomainID,DN.vcDomainName,DN.vcDomainCode,vcAccountName, Sum(Debit) as Debit, Sum(Credit) as Credit, Sum(Total) as Total
from VIEW_EXPENSEDAILYSUMMARY VARD
INNER JOIN 
(select * from DOMAIN DN where numSubscriberID=@numSubscriberID) DN ON VARD.numDomainID  =  DN.numDomainID 
INNER JOIN (select * from DOMAIN DNV where numSubscriberID=@numSubscriberID) DNV ON VARD.numDomainID = DNV.numDomainID
AND (DN.numDomainID = @numParentDomainID  )
AND datEntry_Date between @dtFromDate and @dtToDate  
GROUP BY DN.numDomainID,DN.vcDomainName,DN.vcDomainCode,vcAccountName

----------------------------------------
union 
--------------------------
select DN.numDomainID,DN.vcDomainName,DN.vcDomainCode,vcAccountName, Sum(Debit) as Debit, Sum(Credit) as Credit, Sum(Total) as Total
from VIEW_EXPENSEDAILYSUMMARY VARD
INNER JOIN 
(select * from DOMAIN DN where numSubscriberID=@numSubscriberID) DN ON VARD.vcDomainCode  like  DN.vcDomainCode + '%'
INNER JOIN (select * from DOMAIN DNV where numSubscriberID=@numSubscriberID) DNV ON VARD.numDomainID = DNV.numDomainID INNER JOIN 
financialyear FY ON FY.numDomainID=DN.numDomainID AND 
FY.dtPeriodFrom <= @dtFromDate and dtPeriodTo >= @dtFromDate 
AND (DN.numParentDomainID = @numParentDomainID )
AND datEntry_Date between FY.dtPeriodFrom and @dtFromDate - 1
GROUP BY DN.numDomainID,DN.vcDomainName,DN.vcDomainCode,vcAccountName
union
select DN.numDomainID,DN.vcDomainName,DN.vcDomainCode,vcAccountName, Sum(Debit) as Debit, Sum(Credit) as Credit, Sum(Total) as Total
from VIEW_EXPENSEDAILYSUMMARY VARD
INNER JOIN 
(select * from DOMAIN DN where numSubscriberID=@numSubscriberID) DN ON VARD.vcDomainCode  like  DN.vcDomainCode + '%'
INNER JOIN (select * from DOMAIN DNV where numSubscriberID=@numSubscriberID) DNV ON VARD.numDomainID = DNV.numDomainID
AND (DN.numParentDomainID = @numParentDomainID  )
AND datEntry_Date between @dtFromDate and @dtToDate  
GROUP BY DN.numDomainID,DN.vcDomainName,DN.vcDomainCode,vcAccountName

select 
vcAccountName,
isnull(Sum(isnull(Total,0)),0) as Total  
from #TempExpenseDtl
group by
vcAccountName;

drop table #TempExpenseDtl;

end
