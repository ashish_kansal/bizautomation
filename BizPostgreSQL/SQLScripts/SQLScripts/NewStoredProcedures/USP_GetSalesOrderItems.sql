--EXEC USP_GetSalesTemplateItems 6,72
--SELECT * FROM SalesTemplateItems
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetSalesOrderItems')
DROP PROCEDURE USP_GetSalesOrderItems
GO
CREATE PROCEDURE [dbo].[USP_GetSalesOrderItems]
    @numSalesOrderId NUMERIC,
    @numDomainID NUMERIC
AS 
    SELECT  X.*, (SELECT TOP 1 vcCategoryName FROM [Category] WHERE numCategoryID IN (SELECT numCategoryID FROM [ItemCategory] IC WHERE X.numItemCode = IC.[numItemID])) AS vcCategoryName
    FROM    ( SELECT  DISTINCT
                        STI.[numItemCode],
                        STI.[numUnitHour],
                        STI.[monPrice],
                        STI.[monTotAmount],
                        STI.[numSourceID],
                        STI.[numWarehouseItmsID],
                        I.[bitFreeShipping] FreeShipping, --Use from Item table
                        STI.[bitDiscountType],
                        STI.[fltDiscount],
                        STI.[monTotAmtBefDiscount],
                        OM.[numDomainID],
                        '' vcItemDesc,
                       (SELECT TOP 1 vcPathForTImage FROM dbo.ItemImages II WHERE II.numItemCode = I.numItemCode AND II.numDomainId= I.numDomainId AND II.bitDefault=1) vcPathForTImage,
                        I.vcItemName,
                        'False' AS DropShip,
					    0 as bitDropShip,
                        I.[bitTaxable],
                        ISNULL(STI.numUOMId,0) numUOM,
						ISNULL(STI.numUOMId,0) as numUOMId,
                        ISNULL(U.vcUnitName,'') vcUOMName,
						ISNULL(STI.numVendorWareHouse,0) as numVendorWareHouse,ISNULL(STI.numShipmentMethod,0) as numShipmentMethod,ISNULL(STI.numSOVendorId,0) as numSOVendorId,
						0 as bitWorkOrder,I.charItemType,dbo.fn_GetUOMName(I.numBaseUnit) AS vcBaseUOMName,0 AS numSortOrder,
						ISNULL(I.bitLotNo,0) AS bitLotNo
--                        ISNULL(dbo.fn_UOMConversion(I.numBaseUnit,I.numItemCode,I.numdomainid,0),1) UOMConversionFactor
              FROM      OpportunityItems STI
                        INNER JOIN item I ON I.numItemCode = STI.numItemCode
                        LEFT JOIN dbo.UOM U ON U.numUOMID = I.numSaleUnit
						LEFT JOIN OpportunityMaster AS OM ON OM.numOppId=STI.numOppId
              WHERE     STI.numOppId= @numSalesOrderId
                        AND OM.numDomainId = @numDomainID
            ) X