SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetRecordByLinkingID')
DROP PROCEDURE dbo.USP_GetRecordByLinkingID
GO
CREATE PROCEDURE [dbo].[USP_GetRecordByLinkingID]
(
	@numDomainID NUMERIC(18,0),
	@intImportType NUMERIC(18,0),
    @tintLinkingID TINYINT,
	@vcValue AS VARCHAR(500)
)
AS
BEGIN
	DECLARE @ItemCount INT

	If @intImportType = 20 -- Item
	BEGIN
		IF @tintLinkingID = 1 --ItemCode
		BEGIN
			SELECT @ItemCount = COUNT(*) FROM Item WHERE numDomainID=@numDomainID AND numItemCode=CAST(@vcValue AS NUMERIC(18,0))

			IF @ItemCount = 0
			BEGIN
				RAISERROR('ITEM_NOT_FOUND_WITH_GIVEN_ITEMCODE',16,1)
			END
			ELSE IF @ItemCount > 1
			BEGIN
				RAISERROR('MULTIPLE_ITEMS_FOUND_WITH_GIVEN_ITEMCODE',16,1)
			END
			ELSE
			BEGIN
				SELECT numItemCode FROM Item WHERE numDomainID=@numDomainID AND numItemCode=CAST(@vcValue AS NUMERIC(18,0))
			END
		END
		ELSE IF @tintLinkingID = 2 --SKU
		BEGIN
			SELECT @ItemCount = COUNT(*) FROM Item WHERE numDomainID=@numDomainID AND  LOWER(vcSKU)= LOWER(@vcValue)

			IF @ItemCount = 0
			BEGIN
				RAISERROR('ITEM_NOT_FOUND_WITH_GIVEN_SKU',16,1)
			END
			ELSE IF @ItemCount > 1
			BEGIN
				RAISERROR('MULTIPLE_ITEMS_FOUND_WITH_GIVEN_SKU',16,1)
			END
			ELSE
			BEGIN
				SELECT numItemCode FROM Item WHERE numDomainID=@numDomainID AND  LOWER(vcSKU)= LOWER(@vcValue)
			END
		END
		ELSE IF @tintLinkingID = 3 --UPC
		BEGIN
			SELECT @ItemCount = COUNT(*) FROM Item WHERE numDomainID=@numDomainID AND  LOWER(numBarCodeId)= LOWER(@vcValue)

			IF @ItemCount = 0
			BEGIN
				RAISERROR('ITEM_NOT_FOUND_WITH_GIVEN_UPC',16,1)
			END
			ELSE IF @ItemCount > 1
			BEGIN
				RAISERROR('MULTIPLE_ITEMS_FOUND_WITH_GIVEN_UPC',16,1)
			END
			ELSE
			BEGIN
				SELECT numItemCode FROM Item WHERE numDomainID=@numDomainID AND LOWER(numBarCodeId)=LOWER(@vcValue)
			END
		END
		ELSE IF @tintLinkingID = 4 --ModelID
		BEGIN
			SELECT @ItemCount = COUNT(*) FROM Item WHERE numDomainID=@numDomainID AND  LOWER(vcModelID)= LOWER(@vcValue)

			IF @ItemCount = 0
			BEGIN
				RAISERROR('ITEM_NOT_FOUND_WITH_GIVEN_ModelID',16,1)
			END
			ELSE IF @ItemCount > 1
			BEGIN
				RAISERROR('MULTIPLE_ITEMS_FOUND_WITH_GIVEN_ModelID',16,1)
			END
			ELSE
			BEGIN
				SELECT numItemCode FROM Item WHERE numDomainID=@numDomainID AND  LOWER(vcModelID)= LOWER(@vcValue)
			END
		END
	END
	ELSE IF @intImportType = 133 -- Organization
	BEGIN
		IF @tintLinkingID = 1 -- Non Biz Company ID
		BEGIN
			SELECT @ItemCount = COUNT(*) FROM ImportOrganizationContactReference WHERE numDomainID=@numDomainID AND  LOWER(numNonBizCompanyID)= LOWER(@vcValue)

			IF @ItemCount = 0
			BEGIN
				RAISERROR('ORGANIZATION_NOT_FOUND_WITH_GIVEN_NON BIZ COMPANY ID',16,1)
			END
			ELSE IF @ItemCount > 1
			BEGIN
				RAISERROR('MULTIPLE_ORGANIZATIONS_FOUND_WITH_GIVEN_NON BIZ COMPANY ID',16,1)
			END
			ELSE
			BEGIN
				SELECT numDivisionID,numCompanyID FROM ImportOrganizationContactReference WHERE numDomainID=@numDomainID AND  LOWER(numNonBizCompanyID)= LOWER(@vcValue)
			END
		END
		ELSE IF @tintLinkingID = 2 -- Organization ID
		BEGIN
			SELECT @ItemCount = COUNT(*) FROM DivisionMaster WHERE numDomainID=@numDomainID AND numDivisionID=CAST(@vcValue AS NUMERIC(18,0))

			IF @ItemCount = 0
			BEGIN
				RAISERROR('ORGANIZATION_NOT_FOUND_WITH_GIVEN_ORGANIZATION ID',16,1)
			END
			ELSE IF @ItemCount > 1
			BEGIN
				RAISERROR('MULTIPLE_ORGANIZATIONS_FOUND_WITH_GIVEN_ORGANIZATION ID',16,1)
			END
			ELSE
			BEGIN
				SELECT numDivisionID,numCompanyID  FROM DivisionMaster WHERE numDomainID=@numDomainID AND numDivisionID=CAST(@vcValue AS NUMERIC(18,0))
			END
		END
	END
	ELSE IF @intImportType = 134 -- Contact
	BEGIN
		IF @tintLinkingID = 1 -- Non Biz Contact ID
		BEGIN
			SELECT @ItemCount = COUNT(*) FROM ImportActionItemReference WHERE numDomainID=@numDomainID AND  LOWER(numNonBizContactID)=@vcValue

			IF @ItemCount = 0
			BEGIN
				RAISERROR('CONTACT_NOT_FOUND_WITH_GIVEN_NON BIZ CONTACT ID',16,1)
			END
			ELSE IF @ItemCount > 1
			BEGIN
				RAISERROR('MULTIPLE_CONTACTS_FOUND_WITH_GIVEN_NON BIZ CONTACT ID',16,1)
			END
			ELSE
			BEGIN
				SELECT numContactID,numDivisionID,numCompanyID  FROM ImportActionItemReference WHERE numDomainID=@numDomainID AND  LOWER(numNonBizContactID)=@vcValue
			END
		END
		ELSE IF @tintLinkingID = 2 -- Contact ID
		BEGIN
			SELECT @ItemCount = COUNT(*) FROM AdditionalContactsInformation WHERE numDomainID=@numDomainID AND numContactId=CAST(@vcValue AS NUMERIC(18,0))

			IF @ItemCount = 0
			BEGIN
				RAISERROR('CONTACT_NOT_FOUND_WITH_GIVEN_CONTACT ID',16,1)
			END
			ELSE IF @ItemCount > 1
			BEGIN
				RAISERROR('MULTIPLE_CONTACTS_FOUND_WITH_GIVEN_CONTACT ID',16,1)
			END
			ELSE
			BEGIN
				SELECT numContactId FROM AdditionalContactsInformation WHERE numDomainID=@numDomainID AND numContactId=CAST(@vcValue AS NUMERIC(18,0))
			END
		END
	END
	
END