--Created by anoop Jayaraj  
  
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_FormCourseHierachy')
DROP PROCEDURE USP_FormCourseHierachy
GO
CREATE PROCEDURE USP_FormCourseHierachy  
  
as  
  
  
declare @numDomainID as numeric(9)  
declare @numLevelID as numeric  
declare @Level1 as nvarchar(255)  
declare @Level2 as nvarchar(255)  
declare @Level3 as nvarchar(255)  
declare @Level4 as nvarchar(255)  
declare @Level5 as nvarchar(255)  
  
declare @CatID1 as numeric  
declare @CatID2 as numeric  
declare @CatID3 as numeric  
declare @CatID4 as numeric  
declare @CatID5 as numeric  
  
set @CatID1 =0  
set  @CatID2 =0  
set  @CatID3 =0  
set  @CatID4 =0  
set  @CatID5 =0  
  
  
set @numLevelID=0  
set @numDomainID=104  
select top 1 @numLevelID=LevelID,@Level1=Level1,@Level2=Level2,@Level3=Level3,@Level4=Level4,@Level5=Level5  from CourseLevel  
while @numLevelID>0  
begin  
  
  
  
  
If @Level1 is not null  
begin  
  
 if not exists(select numCategoryID from Category where numDomainID=@numDomainID and vcCategoryName=@Level1 and tintLevel=2)  
 begin  
  insert into  Category(vcCategoryName,tintLevel, numDepCategory, bitLink, vcLink, numDomainID)  
  values (@Level1,2, 65, 0, null, @numDomainID)  
  set @CatID1=@@identity  
 end  
 else   
    update Category set @CatID1=numCategoryID,numDepCategory=65 where numDomainID=@numDomainID and vcCategoryName=@Level1 and tintLevel=2  
  
end  
If @Level2 is not null  
begin  
  
 if not exists(select numCategoryID from Category where numDomainID=@numDomainID and vcCategoryName=@Level2 and tintLevel=3)  
 begin  
  insert into  Category(vcCategoryName,tintLevel, numDepCategory, bitLink, vcLink, numDomainID)  
  values (@Level2,3, @CatID1, 0, null, @numDomainID)  
  set @CatID2=@@identity  
 end  
 else   
    update Category set @CatID2=numCategoryID,numDepCategory=@CatID1  where numDomainID=@numDomainID and vcCategoryName=@Level2 and tintLevel=3  
  
end  
If @Level3 is not null  
begin  
  
 if not exists(select numCategoryID from Category where numDomainID=@numDomainID and vcCategoryName=@Level3 and tintLevel=4)  
 begin  
  insert into  Category(vcCategoryName,tintLevel, numDepCategory, bitLink, vcLink, numDomainID)  
  values (@Level3,4, @CatID2, 0, null, @numDomainID)  
  set @CatID3=@@identity  
        print @Level3  
  print @CatID3  
 end  
 else   
    update Category set @CatID3=numCategoryID,numDepCategory=@CatID2  where numDomainID=@numDomainID and vcCategoryName=@Level3 and tintLevel=4  
  
end  
If @Level4 is not null  
begin  
  
 if not exists(select numCategoryID from Category where numDomainID=@numDomainID and vcCategoryName=@Level4 and tintLevel=5)  
 begin  
  insert into  Category(vcCategoryName,tintLevel, numDepCategory, bitLink, vcLink, numDomainID)  
  values (@Level4,5, @CatID3, 0, null, @numDomainID)  
  set @CatID4=@@identity  
 end  
 else   
    update Category set @CatID4=numCategoryID,numDepCategory=@CatID3  where numDomainID=@numDomainID and vcCategoryName=@Level4  and tintLevel=5  
  
end  
  
If @Level5 is not null  
begin  
  
 if not exists(select numCategoryID from Category where numDomainID=@numDomainID and vcCategoryName=@Level5 and tintLevel=6)  
 begin  
  insert into  Category(vcCategoryName,tintLevel, numDepCategory, bitLink, vcLink, numDomainID)  
  values (@Level5,6, @CatID4, 0, null, @numDomainID)  
 end  
 else   
 update Category set numDepCategory=@CatID4  where numDomainID=@numDomainID and vcCategoryName=@Level5  and tintLevel=6  
  
  
end  
  
set @CatID1 =0  
set  @CatID2 =0  
set  @CatID3 =0  
set  @CatID4 =0  
set  @CatID5 =0  
  
  
select top 1 @numLevelID=LevelID,@Level1=Level1,@Level2=Level2,@Level3=Level3,@Level4=Level4,@Level5=Level5  from CourseLevel  
where LevelID>@numLevelID  
  
 if @@rowcount=0 set @numLevelID=0  
end