GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_OpportunityMaster_CloneOrderLineItem')
DROP PROCEDURE dbo.USP_OpportunityMaster_CloneOrderLineItem
GO
CREATE PROCEDURE [dbo].[USP_OpportunityMaster_CloneOrderLineItem]
(
	@numDomainID NUMERIC(18,0),
	@numUserCntID NUMERIC(18,0),
    @numOppID NUMERIC(18,0),
    @numOppItemID NUMERIC(18,0)
)
AS
BEGIN
	IF EXISTS(SELECT numOppId FROM OpportunityMaster WHERE numOppId=@numOppID AND ISNULL(tintshipped,0)=1)
	BEGIN
		RAISERROR('CLOSED_ORDER',16,1)
	END
	ELSE
	BEGIN
		BEGIN TRY
		BEGIN TRANSACTION
			DECLARE @tintCommitAllocation TINYINT
			DECLARE @tintOppType TINYINT
			DECLARE @tintOppStatus TINYINT
			DECLARE @numDivisionId NUMERIC(18,0)
			DECLARE @bitAllocateInventoryOnPickList BIT

			SELECT @tintCommitAllocation=ISNULL(tintCommitAllocation,1) FROM Domain WHERE numDomainId=@numDomainId
			SELECT @numDivisionId=numDivisionID, @tintOppType=tintOppType,@tintOppStatus=ISNULL(tintOppStatus,0) FROM OpportunityMaster WHERE numOppId=@numOppID

			SELECT 
				@bitAllocateInventoryOnPickList=ISNULL(bitAllocateInventoryOnPickList,0)
			FROM 
				DivisionMaster D
			JOIN 
				CompanyInfo C 
			ON 
				C.numCompanyId=D.numCompanyID
			WHERE 
				D.numDivisionID = @numDivisionId

			--REVERTING BACK THE WAREHOUSE ITEMS                  
			IF @tintOppStatus = 1 AND @tintCommitAllocation=1
			BEGIN               
				EXEC USP_RevertDetailsOpp @numOppID,0,0,@numUserCntID                  
			END       

			DECLARE @vcOppItemsColumns VARCHAR(MAX) = STUFF((SELECT ',' + COLUMN_NAME
															FROM INFORMATION_SCHEMA.COLUMNS
															WHERE TABLE_NAME = N'OpportunityItems' AND COLUMN_NAME NOT IN ('numoppitemtCode','bitMappingRequired','bitRequiredWarehouseCorrection','numQtyReceived','numQtyPicked','numQtyShipped','numUnitHourReceived')
															FOR XML PATH('')),1,1,'')

			DECLARE @vcOppKitItemsColumns VARCHAR(MAX) = STUFF((SELECT ',' + COLUMN_NAME
															FROM INFORMATION_SCHEMA.COLUMNS
															WHERE TABLE_NAME = N'OpportunityKitItems' AND COLUMN_NAME NOT IN ('numOppChildItemID','numOppItemID','numQtyShipped')
															FOR XML PATH('')),1,1,'')

			DECLARE @vcOppKitChildItemsColumns VARCHAR(MAX) = STUFF((SELECT ',' + COLUMN_NAME
																	FROM INFORMATION_SCHEMA.COLUMNS
																	WHERE TABLE_NAME = N'OpportunityKitChildItems' AND COLUMN_NAME NOT IN ('numOppKitChildItemID','numOppItemID','numOppChildItemID','numQtyShipped')
																	FOR XML PATH('')),1,1,'')

			DECLARE @numNewOppItemID NUMERIC(18,0)
			DECLARE @numNewOppChildItemID NUMERIC(18,0)
			DECLARE @numNewOppKitChildItemID NUMERIC(18,0)
			DECLARE @sqlText AS NVARCHAR(MAX) = CONCAT('INSERT INTO OpportunityItems (',@vcOppItemsColumns,') SELECT ',@vcOppItemsColumns,' FROM OpportunityItems WHERE numOppID=',@numOppID,' AND numoppitemtCode=',@numOppItemID,'; SELECT @numNewOppItemID = SCOPE_IDENTITY()')

			EXEC sp_executesql @sqlText, N'@numNewOppItemID NUMERIC(18,0) OUTPUT', @numNewOppItemID OUTPUT

			DECLARE @TEMP TABLE
			(
				ID INT IDENTITY(1,1)
				,numOppChildItemID NUMERIC(18,0)
			)

			INSERT INTO @TEMP
			(
				numOppChildItemID
			)
			SELECT 
				numOppChildItemID 
			FROM 
				OpportunityKitItems 
			WHERE 
				numOppId=@numOppID 
				AND numOppItemID=@numOppItemID

			DECLARE @i AS INT = 1
			DECLARE @iCount AS INT
			DECLARE @numTempOppChildItemID NUMERIC(18,0)
			SELECT @iCount=COUNT(*) FROM @TEMP

			WHILE @i <= @iCount
			BEGIN
				SELECT @numTempOppChildItemID=numOppChildItemID FROM @TEMP WHERE ID=@i

				SET @sqlText = CONCAT('INSERT INTO OpportunityKitItems (',@vcOppKitItemsColumns,',numOppItemID) SELECT ',@vcOppKitItemsColumns,',',@numNewOppItemID,' FROM OpportunityKitItems WHERE numOppID=',@numOppID,' AND numOppItemID=',@numOppItemID,' AND numOppChildItemID=',@numTempOppChildItemID,'; SELECT @numNewOppChildItemID = SCOPE_IDENTITY()')
				EXEC sp_executesql @sqlText, N'@numNewOppChildItemID NUMERIC(18,0) OUTPUT',@numNewOppChildItemID OUTPUT

				SET @sqlText = CONCAT('INSERT INTO OpportunityKitChildItems (',@vcOppKitChildItemsColumns,',numOppItemID,numOppChildItemID) SELECT ',@vcOppKitChildItemsColumns,',',@numNewOppItemID,',',@numNewOppChildItemID,' FROM OpportunityKitChildItems WHERE numOppID=',@numOppID,' AND numOppItemID=',@numOppItemID,' AND numOppChildItemID=',@numTempOppChildItemID,';')
				EXEC sp_executesql @sqlText

				SET @i = @i + 1
			END

			IF NOT (@tintCommitAllocation=2 AND @bitAllocateInventoryOnPickList=1)
			BEGIN
				EXEC USP_ManageSOWorkOrder @numDomainId,@numOppID,@numUserCntID
			END

			IF @tintOppStatus=1 AND @tintCommitAllocation=1             
			BEGIN         
				EXEC USP_UpdatingInventoryonCloseDeal @numOppID,0,@numUserCntID
			END  

			IF @tintOppStatus = 1
			BEGIN
				IF (SELECT COUNT(*) FROM OpportunityItems WHERE numOppId=@numOppID AND ISNULL(numWarehouseItmsID,0) > 0 AND ISNULL(bitDropShip,0)=0) <>
					(SELECT COUNT(*) FROM OpportunityItems INNER JOIN WareHouseItems ON OpportunityItems.numWarehouseItmsID=WareHouseItems.numWareHouseItemID AND OpportunityItems.numItemCode=WareHouseItems.numItemID WHERE numOppId=@numOppID AND OpportunityItems.numWarehouseItmsID > 0 AND ISNULL(bitDropShip,0)=0)
				BEGIN
					RAISERROR('INVALID_ITEM_WAREHOUSES',16,1)
					RETURN
				END
			END

			IF(SELECT 
					COUNT(*) 
				FROM 
					OpportunityBizDocItems OBDI 
				INNER JOIN 
					OpportunityBizDocs OBD 
				ON 
					OBDI.numOppBizDocID=OBD.numOppBizDocsID 
				WHERE 
					OBD.numOppId=@numOppID AND OBDI.numOppItemID NOT IN (SELECT numoppitemtCode FROM OpportunityItems WHERE numOppId=@numOppID)) > 0
			BEGIN
				RAISERROR('ITEM_USED_IN_BIZDOC',16,1)
				RETURN
			END

			IF(SELECT 
					COUNT(*)
				FROM
					OpportunityItems OI
				INNER JOIN
				(
					SELECT
						numOppItemID
						,SUM(numUnitHour) numInvoiceBillQty
					FROM 
						OpportunityBizDocItems OBDI 
					INNER JOIN 
						OpportunityBizDocs OBD 
					ON 
						OBDI.numOppBizDocID=OBD.numOppBizDocsID 
					WHERE 
						OBD.numOppId=@numOppID
						AND ISNULL(OBD.bitAuthoritativeBizDocs,0) = 1
					GROUP BY
						numOppItemID
				) AS TEMP
				ON
					TEMP.numOppItemID = OI.numoppitemtCode
				WHERE
					OI.numOppId = @numOppID
					AND OI.numUnitHour < Temp.numInvoiceBillQty
				) > 0
			BEGIN
				RAISERROR('ORDERED_QUANTITY_CAN_NOT_BE_LESS_THEN_INVOICE/BILL_QTY',16,1)
				RETURN
			END

			IF @tintOppType=1 AND @tintCommitAllocation=2 AND @bitAllocateInventoryOnPickList=1
			BEGIN
				IF(SELECT 
						COUNT(*)
					FROM
					(
						SELECT
							numOppItemID
							,SUM(numUnitHour) PackingSlipUnits
						FROM 
							OpportunityBizDocItems OBDI 
						INNER JOIN 
							OpportunityBizDocs OBD 
						ON 
							OBDI.numOppBizDocID=OBD.numOppBizDocsID 
						WHERE 
							OBD.numOppId=@numOppID
							AND OBD.numBizDocId=29397 --Picking Slip
						GROUP BY
							numOppItemID
					) AS TEMP
					INNER JOIN
						OpportunityItems OI
					ON
						TEMP.numOppItemID = OI.numoppitemtCode
					WHERE
						OI.numUnitHour < PackingSlipUnits
					) > 0
				BEGIN
					RAISERROR('ORDERED_QUANTITY_CAN_NOT_BE_LESS_THEN_PACKING_SLIP_QTY',16,1)
					RETURN
				END
			END

			IF EXISTS (SELECT 
					numoppitemtCode 
				FROM 
					OpportunityItems 
				INNER JOIN 
					Item 
				ON 
					OpportunityItems.numItemCode=Item.numItemCode 
				WHERE 
					numOppId=@numOppID 
					AND charItemType='P' 
					AND ISNULL(numWarehouseItmsID,0) = 0 
					AND ISNULL(bitDropShip,0) = 0)
			BEGIN
				RAISERROR('WAREHOUSE_REQUIRED',16,1)
			END

			IF (SELECT COUNT(*) FROM WorkOrder WHERE numDomainID=@numDomainID AND numOppId=@numOppId AND ISNULL(numOppItemID,0) > 0 AND numOppItemID NOT IN (SELECT OIInner.numoppitemtCode FROM OpportunityItems OIInner WHERE OIInner.numOppId=@numOppID)) > 0
			BEGIN
				RAISERROR ( 'WORK_ORDER_EXISTS', 16, 1 ) ;
			END

			IF (SELECT 
					COUNT(*) 
				FROM 
					WorkOrder 
				INNER JOIN 
					OpportunityItems 
				ON 
					OpportunityItems.numOppId=@numOppID
					AND OpportunityItems.numoppitemtCode=WorkOrder.numOppItemID
				WHERE 
					WorkOrder.numDomainID=@numDomainID 
					AND WorkOrder.numOppId=@numOppID
					AND WorkOrder.numWareHouseItemId <> OpportunityItems.numWarehouseItmsID) > 0
			BEGIN
				RAISERROR ('CAN_NOT_CHANGE_ASSEMBLY_WAREHOUSE_WORK_ORDER_EXISTS', 16, 1 ) ;
			END

			--Delete Tax for Opportunity Items if item deleted 
			DELETE FROM OpportunityItemsTaxItems WHERE numOppId=@numOppId AND numOppItemID NOT IN (SELECT numoppitemtCode FROM OpportunityItems WHERE numOppId=@numOppID)

			--Insert Tax for Opportunity Items
			INSERT INTO dbo.OpportunityItemsTaxItems 
			(
				numOppId,
				numOppItemID,
				numTaxItemID,
				numTaxID
			) 
			SELECT 
				@numOppId,
				OI.numoppitemtCode,
				TI.numTaxItemID,
				0
			FROM 
				dbo.OpportunityItems OI 
			JOIN 
				dbo.ItemTax IT 
			ON 
				OI.numItemCode=IT.numItemCode 
			JOIN
				TaxItems TI 
			ON 
				TI.numTaxItemID = IT.numTaxItemID 
			WHERE 
				OI.numOppId=@numOppID 
				AND IT.bitApplicable=1 
				AND OI.numoppitemtCode NOT IN (SELECT numOppItemID FROM OpportunityItemsTaxItems WHERE numOppId=@numOppID)
			UNION
			SELECT 
				@numOppId,
				OI.numoppitemtCode,
				0,
				0
			FROM 
				dbo.OpportunityItems OI 
			JOIN 
				dbo.Item I 
			ON 
				OI.numItemCode=I.numItemCode
			WHERE 
				OI.numOppId=@numOppID 
				AND I.bitTaxable=1 
				AND OI.numoppitemtCode NOT IN (SELECT numOppItemID FROM OpportunityItemsTaxItems WHERE numOppId=@numOppID)
			UNION
			SELECT
				@numOppId,
				OI.numoppitemtCode,
				1,
				TD.numTaxID
			FROM
				dbo.OpportunityItems OI 
			INNER JOIN
				ItemTax IT
			ON
				IT.numItemCode = OI.numItemCode
			INNER JOIN
				TaxDetails TD
			ON
				TD.numTaxID = IT.numTaxID
				AND TD.numDomainId = @numDomainId
			WHERE
				OI.numOppId = @numOppID
				AND OI.numoppitemtCode NOT IN (SELECT numOppItemID FROM OpportunityItemsTaxItems WHERE numOppId=@numOppID)

			UPDATE 
				OI
			SET
				OI.vcInclusionDetail = [dbo].[GetOrderAssemblyKitInclusionDetails](OI.numOppID,OI.numoppitemtCode,OI.numUnitHour,1,1)
			FROM 
				OpportunityItems OI
			INNER JOIN
				Item I
			ON
				OI.numItemCode = I.numItemCode
			WHERE
				numOppId=@numOppID
				AND ISNULL(I.bitKitParent,0) = 1
  
			UPDATE OpportunityMaster SET monDealAmount=dbo.GetDealAmount(@numOppID,getdate(),0),numModifiedBy=@numUserCntID,bintModifiedDate=GETUTCDATE() WHERE numOppId=@numOppID
		COMMIT
		END TRY
		BEGIN CATCH
			DECLARE @ErrorMessage NVARCHAR(4000)
			DECLARE @ErrorNumber INT
			DECLARE @ErrorSeverity INT
			DECLARE @ErrorState INT
			DECLARE @ErrorLine INT
			DECLARE @ErrorProcedure NVARCHAR(200)

			IF @@TRANCOUNT > 0
				ROLLBACK TRANSACTION;

			SELECT 
				@ErrorMessage = ERROR_MESSAGE(),
				@ErrorNumber = ERROR_NUMBER(),
				@ErrorSeverity = ERROR_SEVERITY(),
				@ErrorState = ERROR_STATE(),
				@ErrorLine = ERROR_LINE(),
				@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

			RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);
		END CATCH
	END

END
GO