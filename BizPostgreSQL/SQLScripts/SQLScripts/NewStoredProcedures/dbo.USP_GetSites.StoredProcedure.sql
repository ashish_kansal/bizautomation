
GO
/****** Object:  StoredProcedure [dbo].[USP_GetSites]    Script Date: 08/08/2009 12:42:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetSites')
DROP PROCEDURE USP_GetSites
GO
CREATE PROCEDURE [dbo].[USP_GetSites]
          @numSiteID   NUMERIC(9)  = 0,
          @numDomainID NUMERIC(9)
AS
  SELECT [numSiteID],
         [vcSiteName],
         [vcDescription],
         [vcHostName],
         [numDomainID],
         bitIsActive,
		 CASE WHEN [bitIsActive] = 1 THEN 'Inactivate'
		 ELSE 'Activate'
		 END AS IsActive,
		 ISNULL(vcLiveURL,'') vcLiveURL,
		 CASE LEN(ISNULL(vcLiveURL,'')) WHEN 0 THEN  ISNULL(vcHostName ,'') +'.bizautomation.com' ELSE vcLiveURL END vcPreviewURL,ISNULL(numCurrencyID,0) AS numCurrencyID,
		 ISNULL(bitOnePageCheckout,0) bitOnePageCheckout,
		 ISNULL(tintRateType,0) AS tintRateType,
		(SELECT STUFF((SELECT ',' + CONVERT(VARCHAR(50) , numCategoryID)  FROM dbo.SiteCategories WHERE numSiteID = @numSiteID FOR XML PATH('')), 1, 1, '')) AS vcItemCategories,
		bitHtml5,
		vcMetaTags,
		bitSSLRedirect
  FROM   Sites
  WHERE  ([numSiteID] = @numSiteID
           OR @numSiteID = 0)
         AND [numDomainID] = @numDomainID


