
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_BroadcastErrorLog_Insert' ) 
    DROP PROCEDURE USP_BroadcastErrorLog_Insert
GO

CREATE PROCEDURE USP_BroadcastErrorLog_Insert  
(  
 @numBroadCastDtlID NUMERIC(18,0),  
 @vcType NVARCHAR(100),  
 @vcMessage NVARCHAR(MAX),
 @vcStackStrace NVARCHAR(MAX)
)  
AS  
BEGIN  
-- SET NOCOUNT ON added to prevent extra result sets from  
-- interfering with SELECT statements.  
SET NOCOUNT ON;  

INSERT INTO BroadcastErrorLog
(
	numBroadcastDtlID,
	vcType,
	vcMessage,
	vcStackStrace
)
VALUES
(
	@numBroadCastDtlID,
	@vcType,
	@vcMessage,
	@vcStackStrace
) 

END  

