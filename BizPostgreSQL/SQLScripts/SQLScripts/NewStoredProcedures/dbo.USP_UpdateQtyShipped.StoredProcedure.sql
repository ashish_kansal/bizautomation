GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_UpdateQtyShipped')
DROP PROCEDURE USP_UpdateQtyShipped
GO
CREATE PROCEDURE  USP_UpdateQtyShipped
    @numQtyShipped FLOAT,
    @numOppItemID NUMERIC(9),
    @numUserCntID NUMERIC(9),
    @vcError VARCHAR(500) ='' OUTPUT
AS 
BEGIN
-- TRANSACTION FROM CODE IS USED FOR PURCHASE FULFILLMENT
BEGIN TRY       
	DECLARE @numDomain AS NUMERIC(18,0)          
    DECLARE @onAllocation AS FLOAT          
    DECLARE @numWarehouseItemID AS NUMERIC       
    DECLARE @numOldQtyShipped AS FLOAT       
    DECLARE @numNewQtyShipped AS FLOAT       
    DECLARE @numOppId NUMERIC
	DECLARE @numUnits FLOAT
    DECLARE @Kit AS BIT                   
	DECLARE @vcOppName VARCHAR(200)

         
    SELECT 
		@numWarehouseItemID = ISNULL([numWarehouseItmsID], 0)
		,@numOldQtyShipped=ISNULL(numQtyShipped,0)
		,@numOppId=numOppId
		,@numUnits=numUnitHour
		,@Kit= (CASE WHEN bitKitParent=1 AND bitAssembly=1 THEN 0 WHEN bitKitParent=1 THEN 1 ELSE 0 END)
		,@numDomain = I.[numDomainID]
    FROM  
		OpportunityItems OI 
	INNER JOIN 
		Item I 
	ON 
		OI.numItemCode=I.numItemCode
    WHERE  
		[numoppitemtCode] = @numOppItemID
        
    SET @numNewQtyShipped = @numQtyShipped-@numOldQtyShipped;
        
    DECLARE @description AS VARCHAR(100)
	SET @description=CONCAT('SO Qty Shipped (Qty:',@numUnits,' Shipped:',@numNewQtyShipped,')')

	IF @Kit=1
	BEGIN
		SELECT 
			OKI.numOppChildItemID
			,OKI.numWareHouseItemId
			,numQtyItemsReq
			,numQtyItemsReq_Orig
			,numQtyShipped
			,ISNULL(numAllocation, 0) AS numAllocation
			,ROW_NUMBER() OVER(ORDER BY numOppChildItemID) AS RowNumber 
		INTO 
			#tempKits
		FROM 
			OpportunityKitItems OKI 
		INNER JOIN 
			Item I 
		ON 
			OKI.numChildItemID=I.numItemCode  
		INNER JOIN 
			WareHouseItems WI 
		ON 
			WI.numWareHouseItemID=OKI.numWareHouseItemID  
		WHERE 
			charitemtype='P' 
			AND OKI.numWareHouseItemId > 0 
			AND OKI.numWareHouseItemId is not null 
			AND numOppItemID=@numOppItemID 
				
		IF(SELECT COUNT(*) FROM #tempKits WHERE numAllocation - ((@numQtyShipped * numQtyItemsReq_Orig) - numQtyShipped) < 0)>0
		BEGIN
			SELECT TOP 1 
				@vcOppName=ISNULL([vcPOppName],'') 
			FROM 
				[OpportunityMaster] 
			WHERE 
				[numOppId] IN (SELECT [numOppId] FROM [OpportunityItems] WHERE [numoppitemtCode]=@numOppItemID)
			
			SET @vcError ='You do not have enough KIT inventory to support this shipment('+ @vcOppName +'). Modify your Qty-Shipped to an amount equal to or less than the Qty-On-Allocation and try again.' 
		END
		ELSE
		BEGIN
			DECLARE @minRowNumber NUMERIC(9),@maxRowNumber NUMERIC(9),@KitWareHouseItemID NUMERIC(9)
			DECLARE @KitonAllocation as FLOAT,@KitQtyShipped as FLOAT,@KiyQtyItemsReq_Orig as FLOAT,@KiyQtyItemsReq as FLOAT  
			DECLARE @KitNewQtyShipped AS FLOAT,@KitOppChildItemID AS NUMERIC(9)
					
			SELECT  @minRowNumber = MIN(RowNumber),@maxRowNumber = MAX(RowNumber) FROM #tempKits

			WHILE  @minRowNumber <= @maxRowNumber
			BEGIN
				SELECT @KitOppChildItemID=numOppChildItemID,@KitWareHouseItemID=numWareHouseItemID,@KiyQtyItemsReq=numQtyItemsReq,
						@KitQtyShipped=numQtyShipped,@KiyQtyItemsReq_Orig=numQtyItemsReq_Orig,
						@KitonAllocation=numAllocation FROM #tempKits WHERE RowNumber=@minRowNumber
			
				SET @KitNewQtyShipped = (@numNewQtyShipped * @KiyQtyItemsReq_Orig) - @KitQtyShipped;
			
				DECLARE @Kitdescription AS VARCHAR(100)
				SET @Kitdescription=CONCAT('Transfer Order KIT Qty Shipped (Qty:',@KiyQtyItemsReq,' Shipped:',@KitNewQtyShipped,')')

				SET @KitonAllocation = @KitonAllocation - @KitNewQtyShipped
        
				IF (@KitonAllocation >= 0 )
				BEGIN
					UPDATE  WareHouseItems
					SET     numAllocation = @KitonAllocation,dtModified = GETDATE() 
					WHERE   numWareHouseItemID = @KitWareHouseItemID      	
						
					UPDATE  [OpportunityKitItems]
					SET     [numQtyShipped] = @numQtyShipped * @KiyQtyItemsReq_Orig 
					WHERE   [numOppChildItemID] = @KitOppChildItemID
			                
					EXEC dbo.USP_ManageWareHouseItems_Tracking
							@numWareHouseItemID = @KitWareHouseItemID, --  numeric(9, 0)
							@numReferenceID = @numOppId, --  numeric(9, 0)
							@tintRefType = 3, --  tinyint
							@vcDescription = @Kitdescription, --  varchar(100)
							@numModifiedBy = @numUserCntID,
							@numDomainID = @numDomain 
				END
            
				SET @minRowNumber=@minRowNumber + 1			 		
			END				
			   
			UPDATE
				[OpportunityItems]
			SET 
				[numQtyShipped] = @numQtyShipped
			WHERE
				[numoppitemtCode] = @numOppItemID
	                
			EXEC dbo.USP_ManageWareHouseItems_Tracking
					@numWareHouseItemID = @numWareHouseItemID, --  numeric(9, 0)
					@numReferenceID = @numOppId, --  numeric(9, 0)
					@tintRefType = 3, --  tinyint
					@vcDescription = @description, --  varchar(100)
					@numModifiedBy = @numUserCntID,
					@numDomainID = @numDomain 
		END			
	END
	ELSE
	BEGIN
        SELECT  
            @onAllocation = ISNULL(numAllocation, 0)
        FROM 
			WareHouseItems
        WHERE 
			numWareHouseItemID = @numWareHouseItemID
        
        
		SET @onAllocation = @onAllocation - @numNewQtyShipped
        
        IF (@onAllocation >= 0 )
        BEGIN
            UPDATE 
				WareHouseItems
            SET 
				numAllocation = @onAllocation
				,dtModified = GETDATE() 
            WHERE
				numWareHouseItemID = @numWareHouseItemID      	
			
			
            UPDATE 
				[OpportunityItems]
            SET 
				[numQtyShipped] = @numQtyShipped
            WHERE 
				[numoppitemtCode] = @numOppItemID
                
			EXEC dbo.USP_ManageWareHouseItems_Tracking
				@numWareHouseItemID = @numWareHouseItemID, --  numeric(9, 0)
				@numReferenceID = @numOppId, --  numeric(9, 0)
				@tintRefType = 3, --  tinyint
				@vcDescription = @description, --  varchar(100)
				@numModifiedBy = @numUserCntID,
				@numDomainID = @numDomain 
        END
        ELSE
        BEGIN
			SELECT TOP 1 @vcOppName=ISNULL([vcPOppName],'') FROM [OpportunityMaster] WHERE [numOppId] IN (SELECT [numOppId] FROM [OpportunityItems] WHERE [numoppitemtCode]=@numOppItemID)
			SET @vcError ='You do not have enough inventory to support this shipment('+ @vcOppName +'). Modify your Qty-Shipped to an amount equal to or less than the Qty-On-Allocation and try again.' 
		END
    END
END TRY
BEGIN CATCH
  -- Raise an error with the details of the exception
  DECLARE @ErrMsg nvarchar(4000), @ErrSeverity int
  SELECT @ErrMsg = ERROR_MESSAGE(),
         @ErrSeverity = ERROR_SEVERITY()

  RAISERROR(@ErrMsg, @ErrSeverity, 1)
END CATCH
END
GO
    