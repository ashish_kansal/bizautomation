GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetPromotionsForSimilarItems')
-- exec USP_GetPromotionsForSimilarItems 72,170564,0
DROP PROCEDURE USP_GetPromotionsForSimilarItems 
GO
CREATE PROCEDURE USP_GetPromotionsForSimilarItems 
	@numDomainID NUMERIC(18,0),
	@numItemCode NUMERIC(18,0),
	@numWarehouseItemID AS NUMERIC(18,0),
	@numSiteID AS NUMERIC(18,0)
AS 
BEGIN
	--SELECT  * FROM PromotionOfferItems POI 
	--JOIN Promotionoffer PO ON PO.numProId = POI.numProId 
	--WHERE numValue = @numItemCode
	DECLARE @numItemClassification AS NUMERIC(18,0), @numShippingCountry AS NUMERIC(18,0)

	-- GET ITEM DETAILS
	SELECT
		@numItemClassification = numItemClassification
	FROM
		Item
	OUTER APPLY
	(
		SELECT TOP 1 numWareHouseItemID,monWListPrice FROM WareHouseItems WHERE numItemID=@numItemCode AND (numWareHouseItemID = @numWarehouseItemID OR ISNULL(@numWarehouseItemID,0) = 0)
	) AS WareHouseItems
	WHERE
		Item.numItemCode = @numItemCode

	SELECT @numShippingCountry = numFreeShippingCountry FROM ShippingPromotions WHERE numDomainId = @numDomainID 

	-- CHECK IF PROMOTION CAN BE TRIGGERED FROM THIS ITEM
	DECLARE @numPromotionID AS NUMERIC(18,0)

	SELECT TOP 1
		@numPromotionID = numProId 
	FROM 
		PromotionOffer PO
		left JOIN PromotionOfferSites POS ON PO.numProId=POS.numPromotionID
	WHERE 
		numDomainId=@numDomainID 
		AND POS.numSiteID = @numSiteID 
		AND ISNULL(bitEnabled,0)=1
		--AND ISNULL(bitApplyToInternalOrders,0)=1 
		AND 1 = (CASE WHEN bitRequireCouponCode = 1 THEN CASE WHEN ISNULL(intCouponCodeUsed,0) < ISNULL(tintUsageLimit,0) OR ISNULL(tintUsageLimit,0) = 0  THEN 1 ELSE 0 END ELSE 1 END)
		AND 1 = (CASE WHEN bitNeverExpires = 1 THEN 1 ELSE (CASE WHEN CAST(GETDATE() AS DATE)>=dtValidFrom AND CAST(GETDATE() AS DATE)<=dtValidTo THEN 1 ELSE 0 END) END)
		AND 1 = (CASE 
					WHEN tintOfferBasedOn = 1 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId=PO.numProId AND tintRecordType=5 AND tintType=1 AND numValue=@numItemCode) > 0 THEN 1 ELSE 0 END)
					WHEN tintOfferBasedOn = 2 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId=PO.numProId AND tintRecordType=5 AND tintType=2 AND numValue=@numItemClassification) > 0 THEN 1 ELSE 0 END)
					ELSE 0
				END)
	ORDER BY
		CASE 
			--WHEN ISNULL(PO.bitRequireCouponCode,0)=1 AND PO.txtCouponCode=@vcCoupon THEN 1			
			WHEN PO.tintCustomersBasedOn = 1 AND PO.tintOfferBasedOn = 2 THEN 2
			WHEN PO.tintCustomersBasedOn = 1 AND PO.tintOfferBasedOn = 4 THEN 3
			WHEN PO.tintCustomersBasedOn = 2 AND PO.tintOfferBasedOn = 1 THEN 4
			WHEN PO.tintCustomersBasedOn = 2 AND PO.tintOfferBasedOn = 2 THEN 5
			WHEN PO.tintCustomersBasedOn = 2 AND PO.tintOfferBasedOn = 4 THEN 6
			WHEN PO.tintCustomersBasedOn = 3 AND PO.tintOfferBasedOn = 1 THEN 7
			WHEN PO.tintCustomersBasedOn = 3 AND PO.tintOfferBasedOn = 2 THEN 8
			WHEN PO.tintCustomersBasedOn = 3 AND PO.tintOfferBasedOn = 4 THEN 9
		END


	IF ISNULL(@numPromotionID,0) > 0
	BEGIN
		-- Get Top 1 Promotion Based on priority

		SELECT 
			numProId
			,vcProName
			,bitNeverExpires
			,bitRequireCouponCode
			,txtCouponCode
			,tintUsageLimit
			,intCouponCodeUsed
			,SP.bitFreeShiping
			,SP.monFreeShippingOrderAmount
			,SP.numFreeShippingCountry
			,SP.bitFixShipping1
			,SP.monFixShipping1OrderAmount
			,SP.monFixShipping1Charge
			,SP.bitFixShipping2
			,SP.monFixShipping2OrderAmount
			,SP.monFixShipping2Charge
			,tintOfferTriggerValueType
			,fltOfferTriggerValue
			,tintOfferBasedOn
			,fltDiscountValue
			,tintDiscountType
			,tintDiscoutBaseOn
			,dbo.FormatedDateFromDate(dtValidTo,@numDomainID) AS dtExpire
			,(CASE tintOfferBasedOn
				WHEN 1 THEN (SELECT Stuff((SELECT CONCAT(',',numValue) FROM PromotionOfferItems WHERE numProId=PO.numProId AND tintRecordType=5 AND tintType=1 FOR XML PATH('')), 1, 1, ''))
				WHEN 2 THEN (SELECT Stuff((SELECT CONCAT(',',numValue) FROM PromotionOfferItems WHERE numProId=PO.numProId AND tintRecordType=5 AND tintType=2 FOR XML PATH('')), 1, 1, ''))
			END) As vcPromotionItems
			,(CASE tintDiscoutBaseOn
				WHEN 1 THEN (SELECT Stuff((SELECT CONCAT(',',numValue) FROM PromotionOfferItems WHERE numProId=PO.numProId AND tintRecordType=6 AND tintType=1 FOR XML PATH('')), 1, 1, ''))
				WHEN 2 THEN (SELECT Stuff((SELECT CONCAT(',',numValue) FROM PromotionOfferItems WHERE numProId=PO.numProId AND tintRecordType=6 AND tintType=2 FOR XML PATH('')), 1, 1, ''))
				WHEN 3 THEN (SELECT Stuff((SELECT CONCAT(',',SimilarItems.numItemCode) FROM SimilarItems WHERE 1 = (CASE 
																														WHEN tintOfferBasedOn=1 THEN CASE WHEN numParentItemCode IN (SELECT numValue FROM PromotionOfferItems WHERE numProId=PO.numProId AND tintRecordType=5 AND tintType=1) THEN 1 ELSE 0 END
																														WHEN tintOfferBasedOn=2 THEN CASE WHEN numParentItemCode IN (SELECT numItemCode FROM Item WHERE numDomainID=@numDomainID AND numItemClassification IN (SELECT numValue FROM PromotionOfferItems WHERE numProId=PO.numProId AND tintRecordType=5 AND tintType=2)) THEN 1 ELSE 0 END
																														ELSE 0
																													END) FOR XML PATH('')), 1, 1, ''))
			END) As vcItems
			,CONCAT('Buy '
					,CASE WHEN tintOfferTriggerValueType=1 THEN CAST(fltOfferTriggerValue AS VARCHAR) ELSE CONCAT('$',fltOfferTriggerValue) END
					,CASE tintOfferBasedOn
							WHEN 1 THEN CONCAT(' of "',(SELECT Stuff((SELECT CONCAT(', ',Item.vcItemName) FROM PromotionOfferItems INNER JOIN Item ON PromotionOfferItems.numValue = Item.numItemCode WHERE numProId=PO.numProId AND tintRecordType=5 AND tintType=1 FOR XML PATH('')), 1, 2, '')), '"')
							WHEN 2 THEN CONCAT(' of items belonging to classification(s) "',(SELECT Stuff((SELECT CONCAT(', ',ListDetails.vcData) FROM PromotionOfferItems INNER JOIN ListDetails ON PromotionOfferItems.numValue = ListDetails.numListItemID WHERE numListID=36 AND numProId=PO.numProId AND tintRecordType=5 AND tintType=2 FOR XML PATH('')), 1, 2, '')), '"')
						END
					,' & get '
					, CASE tintDiscountType WHEN 1 THEN CONCAT(fltDiscountValue,'% off on') WHEN 2 THEN CONCAT('$',fltDiscountValue, ' off on') WHEN 3 THEN CONCAT(fltDiscountValue,' of') END
					, CASE 
						WHEN tintDiscoutBaseOn = 1 THEN 
							CONCAT
							(
								CONCAT(' "',(SELECT Stuff((SELECT CONCAT(',',Item.vcItemName) FROM PromotionOfferItems INNER JOIN Item ON PromotionOfferItems.numValue = Item.numItemCode WHERE numProId=PO.numProId AND tintRecordType=6 AND tintType=1 FOR XML PATH('')), 1, 1, '')), '"')
								,(CASE WHEN PO.tintDiscountType = 3 THEN ' free' ELSE '' END)
								,'.'
							)
						WHEN tintDiscoutBaseOn = 2 THEN 
							CONCAT
							(
								CONCAT(' items belonging to classification(s) "',(SELECT Stuff((SELECT CONCAT(',',ListDetails.vcData) FROM PromotionOfferItems INNER JOIN ListDetails ON PromotionOfferItems.numValue = ListDetails.numListItemID WHERE numListID=36 AND numProId=PO.numProId AND tintRecordType=6 AND tintType=2 FOR XML PATH('')), 1, 1, '')), '"')
								,(CASE WHEN PO.tintDiscountType = 3 THEN ' free' ELSE '' END)
								,'.'
							)
						WHEN tintDiscoutBaseOn = 3 THEN ' related items.'
						ELSE '' 
					END
				) AS vcPromotionDescription
				,CONCAT
				(
					(CASE WHEN ISNULL(SP.bitFixShipping1,0)=1 THEN CONCAT('Spend $',SP.monFixShipping1OrderAmount,' and get $',SP.monFixShipping1Charge,' shipping. ') ELSE '' END)
					,(CASE WHEN ISNULL(SP.bitFixShipping2,0)=1  THEN CONCAT('Spend $',SP.monFixShipping2OrderAmount,' and get $',SP.monFixShipping2Charge,' shipping. ') ELSE '' END)
					,(CASE WHEN (SP.bitFreeShiping=1 AND SP.numFreeShippingCountry=@numShippingCountry) THEN CONCAT('Spend $',SP.monFreeShippingOrderAmount,' and shipping is FREE! ') ELSE '' END) 
				) AS vcShippingDescription,
				CASE 
					--WHEN ISNULL(PO.bitRequireCouponCode,0)=1 AND PO.txtCouponCode=@vcCoupon THEN 1
					WHEN PO.tintCustomersBasedOn = 1 AND PO.tintOfferBasedOn = 1 THEN 1
					WHEN PO.tintCustomersBasedOn = 1 AND PO.tintOfferBasedOn = 2 THEN 2
					WHEN PO.tintCustomersBasedOn = 1 AND PO.tintOfferBasedOn = 4 THEN 3
					WHEN PO.tintCustomersBasedOn = 2 AND PO.tintOfferBasedOn = 1 THEN 4
					WHEN PO.tintCustomersBasedOn = 2 AND PO.tintOfferBasedOn = 2 THEN 5
					WHEN PO.tintCustomersBasedOn = 2 AND PO.tintOfferBasedOn = 4 THEN 6
					WHEN PO.tintCustomersBasedOn = 3 AND PO.tintOfferBasedOn = 1 THEN 7
					WHEN PO.tintCustomersBasedOn = 3 AND PO.tintOfferBasedOn = 2 THEN 8
					WHEN PO.tintCustomersBasedOn = 3 AND PO.tintOfferBasedOn = 4 THEN 9
				END AS tintPriority
		FROM
			PromotionOffer PO
			left JOIN PromotionOfferSites POS ON PO.numProId=POS.numPromotionID
		OUTER APPLY
		(
			SELECT 
				 bitFreeShiping
				,monFreeShippingOrderAmount
				,numFreeShippingCountry
				,bitFixShipping1
				,monFixShipping1OrderAmount
				,monFixShipping1Charge
				,bitFixShipping2
				,monFixShipping2OrderAmount
				,monFixShipping2Charge
			FROM
				ShippingPromotions 
			WHERE
				ShippingPromotions.numDomainId = @numDomainID 
		) AS SP
		WHERE
			numDomainId=@numDomainID 
			AND numProId = @numPromotionID
			AND POS.numSiteID = @numSiteID 
	END
END
