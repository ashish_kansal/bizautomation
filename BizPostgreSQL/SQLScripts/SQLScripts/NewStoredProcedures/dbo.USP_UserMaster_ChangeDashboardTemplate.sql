SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_UserMaster_ChangeDashboardTemplate')
DROP PROCEDURE USP_UserMaster_ChangeDashboardTemplate
GO
CREATE PROCEDURE [dbo].[USP_UserMaster_ChangeDashboardTemplate]
	@numDomainID NUMERIC(18,0)
	,@numUserID AS NUMERIC(18,0)
	,@numTemplateID AS NUMERIC(18,0)
	,@vcDashboardTemplateIDs AS VARCHAR(250) = ''
AS
BEGIN 

IF @vcDashboardTemplateIDs IS NULL OR @vcDashboardTemplateIDs = ''
	BEGIN
		UPDATE 
			UserMaster
		SET
			numDashboardTemplateID=@numTemplateID
		WHERE
			numUserId=@numUserID
	END
ELSE
	BEGIN
		UPDATE 
			UserMaster
		SET
			numDashboardTemplateID=@numTemplateID,
			vcDashboardTemplateIDs=@vcDashboardTemplateIDs
		WHERE
			numUserId=@numUserID
	END
	
	IF ISNULL(@numTemplateID,0) > 0 and (@vcDashboardTemplateIDs IS NOT NULL OR @vcDashboardTemplateIDs <> '')
	BEGIN
		-- DELETE OLD DASHBOARD CONFIGURATION FOR USER BECAUSE IT MAY BE CHANGED FROM LAST ASSIGNMENT 
		DELETE FROM ReportDashboard WHERE numDomainID=@numDomainID AND numDashboardTemplateID=@numTemplateID AND numUserCntID=ISNULL((SELECT numUserDetailId FROM UserMaster WHERE numUserId=@numUserID),0)

		-- INSERT LATEST CONFIGURATION OF DASHBOARD TEMPLATE
		INSERT INTO [dbo].[ReportDashboard]
		(
			numDomainID
			,numReportID
			,numUserCntID
			,tintReportType
			,tintChartType
			,vcHeaderText
			,vcFooterText
			,tintRow
			,tintColumn
			,vcXAxis
			,vcYAxis
			,vcAggType
			,tintReportCategory
			,intHeight
			,intWidth
			,numDashboardTemplateID
		)
		SELECT
			DashboardTemplateReports.numDomainID
			,DashboardTemplateReports.numReportID
			,ISNULL((SELECT numUserDetailId FROM UserMaster WHERE numUserId=@numUserID),0)
			,DashboardTemplateReports.tintReportType
			,tintChartType
			,vcHeaderText
			,vcFooterText
			,tintRow
			,tintColumn
			,vcXAxis
			,vcYAxis
			,vcAggType
			,tintReportCategory
			,intHeight
			,intWidth
			,numDashboardTemplateID
		FROM
			DashboardTemplateReports
		INNER JOIN
			ReportListMaster
		ON
			DashboardTemplateReports.numReportID=ReportListMaster.numReportID
			AND (ReportListMaster.numDomainID=@numDomainID OR bitDefault=1)
		WHERE
			DashboardTemplateReports.numDomainID=@numDOmainID
			AND numDashboardTemplateID=@numTemplateID
	END
END
GO