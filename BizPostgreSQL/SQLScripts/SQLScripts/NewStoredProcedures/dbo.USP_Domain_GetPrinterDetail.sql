SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_Domain_GetPrinterDetail')
DROP PROCEDURE USP_Domain_GetPrinterDetail
GO
CREATE PROCEDURE [dbo].[USP_Domain_GetPrinterDetail]
(                        
	@numDomainID AS NUMERIC(18,0)
)                        
AS 
BEGIN
	SELECT
		ISNULL(vcPrinterIPAddress,'') vcPrinterIPAddress,
		ISNULL(vcPrinterPort,'') vcPrinterPort
	FROM
		Domain
	WHERE
		numDomainId=@numDomainID
END