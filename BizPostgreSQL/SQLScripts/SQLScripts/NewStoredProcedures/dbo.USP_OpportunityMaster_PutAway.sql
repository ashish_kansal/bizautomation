GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO

IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_OpportunityMaster_PutAway')
DROP PROCEDURE USP_OpportunityMaster_PutAway
GO
CREATE PROCEDURE [dbo].[USP_OpportunityMaster_PutAway]
(
	@numDomainID NUMERIC(18,0)
	,@numUserCntID NUMERIC(18,0)
	,@numOppID NUMERIC(18,0)
	,@numOppItemID NUMERIC(18,0)
	,@numQtyReceived FLOAT
	,@dtItemReceivedDate DATETIME
	,@vcWarehouses VARCHAR(MAX)
	,@numVendorInvoiceBizDocID NUMERIC(18,0)
	,@tintMode TINYINT
)
AS
BEGIN
	IF ISNULL(@numQtyReceived,0) <= 0 
	BEGIN
		RAISERROR('QTY_TO_RECEIVE_NOT_PROVIDED',16,1)
		RETURN
	END 

	IF LEN(ISNULL(@vcWarehouses,'')) = 0
	BEGIN
		RAISERROR('LOCATION_NOT_SELECTED',16,1)
		RETURN
	END

	DECLARE @TEMPWarehouseLot TABLE
	(
		ID INT
		,numWareHouseItmsDTLID NUMERIC(18,0)
		,vcSerialNo VARCHAR(300)
		,numQty FLOAT
	)

	DECLARE @TEMPSerialLot TABLE
	(
		ID INT
		,vcSerialNo VARCHAR(300)
		,numQty FLOAT
	)

	DECLARE @TEMPWarehouseLocations TABLE
	(
		ID INT IDENTITY(1,1)
		,numWarehouseLocationID NUMERIC(18,0)
		,numWarehouseItemID NUMERIC(18,0)
		,numReceivedQty FLOAT
		,vcSerialLotNo VARCHAR(MAX)
	)

	DECLARE @hDocItem int
	EXEC sp_xml_preparedocument @hDocItem OUTPUT, @vcWarehouses

	INSERT INTO @TEMPWarehouseLocations
	(
		numWarehouseLocationID
		,numWarehouseItemID
		,numReceivedQty
		,vcSerialLotNo
	)
	SELECT 
		(CASE WHEN ISNULL(bitNewLocation,0) = 1 THEN ID ELSE 0 END)
		,(CASE WHEN ISNULL(bitNewLocation,0) = 1 THEN 0 ELSE ID END)
		,ISNULL(Qty,0)
		,ISNULL(SerialLotNo,'') 
	FROM 
		OPENXML (@hDocItem, '/NewDataSet/Table1',2)
	WITH 
	(
		bitNewLocation BIT, 
		ID NUMERIC(18,0),
		Qty FLOAT,
		SerialLotNo VARCHAR(MAX)
	)

	EXEC sp_xml_removedocument @hDocItem

	IF @numQtyReceived <> ISNULL((SELECT SUM(numReceivedQty) FROM @TEMPWarehouseLocations),0)
	BEGIN
		RAISERROR('INALID_QTY_RECEIVED',16,1)
		RETURN
	END

	IF ISNULL(@numVendorInvoiceBizDocID,0) > 0
	BEGIN
		-- IF RECEIVED AGAINST VENDOR INVOICE MAKE SURE ENOUGH QTY IS PENDING TO RECIEVED AGAINST VEDOR INVOICE BECAUSE USER CAN RECEIVE QTY WITHOUT SELECTING VENDOR INVOCIE ALSO
		IF (SELECT
				COUNT(*)
			FROM
				OpportunityBizDocItems OBDI
			INNER JOIN
				OpportunityItems OI
			ON
				OBDI.numOppItemID=OI.numoppitemtCode
			WHERE
				numOppBizDocID=@numVendorInvoiceBizDocID
				AND OBDI.numOppItemID = @numOppItemID
				AND @numQtyReceived > (ISNULL(OI.numUnitHour,0) - ISNULL(OI.numUnitHourReceived,0))
			) > 0
		BEGIN
			RAISERROR('INVALID_VENDOR_INVOICE_RECEIVE_QTY',16,1)
			RETURN
		END
	END

	DECLARE @description AS VARCHAR(100)
	DECLARE @numWarehouseItemID AS NUMERIC(18,0)
	DECLARE @numFromWarehouseID AS NUMERIC(18,0)
	DECLARE @numFromWarehouseItemID NUMERIC(18,0)
	DECLARE @numWLocationID AS NUMERIC(18,0)     
	DECLARE @numOldQtyReceived AS FLOAT
	DECLARE @bitStockTransfer BIT
	DECLARE @numToWarehouseItemID NUMERIC
	DECLARE @numUnits FLOAT
	DECLARE @monPrice AS DECIMAL(20,5) 
	DECLARE @numItemCode NUMERIC(18,0)
	DECLARE @monListPrice DECIMAL(20,5)
	DECLARE @numWarehouseID NUMERIC(18,0)
	DECLARE @bitSerial BIT
	DECLARE @bitLot BIT
		
	SELECT  
		@numWarehouseItemID = ISNULL([numWarehouseItmsID], 0),
		@numFromWarehouseID=ISNULL(WI.numWareHouseID,0),
		@numFromWarehouseItemID = ISNULL([numWarehouseItmsID], 0),
		@numWLocationID = ISNULL(WI.numWLocationID,0),
		@numOldQtyReceived = ISNULL([numUnitHourReceived], 0),
		@bitStockTransfer = ISNULL(OM.bitStockTransfer,0),
		@numToWarehouseItemID= ISNULL(numToWarehouseItemID,0),
		@numOppId=OM.numOppId,
		@numUnits=OI.numUnitHour,
		@monPrice=isnull(monPrice,0) * (CASE WHEN ISNULL(OM.fltExchangeRate,0)=0 THEN 1 ELSE OM.fltExchangeRate END),
		@numItemCode=OI.numItemCode,
		@monListPrice=ISNULL(I.monListPrice,0),
		@numWarehouseID=WI.numWareHouseID,
		@bitSerial=ISNULL(I.bitSerialized,0),
		@bitLot=ISNULL(I.bitLotNo,0)
	FROM 
		[OpportunityItems] OI 
	INNER JOIN 
		dbo.OpportunityMaster OM
	ON 
		OI.numOppId = OM.numOppId
	INNER JOIN
		Item I
	ON
		OI.numItemCode = I.numItemCode
	LEFT JOIN
		WareHouseItems WI
	ON
		OI.numWarehouseItmsID = WI.numWareHouseItemID
	WHERE 
		[numoppitemtCode] = @numOppItemID

	IF (@numQtyReceived + @numOldQtyReceived) > @numUnits
	BEGIN
		RAISERROR('RECEIVED_QTY_MUST_BE_LESS_THEN_OR_EQUAL_TO_ORDERED_QTY',16,1)
		RETURN
	END

	IF @bitStockTransfer = 1 --added by chintan
	BEGIN
		DECLARE @p3 VARCHAR(MAX) = ''
		
		EXEC USP_UpdateQtyShipped @numQtyReceived,@numOppItemID,@numUserCntID,@vcError=@p3 OUTPUT

		IF LEN(@p3)>0 
		BEGIN
			RAISERROR ( @p3,16, 1 )
			RETURN ;
		END    

		SET @numWarehouseItemID = @numToWarehouseItemID
	END
	ELSE
	BEGIN
		--Updating the Average Cost
		DECLARE @TotalOnHand AS FLOAT = 0
		DECLARE @monAvgCost AS DECIMAL(20,5) 

		SELECT 
			@TotalOnHand=(SUM(ISNULL(numOnHand,0)) + SUM(ISNULL(numAllocation,0))) 
		FROM 
			dbo.WareHouseItems 
		WHERE 
			numItemID=@numItemCode 
						
		SELECT 
			@monAvgCost = (CASE WHEN ISNULL(bitVirtualInventory,0) = 1 THEN 0 ELSE ISNULL(monAverageCost, 0) END) 
		FROM 
			Item 
		WHERE 
			numitemcode = @numItemCode  
    
  		SET @monAvgCost = ((@TotalOnHand * @monAvgCost) + (@numQtyReceived * @monPrice)) / ( @TotalOnHand + @numQtyReceived )
                            
		UPDATE  
			Item
		SET 
			monAverageCost = (CASE WHEN ISNULL(bitVirtualInventory,0) = 1 THEN 0 ELSE @monAvgCost END)
		WHERE 
			numItemCode = @numItemCode
    END	

	UPDATE
		WareHouseItems
	SET 
		numOnOrder = ISNULL(numOnOrder,0) - @numQtyReceived,
		dtModified = GETDATE()
	WHERE
		numWareHouseItemID=@numWareHouseItemID
		
	SET @description = CONCAT('PO Qty Received (Qty:',@numQtyReceived,')')

	EXEC dbo.USP_ManageWareHouseItems_Tracking
		@numWareHouseItemID = @numWareHouseItemID, --  numeric(9, 0)
		@numReferenceID = @numOppId, --  numeric(9, 0)
		@tintRefType = 4, --  tinyint
		@vcDescription = @description,
		@numModifiedBy = @numUserCntID,
		@dtRecordDate =  @dtItemReceivedDate,
		@numDomainID = @numDomainID


	DECLARE @i INT
	DECLARE @j INT
	DECLARE @k INT
	DECLARE @iCount INT = 0
	DECLARE @jCount INT = 0
	DECLARE @kCount INT = 0
	DECLARE @vcSerialLot# VARCHAR(MAX)
	DECLARE @numWareHouseItmsDTLID NUMERIC(18,0)
	DECLARE @vcSerailLotNumber VARCHAR(300)
	DECLARE @numSerialLotQty FLOAT
	DECLARE @numTempWLocationID NUMERIC(18,0)
	DECLARE @numTempWarehouseItemID NUMERIC(18,0)
	DECLARE @numTempReceivedQty FLOAT
	DECLARE @numLotWareHouseItmsDTLID NUMERIC(18,0)
	DECLARE @numLotQty FLOAT
	
	SET @i = 1
	SET @iCount = (SELECT COUNT(*) FROM @TEMPWarehouseLocations)

	WHILE @i <= @iCount
	BEGIN
		SELECT 
			@numTempWLocationID=ISNULL(numWarehouseLocationID,0)
			,@numTempWarehouseItemID=ISNULL(numWarehouseItemID,0)
			,@numTempReceivedQty=ISNULL(numReceivedQty,0)
			,@vcSerialLot# = ISNULL(vcSerialLotNo,'')
		FROM 
			@TEMPWarehouseLocations 
		WHERE 
			ID = @i

		IF (ISNULL(@bitLot,0)=1 OR ISNULL(@bitSerial,0)=1)
		BEGIN
			IF LEN(ISNULL(@vcSerialLot#,'')) = 0
			BEGIN
				RAISERROR('SERIALLOT_REQUIRED',16,1)
				RETURN
			END 
			ELSE
			BEGIN	
				DELETE FROM @TEMPSerialLot

				IF ISNULL(@bitSerial,0) = 1
				BEGIN
					INSERT INTO @TEMPSerialLot
					(
						ID
						,vcSerialNo
						,numQty
					)
					SELECT
						ROW_NUMBER() OVER(ORDER BY OutParam)
						,RTRIM(LTRIM(OutParam))
						,1
					FROM
						dbo.SplitString(@vcSerialLot#,',')
				END
				ELSE IF @bitLot = 1
				BEGIN
					INSERT INTO @TEMPSerialLot
					(
						ID
						,vcSerialNo
						,numQty
					)
					SELECT
						ROW_NUMBER() OVER(ORDER BY OutParam)
						,(CASE 
							WHEN PATINDEX('%(%', OutParam) > 0 AND PATINDEX('%)%', OutParam) > 0 AND PATINDEX('%)%',OutParam) > (PATINDEX('%(%',OutParam) + 1)
							THEN RTRIM(LTRIM(SUBSTRING(OutParam,0,PATINDEX('%(%', OutParam))))
							ELSE ''
						END)
						,(CASE 
							WHEN PATINDEX('%(%', OutParam) > 0 AND PATINDEX('%)%', OutParam) > 0 AND PATINDEX('%)%',OutParam) > (PATINDEX('%(%',OutParam) + 1)
							THEN (CASE WHEN ISNUMERIC(SUBSTRING(OutParam,(PATINDEX('%(%', OutParam) + 1),(PATINDEX('%)%', OutParam) - PATINDEX('%(%', OutParam) - 1))) = 1 
									THEN CAST(SUBSTRING(OutParam,(PATINDEX('%(%', OutParam) + 1),(PATINDEX('%)%', OutParam) - PATINDEX('%(%', OutParam) - 1)) AS FLOAT)  
									ELSE -1 
									END) 
							ELSE -1
						END)	
	
					FROM
						dbo.SplitString(@vcSerialLot#,',')
				END

				IF EXISTS (SELECT vcSerialNo FROM @TEMPSerialLot WHERE numQty = -1)
				BEGIN
					RAISERROR('INVALID_SERIAL_NO',16,1)
					RETURN
				END
				ELSE IF @numTempReceivedQty <> ISNULL((SELECT SUM(numQty) FROM @TEMPSerialLot),0)
				BEGIN
					RAISERROR('INALID_SERIALLOT',16,1)
					RETURN
				END
			END
		END

		IF ISNULL(@numTempWarehouseItemID,0) = 0
		BEGIN
			IF EXISTS (SELECT numWLocationID FROM WarehouseLocation WHERE numDomainID=@numDomainID AND numWLocationID=ISNULL(@numTempWLocationID,0))
			BEGIN
				IF EXISTS (SELECT numWareHouseItemID FROM WareHouseItems WHERE numDomainID=@numDomainID AND numItemID=@numItemCode AND numWareHouseID=@numWarehouseID AND numWLocationID=@numTempWLocationID)
				BEGIN
					SET @numTempWarehouseItemID = (SELECT TOP 1 numWareHouseItemID FROM WareHouseItems WHERE numDomainID=@numDomainID AND numItemID=@numItemCode AND numWareHouseID=@numWarehouseID AND numWLocationID=@numTempWLocationID)
				END
				ELSE 
				BEGIN
					INSERT INTO WareHouseItems 
					(
						numItemID, 
						numWareHouseID,
						numWLocationID,
						numOnHand,
						numAllocation,
						numOnOrder,
						numBackOrder,
						numReorder,
						monWListPrice,
						vcLocation,
						vcWHSKU,
						vcBarCode,
						numDomainID,
						dtModified
					)  
					VALUES
					(
						@numItemCode,
						@numWareHouseID,
						@numTempWLocationID,
						0,
						0,
						0,
						0,
						0,
						@monListPrice,
						ISNULL((SELECT vcLocation FROM WarehouseLocation WHERE numDomainID=@numDomainID AND numWLocationID=ISNULL(@numTempWLocationID,0)),''),
						'',
						'',
						@numDomainID,
						GETDATE()
					)  

					SELECT @numTempWarehouseItemID = SCOPE_IDENTITY()

					DECLARE @dtDATE AS DATETIME = GETUTCDATE()
					DECLARE @vcDescription VARCHAR(100)='INSERT WareHouse'

					EXEC dbo.USP_ManageWareHouseItems_Tracking
						@numWareHouseItemID = @numTempWarehouseItemID,
						@numReferenceID = @numItemCode,
						@tintRefType = 1,
						@vcDescription = @vcDescription,
						@numModifiedBy = @numUserCntID,
						@ClientTimeZoneOffset = 0,
						@dtRecordDate = @dtDATE,
						@numDomainID = @numDomainID 
				END
			END
			ELSE 
			BEGIN
				RAISERROR('INVALID_WAREHOUSE_LOCATION',16,1)
				RETURN
			END
		END

		IF NOT EXISTS (SELECT numWareHouseItemID FROM WareHouseItems WHERE numDomainID=@numDomainID AND numItemID=@numItemCode AND numWareHouseItemID=@numTempWarehouseItemID)
		BEGIN
			RAISERROR('INVALID_WAREHOUSE',16,1)
			RETURN
		END
		ELSE
		BEGIN
			-- INCREASE THE OnHand Of Destination Location
			UPDATE
				WareHouseItems
			SET
				numBackOrder = (CASE WHEN numBackOrder > @numTempReceivedQty THEN ISNULL(numBackOrder,0) - @numTempReceivedQty ELSE 0 END),         
				numAllocation = (CASE WHEN numBackOrder > @numTempReceivedQty THEN ISNULL(numAllocation,0) + @numTempReceivedQty ELSE ISNULL(numAllocation,0) + ISNULL(numBackOrder,0) END),
				numOnHand = (CASE WHEN numBackOrder > @numTempReceivedQty THEN numOnHand ELSE ISNULL(numOnHand,0) + (@numTempReceivedQty - ISNULL(numBackOrder,0)) END),
				dtModified = GETDATE()
			WHERE
				numWareHouseItemID=@numTempWarehouseItemID

			SET @description = CONCAT('PO Qty Put-Away (Qty:',@numTempReceivedQty,')')

			EXEC dbo.USP_ManageWareHouseItems_Tracking
				@numWareHouseItemID = @numTempWarehouseItemID, --  numeric(9, 0)
				@numReferenceID = @numOppId, --  numeric(9, 0)
				@tintRefType = 4, --  tinyint
				@vcDescription = @description,
				@numModifiedBy = @numUserCntID,
				@dtRecordDate =  @dtItemReceivedDate,
				@numDomainID = @numDomainID

			IF ISNULL(@numTempWarehouseItemID,0) > 0 AND ISNULL(@numTempWarehouseItemID,0) <> @numWareHouseItemID		
			BEGIN
				INSERT INTO OpportunityItemsReceievedLocation
				(
					numDomainID,
					numOppID,
					numOppItemID,
					numWarehouseItemID,
					numUnitReceieved
				)
				VALUES
				(
					@numWarehouseItemID,
					@numOppId,
					@numOppItemID,
					@numTempWarehouseItemID,
					@numTempReceivedQty
				)
			END

			IF (ISNULL(@bitLot,0)=1 OR ISNULL(@bitSerial,0)=1)
			BEGIN
				SET @j = 1
				SET @jCount = (SELECT COUNT(*) FROM @TEMPSerialLot)

				WHILE (@j <= @jCount AND @numTempReceivedQty > 0)
				BEGIN
					SELECT 
						@vcSerailLotNumber=ISNULL(vcSerialNo,'')
						,@numSerialLotQty=ISNULL(numQty,0)
					FROM 
						@TEMPSerialLot 
					WHERE 
						ID=@j

					IF @numSerialLotQty > 0
					BEGIN
						IF @bitStockTransfer = 1
						BEGIN
							IF (SELECT COUNT(*) FROM WareHouseItems INNER JOIN WareHouseItmsDTL ON WareHouseItems.numWareHouseItemID = WareHouseItmsDTL.numWareHouseItemID WHERE WareHouseItems.numItemID=@numItemCode AND WareHouseItems.numWareHouseID=@numFromWarehouseID AND LOWER(vcSerialNo)=LOWER(@vcSerailLotNumber)) = 0
							BEGIN
								RAISERROR('INVALID_SERIAL_NO',16,1)
								RETURN
							END
					
							IF @bitLot = 1
							BEGIN
								IF (SELECT SUM(WareHouseItmsDTL.numQty) FROM WareHouseItems INNER JOIN WareHouseItmsDTL ON WareHouseItems.numWareHouseItemID = WareHouseItmsDTL.numWareHouseItemID WHERE WareHouseItems.numItemID=@numItemCode AND WareHouseItems.numWareHouseID=@numFromWarehouseID AND LOWER(vcSerialNo)=LOWER(@vcSerailLotNumber)) < @numTempReceivedQty
								BEGIN
									RAISERROR('INVALID_LOT_QUANTITY',16,1)
									RETURN
								END

								DELETE FROM @TEMPWarehouseLot

								INSERT INTO @TEMPWarehouseLot
								(
									ID
									,numWareHouseItmsDTLID
									,numQty
								)
								SELECT
									ROW_NUMBER() OVER(ORDER BY WareHouseItmsDTL.numQty)
									,WareHouseItmsDTL.numWareHouseItmsDTLID
									,ISNULL(WareHouseItmsDTL.numQty,0)
								FROM
									WareHouseItems 
								INNER JOIN 
									WareHouseItmsDTL 
								ON 
									WareHouseItems.numWareHouseItemID = WareHouseItmsDTL.numWareHouseItemID 
								WHERE 
									WareHouseItems.numItemID=@numItemCode 
									AND WareHouseItems.numWareHouseID=@numFromWarehouseID 
									AND LOWER(vcSerialNo)=LOWER(@vcSerailLotNumber)

								SET @k = 1
								SELECT @kCount=COUNT(*) FROM @TEMPWarehouseLot

								WHILE @k <= @kCount AND @numTempReceivedQty > 0
								BEGIN
									SELECT
										@numLotWareHouseItmsDTLID=numWareHouseItmsDTLID
										,@numLotQty = numQty
									FROM
										@TEMPWarehouseLot
									WHERE
										ID=@k

									IF @numTempReceivedQty > @numLotQty
									BEGIN
										UPDATE
											WareHouseItmsDTL
										SET
											numWareHouseItemID = @numTempWarehouseItemID
										WHERE 
											numWareHouseItmsDTLID=@numLotWareHouseItmsDTLID

										UPDATE @TEMPSerialLot SET numQty = numQty-@numLotQty WHERE ID=@j
										SET @numTempReceivedQty = @numTempReceivedQty - @numLotQty
									END
									ELSE
									BEGIN
										UPDATE 
											WareHouseItmsDTL 
										SET 
											numQty = numQty - @numTempReceivedQty
										WHERE 
											numWareHouseItmsDTLID=@numLotWareHouseItmsDTLID

										INSERT INTO WareHouseItmsDTL
										(
											numWareHouseItemID,
											vcSerialNo,
											numQty,
											dExpirationDate,
											bitAddedFromPO
										)
										SELECT
											@numTempWarehouseItemID,
											vcSerialNo,
											@numTempReceivedQty,
											dExpirationDate,
											bitAddedFromPO
										FROM 
											WareHouseItmsDTL
										WHERE
											numWareHouseItmsDTLID=@numLotWareHouseItmsDTLID

										UPDATE @TEMPSerialLot SET numQty = numQty-@numTempReceivedQty WHERE ID=@j
										SET @numTempReceivedQty = 0
									END
									

									SET @k = @k + 1
								END
							END
							ELSE
							BEGIN
								IF NOT EXISTS (SELECT WareHouseItmsDTL.numWareHouseItmsDTLID FROM WareHouseItems INNER JOIN WareHouseItmsDTL ON WareHouseItems.numWareHouseItemID = WareHouseItmsDTL.numWareHouseItemID WHERE WareHouseItems.numItemID=@numItemCode AND WareHouseItems.numWareHouseID=@numFromWarehouseID AND LOWER(vcSerialNo)=LOWER(@vcSerailLotNumber) AND ISNULL(WareHouseItmsDTL.numQty,0) = 1)
								BEGIN
									RAISERROR('INVALID_SERIAL_QUANTITY',16,1)
									RETURN
								END

								UPDATE
									WHID
								SET
									WHID.numWareHouseItemID = @numTempWarehouseItemID
								FROM
									WareHouseItems WI
								INNER JOIN
									WareHouseItmsDTL WHID
								ON 
									WI.numWareHouseItemID = WHID.numWareHouseItemID 
								WHERE 
									WI.numItemID=@numItemCode 
									AND WI.numWareHouseID=@numFromWarehouseID 
									AND LOWER(WHID.vcSerialNo)=LOWER(@vcSerailLotNumber)

								UPDATE @TEMPSerialLot SET numQty = 0 WHERE ID=@j
								SET @numTempReceivedQty = @numTempReceivedQty - 1
							END
						END
						ELSE
						BEGIN
							IF @bitSerial = 1 AND (SELECT COUNT(*) FROM WareHouseItems INNER JOIN WareHouseItmsDTL ON WareHouseItems.numWareHouseItemID = WareHouseItmsDTL.numWareHouseItemID WHERE WareHouseItems.numItemID=@numItemCode AND LOWER(vcSerialNo)=LOWER(@vcSerailLotNumber)) > 0
							BEGIN
								RAISERROR('DUPLICATE_SERIAL_NO',16,1)
								RETURN
							END

							INSERT INTO WareHouseItmsDTL
							(
								numWareHouseItemID
								,vcSerialNo
								,numQty
								,bitAddedFromPO
							)  
							VALUES
							( 
								@numTempWarehouseItemID
								,@vcSerailLotNumber
								,(CASE WHEN @numTempReceivedQty >= @numSerialLotQty THEN @numSerialLotQty ELSE @numTempReceivedQty END)
								,1 
							)

		    
							SET @numWareHouseItmsDTLID=SCOPE_IDENTITY()
		
							INSERT INTO OppWarehouseSerializedItem
							(
								numWarehouseItmsDTLID
								,numOppId
								,numOppItemId
								,numWarehouseItmsID
								,numQty
							)  
							VALUES
							( 
								@numWareHouseItmsDTLID
								,@numOppID
								,@numOppItemID
								,@numTempWarehouseItemID
								,(CASE WHEN @numTempReceivedQty >= @numSerialLotQty THEN @numSerialLotQty ELSE @numTempReceivedQty END)
							)

							IF @numTempReceivedQty >= @numSerialLotQty
							BEGIN
								SET @numTempReceivedQty = @numTempReceivedQty - @numSerialLotQty
								UPDATE @TEMPSerialLot SET numQty = 0  WHERE ID = @j
							END
							ELSE
							BEGIN
								UPDATE @TEMPSerialLot SET numQty = numQty - @numTempReceivedQty  WHERE ID = @j
								SET @numTempReceivedQty = 0
							END
						END
					END

					SET @j = @j + 1
				END

				IF @numTempReceivedQty > 0
				BEGIN
					RAISERROR('INALID_SERIALLOT',16,1)
					RETURN
				END
			END
		END


		SET @i = @i + 1
	END

	UPDATE  
		[OpportunityItems]
	SET 
		numUnitHourReceived = ISNULL(numUnitHourReceived,0) + ISNULL(@numQtyReceived,0)
		,numQtyReceived = (CASE WHEN ISNULL(@tintMode,0)=1 THEN ISNULL(numQtyReceived,0) + ISNULL(@numQtyReceived,0) ELSE ISNULL(numQtyReceived,0) END)
	WHERE
		[numoppitemtCode] = @numOppItemID


	UPDATE I SET dtItemReceivedDate=@dtItemReceivedDate FROM OpportunityItems OI INNER JOIN Item I ON OI.numItemCode=I.numItemCode WHERE OI.numoppitemtCode=@numOppItemID


	IF ISNULL(@numVendorInvoiceBizDocID,0) > 0
	BEGIN
		UPDATE  
			OpportunityBizDocItems
		SET 
			numVendorInvoiceUnitReceived = ISNULL(numVendorInvoiceUnitReceived,0) +  ISNULL(@numQtyReceived,0)
		WHERE
			numOppBizDocID=@numVendorInvoiceBizDocID
			AND numOppItemID = @numOppItemID
	END

	UPDATE dbo.OpportunityMaster SET dtItemReceivedDate=@dtItemReceivedDate WHERE numOppId=@numOppId

END
GO