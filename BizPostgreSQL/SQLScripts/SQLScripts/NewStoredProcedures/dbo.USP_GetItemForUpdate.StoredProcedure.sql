 --SELECT * FROM  RecImportConfg WHERE ImportType=4 

 --SELECT * FROM  dbo.Item 
GO
 IF EXISTS ( SELECT *
             FROM   sysobjects
             WHERE  xtype = 'p'
                    AND NAME = 'USP_GetItemForUpdate' ) 
    DROP PROCEDURE USP_GetItemForUpdate
GO
-- exec USP_GetItemForUpdate 1 
 CREATE PROCEDURE USP_GetItemForUpdate
    (
      @numDomainID NUMERIC(9) = 0
    )
 AS 
    BEGIN
 
		CREATE TABLE  #TEMPIds
		(
			ID INT 
		)

		DECLARE @i INT= 1

		WHILE @i <= 20
		BEGIN
			INSERT INTO #TEMPIds (ID) VALUES (@i)

			SET @i = @i + 1
		END
   
------------Declaration---------------------  
        DECLARE @strSql VARCHAR(MAX),
            @numMaxFieldId NUMERIC(9),
            @from VARCHAR(MAX),
            @Where VARCHAR(MAX),
            @OrderBy VARCHAR(MAX),
            @GroupBy VARCHAR(MAX),
            @SelectFields VARCHAR(MAX),
            @InneJoinOn VARCHAR(MAX)
         
        DECLARE @tintOrder AS TINYINT,
            @vcFieldName AS VARCHAR(MAX),
            @vcListItemType AS VARCHAR(1),
            @vcListItemType1 AS VARCHAR(1),
            @vcAssociatedControlType VARCHAR(10),
            @numListID AS NUMERIC(9),
            @vcDbColumnName VARCHAR(30),
            @vcLookBackTableName VARCHAR(20),
            @WCondition VARCHAR(MAX),
            @bitCustom BIT,
            @fld_id NUMERIC(9)
        SET @InneJoinOn = ''
        SET @OrderBy = ' Order by I.numItemCode '
        SET @Where = ''
        SET @from = ' FROM item I OUTER APPLY (Select 
		Min(vcPriceLevel1Name) As vcPriceLevel1Name,
		Min(intPriceLevel1FromQty) As intPriceLevel1FromQty,
		Min(intPriceLevel1ToQty) As intPriceLevel1ToQty,
		Min(monPriceLevel1) As monPriceLevel1,
		Min(vcPriceLevel2Name) As vcPriceLevel2Name,
		Min(intPriceLevel2FromQty) As intPriceLevel2FromQty,
		Min(intPriceLevel2ToQty) As intPriceLevel2ToQty,
		Min(monPriceLevel2) As monPriceLevel2,
		Min(vcPriceLevel3Name) As vcPriceLevel3Name,
		Min(intPriceLevel3FromQty) As intPriceLevel3FromQty,
		Min(intPriceLevel3ToQty) As intPriceLevel3ToQty,
		Min(monPriceLevel3) As monPriceLevel3,
		Min(vcPriceLevel4Name) As vcPriceLevel4Name,
		Min(intPriceLevel4FromQty) As intPriceLevel4FromQty,
		Min(intPriceLevel4ToQty) As intPriceLevel4ToQty,
		Min(monPriceLevel4) As monPriceLevel4,
		Min(vcPriceLevel5Name) As vcPriceLevel5Name,
		Min(intPriceLevel5FromQty) As intPriceLevel5FromQty,
		Min(intPriceLevel5ToQty) As intPriceLevel5ToQty,
		Min(monPriceLevel5) As monPriceLevel5,
		Min(vcPriceLevel6Name) As vcPriceLevel6Name,
		Min(intPriceLevel6FromQty) As intPriceLevel6FromQty,
		Min(intPriceLevel6ToQty) As intPriceLevel6ToQty,
		Min(monPriceLevel6) As monPriceLevel6,
		Min(vcPriceLevel7Name) As vcPriceLevel7Name,
		Min(intPriceLevel7FromQty) As intPriceLevel7FromQty,
		Min(intPriceLevel7ToQty) As intPriceLevel7ToQty,
		Min(monPriceLevel7) As monPriceLevel7,
		Min(vcPriceLevel8Name) As vcPriceLevel8Name,
		Min(intPriceLevel8FromQty) As intPriceLevel8FromQty,
		Min(intPriceLevel8ToQty) As intPriceLevel8ToQty,
		Min(monPriceLevel8) As monPriceLevel8,
		Min(vcPriceLevel9Name) As vcPriceLevel9Name,
		Min(intPriceLevel9FromQty) As intPriceLevel9FromQty,
		Min(intPriceLevel9ToQty) As intPriceLevel9ToQty,
		Min(monPriceLevel9) As monPriceLevel9,
		Min(vcPriceLevel10Name) As vcPriceLevel10Name,
		Min(intPriceLevel10FromQty) As intPriceLevel10FromQty,
		Min(intPriceLevel10ToQty) As intPriceLevel10ToQty,
		Min(monPriceLevel10) As monPriceLevel10,
		Min(vcPriceLevel11Name) As vcPriceLevel11Name,
		Min(intPriceLevel11FromQty) As intPriceLevel11FromQty,
		Min(intPriceLevel11ToQty) As intPriceLevel11ToQty,
		Min(monPriceLevel11) As monPriceLevel11,
		Min(vcPriceLevel12Name) As vcPriceLevel12Name,
		Min(intPriceLevel12FromQty) As intPriceLevel12FromQty,
		Min(intPriceLevel12ToQty) As intPriceLevel12ToQty,
		Min(monPriceLevel12) As monPriceLevel12,
		Min(vcPriceLevel13Name) As vcPriceLevel13Name,
		Min(intPriceLevel13FromQty) As intPriceLevel13FromQty,
		Min(intPriceLevel13ToQty) As intPriceLevel13ToQty,
		Min(monPriceLevel13) As monPriceLevel13,
		Min(vcPriceLevel14Name) As vcPriceLevel14Name,
		Min(intPriceLevel14FromQty) As intPriceLevel14FromQty,
		Min(intPriceLevel14ToQty) As intPriceLevel14ToQty,
		Min(monPriceLevel14) As monPriceLevel14,
		Min(vcPriceLevel15Name) As vcPriceLevel15Name,
		Min(intPriceLevel15FromQty) As intPriceLevel15FromQty,
		Min(intPriceLevel15ToQty) As intPriceLevel15ToQty,
		Min(monPriceLevel15) As monPriceLevel15,
		Min(vcPriceLevel16Name) As vcPriceLevel16Name,
		Min(intPriceLevel16FromQty) As intPriceLevel16FromQty,
		Min(intPriceLevel16ToQty) As intPriceLevel16ToQty,
		Min(monPriceLevel16) As monPriceLevel16,
		Min(vcPriceLevel17Name) As vcPriceLevel17Name,
		Min(intPriceLevel17FromQty) As intPriceLevel17FromQty,
		Min(intPriceLevel17ToQty) As intPriceLevel17ToQty,
		Min(monPriceLevel17) As monPriceLevel17,
		Min(vcPriceLevel18Name) As vcPriceLevel18Name,
		Min(intPriceLevel18FromQty) As intPriceLevel18FromQty,
		Min(intPriceLevel18ToQty) As intPriceLevel18ToQty,
		Min(monPriceLevel18) As monPriceLevel18,
		Min(vcPriceLevel19Name) As vcPriceLevel19Name,
		Min(intPriceLevel19FromQty) As intPriceLevel19FromQty,
		Min(intPriceLevel19ToQty) As intPriceLevel19ToQty,
		Min(monPriceLevel19) As monPriceLevel19,
		Min(vcPriceLevel20Name) As vcPriceLevel20Name,
		Min(intPriceLevel20FromQty) As intPriceLevel20FromQty,
		Min(intPriceLevel20ToQty) As intPriceLevel20ToQty,
		Min(monPriceLevel20) As monPriceLevel20
	From 
	(Select 
		TEMP1.ID,
		''monPriceLevel'' + Cast(TEMP1.ID as varchar) As PriceLevel,
		''intPriceLevel'' + Cast(TEMP1.ID as varchar) + ''FromQty'' As FromQty,
		''intPriceLevel'' + Cast(TEMP1.ID as varchar) + ''ToQty'' As ToQty,
		''vcPriceLevel'' + Cast(TEMP1.ID as varchar) + ''Name'' As PriceLevelName,
		decDiscount,
		intFromQty,
		intToQty,
		vcName
	From (
		SELECT
			T1.ID
			,ISNULL(CAST(T2.intFromQty AS VARCHAR),'''') intFromQty
			,ISNULL(CAST(T2.intToQty AS VARCHAR),'''') intToQty
			,ISNULL(CAST(T2.decDiscount AS VARCHAR),'''') decDiscount
			,ISNULL(T2.vcName,'''') vcName
		FROM
			#TEMPIds T1
		LEFT JOIN
		(
			SELECT
				ROW_NUMBER() OVER(ORDER BY numPricingID) ID
				,intFromQty
				,intToQty
				,decDiscount
				,vcName
			FROM
				PricingTable
			WHERE
				numItemCode = I.numItemCode
				AND ISNULL(numCurrencyID,0) = 0
				AND ISNULL(numPriceRuleID,0) = 0
		) AS T2
		ON
			T1.ID = T2.ID
	) AS TEMP1) As Pvt
Pivot (Min(decDiscount)
	For PriceLevel In ([monPriceLevel1], [monPriceLevel2], [monPriceLevel3], [monPriceLevel4], [monPriceLevel5], [monPriceLevel6], [monPriceLevel7], [monPriceLevel8], [monPriceLevel9], [monPriceLevel10], [monPriceLevel11], [monPriceLevel12], [monPriceLevel13], [monPriceLevel14], [monPriceLevel15], [monPriceLevel16], [monPriceLevel17], [monPriceLevel18], [monPriceLevel19], [monPriceLevel20])) As Pvt1
Pivot (Min(intFromQty)
	For FromQty In ([intPriceLevel1FromQty], [intPriceLevel2FromQty], [intPriceLevel3FromQty], [intPriceLevel4FromQty], [intPriceLevel5FromQty], [intPriceLevel6FromQty], [intPriceLevel7FromQty], [intPriceLevel8FromQty], [intPriceLevel9FromQty], [intPriceLevel10FromQty], [intPriceLevel11FromQty], [intPriceLevel12FromQty], [intPriceLevel13FromQty], [intPriceLevel14FromQty], [intPriceLevel15FromQty], [intPriceLevel16FromQty], [intPriceLevel17FromQty], [intPriceLevel18FromQty], [intPriceLevel19FromQty], [intPriceLevel20FromQty])) AS Pvt2
Pivot (Min(intToQty)
	For ToQty In ([intPriceLevel1ToQty], [intPriceLevel2ToQty], [intPriceLevel3ToQty], [intPriceLevel4ToQty], [intPriceLevel5ToQty], [intPriceLevel6ToQty], [intPriceLevel7ToQty], [intPriceLevel8ToQty], [intPriceLevel9ToQty], [intPriceLevel10ToQty], [intPriceLevel11ToQty], [intPriceLevel12ToQty], [intPriceLevel13ToQty], [intPriceLevel14ToQty], [intPriceLevel15ToQty], [intPriceLevel16ToQty], [intPriceLevel17ToQty], [intPriceLevel18ToQty], [intPriceLevel19ToQty], [intPriceLevel20ToQty])) AS Pvt3
Pivot (Min(vcName)
	For PriceLevelName In ([vcPriceLevel1Name], [vcPriceLevel2Name], [vcPriceLevel3Name], [vcPriceLevel4Name], [vcPriceLevel5Name], [vcPriceLevel6Name], [vcPriceLevel7Name], [vcPriceLevel8Name], [vcPriceLevel9Name], [vcPriceLevel10Name], [vcPriceLevel11Name], [vcPriceLevel12Name], [vcPriceLevel13Name], [vcPriceLevel14Name], [vcPriceLevel15Name], [vcPriceLevel16Name], [vcPriceLevel17Name], [vcPriceLevel18Name], [vcPriceLevel19Name], [vcPriceLevel20Name])) As Pvt4 ) AS TempPriceTable '
        SET @GroupBy = ''
        SET @SelectFields = ''  
        SET @WCondition = ''



        SET @Where = @Where
            + ' WHERE I.charItemType <>''A'' and I.numDomainID='
            + CONVERT(VARCHAR(15), @numDomainID)
	


--SELECT * FROM dbo.View_DynamicColumns WHERE numDomainID = 72 AND numFormID = 20
--- Get Fields setting for Update item for given domain
        SELECT  X.*,
                ROW_NUMBER() OVER ( ORDER BY numFieldID ASC ) AS tintColumn
        INTO    #FielList
        FROM    ( SELECT    vcDbColumnName,
                            vcFieldName,
                            vcAssociatedControlType,
                            vcListItemType,
                            numListID,
                            vcLookBackTableName,
                            bitCustom,
                            numFieldID
                  FROM      dbo.View_DynamicColumns
                  WHERE     numFormId = 20
                            AND numDomainID = @numDomainID
                            AND bitImport = 1
                            AND numFieldID NOT IN (
                            SELECT  numFieldID
                            FROM    dbo.DycFieldMaster
                            WHERE   vcDbColumnName = 'numItemCode'
                                    AND vcLookBackTableName = 'Item' )
                  UNION ALL
                  SELECT    vcDbColumnName,
                            vcFieldName,
                            vcAssociatedControlType,
                            '' vcListItemType,
                            numListID,
                            '' vcLookBackTableName,
                            bitCustom,
                            numFieldID
                  FROM      dbo.View_DynamicCustomColumns
                  WHERE     numDomainID = @numDomainID
                            AND grp_id = 5
                            AND numFormID = 20
                ) X
        ORDER BY tintColumn ASC

	
        SET @tintOrder = 0

        SET @strSql = 'SELECT DISTINCT I.numItemCode [Item ID] '

        SELECT TOP 1
                @tintOrder = tintColumn + 1,
                @vcDbColumnName = vcDbColumnName,
                @vcFieldName = vcFieldName,
                @vcAssociatedControlType = vcAssociatedControlType,
                @vcListItemType = vcListItemType,
                @numListID = numListID,
                @vcLookBackTableName = vcLookBackTableName,
                @bitCustom = bitCustom,
                @fld_id = numFieldId
        FROM    #FielList
        ORDER BY tintColumn ASC



        WHILE @tintOrder > 0
            BEGIN
                PRINT @bitCustom
                PRINT @vcAssociatedControlType
                PRINT @vcLookBackTableName
                PRINT @vcDbColumnName
                PRINT @vcListItemType
                IF ISNULL(@bitCustom, 0) = 0 
                    BEGIN

                        IF @vcAssociatedControlType = 'SelectBox' 
                            BEGIN  
                               
                                IF @vcLookBackTableName = 'Warehouses' 
                                    BEGIN
                                        SET @SelectFields = @SelectFields
                                            + ' ,W.vcWarehouse ['
                                            + @vcFieldName + ']'
                                        

										IF CHARINDEX('dbo.WareHouseItems WI',@InneJoinOn) = 0 
                                            SET @InneJoinOn = @InneJoinOn + ' Left JOIN dbo.WareHouseItems WI ON WI.numItemID = I.numItemCode and WI.numDomainID = I.numDomainID '     
                                        
										IF CHARINDEX('dbo.WareHouseItmsDTL',@InneJoinOn) = 0
										BEGIN 
											IF CHARINDEX('dbo.WareHouseItems WI',@InneJoinOn) = 0 
											BEGIN
												SET @InneJoinOn = @InneJoinOn + ' Left JOIN dbo.WareHouseItems WI ON WI.numItemID = I.numItemCode and WI.numDomainID = I.numDomainID '              
											END
	                                            
											SET @InneJoinOn = @InneJoinOn + '  Left JOIN dbo.WareHouseItmsDTL WID ON WID.numWareHouseItemID = WI.numWareHouseItemID  '
                                        END 

                                        IF CHARINDEX('dbo.WareHouses W', @InneJoinOn) = 0 
                                            SET @InneJoinOn = @InneJoinOn + ' Left JOIN dbo.WareHouses W ON W.numWareHouseID = WI.numWareHouseID  '
                                                                                              
                                                        
                                    END   
                                ELSE 
									IF @vcLookBackTableName = 'WareHouseItems' 
										BEGIN
--											PRINT 'TESTED1'
--											SET @SelectFields = @SelectFields + ' ,WI.' + @vcDbColumnName + ' [' + @vcFieldName + ']'
--											IF CHARINDEX('dbo.WareHouseItems WI',@InneJoinOn) = 0 
--												SET @InneJoinOn = @InneJoinOn + ' Left JOIN dbo.WareHouseItems WI ON WI.numItemID = I.numItemCode and WI.numDomainID = I.numDomainID'
											
											IF @vcDbColumnName = 'vcLocation'
											BEGIN
												--PRINT 'TESTED2'
													SET @SelectFields = @SelectFields + ' ,WL.' + @vcDbColumnName + ' [' + @vcFieldName + ']'
													IF CHARINDEX('WarehouseLocation',@InneJoinOn) = 0 
														IF CHARINDEX('dbo.WareHouseItems WI',@InneJoinOn) = 0 
														BEGIN
															SET @InneJoinOn = @InneJoinOn + ' Left JOIN dbo.WareHouseItems WI ON WI.numItemID = I.numItemCode and WI.numDomainID = I.numDomainID '              
														END
														
														SET @InneJoinOn = @InneJoinOn + ' Left JOIN dbo.WarehouseLocation WL ON WI.numWLocationID = WL.numWLocationID and WI.numDomainID = WL.numDomainID '      
												END	
										END                                                    
											
                                    IF @vcLookBackTableName = 'CompanyAssets' 
                                        BEGIN
		 	--SET @SelectFields = @SelectFields + ' ,CA.'+ @vcDbColumnName +' ['+ @vcFieldName+']'
                                            SET @SelectFields = @SelectFields
                                                + ' ,dbo.fn_GetComapnyName(CA.numDivId) ['
                                                + @vcFieldName + ']'
                                            IF CHARINDEX('dbo.CompanyAssets CA',
                                                         @InneJoinOn) = 0 
                                                SET @InneJoinOn = @InneJoinOn
                                                    + 'LEFT JOIN  dbo.CompanyAssets CA  ON I.numItemCode = CA.numItemCode AND I.numDomainID = CA.numDomainId '
                                        END                              
                                    ELSE 
                                        IF @vcLookBackTableName = 'Category' 
                                            BEGIN
                                                SET @SelectFields = @SelectFields
                                                    + ' ,C.vcCategoryName ['
                                                    + @vcFieldName + ']'
                                                IF CHARINDEX('dbo.Category C',
                                                             @InneJoinOn) = 0 
                                                    SET @InneJoinOn = @InneJoinOn
                                                        + ' Left JOIN ItemCategory IC on IC.numItemID = I.numItemCode LEFT JOIN dbo.Category C ON C.numCategoryID = IC.numCategoryID  '
                                            END
                                        ELSE 
                                            IF @vcLookBackTableName = 'chart_of_accounts' 
                                                BEGIN
                                                    IF @vcDbColumnName = 'numCOGsChartAcntId' 
                                                        SET @SelectFields = @SelectFields
                                                            + ' ,(select vcAccountName from chart_of_Accounts COA WHERE COA.numAccountId=numCOGsChartAcntId) as  ['
                                                            + @vcFieldName
                                                            + ']'
                                                    ELSE 
                                                        IF @vcDbColumnName = 'numAssetChartAcntId' 
                                                            SET @SelectFields = @SelectFields + ' ,(select vcAccountName from chart_of_Accounts COA WHERE COA.numAccountId=numAssetChartAcntId) as  [' + @vcFieldName + ']'
                                                        ELSE 
                                                            IF @vcDbColumnName = 'numIncomeChartAcntId' 
                                                                SET @SelectFields = @SelectFields + ' ,(select vcAccountName from chart_of_Accounts COA where COA.numAccountId=numIncomeChartAcntId) as  [' + @vcFieldName + ']'

                                                END 
                                            ELSE 
                                                IF @vcLookBackTableName = 'DivisionMaster' 
                                                    BEGIN
                                                        SET @SelectFields = @SelectFields
                                                            + ' ,dbo.fn_GetComapnyName(V.numVendorID) ['
                                                            + @vcFieldName
                                                            + ']'
                                                        IF CHARINDEX('dbo.Vendor', @InneJoinOn) = 0 
                                                            SET @InneJoinOn = @InneJoinOn + ' LEFT JOIN dbo.Vendor V ON V.numItemCode = I.numItemCode AND I.numDomainID = V.numDomainID '
                                                    END 
                                                ELSE 
                                                    IF @vcLookBackTableName = 'Vendor' 
                                                        BEGIN
                                                            SET @SelectFields = @SelectFields + ' ,dbo.fn_GetComapnyName(V.numVendorID) [' + @vcFieldName + ']'
                                                            IF CHARINDEX('dbo.Vendor', @InneJoinOn) = 0 
                                                                SET @InneJoinOn = @InneJoinOn + ' LEFT JOIN dbo.Vendor V ON V.numItemCode = I.numItemCode AND I.numDomainID = V.numDomainID '
                                                        END
                                                    ELSE    
                                                    IF @vcLookBackTableName = 'PricingTable' 
                                                        BEGIN
															
                                                            IF @vcFieldName = 'Price Rule Type'
															BEGIN
																	SET @SelectFields = @SelectFields + '  , (CASE WHEN (SELECT DISTINCT tintRuleType FROM dbo.PricingTable WHERE numItemCode = I.numItemCode AND ISNULL(numCurrencyID,0) = 0) = 1
																												  THEN ''Deduct From List Price''
																												  WHEN (SELECT DISTINCT tintRuleType FROM dbo.PricingTable WHERE numItemCode = I.numItemCode AND ISNULL(numCurrencyID,0) = 0) = 2 
																												  THEN ''Add to Primary Vendor Cost''
																												  WHEN (SELECT DISTINCT tintRuleType FROM dbo.PricingTable WHERE numItemCode = I.numItemCode AND ISNULL(numCurrencyID,0) = 0) = 3 
																												  THEN ''Named Price''
																												  ELSE ''''
																										      END) AS [Price Rule Type] '
															END
															IF @vcFieldName = 'Price Level Discount Type'
															BEGIN
																	SET @SelectFields = @SelectFields + '  ,(CASE WHEN (SELECT DISTINCT tintDiscountType FROM dbo.PricingTable WHERE numItemCode = I.numItemCode AND ISNULL(numCurrencyID,0) = 0) = 1
																												  THEN ''Percentage''
																												  WHEN (SELECT DISTINCT tintDiscountType FROM dbo.PricingTable WHERE numItemCode = I.numItemCode AND ISNULL(numCurrencyID,0) = 0) = 2 
																												  THEN ''Flat Amount''
																												  ELSE ''''
																										      END) AS [Price Level Discount Type] '
															END
															
															SET @SelectFields = REPLACE(@SelectFields ,',I.vcPriceLevelDetail [Price Level]','');	
															
                                                        END               
                                                    ELSE 
                                                        IF @vcListItemType = 'I' --Item group
                                                            BEGIN                              
                                                                SET @SelectFields = @SelectFields + ',IG.vcItemGroup [' + @vcFieldName + ']'                              
                                                                SET @InneJoinOn = @InneJoinOn + ' LEFT JOIN ItemGroups IG ON I.numDomainID = IG.numDomainID AND I.numItemGroup = IG.numItemGroupID '
			   
                                                            END
                                                        ELSE 
                                                            IF @vcListItemType = 'P'--Item Type
                                                                BEGIN
                                                                    SET @SelectFields = @SelectFields + ', CASE I.charItemType WHEN ''p'' THEN ''Inventory Item'' WHEN ''n'' THEN ''Non-Inventory Item'' WHEN ''s'' THEN ''Service'' ELSE '''' END   [' + @vcFieldName + ']'                              
                                                                END
                                                            ELSE 
                                                                IF @vcListItemType = 'L'--Any List Item
                                                                    BEGIN
                                                                        SET @SelectFields = @SelectFields + ',L' + CONVERT(VARCHAR(3), @tintOrder) + '.vcData' + ' [' + @vcFieldName + ']'                              
                                                                        SET @InneJoinOn = @InneJoinOn + ' left Join ListDetails L' + CONVERT(VARCHAR(3), @tintOrder) + ' on L' + CONVERT(VARCHAR(3), @tintOrder) + '.numListItemID=' + @vcDbColumnName                              
                                                                    END 
                                                                ELSE 
                                                                    IF @vcListItemType = 'U' 
                                                                        BEGIN
                                                                            IF @vcDbColumnName = 'numBaseUnit' 
                                                                                SET @SelectFields = @SelectFields + ' ,(select vcUnitName from UOM where numUOMId=ISNULL(I.numBaseUnit,0)) as  [' + @vcFieldName + ']'
                                                                            ELSE 
                                                                                IF @vcDbColumnName = 'numPurchaseUnit' 
                                                                                    SET @SelectFields = @SelectFields + ' ,(select vcUnitName from UOM where numUOMId=ISNULL(I.numPurchaseUnit,0)) as  [' + @vcFieldName + ']'
                                                                                ELSE 
                                                                                    IF @vcDbColumnName = 'numSaleUnit' 
                                                                                        SET @SelectFields = @SelectFields + ' ,(select vcUnitName from UOM where numUOMId=ISNULL(I.numSaleUnit,0)) as  [' + @vcFieldName + ']'
	          
                                                                        END
				
			
                            END
                        ELSE 
                            BEGIN
                                IF @vcLookBackTableName = 'WareHouseItmsDTL' 
                                    BEGIN
                                        SET @SelectFields = @SelectFields + ' ,WID.' + @vcDbColumnName + ' [' + @vcFieldName + ']'
                                            
                                        IF CHARINDEX('dbo.WareHouseItmsDTL',@InneJoinOn) = 0
											BEGIN 
												IF CHARINDEX('dbo.WareHouseItems WI',@InneJoinOn) = 0 
													BEGIN
														SET @InneJoinOn = @InneJoinOn + ' Left JOIN dbo.WareHouseItems WI ON WI.numItemID = I.numItemCode and WI.numDomainID = I.numDomainID '              
													END 
	                                            
												SET @InneJoinOn = @InneJoinOn + '  Left JOIN dbo.WareHouseItmsDTL WID ON WI.numWarehouseItemID = WI.numWarehouseItemID'
                                            END
                                    END
								
								ELSE IF @vcLookBackTableName = 'WareHouseItems' 
                                    BEGIN
                                        SET @SelectFields = @SelectFields + ' ,WI.' + @vcDbColumnName + ' [' + @vcFieldName + ']'
                                        IF CHARINDEX('dbo.WareHouseItems WI',@InneJoinOn) = 0 
                                            SET @InneJoinOn = @InneJoinOn + ' Left JOIN dbo.WareHouseItems WI ON WI.numItemID = I.numItemCode and WI.numDomainID = I.numDomainID'
                                    END
                                    
                                ELSE 
                                    IF @vcLookBackTableName = 'CompanyAssets' 
                                        BEGIN
                                            SET @SelectFields = @SelectFields
                                                + ' ,CA.' + @vcDbColumnName
                                                + ' [' + @vcFieldName + ']'
                                            IF CHARINDEX('dbo.CompanyAssets CA',
                                                         @InneJoinOn) = 0 
                                                SET @InneJoinOn = @InneJoinOn
                                                    + 'LEFT JOIN  dbo.CompanyAssets CA  ON I.numItemCode = CA.numItemCode AND I.numDomainID = CA.numDomainId '
                                        END 
	
                                    ELSE 
                                        IF @vcLookBackTableName = 'Vendor' 
                                            BEGIN
                                                SET @SelectFields = @SelectFields
                                                    + ' ,V.' + @vcDbColumnName
                                                    + ' [' + @vcFieldName
                                                    + ']'
                                                IF CHARINDEX('dbo.Vendor',
                                                             @InneJoinOn) = 0 
                                                    SET @InneJoinOn = @InneJoinOn
                                                        + ' LEFT JOIN dbo.Vendor V ON V.numItemCode = I.numItemCode AND I.numDomainID = V.numDomainID '
                                                      
                                            END      
                                        ELSE 
                                            IF @vcDbColumnName = 'vcSerialNo' 
                                                BEGIN
--			SET @InneJoinOn= @InneJoinOn +'LEFT JOIN dbo.WareHouseItems WI ON I.numItemCode = WI.numItemID AND I.numDomainID = WI.numDomainID
--			LEFT JOIN dbo.WareHouseItmsDTL WD ON WI.numWareHouseItemID = WD.numWareHouseItemID'	
                                                    SET @SelectFields = @SelectFields
                                                        + ' ,0 ['
                                                        + @vcFieldName + ']'
                                                    IF CHARINDEX('dbo.WareHouseItems WI', @InneJoinOn) = 0 
                                                        SET @InneJoinOn = @InneJoinOn
                                                            + ' LEFT JOIN dbo.WareHouseItems WI ON WI.numItemID = I.numItemCode '
                                                END
                                            ELSE 
                                                IF @vcDbColumnName = 'vcPartNo' 
                                                    BEGIN
                                                        SET @SelectFields = @SelectFields
                                                            + ' ,0 ['
                                                            + @vcFieldName
                                                            + ']'
                                                    END
                                                ELSE 
                                                    IF @vcDbColumnName = 'charItemType' 
                                                        BEGIN
                                                            SET @SelectFields = @SelectFields + ' ,case when i.charItemType=''P'' then ''Inventory Item'' when i.charItemType=''S'' then ''Service'' when i.charItemType=''A'' then ''Asset Item'' when i.charItemType=''N'' then ''Non-Inventory Item'' ELSE ''Non-Inventory Item'' END  [' + @vcFieldName + ']'
                                                        END
                                                    ELSE 
                                                        IF @vcDbColumnName = 'txtDesc' 
                                                            BEGIN
                                                                SET @SelectFields = @SelectFields + ' ,CAST(IED.txtDesc AS NVARCHAR(4000)) [' + @vcFieldName + ']'
                                                                SET @InneJoinOn = @InneJoinOn + ' left JOIN dbo.ItemExtendedDetails IED ON IED.numItemCode = I.numItemCode '
                                                            END
														Else IF @vcDbColumnName = 'txtShortDesc' 
                                                            BEGIN
                                                                SET @SelectFields = @SelectFields + ' ,CAST(IED1.txtShortDesc AS NVARCHAR(4000)) [' + @vcFieldName + ']'
                                                                SET @InneJoinOn = @InneJoinOn + ' left JOIN dbo.ItemExtendedDetails IED1 ON IED1.numItemCode = I.numItemCode '
                                                            END
															ELSE IF @vcDbColumnName = 'monPriceLevel1'
															BEGIN
																SET @SelectFields = @SelectFields + '  ,TempPriceTable.monPriceLevel1 AS [monPriceLevel1]'
															END
															ELSE IF @vcDbColumnName = 'monPriceLevel2'
															BEGIN
																SET @SelectFields = @SelectFields + '  ,TempPriceTable.monPriceLevel2 AS [monPriceLevel2]'
															END
															ELSE IF @vcDbColumnName = 'monPriceLevel3'
															BEGIN
																SET @SelectFields = @SelectFields + '  ,TempPriceTable.monPriceLevel3 AS [monPriceLevel3]'
															END
															ELSE IF @vcDbColumnName = 'monPriceLevel4'
															BEGIN
																SET @SelectFields = @SelectFields + '  ,TempPriceTable.monPriceLevel4 AS [monPriceLevel4]'
															END
															ELSE IF @vcDbColumnName = 'monPriceLevel5'
															BEGIN
																SET @SelectFields = @SelectFields + '  ,TempPriceTable.monPriceLevel5 AS [monPriceLevel5]'
															END
															ELSE IF @vcDbColumnName = 'monPriceLevel6'
															BEGIN
																SET @SelectFields = @SelectFields + '  ,TempPriceTable.monPriceLevel6 AS [monPriceLevel6]'
															END
															ELSE IF @vcDbColumnName = 'monPriceLevel7'
															BEGIN
																SET @SelectFields = @SelectFields + '  ,TempPriceTable.monPriceLevel7 AS [monPriceLevel7]'
															END
															ELSE IF @vcDbColumnName = 'monPriceLevel8'
															BEGIN
																SET @SelectFields = @SelectFields + '  ,TempPriceTable.monPriceLevel8 AS [monPriceLevel8]'
															END
															ELSE IF @vcDbColumnName = 'monPriceLevel9'
															BEGIN
																SET @SelectFields = @SelectFields + '  ,TempPriceTable.monPriceLevel9 AS [monPriceLevel9]'
															END
															ELSE IF @vcDbColumnName = 'monPriceLevel10'
															BEGIN
																SET @SelectFields = @SelectFields + '  ,TempPriceTable.monPriceLevel10 AS [monPriceLevel10]'
															END
															ELSE IF @vcDbColumnName = 'monPriceLevel11'
															BEGIN
																SET @SelectFields = @SelectFields + '  ,TempPriceTable.monPriceLevel11 AS [monPriceLevel11]'
															END
															ELSE IF @vcDbColumnName = 'monPriceLevel12'
															BEGIN
																SET @SelectFields = @SelectFields + '  ,TempPriceTable.monPriceLevel12 AS [monPriceLevel12]'
															END
															ELSE IF @vcDbColumnName = 'monPriceLevel13'
															BEGIN
																SET @SelectFields = @SelectFields + '  ,TempPriceTable.monPriceLevel13 AS [monPriceLevel13]'
															END
															ELSE IF @vcDbColumnName = 'monPriceLevel14'
															BEGIN
																SET @SelectFields = @SelectFields + '  ,TempPriceTable.monPriceLevel14 AS [monPriceLevel14]'
															END
															ELSE IF @vcDbColumnName = 'monPriceLevel15'
															BEGIN
																SET @SelectFields = @SelectFields + '  ,TempPriceTable.monPriceLevel15 AS [monPriceLevel15]'
															END
															ELSE IF @vcDbColumnName = 'monPriceLevel16'
															BEGIN
																SET @SelectFields = @SelectFields + '  ,TempPriceTable.monPriceLevel16 AS [monPriceLevel16]'
															END
															ELSE IF @vcDbColumnName = 'monPriceLevel17'
															BEGIN
																SET @SelectFields = @SelectFields + '  ,TempPriceTable.monPriceLevel17 AS [monPriceLevel17]'
															END
															ELSE IF @vcDbColumnName = 'monPriceLevel18'
															BEGIN
																SET @SelectFields = @SelectFields + '  ,TempPriceTable.monPriceLevel18 AS [monPriceLevel18]'
															END
															ELSE IF @vcDbColumnName = 'monPriceLevel19'
															BEGIN
																SET @SelectFields = @SelectFields + '  ,TempPriceTable.monPriceLevel19 AS [monPriceLevel19]'
															END
															ELSE IF @vcDbColumnName = 'monPriceLevel20'
															BEGIN
																SET @SelectFields = @SelectFields + '  ,TempPriceTable.monPriceLevel20 AS [monPriceLevel20]'
															END
															ELSE IF @vcDbColumnName = 'intPriceLevel1FromQty'
															BEGIN
																SET @SelectFields = @SelectFields + '  ,TempPriceTable.intPriceLevel1FromQty AS [intPriceLevel1FromQty]'
															END
															ELSE IF @vcDbColumnName = 'intPriceLevel1ToQty'
															BEGIN
																SET @SelectFields = @SelectFields + '  ,TempPriceTable.intPriceLevel1ToQty AS [intPriceLevel1ToQty]'
															END
															ELSE IF @vcDbColumnName = 'intPriceLevel2FromQty'
															BEGIN
																SET @SelectFields = @SelectFields + '  ,TempPriceTable.intPriceLevel2FromQty AS [intPriceLevel2FromQty]'
															END
															ELSE IF @vcDbColumnName = 'intPriceLevel2ToQty'
															BEGIN
																SET @SelectFields = @SelectFields + '  ,TempPriceTable.intPriceLevel2ToQty AS [intPriceLevel2ToQty]'
															END
															ELSE IF @vcDbColumnName = 'intPriceLevel3FromQty'
															BEGIN
																SET @SelectFields = @SelectFields + '  ,TempPriceTable.intPriceLevel3FromQty AS [intPriceLevel3FromQty]'
															END
															ELSE IF @vcDbColumnName = 'intPriceLevel3ToQty'
															BEGIN
																SET @SelectFields = @SelectFields + '  ,TempPriceTable.intPriceLevel3ToQty AS [intPriceLevel3ToQty]'
															END
															ELSE IF @vcDbColumnName = 'intPriceLevel4FromQty'
															BEGIN
																SET @SelectFields = @SelectFields + '  ,TempPriceTable.intPriceLevel4FromQty AS [intPriceLevel4FromQty]'
															END
															ELSE IF @vcDbColumnName = 'intPriceLevel4ToQty'
															BEGIN
																SET @SelectFields = @SelectFields + '  ,TempPriceTable.intPriceLevel4ToQty AS [intPriceLevel4ToQty]'
															END
															ELSE IF @vcDbColumnName = 'intPriceLevel5FromQty'
															BEGIN
																SET @SelectFields = @SelectFields + '  ,TempPriceTable.intPriceLevel5FromQty AS [intPriceLevel5FromQty]'
															END
															ELSE IF @vcDbColumnName = 'intPriceLevel5ToQty'
															BEGIN
																SET @SelectFields = @SelectFields + '  ,TempPriceTable.intPriceLevel5ToQty AS [intPriceLevel5ToQty]'
															END
															ELSE IF @vcDbColumnName = 'intPriceLevel6FromQty'
															BEGIN
																SET @SelectFields = @SelectFields + '  ,TempPriceTable.intPriceLevel6FromQty AS [intPriceLevel6FromQty]'
															END
															ELSE IF @vcDbColumnName = 'intPriceLevel6ToQty'
															BEGIN
																SET @SelectFields = @SelectFields + '  ,TempPriceTable.intPriceLevel6ToQty AS [intPriceLevel6ToQty]'
															END
															ELSE IF @vcDbColumnName = 'intPriceLevel7FromQty'
															BEGIN
																SET @SelectFields = @SelectFields + '  ,TempPriceTable.intPriceLevel7FromQty AS [intPriceLevel7FromQty]'
															END
															ELSE IF @vcDbColumnName = 'intPriceLevel7ToQty'
															BEGIN
																SET @SelectFields = @SelectFields + '  ,TempPriceTable.intPriceLevel7ToQty AS [intPriceLevel7ToQty]'
															END
															ELSE IF @vcDbColumnName = 'intPriceLevel8FromQty'
															BEGIN
																SET @SelectFields = @SelectFields + '  ,TempPriceTable.intPriceLevel8FromQty AS [intPriceLevel8FromQty]'
															END
															ELSE IF @vcDbColumnName = 'intPriceLevel8ToQty'
															BEGIN
																SET @SelectFields = @SelectFields + '  ,TempPriceTable.intPriceLevel8ToQty AS [intPriceLevel8ToQty]'
															END
															ELSE IF @vcDbColumnName = 'intPriceLevel9FromQty'
															BEGIN
																SET @SelectFields = @SelectFields + '  ,TempPriceTable.intPriceLevel9FromQty AS [intPriceLevel9FromQty]'
															END
															ELSE IF @vcDbColumnName = 'intPriceLevel9ToQty'
															BEGIN
																SET @SelectFields = @SelectFields + '  ,TempPriceTable.intPriceLevel9ToQty AS [intPriceLevel9ToQty]'
															END
															ELSE IF @vcDbColumnName = 'intPriceLevel10FromQty'
															BEGIN
																SET @SelectFields = @SelectFields + '  ,TempPriceTable.intPriceLevel10FromQty AS [intPriceLevel10FromQty]'
															END
															ELSE IF @vcDbColumnName = 'intPriceLevel10ToQty'
															BEGIN
																SET @SelectFields = @SelectFields + '  ,TempPriceTable.intPriceLevel10ToQty AS [intPriceLevel10ToQty]'
															END
															ELSE IF @vcDbColumnName = 'intPriceLevel11FromQty'
															BEGIN
																SET @SelectFields = @SelectFields + '  ,TempPriceTable.intPriceLevel11FromQty AS [intPriceLevel11FromQty]'
															END
															ELSE IF @vcDbColumnName = 'intPriceLevel11ToQty'
															BEGIN
																SET @SelectFields = @SelectFields + '  ,TempPriceTable.intPriceLevel11ToQty AS [intPriceLevel11ToQty]'
															END
															ELSE IF @vcDbColumnName = 'intPriceLevel12FromQty'
															BEGIN
																SET @SelectFields = @SelectFields + '  ,TempPriceTable.intPriceLevel12FromQty AS [intPriceLevel12FromQty]'
															END
															ELSE IF @vcDbColumnName = 'intPriceLevel12ToQty'
															BEGIN
																SET @SelectFields = @SelectFields + '  ,TempPriceTable.intPriceLevel12ToQty AS [intPriceLevel12ToQty]'
															END
															ELSE IF @vcDbColumnName = 'intPriceLevel13FromQty'
															BEGIN
																SET @SelectFields = @SelectFields + '  ,TempPriceTable.intPriceLevel13FromQty AS [intPriceLevel13FromQty]'
															END
															ELSE IF @vcDbColumnName = 'intPriceLevel13ToQty'
															BEGIN
																SET @SelectFields = @SelectFields + '  ,TempPriceTable.intPriceLevel13ToQty AS [intPriceLevel13ToQty]'
															END
															ELSE IF @vcDbColumnName = 'intPriceLevel14FromQty'
															BEGIN
																SET @SelectFields = @SelectFields + '  ,TempPriceTable.intPriceLevel14FromQty AS [intPriceLevel14FromQty]'
															END
															ELSE IF @vcDbColumnName = 'intPriceLevel14ToQty'
															BEGIN
																SET @SelectFields = @SelectFields + '  ,TempPriceTable.intPriceLevel14ToQty AS [intPriceLevel14ToQty]'
															END
															ELSE IF @vcDbColumnName = 'intPriceLevel15FromQty'
															BEGIN
																SET @SelectFields = @SelectFields + '  ,TempPriceTable.intPriceLevel15FromQty AS [intPriceLevel15FromQty]'
															END
															ELSE IF @vcDbColumnName = 'intPriceLevel15ToQty'
															BEGIN
																SET @SelectFields = @SelectFields + '  ,TempPriceTable.intPriceLevel15ToQty AS [intPriceLevel15ToQty]'
															END
															ELSE IF @vcDbColumnName = 'intPriceLevel16FromQty'
															BEGIN
																SET @SelectFields = @SelectFields + '  ,TempPriceTable.intPriceLevel16FromQty AS [intPriceLevel16FromQty]'
															END
															ELSE IF @vcDbColumnName = 'intPriceLevel16ToQty'
															BEGIN
																SET @SelectFields = @SelectFields + '  ,TempPriceTable.intPriceLevel16ToQty AS [intPriceLevel16ToQty]'
															END
															ELSE IF @vcDbColumnName = 'intPriceLevel17FromQty'
															BEGIN
																SET @SelectFields = @SelectFields + '  ,TempPriceTable.intPriceLevel17FromQty AS [intPriceLevel17FromQty]'
															END
															ELSE IF @vcDbColumnName = 'intPriceLevel17ToQty'
															BEGIN
																SET @SelectFields = @SelectFields + '  ,TempPriceTable.intPriceLevel17ToQty AS [intPriceLevel17ToQty]'
															END
															ELSE IF @vcDbColumnName = 'intPriceLevel18FromQty'
															BEGIN
																SET @SelectFields = @SelectFields + '  ,TempPriceTable.intPriceLevel18FromQty AS [intPriceLevel18FromQty]'
															END
															ELSE IF @vcDbColumnName = 'intPriceLevel18ToQty'
															BEGIN
																SET @SelectFields = @SelectFields + '  ,TempPriceTable.intPriceLevel18ToQty AS [intPriceLevel18ToQty]'
															END
															ELSE IF @vcDbColumnName = 'intPriceLevel19FromQty'
															BEGIN
																SET @SelectFields = @SelectFields + '  ,TempPriceTable.intPriceLevel19FromQty AS [intPriceLevel19FromQty]'
															END
															ELSE IF @vcDbColumnName = 'intPriceLevel19ToQty'
															BEGIN
																SET @SelectFields = @SelectFields + '  ,TempPriceTable.intPriceLevel19ToQty AS [intPriceLevel19ToQty]'
															END
															ELSE IF @vcDbColumnName = 'intPriceLevel20FromQty'
															BEGIN
																SET @SelectFields = @SelectFields + '  ,TempPriceTable.intPriceLevel20FromQty AS [intPriceLevel20FromQty]'
															END
															ELSE IF @vcDbColumnName = 'intPriceLevel20ToQty'
															BEGIN
																SET @SelectFields = @SelectFields + '  ,TempPriceTable.intPriceLevel20ToQty AS [intPriceLevel20ToQty]'
															END
                                                            ELSE IF @vcDbColumnName = 'vcPriceLevel1Name'
															BEGIN
																SET @SelectFields = @SelectFields + '  ,TempPriceTable.vcPriceLevel1Name AS [vcPriceLevel1Name]'
															END
															ELSE IF @vcDbColumnName = 'vcPriceLevel2Name'
															BEGIN
																SET @SelectFields = @SelectFields + '  ,TempPriceTable.vcPriceLevel2Name AS [vcPriceLevel2Name]'
															END
															ELSE IF @vcDbColumnName = 'vcPriceLevel3Name'
															BEGIN
																SET @SelectFields = @SelectFields + '  ,TempPriceTable.vcPriceLevel3Name AS [vcPriceLevel3Name]'
															END
															ELSE IF @vcDbColumnName = 'vcPriceLevel4Name'
															BEGIN
																SET @SelectFields = @SelectFields + '  ,TempPriceTable.vcPriceLevel4Name AS [vcPriceLevel4Name]'
															END
															ELSE IF @vcDbColumnName = 'vcPriceLevel5Name'
															BEGIN
																SET @SelectFields = @SelectFields + '  ,TempPriceTable.vcPriceLevel5Name AS [vcPriceLevel5Name]'
															END
															ELSE IF @vcDbColumnName = 'vcPriceLevel6Name'
															BEGIN
																SET @SelectFields = @SelectFields + '  ,TempPriceTable.vcPriceLevel6Name AS [vcPriceLevel6Name]'
															END
															ELSE IF @vcDbColumnName = 'vcPriceLevel7Name'
															BEGIN
																SET @SelectFields = @SelectFields + '  ,TempPriceTable.vcPriceLevel7Name AS [vcPriceLevel7Name]'
															END
															ELSE IF @vcDbColumnName = 'vcPriceLevel8Name'
															BEGIN
																SET @SelectFields = @SelectFields + '  ,TempPriceTable.vcPriceLevel8Name AS [vcPriceLevel8Name]'
															END
															ELSE IF @vcDbColumnName = 'vcPriceLevel9Name'
															BEGIN
																SET @SelectFields = @SelectFields + '  ,TempPriceTable.vcPriceLevel9Name AS [vcPriceLevel9Name]'
															END
															ELSE IF @vcDbColumnName = 'vcPriceLevel10Name'
															BEGIN
																SET @SelectFields = @SelectFields + '  ,TempPriceTable.vcPriceLevel10Name AS [vcPriceLevel10Name]'
															END
															ELSE IF @vcDbColumnName = 'vcPriceLevel11Name'
															BEGIN
																SET @SelectFields = @SelectFields + '  ,TempPriceTable.vcPriceLevel11Name AS [vcPriceLevel11Name]'
															END
															ELSE IF @vcDbColumnName = 'vcPriceLevel12Name'
															BEGIN
																SET @SelectFields = @SelectFields + '  ,TempPriceTable.vcPriceLevel12Name AS [vcPriceLevel12Name]'
															END
															ELSE IF @vcDbColumnName = 'vcPriceLevel13Name'
															BEGIN
																SET @SelectFields = @SelectFields + '  ,TempPriceTable.vcPriceLevel13Name AS [vcPriceLevel13Name]'
															END
															ELSE IF @vcDbColumnName = 'vcPriceLevel14Name'
															BEGIN
																SET @SelectFields = @SelectFields + '  ,TempPriceTable.vcPriceLevel14Name AS [vcPriceLevel14Name]'
															END
															ELSE IF @vcDbColumnName = 'vcPriceLevel15Name'
															BEGIN
																SET @SelectFields = @SelectFields + '  ,TempPriceTable.vcPriceLevel15Name AS [vcPriceLevel15Name]'
															END
															ELSE IF @vcDbColumnName = 'vcPriceLevel16Name'
															BEGIN
																SET @SelectFields = @SelectFields + '  ,TempPriceTable.vcPriceLevel16Name AS [vcPriceLevel16Name]'
															END
															ELSE IF @vcDbColumnName = 'vcPriceLevel17Name'
															BEGIN
																SET @SelectFields = @SelectFields + '  ,TempPriceTable.vcPriceLevel17Name AS [vcPriceLevel17Name]'
															END
															ELSE IF @vcDbColumnName = 'vcPriceLevel18Name'
															BEGIN
																SET @SelectFields = @SelectFields + '  ,TempPriceTable.vcPriceLevel18Name AS [vcPriceLevel18Name]'
															END
															ELSE IF @vcDbColumnName = 'vcPriceLevel19Name'
															BEGIN
																SET @SelectFields = @SelectFields + '  ,TempPriceTable.vcPriceLevel19Name AS [vcPriceLevel19Name]'
															END
															ELSE IF @vcDbColumnName = 'vcPriceLevel20Name'
															BEGIN
																SET @SelectFields = @SelectFields + '  ,TempPriceTable.vcPriceLevel20Name AS [vcPriceLevel20Name]'
															END 
                                                        ELSE 
                                                            IF @vcDbColumnName = 'bitAllowBackOrder'
                                                                OR @vcDbColumnName = 'bitTaxable'
                                                                OR @vcDbColumnName = 'IsArchieve' 
                                                                BEGIN
                                                                    SET @SelectFields = @SelectFields + ' ,Case  I.' + @vcDbColumnName + ' when 1 then ''TRUE'' ELSE ''FALSE'' END [' + @vcFieldName + ']'
                                                                END
                                                            ELSE 
                                                                BEGIN
                                                                    SET @SelectFields = @SelectFields + ' ,I.' + @vcDbColumnName + ' [' + @vcFieldName + ']'
                                                                END
		   
                            END
                    END
                ELSE 
                    IF @bitCustom = 1 
                        BEGIN
		/*--------------Custom field logic-----------*/
		
                            PRINT @tintOrder
                            PRINT @vcAssociatedControlType
                            PRINT @vcFieldName

                            IF @vcAssociatedControlType = 'Drop Down' 
                                BEGIN
                                    SET @SelectFields = @SelectFields + ',L'
                                        + CONVERT(VARCHAR(5), @tintOrder)
                                        + '.vcData [' + @vcFieldName + ']'
                                    SET @InneJoinOn = @InneJoinOn
                                        + ' left join dbo.CFW_FLD_Values_Item CFV'
                                        + CONVERT(VARCHAR(5), @tintOrder)
                                        + ' ON CFV'
                                        + CONVERT(VARCHAR(5), @tintOrder)
                                        + '.RecId = I.numItemCode and CFV'
                                        + CONVERT(VARCHAR(5), @tintOrder)
                                        + '.Fld_Id = '
                                        + CONVERT(VARCHAR(10), @fld_id)
                                        + ' left Join ListDetails L'
                                        + CONVERT(VARCHAR(3), @tintOrder)
                                        + ' on L'
                                        + CONVERT(VARCHAR(3), @tintOrder)
                                        + '.numListItemID = (CASE WHEN ISNUMERIC(CFV'
                                        + CONVERT(VARCHAR(3), @tintOrder)
                                        + '.Fld_Value)=1 THEN CAST(CFV'
                                        + CONVERT(VARCHAR(3), @tintOrder)
                                        + '.Fld_Value AS NUMERIC(9)) ELSE 0 END)'
							--+' left Join ListDetails L'+ convert(varchar(3),@tintOrder)+ ' on L'+convert(varchar(3),@tintOrder)+ '.numListItemID=CFV'+ convert(varchar(3),@tintOrder)+'.Fld_Value'
							--(CASE WHEN ISNUMERIC(CFV19.Fld_Value)=1 THEN CAST(CFV19.Fld_Value AS NUMERIC(9)) ELSE 0 END)
                                END
                            ELSE 
                                BEGIN
                                    SET @SelectFields = @SelectFields + ',CFV'
                                        + CONVERT(VARCHAR(5), @tintOrder)
                                        + '.Fld_Value [' + @vcFieldName + ']'
                                    SET @InneJoinOn = @InneJoinOn
                                        + ' left join dbo.CFW_FLD_Values_Item CFV'
                                        + CONVERT(VARCHAR(5), @tintOrder)
                                        + ' ON CFV'
                                        + CONVERT(VARCHAR(5), @tintOrder)
                                        + '.RecId = I.numItemCode and CFV'
                                        + CONVERT(VARCHAR(5), @tintOrder)
                                        + '.Fld_Id = '
                                        + CONVERT(VARCHAR(10), @fld_id)
                                END


		/*--------------Custom field ends-----------*/     	
                        END
  
  

	  
	  
                IF CHARINDEX('WareHouseItmsDTL', @Where) > 0 
                    BEGIN
                        IF CHARINDEX('dbo.WareHouseItems WI', @InneJoinOn) = 0 
                            SET @InneJoinOn = @InneJoinOn + ' LEFT JOIN dbo.WareHouseItems WI ON WI.numItemID = I.numItemCode '
                    END
  
  SELECT TOP 1
                        @tintOrder = tintColumn + 1,
                        @vcDbColumnName = vcDbColumnName,
                        @vcFieldName = vcFieldName,
                        @vcAssociatedControlType = vcAssociatedControlType,
                        @vcListItemType = vcListItemType,
                        @numListID = numListID,
                        @vcLookBackTableName = vcLookBackTableName,
                        @bitCustom = bitCustom,
                        @fld_id = numFieldId
                FROM    #FielList
                WHERE   tintColumn > @tintOrder - 1
                ORDER BY tintColumn ASC
                IF @@rowcount = 0 
                    SET @tintOrder = 0                              
            END                              

	
        IF CHARINDEX('V.bitIsPrimaryVendor', @SelectFields) > 0 
            BEGIN
                SET @SelectFields = REPLACE(@SelectFields,
                                            'V.bitIsPrimaryVendor',
                                            ' CASE WHEN V.numVendorID = I.numVendorID THEN 1 ELSE 0 END ')
            END

	
        SET @strSql = @strSql + @SelectFields + @from + @InneJoinOn + @Where
            + @OrderBy
	



        SET @strSql = @strSql + @GroupBy /*+ ' order by vcItemName; '*/
        PRINT @strSql
        EXEC ( @strSql
            )

		DROp TABLE #TEMPIds
-------------------------------------------------	
    END

