GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_GetFinancialReport' ) 
    DROP PROCEDURE USP_GetFinancialReport
GO
CREATE PROC [dbo].[USP_GetFinancialReport]
    @numFRID NUMERIC(18, 0) ,
    @numDomainID NUMERIC
AS 
SELECT  [numFRID],
        [vcReportName],
        [numFinancialViewID],
        dbo.fn_GetListItemName(numFinancialViewID) FinancialView,
        [numFinancialDimensionID],
        dbo.fn_GetListItemName(numFinancialDimensionID) FinancialDimension,
        [intDateRange],
         'Last ' + CAST(ISNULL(intDateRange,0) AS VARCHAR(10)) + ' days' DateRange,
        [numDomainID],
        [numCreatedBy],
        [dtCreateDate],
        [numModifiedBy],
        [dtModifiedDate]
FROM    [dbo].[FinancialReport]
WHERE   ( [numFRID] = @numFRID
          OR @numFRID = 0
        )
        AND numDomainID = @numDomainID

	
GO