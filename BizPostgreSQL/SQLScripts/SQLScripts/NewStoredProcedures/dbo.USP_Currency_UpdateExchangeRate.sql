SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_Currency_UpdateExchangeRate')
DROP PROCEDURE USP_Currency_UpdateExchangeRate
GO
CREATE PROCEDURE [dbo].[USP_Currency_UpdateExchangeRate]  
	@vcToCurrency VARCHAR(10),
	@decRate FLOAT,
	@vcDomains VARCHAR(MAX)
AS  
BEGIN 
	DECLARE @sql AS NVARCHAR(MAX)

	SET @sql = 'UPDATE Currency SET fltExchangeRate = ' + CAST(@decRate AS VARCHAR) + ' WHERE numDomainID IN (' + @vcDomains + ') AND chrCurrency = ''' + @vcToCurrency + ''' AND bitEnabled = 1'

	EXEC (@sql)
END