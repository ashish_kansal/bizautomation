set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go



   
-- =============================================  
-- Author:  <Ajit Kumar Singh>  
-- Create date: <Sunday, September 28, 2008>  
-- Description: <This procedure is used for fetching the record against DivisionID>  
-- =============================================  

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetContactDetails')
DROP PROCEDURE USP_GetContactDetails
GO
CREATE PROCEDURE [dbo].[USP_GetContactDetails]
@numDomainId as numeric(9),        
@numDivisionId as numeric(9)
as              
Begin      
	
	select numDivisionId, (vcFirstName + ' ' + vcLastName)as contactName,[numContactId],ISNULL(bitPrimaryContact,0) bitPrimaryContact from AdditionalContactsInformation
	where numDivisionId=@numDivisionId and numDomainID=@numDomainId;

End  


