SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ReportListMaster_CalculateComission')
DROP PROCEDURE USP_ReportListMaster_CalculateComission
GO
CREATE PROCEDURE [dbo].[USP_ReportListMaster_CalculateComission]
	@numDomainID NUMERIC(18,0)
AS
BEGIN 
	DECLARE @numComPayPeriodID NUMERIC(18,0)

	IF ISNULL(@numDomainID,0) > 0
	BEGIN
		IF EXISTS (SELECT numComPayPeriodID FROM CommissionPayPeriod WHERE numDomainID=@numDomainID AND tintPayPeriod=0)
		BEGIN
			UPDATE 
				CommissionPayPeriod
			SET 
				numModifiedBy = 0
				,dtModified = GETDATE()
				,dtStart = DATEADD(MONTH,-12,GETUTCDATE())
				,dtEnd = GETUTCDATE()
			WHERE
				numDomainID=@numDomainID AND numComPayPeriodID=@numDomainID*-1

				
			SET @numComPayPeriodID=@numDomainID*-1
		END
		-- RECALCULATE PAY PERIOD COMMISSION
		ELSE
		BEGIN
			INSERT INTO CommissionPayPeriod
			(
				numDomainID,
				dtStart,
				dtEnd,
				tintPayPeriod,
				numCreatedBy,
				dtCreated
			)
			VALUES
			(
				@numDomainID,
				DATEADD(MONTH,-12,GETUTCDATE()),
				GETUTCDATE(),
				0,
				0,
				GETDATE()
			)

			SET @numComPayPeriodID = SCOPE_IDENTITY()
		END

		--CALCULATE COMMISSION ENTERIES FOR PAID BIZDOC
		EXEC USP_CalculateCommissionPaidBizDoc @numComPayPeriodID=@numComPayPeriodID,@numDomainID=@numDomainID,@ClientTimeZoneOffset=0
		--CALCULATE COMMISSION ENTERIES FOR UNPAID OR PARTIALLY BIZDOC
		EXEC USP_CalculateCommissionUnPaidBizDoc @numComPayPeriodID=@numComPayPeriodID,@numDomainID=@numDomainID,@ClientTimeZoneOffset=0
	END
	ELSE
	BEGIN
		IF CAST((SELECT LastReindexDate FROM ElasticSearchLastReindex WHERE ID = 2) AS DATE) <> CAST(GETDATE() AS DATE) AND DATEPART(hh, GETDATE()) > 3
		BEGIN
			UPDATE ElasticSearchLastReindex SET LastReindexDate = GETDATE() WHERE ID = 2

			DECLARE @Temp TABLE
			(
				ID INT IDENTITY(1,1)
				,numDomainID NUMERIC(18,0)
			)

			INSERT INTO @Temp
			(
				numDomainID
			)
			SELECT
				numDomainId
			FROM
				Domain
			WHERE
				numDomainId > 0

			DECLARE @i AS INT = 1
			DECLARE @iCount AS INT
			DECLARE @TempDomainID INT
			SELECT @iCount = COUNT(*) FROM @Temp

			WHILE @i <= @iCount
			BEGIN
				SELECT @TempDomainID=numDomainID FROM @Temp WHERE ID=@i

				IF EXISTS (SELECT numComPayPeriodID FROM CommissionPayPeriod WHERE numDomainID=@TempDomainID AND tintPayPeriod=0)
				BEGIN
					UPDATE 
						CommissionPayPeriod
					SET 
						numModifiedBy = 0
						,dtModified = GETDATE()
						,dtStart = DATEADD(MONTH,-12,GETUTCDATE())
						,dtEnd =GETUTCDATE()
					WHERE
						numDomainID=@TempDomainID AND numComPayPeriodID=@TempDomainID*-1

				
					SET @numComPayPeriodID=@TempDomainID*-1
				END
				-- RECALCULATE PAY PERIOD COMMISSION
				ELSE
				BEGIN
					INSERT INTO CommissionPayPeriod
					(
						numDomainID,
						dtStart,
						dtEnd,
						tintPayPeriod,
						numCreatedBy,
						dtCreated
					)
					VALUES
					(
						@TempDomainID,
						DATEADD(MONTH,-12,GETUTCDATE()),
						GETUTCDATE(),
						0,
						0,
						GETDATE()
					)

					SET @numComPayPeriodID = SCOPE_IDENTITY()
				END

				--CALCULATE COMMISSION ENTERIES FOR PAID BIZDOC
				EXEC USP_CalculateCommissionPaidBizDoc @numComPayPeriodID=@numComPayPeriodID,@numDomainID=@TempDomainID,@ClientTimeZoneOffset=0
				--CALCULATE COMMISSION ENTERIES FOR UNPAID OR PARTIALLY BIZDOC
				EXEC USP_CalculateCommissionUnPaidBizDoc @numComPayPeriodID=@numComPayPeriodID,@numDomainID=@TempDomainID,@ClientTimeZoneOffset=0

				SET @i = @i + 1
			END
		END
	END
END
GO