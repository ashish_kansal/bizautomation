
/****** Object:  StoredProcedure [dbo].[USP_DeleteCartItem]    Script Date: 11/08/2011 17:28:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_DeleteCartItem' ) 
                    DROP PROCEDURE USP_DeleteCartItem
                    Go
CREATE PROCEDURE [dbo].[USP_DeleteCartItem]
(
    @numUserCntId NUMERIC(18, 0),
    @numDomainId NUMERIC(18, 0),
    @vcCookieId VARCHAR(100),
    @numCartId NUMERIC(18,0) = 0,
    @bitDeleteAll BIT=0,
	@numSiteID NUMERIC(18,0) = 0,
	@numItemCode NUMERIC(18,0) = 0
)
AS 
BEGIN
	IF @bitDeleteAll = 0
	BEGIN
		IF @numCartId <> 0 
		BEGIN
			-- REMOVE DISCOUNT APPLIED TO ALL ITEMS FROM PROMOTION TRIGGERED OF ITEM TO BE DELETED
			UPDATE 
				CartItems
			SET 
				PromotionDesc='',PromotionID=0,fltDiscount=0,monTotAmount=(monPrice*numUnitHour),monTotAmtBefDiscount=(monPrice*numUnitHour),vcCoupon=NULL,bitParentPromotion=0
			WHERE
				PromotionID=(SELECT TOP 1 PromotionID FROM CartItems WHERE numDomainId = @numDomainId AND numUserCntId = @numUserCntId AND numCartId = @numCartId AND vcCookieId=@vcCookieId AND ISNULL(bitParentPromotion,0)=1)
				AND numDomainId = @numDomainId
				AND numUserCntId = @numUserCntId
				AND vcCookieId=@vcCookieId
				AND ISNULL(bitParentPromotion,0)=0

			--DELETE FROM CartItems WHERE numDomainId = @numDomainId AND numUserCntId = @numUserCntId AND numItemCode = @numItemCode	

			DECLARE @numTempItemCode NUMERIC(18,0)
			SELECT @numTempItemCode=numItemCode FROM CartItems WHERE numCartId=@numCartId

			DELETE FROM CartItems 
			WHERE numDomainId = @numDomainId AND numUserCntId = @numUserCntId
				AND (numCartId = @numCartId 
					OR numItemCode IN (SELECT numItemCode FROM SimilarItems WHERE numParentItemCode = @numTempItemCode AND numDomainId = @numDomainId ))	--AND bitrequired = 1
		END
		ELSE IF ISNULL(@numItemCode,0) > 0
		BEGIN
			-- REMOVE DISCOUNT APPLIED TO ALL ITEMS FROM PROMOTION TRIGGERED OF ITEM TO BE DELETED
			UPDATE 
				CartItems
			SET 
				PromotionDesc='',PromotionID=0,fltDiscount=0,monTotAmount=(monPrice*numUnitHour),monTotAmtBefDiscount=(monPrice*numUnitHour),vcCoupon=NULL,bitParentPromotion=0
			WHERE
				PromotionID=(SELECT TOP 1 PromotionID FROM CartItems WHERE numDomainId = @numDomainId AND numUserCntId = @numUserCntId AND numItemCode = @numItemCode AND vcCookieId=@vcCookieId AND ISNULL(bitParentPromotion,0)=1)
				AND numDomainId = @numDomainId
				AND numUserCntId = @numUserCntId
				AND vcCookieId=@vcCookieId
				AND ISNULL(bitParentPromotion,0)=0

			DELETE FROM CartItems WHERE numDomainId = @numDomainId AND numUserCntId = @numUserCntId AND vcCookieId = @vcCookieId AND numItemCode=@numItemCode
		END
		ELSE
		BEGIN
			UPDATE 
				CartItems
			SET 
				PromotionDesc='',PromotionID=0,fltDiscount=0,monTotAmount=(monPrice*numUnitHour),monTotAmtBefDiscount=(monPrice*numUnitHour),vcCoupon=NULL,bitParentPromotion=0
			WHERE
				numDomainId = @numDomainId
				AND numUserCntId = @numUserCntId
				AND vcCookieId=@vcCookieId

			DELETE FROM CartItems 
			WHERE numDomainId = @numDomainId AND numUserCntId = 0 AND vcCookieId = @vcCookieId		
		END
	END
	ELSE
	BEGIN
		DELETE FROM CartItems WHERE numDomainId = @numDomainId AND numUserCntId = @numUserCntId	AND vcCookieId <> ''
	END

	EXEC USP_PromotionOffer_ApplyItemPromotionToECommerce @numDomainID,@numUserCntId,@numSiteID,@vcCookieId,0,''
END
GO
 



