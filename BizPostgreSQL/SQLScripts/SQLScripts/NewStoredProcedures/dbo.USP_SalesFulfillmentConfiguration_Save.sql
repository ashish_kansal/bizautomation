SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_SalesFulfillmentConfiguration_Save')
DROP PROCEDURE USP_SalesFulfillmentConfiguration_Save
GO
CREATE PROCEDURE [dbo].[USP_SalesFulfillmentConfiguration_Save]  
	@numDomainID NUMERIC(18,0),
	@numUserCntID NUMERIC(18,0),
	@bitActive BIT,
	@bitRule1IsActive BIT,
	@numRule1BizDoc NUMERIC(18,0),
	@tintRule1Type TINYINT,
	@numRule1OrderStatus NUMERIC(18,0),
	@bitRule2IsActive BIT,
	@numRule2OrderStatus NUMERIC(18,0),
	@numRule2SuccessOrderStatus NUMERIC(18,0),
	@numRule2FailOrderStatus NUMERIC(18,0),
	@bitRule3IsActive BIT,
	@numRule3OrderStatus NUMERIC(18,0),
	@numRule3FailOrderStatus NUMERIC(18,0),
	@bitRule4IsActive BIT,
	@numRule4OrderStatus NUMERIC(18,0),
	@numRule4FailOrderStatus NUMERIC(18,0),
	@bitRule5IsActive BIT,
	@numRule5OrderStatus NUMERIC(18,0),
	@bitRule6IsActive BIT,
	@numRule6OrderStatus NUMERIC(18,0)
AS  
BEGIN 
	IF EXISTS(SELECT numSFCID FROM SalesFulfillmentConfiguration WHERE numDomainID=@numDomainID)
	BEGIN
		UPDATE
			SalesFulfillmentConfiguration
		SET
			numDomainID=@numDomainID
			,bitActive=@bitActive
			,bitRule1IsActive=@bitRule1IsActive
			,numRule1BizDoc=@numRule1BizDoc
			,tintRule1Type=@tintRule1Type
			,numRule1OrderStatus=@numRule1OrderStatus
			,bitRule2IsActive=@bitRule2IsActive
			,numRule2OrderStatus=@numRule2OrderStatus
			,numRule2SuccessOrderStatus=@numRule2SuccessOrderStatus
			,numRule2FailOrderStatus=@numRule2FailOrderStatus
			,bitRule3IsActive=@bitRule3IsActive
			,numRule3OrderStatus=@numRule3OrderStatus
			,numRule3FailOrderStatus=@numRule3FailOrderStatus
			,bitRule4IsActive=@bitRule4IsActive
			,numRule4OrderStatus=@numRule4OrderStatus
			,numRule4FailOrderStatus=@numRule4FailOrderStatus
			,bitRule5IsActive=@bitRule5IsActive
			,numRule5OrderStatus=@numRule5OrderStatus
			,bitRule6IsActive=@bitRule6IsActive
			,numRule6OrderStatus=@numRule6OrderStatus
			,dtModified=GETUTCDATE()
			,numModifiedBy=@numUserCntID
		WHERE
			numDomainID=@numDomainID
	END
	ELSE
	BEGIN
		INSERT INTO SalesFulfillmentConfiguration
		(
			numDomainID
			,bitActive
			,bitRule1IsActive
			,numRule1BizDoc
			,tintRule1Type
			,numRule1OrderStatus
			,bitRule2IsActive
			,numRule2OrderStatus
			,numRule2SuccessOrderStatus
			,numRule2FailOrderStatus
			,bitRule3IsActive
			,numRule3OrderStatus
			,numRule3FailOrderStatus
			,bitRule4IsActive
			,numRule4OrderStatus
			,numRule4FailOrderStatus
			,bitRule5IsActive
			,numRule5OrderStatus
			,bitRule6IsActive
			,numRule6OrderStatus
			,dtCreated
			,numCreatedBy
		)
		VALUES
		(
			@numDomainID
			,@bitActive
			,@bitRule1IsActive
			,@numRule1BizDoc
			,@tintRule1Type
			,@numRule1OrderStatus
			,@bitRule2IsActive
			,@numRule2OrderStatus
			,@numRule2SuccessOrderStatus
			,@numRule2FailOrderStatus
			,@bitRule3IsActive
			,@numRule3OrderStatus
			,@numRule3FailOrderStatus
			,@bitRule4IsActive
			,@numRule4OrderStatus
			,@numRule4FailOrderStatus
			,@bitRule5IsActive
			,@numRule5OrderStatus
			,@bitRule6IsActive
			,@numRule6OrderStatus
			,GETUTCDATE()
			,@numUserCntID
		)
	END

END