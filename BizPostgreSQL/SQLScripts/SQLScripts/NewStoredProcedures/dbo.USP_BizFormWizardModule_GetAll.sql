GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_BizFormWizardModule_GetAll' ) 
    DROP PROCEDURE USP_BizFormWizardModule_GetAll
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author: Sandeep Patel
-- Create date: 21 July 2014
-- Description:	Gets list of bizform wizard modules
-- =============================================
CREATE PROCEDURE USP_BizFormWizardModule_GetAll
	@numDomainID NUMERIC(18,0)
AS
BEGIN
	
	SET NOCOUNT ON;

	IF (SELECT COUNT(*) FROM ListDetails WHERE numListID = 5 AND numDomainID = @numDomainID AND constFlag <> 1) > 0
		SELECT * FROM BizFormWizardModule WHERE ISNULL(bitActive,1)=1 ORDER BY tintSortOrder
	ELSE
		SELECT * FROM BizFormWizardModule WHERE ISNULL(bitActive,1)=1 ORDER BY tintSortOrder	
END
GO