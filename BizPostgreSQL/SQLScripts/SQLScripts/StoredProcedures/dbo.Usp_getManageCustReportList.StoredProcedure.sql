/****** Object:  StoredProcedure [dbo].[Usp_getManageCustReportList]    Script Date: 07/26/2008 16:17:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getmanagecustreportlist')
DROP PROCEDURE usp_getmanagecustreportlist
GO
CREATE PROCEDURE [dbo].[Usp_getManageCustReportList]        
@numDomainId as numeric(9) ,
@numGroupId as numeric(9) 
as                                                 
      
Select       
numCustomReportId as ReportId,vcReportName,vcReportDescription,
convert(bit,'false') as bitallowed
from customreport Cr
--left join  DashboardAllowedReports DAR on Cr.numCustomReportId=DAR.numReportId 
where numdomainid=@numDomainID and numCustomReportId not in 
(select numreportId from DashboardAllowedReports where numgrpId =  @numGroupId)
union
Select       
numCustomReportId as ReportId,vcReportName,vcReportDescription,
convert(bit,'true') as bitallowed
from customreport Cr
join  DashboardAllowedReports DAR on Cr.numCustomReportId=DAR.numReportId 
where numdomainid=@numDomainID and numgrpId = @numGroupId 

order by numCustomReportId
GO
