/****** Object:  StoredProcedure [dbo].[USP_GetGenDocList]    Script Date: 07/26/2008 16:17:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getgendoclist')
DROP PROCEDURE usp_getgendoclist
GO
CREATE PROCEDURE [dbo].[USP_GetGenDocList]    
@numDocCategory as numeric =0,
@numDomainID AS numeric
as  
if  @numDocCategory <>0 
begin
select numGenericDocid,vcdocname,* from genericdocuments where cURLType='L'  and  numDoccategory =  @numDocCategory AND tintDocumentType=1
AND numDomainID=@numDomainID
end
else
begin
select numGenericDocid,vcdocname,* from genericdocuments where cURLType='L' AND tintDocumentType=1
AND numDomainID=@numDomainID
end
GO
