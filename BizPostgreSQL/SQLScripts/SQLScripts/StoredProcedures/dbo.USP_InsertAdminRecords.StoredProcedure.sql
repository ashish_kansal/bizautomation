/****** Object:  StoredProcedure [dbo].[USP_InsertAdminRecords]    Script Date: 07/26/2008 16:18:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_insertadminrecords')
DROP PROCEDURE usp_insertadminrecords
GO
CREATE PROCEDURE [dbo].[USP_InsertAdminRecords]  
@vcCompanyName as varchar(100)='',  
@vcDomainName as varchar(50)=''  
as  
declare @strDomainID as varchar(15)  
declare @strCompanyID as varchar(15)  
declare @strDivisionID as Varchar(15)  
declare @strContactID as varchar(15)  
insert into domain (vcDomainName)  
values (@vcDomainName)  
set @strDomainID=@@identity  
  
insert  into CompanyInfo  
 (vcCompanyName,numCompanyType,numNoOfEmployeesID,numCompanyrating,numCompanyIndustry,numAnnualRevID,numCompanyCredit,numCreatedBy,bintCreatedDate,  
 numModifiedBy,bintModifiedDate,numDomainID)  
values   
 (@vcCompanyName,0,0,0,0,0,0,1,getutcdate(),1,getutcdate(),@strDomainID)  
  
set @strCompanyID=@@identity  
  
insert into DivisionMaster   
 (numCompanyID,vcDivisionName,numCreatedBy,bintCreatedDate,numModifiedBy,bintModifiedDate,tintCRMType,numDomainID,numGrpId,numRecOwner,numTerID)  
values  
 (@strCompanyID,'-',1,getutcdate(),1,getutcdate(),1,@strDomainID,0,1,0)  
  
set @strDivisionID=@@identity  
  
insert into AdditionalContactsInformation  
 (vcGivenName,vcFirstName,vcLastname,vcEmail,numPhone,numDivisionId,numCreatedBy,bintCreatedDate,numModifiedBy,bintModifiedDate,numDomainID,bintDOB
 )  
values   
 ('Administrator','Administrator','','','',@strDivisionID,1,getutcdate(),1,getutcdate(),@strDomainID,0)  
set @strContactID=@@identity  
insert into UserMaster  
 (vcUserName,vcUserDesc,numGroupID,numDomainID,numCreatedBy,bintCreatedDate,numModifiedBy,bintModifiedDate,bitActivateFlag,numUserDetailId)  
values ('Administrator','BACRM Admin',1,1,1,getutcdate(),1,getutcdate(),0,@strContactID)
GO
