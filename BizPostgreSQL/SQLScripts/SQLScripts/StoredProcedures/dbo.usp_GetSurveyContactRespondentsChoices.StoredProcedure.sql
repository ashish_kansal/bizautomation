/****** Object:  StoredProcedure [dbo].[usp_GetSurveyContactRespondentsChoices]    Script Date: 07/26/2008 16:18:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created By: Debasish Nag
--Purpose Created: To get the questions and answers pertaining to the Survey
--Modified Date: 3rd Mar 2006
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'usp_getsurveycontactrespondentschoices' ) 
    DROP PROCEDURE usp_getsurveycontactrespondentschoices
GO
CREATE PROCEDURE [dbo].[usp_GetSurveyContactRespondentsChoices]
    @numSurId NUMERIC,
    @numRespondentId NUMERIC
AS 
    BEGIN


        SELECT  sqm.vcQuestion,
                dbo.fn_SurveyContactRespondentsChoices(@numSurId,
                                                       @numRespondentId,
                                                       sr.numQuestionID) AS vcAnsLabel
        FROM    
			SurveyQuestionMaster sqm 
		INNER JOIN 
			SurveyAnsMaster sam 
		ON 
			sqm.numSurId = sam.numSurId 
		INNER JOIN 
			SurveyResponse sr
		ON 
			sr.numParentSurId = sam.numSurId 
			AND sr.numQuestionID = sqm.numQID
            AND sr.numQuestionID = sam.numQID
            AND sr.numAnsID = sam.numAnsID
        WHERE   
            sr.numSurId = @numSurId
            AND sr.numRespondantID = @numRespondentId
        GROUP BY 
			sr.numQuestionID,
            sqm.vcQuestion

    END
GO
