/****** Object:  StoredProcedure [dbo].[usp_GetProfileForCampaign]    Script Date: 07/26/2008 16:18:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getprofileforcampaign')
DROP PROCEDURE usp_getprofileforcampaign
GO
CREATE PROCEDURE [dbo].[usp_GetProfileForCampaign] 
	@numDomainID numeric(9)=0   
--
AS
BEGIN
	SELECT 
		vcProfile
				
	FROM 	
		CompanyInfo 
		where numDomainID=@numDomainID
		order by vcProfile
	
		
END
GO
