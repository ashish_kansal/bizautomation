/****** Object:  StoredProcedure [dbo].[USP_MasterList]    Script Date: 07/26/2008 16:20:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_masterlist')
DROP PROCEDURE usp_masterlist
GO
CREATE PROCEDURE [dbo].[USP_MasterList]      
(      
 @ListId numeric(9)=null,       
 @bitDeleted as bit=null      
)      
as      
begin      
        
  SELECT  Ld.numListItemID, vcData FROM listdetails LD  
 left join listorder LO on Ld.numListItemID= LO.numListItemID 
 WHERE  Ld.numListID=@ListId  order by intSortOrder, numListItemID       
      
end
GO
