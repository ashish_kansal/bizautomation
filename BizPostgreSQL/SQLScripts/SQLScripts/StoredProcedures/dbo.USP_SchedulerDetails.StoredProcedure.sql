/****** Object:  StoredProcedure [dbo].[USP_SchedulerDetails]    Script Date: 07/26/2008 16:21:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_schedulerdetails')
DROP PROCEDURE usp_schedulerdetails
GO
CREATE PROCEDURE [dbo].[USP_SchedulerDetails]    
@numScheduleId as numeric(9)=0    
As    
Begin    
Select * From CustRptScheduler Where numScheduleId=@numScheduleId    
End
GO
