/****** Object:  StoredProcedure [dbo].[usp_CreateDynamicLeadBoxEntry]    Script Date: 07/26/2008 16:15:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created By: Debasish Tapan Nag                            
--Purpose: Saves the Lead Box form entries in the database from an XML document                           
--Created Date: 08/03/2005                  
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_createdynamicleadboxentry')
DROP PROCEDURE usp_createdynamicleadboxentry
GO
CREATE PROCEDURE [dbo].[usp_CreateDynamicLeadBoxEntry]               
 @numDomainId numeric (9),                                   
 @numUserId numeric (9),                                         
 @strXML ntext              
AS    
BEGIN           
 DECLARE @numLeadBoxId numeric              
              
 Insert into DynamicLeadBoxFormDataMaster(numDomainId, numUserId, numCreatedDate)                
 values(@numDomainId, @numUserId, getutcdate())         
 Select @numLeadBoxId = SCOPE_IDENTITY()           
        
  If @@error <> 0 goto ERR_HANDLER                           
              
 DECLARE @iDoc int              
              
 EXECUTE sp_xml_preparedocument @iDoc OUTPUT, @strXML              
              
  INSERT INTO DynamicLeadBoxFormDataDetails(              
  numLeadBoxId,              
  vcDbColumnName,              
  vcFieldName,              
  vcDbColumnValue,         
  vcDbColumnValueText,              
  numRowNum,              
  numColumnNum,              
  vcAssociatedControlType,              
  boolAOIField              
  )      
  SELECT @numLeadBoxId, * FROM OpenXML(@iDoc, '/FormFields/FormField', 2)              
  WITH              
  (              
   vcDbColumnName VARCHAR(50),      
   vcFieldName VARCHAR(50),      
   vcDbColumnValue Numeric,      
   vcDbColumnValueText VARCHAR(50),      
   numRowNum Numeric,      
   numColumnNum Numeric,      
   vcAssociatedControlType VARCHAR(50),      
   boolAOIField Bit      
  )      
  If @@error <> 0 goto ERR_HANDLER                
              
  EXECUTE sp_xml_removedocument @iDoc           
           
 SELECT @numLeadBoxId            
          
            
ERR_HANDLER:     
 RETURN 0    
END
GO
