/****** Object:  StoredProcedure [dbo].[USP_MoveCopyItems]    Script Date: 07/26/2008 16:20:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_movecopyitems')
DROP PROCEDURE usp_movecopyitems
GO
CREATE PROCEDURE [dbo].[USP_MoveCopyItems]                      
@numEmailHstrID as numeric(9)                   ,
@numNodeId as numeric(9),
@ModeType as bit
as          
 if @ModeType = 0

begin
      
 DECLARE @NewEmailHstrID integer;                      
insert into    emailHistory (vcSubject,vcBody,bintCreatedOn,numNoofTimes,vcItemId,vcChangeKey,bitIsRead,vcSize,bitHasAttachments,  
 dtReceivedOn,tintType,chrSource,vcCategory,vcBodyText,numDomainID,numUserCntId,numUid,numNodeId,vcFrom,vcTO,numEMailId)              
              
select vcSubject,vcBody,bintCreatedOn,numNoofTimes,vcItemId,vcChangeKey,bitIsRead,vcSize,bitHasAttachments,dtReceivedOn,tintType,  
 chrSource,vcCategory,vcBodyText,numDomainID,numUserCntId,numUid ,@numNodeId,vcFrom,vcTO,numEMailId            
from emailHistory where numEmailHstrID=@numEmailHstrID              
                  
  SELECT              
   @NewEmailHstrID = SCOPE_IDENTITY();              
              
insert into    EmailHStrToBCCAndCC (numEmailHstrID,vcName,tintType,numDivisionId,numEmailId)              
select @NewEmailHstrID,vcName,tintType,numDivisionId,numEmailId from  EmailHStrToBCCAndCC where numEmailHstrID=@numEmailHstrID              
                    
insert into EmailHstrAttchDtls ( numEmailHstrID,vcFileName,vcAttachmentItemId,vcAttachmentType)              
select  @NewEmailHstrID,vcFileName,vcAttachmentItemId,vcAttachmentType  from EmailHstrAttchDtls where numEmailHstrID=@numEmailHstrID 

end

else
begin
	update emailHistory set numNodeId=@numNodeId  where numEmailHstrID=@numEmailHstrID 
end
GO
