/****** Object:  StoredProcedure [dbo].[GetVarianceKeyByItemId]    Script Date: 07/26/2008 16:14:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
**  Selects the  VariancesKey Of the Root Activity of the recurring series from ItemId of Exchange
*/
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='getvariancekeybyitemid')
DROP PROCEDURE getvariancekeybyitemid
GO
CREATE PROCEDURE [dbo].[GetVarianceKeyByItemId]
	@ItemId	varchar(250)	-- ItemId of the root Activity
	
AS
BEGIN
	
	
	SELECT 
		[Activity].[VarianceID],
		[Activity].[RecurrenceID]

	
	FROM 
		[Activity] 
	WHERE 
		  ( [Activity].[ItemID] = @ItemID ) AND
		  ( ( [Activity].[OriginalStartDateTimeUtc] IS NULL ) ) 
END
GO
