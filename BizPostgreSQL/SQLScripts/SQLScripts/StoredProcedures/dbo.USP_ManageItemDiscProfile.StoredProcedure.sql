/****** Object:  StoredProcedure [dbo].[USP_ManageItemDiscProfile]    Script Date: 07/26/2008 16:19:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_manageitemdiscprofile')
DROP PROCEDURE usp_manageitemdiscprofile
GO
CREATE PROCEDURE [dbo].[USP_ManageItemDiscProfile]
@byteMode as tinyint=0,
@numDiscProfileID as numeric(9),
@decDisc as decimal,
@bitApplicable as bit,
@numItemID as numeric(9)=0,
@numDivisionID as numeric(9)=0,
@numRelID as numeric(9)=0,
@numProID as numeric(9)=0

as

if @byteMode=0
begin

if @numDiscProfileID>0 
begin
update ItemDiscountProfile set decDisc=@decDisc,bitApplicable=@bitApplicable 
where numDiscProfileID=@numDiscProfileID
end
else
begin
insert ItemDiscountProfile(numItemID,numDivisionID,decDisc,bitApplicable)
values(@numItemID,@numDivisionID,@decDisc,@bitApplicable)
end

end
else
begin

if @numDiscProfileID>0 
begin
update ItemDiscountProfile set decDisc=@decDisc,bitApplicable=@bitApplicable 
where numDiscProfileID=@numDiscProfileID
end
else
begin
insert ItemDiscountProfile(numItemID,numRelID,numProID,decDisc,bitApplicable)
values(@numItemID,@numRelID,@numProID,@decDisc,@bitApplicable)
end
end
GO
