/****** Object:  StoredProcedure [dbo].[USP_GetReferringPage]    Script Date: 07/26/2008 16:18:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj                        
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getreferringpage')
DROP PROCEDURE usp_getreferringpage
GO
CREATE PROCEDURE [dbo].[USP_GetReferringPage]                        
@ReferringPage as varchar(100),                        
@searchTerm as varchar(100),                        
@From datetime,                        
@To datetime,                        
@CurrentPage int,                                
@PageSize int,                                
@TotRecs int output,                                
@columnName as Varchar(50),                                
@columnSortOrder as Varchar(10),                
@strCrawler as varchar(5)='',              
@MinimumPages as varchar(5),        
@numDomainID as numeric(9),               
@ClientOffsetTime as integer                          
as                                
                                
--Create a Temporary table to hold data                                
Create table #tempTable                         
    ( ID INT IDENTITY PRIMARY KEY,                                 
      numTrackingID numeric(9),                        
      vcUserHostAddress VARCHAR(100),                                
      vcUserDomain varchar(100),                                
      vcURL VARCHAR(500),                               
      vcReferrer varchar(500),                               
      vcSearchTerm varchar(100),                                
      numVisits numeric,                                
      numVistedPages numeric(9),                                
      vcTotalTime varchar(8),                      
      vcCountry  varchar(50)                              
      )                                
                        
                        
                             
declare @strSql as varchar(5000)                                
set @strSql='select THDR.numTrackingID as numTrackingID,THDR.vcUserHostAddress as vcUserHostAddress,                        
case when THDR.vcUserDomain is null then THDR.vcUserHostAddress when THDR.vcUserDomain = ''-'' then THDR.vcUserHostAddress    
else THDR.vcUserDomain end   as vcUserDomain,      
isnull(THDR.vcURL,''-'') as vcURL,                        
isnull(THDR.vcReferrer,''-'') as vcReferrer,                        
isnull(THDR.vcSearchTerm,''-'') as vcSearchTerm,THDR.numVisits,THDR.numVistedPages,convert(varchar(8),THDR.vcTotalTime) as vcTotalTime,isnull(THDR.vcCountry,''-'') as vcCountry                         
from TrackingVisitorsHDR THDR             
            
where numDomainId='+convert(varchar(10),@numDomainID)+' and  
(THDR.dtCreated between '''+convert(varchar(50),dateadd(minute,@ClientOffsetTime,@From))

+''' and '''+convert(varchar(50),dateadd(minute,@ClientOffsetTime,@To))+''')and THDR.numVistedPages >= '''
+@MinimumPages+''''            
              
                    
                    
if (@searchTerm<>'' and @searchTerm is not null)                        
begin                        
set @strSql=@strSql + ' and THDR.vcSearchTerm like ''%'+@searchTerm+'%'''                         
end                 
if @strCrawler<>''  set @strSql=@strSql + ' and THDR.vcCrawler= '''+@strCrawler+''''                   
if (@ReferringPage<>'' and @ReferringPage is not null)                        
begin                        
set @strSql=@strSql + ' and THDR.vcOrginalRef like ''%'+@ReferringPage+'%'''                         
end                                 
 set  @strSql=@strSql +' ORDER BY ' + @columnName +' '+ @columnSortOrder                              
print @strSql                          
insert into #tempTable (numTrackingID,vcUserHostAddress ,                                
      vcUserDomain ,                                
      vcURL ,                               
      vcReferrer,                               
      vcSearchTerm ,                                
      numVisits,                                
      numVistedPages,                                
      vcTotalTime,vcCountry)                                
exec( @strSql)                                
  declare @firstRec as integer                                
  declare @lastRec as integer                 
 set @firstRec= (@CurrentPage-1) * @PageSize                                
     set @lastRec= (@CurrentPage*@PageSize+1)                                
  select  numTrackingID,                  
   vcUserHostAddress,                   
   vcUserDomain ,                  
         vcURL ,                               
         vcReferrer,                               
         vcSearchTerm ,                                
         numVisits,                                
         numVistedPages,                                
         vcTotalTime,                  
   vcCountry,                  
   CASE WHEN len(vcURL)> 45 then left(vcURL,45)+'..' else vcURL end as URL,                  
                        CASE WHEN len(vcReferrer)> 45 then left(vcReferrer,45)+'..' else vcReferrer end as Referrer                  
 from #tempTable where ID > @firstRec and ID < @lastRec                           
set @TotRecs=(select count(*) from #tempTable)                                
drop table #tempTable
GO
