/****** Object:  StoredProcedure [dbo].[USP_SaveCashCreditCardRecurringDetails]    Script Date: 07/26/2008 16:20:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created By Siva    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_savecashcreditcardrecurringdetails')
DROP PROCEDURE usp_savecashcreditcardrecurringdetails
GO
CREATE PROCEDURE [dbo].[USP_SaveCashCreditCardRecurringDetails]    
@numCashCreditCardId as numeric(9)=0,    
@dtRecurringDate as datetime    
As    
Begin    
	Insert into CashCreditCardRecurringDetails (numCashCreditCardId,dtRecurringDate) Values(@numCashCreditCardId,@dtRecurringDate)    
End
GO
