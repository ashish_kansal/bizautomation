/****** Object:  StoredProcedure [dbo].[Activity_SelByDate]    Script Date: 07/26/2008 16:14:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*  
**  Selects the set of Activities having the given primary Resource  
**  starting within a given date range.  
**  
**  Use this stored procedure to access the subset of Activities  
**  belonging to a Resource by their name during a limited time  
**  span.  
*/  
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='activity_selbydate')
DROP PROCEDURE activity_selbydate
GO
CREATE PROCEDURE [dbo].[Activity_SelByDate]  
 @StartDateTimeUtc datetime, -- the start date before which no activities are retrieved  
 @EndDateTimeUtc datetime, -- the end date, any activities starting after this date are excluded  
 @OrganizerName nvarchar(64) -- resource name (will be used to lookup ID)  
AS  
BEGIN  
 --  
 -- First step is to get the ResourceID for the primary resource name supplied.  
 --  
 DECLARE @ResourceID AS integer;
if isnumeric(@OrganizerName) =1
begin
   set @ResourceID=convert(numeric(9), @OrganizerName)
end
else
begin 
	set @ResourceID =-999	
end 
-- SELECT  
--  @ResourceID = [Resource].[ResourceID]  
-- FROM  
--  [Resource]  
-- WHERE  
--  ( [Resource].[ResourceName] = @OrganizerName );  
 --  
 -- Second step is to retrieve the Activities organized (or owned by) this resource.  
 -- They will be the ones in the ActivityResource table where ResourceID is the  
 -- key number I've just identified.  
 --  
 SELECT   
  [Activity].[AllDayEvent],   
  [Activity].[ActivityDescription],   
  [Activity].[Duration],   
  [Activity].[Location],   
  [Activity].[ActivityID],   
  [Activity].[StartDateTimeUtc],   
  [Activity].[Subject],   
  [Activity].[EnableReminder],   
  [Activity].[ReminderInterval],  
  [Activity].[ShowTimeAs],  
  [Activity].[Importance],  
  [Activity].[Status],  
  [Activity].[RecurrenceID],  
  [Activity].[VarianceID],  
  [Activity].[_ts],  
  [Activity].[ItemId],  
  [Activity].[ChangeKey],isnull(Activity.Color,0) as Color,
 ISNULL(LastSnoozDateTimeUtc,'1900-01-01 00:00:00') AS LastSnoozDateTimeUtc
   
 FROM   
  [Activity] INNER JOIN [ActivityResource] ON [Activity].[ActivityID] = [ActivityResource].[ActivityID]  
 WHERE   
  (([Activity].[ActivityID] = [ActivityResource].[ActivityID]) AND ([ActivityResource].[ResourceID] = @ResourceID)) AND (((( [Activity].[StartDateTimeUtc] >= @StartDateTimeUtc ) AND ( [Activity].[StartDateTimeUtc] < @EndDateTimeUtc ))
 OR ( [Activity].[RecurrenceID] <> -999 )) AND ([Activity].[OriginalStartDateTimeUtc] IS NULL));  
END
GO
