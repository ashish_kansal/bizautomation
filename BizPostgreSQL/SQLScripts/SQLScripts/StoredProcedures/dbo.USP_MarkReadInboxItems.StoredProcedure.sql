/****** Object:  StoredProcedure [dbo].[USP_MarkReadInboxItems]    Script Date: 07/26/2008 16:20:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_markreadinboxitems')
DROP PROCEDURE usp_markreadinboxitems
GO
CREATE PROCEDURE [dbo].[USP_MarkReadInboxItems] 
@numEmailHstrID as numeric(9)       
      
as        
        
update emailHistory set bitIsRead = 1          
where numEmailHstrID=@numEmailHstrID
GO
