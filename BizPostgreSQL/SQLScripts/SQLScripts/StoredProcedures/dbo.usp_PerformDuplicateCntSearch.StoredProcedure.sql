/****** Object:  StoredProcedure [dbo].[usp_PerformDuplicateCntSearch]    Script Date: 07/26/2008 16:20:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created By: Debasish Nag                                                  
--Created On: 13th Jan 2006                                        
--Purpose: Query for Duplicate Contacts                                               
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_performduplicatecntsearch')
DROP PROCEDURE usp_performduplicatecntsearch
GO
CREATE PROCEDURE [dbo].[usp_PerformDuplicateCntSearch]                        
@vcCRMTypes NVarChar(5),                                                  
@numCompanyType Numeric,                                                  
@vcOrgCreationStartDateRange NVarchar(10),                                                  
@vcOrgCreationEndDateRange NVarchar(10),                                                  
@boolNoItemHistory Bit,                                                  
@boolSameParentName Bit,                                                  
@boolSameDivisionName Bit,                                        
@boolSamePhoneFlag Bit,                                        
@boolIgnoreNamesFlag Bit,                                                  
@vcFirstName NVarChar(50),                                        
@vcLastName NVarChar(50),                                        
@vcMatchingField NVarChar(50),                                            
@vcSortColumn NVarchar(50),                                         
@vcSortOrder NVarchar(50),                                    
@numNosRowsReturned Int,                                       
@numCurrentPage Int,                                                  
@numDomainID Numeric                                          
AS                                                  
BEGIN                                                    
   --SET THE RANGE OF RECORDS                      
   DECLARE @intTopOfResultSet Int                                    
   SET @intTopOfResultSet = (@numNosRowsReturned * @numCurrentPage)                    
   DECLARE @intStartOfResultSet Int                                    
   SET @intStartOfResultSet = @numNosRowsReturned * (@numCurrentPage - 1)                       
   SET @vcSortColumn = Replace(@vcSortColumn,'[Date/Time  Created Last Modified & By Whom]', '4')                      
   SET @vcSortColumn = Replace(@vcSortColumn,'[First / Last Name]', '5')                      
   SET @vcSortColumn = Replace(@vcSortColumn,'[Phone Number]', '6')                      
   SET @vcSortColumn = Replace(@vcSortColumn,'[Cell Number]', '7')                      
   SET @vcSortColumn = Replace(@vcSortColumn,'[Email Address]', '8')                      
   SET @vcSortColumn = Replace(@vcSortColumn,'[Zip/Postal]', '9')                      
   SET @vcSortColumn = Replace(@vcSortColumn,'[Has open action items]', '10')                      
   SET @vcSortColumn = Replace(@vcSortColumn,'[Has action item history]', '11')                      
   SET @vcSortColumn = Replace(@vcSortColumn,'[Parent Organization/ Division]', '12')                      
                       
   --FIND THE CUSTOM FIELDS FOR THE CONTACT                        
   DECLARE @ContTempCustomTableColumns NVarchar(4000)                                              
   SELECT @ContTempCustomTableColumns = dbo.fn_OrgAndContCustomFieldsColumns(4,0)                        
   --PRINT '@ContTempCustomTableColumns: ' +  @ContTempCustomTableColumns                        
   --CREATE A STRING OF FIELDS WHICH ARE TO BE COMPARED                        
   DECLARE @vcOrgCompanyName NVarchar(20)                        
   DECLARE @vcOrgDivisionName NVarchar(35)                        
   DECLARE @vcContPhone NVarchar(20)                        
   DECLARE @vcContEmail NVarchar(20)                        
   DECLARE @vcContFirstName NVarchar(20)                        
   DECLARE @vcContLastName NVarchar(20)                        
   DECLARE @vcContMatchingFieldName NVarchar(25)                        
                        
   IF @boolSameParentName = 1 --Same Parent Organization                        

    SET @vcOrgCompanyName = 'vcOrgCompanyName,'                        
   ELSE                        
    SET @vcOrgCompanyName = ''                        
                                                  
   IF @boolSameDivisionName = 1  --Same Division name                        
    SET @vcOrgDivisionName = 'vcOrgDivisionName,numDivisionId,'                        
   ELSE        
    SET @vcOrgDivisionName = ''                        
                        
   IF @boolSamePhoneFlag = 1  --Same Contact Phone number                        
    SET @vcContPhone = 'vcContPhone,'                        
   ELSE                        
    SET @vcContPhone = ''                        
                                  
   SET @vcContFirstName = 'vcContFirstName,'                        
   SET @vcContLastName = 'vcContLastName,'                     
   SET @vcContEmail = 'vcContEmail,'                        
                        
   IF @vcMatchingField <> '' --Other Matching Fields                        
    SET @vcContMatchingFieldName = @vcMatchingField + ','                        
   ELSE                        
    SET @vcContMatchingFieldName = ''                        
   DECLARE @vcComparisionFieldsNames NVarchar(200)                        
   SET @vcComparisionFieldsNames = @vcContFirstName + @vcContLastName + @vcContEmail                    
   IF @boolSameParentName = 1                         
   SET @vcComparisionFieldsNames = @vcComparisionFieldsNames + @vcOrgCompanyName                        
   IF @boolSameDivisionName = 1                        
     SET @vcComparisionFieldsNames = @vcComparisionFieldsNames + @vcOrgDivisionName                        
   IF @boolSamePhoneFlag = 1                         
     SET @vcComparisionFieldsNames = @vcComparisionFieldsNames + @vcContPhone                        
      
   PRINT @vcMatchingField      
   PRINT dbo.fn_OrgAndContCustomFieldsColumns(4,1)      
   PRINT @vcComparisionFieldsNames      
   IF @vcMatchingField <> '' AND (CHARINDEX(@vcMatchingField,@vcComparisionFieldsNames) <= 0 AND CHARINDEX(@vcMatchingField, IsNull(dbo.fn_OrgAndContCustomFieldsColumns(4,1),'')) <= 0)        
     SET @vcComparisionFieldsNames = @vcComparisionFieldsNames + @vcContMatchingFieldName                        
                                                              
   SET @vcComparisionFieldsNames = SUBSTRING(@vcComparisionFieldsNames, 1, LEN(@vcComparisionFieldsNames)-1)                        
   --PRINT '@vcComparisionFieldsNames: ' + @vcComparisionFieldsNames                        
                        
   DECLARE @ContTempCustomTableDataEntryQuery NVarchar(4000)                         
                        
   SET @ContTempCustomTableDataEntryQuery = ' SELECT DISTINCT TOP 100 PERCENT numContactId' + ',' + @vcComparisionFieldsNames                        
        
    IF (@ContTempCustomTableColumns Is Not NULL)                                 
     SET @ContTempCustomTableDataEntryQuery = @ContTempCustomTableDataEntryQuery + ', ' + dbo.fn_OrgAndContCustomFieldsColumns(4,2)                                      
                        
    SET @ContTempCustomTableDataEntryQuery = @ContTempCustomTableDataEntryQuery + ' FROM vw_Records4Duplicates ORDER BY numContactId'                              
                                
    SET @ContTempCustomTableDataEntryQuery = 'Select TOP 100 PERCENT * INTO #tmpTableContactTempFlds FROM (' + @ContTempCustomTableDataEntryQuery + ') AS tmpTableCustomFlds'                          
                      
                                    
DECLARE @DuplicateCntFixedQuery varchar(8000)                                                  
SET @DuplicateCntFixedQuery = 'SELECT Distinct TOP 100 Percent Rec1.numCompanyId, Rec1.numDivisionId, Rec1.numContactId,                                        
 dbo.fn_GetLatestEntityEditedDateAndUser(0, Rec1.numContactId, ''Cnt'') AS [Date/Time  Created Last Modified & By Whom],                                            
 LTRIM(Rec1.vcContFirstName) + Case WHEN (LTrim(Rec1.vcContFirstName) <> '''' AND LTrim(Rec1.vcContLastName) <> '''') THEN '', '' ELSE '''' END + Rec1.vcContLastName As [First / Last Name],                                        
  Rec1.vcContPhone AS [Phone Number],                                              
  Rec1.vcContCell AS [Cell Number],                                             
  Rec1.vcContEmail AS [Email Address],                                        
  Rec1.vcContPostalCode as [Zip/Postal],                                         
  IsNull(recActionItem.boolActionItemOpen,''No'') AS [Has open action items],                                              
  IsNull(recActionItem.boolActionItemHistory,''No'') AS [Has action item history],                                         
  LTRIM(Rec1.vcOrgCompanyName + ''/'' + Rec1.vcOrgDivisionName) AS [Parent Organization/ Division]                                        
 FROM    vw_Records4Duplicates Rec1 INNER JOIN #tmpTableContactTempFlds recCustom ON Rec1.numContactId = recCustom.numContactId                        
  LEFT OUTER JOIN                                               
  (SELECT numContactId, Case WHEN (SUM(numStatus) > 0 AND (Convert(Int, SUM(numStatus)) % 356) = 0) THEN ''Yes'' ELSE ''No'' END AS boolActionItemOpen,                                               
   Case WHEN SUM(Cast(bitClosedFlag As Int)) > 0 THEN ''Yes'' ELSE ''No'' END AS boolActionItemHistory                                               
   FROM Communication GROUP BY numContactId                                            
  ) recActionItem                                              
    ON Rec1.numContactId = recActionItem.numContactId INNER JOIN                                                
   #tmpTableContactTempFlds Rec2                                            
   ON (Rec1.numContactId <> Rec2.numContactId AND Rec1.vcContFirstName + Rec1.vcContLastName = Rec2.vcContFirstName + Rec2.vcContLastName)                                 
   AND Rec1.numEmpStatus = 658 AND Rec1.numDomainID = ' + Cast(@numDomainID AS NVarchar)  
--PRINT @DuplicateCntFixedQuery                                                  
                                                  
DECLARE @DuplicateCntCRMTypesQuery NVarchar(60)                                                  
SET @DuplicateCntCRMTypesQuery = ' WHERE Convert(NVarchar(1), Rec1.vcContCRMType) IN (' + @vcCRMTypes + ') '                                                  
--PRINT @DuplicateCntCRMTypesQuery                                                  
                                                  
DECLARE @DuplicateCompanyTypeQuery NVarchar(50)                                                  
SET @DuplicateCompanyTypeQuery = ''                        
IF @numCompanyType <> 0                                                  
SET @DuplicateCompanyTypeQuery = ' AND Rec1.vcOrgCompanyType = ' + Convert(NVarchar, @numCompanyType)                                                  
--PRINT @DuplicateCompanyTypeQuery                                                  
                                                  
DECLARE @DuplicateDateRangeQuery NVarchar(100)                        
SET @DuplicateDateRangeQuery = ''                        
IF LTRIM(@vcOrgCreationStartDateRange) <> '' AND LTrim(@vcOrgCreationEndDateRange) <> ''                                                  
SET @DuplicateDateRangeQuery = ' AND (Rec1.vcContCreatedDate >= ''' + @vcOrgCreationStartDateRange + '''' +             
    ' AND Rec1.vcContCreatedDate <= ''' + @vcOrgCreationEndDateRange + ''')'                                                  
ELSE IF LTRIM(@vcOrgCreationStartDateRange) <> '' AND LTrim(@vcOrgCreationEndDateRange) = ''                                                  
SET @DuplicateDateRangeQuery = ' AND (Rec1.vcContCreatedDate >= ''' + @vcOrgCreationStartDateRange + ''') '                                                  
ELSE IF LTRIM(@vcOrgCreationStartDateRange) = '' AND LTrim(@vcOrgCreationEndDateRange) <> ''                                                  
SET @DuplicateDateRangeQuery = ' AND (Rec1.vcContCreatedDate <= ''' + @vcOrgCreationEndDateRange + ''') '                          
--PRINT @DuplicateDateRangeQuery                                                  
                                                  
                                                  
DECLARE @DuplicateItemHistoryQuery NVarchar(100)                                                 
SET @DuplicateItemHistoryQuery = ''                                                 
IF @boolNoItemHistory = 1                                                  
SET @DuplicateItemHistoryQuery = ' AND (recActionItem.boolActionItemHistory =  ''No'' OR recActionItem.boolActionItemHistory IS NULL) '                                                  
--PRINT @DuplicateItemHistoryQuery                                                  
                                                  
                                                
DECLARE @DuplicateParentOrgNameQuery NVarchar(200)                                                  
SET @DuplicateParentOrgNameQuery = ''                                                
IF @boolSameParentName = 1                                               
SET @DuplicateParentOrgNameQuery = ' AND Rec1.vcOrgCompanyName = Rec2.vcOrgCompanyName                                                
 AND Rec1.vcContEmail = Rec2.vcContEmail '                                                
--PRINT @DuplicateParentOrgNameQuery                                             
                                        
DECLARE @DuplicateDivisionNameQuery NVarchar(200)                                                  
SET @DuplicateDivisionNameQuery = ''                                                
IF @boolSameDivisionName = 1                                                
SET @DuplicateDivisionNameQuery = ' AND Rec1.vcOrgDivisionName = Rec2.vcOrgDivisionName                                                
 AND Rec1.numDivisionId <> Rec2.numDivisionId '                                                
--PRINT @DuplicateDivisionNameQuery                                                  
                                            
                             
DECLARE @DuplicatePhoneQuery NVarchar(150)                                                
SET @DuplicatePhoneQuery = ''                                                
IF @boolSamePhoneFlag = 1                                                
SET @DuplicatePhoneQuery = ' AND Rec1.vcContPhone = Rec2.vcContPhone '                                                  
--PRINT @DuplicatePhoneQuery                                                      
                                            
                                    
DECLARE @IgnoreNamesQuery NVarchar(150)                                                
SET @IgnoreNamesQuery = ''                                                
IF @boolIgnoreNamesFlag = 1                                                
SET @IgnoreNamesQuery = Case WHEN (LTrim(@vcFirstName) <> '' AND LTrim(@vcLastName) <> '') THEN ' AND Rec1.vcContFirstName + Rec1.vcContLastName <> ''' + @vcFirstName + @vcLastName + ''''        
                        WHEN (LTrim(@vcFirstName) <> '') THEN ' AND Rec1.vcContFirstName <> ''' + @vcFirstName + ''''        
                        WHEN (LTrim(@vcLastName) <> '') THEN ' AND Rec1.vcContLastName <> ''' + @vcLastName + ''''         
                        ELSE '' END        
--PRINT @IgnoreNamesQuery                                                     
                                                  
                                                
DECLARE @DuplicateMatchingFieldQuery NVarchar(150)                                                  
SET @DuplicateMatchingFieldQuery = ''                                                
IF @vcMatchingField <> ''                         
BEGIN                                 
IF @ContTempCustomTableColumns IS NOT NULL AND PATINDEX(@ContTempCustomTableColumns, @vcMatchingField) > -1                                          
 SET @DuplicateMatchingFieldQuery = ' AND recCustom.' + @vcMatchingField + ' = Rec2.' + @vcMatchingField + ' '                                                 
ELSE                                  
 SET @DuplicateMatchingFieldQuery = ' AND Rec1.' + @vcMatchingField + ' = Rec2.' + @vcMatchingField + ' '                                                 
END                        
--PRINT @DuplicateMatchingFieldQuery                                                  
                                    
                                                
DECLARE @GroupOrderColumnFieldQuery NVarchar(1000)                                                  
SET @GroupOrderColumnFieldQuery = ' ORDER BY ' + @vcSortColumn + ' ' + @vcSortOrder                                              
--PRINT @GroupOrderColumnFieldQuery                
--PRINT (@DuplicateCntFixedQuery + @DuplicateCntCRMTypesQuery + @DuplicateCompanyTypeQuery + @DuplicateDateRangeQuery + @DuplicateItemHistoryQuery + @DuplicateParentOrgNameQuery + @DuplicateDivisionNameQuery +  @DuplicatePhoneQuery +                      
  
   
      
         
         
             
             
                
                  
-- @IgnoreNamesQuery + @DuplicateMatchingFieldQuery + @GroupOrderColumnFieldQuery)                                                
                      
DECLARE @vcDuplicateCompleteQuery NVarchar(4000)                      
SET @vcDuplicateCompleteQuery = @DuplicateCntFixedQuery + @DuplicateCntCRMTypesQuery + @DuplicateCompanyTypeQuery + @DuplicateDateRangeQuery + @DuplicateItemHistoryQuery + @DuplicateParentOrgNameQuery + @DuplicateDivisionNameQuery +  @DuplicatePhoneQuery 
  
    
      
        
          
            
              
                
                 
 +                                       
 @IgnoreNamesQuery + @DuplicateMatchingFieldQuery + @GroupOrderColumnFieldQuery                      
                      
SET @vcDuplicateCompleteQuery = Char(10) + 'Select IDENTITY(int) AS numRow, * INTO #tmpTableDuplicateContact FROM (' + @vcDuplicateCompleteQuery + ') AS tmpTableAllFields'                      
                      
DECLARE @vcDuplicateCntSelect  NVarchar(4000)                      
DECLARE @vcDuplicateTableDelete  NVarchar(200)                      
DECLARE @vcDuplicateCountQuery NVarchar(200)                    
SET @vcDuplicateCountQuery = Char(10) + 'SELECT IsNull(Max(numRow),0) AS DupCount FROM #tmpTableDuplicateContact'                
SET @vcDuplicateCntSelect = @ContTempCustomTableDataEntryQuery + @vcDuplicateCompleteQuery + Char(10) + 'SELECT * FROM #tmpTableDuplicateContact WHERE numRow > ' + CAST(@intStartOfResultSet AS NVarchar(9)) +  ' AND numRow <= ' + CAST(@intTopOfResultSet AS
  
    
      
        
            
              
 NVarchar(9)) + ' ORDER BY ' + Cast(CONVERT(Int, @vcSortColumn) + 1 As NVarchar) + ' ' + @vcSortOrder + @vcDuplicateCountQuery                    
SET @vcDuplicateTableDelete = Char(10) + 'DROP TABLE #tmpTableContactTempFlds' + Char(10) + 'DROP TABLE #tmpTableDuplicateContact'                      
PRINT @vcDuplicateCntSelect + @vcDuplicateTableDelete                      
EXECUTE(@vcDuplicateCntSelect + @vcDuplicateTableDelete)                      
END
GO
