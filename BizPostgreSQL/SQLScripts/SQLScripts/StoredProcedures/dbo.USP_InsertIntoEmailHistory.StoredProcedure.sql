/****** Object:  StoredProcedure [dbo].[USP_InsertIntoEmailHistory]    Script Date: 07/26/2008 16:19:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created BY ANoop Jayaraj                          
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_insertintoemailhistory')
DROP PROCEDURE usp_insertintoemailhistory
GO
CREATE PROCEDURE [dbo].[USP_InsertIntoEmailHistory]                          
@vcMessageTo as varchar(5000),                     
@vcMessageToName as varchar(1000)='',              
@vcMessageFrom as varchar(5000),               
@vcMessageFromName as varchar(100)='',              
@vcSubject as varchar(500),                          
@vcBody as text,                       
@vcCC as varchar(5000),                        
@vcCCName as varchar(2000)='',                        
@vcBCC as varchar(5000)='',              
@bitHasAttachment as bit = 0,              
@vcItemId as varchar(1000)='',              
@vcChangeKey as varchar(1000)='',              
@bitIsRead as bit=1,              
@vcSize as numeric=0,              
@chrSource as char(1)= 'O',              
@tinttype as numeric =1,              
@vcCategory as varchar(100)='',          
@vcAttachmentType as varchar(500)= '',          
@vcFileName as varchar (500)= '',              
@vcAttachmentItemId as varchar(1000)=''  ,        
@dtReceived  as datetime  ,      
@numDomainId as numeric  ,    
@vcBodyText as text  ,  
@numUserCntId as numeric(9),
@IsReplied	BIT = FALSE,
@numRepliedMailID	NUMERIC(18) = 0,
@bitInvisible BIT = 0,
@numOppBizDocID NUMERIC(18,0)
AS
BEGIN                
	DECLARE @Identity AS NUMERIC(9)                
	DECLARE @FromName AS VARCHAR(1000)                
	DECLARE @FromEmailAdd AS VARCHAR(1000)                
	DECLARE @startPos AS INTEGER                
	DECLARE @EndPos AS INTEGER                
	DECLARE @EndPos1 AS INTEGER                
                
	SET @startPos=CHARINDEX( '<', @vcMessageFrom)                
	SET @EndPos=CHARINDEX( '>', @vcMessageFrom)
	    
	IF @startPos>0                 
	BEGIN                
		set @FromEmailAdd=  substring(@vcMessageFrom,@startPos+1,@EndPos-@startPos-1)                
		set @startPos=CHARINDEX( '"', @vcMessageFrom)                
		set @EndPos1=CHARINDEX( '"', substring(@vcMessageFrom,@startPos+1,len(@vcMessageFrom)))                
		set @FromName= substring(@vcMessageFrom,@startPos+1,@EndPos1-1)                                           
	END
	ELSE                 
	BEGIN                
		SET @FromEmailAdd=@vcMessageFrom                
		SET @FromName=@vcMessageFromName                
	END                
	
	DECLARE @updateinsert AS BIT
    
	IF @vcItemId = ''    
	BEGIN    
		SET @updateinsert  = 1    
	END    
	ELSE    
	BEGIN    
		IF (SELECT COUNT(*) FROM emailhistory WHERE vcItemId =@vcItemId) > 0    
		BEGIN    
			SET @updateinsert  = 0    
		END    
		ELSE    
		BEGIN    
			SET @updateinsert  = 1    
		END    
	END    
                  
	IF @updateinsert =0    
	BEGIN              
		UPDATE 
			EmailHistory            
		SET            
			vcSubject=@vcSubject,            
			vcBody=@vcBody,      
			vcBodyText=dbo.fn_StripHTML(@vcBodyText),          
			bintCreatedOn=getutcdate(),            
			bitHasAttachments=@bitHasAttachment,            
			vcItemId=@vcItemId,            
			vcChangeKey=@vcChangeKey,            
			bitIsRead=@bitIsRead,            
			vcSize=convert(varchar(200),@vcSize),            
			chrSource=@chrSource,            
			tinttype=@tinttype,            
			vcCategory =@vcCategory ,        
			dtReceivedOn=@dtReceived --,IsReplied = @IsReplied           
		WHERE 
			vcItemId = @vcItemId 
			AND numDomainid= @numDomainId
	END
	ELSE             
	BEGIN
		DECLARE @node AS NUMERIC(18,0)
		set @node = case when @vcCategory = 'B' then ISNULL((SELECT numNodeID FROM InboxTreeSort WHERE numDomainID=@numDomainId AND numUserCntID=@numUserCntId AND bitSystem = 1 AND numFixID = 4),0) else 0 end 
		SET @node=(SELECT TOP 1 numNodeID FROM InboxTreeSort WHERE numDomainId=@numDomainId AND numUserCntID=@numUserCntId AND vcNodeName='Sent Mail')
		SET @vcMessageTo = @vcMessageTo + '#^#'
		SET @vcMessageFrom = @vcMessageFrom + '#^#'
		SET @vcCC = @vcCC + '#^#'
		SET @vcBCC = @vcBCC + '#^#'

		EXECUTE dbo.USP_InsertEmailMaster @vcMessageTo ,@numDomainID
		EXECUTE dbo.USP_InsertEmailMaster @vcMessageFrom ,@numDomainID
		EXECUTE dbo.USP_InsertEmailMaster @vcCC ,@numDomainID
		EXECUTE dbo.USP_InsertEmailMaster @vcBCC ,@numDomainID 

		DECLARE @numNodeId AS NUMERIC(18,0)=0
		IF((SELECT COUNT(*) FROM InboxTreeSort WHERE numDomainId=@numDomainID AND numUserCntID=@numUserCntID AND vcNodeName='Sent Mail')>0)
		BEGIN
			SET @numNodeId = (SELECT TOP 1 ISNULL(numNodeID,0) FROM InboxTreeSort WHERE numDomainId=@numDomainID AND numUserCntID=@numUserCntID AND vcNodeName='Sent Mail')
		END
		ELSE IF((SELECT COUNT(*) FROM InboxTreeSort WHERE numDomainId=@numDomainID AND numUserCntID=@numUserCntID AND vcNodeName='Outbox')>0)
		BEGIN
			SET @numNodeId = (SELECT TOP 1 ISNULL(numNodeID,0) FROM InboxTreeSort WHERE numDomainId=@numDomainID AND numUserCntID=@numUserCntID AND vcNodeName='Outbox')
		END
		ELSE IF((SELECT COUNT(*) FROM InboxTreeSort WHERE numDomainId=@numDomainID AND numUserCntID=@numUserCntID AND vcNodeName='Sent')>0)
		BEGIN
			SET @numNodeId = (SELECT TOP 1 ISNULL(numNodeID,0) FROM InboxTreeSort WHERE numDomainId=@numDomainID AND numUserCntID=@numUserCntID AND vcNodeName='Sent')
		END
		ELSE IF((SELECT COUNT(*) FROM InboxTreeSort WHERE numDomainId=@numDomainID AND numUserCntID=@numUserCntID AND vcNodeName='Sent Messages')>0)
		BEGIN
			SET @numNodeId = (SELECT TOP 1 ISNULL(numNodeID,0) FROM InboxTreeSort WHERE numDomainId=@numDomainID AND numUserCntID=@numUserCntID AND vcNodeName='Sent Messages')
		END
		
		INSERT INTO EmailHistory
		(vcSubject,vcBody,vcBodyText,bintCreatedOn,bitHasAttachments,vcItemId,vcChangeKey,bitIsRead,vcSize,chrSource,tinttype,vcCategory,dtReceivedOn,numDomainid,numUserCntId,numNodeId,vcFrom,vcTo,vcCC,vcBCC,IsReplied,bitInvisible)                          
		VALUES                          
		(@vcSubject,@vcBody,dbo.fn_StripHTML(@vcBodyText),getutcdate(),@bitHasAttachment,@vcItemId,@vcChangeKey,@bitIsRead,convert(varchar(200),@vcSize),@chrSource,@tinttype,@vcCategory,@dtReceived,@numDomainid,@numUserCntId,@numNodeId,dbo.fn_GetNewvcEmailString(@vcMessageFrom,@numDomainId),dbo.fn_GetNewvcEmailString(@vcMessageTo,@numDomainId),dbo.fn_GetNewvcEmailString(@vcCC,@numDomainId),dbo.fn_GetNewvcEmailString(@vcBCC,@numDomainId),@IsReplied,@bitInvisible)              
		
		SET @Identity=SCOPE_IDENTITY()  
           

		UPDATE EmailHistory set numEmailId=(SELECT TOP 1 Item FROM dbo.DelimitedSplit8K(vcFrom,'$^$')) WHERE [numEmailHstrID]=@Identity                

		UPDATE EmailHistory SET IsReplied = @IsReplied WHERE [numEmailHstrID] = @numRepliedMailID      

		IF ISNULL(@numOppBizDocID,0) > 0 AND ISNULL((SELECT bitUsePreviousEmailBizDoc FROM Domain WHERE numDomainID=@numDomainID),0) = 1
		BEGIN
			DECLARE @numDivisionId NUMERIC(18,0)
			DECLARE @numBizDocId NUMERIC(18,0)

			SELECT @numDivisionId=numDivisionId,@numBizDocId=numBizDocId FROM OpportunityBizDocs OBD INNER JOIN OpportunityMaster OM ON OBD.numOppId=OM.numOppId WHERE numOppBizDocsId=@numOppBizDocID

			IF ISNULL(@numDivisionId,0) > 0 AND ISNULL(@numBizDocId,0) > 0
			BEGIN
				IF EXISTS (SELECT ID FROm DivisionMasterBizDocEmail WHERE numDomainID=@numDomainId AND numDivisionID=@numDivisionId AND numBizDocID=@numBizDocId)
				BEGIN
					UPDATE
						DivisionMasterBizDocEmail
					SET
						vcTo = dbo.fn_GetNewvcEmailString(@vcMessageTo,@numDomainId)
						,vcCC = dbo.fn_GetNewvcEmailString(@vcCC,@numDomainId)
						,vcBCC = dbo.fn_GetNewvcEmailString(@vcBCC,@numDomainId)
					WHERE
						numDomainID=@numDomainId 
						AND numDivisionID=@numDivisionId 
						AND numBizDocID=@numBizDocId
				END
				ELSE
				BEGIN
					INSERT INTO DivisionMasterBizDocEmail
					(
						numDomainID
						,numDivisionID
						,numBizDocID
						,vcTo
						,vcCC
						,vcBCC
					)
					VALUES
					(
						@numDomainId
						,@numDivisionId
						,@numBizDocId
						,dbo.fn_GetNewvcEmailString(@vcMessageTo,@numDomainId)
						,dbo.fn_GetNewvcEmailString(@vcCC,@numDomainId)
						,dbo.fn_GetNewvcEmailString(@vcBCC,@numDomainId)
					)
				END
			END
		END
		
		SELECT ISNULL(@Identity,0)
	END
END
GO
