/****** Object:  StoredProcedure [dbo].[USP_GetMemReptByTeam]    Script Date: 07/26/2008 16:17:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getmemreptbyteam')
DROP PROCEDURE usp_getmemreptbyteam
GO
CREATE PROCEDURE [dbo].[USP_GetMemReptByTeam]    
@numDomainId as numeric(9),    
@numUserCntId as numeric(9)    
as    
select distinct(A.numContactID),A.vcFirstName+' '+A.vcLastName as vcUserName from UserMaster U  
Join AdditionalContactsInformation A  
on A.numContactID= u.numUserDetailID  
join UserTeams  UT  
on UT.numUserCntID=A.numContactID    
where A.numTeam in(select numTeam from ForReportsByTeam F where F.numUserCntID=@numUserCntId and F.numDomainID=@numDomainId and F.tintType=5)    
and U.numDomainID=@numDomainId
GO
