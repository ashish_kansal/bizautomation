/****** Object:  StoredProcedure [dbo].[usp_InsertSurveyResponse]    Script Date: 07/26/2008 16:19:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created By: Debasish Tapan Nag                                            
--Purpose: Saves the Survey Respondents Information            
--Created Date: 09/23/2005                                  
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_insertsurveyresponse')
DROP PROCEDURE usp_insertsurveyresponse
GO
CREATE PROCEDURE [dbo].[usp_InsertSurveyResponse]            
 @numSurID Numeric(9),               
 @numRespondantID Numeric(9),             
 @numSurRating Numeric(9),               
 @numSurveyRespondentContactId Numeric(9),                
 @vcSurveyResponseInformation NText,
 @numDomainId Numeric(9)                 
AS              
--This will insert response for the surveys.              
BEGIN         
   PRINT @numSurID    
   PRINT @numRespondantID    
   PRINT @numSurveyRespondentContactId                             
                  
   IF @numRespondantID = 0            
   BEGIN            
        DECLARE @bitRegisteredRespondant Bit        
        IF @numSurveyRespondentContactId > 0        
         SET @bitRegisteredRespondant = 1        
        ELSE        
         SET @bitRegisteredRespondant = 0        
        
         INSERT INTO SurveyRespondentsMaster(numSurID, numSurRating,            
         dtDateofResponse, bitRegisteredRespondant, numRegisteredRespondentContactId, numDomainId)                 
         VALUES(@numSurID, @numSurRating, getutcdate(), @bitRegisteredRespondant, @numSurveyRespondentContactId, @numDomainId)            
         
         SELECT @numRespondantID = SCOPE_IDENTITY()        
   END            
    
    IF @numSurRating>0
    BEGIN
		UPDATE SurveyRespondentsMaster SET numSurRating = @numSurRating
      WHERE numRespondantID = @numRespondantID    
	END
    
   IF @numSurveyRespondentContactId > 0            
   BEGIN      
     UPDATE SurveyRespondentsMaster SET numSurRating = @numSurRating,    
        numRegisteredRespondentContactId = @numSurveyRespondentContactId             
      WHERE numRespondantID = @numRespondantID      
   END    
              
        
   DECLARE @iDoc int       
   EXECUTE sp_xml_preparedocument @iDoc OUTPUT, @vcSurveyResponseInformation       
   INSERT INTO SurveyResponse(                      
        numRespondantID,            
        numSurID,            
        numParentSurID,            
        numQuestionID,            
        numAnsID,            
        vcAnsText,numMatrixID            
   )                          
          SELECT @numRespondantID AS numRespondantID, numSurID, numParentSurID, numQuestionID, numAnsID, vcAnsText ,numMatrixID FROM OpenXML(@iDoc, '/NewDataSet/SurveyResponse', 2)            
          WITH                              
   (                      
        numSurID Numeric,            
        numParentSurID Numeric,            
        numQuestionID Numeric,            
        numAnsID Numeric,            
        vcAnsText NVarchar(500),numMatrixID Numeric        
   )            
                    
   EXECUTE sp_xml_removedocument @iDoc     
END
GO
