/****** Object:  StoredProcedure [dbo].[usp_GetSurveyList]    Script Date: 07/26/2008 16:18:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created By: Debasish Tapan Nag                                      
--Purpose: list of surveys                
--Created Date: 09/19/2005
--Modified By: Gangadhar Rao

                   
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getsurveylist')
DROP PROCEDURE usp_getsurveylist
GO
CREATE PROCEDURE [dbo].[usp_GetSurveyList]    
 @numDomainID numeric                 
AS                           
BEGIN                  
 SELECT sm.numSurID, sm.bitSkipRegistration, vcSurName,                 
  bintCreatedOn,                 
  Convert(Nvarchar,sm.numSurID) + ',' + Convert(Nvarchar,sm.bitSkipRegistration) as LinkedReport ,
(              
     SELECT  Count(numRespondantID)          
     FROM SurveyRespondentsMaster srm        
     WHERE     bitRegisteredRespondant = 1 
	and srm.numDomainId = @numDomainID       
     AND sm.numSurID = srm.numSurID    
                 
  ) as numTotalRespondents              
  FROM SurveyMaster sm              
               
  WHERE sm.numDomainID = @numDomainID              
          
          --UNION select 0,0,'--Select One--',0,Convert(Nvarchar,0),0  
END
GO
