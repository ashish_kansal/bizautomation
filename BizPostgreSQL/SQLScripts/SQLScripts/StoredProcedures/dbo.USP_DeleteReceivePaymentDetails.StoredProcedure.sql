--Created By Siva  
-- exec USP_DeleteReceivePaymentDetails @numBizDocsPaymentDetailsId=1134,@numBizDocsPaymentDetId=1855,@numBizDocsId=5078,@numOppId=5732,@numReturnID=0
--select * from BizDocComission


GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_deletereceivepaymentdetails')
DROP PROCEDURE usp_deletereceivepaymentdetails
GO
CREATE PROCEDURE [dbo].[USP_DeleteReceivePaymentDetails]            
(      
@numBizDocsPaymentDetailsId as numeric(9)=0,      
@numBizDocsPaymentDetId as numeric(9)=0,            
@numBizDocsId as numeric(9)=0,            
@numOppId as numeric(9)=0,
@numReturnID numeric(9)=0,
@tintPaymentType TINYINT,
@numDomainID NUMERIC
)
As            
Begin            
 Declare @bitSalesDeferredIncome as bit  
 Declare @bitIntegratedToAcnt as bit        
 Declare @monAmount as DECIMAL(20,5)        
  
 
 --Added by chintan to Update Embedded cost foreign key reference before deleteting primary key
 UPDATE [EmbeddedCost] SET [numBizDocsPaymentDetId] = NULL WHERE [numBizDocsPaymentDetId] = @numBizDocsPaymentDetId  
 
 --- Added by sojan to delete the BizDoc Comission on  03 May 2010
 
 create table #TempBizDocCom
 ( numJournalId numeric(18)); 
 
 
 Set @monAmount=0      
 
 
 Select @bitSalesDeferredIncome=isnull(bitSalesDeferredIncome,0) From OpportunityBizDocsDetails Where numBizDocsPaymentDetId=@numBizDocsPaymentDetId          
 Select @monAmount=isnull(monAmount,0),@bitIntegratedToAcnt=bitIntegrated From OpportunityBizDocsPaymentDetails Where numBizDocsPaymentDetId=@numBizDocsPaymentDetId  And numBizDocsPaymentDetailsId=@numBizDocsPaymentDetailsId      
 print @monAmount
 PRINT @bitIntegratedToAcnt
 
  IF @numReturnID <> 0 
  BEGIN
	DECLARE @StatusOfReturn NUMERIC(9)
	SELECT @StatusOfReturn = ISNULL([numListItemID],0) FROM [ListDetails] WHERE [numListID]=49 AND [constFlag]=1 AND [vcData]='Confirmed'
	UPDATE [Returns] SET [numReturnStatus] = @StatusOfReturn WHERE [numReturnID]=@numReturnID
	
  	DELETE FROM [OpportunityBizDocsDetails] WHERE [numReturnID] = @numReturnID
  	AND numBizDocsPaymentDetId = @numBizDocsPaymentDetId
  END
 
 If @bitIntegratedToAcnt=0           
  Begin   
       
--     PRINT 'not integrated'
     Delete From OpportunityBizDocsPaymentDetails Where numBizDocsPaymentDetailsId=@numBizDocsPaymentDetailsId            
     if @bitSalesDeferredIncome=1
		update OpportunityBizDocsDetails Set monAmount=isnull(monAmount,0)-@monAmount where numBizDocsPaymentDetId=@numBizDocsPaymentDetId      
	 Else
		BEGIN
            DELETE FROM ProjectsOpportunities WHERE numDomainID = @numDomainID AND numBillID = @numBizDocsPaymentDetId -- added by chintan so it does not throw error while project bill is deleted from money out
			Delete From  OpportunityBizDocsDetails where numBizDocsPaymentDetId=@numBizDocsPaymentDetId      
	    END
     
  END
  
UPDATE BizDocComission set bitCommisionPaid=0,numBizDocsPaymentDetID=null where numBizDocsPaymentDetID=@numBizDocsPaymentDetId
 ---- Added by Sojan to delete the BizDocComission & Its Journals 03 May 2010
  insert into #TempBizDocCom  
 select distinct GD.numJournalId from General_Journal_Details GD where numcontactid in 
(select BC.numComissionID from BizDocComission BC where BC.numOppBizDocId=@numBizDocsId)
 
 delete from CheckDetails  where  numCheckId 
 in (select [numCheckId] from General_Journal_Header GH where numJournal_Id in 
 (select numJournalId from #TempBizDocCom));
 
 delete from General_Journal_Details where numJournalId in (select numJournalId from #TempBizDocCom);
 
 delete from General_Journal_Header where numJournal_Id in (select numJournalId from #TempBizDocCom);
 
 delete from BizDocComission where numOppBizDocId = @numBizDocsId
 
 -------------------- ********** End of BizDoc Deletion**********-----------------
 
 DELETE FROM ProjectsOpportunities WHERE numDomainID = @numDomainID AND numBillID = @numBizDocsPaymentDetId -- added by chintan so it does not throw error while project bill is deleted from money out
 
 Delete From OpportunityBizDocsPaymentDetails Where numBizDocsPaymentDetId=@numBizDocsPaymentDetId            
 Delete From OpportunityBizDocsDetails where numBizDocsPaymentDetId=@numBizDocsPaymentDetId      
 
 
 IF @tintPaymentType<>5
 BEGIN
	Update OpportunityBizDocs Set monAmountPaid=isnull(monAmountPaid,0)-@monAmount Where  numOppBizDocsId=@numBizDocsId And numOppId=@numOppId      
	Update OpportunityMaster Set numPClosingPercent=isnull(numPClosingPercent,0)-@monAmount Where numOppId=@numOppId         
 END
 
 DECLARE @numJournal_ID AS NUMERIC(9)
 
 IF @tintPaymentType = 2 or @tintPaymentType = 6 --Bill or Payroll
 BEGIN
	 SELECT TOP 1 @numJournal_ID=[numJournal_Id] FROM [General_Journal_Header] WHERE [numBizDocsPaymentDetId] = @numBizDocsPaymentDetId
	 exec USP_DeleteJournalDetails @numJournal_ID,@numDomainID,0,0,0
 END
 ELSE
 BEGIN
	 SELECT TOP 1 @numJournal_ID=[numJournalId] FROM [General_Journal_Details] WHERE [numBizDocsPaymentDetailsId] = @numBizDocsPaymentDetId--1850
	--     PRINT @numJournal_ID
	--     PRINT @numDomainID
	 exec USP_DeleteJournalDetails @numJournal_ID,@numDomainID,0,0,0
 END
  
  --Update OpportunityMaster Set numPClosingPercent=isnull(monAmountPaid,0)-@monAmount            
End
