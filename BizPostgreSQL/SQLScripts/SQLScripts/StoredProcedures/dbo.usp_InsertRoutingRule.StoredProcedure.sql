/****** Object:  StoredProcedure [dbo].[usp_InsertRoutingRule]    Script Date: 07/26/2008 16:19:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Modified by anoop jayaraj                
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_insertroutingrule')
DROP PROCEDURE usp_insertroutingrule
GO
CREATE PROCEDURE [dbo].[usp_InsertRoutingRule]                    
 @numDomainID numeric,                    
 @vcRoutName varchar(50),  
 @tintEqualTo as tinyint=0,                                    
 @numValue numeric(9)=0,                                               
 @numUserCntID as numeric(9)=0,                                       
 @tintPriority as tinyint,  
 @strValues as text,  
 @strDistribitionList as text,
 @vcDBCoulmnName as varchar(100)='',
 @vcSelectedColumn as varchar(200)=''              
                     
AS      
       declare @numRoutID as numeric(9)   
     
 select @tintPriority=isnull(max(tintPriority),0) from RoutingLeads where numDomainId = @numDomainId    
 set @tintPriority=@tintPriority+1              
         
  
          
   Insert into RoutingLeads (numDomainID, vcRoutName,tintEqualTo, numValue,  
 numCreatedBy, dtCreatedDate ,numModifiedBy, dtModifiedDate,bitDefault,tintPriority,vcDBColumnName,vcSelectedCoulmn)             
   values(@numDomainID, @vcRoutName, @tintEqualTo, @numValue,            
    @numUserCntID, getutcdate(),@numUserCntID, getutcdate(),0,@tintPriority,@vcDBCoulmnName,@vcSelectedColumn)    
  
       
    set @numRoutID=@@identity    
 if convert(varchar(2),@strValues)<>''  
 begin  
    
  DECLARE @hDoc1 int                                    
   EXEC sp_xml_preparedocument @hDoc1 OUTPUT, @strValues                                    
                                      
   insert into RoutinLeadsValues                                    
    (numRoutID,vcValue)                                    
                                       
   select @numRoutID,X.vcValue   
   from(SELECT *FROM OPENXML (@hDoc1,'/NewDataSet/Table',2)                                    
   WITH  (                                    
    vcValue varchar(100)              
    ))X                                                     
   EXEC sp_xml_removedocument @hDoc1     
  
 end  
 if convert(varchar(2),@strDistribitionList)<>''  
 begin  
    
  DECLARE @hDoc2 int                                    
   EXEC sp_xml_preparedocument @hDoc2 OUTPUT, @strDistribitionList                                    
                                      
   insert into RoutingLeadDetails                                    
    (numRoutID,numEmpId,intAssignOrder)                                    
                                       
   select @numRoutID,X.numEmpId,X.intAssignOrder   
   from(SELECT *FROM OPENXML (@hDoc2,'/NewDataSet/Table',2)                                    
   WITH  (                                    
    numEmpId numeric(9),  
    intAssignOrder int              
    ))X                                                     
   EXEC sp_xml_removedocument @hDoc2     
  
 end         
        
   select @numRoutID
GO
