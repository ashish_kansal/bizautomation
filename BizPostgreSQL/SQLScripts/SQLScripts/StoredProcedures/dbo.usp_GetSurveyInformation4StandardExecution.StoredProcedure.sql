USE [TotalBiz2009]
GO
/****** Object:  StoredProcedure [dbo].[usp_GetSurveyInformation4StandardExecution]    Script Date: 12/28/2010 10:06:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[usp_GetSurveyInformation4StandardExecution]        
 @numSurId Numeric                
AS                  
--This procedure will return all survey master information for the selected survey                
BEGIN                
 SELECT numSurID, vcSurName, vcRedirectURL, bitSkipRegistration, bitRandomAnswers, bitSinglePageSurvey       
  FROM SurveyMaster                  
  WHERE numSurId = @numSurId           
                 
 SELECT numSurID, numQID, vcQuestion, tIntAnsType, intNumOfAns,boolMatrixColumn,intNumOfMatrixColumn              
  FROM SurveyQuestionMaster                  
  WHERE numSurId = @numSurId    
  ORDER BY numQID        
              
 SELECT numSurID, numQID, numAnsID, vcAnsLabel, boolSurveyRuleAttached, 0 as boolDeleted     
  FROM SurveyAnsMaster                  
  WHERE  numSurId = @numSurId     
  ORDER BY numQID, numAnsID                    
              
 SELECT numRuleID, numSurID, numAnsID, numQID, boolActivation, numEventOrder,               
  vcQuestionList, numLinkedSurID, numResponseRating, vcPopUpURL, numGrpId, numCompanyType,              
  tIntCRMType, numRecOwner,tIntRuleFourRadio,tIntCreateNewID,vcRuleFourSelectFields,boolPopulateSalesOpportunity,vcRuleFiveSelectFields,numMatrixID,vcItem
  FROM SurveyWorkflowRules                  
  WHERE  numSurId = @numSurId      
  ORDER BY numSurID, numQID, numAnsID, numEventOrder   
  
 SELECT numSurID, numQID, numMatrixID, vcAnsLabel, boolSurveyRuleAttached, 0 as boolDeleted     
  FROM SurveyMatrixAnsMaster                  
  WHERE  numSurId = @numSurId     
  ORDER BY numQID, numMatrixID                  
END
