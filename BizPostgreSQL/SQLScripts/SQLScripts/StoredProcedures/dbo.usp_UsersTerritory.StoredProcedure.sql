/****** Object:  StoredProcedure [dbo].[usp_UsersTerritory]    Script Date: 07/26/2008 16:21:57 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_usersterritory')
DROP PROCEDURE usp_usersterritory
GO
CREATE PROCEDURE [dbo].[usp_UsersTerritory]     
 @numUserID NUMERIC(9),    
 @vcUserName VARCHAR(50),    
 @vcUserDesc VARCHAR(250),    
 @numTerritoryID numeric,       
 @numUserDetailID numeric  --
AS    
BEGIN    
 UPDATE UserMaster SET vcUserName = @vcUserName,    
    vcUserDesc = @vcUserDesc,       
    numUserDetailId = @numUserDetailID    
 WHERE numUserID = @numUserID    
END
GO
