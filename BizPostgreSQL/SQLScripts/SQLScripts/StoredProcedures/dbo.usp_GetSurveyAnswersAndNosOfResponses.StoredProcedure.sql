/****** Object:  StoredProcedure [dbo].[usp_GetSurveyAnswersAndNosOfResponses]    Script Date: 07/26/2008 16:18:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created By: Debasish Tapan Nag                                      
--Purpose: Returns the answers for a question and also the number of responses         
--Created Date: 10/02/2005                   
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'usp_getsurveyanswersandnosofresponses' ) 
    DROP PROCEDURE usp_getsurveyanswersandnosofresponses
GO
CREATE PROCEDURE [dbo].[usp_GetSurveyAnswersAndNosOfResponses]
    @numParentSurID NUMERIC,
    @numSurID NUMERIC,
    @numQID NUMERIC
AS 
    BEGIN       

        DECLARE @boolMatrixColumn BIT ;

        SET @boolMatrixColumn = ( SELECT    boolMatrixColumn
                                  FROM      SurveyQuestionMaster
                                  WHERE     [numQID] = @numQID
                                            AND [numSurID] = @numSurId
                                )
                 
        IF @boolMatrixColumn = 0 
            BEGIN
                SELECT  sam.numSurID,
                        sam.numQID,
                        sam.numAnsID,
                        sam.vcAnsLabel,
                        COUNT(numRespondantID) AS numResponses,
                        1 AS boolActivation,
                        CASE WHEN @numParentSurID = @numSurID
                             THEN sam.boolSurveyRuleAttached
                             ELSE 0
                        END AS boolSurveyRuleAttached,
                        0 AS boolDeleted
                FROM    SurveyAnsMaster sam INNER JOIN SurveyResponse sr ON sam.numSurId = sr.numParentSurId AND sr.numQuestionID = sam.numQID AND sr.numAnsID = sam.numAnsID
                WHERE   sr.numParentSurId = @numParentSurID
                        AND sr.numSurId = @numSurId
                        AND sam.numSurId = @numParentSurID
                        AND sr.numQuestionID = @numQID
                        AND sam.numQID = @numQID
                GROUP BY sam.numSurID,
                        sam.numQID,
                        sam.numAnsID,
                        sam.vcAnsLabel,
                        sam.boolSurveyRuleAttached
                ORDER BY sam.numAnsID  
            END
            ELSE IF @boolMatrixColumn=1
            BEGIN 
            SELECT  sam.numSurID,
                        sam.numQID,
                        sam.numMatrixID AS numAnsID,
                        sam.vcAnsLabel,
                        COUNT(numRespondantID) AS numResponses,
                        1 AS boolActivation,
                        CASE WHEN @numParentSurID = @numSurID
                             THEN sam.boolSurveyRuleAttached
                             ELSE 0
                        END AS boolSurveyRuleAttached,
                        0 AS boolDeleted
                FROM    SurveyMatrixAnsMaster sam INNER JOIN SurveyResponse sr ON sam.numSurId = sr.numParentSurId AND sr.numQuestionID = sam.numQID AND sr.numMatrixID = sam.numMatrixID
                WHERE   sr.numParentSurId = @numParentSurID
                        AND sr.numSurId = @numSurId
                        AND sam.numSurId = @numParentSurID
                        AND sr.numQuestionID = @numQID
                        AND sam.numQID = @numQID
                GROUP BY sam.numSurID,
                        sam.numQID,
                        sam.numMatrixID,
                        sam.vcAnsLabel,
                        sam.boolSurveyRuleAttached
                ORDER BY sam.numMatrixID  
            
            END
    END
GO
