/****** Object:  StoredProcedure [dbo].[usp_getDefaultTabName]    Script Date: 07/26/2008 16:17:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getdefaulttabname')
DROP PROCEDURE usp_getdefaulttabname
GO
CREATE PROCEDURE [dbo].[usp_getDefaultTabName]  
@numDomainId as numeric  
as  
if ( select count(*) from DefaultTabs where numDomainId = @numDomainId) > 0  
begin   
	 select 
	 numDomainId,  
	 vcContact,  
	 vcProspect,  
	 vcAccount  ,
	 vcLead,
	 vcContactPlural,
	 vcProspectPlural,
	 vcAccountPlural,
	 vcLeadPlural
	 from DefaultTabs   
	 where numDomainId = @numDomainId  
end  
else  
begin   
	select   
	@numDomainId as numDomainId, 
	'Contact' as vcContact,  
	'Prospect' as vcProspect,  
	'Account' as vcAccount  ,
	'Lead' as vcLead,
	'Contacts' as  vcContactPlural,
	'Prospects' as vcProspectPlural,
	'Accounts' as vcAccountPlural,
	'Leads' as vcLeadPlural
end
GO
