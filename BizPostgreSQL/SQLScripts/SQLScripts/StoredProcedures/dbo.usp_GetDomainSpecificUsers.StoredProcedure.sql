/****** Object:  StoredProcedure [dbo].[usp_GetDomainSpecificUsers]    Script Date: 07/26/2008 16:17:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
--This procedure is used for getting the users of the domain which is being passed as
--the parameter
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getdomainspecificusers')
DROP PROCEDURE usp_getdomainspecificusers
GO
CREATE PROCEDURE [dbo].[usp_GetDomainSpecificUsers]
	@numDomainId numeric,
	@numUserId numeric  
-- 
AS
	SELECT numUserID,vcUserName,vcUserDesc,numDomainID
	FROM UserMaster WHERE numdomainid=@numDomainId and numuserid<>@numuserid
GO
