/****** Object:  StoredProcedure [dbo].[usp_GetAvailableFieldsUser]    Script Date: 07/26/2008 16:16:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'usp_getavailablefieldsuser' ) 
    DROP PROCEDURE usp_getavailablefieldsuser
GO
CREATE PROCEDURE [dbo].[usp_GetAvailableFieldsUser]
    @numGroupType NUMERIC,
    @numDomainId NUMERIC,
    @numContactId NUMERIC,
    @numTabId NUMERIC
AS 
    IF ( SELECT COUNT(*)
         FROM   ShortCutGrpConf
         WHERE  numDomainId = @numDomainId
                AND numGroupID = @numGroupType
       ) = 0 
        BEGIN
            INSERT  INTO ShortCutGrpConf
                    (
                      numGroupId,
                      numLinkId,
                      numOrder,
                      bitType,
                      numDomainId,
                      numTabId,
                      bitInitialPage,
                      tintLinkType
                    )
                    SELECT  @numGroupType,
                            x.id,
                            x.[order],
                            x.bittype,
                            @numDomainId,
                            x.numTabId,
                            X.bitInitialPage,
                            0
                    FROM    ( ( SELECT  *
                                FROM    ShortCutBar
                                WHERE   bitdefault = 1
                                        AND numContactId = 0
                              )
                            )x
	               
        END


        
    IF ( SELECT COUNT(*)
         FROM   ShortCutGrpConf
         WHERE  numDomainId = @numDomainId
                AND numGroupID = @numGroupType
                AND numTabId = @numTabId
       ) > 0 
        BEGIN           
            IF ( SELECT COUNT(*)
                 FROM   ShortCutUsrcnf
                 WHERE  numDomainId = @numDomainId
                        AND numGroupID = @numGroupType
                        AND numContactId = @numContactId
                        AND numTabId = @numTabId
               ) > 0 
                BEGIN          
					IF @numTabId = 1
					BEGIN
						( SELECT    CONVERT(VARCHAR(10), Hdr.id) + '~0' AS id,
									Hdr.vclinkname,
									sconf.numOrder AS [Order]
						  FROM      ShortCutGrpconf Sconf
									JOIN ShortCutBar Hdr ON Hdr.id = Sconf.numLinkId
						  WHERE     Hdr.numTabId = @numTabId
									AND Sconf.numTabId = Hdr.numTabId
									AND sconf.numDomainId = @numDomainId
									AND sconf.numGroupID = @numGroupType
									AND numContactId = 0
									AND ISNULL(Sconf.tintLinkType, 0) = 0
									AND Sconf.numLinkId NOT IN (
									SELECT  numLinkId
									FROM    ShortCutUsrcnf
									WHERE   numDomainId = @numDomainId
											AND numGroupID = @numGroupType
											AND numContactId = @numContactId
											AND numTabId = @numTabId
											AND ISNULL(tintLinkType, 0) = 0 )
						  UNION
						  SELECT    CONVERT(VARCHAR(10), LD.numListItemID) + '~5' AS id,
									LD.vcData AS vclinkname,
									sconf.numOrder AS [Order]
						  FROM      ShortCutGrpconf Sconf
									JOIN ListDetails LD ON LD.numListItemID = Sconf.numLinkId
						  WHERE     LD.numListId = 5
									AND LD.numListItemID <> 46
									AND ( LD.numDomainID = @numDomainID
										  OR constflag = 1
										)
									AND Sconf.numTabId = @numTabId
									AND sconf.numDomainId = @numDomainId
									AND sconf.numGroupID = @numGroupType
									AND ISNULL(Sconf.tintLinkType, 0) = 5
									AND @numTabId = 7
									AND Sconf.numLinkId NOT IN (
									SELECT  numLinkId
									FROM    ShortCutUsrcnf
									WHERE   numDomainId = @numDomainId
											AND numGroupID = @numGroupType
											AND numContactId = @numContactId
											AND numTabId = @numTabId
											AND ISNULL(tintLinkType, 0) = 5 )
						  UNION
						  SELECT    CONVERT(VARCHAR(10), Hdr.id) + '~0' AS id,
									Hdr.vclinkname,
									[order]
						  FROM      ShortCutBar Hdr
						  WHERE     Hdr.numTabId = @numTabId
									AND hdr.numDomainId = @numDomainId
									AND hdr.numGroupID = @numGroupType
									AND numContactId = @numContactId
									AND id NOT IN (
									SELECT  numLinkId
									FROM    ShortCutUsrcnf
									WHERE   numDomainId = @numDomainId
											AND numGroupID = @numGroupType
											AND numContactId = @numContactId
											AND numTabId = @numTabId )
						  UNION ALL
						  SELECT    CONVERT(VARCHAR(10), LD.numListItemID) + '~0' AS id,
									LD.vcData AS [vclinkname],
									sintOrder AS [order]
						  FROM      dbo.ListDetails LD
						  WHERE     numListID = 27
									AND LD.numDomainID = @numDomainID
									AND ISNULL(constFlag, 0) = 0
									AND LD.numListItemID NOT IN (
									SELECT  numLinkId
									FROM    ShortCutUsrcnf
									WHERE   numDomainId = @numDomainId
											AND numGroupID = @numGroupType
											AND numContactId = @numContactId
											AND numTabId = @numTabId )
						  UNION ALL
						  SELECT    CONVERT(VARCHAR(10), LD.numListItemID) + '~0' AS id,
									LD.vcData AS [vclinkname],
									sintOrder AS [order]
						  FROM      dbo.ListDetails LD
						  WHERE     numListID = 27
									AND LD.numDomainID = @numDomainID
									AND ISNULL(constFlag, 0) = 1
									AND LD.numListItemID NOT IN (
									SELECT  numLinkId
									FROM    ShortCutUsrcnf
									WHERE   numDomainId = @numDomainId
											AND numGroupID = @numGroupType
											AND numContactId = @numContactId
											AND numTabId = @numTabId )
						 --ORDER BY  sconf.numOrder
	                      
						)
						ORDER BY [order]    
    
						SELECT  CONVERT(VARCHAR(10), Hdr.id) + '~0' AS id,
								Hdr.vclinkname,
								ISNULL(Sconf.bitInitialPage, 0) bitInitialPage,
								sconf.numOrder,
								ISNULL(Sconf.bitFavourite, 0) bitFavourite
						FROM    ShortCutUsrcnf Sconf
								JOIN ShortCutBar Hdr ON Hdr.id = Sconf.numLinkId
						WHERE   Hdr.numTabId = @numTabId
								AND sconf.numTabId = Hdr.numTabId
								AND sconf.numDomainId = @numDomainId
								AND sconf.numGroupID = @numGroupType
								AND sconf.numcontactid = @numContactId
								AND ISNULL(Sconf.tintLinkType, 0) = 0
						UNION
						SELECT  CONVERT(VARCHAR(10), LD.numListItemID) + '~5' AS id,
								LD.vcData AS vclinkname,
								ISNULL(Sconf.bitInitialPage, 0) bitInitialPage,
								sconf.numOrder,
								ISNULL(Sconf.bitFavourite, 0) bitFavourite
						FROM    ShortCutUsrcnf Sconf
								JOIN ListDetails LD ON LD.numListItemID = Sconf.numLinkId
						WHERE   LD.numListId = 5
								AND LD.numListItemID <> 46
								AND ( LD.numDomainID = @numDomainID
									  OR constflag = 1
									)
								AND sconf.numTabId = @numTabId
								AND sconf.numDomainId = @numDomainId
								AND sconf.numGroupID = @numGroupType
								AND sconf.numcontactid = @numContactId
								AND ISNULL(Sconf.tintLinkType, 0) = 5
								AND @numTabId = 7
						--ORDER BY sconf.numOrder
						UNION ALL
						SELECT  CONVERT(VARCHAR(10), LD.numListItemID) + '~0' AS id,
								LD.vcData AS [vclinkname],
								0 bitInitialPage,
								sintOrder AS [order],
								0 bitFavourite
						FROM    dbo.ListDetails LD
								JOIN ShortCutUsrcnf Sconf ON LD.numListItemID = Sconf.numLinkId
						WHERE   numListID = 27
								AND LD.numDomainID = @numDomainID
								AND Sconf.numContactId = @numContactId
								AND ISNULL(constFlag, 0) = 0
						UNION ALL
						SELECT  CONVERT(VARCHAR(10), LD.numListItemID) + '~0' AS id,
								LD.vcData AS [vclinkname],
								0 bitInitialPage,
								sintOrder AS [order],
								0 bitFavourite
						FROM    dbo.ListDetails LD
								JOIN ShortCutUsrcnf Sconf ON LD.numListItemID = Sconf.numLinkId
						WHERE   numListID = 27
								AND LD.numDomainID = @numDomainID
								AND ISNULL(constFlag, 0) = 1
						ORDER BY sconf.numOrder 
					END
					ELSE
					BEGIN
							( SELECT    CONVERT(VARCHAR(10), Hdr.id) + '~0' AS id,
										Hdr.vclinkname,
										sconf.numOrder AS [Order]
							  FROM      ShortCutGrpconf Sconf
										JOIN ShortCutBar Hdr ON Hdr.id = Sconf.numLinkId
							  WHERE     Hdr.numTabId = @numTabId
										AND Sconf.numTabId = Hdr.numTabId
										AND sconf.numDomainId = @numDomainId
										AND sconf.numGroupID = @numGroupType
										AND numContactId = 0
										AND ISNULL(Sconf.tintLinkType, 0) = 0
										AND Sconf.numLinkId NOT IN (
										SELECT  numLinkId
										FROM    ShortCutUsrcnf
										WHERE   numDomainId = @numDomainId
												AND numGroupID = @numGroupType
												AND numContactId = @numContactId
												AND numTabId = @numTabId
												AND ISNULL(tintLinkType, 0) = 0 )
							  UNION
							  SELECT    CONVERT(VARCHAR(10), LD.numListItemID) + '~5' AS id,
										LD.vcData AS vclinkname,
										sconf.numOrder AS [Order]
							  FROM      ShortCutGrpconf Sconf
										JOIN ListDetails LD ON LD.numListItemID = Sconf.numLinkId
							  WHERE     LD.numListId = 5
										AND LD.numListItemID <> 46
										AND ( LD.numDomainID = @numDomainID
											  OR constflag = 1
											)
										AND Sconf.numTabId = @numTabId
										AND sconf.numDomainId = @numDomainId
										AND sconf.numGroupID = @numGroupType
										AND ISNULL(Sconf.tintLinkType, 0) = 5
										AND @numTabId = 7
										AND Sconf.numLinkId NOT IN (
										SELECT  numLinkId
										FROM    ShortCutUsrcnf
										WHERE   numDomainId = @numDomainId
												AND numGroupID = @numGroupType
												AND numContactId = @numContactId
												AND numTabId = @numTabId
												AND ISNULL(tintLinkType, 0) = 5 )
							  UNION
							  SELECT    CONVERT(VARCHAR(10), Hdr.id) + '~0' AS id,
										Hdr.vclinkname,
										[order]
							  FROM      ShortCutBar Hdr
							  WHERE     Hdr.numTabId = @numTabId
										AND hdr.numDomainId = @numDomainId
										AND hdr.numGroupID = @numGroupType
										AND numContactId = @numContactId
										AND id NOT IN (
										SELECT  numLinkId
										FROM    ShortCutUsrcnf
										WHERE   numDomainId = @numDomainId
												AND numGroupID = @numGroupType
												AND numContactId = @numContactId
												AND numTabId = @numTabId )
							  
							)
							ORDER BY [order]    
	    
							SELECT  CONVERT(VARCHAR(10), Hdr.id) + '~0' AS id,
									Hdr.vclinkname,
									ISNULL(Sconf.bitInitialPage, 0) bitInitialPage,
									sconf.numOrder,
									ISNULL(Sconf.bitFavourite, 0) bitFavourite
							FROM    ShortCutUsrcnf Sconf
									JOIN ShortCutBar Hdr ON Hdr.id = Sconf.numLinkId
							WHERE   Hdr.numTabId = @numTabId
									AND sconf.numTabId = Hdr.numTabId
									AND sconf.numDomainId = @numDomainId
									AND sconf.numGroupID = @numGroupType
									AND sconf.numcontactid = @numContactId
									AND ISNULL(Sconf.tintLinkType, 0) = 0
							UNION
							SELECT  CONVERT(VARCHAR(10), LD.numListItemID) + '~5' AS id,
									LD.vcData AS vclinkname,
									ISNULL(Sconf.bitInitialPage, 0) bitInitialPage,
									sconf.numOrder,
									ISNULL(Sconf.bitFavourite, 0) bitFavourite
							FROM    ShortCutUsrcnf Sconf
									JOIN ListDetails LD ON LD.numListItemID = Sconf.numLinkId
							WHERE   LD.numListId = 5
									AND LD.numListItemID <> 46
									AND ( LD.numDomainID = @numDomainID
										  OR constflag = 1
										)
									AND sconf.numTabId = @numTabId
									AND sconf.numDomainId = @numDomainId
									AND sconf.numGroupID = @numGroupType
									AND sconf.numcontactid = @numContactId
									AND ISNULL(Sconf.tintLinkType, 0) = 5
									AND @numTabId = 7
							ORDER BY sconf.numOrder
					END
                    
                END          
            ELSE 
                BEGIN  
                    SELECT  CONVERT(VARCHAR(10), Hdr.id) + '~0' AS id,
                            Hdr.vclinkname
                    FROM    ShortCutGrpConf Sconf
                            JOIN ShortCutBar Hdr ON Hdr.id = Sconf.numLinkId
                                                    AND Sconf.numTabId = Hdr.numTabId
                    WHERE   Sconf.numGroupID = 0
                            AND Hdr.numTabId = @numTabId
                            AND ISNULL(Sconf.tintLinkType, 0) = 0        
                    UNION ALL
                    SELECT  CONVERT(VARCHAR(10), LD.numListItemID) + '~0' AS id,
                            LD.vcData AS [vclinkname]
                    FROM    dbo.ListDetails LD
                            --JOIN ShortCutUsrcnf Sconf ON LD.numListItemID = Sconf.numLinkId
                    WHERE   numListID = 27
                            AND LD.numDomainID = @numDomainID
                            AND ISNULL(constFlag, 0) = 0
                    UNION ALL
                    SELECT  CONVERT(VARCHAR(10), LD.numListItemID) + '~0' AS id,
                            LD.vcData AS [vclinkname]
                    FROM    dbo.ListDetails LD
                            --JOIN ShortCutUsrcnf Sconf ON LD.numListItemID = Sconf.numLinkId
                    WHERE   numListID = 27
                            AND LD.numDomainID = @numDomainID
                            AND ISNULL(constFlag, 0) = 1
                            
		     
                    ( SELECT    CONVERT(VARCHAR(10), Hdr.id) + '~0' AS id,
                                Hdr.vclinkname,
                                Sconf.numorder AS [order],
                                ISNULL(Sconf.bitInitialPage, 0) bitInitialPage,
                                0 AS bitFavourite
                      FROM      ShortCutGrpConf Sconf
                                JOIN ShortCutBar Hdr ON Hdr.id = Sconf.numLinkId
                      WHERE     Hdr.numTabId = @numTabId
                                AND Sconf.numTabId = Hdr.numTabId
                                AND Sconf.numGroupID = @numGroupType
                                AND Sconf.numDomainId = @numDomainId
                                AND numContactId = 0
                                AND ISNULL(Sconf.tintLinkType, 0) = 0
                      UNION
                      SELECT    CONVERT(VARCHAR(10), LD.numListItemID) + '~5' AS id,
                                LD.vcData AS vclinkname,
                                sconf.numOrder AS [order],
                                ISNULL(Sconf.bitInitialPage, 0) bitInitialPage,
                                0 AS bitFavourite
                      FROM      ShortCutGrpconf Sconf
                                JOIN ListDetails LD ON LD.numListItemID = Sconf.numLinkId
                      WHERE     LD.numListId = 5
                                AND LD.numListItemID <> 46
                                AND ( LD.numDomainID = @numDomainID
                                      OR constflag = 1
                                    )
                                AND sconf.numTabId = @numTabId
                                AND sconf.numDomainId = @numDomainId
                                AND sconf.numGroupID = @numGroupType
                                AND ISNULL(Sconf.tintLinkType, 0) = 5
                                AND @numTabId = 7
                      UNION
                      SELECT    CONVERT(VARCHAR(10), id) + '~0' AS id,
                                vclinkname,
                                [order],
                                0 AS bitInitialPage,
                                0 AS bitFavourite
                      FROM      ShortCutBar
                      WHERE     numTabId = @numTabId
                                AND bitdefault = 0
                                AND numGroupID = @numGroupType
                                AND numDomainId = @numDomainId
                                AND numContactId = @numcontactId
                    )
                    ORDER BY [order]    
            
                
        
                END          
        END         
    ELSE 
        BEGIN        
			IF @numTabId = 1
			BEGIN
				SELECT  CONVERT(VARCHAR(10), id) + '~0' AS id,
                    vclinkname
            FROM    ShortCutBar
            WHERE   numTabId = @numTabId
                    AND bitdefault = 0
                    AND numcontactid = 0
    
            SELECT  CONVERT(VARCHAR(10), id) + '~0' AS id,
                    vclinkname,
                    ISNULL(bitInitialPage, 0) AS bitInitialPage,
                    0 AS bitFavourite
            FROM    ShortCutBar
            WHERE   numTabId = @numTabId
                    AND bitdefault = 1
                    AND numContactId = 0
            UNION
            SELECT  CONVERT(VARCHAR(10), numListItemID) + '~5' AS id,
                    vcData AS vclinkname,
                    0 AS bitInitialPage,
                    0 AS bitFavourite
            FROM    ListDetails
            WHERE   numListId = 5
                    AND numListItemID <> 46
                    AND ( numDomainID = @numDomainID
                          OR constflag = 1
                        )
                    AND @numTabId = 7
            UNION ALL
            SELECT  CONVERT(VARCHAR(10), LD.numListItemID) + '~0' AS id,
                    LD.vcData AS [vclinkname],
                    0 AS bitInitialPage,
                    0 AS bitFavourite
            FROM    dbo.ListDetails LD
                    JOIN ShortCutUsrcnf Sconf ON LD.numListItemID = Sconf.numLinkId
            WHERE   numListID = 27
                    AND LD.numDomainID = @numDomainID
                    AND ISNULL(constFlag, 0) = 0
            UNION ALL
            SELECT  CONVERT(VARCHAR(10), LD.numListItemID) + '~0' AS id,
                    LD.vcData AS [vclinkname],
                    0 AS bitInitialPage,
                    0 AS bitFavourite
            FROM    dbo.ListDetails LD
                    JOIN ShortCutUsrcnf Sconf ON LD.numListItemID = Sconf.numLinkId
            WHERE   numListID = 27
                    AND LD.numDomainID = @numDomainID
                    AND ISNULL(constFlag, 0) = 1
			END
			ELSE
			BEGIN
				SELECT  CONVERT(VARCHAR(10), id) + '~0' AS id,
						vclinkname
				FROM    ShortCutBar
				WHERE   numTabId = @numTabId
						AND bitdefault = 0
						AND numcontactid = 0
	    
				SELECT  CONVERT(VARCHAR(10), id) + '~0' AS id,
						vclinkname,
						ISNULL(bitInitialPage, 0) AS bitInitialPage,
						0 AS bitFavourite
				FROM    ShortCutBar
				WHERE   numTabId = @numTabId
						AND bitdefault = 1
						AND numContactId = 0
				UNION
				SELECT  CONVERT(VARCHAR(10), numListItemID) + '~5' AS id,
						vcData AS vclinkname,
						0 AS bitInitialPage,
						0 AS bitFavourite
				FROM    ListDetails
				WHERE   numListId = 5
						AND numListItemID <> 46
						AND ( numDomainID = @numDomainID
							  OR constflag = 1
							)
						AND @numTabId = 7
			END
            
        END        
        
   
GO
