/****** Object:  StoredProcedure [dbo].[usp_GetContactWithoutOppt]    Script Date: 07/26/2008 16:16:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--------------------------------------------
--usp_GetContactWithoutOppt

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getcontactwithoutoppt')
DROP PROCEDURE usp_getcontactwithoutoppt
GO
CREATE PROCEDURE [dbo].[usp_GetContactWithoutOppt]
	@numOppId numeric,
	@numDivId numeric   
--
AS
select a.numcontactid, a.vcFirstName,a.vclastname
from additionalcontactsinformation a
WHERE a.numcontactId not in
(select numcontactId from opportunitycontact where numoppid=@numOppId)
AND a.numcontactid not in(select numcontactId from opportunitymaster where numoppid=@numOppId)
AND a.numdivisionId=@numDivId group by a.numcontactid,
a.vcFirstName,a.vcLastName
GO
