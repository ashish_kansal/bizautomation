/****** Object:  StoredProcedure [dbo].[usp_GetContactUSupportKeyForDivision]    Script Date: 07/26/2008 16:16:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getcontactusupportkeyfordivision')
DROP PROCEDURE usp_getcontactusupportkeyfordivision
GO
CREATE PROCEDURE [dbo].[usp_GetContactUSupportKeyForDivision]
	@numDivID numeric=0
	--
AS
BEGIN
	SELECT ACI.numContactID, ACI.vcFirstName, ACI.vcLastName, UKM.numUniversalSupportKey 
		FROM AdditionalContactsInformation ACI INNER JOIN UniversalSupportKeyMaster UKM ON UKM.numDivisionID=ACI.numDivisionID
		WHERE ACI.numDivisionID=@numDivID
END
GO
