/****** Object:  StoredProcedure [dbo].[USP_ReptTimeExp]    Script Date: 07/26/2008 16:20:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--exec USP_ReptTimeExp 1,'1/1/2000','2/28/2007',2,36,1          
--created by anoop jayaraj          
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_repttimeexp')
DROP PROCEDURE usp_repttimeexp
GO
CREATE PROCEDURE [dbo].[USP_ReptTimeExp]          
@numDomainId as numeric(9)=0,          
@FromDate as datetime,          
@ToDate as datetime,          
@byteMode as tinyint=0,          
@tintType as tinyint,          
@numUserCntID as numeric(9)=0          
as          
          
if @byteMode=1           
begin          
 select numUserID,A.vcFirstName +' '+A.vcLastName as [Name],          
 dbo.fn_GetListItemName(A.vcPosition) as Position,          
 MG.vcFirstName +' '+MG.vcLastName as [MGRName],          
 (select sum(convert(float,DATEDIFF(mi, dtFromDate, dtToDate))/60)          
 from TimeAndExpense where numCategory=1 and numType=1 and A.numContactID=U.numUserDetailId and       
(dtFromDate between @FromDate and @ToDate)and  (dtToDate between @FromDate and @ToDate)) as BillTime,          
 (select sum(convert(float,DATEDIFF(mi, dtFromDate, dtToDate))*monAmount/60)          
 from TimeAndExpense where numCategory=1 and numType=1 and A.numContactID=U.numUserDetailId and       
(dtFromDate between @FromDate and @ToDate)and  (dtToDate between @FromDate and @ToDate)) as BillTimeAmt,          
 (select sum(convert(float,DATEDIFF(mi, dtFromDate, dtToDate))/60)          
 from TimeAndExpense where numCategory=1 and numType=2 and A.numContactID=U.numUserDetailId and       
(dtFromDate between @FromDate and @ToDate)and  (dtToDate between @FromDate and @ToDate)) as NonBillTime,          
 (select sum(convert(float,DATEDIFF(mi, dtFromDate, dtToDate))/60)          
 from TimeAndExpense where numCategory=1 and numType=2 and A.numContactID=U.numUserDetailId and       
(dtFromDate between @FromDate and @ToDate)and  (dtToDate between @FromDate and @ToDate))*monHourlyRate as NonBillTimeAmt,          
 (select sum(monAmount)          
 from TimeAndExpense where numCategory=2 and numType=1 and A.numContactID=U.numUserDetailId and       
(dtFromDate between @FromDate and @ToDate)and  (dtToDate between @FromDate and @ToDate)) as BillExp,          
 (select sum(monAmount)          
 from TimeAndExpense where numCategory=2 and numType=2 and A.numContactID=U.numUserDetailId and       
(dtFromDate between @FromDate and @ToDate)and  (dtToDate between @FromDate and @ToDate)) as NonBillExp,          
 (select sum(datediff(dd,(case when dtFromDate> @FromDate then dtFromDate else @FromDate end ),dateadd(dd,1,(case when dtToDate<@ToDate then dtToDate else @ToDate end)))-          
 (case when bitFromFullDay=1 then 0 else .5 end )-           
 (case when bitToFullDay=1 then 0 else .5 end))            
 from TimeAndExpense where numCategory=3 and bitApproved=0 and A.numContactID=U.numUserDetailId and ((dtFromDate between @FromDate and @ToDate) or (dtToDate between @FromDate and @ToDate))) as NonAppLeave,          
 convert(varchar,(select sum(datediff(dd,(case when dtFromDate> @FromDate then dtFromDate else @FromDate end ),dateadd(dd,1,(case when dtToDate<@ToDate then dtToDate else @ToDate end)))-          
 (case when bitFromFullDay=1 then 0 else .5 end )-           
 (case when bitToFullDay=1 then 0 else .5 end))            
 from TimeAndExpense where numCategory=3   
and bitApproved=1 and A.numContactID=U.numUserDetailId and   
((dtFromDate between @FromDate and @ToDate) or   
(dtToDate between @FromDate and @ToDate))))+  
 case when MG.vcFirstName is null then ',-' else ', '+MG.vcFirstName +' '+  
 MG.vcLastName end  as AppLeave          
 from UserMaster U          
 join AdditionalContactsInformation A          
 on A.numContactID=U.numUserDetailId          
 left join AdditionalContactsInformation MG          
 on MG.numContactID =A.numManagerID where A.numContactID=@numUserCntID and U.numDomainID=@numDomainId          
          
end          
else if @byteMode=2          
begin          
 select numUserID,A.vcFirstName +' '+A.vcLastName as [Name],          
 dbo.fn_GetListItemName(A.vcPosition) as Position,          
 MG.vcFirstName +' '+MG.vcLastName as [MGRName],          
 (select sum(convert(float,DATEDIFF(mi, dtFromDate, dtToDate))/60)          
 from TimeAndExpense where numCategory=1 and numType=1 and A.numContactID=U.numUserDetailId and       
(dtFromDate between @FromDate and @ToDate)and  (dtToDate between @FromDate and @ToDate)) as BillTime,          
 (select sum(convert(float,DATEDIFF(mi, dtFromDate, dtToDate))*monAmount/60)          
 from TimeAndExpense where numCategory=1 and numType=1 and A.numContactID=U.numUserDetailId and       
(dtFromDate between @FromDate and @ToDate)and  (dtToDate between @FromDate and @ToDate)) as BillTimeAmt,          
 (select sum(convert(float,DATEDIFF(mi, dtFromDate, dtToDate))/60)          
 from TimeAndExpense where numCategory=1 and numType=2 and A.numContactID=U.numUserDetailId and       
(dtFromDate between @FromDate and @ToDate)and  (dtToDate between @FromDate and @ToDate)) as NonBillTime,          
 (select sum(convert(float,DATEDIFF(mi, dtFromDate, dtToDate))/60)          
 from TimeAndExpense where numCategory=1 and numType=2 and A.numContactID=U.numUserDetailId and       
(dtFromDate between @FromDate and @ToDate)and  (dtToDate between @FromDate and @ToDate))*monHourlyRate as NonBillTimeAmt,          
 (select sum(monAmount)          
 from TimeAndExpense where numCategory=2 and numType=1 and A.numContactID=U.numUserDetailId and       
(dtFromDate between @FromDate and @ToDate)and  (dtToDate between @FromDate and @ToDate)) as BillExp,          
 (select sum(monAmount)          
 from TimeAndExpense where numCategory=2 and numType=2 and A.numContactID=U.numUserDetailId and       
(dtFromDate between @FromDate and @ToDate)and  (dtToDate between @FromDate and @ToDate)) as NonBillExp,          
 (select sum(datediff(dd,(case when dtFromDate> @FromDate then dtFromDate else @FromDate end ),dateadd(dd,1,(case when dtToDate<@ToDate then dtToDate else @ToDate end)))-          
 (case when bitFromFullDay=1 then 0 else .5 end )-           
 (case when bitToFullDay=1 then 0 else .5 end))            
 from TimeAndExpense where numCategory=3 and bitApproved=0 and A.numContactID=U.numUserDetailId and ((dtFromDate between @FromDate and @ToDate) or (dtToDate between @FromDate and @ToDate))) as NonAppLeave,          
 convert(varchar,(select sum(datediff(dd,(case when dtFromDate> @FromDate then dtFromDate else @FromDate end ),dateadd(dd,1,(case when dtToDate<@ToDate then dtToDate else @ToDate end)))-          
 (case when bitFromFullDay=1 then 0 else .5 end )-           
 (case when bitToFullDay=1 then 0 else .5 end))            
 from TimeAndExpense where numCategory=3 and bitApproved=1 and A.numContactID=U.numUserDetailId   
 and ((dtFromDate between @FromDate and @ToDate) or (dtToDate between @FromDate and @ToDate))))+ case when MG.vcFirstName  
 is null then ',-' else ', '+MG.vcFirstName +' '+MG.vcLastName end  as AppLeave          
 from UserMaster U          
 join AdditionalContactsInformation A          
 on A.numContactID=U.numUserDetailId          
 left join AdditionalContactsInformation MG          
 on MG.numContactID =A.numManagerID where MG.numTeam in(select F.numTeam from ForReportsByTeam F                          
        where F.numUserCntID=@numUserCntID and F.numDomainid=@numDomainId and F.tintType=@tintType)          
          
end
GO
