
GO
/****** Object:  StoredProcedure [dbo].[USP_SendApplicationMail]    Script Date: 06/04/2009 15:16:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created By Anoop jayaraj              
-- exec USP_SendApplicationMail
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_sendapplicationmail')
DROP PROCEDURE usp_sendapplicationmail
GO
CREATE PROCEDURE [dbo].[USP_SendApplicationMail]
              
as              
              
Create table #tempTableSendApplicationMail
(  vcTo varchar(2000),              
   vcSubject varchar(2000),              
   vcBody varchar(Max),              
   vcCC varchar(2000),      
   vcFrom varchar(100),      
   numDomainID numeric(9),
   numConECampDTLID NUMERIC(9) NULL,
   vcPSMTPServer VARCHAR(50) null,
   numPSMTPPort NUMERIC(9),
   vcPSmtpPassword VARCHAR(500) null,
   vcPSMTPUserName VARCHAR(50) null,
   bitPSMTPServer BIT,
   bitPSMTPSSL BIT,
   bitPSMTPAuth BIT
 )                
              
              
----Sending mail to Assignee when the stage past due by fixed days(Opportunity Stages)          
declare @numDomainId as numeric(9)              
declare @numContactID as varchar(15)              
declare @numEmailTemplate as varchar(15)              
declare @tintCCManager as varchar(15)              
declare @numDaysAfterDue as varchar(15)              
declare @numOppStageID as varchar(15)              
declare @numNOppStageID as varchar(15)              
declare @tintAlertOn as varchar(1)              
declare @intCount as integer              
declare @vcSubject as varchar(1000)              
declare @vcBody as varchar(8000)              
declare @to as varchar(100)              
declare @cc as varchar(8000)              
declare @ccManager as varchar(100)              
declare @vcFormattedBody as varchar(8000)              
declare @numDivisionId as varchar(15)             
declare @numAlertDDTLId as numeric(9)          
declare @numDepartMent as varchar(15)            
          
declare @vcPSMTPServer VARCHAR(50)
DECLARE @numPSMTPPort NUMERIC(9)
declare @vcPSmtpPassword VARCHAR(500)
declare @vcPSMTPUserName VARCHAR(50)
declare @bitPSMTPServer BIT
declare @bitPSMTPSSL BIT
declare @bitPSMTPAuth BIT
           
select top 1  @numAlertDDTLId=numAlertDDTLId,@numEmailTemplate=numEmailTemplate,              
@tintCCManager=tintCCManager,@numDomainId= numDomainId,              
@numDaysAfterDue=numDaysAfterDue,@tintAlertOn=tintAlertOn from AlertDTL               
join AlertDomainDTL on AlertDTL.numAlertDTLid=AlertDomainDTL.numAlertDTLid              
where  AlertDomainDTL.numAlertDTLid=3 and tintAlertOn=1 and numEmailTemplate>0          

SELECT @vcPSMTPServer=vcPSMTPServer,@numPSMTPPort=numPSMTPPort,@vcPSmtpPassword=vcPSmtpPassword,@vcPSMTPUserName=vcPSMTPUserName,@bitPSMTPServer=bitPSMTPSSL,@bitPSMTPSSL=bitPSMTPSSL,@bitPSMTPAuth=bitPSMTPAuth  FROM [Domain]
WHERE numDomainId = @numDomainId;
         
while @numAlertDDTLId >0          
begin             
  if @tintAlertOn=1              
  begin              
   declare @vcStagename as varchar(100)              
   declare @vcOppName as varchar(100)              
   declare @numAssignTo as varchar(15)              
   select @vcSubject=vcSubject,@vcBody=vcDocDesc from genericdocuments where numGenericDocID=@numEmailTemplate              
   select top 1 @numOppStageID=numOppStageID,@vcStagename=vcStageDetail,              
   @vcOppName=vcPOppName,@numAssignTo=numAssignTo from OpportunityStageDetails OppStg              
   join OpportunityMaster OppMst              
   on OppMst.numOppId=OppStg.numOppId              
   where OppMst.numDomainid=@numDomainId and  numstagepercentage<>100 and numstagepercentage<>0 and tintOppStatus=0 and bitStageCompleted=0            
   and convert(varchar(11),bintDueDate)=convert(varchar(11),DATEADD(Day, -convert(tinyint,@numDaysAfterDue), getutcdate()))          
                 
   while @numOppStageID>0               
   begin              
   --mail content               
                 
   set @vcFormattedBody=replace(@vcBody,'##Stage##',@vcStagename)              
   set @vcFormattedBody=replace(@vcFormattedBody,'##OppID##',@vcOppName)              
   set @vcFormattedBody=replace(@vcFormattedBody,'##DaysLate##',@numDaysAfterDue)              
                
                 
   set @to=dbo.fn_GetManagerEmail(1,0,@numAssignTo)              
    --exec sendMail_With_CDOMessage  @to , @vcSubject,@vcBody,''              
   insert into #tempTableSendApplicationMail Values ( @to , @vcSubject,@vcFormattedBody,'','',@numDomainId,NULL,@vcPSMTPServer,@numPSMTPPort,@vcPSmtpPassword,@vcPSMTPUserName,@bitPSMTPServer,@bitPSMTPSSL,@bitPSMTPAuth)               
                  
   select top 1 @numOppStageID=numOppStageID from OpportunityStageDetails OppStg              
   join OpportunityMaster OppMst              
   on OppMst.numOppId=OppStg.numOppId              
   where OppMst.numDomainid=@numDomainId and numstagepercentage<>100 and numstagepercentage<>0 and tintOppStatus=0 and bitStageCompleted=0            
   and convert(varchar(11),bintDueDate)=convert(varchar(11),DATEADD(Day, -convert(tinyint,@numDaysAfterDue), getutcdate()))          
   and numOppStageId>@numOppStageID              
   if @@rowcount=0 set @numOppStageID=0            
                 
   end              
                 
   ----Sending mail to Assignee when the stage past due byt fixed days(Project Stages)              
   select @vcSubject=vcSubject,@vcBody=vcDocDesc from genericdocuments where numGenericDocID=@numEmailTemplate              
   select top 1 @numOppStageID=numProStageId,@vcStagename=vcStageDetail,              
   @vcOppName=vcProjectName,@numAssignTo=numAssignTo from ProjectsStageDetails Prostg              
   join ProjectsMaster ProMst              
   on ProMst.numProId=Prostg.numProId              
   where ProMst.numDomainid=@numDomainId and numstagepercentage<>100 and numstagepercentage<>0 and tintproStatus=0 and bitStageCompleted=0            
   and convert(varchar(11),bintDueDate)=convert(varchar(11),DATEADD(Day, -convert(tinyint,@numDaysAfterDue), getutcdate()))          
             
                 
   while @numOppStageID>0               
   begin              
   --mail content               
   set @vcFormattedBody=replace(@vcBody,'##Stage##',@vcStagename)              
   set @vcFormattedBody=replace(@vcFormattedBody,'##OppID##',@vcOppName)              
   set @vcFormattedBody=replace(@vcFormattedBody,'##DaysLate##',@numDaysAfterDue)              
                
   set @to=dbo.fn_GetManagerEmail(1,0,@numAssignTo)              
      -- exec sendMail_With_CDOMessage  @to , @vcSubject,@vcBody,''              
    insert into #tempTableSendApplicationMail Values ( @to , @vcSubject,@vcFormattedBody,'','',@numDomainId,null,@vcPSMTPServer,@numPSMTPPort,@vcPSmtpPassword,@vcPSMTPUserName,@bitPSMTPServer,@bitPSMTPSSL,@bitPSMTPAuth)               
                
   select top 1 @numOppStageID=numProStageId,@vcStagename=vcStageDetail,              
   @vcOppName=vcProjectName,@numAssignTo=numAssignTo  from ProjectsStageDetails Prostg              
   join ProjectsMaster ProMst              
   on ProMst.numProId=Prostg.numProId              
   where ProMst.numDomainid=@numDomainId and numstagepercentage<>100 and numstagepercentage<>0 and tintproStatus=0 and bitStageCompleted=0            
   and convert(varchar(11),bintDueDate)=convert(varchar(11),DATEADD(Day, -convert(tinyint,@numDaysAfterDue), getutcdate()))          
    and numProStageId>@numOppStageID              
   if @@rowcount=0 set @numOppStageID=0              
                 
   end              
  end           
  select top 1  @numAlertDDTLId=numAlertDDTLId,@numEmailTemplate=numEmailTemplate,              
  @tintCCManager=tintCCManager,@numDomainId= numDomainId,              
  @numDaysAfterDue=numDaysAfterDue,@tintAlertOn=tintAlertOn from AlertDTL               
  join AlertDomainDTL on AlertDTL.numAlertDTLid=AlertDomainDTL.numAlertDTLid              
  where  AlertDomainDTL.numAlertDTLid=3 and tintAlertOn=1 and numEmailTemplate>0 and numAlertDDTLId>@numAlertDDTLId          
  if @@rowcount=0 set @numAlertDDTLId=0          
          
end             
             
              
---Sending mail  for Past Due Bills & Invoices               
declare @numAlertDTLid as varchar(15)              
declare @numBizDocs as varchar(15)              
              
declare @numOppBizDocsId as varchar(15)              
declare @DueDate as varchar(15)              
              
            
select top 1 @numAlertDDTLId=numAlertDDTLId,@numAlertDTLid=AlertDTL.numAlertDTLid,              
@numDaysAfterDue=numDaysAfterDue,              
@numEmailTemplate=numEmailTemplate,              
@tintAlertOn=tintAlertOn,              
@numDepartMent=numDepartment,@numDomainId= numDomainId,               
@numBizDocs=numBizDocs              
from AlertDTL               
join AlertDomainDTL on AlertDomainDTL.numAlertDTLid = AlertDTL.numAlertDTLid              
where numAlertID=8 and tintAlertOn=1  and numEmailTemplate>0           
              
while @numAlertDDTLId>0               
begin              
              
if @tintAlertOn=1              
begin               
 declare @BizDocs as varchar(15)              
 declare @contactEmail as varchar(100)              
 select @vcSubject=vcSubject,@vcBody=vcDocDesc from genericdocuments where numGenericDocID=@numEmailTemplate              
 select top 1 @numOppBizDocsId=numOppBizDocsId,@DueDate=bintAccountClosingDate,              
 @numDivisionId=numDivisionId,@BizDocs=numBizDocId               
 from OpportunityBizDocs BD              
 join OpportunityMaster OM on               
 OM.numOppId=BD.numOppId              
 where om.numDomainid=@numDomainId and numBizDocId=@numBizDocs and convert(varchar(11),bintAccountClosingDate)=convert(varchar(11),DATEADD(Day, -convert(numeric,@numDaysAfterDue),   getutcdate()))          
          
               
 while @numOppBizDocsId>0              
 begin              
              
           
  select top 1 @numContactID=numContactID,@contactEmail=vcEmail from AdditionalContactsInformation              
  where numDivisionID=@numDivisionId and  vcDepartment=@numDepartMent and numDomainid=@numDomainId              
                
   while @numContactID>0              
   begin            
               
  ---Mail Content              
    set @vcFormattedBody=replace(@vcBody,'##BizDocName##',dbo.fn_GetListName(@BizDocs,0))              
    set @vcFormattedBody=replace(@vcFormattedBody,'##Organization##',dbo.fn_GetComapnyName(@numDivisionId))              
    set @vcFormattedBody=replace(@vcFormattedBody,'##DaysLate##',@numDaysAfterDue)              
          
             
    set @to=@contactEmail              
          -- exec sendMail_With_CDOMessage  @to , @vcSubject,@vcBody,''              
   insert into #tempTableSendApplicationMail Values ( @to , @vcSubject,@vcFormattedBody,'','',@numDomainId,null,@vcPSMTPServer,@numPSMTPPort,@vcPSmtpPassword,@vcPSMTPUserName,@bitPSMTPServer,@bitPSMTPSSL,@bitPSMTPAuth)               
              
               
              
    select top 1 @numContactID=numContactID,@contactEmail=vcEmail from AdditionalContactsInformation              
    where numDivisionID=@numDivisionId and  vcDepartment=@numDepartMent  and numDomainid=@numDomainId                
    and numContactID>@numContactID              
    if @@rowcount=0 set @numContactID=0              
   end              
               
 select top 1 @numOppBizDocsId=numOppBizDocsId,@DueDate=bintAccountClosingDate,              
 @numDivisionId=numDivisionId,@BizDocs=numBizDocId               
 from OpportunityBizDocs BD              
 join OpportunityMaster OM on               
 OM.numOppId=BD.numOppId              
  where om.numDomainid=@numDomainId and numBizDocId=@numBizDocs and convert(varchar(11),bintAccountClosingDate)=convert(varchar(11),DATEADD(Day, -convert(numeric,@numDaysAfterDue),   getutcdate()))          
 and numOppBizDocsId>@numOppBizDocsId              
 if @@rowcount=0 set @numOppBizDocsId=0              
               
 end              
end              
              
              
              
select top 1 @numAlertDDTLId=numAlertDDTLId,@numAlertDTLid=AlertDTL.numAlertDTLid,              
@numDaysAfterDue=numDaysAfterDue,              
@numEmailTemplate=numEmailTemplate,              
@tintAlertOn=tintAlertOn,@numDomainId= numDomainId,              
@numDepartMent=numDepartment,              
@numBizDocs=numBizDocs from AlertDTL              
join AlertDomainDTL on AlertDomainDTL.numAlertDTLid = AlertDTL.numAlertDTLid              
where numAlertID=8 and tintAlertOn=1   and numEmailTemplate>0          
and AlertDomainDTL.numAlertDDTLId>numAlertDDTLId              
if @@rowcount=0 set @numAlertDDTLId=0              
              
end              
              
             
              
declare @ProID as varchar(15)              
declare @vcProjectname as varchar(100)              
declare @intPrjMgr as varchar(15)              
declare @ExtPrjMgr as varchar(15)              
----sending mail when project is past due date to internal manager              
              
select top 1 @numAlertDDTLId=numAlertDDTLId,@numEmailTemplate=numEmailTemplate,              
@tintCCManager=tintCCManager,@numDomainId= numDomainId,              
@numDaysAfterDue=numDaysAfterDue,@tintAlertOn=tintAlertOn from AlertDTL            
join AlertDomainDTL on AlertDomainDTL.numAlertDTLid = AlertDTL.numAlertDTLid              
where  AlertDomainDTL.numAlertDTLid=12 and tintAlertOn=1 and numEmailTemplate>0          
          
while @numAlertDDTLId>0          
begin          
             
   if @tintAlertOn=1              
   begin              
    select @vcSubject=vcSubject,@vcBody=vcDocDesc from genericdocuments where numGenericDocID=@numEmailTemplate              
                 
    select top 1 @ProID=numProId,@vcProjectname=vcProjectname,@numDivisionId=numDivisionId,@intPrjMgr=numIntPrjMgr           
   from ProjectsMaster where tintProStatus=0 and           
   convert(varchar(11),intDueDate)=convert(varchar(11),DATEADD(Day, -convert(numeric,@numDaysAfterDue),  getutcdate()))          
   and numDomainId= @numDomainId                 
    while @ProID>0              
      begin              
    ---Mail Content              
    set @vcFormattedBody=replace(@vcBody,'##OppID##',@vcProjectname)              
    set @vcFormattedBody=replace(@vcFormattedBody,'##Organization##',dbo.fn_GetComapnyName(@numDivisionId))              
    set @vcFormattedBody=replace(@vcFormattedBody,'##DaysLate##',@numDaysAfterDue)              
                    
    set @to= dbo.fn_GetContactEmail(1,0,@intPrjMgr)              
    if @tintCCManager= 1 set @cc=dbo.fn_GetManagerEmail(1,0,@intPrjMgr)              
    else set @cc=''              
        --exec sendMail_With_CDOMessage  @to , @vcSubject,@vcBody,@cc              
      insert into #tempTableSendApplicationMail Values ( @to , @vcSubject,@vcFormattedBody,@cc,'',@numDomainId,null,@vcPSMTPServer,@numPSMTPPort,@vcPSmtpPassword,@vcPSMTPUserName,@bitPSMTPServer,@bitPSMTPSSL,@bitPSMTPAuth)               
                 
                  
                     
    select top 1 @ProID=numProId,@vcProjectname=vcProjectname,@numDivisionId=numDivisionId,@intPrjMgr=numIntPrjMgr           
   from ProjectsMaster where tintProStatus=0 and          
   convert(varchar(11),intDueDate)=convert(varchar(11),DATEADD(Day, -convert(numeric,@numDaysAfterDue),  getutcdate()))          
   and numDomainId= @numDomainId          
    and numProId>@ProID              
    if @@rowcount=0 set @ProID=0              
      end              
   end          
  select top 1 @numAlertDDTLId=numAlertDDTLId,@numEmailTemplate=numEmailTemplate,              
  @tintCCManager=tintCCManager,@numDomainId= numDomainId,              
  @numDaysAfterDue=numDaysAfterDue,@tintAlertOn=tintAlertOn from AlertDTL               
  join AlertDomainDTL on AlertDomainDTL.numAlertDTLid = AlertDTL.numAlertDTLid              
  where  AlertDomainDTL.numAlertDTLid=12 and numEmailTemplate>0 and tintAlertOn=1 and numAlertDDTLId>@numAlertDDTLId          
  if @@rowcount=0 set @numAlertDDTLId=0          
          
end          
          
              
              
----sending mail when project is past due date to External manager              
              
select top 1 @numAlertDDTLId=numAlertDDTLId,@numEmailTemplate=numEmailTemplate,              
@tintCCManager=tintCCManager,@numDomainId= numDomainId,              
@numDaysAfterDue=numDaysAfterDue,@tintAlertOn=tintAlertOn from AlertDTL               
join AlertDomainDTL on AlertDomainDTL.numAlertDTLid = AlertDTL.numAlertDTLid              
where  AlertDomainDTL.numAlertDTLid=13 and tintAlertOn=1 and numEmailTemplate>0          
          
while   @numAlertDDTLId>0          
begin           
 if @tintAlertOn=1              
 begin              
  select @vcSubject=vcSubject,@vcBody=vcDocDesc from genericdocuments where numGenericDocID=@numEmailTemplate              
               
  select top 1 @ProID=numProId,@vcProjectname=vcProjectname,@numDivisionId=numDivisionId,@ExtPrjMgr=numCustPrjMgr              
  from ProjectsMaster where tintProStatus=0 and           
 convert(varchar(11),intDueDate)=convert(varchar(11),DATEADD(Day, -convert(numeric,@numDaysAfterDue),  getutcdate()))          
 and numDomainId= @numDomainId          
                  
  while @ProID>0              
    begin              
  ---Mail Content              
  set @vcFormattedBody=replace(@vcBody,'##OppID##',@vcProjectname)              
  set @vcFormattedBody=replace(@vcFormattedBody,'##Organization##',dbo.fn_GetComapnyName(@numDivisionId))              
  set @vcFormattedBody=replace(@vcFormattedBody,'##DaysLate##',@numDaysAfterDue)              
                  
  set @to= dbo.fn_GetContactEmail(1,0,@ExtPrjMgr)              
  set @cc=dbo.fn_GetCCEmail(12,@numDomainId)              
      --exec sendMail_With_CDOMessage  @to , @vcSubject,@vcBody,@cc              
    insert into #tempTableSendApplicationMail Values ( @to , @vcSubject,@vcFormattedBody,@cc,'',@numDomainId,null,@vcPSMTPServer,@numPSMTPPort,@vcPSmtpPassword,@vcPSMTPUserName,@bitPSMTPServer,@bitPSMTPSSL,@bitPSMTPAuth)               
            
               
  select top 1 @ProID=numProId,@vcProjectname=vcProjectname,@numDivisionId=numDivisionId,@ExtPrjMgr=numCustPrjMgr              
  from ProjectsMaster where tintProStatus=0 and           
 convert(varchar(11),intDueDate)=convert(varchar(11),DATEADD(Day, -convert(numeric,@numDaysAfterDue),  getutcdate()))          
 and numDomainId= @numDomainId          
  and numProId>@ProID              
  if @@rowcount=0 set @ProID=0              
    end              
 end          
 select top 1 @numAlertDDTLId=numAlertDDTLId,@numEmailTemplate=numEmailTemplate,              
 @tintCCManager=tintCCManager,@numDomainId= numDomainId,          
 @numDaysAfterDue=numDaysAfterDue,@tintAlertOn=tintAlertOn from AlertDTL               
 join AlertDomainDTL on AlertDomainDTL.numAlertDTLid = AlertDTL.numAlertDTLid              
 where  AlertDomainDTL.numAlertDTLid=13 and numEmailTemplate>0 and tintAlertOn=1 and numAlertDDTLId> @numAlertDDTLId          
 if @@rowcount=0 set @numAlertDDTLId=0          
end            
            
              
              
----sending mail when case is past due date               
declare @numCaseId as varchar(15)              
declare @vcCaseNO as varchar(50)              
declare @numRecOwner as varchar(15)              
select top 1 @numAlertDDTLId=numAlertDDTLId,@numAlertDDTLId=numAlertDDTLId,@numEmailTemplate=numEmailTemplate,              
@tintCCManager=tintCCManager,@numDomainId= numDomainId,              
@numDaysAfterDue=numDaysAfterDue,@tintAlertOn=tintAlertOn from AlertDTL              
join AlertDomainDTL on AlertDomainDTL.numAlertDTLid = AlertDTL.numAlertDTLid              
 where  AlertDomainDTL.numAlertDTLid=18  and tintAlertOn=1 and numEmailTemplate>0          
          
while @numAlertDDTLId>0          
begin            
 if @tintAlertOn=1              
 begin              
  select @vcSubject=vcSubject,@vcBody=vcDocDesc from genericdocuments where numGenericDocID=@numEmailTemplate              
               
  select top 1 @numCaseId=numCaseId,@vcCaseNO=vcCaseNumber,@numDivisionID=numDivisionID,@numRecOwner=numCreatedBy              
   from Cases where numStatus<>136 and           
 convert(varchar(11),intTargetResolveDate)=convert(varchar(11),DATEADD(Day, -convert(numeric,@numDaysAfterDue),  getutcdate()))          
 and numDomainId=@numDomainId          
                  
  while @numCaseId>0              
    begin              
  ---Mail Content              
  set @vcFormattedBody=replace(@vcBody,'##CaseID##',@vcCaseNO)              
  set @vcFormattedBody=replace(@vcFormattedBody,'##Organization##',dbo.fn_GetComapnyName(@numDivisionId))              
  set @vcFormattedBody=replace(@vcFormattedBody,'##DaysLate##',@numDaysAfterDue)              
                  
  set @to= dbo.fn_GetContactEmail(1,0,@numRecOwner)              
  if @tintCCManager= 1 set @cc=dbo.fn_GetManagerEmail(0,@numRecOwner,0)              
  else set @cc=''              
      --exec sendMail_With_CDOMessage  @to , @vcSubject,@vcBody,@cc              
  insert into #tempTableSendApplicationMail Values ( @to , @vcSubject,@vcFormattedBody,@cc,'',@numDomainId,null,@vcPSMTPServer,@numPSMTPPort,@vcPSmtpPassword,@vcPSMTPUserName,@bitPSMTPServer,@bitPSMTPSSL,@bitPSMTPAuth)               
                   
  select top 1 @numCaseId=numCaseId,@vcCaseNO=vcCaseNumber,@numDivisionID=numDivisionID,@numRecOwner=numCreatedBy              
   from Cases where numStatus<>136 and          
 convert(varchar(11),intTargetResolveDate)=convert(varchar(11),DATEADD(Day, -convert(numeric,@numDaysAfterDue),  getutcdate()))          
 and numDomainId=@numDomainId          
  and numCaseId>@numCaseId              
  if @@rowcount=0 set @numCaseId=0              
    end              
 end           
 select top 1 @numAlertDDTLId=numAlertDDTLId,@numEmailTemplate=numEmailTemplate,              
 @tintCCManager=tintCCManager,@numDomainId= numDomainId,              
 @numDaysAfterDue=numDaysAfterDue,@tintAlertOn=tintAlertOn from AlertDTL              
 join AlertDomainDTL on AlertDomainDTL.numAlertDTLid = AlertDTL.numAlertDTLid              
 where  AlertDomainDTL.numAlertDTLid=18 and tintAlertOn=1  and numEmailTemplate>0 and  numAlertDDTLId>@numAlertDDTLId          
    if @@rowcount=0 set @numAlertDDTLId=0          
end           
              
            
               
----sending mail to Contacts on fixed age              
declare @numAge as varchar(15)              
declare @vcContactName as varchar(15)                
declare @vcEmail as varchar(100)              
          
select top 1 @numAlertDDTLId=numAlertDDTLId,@numEmailTemplate=numEmailTemplate,              
@tintCCManager=tintCCManager,              
@numAge=numAge,@numDomainId= numDomainId,              
@numDaysAfterDue=numDaysAfterDue,@tintAlertOn=tintAlertOn from AlertDTL              
join AlertDomainDTL on AlertDomainDTL.numAlertDTLid = AlertDTL.numAlertDTLid              
 where  AlertDomainDTL.numAlertDTLid=23 and tintAlertOn=1 and numEmailTemplate>0            
          
while @numAlertDDTLId>0           
begin            
 if @tintAlertOn=1              
 begin              
  select @vcSubject=vcSubject,@vcBody=vcDocDesc from genericdocuments where numGenericDocID=@numEmailTemplate              
               
  select top 1 @numContactId=numContactId,@vcContactName=vcFirstName +' '+vcLastName,              
  @numDivisionID=numDivisionID,@vcEmail=vcEmail from AdditionalContactsInformation               
  where year(getutcdate())-year(bintDOB)=@numAge          
  and numdomainId =@numDomainId                
  while @numContactId>0              
    begin              
  ---Mail Content              
                   
  set @vcFormattedBody=replace(@vcBody,'##ContactName##',@vcContactName)              
  set @vcFormattedBody=replace(@vcFormattedBody,'##Organization##',dbo.fn_GetComapnyName(@numDivisionId))              
  set @vcFormattedBody=replace(@vcFormattedBody,'##Age##',@numAge)              
                  
  set @to= @vcEmail              
  set @cc=dbo.fn_GetCCEmail(23,@numDomainId)              
  if @tintCCManager= 1 set @ccManager=dbo.fn_GetManagerEmail(1,0,@numContactId)              
  else set @ccManager=''              
  if @ccManager<>'' and  @cc<> '' set @cc=@cc+';'+@ccManager              
  if @ccManager<>'' and  @cc= '' set @cc=@ccManager              
      --exec sendMail_With_CDOMessage  @to , @vcSubject,@vcBody,@cc              
  insert into #tempTableSendApplicationMail Values ( @to , @vcSubject,@vcFormattedBody,@cc,'',@numDomainId,null,@vcPSMTPServer,@numPSMTPPort,@vcPSmtpPassword,@vcPSMTPUserName,@bitPSMTPServer,@bitPSMTPSSL,@bitPSMTPAuth)               
               
               
  select top 1 @numContactId=numContactId,@vcContactName=vcFirstName +' '+vcLastName,              
  @numDivisionID=numDivisionID,@vcEmail=vcEmail from AdditionalContactsInformation            
  where year(getutcdate())-year(bintDOB)=@numAge          
  and numdomainId =@numDomainId          
  and numContactId>@numContactId              
  if @@rowcount=0 set @numContactId=0              
  end              
              
 end          
 select top 1 @numAlertDDTLId=numAlertDDTLId,@numEmailTemplate=numEmailTemplate,              
 @tintCCManager=tintCCManager,              
 @numAge=numAge,@numDomainId= numDomainId,              
 @numDaysAfterDue=numDaysAfterDue,@tintAlertOn=tintAlertOn from AlertDTL              
 join AlertDomainDTL on AlertDomainDTL.numAlertDTLid = AlertDTL.numAlertDTLid              
 where  AlertDomainDTL.numAlertDTLid=23 and tintAlertOn=1 and numEmailTemplate>0 and numAlertDDTLId>@numAlertDDTLId          
 if @@rowcount=0 set @numAlertDDTLId=0           
          end          
          
          
          
----Sending mail on every birthday          
select top 1 @numAlertDDTLId=numAlertDDTLId,@numEmailTemplate=numEmailTemplate,              
@tintCCManager=tintCCManager,              
@numAge=numAge,@numDomainId= numDomainId,              
@numDaysAfterDue=numDaysAfterDue,@tintAlertOn=tintAlertOn from AlertDTL              
join AlertDomainDTL on AlertDomainDTL.numAlertDTLid = AlertDTL.numAlertDTLid              
 where  AlertDomainDTL.numAlertDTLid=24 and tintAlertOn=1 and numEmailTemplate>0          
while @numAlertDDTLId>0           
begin            
 if @tintAlertOn=1              
 begin               
                 
    select top 1 @numContactId=numContactId,@vcContactName=vcFirstName +' '+vcLastName,              
    @numDivisionID=numDivisionID,@vcEmail=vcEmail,@numAge=year(getutcdate())-year(bintDOB) from AdditionalContactsInformation               
    where day(bintDOB)=day(getutcdate()) and month(bintDOB)=month(getutcdate())          
  and numDomainId=@numDomainId              
                    
    while @numContactId>0              
   begin              
    set @vcFormattedBody=replace(@vcBody,'##ContactName##',@vcContactName)              
    set @vcFormattedBody=replace(@vcFormattedBody,'##Organization##',dbo.fn_GetComapnyName(@numDivisionId))              
    set @vcFormattedBody=replace(@vcFormattedBody,'##Age##',@numAge)              
                    
    set @to= @vcEmail              
    set  @cc=dbo.fn_GetCCEmail(24,@numDomainId)              
                     
    if @tintCCManager= 1 set @ccManager=dbo.fn_GetManagerEmail(1,0,@numContactId)              
    else set @ccManager=''              
    if @ccManager<>'' and  @cc<> '' set @cc=@cc+';'+@ccManager              
    if @ccManager<>'' and  @cc= '' set @cc=@ccManager              
     --exec sendMail_With_CDOMessage  @to , @vcSubject,@vcBody,@cc              
  insert into #tempTableSendApplicationMail Values ( @to , @vcSubject,@vcFormattedBody,@cc,'',@numDomainId,null,@vcPSMTPServer,@numPSMTPPort,@vcPSmtpPassword,@vcPSMTPUserName,@bitPSMTPServer,@bitPSMTPSSL,@bitPSMTPAuth)               
                     
    select top 1 @numContactId=numContactId,@vcContactName=vcFirstName +' '+vcLastName,              
    @numDivisionID=numDivisionID,@vcEmail=vcEmail,@numAge=year(getutcdate())-year(bintDOB) from AdditionalContactsInformation               
    where day(bintDOB)=day(getutcdate()) and month(bintDOB)=month(getutcdate()) and numDomainId=@numDomainId             
    and numContactId>@numContactId              
    if @@rowcount=0 set @numContactId=0              
               
   end              
  end          
          
 select top 1 @numAlertDDTLId=numAlertDDTLId,@numEmailTemplate=numEmailTemplate,              
 @tintCCManager=tintCCManager,              
 @numAge=numAge,@numDomainId= numDomainId,              
 @numDaysAfterDue=numDaysAfterDue,@tintAlertOn=tintAlertOn from AlertDTL              
 join AlertDomainDTL on AlertDomainDTL.numAlertDTLid = AlertDTL.numAlertDTLid              
  where  AlertDomainDTL.numAlertDTLid=24 and tintAlertOn=1 and numEmailTemplate>0 and numAlertDDTLId>@numAlertDDTLId          
 if @@rowcount=0 set @numAlertDDTLId=0          
          
end              
              
           
          
              
----sending mail when items reaches reorder point              
declare @numWItemCode as varchar(15)              
declare @vcPartNO as varchar(100)              
declare @vcItemName as varchar(100)              
declare @numQtyOnHand as varchar(15)              
declare @Warehouse as varchar(50)           
select top 1 @numAlertDDTLId=numAlertDDTLId,@numEmailTemplate=numEmailTemplate,              
@tintCCManager=tintCCManager,@numDomainId= numDomainId,              
@numDaysAfterDue=numDaysAfterDue,@tintAlertOn=tintAlertOn from AlertDTL              
join AlertDomainDTL on AlertDomainDTL.numAlertDTLid = AlertDTL.numAlertDTLid              
 where  AlertDomainDTL.numAlertDTLid=25 and tintAlertOn=1 and numEmailTemplate>0          
          
while @numAlertDDTLId>0          
begin          
 if @tintAlertOn=1              
  begin              
   select @vcSubject=vcSubject,@vcBody=vcDocDesc from genericdocuments where numGenericDocID=@numEmailTemplate               
          
   select top 1 @numWItemCode=numWareHouseItemID,@Warehouse=vcWareHouse,              
   @vcItemName=vcItemName,@numQtyOnHand=numOnHand  from item I           
   join WareHouseItems           
   on I.numItemCode = numItemID          
  join Warehouses W          
  on W.numWareHouseID=WareHouseItems.numWareHouseID           
   where numReorder>=numOnHand   and I.numDomainId=@numDomainId           
               
   while @numWItemCode>0              
     begin              
   print @vcItemName          
     set @vcFormattedBody=replace(@vcBody,'##ItemName##',@vcItemName)                        
     set @vcFormattedBody=replace(@vcFormattedBody,'##Units##',@numQtyOnHand)              
     set @vcFormattedBody=replace(@vcFormattedBody,'##Warehouse##',@Warehouse)          
     set @to= dbo.fn_GetCCEmail(25,@numDomainId)              
      --exec sendMail_With_CDOMessage  @to , @vcSubject,@vcBody,''              
   insert into #tempTableSendApplicationMail Values ( @to , @vcSubject,@vcFormattedBody,'','',@numDomainId,null,@vcPSMTPServer,@numPSMTPPort,@vcPSmtpPassword,@vcPSMTPUserName,@bitPSMTPServer,@bitPSMTPSSL,@bitPSMTPAuth)               
           
   select top 1 @numWItemCode=numWareHouseItemID,@Warehouse=vcWareHouse,          
   @vcItemName=vcItemName,@numQtyOnHand=numOnHand  from item I           
   join WareHouseItems           
  on I.numItemCode = numItemID           
  join Warehouses W          
  on W.numWareHouseID=WareHouseItems.numWareHouseID           
                
   where numReorder>=numOnHand and numWareHouseItemID>@numWItemCode    and I.numDomainId=@numDomainId          
   if @@rowcount=0 set @numWItemCode=0              
     end              
  end            
          
          
 select top 1 @numAlertDDTLId=numAlertDDTLId,@numEmailTemplate=numEmailTemplate,              
 @tintCCManager=tintCCManager,@numDomainId= numDomainId,              
 @numDaysAfterDue=numDaysAfterDue,@tintAlertOn=tintAlertOn from AlertDTL              
 join AlertDomainDTL on AlertDomainDTL.numAlertDTLid = AlertDTL.numAlertDTLid              
  where  AlertDomainDTL.numAlertDTLid=25 and tintAlertOn=1 and numEmailTemplate>0 and numAlertDDTLId>@numAlertDDTLId          
 if @@rowcount=0 set @numAlertDDTLId=0          
end            
          
          
          
          
 ----sending mailto contacts in vendor list             
set @numContactID=0          
select top 1 @numAlertDDTLId=numAlertDDTLId,@numEmailTemplate=numEmailTemplate,              
@numDomainId= numDomainId,@tintAlertOn=tintAlertOn,@numDepartment=numDepartMent from AlertDTL               
join AlertDomainDTL on AlertDomainDTL.numAlertDTLid = AlertDTL.numAlertDTLid              
where  AlertDomainDTL.numAlertDTLid=26 and tintAlertOn=1 and numEmailTemplate>0          
while @numAlertDDTLId>0          
begin          
 if @tintAlertOn=1              
  begin           
   select @vcSubject=vcSubject,@vcBody=vcDocDesc from genericdocuments where numGenericDocID=@numEmailTemplate               
   select top 1 @numWItemCode=numWareHouseItemID,@vcPartNO=vcPartNo,@numDivisionID=I.numVendorID,              
   @vcItemName=vcItemName,@numQtyOnHand=numOnHand  from item I              
   join Vendor V              
   on I.numItemCode=V.numItemCode            
   join WareHouseItems           
   on I.numItemCode = numItemID            
   where numReorder>=numOnHand and I.numDomainId=@numDomainId           
          
   while @numWItemCode>0              
    begin           
    print @numDepartment           
    print @numDivisionId           
             
    set @vcFormattedBody=replace(@vcBody,'##ItemName##',@vcItemName)              
    set @vcFormattedBody=replace(@vcFormattedBody,'##PartNo##',isnull(@vcPartNO,''))              
    set @vcFormattedBody=replace(@vcFormattedBody,'##Vendor##',isnull(dbo.fn_GetComapnyName(@numDivisionId),''))                
    set @vcFormattedBody=replace(@vcFormattedBody,'##Units##',@numQtyOnHand)              
      
    select top 1 @numContactID=numContactID,@contactEmail=vcEmail from AdditionalContactsInformation              
    where numDivisionID=@numDivisionId and numdomainId=@numDomainId   and vcDepartment =@numDepartment           
                       
    while @numContactID>0              
     begin           
      print @numContactID              
      set @to=@contactEmail
      
                    
      insert into #tempTableSendApplicationMail Values ( @to , @vcSubject,@vcFormattedBody,'','',@numDomainId,NULL,@vcPSMTPServer,@numPSMTPPort,@vcPSmtpPassword,@vcPSMTPUserName,@bitPSMTPServer,@bitPSMTPSSL,@bitPSMTPAuth)
      select top 1 @numContactID=numContactID,@contactEmail=vcEmail from AdditionalContactsInformation              
      where numDivisionID=@numDivisionId  and vcDepartment =@numDepartment             
      and numContactID>@numContactID              
      if @@rowcount=0 set @numContactID=0              
     end          
     select top 1 @numWItemCode=numWareHouseItemID,@vcPartNO=vcPartNo,@numDivisionID=I.numVendorID,              
     @vcItemName=vcItemName,@numQtyOnHand=numOnHand  from item I              
     join Vendor V              
     on I.numItemCode=V.numItemCode            
     join WareHouseItems           
     on I.numItemCode = numItemID          
     where numReorder>=numOnHand    and I.numDomainId=@numDomainId and numWareHouseItemID>@numWItemCode          
     if @@rowcount=0 set @numWItemCode=0              
    end                  
            
          
  end          
  select top 1 @numAlertDDTLId=numAlertDDTLId,@numEmailTemplate=numEmailTemplate,              
  @numDomainId= numDomainId,@tintAlertOn=tintAlertOn,@numDepartment=numDepartMent from AlertDTL               
  join AlertDomainDTL on AlertDomainDTL.numAlertDTLid = AlertDTL.numAlertDTLid              
   where  AlertDomainDTL.numAlertDTLid=26 and tintAlertOn=1 and numEmailTemplate>0  and numAlertDDTLId>@numAlertDDTLId          
  if @@rowcount=0 set @numAlertDDTLId=0          
              
 end               
                
          
          
---For Contracts Emails        
                   
        
declare @numContractID as numeric         
declare @vcContractName as varchar(200)           
declare @RemHours as numeric            
declare @RemAmount as numeric            
declare @Days as numeric            
declare @Incidents as numeric            
declare @numEmaildays as numeric            
declare @numemailIncidents as numeric               
declare @numHours as numeric            
declare @bitEHour as bit            
declare @bitEIncidents  as bit            
declare @bitEDays  as bit            
declare @vcTo  as varchar(200)            
             
            
            
select top 1 @numContractID=numcontractId, @vcContractName=vcContractName,@numDomainId=numDomainID,           
----------script to find remaining hours                               
@RemHours=case when bitHour=1 then                                             
dbo.getContractRemainingHrsAmt(1,c.numContractID)                                
else                              
'0'                              
end ,                                   
-----------------------------------------------                                  
------------script to find remaining Amount                              
@RemAmount=case when bitamount=1                              
then                                          
dbo.getContractRemainingHrsAmt(0,c.numContractID)         
else                              
'0'                             
end  ,                                             
-----------------------------------------------                              
-----------remaining days                            
@Days=case when bitdays =1 then                              
isnull(convert(varchar(100),                            
case when datediff(day,getutcdate(),bintExpdate)>=0 then datediff(day,getutcdate(),bintExpdate) else '0' end ),0)                            
                            
else '0' end ,                             
                            
---------------------------------------------                            
@Incidents=case when bitincidents =1 then                              
convert(varchar(100),convert(decimal(10,2),numincidents) - convert(decimal(10,2),dbo.GetIncidents(numContractID,numDomainId ,c.numdivisionid )))                            
else '0' end                            
--------------------------------              
,@numEmailTemplate=numTemplateId            
,@numEmaildays=numEmaildays,@numemailIncidents=numemailIncidents,@numHours=numHours ,@bitEHour=bitEHour ,@bitEIncidents=bitEIncidents             
,@bitEDays=bitEDays             
,@vcTo=dbo.GetContractsEmails(numContractID)            
from ContractManagement c            
where (bitEHour=1 or bitEIncidents=1 or bitEDays=1 )            
            
            
while @numContractID > 0            
begin            
          
   select @vcSubject=vcSubject,@vcBody=vcDocDesc from genericdocuments where numGenericDocID=@numEmailTemplate             
   if (@RemHours  = @bitEHour) or (@Incidents = @bitEIncidents ) or (@Days  = @bitEDays )        
   begin          
        
  set @vcFormattedBody   = replace(@vcBody,'##RemHours##',@RemHours)        
 set @vcFormattedBody   = replace(@vcFormattedBody,'##Contract##',@vcContractName)        
 set @vcFormattedBody   = replace(@vcFormattedBody,'##RemIncidents##',@Incidents)        
 set @vcFormattedBody   = replace(@vcFormattedBody,'##RemDays##',@Days)        
    insert into #tempTableSendApplicationMail Values (@vcto , @vcSubject,@vcFormattedBody,'','',@numDomainId,null,@vcPSMTPServer,@numPSMTPPort,@vcPSmtpPassword,@vcPSMTPUserName,@bitPSMTPServer,@bitPSMTPSSL,@bitPSMTPAuth)             
   end            
            
-- End            
            
  select top 1 @numContractID=numcontractId, @vcContractName=vcContractName ,@numDomainId=numDomainID,               
  ----------script to find remaining hours                               
  @RemHours=case when bitHour=1 then                                             
 dbo.getContractRemainingHrsAmt(0,c.numContractID)                         
                              
  else                              
  '0'                              
  end ,                                   
  -----------------------------------------------                                  
  ------------script to find remaining Amount                              
  @RemAmount=case when bitamount=1                              
  then                                          
 dbo.getContractRemainingHrsAmt(1,c.numContractID)         
  else                              
  '0'                             
  end  ,                                             
  -----------------------------------------------                              
  -----------remaining days                            
  @Days=case when bitdays =1 then                              
  isnull(convert(varchar(100),                    
  case when datediff(day,getutcdate(),bintExpdate)>=0 then datediff(day,getutcdate(),bintExpdate) else '0' end ),0)                            
                              
  else '0' end ,                             
                              
  ---------------------------------------------                            
  @Incidents=case when bitincidents =1 then                              
  convert(varchar(100),convert(decimal(10,2),numincidents) - convert(decimal(10,2),dbo.GetIncidents(numContractID,numDomainId ,c.numdivisionid )))                            
  else '0' end                            
  --------------------------------             
  ,@numEmailTemplate=numTemplateId             
  ,@numEmaildays=numEmaildays,@numemailIncidents=numemailIncidents,@numHours=numHours ,@bitEHour=bitEHour ,@bitEIncidents=bitEIncidents             
  ,@bitEDays=bitEDays             
  ,@vcTo=dbo.GetContractsEmails(numContractID)            
  from ContractManagement c            
  where (bitEHour=1 or bitEIncidents=1 or bitEDays=1 )            
     and numContractId>@numContractId              
     if @@rowcount=0 set @numContractId=0              
              
end         
      
----Email Campaign      
    --Commented  Because Same code is written in USP_GetEmailCampaignMails ,Which is  used to send mail through email server
--      
--DECLARE  @numConEmailCampID  AS NUMERIC(9)
--DECLARE  @intStartDate  AS DATETIME
--DECLARE  @vcCompanyName  AS VARCHAR(50)
--DECLARE  @vcFirstName  AS VARCHAR(50)
--DECLARE  @vcLastName  AS VARCHAR(50)
--DECLARE  @numPhone  AS VARCHAR(15)
--DECLARE  @numConECampDTLID  AS NUMERIC(9)
--DECLARE  @vcFrom  AS VARCHAR(64)
--SELECT   TOP 1 @numContactID = C.numContactID,
--               @numConECampDTLID = numConECampDTLID,
--               @numConEmailCampID = numConEmailCampID,
--               @intStartDate = intStartDate,
--               @numEmailTemplate = numEmailTemplate,
--               @numDomainId = A.numDomainID,
--               @vcFrom = A.vcEmail
--FROM     ConECampaign C
--         JOIN ConECampaignDTL DTL
--           ON numConEmailCampID = numConECampID
--         JOIN ECampaignDTLs E
--           ON DTL.numECampDTLId = E.numECampDTLID
--         JOIN AdditionalContactsInformation A
--           ON A.numContactID = C.numRecOwner
--WHERE    bitSend IS NULL 
--         AND bitengaged = 1
--         AND numEmailTemplate > 0
--         AND CONVERT(CHAR(10),dateadd(DAY,tintDays
--                                            - 1,intStartDate),
--                     110) = CONVERT(CHAR(10),getutcdate(),110)
--ORDER BY numConEmailCampID
--PRINT '@numConEmailCampID='
--PRINT @numConEmailCampID
--
--WHILE @numConEmailCampID > 0
--  BEGIN
--	PRINT 'inside @numConEmailCampID='
--	PRINT @numConEmailCampID
--
--    SELECT @vcFirstName = A.vcFirstName,
--           @vcLastName = A.vcLastName,
--           @vcCompanyName = C.vcCompanyName,
--           @numPhone = A.numPhone,
--           @vcEmail = A.vcEmail
--    FROM   AdditionalContactsInformation A
--           JOIN DivisionMaster D
--             ON A.numDivisionId = D.numDivisionID
--           JOIN CompanyInfo C
--             ON D.numCompanyID = C.numCompanyId
--    WHERE  A.numContactId = @numContactID
--    
--    SELECT @vcSubject = vcSubject,
--           @vcBody = vcDocDesc
--    FROM   genericdocuments
--    WHERE  numGenericDocID = @numEmailTemplate
--    
--    SET @vcFormattedBody = REPLACE(@vcBody,'##Organization##',@vcCompanyName)
--    SET @vcFormattedBody = REPLACE(@vcFormattedBody,'##vcFirstName##',@vcFirstName)
--    SET @vcFormattedBody = REPLACE(@vcFormattedBody,'##vcLastName##',@vcLastName)
--    SET @vcFormattedBody = REPLACE(@vcFormattedBody,'##Phone##',@numPhone)
--    SET @vcFormattedBody = REPLACE(@vcFormattedBody,'##Email##',@vcEmail)
--    
--    INSERT INTO #tempTableSendApplicationMail
--    VALUES     (@vcEmail,
--                @vcSubject,
--                @vcFormattedBody,
--                '',
--                @vcFrom,
--                @numDomainId,
--                @numConECampDTLID,@vcPSMTPServer,@numPSMTPPort,@vcPSmtpPassword,@vcPSMTPUserName,@bitPSMTPServer,@bitPSMTPSSL,@bitPSMTPAuth)
--                
----    UPDATE ConECampaignDTL
----    SET    bitSend = 1,
----           bintSentON = getutcdate()
----    WHERE  numConECampDTLID = @numConECampDTLID
--    
--    SELECT   TOP 1 @numContactID = C.numContactID,
--                   @numConECampDTLID = numConECampDTLID,
--                   @numConEmailCampID = numConEmailCampID,
--                   @intStartDate = intStartDate,
--                   @numEmailTemplate = numEmailTemplate,
--                   @numDomainId = numDomainID,
--                   @vcFrom = vcEmail
--    FROM     ConECampaign C
--             JOIN ConECampaignDTL DTL
--               ON numConEmailCampID = numConECampID
--             JOIN ECampaignDTLs E
--               ON DTL.numECampDTLId = E.numECampDTLID
--             JOIN AdditionalContactsInformation A
--               ON A.numContactID = C.numRecOwner
--    WHERE    bitSend IS NULL 
--             AND bitengaged = 1
--             AND numConEmailCampID > @numConEmailCampID
--             AND CONVERT(CHAR(10),dateadd(DAY,tintDays
--                                                - 1,intStartDate),
--                         110) = CONVERT(CHAR(10),getutcdate(),110)
--    ORDER BY numConEmailCampID
--    
--    IF @@ROWCOUNT = 0
--      SET @numConEmailCampID = 0
--  END
   

/*Sending EMail When lead is created 
- this Should be done same time when lead is created
*/
/*Start Sending EMail From E-Campaign When "web" lead is created 
- this Is Scheduled Task 
- Start sending mails from very next day as specified in E-Campaign 
- Look for Lead Created Date for Referece e.g. SELECT [bintCreatedDate],* FROM [CompanyInfo] 
- For each Email Tempelate in E-Campaign Check if Date Of Sending Template is Today 
- If any records found then add to Temperory Table
- Mark That E-Mail Tempelate as Sent with Sent DateTime
*/
--
--DECLARE @numEmailCampaignId numeric(9);
--SELECT @numEmailCampaignId=numEmailCampaignId
--FROM   [AlertDomainDtl]
--WHERE  [numEmailCampaignId] > 0
--       AND tintAlertOn = 1
--       AND [numAlertDTLid] = 47 -- IN (SELECT [numAlertDTLid] FROM   [AlertDTL] WHERE  [numAlertID] = 2)/*New WebLead  Arrival=2 in [AlertHDR] table*/
--
----For Each Campiagn loop through 10 diff template
--SELECT [tintDays],[numEmailTemplate],*
--FROM   [ECampaignDTLs]
--WHERE  [numECampID] = 10 --@numEmailCampaignId
--       AND [numEmailTemplate] > 0
--
----get Template Data
--SELECT @vcSubject=[vcSubject],@vcFormattedBody=[vcDocDesc],*
--FROM   [GenericDocuments]
--WHERE  [numGenericDocID] = 104--numEmailTemplate;
--
--insert into #tempTableSendApplicationMail Values (@vcEmail, @vcSubject,@vcFormattedBody,'',@vcFrom,@numDomainId,null)            
--
--
--SELECT [bintCreatedDate],* FROM [CompanyInfo] WHERE [bintCreatedDate] > GETDATE() - 1
--
----ForEach
--SELECT tintAlertOn,
--       numEmailCampaignId
--FROM   AlertDomainDtl
--WHERE  [tintAlertOn] = 1
--       AND [numEmailCampaignId] > 0
--       AND [numDomainId] = 1
--
--SELECT [bintCreatedDate],* FROM [CompanyInfo] --WHERE [bintCreatedDate]

----      
------Sending Mail of opporunity Stages    
----    
----    
----    
----declare @vcPOppName as varchar(100)    
----declare @numOppId as numeric(9)    
----declare @Stage as varchar(100)    
----declare @Amount as DECIMAL(20,5)    
----declare @numChgStatus as numeric(9)  
----declare @bitChgStatus as bit  
----declare @bitClose as bit  
----declare @bitStageCompleted as numeric(9)  
----Create table #OppClosed       
----(    
---- numOppId numeric(9)              
---- )    
----  
----insert #OppClosed  
----select distinct(numOppId) from OpportunityStageDetails where numstagepercentage in(0,100) and bitStageCompleted = 1  
----  
----select top 1 @numOppStageId=numOppStageId,@vcPOppName=vcPOppName,@Stage=vcStageDetail,@numcontactId=numcontactId,    
----   @numAssignTo=numAssignTo,@numDomainId=oppMas.numDomainId,@numOppId=OppStage1.numOppId,@numEmailTemplate=numET    
----  ,@Amount=monPAmount ,@numChgStatus = numChgStatus,@bitChgStatus=bitChgStatus,@bitClose=bitClose,@bitStageCompleted=bitStageCompleted  
----from OpportunityStageDetails OppStage1     
---- join OpportunityMaster OppMas  on OppStage1.numOppId = OppMas.numOppId    
---- where numEvent = 1 and numStartDate <> 0 and numStagePErcentage not in (0,100) and numET <> 0   and OppStage1.numOppId not in (select numOppId from #OppClosed)  
---- and    
----convert(varchar(11),getdate()) =       
----   case when numStartDate = 1 then     
----     convert(varchar(11),bintduedate)    
----       when numStartDate = 2 then         
----     convert(varchar(11),OppStage1.bintCreatedDate)     
----    else    
----     convert(varchar(11),dateadd(day,numStartDate-3,     
----     (    
----      select bintStagecomDate from  OpportunityStageDetails where numOppStageId = dbo.fn_getOppPreviousStage(OppStage1.numOppId,OppStage1.numOppStageId)    
----      and numStagePErcentage <> 0 and numOppId = OppStage1.numOppId and bitStageCompleted =1    
----      )))    
----   end    
----order by numOppStageId,OppStage1.numOppId    
----    
----    
----while @numOppStageId > 0    
----begin    
----   select @vcSubject=vcSubject,@vcBody=vcDocDesc from genericdocuments where numGenericDocID=@numEmailTemplate     
----        
----    select top 1 @numcontactId=numContactID,@contactEmail=vcEmail,    
----    @vcContactName=vcFirstName +' '+vcLastName,@numDivisionId=numDivisionId     
----   from AdditionalContactsInformation              
----    where numcontactId = @numcontactId and  numDomainid=@numDomainId      
----      
---- select top 1 @vcFrom=vcEmail  
---- from AdditionalContactsInformation              
----    where numcontactId = @numAssignTo and  numDomainid=@numDomainId  
----    
----    set @vcFormattedBody=replace(@vcBody,'##OppID##',@vcPOppName)      
---- set @vcFormattedBody=replace(@vcFormattedBody,'##Stage##',@Stage)           
----    set @vcFormattedBody=replace(@vcFormattedBody,'##Organization##',dbo.fn_GetComapnyName(@numDivisionId))      
----    set @vcFormattedBody=replace(@vcFormattedBody,'##vcCompanyName##',dbo.fn_GetComapnyName(@numDivisionId))     
----    set @vcFormattedBody=replace(@vcFormattedBody,'##ContactName##',@vcContactName)              
---- set @vcFormattedBody=replace(@vcFormattedBody,'##Email##',@contactEmail)      
----    
----  insert into #tempTableSendApplicationMail Values (@contactEmail, @vcSubject,@vcFormattedBody,'',@vcFrom,@numDomainId,null)     
----     
---- if @bitChgStatus = 1 and @numChgStatus <> 0  
----  update OpportunityStageDetails set numStage = @numChgStatus  where numOppStageId=@numOppStageId  
---- if @bitClose = 1 and @bitStageCompleted = 0  
----  update OpportunityStageDetails set bitStageCompleted = 1 ,bintStageComDate = getutcdate()  where numOppStageId=@numOppStageId  
----  
----    
---- select top 1 @numOppStageId=numOppStageId,@vcPOppName=vcPOppName,@Stage=vcStageDetail,@numcontactId=numcontactId,    
----    @numAssignTo=numAssignTo,@numDomainId=oppMas.numDomainId,@numOppId=OppStage1.numOppId,@numEmailTemplate=numET    
----    ,@Amount=monPAmount ,@numChgStatus = numChgStatus,@bitChgStatus=bitChgStatus,@bitClose=bitClose,@bitStageCompleted=bitStageCompleted  
---- from OpportunityStageDetails OppStage1     
----  join OpportunityMaster OppMas  on OppStage1.numOppId = OppMas.numOppId    
----  where numEvent = 1 and numStartDate <> 0 and numStagePErcentage not in (0,100) and numET <> 0    
----  and OppStage1.numOppId not in (select numOppId from #OppClosed)  
----  and    
---- convert(varchar(11),getdate()) =       
----    case when numStartDate = 1 then     
----      convert(varchar(11),bintduedate)    
----     when numStartDate = 2 then         
----      convert(varchar(11),OppStage1.bintCreatedDate)     
----     else    
----      convert(varchar(11),dateadd(day,numStartDate-3,     
----      (    
----       select bintStagecomDate from  OpportunityStageDetails where numOppStageId = dbo.fn_getOppPreviousStage(OppStage1.numOppId,OppStage1.numOppStageId)  
----       and numStagePErcentage <> 0 and numOppId = OppStage1.numOppId and bitStageCompleted =1    
----       )))    
----    end    
----    
---- and numOppStageId>@numOppStageId       
---- order by numOppStageId,OppStage1.numOppId    
---- if @@rowcount=0 set @numOppStageID=0      
----     
----end    
----drop table #OppClosed  
----  
------Sending Project Process Mails  
----  
----    
----    
------declare @vcProjectName as varchar(100)    
----declare @numProId as numeric(9)    
------declare @Stage as varchar(100)     
----declare @numProStageId as numeric(9)  
------declare @bitChgStatus as bit  
------declare @bitClose as bit  
------declare @bitStageCompleted as numeric(9)  
----Create table #ProClosed       
----(    
---- numProId numeric(9)              
---- )    
----  
----insert #ProClosed  
----select distinct(numProId) from ProjectsStageDetails where numstagepercentage in(0,100) and bitStageCompleted = 1  
----  
----select top 1 @numProStageId=numProStageId,@vcProjectName=vcProjectName,@Stage=vcStageDetail,@numcontactId=numCustPrjMgr,    
----   @numAssignTo=numAssignTo,@numDomainId=ProMas.numDomainId,@numProId=ProStage1.numProId,@numEmailTemplate=numET    
---- ,@numChgStatus = numChgStatus,@bitChgStatus=bitChgStatus,@bitClose=bitClose,@bitStageCompleted=bitStageCompleted  
----from ProjectsStageDetails ProStage1     
---- join ProjectsMaster ProMas  on ProStage1.numProId = ProMas.numProId    
---- where numEvent = 1 and numStartDate <> 0 and numStagePErcentage not in (0,100) and numET <> 0   and   
----ProStage1.numProId not in (select numProId from #ProClosed)  
---- and    
----convert(varchar(11),getdate()) =       
----   case when numStartDate = 1 then     
----     convert(varchar(11),bintduedate)    
----       when numStartDate = 2 then         
----     convert(varchar(11),ProStage1.bintCreatedDate)     
----    else    
----     convert(varchar(11),dateadd(day,numStartDate-3,     
----     (    
----      select bintStagecomDate from  ProjectsStageDetails where numProStageId = dbo.fn_getProPreviousStage(ProStage1.numProId,ProStage1.numProStageId)    
----      and numStagePErcentage <> 0 and numProId = ProStage1.numProId and bitStageCompleted =1    
----      )))    
----   end    
----order by numProStageId,ProStage1.numProId    
----    
----    
----while @numProStageId > 0    
----begin    
----   select @vcSubject=vcSubject,@vcBody=vcDocDesc from genericdocuments where numGenericDocID=@numEmailTemplate     
----        
----    select top 1 @numcontactId=numContactID,@contactEmail=vcEmail,    
----    @vcContactName=vcFirstName +' '+vcLastName,@numDivisionId=numDivisionId     
----   from AdditionalContactsInformation              
----    where numcontactId = @numcontactId and  numDomainid=@numDomainId      
----      
---- select top 1 @vcFrom=vcEmail  
---- from AdditionalContactsInformation              
----    where numcontactId = @numAssignTo and  numDomainid=@numDomainId  
----    
----    set @vcFormattedBody=replace(@vcBody,'##OppID##',@vcProjectName)      
---- set @vcFormattedBody=replace(@vcFormattedBody,'##Stage##',@Stage)           
----    set @vcFormattedBody=replace(@vcFormattedBody,'##Organization##',dbo.fn_GetComapnyName(@numDivisionId))      
----    set @vcFormattedBody=replace(@vcFormattedBody,'##vcCompanyName##',dbo.fn_GetComapnyName(@numDivisionId))     
----    set @vcFormattedBody=replace(@vcFormattedBody,'##ContactName##',@vcContactName)              
---- set @vcFormattedBody=replace(@vcFormattedBody,'##Email##',@contactEmail)      
----    
----  insert into #tempTableSendApplicationMail Values (@contactEmail, @vcSubject,@vcFormattedBody,'',@vcFrom,@numDomainId,null)     
----     
----  
----   
---- if @bitChgStatus = 1 and @numChgStatus <> 0  
----  begin  
----    
----   update ProjectsStageDetails set numStage = @numChgStatus  where numProStageId=@numProStageId  
----  end  
---- if @bitClose = 1 and @bitStageCompleted = 0  
----  begin  
----   
----   update ProjectsStageDetails set bitStageCompleted = 1 ,bintStageComDate = getutcdate()  where numProStageId=@numProStageId  
----  End  
----  
----    
---- select top 1 @numProStageId=numProStageId,@vcProjectName=vcProjectName,@Stage=vcStageDetail,@numcontactId=numCustPrjMgr,    
----    @numAssignTo=numAssignTo,@numDomainId=ProMas.numDomainId,@numProId=ProStage1.numProId,@numEmailTemplate=numET    
----    ,@numChgStatus = numChgStatus,@bitChgStatus=bitChgStatus,@bitClose=bitClose,@bitStageCompleted=bitStageCompleted  
---- from ProjectsStageDetails ProStage1     
----  join ProjectsMaster ProMas  on ProStage1.numProId = ProMas.numProId    
----  where numEvent = 1 and numStartDate <> 0 and numStagePErcentage not in (0,100) and numET <> 0    
----  and ProStage1.numProId not in (select numProId from #ProClosed)  
----  and    
---- convert(varchar(11),getdate()) =       
----    case when numStartDate = 1 then     
----      convert(varchar(11),bintduedate)    
----     when numStartDate = 2 then         
----      convert(varchar(11),ProStage1.bintCreatedDate)     
----     else    
----      convert(varchar(11),dateadd(day,numStartDate-3,     
----      (    
----       select bintStagecomDate from  ProjectsStageDetails where numProStageId = dbo.fn_getProPreviousStage(ProStage1.numProId,ProStage1.numProStageId)  
----       and numStagePErcentage <> 0 and numProId = ProStage1.numProId and bitStageCompleted =1    
----       )))    
----    end    
----    
---- and numProStageId>@numProStageId       
---- order by numProStageId,ProStage1.numProId    
---- if @@rowcount=0 set @numProStageId=0      
----     
----end    
----drop table #ProClosed  
  
  
select * from #tempTableSendApplicationMail            
      
drop table #tempTableSendApplicationMail
