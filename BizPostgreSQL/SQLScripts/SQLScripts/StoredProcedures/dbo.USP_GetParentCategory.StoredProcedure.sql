/****** Object:  StoredProcedure [dbo].[USP_GetParentCategory]    Script Date: 09/25/2009 16:25:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created by Siva      
-- [USP_GetParentCategory] 72,''

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getparentcategory')
DROP PROCEDURE usp_getparentcategory
GO
CREATE PROCEDURE [dbo].[USP_GetParentCategory]
    @numDomainId AS NUMERIC(9),
    @vcInitialCode VARCHAR(50) = ''
AS 
    BEGIN    

--if @vcInitialCode<>'010101'
--	  begin
        SELECT  
                ISNULL(C.[vcAccountName],'') + ' (' + ATD.[vcAccountType] + ')' AS vcAccountName1,
                ATD.[vcAccountCode] AS vcAccountTypeCode,
                ATD.[numAccountTypeID],
				ATD.[vcAccountType],
				ATD.[numParentID],
				ATD.[numDomainID],
				ATD.[dtCreateDate],
				ATD.[dtModifiedDate],
				C.[numAccountId],
				C.[numParntAcntTypeId],
				C.[numOriginalOpeningBal],
				C.[numOpeningBal],
				C.[dtOpeningDate],
				C.[bitActive],
				C.[bitFixed],
				C.[numDomainId],
				C.[numListItemID],
				C.[bitDepreciation],
				C.[bitOpeningBalanceEquity],
				C.[monEndingOpeningBal],
				C.[monEndingBal],
				C.[dtEndStatementDate],
				C.[chBizDocItems],
				C.[numAcntTypeId],
				C.[vcAccountCode],
				ISNULL(C.[vcAccountName],'') vcAccountName,
				C.[vcAccountDescription],
				C.[bitProfitLoss],ISNULL(C.vcStartingCheckNumber,'') AS vcStartingCheckNumber
        FROM    [Chart_Of_Accounts] C
                INNER JOIN [AccountTypeDetail] ATD 
                ON C.[numParntAcntTypeId] = ATD.[numAccountTypeID]
        WHERE   C.[numDomainId] = @numDomainId AND ISNULL(ATD.bitActive,0) = 1
  AND (C.[vcAccountCode] LIKE @vcInitialCode + '%' OR LEN(@vcInitialCode)=0) AND C.bitActive = 1
  ORDER BY ATD.vcAccountCode, C.[vcAccountCode]
--     Declare @RootAccountId as numeric(9)  
--    Set @RootAccountId=(Select numAccountId From Chart_Of_Accounts Where numParntAcntId is null and numAcntType is null and numDomainId = @numDomainId) --and numAccountId = 1   
--      
--         
-- Select * From Chart_Of_Accounts COA inner join ListDetails LD on COA.numAcntType= LD.numListItemID Where COA.numParntAcntId =@RootAccountId and COA.numDomainId=@numDomainId and COA.numAccountId <>@RootAccountId    
--         
--
--	end
-- else
--	begin
--		SELECT  
--                ISNULL(C.[vcAccountName],'') + ' (' + ATD.[vcAccountType] + ')' AS vcAccountName1,
--                ATD.[vcAccountCode] AS vcAccountTypeCode,
--                ATD.[numAccountTypeID],
--				ATD.[vcAccountCode],
--				ATD.[vcAccountType],
--				ATD.[numParentID],
--				ATD.[numDomainID],
--				ATD.[dtCreateDate],
--				ATD.[dtModifiedDate],
--				C.[numAccountId],
--				C.[numParntAcntTypeId],
--				C.[numOriginalOpeningBal],
--				C.[numOpeningBal],
--				C.[dtOpeningDate],
--				C.[bitActive],
--				C.[bitFixed],
--				C.[numDomainId],
--				C.[numListItemID],
--				C.[bitDepreciation],
--				C.[bitOpeningBalanceEquity],
--				C.[monEndingOpeningBal],
--				C.[monEndingBal],
--				C.[dtEndStatementDate],
--				C.[chBizDocItems],
--				C.[numAcntTypeId],
--				C.[vcAccountCode],
--				ISNULL(C.[vcAccountName],'') vcAccountName,
--				C.[vcAccountDescription],
--				C.[bitProfitLoss],ISNULL(C.vcStartingCheckNumber,'') AS vcStartingCheckNumber
--        FROM    [Chart_Of_Accounts] C
--                INNER JOIN [AccountTypeDetail] ATD 
--                ON C.[numParntAcntTypeId] = ATD.[numAccountTypeID]
--        WHERE   C.[numDomainId] = @numDomainId AND C.bitActive = 1
--  AND (C.[vcAccountCode] LIKE @vcInitialCode + '%' OR  C.[vcAccountCode] LIKE '010201%' OR  LEN(@vcInitialCode)=0)
--  ORDER BY C.[vcAccountCode]
--	end
END
