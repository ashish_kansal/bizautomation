/****** Object:  StoredProcedure [dbo].[USP_GetCaseList]    Script Date: 07/26/2008 16:16:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--  By Anoop Jayaraj                                            
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getcaselist')
DROP PROCEDURE usp_getcaselist
GO
CREATE PROCEDURE [dbo].[USP_GetCaseList]                                               
 @numUserCntId numeric(9)=0,                            
 @tintSortOrder numeric=0,                                                                
 @numDomainID numeric(9)=0,                                            
 @tintUserRightType tinyint,                                            
 @SortChar char(1)='0' ,                                            
 @CustName varchar(100)='',                                            
 @FirstName varChar(100)= '',                                            
 @LastName varchar(100)='',                                            
 @CurrentPage int,                                            
 @PageSize int,                                            
 @TotRecs int output,                                            
 @columnName as Varchar(50),                                            
 @columnSortOrder as Varchar(10),                                          
 @numDivisionID as numeric(9)=0,              
 @bitPartner as bit=0,
 @numStatus AS NUMERIC(9)                                       
                                            
AS                                             
                                            
Create table #tempTable ( ID INT IDENTITY PRIMARY KEY,                                          
      numCaseID VARCHAR(15)                                           
 )                                            
                                             
declare @strSql as varchar(5000)                                            
set @strSql='SELECT  '                               
if @tintSortOrder=3 or @tintSortOrder=4  set @strSql=@strSql + ' top 20 '                                             
   set @strSql=@strSql+ '                                             
   Cs.numCaseID                                                             
 FROM                                               
   AdditionalContactsInformation ADC                                             
 JOIN Cases  Cs                                              
   ON ADC.numContactId =Cs.numContactId                                             
 JOIN DivisionMaster Div                                               
  ON ADC.numDivisionId = Div.numDivisionID'              
   if @bitPartner=1 set @strSql=@strSql+' left join CaseContacts CCont on CCont.numCaseID=Cs.numCaseID and CCont.bitPartner=1 and CCont.numContactId='+convert(varchar(15),@numUserCntID)+ ''              
                                               
   set @strSql=@strSql+' JOIN CompanyInfo CMP                                             
 ON Div.numCompanyID = CMP.numCompanyId                                             
 left join listdetails lst                                            
 on lst.numListItemID= cs.numPriority       
 left join listdetails lst1                                            
 on lst1.numListItemID= cs.numStatus                                                                
 left join AdditionalContactsInformation ADC1                                            
 on Cs.numAssignedTo=ADC1.numContactId                                            
 Where                                            
 cs.numDomainID='+ convert(varchar(15),@numDomainID )+''                      
 
IF @numStatus >0 
 set @strSql=@strSql+ 'and Cs.numStatus = '+ CONVERT(VARCHAR(15),@numStatus) 
ELSE
 set @strSql=@strSql+ 'and Cs.numStatus<>136 '
   
 
 if @FirstName<>'' set @strSql=@strSql+'and ADC.vcFirstName  like '''+@FirstName+'%'''                                                 
 if @LastName<>'' set @strSql=@strSql+'and ADC.vcLastName like '''+@LastName+'%'''                      
 if @CustName<>'' set @strSql=@strSql+' and CMP.vcCompanyName like '''+@CustName+'%'''                                            
                                             
                                           
if @numDivisionID <>0 set @strSql=@strSql + ' And div.numDivisionID =' + convert(varchar(15),@numDivisionID)                                      
if @SortChar<>'0' set @strSql=@strSql + ' And (ADC.vcFirstName like '''+@SortChar+'%'''                                              
if @SortChar<>'0' set @strSql=@strSql + ' or ADC.vcLastName like '''+@SortChar+'%'')'                                             
                                             
if @tintUserRightType=1 set @strSql=@strSql + ' AND (Cs.numRecOwner = '+convert(varchar(15),@numUserCntId)+ ' or Cs.numAssignedTo='+convert(varchar(15),@numUserCntID) +        
+ case when @bitPartner=1 then ' or (CCont.bitPartner=1 and CCont.numContactId='+convert(varchar(15),@numUserCntID)+ ')' else '' end  +')'  
else if @tintUserRightType=2 set @strSql=@strSql + ' AND (Div.numTerID in (select numTerritoryID from  UserTerritory where numUserCntID= '+convert(varchar(15),@numUserCntId)+' ) or Div.numTerID=0 or Cs.numAssignedTo='+convert(varchar(15),@numUserCntID)+'
) '   
  
if @numDivisionID=0                                    
begin                                    
 if @tintSortOrder=0  set @strSql=@strSql + ' AND (Cs.numRecOwner = '+convert(varchar(15),@numUserCntId)+' or Cs.numAssignedTo = '+convert(varchar(15),@numUserCntId)+')'                                                  
end                                            
if @tintSortOrder=2  set @strSql=@strSql + ' AND cs.bintCreatedDate> '''+convert(varchar(20),dateadd(day,-7,getutcdate()))+''''                 
else if @tintSortOrder=3  set @strSql=@strSql + ' and cs.numCreatedBy ='+ convert(varchar(15),@numUserCntId)                              
else if @tintSortOrder=4  set @strSql=@strSql + ' and cs.numModifiedBy ='+ convert(varchar(15),@numUserCntId)                                            
                                       
if @tintSortOrder=0 set @strSql=@strSql + ' ORDER BY ' + @columnName +' '+ @columnSortOrder                                           
else if @tintSortOrder=1  set @strSql=@strSql + ' ORDER BY ' + @columnName +' '+ @columnSortOrder                                            
else if @tintSortOrder=2  set @strSql=@strSql + ' ORDER BY ' + @columnName +' '+ @columnSortOrder                                            
else if @tintSortOrder=3  set @strSql=@strSql + ' ORDER BY cs.bintCreatedDate'                                           
else if @tintSortOrder=4  set @strSql=@strSql + ' ORDER BY cs.bintModifiedDate'                              

PRINT @strSql;                                       
insert into #tempTable (numCaseID)                                            
exec( @strSql)                     
                    
                    
                                          
  declare @firstRec as integer                                            
  declare @lastRec as integer                                            
 set @firstRec= (@CurrentPage-1) * @PageSize                                            
     set @lastRec= (@CurrentPage*@PageSize+1)                     
                    
                    
SELECT   ADC.vcFirstName +' '+ADC.vcLastName as Name,                                            
   CMP.vcCompanyName,                                              
   Cs.numCaseID,                                              
   Cs.vcCaseNumber,                                              
   Cs.intTargetResolveDate,                                              
   Cs.numCreatedBy,                                              
   Cs.textSubject,                                              
   lst.vcdata as Priority,                                                  
   CMP.numCompanyID,                                              
   Div.numDivisionID,                                              
   Div.numTerId,                                              
   Div.tintCRMType,                                              
   ADC.numContactID as ActCntID,                                            
   ADC1.vcFirstName +' '+ADC1.vcLastName +'/ '+dbo.fn_GetContactName(Cs.numAssignedBy)  as AssignedToBy,                       
   dbo.fn_GetListItemName(Cs.numStatus) as Status,     
   Cs.numRecOwner                                          
 FROM                                               
   AdditionalContactsInformation ADC                                             
 JOIN Cases  Cs                                              
   ON ADC.numContactId =Cs.numContactId                                             
 JOIN DivisionMaster Div                                               
  ON ADC.numDivisionId = Div.numDivisionID                                             
  JOIN CompanyInfo CMP                                             
 ON Div.numCompanyID = CMP.numCompanyId                                             
 left join listdetails lst                                            
 on lst.numListItemID= cs.numPriority                                  
 left join AdditionalContactsInformation ADC1                                            
 on Cs.numAssignedTo=ADC1.numContactId                     
 join #tempTable T on T.numCaseID=Cs.numCaseID                                            
 where ID > @firstRec and ID < @lastRec               
                    
                                         
set @TotRecs=(select count(*) from #tempTable)                                            
drop table #tempTable
GO
