/****** Object:  StoredProcedure [dbo].[usp_DeleteSelectedSearchRecords]    Script Date: 07/26/2008 16:15:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created By: Debasish Tapan Nag                                
--Purpose: Deletes the selected Records of Advance Search              
--Created Date: 08/22/2005                           
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_deleteselectedsearchrecords')
DROP PROCEDURE usp_deleteselectedsearchrecords
GO
CREATE PROCEDURE [dbo].[usp_DeleteSelectedSearchRecords]              
  @vcEntityIdList NVarchar(4000)              
AS                                              
BEGIN                               
 SET @vcEntityIdList = @vcEntityIdList + ','              
 DECLARE @intFirstComma integer                
 DECLARE @intFirstUnderScore integer                
              
 DECLARE @numCompanyId NUMERIC(9)              
 DECLARE @numContactId NUMERIC(9)              
 DECLARE @numDivisionId NUMERIC(9)              
              
 DECLARE @vcCompContDivValue NVarchar(30)              
             
 DECLARE @intDivCount numeric                     
 DECLARE @intContCount numeric         
          
 DECLARE @DeletionFlag Int          
 SET @DeletionFlag = 1        
           
 SET @intFirstComma = CHARINDEX(',', @vcEntityIdList, 0)                
              
 WHILE @intFirstComma > 0              
 BEGIN              
 SET @vcCompContDivValue = SUBSTRING(@vcEntityIdList,0,@intFirstComma)              
 SET @intFirstUnderScore = CHARINDEX('_', @vcCompContDivValue, 0)              
 SET @numCompanyId = SUBSTRING(@vcCompContDivValue, 0, @intFirstUnderScore)              
 SET @vcCompContDivValue = SUBSTRING(@vcCompContDivValue, @intFirstUnderScore+1, Len(@vcCompContDivValue))              
 SET @intFirstUnderScore = CHARINDEX('_', @vcCompContDivValue, 0)              
 SET @numContactId = SUBSTRING(@vcCompContDivValue, 0, @intFirstUnderScore)              
 SET @numDivisionId = SUBSTRING(@vcCompContDivValue, @intFirstUnderScore+1, Len(@vcCompContDivValue))              
 PRINT  @numCompanyId              
 PRINT  @numContactId              
 PRINT  @numDivisionId          
     /*----------------DELETION STARTS HERE----------------------*/        
 DELETE FROM AOIContactLink WHERE numContactId=@numContactID            
 PRINT 'AOIContactLink Deleted'        
 DELETE FROM Communication WHERE numContactId=@numContactID and numDivisionID=@numDivisionID            
 PRINT 'Communication Deleted'       
    
  --Deleting ExtraNetAccounts        
 DECLARE @numExtranetID as Numeric        
 SELECT @numExtranetID = numExtranetID FROM ExtranetAccountsDtl WHERE numContactId = @numContactID           
 DELETE FROM ExtranetAccountsDtl WHERE numContactId=@numContactID            
 PRINT 'ExtranetAccountsDtl Deleted'        
 DECLARE @numExtranetIDCount Numeric        
 SELECT @numExtranetIDCount = COUNT(numExtranetID) FROM ExtranetAccountsDtl WHERE numExtranetID IN (SELECT numExtranetID FROM ExtarnetAccounts WHERE numDivisionID=@numDivisionID AND numCompanyId=@numCompanyID)        
        IF(@numExtranetIDCount = 0)        
 BEGIN        
  DELETE FROM ExtarnetAccounts WHERE  numDivisionID=@numDivisionID AND  numCompanyId=@numCompanyId         
  PRINT 'ExtarnetAccounts Deleted'        
 END        
        
 --Deleting Campaigns        
 DECLARE @numCampaignId Numeric        
 SELECT @numCampaignId = numCampaignId FROM CampaignDetails WHERE numContactId=@numContactID AND  numCompanyId=@numCompanyID        
 DELETE FROM CampaignDetails WHERE numContactId=@numContactID AND  numCompanyId=@numCompanyID          
 PRINT 'CampaignDetails Deleted'        
 /*
 DECLARE @CampaignDivisionCount as Numeric        
 SELECT @CampaignDivisionCount = COUNT(numContactId) FROM CampaignDetails WHERE numContactId IN (SELECT DISTINCT numContactId FROM DivisionMaster WHERE numDivisionId = @numDivisionID)        
 IF(@CampaignDivisionCount=0)        
 BEGIN        
  DELETE FROM CampaignDivision WHERE numDivisionID=@numDivisionID AND numCampaignID = @numCampaignId        
  PRINT 'CampaignDivision Deleted'        
 END        
 */     
 --Deleting Cases        
 DECLARE @numCaseId as Numeric        

 SELECT @numCaseId = numCaseId FROM Cases  WHERE numDivisionID=@numDivisionID AND  numContactId=@numContactID        
 DELETE FROM CaseSolutions WHERE numCaseId = @numCaseId        
 PRINT 'CaseSolutions Deleted'        
 DELETE FROM Cases WHERE numDivisionID=@numDivisionID AND  numContactId=@numContactID AND numCaseId = @numCaseId        
 PRINT 'Cases Deleted'        
        
        
 DELETE FROM FollowUpHistory WHERE numDivisionID=@numDivisionID                
 PRINT 'FollowUpHistory Deleted'        
 DELETE FROM Leads WHERE  numCompanyId=@numCompanyID        
 PRINT 'Leads Deleted'        
 DELETE FROM UniversalSupportKeyMaster WHERE numDivisionID=@numDivisionID        
 PRINT 'UniversalSupportKeyMaster Deleted'        
        
        
 DECLARE @numProId as Numeric        
 SELECT @numProId = numProId FROM ProjectsContacts WHERE numContactId = @numContactID           
        
 DECLARE @numOppId as Numeric        
 SELECT @numOppId = numOppId FROM OpportunityMaster  WHERE numDivisionID=@numDivisionID AND  numContactId=@numContactId         
 DECLARE @numProjectForOppId as Numeric        
 SELECT @numProjectForOppId = numProId FROM ProjectsMaster  WHERE numOppId=@numOppId         
        
 DELETE FROM ProjectsContacts WHERE numProId = @numProId and numContactId = @numContactID          
 DELETE FROM ProjectsContacts WHERE numProId = @numProjectForOppId        
 PRINT 'ProjectsContacts Deleted'        
 DELETE FROM ProjectsSubStageDetails WHERE numProId = @numProId           
 DELETE FROM ProjectsSubStageDetails WHERE numProId = @numProjectForOppId           
 PRINT 'ProjectsSubStageDetails Deleted'        
 DELETE FROM ProjectsStageDetails WHERE numProId = @numProId           
 DELETE FROM ProjectsStageDetails WHERE numProId = @numProjectForOppId           
 PRINT 'ProjectsStageDetails Deleted'        
 DELETE FROM ProjectsDependency WHERE numProId = @numProId           
 DELETE FROM ProjectsDependency WHERE numProId = @numProjectForOppId           
 PRINT 'ProjectsDependency Deleted'        
 DELETE FROM ProjectsTime WHERE numProId = @numProId           
 DELETE FROM ProjectsTime WHERE numProId = @numProjectForOppId           
 PRINT 'ProjectsTime Deleted'        
 DELETE FROM ProjectsExpense WHERE numProId = @numProId           
 DELETE FROM ProjectsExpense WHERE numProId = @numProjectForOppId           
 PRINT 'ProjectsExpense Deleted'        
 DELETE FROM ProjectsMaster WHERE numProId = @numProId            
 DELETE FROM ProjectsMaster WHERE numProId = @numProjectForOppId            
 PRINT 'ProjectsMaster Deleted'        
        
          
 DELETE FROM OpportunitySubStageDetails WHERE numOppId = @numOppId        
 PRINT 'OpportunitySubStageDetails Deleted'        
 DELETE FROM OpportunityStageDetails WHERE numOppId = @numOppId        
 PRINT 'OpportunityStageDetails Deleted'        
 DELETE FROM OpportunityDependency WHERE numOpportunityId = @numOppId        
 PRINT 'OpportunityDependency Deleted'        
 DELETE FROM OpportunityExpense WHERE numOppId = @numOppId        
 PRINT 'OpportunityExpense Deleted'        
 DELETE FROM OpportunityItems WHERE numOppId = @numOppId        
 PRINT 'OpportunityTime Deleted'      
 DELETE FROM TimeAndExpense WHERE numOppId = @numOppId        
 PRINT 'Opportunity Time And Expense Deleted'   
        
 DECLARE @numOppBizDocId as Numeric        
 SELECT @numOppBizDocId = numBizDocId FROM OpportunityBizDocs WHERE numOppId = @numOppId        
 DELETE FROM OpportunityTime WHERE numOppId = @numOppId        
 PRINT 'OpportunityTime Deleted'        
 DELETE FROM OpportunityBizDocDtl WHERE numBizDocId = @numOppBizDocId        
 PRINT 'OpportunityBizDocDtl Deleted'        
 DELETE FROM OpportunityBizDocs WHERE numOppId = @numOppId        
 PRINT 'OpportunityBizDocs Deleted'        
        
 DELETE FROM OpportunityContact WHERE numOppId = @numOppId --Delete all opportunities for the contact (whatever the associated contacts in OpportunityContact is)    
 PRINT 'Opportunity Contact Deleted'        
 DELETE FROM ProjectsMaster WHERE numOppId = @numOppId            
 PRINT 'ProjectsMaster Deleted'     
 DELETE FROM OpportunityMaster WHERE numOppId = @numOppId AND numContactId=@numContactId         
  
 PRINT 'SurveyRespondentsMaster Deleted'     
 UPDATE SurveyRespondentsMaster SET numRegisteredRespondentContactId = NULL WHERE numRegisteredRespondentContactId = @numContactId      
  
        
 DELETE FROM AdditionalContactsInformation WHERE numContactId=@numContactID                
 PRINT 'Contact Deleted'        
 SELECT @intContCount=COUNT(*) FROM AdditionalContactsInformation WHERE numDivisionID=@numDivisionID                
      IF @intContCount=0 --If There are no more Contacts for the selected division, then delete division.                
      BEGIN                
          DELETE FROM ExtarnetAccounts WHERE  numDivisionID=@numDivisionID        
          DELETE FROM CompanyAssociations WHERE numDivisionID=@numDivisionID OR numAssociateFromDivisionID =@numDivisionID        
          PRINT 'CompanyAssociations Deleted'        
          DELETE FROM DivisionMaster WHERE numDivisionID=@numDivisionID                
          PRINT 'Division Deleted'        
          SELECT @intDivCount=COUNT(*) FROM  DivisionMaster WHERE numCompanyId=@numCompanyID                      
        
          IF @intDivCount=0      -- If there are no more Divisions for the selected Company then Delete Company.                
          BEGIN                
              DELETE FROM CompanyInfo WHERE numCompanyId=@numCompanyID                   
              PRINT 'Company Deleted'        
          END        
      END           
 IF @@ERROR <> 0        
  SET @DeletionFlag = 0        
     /*----------------DELETION ENDS HERE----------------------*/        
              
 SET @vcEntityIdList = SUBSTRING(@vcEntityIdList,@intFirstComma+1,Len(@vcEntityIdList))                
 SET @intFirstComma = CHARINDEX(',', @vcEntityIdList, 0)                
 END              
            
            
            
 SELECT @DeletionFlag            
 RETURN              
END
GO
