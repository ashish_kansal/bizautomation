/****** Object:  StoredProcedure [dbo].[GetAutoRoutRulesData]    Script Date: 07/26/2008 16:14:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj      
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='getautoroutrulesdata')
DROP PROCEDURE getautoroutrulesdata
GO
CREATE PROCEDURE [dbo].[GetAutoRoutRulesData]      
@numRoutID as numeric(9)=0      
as      
      
declare @tintEqualTo as tinyint      
select @tintEqualTo=tintEqualTo from RoutingLeads where numRoutID=@numRoutID      
      
      
select * from RoutingLeads where numRoutID=@numRoutID      
      
select numContactId,vcFirstName+' '+vcLastname as [Name] from RoutingLeadDetails      
join AdditionalContactsInformation      
on  numContactId=numEmpID      
where numRoutID=@numRoutID order by intAssignOrder      
      
if @tintEqualTo=1      
begin    
 select numStateID,vcState from  State        
 where numStateID in (select vcValue from  RoutinLeadsValues where numRoutID=@numRoutID and isnumeric(vcValue)=1)  
end    
else if @tintEqualTo=2      
begin      
 select vcValue from RoutinLeadsValues where numRoutID=@numRoutID      
end      
else if @tintEqualTo=3      
begin      
      
 select vcValue,vcData as vcAOIName from RoutinLeadsValues      
 join Listdetails       
 on numListItemID=vcValue        
 where numRoutID=@numRoutID      
end
GO
