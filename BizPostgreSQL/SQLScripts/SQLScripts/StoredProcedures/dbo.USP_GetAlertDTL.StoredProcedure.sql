/****** Object:  StoredProcedure [dbo].[USP_GetAlertDTL]    Script Date: 07/26/2008 16:16:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created By Anoop Jayaraj  


GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getalertdtl')
DROP PROCEDURE usp_getalertdtl
GO
CREATE PROCEDURE [dbo].[USP_GetAlertDTL]  
@numAlertDTLID as numeric(9)=0,
@numDomainId as numeric(9)=0 
as 

select * from AlertDTL HDR
join AlertDomainDtl DTL
on HDR.numAlertDTLid=DTL.numAlertDTLid
where HDR.numAlertDTLID=@numAlertDTLID and numDomainID=@numDomainId
GO
