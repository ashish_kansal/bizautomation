/****** Object:  StoredProcedure [dbo].[usp_GetCustRptOrgContactCustomFields]    Script Date: 07/26/2008 16:17:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created By: Debasish Tapan Nag                
--Purpose: Get a list of custom fields for the Organization and Contact Report as applicable      
--Created Date: 10/28/2005                   
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getcustrptorgcontactcustomfields')
DROP PROCEDURE usp_getcustrptorgcontactcustomfields
GO
CREATE PROCEDURE [dbo].[usp_GetCustRptOrgContactCustomFields]  
 @numContactTypeId numeric,  
 @numRelationTypeId numeric ,  
 @numDomainId numeric 
AS          
BEGIN  
 IF @numContactTypeId <> 0 AND  @numRelationTypeId <> 0  
 BEGIN   
    SELECT 1 AS numFieldGroupID, 'CFld' + Convert(NVarchar, m.fld_id) AS vcDbFieldName, m.fld_label AS vcScrFieldName, d.numOrder  
  AS  numOrderAppearance, 0 AS boolSummationPossible, 0 AS boolDefaultColumnSelection    
  FROM CFW_Fld_Master m INNER JOIN CFW_Fld_Dtl d ON m.Fld_id = d.numFieldId
         WHERE      
         m.Grp_id = 1 --Prospect/ Accounts  
         AND m.numDomainID = @numDomainId
  AND d.numRelation = @numRelationTypeId  
    UNION     
    SELECT 2 AS numFieldGroupID, 'CFld' + Convert(NVarchar, m.fld_id) AS vcDbFieldName, m.fld_label AS vcScrFieldName, d.numOrder  
  AS  numOrderAppearance, 0 AS boolSummationPossible, 0 AS boolDefaultColumnSelection    
  FROM CFW_Fld_Master m INNER JOIN CFW_Fld_Dtl d ON m.Fld_id = d.numFieldId
         WHERE m.Grp_id = 4 --Contact Details 
         AND m.numDomainID = @numDomainId 
  AND d.numRelation = @numContactTypeId  
  ORDER BY d.numOrder    
 END  
 ELSE IF @numContactTypeId = 0 AND  @numRelationTypeId = 0  
 BEGIN   
    SELECT 1 AS numFieldGroupID, 'CFld' + Convert(NVarchar, m.fld_id) AS vcDbFieldName, m.fld_label AS vcScrFieldName, d.numOrder  
  AS  numOrderAppearance, 0 AS boolSummationPossible, 0 AS boolDefaultColumnSelection    
  FROM CFW_Fld_Master m INNER JOIN CFW_Fld_Dtl d ON m.Fld_id = d.numFieldId
         WHERE m.Grp_id = 1 --Prospect/ Accounts
         AND m.numDomainID = @numDomainId  
    UNION     
    SELECT 2 AS numFieldGroupID, 'CFld' + Convert(NVarchar, m.fld_id) AS vcDbFieldName, m.fld_label AS vcScrFieldName, d.numOrder  
  AS  numOrderAppearance, 0 AS boolSummationPossible, 0 AS boolDefaultColumnSelection    
  FROM CFW_Fld_Master m INNER JOIN CFW_Fld_Dtl d  ON m.Fld_id = d.numFieldId
         WHERE m.Grp_id = 4 --Contact Details
         AND m.numDomainID = @numDomainId  
  ORDER BY d.numOrder    
 END  
 ELSE IF  @numContactTypeId <> 0  
    SELECT 2 AS numFieldGroupID, 'CFld' + Convert(NVarchar, m.fld_id) AS vcDbFieldName, m.fld_label AS vcScrFieldName, d.numOrder  
  AS  numOrderAppearance, 0 AS boolSummationPossible, 0 AS boolDefaultColumnSelection    
  FROM CFW_Fld_Master m INNER JOIN CFW_Fld_Dtl d ON m.Fld_id = d.numFieldId
         WHERE      
         m.Grp_id = 4 --Contact Details  
         AND m.numDomainID = @numDomainId
  ORDER BY d.numOrder    
 ELSE IF  @numRelationTypeId <> 0  
    SELECT 1 AS numFieldGroupID, 'CFld' + Convert(NVarchar, m.fld_id) AS vcDbFieldName, m.fld_label AS vcScrFieldName, d.numOrder  
  AS  numOrderAppearance, 0 AS boolSummationPossible, 0 AS boolDefaultColumnSelection    
  FROM CFW_Fld_Master m INNER JOIN CFW_Fld_Dtl d ON m.Fld_id = d.numFieldId
         WHERE      
         m.Grp_id = 1 --Prospect/ Accounts  
         AND d.numRelation = @numRelationTypeId  
         AND m.numDomainID = @numDomainId
  ORDER BY d.numOrder    
END
GO
