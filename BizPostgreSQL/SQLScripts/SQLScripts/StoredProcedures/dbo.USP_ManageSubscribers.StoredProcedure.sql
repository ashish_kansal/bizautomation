/****** Object:  StoredProcedure [dbo].[USP_ManageSubscribers]    Script Date: 07/26/2008 16:20:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj                                    
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'usp_managesubscribers' ) 
    DROP PROCEDURE usp_managesubscribers
GO
CREATE PROCEDURE [dbo].[USP_ManageSubscribers]
    @numSubscriberID AS NUMERIC(9) OUTPUT,
    @numDivisionID AS NUMERIC(9),
    @intNoofUsersSubscribed AS INTEGER,
    @dtSubStartDate AS DATETIME,
    @dtSubEndDate AS DATETIME,
    @bitTrial AS BIT,
    @numAdminContactID AS NUMERIC(9),
    @bitActive AS TINYINT,
    @vcSuspendedReason AS VARCHAR(1000),
    @numTargetDomainID AS NUMERIC(9) OUTPUT,
    @numDomainID AS NUMERIC(9),
    @numUserContactID AS NUMERIC(9),
    @strUsers AS TEXT = '',
    @bitExists AS BIT = 0 OUTPUT,
    @TargetGroupId AS NUMERIC(9) = 0 OUTPUT,
	@bitEnabledAccountingAudit BIT,
	@bitEnabledNotifyAdmin BIT,
	@intNoofLimitedAccessUsers INT = NULL,
	@intNoofBusinessPortalUsers INT = NULL,
	@monFullUserCost DECIMAL(20,5) = NULL,
	@monLimitedAccessUserCost DECIMAL(20,5) = NULL,
	@monBusinessPortalUserCost DECIMAL(20,5) = NULL,
	@monSiteCost DECIMAL(20,5) = NULL,
	@intFullUserConcurrency INT  = 0
AS 
    IF @numSubscriberID = 0 
    BEGIN
		IF NOT EXISTS (SELECT * FROM Subscribers WHERE numDivisionID = @numDivisionID AND numDomainID = @numDomainID) 
        BEGIN
            SET @bitExists = 0                              
            
			DECLARE @vcCompanyName AS VARCHAR(100)                             
            DECLARE @numCompanyID AS NUMERIC(9)                                   
                    
			SELECT 
				@vcCompanyName = vcCompanyName
				,@numCompanyID = C.numCompanyID
            FROM 
				CompanyInfo C
            INNER JOIN 
				DivisionMaster D 
			ON 
				D.numCompanyID = C.numCompanyID
            WHERE 
				D.numDivisionID = @numDivisionID                                    
                                
			DECLARE @numNewCompanyID AS NUMERIC(9)                                   
			DECLARE @numNewDivisionID AS NUMERIC(9)                                    
			DECLARE @numNewContactID AS NUMERIC(9)                                    
                                      
            INSERT INTO Domain
            (
                vcDomainName,
                vcDomainDesc,
                numDivisionID,
                numAdminID,
				tintDecimalPoints,
				tintChrForComSearch,
				tintChrForItemSearch,
				bitIsShowBalance,
				tintComAppliesTo,
				tintCommissionType,
				charUnitSystem,
				bitExpenseNonInventoryItem
            )
			VALUES
			(
                @vcCompanyName,
                @vcCompanyName,
                @numDivisionID,
                @numAdminContactID,
                2,
				1,
				1,
				0,
				3,
				1,
				'E',
				1
            )
			                            
            SET @numTargetDomainID = SCOPE_IDENTITY()                                       
                                       
            INSERT  INTO CompanyInfo
            (
                vcCompanyName,
                numCompanyType,
                numCompanyRating,
                numCompanyIndustry,
                numCompanyCredit,
                vcWebSite,
                vcWebLabel1,
                vcWebLink1,
                vcWebLabel2,
                vcWebLink2,
                vcWebLabel3,
                vcWebLink3,
                vcWeblabel4,
                vcWebLink4,
                numAnnualRevID,
                numNoOfEmployeesId,
                vcHow,
                vcProfile,
                bitPublicFlag,
                numDomainID
            )
			SELECT 
				vcCompanyName,
                93,--i.e employer --numCompanyType,
                numCompanyRating,
                numCompanyIndustry,
                numCompanyCredit,
                vcWebSite,
                vcWebLabel1,
                vcWebLink1,
                vcWebLabel2,
                vcWebLink2,
                vcWebLabel3,
                vcWebLink3,
                vcWeblabel4,
                vcWebLink4,
                numAnnualRevID,
                numNoOfEmployeesId,
                vcHow,
                vcProfile,
                bitPublicFlag,
                numDomainID
			FROM 
				CompanyInfo
			WHERE 
				numCompanyID = @numCompanyID
				                 
            SET @numNewCompanyID = SCOPE_IDENTITY()                            
                                     
            INSERT INTO DivisionMaster
            (
                numCompanyID,
                vcDivisionName,
                numGrpId,
                bitPublicFlag,
                tintCRMType,
                numStatusID,
                tintBillingTerms,
                numBillingDays,
                tintInterestType,
                fltInterest,
                vcComPhone,
                vcComFax,
                numDomainID
            )
            SELECT 
				@numNewCompanyID,
				vcDivisionName,
				numGrpId,
				bitPublicFlag,
				2,
				numStatusID,
				tintBillingTerms,
				numBillingDays,
				tintInterestType,
				fltInterest,
				vcComPhone,
				vcComFax,
				numDomainID
			FROM 
				DivisionMaster
            WHERE 
				numDivisionID = @numDivisionID                     
                    
			SET @numNewDivisionID = SCOPE_IDENTITY()     
                                            
            INSERT INTO dbo.AddressDetails (
				vcAddressName,
				vcStreet,
				vcCity,
				vcPostalCode,
				numState,
				numCountry,
				bitIsPrimary,
				tintAddressOf,
				tintAddressType,
				numRecordID,
				numDomainID
			) 
			SELECT 
				vcAddressName,
				vcStreet,
				vcCity,
				vcPostalCode,
				numState,
				numCountry,
				bitIsPrimary,
				tintAddressOf,
				tintAddressType,
				@numNewDivisionID,
				numDomainID 
			FROM 
				dbo.AddressDetails 
			WHERE 
				numRecordID= @numDivisionID 
				AND tintAddressOf=2
                                
                                    
                    INSERT  INTO AdditionalContactsInformation
                            (
                              vcDepartment,
                              vcCategory,
                              vcGivenName,
                              vcFirstName,
                              vcLastName,
                              numDivisionId,
                              numContactType,
                              numTeam,
                              numPhone,
                              numPhoneExtension,
                              numCell,
                              numHomePhone,
                              vcFax,
                              vcEmail,
                              VcAsstFirstName,
                              vcAsstLastName,
                              numAsstPhone,
                              numAsstExtn,
                              vcAsstEmail,
                              charSex,
                              bintDOB,
                              vcPosition,
                              numEmpStatus,
                              vcTitle,
                              vcAltEmail,
                              numDomainID,
                              bitPrimaryContact
                            )
                            SELECT  vcDepartment,
                                    vcCategory,
                                    vcGivenName,
                                    vcFirstName,
                                    vcLastName,
                                    @numNewDivisionID,
                                    numContactType,
                                    numTeam,
                                    numPhone,
                                    numPhoneExtension,
                                    numCell,
                                    numHomePhone,
                                    vcFax,
                                    vcEmail,
                                    VcAsstFirstName,
                                    vcAsstLastName,
                                    numAsstPhone,
                                    numAsstExtn,
                                    vcAsstEmail,
                                    charSex,
                                    bintDOB,
                                    vcPosition,
                                    numEmpStatus,
                                    vcTitle,
                                    vcAltEmail,
                                    numDomainID,
                                    1
                            FROM    AdditionalContactsInformation
                            WHERE   numContactID = @numAdminContactID                                       
                    SET @numNewContactID = SCOPE_IDENTITY()                             
                             
                    UPDATE  CompanyInfo
                    SET     numCreatedBy = @numNewContactID,
                            bintCreatedDate = GETUTCDATE(),
                            numModifiedBy = @numNewContactID,
                            bintModifiedDate = GETUTCDATE(),
                            numDomainID = @numTargetDomainID
                    WHERE   numCompanyID = @numNewCompanyID                            
                             
                    UPDATE  DivisionMaster
                    SET     numCompanyID = @numNewCompanyID,
                            numCreatedBy = @numNewContactID,
                            bintCreatedDate = GETUTCDATE(),
                            numModifiedBy = @numNewContactID,
                            bintModifiedDate = GETUTCDATE(),
                            numDomainID = @numTargetDomainID,
                            tintCRMType = 2,
                            numRecOwner = @numNewContactID
                    WHERE   numDivisionID = @numNewDivisionID                            
                            
                            
                    UPDATE  AdditionalContactsInformation
                    SET     numDivisionId = @numNewDivisionID,
                            numCreatedBy = @numNewContactID,
                            bintCreatedDate = GETUTCDATE(),
                            numModifiedBy = @numNewContactID,
                            bintModifiedDate = GETUTCDATE(),
                            numDomainID = @numTargetDomainID,
                            numRecOwner = @numNewContactID
                    WHERE   numContactId = @numNewContactID                            
                                
                    DECLARE @vcFirstname AS VARCHAR(50)                            
                    DECLARE @vcEmail AS VARCHAR(100)                            
                    SELECT  @vcFirstname = vcFirstName,
                            @vcEmail = vcEmail
                    FROM    AdditionalContactsInformation
                    WHERE   numContactID = @numNewContactID                            
                    EXEC Resource_Add @vcFirstname, '', @vcEmail, 1,
                        @numTargetDomainID, @numNewContactID   
                        
     -- Added by sandeep to add required fields for New Item form field management in Admnistration section
     
     EXEC USP_ManageNewItemRequiredFields @numDomainID = @numTargetDomainID
     
     --Add Default Subtabs and assign permission to all roles by default --added by chintan

                    EXEC USP_ManageTabsInCuSFields @byteMode = 6, @LocID = 0,
                        @TabName = '', @TabID = 0,
                        @numDomainID = @numTargetDomainID, @vcURL = ''
                        
                    INSERT  INTO RoutingLeads
                            (
                              numDomainId,
                              vcRoutName,
                              tintEqualTo,
                              numValue,
                              numCreatedBy,
                              dtCreatedDate,
                              numModifiedBy,
                              dtModifiedDate,
                              bitDefault,
                              tintPriority
                            )
                    VALUES  (
                              @numTargetDomainID,
                              'Default',
                              0,
                              @numNewContactID,
                              @numNewContactID,
                              GETUTCDATE(),
                              @numNewContactID,
                              GETUTCDATE(),
                              1,
                              0
                            )                          
                    INSERT  INTO RoutingLeadDetails
                            (
                              numRoutID,
                              numEmpId,
                              intAssignOrder
                            )
                    VALUES  (
                              SCOPE_IDENTITY(),
                              @numNewContactID,
                              1
                            )                  
                      
                    IF NOT EXISTS (SELECT numGroupID FROM AuthenticationGroupMaster WHERE numDomainId = @numTargetDomainID AND vcGroupName = 'System Administrator') 
                    BEGIN
                        DECLARE @numGroupId1 AS NUMERIC(9)                      
                        INSERT INTO AuthenticationGroupMaster
                        (
                            vcGroupName,
                            numDomainID,
                            tintGroupType,
                            bitConsFlag 
                        )
                        VALUES  
						(
                            'System Administrator',
                            @numTargetDomainID,
                            1,
                            1
                        )                      
                        
						SET @numGroupId1 = SCOPE_IDENTITY()                        
                        SET @TargetGroupId = @numGroupId1                     
                       
						INSERT INTO GroupAuthorization
						(
							numGroupID
							,numModuleID
							,numPageID
							,intExportAllowed
							,intPrintAllowed
							,intViewAllowed
							,intAddAllowed
							,intUpdateAllowed
							,intDeleteAllowed
							,numDomainID
						)
						SELECT 
							@numGroupId1
							,numModuleID
							,numPageID
							,bitISExportApplicable
							,0
							,bitIsViewApplicable
							,bitIsAddApplicable
							,bitISUpdateApplicable
							,bitISDeleteApplicable
							,@numTargetDomainID
						FROM 
							PageMaster

						INSERT INTO GroupTabDetails (numGroupId,numTabId,numRelationShip,bitallowed,numOrder,tintType) VALUES (@numGroupId1,1,0,1,1,0)
						INSERT INTO GroupTabDetails (numGroupId,numTabId,numRelationShip,bitallowed,numOrder,tintType) VALUES (@numGroupId1,36,0,1,2,0)
						INSERT INTO GroupTabDetails (numGroupId,numTabId,numRelationShip,bitallowed,numOrder,tintType) VALUES (@numGroupId1,7,0,1,3,0)
						INSERT INTO GroupTabDetails (numGroupId,numTabId,numRelationShip,bitallowed,numOrder,tintType) VALUES (@numGroupId1,80,0,1,4,0)
						INSERT INTO GroupTabDetails (numGroupId,numTabId,numRelationShip,bitallowed,numOrder,tintType) VALUES (@numGroupId1,3,0,1,5,0)
						INSERT INTO GroupTabDetails (numGroupId,numTabId,numRelationShip,bitallowed,numOrder,tintType) VALUES (@numGroupId1,45,0,1,6,0)
						INSERT INTO GroupTabDetails (numGroupId,numTabId,numRelationShip,bitallowed,numOrder,tintType) VALUES (@numGroupId1,4,0,1,7,0)
						INSERT INTO GroupTabDetails (numGroupId,numTabId,numRelationShip,bitallowed,numOrder,tintType) VALUES (@numGroupId1,44,0,1,8,0)
						INSERT INTO GroupTabDetails (numGroupId,numTabId,numRelationShip,bitallowed,numOrder,tintType) VALUES (@numGroupId1,6,0,1,9,0)

						--default Sub tab From cfw_grp_master
						INSERT  INTO GroupTabDetails
                        (
                            numGroupId,
                            numTabId,
                            numRelationShip,
                            bitallowed,
                            numOrder,tintType
                        )
                        SELECT  
							@numGroupId1,
                            x.Grp_id,
                            0,
                            1,
                            x.numOrder
							,1
                        FROM
						(
							SELECT
								 *
								 ,ROW_NUMBER() OVER(ORDER BY Grp_id) AS numOrder
                            FROM 
								cfw_grp_master
                            WHERE 
								tintType=2 
								AND numDomainID = @numTargetDomainID
                        ) x 


						--Added by Anoop for Dashboard +++++++++++++++++++
                        IF NOT EXISTS (SELECT numCustomReportID FROM CustomReport WHERE numDomainId = @numTargetDomainID)
                        BEGIN                  
                            EXEC CreateCustomReportsForNewDomain @numTargetDomainID,@numNewContactID              
                        END 

                        DECLARE @numGroupId2 AS NUMERIC(9)                      
                        INSERT  INTO AuthenticationGroupMaster
						(
							vcGroupName,
							numDomainID,
							tintGroupType,
							bitConsFlag 
						)
						VALUES  
						(
							'Executive',
							@numTargetDomainID,
							1,
							0
						)
						SET @numGroupId2 = SCOPE_IDENTITY() 

						EXEC USP_ManageSubscribers_GorupPermission @numTargetDomainID,@numGroupId2,258

                        INSERT INTO AuthenticationGroupMaster
                        (
                            vcGroupName,
                            numDomainID,
                            tintGroupType,
                            bitConsFlag 
                        )
                        VALUES
						(
                            'Sales Staff',
                            @numTargetDomainID,
                            1,
                            0
                        )                      
                        SET @numGroupId2 = SCOPE_IDENTITY() 

						EXEC USP_ManageSubscribers_GorupPermission @numTargetDomainID,@numGroupId2,259

                        INSERT  INTO AuthenticationGroupMaster
                        (
                            vcGroupName,
                            numDomainID,
                            tintGroupType,
                            bitConsFlag 
                        )
                        VALUES  
						(
                            'Warehouse Staff',
                            @numTargetDomainID,
                            1,
                            0
                        )                      
						SET @numGroupId2 = SCOPE_IDENTITY() 

						EXEC USP_ManageSubscribers_GorupPermission @numTargetDomainID,@numGroupId2,260
						
						INSERT INTO AuthenticationGroupMaster (vcGroupName,numDomainID,tintGroupType,bitConsFlag) VALUES ('Limited Access Users',@numTargetDomainID,4,1)
						SET @numGroupId2 = SCOPE_IDENTITY()

						INSERT INTO GroupTabDetails (numGroupId,numTabId,numRelationShip,bitallowed,numOrder,tintType) SELECT numGroupID,135,0,1,1,0 FROM AuthenticationGroupMaster WHERE tintGroupType = @numGroupId2
						INSERT INTO GroupTabDetails (numGroupId,numTabId,numRelationShip,bitallowed,numOrder,tintType) SELECT numGroupID,5,0,1,1,0 FROM AuthenticationGroupMaster WHERE tintGroupType = @numGroupId2
						INSERT INTO GroupTabDetails (numGroupId,numTabId,numRelationShip,bitallowed,numOrder,tintType) SELECT numGroupID,36,0,1,1,0 FROM AuthenticationGroupMaster WHERE tintGroupType = @numGroupId2
						INSERT INTO GroupTabDetails (numGroupId,numTabId,numRelationShip,bitallowed,numOrder,tintType) SELECT numGroupID,132,0,1,1,0 FROM AuthenticationGroupMaster WHERE tintGroupType = @numGroupId2

						INSERT INTO TreeNavigationAuthorization
						(
							numDomainID,numGroupID,numTabID,numPageNavID,bitVisible,tintType
						)
						VALUES
						(@numTargetDomainID,@numGroupId2,5,279,1,1)
						,(@numTargetDomainID,@numGroupId2,5,280,1,1)
						,(@numTargetDomainID,@numGroupId2,135,274,1,1)
						,(@numTargetDomainID,@numGroupId2,135,275,1,1)
						,(@numTargetDomainID,@numGroupId2,132,84,1,1)
						,(@numTargetDomainID,@numGroupId2,132,271,1,1)
						,(@numTargetDomainID,@numGroupId2,36,250,1,1)

						INSERT INTO PageMasterGroupSetting 
						(
							numPageID,numModuleID,numGroupID
							,bitIsViewApplicable,bitViewPermission1Applicable,bitViewPermission2Applicable,bitViewPermission3Applicable
							,bitIsAddApplicable,bitAddPermission1Applicable,bitAddPermission2Applicable,bitAddPermission3Applicable
							,bitIsUpdateApplicable,bitUpdatePermission1Applicable,bitUpdatePermission2Applicable,bitUpdatePermission3Applicable
							,bitIsDeleteApplicable,bitDeletePermission1Applicable,bitDeletePermission2Applicable,bitDeletePermission3Applicable
							,bitIsExportApplicable,bitExportPermission1Applicable,bitExportPermission2Applicable,bitExportPermission3Applicable
						)
						SELECT 152,16,numGroupID,1,1,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0 FROM AuthenticationGroupMaster WHERE numGroupID=@numGroupId2
						UNION
						SELECT 153,16,numGroupID,1,1,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0 FROM AuthenticationGroupMaster WHERE numGroupID=@numGroupId2
						UNION
						SELECT 154,16,numGroupID,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0 FROM AuthenticationGroupMaster WHERE numGroupID=@numGroupId2
						UNION
						SELECT 155,16,numGroupID,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0 FROM AuthenticationGroupMaster WHERE numGroupID=@numGroupId2
						UNION
						SELECT 156,16,numGroupID,1,1,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0 FROM AuthenticationGroupMaster WHERE numGroupID=@numGroupId2
						UNION
						SELECT 157,16,numGroupID,0,0,0,0,0,0,0,0,1,1,0,0,0,0,0,0,0,0,0,0 FROM AuthenticationGroupMaster WHERE numGroupID=@numGroupId2
						UNION
						SELECT 158,16,numGroupID,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0 FROM AuthenticationGroupMaster WHERE numGroupID=@numGroupId2
						UNION
						SELECT 159,16,numGroupID,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0 FROM AuthenticationGroupMaster WHERE numGroupID=@numGroupId2
						UNION
						SELECT 160,16,numGroupID,1,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0 FROM AuthenticationGroupMaster WHERE numGroupID=@numGroupId2 
						UNION
						SELECT 161,16,numGroupID,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0 FROM AuthenticationGroupMaster WHERE numGroupID=@numGroupId2
						UNION
						SELECT 162,16,numGroupID,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0 FROM AuthenticationGroupMaster WHERE numGroupID=@numGroupId2
						UNION
						SELECT 163,16,numGroupID,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0 FROM AuthenticationGroupMaster WHERE numGroupID=@numGroupId2
						UNION
						SELECT 164,16,numGroupID,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0 FROM AuthenticationGroupMaster WHERE numGroupID=@numGroupId2
						UNION
						SELECT 165,16,numGroupID,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0 FROM AuthenticationGroupMaster WHERE numGroupID=@numGroupId2

						INSERT INTO GroupAuthorization
						(
							numDomainID,numGroupID,numPageID,numModuleID,intViewAllowed,intAddAllowed,intUpdateAllowed,intDeleteAllowed,intExportAllowed,intPrintAllowed
						)
						SELECT numDomainID,numGroupID,152,16,1,0,0,0,0,0 FROM AuthenticationGroupMaster WHERE numGroupID=@numGroupId2 
						UNION
						SELECT numDomainID,numGroupID,153,16,1,0,0,0,0,0 FROM AuthenticationGroupMaster WHERE numGroupID=@numGroupId2
						UNION
						SELECT numDomainID,numGroupID,154,16,0,0,0,0,0,0 FROM AuthenticationGroupMaster WHERE numGroupID=@numGroupId2
						UNION
						SELECT numDomainID,numGroupID,155,16,0,0,0,0,0,0 FROM AuthenticationGroupMaster WHERE numGroupID=@numGroupId2
						UNION
						SELECT numDomainID,numGroupID,156,16,1,0,0,0,0,0 FROM AuthenticationGroupMaster WHERE numGroupID=@numGroupId2
						UNION
						SELECT numDomainID,numGroupID,157,16,1,0,1,0,0,0 FROM AuthenticationGroupMaster WHERE numGroupID=@numGroupId2
						UNION
						SELECT numDomainID,numGroupID,158,16,0,0,0,0,0,0 FROM AuthenticationGroupMaster WHERE numGroupID=@numGroupId2
						UNION
						SELECT numDomainID,numGroupID,159,16,0,0,0,0,0,0 FROM AuthenticationGroupMaster WHERE numGroupID=@numGroupId2
						UNION
						SELECT numDomainID,numGroupID,160,16,3,0,0,0,0,0 FROM AuthenticationGroupMaster WHERE numGroupID=@numGroupId2
						UNION
						SELECT numDomainID,numGroupID,161,16,0,0,0,0,0,0 FROM AuthenticationGroupMaster WHERE numGroupID=@numGroupId2
						UNION
						SELECT numDomainID,numGroupID,162,16,0,0,0,0,0,0 FROM AuthenticationGroupMaster WHERE numGroupID=@numGroupId2
						UNION
						SELECT numDomainID,numGroupID,163,16,0,0,0,0,0,0 FROM AuthenticationGroupMaster WHERE numGroupID=@numGroupId2
						UNION
						SELECT numDomainID,numGroupID,164,16,0,0,0,0,0,0 FROM AuthenticationGroupMaster WHERE numGroupID=@numGroupId2
						UNION
						SELECT numDomainID,numGroupID,165,16,0,0,0,0,0,0 FROM AuthenticationGroupMaster WHERE numGroupID=@numGroupId2

						INSERT INTO PageMasterGroupSetting 
						(
							numPageID,numModuleID,numGroupID
							,bitIsViewApplicable,bitViewPermission1Applicable,bitViewPermission2Applicable,bitViewPermission3Applicable
							,bitIsAddApplicable,bitAddPermission1Applicable,bitAddPermission2Applicable,bitAddPermission3Applicable
							,bitIsUpdateApplicable,bitUpdatePermission1Applicable,bitUpdatePermission2Applicable,bitUpdatePermission3Applicable
							,bitIsDeleteApplicable,bitDeletePermission1Applicable,bitDeletePermission2Applicable,bitDeletePermission3Applicable
							,bitIsExportApplicable,bitExportPermission1Applicable,bitExportPermission2Applicable,bitExportPermission3Applicable
						)
						SELECT 117,40,numGroupID,1,1,0,0,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0 FROM AuthenticationGroupMaster WHERE numGroupID=@numGroupId2

						INSERT INTO GroupAuthorization
						(
							numDomainID,numGroupID,numPageID,numModuleID,intViewAllowed,intAddAllowed,intUpdateAllowed,intDeleteAllowed,intExportAllowed,intPrintAllowed
						)
						SELECT numDomainID,numGroupID,117,40,1,1,0,0,0,0 FROM AuthenticationGroupMaster WHERE numGroupID=@numGroupId2

						INSERT INTO PageMasterGroupSetting 
						(
							numPageID,numModuleID,numGroupID
							,bitIsViewApplicable,bitViewPermission1Applicable,bitViewPermission2Applicable,bitViewPermission3Applicable
							,bitIsAddApplicable,bitAddPermission1Applicable,bitAddPermission2Applicable,bitAddPermission3Applicable
							,bitIsUpdateApplicable,bitUpdatePermission1Applicable,bitUpdatePermission2Applicable,bitUpdatePermission3Applicable
							,bitIsDeleteApplicable,bitDeletePermission1Applicable,bitDeletePermission2Applicable,bitDeletePermission3Applicable
							,bitIsExportApplicable,bitExportPermission1Applicable,bitExportPermission2Applicable,bitExportPermission3Applicable
						)
						SELECT
							PageMaster.numPageID,PageMaster.numModuleID,@numGroupId2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
						FROM
							PageMaster
						WHERE 
							numModuleID=35
							AND numPageID <> 87
						UNION
						SELECT
							PageMaster.numPageID,PageMaster.numModuleID,@numGroupId2,1,1,0,0,0,0,0,0,1,1,0,0,0,0,0,0,1,1,0,0
						FROM
							PageMaster
						WHERE 
							numModuleID=35
							AND numPageID = 87

						INSERT INTO GroupAuthorization
						(
							numDomainID,numGroupID,numPageID,numModuleID,intViewAllowed,intAddAllowed,intUpdateAllowed,intDeleteAllowed,intExportAllowed,intPrintAllowed
						)
						SELECT
							@numTargetDomainID,@numGroupId2,PageMaster.numPageID,PageMaster.numModuleID,0,0,0,0,0,0
						FROM
							PageMaster
						WHERE 
							numModuleID=35
							AND numPageID <> 87
						UNION
						SELECT
							@numTargetDomainID,@numGroupId2,PageMaster.numPageID,PageMaster.numModuleID,1,0,1,0,1,0
						FROM
							PageMaster
						WHERE 
							numModuleID=35
							AND numPageID = 87

						INSERT INTO PageMasterGroupSetting 
						(
							numPageID,numModuleID,numGroupID
							,bitIsViewApplicable,bitViewPermission1Applicable,bitViewPermission2Applicable,bitViewPermission3Applicable
							,bitIsAddApplicable,bitAddPermission1Applicable,bitAddPermission2Applicable,bitAddPermission3Applicable
							,bitIsUpdateApplicable,bitUpdatePermission1Applicable,bitUpdatePermission2Applicable,bitUpdatePermission3Applicable
							,bitIsDeleteApplicable,bitDeletePermission1Applicable,bitDeletePermission2Applicable,bitDeletePermission3Applicable
							,bitIsExportApplicable,bitExportPermission1Applicable,bitExportPermission2Applicable,bitExportPermission3Applicable
						)
						VALUES
						(1,1,@numGroupId2,1,1,0,0,0,0,0,0,1,1,0,0,1,1,0,0,0,0,0,0)
						,(5,1,@numGroupId2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0)
						,(6,1,@numGroupId2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0)
						,(10,1,@numGroupId2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0)

						INSERT INTO GroupAuthorization
						(
							numDomainID,numGroupID,numPageID,numModuleID,intViewAllowed,intAddAllowed,intUpdateAllowed,intDeleteAllowed,intExportAllowed,intPrintAllowed
						)
						VALUES
						(@numTargetDomainID,@numGroupId2,1,1,1,0,1,1,0,0)
						,(@numTargetDomainID,@numGroupId2,5,1,0,0,0,0,0,0)
						,(@numTargetDomainID,@numGroupId2,6,1,0,0,0,0,0,0)
						,(@numTargetDomainID,@numGroupId2,10,1,0,0,0,0,0,0)
                    END                      
                    
					IF NOT EXISTS (SELECT numGroupID FROM AuthenticationGroupMaster WHERE numDomainId = @numTargetDomainID AND vcGroupName = 'Business Portal Users') 
                    BEGIN
						INSERT INTO AuthenticationGroupMaster
                        (
                            vcGroupName,
                            numDomainID,
                            tintGroupType,
                            bitConsFlag 
                        )
                        VALUES
						(
                            'Business Portal Users',
                            @numTargetDomainID,
                            2,
                            1
                        )                      
                        SET @numGroupId2 = SCOPE_IDENTITY()
						INSERT INTO PageMasterGroupSetting 
						(
							numPageID,numModuleID,numGroupID
							,bitIsViewApplicable,bitViewPermission1Applicable,bitViewPermission2Applicable,bitViewPermission3Applicable
							,bitIsAddApplicable,bitAddPermission1Applicable,bitAddPermission2Applicable,bitAddPermission3Applicable
							,bitIsUpdateApplicable,bitUpdatePermission1Applicable,bitUpdatePermission2Applicable,bitUpdatePermission3Applicable
							,bitIsDeleteApplicable,bitDeletePermission1Applicable,bitDeletePermission2Applicable,bitDeletePermission3Applicable
							,bitIsExportApplicable,bitExportPermission1Applicable,bitExportPermission2Applicable,bitExportPermission3Applicable
						)
						SELECT
							numPageID,numModuleID,@numGroupId2
							,1,1,0,0
							,0,0,0,0
							,0,0,0,0
							,0,0,0,0
							,0,0,0,0
						FROM 
							PageMaster
						WHERE
							numModuleID IN (32,2,3,4,11,37,10,46,12,7,16)
						      
						INSERT INTO GroupAuthorization
						(
							numPageID,numModuleID,numGroupID,intExportAllowed,intPrintAllowed,intViewAllowed,intAddAllowed,intUpdateAllowed,intDeleteAllowed,numDomainID
						)
						SELECT
							numPageID,numModuleID,@numGroupId2,0,0,1,0,0,0,@numTargetDomainID
						FROM 
							PageMaster
						WHERE
							numModuleID IN (32,2,3,4,11,37,10,46,12,7,16)
							AND NOT EXISTS (SELECT numPageID FROM GroupAuthorization GA WHERE GA.numPageID=PageMaster.numPageID AND GA.numModuleID=PageMaster.numModuleID AND GA.numGroupID=@numGroupId2)

						INSERT INTO GroupAuthorization
						(
							numPageID,numModuleID,numGroupID,intExportAllowed,intPrintAllowed,intViewAllowed,intAddAllowed,intUpdateAllowed,intDeleteAllowed,numDomainID
						)
						SELECT
							numListItemID,32,@numGroupId2,0,0,1,0,0,0,@numTargetDomainID
						FROM 
						(	
							SELECT 
								numListItemID 
							FROM 
								ListDetails 
							WHERE 
								numListID=5 
								AND ((constFlag=1 AND numListItemID <> 46) OR constFlag=0)
						) PageMaster
						WHERE
							NOT EXISTS (SELECT numPageID FROM GroupAuthorization GA WHERE GA.numPageID=PageMaster.numListItemID AND GA.numModuleID=32 AND GA.numGroupID=@numGroupId2)
   
        
						UPDATE PageMasterGroupSetting SET bitIsViewApplicable=0,bitViewPermission1Applicable=0 WHERE numModuleID=4 AND numPageID IN (12,13,16,17)
						UPDATE GroupAuthorization SET intViewAllowed=0 WHERE numModuleID=4 AND numPageID IN (12,13,16,17) AND numGroupID = @numGroupId2
						UPDATE PageMasterGroupSetting SET bitIsViewApplicable=0,bitViewPermission1Applicable=0 WHERE numModuleID=11 AND numPageID IN (1,134)
						UPDATE GroupAuthorization SET intViewAllowed=0 WHERE numModuleID=11 AND numPageID IN (1,134) AND numGroupID = @numGroupId2
						UPDATE PageMasterGroupSetting SET bitIsViewApplicable=0,bitViewPermission1Applicable=0 WHERE numModuleID=37 AND numPageID IN (34,130,132,149)
						UPDATE GroupAuthorization SET intViewAllowed=0 WHERE numModuleID=37 AND numPageID IN (34,130,132,149) AND numGroupID = @numGroupId2
						UPDATE PageMasterGroupSetting SET bitIsViewApplicable=0,bitViewPermission1Applicable=0 WHERE numModuleID=2 AND numPageID IN (7,9,10,11,12)
						UPDATE GroupAuthorization SET intViewAllowed=0 WHERE numModuleID=2 AND numPageID IN (7,9,10,11,12) AND numGroupID = @numGroupId2
						UPDATE PageMasterGroupSetting SET bitIsViewApplicable=0,bitViewPermission1Applicable=0 WHERE numModuleID=16 AND numPageID IN (154,155,158,159,161,162,163,164,165)
						UPDATE GroupAuthorization SET intViewAllowed=0 WHERE numModuleID=16 AND numPageID IN (154,155,158,159,161,162,163,164,165) AND numGroupID = @numGroupId2
						UPDATE PageMasterGroupSetting SET bitIsViewApplicable=0,bitViewPermission1Applicable=0 WHERE numModuleID=10 AND numPageID IN (15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35)
						UPDATE GroupAuthorization SET intViewAllowed=0 WHERE numModuleID=10 AND numPageID IN (15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35) AND numGroupID = @numGroupId2
						UPDATE PageMasterGroupSetting SET bitIsViewApplicable=0,bitViewPermission1Applicable=0 WHERE numModuleID=12 AND numPageID IN (5,6,7,8,13,14,16,17)
						UPDATE GroupAuthorization SET intViewAllowed=0 WHERE numModuleID=12 AND numPageID IN (5,6,7,8,13,14,16,17) AND numGroupID = @numGroupId2
						UPDATE PageMasterGroupSetting SET bitIsViewApplicable=0,bitViewPermission1Applicable=0 WHERE numModuleID=3 AND numPageID IN (10,11,13,14,15)
						UPDATE GroupAuthorization SET intViewAllowed=0 WHERE numModuleID=3 AND numPageID IN (10,11,13,14,15) AND numGroupID = @numGroupId2
						UPDATE PageMasterGroupSetting SET bitIsViewApplicable=0,bitViewPermission1Applicable=0 WHERE numModuleID=32 AND numPageID IN (2)
						UPDATE GroupAuthorization SET intViewAllowed=0 WHERE numModuleID=32 AND numPageID IN (2) AND numGroupID = @numGroupId2
						UPDATE PageMasterGroupSetting SET bitIsViewApplicable=0,bitViewPermission1Applicable=0 WHERE numModuleID=7 AND numPageID IN (1,12,13)
						UPDATE GroupAuthorization SET intViewAllowed=0 WHERE numModuleID=7 AND numPageID IN (1,12,13) AND numGroupID = @numGroupId2
						UPDATE PageMasterGroupSetting SET bitIsAddApplicable=1,bitAddPermission1Applicable=1 WHERE numModuleID=7 AND numPageID = 1
						UPDATE GroupAuthorization SET intAddAllowed=1 WHERE numModuleID=7 AND numPageID = 1 AND numGroupID = @numGroupId2
						UPDATE PageMasterGroupSetting SET bitIsAddApplicable=1,bitAddPermission1Applicable=1,bitIsUpdateApplicable=1,bitUpdatePermission1Applicable=1,bitIsDeleteApplicable=1,bitDeletePermission1Applicable=1 WHERE numModuleID=10 AND numPageID = 5
						UPDATE GroupAuthorization SET intAddAllowed=1,intUpdateAllowed=1,intDeleteAllowed=1 WHERE numModuleID=10 AND numPageID = 5 AND numGroupID = @numGroupId2
						UPDATE PageMasterGroupSetting SET bitIsAddApplicable=1,bitAddPermission1Applicable=1 WHERE numModuleID=10 AND numPageID = 28
						UPDATE GroupAuthorization SET intAddAllowed=1 WHERE numModuleID=10 AND numPageID = 28 AND numGroupID = @numGroupId2
						UPDATE PageMasterGroupSetting SET bitIsAddApplicable=1,bitAddPermission1Applicable=1 WHERE numModuleID=10 AND numPageID = 1
						UPDATE GroupAuthorization SET intAddAllowed=1 WHERE numModuleID=10 AND numPageID = 1 AND numGroupID = @numGroupId2

						DELETE FROM GroupTabDetails WHERE numTabId IN (1,4,5,7,80,112,132,133,134,135) AND numGroupId IN (SELECT numGroupId FROM AuthenticationGroupMaster WHERE tintGroupType=2)

						DECLARE @TEMPTabs TABLE
						(
							numTabID INT
							,numOrder INT
						)
						INSERT INTO @TEMPTabs (numTabID,numOrder) VALUES (7,1),(80,2),(1,3),(134,4),(133,5),(135,6),(5,7),(4,8),(112,9),(132,10)

						INSERT INTO GroupTabDetails
						(
							numGroupId,numTabId,numRelationShip,bitallowed,numOrder
						)
						SELECT
							@numGroupId2,numTabID,0,1,numOrder
						FROM
							@TEMPTabs


						DECLARE @TEMP TABLE
						(
							numPageNavID INT
						)

						DELETE FROM @TEMP
						DELETE FROM TreeNavigationAuthorization WHERE numTabID=1 AND numGroupID = @numGroupId2

						INSERT INTO @TEMP (numPageNavID) VALUES (122),(123),(125),(149)

						INSERT INTO TreeNavigationAuthorization
						(
							numDomainID,numGroupID,numTabID,numPageNavID,bitVisible,tintType
						)
						SELECT
							@numTargetDomainID,@numGroupId2,1,numPageNavID,1,1
						FROM
							@TEMP

						-----------------------------------------

						DELETE FROM @TEMP
						DELETE FROM TreeNavigationAuthorization WHERE numTabID=4 AND numGroupID = @numGroupId2

						INSERT INTO @TEMP (numPageNavID) VALUES (246),(247),(248)

						INSERT INTO TreeNavigationAuthorization
						(
							numDomainID,numGroupID,numTabID,numPageNavID,bitVisible,tintType
						)
						SELECT
							@numTargetDomainID,@numGroupId2,4,numPageNavID,1,1
						FROM
							@TEMP

						------------------------------------------

						DELETE FROM @TEMP
						DELETE FROM TreeNavigationAuthorization WHERE numTabID=5 AND numGroupID = @numGroupId2

						INSERT INTO @TEMP (numPageNavID) VALUES (279),(280)

						INSERT INTO TreeNavigationAuthorization
						(
							numDomainID,numGroupID,numTabID,numPageNavID,bitVisible,tintType
						)
						SELECT
							@numTargetDomainID,@numGroupId2,5,numPageNavID,1,1
						FROM
							@TEMP

						-----------------------------------------

						DELETE FROM @TEMP
						DELETE FROM TreeNavigationAuthorization WHERE numTabID=7 AND numGroupID = @numGroupId2

						
						INSERT INTO @TEMP (numPageNavID) VALUES (6),(7),(11),(12)

						INSERT INTO TreeNavigationAuthorization
						(
							numDomainID,numGroupID,numTabID,numPageNavID,bitVisible,tintType
						)
						SELECT
							@numTargetDomainID,@numGroupId2,7,numPageNavID,1,1
						FROM
							@TEMP

						------------------------------------------

						DELETE FROM @TEMP
						DELETE FROM TreeNavigationAuthorization WHERE numTabID=80 AND numGroupID = @numGroupId2

						INSERT INTO @TEMP (numPageNavID) VALUES (62),(1),(2),(3),(4),(71),(72),(76),(171),(240)

						INSERT INTO TreeNavigationAuthorization
						(
							numDomainID,numGroupID,numTabID,numPageNavID,bitVisible,tintType
						)
						SELECT
							@numTargetDomainID,@numGroupId2,80,numPageNavID,1,1
						FROM
							@TEMP

						-----------------------------------------

						DELETE FROM @TEMP
						DELETE FROM TreeNavigationAuthorization WHERE numTabID=132 AND numGroupID = @numGroupId2

						INSERT INTO @TEMP (numPageNavID) VALUES (270),(84)

						INSERT INTO TreeNavigationAuthorization
						(
							numDomainID,numGroupID,numTabID,numPageNavID,bitVisible,tintType
						)
						SELECT
							@numTargetDomainID,@numGroupId2,132,numPageNavID,1,1
						FROM
							@TEMP

						------------------------------------------

						DELETE FROM @TEMP
						DELETE FROM TreeNavigationAuthorization WHERE numTabID=133 AND numGroupID = @numGroupId2

						INSERT INTO @TEMP (numPageNavID) VALUES (273),(124),(126),(207)

						INSERT INTO TreeNavigationAuthorization
						(
							numDomainID,numGroupID,numTabID,numPageNavID,bitVisible,tintType
						)
						SELECT
							@numTargetDomainID,@numGroupId2,133,numPageNavID,1,1
						FROM
							@TEMP

						-----------------------------------------

						DELETE FROM @TEMP
						DELETE FROM TreeNavigationAuthorization WHERE numTabID=134 AND numGroupID = @numGroupId2

						INSERT INTO @TEMP (numPageNavID) VALUES (13)

						INSERT INTO TreeNavigationAuthorization
						(
							numDomainID,numGroupID,numTabID,numPageNavID,bitVisible,tintType
						)
						SELECT
							@numTargetDomainID,@numGroupId2,134,numPageNavID,1,1
						FROM
							@TEMP

						-----------------------------------------

						DELETE FROM @TEMP
						DELETE FROM TreeNavigationAuthorization WHERE numTabID=135 AND numGroupID = @numGroupId2

						INSERT INTO @TEMP (numPageNavID) VALUES (274),(275)

						INSERT INTO TreeNavigationAuthorization
						(
							numDomainID,numGroupID,numTabID,numPageNavID,bitVisible,tintType
						)
						SELECT
							@numTargetDomainID,@numGroupId2,135,numPageNavID,1,1
						FROM
							@TEMP
                    END                      
----                   
                    EXEC USP_ChartAcntDefaultValues @numDomainId = @numTargetDomainID,
                        @numUserCntId = @numNewContactID      
    
    
-- Dashboard Size    
                    INSERT  INTO DashBoardSize
                            SELECT  1,
                                    1,
                                    @numGroupId1,
                                    1
                            UNION
                            SELECT  2,
                                    1,
                                    @numGroupId1,
                                    1
                            UNION
                            SELECT  3,
                                    2,
                                    @numGroupId1,
                                    1     
    
----inserting all the Custome Reports for the newly created group    
    
                    INSERT  INTO DashboardAllowedReports
                            SELECT  numCustomReportID,
                                    @numGroupId1
                            FROM    CustomReport
                            WHERE   numDomainID = @numTargetDomainID    
    
    
           
 --inserting Default BizDocs for new domain
INSERT INTO dbo.AuthoritativeBizDocs
        ( numAuthoritativePurchase ,
          numAuthoritativeSales ,
          numDomainId
        )
SELECT  
ISNULL((SELECT numListItemID  FROM dbo.ListDetails WHERE numListID=27 AND constFlag =1 AND vcData = 'bill'),0),
ISNULL((SELECT numListItemID  FROM dbo.ListDetails WHERE numListID=27 AND constFlag =1 AND vcData = 'invoice'),0),
@numTargetDomainID


IF NOT EXISTS(SELECT * FROM PortalBizDocs WHERE numDomainID=@numTargetDomainID)
BEGIN
	--delete from PortalBizDocs WHERE numDomainID=176
	INSERT INTO dbo.PortalBizDocs
        ( numBizDocID, numDomainID )
	SELECT numListItemID,@numTargetDomainID FROM dbo.ListDetails WHERE numListID=27 AND constFlag=1
END



---Set BizDoc Type Filter for Sales and Purchase BizDoc Type
INSERT INTO dbo.BizDocFilter( numBizDoc ,tintBizocType ,numDomainID)
SELECT numListItemID,1,@numTargetDomainID FROM dbo.ListDetails WHERE numListID=27 AND constFlag=1 AND vcData IN ('Invoice','Sales Opportunity','Sales Order','Sales Credit Memo','Fulfillment Order','Credit Memo','Refund Receipt','RMA','Packing Slip','Pick List')

INSERT INTO dbo.BizDocFilter( numBizDoc ,tintBizocType ,numDomainID)
SELECT numListItemID,2,@numTargetDomainID FROM dbo.ListDetails WHERE numListID=27 AND constFlag=1 AND vcData IN ('Purchase Opportunity','Bill','Purchase Order','Purchase Credit Memo','RMA')



--- Give permission of all tree node to all Groups
exec USP_CreateTreeNavigationForDomain @numTargetDomainID,0

--Create Default BizDoc template for all system bizdocs, Css and template html will be updated though same SP from code.
EXEC dbo.USP_CreateBizDocTemplateByDefault @numDomainID = @numTargetDomainID, -- numeric
    @txtBizDocTemplate = '', -- text
    @txtCss = '', -- text
    @tintMode = 2,
    @txtPackingSlipBizDocTemplate = ''
 

          
 --inserting the details of BizForms Wizard          
                    INSERT  INTO DycFormConfigurationDetails(numFormId,numFieldId,numViewId,intColumnNum,intRowNum,boolAOIField,numAuthGroupID
								,numDomainId,numSurId)
                            SELECT  numFormId,numFieldId,numViewId,intColumnNum,intRowNum,boolAOIField,@numGroupId1,
									@numTargetDomainID,0
                            FROM    DycFormConfigurationDetails
                            WHERE   numDomainID = 0
                                    AND numFormID IN ( 1, 2, 6 )          
          
                    INSERT  INTO DycFormConfigurationDetails(numFormId,numFieldId,numViewId,intColumnNum,intRowNum,boolAOIField,numAuthGroupID
								,numDomainId,numSurId)
                            SELECT  numFormId,numFieldId,numViewId,intColumnNum,intRowNum,boolAOIField,0,
									@numTargetDomainID,0
                            FROM    DycFormConfigurationDetails
                            WHERE   numDomainID = 0
                                    AND numFormID IN ( 3, 4, 5 )          
          
                    INSERT  INTO DycFormConfigurationDetails(numFormId,numFieldId,numViewId,intColumnNum,intRowNum,boolAOIField,numAuthGroupID
								,numDomainId,numSurId)
                            SELECT  numFormId,numFieldId,numViewId,intColumnNum,intRowNum,boolAOIField,numAuthGroupID,
									@numTargetDomainID,0
                            FROM    DycFormConfigurationDetails
                            WHERE   numDomainID = 0
                                    AND numFormID IN ( 7, 8 )          
          
          
          
          ---Insert Default Add Relationship Field Lead/Prospect/Account and Other
          INSERT INTO DycFormConfigurationDetails(numFormId,numFieldId,intColumnNum,intRowNum,
								numDomainId,numUserCntID,numRelCntType,tintPageType,bitCustom)
               SELECT  numFormId,numFieldId,intColumnNum,intRowNum,@numTargetDomainID,0,
								numRelCntType,tintPageType,bitCustom
                     FROM DycFormConfigurationDetails
                     WHERE numDomainID = 0 AND numFormID IN (34,35,36) AND tintPageType=2
          
          ---Set Default Validation for Lead/Prospect/Account
         INSERT INTO DynamicFormField_Validation(numFormId,numFormFieldId,numDomainId,vcNewFormFieldName,
   bitIsRequired,bitIsNumeric,bitIsAlphaNumeric,bitIsEmail,bitIsLengthValidation,intMaxLength,intMinLength,
   bitFieldMessage,vcFieldMessage)
    SELECT numFormId,numFormFieldId,@numTargetDomainID,vcNewFormFieldName,
   bitIsRequired,bitIsNumeric,bitIsAlphaNumeric,bitIsEmail,bitIsLengthValidation,intMaxLength,intMinLength,
   bitFieldMessage,vcFieldMessage
                     FROM DynamicFormField_Validation
                     WHERE numDomainID = 0 AND numFormID IN (34,35,36)
                              
                    
                    DECLARE @numListItemID AS NUMERIC(9)                    
                    DECLARE @numNewListItemID AS NUMERIC(9)                    
                    SELECT TOP 1
                            @numListItemID = numListItemID
                    FROM    ListDetails
                    WHERE   numDomainID = 1
                            AND numListID = 40                    
                    
                    WHILE @numListItemID > 0                    
                        BEGIN                    
                    
                            INSERT  INTO ListDetails
                                    (
                                      numListID,
                                      vcData,
                                      numCreatedBY,
                                      bintCreatedDate,
                                      numModifiedBy,
                                      bintModifiedDate,
                                      bitDelete,
                                      numDomainID,
                                      constFlag,
                                      sintOrder
                                    )
                                    SELECT  numListID,
                                            vcData,
                                            @numNewContactID,
                                            GETUTCDATE(),
                                            @numNewContactID,
                                            GETUTCDATE(),
                                            bitDelete,
                                            @numTargetDomainID,
                                            constFlag,
                                            sintOrder
                                    FROM    ListDetails
                                    WHERE   numListItemID = @numListItemID                    
                            SELECT  @numNewListItemID = SCOPE_IDENTITY()                    
                     
                            INSERT  INTO [state]
                                    (
                                      numCountryID,
                                      vcState,
                                      numCreatedBy,
                                      bintCreatedDate,
                                      numModifiedBy,
                                      bintModifiedDate,
                                      numDomainID,
                                      constFlag,
									  numShippingZone,
									  vcAbbreviations
                                    )
                                    SELECT  @numNewListItemID,
                                            vcState,
                                            @numNewContactID,
                                            GETUTCDATE(),
                                            @numNewContactID,
                                            GETUTCDATE(),
                                            @numTargetDomainID,
                                            constFlag,
											numShippingZone,
											vcAbbreviations
                                    FROM    [State]
                                    WHERE   numCountryID = @numListItemID                    
                            SELECT TOP 1
                                    @numListItemID = numListItemID
                            FROM    ListDetails
                            WHERE   numDomainID = 1
                                    AND numListID = 40
                                    AND numListItemID > @numListItemID                    
                            IF @@rowcount = 0 
                                SET @numListItemID = 0                    
                        END                    
                    
                    --INsert Net Terms
                   	INSERT INTO [ListDetails] ([numListID],[vcData],[numCreatedBY],[bitDelete],[numDomainID],[constFlag],[sintOrder])
								  SELECT 296,'0',1,0,@numTargetDomainID,0,1
						UNION ALL SELECT 296,'7',1,0,@numTargetDomainID,0,2
						UNION ALL SELECT 296,'15',1,0,@numTargetDomainID,0,3
						UNION ALL SELECT 296,'30',1,0,@numTargetDomainID,0,4
						UNION ALL SELECT 296,'45',1,0,@numTargetDomainID,0,5
						UNION ALL SELECT 296,'60',1,0,@numTargetDomainID,0,6
                    --Insert Default email templates
                    INSERT INTO dbo.GenericDocuments (
						VcFileName,
						vcDocName,
						numDocCategory,
						cUrlType,
						vcFileType,
						numDocStatus,
						vcDocDesc,
						numDomainID,
						numCreatedBy,
						bintCreatedDate,
						numModifiedBy,
						bintModifiedDate,
						vcSubject,
						vcDocumentSection,
						numRecID,
						tintCheckOutStatus,
						intDocumentVersion,
						numLastCheckedOutBy,
						dtCheckOutDate,
						dtCheckInDate,
						tintDocumentType,
						numOldSpecificDocID,
						numModuleID
					)  
					SELECT  VcFileName,
							vcDocName,
							numDocCategory,
							cUrlType,
							vcFileType,
							numDocStatus,
							vcDocDesc,
							@numTargetDomainID,
							@numNewContactID,
							GETUTCDATE(),
							@numNewContactID,
							GETUTCDATE(),
							vcSubject,
							vcDocumentSection,
							numRecID,
							tintCheckOutStatus,
							intDocumentVersion,
							numLastCheckedOutBy,
							dtCheckOutDate,
							dtCheckInDate,
							tintDocumentType,
							numOldSpecificDocID,
							numModuleID
					FROM    dbo.GenericDocuments
					WHERE   numDomainID = 0
                       
                    INSERT  INTO UserMaster
                            (
                              vcUserName,
                              vcUserDesc,
                              vcMailNickName,
                              numGroupID,
                              numDomainID,
                              numCreatedBy,
                              bintCreatedDate,
                              numModifiedBy,
                              bintModifiedDate,
                              bitActivateFlag,
                              numUserDetailId,
                              vcEmailID
                            )
                            SELECT  vcFirstName,
                                    vcFirstName,
                                    vcFirstName,
                                    @numGroupId1,
                                    @numTargetDomainID,
                                    @numUserContactID,
                                    GETUTCDATE(),
                                    @numUserContactID,
                                    GETUTCDATE(),
                                    0,
                                    @numNewContactID,
                                    vcEmail
                            FROM    AdditionalContactsInformation
                            WHERE   numContactID = @numAdminContactID                                            
                          
                           INSERT INTO UOM (numDomainId,vcUnitName,tintUnitType,bitEnabled)
			                SELECT @numTargetDomainID, vcUnitName,tintUnitType,bitEnabled FROM UOM WHERE numDomainId=0    
                    
                    if not exists (select * from Currency where numDomainID=@numTargetDomainID)
						insert into Currency (vcCurrencyDesc,chrCurrency,varCurrSymbol,numDomainID,fltExchangeRate,bitEnabled)
							select vcCurrencyDesc, chrCurrency,varCurrSymbol, @numTargetDomainID, fltExchangeRate, bitEnabled from Currency where numDomainID=0
                    
                    ------- Insert Detail For Default Currency In New Domain Entry -------------------------
					DECLARE @numCountryID AS NUMERIC(18,0)
					SELECT @numCountryID = numListItemID FROM dbo.ListDetails WHERE numListID = 40 AND vcData = 'United States' AND numDomainID = @numTargetDomainID

					--IF NOT EXISTS(SELECT * FROM Currency WHERE numCountryId = @numCountryID AND numDomainID = @numTargetDomainID AND vcCurrencyDesc = 'USD-U.S. Dollar')
					IF NOT EXISTS(SELECT * FROM Currency WHERE numDomainID = @numTargetDomainID AND vcCurrencyDesc = 'USD-U.S. Dollar')
					BEGIN
					PRINT 1
						INSERT  INTO dbo.Currency(vcCurrencyDesc,chrCurrency,varCurrSymbol,numDomainID,fltExchangeRate,bitEnabled,numCountryId)
						SELECT  TOP 1 vcCurrencyDesc,chrCurrency,varCurrSymbol,@numTargetDomainID,ISNULL(fltExchangeRate,1),1 AS bitEnabled,@numCountryId FROM dbo.Currency 
						WHERE vcCurrencyDesc = 'USD-U.S. Dollar' AND numDomainID=0
						UNION 
						SELECT  vcCurrencyDesc,chrCurrency,varCurrSymbol,@numTargetDomainID,fltExchangeRate,0 AS bitEnabled,NULL FROM dbo.Currency 
						WHERE vcCurrencyDesc <> 'USD-U.S. Dollar' AND numDomainID=0
					END
					ELSE
						BEGIN
						PRINT 2
							UPDATE Currency SET numCountryId = @numCountryID, bitEnabled = 1, fltExchangeRate=1
							WHERE numDomainID = @numTargetDomainID
							AND vcCurrencyDesc = 'USD-U.S. Dollar'                        
						PRINT 3							
						END
						
					--Set Default Currency
                    DECLARE @numCurrencyID AS NUMERIC(18)
                    SET @numCurrencyID=(SELECT TOP 1 numCurrencyID FROM Currency WHERE (bitEnabled=1 or chrCurrency='USD') AND numDomainID=@numTargetDomainID)
                   
					--Set Default Country
					DECLARE @numDefCountry AS NUMERIC(18)
                    SET @numDefCountry=(SELECT TOP 1 numListItemID FROM ListDetails where numlistid=40 AND numDomainID=@numTargetDomainID and vcdata like '%United States%')		
					                    
                    UPDATE  Domain
                    SET     numDivisionID = @numNewDivisionID,
                            numAdminID = @numNewContactID,numCurrencyID=@numCurrencyID,numDefCountry=isnull(@numDefCountry,0)
                    WHERE   numdomainID = @numTargetDomainID                                      
                         	
					------------------


                         
                    INSERT  INTO Subscribers
                            (
                              numDivisionID,
                              numAdminContactID,
                              numTargetDomainID,
                              numDomainID,
                              numCreatedBy,
                              dtCreatedDate,
                              numModifiedBy,
                              dtModifiedDate,
                              dtEmailStartDate,
                              dtEmailEndDate,
							  bitEnabledAccountingAudit,
							  bitEnabledNotifyAdmin
                            )
                    VALUES  (
                              @numDivisionID,
                              @numNewContactID,
                              @numTargetDomainID,
                              @numDomainID,
                              @numUserContactID,
                              GETUTCDATE(),
                              @numUserContactID,
                              GETUTCDATE(),
                              dbo.GetUTCDateWithoutTime(),
                              dbo.GetUTCDateWithoutTime(),
							  @bitEnabledAccountingAudit,
							  @bitEnabledNotifyAdmin
                            )                                    
                    SET @numSubscriberID = SCOPE_IDENTITY()                        
                      
                      
                              
                END                                
        ELSE 
        BEGIN                        
            SELECT  @numSubscriberID = numSubscriberID
            FROM    Subscribers
            WHERE   numDivisionID = @numDivisionID
                    AND numDomainID = @numDomainID                                
            UPDATE  Subscribers
            SET     bitDeleted = 0
            WHERE   numSubscriberID = @numSubscriberID                                
        END       
    END                                    
    ELSE 
    BEGIN
        DECLARE @bitOldStatus AS BIT                                  
        SET @bitExists = 1                  
            
		SELECT 
			@bitOldStatus = bitActive,
            @numTargetDomainID = numTargetDomainID
        FROM 
			Subscribers
        WHERE 
			numSubscriberID = @numSubscriberID                                    
                                  
        IF (@bitOldStatus = 1 AND @bitActive = 0) 
            UPDATE Subscribers SET dtSuspendedDate = GETUTCDATE() WHERE numSubscriberID = @numSubscriberID                                  
                                  
        UPDATE 
			Subscribers
        SET 
			intNoofUsersSubscribed = @intNoofUsersSubscribed,
            dtSubStartDate = @dtSubStartDate,
            dtSubEndDate = @dtSubEndDate,
            bitTrial = @bitTrial,
            numAdminContactID = @numAdminContactID,
            bitActive = @bitActive,
            vcSuspendedReason = @vcSuspendedReason,
            numModifiedBy = @numUserContactID,
            dtModifiedDate = GETUTCDATE(),
			bitEnabledAccountingAudit = @bitEnabledAccountingAudit,
			bitEnabledNotifyAdmin = @bitEnabledNotifyAdmin,
			intNoofLimitedAccessUsers = @intNoofLimitedAccessUsers,
			intNoofBusinessPortalUsers = @intNoofBusinessPortalUsers,
			monFullUserCost = @monFullUserCost,
			monLimitedAccessUserCost = @monLimitedAccessUserCost,
			monBusinessPortalUserCost = @monBusinessPortalUserCost,
			monSiteCost = @monSiteCost,
			intFullUserConcurrency=@intFullUserConcurrency
        WHERE 
			numSubscriberID = @numSubscriberID                 
                       
        UPDATE 
			Domain
        SET
			numAdminID = @numAdminContactID
        WHERE 
			numdomainID = @numTargetDomainID   
                           
        DECLARE @hDoc1 INT                              
        EXEC sp_xml_preparedocument @hDoc1 OUTPUT, @strUsers                                                              
                                                               
        INSERT INTO UserMaster
        (
            vcUserName,
            vcMailNickName,
            vcUserDesc,
            numGroupID,
            numDomainID,
            numCreatedBy,
            bintCreatedDate,
            numModifiedBy,
            bintModifiedDate,
            bitActivateFlag,
            numUserDetailId,
            vcEmailID,
            vcPassword
        )
        SELECT  UserName,
                UserName,
                UserName,
                0,
                @numTargetDomainID,
                @numUserContactID,
                GETUTCDATE(),
                @numUserContactID,
                GETUTCDATE(),
                Active,
                numContactID,
                Email,
                vcPassword
        FROM    ( SELECT    *
                    FROM      OPENXML (@hDoc1, '/NewDataSet/Users[numUserID=0]',2)
                            WITH ( numContactID NUMERIC(9), numUserID NUMERIC(9), Email VARCHAR(100), vcPassword VARCHAR(100), UserName VARCHAR(100), Active TINYINT )
                ) X                                
                
                              
        UPDATE  UserMaster
        SET     vcUserName = X.UserName,
                numModifiedBy = @numUserContactID,
                bintModifiedDate = GETUTCDATE(),
                vcEmailID = X.Email,
                vcPassword = X.vcPassword,
                bitActivateFlag = Active
        FROM    ( SELECT    numUserID AS UserID,
                            numContactID,
                            Email,
                            vcPassword,
                            UserName,
                            Active
                    FROM      OPENXML (@hDoc1, '/NewDataSet/Users[numUserID>0]',2)
                            WITH ( numContactID NUMERIC(9), numUserID NUMERIC(9), Email VARCHAR(100), vcPassword VARCHAR(100), UserName VARCHAR(100), Active TINYINT )
                ) X
        WHERE   numUserID = X.UserID                                                              
                                                               
                                                               
        EXEC sp_xml_removedocument @hDoc1                               
                                   
    END
	                           
    SELECT  @numSubscriberID
GO
