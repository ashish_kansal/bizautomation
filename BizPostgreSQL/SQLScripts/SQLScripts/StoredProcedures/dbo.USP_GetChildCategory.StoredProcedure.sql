/****** Object:  StoredProcedure [dbo].[USP_GetChildCategory]    Script Date: 07/26/2008 16:16:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by Siva    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getchildcategory')
DROP PROCEDURE usp_getchildcategory
GO
CREATE PROCEDURE [dbo].[USP_GetChildCategory]       
  @numParntAcntId as numeric(9)=0,    
  @numDomainId as numeric(9)=0    
  as      
Begin   
	  Select * From Chart_Of_Accounts COA
	  inner join ListDetails LD on COA.numAcntTypeID= LD.numListItemID  Where COA.numParntAcntTypeId=@numParntAcntId And COA.numDomainId = @numDomainId       
End
GO
