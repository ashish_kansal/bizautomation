GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetBizDocForPackingSlip')
DROP PROCEDURE USP_GetBizDocForPackingSlip
GO
CREATE PROCEDURE [dbo].[USP_GetBizDocForPackingSlip]
	@numOppId AS NUMERIC(9)  = NULL,
	@numDomainID AS NUMERIC(9)  = 0
AS
BEGIN  
	--SELECT vcBizDocID FROM OpportunityBizDocs WHERE numOppId = @numOppId AND vcBizDocName = 'PAC-'
	SELECT vcBizDocID FROM OpportunityBizDocs WHERE numOppId = @numOppId AND numBizDocID=29397
END
