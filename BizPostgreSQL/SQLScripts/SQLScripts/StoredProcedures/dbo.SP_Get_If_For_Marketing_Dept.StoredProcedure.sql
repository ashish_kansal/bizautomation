/****** Object:  StoredProcedure [dbo].[SP_Get_If_For_Marketing_Dept]    Script Date: 07/26/2008 16:14:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--select * from GenericDocuments  

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='sp_get_if_for_marketing_dept')
DROP PROCEDURE sp_get_if_for_marketing_dept
GO
CREATE PROCEDURE [dbo].[SP_Get_If_For_Marketing_Dept]
@numGenericDocID Int
As
If Exists ( Select 1 From GenericDocuments Where numGenericDocID = @numGenericDocID And numDocCategory = 369 And vcDocumentSection = 'M' )
	Select 1
Else
	Select -1
GO
