
GO
/****** Object:  StoredProcedure [dbo].[USP_GetComDivContactID]    Script Date: 05/07/2009 17:52:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- created by anoop jayaraj  
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getcomdivcontactid')
DROP PROCEDURE usp_getcomdivcontactid
GO
CREATE PROCEDURE [dbo].[USP_GetComDivContactID]  
@numCompanyID as numeric(9)=0 output,  
@numDivisionID as numeric(9)=0 output,  
@numContactID as numeric(9)=0 output,  
@vcCompanyName AS VARCHAR(50)='' OUTPUT,
@Email as varchar(100)='',
@numDomainID AS NUMERIC(9)
  
  
  
as  
  
  
select top 1 @numContactID=A.numcontactid,  
@numDivisionID=D.numdivisionid,  
@numCompanyID=C.numCompanyID,
@vcCompanyName =C.vcCompanyName
from AdditionalContactsInformation A  
join DivisionMaster D  
on D.numDivisionID=A.numDivisionID  
join CompanyInfo C  
on C.numCompanyID=D.numCompanyID  
where vcEmail=@Email  and vcEmail<>'' and vcEmail is not NULL
AND A.[numDomainID] = @numDomainID
