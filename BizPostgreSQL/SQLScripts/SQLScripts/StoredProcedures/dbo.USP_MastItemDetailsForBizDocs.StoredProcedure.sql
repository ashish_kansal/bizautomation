/****** Object:  StoredProcedure [dbo].[USP_MastItemDetailsForBizDocs]    Script Date: 07/26/2008 16:20:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by Siva
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_mastitemdetailsforbizdocs')
DROP PROCEDURE usp_mastitemdetailsforbizdocs
GO
CREATE PROCEDURE [dbo].[USP_MastItemDetailsForBizDocs]        
@numDomainID as numeric(9)=0,        
@numListID as numeric(9)=0        
        
as        
        
        
select numListItemID,vcData from ListDetails         
where (numDomainID=@numDomainID or constFlag=1) and numListID=@numListID       
order by sintOrder
GO
