/****** Object:  StoredProcedure [dbo].[USP_GetUserFrmResouce]    Script Date: 07/26/2008 16:18:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getuserfrmresouce')
DROP PROCEDURE usp_getuserfrmresouce
GO
CREATE PROCEDURE [dbo].[USP_GetUserFrmResouce]
@numDomainId numeric(9)
as 
select resourceName,resourceId  from resource where numdomainid=@numDomainId
GO
