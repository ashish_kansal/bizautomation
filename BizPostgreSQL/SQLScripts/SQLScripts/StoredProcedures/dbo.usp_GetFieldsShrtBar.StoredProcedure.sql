/****** Object:  StoredProcedure [dbo].[usp_GetFieldsShrtBar]    Script Date: 07/26/2008 16:17:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getfieldsshrtbar')
DROP PROCEDURE usp_getfieldsshrtbar
GO
CREATE PROCEDURE [dbo].[usp_GetFieldsShrtBar]    
@numGroupID numeric,    
@numDomainId numeric,
@numTabId numeric     
AS 
BEGIN    
	IF(SELECT COUNT(*) FROM ShortCutGrpconf WHERE numTabId=@numTabId and numDomainId = @numDomainId and numGroupID =@numGroupID) > 0    
	BEGIN    
		SELECT 
			CONVERT(VARCHAR(10),Hdr.id) + '~0' AS id
			,Hdr.vclinkname
			,ISNULL(Sconf.bitInitialPage,0) bitInitialPage
			,sconf.numOrder 
		FROM 
			ShortCutGrpconf Sconf 
		JOIN 
			ShortCutBar Hdr 
		ON 
			Hdr.id =Sconf.numLinkId     
		WHERE 
			Hdr.numTabId=@numTabId 
			AND sconf.numTabId=Hdr.numTabId 
			AND sconf.numDomainId = @numDomainId 
			AND sconf.numGroupID =@numGroupID 
			AND Hdr.numcontactid=0 
			AND isnull(Sconf.tintLinkType,0)=0 
		UNION
		SELECT 
			CONVERT(VARCHAR(10),LD.numListItemID) + '~5' AS id
			,LD.vcData as vclinkname
			,ISNULL(Sconf.bitInitialPage,0) bitInitialPage
			,sconf.numOrder 
		FROM 
			ShortCutGrpconf Sconf 
		JOIN 
			ListDetails LD 
		ON 
			LD.numListItemID =Sconf.numLinkId     
		WHERE 
			LD.numListId = 5 
			AND LD.numListItemID <> 46 
			AND (LD.numDomainID=@numDomainID OR constflag=1) 
			AND sconf.numTabId=@numTabId 
			AND sconf.numDomainId = @numDomainId 
			AND sconf.numGroupID =@numGroupID 
			AND ISNULL(Sconf.tintLinkType,0)=5 
			AND @numTabId=7 
		ORDER BY 
			sconf.numOrder    
	END    
	ELSE    
	BEGIN     
		SELECT 
			CONVERT(VARCHAR(10),id) + '~0' AS id,vclinkname
			,ISNULL(bitInitialPage,0) AS bitInitialPage 
		FROM 
			ShortCutBar 
		WHERE 
			numTabId=@numTabId 
			AND bitdefault = 1
		UNION
		SELECT 
			CONVERT(VARCHAR(10),numListItemID) + '~5' AS id
			,vcData as vclinkname
			,0 as bitInitialPage 
		FROM 
			ListDetails 
		WHERE 
			numListId = 5 
			AND numListItemID <> 46       
			AND (numDomainID=@numDomainID or constflag=1) 
			AND @numTabId=7      
	END    

	IF(SELECT COUNT(*) FROM ShortCutGrpconf WHERE numTabId=@numTabId and numDomainId = @numDomainId and numGroupID =@numGroupID AND bitDefaultTab = 1) > 0
	BEGIN
		SELECT 1
	END
	ELSE
	BEGIN
		SELECT 0
	END

END
GO
