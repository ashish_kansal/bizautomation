/****** Object:  StoredProcedure [dbo].[USP_DeleteSolution]    Script Date: 07/26/2008 16:15:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_deletesolution')
DROP PROCEDURE usp_deletesolution
GO
CREATE PROCEDURE [dbo].[USP_DeleteSolution]
@numSolutionID as numeric(9)=null
AS
delete from CaseSolutions where [numSolnID] =@numSolutionID
delete from solutionmaster where numSolnID =@numSolutionID
GO
