/****** Object:  StoredProcedure [dbo].[usp_DeleteTemplate]    Script Date: 07/26/2008 16:15:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_deletetemplate')
DROP PROCEDURE usp_deletetemplate
GO
CREATE PROCEDURE [dbo].[usp_DeleteTemplate]
@numTemplateID numeric,
@bintDateDeleted numeric,
@numDeletedby numeric
--
as
update TemplateDocuments set bitStatus = 1, bintDateDeleted = @bintDateDeleted, numDateDeletedby = @numDeletedby where numTemplateID = @numTemplateID
GO
