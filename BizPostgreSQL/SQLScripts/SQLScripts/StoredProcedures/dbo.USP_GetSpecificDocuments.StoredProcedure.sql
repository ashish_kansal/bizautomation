/****** Object:  StoredProcedure [dbo].[USP_GetSpecificDocuments]    Script Date: 07/26/2008 16:18:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by  anoop jayaraj    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getspecificdocuments')
DROP PROCEDURE usp_getspecificdocuments
GO
CREATE PROCEDURE [dbo].[USP_GetSpecificDocuments]           
@numRecID as numeric(9)=0,        
@strType as varchar(10)='',          
@numCategory as numeric(9)=0,
@numDomainID as numeric(9)=0,
@ClientTimeZoneOffset as int
as          
         
if @numCategory=0           
begin          
 select
  numGenericDocID,
  VcFileName ,
  vcDocName ,
  dbo.fn_GetListItemName(numDocCategory) as Category ,
  vcfiletype,
  cUrlType,
  dbo.fn_GetListItemName(numDocStatus) as BusClass,
  dbo.fn_GetContactName(numModifiedBy)+ ',' + convert(VARCHAR(20),dateAdd(minute, -@ClientTimeZoneOffset,bintModifiedDate)) as Mod,
  (select count(*) from DocumentWorkflow             
 where numDocID=numGenericDocID and tintApprove=1) as Approved,
 (select count(*) from DocumentWorkflow             
 where numDocID=numGenericDocID and tintApprove=2) as Declined,
 (select count(*) from DocumentWorkflow             
 where numDocID=numGenericDocID and tintApprove=0) as Pending,
 dbo.fn_GetContactName(numLastCheckedOutBy)+', '+ convert(varchar(20),dateAdd(minute, -@ClientTimeZoneOffset,dtCheckOutDate )) as CheckedOut
  from dbo.GenericDocuments          
  where numRecID=@numRecID      
  and  vcDocumentSection=@strType   and numDomainID=@numDomainID      
end           
        
if @numCategory>0           
begin          
 select
  numGenericDocID,            
  VcFileName ,
  vcDocName ,
  dbo.fn_GetListItemName(numDocCategory) as Category ,
  vcfiletype,
  cUrlType,
  dbo.fn_GetListItemName(numDocStatus) as BusClass,
  dbo.fn_GetContactName(numModifiedBy)+ ',' + convert(VARCHAR(20),dateAdd(minute, -@ClientTimeZoneOffset,bintModifiedDate)) as Mod,
   (select count(*) from DocumentWorkflow             
 where numDocID=numGenericDocID and tintApprove=1) as Approved,
 (select count(*) from DocumentWorkflow             
 where numDocID=numGenericDocID and tintApprove=2) as Declined,
 (select count(*) from DocumentWorkflow             
 where numDocID=numGenericDocID and tintApprove=0) as Pending,
 dbo.fn_GetContactName(numLastCheckedOutBy)+', '+ convert(varchar(20),dateAdd(minute, -@ClientTimeZoneOffset,dtCheckOutDate )) as CheckedOut
  from GenericDocuments
  where numRecID=@numRecID and numDocCategory=@numCategory
  and  vcDocumentSection=@strType and numDomainID=@numDomainID
end
GO
