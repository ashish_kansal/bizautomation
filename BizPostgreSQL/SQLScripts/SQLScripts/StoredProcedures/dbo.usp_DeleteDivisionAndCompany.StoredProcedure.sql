/****** Object:  StoredProcedure [dbo].[usp_DeleteDivisionAndCompany]    Script Date: 07/26/2008 16:15:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created By: Debasish Tapan Nag                                                
--Purpose: Merge or Copy Contacts to another parent          
--Created Date: 01/16/2006         
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_deletedivisionandcompany')
DROP PROCEDURE usp_deletedivisionandcompany
GO
CREATE PROCEDURE    [dbo].[usp_DeleteDivisionAndCompany]
 @numDivisionId Numeric         
AS
DECLARE @numCompanyId Numeric

IF NOT EXISTS(SELECT * FROM  AdditionalContactsInformation WHERE numDivisionId = @numDivisionId)
BEGIN    
	SELECT @numCompanyId = numCompanyId FROM DivisionMaster WHERE  numDivisionId = @numDivisionId
	--DELETE FROM DivisionMaster WHERE numDivisionId = @numDivisionId      
	--PRINT 'Division ' + Convert(NVarchar(10), @numDivisionId) + ' Deleted'
	--IF NOT EXISTS(SELECT * FROM  DivisionMaster WHERE numCompanyId = @numCompanyId)    
	--BEGIN    
	--	DELETE FROM CompanyInfo WHERE numCompanyId = @numCompanyId
	--	PRINT 'Company ' + Convert(NVarchar(10), @numCompanyId) + ' Deleted'
	--END 
END
GO
