/****** Object:  StoredProcedure [dbo].[USP_GetOpportunityBizDocsIntegratedToAcnt]    Script Date: 07/26/2008 16:17:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created By Siva  
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getopportunitybizdocsintegratedtoacnt')
DROP PROCEDURE usp_getopportunitybizdocsintegratedtoacnt
GO
CREATE PROCEDURE [dbo].[USP_GetOpportunityBizDocsIntegratedToAcnt] 
(@numBizDocsPaymentDetId as numeric(9)=0)  
As  
Begin  
	Select bitIntegratedToAcnt From OpportunityBizDocsDetails Where numBizDocsPaymentDetId=@numBizDocsPaymentDetId  
End
GO
