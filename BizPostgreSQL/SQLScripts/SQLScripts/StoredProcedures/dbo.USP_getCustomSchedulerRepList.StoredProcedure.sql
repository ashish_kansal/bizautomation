/****** Object:  StoredProcedure [dbo].[USP_getCustomSchedulerRepList]    Script Date: 07/26/2008 16:17:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getcustomschedulerreplist')
DROP PROCEDURE usp_getcustomschedulerreplist
GO
CREATE PROCEDURE [dbo].[USP_getCustomSchedulerRepList]  
@numReportId as numeric(9),  
@numDomainId as numeric(9)  
as  
select   
numScheduleId,  
varScheduleName ,  
case when chrIntervalType = 'D'
		then 'Day ' 
	when  chrIntervalType = 'M'
		then 'Monthly '				
	when  chrIntervalType = 'Y'
		then 'Yearly '
end 
+' ,'+
convert(varchar(11),dtStartdate)
+' ,'+
case when bitEndTransactionType =1 
		then convert(varchar(20),numnoTransaction)
	when bitEndTransactionType =0
		then convert(varchar(11),dtEnddate)
end
as Interaval,  
  
vcReportName,vcReportDescription  
from CustRptScheduler CRS   
join CustomReport CR on CRS.numReportId = CR.numCustomReportId   
where CRS.numReportId = @numReportId and CRS.numDomainId = @numDomainId  
--select * from CustRptScheduler
GO
