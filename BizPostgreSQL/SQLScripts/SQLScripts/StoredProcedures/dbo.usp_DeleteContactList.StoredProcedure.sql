/****** Object:  StoredProcedure [dbo].[usp_DeleteContactList]    Script Date: 07/26/2008 16:15:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/*THIS PROCEDURE WE ARE NO LONGER USING .. CHINTAN 13.05.2010*/
--exec  usp_DeleteContactList 49                        
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_deletecontactlist')
DROP PROCEDURE usp_deletecontactlist
GO
CREATE PROCEDURE [dbo].[usp_DeleteContactList]                                
 @numDivisionID as numeric(9)=0,      
 @numContactID numeric(9),          
 @numDomainID as numeric(9)                                      
AS              
if not exists (select * from  usermaster where numuserdetailId=@numContactID and numDomainID=@numDomainID
	 and bitActivateFlag=1 )   
begin  
        
if exists (select * from AdditionalContactsInformation where numContactID=@numContactID and numDomainID=@numDomainID)             
begin          
                         
 delete RECENTITEMS where numRecordID =  @numContactID and chrRecordType='U'                  
                   
 declare @bitPrimaryContact AS bit                  
 declare @intCount as int                           
 declare @numComDivisionID  as numeric(9)                   
 select @numDivisionID=numDivisionID ,@bitPrimaryContact=ISNULL(bitPrimaryContact,0) from AdditionalContactsInformation where numContactID=@numContactID                    
 select @intCount=count(*) from AdditionalContactsInformation where numDivisionID=@numDivisionID                         
 select @numComDivisionID=numDivisionID from domain where numDomainID=@numDomainID                   
 if (@numComDivisionID=@numDivisionID and @bitPrimaryContact=1)                  
 select 0                  
 else if (@bitPrimaryContact=1 and @intCount>1) select 0                    
 else                    
 begin                    
                     
--  declare @numOppID as numeric(9)                        
--  set @numOppID=0                        
--    select Top 1 @numOppID=numOppID from  OpportunityMaster where numContactID= @numContactID order by numOppID                        
--    while @numOppID>0                        
--   begin                        
--    exec USP_DeleteOppurtunity @numOppID,@numDomainID                        
--    select Top 1 @numOppID=numOppID from  OpportunityMaster                         
--    where numContactID= @numContactID  and numOppID>@numOppID order by numOppID                        
--     if @@rowcount=0 set @numOppID=0                        
--                                        
--                                          
--    end                           
                        
--		delete ConECampaignDTL where numConECampID in (select numConEmailCampID from ConECampaign where numContactID=@numContactID)                      
--		delete ConECampaign where numContactID=@numContactID                      
--		delete AOIContactLink where numContactID=@numContactID                        
--		delete CaseContacts where numContactID=@numContactID                       
--		delete ProjectsContacts where numContactID=@numContactID                      
--		delete OpportunityContact where numContactID=@numContactID                      
--		delete UserTeams where numUserCntID=@numContactID                     
--		delete UserTerritory where numUserCntID=@numContactID                     
--                     
--		DELETE AdditionalContactsInformation WHERE numContactID=@numContactID                    
		
--		if  @intCount= 1 exec usp_DeleteAccounts  @numDivisionID,@numDomainID                               
		
  SELECT 1                       
 end             
end    
end  
else  
Select 2
GO
