/****** Object:  StoredProcedure [dbo].[USP_GetResourceId]    Script Date: 07/26/2008 16:18:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getresourceid')
DROP PROCEDURE usp_getresourceid
GO
CREATE PROCEDURE [dbo].[USP_GetResourceId]
@numUserCntId as numeric(9),
@numDomainId as numeric(9)
as 


select * from resource where numUserCntId = @numUserCntId and numDomainId =@numDomainId
GO
