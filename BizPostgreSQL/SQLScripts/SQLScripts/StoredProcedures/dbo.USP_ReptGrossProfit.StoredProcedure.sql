/****** Object:  StoredProcedure [dbo].[USP_ReptGrossProfit]    Script Date: 07/26/2008 16:20:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj    
    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_reptgrossprofit')
DROP PROCEDURE usp_reptgrossprofit
GO
CREATE PROCEDURE [dbo].[USP_ReptGrossProfit]    
@fromDate as datetime,    
@toDate as datetime,    
@numDomainId as numeric(9)=0,    
@numDepID as numeric(9)=0    
as    
    
select numOppID,vcPOppName,bintAccountClosingDate,dbo.GetDealAmount(numOppID,getutcdate(),0) as totAmount,numPClosingPercent as AmtPaid     
from OpportunityMaster where tintOppType=1 and tintOppStatus=1    
and (bintAccountClosingDate between @fromDate and @toDate) and numDomainID=@numDomainId    
    
select  numOppID,vcPOppName,bintAccountClosingDate,dbo.GetDealAmount(numOppID,getutcdate(),0)as totAmount,numPClosingPercent as AmtPaid     
from OpportunityMaster where tintOppType=2 and tintOppStatus=1    
and (bintAccountClosingDate between @fromDate and @toDate) and numDomainiD=@numDomainId    
    
    
    
if @numDepID=0    
begin   
  
select X.*,isnull(X.OvertimeHrs*X.monOverTimeRate,0) as OvertimeAmt from (   
 select numUserID,vcfirstName+ +vcLastName as [Name],isnull(U.monHourlyRate,0) as monHourlyRate,case when bitSalary=1 then 'Yes' when bitSalary=0 then 'No' end as Salary,    
 case when bitHourlyRate=1 then 'Yes' when bitHourlyRate=0 then 'No' end as Hourly,    
 isnull((select sum(convert(float,DATEDIFF(mi, dtFromDate, dtToDate))/60)      
  from TimeAndExpense where numCategory=1 and numType=1 and numUserID=U.numUserID and     
 (dtFromDate between @fromDate and @toDate)and  (dtToDate between @fromDate and @toDate)),0) as BillTime,      
  isnull((select sum(convert(float,DATEDIFF(mi, dtFromDate, dtToDate))/60)     
  from TimeAndExpense where numCategory=1 and numType=2 and numUserID=U.numUserID and     
 (dtFromDate between @fromDate and @toDate)and  (dtToDate between @fromDate and @toDate)),0) as NonBillTime,   
isnull((select sum(convert(float,DATEDIFF(mi, dtFromDate, dtToDate))*monAmount/60)      
  from TimeAndExpense where numCategory=1 and numType=1 and numUserID=U.numUserID and     
 (dtFromDate between @fromDate and @toDate)and  (dtToDate between @fromDate and @toDate)),0) as BillTimeAmt,      
  isnull((select sum(convert(float,DATEDIFF(mi, dtFromDate, dtToDate))/60)     
  from TimeAndExpense where numCategory=1 and numType=2 and numUserID=U.numUserID and     
 (dtFromDate between @fromDate and @toDate)and  (dtToDate between @fromDate and @toDate)),0)*isnull(U.monHourlyRate,0) as NonBillTimeAmt,       
  isnull((select sum(monAmount)      
  from TimeAndExpense where numCategory=2 and numType=1 and numUserID=U.numUserID and     
 (dtFromDate between @fromDate and @toDate)and  (dtToDate between @fromDate and @toDate)),0) as BillExp,      
  isnull((select sum(monAmount)      
  from TimeAndExpense where numCategory=2 and numType=2 and numUserID=U.numUserID and     
 (dtFromDate between @fromDate and @toDate)and  (dtToDate between @fromDate and @toDate)),0) as NonBillExp,    
 isnull((select sum(monAmount) from TimeExpAddAmount where dtAmtAdded between @fromDate and @toDate and numUserID=U.numUserId),0) as AddAmount,    
  dbo.GetOverTimeDtls(U.numUserId,@fromDate,@toDate) as OvertimeHrs,monOverTimeRate ,dbo.GetCommisionDtls(U.numUserId,@fromDate,@toDate) as CommAmt   
  from UserMaster U    
 join AdditionalContactsInformation A    
 on  numContactId=numUserDetailId and A.numDomainID=@numDomainID)X     
    
end    
else if @numDepID>0    
begin    
  
select X.*,isnull(X.OvertimeHrs*X.monOverTimeRate,0) as OvertimeAmt from (   
 select numUserID,vcfirstName+ +vcLastName as [Name],isnull(U.monHourlyRate,0) as monHourlyRate,case when bitSalary=1 then 'Yes' when bitSalary=0 then 'No' end as Salary,    
 case when bitHourlyRate=1 then 'Yes' when bitHourlyRate=0 then 'No' end as Hourly,    
 isnull((select sum(convert(float,DATEDIFF(mi, dtFromDate, dtToDate))/60)      
  from TimeAndExpense where numCategory=1 and numType=1 and numUserID=U.numUserID and     
 (dtFromDate between @fromDate and @toDate)and  (dtToDate between @fromDate and @toDate)),0) as BillTime,      
  isnull((select sum(convert(float,DATEDIFF(mi, dtFromDate, dtToDate))/60)     
  from TimeAndExpense where numCategory=1 and numType=2 and numUserID=U.numUserID and     
 (dtFromDate between @fromDate and @toDate)and  (dtToDate between @fromDate and @toDate)),0) as NonBillTime,   
isnull((select sum(convert(float,DATEDIFF(mi, dtFromDate, dtToDate))*monAmount/60)      
  from TimeAndExpense where numCategory=1 and numType=1 and numUserID=U.numUserID and     
 (dtFromDate between @fromDate and @toDate)and  (dtToDate between @fromDate and @toDate)),0) as BillTimeAmt,      
  isnull((select sum(convert(float,DATEDIFF(mi, dtFromDate, dtToDate))/60)     
  from TimeAndExpense where numCategory=1 and numType=2 and numUserID=U.numUserID and     
 (dtFromDate between @fromDate and @toDate)and  (dtToDate between @fromDate and @toDate)),0)*isnull(U.monHourlyRate,0) as NonBillTimeAmt,       
  isnull((select sum(monAmount)      
  from TimeAndExpense where numCategory=2 and numType=1 and numUserID=U.numUserID and     
 (dtFromDate between @fromDate and @toDate)and  (dtToDate between @fromDate and @toDate)),0) as BillExp,      
  isnull((select sum(monAmount)      
  from TimeAndExpense where numCategory=2 and numType=2 and numUserID=U.numUserID and     
 (dtFromDate between @fromDate and @toDate)and  (dtToDate between @fromDate and @toDate)),0) as NonBillExp,    
 isnull((select sum(monAmount) from TimeExpAddAmount where dtAmtAdded between @fromDate and @toDate and numUserID=U.numUserId),0) as AddAmount,    
  dbo.GetOverTimeDtls(U.numUserId,@fromDate,@toDate) as OvertimeHrs,monOverTimeRate ,dbo.GetCommisionDtls(U.numUserId,@fromDate,@toDate) as CommAmt   
  from UserMaster U    
 join AdditionalContactsInformation A    
 on  numContactId=numUserDetailId and A.numDomainID=@numDomainID and vcDepartment=@numDepID)X      
    
end
GO
