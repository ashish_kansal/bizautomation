/****** Object:  StoredProcedure [dbo].[USP_OPPDetails]    Script Date: 03/25/2009 15:10:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- Created By Anoop Jayaraj
-- [dbo].[USP_OPPDetails] 1362,1,-333
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_oppdetails')
DROP PROCEDURE usp_oppdetails
GO
CREATE PROCEDURE [dbo].[USP_OPPDetails]
(
               @numOppID             AS NUMERIC(9)  = NULL,
               @numDomainID          AS NUMERIC(9)  = 0,
               @ClientTimeZoneOffset AS INT)
AS
BEGIN	
  SELECT Opp.numoppid,
         numCampainID,
         ADC.vcFirstname + ' ' + ADC.vcLastName AS numContactId,
         isnull(ADC.vcEmail,'') AS vcEmail,
         isnull(ADC.numPhone,'') AS Phone,
         isnull(ADC.numPhoneExtension,'') AS PhoneExtension,
         Opp.txtComments,
         Opp.numDivisionID,
         tintOppType,
         ISNULL(dateadd(MINUTE,-@ClientTimeZoneOffset,bintAccountClosingDate),'') AS bintAccountClosingDate,
         Opp.numContactID AS ContactID,
         tintSource,
		 tintSourceType,
         C2.vcCompanyName + Case when isnull(D2.numCompanyDiff,0)>0 then  '  ' + dbo.fn_getlistitemname(D2.numCompanyDiff) + ':' + isnull(D2.vcCompanyDiff,'') else '' end as vcCompanyname,
		 (SELECT TOP 1 ISNULL(vcData,'') FROM ListDetails WHERE numListID=5 AND numListItemID=C2.numCompanyType AND numDomainId=@numDomainID) AS vcCompanyType,
         D2.tintCRMType,
         Opp.vcPoppName,
         intpEstimatedCloseDate,
         [dbo].[GetDealAmount](@numOppID,getutcdate(),0) AS monPAmount,
         lngPConclAnalysis,
         monPAmount AS OppAmount,
         numSalesOrPurType,
         Opp.tintActive,
		  dbo.fn_GetContactName(Opp.numCreatedby) AS CreatedBy,
         --dbo.fn_GetContactName(Opp.numCreatedby)
         --  + ' '
         --  + CONVERT(VARCHAR(20),dateadd(MINUTE,-@ClientTimeZoneOffset,Opp.bintCreatedDate)) AS CreatedBy,
         dbo.fn_GetContactName(Opp.numModifiedBy)
           + ' '
           + CONVERT(VARCHAR,dateadd(MINUTE,-@ClientTimeZoneOffset,Opp.bintModifiedDate),0) AS ModifiedBy,
         dbo.fn_GetContactName(Opp.numRecOwner) AS RecordOwner,
         Opp.numRecOwner,
         isnull(tintshipped,0) AS tintshipped,
         isnull(tintOppStatus,0) AS tintOppStatus,
         dbo.OpportunityLinkedItems(@numOppID) AS NoOfProjects,
         Opp.numAssignedTo,
         Opp.numAssignedBy,
         (SELECT COUNT(* )
          FROM   dbo.GenericDocuments
          WHERE  numRecID = @numOppID
                 AND vcDocumentSection = 'O') AS DocumentCount,
         isnull(OPR.numRecurringId,0) AS numRecurringId,
		 OPR.dtRecurringDate AS dtLastRecurringDate,
         D2.numTerID,
         Link.numOppID AS numParentOppID,
         Link.vcPoppName AS ParentOppName,
         Link.vcSource AS Source,
         Link.numParentProjectID,
         Link.vcProjectName,
         ISNULL(C.varCurrSymbol,'') varCurrSymbol,
         ISNULL(Opp.fltExchangeRate,0) AS [fltExchangeRate],
         ISNULL(C.chrCurrency,'') AS [chrCurrency],
         ISNULL(C.numCurrencyID,0) AS [numCurrencyID],
		 (Select Count(*) from OpportunityBizDocs 
		 where numOppId=@numOppID and bitAuthoritativeBizDocs=1) As AuthBizDocCount,
		 (SELECT COUNT(distinct numChildOppID) FROM [OpportunityLinking] OL WHERE OL.[numParentOppID] = Opp.numOppId) AS ChildCount,
		 (SELECT COUNT(*) FROM [RecurringTransactionReport] RTR WHERE RTR.[numRecTranSeedId] = Opp.numOppId)  AS RecurringChildCount
		 ,Opp.bintCreatedDate,
		 ISNULL(Opp.bitStockTransfer ,0) bitStockTransfer,	ISNULL(Opp.tintTaxOperator,0) AS tintTaxOperator, 
         isnull(Opp.intBillingDays,0) AS numBillingDays,isnull(Opp.bitInterestType,0) AS bitInterestType,
         isnull(opp.fltInterest,0) AS fltInterest,  isnull(Opp.fltDiscount,0) AS fltDiscount,
         isnull(Opp.bitDiscountType,0) AS bitDiscountType,ISNULL(Opp.bitBillingTerms,0) AS bitBillingTerms,ISNULL(OPP.numDiscountAcntType,0) AS numDiscountAcntType,
         ISNULL((SELECT TOP 1 numCountry FROM fn_getOPPAddressDetails(Opp.numOppId,Opp.numDomainID,2)),0) AS [numShipCountry],
		 ISNULL((SELECT TOP 1 numState FROM fn_getOPPAddressDetails(Opp.numOppId,Opp.numDomainID,2)),0) AS [numShipState],
		 ISNULL((SELECT TOP 1 vcState FROM fn_getOPPAddressDetails(Opp.numOppId,Opp.numDomainID,2)),'') AS [vcShipState],
		 ISNULL((SELECT TOP 1 vcCountry FROM fn_getOPPAddressDetails(Opp.numOppId,Opp.numDomainID,2)),'') AS [vcShipCountry],
		 ISNULL((SELECT TOP 1 vcPostalCode FROM fn_getOPPAddressDetails(Opp.numOppId,Opp.numDomainID,2)),'') AS [vcPostalCode],
		 ISNULL(vcCouponCode,'') AS [vcCouponCode],ISNULL(Opp.bitPPVariance,0) AS bitPPVariance,
		 Opp.dtItemReceivedDate,ISNULL(bitUseShippersAccountNo,0) [bitUseShippersAccountNo], ISNULL((SELECT DMSA.vcAccountNumber FROm [dbo].[DivisionMasterShippingAccount] DMSA WHERE DMSA.numDivisionID=D2.numDivisionID AND DMSA.numShipViaID=opp.intUsedShippingCompany),'') [vcShippersAccountNo],
		 ISNULL(bitUseMarkupShippingRate,0) [bitUseMarkupShippingRate],
		 ISNULL(numMarkupShippingRate,0) [numMarkupShippingRate],
		 ISNULL(Opp.intUsedShippingCompany,0) AS intUsedShippingCompany,
		 ISNULL(Opp.numShippingService,0) numOrderShippingService,
		 ISNULL(SR.[vcValue2],ISNULL(numShippingService,
		 (SELECT TOP 1 ISNULL([SST].[intNsoftEnum],0) FROM [dbo].[ShippingServiceTypes] AS SST 
		 WHERE [SST].[numDomainID] = @numDomainID 
		 AND [SST].[numRuleID] = 0
		 AND [SST].[vcServiceName] IN (SELECT ISNULL([OI].[vcItemDesc],'') FROM [dbo].[OpportunityItems] AS OI 
									 WHERE [OI].[numOppId] = [Opp].[numOppId] 
									 AND [OI].[vcType] = 'Service Item')))) AS numShippingService,
		 ISNULL(Opp.[monLandedCostTotal],0) AS monLandedCostTotal,opp.[vcLanedCost],
		 ISNULL(Opp.vcRecurrenceType,'') AS vcRecurrenceType,
		 ISNULL(RecurrenceConfiguration.numRecConfigID,0) AS numRecConfigID,
		 (CASE WHEN ISNULL(Opp.vcRecurrenceType,'') = '' THEN 0 ELSE 1 END) AS bitRecur,
		 (CASE WHEN RecurrenceTransaction.numRecTranID IS NULL THEN 0 ELSE 1 END) AS bitRecurred,
		 dbo.FormatedDateFromDate(RecurrenceConfiguration.dtStartDate,@numDomainID) AS dtStartDate,
		 dbo.FormatedDateFromDate(RecurrenceConfiguration.dtEndDate,@numDomainID) AS dtEndDate,
		 RecurrenceConfiguration.vcFrequency AS vcFrequency,
		 ISNULL(RecurrenceConfiguration.bitDisabled,0) AS bitDisabled,
		 ISNULL(Opp.dtReleaseDate,'') AS dtReleaseDate,
		 ISNULL(Opp.dtExpectedDate,'') AS dtExpectedDate,
		 ISNULL(Opp.numAccountClass,0) AS numAccountClass,D3.vcPartnerCode+'-'+C3.vcCompanyName as numPartenerSource,
		ISNULL(Opp.numPartner,0) AS numPartenerSourceId,
		ISNULL(Opp.numPartenerContact,0) AS numPartenerContactId,
		A.vcFirstName+' '+A.vcLastName AS numPartenerContact,
		numShipFromWarehouse, CPN.CustomerPartNo,
		Opp.numBusinessProcessID,
		SLP.Slp_Name AS vcProcessName,
		(CASE WHEN ISNULL((SELECT COUNT(*) FROM OpportunityBizDocs WHERE OpportunityBizDocs.numOppID=Opp.numOppId AND numBizDocId IN (287,296,644)),0) > 0 THEN 1 ELSE 0 END) bitAuthorativeBizDocAdded
  FROM   OpportunityMaster Opp 
         JOIN divisionMaster D2
           ON Opp.numDivisionID = D2.numDivisionID
		   LEFT JOIN divisionMaster D3 ON Opp.numPartner = D3.numDivisionID
		LEFT JOIN CompanyInfo C3 ON C3.numCompanyID = D3.numCompanyID
		LEFT JOIN AdditionalContactsInformation A ON Opp.numPartenerContact = A.numContactId
		LEFT JOIN Sales_process_List_Master AS SLP ON Opp.numBusinessProcessID=SLP.Slp_Id
         LEFT JOIN CompanyInfo C2
           ON C2.numCompanyID = D2.numCompanyID
         LEFT JOIN (SELECT TOP 1 OM.vcPoppName,
                                 OM.numOppID,
                                 numChildOppID,
                                 vcSource,
                                 numParentProjectID,
                                 vcProjectName
                    FROM   OpportunityLinking
                          LEFT JOIN OpportunityMaster OM
                             ON numOppID = numParentOppID
                          LEFT JOIN [ProjectsMaster]
							 ON numParentProjectID = numProId   
                    WHERE  numChildOppID = @numOppID) Link
           ON Link.numChildOppID = Opp.numOppID
         LEFT OUTER JOIN [Currency] C
					ON C.numCurrencyID = Opp.numCurrencyID
		 LEFT OUTER JOIN [OpportunityRecurring] OPR
					ON OPR.numOppId = Opp.numOppId
		 LEFT OUTER JOIN additionalContactsinformation ADC ON
		 ADC.numdivisionId = D2.numdivisionId AND  ADC.numContactId = Opp.numContactId	
		 LEFT JOIN RecurrenceConfiguration ON Opp.numOppId = RecurrenceConfiguration.numOppID AND RecurrenceConfiguration.numType = 1
		 LEFT JOIN RecurrenceTransaction ON Opp.numOppId = RecurrenceTransaction.numRecurrOppID
		 LEFT JOIN [dbo].[ShippingReport] AS SR ON SR.[numOppID] = [Opp].[numOppId] AND SR.[numDomainID] = [Opp].[numDomainId]
		LEFT JOIN [dbo].CustomerPartNumber CPN ON C3.numCompanyID = CPN.numCompanyID
		WHERE  Opp.numOppId = @numOppID
         AND Opp.numDomainID = @numDomainID
END
GO