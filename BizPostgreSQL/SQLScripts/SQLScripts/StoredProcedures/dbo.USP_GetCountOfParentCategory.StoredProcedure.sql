/****** Object:  StoredProcedure [dbo].[USP_GetCountOfParentCategory]    Script Date: 07/26/2008 16:17:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created by Siva          
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getcountofparentcategory')
DROP PROCEDURE usp_getcountofparentcategory
GO
CREATE PROCEDURE [dbo].[USP_GetCountOfParentCategory]              
@numParntAcntId numeric(9)=0,          
@numDomainId numeric(9)=0,      
@intCount int=0 output,    -- Obsolet, as there will be no child account, any account can be added to leaf node of account category.
@intJournalEntryCount int=0 output      
as               
Begin              
  Declare @numJournalId as numeric(9)    
  Declare @numAcntTypeId as numeric(9)     
--  Select @intCount=Count([numParntAcntTypeId]) From Chart_Of_Accounts Where nump=@numParntAcntId And numDomainId=@numDomainId         
--  Select @numAcntTypeId=isnull([numAcntTypeId],0) From Chart_Of_Accounts Where numAccountId=@numParntAcntId And numDomainId=@numDomainId         
--  Select @numJournalId=numJournal_Id From General_Journal_Header where numChartAcntId=@numParntAcntId  And numDomainId=@numDomainId
	--as OPening balance of account is entered as journal entry we need to discard it while checking for deletion
	SELECT TOP 1 @numJournalId = numJournal_Id FROM dbo.General_Journal_Header WHERE numChartAcntId=@numParntAcntId
  
   Select @intJournalEntryCount=Count(numChartAcntId) From General_Journal_Details     
   Where numChartAcntId=@numParntAcntId And numDomainId=@numDomainId    AND numJournalId<>@numJournalId
End
GO
