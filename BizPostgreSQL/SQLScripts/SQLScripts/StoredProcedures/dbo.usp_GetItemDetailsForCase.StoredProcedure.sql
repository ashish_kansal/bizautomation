/****** Object:  StoredProcedure [dbo].[usp_GetItemDetailsForCase]    Script Date: 07/26/2008 16:17:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getitemdetailsforcase')
DROP PROCEDURE usp_getitemdetailsforcase
GO
CREATE PROCEDURE [dbo].[usp_GetItemDetailsForCase]
	@numOppID numeric(9)=0,
	@numDomainID numeric(9)=0   
--
AS
	SELECT     OpportunityItems.numUnitHour,OpportunityItems.monTotAmount, Item.vcItemName,Item.numItemCode, OpportunityItems.monPrice, Item.charItemType, Item.txtItemDesc, 
                      OpportunityMaster.vcPOppName, CompanyInfo.vcCompanyName
   FROM Item INNER JOIN OpportunityItems ON Item.numItemCode = OpportunityItems.numItemCode INNER JOIN
                      OpportunityMaster ON OpportunityItems.numOppId = OpportunityMaster.numOppId INNER JOIN
                      DivisionMaster ON OpportunityMaster.numDivisionId = DivisionMaster.numDivisionID INNER JOIN
                      CompanyInfo ON DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
   WHERE
		OpportunityMaster.numOppID=@numOppID
		AND Item.numDomainID=@numDomainID
GO
