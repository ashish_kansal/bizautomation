/****** Object:  StoredProcedure [dbo].[USP_ManagePriceBookRulesDTL]    Script Date: 07/26/2008 16:19:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj 
-- select * FROM [PriceBookRules] WHERE [numPricRuleID]=370
--select * FROM [PriceBookRules] LEFT ou WHERE [tintStep2]=2 AND [tintStep3]=3 AND [numPricRuleID]<>370 
-- exec USP_ManagePriceBookRulesDTL @numPricRuleID=368,@tintAppRuleType=3,@numValue=7052,@numProfile=0,@numPriceRuleDTLID=0,@byteMode=0
-- exec USP_ManagePriceBookRulesDTL @numPricRuleID=370,@tintAppRuleType=2,@numValue=6426,@numProfile=0,@numPriceRuleDTLID=0,@byteMode=0
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_managepricebookrulesdtl')
DROP PROCEDURE usp_managepricebookrulesdtl
GO
CREATE PROCEDURE [dbo].[USP_ManagePriceBookRulesDTL]    
@numPricRuleID as numeric(9)=0,    
@tintAppRuleType as tinyint,  
@numValue as numeric(9)=0,  
@numProfile as numeric(9)=0,  
@numPriceRuleDTLID as numeric(9)=0,  
@byteMode as TINYINT
as    


declare @tintRuleFor as tinyint

select @tintRuleFor=tintRuleFor from PriceBookRules where numPricRuleID=@numPricRuleID

  if @byteMode=0  
begin  
	
    
    if @numValue<>0
    begin  
    
    DECLARE @numDomainID as numeric(9)
    DECLARE @Step2 as TINYINT
    DECLARE @Step3 as TINYINT
    
   if not exists(select * from PriceBookRuleDTL where numRuleID=@numPricRuleID and numValue=@numValue and numProfile=@numProfile and tintType=@tintAppRuleType)
	begin
	IF @tintAppRuleType >2
		BEGIN
			IF @tintAppRuleType=3 
			BEGIN
				delete from PriceBookRuleDTL where tintType = 4 and numRuleID=@numPricRuleID
				UPDATE [PriceBookRules] SET [tintStep3] = 1 WHERE [numPricRuleID]=@numPricRuleID
			END
			ELSE IF @tintAppRuleType=4 
			BEGIN
				delete from PriceBookRuleDTL where tintType = 3 and numRuleID=@numPricRuleID
				UPDATE [PriceBookRules] SET [tintStep3] = 2 WHERE [numPricRuleID]=@numPricRuleID
			END
			
			/*Duplicate Rule values checking */
			SELECT @numDomainID=numDomainID,@Step2=tintStep2,@Step3=tintStep3 FROM [PriceBookRules] WHERE numPricRuleID = @numPricRuleID
			IF ( SELECT COUNT(*)
				 FROM   [PriceBookRules] P
						LEFT OUTER JOIN [PriceBookRuleDTL] PDTL ON P.numPricRuleID = PDTL.numRuleID
						LEFT OUTER JOIN [PriceBookRuleItems] PRI ON PRI.numRuleID = PDTL.numRuleID
				 WHERE  P.numPricRuleID <> @numPricRuleID and P.tintRuleFor=@tintRuleFor
						AND P.[numDomainID] = @numDomainID
						AND ( ( P.tintStep2 = @Step2
								AND PDTL.numValue = @numValue
								AND P.tintStep3 = @Step3
								AND PRI.numValue IN (SELECT  numValue FROM    [PriceBookRuleItems] WHERE   numRuleID = @numPricRuleID )
								AND ISNULL(PDTL.numProfile,0) = @numProfile
							  )
							  OR ( P.tintStep3 = @Step3
								   AND PRI.numValue = @numValue
								   AND P.tintStep2 = @Step2
								   AND PDTL.numValue IN (SELECT   numValue FROM     [PriceBookRuleDTL] WHERE numRuleID = @numPricRuleID)
								   AND ISNULL(PDTL.numProfile,0) = @numProfile
								 )
							)
			   ) > 0 
				BEGIN
					RAISERROR ( 'DUPLICATE',16, 1 )
					RETURN ;
				END
			
			
					insert into PriceBookRuleDTL (numRuleID,numValue,numProfile,tintType)  
					values(@numPricRuleID,@numValue,@numProfile,@tintAppRuleType)
		END
		ELSE 
		BEGIN
			
			IF @tintAppRuleType=1 
			BEGIN
				delete from PriceBookRuleItems where tintType = 2 and numRuleID=@numPricRuleID
				UPDATE [PriceBookRules] SET [tintStep2] = 1 WHERE [numPricRuleID]=@numPricRuleID
			END
			ELSE IF @tintAppRuleType=2 
			BEGIN
				delete from PriceBookRuleItems where tintType = 1 and numRuleID=@numPricRuleID
				UPDATE [PriceBookRules] SET [tintStep2] = 2 WHERE [numPricRuleID]=@numPricRuleID
			END
			
			
			/*Duplicate Rule values checking */
			SELECT @numDomainID=numDomainID,@Step2=tintStep2,@Step3=tintStep3 FROM [PriceBookRules] WHERE numPricRuleID = @numPricRuleID
			IF ( SELECT COUNT(*)
				 FROM   [PriceBookRules] P
						LEFT OUTER JOIN [PriceBookRuleDTL] PDTL ON P.numPricRuleID = PDTL.numRuleID
						LEFT OUTER JOIN [PriceBookRuleItems] PRI ON PRI.numRuleID = PDTL.numRuleID
				 WHERE  P.numPricRuleID <> @numPricRuleID and P.tintRuleFor=@tintRuleFor
						AND P.[numDomainID] = @numDomainID 
						AND ( ( P.tintStep2 = @Step2
								AND PDTL.numValue = @numValue
								AND P.tintStep3 = @Step3
								AND PRI.numValue IN (SELECT  numValue FROM    [PriceBookRuleItems] WHERE   numRuleID = @numPricRuleID )
								AND ISNULL(PDTL.numProfile,0) = @numProfile
							  )
							  OR ( P.tintStep3 = @Step3
								   AND PRI.numValue = @numValue
								   AND P.tintStep2 = @Step2
								   AND PDTL.numValue IN (SELECT   numValue FROM     [PriceBookRuleDTL] WHERE numRuleID = @numPricRuleID)
								   AND ISNULL(PDTL.numProfile,0) = @numProfile
								 )
							)
			   ) > 0 
				BEGIN
					RAISERROR ( 'DUPLICATE',16, 1 )
					RETURN ;
				END
			
			insert into [PriceBookRuleItems]  (numRuleID,numValue,tintType)
					values(@numPricRuleID,@numValue,@tintAppRuleType)
		END
			
		
	end
	  
 end   
  
end  
else if @byteMode=1  
begin  
	IF (@tintAppRuleType=1 OR @tintAppRuleType=2)
		DELETE FROM [PriceBookRuleItems] WHERE numPriceBookItemID = @numPriceRuleDTLID
	ELSE 
		delete from PriceBookRuleDTL where numPriceBookRuleDTLID= @numPriceRuleDTLID 
end
GO
