/****** Object:  StoredProcedure [dbo].[USP_ManageDynamicAOI]    Script Date: 07/26/2008 16:19:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_managedynamicaoi')
DROP PROCEDURE usp_managedynamicaoi
GO
CREATE PROCEDURE [dbo].[USP_ManageDynamicAOI] 
@strAOI as text='',
@numDomainID as numeric(9)=0,
@numFormID as numeric(9)=0,
@numGroupID as numeric(9)=0
as


delete from DynamicFormAOIConf where numDomainID=@numDomainID and numFormID= @numFormId
and numGroupID= @numGroupID  

DECLARE @hDoc1 int                                
 EXEC sp_xml_preparedocument @hDoc1 OUTPUT, @strAOI                                
                                
 insert into DynamicFormAOIConf                                
  (numAOIID,numFormID,numGroupID,numDomainID)                                
                                 
 select X.*,@numFormID,@numGroupID,@numDomainID from(SELECT *FROM OPENXML (@hDoc1,'/NewDataSet/Table',2)                                
 WITH (numAOIID numeric(9)))X                               
                                
                                
 EXEC sp_xml_removedocument @hDoc1
GO
