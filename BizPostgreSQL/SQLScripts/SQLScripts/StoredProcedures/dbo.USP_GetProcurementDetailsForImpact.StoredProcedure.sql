/****** Object:  StoredProcedure [dbo].[USP_GetProcurementDetailsForImpact]    Script Date: 07/26/2008 16:18:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created By Siva              
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getprocurementdetailsforimpact')
DROP PROCEDURE usp_getprocurementdetailsforimpact
GO
CREATE PROCEDURE [dbo].[USP_GetProcurementDetailsForImpact]            
(@numDomainId as numeric(9)=0,      
@intFiscalYear as int=0)            
As            
Begin            
 Select distinct convert(varchar(10),IG.numItemGroupID)+'~'+convert(varchar(10),PBM.numProcurementBudgetId) as  numProcurementDetId, IG.vcItemGroup as vcItemGroup From ProcurementBudgetMaster PBM          
inner join ProcurementBudgetDetails PBD on PBM.numProcurementBudgetId=PBD.numProcurementId       
inner join ItemGroups IG on PBD.numItemGroupId=IG.numItemGroupID      
 Where PBM.numDomainId=@numDomainId     -- PBM.intFiscalYear =@intFiscalYear And      
End
GO
