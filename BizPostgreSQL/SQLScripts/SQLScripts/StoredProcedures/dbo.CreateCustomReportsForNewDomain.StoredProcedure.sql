/****** Object:  StoredProcedure [dbo].[CreateCustomReportsForNewDomain]    Script Date: 07/26/2008 16:14:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='createcustomreportsfornewdomain')
DROP PROCEDURE createcustomreportsfornewdomain
GO
CREATE PROCEDURE [dbo].[CreateCustomReportsForNewDomain]
@numdomainId as numeric,
@numContactId as numeric
as
begin

Create table #tempTable ( ID INT IDENTITY PRIMARY KEY,                                                     
      numReportId numeric(18)                                                    
 ) 
insert into #tempTable (numReportId) select (numCustomReportId) from customreport where numdomainId =1 and isnull(bitDefault,0)=1
--select count(*) from #tempTable
Declare @minId as numeric
Declare @maxId as numeric
select @minId=min(numReportId),@maxId=max(numReportId) from #tempTable


	

while @minId<>0
begin




declare @customrptid numeric(18)
insert into customreport (
vcReportName,
vcReportDescription,
textQuery,
numCreatedBy,
numDomainID,
numModifiedBy,
numModuleId,
numGroupId,
bitFilterRelAnd,
varstdFieldId,
custFilter,
FromDate,
ToDate,
bintRows,
bitGridType,
varGrpfld,
varGrpOrd,
varGrpflt,
textQueryGrp,
FilterType,
AdvFilterStr,
OrderbyFld,
Orderf,
MttId,
MttValue,
CompFilter1,
CompFilter2,
CompFilterOpr,
bitDrillDown,
bitSortRecCount,bitReportDisable)

select 
vcReportName,
vcReportDescription,
textQuery,
@numContactId,
@numdomainId,
@numContactId,
numModuleId,
numGroupId,
bitFilterRelAnd,
varstdFieldId,
custFilter,
FromDate,
ToDate,
bintRows,
bitGridType,
varGrpfld,
varGrpOrd,
varGrpflt,
textQueryGrp,
FilterType,
AdvFilterStr,
OrderbyFld,
Orderf,
MttId,
MttValue,
CompFilter1,
CompFilter2,
CompFilterOpr,bitDrillDown,
bitSortRecCount,bitReportDisable from  customreport x where numdomainId =1   and numCustomReportId = @minId
 SELECT  
   @customrptid = SCOPE_IDENTITY(); 


insert into CustReportFields
(numCustomReportId,
vcFieldName,
vcValue)
select @customrptid,
vcFieldName,
vcValue from CustReportFields 
where numCustomReportId = @minId

insert into CustReportFilterlist
(numCustomReportId,
vcFieldsText,
vcFieldsValue,
vcFieldsOperator,
vcFilterValue,
vcFilterValued)
select @customrptid,
vcFieldsText,
vcFieldsValue,
vcFieldsOperator,
vcFilterValue,
vcFilterValued from  CustReportFilterlist 
where numCustomReportId = @minId

insert into CustReportOrderlist( numCustomReportId,
vcFieldText,
vcValue,
numOrder)
select @customrptid,
vcFieldText,
vcValue,
numOrder from  CustReportOrderlist 
where numCustomReportId = @minId

insert into CustReportSummationlist (
numCustomReportId,
vcFieldName,
bitSum,
bitAvg,
bitMin,
bitMax)

select @customrptid,
vcFieldName,
bitSum,
bitAvg,
bitMin,
bitMax from CustReportSummationlist 
where numCustomReportId = @minId



	select top 1 @minId=min(numReportId) from #tempTable where numreportId > @minId group by numReportId order by numReportId
	if @@rowcount=0 set @minId =0
end 

drop table #tempTable 

end
GO
