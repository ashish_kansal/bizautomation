/****** Object:  StoredProcedure [dbo].[USP_GetOppSerializedItems]    Script Date: 07/26/2008 16:18:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getoppserializeditems')
DROP PROCEDURE usp_getoppserializeditems
GO
CREATE PROCEDURE [dbo].[USP_GetOppSerializedItems]      
@numOppID as numeric(9)=0      
as      
      
select numOppItemTCode,OpportunityItems.vcItemName+' -- ' + vcWareHouse as vcItemName  from OpportunityItems       
join Item      
on Item.numItemCode=OpportunityItems.numItemCode
join WareHouseItems WI
on WI.numWareHouseItemID=OpportunityItems.numWarehouseItmsID
join  Warehouses W
on W.numWareHouseID=WI.numWareHouseID 
where numOppID=@numOppID and (bitSerialized=1 OR bitLotNo=1)
GO
