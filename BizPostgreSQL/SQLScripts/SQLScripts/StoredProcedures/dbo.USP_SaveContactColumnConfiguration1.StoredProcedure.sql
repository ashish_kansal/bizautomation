/****** Object:  StoredProcedure [dbo].[USP_SaveContactColumnConfiguration1]    Script Date: 07/26/2008 16:20:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_savecontactcolumnconfiguration1')
DROP PROCEDURE usp_savecontactcolumnconfiguration1
GO
CREATE PROCEDURE [dbo].[USP_SaveContactColumnConfiguration1]    
	@numDomainID Numeric(9),        
	@numUserCntId Numeric(9),      
	@FormId tinyint ,    
	@numtype numeric(9),    
	@strXml   as text ='',
	@numViewID numeric(9)=0         
AS     
BEGIN    
	IF @FormId = 96 OR  @FormId = 97
	BEGIN  
		DELETE FROM 
			DycFormConfigurationDetails 
		WHERE 
			numDomainID=@numDomainID 
			AND [numFormId]= @FormId
			AND tintPageType=1 
			AND ISNULL(numRelCntType,0)=@numtype 
			AND ISNULL(numViewID,0)=@numViewID   
	END
	ELSE
	BEGIN
		DELETE FROM 
			DycFormConfigurationDetails 
		WHERE 
			numDomainID=@numDomainID 
			AND [numFormId]= @FormId      
			AND [numUserCntId]= @numUserCntId 
			AND tintPageType=1 
			AND ISNULL(numRelCntType,0)=@numtype 
			AND ISNULL(numViewID,0)=@numViewID
	END
    
	DECLARE @hDoc1 int                                      
	EXEC sp_xml_preparedocument @hDoc1 OUTPUT, @strXml                                      

	INSERT INTO DycFormConfigurationDetails
	(
		numFormId
		,numFieldId
		,intRowNum
		,intColumnNum
		,numUserCntId
		,numDomainId
		,bitCustom
		,tintPageType
		,numRelCntType
		,numViewID
	)    
	SELECT 
		@FormId
		,X.numFieldID
		,X.tintOrder
		,1
		,CASE WHEN @FormId = 96 OR  @FormId = 97 THEN 0 ELSE @numUserCntId END
		,@numDomainID
		,X.bitCustom,1
		,@numtype
		,@numViewID
	FROM 
	(
		SELECT 
			*
		FROM 
			OPENXML (@hDoc1,'/NewDataSet/Table',2)                                      
		WITH 
		(
			numFieldID numeric(9),      
			tintOrder tinyint,
			bitCustom bit 
		)
	)X                                     
                                      
                                      
 EXEC sp_xml_removedocument @hDoc1
END
GO
