/****** Object:  StoredProcedure [dbo].[Usp_saveCustAllowedDashBoardReports]    Script Date: 07/26/2008 16:20:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_savecustalloweddashboardreports')
DROP PROCEDURE usp_savecustalloweddashboardreports
GO
CREATE PROCEDURE [dbo].[Usp_saveCustAllowedDashBoardReports]
@strReportIds as varchar(2000),
@GroupId as numeric(9)
as
delete  DashboardAllowedReports where  numGrpId= @GroupId
if @strReportIds<>''
begin 
insert into DashboardAllowedReports (numReportId,
numGrpId

)
select x.items,@GroupId from (select * from dbo.split(@strReportIds,','))x
end
GO
