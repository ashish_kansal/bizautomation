/****** Object:  StoredProcedure [dbo].[usp_GetListValues]    Script Date: 07/26/2008 16:17:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getlistvalues')
DROP PROCEDURE usp_getlistvalues
GO
CREATE PROCEDURE [dbo].[usp_GetListValues]
	@numDomainID numeric,
	@numMainListID numeric  
--
AS
--This SP will return the List Details for a particular List Item.
--This List Item is passed via parameter @numListID
BEGIN
	IF @numMainListID=1 
	--Tertritory
	BEGIN
		SELECT numTerID As numID, vcTerName As vcName
			FROM TerritoryMaster
			WHERE numDomainID=@numDomainID
			ORDER BY vcTerName
	END

	IF @numMainListID=3 
	--Annual Revenue. List Item ID = 6
	BEGIN
		SELECT numListItemID As numID, vcData As vcName 
			FROM ListDetails
			WHERE numListID=6
			AND numDomainID=@numDomainID
	END

	IF @numMainListID=4 
	--No Of Employees. List Item ID = 7
	BEGIN
		SELECT numListItemID As numID, vcData As vcName 
			FROM ListDetails
			WHERE numListID=7
			AND numDomainID=@numDomainID
			
	END
END
GO
