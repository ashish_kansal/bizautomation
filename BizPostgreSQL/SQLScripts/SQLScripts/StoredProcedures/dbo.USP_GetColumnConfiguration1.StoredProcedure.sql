/****** Object:  StoredProcedure [dbo].[USP_GetColumnConfiguration1]    Script Date: 07/26/2008 16:16:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'usp_getcolumnconfiguration1' )
    DROP PROCEDURE usp_getcolumnconfiguration1
GO
CREATE PROCEDURE [dbo].[USP_GetColumnConfiguration1]
    @numDomainID NUMERIC(9) ,
    @numUserCntId NUMERIC(9) ,
    @FormId TINYINT ,
    @numtype NUMERIC(9) ,
    @numViewID NUMERIC(9) = 0
AS
    DECLARE @PageId AS TINYINT         
    IF @FormId = 10 OR @FormId = 44
        SET @PageId = 4          
    ELSE IF @FormId = 11 OR @FormId = 34 OR @FormId = 35 OR @FormId = 36 OR @FormId = 96 OR @FormId = 97
            SET @PageId = 1          
    ELSE IF @FormId = 12
                SET @PageId = 3          
    ELSE IF @FormId = 13
                    SET @PageId = 11       
    ELSE IF (@FormId = 14 AND (@numtype = 1 OR @numtype = 3)) OR @FormId = 33 OR @FormId = 38 OR @FormId = 39 OR @FormId = 23
                        SET @PageId = 2          
    ELSE IF @FormId = 14 AND (@numtype = 2 OR @numtype = 4) OR @FormId = 40 OR @FormId = 41
                            SET @PageId = 6  
    ELSE IF @FormId = 21 OR @FormId = 26 OR @FormId = 32 OR @FormId = 126 OR @FormId = 129
                                SET @PageId = 5   
    ELSE IF @FormId = 22 OR @FormId = 30
                                    SET @PageId = 5    
    ELSE IF @FormId = 76
                                        SET @PageId = 10 
    ELSE IF @FormId = 84 OR @FormId = 85
                                            SET @PageId = 5

              
    IF @PageId = 1 OR @PageId = 4
    BEGIN
        SELECT  CONVERT(VARCHAR(9), numFieldID) + '~0' AS numFieldID ,
                ISNULL(vcCultureFieldName, vcFieldName) AS vcFieldName ,
                0 AS Custom
        FROM    View_DynamicDefaultColumns
        WHERE   numFormID = @FormId
                AND ISNULL(bitSettingField, 0) = 1
                AND ISNULL(bitDeleted, 0) = 0
                AND numDomainID = @numDomainID
        UNION
        SELECT  CONVERT(VARCHAR(9), fld_id) + '~1' AS numFieldID ,
                fld_label AS vcFieldName ,
                1 AS Custom
        FROM    CFW_Fld_Master
                LEFT JOIN CFW_Fld_Dtl ON Fld_id = numFieldId
                                            AND numRelation = CASE
                                                            WHEN @numtype IN (
                                                            1, 2, 3 )
                                                            THEN numRelation
                                                            ELSE @numtype
                                                            END
                LEFT JOIN CFw_Grp_Master ON subgrp = CFw_Grp_Master.Grp_id
        WHERE   CFW_Fld_Master.grp_id = @PageId
                AND CFW_Fld_Master.numDomainID = @numDomainID
                AND Fld_type <> 'Link'
        ORDER BY Custom ,
                vcFieldName                                       
    END  
    ELSE IF @PageId = 10
    BEGIN
        SELECT  CONVERT(VARCHAR(9), numFieldID) + '~0' AS numFieldID ,
                ISNULL(vcCultureFieldName, vcFieldName) AS vcFieldName ,
                0 AS Custom
        FROM    View_DynamicDefaultColumns
        WHERE   numFormID = @FormId
                AND ISNULL(bitSettingField, 0) = 1
                AND ISNULL(bitDeleted, 0) = 0
                AND numDomainID = @numDomainID
        UNION
        SELECT  CONVERT(VARCHAR(9), fld_id) + '~1' AS numFieldID ,
                fld_label AS vcFieldName ,
                1 AS Custom
        FROM    CFW_Fld_Master
                LEFT JOIN CFW_Fld_Dtl ON Fld_id = numFieldId
                                            AND numRelation = CASE
                                                        WHEN @numtype IN (
                                                        1, 2, 3 )
                                                        THEN numRelation
                                                        ELSE @numtype
                                                        END
                LEFT JOIN CFw_Grp_Master ON subgrp = CFw_Grp_Master.Grp_id
        WHERE   CFW_Fld_Master.grp_id = 5
                AND CFW_Fld_Master.numDomainID = @numDomainID
                AND Fld_Type = 'SelectBox'
        ORDER BY Custom ,
                vcFieldName       
    END 
	ELSE IF @FormId=43
	BEGIN
		PRINT 'ABC'
		SELECT  CONVERT(VARCHAR(9), numFieldID) + '~0' AS numFieldID ,
                    ISNULL(vcCultureFieldName, vcFieldName) AS vcFieldName ,
                    0 AS Custom ,
                    vcDbColumnName
            FROM    View_DynamicDefaultColumns
            WHERE   numFormID = @FormId
                    AND ISNULL(bitSettingField, 0) = 1
                    AND ISNULL(bitDeleted, 0) = 0
                    AND numDomainID = @numDomainID
            UNION
            SELECT  CONVERT(VARCHAR(9), fld_id) + '~1' AS numFieldID ,
                    fld_label AS vcFieldName ,
                    1 AS Custom ,
                    CONVERT(VARCHAR(10), fld_id) AS vcDbColumnName
            FROM    CFW_Fld_Master
                    LEFT JOIN CFw_Grp_Master ON subgrp = CFw_Grp_Master.Grp_id
            WHERE   CFW_Fld_Master.grp_id = 1
                    AND CFW_Fld_Master.numDomainID = @numDomainID
                    AND Fld_type <> 'Link'
            ORDER BY Custom ,
                    vcFieldName          
	END
	ELSE IF @FormId = 125 AND @PageId=0
	BEGIN
			SELECT  CONVERT(VARCHAR(9), numFieldID) + '~0' AS numFieldID ,
                    ISNULL(vcCultureFieldName, vcFieldName) AS vcFieldName ,
                    0 AS Custom
            FROM    View_DynamicDefaultColumns
            WHERE   numFormID = @FormId
                    AND ISNULL(bitSettingField, 0) = 1
                    AND ISNULL(bitDeleted, 0) = 0
                    AND numDomainID = @numDomainID
	END
    ELSE IF @PageId = 5 AND (@FormId = 84 OR @FormId = 85)
    BEGIN
        SELECT  CONVERT(VARCHAR(9), numFieldID) + '~0' AS numFieldID ,
                ISNULL(vcCultureFieldName, vcFieldName) AS vcFieldName ,
                0 AS Custom ,
                vcDbColumnName
        FROM    View_DynamicDefaultColumns
        WHERE   numFormID = @FormId
                AND ISNULL(bitSettingField, 0) = 1
                AND ISNULL(bitDeleted, 0) = 0
                AND numDomainID = @numDomainID
        UNION
        SELECT  CONVERT(VARCHAR(9), fld_id) + '~1' AS numFieldID ,
                fld_label AS vcFieldName ,
                1 AS Custom ,
                CONVERT(VARCHAR(10), fld_id) AS vcDbColumnName
        FROM    CFW_Fld_Master
                LEFT JOIN CFw_Grp_Master ON subgrp = CFw_Grp_Master.Grp_id
        WHERE   CFW_Fld_Master.grp_id = @PageId
                AND CFW_Fld_Master.numDomainID = @numDomainID
                AND Fld_type = 'TextBox'
        ORDER BY Custom ,
                vcFieldName             
    END 
	ELSE IF @FormId=141
	BEGIN
		SELECT  
			CONVERT(VARCHAR(9), numFieldID) + '~0' AS numFieldID ,
            (CASE WHEN @numViewID=6 AND numFieldID=248 THEN 'BizDoc' ELSE ISNULL(vcCultureFieldName, vcFieldName) END) AS vcFieldName ,
            0 AS Custom,
            vcDbColumnName,
			bitRequired
        FROM
			View_DynamicDefaultColumns
        WHERE 
			numFormID = @FormId
            AND ISNULL(bitSettingField, 0) = 1
            AND ISNULL(bitDeleted, 0) = 0
            AND numDomainID = @numDomainID
        UNION
        SELECT 
			CONVERT(VARCHAR(9), fld_id) + '~1' AS numFieldID ,
            fld_label AS vcFieldName ,
            1 AS Custom ,
            CONVERT(VARCHAR(10), fld_id) AS vcDbColumnName,
			0 AS bitRequired
        FROM 
			CFW_Fld_Master
        LEFT JOIN 
			CFw_Grp_Master 
		ON 
			subgrp = CFw_Grp_Master.Grp_id
        WHERE 
			CFW_Fld_Master.grp_id IN (2,5)
            AND CFW_Fld_Master.numDomainID = @numDomainID
            AND Fld_type <> 'Link'
        ORDER BY 
			Custom,
            (CASE WHEN @numViewID=6 AND numFieldID=248 THEN 'BizDoc' ELSE ISNULL(vcCultureFieldName, vcFieldName) END)
	END
	ELSE IF @FormId=142
	BEGIN
		SELECT  
			CONVERT(VARCHAR(9), numFieldID) + '~0' AS numFieldID,
            ISNULL(vcCultureFieldName, vcFieldName) AS vcFieldName,
            0 AS Custom,
            vcDbColumnName,
			bitRequired
        FROM
			View_DynamicDefaultColumns
        WHERE 
			numFormID = @FormId
            AND ISNULL(bitSettingField, 0) = 1
            AND ISNULL(bitDeleted, 0) = 0
            AND numDomainID = @numDomainID
		ORDER BY
			vcFieldName
	END
	ELSE IF @FormId = 145
	BEGIN
		SELECT 
			'6~0' AS numFieldID,'Actual Start' AS vcFieldName,0 AS Custom,'dtmActualStartDate' AS vcDbColumnName,0 bitRequired
			UNION SELECT '12~0' AS numFieldID,'Build Manager' AS vcFieldName,0 AS Custom,'vcAssignedTo' AS vcDbColumnName,0 bitRequired
			UNION SELECT '11~0' AS numFieldID,'Build Process' AS vcFieldName,0 AS Custom,'Slp_Name' AS vcDbColumnName,0 bitRequired
			UNION SELECT '13~0' AS numFieldID,'Capacity Load' AS vcFieldName,0 AS Custom,'CapacityLoad' AS vcDbColumnName,0 bitRequired
			UNION SELECT '10~0' AS numFieldID,'Forecast' AS vcFieldName,0 AS Custom,'ForecastDetails' AS vcDbColumnName,0 bitRequired
			UNION SELECT '4~0' AS numFieldID,'Item' AS vcFieldName,0 AS Custom,'AssemblyItemSKU' AS vcDbColumnName,0 bitRequired
			UNION SELECT '9~0' AS numFieldID,'Item Release' AS vcFieldName,0 AS Custom,'ItemReleaseDate' AS vcDbColumnName,0 bitRequired
			UNION SELECT '2~0' AS numFieldID,'Max Build Qty' AS vcFieldName,0 AS Custom,'MaxBuildQty' AS vcDbColumnName,0 bitRequired
			UNION SELECT '15~0' AS numFieldID,'Picked | Remaining' AS vcFieldName,0 AS Custom,'vcPickedRemaining' AS vcDbColumnName,0 bitRequired
			UNION SELECT '5~0' AS numFieldID,'Planned Start' AS vcFieldName,0 AS Custom,'dtmStartDate' AS vcDbColumnName,0 bitRequired
			UNION SELECT '8~0' AS numFieldID,'Projected Finish' AS vcFieldName,0 AS Custom,'dtmProjectFinishDate' AS vcDbColumnName,0 bitRequired
			UNION SELECT '7~0' AS numFieldID,'Requested Finish' AS vcFieldName,0 AS Custom,'dtmEndDate' AS vcDbColumnName,0 bitRequired
			UNION SELECT '3~0' AS numFieldID,'Total Progress' AS vcFieldName,0 AS Custom,'TotalProgress' AS vcDbColumnName,0 bitRequired
			UNION SELECT '14~0' AS numFieldID,'Work Order Status' AS vcFieldName,0 AS Custom,'vcWorkOrderStatus' AS vcDbColumnName,0 bitRequired
			UNION SELECT '1~0' AS numFieldID,'Work Order(Qty)' AS vcFieldName,0 AS Custom,'vcWorkOrderNameWithQty' AS vcDbColumnName,0 bitRequired
	END
    ELSE
    BEGIN
        SELECT  CONVERT(VARCHAR(9), numFieldID) + '~0' AS numFieldID ,
                ISNULL(vcCultureFieldName, vcFieldName) AS vcFieldName ,
                0 AS Custom ,
                vcDbColumnName
        FROM    View_DynamicDefaultColumns
        WHERE   numFormID = @FormId
                AND ISNULL(bitSettingField, 0) = 1
                AND ISNULL(bitDeleted, 0) = 0
                AND numDomainID = @numDomainID
        UNION
        SELECT  CONVERT(VARCHAR(9), fld_id) + '~1' AS numFieldID ,
                fld_label AS vcFieldName ,
                1 AS Custom ,
                CONVERT(VARCHAR(10), fld_id) AS vcDbColumnName
        FROM    CFW_Fld_Master
                LEFT JOIN CFw_Grp_Master ON subgrp = CFw_Grp_Master.Grp_id
        WHERE   1 = (CASE WHEN @FormId=21 THEN (CASE WHEN  CFW_Fld_Master.grp_id IN (5,9) THEN 1 ELSE 0 END) ELSE (CASE WHEN  CFW_Fld_Master.grp_id = @PageId THEN 1 ELSE 0 END) END)
                AND CFW_Fld_Master.numDomainID = @numDomainID
                AND Fld_type <> 'Link'
        ORDER BY Custom ,
                vcFieldName             
    END
	                          
                   
    IF @PageId = 1 OR @PageId = 4
    BEGIN
		IF (@FormId = 96 OR @FormId = 97)
                SELECT  CONVERT(VARCHAR(9), numFieldID) + '~0' numFieldID ,
                        ISNULL(vcCultureFieldName, vcFieldName) AS vcFieldName ,
                        vcDbColumnName ,
                        0 AS Custom ,
                        tintRow AS tintOrder
                FROM    View_DynamicColumns
                WHERE   numFormId = @FormId
                        AND numDomainID = @numDomainID
                        AND ISNULL(bitCustom, 0) = 0
                        AND tintPageType = 1
                        AND ISNULL(bitSettingField, 0) = 1
                        AND numRelCntType = @numtype
                        AND ISNULL(bitDeleted, 0) = 0
                UNION
                SELECT  CONVERT(VARCHAR(9), numFieldID) + '~1' AS numFieldID ,
                        vcFieldName ,
                        vcFieldName AS vcDbColumnName ,
                        1 AS Custom ,
                        tintRow AS tintOrder
                FROM    View_DynamicCustomColumns
                WHERE   numFormID = @FormId
                        AND numDomainID = @numDomainID
                        AND grp_id = @PageId
                        AND numDomainID = @numDomainID
                        AND vcAssociatedControlType <> 'Link'
                        AND ISNULL(bitCustom, 0) = 1
                        AND tintPageType = 1
                        AND numRelCntType = @numtype
                ORDER BY tintOrder                
            ELSE
                SELECT  CONVERT(VARCHAR(9), numFieldID) + '~0' numFieldID ,
                        ISNULL(vcCultureFieldName, vcFieldName) AS vcFieldName ,
                        vcDbColumnName ,
                        0 AS Custom ,
                        tintRow AS tintOrder
                FROM    View_DynamicColumns
                WHERE   numFormId = @FormId
                        AND numUserCntID = @numUserCntID
                        AND numDomainID = @numDomainID
                        AND ISNULL(bitCustom, 0) = 0
                        AND tintPageType = 1
                        AND ISNULL(bitSettingField, 0) = 1
                        AND numRelCntType = @numtype
                        AND ISNULL(bitDeleted, 0) = 0
                UNION
                SELECT  CONVERT(VARCHAR(9), numFieldID) + '~1' AS numFieldID ,
                        vcFieldName ,
                        '' AS vcDbColumnName ,
                        1 AS Custom ,
                        tintRow AS tintOrder
                FROM    View_DynamicCustomColumns
                WHERE   numFormID = @FormId
                        AND numUserCntID = @numUserCntID
                        AND numDomainID = @numDomainID
                        AND grp_id = @PageId
                        AND numDomainID = @numDomainID
                        AND vcAssociatedControlType <> 'Link'
                        AND ISNULL(bitCustom, 0) = 1
                        AND tintPageType = 1
                        AND numRelCntType = @numtype
                ORDER BY tintOrder          
    END     
    ELSE IF @PageId = 10
    BEGIN
        SELECT  CONVERT(VARCHAR(9), numFieldID) + '~0' numFieldID ,
                ISNULL(vcCultureFieldName, vcFieldName) AS vcFieldName ,
                vcDbColumnName ,
                0 AS Custom ,
                tintRow AS tintOrder ,
                numListID ,
                numFieldID AS [numFieldID1]
        FROM    View_DynamicColumns
        WHERE   numFormId = @FormId
                AND numUserCntID = @numUserCntID
                AND numDomainID = @numDomainID
                AND ISNULL(bitCustom, 0) = 0
                AND tintPageType = 1
                AND ISNULL(bitSettingField, 0) = 1
                AND numRelCntType = @numtype
                AND ISNULL(bitDeleted, 0) = 0
        UNION
        SELECT  CONVERT(VARCHAR(9), numFieldID) + '~1' AS numFieldID ,
                vcFieldName ,
                '' AS vcDbColumnName ,
                1 AS Custom ,
                tintRow AS tintOrder ,
                numListID ,
                numFieldID AS [numFieldID1]
        FROM    View_DynamicCustomColumns
        WHERE   numFormID = @FormId
                AND numUserCntID = @numUserCntID
                AND numDomainID = @numDomainID   
                AND numDomainID = @numDomainID
                AND vcAssociatedControlType = 'SelectBox'
                AND ISNULL(bitCustom, 0) = 1
                AND tintPageType = 1
                AND numRelCntType = @numtype
        ORDER BY tintOrder    
    END	   
    ELSE IF @PageId = 5 AND (@FormId = 84 OR @FormId = 85)
    BEGIN
        IF EXISTS (SELECT 
						'col1'
                    FROM 
						DycFormConfigurationDetails
                    WHERE 
						numDomainID = @numDomainID
                        AND numFormId = @FormID
                        AND numFieldID IN (507, 508, 509, 510, 511, 512)
					)
        BEGIN
            SELECT  CONVERT(VARCHAR(9), numFieldID) + '~0' numFieldID ,
                    ISNULL(vcCultureFieldName, vcFieldName) AS vcFieldName ,
                    vcDbColumnName ,
                    0 AS Custom ,
                    tintRow AS tintOrder ,
                    vcAssociatedControlType ,
                    numlistid ,
                    vcLookBackTableName ,
                    numFieldID AS FieldId ,
                    bitAllowEdit ,
                    0 AS AZordering
            FROM    View_DynamicColumns
            WHERE   numFormId = @FormID
                    AND numDomainID = @numDomainID
                    AND ISNULL(bitCustom, 0) = 0   
                    AND ISNULL(bitSettingField, 0) = 1
                    AND ISNULL(bitDeleted, 0) = 0
            UNION
            SELECT  CONVERT(VARCHAR(9), numFieldID) + '~1' AS numFieldID ,
                    vcFieldName ,
                    '' AS vcDbColumnName ,
                    1 AS Custom ,
                    tintRow AS tintOrder ,
                    vcAssociatedControlType ,
                    numlistid ,
                    '' AS vcLookBackTableName ,
                    numFieldID AS FieldId ,
                    0 AS bitAllowEdit ,
                    0 AS AZordering
            FROM    View_DynamicCustomColumns
            WHERE   numFormID = @FormID
                    AND numDomainID = @numDomainID
                    AND grp_id = @PageID
                    AND vcAssociatedControlType = 'TextBox'
                    AND ISNULL(bitCustom, 0) = 1
            ORDER BY tintOrder 
        END
        ELSE
        BEGIN
            SELECT  CONVERT(VARCHAR(9), numFieldID) + '~0' numFieldID ,
                    ISNULL(vcCultureFieldName, vcFieldName) AS vcFieldName ,
                    vcDbColumnName ,
                    0 AS Custom ,
                    tintRow AS tintOrder ,
                    vcAssociatedControlType ,
                    numlistid ,
                    vcLookBackTableName ,
                    numFieldID AS FieldId ,
                    bitAllowEdit ,
                    0 AS AZordering
            FROM    View_DynamicColumns
            WHERE   numFormId = @FormID
                    AND numDomainID = @numDomainID
                    AND ISNULL(bitCustom, 0) = 0   
                    AND ISNULL(bitSettingField, 0) = 1
                    AND ISNULL(bitDeleted, 0) = 0
            UNION
            SELECT  CONVERT(VARCHAR(9), numFieldID) + '~1' AS numFieldID ,
                    vcFieldName ,
                    '' AS vcDbColumnName ,
                    1 AS Custom ,
                    tintRow AS tintOrder ,
                    vcAssociatedControlType ,
                    numlistid ,
                    '' AS vcLookBackTableName ,
                    numFieldID AS FieldId ,
                    0 AS bitAllowEdit ,
                    0 AS AZordering
            FROM    View_DynamicCustomColumns
            WHERE   numFormID = @FormID
                    AND numDomainID = @numDomainID
                    AND grp_id = @PageID
                    AND vcAssociatedControlType = 'TextBox'
                    AND ISNULL(bitCustom, 0) = 1
            UNION
            SELECT  '507~0' AS numFieldID ,
                    'Title:A-Z' AS vcFieldName ,
                    'Title:A-Z' AS vcDbColumnName ,
                    1 AS Custom ,
                    0 AS tintOrder ,
                    ' ' AS vcAssociatedControlType ,
                    1 AS numlistid ,
                    '' AS vcLookBackTableName ,
                    507 AS FieldId ,
                    0 AS bitAllowEdit ,
                    1 AS IsFixed
            UNION
            SELECT  '508~0' AS numFieldID ,
                    'Title:Z-A' AS vcFieldName ,
                    'Title:Z-A' AS vcDbColumnName ,
                    1 AS Custom ,
                    0 AS tintOrder ,
                    ' ' AS vcAssociatedControlType ,
                    1 AS numlistid ,
                    '' AS vcLookBackTableName ,
                    508 AS FieldId ,
                    0 AS bitAllowEdit ,
                    1 AS IsFixed
            UNION
            SELECT  '509~0' AS numFieldID ,
                    'Price:Low to High' AS vcFieldName ,
                    'Price:Low to High' AS vcDbColumnName ,
                    1 AS Custom ,
                    0 AS tintOrder ,
                    ' ' AS vcAssociatedControlType ,
                    1 AS numlistid ,
                    '' AS vcLookBackTableName ,
                    509 AS FieldId ,
                    0 AS bitAllowEdit ,
                    1 AS IsFixed
            UNION
            SELECT  '510~0' AS numFieldID ,
                    'Price:High to Low' AS vcFieldName ,
                    'Price:High to Low' AS vcDbColumnName ,
                    1 AS Custom ,
                    0 AS tintOrder ,
                    ' ' AS vcAssociatedControlType ,
                    1 AS numlistid ,
                    '' AS vcLookBackTableName ,
                    510 AS FieldId ,
                    0 AS bitAllowEdit ,
                    1 AS IsFixed
            UNION
            SELECT  '511~0' AS numFieldID ,
                    'New Arrival' AS vcFieldName ,
                    'New Arrival' AS vcDbColumnName ,
                    1 AS Custom ,
                    0 AS tintOrder ,
                    ' ' AS vcAssociatedControlType ,
                    1 AS numlistid ,
                    '' AS vcLookBackTableName ,
                    511 AS FieldId ,
                    0 AS bitAllowEdit ,
                    1 AS IsFixed
            UNION
            SELECT  '512~0' AS numFieldID ,
                    'Oldest' AS vcFieldName ,
                    'Oldest' AS vcDbColumnName ,
                    1 AS Custom ,
                    0 AS tintOrder ,
                    ' ' AS vcAssociatedControlType ,
                    1 AS numlistid ,
                    '' AS vcLookBackTableName ,
                    512 AS FieldId ,
                    0 AS bitAllowEdit ,
                    1 AS IsFixed
            ORDER BY tintOrder 
        END
    END
	ELSE IF @FormId=43
	BEGIN
            SELECT  CONVERT(VARCHAR(9), numFieldID) + '~0' numFieldID ,
                    ISNULL(vcCultureFieldName, vcFieldName) AS vcFieldName ,
                    vcDbColumnName ,
                    0 AS Custom ,
                    tintRow AS tintOrder ,
                    vcAssociatedControlType ,
                    numlistid ,
                    vcLookBackTableName ,
                    numFieldID AS FieldId ,
                    bitAllowEdit
            FROM    View_DynamicColumns
            WHERE   numFormId = @FormId
                    AND numUserCntID = @numUserCntID
                    AND numDomainID = @numDomainID
                    AND ISNULL(bitCustom, 0) = 0
                    AND tintPageType = 1
                    AND ISNULL(bitSettingField, 0) = 1
                    AND ISNULL(numRelCntType, 0) = @numtype
                    AND ISNULL(bitDeleted, 0) = 0
                    AND ISNULL(numViewID, 0) = @numViewID
            UNION
            SELECT  CONVERT(VARCHAR(9), numFieldID) + '~1' AS numFieldID ,
                    vcFieldName ,
                    '' AS vcDbColumnName ,
                    1 AS Custom ,
                    tintRow AS tintOrder ,
                    vcAssociatedControlType ,
                    numlistid ,
                    '' AS vcLookBackTableName ,
                    numFieldID AS FieldId ,
                    0 AS bitAllowEdit
            FROM    View_DynamicCustomColumns
            WHERE   numFormID = @FormId
                    AND numUserCntID = @numUserCntID
                    AND numDomainID = @numDomainID
                    AND grp_id = 1
                    AND numDomainID = @numDomainID
                    AND vcAssociatedControlType <> 'Link'
                    AND ISNULL(bitCustom, 0) = 1
                    AND tintPageType = 1
                    AND ISNULL(numRelCntType, 0) = @numtype
                    AND ISNULL(numViewID, 0) = @numViewID
            ORDER BY tintOrder       
	END
	ELSE IF @FormId=141
	BEGIN
		SELECT  
			CONVERT(VARCHAR(9), numFieldID) + '~0' numFieldID ,
            (CASE WHEN @numViewID=6 AND numFieldID=248 THEN 'BizDoc' ELSE ISNULL(vcCultureFieldName, vcFieldName) END) AS vcFieldName ,
            vcDbColumnName,
            0 AS Custom,
            tintRow AS tintOrder,
            vcAssociatedControlType,
            numlistid,
            vcLookBackTableName,
            numFieldID AS FieldId,
            bitAllowEdit
        FROM 
			View_DynamicColumns
        WHERE 
			numFormId = @FormId
            AND numUserCntID = @numUserCntID
            AND numDomainID = @numDomainID
            AND ISNULL(bitCustom, 0) = 0
            AND tintPageType = 1
            AND ISNULL(bitSettingField, 0) = 1
            AND ISNULL(numRelCntType, 0) = @numtype
            AND ISNULL(bitDeleted, 0) = 0
            AND ISNULL(numViewID, 0) = @numViewID
        UNION
        SELECT 
			CONVERT(VARCHAR(9), numFieldID) + '~1' AS numFieldID ,
            vcFieldName ,
            '' AS vcDbColumnName ,
            1 AS Custom ,
            tintRow AS tintOrder ,
            vcAssociatedControlType ,
            numlistid ,
            '' AS vcLookBackTableName ,
            numFieldID AS FieldId ,
            0 AS bitAllowEdit
        FROM 
			View_DynamicCustomColumns
        WHERE 
			numFormID = @FormId
            AND numUserCntID = @numUserCntID
            AND numDomainID = @numDomainID
            AND grp_id IN (2,5)
            AND numDomainID = @numDomainID
            AND vcAssociatedControlType <> 'Link'
            AND ISNULL(bitCustom, 0) = 1
            AND tintPageType = 1
            AND ISNULL(numRelCntType, 0) = @numtype
            AND ISNULL(numViewID, 0) = @numViewID
        ORDER BY 
			tintOrder 
	END
	ELSE IF @FormId=142
	BEGIN
		SELECT  
			CONVERT(VARCHAR(9), numFieldID) + '~0' numFieldID ,
            ISNULL(vcCultureFieldName, vcFieldName) AS vcFieldName ,
            vcDbColumnName ,
            0 AS Custom ,
            tintRow AS tintOrder ,
            vcAssociatedControlType ,
            numlistid ,
            vcLookBackTableName ,
            numFieldID AS FieldId ,
            bitAllowEdit
        FROM 
			View_DynamicColumns
        WHERE 
			numFormId = @FormId
            AND numUserCntID = @numUserCntID
            AND numDomainID = @numDomainID
            AND ISNULL(bitCustom, 0) = 0
            AND tintPageType = 1
            AND ISNULL(bitSettingField, 0) = 1
            AND ISNULL(numRelCntType, 0) = @numtype
            AND ISNULL(bitDeleted, 0) = 0
            AND ISNULL(numViewID, 0) = @numViewID
	END
	ELSE IF @FormId=145
	BEGIN
		SELECT 
			CONVERT(VARCHAR(9), numFieldID) + '~0' numFieldID ,
            (CASE numFieldID
				WHEN 1 THEN 'Work Order(Qty)'
				WHEN 2 THEN 'Max Build Qty'
				WHEN 3 THEN 'Total Progress'
				WHEN 4 THEN 'Item'
				WHEN 5 THEN 'Planned Start'
				WHEN 6 THEN 'Actual Start'
				WHEN 7 THEN 'Requested Finish'
				WHEN 8 THEN 'Projected Finish'
				WHEN 9 THEN 'Item Release'
				WHEN 10 THEN 'Forecast'
				WHEN 11 THEN 'Build Process'
				WHEN 12 THEN 'Build Manager'
				WHEN 13 THEN 'Capacity Load'
				WHEN 14 THEN 'Work Order Status'
				WHEN 15 THEN 'Picked | Remaining'
				ELSE '-'
			END) AS vcFieldName ,
            (CASE numFieldID
				WHEN 1 THEN 'vcWorkOrderNameWithQty'
				WHEN 2 THEN 'MaxBuildQty'
				WHEN 3 THEN 'TotalProgress'
				WHEN 4 THEN 'AssemblyItemSKU'
				WHEN 5 THEN 'dtmStartDate'
				WHEN 6 THEN 'dtmActualStartDate'
				WHEN 7 THEN 'dtmEndDate'
				WHEN 8 THEN 'dtmProjectFinishDate'
				WHEN 9 THEN 'ItemReleaseDate'
				WHEN 10 THEN 'ForecastDetails'
				WHEN 11 THEN 'Slp_Name'
				WHEN 12 THEN 'vcAssignedTo'
				WHEN 13 THEN 'CapacityLoad'
				WHEN 14 THEN 'vcWorkOrderStatus'
				WHEN 15 THEN 'vcPickedRemaining'
				ELSE '-'
			END) AS vcDbColumnName ,
            0 AS Custom ,
            intRowNum AS tintOrder ,
            'Label' vcAssociatedControlType ,
            0 AS numlistid,
            'WorkOrder' AS vcLookBackTableName ,
            numFieldID AS FieldId ,
            0 AS  bitAllowEdit
		FROM 
			DycFormConfigurationDetails 
		WHERE 
			numFormId=145
			AND numUserCntID = @numUserCntID
            AND numDomainID = @numDomainID
	END
    ELSE
    BEGIN
		SELECT  CONVERT(VARCHAR(9), numFieldID) + '~0' numFieldID ,
				ISNULL(vcCultureFieldName, vcFieldName) AS vcFieldName ,
				vcDbColumnName ,
				0 AS Custom ,
				tintRow AS tintOrder ,
				vcAssociatedControlType ,
				numlistid ,
				vcLookBackTableName ,
				numFieldID AS FieldId ,
				bitAllowEdit
		FROM    View_DynamicColumns
		WHERE   numFormId = @FormId
				AND numUserCntID = @numUserCntID
				AND numDomainID = @numDomainID
				AND ISNULL(bitCustom, 0) = 0
				AND tintPageType = 1
				AND ISNULL(bitSettingField, 0) = 1
				AND ISNULL(numRelCntType, 0) = @numtype
				AND ISNULL(bitDeleted, 0) = 0
				AND ISNULL(numViewID, 0) = @numViewID
		UNION
		SELECT  CONVERT(VARCHAR(9), numFieldID) + '~1' AS numFieldID ,
				vcFieldName ,
				'' AS vcDbColumnName ,
				1 AS Custom ,
				tintRow AS tintOrder ,
				vcAssociatedControlType ,
				numlistid ,
				'' AS vcLookBackTableName ,
				numFieldID AS FieldId ,
				0 AS bitAllowEdit
		FROM    View_DynamicCustomColumns
		WHERE   numFormID = @FormId
				AND numUserCntID = @numUserCntID
				AND numDomainID = @numDomainID
				AND 1 = (CASE WHEN @FormId=21 THEN (CASE WHEN  grp_id IN (5,9) THEN 1 ELSE 0 END) ELSE (CASE WHEN  grp_id = @PageId THEN 1 ELSE 0 END) END)
				AND numDomainID = @numDomainID
				AND vcAssociatedControlType <> 'Link'
				AND ISNULL(bitCustom, 0) = 1
				AND tintPageType = 1
				AND ISNULL(numRelCntType, 0) = @numtype
				AND ISNULL(numViewID, 0) = @numViewID
		ORDER BY tintOrder          
    END
GO
