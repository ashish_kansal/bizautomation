/****** Object:  StoredProcedure [dbo].[Usp_getCorrespondance]    Script Date: 07/26/2008 16:17:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getcorrespondance')
DROP PROCEDURE usp_getcorrespondance
GO
CREATE PROCEDURE [dbo].[usp_getcorrespondance]                          
@FromDate as datetime,                          
@toDate as datetime,                          
@numContactId as numeric,                          
@vcMessageFrom as varchar(200)='' ,                        
@tintSortOrder as numeric,                        
@SeachKeyword as varchar(100),                        
@CurrentPage as INT,                        
@PageSize as INT,                        
@TotRecs as numeric output,                        
@columnName as varchar(100),                        
@columnSortOrder as varchar(50),                        
@filter as numeric,                        
@numdivisionId as numeric =0,              
@numDomainID as numeric(9)=0,              
@numCaseID as numeric(9)=0,              
@ClientTimeZoneOffset INT,
@numOpenRecordID NUMERIC=0,
@tintMode TINYINT=0,
@bitOpenCommu BIT=0,
@numUserCntID NUMERIC=0,
@vcTypes VARCHAR(200)
as 
BEGIN
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
DECLARE @vcUserLoginEmail AS VARCHAR(MAX)=''
SET @vcUserLoginEmail = (SELECT 
distinct  
    stuff((
        select ',' +  replace(vcEmailID,'''','''''') FROM UserMaster AS U where numDomainID=@numDomainID 
		order by U.vcEmailID
        for xml path('')
    ),1,1,'') as userlist
from UserMaster where numDomainID=@numDomainID 
group by vcEmailID)
set @vcMessageFrom=replace(@vcMessageFrom,'''','|')
                         
if @filter>0 set @tintSortOrder=  @filter
              
if ((@filter <=2 and @tintSortOrder<=2) or (@tintSortOrder>2 and @filter>2))                 
begin  
                                                                  
 declare @strSql as nvarchar(MAX)  =''     
                                  
  declare @firstRec as integer                                                        
  declare @lastRec as integer                                                        
  set @firstRec= (@CurrentPage-1) * @PageSize                                                        
  set @lastRec= (@CurrentPage*@PageSize+1)                 
  set @strSql=''                

 if (@filter <=2 and @tintSortOrder<=2 and @bitOpenCommu=0)                 
 begin  
 
 declare @strCondition as varchar(MAX);
 declare @vcContactEmail as varchar(MAX)='';
              
  if @numdivisionId =0 and  @vcMessageFrom <> '' 
 BEGIN 
	If @filter = 1 --ReceivedMessages
		set  @strCondition=' and (vcFrom like ''%'+ REPLACE(@vcMessageFrom,'''','''''')+'%'')  ' 
	ELSE IF @filter=2 --SentMessages
		set  @strCondition=' and (vcTo like ''%'+ REPLACE(@vcMessageFrom,'''','''''')+'%'')  ' 
	ELSE
		set  @strCondition=' and (vcFrom like ''%'+ REPLACE(@vcMessageFrom,'''','''''')+'%'' or vcTo like ''%'+ REPLACE(@vcMessageFrom,'''','''''')+'%'')  ' 
 END
  else if @numdivisionId <>0 
 BEGIN

 /* This approach does not work as numEmailId stores who sent email, id of sender
  set @strCondition=' and HDR.numEmailID in (
  SELECT numEmailID FROM dbo.EmailMaster WHERE numDomainID= ' + CONVERT(VARCHAR(15),@numDomainID) +' AND numContactID 
  IN (SELECT numcontactid FROM dbo.AdditionalContactsInformation WHERE numDivisionID = ' + convert(varchar(15),@numDivisionID) + ' AND numDomainID = ' + CONVERT(VARCHAR(15),@numDomainID) +' ))';
*/
-- bug fix id 1591,1628 
  DECLARE @TnumContactID NUMERIC(9);set @strCondition=''

  select TOP 1 @TnumContactID=numContactID,@vcContactEmail=replace(vcEmail,'''','''''') from AdditionalContactsInformation where numdivisionid=@numdivisionId and len(vcEmail)>2
  ORDER BY numContactID ASC
  SET @vcContactEmail = ISNULL(@vcContactEmail,'')
  WHILE @TnumContactID>0
  BEGIN
   if len(@strCondition)>0
    SET @strCondition=@strCondition + ' or '

	If @filter = 1 --ReceivedMessages
		set  @strCondition = @strCondition + ' (vcFrom like ''%'+ REPLACE(@vcContactEmail,'''','''''') +'%'')  ' 
	ELSE IF @filter=2 --SentMessages
		set  @strCondition= @strCondition + ' (vcTo like ''%'+ REPLACE(@vcContactEmail,'''','''''') +'%'')  ' 
	ELSE
		set  @strCondition= @strCondition + ' (vcFrom like ''%'+ REPLACE(@vcContactEmail,'''','''''')+'%'' or vcTo like ''%'+ REPLACE(@vcContactEmail,'''','''''')+'%'')  '

   select TOP 1 @TnumContactID=numContactID,@vcContactEmail=vcEmail from AdditionalContactsInformation where numdivisionid=@numdivisionId and len(vcEmail)>2 and numContactID>@TnumContactID
   ORDER BY numContactID ASC

   IF @@rowcount = 0 SET @TnumContactID = 0 
  END
 
  if len(@strCondition)>0
   SET @strCondition= 'and (' + @strCondition + ')'
  else if @tintMode <> 6 AND @tintMode <> 5
   SET @strCondition=' and (1=0)' 
  --set @strCondition=' and DTL.numEmailId  in (select numEmailId from EmailMaster where vcEmailID in (select vcEmail  from AdditionalContactsInformation where numdivisionid='''+ convert(varchar(15),@numdivisionId)+''' ))    '          
  END
  else if @numContactId>0 
 BEGIN
  select @vcContactEmail=isnull(replace(vcEmail,'''',''''''),'') from AdditionalContactsInformation where numContactID=@numContactId and len(vcEmail)>2
  SET @vcContactEmail = ISNULL(@vcContactEmail,'')
  if len(@vcContactEmail)>0
	If @filter = 1 --ReceivedMessages
		set  @strCondition=' and (vcFrom like ''%'+ REPLACE(@vcContactEmail,'''','''''') +'%'')  ' 
	ELSE IF @filter=2 --SentMessages
		set  @strCondition=' and (vcTo like ''%'+ REPLACE(@vcContactEmail,'''','''''')+'%'')  ' 
	ELSE
		set  @strCondition=' and (vcFrom like ''%'+ REPLACE(@vcContactEmail,'''','''''')+'%'' or vcTo like ''%'+ REPLACE(@vcContactEmail,'''','''''')+'%'')' 
  else if @tintMode <> 6 AND @tintMode <> 5
   SET @strCondition=' and (1=0)'
 END
 else 
  SET @strCondition=''

  CREATE TABLE #TEMPData
  (
		numTotalRecords INT
		,tintCountType TINYINT
		,numEmailHstrID NUMERIC(18,0)
		,DelData VARCHAR(200)
		,bintCreatedOn DATETIME
		,[date] VARCHAR(100)
		,[Subject] VARCHAR(MAX)
		,[type] VARCHAR(100)
		,phone VARCHAR(100)
		,assignedto VARCHAR(100)
		,caseid NUMERIC(18,0)
		,vcCasenumber VARCHAR(200)
		,caseTimeid NUMERIC(18,0)
		,caseExpid NUMERIC(18,0)
		,tinttype NUMERIC(18,0)
		,dtCreatedDate DATETIME
		,[From] VARCHAR(MAX)
		,bitClosedflag BIT
		,bitHasAttachments BIT
		,vcBody VARCHAR(MAX)
		,InlineEdit VARCHAR(1000)
		,numCreatedBy NUMERIC(18,0)
		,numOrgTerId NUMERIC(18,0)
		,TypeClass VARCHAR(100)
  )


  set @strSql= 'INSERT INTO 
					#TEMPData 
				SELECT 
					COUNT(*) OVER() AS numTotalRecords
					,1 AS tintCountType
					,HDR.numEmailHstrID
					,convert(varchar(15),HDR.numEmailHstrID)+''~1~0''+convert(varchar(15),HDR.numUserCntId) as DelData
					,HDR.dtReceivedOn AS bintCreatedOn
					,dbo.FormatedDateTimeFromDate(DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+', dtReceivedOn),'+convert(varchar(15),@numDomainID)+') as [date]
					,vcSubject as [Subject]
					,CASE WHEN (SELECT COUNT(*) FROM dbo.SplitByString(vcFrom,''$^$'') WHERE Items 
					IN( select  replace(vcEmailID,'''''''','''''''') FROM UserMaster AS U where numDomainID='+convert(varchar(15),@numDomainID)+'))  > 0  THEN ''Email Out'' ELSE ''Email In'' END as [type]
					,'''' as phone
					,'''' as assignedto
					,0 as caseid
					,null as vcCasenumber
					,0 as caseTimeid
					,0 as caseExpid
					,hdr.tinttype
					,HDR.dtReceivedOn as dtCreatedDate
					,vcFrom + '','' + vcTo as [From]
					,0 as bitClosedflag
					,ISNULL(HDR.bitHasAttachments,0) bitHasAttachments
					,CASE WHEN LEN(Cast(vcBodyText AS VARCHAR(1000)))>150 THEN SUBSTRING(vcBodyText,0,150) + ''...'' ELSE Cast(vcBodyText AS VARCHAR(1000)) END AS vcBody
					,''''  AS InlineEdit
					,HDR.numUserCntId AS numCreatedBy
					,0 AS numOrgTerId
					,CASE WHEN (SELECT COUNT(*) FROM dbo.SplitByString(vcFrom,''$^$'') WHERE Items 
					IN( select  replace(vcEmailID,'''''''','''''''') FROM UserMaster AS U where numDomainID='+convert(varchar(15),@numDomainID)+'))  > 0 THEN ''redColorLabel'' ELSE ''greenColorLabel'' END as TypeClass
			  FROM EmailHistory HDR                                 
			  LEFT JOIN [Correspondence] CR ON CR.numEmailHistoryID = HDR.numEmailHstrID ' 
     

   set  @strSql=@strSql +' where ISNULL(HDR.bitInvisible,0) = 0 AND  (HDR.numNodeId IN (SELECT numNodeID FROM InboxTreeSort WHERE vcNodeName NOT IN (''All Mail'',''Important'',''Drafts'') AND numDomainID='+convert(varchar(15),@numDomainID)+')  OR HDR.numNodeId=0) AND HDR.numDomainID='+convert(varchar(15),@numDomainID)
   + ' AND (CR.numOpenRecordID = '+ convert(varchar(15),@numOpenRecordID) +' OR '+ convert(varchar(15),@numOpenRecordID) +'=0)'
   + (CASE WHEN ISNULL(@vcTypes,'') <> '' THEN ' AND 1 = 0' ELSE ' AND 1 = 1' END)
   
   IF(@FromDate IS NOT NULL AND @FromDate<>'')
   BEGIN
  SET @strSql = @strSql +' and  (dtReceivedOn between '''+ convert(varchar(30),@FromDate)+''' and '''+ convert(varchar(30),@ToDate)+''') '
  END
 set  @strSql=@strSql+ @strCondition


  IF @tintMode=1 SET @strSql=@strSql+ ' and  (CR.tintCorrType IN(1,3) OR '+ convert(varchar(15),@numOpenRecordID) +'=0)'
  IF @tintMode=2 SET @strSql=@strSql+ ' and  (CR.tintCorrType IN(2,4) OR '+ convert(varchar(15),@numOpenRecordID) +'=0)'
  IF @tintMode=5 SET @strSql=@strSql+ ' and  (CR.tintCorrType IN(5) OR '+ convert(varchar(15),@numOpenRecordID) +'=0)'
  IF @tintMode=6 SET @strSql=@strSql+ ' and  (CR.tintCorrType IN(6) OR '+ convert(varchar(15),@numOpenRecordID) +'=0)'
  
  if @filter =0 set  @strSql=@strSql+ ' and (hdr.tinttype=1 or hdr.tinttype=2 or  hdr.tinttype=5)'                  
  else if @filter =1 OR @filter =2 set  @strSql=@strSql+ ' and (hdr.tinttype=1 OR hdr.tinttype=2)'                                             
  if @filter > 2 set  @strSql=@strSql+ ' and hdr.tinttype=0'                 
                        
  if @tintSortOrder =0 and @SeachKeyword <> ''                 
  begin                
   set  @strSql=@strSql+ ' and (vcFrom like ''%'+ @SeachKeyword+'%'' or vcTo like ''%'+ @SeachKeyword+'%'' or vcsubject like ''%'+@SeachKeyword+'%'' or vcBody like ''%'+@SeachKeyword+'%'')'               
  end                
  if @tintSortOrder =1 and @SeachKeyword <> ''                 
  begin                
     set  @strSql=@strSql+ ' and (vcFrom like ''%'+ @SeachKeyword+'%'' or vcsubject like ''%'+@SeachKeyword+'%'' or  vcBody like ''%'+@SeachKeyword+'%'')'                 
  end                
  if @tintSortOrder =2 and @SeachKeyword <> ''                 
  begin                
     set  @strSql=@strSql+ ' and (vcTo like ''%'+ @SeachKeyword+'%'' or vcsubject like ''%'+@SeachKeyword+'%'' or  vcBody like ''%'+@SeachKeyword+'%'')'                 
  end                            
 end                

 

 if ((@filter=0 or @filter>2) and (@tintSortOrder=0 or @tintSortOrder>2))                
 begin                
  set  @strSql=@strSql+ '                     
  INSERT INTO #TEMPData SELECT COUNT(*) OVER() AS numTotalRecords, 2 AS tintCountType, c.numCommId as numEmailHstrID,convert(varchar(15),C.numCommId)+''~2~''+convert(varchar(15),C.numCreatedBy) as DelData,CASE WHEN c.bitTask = 973 THEN dtCreatedDate WHEN c.bitTask = 974 THEN dtCreatedDate WHEN c.bitTask = 971 THEN dtStartTime ELSE dtEndTime END AS bintCreatedOn,
  case when c.bitTask=973 then dbo.FormatedDateTimeFromDate(DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+', dtStartTime),'+convert(varchar(15),@numDomainID)+') 
  when c.bitTask=974 then dbo.FormatedDateTimeFromDate(DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+', dtStartTime),'+convert(varchar(15),@numDomainID)+') 
  when c.bitTask=971 THEN dbo.FormatedDateFromDate(DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+', dtEndTime),'+convert(varchar(15),@numDomainID)+') + '' '' + RIGHT(CONVERT(VARCHAR,DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+', [dtStartTime]),100),7)  +'' - '' + RIGHT(CONVERT(VARCHAR,DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+', [dtEndTime]),100),7)
  else  dbo.FormatedDateFromDate(DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+', dtEndTime),'+convert(varchar(15),@numDomainID)+') + '' '' + RIGHT(CONVERT(VARCHAR,DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+', [dtStartTime]),100),7)  +'' - '' + RIGHT(CONVERT(VARCHAR,DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+', [dtEndTime]),100),7)  END AS [date],
  convert(varchar(8000),textdetails) as [Subject],                                                                         
 dbo.fn_GetListItemName(c.bitTask)  AS [Type],                                                                 
  A1.vcFirstName +'' ''+ A1.vcLastName +'' ''+                                      
  case when A1.numPhone<>'''' then + A1.numPhone +case when A1.numPhoneExtension<>'''' then '' - '' + A1.numPhoneExtension else '''' end  else '''' end as [Phone],                        
  A2.vcFirstName+ '' ''+A2.vcLastName  as assignedto,                         
  c.caseid,                           
  (select top 1 vcCaseNumber from cases where cases.numcaseid= c.caseid )as vcCasenumber,                               
  isnull(caseTimeid,0)as caseTimeid,                              
  isnull(caseExpid,0)as caseExpid  ,                        
  1 as tinttype ,dtCreatedDate,'''' as [FROM]     ,isnull(C.bitClosedflag,0) as bitClosedflag,0 as bitHasAttachments,
'''' as vcBody,
''id="Tickler~184~False~''+CAST(c.bitTask AS VARCHAR)+''~''+CAST(c.numCommId AS VARCHAR)+''" class="editable_textarea" onmousemove="bgColor=''''lightgoldenRodYellow''''"  title="Double click to edit..." bgcolor="lightgoldenRodYellow" '' AS InlineEdit,
C.numCreatedBy,
ISNULL(Div.numTerId,0) AS numOrgTerId,
''blueColorLabel'' as TypeClass
  from  Communication C                  
  JOIN AdditionalContactsInformation  A1                                           
  ON c.numContactId = A1.numContactId
  LEFT JOIN CommunicationLinkedOrganization clo ON C.numCommID=clo.numCommID                                                                             
  left join AdditionalContactsInformation A2 on A2.numContactID=c.numAssign
  left join UserMaster UM on A2.numContactid=UM.numUserDetailId and isnull(UM.bitActivateFlag,0)=1                                                                           
  left join listdetails on numActivity=numListID                           
  LEFT JOIN [Correspondence] CR ON CR.numCommID = C.numCommID
  JOIN DivisionMaster Div                                                                         
 ON A1.numDivisionId = Div.numDivisionID    
  where  C.numDomainID=' +convert(varchar(15),@numDomainID)
  + (CASE WHEN ISNULL(@vcTypes,'') <> '' THEN ' AND C.bitTask IN (SELECT Id FROM dbo.SplitIDs(''' + ISNULL(@vcTypes,'') + ''','',''))' ELSE '' END) + 
  + ' AND (numOpenRecordID = '+ convert(varchar(15),@numOpenRecordID) +' OR '+ convert(varchar(15),@numOpenRecordID) +'=0)'
  if (@numdivisionId =0 and @numContactId<>0) set  @strSql=@strSql+' and C.numContactId='+ convert(varchar(15),@numContactId)                        
  if @numdivisionId <> 0 set  @strSql=@strSql+' and  (C.numDivisionId='+ convert(varchar(15),@numdivisionId) + ' OR clo.numDivisionID='+ convert(varchar(15),@numdivisionId) + ') '             
  if @numCaseID>0  set  @strSql=@strSql+' and  C.CaseId='+ convert(varchar(15),@numCaseID)                       
  if @filter =0  
  BEGIN
	IF @tintMode <> 9
	BEGIN
		
		IF(@FromDate IS NOT NULL AND @FromDate<>'')
		BEGIN
			SET @strSql=@strSql+ ' and dtStartTime  >= '''+ convert(varchar(30),@FromDate)+''' and dtStartTime  <= '''+ convert(varchar(30),@ToDate)  +''''
		END
	END
  END
  else
    SET @strSql=@strSql+ ' and c.bitTask=' + CONVERT(varchar(12),@filter)  
		IF(@FromDate IS NOT NULL AND @FromDate<>'')
		BEGIN
			SET @strSql=@strSql+ ' and ((dtStartTime  >= '''+ convert(varchar(30),@FromDate)+''' and dtStartTime  <= '''+ convert(varchar(30),@ToDate)  +''') OR (dtEndTime  >= '''+ convert(varchar(30),@FromDate)+''' and dtEndTime  <= '''+ convert(varchar(30),@ToDate)  +'''))'
		END
  IF @tintMode=1 SET @strSql=@strSql+ ' and  (CR.tintCorrType IN(1,3) OR '+ convert(varchar(15),@numOpenRecordID) +'=0)'
  IF @tintMode=2 SET @strSql=@strSql+ ' and  (CR.tintCorrType IN(2,4) OR '+ convert(varchar(15),@numOpenRecordID) +'=0)'
  IF @tintMode=5 SET @strSql=@strSql+ ' and  (CR.tintCorrType IN(5) OR '+ convert(varchar(15),@numOpenRecordID) +'=0)'
  IF @tintMode=6 SET @strSql=@strSql+ ' and  (CR.tintCorrType IN(6) OR '+ convert(varchar(15),@numOpenRecordID) +'=0)'
                          
  IF @tintSortOrder =0 and @SeachKeyword <>'' 
	SET @strSql=@strSql+ ' and textdetails like ''%'+@SeachKeyword+'%'''
  ELSE
	IF ISNULL(@filter,0) = 0 
	BEGIN
		IF @SeachKeyword <>''
		BEGIN
			SET @strSql=@strSql+ ' and textdetails like ''%'+@SeachKeyword+'%'''
		END
	END
	ELSE
	BEGIN
		SET @strSql=@strSql+ ' and c.bitTask=' + CONVERT(varchar(12),@filter)
		
		IF @SeachKeyword <>''
		BEGIN
			SET @strSql= @strSql + ' and textdetails like ''%'+@SeachKeyword+'%'''
		END                     
	END
  IF @bitOpenCommu=1  SET @strSql=@strSql+ ' and isnull(C.bitClosedflag,0)=0'
 end                




 --set @strSql='With tblCorr as (SELECT ROW_NUMBER() OVER (ORDER BY '+@columnName+' '+ @columnSortOrder+') AS RowNumber,X.* from('+@strSql+')X)'                

 set @strSql= CONCAT(@strSql,'; SELECT ROW_NUMBER() OVER(ORDER BY bintCreatedOn DESC) RowNumber, * FROM #TEMPData ORDER BY bintCreatedOn DESC OFFSET ',((@CurrentPage-1) * @PageSize),' ROWS FETCH NEXT ',@PageSize,' ROWS ONLY;')
                    

print CAST(@strSql AS NTEXT)          

set @strSql = CONCAT(@strSql,'; SET @TotRecs = ISNULL((SELECT SUM(numTotalRecords) FROM (SELECT tintCountType,numTotalRecords FROM #TEMPData GROUP BY tintCountType,numTotalRecords) X),0)')
EXEC sp_executesql @strSql,N'@TotRecs INT OUTPUT',@TotRecs OUTPUT;
                   
IF OBJECT_ID(N'tempdb..#TEMPData') IS NOT NULL
BEGIN
	DROP TABLE #TEMPData
END
                
              
end              
END
GO

