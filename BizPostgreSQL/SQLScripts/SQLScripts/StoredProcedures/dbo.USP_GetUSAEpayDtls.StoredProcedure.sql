
GO
/****** Object:  StoredProcedure [dbo].[USP_GetUSAEpayDtls]    Script Date: 05/07/2009 21:57:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj  
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getusaepaydtls')
DROP PROCEDURE usp_getusaepaydtls
GO
CREATE PROCEDURE [dbo].[USP_GetUSAEpayDtls]
@numDomainID as numeric(9),
@intPaymentGateway AS INT=0,
@numSiteId as int
as  
BEGIN 
	IF @intPaymentGateway =0 
	BEGIN
		IF ISNULL(@numSiteId,0) > 0 AND (SELECT ISNULL(intPaymentGateWay,0) FROM Sites WHERE numDomainId=@numDomainID AND numSiteID=@numSiteID) > 0
		BEGIN
			SELECT 
				S.intPaymentGateWay,
				ISNULL(vcFirstFldValue,'') AS vcFirstFldValue,
				ISNULL(vcSecndFldValue,'') AS vcSecndFldValue,
				ISNULL(vcThirdFldValue,'') AS vcThirdFldValue,
				ISNULL(bitTest,0) AS bitTest,
				ISNULL(D.bitSaveCreditCardInfo,0) AS bitSaveCreditCardInfo
			FROM 
				Sites S
			INNER JOIN
				Domain D
			ON
				S.numDomainID = D.numDomainId
			LEFT JOIN 
				PaymentGatewayDTLID PD
			ON 
				PD.intPaymentGateWay = S.intPaymentGateWay
			WHERE 
				S.numDomainID = @numDomainID
				AND PD.numDomainID = @numDomainID
  				AND ISNULL(PD.numSiteId,0)=@numSiteId
		END
		ELSE
		BEGIN
			SELECT 
				D.intPaymentGateWay,
				ISNULL(vcFirstFldValue,'') AS vcFirstFldValue,
				ISNULL(vcSecndFldValue,'') AS vcSecndFldValue,
				ISNULL(vcThirdFldValue,'') AS vcThirdFldValue,
				ISNULL(bitTest,0) AS bitTest,
				ISNULL(D.bitSaveCreditCardInfo,0) AS bitSaveCreditCardInfo
			FROM 
				Domain D
			LEFT JOIN 
				PaymentGatewayDTLID PD
			ON 
				PD.intPaymentGateWay = D.intPaymentGateWay
			WHERE 
				D.numDomainID = @numDomainID
				AND PD.numDomainID = @numDomainID
  				AND ISNULL(PD.numSiteId,0)=0
		END
	
	END
	ELSE IF @intPaymentGateway >0 
	BEGIN
	SELECT intPaymentGateWay,
		isnull(vcFirstFldValue,'') AS vcFirstFldValue,
		isnull(vcSecndFldValue,'') AS vcSecndFldValue,
		isnull(vcThirdFldValue,'') AS vcThirdFldValue,
		isnull(bitTest,0) AS bitTest
	FROM   PaymentGatewayDTLID PD
	WHERE  
		PD.numDomainID = @numDomainID
		AND PD.intPaymentGateWay = @intPaymentGateway
		AND PD.numSiteId=@numSiteId
	END
END
GO	   

