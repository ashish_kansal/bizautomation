/****** Object:  StoredProcedure [dbo].[USP_HiereachyChartOfAcntDetails]    Script Date: 07/26/2008 16:18:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created By Siva         
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_hiereachychartofacntdetails')
DROP PROCEDURE usp_hiereachychartofacntdetails
GO
CREATE PROCEDURE [dbo].[USP_HiereachyChartOfAcntDetails]               
@numParentAcntID as numeric(9)=0,        
@numDomainID as numeric(9)=0               
As              
Begin        
 If @numParentAcntID=1         
  Set @numParentAcntID=(Select numAccountId From Chart_Of_Accounts Where numParntAcntTypeId is null and numAcntTypeID is null and numDomainId = @numDomainId) --and numAccountId = 1         
  print @numParentAcntID    
 If @numParentAcntID is not null    
  Begin      
   print 'SP'    
   Delete from HierChartOfAct where numDomainID=@numDomainID              
   exec USP_ArranageHierChartAct @numParentAcntID,@numDomainID              
     End    
  select numChartOfAcntID as numChartOfAcntID,vcCategoryName from HierChartOfAct              
  where numDomainID=@numDomainID and  numChartOfAcntID<> @numParentAcntID   
End
GO
