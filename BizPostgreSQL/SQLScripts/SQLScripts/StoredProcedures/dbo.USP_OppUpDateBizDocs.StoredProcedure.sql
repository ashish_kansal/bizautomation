/****** Object:  StoredProcedure [dbo].[USP_OppUpDateBizDocs]    Script Date: 07/26/2008 16:20:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[USP_OppUpDateBizDocs]                    
(                    
 @byteMode as tinyint=null,                    
 @numOppBizDocsId as numeric(9)=null,                    
 @vcPurchaseOdrNo as varchar(100)=null,                    
 @numBizDocID as numeric(9)=null,                    
 @decDiscount as float,                    
 @userId as numeric(9) =null,                    
 @amountPaid as money=null,                    
 @numTerms as numeric(9)=null,                    
 @Comments as varchar(1000)=null,            
 @numContactID as numeric(9)=0,    
 @numPaymentMethod as tinyint=0,    
 @numDepoistToChartAcntId as numeric(9)=0,  
 @numDomainId as numeric(9)=0,
 @vcReference as varchar(50)=null,
 @vcMemo as varchar(50)=null
)                    
as                    
if @byteMode=0                    
begin                
 declare @numOppID as numeric(9)          
 select @numOppID=numOppId from OpportunityBizDocs where numOppBizDocsId=@numOppBizDocsId          
update OpportunityMaster set decDiscount=@decDiscount where numOppID= @numOppID               
update OpportunityBizDocs                     
 set  vcPurchaseOdrNo=@vcPurchaseOdrNo,                    
  decDiscount=@decDiscount,                    
  numModifiedBy=@userId,                    
  vcComments=@Comments,                    
 dtModifiedDate=getutcdate()                    
 where numOppBizDocsId=@numOppBizDocsId                    
    
    
    
end                    
if @byteMode=1                    
begin                    
update OpportunityBizDocs                    
 set tintFinal=1,                    
 vcPurchaseOdrNo=@vcPurchaseOdrNo,                    
 decDiscount=@decDiscount,                    
 numApprovedBy=@userId,                    
 dtApprovedDate=getutcdate()                    
 where numOppBizDocsId=@numOppBizDocsId                    
                    
end                    
if @byteMode=2                    
begin                    
update OpportunityBizDocs                    
 set numViewedBy=@numContactID,                    
 dtViewedDate=getutcdate()                    
 where numOppBizDocsId=@numOppBizDocsId                    
                    
end                    
                    
if @byteMode=3                    
begin                    
                  
declare @OppID as numeric(9)                  
select @OppID=numOppId from OpportunityBizDocs where numOppBizDocsId=@numOppBizDocsId                  
update OpportunityBizDocs                    
 set monAmountPaid=@amountPaid,                    
 numModifiedBy=@userId,                    
 dtModifiedDate=getutcdate()                  
 where numOppId=@OppID                  
update OpportunityMaster set  numPClosingPercent=@amountPaid  where numOppId=@OppID                
    
  -- To insert Payment Details in  OpportunityBizDocsDetails Table for Accounting Purpose       
 Insert into OpportunityBizDocsDetails(numBizDocsId,numPaymentMethod,monAmount,numDepoistToChartAcntId,numDomainId,vcReference,vcMemo)    
 Values(@numOppBizDocsId,@numPaymentMethod,@amountPaid,@numDepoistToChartAcntId,@numDomainId,@vcReference,@vcMemo)    
                  
end                    
                    
if @byteMode=4                    
begin                    
update OpportunityBizDocs                    
 set numTerms=@numTerms,                    
 numModifiedBy=@userId,                    
 dtModifiedDate=getutcdate()                    
 where numOppBizDocsId=@numOppBizDocsId                    
                    
end
GO
