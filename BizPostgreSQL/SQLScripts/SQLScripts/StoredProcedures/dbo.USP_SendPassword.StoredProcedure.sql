/****** Object:  StoredProcedure [dbo].[USP_SendPassword]    Script Date: 07/26/2008 16:21:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_sendpassword')
DROP PROCEDURE usp_sendpassword
GO
CREATE PROCEDURE [dbo].[USP_SendPassword]
@vcEmail as varchar(100),
@vcPassword as varchar(500) output,
@NoOfRows as tinyint OUTPUT,
@bitPortal AS BIT=1,
@numContactID NUMERIC=0 OUTPUT,
@numDomainID AS NUMERIC 
as
BEGIN
	IF @bitPortal=1
	BEGIN
		SELECT 
			@NoOfRows=COUNT(*) 
		FROM 
			ExtranetAccountsDtl E
		JOIN 
			AdditionalContactsInformation A
		ON 
			E.numContactID=A.numContactID 
		WHERE 
			vcEmail=@vcEmail
			AND A.numDomainID=@numDomainID

		IF @NoOfRows = 1
		BEGIN
			DECLARE @ResetLinkID UNIQUEIDENTIFIER
			SET @ResetLinkID = NEWID()  

			UPDATE 
				E
			SET
				vcResetLinkID = CAST(@ResetLinkID AS VARCHAR(255))
				,vcResetLinkCreatedTime = GETUTCDATE()
			FROM
				ExtranetAccountsDtl	E
			JOIN 
				AdditionalContactsInformation A
			ON
				E.numContactID=A.numContactID 
			WHERE
				vcEmail=@vcEmail
				AND A.numDomainID=@numDomainID
		END

		SELECT TOP 1 
			@vcPassword=ISNULL(vcResetLinkID,'')
			,@numContactID=A.numContactId
		FROM 
			ExtranetAccountsDtl E
		JOIN 
			AdditionalContactsInformation A
		ON
			E.numContactID=A.numContactID 
		WHERE 
			vcEmail=@vcEmail
			AND A.numDomainID=@numDomainID 
	END
	ELSE
	BEGIN
		select top 1 @vcPassword=isnull([vcPassword],''),@numContactID=UM.numUserDetailId from [UserMaster] UM
		where [vcEmailID]=@vcEmail
	--	AND UM.numDomainID=@numDomainID 

		select @NoOfRows=count(*) from [UserMaster] UM
		where [vcEmailID]=@vcEmail
	--	AND UM.numDomainID=@numDomainID 
	
	END
END
GO

