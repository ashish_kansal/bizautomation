/****** Object:  StoredProcedure [dbo].[usp_SetAllSelectedSystemModulesPagesAndAccesses]    Script Date: 07/26/2008 16:21:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_setallselectedsystemmodulespagesandaccesses')
DROP PROCEDURE usp_setallselectedsystemmodulespagesandaccesses
GO
CREATE PROCEDURE [dbo].[usp_SetAllSelectedSystemModulesPagesAndAccesses]  
 @numModuleID NUMERIC(9) = 0,  
 @numGroupID NUMERIC(9) = 0,  
 @numPageID NUMERIC(9) = 0,  
 @vcColumnName VARCHAR(30) = '',  
 @vcColumnValue SMALLINT,
 @numDomainID NUMERIC(9)
--  
AS  
  BEGIN  
   DECLARE @bitCustomRelationship BIT
   DECLARE @vcQuerString VARCHAR(200)  
   IF (SELECT COUNT(*) FROM GroupAuthorization WHERE (numGroupID = @numGroupID) AND (numModuleID = @numModuleID) AND (numPageID = @numPageID) AND numDomainID=@numDomainID) > 0  
    BEGIN  
		IF (SELECT COUNT(*) FROM PageMaster WHERE numModuleID = @numModuleID AND numPageID = @numPageID) > 0
		BEGIN
			SET @bitCustomRelationship = 0
		END
		ELSE
		BEGIN
			SET @bitCustomRelationship = 1
		END

     --PRINT 'UPDATE'  
     SELECT @vcQuerString = 'UPDATE GroupAuthorization SET '  + @vcColumnName + ' = ' + CONVERT(CHAR, @vcColumnValue) + ',bitCustomRelationship =' + CONVERT(VARCHAR, @bitCustomRelationship) + '  WHERE (numGroupID = ' + CONVERT(VARCHAR, @numGroupID) + ') AND (numModuleID =' + CONVERT(VARCHAR, @numModuleID) + ') AND (numPageID =' + CONVERT(VARCHAR, @numPageID) + ') AND numDomainID=' + CONVERT(VARCHAR,@numDomainID)
    END  
   ELSE  
    BEGIN  

		IF (SELECT COUNT(*) FROM PageMaster WHERE numModuleID = @numModuleID AND numPageID = @numPageID) > 0
		BEGIN
			SET @bitCustomRelationship = 0
		END
		ELSE
		BEGIN
			SET @bitCustomRelationship = 1
		END

     --PRINT 'INSERT'  
     SELECT @vcQuerString = 'INSERT INTO GroupAuthorization (numGroupID, numModuleID, numPageID,numDomainID,bitCustomRelationship, ' + @vcColumnName +') VALUES(' + CONVERT(VARCHAR, @numGroupID) + ', ' + CONVERT(VARCHAR, @numModuleID) + ', ' + CONVERT(VARCHAR, @numPageID) + ', ' + CONVERT(VARCHAR,@numDomainID) + ', ' + CONVERT(VARCHAR, @bitCustomRelationship) + ', ' + CONVERT(
VARCHAR, @vcColumnValue) + ')'  
    END  
   PRINT @vcQuerString  
   EXEC (@vcQuerString)  
  END
GO
