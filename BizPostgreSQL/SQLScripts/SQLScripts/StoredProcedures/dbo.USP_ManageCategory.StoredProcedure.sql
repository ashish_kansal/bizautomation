/****** Object:  StoredProcedure [dbo].[USP_ManageCategory]    Script Date: 07/26/2008 16:19:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj        
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_managecategory')
DROP PROCEDURE usp_managecategory
GO
CREATE PROCEDURE [dbo].[USP_ManageCategory]        
	@numCatergoryId as numeric(9),        
	@vcCatgoryName as varchar(1000),       
	@numDomainID as numeric(9)=0,
	@vcDescription as VARCHAR(MAX),
	@intDisplayOrder AS INT ,
	@vcPathForCategoryImage AS VARCHAR(100),
	@numDepCategory AS NUMERIC(9,0),
	@numCategoryProfileID AS NUMERIC(18,0),
	@vcCatgoryNameURL VARCHAR(1000),
	@vcMetaTitle as varchar(1000),
	@vcMetaKeywords as varchar(1000),
	@vcMetaDescription as varchar(1000),
	@vcMatrixGroups VARCHAR(300)
AS
BEGIN
	DECLARE @tintLevel AS TINYINT
	SET @tintLevel = 1

	IF	@numDepCategory <> 0
	BEGIN
		SELECT @tintLevel = (tintLevel + 1) FROM dbo.Category WHERE numCategoryID = @numDepCategory
	END       
       
	IF @numCatergoryId=0        
	BEGIN
		INSERT INTO Category 
		(
			vcCategoryName,
			tintLevel,
			numDomainID,
			vcDescription,
			intDisplayOrder,
			vcPathForCategoryImage,
			numDepCategory,
			numCategoryProfileID,
			vcCategoryNameURL,
			vcMetaTitle,
			vcMetaKeywords,
			vcMetaDescription
		)        
		VALUES
		(
			@vcCatgoryName,
			@tintLevel,
			@numDomainID,
			@vcDescription,
			@intDisplayOrder,
			@vcPathForCategoryImage,
			@numDepCategory,
			@numCategoryProfileID,
			@vcCatgoryNameURL,
			@vcMetaTitle,
			@vcMetaKeywords,
			@vcMetaDescription
		) 
		set @numCatergoryId = SCOPE_IDENTITY();   
		insert into SiteCategories 
		(
			numSiteID,
			numCategoryID
		)
		SELECT
			numSiteID
			,@numCatergoryId
		FROM
			CategoryProfileSites
		WHERE
			 numCategoryProfileID=@numCategoryProfileID    
	END        
	ELSE IF @numCatergoryId>0        
	BEGIN
		UPDATE 
			Category 
		SET 
			vcCategoryName=@vcCatgoryName,
			vcDescription=@vcDescription,
			intDisplayOrder = @intDisplayOrder,
			tintLevel = @tintLevel,
			vcPathForCategoryImage = @vcPathForCategoryImage,
			numDepCategory = @numDepCategory,
			vcCategoryNameURL=@vcCatgoryNameURL,
			vcMetaTitle=@vcMetaTitle,
			vcMetaKeywords=@vcMetaKeywords,
			vcMetaDescription=@vcMetaDescription
		WHERE 
			numCategoryID=@numCatergoryId        
	END

	IF ISNULL(@vcMatrixGroups,'') = ''
	BEGIN
		DELETE FROM CategoryMatrixGroup WHERE numDomainID=@numDomainID AND numCategoryID=@numCatergoryId
	END
	ELSE
	BEGIN
		DELETE FROM CategoryMatrixGroup WHERE numDomainID=@numDomainID AND numCategoryID=@numCatergoryId AND numItemGroup NOT IN (SELECT Id FROM dbo.SplitIDs(@vcMatrixGroups,','))
	END

	INSERT INTO CategoryMatrixGroup
	(
		numDomainID,numCategoryID,numItemGroup
	)
	SELECT
		@numDomainID,@numCatergoryId,Temp.Id
	FROM
		dbo.SplitIDs(@vcMatrixGroups,',') Temp
	WHERE 
		Temp.Id NOT IN (SELECT numItemGroup FROM CategoryMatrixGroup WHERE numDomainID=@numDomainID AND numCategoryID=@numCatergoryId)
END	
GO