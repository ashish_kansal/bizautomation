/****** Object:  StoredProcedure [dbo].[USP_PopulateCustomFlds]    Script Date: 07/26/2008 16:20:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_populatecustomflds')
DROP PROCEDURE usp_populatecustomflds
GO
CREATE PROCEDURE [dbo].[USP_PopulateCustomFlds]      
@numRelation as numeric(9)=0,  
@numContactType as numeric(9)=0       
as        
if @numRelation<>0 and @numContactType=0  
begin  
select 1as Type,fld_id,fld_label from CFW_Fld_Master join CFW_Fld_Dtl        
on Fld_id=numFieldId          
where (Fld_type='TextBox' or Fld_type='TextArea') and Grp_id=1 and numRelation=@numRelation  
end  
if @numRelation=0 and @numContactType<>0  
begin  
select 2 as Type ,fld_id,fld_label from CFW_Fld_Master join CFW_Fld_Dtl        
on Fld_id=numFieldId          
where (Fld_type='TextBox' or Fld_type='TextArea') and Grp_id=4 and numRelation=@numContactType  
end  
if @numRelation<>0 and @numContactType<>0  
begin  
select 1 as Type,fld_id,fld_label from CFW_Fld_Master join CFW_Fld_Dtl        
on Fld_id=numFieldId          
where (Fld_type='TextBox' or Fld_type='TextArea') and Grp_id=1 and numRelation=@numRelation  
union  
select 2 as Type,fld_id,fld_label from CFW_Fld_Master join CFW_Fld_Dtl        
on Fld_id=numFieldId          
where (Fld_type='TextBox' or Fld_type='TextArea') and Grp_id=4 and numRelation=@numContactType  
end

if @numRelation=0 and @numContactType=0  
begin  
select 2 as Type ,fld_id,fld_label from CFW_Fld_Master join CFW_Fld_Dtl        
on Fld_id=numFieldId          
where (Fld_type='TextBox' or Fld_type='TextArea') and Grp_id=4 and numRelation=@numContactType  
end
GO
