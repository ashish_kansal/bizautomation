/****** Object:  StoredProcedure [dbo].[usp_GetOpptListDetails]    Script Date: 07/26/2008 16:18:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getopptlistdetails')
DROP PROCEDURE usp_getopptlistdetails
GO
CREATE PROCEDURE [dbo].[usp_GetOpptListDetails]
	@numUserID numeric(9)=0,
	@numDomainID numeric(9)=0,
	@tintUserRightType tinyint=0,
	@tintSortOrder tinyint=4,
	@intLastDate int=0,
	@SortChar char(1)='0',
        @FirstName varChar(100)= '',
        @LastName varchar(100)='',
        @CustName varchar(100)='' ,
	@OppType as tinyint,
	@CurrentPage int,
	@PageSize int,
	@TotRecs int output,
	@columnName as Varchar(50),
	@columnSortOrder as Varchar(10)
as

--Create a Temporary table to hold data
Create table #tempTable ( ID INT IDENTITY PRIMARY KEY, 
     	numOppId VARCHAR(15),
     	Name varchar(15),
     	STAGE VARCHAR(100),
     	CloseDate varchar(15),
	numcreatedby varchar(15),
	Contact varchar(110),
	Company varchar(100),
	numCompanyID varchar(15),
	tintCRMType varchar(10),
	numContactID varchar(15),
	numDivisionID  varchar(15),
	numTerID varchar(15),
	monPAmount varchar(25),
	vcusername varchar(50)
	)
declare @strSql as varchar(8000)
set @strSql='SELECT  Opp.numOppId, 
		Opp.vcPOppName AS Name, 
		dbo.GetOppStatus(Opp.numOppId) as STAGE, 
		Opp.intPEstimatedCloseDate AS CloseDate, 
		Opp.numcreatedby ,
		ADC.vcFirstName + '' '' + ADC.vcLastName AS Contact, 
		C.vcCompanyName AS Company,
                C.numCompanyID,
		ADC.tintCRMType,
		ADC.numContactID,
		Div.numDivisionID,
		Div.numTerID,
		Opp.monPAmount,
		usermaster.vcusername 
		FROM OpportunityMaster Opp 
		INNER JOIN AdditionalContactsInformation ADC 
		ON Opp.numContactId = ADC.numContactId 
		INNER JOIN DivisionMaster Div 
		ON Opp.numDivisionId = Div.numDivisionID AND ADC.numDivisionId = Div.numDivisionID 
		INNER JOIN CompanyInfo C 
		ON Div.numCompanyID = C.numCompanyId
		INNER join usermaster on usermaster.numuserid=Opp.numcreatedby	
		WHERE Opp.tintOppstatus=0
		and ADC.numDomainID=' + convert(varchar(15),@numDomainID)+'
		AND Opp.numCreatedby =' + convert(varchar(15),@numUserID)+' 
		and Opp.tintOppType= ' + convert(varchar(15),@OppType)+'
		and ADC.vcFirstName  like '''+@FirstName+'%'' 
		and ADC.vcLastName like '''+@LastName+'%''
		and CMP.vcCompanyName like '''+@CustName+'%'''

if @SortChar<>'0' set @strSql=@strSql + ' And Opp.vcPOppName like '''+@SortChar+'%''' 
if @tintUserRightType=1 set @strSql=@strSql + ' AND (DM.numRecOwner = '+convert(varchar(15),@numUserID)+ ')' 	   
else if @tintUserRightType=2 set @strSql=@strSql + ' AND (DM.numTerID in (select numTerritoryID from  UserTerritory where numUserID= '+convert(varchar(15),@numUserID)+' ) or DM.numTerID=0) '				

	

insert into #tempTable(numOppId,
     	Name,
     	STAGE,
     	CloseDate,
	numcreatedby ,
	Contact,
	Company,
	numCompanyID,
	tintCRMType,
	numContactID,
	numDivisionID,
	numTerID,
	monPAmount,
	vcusername)
exec (@strSql) 

  declare @firstRec as integer
  declare @lastRec as integer
	set @firstRec= (@CurrentPage-1) * @PageSize
    	set @lastRec= (@CurrentPage*@PageSize+1)
select * from #tempTable where ID > @firstRec and ID < @lastRec
set @TotRecs=(select count(*) from #tempTable)
drop table #tempTable
GO
