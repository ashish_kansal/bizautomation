/****** Object:  StoredProcedure [dbo].[usp_SetModuleName]    Script Date: 07/26/2008 16:21:16 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_setmodulename')
DROP PROCEDURE usp_setmodulename
GO
CREATE PROCEDURE [dbo].[usp_SetModuleName]
	@vcModuleName VARCHAR(50),
	@numModuleID NUMERIC(9)   
--
AS
	BEGIN
		IF @numModuleID > 0 
			BEGIN
				UPDATE ModuleMaster SET vcModuleName = @vcModuleName			
				WHERE numModuleID = @numModuleID
			END
		ELSE
			BEGIN
				INSERT INTO ModuleMaster (vcModuleName)
				VALUES(@vcModuleName)

				SELECT SCOPE_IDENTITY()
			END
	END
GO
