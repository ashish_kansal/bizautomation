/****** Object:  StoredProcedure [dbo].[USP_GetTeamForUsrMst]    Script Date: 07/26/2008 16:18:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj  
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getteamforusrmst')
DROP PROCEDURE usp_getteamforusrmst
GO
CREATE PROCEDURE [dbo].[USP_GetTeamForUsrMst]    
@numUserCntID as numeric(9),    
@numDomainID as numeric(9)    
as    
select distinct(numlistitemid),vcdata from userteams    
join listdetails    
on numlistitemid=numteam    
where numUserCntID=@numUserCntID and numListID=35    
and userteams.numDomainID=@numDomainID
GO
