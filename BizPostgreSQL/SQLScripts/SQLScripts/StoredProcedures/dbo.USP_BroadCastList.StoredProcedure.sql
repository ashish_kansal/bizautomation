/****** Object:  StoredProcedure [dbo].[USP_BroadCastList]    Script Date: 07/26/2008 16:14:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
----ALTER  By Anoop Jayaraj        
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_broadcastlist')
DROP PROCEDURE usp_broadcastlist
GO
CREATE PROCEDURE [dbo].[USP_BroadCastList]          
 @numUserCntID numeric(9)=0,          
 @numDomainID numeric(9)=0,           
 @CurrentPage int,          
 @PageSize int,          
 @TotRecs int output,          
 @columnName as Varchar(50),          
 @columnSortOrder as Varchar(10)        
          
as          
        
 create table #tempTable ( ID INT IDENTITY PRIMARY KEY,           
      numBroadCastID varchar(15),      
      vcBroadCastName VARCHAR(100),          
      numBroadCastBy varchar(50),          
      bintBroadCastDate VARCHAR(20),          
      numTotalRecipients varchar(15),          
      UnsucessFull varchar(15),
      bitBroadcasted bit         
 )          
          
          
declare @strSql as varchar(8000)          
          
          
set @strSql=' select numBroadCastID,        
 convert(varchar(50),vcBroadCastName) as vcBroadCastName,        
 dbo.fn_GetContactName(numBroadCastBy) as numBroadCastBy,        
 bintBroadCastDate,numTotalRecipients,        
 (numTotalRecipients-numTotalSussessfull) as UnsucessFull,
 bitBroadcasted
          
from Broadcast where numDomainId='+convert(varchar(9),@numDomainId)+' order by bintBroadCastDate desc'        
--set @strSql=@strSql + ' ORDER BY ' + @columnName +' '+ @columnSortOrder        
  print @strSql        
insert into #tempTable( numBroadCastID,        
      vcBroadCastName,          
      numBroadCastBy,          
      bintBroadCastDate,          
      numTotalRecipients,          
      UnsucessFull,
      bitBroadcasted 
      )          
exec (@strSql)           
          
  declare @firstRec as integer           
  declare @lastRec as integer          
 set @firstRec= (@CurrentPage-1) * @PageSize          
     set @lastRec= (@CurrentPage*@PageSize+1)          
select *,isnull(convert(varchar(15),(select sum(numNoofTimes) from BroadCastDtls where BroadCastDtls.numBroadCastID=#tempTable.numBroadCastID)),'-') as numNoofTimes ,isnull(convert(varchar(15),(select sum(intNoOfClick) from BroadCastDtls where BroadCastDtls.numBroadCastID=#tempTable.numBroadCastID)),'-') as intNoOfClick,isnull(convert(varchar(15),(select sum(convert(int , bitUnsubscribe)) from BroadCastDtls where BroadCastDtls.numBroadCastID=#tempTable.numBroadCastID)),'-') as bitUnsubscribe,(select count(*) from BroadcastDtls where numBroadCastID = #tempTable.numBroadCastID) as numCountRecipients   from #tempTable where ID > @firstRec and ID < @lastRec 

set @TotRecs=(select count(*) from #tempTable)          
drop table #tempTable
GO
