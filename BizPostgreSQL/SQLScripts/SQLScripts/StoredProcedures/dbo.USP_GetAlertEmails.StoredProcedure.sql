/****** Object:  StoredProcedure [dbo].[USP_GetAlertEmails]    Script Date: 07/26/2008 16:16:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--- Created By Anoop Jayaraj    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getalertemails')
DROP PROCEDURE usp_getalertemails
GO
CREATE PROCEDURE [dbo].[USP_GetAlertEmails]    
@numAlertDTLID as numeric(9)=0,  
@numDomainID as numeric(9)=0    
as    
    
     
select numAlertEmailID,Alt.numAlertDTLid,vcEmailID from  AlertEmailDTL Alt  
where Alt.numAlertDTLId=@numAlertDTLID  and numDomainID=@numDomainID
GO
