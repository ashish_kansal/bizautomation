/****** Object:  StoredProcedure [dbo].[USP_UpdateSpecDocuments]    Script Date: 07/26/2008 16:21:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_updatespecdocuments')
DROP PROCEDURE usp_updatespecdocuments
GO
CREATE PROCEDURE [dbo].[USP_UpdateSpecDocuments]
@VcFileName as varchar(100),
@numSpecificDocID as numeric(9)
as
update SpecificDocuments set VcFileName=@VcFileName
where numSpecificDocID=@numSpecificDocID
GO
