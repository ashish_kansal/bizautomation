/****** Object:  StoredProcedure [dbo].[USP_ManageCampaignForRept]    Script Date: 07/26/2008 16:19:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_managecampaignforrept')
DROP PROCEDURE usp_managecampaignforrept
GO
CREATE PROCEDURE [dbo].[USP_ManageCampaignForRept]            
@numUserCntID as numeric(9),                     
@strTerritory as varchar(4000)            
as            
            
            
declare @separator_position as integer            
declare @strPosition as varchar(1000)            
            
delete from ForReportsByCampaign where numUserCntID = @numUserCntID                    
             
            
   while patindex('%,%' , @strTerritory) <> 0            
    begin -- Begin for While Loop            
      select @separator_position =  patindex('%,%' , @strTerritory)            
    select @strPosition = left(@strTerritory, @separator_position - 1)            
       select @strTerritory = stuff(@strTerritory, 1, @separator_position,'')            
     insert into ForReportsByCampaign (numUserCntID,numCampaignID)            
     values (@numUserCntID,@strPosition)            
             
              
   end
GO
