/****** Object:  StoredProcedure [dbo].[USP_LoadWarehouseAttributes]    Script Date: 07/26/2008 16:19:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--exec USP_LoadWarehouseAttributes 14,''  
--created by anoop jayaraj  
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_loadwarehouseattributes')
DROP PROCEDURE usp_loadwarehouseattributes
GO
CREATE PROCEDURE [dbo].[USP_LoadWarehouseAttributes]  
@numItemCode as varchar(20)='',  
@strAtrr as varchar(1000)=''  
as  
  
declare @bitSerialize as bit  
declare @strSQL as varchar(1000)  
select @bitSerialize=bitSerialized from item where numItemCode=@numItemCode  
  
if @strAtrr!=''  
begin  
Declare @Cnt int  
 Set @Cnt = 1  
declare @SplitOn char(1)  
set @SplitOn=','  
set @strSQL='SELECT recid  
      FROM CFW_Fld_Values_Serialized_Items where  bitSerialized ='+ convert(varchar(1),@bitSerialize)  
 While (Charindex(@SplitOn,@strAtrr)>0)  
 Begin  
  if  @Cnt=1 set @strSQL=@strSQL+' and fld_value=''' + (ltrim(rtrim(Substring(@strAtrr,1,Charindex(@SplitOn,@strAtrr)-1))))+''''  
  else set @strSQL=@strSQL+' or fld_value=''' + (ltrim(rtrim(Substring(@strAtrr,1,Charindex(@SplitOn,@strAtrr)-1))))+''''  
  Set @strAtrr = Substring(@strAtrr,Charindex(@SplitOn,@strAtrr)+1,len(@strAtrr))  
  Set @Cnt = @Cnt + 1  
 End  
set @strSQL=@strSQL+' or fld_value=''' + @strAtrr + ''' GROUP BY recid  
    HAVING count(*) > '+convert(varchar(10), @Cnt-1)  
  
  
end  
  
if @bitSerialize=0  
begin  
  if @strAtrr<>''  
   set @strSQL='select distinct(WareHouseItems.numWareHouseItemId),vcWareHouse,numOnHand,numOnOrder,numReorder,numAllocation,numBackOrder from WareHouseItems  
   join Warehouses on Warehouses.numWareHouseID=WareHouseItems.numWareHouseID  
   where numItemID='+@numItemCode+' and numWareHouseItemId in ('+@strSQL+')'  
  else if @strAtrr=''  
   set @strSQL='select distinct(WareHouseItems.numWareHouseItemId),vcWareHouse,numOnHand,numOnOrder,numReorder,numAllocation,numBackOrder from WareHouseItems  
   join Warehouses on Warehouses.numWareHouseID=WareHouseItems.numWareHouseID  
   where numItemID='+@numItemCode  
 exec(@strSQL)  
end  
else if @bitSerialize=1  
begin  
 if @strAtrr<>''  
  set @strSQL='select distinct(WareHouseItems.numWareHouseItemId),vcWareHouse,numOnHand,numOnOrder,numReorder,numAllocation,numBackOrder from WareHouseItems   
  join WareHouseItmsDTL on WareHouseItmsDTL.numWareHouseItemID=WareHouseItems.numWareHouseItemID  
  join Warehouses on Warehouses.numWareHouseID=WareHouseItems.numWareHouseID  
  where numItemID='+@numItemCode+' and numWareHouseItmsDTLID in ('+@strSQL+')'  
 else if @strAtrr=''  
  set @strSQL='select distinct(WareHouseItems.numWareHouseItemId),vcWareHouse,numOnHand,numOnOrder,numReorder,numAllocation,numBackOrder from WareHouseItems   
  join WareHouseItmsDTL on WareHouseItmsDTL.numWareHouseItemID=WareHouseItems.numWareHouseItemID  
  join Warehouses on Warehouses.numWareHouseID=WareHouseItems.numWareHouseID  
  where numItemID='+@numItemCode  
 exec(@strSQL)  
end  
else select 0  
  
print @strSQL
GO
