/****** Object:  StoredProcedure [dbo].[usp_GetListType]    Script Date: 07/26/2008 16:17:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getlisttype')
DROP PROCEDURE usp_getlisttype
GO
CREATE PROCEDURE [dbo].[usp_GetListType]    
@numListItemId numeric    --
as    
select vcData from listdetails where numListid = 5 and numListItemID = @numListItemId
GO
