GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
       
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_extranetcompanies')
DROP PROCEDURE usp_extranetcompanies
GO
CREATE PROCEDURE [dbo].[USP_ExtranetCompanies]
(       
	@numDomainID NUMERIC(18,0)
	,@CurrentPage INT
	,@PageSize INT       
	,@columnName VARCHAR(100)        
	,@columnSortOrder VARCHAR(100)
	,@vcCustomSearch VARCHAR(MAX)
)
AS
BEGIN
	DECLARE @strSql VARCHAR(MAX)            

	SET @strSql =CONCAT('SELECT 
							COUNT(*) OVER() numTotalRecords
							,Ext.numExtranetID
							,ExtDTL.numExtranetDtlID
							,CONCAT(''<a href='',(CASE WHEN tintCRMType=2 THEN CONCAT(''../account/frmAccounts.aspx?DivID='',Div.numDivisionID,'''') WHEN tintCRMType=1 THEN CONCAT(''../prospects/frmProspects.aspx?DivID='',Div.numDivisionID,'''') ELSE CONCAT(''../Leads/frmLeads.aspx?DivID='',Div.numDivisionID,'''') END),''>'',ISNULL(vcCompanyName,''-''),''</a>'') vcCompanyName
							,ISNULL(A.vcFirstName,'''') vcFirstName
							,ISNULL(A.vcLastName,'''') vcLastName
							,ISNULL(A.vcEmail,'''') vcEmail
							,ISNULL(ExtDTL.vcPassword,'''') vcPassword
							,ISNULL(ExtDTL.tintAccessAllowed,0) tintAccessAllowed
							,ISNULL(ExtDTL.bitPartnerAccess,0) bitPartnerAccess
							,ISNULL((SELECT COUNT(*) FROM UserAccessedDTL UAD WHERE UAD.numDomainID=',@numDomainID,' AND UAD.numDivisionID=Div.numDivisionID AND UAD.numContactID=A.numContactID),0) intLoginCount
							,CONCAT(''Contact~'',(SELECT TOP 1 numFieldID FROM DycFieldMaster WHERE vcLookBackTableName=''ExtranetAccountsDtl'' AND vcOrigDbColumnName=''vcPassword''),''~False~'',A.numContactID,''~'',Div.numDivisionID,''~0~0'') vcPasswordInlineEditID
						FROM 
							ExtarnetAccounts Ext      
						INNER JOIN
							ExtranetAccountsDtl ExtDTL 
						ON
							ExtDTL.numExtranetID=Ext.numExtranetID    
						INNER JOIN 
							DivisionMaster Div 
						ON 
							Div.numDivisionID=Ext.numDivisionID            
						INNER JOIN 
							CompanyInfo Com 
						ON 
							Com.numCompanyID=Ext.numCompanyID
						INNER JOIN 
							AdditionalContactsInformation A
						ON 
							A.numDivisionID = Ext.numDivisionID 
							AND A.numContactID = ExtDTL.numContactID            
						WHERE 
							Ext.numDomainID=',@numDomainID)
		
	IF LEN(ISNULL(@vcCustomSearch,'')) > 0
	BEGIN
		SET @strSql = CONCAT(@strSql,' AND ',@vcCustomSearch)
	END	
						
	IF ISNULL(@columnName,'') <> '' 
	BEGIN
		IF @columnName = 'Organization'
		BEGIN
			SET @columnName = 'Com.vcCompanyName'
		END
		ELSE IF @columnName = 'FirstName'
		BEGIN
			SET @columnName = 'A.vcFirstName'
		END
		ELSE IF @columnName = 'LastName'
		BEGIN
			SET @columnName = 'A.vcLastName'
		END
		ELSE IF @columnName = 'EmailAddress'
		BEGIN
			SET @columnName = 'A.vcEmail'
		END
		ELSE IF @columnName = 'LoginActivity'
		BEGIN
			SET @columnName = CONCAT('ISNULL((SELECT COUNT(*) FROM UserAccessedDTL UAD WHERE UAD.numDomainID=',@numDomainID,' AND UAD.numDivisionID=Div.numDivisionID AND UAD.numContactID=A.numContactID),0)')
		END

		SET @strSql = CONCAT(@strSql,' ORDER BY ',@columnName,' ',@columnSortOrder)
	END
	ELSE
	BEGIN
		SET @strSql = CONCAT(@strSql,' ORDER BY vcCompanyName ASC')
	END

	SET @strSql = CONCAT(@strSql,' OFFSET ',((@CurrentPage-1) * @PageSize),' ROWS FETCH NEXT ',@PageSize,' ROWS ONLY')

	PRINT CAST(@strSql AS NTEXT)

	EXEC (@strSql) 
END
GO