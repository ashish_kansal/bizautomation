/****** Object:  StoredProcedure [dbo].[USP_EmployeeList]    Script Date: 07/26/2008 16:15:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- Created By Anoop Jayaraj    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_employeelist')
DROP PROCEDURE usp_employeelist
GO
CREATE PROCEDURE [dbo].[USP_EmployeeList]      
@numDomainID as numeric(9)      
as      
SELECT A.numContactID,ISNULL(A.vcFirstName,'')+' '+ISNULL(A.vcLastName,'') as vcUserName
from UserMaster UM   
join AdditionalContactsInformation A  
on UM.numUserDetailId=A.numContactID       
where UM.numDomainID=@numDomainID
GO
