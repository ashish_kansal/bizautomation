/****** Object:  StoredProcedure [dbo].[usp_GetCompanyPrimaryContactDTl]    Script Date: 07/26/2008 16:16:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created By                                  
GO 
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'usp_getcompanyprimarycontactdtl' ) 
    DROP PROCEDURE usp_getcompanyprimarycontactdtl
GO
CREATE PROCEDURE [dbo].[usp_GetCompanyPrimaryContactDTl]
    @numDivisionID NUMERIC = 0                             
--                                                    
AS 
BEGIN                                                      
                                                      
    SELECT  A.vcFirstName,
            A.vcLastName,
            ( A.numPhone + ',' + A.numPhoneExtension ) AS numPhone,
            A.vcEmail,
            C.vcCompanyName,
            A.vcFax,
            A.charSex,
            A.bintDOB,
            dbo.GetAge(A.bintDOB, GETUTCDATE()) AS Age,
            A.txtNotes,
            A.NumHomePhone,
            ( A.vcAsstFirstName + ' ' + A.vcAsstLastName ) AS vcAsstName,
            ( A.numAsstPhone + ',' + A.numAsstExtn ) AS numAsstPhone,
            A.vcAsstEmail,
            A.charSex,
		   isnull(vcStreet,'') + isnull(vcCity,'') + ' ,'
			+ isnull(dbo.fn_GetState(numState),'') + ' '
			+ isnull(vcPostalCode,'') + ' <br>'
			+ isnull(dbo.fn_GetListItemName(numCountry),'') [Address],
            C.vcWebSite
    FROM    AdditionalContactsInformation A
            INNER JOIN DivisionMaster D ON A.numDivisionId = D.numDivisionID
            INNER JOIN CompanyInfo C ON D.numCompanyID = C.numCompanyId
            LEFT JOIN dbo.AddressDetails AD ON AD.numRecordID = A.numContactId
                                               AND AD.tintAddressOf = 1
                                               AND AD.tintAddressType = 0
                                               AND AD.bitIsPrimary = 1
                                               AND AD.numDomainID = A.numDomainID
            LEFT JOIN UserMaster U ON U.numUserDetailId = A.numContactId
    WHERE   A.numContactId IN ( SELECT  numrecowner
                                FROM    CompanyInfo CMP INNER JOIN DivisionMaster DM ON CMP.numCompanyId = Dm.numCompanyID
                                WHERE   DM.numDivisionID = @numDivisionID)                                              
END
GO
