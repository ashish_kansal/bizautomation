/****** Object:  StoredProcedure [dbo].[usp_GetPageElements]    Script Date: 07/26/2008 16:18:08 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getpageelements')
DROP PROCEDURE usp_getpageelements
GO
CREATE PROCEDURE [dbo].[usp_GetPageElements]
	@numPageID NUMERIC(9) = 0,
	@numModuleID  NUMERIC(9) = 0,
	@numUserID  NUMERIC(9) = 0   
--
AS
	BEGIN
		IF @numUserID <> 0
			BEGIN
				SELECT  PageActionElements.vcActionName, PageActionElements.vcElementInitiatingAction, PageActionElements.vcElementInitiatingActionType, 
				PageActionElements.vcChildElementInitiatingAction, PageActionElements.vcChildElementInitiatingActionType, 
				GroupAuthorization.intViewAllowed, GroupAuthorization.intAddAllowed, GroupAuthorization.intUpdateAllowed, 
				GroupAuthorization.intDeleteAllowed, GroupAuthorization.intExportAllowed
				FROM         GroupAuthorization INNER JOIN
				PageActionElements ON GroupAuthorization.numModuleID = PageActionElements.numModuleID AND 
				GroupAuthorization.numPageID = PageActionElements.numPageID INNER JOIN
				UserGroups ON GroupAuthorization.numGroupID = UserGroups.numGroupID
				WHERE     (PageActionElements.numPageID = @numPageID) AND (PageActionElements.numModuleID = @numModuleID) AND (UserGroups.numUserID = @numUserID)
			END
	END
GO
