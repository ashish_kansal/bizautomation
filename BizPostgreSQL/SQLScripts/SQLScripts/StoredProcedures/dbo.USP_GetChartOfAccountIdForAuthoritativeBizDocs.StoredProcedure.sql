/****** Object:  StoredProcedure [dbo].[USP_GetChartOfAccountIdForAuthoritativeBizDocs]    Script Date: 07/26/2008 16:16:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created By Siva
-- EXEC USP_GetChartOfAccountIdForAuthoritativeBizDocs 72,'AP'
--SELECT * FROM [AccountTypeDetail] WHERE numdomainID=72
--select * FROM [Chart_Of_Accounts] WHERE [numDomainId]=72
--SELECT * FROM [AccountingChargeTypes]
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getchartofaccountidforauthoritativebizdocs')
DROP PROCEDURE usp_getchartofaccountidforauthoritativebizdocs
GO
CREATE PROCEDURE [dbo].[USP_GetChartOfAccountIdForAuthoritativeBizDocs]
    @numDomainId AS NUMERIC(9) = 0,
    @chChargeCode CHAR(10)
AS 
    BEGIN
        SELECT  AC.[numAccountID]
        FROM    [AccountingChargeTypes] ACT
                INNER JOIN [AccountingCharges] AC ON ACT.[numChargeTypeId] = AC.[numChargeTypeId]
        WHERE   ACT.[chChargeCode] = @chChargeCode
                AND AC.[numDomainID] = @numDomainId
--	Select min(numAccountId) From Chart_of_Accounts Where numAcntType=@numAcntType And numDomainId=@numDomainId
    END
GO
