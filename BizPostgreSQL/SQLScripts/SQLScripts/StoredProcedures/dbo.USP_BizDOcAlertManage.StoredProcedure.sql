/****** Object:  StoredProcedure [dbo].[USP_BizDOcAlertManage]    Script Date: 07/26/2008 16:14:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--createb by anoop jayaraj  
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_bizdocalertmanage')
DROP PROCEDURE usp_bizdocalertmanage
GO
CREATE PROCEDURE [dbo].[USP_BizDOcAlertManage]  
@numBizDocID as numeric,  
@bitCreated as tinyint,  
@bitModified as tinyint,  
@numEmailTemplate as numeric,  
@bitOppOwner as tinyint,  
@bitCCManager as tinyint,  
@bitPriConatct as tinyint,
@bitApproved as TINYINT,
@numDomainID AS numeric
as  
  
insert into BizDocAlerts  
  (  
  numBizDocID,  
  bitCreated,  
  bitModified,
  bitApproved,  
  numEmailTemplate,  
  bitOppOwner,  
  bitCCManager,  
  bitPriConatct,
  numDomainID
  )  
  values  
  (  
  @numBizDocID,  
  @bitCreated,  
  @bitModified,  
  @bitApproved,
  @numEmailTemplate,  
  @bitOppOwner,  
  @bitCCManager,  
  @bitPriConatct,
  @numDomainID
  )
GO
