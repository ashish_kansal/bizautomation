/****** Object:  StoredProcedure [dbo].[usp_Insert_Update_groupTab]    Script Date: 07/26/2008 16:18:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_insert_update_grouptab')
DROP PROCEDURE usp_insert_update_grouptab
GO
CREATE PROCEDURE [dbo].[usp_Insert_Update_groupTab]

	--Input Parameter List

	@numGroupId numeric=0,
	@bitstatus varchar(100)=' '
	
--
AS		
declare @var1 numeric
	--Updating the field in LeadBox Table
begin 

delete from grouptabdetails where numGroupId = @numGroupId

insert into grouptabdetails(numgroupid, numtabid, bitallowed) values (@numGroupId, 1, (right(left(@bitstatus, 1),1)))

insert into grouptabdetails(numgroupid, numtabid, bitallowed) values (@numGroupId, 2, (right(left(@bitstatus, 2),1)))

insert into grouptabdetails(numgroupid, numtabid, bitallowed) values (@numGroupId, 3, (right(left(@bitstatus, 3),1)))

insert into grouptabdetails(numgroupid, numtabid, bitallowed) values (@numGroupId, 4, (right(left(@bitstatus, 4),1)))

insert into grouptabdetails(numgroupid, numtabid, bitallowed) values (@numGroupId, 5, (right(left(@bitstatus, 5),1)))

insert into grouptabdetails(numgroupid, numtabid, bitallowed) values (@numGroupId, 6, (right(left(@bitstatus, 6),1)))

end
GO
