/****** Object:  StoredProcedure [dbo].[usp_GetACInfoForMail]    Script Date: 07/26/2008 16:16:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getacinfoformail')
DROP PROCEDURE usp_getacinfoformail
GO
CREATE PROCEDURE [dbo].[usp_GetACInfoForMail]
	@numCreatedBy numeric(9)=0,
	@numDomainID numeric(9)=0   
--
		
AS
BEGIN
	SELECT 
		vcEmail
		
	FROM 	
		AdditionalContactsInformation
	WHERE
		numCreatedBy=@numCreatedBy 
		and numDomainID=@numDomainID
		
	
END
GO
