GO
/****** Object:  StoredProcedure [dbo].[USP_UserMasterDetails]    Script Date: 02/28/2009 13:54:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
---Created By Siva                  
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_usermasterdetails')
DROP PROCEDURE usp_usermasterdetails
GO
CREATE PROCEDURE [dbo].[USP_UserMasterDetails]                  
(@numUserID as numeric(9)=0)                  
AS                  
Begin                  
SELECT U.numUserID, U.vcUserName as vcUserName,U.numDomainID,                                   
D.vcDomainName, U.numUserDetailId,                  
LD.vcData as vcDepartment,                  
isnull(AD.vcStreet,'') +' ' + isnull(AD.vcCity,'') + ' '+isnull(AD.vcPostalCode,'') as HomeAddress,                  
isnull(ACI.numHomePhone,'') as numHomePhone,                  
isnull(ACI.numCell,'') as CellNo,                  
isnull(ACI.numPhone,'') as OtherPhone,                
isnull(ACI.vcAsstEmail,'') as vcAsstEmail,                  
isnull(ACI.txtNotes,'') as txtNotes,                  
U.bitHourlyRate,U.monHourlyRate,ISNULL(U.bitSalary,0) bitSalary,
U.bitPaidLeave as bitPaidLeave,                  
U.fltPaidLeaveHrs as fltPaidLeaveHrs,                  
U.fltPaidLeaveEmpHrs as fltPaidLeaveEmpHrs,                  
U.numDailyHours as numDailyHours,U.bitOverTime as bitOverTime,                                  
U.numLimDailHrs as numLimDailHrs,                
U.bitManagerOfOwner,
U.fltManagerOfOwner,
U.bitCommOwner, 
U.fltCommOwner,
U.bitCommAssignee,
U.fltCommAssignee,
U.monOverTimeRate as monOverTimeRate,                  
U.bitMainComm as bitMainComm,                  
U.fltMainCommPer as fltMainCommPer,                  
U.bitRoleComm as bitRoleComm,                  
(select count(*) from UserRoles where numUserCntID=U.numUserId)  as Roles,                  
U.bitEmpNetAccount as bitEmpNetAccount,                  
U.dtHired as dtHired,                  
U.dtReleased as dtReleased,              
isnull(U.vcEmployeeId,'') as vcEmployeeId,              
isnull(U.vcEmergencyContact,'') as vcEmergencyContact,            
U.dtSalaryDateTime as dtSalaryDatetime,          
U.bitPayroll as bitPayroll,        
ACI.numContactId as numUserCntId,      
U.fltEmpRating as fltEmpRating,      
U.dtEmpRating as dtEmpRating,      
U.monNetPay as monNetPay,      
U.monAmtWithheld as monAmtWithheld,    
U.bitOverTimeHrsDailyOrWeekly as bitOverTimeHrsDailyOrWeekly      
FROM UserMaster U                                  
join Domain D on U.numDomainID=D.numDomainID      
Left outer join AdditionalContactsInformation ACI on U.numUserDetailId=ACI.numContactId                  
Left outer join ListDetails LD on ACI.vcDepartment=LD.numListItemID And LD.numListID=19 
LEFT OUTER JOIN dbo.AddressDetails AD ON AD.numRecordID=ACI.numContactId AND AD.tintAddressOf=1 AND AD.tintAddressType=0 AND AD.bitIsPrimary=1 AND AD.numDomainID=ACI.numDomainID
Where U.numUserId=@numUserID                
End
