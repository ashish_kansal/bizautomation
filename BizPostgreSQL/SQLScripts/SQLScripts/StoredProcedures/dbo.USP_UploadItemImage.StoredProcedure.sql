/****** Object:  StoredProcedure [dbo].[USP_UploadItemImage]    Script Date: 07/26/2008 16:21:55 ******/
--change 1 update
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_uploaditemimage')
DROP PROCEDURE usp_uploaditemimage
GO
CREATE PROCEDURE [dbo].[USP_UploadItemImage]  
@numItemCode as numeric(9),  
@vcPathForImage as varchar(300)  ,
@vcPathForTImage as varchar(300) ,
@numOppItemCode as numeric(9)=0 ,
@numCatergoryId as numeric(9)=0
as  

--IF @numOppItemCode = 0 AND @numCatergoryId=0
--Note: it needs to be checked if there is a case when @numOppItemCode = 0 AND @numCatergoryId=0 
--BEGIN
   -- kishan  This is not used .
   -- update dbo.ItemImages set vcPathForImage=@vcPathForImage  ,vcPathForTImage=@vcPathForTImage  
   -- where numItemCode=@numItemCode
--END

--ELSE
 IF @numOppItemCode = 0 AND @numItemCode=0
BEGIN
	update Category set vcPathForCategoryImage=@vcPathForImage where numCategoryID=@numCatergoryId
END

ELSE 
BEGIN
	UPDATE [OpportunityItems] SET vcPathForTImage = @vcPathForTImage WHERE [numoppitemtCode]= @numOppItemCode 
END
GO
