/****** Object:  StoredProcedure [dbo].[usp_FindItem]    Script Date: 07/26/2008 16:15:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_finditem')
DROP PROCEDURE usp_finditem
GO
CREATE PROCEDURE [dbo].[usp_FindItem]
@str as varchar(20)=''
as

select numItemCode,vcItemName from item where vcItemName like @str+'%'
GO
