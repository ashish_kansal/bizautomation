/****** Object:  StoredProcedure [dbo].[USP_Journal_EntryList]    Script Date: 07/26/2008 16:19:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created By Siva   
-- 
--exec USP_Journal_EntryList @numChartAcntId=1723,@dtDateFrom='20/Apr/2010',@numDomainId=110,@numDivisionID=0
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_journal_entrylist')
DROP PROCEDURE usp_journal_entrylist
GO
CREATE PROCEDURE [dbo].[USP_Journal_EntryList]                                                                                                                                
@numChartAcntId as numeric(9),                                                                                                                          
@dtDateFrom datetime,                        
@numDomainId as numeric(9),
@numDivisionID AS NUMERIC(9)
                                                                                                                        
As                                                                                                                                
Begin                                                                                                                           
Declare @strSQL as varchar(8000)                                                                                                                      
Declare @strChildCategory as varchar(5000)                                                                              
Declare @numAcntTypeId as integer                                                                                                                 
Declare @OpeningBal as varchar(8000)  
DECLARE @vcAccountCode VARCHAR(50) 
declare @vcDate varchar(50)                                                                                                               
Set @openingBal='Opening Balance'                                                                                                  
Set @strSQL =''                                                                                              
--Create a Temporary table to hold data     

set @vcDate=@dtDateFrom;

                                                                                                                           
Create table #tempTable ( ID INT IDENTITY PRIMARY KEY,                                                                                                                                                
 numCustomerId numeric(9),                                                                                                  
 JournalId numeric(9),                                              
 TransactionType varchar(500),                                                                                                
 EntryDate datetime,                                                                                                  
 CompanyName varchar(8000),                                                                                        
 CheckId numeric(9),                                                                                      
 CashCreditCardId numeric(9),                                                                                                  
 Memo varchar(8000),                                                                                                  
 Payment DECIMAL(20,5),                                                                                                  
 Deposit DECIMAL(20,5),           
 numBalance DECIMAL(20,5),             
 numChartAcntId numeric(9),                                                                                            
 numTransactionId numeric(9),                                                                                                  
 numOppId numeric(9),                    
 numOppBizDocsId numeric(9),                  
 numDepositId numeric(9),                
 numBizDocsPaymentDetId numeric(9),  
 numCategoryHDRID numeric(9),  
 tintTEType   numeric(9),  
 numCategory  numeric(9),   
 numUserCntID   numeric(9),
 dtFromDate DATETIME,
 numCheckNo VARCHAR(20),bitReconcile BIT,bitCleared BIT  
 )
     
     
                                            
   if @dtDateFrom='' Set @dtDateFrom='Jan  1 1753 12:00:00:000AM'                                               
   
SELECT @vcAccountCode=vcAccountCode FROM [Chart_Of_Accounts] WHERE numAccountID = @numChartAcntId
SELECT @strChildCategory = COALESCE(@strChildCategory + ',', '') + 
   CAST(numAccountId AS varchar(10))
FROM Chart_Of_Accounts
WHERE [vcAccountCode] LIKE @vcAccountCode +'%'

Print @strChildCategory
                                           
if @dtDateFrom='Jan  1 1753 12:00:00:000AM'                                                   
Begin                                                                                  
--Commented by chintan
--Set @strChildCategory = dbo.fn_ChildCategory(@numChartAcntId,@numDomainId) + Convert(varchar(10),@numChartAcntId)                                                           

                                                                          
Select @numAcntTypeId=isnull(numParntAcntTypeId,0) from Chart_of_Accounts Where numAccountId=@numChartAcntId And numDomainId=@numDomainId                                                                  
Print @numAcntTypeId                                                         
if @numAcntTypeId=815 or @numAcntTypeId=816 or @numAcntTypeId=820  or @numAcntTypeId=821  or @numAcntTypeId=822  or @numAcntTypeId=825 or @numAcntTypeId=827                                                                        
Begin                                                                          
                                  
         
Set @strSQL= ' Select GJD.numCustomerId AS numCustomerId,GJH.numJournal_Id as JournalId,        
case when isnull(GJH.numCheckId,0) <> 0 then ''Checks''  
Else case when isnull(GJH.numCashCreditCardId,0) <> 0 And CCCD.bitMoneyOut=0 then ''Cash''        
ELse Case when isnull(GJH.numCashCreditCardId,0) <> 0 And CCCD.bitMoneyOut=1 And CCCD.bitChargeBack=1  then ''Charge''        
ELse Case when isnull(GJH.numOppId,0) <> 0 And isnull(GJH.numOppBizDocsId,0) <> 0 And isnull(GJH.numBizDocsPaymentDetId,0)=0 And Opp.tintOppType=1 then ''BizDocs Invoice''        
ELse Case when isnull(GJH.numOppId,0) <> 0 And isnull(GJH.numOppBizDocsId,0) <> 0 And isnull(GJH.numBizDocsPaymentDetId,0)=0 And Opp.tintOppType=2 then ''BizDocs Purchase''        
ELse Case when isnull(GJH.numDepositId,0) <> 0 then ''Deposit''        
ELse Case when isnull(GJH.numBizDocsPaymentDetId,0)<>0 And Opp.tintOppType=1 then ''Receive Amt''        
ELse Case when isnull(GJH.numBizDocsPaymentDetId,0)<>0 And Opp.tintOppType=2 then ''Vendor Amt''        
Else Case when isnull(GJH.numCategoryHDRID,0)<>0 then ''Time And Expenses''  
Else Case When GJH.numJournal_Id <>0 then ''Journal'' End End End End End End End End End End  as TransactionType,        
GJH.datEntry_Date as EntryDate,CI.vcCompanyName AS CompanyName,GJH.numCheckId As CheckId,        
GJH.numCashCreditCardId as CashCreditCardId,        
(case when len(GJD.varDescription) =0 then GJH.varDescription else GJD.varDescription END)  as Memo,        
(case when GJD.numCreditAmt=0 then GJD.numdebitAmt  end) as Deposit,        
(case when GJD.numdebitAmt=0 then GJD.numCreditAmt end) as Payment,        
null numBalance,        
isnull(GJD.numChartAcntId,0) as numChartAcntId,        
GJD.numTransactionId as numTransactionId,               
GJH.numOppId as numOppId,        
GJH.numOppBizDocsId as numOppBizDocsId,        
GJH.numDepositId as numDepositId,          
GJH.numBizDocsPaymentDetId as numBizDocsPaymentDetId,  
isnull(GJH.numCategoryHDRID,0) as numCategoryHDRID,  
isnull(TE.tintTEType,0) as tintTEType,  
TE.numCategory as numCategory,  
TE.numUserCntID as numUserCntID,
TE.dtFromDate as dtFromDate,
isnull(CAST(CD.numCheckNo AS VARCHAR(20)),'''') numCheckNo,
isnull(GJD.bitReconcile,0) as bitReconcile,
isnull(GJD.bitCleared,0) as bitCleared
From General_Journal_Header as GJH        
Left Outer Join General_Journal_Details as GJD  on GJH.numJournal_Id=GJD.numJournalId        
Left outer join DivisionMaster as DM on GJD.numCustomerId=DM.numDivisionID        
Left outer join CompanyInfo as CI on  DM.numCompanyID=CI.numCompanyId        
Left outer join Chart_Of_Accounts CA on GJD.numChartAcntId=CA.numAccountId        
Left outer join CashCreditCardDetails CCCD on GJH.numCashCreditCardId=CCCD.numCashCreditId        
Left outer join CheckDetails CD on GJH.numCheckId=CD.numCheckId        
Left outer join OpportunityMaster Opp on GJH.numOppId=Opp.numOppId    
Left outer join TimeAndExpense TE on GJH.numCategoryHDRID=TE.numCategoryHDRID     
Where GJH.numDomainId='+convert(varchar(4),@numDomainId) +' And  GJD.numChartAcntId in ('+ @strChildCategory + ')'        
+ 'ORDER BY GJH.datEntry_Date'
--And GJH.numChartAcntId is null      
           
End                                                                                                              
Else                                                                          
Begin                               
                               
                           
                  
Set @strSQL=' Select GJD.numCustomerId AS numCustomerId,GJH.numJournal_Id as JournalId,        
case when isnull(GJH.numCheckId,0) <> 0 then +''Checks'' Else case when isnull(GJH.numCashCreditCardId,0) <> 0 And CCCD.bitMoneyOut=0 then ''Cash''        
ELse Case when isnull(GJH.numCashCreditCardId,0) <> 0 And CCCD.bitMoneyOut=1 And CCCD.bitChargeBack=1  then ''Charge''        
ELse Case when isnull(GJH.numOppId,0) <> 0 And isnull(GJH.numOppBizDocsId,0) <> 0 And isnull(GJH.numBizDocsPaymentDetId,0)=0 And Opp.tintOppType=1 then ''BizDocs Invoice''        
ELse Case when isnull(GJH.numOppId,0) <> 0 And isnull(GJH.numOppBizDocsId,0) <> 0 And isnull(GJH.numBizDocsPaymentDetId,0)=0 And Opp.tintOppType=2 then ''BizDocs Purchase''        
ELse Case when isnull(GJH.numDepositId,0) <> 0 then ''Deposit''        
ELse Case when isnull(GJH.numBizDocsPaymentDetId,0)<>0 And Opp.tintOppType=1 then ''Receive Amt''        
ELse Case when isnull(GJH.numBizDocsPaymentDetId,0)<>0 And Opp.tintOppType=2 then ''Vendor Amt''  
Else Case when isnull(GJH.numCategoryHDRID,0)<>0 then ''Time And Expenses''        
Else Case When GJH.numJournal_Id <>0 then ''Journal'' End End End End End End End End End End as TransactionType,        
GJH.datEntry_Date as EntryDate,CI.vcCompanyName AS CompanyName,GJH.numCheckId As CheckId,        
GJH.numCashCreditCardId as CashCreditCardId,        
(case when len(GJD.varDescription) =0 then GJH.varDescription else GJD.varDescription END) as Memo,        
(case when GJD.numCreditAmt<>0 then numCreditAmt end) as Payment,        
(case when GJD.numdebitAmt<>0 then numDebitAmt  end) as Deposit,        
null numBalance,        
isnull(GJD.numChartAcntId,0) as numChartAcntId,        
GJD.numTransactionId as numTransactionId,        
GJH.numOppId as numOppId,        
GJH.numOppBizDocsId as numOppBizDocsId,        
GJH.numDepositId as numDepositId,        
GJH.numBizDocsPaymentDetId as numBizDocsPaymentDetId,  
isnull(GJH.numCategoryHDRID,0) as numCategoryHDRID,  
isnull(TE.tintTEType,0) as tintTEType,  
TE.numCategory as numCategory,  
TE.numUserCntID as numUserCntID,
TE.dtFromDate as dtFromDate,
isnull(CAST(CD.numCheckNo AS VARCHAR(20)),'''') numCheckNo,
isnull(GJD.bitReconcile,0) as bitReconcile,
isnull(GJD.bitCleared,0) as bitCleared
From General_Journal_Header as GJH        
Left Outer Join General_Journal_Details as GJD  on GJH.numJournal_Id=GJD.numJournalId        
Left outer join DivisionMaster as DM on GJD.numCustomerId=DM.numDivisionID        
Left outer join CompanyInfo as CI on  DM.numCompanyID=CI.numCompanyId        
Left outer join Chart_Of_Accounts CA on GJD.numChartAcntId=CA.numAccountId        
Left outer join CashCreditCardDetails CCCD on GJH.numCashCreditCardId=CCCD.numCashCreditId        
Left outer join CheckDetails CD on GJH.numCheckId=CD.numCheckId        
Left outer join OpportunityMaster Opp on GJH.numOppId=Opp.numOppId   
Left outer join TimeAndExpense TE on GJH.numCategoryHDRID=TE.numCategoryHDRID        
Where GJH.numDomainId='+convert(varchar(4),@numDomainId) +' /*And GJH.numChartAcntId is null */ And  GJD.numChartAcntId in ('

if ISNULL(@strChildCategory,'0')='0'
set @strSQL=@strSQL + 'select COAN.numAccountId from Chart_Of_Accounts COAN where COAN.numDomainId=' + CONVERT(VARCHAR(50),@numDomainId) + ')'
else
set @strSQL=@strSQL + @strChildCategory + ')'

if @numDivisionID>0
	set @strSQL=@strSQL +' AND GJD.numCustomerId=' + CONVERT(VARCHAR(50),@numDivisionID)
                    
set @strSQL=@strSQL + 'ORDER BY GJH.datEntry_Date'

--And GJH.numChartAcntId is null      
                                    
End                                                                          
                                                                          
Print (@strChildCategory)                                   
Print (@strSQL)                                                                                                                      
--Exec (@strSQL)                                                                                                  
End                                                                                                 
                                                                   
Else                 
Begin        
                                                                       
--Set @strChildCategory = dbo.fn_ChildCategory(@numChartAcntId,@numDomainId) + Convert(varchar(10),@numChartAcntId)                                                                                                               
Select @numAcntTypeId=isnull(numParntAcntTypeId,0) from Chart_of_Accounts Where numAccountId=@numChartAcntId                                                                          
     Print isnull(@strChildCategory,'0') + ' One'                                                                                      
if @numAcntTypeId=815 or @numAcntTypeId=816 or @numAcntTypeId=820  or @numAcntTypeId=821  or @numAcntTypeId=822  or @numAcntTypeId=825 or @numAcntTypeId=827                                            
Begin                                                               
                                  
                               
Set @strSQL=@strSQL + ' Select GJD.numCustomerId,GJH.numJournal_Id as JournalId,         
case when isnull(GJH.numCheckId,0) <> 0 then +''Checks'' Else case when isnull(GJH.numCashCreditCardId,0) <> 0 And CCCD.bitMoneyOut=0 then ''Cash''        
ELse Case when isnull(GJH.numCashCreditCardId,0) <> 0 And CCCD.bitMoneyOut=1 And CCCD.bitChargeBack=1  then  ''Charge''        
ELse Case when isnull(GJH.numOppId,0) <> 0 And isnull(GJH.numOppBizDocsId,0) <> 0 And isnull(GJH.numBizDocsPaymentDetId,0)=0 And Opp.tintOppType=1 then ''BizDocs Invoice''        
ELse Case when isnull(GJH.numOppId,0) <> 0 And isnull(GJH.numOppBizDocsId,0) <> 0 And isnull(GJH.numBizDocsPaymentDetId,0)=0 And Opp.tintOppType=2 then ''BizDocs Purchase''        
ELse Case when isnull(GJH.numDepositId,0) <> 0 then ''Deposit''        
ELse Case when isnull(GJH.numBizDocsPaymentDetId,0) <> 0 And Opp.tintOppType=1 then ''Receive Amt''        
ELse Case when isnull(GJH.numBizDocsPaymentDetId,0)<>0 And Opp.tintOppType=2 then ''Vendor Amt''        
Else Case when isnull(GJH.numCategoryHDRID,0)<>0 then ''Time And Expenses''  
Else Case When GJH.numJournal_Id <>0 then ''Journal'' End End End End End End End End End End as TransactionType,        
GJH.datEntry_Date as EntryDate,CI.vcCompanyName CompanyName,GJH.numCheckId As CheckId,        
GJH.numCashCreditCardId as CashCreditCardId,        
(case when len(GJD.varDescription) =0 then GJH.varDescription else GJD.varDescription END)  as Memo,        
(case when GJD.numCreditAmt=0 then GJD.numdebitAmt  end) as Deposit,        
(case when GJD.numdebitAmt=0 then GJD.numCreditAmt end) as Payment,        
null numBalance,        
isnull(GJD.numChartAcntId,0) as numChartAcntId,        
GJD.numTransactionId as numTransactionId,        
GJH.numOppId as numOppId,        
GJH.numOppBizDocsId as numOppBizDocsId,        
GJH.numDepositId as numDepositId,        
GJH.numBizDocsPaymentDetId as numBizDocsPaymentDetId,  
isnull(GJH.numCategoryHDRID,0) as numCategoryHDRID,  
isnull(TE.tintTEType,0) as tintTEType,  
TE.numCategory as numCategory,  
TE.numUserCntID as numUserCntID,
TE.dtFromDate as dtFromDate,
isnull(CAST(CD.numCheckNo AS VARCHAR(20)),'''') numCheckNo,
isnull(GJD.bitReconcile,0) as bitReconcile,
isnull(GJD.bitCleared,0) as bitCleared
From General_Journal_Header as GJH        
Left Outer Join General_Journal_Details as GJD on GJH.numJournal_Id=GJD.numJournalId        
Left outer join DivisionMaster as DM on GJD.numCustomerId=DM.numDivisionID        
Left outer join CompanyInfo as CI on DM.numCompanyID=CI.numCompanyId        
Left outer join CashCreditCardDetails CCCD on GJH.numCashCreditCardId=CCCD.numCashCreditId        
Left outer join CheckDetails CD on GJH.numCheckId=CD.numCheckId        
Left outer join OpportunityMaster Opp on GJH.numOppId=Opp.numOppId   
Left outer join TimeAndExpense TE on GJH.numCategoryHDRID=TE.numCategoryHDRID        
Where GJH.numDomainId='+convert(varchar(4),@numDomainId) +' And GJH.numChartAcntId is null And  GJD.numChartAcntId in ('

if ISNULL(@strChildCategory,'0')='0'
set @strSQL=@strSQL + 'select COAN.numAccountId from Chart_Of_Accounts COAN where COAN.numDomainId=' + CONVERT(VARCHAR(50),@numDomainId) + ')'
else
set @strSQL=@strSQL + @strChildCategory + ')'


set @strSQL= @strSQL +' And day(GJH.datEntry_Date)=day(''' + @vcDate +''') and month( GJH.datEntry_Date)=month('''+ @vcDate + ''') and year(GJH.datEntry_Date)=year('''+ @vcDate + ''')' 

if @numDivisionID>0
	set @strSQL=@strSQL +' AND GJD.numCustomerId=' + CONVERT(VARCHAR(50),@numDivisionID)
                    

set @strSQL=@strSQL + 'ORDER BY GJH.datEntry_Date'
     
        
End                                                                          
Else                            
Begin                                                                         
                                                                     
 Print isnull(@strChildCategory,'0') + ' two'                              
                              
Set @strSQL=' Select GJD.numCustomerId,GJH.numJournal_Id as JournalId,        
case when isnull(GJH.numCheckId,0) <> 0 then +''Checks'' Else case when isnull(GJH.numCashCreditCardId,0) <> 0 And CCCD.bitMoneyOut=0 then ''Cash''        
ELse Case when isnull(GJH.numCashCreditCardId,0) <> 0 And CCCD.bitMoneyOut=1 And CCCD.bitChargeBack=1  then ''Charge''        
ELse Case when isnull(GJH.numOppId,0) <> 0 And isnull(GJH.numOppBizDocsId,0) <> 0 And isnull(GJH.numBizDocsPaymentDetId,0)=0 And Opp.tintOppType=1 then ''BizDocs Invoice''        
ELse Case when isnull(GJH.numOppId,0) <> 0 And isnull(GJH.numOppBizDocsId,0) <> 0 And isnull(GJH.numBizDocsPaymentDetId,0)=0 And Opp.tintOppType=2 then ''BizDocs Purchase''        
ELse Case when isnull(GJH.numDepositId,0) <> 0 then ''Deposit''        
ELse Case when isnull(GJH.numBizDocsPaymentDetId,0)<>0 And Opp.tintOppType=1 then ''Receive Amt''        
ELse Case when isnull(GJH.numBizDocsPaymentDetId,0)<>0 And Opp.tintOppType=2 then ''Vendor Amt''    
Else Case when isnull(GJH.numCategoryHDRID,0)<>0 then ''Time And Expenses''      
Else Case When GJH.numJournal_Id <>0 then ''Journal'' End End End End End End End End End End as TransactionType,        
GJH.datEntry_Date as EntryDate,CI.vcCompanyName CompanyName,GJH.numCheckId As CheckId,        
GJH.numCashCreditCardId as CashCreditCardId,        
(case when len(GJD.varDescription) =0 then GJH.varDescription else GJD.varDescription END)  as Memo,        
(case when GJD.numCreditAmt<>0 then GJD.numCreditAmt end) as Payment,        
(case when GJD.numdebitAmt<>0 then GJD.numDebitAmt  end) as Deposit,        
null numBalance,        
isnull(GJD.numChartAcntId,0) as numChartAcntId,        
GJD.numTransactionId as numTransactionId,        
GJH.numOppId as numOppId,        
GJH.numOppBizDocsId as numOppBizDocsId,        
GJH.numDepositId as numDepositId,        
GJH.numBizDocsPaymentDetId as numBizDocsPaymentDetId,  
isnull(GJH.numCategoryHDRID,0) as numCategoryHDRID,  
isnull(TE.tintTEType,0) as tintTEType,  
TE.numCategory as numCategory,  
TE.numUserCntID as numUserCntID,
TE.dtFromDate as dtFromDate,
isnull(CAST(CD.numCheckNo AS VARCHAR(20)),'''') numCheckNo,
isnull(GJD.bitReconcile,0) as bitReconcile,
isnull(GJD.bitCleared,0) as bitCleared
From General_Journal_Header as GJH        
Left Outer Join General_Journal_Details as GJD  on GJH.numJournal_Id=GJD.numJournalId        
Left outer join DivisionMaster as DM on GJD.numCustomerId=DM.numDivisionID        
Left outer join CompanyInfo as CI on  DM.numCompanyID=CI.numCompanyId        
Left outer join CashCreditCardDetails CCCD on GJH.numCashCreditCardId=CCCD.numCashCreditId        
Left outer join CheckDetails CD on GJH.numCheckId=CD.numCheckId        
Left outer join OpportunityMaster Opp on GJH.numOppId=Opp.numOppId        
Left outer join TimeAndExpense TE on GJH.numCategoryHDRID=TE.numCategoryHDRID   
Where GJH.numDomainId='+convert(varchar(4),@numDomainId) +' And GJH.numChartAcntId is null And  GJD.numChartAcntId in ('

if ISNULL(@strChildCategory,'0')='0'
set @strSQL=@strSQL + 'select COAN.numAccountId from Chart_Of_Accounts COAN where COAN.numDomainId=' + CONVERT(VARCHAR(50),@numDomainId) + ')'
else
set @strSQL=@strSQL + @strChildCategory + ')'


set @strSQL= @strSQL +' And day(GJH.datEntry_Date)=day(''' + @vcDate +''') and month( GJH.datEntry_Date)=month('''+ @vcDate + ''') and year(GJH.datEntry_Date)=year('''+ @vcDate + ''')' 

if @numDivisionID>0
	set @strSQL=@strSQL +' AND GJD.numCustomerId=' + CONVERT(VARCHAR(50),@numDivisionID)

 set @strSQL=@strSQL + 'ORDER BY GJH.datEntry_Date'
   
                              
End                                                                                                            
                                                                                                                     
Print (@strChildCategory)                                         
     Print (@strSQL) 
End                                                                                                                                                                                 
exec ('insert into #tempTable' + @strSQL)                                                                                           
                                                                                       
Select * from #temptable 

Drop table #temptable                                                                                                                 
                                                                                              
End
