
GO
/****** Object:  StoredProcedure [dbo].[USP_JobBizDocAction]    Script Date: 02/19/2010 18:26:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_jobbizdocaction')
DROP PROCEDURE usp_jobbizdocaction
GO
CREATE PROCEDURE [dbo].[USP_JobBizDocAction]

as
begin

declare @dtLog datetime;
declare @dtNow datetime;

select @dtLog=isnull(max(dtBizDocLog),'10/Feb/2010') from BizDocActionLog;

set @dtNow=getutcdate()

DECLARE  @maxDomainID NUMERIC(9)
DECLARE  @minDomainID NUMERIC(9)
SELECT @maxDomainID = max([numDomainId])
FROM   [Domain] 
SELECT @minDomainID = min([numDomainId])
FROM   [Domain] 
WHILE @minDomainID <= @maxDomainID
  BEGIN
	PRINT 'DomainID=' + CONVERT(VARCHAR(20),@minDomainID)
	PRINT '--------------'
	
	
	EXEC USP_ManageBizDocActionItem @numDomainID=@minDomainID,@dtFromDate=@dtLog,@dtTodate=@dtNow


	---------------------------------------------------
	SELECT @minDomainID = min([numDomainId])
	FROM   [Domain] WHERE  [numDomainId]> @minDomainID 
END



insert into BizDocActionLog select @dtNow

end