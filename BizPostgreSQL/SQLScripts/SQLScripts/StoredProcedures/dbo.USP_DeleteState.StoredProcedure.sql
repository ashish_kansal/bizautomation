/****** Object:  StoredProcedure [dbo].[USP_DeleteState]    Script Date: 07/26/2008 16:15:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created By Anoop Jayaraj 
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_deletestate')
DROP PROCEDURE usp_deletestate
GO
CREATE PROCEDURE  [dbo].[USP_DeleteState]  
@numStateID as numeric(9)=0,  
@numDomainID as numeric(9)=0  
  
as  
  
  
delete from State where numStateID=@numStateID and numDomainID=@numDomainID
GO
