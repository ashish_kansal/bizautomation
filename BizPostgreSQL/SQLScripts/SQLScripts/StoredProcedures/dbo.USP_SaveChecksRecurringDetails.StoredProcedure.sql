/****** Object:  StoredProcedure [dbo].[USP_SaveChecksRecurringDetails]    Script Date: 07/26/2008 16:20:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created By Siva  
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_savechecksrecurringdetails')
DROP PROCEDURE usp_savechecksrecurringdetails
GO
CREATE PROCEDURE [dbo].[USP_SaveChecksRecurringDetails]  
@numCheckId as numeric(9)=0,  
@dtRecurringDate as datetime  
As  
Begin  
	Insert into ChecksRecurringDetails (numCheckId,dtRecurringDate) Values(@numCheckId,@dtRecurringDate)  
End
GO
