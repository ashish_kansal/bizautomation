/****** Object:  StoredProcedure [dbo].[USP_CLFTabs]    Script Date: 07/26/2008 16:15:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by Anoop Jayaraj  
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_clftabs')
DROP PROCEDURE usp_clftabs
GO
CREATE PROCEDURE [dbo].[USP_CLFTabs]    
@locId as numeric(9)=0,  
@numDomainID numeric(9)=0   
as    
    
Select grp_id,isnull(grp_name,'') as grp_name    
from  cfw_grp_master   
where grp_name is not null and loc_id=@locId   
and numDomainID= @numDomainID and tintType=0 -- means allow one to add custom field only to normal field tab
-- Check Bug ID 454 for more information
GO
