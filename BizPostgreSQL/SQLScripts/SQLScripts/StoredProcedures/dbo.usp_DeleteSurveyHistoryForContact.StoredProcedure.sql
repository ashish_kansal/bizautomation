/****** Object:  StoredProcedure [dbo].[usp_DeleteSurveyHistoryForContact]    Script Date: 07/26/2008 16:15:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_deletesurveyhistoryforcontact')
DROP PROCEDURE usp_deletesurveyhistoryforcontact
GO
CREATE PROCEDURE [dbo].[usp_DeleteSurveyHistoryForContact]  
 @numSurID numeric,             
 @numRespondentID numeric      
AS              
--This procedure will delete the survey responses for a contact  
BEGIN              
 --Delete all Responses  
 DELETE FROM SurveyResponse  
 WHERE numParentSurID = @numSurID  
 AND numRespondantID = @numRespondentID  
   
 --Delete Respondents Information  
 DELETE FROM SurveyRespondentsChild  
 WHERE numSurID = @numSurID  
 AND numRespondantID = @numRespondentID  
  
 DELETE FROM SurveyRespondentsMaster  
 WHERE numSurID = @numSurID  
 AND numRespondantID = @numRespondentID  
END
GO
