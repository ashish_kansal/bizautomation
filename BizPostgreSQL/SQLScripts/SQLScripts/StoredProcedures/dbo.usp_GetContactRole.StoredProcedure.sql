/****** Object:  StoredProcedure [dbo].[usp_GetContactRole]    Script Date: 07/26/2008 16:16:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--modified by anoop jayaraj         
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getcontactrole')
DROP PROCEDURE usp_getcontactrole
GO
CREATE PROCEDURE [dbo].[usp_GetContactRole]           
 @numDomainID numeric,            
 @dtDateFrom datetime,            
 @dtDateTo datetime,            
 @intOppStatus numeric,          
 @numUserCntID numeric=0,                    
 @intType numeric=0,          
 @tintRights TINYINT=1          
  --          
AS            
BEGIN            
 If @tintRights=1          
 --All Records Rights            
 BEGIN            
select AI.numContactID,DM.numDivisionID,CI.numCompanyID,DM.tintCRMType,OM.numOppid,OM.vcPOppName as OpportunityName,dbo.GetOppLstMileStonePercentage(OM.numOppID) as Status,        
(AI.vcLastName + ' '+ AI.vcFirstName) as ContactName, monpAmount as Amount,        
CI.vcCompanyName as CompanyName,        
LD.vcData as Role        
from OpportunityMaster OM        
join  OpportunityContact OC        
on OC.numOppId=OM.numOppId        
left join listdetails LD        
on LD.numListItemID=numRole        
join AdditionalContactsInformation AI        
on AI.numContactID=OC.numContactID        
join DivisionMaster DM        
on DM.numDivisionID=OM.numDivisionID        
join CompanyInfo CI        
on CI.numCompanyID=DM.numCompanyID        
WHERE OM.numDomainID=@numDomainID        
AND OM.tintOppStatus = @intOppStatus          
AND OM.intPEstimatedCloseDate BETWEEN @dtDateFrom AND @dtDateTo          
and OM.numRecOwner=@numUserCntID        
        
 END            
            
 If @tintRights=2          
 BEGIN            
          
select AI.numContactID,DM.numDivisionID,CI.numCompanyID,DM.tintCRMType,OM.numOppid,OM.vcPOppName as OpportunityName,dbo.GetOppLstMileStonePercentage(OM.numOppID) as Status,        
(AI.vcLastName + ' '+ AI.vcFirstName) as ContactName, monpAmount as Amount,        
CI.vcCompanyName as CompanyName,        
LD.vcData as Role        
from OpportunityMaster OM        
join  OpportunityContact OC        
on OC.numOppId=OM.numOppId        
left join listdetails LD        
on LD.numListItemID=numRole        
join AdditionalContactsInformation AI        
on AI.numContactID=OC.numContactID        
join DivisionMaster DM        
on DM.numDivisionID=OM.numDivisionID        
join CompanyInfo CI        
on CI.numCompanyID=DM.numCompanyID
Left outer join AdditionalContactsInformation ACI 
on OM.numRecOwner = ACI.numContactId
         
WHERE OM.numDomainID=@numDomainID        
AND OM.tintOppStatus = @intOppStatus          
AND OM.intPEstimatedCloseDate BETWEEN @dtDateFrom AND @dtDateTo          
and ACI.numTeam in(select F.numTeam from ForReportsByTeam F       -- Added By Anoop          
    where F.numUserCntID=@numUserCntID and F.numDomainid=@numDomainId and F.tintType=@intType)         
 END            
            
 If @tintRights=3          
 BEGIN            
select AI.numContactID,DM.numDivisionID,CI.numCompanyID,DM.tintCRMType,OM.numOppid,OM.vcPOppName as OpportunityName,dbo.GetOppLstMileStonePercentage(OM.numOppID) as Status,        
(AI.vcLastName + ' '+ AI.vcFirstName) as ContactName, monpAmount as Amount,        
CI.vcCompanyName as CompanyName,        
LD.vcData as Role        
from OpportunityMaster OM        
join  OpportunityContact OC        
on OC.numOppId=OM.numOppId        
left join listdetails LD        
on LD.numListItemID=numRole        
join AdditionalContactsInformation AI        
on AI.numContactID=OC.numContactID        
join DivisionMaster DM        
on DM.numDivisionID=OM.numDivisionID        
join CompanyInfo CI        
on CI.numCompanyID=DM.numCompanyID 
       
WHERE OM.numDomainID=@numDomainID        
AND OM.tintOppStatus = @intOppStatus          
AND OM.intPEstimatedCloseDate BETWEEN @dtDateFrom AND @dtDateTo          
and DM.numTerID  in(select F.numTerritory from ForReportsByTerritory F      -- Added By Anoop           
    where F.numUserCntID=@numUserCntID and F.numDomainid=@numDomainID and F.tintType=@intType)        
          
 END            
 END
GO
