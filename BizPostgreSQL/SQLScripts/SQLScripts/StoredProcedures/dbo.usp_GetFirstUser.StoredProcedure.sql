/****** Object:  StoredProcedure [dbo].[usp_GetFirstUser]    Script Date: 07/26/2008 16:17:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
--For getting the First User Id from the user master.
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getfirstuser')
DROP PROCEDURE usp_getfirstuser
GO
CREATE PROCEDURE [dbo].[usp_GetFirstUser]
	@numDomainId numeric   
--
AS
	SELECT min(numuserid) FROM UserMaster WHERE  numdomainid=@numdomainid
GO
