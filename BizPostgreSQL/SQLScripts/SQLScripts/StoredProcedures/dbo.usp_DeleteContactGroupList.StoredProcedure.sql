/****** Object:  StoredProcedure [dbo].[usp_DeleteContactGroupList]    Script Date: 07/26/2008 16:15:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--exec  usp_DeleteContactList 49      
  
   
                
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_deletecontactgrouplist')
DROP PROCEDURE usp_deletecontactgrouplist
GO
CREATE PROCEDURE [dbo].[usp_DeleteContactGroupList]   
  @strcontact as text=null,    
  @numDomainID as numeric,  
  @numUserCntID as numeric,  
  @numGroupID as numeric  
as  
  
  
if  @numGroupID <> 0    
begin  

  
  
if convert(varchar(10),@strcontact) <> ''  
begin                              
   DECLARE @hDoc4 int                                      
   EXEC sp_xml_preparedocument @hDoc4 OUTPUT, @strcontact                                      
delete ProfileEGroupDTL where numContactId not in (                                         
    select X.numContactId from  
(SELECT * FROM OPENXML (@hDoc4,'/NewDataSet/Table',2)                                      
 WITH  (                                  
  numContactId numeric(9)))X  ) and numEmailGroupID =  @numGroupID
     
 
end   
                                      
                                        
   EXEC sp_xml_removedocument @hDoc4    
end
GO
