/****** Object:  StoredProcedure [dbo].[USP_CheckDuplicateBudgetName]    Script Date: 07/26/2008 16:15:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created By Siva  
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_checkduplicatebudgetname')
DROP PROCEDURE usp_checkduplicatebudgetname
GO
CREATE PROCEDURE [dbo].[USP_CheckDuplicateBudgetName]  
(@numBudgetId as numeric(9)=0,        
@vcBudgetName as varchar(100)='',  
@numDivisionId as numeric(9)=0,
@numDepartmentId as numeric(9)=0,
@numCostCenterId as numeric(9)=0,
@numDomainId as numeric(9)=0)  
As  
Begin  
   Select Count(*) From OperationBudgetMaster Where numDivisionId=@numDivisionId And numDepartmentId=@numDepartmentId
   And numCostCenterId=@numCostCenterId
   And numBudgetId<>@numBudgetId And numDomainId=@numDomainId  
End
GO
