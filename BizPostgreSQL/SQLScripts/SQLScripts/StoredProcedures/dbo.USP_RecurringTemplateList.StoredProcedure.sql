/****** Object:  StoredProcedure [dbo].[USP_RecurringTemplateList]    Script Date: 07/26/2008 16:20:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created By Siva
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_recurringtemplatelist')
DROP PROCEDURE usp_recurringtemplatelist
GO
CREATE PROCEDURE [dbo].[USP_RecurringTemplateList]  
(@numDomainId as numeric(9)=0,
@bitBizDocBreakup AS bit=0

)
As  
Begin  

 Select numRecurringId,varRecurringTemplateName, case when chrIntervalType='D' then 'Daily' when chrIntervalType='M' then 'Monthly' When chrIntervalType='Y' then 'Yearly' end as IntervalType  
 From RecurringTemplate Where numDomainId=@numDomainId
 AND (bitBizDocBreakup = @bitBizDocBreakup OR @bitBizDocBreakup =0) 
  
End
GO
