/****** Object:  StoredProcedure [dbo].[usp_GetItemDetailsFor]    Script Date: 07/26/2008 16:17:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getitemdetailsfor')
DROP PROCEDURE usp_getitemdetailsfor
GO
CREATE PROCEDURE [dbo].[usp_GetItemDetailsFor]        
 @numUserCntID numeric(9)=0,        
 @numDomainID numeric(9)=0,      
@numItemCode as numeric(9),
@numDivisionID as numeric(9)=0      
as      
 

if   @numUserCntID>0
begin
   
	 SELECT        
	   intPEstimatedCloseDate,      
	    numUnitHour,       
	  tintOppStatus,      
	  bintAccountClosingDate,      
	  (select top 1 numStagePercentage  from OpportunityStageDetails  where OpportunityStageDetails.numOppId=mst.numOppID and numStagePercentage!=100 and bitstagecompleted=1 order by numStagePercentage,numOppStageId desc ) as Percentage      
	 FROM          
	    OpportunityMaster  mst      
	 join OpportunityItems item      
	 on item.numOppId=mst.numOppId      
	 WHERE tintOppStatus<>2 and  mst.numRecOwner=@numUserCntID        
	 and mst.numDomainID=@numDomainID       
	 and numItemCode=@numItemCode 
end     

if   @numDivisionID>0
begin
   
	 SELECT        
	   intPEstimatedCloseDate,      
	    numUnitHour,       
	  tintOppStatus,      
	  bintAccountClosingDate,      
	  (select top 1 numStagePercentage  from OpportunityStageDetails  where OpportunityStageDetails.numOppId=mst.numOppID and numStagePercentage!=100 and bitstagecompleted=1 order by numStagePercentage,numOppStageId desc ) as Percentage      
	 FROM          
	    OpportunityMaster  mst      
	 join OpportunityItems item      
	 on item.numOppId=mst.numOppId      
	 WHERE tintOppStatus<>2 and  mst.numRecOwner=(select numContactID from AdditionalContactsInformation where numDivisionID=@numDivisionID)        
	 and mst.numDomainID=@numDomainID       
	 and numItemCode=@numItemCode 
end
GO
