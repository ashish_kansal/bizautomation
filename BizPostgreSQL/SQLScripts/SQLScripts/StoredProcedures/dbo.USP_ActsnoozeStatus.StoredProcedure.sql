/****** Object:  StoredProcedure [dbo].[USP_ActsnoozeStatus]    Script Date: 07/26/2008 16:14:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_actsnoozestatus')
DROP PROCEDURE usp_actsnoozestatus
GO
CREATE PROCEDURE [dbo].[USP_ActsnoozeStatus]  
@numCommID as numeric(9)=null  
as  
update Communication set intSnoozeMins=0,intRemainderMins=0,tintSnoozeStatus=0,tintRemStatus=0  
where numCommid=@numCommID
GO
