/****** Object:  StoredProcedure [dbo].[USP_GetJournalEntryDate]    Script Date: 07/26/2008 16:17:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created by Siva
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getjournalentrydate')
DROP PROCEDURE usp_getjournalentrydate
GO
CREATE PROCEDURE [dbo].[USP_GetJournalEntryDate]    
@numJournalId as numeric(9)=0,    
@numDomainId as numeric(9)=0    
As    
Begin    
  Select numJournal_Id,
		 numRecurringId,
		 datEntry_Date,
		 varDescription,
		 numAmount,
		 numDomainId,
		 numCheckId,
		 numCashCreditCardId,
		 numChartAcntId,
		 numOppId,
		 numOppBizDocsId,
		 numDepositId,
		 numBizDocsPaymentDetId,
		 bitOpeningBalance,
		 dtLastRecurringDate,
		 numNoTransactions,
		 numCreatedBy,
		 datCreatedDate,
		 numCategoryHDRID,
		 numProjectID,ISNULL(numClassID,0) numClassID  from General_Journal_Header Where numJournal_Id=@numJournalId And numDomainId=@numDomainId    
End
GO
