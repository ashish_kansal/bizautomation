/****** Object:  StoredProcedure [dbo].[USP_GetPagesVisited]    Script Date: 07/26/2008 16:18:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj      
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getpagesvisited')
DROP PROCEDURE usp_getpagesvisited
GO
CREATE PROCEDURE [dbo].[USP_GetPagesVisited]      
@numTrackingID as numeric(9)=0      
as      
select   numTracVisitorsHDRID,    
vcPageName,      
vcElapsedTime,  
convert(varchar(20),dtTime) as TimeVisited     
from TrackingVisitorsDTL         
where numTracVisitorsHDRID=@numTrackingID
ORDER BY numTracVisitorsDTLID 
GO
