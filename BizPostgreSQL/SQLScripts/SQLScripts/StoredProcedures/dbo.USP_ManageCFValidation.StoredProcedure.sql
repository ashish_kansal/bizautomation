GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_managecfvalidation')
DROP PROCEDURE usp_managecfvalidation
GO
CREATE PROCEDURE [dbo].[USP_ManageCFValidation] 
( @strFieldList AS TEXT = '',@numFieldID AS NUMERIC(9) )
AS 
DECLARE @hDoc INT
EXEC sp_xml_preparedocument @hDoc OUTPUT, @strFieldList

IF EXISTS(SELECT * FROM dbo.CFW_Validation WHERE numFieldId =@numFieldID)
BEGIN
	
UPDATE  CFW_Validation
SET     bitIsRequired = X.bitIsRequired,
        bitIsNumeric = X.bitIsNumeric,
        bitIsAlphaNumeric = X.bitIsAlphaNumeric,
        bitIsEmail = X.bitIsEmail,
        bitIsLengthValidation = X.bitIsLengthValidation,
        intMaxLength = X.intMaxLength,
        intMinLength = X.intMinLength,
        bitFieldMessage = X.bitFieldMessage,
        vcFieldMessage = X.vcFieldMessage
FROM    ( SELECT    *
          FROM      OPENXML (@hDoc, '/NewDataSet/Table',2) WITH ( numFieldId NUMERIC(9), bitIsRequired TINYINT, bitIsNumeric BIT, bitIsAlphaNumeric BIT, bitIsEmail BIT, bitIsLengthValidation BIT, intMaxLength INT, intMinLength INT, bitFieldMessage BIT, vcFieldMessage VARCHAR(500) )
        ) X
WHERE   CFW_Validation.numFieldId = X.numFieldId
END 
ELSE 
BEGIN
	INSERT INTO dbo.CFW_Validation (
		numFieldId,
		bitIsRequired,
		bitIsNumeric,
		bitIsAlphaNumeric,
		bitIsEmail,
		bitIsLengthValidation,
		intMaxLength,
		intMinLength,
		bitFieldMessage,
		vcFieldMessage
	) 
	SELECT 
		X.numFieldId,
		X.bitIsRequired,
		X.bitIsNumeric,
		X.bitIsAlphaNumeric,
		X.bitIsEmail,
		X.bitIsLengthValidation,
		X.intMaxLength,
		X.intMinLength,
		X.bitFieldMessage,
		X.vcFieldMessage
		FROM 	( SELECT    *
          FROM      OPENXML (@hDoc, '/NewDataSet/Table',2) WITH ( numFieldId NUMERIC(9), bitIsRequired TINYINT, bitIsNumeric BIT, bitIsAlphaNumeric BIT, bitIsEmail BIT, bitIsLengthValidation BIT, intMaxLength INT, intMinLength INT, bitFieldMessage BIT, vcFieldMessage VARCHAR(500) )
        ) X

END
EXEC sp_xml_removedocument @hDoc