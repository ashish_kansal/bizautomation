/****** Object:  StoredProcedure [dbo].[usp_RecreateGlobalCustomReportTables]    Script Date: 07/26/2008 16:20:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created By: Debasish Nag        
--Created On: 12th Nov 2005        
--Purpose: To drop the Global Temp tables related to Custom Reports and recreate them            
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_recreateglobalcustomreporttables')
DROP PROCEDURE usp_recreateglobalcustomreporttables
GO
CREATE PROCEDURE [dbo].[usp_RecreateGlobalCustomReportTables]  
 @numRefreshTimeInterval Numeric                                 
AS            
BEGIN        
 DECLARE @tempOppItemTable NVARCHAR(100)            
 SET @tempOppItemTable = '##OppItemCustomField'         
 DECLARE @tempOrgContactTable NVARCHAR(100)            
 SET @tempOrgContactTable = '##OrgContactCustomField'   
  
 IF EXISTS (SELECT 'x' FROM tempdb..sysobjects WHERE type = 'U' and NAME = @tempOrgContactTable)   
 BEGIN  
 DROP TABLE ##OrgContactCustomField  
 EXEC('usp_OrganizationContactsCustomFields ' + @numRefreshTimeInterval)  
 END  
  
 IF EXISTS (SELECT 'x' FROM tempdb..sysobjects WHERE type = 'U' and NAME = @tempOppItemTable)   
 BEGIN  
 DROP TABLE ##OppItemCustomField  
 EXEC('usp_OppItemCustomFields ' + @numRefreshTimeInterval)  
 END  
   
END
GO
