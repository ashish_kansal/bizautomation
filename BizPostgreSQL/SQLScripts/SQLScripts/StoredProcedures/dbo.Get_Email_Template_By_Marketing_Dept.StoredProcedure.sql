/****** Object:  StoredProcedure [dbo].[Get_Email_Template_By_Marketing_Dept]    Script Date: 07/26/2008 16:14:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='get_email_template_by_marketing_dept')
DROP PROCEDURE get_email_template_by_marketing_dept
GO
CREATE PROCEDURE [dbo].[Get_Email_Template_By_Marketing_Dept]  
(  
@currentUserID Int ,  
@currentDomainID Int  ,  
@isForMarketingDept Int -- 0 false , 1 true  
)  
As  

if @isForMarketingDept = 1
Begin    
	select VcDocName , numGenericDocID from GenericDocuments    
	where  numDocCategory = 369    
	And   
	numCreatedBy = @currentUserID    
	And  
	numDomainId = @currentDomainID  
	And  
	vcDocumentSection ='M'

End
Else	-- no filter load all templates back
Begin
	select VcDocName , numGenericDocID from GenericDocuments    
	where  numDocCategory = 369    
	And   
	numCreatedBy = @currentUserID    
	And  
	numDomainId = @currentDomainID  
end
GO
