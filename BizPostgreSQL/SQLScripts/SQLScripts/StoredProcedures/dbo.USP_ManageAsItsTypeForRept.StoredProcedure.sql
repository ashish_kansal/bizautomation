/****** Object:  StoredProcedure [dbo].[USP_ManageAsItsTypeForRept]    Script Date: 07/26/2008 16:19:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_manageasitstypeforrept')
DROP PROCEDURE usp_manageasitstypeforrept
GO
CREATE PROCEDURE [dbo].[USP_ManageAsItsTypeForRept]      
@numUserCntId as numeric(9),      
@numDomainId as numeric(9),      
@tintType as tinyint,      
@strTerritory as varchar(4000)      
as      
      
      
declare @separator_position as integer      
declare @strPosition as varchar(1000)      
      
delete from ForReportsByAsItsType where numUserCntID = @numUserCntId      
and numDomainId=@numDomainID and tintType=@tintType      
       
      
   while patindex('%,%' , @strTerritory) <> 0      
    begin -- Begin for While Loop      
      select @separator_position =  patindex('%,%' , @strTerritory)      
    select @strPosition = left(@strTerritory, @separator_position - 1)      
       select @strTerritory = stuff(@strTerritory, 1, @separator_position,'')      
     insert into ForReportsByAsItsType (numUserCntID,numListItemID,numDomainID,tintType)      
     values (@numUserCntId,@strPosition,@numDomainID,@tintType)      
       
        
   end
GO
