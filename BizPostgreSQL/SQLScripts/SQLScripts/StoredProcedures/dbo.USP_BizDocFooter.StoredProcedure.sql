/****** Object:  StoredProcedure [dbo].[USP_BizDocFooter]    Script Date: 07/26/2008 16:14:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_bizdocfooter')
DROP PROCEDURE usp_bizdocfooter
GO
CREATE PROCEDURE [dbo].[USP_BizDocFooter]
@numDomainID as numeric(9)=0,
@byteMode as tinyint,
@vcBizDocFooter as varchar(100)
as

if @byteMode=1
begin
	update domain set vcBizDocFooter=@vcBizDocFooter where numDomainID=@numDomainID
end
else if @byteMode=2
begin
	update domain set vcBizDocFooter=null where numDomainID=@numDomainID
end
else if @byteMode=3
begin
	update domain set vcPurBizDocFooter=@vcBizDocFooter where numDomainID=@numDomainID
end
else if @byteMode=4
begin
	update domain set vcPurBizDocFooter=null where numDomainID=@numDomainID
end
GO
