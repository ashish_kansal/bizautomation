/****** Object:  StoredProcedure [dbo].[USP_GetRecurringDetails]    Script Date: 07/26/2008 16:18:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created by Siva    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getrecurringdetails')
DROP PROCEDURE usp_getrecurringdetails
GO
CREATE PROCEDURE [dbo].[USP_GetRecurringDetails]      
(@numDomainId as numeric(9)=0)
As    
Begin    
   Select numRecurringId,varRecurringTemplateName From RecurringTemplate Where numDomainId=@numDomainId     
End
GO
