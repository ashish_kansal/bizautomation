/****** Object:  StoredProcedure [dbo].[GetManagersddl]    Script Date: 07/26/2008 16:14:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='getmanagersddl')
DROP PROCEDURE getmanagersddl
GO
CREATE PROCEDURE [dbo].[GetManagersddl]     
@numDivisionID as numeric(9)=0,    
@numContactID as numeric(9)=0,    
@numDomainID as numeric(9)=0
as    
    
    
if @numContactID=0    
begin    
 select vcFirstName+' '+ vcLastName as [Name],  numContactID from AdditionalContactsInformation     
 where numDivisionID = @numDivisionID  and numDomainID=@numDomainID  
end    
else if @numContactID<>0    
begin    
 select vcFirstName+' '+ vcLastName as [Name], numContactID from AdditionalContactsInformation     
 where numDivisionID = @numDivisionID and numContactId =@numContactID and numDomainID=@numDomainID    
end
GO
