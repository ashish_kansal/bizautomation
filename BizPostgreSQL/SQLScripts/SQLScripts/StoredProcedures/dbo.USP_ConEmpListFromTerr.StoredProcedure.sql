/****** Object:  StoredProcedure [dbo].[USP_ConEmpListFromTerr]    Script Date: 07/26/2008 16:15:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- Created By Anoop Jayaraj        
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_conemplistfromterr')
DROP PROCEDURE usp_conemplistfromterr
GO
CREATE PROCEDURE [dbo].[USP_ConEmpListFromTerr]       
@numDomainID as numeric(9)=0,  
@bitPartner as bit=0,  
@numContactID as numeric(9)=0,
@numTerID as numeric(9)=0          
as     
  
if @bitPartner=0  
begin      
 SELECT A.numContactID,A.vcFirstName+' '+A.vcLastName as vcUserName        
 from UserMaster UM       
 join AdditionalContactsInformation A      
 on UM.numUserDetailId=A.numContactID        
 where UM.numDomainID=@numDomainID and UM.intAssociate=1 AND
 A.numContactID in (select numUserCntID from UserTerritory where numTerritoryID=@numTerID and numDomainID=@numDomainID) 
 union  
 select  A.numContactID,vcCompanyName+' - '+A.vcFirstName+' '+A.vcLastName as vcUserName  
 from AdditionalContactsInformation A   
 join DivisionMaster D  
 on D.numDivisionID=A.numDivisionID  
 join ExtarnetAccounts E   
 on E.numDivisionID=D.numDivisionID  
 join ExtranetAccountsDtl DTL  
 on DTL.numExtranetID=E.numExtranetID  
 join CompanyInfo C  
 on C.numCompanyID=D.numCompanyID  
 where A.numDomainID=@numDomainID   
 and D.numDivisionID <>(select numDivisionID from domain where numDomainID=@numDomainID) and  D.numTerID=@numTerID
end  
if @bitPartner=1  
begin      
 SELECT A.numContactID,A.vcFirstName+' '+A.vcLastName as vcUserName        
 from UserMaster UM       
 join AdditionalContactsInformation A      
 on UM.numUserDetailId=A.numContactID        
 where UM.numDomainID=@numDomainID  and  UM.intAssociate=1 AND
 A.numContactID in (select numUserCntID from UserTerritory where numTerritoryID=@numTerID and numDomainID=@numDomainID)
 union  
 select  A.numContactID,vcCompanyName+' - '+A.vcFirstName+' '+A.vcLastName as vcUserName  
 from AdditionalContactsInformation A   
 join DivisionMaster D  
 on D.numDivisionID=A.numDivisionID  
 join CompanyInfo C  
 on C.numCompanyID=D.numCompanyID  
 where A.numContactID=@numContactID and  D.numTerID=@numTerID and D.numDivisionID <>(select numDivisionID from domain where numDomainID=@numDomainID)  
end
GO
