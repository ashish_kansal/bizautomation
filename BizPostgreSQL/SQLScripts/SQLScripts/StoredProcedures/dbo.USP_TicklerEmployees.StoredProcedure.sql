/****** Object:  StoredProcedure [dbo].[USP_TicklerEmployees]    Script Date: 07/26/2008 16:21:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj              
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_tickleremployees')
DROP PROCEDURE usp_tickleremployees
GO
CREATE PROCEDURE [dbo].[USP_TicklerEmployees]
    @numUserCntId AS NUMERIC(9) = 0,
    @numDomainID AS NUMERIC(9) = 0,
    @intType AS INTEGER = 0
AS 
    DECLARE @UserID AS NUMERIC(9)    
    SELECT  @UserID = numUserID
    FROM    UserMaster
    WHERE   numUserDetailID = @numUserCntId    
               
    SELECT  vcUserName + ',' + CONVERT(VARCHAR(15), numUserDetailID) + ','
            + vcEmailID AS numUserId,
            vcFirstName + ' ' + vcLastname AS [Name]
    FROM    AdditionalContactsInformation
            JOIN UserMaster ON numContactID = numUserDetailID
            JOIN [UserTeams] ON [UserMaster].[numDomainID] = [UserTeams].[numDomainID]
            AND [AdditionalContactsInformation].[numContactId]=[UserTeams].[numUserCntID]
    WHERE   [UserTeams].numTeam IN ( SELECT numTeam
                         FROM   UserTeams
                         WHERE  numUserCntID = @numUserCntId )
            AND numUserId <> @UserID
            AND UserMaster.numDomainID = @numDomainID
    UNION
    SELECT  vcUserName + ',' + CONVERT(VARCHAR(15), numUserDetailID) + ','
            + vcEmailID AS numUserId,
            'Myself' AS [Name]
    FROM    AdditionalContactsInformation
            JOIN UserMaster ON numContactID = numUserDetailID
    WHERE   numUserId = @UserID
            AND UserMaster.numDomainID = @numDomainID
GO
