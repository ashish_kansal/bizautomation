/****** Object:  StoredProcedure [dbo].[usp_GetUpdatableFieldList]    Script Date: 07/26/2008 16:18:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created By: Debasish Tapan Nag                          
--Purpose: Returns the list of fields from the database that can be mass updated            
--Created Date: 18/03/2006            
                              
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getupdatablefieldlist')
DROP PROCEDURE usp_getupdatablefieldlist
GO
CREATE PROCEDURE [dbo].[usp_GetUpdatableFieldList]            
 @numDomainID numeric (9),
 @numFormID NUMERIC = 1
AS   

IF @numFormID = 1 
BEGIN
	SELECT vcFieldName as vcFormFieldName, vcOrigDbColumnName+'~'+vcLookBackTableName+'~'+ vcListItemType+'~'+convert(varchar(10),numListID)+'~'+vcFieldDataType+'~'+vcAssociatedControlType+'~'+convert(varchar(10),numFieldId) +'~'+ vcDbColumnName as Value        
		 FROM View_DynamicDefaultColumns            
		 WHERE numDomainID=@numDomainID and bitAllowEdit = 1            
		 AND bitDeleted = 0 and numFormID=@numFormID
	 UNION  
		 select Fld_label,'Custom~'+case when grp_id=1 then 'DivisionMaster~LI~' when grp_id=4 then 'Contacts~LI~' end + convert(varchar(10),isnull(numlistid,0))+'~'+Case Fld_type WHEN 'SelectBox' THEN 'N~' ELSE 'V~' END + Fld_type +'~'+convert(varchar(10),fld_id) +'~' as value  
		 from  CFW_Fld_Master  
		 where (Grp_id=1 or Grp_id=4) and fld_type<>'link' and numDomainID=@numDomainID
	 UNION  
		select 'Organization Record Owner','Ownership~DivisionMaster~~~~SelectBox~~~' as value  
	 UNION  
		select 'Contact Record Owner','Ownership~Contacts~~~~SelectBox~~~' as value	
	 UNION  
		select 'Drip Campaign','DripCampaign~Contacts~~~~SelectBox~~~' as value	
END         
ELSE IF @numFormID = 15
BEGIN
	SELECT vcFieldName as vcFormFieldName, vcOrigDbColumnName+'~'+vcLookBackTableName+'~'+ vcListItemType+'~'+convert(varchar(10),numListID)+'~'+vcFieldDataType+'~'+vcAssociatedControlType+'~'+convert(varchar(10),numFieldId) +'~'+ vcDbColumnName as Value        
		 FROM View_DynamicDefaultColumns            
		 WHERE numDomainID=@numDomainID and bitAllowEdit = 1            
		 AND bitDeleted = 0 and numFormID=@numFormID and vcOrigDbColumnName!='numContactType'
END 
ELSE IF @numFormID = 29 -- adv search (item search)
BEGIN
	SELECT vcFieldName as vcFormFieldName, vcOrigDbColumnName+'~'+vcLookBackTableName+'~'+ vcListItemType+'~'+convert(varchar(10),numListID)+'~'+vcFieldDataType+'~'+vcAssociatedControlType+'~'+convert(varchar(10),numFieldId) +'~'+ vcDbColumnName as Value        
		 FROM View_DynamicDefaultColumns            
		 WHERE numDomainID=@numDomainID and bitAllowEdit = 1            
		 AND bitDeleted = 0 and numFormID=@numFormID AND vcLookBackTableName <> 'WareHouseItmsDTL'
		 AND vcDbColumnName <> 'vcPartNo' AND vcDbColumnName <> 'monAverageCost'
		 AND vcOrigDbColumnName <> 'numAllocation' AND vcOrigDbColumnName <> 'numBackOrder'
		 AND vcOrigDbColumnName <> 'numOnHand' AND vcOrigDbColumnName <> 'numOnOrder'
UNION 
		 SELECT vcFieldName as vcFormFieldName, vcOrigDbColumnName+'~'+vcLookBackTableName+'~'+ ISNULL(vcListItemType,'') +'~'+convert(varchar(10),numListID)+'~'+vcFieldDataType+'~'+vcAssociatedControlType+'~'+convert(varchar(10),numFieldId) +'~'+ vcDbColumnName as Value        
		 FROM View_DynamicDefaultColumns            
		 WHERE numDomainID=@numDomainID and numFormID=29 AND vcLookBackTableName = 'WareHouseItems'
		  AND vcOrigDbColumnName <> 'numAllocation' AND vcOrigDbColumnName <> 'numBackOrder'
		 AND vcOrigDbColumnName <> 'numOnHand' AND vcOrigDbColumnName <> 'numOnOrder'
Union 
		 SELECT 'Tax : ' + isnull(vcTaxName,'') as vcFormFieldName, 'Tax~Tax~~0~Y~CheckBox~'+convert(varchar(10),numTaxItemId)+'~' as Value        
		 FROM TaxItems            
		 WHERE numDomainID=@numDomainID 
END

 
GO
