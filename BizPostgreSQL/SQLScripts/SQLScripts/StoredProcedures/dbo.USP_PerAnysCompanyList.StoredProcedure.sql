/****** Object:  StoredProcedure [dbo].[USP_PerAnysCompanyList]    Script Date: 07/26/2008 16:20:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--- Created By Anoop Jayaraj                                          
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_peranyscompanylist')
DROP PROCEDURE usp_peranyscompanylist
GO
CREATE PROCEDURE [dbo].[USP_PerAnysCompanyList]                                            
@numUserCntID numeric,                                           
@RepType tinyint,                                        
@Condition char(1),                                         
@numStartDate datetime,                                          
@numEndDate datetime,                                          
@CurrentPage int,                                          
@PageSize int,                                          
@TotRecs int output,                                          
@columnName as Varchar(50),                                          
@columnSortOrder as Varchar(10),                                        
@numDomainId as numeric(9),                                  
@strUserIDs as varchar(1000),                      
@numTeamType as tinyint                                        
as                                          
if @strUserIDs='' set @strUserIDs='0'                                        
                                  
declare @strSql as varchar(5000)                                   
                               
if @RepType<>3                                  
begin                                    
--Create a Temporary table to hold data                                          
Create table #tempTable ( ID INT IDENTITY PRIMARY KEY,                                           
      CompanyName VARCHAR(255),                                          
      numTerID varchar(15),                                          
      PrimaryContact VARCHAR(110),                                          
      Phone varchar(25),                                          
 vcEmail varchar(50),                                          
 numCompanyID varchar(15),                                          
 numDivisionID varchar(15),                                          
 numContactID varchar(15),                                          
 numCompanyRating varchar(15),                                          
 vcRating varchar(50),                                          
 numStatusID  varchar(15),                                          
 vcStatus varchar(50),                                          
 numCreatedby varchar(15),                                          
 numRecOwner varchar(15)                                          
 )                                          
                                       
                                        
                                        
                                        
                                        
set @strSql='SELECT  CMP.vcCompanyName + '' - <I>'' + DM.vcDivisionName + ''</I>'' as CompanyName,                                             
     DM.numTerID,                                            
     ADC.vcLastName + '' '' + ADC.vcFirstName as PrimaryContact,                                            
    
      case when ADC.numPhone<>'''' then + ADC.numPhone +case when ADC.numPhoneExtension<>'''' then '' - '' + ADC.numPhoneExtension else '''' end  else '''' end as [Phone],                                            
     ADC.vcEmail As vcEmail,                                            
     CMP.numCompanyID AS numCompanyID,                                            
     DM.numDivisionID As numDivisionID,                                            
     ADC.numContactID AS numContactID,                                             
     CMP.numCompanyRating AS numCompanyRating,                                             
     LD.vcData as vcRating,                                              
     DM.numStatusID AS numStatusID,                                             
     LD1.vcData as vcStatus,                                            
     DM.numCreatedby AS numCreatedby, DM.numRecOwner                                           
    FROM  CompanyInfo CMP                                          
    join DivisionMaster DM on DM.numCompanyID=CMP.numCompanyID                              
    join AdditionalContactsInformation ADC on ADC.numDivisionID=DM.numDivisionID                    
    left join ListDetails LD on LD.numListItemID=CMP.numCompanyRating                                           
    left join ListDetails LD1 on LD1.numListItemID= DM.numStatusID                                         
  WHERE ISNULL(ADC.bitPrimaryContact,0)=1 '                                        
                                        
-- No of Leads prmoted to Prospects                                        
if @Condition ='L' and @RepType=1  set @strSql= @strSql +' AND DM.tintCRMType= 1 and bintLeadProm between '''+convert(varchar(20),@numStartDate)+'''                                          
and '''+convert(varchar(20),@numEndDate)+'''                                             
and bintLeadPromBy in (select numContactId from AdditionalContactsInformation                 
where numTeam in(select numTeam from ForReportsByTeam where numUserCntID='+convert(varchar(15),@numUserCntID)+        
' and numDomainID='+convert(varchar(15),@numDomainId)+' and tintType='+ convert(varchar(15),@numTeamType)+')) ORDER BY ' + @columnName +' '+ @columnSortOrder        
        
              
                           
if @Condition ='L' and @RepType=2  set @strSql= @strSql +'AND DM.tintCRMType= 1 and bintLeadProm between '''+convert(varchar(20),@numStartDate)+'''                                          
 and '''+convert(varchar(20),@numEndDate)+'''                                             
 and bintLeadPromBy ='+convert(varchar(15),@numDomainId)+' ORDER BY ' + @columnName +' '+ @columnSortOrder                                        
                                        
-- Total Prospects Added                                        
if @Condition ='P' and @RepType=1  set @strSql= @strSql +'and DM.tintCRMType=1 and ((DM.bintCreatedDate between '''+convert(varchar(20),@numStartDate)+'''                                         
and '''+convert(varchar(20),@numEndDate)+''' and DM.numCreatedBy in (select numContactId from AdditionalContactsInformation                                           
where numTeam in(select numTeam from ForReportsByTeam where numUserCntID='+convert(varchar(15),@numUserCntID)+' and numDomainID='+convert(varchar(15),@numDomainId)+' and tintType='+ convert(varchar(15),@numTeamType)+')))                                   
  
   
or (bintLeadProm between '''+convert(varchar(20),@numStartDate)+''' and '''+convert(varchar(20),@numEndDate)+''' and bintLeadPromBy in (select numContactId from AdditionalContactsInformation                                           
where numTeam in(select numTeam from ForReportsByTeam where numUserCntID='+convert(varchar(15),@numUserCntID)+' and numDomainID='+convert(varchar(15),@numDomainId)+' and tintType='+ convert(varchar(15),@numTeamType)+'))))  ORDER BY ' + @columnName +' '+  
  
    
 @columnSortOrder        
            
                                        
if @Condition ='P' and @RepType=2  set @strSql= @strSql +'and DM.tintCRMType=1 and ((DM.bintCreatedDate between '''+convert(varchar(20),@numStartDate)+'''                                         
and '''+convert(varchar(20),@numEndDate)+''' and DM.numCreatedBy ='+convert(varchar(15),@numUserCntID)+')                                               
   or (bintLeadProm between '''+convert(varchar(20),@numStartDate)+''' and '''+convert(varchar(20),@numEndDate)+'''                                         
 and bintLeadPromBy ='+convert(varchar(15),@numUserCntID)+'))  ORDER BY ' + @columnName +' '+ @columnSortOrder                                        
                                        
                                        
                                        
-- Total Prospects Promoted to Accounts                                        
if @Condition ='R' and @RepType=1  set @strSql= @strSql +' and DM.tintCRMType=2 and bintProsProm between '''+convert(varchar(20),@numStartDate)+''' and '''+convert(varchar(20),@numEndDate)+''' and bintProsPromBy       
in (select numContactId from AdditionalContactsInformation                                            
where numTeam in(select numTeam from ForReportsByTeam                                         
where numUserCntID='+convert(varchar(15),@numUserCntID)+' and numDomainID='+convert(varchar(15),@numDomainId)+' and tintType='+ convert(varchar(15),@numTeamType)+'))  ORDER BY ' + @columnName +' '+ @columnSortOrder                                         
 
                                        
if @Condition ='R' and @RepType=2  set @strSql= @strSql +' and DM.tintCRMType=2                                         
and bintProsProm between '''+convert(varchar(20),@numStartDate)+''' and '''+convert(varchar(20),@numEndDate)+''' and bintProsPromBy ='+convert(varchar(15),@numUserCntID)+'  ORDER BY ' + @columnName +' '+ @columnSortOrder                                   
 
     
     
                            
-- Total Accounts Added                                        
if @Condition ='A' and @RepType=1  set @strSql= @strSql +' and DM.tintCRMType=2 and ((DM.bintCreatedDate between '''+convert(varchar(20),@numStartDate)+''' and '''+convert(varchar(20),@numEndDate)+'''                                         
and DM.numRecOwner in (select numContactId from AdditionalContactsInformation                                           
where numTeam in(select numTeam from ForReportsByTeam where numUserCntID='+convert(varchar(15),@numUserCntID)+' and numDomainID='+convert(varchar(15),@numDomainId)+' and tintType='+ convert(varchar(15),@numTeamType)+')))                                   
  
    
or (bintProsProm between '''+convert(varchar(20),@numStartDate)+''' and '''+convert(varchar(20),@numEndDate)+'''                                         
and bintProsPromBy in (select numContactId from AdditionalContactsInformation                                             
where numTeam in(select numTeam from ForReportsByTeam where numUserCntID='+convert(varchar(15),@numUserCntID)+'                                         
and numDomainID='+convert(varchar(15),@numDomainId)+' and tintType='+ convert(varchar(15),@numTeamType)+'))))  ORDER BY ' + @columnName +' '+ @columnSortOrder                                          
                                        
if @Condition ='A' and @RepType=2  set @strSql= @strSql +' and DM.tintCRMType=2 and (DM.bintCreatedDate between '''+convert(varchar(20),@numStartDate)+''' and '''+convert(varchar(20),@numEndDate)+'''                                         
and DM.numRecOwner ='+convert(varchar(15),@numUserCntID)+')                                               
   or (bintProsProm between '''+convert(varchar(20),@numStartDate)+''' and '''+convert(varchar(20),@numEndDate)+'''                                         
and bintProsPromBy ='+convert(varchar(15),@numUserCntID)+')  ORDER BY ' + @columnName +' '+ @columnSortOrder                                        
                                            
                                        
print @strSql                                        
insert into #tempTable (CompanyName,                                          
        numTerID,                                          
        PrimaryContact,                                          
        Phone,                                          
   vcEmail,                                          
   numCompanyID ,                                          
   numDivisionID,                                          
   numContactID,                                          
   numCompanyRating,                                          
   vcRating,                                          
   numStatusID,                                          
   vcStatus,                                          
   numCreatedby,                                          
   numRecOwner)                                          
exec( @strSql)                                          
  declare @firstRec as integer                                          
  declare @lastRec as integer                                          
 set @firstRec= (@CurrentPage-1) * @PageSize                                          
     set @lastRec= (@CurrentPage*@PageSize+1)                                          
select * from #tempTable where ID > @firstRec and ID < @lastRec                                          
set @TotRecs=(select count(*) from #tempTable)                                          
drop table #tempTable                                   
end                                  
              
if @RepType =3                                  
begin                                  
                                  
set @strSql='SELECT ADC1.numContactId,isnull(vcFirstName,''-'') as vcFirstName,isnull(vcLastName,''-'') as vcLastName,(SELECT  count(*)                                           
    FROM  CompanyInfo CMP                                
    join DivisionMaster DM on DM.numCompanyID=CMP.numCompanyID                                 
    join AdditionalContactsInformation ADC on ADC.numDivisionID=DM.numDivisionID WHERE ISNULL(ADC.bitPrimaryContact,0)=1'                                        
                                        
-- No of Leads prmoted to Prospects                                        
if @Condition ='L'  set @strSql= @strSql + '  and bintLeadPromBy=ADC1.numRecOwner AND DM.tintCRMType= 1                        
and  (bintLeadProm between '''+ convert(varchar(20),@numStartDate) +''' and '''+ convert(varchar(20),@numEndDate) +''' ) ) as NoofOpp FROM  AdditionalContactsInformation ADC1                                    
WHERE ADC1.numContactId in ('+@strUserIDs+')'                                    
                      if @Condition ='P'   set @strSql= @strSql +'and DM.tintCRMType=1 and ((DM.bintCreatedDate between '''+convert(varchar(20),@numStartDate)+'''                                         
and '''+convert(varchar(20),@numEndDate)+''' and DM.numRecOwner =ADC1.numContactId) or (bintLeadProm between '''+convert(varchar(20),@numStartDate)+''' and '''+convert(varchar(20),@numEndDate)+'''                          
 and bintLeadPromBy =ADC1.numContactId) )) as NoofOpp FROM  AdditionalContactsInformation ADC1                                     
WHERE  ADC1.numContactId in ('+@strUserIDs+')'                                  
                                  
if @Condition ='R' set @strSql= @strSql +' and DM.tintCRMType=2                                         
and bintProsProm between '''+convert(varchar(20),@numStartDate)+''' and '''+convert(varchar(20),@numEndDate)+''' and bintProsPromBy =ADC1.numContactId) as NoofOpp FROM  AdditionalContactsInformation ADC1                                       
WHERE  ADC1.numContactId in ('+@strUserIDs+')'                                   
                                  
if @Condition ='A'  set @strSql= @strSql +' and DM.tintCRMType=2 and (DM.bintCreatedDate between '''+convert(varchar(20),@numStartDate)+''' and '''+convert(varchar(20),@numEndDate)+'''                                        
and DM.numRecOwner =ADC1.numContactId )                                               
   or (bintProsProm between '''+convert(varchar(20),@numStartDate)+''' and '''+convert(varchar(20),@numEndDate)+'''                                         
and bintProsPromBy  =ADC1.numContactId )) as NoofOpp FROM  AdditionalContactsInformation ADC1                                     
WHERE  ADC1.numContactId in ('+@strUserIDs+')'                                    
                                  
print (@strSql)                                  
exec (@strSql)                                  
end
GO
