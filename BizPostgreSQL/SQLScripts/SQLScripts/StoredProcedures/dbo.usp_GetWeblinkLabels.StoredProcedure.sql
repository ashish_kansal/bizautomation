/****** Object:  StoredProcedure [dbo].[usp_GetWeblinkLabels]    Script Date: 07/26/2008 16:18:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getweblinklabels')
DROP PROCEDURE usp_getweblinklabels
GO
CREATE PROCEDURE [dbo].[usp_GetWeblinkLabels]     
 @numDivisionID numeric = 0       
AS    
  
  SELECT top 1 vcWebLabel1 As vcWebLink1Label, vcWebLabel2 as vcWebLink2Label, vcWebLabel3 as vcWebLink3Label, vcWebLabel4 as vcWebLink4Label    
   FROM CompanyInfo C
   join DivisionMaster D
   on D.numCompanyID=C.numCompanyID    
   WHERE numDivisionID=@numDivisionID
GO
