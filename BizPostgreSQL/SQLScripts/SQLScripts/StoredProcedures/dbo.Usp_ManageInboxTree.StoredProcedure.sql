/****** Object:  StoredProcedure [dbo].[Usp_ManageInboxTree]    Script Date: 07/26/2008 16:19:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_manageinboxtree')
DROP PROCEDURE usp_manageinboxtree
GO
CREATE PROCEDURE [dbo].[Usp_ManageInboxTree]    
@numUserCntID as numeric(9),    
@numDomainID as numeric(9),    
@NodeID as numeric(9),    
@NodeName as varchar(5000),    
@mode as tinyint,    
@ParentId as numeric(9),
@ParentNodeText as VARCHAR(5000)='',
@numLastUid as numeric(18,0)=0,
@bitHidden AS BIT=0
as      
    
if @mode = 0      
begin      
IF Not Exists (Select 'col1' from  InboxTreeSort where numdomainid=@numDomainID AND numUserCntId=@numUserCntID)
		 BEGIN
			 select numNodeID,numParentId,vcName as vcNodeName ,ISNULL(numLastUid,0) AS numLastUid,ISNULL(numLastUid,0) AS numLastUid 
			 ,ISNULL(numOldEmailLastUid,0) AS numOldEmailLastUid
			 from InboxTree where (numdomainid=0 or numdomainid=@numDomainID )       
			   and (numUserCntId=@numUserCntID or numUserCntId=0)      
			 order by numNodeID,numParentId 
		 END
   ELSE 
		BEGIN
			SELECT numNodeID,numParentId,CASE WHEN ISNULL(vcNodeName,'')='INBOX' THEN 'Inbox' ELSE ISNULL(vcNodeName,'') END as vcNodeName ,numSortOrder, bitSystem, numFixID ,ISNULL(numLastUid,0) AS numLastUid
			,ISNULL(numOldEmailLastUid,0) AS numOldEmailLastUid
				FROM InboxTreeSort 
				WHERE (numdomainid=@numDomainID)   
					AND (numUserCntId=@numUserCntID )   
                    
			    ORDER BY numSortOrder ASC    
		END     
end      
if @mode = 1      
begin   
	DECLARE @ErrorMessage NVARCHAR(4000)
	SET @ErrorMessage=''
	DECLARE @ErrorSeverity INT
	DECLARE @ErrorState INT
		
	BEGIN TRANSACTION

	BEGIN TRY
		--Changed by sachin sadhu|| Date:21stJan2014
    --Purpose:Bug on creating Folders||VirginDrinks
		DECLARE @numSortOrder NUMERIC
		SELECT @numSortOrder=MAX(numSortOrder)+1 FROM dbo.InboxTreeSort WHERE numDomainID=@numDomainID AND numUserCntID=@numUserCntID   
		IF(@ParentNodeText<>'')
		BEGIN
			SET @ParentId=(SELECT TOP 1 numNodeID FROM dbo.InboxTreeSort WHERE numDomainID=@numDomainID AND numUserCntID=@numUserCntID AND vcNodeName=@ParentNodeText)
		END
		IF(@ParentId=0)
		BEGIN
			SELECT @ParentId=numNodeID FROM dbo.InboxTreeSort WHERE numDomainID=@numDomainID AND numUserCntID=@numUserCntID AND numParentID IS NULL
		END

		IF EXISTS (SELECT numNodeID FROM InboxTreeSort WHERE numDomainID=@numDomainID AND numUserCntID=@numUserCntID AND numParentID = @ParentId AND LOWER(vcNodeName) = LOWER(@NodeName))
		BEGIN
			RAISERROR ('FOLDER_ALREADY_EXISTS',16,1)
			RETURN -1
		END 
		ELSE
		BEGIN
			INSERT INTO [InboxTree]      
				(vcName      
				,numParentId      
				,[numDomainId],numUserCntId)      
			VALUES      
				(@NodeName      
				,@ParentId      
				,@numDomainID,@numUserCntId)   
			--To Save values into InboxTree Sort
			INSERT INTO [InboxTreeSort]      
				(vcNodeName      
				,numParentId      
				,[numDomainId],numUserCntId,numSortOrder)      
			VALUES      
				(@NodeName      
				,@ParentId      
				,@numDomainID,@numUserCntId,@numSortOrder) 
		END
	END TRY
	BEGIN CATCH
		SELECT @ErrorMessage = ERROR_MESSAGE(), @ErrorSeverity = ERROR_SEVERITY(), @ErrorState = ERROR_STATE();

		IF @@TRANCOUNT > 0
			ROLLBACK TRANSACTION;

		RAISERROR (@ErrorMessage,@ErrorSeverity,@ErrorState);
	END CATCH

	IF @@TRANCOUNT > 0
			COMMIT TRANSACTION;
end      
      
if @mode = 2      
begin      
     
update  emailhistory set numNodeid=2 where numdomainid=@numDomainID and numUserCntId=@numUserCntID and  numNodeid in    
  ( select numNodeId from inboxTree where (numNodeID = @NodeID or numParentId = @NodeID)  and       
   numdomainid=@numDomainID and numUserCntId=@numUserCntID )     
   
 delete [InboxTree] where (numNodeID = @NodeID or numParentId = @NodeID)  and       
   numdomainid=@numDomainID and numUserCntId=@numUserCntID    
--Update Email History with [InboxTreeSort]
update  emailhistory set numNodeid=2 where numdomainid=@numDomainID and numUserCntId=@numUserCntID and  numNodeid in    
  ( select numNodeId from InboxTreeSort where (numNodeID = @NodeID or numParentId = @NodeID)  and       
   numdomainid=@numDomainID and numUserCntId=@numUserCntID )  
      
 delete [InboxTreeSort] where (numNodeID = @NodeID or numParentId = @NodeID)  and       
   numdomainid=@numDomainID and numUserCntId=@numUserCntID    
end  
if @mode=3
BEGIN
	BEGIN TRY
	BEGIN TRAN
		DECLARE @TEMPFolder TABLE
		(
			vcFolderName VARCHAR(300)
			,vcParentFolderName VARCHAR(300)
		)

		IF ISNULL(@NodeName,'') <> ''
		BEGIN
			DECLARE @hDocItem int
			EXEC sp_xml_preparedocument @hDocItem OUTPUT, @NodeName

			INSERT INTO @TEMPFolder
			(
				vcFolderName
				,vcParentFolderName
			)
			SELECT 
				vcFolderName
				,vcParentFolderName 
			FROM 
				OPENXML (@hDocItem, '/NewDataSet/Table1',2)
			WITH 
			(
				vcFolderName VARCHAR(300)
				,vcParentFolderName VARCHAR(300)
			)

			EXEC sp_xml_removedocument @hDocItem
		END

		DELETE FROM InboxTreeSort WHERE numDomainID=@numDomainID AND numUserCntID=@numUserCntID AND vcNodeName NOT IN (SELECT vcFolderName FROM @TEMPFolder)
		
		INSERT INTO [InboxTreeSort]      
				(vcNodeName 
				,numLastUid     
				,numParentId      
				,[numDomainId],numUserCntId,numSortOrder)        
				SELECT 
				DISTINCT
				SUBSTRING(Items,CHARINDEX('#',Items)+1,LEN(Items)),  
				0,    
				0,      
				@numDomainID,
				@numUserCntId ,
				0
				--ROW_NUMBER() OVER( ORDER BY Items )
				FROM Split(@NodeName,',') WHERE Items<>'' AND 
				SUBSTRING(Items,CHARINDEX('#',Items)+1,LEN(Items))<>'' AND
				SUBSTRING(Items,CHARINDEX('#',Items)+1,LEN(Items)) NOT IN (SELECT ISNULL(vcNodeName,'') FROM InboxTreeSort WHERE numDomainId=@numDomainID AND numUserCntId=@numUserCntID) 
				
				
			--To Save values into InboxTree Sort
			INSERT INTO [InboxTreeSort]      
				(vcNodeName  
				,numLastUid    
				,numParentId      
				,[numDomainId],numUserCntId,numSortOrder)        
				SELECT 
				DISTINCT
				vcFolderName,  
				0,    
				(SELECT TOP 1 numNodeID FROM InboxTreeSort WHERE numDomainID=@numDomainID AND numUserCntID=@numUserCntID AND vcNodeName=vcParentFolderName),      
				@numDomainID,
				@numUserCntId,
				0
				--ROW_NUMBER() OVER( ORDER BY Items )
				FROM 
					@TEMPFolder TF 
				WHERE 
					ISNULL(TF.vcFolderName,'') <> ''
				AND TF.vcFolderName NOT IN (SELECT ISNULL(vcNodeName,'') FROM InboxTreeSort WHERE numDomainId=@numDomainID AND numUserCntId=@numUserCntID)
				
				IF((SELECT COUNT(*) FROM ImapUserDetails WHERE numDomainId=@numDomainID AND numUserCntId=@numUserCntID AND vcImapServerUrl='outlook.office365.com')>0)
				BEGIN
					UPDATE InboxTreeSort SET vcNodeName='Inbox' WHERE numDomainId=@numDomainID AND numUserCntId=@numUserCntID AND vcNodeName='INBOX'
				END
	COMMIT TRAN
	END TRY
	BEGIN CATCH
		ROLLBACK TRAN	         
	END CATCH
END
if @mode=4
BEGIN
	IF((SELECT TOP 1 ISNULL(numLastUid,0) FROM InboxTreeSort WHERE numUserCntId=@numUserCntID AND numDomainId=@numDomainID AND numNodeID=@NodeID)=0)
	BEGIN
		UPDATE InboxTreeSort SET numOldEmailLastUid=@numLastUid WHERE numUserCntId=@numUserCntID AND numDomainId=@numDomainID AND numNodeID=@NodeID
	END
	UPDATE InboxTree SET numLastUid=@numLastUid WHERE numUserCntId=@numUserCntID AND numDomainId=@numDomainID AND numNodeID=@NodeID
	UPDATE InboxTreeSort SET numLastUid=@numLastUid WHERE numUserCntId=@numUserCntID AND numDomainId=@numDomainID AND numNodeID=@NodeID
END
if @mode=5
BEGIN
	UPDATE InboxTree SET numOldEmailLastUid=@numLastUid WHERE numUserCntId=@numUserCntID AND numDomainId=@numDomainID AND numNodeID=@NodeID
	UPDATE InboxTreeSort SET numOldEmailLastUid=@numLastUid WHERE numUserCntId=@numUserCntID AND numDomainId=@numDomainID AND numNodeID=@NodeID
END


if @mode = 7      
begin      
IF Not Exists (Select 'col1' from  InboxTreeSort where numdomainid=@numDomainID AND numUserCntId=@numUserCntID)
		 BEGIN
			 select numNodeID,numParentId,vcName as vcNodeName ,ISNULL(numLastUid,0) AS numLastUid,ISNULL(numLastUid,0) AS numLastUid 
			 ,ISNULL(numOldEmailLastUid,0) AS numOldEmailLastUid
			 from InboxTree where (numdomainid=0 or numdomainid=@numDomainID )       
			   and (numUserCntId=@numUserCntID or numUserCntId=0)    AND  numNodeID=@NodeId
			 order by numNodeID,numParentId 
		 END
   ELSE 
		BEGIN
			SELECT numNodeID,numParentId,ISNULL(vcNodeName,'') as vcNodeName ,numSortOrder, bitSystem, numFixID ,ISNULL(numLastUid,0) AS numLastUid
			,ISNULL(numOldEmailLastUid,0) AS numOldEmailLastUid
				FROM InboxTreeSort 
				WHERE (numdomainid=@numDomainID)   
					AND (numUserCntId=@numUserCntID )     AND   numNodeID=@NodeId  
                    
			    ORDER BY numSortOrder ASC    
		END     
END
if @mode=8
BEGIN
	UPDATE InboxTreeSort SET bitHidden=@bitHidden WHERE numUserCntId=@numUserCntID AND numDomainId=@numDomainID AND numNodeID=@NodeID
END

if @mode=9
BEGIN
	UPDATE InboxTreeSort SET numParentID=0,numSortOrder=(SELECT MIN(ISNULL(numSortOrder,0)-1) FROM InboxTreeSort WHERE numUserCntId=@numUserCntID AND numDomainId=@numDomainID) WHERE numUserCntId=@numUserCntID AND numDomainId=@numDomainID AND numNodeID=@NodeID
	UPDATE InboxTreeSort SET numSortOrder=(ISNULL(numSortOrder,0)+1) WHERE numUserCntId=@numUserCntID AND numDomainId=@numDomainID AND numNodeID<>@NodeID AND ISNULL(numSortOrder,0)>0

END
    
if @mode = 10      
begin      
IF Not Exists (Select 'col1' from  InboxTreeSort where numdomainid=@numDomainID AND numUserCntId=@numUserCntID)
		 BEGIN
			 select numNodeID,numParentId,vcName as vcNodeName ,ISNULL(numLastUid,0) AS numLastUid,ISNULL(numLastUid,0) AS numLastUid 
			 ,ISNULL(numOldEmailLastUid,0) AS numOldEmailLastUid
			 from InboxTree where (numdomainid=0 or numdomainid=@numDomainID )       
			   and (numUserCntId=@numUserCntID or numUserCntId=0)      
			 order by numNodeID,numParentId 
		 END
   ELSE 
		BEGIN
			SELECT numNodeID,numParentId,CASE WHEN ISNULL(vcNodeName,'')='INBOX' THEN 'Inbox' ELSE ISNULL(vcNodeName,'') END as vcNodeName ,numSortOrder, bitSystem, numFixID ,ISNULL(numLastUid,0) AS numLastUid
			,ISNULL(numOldEmailLastUid,0) AS numOldEmailLastUid
				FROM InboxTreeSort 
				WHERE (numdomainid=@numDomainID)   
					AND (numUserCntId=@numUserCntID )   
                    AND ISNULL(bitHidden,0)=0
			    ORDER BY numSortOrder ASC    
		END     
end  
GO

--SELECT SUBSTRING(Items,1,CHARINDEX('#',Items)-1) AS NodeName,SUBSTRING(Items,CHARINDEX('#',Items)+1,LEN(Items)) AS ParentNodeName FROM Split('APA#12,ASOA#1234,ASOA#1245,ASA#181,h#',',') WHERE Items<>''
--SELECT SUBSTRING(Items,CHARINDEX('#',Items)+1,LEN(Items)) FROM Split('APA#12,ASOA#1234,ASOA#1245,ASA#181,h#',',') WHERE Items<>''
--SELECT Items FROM Split('APA#12,ASOA#1234,ASOA#1245,ASA#181,h#',',') WHERE Items<>''