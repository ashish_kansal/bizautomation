/****** Object:  StoredProcedure [dbo].[USP_GetCompanyCheckDetails]    Script Date: 07/26/2008 16:16:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created By Siva              
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getcompanycheckdetails')
DROP PROCEDURE usp_getcompanycheckdetails
GO
CREATE PROCEDURE [dbo].[USP_GetCompanyCheckDetails]              
(@numDomainId as numeric(9)=0,     
 @numCheckId as numeric(9)=0,         
 @numCheckCompanyId as numeric(9)=0,    
 @byteMode as smallint=0          
)              
AS              
Begin          
Declare @lSQL Varchar(8000)        
if @bytemode=0     
 Begin           
 Set @lSQL='Select DM.numDivisionID AS numDivisionID,CI.vcCompanyName AS vcCompanyName,CD.numCheckCompanyId as numCheckCompanyId,CD.monAmount as monAmount,              
  CD.vcMemo as vcMemo From CheckCompanyDetails CD              
  inner join DivisionMaster DM on CD.numCompanyId=DM.numDivisionID               
  inner join CompanyInfo CI on DM.numCompanyid=CI.numCompanyid         
  Where CD.numDomainId='+convert(varchar(100),@numDomainId) + ' And CD.numCheckCompanyId not in(Select isnull(numCheckCompanyId,0) From CheckDetails Where  numDomainId='+Convert(varchar(10),@numDomainId)+')'        
 End    
 Else If @byteMode=1     
 Begin    
 Set @lSQL='Select DM.numDivisionID AS numDivisionID,CI.vcCompanyName AS vcCompanyName,CD.numCheckCompanyId as numCheckCompanyId,CD.monAmount as monAmount,              
  CD.vcMemo as vcMemo From CheckCompanyDetails CD              
  inner join DivisionMaster DM on CD.numCompanyId=DM.numDivisionID               
  inner join CompanyInfo CI on DM.numCompanyid=CI.numCompanyid         
  Where CD.numDomainId='+convert(varchar(100),@numDomainId) + ' And CD.numCheckCompanyId not in(Select isnull(numCheckCompanyId,0) From CheckDetails Where numCheckId<>'+Convert(varchar(10),@numCheckId)+' And numDomainId='+Convert(varchar(10),@numDomainId)+')'      
  
    
 End    
    
if @numCheckCompanyId <> 0 Set @lSQL=@lSQL+ 'And CD.numCheckCompanyId<>'+Convert(varchar(100),@numCheckCompanyId)          
  print @lSQL        
Exec (@lSQL)          
           
End
GO
