/****** Object:  StoredProcedure [dbo].[USP_GetCustomData]    Script Date: 07/26/2008 16:17:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getcustomdata')
DROP PROCEDURE usp_getcustomdata
GO
CREATE PROCEDURE [dbo].[USP_GetCustomData]
@Options as numeric
as 
select * from CustomReptOptValues where intOptions=@Options
GO
