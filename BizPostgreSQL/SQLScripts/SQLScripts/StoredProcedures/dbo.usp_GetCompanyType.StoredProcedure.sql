/****** Object:  StoredProcedure [dbo].[usp_GetCompanyType]    Script Date: 07/26/2008 16:16:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getcompanytype')
DROP PROCEDURE usp_getcompanytype
GO
CREATE PROCEDURE [dbo].[usp_GetCompanyType]
	@numCompanyId numeric 
--  
AS
	SELECT numCompanyType FROM companyinfo WHERE numCompanyId=@numCompanyId
GO
