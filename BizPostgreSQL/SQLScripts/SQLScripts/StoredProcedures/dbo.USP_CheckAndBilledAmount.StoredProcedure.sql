
/****** Object:  StoredProcedure [dbo].[USP_CheckAndBilledAmount]    Script Date: 03/20/2015 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_CheckAndBilledAmount')
DROP PROCEDURE USP_CheckAndBilledAmount
GO
CREATE PROCEDURE [dbo].[USP_CheckAndBilledAmount]
(
	@numDivisionID			NUMERIC(18,0),
	@numOppBizDocId			NUMERIC(18,0),
	@numBillID				NUMERIC(18,0),
	@numAmount				DECIMAL(20,5),
	@numDomainID			NUMERIC(18,0)
)
AS 
BEGIN
	
	IF @numBillID > 0
	BEGIN

		SELECT  CASE WHEN ISNULL(BH.[monAmountDue],0) = @numAmount THEN 1
					 WHEN ISNULL(BH.[monAmountDue],0) >= ISNULL(SUM(monAmount),0) + @numAmount THEN 2
					 WHEN ISNULL(BH.[monAmountDue],0) < ISNULL(SUM(monAmount),0) + @numAmount THEN 3
				END [IsPaidInFull]
		FROM [dbo].[BillPaymentDetails] AS BPD 
		JOIN [dbo].[BillPaymentHeader] AS BPH ON [BPH].[numBillPaymentID] = BPD.[numBillPaymentID]
		JOIN [dbo].[BillHeader] AS BH ON BH.[numBillID] = BPD.[numBillID]
		WHERE [BH].[numBillID] = @numBillID
		AND [BH].[numDomainID] = @numDomainID
		AND [BH].[numDivisionID] = @numDivisionID
		GROUP BY [monAmountDue]

	END

	ELSE IF @numOppBizDocId > 0
	BEGIN

		SELECT  CASE WHEN ISNULL([OBD].[monDealAmount],0) = @numAmount THEN 1
					 WHEN ISNULL([OBD].[monDealAmount],0) = ISNULL(SUM(BPD.monAmount),0) + @numAmount THEN 2
					 WHEN ISNULL([OBD].[monDealAmount],0) > ISNULL(SUM(BPD.monAmount),0) + @numAmount THEN 3
					 WHEN ISNULL([OBD].[monDealAmount],0) < ISNULL(SUM(BPD.monAmount),0) + @numAmount THEN 4
				END [IsPaidInFull]
		FROM [dbo].[BillPaymentDetails] AS BPD 
		JOIN [dbo].[BillPaymentHeader] AS BPH ON [BPH].[numBillPaymentID] = BPD.[numBillPaymentID]
		JOIN [dbo].[OpportunityBizDocs] AS OBD ON OBD.[numOppBizDocsId] = [BPD].[numOppBizDocsID]
		JOIN [dbo].[OpportunityMaster] AS OM ON OM.[numOppId] = [OBD].[numOppId] AND [BPH].[numDomainID] = OM.[numDomainId]
		WHERE [OBD].[numOppBizDocsId] = @numOppBizDocId
		AND [OM].[numDomainID] = @numDomainID
		AND [OM].[numDivisionID] = @numDivisionID
		GROUP BY [OBD].[monDealAmount]

	END

		
	--SELECT CASE WHEN ISNULL([OBD].[monDealAmount],0) = @numAmount THEN 1 -- If paid amount + new paid amount is equal to total deal amount, Fully Paid
	--			WHEN ISNULL([OBD].[monDealAmount],0) > SUM(ISNULL(DD.[monAmountPaid],0)) + @numAmount THEN 2 -- If paid amount + new paid amount is greater than total deal amount, Due Amounts
	--			WHEN ISNULL([OBD].[monDealAmount],0) < SUM(ISNULL(DD.[monAmountPaid],0)) + @numAmount THEN 3 -- If paid amount + new paid amount is less than total deal   amount, Unapplied Amounts
	--			ELSE 0
	--	   END AS [IsPaidInFull],
	--	   [DM].[numDivisionID],
	--	   [DM].[numDomainId],
	--	   [DD].[numOppBizDocsID],
	--	   ISNULL([OBD].[monDealAmount],0)  [monDealAmount],
	--	   SUM(ISNULL(DD.[monAmountPaid],0)) [monAmountPaid],
	--	   SUM(ISNULL(DD.[monAmountPaid],0)) + @numAmount [NewAmountPaid],
	--	   (SUM(ISNULL(DD.[monAmountPaid],0)) + @numAmount) - ISNULL([OBD].[monDealAmount],0) [OverPaidAmount]
	--FROM [dbo].[DepositMaster] AS DM 
	--JOIN [dbo].[DepositeDetails] AS DD ON [DM].[numDepositId] = [DD].[numDepositID]
	--JOIN [dbo].[OpportunityMaster] AS OM ON OM.[numOppId] = DD.[numOppID] AND OM.[numDomainId] = DM.[numDomainId]
	--JOIN [dbo].[OpportunityBizDocs] AS OBD ON [OBD].[numOppBizDocsId] = DD.[numOppBizDocsID] AND [OM].[numOppId] = [OBD].[numOppId]
	--WHERE [DM].[numDomainId] = @numDomainID
	--AND [DM].[numDivisionID] = @numDivisionID
	--AND DD.[numOppBizDocsID] = @numOppBizDocId
	--GROUP BY [OBD].[monDealAmount],
	--		 [DM].[numDivisionID],
	--		 [DM].[numDomainId],
	--		 [DD].[numOppBizDocsID]

END

GO
