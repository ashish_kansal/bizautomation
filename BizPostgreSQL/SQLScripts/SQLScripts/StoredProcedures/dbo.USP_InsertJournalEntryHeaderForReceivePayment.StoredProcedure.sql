/****** Object:  StoredProcedure [dbo].[USP_InsertJournalEntryHeaderForReceivePayment]    Script Date: 07/26/2008 16:19:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created by Siva                      
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_insertjournalentryheaderforreceivepayment')
DROP PROCEDURE usp_insertjournalentryheaderforreceivepayment
GO
CREATE PROCEDURE [dbo].[USP_InsertJournalEntryHeaderForReceivePayment]                                    
@datEntry_Date as datetime,                                    
@numAmount as DECIMAL(20,5),                                  
@numJournal_Id as numeric(9)=0,                              
@numDomainId as numeric(9)=0,                      
@numRecurringId as numeric(9)=0,                  
@numUserCntID as numeric(9)=0,    
@numBizDocsPaymentDetId as numeric(9)=0,  
@numOppBizDocsId as numeric(9)=0,  
@numOppId as numeric(9)=0 ,
@numCheckNo as numeric(9)=0,
@numProjectID AS NUMERIC=0,
@numClassID AS NUMERIC=0
As                                    
Begin   
set @numCheckNo=(case @numCheckNo when 0 then null else @numCheckNo end);
 if @numRecurringId=0 set @numRecurringId=null                                 
 IF @numOppId = 0 SET @numOppId =NULL /*added by chintan for clean data and while using inner join it should not be skipped*/
 IF @numProjectID = 0 SET @numProjectID =NULL 
 IF @numClassID = 0 SET @numClassID =NULL 
 IF @numOppBizDocsId=0 SET @numOppBizDocsId = NULL 
 If @numJournal_Id=0                            
  Begin                            
   Insert into General_Journal_Header(datEntry_Date,numAmount,numDomainId,numRecurringId,numCreatedBy,datCreatedDate,numBizDocsPaymentDetId,numOppBizDocsId,numOppId,numCheckId,numProjectID,numClassID)            
   Values(@datEntry_Date,@numAmount,@numDomainId,@numRecurringId,@numUserCntID,getutcdate(),@numBizDocsPaymentDetId,@numOppBizDocsId,@numOppId,@numCheckNo,@numProjectID,@numClassID)
   Set @numJournal_Id = SCOPE_IDENTITY()                                  
   Select  @numJournal_Id                              
  End                            
End
GO
