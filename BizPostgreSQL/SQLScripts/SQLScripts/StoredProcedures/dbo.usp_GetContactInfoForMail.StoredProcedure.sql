/****** Object:  StoredProcedure [dbo].[usp_GetContactInfoForMail]    Script Date: 07/26/2008 16:16:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getcontactinfoformail')
DROP PROCEDURE usp_getcontactinfoformail
GO
CREATE PROCEDURE [dbo].[usp_GetContactInfoForMail]
	@vcEmail varchar(50)='',
	@numDomainID numeric(9)=0   
--
AS
BEGIN
	SELECT 
		AC.vcFirstName,AC.vcLastName,CI.vcWebSite,AC.vcEmail,CI.vcCompanyname
		
	FROM 	
		CompanyInfo CI
	INNER JOIN
		DivisionMaster DM
	ON
		DM.numCompanyID=CI.numCompanyID
	INNER JOIN	
		 AdditionalContactsInformation AC
	ON
		AC.numDivisionId=DM.numDivisionID
	WHERE 
		AC.vcEmail like @vcEmail
		and AC.numDomainID=@numDomainID
		
	
END
GO
