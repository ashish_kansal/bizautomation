/****** Object:  StoredProcedure [dbo].[usp_GetContactForCase]    Script Date: 07/26/2008 16:16:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getcontactforcase')
DROP PROCEDURE usp_getcontactforcase
GO
CREATE PROCEDURE [dbo].[usp_GetContactForCase]
	@numDomainID numeric(9)=0   
--
	
AS
BEGIN
	SELECT numContactID,vcFirstName,vcLastName,numCreatedBy FROM AdditionalContactsInformation
	WHERE numDomainID=@numDomainID AND numCreatedBy IS NOT NULL 
	
END
GO
