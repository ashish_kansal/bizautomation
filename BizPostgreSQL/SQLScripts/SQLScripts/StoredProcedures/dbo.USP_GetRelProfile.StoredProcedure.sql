/****** Object:  StoredProcedure [dbo].[USP_GetRelProfile]    Script Date: 07/26/2008 16:18:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj        
GO
IF EXISTS ( SELECT  * FROM    sysobjects WHERE   xtype = 'p' AND NAME = 'usp_getrelprofile' ) 
DROP PROCEDURE usp_getrelprofile
GO
CREATE PROCEDURE [dbo].[USP_GetRelProfile]
    @numRelationship AS NUMERIC(9),
    @numDomainID AS NUMERIC(9) = 0
AS 
SELECT  numSecondaryListItemID numProfileID,
        L2.vcData AS ProName,
        numPrimaryListItemID AS numRelationshipID,
        L1.vcData AS RelName
FROM    FieldRelationship FR
        JOIN FieldRelationshipDTL FRDTL ON FR.numFieldRelID = FRDTL.numFieldRelID
        JOIN ListDetails L1 ON numPrimaryListItemID = L1.numListItemID
        JOIN ListDetails L2 ON numSecondaryListItemID = L2.numListItemID
WHERE   numPrimaryListID = 5
        AND numSecondaryListID = 21
        AND FR.numDomainID = @numDomainID
        AND numPrimaryListItemID = @numRelationship
        AND L2.numDomainID = @numDomainID 
GO
