/****** Object:  StoredProcedure [dbo].[Usp_DelScheduled]    Script Date: 07/26/2008 16:15:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_delscheduled')
DROP PROCEDURE usp_delscheduled
GO
CREATE PROCEDURE [dbo].[Usp_DelScheduled]
@numScheduleId as numeric(9)
as
delete CustRptSchContacts where numScheduleId = @numScheduleId
delete CustRptScheduler where numScheduleId = @numScheduleId
GO
