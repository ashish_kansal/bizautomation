/****** Object:  StoredProcedure [dbo].[USP_ItemforOptions]    Script Date: 07/26/2008 16:19:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
---created by anoop jayaraj  
--exec USP_ItemforOptions 1  
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_itemforoptions')
DROP PROCEDURE usp_itemforoptions
GO
CREATE PROCEDURE [dbo].[USP_ItemforOptions]   
(      
 @numDomainId numeric(9)=null,  
 @numItemGroupID as numeric(9)=0      
)      
as      
if @numItemGroupID=0  
begin  
 Select vcItemname,numItemcode from item   
 where numDomainID=@numDomainId  
 and charItemType<>'S' and (charItemType='A' or(charItemType='P' and bitSerialized=0))  
 ORDER by vcItemName   
end  
else if @numItemGroupID>0  
begin  
 Select vcItemname,numItemcode,monListPrice,txtItemDesc from item   
 where numDomainID=@numDomainId  
 and charItemType<>'S' and (charItemType='A' or(charItemType='P' and bitSerialized=0))  
 and numItemcode in (select numCusFlDItemID from  ItemGroupsDTL GDTL join ItemOppAccAttrDTL ODTL on GDTL.numOppAccAttrID=ODTL.numOptAccAttrID  
 where GDTL.numItemGroupID=@numItemGroupID and GDTL.tintType=1)  
    ORDER by vcItemName   
end
GO
