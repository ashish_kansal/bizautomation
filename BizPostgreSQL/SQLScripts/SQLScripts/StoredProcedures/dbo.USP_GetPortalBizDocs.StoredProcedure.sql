/****** Object:  StoredProcedure [dbo].[USP_GetPortalBizDocs]    Script Date: 07/26/2008 16:18:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getportalbizdocs')
DROP PROCEDURE usp_getportalbizdocs
GO
CREATE PROCEDURE [dbo].[USP_GetPortalBizDocs]  
@numDomainID as numeric(9) 
as  
select numListItemID,vcData from PortalBizDocs P  
join ListDetails   
on numListItemID=numBizDocID  and P.numDomainID=@numDomainID
GO
