/****** Object:  StoredProcedure [dbo].[USP_REpProductsReceivedOrShipped]    Script Date: 07/26/2008 16:20:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_repproductsreceivedorshipped')
DROP PROCEDURE usp_repproductsreceivedorshipped
GO
CREATE PROCEDURE [dbo].[USP_REpProductsReceivedOrShipped]            
 @numDomainID numeric,                  
        @dtFromDate datetime,                  
        @dtToDate datetime,                  
        @numUserCntID numeric=0,                       
        @tintType numeric,            
   @SortOrder as tinyint            
            
as            
          
declare @strSql as varchar(4000)          
 set @strSql='select div1.numDivisionID,dbo.fn_GetPrimaryContact(Div1.numDivisionID) as ContactID,           
 case when tintOppType=1 then ''Shipped'' when tintOppType=2 then ''Received'' end as Event,            
 vcPOppName,numOppID, '''' as Company            
 from OpportunityMaster Opp              
 join DivisionMaster Div1            
 on Div1.numDivisionID=Opp.numDivisionID                    
  where tintshipped= 1 and (bintAccountClosingDate between '''+convert(varchar(20),@dtFromDate)+''' and '''+convert(varchar(20),@dtToDate)+''') and          
  Opp.numDomainID='+ convert(varchar(15),@numDomainID)+' and Div1.numTerId in(select F.numTerritory from ForReportsByTerritory F                        
  where F.numuserCntid='+convert(varchar(15),@numUserCntID)+' and F.numDomainid='+convert(varchar(15),@numDomainID)+' and F.tintType='+convert(varchar(5),@tintType)+')'                         
          
           
          
 if @SortOrder=1 set @strSql=@strSql+ ' and Opp.tintOppType=1'          
 if @SortOrder=2 set @strSql=@strSql+ ' and Opp.tintOppType=2'          
          
 exec (@strSql)
GO
