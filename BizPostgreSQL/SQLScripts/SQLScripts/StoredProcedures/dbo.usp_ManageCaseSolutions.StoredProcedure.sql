/****** Object:  StoredProcedure [dbo].[usp_ManageCaseSolutions]    Script Date: 07/26/2008 16:19:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_managecasesolutions')
DROP PROCEDURE usp_managecasesolutions
GO
CREATE PROCEDURE [dbo].[usp_ManageCaseSolutions]
	@numCaseID numeric,
	@numSolnID numeric
--
AS
-- Created : 19 April 2004
-- This Procedure will take the Case ID and teh Solution ID and check internally if the record exists. If no, it is inserted.
BEGIN
	DECLARE @bitRetVal bit
	SET @bitRetVal=0

	SELECT @bitRetVal=Count(numCaseID) FROM CaseSolutions WHERE numCaseID=@numCaseID AND numSolnID=@numSolnID

	IF @bitRetVal=1
	BEGIN
		INSERT INTO CaseSolutions values(@numCaseID, @numSolnID)
	END

END
GO
