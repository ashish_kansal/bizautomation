/****** Object:  StoredProcedure [dbo].[USP_GetJournalIdForAuthorizativeAccounting]    Script Date: 07/26/2008 16:17:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created By Siva
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getjournalidforauthorizativeaccounting')
DROP PROCEDURE usp_getjournalidforauthorizativeaccounting
GO
CREATE PROCEDURE [dbo].[USP_GetJournalIdForAuthorizativeAccounting]
(@numOppId as numeric(9)=0,
@numOppBizDocsId as numeric(9)=0)
As
Begin
   Select numJournal_Id  From General_Journal_Header Where numOppId=@numOppId and numOppBizDocsId=@numOppBizDocsId
End
GO
