/****** Object:  StoredProcedure [dbo].[USP_TrailBalanceList]    Script Date: 07/26/2008 16:21:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created By Siva                                                                                                                        
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_trailbalancelist')
DROP PROCEDURE usp_trailbalancelist
GO
CREATE PROCEDURE [dbo].[USP_TrailBalanceList]                   
 @numChartAcntId as numeric(9)=0,                
 @numDomainId as numeric(9)=0,                                                                                                                                  
 @dtFromDate as Datetime,                                          
 @dtToDate as Datetime                                                                                                                         
As                                                                                                                                    
Begin                                                                                                                               
 Select CA.numAccountId as numAccountId,CA.vcAccountName as AcntTypeDescription,CA.numAcntTypeID as numAcntType,
 dbo.fn_GetCurrentOpeningBalanceForTrailBalance(@numChartAcntId,@dtFromDate, @dtToDate,@numDomainId) As Amount                              
 from Chart_Of_Accounts CA  Left outer join ListDetails LD on CA.numAcntTypeID=LD.numListItemID                     
 Where  CA.numAccountId=@numChartAcntId And  CA.numDomainId=@numDomainId                                                                                                
                                                                                                            
End
GO
