/****** Object:  StoredProcedure [dbo].[usp_GetTop5DealSource]    Script Date: 07/26/2008 16:18:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_gettop5dealsource')
DROP PROCEDURE usp_gettop5dealsource
GO
CREATE PROCEDURE [dbo].[usp_GetTop5DealSource]      
 @numDomainID numeric,      
 @dtDateFrom datetime,      
 @dtDateTo datetime,          
 @numUserCntID numeric=0,      
 @tintRights tinyint=3,      
 @intType numeric=0         
--      
AS      
BEGIN      
 IF @tintRights=3 --All Records      
 BEGIN      
   IF @intType = 0      
   BEGIN      
  SELECT TOP 5 LD.vcData, SUM(OM.monPAmount) As Amount      
   FROM OpportunityMaster OM INNER JOIN ListDetails LD ON LD.numListItemID=OM.tintSource
   WHERE       
    OM.tintOppStatus=1  and OM.tintOppType=1    
    AND OM.bintAccountClosingDate BETWEEN @dtDateFrom AND @dtDateTo      
    AND OM.numDomainID=@numDomainID      
   GROUP BY LD.vcData      
   ORDER BY Amount DESC      
   END      
   ELSE      
   BEGIN      
  SELECT TOP 5 LD.vcData, SUM(OM.monPAmount) As Amount      
   FROM AdditionalContactsInformation AI
   INNER JOIN OpportunityMaster OM ON AI.numContactID = OM.numRecOwner
   INNER JOIN ListDetails LD ON LD.numListItemID=OM.tintSource 
   WHERE       
    OM.tintOppStatus=1  and OM.tintOppType=1        
    AND OM.bintAccountClosingDate BETWEEN @dtDateFrom AND @dtDateTo      
    AND OM.numDomainID=@numDomainID           
    AND AI.numTeam is not null         
    AND AI.numTeam in(select F.numTeam from ForReportsByTeam F         
    where F.numuserCntId=@numUserCntID and F.numDomainid=@numDomainId and F.tintType=@intType)        
   GROUP BY LD.vcData      
   ORDER BY Amount DESC      
        
   END      
 END      
       
 IF @tintRights=2 --Territory Records      
 BEGIN      
   IF @intType = 0      
   BEGIN      
  SELECT TOP 5 LD.vcData, SUM(OM.monPAmount) As Amount      
   FROM OpportunityMaster OM 
   INNER JOIN ListDetails LD ON LD.numListItemID=OM.tintSource 
   INNER JOIN DivisionMaster DM ON DM.numDivisionID=OM.numDivisionID
   WHERE       
    OM.tintOppStatus=1  and OM.tintOppType=1        
    AND OM.bintAccountClosingDate BETWEEN @dtDateFrom AND @dtDateTo      
    AND OM.numDomainID=@numDomainID          
    AND DM.numTerID in (SELECT numTerritoryID FROM UserTerritory WHERE numUserCntID=@numUserCntID)      
   GROUP BY LD.vcData      
   ORDER BY Amount DESC      
   END      
   ELSE      
   BEGIN      
  SELECT TOP 5 LD.vcData, SUM(OM.monPAmount) As Amount      
   FROM AdditionalContactsInformation AI
   INNER JOIN OpportunityMaster OM ON AI.numContactID = OM.numRecOwner
   INNER JOIN ListDetails LD ON LD.numListItemID=OM.tintSource
   INNER JOIN DivisionMaster DM ON DM.numDivisionID=OM.numDivisionID
   WHERE       
    OM.tintOppStatus=1 and OM.tintOppType=1         
    AND OM.bintAccountClosingDate BETWEEN @dtDateFrom AND @dtDateTo      
    AND OM.numDomainID=@numDomainID           
    AND DM.numTerID in (SELECT numTerritoryID FROM UserTerritory WHERE numUserCntID=@numUserCntID)        
    AND AI.numTeam is not null         
    AND AI.numTeam in(select F.numTeam from ForReportsByTeam F         
    where F.numuserCntId=@numUserCntID and F.numDomainid=@numDomainId and F.tintType=@intType)        
   GROUP BY LD.vcData      
   ORDER BY Amount DESC      
      
   END      
 END      
       
 IF @tintRights=1 --Owner Records      
 BEGIN      
   IF @intType = 0      
   BEGIN      
  SELECT TOP 5 LD.vcData, SUM(OM.monPAmount) As Amount      
   FROM OpportunityMaster OM
   INNER JOIN ListDetails LD ON LD.numListItemID=OM.tintSource
   INNER JOIN DivisionMaster DM ON DM.numDivisionID=OM.numDivisionID
   WHERE       
    OM.tintOppStatus=1   and OM.tintOppType=1       
    AND OM.bintAccountClosingDate BETWEEN @dtDateFrom AND @dtDateTo      
    AND OM.numDomainID=@numDomainID          
    AND OM.numRecOwner=@numUserCntID      
   GROUP BY LD.vcData      
   ORDER BY Amount DESC      
   END      
   ELSE      
   BEGIN      
  SELECT TOP 5 LD.vcData, SUM(OM.monPAmount) As Amount      
   FROM AdditionalContactsInformation AI      
   INNER JOIN OpportunityMaster OM ON AI.numContactID = OM.numRecOwner
   INNER JOIN ListDetails LD ON LD.numListItemID=OM.tintSource
   INNER JOIN DivisionMaster DM ON DM.numDivisionID=OM.numDivisionID
   WHERE       
    OM.tintOppStatus=1  and OM.tintOppType=1        
    AND OM.bintAccountClosingDate BETWEEN @dtDateFrom AND @dtDateTo      
    AND OM.numDomainID=@numDomainID          
    AND OM.numRecOwner=@numUserCntID          
    AND AI.numTeam is not null         
    AND AI.numTeam in(select F.numTeam from ForReportsByTeam F         
    where F.numuserCntId=@numUserCntID and F.numDomainid=@numDomainId and F.tintType=@intType)        
   GROUP BY LD.vcData      
   ORDER BY Amount DESC      
      
   END      
 END      
       
END
GO
