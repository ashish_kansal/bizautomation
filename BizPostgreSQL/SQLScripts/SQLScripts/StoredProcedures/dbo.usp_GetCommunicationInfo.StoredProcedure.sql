/****** Object:  StoredProcedure [dbo].[usp_GetCommunicationInfo]    Script Date: 07/26/2008 16:16:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- Modified By Anoop Jayaraj                            
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getcommunicationinfo')
DROP PROCEDURE usp_getcommunicationinfo
GO
CREATE PROCEDURE [dbo].[USP_GetCommunicationInfo]                                              
 @numCommid numeric(9)=0,                            
 @ClientTimeZoneOffset Int                        
AS                          
DECLARE @numFollowUpStatus NUMERIC
SELECT @numFollowUpStatus = [numFollowUpStatus] FROM [DivisionMaster] WHERE [numDivisionID] IN (SELECT [numDivisionID] FROM [Communication] WHERE [numCommId]=@numCommid)
SELECT comm.numCommId, comm.bitTask, bitOutlook,                        
comm.numContactId,                        
comm.numDivisionId, ISNULL(comm.textDetails,'') AS [textDetails], comm.intSnoozeMins, comm.intRemainderMins,                        
comm.numStatus, comm.numActivity, comm.numAssign, comm.tintSnoozeStatus,                        
comm.tintRemStatus, comm.numOppId, comm.numCreatedBy,ISNULL(bitTimeAddedToContract,0) AS bitTimeAddedToContract,                 
comm.numModifiedBy, comm.bitClosedFlag, comm.vcCalendarName,                         
DateAdd(minute, -@ClientTimeZoneOffset,dtStartTime ) as dtStartTime,            
DateAdd(minute, -@ClientTimeZoneOffset,dtEndTime ) as dtEndTime,                        
'-' as AssignTo,                            
dbo.fn_SecondsConversion(ISNULL((SELECT TOP 1 ISNULL(CAST(timeLeft AS INT),0)-ISNULL(CAST(timeUsed AS INT),0) FROM Contracts WHERE numDomainId=comm.numDomainID AND numDivisonId=comm.numDivisionId AND intType=1),0)) AS timeLeft,
isnull(dbo.fn_GetContactName(comm.numCreatedBy),'-')+' '+convert(varchar(20),DateAdd(minute, -@ClientTimeZoneOffset,Comm.dtCreatedDate )) as CreatedBy,                            
isnull(dbo.fn_GetContactName(comm.numModifiedBy),'-')+' '+convert(varchar(20),DateAdd(minute, -@ClientTimeZoneOffset,Comm.dtModifiedDate )) as ModifiedBy,                
isnull(dbo.fn_GetContactEmail(1,0,comm.numContactId),'') AS AssignEmail,
isnull(dbo.fn_GetContactName(comm.numContactId),'') AS AssignName,
isnull(dbo.fn_GetContactName(comm.numCreatedBy),'-') as RecOwner,                            
convert(varchar(20),dtModifiedDate) as ModifiedDate,        
isnull(bitSendEmailTemp,0) as bitSendEmailTemp,        
isnull(numEmailTemplate,0) as numEmailTemplate,        
isnull(tintHours,0) as tintHours,        
isnull(bitAlert,0) as bitAlert,      
Comm.caseid,        
(select top 1 vcCaseNumber from cases where cases.numcaseid= Comm.caseid )as vcCaseName,             
isnull(caseTimeid,0)as caseTimeid,        
isnull(caseExpid,0)as caseExpid   ,    
numActivityId,
isnull(C.[numCorrespondenceID],0) numCorrespondenceID,
ISNULL(C.[numOpenRecordID],0) numOpenRecordID,
ISNULL(C.[tintCorrType],0) tintCorrType,
ISNULL(@numFollowUpStatus,0) AS numFollowUpStatus,ISNULL(Comm.bitFollowUpAnyTime,0) bitFollowUpAnyTime,isnull(C.monMRItemAmount,0) as monMRItemAmount,
ISNULL(clo.numDivisionID,0) AS numLinkedOrganization,
ISNULL(clo.numContactID,0) AS numLinkedContact,
(CASE WHEN Comm.numDivisionId > 0 THEN (SELECT TOP 1 vcData from ListDetails where numListItemID =DivisionMaster.numTerId) ELSE '' END) numTerId,
(CASE WHEN Comm.numDivisionId > 0 THEN DivisionMaster.numTerId ELSE 0 END) numOrgTerId
FROM communication Comm LEFT OUTER JOIN [Correspondence] C ON C.[numCommID]=Comm.[numCommId]
LEFT JOIN CommunicationLinkedOrganization clo ON Comm.numCommId = clo.numCommID
LEFT JOIN DivisionMaster ON Comm.numDivisionId = DivisionMaster.numDivisionID
WHERE  comm.numCommid=@numCommid
GO
