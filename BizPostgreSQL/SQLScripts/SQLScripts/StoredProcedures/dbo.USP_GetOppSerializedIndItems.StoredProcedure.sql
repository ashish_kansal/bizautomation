/****** Object:  StoredProcedure [dbo].[USP_GetOppSerializedIndItems]    Script Date: 07/26/2008 16:18:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--exec USP_GetWareHouseItems 6                          
--created by anoop jayaraj                            
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getoppserializedinditems')
DROP PROCEDURE usp_getoppserializedinditems
GO
CREATE PROCEDURE [dbo].[USP_GetOppSerializedIndItems]                            
@numOppItemCode as numeric(9)=0,      
@numOppID as numeric(9)=0,  
@tintOppType  as tinyint                         
as      
-- THIS PROCEDURE IS NOT USED IN BIZ AND ITS CODE IS NOT RELEVANT TO CURRENT IMPLEMENTATION SO DO NOT USE IT
--declare @numWareHouseItemID as numeric(9)      
--declare @numItemID as numeric(9)      
--declare @bitSerialize as bit                    
--declare @bitLotNo as bit                    

--declare @str as varchar(2000)  
--declare @strSQL as varchar(2000)                       
--declare @ColName as varchar(50)                    
--set @str=''        
      
--select @numWareHouseItemID=numWarehouseItmsID,@numItemID=numItemID from   OpportunityItems      
--join WareHouseItems       
--on OpportunityItems.numWarehouseItmsID= WareHouseItems.numWareHouseItemID      
--where  numOppItemTcode=@numOppItemCode         
                      
                      
--declare @numItemGroupID as numeric(9)                      
                      
--select @numItemGroupID=numItemGroup,@bitSerialize=ISNULL(bitSerialized,0),@bitLotNo=ISNULL(bitLotNo,0) from Item where numItemCode=@numItemID                      
--set @ColName='WareHouseItmsDTL.numWareHouseItmsDTLID,1'                  
            
----Create a Temporary table to hold data                                                          
--Create table #tempTable ( ID INT IDENTITY PRIMARY KEY,                                                                    
-- numCusFlDItemID numeric(9)                                                       
-- )                       
                      
--insert into #tempTable                       
--(numCusFlDItemID)                                                          
--select numOppAccAttrID from ItemGroupsDTL where numItemGroupID=@numItemGroupID and tintType=2     
              
-- declare @ID as numeric(9)                      
-- declare @numCusFlDItemID as varchar(20)                      
-- declare @fld_label as varchar(100)                      
-- set @ID=0                      
-- select top 1 @ID=ID,@numCusFlDItemID=numCusFlDItemID,@fld_label=fld_label from #tempTable                       
-- join CFW_Fld_Master on numCusFlDItemID=Fld_ID                                   
-- while @ID>0                      
-- begin                                    
--   set @str=@str+',  dbo.GetCustFldItems('+@numCusFlDItemID+',9,'+@ColName+') as ['+ @fld_label+']'                                      
                        
--   select top 1 @ID=ID,@numCusFlDItemID=numCusFlDItemID,@fld_label=fld_label from #tempTable                       
--   join CFW_Fld_Master on numCusFlDItemID=Fld_ID and ID >@ID                      
--   if @@rowcount=0 set @ID=0                      
                        
-- end        
      
--select numWareHouseItemID,vcWareHouse,
----CAST((dbo.fn_UOMConversion(ISNULL(I.numBaseUnit,0),Opp.numItemCode,I.numDomainId,ISNULL(opp.numUOMId,0)) * Opp.numUnitHour) as numeric(18,0)) as numUnitHour
--numUnitHour,ISNULL(u.vcUnitName,'') vcBaseUOMName
--,@bitSerialize AS bitSerialize,@bitLotNo AS bitLotNo from OpportunityItems Opp     
--join WareHouseItems      
--on Opp.numWarehouseItmsID=WareHouseItems.numWareHouseItemID      
--join Warehouses      
--on Warehouses.numWareHouseID=WareHouseItems.numWareHouseID 
-- join item I on Opp.numItemCode=I.numItemcode  
-- LEFT JOIN  UOM u ON u.numUOMId = I.numBaseUnit       
--where Opp.numOppItemTcode=@numOppItemCode      
      
       
      
           
-- set @strSQL='select WareHouseItmsDTL.numWareHouseItmsDTLID,numWareHouseItemID,vcSerialNo,0 as Op_Flag,
--  isnull(WareHouseItmsDTL.numQty,0) - isnull((select sum(w.numQty) from OppWarehouseSerializedItem w INNER JOIN OpportunityMaster opp ON (w.numOppId=opp.numOppId AND opp.tintOppType=1) where isnull(opp.tintshipped,0)=0 and w.numWarehouseItmsDTLID=WareHouseItmsDTL.numWarehouseItmsDTLID),0) + isnull(OppWarehouseSerializedItem.numQty,0) as TotalQty,isnull(OppWarehouseSerializedItem.numQty,0) as UsedQty
--  '+  @str  +',1 as bitAdded
-- from   OppWarehouseSerializedItem      
-- join WareHouseItmsDTL      
-- on WareHouseItmsDTL.numWareHouseItmsDTLID= OppWarehouseSerializedItem.numWarehouseItmsDTLID                           
-- where numOppID='+ convert(varchar(15),@numOppID)+' and  numWareHouseItemID='+ convert(varchar(15),@numWareHouseItemID)                        
-- print @strSQL                     
-- exec (@strSQL)     
  
  
--select Fld_label,fld_id,fld_type,numlistid,vcURL from #tempTable                       
-- join CFW_Fld_Master on numCusFlDItemID=Fld_ID                    
--drop table #tempTable   
  
--if @tintOppType=1  
--begin   
-- set @strSQL='select numWareHouseItmsDTLID,numWareHouseItemID,vcSerialNo,0 as Op_Flag,
--  isnull(numQty,0) - isnull((select sum(w.numQty) from OppWarehouseSerializedItem w INNER JOIN OpportunityMaster opp ON (w.numOppId=opp.numOppId AND opp.tintOppType=1) where isnull(opp.tintshipped,0)=0 and w.numWarehouseItmsDTLID=WareHouseItmsDTL.numWarehouseItmsDTLID),0) as TotalQty,
-- cast(0 as numeric(9,0)) as UsedQty '+  @str  +',0 as bitAdded
-- from   WareHouseItmsDTL   
--    where (tintStatus is null or tintStatus=0) and  numWareHouseItemID='+ convert(varchar(15),@numWareHouseItemID) +'  
--    and numWareHouseItmsDTLID not in(select numWarehouseItmsDTLID from OppWarehouseSerializedItem where numOppID='+ convert(varchar(15),@numOppID)+' and  numWarehouseItmsID='+ convert(varchar(15),@numWareHouseItemID) +')'                    
-- print @strSQL                     
-- exec (@strSQL)   
--end
GO
