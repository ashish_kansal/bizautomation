/****** Object:  StoredProcedure [dbo].[USP_GetContactsEmail]    Script Date: 07/26/2008 16:16:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getcontactsemail')
DROP PROCEDURE usp_getcontactsemail
GO
CREATE PROCEDURE [dbo].[USP_GetContactsEmail]  
@numContactId as numeric(9),
@tintMode AS TINYINT=0
as  

IF @tintMode =0
BEGIN
	select vcEmail from AdditionalContactsInformation where numContactId=@numContactId
END
ELSE IF @tintMode =1 --select primary contact's email id from secondary contact id 
BEGIN
	SELECT ISNULL([vcEmail],'') vcEmail FROM [AdditionalContactsInformation] WHERE ISNULL(bitPrimaryContact,0)=1 AND [numDivisionId] IN (select [numDivisionId] from AdditionalContactsInformation where numContactId=@numContactId)
END
	

GO
