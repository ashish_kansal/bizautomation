/****** Object:  StoredProcedure [dbo].[Usp_GetSalutationdet]    Script Date: 07/26/2008 16:18:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getsalutationdet')
DROP PROCEDURE usp_getsalutationdet
GO
CREATE PROCEDURE [dbo].[Usp_GetSalutationdet]
@intType int,
@numID numeric
--
as
select cfw_fld_values_cont.fld_value, cfw_fld_master.fld_label , recid from cfw_fld_master INNER JOIN cfw_fld_values_cont ON cfw_fld_master.fld_id = cfw_fld_values_cont.fld_id where fld_label = 'Salutation' and recid = @numID
GO
