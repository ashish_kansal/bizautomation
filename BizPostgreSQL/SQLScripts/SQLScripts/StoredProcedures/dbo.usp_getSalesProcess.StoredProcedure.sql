/****** Object:  StoredProcedure [dbo].[usp_getSalesProcess]    Script Date: 07/26/2008 16:18:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getsalesprocess')
DROP PROCEDURE usp_getsalesprocess
GO
CREATE PROCEDURE [dbo].[usp_getSalesProcess]
--
as
Select slp_id, slp_name from Sales_process_List_Master order by slp_name
GO
