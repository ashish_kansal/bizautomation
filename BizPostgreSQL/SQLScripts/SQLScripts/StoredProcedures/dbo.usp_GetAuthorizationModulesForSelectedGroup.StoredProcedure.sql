/****** Object:  StoredProcedure [dbo].[usp_GetAuthorizationModulesForSelectedGroup]    Script Date: 07/26/2008 16:16:17 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getauthorizationmodulesforselectedgroup')
DROP PROCEDURE usp_getauthorizationmodulesforselectedgroup
GO
CREATE PROCEDURE [dbo].[usp_GetAuthorizationModulesForSelectedGroup]
	@numGroupID NUMERIC(9)   
--
AS
		SELECT DISTINCT numModuleID FROM GroupAuthorization
		WHERE numGroupID=@numGroupID
GO
