/****** Object:  StoredProcedure [dbo].[Usp_getCustReportList]    Script Date: 07/26/2008 16:17:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getcustreportlist')
DROP PROCEDURE usp_getcustreportlist
GO
CREATE PROCEDURE [dbo].[Usp_getCustReportList]      
@numDomainId as numeric(9),      
@numUserCntId as numeric(9),    
@CurrentPage int,                                                        
@PageSize int,                                                        
@TotRecs int output,     
@SortChar char(1)='0' ,                                                       
@columnName as Varchar(50),                                                        
@columnSortOrder as Varchar(50)  ,
@numModuleId as numeric(9),   
@SearchStr  as Varchar(50)   
    
as       
    
Create table #tempTable ( ID INT IDENTITY PRIMARY KEY,                                                         
      ReportId varchar(15)                                                        
 )     
declare @strSql as varchar(8000)                                                  
    
set  @strSql='Select     
numCustomReportId as ReportId     
from customreport where isnull(bitDefault,0)=0 and isnull(bitReportDisable,0)=0 and numdomainid='+ convert(varchar(15),@numDomainID) 
    
if @SortChar<>'0' set @strSql=@strSql + ' And customreport.vcReportName like '''+@SortChar+'%'''     
if @numModuleId <> 0 set @strSql=@strSql + ' And customreport.numModuleId = '+ convert(varchar(15),@numModuleId) +''   
if @SearchStr<>'' set @strSql=@strSql + ' And (customreport.vcReportName like ''%'+@SearchStr+'%'' or 
customreport.vcReportDescription like ''%'+@SearchStr+'%'') '     
set  @strSql=@strSql +'ORDER BY ' + @columnName +' '+ @columnSortOrder    
    
Print @strSql            
insert into #tempTable(ReportId)                                                        
exec(@strSql)    
    
 declare @firstRec as integer                                                        
  declare @lastRec as integer                                                        
 set @firstRec= (@CurrentPage-1) * @PageSize                                                        
 set @lastRec= (@CurrentPage*@PageSize+1)                                                         
set @TotRecs=(select count(*) from #tempTable)       
    
    
Select     
numCustomReportId as ReportId,vcReportName,vcReportDescription     
from customreport    
    
 join #tempTable T on T.ReportId=customreport.numCustomReportId                                                   
   WHERE ID > @firstRec and ID < @lastRec order by ID
GO
