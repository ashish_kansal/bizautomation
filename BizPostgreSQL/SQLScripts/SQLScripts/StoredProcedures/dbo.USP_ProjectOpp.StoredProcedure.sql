GO

set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go
-- exec USP_ProjectOpp @byteMode=1,@domainId=72,@numDivisionID=15914,@numProjectId=197,@numCaseId=0
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_projectopp')
DROP PROCEDURE usp_projectopp
GO
CREATE PROCEDURE [dbo].[USP_ProjectOpp]        
	@byteMode as tinyint=0,      
	@domainId as numeric(9)=0,    
	@numDivisionID as numeric(9)=0 ,       
	@numProjectId  as numeric(9)=0  ,
	@numCaseId  as numeric(9)=0 ,
	@numStageID	AS NUMERIC(9)=0,
	@tintOppType as tinyint=0
as        
BEGIN        
	IF @byteMode=0       
	BEGIN      
		SELECT 
			numOppId
			,vcPOppName         
		FROM 
			OpportunityMaster 
		WHERE 
			tintActive=1 
			AND tintShipped=0 
			AND numDivisionID=@numDivisionID 
			AND numDomainId=@domainId 
		ORDER BY 
			numOppId DESC        
	END           
      
	IF @byteMode=1      
	BEGIN 
		If (@numProjectId = 0 and @numCaseId<>0)
		BEGIN
			SELECT 
				numOppId
				,vcPOppName      
			FROM 
				OpportunityMaster     
			WHERE 
				numDomainId=@domainId 
				AND numDivisionID=@numDivisionID  
				AND ((tintActive=1 AND tintShipped=0) OR numOppId IN (SELECT numOppId from CaseOpportunities where numCaseId=@numCaseId and numDomainId=@domainId))  
			ORDER BY 
				numOppId DESC
		END
		ELSE
		BEGIN
			SELECT 
				OM.numOppId
				,0 as numBillID
				,0 as numProjOppID
				,OM.vcPOppName
				,I.[vcItemName]
				,'Purchase' AS [Type]
				,(OPP.[monPrice] * OPP.[numUnitHour]) AS ExpenseAmount
				,0 as bitIntegratedToAcnt
				,0 as numBizDocsPaymentDetailsId
				,COA.vcAccountName as ExpenseAccount
			FROM 
				[OpportunityMaster] OM 
			INNER JOIN [OpportunityItems] OPP ON OM.[numOppId] = OPP.[numOppId]
            LEFT OUTER JOIN [Item] I ON OPP.[numItemCode] = I.[numItemCode]
			LEFT OUTER JOIN [Chart_Of_Accounts] COA ON I.[numCOGsChartAcntId] = COA.[numAccountId]
			WHERE 
				OM.numDomainId=@domainId  
					AND OPP.numProjectID = @numProjectId
					AND (OPP.numProjectStageID = @numStageID OR @numStageID =0)
					AND OM.[tintOppType] = 2
			UNION ALL
			SELECT 
			 0 as numOppId,PO.numBillID,PO.numProjOppID,'','','Bill' as [Type],
			 BH.monAmountDue AS ExpenseAmount,
			ISNULL((SELECT bitIntegrated FROM dbo.OpportunityBizDocsPaymentDetails WHERE numBizDocsPaymentDetId = PO.numBillId),0) AS bitIntegratedToAcnt,
			(SELECT numBizDocsPaymentDetailsId FROM dbo.OpportunityBizDocsPaymentDetails WHERE numBizDocsPaymentDetId = PO.numBillId) AS numBizDocsPaymentDetailsId,
			'' as ExpenseAccount
			 from  ProjectsOpportunities PO 
				   JOIN BillHeader BH ON BH.numBillID = PO.numBillID
			 where  PO.numDomainId=@domainId  
					AND PO.numProID = @numProjectId
					AND (PO.numStageID = @numStageID OR @numStageID =0)
					and isnull(PO.numBillId,0)>0
			 order by numOppId desc
		END     
	END

	IF @byteMode= 2
	BEGIN
	 select  OB.[vcBizDocID],OB.[numOppBizDocsId],CASE WHEN OB.vcBizDocName IS NULL THEN OB.vcBizDocID WHEN LEN(OB.vcBizDocID)>0 THEN OB.vcBizDocID ELSE OB.vcBizDocName END AS vcBizDocName
	 from OpportunityMaster OM INNER JOIN [OpportunityBizDocs] OB ON OM.[numOppId] = OB.[numOppId] where 
	 OM.tintOppType =1 AND 
	 OB.[bitAuthoritativeBizDocs]=1 AND tintActive=1 and  tintShipped=0 and numDivisionID=@numDivisionID and numDomainId=@domainId order by OB.numOppId desc

	END 

	IF @byteMode= 3 --Select only sales/purchase opportunity 
	BEGIN
	   select   numOppId,
				vcPOppName
	   from     OpportunityMaster
	   where    tintActive = 1
				and tintShipped = 0
				and numDomainId = @domainId
				AND numDivisionID=@numDivisionID 
				AND [tintOppType] = @tintOppType
	   order by numOppId desc        

	END 

	IF @byteMode= 4 --Select only closed orders which are not returned yet.
	BEGIN
	   select   numOppId,
				vcPOppName
	   from     OpportunityMaster
	   where    tintActive = 1
				and tintShipped = 1
				and numDomainId = @domainId
				AND numDivisionID=@numDivisionID 
				AND [tintOppType] = @tintOppType
				--AND numOppId NOT IN (SELECT numOppId FROM dbo.ReturnHeader WHERE numDomainId=@domainId)
	   order by numOppId desc        
	END 

	IF @byteMode= 5 --Select Fulfillment BizDocs FROM closed orders which are not returned yet.
	BEGIN
		IF @tintOppType = 2
		BEGIN
			SELECT
				CONCAT(OpportunityMaster.numOppID,'-0') numOppBizDocsId
				,vcPOppName
			FROM
				OpportunityMaster
			WHERE
				tintActive = 1
				and tintShipped = 1
				and numDomainId = @domainId
				AND numDivisionID=@numDivisionID 
				AND [tintOppType] = @tintOppType
			ORDER BY 
				OpportunityMaster.numOppId desc
		END
		ELSE
		BEGIN
			SELECT
				CONCAT(OpportunityMaster.numOppID,'-',numOppBizDocsId) numOppBizDocsId
				,CONCAT(vcBizDocID,' (',vcPOppName,')') vcPOppName
			FROM
				OpportunityMaster
			INNER JOIN
				OpportunityBizDocs
			ON
				OpportunityMaster.numOppId = OpportunityBizDocs.numOppId
			WHERE
				tintActive = 1
				and tintShipped = 1
				and numDomainId = @domainId
				AND numDivisionID=@numDivisionID 
				AND [tintOppType] = @tintOppType
				AND numBizDocId = 296
		   ORDER BY 
				OpportunityMaster.numOppId desc
		END
	END 
END
GO