GO
/****** Object:  StoredProcedure [dbo].[USP_HiereachyChartOfAct]    Script Date: 09/01/2009 19:15:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created By Siva         
-- EXEC [dbo].[USP_HiereachyChartOfAct] 1
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_hiereachychartofact')
DROP PROCEDURE usp_hiereachychartofact
GO
CREATE PROCEDURE [dbo].[USP_HiereachyChartOfAct]               
--@numParentAcntID as numeric(9)=0,        
    @numDomainID AS NUMERIC(9) = 0
AS 
    BEGIN    
--Declare @strSQL as varchar(8000)    
--SELECT * FROM [Chart_Of_Accounts]
        WITH    RecursionCTE ( RecordID, ParentRecordID, vcAccountName, TOC, T, numChartOfAcntID, numAcntTypeId )
                  AS ( SELECT   numAccountId,
                                numParntAcntTypeId,
                                vcAccountName,
                                CONVERT(VARCHAR(1000), '') TOC,
                                CONVERT(VARCHAR(1000), '') T,
                                CONVERT(VARCHAR(10), numAccountId) + '~'
                                + CONVERT(VARCHAR(10), numAcntTypeId) AS numChartOfAcntID,
                                numAcntTypeId
                       FROM     Chart_Of_Accounts
                       WHERE    numParntAcntTypeId IS NULL
                                AND numDomainID = @numDomainID
                       UNION ALL
                       SELECT   R1.numAccountId,
                                R1.numParntAcntTypeId,
                                R1.vcAccountName,
                                CASE WHEN DATALENGTH(R2.TOC) > 0
                                     THEN CONVERT(VARCHAR(1000), CASE WHEN CHARINDEX('.', R2.TOC) > 0 THEN R2.TOC
                                                                      ELSE R2.TOC + '.'
                                                                 END
                                          + CAST(R1.numAccountId AS VARCHAR(10)))
                                     ELSE CONVERT(VARCHAR(1000), CAST(R1.numAccountId AS VARCHAR(10)))
                                END AS TOC,
                                CASE WHEN DATALENGTH(R2.TOC) > 0
                                     THEN CONVERT(VARCHAR(1000), '..' + R2.T)
                                     ELSE CONVERT(VARCHAR(1000), '')
                                END AS T,
                                CONVERT(VARCHAR(10), R1.numAccountId) + '~'
                                + CONVERT(VARCHAR(10), R1.numAcntTypeId) AS numChartOfAcntID,
                                R1.numAcntTypeId
                       FROM     Chart_Of_Accounts AS R1
                                JOIN RecursionCTE AS R2 ON R1.numParntAcntTypeId = R2.RecordID
                                                           AND R1.numDomainID = @numDomainID
                     )
            SELECT  RecordID,
                    T + vcAccountName AS vcCategoryName,
                    TOC,
                    numChartOfAcntID,
                    numAcntTypeId,
                    ISNULL(vcData, '') vcAcntType
            FROM    RecursionCTE
                    LEFT OUTER JOIN ListDetails ON numAcntTypeId = numLIstItemId
            WHERE   ParentRecordID IS NOT NULL
            ORDER BY numAcntTypeId,
                    TOC ASC

--Set @strSQL='  with RecursionCTE (RecordID,ParentRecordID,vcAccountName,TOC,T,numChartOfAcntID,numAcntTypeId)                  
-- as                  
-- (                  
-- select numAccountId,numParntAcntTypeId,vcAccountName,convert(varchar(1000),'''') TOC,convert(varchar(1000),'''') T,
--Convert(varchar(10),numAccountId)+''~''+Convert(varchar(10),numAcntTypeId) as numChartOfAcntID,numAcntTypeId                  
--    from Chart_Of_Accounts                  
--    where numParntAcntTypeId is null and numDomainID='+Convert(varchar(10),@numDomainId)               
--    +' union all                  
--   select R1.numAccountId,                  
--          R1.numParntAcntTypeId,                  
--    R1.vcAccountName,                  
--          case when DataLength(R2.TOC) > 0                  
--                    then convert(varchar(1000),case when CHARINDEX(''.'',R2.TOC)>0 then R2.TOC else R2.TOC +''.'' end                  
--                                 + cast(R1.numAccountId as varchar(10)))                   
--       else convert(varchar(1000),cast(R1.numAccountId as varchar(10)))                   
--                    end as TOC,case when DataLength(R2.TOC) > 0                  
--                    then  convert(varchar(1000),''..''+ R2.T)                  
--else convert(varchar(1000),'''')                  
--                    end as T,Convert(varchar(10),R1.numAccountId)+''~''+Convert(varchar(10),R1.numAcntTypeId) as numChartOfAcntID,R1.numAcntTypeId
--      from Chart_Of_Accounts as R1                        
--      join RecursionCTE as R2 on R1.numParntAcntTypeId = R2.RecordID and R1.numDomainID='+Convert(varchar(10),@numDomainId) +'                   
--  )                   
--select RecordID ,T+vcAccountName as vcCategoryName,TOC,numChartOfAcntID,numAcntTypeId,isnull(vcData,'''') vcAcntType from RecursionCTE left outer join ListDetails on numAcntTypeId=numLIstItemId Where ParentRecordID is not null order by numAcntTypeId,TOC asc'
--  PRINT @strSQL
-- Exec (@strSQL) 
    END
