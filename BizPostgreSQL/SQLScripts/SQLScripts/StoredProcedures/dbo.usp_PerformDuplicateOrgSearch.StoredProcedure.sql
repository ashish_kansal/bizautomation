/****** Object:  StoredProcedure [dbo].[usp_PerformDuplicateOrgSearch]    Script Date: 07/26/2008 16:20:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created By: Debasish Nag                                              
--Created On: 21st Dec 2005                                              
--Purpose: Query for Duplicate Organizations and Divisions                                              
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_performduplicateorgsearch')
DROP PROCEDURE usp_performduplicateorgsearch
GO
CREATE PROCEDURE [dbo].[usp_PerformDuplicateOrgSearch]                          
@vcCRMTypes NVarChar(5),                                              
@numCompanyType Numeric,                                              
@vcOrgCreationStartDateRange NVarchar(10),                                              
@vcOrgCreationEndDateRange NVarchar(10),                                              
@boolAssociation Bit,                                              
@boolSameDivisionName Bit,                                              
@boolSameWebsite Bit,                                              
@vcMatchingField  NVarChar(50),                                            
@vcSortColumn NVarchar(50),                                         
@vcSortOrder NVarchar(50),                                    
@numNosRowsReturned Int,                                       
@numCurrentPage Int,                                              
@numDomainId Numeric                                              
AS                                              
BEGIN                                                      
   --SET THE RANGE OF RECORDS                      
   DECLARE @intTopOfResultSet Int                                    
   SET @intTopOfResultSet = (@numNosRowsReturned * @numCurrentPage)                    
   DECLARE @intStartOfResultSet Int                                    
   SET @intStartOfResultSet = @numNosRowsReturned * (@numCurrentPage - 1)                       
   SET @vcSortColumn = Replace(@vcSortColumn,'[Date/Time  Created Last Modified & By Whom]', '3')                      
   SET @vcSortColumn = Replace(@vcSortColumn,'[Organization Name/ Division]', '4')                      
   SET @vcSortColumn = Replace(@vcSortColumn,'[Relationship]', '5')                      
   SET @vcSortColumn = Replace(@vcSortColumn,'[Zip]', '6')                     
   SET @vcSortColumn = Replace(@vcSortColumn,'[Nos. Contacts]', '7')                      
   SET @vcSortColumn = Replace(@vcSortColumn,'[Open Opportunities?]', '8')                      
   SET @vcSortColumn = Replace(@vcSortColumn,'[Closed Deals?]', '9')                      
   SET @vcSortColumn = Replace(@vcSortColumn,'[Status]', '10')                      
   SET @vcSortColumn = Replace(@vcSortColumn,'[Date/Time last Contact was Created/Modified]', '11')                      
                                
   --FIND THE CUSTOM FIELDS FOR THE CONTACT                        
   DECLARE @OrgTempCustomTableColumns NVarchar(4000)                                              
   SELECT @OrgTempCustomTableColumns = dbo.fn_OrgAndContCustomFieldsColumns(1,0)                        
   --PRINT '@ContTempCustomTableColumns: ' +  @ContTempCustomTableColumns                        
   --CREATE A STRING OF FIELDS WHICH ARE TO BE COMPARED                       
   DECLARE @vcOrgDivisionName NVarchar(20)                        
   DECLARE @vcOrgWebsite NVarchar(20)                      
   DECLARE @vcOrgCompanyName NVarchar(200)                      
   DECLARE @vcOrgMatchingFieldName NVarchar(25)                      
                                                  
   IF @boolSameDivisionName = 1  --Same Division name                        
    SET @vcOrgDivisionName = 'vcOrgDivisionName,'                        
   ELSE                        
    SET @vcOrgDivisionName = ''                        
                        
   IF @boolSameWebsite = 1  --Same Website                       
    SET @vcOrgWebsite = 'vcOrgWebSite,'                        
   ELSE                        
    SET @vcOrgWebsite = ''                        
                                     
   SET @vcOrgCompanyName = 'numDivisionId,vcOrgCompanyName,vcContCreatedDate,vcContCreatedBy,numContModifiedDate,numContModifiedBy,'                       
                        
   IF @vcMatchingField <> '' --Other Matching Fields                        
    SET @vcOrgMatchingFieldName = @vcMatchingField + ','                        
   ELSE                        
    SET @vcOrgMatchingFieldName = ''                        
                       
   DECLARE @vcComparisionFieldsNames NVarchar(200)                        
   SET @vcComparisionFieldsNames = @vcOrgCompanyName                      
   IF @boolSameDivisionName = 1                        
     SET @vcComparisionFieldsNames = @vcComparisionFieldsNames + @vcOrgDivisionName                        
   IF @boolSameWebsite = 1                         
     SET @vcComparisionFieldsNames = @vcComparisionFieldsNames + @vcOrgWebsite                        
                    
   IF @vcMatchingField <> '' AND (CHARINDEX(@vcMatchingField,@vcComparisionFieldsNames) <= 0 AND CHARINDEX(@vcMatchingField,IsNull(dbo.fn_OrgAndContCustomFieldsColumns(1,1),'')) <= 0)                   
     SET @vcComparisionFieldsNames = @vcComparisionFieldsNames + @vcOrgMatchingFieldName                     
                                                              
   SET @vcComparisionFieldsNames = SUBSTRING(@vcComparisionFieldsNames, 1, LEN(@vcComparisionFieldsNames)-1)                        
   --PRINT '@vcComparisionFieldsNames: ' + @vcComparisionFieldsNames                      
                        
                        
   DECLARE @OrgTempCustomTableDataEntryQuery NVarchar(4000)                         
                        
   SET @OrgTempCustomTableDataEntryQuery = ' SELECT DISTINCT TOP 100 PERCENT numCompanyId' + ',' + @vcComparisionFieldsNames                        
                                      
    IF (@OrgTempCustomTableColumns Is Not NULL)                                 
     SET @OrgTempCustomTableDataEntryQuery = @OrgTempCustomTableDataEntryQuery + ', ' + dbo.fn_OrgAndContCustomFieldsColumns(1,2)                                      
                        
    SET @OrgTempCustomTableDataEntryQuery = @OrgTempCustomTableDataEntryQuery + ' FROM vw_Records4Duplicates ORDER BY numCompanyId'                              
                                
    SET @OrgTempCustomTableDataEntryQuery = 'Select TOP 100 PERCENT * INTO #tmpTableCstFld FROM (' + @OrgTempCustomTableDataEntryQuery + ') AS tmpTable1'                        
    --PRINT @OrgTempCustomTableDataEntryQuery                      
    --EXECUTE (@OrgTempCustomTableDataEntryQuery)                       
    DECLARE @OrgTempCustomTableDeleteQuery As NVarchar(100)                      
    SET @OrgTempCustomTableDeleteQuery = 'DROP TABLE #tmpTableCstFld' + Char(10) + 'DROP TABLE #tmpTableDuplicateOrg'                     
                                           
DECLARE @DuplicateOrgFixedQuery varchar(8000)                                              
SET @DuplicateOrgFixedQuery = 'SELECT DISTINCT TOP 100 PERCENT Rec1.numCompanyId, Rec1.numDivisionId,                                              
 dbo.fn_GetLatestEntityEditedDateAndUser(Rec1.numDivisionId, 0, ''Org'') AS [Date/Time  Created Last Modified & By Whom],                                            
 LTrim(Rec1.vcOrgCompanyName) + ''/ '' + Rec1.vcOrgDivisionName As [Organization Name/ Division],                                              
  dbo.fn_AdvSearchColumnName(IsNull(Rec1.vcOrgCompanyType,0),''L'') AS [Relationship],                                              
  Rec1.vcOrgBillPostCode as [Zip],                                               
  Count(DISTINCT(numContactId)) AS [Nos. Contacts],                                             
  IsNull(recOpp.boolOpportunityOpen,''No'') AS [Open Opportunities?],                                              
  IsNull(recOpp.boolClosedDeal,''No'') AS [Closed Deals?],                                              
  dbo.fn_AdvSearchColumnName(IsNull(numStatusID,0),''L'') AS [Status],                                            
  CASE WHEN Max(Rec2.vcContCreatedDate) > Max(IsNull(Rec2.numContModifiedDate,0)) THEN                      
          Cast(Max(Rec2.vcContCreatedDate) As NVarchar)                      
  ELSE                      
Cast(Max(Rec2.numContModifiedDate) As NVarchar)                      
  END                      
  AS [Date/Time last Contact was Created/Modified]                      
  --dbo.fn_GetLatestEntityEditedDateAndUser(Rec1.numDivisionId, 0, ''Cnt'') AS [Date/Time last Contact was Created/Modified & By Whom]                                            
 FROM    vw_Records4Duplicates Rec1  INNER JOIN #tmpTableCstFld recCustom ON recCustom.numDivisionId = Rec1.numDivisionId                               
  LEFT OUTER JOIN                                               
  (SELECT numDivisionId, Case WHEN SUM(tintOppStatus) = 0 THEN ''No'' ELSE ''Yes'' END AS boolOpportunityOpen,                                               
   Case WHEN SUM(tintShipped) = 0 THEN ''No'' ELSE ''Yes'' END AS boolClosedDeal                                               
   FROM OpportunityMaster GROUP BY numDivisionId                                            
  ) recOpp                                              
    ON Rec1.numDivisionId = recOpp.numDivisionId INNER JOIN                                                
    #tmpTableCstFld Rec2                                            
   ON (LTRIM(Rec1.vcOrgCompanyName) = LTRIM(Rec2.vcOrgCompanyName)                             
   AND  Rec1.numCompanyId <> Rec2.numCompanyId AND Rec1.numDomainId = ' + Cast(@numDomainId as NVarchar) + ') '    
--PRINT @DuplicateOrgFixedQuery                                              
                                              
DECLARE @DuplicateOrgCRMTypesQuery NVarchar(60)                                 
SET @DuplicateOrgCRMTypesQuery = ' WHERE Convert(NVarchar(1), Rec1.vcOrgCRMType) IN (' + @vcCRMTypes + ') '                                              
--PRINT @DuplicateOrgCRMTypesQuery                                              
                                              
DECLARE @DuplicateCompanyTypeQuery NVarchar(50)                                        
SET @DuplicateCompanyTypeQuery = ''                      
IF @numCompanyType <> 0                                              
SET @DuplicateCompanyTypeQuery = ' AND Rec1.vcOrgCompanyType = ' + Convert(NVarchar, @numCompanyType)                                              
--PRINT @DuplicateCompanyTypeQuery                                              
                                              
DECLARE @DuplicateDateRangeQuery NVarchar(100)                                              
SET @DuplicateDateRangeQuery = ''                      
IF LTRIM(@vcOrgCreationStartDateRange) <> '' AND LTrim(@vcOrgCreationEndDateRange) <> ''                                              
  SET @DuplicateDateRangeQuery = ' AND (Rec1.vcOrgCreatedDate >= ''' + @vcOrgCreationStartDateRange + '''' +                                     
    ' AND Rec1.vcOrgCreatedDate <= ''' + @vcOrgCreationEndDateRange + ''') '                                    
ELSE IF LTRIM(@vcOrgCreationStartDateRange) <> '' AND LTrim(@vcOrgCreationEndDateRange) = ''                                              
  SET @DuplicateDateRangeQuery = ' AND (Rec1.vcOrgCreatedDate >= ''' + @vcOrgCreationStartDateRange + ''') '                                              
ELSE IF LTRIM(@vcOrgCreationStartDateRange) = '' AND LTrim(@vcOrgCreationEndDateRange) <> ''                                              
  SET @DuplicateDateRangeQuery = ' AND Rec1.vcOrgCreatedDate <= ''' + @vcOrgCreationEndDateRange + ''') '                                              
--PRINT @DuplicateDateRangeQuery                                              
                                              
                                              
DECLARE @DuplicateAssociationQuery NVarchar(30)                                             
SET @DuplicateAssociationQuery = ''           
IF @boolAssociation = 1                         
SET @DuplicateAssociationQuery = ' AND vcAssociation IS NULL'                                              
--PRINT @DuplicateAssociationQuery                                              
                                              
         
DECLARE @DuplicateDivisionNameQuery NVarchar(200)                                              
SET @DuplicateDivisionNameQuery = ''                                            
IF @boolSameDivisionName = 1                                            
SET @DuplicateDivisionNameQuery = ' AND Rec1.vcOrgDivisionName = Rec2.vcOrgDivisionName                                            
 AND Rec1.numDivisionId <> Rec2.numDivisionId '                                            
--PRINT @DuplicateDivisionNameQuery                                              
                                            
DECLARE @DuplicateWebsiteQuery NVarchar(150)                                        
SET @DuplicateWebsiteQuery = ''                                            
IF @boolSameWebsite = 1                                            
SET @DuplicateWebsiteQuery = ' AND Rec1.vcOrgWebSite = Rec2.vcOrgWebSite                                             
 AND Rec1.numCompanyId <> Rec2.numCompanyId '                                              
---PRINT @DuplicateWebsiteQuery                                              
                                    
                                            
DECLARE @DuplicateMatchingFieldQuery NVarchar(150)                                              
SET @DuplicateMatchingFieldQuery = ''                                            
IF @vcMatchingField <> ''                                            
SET @DuplicateMatchingFieldQuery = ' AND recCustom.' + @vcMatchingField + ' = Rec2.' + @vcMatchingField + ' '                                             
--PRINT @DuplicateMatchingFieldQuery                                              
                                              
                                            
DECLARE @GroupOrderColumnFieldQuery NVarchar(1000)                        
SET @GroupOrderColumnFieldQuery = ' GROUP BY Rec1.numCompanyId, Rec1.numDivisionId, Rec1.vcOrgCompanyName, Rec1.vcOrgDivisionName, Rec1.vcOrgCompanyType, Rec1.vcOrgBillPostCode, recOpp.boolOpportunityOpen, recOpp.boolClosedDeal, Rec1.numStatusID ORDER BY
  
     
      
' + @vcSortColumn + ' ' + @vcSortOrder                                      
--PRINT @GroupOrderColumnFieldQuery                                              
                       
DECLARE @vcDuplicateCompleteQuery NVarchar(4000)                    
SET @vcDuplicateCompleteQuery = @DuplicateOrgFixedQuery + @DuplicateOrgCRMTypesQuery + @DuplicateCompanyTypeQuery + @DuplicateDateRangeQuery + @DuplicateAssociationQuery + @DuplicateDivisionNameQuery                    
 + @DuplicateWebsiteQuery +  @DuplicateMatchingFieldQuery + @GroupOrderColumnFieldQuery  SET @vcDuplicateCompleteQuery = Char(10) + 'Select IDENTITY(int) AS numRow, * INTO #tmpTableDuplicateOrg FROM (' + @vcDuplicateCompleteQuery + ') AS tmpTableAllFields
  
    
      
'                      
                    
DECLARE @vcRowBoundOrgDuplicateQuery NVarchar(4000)                    
DECLARE @vcDuplicateCountQuery  NVarchar(4000)                    
SET @vcDuplicateCountQuery = Char(10) + 'SELECT IsNull(Max(numRow),0) AS DupCount FROM #tmpTableDuplicateOrg'                    
SET @vcRowBoundOrgDuplicateQuery = 'SELECT * FROM #tmpTableDuplicateOrg WHERE numRow > ' + CAST(@intStartOfResultSet AS NVarchar(9)) +  ' AND numRow <= ' + CAST(@intTopOfResultSet AS NVarchar(9)) + '            
 ORDER BY ' + Cast(CONVERT(Int, @vcSortColumn) + 1 As NVarchar) + ' ' + @vcSortOrder + @vcDuplicateCountQuery                    
                    
SET @OrgTempCustomTableDataEntryQuery = @OrgTempCustomTableDataEntryQuery + Char(10)        
SET @vcRowBoundOrgDuplicateQuery = @vcRowBoundOrgDuplicateQuery + Char(10)        
        
DECLARE @vcCompleteOrgDuplicateSearchQuery Varchar(8000)                      
SET @vcCompleteOrgDuplicateSearchQuery = @vcDuplicateCompleteQuery + Char(10)                      
PRINT (@OrgTempCustomTableDataEntryQuery + @vcCompleteOrgDuplicateSearchQuery + @vcRowBoundOrgDuplicateQuery + @OrgTempCustomTableDeleteQuery)        
EXECUTE(@OrgTempCustomTableDataEntryQuery + @vcCompleteOrgDuplicateSearchQuery + @vcRowBoundOrgDuplicateQuery + @OrgTempCustomTableDeleteQuery)                    
                      
END
GO
