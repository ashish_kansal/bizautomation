/****** Object:  StoredProcedure [dbo].[USP_GetAuthorizativeBizDocsId]    Script Date: 07/26/2008 16:16:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created By Siva  
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getauthorizativebizdocsid')
DROP PROCEDURE usp_getauthorizativebizdocsid
GO
CREATE PROCEDURE [dbo].[USP_GetAuthorizativeBizDocsId]                        
(                        
  @numOppBizDocsId as numeric(9)=0
 )
as                        
Begin
   Select numBizDocId From OpportunityBizDocs Where numOppBizDocsId =@numOppBizDocsId
End
GO
