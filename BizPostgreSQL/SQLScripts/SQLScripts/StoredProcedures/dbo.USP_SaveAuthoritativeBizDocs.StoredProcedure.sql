/****** Object:  StoredProcedure [dbo].[USP_SaveAuthoritativeBizDocs]    Script Date: 07/26/2008 16:20:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by Siva  
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_saveauthoritativebizdocs')
DROP PROCEDURE usp_saveauthoritativebizdocs
GO
CREATE PROCEDURE [dbo].[USP_SaveAuthoritativeBizDocs]          
@numDomainID as numeric(9)=0,          
@numAuthoritativeMode as tinyint=0,  
@numAuthoritativePurchase as numeric(9)=0,  
@numAuthoritativeSales as numeric(9)=0  
          
as          
If @numAuthoritativeMode=0  
Begin  
   Insert into AuthoritativeBizDocs (numAuthoritativePurchase,numAuthoritativeSales,numDomainId)  values  
   (@numAuthoritativePurchase,@numAuthoritativeSales,@numDomainID)  
End          
Else  
Begin  
 Update AuthoritativeBizDocs Set numAuthoritativePurchase=@numAuthoritativePurchase,numAuthoritativeSales=@numAuthoritativeSales where numDomainId=@numDomainId  
End
GO
