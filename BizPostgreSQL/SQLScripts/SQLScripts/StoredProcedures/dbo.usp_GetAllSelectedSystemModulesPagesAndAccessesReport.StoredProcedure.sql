/****** Object:  StoredProcedure [dbo].[usp_GetAllSelectedSystemModulesPagesAndAccessesReport]    Script Date: 07/26/2008 16:16:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getallselectedsystemmodulespagesandaccessesreport')
DROP PROCEDURE usp_getallselectedsystemmodulespagesandaccessesreport
GO
CREATE PROCEDURE [dbo].[usp_GetAllSelectedSystemModulesPagesAndAccessesReport]             
 @numModuleID NUMERIC(9) = 0,              
 @numGroupID NUMERIC(9) = 0                 
--              
AS              
  select * from ReportList where rptGroup=0 AND [bitEnable]=1 ORDER BY rptSequence
GO
