-- exec USP_PlanningProcurement @numDomainID=72,@numVendorID=0
--created By Anoop Jayaraj
-- 2 change Group by delete
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_planningprocurement')
DROP PROCEDURE usp_planningprocurement
GO
CREATE PROCEDURE [dbo].[USP_PlanningProcurement]
    @numDomainID AS NUMERIC(9) = 0,
    @numVendorID AS NUMERIC(9) = 0,
    @ItemClassification AS NUMERIC(9) = 0,
    @OrderStatus AS NUMERIC(9) = 0
AS 
    
    
SELECT  txtItemDesc,
 vcPathForTImage,
 charItemType,
 numWarehouseItemID,
 numItemCode,
 vcItemName,
 vcWareHouse,
 vcModelID,
 numOnHand,
 numOnOrder,
 numReorder,
 numAllocation,
 numBackOrder,
 numVendorID,SUM(A.Units) AS Units,bitAssembly FROM 
	(SELECT  
            txtItemDesc,
            --kishan
            (SELECT TOP 1 vcPathForTImage 
            FROM  dbo.ItemImages 
            WHERE numItemCode = I.numItemCode AND BitDefault = 1 
            AND numDomainId = @numDomainId ) 
            AS vcPathForTImage,
			'Inventory Item' AS charItemType,
            WI.numWarehouseItemID,
            I.numItemCode,
            vcItemName + ' - '
            + ISNULL(dbo.fn_GetAttributes(WI.numWarehouseItemID, 0), '''') AS vcItemName,
            vcWareHouse,
            vcModelID,
            ISNULL(numOnHand, 0) numOnHand,
            ISNULL(numOnOrder, 0) numOnOrder,
            ISNULL(numReorder, 0) numReorder,
            ISNULL(numAllocation, 0) numAllocation,
            ISNULL(numBackOrder, 0) numBackOrder,
            I.numVendorID,
            SUM(ISNULL([numAllocation], 0) + ISNULL([numBackOrder], 0)) AS Units,CASE WHEN (I.bitKitParent =1 AND I.bitAssembly =1) THEN 1 ELSE 0 END as bitAssembly
    FROM    Item I
            INNER JOIN WareHouseItems WI ON I.numItemCode = WI.numItemID
            LEFT JOIN Vendor V ON V.numItemCode = I.numItemCode
            JOIN Warehouses W ON W.numWareHouseID = WI.numWareHouseID
    WHERE    (( bitKitParent = 0
              OR bitKitParent IS NULL
            ) OR (ISNULL(bitKitParent,0) =1 AND ISNULL(bitAssembly,0)=1))
            AND charItemType = 'P'
            AND ( (( ISNULL(numBackOrder, 0)-ISNULL([numOnOrder],0) ) > 0 AND @OrderStatus IN (0,1))
                  OR 
                  (ISNULL(numOnhand, 0) + ISNULL([numOnOrder], 0) <= ISNULL([numReorder], 0) 
					AND ISNULL(numReorder,0) >0 AND @OrderStatus IN (0,2))
--                  OR numOnhand <= ISNULL(numReorder, 0) + ISNULL(numOnOrder, 0)
                )
            AND I.numDomainID = @numDomainID
            AND ( @numVendorID = 0
                  OR V.[numItemCode] IN (
                  SELECT    [numItemCode]
                  FROM      [Vendor]
                  WHERE     [numVendorID] = @numVendorID )
                ) AND (I.numItemClassification=@ItemClassification OR @ItemClassification=0)                                        
    GROUP BY WI.numWarehouseItemID,
            I.numItemCode,
            vcItemName,
            vcWareHouse,
            vcModelID,
            numOnHand,
            numOnOrder,
            numReorder,
            numAllocation,
            numBackOrder,
            I.numVendorID,
            txtItemDesc,
           
            charItemType,I.bitAssembly,I.bitKitParent
--    UNION
--    SELECT  
--            I1.txtItemDesc,
--            I1.vcPathForTImage,
--            'Inventory Item' AS charItemType,
--            WI1.numWarehouseItemID,
--            I1.numItemCode,
--            I1.vcItemName + ' - '
--            + ISNULL(dbo.fn_GetAttributes(ID.numWareHouseItemId, 0), '') AS vcItemName,
--            W1.vcWareHouse,
--            I1.vcModelID,
--            ISNULL(WI1.numOnHand, 0) numOnHand,
--            ISNULL(WI1.numOnOrder, 0) numOnOrder,
--            ISNULL(WI1.numReorder, 0) numReorder,
--            ISNULL(WI1.numAllocation, 0) numAllocation,
--            ISNULL(WI1.numBackOrder, 0) numBackOrder,
--            I.numVendorID,
--            SUM(( ( ISNULL(WI1.[numAllocation], 0) + ISNULL(WI1.[numBackOrder],0) )
--                  * ISNULL(numQtyItemsReq, 0) - ISNULL(WI1.numOnHand, 0) )) AS Units,CASE WHEN (I1.bitKitParent =1 AND I1.bitAssembly =1) THEN 1 ELSE 0 END as bitAssembly
--    FROM    Item I
--            JOIN ItemDetails ID ON ID.numItemKitID = I.numItemCode
--            JOIN Item I1 ON I1.numItemCode = ID.numChildItemID
--            JOIN WareHouseItems WI1 ON WI1.numWareHouseItemID = ID.numWareHouseItemId
--            JOIN Warehouses W1 ON W1.numWareHouseID = WI1.numWareHouseID
--            LEFT JOIN Vendor V ON V.numItemCode = I1.numItemCode
--            INNER JOIN WareHouseItems WI ON I.numItemCode = WI.numItemID
--            JOIN Warehouses W ON W.numWareHouseID = WI.numWareHouseID
--    WHERE   I.bitKitParent = 1
--            AND I1.charItemType = 'P'
--            AND ( (( ISNULL(WI1.numBackOrder, 0) - ISNULL(WI1.[numOnOrder], 0)) > 0 AND @OrderStatus IN (0,1))
--                  OR ( ISNULL(WI1.numOnhand, 0) + ISNULL(WI1.[numOnOrder], 0) <= ISNULL(WI1.[numReorder], 0)
--                       AND ISNULL(WI1.numReorder, 0) > 0 AND @OrderStatus IN (0,2))
----                  OR numOnhand <= ISNULL(numReorder, 0) + ISNULL(numOnOrder, 0)
--                  
--                )
--            AND I.numDomainID = @numDomainID
--            AND ( @numVendorID = 0
--                  OR V.[numItemCode] IN (
--                  SELECT    [numItemCode]
--                  FROM      [Vendor]
--                  WHERE     [numVendorID] = @numVendorID )
--                ) AND (I1.numItemClassification=@ItemClassification OR @ItemClassification=0)
--    GROUP BY WI1.numWareHouseItemID,
--            ID.numWareHouseItemId,
--            I1.numItemCode,
--            I1.vcItemName,
--            W1.vcWareHouse,
--            I1.vcModelID,
--            WI1.numOnHand,
--            WI1.numOnOrder,
--            WI1.numReorder,
--            WI1.numAllocation,
--            WI1.numBackOrder,
--            I.numVendorID,
--            I1.txtItemDesc,
--            I1.vcPathForTImage,
--            I1.charItemType,I1.bitAssembly,I1.bitKitParent
            
     ) A 
     
     GROUP BY 
 txtItemDesc,
 vcPathForTImage,
 charItemType,
 numWarehouseItemID,
 numItemCode,
 vcItemName,
 vcWareHouse,
 vcModelID,
 numOnHand,
 numOnOrder,
 numReorder,
 numAllocation,
 numBackOrder,
 numVendorID,bitAssembly
 
ORDER BY vcWareHouse,
            vcItemName

