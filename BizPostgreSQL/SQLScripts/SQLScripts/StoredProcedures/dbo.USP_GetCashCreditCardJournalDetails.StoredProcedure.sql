/****** Object:  StoredProcedure [dbo].[USP_GetCashCreditCardJournalDetails]    Script Date: 07/26/2008 16:16:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created By Siva          
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getcashcreditcardjournaldetails')
DROP PROCEDURE usp_getcashcreditcardjournaldetails
GO
CREATE PROCEDURE [dbo].[USP_GetCashCreditCardJournalDetails]            
@numDomainId as numeric(9)=0,                
@numJournalId as numeric(9)=0                 
As                      
Begin                      
 Select GJD.numTransactionId as TransactionId, convert(varchar(10),GJD.numChartAcntId) +'~'+convert(varchar(10),CA.numAcntTypeId) as numChartAcntId,  
 CA.numAcntTypeId as numAcntType,   
 case when GJD.numDebitAmt  <>0 or GJD.numDebitAmt <> null then GJD.numDebitAmt   else GJD.numCreditAmt end as numDebitAmt,                    
 GJD.varDescription as  varDescription,GJD.numCustomerId as numCustomerId,GJD.numJournalId,              
 dbo.fn_GetRelationship_DivisionId(CI.numCompanyType,CI.numDomainID)+' - ' +convert(varchar(400),CI.numCompanyId) as varRelation              
 From General_Journal_Header GJH                      
 Inner join General_Journal_Details GJD  on GJH.numJournal_Id=GJD.numJournalId      
 inner join Chart_of_Accounts CA on  GJD.numChartAcntId=CA.numAccountId      
 Left outer join Companyinfo CI on GJD.numCustomerId=CI.numCompanyId              
 Where GJD.bitMainCashCredit=0 And GJH.numDomainId=@numDomainId And GJH.numJournal_Id=@numJournalId              
End
GO
