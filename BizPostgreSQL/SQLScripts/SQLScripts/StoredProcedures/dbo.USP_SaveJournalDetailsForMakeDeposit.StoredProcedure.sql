/****** Object:  StoredProcedure [dbo].[USP_SaveJournalDetailsForMakeDeposit]    Script Date: 07/26/2008 16:21:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Create by Siva                                                                
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_savejournaldetailsformakedeposit')
DROP PROCEDURE usp_savejournaldetailsformakedeposit
GO
CREATE PROCEDURE [dbo].[USP_SaveJournalDetailsForMakeDeposit]                                                                                
@numJournalId as numeric(9)=0,                                                                  
@strRow as text='',                                                  
@Mode as tinyint=0,                    
@RecurringMode as tinyint=0,  
@numDomainId as numeric(9)=0                                                             
                                                                                  
as                        
                    
Begin                       
if @RecurringMode=0                    
Begin                                                                      
if convert(varchar(100),@strRow) <> ''                                                                                    
Begin                                                                                    
  Declare @hDoc3 int                                                                                                                                        
   EXEC sp_xml_preparedocument @hDoc3 OUTPUT, @strRow                                                                                     
                                                              
                                                           
 if @Mode=0                                                  
Begin                                                                           
	Insert Into General_Journal_Details(numJournalId,numDebitAmt,numCreditAmt,numChartAcntId,varDescription,numCustomerId,numDomainId,bitMainDeposit,vcReference,numPaymentMethod,bitReconcile)                                                                    
	Select * from (                                                                                     
	Select * from OPenXml (@hdoc3,'/NewDataSet/Table1 [TransactionId=0]',2)                                                                                      
	With(                                                                                    
	numJournalId numeric(9),                                                                                    
	numDebitAmt DECIMAL(20,5),                                                                                     
	numCreditAmt DECIMAL(20,5),                                                                                      
	numChartAcntId numeric(9),                                                                                    
	vcMemo varchar(8000),                                                                                    
	numCustomerId numeric(9),                                                                        
	numDomainId numeric(9),                                        
	bitMainDeposit bit,        
	vcReference varchar(50),        
	numPaymentMethod numeric(9),    
	bitReconcile bit))X                                                                          
                                                                      
--Exec USP_UpdateChartAcntOpnBalance @JournalId=@numJournalId ,@numDomainId=@numDomainId                                                                
End                                                                         
If @Mode=1                                                  
Begin                                                  
                                                 
                                            
                                            
--Create table #tempTable ( numTransactionId numeric(9),                                                                                                        
--      numJournalId numeric(9),                                                              
--      numDebitAmt DECIMAL(20,5),                                                              
--      numCreditAmt DECIMAL(20,5),                                               
--      numChartAcntId numeric(9),                                                              
--      varDescription varchar(8000),                                                              
--      numCustomerId numeric(9),                                  
--      numDomainId numeric(9))                     
--                                                              
--                       
--                                                              
-- Insert into #tempTable (numTransactionId,numJournalId,numDebitAmt,numCreditAmt,numChartAcntId,varDescription,                             
--     numCustomerId,numDomainId)  Select numTransactionId,numJournalId,numDebitAmt,numCreditAmt,numChartAcntId,varDescription,                                                              
--     numCustomerId,numDomainId                                                             
--      from  General_Journal_Details Where numJournalId=@numJournalId And  numDomainId=@numDomainId                                                    
--                                                  
                                                  
                                                                     
Update General_Journal_Details  Set numDebitAmt=X.numDebitAmt,numCreditAmt=X.numCreditAmt,                                                                      
numChartAcntId=X.numChartAcntId,varDescription=X.vcMemo,numCustomerId=X.numCustomerId,bitMainDeposit=X.bitMainDeposit,        
vcReference=X.vcReference,numPaymentMethod=X.numPaymentMethod                                                                      
From (SELECT * FROM OPENXML(@hDoc3,'/NewDataSet/Table1 [TransactionId>0]',2)                                                                                      
With(                                                               
TransactionId numeric(9),                                                                                 
numJournalId numeric(9),                                             
numDebitAmt DECIMAL(20,5),                                                                                     
numCreditAmt DECIMAL(20,5),                                                                         
numChartAcntId numeric(9),                                                                
vcMemo varchar(8000),                                                                                    
numCustomerId numeric(9),                                                                        
numDomainId numeric(9),                                        
bitMainDeposit bit,        
vcReference varchar(50),        
numPaymentMethod numeric(9)        
))X                                                                          
Where numTransactionId=X.TransactionId                                                                      
                                                 
         
   Insert Into General_Journal_Details(numJournalId,numDebitAmt,numCreditAmt,numChartAcntId,varDescription,numCustomerId,numDomainId,bitMainDeposit,vcReference,numPaymentMethod,bitReconcile)                                                                    
	Select * from (                                                                                     
	Select * from OPenXml (@hdoc3,'/NewDataSet/Table1 [TransactionId=0]',2)                                                                                      
	With(                                                                                    
	numJournalId numeric(9),                                                                                    
	numDebitAmt DECIMAL(20,5),                                                                                     
	numCreditAmt DECIMAL(20,5),                                                                                      
	numChartAcntId numeric(9),                                                                                    
	vcMemo varchar(8000),                                                                                    
	numCustomerId numeric(9),                                                                        
	numDomainId numeric(9),                                        
	bitMainDeposit bit,        
	vcReference varchar(50),        
	numPaymentMethod numeric(9),    
	bitReconcile bit))X
--Declare @numTransactionId as numeric(9)                                                 
--Declare @numChartAcntId as  varchar(8000)                                                          
--Declare @numChartAcntId1 as varchar(8000)                                                          
--Declare @numDebitAmt as DECIMAL(20,5)                                                          
--Declare @numCreditAmt as DECIMAL(20,5)                                                          
--Declare @strSQl as varchar(8000)                                                         
--Declare @strSQl1 as varchar(8000)                                                        
--Declare @strSQl2 as varchar(8000)                     
--Declare @numChartAcntTypeId as integer                   
--                                                        
--Set @strSQl1=''                                                         
--Set @strSQl=''                                                          
--Set @strSQl2=''            
--                                                          
--Select @numTransactionId=min(numTransactionId) From General_Journal_Details Where numJournalId=@numJournalId  And  numDomainId=@numDomainId                                                          
--                                                          
--While @numTransactionId <> 0                                                          
--Begin                                                          
--                                                            Select @numChartAcntId=numChartAcntId,@numDebitAmt=numDebitAmt,@numCreditAmt=numCreditAmt From #tempTable Where numTransactionId=@numTransactionId  And  numDomainId=@numDomainId                 
--                                  
--   --Select * From General_Journal_Details Where numTransactionId=@numTransactionId                                                           
--    --Set @numChartAcntId1= @numChartAcntId               
--   print @numChartAcntId                                
--   --Select @numChartAcntTypeId =numAcntType  From Chart_Of_Accounts Where numAccountId=@numChartAcntId  And  numDomainId=@numDomainId                      
--   print @numChartAcntTypeId                                         
--   --Set @strSQl=dbo.fn_ParentCategory(@numChartAcntId,@numDomainId)                                        
--    if @numDebitAmt <> 0                                                           
--    Begin                                                        
--      if @strSQl <>''                            
--        Begin                  
--   if @numChartAcntTypeId = 813 Or @numChartAcntTypeId = 814 Or @numChartAcntTypeId = 817 Or @numChartAcntTypeId = 818 Or @numChartAcntTypeId = 819 Or @numChartAcntTypeId = 823 Or @numChartAcntTypeId = 824 Or @numChartAcntTypeId = 826                     
--  
--           set @strSQl2='update chart_of_accounts  set numopeningbal=isnull(numopeningbal,0) - ' + convert(varchar(30),@numDebitAmt) + ' where  numAccountId in ('+@numChartAcntId+','+@strSQl+')  And  numDomainId='+Convert(varchar(10),@numDomainId)  
--   
--      
--        
--          
--            
--               
--          else                  
--            set @strSQl2='update chart_of_accounts  set numopeningbal=isnull(numopeningbal,0) + ' + convert(varchar(30),@numDebitAmt) + ' where  numAccountId in ('+@numChartAcntId+','+@strSQl+')  And  numDomainId='+Convert(varchar(10),@numDomainId)       
--                                                      
--    
--      
--        
--          
--            
--              
--               
--         End                    
--      else                    
--        Begin                  
--         if @numChartAcntTypeId = 813 Or @numChartAcntTypeId = 814 Or @numChartAcntTypeId = 817 Or @numChartAcntTypeId = 818 Or @numChartAcntTypeId = 819 Or @numChartAcntTypeId = 823 Or @numChartAcntTypeId = 824 Or @numChartAcntTypeId = 826               
--  
--    
--      
--        
--         set @strSQl2='update chart_of_accounts  set numopeningbal=isnull(numopeningbal,0) - ' + convert(varchar(30),@numDebitAmt) + ' where  numAccountId in ('+@numChartAcntId+')  And  numDomainId='+Convert(varchar(10),@numDomainId)                      
--                                                    
--         else                  
--         set @strSQl2='update chart_of_accounts  set numopeningbal=isnull(numopeningbal,0) + ' + convert(varchar(30),@numDebitAmt) + ' where  numAccountId in ('+@numChartAcntId+')  And  numDomainId='+Convert(varchar(10),@numDomainId)                      
--                                                   
--        End                      
--     Exec(@strSQl2)                                                      
--      print(@strSQl2)                                                         
--    End                                                          
--    if @numCreditAmt <> 0                                                           
--    Begin                                                          
--                                                              if @strSQl<>''                   
--    Begin                  
--     if @numChartAcntTypeId = 813 Or @numChartAcntTypeId = 814 Or @numChartAcntTypeId = 817 Or @numChartAcntTypeId = 818 Or @numChartAcntTypeId = 819 Or @numChartAcntTypeId = 823 Or @numChartAcntTypeId = 824 Or @numChartAcntTypeId = 826                   
--  
--    
--     set @strSQl1='update chart_of_accounts  set numopeningbal=isnull(numopeningbal,0) + ' + convert(varchar(30),@numCreditAmt) + ' where  numAccountId in ('+@numChartAcntId+','+@strSQl+')  And  numDomainId='+Convert(varchar(10),@numDomainId)             
--                                
--     else                  
--     set @strSQl1='update chart_of_accounts  set numopeningbal=isnull(numopeningbal,0) - ' + convert(varchar(30),@numCreditAmt) + ' where  numAccountId in ('+@numChartAcntId+','+@strSQl+')  And  numDomainId='+Convert(varchar(10),@numDomainId)             
--                               
--     End                  
--    else                  
--    Begin                  
--     if @numChartAcntTypeId = 813 Or @numChartAcntTypeId = 814 Or @numChartAcntTypeId = 817 Or @numChartAcntTypeId = 818 Or @numChartAcntTypeId = 819 Or @numChartAcntTypeId = 823 Or @numChartAcntTypeId = 824 Or @numChartAcntTypeId = 826                   
--  
--    
--     Begin                
--         print 'Shlok'                
--     set @strSQl1='update chart_of_accounts  set numopeningbal=isnull(numopeningbal,0) + ' + convert(varchar(30),@numCreditAmt) + ' where numAccountId in ('+@numChartAcntId+')  And  numDomainId='+Convert(varchar(10),@numDomainId)                          
--                                              
--     End                
--    else                  
--     set @strSQl1='update chart_of_accounts  set numopeningbal=isnull(numopeningbal,0) - ' + convert(varchar(30),@numCreditAmt) + ' where numAccountId in ('+@numChartAcntId+')  And  numDomainId='+Convert(varchar(10),@numDomainId)                          
--                                               
-- End                                                     
--    --Exec(@strSQl1)                                                          
--      --print(@strSQl1)                                                         
--    End                                                     
--                                             
--    print (@numChartAcntId1)                                                          
--    Print ('Debit'+ convert(varchar(200),@numDebitAmt))                                                        
--    print ('Credit'+ convert(varchar(200),@numCreditAmt))                               
--   Select @numTransactionId=min(numTransactionId) From General_Journal_Details Where numJournalId=@numJournalId And  numTransactionId>@numTransactionId                                                          
--End                                      
--                                            
                                            
--Insert Into General_Journal_Details(numJournalId,numDebitAmt,numCreditAmt,numChartAcntId,varDescription,numCustomerId,numDomainId,bitMainDeposit,vcReference,numPaymentMethod,bitReconcile)                                             
--Select * from (                                                                                     
--Select * from OPenXml (@hdoc3,'/NewDataSet/Table1 [TransactionId=0]',2)                                                        
--With(                                                                                    
--numJournalId numeric(9),                                                                                    
--numDebitAmt DECIMAL(20,5),                                                                                     
--numCreditAmt DECIMAL(20,5),                                                                                  
--numChartAcntId numeric(9),                                                                                    
--vcMemo varchar(8000),                                                                                    
--numCustomerId numeric(9),                                                                        
--numDomainId numeric(9),                                        
--bitMainDeposit bit,        
--vcReference varchar(50),        
--numPaymentMethod numeric(9),    
--bitReconcile bit        
--))X                                                                          
                                                                      
--                                                                      
----Exec USP_UpdateChartAcntOpnBalance @JournalId=@numJournalId,@numDomainId=@numDomainId                                                           
--drop table #tempTable                                          
End                                                    
EXEC sp_xml_removedocument @hDoc3
                                                                  
End                                              
End        
                    
  ---For Recurring Transaction                            
                            
if @RecurringMode=1                            
Begin                            
  Declare @numMaxJournalId as numeric(9)                            
  Select @numMaxJournalId=max(numJournal_Id) From General_Journal_Header Where numDomainId=@numDomainId                             
  insert into General_Journal_Details(numJournalId,numDebitAmt,numCreditAmt,numChartAcntId,varDescription,numCustomerId,numDomainId,bitMainDeposit,vcReference,numPaymentMethod)                            
  Select @numMaxJournalId,numDebitAmt,numCreditAmt,numChartAcntId,varDescription,numCustomerId,numDomainId,bitMainDeposit,vcReference,numPaymentMethod from General_Journal_Details Where numJournalId=@numJournalId And numDomainId=@numDomainId           
                            
--Exec USP_UpdateChartAcntOpnBalance @JournalId=@numMaxJournalId,@numDomainId=@numDomainId                            
End                        
End
GO
