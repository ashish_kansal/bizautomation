/****** Object:  StoredProcedure [dbo].[USP_SaveOperationBudgetHeader]    Script Date: 07/26/2008 16:21:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created By Siva                          
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_saveoperationbudgetheader')
DROP PROCEDURE usp_saveoperationbudgetheader
GO
CREATE PROCEDURE [dbo].[USP_SaveOperationBudgetHeader]                          
@numBudgetId as numeric(9)=0,                          
@vcBudgetName as varchar(100)='',                
@numDepartmentId as numeric(9)=0,             
@numDivisionId as numeric(9)=0,                          
@numCostCenterId as numeric(9)=0,        
@intFiscalYear as numeric(9)=0,                 
@numDomainId as numeric(9)=0,                          
@numUserCntId as numeric(9)=0                          
As                          
Begin         
Declare @count as integer      
      
----Select @count=Count(*) From OperationBudgetMaster      
----Where numDivisionId=@numDivisionId And numDepartmentId=@numDepartmentId      
----And numCostCenterId=@numCostCenterId And numDomainId=@numDomainId --And numBudgetId<>@numBudgetId --And @intFiscalYear=intFiscalYear          
if @numBudgetId =0                      
Begin                      
 Insert into OperationBudgetMaster(vcBudgetName,numDivisionId,numDepartmentId,numCostCenterId,intFiscalYear,numDomainId,numCreatedBy,dtCreationDate)                          
 Values(@vcBudgetName,@numDivisionId,@numDepartmentId,@numCostCenterId,@intFiscalYear,@numDomainId,@numUserCntId,getutcdate())                          
 Set @numBudgetId = SCOPE_IDENTITY()                                                        
 Select @numBudgetId                            
End                          
Else                      
Begin                      
 Update OperationBudgetMaster Set vcBudgetName=@vcBudgetName,numDivisionId=@numDivisionId,numDepartmentId=@numDepartmentId,numCostCenterId=@numCostCenterId,          
 numModifiedBy=@numUserCntId,dtModifiedDate=getutcdate() Where numBudgetId=@numBudgetId And  numDomainId=@numDomainId                    
 Select @numBudgetId                       
End                      
End
GO
