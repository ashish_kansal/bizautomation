/****** Object:  StoredProcedure [dbo].[USP_KeyWordsUsed]    Script Date: 07/26/2008 16:19:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--declare @p5 int
--set @p5=695
--exec USP_KeyWordsUsed @From='2009-05-25 00:00:00:000',@To='2009-09-03 12:23:59:407',@CurrentPage=1,@PageSize=5,@TotRecs=@p5 output,@columnName='count',@columnSortOrder='Desc',@numDomainID=1,@ClientOffSetTime=-330
--select @p5
--created by anoop jayaraj      
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_keywordsused')
DROP PROCEDURE usp_keywordsused
GO
CREATE PROCEDURE [dbo].[USP_KeyWordsUsed]      
@From datetime,      
@To datetime,      
@CurrentPage int,              
@PageSize int,              
@TotRecs int output,              
@columnName as Varchar(50),              
@columnSortOrder as Varchar(10) ,    
@numDomainID as numeric(9)  ,  
@ClientOffSetTime as int           
as              
              
--Create a Temporary table to hold data              
Create table #tempTable       
    ( ID INT IDENTITY PRIMARY KEY,               
      vcSearchTerm varchar(100),      
       [Count]  Varchar(15)       
      )              
declare @strSql as varchar(5000)              
set @strSql='select distinct(vcSearchTerm),       
Count(*) as [Count]
from   TrackingVisitorsHDR HDR      
where numDomainId='+convert(varchar(10),@numDomainID)+' and (dtCreated between '''+convert(varchar(50),dateadd(minute,@ClientOffSetTime,@From))+''' and '''+
convert(varchar(50),dateadd(minute,@ClientOffSetTime,@To))+''') and vcSearchTerm is not null'         
set  @strSql=@strSql +' Group By vcSearchTerm ' 
set  @strSql=@strSql +' ORDER BY ' + @columnName +' '+ @columnSortOrder            
print @strSql        
insert into #tempTable (vcSearchTerm,[Count] )              
exec( @strSql)              
  declare @firstRec as integer              
  declare @lastRec as integer              
 set @firstRec= (@CurrentPage-1) * @PageSize              
     set @lastRec= (@CurrentPage*@PageSize+1)              
select * from #tempTable where ID > @firstRec and ID < @lastRec              
set @TotRecs=(select count(*) from #tempTable)              
drop table #tempTable
GO
