/****** Object:  StoredProcedure [dbo].[USP_CreateUsers]    Script Date: 07/26/2008 16:15:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj        
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_createusers')
DROP PROCEDURE usp_createusers
GO
CREATE PROCEDURE [dbo].[USP_CreateUsers]                
@vcUserName as varchar(100),                
@vcDomain as varchar(100),                
@noOfLicences as numeric(9),          
@vcEmailID as varchar(100),        
@vcDisplayName as varchar(100),        
@vcMailName as varchar(100)                 
as                
declare @numUserId numeric(9)                
declare @numDomainID numeric(9)      
declare @numResourceId numeric(9)          
set @numUserId=(select numUserid from usermaster where vcMailNickName=@vcMailName)                
set @numResourceId=(select ResourceId from resource where ResourceName=@vcMailName)                
set @numDomainID=(select numDomainID from Domain where vcDomainName=@vcDomain)              
if @noOfLicences > (select count(*) from UserMaster where bitActivateFlag=1)                
 begin                 
  if not exists(select numUserid from usermaster where vcMailNickName=@vcMailName)                
  begin   
	DECLARE @UserId	integer;             
   insert into UserMaster (                
      vcUserName,                
      vcUserDesc,        
      vcMailNickName,            
      numGroupID,                
      numDomainID,                
      numCreatedBy,                
      bintCreatedDate,                
      numModifiedBy,                
      bintModifiedDate,                
      bitActivateFlag,                
      numUserDetailId,          
      vcEmailID)          
  values                
     (@vcUserName,                
     @vcDisplayName,        
     @vcMailName,            
     0,                
     @numDomainID,                
     1,                
     getutcdate(),                
     1,                
     getutcdate(),                
     1,0,@vcEmailID)  
		SELECT
		@UserId = SCOPE_IDENTITY();

EXEC	[dbo].[Resource_Add]
		@ResourceName = @vcUserName,
		@ResourceDesc = vcUserDesc,
		@ResourceEmail = @vcEmailID,
		@EnableEmailReminders = 1,
		@DomainId = @numDomainID,
		@numUserCntId = @UserId             
                   
                  
  end                
  else                
  begin                
                  
    update UserMaster                
    set bitActivateFlag=1 ,numDomainID=@numDomainID,vcEmailID=@vcEmailID,vcUserDesc=@vcDisplayName ,vcMailNickName=@vcMailName,vcUserName=  @vcUserName          
    where numUserId=@numUserId   
    
		EXEC	[dbo].[Resource_Upd]
		@ResourceID= @numResourceID,
		@ResourceName = @vcUserName,
		@ResourceDesc = vcUserDesc,
		@ResourceEmail = @vcEmailID,
		@EnableEmailReminders = 1
	       
                        
  end     
  select Count(*) from UserMaster where bitActivateFlag=1 and numDomainID=@numDomainID                
  end     
  else select -1
GO
