/****** Object:  StoredProcedure [dbo].[usp_GetTaxDetails]    Script Date: 07/26/2008 16:18:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_gettaxdetails')
DROP PROCEDURE usp_gettaxdetails
GO
CREATE PROCEDURE [dbo].[usp_GetTaxDetails]  
@numDomainId as numeric(9),
@numTaxItemID as numeric(9)=0,
@numCountry as numeric(9)=0,  
@numState as numeric(9)=0  
AS   
	SELECT  
		numTaxID,  
		[dbo].[fn_GetListName](numCountryID,0) as country,  
		CASE WHEN numStateID = 0 THEN 'All' ELSE [dbo].[fn_GetState](numStateID) END AS [state],  
		decTaxPercentage, 
		CASE 
			WHEN TaxDetails.numTaxItemID = 0 OR TaxDetails.numTaxItemID IS NULL THEN 'Sales Tax(Default)' 
			WHEN TaxDetails.numTaxItemID = 1 THEN CONCAT('CRV',CASE WHEN ISNULL(vcTaxUniqueName ,'') = '' THEN '' ELSE ' (' + vcTaxUniqueName + ')' END)
			ELSE ISNULL(vcTaxName,'') 
		END AS vcTaxName
		,ISNULL(vcCity,'') as vcCity
		,ISNULL(vcZipPostal,'') as vcZipPostal,
		CASE WHEN tintTaxType = 2 THEN CONCAT(FORMAT(decTaxPercentage,'##0.0000'),' (Flat Amount)') ELSE CONCAT(FORMAT(decTaxPercentage,'##0.0000'),' %') END AS vcTax,
		tintTaxType
	FROM 
		TaxDetails
	LEFT JOIN 
		TaxItems
	ON 
		TaxItems.numTaxItemID=TaxDetails.numTaxItemID
	WHERE 
		TaxDetails.numdomainid = @numDomainId 
		AND (TaxDetails.numTaxItemID=@numTaxItemID OR @numTaxItemID=0)
		AND (TaxDetails.numCountryID=@numCountry OR @numCountry=0)
		AND (TaxDetails.numStateID=@numState OR @numState=0)
GO
