/****** Object:  StoredProcedure [dbo].[USP_GetGatewatDTls]    Script Date: 07/26/2008 16:17:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getgatewatdtls')
DROP PROCEDURE usp_getgatewatdtls
GO
CREATE PROCEDURE [dbo].[USP_GetGatewatDTls]    
@numDomainID as numeric(9),
@numSiteId  as  numeric(9) 
AS    
BEGIN    
	IF ISNULL(@numSiteId,0) > 0 AND (SELECT ISNULL(intPaymentGateWay,0) FROM Sites WHERE numDomainId=@numDomainID AND numSiteID=@numSiteID) > 0
	BEGIN
		SELECT intPaymentGateWay FROM Sites WHERE numDomainId=@numDomainID AND numSiteID=@numSiteID
	END
	ELSE
	BEGIN
		SELECT intPaymentGateWay FROM Domain WHERE numDomainId=@numDomainID
	END

	SELECT 
		PG.intPaymentGateWay
		,vcGateWayName
		,vcFirstFldName
		,vcSecndFldName
		,vcThirdFldName
		,vcFirstFldValue
		,vcSecndFldValue
		,vcThirdFldValue
		,isnull(bitTest,0) bitTest
		,isnull(vcToolTip,'') vcToolTip  
	FROM 
		PaymentGateway PG    
	LEFT JOIN 
		PaymentGatewayDTLID PGDTLID    
	ON 
		PG.intPaymentGateWay=PGDTLID.intPaymentGateWay 
		AND numDomainID=@numDomainID 
		AND PGDTLID.numSiteId=@numSiteId
END
GO
