/****** Object:  StoredProcedure [dbo].[USP_GetTimeAndExpense]    Script Date: 07/26/2008 16:18:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj    
-- SELECT TOP 1 * FROM [TimeAndExpense] ORDER BY [numCategoryHDRID] DESC 
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetTimeAndExpense')
DROP PROCEDURE USP_GetTimeAndExpense
GO
CREATE PROCEDURE [dbo].[USP_GetTimeAndExpense]
    @numUserCntID AS NUMERIC(9) = 0,
    @numDomainID AS NUMERIC(9) = 0,
    @strDate AS VARCHAR(10) = '',
    @numCategoryHDRID AS NUMERIC(9) = 0,
    @ClientTimeZoneOffset AS INT,
    @tintTEType AS TINYINT = 0,
    @numCaseId AS NUMERIC(9) = 0,
    @numProId AS NUMERIC(9) = 0,
    @numOppId AS NUMERIC(9) = 0,
    @numStageId AS NUMERIC(9) = 0,
    @numCategory AS TINYINT = 0
AS 
	IF @strDate = '12:00:00 A'
	BEGIN
		SET @strDate = CONVERT(varchar(10), GETDATE(), 20)
	END

    IF ( @numCategoryHDRID = 0
         AND @tintTEType = 0
       ) 
        BEGIN                                  
            SELECT  CASE WHEN tintTEType = 0 THEN 'T & E'
                         WHEN tintTEType = 1
                         THEN CASE WHEN numCategory = 1
                                   THEN CASE WHEN ( SELECT  tintopptype
                                                    FROM    opportunitymaster
                                                    WHERE   numoppid = TE.numOppId
                                                  ) = 1 THEN 'Sales Time'
                                             ELSE 'Purch Time'
                                        END
                                   ELSE CASE WHEN ( SELECT  tintopptype
                                                    FROM    opportunitymaster
                                                    WHERE   numoppid = TE.numOppId
                                                  ) = 1 THEN 'Sales Exp'
                                             ELSE 'Purch Exp'
                                        END
                              END
                         WHEN tintTEType = 2
                         THEN CASE WHEN numCategory = 1 THEN 'Project Time'
                                   ELSE 'Project Exp'
                              END
                         WHEN tintTEType = 3
                         THEN CASE WHEN numCategory = 1 THEN 'Case Time'
                                   ELSE 'Case Exp'
                              END
                         WHEN tintTEType = 4
                         THEN 'Non-Paid Leave'
						  WHEN tintTEType = 5
                         THEN 'Expense'
                    END AS chrFrom,
                    numCategoryHDRID,
                    tintTEType,
                   CASE WHEN numCategory = 1 AND numType=1 THEN 'Billable Time'
						 WHEN numCategory = 1 AND numType=2 THEN 'Non-Billable Time'
                         WHEN numCategory = 2 AND ISNULL(bitReimburse,0) = 1 AND numType = 2  THEN 'Reimbursable Expense'
                         WHEN numCategory = 2 AND ISNULL(bitReimburse,0) = 1 AND numType = 6 THEN 'Billable + Reimbursable Expense'
                         WHEN numCategory = 2 AND ISNULL(bitReimburse,0) = 0 AND numType = 1 THEN 'Billable Expense'
                    END  AS Category,
                    CASE WHEN numType = 1 THEN 'Billable'
                         WHEN numType = 2 THEN 'Non-Billable'
                         WHEN numType = 5 THEN 'Reimbursable Expense'
                         WHEN numType = 6 THEN 'Billable'
                         --WHEN numType = 3 THEN 'Paid Leave'
                         --WHEN numType = 4 THEN 'Non-Paid'
                    END AS [Type],
                    CASE WHEN numCategory = 1
                         THEN DATEADD(minute, -@ClientTimeZoneOffset,
                                      dtFromDate)
                         ELSE dtFromDate
                    END AS dtFromDate,
                    CASE WHEN numCategory = 1
                         THEN DATEADD(minute, -@ClientTimeZoneOffset, dtToDate)
                         ELSE dtToDate
                    END AS dtToDate,
                    bitFromFullDay,
                    bitToFullDay,
                    CONVERT(DECIMAL(10, 2), monAmount) AS [monAmount],
                    TE.numOppId,
                    OM.vcPOppName,
					CA.vcAccountName,
                    TE.numDivisionID,
                    txtDesc,
                    numUserCntID,
                    ISNULL(ACI.vcFirstName,'') + ' ' + ISNULL(ACI.vcLastName,'') AS [vcEmployee],
                    TE.numDomainID,
                    TE.numContractId,
                    numCaseid,
                    TE.numProid,
                    PM.vcProjectId,
                    numOppBizDocsId,
                    numStageId AS numStageID,
                    numCategory,
                    numtype,
                    CASE WHEN numCategory = 1
                         THEN CONVERT(VARCHAR, CONVERT(FLOAT, DATEDIFF(mi, dtFromDate, dtToDate)) / 60) 
                         WHEN numCategory = 2
                         THEN CONVERT(VARCHAR, CONVERT(DECIMAL(10, 2), monAmount))
                         WHEN numCategory = 3
                         THEN ( CASE WHEN bitFromFullDay = 0 THEN 'HDL' 
                                     WHEN bitFromFullDay = 1 THEN 'FDL'
                                END )
						 WHEN numCategory = 4
                         THEN ( CASE WHEN bitFromFullDay = 0 THEN 'HDL'
                                     WHEN bitFromFullDay = 1 THEN 'FDL'
                                END )
                    END AS Detail
                    ,CONVERT(VARCHAR, CONVERT(FLOAT, DATEDIFF(mi, dtFromDate, dtToDate)) / 60) [TimeValue]
                    ,CASE WHEN numCategory = 1 THEN CONVERT(DECIMAL(10, 2), (CONVERT(FLOAT, DATEDIFF(mi, dtFromDate, dtToDate)) / 60) * ISNULL(TE.monAmount,1)) 
						  WHEN numCategory = 2 THEN CONVERT(DECIMAL(10, 2),ISNULL(TE.monAmount,1))
						  ELSE 0
						  END AS [ExpenseValue]
                    ,(SELECT ISNULL(numItemCode,0) FROM item  WHERE numItemCode IN (SELECT numItemCode FROM dbo.OpportunityItems WHERE numoppitemtCode = TE.numOppItemID )) AS [numItemCode]
					,(SELECT ISNULL(numClassID,0) FROM dbo.OpportunityItems WHERE numoppitemtCode = TE.numOppItemID ) AS [numClassID]
					,(SELECT ISNULL(numClassID,0) FROM dbo.OpportunityItems WHERE numoppitemtCode = TE.numOppItemID ) AS [numClassID]
					,(SELECT ISNULL(vcItemDesc,'') FROM dbo.OpportunityItems WHERE numoppitemtCode = TE.numOppItemID ) AS [vcItemDesc]
					,CASE WHEN ISNULL(TE.numApprovalComplete,0)=0 THEN 'Approved' WHEN TE.numApprovalComplete=-1 THEN 'Declined' ELSE 'Pending Approval' END as ApprovalStatus
					, ISNULL(ACIC.vcFirstName,'') + ' ' + ISNULL(ACIC.vcLastName,'') +','+ CONVERT(VARCHAR,(DateAdd(minute, -@ClientTimeZoneOffset, TE.dtTCreatedOn))) as CreatedDate
					,CASE WHEN I.vcItemName='' THEN '' ELSE '('+I.vcItemName+')' END as vcItemName
					,Case WHEN numCategory = 1 AND numType=1 AND OM.numOppId>0 
						THEN ((ISNULL(CAST(DATEDIFF(MINUTE,TE.dtfromdate,TE.dttodate) AS FLOAT)/CAST(60 AS FLOAT),0))*TE.monAmount)
					 WHEN  OM.numOppId>0 
						THEN TE.monAmount 
					ELSE '0'
						END [ClientCharge],
					Case WHEN numCategory = 1 AND (numType=1 or numType=2)
						THEN ((ISNULL(CAST(DATEDIFF(MINUTE,TE.dtfromdate,TE.dttodate) AS FLOAT)/CAST(60 AS FLOAT),0))*
						(select ISNULL(monHourlyRate,0) from UserMaster where numUserDetailId=TE.numUserCntID    
						and UserMaster.numDomainID=@numDomainID))
					 WHEN numCategory = 2
						THEN TE.monAmount 
					ELSE  0
						END [EmployeeCharge]
			FROM    TimeAndExpense TE
            LEFT JOIN dbo.OpportunityMaster OM ON om.numoppId = TE.numOppId AND TE.numDomainID = OM.numDomainID
			LEFT JOIN Item as I ON TE.numServiceItemID=I.numItemCode
            LEFT JOIN dbo.ProjectsMaster PM ON PM.numProId = TE.numProID AND TE.numDomainID = PM.numDomainID
            LEFT JOIN dbo.Chart_Of_Accounts CA ON CA.numAccountId = TE.numExpId AND TE.numDomainID = CA.numDomainID
            LEFT JOIN AdditionalContactsInformation ACI ON ACI.numContactID = TE.numUserCntID
			LEFT JOIN AdditionalContactsInformation ACIC ON ACIC.numContactID = TE.numTCreatedBy
            WHERE   (numUserCntID = @numUserCntID OR @numUserCntID = 0)
                    AND TE.numDomainID = @numDomainID
                    AND (TE.numType = @numCategory OR @numCategory = 0)
                    AND                                   
--  (@strDate between dateadd(minute,-@ClientTimeZoneOffset,dtFromDate) and dateadd(minute,-@ClientTimeZoneOffset,dtToDate)   
-- or( day(dateadd(minute,-@ClientTimeZoneOffset,dtFromDate))=day(@strDate)   
--and month(dateadd(minute,-@ClientTimeZoneOffset,dtFromDate))=month(@strDate)   
--and year(dateadd(minute,-@ClientTimeZoneOffset,dtFromDate))=year(@strDate)))       
                    ( 
--						( @strDate >= CASE WHEN numCategory = 1
--                                         THEN DATEADD(minute,
--                                                      -@ClientTimeZoneOffset,
--                                                      dtFromDate)
--                                         ELSE dtFromDate
--                                    END
--                        AND @strDate <= CASE WHEN numCategory = 1
--                                             THEN DATEADD(minute,
--                                                          -@ClientTimeZoneOffset,
--                                                          dttoDate)
--                                             ELSE dttoDate
--                                        END
--                      )
--                      OR 
                      ( DAY(@strDate) = CASE WHEN numCategory = 1
                                                THEN DAY(DATEADD(minute, -@ClientTimeZoneOffset, dtFromDate))
                                                ELSE DAY(dtfromdate)
                                           END
                           AND MONTH(@strDate) = CASE WHEN numCategory = 1
                                                      THEN MONTH(DATEADD(minute, -@ClientTimeZoneOffset, dtFromDate))
                                                      ELSE MONTH(dtfromdate)
                                                 END
                           AND YEAR(@strDate) = CASE WHEN numCategory = 1
                                                     THEN YEAR(DATEADD(minute, -@ClientTimeZoneOffset, dtFromDate))
                                                     ELSE YEAR(dtfromdate)
                                                END
                         )
                    )                       
                
        END                                  
    ELSE 
        BEGIN              
      
DECLARE @TotalTime as DECIMAL(10, 2),@TotalExpense as DECIMAL(10, 2);
SET @TotalTime=0;SET @TotalExpense=0

DECLARE @TimeBudget as DECIMAL(10, 2),@ExpenseBudget as DECIMAL(10, 2);
SET @TimeBudget=0;SET @ExpenseBudget=0

DECLARE @bitTimeBudget as bit,@bitExpenseBudget as BIT
SET @bitTimeBudget = 0;SET @bitExpenseBudget=0


SELECT @TotalTime=isnull(CONVERT(DECIMAL(10, 2), CAST( SUM(monamount * CAST(CAST(DATEDIFF(minute, dtFromDate, dtToDate) AS DECIMAL(10,2))/60 AS DECIMAL(10,2)) ) AS DECIMAL(10,2))),0)
from  timeandexpense where numProID=@numProId and numStageID=@numStageId AND numCategory = 1 and tintTEType = 2 
AND numType= 1 GROUP BY numType

SELECT @TotalExpense=isnull([dbo].[fn_GetExpenseDtlsbyProStgID](@numProId,@numStageId,1),0)


select @TimeBudget=isnull(monTimeBudget,0),@ExpenseBudget=isnull(monExpenseBudget,0),@bitTimeBudget=isnull(bitTimeBudget,0),@bitExpenseBudget=isnull(bitExpenseBudget,0) from StagePercentageDetails where numProjectID=@numProId and numStageDetailsId=@numStageId

if @bitTimeBudget=1
   SET @TotalTime=@TimeBudget-@TotalTime

if @bitExpenseBudget=1
   SET @TotalExpense=@TotalExpense-@TotalExpense
            DECLARE @sql AS VARCHAR(8000)                              
            SET @sql = '          
 select           
 isnull(numCategoryHDRID,0)as numCategoryHDRID,                                  
  numCategory,                                  
  numType,                                  
  Case when numCategory =1 then dateadd(minute,' + CONVERT(VARCHAR(20), -@ClientTimeZoneOffset) + ',dtFromDate) else  dtFromDate end as dtFromDate,                                  
  Case when numCategory =1 then dateadd(minute,' + CONVERT(VARCHAR(20), -@ClientTimeZoneOffset) + ',dtToDate) else dtToDate end as dtToDate ,                                   
  bitFromFullDay,                                  
  bitToFullDay,                                  
  convert(decimal(10,2),monAmount) as monAmount,                                  
  numOppId,
  Case When numOppId>0 then dbo.GetOppName(T.numOppId)  
  When numExpId>0 then (select TOP 1 vcAccountName from Chart_Of_Accounts where T.numExpId=numAccountid and numDomainId=T.numDomainId) else '''' end as  vcPOppName,                     
  T.numDivisionID,                            
  D.numCompanyID,                                  
  txtDesc,                                  
numContractId,                      
numCaseid,                      
numProid,
Case When numProid>0 then (select vcProjectID from ProjectsMaster where numProId=T.numProId and numDomainId=T.numDomainId) 
  When numExpId>0 then (select TOP 1 CAST(numAccountId AS VARCHAR) from Chart_Of_Accounts where T.numExpId=numAccountid and numDomainId=T.numDomainId)
else '''' end as  vcProjectID,                     
  numUserCntID,                                  
  T.numDomainID  ,                    
numOppBizDocsId  ,          
numStageId ,
Case when numStageId>0 and  numProid>0 then dbo.fn_GetProjectStageName(T.numProid,T.numStageId) else '''' end as vcProjectStageName,
bitReimburse,
Case when numCategory =2 then (SELECT ISNULL([numChartAcntId],0) FROM [General_Journal_Header] WHERE [numCategoryHDRID]=' + CONVERT(VARCHAR(20), @numCategoryHDRID) + ')
else 0 END as numExpenseAccountID
,' + CONVERT(VARCHAR(20),@TotalTime)+ ' TotalTime,' + CONVERT(VARCHAR(20),@TotalExpense)+ ' TotalExpense
,' + CONVERT(VARCHAR(20),@bitTimeBudget) + ' bitTimeBudget,' + CONVERT(VARCHAR(20),@bitExpenseBudget) + ' bitExpenseBudget
,(SELECT ISNULL(numItemCode,0) FROM item  WHERE numItemCode IN (SELECT numItemCode FROM dbo.OpportunityItems WHERE numoppitemtCode = numOppItemID )) AS [numItemCode]
,(SELECT ISNULL(numClassID,0) FROM dbo.OpportunityItems WHERE numoppitemtCode = numOppItemID ) AS [numClassID]
,(SELECT ISNULL(vcItemDesc,'''') FROM dbo.OpportunityItems WHERE numoppitemtCode = T.numOppItemID ) AS [vcItemDesc]
,CASE WHEN ISNULL(T.numApprovalComplete,0)=0 THEN ''Approved and Added'' WHEN T.numApprovalComplete=-1 THEN ''Declined'' ELSE ''Waiting For Approve'' END as ApprovalStatus
 from                                   
TimeAndExpense T                            
Left join DivisionMaster D                            
on  D.numDivisionID=T.numDivisionID                                   
 where  T.numDomainID=' + CONVERT(VARCHAR(20), @numDomainID)        
    
            IF @numCategoryHDRID <> 0
                AND @tintTEType <> 0 
                SET @sql = @sql + ' and numCategoryHDRID='
                    + CONVERT(VARCHAR(20), @numCategoryHDRID)    
            ELSE 
                BEGIN    
                    IF @tintTEType = 0 
                        SET @sql = @sql
                            + ' and tintTEType = 0 and numCategoryHDRID='
                            + CONVERT(VARCHAR(20), @numCategoryHDRID)
                            + '          
   and numUserCntID=' + CONVERT(VARCHAR(20), @numUserCntID)          
                    IF @tintTEType = 1 
                        SET @sql = @sql
                            + ' and tintTEType = 1  and numOppId = '
                            + CONVERT(VARCHAR(20), @numOppId) + '           
   and numStageId = ' + CONVERT(VARCHAR(20), @numStageId)
                            + ' and numCategory = '
                            + CONVERT(VARCHAR(20), @numCategory)          
                    IF @tintTEType = 2 
                        SET @sql = @sql
                            + ' and tintTEType = 2  and numProid = '
                            + CONVERT(VARCHAR(20), @numProid) + '           
   and numStageId = ' + CONVERT(VARCHAR(20), @numStageId)
                            + ' and numCategory = '
                            + CONVERT(VARCHAR(20), @numCategory)          
                    IF @tintTEType = 3 
                        SET @sql = @sql
                            + ' and tintTEType = 3  and numCaseid = '
                            + CONVERT(VARCHAR(20), @numCaseid) + '            
   and numStageId = ' + CONVERT(VARCHAR(20), @numStageId)
                            + ' and numCategory = '
                            + CONVERT(VARCHAR(20), @numCategory)          
                END    
            PRINT @sql          
            EXEC (@sql)          
                             
        END
GO
