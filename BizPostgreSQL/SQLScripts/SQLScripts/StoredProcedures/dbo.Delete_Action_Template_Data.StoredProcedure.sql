/****** Object:  StoredProcedure [dbo].[Delete_Action_Template_Data]    Script Date: 07/26/2008 16:14:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='delete_action_template_data')
DROP PROCEDURE delete_action_template_data
GO
CREATE PROCEDURE [dbo].[Delete_Action_Template_Data]
(
@rowID Int 
)
As
Delete From tblActionItemData Where RowID = @rowID
GO
