
GO
/****** Object:  StoredProcedure [dbo].[USP_UpdateUserMasterPayrollExpense]    Script Date: 02/28/2009 13:49:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj                      
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_updateusermasterpayrollexpense')
DROP PROCEDURE usp_updateusermasterpayrollexpense
GO
CREATE PROCEDURE [dbo].[USP_UpdateUserMasterPayrollExpense]                                     
@numUserID numeric(9),                                    
@numUserCntID as numeric(9),                              
@numDomainID as numeric(9),                              
@bitHourlyRate as bit=0,                          
@monHourlyRate as DECIMAL(20,5),                          
@bitSalary as bit=0,                  
@numDailyHours as float,                
@bitPaidLeave as bit=0,              
@fltPaidLeaveHrs as float=0,              
@fltPaidLeaveEmpHrs as float=0,              
@bitOverTime as bit=0,  
@bitOverTimeHrsDailyOrWeekly as bit=0,                        
@numLimDailHrs as float,                          
@monOverTimeRate as DECIMAL(20,5),             
@bitManagerOfOwner as bit=0,
@fltManagerOfOwner as float=0,
@bitCommOwner as bit=0,              
@fltCommOwner as float=0,              
@bitCommAssignee as bit=0,              
@fltCommAssignee as float=0,              
@bitRoleComm as bit=0,              
@bitEmpNetAccount as bit=0,              
@dtHired as datetime,              
@dtReleased as datetime,        
@vcEmployeeId as varchar(150),        
@vcEmergencyContactInfo as varchar(250),      
@dtSalaryDateTime as datetime,    
@bitPayroll as bit=0,  
@fltEmpRating as float=0,  
@dtEmpRating as datetime,  
@monNetPay as DECIMAL(20,5),  
@monAmtWithheld as DECIMAL(20,5)  
AS                                    
BEGIN        
 if @dtEmpRating ='Jan  1 1753 12:00AM' set  @dtEmpRating=null              
 UPDATE UserMaster SET                    
 numModifiedBy= @numUserCntID ,                              
 bintModifiedDate= getutcdate(),                          
 bitHourlyRate=@bitHourlyRate,                           
 monHourlyRate=@monHourlyRate,                          
 bitSalary=@bitSalary,                          
 numDailyHours=@numDailyHours,                  
 bitPaidLeave=@bitPaidLeave,              
 fltPaidLeaveHrs=@fltPaidLeaveHrs,              
 fltPaidLeaveEmpHrs=@fltPaidLeaveEmpHrs,              
 bitOverTime=@bitOverTime,
 bitOverTimeHrsDailyOrWeekly=@bitOverTimeHrsDailyOrWeekly,
 numLimDailHrs=@numLimDailHrs,                          
 monOverTimeRate=@monOverTimeRate,   
 bitManagerOfOwner=@bitManagerOfOwner,
 fltManagerOfOwner=@fltManagerOfOwner,
 bitCommOwner=@bitCommOwner,              
 fltCommOwner=@fltCommOwner,              
 bitCommAssignee=@bitCommAssignee,              
 fltCommAssignee=@fltCommAssignee,                       
 bitRoleComm=@bitRoleComm,                
 bitEmpNetAccount=@bitEmpNetAccount,              
 dtHired=@dtHired,              
 dtReleased=@dtReleased,        
 vcEmployeeId=@vcEmployeeId,        
 vcEmergencyContact=@vcEmergencyContactInfo,      
 dtSalaryDateTime=@dtSalaryDateTime,    
 bitPayroll=@bitPayroll,  
 fltEmpRating=@fltEmpRating,  
 dtEmpRating=@dtEmpRating,  
 monNetPay=@monNetPay,  
 monAmtWithheld=@monAmtWithheld                   
 WHERE numUserID=@numUserID                       
              
End
