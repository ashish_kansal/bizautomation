/****** Object:  StoredProcedure [dbo].[USP_DeleteEmailGroup]    Script Date: 07/26/2008 16:15:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_deleteemailgroup')
DROP PROCEDURE usp_deleteemailgroup
GO
CREATE PROCEDURE [dbo].[USP_DeleteEmailGroup]
@numEmailGroup as numeric(9)=0
as

delete from ProfileEGroupDTL where numEmailGroupID=@numEmailGroup
delete ProfileEmailGroup where numEmailGroupID=@numEmailGroup
GO
