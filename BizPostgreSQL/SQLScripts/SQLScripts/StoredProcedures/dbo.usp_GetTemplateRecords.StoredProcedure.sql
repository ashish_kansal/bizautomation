/****** Object:  StoredProcedure [dbo].[usp_GetTemplateRecords]    Script Date: 07/26/2008 16:18:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_gettemplaterecords')
DROP PROCEDURE usp_gettemplaterecords
GO
CREATE PROCEDURE [dbo].[usp_GetTemplateRecords]
@numTemplateID numeric
--
as
Select * from TemplateDocuments where numTemplateId = @numTemplateId
GO
