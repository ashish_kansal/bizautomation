/****** Object:  StoredProcedure [dbo].[usp_GetItemDescription]    Script Date: 07/26/2008 16:17:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getitemdescription')
DROP PROCEDURE usp_getitemdescription
GO
CREATE PROCEDURE [dbo].[usp_GetItemDescription]
	@numItemcode numeric,
	@numFlg numeric=0   
--
AS
	DECLARE @vcItemDesc varchar(250),@charItemType char(1)
	
	if @numflg =0
		BEGIN
			SELECT @vcItemDesc= txtItemDesc from Item where numItemCode=@numitemcode
			IF @vcItemDesc is not null 
			SELECT @vcItemDesc
		END
	ELSE

		BEGIN
		SELECT @charItemType= charItemType from Item where numItemCode=@numitemcode
			IF @charItemType is not null 
			SELECT @charItemType

		END
GO
