/****** Object:  StoredProcedure [dbo].[USP_DeleteSubscribers]    Script Date: 07/26/2008 16:15:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_deletesubscribers')
DROP PROCEDURE usp_deletesubscribers
GO
CREATE PROCEDURE [dbo].[USP_DeleteSubscribers]
@numSubscriberID as numeric(9)
as

	update Subscribers set bitDeleted=1 where numSubscriberID=@numSubscriberID
GO
