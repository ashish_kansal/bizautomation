/****** Object:  StoredProcedure [dbo].[usp_GetDefaultAutoRoutingRuleDetail]    Script Date: 07/26/2008 16:17:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created By: Debasish Tapan Nag                    
--Purpose: Get a details of Default Auto Routing rules for Leads from the database            
--Created Date: 08/03/2005                       
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getdefaultautoroutingruledetail')
DROP PROCEDURE usp_getdefaultautoroutingruledetail
GO
CREATE PROCEDURE [dbo].[usp_GetDefaultAutoRoutingRuleDetail]  
 @numDomainID numeric,
 @numRuleID numeric=0
AS            
--This procedure will return all the Default Routing Rules details  
BEGIN      
   SELECT r.numRoutID, r.vcRoutName, rd.numEmpId           
   FROM RoutingLeads r INNER JOIN Routingleaddetails rd ON r.numRoutID=rd.numRoutID
   where           
	r.numRoutID=@numRuleID  
END
GO
