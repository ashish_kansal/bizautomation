GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj  
-- exec USP_GetAuthorizationGroup @numDomainID=72,@tintGroupType=0
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getauthorizationgroup')
DROP PROCEDURE usp_getauthorizationgroup
GO
CREATE PROCEDURE [dbo].[USP_GetAuthorizationGroup]    
	@numDomainID NUMERIC(18,0),  
	@vcGroupTypes VARCHAR(200)   
AS    
BEGIN  
	IF LEN(ISNULL(@vcGroupTypes,'')) = 0  
	BEGIN  
		SELECT 
			numGroupID
			,vcGroupName
			,tinTGroupType
			,bitConsFlag
			,CASE 
				WHEN tinTGroupType IN (1,4,5,6) THEN 'Internal Access'   
				WHEN tinTGroupType=2 THEN 'Portal Access'   
				WHEN tinTGroupType=3 THEN 'Partner Access' 
			END AS GroupType
			,0 AS Op_flag
			,(CONVERT(VARCHAR(10),numGroupID) + '~' + CONVERT(CHAR(1),tinTGroupType)) AS numGroupID1
		FROM 
			AuthenticationGroupMaster 
		WHERE 
			numDomainID=@numDomainID 
	END  
	ELSE
	BEGIN  
		SELECT 
			numGroupID
			,vcGroupName
			,tinTGroupType
			,bitConsFlag
			,CASE 
				WHEN tinTGroupType IN (1,4,5,6) THEN 'Internal Access'   
				WHEN tinTGroupType=2 THEN 'Portal Access'   
				WHEN tinTGroupType=3 THEN 'Partner Access' 
			END AS GroupType
			,0 as Op_flag
			,CONCAT(numGroupID,'~',ISNULL(tinTGroupType,1)) AS numGroupID1
		FROM 
			AuthenticationGroupMaster 
		WHERE 
			numDomainID=@numDomainID 
			AND tintGroupType IN (SELECT Id FROM dbo.SplitIDs(@vcGroupTypes,','))
	END
END
GO
