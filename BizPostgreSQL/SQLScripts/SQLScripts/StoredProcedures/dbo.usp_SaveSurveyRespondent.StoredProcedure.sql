/****** Object:  StoredProcedure [dbo].[usp_SaveSurveyRespondent]    Script Date: 07/26/2008 16:21:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created By: Debasish Tapan Nag                                            
--Purpose: Saves the Survey Respondents Information            
--Created Date: 09/23/2005                                  
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_savesurveyrespondent')
DROP PROCEDURE usp_savesurveyrespondent
GO
CREATE PROCEDURE [dbo].[usp_SaveSurveyRespondent]           
 @numSurID Numeric(9),   
 @vcSurveyRespondentInformation as Ntext,         
 @numDomainId  Numeric(9)  
AS                                   
                      
   INSERT INTO SurveyRespondentsMaster(                      
    numSurID,            
    numSurRating,          
    dtDateofResponse, numDomainId        
    ) values (@numSurID, 0, getutcdate(), @numDomainId)                       
   SELECT SCOPE_IDENTITY()
GO
