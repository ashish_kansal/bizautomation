GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_CaseComments')
DROP PROCEDURE USP_CaseComments
GO
CREATE    Proc [dbo].[USP_CaseComments]           
@byteMode as tinyint=0,    
@numCaseId as numeric(9)=0,             
@vcHeading as varchar(1000)='',    
@txtComments as text='',    
@numContactID as numeric(9)=0,    
@numCommentID as numeric(9)=0          
as            
    
if @byteMode=0    
begin           
select numCommentID,vcHeading+' ('+convert(varchar(20),bintCreatedDate)+') - '+ dbo.fn_GetContactName(numCreatedby) as vcHeading,txtComments from CaseComments where numCaseId=@numCaseId  order by    numCommentID desc     
end    
    
if @byteMode=1    
begin    
  
if @numCommentID=0   
begin  
insert into CaseComments(numCaseID,vcHeading,txtComments,numCreatedby,bintCreatedDate)    
values(@numCaseId,@vcHeading,@txtComments,@numContactID,getutcdate())    
select @@identity   
end  
else  
begin  
update CaseComments set vcHeading=@vcHeading,txtComments=@txtComments where numCommentID=@numCommentID  
select @numCommentID  
end   
end       
    
if @byteMode=2     
begin    
delete from CaseComments where numCommentID=@numCommentID    
select 1    
end
GO
