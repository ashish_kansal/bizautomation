
GO
/****** Object:  StoredProcedure [dbo].[usp_UpdateStagePercentageDetails]    Script Date: 09/21/2010 12:00:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
----Created By Gangadhar    
    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_updatestagepercentagedetails')
DROP PROCEDURE usp_updatestagepercentagedetails
GO
CREATE PROCEDURE [dbo].[usp_UpdateStagePercentageDetails]
    @numStagePercentageId NUMERIC,
    @tintConfiguration TINYINT,
    @vcStageName VARCHAR(1000),
    @slpid NUMERIC,
    @numDomainId NUMERIC,
    @numUserCntID NUMERIC,
    @numAssignTo NUMERIC = 0,
    @vcMileStoneName VARCHAR(1000),
    @tintPercentage AS TINYINT,
    @vcDescription VARCHAR(2000),
    @numStageDetailsIds AS NUMERIC(9),
    @bitClose AS BIT,
    @numParentStageID NUMERIC,
    @intDueDays int,
	@dtStartDate as DATETIME,
	@dtEndDate as DATETIME,
	@bitTimeBudget as bit,
	@bitExpenseBudget as bit,
	@monTimeBudget as DECIMAL(20,5),
	@monExpenseBudget as DECIMAL(20,5),
	@numSetPrevStageProgress AS NUMERIC = 0,
	@bitIsDueDaysUsed AS BIT,
	@numTeamId AS NUMERIC,
	@bitRunningDynamicMode AS BIT,
	@numStageOrder AS NUMERIC(18,0)=0
AS 

IF @numSetPrevStageProgress > 0
BEGIN
	UPDATE [StagePercentageDetails] 
		SET tinProgressPercentage = 100
		WHERE   numdomainid = @numDomainid
                AND numStageDetailsId < @numSetPrevStageProgress
                AND slp_id = @slpid
				AND tinProgressPercentage < 100

END


UPDATE  [StagePercentageDetails]
SET     [numStagePercentageId] = @numStagePercentageId,
        [tintConfiguration] = @tintConfiguration,
        [vcStageName] = @vcStageName,
        [numModifiedBy] = @numUserCntID,
        [bintModifiedDate] = GETUTCDATE(),
        [numAssignTo] = (case @numAssignTo when -1 then numAssignTo else @numAssignTo end),
		[tintPercentage] = (case @numAssignTo when -1 then tintPercentage else @tintPercentage end),
		[tinProgressPercentage] = (case @numAssignTo when -1 then @tintPercentage else tinProgressPercentage end), 
        [vcMileStoneName] = @vcMileStoneName,
        [vcDescription] = @vcDescription,
        bitClose = @bitClose,
        numParentStageID = @numParentStageID,
        intDueDays = @intDueDays,
		dtStartDate = @dtStartDate,
		dtEndDate = @dtEndDate,bitTimeBudget=@bitTimeBudget,bitExpenseBudget=@bitExpenseBudget,
		monTimeBudget=@monTimeBudget,monExpenseBudget=@monExpenseBudget,bitIsDueDaysUsed=@bitIsDueDaysUsed,
		numTeamId=@numTeamId,bitRunningDynamicMode=@bitRunningDynamicMode,numStageOrder=@numStageOrder
WHERE   numStageDetailsId = @numStageDetailsIds
        AND slp_id = @slpid
        AND numDomainId = @numDomainId

if @bitClose=1
BEGIN
 update  [StagePercentageDetails] set bitClose = @bitClose,tinProgressPercentage=@tintPercentage where numParentStageID  = @numStageDetailsIds
        AND slp_id = @slpid
        AND numDomainId = @numDomainId
END 


if @numParentStageID>0
BEGIN
Declare @TotalR  numeric,@TotalP  numeric

  select @TotalR = count(*) from StagePercentageDetails where numParentStageID=@numParentStageID AND slp_id = @slpid AND numDomainId = @numDomainId
  select @TotalP = sum(tinProgressPercentage) from StagePercentageDetails where numParentStageID=@numParentStageID AND slp_id = @slpid AND numDomainId = @numDomainId
	
  if @TotalP = @TotalR*100
	Update StagePercentageDetails set tinProgressPercentage=100,bitclose=1 where numStageDetailsId=@numParentStageID AND slp_id = @slpid AND numDomainId = @numDomainId
END


DECLARE @numProjectID NUMERIC
DECLARE @numOppID NUMERIC
SELECT @numProjectID=numProjectID,@numOppID=numOppID FROM dbo.StagePercentageDetails WHERE [numStageDetailsId]=@numStageDetailsIds


--Update total progress
IF @numProjectID>0
	EXEC dbo.USP_GetProjectTotalProgress
	@numDomainID = @numDomainID, --  numeric(9, 0)
	@numProId = @numProjectID, --  numeric(9, 0)
	@tintMode = 1 --  tinyint

IF @numOppID>0
	EXEC dbo.USP_GetProjectTotalProgress
	@numDomainID = @numDomainID, --  numeric(9, 0)
	@numProId = @numOppID, --  numeric(9, 0)
	@tintMode = 0 --  tinyint
