/****** Object:  StoredProcedure [dbo].[USP_GetChartAcntDescription]    Script Date: 07/26/2008 16:16:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created by Siva  
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getchartacntdescription')
DROP PROCEDURE usp_getchartacntdescription
GO
CREATE PROCEDURE [dbo].[USP_GetChartAcntDescription]  
@numChartAcntId as numeric(9)=0,  
@numDomainID as numeric(9)=0  
As  
Begin  
	Select vcAccountName from Chart_Of_Accounts Where numAccountId=@numChartAcntId And numDomainID=@numDomainID  
End
GO
