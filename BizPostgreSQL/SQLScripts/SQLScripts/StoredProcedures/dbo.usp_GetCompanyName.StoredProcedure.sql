/****** Object:  StoredProcedure [dbo].[usp_GetCompanyName]    Script Date: 07/26/2008 16:16:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--modified by anoop jayaraj  
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getcompanyname')
DROP PROCEDURE usp_getcompanyname
GO
CREATE PROCEDURE [dbo].[usp_GetCompanyName]    
@numDivisionID numeric       
AS    
SELECT vcCompanyname FROM companyinfo C  
Join DivisionMaster D  
on D.numCompanyID=C.numCompanyID  
WHERE D.numDivisionID=@numDivisionID
GO
