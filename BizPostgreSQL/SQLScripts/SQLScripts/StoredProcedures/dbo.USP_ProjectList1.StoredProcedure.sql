GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ProjectList1')
DROP PROCEDURE USP_ProjectList1
GO
CREATE PROCEDURE [dbo].[USP_ProjectList1]
(                                                  
	@numUserCntID NUMERIC(18,0)=0,                                              
	@numDomainID NUMERIC(18,0)=0,                                              
	@tintUserRightType tinyint=0,                                                    
	@SortChar char(1)='0',                                                                                           
	@CurrentPage int,                                              
	@PageSize int,                                              
	@TotRecs int output,                                              
	@columnName as Varchar(50),                                              
	@columnSortOrder as Varchar(10),                                              
	@numDivisionID as numeric(9) ,                                             
	@bitPartner as bit=0,          
	@ClientTimeZoneOffset as int,
	@numProjectStatus as numeric(9),
	@vcRegularSearchCriteria varchar(MAX)='',
	@vcCustomSearchCriteria varchar(MAX)='' ,
	@tintDashboardReminderType TINYINT = 0
)
AS
BEGIN                                    
	BEGIN TRY       
		CREATE TABLE #tempForm 
		(tintOrder TINYINT,vcDbColumnName nvarchar(50),vcOrigDbColumnName nvarchar(50),vcFieldName nvarchar(50),vcAssociatedControlType nvarchar(50),
		vcListItemType varchar(3),numListID numeric(9),vcLookBackTableName varchar(50),bitCustomField BIT,	numFieldId NUMERIC,bitAllowSorting BIT,
		bitAllowEdit BIT,bitIsRequired BIT,bitIsEmail BIT,bitIsAlphaNumeric BIT,bitIsNumeric BIT,bitIsLengthValidation BIT,intMaxLength int,intMinLength int,
		bitFieldMessage BIT,vcFieldMessage VARCHAR(500),ListRelID numeric(9),intColumnWidth INT,bitAllowFiltering BIT,vcFieldDataType CHAR(1))
                
		-- FIRST CHECK IF GRID COLUMNS ARE CONFIGURED
		DECLARE @Nocolumns AS TINYINT = 0		             
		
		SELECT 
			@Nocolumns=isnull(sum(TotalRow),0)  
		FROM
		(            
			SELECT 
				count(*) TotalRow 
			FROM 
				View_DynamicColumns 
			WHERE 
				numFormId=13 
				AND numUserCntID=@numUserCntID 
				AND numDomainID=@numDomainID 
				AND tintPageType=1
			UNION
			SELECT 
				COUNT(*) TotalRow 
			FROM 
				View_DynamicCustomColumns 
			WHERE 
				numFormId=13 
				AND numUserCntID=@numUserCntID 
				AND numDomainID=@numDomainID 
				AND tintPageType=1
		) TotalRows
		
		-- IF GRID COLUMNS ARE NOT CONFIGURES THEN CHECK IF MASTER FORM CONFIGURATION IS CREATED BY ADMINISTRATOR
		DECLARE @IsMasterConfAvailable BIT = 0
		DECLARE @numUserGroup NUMERIC(18,0)

		IF @Nocolumns=0
		BEGIN
			SELECT @numUserGroup = numGroupID FROM UserMaster WHERE numUserDetailId = @numUserCntID 

			IF (SELECT COUNT(*) FROM BizFormWizardMasterConfiguration WHERE numDomainID = @numDomainID AND numFormID = 13 AND bitGridConfiguration = 1 AND numGroupID = @numUserGroup) > 0
			BEGIN
				SET @IsMasterConfAvailable = 1
			END
		END


		--IF MASTERCONFIGURATION IS AVAILABLE THEN LOAD IT OTHERWISE LOAD DEFAULT COLUMNS
		IF @IsMasterConfAvailable = 1
		BEGIN
			INSERT INTO DycFormConfigurationDetails 
			(
				numFormID,numFieldID,intColumnNum,intRowNum,numDomainID,numUserCntID,numRelCntType,tintPageType,bitCustom,intColumnWidth
			)
			SELECT 
				13,numFieldId,intColumnNum,intRowNum,@numDomainID,@numUserCntID,NULL,1,0,intColumnWidth 
			FROM 
				View_DynamicColumnsMasterConfig
			WHERE
				View_DynamicColumnsMasterConfig.numFormId=13 AND 
				View_DynamicColumnsMasterConfig.numDomainID=@numDomainID AND 
				View_DynamicColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
				View_DynamicColumnsMasterConfig.bitGridConfiguration = 1 AND
				ISNULL(View_DynamicColumnsMasterConfig.bitSettingField,0)=1 AND 
				ISNULL(View_DynamicColumnsMasterConfig.bitCustom,0)=0
			UNION
			SELECT 
				13,numFieldId,intColumnNum,intRowNum,@numDomainID,@numUserCntID,NULL,1,1,intColumnWidth
			FROM 
				View_DynamicCustomColumnsMasterConfig
			WHERE 
				View_DynamicCustomColumnsMasterConfig.numFormId=13 AND 
				View_DynamicCustomColumnsMasterConfig.numDomainID=@numDomainID AND 
				View_DynamicCustomColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
				View_DynamicCustomColumnsMasterConfig.bitGridConfiguration = 1 AND
				ISNULL(View_DynamicCustomColumnsMasterConfig.bitCustom,0)=1

			INSERT INTO #tempForm
			SELECT 
				(intRowNum + 1) as tintOrder, vcDbColumnName,	vcOrigDbColumnName,	ISNULL(vcCultureFieldName,vcFieldName), vcAssociatedControlType,
				vcListItemType, numListID, vcLookBackTableName,	bitCustom, numFieldId, bitAllowSorting,	bitInlineEdit, bitIsRequired,
				bitIsEmail,	bitIsAlphaNumeric, bitIsNumeric, bitIsLengthValidation,	intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage vcFieldMessage,
				ListRelID,intColumnWidth,bitAllowFiltering,vcFieldDataType 
			FROM 
				View_DynamicColumnsMasterConfig
			WHERE
				View_DynamicColumnsMasterConfig.numFormId=13 AND 
				View_DynamicColumnsMasterConfig.numDomainID=@numDomainID AND 
				View_DynamicColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
				View_DynamicColumnsMasterConfig.bitGridConfiguration = 1 AND
				ISNULL(View_DynamicColumnsMasterConfig.bitSettingField,0)=1 AND 
				ISNULL(View_DynamicColumnsMasterConfig.bitCustom,0)=0
			UNION
			SELECT 
				tintRow+1 as tintOrder,vcDbColumnName,vcDbColumnName,vcFieldName, vcAssociatedControlType,'' as vcListItemType,numListID,'',
				bitCustom,numFieldId,bitAllowSorting,bitAllowEdit,bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
				intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage,ListRelID,intColumnWidth,1,''
			FROM 
				View_DynamicCustomColumnsMasterConfig
			WHERE 
				View_DynamicCustomColumnsMasterConfig.numFormId=13 AND 
				View_DynamicCustomColumnsMasterConfig.numDomainID=@numDomainID AND 
				View_DynamicCustomColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
				View_DynamicCustomColumnsMasterConfig.bitGridConfiguration = 1 AND
				ISNULL(View_DynamicCustomColumnsMasterConfig.bitCustom,0)=1
			ORDER BY 
				tintOrder ASC  
		END
		ELSE                                        
		BEGIN
			IF @Nocolumns=0
			BEGIN
				INSERT INTO DycFormConfigurationDetails 
				(numFormID,numFieldID,intColumnNum,intRowNum,numDomainID,numUserCntID,numRelCntType,tintPageType,bitCustom,intColumnWidth)
				SELECT 
					13,numFieldId,0,Row_number() over(order by tintRow desc),@numDomainID,@numUserCntID,NULL,1,0,intColumnWidth
				FROM 
					View_DynamicDefaultColumns
				WHERE 
					numFormId=13 
					AND bitDefault=1 
					AND ISNULL(bitSettingField,0)=1 
					AND numDomainID=@numDomainID                
				ORDER BY 
					tintOrder ASC
			END
             
			INSERT INTO 
				#tempForm
			SELECT 
				tintRow+1 AS tintOrder,vcDbColumnName,vcOrigDbColumnName,ISNULL(vcCultureFieldName,vcFieldName),vcAssociatedControlType,vcListItemType,
				numListID,vcLookBackTableName,bitCustom,numFieldId,bitAllowSorting,bitAllowEdit,bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,
				bitIsLengthValidation,intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage vcFieldMessage,ListRelID,intColumnWidth,bitAllowFiltering,
				vcFieldDataType  
			FROM 
				View_DynamicColumns 
			WHERE 
				numFormId=13 
				AND numUserCntID=@numUserCntID 
				AND numDomainID=@numDomainID 
				AND tintPageType=1 
				AND ISNULL(bitSettingField,0)=1 AND ISNULL(bitCustom,0)=0
			UNION
			SELECT 
				tintRow+1 as tintOrder,vcDbColumnName,vcDbColumnName,vcFieldName,vcAssociatedControlType,'' as vcListItemType,numListID,''                                                 
				,bitCustom,numFieldId,bitAllowSorting,bitAllowEdit,bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
				intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage,ListRelID,intColumnWidth,1,'' 
			FROM 
				View_DynamicCustomColumns
			WHERE 
				numFormId=13 
				AND numUserCntID=@numUserCntID 
				AND numDomainID=@numDomainID 
				AND tintPageType=1 
				AND ISNULL(bitCustom,0)=1
			ORDER BY 
				tintOrder asc  
		END 	

		DECLARE @strColumns AS VARCHAR(MAX)
		SET @strColumns=' addCon.numContactId,Pro.vcProjectName,Div.numDivisionID,isnull(Div.numTerID,0) as numTerID,pro.numCustPrjMgr,Div.tintCRMType,pro.numProId,Pro.numRecOwner'
	
		DECLARE @tintOrder as tinyint                                                    
		DECLARE @vcFieldName as varchar(50)                                                    
		DECLARE @vcListItemType as varchar(3)                                               
		DECLARE @vcListItemType1 as varchar(1)                                                   
		DECLARE @vcAssociatedControlType varchar(20)                                                    
		DECLARE @numListID AS numeric(9)                                                    
		DECLARE @vcDbColumnName varchar(20)                        
                      
		DECLARE @vcLookBackTableName varchar(2000)                  
		DECLARE @bitCustom as bit  
		DECLARE @bitAllowEdit AS CHAR(1)                 
		DECLARE @numFieldId as numeric  
		DECLARE @bitAllowSorting AS CHAR(1) 
		DECLARE @vcColumnName AS VARCHAR(500)    
                  
		set @tintOrder=0                         
		Declare @ListRelID as numeric(9) 
		DECLARE @SearchQuery AS VARCHAR(MAX) = '' 

		SELECT TOP 1 
			@tintOrder=tintOrder+1,@vcDbColumnName=vcDbColumnName,@vcFieldName=vcFieldName,                
			@vcAssociatedControlType=vcAssociatedControlType,@vcListItemType=vcListItemType,@numListID=numListID,@vcLookBackTableName=vcLookBackTableName                                               
			,@bitCustom=bitCustomField,@numFieldId=numFieldId,@bitAllowSorting=bitAllowSorting,@bitAllowEdit=bitAllowEdit,
			@ListRelID=ListRelID
		FROM  
			#tempForm 
		ORDER BY 
			tintOrder ASC

		DECLARE @WhereCondition VARCHAR(MAX)=''

		WHILE @tintOrder > 0                                                    
		BEGIN         
			PRINT @vcDbColumnName
			PRINT @vcListItemType   
			PRINT @vcAssociatedControlType                                       
	
			IF @bitCustom = 0          
			BEGIN
				DECLARE @Prefix AS VARCHAR(5)            
		  
				IF @vcLookBackTableName = 'AdditionalContactsInformation'                  
					SET @Prefix = 'ADC.'                  
				IF @vcLookBackTableName = 'DivisionMaster'                  
					SET @Prefix = 'DM.'                  
				IF @vcLookBackTableName = 'ProjectsMaster'                  
					SET @PreFix ='Pro.'        
    
				SET @vcColumnName= @vcDbColumnName+'~'+ convert(varchar(10),@numFieldId) +'~0'

				IF @vcAssociatedControlType='SelectBox'                                                    
				BEGIN
					IF @vcListItemType='LI'                                                     
					BEGIN                                                    
					  SET @strColumns= @strColumns + ',L' + convert(varchar(3),@tintOrder)+'.vcData'+' ['+ @vcColumnName+']'                         
					  SET @WhereCondition= @WhereCondition +' left Join ListDetails L'+ convert(varchar(3),@tintOrder)+ ' on L'+convert(varchar(3),@tintOrder)+ '.numListItemID='+@vcDbColumnName
					END                                                    
					ELSE IF @vcListItemType='S'                                                     
					BEGIN                                                    
					  SET @strColumns=@strColumns + ',S' + convert(varchar(3),@tintOrder)+'.vcState'+' ['+ @vcColumnName+']'                                
					  SET @WhereCondition= @WhereCondition +' left join State S'+ convert(varchar(3),@tintOrder)+ ' on S'+convert(varchar(3),@tintOrder)+ '.numStateID='+@vcDbColumnName                                                    
					END   
					ELSE IF @vcListItemType='PP'
					BEGIN                                                    
					  SET @strColumns=@strColumns + ',ISNULL(PP.intTotalProgress,0)'+' ['+ @vcColumnName+']'
					END   
					ELSE IF @vcListItemType='T'                                                     
					BEGIN        
					  SET @strColumns=@strColumns + ',L' + convert(varchar(3),@tintOrder)+'.vcData'+' ['+ @vcColumnName+']'                                    
					  SET @WhereCondition= @WhereCondition +' left Join ListDetails L'+ convert(varchar(3),@tintOrder)+ ' on L'+convert(varchar(3),@tintOrder)+ '.numListItemID='+@vcDbColumnName                                                    
					END                   
					ELSE IF   @vcListItemType='U'                                                 
					BEGIN                     
						SET @strColumns=@strColumns+',dbo.fn_GetContactName('+@Prefix+@vcDbColumnName+') ['+ @vcColumnName+']'                                                 
					END                  
				END             
				ELSE IF @vcAssociatedControlType='DateField'                                                    
				BEGIN
					IF @vcDbColumnName='intDueDate'
					BEGIN
						set @strColumns=@strColumns+',case when convert(varchar(11),'+@Prefix+@vcDbColumnName+' )= convert(varchar(11),getdate()) then ''<b><font color=red>Today</font></b>'''      
						set @strColumns=@strColumns+'when convert(varchar(11),'+@Prefix+@vcDbColumnName+'  )= convert(varchar(11),dateadd(day,-1,getdate())) then ''<b><font color=purple>YesterDay</font></b>'''      
						set @strColumns=@strColumns+'when convert(varchar(11),'+@Prefix+@vcDbColumnName+'  )= convert(varchar(11),dateadd(day,1,getdate())) then''<b><font color=orange>Tommorow</font></b>'' ' 
						set @strColumns=@strColumns+'else dbo.FormatedDateFromDate(' +@Prefix+@vcDbColumnName+','+convert(varchar(10),@numDomainId)+') end  ['+ @vcColumnName+']'
					END
					ELSE
					BEGIN
						SET @strColumns=@strColumns+',case when convert(varchar(11),DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+','+@Prefix+@vcDbColumnName+') )= convert(varchar(11),getdate()) then ''<b><font color=red>Today</font></b>'''    
						SET @strColumns=@strColumns+'when convert(varchar(11),DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+','+@Prefix+@vcDbColumnName+') )= convert(varchar(11),dateadd(day,-1,getdate())) then ''<b><font color=purple>YesterDay</font></b>'''    
						SET @strColumns=@strColumns+'when convert(varchar(11),DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+','+@Prefix+@vcDbColumnName+') )= convert(varchar(11),dateadd(day,1,getdate())) then''<b><font color=orange>Tommorow</font></b>'' '    
						SET @strColumns=@strColumns+'else dbo.FormatedDateFromDate(DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+','+@Prefix+@vcDbColumnName+'),'+convert(varchar(10),@numDomainId)+') end  ['+ @vcColumnName+']'
					END
				END            
				ELSE IF @vcAssociatedControlType='TextBox'                                                    
				BEGIN
					SET @strColumns=@strColumns+','+ case                  
					WHEN @vcDbColumnName='numAge' then 'year(getutcdate())-year(bintDOB)'                 
					WHEN @vcDbColumnName='numDivisionID' then 'DM.numDivisionID'                 
					WHEN @vcDbColumnName='textSubject' then 'convert(varchar(max),textSubject)'                 
					ELSE @PreFix+@vcDbColumnName end+' ['+@vcColumnName+']'          
				END  
				ELSE IF  @vcAssociatedControlType='TextArea'                                              
				BEGIN
					SET @strColumns=@strColumns+', '''' ' +' ['+ @vcColumnName+']'            
				END 
				ELSE  IF @vcAssociatedControlType='CheckBox'                                                
				BEGIN
					SET @strColumns=@strColumns+', isnull('+ @vcDbColumnName +',0) ['+ @vcColumnName+']'         
				END                                               
				ELSE IF @vcAssociatedControlType='Popup' and @vcDbColumnName='numShareWith'                                                   
				BEGIN
					SET @strColumns=@strColumns+',(SELECT SUBSTRING(
								(SELECT '','' + A.vcFirstName+'' ''+A.vcLastName + CASE WHEN ISNULL(SR.numContactType,0)>0 THEN ''('' + dbo.fn_GetListItemName(SR.numContactType) + '')'' ELSE '''' END
								FROM ShareRecord SR JOIN UserMaster UM ON UM.numUserDetailId=SR.numAssignedTo 
								JOIN AdditionalContactsInformation A ON UM.numUserDetailId=A.numContactID 
								WHERE SR.numDomainID=pro.numDomainID AND SR.numModuleID=5 AND SR.numRecordID=pro.numProId
								AND UM.numDomainID=pro.numDomainID and UM.numDomainID=A.numDomainID FOR XML PATH('''')),2,200000)) ['+ @vcColumnName+']'		           
				END       
			END                                              
			ELSE IF @bitCustom = 1        
			BEGIN
				SET @vcColumnName= @vcDbColumnName +'~'+ convert(varchar(10),@numFieldId)+'~1'
    
				IF @vcAssociatedControlType = 'TextBox' OR @vcAssociatedControlType = 'TextArea'
				BEGIN
					SET @strColumns= CONCAT(@strColumns,',CFW',@tintOrder,'.Fld_Value  [',@vcColumnName,']')
					SET @WhereCondition = @WhereCondition + ' left Join CFW_FLD_Values_Pro CFW'+ CONVERT(VARCHAR(3),@tintOrder)+ ' on CFW'+ convert(varchar(3),@tintOrder)+ '.Fld_Id='+convert(varchar(10),@numFieldId) +'and CFW'+ convert(varchar(3),@tintOrder)+ '.RecId=pro.numProid   '                                                   
				END          
				ELSE IF @vcAssociatedControlType = 'Checkbox'         
				BEGIN
					SET @strColumns= @strColumns+',case when isnull(CFW'+ convert(varchar(3),@tintOrder)+'.Fld_Value,0)=0 then 0 when isnull(CFW'+ convert(varchar(3),@tintOrder)+'.Fld_Value,0)=1 then 1 end   ['+ @vcColumnName + ']'             
					SET @WhereCondition= @WhereCondition +' left Join CFW_FLD_Values_Pro CFW'+ convert(varchar(3),@tintOrder)+ ' on CFW'+ convert(varchar(3),@tintOrder)+ '.Fld_Id='+convert(varchar(10),@numFieldId) +'and CFW'+ convert(varchar(3),@tintOrder)+ '.RecId=pro.numProid   '                                                   
				END          
				ELSE IF @vcAssociatedControlType = 'DateField'         
				BEGIN
					SET @strColumns= @strColumns+',dbo.FormatedDateFromDate(CFW'+ convert(varchar(3),@tintOrder)+'.Fld_Value,'+convert(varchar(10),@numDomainId)+')  ['+ @vcColumnName + ']'             
					SET @WhereCondition= @WhereCondition +' left Join CFW_FLD_Values_Pro CFW'+ convert(varchar(3),@tintOrder)+ ' on CFW'+ convert(varchar(3),@tintOrder)+ '.Fld_Id='+convert(varchar(10),@numFieldId) +'and CFW'+ convert(varchar(3),@tintOrder)+ '.RecId=pro.numProid   '                                                   
				end          
				ELSE IF @vcAssociatedControlType = 'SelectBox'       
				BEGIN
					SET @vcDbColumnName = 'DCust'+Convert(varchar(10),@numFieldId)          
					SET @strColumns=@strColumns+',L'+ convert(varchar(3),@tintOrder)+'.vcData'+' ['+ @vcColumnName + ']'                                                    
					SET @WhereCondition= @WhereCondition +' left Join CFW_FLD_Values_Pro CFW'+ convert(varchar(3),@tintOrder)+ ' on CFW'+ convert(varchar(3),@tintOrder)+ '.Fld_Id='+convert(varchar(10),@numFieldId) +'and CFW'+ convert(varchar(3),@tintOrder)+ '.RecId=pro.numProid   '                                                   
					set @WhereCondition= @WhereCondition +' left Join ListDetails L'+ convert(varchar(3),@tintOrder)+ ' on L'+convert(varchar(3),@tintOrder)+ '.numListItemID=CFW'+ convert(varchar(3),@tintOrder)+'.Fld_Value'          
				END
				ELSE
				BEGIN
					SET @strColumns = @strColumns + CONCAT(',dbo.GetCustFldValuePro(',@numFieldId,',pro.numProID)') + ' [' + @vcColumnName + ']'
				END             
			END          
                                                  
			SELECT TOP 1 
				@tintOrder=tintOrder+1,@vcDbColumnName=vcDbColumnName,@vcFieldName=vcFieldName,                
				@vcAssociatedControlType=vcAssociatedControlType,@vcListItemType=vcListItemType,@numListID=numListID,@vcLookBackTableName=vcLookBackTableName,                                             
				@bitCustom=bitCustomField,@numFieldId=numFieldId,@bitAllowSorting=bitAllowSorting,@bitAllowEdit=bitAllowEdit,
				@ListRelID=ListRelID
			FROM 
				#tempForm 
			WHERE 
				tintOrder > @tintOrder-1 
			ORDER BY 
				tintOrder asc            
 
			IF @@rowcount=0 
				SET @tintOrder=0 
		END

		DECLARE @strShareRedordWith AS VARCHAR(MAX);
		SET @strShareRedordWith=' pro.numProId in (select SR.numRecordID FROM ShareRecord SR WHERE SR.numDomainID='+ convert(varchar(15),@numDomainID )+' AND SR.numModuleID=5 AND SR.numAssignedTo='+convert(varchar(15),@numUserCntId)+') '
 
		DECLARE @strExternalUser AS VARCHAR(MAX) = ''
		SET @strExternalUser = CONCAT(' (NOT EXISTS (SELECT numUserDetailID FROM UserMaster WHERE numDomainID=',@numDomainID,' AND numUserDetailID=',@numUserCntId,') AND EXISTS (SELECT EAD.numExtranetDtlID FROM ExtranetAccountsDtl EAD INNER JOIN ExtarnetAccounts EA ON EAD.numExtranetID=EA.numExtranetID WHERE EAD.numDomainID=',@numDomainID,' AND EAD.numContactID=',@numUserCntID,' AND EA.numDivisionID=pro.numDivisionID))')

		DECLARE @StrSql AS VARCHAR(MAX) = ''
	
		SET @StrSql = @StrSql + ' FROM ProjectsMaster pro                                                               
									 LEFT JOIN ProjectsContacts ProCont on ProCont.numProId=Pro.numProId and ProCont.bitPartner=1 and ProCont.numContactId='+convert(varchar(15),@numUserCntID)+ '                                                              
									 JOIN additionalContactsinformation addCon on addCon.numContactId=pro.numCustPrjMgr
									 JOIN DivisionMaster Div on Div.numDivisionID=addCon.numDivisionID    
									 JOIN CompanyInfo cmp on cmp.numCompanyID=div.numCompanyID    
									  left join ProjectProgress PP on PP.numProID=pro.numProID ' + ISNULL(@WhereCondition,'')
								

		IF @columnName like 'CFW.Cust%'           
		BEGIN
			SET @StrSql = @StrSql + ' left Join CFW_FLD_Values_Pro CFW on CFW.RecId=pro.numProid  and CFW.fld_id= '+replace(@columnName,'CFW.Cust','')
			SET @columnName='CFW.Fld_Value'                                                            
		END                               
		IF @columnName like 'DCust%'          
		BEGIN                                  
			SET @StrSql = @StrSql + ' left Join CFW_FLD_Values_Pro CFW on CFW.RecId=pro.numProid and CFW.fld_id= '+replace(@columnName,'DCust','')                                                                                             
			SET @StrSql = @StrSql + ' left Join ListDetails LstCF on LstCF.numListItemID=CFW.Fld_Value'
			SET @columnName='LstCF.vcData'
		END

		IF @bitPartner=1 
			SET @strSql=@strSql+' left join ProjectsContacts ProCont on ProCont.numProId=Pro.numProId and ProCont.bitPartner=1 and ProCont.numContactId='+convert(varchar(15),@numUserCntID)+ ''   


		 -------Change Row Color-------
		DECLARE @vcCSOrigDbCOlumnName AS VARCHAR(50),@vcCSLookBackTableName AS VARCHAR(50),@vcCSAssociatedControlType AS VARCHAR(50)
		SET @vcCSOrigDbCOlumnName=''
		SET @vcCSLookBackTableName=''

		Create table #tempColorScheme(vcOrigDbCOlumnName varchar(50),vcLookBackTableName varchar(50),vcFieldValue varchar(50),vcFieldValue1 varchar(50),vcColorScheme varchar(50),vcAssociatedControlType varchar(50))

		insert into #tempColorScheme select DFM.vcOrigDbCOlumnName,DFM.vcLookBackTableName,DFCS.vcFieldValue,DFCS.vcFieldValue1,DFCS.vcColorScheme,DFFM.vcAssociatedControlType 
		from DycFieldColorScheme DFCS join DycFormField_Mapping DFFM on DFCS.numFieldID=DFFM.numFieldID
		join DycFieldMaster DFM on DFM.numModuleID=DFFM.numModuleID and DFM.numFieldID=DFFM.numFieldID
		where DFCS.numDomainID=@numDomainID and DFFM.numFormID=13 AND DFCS.numFormID=13 and isnull(DFFM.bitAllowGridColor,0)=1

		IF(SELECT COUNT(*) FROM #tempColorScheme)>0
		BEGIN
		   select top 1 @vcCSOrigDbCOlumnName=vcOrigDbCOlumnName,@vcCSLookBackTableName=vcLookBackTableName,@vcCSAssociatedControlType=vcAssociatedControlType from #tempColorScheme
		END   
		----------------------------             
    
		IF len(@vcCSOrigDbCOlumnName)>0 and len(@vcCSLookBackTableName)>0
		BEGIN
			SET @strColumns=@strColumns+',tCS.vcColorScheme'
		END
                                                                            
		IF len(@vcCSOrigDbCOlumnName)>0 and len(@vcCSLookBackTableName)>0
		BEGIN
			 if @vcCSLookBackTableName = 'AdditionalContactsInformation'                  
				set @Prefix = 'ADC.'                  
			 if @vcCSLookBackTableName = 'DivisionMaster'                  
				set @Prefix = 'DM.'
			 if @vcCSLookBackTableName = 'CompanyInfo'                  
				set @Prefix = 'CMP.' 
			 if @vcCSLookBackTableName = 'ProjectsMaster'                  
				set @Prefix = 'pro.'   
 
			IF @vcCSAssociatedControlType='DateField'
			BEGIN
					set @StrSql=@StrSql+' left join #tempColorScheme tCS on CONVERT(VARCHAR(10),' + @Prefix + @vcCSOrigDbCOlumnName + ',111) >= CONVERT(VARCHAR(10),DateAdd(day,cast(tCS.vcFieldValue as int),GETUTCDATE()),111)
					 and CONVERT(VARCHAR(10),' + @Prefix + @vcCSOrigDbCOlumnName + ',111) <= CONVERT(VARCHAR(10),DateAdd(day,cast(tCS.vcFieldValue1 as int),GETUTCDATE()),111)'
			END
			ELSE IF @vcCSAssociatedControlType='SelectBox' OR @vcCSAssociatedControlType='ListBox' OR @vcCSAssociatedControlType='CheckBox'
			BEGIN
				set @StrSql=@StrSql+' left join #tempColorScheme tCS on tCS.vcFieldValue=' + @Prefix + @vcCSOrigDbCOlumnName
			END
		END

		SET @strSql=@strSql+ CONCAT(' WHERE tintProStatus=0 AND Div.numDomainID= ',@numDomainID)
                                
		SET @strSql=@strSql+ ' AND (ISNULL(pro.numProjectStatus,0) = '+ convert(varchar(10),@numProjectStatus) + ' OR (' + CONVERT(VARCHAR(10),@numProjectStatus) +'=0 and ISNULL(pro.numProjectStatus,0) NOT IN (27492,27493)))'
                                       
		if @numDivisionID <>0 set @strSql=@strSql + ' And div.numDivisionID =' + convert(varchar(15),@numDivisionID)                                               
		if @SortChar<>'0' set @strSql=@strSql + ' And Pro.vcProjectName like '''+@SortChar+'%'''                                               
		if @tintUserRightType=1 
			SET @strSql = @strSql + ' AND (Pro.numRecOwner = ' +convert(varchar(15),@numUserCntID) 
									+' or Pro.numAssignedTo = '+convert(varchar(15),@numUserCntID) 
									+' or Pro.numProId in (select distinct(numProID) from ProjectsStageDetails where numAssignTo ='+ convert(varchar(15),@numUserCntID)+')' 
									+ CASE WHEN @bitPartner=1 THEN ' or ( ProCont.bitPartner=1 and ProCont.numContactId='+convert(varchar(15),@numUserCntID)+ ')' else '' end  
									+ ' or ' + @strShareRedordWith 
									+ ' or ' + @strExternalUser + ')'
		else if @tintUserRightType=2 set @strSql=@strSql + ' and  (Div.numTerID in (select numTerritoryID from  UserTerritory where numUserCntID= '+convert(varchar(15),@numUserCntID)+' ) or Pro.numAssignedTo= '+ convert(varchar(15),@numUserCntID
		)  +' or  Pro.numProId in (select distinct(numProID) from ProjectsStageDetails                     
		where numAssignTo ='+ convert(varchar(15),@numUserCntID)+')' + ' or ' + @strShareRedordWith +')'                                             
                                                            
		IF @vcRegularSearchCriteria<>'' set @strSql=@strSql+' and ' + @vcRegularSearchCriteria 
		IF @vcCustomSearchCriteria<>'' set @strSql=@strSql+' AND ' + @vcCustomSearchCriteria

		IF LEN(@SearchQuery) > 0
		BEGIN
			SET @StrSql = @StrSql + ' AND (' + @SearchQuery + ') '
		END	

		IF ISNULL(@tintDashboardReminderType,0) = 5
		BEGIN
			SET @strSql = CONCAT(@strSql,' AND Pro.numProId IN (','SELECT DISTINCT
																		ProjectsMaster.numProId 
																	FROM 
																		StagePercentageDetails SPD 
																	INNER JOIN 
																		ProjectsMaster 
																	ON 
																		SPD.numProjectID=ProjectsMaster.numProId 
																	WHERE 
																		SPD.numDomainId=',@numDOmainID ,'
																		AND ProjectsMaster.numDomainId=',@numDOmainID,'
																		AND tinProgressPercentage <> 100',') ')

		END


		DECLARE  @firstRec  AS INTEGER=0
		DECLARE  @lastRec  AS INTEGER=0
		SET @firstRec = (@CurrentPage - 1) * @PageSize
		SET @lastRec = (@CurrentPage * @PageSize + 1)
 
		-- EXECUTE FINAL QUERY
		DECLARE @strFinal AS NVARCHAR(MAX)
		SET @strFinal = CONCAT('SELECT ROW_NUMBER() OVER(ORDER BY ',@columnName,' ',@columnSortOrder,') ID, ',@strColumns,' INTO #tempTable',@StrSql,'; SELECT @TotalRecords=COUNT(*) FROM #tempTable; SELECT * FROM #tempTable WHERE ID > ',@firstRec,' and ID <',@lastRec,'; DROP TABLE #tempTable;')
	
		PRINT @strFinal
		EXEC sp_executesql @strFinal, N'@TotalRecords INT OUT', @TotRecs OUT 

		SELECT * FROM #tempForm

		DROP TABLE #tempForm

		DROP TABLE #tempColorScheme
	END TRY
	BEGIN CATCH
		IF @@TRANCOUNT > 0
			ROLLBACK TRANSACTION;

		DECLARE @ErrorNumber INT = ERROR_NUMBER();
		DECLARE @ErrorLine INT = ERROR_LINE();
		DECLARE @ErrorMessage NVARCHAR(4000) = ERROR_MESSAGE();
		DECLARE @ErrorSeverity INT = ERROR_SEVERITY();
		DECLARE @ErrorState INT = ERROR_STATE();

		PRINT 'Actual error number: ' + CAST(@ErrorNumber AS VARCHAR(10));
		PRINT 'Actual line number: ' + CAST(@ErrorLine AS VARCHAR(10));

		RAISERROR(@ErrorMessage, @ErrorSeverity, @ErrorState);
	END CATCH
END
GO 