/****** Object:  StoredProcedure [dbo].[USP_SaveImportConfg]    Script Date: 07/26/2008 16:21:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'usp_saveimportconfg' ) 
    DROP PROCEDURE usp_saveimportconfg
GO
CREATE PROCEDURE [dbo].[USP_SaveImportConfg]
    @numDomainID as numeric,
    @FormId AS NUMERIC,
    @numUserCntId AS NUMERIC,
    @StrXml as text = '',
    @tintPageType AS NUMERIC
AS 
    DELETE  FROM [DycFormConfigurationDetails]
    WHERE   numDomainID = @numDomainID
            AND [numFormId] = @FormId
            --AND [numUserCntId] = @numUserCntId
            AND tintPageType = @tintPageType
            AND ISNULL(numRelCntType,0) = 0
    
    
    DECLARE @hDoc1 int                                      
    EXEC sp_xml_preparedocument @hDoc1 OUTPUT, @strXml                                      

    INSERT  INTO DycFormConfigurationDetails
            (
              numFormId,
              numFieldId,
              intRowNum,
              intColumnNum,
              numUserCntId,
              numDomainId,
              bitCustom,
              tintPageType,
              numRelCntType
            )
            SELECT  @FormId,
                    X.numFormFieldID,
                    X.intRowNum,
                    X.intColumnNum,
                    NULL,--@numUserCntId,
                    @numDomainID,
                    X.bitCustom,
                    @tintPageType,
                    0
            FROM    ( SELECT    *
                      FROM      OPENXML (@hDoc1, '/NewDataSet/Table',2) WITH ( numFormFieldID numeric(9), intRowNum TINYINT, intColumnNum INT, bitCustom bit )
                    ) X                                     
                                      
                                      
    EXEC sp_xml_removedocument @hDoc1
GO
        
--delete RecImportConfg where numRelationShip=@numRelation and numDomainId= @numDomain  and  ImportType=@ImportType 
--      
--declare @hDoc  int        
--            
--EXEC sp_xml_preparedocument @hDoc OUTPUT, @StrXml        
--            
-- insert into RecImportConfg            
--  (numFormFieldId,numDomainId,bitCustomFld,numRelationShip,intColumn,cCtype,ImportType)            
--             
-- select *,@ImportType  from(SELECT * FROM OPENXML (@hDoc,'/NewDataSet/Table',2)            
-- WITH  (            
--  numFormFieldId numeric(9),            
--  numDomainId numeric,    
--  bitCustomFld bit,  
--numRelationShip numeric,  
--intColumn numeric,  
--cCtype char))X            
--            
--            
--            
-- EXEC sp_xml_removedocument @hDoc
--GO
