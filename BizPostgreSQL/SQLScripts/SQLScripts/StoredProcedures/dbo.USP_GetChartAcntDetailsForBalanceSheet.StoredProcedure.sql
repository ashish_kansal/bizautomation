set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go

-- Created by Siva    

-- [USP_GetChartAcntDetailsForBalanceSheet] 72,'2008-09-28 17:09:54.263','2009-09-28 17:09:54.263'
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getchartacntdetailsforbalancesheet')
DROP PROCEDURE usp_getchartacntdetailsforbalancesheet
GO
CREATE PROCEDURE [dbo].[USP_GetChartAcntDetailsForBalanceSheet]                                 
@numDomainId as numeric(9),                                                  
@dtFromDate as datetime,                                                
@dtToDate as DATETIME,
@ClientTimeZoneOffset INT, --Added by Chintan to enable calculation of date according to client machine                                                   
@numFRID AS NUMERIC=0,
@numAccountClass AS NUMERIC(9)=0

--@tintByteMode as tinyint                                                            
As                                                                
Begin                                                                
DECLARE @CURRENTPL DECIMAL(20,5) ;
DECLARE @PLCHARTID NUMERIC(8);
DECLARE @PLOPENING DECIMAL(20,5);
--DECLARE @numFinYear INT;
--DECLARE @dtFinYearFrom datetime;
DECLARE @TotalIncome DECIMAL(20,5);
DECLARE @TotalExpense DECIMAL(20,5);
DECLARE @TotalCOGS DECIMAL(20,5);


select * into #VIEW_JOURNALBS from VIEW_JOURNALBS where numdomainid=@numDomainId AND (numAccountClass=@numAccountClass OR @numAccountClass=0) ; 
SELECT * INTO #view_journal FROM view_journal WHERE numDomainID=@numDomainId AND (numAccountClass=@numAccountClass OR @numAccountClass=0) ;

--Select Financial Year and From Date
--SELECT @numFinYear=numFinYearId,@dtFinYearFrom=dtPeriodFrom FROM FINANCIALYEAR WHERE dtPeriodFrom <= @dtFromDate AND  
--dtPeriodTo >=@dtFromDate AND numDomainId=@numDomainId

--SELECT @dtFromDate=MIN(datEntry_Date) FROM dbo.VIEW_JOURNAL WHERE numDomainID=@numDomainId
--Reflact timezone difference in From and To date
--SET @dtFromDate = DateAdd(minute, @ClientTimeZoneOffset, @dtFromDate);

  /*Comment by chintan
        Reason: Standard US was facing balance not matching for undeposited func(check video for more info)
        We do not need timezone settings since all journal entry date does not store time. we only store date. 
        */
SET @dtToDate = DateAdd(minute, @ClientTimeZoneOffset, @dtToDate);
PRINT '@dtToDate :' 
PRINT @dtToDate
--PRINT @numFinYear
--PRINT @dtFinYearFrom
PRINT @dtFromDate
--RETURN
--select * from VIEW_JOURNALPL where numDomainid=72

--------------------------------------------------------
-- GETTING P&L VALUE
SET @CURRENTPL =0;	
SET @PLOPENING=0;

--SELECT @CURRENTPL = /*ISNULL(SUM(Opening),0)+*/
--+
--ISNULL(sum(Credit),0)- ISNULL(sum(Debit),0) FROM
--VIEW_DAILY_INCOME_EXPENSE P WHERE 
--numDomainID=@numDomainId AND datEntry_Date between @dtFromDate and @dtToDate ;


SELECT @TotalIncome = ISNULL(sum(Credit),0)-ISNULL(sum(Debit),0) FROM #view_journal VJ WHERE numDomainID=@numDomainID AND vcAccountCode  LIKE '0103%' AND datEntry_Date between @dtFromDate and @dtToDate ;
SELECT @TotalExpense = ISNULL(sum(Credit),0)-ISNULL(sum(Debit),0) FROM #view_journal VJ WHERE numDomainID=@numDomainID AND vcAccountCode  LIKE '0104%' AND datEntry_Date between @dtFromDate and @dtToDate ;
SELECT @TotalCOGS =  ISNULL(sum(Credit),0)-ISNULL(sum(Debit),0) FROM #view_journal VJ WHERE numDomainID=@numDomainID AND vcAccountCode  LIKE '0106%' AND datEntry_Date between @dtFromDate and @dtToDate ;
PRINT 'TotalIncome=									' + CAST(@TotalIncome AS VARCHAR(20))
PRINT 'TotalExpense=									'+ CAST(@TotalExpense AS VARCHAR(20))
PRINT 'TotalCOGS=										'+ CAST(@TotalCOGS AS VARCHAR(20))
SET @CURRENTPL = (@TotalIncome + @TotalExpense + @TotalCOGS)
PRINT 'Current Profit/Loss = (Income - expense - cogs)= ' + CAST( @CURRENTPL AS VARCHAR(20))



PRINT @CURRENTPL


SELECT @PLCHARTID = numAccountId FROM dbo.Chart_Of_Accounts WHERE numDomainId=@numDomainID AND bitProfitLoss=1
PRINT 'PL Account ID=' +CAST( @PLCHARTID AS VARCHAR(20))
SELECT @PLOPENING = ISNULL(sum(Credit),0)-ISNULL(sum(Debit),0) FROM #view_journal VJ WHERE numDomainID=@numDomainID 
/* AND numAccountId=@PLCHARTID*/
AND ( vcAccountCode  LIKE '0103%' or  vcAccountCode  LIKE '0104%' or  vcAccountCode  LIKE '0106%')
AND datEntry_Date <=  DATEADD(Minute,-1,@dtFromDate);

SELECT @PLOPENING = ISNULL(@PLOPENING,0) + ISNULL(sum(Credit),0)-ISNULL(sum(Debit),0) FROM #view_journal VJ WHERE numDomainID=@numDomainID
AND numAccountId=@PLCHARTID /*AND datEntry_Date <=  DATEADD(Minute,-1,@dtFromDate);*/


PRINT 'PL Account Opening=' +CAST( @PLOPENING AS VARCHAR(20))
SET @CURRENTPL = @PLOPENING + @CURRENTPL
PRINT 'PL = ' + CAST( @CURRENTPL AS VARCHAR(20))






CREATE TABLE #PLSummary (numAccountId numeric(9),vcAccountName varchar(250),
numParntAcntTypeID numeric(9),vcAccountDescription varchar(250),
vcAccountCode varchar(50) COLLATE Database_Default,Opening DECIMAL(20,5),Debit DECIMAL(20,5),Credit DECIMAL(20,5),bitIsSubAccount Bit);

INSERT INTO  #PLSummary
SELECT COA.numAccountId,vcAccountName,numParntAcntTypeID,vcAccountDescription,vcAccountCode,


/*isnull((SELECT SUM(isnull(monOpening,0)) from CHARTACCOUNTOPENING CAO WHERE
numFinYearId=@numFinYear and numDomainID=@numDomainId 
AND CAO.numAccountId IN (SELECT numAccountId FROM dbo.Chart_Of_Accounts WHERE numDomainId = @numDomainId AND vcAccountCode like COA.vcAccountCode + '%' )  ),0) 

+*/

/*ISNULL((SELECT sum(ISNULL(Debit,0)-ISNULL(Credit,0)) FROM #VIEW_JOURNALBS VJ
WHERE VJ.numDomainId=@numDomainId AND
	VJ.COAvcAccountCode like COA.vcAccountCode + '%' /*VJ.numAccountId=COA.numAccountId*/ AND
	datEntry_Date <= DATEADD(Minute,-1,@dtFromDate)),0)*/ 0 AS OPENING,

ISNULL((SELECT sum(ISNULL(Debit,0)) FROM #VIEW_JOURNALBS VJ
WHERE VJ.numDomainId=@numDomainId AND
	VJ.COAvcAccountCode like COA.vcAccountCode + '%' /*VJ.numAccountId=COA.numAccountId*/ AND
	datEntry_Date <= @dtToDate ),0) as DEBIT,

ISNULL((SELECT sum(ISNULL(Credit,0)) FROM #VIEW_JOURNALBS VJ
WHERE VJ.numDomainId=@numDomainId AND
	VJ.COAvcAccountCode like COA.vcAccountCode + '%' /*VJ.numAccountId=COA.numAccountId*/ AND
	datEntry_Date <= @dtToDate ),0) as CREDIT
,ISNULL(COA.bitIsSubAccount,0)
FROM Chart_of_Accounts COA
WHERE COA.numDomainId=@numDomainId  AND COA.bitActive = 1  AND
		ISNULL(bitProfitLoss,0)=0 AND
      (COA.vcAccountCode LIKE '0101%' OR
       COA.vcAccountCode LIKE '0102%' OR
       COA.vcAccountCode LIKE '0105%') 

UNION


/*(* -1) added by chintan to fix bug 2148 #3  */
SELECT @PLCHARTID,vcAccountName,numParntAcntTypeID,vcAccountDescription,
vcAccountCode,
@CURRENTPL,0,0,ISNULL(COA.bitIsSubAccount,0)
 FROM Chart_of_Accounts COA WHERE numDomainID=@numDomainId AND COA.bitActive = 1 and
bitProfitLoss=1 AND COA.numAccountId=@PLCHARTID;


/*Added by chintan bugid 962 #3, For presentation do follwoing Show Credit amount in Posite and Debit Amount in Negative (only for accounts under quaity except PL account)*/
UPDATE #PLSummary SET 
Opening =Opening * (-1),--CASE WHEN Opening <0 THEN Opening * (-1) ELSE Opening END ,
Debit = Debit * (-1),--CASE WHEN Debit >0 THEN Debit * (-1) ELSE Debit END,
Credit = Credit * (-1)--CASE WHEN Credit <0 THEN Credit * (-1) ELSE Credit END 
WHERE vcAccountCode LIKE '0105%' AND numAccountId <> @PLCHARTID;



CREATE TABLE #PLOutPut (numAccountId numeric(9),vcAccountName varchar(250),
numParntAcntTypeID numeric(9),vcAccountDescription varchar(250),
vcAccountCode varchar(50) COLLATE Database_Default,Opening DECIMAL(20,5),Debit DECIMAL(20,5),Credit DECIMAL(20,5));
--insert account types and balances
INSERT INTO #PLOutPut
SELECT ATD.numAccountTypeID,ATD.vcAccountType,ATD.numParentID, '',ATD.vcAccountCode,
 ISNULL(SUM(ISNULL(Opening,0)),0) as Opening,
ISNUlL(Sum(ISNULL(Debit,0)),0) as Debit,ISNULL(Sum(ISNULL(Credit,0)),0) as Credit
FROM 
 AccountTypeDetail ATD RIGHT OUTER JOIN 
#PLSummary PL ON
PL.vcAccountCode LIKE ATD.vcAccountCode + '%'
AND ATD.numDomainId=@numDomainId AND
(ATD.vcAccountCode LIKE '0101%' OR
       ATD.vcAccountCode LIKE '0102%' OR
       ATD.vcAccountCode LIKE '0105%')
WHERE PL.bitIsSubAccount=0
GROUP BY 
ATD.numAccountTypeID,ATD.vcAccountCode,ATD.vcAccountType,ATD.numParentID;

ALTER TABLE #PLSummary
DROP COLUMN bitIsSubAccount

CREATE TABLE #PLShow (numAccountId numeric(9),vcAccountName varchar(250),
numParntAcntTypeID numeric(9),vcAccountDescription varchar(250),
vcAccountCode varchar(50),Opening DECIMAL(20,5),Debit DECIMAL(20,5),Credit DECIMAL(20,5),
Balance DECIMAL(20,5),AccountCode1  varchar(100),vcAccountName1 varchar(250),[Type] INT);





INSERT INTO #PLShow(#PLShow.numAccountId,
					#PLShow.vcAccountName,
					#PLShow.numParntAcntTypeID,
					#PLShow.vcAccountDescription,
					#PLShow.vcAccountCode,
					#PLShow.Opening,
					#PLShow.Debit,
					#PLShow.Credit,
					#PLShow.Balance,
					#PLShow.AccountCode1,
					#PLShow.vcAccountName1,
					#PLShow.[Type])	

 
SELECT numAccountId,
	   vcAccountName,
	   numParntAcntTypeID,
	   vcAccountDescription,
	   vcAccountCode,
	   Opening,
	   Debit,
	   Credit,(Opening * case substring(P.[vcAccountCode],1,4) when '0102' then (-1) else 1 end)
+(Debit * case substring(P.[vcAccountCode],1,4) when '0102' then (-1) else 1 end )
-(Credit * case substring(P.[vcAccountCode],1,4) when '0102' then (-1) else 1 end) as Balance,
CASE 
 WHEN LEN(P.[vcAccountCode])>4 THEN REPLICATE('&nbsp;', LEN(P.[vcAccountCode])-4) + P.[vcAccountCode] 
 ELSE  P.[vcAccountCode]
 END AS AccountCode1,
 CASE 
 WHEN LEN(P.[vcAccountCode])>4 THEN REPLICATE('&nbsp;', LEN(P.[vcAccountCode])-4) + P.[vcAccountName]
 ELSE P.[vcAccountName]
 END AS vcAccountName1, 1 AS Type
 FROM #PLSummary P

UNION

SELECT numAccountId,
	   vcAccountName,
	   numParntAcntTypeID,
	   vcAccountDescription,
	   vcAccountCode,
	   Opening,
	   Debit,
	   Credit,(Opening  * case substring(O.[vcAccountCode],1,4) when '0102' then (-1) else 1 end)+
(Debit * case substring(O.[vcAccountCode],1,4) when '0102' then (-1) else 1 end)-
(Credit * case substring(O.[vcAccountCode],1,4) when '0102' then (-1) else 1 end) as Balance,

CASE 
 WHEN LEN(O.[vcAccountCode])>4 THEN REPLICATE('&nbsp;', LEN(O.[vcAccountCode])-4) +O.[vcAccountCode] 
 ELSE  O.[vcAccountCode]
 END AS AccountCode1,
 CASE 
 WHEN LEN(O.[vcAccountCode])>4 THEN REPLICATE('&nbsp;', LEN(O.[vcAccountCode])-4) + O.[vcAccountName]
 ELSE O.[vcAccountName]
 END AS vcAccountName1, 2 as Type
 FROM #PLOutPut O;

UPDATE #PLShow SET [TYPE]=3 WHERE numAccountId=@PLCHARTID;




select  A.numAccountId ,
        A.vcAccountName ,
        A.numParntAcntTypeID ,
        A.vcAccountDescription ,
        A.vcAccountCode ,
        A.Opening ,
        A.Debit ,
        A.Credit ,
        A.Balance ,
        A.AccountCode1 ,
        CASE WHEN LEN(A.[vcAccountCode]) > 4
                     THEN REPLICATE('&nbsp;',LEN(A.[vcAccountCode]) - 4 )+ A.[vcAccountName]
                     ELSE A.[vcAccountName]
                END AS vcAccountName1, A.vcAccountCode ,
        A.Type	from #PLShow A ORDER BY A.vcAccountCode;

DROP TABLE #PLOutPut;
DROP TABLE #PLSummary;
DROP TABLE #PLShow;
DROP TABLE #VIEW_JOURNALBS;
 End

