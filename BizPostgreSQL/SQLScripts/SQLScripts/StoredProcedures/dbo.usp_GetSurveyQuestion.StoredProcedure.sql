/****** Object:  StoredProcedure [dbo].[usp_GetSurveyQuestion]    Script Date: 07/26/2008 16:18:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getsurveyquestion')
DROP PROCEDURE usp_getsurveyquestion
GO
CREATE PROCEDURE [dbo].[usp_GetSurveyQuestion]
@numDomainId numeric(9),
 @SurveyId Numeric              
AS                
--This procedure will return all survey questions              
BEGIN                
 SELECT convert(varchar(10), numQID)+'~'+convert(varchar(10), tIntAnsType) numQID, vcQuestion           
  FROM SurveyQuestionMaster                
  WHERE numSurId = @SurveyId   
end
GO
