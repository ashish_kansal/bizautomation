/****** Object:  StoredProcedure [dbo].[usp_GetMasterListDetails]    Script Date: 07/26/2008 16:17:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getmasterlistdetails')
DROP PROCEDURE usp_getmasterlistdetails
GO
CREATE PROCEDURE [dbo].[usp_GetMasterListDetails]  
 @numListID NUMERIC(9) = 0,  
 @vcItemType CHAR(1) = 'L',  
 @numDomainID NUMERIC(9) = 1     
--  
AS  
 IF @vcItemType = 'L'  
  BEGIN  
   SELECT vcData AS vcItemName, numListItemID AS numItemID, 'L' As vcItemType, 
    constFlag As flagConst FROM ListDetails WHERE numListID = @numListID AND( numDomainID = @numDomainID or constFlag=1) AND vcData<>'0'  
  END  
 IF @vcItemType = 'T'  
  BEGIN  
   SELECT vcTerName AS vcItemName, numTerID AS numItemID, 'T' As vcItemType, 
   0 As flagConst FROM TerritoryMaster WHERE numDomainID = @numDomainID  
  END
GO
