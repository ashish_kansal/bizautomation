/****** Object:  StoredProcedure [dbo].[usp_InsertUniversalMaster]    Script Date: 07/26/2008 16:19:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_insertuniversalmaster')
DROP PROCEDURE usp_insertuniversalmaster
GO
CREATE PROCEDURE [dbo].[usp_InsertUniversalMaster]

	--Input Parameter List
	@numDivID numeric(9)=0,
	@numUnisprtId numeric(9)=0
	--
AS

BEGIN
	--Insertion into Table  
	INSERT INTO UniversalSupportKeyMaster(numDivisionId)
	VALUES(@numDivID)
	
	END
GO
