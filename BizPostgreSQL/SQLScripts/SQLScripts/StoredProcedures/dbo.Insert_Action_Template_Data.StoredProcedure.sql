/****** Object:  StoredProcedure [dbo].[Insert_Action_Template_Data]    Script Date: 07/26/2008 16:14:30 ******/
Go
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='insert_action_template_data')
DROP PROCEDURE insert_action_template_data
GO
CREATE PROCEDURE [dbo].[Insert_Action_Template_Data]    
(    
@templateName nvarchar(100) ,    
@dueDays Int ,    
@status Int ,    
@type nvarchar(25) ,     
@activity Int ,    
@comments nvarchar(2000),  
@bitSendEmailTemplate as bit,  
@numEmailTemplate as numeric(9),  
@tintHours as tinyint=0,  
@bitAlert as bit ,
@numUserCntID as numeric(9)   ,
@numDomainID as numeric(9),
@numTaskType as numeric(9),
@bitRemind AS TINYINT,
@numRemindBeforeMinutes AS INT        
)    
As    
    
If Not Exists ( Select 1 From tblActionItemData Where RTrim(TemplateName) = RTrim(@templateName)  )    
Begin    
	Insert Into tblActionItemData
	(
		TemplateName,DueDays,Priority,Type,Activity,Comments,bitSendEmailTemplate,numEmailTemplate,
		tintHours,bitAlert,	numDomainID,numCreatedBy,dtCreatedBy,numTaskType,bitRemind,numRemindBeforeMinutes
	)
	Values
	( 
		RTrim(@templateName),@dueDays,@status,@type,@activity,@comments,@bitSendEmailTemplate,@numEmailTemplate,
		@tintHours,@bitAlert,@numDomainID,@numUserCntID,getutcdate(),@numTaskType,@bitRemind,@numRemindBeforeMinutes
	)    
End
GO
