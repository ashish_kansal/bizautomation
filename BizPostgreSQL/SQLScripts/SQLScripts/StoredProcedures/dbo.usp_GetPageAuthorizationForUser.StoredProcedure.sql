/****** Object:  StoredProcedure [dbo].[usp_GetPageAuthorizationForUser]    Script Date: 07/26/2008 16:18:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getpageauthorizationforuser')
DROP PROCEDURE usp_getpageauthorizationforuser
GO
CREATE PROCEDURE [dbo].[usp_GetPageAuthorizationForUser]  
 @numUserID NUMERIC(9) = 0,  
 @vcFileName VARCHAR(50),  
 @numModuleId numeric(9)=0     --Added the module Id for specific calling of the page  
--  
AS  
 DECLARE @numPageID NUMERIC(9)  
 IF @numModuleId=0   
  BEGIN  
   PRINT @vcFileName  
   PRINT @numUserID  
   SELECT     G.numPageID, G.numModuleID, MAX(G.intViewAllowed) AS intViewAllowed,   
                         MAX(G.intAddAllowed) AS intAddAllowed, MAX(G.intUpdateAllowed) AS intUpdateAllowed,   
                         MAX(G.intDeleteAllowed) AS intDeleteAllowed, MAX(G.intExportAllowed) AS intExportAllowed,   
                         MAX(G.intPrintAllowed) AS intPrintAllowed  
   FROM         GroupAuthorization G INNER JOIN  
                         PageMaster ON G.numPageID = PageMaster.numPageID AND   
                         G.numModuleID = PageMaster.numModuleID INNER JOIN  
                         UserGroups ON G.numGroupID = UserGroups.numGroupID  
   WHERE     (dbo.UserGroups.numUserID = @numUserID) AND (dbo.PageMaster.vcFileName = @vcFileName)  
   GROUP BY G.numPageID, G.numModuleID  
  END  
 ELSE  
  BEGIN  
   PRINT @vcFileName  
   PRINT @numUserID  
   SELECT     G.numPageID, G.numModuleID, MAX(G.intViewAllowed) AS intViewAllowed,   
                         MAX(G.intAddAllowed) AS intAddAllowed, MAX(G.intUpdateAllowed) AS intUpdateAllowed,   
                         MAX(G.intDeleteAllowed) AS intDeleteAllowed, MAX(G.intExportAllowed) AS intExportAllowed,   
                         MAX(G.intPrintAllowed) AS intPrintAllowed  
   FROM   GroupAuthorization      G INNER JOIN  
                         PageMaster ON G.numPageID = PageMaster.numPageID AND   
                         G.numModuleID = PageMaster.numModuleID INNER JOIN  
                         UserGroups ON G.numGroupID = UserGroups.numGroupID  
     WHERE     (dbo.UserGroups.numUserID = @numUserID) AND (dbo.PageMaster.vcFileName = @vcFileName) AND (dbo.PageMaster.nummoduleid = @numModuleId)  
   GROUP BY G.numPageID, G.numModuleID  
  END
GO
