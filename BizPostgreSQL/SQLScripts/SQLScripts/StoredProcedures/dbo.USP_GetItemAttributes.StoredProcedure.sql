/****** Object:  StoredProcedure [dbo].[USP_GetItemAttributes]    Script Date: 07/26/2008 16:17:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getitemattributes')
DROP PROCEDURE usp_getitemattributes
GO
CREATE PROCEDURE [dbo].[USP_GetItemAttributes]        
@numItemCode as numeric(9)=0        
as        
        
select distinct(fld_id),Fld_label,fld_type,numlistid,vcURL from ItemGroupsDTL                   
join CFW_Fld_Master on numOppAccAttrID=Fld_ID               
join Item on numItemGroup=ItemGroupsDTL.numItemGroupID and tintType=2        
where numItemcode=@numItemCode
GO
