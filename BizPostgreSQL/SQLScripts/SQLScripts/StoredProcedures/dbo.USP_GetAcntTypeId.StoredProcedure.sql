/****** Object:  StoredProcedure [dbo].[USP_GetAcntTypeId]    Script Date: 07/26/2008 16:16:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created By Siva  
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getacnttypeid')
DROP PROCEDURE usp_getacnttypeid
GO
CREATE PROCEDURE [dbo].[USP_GetAcntTypeId]  
@numAccountId as numeric(9)=0,  
@numDomainId as numeric(9)=0  
As   
Begin  
Select isnull([numParntAcntTypeId],0) from Chart_Of_Accounts Where numAccountId=@numAccountId And numDomainId=@numDomainId  
End
GO
