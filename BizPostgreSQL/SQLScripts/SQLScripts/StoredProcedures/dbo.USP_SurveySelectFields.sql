USE [BizExch2007DB]
GO
/****** Object:  StoredProcedure [dbo].[usp_SurveySelectFields]    Script Date: 10/12/2008 12:19:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created By: Pratik Vasani                                       
--Purpose: Saves the Survey Respondents Information            
--Created Date: 09/23/2005                                  
CREATE PROCEDURE [dbo].[usp_SurveySelectFields]
@numIsLeadsField tinyint
 AS                                   
                      
 select * from SurveySelectFields where tintIsLeadsField = @numIsLeadsField
   SELECT @@IDENTITY

