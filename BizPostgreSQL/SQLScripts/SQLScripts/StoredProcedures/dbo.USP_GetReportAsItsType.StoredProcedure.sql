/****** Object:  StoredProcedure [dbo].[USP_GetReportAsItsType]    Script Date: 07/26/2008 16:18:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created y anoop jayaraj  
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getreportasitstype')
DROP PROCEDURE usp_getreportasitstype
GO
CREATE PROCEDURE [dbo].[USP_GetReportAsItsType]    
@numUserCntid as numeric(9),        
@numDomainId AS NUMERIc(9),        
@tintType as tinyint        
as        
select AIT.numListItemID,LD.vcData from ForReportsByAsItsType AIT    
JOIN ListDetails LD    
ON LD.numListItemID = AIT.numListItemID    
where AIT.numUserCntid=@numUserCntid      
and AIT.numDomainId=@numDomainID      
and AIT.tintType=@tintType
GO
