/****** Object:  StoredProcedure [dbo].[usp_DeleteCustomReport]    Script Date: 07/26/2008 16:15:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-------- Modified By : Gangadhar         
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_deletecustomreport')
DROP PROCEDURE usp_deletecustomreport
GO
CREATE PROCEDURE [dbo].[usp_DeleteCustomReport]          
 @numCustReportID Numeric,          
 @numUserCntID Numeric ,
@numDomainID numeric(9)         
AS                    
BEGIN                
 DELETE FROM CustReportFields WHERE numCustomReportId = @numCustReportID          
 DELETE FROM CustReportFilterlist WHERE numCustomReportId = @numCustReportID        
 DELETE FROM CustReportOrderlist WHERE numCustomReportId = @numCustReportID        
 DELETE FROM CustReportSummationlist WHERE numCustomReportId = @numCustReportID        
       
 delete CustRptSchContacts where numScheduleId in (select numScheduleid from CustRptScheduler where numReportId = @numCustReportID )      
 delete CustRptScheduler where numReportId = @numCustReportID      
delete MyReportList where numRptId = @numCustReportID and tinttype=1 and numUserCntId = @numUserCntID  
        
 DELETE FROM CustomReport WHERE numCustomReportID = @numCustReportID AND numCreatedBy = @numUserCntID    and numDomainID = @numDomainID      
  RETURN 1          
END
GO
