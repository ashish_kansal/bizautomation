/****** Object:  StoredProcedure [dbo].[USP_GetGroups]    Script Date: 07/26/2008 16:17:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj  
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getgroups')
DROP PROCEDURE usp_getgroups
GO
CREATE PROCEDURE [dbo].[USP_GetGroups]          
as      
      
SELECT numGrpID, vcGrpName FROM groups       
ORDER BY vcGrpName
GO
