/****** Object:  StoredProcedure [dbo].[usp_saveAdvSearchTeamTerritoryPreferences]    Script Date: 07/26/2008 16:20:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created By: Debasish Tapan Nag                                    
--Purpose: Save the Team and Territory Preference of the User for Advance Search    
--Created Date: 08/14/2005    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_saveadvsearchteamterritorypreferences')
DROP PROCEDURE usp_saveadvsearchteamterritorypreferences
GO
CREATE PROCEDURE [dbo].[usp_saveAdvSearchTeamTerritoryPreferences]  
 @numDomainID Numeric,    
 @numUserCntID Numeric,    
 @vcTerritoriesSelected NVARCHAR(2000),    
 @vcTeamsSelected NVARCHAR(2000)    
AS    
 IF EXISTS(SELECT * FROM AdvSearchTeamTerritoryPreferences Where numUserCntId = @numUserCntID And @numDomainID = @numDomainID)    
 BEGIN    
  IF LTRIM(@vcTerritoriesSelected) = ''  
   UPDATE AdvSearchTeamTerritoryPreferences SET    
   vcTeamsSelected = @vcTeamsSelected    
   Where numUserCntId = @numUserCntID And @numDomainID = @numDomainID    
  ELSE    
   UPDATE AdvSearchTeamTerritoryPreferences SET    
   vcTerritoriesSelected = @vcTerritoriesSelected    
   Where numUserCntId = @numUserCntID And @numDomainID = @numDomainID    
 END    
 ELSE    
 BEGIN    
  INSERT INTO AdvSearchTeamTerritoryPreferences(numUserCntID, vcTerritoriesSelected, vcTeamsSelected, numDomainID)    
   VALUES (@numUserCntID, @vcTerritoriesSelected, @vcTeamsSelected, @numDomainID)    
 END
GO
