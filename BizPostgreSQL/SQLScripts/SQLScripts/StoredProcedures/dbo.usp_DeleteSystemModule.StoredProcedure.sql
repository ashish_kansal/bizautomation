/****** Object:  StoredProcedure [dbo].[usp_DeleteSystemModule]    Script Date: 07/26/2008 16:15:43 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_deletesystemmodule')
DROP PROCEDURE usp_deletesystemmodule
GO
CREATE PROCEDURE [dbo].[usp_DeleteSystemModule]
	@numModuleID INT   
--
AS
	BEGIN
		DELETE FROM ModuleMaster
		WHERE numModuleID = @numModuleID
	END
GO
