/****** Object:  StoredProcedure [dbo].[USP_GetDataForPricebookRule]    Script Date: 07/26/2008 16:17:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj    
-- USP_GetDataForPricebookRule 366,72,1
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getdataforpricebookrule')
DROP PROCEDURE usp_getdataforpricebookrule
GO
CREATE PROCEDURE [dbo].[USP_GetDataForPricebookRule]    
@numRuleID as numeric(9)=0,    
@numDomainID as numeric(9)=0,    
@tintRuleAppType as tinyint    
as   
  
     
  
if @tintRuleAppType=1    
begin   

--  select numItemCode,vcItemName from item where numDomainID=@numDomainID and numItemCode not in     
-- (select numValue from PriceBookRuleDTL where tintType=1) 
  
 select [numPriceBookItemID],[numRuleID], vcItemName,[txtItemDesc],[vcModelID], dbo.[GetListIemName]([numItemClassification]) ItemClassification from item   
 join [PriceBookRuleItems]  
 on numValue=numItemCode  
 where numDomainID=@numDomainID and numRuleID=@numRuleID and tintType=1
    
end    
if @tintRuleAppType=2    
begin    

  SELECT [numListItemID] numItemClassification,ISNULL([vcData],'-') ItemClassification  FROM [ListDetails] WHERE [numListID]=36 AND [numDomainID]=@numDomainID AND [numListItemID] NOT IN (select numValue from [PriceBookRuleItems] where tintType=2 and numRuleID=@numRuleID)
  
 select [numPriceBookItemID],[numRuleID],[numValue],dbo.[GetListIemName]([numValue]) ItemClassification,(SELECT COUNT(*) FROM item I1 WHERE I1.numItemClassification=[numValue]) AS ItemsCount from 
 [PriceBookRuleItems]   
 where numRuleID=@numRuleID and tintType=2
 
end    
if @tintRuleAppType=3    
begin    
 select numPriceBookRuleDTLID,[numRuleID],vcCompanyName,dbo.[GetListIemName](dbo.CompanyInfo.numCompanyType) +',' + dbo.[GetListIemName](dbo.CompanyInfo.vcProfile) Relationship,
 (SELECT ISNULL(vcFirstName,'') + ' ' + ISNULL(vcLastName,'') + ', ' + ISNULL(vcEmail,'')  FROM [AdditionalContactsInformation] A WHERE A.[numDivisionID]=[DivisionMaster].[numDivisionID] AND ISNULL(A.bitPrimaryContact,0)=1) AS PrimaryContact  from DivisionMaster  
 join Companyinfo on DivisionMaster.numCompanyID=Companyinfo.numCompanyID  
 join PriceBookRuleDTL on numValue=DivisionMaster.numDivisionID  
 where DivisionMaster.numDomainID=@numDomainID and numRuleID=@numRuleID and tintType=3   
end    
--if @tintRuleAppType=5    
--begin    
-- select numPriceBookRuleDTLID,vcData from ListDetails   
-- join PriceBookRuleDTL  
-- on numValue=numListItemID  
-- where numRuleID=@numRuleID and tintType=5   
--end    
if @tintRuleAppType=4    
begin   

 select numPriceBookRuleDTLID,numRuleID,L1.vcData+' - '+L2.vcData as RelProfile from PriceBookRuleDTL DTL
 Join ListDetails L1  
 on L1.numListItemID=DTL.numValue  
 Join ListDetails L2  
 on L2.numListItemID=DTL.numProfile  
 where numRuleID=@numRuleID and tintType=4
end
GO
