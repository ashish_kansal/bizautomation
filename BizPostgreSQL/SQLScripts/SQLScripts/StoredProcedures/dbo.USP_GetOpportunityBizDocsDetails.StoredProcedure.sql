GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getopportunitybizdocsdetails')
DROP PROCEDURE usp_getopportunitybizdocsdetails
GO
CREATE PROCEDURE [dbo].[USP_GetOpportunityBizDocsDetails]
    @numBizDocsPaymentDetId AS NUMERIC(9) = 0,
    @numDomainId AS NUMERIC(9) = 0
AS 
BEGIN      
    SELECT  BD.numBizDocsPaymentDetId,
            numBizDocsId,
            numPaymentMethod,
            BD.monAmount,
            numDepoistToChartAcntId,
            numDomainId,
            vcReference,
            vcMemo,
            bitAuthoritativeBizDocs,
            bitIntegratedToAcnt,
            numCreatedBy,
            dtCreationDate,
            numModifiedBy,
            dtModifiedDate,
            bitDeferredIncome,
            sintDeferredIncomePeriod,
            dtDeferredIncomeStartDate,
            NoofDeferredIncomeTransaction,
            numContractId,
            bitSalesDeferredIncome,
            numExpenseAccount,
            numDivisionID,
            numReturnID,
            tintPaymentType,
            numCurrencyID,
            fltExchangeRate,
            numCardTypeID,
            dbo.FormatedDateFromDate(ISNULL(PD.dtDueDate,GETDATE()),Bd.numDomainId) dtDueDate,
            ISNULL(numLiabilityAccount,0) numLiabilityAccount,
            ISNULL(numClassID,0) numClassID,
            ISNULL(numProjectID,0) numProjectID,
            ISNULL(numTermsID,0) AS [numTermsID],
            ISNULL(dtBillDate,'') AS [dtBillDate]
    FROM    OpportunityBizDocsDetails BD 
    LEFT JOIN dbo.OpportunityBizDocsPaymentDetails PD 
    ON PD.numBizDocsPaymentDetId = BD.numBizDocsPaymentDetId
    WHERE   BD.numBizDocsPaymentDetId = @numBizDocsPaymentDetId
            AND BD.numDomainId = @numDomainId   
END
GO
