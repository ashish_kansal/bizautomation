GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_managerbizdocstatusapprove')
DROP PROCEDURE usp_managerbizdocstatusapprove
GO
CREATE PROCEDURE [dbo].[USP_ManagerBizDocStatusApprove]
@numBizDocStatusAppID numeric(18,0),
@numDomainID int,
@numBizDocTypeID numeric(18,0),
@numBizDocStatusID numeric(18,0),
@numEmployeeID numeric(18,0),
@numActionTypeID numeric(18,0)

as

INSERT INTO BizDocStatusApprove SELECT @numDomainID,@numBizDocTypeID,@numBizDocStatusID,@numEmployeeID,@numActionTypeID
select SCOPE_IDENTITY()