/****** Object:  StoredProcedure [dbo].[USP_GetCategoriesAndItems]    Script Date: 07/26/2008 16:16:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj    
--exec USP_GetCategoriesAndItems 1,6,30  
-- 4 changes kishan 2 group by,,,   
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getcategoriesanditems')
DROP PROCEDURE usp_getcategoriesanditems
GO
CREATE PROCEDURE [dbo].[USP_GetCategoriesAndItems]    
@numDomainID as numeric(9)=0,    
@numCategoryID as numeric(9),    
@numWareHouseID as numeric(9)=0,  
@str as varchar(100)    
as      
    
    
declare @bitShowInStock as bit    
declare @bitShowQuantity as bit    
--declare @tintColumns as tinyint    
set @bitShowInStock=0    
set @bitShowQuantity=0    
--set @tintColumns=1    
    
select @bitShowInStock=isnull(bitShowInStock,0),@bitShowQuantity=isnull(bitShowQOnHand,0)--,@tintColumns=isnull(tintItemColumns,1)    
from eCommerceDTL where numDomainID=@numDomainID    
    
    
if @numCategoryID>0   
begin  
 select C1.numCategoryID,c1.vcCategoryName from Category C1                       
 where C1.numDepCategory=@numCategoryID and numDomainID=@numDomainID                        
                     
 select X.*,(case when charItemType<>'S' then     
 (Case when @bitShowInStock=1 then ( Case when numWareHouseID is null then '<font color=red>Out Of Stock</font>'  when bitAllowBackOrder=1 then 'In Stock' else     
 (Case when X.OnHand>0 then 'In Stock'+(Case when @bitShowQuantity=1 then '('+convert(varchar(20),X.OnHand)+')' else '' end)     
 else '<font color=red>Out Of Stock</font>' end ) end)    else '' end) else '' end) as InStock     
 from 
 (select numItemCode,vcItemName,isnull(Sum(numOnHand),0) as OnHand,charItemType,bitAllowBackOrder,
 --kishan
 (SELECT TOP 1 II.vcPathForTImage 
  FROM dbo.ItemImages II 
  WHERE II.numItemCode = Item.numItemCode 
  AND II.bitDefault = 1 
  AND II.numDomainID = @numDomainID) 
  AS vcPathForTImage,
  txtItemDesc,
  numWareHouseID  from Item                        
 join ItemCategory                         
 on numItemCode=numItemID    
 left join  WareHouseItems W    
 on W.numItemID=numItemCode  and numWareHouseID=@numWareHouseID                       
 where numCategoryID=@numCategoryID  and Item.numDomainID=@numDomainID     AND W.numDomainID = @numDomainID
 group by numItemCode,vcItemName,charItemType,bitAllowBackOrder,txtItemDesc,numWareHouseID)X     
end  
else  
  select X.*,(case when charItemType<>'S' then     
 (Case when @bitShowInStock=1 then ( Case when numWareHouseID is null then '<font color=red>Out Of Stock</font>'  when bitAllowBackOrder=1 then 'In Stock' else     
 (Case when X.OnHand>0 then 'In Stock'+(Case when @bitShowQuantity=1 then '('+convert(varchar(20),X.OnHand)+')' else '' end)     
 else '<font color=red>Out Of Stock</font>' end ) end)    else '' end) else '' end) as InStock     
 from 
 (select numItemCode,vcItemName,isnull(Sum(numOnHand),0) as OnHand,charItemType,bitAllowBackOrder,
 --kishan
 (SELECT TOP 1 II.vcPathForTImage 
  FROM dbo.ItemImages II 
  WHERE II.numItemCode = Item.numItemCode 
  AND II.bitDefault = 1 
  AND II.numDomainID = @numDomainID) 
  AS
  vcPathForTImage,
 txtItemDesc,
 numWareHouseID  
 from Item                         
 left join  WareHouseItems W    
 on W.numItemID=numItemCode  and numWareHouseID=@numWareHouseID                       
 where Item.numDomainID=@numDomainID     AND W.numDomainID = @numDomainID  and  ( vcItemName like '%'+@str+'%' or txtItemDesc like '%'+@str+'%')   
 group by numItemCode,vcItemName,charItemType,bitAllowBackOrder,txtItemDesc,numWareHouseID)X      
   
    
    
--select @tintColumns
GO
