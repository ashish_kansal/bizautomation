/****** Object:  StoredProcedure [dbo].[Usp_GetScheduledEmails]    Script Date: 07/26/2008 16:18:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getscheduledemails')
DROP PROCEDURE usp_getscheduledemails
GO
CREATE PROCEDURE [dbo].[Usp_GetScheduledEmails]    
  
@numScheduleid as numeric(9)    
as    
select vcEmail from CustRptSchContacts CRS    
join AdditionalContactsInformation AC on   CRS.numContactId =  AC.numContactId    
 where    
 numScheduleid = @numScheduleid
GO
