/****** Object:  StoredProcedure [dbo].[USP_ManageAlertEmails]    Script Date: 07/26/2008 16:19:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--- Created By Anoop Jayaraj  
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_managealertemails')
DROP PROCEDURE usp_managealertemails
GO
CREATE PROCEDURE [dbo].[USP_ManageAlertEmails]  
@numAlertDTLID as numeric(9)=0,  
@numAlertEmailID as numeric(9)=0,  
@vcEmailID as varchar(100)='',  
@byteMode as tinyint=0 ,
@numDomainID as numeric(9) 
as  
  
  
  
  
if @byteMode=1  
begin   
delete from AlertEmailDTL where numAlertEmailID=@numAlertEmailID and numDomainID= @numDomainID
end  
  
if @byteMode=2  
begin   
insert into  AlertEmailDTL(numAlertDTLID,vcEmailID,numDomainID)   
values(@numAlertDTLID,@vcEmailID,@numDomainID)  
end
GO
