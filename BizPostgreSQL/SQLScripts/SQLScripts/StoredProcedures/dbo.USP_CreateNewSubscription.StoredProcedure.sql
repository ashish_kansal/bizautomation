/****** Object:  StoredProcedure [dbo].[USP_CreateNewSubscription]    Script Date: 07/26/2008 16:15:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj  
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_createnewsubscription')
DROP PROCEDURE usp_createnewsubscription
GO
CREATE PROCEDURE [dbo].[USP_CreateNewSubscription]  
@numSubscriberID as numeric(9),  
@numUserCntID as numeric(9)  
as  
  
insert into SubscriberHstr (numSubscriberID,intNoofSubscribers,dtSubStartDate,  
dtSubRenewDate,bitTrial,numAdminContactID,bitStatus,dtSuspendedDate,vcSuspendedReason,  
numTargetDomainID,numDomainID,numCreatedBy,dtCreatedDate)  
select numSubscriberID,intNoofUsersSubscribed,dtSubStartDate,  
dtSubEndDate,bitTrial,numAdminContactID,bitActive,dtSuspendedDate,vcSuspendedReason,  
numTargetDomainID,numDomainID,@numUserCntID,getutcdate() from Subscribers where numSubscriberID=@numSubscriberID  
  
  
update subscribers set intNoofUsersSubscribed=0,intNoofPartialSubscribed=0,intNoofMinimalSubscribed=0,
dtSubStartDate=null,dtSubEndDate=null,bitTrial=0,bitActive=0,dtSuspendedDate=null,vcSuspendedReason='' where numSubscriberID=@numSubscriberID
GO
