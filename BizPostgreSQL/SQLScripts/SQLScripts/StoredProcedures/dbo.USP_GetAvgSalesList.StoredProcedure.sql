/****** Object:  StoredProcedure [dbo].[USP_GetAvgSalesList]    Script Date: 07/26/2008 16:16:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getavgsaleslist')
DROP PROCEDURE usp_getavgsaleslist
GO
CREATE PROCEDURE [dbo].[USP_GetAvgSalesList]                                                         
 @numUserCntID numeric,                                                              
 @tintUserRightType tinyint,     
 @intSort integer,                                                                                                   
 @columnName as Varchar(50)='',                                                            
 @columnSortOrder as Varchar(10)='',                                    
 @ListType as tinyint=0,                                    
 @TeamType as integer=0,       
 @TeamMemID as numeric(9)=0,                                
 @numDomainID as numeric(9)=0,              
 @dtFromDate as Datetime,          
 @dtToDate as Datetime    
    
                                                        
                                                              
AS                                                              
declare @strSql as varchar(7000)       
  
  
  
  
  
                                                                        
                                              
 set @strSql= 'SELECT OM.numOppId,    
   OM.numDivisionId,                                                                               
   DM.bintCreatedDate as bintCreatedDate,                                                              
   OM.bintAccountClosingDate as bintAccountClosingDate,    
   VcCompanyName as CName,                                                                                                                                                                                                     
   DATEDIFF ( day,DM.bintCreatedDate, OM.bintAccountClosingDate) as days,                                                                                                                                                                                     
   dbo.fn_GetContactName(OM.numRecOwner) as numRecOwner,                                                                                                                                                                                                     
   dbo.fn_GetContactName(OM.numAssignedTo) as AssignedTo,     
   OM.monPAmount,
  ADC.numContactID,                      
 DM.numCompanyID,                    
  DM.numTerID,
  DM.tintCRMType                                                                       
   FROM        
   OpportunityMaster OM                                                            
   join DivisionMaster DM                                        
   on DM.numDivisionID=OM.numDivisionID    
   join CompanyInfo C                                        
   on C.numCompanyID=DM.numCompanyID      
   join AdditionalContactsInformation ADC    
   on ADC.numContactID=OM.numRecOwner                                                             
   WHERE    
OM.numDomainID='+convert(varchar(15),@numDomainID)+ '        
and OM.bintCreatedDate >= '''+convert(varchar(20),@dtFromDate)+'''        
and OM.bintCreatedDate <= '''+convert(varchar(20),@dtToDate)+'''    
and OM.tintOppStatus =1  
and OM.numOppID = (SELECT top 1  numOppID from    
OpportunityMaster OM where OM.numDivisionId=DM.numDivisionId and bintAccountClosingDate is not null  order by bintAccountClosingDate Asc )  
'  
  
if @intSort<>0 set @strSql=@strSql + ' AND OM.monPAmount >=  '+convert(varchar(15),@intSort)+''     
if @tintUserRightType=1 set @strSql=@strSql + ' AND OM.numRecOwner = '+convert(varchar(15),@numUserCntID)+''    
    
if @tintUserRightType=2 set  @strSql=@strSql + ' and ADC.numTeam in(select F.numTeam from ForReportsByTeam F         
where F.numuserCntid='+convert(varchar(15),@numUserCntID)+' and F.numDomainid='+convert(varchar(15),@numDomainId)+' and F.tintType='+convert(varchar(5),@TeamType)+')'                    
else if @tintUserRightType=3 set @strSql=@strSql + ' AND (DM.numTerID in (select numTerritoryID from  UserTerritory where numUserCntID= '+convert(varchar(15),@numUserCntID)+' ) or DM.numTerID=0)'                                            
  
                    
print @strSql                                            
                                 
exec(@strSql)
GO
