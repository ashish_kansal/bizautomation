/****** Object:  StoredProcedure [dbo].[usp_DelBusinessProcess]    Script Date: 07/26/2008 16:15:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--modified by anoop  
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_delbusinessprocess')
DROP PROCEDURE usp_delbusinessprocess
GO
CREATE PROCEDURE [dbo].[usp_DelBusinessProcess]  
@slpid numeric     
AS  

DELETE from StagePercentageDetailsTask where numStageDetailsId IN(SELECT numStageDetailsId from stagepercentagedetails where slp_id=@slpid)
DELETE from stagepercentagedetails where slp_id=@slpid
delete from Sales_process_List_Master where slp_id=@slpid
	
SELECT '1'


GO
