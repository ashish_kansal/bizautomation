/****** Object:  StoredProcedure [dbo].[USP_GetSubscriptionHistory]    Script Date: 07/26/2008 16:18:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getsubscriptionhistory')
DROP PROCEDURE usp_getsubscriptionhistory
GO
CREATE PROCEDURE [dbo].[USP_GetSubscriptionHistory]
@numSubscriberID as numeric(9)=0
as 

select numSubscriberDTLID, numSubscriberID, intNoofSubscribers, 
dtSubStartDate, dtSubRenewDate,case when bitTrial=0 then 'No' when bitTrial=1 then 'Yes' else 'No' End as Trial, 
case when bitStatus=0 then 'Supended' when bitStatus=1 then 'Active' else 'Supended' End as Status, dtSuspendedDate, vcSuspendedReason, numTargetDomainID, 
numDomainID, numCreatedBy, dtCreatedDate from SubscriberHstr 
where numSubscriberID=@numSubscriberID
GO
