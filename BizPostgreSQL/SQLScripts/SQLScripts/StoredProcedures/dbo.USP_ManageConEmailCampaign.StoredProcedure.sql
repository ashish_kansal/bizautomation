/****** Object:  StoredProcedure [dbo].[USP_ManageConEmailCampaign]    Script Date: 07/26/2008 16:19:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--create by anoop jayaraj        
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_manageconemailcampaign')
DROP PROCEDURE usp_manageconemailcampaign
GO
CREATE PROCEDURE [dbo].[USP_ManageConEmailCampaign]        
@numConEmailCampID numeric(9)=0,        
@numContactID numeric(9)=0,        
@numECampaignID numeric(9)=0,        
@intStartDate datetime,        
@bitEngaged bit,    
@numUserCntID numeric(9)=0        
as        
    
--DisEngage Current contact from ECampaign
IF @numECampaignID = 0 
BEGIN
	UPDATE [ConECampaign] SET [bitEngaged]=0 WHERE [numContactID]=@numContactID
	RETURN ;
END
        
        
if @numConEmailCampID= 0         
begin        
	
	-- stop all other campaign for given contact 
	update ConECampaign set bitEngaged= 0 where numContactID=@numContactID AND [numECampaignID] <> @numECampaignID
	
	--Prevent Duplicate entry
	IF NOT EXISTS (SELECT * FROM [ConECampaign] WHERE [numContactID]=@numContactID AND [numECampaignID]=@numECampaignID AND [bitEngaged]=1)
	BEGIN
		
		/*Add Start Time to Given Start Date from ECampaign*/
		DECLARE @StartDateTime DATETIME 	   
		DECLARE @dtECampStartTime1 DATETIME 	   
		SELECT @dtECampStartTime1 = ISNULL(dtStartTime,0) FROM [ECampaign] WHERE [numECampaignID]=@numECampaignID
		--	SELECT @StartDateTime;
		SELECT @StartDateTime = DATEADD(hour,DATEPART(hour,@dtECampStartTime1) ,@intStartDate)
		SELECT @StartDateTime = DATEADD(minute,DATEPART(minute,@dtECampStartTime1) ,@StartDateTime)
			
	
		--Insert To Header
		insert into ConECampaign(numContactID,numECampaignID,intStartDate,bitEngaged,numRecOwner)        
		values (@numContactID,@numECampaignID,@StartDateTime,@bitEngaged,@numUserCntID)
		set @numConEmailCampID=@@identity       
		--Insert To Detail
		DECLARE @TEMP TABLE
		(
			RowNo INT,
			numECampDTLId NUMERIC(18,0),
			tintDays INT,
			tintWaitPeriod TINYINT
		)

		INSERT INTO 
			@TEMP 
		SELECT 
			ROW_NUMBER() OVER(ORDER BY numECampDTLId),
			numECampDTLId,
			tintDays,
			tintWaitPeriod 
		FROM 
			ECampaignDTLs      
		WHERE 
			numECampID=@numECampaignID  AND
			(numEmailTemplate > 0 OR numActionItemTemplate > 0)
		ORDER BY
			numECampDTLId


		DECLARE @i AS INT = 0
		DECLARE @Count AS INT
		SELECT @Count=COUNT(*) FROM @TEMP


		DECLARE @TempDate AS DATETIME
		DECLARE @numECampDTLId AS NUMERIC(18,0)
		DECLARE @TempDays AS INT
		DECLARE @TempWaitPeriod AS INT

		SET @TempDate = @StartDateTime

		WHILE @i <= @Count
		BEGIN

			SELECT @numECampDTLId=numECampDTLId, @TempDays=tintDays, @TempWaitPeriod=tintWaitPeriod FROM @TEMP WHERE RowNo=@i

			INSERT INTO ConECampaignDTL
			(
				numConECampID,
				numECampDTLID,
				dtExecutionDate
			)       
			SELECT 
				@numConEmailCampID,
				@numECampDTLId,
				@TempDate 
			FROM 
				@TEMP      
			WHERE 
				RowNo=@i

			IF (@TempWaitPeriod = 1) --Day
				SELECT @TempDate = DATEADD(D,@TempDays,@TempDate)
			ELSE IF (@TempWaitPeriod = 2) --Week
				SELECT @TempDate = DATEADD(D,(@TempDays * 7),@TempDate)
			
			SET @i = @i + 1
		END
	END
end        
else         
begin        
	update ConECampaign set numContactID=@numContactID,        
	numECampaignID=@numECampaignID,        
	intStartDate=@intStartDate,        
	bitEngaged=@bitEngaged        
	where numConEmailCampID=@numConEmailCampID        
	end        
	        
	select @numConEmailCampID
GO
