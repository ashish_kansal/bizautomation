/****** Object:  StoredProcedure [dbo].[usp_saveDynamicFormAdditionalParam]    Script Date: 07/26/2008 16:21:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created By: Debasish Tapan Nag              
--Purpose: Saves the form additional optional params and the Group database                  
--Created Date: 07/12/2005                  
                  
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_savedynamicformadditionalparam')
DROP PROCEDURE usp_savedynamicformadditionalparam
GO
CREATE PROCEDURE [dbo].[usp_saveDynamicFormAdditionalParam]              
 @numFormId int,                   
 @vcAdditionalParam varchar(100) ,  
 @numGrpId Numeric(9),
 @numDomainID as numeric(9)  
as 
if not exists(select * from DynamicFormMasterParam where numFormID=@numFormId and numDomainID=@numDomainID) 
begin
	insert into DynamicFormMasterParam (numDomainID,vcAdditionalParam,numGrpId,numFormID)
	values (@numDomainID,@vcAdditionalParam,@numGrpId,@numFormId)
end
else
begin
	 Update DynamicFormMasterParam    
	 set vcAdditionalParam = @vcAdditionalParam ,  
	 numGrpId = @numGrpId  
	 where numFormId = @numFormId and numDomainID=@numDomainID
end
GO
