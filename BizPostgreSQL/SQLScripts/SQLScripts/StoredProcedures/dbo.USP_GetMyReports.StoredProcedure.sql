/****** Object:  StoredProcedure [dbo].[USP_GetMyReports]    Script Date: 07/26/2008 16:17:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- Created By Anoop jayaraj          
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getmyreports')
DROP PROCEDURE usp_getmyreports
GO
CREATE PROCEDURE [dbo].[USP_GetMyReports]
@numUserCntID as numeric(9)=0          
as          
select rptID,vcFileName,RPtHeading,RptDesc, RPtHeading+' - '+ RptDesc as Report,tintRptSequence,tintType from PageMaster           
join ReportList on          
numPageID=NumID          
join MyReportList          
on RptID=numRPTId          
where numModuleID=8 and numUserCntID=@numUserCntID and tintType =0 AND [bitEnable]=1
    
union    
select numRPTId,'frmCustomReport.aspx?reportId='+convert(varchar(10),numRptId) as vcFileName ,    
vcReportName as RPtHeading,    
vcReportDescription as RptDesc,    
vcReportName+' - '+vcReportDescription as Report,tintRptSequence,tintType    
 from customreport    
join MyReportList          
on numcustomReportId=numRPTId     
where  MyReportList.numUserCntID=@numUserCntID and tintType =1  
order by tintRptSequence
GO
