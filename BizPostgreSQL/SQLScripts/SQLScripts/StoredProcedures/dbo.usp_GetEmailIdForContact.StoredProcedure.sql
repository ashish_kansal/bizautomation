/****** Object:  StoredProcedure [dbo].[usp_GetEmailIdForContact]    Script Date: 07/26/2008 16:17:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getemailidforcontact')
DROP PROCEDURE usp_getemailidforcontact
GO
CREATE PROCEDURE [dbo].[usp_GetEmailIdForContact]
	@numContactId numeric 
--  
AS
	SELECT vcEmail FROM additionalcontactsinformation where numcontactid=@numcontactid
GO
