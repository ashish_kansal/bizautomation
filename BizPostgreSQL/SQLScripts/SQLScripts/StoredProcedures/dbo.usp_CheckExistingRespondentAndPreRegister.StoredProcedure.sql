/****** Object:  StoredProcedure [dbo].[usp_CheckExistingRespondentAndPreRegister]    Script Date: 07/26/2008 16:15:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created By: Debasish Tapan Nag                                              
--Purpose: Checks if the Respondent is registered, if yes then it returns the Respondent ID          
--Created Date: 12/29/2005                                    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_checkexistingrespondentandpreregister')
DROP PROCEDURE usp_checkexistingrespondentandpreregister
GO
CREATE PROCEDURE [dbo].[usp_CheckExistingRespondentAndPreRegister]          
 @numSurID Numeric,                 
 @vcEmail NVarchar(50),        
 @numDomainID Numeric                     
AS                
--This will insert response for the surveys.                
BEGIN            
   DECLARE @numDivisionId Numeric          
   DECLARE @numContactId Numeric          
   DECLARE @numRespondantID Numeric          
           
   SELECT TOP 1 @numDivisionId = numDivisionID, @numContactId = numContactID FROM AdditionalContactsInformation         
   WHERE vcEmail = @vcEmail AND LTrim(IsNull(vcEmail,'')) <> '' and numDomainId = @numDomainID     
        
   IF @@ROWCOUNT > 0          
   BEGIN           
 INSERT INTO SurveyRespondentsMaster(numSurID, numSurRating, dtDateofResponse, bitRegisteredRespondant, numRegisteredRespondentContactId, numDomainId)        
 VALUES(@numSurID, 0, getutcdate(), 1, @numContactId, @numDomainID)              
         
 SELECT @numRespondantID = @@IDENTITY          
         
 INSERT INTO SurveyRespondentsChild(numRespondantID, numSurID, vcDbColumnName, vcFormFieldName,           
 vcDbColumnValue, vcDbColumnValueText, intRowNum, intColumnNum, vcAssociatedControlType)          
 SELECT @numRespondantID, @numSurID, 'numCustomerId','Customer Id', 0, numCompanyID,          
 0, 1,'EditBox'  FROM DivisionMaster WHERE numDivisionId = @numDivisionId          
         
 INSERT INTO SurveyRespondentsChild(numRespondantID, numSurID, vcDbColumnName, vcFormFieldName,           
 vcDbColumnValue, vcDbColumnValueText, intRowNum, intColumnNum, vcAssociatedControlType)          
 VALUES (@numRespondantID, @numSurID, 'vcEmail','Email Address', 0, @vcEmail,          
 0, 2,'EditBox')          
         
 INSERT INTO SurveyRespondentsChild(numRespondantID, numSurID, vcDbColumnName, vcFormFieldName,           
 vcDbColumnValue, vcDbColumnValueText, intRowNum, intColumnNum, vcAssociatedControlType)          
 SELECT @numRespondantID, @numSurID, 'vcFirstName','First Name', 0, vcFirstName,          
 1, 1,'EditBox'  FROM AdditionalContactsInformation WHERE numContactId = @numContactId          
         
 INSERT INTO SurveyRespondentsChild(numRespondantID, numSurID, vcDbColumnName, vcFormFieldName,           
 vcDbColumnValue, vcDbColumnValueText, intRowNum, intColumnNum, vcAssociatedControlType)          
 SELECT @numRespondantID, @numSurID, 'vcLastName','Last Name', 0, vcLastName,          
 1, 2,'EditBox'  FROM AdditionalContactsInformation WHERE numContactId = @numContactId          
         
 INSERT INTO SurveyRespondentsChild(numRespondantID, numSurID, vcDbColumnName, vcFormFieldName,           
 vcDbColumnValue, vcDbColumnValueText, intRowNum, intColumnNum, vcAssociatedControlType)          
 SELECT @numRespondantID, @numSurID, 'numPhone','Phone', 0,numPhone ,          
 2, 1,'EditBox'  FROM AdditionalContactsInformation WHERE numContactId = @numContactId          
         
 INSERT INTO SurveyRespondentsChild(numRespondantID, numSurID, vcDbColumnName, vcFormFieldName,           
 vcDbColumnValue, vcDbColumnValueText, intRowNum, intColumnNum, vcAssociatedControlType)          
 SELECT @numRespondantID, @numSurID, 'numPhoneExtension','Extension', 0, numPhoneExtension,          
 2, 2,'EditBox'  FROM AdditionalContactsInformation WHERE numContactId = @numContactId          
         
 SELECT @numRespondantID          
   END          
   ELSE          
   BEGIN          
  SELECT 0          
   END           
END
GO
