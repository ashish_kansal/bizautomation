/****** Object:  StoredProcedure [dbo].[USP_TicklerCases]    Script Date: 07/26/2008 16:21:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj                      
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_ticklercases')
DROP PROCEDURE usp_ticklercases
GO
CREATE PROCEDURE [dbo].[USP_TicklerCases]                          
@numUserCntID as numeric(9)=null,                          
@numDomainID as numeric(9)=null,                          
@startDate as datetime,                          
@endDate as datetime                         
as                          

DECLARE @vcSelectedEmployess VARCHAR(100)
DECLARE @numCriteria INT

SELECT 
	@vcSelectedEmployess = vcSelectedEmployeeCases,
	@numCriteria = numCriteriaCases
FROM 
	TicklerListFilterConfiguration
WHERE
	numDomainID = @numDomainID AND
	numUserCntID = @numUserCntID


declare @strSql as varchar(8000)   
                
SET @strSql = 'SELECT     
	C.numCaseId,                          
	AddC.vcFirstName + '' '' + AddC.vcLastName as Contact,
	(CASE 
		WHEN Addc.numPhone <> '''' 
		THEN AddC.numPhone + (CASE 
								WHEN AddC.numPhoneExtension<>'''' 
								THEN '' - '' + AddC.numPhoneExtension 
								ELSE '''' 
							  END)  
		ELSE '''' 
	END) AS [Phone],                      
	AddC.vcEmail,                                                                        
	C.intTargetResolveDate,                           
	C.vcCaseNumber,                           
	Com.vcCompanyName,                          
	C.textSubject ,                                                 
	Div.numTerId,    
	c.numRecOwner,                                               
	dbo.fn_GetContactName(C.numAssignedTo) as AssignedTo                         
FROM 
	Cases  C                          
INNER JOIN  
	AdditionalContactsInformation  AddC                          
ON 
	C.numContactId = AddC.numContactId                           
INNER JOIN 
	DivisionMaster Div                           
ON 
	AddC.numDivisionId = Div.numDivisionID AND  
	AddC.numDivisionId = Div.numDivisionID                           
INNER JOIN  
	CompanyInfo  Com                          
ON 
	Div.numCompanyID = Com.numCompanyId                            
WHERE  
	C.numDomainid=' + CAST(@numDomainID AS VARCHAR(10)) + ' AND 
	C.numStatus <> 136 AND 
	(intTargetResolveDate >= '''+ Cast(@startDate as varchar(30)) +''' and intTargetResolveDate <= ''' + CAST(@endDate AS VARCHAR(30)) + ''')'

IF @numCriteria  = 1 --Action Items	Assigned
BEGIN
	set @strSql=@strSql+' AND C.numAssignedTo IN (' + CASE WHEN LEN(@vcSelectedEmployess) > 0 THEN @vcSelectedEmployess ELSE Cast(@numUserCntID as varchar(10)) END +')'
END
ELSE IF @numCriteria = 2 --Action Items Owned
BEGIN
	set @strSql=@strSql+' AND C.numCreatedby IN (' + CASE WHEN LEN(@vcSelectedEmployess) > 0 THEN @vcSelectedEmployess ELSE Cast(@numUserCntID as varchar(10)) END +')'
END
ELSE --Action Items Assigned Or Owned
BEGIN
	set @strSql=@strSql+' AND (C.numAssignedTo IN (' + CASE WHEN LEN(@vcSelectedEmployess) > 0 THEN @vcSelectedEmployess ELSE Cast(@numUserCntID as varchar(10)) END +')  or C.numCreatedby IN (' + CASE WHEN LEN(@vcSelectedEmployess) > 0 THEN @vcSelectedEmployess ELSE Cast(@numUserCntID as varchar(10)) END +' ))'
END
                   
SET @strSql = @strSql + 'ORDER BY 
	intTargetResolveDate DESC'

EXEC(@strSql)
GO
