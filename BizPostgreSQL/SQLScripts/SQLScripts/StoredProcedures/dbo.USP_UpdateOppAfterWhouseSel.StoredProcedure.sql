/****** Object:  StoredProcedure [dbo].[USP_UpdateOppAfterWhouseSel]    Script Date: 07/26/2008 16:21:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created BY Anoop jayaraj                                                  
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_updateoppafterwhousesel')
DROP PROCEDURE usp_updateoppafterwhousesel
GO
CREATE PROCEDURE [dbo].[USP_UpdateOppAfterWhouseSel]                                                    
(                                                    
  @numOppId as numeric=0 OUTPUT,                                                                                                        
  @strItems as text=null                                                                                                                                                                            
)                                                    
  
as  

declare @hDocItem as integer                                                
 EXEC sp_xml_preparedocument @hDocItem OUTPUT, @strItems              
                
                                                  
 Update OpportunityItems set numWarehouseItmsID=X.numWarehouseItmsID            
 from (SELECT numoppitemtCode as numOppItemID,numWarehouseItmsID  FROM OPENXML (@hDocItem,'/NewDataSet/Item[Op_Flag=2]',2)                                                    
 WITH  (numoppitemtCode numeric(9),
		numWarehouseItmsID numeric(9))
	)X where numoppitemtCode=X.numOppItemID        
                                                   
 EXEC sp_xml_removedocument @hDocItem  

update    OpportunityMaster set tintOppType=1 where numOppId = @numOppId
GO
