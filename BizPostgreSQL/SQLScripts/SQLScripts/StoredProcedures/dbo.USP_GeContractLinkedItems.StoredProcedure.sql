/****** Object:  StoredProcedure [dbo].[USP_GeContractLinkedItems]    Script Date: 07/26/2008 16:16:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_gecontractlinkeditems')
DROP PROCEDURE usp_gecontractlinkeditems
GO
CREATE PROCEDURE  [dbo].[USP_GeContractLinkedItems]
@numContractId as numeric(9),
@numDomainId as numeric,
@ClientTimeZoneOffset as int
as 

select case when tintTEType = 0 then 'T & E'      
  when tintTEType = 1 then       
    Case when numCategory = 1 then     
 case when (select tintopptype from opportunitymaster  where numoppid = TE.numOppId) = 1 then 'Sales Time' else 'Purch Time' end    
 else     
 case when (select tintopptype from opportunitymaster  where numoppid = TE.numOppId) = 1 then 'Sales Exp' else 'Purch Exp' end    
  end       
  when tintTEType = 2 then       
    Case when numCategory = 1 then 'Project Time' else 'Project Exp' end       
  when tintTEType = 3 then       
    Case when numCategory = 1 then 'Case Time' else 'Case Exp' end       
end as chrFrom,      
                             
  Case when numCategory=1 then 'Time (' + (convert(varchar,dateadd(minute,-@ClientTimeZoneOffset,dtFromDate))+'-'+ convert(varchar,dateadd(minute,-@ClientTimeZoneOffset,dtToDate)))+')'      
    when numCategory=2 then 'Expense'       
  when numCategory=3 then 'Leave' end as Category ,
  Case when numCategory=1 then convert(varchar,convert(float,DATEDIFF(mi, dtFromDate, dtToDate))/60) +' (R/H '+convert(varchar,monAmount) +' )'                             
  when numCategory=2  then convert(varchar,monAmount)                            
 when numCategory=3 then (Case when bitFromFullDay=0  then  'HDL'                             
 when bitFromFullDay=1 then  'FDL' end)                             
 end as Detail
 from 
TimeandExpense te
where numcontractId =@numContractId and numdomainId =@numDomainId
GO
