/****** Object:  StoredProcedure [dbo].[USP_PaymentGateWayBsdDomainID]    Script Date: 07/26/2008 16:20:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj                              
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_paymentgatewaybsddomainid')
DROP PROCEDURE usp_paymentgatewaybsddomainid
GO
CREATE PROCEDURE [dbo].[USP_PaymentGateWayBsdDomainID]                              
@numDomainID as numeric(9)=0                              
as                              
select P.intPaymentGateWay,isnull(vcFirstFldValue,'') vcFirstFldValue,isnull(vcSecndFldValue,'') vcSecndFldValue,isnull(vcThirdFldValue,'') vcThirdFldValue,isnull(bitTest,0) bitTest from Domain D    
join PaymentGateway  P    
on P.intPaymentGateWay=D.intPaymentGateWay    
left join PaymentGatewayDTLID  PDTL    
on PDTL.intPaymentGateWay=P.intPaymentGateWay and PDTL.numDomainID= @numDomainID    
where D.numDomainID=@numDomainID
GO
