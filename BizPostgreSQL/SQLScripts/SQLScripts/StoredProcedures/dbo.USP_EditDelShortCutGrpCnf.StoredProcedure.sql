/****** Object:  StoredProcedure [dbo].[USP_EditDelShortCutGrpCnf]    Script Date: 07/26/2008 16:15:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_editdelshortcutgrpcnf')
DROP PROCEDURE usp_editdelshortcutgrpcnf
GO
CREATE PROCEDURE [dbo].[USP_EditDelShortCutGrpCnf]  
@Bytemode bit,  
@LinkId numeric,  
@Name varchar(100),  
@Link varchar(100)  
as  
 if @Bytemode = 0  
begin  
delete from shortcutbar where Id =@LinkId  
delete from ShortCutGrpConf where numLinkid =@LinkId 
delete from ShortCutUsrCnf where numLinkid =@LinkId 

end  
  
 if @Bytemode = 1  
begin  
update shortcutbar set   
  
vcLinkName = @Name,  
Link = @link  
  
where id =@LinkId  
end
GO
