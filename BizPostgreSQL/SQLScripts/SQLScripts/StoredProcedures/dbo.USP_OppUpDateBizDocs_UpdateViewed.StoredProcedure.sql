/****** Object:  StoredProcedure [dbo].[USP_OppUpDateBizDocs_UpdateViewed]    Script Date: 07/26/2008 16:20:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created By Siva
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_oppupdatebizdocs_updateviewed')
DROP PROCEDURE usp_oppupdatebizdocs_updateviewed
GO
CREATE PROCEDURE [dbo].[USP_OppUpDateBizDocs_UpdateViewed]                      
(                      
@numOppBizDocsId as numeric(9)=null,                      
 @numContactID as numeric(9) =null  
)                      
as                      
                     
update OpportunityBizDocs                      
 set numViewedBy=@numContactID,                      
 dtViewedDate=getutcdate()                      
 where numOppBizDocsId=@numOppBizDocsId                      

GO
