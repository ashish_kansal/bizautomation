/****** Object:  StoredProcedure [dbo].[usp_GetAllSelectedSystemModulesPagesAndAccesses]    Script Date: 07/26/2008 16:16:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getallselectedsystemmodulespagesandaccesses')
DROP PROCEDURE usp_getallselectedsystemmodulespagesandaccesses
GO
CREATE PROCEDURE [dbo].[usp_GetAllSelectedSystemModulesPagesAndAccesses]    
	@numModuleID NUMERIC(18,0) = 0,    
	@numGroupID NUMERIC(18,0) = 0
AS    
BEGIN    
	DECLARE @tintGroupType TINYINT
	SELECT @tintGroupType=ISNULL(tintGroupType,0) FROM AuthenticationGroupMaster WHERE numGroupID=@numGroupID

	IF @numModuleID = -1 --All Modules
	BEGIN
		DECLARE @TEMP TABLE
		(
			numModuleID NUMERIC(18,0)
			,vcModuleName VARCHAR(100)
		)

		IF EXISTS (SELECT ID FROM ModuleMasterGroupType WHERE tintGroupType=@tintGroupType)
		BEGIN
			INSERT INTO @TEMP
			(
				numModuleID
				,vcModuleName
			)
			SELECT 
				ModuleMaster.numModuleID
				,ModuleMaster.vcModuleName
			FROM 
				ModuleMaster 
			INNER JOIN
				ModuleMasterGroupType
			ON
				ModuleMaster.numModuleID = ModuleMasterGroupType.numModuleID
			WHERE 
				ModuleMasterGroupType.tintGroupType=@tintGroupType 
		END
		ELSE
		BEGIN
			INSERT INTO @TEMP
			(
				numModuleID
				,vcModuleName
			)
			SELECT 
				ModuleMaster.numModuleID
				,ModuleMaster.vcModuleName
			FROM 
				ModuleMaster 
			WHERE 
				tintGroupType=@tintGroupType 
		END

		SELECT
			*
		FROM
		(
			SELECT 
				PM.numModuleID
				,PM.numPageID
				,PM.vcPageDesc
				,GM.intViewAllowed
				,GM.intAddAllowed
				,GM.intUpdateAllowed
				,GM.intDeleteAllowed
				,GM.intExportAllowed
				,ISNULL(PM.vcToolTip,'') AS vcToolTip
				,ISNULL(PMGS.bitIsViewApplicable,PM.bitIsViewApplicable) bitIsViewApplicable
				,ISNULL(PMGS.bitViewPermission1Applicable,PM.bitViewPermission1Applicable) bitViewPermission1Applicable
				,ISNULL(PMGS.bitViewPermission2Applicable,PM.bitViewPermission2Applicable) bitViewPermission2Applicable
				,ISNULL(PMGS.bitViewPermission3Applicable,PM.bitViewPermission3Applicable) bitViewPermission3Applicable
				,ISNULL(PMGS.bitIsAddApplicable,PM.bitIsAddApplicable) bitIsAddApplicable
				,ISNULL(PMGS.bitAddPermission1Applicable,PM.bitAddPermission1Applicable) bitAddPermission1Applicable
				,ISNULL(PMGS.bitAddPermission2Applicable,PM.bitAddPermission2Applicable) bitAddPermission2Applicable
				,ISNULL(PMGS.bitAddPermission3Applicable,PM.bitAddPermission3Applicable) bitAddPermission3Applicable
				,ISNULL(PMGS.bitIsUpdateApplicable,PM.bitIsUpdateApplicable) bitIsUpdateApplicable
				,ISNULL(PMGS.bitUpdatePermission1Applicable,PM.bitUpdatePermission1Applicable) bitUpdatePermission1Applicable
				,ISNULL(PMGS.bitUpdatePermission2Applicable,PM.bitUpdatePermission2Applicable) bitUpdatePermission2Applicable
				,ISNULL(PMGS.bitUpdatePermission3Applicable,PM.bitUpdatePermission3Applicable) bitUpdatePermission3Applicable
				,ISNULL(PMGS.bitIsDeleteApplicable,PM.bitIsDeleteApplicable) bitIsDeleteApplicable
				,ISNULL(PMGS.bitDeletePermission1Applicable,PM.bitDeletePermission1Applicable) bitDeletePermission1Applicable
				,ISNULL(PMGS.bitDeletePermission2Applicable,PM.bitDeletePermission2Applicable) bitDeletePermission2Applicable
				,ISNULL(PMGS.bitDeletePermission3Applicable,PM.bitDeletePermission3Applicable) bitDeletePermission3Applicable
				,ISNULL(PMGS.bitIsExportApplicable,PM.bitIsExportApplicable) bitIsExportApplicable
				,ISNULL(PMGS.bitExportPermission1Applicable,PM.bitExportPermission1Applicable) bitExportPermission1Applicable
				,ISNULL(PMGS.bitExportPermission2Applicable,PM.bitExportPermission2Applicable) bitExportPermission2Applicable
				,ISNULL(PMGS.bitExportPermission3Applicable,PM.bitExportPermission3Applicable) bitExportPermission3Applicable
			FROM 
				PageMaster PM
			INNER JOIN
				@TEMP TM
			ON
				PM.numModuleID = TM.numModuleID
			LEFT JOIN
				PageMasterGroupSetting PMGS
			ON
				PM.numPageID = PMGS.numPageID
				AND PM.numModuleID = PMGS.numModuleID
				AND PMGS.numGroupID=@numGroupID
			LEFT JOIN 
				GroupAuthorization GM
			ON 
				PM.numPageID = GM.numPageID 
				AND PM.numModuleID = GM.numModuleID 
				AND (GM.numGroupID = @numGroupID OR GM.numGroupID IS NULL)  
				AND 1 = (CASE WHEN PM.numModuleID=32 THEN (CASE WHEN ISNULL(GM.bitCustomRelationship,0)=0 THEN 1 ELSE 0 END) ELSE 1 END)
			WHERE 
				ISNULL(bitDeleted,0) = 0
			UNION
			SELECT
				32 AS numModuleID, 
				TableCustomRelationship.numListItemID AS numPageID, 
				TableCustomRelationship.vcData + ' List' AS vcPageDesc, 
				ISNULL(GM.intViewAllowed,1) AS intViewAllowed, 
				0 AS intAddAllowed, 
				0 AS intUpdateAllowed,     
				0 AS intDeleteAllowed, 
				0 AS intExportAllowed, 
				'' as  vcToolTip,
				1 AS bitIsViewApplicable,
				(CASE WHEN @tintGroupType = 2 THEN 1 ELSE 1 END) bitViewPermission1Applicable,
				(CASE WHEN @tintGroupType = 2 THEN 0 ELSE 1 END) bitViewPermission2Applicable,
				(CASE WHEN @tintGroupType = 2 THEN 0 ELSE 1 END) bitViewPermission3Applicable,
				0 AS bitIsAddApplicable,
				0 bitAddPermission1Applicable,
				0 bitAddPermission2Applicable,
				0 bitAddPermission3Applicable,
				0 As bitIsUpdateApplicable,
				0 bitUpdatePermission1Applicable,
				0 bitUpdatePermission2Applicable,
				0 bitUpdatePermission3Applicable,
				0 AS bitIsDeleteApplicable,
				0 bitDeletePermission1Applicable,
				0 bitDeletePermission2Applicable,
				0 bitDeletePermission3Applicable,
				0 AS bitIsExportApplicable ,
				0 bitExportPermission1Applicable,
				0 bitExportPermission2Applicable,
				0 bitExportPermission3Applicable
			FROM
				(
					SELECT 
						numListItemID,
						vcData
					FROM 
						ListDetails 
					WHERE 
						numListID = 5 AND 
						(ISNULL(constFlag,0) = 1 OR (numDomainID = ISNULL((SELECT numDomainID FROM AuthenticationGroupMaster WHERE numGroupID = @numGroupID),0) AND 
						ISNULL(bitDelete,0) = 0)) AND 
						numListItemID <> 46) AS TableCustomRelationship
			LEFT JOIN 
				GroupAuthorization GM
			ON 
				GM.numPageID = TableCustomRelationship.numListItemID AND 
				(GM.numGroupID = @numGroupID OR GM.numGroupID IS NULL) AND
				GM.bitCustomRelationship = 1
		) TEMP
		ORDER BY 
			TEMP.vcPageDesc ASC 
	END
	ELSE
	BEGIN
		SELECT
			*
		FROM
		(
			SELECT 
				PM.numModuleID
				,PM.numPageID
				,PM.vcPageDesc
				,GM.intViewAllowed
				,GM.intAddAllowed
				,GM.intUpdateAllowed
				,GM.intDeleteAllowed
				,GM.intExportAllowed
				,ISNULL(PM.vcToolTip,'') AS vcToolTip
				,ISNULL(PMGS.bitIsViewApplicable,PM.bitIsViewApplicable) bitIsViewApplicable
				,ISNULL(PMGS.bitViewPermission1Applicable,PM.bitViewPermission1Applicable) bitViewPermission1Applicable
				,ISNULL(PMGS.bitViewPermission2Applicable,PM.bitViewPermission2Applicable) bitViewPermission2Applicable
				,ISNULL(PMGS.bitViewPermission3Applicable,PM.bitViewPermission3Applicable) bitViewPermission3Applicable
				,ISNULL(PMGS.bitIsAddApplicable,PM.bitIsAddApplicable) bitIsAddApplicable
				,ISNULL(PMGS.bitAddPermission1Applicable,PM.bitAddPermission1Applicable) bitAddPermission1Applicable
				,ISNULL(PMGS.bitAddPermission2Applicable,PM.bitAddPermission2Applicable) bitAddPermission2Applicable
				,ISNULL(PMGS.bitAddPermission3Applicable,PM.bitAddPermission3Applicable) bitAddPermission3Applicable
				,ISNULL(PMGS.bitIsUpdateApplicable,PM.bitIsUpdateApplicable) bitIsUpdateApplicable
				,ISNULL(PMGS.bitUpdatePermission1Applicable,PM.bitUpdatePermission1Applicable) bitUpdatePermission1Applicable
				,ISNULL(PMGS.bitUpdatePermission2Applicable,PM.bitUpdatePermission2Applicable) bitUpdatePermission2Applicable
				,ISNULL(PMGS.bitUpdatePermission3Applicable,PM.bitUpdatePermission3Applicable) bitUpdatePermission3Applicable
				,ISNULL(PMGS.bitIsDeleteApplicable,PM.bitIsDeleteApplicable) bitIsDeleteApplicable
				,ISNULL(PMGS.bitDeletePermission1Applicable,PM.bitDeletePermission1Applicable) bitDeletePermission1Applicable
				,ISNULL(PMGS.bitDeletePermission2Applicable,PM.bitDeletePermission2Applicable) bitDeletePermission2Applicable
				,ISNULL(PMGS.bitDeletePermission3Applicable,PM.bitDeletePermission3Applicable) bitDeletePermission3Applicable
				,ISNULL(PMGS.bitIsExportApplicable,PM.bitIsExportApplicable) bitIsExportApplicable
				,ISNULL(PMGS.bitExportPermission1Applicable,PM.bitExportPermission1Applicable) bitExportPermission1Applicable
				,ISNULL(PMGS.bitExportPermission2Applicable,PM.bitExportPermission2Applicable) bitExportPermission2Applicable
				,ISNULL(PMGS.bitExportPermission3Applicable,PM.bitExportPermission3Applicable) bitExportPermission3Applicable
			FROM 
				PageMaster PM
			LEFT JOIN
				PageMasterGroupSetting PMGS
			ON
				PM.numPageID = PMGS.numPageID
				AND PM.numModuleID = PMGS.numModuleID
				AND PMGS.numGroupID=@numGroupID
			LEFT JOIN 
				GroupAuthorization GM
			ON 
				PM.numPageID = GM.numPageID 
				AND PM.numModuleID = GM.numModuleID 
				AND (GM.numGroupID = @numGroupID OR GM.numGroupID IS NULL)  
				AND 1 = (CASE WHEN PM.numModuleID=32 THEN (CASE WHEN ISNULL(GM.bitCustomRelationship,0)=0 THEN 1 ELSE 0 END) ELSE 1 END)
			WHERE 
				PM.numModuleID = @numModuleID 
				AND ISNULL(bitDeleted,0) = 0
			UNION
				SELECT
					32 AS numModuleID, 
					TableCustomRelationship.numListItemID AS numPageID, 
					TableCustomRelationship.vcData + ' List' AS vcPageDesc, 
					ISNULL(GM.intViewAllowed,1) AS intViewAllowed, 
					0 AS intAddAllowed, 
					0 AS intUpdateAllowed,     
					0 AS intDeleteAllowed, 
					0 AS intExportAllowed, 
					'' as  vcToolTip,
					1 AS bitIsViewApplicable,
					(CASE WHEN @tintGroupType = 2 THEN 1 ELSE 1 END) bitViewPermission1Applicable,
					(CASE WHEN @tintGroupType = 2 THEN 0 ELSE 1 END) bitViewPermission2Applicable,
					(CASE WHEN @tintGroupType = 2 THEN 0 ELSE 1 END) bitViewPermission3Applicable,
					0 AS bitIsAddApplicable,
					0 bitAddPermission1Applicable,
					0 bitAddPermission2Applicable,
					0 bitAddPermission3Applicable,
					0 As bitIsUpdateApplicable,
					0 bitUpdatePermission1Applicable,
					0 bitUpdatePermission2Applicable,
					0 bitUpdatePermission3Applicable,
					0 AS bitIsDeleteApplicable,
					0 bitDeletePermission1Applicable,
					0 bitDeletePermission2Applicable,
					0 bitDeletePermission3Applicable,
					0 AS bitIsExportApplicable ,
					0 bitExportPermission1Applicable,
					0 bitExportPermission2Applicable,
					0 bitExportPermission3Applicable
				FROM
					(
						SELECT 
							numListItemID,
							vcData
						FROM 
							ListDetails 
						WHERE 
							numListID = 5 AND 
							(ISNULL(constFlag,0) = 1 OR (numDomainID = ISNULL((SELECT numDomainID FROM AuthenticationGroupMaster WHERE numGroupID = @numGroupID),0) AND 
							ISNULL(bitDelete,0) = 0)) AND 
							numListItemID <> 46) AS TableCustomRelationship
				LEFT JOIN 
					GroupAuthorization GM
				ON 
					GM.numPageID = TableCustomRelationship.numListItemID AND 
					(GM.numGroupID = @numGroupID OR GM.numGroupID IS NULL) AND
					GM.bitCustomRelationship = 1
				WHERE
					1 = (CASE WHEN @numModuleID=32 THEN 1 ELSE 0 END)
		) TEMP
		ORDER BY 
			TEMP.vcPageDesc ASC 
   END    
  END
GO
