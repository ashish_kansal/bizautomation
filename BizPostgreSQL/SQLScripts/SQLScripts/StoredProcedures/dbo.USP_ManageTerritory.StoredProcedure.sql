/****** Object:  StoredProcedure [dbo].[USP_ManageTerritory]    Script Date: 07/26/2008 16:20:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created By Anoop Jayaraj   
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_manageterritory')
DROP PROCEDURE usp_manageterritory
GO
CREATE PROCEDURE [dbo].[USP_ManageTerritory]     
@numTerID as numeric(9)=0,    
@numDomainID as numeric(9)=0,       
@vcTerName as varchar(100)='',      
@numUserID as numeric(9)=0    
as      
if @numTerID =0      
      
begin      
insert into TerritoryMaster (vcTerName,numDomainID,numCreatedBy,numModifiedBy,bintCreatedDate,bintModifiedDate)      
values(@vcTerName,@numDomainID,@numUserID,@numUserID,getutcdate(),getutcdate())      
end      
else      
begin      
update TerritoryMaster set vcTerName=@vcTerName,      
numDomainID=@numDomainID,      
numModifiedBy=@numUserID,    
bintModifiedDate=getutcdate()      
where numTerID=@numTerID      
         
end
GO
