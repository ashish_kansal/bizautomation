/****** Object:  StoredProcedure [dbo].[USP_InsertCashCreditHeaderDetails]    Script Date: 07/26/2008 16:19:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created by Siva                    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_insertcashcreditheaderdetails')
DROP PROCEDURE usp_insertcashcreditheaderdetails
GO
CREATE PROCEDURE [dbo].[USP_InsertCashCreditHeaderDetails]                
@numCashCreditId as numeric(9)=0,                  
@datEntry_Date as datetime,                                
@monAmount as DECIMAL(20,5),                        
@vcMemo as varchar(8000),                  
@numPurchaseId as numeric(9)=0,                
@numAcntTypeId as numeric(9)=0,           
@vcReferenceNo as varchar(8000),          
@bitMoneyOut as bit,          
@bitChargeBack as bit,          
@numDomainId as numeric(9)=0,        
@numRecurringId as numeric(9)=0,      
@numUserCntID as numeric(9)=0                              
                                
as                                 
Begin    
 if @numRecurringId=0 Set @numRecurringId=null                             
 If @numCashCreditId=0                        
 Begin                        
  Insert into CashCreditCardDetails(datEntry_Date,monAmount,vcMemo,numPurchaseId,numAcntTypeId,vcReference,bitMoneyOut,bitChargeBack,numDomainId,numRecurringId,numCreatedBy,dtCreatedDate) Values(@datEntry_Date,@monAmount,@vcMemo,@numPurchaseId    
  ,@numAcntTypeId,@vcReferenceNo,@bitMoneyOut,@bitChargeBack,@numDomainId,@numRecurringId,@numUserCntID,getutcdate())                              
  Set @numCashCreditId = SCOPE_IDENTITY()                              
  Select @numCashCreditId                  
                        
 End                    
               
 If @numCashCreditId<>0              
 Begin              
   Update CashCreditCardDetails Set datEntry_Date=@datEntry_Date,monAmount=@monAmount,vcMemo=@vcMemo,numPurchaseId=@numPurchaseId,numAcntTypeId=@numAcntTypeId,vcReference=@vcReferenceNo,bitMoneyOut=@bitMoneyOut,bitChargeBack=@bitChargeBack,      
   numDomainId=@numDomainId,numRecurringId=@numRecurringId,numModifiedBy=@numUserCntID,dtModifiedDate=getutcdate()  Where numCashCreditId=@numCashCreditId            
 End                             
End
GO
