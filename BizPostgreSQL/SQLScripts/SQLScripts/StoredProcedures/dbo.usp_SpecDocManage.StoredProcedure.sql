/****** Object:  StoredProcedure [dbo].[usp_SpecDocManage]    Script Date: 07/26/2008 16:21:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoopo jayaraj      
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_specdocmanage')
DROP PROCEDURE usp_specdocmanage
GO
CREATE PROCEDURE [dbo].[usp_SpecDocManage]              
(              
 @numDomainID as numeric(9)=null,              
 @vcDocDesc as varchar(1000)='',              
 @VcFileName as varchar(100)='',              
 @vcDocName as varchar(100)='',              
 @numDocCategory as numeric(9)=null,              
 @vcfiletype as varchar(10)='',              
 @numDocStatus as numeric(9)=null,              
 @numContactID as numeric(9)=null,              
 @numSpecDocId as numeric(9)=0 output,              
 @cUrlType as varchar(1)='',            
 @numRecID as numeric(9)=0 ,        
 @strType as varchar(10),          
 @byteMode as tinyint,          
 @numGenericDocID as numeric(9)            
)              
as              
 if  @byteMode =0          
begin            
  if @numSpecDocId=0               
  begin              
  insert into SpecificDocuments              
                
   (              
   numDomainID,              
   vcDocDesc,              
   VcFileName,              
   vcDocName,              
   numDocCategory,              
   vcfiletype,              
   numDocStatus,              
   numCreatedBy,              
   bintCreatedDate,              
   numModifiedBy,              
   bintModifiedDate,              
   cUrlType,            
   numRecID,        
   vcDocumentSection           
   )              
  values              
   (              
   @numDomainID,              
   @vcDocDesc,              
   @VcFileName,              
   @vcDocName,              
   @numDocCategory,              
   @vcfiletype,              
   @numDocStatus,              
   @numContactID,               
   getutcdate(),              
   @numContactID,              
   getutcdate(),              
   @cUrlType,            
   @numRecID,         
   @strType           
   )              
                
  set @numSpecDocId=@@identity               
                
  end              
                
  else              
                
  begin              
                
   update  SpecificDocuments              
                 
   set               
    vcDocDesc=@vcDocDesc,              
    VcFileName=@VcFileName,              
    vcDocName=@vcDocName,              
    numDocCategory=@numDocCategory,              
    vcfiletype=@vcfiletype,              
    numDocStatus=@numDocStatus,              
    numModifiedBy=@numContactID,              
    bintModifiedDate=getutcdate(),              
    cUrlType=@cUrlType              
                 
   where numSpecificDocID=@numSpecDocId and numDomainID=@numDomainID             
                
  end              
              
 end          
          
if @byteMode=1          
begin          
declare @FileName as varchar(200)          
declare @DocName as varchar(50)          
declare @DocCategory as varchar(20)          
declare @UrlType as varchar(1)          
declare @FileType as varchar(10)          
declare @BusClass as varchar(20)          
declare @DocDesc as varchar(1000)          
select  @FileName=VcFileName,          
 @DocName=vcDocName,          
 @DocCategory=numDocCategory,          
 @UrlType=cUrlType,          
 @FileType=vcFileType,          
 @BusClass=numDocStatus,          
 @DocDesc=vcDocDesc          
 from GenericDocuments where numGenericDocID=@numGenericDocID          
               
  if @numSpecDocId=0               
  begin       
insert into SpecificDocuments              
                
   (              
   numDomainID,              
   vcDocDesc,              
   VcFileName,              
   vcDocName,              
   numDocCategory,              
   vcfiletype,              
   numDocStatus,              
   numCreatedBy,              
   bintCreatedDate,              
   numModifiedBy,              
   bintModifiedDate,              
   cUrlType,            
   numRecID,        
   vcDocumentSection          
   )              
  values              
   (              
   @numDomainID,              
   @DocDesc,              
   @FileName,              
   @DocName,              
   @DocCategory,              
   @filetype,              
   @BusClass,              
   @numContactID,               
   getutcdate(),              
   @numContactID,              
   getutcdate(),              
   @UrlType,      
   @numRecID,        
   @strType           
   )           
      
       
end      
else      
begin      
update  SpecificDocuments              
                 
   set               
    vcDocDesc=@vcDocDesc,              
    VcFileName=@VcFileName,              
    vcDocName=@vcDocName,              
    numDocCategory=@numDocCategory,              
    vcfiletype=@vcfiletype,              
    numDocStatus=@numDocStatus,              
    numModifiedBy=@numContactID,              
    bintModifiedDate=getutcdate(),              
    cUrlType=@cUrlType              
                 
   where numSpecificDocID=@numSpecDocId  and numDomainID=@numDomainID    
      
      
end          
end        
 if  @byteMode =2          
begin       
      
select   numDomainID,              
   vcDocDesc,              
   VcFileName,              
   vcDocName,              
   numDocCategory,              
   vcfiletype,              
   numDocStatus,              
   numCreatedBy,              
   bintCreatedDate,              
   numModifiedBy,              
   bintModifiedDate,              
   cUrlType,            
   numRecID,        
   vcDocumentSection       
      
 from SpecificDocuments where numSpecificDocID=@numSpecDocId  and numDomainID=@numDomainID    
end
GO
