/****** Object:  StoredProcedure [dbo].[USP_GetDocApprovers]    Script Date: 07/26/2008 16:17:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getdocapprovers')
DROP PROCEDURE usp_getdocapprovers
GO
CREATE PROCEDURE [dbo].[USP_GetDocApprovers]    
@numDocID as numeric(9)=0,    
@cDocType as VARCHAR(10)=''    
as    
    
 select numDocID,D.numContactID,D.vcComment,  
case  when tintApprove=0 then '-' when tintApprove=1 then 'Approved' when tintApprove=2 then 'Declined' end Status,  
dtApprovedOn,  
cDocType,vcFirstName+' '+vcLastName as [Name],  
vcCompanyName from DocumentWorkflow D    
 join AdditionalContactsInformation A    
 on A.numContactID=D.numContactID    
 join DivisionMaster DM    
 on DM.numDivisionID=A.numDivisionID    
 join CompanyInfo C    
 on C.numCompanyID=DM.numCompanyID    
 where numDocID=@numDocID and cDocType=@cDocType
GO
