/****** Object:  StoredProcedure [dbo].[USP_ManageAdvSearchViews]    Script Date: 07/26/2008 16:19:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_manageadvsearchviews')
DROP PROCEDURE usp_manageadvsearchviews
GO
CREATE PROCEDURE [dbo].[USP_ManageAdvSearchViews]  
@strAdvSerView AS TEXT='',  
@numDomainID NUMERIC(18,0),  
@FormId NUMERIC(18,0),
@numUserCntID NUMERIC(18,0)
AS
BEGIN
	DELETE FROM AdvSerViewConf WHERE numDomainID=@numDomainID and numUserCntID=@numUserCntID and numFormId = @FormId
  
	DECLARE @hDoc1 INT                                  
	EXEC sp_xml_preparedocument @hDoc1 OUTPUT, @strAdvSerView                                  
                                  
	INSERT INTO AdvSerViewConf                                  
	(
		numFormFieldID
		,tintOrder
		,numDomainID
		,numUserCntID
		,numformId
		,bitCustom
	)                                                                 
	SELECT 
		X.numFormFieldID
		,X.tintOrder
		,@numDomainID
		,@numUserCntID
		,@FormId
		,X.bitCustom 
	FROM 
	(
		SELECT 
			* 
		FROM 
			OPENXML (@hDoc1,'/NewDataSet/Table',2)                                  
		WITH 
		(
			numFormFieldID numeric(9),  
			tintOrder tinyint,
			bitCustom BIT
		)
	)X                                 
                                                                
	EXEC sp_xml_removedocument @hDoc1
END
GO
