/****** Object:  StoredProcedure [dbo].[USP_DeleteChartCategory]    Script Date: 09/25/2009 16:26:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created By Siva                        
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_deletechartcategory')
DROP PROCEDURE usp_deletechartcategory
GO
CREATE PROCEDURE [dbo].[USP_DeleteChartCategory]                                  
@numAccountId numeric(9)=0,                              
@numDomainId numeric(9)=0                              
as                                   
Begin                                  
 Declare @numOpeningBalance as DECIMAL(20,5)                     
 Declare @strSQLEquityBal as varchar(8000)                        
 Declare @numAcntType as numeric(9)                           
 Declare @bitFixed as bit                      
 Declare @JournalId as  numeric(9)             
 Declare @numListItemId as numeric(9)
 Declare @strSQl as varchar(1000)
 Declare @numAccountIdOpeningEquity as numeric(9)
 DECLARE @bitProfitLoss AS BIT
                        
 select @numOpeningBalance=isnull(numOriginalOpeningBal,0),@numAcntType=[numAcntTypeId],@bitFixed=bitFixed,@numListItemId=isnull(numListItemId,0),@bitProfitLoss=ISNULL(bitProfitLoss,0) from Chart_Of_Accounts Where numAccountId=@numAccountId And bitFixed=0 and numDomainId=@numDomainId              

 --Do not allow accounts to be deleted if it is used as default account in Global Settings -> Accounting -> Set Default Accounts
 IF (SELECT COUNT(*) FROM AccountingCharges WHERE numDomainID=@numDomainId AND numAccountID = @numAccountId) > 0
 BEGIN
	RAISERROR ('DEFAULT_ACCOUNT',16,1);
    RETURN
 END

--Do not allow accounts to be deleted if it has child accounts available
IF (SELECT COUNT(*) FROM dbo.Chart_Of_Accounts WHERE numDomainID = @numDomainID AND numParentAccId = @numAccountId) > 0
 BEGIN
  	RAISERROR ('CHILD_AVAILABLE',16,1);
    RETURN
  END
 
IF (SELECT COUNT(*) FROM dbo.TaxItems WHERE numDomainID = @numDomainID AND numChartofAcntID = @numAccountId) >0
 	 BEGIN
	  	RAISERROR ('TaxItems_Depend',16,1);
        RETURN
	  END

 --bug id 882 #2
 IF @bitProfitLoss =1 
 BEGIN
	--Do not allow accounts to be deleted if it is last Profit and Loss Account
 	IF (SELECT COUNT(*) FROM dbo.Chart_Of_Accounts WHERE numDomainID = @numDomainID AND bitProfitLoss = 1) = 1
 	 BEGIN
	  	RAISERROR ('LAST_PL_ACCOUNT',16,1);
        RETURN
	  END
 END
 
 
  
 Set @numAccountIdOpeningEquity=(Select numAccountId From Chart_Of_Accounts Where bitOpeningBalanceEquity=1  and numDomainId = @numDomainId)
 Select @JournalId=numJournal_Id From General_Journal_Header Where numChartAcntId=@numAccountId And numDomainId=@numDomainId                      
                          
 --print @numAccountId                            
 ---print @numOpeningBalance                            
 --Commented by chintan
-- Set @strSQl= dbo.fn_ParentCategory(@numAccountId,@numDomainId)                            
-- Print (@strSQl)                          
-- Set @strSQl='update chart_of_accounts  set numopeningbal=isnull(numopeningbal,0) - '+ convert(varchar(30),@numOpeningBalance) +' where  numAccountId in ('+@strSQl+') And numDomainId='+Convert(varchar(10),@numDomainId)                            
           
 If @numAcntType=813                        
   Begin                       
--Commented by chintan
--    Set @strSQLEquityBal='update chart_of_accounts  set  numopeningbal=isnull(numopeningbal,0) - '+ convert(varchar(30),@numOpeningBalance) +                        
--    ', numOriginalOpeningBal =isnull(numOriginalOpeningBal,0) - '+ convert(varchar(30),@numOpeningBalance) + ' where  numAccountId='+convert(varchar(10),@numAccountIdOpeningEquity) +' And numDomainId='+Convert(varchar(10),@numDomainId)                        
--End Commenting                            
    --To update Opening Balance in General_Journal_Header & General_Journal_Details                      
----    Update General_Journal_Header Set numAmount=0 Where numChartAcntId=@numAccountId And numDomainId=@numDomainId                  
----    Update General_Journal_Details Set numDebitAmt=0 Where numJournalId=@JournalId         
----        
        
    Delete from General_Journal_Details Where numJournalId=@JournalId and numDomainId=@numDomainId       
    Delete from General_Journal_Header Where numChartAcntId=@numAccountId And numDomainId=@numDomainId     
  End                      
 If @numAcntType=816                        
  Begin                      
-- Commented by chintan  
--   Set @strSQLEquityBal='update chart_of_accounts  set  numopeningbal=isnull(numopeningbal,0) + '+ convert(varchar(30),@numOpeningBalance) +                        
--   ', numOriginalOpeningBal =isnull(numOriginalOpeningBal,0) + '+ convert(varchar(30),@numOpeningBalance) + ' where  numAccountId='+convert(varchar(10),@numAccountIdOpeningEquity) +' And numDomainId='+Convert(varchar(10),@numDomainId)
--End commenting
    --To update Opening Balance in General_Journal_Header & General_Journal_Details                      
--    Update General_Journal_Header Set numAmount=0 Where numChartAcntId=@numAccountId And numDomainId=@numDomainId                       
--    Update General_Journal_Details Set numCreditAmt=0 Where numJournalId=@JournalId           
    Delete from General_Journal_Details Where numJournalId=@JournalId And numDomainId=@numDomainId              
   Delete from General_Journal_Header Where numChartAcntId=@numAccountId And numDomainId=@numDomainId       
                 
 End                      
 exec (@strSQl)                           
 exec (@strSQLEquityBal)                          
 
 UPDATE [COARelationships] SET [numARAccountId] = 0,[numARParentAcntTypeID]=0 WHERE [numARAccountId] = @numAccountId
 UPDATE [COARelationships] SET [numAPAccountId] = 0,[numAPParentAcntTypeID]=0 WHERE [numAPAccountId] = @numAccountId
 DELETE FROM [COAShippingMapping] WHERE [numIncomeAccountID]=@numAccountId AND [numDomainID]=@numDomainId
 
  If @bitFixed=0                      
  Begin                      
	 Delete From General_Journal_Details Where numJournalId=@JournalId And numDomainId=@numDomainId                        
     Delete From General_Journal_Header Where  numChartAcntId=@numAccountId And numDomainId=@numDomainId                      
  End   
 
 
 DELETE FROM [ChartAccountOpening] WHERE [numAccountId]=@numAccountId
 Delete From Chart_Of_Accounts Where numAccountId=@numAccountId And bitFixed=0 and numDomainId=@numDomainId                  
            
 --  -- To Delete ListDetails For Long Term Asset            
 --  Delete From ListDetails Where numListItemID=@numListItemId                  
                
                
 End
