--created by anoop jayaraj                  
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_casemanage')
DROP PROCEDURE usp_casemanage
GO
CREATE PROCEDURE [dbo].[USP_CaseManage]                      
@numCaseId as numeric(9)=0 output,                      
@numDivisionID as numeric(9)=null,                      
@numContactId as numeric(9)=null,                      
@vcCaseNumber as varchar(50)='',                      
@textSubject as varchar(2000)='',                      
@textDesc as text='',                      
@numUserCntID as numeric(9)=null,                                  
@numDomainID as numeric(9)=null,                      
@tintSupportKeyType as tinyint,                      
@intTargetResolveDate as datetime,                      
@numStatus as numeric(9)=null,                      
@numPriority as numeric(9)=null,                      
@textInternalComments as text='',                      
@numReason as numeric(9)=null,                      
@numOrigin  as numeric(9)=null,                      
@numType as numeric(9)=null,                      
@numAssignedTo as numeric(9)=0,              
@numContractId as numeric(9)=0                   
as                      
                
if @intTargetResolveDate='Jan  1 1753 12:00:00:000AM' set @intTargetResolveDate=null                        
if @numCaseId = 0                      
begin                              
insert into Cases (                      
 numDivisionID,                      
 numContactId,                      
 vcCaseNumber,                      
 textSubject,                      
 textDesc,                      
 numCreatedBy,                      
 bintCreatedDate,                      
 numModifiedBy,                      
 bintModifiedDate,                      
 numDomainID,                      
 tintSupportKeyType,                      
 intTargetResolveDate,          
 numRecOwner ,    
 numContractId,
 numStatus,numPriority,numOrigin,numType,numReason,textInternalComments                 
 )                      
 values                      
 (                      
 @numDivisionID,                      
 @numContactId,                      
 @vcCaseNumber,                      
 @textSubject,                      
 @textDesc,                      
 @numUserCntID,                      
 getutcdate(),                      
 @numUserCntID,                      
 getutcdate(),                      
 @numDomainID,                      
 @tintSupportKeyType,                      
 @intTargetResolveDate,          
 @numUserCntID ,    
 @numContractId,
 @numStatus,@numPriority,@numOrigin,@numType,@numReason,@textInternalComments
 )                      
set @numCaseId=SCOPE_IDENTITY()              

IF EXISTS(SELECT * FROM Contracts WHERE numDivisonId=@numDivisionID AND numDomainId=@numDomainID AND intType=3 AND (ISNULL(numIncidents,0) - ISNULL(numIncidentsUsed,0))>0)
BEGIN
	UPDATE 
		Contracts
	SET 
		numIncidentsUsed = ISNULL(numIncidentsUsed,0)+1,
		numModifiedBy=@numUserCntID,
		dtmModifiedOn=GETUTCDATE()
	WHERE
		numDomainId=@numDomainID AND numDivisonId=@numDivisionID  AND intType=3
	INSERT INTO ContractsLog (
		intType, numDivisionID, numReferenceId, dtmCreatedOn, numCreatedBy,vcDetails,numUsedTime,numBalance,numContractId
	)
	SELECT 
		3,@numDivisionID,@numCaseId,GETUTCDATE(),@numUserCntID,@vcCaseNumber,1,(ISNULL(numIncidents,0) - ISNULL(numIncidentsUsed,0)),numContractId
	FROM Contracts WHERE numDivisonId=@numDivisionID AND numDomainId=@numDomainID AND intType=3 
END



      EXEC dbo.usp_CaseDefaultAssociateContacts @numCaseId=@numCaseId,@numDomainId=@numDomainId
  --Map Custom Field	
  DECLARE @tintPageID AS TINYINT;SET @tintPageID=3
  	
  EXEC dbo.USP_AddParentChildCustomFieldMap
	@numDomainID = @numDomainID, --  numeric(18, 0)
	@numRecordID = @numCaseId, --  numeric(18, 0)
	@numParentRecId = @numDivisionId, --  numeric(18, 0)
	@tintPageID = @tintPageID --  tinyint
	               
end                      
                       
else                      
                      
begin                      
		-- Checking For Incidents
		declare @update as bit 
		set @update = 0
		if @numContractId <> 0
		begin
		IF (SELECT bitIncidents FROM [ContractManagement] WHERE numdomainId = @numDomainID  and numcontractId = @numContractId) = 1
		BEGIN
			
			declare @Usedincidents as numeric(9)
			set @Usedincidents = 0
			declare @Availableincidents as numeric(9)
			

			select @Usedincidents=@Usedincidents+count(*) from cases 
				where numcontractId = @numContractId and numdomainId = @numDomainID and numCaseId <> @numCaseId

			select @Usedincidents=@Usedincidents+count(*) from projectsmaster 
				where numcontractId = @numContractId and numdomainId = @numDomainID 


			select @Availableincidents=isnull(numincidents,0) from contractManagement 
			where numdomainId = @numDomainID  and numcontractId = @numContractId
print '@Usedincidents-----'
print @Usedincidents
print @Availableincidents
			if @Usedincidents >=@Availableincidents
				set @update = 1
			END
		END 	
    
	if @update = 0
	begin 
			update Cases      
			set                      
			                      
			intTargetResolveDate=@intTargetResolveDate,                      
			textSubject=@textSubject,                      
			numStatus=@numStatus,                      
			numPriority=@numPriority,                      
			textDesc=@textDesc,                      
			textInternalComments=@textInternalComments,                      
			numReason=@numReason,                      
			numOrigin=@numOrigin,                      
			numType=@numType,                      
			numModifiedBy=@numUserCntID,                      
			bintModifiedDate=getutcdate() ,    
			 numContractId = @numContractId,
			 numContactID = @numContactID                              
			where numCaseId=@numCaseId 
			set @numCaseId =@numCaseId
	end       
	else
			set @numCaseId = 0	          
                                
end         
        
        
          
 ---Updating if Case is assigned to someone              
  declare @tempAssignedTo as numeric(9)            
  set @tempAssignedTo=null             
  select @tempAssignedTo=isnull(numAssignedTo,0) from Cases where  numCaseId=@numCaseId              
          
  if (@tempAssignedTo<>@numAssignedTo and  @numAssignedTo<>'0')            
  begin              
    update Cases set numAssignedTo=@numAssignedTo ,numAssignedBy=@numUserCntID where  numCaseId=@numCaseId             
  end    
  else if  (@numAssignedTo =0)            
  begin            
   update Cases set numAssignedTo=0 ,numAssignedBy=0 where  numCaseId=@numCaseId          
  end
GO
