/****** Object:  StoredProcedure [dbo].[USP_TransferOwnerShip]    Script Date: 07/26/2008 16:21:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj  
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_transferownership')
DROP PROCEDURE usp_transferownership
GO
CREATE PROCEDURE [dbo].[USP_TransferOwnerShip]   
@numUserCntID as numeric(9)=0,  
@numRecID as numeric(9)=0,
@byteMode as tinyint,
@numDomainID as numeric(9)=0  
as  
  
if @byteMode=0 
begin
	 update DivisionMaster set  numRecOwner=@numUserCntID where numDivisionID=@numRecID  and numDomainID=@numDomainID
	 UPDATE CompanyInfo SET bintModifiedDate=GETUTCDATE() WHERE numCompanyId=(SELECT TOP 1 
																					numCompanyId 
																				FROM 
																					DivisionMaster 
																				where 
																					numDivisionID=@numRecID  and numDomainID=@numDomainID)
end
else if @byteMode=1 
begin
	 update OpportunityMaster set  numRecOwner=@numUserCntID,bintModifiedDate=GETUTCDATE() where numOppID=@numRecID  and numDomainID=@numDomainID
end
else if @byteMode=2
begin
	 update ProjectsMaster set  numRecOwner=@numUserCntID,bintModifiedDate=GETUTCDATE() where numProId=@numRecID  and numDomainID=@numDomainID
end
else if @byteMode=3
begin
	 update [AdditionalContactsInformation] set  [numRecOwner]=@numUserCntID where [numContactId]=@numRecID  and numDomainID=@numDomainID
end
else if @byteMode=4
begin
	 update [Cases] set  [numRecOwner]=@numUserCntID,bintModifiedDate=GETUTCDATE() where [numCaseId]=@numRecID  and numDomainID=@numDomainID
end
else if @byteMode=5
begin
	 update StagePercentageDetails set  numAssignTo=@numUserCntID,bintModifiedDate=GETUTCDATE() where numStageDetailsId=@numRecID  and numDomainID=@numDomainID
end
GO
