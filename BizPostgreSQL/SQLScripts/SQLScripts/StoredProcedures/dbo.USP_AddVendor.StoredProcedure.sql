/****** Object:  StoredProcedure [dbo].[USP_AddVendor]    Script Date: 07/26/2008 16:14:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj  
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_addvendor')
DROP PROCEDURE usp_addvendor
GO
CREATE PROCEDURE [dbo].[USP_AddVendor]    
@numDivisionId as numeric(9)=0,    
@numDomainID as numeric(9)=0,  
@numItemCode as numeric(9)=0,
@bitIsPrimaryVendor AS BIT=0,
@vcPartNo VARCHAR(100) = '',
@intMinQty INT = 0,
@monCost DECIMAL(20,5) = 0
as    
BEGIN    
	IF NOT EXISTS(SELECT * FROM Vendor WHERE numVendorID=@numDivisionId AND numItemCode=@numItemCode)    
	BEGIN
		INSERT INTO Vendor 
		(
			numVendorID
			,vcPartNo
			,numDomainID
			,monCost
			,numItemCode
			,intMinQty
		)    
		VALUES
		(
			@numDivisionId
			,@vcPartNo
			,@numDomainID
			,@monCost
			,@numItemCode
			,@intMinQty
		)    
	END

	IF @bitIsPrimaryVendor = 1 OR (SELECT COUNT(*) FROM Vendor WHERE numItemCode=@numItemCode) = 1
	BEGIN
		UPDATE Item SET numVendorID = @numDivisionId WHERE numItemCode=@numItemCode AND numDomainID=@numDomainID
	END
END
GO
