/****** Object:  StoredProcedure [dbo].[Reminder_Sel]    Script Date: 07/26/2008 16:14:33 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
/*
**  Fetches all Activities having Reminder notifications that have come due
**  within a window of time for a specified Resource (usually the Logged
**  OnUserName) by that Resource's ID.
*/
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='reminder_sel')
DROP PROCEDURE reminder_sel
GO
CREATE PROCEDURE [dbo].[Reminder_Sel]
	@LookAheadWindowEndTime	datetime,	-- a date/time before the current system time which defines the point past
							-- which outstanding Reminder notifications need to be shown to the end
							-- user.
	@ResourceID			integer		-- the key of the Resource to have reminders selected on behalf of.
AS
BEGIN
	-- Select the Activities that need Reminders displayed between Now and the Lookahead Window.
	SELECT
		[Activity].[AllDayEvent],
		[Activity].[Duration],
		[Activity].[ActivityDescription],
		[Activity].[ActivityID], 
		[Resource].[ResourceName], 
		[Activity].[StartDateTimeUtc], 
		[Activity].[Subject], 
		[Activity].[Location], 
		[Activity].[EnableReminder], 
		[Activity].[ReminderInterval], 
		[Activity].[ShowTimeAs], 
		[Activity].[Importance],
		[Activity].[Status],
		[Activity].[_ts],
		[Activity].[ItemId],
		[Activity].[ChangeKey]
	FROM 
		((Activity INNER JOIN ActivityResource ON Activity.[ActivityID] = ActivityResource.ActivityID) INNER JOIN Resource ON ActivityResource.ResourceID = Resource.[ResourceID]) 
	WHERE 
		( [Resource].[ResourceID] = @ResourceID ) AND 
		( [Activity].[StartDateTimeUtc] < @LookAheadWindowEndTime ) AND 
		( [Activity].[RecurrenceID] = (-999) ) AND
		( [Activity].[Status] < 3 ) AND 
		( [Activity].[EnableReminder] = 1 );	-- Must use '1' in T-SQL for bit column truth value instead of 'True'
END
GO
