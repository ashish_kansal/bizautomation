/****** Object:  StoredProcedure [dbo].[USP_FindWareHouse]    Script Date: 07/26/2008 16:15:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
---created by anoop jayaraj  
  
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_findwarehouse')
DROP PROCEDURE usp_findwarehouse
GO
CREATE PROCEDURE [dbo].[USP_FindWareHouse]
@numDivisionId as numeric(9)=0,
@numDomainID as numeric(9)=0,
@tintMode as TINYINT=0
as  
  
  
 
declare @WarehouseID as numeric(9)  
declare @ShipCountry as numeric(9)  
declare @ShipState as numeric(9)  
declare @ShipPostalCode as varchar(10)   

set @WarehouseID=0 
--IF @tintMode =0
--BEGIN
		if  @numDivisionId>0
		begin
		
			select @ShipCountry=numCountry,@ShipState=numState,@ShipPostalCode=vcPostalCode from AddressDetails where numRecordID=@numDivisionId  and numDomainID=@numDomainID AND tintAddressOf = 2 AND tintAddressType=2 and bitIsPrimary=1
			  
			if (isnumeric(@ShipPostalCode)=1 and @ShipState>0 and @ShipCountry>0)  
			begin  
			  if exists(select * from WareHouseDetails where numCountryID=@ShipCountry and numStateID=@ShipState and numDomainID=@numDomainID and @ShipPostalCode between numFromPinCode and numToPinCode)  
			   begin  
			  select top 1 @WarehouseID=numWareHouse  from WareHouseDetails where numCountryID=@ShipCountry and numStateID=@ShipState and numDomainID=@numDomainID and @ShipPostalCode between numFromPinCode and numToPinCode  
			   end  
			   else if exists(select * from WareHouseDetails where numCountryID=@ShipCountry and numStateID=@ShipState and numDomainID=@numDomainID)  
			   begin  
			  select top 1 @WarehouseID=numWareHouse  from WareHouseDetails where numCountryID=@ShipCountry and numStateID=@ShipState and numDomainID=@numDomainID   
			   end  
			   else if exists(select * from WareHouseDetails where numCountryID=@ShipCountry and numDomainID=@numDomainID)  
			   begin  
			  select top 1 @WarehouseID=numWareHouse  from WareHouseDetails where numCountryID=@ShipCountry and numDomainID=@numDomainID   
			   end  
			end  
			else   
			begin  
			 if exists(select * from WareHouseDetails where numCountryID=@ShipCountry and numStateID=@ShipState and numDomainID=@numDomainID and numStateID<>0)  
			   begin  
			  select top 1 @WarehouseID=numWareHouse  from WareHouseDetails where numCountryID=@ShipCountry and numStateID=@ShipState and numDomainID=@numDomainID and numStateID<>0  
			   end  
			   else if exists(select * from WareHouseDetails where numCountryID=@ShipCountry and numDomainID=@numDomainID)  
			   begin  
			  select top 1 @WarehouseID=numWareHouse  from WareHouseDetails where numCountryID=@ShipCountry and numDomainID=@numDomainID   
			   end  
			end  
		end

		if   @WarehouseID=0
		begin
			if ((select count(*) from Warehouses where numDomainID=@numDomainID)=1)
			begin
				select @WarehouseID=numWareHouseID from Warehouses where numDomainID=@numDomainID
			end
		end
		SELECT ISNULL(@WarehouseID,0)
--END 
--ELSE IF @tintMode=1 -- Find ware house based on shipping zip code of division
--BEGIN
--	select top 1 @ShipPostalCode=vcPostalCode from AddressDetails where numRecordID=@numDivisionId  and numDomainID=@numDomainID AND tintAddressOf = 2 AND tintAddressType=2
--	
--	select top 1 @WarehouseID=numWareHouse  from WareHouseDetails where numDomainID=@numDomainID and @ShipPostalCode between numFromPinCode and numToPinCode
--	
--	SELECT ISNULL(@WarehouseID,0)
--END
