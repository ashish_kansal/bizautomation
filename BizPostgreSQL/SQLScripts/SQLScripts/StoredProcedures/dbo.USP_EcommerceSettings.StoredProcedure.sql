/****** Object:  StoredProcedure [dbo].[USP_EcommerceSettings]    Script Date: 03/25/2009 16:46:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj              
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_ecommercesettings')
DROP PROCEDURE usp_ecommercesettings
GO
CREATE PROCEDURE [dbo].[USP_EcommerceSettings]              
@numDomainID as numeric(9),              
@vcPaymentGateWay as int ,                
@vcPGWUserId as varchar(100)='' ,                
@vcPGWPassword as varchar(100),              
@bitShowInStock as bit,              
@bitShowQOnHand as bit,
@bitCheckCreditStatus as BIT,
@numDefaultWareHouseID as numeric(9),
@numRelationshipId as numeric(9),
@numProfileId as numeric(9),
@bitHidePriceBeforeLogin	BIT,
@bitAuthOnlyCreditCard BIT=0,
@bitSendMail BIT = 1,
@vcGoogleMerchantID VARCHAR(1000)  = '',
@vcGoogleMerchantKey VARCHAR(1000) = '',
@IsSandbox BIT ,
@numAuthrizedOrderStatus NUMERIC,
@numSiteID numeric(18, 0),
@vcPaypalUserName VARCHAR(50)  = '',
@vcPaypalPassword VARCHAR(50)  = '',
@vcPaypalSignature VARCHAR(500)  = '',
@IsPaypalSandbox BIT,
@bitSkipStep2 BIT = 0,
@bitDisplayCategory BIT = 0,
@bitShowPriceUsingPriceLevel BIT = 0,
@bitEnableSecSorting BIT = 0,
@bitSortPriceMode   BIT=0,
@numPageSize    INT=15,
@numPageVariant  INT=6,
@bitAutoSelectWarehouse BIT =0,
@bitPreSellUp BIT = 0, 
@bitPostSellUp BIT = 0, 
@numDefaultClass NUMERIC(18,0) = 0,
@vcPreSellUp VARCHAR(200)=NULL,
@vcPostSellUp VARCHAR(200)=NULL,
@vcRedirectThankYouUrl VARCHAR(300)=NULL,
@tintPreLoginProceLevel TINYINT = 0,
@bitHideAddtoCart BIT=1,
@bitShowPromoDetailsLink BIT = 1,
@tintWarehouseAvailability TINYINT = 1,
@bitDisplayQtyAvailable BIT = 0,
@vcSalesOrderTabs VARCHAR(200)='',
@vcSalesQuotesTabs VARCHAR(200)='',
@vcItemPurchaseHistoryTabs VARCHAR(200)='',
@vcItemsFrequentlyPurchasedTabs VARCHAR(200)='',
@vcOpenCasesTabs VARCHAR(200)='',
@vcOpenRMATabs VARCHAR(200)='',
@bitSalesOrderTabs BIT=0,
@bitSalesQuotesTabs BIT=0,
@bitItemPurchaseHistoryTabs BIT=0,
@bitItemsFrequentlyPurchasedTabs BIT=0,
@bitOpenCasesTabs BIT=0,
@bitOpenRMATabs BIT=0,
@bitSupportTabs BIT=0,
@vcSupportTabs VARCHAR(200)='',
@bitElasticSearch BIT=0
as              
BEGIN	        
	IF NOT EXISTS(select 'col1' from eCommerceDTL where numDomainID=@numDomainID AND [numSiteId] = @numSiteID)
	BEGIN
		INSERT INTO eCommerceDTL
        (
			numDomainId,
            vcPaymentGateWay,
            vcPGWManagerUId,
            vcPGWManagerPassword,
            bitShowInStock,
            bitShowQOnHand,
            numDefaultWareHouseID,
            bitCheckCreditStatus,
            [numRelationshipId],
            [numProfileId],
            [bitHidePriceBeforeLogin],
            bitAuthOnlyCreditCard,
            bitSendEmail,
            vcGoogleMerchantID,
            vcGoogleMerchantKey,
            IsSandbox ,
            numAuthrizedOrderStatus,
			numSiteId,
			vcPaypalUserName,
			vcPaypalPassword,
			vcPaypalSignature,
			IsPaypalSandbox,
			bitSkipStep2,
			bitDisplayCategory,
			bitShowPriceUsingPriceLevel,
			bitEnableSecSorting,
			bitSortPriceMode,
			numPageSize,
			numPageVariant,
			bitAutoSelectWarehouse,
			[bitPreSellUp],
			[bitPostSellUp],
			numDefaultClass,
			vcPreSellUp,
			vcPostSellUp,
			vcRedirectThankYouUrl,
			tintPreLoginProceLevel,
			bitHideAddtoCart,
			bitShowPromoDetailsLink,
			tintWarehouseAvailability,
			bitDisplayQtyAvailable,
			bitElasticSearch
		)
		VALUES
		(
			@numDomainID,
            @vcPaymentGateWay,
            @vcPGWUserId,
            @vcPGWPassword,
            @bitShowInStock,
            @bitShowQOnHand,
            @numDefaultWareHouseID,
            @bitCheckCreditStatus,
            @numRelationshipId,
            @numProfileId,
            @bitHidePriceBeforeLogin,
            @bitAuthOnlyCreditCard,
            @bitSendMail,
            @vcGoogleMerchantID,
            @vcGoogleMerchantKey ,
            @IsSandbox,
            @numAuthrizedOrderStatus,
			@numSiteID,
			@vcPaypalUserName,
			@vcPaypalPassword,
			@vcPaypalSignature,
			@IsPaypalSandbox,
			@bitSkipStep2,
			@bitDisplayCategory,
			@bitShowPriceUsingPriceLevel,
			@bitEnableSecSorting,
			@bitSortPriceMode,
			@numPageSize,
			@numPageVariant,
			@bitAutoSelectWarehouse,
			@bitPreSellUp,
			@bitPostSellUp,
			@numDefaultClass,
			@vcPreSellUp,
			@vcPostSellUp,
			@vcRedirectThankYouUrl,
			@tintPreLoginProceLevel,
			@bitHideAddtoCart,
			@bitShowPromoDetailsLink,
			@tintWarehouseAvailability,
			@bitDisplayQtyAvailable,
			@bitElasticSearch
        )

		
		IF(@bitElasticSearch = 1)
		BEGIN
			IF(NOT EXISTS(SELECT 1 FROM eCommerceDTL where numSiteId <> @numSiteID AND numDomainId = @numDomainID AND bitElasticSearch = 1))
			BEGIN
				INSERT INTO ElasticSearchBizCart 
				(
					numDomainID
					,vcValue
					,vcAction
					,vcModule
					--,numSiteID
				)
				SELECT 
					@numDomainID
					,numItemID
					,'Insert'
					,'Item'
					--,@numSiteID  
				FROM 
					SiteCategories
				INNER JOIN
					ItemCategory
				ON
					SiteCategories.numCategoryID = ItemCategory.numCategoryID
				INNER JOIN
					Item
				ON
					ItemCategory.numItemID = Item.numItemCode
				WHERE 
					SiteCategories.numSiteID = @numSiteID
					AND ISNULL(Item.IsArchieve,0) =0
				GROUP BY
					numItemID
			END
		END

		IF ISNULL((SELECT numDefaultSiteID FROM DOmain WHERE numDomainId=@numDomainID ),0) = 0
		BEGIN
			UPDATE Domain SET numDefaultSiteID=SCOPE_IDENTITY() WHERE numDomainId=@numDomainID
		END
	END
	ELSE
	BEGIN
		IF(@bitElasticSearch = 1)
		BEGIN
			INSERT INTO ElasticSearchBizCart 
			(
				numDomainID
				,vcValue
				,vcAction
				,vcModule
				--,numSiteID
			)
			SELECT 
				@numDomainID
				,numItemID
				,'Insert'
				,'Item'
				--,@numSiteID  
			FROM 
				SiteCategories
			INNER JOIN
				ItemCategory
			ON
				SiteCategories.numCategoryID = ItemCategory.numCategoryID
			INNER JOIN
				Item
			ON
				ItemCategory.numItemID = Item.numItemCode
			WHERE 
				SiteCategories.numSiteID = @numSiteID
				AND ISNULL(Item.IsArchieve,0)=0
			GROUP BY
				numItemID
		END
		--ELSE IF(EXISTS(SELECT 1 FROM eCommerceDTL where numSiteId = @numSiteID AND numDomainId = @numDomainID AND bitElasticSearch = 1))
		--BEGIN
		--	DELETE FROM ElasticSearchBizCart WHERE numDomainID = @numDomainID --And numSiteID = @numSiteID
		--END	

		UPDATE 
			eCommerceDTL                                   
		SET              
			vcPaymentGateWay=@vcPaymentGateWay,                
			vcPGWManagerUId=@vcPGWUserId ,                
			vcPGWManagerPassword= @vcPGWPassword,              
			bitShowInStock=@bitShowInStock ,               
			bitShowQOnHand=@bitShowQOnHand,        
			bitCheckCreditStatus=@bitCheckCreditStatus ,
			[bitHidePriceBeforeLogin]=@bitHidePriceBeforeLogin,
			bitAuthOnlyCreditCard=@bitAuthOnlyCreditCard,
			bitSendEmail = @bitSendMail,
			vcGoogleMerchantID = @vcGoogleMerchantID ,
			vcGoogleMerchantKey =  @vcGoogleMerchantKey ,
			IsSandbox = @IsSandbox,
			numAuthrizedOrderStatus = @numAuthrizedOrderStatus,
			numSiteId = @numSiteID,
			vcPaypalUserName = @vcPaypalUserName ,
			vcPaypalPassword = @vcPaypalPassword,
			vcPaypalSignature = @vcPaypalSignature,
			IsPaypalSandbox = @IsPaypalSandbox,
			bitSkipStep2 = @bitSkipStep2,
			bitDisplayCategory = @bitDisplayCategory,
			bitShowPriceUsingPriceLevel = @bitShowPriceUsingPriceLevel,
			bitEnableSecSorting = @bitEnableSecSorting,
			bitSortPriceMode=@bitSortPriceMode,
			numPageSize=@numPageSize,
			numPageVariant=@numPageVariant,
			bitAutoSelectWarehouse=@bitAutoSelectWarehouse,
			[bitPreSellUp] = @bitPreSellUp,
			[bitPostSellUp] = @bitPostSellUp,
			vcPreSellUp=@vcPreSellUp,
			vcPostSellUp=@vcPostSellUp,
			vcRedirectThankYouUrl=@vcRedirectThankYouUrl,
			bitHideAddtoCart=@bitHideAddtoCart,
			bitShowPromoDetailsLink=@bitShowPromoDetailsLink,
			tintWarehouseAvailability=@tintWarehouseAvailability,
			bitDisplayQtyAvailable=@bitDisplayQtyAvailable,
			bitElasticSearch=@bitElasticSearch
		WHERE 
			numDomainId=@numDomainID 
			AND [numSiteId] = @numSiteID
		IF(@numDefaultWareHouseID>0)
		BEGIN
			UPDATE 
				eCommerceDTL                                   
			SET 
				numDefaultWareHouseID =@numDefaultWareHouseID
			WHERE 
				numDomainId=@numDomainID 
				AND [numSiteId] = @numSiteID
		END
		IF(@numRelationshipId>0)
		BEGIN
			UPDATE 
				eCommerceDTL                                   
			SET 
				numRelationshipId =@numRelationshipId
			WHERE 
				numDomainId=@numDomainID 
				AND [numSiteId] = @numSiteID
		END
		IF(@numDefaultClass>0)
		BEGIN
			UPDATE 
				eCommerceDTL                                   
			SET 
				numDefaultClass =@numDefaultClass
			WHERE 
				numDomainId=@numDomainID 
				AND [numSiteId] = @numSiteID
		END
		IF(@numProfileId>0)
		BEGIN
			UPDATE 
				eCommerceDTL                                   
			SET 
				numProfileId =@numProfileId
			WHERE 
				numDomainId=@numDomainID 
				AND [numSiteId] = @numSiteID
		END
		IF(@tintPreLoginProceLevel>0)
		BEGIN
			UPDATE 
				eCommerceDTL                                   
			SET 
				tintPreLoginProceLevel =@tintPreLoginProceLevel
			WHERE 
				numDomainId=@numDomainID 
				AND [numSiteId] = @numSiteID
		END

	END

	UPDATE
		Domain
	SET
		vcSalesOrderTabs=@vcSalesOrderTabs,
		vcSalesQuotesTabs=@vcSalesQuotesTabs,
		vcItemPurchaseHistoryTabs=@vcItemPurchaseHistoryTabs,
		vcItemsFrequentlyPurchasedTabs=@vcItemsFrequentlyPurchasedTabs,
		vcOpenCasesTabs=@vcOpenCasesTabs,
		vcOpenRMATabs=@vcOpenRMATabs,
		vcSupportTabs=@vcSupportTabs,
		bitSalesOrderTabs=@bitSalesOrderTabs,
		bitSalesQuotesTabs=@bitSalesQuotesTabs,
		bitItemPurchaseHistoryTabs=@bitItemPurchaseHistoryTabs,
		bitItemsFrequentlyPurchasedTabs=@bitItemsFrequentlyPurchasedTabs,
		bitOpenCasesTabs=@bitOpenCasesTabs,
		bitOpenRMATabs=@bitOpenRMATabs,
		bitSupportTabs=@bitSupportTabs
	WHERE
		numDomainId=@numDomainID
END
GO