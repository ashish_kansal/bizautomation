/****** Object:  StoredProcedure [dbo].[USP_GetAuthoritativeBizDocsCount]    Script Date: 07/26/2008 16:16:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getauthoritativebizdocscount')
DROP PROCEDURE usp_getauthoritativebizdocscount
GO
CREATE PROCEDURE [dbo].[USP_GetAuthoritativeBizDocsCount]
(@numDivisionID as numeric(9)=0,
@numContactId as numeric(9)=0,
@numDomainId as numeric(9)=0)
As
Begin
if @numDivisionID>0
	Begin
		select  Count(*) from  OpportunityMaster  Opp
		inner join OpportunityBizDocs OppBiz on Opp.numOppId=OppBiz.numOppId
		Where (OppBiz.numBizDocId in (Select numAuthoritativePurchase  from AuthoritativeBizDocs Where numDomainId=@numDomainId)
		Or OppBiz.numBizDocId in (Select numAuthoritativeSales  from AuthoritativeBizDocs Where numDomainId=@numDomainId))
		And Opp.numDivisionID=@numDivisionID
	End
If @numContactId>0
	Begin
		select  Count(*) from  OpportunityMaster  Opp
		inner join OpportunityBizDocs OppBiz on Opp.numOppId=OppBiz.numOppId
		Where (OppBiz.numBizDocId in (Select numAuthoritativePurchase  from AuthoritativeBizDocs Where numDomainId=@numDomainId)
		Or OppBiz.numBizDocId in (Select numAuthoritativeSales  from AuthoritativeBizDocs Where numDomainId=@numDomainId))
		And Opp.numContactId=@numContactId
	End
End
GO
