
GO
/****** Object:  StoredProcedure [dbo].[USP_GetRecurringTemplateID]    Script Date: 03/25/2009 15:11:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Pratik Vasani
-- Create date: 11/Dec/2008
-- Description:	Get the recurring id for the opportunity Id specified.
-- =============================================
-- exec [dbo].[USP_GetRecurringTemplateID] 1147
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getrecurringtemplateid')
DROP PROCEDURE usp_getrecurringtemplateid
GO
CREATE PROCEDURE [dbo].[USP_GetRecurringTemplateID]
               @numOppId AS NUMERIC(9)
AS
  BEGIN
--    SELECT OM.numOppId,
--           numRecurringid,
--           bitRecurringZeroAmt,
--           ISNULL(tintRecurringType,0) tintRecurringType,
--           ISNULL(bitBillingTerms,0) bitBillingTerms,
--           ISNULL(numBillingDays,0) intNetDays
--    FROM   opportunitymaster OM
--    LEFT OUTER JOIN [OpportunityRecurring] OPR
--					ON OPR.numOppId = OM.numOppId
--    WHERE  
--    OM.numOppId = @numOppId
    
    SELECT ROW_NUMBER() OVER( ORDER BY OPR.numDomainId ) AS  ID,
		   numOppRecID,
		   OPR.numOppId,
		   ISNULL(OPR.numOppBizDocID,0) numOppBizDocID,
		   numRecurringId,
		   tintRecurringType,
		   dbo.FormatedDateFromDate(dtRecurringDate,OPR.numDomainID) dtRecurringDate,
		   bitRecurringZeroAmt,
		   numNoTransactions,
		   OPR.bitBillingTerms,
		   numBillingDays,
		   fltBreakupPercentage,
		   OPR.numDomainID,
		   ISNULL(OBD.vcBizDocID,'') vcBizDocID,
		   --'Net-' + ISNULL(dbo.GetListIemName(OPR.numBillingDays),'0') AS Billingdays
		   ISNULL((SELECT numNetDueInDays FROM dbo.BillingTerms WHERE numTermsID = OM.intBillingDays),'0') AS Billingdays
		   FROM dbo.OpportunityRecurring OPR INNER JOIN dbo.OpportunityMaster OM ON  
		   OPR.numOppID = OM.numOppID
		   LEFT JOIN dbo.OpportunityBizDocs OBD ON OBD.numOppBizDocsID = OPR.numOppBizDocID
		   WHERE OPR.numOppId=@numOppId 
				and tintRecurringType<>4 --Do not include Deferred BizDoc
    
  END
