/****** Object:  StoredProcedure [dbo].[usp_ImportItemAmountXML]    Script Date: 07/26/2008 16:18:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_importitemamountxml')
DROP PROCEDURE usp_importitemamountxml
GO
CREATE PROCEDURE [dbo].[usp_ImportItemAmountXML]
	@numOppId NUMERIC
--
AS
BEGIN
	DECLARE @numTotalAmt NUMERIC
	SELECT @numTotalAmt=sum(numUnitHour*monPrice) FROM OpportunityItems WHERE numOppId=@numOppId
	UPDATE OpportunityMaster SET monPAmount=@numTotalAmt WHERE numOppId=@numOppId
END
GO
