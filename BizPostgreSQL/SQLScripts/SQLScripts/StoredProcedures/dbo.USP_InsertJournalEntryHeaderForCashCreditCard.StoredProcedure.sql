/****** Object:  StoredProcedure [dbo].[USP_InsertJournalEntryHeaderForCashCreditCard]    Script Date: 07/26/2008 16:19:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created by Siva        
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_insertjournalentryheaderforcashcreditcard')
DROP PROCEDURE usp_insertjournalentryheaderforcashcreditcard
GO
CREATE PROCEDURE [dbo].[USP_InsertJournalEntryHeaderForCashCreditCard]                  
@datEntry_Date as datetime,                    
@numAmount as DECIMAL(20,5),                  
@numJournal_Id as numeric(9)=0,              
@numDomainId as numeric(9)=0,      
@numCashCreditCardId as numeric(9)=0,  
@numUserCntID as numeric(9)=0                 
As                    
Begin                    
	If @numJournal_Id=0            
		Begin            
			Insert into General_Journal_Header(datEntry_Date,numAmount,numDomainId,numCashCreditCardId,numCreatedBy,datCreatedDate) Values(@datEntry_Date,@numAmount,@numDomainId,@numCashCreditCardId,@numUserCntID,getutcdate())                  
			Set @numJournal_Id = SCOPE_IDENTITY()                  
			Select  @numJournal_Id              
		End            
	           
End
GO
