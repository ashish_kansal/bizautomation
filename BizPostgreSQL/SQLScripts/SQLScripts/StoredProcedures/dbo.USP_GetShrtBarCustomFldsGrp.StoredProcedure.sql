/****** Object:  StoredProcedure [dbo].[USP_GetShrtBarCustomFldsGrp]    Script Date: 07/26/2008 16:18:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getshrtbarcustomfldsgrp')
DROP PROCEDURE usp_getshrtbarcustomfldsgrp
GO
CREATE PROCEDURE [dbo].[USP_GetShrtBarCustomFldsGrp]  
@numGroupId numeric,  
@numDomainId numeric  ,
@numContactId numeric  =0,
@numTabId numeric(9)
as  
select * from shortcutbar where bitdefault=0 and numTabId=@numTabId  and numGroupId=@numGroupId 
and numDomainId=@numDomainId  and numContactId =@numContactId
GO
