/****** Object:  StoredProcedure [dbo].[usp_getCustomReportResultSetForMyReport]    Script Date: 07/26/2008 16:17:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created By: Debasish Nag          
--Created On: 9th Nov 2005          
--Purpose: To Return the Custom Reports ResultSet for My Reports          
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getcustomreportresultsetformyreport')
DROP PROCEDURE usp_getcustomreportresultsetformyreport
GO
CREATE PROCEDURE [dbo].[usp_getCustomReportResultSetForMyReport]    
 @numCustReportID Numeric,          
 @numUserCntID numeric,  
 @numRefreshTimeInterval Numeric          
AS            
BEGIN          
 DECLARE @sCustomReportQuickQuery NVarchar(4000)      
 DECLARE @vcCustomReportQueryMiddle NVarchar(4000)          
 DECLARE @sCustomReportQuickQueryTrailer NVarchar(4000)          
 DECLARE @vcAggregateReportColumns Nvarchar(1000)          
 DECLARE @vcReportTypeconfig Nvarchar(500)          
 DECLARE @bitRecordCountSummation Bit                         
 DECLARE @ReportType Numeric             
 DECLARE @CustomFields Bit          
          
 SELECT @sCustomReportQuickQuery = SUBSTRING(vcReportQuickQuery,1,4000),      
 @vcCustomReportQueryMiddle = SUBSTRING(vcReportQuickQuery,4001,4000),    
 @sCustomReportQuickQueryTrailer = SUBSTRING(vcReportQuickQuery,8001,DATALENGTH(vcReportQuickQuery)),       
 @vcAggregateReportColumns = vcReportSummationOrder,    
 @vcReportTypeconfig = vcReportTypeconfig    
 FROM CustRptConfigMaster          
 WHERE numCustReportID = @numCustReportID          
 AND numCreatedBy = @numUserCntID       
    
 DECLARE @iDoc int                              
 EXECUTE sp_xml_preparedocument @iDoc OUTPUT, @vcReportTypeconfig    
 SELECT @ReportType = ReportType, @CustomFields = CustomFields FROM OpenXML(@iDoc, '/ReportTypeConfig', 2)                                                
 WITH                                                
 (                                        
  ReportType Numeric,                          
  CustomFields Bit                            
 )    
 --PRINT 'ReportType: ' + Cast(@ReportType As NVarchar)    
 --PRINT 'CustomFields: ' + Cast(@CustomFields As NVarchar)    
    
 EXECUTE sp_xml_removedocument @iDoc    
        
 IF @ReportType  = 2         
 BEGIN          
  EXEC ('usp_OppItemCustomFields ' + @numRefreshTimeInterval)          
 END          
 ELSE IF  @ReportType  = 1    
 BEGIN          
  EXEC ('usp_OrganizationContactsCustomFields ' + @numRefreshTimeInterval)             
 END          
          
 EXEC (@sCustomReportQuickQuery + @vcCustomReportQueryMiddle + @sCustomReportQuickQueryTrailer)      
 EXEC('SELECT ''' +  @vcAggregateReportColumns + ''' AS AggregateReportColumns')       
      
 SELECT bitRecordCountSummation AS 'RecordCountSummation' FROM CustRptConfigLayoutAndStdFilters      
 WHERE numCustReportID = @numCustReportID          
END
GO
