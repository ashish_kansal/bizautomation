/****** Object:  StoredProcedure [dbo].[USP_RecurringTemplateDetails]    Script Date: 07/26/2008 16:20:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_recurringtemplatedetails')
DROP PROCEDURE usp_recurringtemplatedetails
GO
CREATE PROCEDURE [dbo].[USP_RecurringTemplateDetails]  
@numRecurringId as numeric(9)=0,
@numDomainId as numeric(9)=0
 
As  
Begin
Select numRecurringId,
	   varRecurringTemplateName,
	   chrIntervalType,
	   tintIntervalDays,
	   tintMonthlyType,
	   tintFirstDet,
	   tintWeekDays,
	   tintMonths,
	   dtStartDate,
	   dtEndDate,
	   bitEndType,
	   bitRecurring,
	   bitEndTransactionType,
	   numNoTransaction,
	   numDomainId,
	   ISNULL(intBillingBreakup,0) intBillingBreakup,
	   vcBreakupPercentage,
	   ISNULL(bitBillingTerms,0) bitBillingTerms,
	   ISNULL(numBillingDays,0) numBillingDays,
	   bitBizDocBreakup
From RecurringTemplate Where numRecurringId=@numRecurringId And numDomainId=@numDomainId

End
GO
