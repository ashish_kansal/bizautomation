/****** Object:  StoredProcedure [dbo].[USP_SaveDefaultFilter]    Script Date: 07/26/2008 16:21:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'usp_savedefaultfilter' ) 
    DROP PROCEDURE usp_savedefaultfilter
GO
CREATE PROCEDURE [dbo].[USP_SaveDefaultFilter]
    @numDomainId AS NUMERIC(9),
    @numUserCntId AS NUMERIC(9),
    @numRelation AS NUMERIC(9),
    @FilterId AS NUMERIC(9),
    @bitDefault AS BIT,
    @FormId AS NUMERIC(9),
    @GroupId AS NUMERIC(9),
    @vcFilter AS VARCHAR(4000),
    @bitFlag AS BIT = 0  -- 0 = set initial page and default filter id, 1= Update additinal filters of page to vcFilter
AS 
    IF @bitFlag = 0 
        BEGIN

            IF @bitDefault = 1
                AND ( @FormId = 34
                      OR @FormId = 35
                      OR @FormId = 36
                      OR @FormId = 10
                    ) 
                BEGIN  
                    UPDATE  dbo.RelationsDefaultFilter
                    SET     bitInitialPage = 0
                    WHERE   numDomainId = @numDomainId
                            AND numUserCntId = @numUserCntId
                            AND numFormId IN ( 34, 35, 36,10 )
                END  
            IF @bitDefault = 1
                AND ( @FormId = 38
                      OR @FormId = 39
                      OR @FormId = 40
                      OR @FormId = 41
                    ) 
                BEGIN
                    UPDATE  dbo.RelationsDefaultFilter
                    SET     bitInitialPage = 0
                    WHERE   numDomainId = @numDomainId
                            AND numUserCntId = @numUserCntId
                            AND numFormId IN ( 38, 39, 40 , 41 )
                END
            IF NOT EXISTS ( SELECT  *
                            FROM    RelationsDefaultFilter
                            WHERE   numDomainId = @numDomainId
                                    AND numUserCntId = @numUserCntId
                                    AND numFormId = @FormId ) 
                BEGIN
                    INSERT  INTO [RelationsDefaultFilter]
                            (
                              [numRelationId],
                              [numDomainId],
                              [numUserCntId],
                              [numFilterId],
                              [bitInitialPage],
                              numFormId,
                              tintGroupid
                            )
                    VALUES  (
                              @numRelation,
                              @numDomainId,
                              @numUserCntId,
                              @FilterId,
                              @bitDefault,
                              @FormId,
                              @GroupId
                            )
                END
            ELSE 
                BEGIN
                    UPDATE  RelationsDefaultFilter
                    SET     
                            bitInitialPage = @bitDefault
                    WHERE   numDomainId = @numDomainId
                            AND numUserCntId = @numUserCntId
                            AND numformId = @FormId
                            AND tintGroupId = @GroupId
                END
        END
    ELSE 
        BEGIN
            IF NOT EXISTS ( SELECT  *
                            FROM    RelationsDefaultFilter
                            WHERE   numDomainId = @numDomainId
                                    AND numUserCntId = @numUserCntId
                                    AND numFormId = @FormId  AND tintGroupId = @GroupId AND numRelationId = @numRelation) 
                BEGIN
                    INSERT  INTO [RelationsDefaultFilter]
                            (
                              [numRelationId],
                              [numDomainId],
                              [numUserCntId],
                              [numFilterId],
                              [bitInitialPage],
                              numFormId,
                              tintGroupid,vcFilter
                            )
                    VALUES  (
                              @numRelation,
                              @numDomainId,
                              @numUserCntId,
                              @FilterId,
                              @bitDefault,
                              @FormId,
                              @GroupId,@vcFilter 
                            )
                END
            ELSE 
                BEGIN
                    UPDATE  RelationsDefaultFilter
                    SET     numRelationId = @numRelation,
                            tintGroupid = @GroupId,
                            vcFilter = @vcFilter,
							numFilterID= @FilterId
                    WHERE   numDomainId = @numDomainId
                            AND numUserCntId = @numUserCntId
                            AND numformId = @FormId AND tintGroupId = @GroupId AND numRelationId = @numRelation
                END
        END
GO
