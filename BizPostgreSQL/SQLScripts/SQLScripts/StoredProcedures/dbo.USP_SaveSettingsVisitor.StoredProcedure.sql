/****** Object:  StoredProcedure [dbo].[USP_SaveSettingsVisitor]    Script Date: 07/26/2008 16:21:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_savesettingsvisitor')
DROP PROCEDURE usp_savesettingsvisitor
GO
CREATE PROCEDURE [dbo].[USP_SaveSettingsVisitor]  
@MinimumPages as varchar,        
@ReportType as varchar      
  
as        
        
delete TrackSettingsVisitor  
       
insert into TrackSettingsVisitor (numMinPages,numReportType)        
values(@MinimumPages,@ReportType)
GO
