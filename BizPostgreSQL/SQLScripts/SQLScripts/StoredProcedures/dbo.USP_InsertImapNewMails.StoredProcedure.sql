/****** Object:  StoredProcedure [dbo].[USP_InsertImapNewMails]    Script Date: 07/26/2008 16:19:07 ******/

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_insertimapnewmails')
DROP PROCEDURE usp_insertimapnewmails
GO
Create PROCEDURE [dbo].[USP_InsertImapNewMails]            
@numUserCntId as numeric(9),            
@numDomainID as numeric(9),            
@numUid NUMERIC(9),
@vcSubject VARCHAR(500),
@vcBody TEXT,
@vcBodyText text,
@dtReceivedOn datetime,
@vcSize varchar(50),
@bitIsRead bit,
@bitHasAttachments bit,
@vcFromName VARCHAR(5000),
@vcToName VARCHAR(5000),
@vcCCName VARCHAR(5000),
@vcBCCName VARCHAR(5000),
@vcAttachmentName VARCHAR(500),
@vcAttachmenttype VARCHAR(500),
@vcNewAttachmentName VARCHAR(500),
@vcAttachmentSize VARCHAR(500),
@numNodeId NUMERIC(18,0)=0
as             
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
SET CONCAT_NULL_YIELDS_NULL ON
SET NUMERIC_ROUNDABORT OFF
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON

                                        
DECLARE @Identity AS NUMERIC(9)              
            
if (select count(*) from EmailHistory where numUserCntId = @numUserCntId             
   and numDomainid=@numDomainID  and numUid=@numUid AND numNodeId=@numNodeId)=0            
  begin      
             --Inser into Email Master
             SET @vcToName = @vcToName + '#^#'
             SET @vcFromName = @vcFromName + '#^#'
             SET @vcCCName = @vcCCName + '#^#'
             SET @vcBCCName = @vcBCCName + '#^#'

            -- PRINT 'ToName' + @ToName 
             EXECUTE USP_InsertEmailMaster @vcToName ,@numDomainID
             EXECUTE USP_InsertEmailMaster @vcFromName ,@numDomainID
             EXECUTE USP_InsertEmailMaster @vcCCName ,@numDomainID
             EXECUTE USP_InsertEmailMaster @vcBCCName ,@numDomainID 

             -- Insert Into Email History
                SET ARITHABORT ON
                INSERT  INTO EmailHistory
                        (
                          vcSubject,
                          vcBody,
                          vcBodyText,
                          bintCreatedOn,
                          bitHasAttachments,
                          vcItemId,
                          vcChangeKey,
                          bitIsRead,
                          vcSize,
                          chrSource,
                          tinttype,
                          vcCategory,
                          dtReceivedOn,
                          numDomainid,
                          numUserCntId,
                          numUid,IsReplied,
						  numNodeId
                        )
                VALUES  (
                          @vcSubject,
                          @vcBody,
                          dbo.fn_StripHTML(@vcBodyText),
                          GETUTCDATE(),
                          @bitHasAttachments,
                          '',
                          '',
                          @bitIsRead,
                          CONVERT(VARCHAR(200), @vcSize),
                          'I',
                          1,
                          '',
                          @dtReceivedOn,
                          @numDomainid,
                          @numUserCntId,
                          @numUid,0,
						  @numNodeId
                        )                        
                SET @Identity = SCOPE_IDENTITY()        
                
				
--				To increase performance of Select
                UPDATE  [EmailHistory]
                SET     vcFrom = dbo.fn_GetNewvcEmailString(@vcFromName,@numDomainID),	 --dbo.GetEmaillName(numEmailHstrID, 4),
                        [vcTo] = dbo.fn_GetNewvcEmailString(@vcToName,@numDomainID),    --dbo.GetEmaillName(numEmailHstrID, 1),
                        [vcCC] = dbo.fn_GetNewvcEmailString(@vcCCName,@numDomainID),	 --dbo.GetEmaillName(numEmailHstrID, 3)
                        [vcBCC]= dbo.fn_GetNewvcEmailString(@vcBCCName,@numDomainID)
                WHERE   [numEmailHstrID] = @Identity

				IF(SELECT vcFrom FROM EmailHistory WHERE [numEmailHstrID]=@Identity) IS NOT NULL 
				BEGIN
					update EmailHistory set numEmailId=(SELECT TOP 1 Item FROM dbo.DelimitedSplit8K(vcFrom,'$^$')) 
					where [numEmailHstrID]=@Identity	
				END

                IF @bitHasAttachments = 1 
                    BEGIN                    
                        EXEC USP_InsertAttachment @vcAttachmentName, '',
                            @vcAttachmentType, @Identity, @vcNewAttachmentName,@vcAttachmentSize                       
                    END 
    END            
GO
    