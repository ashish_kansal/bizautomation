/****** Object:  StoredProcedure [dbo].[usp_GetCampaignSalesPersonel]    Script Date: 07/26/2008 16:16:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getcampaignsalespersonel')
DROP PROCEDURE usp_getcampaignsalespersonel
GO
CREATE PROCEDURE [dbo].[usp_GetCampaignSalesPersonel]  
        @numDomainID numeric,  
        @numUserCntID numeric,    
        @tintRights numeric=1    
  
--    
AS    
BEGIN    
         IF @tintRights=1   
        -- ALL RECORDS RIGHTS    
        BEGIN    
  Select AI.vcFirstName + ' ' + AI.vcLastName as SalesPeronel,  
  12 as InPipeline, 10 as Closed  
   from AdditionalContactsInformation AI  
  JOIN  DivisionMaster DM  
  ON DM.numDivisionID = AI.numDivisionID  
  WHERE DM.numDomainID = @numDomainID  
  
  
    
    
        END    
    
         IF @tintRights=2  
        -- OWNER RECORDS RIGHTS    
        BEGIN    
  
    
  Select AI.vcFirstName + ' ' + AI.vcLastName as SalesPersonel,  
  12 as InPipeline, 10 as Closed  
   from AdditionalContactsInformation AI  
  JOIN  DivisionMaster DM  
  ON DM.numDivisionID = AI.numDivisionID 
   
  WHERE DM.numDomainID = @numDomainID  and Dm.numRecOwner=@numUserCntID
  
  
      END    
    
         IF @tintRights=3    
        -- TERRITORY RECORDS RIGHTS    
        BEGIN    
Select AI.vcFirstName + ' ' + AI.vcLastName as SalesPersonel,  
  12 as InPipeline, 10 as Closed  
   from AdditionalContactsInformation AI  
  JOIN  DivisionMaster DM  
  ON DM.numDivisionID = AI.numDivisionID  
  WHERE DM.numDomainID = @numDomainID  
  
 END    
    
END
GO
