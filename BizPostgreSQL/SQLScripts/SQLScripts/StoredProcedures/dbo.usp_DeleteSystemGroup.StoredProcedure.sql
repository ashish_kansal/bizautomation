/****** Object:  StoredProcedure [dbo].[usp_DeleteSystemGroup]    Script Date: 07/26/2008 16:15:43 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_deletesystemgroup')
DROP PROCEDURE usp_deletesystemgroup
GO
CREATE PROCEDURE [dbo].[usp_DeleteSystemGroup]
	@numGroupID INT  
-- 
AS
	BEGIN
		DELETE FROM AuthenticationGroupMaster
		WHERE numGroupID = @numGroupID
	END
GO
