--created by anoop Jayaraj
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_addmoneyoutbill')
DROP PROCEDURE usp_addmoneyoutbill
GO
CREATE PROCEDURE USP_AddMoneyOutBill
    @numPaymentMethod as numeric(9),
    @monAmount as DECIMAL(20,5),
    @vcReference as varchar(200),
    @vcMemo as varchar(50),
    @numExpenseAccount as numeric(9),
    @numDivisionID as numeric(9),
    @numDomainID as numeric(9),
    @UserContactId as numeric(9),
    @dtDueDate as datetime,
    @numCurrencyID as numeric(9),
    @numBizDocsPaymentDetId AS NUMERIC(9) = 0 output,
    @fltExchangeRate FLOAT = 0 OUTPUT,
    @numEmbeddedCostID NUMERIC(9)=0, --optional , will be used only when posting bill for embedded cost
    @tintPaymentType AS TINYINT=2,--2 for regular bill, 4 for bill against liability account
    @numLiabilityAccount NUMERIC(9)=NULL,
    @numCommissionID NUMERIC(9)=0,
    @numOppBizDocID NUMERIC(9)=0,
    @numClassID NUMERIC(9)=NULL,
    @numProjectID NUMERIC(9)=NULL,
	@numContactID NUmeriC(9)=NULL,
	@numTermsID		NUMERIC(18,0) = NULL,
	@dtBillDate		DATETIME 
as 

--IF  @tintPaymentType=5
--BEGIn
--	Select @numCurrencyID=isnull(OPP.numCurrencyID,0) From OpportunityMaster OPP
--			join OpportunityBizDocs oppBizDoc on  oppBizDoc.numOppId=OPP.numOppId
--			Where numOppBizDocsId=@numOppBizDocID and  OPP.numDomainID=@numDomainID  
--END   

	if @numCurrencyID = 0 
        set @fltExchangeRate = 1
    else 
        set @fltExchangeRate = dbo.GetExchangeRate(@numDomainID,@numCurrencyID)
     
	

    Insert  into OpportunityBizDocsDetails
            (
              numBizDocsId,
              numPaymentMethod,
              monAmount,
              numDomainId,
              vcReference,
              vcMemo,
              bitIntegratedToAcnt,
              bitAuthoritativeBizDocs,
              bitDeferredIncome,
              numCreatedBy,
              dtCreationDate,
              numExpenseAccount,
              numDivisionID,
              numCurrencyID,
              fltExchangeRate,
              tintPaymentType,
              numLiabilityAccount,
              numCommissionID,
              numClassID,
              numProjectID,numContactID,numTermsID,dtBillDate
            )
    Values  (
              @numOppBizDocID,
              @numPaymentMethod,
              @monAmount,
              @numDomainId,
              @vcReference,
              @vcMemo,
              0,
              1,
              0,
              @UserContactId,
              getutcdate(),
              @numExpenseAccount,
              @numDivisionID,
              @numCurrencyID,
              @fltExchangeRate,
              @tintPaymentType,
              @numLiabilityAccount,
              @numCommissionID,
              @numClassID,
              @numProjectID,@numContactID,@numTermsID,@dtBillDate
            )

    SET @numBizDocsPaymentDetId = SCOPE_IDENTITY() ;

    Insert  into OpportunityBizDocsPaymentDetails
            (
              numBizDocsPaymentDetId,
              monAmount,
              dtDueDate,
              bitIntegrated
            )
    Values  (
              @numBizDocsPaymentDetId,
              @monAmount,
              @dtDueDate,
              0
            ) 
	-- will be used only when posting bill for embedded cost            
	IF @numEmbeddedCostID >0
	BEGIN
		UPDATE [EmbeddedCost] SET [numBizDocsPaymentDetId] = @numBizDocsPaymentDetId WHERE [numEmbeddedCostID] =@numEmbeddedCostID
	END


--Payroll : Update BizDocComission,opportunitybizdocsdetails and OpportunityBizDocsPaymentDetails
If @tintPaymentType=6
BEGIN
--Update OpportunityBizDocsPaymentDetails set bitIntegrated=1 where bitIntegrated=0 and 
--		numBizDocsPaymentDetId in (select numBizDocsPaymentDetId from opportunitybizdocsdetails where 
--			numDomainId = @numDomainId AND bitAuthoritativeBizDocs = 1	
--			and numContactID=@numContactID and bitIntegratedToAcnt=0 and tintPaymentType = 5 and numBizDocsId > 0)

Update BC set bitCommisionPaid=1,numBizDocsPaymentDetId=@numBizDocsPaymentDetId 
   FROM OpportunityMaster Opp 
		INNER JOIN OpportunityBizDocs oppBiz on oppBiz.numOppId=opp.numOppId 
        INNER JOIN BizDocComission BC on BC.numOppBizDocId=oppBiz.numOppBizDocsId and BC.numUserCntId=@numContactID
   where Opp.numDomainId=@numDomainId and
   BC.numUserCntId=@numContactID
   AND BC.bitCommisionPaid=0 --added by chintan BugID 982
   AND oppBiz.bitAuthoritativeBizDocs = 1 
   AND isnull(oppBiz.monAmountPaid,0) >=oppBiz.monDealAmount
	   
--and
--		numComissionID in (select distinct numComissionID from opportunitybizdocsdetails where 
--			numDomainId = @numDomainId AND bitAuthoritativeBizDocs = 1	
--			and numContactID=@numContactID and bitIntegratedToAcnt=0 and tintPaymentType = 5 and numBizDocsId > 0)

--UPDATE opportunitybizdocsdetails set bitIntegratedToAcnt=1  where 
--			numDomainId = @numDomainId AND bitAuthoritativeBizDocs = 1	
--			and numContactID=@numContactID and bitIntegratedToAcnt=0 and tintPaymentType = 5 and numBizDocsId > 0

END
