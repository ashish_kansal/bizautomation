/****** Object:  StoredProcedure [dbo].[USP_RepBizDocsByCont]    Script Date: 07/26/2008 16:20:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- Created By ANoop Jayaraj
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_repbizdocsbycont')
DROP PROCEDURE usp_repbizdocsbycont
GO
CREATE PROCEDURE [dbo].[USP_RepBizDocsByCont]
@numContactID as numeric(9)
as
select vcBizDocID,dbo.fn_GetListItemName(numBizDocID) as BizDoc,'Viewed' as [ActivityType],dtViewedDate as Date from OpportunityBizDocs
where numViewedBy =@numContactID
GO
