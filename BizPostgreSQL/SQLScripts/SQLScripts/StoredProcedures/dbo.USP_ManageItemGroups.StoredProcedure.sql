
GO
/****** Object:  StoredProcedure [dbo].[USP_ManageItemGroups]    Script Date: 10/15/2010 17:13:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj      
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_manageitemgroups')
DROP PROCEDURE usp_manageitemgroups
GO
CREATE PROCEDURE [dbo].[USP_ManageItemGroups]      
@numDomainID as numeric(9)=0,       
@strOpt as varchar(4000)='',    
@strAttr as varchar(4000)='',     
@numItemGroupID as numeric(9),      
@vcName as varchar(100) ,
@bitCombineAssemblies AS BIT,
@numMapToDropdownFld AS NUMERIC(18)=0,
@numProfileItem AS NUMERIC(18)     
as      
DECLARE @hDoc1 int        
DECLARE @hDoc2 int  
  
if @numItemGroupID=0       
 begin      
  insert into ItemGroups (vcItemGroup,numDomainID,bitCombineAssemblies,numMapToDropdownFld,numProfileItem)      
  values (@vcName,@numDomainID,@bitCombineAssemblies,@numMapToDropdownFld,@numProfileItem)      
  set @numItemGroupID=@@identity      
       
                                           
   EXEC sp_xml_preparedocument @hDoc1 OUTPUT, @strOpt                                                                         
   insert into ItemGroupsDTL                                          
     (numItemGroupID,numOppAccAttrID,tintType)                                   
    select @numItemGroupID,X.numOppAccAttrID,1 from(                                          
    SELECT *FROM OPENXML (@hDoc1,'/NewDataSet/Table1',2)                                          
    WITH  (numOppAccAttrID numeric(9)))X                                                               
    EXEC sp_xml_removedocument @hDoc1   
  
    EXEC sp_xml_preparedocument @hDoc2 OUTPUT, @strAttr                                                                         
   insert into ItemGroupsDTL                                          
     (numItemGroupID,numOppAccAttrID,tintType)                                   
    select @numItemGroupID,X.numOppAccAttrID,2 from(                                          
    SELECT *FROM OPENXML (@hDoc2,'/NewDataSet/Table',2)                                          
    WITH  (numOppAccAttrID numeric(9)))X                                                               
    EXEC sp_xml_removedocument @hDoc2      
      
 end      
 else if @numItemGroupID>0      
 begin      
  Update ItemGroups       
   set vcItemGroup=@vcName,            
   numDomainID=@numDomainID,
   bitCombineAssemblies=@bitCombineAssemblies,
   numMapToDropdownFld=@numMapToDropdownFld,
   numProfileItem=@numProfileItem
  where numItemGroupID=@numItemGroupID    

 --delete from ItemGroupsDTL where numItemGroupID= @numItemGroupID
                                         
     EXEC sp_xml_preparedocument @hDoc1 OUTPUT, @strOpt 
     
      update ItemGroupsDTL set                                                                         
    numQtyItemsReq=X.QtyItemsReq,numWareHouseItemId=X.WareHouseItemId,numListid=X.ListItemId                                                                                  
     From (SELECT QtyItemsReq,OppAccAttrID,WareHouseItemId,ListItemId                             
    FROM OPENXML(@hDoc1,'/NewDataSet/Table1',2)                                                                         
     with(OppAccAttrID numeric(9),QtyItemsReq numeric(9),WareHouseItemId numeric(9),ListItemId numeric(9)))X                                                                         
   where  numItemGroupID=@numItemGroupID and numOppAccAttrID=OppAccAttrID
                                                                           
--  insert into ItemGroupsDTL                                          
--     (numItemGroupID,numOppAccAttrID,tintType,numQtyItemsReq,numWareHouseItemId,numDefaultSelect)                                   
--    select @numItemGroupID,X.numOppAccAttrID,1,X.numQtyItemsReq,X.numWareHouseItemId,X.numDefaultSelect from(                                          
--    SELECT *FROM OPENXML (@hDoc1,'/NewDataSet/Table1',2)                                          
--    WITH  (numOppAccAttrID numeric(9),numQtyItemsReq numeric(9),numWareHouseItemId numeric(9),numDefaultSelect numeric(9)))X                                                               
--    EXEC sp_xml_removedocument @hDoc1   
  
    EXEC sp_xml_preparedocument @hDoc2 OUTPUT, @strAttr                                                                         
   insert into ItemGroupsDTL                                          
     (numItemGroupID,numOppAccAttrID,tintType)                                   
    select @numItemGroupID,X.numOppAccAttrID,2 from(                                          
    SELECT *FROM OPENXML (@hDoc2,'/NewDataSet/Table',2)                                          
    WITH  (numOppAccAttrID numeric(9)))X  where X.numOppAccAttrID not in (select numOppAccAttrID from ItemGroupsDTL where numItemGroupID=@numItemGroupID)                                                         
    
	--Bug fix ID 1720 #6
	-- delete saved attributes
	DELETE FROM dbo.CFW_Fld_Values_Serialized_Items WHERE
    Fld_ID NOT IN ( SELECT numOppAccAttrID FROM OPENXML (@hDoc2,'/NewDataSet/Table',2) WITH  (numOppAccAttrID numeric(9)))
	AND RecId IN (SELECT numWareHouseItemID FROM  dbo.WareHouseItems WHERE numDomainID=@numDomainID AND numItemID IN ( SELECT numItemCode FROM  dbo.Item WHERE numDomainID=@numDomainID AND numItemGroup = @numItemGroupID))


	DELETE FROM dbo.ItemGroupsDTL WHERE tintType = 2 AND numItemGroupID=@numItemGroupID AND
	numOppAccAttrID NOT IN(  SELECT numOppAccAttrID FROM OPENXML (@hDoc2,'/NewDataSet/Table',2) WITH  (numOppAccAttrID numeric(9)))

	
	EXEC sp_xml_removedocument @hDoc2

 end      
select @numItemGroupID
