/****** Object:  StoredProcedure [dbo].[usp_TicklerAssignedStages]    Script Date: 07/26/2008 16:21:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj              
    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_ticklerassignedstages')
DROP PROCEDURE usp_ticklerassignedstages
GO
CREATE PROCEDURE [dbo].[usp_TicklerAssignedStages]                            
@numUserCntID as numeric(9)=null,                            
@numDomainID as numeric(9)=null,                            
@startDate as datetime,                            
@endDate as datetime                           
as                             
                            
select opp.numOppId as ID,vcPOppname,(select sum(tintPercentage) from OpportunityStageDetails where numOppId=opp.numOppId and bitStageCompleted=1) as Tprogress,                                                      
OppStg.bintDueDate AS closeDate,                             
opp.numRecOwner,                                               
AddC.vcFirstName+' '+AddC.vcLastName as Name ,                                                
case when Addc.numPhone<>'' then + AddC.numPhone +case when AddC.numPhoneExtension<>'' then ' - ' + AddC.numPhoneExtension else '' end  else '' end as [Phone],      
Com.vcCompanyName,      
'' as vcEmail,                                           
case when opp.tintOppType= 1 then 'Sales Opportunity' when opp.tintOppType= 2 then 'Purchase Opportunity' end as Type,                            
Div.numTerId,                    
convert(varchar(50),numstagepercentage) as numstagepercentage,                    
vcStageDetail  ,  
dbo.fn_GetContactName(OppStg.numAssignTo)+'/'+   dbo.fn_GetContactName(@numUserCntID) AssignedToBy                    
  from OpportunityMaster opp                            
 join AdditionalContactsInformation addC                            
 on addC.numContactId=opp.numContactId                            
 join DivisionMaster div                            
 on div.numDivisionID=opp.numDivisionID                            
 join CompanyInfo Com                            
 on com.numCompanyId=div.numCompanyId                            
 join OpportunityStageDetails OppStg                    
 on OppStg.numOppId=opp.numOppId                    
where   com.numDomainID=@numDomainID and OppStg.numAssignTo=@numUserCntID                     
and bitStageCompleted=0                             
and tintOppStatus=0 and (bintDueDate  >= @startDate and bintDueDate  <= @endDate)                        
union                             
                     
select pro.numProId as ID,vcProjectName as vcPOppname,(select sum(tintPercentage) from ProjectsStageDetails where numProId=pro.numProId and bitStageCompleted=1) as Tprogress,                                                                 
ProStg.bintDueDate AS closeDate,                           
Pro.numRecOwner,                                              
--AddC.vcFirstName+', '+numPhone +'- '+numPhoneExtension as Name ,         
AddC.vcFirstName+' '+AddC.vcLastName as Name ,          
case when Addc.numPhone<>'' then + AddC.numPhone +case when AddC.numPhoneExtension<>'' then ' - ' + AddC.numPhoneExtension else '' end  else '' end as [Phone],                                                                           
Com.vcCompanyName,   
'' as vcEmail,                                              
 'Project' as Type,                            
Div.numTerId,                    
convert(varchar(50),numstagepercentage)as numstagepercentage,                    
vcStageDetail ,  
dbo.fn_GetContactName(ProStg.numAssignTo)+'/'+   dbo.fn_GetContactName(@numUserCntID) AssignedToBy                                              
from ProjectsMaster Pro                            
 left join AdditionalContactsInformation addC                            
 on addC.numContactId=Pro.numCustPrjMgr                            
 join DivisionMaster div                            
 on div.numDivisionID=Pro.numDivisionID                            
 join CompanyInfo Com                            
 on com.numCompanyId=div.numCompanyId                      
join ProjectsStageDetails ProStg                    
 on ProStg.numProId=Pro.numProId                          
where  com.numDomainID=@numDomainID                      
and ProStg.numAssignTo=@numUserCntID                  
and bitStageCompleted=0                           
and tintProStatus=0 and (bintDueDate  >= @startDate and bintDueDate  <= @endDate)
GO
