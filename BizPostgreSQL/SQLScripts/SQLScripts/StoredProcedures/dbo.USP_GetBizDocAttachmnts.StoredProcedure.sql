/****** Object:  StoredProcedure [dbo].[USP_GetBizDocAttachmnts]    Script Date: 07/26/2008 16:16:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj      
--exec USP_GetBizDocAttachmnts,
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getbizdocattachmnts')
DROP PROCEDURE usp_getbizdocattachmnts
GO
CREATE PROCEDURE [dbo].[usp_getbizdocattachmnts]      
 @numBizDocID as numeric(9)=0,      
 @numDomainId as numeric(9)=0,      
 @numAttachmntID as numeric(9)=0,
 @numBizDocTempID as numeric(9)=0     ,
 @intAddReferenceDocument AS NUMERIC(10)=0
as      
       
if (@numAttachmntID=0 and   @numBizDocID <>0)  
begin    
    
 if not exists(select *,'' AS vcFileName from BizDocAttachments where numBizDocID=@numBizDocID and numDomainID=@numDomainID and vcDocName='BizDoc' and isnull(numBizDocTempID,0)=@numBizDocTempID)        
  begin        
   insert into BizDocAttachments (numBizDocID,numDomainID,vcURL,numOrder,vcDocName,numBizDocTempID)        
   values (@numBizDocID,@numDomainID,'BizDoc',1,'BizDoc',@numBizDocTempID)        
  end       
     
 
  IF(@intAddReferenceDocument=1)
  BEGIN
	 (select numAttachmntID,vcDocName,vcURL,numBizDocID,numBizDocTempID,numOrder,(SELECT TOP 1 vcBizDocId FROM OpportunityBizDocs WHERE numOppBizDocsId=@numBizDocID) AS vcFileName from BizDocAttachments       
	 where numBizDocID=@numBizDocID and numDomainId=@numDomainId and isnull(numBizDocTempID,0)=@numBizDocTempID)
	 UNION
	 (SELECT numAttachmntID,vcDocName,vcURL,numBizDocID,numBizDocTempID,numOrder,'' as vcFileName from BizDocAttachments       
	 where vcURL<>'BizDoc' AND numBizDocID=(SELECT TOP 1 numBizDocId FROM OpportunityBizDocs WHERE numOppBizDocsId=@numBizDocID) and numDomainId=@numDomainId and isnull(numBizDocTempID,0)=(SELECT TOP 1 numBizDocTempID FROM OpportunityBizDocs WHERE numOppBizDocsId=@numBizDocID)  )
	order by numOrder 
  END
  ELSE
  BEGIN
	 select numAttachmntID,vcDocName,vcURL,numBizDocID,numBizDocTempID,'' AS vcFileName from BizDocAttachments       
  where numBizDocID=@numBizDocID and numDomainId=@numDomainId and isnull(numBizDocTempID,0)=@numBizDocTempID order by numOrder      
  END
end    
else if @numAttachmntID>0      
      
select *,'' AS vcFileName from BizDocAttachments where numAttachmntID=@numAttachmntID
GO
