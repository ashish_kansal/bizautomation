/****** Object:  StoredProcedure [dbo].[USP_GetWarehouses]    Script Date: 07/26/2008 16:18:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj        
GO
--exec [USP_GetWarehouses] 72,0
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getwarehouses')
DROP PROCEDURE usp_getwarehouses
GO
CREATE PROCEDURE [dbo].[USP_GetWarehouses]        
@numDomainID as numeric(9)=0,        
@numWareHouseID as numeric(9)=0        
as
BEGIN         
	SELECT 
		[numWareHouseID]         
		,(CASE WHEN AD.numAddressID IS NOT NULL THEN AD.vcStreet ELSE vcWStreet END) [vcWStreet]        
		,(CASE WHEN AD.numAddressID IS NOT NULL THEN AD.vcCity ELSE [vcWCity] END) [vcWCity]        
		,(CASE WHEN AD.numAddressID IS NOT NULL THEN AD.vcPostalCode ELSE [vcWPinCode] END) [vcWPinCode]        
		,(CASE WHEN AD.numAddressID IS NOT NULL THEN AD.numState ELSE [numWState] END) [numWState]        
		,(CASE WHEN AD.numAddressID IS NOT NULL THEN AD.numCountry ELSE [numWCountry] END) [numWCountry]        
		,W.[numDomainID]              
		,vcWareHouse
		,W.numAddressID
		,W.vcPrintNodeAPIKey
		,W.vcPrintNodePrinterID
		,(CASE WHEN AD.numAddressID IS NULL THEN '' ELSE CONCAT(ISNULL(vcStreet,''),', ',ISNULL(vcCity,''),', ',ISNULL(dbo.fn_GetState(numState),''),', ',ISNULL(vcPostalCode,''),', ',ISNULL(dbo.fn_GetListItemName(numCountry),'')) END) AS vcAddress
		, ISNULL((SELECT TOP 1 vcStateCode FROM ShippingStateMaster 
			where vcStateName= ( CASE WHEN AD.numState IS NOT NULL THEN (dbo.fn_GetState(AD.numState)) ELSE (dbo.fn_GetState(W.numWState)) END) ), '') AS StateAbbr		
		,ISNULL((SELECT TOP 1 vcCountryCode from ShippingCountryMaster 
			where vcCountryName=( CASE WHEN AD.numCountry IS NOT NULL THEN (dbo.fn_GetListItemName(AD.numCountry)) ELSE (dbo.fn_GetListItemName([numWCountry])) END)),'') AS CountryAbbr

   FROM [dbo].[Warehouses] W 
   LEFT JOIN
		AddressDetails AD
	ON
        W.numAddressID = AD.numAddressID
   WHERE 
		W.numDomainID=@numDomainID
		AND (numWareHouseID=@numWareHouseID  OR ISNULL(@numWareHouseID,0) = 0)
END
GO
