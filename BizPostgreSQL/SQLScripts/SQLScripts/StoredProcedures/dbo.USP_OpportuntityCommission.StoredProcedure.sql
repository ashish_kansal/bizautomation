/****** Object:  StoredProcedure [dbo].[USP_OpportuntityCommission]    Script Date: 07/26/2008 16:20:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created by Siva
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_opportuntitycommission')
DROP PROCEDURE usp_opportuntitycommission
GO
CREATE PROCEDURE [dbo].[USP_OpportuntityCommission]
(@numUserId as numeric(9)=0,
 @numUserCntID as numeric(9)=0,
@numDomainId as numeric(9)=0)
As
Begin
Declare @bitCommOwner as bit  
 Declare @bitCommAssignee as bit 
 Declare @decCommOwner as decimal(10,2)  
 Declare @decCommAssignee as decimal(10,2)

  Select @bitCommOwner=bitCommOwner,@bitCommAssignee=bitCommAssignee,@decCommOwner=isnull(fltCommOwner,0),@decCommAssignee=isnull(fltCommAssignee,0) from UserMaster Where numUserId=1  
 
  Select Opp.vcPOppName AS Name ,Case when Opp.tintOppStatus=0 then 'Open' Else 'Close' End as OppStatus, Opp.monPAmount as DealAmount,
  'Deal Owner' as EmpRole,(Select isnull(fltCommOwner,0) From UserMaster Where numUserId=1) as CommissionPer,isnull(Opp.monPAmount,0)*@decCommOwner/100 as CommissionAmt
  From OpportunityMaster Opp Where Opp.numRecOwner=1 And Opp.tintOppStatus=1
  Union all
  Select Opp.vcPOppName AS Name ,Case when Opp.tintOppStatus=0 then 'Open' Else 'Close' End as OppStatus, Opp.monPAmount as DealAmount,
  'Deal Assigned' as EmpRole,(Select isnull(fltCommAssignee,0) From UserMaster Where numUserId=1) as CommissionPer,isnull(Opp.monPAmount,0)*@decCommAssignee/100 as CommissionAmt
  From OpportunityMaster Opp Where Opp.numAssignedTo=1 And Opp.tintOppStatus=1
  
End
GO
