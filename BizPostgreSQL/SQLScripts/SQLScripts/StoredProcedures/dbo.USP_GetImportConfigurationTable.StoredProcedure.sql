/****** Object:  StoredProcedure [dbo].[USP_GetImportConfigurationTable]    Script Date: 07/26/2008 16:17:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getimportconfigurationtable')
DROP PROCEDURE usp_getimportconfigurationtable
GO
CREATE PROCEDURE [dbo].[USP_GetImportConfigurationTable]            
@numRelation as numeric,          
@numDomain as numeric,
@ImportType as tinyint          
as            
            
       
if  (select count(*) from RecImportConfg where numDomainid=@numDomain and numRelationShip=@numRelation and ImportType=@ImportType)=0    
set @numDomain = 0           
            
      
select HDR.vcFormFieldName,convert(varchar(10),DTL.numFormFieldId)  as numFormFieldId,intcolumn,bitCustomFld,cCtype,vcAssociatedControlType,vcDbColumnName,ISNULL(HDR.numListID,0) numListID
 from RecImportConfg DTL         
join DynamicFormFieldMaster HDR on DTL.numFormFieldId = HDR.numFormFieldId        
where DTL.numDomainId=@numDomain and DTL.numRelationShip=@numRelation and bitCustomFld = 0   and ImportType=@ImportType     
  union      
select CFW.fld_label as vcFormFieldName,convert(varchar(10),DTL.numFormFieldId)  as numFormFieldId,intcolumn,bitCustomFld,cCtype,fld_type as vcAssociatedControlType,'a' as vcDbColumnName ,ISNULL(CFW.numlistid,0) numListID
  from RecImportConfg DTL         
join CFW_Fld_Master CFW on DTL.numFormFieldId = CFW.Fld_id    
        
where DTL.numDomainid=@numDomain and DTL.numRelationShip=@numRelation and bitCustomFld = 1  and ImportType=@ImportType    
order by intcolumn
