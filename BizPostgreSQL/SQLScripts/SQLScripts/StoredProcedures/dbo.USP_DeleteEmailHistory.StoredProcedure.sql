/****** Object:  StoredProcedure [dbo].[USP_DeleteEmailHistory]    Script Date: 07/26/2008 16:15:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_deleteemailhistory')
DROP PROCEDURE usp_deleteemailhistory
GO
CREATE PROCEDURE [dbo].[USP_DeleteEmailHistory]
@vcItemId varchar(1000)
as 

delete EmailHStrToBCCAndCC where numEmailHSTRID in(select numEmailHSTRID from emailhistory where vcItemId=@vcItemId)

delete emailhistory  where vcItemId=@vcItemId
GO
