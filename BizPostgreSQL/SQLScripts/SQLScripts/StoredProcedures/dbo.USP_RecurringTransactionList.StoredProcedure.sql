
GO
/****** Object:  StoredProcedure [dbo].[USP_RecurringTransactionList]    Script Date: 03/25/2009 15:14:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created By Siva                        
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_recurringtransactionlist')
DROP PROCEDURE usp_recurringtransactionlist
GO
CREATE PROCEDURE  [dbo].[USP_RecurringTransactionList]                        
@numFilterBy as numeric(9)=0,                        
@ChrInterval as Char(1),                        
@numAcntType as  numeric(9)=0,                  
@varCustomerOrVendorSearch as varchar(500),              
@numDomainId as numeric(9)=0,      
@ClientTimeZoneOffset Int      
                      
As                        
Begin                        
Declare @strSql as varchar(8000)                           
Set @strSql=''                        
If @numFilterBy=0                         
Begin                        
                 
Set @strSql='Select                         
RT.numRecurringId As RecurringId, dbo.fn_GetContactName(CCCD.numCreatedBy) As CreatedBy,DateAdd(minute,'+Convert(varchar(10), -@ClientTimeZoneOffset)+', CCCD.dtCreatedDate) As CreatedDate, Null DepositId,          
CCCD.numCashCreditId As CashCreditId,Null CheckId, Null as OppId, GJH.numJournal_Id JournalId, 0 MoneyIn,  1 MoneyOut, RT.varRecurringTemplateName as TemplateName,                    
CI.vcCompanyName as CustomerName,dbo.fn_GetAcntTypeDescription(CCCD.numCashCreditId,''CC'') As AccntType,                
Case When CCCD.bitMoneyOut=0 then ''Cash'' Else ''Credit Card Details'' End  as TransactionType,                    
case when chrIntervalType=''D'' then ''Daily, ''  when chrIntervalType=''M'' then ''Monthly , ''                    
When chrIntervalType=''Y'' then ''Yearly , '' end as IntervalType,                     
CCCD.monAmount as monAmount,RT.dtStartDate As StartDate, RT.dtEndDate  As EndDate                        
From RecurringTemplate RT                        
inner join CashCreditCardDetails CCCD on RT.numRecurringId =CCCD.numRecurringId            
inner join General_Journal_Header GJH on CCCD.numCashCreditId=GJH.numCashCreditCardId              
Left outer join DivisionMaster as DM on CCCD.numPurchaseId=DM.numDivisionID                                                                                     
Left outer join CompanyInfo as CI on  DM.numCompanyID=CI.numCompanyId           
Where RT.numDomainId='+convert(varchar(5),@numDomainId)          
                  
 If @varCustomerOrVendorSearch <> ''                    
    Set  @strSql=@strSql + '  And CI.vcCompanyName like ''' + convert(varchar(500),@varCustomerOrVendorSearch) +'%'''                    
                  
Set @strSql=@strSql + 'Union All                        
Select                         
RT.numRecurringId As RecurringId, dbo.fn_GetContactName(CD.numCreatedBy) As CreatedBy, DateAdd(minute,'+Convert(varchar(10), -@ClientTimeZoneOffset)+',CD.dtCreatedDate) As CreatedDate,Null DepositId,                 
Null CashCreditId,CD.numCheckId As  CheckId, Null as OppId, GJH.numJournal_Id JournalId, 0 MoneyIn,1 MoneyOut, RT.varRecurringTemplateName As TemplateName,                
CI.vcCompanyName as CustomerName,                    
dbo.fn_GetAcntTypeDescription(CD.numCheckId,''C'') As AccntType,''Checks'' as TransactionType,case when chrIntervalType=''D'' then ''Daily, ''                   
when chrIntervalType=''M'' then ''Monthly , '' When chrIntervalType=''Y'' then ''Yearly,''end as IntervalType,                    
CD.monCheckAmt as monAmount,RT.dtStartDate As StartDate, RT.dtEndDate  As EndDate                  
From RecurringTemplate RT                        
Inner join CheckDetails CD on RT.numRecurringId =CD.numRecurringId                  
inner join General_Journal_Header GJH on CD.numCheckId=GJH.numCheckId                
Left outer join DivisionMaster as DM on CD.numCustomerId=DM.numDivisionID                                                                                     
Left outer join CompanyInfo as CI on  DM.numCompanyID=CI.numCompanyId                     
Where RT.numDomainId='+convert(varchar(5),@numDomainId)                
 If @varCustomerOrVendorSearch <> ''                    
    Set  @strSql=@strSql + ' And  CI.vcCompanyName like ''' + convert(varchar(500),@varCustomerOrVendorSearch) +'%'''                    
                  
Set @strSql=@strSql + 'Union All                        
Select                         
RT.numRecurringId As RecurringId,dbo.fn_GetContactName(GJH.numCreatedBy)As CreatedBy,DateAdd(minute,'+Convert(varchar(10), -@ClientTimeZoneOffset)+',GJH.datCreatedDate) As CreatedDate, Null DepositId, Null As CashCreditId,                
Null As CheckId, Null as OppId,GJH.numJournal_Id As JournalId, 0 MoneyIn,0 MoneyOut, RT.varRecurringTemplateName as TemplateName,Null as CustomerName,                
dbo.fn_GetAcntTypeDescription(GJH.numJournal_Id,''JD'') As AccntType,                      
''Journal'' as TransactionType,case when chrIntervalType=''D'' then ''Daily, ''  when chrIntervalType=''M'' then ''Monthly , '' When chrIntervalType=''Y'' then ''Yearly,''                      
end as IntervalType, GJH.numAmount as monAmount, RT.dtStartDate As StartDate, RT.dtEndDate  As EndDate                        
From RecurringTemplate RT                        
Inner join General_Journal_Header GJH on RT.numRecurringId =GJH.numRecurringId Where RT.numDomainId='+convert(varchar(5),@numDomainId)                    
          
Set @strSql=@strSql + 'Union All            
 Select                         
RT.numRecurringId As RecurringId,dbo.fn_GetContactName(DD.numCreatedBy) As CreatedBy,DateAdd(minute,'+Convert(varchar(10), -@ClientTimeZoneOffset)+', DD.dtCreationDate) As CreatedDate,                
DD.numDepositId As DepositId, Null CashCreditId,Null CheckId, Null as OppId, GJH.numJournal_Id JournalId, 1 MoneyIn,0 MoneyOut, RT.varRecurringTemplateName as TemplateName,                
NULL as CustomerName,dbo.fn_GetAcntTypeDescription(DD.numDepositId,''DD'') As AccntType,                      
''Deposit'' as TransactionType,case when chrIntervalType=''D'' then ''Daily, ''    when chrIntervalType=''M'' then ''Monthly , ''  When chrIntervalType=''Y'' then ''Yearly , ''                  
end as IntervalType,DD.monDepositAmount As monAmount,RT.dtStartDate As StartDate, RT.dtEndDate  As EndDate                        
From RecurringTemplate RT                        
inner join DepositDetails DD on RT.numRecurringId =DD.numRecurringId             
inner join General_Journal_Header GJH on DD.numDepositId=GJH.numDepositId   
Where RT.numDomainId='+convert(varchar(5),@numDomainId)      
  
Set @strSql=@strSql + 'Union All            
 Select                         
RT.numRecurringId As RecurringId,dbo.fn_GetContactName(Opp.numCreatedBy) As CreatedBy,DateAdd(minute,'+Convert(varchar(10), -@ClientTimeZoneOffset)+', Opp.bintCreatedDate) As CreatedDate,                
Null As DepositId, Null CashCreditId,Null CheckId,Opp.numOppId as OppId, GJH.numJournal_Id JournalId, 0 MoneyIn, 0 MoneyOut,RT.varRecurringTemplateName as TemplateName,CI.vcCompanyName as CustomerName,dbo.fn_GetAcntTypeDescription(Opp.numOppId,''Opp'') As
 AccntType,                      
''Opportunity'' as TransactionType,case when chrIntervalType=''D'' then ''Daily, ''    when chrIntervalType=''M'' then ''Monthly , ''  When chrIntervalType=''Y'' then ''Yearly , ''                  
end as IntervalType,Opp.monPAmount As monAmount,RT.dtStartDate As StartDate, RT.dtEndDate  As EndDate                        
From OpportunityMaster Opp 
LEFT OUTER JOIN [OpportunityRecurring] OPR ON OPR.numOppId = Opp.numOppId
inner join RecurringTemplate RT on RT.numRecurringId =OPR.numRecurringId
inner join General_Journal_Header GJH on Opp.numOppId=GJH.numOppId And GJH.numBizDocsPaymentDetId is null  
Left outer join DivisionMaster as DM on Opp.numDivisionId=DM.numDivisionID                                                                                     
Left outer join CompanyInfo as CI on  DM.numCompanyID=CI.numCompanyId                 
Where RT.numDomainId='+convert(varchar(5),@numDomainId)      
 If @varCustomerOrVendorSearch <> ''                    
    Set  @strSql=@strSql + ' And  CI.vcCompanyName like ''' + convert(varchar(500),@varCustomerOrVendorSearch) +'%'''                    
             
Print '@strSql'+Convert(varchar(10), len(@strSql))          
print @strSql                        
Exec (@strSql)                    
End                        
If @numFilterBy=1                         
Begin                        
Set @strSql= ' Select                         
RT.numRecurringId As RecurringId,dbo.fn_GetContactName(CCCD.numCreatedBy) As CreatedBy, DateAdd(minute,'+Convert(varchar(10), -@ClientTimeZoneOffset)+',CCCD.dtCreatedDate) As CreatedDate, Null DepositId,                 
CCCD.numCashCreditId As CashCreditId,Null CheckId,Null as OppId, GJH.numJournal_Id JournalId, 0 MoneyIn,1 MoneyOut, RT.varRecurringTemplateName as TemplateName,                
CI.vcCompanyName as CustomerName,dbo.fn_GetAcntTypeDescription(CCCD.numCashCreditId,''CC'') As AccntType,                      
Case When CCCD.bitMoneyOut=0 then ''Cash'' Else ''Credit Card Details'' End as TransactionType,case when chrIntervalType=''D'' then ''Daily, ''    when chrIntervalType=''M'' then ''Monthly , ''  When chrIntervalType=''Y'' then ''Yearly , ''               
  
end as IntervalType,CCCD.monAmount As monAmount,RT.dtStartDate As StartDate, RT.dtEndDate  As EndDate                        
From RecurringTemplate RT                        
inner join CashCreditCardDetails CCCD on RT.numRecurringId =CCCD.numRecurringId             
inner join General_Journal_Header GJH on CCCD.numCashCreditId=GJH.numCashCreditCardId                     
Left outer join DivisionMaster as DM on CCCD.numPurchaseId=DM.numDivisionID                                                                                     
Left outer join CompanyInfo as CI on  DM.numCompanyID=CI.numCompanyId         
 Where RT.numDomainId='+convert(varchar(5),@numDomainId)        
                         
   If @varCustomerOrVendorSearch <> ''                    
    Set  @strSql=@strSql + ' And CI.vcCompanyName like ''' + convert(varchar(500),@varCustomerOrVendorSearch) +'%'''                    
                       
Set @strSql=@strSql + ' Union All                        
Select                         
RT.numRecurringId As RecurringId,dbo.fn_GetContactName(CD.numCreatedBy) As CreatedBy,DateAdd(minute,'+Convert(varchar(10), -@ClientTimeZoneOffset)+',CD.dtCreatedDate) As CreatedDate, Null DepositId, Null CashCreditId,                
CD.numCheckId As  CheckId,Null as OppId, GJH.numJournal_Id JournalId, 0 MoneyIn,1 MoneyOut, RT.varRecurringTemplateName As TemplateName,CI.vcCompanyName as CustomerName,                 
dbo.fn_GetAcntTypeDescription(CD.numCheckId,''C'') As AccntType,                       
''Checks'' as TransactionType,case when chrIntervalType=''D'' then ''Daily, ''   when chrIntervalType=''M'' then ''Monthly , ''  When chrIntervalType=''Y'' then                    
''Yearly,''end  as IntervalType,CD.monCheckAmt as monAmount,                  
RT.dtStartDate As StartDate, RT.dtEndDate  As EndDate                        
From RecurringTemplate RT                        
inner join CheckDetails CD on RT.numRecurringId =CD.numRecurringId             
inner join General_Journal_Header GJH on CD.numCheckId=GJH.numCheckId                       
Left outer join DivisionMaster as DM on CD.numCustomerId=DM.numDivisionID                                                                                     
Left outer join CompanyInfo as CI on  DM.numCompanyID=CI.numCompanyId          
 Where RT.numDomainId='+convert(varchar(5),@numDomainId)                       
    If @varCustomerOrVendorSearch <> ''                    
    Set  @strSql=@strSql + ' And CI.vcCompanyName like ''' + convert(varchar(500),@varCustomerOrVendorSearch) +'%'''                  
                  
print @strSql                        
Exec (@strSql)                         
End                        
                  
          
If @numFilterBy=2                        
Begin                        
Set @strSql= ' Select                         
RT.numRecurringId As RecurringId,dbo.fn_GetContactName(DD.numCreatedBy) As CreatedBy,DateAdd(minute,'+Convert(varchar(10), -@ClientTimeZoneOffset)+', DD.dtCreationDate) As CreatedDate,                
DD.numDepositId As DepositId, Null CashCreditId,Null CheckId,Null as OppId, GJH.numJournal_Id JournalId, 1 MoneyIn,0 MoneyOut, RT.varRecurringTemplateName as TemplateName,                
NULL as CustomerName,dbo.fn_GetAcntTypeDescription(DD.numDepositId,''DD'') As AccntType,                      
''Deposit'' as TransactionType,case when chrIntervalType=''D'' then ''Daily, ''    when chrIntervalType=''M'' then ''Monthly , ''  When chrIntervalType=''Y'' then ''Yearly , ''                  
end as IntervalType,DD.monDepositAmount As monAmount,RT.dtStartDate As StartDate, RT.dtEndDate  As EndDate                        
From RecurringTemplate RT                        
inner join DepositDetails DD on RT.numRecurringId =DD.numRecurringId             
inner join General_Journal_Header GJH on DD.numDepositId=GJH.numDepositId                     
----Left outer join DivisionMaster as DM on CCCD.numPurchaseId=DM.numDivisionID                                                                                     
----Left outer join CompanyInfo as CI on  DM.numCompanyID=CI.numCompanyId            
Where RT.numDomainId='+convert(varchar(5),@numDomainId)                          
               
                  
print @strSql                        
Exec (@strSql)                         
End              
                
If  @numFilterBy=3 Or @numFilterBy=4                        
Begin                        
Set @strSql='Select                        
RT.numRecurringId As RecurringId,dbo.fn_GetContactName(CCCD.numCreatedBy) As CreatedBy,DateAdd(minute,'+Convert(varchar(10), -@ClientTimeZoneOffset)+',CCCD.dtCreatedDate) As CreatedDate,   Null DepositId,              
 CCCD.numCashCreditId As CashCreditId,Null CheckId,Null as OppId, GJH.numJournal_Id JournalId, 0 MoneyIn,1 MoneyOut, RT.varRecurringTemplateName as TemplateName,                    
CI.vcCompanyName as CustomerName,dbo.fn_GetAcntTypeDescription(CCCD.numCashCreditId,''CC'') As AccntType, Case When CCCD.bitMoneyOut=0 then ''Cash'' Else ''Credit Card Details'' End  as TransactionType,                    
case when chrIntervalType=''D'' then ''Daily, ''  when chrIntervalType=''M'' then ''Monthly , ''                    
When chrIntervalType=''Y'' then ''Yearly , '' end as IntervalType, CCCD.monAmount as MonAmount,                     
RT.dtStartDate As StartDate, RT.dtEndDate  As EndDate                        
From RecurringTemplate RT                        
inner join CashCreditCardDetails CCCD on RT.numRecurringId=CCCD.numRecurringId             
inner join General_Journal_Header GJH on CCCD.numCashCreditId=GJH.numCashCreditCardId                        
Left outer join DivisionMaster as DM on CCCD.numPurchaseId=DM.numDivisionID                               
Left outer join CompanyInfo as CI on  DM.numCompanyID=CI.numCompanyId Where RT.numDomainId='+convert(varchar(5),@numDomainId)                          
                        
If @numAcntType <> 0                           
     Set @strSql=@strSql + ' And  CCCD.numAcntTypeId=' + convert(varchar(10),@numAcntType)                         
If @ChrInterval <> 'A'                        
      Set  @strSql=@strSql + ' And RT.chrIntervalType=''' + convert(varchar(10),@ChrInterval) +''''                        
If @varCustomerOrVendorSearch <> '' And  @numAcntType <> 0                       
    Set  @strSql=@strSql + ' And CI.vcCompanyName like ''' + convert(varchar(500),@varCustomerOrVendorSearch) +'%'''                    
Else If @varCustomerOrVendorSearch <> '' And @ChrInterval <> 'A'                    
    Set  @strSql=@strSql + ' And CI.vcCompanyName like ''' + convert(varchar(500),@varCustomerOrVendorSearch) +'%'''                    
Else if @varCustomerOrVendorSearch <> '' And @ChrInterval = 'A' And @numAcntType = 0                    
    Set  @strSql=@strSql + ' And  CI.vcCompanyName like ''' + convert(varchar(500),@varCustomerOrVendorSearch) +'%'''                    
  
                  
Set @strSql=@strSql + ' Union All                        
Select RT.numRecurringId As RecurringId,dbo.fn_GetContactName(CD.numCreatedBy)As CreatedBy,DateAdd(minute,'+Convert(varchar(10), -@ClientTimeZoneOffset)+',CD.dtCreatedDate) As CreatedDate,Null DepositId,          
Null CashCreditId,CD.numCheckId As  CheckId,Null as OppId, GJH.numJournal_Id JournalId, 0 MoneyIn, 1MoneyOut, RT.varRecurringTemplateName As TemplateName,             
CI.vcCompanyName as CustomerName, dbo.fn_GetAcntTypeDescription(CD.numCheckId,''C'') As AccntType, ''Checks'' as TransactionType,                    
case when chrIntervalType=''D'' then ''Daily, ''   when chrIntervalType=''M'' then ''Monthly , ''                    
When chrIntervalType=''Y'' then ''Yearly,'' end as IntervalType, CD.monCheckAmt as monAmount,                     
RT.dtStartDate As StartDate, RT.dtEndDate  As EndDate                        
From RecurringTemplate RT                        
inner join CheckDetails CD on RT.numRecurringId =CD.numRecurringId               
inner join General_Journal_Header GJH on CD.numCheckId=GJH.numCheckId                    
Left outer join DivisionMaster as DM on CD.numCustomerId=DM.numDivisionID                                                                                     
Left outer join CompanyInfo as CI on  DM.numCompanyID=CI.numCompanyId Where RT.numDomainId='+convert(varchar(5),@numDomainId)                         
                        
If @numAcntType <> 0                           
     Set @strSql=@strSql + ' And CD.numAcntTypeId=' + convert(varchar(10),@numAcntType)                         
If @ChrInterval <> 'A'                        
      Set  @strSql=@strSql + ' And RT.chrIntervalType=''' + convert(varchar(10),@ChrInterval) +''''                        
If @varCustomerOrVendorSearch <> '' And  @numAcntType <> 0                       
    Set  @strSql=@strSql + ' And CI.vcCompanyName like ''' + convert(varchar(500),@varCustomerOrVendorSearch) +'%'''                    
Else If @varCustomerOrVendorSearch <> '' And @ChrInterval <> 'A'                    
    Set  @strSql=@strSql + ' And CI.vcCompanyName like ''' + convert(varchar(500),@varCustomerOrVendorSearch) +'%'''                    
Else if @varCustomerOrVendorSearch <> '' And @ChrInterval = 'A' And @numAcntType = 0                    
    Set  @strSql=@strSql + ' And CI.vcCompanyName like ''' + convert(varchar(500),@varCustomerOrVendorSearch) +'%'''                    
                   
Set @strSql=@strSql + ' Union All Select                         
RT.numRecurringId As RecurringId,dbo.fn_GetContactName(DD.numCreatedBy) As CreatedBy,DateAdd(minute,'+Convert(varchar(10), -@ClientTimeZoneOffset)+',DD.dtCreationDate) As CreatedDate,                
DD.numDepositId As DepositId, Null CashCreditId,Null CheckId,Null as OppId, GJH.numJournal_Id JournalId, 1 MoneyIn,0 MoneyOut, RT.varRecurringTemplateName as TemplateName,                
NULL as CustomerName,dbo.fn_GetAcntTypeDescription(DD.numDepositId,''DD'') As AccntType,                      
''Deposit'' as TransactionType,case when chrIntervalType=''D'' then ''Daily, ''    when chrIntervalType=''M'' then ''Monthly , ''  When chrIntervalType=''Y'' then ''Yearly , ''                  
end as IntervalType,DD.monDepositAmount As monAmount,RT.dtStartDate As StartDate, RT.dtEndDate  As EndDate                        
From RecurringTemplate RT                        
inner join DepositDetails DD on RT.numRecurringId =DD.numRecurringId             
inner join General_Journal_Header GJH on DD.numDepositId=GJH.numDepositId                     
----Left outer join DivisionMaster as DM on CCCD.numPurchaseId=DM.numDivisionID                                                                                     
----Left outer join CompanyInfo as CI on  DM.numCompanyID=CI.numCompanyId               
Where RT.numDomainId='+convert(varchar(5),@numDomainId)         
          
If @numAcntType <> 0                           
     Set @strSql=@strSql + ' And DD.numChartAcntId=' + convert(varchar(10),@numAcntType)                         
If @ChrInterval <> 'A'                        
      Set  @strSql=@strSql + ' And RT.chrIntervalType=''' + convert(varchar(10),@ChrInterval) +''''                        
        
        
print @strSql                        
Exec (@strSql)                        
                        
End                        
End
