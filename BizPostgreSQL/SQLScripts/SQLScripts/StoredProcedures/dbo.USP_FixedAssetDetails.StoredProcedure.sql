/****** Object:  StoredProcedure [dbo].[USP_FixedAssetDetails]    Script Date: 07/26/2008 16:15:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created By Siva            
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_fixedassetdetails')
DROP PROCEDURE usp_fixedassetdetails
GO
CREATE PROCEDURE [dbo].[USP_FixedAssetDetails] 
 @numAssetClass as numeric(9)=0,
 @numDomainId as numeric(9)=0            
As              
Begin              
 declare @strSQL as varchar(8000)
 Set  @strSQL=' Select numFixedAssetId,CA.vcCatgyName as numAssetClass,FA.vcAssetName as vcAssetName,FA.vcAssetDescription as vcAssetDescription,FA.vcSerialNo as vcSerialNo ,          
   FA.monCost as Cost,FA.monCurrentValue as monCurrentValue,FA.monFiscalDepOrAppAmt as monFiscalDepOrAppAmt from FixedAssetDetails FA     
   Left outer join Chart_of_Accounts as CA on FA.numAssetClass=CA.numAccountId Where FA.numDomainId='+ Convert(varchar(5),@numDomainId)
  if @numAssetClass<>0 
    Set @strSQL=@strSQL + ' And FA.numAssetClass='+ convert(varchar(100),@numAssetClass)
   Exec (@strSQL)      
   --Left outer join DivisionMaster as DM on FA.numCompanyId=DM.numDivisionID                                                                               
   --Left outer join CompanyInfo as CI on  DM.numCompanyID=CI.numCompanyId           
End
GO
