/****** Object:  StoredProcedure [dbo].[USP_LoadAttributesEcommerce]    Script Date: 07/26/2008 16:19:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--exec USP_LoadAttributes 14,66,'834,837'      
--created by anoop jayaraj      
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_loadattributesecommerce')
DROP PROCEDURE usp_loadattributesecommerce
GO
CREATE PROCEDURE [dbo].[USP_LoadAttributesEcommerce]      
@numItemCode as numeric(9)=0,      
@numListID as numeric(9)=0,      
@strAtrr as varchar(1000)='',    
@numWareHouseID as numeric(9)=0      
AS
BEGIN        
	DECLARE @ItemGroup AS NUMERIC(9)      
	DECLARE @strSQL AS VARCHAR(MAX) 
	DECLARE @chrItemType AS VARCHAR(5)
	DECLARE @numDomainID AS NUMERIC(18,0)
	DECLARE @bitMatrix AS BIT
	DECLARE @vcItemName VARCHAR(500)
	
	SELECT 
		@ItemGroup=numItemgroup
		,@chrItemType=charItemType
		,@numDomainID=numDomainID 
		,@bitMatrix=ISNULL(bitMatrix,0)
		,@vcItemName=vcItemName
	FROM
		Item 
	WHERE 
		numItemCode=@numItemCode      
      
      
	IF @strAtrr!=''      
	BEGIN
		DECLARE @Cnt INT = 1
		DECLARE @SplitOn CHAR(1)      
	 
		SET @SplitOn = ','    
		
		IF @bitMatrix = 1
		BEGIN
			SET @strSQL = 'SELECT Item.numItemCode FROM ItemAttributes INNER JOIN Item ON ItemAttributes.numItemCode = Item.numItemCode  WHERE'
		END
		ELSE
		BEGIN  
			SET @strSQL = 'SELECT recid FROM CFW_Fld_Values_Serialized_Items WHERE'  
		END
		    
		While(CHARINDEX(@SplitOn,@strAtrr) > 0)      
		BEGIN      
			IF @Cnt=1 
				SET @strSQL = @strSQL+' fld_value=''' + (ltrim(rtrim(Substring(@strAtrr,1,Charindex(@SplitOn,@strAtrr)-1))))+''''      
			ELSE 
				SET @strSQL=@strSQL+' OR fld_value=''' + (ltrim(rtrim(Substring(@strAtrr,1,Charindex(@SplitOn,@strAtrr)-1))))+''''      

			SET @strAtrr = SUBSTRING(@strAtrr,CHARINDEX(@SplitOn,@strAtrr)+1,len(@strAtrr))      
			
			SET @Cnt = @Cnt + 1      
		End 
		
		IF @bitMatrix = 1
		BEGIN
			IF @Cnt=1 
				SET @strSQL= CONCAT(@strSQL,' fld_value=''',@strAtrr,''' AND ISNULL(bitMatrix,0) = 1 AND ISNULL(Item.numItemGroup,0) = ',@ItemGroup,' AND Item.vcItemName=''',@vcItemName,''' GROUP BY Item.numItemCode HAVING count(*) >',@Cnt-1)      
			ELSE
				SET @strSQL= CONCAT(@strSQL,' OR fld_value=''',@strAtrr,''' AND ISNULL(bitMatrix,0) = 1 AND ISNULL(Item.numItemGroup,0) = ',@ItemGroup,' AND Item.vcItemName=''',@vcItemName,''' GROUP BY Item.numItemCode HAVING count(*) > ', @Cnt-1)
		END
		ELSE
		BEGIN
			IF @Cnt=1 
				SET @strSQL=@strSQL+' fld_value=''' + @strAtrr + ''' GROUP BY recid HAVING count(*) > '+convert(varchar(10), @Cnt-1)      
			ELSE
				SET @strSQL=@strSQL+' OR fld_value=''' + @strAtrr + ''' GROUP BY recid HAVING count(*) > '+convert(varchar(10), @Cnt-1)   
		END   
	END      
    
      
	IF @numItemCode>0      
	BEGIN      
		IF @strAtrr = ''      
		BEGIN
			If @chrItemType <> 'P'
			BEGIN
				SELECT numListItemID,vcData from listdetails where numListID=@numListID AND numDomainID=@numDomainID
			END
			ELSE IF @bitMatrix = 1
			BEGIN
				SELECT 
					numListItemID,vcData 
				FROM 
					ListDetails 
				WHERE 
					numlistitemid IN (
										SELECT 
											DISTINCT(fld_value) 
										FROM 
											Item I
										JOIN
											WareHouseItems W      
										ON
											I.numItemCode=W.numItemID
										JOIN 
											ItemAttributes
										ON 
											I.numItemCode=ItemAttributes.numItemCode      
										JOIN 
											CFW_Fld_Master M 
										ON 
											ItemAttributes.Fld_ID=M.Fld_ID                             
										WHERE 
											I.numDomainID = @numDomainID
											AND W.numDomainID = @numDomainID
											AND M.numListID=@numListID 
											AND vcItemName=@vcItemName
											AND bitMatrix = 1
											AND numItemGroup = @ItemGroup
											AND (W.numWareHouseID=@numWareHouseID OR ISNULL(@numWareHouseID,0)=0)
									)      
			END
			ELSE
			BEGIN
				SELECT 
					numListItemID,vcData 
				FROM 
					ListDetails 
				WHERE 
					numlistitemid IN (
										SELECT 
											DISTINCT(fld_value) 
										FROM 
											WareHouseItems W      
										JOIN 
											CFW_Fld_Values_Serialized_Items CSI 
										ON 
											W.numWareHouseItemID=CSI.RecId      
										JOIN 
											CFW_Fld_Master M 
										ON 
											CSI.Fld_ID=M.Fld_ID                        
										JOIN 
											ItemGroupsDTL 
										ON 
											CSI.Fld_ID=ItemGroupsDTL.numOppAccAttrID      
										WHERE 
											M.numListID=@numListID 
											AND tintType=2 
											AND numItemID=@numItemCode 
											AND W.numWareHouseID=@numWareHouseID
									)      
			END
		END
		ELSE      
		BEGIN      
			If @chrItemType <> 'P'
			BEGIN
				select numListItemID,vcData from listdetails where numListID=@numListID AND numDomainID=@numDomainID
			END
			ELSE IF @bitMatrix = 1
			BEGIN
				PRINT 123
				set @strSQL = CONCAT('SELECT 
											numListItemID,vcData 
										FROM 
											ListDetails 
										WHERE 
											numlistitemid IN (
																SELECT 
																	DISTINCT(fld_value) 
																FROM 
																	Item I
																JOIN
																	WareHouseItems W      
																ON
																	I.numItemCode=W.numItemID
																JOIN 
																	ItemAttributes
																ON 
																	I.numItemCode=ItemAttributes.numItemCode      
																JOIN 
																	CFW_Fld_Master M 
																ON 
																	ItemAttributes.Fld_ID=M.Fld_ID                             
																WHERE 
																	I.numDomainID=',@numDomainID,'
																	AND W.numDomainID =',@numDomainID,'
																	AND M.numListID=',@numListID,'
																	AND vcItemName=''',@vcItemName,'''
																	AND bitMatrix = 1
																	AND numItemGroup =',@ItemGroup,'
																	AND (W.numWareHouseID=',@numWareHouseID ,' OR ' + CAST(ISNULL(@numWareHouseID,0) AS VARCHAR) + ' = 0)
																	AND I.numItemCode IN (',@strSQL,'))')

				Print @strSQL       
				exec (@strSQL) 
			END
			ELSE
			BEGIN
				set @strSQL ='select numListItemID,vcData from listdetails where numlistitemid in(select distinct(fld_value) from WareHouseItems W      
				join CFW_Fld_Values_Serialized_Items CSI on W.numWareHouseItemID=CSI.RecId      
				join CFW_Fld_Master M on CSI.Fld_ID=M.Fld_ID      
				join ItemGroupsDTL on CSI.Fld_ID=ItemGroupsDTL.numOppAccAttrID     
				where  M.numListID='+convert(varchar(20),@numListID)+' and tintType=2 and CSI.bitSerialized=0 and numItemID='+convert(varchar(20),@numItemCode)+' and W.numWareHouseID='+convert(varchar(20),@numWareHouseID)+' and fld_value!=''0'' and fld_value!=''''     
 
				and W.numWareHouseItemID in ('+@strSQL+'))' 
  
				Print @strSQL       
				exec (@strSQL)      
			END
		END
	END     
	ELSE
	BEGIN
		SELECT 0
	END
END
GO
