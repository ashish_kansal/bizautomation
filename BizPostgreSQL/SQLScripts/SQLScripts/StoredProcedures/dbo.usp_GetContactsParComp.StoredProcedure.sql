/****** Object:  StoredProcedure [dbo].[usp_GetContactsParComp]    Script Date: 07/26/2008 16:16:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getcontactsparcomp')
DROP PROCEDURE usp_getcontactsparcomp
GO
CREATE PROCEDURE [dbo].[usp_GetContactsParComp]       
@numDomainID as numeric(9)=0    
AS      
    
select numContactId, vcfirstname +' '  + vclastname as vcGivenName From additionalcontactsinformation  
join  usermaster  
on   numuserdetailid= numContactId  
where UserMaster.numDomainID= @numDomainID
GO
