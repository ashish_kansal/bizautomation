
GO
/****** Object:  StoredProcedure [dbo].[USP_UpdateRecurringTemplateForRecurringTransaction]    Script Date: 03/25/2009 15:17:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created By Siva    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_updaterecurringtemplateforrecurringtransaction')
DROP PROCEDURE usp_updaterecurringtemplateforrecurringtransaction
GO
CREATE PROCEDURE [dbo].[USP_UpdateRecurringTemplateForRecurringTransaction]    
@numAccountTypeMode as tinyint,    
@numPrimaryKeyId as numeric(9)=0,  
@numDomainId as numeric(9)=0   
As    
Begin    
 If @numAccountTypeMode=1    
  Update General_Journal_Header Set numRecurringId=0,dtLastRecurringDate=null Where numJournal_Id=@numPrimaryKeyId And  numDomainId=@numDomainId    
 Else If @numAccountTypeMode=2    
  Update CheckDetails Set numRecurringId=0,LastRecurringDate=null Where numCheckId=@numPrimaryKeyId And numDomainId=@numDomainId  
 Else If @numAccountTypeMode=3    
  Update CashCreditCardDetails Set numRecurringId=0,dtLastRecurringDate=null Where numCashCreditId=@numPrimaryKeyId And numDomainId=@numDomainId  
 Else if @numAccountTypeMode=4
	Update [OpportunityRecurring] Set numRecurringId=0,dtLastRecurringDate=null Where numOppId=@numPrimaryKeyId --And numDomainId=@numDomainId  
End
