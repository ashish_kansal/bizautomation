/****** Object:  StoredProcedure [dbo].[USP_GetAuthoritativeBizDocsDetails]    Script Date: 07/26/2008 16:16:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by Siva  
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getauthoritativebizdocsdetails')
DROP PROCEDURE usp_getauthoritativebizdocsdetails
GO
CREATE PROCEDURE [dbo].[USP_GetAuthoritativeBizDocsDetails]          
@numDomainID as numeric(9)=0       
As          
Begin          
	select * from AuthoritativeBizDocs where  numDomainId=@numDomainID       
End
GO
