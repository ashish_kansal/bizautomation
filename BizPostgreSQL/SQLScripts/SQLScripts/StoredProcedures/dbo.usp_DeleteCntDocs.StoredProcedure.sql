/****** Object:  StoredProcedure [dbo].[usp_DeleteCntDocs]    Script Date: 07/26/2008 16:15:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_deletecntdocs')
DROP PROCEDURE usp_deletecntdocs
GO
CREATE PROCEDURE [dbo].[usp_DeleteCntDocs]
	@numDocID as numeric(9)=0   
--
AS
	DELETE FROM  Cntdocuments WHERE numDocID=@numDocID
GO
