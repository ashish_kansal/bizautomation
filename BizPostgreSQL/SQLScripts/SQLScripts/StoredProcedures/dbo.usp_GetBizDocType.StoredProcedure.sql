SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetBizDocType')
DROP PROCEDURE USP_GetBizDocType
GO
CREATE PROCEDURE [dbo].[USP_GetBizDocType]                
@BizDocType as NUMERIC(9)=0,                  
@numDomainID as numeric(9)=0                   
as                    

IF @numDomainID=0 
BEGIN
	select vcData,numListItemID from listdetails where numlistid=27 AND ([numListItemID] =@BizDocType OR @BizDocType =0)
END

ELSE
BEGIN

IF ((SELECT COUNT(*) FROM BizDocFilter WHERE numDomainID=@numDomainID)=0)
BEGIN
	EXEC USP_GetMasterListItems 27,@numDomainID
END
ELSE
BEGIN
  SELECT Ld.numListItemID, isnull(vcRenamedListName,vcData) as vcData 
  FROM BizDocFilter BDF INNER JOIN listdetails Ld ON BDF.numBizDoc=Ld.numListItemID
  left join listorder LO on Ld.numListItemID= LO.numListItemID AND Lo.numDomainId = @numDomainID
  WHERE Ld.numListID=27 AND BDF.tintBizocType=@BizDocType AND BDF.numDomainID=@numDomainID 
  AND (constFlag=1 or Ld.numDomainID=@numDomainID) ORDER BY LO.intSortOrder 
 END
END
GO
   