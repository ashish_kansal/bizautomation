/****** Object:  StoredProcedure [dbo].[USP_BizDocsViewedDtls]    Script Date: 07/26/2008 16:14:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_bizdocsvieweddtls')
DROP PROCEDURE usp_bizdocsvieweddtls
GO
CREATE PROCEDURE [dbo].[USP_BizDocsViewedDtls]
@numContactID as numeric(9)=0,
@numOppBizDocs as numeric(9)=0
as

update OpportunityBizDocs set numViewedBy=@numContactID ,dtViewedDate=getutcdate()
where numOppBizDocsId=@numOppBizDocs
GO
