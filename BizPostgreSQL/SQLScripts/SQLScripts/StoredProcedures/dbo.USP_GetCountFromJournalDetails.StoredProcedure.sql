/****** Object:  StoredProcedure [dbo].[USP_GetCountFromJournalDetails]    Script Date: 07/26/2008 16:17:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created By Siva                                                                                                                                          
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getcountfromjournaldetails')
DROP PROCEDURE usp_getcountfromjournaldetails
GO
CREATE PROCEDURE [dbo].[USP_GetCountFromJournalDetails]           
 @numChartAcntId  as numeric(9)=0,                           
 @numAcntTypeId as numeric(9)=0,                                  
 @numDomainId as numeric(9)=0,  
 @dtToDate as datetime                                                                                                                                                    
                                                                                                                                        
As                                                                                                                                                      
Begin        
   Declare @numAccountIdOpeningEquity as numeric(9)        
   Set @numAccountIdOpeningEquity=(Select numAccountId From Chart_Of_Accounts Where bitOpeningBalanceEquity=1  and numDomainId = @numDomainId)         
                   
                                                                                                                                           
Select  Count(*) From General_Journal_Header  GJH                                                          
 Inner Join  General_Journal_Details GJD on GJH.numJournal_Id=GJD.numJournalId  
where GJD.numChartAcntId in (Select numAccountId From Chart_Of_Accounts Where numAcntTypeID=@numAcntTypeId And numDomainId=@numDomainId And isnull(bitOpeningBalanceEquity,0)=0)            
And GJH.numDomainId=@numDomainId And GJH.datEntry_Date <='' + convert(varchar(300),@dtToDate)       
         
        
End
GO
