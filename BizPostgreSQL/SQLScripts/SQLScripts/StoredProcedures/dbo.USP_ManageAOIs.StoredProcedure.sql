/****** Object:  StoredProcedure [dbo].[USP_ManageAOIs]    Script Date: 07/26/2008 16:19:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_manageaois')
DROP PROCEDURE usp_manageaois
GO
CREATE PROCEDURE [dbo].[USP_ManageAOIs]
@numDomainID as numeric(9)=0,
@numAOIId as numeric(9)=0,
@vcAOIName as varchar(50)='',
@bitDeleted as bit,
@numUserCntID as numeric(9)=0
as

if @bitDeleted=0 
begin

	if @numAOIId=0
	begin
		insert into AOIMaster(vcAOIName,numDomainID,numCreatedBy,bintCreatedDate,numModifiedBy,bintModifiedDate)
		values (@vcAOIName,@numDomainID,@numUserCntID,getutcdate(),@numUserCntID,getutcdate())
	
	end
	else
	begin
		update AOIMaster set vcAOIName=@vcAOIName,numModifiedBy=@numUserCntID,bintModifiedDate=getutcdate()
		where numAOIID=@numAOIId
	end
end
if @bitDeleted=1 
begin
	 delete from AOIContactLink where numAOIID=@numAOIId
	delete from AOIMaster where numAOIId=@numAOIId

end
GO
