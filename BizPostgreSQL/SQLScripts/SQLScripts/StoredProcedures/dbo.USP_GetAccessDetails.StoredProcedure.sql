/****** Object:  StoredProcedure [dbo].[USP_GetAccessDetails]    Script Date: 07/26/2008 16:16:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj      
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getaccessdetails')
DROP PROCEDURE usp_getaccessdetails
GO
CREATE PROCEDURE [dbo].[USP_GetAccessDetails]      
@numSubscriberID as numeric(9)=0,    
@ClientTimeZoneOffset Int,  
@numDomainId as numeric(9)=0       
as      
      
    
select vcFirstName+' '+ vcLastName as [Name],
dtLoggedInTime,
dtLoggedOutTime,  
datediff(minute,dtLoggedInTime,dtLoggedOutTime) as Total,bitPortal from UserAccessedDTL U      
join AdditionalContactsInformation A       
on A.numContactID=U.numContactID      
where numSubscriberID=@numSubscriberID  order by dtLoggedInTime Desc
GO
