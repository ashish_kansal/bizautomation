/****** Object:  StoredProcedure [dbo].[USP_GetMasterListItems]    Script Date: 07/26/2008 16:17:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--modified by anoop jayaraj        
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getmasterlistitems')
DROP PROCEDURE usp_getmasterlistitems
GO
CREATE PROCEDURE [dbo].[USP_GetMasterListItems]          
@ListID as numeric(9)=0,        
@numDomainID as numeric(9)=0          
as          
BEGIN          
	DECLARE @bit3PL BIT, @bitEDI BIT
	SELECT @bit3PL=ISNULL(bit3PL,0),@bitEDI=ISNULL(bitEDI,0) FROM Domain WHERE numDomainID=@numDomainID

	SELECT 
		Ld.numListItemID
		,ISNULL(LDN.vcName,ISNULL(vcRenamedListName,vcData)) AS vcData
		,ISNULL(ld.numListType,0) AS numListType
		,ISNULL(LD.constFlag,0) constFlag
	FROM 
		ListDetails Ld        
	LEFT JOIN 
		ListOrder LO 
	ON 
		Ld.numListItemID= LO.numListItemID 
		AND Lo.numDomainId = @numDomainID
	LEFT JOIN
		ListDetailsName LDN
	ON
		LDN.numDomainID = @numDomainID
		AND LDN.numListID = @ListID
		AND LDN.numListItemID = Ld.numListItemID
	WHERE 
		Ld.numListID=@ListID
		AND 1 = (CASE 
					WHEN LD.numListItemID IN (15445,15446) THEN (CASE WHEN @bit3PL=1 THEN 1 ELSE 0 END)
					WHEN LD.numListItemID IN (15447,15448) THEN (CASE WHEN @bitEDI=1 THEN 1 ELSE 0 END)
					ELSE 1 
				END)
		AND (constFlag=1 OR Ld.numDomainID=@numDomainID)  
	ORDER BY 
		ISNULL(intSortOrder,LD.sintOrder)
END
GO
