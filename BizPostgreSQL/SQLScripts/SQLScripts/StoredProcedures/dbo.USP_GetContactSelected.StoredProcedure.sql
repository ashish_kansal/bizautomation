/****** Object:  StoredProcedure [dbo].[USP_GetContactSelected]    Script Date: 07/26/2008 16:16:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getcontactselected')
DROP PROCEDURE usp_getcontactselected
GO
CREATE PROCEDURE [dbo].[USP_GetContactSelected] 
@numcontractId as numeric(18),

@numDomainId as numeric(18)
as 


select CC.numcontactId,vcEmail,vcFirstName+'-'+vcLastName as Name from contractsContact CC
join AdditionalContactsInformation ACI on ACI.numcontactId = CC.numcontactId
where numcontractId=@numcontractId and CC.numDomainId=@numDomainId
GO
