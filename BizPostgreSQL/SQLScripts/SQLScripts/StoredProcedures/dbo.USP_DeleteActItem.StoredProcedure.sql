/****** Object:  StoredProcedure [dbo].[USP_DeleteActItem]    Script Date: 07/26/2008 16:15:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_deleteactitem')
DROP PROCEDURE usp_deleteactitem
GO
CREATE PROCEDURE [dbo].[USP_DeleteActItem]        
@numCommId as numeric(9)=null        
as        
    
--- to remover the calendar items when an task is deleted    
declare @Activity numeric     
set @Activity= 0    
select @Activity = numActivityId from communication where numCommId = @numCommId    
exec Activity_Rem @DataKey =@Activity    
    
    
Delete  TimeAndExpense where numCaseId = (select caseID from communication where numCommId = @numCommId AND ISNULL (numCaseId,0)>0) AND ISNULL (numCaseId,0)>0 -- deleting the linked casetime entries      
delete recentItems where numRecordID = @numCommId and chrRecordType = 'A'  
DECLARE @numDomainID AS NUMERIC(18,0)
	DECLARE @numDivisionId AS NUMERIC(18,0)
	DECLARE @numSecounds AS NUMERIC(18,0)
	SELECT @numDomainID=numDomainID,@numDivisionId=numDivisionId FROM Communication
	SELECT @numSecounds =numUsedTime FROM ContractsLog WHERE numDivisionID=@numDivisionId  AND numReferenceId=@numCommId
	AND numContractId=(SELECT TOP 1 numContractId FRoM Contracts WHERE numDivisonId=@numDivisionId AND numDomainId=@numDomainID) 

	UPDATE Contracts SET timeUsed=timeUsed-@numSecounds,dtmModifiedOn=GETUTCDATE() WHERE numDivisonId=@numDivisionId AND numDomainId=@numDomainID

	DELETE FROM ContractsLog WHERE numDivisionID=@numDivisionId  AND numReferenceId=@numCommId
	AND numContractId=(SELECT TOP 1 numContractId FRoM Contracts WHERE numDivisonId=@numDivisionId AND numDomainId=@numDomainID)
Delete from Communication where numCommId = @numCommId

--Delete Communication Attendees Activity
SELECT ActivityID,ROW_NUMBER() OVER (ORDER BY numAttendeeId) AS ROWNUMBER INTO #tempAttendees 
        FROM CommunicationAttendees where numCommId = @numCommId AND ActivityID>0
 
DECLARE @maxROWNUMBER AS NUMERIC(9),@minROWNUMBER AS NUMERIC(9)
 
SELECT @maxROWNUMBER = max(ROWNUMBER),@minROWNUMBER = min(ROWNUMBER) FROM #tempAttendees

WHILE @minROWNUMBER <= @maxROWNUMBER
BEGIN
 SET @Activity= 0    

 SELECT @Activity=ActivityID FROM #tempAttendees WHERE ROWNUMBER = @minROWNUMBER  
 
 exec Activity_Rem @DataKey =@Activity    

 SET @minROWNUMBER=@minROWNUMBER + 1
END

Delete from CommunicationAttendees where numCommId = @numCommId

GO
