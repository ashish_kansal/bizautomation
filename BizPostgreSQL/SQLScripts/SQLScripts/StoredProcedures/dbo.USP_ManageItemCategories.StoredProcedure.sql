/****** Object:  StoredProcedure [dbo].[USP_ManageItemCategories]    Script Date: 07/26/2008 16:19:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj  
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_manageitemcategories')
DROP PROCEDURE usp_manageitemcategories
GO
CREATE PROCEDURE [dbo].[USP_ManageItemCategories]  
@numCategoryID as numeric(9)=0,  
@numSubCategory as numeric(9)=0,  
@numItemCode as numeric(9)=0,  
@tintChecked as tinyint=0,  
@byteMode as tinyint =0  
as  
  
if @byteMode=0  
begin  
 if @tintChecked=1  
 begin  
  declare @level as tinyint  
  select @level=tintLevel from Category where numCategoryID=@numCategoryID  
    
  update Category set tintLevel=(@level+1),numDepCategory=@numCategoryID  
         where numCategoryID=@numSubCategory  
 end  
 else  
 begin  
  update Category set tintLevel=1,numDepCategory=0 where numCategoryID=@numSubCategory  
 end  
end  
  
if @byteMode=1  
begin  
 if @tintChecked=1  
 begin  
    if not exists(select * from ItemCategory where numItemID=@numItemCode and numCategoryID=@numCategoryID) 
    insert into ItemCategory(numItemID,numCategoryID) values(@numItemCode,@numCategoryID)  
 end  
 else  
 begin  
  delete from ItemCategory where numItemID=@numItemCode and numCategoryID=@numCategoryID  
 end  
end

if @byteMode=3
begin  
 if @tintChecked=1  
 begin  
    if not exists(select * from ItemCategory where numItemID=@numItemCode and numCategoryID=@numCategoryID) 
		begin		
			insert into ItemCategory(numItemID,numCategoryID) values(@numItemCode,@numCategoryID)  
		end
	else
		begin
			update  ItemCategory set numItemID=@numItemCode , numCategoryID=@numCategoryID where  numItemID=@numItemCode and numCategoryID=@numCategoryID			
			
		end
	END
	else  
	 begin  
	  delete from ItemCategory where numItemID=@numItemCode
	 end  
 end  
 
 if @byteMode=4 --for item category update through csv
begin  
	delete from ItemCategory where numItemID=@numItemCode
	IF @numCategoryID>0 AND (SELECT COUNT(*) FROM item WHERE numItemCode = @numItemCode)>0 AND (SELECT COUNT(*) FROM dbo.Category WHERE numCategoryID = @numCategoryID)>0
	BEGIN
		insert into ItemCategory(numItemID,numCategoryID) values(@numItemCode,@numCategoryID)	
	END
END

GO
