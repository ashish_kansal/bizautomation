/****** Object:  StoredProcedure [dbo].[USP_UpdateCompetitor]    Script Date: 07/26/2008 16:21:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_updatecompetitor')
DROP PROCEDURE usp_updatecompetitor
GO
CREATE PROCEDURE [dbo].[USP_UpdateCompetitor]        
(        
@strFieldList as text='' ,    
@numItemCode as numeric(9)     
)        
as        
        
 declare @hDoc  int        
 EXEC sp_xml_preparedocument @hDoc OUTPUT, @strFieldList        
 update Competitor        
  set vcPrice= X.Price,  
 dtDateEntered=X.[Date]      
              
 from(SELECT  * FROM OPENXML (@hDoc,'/NewDataSet/Table',2)        
 WITH ( CompetitorID numeric(9),        
  Price varchar(100),  
  [Date] datetime      
  ))X        
 where numCompetitorID=X.CompetitorID  and numItemCode=@numItemCode      
       
        
 EXEC sp_xml_removedocument @hDoc
GO
