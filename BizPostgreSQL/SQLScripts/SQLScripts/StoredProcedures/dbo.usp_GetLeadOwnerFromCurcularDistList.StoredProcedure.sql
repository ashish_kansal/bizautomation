/****** Object:  StoredProcedure [dbo].[usp_GetLeadOwnerFromCurcularDistList]    Script Date: 07/26/2008 16:17:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created By: Debasish Tapan Nag                              
--Purpose: Returns the Owner of the Lead for the given Rule      
--Created Date: 08/06/2005                             
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getleadownerfromcurculardistlist')
DROP PROCEDURE usp_getleadownerfromcurculardistlist
GO
CREATE PROCEDURE [dbo].[usp_GetLeadOwnerFromCurcularDistList]      
 @numRoutId Numeric(9)
AS      
BEGIN          
 DECLARE @numEmpId as Int      
 IF (Select bitDefault from RoutingLeads where numRoutId = @numRoutId) = 1      
 BEGIN      
  SELECT @numEmpId = numEmpId from RoutingLeadDetails where  numRoutId = @numRoutId      
 END      
 ELSE      
 BEGIN      
  
  
  
  declare @count tinyint  
  declare @intAssignOrder integer  
 select @count=count(*) from CircularDistList where numRoutID=@numRoutId  
 if @count=0   
begin  
  select @numEmpId=numEmpID FROM RoutingLeadDetails     
  WHERE numRoutID = @numRoutId and intAssignOrder=1  
 insert into CircularDistList(numRoutID,numLastRecOwner) values(@numRoutId,1)  
end  
 else  
begin  
select @intAssignOrder=numLastRecOwner from CircularDistList where numRoutID=@numRoutId  
delete from CircularDistList where numRoutID=@numRoutId  
select @count=count(*) from RoutingLeadDetails where numRoutID=@numRoutId and intAssignOrder=(@intAssignOrder+1)  
  
if @count=0  
begin  
  select @numEmpId=numEmpID FROM RoutingLeadDetails     
  WHERE numRoutID = @numRoutId and intAssignOrder=1  
 insert into CircularDistList(numRoutID,numLastRecOwner) values(@numRoutId,1)  
end  
else  
begin  
select @numEmpId=numEmpID FROM RoutingLeadDetails     
  WHERE numRoutID = @numRoutId and intAssignOrder=(@intAssignOrder+1)  
 insert into CircularDistList(numRoutID,numLastRecOwner) values(@numRoutId,(@intAssignOrder+1))  
end  
end  
    
  
  
  
  
 END      
 SELECT  @numEmpId      
END
GO
