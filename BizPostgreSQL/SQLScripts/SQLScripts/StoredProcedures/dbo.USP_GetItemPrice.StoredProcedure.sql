/****** Object:  StoredProcedure [dbo].[USP_GetItemPrice]    Script Date: 07/26/2008 16:17:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj    
    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getitemprice')
DROP PROCEDURE usp_getitemprice
GO
CREATE PROCEDURE [dbo].[USP_GetItemPrice]          
@numItemCode as numeric(9),                  
@units numeric(9),             
@numDivisionID as numeric(9)=0                  
as              
          
select monListprice from item where numItemCode=@numItemCode
GO
