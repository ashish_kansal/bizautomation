set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go

--created by anoop jayaraj  
--exec USP_GetWebTrafficDetails 1  
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getwebtrafficdetails')
DROP PROCEDURE usp_getwebtrafficdetails
GO
CREATE PROCEDURE [dbo].[USP_GetWebTrafficDetails]  
@numDivisionID as numeric(9)=0,
@numDomainID as numeric(9)=0 
AS  
BEGIN
	SELECT 
		TOP 1
		ISNULL(vcSearchTerm, '-') vcSearchTerm
		,(CASE WHEN vcOrginalRef='-' THEN ISNULL(vcOrginalURL,'-') ELSE ISNULL(vcOrginalRef,'-') END) vcOrginalRef
		,vcUserHostAddress
		,CONVERT(VARCHAR(20), dtCreated) AS StartDate
		,CONVERT(VARCHAR(20), (SELECT TOP 1 dtCreated FROM TrackingVisitorsHDR WHERE numDivisionID = @numDivisionID ORDER BY numTrackingID DESC)) EndDate
		,ISNULL(vcCountry,'') AS vcLocation
		,ISNULL((SELECT TOP 1 vcPageName FROM TrackingVisitorsDTL WHERE  numTracVisitorsHDRID IN (SELECT numTrackingID FROM TrackingVisitorsHDR WHERE numDivisionID=@numDivisionID) ORDER by numTracVisitorsDTLID DESC),'') vcLastPageVisited
	FROM    
		TrackingVisitorsHDR TVHDR
	WHERE
		numDivisionID = @numDivisionID
	ORDER BY 
		numTrackingID ASC  
  
  
	SELECT TOP 10 
		numTrackingID
		,(SELECT COUNT(*) FROM TrackingVisitorsDTL WHERE numTracVisitorsHDRID=TrackingVisitorsHDR.numTrackingID) AS NoOfTimes
		,CONVERT(VARCHAR(8),vcTotalTime) vcTotalTime
		,CONVERT(VARCHAR(20),dtCreated) AS dtCreated  
	FROM 
		TrackingVisitorsHDR 
	WHERE
		numDivisionID=@numDivisionID 
	ORDER BY 
		dtCreated ASC

END