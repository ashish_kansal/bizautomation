/****** Object:  StoredProcedure [dbo].[USP_GetMenuShortCut]    Script Date: 07/26/2008 16:17:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'usp_getmenushortcut' ) 
    DROP PROCEDURE usp_getmenushortcut
GO
CREATE PROCEDURE [dbo].[USP_GetMenuShortCut]
    @numGroupId AS NUMERIC,
    @numDomainId AS NUMERIC,
    @numcontactId AS NUMERIC
AS 
    IF ( SELECT COUNT(*)
         FROM   ShortCutUsrCnf
         WHERE  numGroupId = @numGroupId
                AND numDomainId = @numDomainId
                AND numContactID = @numContactID
                AND ISNULL(numTabId, 0) > 0
       ) > 0 
        BEGIN    
            SELECT  Hdr.id,
                    Hdr.vclinkname AS [Name],
                    Hdr.link,
                    Hdr.numTabId,
                    Hdr.vcImage,
                    ISNULL(Hdr.bitNew, 0) AS bitNew,
                    ISNULL(Hdr.bitNewPopup, 0) AS bitNewPopup,
                    Hdr.NewLink,
                    ISNULL(Hdr.bitPopup, 0) AS bitPopup,
                    sconf.numOrder,
                    ISNULL(sconf.bitFavourite, 0) AS bitFavourite
            FROM    ShortCutUsrcnf Sconf
                    JOIN ShortCutBar Hdr ON Hdr.id = Sconf.numLinkId
                                            AND Sconf.numTabId = Hdr.numTabId
            WHERE   sconf.numDomainId = @numDomainId
                    AND sconf.numGroupID = @numGroupId
                    AND sconf.numContactId = @numContactId
                    AND ISNULL(Sconf.tintLinkType, 0) = 0
            UNION
            SELECT  LD.numListItemID,
                    LD.vcData AS [Name],
                    '../prospects/frmCompanyList.aspx?RelId='
                    + CONVERT(VARCHAR(10), LD.numListItemID) AS link,
                    Sconf.numTabId,
                    NULL AS vcImage,
                    1 AS bitNew,
                    1 AS bitNewPopup,
                    '../include/frmAddOrganization.aspx?RelID='
                    + CONVERT(VARCHAR(10), LD.numListItemID) + '&FormID=36' AS NewLink,
                    0 AS bitPopup,
                    sconf.numOrder,
                    ISNULL(sconf.bitFavourite, 0) AS bitFavourite
            FROM    ShortCutUsrcnf Sconf
                    JOIN ListDetails LD ON LD.numListItemID = Sconf.numLinkId
            WHERE   LD.numListId = 5
                    AND LD.numListItemID <> 46
                    AND ( LD.numDomainID = @numDomainID
                          OR constflag = 1
                        )
                    AND sconf.numDomainId = @numDomainId
                    AND sconf.numGroupID = @numGroupID
                    AND sconf.numContactId = @numContactId
                    AND ISNULL(Sconf.tintLinkType, 0) = 5
            UNION ALL
            SELECT  LD.numListItemID,
                    LD.vcData AS [Name],
                    '../Opportunity/frmOpenBizDocs.aspx?BizDocID='
                    + CONVERT(VARCHAR(10), LD.numListItemID) AS link,
                    Sconf.numTabId,
                    NULL AS vcImage,
                    0 AS bitNew,
                    0 AS bitNewPopup,
                    '' AS NewLink,
                    0 AS bitPopup,
                    sconf.numOrder,
                    ISNULL(sconf.bitFavourite, 0) AS bitFavourite
            FROM    dbo.ListDetails LD
                    JOIN ShortCutUsrcnf Sconf ON LD.numListItemID = Sconf.numLinkId
            WHERE   numListID = 27
                    AND LD.numDomainID = @numDomainID
                    AND ISNULL(constFlag, 0) = 0
					AND Sconf.numContactID = @numContactID
--                    AND LD.numListItemID NOT IN (
--                    SELECT  numLinkId
--                    FROM    ShortCutUsrcnf
--                    WHERE   numDomainId = @numDomainId
--                            AND numContactId = @numContactId )
            UNION ALL
            SELECT  LD.numListItemID,
                    LD.vcData AS [Name],
                    '../Opportunity/frmOpenBizDocs.aspx?BizDocID='
                    + CONVERT(VARCHAR(10), LD.numListItemID) AS link,
                    Sconf.numTabId,
                    NULL AS vcImage,
                    0 AS bitNew,
                    0 AS bitNewPopup,
                    '' AS NewLink,
                    0 AS bitPopup,
                    sconf.numOrder,
                    ISNULL(sconf.bitFavourite, 0) AS bitFavourite
            FROM    dbo.ListDetails LD
                    JOIN ShortCutUsrcnf Sconf ON LD.numListItemID = Sconf.numLinkId
            WHERE   numListID = 27
                    AND LD.numDomainID = @numDomainID
                    AND ISNULL(constFlag, 0) = 1
--                    AND LD.numListItemID NOT IN (
--                    SELECT  numLinkId
--                    FROM    ShortCutUsrcnf
--                    WHERE   numDomainId = @numDomainId
--                            AND numContactId = @numContactId )
            ORDER BY sconf.numOrder       
        END    
    ELSE 
        BEGIN    
            IF ( SELECT COUNT(*)
                 FROM   ShortCutGrpConf
                 WHERE  numDomainId = @numDomainId
                        AND numGroupID = @numGroupId
                        AND ISNULL(numTabId, 0) > 0
               ) > 0 
                BEGIN    
                    SELECT  Hdr.id,
                            Hdr.vclinkname AS [Name],
                            Hdr.link,
                            Hdr.numTabId,
                            Hdr.vcImage,
                            ISNULL(Hdr.bitNew, 0) AS bitNew,
                            ISNULL(Hdr.bitNewPopup, 0) AS bitNewPopup,
                            Hdr.NewLink,
                            ISNULL(Hdr.bitPopup, 0) AS bitPopup,
                            sconf.numOrder,
                            0 AS bitFavourite
                    FROM    ShortCutGrpConf Sconf
                            JOIN ShortCutBar Hdr ON Hdr.id = Sconf.numLinkId
                                                    AND Sconf.numTabId = Hdr.numTabId
                    WHERE   Sconf.numGroupID = @numGroupId
                            AND Sconf.numDomainId = @numDomainId
                            AND Hdr.numContactId = 0
                            AND ISNULL(Sconf.tintLinkType, 0) = 0
                    UNION
                    SELECT  LD.numListItemID,
                            LD.vcData AS [Name],
                            '../Opportunity/frmOpenBizDocs.aspx?BizDocID=' AS link,
                            Sconf.numTabId,
                            NULL AS vcImage,
                            0 AS bitNew,
                            0 AS bitNewPopup,
                            '' AS NewLink,
                            0 AS bitPopup,
                            sconf.numOrder,
                            0 AS bitFavourite
                    FROM    ShortCutGrpConf Sconf
                            JOIN ListDetails LD ON LD.numListItemID = Sconf.numLinkId
                    WHERE   LD.numListId = 5
                            AND LD.numListItemID <> 46
                            AND ( LD.numDomainID = @numDomainID
                                  OR constflag = 1
                                )
                            AND Sconf.numGroupID = @numGroupId
                            AND sconf.numDomainId = @numDomainId
                            AND ISNULL(Sconf.tintLinkType, 0) = 5
                    UNION ALL
                    SELECT  LD.numListItemID,
                            LD.vcData AS [Name],
                            '../Opportunity/frmOpenBizDocs.aspx?BizDocID=' AS link,
                            Sconf.numTabId,
                            NULL AS vcImage,
                            0 AS bitNew,
                            0 AS bitNewPopup,
                            '' AS NewLink,
                            0 AS bitPopup,
                            sconf.numOrder,
                            ISNULL(sconf.bitFavourite, 0) AS bitFavourite
                    FROM    dbo.ListDetails LD
                            JOIN ShortCutUsrcnf Sconf ON LD.numListItemID = Sconf.numLinkId
                    WHERE   numListID = 27
                            AND LD.numDomainID = @numDomainID
                            AND ISNULL(constFlag, 0) = 0
--                    AND LD.numListItemID NOT IN (
--                    SELECT  numLinkId
--                    FROM    ShortCutUsrcnf
--                    WHERE   numDomainId = @numDomainId
--                            AND numContactId = @numContactId )
                    UNION ALL
                    SELECT  LD.numListItemID,
                            LD.vcData AS [Name],
                            '../Opportunity/frmOpenBizDocs.aspx?BizDocID='
                            + CONVERT(VARCHAR(10), LD.numListItemID) AS link,
                            Sconf.numTabId,
                            NULL AS vcImage,
                            0 AS bitNew,
                            0 AS bitNewPopup,
                            '' AS NewLink,
                            0 AS bitPopup,
                            sconf.numOrder,
                            ISNULL(sconf.bitFavourite, 0) AS bitFavourite
                    FROM    dbo.ListDetails LD
                            JOIN ShortCutUsrcnf Sconf ON LD.numListItemID = Sconf.numLinkId
                    WHERE   numListID = 27
                            AND LD.numDomainID = @numDomainID
                            AND ISNULL(constFlag, 0) = 1
--                    AND LD.numListItemID NOT IN (
--                    SELECT  numLinkId
--                    FROM    ShortCutUsrcnf
--                    WHERE   numDomainId = @numDomainId
--                            AND numContactId = @numContactId )
                    ORDER BY sconf.numOrder 
 
                END    
            ELSE 
                BEGIN    
                    SELECT  id,
                            vclinkname AS [Name],
                            link,
                            numTabId,
                            vcImage,
                            ISNULL(bitNew, 0) AS bitNew,
                            ISNULL(bitNewPopup, 0) AS bitNewPopup,
                            NewLink,
                            ISNULL(bitPopup, 0) AS bitPopup,
                            0 AS bitFavourite
                    FROM    ShortCutBar
                    WHERE   bitdefault = 1
                            AND numContactId = 0
                            AND ISNULL(numTabId, 0) > 0
                    UNION
                    SELECT  numListItemID AS id,
                            vcData AS [Name],
                            '../prospects/frmCompanyList.aspx?RelId='
                            + CONVERT(VARCHAR(10), numListItemID) AS link,
                            7 AS numTabId,
                            NULL AS vcImage,
                            0 AS bitNew,
                            1 AS bitNewPopup,
                            '../include/frmAddOrganization.aspx?RelID='
                            + CONVERT(VARCHAR(10), numListItemID)
                            + '&FormID=36' AS NewLink,
                            0 AS bitPopup,
                            0 AS bitFavourite
                    FROM    ListDetails
                    WHERE   numListId = 5
                            AND numListItemID <> 46
                            AND ( numDomainID = @numDomainID
                                  OR constflag = 1
                                )
                    SELECT  numListItemID AS id,
                            vcData AS [Name],
                            '../Opportunity/frmOpenBizDocs.aspx?BizDocID='
                            + CONVERT(VARCHAR(10), numListItemID) AS link,
                            7 AS numTabId,
                            NULL AS vcImage,
                            0 AS bitNew,
                            0 AS bitNewPopup,
                            '' AS NewLink,
                            0 AS bitPopup,
                            0 AS bitFavourite
                    FROM    dbo.ListDetails LD
                            JOIN ShortCutUsrcnf Sconf ON LD.numListItemID = Sconf.numLinkId
                    WHERE   numListID = 27
                            AND LD.numDomainID = @numDomainID
                            AND ISNULL(constFlag, 0) = 0
--                            AND LD.numListItemID NOT IN (
--                            SELECT  numLinkId
--                            FROM    ShortCutUsrcnf
--                            WHERE   numDomainId = @numDomainId
--                                    AND numContactId = @numContactId )
                    UNION ALL
                    SELECT  numListItemID AS id,
                            vcData AS [Name],
                            '../Opportunity/frmOpenBizDocs.aspx?BizDocID=' AS link,
                            7 AS numTabId,
                            NULL AS vcImage,
                            0 AS bitNew,
                            0 AS bitNewPopup,
                            '' AS NewLink,
                            0 AS bitPopup,
                            0 AS bitFavourite
                    FROM    dbo.ListDetails LD
                            JOIN ShortCutUsrcnf Sconf ON LD.numListItemID = Sconf.numLinkId
                    WHERE   numListID = 27
                            AND LD.numDomainID = @numDomainID
                            AND ISNULL(constFlag, 0) = 1
--                            AND LD.numListItemID NOT IN (
--                            SELECT  numLinkId
--                            FROM    ShortCutUsrcnf
--                            WHERE   numDomainId = @numDomainId
--                                    AND numContactId = @numContactId )                                        
                END    
    
        END    
 
GO
