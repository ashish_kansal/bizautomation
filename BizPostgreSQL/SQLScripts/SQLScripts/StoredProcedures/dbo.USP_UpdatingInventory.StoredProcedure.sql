
GO
/****** Object:  StoredProcedure [dbo].[USP_UpdatingInventory]    Script Date: 06/01/2009 23:48:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj       
--exec usp_updatinginventory 0,181193,1,0,1       
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_updatinginventory')
DROP PROCEDURE usp_updatinginventory
GO
CREATE PROCEDURE [dbo].[USP_UpdatingInventory]        
	@byteMode AS TINYINT=0,  
	@numWareHouseItemID AS NUMERIC(18,0)=0,  
	@Units AS FLOAT,
	@numWOId AS NUMERIC(9)=0,
	@numUserCntID AS NUMERIC(9)=0,
	@numOppId AS NUMERIC(9)=0,
	@numAssembledItemID AS NUMERIC(18,0) = 0,
	@fltQtyRequiredForSingleBuild AS FLOAT = 0   
AS      
BEGIN TRY
BEGIN TRANSACTION
	DECLARE @Description AS VARCHAR(200)
	DECLARE @numItemCode NUMERIC(18)
	DECLARE @numDomain AS NUMERIC(18,0)
	DECLARE @ParentWOID AS NUMERIC(18,0)
	DECLARE @numDivisionID NUMERIC(18,0)
	DECLARE @numOverheadServiceItemID NUMERIC(18,0)

	DECLARE @tintCommitAllocation AS TINYINT
	DECLARE @bitAllocateInventoryOnPickList AS BIT

	SELECT @ParentWOID=numParentWOID FROM WorkOrder WHERE numWOId=@numWOId
	SELECT @numItemCode=numItemID,@numDomain = numDomainID from WareHouseItems where numWareHouseItemID = @numWareHouseItemID

	SELECT @numOverheadServiceItemID=ISNULL(numOverheadServiceItemID,0) FROM Domain WHERE numDomainId=@numDomain

	IF ISNULL(@numOppId,0) > 0
	BEGIN
		SELECT @tintCommitAllocation=ISNULL(tintCommitAllocation,1) FROM Domain WHERE numDomainId=@numDomain
		SELECT @bitAllocateInventoryOnPickList=ISNULL(bitAllocateInventoryOnPickList,0) FROM OpportunityMaster INNER JOIN DivisionMaster ON OpportunityMaster.numDivisionId=DivisionMaster.numDivisionID WHERE numOppId=@numOppId
	END
  
	IF @byteMode=0  -- Aseeembly Item
	BEGIN  
		DECLARE @CurrentAverageCost DECIMAL(20,5)
		DECLARE @TotalCurrentOnHand FLOAT
		DECLARE @newAverageCost DECIMAL(20,5)
	
		SELECT @CurrentAverageCost = (CASE WHEN ISNULL(bitVirtualInventory,0) = 1 THEN 0 ELSE monAverageCost END) FROM dbo.Item WHERE numItemCode =@numItemCode 
		SELECT @TotalCurrentOnHand = ISNULL((SUM(numOnHand) + SUM(numAllocation)),0) FROM dbo.WareHouseItems WHERE numItemID =@numItemCode 
		PRINT @CurrentAverageCost
		PRINT @TotalCurrentOnHand

		IF @numWOId > 0
		BEGIN
			DECLARE @monOverheadCost DECIMAL(20,5) 
			DECLARE @monLabourCost DECIMAL(20,5)

			-- Store hourly rate for all task assignee
			UPDATE 
				SPDT
			SET
				SPDT.monHourlyRate = ISNULL(UM.monHourlyRate,0)
			FROM
				StagePercentageDetailsTask SPDT
			INNER JOIN
				UserMaster UM
			ON
				SPDT.numAssignTo = UM.numUserDetailId
			WHERE
				SPDT.numDomainID = @numDomain
				AND SPDT.numWorkOrderId = @numWOID

			SELECT 
				@newAverageCost = SUM(numQtyItemsReq * ISNULL(I.monAverageCost,0))
			FROM 
				WorkOrderDetails WOD
			LEFT JOIN 
				dbo.Item I 
			ON 
				I.numItemCode = WOD.numChildItemID
			WHERE 
				WOD.numWOId = @numWOId
				AND I.charItemType = 'P'

			IF @numOverheadServiceItemID > 0 AND EXISTS (SELECT numWODetailId FROM WorkOrderDetails WHERE numWOId=@numWOId AND numChildItemID=@numOverheadServiceItemID)
			BEGIN
				SET @monOverheadCost = (SELECT SUM(numQtyItemsReq * ISNULL(Item.monListPrice,0)) FROM WorkOrderDetails WOD INNER JOIN Item ON WOD.numChildItemID=Item.numItemCode WHERE WOD.numWOId=@numWOId AND WOD.numChildItemID=@numOverheadServiceItemID)
			END

			SET @monLabourCost = dbo.GetWorkOrderLabourCost(@numDomain,@numWOId)			

			UPDATE WorkOrder SET monAverageCost=@newAverageCost,monLabourCost=ISNULL(@monLabourCost,0),monOverheadCost=ISNULL(@monOverheadCost,0) WHERE numWOId=@numWOId

			SET @newAverageCost = ((@CurrentAverageCost * @TotalCurrentOnHand)  + (@newAverageCost + ISNULL(@monOverheadCost,0) + ISNULL(@monLabourCost,0))) / (@TotalCurrentOnHand + @Units)
		END
		ELSE
		BEGIN
			SELECT 
				@newAverageCost = SUM((numQtyItemsReq * dbo.fn_UOMConversion(ID.numUOMID,I.numItemCode,I.numDomainId,I.numBaseUnit)) * I.monAverageCost )
			FROM 
				dbo.ItemDetails ID
			LEFT JOIN 
				dbo.Item I 
			ON 
				I.numItemCode = ID.numChildItemID
			WHERE 
				numItemKitID = @numItemCode
				AND I.charItemType = 'P'

			UPDATE AssembledItem SET monAverageCost=@newAverageCost WHERE ID=@numAssembledItemID

			SET @newAverageCost = ((@CurrentAverageCost * @TotalCurrentOnHand)  + (@newAverageCost * @Units)) / (@TotalCurrentOnHand + @Units)
		END		
	
		UPDATE item SET monAverageCost = (CASE WHEN ISNULL(bitVirtualInventory,0) = 1 THEN 0 ELSE @newAverageCost END) WHERE numItemCode = @numItemCode

		IF ISNULL(@numWOId,0) > 0
		BEGIN
			UPDATE 
				WareHouseItems 
			SET 
				numOnHand = ISNULL(numOnHand,0) + (CASE WHEN ISNULL(numBackOrder,0) >= @Units THEN 0 ELSE @Units - ISNULL(numBackOrder,0) END)
				,numAllocation = ISNULL(numAllocation,0) + (CASE WHEN ISNULL(numBackOrder,0) > @Units THEN @Units ELSE ISNULL(numBackOrder,0) END)
				,numBackOrder = (CASE WHEN ISNULL(numBackOrder,0) > @Units THEN ISNULL(numBackOrder,0) - @Units ELSE 0 END)  
				,numOnOrder = (CASE WHEN @tintCommitAllocation=2 AND @bitAllocateInventoryOnPickList=0 AND @numOppId > 0 THEN numOnOrder ELSE (CASE WHEN ISNULL(numOnOrder,0) < @Units THEN 0 ELSE ISNULL(numOnOrder,0) - @Units END) END) 
				,dtModified = GETDATE()  
			WHERE 
				numWareHouseItemID=@numWareHouseItemID  
		END
		ELSE
		BEGIN
			UPDATE 
				WareHouseItems 
			SET 
				numOnHand = ISNULL(numOnHand,0) + (CASE WHEN ISNULL(numBackOrder,0) >= @Units THEN  0 ELSE @Units - ISNULL(numBackOrder,0) END)
				,numAllocation = ISNULL(numAllocation,0) + (CASE WHEN ISNULL(numBackOrder,0) > @Units THEN @Units ELSE ISNULL(numBackOrder,0) END)
				,numBackOrder = (CASE WHEN ISNULL(numBackOrder,0) > @Units THEN ISNULL(numBackOrder,0) - @Units ELSE 0 END)
				,dtModified = GETDATE() 
			WHERE 
				numWareHouseItemID=@numWareHouseItemID  
		END
	END  
	ELSE IF @byteMode=1  --Aseembly Child item
	BEGIN  
		DECLARE @onHand as FLOAT                                    
		DECLARE @onOrder as FLOAT                                    
		DECLARE @onBackOrder as FLOAT                                   
		DECLARE @onAllocation as FLOAT    
		DECLARE @monAverageCost AS DECIMAL(20,5)


		SELECT @monAverageCost= (CASE WHEN ISNULL(bitVirtualInventory,0)=1 THEN 0 ELSE ISNULL(monAverageCost,0) END) FROM Item WHERE  numItemCode=@numItemCode

		IF @numWOId > 0
		BEGIN
			UPDATE
				WorkOrderDetails
			SET
				monAverageCost=@monAverageCost
			WHERE
				numWOId=@numWOId
				AND numChildItemID=@numItemCode
				AND numWareHouseItemId=@numWareHouseItemID
		END
		ELSE
		BEGIN
			-- KEEP HISTRIY OF AT WHICH PRICE CHILD ITEM IS USED IN ASSEMBLY BUILDING
			-- USEFUL IN CALCULATING AVERAGECOST WHEN DISASSEMBLING ITEM
			INSERT INTO [dbo].[AssembledItemChilds]
			(
				numAssembledItemID,
				numItemCode,
				numQuantity,
				numWarehouseItemID,
				monAverageCost,
				fltQtyRequiredForSingleBuild
			)
			VALUES
			(
				@numAssembledItemID,
				@numItemCode,
				@Units,
				@numWareHouseItemID,
				@monAverageCost,
				@fltQtyRequiredForSingleBuild
			)
		END

		select                                     
			@onHand=isnull(numOnHand,0),                                    
			@onAllocation=isnull(numAllocation,0),                                    
			@onOrder=isnull(numOnOrder,0),                                    
			@onBackOrder=isnull(numBackOrder,0)                                     
		from 
			WareHouseItems 
		where 
			numWareHouseItemID=@numWareHouseItemID  


		IF ISNULL(@numWOId,0) > 0
		BEGIN
			IF @tintCommitAllocation=2 AND @bitAllocateInventoryOnPickList=0 AND @numOppId > 0 -- Allocation When Fulfillment Order (Packing Slip) is added
			BEGIN
				IF @onHand >= @Units
				BEGIN
					--RELEASE QTY FROM ON HAND
					SET @onHand=@onHand-@Units
				END
				ELSE
				BEGIN
					RAISERROR('NOTSUFFICIENTQTY_ONHAND',16,1)
				END
			END
			ELSE
			BEGIN
				IF @onAllocation >= @Units
				BEGIN
					--RELEASE QTY FROM ALLOCATION
					SET @onAllocation=@onAllocation-@Units
				END
				ELSE
				BEGIN
					RAISERROR('NOTSUFFICIENTQTY_ALLOCATION',16,1)
				END
			END
		END
		ELSE
		BEGIN
			--DECREASE QTY FROM ONHAND
			SET @onHand=@onHand-@Units
		END

		 --UPDATE INVENTORY
		UPDATE 
			WareHouseItems 
		SET      
			numOnHand=@onHand,
			numOnOrder=@onOrder,                         
			numAllocation=@onAllocation,
			numBackOrder=@onBackOrder,
			dtModified = GETDATE()                                    
		WHERE 
			numWareHouseItemID=@numWareHouseItemID   
	END

	IF @numWOId>0
	BEGIN
    
		DECLARE @numReferenceID NUMERIC(18,0)
		DECLARE @tintRefType NUMERIC(18,0)

		IF ISNULL(@numOppId,0)>0 --FROM SALES ORDER 
		BEGIN
			IF @byteMode = 0 --Aseeembly Item
				SET @Description = 'Work Order Completed (Qty: ' + CONVERT(varchar(10),@Units) + ')'
			ELSE IF @byteMode =1 --Aseembly Child item
				IF ISNULL(@ParentWOID,0) = 0
				BEGIN
					SET @Description = 'Items Used In SO-WO (Qty: ' + CONVERT(varchar(10),@Units) + ')'
				END
				ELSE
				BEGIN
					SET @Description = 'Items Used In SO-WO Work Order (Qty: ' + CONVERT(varchar(10),@Units) + ')'
				END

			SET @numReferenceID = @numWOId
			SET @tintRefType = 2
		END
		ELSE --FROM CREATE ASSEMBLY SCREEN FROM ITEM
		BEGIN
			IF @byteMode = 0 --Aseeembly Item
				SET @Description = 'Work Order Completed (Qty: ' + CONVERT(varchar(10),@Units) + ')'
			ELSE IF @byteMode =1 --Aseembly Child item
				SET @Description = 'Items Used In Work Order (Qty: ' + CONVERT(varchar(10),@Units) + ')'

			SET @numReferenceID = @numWOId
			SET @tintRefType=2
		END

		EXEC dbo.USP_ManageWareHouseItems_Tracking
					@numWareHouseItemID = @numWareHouseItemID, --  numeric(9, 0)
					@numReferenceID = @numReferenceID, --  numeric(9, 0)
					@tintRefType = @tintRefType, --  tinyint
					@vcDescription = @Description, --  varchar(100)
					@numModifiedBy = @numUserCntID,
					@numDomainID = @numDomain
	END
	ELSE if @byteMode=0
	BEGIN
	DECLARE @desc AS VARCHAR(100)
	SET @desc= 'Create Assembly:' + CONVERT(varchar(10),@Units) + ' Units';

	  EXEC dbo.USP_ManageWareHouseItems_Tracking
					@numWareHouseItemID = @numWareHouseItemID, --  numeric(9, 0)
					@numReferenceID = @numItemCode, --  numeric(9, 0)
					@tintRefType = 1, --  tinyint
					@vcDescription = @desc, --  varchar(100)
					@numModifiedBy = @numUserCntID,
					@numDomainID = @numDomain
	END
	ELSE if @byteMode=1
	BEGIN
	SET @Description = 'Item Used in Assembly (Qty: ' + CONVERT(varchar(10),@Units) + ')' 

	EXEC dbo.USP_ManageWareHouseItems_Tracking
					@numWareHouseItemID = @numWareHouseItemID, --  numeric(9, 0)
					@numReferenceID = @numItemCode, --  numeric(9, 0)
					@tintRefType = 1, --  tinyint
					@vcDescription = @Description, --  varchar(100)
					@numModifiedBy = @numUserCntID,
					@numDomainID = @numDomain
	END 
COMMIT
END TRY
BEGIN CATCH
	DECLARE @ErrorMessage NVARCHAR(4000)
	DECLARE @ErrorNumber INT
	DECLARE @ErrorSeverity INT
	DECLARE @ErrorState INT
	DECLARE @ErrorLine INT
	DECLARE @ErrorProcedure NVARCHAR(200)

	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;

	SET @numOppID = 0

	SELECT 
		@ErrorMessage = ERROR_MESSAGE(),
		@ErrorNumber = ERROR_NUMBER(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE(),
		@ErrorLine = ERROR_LINE(),
		@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

	RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);
END CATCH