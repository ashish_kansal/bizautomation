/****** Object:  StoredProcedure [dbo].[USP_UpdateChartAcntOpenBal]    Script Date: 07/26/2008 16:21:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created by Siva                      
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_updatechartacntopenbal')
DROP PROCEDURE usp_updatechartacntopenbal
GO
CREATE PROCEDURE [dbo].[USP_UpdateChartAcntOpenBal]                          
@numChartAcntId as numeric(9),                          
@numDomainId as numeric(9),                          
@numOpeningBal as DECIMAL(20,5)                     
as                          
Begin                          
 Declare @numOldOpeningBal as DECIMAL(20,5)                  
 Declare @numNewOpeningBal as DECIMAL(20,5)                  
 Declare @strSQl as varchar(5000)                        
 Declare @strSQl2 as varchar(5000)                 
 Declare @strUpdateOpenEquityBal as varchar(5000)                 
 Declare @numAcntType as numeric(9)                
 Declare @numJournalId as numeric(9)  
 Declare @numAccountIdOpeningEquity as numeric(9)          
 Set @strSQl=''                
 Set @strSQl2=''                
 Set @strUpdateOpenEquityBal=''                 
                     
 Select @numOldOpeningBal= numOriginalOpeningBal,@numAcntType=numAcntTypeID From Chart_Of_Accounts Where numAccountId=@numChartAcntId                        
 Set @numAccountIdOpeningEquity=(Select numAccountId From Chart_Of_Accounts Where bitOpeningBalanceEquity=1  and numDomainId = @numDomainId)   
                     
  Update Chart_Of_Accounts Set numOriginalOpeningBal=@numOpeningBal Where numAccountId=@numChartAcntId And  numDomainId=@numDomainId                
                        
  Set @strSQl=dbo.fn_ParentCategory(@numChartAcntId,@numDomainId)                                                  
  Print ('strSQL'+@strSQl)                      
  If @strSQl <>''                                            
    Begin                 
      Set @strSQl2='update chart_of_accounts  set numopeningbal=isnull(numopeningbal,0) - ' + convert(varchar(30),@numOldOpeningBal) + ' where  numAccountId in ('+convert(varchar(30),@numChartAcntId)+','+@strSQl+') And numDomainId='+Convert(varchar(10),@numDomainId)                                        
  
    
       If @numAcntType=813              
           set @strUpdateOpenEquityBal='update chart_of_accounts  set numopeningbal=isnull(numopeningbal,0) - ' + convert(varchar(30),@numOldOpeningBal) +              
          ' , numOriginalOpeningBal=isnull(numOriginalOpeningBal,0) - ' + convert(varchar(30),@numOldOpeningBal) + ' where  numAccountId='+convert(varchar(10),@numAccountIdOpeningEquity)+' And numDomainId='+Convert(varchar(10),@numDomainId)            
    If @numAcntType=816                                         
           set @strUpdateOpenEquityBal='update chart_of_accounts  set numopeningbal=isnull(numopeningbal,0) + ' + convert(varchar(30),@numOldOpeningBal) +               
           ' , numOriginalOpeningBal=isnull(numOriginalOpeningBal,0) + ' + convert(varchar(30),@numOldOpeningBal) +  ' where  numAccountId='+convert(varchar(10),@numAccountIdOpeningEquity)  +' And numDomainId='+Convert(varchar(10),@numDomainId)                                                   
    End                 
   Else                            
    Begin                                
        set @strSQl2='update chart_of_accounts  set   numopeningbal=isnull(numopeningbal,0) - ' + convert(varchar(30),@numOldOpeningBal) + ' where  numAccountId in ('+convert(varchar(30),@numChartAcntId)+') And numDomainId='+Convert(varchar(10),@numDomainId)                                                
  
    
           If @numAcntType=813               
         set @strUpdateOpenEquityBal='update chart_of_accounts  set numopeningbal=isnull(numopeningbal,0) - ' + convert(varchar(30),@numOldOpeningBal) +               
           ' , numOriginalOpeningBal=isnull(numOriginalOpeningBal,0) - ' + convert(varchar(30),@numOldOpeningBal) +  ' where  numAccountId='+convert(varchar(10),@numAccountIdOpeningEquity) +' And numDomainId='+Convert(varchar(10),@numDomainId)                
          If @numAcntType=816               
            set @strUpdateOpenEquityBal='update chart_of_accounts  set numopeningbal=isnull(numopeningbal,0) + ' + convert(varchar(30),@numOldOpeningBal) +               
           ' , numOriginalOpeningBal=isnull(numOriginalOpeningBal,0) + ' + convert(varchar(30),@numOldOpeningBal) +  ' where  numAccountId='+convert(varchar(10),@numAccountIdOpeningEquity) +' And numDomainId='+Convert(varchar(10),@numDomainId)                
                 
    End                
   Exec(@strSQl2)                                            
   print(@strSQl2)                   
   Exec(@strUpdateOpenEquityBal)                
   print(@strUpdateOpenEquityBal)                                            
  -- To Get the New Value from the table for Update purpose                        
      Select @numNewOpeningBal= numOriginalOpeningBal From Chart_Of_Accounts Where numAccountId=@numChartAcntId    And numDomainId= @numDomainId                    
     -- Print (@numNewOpeningBal)                      
            
     if @strSQl <>''                       
      Begin                             
                     
       set @strSQl2='update chart_of_accounts  set numopeningbal=isnull(numopeningbal,0) + ' + convert(varchar(30),@numNewOpeningBal) + ' where  numAccountId in ('+convert(varchar(30),@numChartAcntId)+','+@strSQl+') And numDomainId='+Convert(varchar(10),@numDomainId)                                       
  
   
          if @numAcntType=813              
           Begin          
             set @strUpdateOpenEquityBal='update chart_of_accounts  set numopeningbal=isnull(numopeningbal,0) + ' + convert(varchar(30),@numNewOpeningBal) +               
             ' , numOriginalOpeningBal=isnull(numOriginalOpeningBal,0) + ' + convert(varchar(30),@numNewOpeningBal) +  ' where  numAccountId='+convert(varchar(10),@numAccountIdOpeningEquity) +' And numDomainId='+Convert(varchar(10),@numDomainId)                                            
          
            -- To update Opening Balance in General_Journal_Header and General_Journal_Details Table          
            Update General_Journal_Header Set numAmount=@numOpeningBal Where numChartAcntId=@numChartAcntId And numDomainId= @numDomainId       
            Select @numJournalId=numJournal_Id From General_Journal_Header Where numChartAcntId=@numChartAcntId And numDomainId= @numDomainId            
            Update General_Journal_Details Set numDebitAmt=@numOpeningBal Where numJournalId=@numJournalId And numDomainId= @numDomainId             
         End          
          if @numAcntType=816              
          Begin          
             set @strUpdateOpenEquityBal='update chart_of_accounts  set numopeningbal=isnull(numopeningbal,0)- ' + convert(varchar(30),@numNewOpeningBal) +               
             ' , numOriginalOpeningBal=isnull(numOriginalOpeningBal,0) - ' + convert(varchar(30),@numNewOpeningBal) +  ' where  numAccountId='+convert(varchar(10),@numAccountIdOpeningEquity)   +' And numDomainId='+Convert(varchar(10),@numDomainId)                                                 
                     
            -- To update Opening Balance in General_Journal_Header and General_Journal_Details Table          
            Update General_Journal_Header Set numAmount=@numOpeningBal Where numChartAcntId=@numChartAcntId And numDomainId= @numDomainId          
            Select @numJournalId=numJournal_Id From General_Journal_Header Where numChartAcntId=@numChartAcntId And numDomainId= @numDomainId          
            Update General_Journal_Details Set numCreditAmt=@numOpeningBal Where numJournalId=@numJournalId  And numDomainId= @numDomainId         
          End             
                      
                 
      End                            
      else                                            
      Begin                  
        print 'sp'            
        set @strSQl2='update chart_of_accounts  set numopeningbal=isnull(numopeningbal,0) + ' + convert(varchar(30),@numNewOpeningBal) + ' where  numAccountId in ('+convert(varchar(30),@numChartAcntId)+')'                                                  
  
    
   if @numAcntType=813                
         Begin          
             set @strUpdateOpenEquityBal='update chart_of_accounts  set numopeningbal=isnull(numopeningbal,0) + ' + convert(varchar(30),@numNewOpeningBal) +               
             ' , numOriginalOpeningBal=isnull(numOriginalOpeningBal,0) + ' + convert(varchar(30),@numNewOpeningBal) + ' where  numAccountId='+convert(varchar(10),@numAccountIdOpeningEquity)+' And numDomainId='+Convert(varchar(10),@numDomainId)                   
            -- To update Opening Balance in General_Journal_Header and General_Journal_Details Table          
            Update General_Journal_Header Set numAmount=@numOpeningBal Where numChartAcntId=@numChartAcntId   And numDomainId= @numDomainId       
            Select @numJournalId=numJournal_Id From General_Journal_Header Where numChartAcntId=@numChartAcntId  And numDomainId= @numDomainId       
            Update General_Journal_Details Set numDebitAmt=@numOpeningBal Where numJournalId=@numJournalId  And numDomainId= @numDomainId       
          End          
          if @numAcntType=816             
          Begin              
             set @strUpdateOpenEquityBal='update chart_of_accounts  set numopeningbal=isnull(numopeningbal,0) - ' + convert(varchar(30),@numNewOpeningBal) +               
             ' , numOriginalOpeningBal=isnull(numOriginalOpeningBal,0) - ' + convert(varchar(30),@numNewOpeningBal) + ' where  numAccountId='+convert(varchar(10),@numAccountIdOpeningEquity) +' And numDomainId='+Convert(varchar(10),@numDomainId)                                                      
                     
            -- To update Opening Balance in General_Journal_Header and General_Journal_Details Table          
            Update General_Journal_Header Set numAmount=@numOpeningBal Where numChartAcntId=@numChartAcntId          
            Select @numJournalId=numJournal_Id From General_Journal_Header Where numChartAcntId=@numChartAcntId          
            Update General_Journal_Details Set numCreditAmt=@numOpeningBal Where numJournalId=@numJournalId          
          End          
      End                
                       
     Exec(@strSQl2)                                            
     print(@strSQl2)                   
 Exec(@strUpdateOpenEquityBal)                
   print(@strUpdateOpenEquityBal)                       
End
GO
