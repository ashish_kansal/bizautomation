/****** Object:  StoredProcedure [dbo].[USP_OPPMilestoneDTLPL]    Script Date: 07/26/2008 16:20:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj                                
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_oppmilestonedtlpl')
DROP PROCEDURE usp_oppmilestonedtlpl
GO
CREATE PROCEDURE [dbo].[USP_OPPMilestoneDTLPL]                                  
(                                  
 @ProcessID numeric(9)=null,                                  
 @numUserCntID numeric(9)=null,                                  
 @DomianID numeric(9)=null,                                  
 @OpportunityID numeric(9)=0                                           
)                                  
as                                  
--begin                                  
--                                  
--if @ProcessID != 0                                   
--begin                                  
-- select  mst.numStagePercentage,                                  
--  dtl.numstagedetailsID,                                  
--  dtl.vcstageDetail,                                  
--  0 AS OppStageID,                                  
--  @DomianID AS numDomainId,                                  
--  @numUserCntID AS numCreatedBy,                                  
--  getutcdate() AS bintCreatedDate,                                  
--  @numUserCntID AS numModifiedBy ,                                   
--  getutcdate() AS bintModifiedDate,                                  
--  getutcdate() AS bintDueDate,                                  
--  null AS bintStageComDate,                                  
--  '' AS vcComments,                                  
--dbo.fn_GetContactName(numAssignTo) as numAssignTo,                                  
--  0 AS bitAlert ,                                  
--  0 AS bitStageCompleted,                                  
--  0 as Op_Flag,                                  
--  dbo.fn_GetContactName(@numUserCntID) as numModifiedByName,                                  
--  0 as numOppStageID,                                  
--  0 as Depend,         
--0 as  numTCategoryHDRID,        
--0 as numECategoryHDRID,        
--  0 as Expense,                                  
--  0 as Time,                                  
--  0 as SubStg,                            
--  '' as numStage   ,            
--isnull(vcStagePercentageDtl,'') as vcStagePercentageDtl,isnull(tintPercentage,0) tintPercentage  ,           
--      
--case when numEvent = 0 then '' else dbo.fn_getCustomDataValue(numEvent,19)end numEvent,      
--case when numReminder=0 then '' else dbo.fn_getCustomDataValue(numReminder,18) end  numReminder,      
--dbo.fn_getTemplateName(numET,@DomianID) numET,      
--dbo.fn_GetListItemName(numActivity) numActivity,      
--case when numStartDate =0 then '' else dbo.fn_getCustomDataValue(numStartDate,16) end numStartDate,      
--dbo.fn_getCustomDataValue(numStartTime,17) numStartTime,      
--case when numStartTimePeriod =1 then 'PM' else 'AM' end numStartTimePeriod,      
--dbo.fn_getCustomDataValue(numEndTime,17) numEndTime,      
--case when numEndTimePeriod =1 then 'PM' else 'AM' end numEndTimePeriod,      
--isnull(txtCom,'') txtCom,              
-- dbo.fn_GetListItemName(numChgStatus)    numChgStatus ,  
--case when bitChgStatus = 1 then 'True' else 'False' end bitChgStatus,  
--case when bitClose = 1 then 'True' else 'False' end bitClose   ,        
-- dbo.fn_GetListItemName(numtype) numtype                              
-- from StagePercentageMaster mst                                  
-- join StagePercentageDetails dtl                                   
-- on dtl.numstagepercentageid=mst.numstagepercentageid                                  
-- where dtl.slp_id=@ProcessID                                  
-- order by numStagePercentage,numOppstageid                                  
--end                                  
--else                                  
--begin                                  
--                                  
-- select  numStagePercentage,                                  
--  numOppStageID as numstagedetailsID,                                  
--  vcstageDetail,                                  
--  numOppStageID as OppStageID,                                  
--  numDomainId,                                  
--  numCreatedBy,             
--  bintCreatedDate,                                  
--  numModifiedBy ,                                   
--  bintModifiedDate,                                  
--  bintDueDate,                                  
--  bintStageComDate,                                  
--  vcComments,                                  
-- dbo.fn_GetContactName(numAssignTo) as numAssignTo,                               
--  bitAlert ,                                  
--  bitStageCompleted,                                  
--  0 as Op_Flag,                                  
--  dbo.fn_GetContactName(numModifiedBy) as numModifiedByName,                                  
--  numOppStageID,        
--  isnull((select [numCategoryHDRID] from TimeAndExpense where numDomainId = @DomianID and        
--  numOppid=@OpportunityID and numStageId = numOppStageID and tintTEType=1 and numCategory = 1),0) as numTCategoryHDRID,        
--  isnull((select [numCategoryHDRID] from TimeAndExpense where numDomainId = @DomianID and        
--  numOppid=@OpportunityID and numStageId = numOppStageID and tintTEType=1 and numCategory = 2),0) as numECategoryHDRID,        
--        
--  dbo.fn_GetDepDtlsbySalesStgID(@OpportunityID,numOppStageID,0) as Depend,                                  
--  dbo.fn_GetExpenseDtlsbySalesStgID(@OpportunityID,numOppStageID,0) as Expense,                                  
--  dbo.fn_GeTimeDtlsbySalesStgID(@OpportunityID,numOppStageID,0) as [Time],                                  
--  dbo.fn_GetSubStgDtlsbySalesStgID(@OpportunityID,numOppStageID,0) as SubStg,                            
--(select vcData from listdetails where numlistitemid= isnull(numStage,0)) as numStage ,            
--isnull(vcStagePercentageDtl,'') as vcStagePercentageDtl,isnull(tintPercentage,0) tintPercentage  ,           
--      
--case when numEvent = 0 then '' else dbo.fn_getCustomDataValue(numEvent,19)end numEvent,      
--case when numReminder=0 then '' else dbo.fn_getCustomDataValue(numReminder,18) end  numReminder,      
--dbo.fn_getTemplateName(numET,@DomianID) numET,      
--dbo.fn_GetListItemName(numActivity) numActivity,      
--case when numStartDate =0 then '' else dbo.fn_getCustomDataValue(numStartDate,16) end numStartDate,      
--dbo.fn_getCustomDataValue(numStartTime,17) numStartTime,      
--case when numStartTimePeriod =1 then 'PM' else 'AM' end numStartTimePeriod,      
--dbo.fn_getCustomDataValue(numEndTime,17) numEndTime,      
--case when numEndTimePeriod =1 then 'PM' else 'AM' end numEndTimePeriod,      
--isnull(txtCom,'') txtCom  ,  
--dbo.fn_GetListItemName(numChgStatus)    numChgStatus ,  
--case when bitChgStatus = 1 then 'True' else 'False' end bitChgStatus,  
--case when bitClose = 1 then 'True' else 'False' end bitClose  ,  
-- dbo.fn_GetListItemName(numtype) numtype                         
-- from OpportunityStageDetails                                   
-- where numOppId=@OpportunityID and numDomainID=@DomianID  order by numStagePercentage,numOppstageid                                  
--                                   
--                                  
--                            
--                                  
--end                                  
--                                  
--end
GO
