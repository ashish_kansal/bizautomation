/****** Object:  StoredProcedure [dbo].[usp_caseTime1]    Script Date: 07/26/2008 16:15:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_casetime1')
DROP PROCEDURE usp_casetime1
GO
CREATE PROCEDURE [dbo].[usp_caseTime1]                
(                
@byteMode  as tinyint,                
@numCaseId as numeric(9),     
@numCaseTimeId as numeric,    
@numRate as DECIMAL(20,5),                
@vcDesc as varchar(1000),            
@bitBillable as bit,     
@numcontractId as numeric(9),       
@dtStartTime as datetime,      
@dtEndTime as datetime      
     
)                
as                
                
if @byteMode= 0                
begin                
  DECLARE @RetCaseTimeId integer              
delete from CaseTime where numCaseId=@numCaseId and numCaseTimeId=@numCaseTimeId               
insert into CaseTime                
(                
numCaseId,                
numRate,                
vcDesc,            
bitBill,        
numContractId,      
dtStartTime,      
dtEndTime      
)                
values                
(                
@numCaseId,                           
@numRate,               
@vcDesc,            
@bitBillable,        
@numcontractId ,      
@dtStartTime,      
@dtEndTime         
)                
       
  SELECT    
   @numCaseTimeId = SCOPE_IDENTITY();      
    
    
Set @RetCaseTimeId = SCOPE_IDENTITY()                        
 Select  @numCaseTimeId        

end
              
if @byteMode= 1              
begin              
select numCaseId,              
numCaseTimeId,              
numRate,              
vcDesc,      
bitBill,      
numContractId ,     
dtStartTime,    
dtEndTime        
 from CaseTime              
where numCaseId=@numCaseId and numCaseTimeId=@numCaseTimeId              
end            
        
if @byteMode= 2           
begin              
              
delete from caseTime where numCaseId=@numCaseId and numCaseTimeId=@numCaseTimeId                    
              
  
end
GO
