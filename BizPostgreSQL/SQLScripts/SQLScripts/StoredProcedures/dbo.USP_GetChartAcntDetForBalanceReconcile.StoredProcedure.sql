/****** Object:  StoredProcedure [dbo].[USP_GetChartAcntDetForBalanceReconcile]    Script Date: 07/26/2008 16:16:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created by Siva       
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getchartacntdetforbalancereconcile')
DROP PROCEDURE usp_getchartacntdetforbalancereconcile
GO
CREATE PROCEDURE [dbo].[USP_GetChartAcntDetForBalanceReconcile]             
@numDomainID as numeric(9)=0          
As            
Begin          
---- Declare @numParentAcntID as numeric(9)        
---- Set @numParentAcntID=(Select numAccountId From Chart_Of_Accounts Where numParntAcntId is null and numAcntType is null and numDomainId = @numDomainId) --and numAccountId = 1       
----       
----         
---- Delete from HierChartOfAct where numDomainID=@numDomainID            
---- exec USP_ArranageHierChartAct @numParentAcntID,@numDomainID            
----             
---- select numChartOfAcntID,vcCategoryName from HierChartOfAct            
---- where numDomainID=@numDomainID and  numChartOfAcntID<> @numParentAcntID  And numAcntType not in (814,815,822,823,824,825,826)    

Declare @strSQL as varchar(8000)        
    
Set @strSQL='  with RecursionCTE (RecordID,ParentRecordID,vcCatgyName,TOC,T)                      
 as                      
 (                      
 select numAccountId,numParntAcntId,vcCatgyName,convert(varchar(1000),'''') TOC,convert(varchar(1000),'''') T     
                     
    from Chart_Of_Accounts                      
    where numParntAcntId is null and numDomainID='+Convert(varchar(10),@numDomainId)                   
    +' union all                      
   select R1.numAccountId,                      
          R1.numParntAcntId,                      
    R1.vcCatgyName,                      
          case when DataLength(R2.TOC) > 0                      
                    then convert(varchar(1000),case when CHARINDEX(''.'',R2.TOC)>0 then R2.TOC else R2.TOC +''.'' end                      
                                 + cast(R1.numAccountId as varchar(10)))                       
       else convert(varchar(1000),cast(R1.numAccountId as varchar(10)))                       
                    end as TOC,case when DataLength(R2.TOC) > 0                      
                    then  convert(varchar(1000),''..''+ R2.T)                      
else convert(varchar(1000),'''')                      
                    end as T                       
      from Chart_Of_Accounts as R1                            
      join RecursionCTE as R2 on R1.numParntAcntId = R2.RecordID and R1.numAcntType not in (814,815,822,823,824,825,826) and R1.numDomainID='+Convert(varchar(10),@numDomainId) +'                       
  )                       
select RecordID as numChartOfAcntID,T+vcCatgyName as vcCategoryName,TOC  from RecursionCTE Where ParentRecordID is not null order by TOC asc'    
  print (@strSQL)  
 Exec (@strSQL)  
End
GO
