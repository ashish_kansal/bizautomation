/****** Object:  StoredProcedure [dbo].[USP_ReportCases]    Script Date: 07/26/2008 16:20:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- created by Anoop Jayaraj                                  
                                  
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_reportcases')
DROP PROCEDURE usp_reportcases
GO
CREATE PROCEDURE [dbo].[USP_ReportCases]                                  
   @numDomainID numeric,                                        
        @dtFromDate datetime,                                        
        @dtToDate datetime,                                        
        @numUserCntID numeric=0,                                             
        @tintType numeric,                                        
        @tintRights numeric=0,                                  
        @status as integer                                 
as                                  
                                  
Declare @strSQl as varchar(2000)                                  
If @tintRights=1                                  
Begin   
           
If @status=0             
Begin                                
 set @strSQl= 'select A.numContactID as vcAssignTo ,vcFirstname+'' ''+vcLastname  +''(''+                                  
 convert(varchar(15),(select count(*) from cases C where C.numAssignedTo=A.numContactID and C.bintCreatedDate between '''+ convert(varchar(20),@dtFromDate) +''' and'''+convert(varchar(20),@dtToDate)+'''  ))+'')'' as [Nam]                               
 from  AdditionalContactsInformation A                                                               
 where   A.numContactID='+convert(varchar(15),@numUserCntID)+' and A.numDomainid='+convert(varchar(15),@numDomainId) +' and A.numContactID in (select numAssignedTo from cases where bintCreatedDate between '''+ convert(varchar(20),@dtFromDate) +''' and '''+convert(varchar(20),@dtToDate)+''')'              
End            
If @status<>0            
Begin            
 set @strSQl= '
select A.numContactID as vcAssignTo ,vcFirstname+'' ''+vcLastname  +''(''+                                  
 convert(varchar(15),(select count(*) from cases C where C.numAssignedTo=A.numContactID and C.numStatus ='+ convert(varchar(15),@status) +' and C.bintCreatedDate between '''+ convert(varchar(20),@dtFromDate) +''' and'''+convert(varchar(20),@dtToDate)+'''  ))+'')'' as [Nam]                               
 from  AdditionalContactsInformation A                                                               
 where   A.numContactID='+convert(varchar(15),@numUserCntID)+' and A.numDomainid='+convert(varchar(15),@numDomainId) +' and A.numContactID in (select numAssignedTo from cases where bintCreatedDate between '''+ convert(varchar(20),@dtFromDate) +''' and '''+convert(varchar(20),@dtToDate)+''' )'                 
            
End                                  
                                                        
Print @strSQl                       
Exec (@strSQl)                                  
                                  
End                                  
If @tintRights=2                                  
Begin                        
                    
--on A.numContactID= C1.numAssignedTo              
If  @status=0            
Begin
 set @strSQl= 'select A.numContactID as vcAssignTo ,vcFirstname+'' ''+vcLastname  +''(''+                                  
 convert(varchar(15),(select count(*) from cases C where C.numAssignedTo=A.numContactID and C.bintCreatedDate between '''+ convert(varchar(20),@dtFromDate) +''' and'''+convert(varchar(20),@dtToDate)+'''  ))+'')'' as [Nam]                               
 from  AdditionalContactsInformation A                                                               
 where   A.numContactID='+convert(varchar(15),@numUserCntID) +' and A.numContactID in (select numAssignedTo from cases where bintCreatedDate between '''+ convert(varchar(20),@dtFromDate) +''' and '''+convert(varchar(20),@dtToDate)+''')' +' And A.numTeam in(select F.numTeam from ForReportsByTeam F       -- Added By Anoop                   
 where F.numUserCntID='+convert(varchar(15),@numUserCntID)+' and A.numDomainid='+convert(varchar(15),@numDomainId)+' and F.tintType='+convert(varchar(5),@tintType)+')'                                  

                                                         
End          
If @status<>0            
Begin   

 set @strSQl= 'select A.numContactID as vcAssignTo ,vcFirstname+'' ''+vcLastname  +''(''+                                  
 convert(varchar(15),(select count(*) from cases C where C.numAssignedTo=A.numContactID  and C.numStatus ='+ convert(varchar(15),@status) +' and C.bintCreatedDate between '''+ convert(varchar(20),@dtFromDate) +''' and'''+convert(varchar(20),@dtToDate)+'''  ))+'')'' as [Nam]                               
 from  AdditionalContactsInformation A                                                               
 where   A.numContactID='+convert(varchar(15),@numUserCntID) +' and A.numContactID in (select numAssignedTo from cases where bintCreatedDate between '''+ convert(varchar(20),@dtFromDate) +''' and '''+convert(varchar(20),@dtToDate)+''')' +' And A.numTeam in(select F.numTeam from ForReportsByTeam F       -- Added By Anoop                   
 where F.numUserCntID='+convert(varchar(15),@numUserCntID)+' and A.numDomainid='+convert(varchar(15),@numDomainId)+' and F.tintType='+convert(varchar(5),@tintType)+')'        

End          
                                                                
Print @strSQl            
Exec (@strSQl)                                  
End                                  
                                  
If @tintRights=3                                  
Begin                           
If @status=0              
Begin    
 set @strSQl= 'select A.numContactID as vcAssignTo ,vcFirstname+'' ''+vcLastname  +''(''+                                  
 convert(varchar(15),(select count(*) from cases C
 join DivisionMaster DM on DM.numDivisionID = C.numDivisionID 
 where   DM.numTerId   in(select F.numTerritory from ForReportsByTerritory F      -- Added By Anoop                                     
 where F.numUserCntID='+convert(varchar(15),@numUserCntID)+' and F.numDomainid='+convert(varchar(15),@numDomainID)+' and F.tintType='+convert(varchar(5),@tintType)+') and C.numAssignedTo=A.numContactID and C.bintCreatedDate between '''+ convert(varchar(20),@dtFromDate) +''' and'''+convert(varchar(20),@dtToDate)+'''  ))+'')'' as [Nam]                               
 from  AdditionalContactsInformation A                                                               
 where  A.numContactID in (select numAssignedTo from cases where bintCreatedDate between '''+ convert(varchar(20),@dtFromDate) +''' and '''+convert(varchar(20),@dtToDate)+''')'

End          
If @status<>0            
Begin    

set @strSQl= 'select A.numContactID as vcAssignTo ,vcFirstname+'' ''+vcLastname  +''(''+                                  
 convert(varchar(15),(select count(*) from cases C
 join DivisionMaster DM on DM.numDivisionID = C.numDivisionID 
 where C.numStatus ='+ convert(varchar(15),@status) +' and DM.numTerId   in(select F.numTerritory from ForReportsByTerritory F      -- Added By Anoop                                     
 where F.numUserCntID='+convert(varchar(15),@numUserCntID)+' and F.numDomainid='+convert(varchar(15),@numDomainID)+' and F.tintType='+convert(varchar(5),@tintType)+') and C.numAssignedTo=A.numContactID and C.bintCreatedDate between '''+ convert(varchar(20),@dtFromDate) +''' and'''+convert(varchar(20),@dtToDate)+'''  ))+'')'' as [Nam]                               
 from  AdditionalContactsInformation A                                                               
 where  A.numContactID in (select numAssignedTo from cases where bintCreatedDate between '''+ convert(varchar(20),@dtFromDate) +''' and '''+convert(varchar(20),@dtToDate)+''')'

  
End          
          
                                
Print @strSQl            
Exec (@strSQl)                                  
End
GO
