/****** Object:  StoredProcedure [dbo].[Usp_GetTabMenu]    Script Date: 07/26/2008 16:18:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Modified By Anoop Jayaraj    
              
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_gettabmenu')
DROP PROCEDURE usp_gettabmenu
GO
CREATE PROCEDURE [dbo].[Usp_GetTabMenu]                
(                
@numListID as  numeric(9)=0,      
@numDomainID as numeric(9)=0 ,
@bitMode as bit=0               
)                
as      
if    @bitmode=1
 begin    
  SELECT 6 parentid ,numListItemID, vcData,'../prospects/frmCompanyList.aspx?RelId='+    
  convert(varchar(10),numListItemID) vcNavURL,null as vcImageURL    
  FROM ListDetails WHERE numListId = @numListID and numListItemID<>46       
  and(numDomainID=@numDomainID or constflag=1)      
      
  union    
    
  select    FRDTL.numPrimaryListItemID as ParentID,
            0 as numlistitemid,
            L2.vcData as vcPageNavName,
            '../prospects/frmCompanyList.aspx?RelId='
            + convert(varchar(10), FRDTL.numPrimaryListItemID) + '&profileid='
            + convert(varchar(10), numSecondaryListItemID) as vcNavURL,
            null as vcImageURL
  from      FieldRelationship FR
            join FieldRelationshipDTL FRDTL on FRDTL.numFieldRelID = FRDTL.numFieldRelID
            join ListDetails L1 on numPrimaryListItemID = L1.numListItemID
            join ListDetails L2 on numSecondaryListItemID = L2.numListItemID
  where     numPrimaryListItemID <> 46
            and FR.numPrimaryListID = 5
            and FR.numSecondaryListID = 21
            and FR.numDomainID = @numDomainID
            and L2.numDomainID = @numDomainID
  order by  parentid    
 end     
else    
 begin    
     
  SELECT numListItemID, vcData FROM ListDetails WHERE numListId = @numListID and numListItemID<>46       
  and(numDomainID=@numDomainID or constflag=1)      
  ORDER BY vcData      
    
 end
GO
