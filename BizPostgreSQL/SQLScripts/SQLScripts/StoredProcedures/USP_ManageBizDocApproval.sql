
/****** Object:  StoredProcedure [dbo].[USP_ManageBizDocApproval]    Script Date: 02/19/2010 17:44:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_managebizdocapproval')
DROP PROCEDURE usp_managebizdocapproval
GO
CREATE PROCEDURE [dbo].[USP_ManageBizDocApproval]
@intMode int,
@numDomainID int,
@numBizDocAppID numeric(18,0)=0,
@numBizDocTypeID numeric(18,0)=0,
@numRangeFrom numeric(18,2)=0,
@numRangeTo numeric(18,2)=0,
@btBizDocCreated bit=0,
@btBizDocPaidFull bit=0,
@numActionTypeID numeric(18,0)=0,
@numApproveDocStatusID numeric(18,0)=0,
@numDeclainDocStatusID numeric(18,0)=0

as

if @intMode=0
	begin
			DELETE FROM BizDocApprovalRuleEmployee WHERE numBizDocAppID 
			in (select numBizDocAppID from BizDocApprovalRule where numDomainID=@numDomainID);

			DELETE FROM BizDocStatusApprove WHERE numDomainID=@numDomainID;
			DELETE FROM BizDocApprovalRule WHERE numDomainID=@numDomainID;

			INSERT INTO BizDocApprovalRule SELECT @numBizDocTypeID,@numDomainID,@numRangeFrom,@numRangeTo,
								@btBizDocCreated,@btBizDocPaidFull,@numActionTypeID,@numApproveDocStatusID,
								@numDeclainDocStatusID
			SELECT SCOPE_IDENTITY()
	end
else if @intMode>0
	begin
			INSERT INTO BizDocApprovalRule SELECT @numBizDocTypeID,@numDomainID,@numRangeFrom,@numRangeTo,
								@btBizDocCreated,@btBizDocPaidFull,@numActionTypeID,@numApproveDocStatusID,
								@numDeclainDocStatusID
			SELECT SCOPE_IDENTITY()	
	end
else if @intMode=-1
	begin
			DELETE FROM BizDocApprovalRuleEmployee WHERE numBizDocAppID 
			in (select numBizDocAppID from BizDocApprovalRule where numDomainID=@numDomainID);

			DELETE FROM BizDocStatusApprove WHERE numDomainID=@numDomainID;
			DELETE FROM BizDocApprovalRule WHERE numDomainID=@numDomainID;
			SELECT 1
	end