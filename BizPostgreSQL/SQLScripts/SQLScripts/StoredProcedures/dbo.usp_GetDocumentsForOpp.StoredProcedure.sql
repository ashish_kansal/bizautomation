/****** Object:  StoredProcedure [dbo].[usp_GetDocumentsForOpp]    Script Date: 07/26/2008 16:17:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getdocumentsforopp')
DROP PROCEDURE usp_getdocumentsforopp
GO
CREATE PROCEDURE [dbo].[usp_GetDocumentsForOpp]
@numOppID numeric(9)=0,
@numDocID numeric(9)=0   
--
AS
BEGIN

	if @numDocID=0
	begin
	select vcFileName,vcDisplayName,numSize,bintDateOfUpload,numDocID,numuserid from opptdocuments OD INNER JOIN DivisionMaster DM ON OD.numDivisionID=DM.numDivisionID
	 where OD.numOppID=@numOppID 
	Order By OD.vcDisplayName
	end
	else
	begin
		select vcFileName,vcDisplayName,numSize,bintDateOfUpload,numDocID,numuserid from opptdocuments OD INNER JOIN DivisionMaster DM ON OD.numDivisionID=DM.numDivisionID
		 where OD.numDocID=@numDocID
		 
	end
	
END
GO
