
GO
/* 
UnComment this  Only for dubug purpose
UPDATE item SET numAssetChartAcntId=NULL,numCOGsChartAcntId=NULL,numIncomeChartAcntId=NULL WHERE numDomainID=@numDomainId
DELETE FROM dbo.AccountingCharges WHERE numDomainID=@numDomainId                                
DELETE FROM dbo.COAShippingMapping WHERE numDomainID=@numDomainId
DELETE FROM dbo.COARelationships WHERE numDomainID=@numDomainId
DELETE FROM dbo.ChartAccountOpening WHERE numDomainId=@numDomainId
DELETE FROM dbo.Chart_Of_Accounts WHERE numDomainId=@numDomainId
DELETE FROM dbo.AccountTypeDetail WHERE numDomainId=@numDomainId
*/    
--exec USP_ChartAcntDefaultValues @numDomainId=204,@numUserCntId=116784
--SELECT * FROM chart_of_Accounts where numDomainID = 183
--select * from AccountTypeDetail where numDomainID=110
     
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_ChartAcntDefaultValues' ) 
    DROP PROCEDURE USP_ChartAcntDefaultValues
GO
CREATE PROCEDURE [dbo].[USP_ChartAcntDefaultValues]
    @numDomainId AS NUMERIC(9) = 0,
    @numUserCntId AS NUMERIC(9) = 0
AS 
    BEGIN        
 
        DECLARE @intCount AS NUMERIC(9)   
        DECLARE @dtTodayDate AS DATETIME
        SET @dtTodayDate = GETDATE()                                                 
        SET @intCount = 0                                  
                                           
        SELECT  @intCount = COUNT(*)
        FROM    Chart_Of_Accounts
        WHERE   numDomainId = @numDomainId                                                    
        
        
        IF @intCount = 0 
            BEGIN      
----INSERTING DEFAULT ACCOUNT TYPES
                EXEC USP_AccountTypeDefaultValue @numDomainId = @numDomainId ;


 --Check for Current Fiancial Year
        IF ( SELECT COUNT([numFinYearId])
             FROM   [FinancialYear]
             WHERE  [numDomainId] = @numDomainId
                    AND [bitCurrentYear] = 1
           ) = 0 
            BEGIN
            
				DECLARE @dtPeriodFrom AS DATETIME,@dtPeriodTo AS DATETIME
				
				SELECT	@dtPeriodFrom=DATEADD(YEAR, DATEDIFF(YEAR, 0, GETDATE()), 0),
						@dtPeriodTo=DATEADD(YEAR, DATEDIFF(YEAR, -1, GETDATE()), -1)
	
				EXEC dbo.USP_ManageFinancialYear
					@numDomainId = @numDomainId, --  numeric(9, 0)
					@dtPeriodFrom = @dtPeriodFrom, --  datetime
					@dtPeriodTo = @dtPeriodTo, --  datetime
					@vcFinYearDesc = '', --  varchar(50)
					@bitCloseStatus = 0, --  bit
					@bitAuditStatus = 0, --  bit
					@bitCurrentYear = 1 --  bit
            END
            
                


DECLARE @numAssetAccountTypeID AS NUMERIC(9,0)
DECLARE @numLiabilityAccountTypeID  AS NUMERIC(9,0)
DECLARE @numIncomeAccountTypeID AS NUMERIC(9,0) 
DECLARE @numExpenseAccountTypeID  AS NUMERIC(9,0)
DECLARE @numEquityAccountTypeID  AS NUMERIC(9,0)
DECLARE @numCOGSAccountTypeID  AS NUMERIC(9,0)

SELECT @numAssetAccountTypeID = numAccountTypeId FROM dbo.AccountTypeDetail WHERE vcAccountCode ='0101' AND numDomainID = @numDomainID 
SELECT @numLiabilityAccountTypeID = numAccountTypeId FROM dbo.AccountTypeDetail WHERE vcAccountCode ='0102' AND numDomainID = @numDomainID 
SELECT @numIncomeAccountTypeID = numAccountTypeId FROM dbo.AccountTypeDetail WHERE vcAccountCode ='0103' AND numDomainID = @numDomainID 
SELECT @numExpenseAccountTypeID = numAccountTypeId FROM dbo.AccountTypeDetail WHERE vcAccountCode ='0104' AND numDomainID = @numDomainID 
SELECT @numEquityAccountTypeID = numAccountTypeId FROM dbo.AccountTypeDetail WHERE vcAccountCode ='0105' AND numDomainID = @numDomainID  

DECLARE @numLoanAccountTypeID AS NUMERIC(9)
SELECT @numLoanAccountTypeID = numAccountTypeID FROM dbo.AccountTypeDetail WHERE vcAccountCode = '01020201'  AND numDomainID = @numDomainID  
PRINT @numLoanAccountTypeID

DECLARE @numParentForCOGSAccID AS NUMERIC(9)
SELECT @numParentForCOGSAccID = numParentID FROM dbo.AccountTypeDetail WHERE numDomainID = @numDomainID AND vcAccountCode = '0101'
PRINT @numParentForCOGSAccID


SELECT @numCOGSAccountTypeID = numAccountTypeId FROM dbo.AccountTypeDetail WHERE vcAccountCode ='0106' AND numDomainID = @numDomainID  
DECLARE @numRSccountTypeID AS NUMERIC(9)
SELECT @numRSccountTypeID = numAccountTypeID FROM dbo.AccountTypeDetail WHERE vcAccountCode = '01050101'  AND numDomainID = @numDomainID  
PRINT @numRSccountTypeID

EXEC usp_InsertNewChartAccountDetails @numAcntTypeId = @numEquityAccountTypeID,
    @numParntAcntTypeID = @numRSccountTypeID, @vcAccountName = 'Profit & Loss',
    @vcAccountDescription = 'Profit & Loss',
    @monOriginalOpeningBal = $0, @monOpeningBal = $0,
    @dtOpeningDate = @dtTodayDate, @bitActive = 1, @numAccountId = 0,
    @bitFixed = 0, @numDomainID = @numDomainID   , @numListItemID = 0, @numUserCntId = 1,
    @bitProfitLoss = 1, @bitDepreciation = 0,
    @dtDepreciationCostDate = '1753-01-01', @monDepreciationCost = $0.0000,
    @vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, @bitIsSubAccount = 0, @numParentAccId = 0
   
PRINT 18
EXEC usp_InsertNewChartAccountDetails @numAcntTypeId = @numEquityAccountTypeID,
    @numParntAcntTypeID = @numRSccountTypeID, @vcAccountName = 'Retained Earnings',
    @vcAccountDescription = 'Retained Earnings', @monOriginalOpeningBal = $0,
    @monOpeningBal = $0, @dtOpeningDate = @dtTodayDate, @bitActive = 1,
    @numAccountId = 0, @bitFixed = 0, @numDomainID = @numDomainID   , @numListItemID = 0,
    @numUserCntId = 1, @bitProfitLoss = 0, @bitDepreciation = 0,
    @dtDepreciationCostDate = '1753-01-01', @monDepreciationCost = $0.0000,
    @vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, @bitIsSubAccount = 0, @numParentAccId = 0


EXEC usp_InsertNewChartAccountDetails @numAcntTypeId = @numEquityAccountTypeID,
    @numParntAcntTypeID = @numRSccountTypeID, @vcAccountName = 'Opening Balance Equity',
    @vcAccountDescription = 'Opening Balance Equity', @monOriginalOpeningBal = $0,
    @monOpeningBal = $0, @dtOpeningDate = @dtTodayDate, @bitActive = 1,
    @numAccountId = 0, @bitFixed = 0, @numDomainID = @numDomainID   , @numListItemID = 0,
    @numUserCntId = 1, @bitProfitLoss = 0, @bitDepreciation = 0,
    @dtDepreciationCostDate = '1753-01-01', @monDepreciationCost = $0.0000,
    @vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, @bitIsSubAccount = 0, @numParentAccId = 0

   
------------------------------------ ASSETS ENTRY --------------------------------------------
    
DECLARE @numBankAccountTypeID AS NUMERIC(9)
SELECT @numBankAccountTypeID = numAccountTypeID FROM dbo.AccountTypeDetail WHERE vcAccountCode = '01010101'  AND numDomainID = @numDomainID  
PRINT @numBankAccountTypeID

--select numAccountTypeID,'UnDepositedFunds','UnDepositedFunds','0101010101',numAccountTypeID,0,0,0,1,0,1,0,0  from AccountTypeDetail where numDomainID=1 and vcAccountCode='01010101'
EXEC usp_InsertNewChartAccountDetails @numAcntTypeId = @numAssetAccountTypeID,
    @numParntAcntTypeID = @numBankAccountTypeID , @vcAccountName = 'UnDeposited Funds',
    @vcAccountDescription = 'UnDepositedFunds',
    @monOriginalOpeningBal = $0, @monOpeningBal = $0,
    @dtOpeningDate = @dtTodayDate, @bitActive = 1, @numAccountId = 0,
    @bitFixed = 0, @numDomainID = @numDomainID   , @numListItemID = 0, @numUserCntId = 1,
    @bitProfitLoss = 0, @bitDepreciation = 0,
    @dtDepreciationCostDate = '1753-01-01', @monDepreciationCost = $0.0000,
    @vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, @bitIsSubAccount = 0, @numParentAccId = 0
    
-----------------
 
DECLARE @numPropAccountTypeID AS NUMERIC(9)
SELECT @numPropAccountTypeID = numAccountTypeID FROM dbo.AccountTypeDetail WHERE vcAccountCode = '01010201'  AND numDomainID = @numDomainID  
PRINT @numPropAccountTypeID   
    
PRINT 1
EXEC usp_InsertNewChartAccountDetails @numAcntTypeId = @numAssetAccountTypeID,
    @numParntAcntTypeID = @numPropAccountTypeID, @vcAccountName = 'Office Equipment',
    @vcAccountDescription = 'Office equipment that is owned and controlled by the business',
    @monOriginalOpeningBal = $0, @monOpeningBal = $0,
    @dtOpeningDate = @dtTodayDate, @bitActive = 1, @numAccountId = 0,
    @bitFixed = 0, @numDomainID = @numDomainID   , @numListItemID = 0, @numUserCntId = 1,
    @bitProfitLoss = 0, @bitDepreciation = 0,
    @dtDepreciationCostDate = '1753-01-01', @monDepreciationCost = $0.0000,
    @vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, @bitIsSubAccount = 0, @numParentAccId = 0

PRINT 2
EXEC usp_InsertNewChartAccountDetails @numAcntTypeId = @numAssetAccountTypeID,
    @numParntAcntTypeID = @numPropAccountTypeID,
    @vcAccountName = 'Less Accumulated Depreciation on Office Equipment',
    @vcAccountDescription = 'The total amount of office equipment cost that has been consumed by the entity (based on the useful life)',
    @monOriginalOpeningBal = $0, @monOpeningBal = $0,
    @dtOpeningDate = @dtTodayDate, @bitActive = 1, @numAccountId = 0,
    @bitFixed = 0, @numDomainID = @numDomainID   , @numListItemID = 0, @numUserCntId = 1,
    @bitProfitLoss = 0, @bitDepreciation = 0,
    @dtDepreciationCostDate = '1753-01-01', @monDepreciationCost = $0.0000,
    @vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, @bitIsSubAccount = 0, @numParentAccId = 0

PRINT 3
EXEC usp_InsertNewChartAccountDetails @numAcntTypeId = @numAssetAccountTypeID,
    @numParntAcntTypeID = @numPropAccountTypeID, @vcAccountName = 'Computer Equipment',
    @vcAccountDescription = 'Computer equipment that is owned and controlled by the business',
    @monOriginalOpeningBal = $0, @monOpeningBal = $0,
    @dtOpeningDate = @dtTodayDate, @bitActive = 1, @numAccountId = 0,
    @bitFixed = 0, @numDomainID = @numDomainID   , @numListItemID = 0, @numUserCntId = 1,
    @bitProfitLoss = 0, @bitDepreciation = 0,
    @dtDepreciationCostDate = '1753-01-01', @monDepreciationCost = $0.0000,
    @vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, @bitIsSubAccount = 0, @numParentAccId = 0

--PRINT 4
--EXEC usp_InsertNewChartAccountDetails @numAcntTypeId = @numAssetAccountTypeID,
--    @numParntAcntTypeID = @numPropAccountTypeID,
--    @vcAccountName = 'Less Accumulated Depreciation on Computer Equipment',
--    @vcAccountDescription = 'The total amount of computer equipment cost that has been consumed by the business (based on the useful life)',
--    @monOriginalOpeningBal = $0, @monOpeningBal = $0,
--    @dtOpeningDate = @dtTodayDate, @bitActive = 1, @numAccountId = 0,
--    @bitFixed = 0, @numDomainID = @numDomainID   , @numListItemID = 0, @numUserCntId = 1,
--    @bitProfitLoss = 0, @bitDepreciation = 0,
--    @dtDepreciationCostDate = '1753-01-01', @monDepreciationCost = $0.0000,
--    @vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, @bitIsSubAccount = 0, @numParentAccId = 0
-------------------------

DECLARE @numARAccountTypeID AS NUMERIC(9)
SELECT @numARAccountTypeID = numAccountTypeID FROM dbo.AccountTypeDetail WHERE vcAccountCode = '01010105'  AND numDomainID = @numDomainID  
PRINT @numARAccountTypeID  

EXEC usp_InsertNewChartAccountDetails @numAcntTypeId = @numAssetAccountTypeID,
    @numParntAcntTypeID = @numARAccountTypeID, @vcAccountName = 'Account Receivable',
    @vcAccountDescription = 'Account Receivable.',
    @monOriginalOpeningBal = $0, @monOpeningBal = $0,
    @dtOpeningDate = @dtTodayDate, @bitActive = 1, @numAccountId = 0,
    @bitFixed = 0, @numDomainID = @numDomainID   , @numListItemID = 0, @numUserCntId = 1,
    @bitProfitLoss = 0, @bitDepreciation = 0,
    @dtDepreciationCostDate = '1753-01-01', @monDepreciationCost = $0.0000,
    @vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, @bitIsSubAccount = 0, @numParentAccId = 0
    
PRINT 5
EXEC usp_InsertNewChartAccountDetails @numAcntTypeId = @numAssetAccountTypeID,
    @numParntAcntTypeID = @numARAccountTypeID, @vcAccountName = 'Prepayments',
    @vcAccountDescription = 'An expenditure that has been paid for in advance.',
    @monOriginalOpeningBal = $0, @monOpeningBal = $0,
    @dtOpeningDate = @dtTodayDate, @bitActive = 1, @numAccountId = 0,
    @bitFixed = 0, @numDomainID = @numDomainID   , @numListItemID = 0, @numUserCntId = 1,
    @bitProfitLoss = 0, @bitDepreciation = 0,
    @dtDepreciationCostDate = '1753-01-01', @monDepreciationCost = $0.0000,
    @vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, @bitIsSubAccount = 0, @numParentAccId = 0

EXEC usp_InsertNewChartAccountDetails @numAcntTypeId = @numAssetAccountTypeID,
    @numParntAcntTypeID = @numARAccountTypeID, @vcAccountName = 'Sales Clearing',
    @vcAccountDescription = 'Sales Clearing.',
    @monOriginalOpeningBal = $0, @monOpeningBal = $0,
    @dtOpeningDate = @dtTodayDate, @bitActive = 1, @numAccountId = 0,
    @bitFixed = 0, @numDomainID = @numDomainID   , @numListItemID = 0, @numUserCntId = 1,
    @bitProfitLoss = 0, @bitDepreciation = 0,
    @dtDepreciationCostDate = '1753-01-01', @monDepreciationCost = $0.0000,
    @vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, @bitIsSubAccount = 0, @numParentAccId = 0


DECLARE @numStockAccountTypeID AS NUMERIC(9)
SELECT @numStockAccountTypeID = numAccountTypeID FROM dbo.AccountTypeDetail WHERE vcAccountCode = '01010104'  AND numDomainID = @numDomainID  
PRINT @numStockAccountTypeID  

PRINT 6
EXEC usp_InsertNewChartAccountDetails @numAcntTypeId = @numAssetAccountTypeID,
    @numParntAcntTypeID = @numStockAccountTypeID, @vcAccountName = 'Inventory',
    @vcAccountDescription = 'Items available for sale including all costs of production.',
    @monOriginalOpeningBal = $0, @monOpeningBal = $0,
    @dtOpeningDate = @dtTodayDate, @bitActive = 1, @numAccountId = 0,
    @bitFixed = 0, @numDomainID = @numDomainID   , @numListItemID = 0, @numUserCntId = 1,
    @bitProfitLoss = 0, @bitDepreciation = 0,
    @dtDepreciationCostDate = '1753-01-01', @monDepreciationCost = $0.0000,
    @vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, @bitIsSubAccount = 0, @numParentAccId = 0

EXEC usp_InsertNewChartAccountDetails @numAcntTypeId = @numAssetAccountTypeID,
    @numParntAcntTypeID = @numStockAccountTypeID, @vcAccountName = 'Work In Progress',
    @vcAccountDescription = 'Work In Progress',
    @monOriginalOpeningBal = $0, @monOpeningBal = $0,
    @dtOpeningDate = @dtTodayDate, @bitActive = 1, @numAccountId = 0,
    @bitFixed = 0, @numDomainID = @numDomainID   , @numListItemID = 0, @numUserCntId = 1,
    @bitProfitLoss = 0, @bitDepreciation = 0,
    @dtDepreciationCostDate = '1753-01-01', @monDepreciationCost = $0.0000,
    @vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, @bitIsSubAccount = 0, @numParentAccId = 0    

EXEC usp_InsertNewChartAccountDetails @numAcntTypeId = @numAssetAccountTypeID,
    @numParntAcntTypeID = @numStockAccountTypeID, @vcAccountName = 'Finished Goods',
    @vcAccountDescription = 'Finished Goods',
    @monOriginalOpeningBal = $0, @monOpeningBal = $0,
    @dtOpeningDate = @dtTodayDate, @bitActive = 1, @numAccountId = 0,
    @bitFixed = 0, @numDomainID = @numDomainID   , @numListItemID = 0, @numUserCntId = 1,
    @bitProfitLoss = 0, @bitDepreciation = 0,
    @dtDepreciationCostDate = '1753-01-01', @monDepreciationCost = $0.0000,
    @vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, @bitIsSubAccount = 0, @numParentAccId = 0     
---------------------------------------------------------------------------------------------------------

------------------------------------ LIABILITIES ENTRY --------------------------------------------
DECLARE @numDutyTaxAccountTypeID AS NUMERIC(9)
SELECT @numDutyTaxAccountTypeID = numAccountTypeID FROM dbo.AccountTypeDetail WHERE vcAccountCode = '01020101'  AND numDomainID = @numDomainID  
PRINT @numDutyTaxAccountTypeID

--select numAccountTypeID,'Sales Tax Payable','Sales Tax Payable','0102010101',numAccountTypeID,0,0,0,1,0,1,0,0  from AccountTypeDetail where numDomainID=1 and vcAccountCode='01020101'   
EXEC usp_InsertNewChartAccountDetails @numAcntTypeId = @numLiabilityAccountTypeID,
    @numParntAcntTypeID =  @numDutyTaxAccountTypeID , @vcAccountName = 'Sales Tax Payable',
    @vcAccountDescription = 'Sales Tax Payable',
    @monOriginalOpeningBal = $0, @monOpeningBal = $0,
    @dtOpeningDate = @dtTodayDate, @bitActive = 1, @numAccountId = 0,
    @bitFixed = 0, @numDomainID = @numDomainID   , @numListItemID = 0, @numUserCntId = 1,
    @bitProfitLoss = 0, @bitDepreciation = 0,
    @dtDepreciationCostDate = '1753-01-01', @monDepreciationCost = $0.0000,
    @vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, @bitIsSubAccount = 0, @numParentAccId = 0

DECLARE @numAPAccountTypeID AS NUMERIC(9)
SELECT @numAPAccountTypeID = numAccountTypeID FROM dbo.AccountTypeDetail WHERE vcAccountCode = '01020102'  AND numDomainID = @numDomainID  
PRINT @numAPAccountTypeID


EXEC usp_InsertNewChartAccountDetails @numAcntTypeId = @numLiabilityAccountTypeID,
    @numParntAcntTypeID =  @numAPAccountTypeID , @vcAccountName = 'Deduction from Employee Payroll',
    @vcAccountDescription = 'Deduction from Employee Payroll',
    @monOriginalOpeningBal = $0, @monOpeningBal = $0,
    @dtOpeningDate = @dtTodayDate, @bitActive = 1, @numAccountId = 0,
    @bitFixed = 0, @numDomainID = @numDomainID   , @numListItemID = 0, @numUserCntId = 1,
    @bitProfitLoss = 0, @bitDepreciation = 0,
    @dtDepreciationCostDate = '1753-01-01', @monDepreciationCost = $0.0000,
    @vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, @bitIsSubAccount = 0, @numParentAccId = 0

EXEC usp_InsertNewChartAccountDetails @numAcntTypeId = @numLiabilityAccountTypeID,
    @numParntAcntTypeID =  @numAPAccountTypeID , @vcAccountName = 'Accounts Payable',
    @vcAccountDescription = 'Accounts Payable',
    @monOriginalOpeningBal = $0, @monOpeningBal = $0,
    @dtOpeningDate = @dtTodayDate, @bitActive = 1, @numAccountId = 0,
    @bitFixed = 0, @numDomainID = @numDomainID   , @numListItemID = 0, @numUserCntId = 1,
    @bitProfitLoss = 0, @bitDepreciation = 0,
    @dtDepreciationCostDate = '1753-01-01', @monDepreciationCost = $0.0000,
    @vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, @bitIsSubAccount = 0, @numParentAccId = 0
        
PRINT 7
EXEC usp_InsertNewChartAccountDetails @numAcntTypeId = @numLiabilityAccountTypeID,
    @numParntAcntTypeID = @numAPAccountTypeID, @vcAccountName = 'Employee Tax Payable',
    @vcAccountDescription = 'The amount of tax that has been deducted from wages or salaries paid to employes and is due to be paid',
    @monOriginalOpeningBal = $0, @monOpeningBal = $0,
    @dtOpeningDate = @dtTodayDate, @bitActive = 1, @numAccountId = 0,
    @bitFixed = 0, @numDomainID = @numDomainID   , @numListItemID = 0, @numUserCntId = 1,
    @bitProfitLoss = 0, @bitDepreciation = 0,
    @dtDepreciationCostDate = '1753-01-01', @monDepreciationCost = $0.0000,
    @vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, @bitIsSubAccount = 0, @numParentAccId = 0

PRINT 8
EXEC usp_InsertNewChartAccountDetails @numAcntTypeId = @numLiabilityAccountTypeID,
    @numParntAcntTypeID = @numAPAccountTypeID, @vcAccountName = 'Income Tax Payable',
    @vcAccountDescription = 'The amount of income tax that is due to be paid, also resident withholding tax paid on interest received.',
    @monOriginalOpeningBal = $0, @monOpeningBal = $0,
    @dtOpeningDate = @dtTodayDate, @bitActive = 1, @numAccountId = 0,
    @bitFixed = 0, @numDomainID = @numDomainID   , @numListItemID = 0, @numUserCntId = 1,
    @bitProfitLoss = 0, @bitDepreciation = 0,
    @dtDepreciationCostDate = '1753-01-01', @monDepreciationCost = $0.0000,
    @vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, @bitIsSubAccount = 0, @numParentAccId = 0

--PRINT 9
--EXEC usp_InsertNewChartAccountDetails @numAcntTypeId = 1490,
--    @numParntAcntTypeID = 1491, @vcAccountName = 'Suspense',
--    @vcAccountDescription = 'An entry that allows an unknown transaction to be entered, so the accounts can still be worked on in balance and the entry can be dealt with later.',
--    @monOriginalOpeningBal = $0, @monOpeningBal = $0,
--    @dtOpeningDate = @dtTodayDate, @bitActive = 1, @numAccountId = 0,
--    @bitFixed = 0, @numDomainID = @numDomainID   , @numListItemID = 0, @numUserCntId = 1,
--    @bitProfitLoss = 0, @bitDepreciation = 0,
--    @dtDepreciationCostDate = '1753-01-01', @monDepreciationCost = $0.0000,
--    @vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, @bitIsSubAccount = 0, @numParentAccId = 0

--SELECT * FROM dbo.AccountTypeDetail WHERE vcAccountCode = '010201' AND numDomainID = @numDomainID  
--SELECT * FROM dbo.AccountTypeDetail WHERE vcAccountCode = '01020101' AND numDomainID = @numDomainID  
--SELECT * FROM dbo.AccountTypeDetail WHERE vcAccountCode = '01020102' AND numDomainID = @numDomainID  

DECLARE @numCLAccountTypeID AS NUMERIC(9)
SELECT @numCLAccountTypeID = numAccountTypeID FROM dbo.AccountTypeDetail WHERE vcAccountCode = '01020102'  AND numDomainID = @numDomainID  
PRINT @numCLAccountTypeID

PRINT 10
EXEC usp_InsertNewChartAccountDetails @numAcntTypeId = @numLiabilityAccountTypeID,
    @numParntAcntTypeID = @numCLAccountTypeID, @vcAccountName = 'Unpaid Expense Claims',
    @vcAccountDescription = 'Automatically updates this account for payroll entries created using Payroll and will store the payroll amount to be paid to the employee for the pay run. This account enables you to maintain separate accounts for employee Wages Payable amounts and Accounts Payable amounts',
    @monOriginalOpeningBal = $0, @monOpeningBal = $0,
    @dtOpeningDate = @dtTodayDate, @bitActive = 1, @numAccountId = 0,
    @bitFixed = 0, @numDomainID = @numDomainID   , @numListItemID = 0, @numUserCntId = 1,
    @bitProfitLoss = 0, @bitDepreciation = 0,
    @dtDepreciationCostDate = '1753-01-01', @monDepreciationCost = $0.0000,
    @vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, @bitIsSubAccount = 0, @numParentAccId = 0

PRINT 11
EXEC usp_InsertNewChartAccountDetails @numAcntTypeId = @numLiabilityAccountTypeID,
    @numParntAcntTypeID = @numAPAccountTypeID, @vcAccountName = 'Wages Payable',
    @vcAccountDescription = 'Office equipment that is owned and controlled by the business',
    @monOriginalOpeningBal = $0, @monOpeningBal = $0,
    @dtOpeningDate = @dtTodayDate, @bitActive = 1, @numAccountId = 0,
    @bitFixed = 0, @numDomainID = @numDomainID   , @numListItemID = 0, @numUserCntId = 1,
    @bitProfitLoss = 0, @bitDepreciation = 0,
    @dtDepreciationCostDate = '1753-01-01', @monDepreciationCost = $0.0000,
    @vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, @bitIsSubAccount = 0, @numParentAccId = 0

PRINT 14
EXEC usp_InsertNewChartAccountDetails @numAcntTypeId = @numLiabilityAccountTypeID,
    @numParntAcntTypeID = @numCLAccountTypeID, @vcAccountName = 'Tracking Transfers',
    @vcAccountDescription = 'Transfers between tracking categories',
    @monOriginalOpeningBal = $0, @monOpeningBal = $0,
    @dtOpeningDate = @dtTodayDate, @bitActive = 1, @numAccountId = 0,
    @bitFixed = 0, @numDomainID = @numDomainID   , @numListItemID = 0, @numUserCntId = 1,
    @bitProfitLoss = 0, @bitDepreciation = 0,
    @dtDepreciationCostDate = '1753-01-01', @monDepreciationCost = $0.0000,
    @vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, @bitIsSubAccount = 0, @numParentAccId = 0

DECLARE @numWPAccountTypeID AS NUMERIC(9)
SELECT @numWPAccountTypeID = numAccountId FROM dbo.Chart_Of_Accounts WHERE numDomainID = @numDomainID  AND vcAccountName = 'Wages Payable'
PRINT @numWPAccountTypeID
EXEC usp_InsertNewChartAccountDetails @numAcntTypeId = @numLiabilityAccountTypeID,
    @numParntAcntTypeID = @numCLAccountTypeID, @vcAccountName = 'Wages & Salaries Payable',
    @vcAccountDescription = 'Wages & Salaries Payable',
    @monOriginalOpeningBal = $0, @monOpeningBal = $0,
    @dtOpeningDate = @dtTodayDate, @bitActive = 1, @numAccountId = 0,
    @bitFixed = 0, @numDomainID = @numDomainID   , @numListItemID = 0, @numUserCntId = 1,
    @bitProfitLoss = 0, @bitDepreciation = 0,
    @dtDepreciationCostDate = '1753-01-01', @monDepreciationCost = $0.0000,
    @vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, @bitIsSubAccount = 1, @numParentAccId = @numWPAccountTypeID
    
--SELECT * FROM dbo.AccountTypeDetail WHERE numDomainID = @numDomainID  
--SELECT * FROM dbo.AccountTypeDetail WHERE numAccountTypeID = 1490 AND numDomainID = @numDomainID  
--SELECT * FROM dbo.AccountTypeDetail WHERE vcAccountCode = '010202' AND numDomainID = @numDomainID  
--SELECT * FROM dbo.AccountTypeDetail WHERE vcAccountCode = '010202' AND numDomainID = @numDomainID  
--SELECT * FROM dbo.Chart_Of_Accounts WHERE  numDomainID = @numDomainID   AND bitActive = 0 AND vcAccountName IN ('Sales Tax Payable', 'Loan','Tracking Transfers')


PRINT 15
EXEC usp_InsertNewChartAccountDetails @numAcntTypeId = @numLiabilityAccountTypeID,
    @numParntAcntTypeID = @numLoanAccountTypeID , @vcAccountName = 'Loan',
    @vcAccountDescription = 'Money that has been borrowed from a creditor',
    @monOriginalOpeningBal = $0, @monOpeningBal = $0,
    @dtOpeningDate = @dtTodayDate, @bitActive = 1, @numAccountId = 0,
    @bitFixed = 0, @numDomainID = @numDomainID   , @numListItemID = 0, @numUserCntId = 1,
    @bitProfitLoss = 0, @bitDepreciation = 0,
    @dtDepreciationCostDate = '1753-01-01', @monDepreciationCost = $0.0000,
    @vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, @bitIsSubAccount =0, @numParentAccId = 0

DECLARE @numCurrentLiabilitiesAccountTypeID AS NUMERIC(9)
SELECT @numCurrentLiabilitiesAccountTypeID = numAccountTypeID FROM dbo.AccountTypeDetail WHERE vcAccountCode = '010201'  AND numDomainID = @numDomainID  
PRINT @numCurrentLiabilitiesAccountTypeID

EXEC usp_InsertNewChartAccountDetails @numAcntTypeId = @numLiabilityAccountTypeID,
    @numParntAcntTypeID = @numCurrentLiabilitiesAccountTypeID, @vcAccountName = 'Deferred Income',
    @vcAccountDescription = 'Deferred Income',
    @monOriginalOpeningBal = $0, @monOpeningBal = $0,
    @dtOpeningDate = @dtTodayDate, @bitActive = 1, @numAccountId = 0,
    @bitFixed = 0, @numDomainID = @numDomainID   , @numListItemID = 0, @numUserCntId = 1,
    @bitProfitLoss = 0, @bitDepreciation = 0,
    @dtDepreciationCostDate = '1753-01-01', @monDepreciationCost = $0.0000,
    @vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, @bitIsSubAccount = 0, @numParentAccId = 0

---------------------------------------------------------------------------------------------------

------------------------------------ EQUITY ENTRY --------------------------------------------

--SELECT * FROM dbo.AccountTypeDetail WHERE vcAccountCode = '0105' AND numDomainID = @numDomainID  
DECLARE @numEquityID AS NUMERIC(9)
SELECT @numEquityID = numAccountTypeID FROM dbo.AccountTypeDetail WHERE vcAccountCode = '0105'  AND numDomainID = @numDomainID  
PRINT @numEquityID

---------------------------------------------------------------------------------------------------

------------------------------------ EXPENSE ENTRY --------------------------------------------

----SELECT * FROM dbo.AccountTypeDetail WHERE vcAccountCode = '010601' AND numDomainID = @numDomainID  
--DECLARE @numCOGSID AS NUMERIC(9)
--SELECT @numCOGSID = numAccountTypeID FROM dbo.AccountTypeDetail WHERE numDomainID = @numDomainID   AND vcAccountCode = '01040101' 
--PRINT @numCOGSID
----select numAccountTypeID,'COGS Control Account','COGS','0104010401',numAccountTypeID,0,0,0,1,0,1,0,0  from AccountTypeDetail where numDomainID=1 and vcAccountCode='01040104'
--EXEC usp_InsertNewChartAccountDetails @numAcntTypeId = @numExpenseAccountTypeID,
--    @numParntAcntTypeID = @numCOGSID, @vcAccountName = 'COGS',
--    @vcAccountDescription = 'COGS Control Account',
--    @monOriginalOpeningBal = $0, @monOpeningBal = $0,
--    @dtOpeningDate = @dtTodayDate, @bitActive = 1, @numAccountId = 0,
--    @bitFixed = 0, @numDomainID = @numDomainID   , @numListItemID = 0, @numUserCntId = 1,
--    @bitProfitLoss = 0, @bitDepreciation = 0,
--    @dtDepreciationCostDate = '1753-01-01', @monDepreciationCost = $0.0000,
--    @vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, @bitIsSubAccount = 0, @numParentAccId = 0

--SELECT * FROM dbo.AccountTypeDetail WHERE vcAccountCode = '01040105' AND numDomainID = @numDomainID  
DECLARE @numOtherDirectExpenseID AS NUMERIC(9)
SELECT @numOtherDirectExpenseID = numAccountTypeID FROM dbo.AccountTypeDetail WHERE numDomainID = @numDomainID   AND vcAccountCode = '01040101' 
PRINT @numOtherDirectExpenseID
--select numAccountTypeID,'Billable Time & Expenses','Billable Time & Expenses','0104010101',numAccountTypeID,0,0,0,1,0,1,0,0  from AccountTypeDetail where numDomainID=1 and vcAccountCode='01040101'
EXEC usp_InsertNewChartAccountDetails @numAcntTypeId = @numExpenseAccountTypeID,
    @numParntAcntTypeID = @numOtherDirectExpenseID, @vcAccountName = 'Billable Time & Expenses',
    @vcAccountDescription = 'Office equipment that is owned and controlled by the business',
    @monOriginalOpeningBal = $0, @monOpeningBal = $0,
    @dtOpeningDate = @dtTodayDate, @bitActive = 1, @numAccountId = 0,
    @bitFixed = 0, @numDomainID = @numDomainID   , @numListItemID = 0, @numUserCntId = 1,
    @bitProfitLoss = 0, @bitDepreciation = 0,
    @dtDepreciationCostDate = '1753-01-01', @monDepreciationCost = $0.0000,
    @vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, @bitIsSubAccount = 0, @numParentAccId = 0

--select numAccountTypeID,'Purchase Account','Purchase','0104010402',numAccountTypeID,0,0,0,1,0,1,0,0  from AccountTypeDetail where numDomainID=1 and vcAccountCode='01040104'
EXEC usp_InsertNewChartAccountDetails @numAcntTypeId = @numExpenseAccountTypeID,
    @numParntAcntTypeID = @numOtherDirectExpenseID, @vcAccountName = 'Purchase',
    @vcAccountDescription = 'Purchase Account',
    @monOriginalOpeningBal = $0, @monOpeningBal = $0,
    @dtOpeningDate = @dtTodayDate, @bitActive = 1, @numAccountId = 0,
    @bitFixed = 0, @numDomainID = @numDomainID   , @numListItemID = 0, @numUserCntId = 1,
    @bitProfitLoss = 0, @bitDepreciation = 0,
    @dtDepreciationCostDate = '1753-01-01', @monDepreciationCost = $0.0000,
    @vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, @bitIsSubAccount = 0, @numParentAccId = 0

EXEC usp_InsertNewChartAccountDetails @numAcntTypeId = @numExpenseAccountTypeID,
    @numParntAcntTypeID = @numOtherDirectExpenseID, @vcAccountName = 'Reconciliation Discrepancies',
    @vcAccountDescription = 'Reconciliation Discrepancies',
    @monOriginalOpeningBal = $0, @monOpeningBal = $0,
    @dtOpeningDate = @dtTodayDate, @bitActive = 1, @numAccountId = 0,
    @bitFixed = 0, @numDomainID = @numDomainID   , @numListItemID = 0, @numUserCntId = 1,
    @bitProfitLoss = 0, @bitDepreciation = 0,
    @dtDepreciationCostDate = '1753-01-01', @monDepreciationCost = $0.0000,
    @vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, @bitIsSubAccount = 0, @numParentAccId = 0

EXEC usp_InsertNewChartAccountDetails @numAcntTypeId = @numExpenseAccountTypeID,
    @numParntAcntTypeID = @numOtherDirectExpenseID, @vcAccountName = 'Foreign Exchange Gain/Loss',
    @vcAccountDescription = 'Foreign Exchange Gain/Loss',
    @monOriginalOpeningBal = $0, @monOpeningBal = $0,
    @dtOpeningDate = @dtTodayDate, @bitActive = 1, @numAccountId = 0,
    @bitFixed = 0, @numDomainID = @numDomainID   , @numListItemID = 0, @numUserCntId = 1,
    @bitProfitLoss = 0, @bitDepreciation = 0,
    @dtDepreciationCostDate = '1753-01-01', @monDepreciationCost = $0.0000,
    @vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, @bitIsSubAccount = 0, @numParentAccId = 0
    
--01040104
--DECLARE @numPriceExpenseID AS NUMERIC(9)
--SELECT @numPriceExpenseID = numAccountTypeID FROM dbo.AccountTypeDetail WHERE numDomainID = @numDomainID   AND vcAccountCode = '01040104' 
--PRINT @numPriceExpenseID 

--PRINT 20
--EXEC usp_InsertNewChartAccountDetails @numAcntTypeId = @numExpenseAccountTypeID,
--    @numParntAcntTypeID = @numOtherDirectExpenseID, @vcAccountName = 'Price Variance',
--    @vcAccountDescription = 'Price Variance',
--    @monOriginalOpeningBal = $0, @monOpeningBal = $0,
--    @dtOpeningDate = @dtTodayDate, @bitActive = 1, @numAccountId = 0,
--    @bitFixed = 0, @numDomainID = @numDomainID   , @numListItemID = 0, @numUserCntId = 1,
--    @bitProfitLoss = 0, @bitDepreciation = 0,
--    @dtDepreciationCostDate = '1753-01-01', @monDepreciationCost = $0.0000,
--    @vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, @bitIsSubAccount = 0, @numParentAccId = 0
    
EXEC usp_InsertNewChartAccountDetails @numAcntTypeId = @numExpenseAccountTypeID,
    @numParntAcntTypeID = @numOtherDirectExpenseID, @vcAccountName = 'Inventory Adjustment',
    @vcAccountDescription = 'Inventory Adjustment',
    @monOriginalOpeningBal = $0, @monOpeningBal = $0,
    @dtOpeningDate = @dtTodayDate, @bitActive = 1, @numAccountId = 0,
    @bitFixed = 0, @numDomainID = @numDomainID   , @numListItemID = 0, @numUserCntId = 1,
    @bitProfitLoss = 0, @bitDepreciation = 0,
    @dtDepreciationCostDate = '1753-01-01', @monDepreciationCost = $0.0000,
    @vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, @bitIsSubAccount = 0, @numParentAccId = 0


--SELECT * FROM dbo.AccountTypeDetail WHERE numDomainID = @numDomainID   AND vcAccountCode = '010601' 
DECLARE @numCOGSExpenseID AS NUMERIC(9)
SELECT @numCOGSExpenseID = numAccountTypeID FROM dbo.AccountTypeDetail WHERE numDomainID = @numDomainID   AND vcAccountCode = '010601' 
PRINT @numCOGSExpenseID 
       
PRINT 20
EXEC usp_InsertNewChartAccountDetails @numAcntTypeId = @numCOGSAccountTypeID,
    @numParntAcntTypeID = @numCOGSExpenseID, @vcAccountName = 'Cost of Goods Sold',
    @vcAccountDescription = 'Costs of goods made by the business include material, labor, and other modification costs.',
    @monOriginalOpeningBal = $0, @monOpeningBal = $0,
    @dtOpeningDate = @dtTodayDate, @bitActive = 1, @numAccountId = 0,
    @bitFixed = 0, @numDomainID = @numDomainID   , @numListItemID = 0, @numUserCntId = 1,
    @bitProfitLoss = 0, @bitDepreciation = 0,
    @dtDepreciationCostDate = '1753-01-01', @monDepreciationCost = $0.0000,
    @vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, @bitIsSubAccount = 0, @numParentAccId = 0


EXEC usp_InsertNewChartAccountDetails @numAcntTypeId = @numCOGSAccountTypeID,
    @numParntAcntTypeID = @numCOGSExpenseID, @vcAccountName = 'Purchase Price Variance',
    @vcAccountDescription = 'Stores difference of purchase price when Cost of Product is different then what is entered in Purchase Order.',
    @monOriginalOpeningBal = $0, @monOpeningBal = $0,
    @dtOpeningDate = @dtTodayDate, @bitActive = 1, @numAccountId = 0,
    @bitFixed = 0, @numDomainID = @numDomainID   , @numListItemID = 0, @numUserCntId = 1,
    @bitProfitLoss = 0, @bitDepreciation = 0,
    @dtDepreciationCostDate = '1753-01-01', @monDepreciationCost = $0.0000,
    @vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, @bitIsSubAccount = 0, @numParentAccId = 0


    
--SELECT * FROM dbo.AccountTypeDetail WHERE vcAccountCode = '010401' AND numDomainID = @numDomainID  
DECLARE @numDirectExpenseID AS NUMERIC(9)
SELECT @numDirectExpenseID = numAccountTypeID FROM dbo.AccountTypeDetail WHERE numDomainID = @numDomainID   AND vcAccountCode = '010401' 
PRINT @numDirectExpenseID 
--       
----SELECT * FROM dbo.AccountTypeDetail WHERE vcAccountCode = '010401' AND numDomainID = @numDomainID  
--DECLARE @numDirectExpenseID AS NUMERIC(9)
--SELECT @numDirectExpenseID = numAccountTypeID FROM dbo.AccountTypeDetail WHERE numDomainID = @numDomainID   AND vcAccountCode = '010401' 
--PRINT @numDirectExpenseID 

--SELECT * FROM dbo.AccountTypeDetail WHERE vcAccountCode = '01040103' AND numDomainID = @numDomainID  
DECLARE @numOpeExpenseID AS NUMERIC(9)
SELECT @numOpeExpenseID = numAccountTypeID FROM dbo.AccountTypeDetail WHERE numDomainID = @numDomainID   AND vcAccountCode = '01040103' 
PRINT @numOpeExpenseID 

--PRINT 28
--EXEC usp_InsertNewChartAccountDetails @numAcntTypeId = @numExpenseAccountTypeID,
--    @numParntAcntTypeID = @numOpeExpenseID, @vcAccountName = 'Office Equipment',
--    @vcAccountDescription = 'Office equipment that is owned and controlled by the business',
--    @monOriginalOpeningBal = $0, @monOpeningBal = $0,
--    @dtOpeningDate = @dtTodayDate, @bitActive = 1, @numAccountId = 0,
--    @bitFixed = 0, @numDomainID = @numDomainID   , @numListItemID = 0, @numUserCntId = 1,
--    @bitProfitLoss = 0, @bitDepreciation = 0,
--    @dtDepreciationCostDate = '1753-01-01', @monDepreciationCost = $0.0000,
--    @vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, @bitIsSubAccount = 0, @numParentAccId = 0
  
PRINT 31
EXEC usp_InsertNewChartAccountDetails @numAcntTypeId = @numExpenseAccountTypeID,
    @numParntAcntTypeID = @numOpeExpenseID, @vcAccountName = 'Utilities',
    @vcAccountDescription = 'Expenses incurred for lighting, powering or heating the premises',
    @monOriginalOpeningBal = $0, @monOpeningBal = $0,
    @dtOpeningDate = @dtTodayDate, @bitActive = 1, @numAccountId = 0,
    @bitFixed = 0, @numDomainID = @numDomainID   , @numListItemID = 0, @numUserCntId = 1,
    @bitProfitLoss = 0, @bitDepreciation = 0,
    @dtDepreciationCostDate = '1753-01-01', @monDepreciationCost = $0.0000,
    @vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, @bitIsSubAccount = 0, @numParentAccId = 0
     
PRINT 32 
EXEC usp_InsertNewChartAccountDetails @numAcntTypeId = @numExpenseAccountTypeID,
    @numParntAcntTypeID = @numOpeExpenseID, @vcAccountName = 'Automobile Expenses',
    @vcAccountDescription = 'Expenses incurred on the running of company automobiles.',
    @monOriginalOpeningBal = $0, @monOpeningBal = $0,
    @dtOpeningDate = @dtTodayDate, @bitActive = 1, @numAccountId = 0,
    @bitFixed = 0, @numDomainID = @numDomainID   , @numListItemID = 0, @numUserCntId = 1,
    @bitProfitLoss = 0, @bitDepreciation = 0,
    @dtDepreciationCostDate = '1753-01-01', @monDepreciationCost = $0.0000,
    @vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, @bitIsSubAccount = 0, @numParentAccId = 0

PRINT 35 
EXEC usp_InsertNewChartAccountDetails @numAcntTypeId = @numExpenseAccountTypeID,
    @numParntAcntTypeID = @numOpeExpenseID, @vcAccountName = 'Rent',
    @vcAccountDescription = 'The payment to lease a building or area.',
    @monOriginalOpeningBal = $0, @monOpeningBal = $0,
    @dtOpeningDate = @dtTodayDate, @bitActive = 1, @numAccountId = 0,
    @bitFixed = 0, @numDomainID = @numDomainID   , @numListItemID = 0, @numUserCntId = 1,
    @bitProfitLoss = 0, @bitDepreciation = 0,
    @dtDepreciationCostDate = '1753-01-01', @monDepreciationCost = $0.0000,
    @vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, @bitIsSubAccount = 0, @numParentAccId = 0

PRINT 42
EXEC usp_InsertNewChartAccountDetails @numAcntTypeId = @numExpenseAccountTypeID,
    @numParntAcntTypeID = @numOpeExpenseID, @vcAccountName = 'Bad Debts',
    @vcAccountDescription = 'Noncollectable accounts receivable which have been written off.',
    @monOriginalOpeningBal = $0, @monOpeningBal = $0,
    @dtOpeningDate = @dtTodayDate, @bitActive = 1, @numAccountId = 0,
    @bitFixed = 0, @numDomainID = @numDomainID   , @numListItemID = 0, @numUserCntId = 1,
    @bitProfitLoss = 0, @bitDepreciation = 0,
    @dtDepreciationCostDate = '1753-01-01', @monDepreciationCost = $0.0000,
    @vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, @bitIsSubAccount = 0, @numParentAccId = 0

--SELECT * FROM dbo.AccountTypeDetail WHERE vcAccountCode = '010402' AND numDomainID = @numDomainID  
DECLARE @numIndirectExpenseID AS NUMERIC(9)
SELECT @numIndirectExpenseID = numAccountTypeID FROM dbo.AccountTypeDetail WHERE numDomainID = @numDomainID   AND vcAccountCode = '010402' 
PRINT @numIndirectExpenseID 
        
--SELECT * FROM dbo.AccountTypeDetail WHERE vcAccountCode = '01040201' AND numDomainID = @numDomainID  
DECLARE @numAdminExpenseID AS NUMERIC(9)
SELECT @numAdminExpenseID = numAccountTypeID FROM dbo.AccountTypeDetail WHERE numDomainID = @numDomainID   AND vcAccountCode = '01040201' 
PRINT @numAdminExpenseID 

       
PRINT 26
EXEC usp_InsertNewChartAccountDetails @numAcntTypeId = @numExpenseAccountTypeID,
    @numParntAcntTypeID = @numAdminExpenseID, @vcAccountName = 'Postage & Delivery',
    @vcAccountDescription = 'Expenses incurred on postage & delivery costs.',
    @monOriginalOpeningBal = $0, @monOpeningBal = $0,
    @dtOpeningDate = @dtTodayDate, @bitActive = 1, @numAccountId = 0,
    @bitFixed = 0, @numDomainID = @numDomainID   , @numListItemID = 0, @numUserCntId = 1,
    @bitProfitLoss = 0, @bitDepreciation = 0,
    @dtDepreciationCostDate = '1753-01-01', @monDepreciationCost = $0.0000,
    @vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, @bitIsSubAccount = 0, @numParentAccId = 0
        
PRINT 23
EXEC usp_InsertNewChartAccountDetails @numAcntTypeId = @numExpenseAccountTypeID,
    @numParntAcntTypeID = @numAdminExpenseID, @vcAccountName = 'Janitorial Expenses',
    @vcAccountDescription = 'Expenses incurred for cleaning business property.',
    @monOriginalOpeningBal = $0, @monOpeningBal = $0,
    @dtOpeningDate = @dtTodayDate, @bitActive = 1, @numAccountId = 0,
    @bitFixed = 0, @numDomainID = @numDomainID   , @numListItemID = 0, @numUserCntId = 1,
    @bitProfitLoss = 0, @bitDepreciation = 0,
    @dtDepreciationCostDate = '1753-01-01', @monDepreciationCost = $0.0000,
    @vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, @bitIsSubAccount = 0, @numParentAccId = 0
    
PRINT 24
EXEC usp_InsertNewChartAccountDetails @numAcntTypeId = @numExpenseAccountTypeID,
    @numParntAcntTypeID = @numAdminExpenseID, @vcAccountName = 'Consulting & Accounting',
    @vcAccountDescription = 'Expenses related to paying consultants',
    @monOriginalOpeningBal = $0, @monOpeningBal = $0,
    @dtOpeningDate = @dtTodayDate, @bitActive = 1, @numAccountId = 0,
    @bitFixed = 0, @numDomainID = @numDomainID   , @numListItemID = 0, @numUserCntId = 1,
    @bitProfitLoss = 0, @bitDepreciation = 0,
    @dtDepreciationCostDate = '1753-01-01', @monDepreciationCost = $0.0000,
    @vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, @bitIsSubAccount = 0, @numParentAccId = 0
        
PRINT 27
EXEC usp_InsertNewChartAccountDetails @numAcntTypeId = @numExpenseAccountTypeID,
    @numParntAcntTypeID = @numAdminExpenseID, @vcAccountName = 'General Expenses',
    @vcAccountDescription = 'General expenses related to the running of the business.',
    @monOriginalOpeningBal = $0, @monOpeningBal = $0,
    @dtOpeningDate = @dtTodayDate, @bitActive = 1, @numAccountId = 0,
    @bitFixed = 0, @numDomainID = @numDomainID   , @numListItemID = 0, @numUserCntId = 1,
    @bitProfitLoss = 0, @bitDepreciation = 0,
    @dtDepreciationCostDate = '1753-01-01', @monDepreciationCost = $0.0000,
    @vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, @bitIsSubAccount = 0, @numParentAccId = 0
    
PRINT 33
EXEC usp_InsertNewChartAccountDetails @numAcntTypeId = @numExpenseAccountTypeID,
    @numParntAcntTypeID = @numAdminExpenseID, @vcAccountName = 'Office Expenses',
    @vcAccountDescription = 'General expenses related to the running of the business office.',
    @monOriginalOpeningBal = $0, @monOpeningBal = $0,
    @dtOpeningDate = @dtTodayDate, @bitActive = 1, @numAccountId = 0,
    @bitFixed = 0, @numDomainID = @numDomainID   , @numListItemID = 0, @numUserCntId = 1,
    @bitProfitLoss = 0, @bitDepreciation = 0,
    @dtDepreciationCostDate = '1753-01-01', @monDepreciationCost = $0.0000,
    @vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, @bitIsSubAccount = 0, @numParentAccId = 0

PRINT 36
EXEC usp_InsertNewChartAccountDetails @numAcntTypeId = @numExpenseAccountTypeID,
    @numParntAcntTypeID = @numAdminExpenseID, @vcAccountName = 'Repairs and Maintenance',
    @vcAccountDescription = 'Expenses incurred on a damaged or run down asset that will bring the asset back to its original condition.',
    @monOriginalOpeningBal = $0, @monOpeningBal = $0,
    @dtOpeningDate = @dtTodayDate, @bitActive = 1, @numAccountId = 0,
    @bitFixed = 0, @numDomainID = @numDomainID   , @numListItemID = 0, @numUserCntId = 1,
    @bitProfitLoss = 0, @bitDepreciation = 0,
    @dtDepreciationCostDate = '1753-01-01', @monDepreciationCost = $0.0000,
    @vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, @bitIsSubAccount = 0, @numParentAccId = 0
 
PRINT 39 
EXEC usp_InsertNewChartAccountDetails @numAcntTypeId = @numExpenseAccountTypeID,
    @numParntAcntTypeID = @numAdminExpenseID, @vcAccountName = 'Dues & Subscriptions',
    @vcAccountDescription = 'E.g. Magazines, professional bodies',
    @monOriginalOpeningBal = $0, @monOpeningBal = $0,
    @dtOpeningDate = @dtTodayDate, @bitActive = 1, @numAccountId = 0,
    @bitFixed = 0, @numDomainID = @numDomainID   , @numListItemID = 0, @numUserCntId = 1,
    @bitProfitLoss = 0, @bitDepreciation = 0,
    @dtDepreciationCostDate = '1753-01-01', @monDepreciationCost = $0.0000,
    @vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, @bitIsSubAccount = 0, @numParentAccId = 0

--SELECT * FROM dbo.AccountTypeDetail WHERE vcAccountCode = '01040202' AND numDomainID = @numDomainID  
DECLARE @numStaffExpenseID AS NUMERIC(9)
SELECT @numStaffExpenseID = numAccountTypeID FROM dbo.AccountTypeDetail WHERE numDomainID = @numDomainID   AND vcAccountCode = '01040202' 
PRINT @numStaffExpenseID 

PRINT 37 
EXEC usp_InsertNewChartAccountDetails @numAcntTypeId = @numExpenseAccountTypeID,
    @numParntAcntTypeID = @numStaffExpenseID, @vcAccountName = 'Wages and Salaries',
    @vcAccountDescription = 'Payment to employees in exchange for their resources',
    @monOriginalOpeningBal = $0, @monOpeningBal = $0,
    @dtOpeningDate = @dtTodayDate, @bitActive = 1, @numAccountId = 0,
    @bitFixed = 0, @numDomainID = @numDomainID   , @numListItemID = 0, @numUserCntId = 1,
    @bitProfitLoss = 0, @bitDepreciation = 0,
    @dtDepreciationCostDate = '1753-01-01', @monDepreciationCost = $0.0000,
    @vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, @bitIsSubAccount = 0, @numParentAccId = 0

PRINT 41 
EXEC usp_InsertNewChartAccountDetails @numAcntTypeId = @numExpenseAccountTypeID,
    @numParntAcntTypeID = @numStaffExpenseID, @vcAccountName = 'Travel',
    @vcAccountDescription = 'Expenses incurred from travel which has a business purpose',
    @monOriginalOpeningBal = $0, @monOpeningBal = $0,
    @dtOpeningDate = @dtTodayDate, @bitActive = 1, @numAccountId = 0,
    @bitFixed = 0, @numDomainID = @numDomainID   , @numListItemID = 0, @numUserCntId = 1,
    @bitProfitLoss = 0, @bitDepreciation = 0,
    @dtDepreciationCostDate = '1753-01-01', @monDepreciationCost = $0.0000,
    @vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, @bitIsSubAccount = 0, @numParentAccId = 0
    

SELECT * FROM dbo.Chart_Of_Accounts WHERE numDomainID = 1 AND vcAccountName = 'Wages and Salaries'
DECLARE @numWSID AS NUMERIC(9)
SELECT @numWSID = numAccountID FROM dbo.Chart_Of_Accounts WHERE numDomainID = @numDomainID   AND vcAccountName = 'Wages and Salaries'
PRINT @numWSID 

EXEC usp_InsertNewChartAccountDetails @numAcntTypeId = @numExpenseAccountTypeID,
    @numParntAcntTypeID = @numStaffExpenseID , @vcAccountName = 'Employee payroll expense',
    @vcAccountDescription = 'Employee payroll expense',
    @monOriginalOpeningBal = $0, @monOpeningBal = $0,
    @dtOpeningDate = @dtTodayDate, @bitActive = 1, @numAccountId = 0,
    @bitFixed = 0, @numDomainID = @numDomainID   , @numListItemID = 0, @numUserCntId = 1,
    @bitProfitLoss = 0, @bitDepreciation = 0,
    @dtDepreciationCostDate = '1753-01-01', @monDepreciationCost = $0.0000,
    @vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, @bitIsSubAccount = 1, @numParentAccId = @numWSID

EXEC usp_InsertNewChartAccountDetails @numAcntTypeId = @numExpenseAccountTypeID,
    @numParntAcntTypeID = @numStaffExpenseID, @vcAccountName = 'Contract Employee Payroll Expense',
    @vcAccountDescription = 'Contract Employee Payroll Expense',
    @monOriginalOpeningBal = $0, @monOpeningBal = $0,
    @dtOpeningDate = @dtTodayDate, @bitActive = 1, @numAccountId = 0,
    @bitFixed = 0, @numDomainID = @numDomainID   , @numListItemID = 0, @numUserCntId = 1,
    @bitProfitLoss = 0, @bitDepreciation = 0,
    @dtDepreciationCostDate = '1753-01-01', @monDepreciationCost = $0.0000,
    @vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, @bitIsSubAccount = 1, @numParentAccId = @numWSID
  
---------------------------------------------------------------------------------------------------

------------------------------------ INCOME ENTRY --------------------------------------------
--SELECT * FROM dbo.AccountTypeDetail WHERE vcAccountCode = '010301' AND numDomainID = @numDomainID  
DECLARE @numDirectIncomeAccountTypeID AS NUMERIC(9)
SELECT @numDirectIncomeAccountTypeID = numAccountTypeID FROM dbo.AccountTypeDetail WHERE vcAccountCode = '010301'  AND numDomainID = @numDomainID  
PRINT @numDirectIncomeAccountTypeID

--select numAccountTypeID,'Sales Discounts','Sales Discounts','01030101',numAccountTypeID,0,0,0,1,0,1,0,0  from AccountTypeDetail where numDomainID=1 and vcAccountCode='010301'
EXEC usp_InsertNewChartAccountDetails @numAcntTypeId = @numIncomeAccountTypeID,
    @numParntAcntTypeID = @numDirectIncomeAccountTypeID , @vcAccountName = 'Sales Discounts',
    @vcAccountDescription = 'Sales Discounts',
    @monOriginalOpeningBal = $0, @monOpeningBal = $0,
    @dtOpeningDate = @dtTodayDate, @bitActive = 1, @numAccountId = 0,
    @bitFixed = 0, @numDomainID = @numDomainID   , @numListItemID = 0, @numUserCntId = 1,
    @bitProfitLoss = 0, @bitDepreciation = 0,
    @dtDepreciationCostDate = '1753-01-01', @monDepreciationCost = $0.0000,
    @vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, @bitIsSubAccount = 0, @numParentAccId = 0

--select numAccountTypeID,'Shipping Income','Shipping Income','01030201',numAccountTypeID,0,0,0,1,0,1,0,0  from AccountTypeDetail where numDomainID=1 and vcAccountCode='010302'
EXEC usp_InsertNewChartAccountDetails @numAcntTypeId = @numAssetAccountTypeID,
    @numParntAcntTypeID = @numDirectIncomeAccountTypeID, @vcAccountName = 'Shipping Income',
    @vcAccountDescription = 'Shipping Income',
    @monOriginalOpeningBal = $0, @monOpeningBal = $0,
    @dtOpeningDate = @dtTodayDate, @bitActive = 1, @numAccountId = 0,
    @bitFixed = 0, @numDomainID = @numDomainID   , @numListItemID = 0, @numUserCntId = 1,
    @bitProfitLoss = 0, @bitDepreciation = 0,
    @dtDepreciationCostDate = '1753-01-01', @monDepreciationCost = $0.0000,
    @vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, @bitIsSubAccount = 0, @numParentAccId = 0

--select numAccountTypeID,'Late Charges','Late Charges','01030202',numAccountTypeID,0,0,0,1,0,1,0,0  from AccountTypeDetail where numDomainID=1 and vcAccountCode='010302'
EXEC usp_InsertNewChartAccountDetails @numAcntTypeId = @numAssetAccountTypeID,
    @numParntAcntTypeID = @numDirectIncomeAccountTypeID, @vcAccountName = 'Late Charges',
    @vcAccountDescription = 'Late Charges',
    @monOriginalOpeningBal = $0, @monOpeningBal = $0,
    @dtOpeningDate = @dtTodayDate, @bitActive = 1, @numAccountId = 0,
    @bitFixed = 0, @numDomainID = @numDomainID   , @numListItemID = 0, @numUserCntId = 1,
    @bitProfitLoss = 0, @bitDepreciation = 0,
    @dtDepreciationCostDate = '1753-01-01', @monDepreciationCost = $0.0000,
    @vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, @bitIsSubAccount = 0, @numParentAccId = 0
        
PRINT 46 
EXEC usp_InsertNewChartAccountDetails @numAcntTypeId = @numIncomeAccountTypeID,
    @numParntAcntTypeID =  @numDirectIncomeAccountTypeID, @vcAccountName = 'Other Revenue',
    @vcAccountDescription = 'Any other income that does not relate to normal business activities and is not recurring',
    @monOriginalOpeningBal = $0, @monOpeningBal = $0,
    @dtOpeningDate = @dtTodayDate, @bitActive = 1, @numAccountId = 0,
    @bitFixed = 0, @numDomainID = @numDomainID   , @numListItemID = 0, @numUserCntId = 1,
    @bitProfitLoss = 0, @bitDepreciation = 0,
    @dtDepreciationCostDate = '1753-01-01', @monDepreciationCost = $0.0000,
    @vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, @bitIsSubAccount = 0, @numParentAccId = 0

EXEC usp_InsertNewChartAccountDetails @numAcntTypeId = @numIncomeAccountTypeID,
    @numParntAcntTypeID = @numDirectIncomeAccountTypeID , @vcAccountName = 'Sales',
    @vcAccountDescription = 'Sales',
    @monOriginalOpeningBal = $0, @monOpeningBal = $0,
    @dtOpeningDate = @dtTodayDate, @bitActive = 1, @numAccountId = 0,
    @bitFixed = 0, @numDomainID = @numDomainID   , @numListItemID = 0, @numUserCntId = 1,
    @bitProfitLoss = 0, @bitDepreciation = 0,
    @dtDepreciationCostDate = '1753-01-01', @monDepreciationCost = $0.0000,
    @vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, @bitIsSubAccount = 0, @numParentAccId = 0
    
---------------------------------------------------------------------------------------------------

--SELECT * FROM dbo.Chart_Of_Accounts WHERE  numDomainID = @numDomainID   AND vcAccountCode = '01020201'
--DELETE FROM dbo.ChartAccountOpening WHERE  numDomainID = @numDomainID   AND numAccountId = (SELECT numAccountId FROM dbo.Chart_Of_Accounts WHERE  numDomainID = @numDomainID   AND vcAccountCode = '01020201')
--DELETE FROM dbo.Chart_Of_Accounts WHERE  numDomainID = @numDomainID   AND vcAccountCode = '01020201'

----------------

-- Create Credit Card as a new account under Current liability.
EXEC usp_InsertNewChartAccountDetails @numAcntTypeId = @numLiabilityAccountTypeID,
    @numParntAcntTypeID =  @numCLAccountTypeID, @vcAccountName = 'Credit Card',
    @vcAccountDescription = 'Credit Card',
    @monOriginalOpeningBal = $0, @monOpeningBal = $0,
    @dtOpeningDate = @dtTodayDate, @bitActive = 1, @numAccountId = 0,
    @bitFixed = 0, @numDomainID = @numDomainID   , @numListItemID = 0, @numUserCntId = 1,
    @bitProfitLoss = 0, @bitDepreciation = 0,
    @dtDepreciationCostDate = '1753-01-01', @monDepreciationCost = $0.0000,
    @vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, @bitIsSubAccount = 0, @numParentAccId = 0

--- Adding new account "Purchase Clearing" under Current liability.
EXEC usp_InsertNewChartAccountDetails @numAcntTypeId = @numLiabilityAccountTypeID,
    @numParntAcntTypeID =  @numCLAccountTypeID, @vcAccountName = 'Purchase Clearing',
    @vcAccountDescription = 'Purchase Clearing',
    @monOriginalOpeningBal = $0, @monOpeningBal = $0,
    @dtOpeningDate = @dtTodayDate, @bitActive = 1, @numAccountId = 0,
    @bitFixed = 0, @numDomainID = @numDomainID   , @numListItemID = 0, @numUserCntId = 1,
    @bitProfitLoss = 0, @bitDepreciation = 0,
    @dtDepreciationCostDate = '1753-01-01', @monDepreciationCost = $0.0000,
    @vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, @bitIsSubAccount = 0, @numParentAccId = 0
-- From Accounts payable remove �Historical adjustments� and �rounding� unless it is required as default for some specific purpose. 
--SELECT * FROM dbo.Chart_Of_Accounts WHERE vcAccountCode = '0102010212' AND numDomainID = @numDomainID   
--DELETE FROM dbo.ChartAccountOpening WHERE  numDomainID = @numDomainID   AND numAccountId = (SELECT numAccountId FROM dbo.Chart_Of_Accounts WHERE  numDomainID = @numDomainID   AND vcAccountCode = '0102010212')
--DELETE FROM dbo.Chart_Of_Accounts WHERE vcAccountCode = '0102010212' AND numDomainID = @numDomainID  
--
----SELECT * FROM dbo.Chart_Of_Accounts WHERE vcAccountCode = '0102010211' AND numDomainID = @numDomainID  
--DELETE FROM dbo.ChartAccountOpening WHERE  numDomainID = @numDomainID   AND numAccountId = (SELECT numAccountId FROM dbo.Chart_Of_Accounts WHERE  numDomainID = @numDomainID   AND vcAccountCode = '0102010211')
--DELETE FROM dbo.Chart_Of_Accounts WHERE vcAccountCode = '0102010211' AND numDomainID = @numDomainID  

PRINT 12
--EXEC usp_InsertNewChartAccountDetails @numAcntTypeId = @numLiabilityAccountTypeID,
--    @numParntAcntTypeID = @numCLAccountTypeID, @vcAccountName = 'Historical Adjustment',
--    @vcAccountDescription = 'For accountant adjustments',
--    @monOriginalOpeningBal = $0, @monOpeningBal = $0,
--    @dtOpeningDate = @dtTodayDate, @bitActive = 1, @numAccountId = 0,
--    @bitFixed = 0, @numDomainID = @numDomainID   , @numListItemID = 0, @numUserCntId = 1,
--    @bitProfitLoss = 0, @bitDepreciation = 0,
--    @dtDepreciationCostDate = '1753-01-01', @monDepreciationCost = $0.0000,
--    @vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, @bitIsSubAccount = 0, @numParentAccId = 0

PRINT 13
--EXEC usp_InsertNewChartAccountDetails @numAcntTypeId = @numLiabilityAccountTypeID,
--    @numParntAcntTypeID = @numCLAccountTypeID, @vcAccountName = 'Rounding',
--    @vcAccountDescription = 'An adjustment entry to allow for rounding',
--    @monOriginalOpeningBal = $0, @monOpeningBal = $0,
--    @dtOpeningDate = @dtTodayDate, @bitActive = 1, @numAccountId = 0,
--    @bitFixed = 0, @numDomainID = @numDomainID   , @numListItemID = 0, @numUserCntId = 1,
--    @bitProfitLoss = 0, @bitDepreciation = 0,
--    @dtDepreciationCostDate = '1753-01-01', @monDepreciationCost = $0.0000,
--    @vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, @bitIsSubAccount = 0, @numParentAccId = 0
    
--Remove Suspense A/c from Current Liability
--SELECT * FROM dbo.AccountTypeDetail WHERE vcAccountCode = '01020104' AND numDomainID = @numDomainID  
--DELETE FROM dbo.ChartAccountOpening WHERE  numDomainID = @numDomainID   AND numAccountId = (SELECT numAccountId FROM dbo.Chart_Of_Accounts WHERE  numDomainID = @numDomainID   AND vcAccountCode = '0102010212')
--DELETE FROM dbo.AccountTypeDetail WHERE vcAccountCode = '0102010212' AND numDomainID = @numDomainID  

--From Current liability change the name of �Loans (Liability)� account type to Short Term borrowings.
--SELECT * FROM dbo.AccountTypeDetail WHERE numDomainID = @numDomainID   AND vcAccountCode = '01020103'
--UPDATE dbo.AccountTypeDetail SET vcAccountType = 'Short Term Borrowings' WHERE vcAccountCode = '01020103' AND numDomainID = @numDomainID  


--Interest income should be under indirect income instead of direct income.
--SELECT * FROM dbo.AccountTypeDetail WHERE numDomainID = @numDomainID   AND vcAccountCode = '010302' 
DECLARE @numIndirectIncomeAccountTypeID AS NUMERIC(9,0)
SELECT @numIndirectIncomeAccountTypeID = numAccountTypeID FROM dbo.AccountTypeDetail WHERE vcAccountCode = '010302'  AND numDomainID = @numDomainID  
PRINT @numIndirectIncomeAccountTypeID

EXEC usp_InsertNewChartAccountDetails @numAcntTypeId = @numIncomeAccountTypeID,
    @numParntAcntTypeID =  @numIndirectIncomeAccountTypeID, @vcAccountName = 'Interest Income',
    @vcAccountDescription = 'Interest income',
    @monOriginalOpeningBal = $0, @monOpeningBal = $0,
    @dtOpeningDate = @dtTodayDate, @bitActive = 1, @numAccountId = 0,
    @bitFixed = 0, @numDomainID = @numDomainID   , @numListItemID = 0, @numUserCntId = 1,
    @bitProfitLoss = 0, @bitDepreciation = 0,
    @dtDepreciationCostDate = '1753-01-01', @monDepreciationCost = $0.0000,
    @vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, @bitIsSubAccount = 0, @numParentAccId = 0
    
--Account with the name of �Dividend income� should be there under indirect income.
EXEC usp_InsertNewChartAccountDetails @numAcntTypeId = @numIncomeAccountTypeID,
    @numParntAcntTypeID =  @numIndirectIncomeAccountTypeID, @vcAccountName = 'Dividend Income',
    @vcAccountDescription = 'Dividend income',
    @monOriginalOpeningBal = $0, @monOpeningBal = $0,
    @dtOpeningDate = @dtTodayDate, @bitActive = 1, @numAccountId = 0,
    @bitFixed = 0, @numDomainID = @numDomainID   , @numListItemID = 0, @numUserCntId = 1,
    @bitProfitLoss = 0, @bitDepreciation = 0,
    @dtDepreciationCostDate = '1753-01-01', @monDepreciationCost = $0.0000,
    @vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, @bitIsSubAccount = 0, @numParentAccId = 0

--------------------------------------------------------------------------------------------------------------------------------------
-- DONE 																															--																																		    
--Under expense side COGS accounts should not be under any other category except COGS so remove the same from other direct expense. --
--Remove �office equipment� from operating expense type.																			--
--------------------------------------------------------------------------------------------------------------------------------------

--Transfer following accounts to Administrative Expense from Operating Expense.
--Printing & Stationary,   telephone & Internet, Income Tax expense, Entertainment, Insurance.
PRINT 34 
EXEC usp_InsertNewChartAccountDetails @numAcntTypeId = @numExpenseAccountTypeID,
    @numParntAcntTypeID = @numAdminExpenseID, @vcAccountName = 'Printing & Stationery',
    @vcAccountDescription = 'Expenses incurred by the entity as a result of printing and stationery',
    @monOriginalOpeningBal = $0, @monOpeningBal = $0,
    @dtOpeningDate = @dtTodayDate, @bitActive = 1, @numAccountId = 0,
    @bitFixed = 0, @numDomainID = @numDomainID   , @numListItemID = 0, @numUserCntId = 1,
    @bitProfitLoss = 0, @bitDepreciation = 0,
    @dtDepreciationCostDate = '1753-01-01', @monDepreciationCost = $0.0000,
    @vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, @bitIsSubAccount = 0, @numParentAccId = 0

PRINT 40
EXEC usp_InsertNewChartAccountDetails @numAcntTypeId = @numExpenseAccountTypeID,
    @numParntAcntTypeID = @numAdminExpenseID, @vcAccountName = 'Telephone & Internet',
    @vcAccountDescription = 'Expenditure incurred from any business-related phone calls, phone lines, or internet connections',
    @monOriginalOpeningBal = $0, @monOpeningBal = $0,
    @dtOpeningDate = @dtTodayDate, @bitActive = 1, @numAccountId = 0,
    @bitFixed = 0, @numDomainID = @numDomainID   , @numListItemID = 0, @numUserCntId = 1,
    @bitProfitLoss = 0, @bitDepreciation = 0,
    @dtDepreciationCostDate = '1753-01-01', @monDepreciationCost = $0.0000,
    @vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, @bitIsSubAccount = 0, @numParentAccId = 0
        
PRINT 44 
EXEC usp_InsertNewChartAccountDetails @numAcntTypeId = @numExpenseAccountTypeID,
    @numParntAcntTypeID = @numAdminExpenseID, @vcAccountName = 'Income Tax Expense',
    @vcAccountDescription = 'A percentage of total earnings paid to the government',
    @monOriginalOpeningBal = $0, @monOpeningBal = $0,
    @dtOpeningDate = @dtTodayDate, @bitActive = 1, @numAccountId = 0,
    @bitFixed = 0, @numDomainID = @numDomainID   , @numListItemID = 0, @numUserCntId = 1,
    @bitProfitLoss = 0, @bitDepreciation = 0,
    @dtDepreciationCostDate = '1753-01-01', @monDepreciationCost = $0.0000,
    @vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, @bitIsSubAccount = 0 , @numParentAccId = 0

PRINT 25
EXEC usp_InsertNewChartAccountDetails @numAcntTypeId = @numExpenseAccountTypeID,
    @numParntAcntTypeID = @numAdminExpenseID, @vcAccountName = 'Entertainment',
    @vcAccountDescription = 'Expenses paid by company for the business but are not deductable for income tax purposes.',
    @monOriginalOpeningBal = $0, @monOpeningBal = $0,
    @dtOpeningDate = @dtTodayDate, @bitActive = 1, @numAccountId = 0,
    @bitFixed = 0, @numDomainID = @numDomainID   , @numListItemID = 0, @numUserCntId = 1,
    @bitProfitLoss = 0, @bitDepreciation = 0,
    @dtDepreciationCostDate = '1753-01-01', @monDepreciationCost = $0.0000,
    @vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, @bitIsSubAccount = 0, @numParentAccId = 0

PRINT 29
EXEC usp_InsertNewChartAccountDetails @numAcntTypeId = @numExpenseAccountTypeID,
    @numParntAcntTypeID = @numAdminExpenseID, @vcAccountName = 'Insurance',
    @vcAccountDescription = 'Expenses incurred for insuring the business'' assets',
    @monOriginalOpeningBal = $0, @monOpeningBal = $0,
    @dtOpeningDate = @dtTodayDate, @bitActive = 1, @numAccountId = 0,
    @bitFixed = 0, @numDomainID = @numDomainID   , @numListItemID = 0, @numUserCntId = 1,
    @bitProfitLoss = 0, @bitDepreciation = 0,
    @dtDepreciationCostDate = '1753-01-01', @monDepreciationCost = $0.0000,
    @vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, @bitIsSubAccount = 0, @numParentAccId = 0
           
--Advertising expense should be moved to Selling & Distribution from operating expense.
--SELECT * FROM dbo.AccountTypeDetail WHERE numDomainID = @numDomainID   AND vcAccountCode = '01040102' 
DECLARE @numSDccountTypeID AS NUMERIC(9,0)
SELECT @numSDccountTypeID = numAccountTypeID FROM dbo.AccountTypeDetail WHERE vcAccountCode = '01040102'  AND numDomainID = @numDomainID  
PRINT @numSDccountTypeID
   
PRINT 21
EXEC usp_InsertNewChartAccountDetails @numAcntTypeId = @numExpenseAccountTypeID,
    @numParntAcntTypeID = @numSDccountTypeID, @vcAccountName = 'Advertising',
    @vcAccountDescription = 'Expenses incurred for advertising while trying to increase sales',
    @monOriginalOpeningBal = $0, @monOpeningBal = $0,
    @dtOpeningDate = @dtTodayDate, @bitActive = 1, @numAccountId = 0,
    @bitFixed = 0, @numDomainID = @numDomainID   , @numListItemID = 0, @numUserCntId = 1,
    @bitProfitLoss = 0, @bitDepreciation = 0,
    @dtDepreciationCostDate = '1753-01-01', @monDepreciationCost = $0.0000,
    @vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, @bitIsSubAccount = 0, @numParentAccId = 0
    
--�Interest Expense� should be under Finance Cost rather than Operating Expense.
--SELECT * FROM dbo.AccountTypeDetail WHERE numDomainID = @numDomainID   AND vcAccountCode = '01040203' 
DECLARE @numFCccountTypeID AS NUMERIC(9,0)
SELECT @numFCccountTypeID = numAccountTypeID FROM dbo.AccountTypeDetail WHERE vcAccountCode = '01040203'  AND numDomainID = @numDomainID  
PRINT @numFCccountTypeID

PRINT 45 
EXEC usp_InsertNewChartAccountDetails @numAcntTypeId = @numExpenseAccountTypeID,
    @numParntAcntTypeID = @numFCccountTypeID, @vcAccountName = 'Interest Expense',
    @vcAccountDescription = 'Any interest expenses paid to your tax authority, business bank accounts or credit card accounts.',
    @monOriginalOpeningBal = $0, @monOpeningBal = $0,
    @dtOpeningDate = @dtTodayDate, @bitActive = 1, @numAccountId = 0,
    @bitFixed = 0, @numDomainID = @numDomainID   , @numListItemID = 0, @numUserCntId = 1,
    @bitProfitLoss = 0, @bitDepreciation = 0,
    @dtDepreciationCostDate = '1753-01-01', @monDepreciationCost = $0.0000,
    @vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, @bitIsSubAccount = 0, @numParentAccId = 0
    
--Move bank Service charges under finance Costs.
PRINT 22
EXEC usp_InsertNewChartAccountDetails @numAcntTypeId = @numExpenseAccountTypeID,
    @numParntAcntTypeID = @numFCccountTypeID, @vcAccountName = 'Bank Service Charges',
    @vcAccountDescription = 'Fees charged by your bank for transactions regarding your bank account(s).',
    @monOriginalOpeningBal = $0, @monOpeningBal = $0,
    @dtOpeningDate = @dtTodayDate, @bitActive = 1, @numAccountId = 0,
    @bitFixed = 0, @numDomainID = @numDomainID   , @numListItemID = 0, @numUserCntId = 1,
    @bitProfitLoss = 0, @bitDepreciation = 0,
    @dtDepreciationCostDate = '1753-01-01', @monDepreciationCost = $0.0000,
    @vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, @bitIsSubAccount = 0, @numParentAccId = 0
    
--Move �Payroll tax expense� to Staff expense from operating expense.

--SELECT * FROM dbo.AccountTypeDetail WHERE numDomainID = @numDomainID   AND vcAccountCode = '01040202' 
DECLARE @numSEccountTypeID AS NUMERIC(9,0)
SELECT @numSEccountTypeID = numAccountTypeID FROM dbo.AccountTypeDetail WHERE vcAccountCode = '01040202'  AND numDomainID = @numDomainID  
PRINT @numSEccountTypeID
    
PRINT 38 
EXEC usp_InsertNewChartAccountDetails @numAcntTypeId = @numExpenseAccountTypeID,
    @numParntAcntTypeID = @numSEccountTypeID, @vcAccountName = 'Payroll Tax Expense',
    @vcAccountDescription = '', @monOriginalOpeningBal = $0,
    @monOpeningBal = $0, @dtOpeningDate = @dtTodayDate, @bitActive = 1,
    @numAccountId = 0, @bitFixed = 0, @numDomainID = @numDomainID   , @numListItemID = 0,
    @numUserCntId = 1, @bitProfitLoss = 0, @bitDepreciation = 0,
    @dtDepreciationCostDate = '1753-01-01', @monDepreciationCost = $0.0000,
    @vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, @bitIsSubAccount = 0, @numParentAccId = 0
    
--Move �Depreciation� from administrative expense to Non-cash Expense.
--SELECT * FROM dbo.AccountTypeDetail WHERE numDomainID = @numDomainID   AND vcAccountCode = '010404' 
DECLARE @numNCEccountTypeID AS NUMERIC(9,0)
SELECT @numNCEccountTypeID = numAccountTypeID FROM dbo.AccountTypeDetail WHERE vcAccountCode = '01040205'  AND numDomainID = @numDomainID  
PRINT @numNCEccountTypeID

PRINT 43 
EXEC usp_InsertNewChartAccountDetails @numAcntTypeId = @numExpenseAccountTypeID,
    @numParntAcntTypeID = @numNCEccountTypeID, @vcAccountName = 'Depreciation',
    @vcAccountDescription = 'The amount of the asset''s cost (based on the useful life) that was consumed during the period',
    @monOriginalOpeningBal = $0, @monOpeningBal = $0,
    @dtOpeningDate = @dtTodayDate, @bitActive = 1, @numAccountId = 0,
    @bitFixed = 0, @numDomainID = @numDomainID   , @numListItemID = 0, @numUserCntId = 1,
    @bitProfitLoss = 0, @bitDepreciation = 0,
    @dtDepreciationCostDate = '1753-01-01', @monDepreciationCost = $0.0000,
    @vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, @bitIsSubAccount = 0, @numParentAccId = 0
    
--Create new account with the name �Amortization� under non-cash expense.   

PRINT 43 
EXEC usp_InsertNewChartAccountDetails @numAcntTypeId = @numExpenseAccountTypeID,
    @numParntAcntTypeID = @numNCEccountTypeID, @vcAccountName = 'Amortization',
    @vcAccountDescription = 'Amortization',
    @monOriginalOpeningBal = $0, @monOpeningBal = $0,
    @dtOpeningDate = @dtTodayDate, @bitActive = 1, @numAccountId = 0,
    @bitFixed = 0, @numDomainID = @numDomainID   , @numListItemID = 0, @numUserCntId = 1,
    @bitProfitLoss = 0, @bitDepreciation = 0,
    @dtDepreciationCostDate = '1753-01-01', @monDepreciationCost = $0.0000,
    @vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, @bitIsSubAccount = 0, @numParentAccId = 0
    
--Legal Expense should be under Administrative Expenses.
PRINT 30
EXEC usp_InsertNewChartAccountDetails @numAcntTypeId = @numExpenseAccountTypeID,
    @numParntAcntTypeID = @numAdminExpenseID, @vcAccountName = 'Legal Expenses',
    @vcAccountDescription = 'Expenses incurred on any legal matters',
    @monOriginalOpeningBal = $0, @monOpeningBal = $0,
    @dtOpeningDate = @dtTodayDate, @bitActive = 1, @numAccountId = 0,
    @bitFixed = 0, @numDomainID = @numDomainID   , @numListItemID = 0, @numUserCntId = 1,
    @bitProfitLoss = 0, @bitDepreciation = 0,
    @dtDepreciationCostDate = '1753-01-01', @monDepreciationCost = $0.0000,
    @vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, @bitIsSubAccount = 0, @numParentAccId = 0

--Owner�s contribution, owner�s draw, and common stock should be under Shareholder�s Fund.

--SELECT * FROM dbo.AccountTypeDetail WHERE numDomainID = @numDomainID   AND vcAccountCode = '010502' 
DECLARE @numOCccountTypeID AS NUMERIC(9,0)
SELECT @numOCccountTypeID = numAccountTypeID FROM dbo.AccountTypeDetail WHERE vcAccountCode = '010502'  AND numDomainID = @numDomainID  
PRINT @numOCccountTypeID
        
PRINT 16
EXEC usp_InsertNewChartAccountDetails @numAcntTypeId = @numEquityAccountTypeID,
    @numParntAcntTypeID = @numOCccountTypeID, @vcAccountName = 'Owner''s Contribution',
    @vcAccountDescription = 'Funds contributed by the owner',
    @monOriginalOpeningBal = $0, @monOpeningBal = $0,
    @dtOpeningDate = @dtTodayDate, @bitActive = 1, @numAccountId = 0,
    @bitFixed = 0, @numDomainID = @numDomainID   , @numListItemID = 0, @numUserCntId = 1,
    @bitProfitLoss = 0, @bitDepreciation = 0,
    @dtDepreciationCostDate = '1753-01-01', @monDepreciationCost = $0.0000,
    @vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, @bitIsSubAccount = 0, @numParentAccId = 0

PRINT 17
EXEC usp_InsertNewChartAccountDetails @numAcntTypeId = @numEquityAccountTypeID,
    @numParntAcntTypeID = @numOCccountTypeID, @vcAccountName = 'Owner''s Draw',
    @vcAccountDescription = 'Withdrawals by the owners',
    @monOriginalOpeningBal = $0, @monOpeningBal = $0,
    @dtOpeningDate = @dtTodayDate, @bitActive = 1, @numAccountId = 0,
    @bitFixed = 0, @numDomainID = @numDomainID   , @numListItemID = 0, @numUserCntId = 1,
    @bitProfitLoss = 0, @bitDepreciation = 0,
    @dtDepreciationCostDate = '1753-01-01', @monDepreciationCost = $0.0000,
    @vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, @bitIsSubAccount = 0, @numParentAccId = 0

--SELECT * FROM dbo.AccountTypeDetail WHERE numDomainID = @numDomainID   AND vcAccountCode = '01050102' 
DECLARE @numSFccountTypeID AS NUMERIC(9,0)
SELECT @numSFccountTypeID = numAccountTypeID FROM dbo.AccountTypeDetail WHERE vcAccountCode = '01050102'  AND numDomainID = @numDomainID  
PRINT @numSFccountTypeID

PRINT 19
EXEC usp_InsertNewChartAccountDetails @numAcntTypeId = @numEquityAccountTypeID,
    @numParntAcntTypeID = @numSFccountTypeID, @vcAccountName = 'Common Stock',
    @vcAccountDescription = 'The value of shares purchased by the shareholders',
    @monOriginalOpeningBal = $0, @monOpeningBal = $0,
    @dtOpeningDate = @dtTodayDate, @bitActive = 1, @numAccountId = 0,
    @bitFixed = 0, @numDomainID = @numDomainID   , @numListItemID = 0, @numUserCntId = 1,
    @bitProfitLoss = 0, @bitDepreciation = 0,
    @dtDepreciationCostDate = '1753-01-01', @monDepreciationCost = $0.0000,
    @vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, @bitIsSubAccount = 0, @numParentAccId = 0
        
--Change the name of �Capital & Reserves� to �Reserves & Surplus�.
--SELECT * FROM dbo.AccountTypeDetail WHERE numDomainID = @numDomainID   AND vcAccountCode = '01050101' 
--UPDATE AccountTypeDetail SET vcAccountType = 'Reserves & Surplus' WHERE numDomainID = @numDomainID   AND vcAccountCode = '01050101' 

--�Profit & Loss� and �retained Earning� should be under Reserves & Surplus.
--SELECT * FROM dbo.AccountTypeDetail WHERE vcAccountCode = '01050101' AND numDomainID = @numDomainID  


--add 2 new COA under : Indirect expense->UnCategorized Expense
--					  : Indirect Income->UnCategorized Income
    
EXEC usp_InsertNewChartAccountDetails @numAcntTypeId = @numIncomeAccountTypeID,
    @numParntAcntTypeID = @numIndirectIncomeAccountTypeID , @vcAccountName = 'UnCategorized Income',
    @vcAccountDescription = 'UnCategorized Income',
    @monOriginalOpeningBal = $0, @monOpeningBal = $0,
    @dtOpeningDate = @dtTodayDate, @bitActive = 1, @numAccountId = 0,
    @bitFixed = 0, @numDomainID = @numDomainID   , @numListItemID = 0, @numUserCntId = 1,
    @bitProfitLoss = 0, @bitDepreciation = 0,
    @dtDepreciationCostDate = '1753-01-01', @monDepreciationCost = $0.0000,
    @vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, @bitIsSubAccount = 0, @numParentAccId = 0

------- Add new Account type UnCategorized Expense and move account "UnCategorized Expense" under it 

DECLARE @numUnCategorizedExpenseID AS NUMERIC(9,0)
SELECT @numUnCategorizedExpenseID = numAccountTypeID FROM dbo.AccountTypeDetail WHERE vcAccountCode = '01040206'  AND numDomainID = @numDomainID  
PRINT 'numUnCategorizedExpenseID : ' + CONVERT(VARCHAR(10),@numUnCategorizedExpenseID)

EXEC usp_InsertNewChartAccountDetails @numAcntTypeId = @numExpenseAccountTypeID,
    @numParntAcntTypeID = @numUnCategorizedExpenseID , @vcAccountName = 'UnCategorized Expense',
    @vcAccountDescription = 'UnCategorized Expense',
    @monOriginalOpeningBal = $0, @monOpeningBal = $0,
    @dtOpeningDate = @dtTodayDate, @bitActive = 1, @numAccountId = 0,
    @bitFixed = 0, @numDomainID = @numDomainID   , @numListItemID = 0, @numUserCntId = 1,
    @bitProfitLoss = 0, @bitDepreciation = 0,
    @dtDepreciationCostDate = '1753-01-01', @monDepreciationCost = $0.0000,
    @vcNumber = '-1'/*pass -1 to indicate this call is being made from This Procedure */, @bitIsSubAccount = 0, @numParentAccId = 0

----------------

--SELECT * FROM dbo.AccountingCharges WHERE numDomainID = 1
DELETE FROM dbo.AccountingCharges WHERE numDomainID = @numDomainID

DECLARE @strSQL AS NVARCHAR(MAX)
DECLARE @ST AS VARCHAR(10)
SELECT @ST = numAccountId FROM dbo.Chart_Of_Accounts WHERE numDomainId = @numDomainId AND vcAccountName = 'Sales Tax Payable'
PRINT 'Sales Tax Payable : ' + @ST

DECLARE @AR AS VARCHAR(10)
SELECT @AR = numAccountId FROM dbo.Chart_Of_Accounts WHERE numDomainId = @numDomainId AND vcAccountName = 'Account Receivable'
PRINT 'Account Receivable : ' + @AR

DECLARE @SC AS VARCHAR(10)
SELECT @SC = numAccountId FROM dbo.Chart_Of_Accounts WHERE numDomainId = @numDomainId AND vcAccountName = 'Sales Clearing'
PRINT 'Sales Clearing : ' + @SC

DECLARE @DG AS VARCHAR(10)
SELECT @DG = numAccountId FROM dbo.Chart_Of_Accounts WHERE numDomainId = @numDomainId AND vcAccountName = 'Sales Discounts'
PRINT 'Sales Discounts : ' + @DG

DECLARE @SI AS VARCHAR(10)
SELECT @SI = numAccountId FROM dbo.Chart_Of_Accounts WHERE numDomainId = @numDomainId AND vcAccountName = 'Shipping Income'
PRINT 'Shipping Income: ' + @SI

DECLARE @LC AS VARCHAR(10)
SELECT @LC = numAccountId FROM dbo.Chart_Of_Accounts WHERE numDomainId = @numDomainId AND vcAccountName = 'Late Charges'
PRINT 'Late Charges :' + @LC

DECLARE @AP AS VARCHAR(10)
SELECT @AP = numAccountId FROM dbo.Chart_Of_Accounts WHERE numDomainId = @numDomainId AND vcAccountName = 'Accounts Payable'
PRINT 'Account Payable : ' + @AP

DECLARE @UF AS VARCHAR(10)
SELECT @UF = numAccountId FROM dbo.Chart_Of_Accounts WHERE numDomainId = @numDomainId AND vcAccountName = 'UnDeposited Funds'
PRINT 'UnDeposited Funds : ' + @UF

DECLARE @BE AS VARCHAR(10)
SELECT @BE = numAccountId FROM dbo.Chart_Of_Accounts WHERE numDomainId = @numDomainId AND vcAccountName = 'Billable Time & Expenses'
PRINT 'Billable Time & Expenses : ' + @BE

DECLARE @CG AS VARCHAR(10)
SELECT @CG = numAccountId FROM dbo.Chart_Of_Accounts WHERE numDomainId = @numDomainId AND vcAccountName = 'Cost of Goods Sold'
PRINT 'Cost of Goods Sold : ' + @CG

DECLARE @EP AS VARCHAR(10)
SELECT @EP = numAccountId FROM dbo.Chart_Of_Accounts WHERE numDomainId = @numDomainId AND vcAccountName = 'Employee Payroll Expense'
PRINT 'Employee Payroll Expense : ' + @EP

DECLARE @CP AS VARCHAR(10)
SELECT @CP = numAccountId FROM dbo.Chart_Of_Accounts WHERE numDomainId = @numDomainId AND vcAccountName = 'Contract Employee Payroll Expense'
PRINT 'Contract Employee Payroll Expense : ' + @CP

DECLARE @PL AS VARCHAR(10)
SELECT @PL = ISNULL(numAccountId,0) FROM dbo.Chart_Of_Accounts WHERE numDomainId = @numDomainId AND vcAccountName = 'Wages & Salaries Payable'
--SELECT ISNULL(numAccountId,0) FROM dbo.Chart_Of_Accounts WHERE numDomainId = 1 AND vcAccountName = 'Payroll Liability'
--SET @WSP = '1010'
PRINT 'Wages & Salaries Payable : ' + @PL

DECLARE @WP AS VARCHAR(10)
SELECT @WP = ISNULL(numAccountId,0) FROM dbo.Chart_Of_Accounts WHERE numDomainId = @numDomainId AND vcAccountName = 'Work in Progress'
--SET @WP = '1010'
PRINT 'Wok in Progress : ' + @WP

DECLARE @DP AS VARCHAR(10)       
SELECT @DP = ISNULL(numAccountId,0) FROM dbo.Chart_Of_Accounts WHERE numDomainId = @numDomainId AND vcAccountName = 'Deduction from Employee Payroll'
--SET @DP = '1010'
PRINT 'Deductions form Employee Payroll : ' + @DP

DECLARE @UI AS VARCHAR(10)       
SELECT @UI = ISNULL(numAccountId,0) FROM dbo.Chart_Of_Accounts WHERE numDomainId = @numDomainId AND vcAccountName = 'UnCategorized Income'
--SET @DP = '1010'
PRINT 'UnCategorized Income : ' + @UI

DECLARE @UE AS VARCHAR(10)       
SELECT @UE = ISNULL(numAccountId,0) FROM dbo.Chart_Of_Accounts WHERE numDomainId = @numDomainId AND ISNULL(vcAccountName,'') = 'UnCategorized Expense'
--SET @DP = '1010'
PRINT 'UnCategorized Expense : ' + CONVERT(VARCHAR(10),@UE)

DECLARE @RC AS VARCHAR(10)       
SELECT @RC = ISNULL(numAccountId,0) FROM dbo.Chart_Of_Accounts WHERE numDomainId = @numDomainId AND vcAccountName = 'Reconciliation Discrepancies'
--SET @DP = '1010'
PRINT 'Reconciliation Discrepancies : ' + @RC

DECLARE @FE AS VARCHAR(10)       
SELECT @FE = ISNULL(numAccountId,0) FROM dbo.Chart_Of_Accounts WHERE numDomainId = @numDomainId AND vcAccountName = 'Foreign Exchange Gain/Loss'
PRINT 'Foreign Exchange Gain/Loss : ' + ISNULL(@FE,'')

DECLARE @IA AS VARCHAR(10)       
SELECT @IA = ISNULL(numAccountId,0) FROM dbo.Chart_Of_Accounts WHERE numDomainId = @numDomainId AND vcAccountName = 'Inventory Adjustment'
PRINT 'Inventory Adjustment : ' + ISNULL(@IA,'')

DECLARE @OE AS VARCHAR(10)       
SELECT @OE = ISNULL(numAccountId,0) FROM dbo.Chart_Of_Accounts WHERE numDomainId = @numDomainId AND vcAccountName = 'Opening Balance Equity'
PRINT 'Opening Balance Equity : ' + ISNULL(@OE,'')

DECLARE @PC AS VARCHAR(10)       
SELECT @PC = ISNULL(numAccountId,0) FROM dbo.Chart_Of_Accounts WHERE numDomainId = @numDomainId AND vcAccountName = 'Purchase Clearing'

PRINT 'Purchase Clearing : ' + ISNULL(@PC,'')

DECLARE @PV AS VARCHAR(10)       
SELECT @PV = ISNULL(numAccountId,0) FROM dbo.Chart_Of_Accounts WHERE numDomainId = @numDomainId AND vcAccountName = 'Purchase Price Variance'

PRINT 'Purchase Price Variance : ' + ISNULL(@PV,'')

DECLARE @DI AS VARCHAR(10)
SELECT @DI = numAccountId FROM dbo.Chart_Of_Accounts WHERE numDomainId = @numDomainId AND vcAccountName = 'Deferred Income'
PRINT 'Deferred Income : ' + @DI


--SET @strSQL = ''
SET @strSQL = ' exec USP_ManageDefaultAccountsForDomain @numDomainID = ' + CONVERT(VARCHAR(10),@numDomainId) + ',@str=''<NewDataSet>
																			  <Table1>
																				<chChargeCode>EP</chChargeCode>
																				<numAccountID>' + ISNULL(@EP,'0') + '</numAccountID>
																			  </Table1>
																			  <Table1>
																				<chChargeCode>CP</chChargeCode>
																				<numAccountID>' + ISNULL(@CP,'0') + '</numAccountID>
																			  </Table1>
																			  <Table1>
																				<chChargeCode>ST</chChargeCode>
																				<numAccountID>' + ISNULL(@ST,'0') + '</numAccountID>
																			  </Table1>
																			  <Table1>
																				<chChargeCode>AR</chChargeCode>
																				<numAccountID>' + ISNULL(@AR,'0') + '</numAccountID>
																			  </Table1>
																			  <Table1>
																				<chChargeCode>DG</chChargeCode>
																				<numAccountID>' + ISNULL(@DG,'0') + '</numAccountID>
																			  </Table1>
																			  <Table1>
																				<chChargeCode>SI</chChargeCode>
																				<numAccountID>' + ISNULL(@SI,'0') + '</numAccountID>
																			  </Table1>
																			  <Table1>
																				<chChargeCode>LC</chChargeCode>
																				<numAccountID>' + ISNULL(@LC,'0') + '</numAccountID>
																			  </Table1>
																			  <Table1>
																				<chChargeCode>AP</chChargeCode>
																				<numAccountID>' + ISNULL(@AP,'0') + '</numAccountID>
																			  </Table1>
																			  <Table1>
																				<chChargeCode>UF</chChargeCode>
																				<numAccountID>' + ISNULL(@UF,'0') + '</numAccountID>
																			  </Table1>
																			  <Table1>
																				<chChargeCode>BE</chChargeCode>
																				<numAccountID>' + ISNULL(@BE,'0') + '</numAccountID>
																			  </Table1>
																			  <Table1>
																				<chChargeCode>CG</chChargeCode>
																				<numAccountID>' + ISNULL(@CG,'0') + '</numAccountID>
																			  </Table1>
																			  <Table1>
																				<chChargeCode>PL</chChargeCode>
																				<numAccountID>' + ISNULL(@PL,'0') + '</numAccountID>
																			  </Table1>
																			  <Table1>
																				<chChargeCode>WP</chChargeCode>
																				<numAccountID>' + ISNULL(@WP,'0') + '</numAccountID>
																			  </Table1>
																			  <Table1>
																				<chChargeCode>DP</chChargeCode>
																				<numAccountID>' + ISNULL(@DP,'0') + '</numAccountID>
																			  </Table1>
																			  <Table1>
																				<chChargeCode>UI</chChargeCode>
																				<numAccountID>' + ISNULL(@UI,'0') + '</numAccountID>
																			  </Table1>
																			  <Table1>
																				<chChargeCode>UE</chChargeCode>
																				<numAccountID>' + ISNULL(@UE,'0') + '</numAccountID>
																			  </Table1>
																			  <Table1>
																				<chChargeCode>RC</chChargeCode>
																				<numAccountID>' + ISNULL(@RC,'0') + '</numAccountID>
																			  </Table1>
																			  <Table1>
																				<chChargeCode>FE</chChargeCode>
																				<numAccountID>' + ISNULL(@FE,'0') + '</numAccountID>
																			  </Table1>
																			  <Table1>
																				<chChargeCode>IA</chChargeCode>
																				<numAccountID>' + ISNULL(@IA,'0') + '</numAccountID>
																			  </Table1>
																			  <Table1>
																				<chChargeCode>OE</chChargeCode>
																				<numAccountID>' + ISNULL(@OE,'0') + '</numAccountID>
																			  </Table1>
																			  <Table1>
																				<chChargeCode>PC</chChargeCode>
																				<numAccountID>' + ISNULL(@PC,'0') + '</numAccountID>
																			  </Table1>
																			  <Table1>
																				<chChargeCode>PV</chChargeCode>
																				<numAccountID>' + ISNULL(@PV,'0') + '</numAccountID>
																			  </Table1>
																			  <Table1>
																				<chChargeCode>SC</chChargeCode>
																				<numAccountID>' + ISNULL(@SC,'0') + '</numAccountID>
																			  </Table1>
																			  <Table1>
																				<chChargeCode>DI</chChargeCode>
																				<numAccountID>' + ISNULL(@DI,'0') + '</numAccountID>
																			  </Table1>
																			</NewDataSet>'''

PRINT CAST(@strSQL AS VARCHAR(MAX))
EXEC SP_EXECUTESQL @strSQL

------- Setting up Default Accounts for Income, Asset & COGS of domain. ---------------------------------------
DECLARE @AssetAccountID AS VARCHAR(10)
SELECT @AssetAccountID = numAccountId FROM dbo.Chart_Of_Accounts WHERE vcAccountName = 'Inventory' AND numDomainID = @numDomainID
PRINT 'AssetAccountID : ' + @AssetAccountID

DECLARE @IncomeAccountID AS VARCHAR(10)
SELECT @IncomeAccountID = numAccountId FROM dbo.Chart_Of_Accounts WHERE vcAccountName = 'Sales' AND numDomainID = @numDomainID
PRINT 'IncomeAccountID : ' + @IncomeAccountID

DECLARE @COGSAccountID AS VARCHAR(10)
SELECT @COGSAccountID = numAccountId FROM dbo.Chart_Of_Accounts WHERE vcAccountName = 'Cost of Goods Sold' AND numDomainID = @numDomainID
PRINT 'COGSAccountID : ' + @COGSAccountID

UPDATE dbo.Domain SET numAssetAccID = @AssetAccountID, numIncomeAccID = @IncomeAccountID, numCOGSAccID = @COGSAccountID WHERE numDomainId = @numDomainID
------------------------------------------------------------

DECLARE @numModuleID AS INT

------- UPDATE COARelationships FOR DEFAULT ACCOUNT SETTING FOR Accounts Receivable & Accounts Payable --------
DECLARE @numCustRelationshipID AS NUMERIC(10)
DECLARE @numEmpRelationshipID AS NUMERIC(10)
DECLARE @numVendorRelationShipID AS NUMERIC(10)

DECLARE @numListID AS NUMERIC(10)
SELECT @numListID = numListID FROM dbo.ListMaster WHERE vcListName = 'Relationship'  AND bitFlag = 1
PRINT @numListID

SELECT @numCustRelationshipID = numListItemID FROM dbo.ListDetails WHERE numListID = @numListID
AND constFlag = 1 
--AND numDomainID = @numDomainID
AND vcData = 'Customer'
PRINT @numCustRelationshipID

SELECT @numEmpRelationshipID = numListItemID FROM dbo.ListDetails WHERE numListID = @numListID
AND constFlag = 1 
--AND numDomainID = @numDomainID
AND vcData = 'Employer'
PRINT @numEmpRelationshipID

SELECT @numVendorRelationShipID = numListItemID FROM dbo.ListDetails WHERE numListID = @numListID
AND constFlag = 1 
--AND numDomainID = @numDomainID
AND vcData = 'Vendor'
PRINT @numVendorRelationShipID


DELETE FROM dbo.COARelationships WHERE numDomainID = @numDomainID

INSERT INTO dbo.COARelationships (numRelationshipID,numARParentAcntTypeID,numAPParentAcntTypeID,numARAccountId,numAPAccountId,numDomainID,dtCreateDate,dtModifiedDate,numCreatedBy,numModifiedBy) 
SELECT @numCustRelationshipID,@numARAccountTypeID,@numAPAccountTypeID,@AR,@AP,@numDomainID,GETDATE(),NULL,NULL,NULL
UNION ALL
SELECT @numEmpRelationshipID,@numARAccountTypeID,@numAPAccountTypeID,@AR,@AP,@numDomainID,GETDATE(),NULL,NULL,NULL
UNION ALL
SELECT @numVendorRelationShipID,@numARAccountTypeID,@numAPAccountTypeID,@AR,@AP,@numDomainID,GETDATE(),NULL,NULL,NULL
---------------------------------------------------------------------------------------------------------------------                   
 
------- UPDATE COAShippingMapping FOR DEFAULT SHIPPING METHOD MAPPING SETTING ---------------------------------
--SELECT * FROM dbo.COAShippingMapping WHERE numDomainID = @numDomainID
DELETE FROM dbo.COAShippingMapping WHERE numDomainID = @numDomainID
--SELECT * FROM dbo.Chart_Of_Accounts WHERE vcAccountName = 'Shipping Income' AND numDomainId = @numDomainID AND bitActive = 0
DECLARE @numShipIncomeID AS NUMERIC(10)
SELECT @numShipIncomeID = numAccountId FROM dbo.Chart_Of_Accounts WHERE vcAccountName = 'Shipping Income' AND numDomainId = @numDomainID --AND bitActive = 0
PRINT @numShipIncomeID

INSERT INTO dbo.COAShippingMapping (numShippingMethodID,numIncomeAccountID,numDomainID) 
SELECT 88,@numShipIncomeID,@numDomainID
UNION ALL
SELECT 91,@numShipIncomeID,@numDomainID
UNION ALL
SELECT 90,@numShipIncomeID,@numDomainID

--Script to Correct  Account type for COA -added by chintan
                SELECT  *
                INTO    #TempAccounts
                FROM    ( SELECT    numAccountId,
                                    vcAccountName,
                                    COA.vcAccountCode,
                                    COA.numParntAcntTypeId,
                                    COA.numAcntTypeId OLDnumAcntTypeId,
                                    ( SELECT    ATD.numAccountTypeID
                                      FROM      dbo.AccountTypeDetail ATD
                                      WHERE     ATD.numDomainID = COA.numDomainID
                                                AND ATD.vcAccountCode = SUBSTRING(COA.vcAccountCode, 1, 4)
                                    ) NEWnumAcntTypeId
                          FROM      dbo.Chart_Of_Accounts COA
                          WHERE     COA.numDomainId = @numDomainId
                        ) X
                WHERE   X.OLDnumAcntTypeId <> X.NEWnumAcntTypeId

--                SELECT  *
--                FROM    #TempAccounts

                UPDATE  dbo.Chart_Of_Accounts
                SET     numAcntTypeId = X.NEWnumAcntTypeId
                FROM    #TempAccounts X
                WHERE   X.numAccountID = dbo.Chart_Of_Accounts.numAccountId

                DROP TABLE #TempAccounts
--
--
----- MAPPING CUSTOMER AND VENDOR RELATION WITH ITS DEFAULT COA.
--
--

                DECLARE @numCustCount INT ;

                SET @numCustCount = 0
                SELECT  @numCustCount = COUNT(*)
                FROM    [ListDetails]
                WHERE   [numListID] = 5
                        AND ( constFlag = 1
                              OR [numDomainID] = @numDomainId
                            )
                        AND vcData = 'Customer' ;


                IF @numCustCount > 0 
                    BEGIN
                        INSERT  INTO COARelationships
                                SELECT  ( SELECT    numListItemId
                                          FROM      [ListDetails]
                                          WHERE     [numListID] = 5
                                                    AND ( constFlag = 1
                                                          OR [numDomainID] = @numDomainId
                                                        )
                                                    AND vcData = 'Customer'
                                        ),
                                        numAcntTypeID,
                                        NULL,
                                        numAccountId,
                                        NULL,
                                        @numDomainID,
                                        GETUTCDATE(),
                                        NULL,
                                        @numUserCntId,
                                        NULL
                                FROM    chart_of_accounts
                                WHERE   numDomainID = @numDomainId
                                        AND vcAccountCode = '0101010501'
                    END
                SET @numCustCount = 0
                SELECT  @numCustCount = COUNT(*)
                FROM    [ListDetails]
                WHERE   [numListID] = 5
                        AND ( constFlag = 1
                              OR [numDomainID] = @numDomainId
                            )
                        AND vcData = 'Vendor' ;
                IF @numCustCount > 0 
                    BEGIN
                        INSERT  INTO COARelationships
                                SELECT  ( SELECT    numListItemId
                                          FROM      [ListDetails]
                                          WHERE     [numListID] = 5
                                                    AND ( constFlag = 1
                                                          OR [numDomainID] = @numDomainId
                                                        )
                                                    AND vcData = 'Vendor'
                                        ),
                                        NULL,
                                        numAcntTypeID,
                                        NULL,
                                        numAccountId,
                                        @numDomainID,
                                        GETUTCDATE(),
                                        NULL,
                                        @numUserCntId,
                                        NULL
                                FROM    chart_of_accounts
                                WHERE   numDomainID = @numDomainId
                                        AND vcAccountCode = '0102010201'
                    END
            END                                                    
    END