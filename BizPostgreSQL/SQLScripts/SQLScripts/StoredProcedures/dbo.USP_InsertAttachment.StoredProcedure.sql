/****** Object:  StoredProcedure [dbo].[USP_InsertAttachment]    Script Date: 07/26/2008 16:18:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'usp_insertattachment' ) 
    DROP PROCEDURE usp_insertattachment
GO
CREATE PROCEDURE [dbo].[USP_InsertAttachment]
@vcFileName as varchar(2000)='',
@vcAttachmentItemId as varchar(2000)='',
@vcAttachmentType as varchar(500)='',
@numEmailHSTRID as numeric(9),
@NewAttachmentName as varchar(500)=NULL,
@vcSize VARCHAR(1000)=''
as   

DECLARE @NewvcFileName varchar(2000)
DECLARE @NewvcAttachmentItemId varchar(2000)
DECLARE @NewvcAttachmentType varchar(500)
--DECLARE @numEmailHSTRID numeric(9)
DECLARE @SNewAttachmentName varchar(500)
--DECLARE @NewExtensionType VARCHAR(100)
DECLARE @NewIcon VARCHAR(1000)
DECLARE @NewvcSize VARCHAR(1000)
SET @vcFileName = @vcFileName + '|'
SET @vcAttachmentType=@vcAttachmentType+'|'
SET @NewAttachmentName =@NewAttachmentName+'|'
SET @vcSize = @vcSize+'|'
WHILE CHARINDEX('|', @vcFileName) > 0
    BEGIN
	
        SET @NewvcFileName = LTRIM(RTRIM(SUBSTRING(@vcFileName, 1,
                                                   CHARINDEX('|', @vcFileName,
                                                             0))))
        SET @NewvcAttachmentType = LTRIM(RTRIM(SUBSTRING(@vcAttachmentType, 1,
                                                         CHARINDEX('|', @vcAttachmentType, 0))))
        SET @SNewAttachmentName = LTRIM(RTRIM(SUBSTRING(@NewAttachmentName, 1,
                                                        CHARINDEX('|', @NewAttachmentName, 0))))
--        SET @NewExtensionType = LTRIM(RTRIM(SUBSTRING(@ExtensionType, 1,
--                                                      CHARINDEX('|', @ExtensionType, 0))))
        SET @NewvcSize = LTRIM(RTRIM(SUBSTRING(@vcSize, 1,
                                               CHARINDEX('|', @vcSize, 0))))
		   

        SET @NewvcFileName = REPLACE(@NewvcFileName, '|', '')
        SET @NewvcAttachmentType = REPLACE(@NewvcAttachmentType, '|', '')
        SET @SNewAttachmentName = REPLACE(@SNewAttachmentName, '|', '')
        --SET @NewExtensionType = REPLACE(@NewExtensionType, '|', '')
        SET @NewvcSize = REPLACE(@NewvcSize, '|', '')
        
--        PRINT 'NewvcFileName :' + @NewvcFileName 
--        PRINT 'NewvcAttachmentType :' + @NewvcAttachmentType 
--        PRINT 'SNewAttachmentName  :' + @SNewAttachmentName 
--        PRINT 'NewExtensionType :' + @NewExtensionType 
--        PRINT 'NewvcSize:' + @NewvcSize 

insert into EmailHstrAttchDtls(numEmailHstrID,vcFileName,vcAttachmentItemId,vcAttachmentType,[vcLocation],[vcSize])
values(@numEmailHSTRID,@NewvcFileName,@vcAttachmentItemId,@NewvcAttachmentType,@SNewAttachmentName,@NewvcSize)

        SET @vcFileName = LTRIM(RTRIM(SUBSTRING(@vcFileName,
                                                CHARINDEX('|', @vcFileName)
                                                + 1, LEN(@vcFileName))))
        SET @vcAttachmentType = LTRIM(RTRIM(SUBSTRING(@vcAttachmentType,
                                                      CHARINDEX('|', @vcAttachmentType)
                                                      + 1,
                                                      LEN(@vcAttachmentType))))
        SET @NewAttachmentName = LTRIM(RTRIM(SUBSTRING(@NewAttachmentName,
                                                       CHARINDEX('|', @NewAttachmentName)
                                                       + 1,
                                                       LEN(@NewAttachmentName))))
--        SET @ExtensionType = LTRIM(RTRIM(SUBSTRING(@ExtensionType,
--                                                   CHARINDEX('|',
--                                                             @ExtensionType)
--                                                   + 1, LEN(@ExtensionType))))
        SET @vcSize = LTRIM(RTRIM(SUBSTRING(@vcSize,
                                            CHARINDEX('|', @vcSize) + 1,
                                            LEN(@vcSize))))
    END
--insert into EmailHstrAttchDtls(numEmailHstrID,vcFileName,vcAttachmentItemId,vcAttachmentType,[vcLocation])
--values(@numEmailHSTRID,@vcFileName,@vcAttachmentItemId,@vcAttachmentType,@NewAttachmentName)
GO
