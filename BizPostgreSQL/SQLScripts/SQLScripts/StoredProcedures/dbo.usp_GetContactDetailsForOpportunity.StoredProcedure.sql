/****** Object:  StoredProcedure [dbo].[usp_GetContactDetailsForOpportunity]    Script Date: 07/26/2008 16:16:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
------------------------------------
--usp_GetContactDetailsForOpportunity

--This SP is used for getting the details of the contact associated with the opportunity
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getcontactdetailsforopportunity')
DROP PROCEDURE usp_getcontactdetailsforopportunity
GO
CREATE PROCEDURE [dbo].[usp_GetContactDetailsForOpportunity]
	@numOppId numeric
--   
AS
	SELECT numcontactId FROM opportunitycontact WHERE numoppid=@numOppId
GO
