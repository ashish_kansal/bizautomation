/****** Object:  StoredProcedure [dbo].[USP_PortalAddItem]    Script Date: 07/26/2008 16:20:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj        
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_portaladditem')
DROP PROCEDURE usp_portaladditem
GO
CREATE PROCEDURE [dbo].[USP_PortalAddItem]                  
@numDivID as numeric(9),                  
@numOppID as numeric(9)=null output ,                  
@numItemCode as numeric(9),                  
@vcPOppName as varchar(200)='' output,                  
@numContactId as numeric(9),                  
@numDomainId as numeric(9),                  
@numUnits as numeric(9)                  
                  
as                  
declare @CRMType as integer     
declare @numRecOwner as integer                  
if @numOppID=  0                   
begin                  
select @CRMType=tintCRMType,@numRecOwner=numRecOwner from DivisionMaster where numDivisionID=@numDivID                  
if @CRMType= 0                   
begin                  
UPDATE DivisionMaster                     
   SET tintCRMType=1                    
   WHERE numDivisionID=@numDivID                    
                                        
end                  
declare @TotalAmount as integer                     
declare @intOppTcode as numeric(9)                    
Select @intOppTcode=max(numOppId)from OpportunityMaster                    
set @intOppTcode =isnull(@intOppTcode,0) + 1                    
set  @vcPOppName=(select  vcCompanyName from CompanyInfo Com                  
join divisionMaster Div                  
on div.numCompanyID=com.numCompanyID                  
where div.numdivisionID=@numDivID)+'-' + @vcPOppName                   
                  
                  
set @vcPOppName=@vcPOppName+ '-'+convert(varchar(4),year(getutcdate()))  +'-A'+ convert(varchar(10),@intOppTcode)                          
insert into OpportunityMaster                    
  (                    
  numContactId,                    
  numDivisionId,                    
  txtComments,                    
  numCampainID,                    
  bitPublicFlag,                    
  tintSource,                    
  vcPOppName,                    
  monPAmount,                    
  numCreatedBy,                    
  bintCreatedDate,                     
  numModifiedBy,                    
  bintModifiedDate,                    
  numDomainId,                     
  tintOppType,          
  intPEstimatedCloseDate,      
  numRecOwner,
  bitOrder                    
  )                    
 Values                    
  (                    
  @numContactId,                    
  @numDivID,                    
  '',                    
  0,                    
  0,                    
  0,                    
  @vcPOppName,                   
  0,                      
  @numContactId,                    
  getutcdate(),                    
  @numContactId,                    
  getutcdate(),                    
  @numDomainId,                    
  3,          
  getutcdate(),         
  @numRecOwner,
  1                
  )                     
set @numOppID=scope_identity()                    
end                   
                  
declare @monPrice as DECIMAL(20,5)             
declare @TotalmonPrice as DECIMAL(20,5)                 

select @monPrice=convert(DECIMAL(20,5),dbo.GetPriceBasedonOrgDisc(@numItemCode,1,@numDivID))   
select @TotalmonPrice=@monPrice*@numUnits                       
                 

insert into OpportunityItems(numOppId,numItemCode,numUnitHour,monPrice,monTotAmount)                  
values(@numOppID,@numItemCode,@numUnits,isnull(@monPrice,0),isnull(@TotalmonPrice,0))                  
                   
                  
select @TotalAmount=sum(monTotAmount)from OpportunityItems where numOppId=@numOppID                  
 update OpportunityMaster set  monPamount=@TotalAmount                  
 where numOppId=@numOppID                 
                
set @vcPOppName=(select  vcPOppName from OpportunityMaster where numOppId=@numOppID )
GO
