/****** Object:  StoredProcedure [dbo].[USP_GetAlertIDs]    Script Date: 07/26/2008 16:16:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created By Pratik Vasani


GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getalertids')
DROP PROCEDURE usp_getalertids
GO
CREATE PROCEDURE [dbo].[USP_GetAlertIDs]  
@numAlertID as numeric(9)=0
as 

select * from AlertDTL where numAlertID = @numAlertID and numAlertDTLid In (50,51)

GO
