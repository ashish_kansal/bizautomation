/****** Object:  StoredProcedure [dbo].[USP_DeleteNews]    Script Date: 07/26/2008 16:15:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_deletenews')
DROP PROCEDURE usp_deletenews
GO
CREATE PROCEDURE [dbo].[USP_DeleteNews]
@numNewsID as numeric(9)=0

as
delete from news where numNewsID=@numNewsID
GO
