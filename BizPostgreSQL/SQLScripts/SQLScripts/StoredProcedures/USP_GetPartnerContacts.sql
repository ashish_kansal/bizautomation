/****** Object:  StoredProcedure [dbo].[USP_GetPartnerContacts]    Script Date: 05/07/2009 22:09:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj                  
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetPartnerContacts')
DROP PROCEDURE USP_GetPartnerContacts
GO
CREATE  PROCEDURE [dbo].[USP_GetPartnerContacts]                                                                                    
 @numDomainID  numeric=0,
 @numOppID numeric=0    ,
 @DivisionID numeric=0                                 
AS                                                                       
BEGIN   
	IF(@numOppID>0 AND @DivisionID=0)
	BEGIN
		SET @DivisionID=(SELECT TOP 1 numPartner FROM OpportunityMaster where numOppID=@numOppID and numDomainId=@numDomainID)
	END
	SELECT A.numContactId,A.vcGivenName FROM AdditionalContactsInformation AS A 
	LEFT JOIN DivisionMaster AS D 
	ON D.numDivisionID=A.numDivisionId
	WHERE D.numDomainID=@numDomainID AND D.numDivisionID=@DivisionID AND A.vcGivenName<>''

END

 