
GO
/****** Object:  StoredProcedure [dbo].[USP_LoadWarehouseBasedOnItem]    Script Date: 09/01/2009 19:12:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--exec USP_LoadWarehouseBasedOnItem 14    
--created by anoop jayaraj    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_loadwarehousebasedonitem')
DROP PROCEDURE usp_loadwarehousebasedonitem
GO
CREATE PROCEDURE [dbo].[USP_LoadWarehouseBasedOnItem]    
@numItemCode as varchar(20)=''  
as    
BEGIN
DECLARE @numDomainID AS NUMERIC(18,0)
DECLARE @numBaseUnit AS NUMERIC(18,0)
DECLARE @vcUnitName as VARCHAR(100) 
DECLARE @numSaleUnit AS NUMERIC(18,0)
DECLARE @vcSaleUnitName as VARCHAR(100) 
DECLARE @numPurchaseUnit AS NUMERIC(18,0)
DECLARE @vcPurchaseUnitName as VARCHAR(100) 
declare @bitSerialize as bit     
DECLARE @bitKitParent BIT
 
SELECT 
	@numDomainID = numDomainID,
	@numBaseUnit = ISNULL(numBaseUnit,0),
	@numSaleUnit = ISNULL(numSaleUnit,0),
	@numPurchaseUnit = ISNULL(numPurchaseUnit,0),
	@bitSerialize=bitSerialized,
	@bitKitParent = ( CASE WHEN ISNULL(bitKitParent,0) = 1
                               AND ISNULL(bitAssembly,0) = 1 THEN 0
                          WHEN ISNULL(bitKitParent,0) = 1 THEN 1
                          ELSE 0
                     END ) 
FROM 
	Item 
WHERE 
	numItemCode=@numItemCode    


SELECT @vcUnitName=ISNULL(vcUnitName,'-') FROM UOM u WHERE u.numUOMId=@numBaseUnit
SELECT @vcSaleUnitName=ISNULL(vcUnitName,'-') FROM UOM u WHERE u.numUOMId=@numSaleUnit
SELECT @vcPurchaseUnitName=ISNULL(vcUnitName,'-') FROM UOM u WHERE u.numUOMId=@numPurchaseUnit

DECLARE @numSaleUOMFactor AS DECIMAL(28,14)
DECLARE @numPurchaseUOMFactor AS DECIMAL(28,14)

SELECT @numSaleUOMFactor = dbo.fn_UOMConversion(@numSaleUnit,@numItemCode,@numDOmainID,@numBaseUnit)
SELECT @numPurchaseUOMFactor = dbo.fn_UOMConversion(@numPurchaseUnit,@numItemCode,@numDOmainID,@numBaseUnit)

SELECT 
	DISTINCT(WareHouseItems.numWareHouseItemId),
	ISNULL(WareHouseItems.numWLocationID,0) numWLocationID,
	CONCAT(vcWareHouse, (CASE WHEN WareHouseItems.numWLocationID = -1 THEN ' (Global)' ELSE (CASE WHEN ISNULL(WarehouseLocation.numWLocationID,0) > 0 THEN CONCAT(' (',WarehouseLocation.vcLocation,')') ELSE '' END) END)) AS vcWareHouse, 
	dbo.fn_GetAttributes(WareHouseItems.numWareHouseItemId,@bitSerialize) as Attr, 
	WareHouseItems.monWListPrice,
	CASE WHEN @bitKitParent=1 THEN dbo.fn_GetKitInventory(@numItemCode,WareHouseItems.numWareHouseID) else ISNULL(numOnHand,0) END numOnHand,
	CASE WHEN @bitKitParent=1 THEN 0 ELSE ISNULL(numOnOrder,0) END numOnOrder,
	CASE WHEN @bitKitParent=1 THEN 0 ELSE ISNULL(numReorder,0) END numReorder,
	CASE WHEN @bitKitParent=1 THEN 0 ELSE ISNULL(numAllocation,0) END numAllocation,
	CASE WHEN @bitKitParent=1 THEN 0 ELSE ISNULL(numBackOrder,0) END numBackOrder,
	Warehouses.numWareHouseID,
	@vcUnitName AS vcUnitName,
	@vcSaleUnitName AS vcSaleUnitName,
	@vcPurchaseUnitName AS vcPurchaseUnitName,
	CASE WHEN @numBaseUnit > 0 AND @numSaleUnit > 0
	THEN
		CASE WHEN @bitKitParent=1 THEN CAST(CAST(dbo.fn_GetKitInventory(@numItemCode,WareHouseItems.numWareHouseID)/@numSaleUOMFactor AS INT) AS VARCHAR) else CAST(CAST(ISNULL(numOnHand,0)/@numSaleUOMFactor AS INT) AS VARCHAR) END
	ELSE
		'-'
	END AS numOnHandUOM,
	CASE WHEN @numBaseUnit > 0 AND @numSaleUnit > 0
	THEN
		CASE WHEN @bitKitParent=1 THEN '0' else CAST(CAST(ISNULL(numOnOrder,0)/@numPurchaseUOMFactor AS INT) AS VARCHAR) END
	ELSE
		'-'
	END AS numOnOrderUOM,
	CASE WHEN @numBaseUnit > 0 AND @numSaleUnit > 0
	THEN
		CASE WHEN @bitKitParent=1 THEN '0' else CAST(CAST(ISNULL(numReorder,0)/@numSaleUOMFactor AS INT) AS VARCHAR) END
	ELSE
		'-'
	END AS numReorderUOM,
	CASE WHEN @numBaseUnit > 0 AND @numSaleUnit > 0
	THEN
		CASE WHEN @bitKitParent=1 THEN '0' else CAST(CAST(ISNULL(numAllocation,0)/@numSaleUOMFactor AS INT) AS VARCHAR) END
	ELSE
		'-'
	END AS numAllocationUOM,
	CASE WHEN @numBaseUnit > 0 AND @numSaleUnit > 0
	THEN
		CASE WHEN @bitKitParent=1 THEN '0' else CAST(CAST(ISNULL(numBackOrder,0)/@numSaleUOMFactor AS INT) AS VARCHAR) END
	ELSE
		'-'
	END AS numBackOrderUOM
FROM 
	WareHouseItems    
JOIN 
	Warehouses 
ON 
	Warehouses.numWareHouseID=WareHouseItems.numWareHouseID  
LEFT JOIN
	WarehouseLocation
ON
	WareHouseItems.numWLocationID = WarehouseLocation.numWLocationID
WHERE 
	WareHouseItems.numDomainID = @numDomainID
	AND numItemID=@numItemCode
ORDER BY
	numWareHouseItemId ASC
END
GO