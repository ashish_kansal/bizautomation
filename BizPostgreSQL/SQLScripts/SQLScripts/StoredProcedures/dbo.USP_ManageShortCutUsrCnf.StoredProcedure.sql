/****** Object:  StoredProcedure [dbo].[USP_ManageShortCutUsrCnf]    Script Date: 07/26/2008 16:20:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj            
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_manageshortcutusrcnf')
DROP PROCEDURE usp_manageshortcutusrcnf
GO
CREATE PROCEDURE [dbo].[USP_ManageShortCutUsrCnf]              
@numGroupID as numeric(9)=0,              
@numDomainId as numeric(9)=0,              
@numContactId as numeric(9)=0,    
@strFav as text='',  
@numTabId as numeric(9)        
    
as              
   
------------------------------------------------------------------------------------------    
--For Links    
------------------------------------------------------------------------------------------              
--IF (select count(*) from ShortCutUsrCnf where numTabId=@numTabId and numDomainId = @numDomainId and numGroupID =@numGroupID and numcontactId= @numContactId ) > 0    
    
 --begin     
  delete ShortCutUsrCnf where numTabId=@numTabId and numDomainId = @numDomainId and numGroupID =@numGroupID  and numcontactId= @numContactId  
 --end    
    
    
   declare @hDoc  int              
                 
   EXEC sp_xml_preparedocument @hDoc OUTPUT, @strFav    
                 
    insert into ShortCutUsrCnf              
     (numGroupId,numDomainId,numContactId,numTabId,numOrder,numLinkId,bitInitialPage,tintLinkType,bitFavourite)              
                  
    select @numGroupID,@numDomainId,@numContactId,@numTabId,              
     X.*   from(SELECT * FROM OPENXML (@hDoc,'/NewDataSet/Table',2)              
    WITH  (              
    numOrder numeric,    
    numLinkId numeric,bitInitialPage bit,tintLinkType tinyint,bitFavourite bit))X              
                 
    EXEC sp_xml_removedocument @hDoc              

GO
