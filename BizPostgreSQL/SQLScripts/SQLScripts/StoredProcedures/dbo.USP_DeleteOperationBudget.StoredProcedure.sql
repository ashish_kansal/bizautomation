/****** Object:  StoredProcedure [dbo].[USP_DeleteOperationBudget]    Script Date: 07/26/2008 16:15:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Create by Siva    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_deleteoperationbudget')
DROP PROCEDURE usp_deleteoperationbudget
GO
CREATE PROCEDURE [dbo].[USP_DeleteOperationBudget](    
@numOperationId as numeric(9)=0,    
@numDomainId as numeric(9)=0)    
As    
Begin    
 Delete OBD from operationBudgetMaster OBM 
 inner join OperationBudgetDetails OBD on OBM.numBudgetId=OBD.numBudgetId Where OBD.numBudgetId=@numOperationId  
 And OBM.numDomainId=@numDomainId 
 Delete from operationBudgetMaster Where numBudgetId=@numOperationId  And numDomainId=@numDomainId    
End
GO
