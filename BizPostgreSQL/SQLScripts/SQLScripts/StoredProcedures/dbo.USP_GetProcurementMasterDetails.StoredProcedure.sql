/****** Object:  StoredProcedure [dbo].[USP_GetProcurementMasterDetails]    Script Date: 07/26/2008 16:18:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created by Siva    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getprocurementmasterdetails')
DROP PROCEDURE usp_getprocurementmasterdetails
GO
CREATE PROCEDURE [dbo].[USP_GetProcurementMasterDetails]    
(@numDomainId as numeric(9)=0,      
@intFiscalYear as numeric(9)=0)    
As      
Begin    
   Select numProcurementBudgetId,bitPurchaseDeal From ProcurementBudgetMaster Where  numDomainId=@numDomainId    
End
GO
