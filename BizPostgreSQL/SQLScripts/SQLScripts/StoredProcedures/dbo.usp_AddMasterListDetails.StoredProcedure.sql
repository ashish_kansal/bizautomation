/****** Object:  StoredProcedure [dbo].[usp_AddMasterListDetails]    Script Date: 07/26/2008 16:14:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--modified by anoop jayaraj
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_addmasterlistdetails')
DROP PROCEDURE usp_addmasterlistdetails
GO
CREATE PROCEDURE [dbo].[usp_AddMasterListDetails]  
 @numListID NUMERIC,  
 @vcItemName VARCHAR(150) = '',  
 @vcItemType CHAR(1) = 'L',  
 @numDomainID NUMERIC(9) = 1,  
 @numCreatedBy NUMERIC(9),  
 @bintCreatedDate datetime   
--      
AS  
 DECLARE @itemCount INT  
 IF @vcItemType = 'L'  
  BEGIN  
   SELECT @itemCount =  COUNT(*) FROM ListDetails WHERE vcData = @vcItemName AND numDomainid = @numDomainid  
   AND numListID=@numListID  
   IF @itemCount = 0   
        BEGIN  
    INSERT INTO ListDetails(numListID, vcData, numCreatedBy, bintCreatedDate, numDomainID, constFlag)  
    VALUES(@numListID, @vcItemName, @numCreatedBy, @bintCreatedDate, @numDomainID, 0)  
                      SELECT 1             
                                               END  
                                         ELSE  
                                                 BEGIN  
                                                            SELECT 0  
                                                  END  
  END  
 IF @vcItemType = 'T'  
  BEGIN  
   SELECT @itemCount =  COUNT(*) FROM TerritoryMaster WHERE vcTerName = @vcItemName AND numDomainid = @numDomainid  
   IF @itemCount = 0   
   BEGIN  
    INSERT INTO TerritoryMaster(vcTerName, numCreatedBy, bintCreatedDate, numDomainID)  
    VALUES(@vcItemName, @numCreatedBy, @bintCreatedDate, @numDomainID)  
              SELECT 1    
   END   
   ELSE  
   BEGIN  
                 SELECT 0  
   END                
  END
GO
