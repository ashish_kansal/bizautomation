/****** Object:  StoredProcedure [dbo].[USP_cfwSaveCusfld]    Script Date: 07/26/2008 16:15:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj      
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_cfwsavecusfld')
DROP PROCEDURE usp_cfwsavecusfld
GO
CREATE PROCEDURE [dbo].[USP_cfwSaveCusfld]          
          
@RecordId as numeric(9)=null,          
@strDetails as text='',          
@PageId as tinyint=null          
AS 
BEGIN        
	IF CHARINDEX('<?xml',@strDetails) = 0
	BEGIN
		SET @strDetails = CONCAT('<?xml version="1.0" encoding="ISO-8859-1"?>',@strDetails)
	END

 
declare @hDoc int          
          
        
          
EXEC sp_xml_preparedocument @hDoc OUTPUT, @strDetails          
          
if @PageId=1          
begin          

Delete from CFW_FLD_Values where FldDTLID not in (SELECT FldDTLID FROM OPENXML (@hDoc,'/NewDataSet/Table[FldDTLID>0]',2)          
WITH (fld_id numeric(9),FldDTLID numeric(9),Value varchar(1000))) 
and  RecId= @RecordId      


insert into CFW_FLD_Values (Fld_ID,Fld_Value,RecId)          
select X.fld_id,X.Value,@RecordId  from(SELECT *FROM OPENXML (@hDoc,'/NewDataSet/Table[FldDTLID=0]',2)          
WITH (fld_id numeric(9),FldDTLID numeric(9),Value varchar(1000)))X 


update CFW_FLD_Values set Fld_Value=X.Value  from      
(SELECT * FROM OPENXML (@hDoc,'/NewDataSet/Table[FldDTLID>0]',2)          
WITH (fld_id numeric(9),FldDTLID numeric(9),Value varchar(1000)))X
where CFW_FLD_Values.FldDTLID=X.FldDTLID


--Author :Sachin Sadhu ||Date:13thOctober2014
--Purpose:To make Entry in WorkFLow queue(Identify rules based on Custom fields)

DECLARE @CFFields as table (  
  RowID INT IDENTITY(1, 1),
  fld_id numeric(9)
  )
INSERT INTO @CFFields(fld_id) 
SELECT Fld_ID from CFW_FLD_Values where RecId=@RecordId
--select X.fld_id  from(SELECT *FROM OPENXML (@hDoc,'/NewDataSet/Table[FldDTLID>0]',2)          
--WITH (fld_id numeric(9),FldDTLID numeric(9),Value varchar(1000)))X 

DECLARE @intCnt AS INT
DECLARE @intCntFields AS INT
Declare @numFieldID As numeric(9,0)
Declare @numDomainID as numeric(18,0)
Declare @numUserID as numeric(18,0)
SET @intCnt = 0
SET @intCntFields = (SELECT COUNT(*) FROM @CFFields)
SELECT @numDomainID=numDomainID,@numUserID=numCreatedBy from DivisionMaster where numDivisionID=@RecordId

IF @intCntFields > 0
	BEGIN
		WHILE(@intCnt < @intCntFields)
			BEGIN
			--print 'sahc'
				SET @intCnt = @intCnt + 1
				SELECT @numFieldID = fld_id FROM @CFFields WHERE RowID = @intCnt
				 --Added By :Sachin Sadhu ||Date:13thOct2014
				 --Purpose  :To manage Custom Fields Queue
							EXEC USP_CFW_FLD_Values_CT
									@numDomainID =@numDomainID,
									@numUserCntID =0,
									@numRecordID =@RecordId ,
									@numFormFieldId  =@numFieldID
			END 

	END
End       
-- action item details, hard coded flag to control delete behaviour of custom field bug id 635
if @PageId=128 
begin          

insert into CFW_FLD_Values (Fld_ID,Fld_Value,RecId)          
select X.fld_id,X.Value,@RecordId  from(SELECT *FROM OPENXML (@hDoc,'/NewDataSet/Table[FldDTLID=0]',2)          
WITH (fld_id numeric(9),FldDTLID numeric(9),Value varchar(1000)))X 


update CFW_FLD_Values set Fld_Value=X.Value  from      
(SELECT * FROM OPENXML (@hDoc,'/NewDataSet/Table[FldDTLID>0]',2)          
WITH (fld_id numeric(9),FldDTLID numeric(9),Value varchar(1000)))X
where CFW_FLD_Values.FldDTLID=X.FldDTLID

End        
 
          
if @PageId=4          
begin  

Delete from CFW_FLD_Values_Cont where FldDTLID not in (SELECT FldDTLID FROM OPENXML (@hDoc,'/NewDataSet/Table[FldDTLID>0]',2)          
WITH (fld_id numeric(9),FldDTLID numeric(9),Value varchar(1000))) 
and  RecId= @RecordId      


insert into CFW_FLD_Values_Cont (Fld_ID,Fld_Value,RecId)          
select X.fld_id,X.Value,@RecordId  from(SELECT *FROM OPENXML (@hDoc,'/NewDataSet/Table[FldDTLID=0]',2)          
WITH (fld_id numeric(9),FldDTLID numeric(9),Value varchar(1000)))X 


update CFW_FLD_Values_Cont set Fld_Value=X.Value  from      
(SELECT * FROM OPENXML (@hDoc,'/NewDataSet/Table[FldDTLID>0]',2)          
WITH (fld_id numeric(9),FldDTLID numeric(9),Value varchar(1000)))X
where CFW_FLD_Values_Cont.FldDTLID=X.FldDTLID

--        
--Delete from CFW_FLD_Values_Cont where RecId=@RecordId          
--insert into CFW_FLD_Values_Cont (Fld_ID,Fld_Value,RecId)          
--          
--select X.fld_id,X.Value,@RecordId  from(SELECT *FROM OPENXML (@hDoc,'/NewDataSet/Table[fld_id>0]',2)          
-- WITH (fld_id numeric(9),Value varchar(1000)))X          
    
	
--Author :Sachin Sadhu ||Date:13thOctober2014
--Purpose:To make Entry in WorkFLow queue(Identify rules based on Custom fields)

DECLARE @CFFieldsCon as table (  
  RowID INT IDENTITY(1, 1),
  fld_id numeric(9)
  )
INSERT INTO @CFFieldsCon(fld_id) 
SELECT Fld_ID from CFW_FLD_Values_Cont where RecId=@RecordId
--select X.fld_id  from(SELECT *FROM OPENXML (@hDoc,'/NewDataSet/Table[FldDTLID>0]',2)          
--WITH (fld_id numeric(9),FldDTLID numeric(9),Value varchar(1000)))X 

DECLARE @intCntCon AS INT
DECLARE @intCntFieldsCon AS INT
Declare @numFieldIDCon As numeric(9,0)
Declare @numDomainIDCon as numeric(18,0)
Declare @numUserIDCon as numeric(18,0)
SET @intCntCon = 0
SET @intCntFieldsCon = (SELECT COUNT(*) FROM @CFFieldsCon)
SELECT @numDomainIDCon=numDomainID,@numUserIDCon=numCreatedBy from AdditionalContactsInformation where numContactId=@RecordId

IF @intCntFieldsCon > 0
	BEGIN
		WHILE(@intCntCon < @intCntFieldsCon)
			BEGIN
			
				SET @intCntCon = @intCntCon + 1
				SELECT @numFieldIDCon = fld_id FROM @CFFieldsCon WHERE RowID = @intCntCon
				 --Added By :Sachin Sadhu ||Date:14thOct2014
				 --Purpose  :To manage Custom Fields Queue
							EXEC USP_CFW_FLD_Values_Cont_CT
									@numDomainID =@numDomainIDCon,
									@numUserCntID =@numUserIDCon,
									@numRecordID =@RecordId ,
									@numFormFieldId  =@numFieldIDCon
			END 

	END  
	   
end          
          
if @PageId=3          
BEGIN 
	DECLARE @TEMP TABLE
	(
		fld_id numeric(9),
		FldDTLID numeric(9),
		Value varchar(1000)
	)

	INSERT INTO @TEMP 
	(
		fld_id,
		FldDTLID,
		Value
	)
	SELECT
		fld_id,
		FldDTLID,
		Value
	FROM OPENXML 
		(@hDoc,'/NewDataSet/Table',2)          
	WITH 
		(fld_id NUMERIC(18,0),FldDTLID NUMERIC(18,0),Value VARCHAR(1000)) 

  
	DELETE FROM 
		CFW_FLD_Values_Case 
	WHERE 
		FldDTLID NOT IN (SELECT FldDTLID FROM @TEMP WHERE FldDTLID > 0) 
		AND RecId= @RecordId      
		AND Fld_ID NOT IN (SELECT ISNULL(numChildFieldID,0) FROM ParentChildCustomFieldMap WHERE tintChildModule=3)  


	INSERT INTO CFW_FLD_Values_Case 
	(
		Fld_ID,
		Fld_Value,
		RecId
	)          
	SELECT 
		fld_id,
		Value,
		@RecordId  
	FROM
		@TEMP
	WHERE
		FldDTLID=0
		AND fld_id NOT IN (SELECT ISNULL(numChildFieldID,0) FROM ParentChildCustomFieldMap WHERE tintChildModule=3)


	UPDATE 
		CFW_FLD_Values_Case 
	SET 
		Fld_Value = X.Value  
	FROM
	   @TEMP X
	WHERE
	   X.FldDTLID > 0
	   AND CFW_FLD_Values_Case.FldDTLID=X.FldDTLID
	   AND  CFW_FLD_Values_Case.Fld_ID NOT IN (SELECT ISNULL(numChildFieldID,0) FROM ParentChildCustomFieldMap WHERE tintChildModule=3)           
          
--Delete from CFW_FLD_Values_Case where RecId=@RecordId          
--insert into CFW_FLD_Values_Case (Fld_ID,Fld_Value,RecId)          
--          
--select X.fld_id,X.Value,@RecordId  from(SELECT *FROM OPENXML (@hDoc,'/NewDataSet/Table[fld_id>0]',2)          
-- WITH (fld_id numeric(9),Value varchar(1000)))X          
     
	 --Author :Sachin Sadhu ||Date:13thOctober2014
--Purpose:To make Entry in WorkFLow queue(Identify rules based on Custom fields)

DECLARE @CFFieldsCase as table (  
  RowID INT IDENTITY(1, 1),
  fld_id numeric(9)
  )
INSERT INTO @CFFieldsCase(fld_id) 
SELECT Fld_ID from CFW_FLD_Values_Case where RecId=@RecordId
--select X.fld_id  from(SELECT *FROM OPENXML (@hDoc,'/NewDataSet/Table[FldDTLID>0]',2)          
--WITH (fld_id numeric(9),FldDTLID numeric(9),Value varchar(1000)))X 

DECLARE @intCntCase AS INT
DECLARE @intCntFieldsCase AS INT
Declare @numFieldIDCase As numeric(9,0)
Declare @numDomainIDCase as numeric(18,0)
Declare @numUserIDCase as numeric(18,0)
SET @intCntCase = 0
SET @intCntFieldsCase = (SELECT COUNT(*) FROM @CFFieldsCase)
SELECT @numDomainIDCase=numDomainID,@numUserIDCase=numCreatedBy from Cases where numCaseId=@RecordId

IF @intCntFieldsCase > 0
	BEGIN
		WHILE(@intCntCase < @intCntFieldsCase)
			BEGIN
			--print 'sahc'
				SET @intCntCase = @intCntCase + 1
				SELECT @numFieldIDCase = fld_id FROM @CFFieldsCase WHERE RowID = @intCntCase
				 --Added By :Sachin Sadhu ||Date:13thOct2014
				 --Purpose  :To manage Custom Fields Queue
							EXEC USP_CFW_FLD_Values_Case_CT
									@numDomainID =@numDomainIDCase,
									@numUserCntID =@numUserIDCase,
									@numRecordID =@RecordId ,
									@numFormFieldId  =@numFieldIDCase
			END 

	END
	      
end          
        
if @PageId=5        
begin          


Delete from CFW_FLD_Values_Item where FldDTLID not in (SELECT FldDTLID FROM OPENXML (@hDoc,'/NewDataSet/Table[FldDTLID>0]',2)          
WITH (fld_id numeric(9),FldDTLID numeric(9),Value varchar(1000))) 
and  RecId= @RecordId      


insert into CFW_FLD_Values_Item (Fld_ID,Fld_Value,RecId)          
select X.fld_id,X.Value,@RecordId  from(SELECT *FROM OPENXML (@hDoc,'/NewDataSet/Table[FldDTLID=0]',2)          
WITH (fld_id numeric(9),FldDTLID numeric(9),Value varchar(1000)))X 


update CFW_FLD_Values_Item set Fld_Value=X.Value  from      
(SELECT * FROM OPENXML (@hDoc,'/NewDataSet/Table[FldDTLID>0]',2)          
WITH (fld_id numeric(9),FldDTLID numeric(9),Value varchar(1000)))X
where CFW_FLD_Values_Item.FldDTLID=X.FldDTLID  


--Delete from CFW_FLD_Values_Item where RecId=@RecordId          
--insert into CFW_FLD_Values_Item (Fld_ID,Fld_Value,RecId)          
--          
--select X.fld_id,X.Value,@RecordId  from(SELECT *FROM OPENXML (@hDoc,'/NewDataSet/Table[fld_id>0]',2)          
-- WITH (fld_id numeric(9),Value varchar(1000)))X          
          
end         
          
if @PageId=6 or @PageId=2      
begin       

Delete from CFW_Fld_Values_Opp where FldDTLID not in (SELECT FldDTLID FROM OPENXML (@hDoc,'/NewDataSet/Table[FldDTLID>0]',2)          
WITH (fld_id numeric(9),FldDTLID numeric(9),Value varchar(1000))) 
and  RecId= @RecordId      


insert into CFW_Fld_Values_Opp (Fld_ID,Fld_Value,RecId)          
select X.fld_id,X.Value,@RecordId  from(SELECT *FROM OPENXML (@hDoc,'/NewDataSet/Table[FldDTLID=0]',2)          
WITH (fld_id numeric(9),FldDTLID numeric(9),Value varchar(1000)))X 


update CFW_Fld_Values_Opp set Fld_Value=X.Value  from      
(SELECT * FROM OPENXML (@hDoc,'/NewDataSet/Table[FldDTLID>0]',2)          
WITH (fld_id numeric(9),FldDTLID numeric(9),Value varchar(1000)))X
where CFW_Fld_Values_Opp.FldDTLID=X.FldDTLID 
   
--Delete from CFW_Fld_Values_Opp where RecId=@RecordId          
--insert into CFW_Fld_Values_Opp (Fld_ID,Fld_Value,RecId)          
--          
--select X.fld_id,X.Value,@RecordId  from(SELECT *FROM OPENXML (@hDoc,'/NewDataSet/Table[fld_id>0]',2)          
-- WITH (fld_id numeric(9),Value varchar(1000)))X     
  
--Author :Sachin Sadhu ||Date:13thOctober2014
--Purpose:To make Entry in WorkFLow queue(Identify rules based on Custom fields)

DECLARE @CFFieldsOpp as table (  
  RowID INT IDENTITY(1, 1),
  fld_id numeric(9)
  )
INSERT INTO @CFFieldsOpp(fld_id) 
SELECT Fld_ID from CFW_Fld_Values_Opp where RecId=@RecordId
--select X.fld_id  from(SELECT *FROM OPENXML (@hDoc,'/NewDataSet/Table[FldDTLID>0]',2)          
--WITH (fld_id numeric(9),FldDTLID numeric(9),Value varchar(1000)))X 

DECLARE @intCntOpp AS INT
DECLARE @intCntFieldsOpp AS INT
Declare @numFieldIDOpp As numeric(9,0)
Declare @numDomainIDOpp as numeric(18,0)
Declare @numUserIDOpp as numeric(18,0)
SET @intCntOpp = 0
SET @intCntFieldsOpp = (SELECT COUNT(*) FROM @CFFieldsOpp)
SELECT @numDomainIDOpp=numDomainID,@numUserIDOpp=numCreatedBy from OpportunityMaster where numOppId=@RecordId

IF @intCntFieldsOpp > 0
	BEGIN
		WHILE(@intCntOpp < @intCntFieldsOpp)
			BEGIN
			--print 'sahc'
				SET @intCntOpp = @intCntOpp + 1
				SELECT @numFieldIDOpp = fld_id FROM @CFFieldsOpp WHERE RowID = @intCntOpp
				 --Added By :Sachin Sadhu ||Date:14thOct2014
				 --Purpose  :To manage Custom Fields Queue
							EXEC USP_CFW_Fld_Values_Opp_CT
									@numDomainID =@numDomainIDOpp,
									@numUserCntID =@numUserIDOpp,
									@numRecordID =@RecordId ,
									@numFormFieldId  =@numFieldIDOpp
			END 

	END

   
          
end      
    
if @PageId=7 or    @PageId=8    
begin

Delete from CFW_Fld_Values_Product where FldDTLID not in (SELECT FldDTLID FROM OPENXML (@hDoc,'/NewDataSet/Table[FldDTLID>0]',2)          
WITH (fld_id numeric(9),FldDTLID numeric(9),Value varchar(1000))) 
and  RecId= @RecordId      


insert into CFW_Fld_Values_Product (Fld_ID,Fld_Value,RecId)          
select X.fld_id,X.Value,@RecordId  from(SELECT *FROM OPENXML (@hDoc,'/NewDataSet/Table[FldDTLID=0]',2)          
WITH (fld_id numeric(9),FldDTLID numeric(9),Value varchar(1000)))X 


update CFW_Fld_Values_Product set Fld_Value=X.Value  from      
(SELECT * FROM OPENXML (@hDoc,'/NewDataSet/Table[FldDTLID>0]',2)          
WITH (fld_id numeric(9),FldDTLID numeric(9),Value varchar(1000)))X
where CFW_Fld_Values_Product.FldDTLID=X.FldDTLID
          
--Delete from CFW_Fld_Values_Product where RecId=@RecordId          
--insert into CFW_Fld_Values_Product (Fld_ID,Fld_Value,RecId)          
--          
--select X.fld_id,X.Value,@RecordId  from(SELECT *FROM OPENXML (@hDoc,'/NewDataSet/Table[fld_id>0]',2)          
-- WITH (fld_id numeric(9),Value varchar(1000)))X          
          
end     
          
if @PageId=11  
begin   

Delete from CFW_Fld_Values_Pro where FldDTLID not in (SELECT FldDTLID FROM OPENXML (@hDoc,'/NewDataSet/Table[FldDTLID>0]',2)          
WITH (fld_id numeric(9),FldDTLID numeric(9),Value varchar(1000))) 
and  RecId= @RecordId      


insert into CFW_Fld_Values_Pro (Fld_ID,Fld_Value,RecId)          
select X.fld_id,X.Value,@RecordId  from(SELECT *FROM OPENXML (@hDoc,'/NewDataSet/Table[FldDTLID=0]',2)          
WITH (fld_id numeric(9),FldDTLID numeric(9),Value varchar(1000)))X 


update CFW_Fld_Values_Pro set Fld_Value=X.Value  from      
(SELECT * FROM OPENXML (@hDoc,'/NewDataSet/Table[FldDTLID>0]',2)          
WITH (fld_id numeric(9),FldDTLID numeric(9),Value varchar(1000)))X
where CFW_Fld_Values_Pro.FldDTLID=X.FldDTLID
       
--Delete from CFW_Fld_Values_Pro where RecId=@RecordId          
--insert into CFW_Fld_Values_Pro (Fld_ID,Fld_Value,RecId)          
--          
--select X.fld_id,X.Value,@RecordId  from(SELECT *FROM OPENXML (@hDoc,'/NewDataSet/Table[fld_id>0]',2)          
-- WITH (fld_id numeric(9),Value varchar(1000)))X    


--Author :Sachin Sadhu ||Date:13thOctober2014
--Purpose:To make Entry in WorkFLow queue(Identify rules based on Custom fields)

DECLARE @CFFieldsPro as table (  
  RowID INT IDENTITY(1, 1),
  fld_id numeric(9)
  )
INSERT INTO @CFFieldsPro(fld_id) 
SELECT Fld_ID from CFW_FLD_Values_Pro where RecId=@RecordId
--select X.fld_id  from(SELECT *FROM OPENXML (@hDoc,'/NewDataSet/Table[FldDTLID>0]',2)          
--WITH (fld_id numeric(9),FldDTLID numeric(9),Value varchar(1000)))X 

DECLARE @intCntPro AS INT
DECLARE @intCntFieldsPro AS INT
Declare @numFieldIDPro As numeric(9,0)
Declare @numDomainIDPro as numeric(18,0)
Declare @numUserIDPro as numeric(18,0)
SET @intCntPro = 0
SET @intCntFieldsPro = (SELECT COUNT(*) FROM @CFFieldsPro)
SELECT @numDomainIDPro=numDomainID,@numUserIDPro=numCreatedBy from ProjectsMaster where numProId=@RecordId

IF @intCntFieldsPro > 0
	BEGIN
		WHILE(@intCntPro < @intCntFieldsPro)
			BEGIN
			--print 'sahc'
				SET @intCntPro = @intCntPro + 1
				SELECT @numFieldIDPro = fld_id FROM @CFFieldsPro WHERE RowID = @intCntPro
				 --Added By :Sachin Sadhu ||Date:14thOct2014
				 --Purpose  :To manage Custom Fields Queue
							EXEC USP_CFW_FLD_Values_Pro_CT
									@numDomainID =@numDomainIDPro,
									@numUserCntID =@numUserIDPro,
									@numRecordID =@RecordId ,
									@numFormFieldId  =@numFieldIDPro
			END 

	END      
          
end

--Added by Neelam Kapila || 12/01/2017 - Added condition to save custom items to CFW_Fld_Values_OppItems table
IF @PageId = 18        
BEGIN      
	INSERT INTO CFW_Fld_Values_OppItems 
	(
		Fld_ID
		,Fld_Value
		,RecId
	)          
	SELECT 
		X.fld_id
		,X.Value
		,@RecordId  
	FROM 
	(
		SELECT 
			* 
		FROM 
			OPENXML (@hDoc,'/NewDataSet/Table',2)          
		WITH 
			(fld_id NUMERIC(9), FldDTLID NUMERIC(9), Value VARCHAR(1000))
	)X 
	WHERE
		X.fld_id NOT IN (SELECT fld_id FROM CFW_Fld_Values_OppItems WHERE RecId = @RecordId)

	UPDATE 
		 CFVOppItems
	SET 
		CFVOppItems.Fld_Value=X.Value  
	FROM
		CFW_Fld_Values_OppItems CFVOppItems
	INNER JOIN 
	(
		SELECT 
			* 
		FROM 
			OPENXML (@hDoc,'/NewDataSet/Table',2)          
		WITH 
			(fld_id NUMERIC(9), FldDTLID NUMERIC(9), Value VARCHAR(1000))
	) X
	ON
		CFVOppItems.fld_id = X.fld_id
	WHERE 
		CFVOppItems.RecId = @RecordId
END 
 
       
EXEC sp_xml_removedocument @hDoc
END
GO
