/****** Object:  StoredProcedure [dbo].[usp_GetOpportunityStageComments]    Script Date: 07/26/2008 16:18:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--------------------------------------
--usp_GetOpportunityStageComments


--This is for getting the stage details associated with the opportunity
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getopportunitystagecomments')
DROP PROCEDURE usp_getopportunitystagecomments
GO
CREATE PROCEDURE [dbo].[usp_GetOpportunityStageComments]
	@numOppId numeric   
--
AS
SELECT * FROM OpptStageComments WHERE numOppId=@numOppId
GO
