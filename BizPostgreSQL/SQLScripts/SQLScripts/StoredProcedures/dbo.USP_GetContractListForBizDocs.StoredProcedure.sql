/****** Object:  StoredProcedure [dbo].[USP_GetContractListForBizDocs]    Script Date: 07/26/2008 16:16:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getcontractlistforbizdocs')
DROP PROCEDURE usp_getcontractlistforbizdocs
GO
CREATE PROCEDURE [dbo].[USP_GetContractListForBizDocs]    
@numOppId numeric (9) =0,     
@numUserCntID numeric(9)=0,                                                          
 @numDomainID numeric(9)=0    
    
as    
    
Begin
Declare @numDivisionId as numeric(9)
Set @numDivisionId=0
Select @numDivisionId=numDivisionId From OpportunityMaster Where numOppId=@numOppId  And @numDomainID=numDomainId

Select numcontractId,vcContractName from ContractManagement c    
where   
@numUserCntID = numUserCntId   
and   
c.numDomainId =@numDomainId and numDivisionId= @numDivisionId  
End
GO
