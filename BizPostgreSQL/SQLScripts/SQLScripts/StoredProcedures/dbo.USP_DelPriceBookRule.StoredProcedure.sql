/****** Object:  StoredProcedure [dbo].[USP_DelPriceBookRule]    Script Date: 07/26/2008 16:15:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_delpricebookrule')
DROP PROCEDURE usp_delpricebookrule
GO
CREATE PROCEDURE [dbo].[USP_DelPriceBookRule]
@numPriceRuleID as numeric(9)=0
AS

DELETE FROM [PricingTable] WHERE [numPriceRuleID]=@numPriceRuleID
DELETE FROM PriceBookRuleItems WHERE numRuleID = @numPriceRuleID
delete from PriceBookRuleDTL where numRuleID=@numPriceRuleID
delete from PriceBookRules where numPricRuleID=@numPriceRuleID


GO
