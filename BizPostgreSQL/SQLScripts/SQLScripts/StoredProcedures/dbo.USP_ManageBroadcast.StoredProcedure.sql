/****** Object:  StoredProcedure [dbo].[USP_ManageBroadcast]    Script Date: 07/26/2008 16:19:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj  
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_managebroadcast')
DROP PROCEDURE usp_managebroadcast
GO
CREATE PROCEDURE [dbo].[USP_ManageBroadcast]
@numBroadcastId as numeric(9)=0 ,   
@vcBroadCastName as varchar(500),    
@numEmailTemplateID as numeric(9),    
@vcSubject as varchar(500),    
@txtMessage as text ,  
@txtBizMessage as text,  
@numTotalRecipients as numeric(9),    
@numTotalSussessfull as numeric(9)=0,    
@numUserCntID as numeric(9),    
@numDomainID as numeric(9),    
@strBroadCast as text,
@bitBroadcasted as bit,
@numSearchID as numeric(9),
@numConfigurationId NUMERIC(18)          
as    

IF EXISTS(SELECT * FROM [dbo].[Broadcast] WHERE [numBroadcastId] = @numBroadcastId)
BEGIN
   update Broadcast    
         set vcBroadCastName = @vcBroadCastName    ,    
             numEmailTemplateID = @numEmailTemplateID,    
             vcSubject = @vcSubject,    
             txtMessage = @txtMessage, 
             txtBizMessage = @txtBizMessage,   
             numTotalRecipients = @numTotalRecipients,    
             numTotalSussessfull = @numTotalSussessfull,    
             bintBroadCastDate = getutcdate(),    
             numBroadCastBy = @numUserCntID,    
             numDomainID = @numDomainID,
             bitBroadcasted = @bitBroadcasted,
             numSearchID = @numSearchID ,
             numConfigurationId = @numConfigurationId   
         where 
             numBroadcastId = @numBroadcastId 
    IF @bitBroadcasted = 1
    BEGIN
    
       update BroadCastDTLs set tintSucessfull = 1 where numContactID in 
       (select BCD.numContactID 
               from 
            BroadCast BC left join BroadCastDTLs BCD on BC.numBroadCastId = BCD.numBroadcastId  
                         left join AdditionalContactsInformation AI on BCD.numContactID = AI.numContactID
            where BC.numBroadcastId = @numBroadcastId and isnull(AI.vcEmail,'') <> ''  ) and numBroadcastID = @numBroadcastID
    END           
             select @numBroadcastId   
             
END
ELSE
BEGIN

         insert into Broadcast    
              (vcBroadCastName,    
               numEmailTemplateID,    
               vcSubject,    
               txtMessage, 
               txtBizMessage ,   
               numTotalRecipients,    
               numTotalSussessfull,    
               bintBroadCastDate,    
               numBroadCastBy,    
               numDomainID,
               bitBroadcasted ,
               numSearchID   ,
               numConfigurationId 
               )    
         values(    
               @vcBroadCastName,    
               @numEmailTemplateID,    
               @vcSubject,    
               @txtMessage, 
               @txtBizMessage ,   
               @numTotalRecipients,    
               @numTotalSussessfull,    
               getutcdate(),    
               @numUserCntID,    
               @numDomainID,
               @bitBroadcasted ,
               @numSearchID  ,
               @numConfigurationId  
               )    
              select SCOPE_IDENTITY()
END 

GO


--exec USP_ManageBroadcast @numBroadcastId=567,@vcBroadCastName='Customer Reminder',@numEmailTemplateID=6040,@vcSubject='New Promotional Offer',@txtMessage='a',@txtBizMessage='a',@numTotalRecipients=5,@numTotalSussessfull=5,@numUserCntID=1,@numDomainID=1,@strBroadCast=NULL,@bitBroadcasted=1,@numSearchID=0
--select * from BroadCastDTLs where numBroadcastID = 567
--update BroadCastDTLs set tintSucessfull = 0 where numBroadcastID = 567