--exec USP_UpdateInventoryRemarket 104,'F:\Remarket\239200810774remarket.del',8997      
--exec USP_UpdateInventoryRemarket 1,'c:\remarket.del',1      
--created by anoop jayaraj        
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_updateinventoryremarket')
DROP PROCEDURE usp_updateinventoryremarket
GO
CREATE PROCEDURE [dbo].[USP_UpdateInventoryRemarket]        
@numDomainID as numeric(9),        
@FilePath as varchar(500),        
@numUserCntID as numeric(9)        
as   
         
        
truncate table remarket        
truncate table remarketDTL       
declare @varSQL as varchar(500)        
        
set @varSQL='BULK INSERT  Remarket FROM '''+@FilePath+''' WITH (FIELDTERMINATOR = ''~'')'        
exec (@varSQL )       
        
        
insert into  Item (vcSKU,vcModelID,vcItemName, txtItemDesc, charItemType, monListPrice, --dtDateEntered, 
numDomainID, numCreatedBy, bintCreatedDate, bintModifiedDate, numModifiedBy,numCOGsChartAcntId,
numAssetChartAcntId,numIncomeChartAcntId)        
select CLASSNUM,COURSECODE,COURSETITLE,convert(varchar(1000),DESCR),'P',PRICE,
@numDomainID,@numUserCntID,getdate(),getdate(),@numUserCntID,957,994,932 from remarket        
where CLASSNUM not in (select vcSKU from Item where numDomainID=@numDomainID)        
        
update Tab1 set vcItemName=COURSETITLE,         
txtItemDesc=DESCR,         
monListPrice=PRICE,         
bintModifiedDate=getdate(),         
numModifiedBy=@numUserCntID        
from Item Tab1        
join (select CLASSNUM as CLASSNUM,COURSETITLE,convert(varchar(1000),DESCR) as DESCR,PRICE from remarket) Tab2        
on Tab1.vcSKU=Tab2.CLASSNUM     
where numDomainID=@numDomainID       
      
      
insert into Warehouses (vcWareHouse,vcWCity,numWState,numDomainID,vcWStreet,numWCountry,vcWPinCode)      
select distinct(FACNAME),CITY,isnull((select top 1 numStateID from [STATE] where vcState=R.STATE and numDomainID=@numDomainID),0),@numDomainID,'',0,''      
from remarket R where FACNAME not in (select vcWareHouse from Warehouses where numDomainID=@numDomainID)      
      
update Tab1 set vcWCity=CITY,         
numWState=numStateID,      
numWCountry=11708      
from Warehouses Tab1        
join (select distinct(FACNAME),CITY,isnull((select top 1 numStateID from [STATE] where vcState=R.STATE and numDomainID=@numDomainID),0) as numStateID       
from remarket R) Tab2        
on Tab1.vcWareHouse=Tab2.FACNAME        
where numDomainID=@numDomainID      
      
      
insert  into RemarketDTL(vcClassNum)      
select CLASSNUM from Remarket      
      
      
declare @numClassID as numeric      
declare @vcClassNum as varchar(100)      
--declare @ModelID as varchar(100)      
declare @Warehouse as varchar(200)      
declare @numWareHouseID as numeric(9)      
declare @numWareHouseItemID as numeric(9)      
declare @numItemID as numeric(9)      
declare @Price as DECIMAL(20,5)      
declare @Chapter as numeric      
declare @SubChapter as numeric      
declare @ChapterItemID as numeric    
declare @ClassStatusItemID as numeric   
declare @SubChapterItemID as numeric      
declare @vcChapter as varchar(100)      
declare @vcSubChapter as varchar(100) 
declare @vcClassStatus as varchar(100)   
declare @numCategoryID as numeric(9)     
declare @numSubCategoryID as numeric(9) 
declare @dtStartDate as datetime
declare @dtEndDate as datetime
declare @ClassStatusID as numeric

     
      
declare @CusChapterItemID as numeric      
declare @CusSubChapterItemID as numeric  
declare @CusStartDate as numeric      
declare @CusEndDate as numeric 
declare @CusClassNum as numeric
declare @CusClassStatus as numeric       

    
      
select @CusChapterItemID=Fld_ID from CFW_Fld_Master where Fld_label='Chapter' and numDomainID=@numDomainID      
select @CusSubChapterItemID=Fld_ID from CFW_Fld_Master where Fld_label='SubChapter' and numDomainID=@numDomainID  
select @CusStartDate=Fld_ID from CFW_Fld_Master where Fld_label='Start Date' and numDomainID=@numDomainID 
select @CusEndDate=Fld_ID from CFW_Fld_Master where Fld_label='End Date' and numDomainID=@numDomainID 
select @CusClassNum=Fld_ID from CFW_Fld_Master where Fld_label='ClassNum' and numDomainID=@numDomainID 
select @CusClassStatus=Fld_ID from CFW_Fld_Master where Fld_label='Class Status' and numDomainID=@numDomainID    
      
      
select @Chapter=numListID from ListMaster where vcListName='Chapter' and numDomainID=@numDomainID      
      
select @SubChapter=numListID from ListMaster where vcListName='SubChapter' and numDomainID=@numDomainID   

select @ClassStatusID=numListID from ListMaster where vcListName='Class Status' and numDomainID=@numDomainID   
      
      
      
      
if @Chapter>0       
begin      
   select distinct(Chapter) into #Temp2 from remarket      
  insert into ListDetails (vcData,numListID, numCreatedBY, bintCreatedDate, numModifiedBy, bintModifiedDate,numDomainID, constFlag, sintOrder)     
  select convert(varchar(50),Chapter),@Chapter,@numUserCntID,getdate(),@numUserCntID,getdate(),@numDomainID,0,0      
  from #Temp2 where convert(varchar(50),Chapter) not in (select vcData from Listdetails where numDomainID=@numDomainID and numListID=@Chapter)      
      
  drop table #Temp2      
end      
if @SubChapter>0       
begin      
      
  select distinct(SubChapter) into #Temp1 from remarket      
         
  insert into ListDetails (vcData,numListID, numCreatedBY, bintCreatedDate, numModifiedBy, bintModifiedDate,numDomainID, constFlag, sintOrder)      
  select convert(varchar(50),SubChapter),@SubChapter,@numUserCntID,getdate(),@numUserCntID,getdate(),@numDomainID,0,0      
  from #Temp1 where convert(varchar(50),SubChapter) not in (select vcData from Listdetails where numDomainID=@numDomainID and numListID=@SubChapter)      
      
  drop table #Temp1      
end      
      
set @numClassID=0      
select top 1 @numClassID=numClassID,@vcClassNum=vcClassNum from RemarketDTL       
while @numClassID>0      
begin      
         
          
    select  @Warehouse= FACNAME,@Price=Price,@vcChapter=Chapter,@vcSubChapter=SubChapter,@vcClassStatus=CLASSAT,@dtStartDate=StartDate,@dtEndDate=EndDate 
	from  Remarket where CLASSNUM=@vcClassNum      
        
    select  @numItemID=numItemCode from Item where vcSKU=@vcClassNum and numDomainID=@numDomainID      
      
    delete from CFW_FLD_Values_Item where RecId=@numItemID and Fld_ID in (@CusChapterItemID,@CusSubChapterItemID,@CusStartDate,@CusEndDate,@CusClassNum,@CusClassStatus)      
      
    select top 1 @ChapterItemID=numListItemID from ListDetails where vcdata=@vcChapter and numDomainID=@numDomainID and numListID=@Chapter      
    select top 1 @SubChapterItemID=numListItemID from ListDetails where vcdata=@vcSubChapter and numDomainID=@numDomainID and numListID=@SubChapter      
    select top 1 @ClassStatusItemID=numListItemID from ListDetails where vcdata=@vcClassStatus and numDomainID=@numDomainID and numListID=@ClassStatusID
	insert into CFW_FLD_Values_Item values(@CusChapterItemID,@ChapterItemID,@numItemID)      
    insert into CFW_FLD_Values_Item values(@CusSubChapterItemID,@SubChapterItemID,@numItemID) 
	insert into CFW_FLD_Values_Item values(@CusStartDate,@dtStartDate,@numItemID) 
	insert into CFW_FLD_Values_Item values(@CusEndDate,@dtEndDate,@numItemID) 
	insert into CFW_FLD_Values_Item values(@CusClassNum,@vcClassNum,@numItemID)   
	insert into CFW_FLD_Values_Item values(@CusClassStatus,@ClassStatusItemID,@numItemID) 
        
 set @numSubCategoryID=0    
    select @numSubCategoryID=numCategoryID from Category where vcCategoryName=convert(varchar(50),@vcSubChapter) and numDomainID=@numDomainID    
    set @numCategoryID=0    
    select @numCategoryID=numCategoryID from Category where vcCategoryName=@vcChapter and numDomainID=@numDomainID    
    if @vcSubChapter<>'No Value'    
    begin    
            
        if not exists(select * from ItemCategory where numItemID=@numItemID and numCategoryID=@numSubCategoryID)    
        insert into ItemCategory values(@numItemID,@numSubCategoryID)    
    
 end    
 else    
    begin    
  if not exists(select * from ItemCategory where numItemID=@numItemID and numCategoryID=@numCategoryID)    
        insert into ItemCategory values(@numItemID,@numCategoryID)    
    end    
        
    
--    if @numCategoryID>0    
--    begin    
--  if not exists(select * from ItemCategory where numItemID=@numItemID and numCategoryID=@numCategoryID)    
--        insert into ItemCategory values(@numItemID,@numCategoryID)    
--    
-- end    
       
    
    
    
    select @numWareHouseID=numWareHouseID from Warehouses where vcWareHouse=@Warehouse and numDomainID=@numDomainID      
      
   if exists(select * from WareHouseItems where numItemID=@numItemID and numWareHouseID=@numWareHouseID)      
   begin      
      select @numWareHouseItemID=numWareHouseItemID from WareHouseItems where numItemID=@numItemID and numWareHouseID=@numWareHouseID      
      update WareHouseItems set   monWListPrice=@Price,dtModified = GETDATE() where numWareHouseItemID=@numWareHouseItemID      
   end      
   else      
   begin      
     insert into WareHouseItems (numItemID, numWareHouseID, monWListPrice,dtModified)        
      values (@numItemID,@numWareHouseID,@Price,GETDATE())      
     set @numWareHouseItemID=@@identity      
   end      
    update RemarketDTL set numItemID=@numItemID,numWareHouseItemID=@numWareHouseItemID where numClassID=@numClassID      
 select top 1 @numClassID=numClassID,@vcClassNum=vcClassNum from RemarketDTL       
 where numClassID>@numClassID      
 if @@rowcount=0 set @numClassID=0      
end 