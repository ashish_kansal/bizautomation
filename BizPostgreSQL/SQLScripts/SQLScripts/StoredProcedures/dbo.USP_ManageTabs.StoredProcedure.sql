--SELECT * FROM [GroupTabDetails]
--SELECT * FROM [TabMaster]
--SELECT * FROM AuthenticationGroupMaster
--created by anoop jayaraj      
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_managetabs')
DROP PROCEDURE usp_managetabs
GO
CREATE PROCEDURE [dbo].[USP_ManageTabs]        
@numGroupID as numeric(9)=0,        
@strTabs as text='',    
@numRelationships as numeric(9)=0,
@numProfileID as numeric(9)=0,
@tintType TINYINT=0,-- if 1 then tabId is subtabid e.g. [CFw_Grp_Master].Grp_id
@Loc_Id NUMERIC(9)=0-- will be passed when subtab
as        

IF @tintType = 1 --subtab
BEGIN
	DELETE FROM GroupTabDetails
	WHERE       [numTabDetailID] IN (SELECT [numTabDetailID]
									 FROM   [GroupTabDetails] GTD
											INNER JOIN [CFw_Grp_Master] G
											  ON GTD.[numTabId] = G.[Grp_id]
									 WHERE  GTD.[numGroupId] = @numGroupID
											AND GTD.tintType = @tintType
											AND G.[Loc_Id] = @Loc_Id)
END
ELSE -- main tab
BEGIN
	declare @tintGroupType tinyint
	select  @tintGroupType=tintGroupType from AuthenticationGroupMaster where numGroupID=@numGroupID
	if @tintGroupType=2 
		delete from GroupTabDetails where numGroupID=@numGroupID  and ISNULL(numRelationship,0)=@numRelationships AND ISNULL([numProfileID],0)=@numProfileID AND ISNULL([tintType],0) <> 1
	else  
		delete from GroupTabDetails where numGroupID=@numGroupID 
		AND ISNULL([tintType],0) <> 1
END


declare @hDoc  int        
        
EXEC sp_xml_preparedocument @hDoc OUTPUT, @strTabs        
        
 


insert into GroupTabDetails        
  (numGroupId,numTabId,bitallowed,numOrder,numRelationShip,tintType,[numProfileID])
         
 select @numGroupID,
 X.*,@numRelationships,@tintType,@numProfileID  from(SELECT * FROM OPENXML (@hDoc,'/NewDataSet/Table',2)
 WITH  (
  TabId numeric(9),      
  bitallowed tinyint,
  numOrder NUMERIC
  ))X
        
        
        
 EXEC sp_xml_removedocument @hDoc
