/****** Object:  StoredProcedure [dbo].[USP_ShoppingcartHDRDtls]    Script Date: 07/26/2008 16:21:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj    
    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_shoppingcarthdrdtls')
DROP PROCEDURE usp_shoppingcarthdrdtls
GO
CREATE PROCEDURE [dbo].[USP_ShoppingcartHDRDtls]    
@numItemCode as numeric(9)=0,    
@byteMode as tinyint=0 ,
@txtDesc   as NTEXT,
@numUserCntID NUMERIC,
@txtShortDesc NTEXT
as    
      
if @byteMode=0    
begin    
 SELECT ISNULL(txtDesc,'') txtDesc from ItemExtendedDetails where numItemCode= @numItemCode   
end
else if  @byteMode=1 
begin

 if not exists (select * from ItemExtendedDetails  where numItemCode=@numItemCode)
	insert into ItemExtendedDetails 
	values
	(@numItemCode,@txtDesc,@txtShortDesc)
 else
		update ItemExtendedDetails set txtDesc=@txtDesc,txtShortDesc=@txtShortDesc where numItemCode=@numItemCode
		
UPDATE item SET bintModifiedDate = GETUTCDATE(),numModifiedBy= @numUserCntID WHERE numItemCode=@numItemCode
end

else if @byteMode=2
BEGIN
	SELECT ISNULL(txtDesc,'') txtDesc,ISNULL(txtShortDesc,'') txtShortDesc  from ItemExtendedDetails where numItemCode= @numItemCode   
END
GO
