/****** Object:  StoredProcedure [dbo].[USP_UpdateSubscription]    Script Date: 07/26/2008 16:21:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_updatesubscription')
DROP PROCEDURE usp_updatesubscription
GO
CREATE PROCEDURE [dbo].[USP_UpdateSubscription]
@UserId numeric,
@strSubscriptionId varchar(500)
as 
update UserMaster set SubscriptionId = @strSubscriptionId where numUserId=@UserId
GO
