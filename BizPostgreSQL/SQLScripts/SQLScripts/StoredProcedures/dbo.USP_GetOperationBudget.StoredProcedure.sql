/****** Object:  StoredProcedure [dbo].[USP_GetOperationBudget]    Script Date: 07/26/2008 16:17:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Create By Siva          
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getoperationbudget')
DROP PROCEDURE usp_getoperationbudget
GO
CREATE PROCEDURE [dbo].[USP_GetOperationBudget]          
(@numDomainId as numeric(9)=0,
@tintType as int=0
)          
As          
Begin          
	Declare @numMonth as tinyint          
	Declare @str as varchar(10)          
    Declare @lstrSQL as varchar(800)          
    Declare @vc as varchar(10)          
    Declare @len as numeric(9)          
    Set @lstrSQL ='Select  ' + ''''' as numChartAcntId,'+  ''''' as numParentAcntId, '+''''' as vcCategoryName,'+  ''''' as TOC,'           
    Set @numMonth=1    
    Declare @Date as datetime
	set @Date = dateadd(year,@tintType,getutcdate())     
    While @numMonth <=12          
		Begin          
       
			Declare @dtFiscalStDate as datetime          
			Declare @dtFiscalEndDate as datetime     
			Set @dtFiscalStDate =dbo.GetFiscalStartDate(dbo.GetFiscalyear(@Date,@numDomainId),@numDomainId)       
			set @dtFiscalStDate=dateadd(month,@numMonth-1,@dtFiscalStDate)    
			set @dtFiscalEndDate =  dateadd(day,-1,dateadd(year,1,dbo.GetFiscalStartDate(dbo.GetFiscalyear(@Date,@numDomainId),@numDomainId)))      
          
         
			Set @lstrSQL=@lstrSQL+' '+  '''0''' + ' as ''' +Convert(varchar(2),month(@dtFiscalStDate))+ '~'+Convert(varchar(4),year(@dtFiscalStDate))+''','          
			Set @numMonth=@numMonth+1          
		End          
             
	Set @lstrSQL = @lstrSQL +  '''''' + ' as Total' + ',''''' + ' as  Comments'           
    Print @lstrSQL          
	Exec (@lstrSQL)          
End
GO
