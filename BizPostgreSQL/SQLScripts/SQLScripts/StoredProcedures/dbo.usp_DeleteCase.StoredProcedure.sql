/****** Object:  StoredProcedure [dbo].[usp_DeleteCase]    Script Date: 07/26/2008 16:15:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--modified by anoop jayaraj      
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_deletecase')
DROP PROCEDURE usp_deletecase
GO
CREATE PROCEDURE [dbo].[usp_DeleteCase]          
 @numCaseID numeric(9)=0 ,  
 @numDomainID numeric(9)          
 AS             
if exists(select count(*) from cases where numCaseId=@numCaseID and numDomainID=@numDomainID)   
begin  
 DELETE FROM dbo.Correspondence WHERE numOpenRecordID=@numCaseID AND tintCorrType=6 
 DELETE FROM [CaseOpportunities] WHERE [numCaseId]=@numCaseID AND [numDomainId]=@numDomainID	
 delete recentItems where numRecordID = @numCaseID and chrRecordType = 'S'
 delete from TimeAndExpense where  numCaseId=  @numCaseID and numDomainID=   @numDomainID  
 delete from CaseContacts where numCaseID=@numCaseID      
 delete from CaseSolutions where numCaseID=@numCaseID
 delete  from Comments where numCaseId=@numCaseID        
 DELETE from Cases where numCaseId=@numCaseID                
END
GO
