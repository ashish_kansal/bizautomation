/****** Object:  StoredProcedure [dbo].[usp_UpdateMyLeadBox]    Script Date: 07/26/2008 16:21:44 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_updatemyleadbox')
DROP PROCEDURE usp_updatemyleadbox
GO
CREATE PROCEDURE [dbo].[usp_UpdateMyLeadBox]

	--Input Parameter List

	@numUserID numeric=0,
	@bitWebSite bit=0,
	@bitPosition bit=0,
	@bitNotes bit=0,
	@bitAnnualRevenue bit=0,
	@bitNumEmployees bit=0,
	@bitHowHeard bit=0,
	@bitProfile bit=0,
	@vcRedirectURL varchar(100)=' ',
	@vcInterested1 varchar(100)=' ',
	@vcInterested2 varchar(100)=' ',
	@vcInterested3 varchar(100)=' ',
	@vcInterested4 varchar(100)=' ',
	@vcInterested5 varchar(100)=' ',
	@vcInterested6 varchar(100)=' ',
	@vcInterested7 varchar(100)=' ',
	@vcInterested8 varchar(100)=' '   
--
AS
	--Updating the field in LeadBox Table

	update LeadBox set vcInterested1=@vcInterested1,vcInterested2=@vcInterested2,vcInterested3=@vcInterested3,vcInterested4=@vcInterested4,vcInterested5=@vcInterested5,vcInterested6=@vcInterested6,vcInterested7=@vcInterested7,vcInterested8=@vcInterested8,bitWebSite=@bitWebSite, bitPosition=@bitPosition,bitNotes=@bitNotes,bitAnnualRevenue=@bitAnnualRevenue,bitNumEmployees=@bitNumEmployees,bitHowHeard=@bitHowHeard,bitProfile=@bitProfile,vcRedirectURL=@vcRedirectURL  where numUserID=@numUserID
GO
