/****** Object:  StoredProcedure [dbo].[usp_InsertStageOpport]    Script Date: 07/26/2008 16:19:17 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_insertstageopport')
DROP PROCEDURE usp_insertstageopport
GO
CREATE PROCEDURE [dbo].[usp_InsertStageOpport]
	@numOppID  numeric(9)=0,
	@numstageID numeric(9)=0,
	@slp_id  integer=0
	

AS
	--Insertion into Table  
             --delete from stageOpportunity where numOppID=@numOppID
	INSERT INTO stageOpportunity(numOppID,numstageID,slp_id)
	VALUES(@numOppID,@numstageID,@slp_id)
GO
