/****** Object:  StoredProcedure [dbo].[USP_GetEmailtemplate]    Script Date: 07/26/2008 16:17:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getemailtemplate')
DROP PROCEDURE usp_getemailtemplate
GO
CREATE PROCEDURE [dbo].[USP_GetEmailtemplate]      
(   
@numDomainID numeric(9)=0,
@numModuleID NUMERIC=0  
)  
As      
select VcDocName , numGenericDocID from GenericDocuments    
where  numDocCategory = 369       
 And  
 numDomainId = @numDomainID
 AND (numModuleID = @numModuleID OR @numModuleID=0)
GO
