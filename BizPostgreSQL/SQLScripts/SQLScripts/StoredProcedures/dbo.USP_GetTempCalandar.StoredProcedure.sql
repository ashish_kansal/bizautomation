/****** Object:  StoredProcedure [dbo].[USP_GetTempCalandar]    Script Date: 07/26/2008 16:18:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_gettempcalandar')
DROP PROCEDURE usp_gettempcalandar
GO
CREATE PROCEDURE [dbo].[USP_GetTempCalandar]                                    
 @StartDateTimeUtc datetime,
@itemId varchar(250)
AS   
select * from tmpCalandarTbl where StartDateTimeUtc= @StartDateTimeUtc and itemId= @itemId
GO
