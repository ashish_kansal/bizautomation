/****** Object:  StoredProcedure [dbo].[USP_TerritoryName]    Script Date: 07/26/2008 16:21:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_territoryname')
DROP PROCEDURE usp_territoryname
GO
CREATE PROCEDURE [dbo].[USP_TerritoryName]
@numTerritoryID as numeric(9)=0
as

select vcTerName from TerritoryMaster where numTerID=@numTerritoryID
GO
