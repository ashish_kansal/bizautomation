/****** Object:  StoredProcedure [dbo].[USP_ManageExtranetAccounts]    Script Date: 07/26/2008 16:19:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_manageextranetaccounts')
DROP PROCEDURE usp_manageextranetaccounts
GO
CREATE PROCEDURE [dbo].[USP_ManageExtranetAccounts]
@numExtranetID as numeric(9)=null,      
@numGroupID as numeric(9)=null,      
@strEmail as text='',  
--@numPartnerGroup as numeric(9)=0,
@numDomainID as numeric(9)=0   
as      
      
update  ExtarnetAccounts set       
numGroupID=@numGroupID
--numPartnerGroupID=@numPartnerGroup      
where numExtranetID=@numExtranetID      
          
declare @hDoc int      
EXEC sp_xml_preparedocument @hDoc OUTPUT, @strEmail      
      
 insert into ExtranetAccountsDtl      
  (numExtranetID,numContactID,tintAccessAllowed,vcPassword,numDomainID)      
       
 select @numExtranetID,      
  X.*,@numDomainID  from(SELECT ContactID,Portal,[Password] FROM OPENXML (@hDoc,'/NewDataSet/Table[ExtranetDTLID=0]',2)      
 WITH  (      
  ContactID numeric(9),      
   Portal tinyint,      
  [Password] varchar(100),
  ExtranetDTLID numeric(9)      
  ))X 



  Update ExtranetAccountsDtl               
   set tintAccessAllowed=X.Portal,              
--   bitPartnerAccess=X.Partner,              
   vcPassword=X.[Password],              
   numDomainID=@numDomainID                          
  from (SELECT * FROM OPENXML (@hDoc,'/NewDataSet/Table[ExtranetDTLID>0]',2)                                                              
      
        
   WITH  (              
    ContactID numeric(9),      
   Portal tinyint,      
--   [Partner] tinyint,     
  [Password] varchar(100),
  ExtranetDTLID numeric(9)                                              
   ))X where numExtranetDtlID=X.ExtranetDTLID        
      
      
      
 EXEC sp_xml_removedocument @hDoc
GO
