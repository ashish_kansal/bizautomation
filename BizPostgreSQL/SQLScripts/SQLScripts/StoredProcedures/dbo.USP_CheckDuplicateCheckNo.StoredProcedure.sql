/****** Object:  StoredProcedure [dbo].[USP_CheckDuplicateCheckNo]    Script Date: 07/26/2008 16:15:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Create By Siva
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_checkduplicatecheckno')
DROP PROCEDURE usp_checkduplicatecheckno
GO
CREATE PROCEDURE [dbo].[USP_CheckDuplicateCheckNo]
(@numCheckId as numeric(9)=0,        
@numCheckNo as numeric(9)=0,   
@numDomainId as numeric(9)=0) 
As
Begin
   Select Count(*) From CheckDetails Where numCheckNo=@numCheckNo And numCheckId<>@numCheckId And numDomainId=@numDomainId  
End
GO
