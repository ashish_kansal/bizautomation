/****** Object:  StoredProcedure [dbo].[usp_GetRecentItems]    Script Date: 07/26/2008 16:18:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj  
--EXEC usp_GetRecentItems 1 
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getrecentitems')
DROP PROCEDURE usp_getrecentitems
GO
CREATE PROCEDURE [dbo].[usp_GetRecentItems]
(   
	@numUserCntID as numeric(9)      
)
AS
BEGIN
	IF @numUserCntID = 0
	BEGIN
		--get blank table to avoid error
		select 0 numRecId 
		from communication    
		where  1=0
		RETURN
	END
	

	declare @v1 int
	if exists(select  ISNULL([intLastViewedRecord],10) FROM [Domain] WHERE [numDomainId] IN (SELECT numDomainID FROM [AdditionalContactsInformation] WHERE [numContactId]=@numUserCntID))
		begin
			select @v1 = ISNULL([intLastViewedRecord],10) FROM [Domain] WHERE [numDomainId] IN (SELECT numDomainID FROM [AdditionalContactsInformation] WHERE [numContactId]=@numUserCntID)
		end
	else
		begin
			set @v1=10
		end

	DECLARE @TEMPTABLE TABLE
	(
		numRecordId numeric(18,0),
		bintvisiteddate DATETIME,
		numRecentItemsID numeric(18,0),
		chrRecordType char(10)
	)

	INSERT INTO 
		@TEMPTABLE
	SELECT DISTINCT TOP (@v1)
		numrecordId,
		bintvisiteddate,
		numRecentItemsID,
		chrRecordType	
	FROM    
		RECENTITEMS
	WHERE   
		numUserCntId = @numUserCntID
		AND bitdeleted = 0
	ORDER BY 
		bintvisiteddate DESC

	SELECT  DM.numDivisionID AS numRecID ,isnull(CMP.vcCompanyName,'' ) as RecName,                           
		 DM.tintCRMType,                             
		 CMP.numCompanyID AS numCompanyID,                                           
		 ADC.numContactID AS numContactID,          
		 DM.numDivisionID,'C'   as Type,bintvisiteddate  ,0 as caseId,0 as caseTimeId,0 as caseExpId,
		 0 as numOppID,
		'~/images/Icon/organization.png' AS vcImageURL
		FROM  CompanyInfo CMP                            
		join DivisionMaster DM on DM.numCompanyID=CMP.numCompanyID                  
		LEFT join AdditionalContactsInformation ADC on ADC.numDivisionID=DM.numDivisionID AND ISNULL(ADC.bitPrimaryContact,0)=1         
		join @TEMPTABLE R on R.numRecordID=DM.numDivisionID                                             
		WHERE chrRecordType='C'    
	union     
         
	SELECT  DM.numDivisionID AS numRecID,    
	isnull(ISNULL(vcFirstName,'-') + ' '+ISNULL(vcLastName,'-'),'') as RecName,                     
		 DM.tintCRMType,                             
		 CMP.numCompanyID AS numCompanyID,                                           
		 ADC.numContactID AS numContactID,          
		 DM.numDivisionID,'U',bintvisiteddate     ,0 as caseId,0 as caseTimeId,0 as caseExpId,
		 0 as numOppID,
		 '~/images/Icon/contact.png' AS vcImageURL                
		FROM  CompanyInfo CMP                            
		join DivisionMaster DM on DM.numCompanyID=CMP.numCompanyID                  
		join AdditionalContactsInformation ADC on ADC.numDivisionID=DM.numDivisionID          
		join @TEMPTABLE R on numRecordID=ADC.numContactID   where chrRecordType='U'                                         
    
    
	union           
    
	select  numOppId as numRecID,isnull(vcPOppName,'') as RecName,          
	0,0,0,numDivisionID,'O',bintvisiteddate  ,0 as caseId,0 as caseTimeId,0 as caseExpId,
	0 as numOppID,
	CASE 
		WHEN tintOppType=1 AND tintOppStatus=0 THEN '~/images/Icon/Sales opportunity.png'
		WHEN tintOppType=1 AND tintOppStatus=2 THEN '~/images/Icon/Sales opportunity.png'
		WHEN tintOppType=1 AND tintOppStatus=1 THEN '~/images/Icon/Sales order.png'
		WHEN tintOppType=2 AND tintOppStatus=0 THEN '~/images/Icon/Purchase opportunity.png'
		WHEN tintOppType=2 AND tintOppStatus=1 THEN '~/images/Icon/Purchase order.png'
	ELSE '~/images/icons/cart.png' 
	END AS vcImageURL from OpportunityMaster          
	join @TEMPTABLE  on numRecordID=numOppId    where  chrRecordType='O'      
    
          
	union       
        
	select  numProId as numRecID,isnull(vcProjectName,'') as RecName,          
	0,0,0,numDivisionID,'P',bintvisiteddate  ,0 as caseId,0 as caseTimeId,0 as caseExpId,0 as numOppID,'~/images/Icon/project.png' AS vcImageURL from ProjectsMaster          
	join @TEMPTABLE  on numRecordID=numProId  where  chrRecordType='P'    
       
    
	union    
           
	select  numCaseId as numRecID,    
	isnull(vcCaseNumber,'')  as RecName,          
	0,0,0,numDivisionID,'S',bintvisiteddate ,0 as caseId,0 as caseTimeId,0 as caseExpId,0 as numOppID,'~/images/Icon/case.png' AS vcImageURL  from Cases          
	join @TEMPTABLE  on numRecordID=numCaseId   where  chrRecordType='S'      
    
	union    
    
	select numCommId as numRecId,    
	isnull(    
	case when bitTask =971 then 'C - '    
	  when bitTask =972 then 'T - '    
	  when bitTask =973 then 'N - '    
	  when bitTask =974 then 'F - '   
	 else convert(varchar(2),dbo.fn_GetListItemName(bitTask))  
	end + CONVERT(VARCHAR(100),textDetails),'')    
	as RecName,0,0,0,numDivisionID,'A',bintvisiteddate     
	 , caseId, caseTimeId, caseExpId,0 as numOppID,'~/images/Icon/action.png' AS vcImageURL  
	from communication    
	join @TEMPTABLE  on numRecordID=numCommId   where  chrRecordType='A'    
    
    
		union    
        

	select  numItemCode as numRecID,    
	isnull(vcItemName,'') as RecName, 
	0,0,0,0,'I',bintvisiteddate ,0 as caseId,0 as caseTimeId,0 as caseExpId,0 as numOppID,'~/images/Icon/item.png' AS vcImageURL  from Item          
	join @TEMPTABLE  on numRecordID=numItemCode   where  chrRecordType='I'  

	  union    
           
	select  numItemCode as numRecID,    
	isnull(vcItemName,'') as RecName, 
	0,0,0,0,'AI',bintvisiteddate ,0 as caseId,0 as caseTimeId,0 as caseExpId,0 as numOppID,'~/images/Icon/item.png' AS vcImageURL  from Item          
	join @TEMPTABLE  on numRecordID=numItemCode   where  chrRecordType='AI'  
                                    
	union    
           
	select  numGenericDocId as numRecID,    
	isnull(vcDocName,'')  as RecName,          
	0,0,0,0,'D',bintvisiteddate ,0 as caseId,0 as caseTimeId,0 as caseExpId,0 as numOppID,'~/images/Icon/Doc.png' AS vcImageURL  from GenericDocuments          
	join @TEMPTABLE  on numRecordID=numGenericDocId   where  chrRecordType='D'      

	union 

	select  numCampaignId as numRecID,isnull(vcCampaignName,'') as RecName,          
	0,0,0,0 AS numDivisionID,'M',bintvisiteddate  ,0 as caseId,0 as caseTimeId,0 as caseExpId,0 as numOppID,'~/images/Icon/campaign.png' AS vcImageURL from dbo.CampaignMaster        
	join @TEMPTABLE  on numRecordID=numCampaignId    where  chrRecordType='M'      
    
	union 

	select  numReturnHeaderID as numRecID,isnull(vcRMA,'') as RecName,          
	0,0,0,numDivisionID,'R',bintvisiteddate  ,0 as caseId,0 as caseTimeId,0 as caseExpId,0 as numOppID, CASE 
																											WHEN tintReturnType=1 THEN '~/images/Icon/Sales Return.png'
																											WHEN tintReturnType=2 THEN '~/images/Icon/Purchase Return.png'
																											WHEN tintReturnType=3 THEN '~/images/Icon/CreditMemo.png'
																											WHEN tintReturnType=4 THEN '~/images/Icon/Refund.png'
																										ELSE 
																											'~/images/Icon/ReturnLastView.png' 
																										END AS vcImageURL from ReturnHeader          
	join @TEMPTABLE  on numRecordID=numReturnHeaderID  where  chrRecordType='R'   

	UNION

	SELECT
		numOppBizDocsId as numRecID,
		isnull(vcBizDocID,'') as RecName,
		0,0,0,0 AS numDivisionID,'B',bintvisiteddate,0 as caseId,0 as caseTimeId,0 as caseExpId,OpportunityMaster.numOppId as numOppID,
		(CASE 
			WHEN OpportunityMaster.tintOppType = 1 THEN '~/images/Icon/Sales BizDoc.png' 
			WHEN OpportunityMaster.tintOppType = 2 THEN '~/images/Icon/Purchase BizDoc.png' 
			ELSE 
				'~/images/Icon/Doc.png' 
		END) AS vcImageURL
	FROM
		OpportunityBizDocs
	JOIN
		OpportunityMaster
	ON
		OpportunityBizDocs.numOppId = OpportunityMaster.numOppId
	JOIN
		@TEMPTABLE
	ON
		numOppBizDocsId = numRecordId
	WHERE
		chrRecordType = 'B'
	union 

	SELECT 
		numWOId AS numRecID
		,ISNULL(vcWorkOrderName,'') as RecName
		,0
		,0
		,0
		,0 AS numDivisionID
		,'W'
		,bintvisiteddate
		,0 as caseId
		,0 as caseTimeId
		,0 as caseExpId
		,0 as numOppID
		,'~/images/Icon/WorkOrder.png' AS vcImageURL 
	FROM 
		dbo.WorkOrder        
	JOIN
		@TEMPTABLE 
	ON 
		numRecordID=numWOId
	WHERE 
		chrRecordType='W'  

	order by bintVisitedDate desc     
END
GO