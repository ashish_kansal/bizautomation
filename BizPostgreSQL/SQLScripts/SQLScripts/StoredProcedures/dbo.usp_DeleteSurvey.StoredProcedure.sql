/****** Object:  StoredProcedure [dbo].[usp_DeleteSurvey]    Script Date: 07/26/2008 16:15:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created By: Debasish Tapan Nag                              
--Purpose: Deletes the Survey from the Database    
--Created Date: 09/20/2005    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_deletesurvey')
DROP PROCEDURE usp_deletesurvey
GO
CREATE PROCEDURE [dbo].[usp_DeleteSurvey]      
 @numSurveyID Numeric,    
 @numDomainID Numeric    
--      
AS      
BEGIN      
 --First Delete All Survey Responses related to this Survey.      
 DELETE FROM SurveyResponse       
  WHERE numRespondantID IN       
   (SELECT numRespondantID       
    FROM SurveyRespondentsMaster       
     WHERE numSurID = @numSurveyID)      
    
 --Delete all Survey Resposen for the Current Survey   
 DELETE FROM  SurveyResponse  
  WHERE numSurID = @numSurveyID     
  
 --Then Delete All Survey Respondants related to this Survey.      
 DELETE FROM  SurveyRespondentsChild  
  WHERE numSurID = @numSurveyID    
    
 DELETE FROM  SurveyRespondentsMaster  
  WHERE numSurID = @numSurveyID      
  
 --Deletes the Rules for the Survey Answers    
 DELETE FROM SurveyWorkflowRules    
  WHERE numSurID = @numSurveyID      
    
 --Then Delete All Survey Ans Master related to this Survey.      
 DELETE FROM SurveyAnsMaster       
  WHERE numSurID = @numSurveyID      
    
 --Then Delete All Survey Question Master related to this Survey.      
 DELETE FROM SurveyQuestionMaster       
  WHERE numSurID = @numSurveyID      
 
  --Then Delete The Survey from the Survey Template.      
 DELETE FROM StyleSheetDetails WHERE numSurTemplateId IN (SELECT numSurTemplateId FROM  SurveyTemplate       
  WHERE numSurID = @numSurveyID      
  AND numDomainId = @numDomainID )
     
 --Then Delete The Survey from the Survey Template.      
 DELETE FROM SurveyTemplate       
  WHERE numSurID = @numSurveyID      
  AND numDomainId = @numDomainID   
  
 --Then Delete The Survey from the Survey Master.      
 DELETE FROM SurveyMaster       
  WHERE numSurID = @numSurveyID      
  AND numDomainId = @numDomainID   
  
   
END
GO
