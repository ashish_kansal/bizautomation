/****** Object:  StoredProcedure [dbo].[USP_GetAcivitybyActId]    Script Date: 07/26/2008 16:16:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getacivitybyactid')
DROP PROCEDURE usp_getacivitybyactid
GO
CREATE PROCEDURE [dbo].[USP_GetAcivitybyActId]      
@ActivityId numeric,@mode bit      
as       
      
if @mode = 0  
begin      
 SELECT       
  isnull([Activity].[AllDayEvent],0) as AllDayEvent,       
  [Activity].[ActivityDescription],       
  [Activity].[Duration],       
  [Activity].[Location],       
  [Activity].[ActivityID],       
  [Activity].[StartDateTimeUtc],       
  [Activity].[Subject],       
  [Activity].[EnableReminder],       
  [Activity].[ReminderInterval],      
  [Activity].[ShowTimeAs],      
  [Activity].[Importance],      
  [Activity].[Status],      
  [Activity].[RecurrenceID],      
  [Activity].[VarianceID],      
[ActivityResource].[ResourceID],      
[Activity].[ItemID],    
[Activity].[ChangeKey], 
[Activity].[ItemIDOccur],      
  [Activity].[_ts],Activity.GoogleEventId,ACI.numContactID,Div.numDomainID,isnull(Activity.Color,0) as Color       
 FROM       
  [Activity] INNER JOIN [ActivityResource] ON [Activity].[ActivityID] = [ActivityResource].[ActivityID]
   join resource res on [ActivityResource].resourceid=res.resourceid  JOIN 
  AdditionalContactsInformation ACI on ACI.numContactId = res.numUserCntId JOIN DivisionMaster Div                                                          
 ON ACI.numDivisionId = Div.numDivisionID                        
 WHERE       
  [Activity].[ActivityID] = @ActivityId and  [Activity].[OriginalStartDateTimeUtc] IS NULL ;   
end   
else  
begin  
 SELECT       
  isnull([Activity].[AllDayEvent],0) as AllDayEvent,         
  [Activity].[ActivityDescription],       
  [Activity].[Duration],       
  [Activity].[Location],       
  [Activity].[ActivityID],       
  [Activity].[StartDateTimeUtc],       
  [Activity].[Subject],       
  [Activity].[EnableReminder],       
  [Activity].[ReminderInterval],      
  [Activity].[ShowTimeAs],      
  [Activity].[Importance],      
  [Activity].[Status],      
  [Activity].[RecurrenceID],      
  [Activity].[VarianceID],      
[ActivityResource].[ResourceID],      
[Activity].[ItemID],    
[Activity].[ChangeKey],     
[Activity].[ItemIDOccur],  
  [Activity].[_ts],Activity.GoogleEventId,ACI.numContactID,Div.numDomainID,isnull(Activity.Color,0) as Color,
R.EndDateUtc,R.DayOfWeekMaskUtc,R.UtcOffset,R.DayOfMonth,R.MonthOfYear,R.PeriodMultiple,R.Period
 FROM       
  [Activity] INNER JOIN [ActivityResource] ON [Activity].[ActivityID] = [ActivityResource].[ActivityID] 
  join resource res on [ActivityResource].resourceid=res.resourceid  
  JOIN AdditionalContactsInformation ACI on ACI.numContactId = res.numUserCntId 
  JOIN DivisionMaster Div ON ACI.numDivisionId = Div.numDivisionID
  left outer join Recurrence R on  [Activity].RecurrenceID=R.RecurrenceID                  
 WHERE       
  [Activity].[ActivityID] = @ActivityId  ;   
end
GO
