/****** Object:  StoredProcedure [dbo].[USP_GetOrderList]    Script Date: 07/26/2008 16:18:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getorderlist')
DROP PROCEDURE usp_getorderlist
GO
CREATE PROCEDURE [dbo].[USP_GetOrderList]        
 @SortChar char(1)='0',       
 @CurrentPage int,    
 @PageSize int,    
 @TotRecs int output,    
 @columnName as Varchar(50),    
 @columnSortOrder as Varchar(10),  
 @numCompanyID as numeric(9)=0    
as    
    
--Create a Temporary table to hold data    
Create table #tempTable ( ID INT IDENTITY PRIMARY KEY,     
      numOppId VARCHAR(15),    
      Name varchar(100),    
      STAGE VARCHAR(100),    
      CloseDate varchar(15),    
 numcreatedby varchar(15),    
 Contact varchar(110),    
 Company varchar(100),    
 numCompanyID varchar(15),    
 tintCRMType varchar(10),    
 numContactID varchar(15),    
 numDivisionID  varchar(15),    
 numTerID varchar(15),    
 monPAmount DECIMAL(20,5),    
 vcusername varchar(50)    
 )    
declare @strSql as varchar(8000)    
set @strSql='SELECT  Opp.numOppId,     
  Opp.vcPOppName AS Name,     
  dbo.GetOppStatus(Opp.numOppId) as STAGE,     
  Opp.intPEstimatedCloseDate AS CloseDate,     
  isnull(Opp.numcreatedby,0) ,    
  ADC.vcFirstName + '' '' + ADC.vcLastName AS Contact,     
  C.vcCompanyName AS Company,    
                C.numCompanyID,    
  ADC.tintCRMType,    
  ADC.numContactID,    
  Div.numDivisionID,    
  Div.numTerID,    
  Opp.monPAmount,    
  isnull(usermaster.vcusername,'''')     
  FROM OpportunityMaster Opp     
  INNER JOIN AdditionalContactsInformation ADC     
  ON Opp.numContactId = ADC.numContactId     
  INNER JOIN DivisionMaster Div     
  ON Opp.numDivisionId = Div.numDivisionID AND ADC.numDivisionId = Div.numDivisionID     
  INNER JOIN CompanyInfo C     
  ON Div.numCompanyID = C.numCompanyId    
  left join usermaster on usermaster.numuserid=Opp.numcreatedby     
  WHERE Opp.tintOppstatus=1 '    
  
  if @numCompanyID<>0 set @strSql=@strSql + ' And Div.numCompanyID =' + convert(varchar(15),@numCompanyID)    
if @SortChar<>'0' set @strSql=@strSql + ' And Opp.vcPOppName like '''+@SortChar+'%'''     
  
    
insert into #tempTable(numOppId,    
      Name,    
      STAGE,    
      CloseDate,    
 numcreatedby ,    
 Contact,    
 Company,    
 numCompanyID,    
 tintCRMType,    
 numContactID,    
 numDivisionID,    
 numTerID,    
 monPAmount,    
 vcusername)    
exec (@strSql)     
    
  declare @firstRec as integer     
  declare @lastRec as integer    
 set @firstRec= (@CurrentPage-1) * @PageSize    
     set @lastRec= (@CurrentPage*@PageSize+1)    
select * from #tempTable where ID > @firstRec and ID < @lastRec    
set @TotRecs=(select count(*) from #tempTable)    
drop table #tempTable
GO
