/****** Object:  StoredProcedure [dbo].[usp_ActivateUser]    Script Date: 07/26/2008 16:14:42 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_activateuser')
DROP PROCEDURE usp_activateuser
GO
CREATE PROCEDURE [dbo].[usp_ActivateUser]
	@intUserId int,
	@bitActivate bit 
--  

AS
	--Update ActivationStatus into Table  
	Update UserMaster set bitActivateFlag=@bitActivate
	WHERE numUserId=@intUserId
GO
