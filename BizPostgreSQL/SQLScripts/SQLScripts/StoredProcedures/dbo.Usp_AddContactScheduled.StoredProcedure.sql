/****** Object:  StoredProcedure [dbo].[Usp_AddContactScheduled]    Script Date: 07/26/2008 16:14:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_addcontactscheduled')
DROP PROCEDURE usp_addcontactscheduled
GO
CREATE PROCEDURE [dbo].[Usp_AddContactScheduled]
@numContactID as numeric(9),
@numScheduleId as numeric(9)
as 
insert into CustRptSchContacts(numScheduleId,numcontactId) 
values(@numScheduleId,@numContactID)
GO
