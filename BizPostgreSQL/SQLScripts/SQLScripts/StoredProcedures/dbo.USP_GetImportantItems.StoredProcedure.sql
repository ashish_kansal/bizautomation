/****** Object:  StoredProcedure [dbo].[USP_GetImportantItems]    Script Date: 07/26/2008 16:17:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--- Created By Anoop Jayaraj                                                
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getimportantitems')
DROP PROCEDURE usp_getimportantitems
GO
CREATE PROCEDURE [dbo].[USP_GetImportantItems]                                                                                   
@PageSize [int] ,                      
@CurrentPage [int],                      
@srchSubjectBody as varchar(100) = '',                      
--@srchEmail as varchar(100) ='',                      
@srchAttachmenttype as varchar(100) ='',                             
@TotRecs int output        ,              
@ToEmail as varchar (100)  ,    
@columnName as varchar(50) ,    
@columnSortOrder as varchar(4)      
       
as                                                
                                                
            
                                      
 declare @firstRec as integer                                                
 declare @lastRec as integer                                                
 set @firstRec= (@CurrentPage-1) * @PageSize                                                
 set @lastRec= (@CurrentPage*@PageSize+1)                                                
                                               
               
declare @strSql as varchar(8000)              
            
   if @columnName = 'FromName'           
 begin    
 set @columnName =  'dbo.GetEmaillName(emailHistory.numEmailHstrID,4)'    
 end    
 if @columnName = 'FromEmail'    
 begin    
 set @columnName = 'dbo.GetEmaillAdd(emailHistory.numEmailHstrID,4)'    
 end           
            
set @strSql='With tblSubscriber AS (SELECT  distinct(emailHistory.numEmailHstrID) ,      
ROW_NUMBER() OVER (ORDER BY '+@columnName+' '+@columnSortOrder+') AS  RowNumber       
from emailHistory                          
left join EmailHstrAttchDtls on emailHistory.numEmailHstrID=EmailHstrAttchDtls.numEmailHstrID               
join EmailHStrToBCCAndCC on emailHistory.numEmailHstrID=EmailHStrToBCCAndCC.numEmailHstrID               
join EmailMaster on        EmailMaster.numEmailId=EmailHStrToBCCAndCC.numEmailId               
where emailHistory.tinttype=4 '      
           
 if @srchSubjectBody <> '' set @strSql=@strSql +'and (vcSubject  like ''%'+@srchSubjectBody+'%'' or vcBodyText like ''%'+@srchSubjectBody+'%''     
or emailHistory.numEmailHstrID in (select numEmailHstrId from EmailHStrToBCCAndCC where       
(numemailid in (select numEmailid from emailmaster where vcemailid like ''%'+@srchSubjectBody+'%'')) or  vcName like ''%'+@srchSubjectBody+'%'')) '          
if @ToEmail <> '' set @strSql=@strSql +'         
      and (EmailMaster.vcEmailId like '''+@ToEmail+''' and EmailHStrToBCCAndCC.tintType<>4 )  '      
if @srchAttachmenttype <>'' set @strSql=@strSql +'        
  and  emailHistory.numEmailHstrID in  (select numEmailHstrId from EmailHstrAttchDtls          
  where EmailHstrAttchDtls.vcAttachmentType like ''%'+@srchAttachmenttype+'%'')'      
      
      
           
set @strSql=@strSql +')               
 select RowNumber,      
dbo.GetEmaillAdd(emailHistory.numEmailHstrID,4) as FromEmail,            
 dbo.GetEmaillName(emailHistory.numEmailHstrID,4) as FromName,                       
--isnull(vcFromEmail,'''') as FromEmail,                                
dbo.GetEmaillAdd(emailHistory.numEmailHstrID,1) as ToEmail,                           
dbo.GetEmaillAdd(emailHistory.numEmailHstrID,2) as CCEmail,                          
dbo.GetEmaillAdd(emailHistory.numEmailHstrID,3) as BCCEmail,                               
emailHistory.numEmailHstrID,                      
isnull(vcSubject,'''') as vcSubject,                                
convert(varchar(max),vcBody) as Body,                        
dbo.FormatedDateFromDate(bintCreatedOn,numDomainId) as created,                        
isnull(vcItemId,'''') as ItemId,                        
isnull(vcChangeKey,'''') as ChangeKey,                        
isnull(bitIsRead,''False'') as IsRead,                        
isnull(vcSize,0) as vcSize,                        
isnull(bitHasAttachments,''False'') as HasAttachments,                       
  isnull(EmailHstrAttchDtls.vcFileName,'''') as AttachmentPath,                      
isnull(EmailHstrAttchDtls.vcAttachmentType,'''') as AttachmentType,                
isnull(EmailHstrAttchDtls.vcAttachmentItemId,'''') as AttachmentItemId,                       
dbo.FormatedDateFromDate(dtReceivedOn,numDomainId) as dtReceivedOn,                        
isnull(emailHistory.tintType,1) as type,                        
isnull(chrSource,''B'') as chrSource,                    
isnull(vcCategory,''white'') as vcCategory             
 from tblSubscriber T            
 join emailHistory             
 on emailHistory.numEmailHstrID=T.numEmailHstrID       
left join EmailHstrAttchDtls on emailHistory.numEmailHstrID=EmailHstrAttchDtls.numEmailHstrID       
where emailHistory.tinttype=4  and          
  RowNumber > '+convert(varchar(10),@firstRec)+' and RowNumber < '+convert(varchar(10),@lastRec)+'        
union           
 select 0 as RowNumber,null,null,null,null,null,count(*),null,null,      
null,null,null,null,null,null,null,null,null,      
null,1,null,null from tblSubscriber  order by RowNumber'      
print    @strSql       
exec (@strSql)
GO
