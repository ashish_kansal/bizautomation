/****** Object:  StoredProcedure [dbo].[USP_GetOpeningBalanceForMasterListDetails]    Script Date: 07/26/2008 16:17:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created by Siva    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getopeningbalanceformasterlistdetails')
DROP PROCEDURE usp_getopeningbalanceformasterlistdetails
GO
CREATE PROCEDURE [dbo].[USP_GetOpeningBalanceForMasterListDetails]      
@numListItemID as numeric(9),      
@numDomainId as numeric(9)      
      
AS      
Begin      
	Select isnull(numOpeningBal,0) From Chart_Of_Accounts 
	Where numListItemID=@numListItemID And numDomainId=@numDomainId      
End
GO
