/****** Object:  StoredProcedure [dbo].[usp_DeleteContact]    Script Date: 07/26/2008 16:15:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- exec usp_DeleteContact @numContactID=90126
--created y anoop jayaraj    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_DeleteContact')
DROP PROCEDURE usp_DeleteContact
GO
CREATE PROCEDURE [dbo].[usp_DeleteContact]
 @numContactID numeric(9)=0
AS      
--------Date:17-Oct-2011 By:Pinkal Patel----------------------    
DECLARE @Email AS VARCHAR(1000) 
DECLARE @AddEmail AS VARCHAR(1000)

SELECT @Email =vcEmail FROM dbo.AdditionalContactsInformation WHERE numContactId=@numContactID
SELECT @AddEmail =vcAsstEmail FROM dbo.AdditionalContactsInformation WHERE numContactId=@numContactID
--------Date:17-Oct-2011 By:Pinkal Patel----------------------
declare @bitPrimaryContact as bit   
DECLARE @numDomainID AS numeric 
select @bitPrimaryContact=ISNULL(bitPrimaryContact,0),@numDomainID=[numDomainID] from AdditionalContactsInformation where numContactID=@numContactID    


IF EXISTS( select * from  usermaster where numuserdetailId=@numContactID and numDomainID=@numDomainID and bitActivateFlag=1)
BEGIN
	raiserror ('USER', 16, 1 ) ;
    RETURN 1
END
if @bitPrimaryContact=1    /*Deleting Primary Contact is not allowed*/
begin    
	  	raiserror ('PRIMARY_CONTACT', 16, 1 ) ;
        RETURN 1
end
IF EXISTS ( select * from [OpportunityMaster] WHERE [numContactId] =@numContactID)
  BEGIN
  	raiserror ('CHILD_OPP', 16, 1 ) ;
    RETURN 1
  END
IF EXISTS ( SELECT * FROM [Cases] WHERE [numContactId] = @numContactID)
  BEGIN
  	raiserror ('CHILD_CASE', 16, 1 ) ;
    RETURN 1
  END
begin  
 
--  declare @numOppID as numeric(9)    
--    set @numOppID=0    
--   select Top 1 @numOppID=numOppID from  OpportunityMaster where numContactID= @numContactID order by numOppID    
--   while @numOppID>0    
--     begin    
--   exec USP_DeleteOppurtunity @numOppID    
--   select Top 1 @numOppID=numOppID from  OpportunityMaster     
--   where numContactID= @numContactID  and numOppID>@numOppID order by numOppID    
--    if @@rowcount=0 set @numOppID=0
--   end 
BEGIN TRY
   BEGIN TRAN
		DELETE FROM dbo.CompanyAssociations WHERE numDomainID=@numDomainID AND numContactID=@numContactID
		DELETE FROM dbo.ImportActionItemReference WHERE numContactID=@numContactID
		delete ConECampaignDTL where numConECampID in (select numConEmailCampID from ConECampaign where numContactID=@numContactID)  
		delete ConECampaign where numContactID=@numContactID  
		delete AOIContactLink where numContactID=@numContactID    
		delete CaseContacts where numContactID=@numContactID   
		delete ProjectsContacts where numContactID=@numContactID  
		delete OpportunityContact where numContactID=@numContactID  
		delete UserTeams where numUserCntID=@numContactID 
		delete UserTerritory where numUserCntID=@numContactID 
		DELETE FROM dbo.UserMaster WHERE numUserDetailId = @numContactID AND bitActivateFlag=0
		DELETE AdditionalContactsInformation WHERE numContactID=@numContactID
		--------Date:17-Oct-2011 By:Pinkal Patel----------------------    
		--IF @Email <> ''
		--BEGIN 
		UPDATE EmailMaster SET numContactID=NULL WHERE vcEmailId = @Email AND numContactID =@numContactID
		--END 
		--IF @AddEmail<>''
		--BEGIN 
		UPDATE EmailMaster SET numContactID=NULL WHERE vcEmailID = @AddEmail AND numContactID =@numContactID
		--END 
		--------Date:17-Oct-2011 By:Pinkal Patel----------------------    
		 select 1    
COMMIT TRAN
        END TRY 
        BEGIN CATCH		
        IF ( @@TRANCOUNT > 0 ) 
                BEGIN
                    ROLLBACK TRAN
                    DECLARE @error VARCHAR(1000)
					SET @error = ERROR_MESSAGE();
                    raiserror (@error, 16, 1 ) ;
                    RETURN 1
                END
        END CATCH	
end
GO
