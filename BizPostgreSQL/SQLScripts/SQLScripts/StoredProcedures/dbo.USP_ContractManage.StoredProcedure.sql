/****** Object:  StoredProcedure [dbo].[USP_ContractManage]    Script Date: 07/26/2008 16:15:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_contractmanage')
DROP PROCEDURE usp_contractmanage
GO
CREATE PROCEDURE  [dbo].[USP_ContractManage]      
@numContractId bigint output,      
@numDivisionID BigInt,      
@vcContractName VarChar(250),      
@bintStartDate DateTime,      
@bintExpDate DateTime,      
@numIncidents BigInt,      
@numHours BigInt,      
@vcNotes Text,      
@numEmailDays BigInt,      
@numEmailIncidents BigInt,      
@numEmailHours BigInt,      
@numUserCntID BigInt,      
@numDomainID BigInt,      
@numAmount Bigint,    
@bitAmount bit,    
@bitDays bit,    
@bitHours bit,    
@bitInci bit    ,  
@bitEDays bit,    
@bitEHours bit,    
@bitEInci bit,      
@decRate DECIMAL(20,5)                      
as       
      
insert into  ContractManagement      
(      
numDivisionID ,      
vcContractName ,      
bintStartDate ,      
bintExpDate ,      
numIncidents ,      
numHours ,      
vcNotes ,      
numEmailDays ,      
numEmailIncidents ,      
numEmailHours ,      
numUserCntID ,      
numDomainID,      
bintcreatedOn,      
numAmount ,    
bitAmount,    
bitDays ,    
bitHour,    
bitIncidents ,decrate  )      
      
values      
(      
@numDivisionID ,      
@vcContractName ,      
@bintStartDate ,      
@bintExpDate ,      
@numIncidents ,      
@numHours ,      
@vcNotes ,      
@numEmailDays ,      
@numEmailIncidents ,      
@numEmailHours ,      
@numUserCntID ,      
@numDomainID,      
getutcdate() ,      
@numAmount,    
@bitAmount,    
@bitDays ,    
@bitHours,    
@bitInci ,@decRate         
)      
SELECT      
   @numContractId = SCOPE_IDENTITY();      
SELECT      
   @numContractId;
GO
