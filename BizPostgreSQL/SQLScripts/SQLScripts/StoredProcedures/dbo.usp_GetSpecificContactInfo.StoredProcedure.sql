/****** Object:  StoredProcedure [dbo].[usp_GetSpecificContactInfo]    Script Date: 07/26/2008 16:18:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
--This stored procedure is for getting specific details such as first name,Last name,phone,email
--pertaining to a specific contactid
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getspecificcontactinfo')
DROP PROCEDURE usp_getspecificcontactinfo
GO
CREATE PROCEDURE [dbo].[usp_GetSpecificContactInfo]
	@numContactId numeric   
--
AS
	--SELECT vcFirstname,vcLastName,numPhone,vcEmail,numContactType  FROM additionalContactsinformation WHERE
	SELECT a.vcFirstname,a.vcLastName,a.numPhone,a.vcEmail,b.vcData,a.numcontacttype FROM additionalContactsinformation a INNER JOIN listdetails b ON a.numcontacttype=b.numlistitemid WHERE
	 a.numContactId=@numcontactId
GO
