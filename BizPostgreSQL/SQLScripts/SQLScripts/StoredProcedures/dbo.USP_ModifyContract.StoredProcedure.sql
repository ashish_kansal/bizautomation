/****** Object:  StoredProcedure [dbo].[USP_ModifyContract]    Script Date: 07/26/2008 16:20:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_modifycontract')
DROP PROCEDURE usp_modifycontract
GO
CREATE PROCEDURE  [dbo].[USP_ModifyContract]      
@numContractId bigint ,      
@numDivisionID BigInt,      
@vcContractName VarChar(250),      
@bintStartDate DateTime,      
@bintExpDate DateTime,      
@numIncidents BigInt,      
@numHours BigInt,      
@vcNotes Text,      
@numEmailDays BigInt,      
@numEmailIncidents BigInt,      
@numEmailHours BigInt,      
@numUserCntID BigInt,      
@numDomainID BigInt,      
@numAmount Bigint,    
@bitAmount bit,    
@bitDays bit,    
@bitHours bit,    
@bitInci bit ,  
@bitEDays bit,    
@bitEHours bit,    
@bitEInci bit      ,  
 @numTemplateId BigInt  ,
@rateHr decimal(10,2)                    
as       
      
update ContractManagement      
set numDivisionID =@numDivisionID ,      
vcContractName =@vcContractName,      
bintStartDate =@bintStartDate,      
bintExpDate =@bintExpDate,      
numIncidents =@numIncidents,      
numHours =@numHours,      
vcNotes =@vcNotes,      
numEmailDays =@numEmailDays ,      
numEmailIncidents =@numEmailIncidents ,      
numEmailHours=@numEmailHours ,      
numAmount = @numAmount ,    
bitAmount=@bitAmount,    
bitDays =@bitDays ,    
bitHour=@bitHours,    
bitIncidents  =@bitinci  ,  
bitEDays =@bitEDays ,    
bitEHour=@bitEHours,    
bitEIncidents  =@bitEinci ,  
numTemplateId=@numTemplateId,
decRate =  @rateHr
where      
numContractId =@numContractId and      
numUserCntID =@numUserCntID and      
numDomainID =@numDomainID
GO
