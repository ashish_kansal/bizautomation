/****** Object:  StoredProcedure [dbo].[usp_GetUsersListForActivationChange]    Script Date: 07/26/2008 16:18:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj  
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getuserslistforactivationchange')
DROP PROCEDURE usp_getuserslistforactivationchange
GO
CREATE PROCEDURE [dbo].[usp_GetUsersListForActivationChange]           
@DomainID as numeric(9)           
As            
                           
SELECT     U.numUserId,U.vcUserName,vcMailNickName, isnull(U.vcEmailID,'') AS Email, U.vcUserDesc AS vcUserDesc,isnull(U.vcEmailID,'') as vcEmailID,             
                      U.bitActivateFlag AS Active, 0 as activeflag           
FROM         UserMaster U INNER JOIN            
[Domain] D ON U.numDomainID = [D].numDomainID where [D].numDomainID = @DomainID        
and U.bitActivateFlag=1        
 order by U.bitActivateFlag desc
GO
