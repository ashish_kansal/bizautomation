/****** Object:  StoredProcedure [dbo].[USP_GetManageNews]    Script Date: 07/26/2008 16:17:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj  
--created by anoop jayaraj  
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getmanagenews')
DROP PROCEDURE usp_getmanagenews
GO
CREATE PROCEDURE [dbo].[USP_GetManageNews]  
@numNewsID as numeric(9)=0 output,  
@vcHeading as varchar(1000)='',  
@vcDate as varchar(50)='',  
@vcDesc as text='',  
@vcImagePath as varchar(500)='',  
@vcURL as varchar(500)='',  
@bitActive as tinyint=0,
@tintType as tinyint=0 
as  
  
if @numNewsID=0   
begin  
insert into news(vcNewsHeading,vcDate,vcDesc,vcImagePath,vcURL,vcCreatedDate,bitActive,tintType)  
values(@vcHeading,@vcDate,@vcDesc,@vcImagePath,@vcURL,getutcdate(),@bitActive,@tintType)  
set @numNewsID=@@identity  
end  
else if @numNewsID>0  
begin  
update news set vcNewsHeading=@vcHeading,  
vcDate=@vcDate,  
vcDesc=@vcDesc,vcImagePath=@vcImagePath,vcURL=@vcURL,vcModifieddate=getutcdate(),bitActive=@bitActive ,tintType=@tintType 
where numNewsID=@numNewsID  
end
GO
