/****** Object:  StoredProcedure [dbo].[usp_GetOpenBizDocs]    Script Date: 03/06/2009 00:39:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
DECLARE @p8 INT
SET @p8 = 0
EXEC usp_GetOpenBizDocs @numDomainID = 1, @vcBizDocStatus = NULL,
    @tintOppType = 1, @SortCol = 'vcBizDocID', @SortDirection = 'asc',
    @CurrentPage = 1, @PageSize = 20, @TotRecs = @p8 OUTPUT,
    @numDivisionID = 0, @numBizDocId = 287, @tintSalesUserRightType = 3,
    @tintPurchaseUserRightType = 0, @numUserCntID = 1, @tintFilter = 0,
    @dtFromDate = '2013-03-22 00:00:00', @dtToDate = '2013-03-22 
23:59:59', @ClientTimeZoneOffset = -330
SELECT  @p8
*/
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getopenbizdocs')
DROP PROCEDURE usp_getopenbizdocs
GO
CREATE PROCEDURE [dbo].[usp_GetOpenBizDocs]
    (
      @numDomainId AS NUMERIC(9) = 0,
      @vcBizDocStatus AS VARCHAR(MAX) = '',
      @tintOppType AS TINYINT,
      @SortCol VARCHAR(50) = 'dtCreatedDate',
      @SortDirection VARCHAR(4) = 'desc',
      @CurrentPage INT,
      @PageSize INT,
      @TotRecs INT OUTPUT,
      @numDivisionID NUMERIC(9) = 0,
	  @numBizDocId NUMERIC(9) = '',
      @tintSalesUserRightType TINYINT,
      @tintPurchaseUserRightType TINYINT,
      @numUserCntID NUMERIC,
	  @tintFilter TINYINT,
      @dtFromDate DATETIME,
      @dtToDate DATETIME,
      @ClientTimeZoneOffset INT
    )
AS 
    BEGIN
    DECLARE @BaseCurrencySymbol nVARCHAR(3);SET @BaseCurrencySymbol='';
	SELECT @BaseCurrencySymbol = ISNULL(varCurrSymbol,'$') FROM dbo.Currency 
															   WHERE numCurrencyID IN ( SELECT numCurrencyID FROM dbo.Domain 
																						WHERE numDomainId=@numDomainId)

/*paging logic*/  
        DECLARE @firstRec AS INTEGER                                                              
        DECLARE @lastRec AS INTEGER                                                     
--        IF @vcOppBizDocIds <>'' SET @CurrentPage = 1; --bug id 1081
        SET @firstRec = ( @CurrentPage - 1 ) * @PageSize                             
        SET @lastRec = ( @CurrentPage * @PageSize + 1 )                                                                    


/*Dynamic sql*/
        DECLARE @strSql NVARCHAR(MAX)
        DECLARE @SELECT NVARCHAR(MAX)
        DECLARE @FROM NVARCHAR(MAX)
        DECLARE @WHERE NVARCHAR(MAX)
        IF @vcBizDocStatus IS NULL 
            SET @vcBizDocStatus = ''
       
		IF @SortCOl = 'status' SET @SortCol ='ld.vcData'
      
--SELECT numOppBizDocsID FROM dbo.DepositeDetails WHERE numDepositeID = 432



        SET @SELECT = 'WITH bizdocs
         AS (SELECT Row_number() OVER(ORDER BY ' + @SortCol + ' '
            + @SortDirection
            + ') AS row,
			om.vcpoppname,
			ISNULL(C.fltExchangeRate,1) fltExchangeRateCurrent,
			ISNULL(om.fltExchangeRate,1) fltExchangeRateOfOrder,
			''' + CONVERT(VARCHAR(100), @BaseCurrencySymbol) + ''' [BaseCurrencySymbol],
			ISNULL(tintOppType,0) AS [tintOppType],
			om.numoppid,
			obd.[numoppbizdocsid],
			obd.[vcbizdocid],
			obd.[numbizdocstatus],
			obd.[vccomments],
			obd.[monamountpaid],
			obd.monDealAmount,
			obd.monCreditAmount monCreditAmount,
			ISNULL(C.varCurrSymbol,'''') varCurrSymbol,
			ld.vcData AS [status],
			[dbo].[FormatedDateFromDate](dtFromDate,'+ CONVERT(VARCHAR(15), @numDomainId) + ') dtFromDate,
			CASE ISNULL(om.bitBillingTerms,0)
			--WHEN 1 THEN [dbo].[FormatedDateFromDate]( DATEADD(dd, convert(int,ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0)),dtFromDate)
			WHEN 1 THEN [dbo].[FormatedDateFromDate]( DATEADD(dd, convert(int,ISNULL(( SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(intBillingDays, 0)), 0)),dtFromDate),'
            + CONVERT(VARCHAR(15), @numDomainId) + ') 
			WHEN 0 THEN [dbo].[FormatedDateFromDate](obd.[dtFromDate],'
            + CONVERT(VARCHAR(15), @numDomainId) + ') 
			END AS dtDueDate,
			om.numContactId,
			om.numDivisionID,
			(obd.monDealAmount - isnull(monCreditAmount,0) - isnull(monamountpaid,0) ) AS baldue,
			vcCompanyName,DM.tintCRMType,
			obd.vcRefOrderNo,isnull(con.vcEmail,'''') AS vcEmail,ISNULL(obd.numBizDocTempID,0) AS numBizDocTempID,ISNULL(obd.[bitAuthoritativeBizDocs],0) bitAuthoritativeBizDocs,
--			case when isnumeric(ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0))=1 
--				then ISNULL(dbo.fn_GetListItemName(isnull(intBillingDays,0)),0) 
--				else 0 
--			end AS numBillingDaysName,
			CASE WHEN ISNUMERIC((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(intBillingDays,0))) = 1
 			     THEN (SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(intBillingDays,0))
				 ELSE 0
			END AS numBillingDaysName,
			obd.numBizDocId  '
			
		SET @FROM =' FROM   opportunitymaster om
                    INNER JOIN [opportunitybizdocs] obd
                      ON om.[numoppid] = obd.[numoppid]
                     INNER JOIN DivisionMaster DM
						ON om.numDivisionId = DM.numDivisionID
						Inner join CompanyInfo cmp on cmp.numCompanyID=DM.numCompanyID  
                    LEFT OUTER JOIN  [ListDetails] ld
					  ON ld.[numListItemID] = obd.[numbizdocstatus]
					LEFT OUTER JOIN [Currency] C
					ON C.numCurrencyID = om.numCurrencyID
					JOIN AdditionalContactsInformation con ON con.numdivisionId = DM.numdivisionId AND con.numContactid = om.numContactId' ;
					
        SET @WHERE = ' WHERE  (obd.[bitauthoritativebizdocs] = 1 OR obd.[bitauthoritativebizdocs] = 0)
                    /*AND om.tintopptype = ' + CONVERT(VARCHAR(15), @tintOppType)+ '*/
					AND om.numDomainID = '+ CONVERT(VARCHAR(15), @numDomainId) + '
					AND (om.numDivisionID ='+ CONVERT(VARCHAR(15), @numDivisionID) + ' OR '+ CONVERT(VARCHAR(15), @numDivisionID) + ' = 0) 
					AND (obd.monDealAmount - monamountpaid >0 ) 
					AND DATEADD(MINUTE, ' + CAST(-@ClientTimeZoneOffset AS VARCHAR(10)) + ',obd.dtFromDate) 
						BETWEEN ''' + CONVERT(VARCHAR(20),@dtFromDate) + ''' AND ''' + CONVERT(VARCHAR(20),@dtToDate) + ''''
					--AND ((DATEADD(MINUTE,' + CONVERT(VARCHAR(10),-@ClientTimeZoneOffset) + ',obd.dtFromDate) BETWEEN ''' + CONVERT(VARCHAR(10),@dtFromDate) + ''' AND ''' +  CONVERT(VARCHAR(10),@dtToDate) + ''')) '
		
		IF ISNULL(@vcBizDocStatus,'') = '-1'
			SET @WHERE = @WHERE + 'AND isnull(obd.[numbizdocstatus],0) = 0 ';
		ELSE IF ISNULL(@vcBizDocStatus,'') = ''
			SET @vcBizDocStatus = ''--@WHERE = @WHERE + ' AND ISNULL(' + @vcBizDocStatus + ','''') = '''')'
		ELSE 
			SET @WHERE = @WHERE + ' AND obd.[numbizdocstatus]  IN (' + @vcBizDocStatus + ')'
			
--		IF @intBizDocStatus = -1 
--			SET @WHERE = @WHERE + 'AND isnull(obd.[numbizdocstatus],0) = 0 ';
--		ELSE 
--			SET @WHERE = @WHERE + 'AND (obd.[numbizdocstatus] = ' + CONVERT(VARCHAR(15), @intBizDocStatus) + ' OR ' + CONVERT(VARCHAR(15), @intBizDocStatus)+ ' = 0) ';
					
        IF @numBizDocId > 0
            SET @WHERE = @WHERE + ' AND obd.numBizDocId = ' + CONVERT(VARCHAR(10),@numBizDocId)
        IF @tintSalesUserRightType = 1 --Owner
            BEGIN
                SET @WHERE = @WHERE + ' AND (om.numRecOwner = ' + CONVERT(VARCHAR(15), @numUserCntID)+ ' or om.numAssignedTo= ' + CONVERT(VARCHAR(15), @numUserCntID)+ ' or  om.numOppId in (select distinct(numOppId)                             
							from OpportunityStageDetails where numAssignTo =' + CONVERT(VARCHAR(15), @numUserCntID) + '))'
            END
        ELSE 
            IF @tintSalesUserRightType = 2 -- Terittory
                BEGIN
                    SET @WHERE = @WHERE + ' AND (DM.numTerID in (select numTerritoryID from  UserTerritory where numUserCntID= ' + CONVERT(VARCHAR(15), @numUserCntID)
                        + ' ) or DM.numTerID=0 or om.numAssignedTo= ' + CONVERT(VARCHAR(15), @numUserCntID) + ' or  om.numOppId in (select distinct(numOppId) from OpportunityStageDetails where numAssignTo =' + CONVERT(VARCHAR(15), @numUserCntID) + '))'       
                END 

		IF @tintFilter=1
		BEGIN
			SET @WHERE = @WHERE + ' AND om.numRecOwner=' + CONVERT(VARCHAR(15), @numUserCntID) 
		END
		
		IF @tintFilter=2
		BEGIN
			SET @WHERE = @WHERE + ' AND om.numAssignedTo=' + CONVERT(VARCHAR(15), @numUserCntID)
		END
        SET @strSql = @SELECT + @FROM + 
					  @WHERE + ') SELECT * FROM   [bizdocs] WHERE row > ' + CONVERT(VARCHAR(10), @firstRec) + ' and row <' + CONVERT(VARCHAR(10), @lastRec) 
        
        
        PRINT @strSql ;
        EXEC ( @strSql ) ;
        
        /*Get total count */
        SET @strSql =  ' (SELECT @TotRecs =COUNT(*) ' + @FROM + @WHERE + ')';
        
        PRINT @strSql ;
        EXEC sp_executesql 
        @query = @strSql, 
        @params = N'@TotRecs INT OUTPUT', 
        @TotRecs = @TotRecs OUTPUT 
        
    END
  
