/****** Object:  StoredProcedure [dbo].[usp_GetDynamicLeadBoxEntryForLeadGeneration]    Script Date: 07/26/2008 16:17:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created By: Debasish Tapan Nag                              
--Purpose: Saves the Lead Box form entries in the database from an XML document                             
--Created Date: 08/03/2005                    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getdynamicleadboxentryforleadgeneration')
DROP PROCEDURE usp_getdynamicleadboxentryforleadgeneration
GO
CREATE PROCEDURE [dbo].[usp_GetDynamicLeadBoxEntryForLeadGeneration]          
 @numLeadBoxId numeric (9)          
AS                
 SELECT vcDbColumnName,         
 Case When boolAOIField = 1 Then      
  Case When vcDbColumnValue = 1 Then        
   vcFieldName        
  Else         
   ''        
  End        
 When vcDbColumnValue = 0 Then      
   vcDbColumnValueText      
 Else      
   vcDbColumnValue    
 End      
 as vcDbColumnValue, vcDbColumnValueText, vcAssociatedControlType     
 from DynamicLeadBoxFormDataDetails          
 WHERE numLeadBoxId = @numLeadBoxId
GO
