/****** Object:  StoredProcedure [dbo].[usp_GenDocDetails]    Script Date: 07/26/2008 16:16:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_gendocdetails')
DROP PROCEDURE usp_gendocdetails
GO
CREATE PROCEDURE [dbo].[usp_GenDocDetails]                          
(                          
@byteMode as tinyint=null,                          
@GenDocId as numeric(9)=null,                
@numUserCntID as numeric(9)=0 ,    
@numDomainID numeric(9),
@ClientTimeZoneOffset Int                          
)                          
as                          
if @byteMode=0                          
begin                          
select                           
 numGenericDocId,                          
 VcFileName ,                          
 vcDocName ,                          
 numDocCategory,                          
 vcfiletype,                          
 numDocStatus,                          
 vcDocdesc,                        
 vcSubject,                         
 ISNULL(cUrlType,'') cUrlType,
numCreatedBy,       
case when ISNULL(vcDocumentSection,'') ='M' then 'true' else 'false' end as isForMarketingDept,
 dbo.fn_GetContactName(numCreatedBy) as RecordOwner,                          
 dbo.fn_GetContactName(numCreatedBy)+', '+ convert(varchar(20),dateAdd(minute, -@ClientTimeZoneOffset,bintCreatedDate )) as Created ,                          
 dbo.fn_GetContactName(numModifiedBy)+', '+ convert(varchar(20),dateAdd(minute, -@ClientTimeZoneOffset,bintModifiedDate )) as Modified,
 (select count(*) from DocumentWorkflow where numDocID=@GenDocId and numContactID=@numUserCntID and cDocType='D' and tintApprove=0) as AppReq ,            
 (select count(*) from DocumentWorkflow                     
 where numDocID=@GenDocId and cDocType='D' and tintApprove=1) as Approved,            
 (select count(*) from DocumentWorkflow                     
 where numDocID=@GenDocId and cDocType='D' and tintApprove=2) as Declined,            
 (select count(*) from DocumentWorkflow                     
 where numDocID=@GenDocId and cDocType='D' and tintApprove=0) as Pending,
 ISNULL(tintCheckOutStatus,0) tintCheckOutStatus,
 ISNULL(numLastCheckedOutBy,0) numLastCheckedOutBy,
 dbo.fn_GetContactName(numLastCheckedOutBy)+', '+ convert(varchar(20),dateAdd(minute, -@ClientTimeZoneOffset,dtCheckOutDate )) as CheckedOut,
 ISNULL(numModuleId,0) numModuleID,ISNULL(vcContactPosition,'') vcContactPosition,
 BizDocOppType,
 BizDocType,
 BizDocTemplate,
 ISNULL(numCategoryId,0) AS numCategoryId,
 ISNULL(bitLastFollowupUpdate,0) AS bitLastFollowupUpdate,
 ISNULL(vcGroupsPermission,'') AS vcGroupsPermission,
 ISNULL(numFollowUpStatusId,0) AS numFollowUpStatusId,
 ISNULL(bitUpdateFollowupStatus,0) AS bitUpdateFollowupStatus
from GenericDocuments where numGenericDocId=@GenDocId  and    numDomainID = @numDomainID
              
end                          

if @byteMode=1                          
begin                          
                          
delete from DocumentWorkflow where numDocID=@GenDocId and cDocType='D'      

delete from BizDocAction where numBizActionId in(select numBizActionId from BizActionDetails where numOppBizDocsId =@GenDocId and btDocType=2) 
delete from BizActionDetails where numOppBizDocsId =@GenDocId and btDocType=2


delete from GenericDocuments where numGenericDocId=@GenDocId  and numDomainID = @numDomainID                        
end          
                        
if @byteMode=2                          
begin                          
                          
select  numGenericDocId,                          
 VcFileName  from GenericDocuments where numGenericDocId=@GenDocId and    numDomainID = @numDomainID                      
end
GO
