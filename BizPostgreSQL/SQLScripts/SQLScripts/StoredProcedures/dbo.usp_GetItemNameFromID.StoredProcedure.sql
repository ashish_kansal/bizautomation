/****** Object:  StoredProcedure [dbo].[usp_GetItemNameFromID]    Script Date: 07/26/2008 16:17:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getitemnamefromid')
DROP PROCEDURE usp_getitemnamefromid
GO
CREATE PROCEDURE [dbo].[usp_GetItemNameFromID]
	@numListItemID numeric(9)=0   
--
AS
	select vcData from listdetails where numListItemID=@numListItemID
GO
