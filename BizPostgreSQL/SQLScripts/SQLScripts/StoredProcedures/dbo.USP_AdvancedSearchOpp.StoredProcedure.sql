/****** Object:  StoredProcedure [dbo].[USP_AdvancedSearchOpp]    Script Date: 07/26/2008 16:14:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_advancedsearchopp')
DROP PROCEDURE usp_advancedsearchopp
GO
Create PROCEDURE [dbo].[USP_AdvancedSearchOpp]
@WhereCondition as varchar(4000)='',
@numDomainID as numeric(9)=0,
@numUserCntID as numeric(9)=0,
@CurrentPage int,
@PageSize int,                                                                  
@TotRecs int output,                                                                                                           
@columnSortOrder as Varchar(10),    
@ColumnName as varchar(50)='',    
@SortCharacter as char(1),                
@SortColumnName as varchar(50)='',    
@LookTable as varchar(10)='',
@strMassUpdate as varchar(2000)='',
@vcRegularSearchCriteria varchar(MAX)='',
@vcCustomSearchCriteria varchar(MAX)='',
@vcDisplayColumns VARCHAR(MAX) = '',
@GetAll BIT = 0
as   
  
	  
declare @tintOrder as tinyint                                  
declare @vcFormFieldName as varchar(50)                                  
declare @vcListItemType as varchar(3)                             
declare @vcListItemType1 as varchar(3)                                 
declare @vcAssociatedControlType varchar(20)                                  
declare @numListID AS numeric(9)                                  
declare @vcDbColumnName varchar(30)                                   
declare @ColumnSearch as varchar(10)
DECLARE @vcLookBackTableName VARCHAR(50)
DECLARE @bitIsSearchBizDoc BIT




IF CHARINDEX('OpportunityBizDocs', @WhereCondition) > 0
begin

	SET @bitIsSearchBizDoc = 1
	end
ELSE 
 BEGIN
	SET @bitIsSearchBizDoc = 0
	
 END
 --Added By Sachin Sadhu||Date:12thJune2014
  --Purpose:To add Functionaly :Search with Notes Field-OpportunityBizDocItems
  --Set Manually To fullfill "All" Selection in Search
 IF CHARINDEX('vcNotes', @WhereCondition) > 0
	BEGIN
			SET 	@bitIsSearchBizDoc = 1
	END
--End of script

if (@SortCharacter<>'0' and @SortCharacter<>'') set @ColumnSearch=@SortCharacter   
Create table #tempTable ( ID INT IDENTITY PRIMARY KEY,                                                                         
      numOppID varchar(15),
	  tintCRMType TINYINT,
	  numContactID NUMERIC(18,0),
	  numDivisionID NUMERIC(18,0),
      numOppBizDocID varchar(15)
 )  
  
declare @strSql as varchar(8000)
 set  @strSql='select OppMas.numOppId, DM.tintCRMType, OppMas.numDivisionID, ADC.numContactId '
 
IF @bitIsSearchBizDoc = 1 --Added to eliminate duplicate records 
	SET @strSql = @strSql + ' ,OpportunityBizDocs.numOppBizDocsId '
ELSE 
	SET @strSql = @strSql + ' ,0 numOppBizDocsId '

IF @SortColumnName ='CalAmount'
BEGIN
	ALTER TABLE #tempTable ADD CalAmount DECIMAL(20,5) ;
	SET @strSql = @strSql + ' ,[dbo].[getdealamount](OppMas.numOppId,Getutcdate(),0) as CalAmount'
END	

IF @SortColumnName ='monDealAmount'
BEGIN
	ALTER TABLE #tempTable ADD monDealAmount DECIMAL(20,5) ;

	SET @strSql = @strSql + ' ,ISNULL((SELECT SUM(ISNULL(BD.monDealAmount,0)) FROM   OpportunityBizDocs BD WHERE  BD.numOppId = OppMas.numOppId AND ISNULL(bitAuthoritativeBizDocs,0)=1),0) as monDealAmount'
END	

IF @SortColumnName ='monAmountPaid'
BEGIN
	ALTER TABLE #tempTable ADD monAmountPaid DECIMAL(20,5) ;

	SET @strSql = @strSql + ' ,ISNULL((SELECT SUM(ISNULL(BD.monAmountPaid,0)) FROM   OpportunityBizDocs BD WHERE  BD.numOppId = OppMas.numOppId AND ISNULL(bitAuthoritativeBizDocs,0)=1),0) AS monAmountPaid'
END	

SET @strSql = @strSql + ' from   
OpportunityMaster OppMas   
Join AdditionalContactsinformation ADC on OppMas.numContactId = ADC.numContactId  
join DivisionMaster DM on Dm.numDivisionId = OppMas.numDivisionId  
JOIN CompanyInfo C ON DM.numCompanyID = C.numCompanyId   
'  
IF @bitIsSearchBizDoc = 1 --Added to eliminate duplicate records 
BEGIN	
	IF CHARINDEX('vcNotes', @WhereCondition) > 0
		BEGIN
	  		SET @strSql = @strSql + ' left join OpportunityBizDocs on OppMas.numOppId = OpportunityBizDocs.numOppId left join OpportunityBizDocItems on OpportunityBizDocItems.numOppBizDocID=OpportunityBizDocs.numOppBizDocsId '
		END
	ELSE
		BEGIN
			SET @strSql = @strSql + ' left join OpportunityBizDocs on OppMas.numOppId = OpportunityBizDocs.numOppId '
		END


END
	
IF @SortColumnName ='numBillCountry' OR @SortColumnName ='numBillState'
     OR @ColumnName ='numBillCountry' OR @ColumnName ='numBillState'
BEGIN 	
	set @strSql= @strSql +' left Join AddressDetails AD1 on AD1.numRecordId= DM.numDivisionID and AD1.tintAddressOf=2 and AD1.tintAddressType=1 AND AD1.bitIsPrimary=1 and AD1.numDomainID= ADC.numDomainID '
END	
ELSE IF @SortColumnName ='numShipCountry' OR @SortColumnName ='numShipState'
     OR @ColumnName ='numShipCountry' OR @ColumnName ='numShipState'
BEGIN
	set @strSql= @strSql +' left Join AddressDetails AD2 on AD2.numRecordId= DM.numDivisionID and AD2.tintAddressOf=2 and AD2.tintAddressType=2 AND AD2.bitIsPrimary=1 and AD2.numDomainID= ADC.numDomainID '
END

   if (@SortColumnName<>'')                        
  begin                              
 select top 1 @vcListItemType1=vcListItemType from View_DynamicDefaultColumns where vcDbColumnName=@SortColumnName and numFormID=15 and numDomainId=@numDomainId
  if @vcListItemType1='LI'                       
  begin
			IF @SortColumnName ='numBillCountry'
			BEGIN
					set @strSql= @strSql +' left Join ListDetails L2 on L2.numListItemID=AD1.numCountry' 
			END
			ELSE IF @SortColumnName ='numShipCountry'
			BEGIN
					set @strSql= @strSql +' left Join ListDetails L2 on L2.numListItemID=AD2.numCountry' 
			END
		  ELSE
		  BEGIN                                    
				set @strSql= @strSql +' left Join ListDetails L2 on L2.numListItemID='+@SortColumnName                                  
		  END
  end  
 else if @vcListItemType1='S'                               
    begin           
		IF @SortColumnName ='numBillState'
		BEGIN
   			set @strSql= @strSql +' left join State S2 on S2.numStateID=AD1.numState'                            
		END
	    ELSE IF @SortColumnName ='numShipState'
	    BEGIN
   			set @strSql= @strSql +' left join State S2 on S2.numStateID=AD2.numState'                           
		END
    end                          
  end                              
  set @strSql=@strSql+' where OppMas.numDomainID  = '+convert(varchar(15),@numDomainID)+   @WhereCondition                                      
     
  
if (@ColumnName<>'' and  @ColumnSearch<>'')                             
begin                              
  if @vcListItemType='LI'                                   
    begin    
			IF @ColumnName ='numBillCountry'
			BEGIN
					set @strSql= @strSql +' left Join ListDetails L1 on L1.numListItemID=AD1.numCountry' 
			END
			ELSE IF @ColumnName ='numShipCountry'
			BEGIN
					set @strSql= @strSql +' left Join ListDetails L1 on L1.numListItemID=AD2.numCountry' 
			END
		  ELSE
		  BEGIN                                
				set @strSql= @strSql +' and L1.vcData like '''+@ColumnSearch +'%'''                                 
			END
		end                                  
 else if @vcListItemType='S'                               
    begin           
		IF @ColumnName ='numBillState'
		BEGIN
   			set @strSql= @strSql +' left join State S1 on S1.numStateID=AD1.numState'                            
		END
	    ELSE IF @ColumnName ='numShipState'
	    BEGIN
   			set @strSql= @strSql +' left join State S1 on S1.numStateID=AD2.numState'                           
		END
    end
    else set @strSql= @strSql +' and '+ 
				case when @ColumnName = 'numAssignedTo' then 'OppMas.'+@ColumnName 
				else @ColumnName end 
				+' like '''+@ColumnSearch  +'%'''                                
                              
end                              
   if (@SortColumnName<>'')                            
  begin     
                          
    if @vcListItemType1='LI'                                   
    begin        
      set @strSql= @strSql +' order by L2.vcData '+@columnSortOrder                                  
    end   
	else if @vcListItemType1='S'                               
		begin                                  
		  set @strSql= @strSql +' order by S2.vcState '+@columnSortOrder                              
		end                                
    ELSE
	BEGIN
		--SET @strSql  = REPLACE(@strSql,'distinct','')
		set @strSql= @strSql +' order by '+ @SortColumnName +' ' +@columnSortOrder
	END  
  end   
  
  
insert into #tempTable exec(@strSql)    
 print @strSql 
 print '===================================================='  
         
         --SELECT * FROM #tempTable               
set @strSql=''                                  
                                  
DECLARE @bitBillAddressJoinAdded AS BIT;
DECLARE @bitShipAddressJoinAdded AS BIT;
SET @bitBillAddressJoinAdded=0;
SET @bitShipAddressJoinAdded=0;
                      
set @tintOrder=0                                  
set @WhereCondition =''                               
set @strSql='select OppMas.numOppId, DM.tintCRMType, OppMas.numDivisionID, ADC.numContactId, OppMas.numRecOwner  '
IF @bitIsSearchBizDoc = 1 --Added to eliminate duplicate records 
BEGIN
	SET @strSql = @strSql + ' ,OpportunityBizDocs.numOppBizDocsId '
END

	DECLARE @TEMPSelectedColumns TABLE
	(
		numFieldID NUMERIC(18,0)
		,bitCustomField BIT
		,tintOrder INT
		,intColumnWidth FLOAT
	)
	DECLARE @vcLocationID VARCHAR(100) = '2,6'

	-- IF USER HAS SELECTED ITS OWN DISPLAY COLUMNS THEN USE IT
	IF LEN(ISNULL(@vcDisplayColumns,'')) > 0
	BEGIN
		DECLARE @TempIDs TABLE
		(
			ID INT IDENTITY(1,1),
			vcFieldID VARCHAR(300)
		)

		INSERT INTO @TempIDs
		(
			vcFieldID
		)
		SELECT 
			OutParam 
		FROM 
			SplitString(@vcDisplayColumns,',')

		INSERT INTO @TEMPSelectedColumns
		(
			numFieldID,
			bitCustomField,
			tintOrder,
			intColumnWidth
		)
		SELECT
			numFormFieldID
			,bitCustomField
			,(SELECT (ID-1) FROM @TempIDs T3 WHERE T3.vcFieldID = TEMP.vcFieldID)
			,0
		FROM
		(
			SELECT  
				numFieldID as numFormFieldID,
				0 bitCustomField,
				CONCAT(numFieldID,'~0') vcFieldID
			FROM    
				View_DynamicDefaultColumns
			WHERE   
				numFormID = 15
				AND bitInResults = 1
				AND bitDeleted = 0 
				AND numDomainID = @numDomainID
			UNION 
			SELECT  
				c.Fld_id AS numFormFieldID
				,1 bitCustomField
				,CONCAT(Fld_id,'~1') vcFieldID
			FROM    
				CFW_Fld_Master C 
			LEFT JOIN 
				CFW_Validation V ON V.numFieldID = C.Fld_id
			JOIN 
				CFW_Loc_Master L on C.GRP_ID=L.Loc_Id
			WHERE   
				C.numDomainID = @numDomainID
				AND GRP_ID IN (SELECT ID FROM dbo.SplitIDs(@vcLocationID,','))
		) TEMP
		WHERE
			vcFieldID IN (SELECT vcFieldID FROM @TempIDs)
	END

	-- IF USER SELECTED DISPLAY COLUMNS IS NOT AVAILABLE THAN GET COLUMNS CONFIGURED FROM SEARCH RESULT GRID
	IF (SELECT COUNT(*) FROM @TEMPSelectedColumns) = 0
	BEGIN
		INSERT INTO @TEMPSelectedColumns
		(
			numFieldID,
			bitCustomField,
			tintOrder,
			intColumnWidth
		)
		SELECT
			numFormFieldID
			,bitCustomField
			,tintOrder
			,intColumnWidth
		FROM
		(
			SELECT  
				A.numFormFieldID,
				0 bitCustomField,
				A.tintOrder,
				A.intColumnWidth
			FROM    AdvSerViewConf A
					JOIN View_DynamicDefaultColumns D ON D.numFieldID = A.numFormFieldID
			WHERE   A.numFormID = 15
					AND D.bitInResults = 1
					AND D.bitDeleted = 0
					AND D.numDomainID = @numDomainID 
					AND D.numFormID = 15
					AND A.numDomainID = @numDomainID
					AND A.numUserCntID=@numUserCntID
					AND ISNULL(A.bitCustom,0) = 0
			UNION
			SELECT  
				A.numFormFieldID,
				1 bitCustomField,
				A.tintOrder,
				A.intColumnWidth
			FROM    AdvSerViewConf A
					JOIN CFW_Fld_Master C ON C.Fld_id = A.numFormFieldID
			WHERE   A.numFormID = 15
					AND C.numDomainID = @numDomainID 
					AND A.numDomainID = @numDomainID
					AND A.numUserCntID=@numUserCntID
					AND ISNULL(A.bitCustom,0) = 1
		) T1
		ORDER BY
			 tintOrder
	END

	-- IF USER HASN'T CONFIGURED COLUMNS FROM SEARCH RESULT GRID THEN RETURN DEFAULT COLUMNS
	IF (SELECT COUNT(*) FROM @TEMPSelectedColumns) = 0
	BEGIN
		INSERT INTO @TEMPSelectedColumns
		(
			numFieldID,
			bitCustomField,
			tintOrder,
			intColumnWidth
		)
		SELECT  
			numFieldID,
			0,
			1,
			0
		FROM    
			View_DynamicDefaultColumns
		WHERE   
			numFormID = 15
			AND numDomainID = @numDomainID
			AND numFieldID = 96
	END

DECLARE @bitCustom BIT
DECLARE @numFieldGroupID AS INT
DECLARE @vcColumnName AS VARCHAR(200)
DECLARE @numFieldID AS NUMERIC(18,0)

	SELECT TOP 1
		@tintOrder=tintOrder,
		@numFieldID=numFormFieldID,
		@vcDbColumnName=vcDbColumnName,
		@vcFormFieldName=vcFieldName,
		@vcAssociatedControlType=vcAssociatedControlType,
		@vcListItemType=vcListItemType,
		@numListID=numListID,
		@vcLookBackTableName=vcLookBackTableName,
		@bitCustom=bitCustom,
		@numFieldGroupID=numGroupID
	FROM
	(
		SELECT
			T1.tintOrder+1 tintOrder,
			D.numFieldID AS numFormFieldID,
			vcDbColumnName,
			vcFieldName,
			vcAssociatedControlType,
			vcListItemType,
			numListID,
			vcLookBackTableName,
			0 AS bitCustom,
			0 AS numGroupID
		FROM 
			View_DynamicDefaultColumns D                               
		INNER JOIN
			@TEMPSelectedColumns T1
		ON
			D.numFieldID = T1.numFieldID                       
		WHERE 
			D.numDomainID = @numDomainID 
			AND D.numFormID = 15 
			AND ISNULL(T1.bitCustomField,0) = 0
		UNION
		SELECT
			T1.tintOrder+1 tintOrder,
			Fld_id AS numFormFieldID,
			CONCAT('Cust',Fld_id),
			Fld_label,
			c.Fld_type,
			'',
			c.numlistid,
			'',
			1 AS bitCustom,
			Grp_id
		FROM    
			CFW_Fld_Master C
		INNER JOIN
			@TEMPSelectedColumns T1
		ON
			C.Fld_id = T1.numFieldID
		WHERE   
			C.numDomainID = @numDomainID
			AND ISNULL(T1.bitCustomField,0) = 1
	) T1
	ORDER BY 
		tintOrder asc                                                             

while @tintOrder>0                                  
begin                                  
	IF @bitCustom = 0
	BEGIN
		DECLARE  @Prefix  AS VARCHAR(10)

		IF @vcLookBackTableName = 'OpportunityMaster'
			SET @PreFix = 'OppMas.'

	 SET @vcColumnName= @vcDbColumnName+'~'+ convert(varchar(10),@numFieldId) +'~0'
		if @vcAssociatedControlType='SelectBox'                                  
        begin                                  
                                                                              
			IF @vcDbColumnName = 'tintSource'
            BEGIN
              SET @strSql = @strSql
                                  + ',dbo.fn_GetOpportunitySourceValue(ISNULL(OppMas.tintSource,0),ISNULL(OppMas.tintSourceType,0),OppMas.numDomainID) '
                                  +' ['+ @vcColumnName+']'
				
            END
			ELSE IF @vcListItemType = 'U'
			BEGIN
				SET @strSql = @strSql + ',dbo.fn_GetContactName(' + @Prefix + @vcDbColumnName + ') [' + @vcColumnName + ']'
			END                                    
			ELSE if @vcListItemType='LI'                                   
			begin   
				IF @numListID=40--Country
				BEGIN
	  				set @strSql=@strSql+',L'+ convert(varchar(3),@tintOrder)+'.vcData'++' ['+ @vcColumnName+']'                              
				IF @vcDbColumnName ='numBillCountry'
				BEGIN
					IF @bitBillAddressJoinAdded=0 
					BEGIN
						set @WhereCondition= @WhereCondition +' left Join AddressDetails AD1 on AD1.numRecordId= DM.numDivisionID and AD1.tintAddressOf=2 and AD1.tintAddressType=1 AND AD1.bitIsPrimary=1 and AD1.numDomainID= ADC.numDomainID '
					
						SET @bitBillAddressJoinAdded=1;
					END
					set @WhereCondition= @WhereCondition +' left Join ListDetails L'+ convert(varchar(3),@tintOrder)+ ' on L'+convert(varchar(3),@tintOrder)+ '.numListItemID=AD1.numCountry'
				END
				ELSE IF @vcDbColumnName ='numShipCountry'
				BEGIN
					IF @bitShipAddressJoinAdded=0 
					BEGIN
						set @WhereCondition= @WhereCondition +' left Join AddressDetails AD2 on AD2.numRecordId= DM.numDivisionID and AD2.tintAddressOf=2 and AD2.tintAddressType=2 AND AD2.bitIsPrimary=1 and AD2.numDomainID= ADC.numDomainID '
					
						SET @bitShipAddressJoinAdded=1;
					END
					set @WhereCondition= @WhereCondition +' left Join ListDetails L'+ convert(varchar(3),@tintOrder)+ ' on L'+convert(varchar(3),@tintOrder)+ '.numListItemID=AD2.numCountry'
				END
			  END                        
			  ELSE
			  BEGIN                               
				set @strSql=@strSql+',L'+ convert(varchar(3),@tintOrder)+'.vcData'+' ['+ @vcColumnName+']'
				
				if @vcDbColumnName = 'numSalesOrPurType' AND @numListID=46
					set @WhereCondition= @WhereCondition +' left Join ListDetails L'+ convert(varchar(3),@tintOrder)+ ' on L'+convert(varchar(3),@tintOrder)+ '.numListItemID=OppMas.numSalesOrPurType and OppMas.tintOppType = 2 '                                 		
				else if @vcDbColumnName = 'numSalesOrPurType' AND @numListID=45
					set @WhereCondition= @WhereCondition +' left Join ListDetails L'+ convert(varchar(3),@tintOrder)+ ' on L'+convert(varchar(3),@tintOrder)+ '.numListItemID=OppMas.numSalesOrPurType and OppMas.tintOppType = 1 '                                 
				else
					set @WhereCondition= @WhereCondition +' left Join ListDetails L'+ convert(varchar(3),@tintOrder)+ ' on L'+convert(varchar(3),@tintOrder)+ '.numListItemID='+@vcDbColumnName                                  
			  END                                  
        END                      
		END 
		ELSE IF @vcAssociatedControlType='ListBox'   
		BEGIN
			if @vcListItemType='S'                               
			  begin                         
				set @strSql=@strSql+',S'+ convert(varchar(3),@tintOrder)+'.vcState'+' ['+ @vcColumnName+']'
				IF @vcDbColumnName ='numBillState'
				BEGIN
					IF @bitBillAddressJoinAdded=0 
					BEGIN
						set @WhereCondition= @WhereCondition +' left Join AddressDetails AD1 on AD1.numRecordId= DM.numDivisionID and AD1.tintAddressOf=2 and AD1.tintAddressType=1 AND AD1.bitIsPrimary=1 and AD1.numDomainID= ADC.numDomainID '
						SET @bitBillAddressJoinAdded=1;
					END
					set @WhereCondition= @WhereCondition +' left join State S'+ convert(varchar(3),@tintOrder)+ ' on S'+convert(varchar(3),@tintOrder)+ '.numStateID=AD1.numState'
				END
				ELSE IF @vcDbColumnName ='numShipState'
				BEGIN
					IF @bitShipAddressJoinAdded=0 
					BEGIN
						set @WhereCondition= @WhereCondition +' left Join AddressDetails AD2 on AD2.numRecordId= DM.numDivisionID and AD2.tintAddressOf=2 and AD2.tintAddressType=2 AND AD2.bitIsPrimary=1 and AD2.numDomainID= ADC.numDomainID '
						SET @bitShipAddressJoinAdded=1;
					END
					set @WhereCondition= @WhereCondition +' left join State S'+ convert(varchar(3),@tintOrder)+ ' on S'+convert(varchar(3),@tintOrder)+ '.numStateID=AD2.numState'
				END
			  end  
		END  
		else if @vcAssociatedControlType = 'CheckBox'           
		   begin            
               
			set @strSql= @strSql+',case when isnull('+ @vcDbColumnName +',0)=0 then ''No'' when isnull('+ @vcDbColumnName +',0)=1 then ''Yes'' end  ['+ @vcColumnName+']'              
 
		   end                                  
		 else 
				BEGIN
					IF @bitIsSearchBizDoc = 0
					BEGIN
						IF @vcLookBackTableName = 'OpportunityBizDocs' AND @vcDbColumnName != 'monDealAmount' AND @vcDbColumnName != 'monAmountPaid'
						  BEGIN
	  							SET @vcDbColumnName = '''''' --include OpportunityBizDocs tables column as blank string
						  END	
					END
	  
					set @strSql=@strSql+','+ 
						case  
							when @vcLookBackTableName = 'OpportunityMaster' AND @vcDbColumnName='monDealAmount' then 'OppMas.monDealAmount' 
							when @vcDbColumnName='numAge' then 'year(getutcdate())-year(bintDOB)'   
							when @vcDbColumnName='numDivisionID' then 'DM.numDivisionID' 
							when @vcDbColumnName='vcProgress' then  ' dbo.fn_OppTotalProgress(OppMas.numOppId)'
							when @vcDbColumnName='intPEstimatedCloseDate' or @vcDbColumnName='dtDateEntered' or @vcDbColumnName='bintShippedDate'  then  
									'dbo.FormatedDateFromDate('+@vcDbColumnName+','+convert(varchar(10),@numDomainId)+')'  
							WHEN @vcDbColumnName = 'CalAmount' THEN '[dbo].[getdealamount](OppMas.numOppId,Getutcdate(),0)'
							WHEN @vcLookBackTableName = 'OpportunityBizDocs' AND @vcDbColumnName = 'monDealAmount' THEN 'ISNULL((SELECT SUM(ISNULL(BD.monDealAmount,0)) FROM   OpportunityBizDocs BD WHERE  BD.numOppId = OppMas.numOppId AND ISNULL(bitAuthoritativeBizDocs,0)=1),0)'
							WHEN @vcLookBackTableName = 'OpportunityBizDocs' AND @vcDbColumnName = 'monAmountPaid' THEN 'ISNULL((SELECT SUM(ISNULL(BD.monAmountPaid,0)) FROM   OpportunityBizDocs BD WHERE  BD.numOppId = OppMas.numOppId AND ISNULL(bitAuthoritativeBizDocs,0)=1),0)'
							else @vcDbColumnName end+ ' ['+ @vcColumnName+']'
				END
    END
	ELSE IF @bitCustom = 1
	BEGIN
		--SET @vcColumnName= CONCAT(@vcFormFieldName,'~','Cust',@numFieldId)
			SET @vcColumnName= @vcDbColumnName +'~'+ convert(varchar(10),@numFieldId)+'~1'  
		IF @vcAssociatedControlType = 'TextBox' or @vcAssociatedControlType = 'TextArea'            
		BEGIN
			SET @strSql= @strSql+',CFW'+ convert(VARCHAR(3),@tintOrder)+'.Fld_Value  ['+ @vcColumnName +']'                   
				
			SET @WhereCondition= @WhereCondition 
								+' left Join CFW_Fld_Values_Opp CFW'+ convert(varchar(3),@tintOrder)
								+ ' on CFW' + convert(varchar(3),@tintOrder) 
								+ '.Fld_Id='+convert(varchar(10),@numFieldId) 
								+ 'and CFW' + convert(varchar(3),@tintOrder) 
								+ '.RecId=OppMas.numOppId '                                                         
		END   
		ELSE IF @vcAssociatedControlType = 'CheckBox'       
		BEGIN
			set @strSql= @strSql+',case when isnull(CFW' + CONVERT(VARCHAR(3), @tintOrder)+ '.Fld_Value,0)='''' then 0 else  isnull(CFW'
											+ CONVERT(VARCHAR(3), @tintOrder)+ '.Fld_Value,0) end ['+ @vcColumnName + ']'
 
			set @WhereCondition= @WhereCondition +' left Join CFW_Fld_Values_Opp CFW'+ convert(varchar(3),@tintOrder)+ '             
			on CFW'+ convert(varchar(3),@tintOrder)+ '.Fld_Id='+convert(varchar(10),@numFieldId) +'and CFW'+ convert(varchar(3),@tintOrder)+ '.RecId=OppMas.numOppId '                                                     
		END                
		ELSE IF @vcAssociatedControlType = 'DateField'            
		BEGIN
			set @strSql= @strSql+',dbo.FormatedDateFromDate(CFW'+ convert(varchar(3),@tintOrder)+'.Fld_Value,'+convert(varchar(10),@numDomainId)+')  ['+ @vcColumnName +']'                   
			set @WhereCondition= @WhereCondition +' left Join CFW_Fld_Values_Opp CFW'+ convert(varchar(3),@tintOrder)+ '                 
			on CFW'+ convert(varchar(3),@tintOrder)+ '.Fld_Id='+convert(varchar(10),@numFieldId) +'and CFW'+ convert(varchar(3),@tintOrder)+ '.RecId=OppMas.numOppId '
		END                
		ELSE IF @vcAssociatedControlType = 'SelectBox'             
		BEGIN
			set @vcDbColumnName = 'DCust'+Convert(varchar(10),@numFieldId)                
			set @strSql=@strSql+',L'+ convert(varchar(3),@tintOrder)+'.vcData'+' ['+ @vcColumnName +']'                                                          
			set @WhereCondition= @WhereCondition +' left Join CFW_Fld_Values_Opp CFW'+ convert(varchar(3),@tintOrder)+ '                 
				on CFW'+ convert(varchar(3),@tintOrder)+ '.Fld_Id='+convert(varchar(10),@numFieldId) +'and CFW'+ convert(varchar(3),@tintOrder)+ '.RecId=OppMas.numOppId '
			set @WhereCondition= @WhereCondition +' left Join ListDetails L'+ convert(varchar(3),@tintOrder)+ ' on L'+convert(varchar(3),@tintOrder)+ '.numListItemID=CFW'+ convert(varchar(3),@tintOrder)+'.Fld_Value'                
		END         
		ELSE IF @vcAssociatedControlType = 'CheckBoxList'
		BEGIN
			SET @strSql= @strSql+',(SELECT STUFF((SELECT CONCAT('', '', vcData) FROM ListDetails WHERE numListID= ' + CAST(@numListID AS varchar) + ' AND numlistitemid IN (SELECT ISNULL(Id,0) FROM dbo.SplitIDs(CFW'+ convert(VARCHAR(3),@tintOrder)+'.Fld_Value,'','')) FOR XML PATH('''')), 1, 1, ''''))  ['+ @vcColumnName +']'   

			SET @WhereCondition= @WhereCondition 
								+' left Join CFW_Fld_Values_Opp CFW'+ convert(varchar(3),@tintOrder)
								+ ' on CFW' + convert(varchar(3),@tintOrder) 
								+ '.Fld_Id='+convert(varchar(10),@numFieldId) 
								+ 'and CFW' + convert(varchar(3),@tintOrder) 
								+ '.RecId=OppMas.numOppId '
	END
	END            
                                          
	SELECT TOP 1
		@tintOrder=tintOrder,
		@numFieldID=numFormFieldID,
		@vcDbColumnName=vcDbColumnName,
		@vcFormFieldName=vcFieldName,
		@vcAssociatedControlType=vcAssociatedControlType,
		@vcListItemType=vcListItemType,
		@numListID=numListID,
		@vcLookBackTableName=vcLookBackTableName,
		@bitCustom=bitCustom,
		@numFieldGroupID=numGroupID
	FROM
	(
		SELECT
			T1.tintOrder+1 tintOrder,
			D.numFieldID AS numFormFieldID,
			vcDbColumnName,
			vcFieldName,
			vcAssociatedControlType,
			vcListItemType,
			numListID,
			vcLookBackTableName,
			0 AS bitCustom,
			0 AS numGroupID
		FROM 
			View_DynamicDefaultColumns D                               
		INNER JOIN
			@TEMPSelectedColumns T1
		ON
			D.numFieldID = T1.numFieldID                       
		WHERE 
			D.numDomainID = @numDomainID 
			AND D.numFormID = 15 
			AND ISNULL(T1.bitCustomField,0) = 0
			AND T1.tintOrder > @tintOrder-1
		UNION
		SELECT
			T1.tintOrder+1 tintOrder,
			Fld_id AS numFormFieldID,
			CONCAT('Cust',Fld_id),
			Fld_label,
			c.Fld_type,
			'',
			c.numlistid,
			'',
			1 AS bitCustom,
			Grp_id
		FROM    
			CFW_Fld_Master C
		INNER JOIN
			@TEMPSelectedColumns T1
		ON
			C.Fld_id = T1.numFieldID
		WHERE   
			C.numDomainID = @numDomainID
			AND ISNULL(T1.bitCustomField,0) = 1
			AND T1.tintOrder > @tintOrder-1
	) T1
	ORDER BY 
		tintOrder asc                                   

 if @@rowcount=0 set @tintOrder=0                                  
end   
	declare @firstRec as integer                                                                        
	declare @lastRec as integer                                                                        
	set @firstRec= (@CurrentPage-1) * @PageSize                          
	set @lastRec= (@CurrentPage*@PageSize+1)                                                                         
	set @TotRecs=(select count(*) from #tempTable)   


DECLARE @from VARCHAR(8000) 
set @from = ' from OpportunityMaster OppMas   
Join AdditionalContactsinformation ADC on OppMas.numContactId = ADC.numContactId  
join DivisionMaster DM on Dm.numDivisionId = OppMas.numDivisionId  
JOIN CompanyInfo C ON DM.numCompanyID = C.numCompanyId '

IF @bitIsSearchBizDoc = 1 --Added to eliminate duplicate records 
 --Added By Sachin Sadhu||Date:12thJune2014
  --Purpose:To add Functionaly :Search with Notes Field-OpportunityBizDocItems

	IF CHARINDEX('vcNotes', @WhereCondition) > 0
		BEGIN
	  		SET @from = @from + ' left join OpportunityBizDocs on OppMas.numOppId = OpportunityBizDocs.numOppId left join OpportunityBizDocItems on OpportunityBizDocItems.numOppBizDocID=OpportunityBizDocs.numOppBizDocsId '
		END
	ELSE
		BEGIN
			SET @from = @from + ' left join OpportunityBizDocs on OppMas.numOppId = OpportunityBizDocs.numOppId '
		END
	--End of Script

SET @strSql=@strSql+@from;

 
 
 if LEN(@strMassUpdate)>1
 begin     
	Declare @strReplace as varchar(2000)
    set @strReplace = case when @LookTable='OppMas' OR @LookTable='Opportunit' then 'OppMas.numOppID' ELSE '' end  + @from
   
    set @strMassUpdate=replace(@strMassUpdate,'@$replace',@strReplace)
    PRINT @strMassUpdate
   exec (@strMassUpdate)         
 end         




SET @strSql=@strSql+ @WhereCondition


	SET @strSql = @strSql +' join #tempTable T on T.numOppID=OppMas.numOppID WHERE 1=1 ' 
	
	IF ISNULL(@GetAll,0)=0
	BEGIN
		SET @strSql = @strSql +' AND ID > '+convert(varchar(10),@firstRec)+ ' and ID <'+ convert(varchar(10),@lastRec)
	END

	
 IF @bitIsSearchBizDoc = 1 
 BEGIN
 	SET @strSql = @strSql + '  and T.numOppBizDocID = OpportunityBizDocs.numOppBizDocsID '
 END
 
 SET @strSql = @strSql + ' order by ID'
print @strSql
exec(@strSql)                                                                     
drop table #tempTable

 

	SELECT 
 		tintOrder,
		numFormFieldID,
		vcDbColumnName,
		vcFieldName,
		vcAssociatedControlType,
		vcListItemType,
		numListID,
		vcLookBackTableName,
		numFieldID,
		intColumnWidth,
		bitCustom as bitCustomField,
		numGroupID,
		vcOrigDbColumnName,
		bitAllowSorting,
		bitAllowEdit,
		ListRelID,
		bitAllowFiltering,
		vcFieldDataType
	FROM
	(
		SELECT
			T1.tintOrder+1 tintOrder, 
			D.numFieldID numFormFieldID,
			vcDbColumnName,
			vcFieldName,
			vcAssociatedControlType,
			vcListItemType,
			numListID,
			vcLookBackTableName,
			D.numFieldID,
			ISNULL(T1.intColumnWidth,0) AS intColumnWidth,
			ISNULL(D.bitCustom,0) AS bitCustom,
			0 AS numGroupID
			,vcOrigDbColumnName
			,bitAllowSorting,
			bitAllowEdit,
			ListRelID,
			bitAllowFiltering,
			vcFieldDataType
		FROM 
			View_DynamicDefaultColumns D                               
		INNER JOIN
			@TEMPSelectedColumns T1
		ON
			D.numFieldID = T1.numFieldID
		WHERE 
			D.numDomainID = @numDomainID 
			AND D.numFormID = 15 
			AND ISNULL(T1.bitCustomField,0) = 0
			AND T1.tintOrder > @tintOrder-1
		UNION
		SELECT
			T1.tintOrder+1 tintOrder,
			Fld_id numFormFieldID,
			CONCAT('Cust',Fld_id),
			Fld_label,
			c.Fld_type,
			'',
			c.numlistid,
			'',
			Fld_id,
			ISNULL(T1.intColumnWidth,0) AS intColumnWidth,
			1 AS bitCustom,
			Grp_id
			,'',
			null,
			null,
			null,
			null,
			''
		FROM    
			CFW_Fld_Master C 
		INNER JOIN
			@TEMPSelectedColumns T1
		ON
			C.Fld_id = T1.numFieldID
		WHERE   
			C.numDomainID = @numDomainID
			AND ISNULL(T1.bitCustomField,0) = 1
			AND T1.tintOrder > @tintOrder-1
	) T1
	ORDER BY 
		tintOrder asc  

--Left Join OpportunityItems OppItems on OppItems.numOppId = OppMas.numOppId   
--left join Item on Item.numItemCode = OppItems.numItemCode  
--left join WareHouseItems on Item.numItemCode = WareHouseItems.numItemId  
--left Join WareHouses WareHouse on WareHouse.numWareHouseId =WareHouseItems.numWareHouseId  
--left Join WareHouseItmsDTL WareHouseItemDTL on WareHouseItemDTL.numWareHouseItemID = WareHouse.numWareHouseId
GO
