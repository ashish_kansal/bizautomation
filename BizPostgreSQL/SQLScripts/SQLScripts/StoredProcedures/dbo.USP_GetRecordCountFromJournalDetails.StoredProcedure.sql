/****** Object:  StoredProcedure [dbo].[USP_GetRecordCountFromJournalDetails]    Script Date: 07/26/2008 16:18:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created By Siva                                                                                                                              
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getrecordcountfromjournaldetails')
DROP PROCEDURE usp_getrecordcountfromjournaldetails
GO
CREATE PROCEDURE [dbo].[USP_GetRecordCountFromJournalDetails]                    
 @numChartAcntId as numeric(9)=0,                      
 @numDomainId as numeric(9)=0                                                                                                                                        
                                                                                                                            
As                                                                                                                                          
Begin                                                                                                                                     
	Select  Count(*) From General_Journal_Header  GJH                                          
	Inner Join  General_Journal_Details GJD on GJH.numJournal_Id=GJD.numJournalId Where (GJD.numChartAcntId=@numChartAcntId or GJH.numChartAcntId=@numChartAcntId)
	And  GJD.numDomainId=@numDomainId
End
GO
