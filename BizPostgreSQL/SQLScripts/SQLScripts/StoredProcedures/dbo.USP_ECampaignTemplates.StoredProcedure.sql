/****** Object:  StoredProcedure [dbo].[USP_ECampaignTemplates]    Script Date: 07/26/2008 16:15:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_ecampaigntemplates')
DROP PROCEDURE usp_ecampaigntemplates
GO
CREATE PROCEDURE [dbo].[USP_ECampaignTemplates]  
@numDomainID as numeric(9)=0 
as  
  
select numECampaignID,vcECampName 
from ECampaign  where numDomainID=@numDomainID
GO
