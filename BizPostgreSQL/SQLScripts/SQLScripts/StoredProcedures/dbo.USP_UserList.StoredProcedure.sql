/****** Object:  StoredProcedure [dbo].[USP_UserList]    Script Date: 07/26/2008 16:21:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj    
--Modified By Sachin Sadhu
--Worked on AND-OR condition       
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_userlist')
DROP PROCEDURE usp_userlist
GO
CREATE PROCEDURE [dbo].[USP_UserList]
(
	 @numDomainID NUMERIC(18,0),             
	 @CurrentPage INT,                  
	 @PageSize INT,                
	 @columnName VARCHAR(100),                  
	 @columnSortOrder VARCHAR(100),
	 @vcCustomSearch VARCHAR(MAX)
)                     
AS                  
BEGIN
	DECLARE @strSql AS VARCHAR(8000)                  
	SET @strSql=CONCAT('SELECT 
							COUNT(*) OVER() numTotalRecords
							,numUserID
							,ISNULL(vcEmailID,''-'') as vcEmailID
							,ISNULL(vcEmailAlias,'''') as vcEmailAlias
							,ISNULL(vcEmailAliasPassword,'''') vcEmailAliasPassword
							,CONCAT(''<a onclick="return fnEditUser('',numUserID,'')">'',ISNULL(ADC.vcfirstname,''''),'' '',ISNULL(ADC.vclastname,''''),''</a>'') AS Name
							,ISNULL(vcGroupName,''-'') AS vcGroupName
							,vcDomainName
							,(CASE WHEN bitActivateFlag=1 THEN ''Active'' ELSE ''Suspended'' END) AS [Active]
							,ISNULL(bitActivateFlag,0) bitActivateFlag
							,numDefaultWarehouse
							,ISNULL((SELECT COUNT(*) FROM UserAccessedDTL UAD WHERE UAD.numDomainID=',@numDomainID,' AND UAD.numDivisionID=Div.numDivisionID AND UAD.numContactID=ADC.numContactID),0) intLoginCount      
						FROM 
							UserMaster                    
						JOIN 
							Domain                      
						ON 
							UserMaster.numDomainID =  Domain.numDomainID 
						INNER JOIN 
							DivisionMaster Div 
						ON 
							Domain.numDivisionID=Div.numDivisionID
						INNER JOIN 
							AdditionalContactsInformation ADC                    
						ON 
							ADC.numContactid=UserMaster.numUserDetailId                   
						LEFT JOIN 
							AuthenticationGroupMaster GM                   
						ON 
							Gm.numGroupID= UserMaster.numGroupID              
						WHERE 
							UserMaster.numDomainID=',@numDomainID)       

	IF LEN(ISNULL(@vcCustomSearch,'')) > 0
	BEGIN
		SET @strSql = CONCAT(@strSql,' AND ',@vcCustomSearch)
	END	
						
	IF ISNULL(@columnName,'') <> '' 
	BEGIN
		IF @columnName = 'UserName'
		BEGIN
			SET @columnName = 'CONCAT(ADC.vcfirstname,'' '',ADC.vcLastName)'
		END
		ELSE IF @columnName = 'Email'
		BEGIN
			SET @columnName = 'UserMaster.vcEmailID'
		END
		ELSE IF @columnName = 'LoginActivity'
		BEGIN
			SET @columnName = CONCAT('ISNULL((SELECT COUNT(*) FROM UserAccessedDTL UAD WHERE UAD.numDomainID=',@numDomainID,' AND UAD.numDivisionID=Div.numDivisionID AND UAD.numContactID=ADC.numContactID),0)')
		END

		SET @strSql = CONCAT(@strSql,' ORDER BY ',@columnName,' ',@columnSortOrder)
	END
	ELSE
	BEGIN
		SET @strSql = CONCAT(@strSql,' ORDER BY CONCAT(ADC.vcfirstname,'' '',ADC.vcLastName) ASC')
	END
   
	SET @strSql = CONCAT(@strSql,' OFFSET ',(@CurrentPage-1) * @PageSize,' ROWS FETCH NEXT ',@PageSize,' ROWS ONLY')

	EXEC (@strSql)
END
GO
