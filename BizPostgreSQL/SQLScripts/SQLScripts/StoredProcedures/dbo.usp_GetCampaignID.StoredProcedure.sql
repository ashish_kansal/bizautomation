/****** Object:  StoredProcedure [dbo].[usp_GetCampaignID]    Script Date: 07/26/2008 16:16:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getcampaignid')
DROP PROCEDURE usp_getcampaignid
GO
CREATE PROCEDURE [dbo].[usp_GetCampaignID] 
	@numCreatedBy numeric(9)=0,
	@numDomainID numeric(9)=0   
--
AS
BEGIN
	SELECT 
		numCreatedBy
		
	FROM 	
		Campaign
	WHERE numCreatedBy=@numCreatedBy
	and numDomainID=@numDomainID
		 
	
END
GO
