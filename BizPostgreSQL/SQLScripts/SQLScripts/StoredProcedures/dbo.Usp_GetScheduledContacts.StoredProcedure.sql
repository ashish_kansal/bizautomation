/****** Object:  StoredProcedure [dbo].[Usp_GetScheduledContacts]    Script Date: 07/26/2008 16:18:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getscheduledcontacts')
DROP PROCEDURE usp_getscheduledcontacts
GO
CREATE PROCEDURE [dbo].[Usp_GetScheduledContacts]  
  
@numDomainId as numeric(9),  
@numScheduleid as numeric(9)  
as  
select CRS.numContactId,vcEmail from CustRptSchContacts CRS  
join AdditionalContactsInformation AC on   CRS.numContactId =  AC.numContactId  
 where  
 numScheduleid = @numScheduleid and   
AC.numDomainId = @numDomainId
GO
