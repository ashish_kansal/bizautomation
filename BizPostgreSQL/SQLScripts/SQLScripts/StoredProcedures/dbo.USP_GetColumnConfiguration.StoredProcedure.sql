/****** Object:  StoredProcedure [dbo].[USP_GetColumnConfiguration]    Script Date: 07/26/2008 16:16:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getcolumnconfiguration')
DROP PROCEDURE usp_getcolumnconfiguration
GO
CREATE PROCEDURE [dbo].[USP_GetColumnConfiguration]        
 @numDomainID Numeric(9),          
 @numUserCntId Numeric(9),        
 @FormId tinyint ,      
@numtype numeric(9)            
AS                    
             
          
          
          
select numFormFieldID,vcFormFieldName from DynamicFormFieldMaster where numFormID=@FormId     
order by vcFormFieldName       
  
select A.numFormFieldID,vcFieldName as vcFormFieldName,vcDbColumnName from [InitialListColumnConf] A           
join View_DynamicDefaultColumns D          
on D.numFieldID=A.numFormFieldID          
where D.numDomainID=@numDomainID and D.numDomainID=@numDomainID and a.numFormID=@FormId  and numtype=@numtype      
and numUserCntId=@numUserCntId and A.numDomainID=@numDomainID  order by A.tintOrder
GO
