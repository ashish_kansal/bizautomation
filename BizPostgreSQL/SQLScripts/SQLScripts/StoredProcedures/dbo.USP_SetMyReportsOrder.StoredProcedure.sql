/****** Object:  StoredProcedure [dbo].[USP_SetMyReportsOrder]    Script Date: 07/26/2008 16:21:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj  
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_setmyreportsorder')
DROP PROCEDURE usp_setmyreportsorder
GO
CREATE PROCEDURE [dbo].[USP_SetMyReportsOrder]    
@numUserCntID as numeric(9)=0,    
@strLinks as text    
as    
delete from MyReportList where numUserCntID=@numUserCntID    
    
DECLARE @hDoc int        
 EXEC sp_xml_preparedocument @hDoc OUTPUT, @strLinks        
        
 insert into MyReportList        
  (numUserCntID,numRPTId,tintRptSequence,tintType)        
         
 select @numUserCntID,X.RPTID,x.[Sequence],[type] from(        
 SELECT *FROM OPENXML (@hDoc,'/NewDataSet/Table',2)        
 WITH  (        
  RPTID numeric(9),        
  [Sequence] tinyint,
  [Type]    tinyint
  ))X
GO
