/****** Object:  StoredProcedure [dbo].[usp_GetLeadDetailsDTLPL]    Script Date: 07/26/2008 16:17:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getleaddetailsdtlpl')
DROP PROCEDURE usp_getleaddetailsdtlpl
GO
CREATE PROCEDURE [dbo].[usp_GetLeadDetailsDTLPL]                                                                                                    
 @numContactID numeric,                  
 @numDomainID numeric ,  
@ClientTimeZoneOffset  int                                                           
AS                                                              
BEGIN                                                              
 SELECT                                                               
  cmp.vcCompanyName,                                                              
  DM.vcDivisionName,                                                              
  ADC.vcFirstName+' '+ADC.vcLastName as Name,                                                              
  cmp.vcWebSite,                                                              
  ADC.vcEmail,               
-- case when DM.vcBillStreet is null then '' when DM.vcBillStreet = ''then '' else vcBillStreet end +           
-- case when DM.vcBillCity is null then '' when DM.vcBillCity ='' then '' else DM.vcBillCity end +           
-- case when dbo.fn_GetState(DM.vcBilState) is null then '' when dbo.fn_GetState(DM.vcBilState) ='' then '' else ','+ dbo.fn_GetState(DM.vcBilState)end +           
-- case when DM.vcBillPostCode is null then '' when DM.vcBillPostCode ='' then '' else ','+DM.vcBillPostCode end +           
-- case when dbo.fn_GetListName(DM.vcBillCountry,0) is null then '' when dbo.fn_GetListName(DM.vcBillCountry,0) ='' then '' else ','+dbo.fn_GetListName(DM.vcBillCountry,0) end  as address,        
    dbo.getCompanyAddress(dm.numdivisionId,1,dm.numDomainID) as address,           
AD.vcStreet,AD.numCountry AS vccountry,
  (select vcdata from listdetails where numListItemId=DM.numFollowUpStatus) as numFollowUpStatus,                      
   (select vcdata from listdetails where numListItemId=DM.numTerID) as numTerID,        
  (select vcdata from listdetails where numListItemId=AD.numState) as vcState,                                                              
  (select vcdata from listdetails where numListItemId=ADC.vcPosition) as vcPosition,                                                              
  ADC.numPhone+' '+ADC.numPhoneExtension as phone,                                                              
  (select vcdata from listdetails where numListItemId=cmp.numNoOfEmployeesId) as numNoOfEmployeesId,                                
                                                   
  (select vcdata from listdetails where numListItemId=cmp.numAnnualRevID)as numAnnualRevID,                                                              
  (select vcdata from listdetails where numListItemId=cmp.vcHow)as  vcHow,                                                              
                                                              
  (select vcdata from listdetails where numListItemId=cmp.vcProfile) as  vcProfile,                                                              
  ADC.txtNotes,                                                            
  DM.numCreatedBy,                                                   
  (select vcGrpName from groups where numGrpID=dm.numgrpid) as numgrpid ,   dm.numgrpid as grpid,                                      
  cmp.txtComments,                                           
 (select vcdata from listdetails where numListItemId=cmp.numCompanyType) as numCompanyType ,cmp.numCompanyType as numCmptype,                                      
  vcTitle,                                      
  vcComPhone+' '+vcComFax as vcComPhone,                                                
  ISNULL(dbo.fn_GetContactName(DM.numCreatedBy), '&nbsp;&nbsp;-')+''+convert(varchar(20),dateadd(minute,-@ClientTimeZoneOffset,DM.bintCreatedDate)) AS vcCreatedBy,                                                          DM.numModifiedBy,                                                      
  ISNULL(dbo.fn_GetContactName(DM.numModifiedBy), '&nbsp;&nbsp;-')+' '+ convert(varchar(20),dateadd(minute,-@ClientTimeZoneOffset,DM.bintModifiedDate)) AS vcModifiedBy,                                                                         
  DM.numRecOwner,                          
  ISNULL(dbo.fn_GetContactName(DM.numRecOwner), '&nbsp;&nbsp;-') AS vcRecordOwner,          
  numAssignedBy,                            
 (select A.vcFirstName+' '+A.vcLastName                       
 from AdditionalContactsInformation A                         
 join DivisionMaster D                        
 on D.numDivisionID=A.numDivisionID                                     
 where A.numContactID=DM.numAssignedTo) as numAssignedTo,                  
  (select vcdata from listdetails where numListItemId=numCampaignID) as numCampaignID                                                          
 FROM                                                    
  CompanyInfo cmp join DivisionMaster DM                                                               
  on cmp.numCompanyID=DM.numCompanyID                                               
  join  AdditionalContactsInformation ADC                                                                 
   on  DM.numDivisionID=ADC.numDivisionID                                    
   LEFT JOIN dbo.AddressDetails AD ON AD.numRecordID =ADC.numContactId
                                               AND AD.tintAddressOf = 1
                                               AND AD.tintAddressType = 0
                                               AND AD.bitIsPrimary = 1
                                               AND AD.numDomainID = ADC.numDomainID                            
 WHERE                                                                                
  DM.tintCRMType = 0                                                                                           
  AND ADC.numContactId = @numContactID and ADC.numDomainID=@numDomainID                                                             
END
GO
