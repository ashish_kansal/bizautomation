/****** Object:  StoredProcedure [dbo].[USP_ExtranetLogin]    Script Date: 07/26/2008 16:15:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--exec USP_ExtranetLogin @Email = 'john@jinco.com', @Password = '12345' ,@numDomainID=72
--Create By Anoop jayaraj                                          
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_extranetlogin')
DROP PROCEDURE usp_extranetlogin
GO
CREATE PROCEDURE [dbo].[USP_ExtranetLogin]                                              
@Email as varchar(100),
@Password as varchar(100),
@numDomainID as numeric(9)                                             
as                                              
declare @numContactId as numeric(9)                                              
declare @numDivisionId as numeric(9)                                              
declare @numcompanyID as numeric(9)                                              
declare @numGroupID as numeric(9)                                              
declare @tintCRMType as numeric(9)                                                           
declare @vcCompanyName as varchar(100)                                          
declare @AccOwner as numeric(9)                                        
declare @AccOwnerName as varchar(100)                                       
declare @numExtranetDtlID as varchar(15)                                             
declare @numNoOfTimes as varchar(15)                        
declare @numPartnerGroup as numeric(9)                        
declare @bitPartnerAccess as bit                        
declare @numRelationShip as numeric(9)                        
declare @numProfileID as numeric(9)
declare @numContactType as numeric(9)                      
--declare @vcHomePage as varchar(1000)                     
declare @UserEmail as varchar(100)                   
declare @ContactName as varchar(100)         
declare @vcFirstName AS VARCHAR(100)
declare @vcLastName AS VARCHAR(100)                  
declare @count as tinyint       

                
select @count=count(*)  from AdditionalContactsInformation A                                  
join ExtranetAccountsDtl E                                  
on A.numContactID=E.numContactID                                  
where (E.vcUserName=@Email OR vcemail=@Email) and (vcPassword=@Password OR @Password='EXISTINGUSERGUESTLOGIN') and tintAccessAllowed=1 and A.numDomainID= @numDomainID
print  @count                                 
if @count=1             ---checking whether an user exists with the username and password.Multiple results will be rejected                                 
begin                                         
 select @numContactId=A.numContactId,@ContactName=vcFirstName+' '+vcLastName,@numDivisionId=numDivisionId,@numContactType=numContactType,@UserEmail=isnull(vcEmail,''),@vcFirstName=ISNULL(vcFirstName,''),@vcLastName=ISNULL(vcLastName,'') from AdditionalContactsInformation A                                  
 join ExtranetAccountsDtl E                                  
 on A.numContactID=E.numContactID                                  
 where (E.vcUserName=@Email OR vcemail=@Email) and (vcPassword=@Password OR @Password='EXISTINGUSERGUESTLOGIN') AND E.numDomainID=@numDomainID AND E.tintAccessAllowed=1
                                  
select @numcompanyID=numCompanyID,@tintCRMType=tintCRMType,@AccOwner=numrecowner from divisionmaster where numDivisionID=@numDivisionId                                              
                                              
select @numDomainID=numDomainID,@vcCompanyName=vcCompanyName ,@numRelationShip=numCompanyType,@numProfileID=[vcProfile] from companyInfo where numCompanyID=@numcompanyID                                              
select @AccOwnerName=isnull(vcFirstName,'')+' '+isnull(vcLastName,'') from AdditionalContactsInformation where numContactID=@AccOwner                       
                      
--select @vcHomePage=vcHomePage from HomePage where numDomainID=@numDomainID and numRelationship=@numRelationShip and numContactType=@numContactType                                         
select @numNoOfTimes=numNoOfTimes,@bitPartnerAccess=bitPartnerAccess,@numPartnerGroup=numPartnerGroupID,@numExtranetDtlID=numExtranetDtlID,@numGroupID=numGroupID from ExtarnetAccounts hdr                                              
join ExtranetAccountsDtl Dtl                                              
on hdr.numExtranetID=Dtl.numExtranetID where numDivisionId=@numDivisionId and tintAccessAllowed=1 and numContactID=@numContactId and vcPassword=@Password            
if @bitPartnerAccess=0 set @numPartnerGroup=0                             


              
select 1 ,@numContactId,@numDivisionId,@numcompanyID,@numGroupID,@tintCRMType,@numDomainID,@vcCompanyName,@AccOwner,@AccOwnerName,isnull(@numPartnerGroup,0),isnull(@numRelationShip,0) ,/* isnull(@vcHomePage,'')*/ '',              
@UserEmail ,@ContactName ContactName,isnull(@bitPartnerAccess,0),               
tintCustomPagingRows,               
vcDateFormat,               
numDefCountry,               
tintAssignToCriteria,               
bitIntmedPage,            
isnull(vcPortalLogo,'') vcPortalLogo,               
tintFiscalStartMonth,            
 dateadd(day,-sintStartDate,getutcdate()) as StartDate,                    
 dateadd(day,sintNoofDaysInterval,dateadd(day,-sintStartDate,getutcdate())) as EndDate,  
case when bitPSMTPServer= 1 then vcPSMTPServer else '' end as vcSMTPServer ,     
case when bitPSMTPServer= 1 then isnull(numPSMTPPort,0) else 0 end as numSMTPPort 
,isnull(bitPSMTPAuth,0) bitSMTPAuth  
,isnull([vcPSmtpPassword],'')vcSmtpPassword  
,isnull(bitPSMTPSSL,0) bitSMTPSSL   ,isnull(bitPSMTPServer,0) bitSMTPServer  ,isnull(vcPSMTPUserName,'') vcSMTPUserName ,
isnull(numCurrencyID,0) numCurrencyID, isnull(vcCurrency ,'') vcCurrency,vcDomainName,
isnull(@numProfileID,0) numProfileID,
ISNULL(bitCustomizePortal,0) bitCustomizePortal,
ISNULL(vcPortalName,'') vcPortalName,
@vcFirstName as vcFirstName,
@vcLastName as vcLastName
from Domain              
where numDomainID=@numDomainID                                     
                   
IF @Password <> 'EXISTINGUSERGUESTLOGIN'                                   
BEGIN
set @numNoOfTimes=convert(numeric,isnull(@numNoOfTimes,0))+1                                    
update ExtranetAccountsDtl set bintLastLoggedIn=getutcdate(),numNoOfTimes=@numNoOfTimes  where numExtranetDtlID=@numExtranetDtlID                                   
END                              
end                                           
select @count
GO
