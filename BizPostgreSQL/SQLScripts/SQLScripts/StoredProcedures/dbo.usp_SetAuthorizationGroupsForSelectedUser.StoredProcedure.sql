/****** Object:  StoredProcedure [dbo].[usp_SetAuthorizationGroupsForSelectedUser]    Script Date: 07/26/2008 16:21:15 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_setauthorizationgroupsforselecteduser')
DROP PROCEDURE usp_setauthorizationgroupsforselecteduser
GO
CREATE PROCEDURE [dbo].[usp_SetAuthorizationGroupsForSelectedUser]
	@numUserID NUMERIC(9),
	@numGroupID NUMERIC(9) = 0   
--
AS
	IF @numGroupID = 0	-- Signifies that all groups for the selected user be removed
		BEGIN
			DELETE FROM UserGroups
			WHERE numUserID=@numUserID
		END
	ELSE	-- Signifies addition of  groups for the selected user
		BEGIN
			INSERT INTO UserGroups(numUserID, numGroupID)
			VALUES (@numUserID, @numGroupID)
		END
GO
