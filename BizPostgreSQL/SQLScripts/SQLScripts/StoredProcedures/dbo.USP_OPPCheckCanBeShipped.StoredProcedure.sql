/****** Object:  StoredProcedure [dbo].[USP_OPPCheckCanBeShipped]    Script Date: 07/26/2008 16:20:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--exec USP_OPPCheckCanBeShipped 5853,1  
--created by anoop jayaraj              
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_oppcheckcanbeshipped')
DROP PROCEDURE usp_oppcheckcanbeshipped
GO
CREATE PROCEDURE [dbo].[USP_OPPCheckCanBeShipped]              
@intOpportunityId as numeric(9),
@bitCheck AS BIT=0
AS              
BEGIN         
	CREATE TABLE #tempItem(vcItemName VARCHAR(300))   

	---REVERTING BACK TO PREVIOUS STATE IF DEAL IS BEING EDITED AFTER IT IS CLOSED
	DECLARE @OppType AS VARCHAR(2)
	DECLARE @vcItemName AS VARCHAR(300)
	DECLARE @vcKitItemName AS VARCHAR(300)
	DECLARE @Sel AS TINYINT   
	DECLARE @itemcode AS NUMERIC     
	DECLARE @numWareHouseItemID AS NUMERIC                                 
	DECLARE @numUnits AS FLOAT                                            
	DECLARE @numQtyShipped AS FLOAT
	DECLARE @numAllocation AS FLOAT                                                                                                                          
	DECLARE @numoppitemtCode AS NUMERIC(9)
	DECLARE @Kit AS BIT                   
	DECLARE @numQtyReceived AS FLOAT
	DECLARE @bitAsset as BIT

	SET @Sel=0          
	SELECT @OppType=tintOppType from OpportunityMaster where numOppId=@intOpportunityId
                                            
	SELECT TOP 1 
		@numoppitemtCode=numoppitemtCode,
		@itemcode=OI.numItemCode,
		@numUnits=ISNULL(numUnitHour,0),
		@numQtyShipped=ISNULL(numQtyShipped,0),
		@numQtyReceived=ISNULL(numUnitHourReceived,0),
		@numWareHouseItemID=numWarehouseItmsID,
		@bitAsset=ISNULL(I.bitAsset,0),
		@Kit= (CASE WHEN bitKitParent=1 AND bitAssembly=1 THEN 0 WHEN bitKitParent=1 THEN 1 ELSE 0 END),
		@vcItemName=OI.vcItemName 
	FROM 
		OpportunityItems OI                                            
	JOIN 
		Item I                                            
	ON 
		OI.numItemCode=I.numItemCode 
		AND numOppId=@intOpportunityId 
		AND (bitDropShip=0 OR bitDropShip IS NULL)                                        
	WHERE 
		charitemtype='P'  
	ORDER BY 
		OI.numoppitemtCode        
		                                      
	WHILE @numoppitemtCode>0                               
	BEGIN  
		IF @OppType=1 AND @Kit = 1
		BEGIN
			DECLARE @TEMPTABLE TABLE
			(
				numItemCode NUMERIC(18,0),
				numParentItemID NUMERIC(18,0),
				numOppChildItemID NUMERIC(18,0),
				vcItemName VARCHAR(300),
				bitKitParent BIT,
				numWarehouseItmsID NUMERIC(18,0),
				numQtyItemsReq FLOAT,
				numAllocation FLOAT
			)

			INSERT INTO 
				@TEMPTABLE
			SELECT
				OKI.numChildItemID,
				0,
				OKI.numOppChildItemID,
				I.vcItemName,
				I.bitKitParent,
				OKI.numWareHouseItemId,
				OKI.numQtyItemsReq_Orig * (ISNULL(OI.numUnitHour,0) - ISNULL(OI.numQtyShipped,0)),
				ISNULL(WI.numAllocation,0)
			FROM
				OpportunityKitItems OKI
			INNER JOIN
				WareHouseItems WI
			ON
				OKI.numWareHouseItemId = WI.numWareHouseItemID
			INNER JOIN
				OpportunityItems OI
			ON
				OKI.numOppItemID = OI.numoppitemtCode
			INNER JOIN
				Item I
			ON
				OKI.numChildItemID = I.numItemCode
			WHERE
				OKI.numOppID = @intOpportunityId
				AND OKI.numOppItemID = @numoppitemtCode
				AND OI.numoppitemtCode = @numoppitemtCode  

			INSERT INTO
				@TEMPTABLE
			SELECT
				OKCI.numItemID,
				t1.numItemCode,
				t1.numOppChildItemID,
				I.vcItemName,
				I.bitKitParent,
				OKCI.numWareHouseItemId,
				OKCI.numQtyItemsReq_Orig * t1.numQtyItemsReq,
				ISNULL(WI.numAllocation,0)
			FROM
				OpportunityKitChildItems OKCI
			INNER JOIN
				WareHouseItems WI
			ON
				OKCI.numWareHouseItemId = WI.numWareHouseItemID
			INNER JOIN
				@TEMPTABLE t1
			ON
				OKCI.numOppChildItemID = t1.numOppChildItemID
			INNER JOIN
				Item I
			ON
				OKCI.numItemID = I.numItemCode
			WHERE
				OKCI.numOppID = @intOpportunityId
				AND OKCI.numOppItemID = @numoppitemtCode
	
			IF (SELECT COUNT(*) FROM @TEMPTABLE WHERE ISNULL(bitKitParent,0) = 0 AND ISNULL(numQtyItemsReq,0) > ISNULL(numAllocation,0)) > 0
			BEGIN
				INSERT INTO #tempItem values (@vcItemName)
			END    
        END
		ELSE
		BEGIN
			SELECT                                          
				@numAllocation=isnull(numAllocation,0)                                           
			FROM 
				WareHouseItems 
			WHERE 
				numWareHouseItemID=@numWareHouseItemID  
				                                            
			IF @OppType=1                                              
			BEGIN          
				/*below is added to be validated from Sales fulfillment*/
				IF @bitCheck =1
				BEGIN
					IF @numUnits-@numQtyShipped>0
					BEGIN  
						IF (@bitAsset=0 )
						BEGIN
							INSERT INTO #tempItem values (@vcItemName)     
						END              
					 END            	
				END
				 
				SET @numUnits = @numUnits - @numQtyShipped
				
				IF @numUnits > @numAllocation   
				BEGIN  
					If (@bitAsset=0 )
					BEGIN
						INSERT INTO #tempItem VALUES (@vcItemName)  
					END                                                             
				END            
			END    
			ELSE IF @OppType=2
			BEGIN
				IF @bitCheck =1
				BEGIN
   					IF @numUnits-@numQtyReceived>0
					BEGIN  
						INSERT INTO #tempItem VALUES (@vcItemName)                              
					END  
			   END
			END
		END
                           
		SELECT TOP 1 
			@numoppitemtCode=numoppitemtCode,
			@itemcode=OI.numItemCode,
			@numUnits=ISNULL(numUnitHour,0),
			@numQtyShipped=ISNULL(numQtyShipped,0),
			@numQtyReceived=ISNULL(numUnitHourReceived,0),
			@numWareHouseItemID=numWarehouseItmsID,@Kit= (case when bitKitParent=1 and bitAssembly=1 then 0 when bitKitParent=1 then 1 else 0 end),@vcItemName=OI.vcItemName from OpportunityItems OI                                            
		JOIN 
			Item I                                            
		ON 
			OI.numItemCode=I.numItemCode and numOppId=@intOpportunityId                                          
		WHERE 
			charitemtype='P' 
			AND OI.numoppitemtCode>@numoppitemtCode 
			AND (bitDropShip=0 or bitDropShip is null) 
		ORDER BY 
			OI.numoppitemtCode                                            
    
		IF @@rowcount=0 SET @numoppitemtCode=0    
	END   
  
	SELECT * FROM #tempItem
	DROP TABLE #tempItem
END
GO
