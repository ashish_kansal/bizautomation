/****** Object:  StoredProcedure [dbo].[usp_DeleteStage]    Script Date: 07/26/2008 16:15:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_deletestage')
DROP PROCEDURE usp_deletestage
GO
CREATE PROCEDURE [dbo].[usp_DeleteStage]
@numDomainId as numeric(9),
@strStageDetailsIds as varchar(500)
as

delete from stagepercentagedetails where  
	numStageDetailsId in (select Items from dbo.split(@strStageDetailsIds,',') WHERE Items<>'')
and numDomainId=@numDomainId
GO
