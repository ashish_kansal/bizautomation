/****** Object:  StoredProcedure [dbo].[usp_SetRecordOwnersForSelectedRecords]    Script Date: 07/26/2008 16:21:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created By: Debasish Nag                
--Created On: 21st Aug 2005                
--Modified On: 17th Mar 2006   Changed length of @vcEntityIdList to 4000  
--Updates the Record Owners for the selected list of Company Ids           
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_setrecordownersforselectedrecords')
DROP PROCEDURE usp_setrecordownersforselectedrecords
GO
CREATE PROCEDURE [dbo].[usp_SetRecordOwnersForSelectedRecords]      
  @vcEntityIdList NVarchar(4000),          
  @numOwnerId Numeric(9),    
  @bitIncludeContactRecords Bit         
AS                                      
BEGIN                       
 SET @vcEntityIdList = @vcEntityIdList + ','      
 DECLARE @intFirstComma integer        
 DECLARE @intFirstUnderScore integer        
      
 DECLARE @numCompanyId NUMERIC(9)      
 DECLARE @numContactId NUMERIC(9)      
 DECLARE @numDivisionId NUMERIC(9)      
      
 DECLARE @vcCompContDivValue NVarchar(30)      
 DECLARE @recordCount Numeric      
       
 SET @intFirstComma = CHARINDEX(',', @vcEntityIdList, 0)        
      
 WHILE @intFirstComma > 0      
 BEGIN      
  SET @vcCompContDivValue = SUBSTRING(@vcEntityIdList,0,@intFirstComma)      
  SET @intFirstUnderScore = CHARINDEX('_', @vcCompContDivValue, 0)      
  SET @numCompanyId = SUBSTRING(@vcCompContDivValue, 0, @intFirstUnderScore)      
  SET @vcCompContDivValue = SUBSTRING(@vcCompContDivValue, @intFirstUnderScore+1, Len(@vcCompContDivValue))      
  SET @intFirstUnderScore = CHARINDEX('_', @vcCompContDivValue, 0)      
  SET @numContactId = SUBSTRING(@vcCompContDivValue, 0, @intFirstUnderScore)      
  SET @numDivisionId = SUBSTRING(@vcCompContDivValue, @intFirstUnderScore+1, Len(@vcCompContDivValue))      
         PRINT  @numCompanyId      
         PRINT  @numContactId      
         PRINT  @numDivisionId      
    
   IF @bitIncludeContactRecords = 1    
 UPDATE AdditionalContactsInformation SET          
    numRecOwner = @numOwnerId           
    WHERE numDivisionId = @numDivisionId     
      
   UPDATE DivisionMaster SET          
   numRecOwner = @numOwnerId    
   WHERE numCompanyId = @numCompanyId      
   AND numDivisionId = @numDivisionId     
       
   SELECT @recordCount = @@ROWCOUNT      
      
   SET @vcEntityIdList = SUBSTRING(@vcEntityIdList,@intFirstComma+1,Len(@vcEntityIdList))        
   SET @intFirstComma = CHARINDEX(',', @vcEntityIdList, 0)        
 END      
 SELECT @recordCount      
END
GO
