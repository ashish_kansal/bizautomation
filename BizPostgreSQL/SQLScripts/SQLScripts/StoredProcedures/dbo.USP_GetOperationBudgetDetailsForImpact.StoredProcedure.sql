/****** Object:  StoredProcedure [dbo].[USP_GetOperationBudgetDetailsForImpact]    Script Date: 07/26/2008 16:17:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created By Siva              
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getoperationbudgetdetailsforimpact')
DROP PROCEDURE usp_getoperationbudgetdetailsforimpact
GO
CREATE PROCEDURE [dbo].[USP_GetOperationBudgetDetailsForImpact]              
(@numDomainId as numeric(9)=0,            
@numBudgetId as numeric(9)=0)              
As              
Begin              
	 Select distinct convert(varchar(10),OBD.numChartAcntId)+'~'+convert(varchar(10),OBD.numParentAcntId)  as numChartAcntId,COA.vcAccountName as  ChartAcntname From OperationBudgetMaster OBM            
	 inner join OperationBudgetDetails OBD  on OBM.numBudgetId=OBD.numBudgetId            
	 inner join Chart_of_Accounts COA on OBD.numChartAcntId=COA.numAccountId        
	 Where  OBM.numBudgetId=@numBudgetId And OBD.intYear=year(getutcdate()) And OBM.numDomainId=@numDomainId              
End
GO
