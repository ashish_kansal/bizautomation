/****** Object:  StoredProcedure [dbo].[USP_SumDebitCreditValues]    Script Date: 07/26/2008 16:21:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created by Siva  
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_sumdebitcreditvalues')
DROP PROCEDURE usp_sumdebitcreditvalues
GO
CREATE PROCEDURE [dbo].[USP_SumDebitCreditValues]  
@numDomainId as numeric(9)=0,  
@numJournalId as numeric(9)=0,  
@numAccountsType as numeric(9)=0  
AS  
Begin  
if @numAccountsType=1 --To Sum Debit Values  
Begin  
  Select sum(numDebitAmt) from General_Journal_Details Where numDomainId=@numDomainId   
  And numJournalId=@numJournalId  
End  
if @numAccountsType=2 --To Sum Credit Values  
Begin  
Select sum(numCreditAmt) from General_Journal_Details Where numDomainId=@numDomainId   
  And numJournalId=@numJournalId  
End  
End
GO
