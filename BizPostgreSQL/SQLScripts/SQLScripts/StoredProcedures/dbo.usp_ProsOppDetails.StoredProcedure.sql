/****** Object:  StoredProcedure [dbo].[usp_ProsOppDetails]    Script Date: 07/26/2008 16:20:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_prosoppdetails')
DROP PROCEDURE usp_prosoppdetails
GO
CREATE PROCEDURE [dbo].[usp_ProsOppDetails]
    @bytemode AS TINYINT = NULL,
    @numDivisionId AS NUMERIC(9) = NULL,
    @numContactId AS NUMERIC(9) = NULL,
    @tintOppStatus AS TINYINT = 0,
    @tintOppType AS TINYINT = 0,
    @numDomainID AS NUMERIC(9) = 0,
    @CurrentPage INT,
    @PageSize INT,
    @TotRecs INT OUTPUT,
    @intQuarter INT,
    @intYear INT
AS 

/*paging logic*/  
        DECLARE @firstRec AS INTEGER
        DECLARE @lastRec AS INTEGER
        SET @firstRec = ( @CurrentPage - 1 ) * @PageSize
        SET @lastRec = ( @CurrentPage * @PageSize + 1 )  

/*Dynamic sql*/
        DECLARE @strSql NVARCHAR(4000)
        DECLARE @SELECT NVARCHAR(4000)
        DECLARE @FROM NVARCHAR(4000)
        DECLARE @WHERE NVARCHAR(4000)
        SET @strSql='';
        SET @SELECT='';
        SET @FROM='';
        SET @WHERE='';
        

        IF @bytemode = 0 
            BEGIN                    
           SET @SELECT ='
						WITH Orders
						AS (SELECT Row_number() OVER(ORDER BY Opp.bintCreatedDate desc ) AS row,
						numOppId,
                        vcPOppName,
                        tintopptype,
                        --vcRefOrderNo,
                        CASE ISNULL(tintshipped, 0)
                          WHEN 0 THEN ''Open''
                          WHEN 1 THEN ''Completed''
                        END vcDealStatus,
                        dbo.GetListIemName(numStatus) vcStatus,
                        monDealAmount,
                        dbo.FormatedDateFromDate(Opp.bintCreatedDate,
                                                 Opp.numDomainId) bintCreatedDate '
            SET @FROM = ' FROM    OpportunityMaster opp
                        JOIN AdditionalContactsInformation addC ON addC.numContactId = opp.numContactId
                        JOIN DivisionMaster div ON div.numDivisionID = opp.numDivisionID
                        JOIN CompanyInfo Com ON com.numCompanyId = div.numCompanyId '
            SET @WHERE =' WHERE   OPP.tintOppStatus = 1 AND
                        opp.numDomainID = '+ +convert(varchar(15), @numDomainID)
            IF @tintOppType >0 
				SET @WHERE = @WHERE + ' AND opp.tintOppType = ' + CONVERT(VARCHAR(3),@tintOppType) 
			
        END  
            

        
              
      
            
            
        IF @bytemode = 1 
            BEGIN       
             SET @SELECT ='
						WITH Orders
						AS (SELECT Row_number() OVER(ORDER BY Opp.bintCreatedDate desc ) AS row,
						OPP.numOppId,
                        OBD.numOppBizDocsId,
                        dbo.FormatedDateFromDate(OBD.dtCreatedDate,
                                                 Opp.numDomainId) dtCreatedDate,
                        opp.numContactID,
                        vcBizDocID,
                        OBD.monDealAmount,
                        CASE ISNULL(opp.bitBillingTerms, 0)
                          WHEN 1
--                          THEN [dbo].[FormatedDateFromDate](DATEADD(dd, CONVERT(INT, ISNULL(dbo.fn_GetListItemName(ISNULL(opp.intBillingDays, 0)), 0)), dtFromDate),
--                                                            Opp.numDomainID)
						  THEN [dbo].[FormatedDateFromDate](DATEADD(dd, CONVERT(INT, ISNULL((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(Opp.intBillingDays,0)), 0)), dtFromDate),
                                                            Opp.numDomainID)	                                                            
                          WHEN 0
                          THEN [dbo].[FormatedDateFromDate](obd.[dtFromDate],
                                                            Opp.numDomainID)
                        END AS dtDueDate,
                        obd.monDealAmount - monamountpaid AS baldue,
                        dbo.GetListIemName(OBD.numBizDocStatus) vcStatus'
                SET @FROM =' FROM    OpportunityMaster opp
                        INNER JOIN dbo.OpportunityBizDocs OBD ON OBD.numOppId = OPP.numOppId
                        JOIN AdditionalContactsInformation addC ON addC.numContactId = opp.numContactId
                        JOIN DivisionMaster div ON div.numDivisionID = opp.numDivisionID
                        JOIN CompanyInfo Com ON com.numCompanyId = div.numCompanyId '
                SET @WHERE =' WHERE OPP.tintOppStatus = 1 AND
                        opp.numDomainID = ' +convert(varchar(15), @numDomainID) + '
                        AND opp.tintOppType = '+ CONVERT(VARCHAR(3),@tintOppType) + '
						AND OBD.[bitAuthoritativeBizDocs] =1'
				
            END
            
            
              IF @bytemode = 2 
            BEGIN  
            SET @SELECT ='
						WITH Orders
						AS (SELECT Row_number() OVER(ORDER BY Opp.bintCreatedDate desc ) AS row,
						Opp.numOppId,
                        vcPOppName,
                        tintopptype,
                        --vcRefOrderNo,
                        dbo.FormatedDateFromDate(Opp.bintCreatedDate,
                                                 Opp.numDomainId) bintCreatedDate,
                        CASE ISNULL(tintshipped, 0)
                          WHEN 0 THEN ''Open''
                          WHEN 1 THEN ''Completed''
                        END vcDealStatus,
                        dbo.GetListIemName(numStatus) vcStatus,
                        monDealAmount,
                        CAST(ISNULL(PP.intTotalProgress, 0) AS VARCHAR(3))
                        + '' %'' intTotalProgress,
                        CASE tintoppType
                          WHEN 1 THEN ''Sales''
                          WHEN 2 THEN ''Purchase''
                        END AS OppType,
                        dbo.FormatedDateFromDate(Opp.intPEstimatedCloseDate,
                                                 Opp.numDomainId) intPEstimatedCloseDate '
               SET @FROM =' FROM    OpportunityMaster opp
                        left JOIN dbo.ProjectProgress PP ON PP.numOppId = Opp.numOppId
                        JOIN AdditionalContactsInformation addC ON addC.numContactId = opp.numContactId
                        JOIN DivisionMaster div ON div.numDivisionID = opp.numDivisionID
                        JOIN CompanyInfo Com ON com.numCompanyId = div.numCompanyId '
               SET @WHERE =' WHERE   
                        opp.numDomainID = ' +convert(varchar(15), @numDomainID) + '
                        AND tintOppStatus =  ' + CONVERT(VARCHAR(3),@tintOppStatus) 
  
				IF @tintOppType >0 
					SET @WHERE = @WHERE + ' AND opp.tintOppType = ' + CONVERT(VARCHAR(3),@tintOppType) 
                        
            END

		IF @bytemode = 3 
            BEGIN                    
           SET @SELECT ='
						WITH Orders
						AS (SELECT Row_number() OVER(ORDER BY Opp.bintCreatedDate desc ) AS row,
						numOppId,
                        vcPOppName,
                        tintopptype,
                        --vcRefOrderNo,
                        CASE ISNULL(tintshipped, 0)
                          WHEN 0 THEN ''Open''
                          WHEN 1 THEN ''Completed''
                        END vcDealStatus,
                        dbo.GetListIemName(numStatus) vcStatus,
                        monDealAmount,
                        dbo.FormatedDateFromDate(Opp.bintCreatedDate,
                                                 Opp.numDomainId) bintCreatedDate '
            SET @FROM = ' FROM    OpportunityMaster opp
                        JOIN AdditionalContactsInformation addC ON addC.numContactId = opp.numContactId
                        JOIN DivisionMaster div ON div.numDivisionID = opp.numDivisionID
                        JOIN CompanyInfo Com ON com.numCompanyId = div.numCompanyId '
            SET @WHERE =' WHERE   OPP.tintOppStatus = 1 AND DATENAME(q,opp.bintCreatedDate)='+convert(varchar(15), @intQuarter) + ' And year(opp.bintCreatedDate)='+convert(varchar(15), @intYear) + ' and
                        opp.numDomainID = '+convert(varchar(15), @numDomainID)
            IF @tintOppType >0 
				SET @WHERE = @WHERE + ' AND opp.tintOppType = ' + CONVERT(VARCHAR(3),@tintOppType) 
			
        END  
    
         IF @bytemode = 4 
            BEGIN  
            SET @SELECT ='
						WITH Orders
						AS (SELECT Row_number() OVER(ORDER BY Opp.bintCreatedDate desc ) AS row,
						Opp.numOppId,
                        vcPOppName,
                        tintopptype,
                        --vcRefOrderNo,
                        dbo.FormatedDateFromDate(Opp.bintCreatedDate,
                                                 Opp.numDomainId) bintCreatedDate,
                        CASE ISNULL(tintshipped, 0)
                          WHEN 0 THEN ''Open''
                          WHEN 1 THEN ''Completed''
                        END vcDealStatus,
                        dbo.GetListIemName(numStatus) vcStatus,
                        monDealAmount,
                        CAST(ISNULL(PP.intTotalProgress, 0) AS VARCHAR(3))
                        + '' %'' intTotalProgress,
                        CASE tintoppType
                          WHEN 1 THEN ''Sales''
                          WHEN 2 THEN ''Purchase''
                        END AS OppType,
                        dbo.FormatedDateFromDate(Opp.intPEstimatedCloseDate,
                                                 Opp.numDomainId) intPEstimatedCloseDate '
               SET @FROM =' FROM    OpportunityMaster opp
                        left JOIN dbo.ProjectProgress PP ON PP.numOppId = Opp.numOppId
                        JOIN AdditionalContactsInformation addC ON addC.numContactId = opp.numContactId
                        JOIN DivisionMaster div ON div.numDivisionID = opp.numDivisionID
                        JOIN CompanyInfo Com ON com.numCompanyId = div.numCompanyId '
               SET @WHERE =' WHERE   
                        opp.numDomainID = ' +convert(varchar(15), @numDomainID) + '
                        AND tintOppStatus =  ' + CONVERT(VARCHAR(3),@tintOppStatus) + '
						AND DATENAME(q,opp.bintCreatedDate)='+convert(varchar(15), @intQuarter) + ' And year(opp.bintCreatedDate)=' + convert(varchar(15), @intYear)
				IF @tintOppType >0 
					SET @WHERE = @WHERE + ' AND opp.tintOppType = ' + CONVERT(VARCHAR(3),@tintOppType) 
                        
            END

	IF @numDivisionID >0 
		SET @WHERE = @WHERE + ' AND opp.numDivisionId = ' +convert(varchar(15), @numDivisionId) 
	IF @numContactID >0 
		SET @WHERE = @WHERE + ' AND Opp.numContactID = ' +convert(varchar(15), @numContactID) 

	IF @PageSize>0
    	SET @strSql = @SELECT + @FROM + @WHERE + ') SELECT * FROM   [Orders] WHERE row > ' + CONVERT(VARCHAR(10), @firstRec) + ' and row <' + CONVERT(VARCHAR(10), @lastRec)
	ELSE 
		SET @strSql = @SELECT + @FROM + @WHERE + ') SELECT * FROM   [Orders]' 

		PRINT @strSql         
		EXEC(@strSql)
       /*Get total count */
        SET @strSql =  ' (SELECT @TotRecs =COUNT(*) ' + @FROM + @WHERE + ')';
        
        EXEC sp_executesql 
        @query = @strSql, 
        @params = N'@TotRecs INT OUTPUT', 
        @TotRecs = @TotRecs OUTPUT 
GO
