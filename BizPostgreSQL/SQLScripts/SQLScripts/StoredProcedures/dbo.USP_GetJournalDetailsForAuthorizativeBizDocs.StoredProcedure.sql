/****** Object:  StoredProcedure [dbo].[USP_GetJournalDetailsForAuthorizativeBizDocs]    Script Date: 07/26/2008 16:17:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created By Siva  
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getjournaldetailsforauthorizativebizdocs')
DROP PROCEDURE usp_getjournaldetailsforauthorizativebizdocs
GO
CREATE PROCEDURE [dbo].[USP_GetJournalDetailsForAuthorizativeBizDocs]  
(  
  @numOppId as numeric(9)=0,  
  @numOppBizDocsId as numeric(9)=0  
)  
As  
Begin  
	Select GJD.numTransactionId as numTransactionId from General_Journal_Header GJH  
	join General_Journal_Details GJD on GJH.numJournal_Id=GJD.numJournalId  
	Where GJH.numOppId=@numOppId And GJH.numOppBizDocsId=@numOppBizDocsId And chBizDocItems='OI'
    And (GJD.numChartAcntId=2 Or GJD.numChartAcntId=16 Or GJD.numChartAcntId=17 Or GJD.numChartAcntId=19 Or GJD.numChartAcntId=20)  
End
GO
