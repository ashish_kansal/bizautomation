SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO         
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetListDetailAttributesUsingDomainID')
	DROP PROCEDURE USP_GetListDetailAttributesUsingDomainID
GO
CREATE PROCEDURE [dbo].[USP_GetListDetailAttributesUsingDomainID]            
	@numDomainID as numeric(9)=0       
AS
BEGIN            
	SELECT * FROM ListDetails LD
	JOIN CFW_Fld_Master CFM ON CFM.numlistid=LD.numListID  
	WHERE Grp_id=9 AND LD.numDomainID = @numDomainID
END
GO
