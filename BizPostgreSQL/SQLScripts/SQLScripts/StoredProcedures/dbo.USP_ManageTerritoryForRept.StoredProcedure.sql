/****** Object:  StoredProcedure [dbo].[USP_ManageTerritoryForRept]    Script Date: 07/26/2008 16:20:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_manageterritoryforrept')
DROP PROCEDURE usp_manageterritoryforrept
GO
CREATE PROCEDURE [dbo].[USP_ManageTerritoryForRept]    
@numUserCntId as numeric(9),    
@numDomainId as numeric(9),    
@tintType as tinyint,    
@strTerritory as varchar(4000)    
as    
    
    
declare @separator_position as integer    
declare @strPosition as varchar(1000)    
    
delete from ForReportsByTerritory where numUserCntID = @numUserCntId    
and numDomainId=@numDomainID and tintType=@tintType    
     
    
   while patindex('%,%' , @strTerritory) <> 0    
    begin -- Begin for While Loop    
      select @separator_position =  patindex('%,%' , @strTerritory)    
    select @strPosition = left(@strTerritory, @separator_position - 1)    
       select @strTerritory = stuff(@strTerritory, 1, @separator_position,'')    
     insert into ForReportsByTerritory (numUserCntID,numTerritory,numDomainID,tintType)    
     values (@numUserCntId,@strPosition,@numDomainID,@tintType)    
     
      
   end
GO
