/****** Object:  StoredProcedure [dbo].[USP_GetReportForTerritory]    Script Date: 07/26/2008 16:18:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getreportforterritory')
DROP PROCEDURE usp_getreportforterritory
GO
CREATE PROCEDURE [dbo].[USP_GetReportForTerritory]  
@numUserCntId as numeric(9),      
@numDomainId AS NUMERIc(9),      
@tintType as tinyint      
as      
select numListitemid,vcdata from forreportsbyterritory  
join listdetails       
on numlistitemid=numterritory  
where forreportsbyterritory.numUserCntID=@numUserCntId      
and forreportsbyterritory.numDomainId=@numDomainId      
and tintType=@tintType
GO
