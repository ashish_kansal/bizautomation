/****** Object:  StoredProcedure [dbo].[USP_ItemList]    Script Date: 07/26/2008 16:19:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--- Created By Anoop Jayaraj                                      
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_itemlist')
DROP PROCEDURE usp_itemlist
GO
CREATE PROCEDURE [dbo].[USP_ItemList]                                        
@ItemClassification as numeric(9)=0,                                    
@KeyWord as varchar(100)='',                                    
@IsKit as tinyint,                                    
@SortChar char(1)='0',                                     
@CurrentPage int,                                      
@PageSize int,                                      
@TotRecs int output,                                      
@columnName as Varchar(50),                                      
@columnSortOrder as Varchar(10),                      
@numDomainID as numeric(9)=0,          
@ItemType as char(1),          
@bitSerialized as bit,        
@numItemGroup as numeric(9),  
@bitAssembly as bit                                      
as                  
                
                
                                    
declare @firstRec as integer                                      
declare @lastRec as integer                                      
set @firstRec= (@CurrentPage-1) * @PageSize                                      
set @lastRec= (@CurrentPage*@PageSize+1)                                     
                                     
                                      
                                      
declare @strSql as varchar(5000)                  
set @strSql='With tblItem AS (select                  
Row_Number() Over (ORDER BY ' + @columnName +' '+ @columnSortOrder+' ) as RunningCount,numItemCode,vcItemName,txtItemDesc,                                   
case when charItemType=''P'' then ''Inventory Item'' when charItemType=''N'' then ''Non Inventory Item'' when charItemType=''S'' then ''Service'' when charItemType=''A'' then ''Accessory'' end as ItemType,                                    
sum(numOnHand) as numQtyOnHand,                
sum(numOnOrder) as numQtyOnOrder,                                    
sum(numReorder) as numQtyReorder,                
sum(numBackOrder) as numQtyBackOrder,                
sum(numAllocation) as numQtyOnAllocation,                
isnull(com.vcCompanyName,''-'') as vendor                               
from item                   
left join WareHouseItems                 
on numItemID=numItemCode                                
left join divisionmaster div                                  
on numVendorid=div.numDivisionId                                  
left join companyInfo com                                  
on com.numCompanyid=div.numcompanyID where Item.numDomainID='+ convert(varchar(15),@numDomainID)   
if @bitAssembly='1'  set @strSql=@strSql + ' and bitAssembly= '+ convert(varchar(2),@bitAssembly)+''                                  
else if @IsKit='1'  set @strSql=@strSql + ' and (bitAssembly=0 or  bitAssembly is null) and bitKitParent= '+ convert(varchar(2),@IsKit)+''             
if @ItemType<>''  set @strSql=@strSql + ' and charItemType= '''+ @ItemType+''''          
if @bitSerialized=1  set @strSql=@strSql + ' and bitSerialized=1 '                                                 
if @SortChar<>'0'  set @strSql=@strSql + ' and vcItemName like '''+@SortChar+'%'''                                     
if @ItemClassification<>'0' set @strSql=@strSql + ' and numItemClassification='+convert(varchar(15),@ItemClassification)                                       
if @KeyWord<>'' set @strSql=@strSql + ' and (vcItemName like '''+@KeyWord+'%'' or txtItemDesc like  '''+@KeyWord+'%'')'          
if @numItemGroup>0  set @strSql=@strSql + ' and numItemGroup = ' +  convert(varchar(20),@numItemGroup)     
if @numItemGroup=-1   set @strSql=@strSql + ' and numItemGroup in (select numItemGroupID from ItemGroups where numDomainID=' +  convert(varchar(20),@numDomainID) +')'                                    
set @strSql=@strSql +' group by numItemCode,vcItemName,txtItemDesc,charItemType,vcCompanyName,vcDivisionName)'              
              
              
              
              
     
set @strSql=@strSql+ 'select * from tblItem where  RunningCount >'+ convert(varchar(15),@firstRec) +' and RunningCount < '+ convert(varchar(15),@lastRec) +'              
              
union select 0,count(*),null,null,null,null,null,null,null,null,null from tblItem order by RunningCount '                                            
 print    @strSql          
exec (@strSql)
GO
