/****** Object:  StoredProcedure [dbo].[usp_GetOpportunitySourcesForReport]    Script Date: 07/26/2008 16:18:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Modified by anoop jayaraj        
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getopportunitysourcesforreport')
DROP PROCEDURE usp_getopportunitysourcesforreport
GO
CREATE PROCEDURE [dbo].[usp_GetOpportunitySourcesForReport]              
        @numDomainID numeric,              
        @dtFromDate datetime,              
        @dtToDate datetime,              
        @numUserCntID numeric=0,                     
        @intType numeric,              
        @tintRights numeric              
              
--              
AS              
BEGIN              
 IF @tintRights=1              
        BEGIN              
  SELECT  isnull(LD.vcData,'None') as Source, COUNT(OM.numOppID) Total,               
   (COUNT(OM.numOppID) * 100.00) /              
    (select Count(*) from Opportunitymaster OM--, ListDetails LD              
    Left Outer Join ListDetails LD  on OM.tintSource = LD.numListItemID   
    WHERE --LD.numListItemID = tintSource               
     --AND LD.numListID = 18              
     OM.numDomainID = @numDomainID              
     AND OM.tintOppStatus = 0             
     AND OM.bintCreatedDate BETWEEN @dtFromDate AND @dtToDate            
     and om.numrecOwner= @numUserCntID             
    ) as Percentage              
   FROM Opportunitymaster OM  
  Left Outer Join ListDetails LD  on OM.tintSource = LD.numListItemID                
   WHERE               
    --AND LD.numListID = 18              
    OM.numDomainID = @numDomainID              
    AND OM.tintOppStatus = 0               
    and om.numrecOwner= @numUserCntID             
    AND OM.bintCreatedDate BETWEEN @dtFromDate AND @dtToDate              
   GROUP BY OM.tintSource, LD.vcData              
              
              
        END              
              
        IF @tintRights=2              
        BEGIN              
              
              
  SELECT isnull(LD.vcData,'None') as Source, COUNT(OM.numOppID) Total,               
   (COUNT(OM.numOppID) * 100.00) /              
    (select Count(*) from Opportunitymaster OM--, ListDetails LD,AdditionalContactsInformation ADC   
     Left Outer Join ListDetails LD  on OM.tintSource = LD.numListItemID  
     Join AdditionalContactsInformation ADC On  OM.numrecOwner = ADC.numContactId                 
    WHERE  OM.numDomainID = @numDomainID              
     AND OM.tintOppStatus =0              
     AND OM.bintCreatedDate BETWEEN @dtFromDate AND @dtToDate              
     and ADC.numTeam in(select F.numTeam from ForReportsByTeam F                 
     where F.numUserCntID=@numUserCntID and F.numDomainid=@numDomainId and F.tintType=@intType)                
    ) as Percentage              
   FROM Opportunitymaster OM--, ListDetails LD,AdditionalContactsInformation ADC                         
   Left Outer Join ListDetails LD  on OM.tintSource = LD.numListItemID  
   Join AdditionalContactsInformation ADC On  OM.numrecOwner = ADC.numContactId  
   WHERE OM.numDomainID = @numDomainID              
    AND OM.tintOppStatus =0             
    AND OM.bintCreatedDate BETWEEN @dtFromDate AND @dtToDate              
    and ADC.numTeam in(select F.numTeam from ForReportsByTeam F                 
    where F.numUserCntID=@numUserCntID and F.numDomainid=@numDomainId and F.tintType=@intType)                
   GROUP BY OM.tintSource, LD.vcData              
        END              
              
        IF @tintRights=3               
        -- TERRITORY RECORDS RIGHTS              
        BEGIN              
              
  SELECT  isnull(LD.vcData,'None') as Source, COUNT(OM.numOppID) Total,               
   (COUNT(OM.numOppID) * 100.00) /              
    (select Count(*) from Opportunitymaster OM--, ListDetails LD, DivisionMaster DM    
     Left Outer Join ListDetails LD  On OM.tintSource = LD.numListItemID        
     Join DivisionMaster DM On OM.numDivisionId = DM.numDivisionID   
    WHERE OM.numDomainID = @numDomainID              
     AND OM.tintOppStatus = 0            
      and DM.NumTerId in(select F.numTerritory from ForReportsByTerritory F      -- Added By Anoop                 
    where F.numUserCntID=@numUserCntID and F.numDomainid=@numDomainID and F.tintType=@intType)             
     AND OM.bintCreatedDate BETWEEN @dtFromDate AND @dtToDate              
    ) as Percentage              
   FROM Opportunitymaster OM  
   Left Outer Join ListDetails LD  On OM.tintSource = LD.numListItemID        
   Join DivisionMaster DM On OM.numDivisionId = DM.numDivisionID  
   WHERE OM.numDomainID = @numDomainID              
    AND OM.tintOppStatus = 0             
    AND DM.numTerId in(select F.numTerritory from ForReportsByTerritory F   -- Added By Anoop                 
    where F.numUserCntID=@numUserCntID and F.numDomainid=@numDomainID and F.tintType=@intType)             
    AND OM.bintCreatedDate BETWEEN @dtFromDate AND @dtToDate              
   GROUP BY OM.tintSource, LD.vcData              
              
                             
        END              
              
END
GO
