/****** Object:  StoredProcedure [dbo].[USP_getCustomReportUser]    Script Date: 07/26/2008 16:17:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by Ganga    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getcustomreportuser')
DROP PROCEDURE usp_getcustomreportuser
GO
CREATE PROCEDURE [dbo].[USP_getCustomReportUser]       
@numUserCntID as numeric(9),      
@numDomainId as numeric(9)  ,    
@numGroupId as numeric(9)  =0
as      
if  @numGroupId = 0
	begin

		select vcReportName+' - '+ vcReportDescription as vcReportName,numcustomReportId as numReportId from customreport     

		where numCreatedBy = @numUserCntID and numDomainId = @numDomainId
	end
else

	begin

		select vcReportName+' - '+ vcReportDescription as vcReportName,numcustomReportId as numReportId from customreport cr    
		join  DashboardAllowedReports DAR on Cr.numCustomReportId=DAR.numReportId 
		where  numDomainId = @numDomainId and numgrpId = @numGroupId 
	end
GO
