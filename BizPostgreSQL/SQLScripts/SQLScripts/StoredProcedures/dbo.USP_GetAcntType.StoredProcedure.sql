/****** Object:  StoredProcedure [dbo].[USP_GetAcntType]    Script Date: 07/26/2008 16:16:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created By Siva    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getacnttype')
DROP PROCEDURE usp_getacnttype
GO
CREATE PROCEDURE [dbo].[USP_GetAcntType]    
@numAccountId as numeric(9)=0,    
@numDomainId as numeric(9)=0    
As    
Begin    
if @numAccountId<>0     
Begin    
   Select isnull(numAcntTypeId,0) From Chart_Of_Accounts Where numAccountId=@numAccountId  And numDomainId=@numDomainId  
End    
End
GO
