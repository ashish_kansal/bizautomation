
GO
/****** Object:  StoredProcedure [dbo].[USP_Journal_EntryListMultiCOA]    Script Date: 02/19/2010 23:50:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created By Siva   
-- exec USP_Journal_EntryListMultiCOA @dtDateFrom='1753-01-01 00:00:00:000',@numDomainId=72,@numDivisionID=15851
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_journal_entrylistmulticoa')
DROP PROCEDURE usp_journal_entrylistmulticoa
GO
CREATE PROCEDURE [dbo].[USP_Journal_EntryListMultiCOA]                                                                                                                                
--@numChartAcntId as numeric(9),                                                                                                                          
@dtDateFrom datetime,                        
@numDomainId as numeric(9),
@numDivisionID AS NUMERIC(9)
                                                                                                                        
As                                                                                                                                
Begin                                                                                                                           
Declare @strSQL as varchar(8000)                                                                                                                      
Declare @strChildCategory as varchar(5000)                                                                              
Declare @numAcntTypeId as integer                                                                                                                 
Declare @OpeningBal as varchar(8000)  
DECLARE @vcAccountCode VARCHAR(50)                                                                                                                
Set @openingBal='Opening Balance'                                                                                                  
Set @strSQL =''                                                                                              
--Create a Temporary table to hold data      

create table #TempCoa
(numChartAcntId numeric(18));

set @dtDateFrom = ( SELECT max(dtPeriodFrom) FROM FinancialYear where bitCurrentYear=1 and numDomainID=@numDomainId)

INSERT INTO #TempCoa

select ISNULL(numARAccountID,0) AS numChartAccountID from COARelationships CR, 
							CompanyInfo CI, 
							DivisionMaster DM
WHERE CR.numRelationShipID=CI.numCompanyType and 
		CR.numDomainID=DM.numDomainID and
	  CI.numCompanyID=DM.numCompanyID and 
	  DM.numDivisionID= @numDivisionID AND 
	  DM.numDomainID=@numDomainId
UNION
select ISNULL(numAPAccountID,0) from COARelationships CR, 
							CompanyInfo CI, 
							DivisionMaster DM
WHERE CR.numRelationShipID=CI.numCompanyType and 
	  CR.numDomainID=DM.numDomainID and
	  CI.numCompanyID=DM.numCompanyID and 
	  DM.numDivisionID= @numDivisionID AND 
	  DM.numDomainID=@numDomainId;


                                                                                                                                
Create table #tempTable ( ID INT IDENTITY PRIMARY KEY,                                                                                                                                                
 numCustomerId numeric(9),                                                                                                  
 JournalId numeric(9),                                              
 TransactionType varchar(500),                                                                                                
 EntryDate datetime,                                                                                                  
 CompanyName varchar(8000),                                                                                        
 CheckId numeric(9),                                                                                      
 CashCreditCardId numeric(9),                                                                                                  
 Memo varchar(8000),                                                                                                  
 Payment DECIMAL(20,5),                                                                                                  
 Deposit DECIMAL(20,5),           
 numBalance DECIMAL(20,5),             
 numChartAcntId numeric(9),                                                                                            
 numTransactionId numeric(9),                                                                                                  
 numOppId numeric(9),                    
 numOppBizDocsId numeric(9),                  
 numDepositId numeric(9),                
 numBizDocsPaymentDetId numeric(9),  
 numCategoryHDRID numeric(9),  
 tintTEType   numeric(9),  
 numCategory  numeric(9),   
 numUserCntID   numeric(9),
 dtFromDate datetime,
 numCheckNo VARCHAR(20),bitReconcile BIT,bitCleared BIT  
)                                                                                                                                      
                                            
   if @dtDateFrom='' Set @dtDateFrom='Jan  1 1753 12:00:00:000AM'                                               
   
SELECT @vcAccountCode=vcAccountCode FROM [Chart_Of_Accounts] WHERE numAccountID in (select numChartAcntId from #TempCoa ) -- = @numChartAcntId
SELECT @strChildCategory = COALESCE(@strChildCategory + ',', '') + 
   CAST(numAccountId AS varchar(10))
FROM Chart_Of_Accounts
WHERE [vcAccountCode] LIKE @vcAccountCode +'%'

Print @strChildCategory
                                           
if @dtDateFrom='Jan  1 1753 12:00:00:000AM'                                                   
Begin                                                                                  
--Commented by chintan
--Set @strChildCategory = dbo.fn_ChildCategory(@numChartAcntId,@numDomainId) + Convert(varchar(10),@numChartAcntId)                                                           

                                                                          
Select @numAcntTypeId=isnull(numParntAcntTypeId,0) from Chart_of_Accounts Where numAccountId in (select MAX(numChartAcntId) from #TempCoa )  And numDomainId=@numDomainId                                                                  
Print @numAcntTypeId                                                         
if @numAcntTypeId=815 or @numAcntTypeId=816 or @numAcntTypeId=820  or @numAcntTypeId=821  or @numAcntTypeId=822  or @numAcntTypeId=825 or @numAcntTypeId=827                                                                        
Begin                                                                          
                                  
         
Set @strSQL= ' Select GJD.numCustomerId AS numCustomerId,GJH.numJournal_Id as JournalId,        
case when isnull(GJH.numCheckId,0) <> 0 then ''Checks''  
Else case when isnull(GJH.numCashCreditCardId,0) <> 0 And CCCD.bitMoneyOut=0 then ''Cash''        
ELse Case when isnull(GJH.numCashCreditCardId,0) <> 0 And CCCD.bitMoneyOut=1 And CCCD.bitChargeBack=1  then ''Charge''        
ELse Case when isnull(GJH.numOppId,0) <> 0 And isnull(GJH.numOppBizDocsId,0) <> 0 And isnull(GJH.numBizDocsPaymentDetId,0)=0 And Opp.tintOppType=1 then ''BizDocs Invoice''        
ELse Case when isnull(GJH.numOppId,0) <> 0 And isnull(GJH.numOppBizDocsId,0) <> 0 And isnull(GJH.numBizDocsPaymentDetId,0)=0 And Opp.tintOppType=2 then ''BizDocs Purchase''        
ELse Case when isnull(GJH.numDepositId,0) <> 0 then ''Deposit''        
ELse Case when isnull(GJH.numBizDocsPaymentDetId,0)<>0 And Opp.tintOppType=1 then ''Receive Amt''        
ELse Case when isnull(GJH.numBizDocsPaymentDetId,0)<>0 And Opp.tintOppType=2 then ''Vendor Amt''        
Else Case when isnull(GJH.numCategoryHDRID,0)<>0 then ''Time And Expenses''  
Else Case When GJH.numJournal_Id <>0 then ''Journal'' End End End End End End End End End End  as TransactionType,        
GJH.datEntry_Date as EntryDate,CI.vcCompanyName AS CompanyName,GJH.numCheckId As CheckId,        
GJH.numCashCreditCardId as CashCreditCardId,        
GJH.varDescription as Memo,        
(case when GJD.numCreditAmt=0 then GJD.numdebitAmt  end) as Deposit,        
(case when GJD.numdebitAmt=0 then GJD.numCreditAmt end) as Payment,        
null numBalance,        
isnull(GJD.numChartAcntId,0) as numChartAcntId,        
GJD.numTransactionId as numTransactionId,               
GJH.numOppId as numOppId,        
GJH.numOppBizDocsId as numOppBizDocsId,        
GJH.numDepositId as numDepositId,          
GJH.numBizDocsPaymentDetId as numBizDocsPaymentDetId,  
isnull(GJH.numCategoryHDRID,0) as numCategoryHDRID,  
isnull(TE.tintTEType,0) as tintTEType,  
TE.numCategory as numCategory,  
TE.numUserCntID as numUserCntID,
TE.dtFromDate as dtFromDate,
isnull(CAST(CD.numCheckNo AS VARCHAR(20)),'''') numCheckNo,
isnull(GJD.bitReconcile,0) as bitReconcile,
isnull(GJD.bitCleared,0) as bitCleared      
From General_Journal_Header as GJH        
Left Outer Join General_Journal_Details as GJD  on GJH.numJournal_Id=GJD.numJournalId        
Left outer join DivisionMaster as DM on GJD.numCustomerId=DM.numDivisionID        
Left outer join CompanyInfo as CI on  DM.numCompanyID=CI.numCompanyId        
Left outer join Chart_Of_Accounts CA on GJD.numChartAcntId=CA.numAccountId        
Left outer join CashCreditCardDetails CCCD on GJH.numCashCreditCardId=CCCD.numCashCreditId        
Left outer join CheckDetails CD on GJH.numCheckId=CD.numCheckId        
Left outer join OpportunityMaster Opp on GJH.numOppId=Opp.numOppId    
Left outer join TimeAndExpense TE on GJH.numCategoryHDRID=TE.numCategoryHDRID     
Where  GJH.numDomainId='+convert(varchar(4),@numDomainId)       
--And GJH.numChartAcntId is null      
+ ' ORDER BY GJH.datEntry_Date'           
End                                                                                                              
Else                                                                          
Begin                               
                               
                           
                  
Set @strSQL=' Select GJD.numCustomerId AS numCustomerId,GJH.numJournal_Id as JournalId,        
case when isnull(GJH.numCheckId,0) <> 0 then +''Checks'' Else case when isnull(GJH.numCashCreditCardId,0) <> 0 And CCCD.bitMoneyOut=0 then ''Cash''        
ELse Case when isnull(GJH.numCashCreditCardId,0) <> 0 And CCCD.bitMoneyOut=1 And CCCD.bitChargeBack=1  then ''Charge''        
ELse Case when isnull(GJH.numOppId,0) <> 0 And isnull(GJH.numOppBizDocsId,0) <> 0 And isnull(GJH.numBizDocsPaymentDetId,0)=0 And Opp.tintOppType=1 then ''BizDocs Invoice''        
ELse Case when isnull(GJH.numOppId,0) <> 0 And isnull(GJH.numOppBizDocsId,0) <> 0 And isnull(GJH.numBizDocsPaymentDetId,0)=0 And Opp.tintOppType=2 then ''BizDocs Purchase''        
ELse Case when isnull(GJH.numDepositId,0) <> 0 then ''Deposit''        
ELse Case when isnull(GJH.numBizDocsPaymentDetId,0)<>0 And Opp.tintOppType=1 then ''Receive Amt''        
ELse Case when isnull(GJH.numBizDocsPaymentDetId,0)<>0 And Opp.tintOppType=2 then ''Vendor Amt''  
Else Case when isnull(GJH.numCategoryHDRID,0)<>0 then ''Time And Expenses''        
Else Case When GJH.numJournal_Id <>0 then ''Journal'' End End End End End End End End End End as TransactionType,        
GJH.datEntry_Date as EntryDate,CI.vcCompanyName AS CompanyName,GJH.numCheckId As CheckId,        
GJH.numCashCreditCardId as CashCreditCardId,        
GJH.varDescription as Memo,        
(case when GJD.numCreditAmt<>0 then numCreditAmt end) as Payment,        
(case when GJD.numdebitAmt<>0 then numDebitAmt  end) as Deposit,        
null numBalance,        
isnull(GJD.numChartAcntId,0) as numChartAcntId,        
GJD.numTransactionId as numTransactionId,        
GJH.numOppId as numOppId,        
GJH.numOppBizDocsId as numOppBizDocsId,        
GJH.numDepositId as numDepositId,        
GJH.numBizDocsPaymentDetId as numBizDocsPaymentDetId,  
isnull(GJH.numCategoryHDRID,0) as numCategoryHDRID,  
isnull(TE.tintTEType,0) as tintTEType,  
TE.numCategory as numCategory,  
TE.numUserCntID as numUserCntID,
TE.dtFromDate as dtFromDate,
isnull(CAST(CD.numCheckNo AS VARCHAR(20)),'''') numCheckNo,
isnull(GJD.bitReconcile,0) as bitReconcile,
isnull(GJD.bitCleared,0) as bitCleared             
From General_Journal_Header as GJH        
Left Outer Join General_Journal_Details as GJD  on GJH.numJournal_Id=GJD.numJournalId        
Left outer join DivisionMaster as DM on GJD.numCustomerId=DM.numDivisionID        
Left outer join CompanyInfo as CI on  DM.numCompanyID=CI.numCompanyId        
Left outer join Chart_Of_Accounts CA on GJD.numChartAcntId=CA.numAccountId        
Left outer join CashCreditCardDetails CCCD on GJH.numCashCreditCardId=CCCD.numCashCreditId        
Left outer join CheckDetails CD on GJH.numCheckId=CD.numCheckId        
Left outer join OpportunityMaster Opp on GJH.numOppId=Opp.numOppId   
Left outer join TimeAndExpense TE on GJH.numCategoryHDRID=TE.numCategoryHDRID        
Where GJH.numDomainId='+convert(varchar(4),@numDomainId) + ' And (GJD.numCustomerId = ' + CONVERT(VARCHAR(10),@numDivisionID) + ' or '+ CONVERT(VARCHAR(10),@numDivisionID) +' = 0)'
+ ' ORDER BY GJH.datEntry_Date'
--And GJH.numChartAcntId is null      
                                    
End                                                                          
                                                                          
Print (@strChildCategory)                                   
Print (@strSQL)                                                                                                                      
--Exec (@strSQL)                                                                                                  
End                                                                                                 
                                                                   
Else                 
Begin                                                                               
--Set @strChildCategory = dbo.fn_ChildCategory((select MAX(numChartAcntId) from #TempCoa),@numDomainId) + Convert(varchar(10),(select MAX(numChartAcntId) from #TempCoa))                                                                                                               
----Set @strChildCategory = @strChildCategory + ',' + dbo.fn_ChildCategory((select MIN(numChartAcntId) from #TempCoa),@numDomainId) + Convert(varchar(10),(select MIN(numChartAcntId) from #TempCoa))                                                                                                               

Select @numAcntTypeId=isnull(numParntAcntTypeId,0) from Chart_of_Accounts Where numAccountId in (select MAX(numChartAcntId) from #TempCoa )                                                                          
                                                                                          
if @numAcntTypeId=815 or @numAcntTypeId=816 or @numAcntTypeId=820  or @numAcntTypeId=821  or @numAcntTypeId=822  or @numAcntTypeId=825 or @numAcntTypeId=827                                            
Begin                                                               
                                   
                               
Set @strSQL=@strSQL + ' Select GJD.numCustomerId,GJH.numJournal_Id as JournalId,         
case when isnull(GJH.numCheckId,0) <> 0 then +''Checks'' Else case when isnull(GJH.numCashCreditCardId,0) <> 0 And CCCD.bitMoneyOut=0 then ''Cash''        
ELse Case when isnull(GJH.numCashCreditCardId,0) <> 0 And CCCD.bitMoneyOut=1 And CCCD.bitChargeBack=1  then  ''Charge''        
ELse Case when isnull(GJH.numOppId,0) <> 0 And isnull(GJH.numOppBizDocsId,0) <> 0 And isnull(GJH.numBizDocsPaymentDetId,0)=0 And Opp.tintOppType=1 then ''BizDocs Invoice''        
ELse Case when isnull(GJH.numOppId,0) <> 0 And isnull(GJH.numOppBizDocsId,0) <> 0 And isnull(GJH.numBizDocsPaymentDetId,0)=0 And Opp.tintOppType=2 then ''BizDocs Purchase''        
ELse Case when isnull(GJH.numDepositId,0) <> 0 then ''Deposit''        
ELse Case when isnull(GJH.numBizDocsPaymentDetId,0) <> 0 And Opp.tintOppType=1 then ''Receive Amt''        
ELse Case when isnull(GJH.numBizDocsPaymentDetId,0)<>0 And Opp.tintOppType=2 then ''Vendor Amt''        
Else Case when isnull(GJH.numCategoryHDRID,0)<>0 then ''Time And Expenses''  
Else Case When GJH.numJournal_Id <>0 then ''Journal'' End End End End End End End End End End as TransactionType,        
GJH.datEntry_Date as EntryDate,CI.vcCompanyName CompanyName,GJH.numCheckId As CheckId,        
GJH.numCashCreditCardId as CashCreditCardId,        
GJH.varDescription as Memo,        
(case when GJD.numCreditAmt=0 then GJD.numdebitAmt  end) as Deposit,        
(case when GJD.numdebitAmt=0 then GJD.numCreditAmt end) as Payment,        
null numBalance,        
isnull(GJD.numChartAcntId,0) as numChartAcntId,        
GJD.numTransactionId as numTransactionId,        
GJH.numOppId as numOppId,        
GJH.numOppBizDocsId as numOppBizDocsId,        
GJH.numDepositId as numDepositId,        
GJH.numBizDocsPaymentDetId as numBizDocsPaymentDetId,  
isnull(GJH.numCategoryHDRID,0) as numCategoryHDRID,  
isnull(TE.tintTEType,0) as tintTEType,  
TE.numCategory as numCategory,  
TE.numUserCntID as numUserCntID,
TE.dtFromDate as dtFromDate,
isnull(CAST(CD.numCheckNo AS VARCHAR(20)),'''') numCheckNo,
isnull(GJD.bitReconcile,0) as bitReconcile,
isnull(GJD.bitCleared,0) as bitCleared 
From General_Journal_Header as GJH        
Left Outer Join General_Journal_Details as GJD on GJH.numJournal_Id=GJD.numJournalId        
Left outer join DivisionMaster as DM on GJD.numCustomerId=DM.numDivisionID        
Left outer join CompanyInfo as CI on DM.numCompanyID=CI.numCompanyId        
Left outer join CashCreditCardDetails CCCD on GJH.numCashCreditCardId=CCCD.numCashCreditId        
Left outer join CheckDetails CD on GJH.numCheckId=CD.numCheckId        
Left outer join OpportunityMaster Opp on GJH.numOppId=Opp.numOppId   
Left outer join TimeAndExpense TE on GJH.numCategoryHDRID=TE.numCategoryHDRID        
Where GJH.numDomainId='+convert(varchar(4),@numDomainId) + ' And GJH.datEntry_Date>=''' + convert(varchar(300),@dtDateFrom) +''' And (GJD.numCustomerId = ' + CONVERT(VARCHAR(10),@numDivisionID) + ' or '+ CONVERT(VARCHAR(10),@numDivisionID) +' = 0)'                                       
+ ' ORDER BY GJH.datEntry_Date'  
    
     
        
End                                                                          
Else                            
Begin                                                                         
                                                                     
                                
                              
Set @strSQL=' Select GJD.numCustomerId,GJH.numJournal_Id as JournalId,        
case when isnull(GJH.numCheckId,0) <> 0 then +''Checks'' Else case when isnull(GJH.numCashCreditCardId,0) <> 0 And CCCD.bitMoneyOut=0 then ''Cash''        
ELse Case when isnull(GJH.numCashCreditCardId,0) <> 0 And CCCD.bitMoneyOut=1 And CCCD.bitChargeBack=1  then ''Charge''        
ELse Case when isnull(GJH.numOppId,0) <> 0 And isnull(GJH.numOppBizDocsId,0) <> 0 And isnull(GJH.numBizDocsPaymentDetId,0)=0 And Opp.tintOppType=1 then ''BizDocs Invoice''        
ELse Case when isnull(GJH.numOppId,0) <> 0 And isnull(GJH.numOppBizDocsId,0) <> 0 And isnull(GJH.numBizDocsPaymentDetId,0)=0 And Opp.tintOppType=2 then ''BizDocs Purchase''        
ELse Case when isnull(GJH.numDepositId,0) <> 0 then ''Deposit''        
ELse Case when isnull(GJH.numBizDocsPaymentDetId,0)<>0 And Opp.tintOppType=1 then ''Receive Amt''        
ELse Case when isnull(GJH.numBizDocsPaymentDetId,0)<>0 And Opp.tintOppType=2 then ''Vendor Amt''    
Else Case when isnull(GJH.numCategoryHDRID,0)<>0 then ''Time And Expenses''      
Else Case When GJH.numJournal_Id <>0 then ''Journal'' End End End End End End End End End End as TransactionType,        
GJH.datEntry_Date as EntryDate,CI.vcCompanyName CompanyName,GJH.numCheckId As CheckId,        
GJH.numCashCreditCardId as CashCreditCardId,        
GJH.varDescription as Memo, 
(case when GJD.numCreditAmt<>0 then GJD.numCreditAmt end) as Payment,        
(case when GJD.numdebitAmt<>0 then GJD.numDebitAmt  end) as Deposit,        
null numBalance,        
isnull(GJD.numChartAcntId,0) as numChartAcntId,        
GJD.numTransactionId as numTransactionId,        
GJH.numOppId as numOppId,        
GJH.numOppBizDocsId as numOppBizDocsId,        
GJH.numDepositId as numDepositId,        
GJH.numBizDocsPaymentDetId as numBizDocsPaymentDetId,  
isnull(GJH.numCategoryHDRID,0) as numCategoryHDRID,  
isnull(TE.tintTEType,0) as tintTEType,  
TE.numCategory as numCategory,  
TE.numUserCntID as numUserCntID,
TE.dtFromDate as dtFromDate,
isnull(CAST(CD.numCheckNo AS VARCHAR(20)),'''') numCheckNo,
isnull(GJD.bitReconcile,0) as bitReconcile,
isnull(GJD.bitCleared,0) as bitCleared
From General_Journal_Header as GJH        
Left Outer Join General_Journal_Details as GJD  on GJH.numJournal_Id=GJD.numJournalId        
Left outer join DivisionMaster as DM on GJD.numCustomerId=DM.numDivisionID        
Left outer join CompanyInfo as CI on  DM.numCompanyID=CI.numCompanyId        
Left outer join CashCreditCardDetails CCCD on GJH.numCashCreditCardId=CCCD.numCashCreditId        
Left outer join CheckDetails CD on GJH.numCheckId=CD.numCheckId        
Left outer join OpportunityMaster Opp on GJH.numOppId=Opp.numOppId        
Left outer join TimeAndExpense TE on GJH.numCategoryHDRID=TE.numCategoryHDRID   
Where GJH.numDomainId='+convert(varchar(4),@numDomainId) + 'And GJH.datEntry_Date>=''' + convert(varchar(300),@dtDateFrom) +''' And (GJD.numCustomerId = ' + CONVERT(VARCHAR(10),@numDivisionID) + ' or '+ CONVERT(VARCHAR(10),@numDivisionID) +' = 0)'                                    
+ ' ORDER BY GJH.datEntry_Date'   
    
      
       
          
            
               
                
                  
                    
                                   
End                                                                                                            
                                                                                                                     
Print (@strChildCategory)                                         
     Print (@strSQL)                                                                      --Exec (@strSQL)                                                                                                                   
End                                                                                                                                                                                 
exec ('insert into #tempTable' + @strSQL)                                                                                           
Declare @TotRecs as  numeric(9)                                                                                            
Declare @i as numeric(9)                                                                                                  
Declare @numCustId as varchar(9)                                                       
Declare @numPayment as DECIMAL(20,5)                                                                                          
Declare @numDeposit as DECIMAL(20,5)                                                             
Declare @numBalanceAmt as DECIMAL(20,5)                                                                                                
Declare @bitBalance as bit                                                                 
Declare @numCheckId as numeric(9)                                                            
Declare @numCashCreditCardId as numeric(9)          
Declare @numOppBizDocsId as numeric(9)                                                                
--Declare @numCashCreditCardId as numeric(9)                                                                                         
Set @bitBalance=0                         
Declare @j as numeric(9)                                     
set @i=1                                                                                                  
Set @j=0                                               
                                         
                                                        
                                                                                               
set @TotRecs=(select count(*) from #tempTable)                                                                                        
print (@TotRecs)                                                                                                  
Declare @numCustomerId as numeric(9)                                                               
Declare @bitChargeBack as bit                                                
Declare @bitMoneyOut as bit                                  
                                                          
While @i<=@TotRecs                                                                              
Begin                                                                  
 Select @numCustId=isnull(numCustomerId,0),@numPayment=isnull(Payment,0),@numDeposit=isnull(Deposit,0),@numCheckId=isnull(CheckId,0),@numCashCreditCardId=isnull(CashCreditCardId,0),@numOppBizDocsId=isnull(numOppBizDocsId,0) From #temptable Where Id=@i   
  
     
      
       
                                                      
                    
 print '@CheckId1' + convert(varchar(30),@numCheckId)                                                 
 Select @bitChargeBack=bitChargeBack,@bitMoneyOut=bitMoneyOut From CashCreditCardDetails Where numCashCreditId=@numCashCreditCardId And numDomainId=@numDomainId                                                      
 print @bitChargeBack                                               
 print @bitMoneyOut                                                   
 if @i=1                                                                                                  
 Begin                                                                                                  
   If @numPayment <> 0                          
  Begin                                                                
  -- print '@numPayment' +convert(varchar(50),@numPayment)                                                              
 --  Print 'Siva'                                                               
 -- print '@numAcntTypeId' +Convert(varchar(50),@numAcntTypeId)                                                     
    Update #temptable Set numBalance=-@numPayment Where Id=@i                                                          
     if @numAcntTypeId=815                                                 
     Begin                                                            
      if @numCheckId<>0 Or @numCashCreditCardId<>0                                                           
       Begin                                                        
          if  @numCashCreditCardId<>0                                               
            Begin                                              
              if @bitChargeBack=0 And @bitMoneyOut=1                                              
                   Update #temptable Set Deposit=null,Payment=-@numPayment Where Id=@i                                               
          Else                                              
                     Update #temptable Set Deposit=null,Payment=@numPayment Where Id=@i                                                             
             End                                              
            Else                                              
                   Update #temptable Set Deposit=null,Payment=@numPayment Where Id=@i                                               
                                                      
                                                          
        End                                                        
      Else                                            
        Update #temptable Set Deposit=-@numPayment,Payment=null Where Id=@i                                                               
     End                                                       
     if @numAcntTypeId=814                                                                 
      Update #temptable Set Deposit=null,Payment=@numPayment Where Id=@i                                                               
                                                                                               
  End                                                                                                  
  else                                                                 
    Begin                    
                                                                                               
    Update  #temptable Set numBalance=@numDeposit Where Id=@i                                                  
    if @numAcntTypeId=815                                                                   
    Begin                                                            
     if @numCheckId<>0 Or @numCashCreditCardId<>0                                                          
        Begin                                                        
           if  @numCashCreditCardId<>0                                               
            Begin                                              
              if @bitChargeBack=0 And @bitMoneyOut=1                                              
                   Update #temptable Set Deposit=null,Payment=@numDeposit Where Id=@i                                                                  
               Else                                              
                   Update #temptable Set Deposit=null,Payment=-@numDeposit Where Id=@i                                                                  
              End                                                
             Else                                              
            Update #temptable Set Deposit=null,Payment=-@numDeposit Where Id=@i                                                                  
                                      
       End                                                        
     else                                                            
        Update #temptable Set Deposit=@numDeposit,Payment=null Where Id=@i                                                                  
    End                                                            
    if @numAcntTypeId=814 And @numOppBizDocsId=0                  
       Update #temptable Set Deposit=null,Payment=-@numDeposit Where Id=@i                                                       
 Else          
       Update #temptable Set Deposit=@numDeposit,Payment=null Where Id=@i              
    End                                                                                     
  End                                                                                                  
                                                                                                   
if @i>1                                                                                               
                                                                    
Begin                                                                                                  
if @numPayment <> 0                                                                                                  
Begin                                                                                                  
   Set @j=@i-1                                    
   Select @numBalanceAmt=numBalance From #temptable Where id=@j                                                                                                  
   Update #temptable Set numBalance=@numBalanceAmt-@numPayment Where Id=@i                                                               
   if @numAcntTypeId=815                                                                  
   Begin                                                            
   if @numCheckId<>0 Or @numCashCreditCardId<>0                                                            
       Begin                                                    
    if  @numCashCreditCardId<>0                                               
            Begin                                              
              if @bitChargeBack=0 And @bitMoneyOut=1                                                  
         Update  #temptable Set Deposit=null,Payment=-@numPayment Where Id=@i            
             Else                                      
         Update  #temptable Set Deposit=null,Payment=@numPayment Where Id=@i                                                
         End                                              
      Else                                                        
         Update #temptable Set Deposit=null,Payment=@numPayment Where Id=@i                                                
                                                      
       End                                                            
    Else                                                            
     Update  #temptable Set Deposit=-@numPayment,Payment=null Where Id=@i                                                                 
    End                                                            
   if @numAcntTypeId=814                                                                             
   Update #temptable Set Deposit=null,Payment=@numPayment Where Id=@i                                                                        
End                                                                                                  
Else                                                                                                  
Begin                                                                                                  
   Set @j=@i-1                                                   
    print '@CheckId2' + convert(varchar(30),@numCheckId)                                                                                                  
   Select @numBalanceAmt=numBalance From #temptable Where id=@j                                                                      
   Update #temptable Set numBalance=@numBalanceAmt+@numDeposit Where Id=@i                                                                   
    if @numAcntTypeId=815                                                                 
  Begin                                                            
     If @numCheckId<>0 Or @numCashCreditCardId<>0                                                           
       Begin                                                        
 if  @numCashCreditCardId<>0                                               
            Begin                                              
              if @bitChargeBack=0 And @bitMoneyOut=1                                              
           Update #temptable Set Deposit=null,Payment=-@numDeposit Where Id=@i                                                   
             Else                         
           Update #temptable Set Deposit=null,Payment=@numDeposit Where Id=@i                                                   
                                              
           End                                          
           Else                                              
           Update  #temptable Set Deposit=null,Payment=@numDeposit Where Id=@i                                                   
                                                           
       End                                                        
     Else                                                            
      Update #temptable Set Deposit=@numDeposit,Payment=null Where Id=@i                   
     End                                                            
     if @numAcntTypeId=814 And @numOppBizDocsId=0                                    
      Update #temptable Set Deposit=null,Payment=-@numDeposit Where Id=@i                                                                                        
     Else          
      Update #temptable Set Deposit=@numDeposit,Payment=null Where Id=@i                                                                                        
          
End                                            
End                                                                                                  
Set @i=@i+1                                                    
End                  
drop table #TempCoa;                                                                            
Select numCustomerID,JournalId,TransactionType,EntryDate,CompanyName,
CheckID,CashCreditCardID,Memo,MAX(isnull(Payment,0)) AS Payment, max(isnull(Deposit,0)) as Deposit,max(isnull(numBalance,0)) as numBalance ,0 as numChartAcntId, 0 as numTransactionID
 ,numOppID,numOppBizDocsId,numDepositId,numBizDocsPaymentDetId,numCategoryHDRID,
tintTEType,numCategory,numUserCntId,dtFromDate,numCheckNo  from #temptable
group by 
numCustomerID,JournalId,TransactionType,EntryDate,CompanyName,
CheckID,CashCreditCardID,Memo
 ,numOppID,numOppBizDocsId,numDepositId,numOppID,numOppBizDocsId,numBizDocsPaymentDetId,numCategoryHDRID,
tintTEType,numCategory,numUserCntId,dtFromDate,numCheckNo 
                                                                                              
Drop table #temptable                                                                                                                 
                                                                                              
End
