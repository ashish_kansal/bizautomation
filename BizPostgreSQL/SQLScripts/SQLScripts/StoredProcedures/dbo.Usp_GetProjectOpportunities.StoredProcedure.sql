/****** Object:  StoredProcedure [dbo].[Usp_GetProjectOpportunities]    Script Date: 07/26/2008 16:18:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getprojectopportunities')
DROP PROCEDURE usp_getprojectopportunities
GO
CREATE PROCEDURE [dbo].[Usp_GetProjectOpportunities]
@numProjectId numeric,
@numDomainId numeric

as


select OpportunityMaster.numOppID,vcPOppName from ProjectsOpportunities 
join OpportunityMaster
on ProjectsOpportunities.numOppID=OpportunityMaster.numOppID
where numproid=@numProjectId and OpportunityMaster.numDomainId=@numDomainId
GO
