/****** Object:  StoredProcedure [dbo].[usp_getCustReportCheckedFields]    Script Date: 07/26/2008 16:17:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getcustreportcheckedfields')
DROP PROCEDURE usp_getcustreportcheckedfields
GO
CREATE PROCEDURE [dbo].[usp_getCustReportCheckedFields]
@ReportId as numeric =0
as
select * from CustReportFields where numcustomReportId =@ReportId
GO
