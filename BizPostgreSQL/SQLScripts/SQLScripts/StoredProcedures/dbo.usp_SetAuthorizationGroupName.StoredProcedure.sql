/****** Object:  StoredProcedure [dbo].[usp_SetAuthorizationGroupName]    Script Date: 07/26/2008 16:21:15 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_setauthorizationgroupname')
DROP PROCEDURE usp_setauthorizationgroupname
GO
CREATE PROCEDURE [dbo].[usp_SetAuthorizationGroupName]
	@numGroupID NUMERIC(9) = 0,
	@vcGroupName VARCHAR(30)   
--
AS
	IF @numGroupID = 0
		BEGIN
			DECLARE @numGrpCount INT
			SELECT @numGrpCount = COUNT(*) FROM AuthenticationGroupMaster WHERE vcGroupName = @vcGroupName
			IF @numGrpCount <= 0
				BEGIN
					INSERT INTO AuthenticationGroupMaster(vcGroupName)  VALUES(@vcGroupName)
					SELECT  SCOPE_IDENTITY()
				END
			ELSE
				BEGIN
					SELECT 0
				END
		END
	ELSE
		BEGIN
			UPDATE AuthenticationGroupMaster 
			SET vcGroupName = @vcGroupName 
			WHERE numGroupID = @numGroupID
		END
GO
