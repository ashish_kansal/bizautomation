/****** Object:  StoredProcedure [dbo].[usp_SetFollowUpStatusForSelectedRecords]    Script Date: 07/26/2008 16:21:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created By: Debasish Tapan Nag                            
--Purpose: Deletes the selected Records of Advance Search          
--Created Date: 08/22/2005                       
--Altered Date: 03/17/2006: Changed length of @vcEntityIdList to 4000      
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_setfollowupstatusforselectedrecords')
DROP PROCEDURE usp_setfollowupstatusforselectedrecords
GO
CREATE PROCEDURE [dbo].[usp_SetFollowUpStatusForSelectedRecords]          
  @vcEntityIdList NVarchar(4000),          
  @numFollowUpStatusId Numeric          
AS                                          
BEGIN                           
 SET @vcEntityIdList = @vcEntityIdList + ','          
 DECLARE @intFirstComma integer            
 DECLARE @intFirstUnderScore integer            
          
 DECLARE @numCompanyId NUMERIC(9)          
 DECLARE @numContactId NUMERIC(9)          
 DECLARE @numDivisionId NUMERIC(9)          
          
 DECLARE @vcCompContDivValue NVarchar(30)          
 DECLARE @recordCount Numeric          
           
 SET @intFirstComma = CHARINDEX(',', @vcEntityIdList, 0)            
          
 WHILE @intFirstComma > 0          
 BEGIN          
  SET @vcCompContDivValue = SUBSTRING(@vcEntityIdList,0,@intFirstComma)          
  SET @intFirstUnderScore = CHARINDEX('_', @vcCompContDivValue, 0)          
  SET @numCompanyId = SUBSTRING(@vcCompContDivValue, 0, @intFirstUnderScore)          
  SET @vcCompContDivValue = SUBSTRING(@vcCompContDivValue, @intFirstUnderScore+1, Len(@vcCompContDivValue))          
  SET @intFirstUnderScore = CHARINDEX('_', @vcCompContDivValue, 0)          
  SET @numContactId = SUBSTRING(@vcCompContDivValue, 0, @intFirstUnderScore)          
  SET @numDivisionId = SUBSTRING(@vcCompContDivValue, @intFirstUnderScore+1, Len(@vcCompContDivValue))          
         PRINT  @numCompanyId          
         PRINT  @numContactId          
         PRINT  @numDivisionId          
          
   UPDATE DivisionMaster SET numFollowUpStatus = @numFollowUpStatusId          
          WHERE numDivisionID = @numDivisionId          
   AND numCompanyID = @numCompanyId          
           
   INSERT INTO FollowUpHistory (numFollowUpstatus, numDivisionID, bintAddedDate)        
   VALUES(@numFollowUpStatusId, @numDivisionId, getutcdate())        
        
   SELECT @recordCount = @@ROWCOUNT          
          
   SET @vcEntityIdList = SUBSTRING(@vcEntityIdList,@intFirstComma+1,Len(@vcEntityIdList))            
   SET @intFirstComma = CHARINDEX(',', @vcEntityIdList, 0)            
 END          
 SELECT @recordCount          
END
GO
