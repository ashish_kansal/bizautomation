/****** Object:  StoredProcedure [dbo].[usp_GetCompanyDivContFromUniverSuppKey]    Script Date: 07/26/2008 16:16:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getcompanydivcontfromuniversuppkey')
DROP PROCEDURE usp_getcompanydivcontfromuniversuppkey
GO
CREATE PROCEDURE [dbo].[usp_GetCompanyDivContFromUniverSuppKey]
	@numUSuppKey numeric
	--
AS
BEGIN
	DECLARE @numDivID numeric
	SELECT @numDivID=numDivisionID FROM UniversalSupportKeyMaster WHERE numUniversalSupportKey=@numUSuppKey
	
	SELECT DM.numDivisionID, DM.vcDivisionName, CM.numCompanyID, CM.vcCompanyName, 
	ACI.numContactID, ACI.vcFirstName, ACI.vcLastName 
		FROM DivisionMaster DM INNER JOIN CompanyInfo CM ON DM.numCompanyID=CM.numCompanyId INNER JOIN AdditionalcontactsInformation ACI ON ACI.numDivisionID=DM.numDivisionID
		WHERE DM.numDivisionID=@numDivID
		ORDER BY ACI.vcFirstName, ACI.vcLastName 

END
GO
