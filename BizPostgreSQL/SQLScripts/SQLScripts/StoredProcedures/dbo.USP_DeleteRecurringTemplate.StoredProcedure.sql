/****** Object:  StoredProcedure [dbo].[USP_DeleteRecurringTemplate]    Script Date: 07/26/2008 16:15:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created by Siva
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_deleterecurringtemplate')
DROP PROCEDURE usp_deleterecurringtemplate
GO
CREATE PROCEDURE [dbo].[USP_DeleteRecurringTemplate]
@numRecurringId as numeric(9)=0,
@numDomainId as numeric(9)=0
AS
Begin
	Delete From RecurringTemplate Where numRecurringId=@numRecurringId And numDomainId=@numDomainId

End
GO
