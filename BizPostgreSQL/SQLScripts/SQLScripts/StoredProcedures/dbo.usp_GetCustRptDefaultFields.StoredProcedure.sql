/****** Object:  StoredProcedure [dbo].[usp_GetCustRptDefaultFields]    Script Date: 07/26/2008 16:17:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getcustrptdefaultfields')
DROP PROCEDURE usp_getcustrptdefaultfields
GO
CREATE PROCEDURE [dbo].[usp_GetCustRptDefaultFields]                                  
@numReportOptionsTypeId numeric ,                           
@numDomainId numeric =1  ,                      
@numContactTypeId numeric =0,                      
@numRelationTypeId numeric =0                       
                      
AS                                    
BEGIN                   
                                                                      
declare @strSql as varchar(8000)                                                                
 set  @strSql=' '                   
                  
 set  @strSql =@strSql + 'SELECT rel.numFieldGroupID, fld.vcDbFieldName, fld.vcScrFieldName,                                
      fld.numOrderAppearance, fld.boolSummationPossible, fld.boolDefaultColumnSelection,vcFieldType  ,                           
     ListId                       
     ,0 as Ctype ,fld.tintMyself ,fld.tintTeam ,fld.tintTerr                     
     FROM CustRptAndFieldGroupRelation rel, CustRptDefaultFields fld                                
     WHERE rel.numFieldGroupID = fld.numFieldGroupID                                
     AND rel.numReportOptionTypesId = '+ convert(varchar(15),@numReportOptionsTypeId)                   
                  
if @numReportOptionsTypeId = 5 or @numReportOptionsTypeId =6 or @numReportOptionsTypeId =7 or @numReportOptionsTypeId =61                  
begin                  
 set  @strSql =@strSql + '                  
 union                  
 SELECT 7 AS numFieldGroupID, ''CFld'' +  convert(varchar(15),m.fld_id) AS vcDbFieldName, m.fld_label AS vcScrFieldName, 0                          
  AS  numOrderAppearance, 0 AS boolSummationPossible, 0 AS boolDefaultColumnSelection ,                      
  fld_type as vcFieldType, isnull(numListId,0) as ListId                      
  ,1 as Ctype  ,0 as tintMyself,0 as  tintTeam,0 as  tintTerr                       
  FROM CFW_Fld_Master m, CFW_Fld_Dtl d                          
    WHERE                       
     m.Fld_id = d.numFieldId                             
     and m.Grp_id = 1 --Prospect/ Accounts                          
     AND m.numDomainID = '+ convert(varchar(15),@numDomainId) +  '                   
   AND fld_type <> ''Link'''                     
                    
 -- if @numRelationTypeId <> 0 set  @strSql =@strSql +  'AND d.numRelation ='+ convert(varchar(15),@numRelationTypeId) +  ''                  
  end                  
if @numReportOptionsTypeId = 4 or @numReportOptionsTypeId =5                  
begin                  
  set  @strSql =@strSql + '                  
 union                  
    SELECT 6 AS numFieldGroupID, ''CFld'' +  convert(varchar(15),m.fld_id) AS vcDbFieldName, m.fld_label AS vcScrFieldName, 0                          
  AS  numOrderAppearance, 0 AS boolSummationPossible, 0 AS boolDefaultColumnSelection   ,                      
  Fld_type as vcFieldType, isnull(numListId,0) as ListId                         
  ,1 as Ctype   ,0 as tintMyself,0 as  tintTeam,0 as  tintTerr                           
  FROM CFW_Fld_Master m,CFW_Fld_Dtl d                          
    WHERE                      
    m.Fld_id = d.numFieldId      and 
	m.Grp_id = 4 and                   
    m.numDomainID = '+ convert(varchar(15),@numDomainId) +  '                     
                              
   AND fld_type <> ''Link'' '                  
 --if @numRelationTypeId <> 0 set  @strSql =@strSql + 'AND d.numRelation ='+ convert(varchar(15),@numContactTypeId) +  ''                  
end                
              
if @numReportOptionsTypeId = 12 or @numReportOptionsTypeId = 13                  
begin                  
  set  @strSql =@strSql + '                  
 union                  
    SELECT 15 AS numFieldGroupID, ''CFld'' +  convert(varchar(15),m.fld_id) AS vcDbFieldName, m.fld_label AS vcScrFieldName, 0                          
  AS numOrderAppearance, 0 AS boolSummationPossible, 0 AS boolDefaultColumnSelection   ,                      
  fld_type as vcFieldType, isnull(numListId,0) as ListId                 
  ,1 as Ctype    ,0 as tintMyself,0 as  tintTeam,0 as  tintTerr                          
  FROM CFW_Fld_Master m              
--,CFW_Fld_Dtl d                          
    WHERE                      
  --  m.Fld_id = d.numFieldId                      
-- and               
m.Grp_id = 3 --Cases                   
    and m.numDomainID = '+ convert(varchar(15),@numDomainId) +  '                     
                              
   AND fld_type <> ''Link'' '                  
                 
end              
      
if @numReportOptionsTypeId = 14 or @numReportOptionsTypeId = 15 or @numReportOptionsTypeId = 16 or @numReportOptionsTypeId = 17 or @numReportOptionsTypeId = 18 or @numReportOptionsTypeId = 19                 
begin                  
  set  @strSql =@strSql + '                  
 union                  
    SELECT 18 AS numFieldGroupID, ''CFld'' +  convert(varchar(15),m.fld_id) AS vcDbFieldName, m.fld_label AS vcScrFieldName, 0                          
  AS  numOrderAppearance, 0 AS boolSummationPossible, 0 AS boolDefaultColumnSelection   ,                      
  fld_type as vcFieldType, isnull(numListId,0) as ListId                         
  ,1 as Ctype    ,0 as tintMyself,0 as  tintTeam,0 as  tintTerr                          
  FROM CFW_Fld_Master m              
--,CFW_Fld_Dtl d                          
    WHERE                      
  --  m.Fld_id = d.numFieldId                      
-- and               
(m.Grp_id = 2 or m.Grp_id = 6)      
    and m.numDomainID = '+ convert(varchar(15),@numDomainId) +  '                     
                              
   AND fld_type <> ''Link'' '                  
                 
end     

if @numReportOptionsTypeId = 8 or @numReportOptionsTypeId = 10
begin                  
  set  @strSql =@strSql + '                  
 union                  
 SELECT 9 AS numFieldGroupID, ''CFld'' +  convert(varchar(15),m.fld_id) AS vcDbFieldName, m.fld_label AS vcScrFieldName, 0                          
  AS  numOrderAppearance, 0 AS boolSummationPossible, 0 AS boolDefaultColumnSelection ,                      
  fld_type as vcFieldType, isnull(numListId,0) as ListId                      
  ,1 as Ctype  ,0 as tintMyself,0 as  tintTeam,0 as  tintTerr                       
  FROM CFW_Fld_Master m, CFW_Fld_Dtl d                          
    WHERE                       
     m.Fld_id = d.numFieldId                             
     and m.Grp_id = 11 --Projects                         
     AND m.numDomainID = '+ convert(varchar(15),@numDomainId) +  '                   
   AND fld_type <> ''Link'''                     
                 
end      
            
if @numReportOptionsTypeId = 20 or @numReportOptionsTypeId = 21 or @numReportOptionsTypeId = 22 or @numReportOptionsTypeId = 23 
begin                  
  set  @strSql =@strSql + '                  
 union                  
 SELECT 27 AS numFieldGroupID, ''CFld'' +  convert(varchar(15),m.fld_id) AS vcDbFieldName, m.fld_label AS vcScrFieldName, 0                          
  AS  numOrderAppearance, 0 AS boolSummationPossible, 0 AS boolDefaultColumnSelection ,                      
  fld_type as vcFieldType, isnull(numListId,0) as ListId                      
  ,1 as Ctype  ,0 as tintMyself,0 as  tintTeam,0 as  tintTerr                       
  FROM CFW_Fld_Master m
--, CFW_Fld_Dtl d                          
    WHERE                       
--     m.Fld_id = d.numFieldId   and                          
 m.Grp_id = 5 --Items                         
     AND m.numDomainID = '+ convert(varchar(15),@numDomainId) +  '                   
   AND fld_type <> ''Link'''                     
                 
end      
                
set  @strSql =@strSql +'order by numFieldGroupID,Ctype, numOrderAppearance'                
exec (@strSql)                  
print @strSql                  
end
GO
