/****** Object:  StoredProcedure [dbo].[USP_GetImportConfiguration]    Script Date: 07/26/2008 16:17:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getimportconfiguration')
DROP PROCEDURE usp_getimportconfiguration
GO
set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go
CREATE  PROCEDURE [dbo].[USP_GetImportConfiguration]
@numRelation as numeric,              
@numDomain as numeric,
@ImportType as tinyint             
as                
--    
--if  (select count(*) from RecImportConfg where numDomainid=@numDomain and numRelationShip=@numRelation)=0    
--set @numDomain = 0    
IF @ImportType=1
BEGIN   

--Available fields               
(select vcFormFieldName,convert(varchar(10),numFormFieldId) +'/0'+'/R' as numFormFieldId from DynamicFormFieldMaster where numFormID=9               
and numFormFieldId not in (select numFormFieldId from RecImportConfg where numDomainid=@numDomain and bitCustomFld=0 and cCtype='R' and numRelationShip=@numRelation and Importtype=1)              
              
union              
select fld_label as vcFormFieldName ,convert(varchar(10),fld_id)+ '/1'+'/L' as numFormFieldId  from CFW_Fld_Master            
 join CFW_Fld_Dtl                                          
on Fld_id=numFieldId                                          
left join CFw_Grp_Master                                           
on subgrp=CFw_Grp_Master.Grp_id                                          
where CFW_Fld_Master.grp_id IN (1,12,13,14) /*Leads/Prospects/Accounts*/ and numRelation=@numRelation and CFW_Fld_Master.numDomainID=@numDomain  and fld_id not in (select DTL.numFormFieldId from                   
RecImportConfg dtl where  DTL.numDomainID=@numDomain and bitCustomFld=1 and cCtype='L' and numRelationShip=@numRelation  and Importtype=1)          
          
union              
select fld_label as vcFormFieldName ,convert(varchar(10),fld_id)+ '/1'+'/C' as numFormFieldId  from CFW_Fld_Master            
 join CFW_Fld_Dtl                                          
on Fld_id=numFieldId                                          
left join CFw_Grp_Master                                           
on subgrp=CFw_Grp_Master.Grp_id                                          
where CFW_Fld_Master.grp_id=4 /*Contact Details*/and numRelation=@numRelation and CFW_Fld_Master.numDomainID=@numDomain     
and fld_id not in (select DTL.numFormFieldId from                   
RecImportConfg dtl where  DTL.numDomainID=@numDomain and bitCustomFld=1 and cCtype='C' and numRelationShip=@numRelation and Importtype=1)          
) order by vcFormFieldName       
              
              
             
                
--Added fields          
select HDR.vcFormFieldName,convert(varchar(10),DTL.numFormFieldId) +'/0'+'/'+cCtype as numFormFieldId,intcolumn     
from RecImportConfg DTL             
join DynamicFormFieldMaster HDR on DTL.numFormFieldId = HDR.numFormFieldId            
where DTL.numDomainId=@numDomain and DTL.numRelationShip=@numRelation and bitCustomFld = 0 and Importtype=1          
  union          
select CFW.fld_label as vcFormFieldName,convert(varchar(10),DTL.numFormFieldId) +'/1'+'/'+cCtype as numFormFieldId,intcolumn    
 from RecImportConfg DTL             
join CFW_Fld_Master CFW on DTL.numFormFieldId = CFW.Fld_id            
where DTL.numDomainid=@numDomain and DTL.numRelationShip=@numRelation and bitCustomFld = 1   and Importtype=1        
order by intcolumn  


end
else  if @ImportType=2 OR @ImportType=4
begin   
               
(select vcFormFieldName,convert(varchar(10),numFormFieldId) +'/0'+'/R' as numFormFieldId from DynamicFormFieldMaster where bitDeleted=0 AND numFormID in( 20,27) --,26,27,28
and numFormFieldId not in (select numFormFieldId from RecImportConfg where numDomainid=@numDomain and bitCustomFld=0 and cCtype='R' and Importtype=@ImportType)
AND 
(
	@ImportType=2 OR 
	(vcDbColumnName <> CASE WHEN @ImportType=4 THEN   'numWareHouseID' ELSE vcDbColumnName END 
	AND  vcDbColumnName <> CASE WHEN @ImportType=4 THEN   'numOnHand' ELSE vcDbColumnName END 
	AND  vcDbColumnName <> CASE WHEN @ImportType=4 THEN   'vcLocation' ELSE vcDbColumnName END )
)
              
union              
select fld_label as vcFormFieldName ,convert(varchar(10),fld_id)+ '/1'+'/I' as numFormFieldId  from CFW_Fld_Master                                                    
left join CFw_Grp_Master                                           
on subgrp=CFw_Grp_Master.Grp_id                                          
where CFW_Fld_Master.grp_id=5  and CFW_Fld_Master.numDomainID=@numDomain  and fld_id not in (select DTL.numFormFieldId from                   
RecImportConfg dtl where  DTL.numDomainID=@numDomain and bitCustomFld=1 and cCtype='I'   and Importtype=@ImportType)          
) order by vcFormFieldName       
              
             
select HDR.vcFormFieldName,convert(varchar(10),DTL.numFormFieldId) +'/0'+'/'+cCtype as numFormFieldId,intcolumn     
from RecImportConfg DTL             
join DynamicFormFieldMaster HDR on DTL.numFormFieldId = HDR.numFormFieldId            
where bitDeleted=0 AND DTL.numDomainId=@numDomain  and bitCustomFld = 0 and Importtype=@ImportType         
AND 
(
	@ImportType=2 OR 
	(vcDbColumnName <> CASE WHEN @ImportType=4 THEN   'numWareHouseID' ELSE vcDbColumnName END 
	AND  vcDbColumnName <> CASE WHEN @ImportType=4 THEN   'numOnHand' ELSE vcDbColumnName END 
	AND  vcDbColumnName <> CASE WHEN @ImportType=4 THEN   'vcLocation' ELSE vcDbColumnName END )
)
  union          
select CFW.fld_label as vcFormFieldName,convert(varchar(10),DTL.numFormFieldId) +'/1'+'/'+cCtype as numFormFieldId,intcolumn    
 from RecImportConfg DTL             
join CFW_Fld_Master CFW on DTL.numFormFieldId = CFW.Fld_id            
where DTL.numDomainid=@numDomain and bitCustomFld = 1   and Importtype=@ImportType      
order by intcolumn  


end
else  if @ImportType=3
begin   
(select vcFormFieldName,convert(varchar(10),numFormFieldId)  as numFormFieldId,0 as intcolumn,convert(bit,0) as bitCustomFld,'R' as cCtype,vcAssociatedControlType,vcDbColumnName from DynamicFormFieldMaster where numFormID=25)              
end

else  if @ImportType=5 --Import Assembly Items
begin   
               
select vcFormFieldName,convert(varchar(10),numFormFieldId) as numFormFieldId,0 as intcolumn,convert(bit,0) as bitCustomFld,'R' as cCtype,vcAssociatedControlType,vcDbColumnName from DynamicFormFieldMaster where numFormID=31
 order by numFormFieldId       

end
else  if @ImportType=6 --Import Tax Details
begin   
               
select vcFormFieldName,convert(varchar(10),numFormFieldId) as numFormFieldId,0 as intcolumn,convert(bit,0) as bitCustomFld,'R' as cCtype,vcAssociatedControlType,vcDbColumnName from DynamicFormFieldMaster where numFormID=45
 order by numFormFieldId       

end
else  if @ImportType=7 --Import Update Item Warehouse Information/Matrix Item
begin   
               
select vcFormFieldName,convert(varchar(10),numFormFieldId) as numFormFieldId,[order],convert(bit,0) as bitCustomFld,'R' as cCtype,vcAssociatedControlType,vcDbColumnName,numListID from DynamicFormFieldMaster where numFormID=48
UNION
 select Fld_label as vcFormFieldName,Fld_id as numFormFieldId,100 as [order],convert(bit,0) as bitCustomFld,'C' as cCtype,fld_type as vcAssociatedControlType,'' as vcDbColumnName,CFW_Fld_Master.numListID
 from CFW_Fld_Master join ItemGroupsDTL on numOppAccAttrID=Fld_id where numItemGroupID=@numRelation and tintType=2 
order by [order] 

end
ELSE IF @ImportType IN (136,137,140)
BEGIN
	SELECT
		numFieldID
		,vcFieldName
	FROM 
		DycFormField_Mapping 
	WHERE 
		numFormID=@ImportType               
		AND numFieldID NOT IN (SELECT numFieldID FROM View_DynamicColumns WHERE numDomainid=@numDomain AND numFormId=@ImportType)
                         
	--Added fields          
	SELECT
		numFieldID
		,vcFieldName
	FROM 
		View_DynamicColumns                   
	WHERE 
		numDomainID=@numDomain 
		AND numFormId=@ImportType
END


