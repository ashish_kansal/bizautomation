/****** Object:  StoredProcedure [dbo].[USP_GetLeadboxFields]    Script Date: 07/26/2008 16:17:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getleadboxfields')
DROP PROCEDURE usp_getleadboxfields
GO
CREATE PROCEDURE [dbo].[USP_GetLeadboxFields]
@numDomainID as numeric(9)
as

select vcFieldname as vcFormFieldname,vcFieldType+'~'+vcAssociatedControlType+'~'+vcDBColumnName+'~'+vcListItemType+'~'+convert(varchar(15),numListID)+'~'+convert(varchar(15),numfieldID) as ID
from View_DynamicDefaultColumns where numDomainID=@numDomainID and numFormID=3 and vcDbColumnName!='bitSameAddr' 
union 
select 'Areas of Interests','AOI~AOI~AOI~AOI~0~0'
union 
select '--Select One--' ,'0'
GO
