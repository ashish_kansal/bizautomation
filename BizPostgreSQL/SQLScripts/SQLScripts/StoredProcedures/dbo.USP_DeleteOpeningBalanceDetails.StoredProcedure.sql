/****** Object:  StoredProcedure [dbo].[USP_DeleteOpeningBalanceDetails]    Script Date: 07/26/2008 16:15:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created By Siva              
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_deleteopeningbalancedetails')
DROP PROCEDURE usp_deleteopeningbalancedetails
GO
CREATE PROCEDURE [dbo].[USP_DeleteOpeningBalanceDetails]              
@numChartAcntId as numeric(9),              
@numDomainId as numeric(9)              
As              
Begin    
RETURN          
--  Declare @numOldOpeningBal as DECIMAL(20,5)               
--  Declare @strSQl as varchar(8000)                    
--  Declare @strSQl2 as varchar(8000)             
--  Declare @strSQLEquityBal as varchar(8000)          
--  Declare @numAcntType as integer        
--  Declare @numJournalId as numeric(9)   
--  Declare @numAccountIdOpeningEquity as numeric(9)          
--                  
--  Select @numOldOpeningBal= isnull(numOriginalOpeningBal,0),@numAcntType=numAcntType From Chart_Of_Accounts Where numAccountId=@numChartAcntId And numDomainId=@numDomainId                   
--    
-- Set @numAccountIdOpeningEquity=(Select numAccountId From Chart_Of_Accounts Where bitOpeningBalanceEquity=1  and numDomainId = @numDomainId)   
--             
--  Update Chart_Of_Accounts Set numOriginalOpeningBal=0 Where numAccountId=@numChartAcntId  And numDomainId=@numDomainId                 
--                    
--  Set @strSQl=dbo.fn_ParentCategory(@numChartAcntId,@numDomainId)                                              
--  Print ('strSQL'+@strSQl)                  
--  If @strSQl <>''              
--  Begin                                   
--   set @strSQl2='update chart_of_accounts  set numopeningbal=isnull(numopeningbal,0) - ' + convert(varchar(30),@numOldOpeningBal) + ' where  numAccountId in ('+convert(varchar(30),@numChartAcntId)+','+@strSQl+') And numDomainId='+Convert(varchar(10),@numDomainId)                                           
--      If @numAcntType=813          
--    Begin    
--     set @strSQLEquityBal='update chart_of_accounts  set numOriginalOpeningBal=isnull(numOriginalOpeningBal,0) - ' + convert(varchar(30),@numOldOpeningBal) +', numopeningbal=isnull(numopeningbal,0) - ' + convert(varchar(30),@numOldOpeningBal) + ' where   
-- 
--           numAccountId='+convert(varchar(10),@numAccountIdOpeningEquity) +' And numDomainId='+Convert(varchar(10),@numDomainId)          
--              
--          -- To update Opening Balance in General_Journal_Header and General_Journal_Details Table          
--      Update General_Journal_Header Set numAmount=0 Where numChartAcntId=@numChartAcntId  And numDomainId=@numDomainId         
--      Select @numJournalId=numJournal_Id From General_Journal_Header Where numChartAcntId=@numChartAcntId  And numDomainId=@numDomainId         
--      Update General_Journal_Details Set numDebitAmt=0 Where numJournalId=@numJournalId  And numDomainId=@numDomainId     
--    End    
--                                             
--         If @numAcntType=816       
--        Begin       
--     set @strSQLEquityBal='update chart_of_accounts  set numOriginalOpeningBal=isnull(numOriginalOpeningBal,0) + ' + convert(varchar(30),@numOldOpeningBal) +', numopeningbal=isnull(numopeningbal,0) + ' + convert(varchar(30),@numOldOpeningBal) + ' where   
-- 
--           numAccountId='+convert(varchar(10),@numAccountIdOpeningEquity) +' And numDomainId='+Convert(varchar(10),@numDomainId)         
--      -- To update Opening Balance in General_Journal_Header and General_Journal_Details Table          
--      Update General_Journal_Header Set numAmount=0 Where numChartAcntId=@numChartAcntId  And numDomainId=@numDomainId         
--      Select @numJournalId=numJournal_Id From General_Journal_Header Where numChartAcntId=@numChartAcntId  And numDomainId=@numDomainId         
--      Update General_Journal_Details Set numCreditAmt=0 Where numJournalId=@numJournalId  And numDomainId=@numDomainId      
--    End    
--  End          
--  Else             
--   Begin                                     
--    Set @strSQl2='update chart_of_accounts  set numopeningbal=isnull(numopeningbal,0) - ' + convert(varchar(30),@numOldOpeningBal) + ' where  numAccountId in ('+convert(varchar(30),@numChartAcntId)+') And numDomainId='+Convert(varchar(10),@numDomainId)                                                      
--         If @numAcntType=813       
--      Begin       
--       set @strSQLEquityBal='update chart_of_accounts  set numOriginalOpeningBal=isnull(numOriginalOpeningBal,0) - ' + convert(varchar(30),@numOldOpeningBal) +', numopeningbal=isnull(numopeningbal,0) - ' + convert(varchar(30),@numOldOpeningBal) + ' where 
--  
--   
--       numAccountId='+convert(varchar(10),@numAccountIdOpeningEquity)  +' And numDomainId='+Convert(varchar(10),@numDomainId)                                           
--    
--        -- To update Opening Balance in General_Journal_Header and General_Journal_Details Table          
--        Update General_Journal_Header Set numAmount=0 Where numChartAcntId=@numChartAcntId  And numDomainId=@numDomainId         
--        Select @numJournalId=numJournal_Id From General_Journal_Header Where numChartAcntId=@numChartAcntId And numDomainId=@numDomainId        
--        Update General_Journal_Details Set numDebitAmt=0 Where numJournalId=@numJournalId  And numDomainId=@numDomainId     
--    End    
--   If @numAcntType=816      
--    Begin       
--     Set @strSQLEquityBal='update chart_of_accounts  set numOriginalOpeningBal=isnull(numOriginalOpeningBal,0) + ' + convert(varchar(30),@numOldOpeningBal) +', numopeningbal=isnull(numopeningbal,0) + ' + convert(varchar(30),@numOldOpeningBal) + ' where  
--  
--      numAccountId= '+convert(varchar(10),@numAccountIdOpeningEquity) +' And numDomainId='+Convert(varchar(10),@numDomainId)         
--     -- To update Opening Balance in General_Journal_Header and General_Journal_Details Table          
--     Update General_Journal_Header Set numAmount=0 Where numChartAcntId=@numChartAcntId  And numDomainId=@numDomainId         
--     Select @numJournalId=numJournal_Id From General_Journal_Header Where numChartAcntId=@numChartAcntId  And numDomainId=@numDomainId          
--     Update General_Journal_Details Set numCreditAmt=0 Where numJournalId=@numJournalId  And numDomainId=@numDomainId     
--    End    
--  End          
--  print(@strSQl2)          
--  Exec(@strSQl2)                                        
--  print(@strSQLEquityBal)          
--  Exec(@strSQLEquityBal)               
--  -- Delete From Chart_Of_Accounts Where numAccountId=@numChartAcntId And numDomainId=@numDomainId              
--               
 End
GO
