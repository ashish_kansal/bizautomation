/****** Object:  StoredProcedure [dbo].[USP_FollowUpHstr]    Script Date: 07/26/2008 16:15:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_followuphstr')
DROP PROCEDURE usp_followuphstr
GO
CREATE PROCEDURE [dbo].[USP_FollowUpHstr]  
@numDivisionID as numeric(9),
@numDomainID as numeric(9)=0  
as  
select numFollowUpStatusID,numFollowUpstatus,vcdata,numDivisionID,bintAddedDate from FollowUpHistory  
join listdetails on numlistitemid=numFollowUpstatus  
where numDivisionID=@numDivisionID  and FollowUpHistory.numDomainID=@numDomainID
GO
