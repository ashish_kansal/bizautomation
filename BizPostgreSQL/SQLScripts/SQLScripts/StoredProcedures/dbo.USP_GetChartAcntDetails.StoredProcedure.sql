/****** Object:  StoredProcedure [dbo].[USP_GetChartAcntDetails]    Script Date: 09/25/2009 15:47:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- Created by Siva        
-- [USP_GetChartAcntDetails] @numDomainId=72,@dtFromDate='2007-01-01 00:00:00:000',@dtToDate='2009-09-25 00:00:00:000'                            
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'usp_getchartacntdetails' ) 
    DROP PROCEDURE usp_getchartacntdetails
GO
CREATE PROCEDURE [dbo].[USP_GetChartAcntDetails]
    @numDomainId AS NUMERIC(9),
    @dtFromDate AS DATETIME,
    @dtToDate AS DATETIME,
    @ClientTimeZoneOffset INT, --Added by Chintan to enable calculation of date according to client machine
    @numAccountClass AS NUMERIC(9)=0
AS 
    BEGIN    
--        DECLARE @numFinYear INT ;
        DECLARE @dtFinYearFromJournal DATETIME ;
		DECLARE @dtFinYearFrom DATETIME ;

--        SET @numFinYear = ( SELECT  numFinYearId
--                            FROM    FINANCIALYEAR
--                            WHERE   dtPeriodFrom <= @dtFromDate
--                                    AND dtPeriodTo >= @dtFromDate
--                                    AND numDomainId = @numDomainId
--                          ) ;
SELECT @dtFinYearFromJournal = MIN(datEntry_Date) FROM view_journal WHERE numDomainID=@numDomainId AND (numAccountClass=@numAccountClass OR @numAccountClass=0)
SET @dtFinYearFrom = ( SELECT   dtPeriodFrom
                        FROM     FINANCIALYEAR
                        WHERE    dtPeriodFrom <= @dtFromDate
                                AND dtPeriodTo >= @dtFromDate
                                AND numDomainId = @numDomainId
                        ) ;

SELECT * INTO #view_journal FROM view_journal WHERE numDomainID=@numDomainId AND (numAccountClass=@numAccountClass OR @numAccountClass=0);

SET @dtToDate = DateAdd(minute, @ClientTimeZoneOffset, @dtToDate);
/*Timezone Logic.. Stored Date are in UTC/GMT  */
--SET @dtToDate = DATEADD(DAY, DATEDIFF(DAY, '19000101',  @dtToDate), '23:59:59');
--SELECT @dtFromDate,@ClientTimeZoneOffset,DATEADD(minute, @ClientTimeZoneOffset, @dtFromDate)
--        SET @dtFromDate = DATEADD(minute, @ClientTimeZoneOffset, @dtFromDate) ;

        /*Comment by chintan
        Reason: Standard US was facing balance not matching for undeposited func(check video for more info)
        We do not need timezone settings since all journal entry date does not store time. we only store date. 
        */
--        SET @dtToDate = DATEADD(minute, @ClientTimeZoneOffset, @dtToDate) ;


--PRINT @dtFromDate
--PRINT @dtToDate
--PRINT @numFinYear 
--PRINT @dtFinYearFrom 

--select * from view_journal where numDomainid=72

		
        CREATE TABLE #PLSummary
            (
              numAccountId NUMERIC(9),
              vcAccountName VARCHAR(250),
              numParntAcntTypeID NUMERIC(9),
              vcAccountDescription VARCHAR(250),
              vcAccountCode VARCHAR(50) COLLATE Database_Default,
              Opening DECIMAL(20,5),
              Debit DECIMAL(20,5),
              Credit DECIMAL(20,5)
              ,bitIsSubAccount BIT,numParentAccID NUMERIC(9),Balance DECIMAL(20,5) 
            ) ;

        INSERT  INTO #PLSummary
                SELECT  COA.numAccountId,
                        vcAccountName,
                        numParntAcntTypeID,
                        vcAccountDescription,
                        vcAccountCode,
                      /*  ISNULL(( SELECT SUM(ISNULL(monOpening, 0))
                                 FROM   CHARTACCOUNTOPENING CAO
                                 WHERE  numFinYearId = @numFinYear
                                        AND numDomainID = @numDomainId
                                        AND CAO.numAccountId IN (SELECT numAccountId FROM dbo.Chart_Of_Accounts WHERE numDomainId = @numDomainId AND vcAccountCode like COA.vcAccountCode + '%' )
                               ), 0)
                        + */ 
						CASE WHEN COA.[vcAccountCode] LIKE '0103%' OR 
							 	  COA.[vcAccountCode] LIKE '0104%' OR 
							 	  COA.[vcAccountCode] LIKE '0106%' 
							 THEN 0 
							 ELSE isnull(t1.OPENING,0) 
						END as OPENING,
                        ISNULL(t.DEBIT, 0) AS DEBIT,
                        ISNULL(t.CREDIT, 0) AS CREDIT
                               ,ISNULL(COA.bitIsSubAccount,0),ISNULL(COA.numParentAccID,0) AS numParentAccID,0
                FROM    Chart_of_Accounts COA
					OUTER APPLY (SELECT   SUM(Debit - Credit) AS OPENING
                                   FROM     #view_journal VJ
                                   WHERE    VJ.numDomainId = @numDomainId
                                            AND VJ.numAccountID = COA.numAccountID /*AND VJ.COAvcAccountCode like COA.vcAccountCode + '%'*/ /*VJ.numAccountId = COA.numAccountId*/
                                            AND datEntry_Date BETWEEN (CASE WHEN COA.[vcAccountCode] LIKE '0103%' OR COA.[vcAccountCode] LIKE '0104%' OR COA.[vcAccountCode] LIKE '0106%' THEN @dtFinYearFrom ELSE @dtFinYearFromJournal END )
                                                              AND  DATEADD(Minute,-1,@dtFromDate) AND (VJ.numAccountClass=@numAccountClass OR @numAccountClass=0)) AS t1
					OUTER APPLY(SELECT   SUM(Debit) as DEBIT,SUM(Credit) as CREDIT
                                   FROM   #view_journal VJ
                                 WHERE  VJ.numDomainId = @numDomainId
                                        AND VJ.numAccountID = COA.numAccountID /*AND VJ.COAvcAccountCode like COA.vcAccountCode + '%'*/ /*VJ.numAccountId = COA.numAccountId*/
                                        AND datEntry_Date BETWEEN @dtFromDate AND @dtToDate AND (VJ.numAccountClass=@numAccountClass OR @numAccountClass=0)
                                 ) as t
                    WHERE   COA.numDomainId = @numDomainId  ;
--SELECT @dtFinYearFrom,@dtFromDate - 1,* FROM #PLSummary
        CREATE TABLE #PLOutPut
            (
              numAccountId NUMERIC(9),
              vcAccountName VARCHAR(250),
              numParntAcntTypeID NUMERIC(9),
              vcAccountDescription VARCHAR(250),
              vcAccountCode VARCHAR(50) COLLATE Database_Default,
              Opening DECIMAL(20,5),
              Debit DECIMAL(20,5),
              Credit DECIMAL(20,5),numParentAccID NUMERIC(9),Balance DECIMAL(20,5) 
            ) ;

        INSERT  INTO #PLOutPut
                SELECT  ATD.numAccountTypeID,
                        ATD.vcAccountType,
                        ATD.numParentID,
                        '',
                        ATD.vcAccountCode,
                        ISNULL(SUM(Opening), 0) AS Opening,
                        ISNULL(SUM(Debit), 0) AS Debit,
                        ISNULL(SUM(Credit), 0) AS Credit,0,0
                FROM    /*Chart_Of_Accounts c JOIN [AccountTypeDetail] ATD ON ATD.[numAccountTypeID] = c.[numParntAcntTypeId]*/
						[AccountTypeDetail] ATD
                        RIGHT OUTER JOIN #PLSummary PL ON PL.vcAccountCode LIKE ATD.vcAccountCode + '%' AND LEN(PL.vcAccountCode)>LEN(ATD.vcAccountCode)
                        --(PL.vcAccountCode LIKE c.vcAccountCode OR (PL.vcAccountCode LIKE c.vcAccountCode + '%' AND PL.numParentAccID>0))
                                                          AND ATD.numDomainId = @numDomainId
				--WHERE  PL.bitIsSubAccount=0
                GROUP BY ATD.numAccountTypeID,
                        ATD.vcAccountCode,
                        ATD.vcAccountType,
                        ATD.numParentID ;

ALTER TABLE #PLSummary
DROP COLUMN bitIsSubAccount


-- GETTING P&L VALUE

DECLARE @CURRENTPL DECIMAL(20,5) ;
DECLARE @PLOPENING DECIMAL(20,5);
DECLARE @PLCHARTID NUMERIC(8)
DECLARE @TotalIncome DECIMAL(20,5);
DECLARE @TotalExpense DECIMAL(20,5);
DECLARE @TotalCOGS DECIMAL(20,5);
DECLARE  @CurrentPL_COA DECIMAL(20,5)
SET @CURRENTPL =0;	
SET @PLOPENING=0;


SELECT @PLCHARTID=COA.numAccountId FROM Chart_of_Accounts COA WHERE numDomainID=@numDomainId and
bitProfitLoss=1;

SELECT @CurrentPL_COA =  ISNULL(sum(Credit),0)-ISNULL(sum(Debit),0) FROM #view_journal VJ WHERE numDomainID=@numDomainID
AND numAccountId=@PLCHARTID AND datEntry_Date between  @dtFromDate AND @dtToDate;

SELECT  @TotalIncome= ISNULL(sum(Opening),0) + ISNULL(sum(Credit),0) -ISNULL(sum(Debit),0) FROM
#PLOutPut P WHERE 
vcAccountCode IN ('0103')

SELECT  @TotalExpense=ISNULL(sum(Opening),0)+ ISNULL(sum(Credit),0) -ISNULL(sum(Debit),0) FROM
#PLOutPut P WHERE 
vcAccountCode IN ('0104')

SELECT  @TotalCOGS= ISNULL(sum(Opening),0) + ISNULL(sum(Credit),0) -ISNULL(sum(Debit),0) FROM
#PLOutPut P WHERE 
vcAccountCode IN ('0106')

PRINT 'TotalIncome=									' + CAST(@TotalIncome AS VARCHAR(20))
PRINT 'TotalExpense=									'+ CAST(@TotalExpense AS VARCHAR(20))
PRINT 'TotalCOGS=										'+ CAST(@TotalCOGS AS VARCHAR(20))

SELECT @CURRENTPL = /*@CurrentPL_COA +*/ @TotalIncome + @TotalExpense + @TotalCOGS 
PRINT 'Current Profit/Loss = (Income - expense - cogs)= ' + CAST( @CURRENTPL AS VARCHAR(20))

set @CURRENTPL=@CURRENTPL * (-1)

SELECT @PLOPENING = ISNULL(sum(Credit),0)-ISNULL(sum(Debit),0) FROM #view_journal VJ WHERE numDomainID=@numDomainID 
AND ( vcAccountCode  LIKE '0103%' or  vcAccountCode  LIKE '0104%' or  vcAccountCode  LIKE '0106%')
AND datEntry_Date <=  DATEADD(Minute,-1,@dtFromDate);

SELECT @PLOPENING = ISNULL(@PLOPENING,0) + ISNULL(sum(Credit),0)-ISNULL(sum(Debit),0) FROM #view_journal VJ WHERE numDomainID=@numDomainID
AND numAccountId=@PLCHARTID AND datEntry_Date <=  DATEADD(Minute,-1,@dtFromDate);

PRINT '@PLOPENING='+ CAST(@PLOPENING AS VARCHAR(20))
PRINT '@CURRENTPL='+ CAST(@CURRENTPL AS VARCHAR(20))
PRINT '-(@PLOPENING + @CURRENTPL)='+ CAST((-(@PLOPENING + @CURRENTPL)) AS VARCHAR(20))


set @CURRENTPL=@CURRENTPL * (-1)

UPDATE #PLSummary SET Opening = CASE WHEN vcAccountCode = '0105010101' THEN -@PLOPENING ELSE Opening END,
Balance = CASE WHEN vcAccountCode = '0105010101' THEN -(@PLOPENING + @CURRENTPL) ELSE Opening + Debit - Credit END
--WHERE vcAccountCode = '0105010101'
                        

UPDATE PO SET Opening = T.Opening,
Debit = T.Debit,
Credit = T.Credit,
Balance = T.Balance
FROM #PLOutPut PO JOIN (SELECT PO.vcAccountCode,ISNULL(SUM(PL.Opening), 0) AS Opening,ISNULL(SUM(PL.Debit), 0) AS Debit,ISNULL(SUM(PL.Credit), 0) AS Credit,
/*ISNULL(SUM(PL.Opening), 0) + ISNULL(SUM(PL.Debit), 0) - ISNULL(SUM(PL.Credit), 0)*/ISNULL(SUM(PL.Balance), 0) AS Balance FROM #PLOutPut PO
RIGHT OUTER JOIN #PLSummary PL ON PL.vcAccountCode LIKE PO.vcAccountCode + '%' AND LEN(PL.vcAccountCode)>LEN(PO.vcAccountCode)
GROUP BY PO.vcAccountCode) T ON T.vcAccountCode = PO.vcAccountCode

--SELECT PO.vcAccountCode,ISNULL(SUM(PL.Opening), 0) AS Opening,ISNULL(SUM(PL.Debit), 0) AS Debit,ISNULL(SUM(PL.Credit), 0) AS Credit,
--ISNULL(SUM(PL.Balance), 0) AS Balance FROM #PLOutPut PO
--RIGHT OUTER JOIN #PLSummary PL ON PL.vcAccountCode LIKE PO.vcAccountCode + '%' AND LEN(PL.vcAccountCode)>LEN(PO.vcAccountCode)
--GROUP BY PO.vcAccountCoder

--WHERE vcAccountCode = '0105010101'
--------------------------------------------------------
		--UPDATE #PLSummary SET Opening = @PLOPENING WHERE #PLSummary.vcAccountCode = '0105010101'

		SELECT  P.numAccountId,
				P.vcAccountName,
				P.numParntAcntTypeID, 
				P.vcAccountDescription,
				P.vcAccountCode,
				/*(CASE WHEN P.[vcAccountCode] = '0105010101' THEN -@PLOPENING ELSE P.Opening END) AS*/ P.Opening,
				P.Debit ,
				P.Credit,
				P.numParentAccID,
                /*(CASE WHEN P.[vcAccountCode] = '0105010101' THEN -(@PLOPENING + @CURRENTPL) ELSE Opening + Debit - Credit END) AS*/ P.Balance,
                CASE WHEN LEN(P.[vcAccountCode])>4 THEN REPLICATE('&nbsp;', LEN(P.[vcAccountCode])-4 ) + P.[vcAccountCode] 
				ELSE P.[vcAccountCode]
                END AS AccountCode1,
                CASE WHEN LEN(P.[vcAccountCode]) > 4 THEN REPLICATE('&nbsp;', LEN(P.[vcAccountCode]) - 4 ) + P.[vcAccountName] 
                     ELSE P.[vcAccountName]
                END AS vcAccountName1,
                1 AS TYPE
        FROM    #PLSummary P
                LEFT JOIN dbo.Chart_Of_Accounts C ON C.numAccountId = P.numAccountId
        WHERE   LEN(ISNULL(P.[vcAccountCode], '')) > 2
        UNION
        SELECT  O.numAccountId,
				O.vcAccountName,
				O.numParntAcntTypeID, 
				O.vcAccountDescription,
				O.vcAccountCode,
				/*(CASE WHEN O.[vcAccountCode] = '0105010101' THEN -@PLOPENING ELSE O.Opening END) AS*/ Opening,
				O.Debit ,
				O.Credit,
				O.numParentAccID,
                /*(CASE WHEN O.[vcAccountCode] = '0105010101' THEN -(@PLOPENING + @CURRENTPL) ELSE Opening + Debit - Credit END) AS*/ Balance,
                CASE WHEN LEN(O.[vcAccountCode]) > 4
                     THEN REPLICATE('&nbsp;',LEN(O.[vcAccountCode]) - 4) + O.[vcAccountCode]
                     ELSE O.[vcAccountCode]
                END AS AccountCode1,
                CASE WHEN LEN(O.[vcAccountCode]) > 4
                      THEN REPLICATE('&nbsp;',LEN(O.[vcAccountCode]) - 4) + O.[vcAccountName]
                     ELSE O.[vcAccountName]
                END AS vcAccountName1,
                2 AS TYPE
                --CAST(O.[vcAccountCode] AS deci)  vcSortableAccountCode
        FROM    #PLOutPut O
--                LEFT JOIN dbo.Chart_Of_Accounts C ON C.numAccountId = O.numAccountId
        WHERE   LEN(ISNULL(O.[vcAccountCode], '')) > 2
        ORDER BY vcAccountCode,TYPE 
 
    --SELECT * FROM #PLOutPut ;
    --SELECT * FROM #PLSummary ;

        SELECT  (SUM(Opening) - @PLOPENING) AS OpeningTotal,
                SUM(Debit) AS DebitTotal,
                SUM(Credit) AS CreditTotal,
                (SUM(Opening) + SUM(Debit) - SUM(Credit) - @PLOPENING)AS BalanceTotal
        FROM    [#PLSummary]

        DROP TABLE #PLOutPut ;
        DROP TABLE #PLSummary ;


    END
