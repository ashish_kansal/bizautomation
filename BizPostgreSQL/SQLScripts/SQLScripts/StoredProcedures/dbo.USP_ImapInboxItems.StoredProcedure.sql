/****** Object:  StoredProcedure [dbo].[USP_ImapInboxItems]    Script Date: 07/26/2008 16:18:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--- Created By Anoop Jayaraj                                                      
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_imapinboxitems')
DROP PROCEDURE usp_imapinboxitems
GO
CREATE PROCEDURE [dbo].[USP_ImapInboxItems]                                                                                              
@PageSize [int] ,                            
@CurrentPage [int],                            
@srchSubjectBody as varchar(100) = '',                            
@TotRecs int output        ,                         
@columnName as varchar(50) ,        
@columnSortOrder as varchar(4) ,  
@numDomainId as numeric,  
@numUserCntId as numeric ,
@tinttype as tinyint ,
@ToEmail as varchar (150)  
as                                                      
                                                      
                  
                                            
 declare @firstRec as integer                                                      
 declare @lastRec as integer                                                      
 set @firstRec= (@CurrentPage-1) * @PageSize                                                      
 set @lastRec= (@CurrentPage*@PageSize+1)                                                      
                                                     
                     
declare @strSql as varchar(8000)                    
   if @columnName = 'FromName'               
 begin        
 set @columnName =  'dbo.GetEmaillName(emailHistory.numEmailHstrID,4)'        
 end        
 if @columnName = 'FromEmail'        
 begin        
 set @columnName = 'dbo.GetEmaillAdd(emailHistory.numEmailHstrID,4)'        
 end        
                  
set @strSql='With tblSubscriber AS (SELECT  distinct(emailHistory.numEmailHstrID) ,            
ROW_NUMBER() OVER (ORDER BY '+@columnName+' '+@columnSortOrder+') AS  RowNumber             
from emailHistory  '                              
 if @tinttype = 5 set @strSql=@strSql +                   
'where emailHistory.tinttype=5  and numDomainId='+convert(varchar(10),@numDomainId)+' and numUserCntId='+convert(varchar(10),@numUserCntId)                  

if @tinttype = 2 set @strSql=@strSql +   
'join EmailHStrToBCCAndCC on emailHistory.numEmailHstrID=EmailHStrToBCCAndCC.numEmailHstrID                   
join EmailMaster on        EmailMaster.numEmailId=EmailHStrToBCCAndCC.numEmailId   
where emailHistory.tinttype=2 and numDomainId='+convert(varchar(10),@numDomainId)+' and (EmailMaster.vcEmailId like '''+@ToEmail+''' and EmailHStrToBCCAndCC.tintType=4 ) '

if @tinttype = 3 set @strSql=@strSql +   
'join EmailHStrToBCCAndCC on emailHistory.numEmailHstrID=EmailHStrToBCCAndCC.numEmailHstrID                   
join EmailMaster on        EmailMaster.numEmailId=EmailHStrToBCCAndCC.numEmailId   
where emailHistory.tinttype=3 and numDomainId='+convert(varchar(10),@numDomainId)+' and (EmailMaster.vcEmailId like '''+@ToEmail+''' ) '
                 
 if @srchSubjectBody <> '' set @strSql=@strSql +' and (vcSubject  like ''%'+@srchSubjectBody+'%''  
  or vcBodyText like ''%'+@srchSubjectBody+'%''    
          
or emailHistory.numEmailHstrID in (select numEmailHstrId from EmailHStrToBCCAndCC where             
(numemailid in (select numEmailid from emailmaster where vcemailid like ''%'+@srchSubjectBody+'%'')) or  
   vcName like ''%'+@srchSubjectBody+'%'') )'                
           
            
            
                 
set @strSql=@strSql +')                     
 select RowNumber,            
dbo.GetEmaillAdd(emailHistory.numEmailHstrID,4) as FromEmail,                  
 dbo.GetEmaillName(emailHistory.numEmailHstrID,4) as FromName,   
dbo.GetEmaillAdd(emailHistory.numEmailHstrID,1) as ToEmail,                                 
dbo.GetEmaillAdd(emailHistory.numEmailHstrID,2) as CCEmail,  
emailHistory.numEmailHstrID,                            
isnull(vcSubject,'''') as vcSubject,   
dbo.FormatedDateFromDate(bintCreatedOn,numDomainId) as bintCreatedOn,  
isnull(bitIsRead,''False'') as IsRead,                  
isnull(vcSize,0) as vcSize,                              
isnull(bitHasAttachments,''False'') as HasAttachments,                             
isnull(EmailHstrAttchDtls.vcFileName,'''') as AttachmentPath,                                                       
dbo.FormatedDateFromDate(dtReceivedOn,numDomainId) as dtReceivedOn,                              
isnull(emailHistory.tintType,5) as type,                              
isnull(chrSource,''B'') as chrSource,                          
numUid  
  
 from tblSubscriber T                  
 join emailHistory                   
 on emailHistory.numEmailHstrID=T.numEmailHstrID             
left join EmailHstrAttchDtls on emailHistory.numEmailHstrID=EmailHstrAttchDtls.numEmailHstrID             
where emailHistory.tinttype='+convert(varchar(10),@tinttype)+'  and                
  RowNumber > '+convert(varchar(10),@firstRec)+' and RowNumber < '+convert(varchar(10),@lastRec)+'              
union                 
 select 0 as RowNumber,null,null,null,null,count(*),null,null,            
null,null,null,null,null,null,null,null from tblSubscriber  order by RowNumber'            
print    @strSql             
exec (@strSql)
GO
