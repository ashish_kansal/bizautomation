/****** Object:  StoredProcedure [dbo].[USP_UpdatePortalWorkFlow]    Script Date: 07/26/2008 16:21:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_updateportalworkflow')
DROP PROCEDURE usp_updateportalworkflow
GO
CREATE PROCEDURE [dbo].[USP_UpdatePortalWorkFlow]
@tintCreateOppStatus as tinyint,
@numStatus as numeric(9),
@numRelationShip as numeric(9),
@tintCreateBizDoc as tinyint,
@numBizDocName as numeric(9),
@tintAccessAllowed as tinyint,
@vcOrderSuccessfullPage as varchar(300),
@vcOrderUnsucessfullPage as varchar(300)
as

update PortalWorkflow set 
		tintCreateOppStatus=@tintCreateOppStatus,
  		numStatus=@numStatus,
		numRelationShip=@numRelationShip,
		tintCreateBizDoc=@tintCreateBizDoc,
		numBizDocName=@numBizDocName,
		tintAccessAllowed=@tintAccessAllowed,
		vcOrderSuccessfullPage=@vcOrderSuccessfullPage,
		vcOrderUnsucessfullPage=@vcOrderUnsucessfullPage
GO
