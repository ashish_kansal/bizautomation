/****** Object:  StoredProcedure [dbo].[usp_GetCustomerAddress]    Script Date: 07/26/2008 16:17:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getcustomeraddress')
DROP PROCEDURE usp_getcustomeraddress
GO
CREATE PROCEDURE [dbo].[usp_GetCustomerAddress]

	@numDivID numeric
--
AS
BEGIN
	SELECT 
--	vcBillStreet, vcBillCity, vcBilState, vcBillPostCode, vcBillCountry, 
--		vcShipStreet, vcShipCity, vcShipState, vcShipPostCode, vcShipCountry,
  isnull(AD1.vcStreet,'') as vcBillStreet, 
  isnull(AD1.vcCity,'') as vcBillCity, 
  isnull(AD1.numState,0) as vcBilState,
  isnull(AD1.vcPostalCode,'') as vcBillPostCode,
  isnull(AD1.numCountry,0)  as vcBillCountry,
  isnull(AD2.vcStreet,'') as vcShipStreet,
  isnull(AD2.vcCity,'') as vcShipCity,
  isnull(AD2.numState,0)  as vcShipState,
  isnull(AD2.vcPostalCode,'') as vcShipPostCode, 
  isnull(AD2.numCountry,0) vcShipCountry
		
	FROM DivisionMaster DM
	 LEFT JOIN dbo.AddressDetails AD1 ON AD1.numDomainID=DM.numDomainID 
   AND AD1.numRecordID=DM.numDivisionID AND AD1.tintAddressOf=2 AND AD1.tintAddressType=1 AND AD1.bitIsPrimary=1
   LEFT JOIN dbo.AddressDetails AD2 ON AD2.numDomainID=DM.numDomainID 
   AND AD2.numRecordID= DM.numDivisionID AND AD2.tintAddressOf=2 AND AD2.tintAddressType=2 AND AD2.bitIsPrimary=1
	WHERE numDivisionID=@numDivID
	
	

END
GO
