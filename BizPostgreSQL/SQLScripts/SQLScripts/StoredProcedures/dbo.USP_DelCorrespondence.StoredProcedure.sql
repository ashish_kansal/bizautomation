/****** Object:  StoredProcedure [dbo].[USP_DelCorrespondence]    Script Date: 07/26/2008 16:15:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_delcorrespondence')
DROP PROCEDURE usp_delcorrespondence
GO
CREATE PROCEDURE [dbo].[USP_DelCorrespondence]
@numID as numeric(9)=0,
@tintType as tinyint
as

if @tintType=1
BEGIN
	DELETE FROM [Correspondence] WHERE [numEmailHistoryID]=@numID
	delete from EmailHStrToBCCAndCC where numEmailHstrID=@numID
	delete from EmailHistory where numEmailHstrID=@numID
end
else if @tintType=2
BEGIN
	DELETE FROM [Correspondence] WHERE [numCommID]=@numID
	DECLARE @numDomainID AS NUMERIC(18,0)
	DECLARE @numDivisionId AS NUMERIC(18,0)
	DECLARE @numSecounds AS NUMERIC(18,0)
	SELECT @numDomainID=numDomainID,@numDivisionId=numDivisionId FROM Communication
	SELECT @numSecounds =numUsedTime FROM ContractsLog WHERE numDivisionID=@numDivisionId  AND numReferenceId=@numID
	AND numContractId=(SELECT TOP 1 numContractId FRoM Contracts WHERE numDivisonId=@numDivisionId AND numDomainId=@numDomainID) 

	UPDATE Contracts SET timeUsed=timeUsed-@numSecounds,dtmModifiedOn=GETUTCDATE() WHERE numDivisonId=@numDivisionId AND numDomainId=@numDomainID

	DELETE FROM ContractsLog WHERE numDivisionID=@numDivisionId  AND numReferenceId=@numID
	AND numContractId=(SELECT TOP 1 numContractId FRoM Contracts WHERE numDivisonId=@numDivisionId AND numDomainId=@numDomainID)
	delete from Communication where numCommId=@numID

end
GO
