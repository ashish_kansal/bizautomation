/****** Object:  StoredProcedure [dbo].[USP_tmpCalandarSave]    Script Date: 07/26/2008 16:21:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_tmpcalandarsave')
DROP PROCEDURE usp_tmpcalandarsave
GO
CREATE PROCEDURE [dbo].[USP_tmpCalandarSave]                                    
                          
@strXml as text='' ,                       
@ItemId  as varchar(250)                          
AS                              
                          
if (select count(*) from tmpCalandarTbl where ItemId=@ItemId)= 0
    
begin                         
if convert(varchar(10),@strXml) <> ''                              
begin                                                          
   DECLARE @hDoc4 int                                                                  
   EXEC sp_xml_preparedocument @hDoc4 OUTPUT, @strXml                                                                  
                                                                    
    insert into  tmpCalandarTbl(ItemId,ItemIdOccur,StartDateTimeUtc,OccurIndex)  
 select * from (                             
SELECT * FROM OPENXML (@hDoc4,'/NewDataSet/Table',2)                                                                  
 WITH  (                                                              
                          
  ItemId nvarchar(300),                          
  ItemIdOccur nvarchar(300),  
  StartDateTimeUtc varchar(19) ,
  OccurIndex numeric(9)  
))X                           
                  
EXEC sp_xml_removedocument @hDoc4 
end                            
       
end
GO
