/****** Object:  StoredProcedure [dbo].[USP_GetImagePath]    Script Date: 07/26/2008 16:17:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Create By Siva  
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getimagepath')
DROP PROCEDURE usp_getimagepath
GO
CREATE PROCEDURE [dbo].[USP_GetImagePath]  
(@numDomainId as numeric(9)=0,  
 @vcBizDocImagePath as varchar(100)='' output,
 @vcCompanyImagePath as varchar(100)='' output,
 @vcBankImagePath as varchar(100)='' output
)  
As  
Begin  
   Select @vcBizDocImagePath=isnull(vcBizDocImagePath,''),@vcCompanyImagePath=isnull(vcCompanyImagePath,''),@vcBankImagePath=isnull(vcBankImagePath,'')  From Domain Where numDomainId=@numDomainId  

End
GO
