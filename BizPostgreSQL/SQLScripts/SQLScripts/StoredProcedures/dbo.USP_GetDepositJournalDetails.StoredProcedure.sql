/****** Object:  StoredProcedure [dbo].[USP_GetDepositJournalDetails]    Script Date: 07/26/2008 16:17:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created by Siva           
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getdepositjournaldetails')
DROP PROCEDURE usp_getdepositjournaldetails
GO
CREATE PROCEDURE [dbo].[USP_GetDepositJournalDetails]            
@numDomainId as numeric(9)=0,                
@numJournalId as numeric(9)=0                 
As                      
Begin                      
Select GJD.numTransactionId as TransactionId , Convert(varchar(10),GJD.numChartAcntId)+'~'+Convert(varchar(10),CA.[numParntAcntTypeId]) as numChartAcntId,  
 CA.[numParntAcntTypeId] as numAcntType,GJD.numDebitAmt as numDebitAmt,                    
GJD.numCreditAmt as numCreditAmt,GJD.numCustomerId as numCustomerId,GJD.numJournalId,              
 GJD.varDescription,GJD.vcReference as vcReference,GJD.numPaymentMethod as numPaymentMethod      ,GJD.numClassID,GJD.numProjectID
From General_Journal_Header GJH                      
Inner join General_Journal_Details GJD  on GJH.numJournal_Id=GJD.numJournalId     
inner join Chart_of_Accounts CA on GJD.numChartAcntId=CA.numAccountId             
Left outer join Companyinfo CI on GJD.numCustomerId=CI.numCompanyId              
Where ISNULL(GJD.bitMainDeposit,0)=0 And GJH.numDomainId=@numDomainId And GJH.numJournal_Id=@numJournalId              
End
GO
