/****** Object:  StoredProcedure [dbo].[USP_UpdateLeaveDetails]    Script Date: 07/26/2008 16:21:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_updateleavedetails')
DROP PROCEDURE usp_updateleavedetails
GO
CREATE PROCEDURE [dbo].[USP_UpdateLeaveDetails]
@numCategoyHDRID as numeric(9)=0,
@numType as numeric(9)=0,
@bitApproved as bit
as

update TimeAndExpense set bitApproved=@bitApproved,
			numType=@numType
 
where numCategoryHDRID=@numCategoyHDRID
GO
