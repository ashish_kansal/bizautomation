/*
declare @p5 bigint
set @p5=5266
exec USP_UpdateDealStatus @numOppID=5885,@byteMode=1,@Amount=$100.0000,@ShippingCost=$0.0000,@numOppBizDocID=@p5 output,@numUserContactID=82933,@numDomainID=72
select @p5
*/
--created by anoop jayaraj                    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_updatedealstatus')
DROP PROCEDURE usp_updatedealstatus
GO
CREATE PROCEDURE [dbo].[USP_UpdateDealStatus]
@numOppID numeric(9)=0,                    
@byteMode tinyint=0,                    
@Amount as DECIMAL(20,5)=0,  
@ShippingCost as DECIMAL(20,5),
@numOppBizDocID as numeric(9) = 0 output,
@numUserContactID as numeric(9),
@numDomainID as numeric(9),
@BizDocID AS NUMERIC(9)=0,
@numSiteID AS NUMERIC(9),
@numPaymentMethodId AS NUMERIC(9)

as              
declare @numDivid numeric(9)
DECLARE @tintOppType TINYINT
select   @numDivid=numdivisionid,@tintOppType=tintOppType from   OpportunityMaster where numOppID=@numOppID             
update divisionmaster set tintCRMType=2   where numdivisionid=@numDivid               
update OpportunityMaster set  tintOppStatus=1,bintAccountClosingDate=getutcdate() where numOppID=@numOppID
declare @status as tinyint               
           
-------Updating Item Details    
                
  --update OpportunityMaster set tintOppStatus=1,bintAccountClosingDate=getutcdate() where numOppid=@numOppID                   
                    
if  @byteMode=1                     
begin                    
declare @numDivisionID as numeric           
Declare @numAuthoritativeeCommerce as numeric(9)                                
	Set @numAuthoritativeeCommerce=0;
	
DECLARE @fltDiscount AS FLOAT,@bitDiscountType AS bit	
select @numDivisionID=numDivisionID,@fltDiscount=ISNULL(fltDiscountTotal,0),@bitDiscountType=ISNULL(bitDiscountTypeTotal,0) from OpportunityMaster where numOppId=@numOppId                            
/*When first order is placed any lead or prospect should be converted to Account automatically-By chintan*/
update divisionMaster set tintCRMtype=2 where numDivisionID=@numDivisionID                
END
 
/*From e-commerce setting one can customize type of bizdoc to create based on payment mechanism used--By chintan 13/12/2011*/
DECLARE @numOrderStatus NUMERIC
DECLARE @numFailedOrderStatus NUMERIC
DECLARE @numRecOwner NUMERIC
DECLARE @numAssignTo NUMERIC

SELECT @numOrderStatus = ISNULL(numOrderStatus,0),@numFailedOrderStatus = ISNULL(numFailedOrderStatus,0),@numRecOwner = ISNULL(numRecordOwner,0),@numAssignTo = ISNULL(numAssignTo,0) FROM dbo.eCommercePaymentConfig 
WHERE numDomainID=@numDomainID AND bitEnable=1 AND numSiteId= @numSiteID AND numPaymentMethodId=@numPaymentMethodId         	
                
IF @numPaymentMethodId = 1 --Credit Card 
	BEGIN
	      IF EXISTS(SELECT * FROM dbo.eCommerceDTL WHERE numDomainId = @numDomainId  AND bitAuthOnlyCreditCard = 1)
          BEGIN
                 SELECT @numOrderStatus = numAuthrizedOrderStatus  FROM dbo.eCommerceDTL WHERE numDomainId = @numDomainId  AND bitAuthOnlyCreditCard = 1
		  END
    
          UPDATE dbo.OpportunityMaster SET numStatus = ISNULL(@numOrderStatus,0) ,numRecOwner = ISNULL(@numRecOwner,0),numAssignedTo = ISNULL(@numAssignTo,0)
          WHERE numOppId =@numOppID AND numDomainId=@numDomainID  
			   
--		  IF @numOppBizDocID > 0 --it will update monAmount in the case of Google checkout or Credit Card
--		  BEGIN
--				Update OpportunityBizDocs set monAmountPaid =@Amount ,numBizDocStatus = @numBizdocStatus WHERE  numOppBizDocsId = @numOppBizDocID and bitAuthoritativeBizDocs =1		
--		  END 
    
--        ELSE
--              BEGIN
--                 UPDATE dbo.OpportunityMaster SET numStatus = ISNULL(@numFailedOrderStatus,0) 
--                 WHERE numOppId =@numOppID AND numDomainId=@numDomainID 
--                  
--                 IF @numOppBizDocID > 0 --it will update monAmount in the case of Google checkout or Credit Card
--					BEGIN
--						Update OpportunityBizDocs set monAmountPaid = 0 WHERE  numOppBizDocsId = @numOppBizDocID		
--					END           
--           END
    END
ELSE IF @numPaymentMethodId = 27261 --Bill Me
	BEGIN
				 --PRINT(@numOppBizDocID)
                 UPDATE dbo.OpportunityMaster SET numStatus = ISNULL(@numOrderStatus,0) ,numRecOwner = ISNULL(@numRecOwner,0),numAssignedTo = ISNULL(@numAssignTo,0)
                 WHERE numOppId =@numOppID AND numDomainId=@numDomainID 
                 
--                 UPDATE dbo.OpportunityBizDocs SET numBizdocStatus = @numBizdocStatus 
--              WHERE numOppBizDocsId = @numOppBizDocID and bitAuthoritativeBizDocs =1		
  	END    
ELSE IF @numPaymentMethodId = 31488 --Google Checkout By Default numStatus pass 0 .When actual order placed from Google checkout its status updated.
    BEGIN
       
       UPDATE dbo.OpportunityMaster SET numStatus = @numFailedOrderStatus ,numRecOwner = ISNULL(@numRecOwner,0),numAssignedTo = ISNULL(@numAssignTo,0)
       WHERE numOppId =@numOppID AND numDomainId=@numDomainID 
              
--              UPDATE dbo.OpportunityBizDocs SET numBizdocStatus = @numBizdocStatus 
--              WHERE numOppBizDocsId = @numOppBizDocID 
       
    END     
    

DECLARE @numStatus AS NUMERIC(9);SET @numStatus=0
DECLARE @numUserCntID AS NUMERIC(9);SET @numUserCntID=0
SELECT @numStatus=ISNULL(numStatus,0),@numUserCntID=ISNULL(numCreatedBy,0) FROM OpportunityMaster WHERE numOppId =@numOppID AND numDomainId=@numDomainID 

IF ISNULL(@numStatus,0) > 0
	   BEGIN
	   		EXEC USP_ManageOpportunityAutomationQueue
					@numOppQueueID = 0, -- numeric(18, 0)
					@numDomainID = @numDomainID, -- numeric(18, 0)
					@numOppId = @numOppID, -- numeric(18, 0)
					@numOppBizDocsId = 0, -- numeric(18, 0)
					@numOrderStatus = @numStatus, -- numeric(18, 0)
					@numUserCntID = @numUserCntID, -- numeric(18, 0)
					@tintProcessStatus = 1, -- tinyint
					@tintMode = 1 -- TINYINT
	   END
	   
/*below used to insert journal entries . which is now done from frontend.- chintan */
 --exec USP_UpdateAuthorizativeBizDocsForEcommerce  @numOppID,'',@numDomainId,@numOppBizDocID,@numUserContactID


/*below code is obsolete as its being replaced by updateDealamount function from front endt, in checkout.ascx.cs- chintan*/
--if @Amount>0
--begin 
-- -- To Insert into OpportunityBizDocsDetails table 
-- declare @numCurrencyID as numeric(9)
-- declare @fltExchangeRate as FLOAT
-- set @numCurrencyID=0
-- select @numCurrencyID=isnull(numCurrencyID,0) from Domain where numDomainID=@numDomainId
-- if @numCurrencyID=0 set   @fltExchangeRate=1
-- else set @fltExchangeRate=dbo.GetExchangeRate(@numDomainID,@numCurrencyID)
--
--
-- Declare @numBizDocsPaymentDetId as numeric(9)  
-- Insert into OpportunityBizDocsDetails(numBizDocsId,numPaymentMethod,monAmount,numDomainId,bitAuthoritativeBizDocs,bitIntegratedToAcnt,numCreatedBy,dtCreationDate,[numCurrencyID],[fltExchangeRate])  
-- Values(@numOppBizDocID,1,@Amount,@numDomainId,@bitAuthoritativeBizDocs,0,@numUserContactID,getutcdate(),@numCurrencyID,@fltExchangeRate)  
--    Set @numBizDocsPaymentDetId=@@Identity  
--   --To insert into OpportunityBizDocsPaymentDetails table  
--    Insert into OpportunityBizDocsPaymentDetails(numBizDocsPaymentDetId,monAmount,dtDueDate,bitIntegrated)   
-- Values(@numBizDocsPaymentDetId,@Amount,getutcdate(),0)  
--
-- end                
--end
GO
