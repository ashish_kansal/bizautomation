/****** Object:  StoredProcedure [dbo].[USP_GetParentCategoryForSelType]    Script Date: 07/26/2008 16:18:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created by Siva  
--Modified by anoop to include HeadeQuarters also    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getparentcategoryforseltype')
DROP PROCEDURE usp_getparentcategoryforseltype
GO
CREATE PROCEDURE [dbo].[USP_GetParentCategoryForSelType]            
@numAcntType as numeric(9)=0,      
@numAccountId as numeric(9)=0,  
@numDomainId as numeric(9)=0          
          
AS          
Begin 
         
  Select * From Chart_Of_Accounts Where   ((numAccountId <> @numAccountId And numAcntTypeID=@numAcntType) or numParntAcntTypeID is null) and  numDomainId=@numDomainId    
        
End
GO
