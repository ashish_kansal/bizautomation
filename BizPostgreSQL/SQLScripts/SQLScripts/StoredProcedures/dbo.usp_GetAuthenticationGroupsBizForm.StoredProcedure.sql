GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getauthenticationgroupsbizform')
DROP PROCEDURE usp_getauthenticationgroupsbizform
GO
CREATE PROCEDURE [dbo].[usp_GetAuthenticationGroupsBizForm]      
	@numDomainID NUMERIC(18,0)      
AS
BEGIN       
	SELECT 
		numGroupID
		,vcGroupName
	FROM 
		AuthenticationGroupMaster          
	WHERE
		numDomainID=@numDomainID 
END
GO
