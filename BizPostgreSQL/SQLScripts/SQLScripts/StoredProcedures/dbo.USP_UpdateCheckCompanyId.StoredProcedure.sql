/****** Object:  StoredProcedure [dbo].[USP_UpdateCheckCompanyId]    Script Date: 07/26/2008 16:21:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created By Siva
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_updatecheckcompanyid')
DROP PROCEDURE usp_updatecheckcompanyid
GO
CREATE PROCEDURE [dbo].[USP_UpdateCheckCompanyId]
(@numCheckId as numeric(9)=0,
@numCheckCompanyId as numeric(9)=0,
@numCompanyId as numeric(9)=0)
As
Begin
	Update CheckDetails set numCheckCompanyId=@numCheckCompanyId,numCustomerId=@numCompanyId Where numCheckId=@numCheckId
End
GO
