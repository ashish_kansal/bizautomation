/****** Object:  StoredProcedure [dbo].[usp_BusinessProcessList]    Script Date: 07/26/2008 16:14:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_businessprocesslist')
DROP PROCEDURE usp_businessprocesslist
GO
CREATE PROCEDURE [dbo].[usp_BusinessProcessList] 
@numDomainId as numeric(9),
@Mode as int
as 
IF(@Mode=-1)
BEGIN
select Slp_Id,Slp_Name from Sales_process_List_Master where numDomainID= @numDomainId AND ISNULL(numOppId,0)=0 AND ISNULL(numProjectId,0)=0 AND ISNULL(numWorkOrderId,0)=0 order by slp_name
END
ELSE
BEGIN
select Slp_Id,Slp_Name from Sales_process_List_Master where Pro_Type=@Mode and numDomainID= @numDomainId AND ISNULL(numOppId,0)=0 AND ISNULL(numProjectId,0)=0 AND ISNULL(numWorkOrderId,0)=0 order by slp_name
END
GO
