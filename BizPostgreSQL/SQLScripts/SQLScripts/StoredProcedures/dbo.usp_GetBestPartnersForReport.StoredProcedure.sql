/****** Object:  StoredProcedure [dbo].[usp_GetBestPartnersForReport]    Script Date: 07/26/2008 16:16:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj        
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getbestpartnersforreport')
DROP PROCEDURE usp_getbestpartnersforreport
GO
CREATE PROCEDURE [dbo].[usp_GetBestPartnersForReport]          
 @numDomainID numeric,            
 @dtDateFrom datetime,            
 @dtDateTo datetime,            
 @numUserCntID numeric=0,                    
 @intType numeric=0,          
 @tintRights TINYINT=1          
  --          
AS            
BEGIN            
 If @tintRights=1          
 --All Records Rights            
 BEGIN            
          
 SELECT CI.numCompanyID,DM.numDivisionID,DM.tintCRMType,dbo.fn_GetPrimaryContact(DM.numDivisionID) as ContactID,CI.vcCompanyName + ','+ DM.vcDivisionName as Partner,          
 COUNT(CA.numCompanyID) as Referrals,          
 dbo.GetTotalDealsWonForBestPartners(CI.numCompanyID) AS DealsWon          
 FROM CompanyInfo CI           
 JOIN DivisionMaster DM          
 ON DM.numCompanyID = CI.numCompanyID          
 JOIN CompanyAssociations CA          
 ON CA.numDivisionID = DM.numDivisionID          
 WHERE CI.numDomainID = @numDomainID          
 AND DM.bintCreatedDate BETWEEN @dtDateFrom AND @dtDateTo          
 GROUP BY CI.vcCompanyName,DM.vcDivisionName,CI.numCompanyID,DM.numDivisionID,DM.tintCRMType          
          
 END            
            
 If @tintRights=2          
 BEGIN            
          
 SELECT CI.numCompanyID,DM.numDivisionID,DM.tintCRMType,dbo.fn_GetPrimaryContact(DM.numDivisionID) as ContactID,CI.vcCompanyName + ','+ DM.vcDivisionName as Partner,          
 COUNT(CA.numCompanyID) as Referrals,          
 dbo.GetTotalDealsWonForBestPartners(CI.numCompanyID) AS DealsWon          
 FROM CompanyInfo CI           
 JOIN DivisionMaster DM          
 ON DM.numCompanyID = CI.numCompanyID          
 JOIN CompanyAssociations CA          
 ON CA.numDivisionID = DM.numDivisionID           
 JOIN AdditionalContactsInformation AI          
 ON AI.numDivisionID = DM.numDivisionID          
 WHERE CI.numDomainID = @numDomainID          
 AND CI.bintCreatedDate BETWEEN @dtDateFrom AND @dtDateTo          
 AND AI.numTeam is not null             
 AND AI.numTeam in(select F.numTeam from ForReportsByTeam F             
 where F.numUserCntID=@numUserCntID and F.numDomainid=@numDomainId and F.tintType=@intType)      
  AND DM.bintCreatedDate BETWEEN @dtDateFrom AND @dtDateTo           
 GROUP BY CI.vcCompanyName,DM.vcDivisionName,CI.numCompanyID  ,DM.numDivisionID,DM.tintCRMType          
              
          
 END            
            
 If @tintRights=3          
 BEGIN            
          
 SELECT CI.numCompanyID,DM.numDivisionID,DM.tintCRMType,dbo.fn_GetPrimaryContact(DM.numDivisionID) as ContactID,CI.vcCompanyName + ','+ DM.vcDivisionName as Partner,          
 COUNT(CA.numCompanyID) as Referrals,          
 dbo.GetTotalDealsWonForBestPartners(CI.numCompanyID) AS DealsWon          
 FROM CompanyInfo CI           
 JOIN DivisionMaster DM          
 ON DM.numCompanyID = CI.numCompanyID          
 JOIN CompanyAssociations CA          
 ON CA.numDivisionID = DM.numDivisionID           
 WHERE CI.numDomainID = @numDomainID          
 AND DM.bintCreatedDate BETWEEN @dtDateFrom AND @dtDateTo
 AND DM.numTerID in(SELECT F.numTerritory FROM ForReportsByTerritory F                   
 WHERE F.numUserCntID=@numUserCntID and F.numDomainid=@numDomainID and F.tintType=@intType)           
 GROUP BY CI.vcCompanyName,DM.vcDivisionName,CI.numCompanyID,DM.numDivisionID,DM.tintCRMType              
          
 END            
 END
GO
