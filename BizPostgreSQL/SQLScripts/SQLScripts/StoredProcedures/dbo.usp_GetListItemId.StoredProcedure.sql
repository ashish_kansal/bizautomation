/****** Object:  StoredProcedure [dbo].[usp_GetListItemId]    Script Date: 07/26/2008 16:17:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
--This is for getting the corresponding list item id for the stage itemid
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getlistitemid')
DROP PROCEDURE usp_getlistitemid
GO
CREATE PROCEDURE [dbo].[usp_GetListItemId]
	@numStageItemId numeric   
--
AS
	SELECT numlistitemid FROM stagelistitemdetails WHERE numstagelistitemid=@numStageItemId
GO
