/****** Object:  StoredProcedure [dbo].[USP_GetRelFollw]    Script Date: 07/26/2008 16:18:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj        
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getrelfollw')
DROP PROCEDURE usp_getrelfollw
GO
CREATE PROCEDURE [dbo].[USP_GetRelFollw]        
@numRelationship as numeric(9)=0,
@numDomainID as numeric(9)=0       
as        
         
select numFollowID ,L2.vcData as Follow,numRelFolID,numRelationshipID,L1.vcData as RelName  from RelFollowupStatus    
join ListDetails L1    
on numRelationshipID=L1.numListItemID    
join ListDetails L2       
on numFollowID=L2.numListItemID    
where numRelationshipID=@numRelationship and L2.numDomainID=@numDomainID
GO
