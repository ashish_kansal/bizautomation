/****** Object:  StoredProcedure [dbo].[usp_cfwListFieldsAdded]    Script Date: 07/26/2008 16:15:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_cfwlistfieldsadded')
DROP PROCEDURE usp_cfwlistfieldsadded
GO
CREATE PROCEDURE [dbo].[usp_cfwListFieldsAdded]  
@TabId as integer=null,  
@loc_id as tinyint=null,  
@relId as numeric(9)=null,
@numDomainID as numeric(9)=0  
as  
  
if @TabId = -1   
begin  
Select fld_id,fld_label from CFW_Fld_Master join CFW_Fld_Dtl  
on Fld_id=numFieldId   
where Grp_id=@loc_id and numrelation =@relId and numDomainID=@numDomainID  
order by numOrder  
end  
  
if @TabId !=-1  
begin  
Select fld_id,fld_label from CFW_Fld_Master join CFW_Fld_Dtl  
on Fld_id=numFieldId  
where Grp_id=@loc_id  and subgrp=@TabId and numrelation =@relId  and numDomainID=@numDomainID
order by numOrder  
end
GO
