-- exec USP_GetPageNavigationRel @numModuleID=2,@numDomainID=72
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getpagenavigationrel')
DROP PROCEDURE usp_getpagenavigationrel
GO
CREATE PROCEDURE [dbo].[USP_GetPageNavigationRel]
@numModuleID as numeric(9)      ,    
@numDomainID as numeric(9),
@numGroupID NUMERIC(18, 0)        
as          

        
SELECT  PND.numPageNavID,
        numModuleID,
        numParentID,
        vcPageNavName,
        ISNULL(vcNavURL, '') AS vcNavURL,
        vcImageURL,
        0 AS numorder
FROM    PageNavigationDTL PND
        JOIN dbo.TreeNavigationAuthorization TNA ON PND.numPageNavID = TNA.numPageNavID
WHERE   1 = 1
        AND ( ISNULL(TNA.bitVisible, 0) = 1
              --OR ISNULL(numParentID, 0) = 0
            )
        AND ISNULL(PND.bitVisible, 0) = 1
        AND numModuleID = @numModuleID
        AND numGroupID = @numGroupID
UNION
SELECT  0,
        2 AS numModuleID,
        11 AS numParentID,
        L2.vcData AS vcPageNavName,
        '../prospects/frmProspectList.aspx?numProfile='
        + CONVERT(VARCHAR(10), L2.numListItemID) AS vcNavURL,
        NULL AS vcImageURL,
        0
FROM    FieldRelationship AS FR
        INNER JOIN FieldRelationshipDTL AS FRD ON FRD.numFieldRelID = FR.numFieldRelID
        INNER JOIN ListDetails AS L1 ON L1.numListItemID = FRD.numPrimaryListItemID
        INNER JOIN ListDetails AS L2 ON L2.numListItemID = FRD.numSecondaryListItemID
WHERE   ( FRD.numPrimaryListItemID = 46 )
        AND ( FR.numDomainID = @numDomainID )
UNION
SELECT  0,
        2 AS numModuleID,
        12 AS numParentID,
        L2.vcData AS vcPageNavName,
        '../account/frmAccountList.aspx?numProfile='
        + CONVERT(VARCHAR(10), L2.numListItemID) AS vcNavURL,
        NULL AS vcImageURL,
        0
FROM    FieldRelationship AS FR
        INNER JOIN FieldRelationshipDTL AS FRD ON FRD.numFieldRelID = FR.numFieldRelID
        INNER JOIN ListDetails AS L1 ON L1.numListItemID = FRD.numPrimaryListItemID
        INNER JOIN ListDetails AS L2 ON L2.numListItemID = FRD.numSecondaryListItemID
WHERE   ( FRD.numPrimaryListItemID = 46 )
        AND ( FR.numDomainID = @numDomainID )
UNION    
--Select Contact
SELECT  0,
        2 AS numModuleID,
        13 AS numParentID,
        vcData AS vcPageNavName,
        '../contact/frmContactList.aspx?ContactType='
        + CONVERT(VARCHAR(10), numListItemID) AS vcNavURL,
        NULL,
        sintOrder
FROM    listdetails
WHERE   numListID = 8
        AND ( constFlag = 1
              OR numDomainID = @numDomainID
            )
UNION
SELECT  0,
        2 AS numModuleId,
        13 AS numParentId,
        'Primary Contact' AS vcPageNavName,
        '../Contact/frmcontactList.aspx?ContactType=101',
        NULL,
        0
ORDER BY numParentID