/****** Object:  StoredProcedure [dbo].[usp_CaseDealSupportKey]    Script Date: 07/26/2008 16:15:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_casedealsupportkey')
DROP PROCEDURE usp_casedealsupportkey
GO
CREATE PROCEDURE [dbo].[usp_CaseDealSupportKey]
@numDivID numeric = 0  

as
select numOppID from OpportunityMaster  where numDivisionID=@numDivID
GO
