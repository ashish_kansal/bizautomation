/****** Object:  StoredProcedure [dbo].[USP_ManageTabsInCuSFields]    Script Date: 07/26/2008 16:20:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj      
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_managetabsincusfields')
DROP PROCEDURE usp_managetabsincusfields
GO
CREATE PROCEDURE [dbo].[USP_ManageTabsInCuSFields]      
@byteMode as tinyint=0,      
@LocID as numeric(9)=0,      
@TabName as varchar(50)='',      
@TabID as numeric(9)=0  ,    
--@vcURL as varchar(1000)='',  
@numDomainID as numeric(9),
@vcURL as varchar(100)=''    
as      
      
      
if @byteMode=1      
begin      
insert into  CFw_Grp_Master(Grp_Name,Loc_Id,numDomainID,tintType)       
values(@TabName,@LocID,@numDomainID,0)      
      
end      
      
if @byteMode=2      
begin      
update  CFw_Grp_Master set Grp_Name=@TabName       
where Grp_id=@TabID      
      
end      
      
if @byteMode=3      
begin      
--Check if any custom fields associated with subtab 
IF (SELECT COUNT(*) FROM cfw_fld_master WHERE [subgrp]= @TabID)>0
BEGIN
	  RAISERROR ('CHILD_RECORD_FOUND',16,1);
	RETURN 
END
--delete permission
--[tintType]=1=subtab 
DELETE FROM [GroupTabDetails] WHERE [numTabId]=@TabID AND [tintType]=1
--delete subtabs
delete from  CFw_Grp_Master        
where Grp_id=@TabID  
    
      
end     
   
if @byteMode=4      
begin      
insert into  CFw_Grp_Master(Grp_Name,Loc_Id,numDomainID,tintType,vcURLF)       
values(@TabName,@LocID,@numDomainID,1,@vcURL)      
      
END
--Added by chintan, this mode will add existing subtabs with tintType=2 which can not be deleted  
if @byteMode=5 
begin      
insert into  CFw_Grp_Master(Grp_Name,Loc_Id,numDomainID,tintType,vcURLF)       
values(@TabName,@LocID,@numDomainID,2,@vcURL)      
      
END
--Added by chintan, this mode will add Default subtabs and give permission to all Roles by default
-- Only required Parameter is DomainID
IF @byteMode = 6 
    BEGIN      
        IF ( SELECT COUNT(*) FROM   domain WHERE  numDomainID = @numDomainID ) > 0 
        BEGIN
			DECLARE @i NUMERIC(9)	

			--ContactDetails
			exec USP_ManageTabsInCuSFields @byteMode=5,@LocID=4,@TabName='Contact Details',@TabID=0,@numDomainID=@numDomainID,@vcURL=''
			exec USP_ManageTabsInCuSFields @byteMode=5,@LocID=4,@TabName='Areas of Interest',@TabID=0,@numDomainID=@numDomainID,@vcURL=''
			exec USP_ManageTabsInCuSFields @byteMode=5,@LocID=4,@TabName='Opportunities',@TabID=0,@numDomainID=@numDomainID,@vcURL=''
			exec USP_ManageTabsInCuSFields @byteMode=5,@LocID=4,@TabName='Survey History',@TabID=0,@numDomainID=@numDomainID,@vcURL=''
			exec USP_ManageTabsInCuSFields @byteMode=5,@LocID=4,@TabName='Correspondence',@TabID=0,@numDomainID=@numDomainID,@vcURL=''
			--SO
			exec USP_ManageTabsInCuSFields @byteMode=5,@LocID=2,@TabName='Deal Details',@TabID=0,@numDomainID=@numDomainID,@vcURL=''
			exec USP_ManageTabsInCuSFields @byteMode=5,@LocID=2,@TabName='Milestones & Stages',@TabID=0,@numDomainID=@numDomainID,@vcURL=''
			exec USP_ManageTabsInCuSFields @byteMode=5,@LocID=2,@TabName='Associated Contacts',@TabID=0,@numDomainID=@numDomainID,@vcURL=''
			exec USP_ManageTabsInCuSFields @byteMode=5,@LocID=2,@TabName='Product/Service',@TabID=0,@numDomainID=@numDomainID,@vcURL=''
			exec USP_ManageTabsInCuSFields @byteMode=5,@LocID=2,@TabName='BizDocs',@TabID=0,@numDomainID=@numDomainID,@vcURL=''
			exec USP_ManageTabsInCuSFields @byteMode=5,@LocID=2,@TabName='Correspondence',@TabID=0,@numDomainID=@numDomainID,@vcURL=''
			--Added By Manish || To add Packaging/Shipping in Orders
			exec USP_ManageTabsInCuSFields @byteMode=5,@LocID=2,@TabName='Shipping & Packaging',@TabID=0,@numDomainID=@numDomainID,@vcURL=''
			
			--PO
			exec USP_ManageTabsInCuSFields @byteMode=5,@LocID=6,@TabName='Deal Details',@TabID=0,@numDomainID=@numDomainID,@vcURL=''
			exec USP_ManageTabsInCuSFields @byteMode=5,@LocID=6,@TabName='Milestones & Stages',@TabID=0,@numDomainID=@numDomainID,@vcURL=''
			exec USP_ManageTabsInCuSFields @byteMode=5,@LocID=6,@TabName='Associated Contacts',@TabID=0,@numDomainID=@numDomainID,@vcURL=''
			exec USP_ManageTabsInCuSFields @byteMode=5,@LocID=6,@TabName='Product/Service',@TabID=0,@numDomainID=@numDomainID,@vcURL=''
			exec USP_ManageTabsInCuSFields @byteMode=5,@LocID=6,@TabName='BizDocs',@TabID=0,@numDomainID=@numDomainID,@vcURL=''
			exec USP_ManageTabsInCuSFields @byteMode=5,@LocID=6,@TabName='Correspondence',@TabID=0,@numDomainID=@numDomainID,@vcURL=''
			--Added By Manish || To add Packaging/Shipping in Orders
			exec USP_ManageTabsInCuSFields @byteMode=5,@LocID=6,@TabName='Shipping & Packaging',@TabID=0,@numDomainID=@numDomainID,@vcURL=''
			
			-- Case details
			exec USP_ManageTabsInCuSFields @byteMode=5,@LocID=3,@TabName='Case Details',@TabID=0,@numDomainID=@numDomainID,@vcURL=''
			exec USP_ManageTabsInCuSFields @byteMode=5,@LocID=3,@TabName='Associated Contacts',@TabID=0,@numDomainID=@numDomainID,@vcURL=''
			exec USP_ManageTabsInCuSFields @byteMode=5,@LocID=3,@TabName='Correspondence',@TabID=0,@numDomainID=@numDomainID,@vcURL=''
			-- Project details
			exec USP_ManageTabsInCuSFields @byteMode=5,@LocID=11,@TabName='Project Details',@TabID=0,@numDomainID=@numDomainID,@vcURL=''
			exec USP_ManageTabsInCuSFields @byteMode=5,@LocID=11,@TabName='Milestones And Stages',@TabID=0,@numDomainID=@numDomainID,@vcURL=''
			exec USP_ManageTabsInCuSFields @byteMode=5,@LocID=11,@TabName='Associated Contacts',@TabID=0,@numDomainID=@numDomainID,@vcURL=''
			exec USP_ManageTabsInCuSFields @byteMode=5,@LocID=11,@TabName='Project Income & Expense',@TabID=0,@numDomainID=@numDomainID,@vcURL=''
			exec USP_ManageTabsInCuSFields @byteMode=5,@LocID=11,@TabName='Correspondence',@TabID=0,@numDomainID=@numDomainID,@vcURL=''
			-- Leads locid has been changed from 1 to 14
			exec USP_ManageTabsInCuSFields @byteMode=5,@LocID=14,@TabName='Lead Detail',@TabID=0,@numDomainID=@numDomainID,@vcURL=''
			exec USP_ManageTabsInCuSFields @byteMode=5,@LocID=14,@TabName='Areas of Interest',@TabID=0,@numDomainID=@numDomainID,@vcURL=''
			exec USP_ManageTabsInCuSFields @byteMode=5,@LocID=14,@TabName='Web Analysis',@TabID=0,@numDomainID=@numDomainID,@vcURL=''
			exec USP_ManageTabsInCuSFields @byteMode=5,@LocID=14,@TabName='Correspondence',@TabID=0,@numDomainID=@numDomainID,@vcURL=''
			exec USP_ManageTabsInCuSFields @byteMode=5,@LocID=14,@TabName='Assets',@TabID=0,@numDomainID=@numDomainID,@vcURL=''
			exec USP_ManageTabsInCuSFields @byteMode=5,@LocID=14,@TabName='Associations',@TabID=0,@numDomainID=@numDomainID,@vcURL=''
			exec USP_ManageTabsInCuSFields @byteMode=5,@LocID=14,@TabName='Survey History',@TabID=0,@numDomainID=@numDomainID,@vcURL=''
			-- Prospects
			exec USP_ManageTabsInCuSFields @byteMode=5,@LocID=12,@TabName='Prospect Details',@TabID=0,@numDomainID=@numDomainID,@vcURL=''
			exec USP_ManageTabsInCuSFields @byteMode=5,@LocID=12,@TabName='Contacts',@TabID=0,@numDomainID=@numDomainID,@vcURL=''
			exec USP_ManageTabsInCuSFields @byteMode=5,@LocID=12,@TabName='Transactions',@TabID=0,@numDomainID=@numDomainID,@vcURL=''
			exec USP_ManageTabsInCuSFields @byteMode=5,@LocID=12,@TabName='Projects',@TabID=0,@numDomainID=@numDomainID,@vcURL=''
			exec USP_ManageTabsInCuSFields @byteMode=5,@LocID=12,@TabName='Default Settings',@TabID=0,@numDomainID=@numDomainID,@vcURL=''
			exec USP_ManageTabsInCuSFields @byteMode=5,@LocID=12,@TabName='Web Analysis',@TabID=0,@numDomainID=@numDomainID,@vcURL=''
			exec USP_ManageTabsInCuSFields @byteMode=5,@LocID=12,@TabName='Correspondence',@TabID=0,@numDomainID=@numDomainID,@vcURL=''
			exec USP_ManageTabsInCuSFields @byteMode=5,@LocID=12,@TabName='Assets',@TabID=0,@numDomainID=@numDomainID,@vcURL=''
			exec USP_ManageTabsInCuSFields @byteMode=5,@LocID=12,@TabName='Associations',@TabID=0,@numDomainID=@numDomainID,@vcURL=''
			-- Accounts
			exec USP_ManageTabsInCuSFields @byteMode=5,@LocID=13,@TabName='Account Details',@TabID=0,@numDomainID=@numDomainID,@vcURL=''
			exec USP_ManageTabsInCuSFields @byteMode=5,@LocID=13,@TabName='Contacts',@TabID=0,@numDomainID=@numDomainID,@vcURL=''
			exec USP_ManageTabsInCuSFields @byteMode=5,@LocID=13,@TabName='Transactions',@TabID=0,@numDomainID=@numDomainID,@vcURL=''
			exec USP_ManageTabsInCuSFields @byteMode=5,@LocID=13,@TabName='Projects',@TabID=0,@numDomainID=@numDomainID,@vcURL=''
			exec USP_ManageTabsInCuSFields @byteMode=5,@LocID=13,@TabName='Cases',@TabID=0,@numDomainID=@numDomainID,@vcURL=''
			exec USP_ManageTabsInCuSFields @byteMode=5,@LocID=13,@TabName='Default Settings',@TabID=0,@numDomainID=@numDomainID,@vcURL=''
			exec USP_ManageTabsInCuSFields @byteMode=5,@LocID=13,@TabName='Web Analysis',@TabID=0,@numDomainID=@numDomainID,@vcURL=''
			exec USP_ManageTabsInCuSFields @byteMode=5,@LocID=13,@TabName='Correspondence',@TabID=0,@numDomainID=@numDomainID,@vcURL=''
			exec USP_ManageTabsInCuSFields @byteMode=5,@LocID=13,@TabName='Assets',@TabID=0,@numDomainID=@numDomainID,@vcURL=''
			exec USP_ManageTabsInCuSFields @byteMode=5,@LocID=13,@TabName='Contracts',@TabID=0,@numDomainID=@numDomainID,@vcURL=''
			exec USP_ManageTabsInCuSFields @byteMode=5,@LocID=13,@TabName='Associations',@TabID=0,@numDomainID=@numDomainID,@vcURL=''
			
			--Added By Sachin ||Issue raised by Customer:EasternBikes
			--Reason:To Make default entry for any new subscription
			exec USP_ManageTabsInCuSFields @byteMode=5,@LocID=13,@TabName='Items',@TabID=0,@numDomainID=@numDomainID,@vcURL=''
			exec USP_ManageTabsInCuSFields @byteMode=5,@LocID=15,@TabName='Bought & Sold',@TabID=0,@numDomainID=@numDomainID,@vcURL=''
			exec USP_ManageTabsInCuSFields @byteMode=5,@LocID=15,@TabName='Vended',@TabID=0,@numDomainID=@numDomainID,@vcURL=''
			--end of code		
			--Added By Sachin||Issue raised by Customer:Coloroda State Uni
			exec USP_ManageTabsInCuSFields @byteMode=5,@LocID=2,@TabName='Shipping & Packaging',@TabID=0,@numDomainID=@numDomainID,@vcURL=''
            exec USP_ManageTabsInCuSFields @byteMode=5,@LocID=6,@TabName='Shipping & Packaging',@TabID=0,@numDomainID=@numDomainID,@vcURL=''
			--end of Code

			exec USP_ManageTabsInCuSFields @byteMode=5,@LocID=17,@TabName='Action Items & Meetings',@TabID=0,@numDomainID=@numDomainID,@vcURL=''
			exec USP_ManageTabsInCuSFields @byteMode=5,@LocID=17,@TabName='Opportunities & Projects',@TabID=0,@numDomainID=@numDomainID,@vcURL=''
			exec USP_ManageTabsInCuSFields @byteMode=5,@LocID=17,@TabName='Cases',@TabID=0,@numDomainID=@numDomainID,@vcURL=''
			exec USP_ManageTabsInCuSFields @byteMode=5,@LocID=17,@TabName='Approval Requests',@TabID=0,@numDomainID=@numDomainID,@vcURL=''
			/*********Get Auth groups */
			---- Give permission by default
				DECLARE  @minGroupID NUMERIC(9)
				DECLARE  @maxGroupID NUMERIC(9)
				SELECT @minGroupID=MIN(numGroupID) FROM AuthenticationGroupMaster WHERE [numDomainID]=@numDomainID
				SELECT @maxGroupID=MAX(numGroupID) FROM AuthenticationGroupMaster WHERE [numDomainID]=@numDomainID
				WHILE  @minGroupID <= @maxGroupID
				  BEGIN
					PRINT 'GroupID=' + CONVERT(VARCHAR(20),@minGroupID)
							Set @i=0
							DECLARE  @minTabID NUMERIC(9)
							DECLARE  @maxTabID NUMERIC(9)
							SELECT @minTabID=MIN([Grp_id]) FROM [CFw_Grp_Master] WHERE [numDomainID]=@numDomainID 
							SELECT @maxTabID=max([Grp_id]) FROM [CFw_Grp_Master] WHERE [numDomainID]=@numDomainID 
							WHILE @minTabID<=@maxTabID
							BEGIN
								PRINT 'TabID=' + CONVERT(VARCHAR(20),@minTabID)
								---------------
								insert into GroupTabDetails
								(numGroupId,numTabId,bitallowed,numOrder,numRelationShip,tintType)
								VALUES (@minGroupID,@minTabID,1,@i+1,0,1)

								---------------
								SELECT @minTabID=MIN([Grp_id]) FROM [CFw_Grp_Master] WHERE [numDomainID]=@numDomainID AND [Grp_id]>@minTabID
							END
					
					SELECT @minGroupID = min(numGroupID)
					FROM AuthenticationGroupMaster WHERE [numDomainID]= @numDomainID AND [numGroupID] > @minGroupID
				  END		


        END
      
    END
GO
