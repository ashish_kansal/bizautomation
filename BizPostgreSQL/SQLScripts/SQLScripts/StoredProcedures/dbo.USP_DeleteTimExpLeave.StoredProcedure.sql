/****** Object:  StoredProcedure [dbo].[USP_DeleteTimExpLeave]    Script Date: 07/26/2008 16:15:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj        
--USP_DeleteTimExpLeave 164,72
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_deletetimexpleave')
DROP PROCEDURE usp_deletetimexpleave
GO
CREATE PROCEDURE [dbo].[USP_DeleteTimExpLeave]        
@numCategoryHDRID as numeric(9)=0,      
@numDomainId as numeric(9)=0   
  
as        
Begin      
Declare @numCategory as numeric(9)      
Declare @numType as numeric(9)      
Declare @numJournalId as numeric(9)      
DECLARE @numProId AS NUMERIC(9)
DECLARE @numStageID AS NUMERIC(9)
DECLARE @numOppItemID AS NUMERIC(9)

Select @numCategory=numCategory,@numType=numType,@numOppItemID=isnull(numOppItemID,0),@numProId = isnull([numProId],0),@numStageID = ISNULL(numStageId,0) From  TimeAndExpense where numCategoryHDRID=@numCategoryHDRID
--Delete Journal entries 
If  @numCategory=1 OR @numCategory=2  OR @numCategory=3 
   Begin      
	  Select @numJournalId=numJournal_Id From General_Journal_Header WHERE (numProjectID=@numProId OR @numProId =0) AND numCategoryHDRID=@numCategoryHDRID And numDomainId=@numDomainId      
	  Delete from General_Journal_Details where numJournalId=@numJournalId and numDomainId=@numDomainId      
	  Delete from General_Journal_Header Where numJournal_Id=@numJournalId and numDomainId=@numDomainId      
   End      

IF (@numProId >0 AND @numOppItemID >0 )
BEGIN
--	--Delete bizdoc service item 
--	DELETE FROM dbo.OpportunityBizDocItems WHERE numOppBizDocItemID =@numOppBizDocItemID
--	--delete linking id between project stage and bizdoc 
--	DELETE FROM [ProjectsOpportunities] Where numProID=@numProId AND numStageID = @numStageID and numOppBizDocID = @numOppBizDocsId AND numDomainID = @numDomainId	

	--Update OpportunityItems and Reset Project and StageId
   Update OpportunityItems set numProjectID=null,numProjectStageID=null where numoppitemtCode=@numOppItemID
END
--Delete PK
delete from TimeAndExpense where numCategoryHDRID=@numCategoryHDRID   and numDomainId=@numDomainId    
--remove reference of time 
UPDATE dbo.General_Journal_Header SET numCategoryHDRID = NULL WHERE numCategoryHDRID=@numCategoryHDRID
  
  
End
GO
