/****** Object:  StoredProcedure [dbo].[GetParentOperationBudgetDetails]    Script Date: 07/26/2008 16:14:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created By Siva                      
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='getparentoperationbudgetdetails')
DROP PROCEDURE getparentoperationbudgetdetails
GO
CREATE PROCEDURE [dbo].[GetParentOperationBudgetDetails]                    
(@numBudgetId as numeric(9)=0,                
@numDomainId as numeric(9)=0,            
@intFiscalYear as int=0,            
@sintByte as smallint=0,
@intType as int=0
)                    
As                    
Begin       
Declare @Date as datetime
Set @Date = dateadd(year,@intType,getutcdate()) 
--@sintByte =0 -> Operation Budget @sintByte=1 -> Market Budget @sintByte=2 -> Procurement Budget                  
--if @sintByte =0            
-- Begin             
-- Select distinct  OBD.numChartAcntId as numChartAcntId,COA.vcCatgyName as vcData,isnull(dbo.fn_GetBudgetMonthTotalAmtForTreeView(@numBudgetId,@Date,OBD.numChartAcntId,@sintByte,@numDomainId),0) as monAmount From OperationBudgetMaster OBM                  
-- inner join OperationBudgetDetails OBD on OBM.numBudgetId=OBD.numBudgetId         
-- inner join Chart_of_Accounts COA on OBD.numChartAcntId=COA.numAccountId         
-- Where OBD.numBudgetId=@numBudgetId And OBM.numDomainId=@numDomainId                   
-- End            
--if @sintByte=1            
--Begin            
-- Select distinct MBD.numListItemID as numChartAcntId,LD.vcData as vcData,isnull(dbo.fn_GetBudgetMonthTotalAmtForTreeView(MBD.numMarketId,@Date,MBD.numListItemID,@sintByte,@numDomainId),0) as monAmount From MarketBudgetMaster MBM                  
-- inner join MarketBudgetDetails MBD on MBM.numMarketBudgetId=MBD.numMarketId              
-- inner join ListDetails LD on MBD.numListItemID= LD.numListItemID Where -- MBD.intYear=@intFiscalYear And 
-- MBM.numDomainId=@numDomainId              
--End            
--if @sintByte=2            
--Begin           
--           
-- Select distinct PBD.numItemGroupId  as numChartAcntId,IG.vcItemGroup as vcData,isnull(dbo.fn_GetBudgetMonthTotalAmtForTreeView(PBM.numProcurementBudgetId,@Date,PBD.numItemGroupId,@sintByte,@numDomainId),0) as monAmount From ProcurementBudgetMaster PBM                  
-- inner join ProcurementBudgetDetails PBD on PBM.numProcurementBudgetId=PBD.numProcurementId            
-- inner join ItemGroups IG on IG.numItemGroupID=PBD.numItemGroupId            
-- Where --PBD.intYear=@intFiscalYear And 
--PBM.numDomainId=@numDomainId              
--End            
End
GO
