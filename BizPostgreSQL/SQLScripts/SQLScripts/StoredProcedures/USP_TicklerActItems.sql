GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_TicklerActItems')
DROP PROCEDURE USP_TicklerActItems
GO
CREATE Proc [dbo].[USP_TicklerActItems]                                                                        
@numUserCntID as numeric(9)=null,                                                                        
@numDomainID as numeric(9)=null,                                                                        
@startDate as datetime,                                                                        
@endDate as datetime,                                                                    
@ClientTimeZoneOffset Int  --Added by Debasish to enable calculation of date according to client machine                                                                      
                                                  
As                                                                         
                                                            
 Select * from (                                                    
 SELECT Addc.numContactID,Addc.numDivisionID,Div.tintCRMType,Comm.numCommId AS Id,convert(varchar(15),bitTask) as bitTask,                                               
 DateAdd(minute, -@ClientTimeZoneOffset, dtStartTime) as Startdate,                        
 DateAdd(minute, -@ClientTimeZoneOffset, dtEndTime) as EndTime ,                                                                                                                                                                  
 --DateAdd(minute, -@ClientTimeZoneOffset, dtStartTime) AS CloseDate,                                                                         
 Comm.textDetails AS itemDesc,                                                                                                               
 AddC.vcFirstName + ' ' + AddC.vcLastName  as [Name],                                
 case when Addc.numPhone<>'' then + AddC.numPhone +case when AddC.numPhoneExtension<>'' then ' - ' + AddC.numPhoneExtension else '' end  else '' end as [Phone],                                                                       
 Comp.vcCompanyName As vcCompanyName , --changed by bharat so that the complete name is displayed                                                                
 AddC.vcEmail As vcEmail ,                                                                         
-- case when Comm.bitTask=971 then  'Communication' when Comm.bitTask=2 then  'Opportunity'       
--when Comm.bitTask = 972 then  'Task' when Comm.bitTask=973 then  'Notes' When Comm.bitTask = 974 then        
--'Follow-up' else 'Unknown' end         
dbo.GetListIemName(Comm.bitTask)AS Task,                
 listdetailsActivity.VcData As Activity ,                                                    
 isnull(listdetailsStatus.VcData,'') As Status   ,  -- Priority column                                                                                             
 Comm.numCreatedBy,                                                                                                             
 Div.numTerId,                                           
 case When Len( dbo.fn_GetContactName(numAssign) ) > 12                                           
  Then Substring( dbo.fn_GetContactName(numAssign) , 0  ,  12 ) + '..'                                           
  Else dbo.fn_GetContactName(numAssign)                                           
 end                                          
 +                                          
 ' / '                                             
 +                                              
 case                                                
  When Len( dbo.fn_GetContactName(Comm.numAssignedBy) ) > 12                                           
  Then Substring( dbo.fn_GetContactName(Comm.numAssignedBy) , 0  ,  12 ) + '..'                                         Else dbo.fn_GetContactName(Comm.numAssignedBy)                                          
 end  As AssignedTo ,         
convert(varchar(50),comm.caseid) as caseid,                          
(select top 1 vcCaseNumber from cases where cases.numcaseid= comm.caseid )as vcCasenumber,                          
comm.casetimeId,                          
comm.caseExpId ,                        
0 as type   ,                    
'' as itemid                       ,                    
'' as changekey ,
dtEndTime as DueDate                                               
 FROM  Communication Comm                              
 JOIN AdditionalContactsInformation AddC                                                  
 ON Comm.numContactId = AddC.numContactId                                  
 JOIN DivisionMaster Div                                                                         
 ON AddC.numDivisionId = Div.numDivisionID                                   
 JOIN CompanyInfo  Comp                                                                         
 ON Div.numCompanyID = Comp.numCompanyId                                                       
 Left Join listdetails listdetailsActivity                                                  
 On Comm.numActivity = listdetailsActivity.NumlistItemID                                               
 Left Join listdetails listdetailsStatus                                                 
 On Comm.numStatus = listdetailsStatus.NumlistItemID                                   
 WHERE Comm.numDomainID = @numDomainID                                                                         
 AND (Comm.numAssign =@numUserCntID or Comm.numCreatedBy=@numUserCntID)                                                                 
 AND (dtStartTime >= @startDate and dtStartTime <= @endDate)                                                     
 AND Comm.bitclosedflag=0                                                                         
 and Comm.bitTask <> 973  ) As X               
  

         
select                          
 ACI.numContactID,ACI.numDivisionID,Div.tintCRMType,ac.activityid as id ,case when alldayevent = 1 then '5' else '0' end as bitTask,                        
DateAdd(minute, -@ClientTimeZoneOffset, startdatetimeutc) startdate,                        
DateAdd(minute, -@ClientTimeZoneOffset, dateadd(second,duration,startdatetimeutc)) as endtime,                        
activitydescription as itemdesc,                        
ACI.vcFirstName + ' ' + ACI.vcLastName name,                        
 case when ACI.numPhone<>'' then + ACI.numPhone +case when ACI.numPhoneExtension<>'' then ' - ' + ACI.numPhoneExtension else '' end  else '' end as [Phone],                        
 Substring ( Comp.vcCompanyName , 0 , 12 ) + '..' As vcCompanyName ,                      
vcEmail,                        
'Calandar' as task,                        
[subject] as Activity,                        
isnull(case when importance =0 then 'Low' when importance =1 then 'Normal' when importance =2 then 'High' end,'') as status,                        
0 as numcreatedby,                        
0 as numterid,                        
'-/-' as assignedto,                        
'0' as caseid,                        
null as vccasenumber,                        
0 as casetimeid,                        
0 as caseexpid,                        
0 as type ,                    
itemid  ,                    
changekey  ,
startdatetimeutc as DueDate                      
from activity ac join             
activityresource ar on  ac.activityid=ar.activityid             
join resource res on ar.resourceid=res.resourceid                         
--join usermaster UM on um.numUserId = res.numUserCntId                
join AdditionalContactsInformation ACI on ACI.numContactId = res.numUserCntId                
 JOIN DivisionMaster Div                                                          
 ON ACI.numDivisionId = Div.numDivisionID                                   
 JOIN CompanyInfo  Comp                             
 ON Div.numCompanyID = Comp.numCompanyId                 
 where  res.numUserCntId = @numUserCntID            
and ac.OriginalStartDateTimeUtc IS NULL            
and res.numDomainID = @numDomainID  and (startdatetimeutc between dateadd(month,-1,getutcdate()) and dateadd(month,1,getutcdate())) and [status] <> -1                                                     
and ac.activityid not in 
(select distinct(numActivityId) from communication Comm where Comm.numDomainID = @numDomainID                                                                         
 AND (Comm.numAssign =@numUserCntID or Comm.numCreatedBy=@numUserCntID)                                                                 
 AND (dtStartTime >= @startDate and dtStartTime <= @endDate) )
 Order by endtime




select 
B.numContactID,B.numDivisionID,E.tintCRMType,numBizActionID as ID,
convert(varchar(15),A.bitTask) as bitTask,
DateAdd(minute, -@ClientTimeZoneOffset, dtCreatedDate) as Startdate,                        
DateAdd(minute, -@ClientTimeZoneOffset, dtCreatedDate) as EndTime ,  
'' as itemDesc,
vcFirstName + ' ' + vcLastName  as [Name],
case when B.numPhone<>'' then + B.numPhone +case when B.numPhoneExtension<>'' then ' - ' + B.numPhoneExtension else '' end  else '' end as [Phone],
vcCompanyName,
vcEmail,
'Approval Request' as Task,
dbo.GetListIemName(bitTask) as Activity,
'' as Status,
A.numCreatedBy,
E.numTerId, 
case When Len( dbo.fn_GetContactName(numAssign) ) > 12                                           
  Then Substring( dbo.fn_GetContactName(numAssign) , 0  ,  12 ) + '..'                                           
  Else dbo.fn_GetContactName(numAssign)                                           
 end                                          
 AssignedTo ,  
'0' as caseid,
'0' as vcCasenumber,
0 as CaseTimeId,
0 as CaseExpId,
-1 as type   ,                    
'' AS itemid  ,                    
 '' AS changekey ,   
dtCreatedDate as DueDate
from BizDocAction A , 
AdditionalContactsInformation B, 
CompanyInfo D,
DivisionMaster E

WHERE A.numContactId=b.numContactId and a.numDomainID=b.numDomainID  and 
D.numCompanyId=E.numCompanyId and 
D.numDomainID=a.numDomainID and 
E.numDivisionId=B.numDivisionId and 
a.numDomainID= @numDomainID and a.numContactId = @numUserCntID and 
A.numStatus=0
GO