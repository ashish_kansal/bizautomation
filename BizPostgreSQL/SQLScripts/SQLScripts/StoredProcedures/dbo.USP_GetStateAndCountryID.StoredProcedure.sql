/****** Object:  StoredProcedure [dbo].[USP_GetStateAndCountryID]    Script Date: 07/26/2008 16:18:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--- Created By Anoop Jayaraj  
  
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getstateandcountryid')
DROP PROCEDURE usp_getstateandcountryid
GO
CREATE PROCEDURE [dbo].[USP_GetStateAndCountryID]  
@byteMode as tinyint=0,  
@vcText as varchar(100)='',
@numDomainID as numeric(9),
@numListID AS NUMERIC(9)=0
as  
  
if @byteMode=0 -- ListMaster  
select isnull(numListItemID,0) from ListDetails   
where vcData=@vcText and (numDomainID=@numDomainID or constFlag=1)
  
  
if @byteMode=1 -- State  
select isnull(numStateID,0) from [State]   
where vcState=@vcText and numDomainID=@numDomainID 


if @byteMode=2 -- GroupID 
select isnull(numItemGroupID,0) from ItemGroups   
where vcItemGroup=@vcText and numDomainID=@numDomainID

if @byteMode=3 -- Item Type

	select case @vcText when 'Inventory Item' then 'P' when 'Service' then 'S' else 'N' end

if @byteMode=4 -- Item Classification
	select top 1 numListItemID from ListDetails where numListid=36 and numDomainID=@numDomainID and vcData =@vcText

if @byteMode=5 -- Income Account
	select top 1 numAccountId from chart_of_accounts where numDomainID=@numDomainID and vcAccountName  =@vcText

if @byteMode=6 -- Warehouse
	select top 1 numWareHouseId from warehouses where numDomainID=@numDomainID and vcWareHouse=@vcText

if @byteMode=7 -- Preffered Vendor
	select top 1 DM.numDivisionId from DivisionMaster DM INNER JOIN CompanyInfo CI ON DM.numCompanyId=CI.numCompanyId
 where DM.numDomainId=@numDomainID and CI.vcCompanyName=@vcText

if @byteMode=8 -- Department
	select numListItemID from ListDetails where numListid=19 and numDomainId=@numDomainID and vcData=@vcText
if @byteMode=9 -- Category
	select top 1 numCategoryID from category where vcCategoryName=@vcText and numDomainID=@numDomainId

if @byteMode=10 -- Action Item Priority
	select top 1 numListItemID from ListDetails where numListid=33 and numDomainID=@numDomainID and vcData =@vcText

if @byteMode=11 -- Action Item Type
	select top 1 numListItemID from ListDetails where numListid=73 and (numDomainID=@numDomainID OR constFlag=1) and vcData =@vcText

if @byteMode=12 -- Action Item Activity
	select top 1 numListItemID from ListDetails where numListid=32 and numDomainID=@numDomainID and vcData =@vcText

if @byteMode=13 -- Association Type
	select top 1 numListItemID from ListDetails where numListid=43 and numDomainID=@numDomainID and vcData =@vcText
	
if @byteMode=14 -- User Name
	select top 1 numUserDetailID from dbo.UserMaster UM INNER JOIN dbo.AdditionalContactsInformation ACI ON ACI.numContactId=UM.numUserDetailId where UM.numDomainID=@numDomainID AND ISNULL(ACI.vcFirstName,'') + ' ' + ISNULL(ACI.vcLastName,'') = @vcText
/*changed code: while importing username of usermaster doesn't containt updated first name last name.. no way to update that field */
if @byteMode=15 -- UOM
	Begin
		--select top 1 numUOMId from dbo.UOM where numDomainID=@numDomainID AND vcUnitName = @vcText
		 SELECT TOP 1 u.numUOMId FROM UOM u INNER JOIN Domain d ON u.numDomainID=d.numDomainID
				WHERE u.numDomainID=@numDomainID AND d.numDomainID=@numDomainID AND u.vcUnitName = @vcText AND 
				u.tintUnitType=(CASE WHEN ISNULL(d.charUnitSystem,'E')='E' THEN 1 WHEN d.charUnitSystem='M' THEN 2 END)
	END
if @byteMode=16 -- Assembly Item or Kit Item
	select top 1 numItemCode from Item where numDomainID=@numDomainID and vcItemName=@vcText AND ISNULL(bitKitParent,0)=1 
--AND ISNULL(bitAssembly,0)=1

if @byteMode=17 -- Item
	select top 1 numItemCode from Item where numDomainID=@numDomainID and vcItemName=@vcText

if @byteMode=18 -- ListDetail
select isnull(numListItemID,0) from ListDetails   
where (numListID= @numListID OR @numListID=0) and vcData=@vcText and (numDomainID=@numDomainID or constFlag=1)
  
if @byteMode=19 -- TaxItems
select top 1 isnull(numTaxItemID,0) from TaxItems where vcTaxName=@vcText and numDomainID=@numDomainID
