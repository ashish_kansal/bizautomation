
/* OBSOLETE PROCEDEURE*/
/****** Object:  StoredProcedure [dbo].[USP_ProjectSaveMilestone]    Script Date: 07/26/2008 16:20:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj                  
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_projectsavemilestone')
DROP PROCEDURE usp_projectsavemilestone
GO
CREATE PROCEDURE [dbo].[USP_ProjectSaveMilestone]                    
(                                        
 @numProId as numeric(9)=null,                    
 @strMilestone as text=''                    
)                    
as                    
declare @status as tinyint                    
declare @hDoc3 int                    
				   
if convert(varchar(10),@strMilestone) <>''                          
begin                   
EXEC sp_xml_preparedocument @hDoc3 OUTPUT, @strMilestone   


   delete from ProjectsStageDetails                                               
   where numProStageId not in (SELECT StageID FROM OPENXML(@hDoc3,'/NewDataSet/Table',2) with(StageID numeric,Op_Flag numeric))                                             
   and numProID=  @numProId                   
					
  update ProjectsStageDetails set                     
 vcstageDetail=X.vcstageDetail,                    
 numModifiedBy=X.numModifiedBy,                    
 bintModifiedDate=X.bintModifiedDate,                    
 bintDueDate=X.bintDueDate,                    
 bintStageComDate=X.bintStageComDate,                    
 vcComments=X.vcComments,                    
 numAssignTo=X.numAssignTo,                    
 bitAlert=X.bitAlert,                    
 bitStageCompleted=X.bitStageCompleted,                
 numStage=X.numStage ,        
 vcStagePercentageDtl= x.vcStagePercentageDtl  ,        
 numTemplateId = x.numTemplateId ,      
 tintPercentage=X.tintPercentage  ,    
 numEvent=X.numEvent,    
 numReminder=X.numReminder,    
 numET=X.numET,    
 numActivity =X.numActivity ,    
 numStartDate=X.numStartDate,    
 numStartTime=X.numStartTime,    
 numStartTimePeriod=X.numStartTimePeriod,    
 numEndTime=X.numEndTime,    
 numEndTimePeriod=X.numEndTimePeriod,    
 txtCom=X.txtCom ,  
numChgStatus =x.numChgStatus,  
bitChgStatus =x.bitChgStatus ,  
bitClose=x.bitClose,  
numType=x.numType ,numCommActId=x.numCommActId 
	From (SELECT *                    
   FROM OPENXML(@hDoc3,'/NewDataSet/Table[StageID>0][Op_Flag=0]',2)                     
	with(StageID numeric,                    
	vcstageDetail varchar(100),                  
	numModifiedBy numeric,                    
	bintModifiedDate datetime,                    
	bintDueDate varchar(23),                    
	bintStageComDate datetime,                    
	vcComments varchar(1000),                    
	numAssignTo numeric,                    
 bitAlert bit,                    
	bitStageCompleted bit,                    
	Op_Flag numeric,                
	numStage numeric,        
 vcStagePercentageDtl varchar(500),        
 numTemplateId numeric,      
 tintPercentage tinyint,    
 numEvent tinyint,    
 numReminder numeric,    
 numET numeric,    
 numActivity numeric ,    
 numStartDate tinyint,    
 numStartTime tinyint,    
 numStartTimePeriod tinyint ,    
 numEndTime tinyint ,    
 numEndTimePeriod tinyint,     
 txtCom varchar(1000)  ,numChgStatus numeric(9) ,bitChgStatus bit ,bitClose bit,numType numeric(9) ,numCommActId numeric(9) 
))X                     
	where  numProStageId=X.StageID                    
					
					
 insert into ProjectsStageDetails                    
  (numProId,vcStageDetail,numStagePercentage,numDomainId,                    
  numCreatedBy,bintCreatedDate,numModifiedBy,bintModifiedDate,                    
  bintDueDate,bintStageComDate,vcComments,numAssignTo,bitAlert,                    
  bitStageCompleted,numStage,vcStagePercentageDtl,numTemplateId,tintPercentage,numEvent,numReminder,numET,numActivity ,numStartDate,numStartTime,numStartTimePeriod ,numEndTime ,    
numEndTimePeriod, txtCom,numChgStatus ,bitChgStatus  ,bitClose,numType,numCommActId)                    
					 
 select @numProId,                    
  X.vcstageDetail ,                    
  X.numStagePercentage ,                    
  X.numDomainId,                    
  X.numCreatedBy ,                    
  X.bintCreatedDate ,                    
  X.numModifiedBy ,                    
  X.bintModifiedDate ,                    
  X.bintDueDate ,                    
  X.bintStageComDate ,                    
  X.vcComments ,                    
  X.numAssignTo,                    
  X.bitAlert ,                    
  X.bitStageCompleted,                
  X.numStage,x.vcStagePercentageDtl,x.numTemplateId,X.tintPercentage,X.numEvent,    
 X.numReminder,    
 X.numET,    
 X.numActivity ,    
 X.numStartDate,    
 X.numStartTime,    
 X.numStartTimePeriod ,    
 X.numEndTime ,    
 X.numEndTimePeriod,     
 X.txtCom ,x.numChgStatus ,x.bitChgStatus  ,x.bitClose,x.numType,x.numCommActId  from(SELECT *FROM OPENXML (@hDoc3,'/NewDataSet/Table[StageID=0][Op_Flag=0]',2)                    
 WITH  (StageID numeric,                    
  vcstageDetail varchar(100),                    
  numStagePercentage numeric,                    
  numDomainId numeric,                    
  numCreatedBy numeric,                    
  bintCreatedDate datetime,                    
  numModifiedBy numeric,                    
  bintModifiedDate datetime,                    
  bintDueDate varchar(23),                    
  bintStageComDate datetime,                    
  vcComments varchar(1000),                    
  numAssignTo numeric,                    
  bitAlert bit,                    
  bitStageCompleted bit,                    
  Op_Flag numeric ,                
  numStage numeric ,        
  vcStagePercentageDtl varchar(500),        
  numTemplateId numeric,      
  tintPercentage tinyint  ,    
 numEvent tinyint,    
 numReminder numeric,    
 numET numeric,    
 numActivity numeric ,    
 numStartDate tinyint,    
 numStartTime tinyint,    
 numStartTimePeriod tinyint ,    
 numEndTime tinyint ,    
 numEndTimePeriod tinyint,     
 txtCom varchar(1000),numChgStatus numeric(9) ,bitChgStatus bit ,bitClose     bit,numType numeric(9) ,numCommActId numeric(9) 
  ))X                    
				   
					
					
					
 EXEC sp_xml_removedocument @hDoc3                    
					
				 
				  
				  
select @status=bitstageCompleted from ProjectsMaster mst                    
left join  ProjectsStageDetails  stg                    
on mst.numProId=stg.numProId                    
where stg.numstagepercentage=100 and mst.numproid=@numProId                     
 if @status=1                     
 begin                    
  update ProjectsMaster set tintProStatus=1,bintProClosingDate=getutcdate() where numProid=@numProId and tintProStatus<>1                    
					 
 end                    
					
					
end
GO
