/****** Object:  StoredProcedure [dbo].[usp_GetTop5Accounts]    Script Date: 07/26/2008 16:18:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_gettop5accounts')
DROP PROCEDURE usp_gettop5accounts
GO
CREATE PROCEDURE [dbo].[usp_GetTop5Accounts]            
 @numDomainID numeric,            
 @dtDateFrom datetime,            
 @dtDateTo datetime,            
 @numTerID numeric=0,            
 @numUserCntID numeric=0,            
 @tintRights tinyint=3,            
 @intType numeric=0            
--            
AS            
BEGIN            
 IF @tintRights=3 --All Records            
 BEGIN            
  IF @intType = 0             
  BEGIN            
   SELECT TOP 5 CI.vcCompanyName + ' - (' + DM.vcDivisionName + ')' As CompanyName,             
     sum(OM.monPAmount) As Amount            
     FROM OpportunityMaster OM          
     JOIN DivisionMaster DM          
     ON DM.numDivisionID=OM.numDivisionID            
     JOIN CompanyInfo CI            
     ON CI.numCompanyID=DM.numCompanyID            
     WHERE OM.tintOppStatus=1   and OM.tintOppType=1       
      AND OM.bintAccountClosingDate BETWEEN @dtDateFrom AND @dtDateTo            
      AND OM.numDomainID=@numDomainID            
     GROUP BY CI.vcCompanyName + ' - (' + DM.vcDivisionName + ')'            
     ORDER BY Amount DESC            
          
  END            
  ELSE            
  BEGIN            
   SELECT TOP 5 CI.vcCompanyName + ' - (' + DM.vcDivisionName + ')' As CompanyName,             
     sum(OM.monPAmount) As Amount            
     FROM OpportunityMaster OM          
     JOIN DivisionMaster DM          
     ON DM.numDivisionID=OM.numDivisionID            
     JOIN CompanyInfo CI            
     ON CI.numCompanyID=DM.numCompanyID            
     JOIN AdditionalContactsInformation AI          
     ON AI.numContactID = OM.numRecOwner          
     WHERE OM.tintOppStatus=1  and OM.tintOppType=1        
      AND OM.bintAccountClosingDate BETWEEN @dtDateFrom AND @dtDateTo            
      AND OM.numDomainID=@numDomainID            
      AND AI.numTeam is not null               
      AND AI.numTeam in(select F.numTeam from ForReportsByTeam F               
     where F.numuserCntId=@numUserCntID and F.numDomainid=@numDomainId and F.tintType=@intType)              
     GROUP BY CI.vcCompanyName + ' - (' + DM.vcDivisionName + ')'            
     ORDER BY Amount DESC            
          
           
  END            
 END            
            
 IF @tintRights=2 --Territory Records            
 BEGIN            
   IF @intType = 0            
   BEGIN            
   SELECT TOP 5 CI.vcCompanyName + ' - (' + DM.vcDivisionName + ')' As CompanyName,             
     sum(OM.monPAmount) As Amount            
     FROM OpportunityMaster OM          
     JOIN DivisionMaster DM          
     ON DM.numDivisionID=OM.numDivisionID            
     JOIN CompanyInfo CI            
     ON CI.numCompanyID=DM.numCompanyID            
     WHERE OM.tintOppStatus=1     and OM.tintOppType=1     
      AND OM.bintAccountClosingDate BETWEEN @dtDateFrom AND @dtDateTo            
      AND OM.numDomainID=@numDomainID            
     AND DM.numTerID in (SELECT numTerritoryID FROM UserTerritory WHERE numUserCntID=@numUserCntID)          
     GROUP BY CI.vcCompanyName + ' - (' + DM.vcDivisionName + ')'            
     ORDER BY Amount DESC            
          
   END            
   ELSE            
   BEGIN            
   SELECT TOP 5 CI.vcCompanyName + ' - (' + DM.vcDivisionName + ')' As CompanyName,             
     sum(OM.monPAmount) As Amount            
     FROM OpportunityMaster OM          
     JOIN DivisionMaster DM          
     ON DM.numDivisionID=OM.numDivisionID            
     JOIN CompanyInfo CI            
     ON CI.numCompanyID=DM.numCompanyID            
     JOIN AdditionalContactsInformation AI          
     ON AI.numContactID = OM.numRecOwner          
     WHERE OM.tintOppStatus=1      and OM.tintOppType=1    
      AND OM.bintAccountClosingDate BETWEEN @dtDateFrom AND @dtDateTo            
      AND OM.numDomainID=@numDomainID            
     AND DM.numTerID in (SELECT numTerritoryID FROM UserTerritory WHERE numUserCntID=@numUserCntID)          
      AND AI.numTeam is not null               
      AND AI.numTeam in(select F.numTeam from ForReportsByTeam F               
     where F.numuserCntId=@numUserCntID and F.numDomainid=@numDomainId and F.tintType=@intType)              
     GROUP BY CI.vcCompanyName + ' - (' + DM.vcDivisionName + ')'            
     ORDER BY Amount DESC            
          
            
   END            
 END            
            
 IF @tintRights=1 --Owner Records            
 BEGIN            
   IF @intType = 0            
   BEGIN            
   SELECT TOP 5 CI.vcCompanyName + ' - (' + DM.vcDivisionName + ')' As CompanyName,             
     sum(OM.monPAmount) As Amount            
     FROM OpportunityMaster OM          
     JOIN DivisionMaster DM          
     ON DM.numDivisionID=OM.numDivisionID            
     JOIN CompanyInfo CI            
     ON CI.numCompanyID=DM.numCompanyID            
     WHERE OM.tintOppStatus=1  and OM.tintOppType=1        
      AND OM.bintAccountClosingDate BETWEEN @dtDateFrom AND @dtDateTo             
      AND OM.numDomainID=@numDomainID            
     AND OM.numRecOwner=@numUserCntID            
     GROUP BY CI.vcCompanyName + ' - (' + DM.vcDivisionName + ')'            
     ORDER BY Amount DESC            
          
   END            
   ELSE            
   BEGIN            
   SELECT TOP 5 CI.vcCompanyName + ' - (' + DM.vcDivisionName + ')' As CompanyName,             
     sum(OM.monPAmount) As Amount            
     FROM OpportunityMaster OM          
     JOIN DivisionMaster DM          
     ON DM.numDivisionID=OM.numDivisionID            
     JOIN CompanyInfo CI            
     ON CI.numCompanyID=DM.numCompanyID            
     JOIN AdditionalContactsInformation AI          
     ON AI.numContactID = OM.numRecOwner          
     WHERE OM.tintOppStatus=1    and OM.tintOppType=1     
      AND OM.bintAccountClosingDate BETWEEN @dtDateFrom AND @dtDateTo            
      AND OM.numDomainID=@numDomainID            
     AND DM.numRecOwner=@numUserCntID            
      AND AI.numTeam is not null               
      AND AI.numTeam in(select F.numTeam from ForReportsByTeam F               
     where F.numuserCntId=@numUserCntID and F.numDomainid=@numDomainId and F.tintType=@intType)              
     GROUP BY CI.vcCompanyName + ' - (' + DM.vcDivisionName + ')'            
     ORDER BY Amount DESC            
          
            
   END            
 END            
            
END
GO
