/****** Object:  StoredProcedure [dbo].[usp_GetTopProducers]    Script Date: 07/26/2008 16:18:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_gettopproducers')
DROP PROCEDURE usp_gettopproducers
GO
CREATE PROCEDURE [dbo].[usp_GetTopProducers]              
 @numDomainID numeric,                
 @dtDateFrom datetime,                
 @dtDateTo datetime,                
 @numUserCntID numeric=0,                           
 @intType numeric=0,              
 @tintRights TINYINT=1              
  --              
AS                
BEGIN                
 If @tintRights=1              
 --All Records Rights                
 BEGIN                
 SELECT vcFirstname+' '+vcLastName as Employee,              
 (select count(*) from divisionmaster where tintcrmtype=1 and numrecowner=numContactID ) as ProspectsOwned,              
 (select count(*) from OpportunityMaster where numrecowner=numContactID and tintOppStatus=1 and numdomainid=@numDomainID and bintAccountClosingDate BETWEEN @dtDateFrom AND @dtDateTo) AS DealsWon,              
 (select count(*) from OpportunityMaster where numrecowner=numContactID and tintOppStatus=2 and numdomainid=@numDomainID and bintAccountClosingDate BETWEEN @dtDateFrom AND @dtDateTo ) AS DealsLost,              
 dbo.fn_GetPercentageWon(numContactID,@numDomainID,@dtDateFrom,@dtDateTo)  AS PercentageWon,              
 (select count(*) from divisionmaster where tintcrmtype=2 and numrecowner=numContactID) as AccountsOwned,              
 (select count(*) from opportunitymaster where numrecowner=numContactID and numdomainid=@numDomainID             
 and intPEstimatedCloseDate BETWEEN @dtDateFrom AND @dtDateTo) as OpportunitiesOpened,              
 (select sum(monPAmount) from OpportunityMaster where numrecowner=numContactID and tintOppStatus=1 and numdomainid=@numDomainID and bintAccountClosingDate BETWEEN @dtDateFrom AND @dtDateTo) as AmountWon              
 from AdditionalContactsInformation A              
 WHERE            
 A.numDomainID=@numDomainID             
 and A.numContactID=@numUserCntID                
          
              
 END                
                
 If @tintRights=2              
 BEGIN                
              
  SELECT vcFirstname+' '+vcLastName as Employee,              
 (select count(*) from divisionmaster where tintcrmtype=1 and numrecowner=numContactID) as ProspectsOwned,              
 (select count(*) from OpportunityMaster where numrecowner=numContactID and tintOppType=1 and tintOppStatus=1 and numdomainid=@numDomainID and bintAccountClosingDate BETWEEN @dtDateFrom AND @dtDateTo) AS DealsWon,              
 (select count(*) from OpportunityMaster where numrecowner=numContactID and tintOppType=1  and tintOppStatus=2 and numdomainid=@numDomainID and bintAccountClosingDate BETWEEN @dtDateFrom AND @dtDateTo ) AS DealsLost,              
  dbo.fn_GetPercentageWon(numContactID,@numDomainID,@dtDateFrom,@dtDateTo)  AS PercentageWon,              
 (select count(*) from divisionmaster where tintcrmtype=2 and numrecowner=numContactID ) as AccountsOwned,              
 (select count(*) from opportunitymaster where numrecowner=numContactID and tintOppType=1  and numdomainid=@numDomainID             
 and intPEstimatedCloseDate BETWEEN @dtDateFrom AND @dtDateTo) as OpportunitiesOpened,              
 (select sum(monPAmount) from OpportunityMaster where numrecowner=numContactID and tintOppType=1  and tintOppStatus=1 and numdomainid=@numDomainID and bintAccountClosingDate BETWEEN @dtDateFrom AND @dtDateTo) as AmountWon              
 from AdditionalContactsInformation A              
 WHERE            
 A.numDomainID=@numDomainID             
 and A.numTeam in(select F.numTeam from ForReportsByTeam F                     
 where F.numUserCntid=@numUserCntID and F.numDomainid=@numDomainId and F.tintType=@intType)               
              
 END                
                
 If @tintRights=3              
 BEGIN                
SELECT vcFirstname+' '+vcLastName as Employee,              
 (select count(*) from divisionmaster where tintcrmtype=1 and numrecowner=numContactID ) as ProspectsOwned,              
 (select count(*) from OpportunityMaster where numrecowner=numContactID and tintOppStatus=1 and tintOppType=1  and numdomainid=@numDomainID and bintAccountClosingDate BETWEEN @dtDateFrom AND @dtDateTo) AS DealsWon,              
 (select count(*) from OpportunityMaster where numrecowner=numContactID and tintOppStatus=2 and tintOppStatus=1 and numdomainid=@numDomainID and bintAccountClosingDate BETWEEN @dtDateFrom AND @dtDateTo ) AS DealsLost,          
 dbo.fn_GetPercentageWon(numContactID,@numDomainID,@dtDateFrom,@dtDateTo)  AS PercentageWon,              
 (select count(*) from divisionmaster where tintcrmtype=2 and numrecowner=numContactID ) as AccountsOwned,              
 (select count(*) from opportunitymaster where numrecowner=numContactID and numdomainid=@numDomainID             
 and intPEstimatedCloseDate BETWEEN @dtDateFrom AND @dtDateTo and tintOppType=1 ) as OpportunitiesOpened,              
 (select sum(monPAmount) from OpportunityMaster where numrecowner=numContactID and tintOppType=1  and tintOppStatus=1 and numdomainid=@numDomainID and bintAccountClosingDate BETWEEN @dtDateFrom AND @dtDateTo) as AmountWon              
 from AdditionalContactsInformation A   
 Left Outer Join DivisionMaster DM On A.numDivisionId = DM.numDivisionID
 WHERE  A.numDomainID=@numDomainID             
 and DM.numTerId  in(select F.numTerritory from ForReportsByTerritory F      -- Added By Anoop                 
    where F.numUserCntid=@numUserCntID and F.numDomainid=@numDomainID and F.tintType=@intType)             
              
              
 END                
 END
GO
