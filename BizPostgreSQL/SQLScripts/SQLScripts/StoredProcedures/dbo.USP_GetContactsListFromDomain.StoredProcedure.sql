GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj      
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getcontactslistfromdomain')
DROP PROCEDURE usp_getcontactslistfromdomain
GO
CREATE PROCEDURE [dbo].[USP_GetContactsListFromDomain]     
	@numDomainId NUMERIC(18,0)
	,@tintGroupType TINYINT = 0
AS
BEGIN
	SELECT 
		numContactID
		,numUserID
		,ISNULL(vcFirstname,'') + ' ' + ISNULL(vcLastname,'') as [Name]
		,ISNULL(vcPassword,'') as vcPassword
		,CASE WHEN vcEmailID IS NULL THEN ISNULL(vcEmail,'') ELSE ISNULL(vcEmailID,'') END as EmailID
		,bitActivateFlag
		,ISNULL(AGM.vcGroupName,'') vcGroupName
		,(CASE WHEN tintGroupType=4 THEN 'Limited Access User' ELSE 'Full User' END) vcGroupType
	FROM 
		AdditionalContactsInformation A      
	INNER JOIN
		Domain D      
	ON 
		D.numDomainID=A.numDomainID    
	LEFT JOIN
		UserMaster U    
	ON
		U.numUserDetailId=A.numContactID       
	LEFT JOIN
		dbo.AuthenticationGroupMaster AGM 
	ON 
		AGM.numDomainID = U.numDomainID AND AGM.numGroupID = U.numGroupID
	WHERE 
		A.numDivisionID=D.numDivisionID      
		AND D.numDomainID=@numDomainID
		AND (ISNULL(@tintGroupType,0) = 0 OR tintGroupType=@tintGroupType)
END
GO
