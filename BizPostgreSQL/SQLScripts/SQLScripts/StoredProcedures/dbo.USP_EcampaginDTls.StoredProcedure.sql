/****** Object:  StoredProcedure [dbo].[USP_EcampaginDTls]    Script Date: 06/04/2009 16:21:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj  
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_ecampagindtls')
DROP PROCEDURE usp_ecampagindtls
GO
CREATE PROCEDURE [dbo].[USP_EcampaginDTls]  
@numECampaignID as numeric(9)=0,  
@byteMode as tinyint=0  
as  
  
if @byteMode=0  
begin  
select vcECampName,txtDesc,ISNULL(dtStartTime,GETDATE()) dtStartTime,ISNULL(fltTimeZone,0) fltTimeZone,ISNULL(tintTimeZoneIndex,0) tintTimeZoneIndex, ISNULL([tintFromField],1) tintFromField,ISNULL([numFromContactID],0) numFromContactID from ECampaign   
where numECampaignID=@numECampaignID  
select numECampDTLId,numEmailTemplate,numActionItemTemplate, tintDays, tintWaitPeriod, ISNULL(numFollowUpID,0) numFollowUpID from ECampaignDTLs  
where numECampID=@numECampaignID  
end  
if @byteMode=1  
begin 
	UPDATE ECampaign SET bitDeleted=1 WHERE numECampaignID=@numECampaignID
end
