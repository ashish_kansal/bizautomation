/****** Object:  StoredProcedure [dbo].[USP_InsertJournalEntryHeader]    Script Date: 07/26/2008 16:19:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created by Siva
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'usp_insertjournalentryheader' ) 
    DROP PROCEDURE usp_insertjournalentryheader
GO
CREATE PROCEDURE [dbo].[USP_InsertJournalEntryHeader]
	@numJournal_Id AS NUMERIC(9) = 0 OUTPUT,
	@numRecurringId AS NUMERIC(9) = 0,
    @datEntry_Date AS DATETIME,
    @vcDescription AS VARCHAR(1000),
    @numAmount AS DECIMAL(20,5),
    @numCheckId NUMERIC=NULL,
    @numCashCreditCardId NUMERIC= NULL,
    @numChartAcntId NUMERIC=NULL,
    @numOppId NUMERIC=NULL,
    @numOppBizDocsId NUMERIC=NULL,
    @numDepositId NUMERIC=NULL,
    @numBizDocsPaymentDetId NUMERIC=NULL,
    @bitOpeningBalance BIT=NULL,
    @numCategoryHDRID NUMERIC=NULL,
    @numReturnID NUMERIC=NULL,
    @numCheckHeaderID numeric(18, 0),
	@numBillID numeric(18, 0),
	@numBillPaymentID numeric(18, 0),
    @numDomainId AS NUMERIC(9) = 0,
    @numUserCntID AS NUMERIC(9) = 0,
    @numReconcileID AS NUMERIC(9) = NULL,
    @numJournalReferenceNo AS NUMERIC(9)=0,
    @numPayrollDetailID NUMERIC=NULL,
	@bitReconcileInterest BIT = 0
AS 
BEGIN    
	--Validation of closed financial year
	EXEC dbo.USP_ValidateFinancialYearClosingDate @numDomainID,@datEntry_Date
	
    IF @numRecurringId = 0 SET @numRecurringId = NULL  
--    IF @numProjectID = 0 SET @numProjectID = NULL
    IF @numCheckId = 0 SET @numCheckId = NULL
    IF @numCashCreditCardId = 0 SET @numCashCreditCardId = NULL
    IF @numChartAcntId = 0 SET @numChartAcntId = NULL
    IF @numOppId = 0 SET @numOppId = NULL
    IF @numOppBizDocsId = 0 SET @numOppBizDocsId = NULL
    IF @numDepositId = 0 SET @numDepositId = NULL
    IF @numBizDocsPaymentDetId = 0 SET @numBizDocsPaymentDetId = NULL
    IF @bitOpeningBalance = 0 SET @bitOpeningBalance = NULL
    IF @numCategoryHDRID = 0 SET @numCategoryHDRID = NULL
--    IF @numClassID = 0 SET @numClassID = NULL
    IF @numReturnID=0 SET @numReturnID=NULL
    IF @numReconcileID=0 SET @numReconcileID=NULL
    IF @numJournalReferenceNo=0 SET @numJournalReferenceNo=NULL
    IF @numPayrollDetailID=0 SET @numPayrollDetailID=NULL
    
DECLARE @numEntryDateSortOrder AS NUMERIC

SET @numEntryDateSortOrder = convert(numeric,
RIGHT('00000'+ CONVERT(VARCHAR,DATEPART(year, @datEntry_Date)),4)+
RIGHT('00000'+ CONVERT(VARCHAR,DATEPART(month, @datEntry_Date)),2)+
RIGHT('00000'+ CONVERT(VARCHAR,DATEPART(day, @datEntry_Date)),2)+
RIGHT('00000'+ CONVERT(VARCHAR,DATEPART(hour, @datEntry_Date)),2)+
RIGHT('00000'+ CONVERT(VARCHAR,DATEPART(minute, @datEntry_Date)),2)+
RIGHT('00000'+ CONVERT(VARCHAR,DATEPART(second, @datEntry_Date)),2)+
RIGHT('00000'+ CONVERT(VARCHAR,DATEPART(millisecond, @datEntry_Date)),3))

    IF @numJournal_Id = 0
        BEGIN
           --Set Default Class If enable User Level Class Accountng 
		  DECLARE @numAccountClass AS NUMERIC(18);SET @numAccountClass=0
		  
		  IF ISNULL(@numDepositID,0)>0
		  BEGIN
			 SELECT @numAccountClass=ISNULL(numAccountClass,0) FROM DepositMaster 
				WHERE numDomainID=@numDomainID AND numDepositID=@numDepositID
		  END
		  ELSE IF ISNULL(@numBillID,0)>0
		  BEGIN
			 SELECT @numAccountClass=ISNULL(numAccountClass,0) FROM BillHeader 
				WHERE numDomainID=@numDomainID AND numBillID=@numBillID
		  END
		  ELSE IF ISNULL(@numBillPaymentID,0)>0
		  BEGIN
			 SELECT @numAccountClass=ISNULL(numAccountClass,0) FROM BillPaymentHeader 
				WHERE numDomainID=@numDomainID AND numBillPaymentID=@numBillPaymentID
		  END
		  ELSE IF ISNULL(@numCheckHeaderID,0)>0
		  BEGIN
			 SELECT @numAccountClass=ISNULL(numAccountClass,0) FROM CheckHeader 
				WHERE numDomainID=@numDomainID AND numCheckHeaderID=@numCheckHeaderID
		  END
		  ELSE IF ISNULL(@numReturnID,0)>0
		  BEGIN
			 SELECT @numAccountClass=ISNULL(numAccountClass,0) FROM ReturnHeader 
				WHERE numDomainID=@numDomainID AND numReturnHeaderID=@numReturnID
		  END
		  ELSE IF ISNULL(@numOppId,0)>0
		  BEGIN
		     SELECT @numAccountClass=ISNULL(numAccountClass,0) FROM OpportunityMaster 
				WHERE numDomainID=@numDomainID AND numOppId=@numOppId
		  END
		  ELSE
		  BEGIN
		  	  -- GET ACCOUNT CLASS IF ENABLED
			DECLARE @tintDefaultClassType AS INT = 0
			SELECT @tintDefaultClassType = ISNULL(tintDefaultClassType,0) FROM Domain WHERE numDomainId = @numDomainID 

			IF @tintDefaultClassType = 1 --USER
			BEGIN
				SELECT 
					@numAccountClass=ISNULL(numDefaultClass,0) 
				FROM 
					dbo.UserMaster UM 
				JOIN 
					dbo.Domain D 
				ON 
					UM.numDomainID=D.numDomainId
				WHERE 
					D.numDomainId=@numDomainId 
					AND UM.numUserDetailId=@numUserCntID
			END
			ELSE
			BEGIN
				SET @numAccountClass = 0
			END
		  END
		
           INSERT INTO dbo.General_Journal_Header (
				numRecurringId,
				datEntry_Date,
				varDescription,
				numAmount,
				numDomainId,
				numCheckId,
				numCashCreditCardId,
				numChartAcntId,
				numOppId,
				numOppBizDocsId,
				numDepositId,
				numBizDocsPaymentDetId,
				bitOpeningBalance,
				dtLastRecurringDate,
				numNoTransactions,
				numCreatedBy,
				datCreatedDate,
				numCategoryHDRID,
				numModifiedBy,
				dtModifiedDate,
				numReturnID,
				[numCheckHeaderID],
				[numBillID],
				[numBillPaymentID],numReconcileID,numJournalReferenceNo,numPayrollDetailID,
				[numEntryDateSortOrder],numAccountClass,bitReconcileInterest
			) VALUES ( 
				 @numRecurringId,
				 CAST(@datEntry_Date AS SMALLDATETIME),
				 @vcDescription,
				 @numAmount,
				 @numDomainId,
				 @numCheckId,
				 @numCashCreditCardId,
				 @numChartAcntId,
				 @numOppId,
				 @numOppBizDocsId,
				 @numDepositId,
				 @numBizDocsPaymentDetId,
				 @bitOpeningBalance,
				 null,
				 null,
				 @numUserCntID,
				 GETUTCDATE(),
				 @numCategoryHDRID,
				 @numUserCntID,
				 GETUTCDATE(),
				 @numReturnID,
				 @numCheckHeaderID,
				 @numBillID,
				 @numBillPaymentID,@numReconcileID,@numJournalReferenceNo,@numPayrollDetailID,
				 @numEntryDateSortOrder,@numAccountClass,@bitReconcileInterest)
            SET @numJournal_Id = SCOPE_IDENTITY()                              
            SELECT  @numJournal_Id 
        END                        
        ELSE
        BEGIN
			UPDATE  [General_Journal_Header]
			SET     [numRecurringId] = @numRecurringId,
					[datEntry_Date] = CAST(@datEntry_Date AS SMALLDATETIME),
					[varDescription] = @vcDescription,
					[numAmount] = @numAmount,
					[numDomainId] = @numDomainId,
					[numCheckId] = @numCheckId,
					[numCashCreditCardId] = @numCashCreditCardId,
					[numOppId] = @numOppId,
					[numOppBizDocsId] = @numOppBizDocsId,
					[numDepositId] = @numDepositId,
					[numBizDocsPaymentDetId] = @numBizDocsPaymentDetId,
					[bitOpeningBalance] = @bitOpeningBalance,
					[numCategoryHDRID] = @numCategoryHDRID,
					numModifiedBy = @numUserCntID,
					dtModifiedDate =GETUTCDATE(),
					numReturnID = @numReturnID,
					[numCheckHeaderID] = @numCheckHeaderID,
					[numBillID] = @numBillID,
					[numBillPaymentID] = @numBillPaymentID,
					numReconcileID=@numReconcileID,
					numJournalReferenceNo=@numJournalReferenceNo,
					numPayrollDetailID=@numPayrollDetailID,
					bitReconcileInterest=@bitReconcileInterest,
					numEntryDateSortOrder = @numEntryDateSortOrder
			WHERE   numJournal_Id = @numJournal_Id
					AND numDomainId = @numDomainID
			SELECT  @numJournal_Id 
		END

		UPDATE 
			General_Journal_Header 
		SET 
			numEntryDateSortOrder = ISNULL((SELECT MAX(numEntryDateSortOrder) FROM General_Journal_Header WHERE numEntryDateSortOrder=@numEntryDateSortOrder),@numEntryDateSortOrder) + 1 
		WHERE
			numJournal_Id = @numJournal_Id

END
GO
