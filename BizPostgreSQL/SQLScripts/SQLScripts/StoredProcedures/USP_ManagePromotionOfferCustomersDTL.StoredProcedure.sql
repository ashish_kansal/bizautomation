GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_ManagePromotionOfferCustomersDTL' ) 
    DROP PROCEDURE USP_ManagePromotionOfferCustomersDTL
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[USP_ManagePromotionOfferCustomersDTL]    
	@numProID			AS NUMERIC(9) = 0,  
	@numProOrgId		AS NUMERIC(9)=0,    
	@tintRuleAppType	AS TINYINT,  
	@numDivisionID		AS NUMERIC(9)=0,
	@numRelationship	AS NUMERIC(9)=0,  
	@numProfile			AS NUMERIC(9)=0,  
	@byteMode			AS TINYINT,
	@numDomainID		AS NUMERIC(9)=0  
AS    
	IF @byteMode = 0  
	BEGIN  
		IF NOT EXISTS(SELECT * FROM PromotionOfferorganizations 
			WHERE numProId = @numProId  
				AND numDivisionID = @numDivisionID 
				AND numRelationship = @numRelationship 
				AND numProfile = @numProfile 
				AND tintType = @tintRuleAppType)
		BEGIN
			IF @tintRuleAppType = 1 
			BEGIN
				DELETE FROM PromotionOfferorganizations 
				WHERE tintType = 2 
					AND numProId = @numProId 
					AND numDivisionID <> @numDivisionID 
			END
			ELSE IF @tintRuleAppType = 2 
			BEGIN
				DELETE FROM PromotionOfferorganizations 
					WHERE tintType = 1 
					AND numProId = @numProId 
					AND numDivisionID <> @numDivisionID 
			END
			
			/*Check For Duplicates 
			1- The customer is specific (level 1), and the item is specific (also level 1). If the user tries to create another rule that targets the customer by name, or that same item by name, validation would say �You�ve already created a promotion rule that targets this customer and item by name�*/
			/*IF (SELECT COUNT(*) 
				FROM PromotionOfferorganizations POO
					JOIN PromotionOfferItems POI ON POI.numProId = POO.numProId
					JOIN PromotionOffer PO ON PO.numProId = POI.numProId
				WHERE ISNULL(POO.numDivisionID,0) = @numDivisionID 
					AND POI.[numValue] IN (SELECT numValue 
												FROM [PromotionOfferItems] 
												WHERE numProID = @numProID)
					AND PO.[numDomainID] = @numDomainID) > 0
			BEGIN
				RAISERROR ( 'DUPLICATE-LEVEL1',16, 1 )
				RETURN ;
			END*/
			
			/* 2- The customer is targeted at the group level (level 2), and the item is specific (level 1). If the user tries to create another rule that targets this same Relationship / Profile combination and that same item in level 1 (L2 customer and L1 item both match), validation would say �You�ve already created a promotion rule that targets this relationship / profile and one or more item specifically�*/
			/*IF (SELECT COUNT(*) 
				FROM PromotionOfferorganizations POO
					JOIN PromotionOfferItems POI ON POI.numProId = POO.numProId
					JOIN PromotionOffer PO ON PO.numProId = POI.numProId
				WHERE ISNULL(POO.numRelationship,0) = @numRelationship
					AND ISNULL(POO.numProfile,0) = @numProfile
					AND POI.[numValue] IN (SELECT numValue 
												FROM [PromotionOfferItems] 
												WHERE numProID = @numProID)
					AND PO.[numDomainID] = @numDomainID) > 0
			BEGIN
				RAISERROR ( 'DUPLICATE-LEVEL2',16, 1 )
				RETURN ;
			END*/

			INSERT INTO [PromotionOfferorganizations] 
				(numProId, numDivisionID, numRelationship, numProfile, tintType) 
			VALUES (@numProId, @numDivisionID, @numRelationship, @numProfile, @tintRuleAppType)
		END  
		ELSE
		BEGIN
			RAISERROR ( 'DUPLICATE',16, 1 )
			RETURN ;
		END
	END
	ELSE IF @byteMode = 1  
	BEGIN  
		DELETE FROM PromotionOfferorganizations 
		WHERE numProOrgId = @numProOrgId 
	END
