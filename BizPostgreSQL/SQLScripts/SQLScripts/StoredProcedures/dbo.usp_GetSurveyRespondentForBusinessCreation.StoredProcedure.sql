/****** Object:  StoredProcedure [dbo].[usp_GetSurveyRespondentForBusinessCreation]    Script Date: 07/26/2008 16:18:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created By: Debasish Nag    
--Created On: 09/13/2005    
--Purpose: Returns the Survey Respondent information   
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getsurveyrespondentforbusinesscreation')
DROP PROCEDURE usp_getsurveyrespondentforbusinesscreation
GO
CREATE PROCEDURE [dbo].[usp_GetSurveyRespondentForBusinessCreation]    
 @numSurId Numeric,  
 @numRespondantID Numeric  
AS      
--This procedure will return all survey Respondent information for the selected survey    
BEGIN      
  SELECT  vcDbColumnName, 
  Case When vcDbColumnValue = 0 Then      
   vcDbColumnValueText      
  Else      
   vcDbColumnValue    
  End      
  as vcDbColumnValue, vcDbColumnValueText
  FROM SurveyRespondentsChild  
  WHERE numRespondantID = @numRespondantID  
  AND numSurId = @numSurId    
END
GO
