/****** Object:  StoredProcedure [dbo].[usp_UpdateCompanyAndDivision]    Script Date: 07/26/2008 16:21:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_updatecompanyanddivision')
DROP PROCEDURE usp_updatecompanyanddivision
GO
CREATE PROCEDURE [dbo].[usp_UpdateCompanyAndDivision]    
 @vcCompanyName varchar(100)='',    
 @vcWebsite varchar(255)='',    
 @numNoOfEmployeeId  numeric(9)=0,    
 @numAnnualRevID numeric(9)=0,    
 @numCompanyID numeric(9)=0,    
 @vcHow varchar(100)='',    
 @vcProfile varchar(100)='',    
 @vcDivisionName varchar(10)='',    
 @numDivisionID numeric(9)=0,  
 @numCampaignID numeric(9)=0,
 @vcCity varchar(500)='',
 @vcStreet varchar(500)='',
 @vcState varchar(500)='',
 @vcCountry varchar(500)='',
 @vcPostCode varchar(500)='',
 @numModifiedBy numeric = 0,
 @bintModifiedDate numeric=0   
     --
As    
/*
set @vcCompanyName = replace (@vcCompanyName,'''','''''')
set @vcWebsite = replace (@vcWebsite,'''','''''')
set @vcHow = replace (@vcHow,'''','''''')
set @vcCompanyName = replace (@vcCompanyName,'''','''''')
set @vcProfile = replace (@vcProfile,'''','''''')
set @vcDivisionName = replace (@vcDivisionName,'''','''''')
--set @vcCompanyName = replace (@vcCompanyName,'''','''''')
*/
Declare @numLinkID numeric  
    
 UPDATE CompanyInfo SET vcCompanyName=@vcCompanyName,vcWebSite=@vcWebsite,numNoOfEmployeesId=@numNoOfEmployeeId,    
  numAnnualRevID=@numAnnualRevID,vcHow=@vcHow, vcProfile=@vcProfile, 
  numModifiedBy=CASE WHEN @numModifiedBy>0 THEN @numModifiedBy ELSE NULL END, 
  bintModifiedDate=CASE WHEN @bintModifiedDate>0 THEN @bintModifiedDate ELSE NULL END
  WHERE numCompanyID=@numCompanyID    
  
 UPDATE DivisionMaster SET vcDivisionName=@vcDivisionName,  
  numModifiedBy=CASE WHEN @numModifiedBy>0 THEN @numModifiedBy ELSE NULL END, 
  bintModifiedDate=CASE WHEN @bintModifiedDate>0 THEN @bintModifiedDate ELSE NULL END
  WHERE numDivisionID=@numDivisionID  
  
	UPDATE dbo.AddressDetails 
	SET vcStreet = @vcStreet,
			vcCity = @vcCity,
			numCountry = @vcCountry,
			vcPostalCode = @vcPostCode,
			numState = @vcState
	 WHERE  numRecordID = @numDivisionID AND tintAddressOf=2 AND tintAddressType=1 AND bitIsPrimary=1
  
  
 Select @numLinkID = numLinkId from CampaignDivision where numDivisionID=@numDivisionID  
 If @numLinkId > 0  
 Begin  
 Update CampaignDivision Set numCampaignID = @numCampaignID where numDivisionID = @numDivisionID  
 End  
 Else  
 Begin  
 Insert into CampaignDivision (numDivisionID, numCampaignID) values (@numDivisionID, @numCampaignID)  
 End
GO
