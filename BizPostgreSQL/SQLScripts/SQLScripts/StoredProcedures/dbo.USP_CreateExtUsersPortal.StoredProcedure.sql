GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_createextusersportal')
DROP PROCEDURE usp_createextusersportal
GO
CREATE PROCEDURE [dbo].[USP_CreateExtUsersPortal]
(
	@numCompanyID as numeric(9)=0,
	@numDivisionID as numeric(9)=0,
	@numContactID as numeric(9)=0,
	@vcPassword as varchar(100)='',
	@numDomainID AS NUMERIC,
	@vcEmail Varchar (100)='' output
)
AS
BEGIN
	DECLARE @intNoofBusinessPortalUsers INT

	SELECT 
		@intNoofBusinessPortalUsers=ISNULL(intNoofBusinessPortalUsers,0) 
	FROM 
		Subscribers 
	WHERE 
		numTargetDomainID = @numDomainID

	DECLARE @numBusinessPortalUsers INT
	SET @numBusinessPortalUsers = ISNULL((SELECT COUNT(numExtranetDtlID) FROM ExtranetAccountsDtl WHERE numDOmainID=@numDOmainID AND ISNULL(bitPartnerAccess,0)=1),0)

	IF (@numBusinessPortalUsers + 1) > @intNoofBusinessPortalUsers
	BEGIN
		RAISERROR('BUSINESS_PORTAL_USERS_EXCEED',16,1)
		RETURN
	END

	DECLARE @Identity AS NUMERIC(18,0)

	IF ISNULL(@numCompanyID,0) = 0
	BEGIN
		SELECT @numCompanyID = numCompanyID FROM dbo.DivisionMaster WHERE numDivisionID = @numDivisionID 
	END
  
	IF NOT EXISTS(SELECT * FROM ExtarnetAccounts WHERE numCompanyID=@numCompanyID and numDivisionID=@numDivisionID)
	BEGIN
		DECLARE @numGroupID NUMERIC
		
		SELECT TOP 1 @numGroupID=numGroupID FROM dbo.AuthenticationGroupMaster WHERE numDomainID=@numDomainID AND tintGroupType=2
		
		IF @numGroupID IS NULL SET @numGroupID = 0 

		INSERT INTO ExtarnetAccounts
		(numCompanyID,numDivisionID,numGroupID,numDomainID)
		VALUES
		(@numCompanyID,@numDivisionID,@numGroupID,@numDomainID)

		SET @Identity=@@Identity
	END
	ELSE 
	BEGIN
		SELECT @identity = numExtranetID FROM dbo.ExtarnetAccounts WHERE numCompanyID =@numCompanyID AND numDivisionID=@numDivisionID AND numDomainID = @numDomainID
	END
	

	IF NOT EXISTS(select * from ExtranetAccountsDtl where numContactID=@numContactID)
	BEGIN
		INSERT INTO ExtranetAccountsDtl
		(numExtranetID,numContactID,vcPassword,bitPartnerAccess,numDomainID)
		VALUES
		(@Identity,@numContactID,@vcPassword,1,@numDomainID)
	END

	SELECT @vcEmail=isnull(vcEmail,'') FROM AdditionalContactsInformation WHERE numContactID=@numContactID
END
GO
