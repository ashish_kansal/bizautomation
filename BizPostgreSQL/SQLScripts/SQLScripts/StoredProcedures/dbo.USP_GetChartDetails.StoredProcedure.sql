/****** Object:  StoredProcedure [dbo].[USP_GetChartDetails]    Script Date: 07/26/2008 16:16:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created by Siva   
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'usp_getchartdetails' ) 
    DROP PROCEDURE usp_getchartdetails
GO
CREATE PROCEDURE [dbo].[USP_GetChartDetails]
    @numAcntId AS NUMERIC(9) = 0,
    @numDomainId AS NUMERIC(9) = 0
AS 
    BEGIN    
        SELECT  CCC.[numAccountId],
                CCC.[numParntAcntTypeId],
                ISNULL(CCC.[numOriginalOpeningBal], 0) numOriginalOpeningBal,
                ISNULL(CCC.[numOpeningBal], 0) numOpeningBal,
                CCC.[dtOpeningDate],
                CCC.[bitActive],
                CCC.[bitFixed],
                CCC.[numDomainId],
                CCC.[numListItemID],
                ISNULL(CCC.[bitDepreciation], 0) bitDepreciation,
                CCC.[monDepreciationCost],
                CCC.[dtDepreciationCostDate],
                CCC.[bitOpeningBalanceEquity],
                CCC.[monEndingOpeningBal],
                CCC.[monEndingBal],
                CCC.[dtEndStatementDate],
                CCC.[chBizDocItems],
                CCC.[numAcntTypeId],
                CCC.[vcAccountCode],
                CASE WHEN ISNULL(CCC.vcNumber, '') <> ''
                     THEN ISNULL(REPLACE(ISNULL(CCC.vcAccountName, ''),
                                         ISNULL(CCC.vcNumber, '') + ' ', ''),
                                 '')
                     ELSE ISNULL(CCC.vcAccountName, '')
                END AS [vcAccountName],
                CCC.[vcAccountDescription],
                CCC.[bitProfitLoss],
                dbo.[GetAccountTypeCode](CCC.numDomainID, CCC.numAcntTypeId, 2) AS [vcAccountTypeCode],
                ( dbo.[GetAccountTypeCode](CCC.numDomainID, CCC.numAcntTypeId,
                                           2) + '~'
                  + CONVERT(VARCHAR(10), CCC.numAcntTypeId) ) AS numAcntTypeId1,
                ISNULL(CCC.vcNumber, '') AS [vcNumber],
                ISNULL(CCC.numParentAccId, 0) AS [numParentAccId],
                ISNULL(CCC.bitIsSubAccount, 0) AS [bitIsSubAccount],
                ( SELECT    COUNT(*)
                  FROM      dbo.Chart_Of_Accounts COA
                  WHERE     COA.numAccountId = C.numParentAccId
                ) AS [ChildCount],
                ATD.[vcAccountCode] + '~'
                + CAST(ATD.[numAccountTypeID] AS VARCHAR(20)) AS CandidateKey,
                ISNULL(CCC.numBankDetailID, 0) AS [numBankDetailID],
                ISNULL(CCC.IsBankAccount, 0) AS [IsBankAccount],
                ISNULL(CCC.vcStartingCheckNumber, '') AS [vcStartingCheckNumber],
                ISNULL(CCC.vcBankRountingNumber, '') AS [vcBankRountingNumber],
                ISNULL(CCC.IsConnected, 0) AS [IsConnected],
                BM.vcFIName AS [vcBankName],
                CCC.vcNumber AS [vcBankAccountNumber],
--              CASE WHEN C.[numParntAcntTypeId] IS NULL THEN NULL
--							 ELSE CCC.numOpeningBal
--				END AS [monCompanyBalance],
                CCC.numOpeningBal AS [monCompanyBalance],
                CASE WHEN ( ISNULL(CCC.IsBankAccount, 0) = 0
                          ) THEN 0
                     ELSE ( SELECT DISTINCT
                                    ISNULL(( SELECT TOP 1
                                                    monAvailableBalance
                                             FROM   BankStatementHeader A
                                             WHERE  A.numBankDetailID = BD.numBankDetailID
                                                    AND dtAvailableBalanceDate = ( SELECT TOP 1
                                                                                            dtLedgerBalanceDate
                                                                                   FROM     BankStatementHeader
                                                                                   WHERE    numBankDetailID = BD.numBankDetailID
                                                                                   ORDER BY dtCreatedDate DESC
                                                                                 )
                                             ORDER BY dtCreatedDate DESC
                                           ), 0) AS monAvailableBalance
                            FROM    dbo.BankDetails BD
                                    LEFT JOIN dbo.BankStatementHeader BSH ON BD.numBankDetailID = BSH.numBankDetailID
                            WHERE   BD.numBankDetailID = CCC.numBankDetailID
                          )
                END AS [monBankBalance]
        FROM    Chart_Of_Accounts CCC
                LEFT JOIN dbo.Chart_Of_Accounts C ON CCC.numAccountId = C.numParentAccId
                LEFT JOIN dbo.AccountTypeDetail ATD ON CCC.numAcntTypeId = ATD.numAccountTypeID
                LEFT JOIN BankDetails BD ON BD.numBankDetailID = CCC.numBankDetailID
                LEFT JOIN dbo.BankMaster BM ON BM.numBankMasterID = BD.numBankMasterID
        WHERE   CCC.numAccountId = @numAcntId
                AND CCC.numDomainId = @numDomainId   
    END
GO
