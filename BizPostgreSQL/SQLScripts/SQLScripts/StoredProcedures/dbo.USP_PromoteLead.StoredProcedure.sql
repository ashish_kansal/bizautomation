/****** Object:  StoredProcedure [dbo].[USP_PromoteLead]    Script Date: 07/26/2008 16:20:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--modified by anoop jayaraj    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_promotelead')
DROP PROCEDURE usp_promotelead
GO
CREATE PROCEDURE [dbo].[USP_PromoteLead]          
 @numDivisionID numeric,          
 @numContactID numeric,          
 @numUserCntID numeric        
         
--          
AS          
         
BEGIN          
	DECLARE @numDomainID NUMERIC(18,0)
	DECLARE @tintCurrentCRMType TINYINT
	
	SELECT @numDomainID=numDomainID,@tintCurrentCRMType=tintCRMType FROM DivisionMaster WHERE numDivisionID=@numDivisionID     
	
	INSERT INTO DivisionMasterPromotionHistory
	(
		numDomainID
		,numDivisionID
		,tintPreviousCRMType
		,tintNewCRMType
		,dtPromotedBy
		,dtPromotionDate
	)
	VALUES
	(
		@numDomainID
		,@numDivisionID
		,@tintCurrentCRMType
		,1
		,@numUserCntID
		,GETUTCDATE()
	)


   UPDATE DivisionMaster           
   SET tintCRMType=1,          
   numModifiedBy=@numUserCntID,          
   bintModifiedDate=getutcdate(),          
   bintLeadProm = getutcdate(),          
   bintLeadPromBy = @numUserCntID          
   WHERE numDivisionID=@numDivisionID          
    declare @numCompanyID as numeric(9)        
   select @numCompanyID=numCompanyID from DivisionMaster where numDivisionID=@numDivisionID        
   UPDATE AdditionalContactsInformation       
   set numModifiedBy=@numUserCntID,          
   bintModifiedDate=getutcdate()          
   WHERE numContactId=@numContactID       
   
  
   	DECLARE @numGroupID NUMERIC       
	SELECT TOP 1 @numGroupID=numGroupID FROM dbo.AuthenticationGroupMaster WHERE numDomainID=@numDomainID AND tintGroupType=2
	IF @numGroupID IS NULL SET @numGroupID = 0    
	  
   insert into ExtarnetAccounts(numCompanyID,numDivisionID,numGroupID,numDomainID)         
   values(@numCompanyID,@numDivisionID,@numGroupID,@numDomainID)        
      
END
GO
