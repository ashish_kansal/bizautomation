/****** Object:  StoredProcedure [dbo].[USP_ManageECampaign]    Script Date: 06/04/2009 16:20:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj  
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_manageecampaign')
DROP PROCEDURE usp_manageecampaign
GO
CREATE PROCEDURE [dbo].[USP_ManageECampaign]  
@numECampaignID numeric(9)=0,  
@vcCampaignName as varchar(100)='',  
@txtDesc text='',  
@strECampaignDTLs as text,
@numDomainID as numeric(9)=0 ,
@dtStartTime AS DATETIME,
@fltTimeZone AS FLOAT,
@tintTimeZoneIndex TINYINT,
@tintFromField TINYINT,
@numFromContactID NUMERIC,
@numUserContactID NUMERIC(9)
as  
DECLARE @hDoc int    
if @numECampaignID=0   
  
begin  
insert into ECampaign(vcECampName,txtDesc,numDomainID,dtStartTime,[tintFromField],[numFromContactID],fltTimeZone,tintTimeZoneIndex,numCreatedBy)  
values(@vcCampaignName,@txtDesc,@numDomainID,@dtStartTime,@tintFromField,@numFromContactID,@fltTimeZone,@tintTimeZoneIndex,@numUserContactID)  
set @numECampaignID=@@identity    
  
      
 EXEC sp_xml_preparedocument @hDoc OUTPUT, @strECampaignDTLs        
        
 insert into ECampaignDTLs        
  (numECampID,numEmailTemplate,numActionItemTemplate,tintDays,tintWaitPeriod,[numFollowUpID])        --,[numEmailGroupID],[dtStartDate]  ,@numEmailGroupID,@dtStartDate
         
 select @numECampaignID,X.numEmailTemplate,X.numActionItemTemplate,x.tintDays,X.tintWaitPeriod,X.numFollowUpID from(        
 SELECT *FROM OPENXML (@hDoc,'/NewDataSet/Table',2)        
 WITH  (        
  numEmailTemplate numeric(9),
  numActionItemTemplate NUMERIC(9),      
  tintDays TINYINT,
  tintWaitPeriod TINYINT,
  [numFollowUpID] numeric
  ))X        
        
        
 EXEC sp_xml_removedocument @hDoc  
  
end  
else  
begin  
  
update ECampaign set vcECampName=@vcCampaignName,  
txtDesc=@txtDesc ,
dtStartTime=@dtStartTime,
fltTimeZone = @fltTimeZone,
tintTimeZoneIndex=@tintTimeZoneIndex,
[tintFromField] = @tintFromField,
[numFromContactID]=@numFromContactID
where numECampaignID= @numECampaignID  
  
 EXEC sp_xml_preparedocument @hDoc OUTPUT, @strECampaignDTLs        
          
    update ECampaignDTLs set         
     numEmailTemplate=X.numEmailTemplate,        
     numActionItemTemplate=X.numActionItemTemplate,
     tintDays=X.tintDays,
	 tintWaitPeriod = X.tintWaitPeriod,
     [numFollowUpID] = X.numFollowUpID 
      From (SELECT *        
     FROM OPENXML(@hDoc,'/NewDataSet/Table1',2)         
      with(  
      numECampDTLId numeric(9),  
      numEmailTemplate numeric(9),
      numActionItemTemplate numeric(9),
      tintDays TINYINT,
	  tintWaitPeriod TINYINT,
      numFollowUpID NUMERIC ))X         
      where  ECampaignDTLs.numECampDTLId=X.numECampDTLId    
  
 EXEC sp_xml_removedocument @hDoc  
  
end  
  
select @numECampaignID
