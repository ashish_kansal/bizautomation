/****** Object:  StoredProcedure [dbo].[USP_GetFirstPage]    Script Date: 07/26/2008 16:17:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--create by anoop jayaraj    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getfirstpage')
DROP PROCEDURE usp_getfirstpage
GO
CREATE PROCEDURE [dbo].[USP_GetFirstPage]    
@numDomainID as numeric(9)=0,    
@numGroupID as numeric(9)=0,
@numRelationShip as numeric(9)=0,
@numProfileId AS NUMERIC(9)=0 
as    
    
select top 1  vcURL from TabMaster T          
 join GroupTabDetails G           
 on G.numTabId=T.numTabId          
 where (numDomainID=@numDomainID or bitFixed=1) and numGroupID=@numGroupID and ISNULL(numRelationShip,0)=@numRelationShip AND ISNULL(numProfileID,0) = @numProfileId 
	AND ISNULL(G.[tintType],0) <> 1
order by numOrder
GO
