/****** Object:  StoredProcedure [dbo].[USP_ConEmpListBasedonTeam]    Script Date: 07/26/2008 16:15:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- Created By Anoop Jayaraj          
-- exec USP_ConEmpListBasedonTeam @numDomainID=72,@numUserCntID=17
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_conemplistbasedonteam')
DROP PROCEDURE usp_conemplistbasedonteam
GO
CREATE PROCEDURE [dbo].[USP_ConEmpListBasedonTeam]
    @numDomainID AS NUMERIC(9) = 0,
    @numUserCntID AS NUMERIC(9) = 0
AS 
	
    SELECT  DISTINCT A.numContactID,
            A.vcFirstName + ' ' + A.vcLastName AS vcUserName
            --,[numTeam]
    FROM    UserMaster UM
            JOIN AdditionalContactsInformation A ON UM.numUserDetailId = A.numContactID
            LEFT OUTER JOIN [UserTeams] UT ON UT.[numUserCntID] = UM.[numUserDetailId]
    WHERE   UM.numDomainID = @numDomainID AND UM.intAssociate=1
            --AND [bitActivateFlag] = 1
            AND UT.[numTeam] IN ( SELECT    numTeam
                                  FROM      UserTeams
                                  WHERE     numUserCntID = @numUserCntID
                                            AND numDomainID = @numDomainID )
            
--    SELECT  A.numContactID,
--            A.vcFirstName + ' ' + A.vcLastName AS vcUserName
--    FROM    UserMaster UM
--            JOIN AdditionalContactsInformation A ON UM.numUserDetailId = A.numContactID
--    WHERE   D.numDomainID = @numDomainID
--            AND D.numDivisionID = A.numDivisionID
--            AND [bitActivateFlag] = 1
--            AND A.numTeam IN ( SELECT   numTeam
--                               FROM     UserTeams
--                               WHERE    numUserCntID = @numUserCntID
--                                        AND numDomainID = @numDomainID )
GO
