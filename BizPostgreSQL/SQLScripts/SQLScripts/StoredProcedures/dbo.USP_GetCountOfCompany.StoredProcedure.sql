/****** Object:  StoredProcedure [dbo].[USP_GetCountOfCompany]    Script Date: 07/26/2008 16:17:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created By Anoop Jayaraj
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getcountofcompany')
DROP PROCEDURE usp_getcountofcompany
GO
CREATE PROCEDURE [dbo].[USP_GetCountOfCompany]
@numAlerDTLId as numeric(9)=0,
@numContactID as numeric(9)=0
as
select count(*) from AlertContactsAndCompany where numAlerDTLId=@numAlerDTLId and numContactID=@numContactID
GO
