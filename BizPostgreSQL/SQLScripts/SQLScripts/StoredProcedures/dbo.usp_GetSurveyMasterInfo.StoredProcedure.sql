/****** Object:  StoredProcedure [dbo].[usp_GetSurveyMasterInfo]    Script Date: 07/26/2008 16:18:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created By: Debasish Nag    
--Created On: 09/13/2005    
--Purpose: Returns the Survey Master information    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getsurveymasterinfo')
DROP PROCEDURE usp_getsurveymasterinfo
GO
CREATE PROCEDURE [dbo].[usp_GetSurveyMasterInfo]    
 @numDomainID Numeric,       
 @numSurId Numeric    
AS      
--This procedure will return all survey master information for the selected survey    
BEGIN      
 SELECT numSurID, vcSurName, vcRedirectURL,    
  CONVERT(char(10), bintCreatedOn, 101) as dtCreatedOn, bitSkipRegistration, bitRandomAnswers,    
  bitSinglePageSurvey, bitPostRegistration  ,bitRelationshipProfile,bitSurveyRedirect,bitEmailTemplateContact,bitEmailTemplateRecordOwner
      ,numRelationShipId,numEmailTemplate1Id,numEmailTemplate2Id,bitLandingPage,vcLandingPageYes,vcLandingPageNo ,vcSurveyRedirect,
      tIntCRMType,numGrpId,numRecOwner,bitCreateRecord,numProfileId
  FROM SurveyMaster      
  WHERE numDomainID = @numDomainID      
  AND numSurId = @numSurId    
END
GO
