
GO
/****** Object:  StoredProcedure [dbo].[USP_RecurringTransactionReport]    Script Date: 03/25/2009 15:15:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Pratik Vasani
-- Create date: 21/DEC/2008
-- Description:	This procedure creates the recurring transaction report
-- =============================================
-- EXEC [dbo].[USP_RecurringTransactionReport] 1,1,20,0
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_recurringtransactionreport')
DROP PROCEDURE usp_recurringtransactionreport
GO
CREATE PROCEDURE [dbo].[USP_RecurringTransactionReport] 
	-- Add the parameters for the stored procedure here
	@numDomainID numeric(9,0),
	@CurrentPage int,                                                              
	@PageSize int,                                                              
	@TotRecs int output      

AS
BEGIN
Create table #tempTable  (ID INT IDENTITY PRIMARY KEY ,                                                            
 numRecTranRepId numeric(9)  )  


insert into #tempTable 
select numRecTranRepId from RecurringTransactionReport RTR INNER JOIN OpportunityMaster OM ON OM.numOppId = ISNULL(RTR.numRecTranOppID,RTR.numRecTranSeedId)
WHERE OM.numDomainId =@numDomainID
UNION 
SELECT numOppRecID FROM opportunityRecurring WHERE tintRecurringType =2 AND numDomainID=@numDomainID


declare @firstRec as integer                                                              
  declare @lastRec as integer                                                              
 set @firstRec= (@CurrentPage-1) * @PageSize                                                             
     set @lastRec= (@CurrentPage*@PageSize+1)                                                              
                                                             
set @TotRecs=(select count(*) from #tempTable)

print @firstRec
print @lastRec

SELECT X.* FROM 
(SELECT OPR.numOppRecID numRecTranRepId,
       OPR.numOppBizDocId numRecTranSeedId,
       OPR.numOppID AS numRecTranOppID,
       OBD.numOppBizDocsId,
	   OPR.tintRecurringType tintRecType,
       'Recurring Biz Doc' TranType,
        RT.varRecurringTemplateName,
        OBD.monDealAmount AS TotalCostAmount,
       dbo.FormatedDateFromDate(OPR.dtRecurringDate,OM.[numDomainId]) dteOppCreationDate,
       OBD.vcBizDocID AS RecurringName,
       OPR.numRecurringId,
       0 Frequency,
       (SELECT TOP 1 dtRecurringDate FROM dbo.OpportunityRecurring WHERE numOppID=OM.numOppID ORDER BY numOppRecID asc) AS StartDate,
       (SELECT TOP 1 dtRecurringDate FROM dbo.OpportunityRecurring WHERE numOppID=OM.numOppID ORDER BY numOppRecID asc) EndDate,
       (SELECT COUNT(*) dtRecurringDate FROM dbo.OpportunityRecurring WHERE numOppID=OM.numOppID AND numOppBizDocID>0 ) AS numNoTransactions,
       (SELECT COUNT(*) dtRecurringDate FROM dbo.OpportunityRecurring WHERE numOppID=OM.numOppID ) AS ExpectedChildRecords
FROM   OpportunityRecurring AS OPR
       INNER JOIN OpportunityMaster AS OM
         ON OPR.numOppId = OM.numOppId
       INNER JOIN RecurringTemplate AS RT
         ON OPR.numRecurringId = RT.numRecurringId
       INNER JOIN OpportunityBizDocs AS OBD
         ON OM.numOppId = OBD.numOppId
         AND OBD.numOppBizDocsId = OPR.numOppBizDocID
WHERE OPR.numDomainID =@numDomainID
AND OPR.tintRecurringType=2

UNION

SELECT   RTR.numRecTranRepId,
         RTR.numRecTranSeedId,
         RTR.numRecTranOppID,
         0 AS numOppBizDocsId,
         RTR.tintRecType,
         (CASE 
            WHEN RTR.tintRecType = 1 THEN 'Recurring Sales Order'
            WHEN RTR.tintRecType = 2 THEN 'Recurring Biz Doc'
            ELSE 'Unknown'
          END) AS TranType,
         RecurringTemplate.varRecurringTemplateName,
         dbo.getDealAmount(o1.numOppid,getutcdate(),0) AS TotalCostAmount,
         dbo.FormatedDateFromDate(RTR.dteOppCreationDate,o1.[numDomainId]) dteOppCreationDate,
         o1.vcPOppName AS RecurringName,
         OPR.numRecurringId,
         0 Frequency,
         GETDATE() AS StartDate,
         GETDATE() AS  EndDate,
         OPR.numNoTransactions,
         0 ExpectedChildRecords
FROM     RecurringTransactionReport RTR
         INNER JOIN OpportunityMaster AS o1
           ON RTR.numRecTranOppID = o1.numOppId
         INNER JOIN OpportunityMaster AS o2
           ON RTR.numRecTranSeedId = o2.numOppId
         LEFT OUTER JOIN [OpportunityRecurring] OPR
           ON OPR.numOppId = o2.numOppId
         INNER JOIN RecurringTemplate
           ON OPR.numRecurringId = RecurringTemplate.numRecurringId
              AND o1.numDomainId = @numDomainID
              AND o2.numdomainId = @numDomainID
WHERE o1.[numDomainId] =@numDomainID) AS X
--			LEFT OUTER JOIN #tempTable
--				ON X.numRecTranRepId = #tempTable.numRecTranRepId
--					AND ID > @firstRec
--					AND ID < @lastRec
ORDER BY X.dteOppCreationDate DESC

END
drop table  #tempTable
select  @TotRecs as TotRecs 
