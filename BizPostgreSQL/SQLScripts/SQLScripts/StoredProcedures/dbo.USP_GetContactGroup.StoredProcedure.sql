/****** Object:  StoredProcedure [dbo].[USP_GetContactGroup]    Script Date: 07/26/2008 16:16:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj            
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getcontactgroup')
DROP PROCEDURE usp_getcontactgroup
GO
CREATE PROCEDURE [dbo].[USP_GetContactGroup]  
  
@ContactId as varchar(100)                                                                                         
    
AS                                                                
  
Select ADC.numContactId ,ADC.vcFirstName,ADC.vcLastName,Adc.vcEmail,C.vcCompanyName                                            
  FROM AdditionalContactsInformation ADC                                 
  JOIN DivisionMaster DM ON ADC.numDivisionId = DM.numDivisionID                                
  JOIN CompanyInfo C ON DM.numCompanyID = C.numCompanyId            
where Adc.numcontactId in ( select * from dbo.split(@ContactId,',')  )
GO
