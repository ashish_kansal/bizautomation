/****** Object:  StoredProcedure [dbo].[usp_DeleteContract]    Script Date: 07/26/2008 16:15:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_deletecontract')
DROP PROCEDURE usp_deletecontract
GO
CREATE PROCEDURE [dbo].[usp_DeleteContract]  
@numContractId as bigint  ,
@numDomainID as numeric(9)
as   
DELETE [ContractsContact] WHERE [numContactId]=@numContractId AND [numDomainId]=@numDomainID
delete contractManagement where numContractId = @numContractId and numDomainID=@numDomainID
GO
