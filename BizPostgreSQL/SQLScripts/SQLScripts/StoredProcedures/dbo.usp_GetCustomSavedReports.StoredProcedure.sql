/****** Object:  StoredProcedure [dbo].[usp_GetCustomSavedReports]    Script Date: 07/26/2008 16:17:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getcustomsavedreports')
DROP PROCEDURE usp_getcustomsavedreports
GO
CREATE PROCEDURE [dbo].[usp_GetCustomSavedReports]
	@numCustomReportID numeric(9)=0   
--
AS
select * from customreport where numCustomReportID=@numCustomReportID
GO
