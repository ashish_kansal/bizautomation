/****** Object:  StoredProcedure [dbo].[usp_ManageWarehouseDetails]    Script Date: 07/26/2008 16:20:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_managewarehousedetails')
DROP PROCEDURE usp_managewarehousedetails
GO
CREATE PROCEDURE [dbo].[usp_ManageWarehouseDetails]    
    
 @numWareHouseDetailID as numeric(18, 0) ,    
 @numCountryID as numeric(18, 0) ,    
 @numStateID numeric(18, 0),    
 @numWareHouse numeric(18, 2),    
 @numDomainId numeric(18, 0) ,    
 @mode  as bit  ,  
 @numFromPin as integer,  
 @numToPin as integer  
as 
if @mode = 0
	begin    
		if @numFromPin = 0  
			set @numFromPin = null	
		if @numToPin = 0  
			set @numToPin = null  

		declare @Check as integer
		set @check = 0  
		
		if @numFromPin is null
			begin
				select @Check= count(*) from wareHouseDetails where numcountryid = @numCountryID and numStateID=@numStateID   
				and numdomainId=@numDomainId and numFromPinCode is null and numToPinCode is null
			end
		else
			begin			
				select @Check= count(*) from wareHouseDetails where numcountryid = @numCountryID and numStateID=@numStateID   
				and numdomainId=@numDomainId and numFromPinCode = @numFromPin and numToPinCode = @numToPin
			end
end

if @mode = 0       
begin  
	 if @Check =0    
	 begin    
		  INSERT INTO WareHouseDetails    
			   ([numCountryID]    
			   ,[numStateID]    
			   ,[numWareHouse]    
			   ,[numDomainId]
			   ,numFromPinCode
			   ,numToPinCode)    
		  VALUES    
			   (@numCountryID    
			   ,@numStateID    
			   ,@numWareHouse    
			   ,@numDomainId
			   ,@numFromPin
			   ,@numToPin)    
		  select SCOPE_IDENTITY();      
	 end    
	 else    
	  select 0    
end
else    
 DELETE FROM [WareHouseDetails]    
      WHERE numWareHouseDetailID = @numWareHouseDetailID and numDomainid=@numDomainid
GO
