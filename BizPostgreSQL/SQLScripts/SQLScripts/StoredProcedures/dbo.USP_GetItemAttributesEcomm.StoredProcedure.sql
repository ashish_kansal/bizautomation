/****** Object:  StoredProcedure [dbo].[USP_GetItemAttributesEcomm]    Script Date: 07/26/2008 16:17:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj  
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getitemattributesecomm')
DROP PROCEDURE usp_getitemattributesecomm
GO
CREATE PROCEDURE [dbo].[USP_GetItemAttributesEcomm]        
@numItemCode as numeric(9)=0,  
@numWarehouseID as numeric(9)=0       
AS  
BEGIN  
	DECLARE @bitMatrix AS BIT  
	DECLARE @numItemGroup AS NUMERIC(18,0)

	SELECT 
		@bitMatrix=ISNULL(bitMatrix,0)
		,@numItemGroup=ISNULL(numItemGroup,0)
	FROM 
		Item 
	WHERE 
		numItemcode= @numItemCode  

	IF @bitMatrix = 1 AND @numItemGroup > 0
	BEGIN
		SELECT DISTINCT
			CFW_Fld_Master.fld_id
			,Fld_label
			,fld_type
			,CFW_Fld_Master.numlistid
			,vcURL
			,ISNULL(ItemAttributes.FLD_Value,0) AS Fld_Value
		FROM
			ItemGroupsDTL
		INNER JOIN
			CFW_Fld_Master
		ON
			ItemGroupsDTL.numOppAccAttrID=CFW_Fld_Master.Fld_ID
		INNER JOIN
			ItemAttributes
		ON
			ItemAttributes.numItemCode = @numItemCode
			AND ItemAttributes.FLD_ID = CFW_Fld_Master.Fld_ID
		WHERE
			ItemGroupsDTL.numItemGroupID = @numItemGroup
			AND tintType=2
		ORDER BY
			CFW_Fld_Master.Fld_id
	END
	ELSE
	BEGIN
		 SELECT DISTINCT
			CFW_Fld_Master.fld_id
			,Fld_label
			,fld_type
			,CFW_Fld_Master.numlistid
			,vcURL
			,0 AS Fld_Value
		FROM 
			ItemGroupsDTL                   
		JOIN 
			CFW_Fld_Master 
		ON 
			numOppAccAttrID=Fld_ID              
		INNER JOIN 
			Item 
		ON 
			numItemGroup = ItemGroupsDTL.numItemGroupID 
			AND tintType=2  
		LEFT JOIN 
			WareHouseItems WI 
		ON 
			WI.numItemID= numItemcode  
		LEFT JOIN 
			CFW_Fld_Values_Serialized_Items 
		ON 
			RecId=WI.numWareHouseItemID    
		WHERE 
			numItemcode=@numItemCode 
			AND (numWarehouseID=@numWarehouseID OR charItemType <> 'P')
		ORDER BY
			CFW_Fld_Master.Fld_id
	END
END
GO
