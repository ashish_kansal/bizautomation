/****** Object:  StoredProcedure [dbo].[USP_GetPageIps]    Script Date: 07/26/2008 16:18:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getpageips')
DROP PROCEDURE usp_getpageips
GO
CREATE PROCEDURE [dbo].[USP_GetPageIps]  
@From datetime,                  
@To datetime,                  
@searchTerm varchar(500)          ,  
@numDomainID as numeric(9),
@ClientOffsetTime as integer    
  
as   
  
select vcUserHostAddress,  
case when vcUserDomain is null then vcUserHostAddress  
  when vcUserDomain ='-' then vcUserHostAddress  
  else vcUserDomain end as vcUserDomain  
from TrackingVisitorsDTL   
join  TrackingVisitorsHDR hdr                  
on numTrackingID=numTracVisitorsHDRID    
where vcpagename  
 like @searchTerm and numDomainID = @numDomainID and (hdr.dtCreated between dateadd(minute,@ClientOffsetTime,@From) and dateadd(minute,@ClientOffsetTime,@To))
GO
