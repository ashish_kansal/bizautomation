/****** Object:  StoredProcedure [dbo].[usp_GetMySales]    Script Date: 07/26/2008 16:17:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj      
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getmysales')
DROP PROCEDURE usp_getmysales
GO
CREATE PROCEDURE [dbo].[usp_GetMySales]          
 @numDomainID numeric,          
 @dtDateFrom datetime,          
 @dtDateTo datetime,          
 @numUserCntID numeric,          
 @intType numeric          
--          
AS          
BEGIN          
 IF @intType = 0          
 BEGIN          
  select top 5 A.vcFirstName+' '+A.vcLastName as [Name],sum(monPAmount) as Amount  from OpportunityMaster OM     
join AdditionalContactsInformation A on A.numContactId=OM.numRecOwner     
where OM.tintOppStatus=1        
    AND OM.numDomainID=@numDomainID          
    AND OM.bintAccountClosingDate BETWEEN @dtDateFrom AND @dtDateTo          
    AND OM.numRecOwner=@numUserCntID     
    group by A.vcFirstName,A.vcLastName order by Amount desc         
 END          
 ELSE          
 BEGIN      
    
select top 5 A.vcFirstName+' '+A.vcLastName as [Name],sum(monPAmount) as Amount  from OpportunityMaster OM      
join AdditionalContactsInformation A on A.numContactId=OM.numRecOwner     
where OM.tintOppStatus=1        
    AND OM.numDomainID=@numDomainID          
    AND OM.bintAccountClosingDate BETWEEN @dtDateFrom AND @dtDateTo             
    AND A.numTeam is not null             
    AND A.numTeam in(select F.numTeam from ForReportsByTeam F             
    where F.numuserCntId=@numUserCntID and F.numDomainid=@numDomainId and F.tintType=@intType)      
    group by A.vcFirstName,A.vcLastName order by Amount desc    
 END           
END
GO
