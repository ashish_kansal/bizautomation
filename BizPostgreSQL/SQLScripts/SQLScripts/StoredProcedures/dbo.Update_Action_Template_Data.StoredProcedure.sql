/****** Object:  StoredProcedure [dbo].[Update_Action_Template_Data]    Script Date: 07/26/2008 16:14:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='update_action_template_data')
DROP PROCEDURE update_action_template_data
GO
CREATE PROCEDURE [dbo].[Update_Action_Template_Data]    
(    
  
@templateName nvarchar(100) ,    
@dueDays Int ,    
@status Int ,    
@type nvarchar(25) ,     
@activity Int ,    
@comments nvarchar(2000),  
@rowID Int ,    
@bitSendEmailTemplate as bit,    
@numEmailTemplate as numeric(9),    
@tintHours as tinyint=0,    
@bitAlert as bit,
@numDomainID as numeric(9)  ,
@numUserCntID as numeric(9) ,
@numTaskType as numeric(9),
@bitRemind AS TINYINT,
@numRemindBeforeMinutes AS INT    
)    
    
As    
    
UpDate tblActionItemData     
Set  TemplateName = @templateName ,     
 DueDays = @dueDays ,     
 Priority = @status ,     
 Type = @type ,     
 Activity = @activity ,     
 Comments = @comments,  
 bitSendEmailTemplate=@bitSendEmailTemplate,  
 numEmailTemplate=@numEmailTemplate,  
 tintHours=@tintHours,  
 bitAlert=@bitAlert ,
  numDomainID = @numDomainID,
numModifiedBy=@numUserCntID,
dtModifiedBy=@numUserCntID,
numTaskType=@numTaskType,
bitRemind=@bitRemind,
numRemindBeforeMinutes=@numRemindBeforeMinutes
Where RowID = @rowID
GO
