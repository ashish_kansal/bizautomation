/****** Object:  StoredProcedure [dbo].[USP_GetCurrentOpeningBalanceForGeneralLedgerDetails]    Script Date: 07/26/2008 16:17:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created By Siva                                                    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getcurrentopeningbalanceforgeneralledgerdetails')
DROP PROCEDURE usp_getcurrentopeningbalanceforgeneralledgerdetails
GO
CREATE PROCEDURE [dbo].[USP_GetCurrentOpeningBalanceForGeneralLedgerDetails]          
(@numChartAcntId as numeric(9),          
 @numDomainId as numeric(9),          
 @dtFromDate as datetime,          
 @dtToDate as datetime)                                                    
As                                                    
Begin                                                    
	Declare @numTransactionId as integer                                                    
	Declare @numDebitAmt as DECIMAL(20,5)                                                    
	Declare @numCreditAmt as DECIMAL(20,5)                                                    
	Declare @numAcntTypeId as integer                                                     
	Declare @monCurrentOpeningBalance as DECIMAL(20,5)                                                    
	Declare @numOriginalOpeningBal as DECIMAL(20,5)                                          
	Declare @numOpeningBal as DECIMAL(20,5)                                             
	Declare @count as integer                                    
	Declare @bal as DECIMAL(20,5)                              
	Declare @numAccountId as varchar(1000)                              
	Declare @numAccountId1 as varchar(1000)                              
	Declare @numOrgBal as DECIMAL(20,5)          
	Declare @numAccountIdOpeningEquity as numeric(9)                        
	                            
	Set @monCurrentOpeningBalance=0                                                  
	Set @numOriginalOpeningBal=0                                                  
	Set @count=0                                               
	Set @numOpeningBal=0                                       
	Set @numOrgBal=0                            
      
	Set @numAccountIdOpeningEquity=(Select numAccountId From Chart_Of_Accounts Where bitOpeningBalanceEquity=1  and numDomainId = @numDomainId)       
            
                                                          
	Create table #tempTable (ID INT IDENTITY PRIMARY KEY, numChartAcntId numeric(9))                                                                                                                                  
                            
	Select @bal=dbo.fn_GetCurrentOpeningBalanceForGeneralLedger(@numChartAcntId,@dtFromDate,@dtToDate,@numDomainId)                              
    print 'Bal'+Convert(varchar(50), @bal)                            
	Select @numAccountId1=[dbo].[fn_ChildCategory](@numChartAcntId,@numDomainId)                            
                            
                             
	insert into #tempTable (numChartAcntId)                             
	(SELECT * FROM dbo.function_string_to_table(@numAccountId1, ','))                            
	--exec ('insert into #tempTable' +  )                              
                           
	Declare @TotRecs as integer                            
	Declare @i as integer                            
	Set @i=0                            
	set @TotRecs=(select count(*) from #tempTable)                            
	print @numAccountId                              
	while @i<=@TotRecs                               
		Begin                                  
			Select @numAccountId=isnull(numChartAcntId,0) From #temptable Where Id=@i                                                                          
			Print  @numAccountId                                     
			If @numAccountId<>@numAccountIdOpeningEquity                                    
				Begin                                  
					 Print 'SP' + convert(varchar(5),@numAccountId)                            
					 Select @count= Count(*) From General_Journal_Header  GJH                                                    
					 Inner Join  General_Journal_Details GJD on GJH.numJournal_Id=GJD.numJournalId Where (GJD.numChartAcntId=@numAccountId Or GJH.numChartAcntId=@numAccountId)            
					 And GJH.numDomainId=@numDomainId And GJH.datEntry_Date <='' + convert(varchar(300),@dtFromDate)                                  
					 Print '@count' +Convert(varchar(5),@count)                            
				End           
                                              
				If @count=0                                               
					Begin                              
						 Select @numTransactionId=min(numTransactionId) From General_Journal_Header  GJH                                                    
						 Inner Join  General_Journal_Details GJD on GJH.numJournal_Id=GJD.numJournalId Where GJD.numChartAcntId=@numAccountId And GJH.numDomainId=@numDomainId                                                   
						 And GJH.datEntry_Date >='' + convert(varchar(300),@dtFromDate) +'' And GJH.datEntry_Date<='' + convert(varchar(300),@dtToDate) +''                                              
							Select @numOriginalOpeningBal=isnull(numOriginalOpeningBal,0),@numOpeningBal=isnull(numOpeningBal,0) From Chart_Of_Accounts                                            
						 Where numAccountId=@numAccountId And numDomainId=@numDomainId -- And dtOpeningDate <='' + convert(varchar(300),@dtFromDate) +''                                            
						 And dtOpeningDate>='' + convert(varchar(300),@dtFromDate) +''  And dtOpeningDate<='' + convert(varchar(300),@dtToDate) +''                                  
						 Print @numOriginalOpeningBal                            
					End                           
				Else                                              
					Begin                                              
						Select @numTransactionId=min(numTransactionId) From General_Journal_Header  GJH                      
						Inner Join  General_Journal_Details GJD on GJH.numJournal_Id=GJD.numJournalId Where GJD.numChartAcntId=@numAccountId  And GJH.numDomainId=@numDomainId                                                  
						-- And GJH.datEntry_Date >='' + convert(varchar(300),@dtFromDate) +''                                              
						And GJH.datEntry_Date<='' + convert(varchar(300),@dtToDate) +''                                    
					                               
					                                
						Select @numOriginalOpeningBal=isnull(numOriginalOpeningBal,0),@numOpeningBal=isnull(numOpeningBal,0) From Chart_Of_Accounts                                            
						Where numAccountId=@numAccountId And numDomainId=@numDomainId --And dtOpeningDate>='' + convert(varchar(300),@dtFromDate) +''                                            
						And dtOpeningDate<='' + convert(varchar(300),@dtToDate) +''                                                
					               
						Print '@numOriginalOpeningBal'            
	                                               
					End                                     
	Set @numOrgBal = @numOrgBal+@numOriginalOpeningBal                                  
	print '@numOrgBal'+convert(varchar(100),@numOrgBal)                            
	While @numTransactionId <>0                                                    
		Begin                                                    
			Select @numAcntTypeId=numAcntTypeID From Chart_Of_Accounts Where numAccountId=@numAccountId  And numDomainId=@numDomainId                                                  
			If @numAcntTypeId=815 or @numAcntTypeId=816 or @numAcntTypeId=820  or @numAcntTypeId=821  or @numAcntTypeId=822  or @numAcntTypeId=825 or @numAcntTypeId=827                                                                                           
				Begin                                                    
					Select @numDebitAmt=isnull(numdebitAmt,0),@numCreditAmt=isnull(numCreditAmt,0) From General_Journal_Details Where numTransactionId=@numTransactionId And numDomainId=@numDomainId                                                   
					If @numDebitAmt <>0                                                     
						Set @monCurrentOpeningBalance=@monCurrentOpeningBalance-@numDebitAmt                                                    
					Else                                      
						Set  @monCurrentOpeningBalance=@monCurrentOpeningBalance+@numCreditAmt                                                    
					End                                                    
				Else                                      
					Begin                                                    
						Select @numDebitAmt=numdebitAmt ,@numCreditAmt=numCreditAmt From General_Journal_Details  Where numTransactionId=@numTransactionId                                                    
						If @numDebitAmt <>0                
							Set @monCurrentOpeningBalance=@monCurrentOpeningBalance+@numDebitAmt                                                    
						Else                                                    
							Set @monCurrentOpeningBalance=@monCurrentOpeningBalance-@numCreditAmt                                                    
					End                                                  
				If @count=0
					Begin                                                
						Select @numTransactionId=min(numTransactionId) From General_Journal_Header  GJH                                                    
						inner join  General_Journal_Details GJD on GJH.numJournal_Id=GJD.numJournalId Where GJD.numChartAcntId=@numAccountId  And GJD.numTransactionId>@numTransactionId                                            
						And GJH.numDomainId=@numDomainId And GJH.datEntry_Date >='' + convert(varchar(300),@dtFromDate) +'' And GJH.datEntry_Date<='' + convert(varchar(300),@dtToDate) +''                                                    
					End                                                  
				Else                                         
					Begin                                              
						Select @numTransactionId=min(numTransactionId) From General_Journal_Header  GJH                                                    
						inner join  General_Journal_Details GJD on GJH.numJournal_Id=GJD.numJournalId Where GJD.numChartAcntId=@numAccountId And GJD.numTransactionId>@numTransactionId                                                    
						And GJH.numDomainId=@numDomainId And  GJH.datEntry_Date<='' + convert(varchar(300),@dtToDate) +''                                                    
                                              
			        End                                              
			End                               
		print 'Sivaprakasam'                                         
                                      
		Select @numAccountId=MIN(numAccountId) From Chart_Of_Accounts Where numParntAcntTypeId = @numChartAcntId  And numAccountId>@numAccountId  And numDomainId=@numDomainId                                      
                                   
		Set @i=@i+1                             
                              
 End                                  
                             
	If @numChartAcntId<>@numAccountIdOpeningEquity -- For Opening Balance Equity                               
		Begin                                   
			Set @monCurrentOpeningBalance=@monCurrentOpeningBalance                                   
		End                            
		print @monCurrentOpeningBalance                            
		Set @monCurrentOpeningBalance= @bal+@monCurrentOpeningBalance                                           
                                 
		Select convert(varchar(20),@monCurrentOpeningBalance)                                                    
                                                     
 End
GO
