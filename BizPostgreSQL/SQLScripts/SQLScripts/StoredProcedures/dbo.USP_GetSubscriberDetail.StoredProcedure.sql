GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getsubscriberdetail')
DROP PROCEDURE usp_getsubscriberdetail
GO
CREATE PROCEDURE [dbo].[USP_GetSubscriberDetail]  
(
	@numSubscriberID NUMERIC(18,0),
	@numDomainID NUMERIC(18,0)
)
AS
BEGIN
	DECLARE @BranchCount AS INT
	SELECT @BranchCount=COUNT(*) FROM [Domain] WHERE [numSubscriberID]=@numSubscriberID
  
	SELECT   
		vcCompanyName,  
		ISNULL(intNoofUsersSubscribed,0) intNoofUsersSubscribed,
		ISNULL(intNoofLimitedAccessUsers,0) intNoofLimitedAccessUsers,
		ISNULL(intNoofBusinessPortalUsers,0) intNoofBusinessPortalUsers,
		ISNULL(intFullUserConcurrency,0) intFullUserConcurrency,
		ISNULL((SELECT COUNT(*) FROM Sites SI WHERE SI.numDomainID=S.numTargetDomainID AND ISNULL(SI.bitIsActive,0)=1 AND LEN(ISNULL(SI.vcLiveURL,'')) > 0),0) intNoofSites,
		monFullUserCost,
		monLimitedAccessUserCost,
		monBusinessPortalUserCost,
		monSiteCost,
		ISNULL(intNoOfPartners,0) intNoOfPartners,  
		dtSubStartDate,  
		dtSubEndDate,  
		ISNULL(bitTrial,0) bitTrial,  
		numAdminContactID,  
		ISNULL(bitActive,0) bitActive,  
		numTargetDomainID,  
		dtSuspendedDate,  
		ISNULL(vcSuspendedReason,'') vcSuspendedReason,  
		S.numCreatedBy,  
		S.dtCreatedDate,  
		S.numModifiedBy,  
		S.dtModifiedDate,
		ISNULL(@BranchCount,0) AS BranchCount,Do.tintLogin,
		ISNULL(intNoofPartialSubscribed,0) intNoofPartialSubscribed,  
		ISNULL(intNoofMinimalSubscribed,0) intNoofMinimalSubscribed,
		dtEmailStartDate,
		dtEmailEndDate,
		ISNULL(intNoOfEmail,0) intNoOfEmail,
		ISNULL([S].[bitEnabledAccountingAudit],0) AS [bitEnabledAccountingAudit],
		ISNULL([S].[bitEnabledNotifyAdmin],0) AS [bitEnabledNotifyAdmin]
	FROM 
		Subscribers S  
	INNER JOIN 
		DivisionMaster D  
	ON 
		D.numDivisionID=S.numDivisionID  
	INNER JOIN 
		CompanyInfo C  
	ON 
		C.numCompanyID=D.numCompanyID  
	INNER JOIN 
		Domain Do  
	ON 
		S.numTargetDomainID=Do.numDomainID
	WHERE 
		S.numSubscriberID=@numSubscriberID 
		AND S.numDomainID=@numDomainID
END
GO
