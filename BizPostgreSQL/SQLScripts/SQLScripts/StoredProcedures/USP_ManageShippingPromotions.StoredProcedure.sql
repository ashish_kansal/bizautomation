GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_ManageShippingPromotions' ) 
    DROP PROCEDURE USP_ManageShippingPromotions
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[USP_ManageShippingPromotions]
    @bitFreeShiping BIT,
	@monFreeShippingOrderAmount DECIMAL(20,5),
	@numFreeShippingCountry NUMERIC(18,0),
	@bitFixShipping1 BIT,
	@monFixShipping1OrderAmount DECIMAL(20,5),
	@monFixShipping1Charge DECIMAL(20,5),
	@bitFixShipping2 BIT,
	@monFixShipping2OrderAmount DECIMAL(20,5),
	@monFixShipping2Charge DECIMAL(20,5),	
    @numDomainId NUMERIC(18,0),
	@numUserCntID NUMERIC(18,0)
AS 
BEGIN
	IF EXISTS(SELECT * FROM ShippingPromotions WHERE numDomainId = @numDomainId)
	BEGIN
		DECLARE @numShipProId AS NUMERIC(9)
		SELECT TOP 1 @numShipProId = numShipProId FROM ShippingPromotions WHERE numDomainId = @numDomainId

		UPDATE ShippingPromotions
		SET [bitFixShipping1] = @bitFixShipping1,
			[monFixShipping1OrderAmount] = @monFixShipping1OrderAmount,
			[monFixShipping1Charge] = @monFixShipping1Charge, 
			[bitFixShipping2] = @bitFixShipping2, 
			[monFixShipping2OrderAmount] = @monFixShipping2OrderAmount, 
			[monFixShipping2Charge] = @monFixShipping2Charge,
		    [bitFreeShiping] = @bitFreeShiping, 
			[monFreeShippingOrderAmount] = @monFreeShippingOrderAmount, 
			[numFreeShippingCountry] = @numFreeShippingCountry, 
			[numModifiedBy] = @numUserCntID, 
			[dtModified] = GETUTCDATE()
		WHERE numDomainId = @numDomainId

		SELECT @numShipProId
	END
	ELSE
	BEGIN
		INSERT INTO ShippingPromotions
        (
           [bitFixShipping1], [monFixShipping1OrderAmount], [monFixShipping1Charge], [bitFixShipping2], [monFixShipping2OrderAmount], [monFixShipping2Charge],
		   [bitFreeShiping], [monFreeShippingOrderAmount], [numFreeShippingCountry], [numDomainId], [numCreatedBy], [dtCreated]
        )
        VALUES  
		(
			
		   @bitFixShipping1, @monFixShipping1OrderAmount, @monFixShipping1Charge, @bitFixShipping2, @monFixShipping2OrderAmount, @monFixShipping2Charge, 
		   @bitFreeShiping, @monFreeShippingOrderAmount, @numFreeShippingCountry, @numDomainId, @numUserCntID, GETUTCDATE()
        )
		SELECT SCOPE_IDENTITY()
	END
		
END
