/****** Object:  StoredProcedure [dbo].[USP_GetCustomFieldsTab]    Script Date: 07/26/2008 16:17:04 ******/
SET ANSI_NULLS  ON 
GO
SET QUOTED_IDENTIFIER  ON 
GO
--created by anoop jayaraj
-- EXEC [dbo].[USP_GetCustomFieldsTab] 2,1,1,1
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getcustomfieldstab')
DROP PROCEDURE usp_getcustomfieldstab
GO
CREATE PROCEDURE [dbo].[USP_GetCustomFieldsTab]
           @LocID       AS NUMERIC,
           @numDomainID AS NUMERIC(9),
           @Type        AS NUMERIC=-1,
           @numGroupID AS NUMERIC(9)
AS
  SELECT 
  G.*,
  CASE G.tintType
  WHEN 0 THEN 'Tab For Normal Fields'
  WHEN 1 THEN 'Tab For Frame'
  WHEN 2 THEN 'Default Sub-tabs'
  ELSE 'Tab For Normal Fields'
  END AS TabType,
  L.*, 
  ISNULL((SELECT TOP 1 [bitallowed] FROM [GroupTabDetails] GTD WHERE ISNULL(GTD.tintType,0) = 1 AND GTD.[numGroupId] = @numGroupID AND GTD.numTabID = G.[Grp_id]),0) AS bitAllowed
--  GTD.tintType,
  FROM   CFw_Grp_Master G
         JOIN CFW_Loc_Master L
           ON G.Loc_id = L.Loc_id
--         LEFT OUTER JOIN [GroupTabDetails] GTD ON GTD.numTabID = G.[Grp_id]
  WHERE  (G.Loc_Id = @LocID OR 1=(CASE WHEN @LocID IN (12,13,14) THEN CASE WHEN G.Loc_Id=1 THEN 1 ELSE 0 END ELSE 0 END))
         AND numDomainID = @numDomainID
         AND (G.tintType = @Type OR @Type=-1)
--         AND ISNULL(GTD.tintType,0) = 1 -- 1 for subtabs
--SELECT * FROM CFw_Grp_Master 
-- SELECT * FROM CFw_Grp_Master G LEFT OUTER JOIN [GroupTabDetails] GTD ON G.Grp_Id =GTD.numTabID WHERE ISNULL(GTD.tintType,0) = 1 AND GTD.[numGroupId] = @numGroupID
 
 
