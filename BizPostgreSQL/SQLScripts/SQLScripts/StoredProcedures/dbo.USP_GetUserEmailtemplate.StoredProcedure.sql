--modified by anoop jayaraj
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'usp_getuseremailtemplate' ) 
    DROP PROCEDURE usp_getuseremailtemplate
GO
--exec [USP_GetUserEmailtemplate] 17,72,0
CREATE PROCEDURE [dbo].[USP_GetUserEmailtemplate] 
( 
	@numUserID NUMERIC(18,0) = 0,
    @numDomainID NUMERIC(18,0) = 0,
    @isMarketing AS BIT =0,
	@numGroupId AS NUMERIC(18)=0,
	@numCategoryId AS NUMERIC(18)=0
)
AS 
BEGIN
	DECLARE @dynamicQuery AS VARCHAR(MAX)=''

	IF @isMarketing = 1 
    BEGIN
        SET @dynamicQuery=CONCAT('SELECT 
									vcDocName,
									numGenericDocID,
									vcSubject,
									vcDocDesc,
									VcFileName
								FROM 
									GenericDocuments
								WHERE 
									numDocCategory = (CASE WHEN ',ISNULL(@numCategoryId,0),' = 0 THEN 369 ELSE ',ISNULL(@numCategoryId,0),' END)
									AND numDomainId = ',@numDomainID,'  AND ISNULL(VcFileName,'''') not LIKE ''#SYS#%''	')
    END
    
	IF @isMarketing=0 
	BEGIN
		--1 =generic,2=specific
		 SET @dynamicQuery=CONCAT('SELECT 
										vcDocName,
										numGenericDocID,
										vcSubject,
										vcDocDesc,
										VcFileName
									FROM 
										GenericDocuments
									WHERE 
										numDocCategory = (CASE WHEN ',ISNULL(@numCategoryId,0),' = 0 THEN 369 ELSE ',ISNULL(@numCategoryId,0),' END)
										AND tintDocumentType =1 
										AND numDomainId = ',@numDomainID)
	END

	IF(@numGroupId>0)
	BEGIN
		SET @dynamicQuery=@dynamicQuery+'  AND ('+CAST(@numGroupId AS VARCHAR)+' IN (SELECT Items FROM Split(ISNULL(vcGroupsPermission,''''),'','')) OR ISNULL(vcGroupsPermission,''0'')=''0'') '
	END

	--PRINT @dynamicQuery
	EXEC(@dynamicQuery)
END
GO
