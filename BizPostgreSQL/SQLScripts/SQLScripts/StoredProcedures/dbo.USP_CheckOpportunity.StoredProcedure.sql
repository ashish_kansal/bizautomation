/****** Object:  StoredProcedure [dbo].[USP_CheckOpportunity]    Script Date: 07/26/2008 16:15:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_checkopportunity')
DROP PROCEDURE usp_checkopportunity
GO
CREATE PROCEDURE [dbo].[USP_CheckOpportunity]
@numDivisionId as numeric(9)
as
select count(*) from OpportunityMaster where numDivisionId=@numDivisionId and tintOppStatus=1
GO
