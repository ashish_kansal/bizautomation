/****** Object:  StoredProcedure [dbo].[Usp_GetContactsScheduler]    Script Date: 07/26/2008 16:16:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getcontactsscheduler')
DROP PROCEDURE usp_getcontactsscheduler
GO
CREATE PROCEDURE [dbo].[Usp_GetContactsScheduler]
@numDivisionId as numeric(9),
@numDomainId as numeric(9),
@numScheduleid as numeric(9)
 as            
select 
	vcFirstName+' '+vcLastName as [Name],
	vcEmail as  Email,                                                                                   
	numContactID       
 FROM AdditionalContactsInformation AC join             
 divisionmaster DM on DM.numDivisionID=AC.numDivisionID           
 
 WHERE AC.numDivisionID = @numDivisionId   and      
AC.numDomainId = @numDomainId     
and numContactId not in 
(select numContactId from CustRptSchContacts where numScheduleid = @numScheduleid )
GO
