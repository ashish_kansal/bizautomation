/****** Object:  StoredProcedure [dbo].[usp_DeleteAccounts]    Script Date: 07/26/2008 16:15:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- exec usp_DeleteAccounts @numDivisionID=326791,@numContactID=0,@numDomainID=169
--- Modified By Anoop Jayaraj                          
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_deleteaccounts')
DROP PROCEDURE usp_deleteaccounts
GO
CREATE PROCEDURE [dbo].[usp_DeleteAccounts]
    @numDivisionID numeric(9),
    @numContactID NUMERIC(9)=0,
    @numDomainID as numeric(9)
AS 
BEGIN
    if not exists ( select  *
                    from    DivisionMaster DM
                            join AdditionalContactsInformation ADC on ADC.numDivisionID = DM.numDivisionID
                            join usermaster on numUserDetailid = Adc.numContactId
                            join Subscribers sb on dm.numDivisionid = Sb.numDivisionid
                    where   dm.numDivisionID = @numDivisionID
                            and dm.numDomainID = @numDomainID
                            and bitActivateFlag = 1
                            and bitActive = 1 ) 
        begin     
            if exists ( select  *
                        from    DivisionMaster
                        where   numDivisionID = @numDivisionID
                                and numDomainID = @numDomainID ) 
                begin                           
                  
                  IF EXISTS ( select * from [OpportunityMaster] WHERE numDivisionID =@numDivisionID AND numDomainId=@numDomainID)
                  BEGIN
				  	RAISERROR ('CHILD_OPP', 16, 1 ) ;
                    RETURN 1
				  END
                  IF EXISTS( SELECT  * FROM [Vendor] WHERE numVendorID =@numDivisionID AND numDomainId=@numDomainID)
                  BEGIN
				  	RAISERROR ('CHILD_VENDOR', 16, 1 ) ;
                    RETURN 1
				  END
                  IF EXISTS ( select * from ProjectsMaster WHERE numDivisionID =@numDivisionID AND numDomainId=@numDomainID)
                  BEGIN
				  	RAISERROR ('CHILD_PROJECT', 16, 1 ) ;
                    RETURN 1
				  END
				  IF EXISTS ( select * from [Cases] WHERE numDivisionID =@numDivisionID AND numDomainId=@numDomainID)
                  BEGIN
				  	RAISERROR ('CHILD_CASE', 16, 1 ) ;
                    RETURN 1
				  END
				  IF EXISTS ( select * from [CompanyAssets] WHERE numDivID =@numDivisionID AND numDomainId=@numDomainID)
                  BEGIN
				  	RAISERROR ('CHILD_ASSET', 16, 1 ) ;
                    RETURN 1
				  END
				  IF EXISTS ( select * from [dbo].[DepositMaster] AS DM WHERE [DM].[numDivisionID] = @numDivisionID AND numDomainId = @numDomainID)
                  BEGIN
				  	RAISERROR ('DEPOSIT_EXISTS', 16, 1 ) ;
                    RETURN 1
				  END
				  IF EXISTS ( select * from [dbo].[ReturnHeader] AS RH WHERE [RH].[numDivisionID] = @numDivisionID AND numDomainId = @numDomainID)
                  BEGIN
				  	RAISERROR ('RETURN_EXISTS', 16, 1 ) ;
                    RETURN 1
				  END
				  IF EXISTS ( select * from [dbo].[BillPaymentHeader] AS BPH WHERE [BPH].[numDivisionID] = @numDivisionID AND numDomainId = @numDomainID)
                  BEGIN
				  	RAISERROR ('BILL_PAYMENT_EXISTS', 16, 1 ) ;
                    RETURN 1
				  END
				  IF EXISTS ( select * from [dbo].[BillHeader] AS BH WHERE [BH].[numDivisionID] = @numDivisionID AND numDomainId = @numDomainID)
                  BEGIN
				  	RAISERROR ('STANDALONE_BILL_EXISTS', 16, 1 ) ;
                    RETURN 1
				  END
				  IF EXISTS ( select * from [dbo].[CheckHeader] AS CH WHERE [CH].[numDivisionID] = @numDivisionID AND numDomainId = @numDomainID)
                  BEGIN
				  	RAISERROR ('CHECK_EXISTS', 16, 1 ) ;
                    RETURN 1
				  END

				  /*Bill added against selected company*/
				  IF EXISTS ( select * from [OpportunityBizDocsDetails] WHERE numDivisionID=@numDivisionID AND tintPaymentType=2 AND numDomainId=@numDomainID)
                  BEGIN
				  	raiserror ('BILL', 16, 1 ) ;
                    RETURN 1
				  END
                  
                    BEGIN TRY 
                        BEGIN TRAN
																										
                        delete  RECENTITEMS
                        where   numRecordID = @numDivisionID
                                and chrRecordType = 'C'                      
                    
                        declare @numCompanyID as numeric(9)
                        select  @numCompanyID = numCompanyId
                        from    divisionmaster
                        where   numDivisionID = @numDivisionID    AND numDomainId=@numDomainID                      
                              
                        DELETE  FROM Communication
                        WHERE   numDivisionID = @numDivisionID    AND numDomainId=@numDomainID                           
                          
                        delete  from ExtranetAccountsDtl 
                        where   numExtranetID in (
                                select  numExtranetID
                                from    ExtarnetAccounts
                                where   numDivisionID = @numDivisionID AND numDomainId=@numDomainID )  AND numDomainId=@numDomainID                         
                        delete  from ExtarnetAccounts
                        where   numDivisionID = @numDivisionID  AND numDomainId=@numDomainID                 
                  
                  
                        delete  from CaseContacts
                        where   numContactID in (
                                select  numContactID
                                from    AdditionalContactsInformation
                                where   numDivisionID = @numDivisionID AND numDomainId=@numDomainID)                     
                        delete  from ProjectsContacts
                        where   numContactID in (
                                select  numContactID
                                from    AdditionalContactsInformation
                                where   numDivisionID = @numDivisionID AND numDomainId=@numDomainID)                   
                        delete  from OpportunityContact
                        where   numContactID in (
                                select  numContactID
                                from    AdditionalContactsInformation
                                where   numDivisionID = @numDivisionID AND numDomainId=@numDomainID)                     
                        ------------------------------
						DELETE FROM [dbo].[DepositeDetails]
						WHERE [numDepositID] IN (SELECT [DM].[numDepositId] FROM [dbo].[DepositMaster] AS DM 
												WHERE DM.[numDivisionID] = @numDivisionID
												AND [DM].[numDomainId] = @numDomainID)
						
						------------------------------
						DELETE FROM [dbo].[DepositMaster] 
						WHERE [numDivisionID] = @numDivisionID
						AND [numDomainId] = @numDomainID
						------------------------------
						DELETE FROM dbo.ReturnItems WHERE [numReturnHeaderID] IN (SELECT [numReturnHeaderID] FROM ReturnHeader 
																				 WHERE numDomainID = @numDomainID 
																				 AND [ReturnHeader].[numDivisionId] = @numDivisionID)
						DELETE FROM ReturnPaymentHistory WHERE numReturnHeaderID IN ( SELECT [numReturnHeaderID] FROM ReturnHeader 
																				    WHERE numDomainID = @numDomainID  
																					AND [ReturnHeader].[numDivisionId] = @numDivisionID)
						DELETE FROM	dbo.ReturnHeader 
						WHERE numDomainID = @numDomainID  
						AND [ReturnHeader].[numDivisionId] = @numDivisionID
						------------------------------
						DELETE FROM [dbo].[BillPaymentDetails]
						WHERE [BillPaymentDetails].[numBillPaymentID] IN 
						(SELECT [BPH].[numBillPaymentID] FROM [dbo].[BillPaymentHeader] AS BPH WHERE [BPH].[numDivisionID] = @numDivisionID AND [BPH].[numDomainId] = @numDomainID)
						
						DELETE FROM [dbo].[BillPaymentHeader] WHERE [numDivisionID] = @numDivisionID AND [numDomainId] = @numDomainID
						------------------------------
						DELETE FROM [dbo].[BillDetails]
						WHERE [numBillID] IN (SELECT numBillID FROM [dbo].[BillHeader] AS BH WHERE [BH].[numDivisionID] = @numDivisionID AND [BH].[numDomainId] = @numDomainID)

						DELETE FROM [dbo].[BillHeader] WHERE [numDivisionID] = @numDivisionID AND [numDomainId] = @numDomainID
						------------------------------
						DELETE FROM [dbo].[CheckDetails] WHERE [CheckDetails].[numCheckHeaderID] IN 
						(SELECT [CH].[numCheckHeaderID] FROM [dbo].[CheckHeader] AS CH WHERE CH.[numDivisionID] = @numDivisionID AND [CH].[numDomainID] = @numDomainID)

						DELETE FROM [dbo].[CheckDetails_Old] WHERE [CheckDetails_Old].[numCheckId] IN 
						(SELECT [CH].[numCheckHeaderID] FROM [dbo].[CheckHeader] AS CH WHERE CH.[numDivisionID] = @numDivisionID AND [CH].[numDomainID] = @numDomainID)

						DELETE FROM [dbo].[CheckHeader] WHERE [numDivisionID] = @numDivisionID AND [numDomainID] = @numDomainID            
                  
                   
                        set @numContactID = 0                  
                        select Top 1
                                @numContactID = numContactID
                        from    AdditionalContactsInformation
                        where   numDivisionID = @numDivisionID AND numDomainId=@numDomainID
                        order by numContactID                  
                        while @numContactID > 0                  
                            begin        
                                delete  ConECampaignDTL
                                where   numConECampID in (
                                        select  numConEmailCampID
                                        from    ConECampaign
                                        where   numContactID = @numContactID )                    
                                delete  ConECampaign where   numContactID = @numContactID                    
                                delete  AOIContactLink where   numContactID = @numContactID                      
                                delete  CaseContacts where   numContactID = @numContactID                     
                                delete  ProjectsContacts where   numContactID = @numContactID                    
                                delete  OpportunityContact where   numContactID = @numContactID                    
                                delete  UserTeams where   numUserCntID = @numContactID                   
                                delete  UserTerritory where   numUserCntID = @numContactID                   
                    
                                delete from CustomerCreditCardInfo where numContactId = @numContactId  
								DELETE FROM SiteSubscriberDetails WHERE numContactID = @numContactID

                                DELETE  AdditionalContactsInformation WHERE   numContactID = @numContactID    AND numDomainId=@numDomainID  
								          
                                select Top 1
                                        @numContactID = numContactID
                                from    AdditionalContactsInformation
                                where   numDivisionID = @numDivisionID
                                        and numContactID > @numContactID
                                        AND numDomainId=@numDomainID
                                order by numContactID                  
                                if @@rowcount = 0 
                                    set @numContactID = 0                  
                                
                                  
                            end                  
                  
                  
						DELETE FROM dbo.VendorShipmentMethod WHERE numDomainId = @numDomainID AND numVendorID = @numDivisionID 
						DELETE FROM dbo.CompanyAssociations WHERE numDomainID=@numDomainID AND numAssociateFromDivisionID=@numDivisionID
                        DELETE FROM dbo.CompanyAssociations WHERE numDomainID=@numDomainID AND numDivisionID=@numDivisionID  
                        DELETE FROM dbo.ImportActionItemReference WHERE numDomainID=@numDomainID AND numDivisionID=@numDivisionID    
                        DELETE FROM dbo.ImportOrganizationContactReference WHERE numDomainID=@numDomainID AND numDivisionID=@numDivisionID    
                        DELETE  FROM AdditionalContactsInformation WHERE   numDivisionID = @numDivisionID    AND numDomainId=@numDomainID                           
                          
--                        delete  from Vendor where   numVendorID = @numDivisionID
						SELECT * INTO #GJDetails FROM [dbo].[General_Journal_Details] AS GJD WHERE [GJD].[numCustomerId] = @numDivisionID
						AND [GJD].[numDomainId] = @numDomainID

						DELETE FROM [General_Journal_Details] WHERE [General_Journal_Details].[numCustomerId] = @numDivisionID
						AND [General_Journal_Details].[numDomainId] = @numDomainID

						DELETE FROM [dbo].[CashCreditCardDetails] WHERE [CashCreditCardDetails].[numCashCreditId] IN 
						(SELECT [numCashCreditCardId] FROM [dbo].[General_Journal_Header] AS GJH WHERE [numDomainId]=@numDomainID
						AND [GJH].[numJournal_Id] IN (SELECT [numJournalID] FROM #GJDetails 
																		WHERE [#GJDetails].[numDomainId] = @numDomainID 
																		AND [#GJDetails].[numCustomerId] = @numDivisionID))
						
						DELETE FROM [dbo].[RecurringTemplate] WHERE [numRecurringId] IN 
						(
							SELECT [numRecurringId] FROM [General_Journal_Header] WHERE [numDomainId]=@numDomainID
							AND [General_Journal_Header].[numJournal_Id] IN (SELECT [numJournalID] FROM #GJDetails 
																			WHERE [#GJDetails].[numDomainId] = @numDomainID 
																			AND [#GJDetails].[numCustomerId] = @numDivisionID)
						)

						DELETE FROM [General_Journal_Header] WHERE [numDomainId]=@numDomainID
						AND [General_Journal_Header].[numJournal_Id] IN (SELECT [numJournalID] FROM #GJDetails 
																		WHERE [#GJDetails].[numDomainId] = @numDomainID 
																		AND [#GJDetails].[numCustomerId] = @numDivisionID);
						DROP TABLE #GJDetails

                        DELETE  FROM DivisionMaster
                        WHERE   numDivisionID = @numDivisionID   AND numDomainId=@numDomainID                                   
                  
                          
                        DELETE  FROM CompanyInfo
                        WHERE   numCompanyID = @numCompanyID  AND numDomainId=@numDomainID        
                        select  1        
                    
                        COMMIT TRAN
                    END TRY 
                    BEGIN CATCH		
                    IF ( @@TRANCOUNT > 0 ) 
                            BEGIN
                                ROLLBACK TRAN
                                DECLARE @error VARCHAR(1000)
								SET @error = ERROR_MESSAGE();
                                raiserror (@error, 16, 1 ) ;
                                RETURN 1
                            END
                    END CATCH	
                    
                end  
                
        end
    else 
        select  0
END
GO
