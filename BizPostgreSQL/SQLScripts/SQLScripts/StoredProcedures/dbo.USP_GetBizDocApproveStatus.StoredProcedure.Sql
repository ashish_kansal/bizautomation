
GO
/****** Object:  StoredProcedure [dbo].[USP_GetBizDocApproveStatus]    Script Date: 02/22/2010 12:21:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getbizdocapprovestatus')
DROP PROCEDURE usp_getbizdocapprovestatus
GO
CREATE PROCEDURE [dbo].[USP_GetBizDocApproveStatus]    
@numBizDocID as numeric(9)=0,    
@numDomainID as numeric(18),
@btDocType as bit,
@numStatus as numeric(1)

as    
begin

		select  @numBizDocID as numDocId,A.numContactId,'' as VcComment,@numStatus as Status,B.dtApprovedOn,'' as cDocType,vcFirstName+' '+vcLastName as [Name] ,vcCompanyName  
		from BizDocAction A join BizActionDetails B
		on A.numBizActionId=B.numBizActionId join AdditionalContactsInformation C 
		on C.numContactID=A.numContactID join DivisionMaster DM 
		on DM.numDivisionID=C.numDivisionID  join CompanyInfo CI    
		on CI.numCompanyID=DM.numCompanyID 
--join DocumentWorkflow DW
--		on DW.numDocId=numOppBizDocsId and DW.numContactID=A.numContactID			
		where B.numOppBizDocsId=@numBizDocID and 
			  A.numDomainId=@numDomainID and 
			  b.btDocType=@btDocType and
			  b.btStatus=@numStatus

	Union
		
		select  @numBizDocID  as numDocId,A.numContactId,'' as VcComment,@numStatus as Status,B.dtApprovedOn,'' as cDocType,vcFirstName+' '+vcLastName as [Name]  ,vcCompanyName 
		from BizDocAction A join BizActionDetails B
		on A.numBizActionId=B.numBizActionId join AdditionalContactsInformation C 
		on C.numContactID=A.numContactID join DivisionMaster DM 
		on DM.numDivisionID=C.numDivisionID  join CompanyInfo CI    
		on CI.numCompanyID=DM.numCompanyID 			
		where B.numOppBizDocsId=@numBizDocID and 
			  A.numDomainId=@numDomainID and 
			  b.btDocType=@btDocType and
			  b.btStatus=@numStatus

end
	






  
