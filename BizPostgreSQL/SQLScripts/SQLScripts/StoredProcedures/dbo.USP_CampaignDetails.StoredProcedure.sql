set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go

--created by anoop jayaraj          
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_campaigndetails')
DROP PROCEDURE usp_campaigndetails
GO
CREATE PROCEDURE [dbo].[USP_CampaignDetails]            
@numCampaignId as numeric(9)=0 ,
@ClientTimeZoneOffset Int            
as            
select 
[vcCampaignName],
numCampaignStatus,            
numCampaignType,            
intLaunchDate,            
intEndDate,            
numRegion,            
numNoSent,            
monCampaignCost,            
CAMP.numDomainID,            
dbo.fn_GetContactName(CAMP.numCreatedBy) as CreatedBy,            
convert(varchar(20),dateAdd(minute, -@ClientTimeZoneOffset,CAMP.bintCreatedDate )) as CreatedDate,            
dbo.fn_GetContactName(CAMP.numModifiedBy) as ModifiedBy,            
convert(varchar(20),dateAdd(minute, -@ClientTimeZoneOffset,CAMP.bintModifiedDate )) as ModifiedDate,            
dbo.GetIncomeforCampaign(numCampaignId,1,NULL,NULL) as TIP,            
dbo.GetIncomeforCampaign(numCampaignId,2,NULL,NULL) as TI,            
dbo.GetIncomeforCampaign(numCampaignId,3,NULL,NULL) as CostMFG,            
dbo.GetIncomeforCampaign(numCampaignId,4,NULL,NULL) as ROI,        
dbo.GetIncomeforCampaign(numCampaignId,5,NULL,NULL) as COGS,  
dbo.GetIncomeforCampaign(numCampaignId,6,NULL,NULL) as LaborCost,
ISNULL(bitIsOnline,0) bitIsOnline,
ISNULL(bitIsMonthly,0) bitIsMonthly,
ISNULL(vcCampaignCode,'')  vcCampaignCode,
ISNULL(bitActive,1) bitActive,
(SELECT SUM(monAmount) FROM dbo.BillDetails BD JOIN dbo.BillHeader BH ON BD.numBillID = BH.numBillID AND numCampaignId = CAMP.numCampaignId GROUP BY numCampaignId) AS [numTotalCampaignCost]
from CampaignMaster  CAMP 
--LEFT JOIN dbo.OpportunityMaster Opp ON CAMP.numCampaignId = Opp.numCampainID AND CAMP.numDomainID = Opp.numDomainId        
where numCampaignId = @numCampaignId

