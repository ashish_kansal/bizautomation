/****** Object:  StoredProcedure [dbo].[usp_getDynamicFormList]    Script Date: 07/26/2008 16:17:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- Modified By: Anoop Jayaraj
   -- Created By: Debasish Nag              
-- Created On: 07/08/2005              
-- Purpose: To get a list of dynamic form names and their details              
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getdynamicformlist')
DROP PROCEDURE usp_getdynamicformlist
GO
CREATE PROCEDURE [dbo].[usp_getDynamicFormList]              
@numDomainID as numeric(9)=0,
@tintFlag AS TINYINT=0,
@bitWorkFlow as bit=0,
@bitAllowGridColor as bit=0,
@numBizFormModuleID as INT=0
as              
              
begin     
IF @bitAllowGridColor=1
BEGIn
	Select DM.numFormId, vcFormName, cCustomFieldsAssociated, cAOIAssociated
			from DynamicFormMaster DM where isnull(bitAllowGridColor,0)=@bitAllowGridColor order by DM.numFormId
END

ELSE IF @bitWorkFlow=1
BEGIn
	Select DM.numFormId, vcFormName, cCustomFieldsAssociated, cAOIAssociated
			from DynamicFormMaster DM where isnull(bitWorkFlow,0)=@bitWorkFlow order by DM.numFormId
END

Else IF  @tintFlag =1 OR @tintFlag=3
BEGIN
	Select DM.numFormId, vcFormName, cCustomFieldsAssociated, cAOIAssociated
from DynamicFormMaster DM 
where tintFlag=@tintFlag 
order by DM.numFormId
END 
Else IF  @tintFlag =6
BEGIN
	Select DM.numFormId, vcFormName, cCustomFieldsAssociated, cAOIAssociated
from DynamicFormMaster DM 
where tintFlag=1  AND DM.numFormId IN (34,35,12,36,10,13,40,41,38,39)
order by DM.numFormId
END
ELSE IF(@tintFlag=5)
BEGIN
	Select DM.numFormId, vcFormName, cCustomFieldsAssociated, cAOIAssociated, isNull(vcAdditionalParam,'') as vcAdditionalParam, numGrpId    
from DynamicFormMaster DM 
left join DynamicFormMasterParam  DMP 
on DMP.numFormID=DM.numFormID and DMP.numDomainID=@numDomainID            
where bitDeleted = 0 AND (numBizFormModuleID = @numBizFormModuleID OR ISNULL(@numBizFormModuleID,0) = 0) 
AND tintFlag=0 /*tintFlag=1 are internal forms need not to show in bizform wizard*/
AND DM.numFormId IN (34,35,12,36,10,13,40,41,38,39)
order by DM.numFormId
END
ELSE
BEGIN       
Select DM.numFormId, vcFormName, cCustomFieldsAssociated, cAOIAssociated, isNull(vcAdditionalParam,'') as vcAdditionalParam, numGrpId    
from DynamicFormMaster DM 
left join DynamicFormMasterParam  DMP 
on DMP.numFormID=DM.numFormID and DMP.numDomainID=@numDomainID            
where bitDeleted = 0 AND (numBizFormModuleID = @numBizFormModuleID OR ISNULL(@numBizFormModuleID,0) = 0) 
AND tintFlag=0 /*tintFlag=1 are internal forms need not to show in bizform wizard*/
order by DM.numFormId
END              
end
GO
