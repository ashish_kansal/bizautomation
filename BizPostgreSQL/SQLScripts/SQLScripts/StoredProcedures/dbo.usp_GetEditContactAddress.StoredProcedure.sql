
GO
/****** Object:  StoredProcedure [dbo].[usp_GetEditContactAddress]    Script Date: 01/09/2009 23:36:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created By Anoop Jayaraj                      
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_geteditcontactaddress')
DROP PROCEDURE usp_geteditcontactaddress
GO
CREATE PROCEDURE [dbo].[usp_GetEditContactAddress]                                
 @numContactID NUMERIC = 0                                
--                              
AS                                
BEGIN   

SELECT vcPStreet ,vcPCity, vcPState,vcPPostalCode,vcPCountry,
vcContactLocation,vcStreet,vcCity,intPostalCode,vcState,vcCountry,        
vcContactLocation1,vcStreet1,vcCity1,vcPostalCode1,vcState1,vcCountry1,        
vcContactLocation2,vcStreet2,vcCity2,vcPostalCode2,vcState2,vcCountry2,        
vcContactLocation3,vcStreet3,vcCity3,vcPostalCode3,vcState3,vcCountry3,        
vcContactLocation4,vcStreet4,vcCity4,vcPostalCode4,vcState4,vcCountry4        
FROM AdditionalContactsInformation A left join  ContactAddress  C 
on C.numContactId=A.numContactId                       
WHERE A.numContactId = @numContactID   
                             

       
                                
                             
                                
END
