/****** Object:  StoredProcedure [dbo].[usp_GetCustomFields]    Script Date: 07/26/2008 16:17:04 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getcustomfields')
DROP PROCEDURE usp_getcustomfields
GO
CREATE PROCEDURE [dbo].[usp_GetCustomFields]
	@recid numeric
--
AS

BEGIN
select cfw_fld_master.fld_label, cfw_fld_values_cont.fld_value , recid
from cfw_fld_master INNER JOIN cfw_fld_values_cont ON cfw_fld_master.fld_id = cfw_fld_values_cont.fld_id where 
fld_label = 'Salutation'
and recid = @recid


END
GO
