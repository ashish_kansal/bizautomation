
GO
/****** Object:  StoredProcedure [dbo].[USP_UpdateOppAddress]    Script Date: 05/31/2009 15:44:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj      
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_updateoppaddress')
DROP PROCEDURE usp_updateoppaddress
GO
CREATE PROCEDURE [dbo].[USP_UpdateOppAddress]      
@numOppID as numeric(9)=0,      
@byteMode as tinyint,      
@vcStreet as varchar(100),      
@vcCity as varchar(50),      
@vcPostalCode as varchar(15),      
@numState as numeric(9),      
@numCountry as numeric(9),  
@vcCompanyName as varchar(100), 
@numCompanyId as numeric(9), -- This in Parameter is added by Ajit Singh on 01/10/2008,
@vcAddressName as varchar(50)='',
@numReturnHeaderID as numeric(9)=0,
@bitCalledFromProcedure AS BIT = 0, -- Avoid Tax calculation when this procedure is called from another procedure because all calling procedure have tax calulation uptill now
@numContact NUMERIC(18,0) = 0,
@bitAltContact BIT = 0,
@vcAltContact VARCHAR(200) = '',
@numUserCntID NUMERIC(18,0) = 0
as      
	declare @numDivisionID as numeric(9)   -- This local variable is added by Ajit Singh on 01/10/2008
	
	SET @numReturnHeaderID = NULLIF(@numReturnHeaderID,0)
	SET @numOppID = NULLIF(@numOppID,0)
	
	DECLARE @numDomainID AS NUMERIC(9)
	SET @numDomainID = 0
	
	IF @byteMode=0      
	BEGIN
		IF ISNULL(@numOppID,0) >0
		BEGIN
			 update OpportunityMaster set tintBillToType=2 where numOppID=@numOppID    
	 
			IF NOT EXISTS(SELECT * FROM OpportunityAddress WHERE numOppID=@numOppID)    
			BEGIN
				INSERT INTO OpportunityAddress
				(
					numOppID
					,vcBillCompanyName
					,vcBillStreet
					,vcBillCity
					,numBillState
					,vcBillPostCode
					,numBillCountry
					,vcAddressName
					,numReturnHeaderID
					,numBillCompanyID
					,numBillingContact
					,bitAltBillingContact
					,vcAltBillingContact
				)    
				VALUES
				(
					@numOppID
					,@vcCompanyName
					,@vcStreet
					,@vcCity
					,@numState
					,@vcPostalCode
					,@numCountry
					,@vcAddressName
					,@numReturnHeaderID
					,@numCompanyID
					,@numContact
					,@bitAltContact
					,@vcAltContact
				)    
			END
			ELSE
			BEGIN
				 UPDATE 
					OpportunityAddress 
				SET  
					vcBillCompanyName=@vcCompanyName,       
					vcBillStreet=@vcStreet,      
					vcBillCity=@vcCity,      
					numBillState=@numState,      
					vcBillPostCode=@vcPostalCode,      
					numBillCountry=@numCountry,vcAddressName=@vcAddressName,
					numBillCompanyID=@numCompanyId      
					,numBillingContact=@numContact
					,bitAltBillingContact=@bitAltContact
					,vcAltBillingContact=@vcAltContact
				 WHERE 
					numOppID=@numOppID 
			END

			IF ISNULL(@numUserCntID,0) > 0
			BEGIN
				IF NOT EXISTS (SELECT ID FROM OrgOppAddressModificationHistory WHERE numRecordID=@numOppID AND tintAddressOf=2 AND tintAddressType=1)
				BEGIN
					INSERT INTO OrgOppAddressModificationHistory
					(
						tintAddressOf
						,tintAddressType
						,numRecordID
						,numModifiedBy
						,dtModifiedDate
					)
					VALUES
					(
						2
						,1
						,@numOppID
						,@numUserCntID
						,GETUTCDATE()
					)
				END
				ELSE
				BEGIN
					UPDATE
						OrgOppAddressModificationHistory
					SET
						numModifiedBy=@numUserCntID
						,dtModifiedDate=GETUTCDATE()
					WHERE
						numRecordID=@numOppID 
						AND tintAddressOf=2 
						AND tintAddressType=1
				END
			END
		END	 
		ELSE IF ISNULL(@numReturnHeaderID,0) >0
		BEGIN
			IF NOT EXISTS(SELECT * FROM OpportunityAddress WHERE numReturnHeaderID=@numReturnHeaderID)    
			BEGIN
				INSERT INTO OpportunityAddress
				(
					numOppID
					,vcBillCompanyName
					,vcBillStreet
					,vcBillCity
					,numBillState
					,vcBillPostCode
					,numBillCountry
					,vcAddressName
					,numReturnHeaderID
					,numBillCompanyID
					,numBillingContact
					,bitAltBillingContact
					,vcAltBillingContact
				)    
				VALUES
				(
					@numOppID
					,@vcCompanyName
					,@vcStreet
					,@vcCity
					,@numState
					,@vcPostalCode
					,@numCountry
					,@vcAddressName
					,@numReturnHeaderID
					,@numCompanyID
					,@numContact
					,@bitAltContact
					,@vcAltContact
				)    
			END
			ELSE
			BEGIN
 				UPDATE 
					OpportunityAddress 
				SET  
					vcBillCompanyName=@vcCompanyName     
					,vcBillStreet=@vcStreet  
					,vcBillCity=@vcCity
					,numBillState=@numState    
					,vcBillPostCode=@vcPostalCode
					,numBillCountry=@numCountry
					,vcAddressName=@vcAddressName
					,numBillCompanyID=@numCompanyId
					,numBillingContact=@numContact
					,bitAltBillingContact=@bitAltContact
					,vcAltBillingContact=@vcAltContact
				WHERE 
					numReturnHeaderID = @numReturnHeaderID 
			END
		END
	END      
	ELSE IF @byteMode=1
	BEGIN
		IF ISNULL(@numOppID,0) >0
		BEGIN
			IF @numCompanyId >0 
			BEGIN
				UPDATE OpportunityMaster set tintShipToType=2 where numOppID=@numOppID     
			END 
		
			IF NOT EXISTS(SELECT * FROM OpportunityAddress where numOppID=@numOppID)
			BEGIN
				INSERT INTO OpportunityAddress
				(
					numOppID
					,vcShipCompanyName
					,vcShipStreet
					,vcShipCity
					,numShipState
					,vcShipPostCode
					,numShipCountry
					,vcAddressName
					,numShipCompanyID
					,numShippingContact
					,bitAltShippingContact
					,vcAltShippingContact
				)    
				VALUES
				(
					@numOppID
					,@vcCompanyName
					,@vcStreet
					,@vcCity
					,@numState
					,@vcPostalCode
					,@numCountry
					,@vcAddressName
					,@numCompanyID
					,@numContact
					,@bitAltContact
					,@vcAltContact
				)    
			END
			ELSE 
			BEGIN
				UPDATE 
					OpportunityAddress 
				SET  
					vcShipCompanyName= @vcCompanyName
					,vcShipStreet=@vcStreet
					,vcShipCity=@vcCity
					,numShipState=@numState
					,vcShipPostCode=@vcPostalCode
					,numShipCountry=@numCountry
					,vcAddressName=@vcAddressName
					,numShipCompanyID=@numCompanyId
					,numShippingContact=@numContact
					,bitAltShippingContact=@bitAltContact
					,vcAltShippingContact=@vcAltContact     
				WHERE 
					numOppID=@numOppID   
			END

			IF ISNULL(@numUserCntID,0) > 0
			BEGIN
				IF NOT EXISTS (SELECT ID FROM OrgOppAddressModificationHistory WHERE numRecordID=@numOppID AND tintAddressOf=2 AND tintAddressType=2)
				BEGIN
					INSERT INTO OrgOppAddressModificationHistory
					(
						tintAddressOf
						,tintAddressType
						,numRecordID
						,numModifiedBy
						,dtModifiedDate
					)
					VALUES
					(
						2
						,2
						,@numOppID
						,@numUserCntID
						,GETUTCDATE()
					)
				END
				ELSE
				BEGIN
					UPDATE
						OrgOppAddressModificationHistory
					SET
						numModifiedBy=@numUserCntID
						,dtModifiedDate=GETUTCDATE()
					WHERE
						numRecordID=@numOppID 
						AND tintAddressOf=2 
						AND tintAddressType=2
				END
			END
		END
		ELSE IF ISNULL(@numReturnHeaderID,0) >0
		BEGIN
			IF NOT EXISTS(SELECT * FROM OpportunityAddress where numReturnHeaderID=@numReturnHeaderID)    
			BEGIN
				INSERT INTO OpportunityAddress
				(
					numOppID
					,vcShipCompanyName
					,vcShipStreet
					,vcShipCity
					,numShipState
					,vcShipPostCode
					,numShipCountry
					,vcAddressName
					,numShipCompanyID
					,numShippingContact
					,bitAltShippingContact
					,vcAltShippingContact
				)    
				VALUES
				(
					@numOppID
					,@vcCompanyName
					,@vcStreet
					,@vcCity
					,@numState
					,@vcPostalCode
					,@numCountry
					,@vcAddressName
					,@numCompanyID
					,@numContact
					,@bitAltContact
					,@vcAltContact
				)    
			END
			ELSE
			BEGIN
				UPDATE 
					OpportunityAddress 
				SET  
					vcShipCompanyName= @vcCompanyName
					,vcShipStreet=@vcStreet
					,vcShipCity=@vcCity
					,numShipState=@numState  
					,vcShipPostCode=@vcPostalCode  
					,numShipCountry=@numCountry
					,vcAddressName=@vcAddressName
					,numShipCompanyID=@numCompanyId
					,numShippingContact=@numContact
					,bitAltShippingContact=@bitAltContact
					,vcAltShippingContact=@vcAltContact     
				WHERE 
					numReturnHeaderID = @numReturnHeaderID 
			END
		END
	END
	ELSE IF @byteMode=2
	BEGIN
		update OpportunityMaster set tintShipToType=3 where numOppID=@numOppID   

		IF ISNULL(@numUserCntID,0) > 0
		BEGIN
			IF NOT EXISTS (SELECT ID FROM OrgOppAddressModificationHistory WHERE numRecordID=@numOppID AND tintAddressOf=2 AND tintAddressType=2)
			BEGIN
				INSERT INTO OrgOppAddressModificationHistory
				(
					tintAddressOf
					,tintAddressType
					,numRecordID
					,numModifiedBy
					,dtModifiedDate
				)
				VALUES
				(
					2
					,2
					,@numOppID
					,@numUserCntID
					,GETUTCDATE()
				)
			END
			ELSE
			BEGIN
				UPDATE
					OrgOppAddressModificationHistory
				SET
					numModifiedBy=@numUserCntID
					,dtModifiedDate=GETUTCDATE()
				WHERE
					numRecordID=@numOppID 
					AND tintAddressOf=2 
					AND tintAddressType=2
			END
		END
	END
	ELSE IF @byteMode= 3     
	BEGIN
		--SELECT * FROM dbo.AddressDetails WHERE numRecordID = @numOppID 
		IF NOT EXISTS(SELECT * FROM dbo.AddressDetails WHERE numRecordID = @numOppID AND tintAddressOf = 4)    
			BEGIN
				PRINT 'ADD Mode'
				INSERT INTO dbo.AddressDetails 
				(vcAddressName,vcStreet,vcCity,vcPostalCode,numState,numCountry,bitIsPrimary,tintAddressOf,tintAddressType,numRecordID,numDomainID )
				SELECT @vcAddressName,@vcStreet,@vcCity,@vcPostalCode,@numState,@numCountry,1,4,2,@numOppID,numDomainID	
				FROM dbo.ProjectsMaster WHERE numProId = @numOppID
				
				UPDATE dbo.ProjectsMaster SET numAddressID = SCOPE_IDENTITY() WHERE numProId = @numOppID
			END
			
		ELSE
			BEGIN
				PRINT 'Update Mode'	 
				UPDATE dbo.AddressDetails SET 
							vcStreet = @vcStreet,
							vcCity = @vcCity,
							numState = @numState,
							vcPostalCode = @vcPostalCode,
							numCountry = @numCountry,
							vcAddressName = @vcAddressName  
				WHERE   numRecordID = @numOppID   	
			END
			
        --SELECT * FROM dbo.AddressDetails WHERE numRecordID = @numOppID 			
	END

IF @numOppID>0 AND ISNULL(@bitCalledFromProcedure,0)=0
BEGIN
	IF (SELECT COUNT(*) FROM OpportunityBizDocs WHERE numOppId=@numOppID AND bitAuthoritativeBizDocs=1) = 0
	BEGIN
		DECLARE @tintOppType TINYINT
		SELECT 
			@numDomainID=numDomainID
			,@numDivisionID=numDivisionID
			,@tintOppType=tintOppType
		FROM 
			OpportunityMaster 
		WHERE 
			numOppID=@numOppId

		DELETE FROM OpportunityMasterTaxItems WHERE numOppId=@numOppID

		--INSERT TAX FOR DIVISION   
		IF @tintOppType=1 OR (@tintOppType=2 and (select isnull(bitPurchaseTaxCredit,0) from Domain where numDomainId=@numDomainId)=1)
		BEGIN
			INSERT INTO dbo.OpportunityMasterTaxItems 
			(
				numOppId,
				numTaxItemID,
				fltPercentage,
				tintTaxType,
				numTaxID
			) 
			SELECT 
				@numOppID,
				TI.numTaxItemID,
				TEMPTax.decTaxValue,
				TEMPTax.tintTaxType,
				0
			FROM 
				TaxItems TI 
			JOIN 
				DivisionTaxTypes DTT 
			ON 
				TI.numTaxItemID = DTT.numTaxItemID
			CROSS APPLY
			(
				SELECT
					decTaxValue,
					tintTaxType
				FROM
					dbo.fn_CalItemTaxAmt(@numDomainID,@numDivisionID,0,TI.numTaxItemID,@numOppId,0,NULL)
			) AS TEMPTax
			WHERE 
				DTT.numDivisionID=@numDivisionId 
				AND DTT.bitApplicable=1
			UNION 
			SELECT 
				@numOppID,
				0,
				TEMPTax.decTaxValue,
				TEMPTax.tintTaxType,
				0
			FROM 
				dbo.DivisionMaster 
			CROSS APPLY
			(
				SELECT
					decTaxValue,
					tintTaxType
				FROM
					dbo.fn_CalItemTaxAmt(@numDomainID,@numDivisionID,0,0,@numOppId,1,NULL)
			) AS TEMPTax
			WHERE 
				bitNoTax=0 
				AND numDivisionID=@numDivisionID	
			UNION 
			SELECT
				@numOppId
				,1
				,decTaxValue
				,tintTaxType
				,numTaxID
			FROM
				dbo.fn_CalItemTaxAmt(@numDomainID,@numDivisionID,0,1,@numOppId,1,NULL)		
		END	

		UPDATE OBD SET monDealAmount= [dbo].[GetDealAmount](@numOppId ,getutcdate(),OBD.numOppBizDocsId ) 
		FROM dbo.OpportunityBizDocs OBD WHERE numOppId=@numOppID
	END                
END 


