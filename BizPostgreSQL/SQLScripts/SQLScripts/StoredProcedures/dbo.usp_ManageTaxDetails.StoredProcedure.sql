/****** Object:  StoredProcedure [dbo].[usp_ManageTaxDetails]    Script Date: 07/26/2008 16:20:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_managetaxdetails')
DROP PROCEDURE usp_managetaxdetails
GO
CREATE PROCEDURE [dbo].[usp_ManageTaxDetails]  
@numCountry as numeric(9),  
@numState as numeric(9),  
@decTax as decimal(10,4),    
@numTaxId as numeric(10),  
@numDomainId  as numeric(9),  
@tintMode as tinyint,
@numTaxItemID as numeric(9),
@vcCity as varchar(100),
@vcZipPostal as varchar(20),
@tintTaxType AS TINYINT,
@vcTaxUniqueName AS VARCHAR(300)
AS   
	if @tintMode = 0 --Insert record
	BEGIN  
		IF @numState = 0
		BEGIN
			SET  @vcCity = ''
			SET  @vcZipPostal = ''  
		END

		IF (SELECT COUNT(*) FROM TaxDetails WHERE numCountryID = @numCountry AND numStateID = @numState AND numDomainId = @numDomainId AND numTaxItemID=@numTaxItemID and vcCity=@vcCity and vcZipPostal=@vcZipPostal AND ISNULL(vcTaxUniqueName,'') = ISNULL(@vcTaxUniqueName,'')) = 0  
		BEGIN  
			INSERT INTO TaxDetails  
			(
				numTaxItemID, [numCountryID], [numStateID], [decTaxPercentage], [numDomainId], vcCity, vcZipPostal, tintTaxType, vcTaxUniqueName)  
			VALUES  
			( 
				ISNULL(@numTaxItemID,0), @numCountry, @numState, @decTax, @numDomainId, @vcCity, @vcZipPostal, @tintTaxType, @vcTaxUniqueName
			)  

			SELECT  SCOPE_IDENTITY();   
		END  
		ELSE
		BEGIN
			SELECT 0
		END
	END  
	ELSE IF @tintMode = 1 --Delete particular record
	BEGIN
		DELETE TaxDetails WHERE numTaxId = @numTaxId AND [numDomainId]=@numDomainId  
		SELECT @numTaxId  
	END
	ELSE IF @tintMode = 2 --Delete All records
	BEGIN  
		DELETE TaxDetails WHERE [numDomainId]=@numDomainId  
		SELECT 0 
	END
GO
