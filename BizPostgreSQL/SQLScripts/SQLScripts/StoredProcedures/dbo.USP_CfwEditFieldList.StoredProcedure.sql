/****** Object:  StoredProcedure [dbo].[USP_CfwEditFieldList]    Script Date: 07/26/2008 16:15:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE         proc [dbo].[USP_CfwEditFieldList]
(
@strFieldList as text='',
@relId as numeric(9)=null
)
as

	declare @hDoc  int
	EXEC sp_xml_preparedocument @hDoc OUTPUT, @strFieldList
	update CFW_Fld_Dtl
		set tintFldReq= X.req
						
	from(SELECT  * FROM OPENXML (@hDoc,'/NewDataSet/Table',2)
	WITH (	fieldid numeric(9),
		req tinyint
		))X
	where numFieldId=X.fieldid and numRelation=@relId

	


	EXEC sp_xml_removedocument @hDoc
GO
