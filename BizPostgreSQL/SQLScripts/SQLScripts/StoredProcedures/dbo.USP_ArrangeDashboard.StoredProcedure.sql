/****** Object:  StoredProcedure [dbo].[USP_ArrangeDashboard]    Script Date: 07/26/2008 16:14:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_arrangedashboard')
DROP PROCEDURE usp_arrangedashboard
GO
CREATE PROCEDURE [dbo].[USP_ArrangeDashboard]
@numDashboardReptID as numeric(9)=0,
@charMove as char(1),
@strItems TEXT=''
AS
declare @numGroupUserCntId as numeric(9)
declare @bitGroup as bit
declare @tintColumn as tinyint
declare @tintRow as tinyint

select @numGroupUserCntId=numGroupUserCntID,@bitGroup=bitGroup,@tintColumn=tintColumn,@tintRow=tintRow

from Dashboard where numDashBoardReptID=@numDashboardReptID

if @charMove='L'
begin

	update Dashboard set tintRow=tintRow-1 where numGroupUserCntID=@numGroupUserCntId 
	and bitGroup=@bitGroup and tintColumn=@tintColumn and tintRow>@tintRow
	update Dashboard set tintRow=tintRow+1 where numGroupUserCntID=@numGroupUserCntId 
	and bitGroup=@bitGroup and tintColumn=@tintColumn-1
	update Dashboard set tintColumn=tintColumn-1,tintRow=1 where numDashBoardReptID= @numDashboardReptID
end
else if @charMove='R'
begin
	update Dashboard set tintRow=tintRow-1 where numGroupUserCntID=@numGroupUserCntId 
	and bitGroup=@bitGroup and tintColumn=@tintColumn and tintRow>@tintRow
	update Dashboard set tintRow=tintRow+1 where numGroupUserCntID=@numGroupUserCntId 
	and bitGroup=@bitGroup and tintColumn=@tintColumn+1
	update Dashboard set tintColumn=tintColumn+1,tintRow=1 where numDashBoardReptID= @numDashboardReptID
end
else if @charMove='U'
begin
	update Dashboard set tintRow=tintRow+1 where numGroupUserCntID=@numGroupUserCntId 
	and bitGroup=@bitGroup and tintColumn=@tintColumn and tintRow=@tintRow-1
	update Dashboard set tintRow=tintRow-1 where numDashBoardReptID= @numDashboardReptID
end
else if @charMove='D'
begin
	update Dashboard set tintRow=tintRow-1 where numGroupUserCntID=@numGroupUserCntId 
	and bitGroup=@bitGroup and tintColumn=@tintColumn and tintRow=@tintRow+1
	update Dashboard set tintRow=tintRow+1 where numDashBoardReptID= @numDashboardReptID
end
else if @charMove='X'
begin
	delete Dashboard where numDashBoardReptID= @numDashboardReptID
END
else if @charMove='A'
BEGIN
	DECLARE  @hDocItem INT
      IF CONVERT(VARCHAR(10),@strItems) <> ''
        BEGIN
          EXEC sp_xml_preparedocument
            @hDocItem OUTPUT ,
            @strItems
            
		  UPDATE dbo.Dashboard SET tintColumn=X.tintColumn,tintRow=X.tintRow
		  FROM  (SELECT *
                  FROM   OPENXML (@hDocItem, '/NewDataSet/Table1', 2)
                            WITH (numDashBoardReptID NUMERIC(9),
								  tintColumn tinyint,
								  tintRow tinyint)) X
		  WHERE Dashboard.numDashBoardReptID=x.numDashBoardReptID
          
          EXEC sp_xml_removedocument
            @hDocItem
        END
	
END

GO
