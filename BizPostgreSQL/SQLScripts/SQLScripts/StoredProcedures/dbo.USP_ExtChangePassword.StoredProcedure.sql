/****** Object:  StoredProcedure [dbo].[USP_ExtChangePassword]    Script Date: 07/26/2008 16:15:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_extchangepassword')
DROP PROCEDURE usp_extchangepassword
GO
CREATE PROCEDURE [dbo].[USP_ExtChangePassword]
	@numContactId AS NUMERIC,
	@numDivisionId AS NUMERIC,
    @OldPassword AS VARCHAR(100),
    @NewPassword AS VARCHAR(100),
    @numDomainID AS NUMERIC
AS 


DECLARE @Count AS INTEGER

SELECT  @Count = COUNT(*)
FROM    ExtarnetAccounts hdr
        JOIN ExtranetAccountsDtl Dtl ON hdr.numExtranetID = Dtl.numExtranetID
WHERE   numDivisionId = @numDivisionId
        AND tintAccessAllowed = 1
        AND numContactID = @numContactId
        AND vcPassword = @OldPassword
        AND Dtl.numDomainID = @numDomainID


IF @Count = 1 
    BEGIN
        UPDATE  ExtranetAccountsDtl
        SET     vcPassword = @NewPassword
        WHERE   tintAccessAllowed = 1
                AND numContactID = @numContactId
                AND numDomainID = @numDomainID
        SELECT  1
    END
ELSE 
    BEGIN
        SELECT  0
    END
GO
