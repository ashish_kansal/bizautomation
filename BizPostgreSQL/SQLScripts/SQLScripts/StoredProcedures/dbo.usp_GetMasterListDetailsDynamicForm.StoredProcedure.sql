/****** Object:  StoredProcedure [dbo].[usp_GetMasterListDetailsDynamicForm]    Script Date: 07/26/2008 16:17:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created By: Debasish Tapan Nag                                                    
--Purpose: Returns the available form fields from the database                                                        
--Created Date: 07/09/2005                            
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getmasterlistdetailsdynamicform')
DROP PROCEDURE usp_getmasterlistdetailsdynamicform
GO
CREATE PROCEDURE [dbo].[usp_GetMasterListDetailsDynamicForm]                      
 @numListID NUMERIC(9),                        
 @vcItemType CHAR(3),                      
 @numDomainID NUMERIC(9)                      
AS     
 
 IF @vcItemType = 'SYS'
	BEGIN
		SELECT 0 AS numItemID,'Lead' AS vcItemName
		UNION ALL
		SELECT 1 AS numItemID,'Prospect' AS vcItemName
		UNION ALL
		SELECT 2 AS numItemID,'Account' AS vcItemName
	END
	
 IF @vcItemType='LI' AND @numListID=9 --Opportunity Source
	 BEGIN
		EXEC USP_GetOpportunitySource @numDomainID
	 END                 
 ELSE IF @vcItemType = 'LI'    --Master List                    
	BEGIN                        
		SELECT LD.vcData AS vcItemName, LD.numListItemID AS numItemID, 'L' As vcItemType, LD.constFlag As flagConst 
		FROM ListDetails LD left join listorder LO on Ld.numListItemID= LO.numListItemID and Lo.numDomainId = @numDomainID  
		WHERE LD.numListID = @numListID AND (LD.numDomainID = @numDomainID OR LD.constFlag = 1)
		order by ISNULL(intSortOrder,LD.sintOrder)                        
	END      
	ELSE IF @vcItemType = 'L' AND ISNULL(@numListID,0) > 0    --Master List                    
	BEGIN                        
		SELECT LD.vcData AS vcItemName, LD.numListItemID AS numItemID, 'L' As vcItemType, LD.constFlag As flagConst 
		FROM ListDetails LD left join listorder LO on Ld.numListItemID= LO.numListItemID and Lo.numDomainId = @numDomainID  
		WHERE LD.numListID = @numListID AND (LD.numDomainID = @numDomainID OR LD.constFlag = 1)
		order by ISNULL(intSortOrder,LD.sintOrder)                        
	END                                          
 ELSE IF @vcItemType = 'T'    --Territories                    
	BEGIN    
		SELECT vcData AS vcItemName, numListItemID AS numItemID, 'T' As vcItemType, 0 As flagConst FROM ListDetails WHERE numListID = 78 AND numDomainID = @numDomainID                                        
	END                        
 ELSE IF @vcItemType = 'AG'   --Lead Groups                     
	BEGIN                        
		SELECT vcGrpName AS vcItemName, numGrpId AS numItemID, 'G' As vcItemType, 0 As flagConst FROM Groups                     
	END                         
 ELSE IF @vcItemType = 'S'    --States                    
	BEGIN                        
		SELECT vcState AS vcItemName, numStateID AS numItemID, 'S' As vcItemType, numCountryID As flagConst FROM State WHERE (numDomainID = @numDomainID OR constFlag = 1) AND 1 = 2                    
	END         
 ELSE IF @vcItemType = 'U'    --Users                    
	BEGIN                        
		SELECT A.numContactID AS numItemID,A.vcFirstName+' '+A.vcLastName AS vcItemName              
		from UserMaster UM             
		join AdditionalContactsInformation A            
		on UM.numUserDetailId=A.numContactID              
		where UM.numDomainID=@numDomainID        
		union        
		select  A.numContactID,vcCompanyName+' - '+A.vcFirstName+' '+A.vcLastName        
		from AdditionalContactsInformation A         
		join DivisionMaster D        
		on D.numDivisionID=A.numDivisionID        
		join ExtarnetAccounts E         
		on E.numDivisionID=D.numDivisionID        
		join ExtranetAccountsDtl DTL        
		on DTL.numExtranetID=E.numExtranetID        
		join CompanyInfo C        
		on C.numCompanyID=D.numCompanyID        
		where A.numDomainID=@numDomainID and bitPartnerAccess=1        
		and D.numDivisionID <>(select numDivisionID from domain where numDomainID=@numDomainID)                      
	END
  ELSE IF @vcItemType = 'C'    --Organization Campaign                    
	BEGIN
	   SELECT  numCampaignID AS numItemID,
                vcCampaignName + CASE bitIsOnline WHEN 1 THEN ' (Online)' ELSE ' (Offline)' END AS vcItemName
       FROM    CampaignMaster
       WHERE   numDomainID = @numDomainID
	END               
  ELSE IF @vcItemType = 'IG' 
	BEGIN
		SELECT vcItemGroup AS vcItemName, numItemGroupID AS numItemID, 'I' As vcItemType, 0 As flagConst FROM dbo.ItemGroups WHERE numDomainID=@numDomainID   	
	END
 ELSE IF @vcItemType = 'PP' 
   BEGIN
		SELECT 'Inventory Item' AS vcItemName, 'P' AS numItemID, 'P' As vcItemType, 0 As flagConst 
		UNION 
		SELECT 'Non-Inventory Item' AS vcItemName, 'N' AS numItemID, 'P' As vcItemType, 0 As flagConst 
		UNION 
		SELECT 'Service' AS vcItemName, 'S' AS numItemID, 'P' As vcItemType, 0 As flagConst 
   END
 ELSE IF @vcItemType = 'V' 
   BEGIN
		SELECT DISTINCT ISNULL(C.vcCompanyName,'') AS vcItemName,  numVendorID AS numItemID ,'V' As vcItemType, 0 As flagConst   FROM dbo.Vendor V INNER JOIN dbo.DivisionMaster DM ON DM.numDivisionID= V.numVendorID 
		INNER JOIN dbo.CompanyInfo C ON C.numCompanyId = DM.numCompanyID WHERE V.numDomainID =@numDomainID
   END
 ELSE IF @vcItemType = 'OC' 
   BEGIN
		SELECT DISTINCT C.vcCurrencyDesc AS vcItemName,C.numCurrencyID AS numItemID FROM dbo.OpportunityMaster OM INNER JOIN dbo.Currency C ON C.numCurrencyID = OM.numCurrencyID
		WHERE OM.numDomainId=@numDomainID
   END
 ELSE  IF @vcItemType = 'UOM'    --Organization Campaign                    
	BEGIN
	   
	   
	   --SELECT  numUOMId AS numItemID,
    --            vcUnitName AS vcItemName
    --   FROM    UOM
    --   WHERE   numDomainID = @numDomainID

	--Modified by Sachin Sadhu ||Date:7thAug2014
	--Purpose :Boneta facing problem in updating units 
	   
			SELECT u.numUOMId as numItemID , u.vcUnitName as vcItemName FROM 
			UOM u INNER JOIN Domain d ON u.numDomainID=d.numDomainID
			WHERE u.numDomainID=@numDomainID AND d.numDomainID=@numDomainID AND 
			u.tintUnitType=(CASE WHEN ISNULL(d.charUnitSystem,'E')='E' THEN 1 WHEN d.charUnitSystem='M' THEN 2 END)


    END   
 ELSE IF @vcItemType = 'O'    --Opp Type                   
	 BEGIN
		SELECT  1 AS numItemID,'Sales' AS vcItemName
		UNion ALL
		SELECT  2 AS numItemID,'Purchase' AS vcItemName
	 END  
 ELSE  IF @vcItemType = 'CHK'    --Opp Type                   
	BEGIN
		SELECT  1 AS numItemID,'Yes' AS vcItemName
		UNion ALL
		SELECT  0 AS numItemID,'No' AS vcItemName
	END  
 ELSE IF @vcItemType='COA'
	BEGIN
		SELECT  C.[numAccountId] AS numItemID,ISNULL(C.[vcAccountName],'') AS vcItemName
		FROM    [Chart_Of_Accounts] C
		INNER JOIN [AccountTypeDetail] ATD 
		ON C.[numParntAcntTypeId] = ATD.[numAccountTypeID]
		WHERE   C.[numDomainId] = @numDomainId
		ORDER BY C.[vcAccountCode]
	END
	ELSE IF @vcItemType = 'PSS'
	BEGIN
		SELECT 
			numShippingServiceID AS numItemID
			,vcShipmentService AS vcItemName 
		FROM 
			ShippingService 
		WHERE 
			(numDomainID=@numDomainID OR ISNULL(numDomainID,0)=0)
		ORDER BY 
			vcShipmentService
	END
       
GO
