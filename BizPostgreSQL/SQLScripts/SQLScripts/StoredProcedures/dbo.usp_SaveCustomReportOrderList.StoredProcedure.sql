/****** Object:  StoredProcedure [dbo].[usp_SaveCustomReportOrderList]    Script Date: 07/26/2008 16:21:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_savecustomreportorderlist')
DROP PROCEDURE usp_savecustomreportorderlist
GO
CREATE PROCEDURE [dbo].[usp_SaveCustomReportOrderList]
@ReportId as numeric(9)=0,
@strOrderList as text=''
as
 delete CustReportOrderlist where numCustomReportId =@ReportId
 declare @hDoc  int              

   EXEC sp_xml_preparedocument @hDoc OUTPUT, @strOrderList    
                 
    insert into CustReportOrderlist              
     (numCustomReportId,vcFieldText,vcValue,numOrder)              
                  
    select @ReportId,
     X.*   from(SELECT * FROM OPENXML (@hDoc,'/NewDataSet/Table',2)              
    WITH  (              
    vcText varchar(200),    
    vcValue varchar(200),
	numOrder numeric))X 
    EXEC sp_xml_removedocument @hDoc
GO
