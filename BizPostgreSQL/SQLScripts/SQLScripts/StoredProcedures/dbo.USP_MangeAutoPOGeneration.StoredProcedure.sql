GO
/****** Object:  StoredProcedure [dbo].[USP_MangeAutoPOGeneration]    Script Date: 03/05/2010 20:37:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_mangeautopogeneration')
DROP PROCEDURE usp_mangeautopogeneration
GO
CREATE PROCEDURE [dbo].[USP_MangeAutoPOGeneration]
@numParentOppID numeric(9),
@numOppBizDocID numeric(9),
@numItemCode numeric(9),
@numDomainID numeric(9),
@numOppBizDocItemID numeric(9),
@monDealAmount DECIMAL(20,5),
@monPaidAmount DECIMAL(20,5)

AS

BEGIN


DECLARE @numNewOppId numeric(9)
DECLARE @numContactId numeric(9)
DECLARE @numDivisionId numeric(9)
DECLARE @numUserCntID NUMERIC(9)
DECLARE @vcCompanyName varchar(250)
DECLARE @monCost DECIMAL(20,5)
DECLARE @CloseDate datetime
DECLARE @vcPOppName VARCHAR(500)
DECLARE @numBizDocId numeric(9)
DECLARE @numCurrencyCode numeric
DECLARE @TotalAmount DECIMAL(20,5)
DECLARE @numOppBizDocsId NUMERIC(9)
DECLARE @numOldBizDocId NUMERIC(9)

select @numCurrencyCode=numCurrencyID from Domain where numDomainID=@numDomainID;

select @CloseDate=getdate()

--delete from  temp_AutoOrder;



--insert into temp_AutoOrder
--select
--@numParentOppID ,
--@numOppBizDocID ,
--@numItemCode ,
--@numDomainID ,
--@numOppBizDocItemID ,
--@numNewOPpId ,
--@numDivisionID

------ FOR THE RULE WHERE BIZDOC PAID FULLY

set @numDivisionId=0
--if @monDealAmount=@monPaidAmount
	
				
			select @numDivisionId=DV.numDivisionId,
			@vcCompanyName=vcCompanyName,@numUserCntID=B.numCreatedBy,@numContactId=AC.numContactId,
			@monCost=monCost
			from OpportunityMaster A
			inner join OpportunityBizDocs B 
			on B.numOppID=A.numOppID
			--inner join 
			--View_BizDoc VB on VB.numOppBizDocsId=B.numOppBizDocsId
			inner join (select * from Item D where d.numItemCode=@numItemCode) D
			on d.numItemCode=@numItemCode
			inner join vendor E 
			on E.numVendorId=D.numVendorId and E.numItemCode=D.numItemCode
			inner join
			OrderAutoRule F
			on F.numDomainID=A.numDomainID and f.btActive=1 --and F.numBillToId=A.tintBillToType and F.numShipToID=A.tintShipToType
			inner join DivisionMaster DV
			on DV.numDivisionId = E.numVendorId 
			inner join CompanyInfo CF 
			on Cf.numCompanyId=DV.numCompanyId inner join 
			AdditionalContactsInformation AC 
			on AC.numDivisionId=DV.numDivisionID and ISNULL(AC.bitPrimaryContact,0)=1
			where A.numOppID=@numParentOppID and D.numItemCode = @numItemCode and 
			A.numDomainID=@numDomainID and D.charItemType='P' and 
			btFullPaid=1 and b.monAMountPaid= @monDealAmount and 
			B.numBizDocStatus=(case isnull(F.numBizDocStatus,0) when  0 then B.numBizDocStatus else F.numBizDocStatus end) and 
			AC.numDomainId=@numDomainId and
			b.numOppBizDocsID=@numOppBizDocID

					insert into Temp_Cheking select 'Insert Amount Match: Div ' + cast(@numDivisionId as varchar(50));

					set @numNewOppId=0;

					Select @numNewOppId=OM.numOppId  from OpportunityLinking OL inner join 
								OpportunityMaster OM on OM.numOppId= OL.numChildOppID
					 where numParentOppBizDocID=@numOppBizDocID and numParentOppID=@numParentOppID and 
						   OM.numDomainId=@numDomainId and numDivisionId=@numDivisionId;
						   
					--update temp_autoOrder set numDivisionID =@numDivisionId;

					set @vcPOppName=''
					if @numDivisionId>0
						begin

							SET @vcPOppName=@vcCompanyName + '-PO-' + datename(Month,GETUTCDATE());

							if @numNewOppId=0
								begin
									EXEC USP_OppManage
									@numNewOppId OUTPUT,
									@numContactId =@numContactId,                                                                          
									@numDivisionId =@numDivisionId,                                                                          
									@tintSource=0,                                                                         
									@vcPOppName =@vcPOppName,                                                                          
									--@Comments ='',                                                                          
									--@bitPublicFlag =0,                                                                          
									@numUserCntID =@numUserCntID,                                                                                 
									--@monPAmount  =0,                                                 
									--@numAssignedTo =0,                                                                                                        
									@numDomainId =@numDomainID,                                                                                                                                                                                   
									--@strItems =null,                                                                          
									--@strMilestone =null,                                                                          
									@dtEstimatedCloseDate =@CloseDate,                                                                          
									@CampaignID =0,
									@lngPConclAnalysis =0,
									@tintOppType =2,                                                                                                                                             
									@tintActive =1,                                                              
									@numSalesOrPurType =0,             
									@numRecurringId=0,
									@numCurrencyID =@numCurrencyCode,
									--@DealStatus as tinyint =0,-- 0=Open,1=Won,2=Lost
									@numStatus =0,
									@vcRefOrderNo =''
									--@numBillAddressId =0,
									--@numShipAddressId =0 


									--update temp_autoOrder set numNewOPpId =@numNewOppId;

									insert into OpportunityLinking (numParentOppID,numChildOppID,numParentOppBizDocID) values
									(@numParentOppID,@numNewOppId,@numOppBizDocID)

									insert into Temp_Cheking select 'Insert Amount Match: New Opp ' + cast(@numNewOppId as varchar(50));
								end
										if @numNewOppId>0 
											begin
												
												if not exists (select numItemCode from OpportunityItems where numOppId=@numNewOppId and numItemCode=@numItemCode)
													begin	
														insert into Temp_Cheking select 'Insert Amount Match: BizDoc Opp Items - ' + cast(@numOppBizDocItemID as varchar(50));														
														insert into OpportunityItems (numOppId,numItemCode,numUnitHour,monPrice,monTotAmount,vcItemDesc,vcType,vcAttributes,bitDropShip)
														select @numNewOppId,@numItemCode,numUnitHour,@monCost,@monCost*numUnitHour,vcItemDesc,vcType,vcAttributes,bitDropShip
														from OpportunityBizDocItems where numOppBizDocItemID=@numOppBizDocItemID;
													end
											end
								
							
							end

------------------- END OF AMOUTN NOT MATCH

--------------- INSERTING INTO BIZDOC AND BIZDOC ITEMS-----------------------

if @numNewOppId>0
					begin
					select @TotalAmount=sum(monTotAmount)from OpportunityItems where numOppId=@numNewOppId                                                                          
					update OpportunityMaster set  monPamount=@TotalAmount ,tintOppStatus=1                                                         
					where numOppId=@numNewOppId  
					
					

				if not exists (select numOppId from OpportunityBizDocs where numOppId=@numNewOppId)
					begin

						

							select @numBizDocId =numAuthoritativePurchase from AuthoritativeBizDocs where numDomainID=@numDomainId
						
							exec USP_CreateBizDocs
							 @numOppId =@numNewOppId,                        
							 @numBizDocId =@numBizDocId,                        
							 @numUserCntID=@numUserCntID,                        
							 @numOppBizDocsId =0,
							 @vcComments ='',
							 @bitPartialFulfillment =0,
							 @strBizDocItems ='' ,
							 @bitDiscountType =0,
							 @fltDiscount  =0,
							 --@monShipCost =0,
							 @numShipVia =0,
							 @vcTrackingURL ='',
							 @bitBillingTerms =0,
							 @intBillingDays =0,
							 @bitInterestType =0,
							 @fltInterest =0,
							 @numShipDoc =0,
							 @numBizDocStatus =0,
							 @bitRecurringBizDoc = 0,
							 @vcBizDocName  = ''
					end
				else
					begin
						SET @numOldBizDocId=0;
						

						select @numOldBizDocId=B.numOppBizDocsID  from OpportunityBizDocItems A inner join OpportunityBizDocs B on A.numOppBizDocId=B.numOppBizDocsID
						where numItemCode=@numItemCode AND B.numOppId=@numNewOppId
						
						select @numOppBizDocID = numOppBizDocsID from OpportunityBizDocs where numOppId=@numNewOppId;

						if @numOldBizDocId=0
						
						begin


						insert into Temp_Cheking select 'Insert BizDocs 2nd Item - '
								insert into                       
							   OpportunityBizDocItems                                                                          
  							   (numOppBizDocID,numOppItemID,numItemCode,numUnitHour,monPrice,monTotAmount,vcItemDesc,numWarehouseItmsID,vcType,vcAttributes,bitDropShip,bitDiscountType,fltDiscount,monTotAmtBefDiscount)
							   select @numOppBizDocID,numoppitemtCode,numItemCode,CASE 0 WHEN 1 THEN 1 ELSE numUnitHour END AS numUnitHour ,monPrice,CASE 0 WHEN 1 THEN monPrice * 1 ELSE monTotAmount END AS monTotAmount,vcItemDesc,numWarehouseItmsID,vcType,vcAttributes,bitDropShip,bitDiscountType,fltDiscount,monTotAmtBefDiscount from OpportunityItems OI
							   where  numOppId=@numNewOppId AND numItemCode=@numItemCode;						
						end	
					end
				end
------------------------------------- END OF BIZDOC ITEM INSERT -------------------
------------------------------------- START AMOUNT NOT MATCH -------------------
			insert into Temp_Cheking select 'Insert Amount Not Match'
			set @numDivisionId=0

			select @numDivisionId=DV.numDivisionId,
			@vcCompanyName=vcCompanyName,@numUserCntID=B.numCreatedBy,@numContactId=AC.numContactId,
			@monCost=monCost
			from OpportunityMaster A
			inner join OpportunityBizDocs B 
			on B.numOppID=A.numOppID
			--inner join 
			--View_BizDoc VB on VB.numOppBizDocsId=B.numOppBizDocsId
			inner join (select * from Item D where d.numItemCode=@numItemCode) D
			on d.numItemCode=@numItemCode
			inner join vendor E 
			on E.numVendorId=D.numVendorId and E.numItemCode=D.numItemCode
			inner join
			OrderAutoRule F
			on F.numDomainID=A.numDomainID and f.btActive=1--and F.numBillToId=A.tintBillToType and F.numShipToID=A.tintShipToType
			inner join DivisionMaster DV
			on DV.numDivisionId = E.numVendorId 
			inner join CompanyInfo CF 
			on Cf.numCompanyId=DV.numCompanyId inner join 
			AdditionalContactsInformation AC 
			on AC.numDivisionId=DV.numDivisionID and ISNULL(AC.bitPrimaryContact,0)=1
			where A.numOppID=@numParentOppID and D.numItemCode = @numItemCode and 
			A.numDomainID=@numDomainID and D.charItemType='P' and 
			btFullPaid=0 and
			B.numBizDocStatus=(case isnull(F.numBizDocStatus,0) when  0 then B.numBizDocStatus else F.numBizDocStatus end) and 
			AC.numDomainId=@numDomainId and
			b.numOppBizDocsID=@numOppBizDocID

			insert into Temp_Cheking select 'Insert Amount Not Match ' + cast(@numDivisionId as varchar(100));

			set @numNewOppId=0;

			Select @numNewOppId=OM.numOppId  from OpportunityLinking OL inner join 
						OpportunityMaster OM on OM.numOppId= OL.numChildOppID
			 where numParentOppBizDocID=@numOppBizDocID and numParentOppID=@numParentOppID and 
				   OM.numDomainId=@numDomainId and numDivisionId=@numDivisionId;


			


			set @vcPOppName=''
			if @numDivisionId>0
				begin
					SET @vcPOppName=@vcCompanyName + '-PO-' + datename(Month,GETUTCDATE());
					if @numNewOppId=0
						begin
							EXEC USP_OppManage
							@numNewOppId OUTPUT,
							@numContactId =@numContactId,                                                                          
							@numDivisionId =@numDivisionId,                                                                          
							@tintSource=0,                                                                         
							@vcPOppName =@vcPOppName,                                                                          
							--@Comments ='',                                                                          
							--@bitPublicFlag =0,                                                                          
							@numUserCntID =@numUserCntID,                                                                                 
							--@monPAmount  =0,                                                 
							--@numAssignedTo =0,                                                                                                        
							@numDomainId =@numDomainID,                                                                                                                                                                                   
							--@strItems =null,                                                                          
							--@strMilestone =null,                                                                          
							@dtEstimatedCloseDate =@CloseDate,                                                                          
							@CampaignID =0,
							@lngPConclAnalysis =0,
							@tintOppType =2,                                                                                                                                             
							@tintActive =1,                                                              
							@numSalesOrPurType =0,             
							@numRecurringId=0,
							@numCurrencyID =@numCurrencyCode,
							--@DealStatus as tinyint =0,-- 0=Open,1=Won,2=Lost
							@numStatus =0,
							@vcRefOrderNo =''
							--@numBillAddressId =0,
							--@numShipAddressId =0  
							
							--update temp_autoOrder set numNewOPpId =@numNewOppId;
							
							insert into OpportunityLinking (numParentOppID,numChildOppID,numParentOppBizDocID) values
							(@numParentOppID,@numNewOppId,@numOppBizDocID)
						end
							if @numNewOppId>0 
									begin
									if not exists (select numItemCode from OpportunityItems where numOppId=@numNewOppId and numItemCode=@numItemCode)
										begin															
											insert into OpportunityItems (numOppId,numItemCode,numUnitHour,monPrice,monTotAmount,vcItemDesc,vcType,vcAttributes,bitDropShip)
											select @numNewOppId,@numItemCode,numUnitHour,@monCost,@monCost*numUnitHour,vcItemDesc,vcType,vcAttributes,bitDropShip
											from OpportunityBizDocItems where numOppBizDocItemID=@numOppBizDocItemID;
										end
								end
				end
------------------- END OF AMOUNT NOT MATCH------------------------------------------

if @numNewOppId>0
					begin
					select @TotalAmount=sum(monTotAmount)from OpportunityItems where numOppId=@numNewOppId                                                                          
					update OpportunityMaster set  monPamount=@TotalAmount ,tintOppStatus=1                                                         
					where numOppId=@numNewOppId  
					
					

				if not exists (select numOppId from OpportunityBizDocs where numOppId=@numNewOppId)
					begin

						

							select @numBizDocId =numAuthoritativePurchase from AuthoritativeBizDocs where numDomainID=@numDomainId
						
							exec USP_CreateBizDocs
							 @numOppId =@numNewOppId,                        
							 @numBizDocId =@numBizDocId,                        
							 @numUserCntID=@numUserCntID,                        
							 @numOppBizDocsId =0,
							 @vcComments ='',
							 @bitPartialFulfillment =0,
							 @strBizDocItems ='' ,
							 @bitDiscountType =0,
							 @fltDiscount  =0,
							 --@monShipCost =0,
							 @numShipVia =0,
							 @vcTrackingURL ='',
							 @bitBillingTerms =0,
							 @intBillingDays =0,
							 @bitInterestType =0,
							 @fltInterest =0,
							 @numShipDoc =0,
							 @numBizDocStatus =0,
							 @bitRecurringBizDoc = 0,
							 @vcBizDocName  = ''
					end
				else
					begin
						SET @numOldBizDocId=0;
						

						select @numOldBizDocId=B.numOppBizDocsID  from OpportunityBizDocItems A inner join OpportunityBizDocs B on A.numOppBizDocId=B.numOppBizDocsID
						where numItemCode=@numItemCode AND B.numOppId=@numNewOppId
						
						select @numOppBizDocID = numOppBizDocsID from OpportunityBizDocs where numOppId=@numNewOppId;

						if @numOldBizDocId=0
						
						begin


						insert into Temp_Cheking select 'Insert BizDocs 2nd Item - '
								insert into                       
							   OpportunityBizDocItems                                                                          
  							   (numOppBizDocID,numOppItemID,numItemCode,numUnitHour,monPrice,monTotAmount,vcItemDesc,numWarehouseItmsID,vcType,vcAttributes,bitDropShip,bitDiscountType,fltDiscount,monTotAmtBefDiscount)
							   select @numOppBizDocID,numoppitemtCode,numItemCode,CASE 0 WHEN 1 THEN 1 ELSE numUnitHour END AS numUnitHour ,monPrice,CASE 0 WHEN 1 THEN monPrice * 1 ELSE monTotAmount END AS monTotAmount,vcItemDesc,numWarehouseItmsID,vcType,vcAttributes,bitDropShip,bitDiscountType,fltDiscount,monTotAmtBefDiscount from OpportunityItems OI
							   where  numOppId=@numNewOppId AND numItemCode=@numItemCode;
						
						end	
					end
				end
-------------------------------------------
---======================================================

END -- THE FINAL END