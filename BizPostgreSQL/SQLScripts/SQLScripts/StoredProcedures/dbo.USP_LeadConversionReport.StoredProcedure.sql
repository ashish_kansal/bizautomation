/****** Object:  StoredProcedure [dbo].[USP_LeadConversionReport]    Script Date: 07/26/2008 16:19:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Create by Siva           
             
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_leadconversionreport')
DROP PROCEDURE usp_leadconversionreport
GO
CREATE PROCEDURE [dbo].[USP_LeadConversionReport]                    
@numUserCntID numeric=0,               
@numDomainID numeric=0,              
@dtStartDate datetime,                                        
@dtEndDate datetime,                     
@tintType numeric,                     
--@tintRights tinyint,                              
--@tintLeadConversion as tinyint,                    
@numLeadGroupId as tinyint                    
As                     
Begin                    
                    
Declare @numTotalLeadAssigned as numeric                    
Declare @numTotalLeadAccountAssigned as numeric                    
Declare @numTotalLeadOwned as numeric                    
Declare @numTotalLeadAccountOwned as numeric                    
Declare @numTotalLeadAmountAssigned as numeric                    
Declare @numTotalLeadAmountOwned as numeric  
Declare @numTotalLeadAmount as numeric                   
          
If @tintType=1           
Begin          
          
If @numLeadGroupId<>0               
Begin              
 --Total Leads Assigned                     
                    
Select @numTotalLeadAssigned=count(*) From divisionmaster DM                    
Where DM.numDomainID=@numDomainID          
And DM.numGrpId=@numLeadGroupId and (tintCRMType=0 or (tintCRMType=1 and  DM.bintLeadPromBy is not null) or 
(tintCRMType=2 and  DM.bintLeadPromBy is not null and bintProsPromBy is not null)) 
And DM.numassignedto in(select numContactID from  AdditionalContactsInformation                    
where numTeam in(select numTeam from ForReportsByTeam where numUserCntID=@numUserCntID and tintType=34))        
And DM.bintCreatedDate  between @dtStartDate And @dtEndDate           
          
                     
                    
---Total Leads Converted Into Prospects (Of those Leads Assigned)                     
                    
                    
Select @numTotalLeadAccountAssigned=count(*) From DivisionMaster DM                     
Where DM.bintLeadPromBy is Not Null And DM.tintCRMType=1           
And DM.numDomainID=@numDomainID And DM.numGrpId =@numLeadGroupId          
And DM.numassignedto  in(select numContactID from  AdditionalContactsInformation                    
where numTeam in(select numTeam from ForReportsByTeam where numUserCntID=@numUserCntID and tintType=34)) 
And DM.bintCreatedDate between @dtStartDate And @dtEndDate                     
                    
--Total Leads Owned                     
                    
Select @numTotalLeadOwned=count(*) From divisionmaster DM                    
Where DM.numDomainID=@numDomainID and (tintCRMType=0 or (tintCRMType=1 and  DM.bintLeadPromBy is not null) or 
(tintCRMType=2 and  DM.bintLeadPromBy is not null and bintProsPromBy is not null))         
And DM.numGrpId =@numLeadGroupId And DM.numRecOwner 
in(select numContactID from  AdditionalContactsInformation                    
where numTeam in(select numTeam from ForReportsByTeam where numUserCntID=@numUserCntID and tintType=34))                
And DM.bintCreatedDate  between @dtStartDate And @dtEndDate                     
                    
                    
--Total Leads Converted Into Prospects (Of Leads Owned)                    
                    
Select @numTotalLeadAccountOwned=count(*) From divisionmaster DM                    
Where  DM.bintLeadPromBy is not null And DM.tintCRMType=1           
And DM.numDomainID=@numDomainID And DM.numGrpId =@numLeadGroupId               
And DM.numRecOwner  in(select numContactID from  AdditionalContactsInformation                    
where numTeam in(select numTeam from ForReportsByTeam where numUserCntID=@numUserCntID and tintType=34)) 
And DM.bintCreatedDate between @dtStartDate And @dtEndDate                     
                    
--Total Amount Won By Total Lead Assigned                    
                    
Select @numTotalLeadAmountAssigned=isnull(sum(monpAmount),0) From OpportunityMaster                    
Where tintOppStatus=0 And tintOppType=1 And numDivisionId in (Select numDivisionID                    
From DivisionMaster Where  bintLeadPromBy is not null And tintCRMType=1          
And numDomainID=@numDomainID And numGrpId =@numLeadGroupId And numassignedto in(select numContactID from  AdditionalContactsInformation                    
where numTeam in(select numTeam from ForReportsByTeam where numUserCntID=@numUserCntID and tintType=34))            
And bintCreatedDate  between @dtStartDate And @dtEndDate)                    
                    
                           
--Total Amount Won by Total Lead Owned                    
                    
Select @numTotalLeadAmountOwned=isnull(sum(monpAmount),0) From OpportunityMaster                    
Where tintOppStatus=0 And tintOppType=1 And numDivisionId in (Select numDivisionID                    
From DivisionMaster Where bintLeadPromBy is not null And  tintCRMType=1           
And numDomainID=@numDomainID And numGrpId =@numLeadGroupId          
And numRecOwner  in(select numContactID from  AdditionalContactsInformation                    
where numTeam in(select numTeam from ForReportsByTeam where numUserCntID=@numUserCntID and tintType=34)) 
And bintCreatedDate between @dtStartDate And @dtEndDate )                    
     
  
                         
--Total Amount in Pipeline                 
                    
Select @numTotalLeadAmount=isnull(sum(monpAmount),0) From OpportunityMaster                    
Where tintOppStatus=0 And tintOppType=1 And numDivisionId in (Select numDivisionID                    
From DivisionMaster Where bintLeadPromBy is not null And  tintCRMType=1           
And numDomainID=@numDomainID And numGrpId =@numLeadGroupId          
And (numRecOwner  in(select numContactID from  AdditionalContactsInformation                    
where numTeam in(select numTeam from ForReportsByTeam where numUserCntID=@numUserCntID and tintType=34)) 
Or numassignedto in (select numContactID from  AdditionalContactsInformation                    
where numTeam in(select numTeam from ForReportsByTeam where numUserCntID=@numUserCntID and tintType=34)))
And bintCreatedDate between @dtStartDate And @dtEndDate )     
                   
Select                     
@numTotalLeadAssigned 'TotalLeadAssigned',                    
@numTotalLeadOwned 'TotalLeadOwned',                    
@numTotalLeadAccountAssigned 'TotalLeadAccountAssigned',                    
@numTotalLeadAccountOwned 'TotalLeadAccountOwned',                    
@numTotalLeadAmountAssigned 'TotalLeadAmountAssigned',                    
@numTotalLeadAmountOwned 'TotalLeadAmountOwned',  
@numTotalLeadAmount 'TotalLeadAmount'  
         
END                  
              
if @numLeadGroupId=0               
Begin              
 --Total Leads Assigned                     
                    
Select @numTotalLeadAssigned=count(*) From divisionmaster DM                    
Where DM.numDomainID=@numDomainID           
And DM.numassignedto in(select numContactID from  AdditionalContactsInformation                    
where numTeam in(select numTeam from ForReportsByTeam where numUserCntID=@numUserCntID and tintType=34)) 
and (tintCRMType=0 or (tintCRMType=1 and  DM.bintLeadPromBy is not null) or (tintCRMType=2 and  DM.bintLeadPromBy is not null and bintProsPromBy is not null))             
And DM.bintCreatedDate  between @dtStartDate And @dtEndDate                      
                    
---Total Leads Converted Into Prospects (Of those Leads Assigned)                     
                    
                    
Select @numTotalLeadAccountAssigned=count(*) From DivisionMaster DM                     
Where DM.bintLeadPromBy is not null And DM.tintCRMType=1 And           
DM.numDomainID=@numDomainID And DM.numassignedto in(select numContactID from  AdditionalContactsInformation                    
where numTeam in(select numTeam from ForReportsByTeam where numUserCntID=@numUserCntID and tintType=34))             
And DM.bintCreatedDate  between @dtStartDate And @dtEndDate                     
                    
--Total Leads Owned                     
                    
Select @numTotalLeadOwned=count(*) From divisionmaster DM                    
Where DM.numDomainID=@numDomainID  and (tintCRMType=0 or (tintCRMType=1 and  DM.bintLeadPromBy is not null) or (tintCRMType=2 and  DM.bintLeadPromBy is not null and bintProsPromBy is not null))        
And DM.numRecOwner in (select numContactID from  AdditionalContactsInformation                    
where numTeam in(select numTeam from ForReportsByTeam where numUserCntID=@numUserCntID and tintType=34))               
And DM.bintCreatedDate  between @dtStartDate And @dtEndDate                     
                    
                    
--Total Leads Converted Into Prospect (Of Leads Owned)                    
                    
Select @numTotalLeadAccountOwned=count(*) From divisionmaster DM                    
Where DM.bintLeadPromBy is not null And DM.tintCRMType=1 And DM.numDomainID=@numDomainID             
And DM.numRecOwner in(select numContactID from  AdditionalContactsInformation                    
where numTeam in(select numTeam from ForReportsByTeam where numUserCntID=@numUserCntID and tintType=34)) 
And DM.bintCreatedDate between @dtStartDate And @dtEndDate                     
                    
--Total Amount Won By Total Lead Assigned                    
                    
Select @numTotalLeadAmountAssigned=isnull(sum(monpAmount),0) From OpportunityMaster                    
Where tintOppStatus=0 And tintOppType=1 And numDivisionId in (Select numDivisionID                    
From DivisionMaster Where bintLeadPromBy Is Not Null And tintCRMType=1           
AND numDomainID=@numDomainID And numassignedto in(select numContactID from  AdditionalContactsInformation                    
where numTeam in(select numTeam from ForReportsByTeam where numUserCntID=@numUserCntID and tintType=34))            
And bintCreatedDate Between @dtStartDate And @dtEndDate)                    
                    
                    
                    
--Total Amount Won by Total Lead Owned                    
                    
Select @numTotalLeadAmountOwned=isnull(sum(monpAmount),0) From OpportunityMaster          
Where tintOppStatus=0 And tintOppType=1 And numDivisionId in (Select numDivisionID                    
From DivisionMaster Where bintLeadPromBy is not null And tintCRMType=1          
And numDomainID=@numDomainID And numRecOwner in(select numContactID from  AdditionalContactsInformation                    
where numTeam in(select numTeam from ForReportsByTeam where numUserCntID=@numUserCntID and tintType=34))             
And bintCreatedDate between @dtStartDate And @dtEndDate )   
  
--  Total Amount in Pipeline   
  
Select @numTotalLeadAmount=isnull(sum(monpAmount),0) From OpportunityMaster                    
Where tintOppStatus=0 And tintOppType=1 And numDivisionId in (Select numDivisionID                    
From DivisionMaster Where bintLeadPromBy is not null And tintCRMType=1          
And numDomainID=@numDomainID And (numRecOwner in(select numContactID from  AdditionalContactsInformation                    
where numTeam in(select numTeam from ForReportsByTeam where numUserCntID=@numUserCntID and tintType=34))  or numAssignedTo in(select numContactID from  AdditionalContactsInformation                    
where numTeam in(select numTeam from ForReportsByTeam where numUserCntID=@numUserCntID and tintType=34)))  
And bintCreatedDate between @dtStartDate And @dtEndDate)   
                    
                      
Select                     
@numTotalLeadAssigned 'TotalLeadAssigned',                    
@numTotalLeadOwned 'TotalLeadOwned',                    
@numTotalLeadAccountAssigned 'TotalLeadAccountAssigned',                    
@numTotalLeadAccountOwned 'TotalLeadAccountOwned',                    
@numTotalLeadAmountAssigned 'TotalLeadAmountAssigned',                    
@numTotalLeadAmountOwned 'TotalLeadAmountOwned',  
@numTotalLeadAmount 'TotalLeadAmount'  
                    
END                 
              
End          
          
If @tintType=2                    
                 
Begin                  
if @numLeadGroupId<>0               
Begin              
 --Total Leads Assigned                     
                    
Select @numTotalLeadAssigned=count(*) From divisionmaster DM                    
Where DM.numDomainID=@numDomainID And DM.numGrpId=@numLeadGroupId
And (tintCRMType=0 or (tintCRMType=1 and  DM.bintLeadPromBy is not null) or (tintCRMType=2 and  DM.bintLeadPromBy is not null and bintProsPromBy is not null))               
And DM.numassignedto in(select numContactID from  AdditionalContactsInformation                    
where numTeam in(select numTeam from ForReportsByTeam where numUserCntID=@numUserCntID and tintType=34))
And DM.bintCreatedDate  Between @dtStartDate And @dtEndDate           
          
                     
                    
---Total Leads Converted Into Accounts (Of those Leads Assigned)                     
                    
                    
Select @numTotalLeadAccountAssigned=count(*) From DivisionMaster DM                     
Where DM.bintLeadPromBy is Not Null AND DM.tintCRMType=2 And DM.numDomainID=@numDomainID           
And DM.numGrpId=@numLeadGroupId AND DM.numassignedto in(select numContactID from  AdditionalContactsInformation                    
where numTeam in(select numTeam from ForReportsByTeam where numUserCntID=@numUserCntID and tintType=34))           
And DM.bintCreatedDate Between @dtStartDate And @dtEndDate                     
                    
--Total Leads Owned                     
                    
Select @numTotalLeadOwned=count(*) From divisionmaster DM                    
Where DM.numDomainID=@numDomainID and (tintCRMType=0 or (tintCRMType=1 and  DM.bintLeadPromBy is not null) or (tintCRMType=2 and  DM.bintLeadPromBy is not null and bintProsPromBy is not null))        
And DM.numGrpId =@numLeadGroupId And DM.numRecOwner in(select numContactID from  AdditionalContactsInformation                    
where numTeam in(select numTeam from ForReportsByTeam where numUserCntID=@numUserCntID and tintType=34))                
And DM.bintCreatedDate  between @dtStartDate And @dtEndDate                     
                    
                    
--Total Leads Converted Into Accounts (Of Leads Owned)             
                    
Select @numTotalLeadAccountOwned=count(*) From divisionmaster DM                    
Where DM.bintLeadPromBy is not null And DM.tintCRMType=2 And DM.numDomainID=@numDomainID           
And DM.numGrpId =@numLeadGroupId And DM.numRecOwner in(select numContactID from  AdditionalContactsInformation                    
where numTeam in(select numTeam from ForReportsByTeam where numUserCntID=@numUserCntID and tintType=34))            
And DM.bintCreatedDate between @dtStartDate And @dtEndDate                     
                    
--Total Amount Won By Total Lead Assigned                    
                    
Select @numTotalLeadAmountAssigned=isnull(sum(monpAmount),0) From OpportunityMaster                    
Where tintOppStatus=1 And tintOppType=1 And numDivisionId in (Select numDivisionID                    
From DivisionMaster Where bintLeadPromBy is not null And tintCRMType=2          
And numDomainID=@numDomainID And numGrpId =@numLeadGroupId             
And numassignedto in(select numContactID from  AdditionalContactsInformation                    
where numTeam in(select numTeam from ForReportsByTeam where numUserCntID=@numUserCntID and tintType=34))            
And bintCreatedDate  between @dtStartDate And @dtEndDate)                    
                    
                           
--Total Amount Won by Total Lead Owned                    
                    
Select @numTotalLeadAmountOwned=isnull(sum(monpAmount),0) From OpportunityMaster                    
Where tintOppStatus=1 And tintOppType=1 And numDivisionId in (Select numDivisionID                    
From DivisionMaster Where bintLeadPromBy is not null And tintCRMType=2           
And numDomainID=@numDomainID And numGrpId =@numLeadGroupId          
And numRecOwner  in(select numContactID from  AdditionalContactsInformation                    
where numTeam in(select numTeam from ForReportsByTeam where numUserCntID=@numUserCntID and tintType=34)) 
And bintCreatedDate between @dtStartDate And @dtEndDate )                    
            
                 
--Total Amount Won                     
                    
Select @numTotalLeadAmount=isnull(sum(monpAmount),0) From OpportunityMaster                    
Where tintOppStatus=1 And tintOppType=1 And numDivisionId in (Select numDivisionID                    
From DivisionMaster Where bintLeadPromBy is not null And tintCRMType=2           
And numDomainID=@numDomainID And numGrpId =@numLeadGroupId          
And (numRecOwner in(select numContactID from  AdditionalContactsInformation                    
where numTeam in(select numTeam from ForReportsByTeam where numUserCntID=@numUserCntID and tintType=34))
Or numassignedto in(select numContactID from  AdditionalContactsInformation                    
where numTeam in(select numTeam from ForReportsByTeam where numUserCntID=@numUserCntID and tintType=34))) 
And bintCreatedDate between @dtStartDate And @dtEndDate)    
            
Select                     
@numTotalLeadAssigned 'TotalLeadAssigned',                    
@numTotalLeadOwned 'TotalLeadOwned',                    
@numTotalLeadAccountAssigned 'TotalLeadAccountAssigned',                    
@numTotalLeadAccountOwned 'TotalLeadAccountOwned',                    
@numTotalLeadAmountAssigned 'TotalLeadAmountAssigned',                    
@numTotalLeadAmountOwned 'TotalLeadAmountOwned',  
@numTotalLeadAmount 'TotalLeadAmount'  
                    
END                  
              
if @numLeadGroupId=0               
Begin              
 --Total Leads Assigned                     
                    
Select @numTotalLeadAssigned=count(*) From divisionmaster DM                    
Where DM.numDomainID=@numDomainID And DM.numassignedto in(select numContactID from  AdditionalContactsInformation                    
where numTeam in(select numTeam from ForReportsByTeam where numUserCntID=@numUserCntID and tintType=34)) 
And (tintCRMType=0 or (tintCRMType=1 and  DM.bintLeadPromBy is not null) or (tintCRMType=2 and  DM.bintLeadPromBy is not null and bintProsPromBy is not null))             
And DM.bintCreatedDate  between @dtStartDate And @dtEndDate                      
                    
---Total Leads Converted Into Accounts (Of those Leads Assigned)                     
                    
                    
Select @numTotalLeadAccountAssigned=count(*) From DivisionMaster DM                     
Where DM.bintLeadPromBy is not null And DM.tintCRMType=2           
And DM.numDomainID=@numDomainID And DM.numassignedto in(select numContactID from  AdditionalContactsInformation                    
where numTeam in(select numTeam from ForReportsByTeam where numUserCntID=@numUserCntID and tintType=34))               
And DM.bintCreatedDate  Between @dtStartDate And @dtEndDate                     
                    
--Total Leads Owned                     
                    
Select @numTotalLeadOwned=count(*) From divisionmaster DM                    
Where DM.numDomainID=@numDomainID And DM.numRecOwner in(select numContactID from  AdditionalContactsInformation                    
where numTeam in(select numTeam from ForReportsByTeam where numUserCntID=@numUserCntID and tintType=34))  
And (tintCRMType=0 or (tintCRMType=1 and  DM.bintLeadPromBy is not null) or (tintCRMType=2 and  DM.bintLeadPromBy is not null and bintProsPromBy is not null))             
And DM.bintCreatedDate  between @dtStartDate And @dtEndDate                     
                    
                    
--Total Leads Converted Into Accounts (Of Leads Owned)                    
                    
Select @numTotalLeadAccountOwned=count(*) From divisionmaster DM                    
Where DM.numDomainID=@numDomainID And DM.bintLeadPromBy is not null And DM.tintCRMType=2               
And DM.numRecOwner in(select numContactID from  AdditionalContactsInformation                    
where numTeam in(select numTeam from ForReportsByTeam where numUserCntID=@numUserCntID and tintType=34))
And DM.bintCreatedDate between @dtStartDate And @dtEndDate                     
                    
--Total Amount Won By Total Lead Assigned                    
                    
Select @numTotalLeadAmountAssigned=isnull(sum(monpAmount),0) From OpportunityMaster                    
Where numDivisionId in (Select numDivisionID From DivisionMaster          
Where numDomainID=@numDomainID And numassignedto in(select numContactID from  AdditionalContactsInformation                    
where numTeam in(select numTeam from ForReportsByTeam where numUserCntID=@numUserCntID and tintType=34))            
And bintLeadPromBy is not null And tintCRMType=2            
And bintCreatedDate between @dtStartDate And @dtEndDate)                    
                    
                    
                    
--Total Amount Won by Total Lead Owned                    
                    
Select @numTotalLeadAmountOwned=isnull(sum(monpAmount),0) From OpportunityMaster                    
Where numDivisionId in (Select numDivisionID From DivisionMaster          
Where numDomainID=@numDomainID And numRecOwner  in(select numContactID from  AdditionalContactsInformation                    
where numTeam in(select numTeam from ForReportsByTeam where numUserCntID=@numUserCntID and tintType=34))          
And bintLeadPromBy is Not Null And tintCRMType=2              
And bintCreatedDate between @dtStartDate And @dtEndDate )       
  
  
--  Total Amount Won   
  
  
Select @numTotalLeadAmount=isnull(sum(monpAmount),0) From OpportunityMaster                    
Where numDivisionId in (Select numDivisionID From DivisionMaster          
Where numDomainID=@numDomainID And (numRecOwner  in(select numContactID from  AdditionalContactsInformation                    
where numTeam in(select numTeam from ForReportsByTeam where numUserCntID=@numUserCntID and tintType=34)) 
Or numassignedto in(select numContactID from  AdditionalContactsInformation                    
where numTeam in(select numTeam from ForReportsByTeam where numUserCntID=@numUserCntID and tintType=34)))     
And bintLeadPromBy is Not Null And tintCRMType=2              
And bintCreatedDate between @dtStartDate And @dtEndDate )       
  
               
                      
Select                     
@numTotalLeadAssigned 'TotalLeadAssigned',                    
@numTotalLeadOwned 'TotalLeadOwned',                    
@numTotalLeadAccountAssigned 'TotalLeadAccountAssigned',                    
@numTotalLeadAccountOwned 'TotalLeadAccountOwned',                    
@numTotalLeadAmountAssigned 'TotalLeadAmountAssigned',                    
@numTotalLeadAmountOwned 'TotalLeadAmountOwned',  
@numTotalLeadAmount 'TotalLeadAmount'                   
END                 
              
END                  
          
End
GO
