/****** Object:  StoredProcedure [dbo].[USP_GetDealsListForPortal]    Script Date: 09/10/2013  ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_GetDealsListForPortal' ) 
    DROP PROCEDURE USP_GetDealsListForPortal
GO
CREATE PROCEDURE [dbo].[USP_GetDealsListForPortal]
    @numUserCntID NUMERIC(9) = 0,
    @numDomainID NUMERIC(9) = 0,
    @tintSortOrder TINYINT = 4,
    @SortChar CHAR(1) = '0',
    @CurrentPage INT,
    @PageSize INT,
    @TotRecs INT OUTPUT,
    @columnName AS VARCHAR(50),
    @columnSortOrder AS VARCHAR(10),
    @ClientTimeZoneOffset AS INT,
    @tintShipped AS TINYINT,
    @intOrder AS TINYINT,
    @intType AS TINYINT,
    @tintFilterBy AS TINYINT = 0,
    @numOrderStatus AS NUMERIC,
    @vcRegularSearchCriteria VARCHAR(1000) = '',
    @vcCustomSearchCriteria VARCHAR(1000) = '',
    @numDivisionID AS NUMERIC(9) = 0
AS 
    DECLARE @PageId AS TINYINT
    DECLARE @numFormId AS INT 
  
    SET @PageId = 0
    IF @inttype = 1 
        BEGIN
            SET @PageId = 2
            SET @inttype = 3
            SET @numFormId = 78
        END
    IF @inttype = 2 
        BEGIN
            SET @PageId = 6
            SET @inttype = 4
            SET @numFormId = 79
        END
        
  --Create a Temporary table to hold data
    CREATE TABLE #tempTable
        (
          ID INT IDENTITY
                 PRIMARY KEY,
          numOppId NUMERIC(9),
          intTotalProgress INT
        )
    
    DECLARE @strShareRedordWith AS VARCHAR(300) ;
    SET @strShareRedordWith = ' 1=1 '
--    SET @strShareRedordWith = ' Opp.numOppId in (select SR.numRecordID FROM ShareRecord SR WHERE SR.numDomainID='
--        + CONVERT(VARCHAR(15), @numDomainID)
--        + ' AND SR.numModuleID = 3 AND SR.numAssignedTo = '
--        + CONVERT(VARCHAR(15), @numUserCntId) + ') '

    DECLARE @strSql AS VARCHAR(8000)
    SET @strSql = 'SELECT  Opp.numOppId,PP.intTotalProgress '
  
    DECLARE @strOrderColumn AS VARCHAR(100) ;
    SET @strOrderColumn = @columnName

    SET @strSql = @strSql
        + ' FROM OpportunityMaster Opp                                                               
                                 INNER JOIN AdditionalContactsInformation ADC ON Opp.numContactId = ADC.numContactId                                                               
                                 INNER JOIN DivisionMaster Div ON Opp.numDivisionId = Div.numDivisionID AND ADC.numDivisionId = Div.numDivisionID
								 INNER JOIN CompanyInfo cmp ON Div.numCompanyID = cmp.numCompanyId 
								 LEFT JOIN ProjectProgress PP On PP.numOppId = Opp.numOppId '
								 
      
    IF @columnName LIKE 'CFW.Cust%' 
        BEGIN            
            SET @strSql = @strSql
                + ' left Join CFW_Fld_Values_Opp CFW on CFW.RecId=Opp.numOppid  and CFW.fld_id= '
                + REPLACE(@columnName, 'CFW.Cust', '') 
            SET @strOrderColumn = 'CFW.Fld_Value'   
        END                                     
    ELSE 
        IF @columnName LIKE 'DCust%' 
            BEGIN            
                SET @strSql = @strSql
                    + ' left Join CFW_Fld_Values_Opp CFW on CFW.RecId=Opp.numOppid   and CFW.fld_id= '
                    + REPLACE(@columnName, 'DCust', '')                                                                                               
                SET @strSql = @strSql
                    + ' left Join ListDetails LstCF on LstCF.numListItemID=CFW.Fld_Value  '            
                SET @strOrderColumn = 'LstCF.vcData'  
            END       

             
    IF @tintFilterBy = 1 --Partially Fulfilled Orders 
        SET @strSql = @strSql
            + ' left join AdditionalContactsInformation ADC1 on ADC1.numContactId=Opp.numRecOwner                                                               
                             LEFT OUTER JOIN [OpportunityBizDocs] OB ON OB.[numOppId] = Opp.[numOppId] 
                             WHERE Opp.numDomainID='
            + CONVERT(VARCHAR(15), @numDomainID)
            + ' and Opp.tintOppstatus = 1 and Opp.tintShipped = '
            + CONVERT(VARCHAR(15), @tintShipped)
    ELSE 
        SET @strSql = @strSql
            + ' left join AdditionalContactsInformation ADC1 on ADC1.numContactId=Opp.numRecOwner                                                               
                             WHERE Opp.numDomainID='
            + CONVERT(VARCHAR(15), @numDomainID)
            + ' and Opp.tintOppstatus = 1 and Opp.tintShipped = '
            + CONVERT(VARCHAR(15), @tintShipped) 
     
    IF @intOrder <> 0 
        BEGIN
            IF @intOrder = 1 
                SET @strSql = @strSql + ' and Opp.bitOrder = 0 '
            IF @intOrder = 2 
                SET @strSql = @strSql + ' and Opp.bitOrder = 1'
        END
        
    if @numDivisionID <> 0 
		SET @strSql = @strSql + ' and Div.numDivisionID = ' + CONVERT(VARCHAR(15),@numDivisionID)                        
    
    IF @SortChar <> '0' 
        SET @strSql = @strSql + ' And Opp.vcPOppName like ''' + @SortChar
            + '%'''
 
   IF @tintSortOrder <> '0' 
        SET @strSql = @strSql + '  AND Opp.tintOppType = '
            + CONVERT(VARCHAR(1), @tintSortOrder)
    
    
    IF @tintFilterBy = 1 --Partially Fulfilled Orders
        SET @strSql = @strSql
            + ' AND OB.bitPartialFulfilment = 1 AND OB.bitAuthoritativeBizDocs = 1 '
    ELSE 
        IF @tintFilterBy = 2 --Fulfilled Orders
            SET @strSql = @strSql
                + ' AND Opp.[numOppId] IN (SELECT OM.numOppId FROM [OpportunityMaster] OM
                      			INNER JOIN [OpportunityBizDocs] OB ON OB.[numOppId] = OM.[numOppId]
                      			INNER JOIN [OpportunityItems] OI ON OM.[numOppId] = OI.[numOppId]
                      			INNER JOIN [OpportunityBizDocItems] BI ON BI.[numOppBizDocID] = OB.[numOppBizDocsId]
                      			Where OM.numDomainId='
                + CONVERT(VARCHAR(15), @numDomainId)
                + 'AND OB.[bitAuthoritativeBizDocs] = 1 GROUP BY OM.[numOppId]
                      			HAVING   COUNT(OI.[numUnitHour]) = COUNT(BI.[numUnitHour])) '
        ELSE 
            IF @tintFilterBy = 3 --Orders without Authoritative-BizDocs
                SET @strSql = @strSql
                    + ' AND Opp.[numOppId] not IN (SELECT DISTINCT OM.[numOppId]
                      		  FROM [OpportunityMaster] OM JOIN [OpportunityBizDocs] OB ON OM.[numOppId] = OB.[numOppId]
                      		  Where OM.numDomainId='
                    + CONVERT(VARCHAR(15), @numDomainId)
                    + ' AND OM.tintOppstatus=1 
                      		    AND OM.tintShipped = '
                    + CONVERT(VARCHAR(15), @tintShipped)
                    + ' AND OM.tintOppType = '
                    + CONVERT(VARCHAR(1), @tintSortOrder)
                    + ' AND isnull(OB.[bitAuthoritativeBizDocs],0)=1) '

            ELSE 
                IF @tintFilterBy = 4 --Orders with items yet to be added to Invoices
                    SET @strSql = @strSql
                        + ' AND Opp.[numOppId] IN (SELECT distinct OPPM.numOppId FROM [OpportunityMaster] OPPM
                                  INNER JOIN [OpportunityItems] OPPI ON OPPM.[numOppId] = OPPI.[numOppId]
                                  Where OPPM.numDomainId='
                        + CONVERT(VARCHAR(15), @numDomainId)
                        + ' 
								  and OPPI.numoppitemtCode not in (select numOppItemID from [OpportunityBizDocs] OB INNER JOIN [OpportunityBizDocItems] BI
                      			  ON BI.[numOppBizDocID] = OB.[numOppBizDocsId] where OB.[numOppId] = OPPM.[numOppId]))'      
                ELSE 
                    IF @tintFilterBy = 6 --Orders with deferred Income/Invoices
                        SET @strSql = @strSql
                            + ' AND Opp.[numOppId] IN (SELECT DISTINCT OM.[numOppId]
                      		 FROM [OpportunityMaster] OM LEFT OUTER JOIN [OpportunityBizDocs] OB ON OM.[numOppId] = OB.[numOppId]
                      		 Where OM.numDomainId='
                            + CONVERT(VARCHAR(15), @numDomainId)
                            + ' AND OB.[bitAuthoritativeBizDocs] =1 and OB.tintDeferred=1)'

    IF @numOrderStatus <> 0 
        SET @strSql = @strSql + ' AND Opp.numStatus = '
            + CONVERT(VARCHAR(15), @numOrderStatus) ;


    IF @vcRegularSearchCriteria <> '' 
        SET @strSql = @strSql + ' and ' + @vcRegularSearchCriteria 
	
    IF @vcCustomSearchCriteria <> '' 
        SET @strSql = @strSql
            + ' and Opp.numOppid in (select distinct CFW.RecId from CFW_Fld_Values_Opp CFW where '
            + @vcCustomSearchCriteria + ')'

    IF LEN(@strOrderColumn) > 0 
        SET @strSql = @strSql + ' ORDER BY  ' + @strOrderColumn + ' '
            + @columnSortOrder    

	
    PRINT @strSql
    INSERT  INTO #tempTable
            (
              numOppId,
              intTotalProgress
            )
            EXEC ( @strSql
                )
	
	DECLARE @firstRec AS INTEGER
    DECLARE @lastRec AS INTEGER
    SET @firstRec = ( @CurrentPage - 1 ) * @PageSize
    SET @lastRec = ( @CurrentPage * @PageSize + 1 )
  
    SET @TotRecs = ( SELECT COUNT(*)
                     FROM   #tempTable
                   )
  
    DECLARE @tintOrder AS TINYINT ;
    SET @tintOrder = 0
    DECLARE @vcFieldName AS VARCHAR(50)
    DECLARE @vcListItemType AS VARCHAR(3)
    DECLARE @vcListItemType1 AS VARCHAR(1)
    DECLARE @vcAssociatedControlType VARCHAR(30)
    DECLARE @numListID AS NUMERIC(9)
    DECLARE @vcDbColumnName VARCHAR(40)
    DECLARE @WhereCondition VARCHAR(2000) ;
    SET @WhereCondition = ''
    DECLARE @vcLookBackTableName VARCHAR(2000)
    DECLARE @bitCustom AS BIT
    DECLARE @numFieldId AS NUMERIC  
    DECLARE @bitAllowSorting AS CHAR(1)   
    DECLARE @bitAllowEdit AS CHAR(1)                 
    DECLARE @vcColumnName AS VARCHAR(500)                  
    DECLARE @Nocolumns AS TINYINT ;
    SET @Nocolumns = 0

    SELECT  @Nocolumns = ISNULL(SUM(TotalRow), 0)
    FROM    ( SELECT    COUNT(*) TotalRow
              FROM      View_DynamicColumns
              WHERE     numFormId = @numFormId
                        --AND numUserCntID = @numUserCntID
                        AND numDomainID = @numDomainID
                        AND tintPageType = 1
                        --AND numRelCntType = @numFormId
              UNION
              SELECT    COUNT(*) TotalRow
              FROM      View_DynamicCustomColumns
              WHERE     numFormId = @numFormId
                        --AND numUserCntID = @numUserCntID
                        AND numDomainID = @numDomainID
                        AND tintPageType = 1
                        --AND numRelCntType = @numFormId
            ) TotalRows

	PRINT '@Nocolumns: ' + CAST(@Nocolumns AS VARCHAR(10))
	
    CREATE TABLE #tempForm
        (
          tintOrder TINYINT,
          vcDbColumnName NVARCHAR(50),
          vcOrigDbColumnName NVARCHAR(50),
          vcFieldName NVARCHAR(50),
          vcAssociatedControlType NVARCHAR(50),
          vcListItemType VARCHAR(3),
          numListID NUMERIC(9),
          vcLookBackTableName VARCHAR(50),
          bitCustomField BIT,
          numFieldId NUMERIC,
          bitAllowSorting BIT,
          bitAllowEdit BIT,
          bitIsRequired BIT,
          bitIsEmail BIT,
          bitIsAlphaNumeric BIT,
          bitIsNumeric BIT,
          bitIsLengthValidation BIT,
          intMaxLength INT,
          intMinLength INT,
          bitFieldMessage BIT,
          vcFieldMessage VARCHAR(500),
          ListRelID NUMERIC(9),
          intColumnWidth INT,
          bitAllowFiltering BIT,
          vcFieldDataType CHAR(1)
        )
    
    SET @strSql = ''
    SET @strSql = 'select ADC.numContactId,DM.numDivisionID,isnull(DM.numTerID,0) numTerID,opp.numRecOwner as numRecOwner,DM.tintCRMType,opp.numOppId'

    IF @Nocolumns = 0 
        BEGIN
            INSERT  INTO DycFormConfigurationDetails
                    (
                      numFormID,
                      numFieldID,
                      intColumnNum,
                      intRowNum,
                      numDomainID,
                      numUserCntID,
                      numRelCntType,
                      tintPageType,
                      bitCustom,
                      intColumnWidth
                    )
                    SELECT  @numFormId,
                            numFieldId,
                            0,
                            Row_number() OVER ( ORDER BY tintRow DESC ),
                            @numDomainID,
                            0,
                            0,
                            1,
                            0,
                            intColumnWidth
                    FROM    View_DynamicDefaultColumns
                    WHERE   numFormId = @numFormId
                            AND bitDefault = 1
                            AND ISNULL(bitSettingField, 0) = 1
                            AND numDomainID = @numDomainID
                    ORDER BY tintOrder ASC   

        END

    INSERT  INTO #tempForm
            SELECT  tintRow + 1 AS tintOrder,
                    vcDbColumnName,
                    vcOrigDbColumnName,
                    ISNULL(vcCultureFieldName, vcFieldName),
                    vcAssociatedControlType,
                    vcListItemType,
                    numListID,
                    vcLookBackTableName,
                    bitCustom,
                    numFieldId,
                    bitAllowSorting,
                    bitInlineEdit,
                    bitIsRequired,
                    bitIsEmail,
                    bitIsAlphaNumeric,
                    bitIsNumeric,
                    bitIsLengthValidation,
                    intMaxLength,
                    intMinLength,
                    bitFieldMessage,
                    vcFieldMessage vcFieldMessage,
                    ListRelID,
                    intColumnWidth,
                    bitAllowFiltering,
                    vcFieldDataType
            FROM    View_DynamicColumns
            WHERE   numFormId = @numFormId
                    --AND numUserCntID = @numUserCntID
                    AND numDomainID = @numDomainID
                    AND tintPageType = 1
                    AND ISNULL(bitSettingField, 0) = 1
                    AND ISNULL(bitCustom, 0) = 0
                    --AND numRelCntType = @numFormId
            UNION
            SELECT  tintRow + 1 AS tintOrder,
                    vcDbColumnName,
                    vcDbColumnName,
                    vcFieldName,
                    vcAssociatedControlType,
                    '' AS vcListItemType,
                    numListID,
                    '',
                    bitCustom,
                    numFieldId,
                    bitAllowSorting,
                    bitAllowEdit,
                    bitIsRequired,
                    bitIsEmail,
                    bitIsAlphaNumeric,
                    bitIsNumeric,
                    bitIsLengthValidation,
                    intMaxLength,
                    intMinLength,
                    bitFieldMessage,
                    vcFieldMessage,
                    ListRelID,
                    intColumnWidth,
                    0,
                    ''
            FROM    View_DynamicCustomColumns
            WHERE   numFormId = @numFormId
                    --AND numUserCntID = @numUserCntID
                    AND numDomainID = @numDomainID
                    AND tintPageType = 1
                    AND ISNULL(bitCustom, 0) = 1
                    --AND numRelCntType = @numFormId
            ORDER BY tintOrder ASC  
    
    DECLARE @ListRelID AS NUMERIC(9) 

    SELECT TOP 1
            @tintOrder = tintOrder + 1,
            @vcDbColumnName = vcDbColumnName,
            @vcFieldName = vcFieldName,
            @vcAssociatedControlType = vcAssociatedControlType,
            @vcListItemType = vcListItemType,
            @numListID = numListID,
            @vcLookBackTableName = vcLookBackTableName,
            @bitCustom = bitCustomField,
            @numFieldId = numFieldId,
            @bitAllowSorting = bitAllowSorting,
            @bitAllowEdit = bitAllowEdit,
            @ListRelID = ListRelID
    FROM    #tempForm
--    ORDER BY tintOrder ASC            

	--SELECT TOP 1 * FROM    #tempForm ORDER BY tintOrder ASC            

    WHILE @tintOrder > 0
        BEGIN
            IF @bitCustom = 0 
                BEGIN
                    DECLARE @Prefix AS VARCHAR(5)
                    IF @vcLookBackTableName = 'AdditionalContactsInformation' 
                        SET @Prefix = 'ADC.'
                    IF @vcLookBackTableName = 'DivisionMaster' 
                        SET @Prefix = 'DM.'
                    IF @vcLookBackTableName = 'OpportunityMaster' 
                        SET @PreFix = 'Opp.'
                    IF @vcLookBackTableName = 'OpportunityRecurring' 
                        SET @PreFix = 'OPR.'
            
                    SET @vcColumnName = @vcDbColumnName + '~'
                        + CONVERT(VARCHAR(10), @numFieldId) + '~0'
            
                    IF @vcAssociatedControlType = 'SelectBox' 
                        BEGIN
                            IF @vcDbColumnName = 'tintSource' 
                                BEGIN
                                    SET @strSql = @strSql
                                        + ',ISNULL(dbo.fn_GetOpportunitySourceValue(ISNULL(Opp.tintSource,0),ISNULL(Opp.tintSourceType,0),Opp.numDomainID),''Internal Order'')'
                                        + ' [' + @vcColumnName + ']'
                                END
            
                            ELSE 
                                IF @vcDbColumnName = 'numCampainID' 
                                    BEGIN
                                        SET @strSql = @strSql
                                            + ', (SELECT vcCampaignName FROM CampaignMaster WHERE numCampaignID= Opp.numCampainID) '
                                            + ' [' + @vcColumnName + ']'
                                    END
            
                                ELSE 
                                    IF @vcListItemType = 'LI' 
                                        BEGIN
                                            SET @strSql = @strSql + ',L'
                                                + CONVERT(VARCHAR(3), @tintOrder)
                                                + '.vcData' + ' ['
                                                + @vcColumnName + ']'
                                            SET @WhereCondition = @WhereCondition
                                                + ' left Join ListDetails L'
                                                + CONVERT(VARCHAR(3), @tintOrder)
                                                + ' on L'
                                                + CONVERT(VARCHAR(3), @tintOrder)
                                                + '.numListItemID='
                                                + @vcDbColumnName
                                        END
                                    ELSE 
                                        IF @vcListItemType = 'S' 
                                            BEGIN
                                                SET @strSql = @strSql + ',S'
                                                    + CONVERT(VARCHAR(3), @tintOrder)
                                                    + '.vcState' + ' ['
                                                    + @vcColumnName + ']'
                                                SET @WhereCondition = @WhereCondition
                                                    + ' left join State S'
                                                    + CONVERT(VARCHAR(3), @tintOrder)
                                                    + ' on S'
                                                    + CONVERT(VARCHAR(3), @tintOrder)
                                                    + '.numStateID='
                                                    + @vcDbColumnName
                                            END
                                        ELSE 
                                            IF @vcListItemType = 'BP' 
                                                BEGIN
                                                    SET @strSql = @strSql
                                                        + ',SPLM.Slp_Name'
                                                        + ' [' + @vcColumnName
                                                        + ']'
                                                    SET @WhereCondition = @WhereCondition
                                                        + ' LEFT JOIN Sales_process_List_Master SPLM on SPLM.Slp_Id = OPP.numBusinessProcessID '
                                                END
                                            ELSE 
                                                IF @vcListItemType = 'PP' 
                                                    BEGIN
                                                        SET @strSql = @strSql
                                                            + ',ISNULL(T.intTotalProgress,0)'
                                                            + ' ['
                                                            + @vcColumnName
                                                            + ']'

                                                    END
                                                ELSE 
                                                    IF @vcListItemType = 'T' 
                                                        BEGIN
                                                            SET @strSql = @strSql + ',L' + CONVERT(VARCHAR(3), @tintOrder) + '.vcData' + ' [' + @vcColumnName + ']'
                                                            SET @WhereCondition = @WhereCondition + ' left Join ListDetails L' + CONVERT(VARCHAR(3), @tintOrder) + ' on L' + CONVERT(VARCHAR(3), @tintOrder) + '.numListItemID=' + @vcDbColumnName
                                                        END
                                                    ELSE 
                                                        IF @vcListItemType = 'U' 
                                                            BEGIN
                                                                SET @strSql = @strSql + ',dbo.fn_GetContactName(' + @Prefix + @vcDbColumnName + ') [' + @vcColumnName + ']'
                                                            END
                                                        ELSE 
                                                            IF @vcListItemType = 'WI' 
                                                                BEGIN
                                                                    SET @strSql = @strSql + ', Case when isnull(OL.numSiteID,0)>0 then Sites.vcSiteName else WebAPI.vcProviderName end [' + @vcColumnName + ']'
                                                                    SET @WhereCondition = @WhereCondition + ' left JOIN OpportunityLinking OL on OL.numChildOppID=Opp.numOppId left join WebAPI on WebAPI.WebApiId=isnull(OL.numWebApiId,0)
													left join Sites on isnull(OL.numSiteID,0)=Sites.numSIteID and Sites.numDomainID=' + CONVERT(VARCHAR(15), @numDomainID)                                                       
                                                                END 
                        END
                    ELSE 
                        IF @vcAssociatedControlType = 'DateField' 
                            BEGIN
                                IF @Prefix = 'OPR.' 
                                    BEGIN
                                        SET @strSql = @strSql
                                            + ', (SELECT TOP 1 dbo.FormatedDateFromDate( DateAdd(minute,+'
                                            + CONVERT(VARCHAR(15), -@ClientTimeZoneOffset)
                                            + ', dtRecurringDate)' + ','
                                            + CONVERT(VARCHAR(10), @numDomainId)
                                            + ') FROM  dbo.OpportunityRecurring WHERE numOppID=OPP.numOppID AND ISNULL(numOppBizDocID,0)=0 and dtRecurringDate > dbo.GetUTCDateWithoutTime()) AS  ['
                                            + @vcColumnName + ']'
                                    END
                                ELSE 
                                    BEGIN
                                        SET @strSql = @strSql
                                            + ',case when convert(varchar(11),DateAdd(minute, '
                                            + CONVERT(VARCHAR(15), -@ClientTimeZoneOffset)
                                            + ',' + @Prefix + @vcDbColumnName
                                            + ') )= convert(varchar(11),getdate()) then ''<b><font color=red>Today</font></b>'''
                                        SET @strSql = @strSql
                                            + 'when convert(varchar(11),DateAdd(minute, '
                                            + CONVERT(VARCHAR(15), -@ClientTimeZoneOffset)
                                            + ',' + @Prefix + @vcDbColumnName
                                            + ') )= convert(varchar(11),dateadd(day,-1,getdate())) then ''<b><font color=purple>YesterDay</font></b>'''
                                        SET @strSql = @strSql
                                            + 'when convert(varchar(11),DateAdd(minute, '
                                            + CONVERT(VARCHAR(15), -@ClientTimeZoneOffset)
                                            + ',' + @Prefix + @vcDbColumnName
                                            + ') )= convert(varchar(11),dateadd(day,1,getdate())) then''<b><font color=orange>Tommorow</font></b>'' '
                                        SET @strSql = @strSql
                                            + 'else dbo.FormatedDateFromDate('
                                            + @Prefix + @vcDbColumnName + ','
                                            + CONVERT(VARCHAR(10), @numDomainId)
                                            + ') end  [' + @vcColumnName + ']' 	
                                    END
                            END
                        ELSE 
                            IF @vcAssociatedControlType = 'TextBox' 
                                BEGIN
                                    SET @strSql = @strSql + ','
                                        + CASE WHEN @vcDbColumnName = 'numAge'
                                               THEN 'year(getutcdate())-year(bintDOB)'
                                               WHEN @vcDbColumnName = 'monPAmount'
                                               THEN 'isnull((SELECT varCurrSymbol FROM [Currency] WHERE numCurrencyID = Opp.[numCurrencyID]),'''') + '' ''+ CONVERT(VARCHAR,CONVERT(DECIMAL(20,5),monPAmount),1)'
                                               WHEN @vcDbColumnName = 'numDivisionID'
                                               THEN 'DM.numDivisionID'
                                               WHEN @vcDbColumnName = 'tintOppStatus'
                                               THEN 'CASE tintOppStatus WHEN 0 then ''Open'' WHEN 1 THEN ''Closed-Won'' WHEN 2 THEN ''Closed-Lost'' END '
                                               WHEN @vcDbColumnName = 'tintOppType'
                                               THEN 'CASE tintOppType WHEN 1 then ''Sales'' WHEN 2 THEN ''Purchase'' Else ''NA'' END '
                                               WHEN @vcDbColumnName = 'vcPOppName'
                                               THEN 'Opp.vcPOppName'
                                               ELSE @vcDbColumnName
                                          END + ' [' + @vcColumnName + ']'
                                END
                            ELSE 
                                IF @vcAssociatedControlType = 'TextArea' 
                                    BEGIN
                                        SET @strSql = @strSql + ',' + @Prefix
                                            + @vcDbColumnName + ' ['
                                            + @vcColumnName + ']'  
                                    END
                                ELSE 
                                    IF @vcAssociatedControlType = 'Label' 
                                        BEGIN  
                                            SET @strSql = @strSql + ','
                                                + CASE WHEN @vcDbColumnName = 'CalAmount'
                                                       THEN 'Opp.monDealAmount'--'[dbo].[getdealamount](Opp.numOppId,Getutcdate(),0)'
                                                       WHEN @vcLookBackTableName = 'OpportunityBizDocs'
                                                            AND @vcDbColumnName = 'monDealAmount'
                                                       THEN 'ISNULL((SELECT SUM(ISNULL(monDealAmount,0)) FROM   OpportunityBizDocs BD WHERE  BD.numOppId = Opp.numOppId AND ISNULL(bitAuthoritativeBizDocs,0)=1),0)'
                                                       WHEN @vcLookBackTableName = 'OpportunityBizDocs'
                                                            AND @vcDbColumnName = 'monAmountPaid'
                                                       THEN 'ISNULL((SELECT SUM(ISNULL(monAmountPaid,0)) FROM   OpportunityBizDocs BD WHERE  BD.numOppId = Opp.numOppId AND ISNULL(bitAuthoritativeBizDocs,0)=1),0)'
                                                       WHEN @vcLookBackTableName = 'OpportunityBizDocs'
                                                            AND @vcDbColumnName = 'vcBizDocsList'
                                                       THEN 'ISNULL((SELECT SUBSTRING((SELECT ''$^$'' + CAST(numOppBizDocsId AS VARCHAR(18)) +''#^#''+ vcBizDocID
	FROM OpportunityBizDocs WHERE numOppId=Opp.numOppId FOR XML PATH('''')),4,200000)),'''')'
                                                       ELSE @vcDbColumnName
                                                  END + ' [' + @vcColumnName
                                                + ']'            
                                        END

                                    ELSE 
                                        IF @vcAssociatedControlType = 'Popup'
                                            AND @vcDbColumnName = 'numShareWith' 
                                            BEGIN      
                                                SET @strSql = @strSql
                                                    + ',(SELECT SUBSTRING(
	(SELECT '','' + A.vcFirstName+'' ''+A.vcLastName + CASE WHEN ISNULL(SR.numContactType,0)>0 THEN ''('' + dbo.fn_GetListItemName(SR.numContactType) + '')'' ELSE '''' END
	FROM ShareRecord SR JOIN UserMaster UM ON UM.numUserDetailId=SR.numAssignedTo 
							 JOIN AdditionalContactsInformation A ON UM.numUserDetailId=A.numContactID 
		 WHERE SR.numDomainID=Opp.numDomainID AND SR.numModuleID=3 AND SR.numRecordID=Opp.numOppId
				AND UM.numDomainID=Opp.numDomainID and UM.numDomainID=A.numDomainID FOR XML PATH('''')),2,200000)) ['
                                                    + @vcColumnName + ']'                
                                            END		

                END
            ELSE 
                BEGIN
        
                    SET @vcColumnName = @vcDbColumnName + '~'
                        + CONVERT(VARCHAR(10), @numFieldId) + '~1'

                    IF @vcAssociatedControlType = 'TextBox' OR @vcAssociatedControlType = 'TextArea' 
                        BEGIN
                            SET @strSql = @strSql + ',CFW'
                                + CONVERT(VARCHAR(3), @tintOrder)
                                + '.Fld_Value  [' + @vcColumnName + ']'
                            SET @WhereCondition = @WhereCondition
                                + ' left Join CFW_FLD_Values_Opp CFW'
                                + CONVERT(VARCHAR(3), @tintOrder)
                                + '               
                                                                                                                            on CFW'
                                + CONVERT(VARCHAR(3), @tintOrder) + '.Fld_Id='
                                + CONVERT(VARCHAR(10), @numFieldId)
                                + 'and CFW' + CONVERT(VARCHAR(3), @tintOrder)
                                + '.RecId=Opp.numOppid   '
                        END
                    ELSE 
                        IF @vcAssociatedControlType = 'CheckBox' 
                            BEGIN
							
                                SET @strSql = @strSql 
                                    + ',case when isnull(CFW' + CONVERT(VARCHAR(3), @tintOrder) 
                                    + '.Fld_Value,0)=0 then ''No'' when isnull(CFW' + CONVERT(VARCHAR(3), @tintOrder) 
									+ '.Fld_Value,0)=1 then ''Yes'' end   [' + @vcColumnName + ']'

                                SET @WhereCondition = @WhereCondition
                                    + ' left Join CFW_FLD_Values_Opp CFW'
                                    + CONVERT(VARCHAR(3), @tintOrder) + ' on CFW'
                                    + CONVERT(VARCHAR(3), @tintOrder) + '.Fld_Id='
                                    + CONVERT(VARCHAR(10), @numFieldId) + 'and CFW'
                                    + CONVERT(VARCHAR(3), @tintOrder) + '.RecId=Opp.numOppid   '
                            END
                        ELSE 
                            IF @vcAssociatedControlType = 'DateField' 
                                BEGIN
                                    SET @strSql = @strSql
                                        + ',dbo.FormatedDateFromDate(CFW'
                                        + CONVERT(VARCHAR(3), @tintOrder)
                                        + '.Fld_Value,'
                                        + CONVERT(VARCHAR(10), @numDomainId)
                                        + ')  [' + @vcColumnName + ']'
                                    SET @WhereCondition = @WhereCondition
                                        + ' left Join CFW_FLD_Values_Opp CFW'
                                        + CONVERT(VARCHAR(3), @tintOrder)
                                        + '               
                                                                                                                                        on CFW'
                                        + CONVERT(VARCHAR(3), @tintOrder)
                                        + '.Fld_Id='
                                        + CONVERT(VARCHAR(10), @numFieldId)
                                        + 'and CFW'
                                        + CONVERT(VARCHAR(3), @tintOrder)
                                        + '.RecId=Opp.numOppid    '
                                END
                            ELSE 
                                IF @vcAssociatedControlType = 'SelectBox' 
                                    BEGIN
                                        SET @vcDbColumnName = 'DCust'
                                            + CONVERT(VARCHAR(10), @numFieldId)
                                        SET @strSql = @strSql + ',L'
                                            + CONVERT(VARCHAR(3), @tintOrder)
                                            + '.vcData' + ' [' + @vcColumnName
                                            + ']'
                                        SET @WhereCondition = @WhereCondition
                                            + ' left Join CFW_FLD_Values_Opp CFW'
                                            + CONVERT(VARCHAR(3), @tintOrder)
                                            + '               
                                                                                                                                               on CFW'
                                            + CONVERT(VARCHAR(3), @tintOrder)
                                            + '.Fld_Id='
                                            + CONVERT(VARCHAR(10), @numFieldId)
                                            + 'and CFW'
                                            + CONVERT(VARCHAR(3), @tintOrder)
                                            + '.RecId=Opp.numOppid     '
                                        SET @WhereCondition = @WhereCondition
                                            + ' left Join ListDetails L'
                                            + CONVERT(VARCHAR(3), @tintOrder)
                                            + ' on L'
                                            + CONVERT(VARCHAR(3), @tintOrder)
                                            + '.numListItemID=CFW'
                                            + CONVERT(VARCHAR(3), @tintOrder)
                                            + '.Fld_Value'
                                    END
                END
      
     
            SELECT TOP 1
                    @tintOrder = tintOrder + 1,
                    @vcDbColumnName = vcDbColumnName,
                    @vcFieldName = vcFieldName,
                    @vcAssociatedControlType = vcAssociatedControlType,
                    @vcListItemType = vcListItemType,
                    @numListID = numListID,
                    @vcLookBackTableName = vcLookBackTableName,
                    @bitCustom = bitCustomField,
                    @numFieldId = numFieldId,
                    @bitAllowSorting = bitAllowSorting,
                    @bitAllowEdit = bitAllowEdit,
                    @ListRelID = ListRelID
            FROM    #tempForm
            WHERE   tintOrder > @tintOrder - 1
            ORDER BY tintOrder ASC            
 
            IF @@rowcount = 0 
                SET @tintOrder = 0 
        END 
  
  -------Change Row Color-------
    DECLARE @vcCSOrigDbCOlumnName AS VARCHAR(50),
        @vcCSLookBackTableName AS VARCHAR(50),
        @vcCSAssociatedControlType AS VARCHAR(50)
    SET @vcCSOrigDbCOlumnName = ''
    SET @vcCSLookBackTableName = ''

    CREATE TABLE #tempColorScheme
        (
          vcOrigDbCOlumnName VARCHAR(50),
          vcLookBackTableName VARCHAR(50),
          vcFieldValue VARCHAR(50),
          vcFieldValue1 VARCHAR(50),
          vcColorScheme VARCHAR(50),
          vcAssociatedControlType VARCHAR(50)
        )

    INSERT  INTO #tempColorScheme
            SELECT  DFM.vcOrigDbCOlumnName,
                    DFM.vcLookBackTableName,
                    DFCS.vcFieldValue,
                    DFCS.vcFieldValue1,
                    DFCS.vcColorScheme,
                    DFFM.vcAssociatedControlType
            FROM    DycFieldColorScheme DFCS
                    JOIN DycFormField_Mapping DFFM ON DFCS.numFieldID = DFFM.numFieldID
                    JOIN DycFieldMaster DFM ON DFM.numModuleID = DFFM.numModuleID
                                               AND DFM.numFieldID = DFFM.numFieldID
            WHERE   DFCS.numDomainID = @numDomainID
                    AND DFFM.numFormID = @numFormId
                    AND DFCS.numFormID = @numFormId
                    AND ISNULL(DFFM.bitAllowGridColor, 0) = 1

    IF ( SELECT COUNT(*)
         FROM   #tempColorScheme
       ) > 0 
        BEGIN
            SELECT TOP 1
                    @vcCSOrigDbCOlumnName = vcOrigDbCOlumnName,
                    @vcCSLookBackTableName = vcLookBackTableName,
                    @vcCSAssociatedControlType = vcAssociatedControlType
            FROM    #tempColorScheme
        END   
----------------------------                       

    IF LEN(@vcCSOrigDbCOlumnName) > 0
        AND LEN(@vcCSLookBackTableName) > 0 
        BEGIN
            SET @strSql = @strSql + ',tCS.vcColorScheme'
        END

  
    SET @strSql = @strSql
        + ' FROM OpportunityMaster Opp                                                               
                            INNER JOIN AdditionalContactsInformation ADC ON Opp.numContactId = ADC.numContactId                                                               
							INNER JOIN DivisionMaster DM ON Opp.numDivisionId = DM.numDivisionID AND ADC.numDivisionId = DM.numDivisionID                                                               
                            INNER JOIN CompanyInfo cmp ON DM.numCompanyID = cmp.numCompanyId '
  
    IF LEN(@vcCSOrigDbCOlumnName) > 0
        AND LEN(@vcCSLookBackTableName) > 0 
        BEGIN
            IF @vcCSLookBackTableName = 'AdditionalContactsInformation' 
                SET @Prefix = 'ADC.'                  
            IF @vcCSLookBackTableName = 'DivisionMaster' 
                SET @Prefix = 'DM.'
            IF @vcCSLookBackTableName = 'CompanyInfo' 
                SET @Prefix = 'CMP.'   
            IF @vcCSLookBackTableName = 'OpportunityMaster' 
                SET @Prefix = 'Opp.'   
 
            IF @vcCSAssociatedControlType = 'DateField' 
                BEGIN
                    SET @strSql = @strSql
                        + ' left join #tempColorScheme tCS on CONVERT(VARCHAR(10),'
                        + @Prefix + @vcCSOrigDbCOlumnName
                        + ',111) >= CONVERT(VARCHAR(10),DateAdd(day,cast(tCS.vcFieldValue as int),GETUTCDATE()),111)
			 and CONVERT(VARCHAR(10),' + @Prefix + @vcCSOrigDbCOlumnName
                        + ',111) <= CONVERT(VARCHAR(10),DateAdd(day,cast(tCS.vcFieldValue1 as int),GETUTCDATE()),111)'
                END
            ELSE 
                IF @vcCSAssociatedControlType = 'SelectBox'
                    OR @vcCSAssociatedControlType = 'ListBox'
                    OR @vcCSAssociatedControlType = 'CheckBox' 
                    BEGIN
                        SET @strSql = @strSql
                            + ' left join #tempColorScheme tCS on tCS.vcFieldValue='
                            + @Prefix + @vcCSOrigDbCOlumnName
                    END
        END
                          
    IF @columnName LIKE 'CFW.Cust%' 
        BEGIN            
            SET @strSql = @strSql
                + ' left Join CFW_Fld_Values_Opp CFW on CFW.RecId=Opp.numOppid and CFW.fld_id= '
                + REPLACE(@columnName, 'CFW.Cust', '') + ' '                                                     
            SET @columnName = 'CFW.Fld_Value'            
        END 

    SET @strSql = @strSql + @WhereCondition
        + ' join (select ID,numOppId,intTotalProgress FROM  #tempTable )T on T.numOppId=Opp.numOppId                                              
                          WHERE ID > ' + CONVERT(VARCHAR(10), @firstRec)
        + ' and ID <' + CONVERT(VARCHAR(10), @lastRec)
    
    IF ( @tintFilterBy = 5 ) --Sort alphabetically
        SET @strSql = @strSql + ' order by ID '
    ELSE 
        IF ( @columnName = 'numAssignedBy'
             OR @columnName = 'numAssignedTo'
           ) 
            SET @strSql = @strSql + ' ORDER BY  opp.' + @columnName + ' '
                + @columnSortOrder 
        ELSE 
            IF ( @columnName = 'PP.intTotalProgress' ) 
                SET @strSql = @strSql + ' ORDER BY  T.intTotalProgress '
                    + @columnSortOrder 
            ELSE 
                IF @columnName <> 'opp.bintCreatedDate'
                    AND @columnName <> 'vcPOppName'
                    AND @columnName <> 'bintCreatedDate'
                    AND @columnName <> 'vcCompanyName' 
                    SET @strSql = @strSql + ' ORDER BY  ' + @columnName + ' '
                        + @columnSortOrder
                ELSE 
                    SET @strSql = @strSql + ' order by ID '

                                             
    PRINT @strSql                      
              
    EXEC ( @strSql
        )  

    SELECT  *
    FROM    #tempForm

    DROP TABLE #tempForm

    DROP TABLE #tempColorScheme

    DROP TABLE #tempTable