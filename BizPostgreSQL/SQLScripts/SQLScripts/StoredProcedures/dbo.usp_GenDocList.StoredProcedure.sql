
GO
/****** Object:  StoredProcedure [dbo].[usp_GenDocList]    Script Date: 02/16/2010 00:39:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created By Anoop Jayaraj                  
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_gendoclist')
DROP PROCEDURE usp_gendoclist
GO
CREATE PROCEDURE [dbo].[usp_GenDocList]                    
(
	@numDomainID NUMERIC(18,0)=0,           
	@numUserCntID NUMERIC(18,0)=0,	                   
	@tintUserRightType TINYINT=0,
	@CurrentPage INT,
	@PageSize INT,
	@columnName VARCHAR(50),
	@columnSortOrder VARCHAR(10),
	@DocCategory NUMERIC(18,0)=0,
	@numDocStatus NUMERIC(18,0)=0,
	@ClientTimeZoneOffset INT,
	@numGenericDocId NUMERIC(18,0) = 0,
	@SortChar char(1)='0',
	@tintDocumentType TINYINT
)                   
AS
BEGIN  
	IF @columnSortOrder <> 'Desc' AND @columnSortOrder <> 'Asc'
	BEGIN
		SET @columnSortOrder = 'Asc'
	END

	SELECT 
		COUNT(*) OVER() AS numTotalRecords,
		numGenericDocId,                    
		VcFileName ,                    
		vcDocName ,                  
		numDocCategory,                    
		(CASE numDocCategory 
			WHEN 369 THEN CONCAT('Email Template',' <font color=green>(',ISNULL(EMM.vcModuleName,'-'),')</font> ')
			ELSE dbo.fn_GetListItemName(numDocCategory) 
		END) Category,
		vcfiletype,                    
		cUrlType,                    
		dbo.fn_GetListItemName(numDocStatus) as BusClass,                    
		dbo.fn_GetContactName(numModifiedBy)+','+ convert(varchar(20),dateadd(minute,-@ClientTimeZoneOffset,bintModifiedDate)) as [Mod]
	FROM 
		GenericDocuments 
	LEFT JOIN 
		EmailMergeModule EMM 
	ON 
		EMM.numModuleID = GenericDocuments.numModuleID 
		AND EMM.tintModuleType=0 
	WHERE 
		numDomainID = @numDomainID
		AND (ISNULL(@numGenericDocId,0) = 0 OR numGenericDocID=@numGenericDocId)
		AND tintDocumentType=1
		AND (ISNULL(@numDocStatus,0) = 0 OR numDocStatus=@numDocStatus)
		AND (ISNULL(@DocCategory,0) = 0 OR numDocCategory=@DocCategory)
		AND 1 =(CASE WHEN @tintUserRightType=1 THEN (CASE WHEN numCreatedby = @numUserCntID THEN 1 ELSE 0 END) ELSE 1 END)
		AND 1 = (CASE WHEN @SortChar <> '0' THEN (CASE WHEN vcDocName like CONCAT(@SortChar,'%') THEN 1 ELSE 0 END) ELSE 1 END)
		AND 1 = (CASE @tintDocumentType 
					WHEN 2 THEN (CASE WHEN ISNULL(VcFileName,'') LIKE '#SYS#%' THEN 1 ELSE 0 END)
					WHEN 1 THEN (CASE WHEN ISNULL(VcFileName,'') NOT LIKE '#SYS#%' THEN 1 ELSE 0 END) 
					ELSE 1 
				END)
	ORDER BY
		CASE 
			WHEN @columnSortOrder = 'Desc' AND @ColumnName <> 'Mod' THEN
			CASE @ColumnName 
			   WHEN 'vcDocName' THEN vcDocName
			   WHEN 'Category' THEN (CASE numDocCategory 
										WHEN 369 THEN CONCAT('Email Template',' <font color=green>(',ISNULL(EMM.vcModuleName,'-'),')</font> ')
										ELSE dbo.fn_GetListItemName(numDocCategory) 
									END)
			   WHEN 'vcfiletype' THEN vcfiletype
			   WHEN 'BusClass' THEN dbo.fn_GetListItemName(numDocStatus)
			END
		END DESC,
		CASE @columnSortOrder WHEN 'Desc' THEN
			CASE @ColumnName 
				WHEN 'Mod' THEN bintModifiedDate
			   ELSE bintcreateddate
			END
		END DESC,
		CASE WHEN @columnSortOrder = 'Asc' AND  @ColumnName <> 'Mod' THEN
			CASE @ColumnName
			   WHEN 'vcDocName' THEN vcDocName
			   WHEN 'Category' THEN (CASE numDocCategory 
										WHEN 369 THEN CONCAT('Email Template',' <font color=green>(',ISNULL(EMM.vcModuleName,'-'),')</font> ')
										ELSE dbo.fn_GetListItemName(numDocCategory) 
									END)
			   WHEN 'vcfiletype' THEN vcfiletype
			   WHEN 'BusClass' THEN dbo.fn_GetListItemName(numDocStatus)
			END
		END ASC,
		CASE @columnSortOrder WHEN 'Asc' THEN
			CASE @ColumnName 
				WHEN 'Mod' THEN bintModifiedDate
			   ELSE bintcreateddate
			END
		END ASC
	OFFSET (@CurrentPage-1) * @PageSize ROWS FETCH NEXT @PageSize ROWS ONLY		
END
GO