/* FOR SO:Adds Qty on allocation and deducts from onhand*/
/* FOR PO:Adds Qty on Order and updates average cost */
--exec USP_UpdatingInventoryonCloseDeal 5833      
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_updatinginventoryonclosedeal')
DROP PROCEDURE usp_updatinginventoryonclosedeal
GO
CREATE PROCEDURE [dbo].[USP_UpdatingInventoryonCloseDeal]
@numOppID NUMERIC(18,0),
@numOppItemId NUMERIC(18,0),
@numUserCntID AS NUMERIC(18,0)        
AS
BEGIN TRY
   BEGIN TRANSACTION      
		
		declare @tintOpptype as tinyint        
		declare @itemcode as numeric                                    
		declare @numUnits as FLOAT                                      
		declare @numoppitemtCode as numeric(9)         
		declare @numWareHouseItemID as numeric(9) 
		declare @numToWarehouseItemID as numeric(9) --to be used with stock transfer
		declare @monAmount as DECIMAL(20,5) 
		declare @bitStockTransfer as bit
		declare @QtyShipped as FLOAT
		declare @QtyReceived as FLOAT
		DECLARE @fltExchangeRate AS FLOAT 
		DECLARE @bitWorkOrder AS BIT

		DECLARE @numDomain AS NUMERIC(18,0)
		SELECT @numDomain = OM.numDomainID FROM [dbo].[OpportunityMaster] AS OM WHERE [OM].[numOppId] = @numOppID
		select @tintOpptype=tintOppType,@bitStockTransfer=ISNULL(bitStockTransfer,0),@fltExchangeRate=(CASE WHEN ISNULL(fltExchangeRate,0)=0 THEN 1 ELSE fltExchangeRate END) from OpportunityMaster where numOppId=@numOppID
               
		 select top 1 @numoppitemtCode=numoppitemtCode,@itemcode=OI.numItemCode,@numUnits=numUnitHour,@qtyShipped=ISNULL([numQtyShipped],0),@qtyReceived=ISNULL([numUnitHourReceived],0),        
		 @numWareHouseItemID=ISNULL(numWarehouseItmsID,0),@numToWarehouseItemID=numToWarehouseItemID,@monAmount=monTotAmount * @fltExchangeRate,@bitWorkOrder=OI.bitWorkOrder
		 FROM OpportunityItems OI join Item I                                                
		 on OI.numItemCode=I.numItemCode   and numOppId=@numOppId                                          
		 where (charitemtype='P' OR 1=(CASE WHEN @tintOpptype=1 THEN 
							CASE WHEN ISNULL(I.bitKitParent,0) = 1 AND ISNULL(I.bitAssembly,0) = 1 THEN 0
								 WHEN ISNULL(I.bitKitParent,0) = 1 THEN 1
								 ELSE 0 END 
							ELSE 0 END)) and (bitDropShip=0 or bitDropShip is null)
		 AND I.[numDomainID] = @numDomain
		 AND (ISNULL(@numOppItemId,0) = 0 OR OI.numoppitemtCode=@numOppItemId)
		 ORDER by OI.numoppitemtCode                                       
		 while @numoppitemtCode>0                                        
		  begin   
	
	
				IF @bitStockTransfer = 1
				BEGIN
					--Make inventory changes as if sales order was placed , using OpportunityItems.numWarehouseItmsID
					EXEC usp_ManageInventory @itemcode,@numWareHouseItemID,@monAmount,1,@numUnits,@QtyShipped,@qtyReceived,0,1,@numOppID,@numoppitemtCode,@numUserCntID,@bitWorkOrder
					--Make inventory changes as if purchase order was placed , using OpportunityItems.numToWarehouseItemID
					EXEC usp_ManageInventory @itemcode,@numToWarehouseItemID,@monAmount,2,@numUnits,@QtyShipped,@qtyReceived,0,1,@numOppID,@numoppitemtCode,@numUserCntID,@bitWorkOrder
				END
				ELSE
				BEGIN
					EXEC usp_ManageInventory @itemcode,@numWareHouseItemID,@monAmount,@tintOpptype,@numUnits,@QtyShipped,@qtyReceived,0,1,@numOppID,@numoppitemtCode,@numUserCntID,@bitWorkOrder
				END
                                                                                                                                      
		   select top 1 @numoppitemtCode=numoppitemtCode,@itemcode=OI.numItemCode,@numUnits=numUnitHour,@qtyShipped=ISNULL([numQtyShipped],0),@qtyReceived=ISNULL([numUnitHourReceived],0),        
		 @numWareHouseItemID=ISNULL(numWarehouseItmsID,0),@numToWarehouseItemID=numToWarehouseItemID,@monAmount=monTotAmount * @fltExchangeRate,@bitWorkOrder=OI.bitWorkOrder
			from OpportunityItems OI join Item I         
		   on OI.numItemCode=I.numItemCode and numOppId=@numOppId               
		   where (charitemtype='P' OR 1=(CASE WHEN @tintOpptype=1 THEN 
							CASE WHEN ISNULL(I.bitKitParent,0) = 1 AND ISNULL(I.bitAssembly,0) = 1 THEN 0
								 WHEN ISNULL(I.bitKitParent,0) = 1 THEN 1
								 ELSE 0 END 
							ELSE 0 END)) and OI.numoppitemtCode>@numoppitemtCode and (bitDropShip=0 or bitDropShip is null)  
			AND I.[numDomainID] = @numDomain
			AND (ISNULL(@numOppItemId,0) = 0 OR OI.numoppitemtCode=@numOppItemId)
			order by OI.numoppitemtCode                                                
		   if @@rowcount=0 set @numoppitemtCode=0         
		  END

COMMIT
END TRY
BEGIN CATCH
  -- Whoops, there was an error
  IF @@TRANCOUNT > 0
     ROLLBACK

  -- Raise an error with the details of the exception
  DECLARE @ErrMsg nvarchar(4000), @ErrSeverity int
  SELECT @ErrMsg = ERROR_MESSAGE(),
         @ErrSeverity = ERROR_SEVERITY()

  RAISERROR(@ErrMsg, @ErrSeverity, 1)
END CATCH
        
GO
