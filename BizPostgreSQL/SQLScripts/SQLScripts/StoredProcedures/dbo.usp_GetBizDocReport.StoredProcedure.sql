/****** Object:  StoredProcedure [dbo].[usp_GetBizDocReport]    Script Date: 07/26/2008 16:16:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- mOdified By Anoop Jayaraj              
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getbizdocreport')
DROP PROCEDURE usp_getbizdocreport
GO
CREATE PROCEDURE [dbo].[usp_GetBizDocReport]              
 @numDomainID numeric,                  
 @intSortOrder numeric,                  
 @vcChooseBizDoc varchar(30),                  
 @numUserCntID numeric=0,                                 
 @intType numeric=0,                
 @tintRights TINYINT=1                
  --                
AS                  
BEGIN              
            
declare @strSQL as varchar(8000)                
 If @tintRights=1                
 --All Records Rights                  
 BEGIN                  
                
              
 set @strSQL='select *,(X.GrandTotal-X.AmountPaid) as BalanceDue from (SELECT Opp.numOppid,div.tintCRMtype,numOppBizDocsId,com.numCompanyId,div.numDivisionID,dbo.fn_GetPrimaryContact(div.numDivisionID) as ContactID,dtCreatedDate as DueDate,
 dbo.GetDealAmount(Opp.numOppid,getutcdate(),0) as GrandTotal,              
 isnull(monAmountPaid,0) as AmountPaid,case when intPEstimatedCloseDate<getutcdate() then ''-''            
 when intPEstimatedCloseDate>getutcdate()then datediff(day,intPEstimatedCloseDate,getutcdate()) end as DaysPastdue,            
 vcCompanyName+'', ''+ vcDivisionName as CustomerDivision,              
 vcBizDocID as BizDocID, '''' as BillingTerms ,numBizDocId             
 FROM OpportunityBizDocs BizDocs             
 join OpportunityMaster Opp            
 on BizDocs.numOppId=Opp.numOppId          
 join DivisionMaster Div          
 on Div.numDivisionID=Opp.numDivisionID          
 join Companyinfo Com          
 on Com.numCompanyID=Div.numCompanyID            
 where BizDocs.numCreatedBy='+ convert(varchar(15),@numUserCntID)+')X Where 1=1 '            
 if @vcChooseBizDoc<>'0' set @strSQL=@strSQL+ ' and X.numBizDocId='+@vcChooseBizDoc            
 if @intSortOrder=1  set @strSQL=@strSQL+ ' order by X.BalanceDue,X.DaysPastdue'            
 if @intSortOrder=2  set @strSQL=@strSQL+ ' order by X.DaysPastdue,X.BalanceDue'      
print @strSQL           
exec (@strSQL)            
                
 END                  
                  
 If @tintRights=2                
 BEGIN                  
                
               
 set @strSQL='select * from (SELECT Opp.numOppid,div.tintCRMtype,numOppBizDocsId,com.numCompanyId,div.numDivisionID,dbo.fn_GetPrimaryContact(div.numDivisionID) as ContactID,dtCreatedDate as DueDate,monPAmount as GrandTotal,              
 isnull(monAmountPaid,0) as AmountPaid, (monPAmount-isnull(monAmountPaid,0)) as BalanceDue,              
 case when intPEstimatedCloseDate<getutcdate() then ''-''            
 when intPEstimatedCloseDate>getutcdate()then datediff(day,intPEstimatedCloseDate,getutcdate()) end as DaysPastdue,             
 vcCompanyName+'', ''+ vcDivisionName as CustomerDivision,              
 vcBizDocID as BizDocID, '''' as BillingTerms ,numBizDocId             
 FROM OpportunityBizDocs BizDocs             
 join OpportunityMaster Opp            
 on BizDocs.numOppId=Opp.numOppId          
 join DivisionMaster Div          
 on Div.numDivisionID=Opp.numDivisionID          
 join Companyinfo Com          
 on Com.numCompanyID=Div.numCompanyID 
 join AdditionalContactsInformation  ADC
 on ADC.numContactId= Opp.numRecOwner          
 where ADC.numTeam in(select F.numTeam from ForReportsByTeam F                     
 where F.numUserCntid='+convert(varchar(15),@numUserCntID)+' and F.numDomainid='+convert(varchar(15),@numDomainId)+' and F.tintType='+convert(varchar(15),@intType)+'))X Where 1=1 '              
            
 if @vcChooseBizDoc<>'0' set @strSQL=@strSQL+ ' and X.numBizDocId='+@vcChooseBizDoc            
 if @intSortOrder=1  set @strSQL=@strSQL+ ' order by X.BalanceDue,X.DaysPastdue'            
 if @intSortOrder=2  set @strSQL=@strSQL+ ' order by X.DaysPastdue,X.BalanceDue'            
  print @strSQL            
exec (@strSQL)            
 END                  

                  
 If @tintRights=3                
 BEGIN                  
               
 set @strSQL='select * from (SELECT Opp.numOppid,div.tintCRMtype,numOppBizDocsId,com.numCompanyId,div.numDivisionID,dbo.fn_GetPrimaryContact(div.numDivisionID) as ContactID,dtCreatedDate as DueDate,monPAmount as GrandTotal,              
 isnull(monAmountPaid,0) as AmountPaid, (monPAmount-isnull(monAmountPaid,0)) as BalanceDue,           
 case when intPEstimatedCloseDate<getutcdate() then ''-''            
 when intPEstimatedCloseDate>getutcdate()then datediff(day,intPEstimatedCloseDate,getutcdate()) end as DaysPastdue,             
 vcCompanyName+'', ''+ vcDivisionName as CustomerDivision,              
 vcBizDocID as BizDocID, '''' BillingTerms ,numBizDocId             
 FROM OpportunityBizDocs BizDocs             
 join OpportunityMaster Opp            
 on BizDocs.numOppId=Opp.numOppId          
 join DivisionMaster Div          
 on Div.numDivisionID=Opp.numDivisionID          
 join Companyinfo Com          
 on Com.numCompanyID=Div.numCompanyID            
where Div.numTerId in(select F.numTerritory from ForReportsByTerritory F                     
     where F.numUserCntid='+convert(varchar(15),@numUserCntID)+' and F.numDomainid='+convert(varchar(15),@numDomainId)+' and F.tintType='+convert(varchar(15),@intType)+'))X Where 1=1 '             
            
if @vcChooseBizDoc<>'0' set @strSQL=@strSQL+ ' and X.numBizDocId='+@vcChooseBizDoc            
 if @intSortOrder=1  set @strSQL=@strSQL+ ' order by X.BalanceDue,X.DaysPastdue'            
 if @intSortOrder=2  set @strSQL=@strSQL+ ' order by X.DaysPastdue,X.BalanceDue'    
print @strSQL               
exec (@strSQL)             
                
 END                  
 END
GO
