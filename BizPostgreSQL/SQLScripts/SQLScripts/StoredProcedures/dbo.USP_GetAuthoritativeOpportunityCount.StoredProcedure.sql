/****** Object:  StoredProcedure [dbo].[USP_GetAuthoritativeOpportunityCount]    Script Date: 07/26/2008 16:16:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created By Siva    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getauthoritativeopportunitycount')
DROP PROCEDURE usp_getauthoritativeopportunitycount
GO
CREATE PROCEDURE [dbo].[USP_GetAuthoritativeOpportunityCount]      
(
	@numDomainId as numeric(9)=0,      
	@numOppId as numeric(9)=0        
)
AS   
BEGIN
	DECLARE @tinyOppType AS TINYINT
	DECLARE @numAuthoritativeBizDocId AS NUMERIC(9)
	DECLARE @numBizDocsId AS NUMERIC(9)      
  
	SELECT @tinyOppType=ISNULL(tintOppType,0) FROM OpportunityMaster WHERE numOppId=@numOppId       
  
	IF @tinyOppType=1      
		SELECT @numAuthoritativeBizDocId=ISNULL(numAuthoritativeSales,0) FROM AuthoritativeBizDocs WHERE numDomainId=@numDomainId      
	ELSE IF @tinyOppType=2
		SELECT @numAuthoritativeBizDocId=ISNULL(numAuthoritativePurchase,0) FROM AuthoritativeBizDocs WHERE numDomainId=@numDomainId      
 
	SELECT 
		COUNT(*) 
	FROM 
		OpportunityBizDocs 
	WHERE 
		numOppId=@numOppId 
		AND ((numBizDocId=@numAuthoritativeBizDocId AND ISNULL(bitAuthoritativeBizDocs,0)=1) OR numBizDocId=304) -- 304 - IS DEFERRED INCOME BIZDOC 
End
GO
