/****** Object:  StoredProcedure [dbo].[usp_GetContactInfoForCase]    Script Date: 07/26/2008 16:16:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getcontactinfoforcase')
DROP PROCEDURE usp_getcontactinfoforcase
GO
CREATE PROCEDURE [dbo].[usp_GetContactInfoForCase]
	@numDomainID numeric(9)=0   
--				
AS
BEGIN
	SELECT 
		numContactID,vcFirstName,numCreatedBy,vcLastName
		
	FROM 	
		AdditionalContactsInformation
	WHERE 
		numDomainID=@numDomainID 
		AND numCreatedBy IS NOT NULL
				
	
END
GO
