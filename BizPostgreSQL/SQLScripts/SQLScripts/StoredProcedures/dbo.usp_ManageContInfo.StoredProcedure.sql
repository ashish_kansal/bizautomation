/****** Object:  StoredProcedure [dbo].[usp_ManageContInfo]    Script Date: 07/26/2008 16:19:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_managecontinfo')
DROP PROCEDURE usp_managecontinfo
GO
CREATE PROCEDURE [dbo].[usp_ManageContInfo]                  
 @numContactId numeric=0,                  
 @vcDepartment numeric(9)=0,                  
 @vcFirstName varchar(50)='',                  
 @vcLastName varchar(50)='',                  
 @vcEmail varchar(50)='',                  
 @vcSex char(1)='',                  
 @vcPosition  numeric(9)=0,                  
 @numPhone varchar(15)='',                  
 @numPhoneExtension varchar(7)='',                  
 @bintDOB DATETIME=null,                  
 @txtNotes text='',                  
 @numUserCntID numeric=0,                      
 @numCategory numeric=0,                   
 @numCell varchar(15)='',                  
 @NumHomePhone varchar(15)='',                  
 @vcFax varchar(15)='',                  
 @vcAsstFirstName varchar(50)='',                  
 @vcAsstLastName varchar(50)='',                  
 @numAsstPhone varchar(15)='',                  
 @numAsstExtn varchar(6)='',                  
 @vcAsstEmail varchar(50)='',                      
 @numContactType numeric=0 ,               
 @bitOptOut bit=0,                
 @numManagerId numeric=0,                
 @numTeam as numeric(9)=0,                
 @numEmpStatus as numeric(9)=0,        
 @vcAltEmail as varchar(100),        
 @vcTitle as varchar(100),
 @numECampaignID AS NUMERIC(9),
 @bitSelectiveUpdate AS BIT = 0, --Update fields whose value are supplied
 @bitPrimaryContact AS BIT=0,
 @vcTaxID VARCHAR(100) = ''        
AS                  
IF @bitSelectiveUpdate = 0
BEGIN

if @bintDOB ='Jan  1 1753 12:00AM' set  @bintDOB=null                  
       
        DECLARE @numDivisionID AS NUMERIC(9)            
        SELECT  @numDivisionID = numDivisionID FROM AdditionalContactsInformation WHERE numContactID = @numContactId   
           
 IF @bitPrimaryContact = 1 
    BEGIN            
        DECLARE @numID AS NUMERIC(9)            
        DECLARE @bitPrimaryContactCheck AS BIT             
             
        SELECT TOP 1
                @numID = numContactID,
                @bitPrimaryContactCheck = ISNULL(bitPrimaryContact,0)
        FROM    AdditionalContactsInformation
        WHERE   numDivisionID = @numDivisionID 
                   
        IF @@rowcount = 0 
            SET @numID = 0             
        WHILE @numID > 0            
            BEGIN            
                IF @bitPrimaryContactCheck = 1 
                    UPDATE  AdditionalContactsInformation
                    SET     bitPrimaryContact = 0
                    WHERE   numContactID = @numID  
                              
                SELECT TOP 1
                        @numID = numContactID,
                        @bitPrimaryContactCheck = ISNULL(bitPrimaryContact,0)
                FROM    AdditionalContactsInformation
                WHERE   numDivisionID = @numDivisionID
                        AND numContactID > @numID    
                                
                IF @@rowcount = 0 
                    SET @numID = 0             
            END            
    END 
    ELSE IF @bitPrimaryContact = 0
    BEGIN
		IF (SELECT COUNT(*) FROM AdditionalContactsInformation WHERE numDivisionID = @numDivisionID AND numContactId!=@numContactId AND ISNULL(bitPrimaryContact,0)=1)=0
			SET @bitPrimaryContact = 1
    END                 
    
    UPDATE AdditionalContactsInformation SET                  
     vcDepartment=@vcDepartment,                  
     vcFirstName=@vcFirstName ,                  
     vcLastName =@vcLastName ,                  
     numPhone=@numPhone ,                  
     numPhoneExtension=@numPhoneExtension,                  
     vcEmail=@vcEmail ,                  
     charSex=@vcSex ,                  
     vcPosition = @vcPosition,                  
     bintDOB=@bintDOB ,                  
     txtNotes=@txtNotes,                  
     numModifiedBy=@numUserCntID,                  
     bintModifiedDate=getutcdate(),                
     vcCategory = @numCategory,                  
     numCell=@numCell,                  
     NumHomePhone=@NumHomePhone,                  
     vcFax=@vcFax,                  
     vcAsstFirstName=@vcAsstFirstName,                  
     vcAsstLastName=@vcAsstLastName,                  
     numAsstPhone=@numAsstPhone,                  
     numAsstExtn=@numAsstExtn,                  
     vcAsstEmail=@vcAsstEmail,                     
     numContactType=@numContactType,                      
     bitOptOut=@bitOptOut,                
     numManagerId=@numManagerId,                
     numTeam=@numTeam,           
     numEmpStatus = @numEmpStatus,        
     vcTitle=@vcTitle,        
     vcAltEmail= @vcAltEmail,
     numECampaignID = @numECampaignID,
     bitPrimaryContact=@bitPrimaryContact,
	 vcTaxID=@vcTaxID
 WHERE numContactId=@numContactId
 
 /*Added by chintan BugID-262*/
	 DECLARE @Date AS DATETIME 
	 SELECT @Date = dbo.[GetUTCDateWithoutTime]()
	 IF @numECampaignID = -1 
		SET @numECampaignID=0
	 
 	EXEC [USP_ManageConEmailCampaign]
	@numConEmailCampID = 0, --  numeric(9, 0)
	@numContactID = @numContactID, --  numeric(9, 0)
	@numECampaignID = @numECampaignID, --  numeric(9, 0)
	@intStartDate = @Date, --  datetime
	@bitEngaged = 1, --  bit
	@numUserCntID = @numUserCntID --  numeric(9, 0)
END


IF @bitSelectiveUpdate = 1
BEGIN
		DECLARE @sql VARCHAR(1000)
		SET @sql = 'update AdditionalContactsInformation Set '

		IF(LEN(@vcFirstName)>0)
		BEGIN
			SET @sql =@sql + ' vcFirstName=''' + @vcFirstName + ''', '
		END
		IF(LEN(@vcLastName)>0)
		BEGIN
			SET @sql =@sql + ' vcLastName=''' + @vcLastName + ''', '
		END
		
		IF(LEN(@vcEmail)>0)
		BEGIN
			SET @sql =@sql + ' vcEmail=''' + @vcEmail + ''', '
		END
		
		IF(LEN(@numPhone)>0)
		BEGIN
			SET @sql =@sql + ' numPhone=''' + @numPhone + ''', '
		END
		
		IF(LEN(@numPhoneExtension)>0)
		BEGIN
			SET @sql =@sql + ' numPhoneExtension=''' + @numPhoneExtension + ''', '
		END
		
		IF(LEN(@numCell)>0)
		BEGIN
			SET @sql =@sql + ' numCell=''' + @numCell + ''', '
		END
		
		IF(LEN(@vcFax)>0)
		BEGIN
			SET @sql =@sql + ' vcFax=''' + @vcFax + ''', '
		END
		
		SET @sql =@sql + ' bitOptOut=''' + CONVERT(VARCHAR(10),@bitOptOut) + ''', '
		
		IF(@numUserCntID>0)
		BEGIN
			SET @sql =@sql + ' numModifiedBy='+ CONVERT(VARCHAR(10),@numUserCntID) +', '
		END
		
	
	
		SET @sql=SUBSTRING(@sql,0,LEN(@sql)) + ' '
		SET @sql =@sql + 'where numContactId= '+CONVERT(VARCHAR(10),@numContactId)

		PRINT @sql
		EXEC(@sql) 
END 