/****** Object:  StoredProcedure [dbo].[USP_GetOperationBudgetDetails]    Script Date: 07/26/2008 16:17:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created by siva                                                  
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getoperationbudgetdetails')
DROP PROCEDURE usp_getoperationbudgetdetails
GO
CREATE PROCEDURE  [dbo].[USP_GetOperationBudgetDetails]                                                  
(@numDomainId as numeric(9)=0,                                                  
 @numBudgetId as numeric(9)=0,                  
 @intFiscalYear as int=0,       
 @tintType as int=0                  
)                                                  
As                                                  
Begin                             
                        
 Declare @strSQL as varchar(8000)                         
 Declare @strMonSQL as varchar(8000)                           
 Set @strMonSQL=''                        
 Declare @numMonth as tinyint       
  Declare @dtFiscalStDate as datetime                            
     Declare @dtFiscalEndDate as datetime                 
 set @numMonth =1                                  
 --Select @numMonth=tintFiscalStartMonth From Domain Where numDomainId=@numDomainId                              
    Declare @Date as datetime                  
 set @Date = dateadd(year,@tintType,getutcdate())            
 print '@Date ======'+Convert(varchar(100),@Date)          
 While @numMonth <=12                              
  Begin                         
                      
                          
     Set @dtFiscalStDate =dbo.GetFiscalStartDate(dbo.GetFiscalyear(@Date,@numDomainId),@numDomainId)                         
     Set @dtFiscalStDate=dateadd(month,@numMonth-1,@dtFiscalStDate)                      
     Set @dtFiscalEndDate =  dateadd(day,-1,dateadd(year,1,dbo.GetFiscalStartDate(dbo.GetFiscalyear(@Date,@numDomainId),@numDomainId)))                      
     print @dtFiscalEndDate          
     print @dtFiscalStDate          
     --Set @strMonSQL=@strMonSQL+' '+  'dbo.fn_GetBudgetMonthDet('+Convert(varchar(2),month(@dtFiscalStDate))+',dbo.GetFiscalyear(getutcdate(),'+convert(varchar(18),@numDomainId)+'),RecordID)' + ' as ''' +Convert(varchar(2),month(@dtFiscalStDate))           
  
       
         
     Set @strMonSQL=@strMonSQL+' '+  'dbo.fn_GetBudgetMonthDet('+Convert(varchar(10),@numBudgetId)+','+Convert(varchar(2),month(@dtFiscalStDate))+','+Convert(varchar(4),year(@dtFiscalStDate))+','+Convert(varchar(10),@numDomainId)+',RecordID)' + ' as ''' +
  
Convert(varchar(2),month(@dtFiscalStDate)    
       ) +'~'+Convert(varchar(4),year(@dtFiscalStDate))+''','                         
                            
     Set @numMonth=@numMonth+1                              
  End      
   Set @dtFiscalStDate =dbo.GetFiscalStartDate(dbo.GetFiscalyear(@Date,@numDomainId),@numDomainId)                        
Set @strMonSQL = @strMonSQL + 
-- ' dbo.fn_GetBudgetMonthTotalAmt('+Convert(varchar(10),@numBudgetId)+','''+convert(varchar(500),@Date)+''',RecordID,'+convert(varchar(10),@numDomainId)+') as Total' +      
'(Select Sum(OBD.monAmount)  From OperationBudgetMaster OBM  
inner join OperationBudgetDetails OBD on OBM.numBudgetId=OBD.numBudgetId  
 Where OBM.numDomainId='+Convert(varchar(10),@numDomainId)+' And OBD.numBudgetId='+Convert(varchar(10),@numBudgetId)+' And OBD.numChartAcntId=RecordID  
   And (((OBD.tintMonth between Month('''+Convert(varchar(100),@dtFiscalStDate)+''') and 12) and OBD.intYear=Year('''+Convert(varchar(100),@dtFiscalStDate)+''')) Or ((OBD.tintMonth between 1 and month('''+Convert(varchar(100),@dtFiscalEndDate)+'''))
 and OBD.intYear=Year('''+Convert(varchar(100),@dtFiscalEndDate)+'''))))  as Total
,(Select top 1 isnull(vcComments,'''') From OperationBudgetDetails Where numBudgetId='+Convert(varchar(10),@numBudgetId) +' and tintMonth=Month('''+Convert(varchar(100),@dtFiscalEndDate)+''') 
And intYear=Year('''+Convert(varchar(100),@dtFiscalEndDate)+''') And numChartAcntId=RecordID)  as  Comments '                       
          print @strMonSQL                                     
Set @strSQL='  with RecursionCTE (RecordID,ParentRecordID,vcCatgyName,TOC,T)                        
     as                        
     (                        
     select numAccountId,numParntAcntId,vcCatgyName,convert(varchar(1000),'''') TOC,convert(varchar(1000),'''') T                        
      from Chart_Of_Accounts                        
     where numParntAcntId is null and numDomainID='+Convert(varchar(10),@numDomainId)                        
       +' union all                        
       select R1.numAccountId,                        
        R1.numParntAcntId,                        
     R1.vcCatgyName,                        
        case when DataLength(R2.TOC) > 0                        
         then convert(varchar(1000),case when CHARINDEX(''.'',R2.TOC)>0 then R2.TOC else R2.TOC +''.'' end                        
             + cast(R1.numAccountId as varchar(10)))                         
         else convert(varchar(1000),cast(R1.numAccountId as varchar(10)))                         
         end as TOC,case when DataLength(R2.TOC) > 0                        
         then  convert(varchar(1000),''&nbsp;&nbsp;''+ R2.T)                        
         else convert(varchar(1000),'''')                        
         end as T                        
       from Chart_Of_Accounts as R1                               
      join RecursionCTE as R2 on R1.numParntAcntId = R2.RecordID and R1.numDomainID='+ Convert(varchar(10),@numDomainId) +' and R1.numAccountId in(Select OBD.numChartAcntId From  OperationBudgetMaster OBM                          
      inner join OperationBudgetDetails OBD on OBM.numBudgetId=OBD.numBudgetId                                            
      Where OBD.numBudgetId='+convert(varchar(5),@numBudgetId)+ 'And OBD.intYear='+Convert(varchar(5),@intFiscalYear)+          
     ' And OBM.numDomainId='+convert(varchar(5),@numDomainId)+'))                         
    select RecordID as numChartAcntId,ParentRecordID as numParentAcntId,T+vcCatgyName as vcCategoryName,TOC,'+Convert(varchar(8000),@strMonSQL)+' from RecursionCTE  Where ParentRecordID is not null order by TOC asc'                        
  print @strSQL                                     
  Exec (@strSQL)                                   
                         
End
GO
