/****** Object:  StoredProcedure [dbo].[USP_ConECampaign]    Script Date: 07/26/2008 16:15:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_conecampaign')
DROP PROCEDURE usp_conecampaign
GO
CREATE PROCEDURE [dbo].[USP_ConECampaign]
@numContactID as numeric(9)=0

as

select top 1 numConEmailCampID,ConECampaign.numECampaignID,intStartDate,bitEngaged,isnull(txtDesc,'-') as txtDesc from ConECampaign
join ECampaign on ECampaign.numECampaignID=ConECampaign.numECampaignID
where  numContactID=@numContactID order by numConEmailCampID desc
GO
