/****** Object:  StoredProcedure [dbo].[Usp_GetCaseOpportunities]    Script Date: 07/26/2008 16:16:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getcaseopportunities')
DROP PROCEDURE usp_getcaseopportunities
GO
CREATE PROCEDURE [dbo].[Usp_GetCaseOpportunities]
    @numcaseId NUMERIC,
    @numDomainId NUMERIC,
    @vcOppName VARCHAR(200) = ''
AS 
    IF @vcOppName = '' 
        BEGIN
            SELECT  OI.[numOppId],
                    OI.[numoppitemtCode],
                    OI.[vcPathForTImage],
                    OI.[numItemCode],
                    OI.[vcItemName],
                    OI.[vcModelID],
                    OI.[vcItemDesc],
                    OI.[numUnitHour],
                    OI.[monTotAmount],
                    dbo.[GetSerialNumberList](OI.[numoppitemtCode]) AS vcSerialNo,
                    C.[vcCaseNumber]
            FROM    [CaseOpportunities] CO
                    INNER JOIN [OpportunityItems] OI ON CO.[numOppItemID] = OI.[numoppitemtCode]
                                                        AND CO.[numOppId] = OI.[numOppId]
					INNER JOIN [Cases] C ON C.[numCaseId] = CO.[numCaseId]	
            WHERE   CO.[numDomainId] = @numDomainId
                    AND CO.[numCaseId] = @numcaseId
    
        END
    ELSE 
        BEGIN
            DECLARE @OppID NUMERIC 
            SELECT TOP 1
                    @OppID = [numOppId]
            FROM    [OpportunityMaster] OM
            WHERE   OM.[numDomainId] = @numDomainId
					AND OM.[numDivisionId] IN (SELECT [numDivisionId] FROM [Cases] WHERE [numCaseId]=@numcaseId)
					AND OM.[numContactId] IN (SELECT [numContactId] FROM [Cases] WHERE [numCaseId]=@numcaseId)
					AND OM.[tintOppType]=1 -- only sales orders
                    AND OM.[vcPOppName] LIKE '%' + @vcOppName + '%'
                    
            SELECT  OI.[numOppId],
                    OI.[numoppitemtCode],
                    OI.[vcPathForTImage],
                    OI.[numItemCode],
                    OI.[vcItemName],
                    OI.[vcModelID],
                    OI.[vcItemDesc],
                    OI.[numUnitHour],
                    OI.[monTotAmount],
                    dbo.[GetSerialNumberList](OI.[numoppitemtCode]) AS vcSerialNo
            FROM    
                    [OpportunityItems] OI 
                    INNER JOIN [OpportunityMaster] OM ON OI.[numOppId] = OM.[numOppId]
            WHERE   OM.[numDomainId] = @numDomainId
                    AND OI.numOppId = @OppID
                    
	        
    
        END
            

