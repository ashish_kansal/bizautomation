/****** Object:  StoredProcedure [dbo].[usp_DeleteItemCmpAsset]    Script Date: 07/26/2008 16:15:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_deleteitemcmpasset')
DROP PROCEDURE usp_deleteitemcmpasset
GO
CREATE PROCEDURE  [dbo].[usp_DeleteItemCmpAsset]
@numDivId as numeric(9),
@numAItemCode as numeric(9),
@numDomainId as numeric(9)
as

delete companyassets where numAitemCode= @numAItemCode and numDomainId = @numDomainId and numDivId = @numDivId
GO
