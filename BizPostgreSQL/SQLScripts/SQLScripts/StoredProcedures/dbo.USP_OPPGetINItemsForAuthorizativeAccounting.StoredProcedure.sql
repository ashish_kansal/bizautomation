/****** Object:  StoredProcedure [dbo].[USP_OPPGetINItemsForAuthorizativeAccounting]    Script Date: 07/26/2008 16:20:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by Siva                                                                                                            
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_oppgetinitemsforauthorizativeaccounting')
DROP PROCEDURE usp_oppgetinitemsforauthorizativeaccounting
GO
CREATE PROCEDURE  [dbo].[USP_OPPGetINItemsForAuthorizativeAccounting]                                                                                                                                                
(                                                                                                                                                
	@numOppId AS NUMERIC(18,0)=null,                                                                                                                                                
	@numOppBizDocsId AS NUMERIC(18,0)=null,                                                                
	@numDomainID AS NUMERIC(18,0)=0 ,                                                      
	@numUserCntID AS NUMERIC(18,0)=0                                                                                                                                               
)                                                                                                                                                                                                                                                                                         
AS 
BEGIN
	DECLARE @numCurrencyID AS NUMERIC(18,0)
	DECLARE @fltExchangeRate AS FLOAT

	DECLARE @fltCurrentExchangeRate AS FLOAT
	DECLARE @tintType AS TINYINT
	DECLARE @vcPOppName AS VARCHAR(100)
	DECLARE @bitPPVariance AS BIT
	DECLARE @numDivisionID AS NUMERIC(18,0)

	SELECT 
		@numCurrencyID=numCurrencyID
		,@fltExchangeRate=fltExchangeRate
		,@tintType=tintOppType
		,@vcPOppName=vcPOppName
		,@bitPPVariance=ISNULL(bitPPVariance,0)
		,@numDivisionID=numDivisionID 
	FROM 
		OpportunityMaster
	WHERE 
		numOppID=@numOppId 

	DECLARE @vcBaseCurrency AS VARCHAR(100)=''
	DECLARE @bitAutolinkUnappliedPayment AS BIT 

	SELECT 
		@vcBaseCurrency=ISNULL(C.varCurrSymbol,'')
		,@bitAutolinkUnappliedPayment=ISNULL(bitAutolinkUnappliedPayment,0) 
	FROM 
		dbo.Domain D 
	LEFT JOIN 
		Currency C 
	ON 
		D.numCurrencyID=C.numCurrencyID 
	WHERE 
		D.numDomainID=@numDomainID

	DECLARE @vcForeignCurrency AS VARCHAR(100) = ''
	SELECT 
		@vcForeignCurrency=ISNULL(C.varCurrSymbol,'') 
	FROM  
		Currency C 
	WHERE 
		numCurrencyID=@numCurrencyID

	DECLARE @fltExchangeRateBizDoc AS FLOAT 
	DECLARE @numBizDocId AS NUMERIC(18,0)                                                           
	DECLARE @vcBizDocID AS VARCHAR(100)

	SELECT 
		@numBizDocId=numBizDocId
		,@fltExchangeRateBizDoc=fltExchangeRateBizDoc
		,@vcBizDocID=vcBizDocID 
	FROM 
		OpportunityBizDocs 
	WHERE 
		numOppBizDocsId=@numOppBizDocsId                                                         

	IF @numCurrencyID > 0 
		SET @fltCurrentExchangeRate=dbo.GetExchangeRate(@numDomainID,@numCurrencyID)
	ELSE 
		SET @fltCurrentExchangeRate=@fltExchangeRate                                                                                                                                          

	IF @numBizDocId = 296 OR @numBizDocId = 287 --FULFILLMENT BIZDOC OR INVOICE
	BEGIN
		--STORE AVERAGE COST FOR SHIPPED ITEM SO THAT WE CAN CALCULATE INVENTORY IMAPCT IN ACCOUNTING AND ALSO USE IT WHEN IETM RETURN
		UPDATE
			OBDI
		SET
			OBDI.monAverageCost= (CASE 
										WHEN ISNULL(I.bitKitParent,0) = 1 
										THEN ISNULL((SELECT SUM(ISNULL(OKI.numQtyItemsReq_Orig,0) * ISNULL(IInner.monAverageCost,0)) FROM OpportunityKitItems OKI INNER JOIN Item IInner ON OKI.numChildItemID=IInner.numItemCode WHERE OKI.numOppId=OI.numOppId AND OKI.numOppItemID=OI.numoppitemtCode),0) + 
												ISNULL((SELECT SUM(ISNULL(OKCI.numQtyItemsReq_Orig,0) * ISNULL(OKI.numQtyItemsReq_Orig,0) * ISNULL(IInner.monAverageCost,0)) FROM OpportunityKitChildItems OKCI INNER JOIN OpportunityKitItems OKI ON OKCI.numOppChildItemID=OKI.numOppChildItemID INNER JOIN Item IInner ON OKCI.numItemID=IInner.numItemCode WHERE OKI.numOppId=OI.numOppId AND OKI.numOppItemID=OI.numoppitemtCode),0)
										ELSE ISNULL(I.monAverageCost,0) 
									END)
		FROM
			OpportunityBizDocItems OBDI
		INNER JOIN
			OpportunityItems OI
		ON
			OBDI.numOppItemID=OI.numoppitemtCode
		INNER JOIN
			Item I
		ON
			OI.numItemCode = I.numItemCode
		WHERE
			OBDI.numOppBizDocID=@numOppBizDocsId
			AND OBDI.monAverageCost IS NULL
	END
                                                                                                                     
	SELECT 
		I.[vcItemName] as Item
		,charitemType as type
		,OBI.vcitemdesc as [desc]
		,OBI.numUnitHour as Unit
		,ISNULL(OBI.monPrice,0) as Price
		,ISNULL(OBI.monTotAmount,0) as Amount
		,ISNULL((opp.monTotAmount/opp.numUnitHour)*OBI.numUnitHour,0) AS  ItemTotalAmount
		,isnull(monListPrice,0) as listPrice
		,convert(varchar,i.numItemCode) as ItemCode
		,numoppitemtCode
		,L.vcdata as vcItemClassification
		,case when bitTaxable=0 then 'No' else 'Yes' end as Taxable
		,isnull(i.numIncomeChartAcntId,0) as itemIncomeAccount
		,isnull(i.numAssetChartAcntId,0) as itemInventoryAsset,'NI' as ItemType        
		,isnull(i.numCOGsChartAcntId,0) as itemCoGs
		,isnull(i.bitExpenseItem,0) as bitExpenseItem
		,isnull(i.numExpenseChartAcntId,0) as itemExpenseAccount
		,(CASE WHEN ISNULL(i.bitVirtualInventory,0) = 1 THEN 0 ELSE isnull(Opp.monAvgCost,0) END) as AverageCost
		,isnull(OBI.bitDropShip,0) as bitDropShip
		,(OBI.monTotAmtBefDiscount-OBI.monTotAmount) as DiscAmt
		,NULLIF(Opp.numProjectID,0) numProjectID
		,NULLIF(Opp.numClassID,0) numClassID
		,ISNULL(i.bitKitParent,0) AS bitKitParent
		,ISNULL(i.bitAssembly,0) AS bitAssembly
		,(CASE WHEN ISNULL(i.bitVirtualInventory,0) = 1 THEN 0 ELSE ISNULL(OBI.monAverageCost,0) END) AS ShippedAverageCost
	FROM 
		OpportunityItems opp
	INNER JOIN 
		OpportunityBizDocItems OBI
	ON 
		OBI.numOppItemID=Opp.numoppitemtCode       
	LEFT JOIN 
		Item I 
	ON 
		opp.numItemCode=I.numItemCode        
	LEFT JOIN 
		ListDetails L 
	ON 
		I.numItemClassification=L.numListItemID        
	WHERE 
		Opp.numOppId=@numOppId 
		AND OBI.numOppBizDocID=@numOppBizDocsId
                                                               
	SELECT 
		ISNULL(@numCurrencyID,0) as numCurrencyID
		,ISNULL(@fltExchangeRate,1) as fltExchangeRate
		,ISNULL(@fltCurrentExchangeRate,1) as CurrfltExchangeRate
		,ISNULL(@fltExchangeRateBizDoc,1) AS fltExchangeRateBizDoc
		,@vcBaseCurrency AS vcBaseCurrency
		,@vcForeignCurrency AS vcForeignCurrency
		,ISNULL(@vcPOppName,'') AS vcPOppName
		,ISNULL(@vcBizDocID,'') AS vcBizDocID
		,ISNULL(@bitPPVariance,0) AS bitPPVariance
		,@numDivisionID AS numDivisionID
		,ISNULL(@bitAutolinkUnappliedPayment,0) AS bitAutolinkUnappliedPayment                                            
END
GO
