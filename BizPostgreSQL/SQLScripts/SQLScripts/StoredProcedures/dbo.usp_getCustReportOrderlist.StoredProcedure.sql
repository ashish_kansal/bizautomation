/****** Object:  StoredProcedure [dbo].[usp_getCustReportOrderlist]    Script Date: 07/26/2008 16:17:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getcustreportorderlist')
DROP PROCEDURE usp_getcustreportorderlist
GO
CREATE PROCEDURE [dbo].[usp_getCustReportOrderlist]
@ReportId as numeric =0
as

select * from CustReportOrderlist where numCustomReportId = @ReportId order by numOrder
GO
