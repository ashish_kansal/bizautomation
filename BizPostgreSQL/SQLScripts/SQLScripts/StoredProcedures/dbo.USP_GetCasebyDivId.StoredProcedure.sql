/****** Object:  StoredProcedure [dbo].[USP_GetCasebyDivId]    Script Date: 07/26/2008 16:16:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getcasebydivid')
DROP PROCEDURE usp_getcasebydivid
GO
CREATE PROCEDURE [dbo].[USP_GetCasebyDivId]    
  
@numdivisionid numeric(9)  ,  
@byteMode bit,  
@numDomainId numeric(9)  
as     
if @bytemode = 1  
begin  
select vcCompanyName+' - '+dbo.fn_GetContactName(cases.numContactId)+' - '+ vcCaseNumber as vcCaseNumber,numCaseId from cases 
join Divisionmaster dm on    cases.numDivisionId =  dm.numDivisionId
join companyinfo comp on comp.numCompanyId =  dm.numCompanyId
where cases.numDivisionId = @numDivisionId  and cases.numdomainid=@numdomainid

end  
  
if @bytemode = 0  
begin  
select vcCompanyName+' - '+dbo.fn_GetContactName(cases.numContactId)+' - '+ vcCaseNumber as vcCaseNumber,numCaseId from cases 
join Divisionmaster dm on    cases.numDivisionId =  dm.numDivisionId
join companyinfo comp on comp.numCompanyId =  dm.numCompanyId
where cases.numDivisionId = @numDivisionId  and cases.numdomainid=@numdomainid and numStatus <> 136 
end
GO
