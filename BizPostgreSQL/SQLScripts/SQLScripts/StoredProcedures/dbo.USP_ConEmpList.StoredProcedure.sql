-- Created By Anoop Jayaraj          
-- exec USP_ConEmpList @numDomainID=72,@bitPartner=1,@numContactID=0
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_conemplist')
DROP PROCEDURE usp_conemplist
GO
CREATE PROCEDURE [dbo].[USP_ConEmpList]         
@numDomainID as numeric(9)=0,    
@bitPartner as bit=0,    
@numContactID as numeric(9)=0            
as       
    
if @bitPartner=0    
begin        
 SELECT A.numContactID,A.vcFirstName+' '+A.vcLastName as vcUserName          
 from UserMaster UM         
 join AdditionalContactsInformation A        
 on UM.numUserDetailId=A.numContactID          
 where UM.numDomainID=@numDomainID and UM.numDomainID=A.numDomainID and UM.intAssociate=1  
 --AND [bitActivateFlag]=1
-- union    
-- select  A.numContactID,vcCompanyName+' - '+A.vcFirstName+' '+A.vcLastName as vcUserName    
-- from AdditionalContactsInformation A     
-- join DivisionMaster D    
-- on D.numDivisionID=A.numDivisionID    
-- join ExtarnetAccounts E     
-- on E.numDivisionID=D.numDivisionID    
-- join ExtranetAccountsDtl DTL    
-- on DTL.numExtranetID=E.numExtranetID    
-- join CompanyInfo C    
-- on C.numCompanyID=D.numCompanyID    
-- where A.numDomainID=@numDomainID and  C.numDomainID=A.numDomainID and D.numDomainID=A.numDomainID    
-- and D.numDivisionID <>(select numDivisionID from domain where numDomainID=@numDomainID)    
end    
if @bitPartner=1    
begin        
 SELECT A.numContactID,A.vcFirstName+' '+A.vcLastName as vcUserName          
 from UserMaster UM         
 join AdditionalContactsInformation A        
 on UM.numUserDetailId=A.numContactID          
 where UM.numDomainID=@numDomainID    
-- union    
-- select  A.numContactID,vcCompanyName+' - '+A.vcFirstName+' '+A.vcLastName as vcUserName    
-- from AdditionalContactsInformation A     
-- join DivisionMaster D    
-- on D.numDivisionID=A.numDivisionID    
-- join CompanyInfo C    
-- on C.numCompanyID=D.numCompanyID    
-- where A.numContactID=@numContactID and D.numDivisionID <>(select numDivisionID from domain where numDomainID=@numDomainID)    
end 