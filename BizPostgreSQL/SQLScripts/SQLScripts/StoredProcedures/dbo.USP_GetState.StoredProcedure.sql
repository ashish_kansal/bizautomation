/****** Object:  StoredProcedure [dbo].[USP_GetState]    Script Date: 07/26/2008 16:18:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- created By Anoop Jayaraj  
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getstate')
DROP PROCEDURE usp_getstate
GO
CREATE PROCEDURE [dbo].[USP_GetState]       
@numDomainID as numeric(9)=0,    
@numCountryID as numeric(9)=0,
@vcAbbreviations AS VARCHAR(300)=''      
AS
BEGIN
	IF LEN(ISNULL(@vcAbbreviations,'')) > 0 AND ISNULL(@numCountryID,0) > 0
	BEGIN
		SELECT 
			numStateID 
		FROM 
			State  
		JOIN 
			ListDetails 
		ON 
			numListItemID = numCountryID     
		WHERE 
			State.numDomainID=@numDomainID 
			AND numCountryID=@numCountryID 
			AND @vcAbbreviations IN (vcAbbreviations,vcState)
		ORDER BY 
			vcState
	END
	ELSE IF LEN(@vcAbbreviations) > 0 AND ISNULL(@numCountryID,0) = 0
	BEGIN
		SELECT 
			numStateID
			,numCountryID
			,vcState
		FROM 
			State  
		JOIN 
			ListDetails 
		ON 
			numListItemID = numCountryID     
		WHERE 
			State.numDomainID=@numDomainID 
			AND (LOWER(ISNULL(vcAbbreviations,'')) = LOWER(@vcAbbreviations) OR LOWER(ISNULL(vcState,'')) = LOWER(@vcAbbreviations))
		ORDER BY 
			vcState
	END
	ELSE
	BEGIN   
		SELECT 
			numStateID
			,vcState
			,numCountryID
			,LD.vcData
			,vcAbbreviations
			,ISNULL(numShippingZone,0) AS numShippingZone
			,ISNULL(LDShippingZone.vcData,'') AS vcShippingZone
		FROM 
			State  
		JOIN 
			ListDetails LD 
		ON 
			LD.numListItemID = numCountryID     
		LEFT JOIN
			ListDetails LDShippingZone
		ON
			State.numShippingZone = LDShippingZone.numListItemID
		WHERE 
			State.numDomainID = @numDomainID 
			AND numCountryID=@numCountryID
		ORDER BY 
			vcState
	END
END
GO
