set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go

--created by anoop jayaraj                      
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_managetrackingvisitors')
DROP PROCEDURE usp_managetrackingvisitors
GO
CREATE PROCEDURE [dbo].[USP_ManageTrackingVisitors]                      
@vcUserHostAddress as varchar(100)='',
@vcUserDomain as varchar(100)='',
@vcUserAgent as varchar(1000)='',
@vcBrowser as varchar(50)='',
@vcCrawler as varchar(50)='',
@vcURL as varchar(500)='',
@vcReferrer as varchar(500)='',
@numVisits as numeric(9)=0,
@vcOrginalURL as varchar(500)='',                      
@vcOrginalRef as varchar(500)='',                      
@numVistedPages as numeric(9)=0,                      
@vcTotalTime as varchar(100)='',                      
@strURL as text='',                               
@vcCountry as varchar(50),                
@IsVisited as bit,
@numDivisionID as numeric(9)  ,    
@numDomainId as numeric (9) =0,
@vcCampaign AS VARCHAR(500)
AS  
BEGIN
	SET @vcOrginalURL = replace(replace(replace(replace(isnull(@vcOrginalURL,'-'),'%3A',':'),'%3F','?'),'%3D','='),'%26','&')                      
	SET @vcOrginalRef = replace(replace(replace(replace(isnull(@vcOrginalRef,'-'),'%3A',':'),'%3F','?'),'%3D','='),'%26','&')  
	SET @vcURL = replace(replace(replace(replace(isnull(@vcURL,'-'),'%3A',':'),'%3F','?'),'%3D','='),'%26','&')                      
	SET @vcReferrer = replace(replace(replace(replace(isnull(@vcReferrer,'-'),'%3A',':'),'%3F','?'),'%3D','='),'%26','&')
           
	DECLARE @numIdentity AS NUMERIC           
	DECLARE @vcTime AS VARCHAR(15)           

	/*Check for online campaign existance*/   
	DECLARE @numCampaignID NUMERIC(9)
	IF @numDivisionID > 0
	BEGIN
		SELECT TOP 1 
			@numCampaignID = [numCampaignId] 
		FROM 
			[CampaignMaster] 
		WHERE 
			 vcCampaignCode =@vcCampaign
			AND [numDomainID]=@numDomainId 
			AND [bitIsOnline] = 1
	END          
          
          


	SELECT 
		@IsVisited = COUNT(*) 
	FROM 
		TrackingVisitorsHDR
	WHERE 
		vcUserHostAddress=@vcUserHostAddress 
		AND dtCreated BETWEEN CONVERT(VARCHAR(20),GETUTCDATE(),101) AND CONVERT(VARCHAR(20),DATEADD(DAY,1,GETUTCDATE()),101) 
		AND numDomainID= @numDomainID


		IF @numDivisionID > 0
		BEGIN
			/*Check if previous visit had already created Division- if yes then update old record else create new record for TrackingVisitorsHDR */          
			IF (SELECT 
					COUNT(*) 
				FROM 
					TrackingVisitorsHDR
				WHERE 
					numDomainID= @numDomainID
					AND ISNULL([numDivisionID],0) = @numDivisionID) > 0
			BEGIN
				SET @IsVisited = 1
			END
		END
       
       
PRINT   @IsVisited
if @IsVisited=0          
begin
		declare @startPos as integer          
		declare @EndPos as integer          
		declare @vcSearchTerm as varchar(100)          
		          
		          
		          
		 SELECT @startPos=CHARINDEX( 'Referrer', @vcURL)          
		 if @startPos>0          
		 begin          
		  set @vcReferrer=substring(@vcURL,@startPos+9,500)          
				end          
		           
		 SELECT @startPos=CHARINDEX( '&q=', @vcReferrer)          
		 if @startPos>0          
		 begin          
		  set @vcSearchTerm=substring(@vcReferrer,@startPos+1,150)          
		            
		  SELECT @EndPos=CHARINDEX( '&', @vcSearchTerm)          
		  if @EndPos= 0 set @EndPos=100          
		  set @vcSearchTerm=substring(@vcSearchTerm,3,@EndPos-3)          
		           
		 end          
		 SELECT @startPos=CHARINDEX( '&p=', @vcReferrer)          
		 if @startPos>0          
		 begin          
		  set @vcSearchTerm=substring(@vcReferrer,@startPos+1,150)          
		            
		            
		  SELECT @EndPos=CHARINDEX( '&', @vcSearchTerm)          
		  if @EndPos= 0 set @EndPos=100          
		  set @vcSearchTerm=substring(@vcSearchTerm,3,@EndPos-3)          
		           
		 end           
		 SELECT @startPos=CHARINDEX( '?q=', @vcReferrer)          
		 if @startPos>0          
		 begin          
		  set @vcSearchTerm=substring(@vcReferrer,@startPos+1,150)          
		            
		  SELECT @EndPos=CHARINDEX( '&', @vcSearchTerm)          
		  if @EndPos= 0 set @EndPos=100          
		  set @vcSearchTerm=substring(@vcSearchTerm,3,@EndPos-3)          
		           
		 end          
		 SELECT @startPos=CHARINDEX( '?p=', @vcReferrer)          
		 if @startPos>0          
		 begin          
		  set @vcSearchTerm=substring(@vcReferrer,@startPos+1,150)          
		            
		            
		  SELECT @EndPos=CHARINDEX( '&', @vcSearchTerm)          
		  if @EndPos= 0 set @EndPos=100          
		  set @vcSearchTerm=substring(@vcSearchTerm,3,@EndPos-3)          
		           
		 end          
		          
		           
		 set @vcSearchTerm=replace(@vcSearchTerm,'+',' ')       
		      
		    
		if (@vcUserDomain like '%inktomisearch.com' or @vcUserDomain like '%ask.com'  or @vcUserDomain like  '%yahoo.net' or @vcUserDomain like  '%chello.nl' or @vcUserDomain like  '%linksmanager.com' or @vcUserDomain like  '%googlebot.com') set @vcCrawler='True'
  
   
   


         
          
          
          
			INSERT  INTO TrackingVisitorsHDR
					(
					  vcUserHostAddress,
					  vcUserDomain,
					  vcUserAgent,
					  vcBrowser,
					  vcCrawler,
					  vcURL,
					  vcReferrer,
					  vcSearchTerm,
					  numVisits,
					  vcOrginalURL,
					  vcOrginalRef,
					  numVistedPages,
					  vcTotalTime,
					  dtCreated,
					  vcCountry,
					  numDivisionID,
					  numDomainId,
					  [numCampaignID],
					  [vcDomainName]
					)
			VALUES  (
					  @vcUserHostAddress,
					  @vcUserDomain,
					  @vcUserAgent,
					  @vcBrowser,
					  @vcCrawler,
					  @vcURL,
					  @vcReferrer,
					  @vcSearchTerm,
					  @numVisits,
					  @vcOrginalURL,
					  @vcOrginalRef,
					  @numVistedPages,
					  @vcTotalTime,
					  GETUTCDATE(),
					  @vcCountry,
					  @numDivisionID,
					  @numDomainId,
					  @numCampaignID,
					  dbo.GetDomainNameFromURL(@vcOrginalURL)
					)           
			          
			select @numIdentity=@@identity   

	

                    
 end          
else          
begin          
          
	select top 1 @numIdentity=numTrackingID,@vcTime=vcTotalTime from TrackingVisitorsHDR where vcUserHostAddress=@vcUserHostAddress order by numTrackingID desc          
	declare @sec1 as integer          
	declare @min1 as integer           
	declare @hour1 as integer          
	declare @sec2 as integer          
	declare @min2 as integer          
	declare @hour2 as integer          
	set @sec1=substring(@vcTime,7,2)          
	set @min1=substring(@vcTime,4,2)          
	set @hour1=left(@vcTime,2)          
	set @sec2=substring(@vcTotalTime,7,2)          
	set @min2=substring(@vcTotalTime,4,2)          
	set @hour2=left(@vcTotalTime,2)          
	--print @sec1          
	--print @min1          
	--print @hour1          
	--print @sec2          
	--print @min2          
	--print @hour2          
	          
	set @sec1=@sec1+@sec2          
	 if @sec1>=60           
	 begin          
	  set @sec1=@sec1-60          
	  set @min1=@min1+1          
	 end          
	set @min1=@min1+@min2          
	 if @min1>=60           
	 begin          
	  set @min1=@min1-60          
	  set @hour1=@hour1+1           
	 end          
	set @hour1=@hour1+@hour2          
	--print @sec1          
	--print @min1          
	--print @hour1      
	if len(@hour1)=1 set @vcTotalTime='0'+convert(varchar,@hour1)+':'      
	else set @vcTotalTime=convert(varchar,@hour1)+':'      
	if len(@min1)=1  set @vcTotalTime=@vcTotalTime+'0'+convert(varchar,@min1)+':'      
	else set @vcTotalTime=@vcTotalTime+convert(varchar,@min1)+':'      
	      
	         
	if len(@sec1)=1 set @vcTotalTime=@vcTotalTime+'0'+convert(varchar,@sec1)      
	else set @vcTotalTime=@vcTotalTime+convert(varchar,@sec1)      
	         
	      
--set @vcTotalTime=convert(varchar,@hour1)+':'+convert(varchar,@min1)+':'+convert(varchar,@sec1)          
--print @vcTotalTime          
end          
          
                           
 DECLARE @hDoc int                            
 EXEC sp_xml_preparedocument @hDoc OUTPUT, @strURL                            
                            
 insert into TrackingVisitorsDTL                            
  (numTracVisitorsHDRID,vcPageName,vcElapsedTime,numPageID,dtTime)                            
                             
 select @numIdentity,X.URL,X.ElapsedTime,(select numPageID from PageMasterWebAnlys where X.URL like '%'+ vcPageURL+'%' ),X.[Time] from(                            
 SELECT *FROM OPENXML (@hDoc,'/NewDataSet/Table',2)                            
 WITH  (                            
  URL varchar(500),                            
  ElapsedTime varchar(100),          
  [Time] datetime                             
  ))X                   
  EXEC sp_xml_removedocument @hDoc                   
                          
update TrackingVisitorsHDR set vcTotalTime=@vcTotalTime,  numVistedPages=(select count(*) from TrackingVisitorsDTL where numTracVisitorsHDRID=@numIdentity)  where  numTrackingID= @numIdentity                      
        
if @numDivisionID>0         
begin        
  update TrackingVisitorsHDR set numDivisionID=@numDivisionID  where  numTrackingID= @numIdentity     
  
	DECLARE @vcTempOrigReferer VARCHAR(MAX)
	SELECT @vcTempOrigReferer=(CASE WHEN ISNULL(vcOrginalRef,'-') = '-' THEN ISNULL(vcOrginalURL,'-') ELSE vcOrginalRef END) FROM TrackingVisitorsHDR WHERE numTrackingID= @numIdentity

	DECLARE @numTempDomainID NUMERIC(18,0)
	SELECT @numTempDomainID=numDomainID FROM DivisionMaster WHERE numDivisionID=@numDivisionID

	IF EXISTS (SELECT CompanyInfo.numCompanyID FROM DivisionMaster INNER JOIN CompanyInfo ON DivisionMaster.numCompanyID=CompanyInfo.numCompanyId WHERE numDivisionID=@numDivisionID AND ISNULL(vcHow,0) = 0)
	BEGIN
		IF EXISTS (SELECT numTrackingID FROM TrackingVisitorsHDR WHERE numDomainID=@numTempDomainID AND ISNULL(numListItemID,0) > 0 AND CHARINDEX(vcOrginalRef,@vcTempOrigReferer) > 0)
		BEGIN
			DECLARE @vcHow NUMERIC(18,0)
	
			SELECT TOP 1
				@vcHow=numListItemID 
			FROM 
				TrackingVisitorsHDR 
			WHERE 
				numDomainID=@numTempDomainID 
				AND ISNULL(numListItemID,0) > 0 
				AND @vcTempOrigReferer LIKE CONCAT(vcOrginalRef,'%')
			ORDER BY
				LEN(ISNULL(vcOrginalRef,'')) DESC

			UPDATE
				C
			SET
				vcHow=@vcHow
			FROM
				DivisionMaster D
			INNER JOIN 
				CompanyInfo C
			ON  
				D.numCompanyID=C.numCompanyId 
			WHERE 
				numDivisionID=@numDivisionID
		END
	END
END


/*In Order to increase performace for campaign report we need to insert filtered data into seperate table-chintan*/
IF @numCampaignID >0 AND @numIdentity>0 AND @numDivisionID>0
	BEGIN
	-- making TrackingCampaignData table obsolete .. by directly updating data on division
	UPDATE dbo.DivisionMaster SET numCampaignID = @numCampaignID WHERE numDivisionID = @numDivisionID

	END
END
GO