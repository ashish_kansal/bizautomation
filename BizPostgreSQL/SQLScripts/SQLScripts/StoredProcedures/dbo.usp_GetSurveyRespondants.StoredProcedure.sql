/****** Object:  StoredProcedure [dbo].[usp_GetSurveyRespondants]    Script Date: 07/26/2008 16:18:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created By: Debasish Nag                
--Purpose: To show the Survey Respondents 
--exec usp_GetSurveyRespondants 15,1               
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getsurveyrespondants')
DROP PROCEDURE usp_getsurveyrespondants
GO
CREATE PROCEDURE [dbo].[usp_GetSurveyRespondants]      
 @numSurId Numeric(9)=0,  
 @numDomainID as numeric(9)=0                  
AS                                  
BEGIN                    
select srm.numSurId, sm.vcSurName, srm.numRespondantID, vcFirstname+ ' '+vcLastname as vcRespondentName,     
vcCompanyName,     
CASE WHEN IsNull(srm.numRegisteredRespondentContactId, 0) = '0' Then 'No' Else 'Yes' End as           
bitAddedToBz,                 
srm.numSurRating, srm.numRegisteredRespondentContactId ,dtDateofResponse,vcEmail,A.bintCreatedDate            
from SurveyRespondentsMaster srm  
join SurveyMaster sm  
on   srm.numSurId = sm.numSurID  
left join  AdditionalContactsInformation A  
on    numContactId=numRegisteredRespondentContactId  and A.numDomainID=@numDomainID 
left join DivisionMaster D   
on D.numDivisionID=A.numDivisionID  
left join CompanyInfo C  
on C.numCompanyID=D.numCompanyID  
where  sm.numSurId = srm.numSurId                         
and sm.numSurId = @numSurId  and srm.numDomainID= @numDomainID            
END
GO
