/****** Object:  StoredProcedure [dbo].[USP_ECampaignList]    Script Date: 07/26/2008 16:15:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj        
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_ecampaignlist')
DROP PROCEDURE usp_ecampaignlist
GO
CREATE PROCEDURE [dbo].[USP_ECampaignList]            
 @numUserCntID numeric(9)=0,            
 @numDomainID numeric(9)=0,              
 @SortChar char(1)='0',           
 @CurrentPage int,            
 @PageSize int,            
 @TotRecs int output,            
 @columnName as Varchar(50),            
 @columnSortOrder as Varchar(10)              
as            
            
            
               
             
            
create table #tempTable ( ID INT IDENTITY PRIMARY KEY,             
 numECampaignID numeric(9),            
 vcECampName varchar(100),            
 txtDesc text,
 NoOfUsersAssigned INT,
 NoOfEmailSent INT,
 NoOfEmailOpened INT,
 NoOfLinks INT,
 NoOfLinkClicked INT        
 )            
            
            
declare @strSql as varchar(8000)            
          
          
            
set @strSql='select 
numECampaignID,          
vcECampName,          
txtDesc, 
ISNULL((SELECT COUNT(*) FROM ConECampaign WHERE numECampaignID = ECampaign.numECampaignID AND ISNULL(ConECampaign.bitEngaged,0) = 1 ),0) AS NoOfUsersAssigned,
ISNULL((SELECT COUNT(*) FROM ConECampaignDTL INNER JOIN ECampaignDTLs ON ConECampaignDTL.numECampDTLID = ECampaignDTLs.numECampDTLId WHERE ISNULL(ECampaignDTLs.numEmailTemplate,0) > 0 AND numECampID = ECampaign.numECampaignID AND ISNULL(bitSend,0) = 1 AND ISNULL(tintDeliveryStatus,0) = 1),0) AS NoOfEmailSent,
ISNULL((SELECT COUNT(*) FROM ConECampaignDTL INNER JOIN ECampaignDTLs ON ConECampaignDTL.numECampDTLID = ECampaignDTLs.numECampDTLId WHERE ISNULL(ECampaignDTLs.numEmailTemplate,0) > 0 AND numECampID = ECampaign.numECampaignID AND ISNULL(bitSend,0) = 1 AND ISNULL(tintDeliveryStatus,0) = 1 AND ISNULL(bitEmailRead,0) = 1),0) AS NoOfEmailOpened,
(SELECT COUNT(*) FROM ConECampaignDTLLinks WHERE numConECampaignDTLID IN (SELECT ISNULL(CECDTL.numConECampDTLID,0) FROM ConECampaignDTL CECDTL WHERE numConECampID IN (SELECT ISNULL(CEC.numConEmailCampID,0) FROM ConECampaign CEC WHERE numECampaignID = ECampaign.numECampaignID))) AS NoOfLinks,
(SELECT COUNT(*) FROM ConECampaignDTLLinks WHERE ISNULL(bitClicked,0) = 1 AND numConECampaignDTLID IN (SELECT ISNULL(CECDTL.numConECampDTLID,0) FROM ConECampaignDTL CECDTL WHERE numConECampID IN (SELECT ISNULL(CEC.numConEmailCampID,0) FROM ConECampaign CEC WHERE numECampaignID = ECampaign.numECampaignID))) AS NoOfLinkClicked
from ECampaign          
where ISNULL(bitDeleted,0) = 0 AND numDomainID= '+ convert(varchar(15),@numDomainID)           
            
if @SortChar <>'0' set @strSql=@strSql + ' And vcECampName like '''+@SortChar+'%'''          
if @columnName<>''  set @strSql=@strSql + ' ORDER BY ' + @columnName +' '+ @columnSortOrder   
  
print @strSql         
insert into #tempTable
(            
	numECampaignID,            
	vcECampName,            
	txtDesc,
	NoOfUsersAssigned,
	NoOfEmailSent,
	NoOfEmailOpened,
	NoOfLinks,
	NoOfLinkClicked 
)            
exec 
(
	@strSql
)             
            
  declare @firstRec as integer             
  declare @lastRec as integer            
 set @firstRec= (@CurrentPage-1) * @PageSize            
     set @lastRec= (@CurrentPage*@PageSize+1)            
--Don't change column sequence because its being used for Drip Campaign DropDown in Contact Details
select numECampaignID,vcECampName,txtDesc,NoOfUsersAssigned,NoOfEmailSent,NoOfEmailOpened,NoOfLinks,NoOfLinkClicked from #tempTable where ID > @firstRec and ID < @lastRec            
set @TotRecs=(select count(*) from #tempTable)            
drop table #tempTable
GO
