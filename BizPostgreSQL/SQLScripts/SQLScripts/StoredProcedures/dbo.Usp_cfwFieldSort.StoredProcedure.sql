/****** Object:  StoredProcedure [dbo].[Usp_cfwFieldSort]    Script Date: 07/26/2008 16:15:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_cfwfieldsort')
DROP PROCEDURE usp_cfwfieldsort
GO
CREATE PROCEDURE  [dbo].[Usp_cfwFieldSort]  
@strFiledId as varchar(4000)='',  
@RelaionId as numeric(9),  
@TabId as NUMERIC(9),
@numDomainID AS NUMERIC(9)
as  
begin  
declare @separator_position as integer  
declare @strPosition as varchar(1000)  
declare @count as integer  
declare @strSql as varchar(4000)  
  
set @strSql= 'delete from CFW_Fld_Dtl where numFieldId not in ('   
 set @count =1  
  
   while patindex('%,%' , @strFiledId) <> 0  
    begin -- Begin for While Loop  
      select @separator_position =  patindex('%,%' , @strFiledId)  
    select @strPosition = left(@strFiledId, @separator_position - 1)  
       select @strFiledId = stuff(@strFiledId, 1, @separator_position,'')  
      
   if exists (select * from CFW_Fld_Dtl where numFieldId=@strPosition and numRelation=@RelaionId )  
   begin  
    update CFW_Fld_Dtl set numOrder=@count where numFieldId=@strPosition and numRelation=@RelaionId  
     
   end  
   else  
   begin  
     insert into CFW_Fld_Dtl (numFieldId,numRelation,numOrder)  
     values (@strPosition,@RelaionId,@count)  
   end  
   set @strSql=@strSql + @strPosition +','  
  
   set @count=@count+1  
   end  
   set @strSql=@strSql  +'0) and numRelation='+ convert(varchar(10),@RelaionId) +' and numFieldId in( Select fld_id from CFW_Fld_Master where subgrp='+ convert(varchar(10),@TabId) +' and numDomainID= '+ CONVERT(VARCHAR(10),@numDomainID) +')'
     
   exec (@strSql)  
end
GO
