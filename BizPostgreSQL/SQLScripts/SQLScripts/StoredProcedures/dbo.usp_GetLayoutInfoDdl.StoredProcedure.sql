/****** Object:  StoredProcedure [dbo].[usp_GetLayoutInfoDdl]    Script Date: 07/26/2008 16:17:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
exec usp_GetLayoutInfoDdl @numUserCntId=85098,@intcoulmn=0,@numDomainID=110,@Ctype='P',@PageId=1,@numRelation=46
exec usp_GetLayoutInfoDdl @numUserCntId=85098,@intcoulmn=1,@numDomainID=110,@Ctype='P',@PageId=1,@numRelation=46
*/
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getlayoutinfoddl')
DROP PROCEDURE usp_getlayoutinfoddl
GO
CREATE PROCEDURE [dbo].[usp_GetLayoutInfoDdl]                                
@numUserCntId as numeric(9)=0,                              
@intcoulmn as numeric(9),                              
@numDomainID as numeric(9)=0 ,                    
--@Ctype as char ,              
@PageId as numeric(9)=4,              
@numRelation as numeric(9)=0,
@numFormID as numeric(9)=0,
@tintPageType TINYINT             
as                        
                      
 if( @intcoulmn <> 0 )--Fields added to layout
begin                         
IF(@tintPageType=3)
BEGIN
IF((SELECT COUNT(*) FROM View_DynamicColumns WHERE      numUserCntID=@numUserCntID and  numDomainID=@numDomainID
 and numFormId=@numFormID and numRelCntType=@numRelation 
 AND tintPageType=@tintPageType AND 
  1=(CASE WHEN @tintPageType=2 THEN CASE WHEN ISNULL(bitAddField,0)=1 THEN 1 ELSE 0 end 
		  WHEN @tintPageType=3 THEN CASE WHEN ISNULL(bitDetailField,0)=1 THEN 1 ELSE 0 END 
		  ELSE 0 END))=0)
BEGIN
		INSERT INTO DycFormConfigurationDetails
			(
				numFormID,
				numFieldID,
				intColumnNum,
				intRowNum,
				numDomainID,
				numAuthGroupID,
				numRelCntType,
				tintPageType,
				bitCustom,
				numUserCntID,
				numFormFieldGroupId,
				bitDefaultByAdmin
			)
		SELECT numFormID,numFieldID,intColumnNum,intRowNum,@numDomainID,numGroupID,@numRelation,@tintPageType,bitCustom,@numUserCntId,numFormFieldGroupId,1 FROM 
		BizFormWizardMasterConfiguration WHERE numDomainID=@numDomainID
		AND numFormID=@numFormID AND tintPageType=@tintPageType
		AND numRelCntType=@numRelation
		AND numGroupID=(SELECT TOP 1 numGroupID FROM  UserMaster WHERE  numDomainID =  @numDomainID AND numUserDetailID = @numUserCntId)
END
END
SELECT ISNULL(vcCultureFieldName,vcFieldName) AS vcFieldName,numFieldId,
convert(char(1),bitCustom) as bitCustomField,tintRow,bitRequired,ISNULL(bitDefaultByAdmin,0) AS bitDefaultByAdmin
FROM View_DynamicColumns
WHERE numUserCntID=@numUserCntID and  numDomainID=@numDomainID  and tintColumn = @intcoulmn      
 and numFormId=@numFormID and ISNULL(bitCustom,0)=0 and numRelCntType=@numRelation 
 AND tintPageType=@tintPageType AND 
  1=(CASE WHEN @tintPageType=2 THEN CASE WHEN ISNULL(bitAddField,0)=1 THEN 1 ELSE 0 end 
		  WHEN @tintPageType=3 THEN CASE WHEN ISNULL(bitDetailField,0)=1 THEN 1 ELSE 0 END 
		  ELSE 0 END)  order by tintRow  
       
if @pageid =1 or @pageid =4  OR @pageid =12 OR @pageid =13 OR @pageid =14
 begin    
 
 select vcfieldName,numFieldId,'1' as bitCustomField,tintRow,bitIsRequired AS bitRequired,ISNULL(bitDefaultByAdmin,0) AS bitDefaultByAdmin
  from View_DynamicCustomColumns_RelationShip
 where grp_id=@PageId and numDomainID=@numDomainID and subgrp=0           
 and numUserCntID=@numUserCntID and tintColumn = @intcoulmn
 AND numFormId=@numFormID and ISNULL(bitCustom,0)=1 and numRelCntType=@numRelation AND tintPageType=@tintPageType
  
end      
      
if @PageId= 2 or  @PageId= 3 or @PageId= 5 or @PageId= 6 or @PageId= 7 or @PageId= 8    or @PageId= 11               
begin 

 select vcfieldName,numFieldId,'1' as bitCustomField,tintRow,bitIsRequired AS bitRequired,ISNULL(bitDefaultByAdmin,0) AS bitDefaultByAdmin
  from View_DynamicCustomColumns
 where grp_id=@PageId and numDomainID=@numDomainID and subgrp=0           
 and numUserCntID=@numUserCntID and  tintColumn = @intcoulmn
 AND numFormId=@numFormID and ISNULL(bitCustom,0)=1 and numRelCntType=@numRelation AND tintPageType=@tintPageType

end       
                      
end                        
                        
                          
if @intcoulmn = 0 --Available Fields
begin                          
                      
 
 SELECT ISNULL(vcCultureFieldName,vcFieldName) AS vcFieldName,numFieldId,'0' as bitCustomField,bitRequired, 0 AS bitDefaultByAdmin
FROM View_DynamicDefaultColumns
 WHERE numFormId=@numFormID AND numDomainID=@numDomainID  AND
   1=(CASE WHEN @tintPageType=2 THEN CASE WHEN ISNULL(bitAddField,0)=1 THEN 1 ELSE 0 end 
		 WHEN @tintPageType=3 THEN CASE WHEN ISNULL(bitDetailField,0)=1 THEN 1 ELSE 0 END
		 ELSE 0 END)
 AND numFieldId NOT IN 
 (SELECT numFieldId FROM View_DynamicColumns
WHERE numUserCntID=@numUserCntID and  numDomainID=@numDomainID       
 and numFormId=@numFormID and ISNULL(bitCustom,0)=0 and numRelCntType=@numRelation 
 AND tintPageType=@tintPageType AND 1=(CASE WHEN @tintPageType=2 THEN Case WHEN ISNULL(bitAddField,0)=1 THEN 1 ELSE 0 end 
		 WHEN @tintPageType=3 THEN Case WHEN ISNULL(bitDetailField,0)=1 THEN 1 ELSE 0 END 
		 ELSE 0 END)) ORDER BY ISNULL(vcCultureFieldName,vcFieldName)

       
if @pageid=4 or @pageid=1  OR @pageid =12 OR @pageid =13  OR @pageid =14                
begin           

 select fld_label as vcfieldName ,fld_id as numFieldId,'1' as bitCustomField,CAST(0 as bit) AS bitRequired, 0 AS bitDefaultByAdmin
  from CFW_Fld_Master CFM 
						  left join CFw_Grp_Master on subgrp=CFM.Grp_id                              
 where CFM.grp_id=@PageId and CFM.numDomainID=@numDomainID and subgrp=0 
 AND fld_id not IN (SELECT numFieldId FROM View_DynamicCustomColumns_RelationShip where           
 numUserCntID=@numUserCntID and  numDomainID=@numDomainID  
 AND numFormId=@numFormID and ISNULL(bitCustom,0)=1 and numRelCntType=@numRelation AND tintPageType=@tintPageType)
 order by subgrp 

end        
      
if @PageId= 2 or  @PageId= 3 or @PageId= 5 or @PageId= 6 or @PageId= 7 or @PageId= 8   or @PageId= 11                
begin 

 select fld_label as vcfieldName ,fld_id as numFieldId,'1' as bitCustomField,CAST(0 as bit) AS bitRequired, 0 AS bitDefaultByAdmin
  from CFW_Fld_Master CFM left join CFw_Grp_Master on subgrp=CFM.Grp_id                              
 where CFM.grp_id=@PageId and CFM.numDomainID=@numDomainID and subgrp=0 
 AND fld_id not IN (SELECT numFieldId FROM View_DynamicCustomColumns where           
 numUserCntID=@numUserCntID and  numDomainID=@numDomainID  
 AND numFormId=@numFormID and ISNULL(bitCustom,0)=1 and numRelCntType=@numRelation AND tintPageType=@tintPageType)
 order by subgrp 
                          
end       
      
end
GO
