GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj              
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getuserswithdomains')
DROP PROCEDURE usp_getuserswithdomains
GO
CREATE PROCEDURE [dbo].[usp_GetUsersWithDomains]                            
 @numUserID NUMERIC(18,0)=0,
 @numDomainID NUMERIC(18,0)                       
AS                            
BEGIN                            
	IF @numUserID = 0 AND @numDomainID > 0 --Check if request is made while session is on
	BEGIN                            
		SELECT 
			numUserID
			,vcUserName
			,UserMaster.numGroupID
			,ISNULL(vcGroupName,'-') AS vcGroupName
			,vcUserDesc
			,ISNULL(ADC.vcfirstname,'') +' '+ ISNULL(ADC.vclastname,'') as Name
			,UserMaster.numDomainID
			,vcDomainName
			,UserMaster.numUserDetailId
			,ISNULL(UserMaster.SubscriptionId,'') AS SubscriptionId
			,ISNULL(UserMaster.tintDefaultRemStatus,0) tintDefaultRemStatus
			,ISNULL(UserMaster.numDefaultTaskType,0) numDefaultTaskType
			,ISNULL(UserMaster.bitOutlook,0) bitOutlook
			,UserMaster.vcLinkedinId
			,ISNULL(UserMaster.intAssociate,0) AS intAssociate
			,UserMaster.ProfilePic
			,UserMaster.numDashboardTemplateID
			,UserMaster.vcDashboardTemplateIDs
			,UserMaster.vcEmailAlias
		FROM 
			UserMaster                      
		JOIN 
			Domain                        
		ON 
			UserMaster.numDomainID =  Domain.numDomainID                       
		LEFT JOIN 
			AdditionalContactsInformation ADC                      
		ON 
			ADC.numContactid=UserMaster.numUserDetailId                     
		LEFT JOIN 
			AuthenticationGroupMaster GM                     
		ON 
			Gm.numGroupID= UserMaster.numGroupID                    
		WHERE 
			UserMaster.numDomainID = @numDomainID
			AND tintGroupType IN (1,4)         
		ORDER BY 
			Domain.numDomainID, vcUserDesc                            
	END   
	ELSE IF @numUserID = -1 AND @numDomainID > 0 --Support Email
	BEGIN                            
		SELECT 
			'' AS vcEmailId
			,vcImapUserName
			,ISNULL(bitUseUserName,0) bitUseUserName
			,ISNULL(ImapUserDTL.bitImap,0) bitImap
			,ImapUserDTL.vcImapServerUrl
			,ISNULL(ImapUserDTL.vcImapPassword,'') vcImapPassword
			,ImapUserDTL.bitSSl
			,ImapUserDTL.numPort 
			,'' AS vcEmailAlias
		FROM 
			[ImapUserDetails] ImapUserDTL   
		WHERE 
			ImapUserDTL.numUserCntId = @numUserID 
			AND ImapUserDTL.numDomainID = @numDomainID
	END                         
	ELSE                            
	BEGIN   
		DECLARE @vcSMTPServer VARCHAR(100)
			,@numSMTPPort NUMERIC(18,0)
			,@bitSMTPSSL BIT
			,@bitSMTPAuth BIT
			,@vcIMAPServer  VARCHAR(100)
			,@numIMAPPort NUMERIC(18,0)
			,@bitIMAPSSL BIT
			,@bitIMAPAuth BIT
	    
		SELECT
			@vcSMTPServer=vcSMTPServer
			,@numSMTPPort=numSMTPPort
			,@bitSMTPSSL=bitSMTPSSL
			,@bitSMTPAuth=bitSMTPAuth
			,@vcIMAPServer=vcIMAPServer
			,@numIMAPPort=numIMAPPort
			,@bitIMAPSSL=bitIMAPSSL
			,@bitIMAPAuth=bitIMAPAuth
		FROM
			UniversalSMTPIMAP
	    WHERE
			numDomainID=@numDomainID
		             
		SELECT       
			U.numUserID
			,vcUserName
			,numGroupID
			,vcUserDesc
			,U.numDomainID
			,vcDomainName
			,U.numUserDetailId
			,bitHourlyRate
			,bitActivateFlag
			,bitSalary
			,numDailyHours
			,bitOverTime
			,numLimDailHrs
			,ISNULL(monOverTimeRate,0) monOverTimeRate
			,bitMainComm
			,fltMainCommPer
			,bitRoleComm
			,(SELECT COUNT(*) FROM UserRoles WHERE UserRoles.numUserCntID=U.numUserDetailId) AS Roles
			,vcEmailid
			,ISNULL(vcEmailAlias,'') vcEmailAlias
			,vcPassword
			,ISNULL(ExcUserDTL.bitExchangeIntegration,0) bitExchangeIntegration
			,ISNULL(ExcUserDTL.bitAccessExchange,0) bitAccessExchange
			,ISNULL(ExcUserDTL.vcExchPassword,'') as vcExchPassword
			,ISNULL(ExcUserDTL.vcExchPath,'') as vcExchPath
			,ISNULL(ExcUserDTL.vcExchDomain,'') as vcExchDomain
			,ISNULL(U.SubscriptionId,'') as SubscriptionId
			,ISNULL(ImapUserDTL.bitImap,0) bitImap
			,ISNULL(ImapUserDTL.vcImapServerUrl,ISNULL(@vcIMAPServer,'')) vcImapServerUrl
			,ISNULL(ImapUserDTL.vcImapPassword,'') vcImapPassword
			,ISNULL(ImapUserDTL.bitSSl,ISNULL(@bitIMAPSSL,0)) bitSSl
			,ISNULL(ImapUserDTL.numPort,ISNULL(@numIMAPPort,0)) numPort
			,ISNULL(ImapUserDTL.bitUseUserName,ISNULL(@bitIMAPAuth,0)) bitUseUserName
			,(CASE WHEN ISNULL([vcSMTPServer],'') <> '' AND ISNULL(numSMTPPort,0) <> 0 THEN ISNULL(bitSMTPAuth,0) ELSE ISNULL(@bitSMTPAuth,0) END) bitSMTPAuth
			,ISNULL([vcSmtpPassword],'')vcSmtpPassword
			,(CASE WHEN ISNULL([vcSMTPServer],'') = '' THEN ISNULL(@vcSMTPServer,'') ELSE [vcSMTPServer] END) vcSMTPServer
			,(CASE WHEN ISNULL(numSMTPPort,0) = 0 THEN ISNULL(@numSMTPPort,0) ELSE numSMTPPort END) numSMTPPort
			,(CASE WHEN ISNULL([vcSMTPServer],'') <> '' AND ISNULL(numSMTPPort,0) <> 0 THEN ISNULL(bitSMTPSSL,0) ELSE ISNULL(@bitSMTPSSL,0) END) bitSMTPSSL
			,ISNULL(bitSMTPServer,0) bitSMTPServer
			,ISNULL(U.tintDefaultRemStatus,0) tintDefaultRemStatus
			,ISNULL(U.numDefaultTaskType,0) numDefaultTaskType
			,ISNULL(U.bitOutlook,0) bitOutlook
			,ISNULL(numDefaultClass,0) numDefaultClass
			,ISNULL(numDefaultWarehouse,0) numDefaultWarehouse
			,U.vcLinkedinId
			,ISNULL(U.intAssociate,0) as intAssociate
			,U.ProfilePic
			,U.numDashboardTemplateID
			,U.vcDashboardTemplateIDs
			,ISNULL(bitPayroll,0) bitPayroll
			,ISNULL(monHourlyRate,0) monHourlyRate
			,ISNULL(tintPayrollType,1) tintPayrollType
			,ISNULL(tintHourType,1) tintHourType
			,ISNULL(U.tintMailProvider,1) tintMailProvider
			,ISNULL(U.vcMailAccessToken,'') vcMailAccessToken
			,ISNULL(U.vcMailRefreshToken,'') vcMailRefreshToken
			,ISNULL(U.bitOauthImap,0) bitOauthImap
		FROM 
			UserMaster U                        
		JOIN 
			Domain D                       
		ON 
			U.numDomainID =  D.numDomainID       
		LEFT JOIN 
			ExchangeUserDetails ExcUserDTL      
		ON 
			ExcUserDTL.numUserID=U.numUserID                      
		LEFT JOIN 
			[ImapUserDetails] ImapUserDTL   
		ON 
			ImapUserDTL.numUserCntId = U.numUserDetailId   
			AND ImapUserDTL.numDomainID = D.numDomainID
		WHERE 
			U.numUserID=@numUserID                            
	 END                            
END
