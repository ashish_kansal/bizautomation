/****** Object:  StoredProcedure [dbo].[USP_GetChartAcountIdForAuthorizativeBizDocs]    Script Date: 07/26/2008 16:16:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created By Siva
/* By chintan
Both PROCEDURE 
USP_GetChartOfAccountIdForAuthoritativeBizDocs
USP_GetChartAcountIdForAuthorizativeBizDocs
Works same way .. so decided to Not to use this procedure
*/
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getchartacountidforauthorizativebizdocs')
DROP PROCEDURE usp_getchartacountidforauthorizativebizdocs
GO
CREATE PROCEDURE [dbo].[USP_GetChartAcountIdForAuthorizativeBizDocs]
(@numDomainId as numeric(9)=0,
 @numAccountTypeId as numeric(9)=0,
 @chBizDocItems as char(2)='')
As
Begin
	Select numAccountId From Chart_Of_Accounts Where numAcntTypeID=@numAccountTypeId And chBizDocItems=@chBizDocItems And numDomainId=@numDomainId
End
GO
