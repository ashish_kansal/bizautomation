/****** Object:  StoredProcedure [dbo].[USP_ManageShortCutGroupConf]    Script Date: 07/26/2008 16:20:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj          
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_manageshortcutgroupconf')
DROP PROCEDURE usp_manageshortcutgroupconf
GO
CREATE PROCEDURE [dbo].[USP_ManageShortCutGroupConf]            
@numGroupID as numeric(9)=0,            
@numDomainId as numeric(9)=0,            
@strFav as text='',        
@numTabId as numeric(9),
@bitDefaultTab AS BIT
AS
BEGIN        
	DELETE 
		ShortCutUsrCnf 
	WHERE 
		numTabId=@numTabId 
		AND numDomainId = @numDomainId 
		AND numGroupID = @numGroupID 
		AND numlinkId in (select numlinkId from ShortCutGrpconf where numTabId=@numTabId and numDomainId = @numDomainId and numGroupID =@numGroupID)	
  
	DELETE 
		ShortCutGrpconf 
	WHERE 
		numTabId=@numTabId 
		AND numDomainId = @numDomainId 
		AND numGroupID =@numGroupID  
-- end  
  
  
   DECLARE @hDoc INT            
               
   EXEC sp_xml_preparedocument @hDoc OUTPUT, @strFav  
               
    INSERT INTO ShortCutGrpconf            
    (
		numGroupId
		,numDomainId
		,numTabId
		,numOrder
		,numLinkId
		,bitInitialPage
		,tintLinkType
	)                        
    SELECT 
		@numGroupID
		,@numDomainId
		,@numTabId
		,X.*   
	FROM
	(
		SELECT 
			* 
		FROM 
			OPENXML (@hDoc,'/NewDataSet/Table',2)            
		WITH  
		(            
			numOrder numeric
			,numLinkId numeric
			,bitInitialPage bit
			,tintLinkType tinyint
		)
	)X            
                
    EXEC sp_xml_removedocument @hDoc            
      
	IF @bitDefaultTab = 1
	BEGIN
		UPDATE GroupTabDetails SET bitInitialTab = 0 WHERE numGroupID =@numGroupID
		UPDATE GroupTabDetails SET bitInitialTab = 1 WHERE numGroupID =@numGroupID AND numTabId=@numTabId

		UPDATE ShortCutGrpconf SET bitDefaultTab = 0 WHERE numDomainId = @numDomainId AND numGroupID =@numGroupID
		UPDATE ShortCutGrpconf SET bitDefaultTab = 1 WHERE numTabId=@numTabId AND numDomainId = @numDomainId AND numGroupID =@numGroupID
	END
END
GO
