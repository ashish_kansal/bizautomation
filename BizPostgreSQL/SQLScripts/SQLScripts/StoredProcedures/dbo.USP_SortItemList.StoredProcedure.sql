/****** Object:  StoredProcedure [dbo].[USP_SortItemList]    Script Date: 07/26/2008 16:21:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_sortitemlist')
DROP PROCEDURE usp_sortitemlist
GO
CREATE PROCEDURE [dbo].[USP_SortItemList]            
 @xmlStr text = ''  ,  
 @ListId as numeric,   
 @numDomainID as numeric  
AS            
  
if convert(varchar(10),@xmlStr) <> ''                                    
begin               
--DECLARE @tblTemp TABLE (      
--        numListItemID numeric(9),       
--        tintOrder numeric(9),       
--        numDomainID numeric(9)      
--    )      
--                             
  
delete listorder  where numdomainId = @numDomainID and numListId = @ListId  
                                                                             
DECLARE @hDoc4 int                   --inserting data in temp table                                                     
EXEC sp_xml_preparedocument @hDoc4 OUTPUT, @xmlStr      
        
INSERT INTO [ListOrder]  
           ([numListId]  
           ,[numListItemId]  
           ,[numDomainId]  
           ,[intSortOrder])  
       
select @ListId,x.numListItemID,@numDomainID,x.tintOrder from (                              
SELECT * FROM OPENXML (@hDoc4,'/NewDataSet/Table',2)                                                                        
 WITH  (                                                                    
  numListItemID numeric(9),                                
  tintOrder numeric(9),                                
  numDomainID numeric(9)      
))X                                 
                                  
                  
--EXEC sp_xml_removedocument @hDoc4          
----inserting data from temp table to main table(listdetails)      
--UPDATE ListDetails SET sintOrder = t.tintOrder FROM  @tblTemp t, ListDetails dtl        
-- WHERE dtl.numListItemID = t.numListItemID  and dtl.numDomainId= t.numDomainID        
        
   
end
GO
