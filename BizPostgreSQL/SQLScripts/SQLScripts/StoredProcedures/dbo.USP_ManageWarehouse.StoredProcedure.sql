/****** Object:  StoredProcedure [dbo].[USP_ManageWarehouse]    Script Date: 07/26/2008 16:20:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj      
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_managewarehouse')
DROP PROCEDURE usp_managewarehouse
GO
CREATE PROCEDURE [dbo].[USP_ManageWarehouse]      
@byteMode as tinyint=0,      
@numWareHouseID as numeric(9)=0,      
@vcWarehouse as varchar(50),          
@numDomainID as numeric(9)=0,
@numAddressID As numeric(18,0) = 0,
@vcPrintNodeAPIKey AS VARCHAR(100) = '',
@vcPrintNodePrinterID AS VARCHAR(100) = ''
AS      
BEGIN      
	IF @byteMode=0      
	BEGIN      
		IF @numWareHouseID=0      
		BEGIN
			INSERT INTO Warehouses 
			(
				vcWarehouse
				,numDomainID
				,numAddressID
				,vcPrintNodeAPIKey
				,vcPrintNodePrinterID	
			)      
			VALUES 
			(
				@vcWarehouse
				,@numDomainID
				,@numAddressID
				,@vcPrintNodeAPIKey
				,@vcPrintNodePrinterID
			)      
        
			DECLARE @numNewWarehouseID NUMERIC(18,0)
			SET @numNewWarehouseID = SCOPE_IDENTITY()


			INSERT INTO WareHouseItems 
			(
				numItemID, 
				numWareHouseID,
				numWLocationID,
				numOnHand,
				numReorder,
				monWListPrice,
				vcLocation,
				vcWHSKU,
				vcBarCode,
				numDomainID,
				dtModified
			) 
			SELECT
				numItemCode
				,@numNewWarehouseID
				,NULL
				,0
				,0
				,monListPrice
				,''
				,vcSKU
				,numBarCodeId
				,@numDomainID
				,GETDATE()
			FROM
				Item
			WHERE
				numDomainID=@numDomainID
				AND charItemType='P'
				AND 1 = (CASE WHEN ISNULL(numItemGroup,0) > 0 THEN (CASE WHEN ISNULL(bitMatrix,0)=1 THEN 1 ELSE 0 END) ELSE 1 END)

			INSERT  INTO WareHouseItems_Tracking
            (
              numWareHouseItemID,
              numOnHand,
              numOnOrder,
              numReorder,
              numAllocation,
              numBackOrder,
              numDomainID,
              vcDescription,
              tintRefType,
              numReferenceID,
              dtCreatedDate,numModifiedBy,monAverageCost,numTotalOnHand,dtRecordDate
            )
            SELECT  
				WHI.numWareHouseItemID
				,ISNULL(WHI.numOnHand,0)
				,ISNULL(WHI.numOnOrder,0)
				,ISNULL(WHI.numReorder,0)
				,ISNULL(WHI.numAllocation,0)
				,ISNULL(WHI.numBackOrder,0)
				,WHI.numDomainID
				,'INSERT WareHouse'
				,1
				,I.numItemCode
				,GETUTCDATE()
				,0
				,(CASE WHEN ISNULL(I.bitVirtualInventory,0) = 1 THEN 0 ELSE ISNULL(I.monAverageCost,0) END)
				,(SELECT SUM(ISNULL(numOnHand, 0)) FROM dbo.WareHouseItems WHERE numItemID=I.numItemCode)
				,GETUTCDATE()
            FROM 
				Item I 
			INNER JOIN 
				WareHouseItems WHI 
			ON 
				I.numItemCode=WHI.numItemID
			WHERE 
				WHI.numWareHouseID = @numNewWarehouseID
				AND [WHI].[numDomainID] = @numDomainID


			SELECT @numNewWarehouseID     
		END      
		ELSE IF @numWareHouseID>0      
		BEGIN
			UPDATE 
				Warehouses 
			SET    
				vcWarehouse= @vcWarehouse
				,numAddressID=@numAddressID
				,vcPrintNodeAPIKey=@vcPrintNodeAPIKey
				,vcPrintNodePrinterID=@vcPrintNodePrinterID
			WHERE 
				numWareHouseID=@numWareHouseID      
		
			SELECT @numWareHouseID      
		END      
	END
	ELSE IF @byteMode=1      
	BEGIN     
		IF (SELECT COUNT(*) FROM [WareHouseItems] WHERE numWarehouseID = @numWareHouseID  AND (ISNULL(numOnHand,0) > 0 OR ISNULL(numOnOrder,0) > 0 OR ISNULL(numAllocation,0) > 0 OR ISNULL(numBackOrder,0) > 0)) > 0
		BEGIN
			RAISERROR('Dependant', 16, 1)
		END
		ELSE IF EXISTS (SELECT OI.numoppitemtCode FROM WareHouseItems WI INNER JOIN OpportunityItems OI ON WI.numWareHouseItemID=OI.numWarehouseItmsID WHERE WI.numWareHouseID=@numWareHouseID)
		BEGIN
			RAISERROR('Dependant', 16, 1)
		END
		ELSE
		BEGIN
			DELETE FROM WareHouseItems WHERE ISNULL(numWareHouseID,0)=ISNULL(@numWareHouseID,0)
			DELETE FROM Warehouses WHERE numWareHouseID=@numWareHouseID	
		END
	END
END
GO
