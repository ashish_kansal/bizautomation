/****** Object:  StoredProcedure [dbo].[usp_updateProjOppStageEvent]    Script Date: 07/26/2008 16:21:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_updateprojoppstageevent')
DROP PROCEDURE usp_updateprojoppstageevent
GO
CREATE PROCEDURE [dbo].[usp_updateProjOppStageEvent]  
  
as  
-----------Checking Opportunity Events and Updating  
update OpportunityStageDetails set numStage  = case when bitchgStatus = 1 then numChgStatus else numStage end,  
     bitStageCompleted = case when bitClose = 1 then 1 else bitStageCompleted end ,  
  bintStageComDate =  case when bitClose = 1 then  
        case when bitStageCompleted = 1 then bintStageComDate else getutcdate() end  
        else bintStageComDate end  
where numOppStageId in(   
  
select numOppStageId from   
OpportunityStageDetails   
where numCommActId in (select numCommId from communication where dtEndTime between getdate() and dateadd(hour,1,getdate()) )   
and (bitchgStatus = 1 or bitClose = 1 ) and numStartDate <> 0 and numEvent in (2)  
union  
select numOppStageId from   
OpportunityStageDetails where numCommActId in (  
select ActivityId from activity where  
 DATEADD(second ,duration,StartDateTimeUtc)  between getutcdate() and dateadd(hour,1,getutcdate())  )  
 and (bitchgStatus = 1 or bitClose = 1 ) and numStartDate <> 0 and numEvent in (3))   
  
  
-----------Checking  Projects Events and Updating  
  
update ProjectsStageDetails set numStage = case when bitchgStatus = 1 then numChgStatus else numStage end,  
     bitStageCompleted = case when bitClose = 1 then 1 else bitStageCompleted end ,  
  bintStageComDate =  case when bitClose = 1 then  
        case when bitStageCompleted = 1 then bintStageComDate else getutcdate() end  
        else bintStageComDate end  
where numProStageId in(   
  
select numProStageId from   
ProjectsStageDetails   
where numCommActId in (select numCommId from communication where dtEndTime between getdate() and dateadd(hour,1,getdate()) )   
and (bitchgStatus = 1 or bitClose = 1 ) and numStartDate <> 0 and numEvent in (2)  
union  
select numProStageId from   
ProjectsStageDetails where numCommActId in (  
select ActivityId from activity where  
 DATEADD(second ,duration,StartDateTimeUtc)  between getutcdate() and dateadd(hour,1,getutcdate())  )  
 and (bitchgStatus = 1 or bitClose = 1 ) and numStartDate <> 0 and numEvent in (3))   


-----------Checking  SendEmailTemplate


Create table #OppClosed(numOppId numeric(9))    

Create table #ProClosed(numProId numeric(9))

Create table #changeStatus       
(numStageId numeric(9),vcFrom  varchar(100),	vcTo  varchar(100),	vcCC varchar(1),vcSubject  varchar(1000),
	vcBody  text,numDomainId  numeric(9),chrType  Char(1))
  
--Getting Opportunities Closed
--================================================--================================================
insert #OppClosed  
select distinct(numOppId) from OpportunityStageDetails where numstagepercentage in(0,100) and bitStageCompleted = 1  
--================================================--================================================
--Getting Projects Closed
--================================================--================================================
insert #ProClosed  
select distinct(numProId) from ProjectsStageDetails where numstagepercentage in(0,100) and bitStageCompleted = 1 
--================================================--================================================
--Getting Details and inserting into TempTable
--================================================--================================================
insert #changeStatus
--From opportunities--================================================
select numOppStageId,Adc2.vcEmail as vcFrom,ADC1.vcEmail as vcTo,'' as vcCC,vcSubject,
replace(replace(replace(replace(replace(replace(isnull(convert(varchar(max),vcDocDesc),''),'##OppID##',isnull(vcPOppName,'')),
'##Stage##',numOppStageId),'##Organization##',dbo.fn_GetComapnyName(ADC1.numDivisionId)),'##vcCompanyName##',
dbo.fn_GetComapnyName(ADC1.numDivisionId)),'##ContactName##',isnull(ADC1.vcFirstName+' '+ADC1.vcLastName,'')),'##Email##',
isnull(ADC1.vcEmail,'')) as vcBody ,
OppMas.numDomainId,'O'

from OpportunityStageDetails OppStage1     
join OpportunityMaster OppMas  on OppStage1.numOppId = OppMas.numOppId  
join AdditionalcontactsInformation ADC1 on ADC1.numContactId = OppMas.numContactId
join AdditionalcontactsInformation ADC2  on ADC2.numContactId = OppStage1.numAssignTo
join genericdocuments Gdoc on numGenericDocID=numET
  
 where numEvent = 1
 and numStartDate <> 0 and numStagePErcentage not in (0,100) and numET <> 0  and OppStage1.numAssignTo <> 0
 and OppStage1.numOppId not in (select numOppId from #OppClosed)  
and dbo. fn_checkEventDateRange(bintDueDate,OppStage1.bintCreatedDate,numstartDate
			,numstartTime,numStartTimePeriod,OppMas.numOppId,numOppStageId,'O') = 1

union all
--From Projects--================================================
select numProStageId,Adc2.vcEmail as vcFrom,ADC1.vcEmail as vcTo,'' as vcCC,vcSubject,
replace(replace(replace(replace(replace(replace(isnull(convert(varchar(max),vcDocDesc),''),'##OppID##',isnull(vcProjectName,'')),'##Stage##',numProStageId),
'##Organization##',dbo.fn_GetComapnyName(ADC1.numDivisionId)),'##vcCompanyName##',dbo.fn_GetComapnyName(ADC1.numDivisionId)),
'##ContactName##',isnull(ADC1.vcFirstName+' '+ADC1.vcLastName,'')),'##Email##',isnull(ADC1.vcEmail,'')) as vcBody ,
ProMas.numDomainId,'P'

from ProjectsStageDetails ProStage1     
join ProjectsMaster ProMas  on ProStage1.numProId = ProMas.numProId  
join AdditionalcontactsInformation ADC1 on ADC1.numContactId = ProMas.numCustPrjMgr
join AdditionalcontactsInformation ADC2  on ADC2.numContactId = ProStage1.numAssignTo
join genericdocuments Gdoc on numGenericDocID=numET
  
 where numEvent = 1
 and numStartDate <> 0 and numStagePErcentage not in (0,100) and numET <> 0  and ProStage1.numAssignTo <> 0
 and ProStage1.numProId not in (select numProId from #ProClosed)  
and dbo. fn_checkEventDateRange(bintDueDate,ProStage1.bintCreatedDate,numstartDate
			,numstartTime,numStartTimePeriod,ProMas.numProId,numProStageId,'P') = 1
--================================================--================================================
--Updating Opportunities
--================================================--================================================
update OpportunityStageDetails set numStage  = case when bitchgStatus = 1 then numChgStatus else numStage end,  
     bitStageCompleted = case when bitClose = 1 then 1 else bitStageCompleted end ,  
  bintStageComDate =  case when bitClose = 1 then  
        case when bitStageCompleted = 1 then bintStageComDate else getutcdate() end  
        else bintStageComDate end  
where numOppStageId in  (select numStageId from #changeStatus where chrType='O')  
--================================================--================================================
--Updating Projects
--================================================--================================================

update ProjectsStageDetails set numStage = case when bitchgStatus = 1 then numChgStatus else numStage end,  
     bitStageCompleted = case when bitClose = 1 then 1 else bitStageCompleted end ,  
  bintStageComDate =  case when bitClose = 1 then  
        case when bitStageCompleted = 1 then bintStageComDate else getutcdate() end  
        else bintStageComDate end  
where numProStageId in (select numStageId from #changeStatus where chrType='P')  
--================================================--================================================
--retuning Mail details
--================================================--================================================
select * from #changeStatus

--================================================--================================================
--Drop  Temp Tables
--================================================--================================================
drop table #changeStatus
drop table #OppClosed
drop table #ProClosed
GO
