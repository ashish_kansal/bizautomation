/****** Object:  StoredProcedure [dbo].[USP_GetShippingDTLs]    Script Date: 07/26/2008 16:18:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj

GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'usp_getshippingdtls' )
    DROP PROCEDURE usp_getshippingdtls
GO
CREATE PROCEDURE [dbo].[USP_GetShippingDTLs]
    @numListItemID AS NUMERIC(9) ,
    @numDomainID AS NUMERIC(9)
AS
    IF @numListItemID = 0
        SELECT  @numListItemID = ISNULL(numShipCompany, 0)
        FROM    Domain
        WHERE   numDomainID = @numDomainID

    SELECT  SF.intShipFieldID ,
            vcFieldName ,
            ISNULL(SFV.vcShipFieldValue, '') AS vcShipFieldValue ,
            ISNULL(SF.vcToolTip, '') AS vcToolTip ,
            ISNULL(SF.numListItemID, 0) AS numListItemID
    FROM    ShippingFields SF
            LEFT JOIN ShippingFieldValues SFV ON SFV.intShipFieldID = SF.intShipFieldID
                                                 AND SFV.numDomainID = @numDomainID
    WHERE   SF.numListItemID = @numListItemID

    SELECT  @numListItemID
GO
--exec USP_GetShippingDTLs @numListItemID=90,@numDomainID=1
--SELECT * FROM  ShippingFieldValues
--SELECT * FROM dbo.ListDetails WHERE numListItemID = 88
--SELECT * FROM ShippingFields
--SELECT * FROM Sites
