GO
/****** Object:  StoredProcedure [dbo].[USP_UpdatedomainMinimumPriceUpdate]    Script Date: 15-01-2018 04:30:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO                 
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype = 'p' AND NAME ='USP_UserListAuthorisedApprovers')
DROP PROCEDURE USP_UserListAuthorisedApprovers
GO
CREATE PROCEDURE  [dbo].[USP_UserListAuthorisedApprovers]                  
 @UserName as varchar(100)='',                  
 @tintGroupType as tinyint,                  
 @SortChar char(1)='0',                  
 @CurrentPage int,                  
 @PageSize int,                  
 @TotRecs int output,                  
 @columnName as Varchar(50),                  
 @columnSortOrder as Varchar(10),            
 @numDomainID as numeric(9),          
 @Status as char(1)                  
                  
as                  
                     
Create table #tempTable ( ID INT IDENTITY PRIMARY KEY,                   
      numUserID VARCHAR(15),                  
      vcEmailID varchar(100),                  
      [Name] VARCHAR(100),                  
      vcGroupName varchar(100),                  
 vcDomainName varchar(50),          
 Active varchar(10),
 numDefaultWarehouse numeric(18,0)              
 )                  
declare @strSql as varchar(8000)                  
set @strSql='SELECT numUserID, isnull(vcEmailID,''-'') as vcEmailID,                  
ISNULL(ADC.vcfirstname,'''') +'' ''+ ISNULL(ADC.vclastname,'''') as Name,                  
isnull(vcGroupName,''-'')as vcGroupName, vcDomainName, case when bitActivateFlag=1 then ''Active'' else ''Suspended'' end as bitActivateFlag, numDefaultWarehouse                          
   FROM UserMaster                    
   join Domain                      
   on UserMaster.numDomainID =  Domain.numDomainID                     
    join AdditionalContactsInformation ADC                    
   on ADC.numContactid=UserMaster.numUserDetailId                   
   left join AuthenticationGroupMaster GM                   
   on Gm.numGroupID= UserMaster.numGroupID              
    where  UserMaster.numDomainID='+convert(varchar(15),@numDomainID)+ ' and bitActivateFlag=' +@Status+' and (numContactId in (select numLevel1Authority from ApprovalConfig where numLevel1Authority <> 0 AND numDomainID='+convert(varchar(15),@numDomainID) + ')'
	+ ' OR numContactId in (select numLevel2Authority from ApprovalConfig where numLevel2Authority <> 0 AND numDomainID='+convert(varchar(15),@numDomainID) + ')'
	+ ' OR numContactId in (select numLevel3Authority from ApprovalConfig where numLevel3Authority <> 0 AND numDomainID='+convert(varchar(15),@numDomainID) + ')'
	+ ' OR numContactId in (select numLevel4Authority from ApprovalConfig where numLevel4Authority <> 0 AND numDomainID='+convert(varchar(15),@numDomainID) + ')'
	+ ' OR numContactId in (select numLevel5Authority from ApprovalConfig where numLevel5Authority <> 0 AND numDomainID='+convert(varchar(15),@numDomainID) + '))'               
                  
if @SortChar<>'0' set @strSql=@strSql + ' And (vcUserName like '''+@SortChar+'%'' OR vcEmailID LIKE '''+@SortChar+'%'')'

if @UserName<>''  set @strSql=@strSql + ' And (vcUserName like '''+@UserName+'%'' OR vcEmailID LIKE '''+@UserName+'%'')'

if @columnName<>'' set @strSql=@strSql + ' ORDER BY ' + @columnName +' '+ @columnSortOrder                  
   print @strSql               
insert into #tempTable(                  
 numUserID,                  
      vcEmailID,                  
      [Name],                  
      vcGroupName,                  
 vcDomainName,Active,numDefaultWarehouse)                  
exec (@strSql)                   
                  
  declare @firstRec as integer                   
  declare @lastRec as integer                  
 set @firstRec= (@CurrentPage-1) * @PageSize                  
     set @lastRec= (@CurrentPage*@PageSize+1)                  
select * from #tempTable where ID > @firstRec and ID < @lastRec                  
set @TotRecs=(select count(*) from #tempTable)                  
drop table #tempTable            
          
          
/*select  vcCompanyName,ISNULL(intNoofUsersSubscribed,0) + ISNULL(intNoofPartialSubscribed,0)/2 + ISNULL(intNoofMinimalSubscribed,0)/10 AS intNoofUsersSubscribed,dtSubStartDate,dtSubEndDate,(select count(*) from UserMaster  join Domain                      
   on UserMaster.numDomainID =  Domain.numDomainID                     
    join AdditionalContactsInformation ADC                    
   on ADC.numContactid=UserMaster.numUserDetailId  where Domain.numDomainID=@numDomainID and bitActivateFlag=1) as ActiveSub          
from Subscribers S           
Join Domain D          
on D.numDomainID=S.numTargetDomainID          
join DivisionMaster Div          
on Div.numDivisionID=D.numDivisionID          
join CompanyInfo C          
on C.numCompanyID=Div.numCompanyID          
where D.numDomainID=@numDomainID*/
