/****** Object:  StoredProcedure [dbo].[USP_cfwGetFields]    Script Date: 07/26/2008 16:15:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj                    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_cfwgetfields')
DROP PROCEDURE usp_cfwgetfields
GO
CREATE PROCEDURE [dbo].[USP_cfwGetFields]                            
@PageId as tinyint,                              
@numRelation as numeric(9),                              
@numRecordId as numeric(9),                  
@numDomainID as numeric(9),
@numLocationType As numeric(9)
AS            
BEGIN
                  
	if @PageId= 1 
	begin                              
		select CFW_Fld_Master.fld_id,fld_type,fld_label,numlistid,numOrder,Rec.FldDTLID,CASE WHEN CFW_Fld_Master.Fld_type='SelectBox' THEN isnull(Rec.Fld_Value,0) ELSE isnull(Rec.Fld_Value,'') END as Value,subgrp as TabId,Grp_Name as tabname,vcURL,CFW_Fld_Master.vcToolTip from CFW_Fld_Master join CFW_Fld_Dtl                              
		on Fld_id=numFieldId                              
		left join CFw_Grp_Master                               
		on subgrp=CFw_Grp_Master.Grp_id 
		left join dbo.GetCustomFieldDTLIDAndValues(@PageId,@numRecordId) Rec
		on Rec.Fld_ID=CFW_Fld_Master.fld_id                             
		where CFW_Fld_Master.grp_id=@PageId and numRelation=@numRelation and CFW_Fld_Master.numDomainID=@numDomainID                          
         
		union        
		select 0 as fld_id,'Frame' as fld_type,'' as fld_label,0 as numlistid,0,0,'' as Value,Grp_id as TabId,Grp_Name as tabname,vcURLF,Null as vcToolTip from CFw_Grp_Master        
		where tintType=1 and loc_Id = @PageId   and numDomainID=@numDomainID           
		order by subgrp,numOrder                             
	end                              

	---select Shared Custom fields and tab specific customfields -- where Pageid=1 stands for shared custom fields among accounts,leads,prospects
	IF  @PageId= 12 or  @PageId= 13 or  @PageId= 14
	BEGIN
		select CFW_Fld_Master.fld_id,fld_type,fld_label,numlistid,numOrder,Rec.FldDTLID,CASE WHEN CFW_Fld_Master.Fld_type='SelectBox' THEN isnull(Rec.Fld_Value,0) ELSE isnull(Rec.Fld_Value,'') END as Value,subgrp as TabId,Grp_Name as tabname,vcURL,CFW_Fld_Master.vcToolTip from CFW_Fld_Master join CFW_Fld_Dtl                              
		on Fld_id=numFieldId                              
		left join CFw_Grp_Master                               
		on subgrp=CFw_Grp_Master.Grp_id 
		left join dbo.GetCustomFieldDTLIDAndValues(@PageId,@numRecordId) Rec
		on Rec.Fld_ID=CFW_Fld_Master.fld_id                             
		where CFW_Fld_Master.grp_id=@PageId and numRelation=@numRelation and CFW_Fld_Master.numDomainID=@numDomainID                          
	
		UNION 
	
		select CFW_Fld_Master.fld_id,fld_type,fld_label,numlistid,numOrder,Rec.FldDTLID,CASE WHEN CFW_Fld_Master.Fld_type='SelectBox' THEN isnull(Rec.Fld_Value,0) ELSE isnull(Rec.Fld_Value,'') END as Value,subgrp as TabId,Grp_Name as tabname,vcURL,CFW_Fld_Master.vcToolTip from CFW_Fld_Master join CFW_Fld_Dtl                              
		on Fld_id=numFieldId                              
		left join CFw_Grp_Master                               
		on subgrp=CFw_Grp_Master.Grp_id 
		left join dbo.GetCustomFieldDTLIDAndValues(1,@numRecordId) Rec
		on Rec.Fld_ID=CFW_Fld_Master.fld_id                             
		where CFW_Fld_Master.grp_id=1 and numRelation=@numRelation and CFW_Fld_Master.numDomainID=@numDomainID
	         
		union        
		select 0 as fld_id,'Frame' as fld_type,'' as fld_label,0 as numlistid,0,0,'' as Value,Grp_id as TabId,Grp_Name as tabname,vcURLF,NUll as vcToolTip from CFw_Grp_Master        
		where tintType=1 and loc_Id = @PageId   and numDomainID=@numDomainID           
		order by subgrp,numOrder                             
	
	END


                              
	if @PageId= 4                              
	begin       
		---------------------Very Important----------------------    
		-------Who wrote this part    
		---------------------------------------------------------    
    
    
		--declare @ContactId as numeric(9)                              
		---select @ContactId=numContactId from cases where numCaseid=@numRecordId                              
		--select @numRelation=numCompanyType from AdditionalContactsInformation AddC                              
		---join DivisionMaster Div on AddC.numDivisionId=Div.numDivisionId                              
		---join CompanyInfo Com on  Com.numCompanyId=Div.numCompanyId                              
		---where AddC.numContactID=@ContactId                              
                              
		select CFW_Fld_Master.fld_id,fld_type,fld_label,numlistid,numOrder,
		Rec.FldDTLID,CASE WHEN CFW_Fld_Master.Fld_type='SelectBox' THEN isnull(Rec.Fld_Value,0) ELSE isnull(Rec.Fld_Value,'') END as Value,
		subgrp as TabId,Grp_Name as tabname,vcURL,CFW_Fld_Master.vcToolTip from CFW_Fld_Master join CFW_Fld_Dtl                              
		on Fld_id=numFieldId                              
		left join CFw_Grp_Master                               
		on subgrp=CFw_Grp_Master.Grp_id  
		left join dbo.GetCustomFieldDTLIDAndValues(@PageId,@numRecordId) Rec
		on Rec.Fld_ID=CFW_Fld_Master.fld_id                             
		where CFW_Fld_Master.grp_id=@PageId and numRelation=@numRelation and CFW_Fld_Master.numDomainID=@numDomainID                             
		--select @numRelation=numContactType from AdditionalContactsInformation AddC                              
		--join DivisionMaster Div on AddC.numDivisionId=Div.numDivisionId                              
		--join CompanyInfo Com on  Com.numCompanyId=Div.numCompanyId                              
		--where AddC.numContactID=@numRecordId                              
                              
		union        
		select 0 as fld_id,'Frame' as fld_type,'' as fld_label,0 as numlistid,0,0,'' as Value,
		Grp_id as TabId,Grp_Name as tabname,vcURLF,NUll as vcToolTip from CFw_Grp_Master        
		where tintType=1 and loc_Id = @PageId   and numDomainID=@numDomainID                              
		   order by subgrp,numOrder                           
	end                              
                              
	if @PageId= 2 or  @PageId= 3 or @PageId= 5 or @PageId= 6 or @PageId= 7 or @PageId= 8     or @PageId= 11   or @PageId = 17                
	begin     
		IF(@PageId=5)
		BEGIN
			select CFW_Fld_Master.fld_id,fld_type,fld_label,numlistid,ISNULL(Rec.FldDTLID,0) FldDTLID,
			(CASE 
				WHEN ISNULL(Rec.Fld_Value, '') = '' AND CFW_Fld_Master.fld_type = 'Checkbox' THEN '0' 
				WHEN ISNULL(Rec.Fld_Value, '') = '' AND CFW_Fld_Master.fld_type = 'SelectBox' THEN '0'
				WHEN CFW_Fld_Master.fld_type = 'SelectBox' THEN ISNULL(Rec.Fld_Value,0) 
				ELSE ISNULL(Rec.Fld_Value,'') 
				END)   AS Value,

			subgrp as TabId,Grp_Name as tabname,vcURL,CFW_Fld_Master.vcToolTip,CFW_Fld_Master.vcItemsLocation  from CFW_Fld_Master                               
			left join CFw_Grp_Master                               
			on subgrp=CFw_Grp_Master.Grp_id  
			left join dbo.GetCustomFieldDTLIDAndValues(@PageId,@numRecordId) Rec
			on Rec.Fld_ID=CFW_Fld_Master.fld_id                             
			where CFW_Fld_Master.grp_id=@PageId and CFW_Fld_Master.numDomainID=@numDomainID   AND 1= (CASE WHEN CFW_Fld_Master.vcItemsLocation IS NULL THEN 1 WHEN @numLocationType IN (SELECT CAST(Items As NUMERIC) FROM Split(CFW_Fld_Master.vcItemsLocation,',') WHERE Items<>'') THEN 1 ELSE 0 END)      
			union        
			select 0 as fld_id,'Frame' as fld_type,'' as fld_label,0 as numlistid,0 as FldDTLID,'0' as Value,Grp_id as TabId,Grp_Name as tabname,vcURLF,NULL as vcToolTip,NULL AS vcItemsLocation from CFw_Grp_Master        
			where tintType=1 and loc_Id = @PageId  and numDomainID=@numDomainID  
		END
		ELSE
		BEGIN
		select CFW_Fld_Master.fld_id,fld_type,fld_label,numlistid,ISNULL(Rec.FldDTLID,0) FldDTLID,
		(CASE 
			WHEN ISNULL(Rec.Fld_Value, '') = '' AND CFW_Fld_Master.fld_type = 'Checkbox' THEN '0' 
			WHEN ISNULL(Rec.Fld_Value, '') = '' AND CFW_Fld_Master.fld_type = 'SelectBox' THEN '0'
			WHEN CFW_Fld_Master.fld_type = 'SelectBox' THEN ISNULL(Rec.Fld_Value,0) 
			ELSE ISNULL(Rec.Fld_Value,'') 
			END)   AS Value,

		subgrp as TabId,Grp_Name as tabname,vcURL,CFW_Fld_Master.vcToolTip,CFW_Fld_Master.vcItemsLocation  from CFW_Fld_Master                               
		left join CFw_Grp_Master                               
		on subgrp=CFw_Grp_Master.Grp_id  
		left join dbo.GetCustomFieldDTLIDAndValues(@PageId,@numRecordId) Rec
		on Rec.Fld_ID=CFW_Fld_Master.fld_id                             
		where CFW_Fld_Master.grp_id=@PageId and CFW_Fld_Master.numDomainID=@numDomainID         
		union        
		select 0 as fld_id,'Frame' as fld_type,'' as fld_label,0 as numlistid,0 as FldDTLID,'0' as Value,Grp_id as TabId,Grp_Name as tabname,vcURLF,NULL as vcToolTip,NULL AS vcItemsLocation from CFw_Grp_Master        
		where tintType=1 and loc_Id = @PageId  and numDomainID=@numDomainID 
		END
	end

	if @PageId= 18               
	begin                              
		select CFW_Fld_Master.fld_id,fld_type,fld_label,numlistid,ISNULL(Rec.FldDTLID,0) FldDTLID,
		(CASE 
			WHEN ISNULL(Rec.Fld_Value, '') = '' AND fld_type = 'Checkbox' THEN '0' 
			WHEN ISNULL(Rec.Fld_Value, '') = '' AND fld_type = 'SelectBox' THEN '0'
			WHEN fld_type = 'SelectBox' THEN ISNULL(Rec.Fld_Value,0) 
			ELSE ISNULL(Rec.Fld_Value,'') END)  AS Value,
		subgrp as TabId,Grp_Name as tabname,vcURL,CFW_Fld_Master.vcToolTip  from CFW_Fld_Master                               
		left join CFw_Grp_Master                               
		on subgrp=CFw_Grp_Master.Grp_id  
		left join dbo.GetCustomFieldDTLIDAndValues(@PageId,@numRecordId) Rec
		on Rec.Fld_ID=CFW_Fld_Master.fld_id                             
		where CFW_Fld_Master.grp_id=5 and CFW_Fld_Master.numDomainID=@numDomainID         
		union        
		select 0 as fld_id,'Frame' as fld_type,'' as fld_label,0 as numlistid,0 as FldDTLID,'0' as Value,Grp_id as TabId,Grp_Name as tabname,vcURLF,NULL as vcToolTip from CFw_Grp_Master        
		where tintType=1 and loc_Id = 5  and numDomainID=@numDomainID      
	end

select GRp_Id as TabID,Grp_Name AS TabName from CFw_Grp_Master where 
(Loc_ID=@PageId OR 1=(CASE WHEN @PageId IN (12,13,14) THEN CASE WHEN Loc_Id=1 THEN 1 ELSE 0 END ELSE 0 END))
 and numDomainID=@numDomainID
/*tintType stands for static tabs */ AND tintType<>2

END
