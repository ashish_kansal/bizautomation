/****** Object:  StoredProcedure [dbo].[USP_RepItems]    Script Date: 07/26/2008 16:20:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_repitems')
DROP PROCEDURE usp_repitems
GO
CREATE PROCEDURE [dbo].[USP_RepItems]        
  @numDomainID numeric,              
  @dtFromDate datetime,              
  @dtToDate datetime,              
  @numUserCntID numeric=0,                   
  @tintType numeric,
  @numSort numeric,
  @numDivisionId numeric
          
        
as        

create table #TempRepItems
(numItemCode numeric(9),
vcItemName varchar(300),
vcModelID varchar(250),
txtItemDesc varchar(1000),
Type varchar(100),
Amount numeric(9,2),       
TotalItem FLOAT,
OnHand FLOAT,
 OnOrder FLOAT,
 BackOrder FLOAT,
 Allocation FLOAT)
 
 DECLARE @vcSql varchar(2500);
 
 INSERT INTO #TempRepItems
 
SELECT  numItemCode,
        vcItemName,
        vcModelID,
        txtItemDesc,
        CASE WHEN charItemType = 'P' THEN 'Product'
             WHEN charItemType = 'S' THEN 'Service'
        END AS TYPE,
        0,
        0,

        ( SELECT    ISNULL(SUM(numOnHand), 0)
          FROM      WareHouseItems
          WHERE     numItemID = numItemCode
        ) AS OnHand,
        ( SELECT    ISNULL(SUM(numOnOrder), 0)
          FROM      WareHouseItems
          WHERE     numItemID = numItemCode
        ) AS OnOrder,
        ( SELECT    ISNULL(SUM(numBackOrder), 0)
          FROM      WareHouseItems
          WHERE     numItemID = numItemCode
        ) AS BackOrder,
        ( SELECT    ISNULL(SUM(numAllocation), 0)
          FROM      WareHouseItems
          WHERE     numItemID = numItemCode
        ) AS Allocation
FROM    item
WHERE   numDomainID = @numDomainID

UPDATE #TempRepItems
SET Amount=ISNULL(X.Amount,0),TotalItem=ISNULL(X.TotalItem,0)
FROM (
	SELECT OppItem.numItemCode,SUM(OppItem.numUnitHour) TotalItem,SUM(monTotAmount) Amount
					 FROM   OpportunityMaster Opp
							JOIN OpportunityItems OppItem ON OppItem.numOppId = Opp.numOppId
					 WHERE  OPP.numDomainId = @numDomainID AND Opp.tintOppType = 1
							AND Opp.tintOppStatus = 1
							AND Opp.numDivisionId = ( CASE @numDivisionId
														WHEN 0
														THEN Opp.numDivisionId
														ELSE @numDivisionId
													  END )
							AND Opp.bintCreatedDate >= @dtFromDate
							AND Opp.bintCreatedDate <= @dtToDate
	GROUP BY numItemCode
) X
WHERE #TempRepItems.numItemCode = X.numItemCode

set @vcSql='SELECT * FROM #TempRepItems WHERE TotalItem > 0 ORDER BY ' + (Case @numSort WHEN 0 THEN 'Amount' ELSE 'TotalItem'  END) + ' desc'

exec (@vcSql)
--
GO
