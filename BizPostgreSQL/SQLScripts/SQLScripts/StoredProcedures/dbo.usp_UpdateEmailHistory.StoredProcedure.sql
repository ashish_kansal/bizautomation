/****** Object:  StoredProcedure [dbo].[usp_UpdateEmailHistory]    Script Date: 07/26/2008 16:21:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_updateemailhistory')
DROP PROCEDURE usp_updateemailhistory
GO
CREATE PROCEDURE [dbo].[usp_UpdateEmailHistory]   
--
AS
DECLARE @bIntEmailSendDate BIGINT
INSERT INTO EmailHistory ( bintEmailDate ,vcFrom ,vcTo ,vcSubject ,txtbody , numDomainId )
 SELECT bintSendEmailDate , vcEmailFrom ,  ',' + vcEmailTo + ' , ' +  vcEmailCC , vcEmailSubject ,vcEmailBody , dbo.fn_getDomainID(vcEmailFrom + ' , ' +  vcEmailTo + ' , ' +  vcEmailCC )
  FROM TempEmails 
  WHERE dbo.fn_getValidEmailIds(vcEmailFrom + ' , ' +  vcEmailTo + ' , ' +  vcEmailCC  ) = 1 
  AND bolUpload = 0
 
UPDATE TempEmails SET bolUpload = 1
GO
