/****** Object:  StoredProcedure [dbo].[usp_GetCustomReportModules]    Script Date: 07/26/2008 16:17:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getcustomreportmodules')
DROP PROCEDURE usp_getcustomreportmodules
GO
CREATE PROCEDURE [dbo].[usp_GetCustomReportModules]  
@numcustModuleId as numeric(9)  
as   
select * from CustReptModuleMaster where isnull(bitCustModuleDisable,0)<>1 order by numCustModuleId  
  
select * from CustRptAndFieldGroups where isnull(bitGroupDisable,0)<>1 and numcustModuleId = @numcustModuleId order by numReportOptionGroupId
GO
