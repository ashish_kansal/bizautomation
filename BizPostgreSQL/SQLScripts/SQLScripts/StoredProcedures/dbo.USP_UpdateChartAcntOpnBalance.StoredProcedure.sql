/****** Object:  StoredProcedure [dbo].[USP_UpdateChartAcntOpnBalance]    Script Date: 07/26/2008 16:21:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created By Siva                          
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_updatechartacntopnbalance')
DROP PROCEDURE usp_updatechartacntopnbalance
GO
CREATE PROCEDURE [dbo].[USP_UpdateChartAcntOpnBalance]                                
@JournalId as numeric(9)=0,  
@numDomainId as numeric(9)=0                                
As                                
Begin                                
Declare @numTransactionId as numeric(9)                                
Declare @numChartAcntId as  varchar(8000)                                
Declare @numChartAcntId1 as varchar(8000)                                
Declare @numDebitAmt as  DECIMAL(20,5)                              
Declare @numCreditAmt as DECIMAL(20,5)                              
Declare @strSQl as varchar(8000)                               
Declare @strSQl1 as varchar(8000)                              
Declare @strSQl2 as varchar(8000)                  
Declare @strSQlBalance as varchar(8000)                 
Declare @numChartAcntTypeId as integer                                
Set @strSQl1=''                               
Set @strSQl=''                                
Set @strSQl2=''                 
Set @strSQlBalance=''                          
                                
Select @numTransactionId=min(numTransactionId) From General_Journal_Details Where numJournalId=@JournalId And numDomainId=@numDomainId                               
                                
While @numTransactionId <> 0                                
Begin                                
                                   
   Select @numChartAcntId=numChartAcntId,@numDebitAmt=numDebitAmt,@numCreditAmt=numCreditAmt From General_Journal_Details Where numTransactionId=@numTransactionId And numDomainId=@numDomainId                                
   --Select * From General_Journal_Details Where numTransactionId=@numTransactionId                                 
    --Set @numChartAcntId1= @numChartAcntId                                
   print @numChartAcntId                            
 Select @numChartAcntTypeId =numAcntTypeID  From Chart_Of_Accounts Where numAccountId=@numChartAcntId And numDomainId=@numDomainId                 
    print '@numChartAcntTypeId'+ Convert(varchar(10),@numChartAcntTypeId)                
   Set @strSQl=dbo.fn_ParentCategory(@numChartAcntId,@numDomainId)                       
    print '@strSQl'+@strSQl                           
    if @numDebitAmt <> 0                                 
    Begin                              
      if @strSQl <>''                     
      Begin                  
       if @numChartAcntTypeId = 813 Or @numChartAcntTypeId = 814 Or @numChartAcntTypeId = 817 Or @numChartAcntTypeId = 818 Or @numChartAcntTypeId = 819 Or @numChartAcntTypeId = 823 Or @numChartAcntTypeId = 824 Or @numChartAcntTypeId = 826                 
  
    
      
        
         
      Begin              
       set @strSQl2='update chart_of_accounts  set numopeningbal=isnull(numopeningbal,0) + ' + convert(varchar(30),@numDebitAmt) + ' where  numAccountId in ('+@numChartAcntId+','+@strSQl+') And numDomainId='+Convert(varchar(10),@numDomainId)              
            
      --  if @numChartAcntTypeId = 813              
    --    set @strSQlBalance='update chart_of_accounts  set numOriginalOpeningBal=isnull(numOriginalOpeningBal,0) + ' + convert(varchar(30),@numDebitAmt) + ', numopeningbal=isnull(numopeningbal,0) + '            
       --  + convert(varchar(30),@numDebitAmt) +' where  numAccountId=15'                                                                       
       End                    
        else                 
        Begin                
         set @strSQl2='update chart_of_accounts  set numopeningbal=isnull(numopeningbal,0) - ' + convert(varchar(30),@numDebitAmt) + ' where  numAccountId in ('+@numChartAcntId+','+@strSQl+') And numDomainId='+Convert(varchar(10),@numDomainId)            
           
           -- if @numChartAcntTypeId = 816              
          --  set @strSQlBalance='update chart_of_accounts  set numOriginalOpeningBal=isnull(numOriginalOpeningBal,0) - ' + convert(varchar(30),@numDebitAmt) +             
         --   ', numopeningbal=isnull(numopeningbal,0) - ' + convert(varchar(30),@numDebitAmt) +' where  numAccountId=15'                                                                       
        End              
       End                                
      else                            
        Begin               
        if @numChartAcntTypeId = 813 Or @numChartAcntTypeId = 814 Or @numChartAcntTypeId = 817 Or @numChartAcntTypeId = 818 Or @numChartAcntTypeId = 819 Or @numChartAcntTypeId = 823 Or @numChartAcntTypeId = 824 Or @numChartAcntTypeId = 826                
 
     
      
        
          
                    
          set @strSQl2='update chart_of_accounts  set numopeningbal=isnull(numopeningbal,0) + ' + convert(varchar(30),@numDebitAmt) + ' where  numAccountId in ('+@numChartAcntId+') And numDomainId='+Convert(varchar(10),@numDomainId)                       
                          
                       
        else                  
         Begin              
          set @strSQl2='update chart_of_accounts  set numopeningbal=isnull(numopeningbal,0) - ' + convert(varchar(30),@numDebitAmt) + ' where  numAccountId in ('+@numChartAcntId+') And numDomainId='+Convert(varchar(10),@numDomainId)                       
                          
                                                                        
         End              
       End                  
     Exec(@strSQl2)                            
      print(@strSQl2)                               
    End                                
    if @numCreditAmt <> 0                                 
    Begin                                
                                
    if @strSQl<>''                         
     Begin                  
      if @numChartAcntTypeId = 813 Or @numChartAcntTypeId = 814 Or @numChartAcntTypeId = 817 Or @numChartAcntTypeId = 818 Or @numChartAcntTypeId = 819 Or @numChartAcntTypeId = 823 Or @numChartAcntTypeId = 824 Or @numChartAcntTypeId = 826                  
  
    
      
        
       Begin              
         set @strSQl1='update chart_of_accounts  set numopeningbal=isnull(numopeningbal,0) - ' + convert(varchar(30),@numCreditAmt) + ' where  numAccountId in ('+@numChartAcntId+','+@strSQl+') And numDomainId='+Convert(varchar(10),@numDomainId)           
                                   
                                                                            
       End              
      else                  
      Begin              
        set @strSQl1='update chart_of_accounts  set numopeningbal=isnull(numopeningbal,0) + ' + convert(varchar(30),@numCreditAmt) + ' where  numAccountId in ('+@numChartAcntId+','+@strSQl+') And numDomainId='+Convert(varchar(10),@numDomainId)            
                                     
                        
      End              
     End                  
    else                      
     Begin                  
     if @numChartAcntTypeId = 813 Or @numChartAcntTypeId = 814 Or @numChartAcntTypeId = 817 Or @numChartAcntTypeId = 818 Or @numChartAcntTypeId = 819 Or @numChartAcntTypeId = 823 Or @numChartAcntTypeId = 824 Or @numChartAcntTypeId = 826                   
  
    
      
       
     Begin              
       set @strSQl1='update chart_of_accounts  set numopeningbal=isnull(numopeningbal,0) - ' + convert(varchar(30),@numCreditAmt) + ' where  numAccountId in ('+@numChartAcntId+') And numDomainId='+Convert(varchar(10),@numDomainId)                         
                      
                                                                           
     End              
     else                  
     Begin              
      set @strSQl1='update chart_of_accounts  set numopeningbal=isnull(numopeningbal,0) + ' + convert(varchar(30),@numCreditAmt) + ' where  numAccountId in ('+@numChartAcntId+') And numDomainId='+Convert(varchar(10),@numDomainId)                          
                      
                   
    End               
     End                    
    Exec(@strSQl1)                                
      print(@strSQl1)                
    Exec(@strSQlBalance)              
    print(@strSQlBalance)                             
    End                                
                                 
    print (@numChartAcntId1)                                
    Print ('Debit'+ convert(varchar(200),@numDebitAmt))                              
    print ('Credit'+ convert(varchar(200),@numCreditAmt))                                
   Select @numTransactionId=min(numTransactionId) From General_Journal_Details Where numJournalId=@JournalId And  numTransactionId>@numTransactionId  And numDomainId= @numDomainId                               
End                                
End
GO
