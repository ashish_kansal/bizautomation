/****** Object:  StoredProcedure [dbo].[USP_GetDivision]    Script Date: 07/26/2008 16:17:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getdivision')
DROP PROCEDURE usp_getdivision
GO
CREATE PROCEDURE [dbo].[USP_GetDivision]    
@numDomainID as numeric(9)=0    
as      
select numListItemID,vcData from ListDetails    
where numListID=71 and numDomainID=@numDomainID
GO
