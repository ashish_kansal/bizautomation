/****** Object:  StoredProcedure [dbo].[usp_DisplayUniversalMaster]    Script Date: 07/26/2008 16:15:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_displayuniversalmaster')
DROP PROCEDURE usp_displayuniversalmaster
GO
CREATE PROCEDURE [dbo].[usp_DisplayUniversalMaster]

	--Input Parameter List
	@numDivID numeric(9)=0
	 
	 	--
AS

BEGIN
	--selecting the value of Universal key to display on prospects-display.aspx page  
	SELECT numUniversalSupportKey from UniversalSupportKeyMaster where numDivisionId=@numDivId
	
	
	END
GO
