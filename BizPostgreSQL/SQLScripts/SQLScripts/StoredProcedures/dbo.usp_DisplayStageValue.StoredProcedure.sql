/****** Object:  StoredProcedure [dbo].[usp_DisplayStageValue]    Script Date: 07/26/2008 16:15:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
---------------------------------
--usp_DisplayStageValue

--This is for getting the value of the stage percentage from the stage percentage master
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_displaystagevalue')
DROP PROCEDURE usp_displaystagevalue
GO
CREATE PROCEDURE [dbo].[usp_DisplayStageValue]
	@numStagePercentageId numeric   
--
AS
	SELECT numstagepercentage FROM stagepercentagemaster WHERE numstagepercentageid=@numStagePercentageId
GO
