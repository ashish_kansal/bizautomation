/****** Object:  StoredProcedure [dbo].[USP_ManagePriceBookRules]    Script Date: 07/26/2008 16:19:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj  
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_managepricebookrules')
DROP PROCEDURE usp_managepricebookrules
GO
CREATE PROCEDURE [dbo].[USP_ManagePriceBookRules]
    @numPricRuleID AS NUMERIC(9) = 0,
    @vcRuleName AS VARCHAR(100),
    @vcRuleDescription AS VARCHAR(100),
    @tintRuleType AS TINYINT,
    @decDiscount AS DECIMAL(18, 2),
    @intQntyItems AS INTEGER,
    @decMaxDedPerAmt AS DECIMAL(18, 2),
    @numDomainID AS NUMERIC(9) = 0,
    @tintPricingMethod AS TINYINT,
    @tintDiscountType AS TINYINT,
    @tintStep2 AS TINYINT,
    @tintStep3 AS TINYINT,
    @tintRuleFor AS TINYINT 
AS 
	IF (@tintStep2=3 and @tintStep3=3)
	BEGIN
		IF (SELECT COUNT(*) FROM [PriceBookRules] WHERE [tintStep2]=@tintStep2 AND [tintStep3]=@tintStep3 AND [numPricRuleID]<>@numPricRuleID AND [numDomainID]=@numDomainID AND tintRuleFor=@tintRuleFor)>0
		BEGIN
			RAISERROR ( 'DUPLICATE',16, 1 )
			RETURN ;
		END	
	END
	ELSE IF @numPricRuleID > 0
	BEGIN
		DECLARE @isItemDuplicate BIT = 0
		DECLARE @isOrganizationDupicate BIT = 0

		IF EXISTS
		(
			SELECT
				PriceBookRules.numPricRuleID
			FROM
				PriceBookRuleItems
			INNER JOIN
				PriceBookRules
			ON
				PriceBookRuleItems.numRuleID = PriceBookRules.numPricRuleID
			INNER JOIN
				PriceBookRuleItems t1
			ON
				t1.numRuleID = @numPricRuleID
				AND PriceBookRuleItems.tintType = t1.tintType
				AND t1.numValue = PriceBookRuleItems.numValue
			WHERE
				PriceBookRules.numDomainID = @numDomainID
				AND PriceBookRules.numPricRuleID <> @numPricRuleID
				AND PriceBookRules.tintStep2 = @tintStep2
				AND PriceBookRules.tintStep3 = @tintStep3
				AND PriceBookRules.tintRuleFor=@tintRuleFor
		) OR (@tintStep2 = 3 AND (SELECT COUNT(*) FROM [PriceBookRules] WHERE [tintStep2]=@tintStep2 AND [numPricRuleID]<>@numPricRuleID AND [numDomainID]=@numDomainID AND tintRuleFor=@tintRuleFor)>0)
		BEGIN
			SET @isItemDuplicate = 1
		END

		IF EXISTS
		(
			SELECT
				PriceBookRules.numPricRuleID
			FROM
				PriceBookRuleDTL
			INNER JOIN
				PriceBookRules
			ON
				PriceBookRuleDTL.numRuleID = PriceBookRules.numPricRuleID
			INNER JOIN
				PriceBookRuleDTL t1
			ON
				t1.numRuleID = @numPricRuleID
				AND PriceBookRuleDTL.tintType = t1.tintType
				AND t1.numValue = PriceBookRuleDTL.numValue
				AND ISNULL(t1.numProfile,0) = ISNULL(PriceBookRuleDTL.numProfile,0)
			WHERE
				numDomainID = @numDomainID
				AND numPricRuleID <> @numPricRuleID
				AND tintStep2 = @tintStep2
				AND tintStep3 = @tintStep3
				AND tintRuleFor=@tintRuleFor
		) OR (@tintStep3 = 3 AND (SELECT COUNT(*) FROM [PriceBookRules] WHERE [tintStep3]=@tintStep3 AND [numPricRuleID]<>@numPricRuleID AND [numDomainID]=@numDomainID AND tintRuleFor=@tintRuleFor) > 0)
		BEGIN
			SET @isOrganizationDupicate = 1
		END

		IF @isOrganizationDupicate = 1 AND @isItemDuplicate = 1
		BEGIN
			RAISERROR ( 'DUPLICATE',16, 1 )
			RETURN ;
		END
	END


    DECLARE @strDiscount VARCHAR(10)
    IF @tintDiscountType = 1 
        SET @strDiscount = CONVERT(DECIMAL(9, 2), @decDiscount)
    ELSE 
        SET @strDiscount = CONVERT(int, @decDiscount)
 
    IF @numPricRuleID = 0 
        BEGIN  
            INSERT  INTO PriceBookRules
                    (
                      vcRuleName,
                      vcRuleDescription,
                      tintRuleType,
                      decDiscount,
                      intQntyItems,
                      decMaxDedPerAmt,
                      numDomainID,
                      tintPricingMethod,
                      tintDiscountType,
                      tintStep2,
                      tintStep3,tintRuleFor
                    )
            VALUES  (
                      @vcRuleName,
                      @vcRuleDescription,
                      @tintRuleType,
                      @strDiscount,
                      @intQntyItems,
                      @decMaxDedPerAmt,
                      @numDomainID,
                      @tintPricingMethod,
                      @tintDiscountType,
                      @tintStep2,
                      @tintStep3,@tintRuleFor
                    )
            SELECT  numPricRuleID = SCOPE_IDENTITY()  
  
        END  
    ELSE 
        BEGIN  
            UPDATE  PriceBookRules
            SET     vcRuleName = @vcRuleName,
                    vcRuleDescription = @vcRuleDescription,
                    tintRuleType = @tintRuleType,
                    decDiscount = @strDiscount,
                    intQntyItems = @intQntyItems,
                    decMaxDedPerAmt = @decMaxDedPerAmt,
                    tintPricingMethod = @tintPricingMethod,
                    tintDiscountType = @tintDiscountType,
                    tintStep2 = @tintStep2,
                    tintStep3 = @tintStep3,tintRuleFor=@tintRuleFor
            WHERE   numPricRuleID = @numPricRuleID  
            SELECT  @numPricRuleID
        END
GO
