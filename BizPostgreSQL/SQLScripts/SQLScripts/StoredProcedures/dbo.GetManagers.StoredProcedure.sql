/****** Object:  StoredProcedure [dbo].[GetManagers]    Script Date: 07/26/2008 16:14:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj  
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='getmanagers')
DROP PROCEDURE getmanagers
GO
CREATE PROCEDURE [dbo].[GetManagers]   
@numDivisionID as numeric(9)=0,  
@numContactID as numeric(9)=0,  
@numDomainID as numeric(9)=0  
as  
  
  
if @numContactID=0  
begin  
 select numContactID,vcFirstName+' '+ vcLastName as [Name] from AdditionalContactsInformation   
 where numDivisionID = @numDivisionID  and numDomainID=@numDomainID  
end  
else if @numContactID<>0  
begin  
 select numContactID,vcFirstName+' '+ vcLastName as [Name] from AdditionalContactsInformation   
 where numDivisionID = @numDivisionID and numContactId <>@numContactID and numDomainID=@numDomainID  
end
