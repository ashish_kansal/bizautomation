/****** Object:  StoredProcedure [dbo].[USP_SaveProfileEGroup]    Script Date: 07/26/2008 16:21:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'usp_saveprofileegroup' ) 
    DROP PROCEDURE usp_saveprofileegroup
GO
CREATE PROCEDURE [dbo].[USP_SaveProfileEGroup]
    @strcontact AS TEXT = NULL,
    @strGroupName AS VARCHAR(10),
    @numDomainID AS NUMERIC,
    @numUserCntID AS NUMERIC,
    @numGroupID AS NUMERIC
AS 
IF @strGroupName <> '' 
    BEGIN
        IF @strGroupName NOT IN ( SELECT DISTINCT
                                            vcEmailGroupName
                                  FROM      ProfileEmailGroup ) 
            BEGIN
                INSERT  INTO ProfileEmailGroup ( vcEmailGroupName,
                                                 numDomainID,
                                                 numCreatedBy,
                                                 dtCreatedDate,
                                                 numModifiedBy,
                                                 dtModifiedDate )
                VALUES  (
                          @strGroupName,
                          @numDomainID,
                          @numUserCntID,
                          GETUTCDATE(),
                          @numUserCntID,
                          GETUTCDATE()
                        )


                IF CONVERT(VARCHAR(10), @strcontact) <> '' 
                    BEGIN                            
                        DECLARE @hDoc4 INT                                    
                        EXEC sp_xml_preparedocument @hDoc4 OUTPUT, @strcontact                                    
                        
                        INSERT  INTO ProfileEGroupDTL ( numContactID,
                                                        numEmailGroupID )
                                SELECT  X.numContactId,
                                        ( SELECT TOP 1
                                                    numEmailGroupID
                                          FROM      ProfileEmailGroup
                                          WHERE     vcEmailGroupName = @strGroupName
                                        )
                                FROM    ( SELECT    *
                                          FROM      OPENXML (@hDoc4, '/NewDataSet/Table',2)
                                                    WITH ( numContactId NUMERIC(9) )
                                        ) X
   
                    END
            END 
                                    
                                      
        EXEC sp_xml_removedocument @hDoc4  
    END


IF @numGroupID <> 0 
    BEGIN

        IF CONVERT(VARCHAR(10), @strcontact) <> '' 
            DECLARE @hDoc3 INT                                    
        EXEC sp_xml_preparedocument @hDoc3 OUTPUT, @strcontact                                    
        BEGIN     
                             
            INSERT  INTO ProfileEGroupDTL ( numContactID,
                                            numEmailGroupID )
                    SELECT  Y.numContactId,
                            @numGroupID
                    FROM    ( SELECT    *
                              FROM      OPENXML (@hDoc3, '/NewDataSet/Table',2)
                                        WITH ( numContactId NUMERIC(9) )
                              WHERE     numContactId NOT IN (
                                        SELECT  numContactID
                                        FROM    ProfileEGroupDTL
                                        WHERE   numEmailGroupID = @numGroupID )
                            ) Y

        END 
                                    
                                      
        EXEC sp_xml_removedocument @hDoc3


                                 
    END
GO
