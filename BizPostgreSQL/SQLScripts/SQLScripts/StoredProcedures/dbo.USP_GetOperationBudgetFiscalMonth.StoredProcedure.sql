/****** Object:  StoredProcedure [dbo].[USP_GetOperationBudgetFiscalMonth]    Script Date: 07/26/2008 16:17:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created By Siva
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getoperationbudgetfiscalmonth')
DROP PROCEDURE usp_getoperationbudgetfiscalmonth
GO
CREATE PROCEDURE [dbo].[USP_GetOperationBudgetFiscalMonth]
(@numDomainId as numeric(9)=0)
As
Begin
 declare @month as varchar(2)  
 select @month=isnull(tintFiscalStartMonth,1) from Domain where numDomainID=@numDomainID  
 Print @month
End
GO
