/****** Object:  StoredProcedure [dbo].[USP_GetAlertdetails]    Script Date: 07/26/2008 16:16:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getalertdetails')
DROP PROCEDURE usp_getalertdetails
GO
CREATE PROCEDURE [dbo].[USP_GetAlertdetails]      
@numAlertID as numeric(9)=0  ,    
@numDomainID as numeric(9)=0      
as 

select    
	AD.numAlertDTLid,    
	AD.numAlertID,    
	AD.tintCCManager,    
	ADDT.numEmailTemplate,    
	ADDT.numDaysAfterDue,    
	ADDT.monAmount ,    
	ADDT.numBizDocs,    
	ADDT.numDepartment,    
	ADDT.numNoOfItems,    
	ADDT.numAge ,    
	ADDT.tintAlertOn ,    
	AD.vcDesc ,
	ADDT.numEmailCampaignId   
	from AlertDTL AD    
	join AlertDomainDtl ADDT on ADDT.numAlertDTLid=AD.numAlertDTLid    
	where numAlertID=@numAlertID  and ADDT.numdomainID= @numDomainID
GO
