/****** Object:  StoredProcedure [dbo].[USP_SolManage]    Script Date: 07/26/2008 16:21:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj      
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_solmanage')
DROP PROCEDURE usp_solmanage
GO
CREATE PROCEDURE [dbo].[USP_SolManage]          
@numCategory as numeric(9)=null,           
@vcSolnTitle as varchar(1000)=null,          
@txtSolution as text='',          
@numSolID as numeric(9)=null output,      
@vcLink as varchar(500),  
@numDomainID as numeric(9)=0,
@numUserCntID as numeric(9)=0          
as          
if @numSolID= 0          
begin        
insert into SolutionMaster           
 (numCategory,vcSolnTitle,txtSolution,vcLink,numDomainID,numCreatedby,dtCreatedon,numModifiedBy,dtModifiedOn)          
 values          
 (@numCategory,@vcSolnTitle,@txtSolution,'',@numDomainID,@numUserCntID,getutcdate(),@numUserCntID,getutcdate())          
  set @numSolID=@@identity        
end        
else          
          
update SolutionMaster          
          
 set           
  numCategory=@numCategory,          
  vcSolnTitle=@vcSolnTitle,          
  txtSolution=@txtSolution,      
  vcLink=@vcLink,
  numModifiedBy=@numUserCntID,
  dtModifiedOn=getutcdate()         
 where numSolnID=@numSolID
GO
