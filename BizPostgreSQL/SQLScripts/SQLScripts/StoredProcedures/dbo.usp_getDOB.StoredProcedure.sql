/****** Object:  StoredProcedure [dbo].[usp_getDOB]    Script Date: 07/26/2008 16:17:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getdob')
DROP PROCEDURE usp_getdob
GO
CREATE PROCEDURE [dbo].[usp_getDOB]
	@numContactid NUMERIC(9)=0 
--
AS 
	BEGIN
		SELECT Count(*) FROM AdditionalContactsInformation WHERE numContactId = @numContactid AND bintDOB <>0 AND NOT bintDOB IS NULL
	END
GO
