/****** Object:  StoredProcedure [dbo].[usp_DeleteGroup]    Script Date: 07/26/2008 16:15:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_deletegroup')
DROP PROCEDURE usp_deletegroup
GO
CREATE PROCEDURE [dbo].[usp_DeleteGroup]
	@numGrpID numeric  
-- 
AS
BEGIN
	DECLARE @numDivCount numeric
	SELECT @numDivCount=Count(*) 
		FROM DivisionMaster 
		WHERE numGrpID=@numGrpID

	IF @numDivCount=0 
		BEGIN
			DELETE FROM Groups WHERE numGrpID=@numGrpID
			SELECT 0
		END
	ELSE
		BEGIN
			SELECT 1
		END
END
GO
