/****** Object:  StoredProcedure [dbo].[USP_GetOperationBudgetMasterDetails]    Script Date: 07/26/2008 16:17:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created By Siva

 
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getoperationbudgetmasterdetails')
DROP PROCEDURE usp_getoperationbudgetmasterdetails
GO
CREATE PROCEDURE  [dbo].[USP_GetOperationBudgetMasterDetails]
(@numDomainId as numeric(9)=0,
 @numBudgetId as numeric(9)=0)
As
Begin
	Select * From  OperationBudgetMaster Where numBudgetId=@numBudgetId 
	And numDomainId=@numDomainId 
End
GO
