/****** Object:  StoredProcedure [dbo].[USP_GetCountOfJournalEntry]    Script Date: 07/26/2008 16:17:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created by Siva      
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getcountofjournalentry')
DROP PROCEDURE usp_getcountofjournalentry
GO
CREATE PROCEDURE [dbo].[USP_GetCountOfJournalEntry]          
@numParntAcntId numeric(9)=0,      
@numDomainId numeric(9)=0       
as           
Begin          
Select Count(numChartAcntId) From General_Journal_Details 
Where numChartAcntId=@numParntAcntId And numDomainId=@numDomainId      
End
GO
