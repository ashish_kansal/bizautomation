/****** Object:  StoredProcedure [dbo].[usp_OppItemCustomFields]    Script Date: 07/26/2008 16:20:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--DROP TABLE ##OppItemCustomField                    
--SELECT * FROM ##OppItemCustomField                    
                    
--Created By: Debasish Nag                        
--Created On: 12th Nov 2005                        
--Purpose: To Get the Custom Fields for Opportunities and Items and return the same as a pivoted table in a view                            
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_oppitemcustomfields')
DROP PROCEDURE usp_oppitemcustomfields
GO
CREATE PROCEDURE [dbo].[usp_OppItemCustomFields]              
 @numRefreshTimeInterval Numeric                            
AS                            
BEGIN                            
 DECLARE @tempOppItemTable NVARCHAR(100)                  
 DECLARE @tempOrgContactTable NVARCHAR(100)                            
 SET @tempOppItemTable = '##OppItemCustomField'                                
 SET @tempOrgContactTable = '##OrgContactCustomField'                      
                      
 DECLARE @vcColumnListNew NVarchar(4000)                      
 DECLARE @vcColumnListNewOpp NVarchar(4000)                      
 DECLARE @vcColumnListNewItem NVarchar(4000)                      
                            
 SELECT @vcColumnListNewOpp = dbo.fn_OppAndItemsCustomFieldsColumns(2,1)                            
 --PRINT 'ColumnListNewOpp: ' + @vcColumnListNewOpp                            
 SELECT @vcColumnListNewItem = dbo.fn_OppAndItemsCustomFieldsColumns(5,1)                            
 --PRINT 'ColumnListNewItem: ' + @vcColumnListNewItem                            
 IF @vcColumnListNewOpp Is Not NULL AND @vcColumnListNewItem Is Not NULL                            
  SELECT @vcColumnListNew = @vcColumnListNewOpp +  ', ' + @vcColumnListNewItem                            
 ELSE IF  @vcColumnListNewOpp Is NULL AND @vcColumnListNewItem Is Not NULL                         
  SELECT @vcColumnListNew = @vcColumnListNewItem                            
 ELSE IF  @vcColumnListNewItem Is NULL AND @vcColumnListNewOpp Is Not NULL                            
  SELECT @vcColumnListNew = @vcColumnListNewOpp                            
                            
 --PRINT @vcColumnListNew                            
                            
 IF NOT EXISTS (SELECT 'x' FROM tempdb..sysobjects WHERE type = 'U' and NAME = @tempOppItemTable)                            
  BEGIN                            
   --Print 'Table To Be Created'                     
   DECLARE @OppItemTempCustomTableColumns NVarchar(4000)                      
   DECLARE @OppTempCustomTableQuery NVarchar(4000)                      
   DECLARE @ItemTempCustomTableQuery NVarchar(4000)                      
   SELECT @OppTempCustomTableQuery = dbo.fn_OppAndItemsCustomFieldsColumns(2,0)                      
   SELECT @ItemTempCustomTableQuery = dbo.fn_OppAndItemsCustomFieldsColumns(5,0)                         
                            
   IF @OppTempCustomTableQuery Is Not NULL AND @ItemTempCustomTableQuery Is Not NULL                            
    SELECT @OppItemTempCustomTableColumns = @OppTempCustomTableQuery +  ', ' + @ItemTempCustomTableQuery                            
   ELSE IF  @OppTempCustomTableQuery Is NULL AND @ItemTempCustomTableQuery Is Not NULL                            
    SELECT @OppItemTempCustomTableColumns = @ItemTempCustomTableQuery                            
   ELSE IF  @ItemTempCustomTableQuery Is NULL AND @OppTempCustomTableQuery Is Not NULL                            
    SELECT @OppItemTempCustomTableColumns = @OppTempCustomTableQuery                      
   --PRINT '@OppItemTempCustomTableColumns: ' + @OppItemTempCustomTableColumns                    
                    
   DECLARE @vwOppItemTableColumns Nvarchar(4000)                       
                    
   SELECT @vwOppItemTableColumns = Coalesce(@vwOppItemTableColumns + ', ', '') + name                     
   FROM dbo.syscolumns                     
    WHERE ID = (                    
    SELECT ID FROM dbo.sysobjects                    
    WHERE name  = 'vw_OppItemsCustomReport'                    
    AND xType = 'V'                    
    )         
   AND colid > 4                    
   ORDER BY colid                     
    --PRINT @vwOppItemTableColumns                 
                    
    DECLARE @vwOppItemTableColumnsAndDataType Nvarchar(4000)                       
       
    SELECT @vwOppItemTableColumnsAndDataType = Coalesce(@vwOppItemTableColumnsAndDataType + ', ', '') + c.name + ' ' + t.name +  + CASE WHEN t.name <> 'tinyInt' AND t.name <> 'DECIMAL(20,5)' AND t.name <> 'int' Then                     
                  CASE WHEN (c.prec IS NULL OR t.name = 'bit') THEN ''                     
                  ELSE                     
                   '(' + Cast(c.prec As NVarchar) + CASE WHEN c.scale IS NULL THEN ')' ELSE ',' + Cast(c.scale AS Nvarchar) + ')' END                     
                  END                    
                 ELSE                    
               ''                    
                 END                    
    FROM dbo.syscolumns c, (SELECT name, type, xtype, xusertype, usertype FROM dbo.systypes) t                    
    WHERE c.ID = (                    
    SELECT ID FROM dbo.sysobjects                    
    WHERE name  = 'vw_OppItemsCustomReport'                    
    AND xType = 'V'                    
    )                     
   --AND t.type = c.type                    
   AND t.xusertype = c.xusertype                    
   AND t.usertype = c.usertype                    
   AND colid > 4                   
   ORDER BY colid                     
   --PRINT @vwOppItemTableColumnsAndDataType                    
                    
   DECLARE @OrgAndContCustomFields as NVarchar(1000)                    
   DECLARE @OrgAndContCustomFieldsAndDataTypes as NVarchar(1000)                    
   EXEC('usp_OrganizationContactsCustomFields ' +@numRefreshTimeInterval)                    
   SELECT @OrgAndContCustomFields = Coalesce(@OrgAndContCustomFields + ', ', '') + name,                    
   @OrgAndContCustomFieldsAndDataTypes = Coalesce(@OrgAndContCustomFieldsAndDataTypes + ', ', '') + name + ' NVarchar(50)'                     
   FROM tempdb..syscolumns                     
    WHERE ID = (                    
    SELECT ID FROM tempdb..sysobjects                    
    WHERE name  = @tempOrgContactTable                
    AND xType = 'U'                    
    )                    
   AND name like 'CFld%'                
   ORDER BY colid                     
   PRINT @OrgAndContCustomFields                    
                    
   DECLARE @OppTempCustomTableCreateQuery1st Nvarchar(4000)                    
   DECLARE @OppTempCustomTableCreateQuery2nd Nvarchar(4000)                    
   DECLARE @OppTempCustomTableCreateQuery3rd Nvarchar(4000)                    
   SELECT @OppTempCustomTableCreateQuery1st = 'CREATE TABLE ' + @tempOppItemTable + '(numOppId Numeric, vcColumnList NVarchar(2000), CreatedDateTime DateTime, '                     
   SELECT @OppTempCustomTableCreateQuery2nd = @vwOppItemTableColumnsAndDataType                
   SELECT @OppTempCustomTableCreateQuery3rd = CASE WHEN @OppItemTempCustomTableColumns IS Not NULL THEN ', ' +  @OppItemTempCustomTableColumns ELSE '' END + CASE WHEN @OrgAndContCustomFieldsAndDataTypes IS Not NULL THEN ',       
                                              ' + @OrgAndContCustomFieldsAndDataTypes ELSE '' END + ')'                    
   PRINT @OppTempCustomTableCreateQuery1st                            
   PRINT @OppTempCustomTableCreateQuery2nd                            
   PRINT @OppTempCustomTableCreateQuery3rd                            
   EXEC (@OppTempCustomTableCreateQuery1st + @OppTempCustomTableCreateQuery2nd + @OppTempCustomTableCreateQuery3rd)                            
                            
    DECLARE @OppItemTempCustomTableDataEntryQuery1st NVarchar(4000)                     
    DECLARE @OppItemTempCustomTableDataEntryQuery2nd NVarchar(4000)                      
    DECLARE @OppItemTempCustomTableDataEntryQuery3rd NVarchar(4000)                       
    DECLARE @OppItemTempCustomTableDataEntryQuery4th NVarchar(4000)                
    SELECT @OppItemTempCustomTableDataEntryQuery1st = 'INSERT INTO ' + @tempOppItemTable + ' (numOppId, vcColumnList, CreatedDateTime, ' + @vwOppItemTableColumns +              
    CASE WHEN (@OppTempCustomTableQuery Is Not NULL)  AND (@ItemTempCustomTableQuery Is Not NULL) THEN                    
     ', ' + dbo.fn_OppAndItemsCustomFieldsColumns(2,1) + ', ' + dbo.fn_OppAndItemsCustomFieldsColumns(5,1)                    
    WHEN (@OppTempCustomTableQuery Is NULL AND @ItemTempCustomTableQuery Is Not NULL) THEN                    
    ', ' + dbo.fn_OppAndItemsCustomFieldsColumns(5,1)                    
    WHEN (@ItemTempCustomTableQuery Is NULL AND @OppTempCustomTableQuery Is NOT NULL) THEN                    
    ', ' + dbo.fn_OppAndItemsCustomFieldsColumns(2,1)                    
    ELSE              
    ''              
    END              
    +              
    CASE WHEN (@OrgAndContCustomFields IS NULL) THEN              
    ') '              
    ELSE              
    ', ' + @OrgAndContCustomFields + ') '              
    END              
              
    SELECT @OppItemTempCustomTableDataEntryQuery2nd = ' SELECT numOppId, ''' + CASE WHEN @vcColumnListNew IS NOT NULL THEN @vcColumnListNew ELSE '' END + ''', getutcdate(), ' + @vwOppItemTableColumns              
              
    IF (@OppTempCustomTableQuery Is Not NULL)  AND (@ItemTempCustomTableQuery Is Not NULL)              
     SELECT @OppItemTempCustomTableDataEntryQuery3rd = ', ' + dbo.fn_OppAndItemsCustomFieldsColumns(2,2)  + ', ' + dbo.fn_OppAndItemsCustomFieldsColumns(5,2)              
    ELSE IF(@OppTempCustomTableQuery Is NULL AND @ItemTempCustomTableQuery Is NOT NULL)              
     SELECT @OppItemTempCustomTableDataEntryQuery3rd = ', ' + dbo.fn_OppAndItemsCustomFieldsColumns(5,2)              
    ELSE IF(@ItemTempCustomTableQuery Is NULL AND @OppTempCustomTableQuery Is Not NULL)              
     SELECT @OppItemTempCustomTableDataEntryQuery3rd = ', ' + dbo.fn_OppAndItemsCustomFieldsColumns(2,2)              
    ELSE              
     SELECT @OppItemTempCustomTableDataEntryQuery3rd = ''              
              
    SELECT @OppItemTempCustomTableDataEntryQuery4th = CASE WHEN (@OrgAndContCustomFields IS NULL) THEN '' ELSE ', ' + @OrgAndContCustomFields END + '       
                                                      FROM vw_OppItemsCustomReport opp, (SELECT Distinct numDivisionId, numContactId' + CASE WHEN @OrgAndContCustomFields IS NOT NULL THEN ', ' + @OrgAndContCustomFields ELSE '' END + '       
                                                      FROM  ##OrgContactCustomField) org WHERE opp.numDivisionId = org.numDivisionId AND opp.numContactId = org.numContactId  ORDER BY numOppId, vcItemCode'              
              
    PRINT @OppItemTempCustomTableDataEntryQuery1st              
    PRINT @OppItemTempCustomTableDataEntryQuery2nd              
    PRINT @OppItemTempCustomTableDataEntryQuery3rd              
    PRINT @OppItemTempCustomTableDataEntryQuery4th              
    EXEC (@OppItemTempCustomTableDataEntryQuery1st + @OppItemTempCustomTableDataEntryQuery2nd + @OppItemTempCustomTableDataEntryQuery3rd + @OppItemTempCustomTableDataEntryQuery4th)                    
    --EXEC ('SELECT * FROM ' + @tempOppItemTable)              
  END              
 ELSE              
  BEGIN              
   IF (NOT EXISTS (SELECT TOP 1 vcColumnList FROM ##OppItemCustomField WHERE vcColumnList = @vcColumnListNew)) OR ((SELECT TOP 1 numOppID FROM ##OppItemCustomField WHERE DATEDIFF(mi,  CreatedDateTime, getutcdate()) > @numRefreshTimeInterval) > 0)           
  
   
   BEGIN              
      EXEC('DROP TABLE ' +  @tempOppItemTable)           
      IF EXISTS (SELECT 'x' FROM tempdb..sysobjects WHERE type = 'U' and NAME = @tempOrgContactTable)            
        EXEC('DROP TABLE ' +  @tempOrgContactTable)              
      EXEC ('usp_OppItemCustomFields ' + @numRefreshTimeInterval)              
   END     
  END                            
END
GO
