/****** Object:  StoredProcedure [dbo].[Select_Action_Item_Name]    Script Date: 07/26/2008 16:14:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='select_action_item_name')
DROP PROCEDURE select_action_item_name
GO
CREATE PROCEDURE [dbo].[Select_Action_Item_Name]

@numDomainID as numeric(9)  ,
@numUserCntID as numeric(9)   
As  
Select RowID , TemplateName From tblActionItemData  where numdomainId=@numDomainID 
and (numCreatedby =@numUserCntID or @numUserCntID=0)
GO
