/****** Object:  StoredProcedure [dbo].[usp_UpdateCases]    Script Date: 07/26/2008 16:21:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_updatecases')
DROP PROCEDURE usp_updatecases
GO
CREATE PROCEDURE [dbo].[usp_UpdateCases]
	@intTargetResolveDate int=0,
	@textSubject text='',
	@vcStatus varchar(50),
	@vcPriority varchar(25)='',
	@textDesc text='',
	@textInternalComments text='',
	@vcReason varchar(250)='',
	@vcOrigin varchar(50),
	@vcType varchar(50),
	@vcAssignTo varchar(100)='',
	@numCaseID numeric(9)=0   
--
AS
	--Insertion into Table  
	Update Cases set intTargetResolveDate=@intTargetResolveDate,
	textSubject=@textSubject,numStatus=@vcStatus,numPriority=@vcPriority,
	textDesc=@textDesc,textInternalComments=@textInternalComments,
	numReason=@vcReason,numOrigin=@vcOrigin,numType=@vcType,
	numAssignedTo=@vcAssignTo
	WHERE numCaseID=@numCaseID
GO
