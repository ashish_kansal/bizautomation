/****** Object:  StoredProcedure [dbo].[USP_GetOppAddress]    Script Date: 05/08/2009 15:45:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj    
-- Modified by Ajit Singh on 01/10/2008 for Division ID in each select statement. 
-- like "@numDivisionID as DivisionID"
-- EXEC dbo.USP_GetOppAddress 2967,2
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'usp_getoppaddress' ) 
    DROP PROCEDURE usp_getoppaddress
GO
CREATE PROCEDURE [dbo].[USP_GetOppAddress]
    @numOppID AS NUMERIC(9) = 0,
    @byteMode AS TINYINT,
    @numReturnHeaderID AS NUMERIC(9) = 0
AS 
    DECLARE @tintType AS TINYINT
    DECLARE @numDomainID AS NUMERIC(9) 
    DECLARE @numDivisionID AS NUMERIC(9)
    DECLARE @numContactID AS NUMERIC(9)
    DECLARE @Name AS VARCHAR(100)
    DECLARE @Phone AS VARCHAR(20)

    IF @byteMode = 0 -- Get Billing Address
        BEGIN
            SELECT  @tintType = tintBillToType,
                    @numDomainID = numDomainID,
                    @numDivisionID = numDivisionID,
                    @numContactID = numContactID
            FROM    OpportunityMaster
            WHERE   numOppId = @numOppID
            SELECT  @Name = ISNULL(vcFirstName, '') + ' ' + ISNULL(vcLastname, ''),
                    @Phone = (CASE WHEN LEN(ISNULL(numPhone,'')) > 0 THEN numPhone ELSE vcComPhone END)
            FROM    AdditionalContactsInformation
			INNER JOIN
				DivisionMaster 
			ON
				AdditionalContactsInformation.numDivisionId = DivisionMaster.numDivisionID
            WHERE   numContactID = @numContactID
            IF ( @tintType IS NULL
                 OR @tintType = 1
               ) 
                SELECT  @Name AS Name,
                        @Phone AS Phone,
                        dbo.fn_GetComapnyName(@numDivisionID) AS Company,
                        @numDivisionID AS DivisionID,
                        AD.VcStreet AS Street,
                        AD.VcCity AS City,
                        AD.numState AS State,
                        AD.vcPostalCode AS PostCode,
                        AD.numCountry AS Country,
						AD.numContact,
						(CASE WHEN ISNULL(AD.bitAltContact,0)=0 THEN CONCAT(AdditionalContactsInformation.vcFirstName,' ',AdditionalContactsInformation.vcLastName) ELSE '' END) vcContact,
						AD.bitAltContact,
						AD.vcAltContact
                FROM    dbo.AddressDetails AD
				LEFT JOIN
					AdditionalContactsInformation
				ON
					AD.numContact = AdditionalContactsInformation.numContactId
                WHERE   AD.numRecordID = @numDivisionID
                        AND AD.tintAddressOf = 2
                        AND AD.tintAddressType = 1
                        AND AD.bitIsPrimary = 1
            ELSE 
                IF @tintType = 0 
                    SELECT  @Name AS Name,
                            @Phone AS Phone,
                            Com1.vcCompanyname AS Company,
                            @numDivisionID AS DivisionID,
                            AD1.VcStreet AS Street,
                            AD1.VcCity AS City,
                            AD1.numState AS State,
                            AD1.vcPostalCode AS PostCode,
                            AD1.numCountry AS Country,
							AD1.numContact,
							(CASE WHEN ISNULL(AD1.bitAltContact,0)=0 THEN CONCAT(AdditionalContactsInformation.vcFirstName,' ',AdditionalContactsInformation.vcLastName) ELSE '' END) vcContact,
							AD1.bitAltContact,
							AD1.vcAltContact
                    FROM    companyinfo Com1
                            JOIN divisionmaster div1 ON com1.numCompanyID = div1.numCompanyID
                            JOIN Domain D1 ON D1.numDivisionID = div1.numDivisionID
                            JOIN dbo.AddressDetails AD1 ON AD1.numDomainID = div1.numDomainID
                                                           AND AD1.numRecordID = div1.numDivisionID
                                                           AND tintAddressOf = 2
                                                           AND tintAddressType = 1
                                                           AND bitIsPrimary = 1
							LEFT JOIN
								AdditionalContactsInformation
							ON
								AD1.numContact = AdditionalContactsInformation.numContactId
                    WHERE   D1.numDomainID = @numDomainID
                ELSE 
                    IF @tintType = 2 
                        SELECT  @Name AS Name,
                                @Phone AS Phone,
                                vcBillCompanyName AS Company,
                                numBillCompanyID AS DivisionID,
                                vcBillStreet AS Street,
                                vcBillCity AS City,
                                numBillState AS State,
                                vcBillPostCode AS PostCode,
                                numBillCountry AS Country,
                                dbo.GetCompanyNameFromContactID(@numContactID, @numDomainID) AS OppCompanyName,
								numBillingContact AS numContact,
								(CASE WHEN ISNULL(bitAltBillingContact,0)=0 THEN CONCAT(AdditionalContactsInformation.vcFirstName,' ',AdditionalContactsInformation.vcLastName) ELSE '' END) vcContact,
								bitAltBillingContact AS bitAltContact,
								vcAltBillingContact AS vcAltContact
                        FROM    OpportunityAddress
						LEFT JOIN
								AdditionalContactsInformation
							ON
								OpportunityAddress.numBillingContact = AdditionalContactsInformation.numContactId
                        WHERE   numOppID = @numOppID
                    ELSE 
                        IF @tintType = 3 
                            SELECT  @Name AS Name,
                                    @Phone AS Phone,
                                    vcBillCompanyName AS Company,
                                    numBillCompanyID AS DivisionID,
                                    vcBillStreet AS Street,
                                    vcBillCity AS City,
                                    numBillState AS State,
                                    vcBillPostCode AS PostCode,
                                    numBillCountry AS Country,
                                    dbo.GetCompanyNameFromContactID(@numContactID, @numDomainID) AS OppCompanyName,
									numBillingContact AS numContact,
									(CASE WHEN ISNULL(bitAltBillingContact,0)=0 THEN CONCAT(AdditionalContactsInformation.vcFirstName,' ',AdditionalContactsInformation.vcLastName) ELSE '' END) vcContact,
									bitAltBillingContact AS bitAltContact,
									vcAltBillingContact AS vcAltContact
                            FROM    OpportunityAddress
							LEFT JOIN
								AdditionalContactsInformation
							ON
								OpportunityAddress.numBillingContact = AdditionalContactsInformation.numContactId
                            WHERE   numOppID = @numOppID
        END
    ELSE 
        IF @byteMode = 1 -- Get Shipping Address
            BEGIN
                SELECT  @tintType = tintShipToType,
                        @numDomainID = numDomainID,
                        @numDivisionID = numDivisionID,
                        @numContactID = numContactID
                FROM    OpportunityMaster
                WHERE   numOppId = @numOppID
                SELECT  @Name = ISNULL(vcFirstName, '') + ' '
                        + ISNULL(vcLastname, ''),
                        @Phone = (CASE WHEN LEN(ISNULL(numPhone,'')) > 0 THEN numPhone ELSE vcComPhone END)
                FROM    AdditionalContactsInformation
				INNER JOIN
					DivisionMaster 
				ON
					AdditionalContactsInformation.numDivisionId = DivisionMaster.numDivisionID
                WHERE   numContactID = @numContactID

                IF (@tintType IS NULL OR @tintType = 1)
				BEGIN
                    SELECT  @Name AS Name,
                            @Phone AS Phone,
                            dbo.fn_GetComapnyName(@numDivisionID) AS Company,
                            @numDivisionID AS DivisionID,
                            AD.VcStreet AS Street,
                            AD.VcCity AS City,
                            AD.numState AS State,
                            AD.vcPostalCode AS PostCode,
                            AD.numCountry AS Country,
							AD.numContact,
							(CASE WHEN ISNULL(AD.bitAltContact,0)=0 THEN CONCAT(AdditionalContactsInformation.vcFirstName,' ',AdditionalContactsInformation.vcLastName) ELSE '' END) vcContact,
							AD.bitAltContact,
							AD.vcAltContact,
							dbo.fn_GetComapnyName(@numDivisionID) AS vcShipCompanyName
                    FROM    dbo.AddressDetails AD
					LEFT JOIN
						AdditionalContactsInformation
					ON
						AD.numContact = AdditionalContactsInformation.numContactId
                    WHERE   AD.numRecordID = @numDivisionID
                            AND AD.tintAddressOf = 2
                            AND AD.tintAddressType = 2
                            AND AD.bitIsPrimary = 1
				END
                ELSE IF @tintType = 0
				BEGIN
                    SELECT  (CASE 
								WHEN ISNULL(bitAltContact,0)=1 AND LEN(ISNULL(vcAltContact,'')) > 0
								THEN vcAltContact
								WHEN ISNULL(numContact,0) > 0
								THEN ISNULL(vcFirstName, '') + ' ' + ISNULL(vcLastname, '')
								ELSE @Name
							END) AS Name,
                            (CASE 
								WHEN ISNULL(bitAltContact,0)=1 AND LEN(ISNULL(vcAltContact,'')) > 0
								THEN (SELECT ISNULL(DM.vcComPhone,0) FROM DivisionMaster DM WHERE DM.numDivisionID=@numDivisionID)
								WHEN ISNULL(numContact,0) > 0
								THEN (CASE WHEN LEN(ISNULL(numPhone,'')) > 0 THEN  numPhone ELSE (SELECT ISNULL(DM.vcComPhone,0) FROM DivisionMaster DM WHERE DM.numDivisionID=@numDivisionID) END)
								ELSE @Phone
							END) AS Phone,
                            com1.vcCompanyname AS Company,
                            @numDivisionID AS DivisionID,
                            AD1.VcStreet AS Street,
                            AD1.VcCity AS City,
                            AD1.numState AS State,
                            AD1.vcPostalCode AS PostCode,
                            AD1.numCountry AS Country,
							AD1.numContact,
							(CASE WHEN ISNULL(AD1.bitAltContact,0)=0 THEN CONCAT(AdditionalContactsInformation.vcFirstName,' ',AdditionalContactsInformation.vcLastName) ELSE '' END) vcContact,
							AD1.bitAltContact,
							AD1.vcAltContact,
							com1.vcCompanyname AS vcShipCompanyName
                    FROM    companyinfo Com1
                            JOIN divisionmaster div1 ON com1.numCompanyID = div1.numCompanyID
                            JOIN Domain D1 ON D1.numDivisionID = div1.numDivisionID
                            JOIN dbo.AddressDetails AD1 ON AD1.numDomainID = div1.numDomainID
                                                            AND AD1.numRecordID = div1.numDivisionID
                                                            AND tintAddressOf = 2
                                                            AND tintAddressType = 2
                                                            AND bitIsPrimary = 1
							LEFT JOIN
								AdditionalContactsInformation
							ON
								AD1.numContact = AdditionalContactsInformation.numContactId
                    WHERE   D1.numDomainID = @numDomainID
                END    
				ELSE IF @tintType = 2 
				BEGIN
                    SELECT  (CASE 
								WHEN ISNULL(bitAltShippingContact,0)=1 AND LEN(ISNULL(vcAltShippingContact,'')) > 0
								THEN vcAltShippingContact
								WHEN ISNULL(numShippingContact,0) > 0
								THEN ISNULL(vcFirstName, '') + ' ' + ISNULL(vcLastname, '')
								ELSE @Name
							END) AS Name,
                            (CASE 
								WHEN ISNULL(bitAltShippingContact,0)=1 AND LEN(ISNULL(vcAltShippingContact,'')) > 0
								THEN (SELECT ISNULL(DM.vcComPhone,0) FROM DivisionMaster DM WHERE DM.numDivisionID=@numDivisionID)
								WHEN ISNULL(numShippingContact,0) > 0
								THEN (CASE WHEN LEN(ISNULL(numPhone,'')) > 0 THEN  numPhone ELSE (SELECT ISNULL(DM.vcComPhone,0) FROM DivisionMaster DM WHERE DM.numDivisionID=@numDivisionID) END)
								ELSE @Phone
							END) AS Phone,
                            dbo.fn_GetComapnyName(@numDivisionID) AS Company,
							@numDivisionID AS DivisionID,
                            vcShipStreet AS Street,
                            vcShipCity AS City,
                            numShipState AS State,
                            vcShipPostCode AS PostCode,
                            numShipCountry AS Country,
                            dbo.GetCompanyNameFromContactID(@numContactID, @numDomainID) AS OppCompanyName,
							numShippingContact AS numContact,
							(CASE WHEN ISNULL(bitAltShippingContact,0)=0 THEN CONCAT(AdditionalContactsInformation.vcFirstName,' ',AdditionalContactsInformation.vcLastName) ELSE '' END) vcContact,
							bitAltShippingContact AS bitAltContact,
							vcAltShippingContact AS vcAltContact,
							vcShipCompanyName
                    FROM    OpportunityAddress
					LEFT JOIN
						AdditionalContactsInformation
					ON
						OpportunityAddress.numShippingContact = AdditionalContactsInformation.numContactId
                    WHERE   numOppID = @numOppID
                END
				ELSE IF @tintType = 3
				BEGIN
                    SELECT  (CASE 
								WHEN ISNULL(bitAltShippingContact,0)=1 AND LEN(ISNULL(vcAltShippingContact,'')) > 0
								THEN vcAltShippingContact
								WHEN ISNULL(numShippingContact,0) > 0
								THEN ISNULL(vcFirstName, '') + ' ' + ISNULL(vcLastname, '')
								ELSE @Name
							END) AS Name,
                                (CASE 
								WHEN ISNULL(bitAltShippingContact,0)=1 AND LEN(ISNULL(vcAltShippingContact,'')) > 0
								THEN (SELECT ISNULL(DM.vcComPhone,0) FROM DivisionMaster DM WHERE DM.numDivisionID=@numDivisionID)
								WHEN ISNULL(numShippingContact,0) > 0
								THEN (CASE WHEN LEN(ISNULL(numPhone,'')) > 0 THEN  numPhone ELSE (SELECT ISNULL(DM.vcComPhone,0) FROM DivisionMaster DM WHERE DM.numDivisionID=@numDivisionID) END)
								ELSE @Phone
							END) AS Phone,
                            dbo.fn_GetComapnyName(@numDivisionID) AS Company,
							@numDivisionID AS DivisionID,
                            vcShipStreet AS Street,
                            vcShipCity AS City,
                            numShipState AS State,
                            vcShipPostCode AS PostCode,
                            numShipCountry AS Country,
                            dbo.GetCompanyNameFromContactID(@numContactID, @numDomainID) AS OppCompanyName,
							numShippingContact AS numContact,
							(CASE WHEN ISNULL(bitAltShippingContact,0)=0 THEN CONCAT(AdditionalContactsInformation.vcFirstName,' ',AdditionalContactsInformation.vcLastName) ELSE '' END) vcContact,
							bitAltShippingContact AS bitAltContact,
							vcAltShippingContact AS vcAltContact,
							vcShipCompanyName
                    FROM    OpportunityAddress
					LEFT JOIN
						AdditionalContactsInformation
					ON
						OpportunityAddress.numShippingContact = AdditionalContactsInformation.numContactId
                    WHERE   numOppID = @numOppID
				END
            END
    
    IF @byteMode = 2 -- Get Billing Address
        BEGIN
    
            SELECT  @numDomainID = numDomainID,
                    @numDivisionID = numDivisionID,
                    @numContactID = numContactID
            FROM    dbo.ReturnHeader
            WHERE   numReturnHeaderID = @numReturnHeaderID
      
            SELECT  @Name = ISNULL(vcFirstName, '') + ' ' + ISNULL(vcLastname, ''),
                    @Phone = numPhone
            FROM    AdditionalContactsInformation
            WHERE   numContactID = @numContactID
      
            SELECT  @Name AS Name,
                    @Phone AS Phone,
                    vcBillCompanyName AS Company,
                    @numDivisionID AS DivisionID,
                    vcBillStreet AS Street,
                    vcBillCity AS City,
                    numBillState AS State,
                    vcBillPostCode AS PostCode,
                    numBillCountry AS Country,
                    dbo.GetCompanyNameFromContactID(@numContactID,
                                                    @numDomainID) AS OppCompanyName
            FROM    OpportunityAddress
            WHERE   numReturnHeaderID = @numReturnHeaderID
		
        END
	
	IF @byteMode = 3 -- Get Shipping Address
        BEGIN
    
			DECLARE @numAddressID AS NUMERIC(18,0)
			DECLARE @vcCompanyName AS VARCHAR(100)
			
						
            
            SELECT  @numAddressID = numAddressID, @numDivisionId = numDivisionId
            FROM    dbo.ProjectsMaster
            WHERE   numProId = @numOppID
						
            SELECT  @Name = ISNULL(vcFirstName, '') + ' ' + ISNULL(vcLastname, ''),
                    @Phone = numPhone,
                    @numContactID = numContactId,
                    @vcCompanyName = ''
            FROM    AdditionalContactsInformation
            WHERE numDomainID = @numDomainId
			AND numDivisionId = @numDivisionId
			AND ISNULL(bitPrimaryContact,0) = 1
      
            SELECT  (CASE 
						WHEN ISNULL(bitAltContact,0)=1 AND LEN(ISNULL(vcAltContact,'')) > 0
						THEN vcAltContact
						WHEN ISNULL(numContact,0) > 0
						THEN ISNULL(vcFirstName, '') + ' ' + ISNULL(vcLastname, '')
						ELSE @Name
					END) AS Name,
                    (CASE 
						WHEN ISNULL(bitAltContact,0)=1 AND LEN(ISNULL(vcAltContact,'')) > 0
						THEN (SELECT ISNULL(DM.vcComPhone,0) FROM DivisionMaster DM WHERE DM.numDivisionID=@numDivisionID)
						WHEN ISNULL(numContact,0) > 0
						THEN (CASE WHEN LEN(ISNULL(numPhone,'')) > 0 THEN  numPhone ELSE (SELECT ISNULL(DM.vcComPhone,0) FROM DivisionMaster DM WHERE DM.numDivisionID=@numDivisionID) END)
						ELSE @Phone
					END) AS Phone,
                    @vcCompanyName AS Company,
                    @numDivisionID AS DivisionID,
                    vcStreet AS Street,
                    vcCity AS City,
                    numState AS State,
                    vcPostalCode AS PostCode,
                    numCountry AS Country,
                    dbo.GetCompanyNameFromContactID(@numContactID,@numDomainID) AS OppCompanyName
            FROM   dbo.AddressDetails
			LEFT JOIN
				AdditionalContactsInformation
			ON
				AddressDetails.numContact = AdditionalContactsInformation.numContactId
            WHERE  numRecordID = @numOppID
		
        END        
        

