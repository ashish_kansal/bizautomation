/****** Object:  StoredProcedure [dbo].[usp_getCustomReports4MyReports]    Script Date: 07/26/2008 16:17:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created By: Debasish Nag  
--Created On: 9th Nov 2005  
--Purpose: To Return the Custom Reports which were saved as My Reports  
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getcustomreports4myreports')
DROP PROCEDURE usp_getcustomreports4myreports
GO
CREATE PROCEDURE [dbo].[usp_getCustomReports4MyReports]    
 @numUserCntID numeric(9)  
AS    
  SELECT numCustReportID, vcReportName + ' (' + vcReportDescription +   
')' AS vcReportName FROM CustRptConfigMaster  
  WHERE numCreatedBy = @numUserCntID  
  ORDER BY numCreatedDate DESC
GO
