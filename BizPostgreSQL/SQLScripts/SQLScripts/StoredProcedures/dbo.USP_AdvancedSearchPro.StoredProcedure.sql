/****** Object:  StoredProcedure [dbo].[USP_AdvancedSearchPro]    Script Date: 07/26/2008 16:14:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_advancedsearchpro')
DROP PROCEDURE usp_advancedsearchpro
GO
CREATE PROCEDURE [dbo].[USP_AdvancedSearchPro]
@WhereCondition as varchar(1000)='',                                                        
@numDomainID as numeric(9)=0,                                      
@numUserCntID as numeric(9)=0,                                     
@CurrentPage int,                                                                            
@PageSize int,                                                                            
@TotRecs int output,                                                                                                                     
@columnSortOrder as Varchar(10),        
@ColumnName as varchar(20)='',        
@SortCharacter as char(1),                                
@SortColumnName as varchar(20)='',        
@LookTable as varchar(10)='' ,
@vcDisplayColumns VARCHAR(MAX),
@GetAll BIT
as
     
declare @tintOrder as tinyint                                      
declare @vcFormFieldName as varchar(50)                                      
declare @vcListItemType as varchar(3)                                 
declare @vcListItemType1 as varchar(3)                                     
declare @vcAssociatedControlType varchar(20)                                      
declare @numListID AS numeric(9)                                      
declare @vcDbColumnName varchar(30)                                       
declare @ColumnSearch as varchar(10)                              
if (@SortCharacter<>'0' and @SortCharacter<>'') set @ColumnSearch=@SortCharacter       
Create table #tempTable ( ID INT IDENTITY PRIMARY KEY,                                                                             
      numProID varchar(15)                                                                            
 )      
      
declare @strSql as varchar(8000)                                                                      
 set  @strSql='select numProID from       
ProjectsMaster ProMas
join DivisionMaster DM on Dm.numDivisionId = ProMas.numDivisionId    
JOIN CompanyInfo C ON DM.numCompanyID = C.numCompanyId
LEFT JOIN AdditionalContactsinformation ADC on ADC.numContactId = ProMas.numIntPrjMgr
'      
      
                                 
   if (@SortColumnName<>'')                            
  begin                                  
 select @vcListItemType1=vcListItemType from View_DynamicDefaultColumns where vcDbColumnName=@SortColumnName and numFormID=1  and numDomainId=@numDomainId                                
  if @vcListItemType1='L'                           
  begin                                        
    set @strSql= @strSql +' left Join ListDetails L2 on L2.numListItemID='+@SortColumnName                                      
  end                                
  end                                  
  set @strSql=@strSql+' where ProMas.numDomainID  = '+convert(varchar(15),@numDomainID)+   @WhereCondition                                          
         
      
if (@ColumnName<>'' and  @ColumnSearch<>'')                                 
begin                                  
  if @vcListItemType='L'                                       
    begin                                        
      set @strSql= @strSql +' and L1.vcData like '''+@ColumnSearch +'%'''                                     
    end                                      
    else set @strSql= @strSql +' and '+  @ColumnName +' like '''+@ColumnSearch  +'%'''                                    
                                  
end                                  
   if (@SortColumnName<>'')                                
  begin                                   
    if @vcListItemType1='L'                                       
    begin            
      set @strSql= @strSql +' order by L2.vcData '+@columnSortOrder              
    end                                      
    else  set @strSql= @strSql +' order by '+ case  when @SortColumnName = 'txtComments' then 'convert(varchar(500),txtComments)' 
		  else @SortColumnName end  +' ' +@columnSortOrder                                
  end       
      
      
insert into #tempTable(numProID)                       
      
exec(@strSql)        
 print @strSql     
 print '===================================================='      
                            
set @strSql=''                                      
                                      
                                     
set @tintOrder=0                                      
set @WhereCondition =''                                   
set @strSql='select ProMas.numProID '

	DECLARE @TEMPSelectedColumns TABLE
	(
		numFieldID NUMERIC(18,0)
		,bitCustomField BIT
		,tintOrder INT
		,intColumnWidth FLOAT
	)
	DECLARE @vcLocationID VARCHAR(100) = '0'
	SELECT @vcLocationID =isnull(vcLocationID,'0') from dynamicFormMaster where numFormID=18  

	-- IF USER HAS SELECTED ITS OWN DISPLAY COLUMNS THEN USE IT
	IF LEN(ISNULL(@vcDisplayColumns,'')) > 0
	BEGIN
		DECLARE @TempIDs TABLE
		(
			ID INT IDENTITY(1,1),
			vcFieldID VARCHAR(300)
		)

		INSERT INTO @TempIDs
		(
			vcFieldID
		)
		SELECT 
			OutParam 
		FROM 
			SplitString(@vcDisplayColumns,',')

		INSERT INTO @TEMPSelectedColumns
		(
			numFieldID,
			bitCustomField,
			tintOrder,
			intColumnWidth
		)
		SELECT
			numFormFieldID
			,bitCustomField
			,(SELECT (ID-1) FROM @TempIDs T3 WHERE T3.vcFieldID = TEMP.vcFieldID)
			,0
		FROM
		(
			SELECT  
				numFieldID as numFormFieldID,
				0 bitCustomField,
				CONCAT(numFieldID,'~0') vcFieldID
			FROM    
				View_DynamicDefaultColumns
			WHERE   
				numFormID = 18
				AND bitInResults = 1
				AND bitDeleted = 0 
				AND numDomainID = @numDomainID
			UNION 
			SELECT  
				c.Fld_id AS numFormFieldID
				,1 bitCustomField
				,CONCAT(Fld_id,'~1') vcFieldID
			FROM    
				CFW_Fld_Master C 
			LEFT JOIN 
				CFW_Validation V ON V.numFieldID = C.Fld_id
			JOIN 
				CFW_Loc_Master L on C.GRP_ID=L.Loc_Id
			WHERE   
				C.numDomainID = @numDomainID
				AND GRP_ID IN (SELECT ID FROM dbo.SplitIDs(@vcLocationID,','))
		) TEMP
		WHERE
			vcFieldID IN (SELECT vcFieldID FROM @TempIDs)
	END

	-- IF USER SELECTED DISPLAY COLUMNS IS NOT AVAILABLE THAN GET COLUMNS CONFIGURED FROM SEARCH RESULT GRID
	IF (SELECT COUNT(*) FROM @TEMPSelectedColumns) = 0
	BEGIN
		INSERT INTO @TEMPSelectedColumns
		(
			numFieldID,
			bitCustomField,
			tintOrder,
			intColumnWidth
		)
		SELECT
			numFormFieldID
			,bitCustomField
			,tintOrder
			,intColumnWidth
		FROM
		(
			SELECT  
				A.numFormFieldID,
				0 bitCustomField,
				A.tintOrder,
				A.intColumnWidth
			FROM    AdvSerViewConf A
					JOIN View_DynamicDefaultColumns D ON D.numFieldID = A.numFormFieldID
			WHERE   A.numFormID = 18
					AND D.bitInResults = 1
					AND D.bitDeleted = 0
					AND D.numDomainID = @numDomainID 
					AND D.numFormID = 18
					AND A.numDomainID = @numDomainID
					AND A.numUserCntID=@numUserCntID
					AND ISNULL(A.bitCustom,0) = 0
			UNION
			SELECT  
				A.numFormFieldID,
				1 bitCustomField,
				A.tintOrder,
				A.intColumnWidth
			FROM    AdvSerViewConf A
					JOIN CFW_Fld_Master C ON C.Fld_id = A.numFormFieldID
			WHERE   A.numFormID = 18
					AND C.numDomainID = @numDomainID 
					AND A.numDomainID = @numDomainID
					AND A.numUserCntID=@numUserCntID
					AND ISNULL(A.bitCustom,0) = 1
		) T1
		ORDER BY
			 tintOrder
	END

	-- IF USER HASN'T CONFIGURED COLUMNS FROM SEARCH RESULT GRID THEN RETURN DEFAULT COLUMNS
	IF (SELECT COUNT(*) FROM @TEMPSelectedColumns) = 0
	BEGIN
		INSERT INTO @TEMPSelectedColumns
		(
			numFieldID,
			bitCustomField,
			tintOrder,
			intColumnWidth
		)
		SELECT  
			numFieldID,
			0,
			1,
			0
		FROM    
			View_DynamicDefaultColumns
		WHERE   
			numFormID = 18
			AND numDomainID = @numDomainID
			AND numFieldID = 148
	END                                      


DECLARE @Table AS VARCHAR(200)
DECLARE @bitCustom BIT
DECLARE @numFieldGroupID AS INT
DECLARE @vcColumnName AS VARCHAR(200)
DECLARE @numFieldID AS NUMERIC(18,0)
DECLARE @Prefix AS VARCHAR(10)

	SELECT TOP 1
		@tintOrder=tintOrder,
		@numFieldID=numFormFieldID,
		@vcDbColumnName=vcDbColumnName,
		@vcFormFieldName=vcFieldName,
		@vcAssociatedControlType=vcAssociatedControlType,
		@vcListItemType=vcListItemType,
		@numListID=numListID,
		@Table=vcLookBackTableName,
		@bitCustom=bitCustom,
		@numFieldGroupID=numGroupID
	FROM
	(
		SELECT
			T1.tintOrder+1 tintOrder,
			D.numFieldID AS numFormFieldID,
			vcDbColumnName,
			vcFieldName,
			vcAssociatedControlType,
			vcListItemType,
			numListID,
			vcLookBackTableName,
			0 AS bitCustom,
			0 AS numGroupID
		FROM 
			View_DynamicDefaultColumns D                               
		INNER JOIN	
			@TEMPSelectedColumns T1
		ON
			D.numFieldID=T1.numFieldID
		WHERE 
			D.numDomainID = @numDomainID 
			AND D.numFormID = 18 
			AND ISNULL(T1.bitCustomField,0) = 0
		UNION
		SELECT
			T1.tintOrder+1 tintOrder,
			C.Fld_id AS numFormFieldID,
			CONCAT('Cust',Fld_id),
			Fld_label,
			c.Fld_type,
			'',
			c.numlistid,
			'',
			1 AS bitCustom,
			Grp_id
		FROM    
			CFW_Fld_Master C
		INNER JOIN	
			@TEMPSelectedColumns T1
		ON
			C.Fld_id=T1.numFieldID
		WHERE   
			C.numDomainID = @numDomainID
			AND ISNULL(T1.bitCustomField,0) = 1
	) T1
	ORDER BY 
		tintOrder asc    
                                    
while @tintOrder>0                                      
begin       

	IF @Table = 'AdditionalContactsInformation' 
        SET @Prefix = 'ADC.'
	IF @Table = 'DivisionMaster' 
        SET @Prefix = 'DM.'
    IF @Table = 'CompanyInfo' 
        SET @Prefix = 'C.'
    IF @Table = 'ProjectsMaster' 
        SET @Prefix = 'ProMas.'                               
                                 
   IF @bitCustom = 0
   BEGIN                                    
		if @vcAssociatedControlType='SelectBox'                                      
        begin                                      
                                                      
			if @vcListItemType='LI'                                       
			begin                                      
			set @strSql=@strSql+',L'+ convert(varchar(3),@tintOrder)+'.vcData'+' ['+ @vcFormFieldName+'~'+ @vcDbColumnName+']'                                      
			set @WhereCondition= @WhereCondition +' left Join ListDetails L'+ convert(varchar(3),@tintOrder)+ ' on L'+convert(varchar(3),@tintOrder)+ '.numListItemID='+ (@Prefix + @vcDbColumnName)                                      
			end 
                                     
			if @vcListItemType='U'                                       
			begin                                      
				SET @strSql=@strSql+',dbo.fn_GetContactName(' + @Prefix + @vcDbColumnName+') ['+ @vcFormFieldName+'~'+ @vcDbColumnName+']'
			end 
                                      
			end                                      
		else set @strSql=@strSql+','+ case  when @vcDbColumnName='numAge' then 'year(getutcdate())-year(bintDOB)'       
			when @vcDbColumnName='numDivisionID' then 'DM.numDivisionID'     
			when @vcDbColumnName='vcProgress' then  ' dbo.fn_ProTotalProgress(ProMas.numProId)'    
			when @vcDbColumnName='intTargetResolveDate' or @vcDbColumnName='dtDateEntered' or @vcDbColumnName='bintShippedDate'  then      
			'dbo.FormatedDateFromDate('+ @Prefix + @vcDbColumnName +','+convert(varchar(10),@numDomainId)+')'      
			 else @Prefix + @vcDbColumnName end +' ['+ @vcFormFieldName+'~'+ @vcDbColumnName+']'                   
    END
	ELSE IF @bitCustom = 1
	BEGIN
		SET @vcColumnName= CONCAT(@vcFormFieldName,'~','Cust',@numFieldId)
			
		IF @vcAssociatedControlType = 'TextBox' or @vcAssociatedControlType = 'TextArea'            
		BEGIN
			SET @strSql= @strSql+',CFW'+ convert(VARCHAR(3),@tintOrder)+'.Fld_Value  ['+ @vcColumnName +']'                   
				
			SET @WhereCondition= @WhereCondition 
								+' left Join CFW_FLD_Values_Pro CFW'+ convert(varchar(3),@tintOrder)
								+ ' on CFW' + convert(varchar(3),@tintOrder) 
								+ '.Fld_Id='+convert(varchar(10),@numFieldId) 
								+ 'and CFW' + convert(varchar(3),@tintOrder) 
								+ '.RecId=ProMas.numProID '                                                         
		END   
		ELSE IF @vcAssociatedControlType = 'CheckBox'       
		BEGIN
			set @strSql= @strSql+',case when isnull(CFW' + CONVERT(VARCHAR(3), @tintOrder)+ '.Fld_Value,0)='''' then 0 else  isnull(CFW'
											+ CONVERT(VARCHAR(3), @tintOrder)+ '.Fld_Value,0) end ['+ @vcColumnName + ']'
 
			set @WhereCondition= @WhereCondition +' left Join CFW_FLD_Values_Pro CFW'+ convert(varchar(3),@tintOrder)+ '             
			on CFW'+ convert(varchar(3),@tintOrder)+ '.Fld_Id='+convert(varchar(10),@numFieldId) +'and CFW'+ convert(varchar(3),@tintOrder)+ '.RecId=ProMas.numProID '                                                     
		END                
		ELSE IF @vcAssociatedControlType = 'DateField'            
		BEGIN
			set @strSql= @strSql+',dbo.FormatedDateFromDate(CFW'+ convert(varchar(3),@tintOrder)+'.Fld_Value,'+convert(varchar(10),@numDomainId)+')  ['+ @vcColumnName +']'                   
			set @WhereCondition= @WhereCondition +' left Join CFW_FLD_Values_Pro CFW'+ convert(varchar(3),@tintOrder)+ '                 
			on CFW'+ convert(varchar(3),@tintOrder)+ '.Fld_Id='+convert(varchar(10),@numFieldId) +'and CFW'+ convert(varchar(3),@tintOrder)+ '.RecId=ProMas.numProID '
		END                
		ELSE IF @vcAssociatedControlType = 'SelectBox'             
		BEGIN
			set @vcDbColumnName = 'DCust'+Convert(varchar(10),@numFieldId)                
			set @strSql=@strSql+',L'+ convert(varchar(3),@tintOrder)+'.vcData'+' ['+ @vcColumnName +']'                                                          
			set @WhereCondition= @WhereCondition +' left Join CFW_FLD_Values_Pro CFW'+ convert(varchar(3),@tintOrder)+ '                 
				on CFW'+ convert(varchar(3),@tintOrder)+ '.Fld_Id='+convert(varchar(10),@numFieldId) +'and CFW'+ convert(varchar(3),@tintOrder)+ '.RecId=ProMas.numProID '
			set @WhereCondition= @WhereCondition +' left Join ListDetails L'+ convert(varchar(3),@tintOrder)+ ' on L'+convert(varchar(3),@tintOrder)+ '.numListItemID=CFW'+ convert(varchar(3),@tintOrder)+'.Fld_Value'                
		END         
		ELSE IF @vcAssociatedControlType = 'CheckBoxList'
		BEGIN
			SET @strSql= @strSql+',(SELECT STUFF((SELECT CONCAT('', '', vcData) FROM ListDetails WHERE numListID= ' + CAST(@numListID AS varchar) + ' AND numlistitemid IN (SELECT ISNULL(Id,0) FROM dbo.SplitIDs(CFW'+ convert(VARCHAR(3),@tintOrder)+'.Fld_Value,'','')) FOR XML PATH('''')), 1, 1, ''''))  ['+ @vcColumnName +']'   

			SET @WhereCondition= @WhereCondition 
								+' left Join CFW_FLD_Values_Pro CFW'+ convert(varchar(3),@tintOrder)
								+ ' on CFW' + convert(varchar(3),@tintOrder) 
								+ '.Fld_Id='+convert(varchar(10),@numFieldId) 
								+ 'and CFW' + convert(varchar(3),@tintOrder) 
								+ '.RecId=ProMas.numProID '
		END
	END    
          
                
             
                
                  
                       
                                              
 SELECT TOP 1
		@tintOrder=tintOrder,
		@numFieldID=numFormFieldID,
		@vcDbColumnName=vcDbColumnName,
		@vcFormFieldName=vcFieldName,
		@vcAssociatedControlType=vcAssociatedControlType,
		@vcListItemType=vcListItemType,
		@numListID=numListID,
		@Table=vcLookBackTableName,
		@bitCustom=bitCustom,
		@numFieldGroupID=numGroupID
	FROM
	(
		SELECT
			T1.tintOrder+1 tintOrder,
			D.numFieldID AS numFormFieldID,
			vcDbColumnName,
			vcFieldName,
			vcAssociatedControlType,
			vcListItemType,
			numListID,
			vcLookBackTableName,
			0 AS bitCustom,
			0 AS numGroupID
		FROM 
			View_DynamicDefaultColumns D                               
		INNER JOIN	
			@TEMPSelectedColumns T1
		ON
			D.numFieldID=T1.numFieldID
		WHERE 
			D.numDomainID = @numDomainID 
			AND D.numFormID = 18 
			AND ISNULL(T1.bitCustomField,0) = 0
			AND T1.tintOrder > @tintOrder-1
		UNION
		SELECT
			T1.tintOrder+1 tintOrder,
			C.Fld_id AS numFormFieldID,
			CONCAT('Cust',Fld_id),
			Fld_label,
			c.Fld_type,
			'',
			c.numlistid,
			'',
			1 AS bitCustom,
			Grp_id
		FROM    
			CFW_Fld_Master C
		INNER JOIN	
			@TEMPSelectedColumns T1
		ON
			C.Fld_id=T1.numFieldID
		WHERE   
			C.numDomainID = @numDomainID
			AND ISNULL(T1.bitCustomField,0) = 1
			AND T1.tintOrder > @tintOrder-1
	) T1
	ORDER BY 
		tintOrder asc                                              

 if @@rowcount=0 set @tintOrder=0                                      
end       
  declare @firstRec as integer                                                                            
  declare @lastRec as integer                                                                            
 set @firstRec= (@CurrentPage-1) * @PageSize                              
 set @lastRec= (@CurrentPage*@PageSize+1)                                                                             
set @TotRecs=(select count(*) from #tempTable)       
set @strSql=@strSql+'from ProjectsMaster ProMas join DivisionMaster DM on Dm.numDivisionId = ProMas.numDivisionId    
JOIN CompanyInfo C ON DM.numCompanyID = C.numCompanyId
LEFT JOIN AdditionalContactsinformation ADC on ADC.numContactId = ProMas.numIntPrjMgr     
 '+@WhereCondition+'      
      
 join #tempTable T on T.numProID=ProMas.numProID'    

	IF ISNULL(@GetAll,0) = 0
	BEGIN
		SET @strSql = @strSql + ' WHERE ID > '+convert(varchar(10),@firstRec)+ ' and ID <'+ convert(varchar(10),@lastRec)   
	END

                        
 print @strSql                                                              
exec(@strSql)                                                                         
drop table #tempTable
GO
