/****** Object:  StoredProcedure [dbo].[usp_GetOppContactId]    Script Date: 07/26/2008 16:17:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
------------------------------------------
--usp_GetOppContactId

--For getting the contact id associated with the opportunity entry
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getoppcontactid')
DROP PROCEDURE usp_getoppcontactid
GO
CREATE PROCEDURE [dbo].[usp_GetOppContactId] 
	@numOppId numeric   
--
AS
	DECLARE @numContactId numeric
	
	SELECT @numContactId=numcontactId from OpportunityMaster WHERE numOppId=@numoppid
	SELECT @numcontactID
GO
