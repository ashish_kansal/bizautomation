/****** Object:  StoredProcedure [dbo].[usp_GetSurveyAnswers]    Script Date: 07/26/2008 16:18:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getsurveyanswers')
DROP PROCEDURE usp_getsurveyanswers
GO
CREATE PROCEDURE [dbo].[usp_GetSurveyAnswers]
@numDomainId numeric(9),
@SurveyId Numeric   ,
@QuestionId Numeric

as 


 SELECT numSurId,numAnsID, vcAnsLabel
  FROM SurveyAnsMaster                
  WHERE  numSurId = @SurveyId  and numQid = @QuestionId
GO
