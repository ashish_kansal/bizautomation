/****** Object:  StoredProcedure [dbo].[USP_HierarchyGridForProcumentBudget]    Script Date: 07/26/2008 16:18:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created By siva                              
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_hierarchygridforprocumentbudget')
DROP PROCEDURE usp_hierarchygridforprocumentbudget
GO
CREATE PROCEDURE [dbo].[USP_HierarchyGridForProcumentBudget]                              
(@numDomainId as numeric(9)=0,          
 @numProcurementId as numeric(9)=0,          
 @intType as int=0                           
 )                              
As                              
Begin                              
	Declare @numItemGroupId as numeric(9)                              
	Declare @vcItemGroup as varchar(150)                              
	Declare @Count as numeric(9)                 
	Declare @lstrSQL as varchar(8000)             
	Declare @lstr as varchar(8000)                 
	Declare @numMonth as tinyint               
	Set @numMonth=1                
	Set @lstrSQL=''             
	Set @lstr=''             
	Declare @Date as datetime     
	Declare @dtFiscalStDate as datetime                        
	Declare @dtFiscalEndDate as datetime         
	Set @Date = dateadd(year,@intType,getutcdate())               
              
	While @numMonth <=12                        
		 Begin                        
			  Set @dtFiscalStDate =dbo.GetFiscalStartDate(dbo.GetFiscalyear(@Date,@numDomainId),@numDomainId)                     
			  Set @dtFiscalStDate=dateadd(month,@numMonth-1,@dtFiscalStDate)                  
			  Set @dtFiscalEndDate =  dateadd(day,-1,dateadd(year,1,dbo.GetFiscalStartDate(dbo.GetFiscalyear(@Date,@numDomainId),@numDomainId)))               
			  print @dtFiscalStDate              
			  Set @lstrSQL=@lstrSQL+' '+  'dbo.fn_GetProcurementBudgetMonthDetail('+convert(varchar(10),@numProcurementId)+','+Convert(varchar(2),month(@dtFiscalStDate))+','+Convert(varchar(4),year(@dtFiscalStDate))+','+convert(varchar(10),@numDomainId)+  
			',IG.numItemGroupID)'         
			  + ' as ''' +Convert(varchar(2),month(@dtFiscalStDate))+ '~'+Convert(varchar(4),year(@dtFiscalStDate))+''','              
			  Set @numMonth=@numMonth+1                        
		End 
	Set @dtFiscalStDate =dbo.GetFiscalStartDate(dbo.GetFiscalyear(@Date,@numDomainId),@numDomainId)               
    Set @lstrSQL = @lstrSQL +
	--' dbo.fn_GetProcurementBudgetMonthTotalAmt('+Convert(varchar(10),@numProcurementId)+','''+convert(varchar(500),@Date)+''',IG.numItemGroupID,'+convert(varchar(10),@numDomainId)+') as Total' +         
	--',dbo.fn_GetProcurementBudgetComments('+Convert(varchar(10),@numProcurementId)+','+Convert(varchar(10),@numDomainId)+',IG.numItemGroupID) as  Comments'         
	' (Select Sum(PBD.monAmount)  From ProcurementBudgetMaster PBM  
	inner join ProcurementBudgetDetails PBD on PBM.numProcurementBudgetId=PBD.numProcurementId  
	Where PBM.numDomainId='+Convert(varchar(10),@numDomainId)+' And PBD.numItemGroupId=IG.numItemGroupID  And ((PBD.tintMonth between Month('''+Convert(varchar(100),@dtFiscalStDate)+''') and 12) and PBD.intYear=Year('''+Convert(varchar(100),@dtFiscalStDate)+
	''')) Or ((PBD.tintMonth between 1 and month('''+Convert(varchar(100),@dtFiscalEndDate)+''')) and PBD.intYear=Year('''+Convert(varchar(100),@dtFiscalEndDate)+''')))  as Total
    ,(Select top 1 isnull(PBD.vcComments,'''') From ProcurementBudgetMaster PBM    
	inner join ProcurementBudgetDetails PBD on PBM.numProcurementBudgetId=PBD.numProcurementId     
	Where PBM.numDomainId='+Convert(varchar(10),@numDomainId)+' And PBD.numProcurementId='+Convert(varchar(10),@numProcurementId)+' And PBD.numItemGroupId=IG.numItemGroupID'  
    +' And PBD.tintMonth=Month('''+Convert(varchar(100),@dtFiscalEndDate)+''') And PBD.intYear=Year('''+Convert(varchar(100),@dtFiscalEndDate)+'''))as  Comments'  
	print @lstrSQL                      
	Set @lstr  ='Select IG.numItemGroupID as numItemGroupId,IG.vcItemGroup as vcItemGroupName,IG.numDomainID as numDomainID,' +Convert(varchar(8000),@lstrSQL)        
            +' From ItemGroups IG Where IG.numDomainID='+Convert(varchar(10),@numDomainId)                
               
	Print(@lstr)            
	Exec (@lstr)            
End
GO
