/****** Object:  StoredProcedure [dbo].[USP_LoadWarehouseBasedOnItemsWithAttr]    Script Date: 07/26/2008 16:19:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--exec USP_LoadWarehouseAttributes 14,''    
--created by anoop jayaraj  
--exec USP_LoadWarehouseBasedOnItemsWithAttr 99
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_loadwarehousebasedonitemswithattr')
DROP PROCEDURE usp_loadwarehousebasedonitemswithattr
GO
CREATE PROCEDURE [dbo].[USP_LoadWarehouseBasedOnItemsWithAttr]    
@numItemCode as varchar(20)=''   
as    
    
declare @bitSerialize as bit  
DECLARE @bitKitParent BIT
     
select @bitSerialize=bitSerialized,@bitKitParent = ( CASE WHEN ISNULL(bitKitParent,0) = 1
                               AND ISNULL(bitAssembly,0) = 1 THEN 0
                          WHEN ISNULL(bitKitParent,0) = 1 THEN 1
                          ELSE 0
                     END ) from item where numItemCode=@numItemCode    

  select distinct(WareHouseItems.numWareHouseItemId),vcWareHouse+' --- '+dbo.USP_GetAttributes(numWareHouseItemId,@bitSerialize) as vcWareHouse,
  Case when @bitKitParent=1 then dbo.fn_GetKitInventory(@numItemCode,WareHouseItems.numWareHouseID) else isnull(numOnHand,0) END numOnHand,
Case when @bitKitParent=1 then 0 else isnull(numOnOrder,0) END  numOnOrder,
Case when @bitKitParent=1 then 0 else isnull(numReorder,0) END  numReorder,
Case when @bitKitParent=1 then 0 else isnull(numAllocation,0) END  numAllocation,
Case when @bitKitParent=1 then 0 else isnull(numBackOrder,0) END  numBackOrder
   from WareHouseItems    
   join Warehouses on Warehouses.numWareHouseID=WareHouseItems.numWareHouseID    
   where numItemID=@numItemCode
GO
