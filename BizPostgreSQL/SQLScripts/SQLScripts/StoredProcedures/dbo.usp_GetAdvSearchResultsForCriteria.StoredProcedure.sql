/****** Object:  StoredProcedure [dbo].[usp_GetAdvSearchResultsForCriteria]    Script Date: 07/26/2008 16:16:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created By: Debasish Nag                                                                
--Created On: 16th Aug 2005                                                                
--The Search Criteria is dynamic and so this procedure returns the result matching specific criteria                                                               
--Modified On: 10th Dec 2005  Performance Improvement                                                               
--Modified On: 21st Mar 2006  Include a filteration on the Primary Contacts/ All Contacts                
              
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getadvsearchresultsforcriteria')
DROP PROCEDURE usp_getadvsearchresultsforcriteria
GO
CREATE PROCEDURE [dbo].[usp_GetAdvSearchResultsForCriteria]      
  @vcColumnsList NVarchar(500),                                                    
  @vcWhereCondition NVarchar(4000),                                               
  @vcAdditionalWhereCondition NVarchar(200),                           
  @vcCustomFieldGroups NVarchar(20),                                                 
  @vcCustomFieldSearchKeyword NVarchar(100),                                  
  @vcSortColumn NVarchar(50),                                                   
  @vcSortOrder NVarchar(50),                           
  @tIntSearchContext tinyInt,                   
  @bitOnlyPrimaryContacts bit,                                               
  @vcRatingInSurvey NVarchar(75),                           
  @tIntEmailLink tinyInt,                                              
  @numNosRowsReturned Int,                                                 
  @numCurrentPage Int,                                               
  @numUserCntId Numeric,                                               
  @numDomainId Numeric                                                 
AS                                                                                      
BEGIN                                            
  --SET @vcWhereCondition = Replace(@vcWhereCondition,'''', char(39))                    
  --EXEC ('usp_CreateTempTableForAdvSearch')                                                           
  DECLARE @vcNewColumnsList NVarchar(2000)                                  
  IF @numCurrentPage <> -9652                                  
  BEGIN                                  
      SET @vcNewColumnsList = REPLACE(@vcColumnsList, 'vcCompanyName', '''<a href="javascript: GoOrgDetails('' + Cast(numDivisionId AS NVarchar) + '');">'' + vcCompanyName + ''</a>'' AS vcCompanyName')                       
                     
      IF @tIntEmailLink = 1      
       SET @vcNewColumnsList = REPLACE(@vcNewColumnsList, 'vcEmail', '''<a href="mailto:'' + vcEmail + ''">'' + vcEmail + ''</a>'' AS vcEmail')                                  
      ELSE      
       SET @vcNewColumnsList = REPLACE(@vcNewColumnsList, 'vcEmail', '''<a href="../common/callemail.aspx?Lsemail='' + vcEmail + ''&ContID='' + Cast(numContactId AS NVarchar) + ''" target=_blank>'' + vcEmail + ''</a>'' AS vcEmail')                        
  
    
          
      SET @vcNewColumnsList = REPLACE(@vcNewColumnsList, 'vcFirstName', '''<a href="javascript: GoContactDetails('' + Cast(numContactId AS NVarchar) + '');">'' + vcFirstName + ''</a>'' AS vcFirstName')                                  
      SET @vcNewColumnsList = REPLACE(@vcNewColumnsList, 'vcLastName', '''<a href="javascript: GoContactDetails('' + Cast(numContactId AS NVarchar) + '');">'' + vcLastName + ''</a>'' AS vcLastName')                                  
      SET @vcNewColumnsList = @vcNewColumnsList + ',''<input Type=CheckBox name=cbSR'' + Cast(numCompanyID AS NVarchar) + '' value="'' + Cast(numCompanyID AS NVarchar) + ''_'' + Cast(numContactID AS NVarchar) + ''_'' +                         
                                                  Cast(numDivisionID AS NVarchar) + ''">'' AS ''cbBox'''                                  
  END                                  
  ELSE                                  
      SET @vcNewColumnsList = @vcColumnsList + ','''' AS cbBox'                                  
                   
  --PRINT @vcNewColumnsList                                  
  DECLARE @sqlString Varchar(8000)                                                          
  DECLARE @sqlStringSelect Varchar(8000)               
  DECLARE @sqlStringCustomFieldSelect Varchar(8000)                          
  DECLARE @sqlStringDropTable Varchar(100)                                                    
  DECLARE @sqlCombinedStringSelect Varchar(8000)                                                           
  DECLARE @sqlCountString Varchar(8000)                                                      
  DECLARE @sqlStringCoalesce Varchar(8000)                                 
  DECLARE @sqlFilterPrimaryString NVarchar(100)         
  DECLARE @sqlRatingInSurvey NVarchar(400)        
  DECLARE @intTopOfResultSet Int                                              
  SET @intTopOfResultSet = @numNosRowsReturned * @numCurrentPage                                                          
  DECLARE @intStartOfResultSet Int                                              
  SET @intStartOfResultSet = @numNosRowsReturned * (@numCurrentPage - 1)                  
                
  IF @bitOnlyPrimaryContacts = 1                
    SET @sqlFilterPrimaryString = ' AND numContactType = 70 '                
  ELSE                     
    SET @sqlFilterPrimaryString = ''                    
  IF @vcRatingInSurvey <> ''        
    BEGIN        
       DECLARE @vcStartSurveyRating NVarchar(10)        
       DECLARE @vcEndSurveyRating NVarchar(10)        
       DECLARE @vcSurveyIds NVarchar(50)        
       SET @vcSurveyIds = PARSENAME(@vcRatingInSurvey, 1)        
       SET @vcEndSurveyRating = PARSENAME(@vcRatingInSurvey, 2)        
       SET @vcStartSurveyRating = PARSENAME(@vcRatingInSurvey, 3)        
       SET @sqlRatingInSurvey = ' AND EXISTS(SELECT numRegisteredRespondentContactId FROM SurveyRespondentsMaster ' + CASE WHEN @vcSurveyIds = '*' THEN '' ELSE ' WHERE numSurID IN (' + @vcSurveyIds + ')' END  + ' GROUP BY numRegisteredRespondentContactId 
  
   
       
       
                                          HAVING SUM(numSurRating) >= ' + @vcStartSurveyRating + ' AND SUM(numSurRating) <=  ' + @vcEndSurveyRating +        
                                          ' AND numRegisteredRespondentContactId = numContactId        
                                         )'         
    END        
  ELSE        
    SET @sqlRatingInSurvey = ''               
                          
  IF @vcCustomFieldGroups <> ''                          
  BEGIN                          
       DECLARE @vcCustomColumnsSelectQuery NVarchar(1500)                          
       DECLARE @vcCustomColumnsWhereCondition NVarchar(1000)                          
       DECLARE @vcCustomColumnsSalesPurchaseOppWhereCondition NVarchar(100)                          
       SELECT @vcCustomColumnsSelectQuery=COALESCE(@vcCustomColumnsSelectQuery + ' UNION ', '') +                           
                'SELECT ' + CASE Items When 1 Then '''D''' When 4 THEN '''C''' ELSE '''O''' END + ' AS cType, M.Fld_ID, V.RecId,                          
                Case WHEN (M.Fld_type = ''CheckBox'' AND V.Fld_Value = 1) THEN M.Fld_Label                        
                WHEN (M.Fld_type = ''SelectBox'') THEN dbo.fn_AdvSearchColumnName(V.Fld_Value,''L'')                        
                ELSE V.Fld_Value                        
                END As Fld_Value FROM CFW_Fld_Master M,' +  CASE Items When 1 Then 'CFW_FLD_Values' When 4 THEN 'CFW_FLD_Values_Cont' ELSE 'CFW_FLD_Values_Opp' END + ' V ' +                        
                'WHERE M.Fld_id = V.Fld_ID'                        
                FROM dbo.Split(@vcCustomFieldGroups,',')                         
                          
       SELECT @vcCustomColumnsWhereCondition=COALESCE(@vcCustomColumnsWhereCondition + ' OR ', '') +                           
             '(Fld_Value LIKE ' + Case WHEN @tIntSearchContext = 1 THEN '''%' ELSE '''' END + @vcCustomFieldSearchKeyword + '%''' +                           
             ' AND cType = ' + CASE Items When 1 Then '''D''' When 4 THEN '''C''' ELSE '''O''' END +        
             ' AND RecId = ' + CASE Items When 1 Then 'numDivisionId' When 4 THEN 'numContactId' ELSE 'numOppId' END + ')'                          
             FROM dbo.Split(@vcCustomFieldGroups,',')                                                
       SET @vcCustomColumnsSalesPurchaseOppWhereCondition = CASE WHEN (CHARINDEX(',2,',',' + @vcCustomFieldGroups + ',') > 0 AND CHARINDEX(',6,',',' + @vcCustomFieldGroups + ',') > 0) THEN ''                           
                                                                 WHEN (CHARINDEX(',2,',',' + @vcCustomFieldGroups + ',') > 0 AND CHARINDEX(',6,',',' + @vcCustomFieldGroups + ',') = 0)THEN ' AND tIntOppType = 1 '                          
                                            WHEN (CHARINDEX(',2,',',' + @vcCustomFieldGroups + ',') = 0 AND CHARINDEX(',6,',',' + @vcCustomFieldGroups + ',') > 0)THEN ' AND tIntOppType = 2 '                          
                                                                 ELSE ''                          
                                                            END                          
                          
  END                          
                          
  IF @vcCustomFieldGroups = ''  --No Custom Fields Involved in the Search                          
     SET @sqlString =  'SELECT TOP 100 PERCENT ' + @vcColumnsList + '                                                                 
     FROM dbo.DynamicAdvSearchView AS TableOne WHERE ' + @vcWhereCondition + @vcAdditionalWhereCondition + @sqlFilterPrimaryString + @sqlRatingInSurvey + '  
     AND numDomainId = ' + Cast(@numDomainId As NVarchar) + '   
     GROUP BY ' + @vcColumnsList + '                                                     
     ORDER BY ' + @vcSortColumn + ' ' + @vcSortOrder                                              
  ELSE  --Custom Fields Involved in the Search                          
     SET @sqlString =  'SELECT TOP 100 PERCENT ' + @vcColumnsList + '                                                                 
     FROM dbo.DynamicAdvSearchView AS TableOne, (' + @vcCustomColumnsSelectQuery + ') TableCustomOne WHERE ' + @vcWhereCondition + @vcAdditionalWhereCondition + @sqlFilterPrimaryString + @sqlRatingInSurvey + '    
     AND (' + @vcCustomColumnsWhereCondition + ') ' +   
     @vcCustomColumnsSalesPurchaseOppWhereCondition + '   
     AND numDomainId = ' + Cast(@numDomainId As NVarchar) + '                                                        
     GROUP BY ' + @vcColumnsList + '                                                     
     ORDER BY ' + @vcSortColumn + ' ' + @vcSortOrder                                              
                          
                                              
  SET @sqlString = 'Select IDENTITY(int) AS numRow, ' + @vcColumnsList + ' INTO #tmpTableX FROM (' + @sqlString + ') AS tmpTable1'                                            
  IF @numCurrentPage <> -9652                                      
      SET @sqlStringSelect = 'SELECT ' + @vcNewColumnsList + ' FROM #tmpTableX WHERE numRow > ' + CAST(@intStartOfResultSet AS NVarchar(9)) +  ' AND numRow <= ' + CAST(@intTopOfResultSet AS NVarchar(9))                                            
  ELSE                                      
      SET @sqlStringSelect = 'SELECT TOP 100 PERCENT ' + @vcNewColumnsList + ' FROM #tmpTableX'                                       
                                      
                                            
  SET @sqlStringCoalesce = 'SELECT TOP 100 PERCENT Cast(numCompanyID AS NVarchar) + ''_'' + Cast(numContactID AS NVarchar) + ''_'' + Cast(numDivisionID AS NVarchar) AS ''vcCoalesce'' FROM #tmpTableX'                        
  SET @sqlCountString = 'SELECT IsNull(Max(numRow),0) AS RowCountRecords FROM #tmpTableX'                              
                                  
  SET @sqlStringDropTable = 'DROP TABLE #tmpTableX'                                            
                                          
  SET @sqlCombinedStringSelect = @sqlString +                                            
                    char(10) +                                            
                                       @sqlStringSelect +                                             
                                          char(10) +                                           
    @sqlCountString +                                             
                                               char(10) +                                           
                                                  @sqlStringCoalesce +                                             
                                                     char(10) +                                          
                                                        @sqlStringDropTable                                            
                                            
  EXECUTE (@sqlCombinedStringSelect)                                            
  PRINT   (@sqlCombinedStringSelect)                                            
END
GO
