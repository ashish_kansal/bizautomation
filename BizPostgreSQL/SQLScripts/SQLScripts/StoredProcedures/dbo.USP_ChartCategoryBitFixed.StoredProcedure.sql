/****** Object:  StoredProcedure [dbo].[USP_ChartCategoryBitFixed]    Script Date: 07/26/2008 16:15:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created By Siva  
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_chartcategorybitfixed')
DROP PROCEDURE usp_chartcategorybitfixed
GO
CREATE PROCEDURE [dbo].[USP_ChartCategoryBitFixed]  
@numAccountId as numeric(9)=0  
As  
Begin  
Select bitFixed from Chart_Of_Accounts Where numAccountId=@numAccountId  
End
GO
