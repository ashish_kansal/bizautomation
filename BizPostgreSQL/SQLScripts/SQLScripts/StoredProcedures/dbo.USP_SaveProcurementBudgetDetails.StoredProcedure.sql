/****** Object:  StoredProcedure [dbo].[USP_SaveProcurementBudgetDetails]    Script Date: 07/26/2008 16:21:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created By Siva                            
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_saveprocurementbudgetdetails')
DROP PROCEDURE usp_saveprocurementbudgetdetails
GO
CREATE PROCEDURE [dbo].[USP_SaveProcurementBudgetDetails]                            
@numDomainId as numeric(9)=0,                 
@numProcurementId as numeric(9)=0,                
@strRow as text='',          
@intYear as int=0,        
@intType as int=0,      
@sintByte as tinyint=0,    
@numItemGroupId as numeric(9)=0                       
As                            
Begin                            
                      
 Declare @hDoc3 int                                                                                                                                                                                            
 EXEC sp_xml_preparedocument @hDoc3 OUTPUT, @strRow                
 Declare @Date as datetime                    
 Set @Date = dateadd(year,@intType,getutcdate())               
 Declare @dtFiscalStDate as datetime                              
 Declare @dtFiscalEndDate as datetime                         
 Set @dtFiscalStDate =dbo.GetFiscalStartDate(dbo.GetFiscalyear(@Date,@numDomainId),@numDomainId)                           
 Set @dtFiscalEndDate =  dateadd(day,-1,dateadd(year,1,dbo.GetFiscalStartDate(dbo.GetFiscalyear(@Date,@numDomainId),@numDomainId)))                     
        
 if @sintByte=0      
 Begin       
            
   --To Delete the Records          
   Delete PBD  from ProcurementBudgetMaster PBM
    inner join ProcurementBudgetDetails PBD on PBM.numProcurementBudgetId=PBD.numProcurementId
   Where  PBD.numProcurementId=@numProcurementId And ((PBD.tintMonth between Month(@dtFiscalStDate) and 12) and PBD.intYear=Year(@dtFiscalStDate)) Or ((PBD.tintMonth between 1 and month(@dtFiscalEndDate) ) and PBD.intYear=Year(@dtFiscalEndDate))            
            
                                        
   Insert Into ProcurementBudgetDetails(numProcurementId,numItemGroupId,tintMonth,intYear,monAmount,vcComments)                                                                                                               
   Select * from (                                                                                                                                         
   Select * from OPenXml (@hdoc3,'/NewDataSet/Table1',2)                                                                                                                                          
   With(                                     
    numProcurementId numeric(9),                          
    numItemGroupId numeric(9),                       
    tintMonth tinyint,                            
    intYear integer,                            
    monAmount DECIMAL(20,5),            
    Comments varchar(100)          
   ))X                          
    End      
 Else      
  Begin      
    --To Delete Record            
    Delete PBD  from ProcurementBudgetMaster PBM
    inner join ProcurementBudgetDetails PBD on PBM.numProcurementBudgetId=PBD.numProcurementId Where PBD.numProcurementId=@numProcurementId And PBD.numItemGroupId=@numItemGroupId     
    And (((PBD.tintMonth between month(getutcdate()) and 12) and PBD.intYear=year(@Date)) Or ((PBD.tintMonth between 1 and 12) and PBD.intYear=Year(@date)+1))  
                                                 
   Insert Into ProcurementBudgetDetails(numProcurementId,numItemGroupId,tintMonth,intYear,monAmount,vcComments)                                                                                                               
    Select * from (                                                                                                                                         
    Select * from OPenXml (@hdoc3,'/NewDataSet/Table1',2)                                                                                                                                          
    With(                                     
     numProcurementId numeric(9),                          
     numItemGroupId numeric(9),                       
     tintMonth tinyint,                            
     intYear integer,                            
     monAmount DECIMAL(20,5),            
     Comments varchar(100)          
    ))X                              
  End                
             
               
End
GO
