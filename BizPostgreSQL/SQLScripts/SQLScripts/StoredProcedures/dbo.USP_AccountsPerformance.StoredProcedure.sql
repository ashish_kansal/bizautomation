/****** Object:  StoredProcedure [dbo].[USP_AccountsPerformance]    Script Date: 07/26/2008 16:14:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj  
--EXEC USP_AccountsPerformance 16008
--SELECT * FROM [CompanyInfo] INNER JOIN [DivisionMaster] ON [CompanyInfo].[numCompanyId] = [DivisionMaster].[numCompanyID] WHERE [vcCompanyName] = 'ABC inc'

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_accountsperformance')
DROP PROCEDURE usp_accountsperformance
GO
CREATE PROCEDURE [dbo].[USP_AccountsPerformance]    
@numdivisionid as numeric(9)
--@tintOppType as tinyint    
as    
declare @year as varchar(5)    
declare @Month as varchar(5)   
declare @day as varchar(5)  
   
set @year=year(getutcdate())     
set @Month=month(getutcdate())    
if @Month<10 set @Month='0'+@Month    

declare @startDate as varchar(10)    
declare @EndDate as varchar(10)    
declare @count as tinyint    
DECLARE @SalesAmount FLOAT
DECLARE @PurchaseAmount FLOAT

set @count=1   

CREATE TABLE #AccountingPerformance(
vcMonth VARCHAR(15),
SalesAmount float,
PurchaseAmount float  )



-- Do some stuff with the table



print @count
while  @count <> 13
begin   

set @day=(SELECT CASE WHEN @Month = '01'  
            THEN 31  
            WHEN @Month = '02'  
            THEN CASE WHEN (@year % 4 = 0 AND @year % 100 <> 0) OR  
                           @year % 400 = 0  
                      THEN 29  
                      ELSE 28  
            END  
            WHEN @Month = '03'  
            THEN 31  
            WHEN @Month = '04'  
            THEN 30  
            WHEN @Month = '05'  
            THEN 31  
            WHEN @Month = '06'  
            THEN 30  
            WHEN @Month = '07'  
            THEN 31  
            WHEN @Month = '08'  
            THEN 31  
            WHEN @Month = '09'  
            THEN 30  
            WHEN @Month = '10'  
            THEN 31  
            WHEN @Month = '11'  
            THEN 30  
            WHEN @Month = '12'  
            THEN 31 END )  
  
set @startDate=@Month+'/01/'+@year   
set @EndDate=@Month+'/'+ @day +'/'+@year 
print @startDate
print @EndDate 

print @count

	select @SalesAmount = isnull(sum(monPAmount),0) from OpportunityMaster Opp Inner Join OpportunityBizDocs OppBizDocs On OppBizDocs.numOppId=Opp.numOppId    
	   WHERE  numdivisionid=@numdivisionid and tintOppType=1 and /*tintOppStatus=1 and */convert(datetime, convert(char, OppBizDocs.dtCreatedDate, 106)) >= @startDate and convert(datetime, convert(char, OppBizDocs.dtCreatedDate, 106)) <= @EndDate
	   AND ( OppBizDocs.numBizDocId IN (SELECT    numAuthoritativePurchase FROM      AuthoritativeBizDocs WHERE     numDomainId = Opp.[numDomainId] ) OR OppBizDocs.numBizDocId IN ( SELECT    numAuthoritativeSales FROM      AuthoritativeBizDocs WHERE     numDomainId = Opp.[numDomainId] ) )
	select @PurchaseAmount = isnull(sum(monPAmount),0) from OpportunityMaster Opp Inner Join OpportunityBizDocs OppBizDocs On OppBizDocs.numOppId=Opp.numOppId
		where numdivisionid=@numdivisionid and tintOppType=2 and /*tintOppStatus=1 and */convert(datetime, convert(char, OppBizDocs.dtCreatedDate, 106)) >= @startDate and convert(datetime, convert(char, OppBizDocs.dtCreatedDate, 106)) <= @EndDate
		AND ( OppBizDocs.numBizDocId IN (SELECT    numAuthoritativePurchase FROM      AuthoritativeBizDocs WHERE     numDomainId = Opp.[numDomainId] ) OR OppBizDocs.numBizDocId IN ( SELECT    numAuthoritativeSales FROM      AuthoritativeBizDocs WHERE     numDomainId = Opp.[numDomainId] ) )
	
	INSERT INTO #AccountingPerformance (vcMonth, SalesAmount,PurchaseAmount)
	SELECT 	datename(month,dateadd(month, @Month - 1, 0)),@SalesAmount,@PurchaseAmount
	

   set @Month=@Month-1    
   if @Month<10 set @Month='0'+@Month    
   if @Month='00' set @year=@year-1    
   if @Month='00' set @Month=12 
     
   set @count=@count+1    
end    
    
SELECT SUBSTRING([vcMonth],0,4) AS vcMonth,SalesAmount AS Invoice,PurchaseAmount AS [Purchase Orders]
FROM [#AccountingPerformance]
drop table [#AccountingPerformance]
