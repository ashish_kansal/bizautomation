/****** Object:  StoredProcedure [dbo].[usp_SaveWeblinkLabels]    Script Date: 07/26/2008 16:21:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- Modified By Anoop Jayaraj  
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_saveweblinklabels')
DROP PROCEDURE usp_saveweblinklabels
GO
CREATE PROCEDURE [dbo].[usp_SaveWeblinkLabels]      
 @numDivisionID numeric=0,    
 @vcWebLable1 varchar(100),    
 @vcWebLable2 varchar(100),    
 @vcWebLable3 varchar(100),    
 @vcWebLable4 varchar(100)        
AS    
   
   
  UPDATE CompanyInfo    
   SET vcWebLabel1=@vcWebLable1, vcWebLabel2=@vcWebLable2,     
   vcWebLabel3=@vcWebLable3, vcWebLabel4=@vcWebLable4    
   WHERE numCompanyID=(select numCompanyID from divisionmaster where numDivisionID=@numDivisionID)
GO
