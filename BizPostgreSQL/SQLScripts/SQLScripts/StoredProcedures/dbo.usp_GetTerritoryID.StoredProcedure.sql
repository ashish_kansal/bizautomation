/****** Object:  StoredProcedure [dbo].[usp_GetTerritoryID]    Script Date: 07/26/2008 16:18:41 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getterritoryid')
DROP PROCEDURE usp_getterritoryid
GO
CREATE PROCEDURE [dbo].[usp_GetTerritoryID]
	@vcTerrName varchar(50)='',
	@numCreatedby numeric(9)=0,
	@numDomainId numeric(9)=0,
	@bintCreatedDate bigint=0   
--
AS	
	DECLARE @numTerId numeric
	
	IF @vcterrName <> ''
		BEGIN
			SELECT @numTerId=  numTerId from territoryMaster 
			WHERE  upper(VcTerName) = upper(@vcTerrName)
			
			IF @numTerID is null  
				BEGIN
					INSERT into territorymaster(vcTerName,numCreatedBy,bintCreatedDate,numDomainId)
					values(@vcTerrname,@numCreatedby,@bintCreatedDate,@numDomainid)
					SELECT SCOPE_IDENTITY()
				END
			ELSE
				BEGIN	
					SELECT @numTerId
				END
		END
	ELSE
		BEGIN
			SELECT 0
		END
GO
