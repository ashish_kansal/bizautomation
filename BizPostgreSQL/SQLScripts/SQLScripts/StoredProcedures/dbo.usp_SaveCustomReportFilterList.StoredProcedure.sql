/****** Object:  StoredProcedure [dbo].[usp_SaveCustomReportFilterList]    Script Date: 07/26/2008 16:21:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_savecustomreportfilterlist')
DROP PROCEDURE usp_savecustomreportfilterlist
GO
CREATE PROCEDURE [dbo].[usp_SaveCustomReportFilterList]    
@ReportId as numeric(9)=0,    
@strFilterList as text=''    
as    
 delete CustReportFilterlist where numCustomReportId =@ReportId    
 declare @hDoc  int                  
    
   EXEC sp_xml_preparedocument @hDoc OUTPUT, @strFilterList        
                     
    insert into CustReportFilterlist                  
     (numCustomReportId,vcFieldsText,vcFieldsValue,vcFieldsOperator,vcFilterValue,vcFilterValued)               
                      
    select @ReportId,    
     X.*   from(SELECT * FROM OPENXML (@hDoc,'/NewDataSet/Table',2)                  
    WITH  (                  
 vcFilterFieldsText varchar(200),      
    vcFilterFieldsValue varchar(200),  
 vcFilterFieldsOperator varchar(200),  
 vcFilterValue varchar(200)  ,
vcFilterValued varchar(200) 
 ))X     
    EXEC sp_xml_removedocument @hDoc
GO
