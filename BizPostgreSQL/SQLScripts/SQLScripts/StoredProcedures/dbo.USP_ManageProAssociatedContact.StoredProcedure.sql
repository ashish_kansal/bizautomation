/****** Object:  StoredProcedure [dbo].[USP_ManageProAssociatedContact]    Script Date: 07/26/2008 16:19:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_manageproassociatedcontact')
DROP PROCEDURE usp_manageproassociatedcontact
GO
CREATE PROCEDURE [dbo].[USP_ManageProAssociatedContact]
@numProId as numeric(9)=0,
@numDomainID as numeric(9)=0,
@numContactID as numeric(9)=0,
@numRole as numeric(9)=0,
@bitPartner as bit,
@byteMode as tinyint,
@bitSubscribedEmailAlert AS bit
as
if  @byteMode=0
begin
 insert into ProjectsContacts(numProId, numContactId, numRole, bitPartner,bitSubscribedEmailAlert)
 values (@numProId,@numContactID,@numRole,@bitPartner,@bitSubscribedEmailAlert)
end
else if @byteMode=1
begin 
	delete from ProjectsContacts where numProId=@numProId and numContactId=@numContactID
end






SELECT  a.numContactId,vcCompanyName+', '+isnull(Lst.vcData,'-') as Company,     
  a.vcFirstname +' '+ a.vcLastName as [Name],      
  a.numPhone +', '+ a.numPhoneExtension as Phone,a.vcEmail as Email,      
  b.vcData as ContactRole,      
  pro.numRole as ContRoleId,     
  convert(integer,isnull(bitPartner,0)) as bitPartner,     
  a.numcontacttype,CASE WHEN pro.bitSubscribedEmailAlert=1 THEN 'Yes' ELSE 'No' END AS SubscribedEmailAlert FROM ProjectsContacts pro    
  join additionalContactsinformation a on      
  a.numContactId=pro.numContactId    
  join DivisionMaster D    
  on a.numDivisionID =D.numDivisionID    
  join CompanyInfo C    
  on D.numCompanyID=C.numCompanyID     
  left join listdetails b       
  on b.numlistitemid=pro.numRole    
  left join listdetails Lst    
  on Lst.numlistitemid=C.numCompanyType    
  WHERE pro.numProId=@numProId
GO
