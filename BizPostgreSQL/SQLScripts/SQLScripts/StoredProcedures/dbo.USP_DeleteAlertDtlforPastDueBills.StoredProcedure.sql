/****** Object:  StoredProcedure [dbo].[USP_DeleteAlertDtlforPastDueBills]    Script Date: 07/26/2008 16:15:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created By Anoop Jayaraj  
  
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_deletealertdtlforpastduebills')
DROP PROCEDURE usp_deletealertdtlforpastduebills
GO
CREATE PROCEDURE [dbo].[USP_DeleteAlertDtlforPastDueBills]  
@numAlertDTLid as numeric(9)=0  
as  
  
delete from AlertDTL where  numAlertDTLid=@numAlertDTLid
GO
