/****** Object:  StoredProcedure [dbo].[USP_GetSoltionForCases]    Script Date: 07/26/2008 16:18:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getsoltionforcases')
DROP PROCEDURE usp_getsoltionforcases
GO
CREATE PROCEDURE [dbo].[USP_GetSoltionForCases]
@numCaseId as numeric(9)
as
select S.[numSolnID],vcSolnTitle as [Solution Name],ISNULL(txtSolution,'') as [Solution Descrition],SUBSTRING(ISNULL(txtSolution,''),0,100) as [ShortDesc] from CaseSolutions C
join SolutionMaster S
on S.numSolnID=C.numSolnID
where C.numCaseId=@numCaseId
GO
