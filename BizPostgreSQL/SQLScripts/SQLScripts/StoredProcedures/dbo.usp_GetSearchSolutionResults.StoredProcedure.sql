/****** Object:  StoredProcedure [dbo].[usp_GetSearchSolutionResults]    Script Date: 07/26/2008 16:18:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getsearchsolutionresults')
DROP PROCEDURE usp_getsearchsolutionresults
GO
CREATE PROCEDURE [dbo].[usp_GetSearchSolutionResults]  
 @vcSearchText varchar(100)='',  
 @numSolnID numeric=0  
  --

AS  
-- Created : 19 April 2004  
-- This Procedure will take the search query as string and return a recordset having matching Title of text  
BEGIN  
 IF @numSolnID=0  
  SELECT * FROM SolutionMaster   
   WHERE vcSolnTitle LIKE '%' + @vcSearchText + '%'  
   OR txtSolution LIKE '%' + @vcSearchText + '%'  
 ELSE  
  SELECT * FROM SolutionMaster   
   WHERE numSolnID= @numSolnID  
END
GO
