/****** Object:  StoredProcedure [dbo].[usp_GetSalesVsQuota]    Script Date: 07/26/2008 16:18:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj        
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getsalesvsquota')
DROP PROCEDURE usp_getsalesvsquota
GO
CREATE PROCEDURE [dbo].[usp_GetSalesVsQuota]          
 @numDomainID Numeric,          
 @intYear integer,          
 @intFromMonth integer,          
 @intToMonth integer,          
 @dtDateFrom datetime,          
 @dtDateTo datetime,          
 @numTerID numeric=0,          
 @numUserCntID numeric=0,          
 @tintRights tinyint=3,          
 @intType numeric =0             
--          
AS          
BEGIN          
 DECLARE @Quota DECIMAL(20,5)          
 DECLARE @DealsWon DECIMAL(20,5)          
 DECLARE @Pipeline DECIMAL(20,5)          
 DECLARE @SUM DECIMAL(20,5)          
 DECLARE @QuotaPer float          
 DECLARE @DealsWonPer float          
 DECLARE @PipelinePer float          
           
 SET @Quota = 0          
 SET @DealsWon = 0          
 SET @Pipeline = 0          
 SET @SUM = 0          
 SET @QuotaPer = 0          
 SET @DealsWonPer = 0          
 SET @PipelinePer = 0          
          
         
    
 IF @intType = 0          
   BEGIN  
   
		 --Quota          
	 SELECT @Quota=SUM(monQuota)          
	  FROM Forecast          
	  WHERE (sintYear>=@intYear AND sintYear<=@intYear)          
	   AND (tintMonth>=@intFromMonth AND tintMonth<=@intToMonth) and numCreatedby=@numUserCntID  
   end
 else
 begin
	 --Quota          
	 SELECT @Quota=SUM(monQuota)          
	  FROM Forecast
	  join AdditionalContactsInformation
          on numContactID=Forecast.numCreatedby          
	  WHERE (sintYear>=@intYear AND sintYear<=@intYear)          
	   AND (tintMonth>=@intFromMonth AND tintMonth<=@intToMonth) 
	 AND numTeam is not null             
    	AND numTeam in(select F.numTeam from ForReportsByTeam F             
    	where F.numuserCntId=@numUserCntID and F.numDomainid=@numDomainId and F.tintType=@intType)

 end

 
       
          
 IF @tintRights=3 --All Records          
 BEGIN          
   IF @intType = 0          
   BEGIN          
  --Deals Won          
  SELECT @DealsWon=SUM(monPAmount)          
   FROM OpportunityMaster           
   WHERE tintOppStatus=1   and tintOpptype=1       
    AND bintAccountClosingDate BETWEEN @dtDateFrom AND @dtDateTo          
    AND numDomainID=@numDomainID          
              
            
  --Deals in Pipeline          
  SELECT @Pipeline=SUM(monPAmount)          
   FROM OpportunityMaster           
   WHERE tintOppStatus=0 and tintOpptype=1          
    AND intPEstimatedCloseDate BETWEEN @dtDateFrom AND @dtDateTo          
    AND numDomainID=@numDomainID          
   END          
   ELSE          
   BEGIN          
  --Deals Won          
  SELECT @DealsWon=SUM(OM.monPAmount)          
   FROM AdditionalContactsInformation ACI INNER JOIN OpportunityMaster OM ON ACI.numContactID = OM.numRecOwner        
   WHERE tintOppStatus=1   and tintOpptype=1         
    AND OM.bintAccountClosingDate BETWEEN @dtDateFrom AND @dtDateTo          
    AND OM.numDomainID=@numDomainID                 
    AND ACI.numTeam is not null             
    AND ACI.numTeam in(select F.numTeam from ForReportsByTeam F             
    where F.numuserCntId=@numUserCntID and F.numDomainid=@numDomainId and F.tintType=@intType)            
          
              
            
  --Deals in Pipeline          
  SELECT @Pipeline=SUM(OM.monPAmount)          
   FROM AdditionalContactsInformation ACI INNER JOIN OpportunityMaster OM ON ACI.numContactID = OM.numRecOwner  
   WHERE OM.tintOppStatus=0     and tintOpptype=1       
    AND OM.intPEstimatedCloseDate BETWEEN @dtDateFrom AND @dtDateTo          
    AND OM.numDomainID=@numDomainID                  
    AND ACI.numTeam is not null             
    AND ACI.numTeam in(select F.numTeam from ForReportsByTeam F             
    where F.numuserCntId=@numUserCntID and F.numDomainid=@numDomainId and F.tintType=@intType)            
          
          
   END          
 END          
          
 IF @tintRights=2 --Territory Records          
 BEGIN          
   IF @intType = 0          
   BEGIN          
  --Deals Won          
  SELECT @DealsWon=SUM(OM.monPAmount)          
   FROM OpportunityMaster OM INNER JOIN DivisionMaster DM ON OM.numDivisionId = DM.numDivisionID
   WHERE OM.tintOppStatus=1  and tintOpptype=1          
    AND OM.bintAccountClosingDate BETWEEN @dtDateFrom AND @dtDateTo          
    AND OM.numDomainID=@numDomainID                 
    AND DM.numTerID in (SELECT numTerritoryID FROM UserTerritory WHERE numUserCntID=@numUserCntID)          
            
  --Deals in Pipeline          
  SELECT @Pipeline=SUM(OM.monPAmount)          
   FROM OpportunityMaster OM INNER JOIN DivisionMaster DM ON OM.numDivisionId = DM.numDivisionID          
   WHERE OM.tintOppStatus=0   and tintOpptype=1         
    AND OM.intPEstimatedCloseDate BETWEEN @dtDateFrom AND @dtDateTo          
    AND OM.numDomainID=@numDomainID              
    AND DM.numTerID in (SELECT numTerritoryID FROM UserTerritory WHERE numUserCntID=@numUserCntID)          
          
   END          
   ELSE          
   BEGIN          
  --Deals Won          
SELECT @DealsWon=SUM(OM.monPAmount)          
   FROM AdditionalContactsInformation ACI
   INNER JOIN OpportunityMaster OM ON ACI.numContactID = OM.numRecOwner INNER JOIN DivisionMaster DM ON DM.numDivisionID=OM.numDivisionID
   WHERE OM.tintOppStatus=1 and tintOpptype=1           
    AND OM.bintAccountClosingDate BETWEEN @dtDateFrom AND @dtDateTo          
    AND OM.numDomainID=@numDomainID               
    AND DM.numTerID in (SELECT numTerritoryID FROM UserTerritory WHERE numUserCntID=@numUserCntID)            
    AND ACI.numTeam is not null             
    AND ACI.numTeam in(select F.numTeam from ForReportsByTeam F             
    where F.numuserCntId=@numUserCntID and F.numDomainid=@numDomainId and F.tintType=@intType)            
          
            
  --Deals in Pipeline          
  SELECT @Pipeline=SUM(OM.monPAmount)          
   FROM AdditionalContactsInformation ACI       
        INNER JOIN OpportunityMaster OM ON ACI.numContactID = OM.numRecOwner INNER JOIN DivisionMaster DM ON DM.numDivisionID=OM.numDivisionID          
   WHERE OM.tintOppStatus=0 and tintOpptype=1           
    AND OM.intPEstimatedCloseDate BETWEEN @dtDateFrom AND @dtDateTo          
    AND OM.numDomainID=@numDomainID          
    AND DM.numTerID in (SELECT numTerritoryID FROM UserTerritory WHERE numUserCntID=@numUserCntID)         
    AND ACI.numTeam is not null             
    AND ACI.numTeam in(select F.numTeam from ForReportsByTeam F             
    where F.numuserCntId=@numUserCntID and F.numDomainid=@numDomainId and F.tintType=@intType)            
          
          
   END          
 END          
          
 IF @tintRights=1 --Owner Records          
 BEGIN          
   IF @intType = 0          
   BEGIN          
  --Deals Won          
  SELECT @DealsWon=SUM(OM.monPAmount)          
   FROM OpportunityMaster OM          
   WHERE OM.tintOppStatus=1    and tintOpptype=1        
    AND OM.bintAccountClosingDate BETWEEN @dtDateFrom AND @dtDateTo          
    AND OM.numDomainID=@numDomainID                   
    AND OM.numRecOwner=@numUserCntID          
          
            
  --Deals in Pipeline          
  SELECT @Pipeline=SUM(OM.monPAmount)          
   FROM OpportunityMaster OM         
   WHERE OM.tintOppStatus=0   and tintOpptype=1         
    AND OM.intPEstimatedCloseDate BETWEEN @dtDateFrom AND @dtDateTo          
    AND OM.numDomainID=@numDomainID                  
    AND OM.numRecOwner=@numUserCntID          
   END          
   ELSE          
   BEGIN          
  --Deals Won          
  SELECT @DealsWon=SUM(OM.monPAmount)          
   FROM AdditionalContactsInformation ACI
   INNER JOIN OpportunityMaster OM ON ACI.numRecOwner = OM.numRecOwner
   WHERE OM.tintOppStatus=1      and tintOpptype=1      
    AND OM.bintAccountClosingDate BETWEEN @dtDateFrom AND @dtDateTo          
    AND OM.numDomainID=@numDomainID         
    AND ACI.numTeam is not null             
    AND ACI.numTeam in(select F.numTeam from ForReportsByTeam F             
    where F.numuserCntId=@numUserCntID and F.numDomainid=@numDomainId and F.tintType=@intType)            
          
              
            
  --Deals in Pipeline          
  SELECT @Pipeline=SUM(OM.monPAmount)          
   FROM AdditionalContactsInformation ACI          
    INNER JOIN OpportunityMaster OM ON ACI.numRecOwner = OM.numRecOwner       
   WHERE OM.tintOppStatus=0 and tintOpptype=1           
    AND OM.intPEstimatedCloseDate BETWEEN @dtDateFrom AND @dtDateTo          
    AND OM.numDomainID=@numDomainID                           
    AND ACI.numTeam is not null             
    AND ACI.numTeam in(select F.numTeam from ForReportsByTeam F             
    where F.numuserCntId=@numUserCntID and F.numDomainid=@numDomainId and F.tintType=@intType)            
          
          
   END          
 END          
          
 SET @Pipeline = ISNULL(@Pipeline,0)          
 SET @Quota = ISNULL(@Quota,0)          
 SET @DealsWon = ISNULL(@DealsWon,0)          
           
          
 SET @SUM=@Quota + @DealsWon + @Pipeline           
 IF @SUM>0          
 BEGIN          
  SET @QuotaPer = (@Quota/@SUM)*100          
  SET @DealsWonPer = (@DealsWon/@SUM)*100          
  SET @PipelinePer = (@Pipeline/@SUM)*100          
 END          
 ELSE          
 BEGIN          
  SET @QuotaPer = 0          
  SET @DealsWonPer = 0          
  SET @PipelinePer = 0          
 END          
 SELECT  @QuotaPer As QuotaPer, @Quota As Quota, @DealsWonPer As DealsWonPer, @DealsWon As DealsWon, @PipelinePer AS PipelinePer, @Pipeline AS Pipeline          
          
          
END
GO
