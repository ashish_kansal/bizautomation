/****** Object:  StoredProcedure [dbo].[USP_ProsActItems1]    Script Date: 07/26/2008 16:20:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj                                    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_prosactitems1')
DROP PROCEDURE usp_prosactitems1
GO
CREATE PROCEDURE [dbo].[USP_ProsActItems1]                                                  
@bytemode as tinyint=0,                                                  
@numdivisionid as numeric(9)=0,                                                  
@numContactId as numeric(9)=0 ,                                              
@bitTask as tinyint=0,                                              
@FromDate as datetime,                                              
@ToDate as datetime,                                              
@KeyWord as varchar(100)='',                                              
@CurrentPage int,                                              
@PageSize int,                                            
@ClientTimeZoneOffset Int,  --Added by Debasish to enable calculation of date according to client machine                                            
@TotRecs int output                                              
as                                              
                                              
--Create a Temporary table to hold data                                              
Create table #tempTable ( ID INT IDENTITY PRIMARY KEY,numCommId VARCHAR(15))                                                 
declare @strSql as varchar(500)                                              
                                                
if @numdivisionid>0                                                   
begin                                              
                                              
set @strSql= 'SELECT  numCommId                                                  
                FROM  Communication                                             
   where bitclosedflag = '+ convert(varchar(2),@bytemode) +'                                              
   and Communication.numdivisionid = '+ convert(varchar(15),@numdivisionid)      
      
if @KeyWord<>'' set @strSql=@strSql+  'and  textDetails like '''+ @KeyWord+'%'''                                              
                                               
                                                 
                                                 
                                                  
end                                                  
                                                  
if @numContactId >0                                                   
begin                                                 
                                              
                                           
set @strSql= 'SELECT '                                    
if @FromDate=0 set  @strSql=@strSql + ' top 10'                                    
set  @strSql=@strSql +' Communication.numCommId                                                 
                FROM  Communication                                               
   where bitclosedflag = '+ convert(varchar(2),@bytemode)+'                                              
   and Communication.numContactId= '+convert(varchar(15),@numContactId)                                             
   if @KeyWord<>'' set @strSql=@strSql+  'and  textDetails like '''+ @KeyWord+'%'''                                                   
                                                  
end                                              
                                              
                                              
if @FromDate>0 set  @strSql=@strSql +' and dtStartTime  >= '''+ convert(varchar(30),@FromDate)+''' and dtStartTime  <= '''+ convert(varchar(30),@ToDate)  +''''                                              
if   @bitTask > 0 set  @strSql=@strSql +' and  bitTask  = '+ convert(varchar(2),@bitTask) +''                                     
                                              
                                            
                                              
insert into #tempTable (numCommId)          
 exec( @strSql)                    
                  
                                            
  declare @firstRec as integer                                              
  declare @lastRec as integer                      
 set @firstRec= (@CurrentPage-1) * @PageSize                                              
     set @lastRec= (@CurrentPage*@PageSize+1)                                              
                                    
                                          
select C.numCommId, c.numCommId,                        
isnull(A2.vcFirstName,'')+' '+                                                   
isnull(A2.vcLastName,'') as vcusername,                                                            
A1.numContactId, textDetails,isnull(vcData,'-') as Activity,                               
A1.vcFirstName+' '+                                                   
A1.vcLastName as [Name],                 
case when A1.numPhone<>'' then + A1.numPhone +case when A1.numPhoneExtension<>'' then ' - ' + A1.numPhoneExtension else '' end  else '' end as [Phone],                                                                      
case when c.bitTask=1 then 'Communication' when c.bitTask=3 then 'Task' when c.bitTask=4 then 'Notes' when c.bitTask=5 then 'Follow up Status' end AS bitTask,                                         
DateAdd(minute,-@ClientTimeZoneOffset, dtStartTime) As CloseDate,                                            
dtCreatedDate As CreatedDate,    
c.caseid,   
(select top 1 vcCaseNumber from cases where cases.numcaseid= c.caseid )as vcCasenumber,       
isnull(caseTimeid,0)as caseTimeid,      
isnull(caseExpid,0)as caseExpid                                             
from #tempTable T                    
join Communication C                    
on C.numCommId=T.numCommId                     
JOIN AdditionalContactsInformation  A1                 
ON c.numContactId = A1.numContactId                                                   
left join AdditionalContactsInformation A2 on A2.numContactID=c.numAssign                                                  
left join listdetails on numActivity=numListID                    
where ID > @firstRec and ID < @lastRec order by  C.numCommId  Desc                                                          
                                            
set @TotRecs=(select count(*) from #tempTable)                                              
drop table #tempTable
GO
