/****** Object:  StoredProcedure [dbo].[Usp_cflList]    Script Date: 07/26/2008 16:15:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj  
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_cfllist')
DROP PROCEDURE usp_cfllist
GO
CREATE PROCEDURE [dbo].[Usp_cflList]    
@numDomainID as numeric(9)=0 ,   
@locId as numeric(9)
as    

IF(@locId = 45)

SELECT fld_id,Fld_label,fld_type,(CASE WHEN subgrp=0 THEN 'Detail Section' ELSE  grp_name END) AS TabName,
loc_name, 
ISNULL(CFV.bitIsRequired,0) AS bitIsRequired,
ISNULL(CFV.bitIsEmail,0) AS bitIsEmail,
ISNULL(CFV.bitIsAlphaNumeric,0) AS bitIsAlphaNumeric,
ISNULL(CFV.bitIsNumeric,0) AS bitIsNumeric,
ISNULL(CFV.bitIsLengthValidation,0) AS bitIsLengthValidation,
ISNULL(fmst.numlistid,0) AS numlistid,
CAST(fld_id AS varchar)+'-'+CAST(ISNULL(fmst.numlistid,0) as varchar) AS MergeFldLstId,
ISNULL(fmst.vcItemsLocation,'') AS vcItemsLocation
FROM CFW_Fld_Master fmst    
LEFT JOIN CFw_Grp_Master Tmst    
on Tmst.Grp_id=fmst.subgrp    
JOIN CFW_Loc_Master Lmst    
on Lmst.Loc_id=fmst.Grp_id  
LEFT JOIN CFW_Validation CFV ON CFV.numFieldID=fld_id
WHERE fmst.numDomainID = @numDomainID  
AND (Lmst.Loc_id = 1 or Lmst.Loc_id = 4)  -- for Leads/Prospects/Accounts and Contacts

ELSE IF(@locId = 26) -- (Sales Opportunities / Orders and Purchase Opportunities / Orders) combined for Email Template screen

SELECT fld_id,Fld_label,fld_type,(CASE WHEN subgrp=0 THEN 'Detail Section' ELSE  grp_name END) AS TabName,
loc_name, 
ISNULL(CFV.bitIsRequired,0) AS bitIsRequired,
ISNULL(CFV.bitIsEmail,0) AS bitIsEmail,
ISNULL(CFV.bitIsAlphaNumeric,0) AS bitIsAlphaNumeric,
ISNULL(CFV.bitIsNumeric,0) AS bitIsNumeric,
ISNULL(CFV.bitIsLengthValidation,0) AS bitIsLengthValidation,
ISNULL(fmst.numlistid,0) AS numlistid,
CAST(fld_id AS varchar)+'-'+CAST(ISNULL(fmst.numlistid,0) as varchar) AS MergeFldLstId,
ISNULL(fmst.vcItemsLocation,'') AS vcItemsLocation
FROM CFW_Fld_Master fmst    
LEFT JOIN CFw_Grp_Master Tmst    
on Tmst.Grp_id=fmst.subgrp    
JOIN CFW_Loc_Master Lmst    
on Lmst.Loc_id=fmst.Grp_id  
LEFT JOIN CFW_Validation CFV ON CFV.numFieldID=fld_id
WHERE fmst.numDomainID = @numDomainID  
AND (Lmst.Loc_id = 2 or Lmst.Loc_id = 6) 

ELSE
    
select fld_id,Fld_label,fld_type,case when subgrp=0 then 'Detail Section' else  grp_name  end as TabName,loc_name, 
ISNULL(CFV.bitIsRequired,0) AS bitIsRequired,ISNULL(CFV.bitIsEmail,0) AS bitIsEmail,ISNULL(CFV.bitIsAlphaNumeric,0) AS bitIsAlphaNumeric,ISNULL(CFV.bitIsNumeric,0) AS bitIsNumeric,ISNULL(CFV.bitIsLengthValidation,0) AS bitIsLengthValidation
,ISNULL(fmst.numlistid,0) AS numlistid,
CAST(fld_id AS varchar)+'-'+CAST(ISNULL(fmst.numlistid,0) as varchar) AS MergeFldLstId,
ISNULL(fmst.vcItemsLocation,'') AS vcItemsLocation
from CFW_Fld_Master fmst    
left join CFw_Grp_Master Tmst    
on Tmst.Grp_id=fmst.subgrp    
join CFW_Loc_Master Lmst    
on Lmst.Loc_id=fmst.Grp_id  
LEFT JOIN CFW_Validation CFV ON CFV.numFieldID=fld_id
where fmst.numDomainID=@numDomainID  
and  Lmst.Loc_id= case when @locId=0 then Lmst.Loc_id else @locId end

GO
