/****** Object:  StoredProcedure [dbo].[USP_DeleteItem]    Script Date: 03/25/2009 16:15:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--exec [dbo].[USP_DeleteItem] 147,1
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_deleteitem')
DROP PROCEDURE usp_deleteitem
GO
CREATE PROCEDURE [dbo].[USP_DeleteItem]
@ItemCode as numeric(9),
@numDomianID AS NUMERIC(9)
as  
--SELECT * FROM WareHouseItems [WareHouses]
--SELECT * FROM [WareHouseItems] WHERE [numItemID] =152  
--DELETE FROM [OpportunityBizDocItems] WHERE [numItemCode]=@ItemCode
--DELETE FROM [OpportunityBizDocKitItems] WHERE [numChildItem]=@ItemCode
--DELETE FROM [OpportunityItems] WHERE [numItemCode]=@ItemCode
BEGIN TRY 
BEGIN TRAN 
-----Delete From Similar Item Table

IF (SELECT COUNT(*) FROM OpportunityItems OI JOIN dbo.OpportunityMaster OM ON OI.numOppID=OM.numOppID 
		WHERE OM.numDomainID=@numDomianID AND numItemCode=@ItemCode) >0
 	 BEGIN
	  	RAISERROR ('OpportunityItems_Depend',16,1);
        RETURN
	  END
	  
IF (SELECT COUNT(*) FROM OpportunityKitItems WHERE numChildItemID=@ItemCode) >0
 	 BEGIN
	  	RAISERROR ('OpportunityKitItems_Depend',16,1);
        RETURN
	  END

	IF (SELECT COUNT(*) FROM WorkOrder WHERE numItemCode=@ItemCode) > 0
 	BEGIN
	  	RAISERROR ('WorkOrder_Depend',16,1);
        RETURN
	END
	  
IF (SELECT COUNT(*) FROM ItemDetails WHERE numChildItemID=@ItemCode) >0
 	 BEGIN
	  	RAISERROR ('KitItems_Depend',16,1);
        RETURN
	  END	  
	  
EXEC dbo.USP_ValidateFinancialYearClosingDate
	@numDomainID = @numDomianID, --  numeric(18, 0)
	@tintRecordType = 1, --  tinyint
	@numRecordID = @ItemCode --  numeric(18, 0)
	
	--Validation of closed financial year
--	EXEC dbo.USP_ValidateFinancialYearClosingDate @numDomainID,@datEntry_Date
--IF (SELECT COUNT(*) FROM dbo.General_Journal_Details WHERE numDomainId=@numDomianID AND  numItemID=@ItemCode AND chBizDocItems='IA1') >0
-- 	 BEGIN
--	  	RAISERROR ('Inventory_Adjustment',16,1);
--        RETURN
--	  END
--	--'Do not allow  to delete when any journal entry exist except its opening balance journal entry
--SELECT numJournalId into #temp FROM dbo.General_Journal_Details WHERE numDomainId=@numDomianID AND numItemID=@ItemCode
----SELECT * FROM #temp
--IF EXISTS(SELECT numJournalId FROM #temp WHERE numJournalId NOT IN (SELECT numJournalId FROM dbo.General_Journal_Details WHERE numDomainId=@numDomianID AND numItemID=@ItemCode AND chBizDocItems='OE1'))
--BEGIN
--  	RAISERROR ('Journal_Exist',16,1);
--    RETURN
--  END
--DROP TABLE #temp

DELETE FROM AssembledItem WHERE numDomainID=@numDomianID AND numItemCode=@ItemCode
DELETE FROM AssembledItem WHERE numDomainID=@numDomianID AND ID IN (SELECT ISNULL(numAssembledItemID,0) FROM AssembledItemChilds WHERE numItemCode=@ItemCode)
DELETE FROM OpportunityItems WHERE numItemCode=@ItemCode AND numOppID IS null
DELETE FROM dbo.ItemImages WHERE numItemCode = @ItemCode AND numDomainId = @numDomianID
DELETE FROM dbo.RecentItems WHERE numRecordID = @ItemCode AND chrRecordType='I'
DELETE FROM SimilarItems WHERE numParentItemCode = @ItemCode AND numDomainId= @numDomianID
delete from [CompanyAssets] where numItemCode=@ItemCode
DELETE FROM [Vendor] WHERE [numItemCode]=@ItemCode
DELETE FROM [ItemDiscountProfile] WHERE [numItemID]=@ItemCode
DELETE FROM [ItemDetails] WHERE [numItemKitID] = @ItemCode
DELETE FROM [ItemDetails] WHERE [numChildItemID] = @ItemCode

DELETE FROM WareHouseItems_Tracking WHERE numWareHouseItemID IN (SELECT numWareHouseItemID FROM WareHouseItems
where numItemID=@ItemCode )

DELETE FROM WareHouseItmsDTL WHERE numWareHouseItemID IN (SELECT numWareHouseItemID FROM WareHouseItems
where numItemID=@ItemCode )

delete from WareHouseItems
where numItemID=@ItemCode 

DELETE FROM dbo.ItemCategory WHERE numItemID=@ItemCode
--Delete all journal related to item
SELECT numJournalId into #temp FROM dbo.General_Journal_Details WHERE numDomainId=@numDomianID AND numItemID=@ItemCode
----SELECT * FROM #temp
DELETE FROM dbo.General_Journal_Details WHERE numDomainId=@numDomianID AND numJournalId IN (SELECT numJournalId FROM #temp)
DELETE FROM dbo.General_Journal_Header WHERE numDomainId=@numDomianID AND numJournal_Id IN (SELECT numJournalId FROM #temp)
DROP TABLE #temp


delete from item where numItemCode=@ItemCode AND [numDomainID] = @numDomianID

DELETE FROM [ItemAPI] WHERE [numItemID] = @ItemCode AND [numDomainId] = @numDomianID
--For Lucene index 
UPDATE LuceneItemsIndex SET bitItemDeleted = 1 WHERE numItemCode =@ItemCode AND numDomainID =@numDomianID

COMMIT
END TRY 
BEGIN CATCH 
DECLARE @ErrMsg nvarchar(4000)
SELECT @ErrMsg = ERROR_MESSAGE()
RaisError(@ErrMsg,16,1)

IF @@ERROR <> 0
ROLLBACK
END CATCH 