/****** Object:  StoredProcedure [dbo].[usp_InsertCommunication]    Script Date: 07/26/2008 16:19:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Modified By Anoop jayaraj                 
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_insertcommunication')
DROP PROCEDURE usp_insertcommunication
GO
CREATE PROCEDURE [dbo].[usp_InsertCommunication]                                                
@numCommId numeric=0,                            
@bitTask numeric,                                        
@numContactId numeric,                            
@numDivisionId numeric,                            
@txtDetails text,                            
@numOppId numeric=0,                            
@numAssigned numeric=0,                                                
@numUserCntID numeric,                                                                                                                                               
@numDomainId numeric,                                                
@bitClosed tinyint,                                                
@vcCalendarName varchar(100)='',                                                                                                     
@dtStartTime datetime,                 
@dtEndtime datetime,                                                                             
@numActivity as numeric(9),                                              
@numStatus as numeric(9),                                              
@intSnoozeMins as int,                                              
@tintSnoozeStatus as tinyint,                                            
@intRemainderMins as int,                                            
@tintRemStaus as tinyint,                                  
@ClientTimeZoneOffset Int,                  
@bitOutLook tinyint,            
@bitSendEmailTemplate bit,            
@bitAlert bit,            
@numEmailTemplate numeric(9)=0,            
@tintHours tinyint=0,          
@CaseID  numeric=0 ,        
@CaseTimeId numeric =0,    
@CaseExpId numeric = 0  ,                     
@ActivityId numeric = 0 ,
@bitFollowUpAnyTime BIT,
@strAttendee TEXT,
@numLinkedOrganization NUMERIC(18,0) = 0,
@numLinkedContact NUMERIC(18,0) = 0,
@strContactIds VARCHAR(500)=''                                             
AS
BEGIN               
	DECLARE @CheckRecord AS BIT   
	SET @CheckRecord = 1                

	IF NOT EXISTS (SELECT * FROM communication WHERE numcommid=@numCommId)                                                
		SET @CheckRecord = 0                                                

	IF @numCommId = 0 OR @CheckRecord = 0                                            
	BEGIN
		IF(@strContactIds='')
		BEGIN
			IF ISNULL(@numDivisionId,0) = 0                         
			BEGIN
				IF ISNULL(@numContactId,0) > 0
				BEGIN
					SELECT @numDivisionId = numDivisionId FROM AdditionalContactsInformation WHERE numContactId = @numContactId
					
					IF ISNULL(@numDivisionId,0) = 0 
					BEGIN
						RAISERROR('COMPANY_NOT_FOUND_FOR_CONTACT' ,16,1)
						RETURN
					END                   
				END
				ELSE
				BEGIN
					RAISERROR('COMPANY_NOT_FOUND',16,1)
					RETURN	
				END
			END
  
			IF ISNULL(@numContactId,0) = 0 
			BEGIN
				RAISERROR('CONTACT_NOT_FOUND',16,1)
				RETURN
			END   
		END         
   
		IF @numAssigned=0  
			SET @numAssigned=@numUserCntID
			                                            
		IF(@strContactIds='')
		BEGIN
			INSERT INTO communication                  
			(                
				bitTask,                
				numContactId,                
				numDivisionId,                
				textDetails,                
				intSnoozeMins,                
				intRemainderMins,                
				numStatus,                
				numActivity,                
				numAssign,                
				tintSnoozeStatus,                
				tintRemStatus,                
				numOppId,                
				numCreatedby,                
				dtCreatedDate,                
				numModifiedBy,                
				dtModifiedDate,                
				numDomainID,                
				bitClosedFlag,                
				vcCalendarName,                
				bitOutlook,                
				dtStartTime,                
				dtEndTime,              
				numAssignedBy,            
				bitSendEmailTemp,            
				numEmailTemplate,            
				tintHours,            
				bitAlert,          
				CaseId,        
				CaseTimeId,                                                                                 
				CaseExpId,  
				numActivityId,
				bitFollowUpAnyTime            
			)                 
			VALUES                
			(                                              
				@bitTask,                
				@numcontactId,                                              
				@numDivisionId,                
				ISNULL(@txtdetails,''),                
				@intSnoozeMins,                
				@intRemainderMins,                
				@numStatus,                
				@numActivity,                                            
				@numAssigned,                
				@tintSnoozeStatus,                
				@tintRemStaus,                                             
				@numoppid,                
				@numUserCntID,                
				getutcdate(),                
				@numUserCntID,                                              
				getutcdate(),                
				@numDomainId,                
				@bitClosed,                
				@vcCalendarName,                
				@bitOutLook,                                              
				DateAdd(minute, @ClientTimeZoneOffset, @dtStartTime),                
				DateAdd(minute, @ClientTimeZoneOffset,@dtEndtime),              
				@numUserCntID,            
				@bitSendEmailTemplate,            
				@numEmailTemplate,            
				@tintHours,            
				@bitAlert,          
				@CaseID,        
				@CaseTimeId,                                                                                 
				@CaseExpId,  
				@ActivityId,
				@bitFollowUpAnyTime 
			)  

			SET @numCommId= SCOPE_IDENTITY() 

			IF NOT EXISTS(SELECT * FROM CommunicationAttendees WHERE numCommId=@numCommId AND numContactId=@numcontactId)
			BEGIN
				INSERT INTO CommunicationAttendees(numCommId,numContactId) VALUES(@numCommId,@numcontactId)
			END
			IF NOT EXISTS(SELECT * FROM CommunicationAttendees WHERE numCommId=@numCommId AND numContactId=@numAssigned)
			BEGIN
				INSERT INTO CommunicationAttendees(numCommId,numContactId) VALUES(@numCommId,@numAssigned)
			END
			IF NOT EXISTS(SELECT * FROM CommunicationAttendees WHERE numCommId=@numCommId AND numContactId=@numUserCntID)
			BEGIN
				INSERT INTO CommunicationAttendees(numCommId,numContactId) VALUES(@numCommId,@numUserCntID)
			END
		END
		ELSE
		BEGIN
			INSERT INTO COMMUNICATION                  
			(                
				bitTask,                
				numContactId,                
				numDivisionId,                
				textDetails,                
				intSnoozeMins,                
				intRemainderMins,                
				numStatus,                
				numActivity,                
				numAssign,                
				tintSnoozeStatus,                
				tintRemStatus,                
				numOppId,                
				numCreatedby,                
				dtCreatedDate,                
				numModifiedBy,                
				dtModifiedDate,                
				numDomainID,                
				bitClosedFlag,                
				vcCalendarName,                
				bitOutlook,                
				dtStartTime,                
				dtEndTime,              
				numAssignedBy,            
				bitSendEmailTemp,            
				numEmailTemplate,            
				tintHours,            
				bitAlert,          
				CaseId,        
				CaseTimeId,                                                                                 
				CaseExpId,  
				numActivityId,
				bitFollowUpAnyTime            
			)                 
			SELECT                                          
				@bitTask,                
				CAST(Items AS NUMERIC(9)),                                              
				(SELECT TOP 1 numDivisionId FROM AdditionalContactsInformation WHERE numContactId=CAST(Items AS NUMERIC(9))),                
				ISNULL(@txtdetails,''),                
				@intSnoozeMins,                
				@intRemainderMins,                
				@numStatus,                
				@numActivity,                                            
				@numAssigned,                
				@tintSnoozeStatus,                
				@tintRemStaus,                                             
				@numoppid,                
				@numUserCntID,                
				getutcdate(),                
				@numUserCntID,                                              
				getutcdate(),                
				@numDomainId,                
				@bitClosed,                
				@vcCalendarName,                
				@bitOutLook,                                              
				DateAdd(minute, @ClientTimeZoneOffset, @dtStartTime),                
				DateAdd(minute, @ClientTimeZoneOffset,@dtEndtime),              
				@numUserCntID,            
				@bitSendEmailTemplate,            
				@numEmailTemplate,            
				@tintHours,            
				@bitAlert,          
				@CaseID,        
				@CaseTimeId,                                                                                 
				@CaseExpId,  
				@ActivityId ,@bitFollowUpAnyTime 
			FROM 
				dbo.Split(@strContactIds,',') 
			WHERE 
				Items<>'' AND Items<>'0'

			SET @numCommId= SCOPE_IDENTITY() 
		END                                                           
   
		IF ISNULL(@numLinkedOrganization,0) > 0
		BEGIN
			INSERT INTO CommunicationLinkedOrganization VALUES (@numCommId,@numLinkedOrganization,@numLinkedContact)
		END                                          
	END                                                
	ELSE                    
	BEGIN
    ---Updating if Action Item is assigned to someone                      
		DECLARE @tempAssignedTo AS NUMERIC(18,0)                    
		SET @tempAssignedTo=null                     
		SELECT @tempAssignedTo=isnull(numAssign,0) FROM communication WHERE numcommid=@numCommId                     
	                 
		IF (@tempAssignedTo <> @numAssigned AND @numAssigned<>'0')                    
		BEGIN                      
			UPDATE communication SET numAssignedBy=@numUserCntID WHERE numcommid=@numCommId              
		END                     
		ELSE IF (@numAssigned =0)                    
		BEGIN                    
			UPDATE communication SET numAssignedBy=0 WHERE numcommid=@numCommId                  
		END                
                                              
		IF @vcCalendarName=''                                                 
		BEGIN
			UPDATE 
				communication 
			SET                 
				bittask=@bittask,                                                                              
				textdetails=@txtdetails,                                               
				intSnoozeMins=@intSnoozeMins,                                               
				numStatus=@numStatus,                                              
				numActivity=@numActivity,                                             
				tintSnoozeStatus=@tintSnoozeStatus,                                            
				intRemainderMins= @intRemainderMins,                                            
				tintRemStatus= @tintRemStaus,                                              
				numassign=@numAssigned,                                                
				nummodifiedby=@numUserCntID,                                                
				dtModifiedDate=getutcdate(),                                                
				bitClosedFlag=@bitClosed,                                                        
				bitOutLook=@bitOutLook,                
				dtStartTime=DateAdd(minute, @ClientTimeZoneOffset, @dtStartTime),                
				dtEndTime=DateAdd(minute, @ClientTimeZoneOffset,@dtEndtime),            
				bitSendEmailTemp=@bitSendEmailTemplate,            
				numEmailTemplate=@numEmailTemplate,            
				tintHours=@tintHours,            
				bitAlert=@bitAlert,       
				CaseID=@CaseID,        
				CaseTimeId=@CaseTimeId,                                                                                 
				CaseExpId=@CaseExpId  ,  
				numActivityId=@ActivityId ,bitFollowUpAnyTime=@bitFollowUpAnyTime,
				dtEventClosedDate=Case When isnull(@bitClosed,0)=1 then getutcdate() else null end                                 
			WHERE 
				numcommid=@numCommId                                                
		END                                                
		ELSE                                                
		BEGIN
			UPDATE 
				communication 
			SET                 
				bittask=@bittask,                                                                              
				textdetails=@txtdetails,                                               
				intSnoozeMins=@intSnoozeMins,                                
				numStatus=@numStatus,                                              
				numActivity=@numActivity,                                             
				tintSnoozeStatus=@tintSnoozeStatus,                                            
				intRemainderMins= @intRemainderMins,                                            
				tintRemStatus= @tintRemStaus,        
				numassign=@numAssigned,                                                
				nummodifiedby=@numUserCntID,                                                
				dtModifiedDate=getutcdate(),                                                
				bitClosedFlag=@bitClosed,                                                        
				bitOutLook=@bitOutLook,                
				dtStartTime=DateAdd(minute, @ClientTimeZoneOffset, @dtStartTime),                
				dtEndTime=DateAdd(minute, @ClientTimeZoneOffset,@dtEndtime),                                                 
				vcCalendarName=@vcCalendarName,            
				bitSendEmailTemp=@bitSendEmailTemplate,            
				numEmailTemplate=@numEmailTemplate,            
				tintHours=@tintHours,            
				bitAlert=@bitAlert ,      
				CaseID=@CaseID,        
				CaseTimeId=@CaseTimeId,                                                                                 
				CaseExpId=@CaseExpId  ,  
				numActivityId=@ActivityId  ,bitFollowUpAnyTime=@bitFollowUpAnyTime,
				dtEventClosedDate=Case When isnull(@bitClosed,0)=1 then getutcdate() else null end 
			WHERE 
				numcommid=@numCommId                                                
		END  
   
		IF ISNULL(@numLinkedOrganization,0) > 0
		BEGIN
			IF EXISTS (SELECT numCLOID FROM CommunicationLinkedOrganization WHERE numCommID=@numCommId)
			BEGIN
				UPDATE CommunicationLinkedOrganization SET numDivisionID=@numLinkedOrganization,numContactID=@numLinkedContact WHERE numCommID=@numCommId
			END
			ELSE
			BEGIN
				INSERT INTO CommunicationLinkedOrganization VALUES (@numCommId,@numLinkedOrganization,@numLinkedContact)
			END
		END
		ELSE
		BEGIN
			DELETE FROM CommunicationLinkedOrganization WHERE numCommID=@numCommId
		END                                         
	END 
   
	SELECT @numCommId
END
GO
