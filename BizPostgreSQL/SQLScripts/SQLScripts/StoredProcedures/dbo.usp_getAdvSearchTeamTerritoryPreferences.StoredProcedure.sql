/****** Object:  StoredProcedure [dbo].[usp_getAdvSearchTeamTerritoryPreferences]    Script Date: 07/26/2008 16:16:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created By: Debasish Tapan Nag                                          
--Purpose: Retireves the Team and Territory Preference of the User for Advance Search          
--Created Date: 08/14/2005          
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getadvsearchteamterritorypreferences')
DROP PROCEDURE usp_getadvsearchteamterritorypreferences
GO
CREATE PROCEDURE [dbo].[usp_getAdvSearchTeamTerritoryPreferences]        
 @numDomainID Numeric,          
 @numUserCntID Numeric,        
 @vcTerritoryTeamFlag NVarchar(5)        
AS        
 DECLARE @vcTerritoriesSelected NVARCHAR(2000)        
 DECLARE @vcTeamsSelected NVARCHAR(2000)        
 SELECT @vcTeamsSelected = vcTeamsSelected,        
 @vcTerritoriesSelected = vcTerritoriesSelected        
 FROM AdvSearchTeamTerritoryPreferences        
 Where numUserCntId = @numUserCntID And numDomainID = @numDomainID        
 IF @vcTerritoryTeamFlag = 'Teams'        
 BEGIN        
  If LTrim(@vcTeamsSelected) = ''        
  select numListItemId as numTeamId, vcData as vcTeamName from UserTeams, ListDetails          
  where 1 = 2        
  Else        
  BEGIN        
    DECLARE @iTeamDoc int        
    EXECUTE sp_xml_preparedocument @iTeamDoc OUTPUT, @vcTeamsSelected        
    SELECT numTeamId as numListItemId, vcData FROM OpenXML(@iTeamDoc, '/Teams/Team', 2)        
    WITH        
    (        
     numTeamId Numeric,        
     vcTeamName NVarchar(50)        
    ) st, UserTeams ut, ListDetails ld        
    WHERE st.numTeamId = ut.numTeam        
    AND ld.numListItemId = ut.numTeam        
    AND ut.numUserCntID = @numUserCntID          
    AND ut.numDomainID = @numDomainID       
    AND ld.numDomainId = ut.numDomainId         
         
    EXECUTE sp_xml_removedocument @iTeamDoc        
  END        
 END        
 ELSE        
 BEGIN        
  If LTrim(@vcTerritoriesSelected) = ''        
  select numTerId as numTerritoryID, vcTerName from TerritoryMaster        
  where 1 = 2        
  Else        
  BEGIN        
    DECLARE @iTerrDoc int        
    EXECUTE sp_xml_preparedocument @iTerrDoc OUTPUT, @vcTerritoriesSelected        
    SELECT st.numTerId as numTerritoryID, tm.vcTerName FROM OpenXML(@iTerrDoc, '/Territories/Territory', 2)        
    WITH        
    (        
     numTerId Numeric,        
     vcTerName NVarchar(50)        
    ) st, TerritoryMaster tm        
    WHERE st.numTerId = tm.numTerId        
    AND tm.numDomainID = @numDomainID        
         
    EXECUTE sp_xml_removedocument @iTerrDoc        
   END        
 END
GO
