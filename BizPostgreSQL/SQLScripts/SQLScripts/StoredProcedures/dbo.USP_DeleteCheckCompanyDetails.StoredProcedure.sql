/****** Object:  StoredProcedure [dbo].[USP_DeleteCheckCompanyDetails]    Script Date: 07/26/2008 16:15:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created By Siva
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_deletecheckcompanydetails')
DROP PROCEDURE usp_deletecheckcompanydetails
GO
CREATE PROCEDURE [dbo].[USP_DeleteCheckCompanyDetails]
(@numDomainId as numeric(9)=0,
 @numCheckCompanyId as numeric(9)=0
)   
As
Begin
	Delete from CheckCompanyDetails Where numCheckCompanyId=@numCheckCompanyId And numDomainId=@numDomainId
End
GO
