/****** Object:  StoredProcedure [dbo].[USP_GetReportForTeams]    Script Date: 07/26/2008 16:18:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getreportforteams')
DROP PROCEDURE usp_getreportforteams
GO
CREATE PROCEDURE [dbo].[USP_GetReportForTeams]
@numUserCntid as numeric(9),    
@numDomainId AS NUMERIc(9),    
@tintType as tinyint    
as    
select numListitemid,vcdata from forreportsbyteam
join listdetails     
on numlistitemid=numteam
where forreportsbyteam.numUserCntid=@numUserCntid    
and forreportsbyteam.numDomainId=@numDomainId    
and tintType=@tintType
GO
