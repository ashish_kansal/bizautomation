/****** Object:  StoredProcedure [dbo].[uspp_ExecteDynamicQuery]    Script Date: 07/26/2008 16:21:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='uspp_exectedynamicquery')
DROP PROCEDURE uspp_exectedynamicquery
GO
CREATE PROCEDURE [dbo].[uspp_ExecteDynamicQuery]    
@Sql as varchar(max)  ,  
@numUserCntId as varchar(9)='1',  
@numDomainId as varchar (9)='1' , 
 @ClientTimeZoneOffset as numeric 
    
as     
set @Sql=replace(@Sql,'@numUserCntId',  @numUserCntId)

declare @sql1  varchar(max)  
set @sql1 ='declare @numUserCntId numeric(9) set @numUserCntId ='+@numUserCntId  
--set @sql1 = @sql1+ ' declare @numDomainId  numeric(9) set @numDomainId='+@numDomainId  
set @sql1 = @sql1+ ' declare @ClientTimeZoneOffset  numeric(9) set @ClientTimeZoneOffset='+convert(varchar(10),@ClientTimeZoneOffset )
set @sql1 = @sql1+' '+ @sql  

set @sql1=replace(@sql1,'@numDomainId',  @numDomainId)

print(@Sql1)  
execute(@Sql1)
GO
