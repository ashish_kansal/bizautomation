/****** Object:  StoredProcedure [dbo].[USP_GetTerrForUsers]    Script Date: 07/26/2008 16:18:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getterrforusers')
DROP PROCEDURE usp_getterrforusers
GO
CREATE PROCEDURE [dbo].[USP_GetTerrForUsers]        
@numUserCntID as numeric(9),        
@numDomainId as numeric(9)        
as        
SELECT distinct(T.numTerritoryID),Lst.vcData vcTerName          
   FROM  UserTerritory T          
 join ListDetails Lst          
 on Lst.numListItemID=T.numTerritoryID          
   WHERE  T.numUserCntID=@numUserCntID and Lst.numListID=78        
 and T.numdomainid=@numDomainId
GO
