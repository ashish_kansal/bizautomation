/****** Object:  StoredProcedure [dbo].[usp_GetEcosystemReport]    Script Date: 07/26/2008 16:17:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getecosystemreport')
DROP PROCEDURE usp_getecosystemreport
GO
CREATE PROCEDURE [dbo].[usp_GetEcosystemReport]            
 @numDomainID varchar(15),                           
 @numDivisionID varchar(15),                
 @numUserCntID numeric=0,                 
 @intType numeric=0        
  --              
AS                
declare @strSql as varchar(8000)         
declare @strFields as varchar(8000)      
set @strFields = dbo.fn_GetReortFieldsData()      
      
declare @entity1 as varchar(8000)      
set @entity1=replace(@strFields,'CM','CM1')      
set @entity1=replace(@entity1,'DM','DM1')      
      
      
declare @entity2 as varchar(8000)      
set @entity2=replace(@strFields,'CM','CM2')      
set @entity2=replace(@entity2,'DM','DM2')      
      
      
      
set @strSql='select CM1.numCompanyID as numCompanyID1,DM1.numDivisionID as numDivisionID1,dbo.fn_GetPrimaryContact(DM1.numDivisionID) as ContactID1,DM1.tintCRMType as tintCRMType1,    
CM2.numCompanyID as numCompanyID2,DM2.numDivisionID as numDivisionID2,dbo.fn_GetPrimaryContact(DM2.numDivisionID) as ContactID2,DM2.tintCRMType as tintCRMType2,    
CM1.vcCompanyName +'', ''+DM1.vcDivisionName as Entity1,      
CM2.vcCompanyName +'', ''+DM2.vcDivisionName as Entity2,      
dbo.fn_GetListItemName(numReferralType)  as Type,'+@entity1+' as SelectedData1 ,      
'+@entity2 +' as SelectedData2      
from CompanyAssociations Ass      
join DivisionMaster DM1      
on DM1.numDivisionID=Ass.numAssociateFromDivisionID      
join CompanyInfo CM1       
on CM1.numCompanyID=DM1.numCompanyID      
join DivisionMaster DM2      
on DM2.numDivisionID=Ass.numDivisionID      
join CompanyInfo CM2       
on CM2.numCompanyID=DM2.numCompanyID      
where DM1.numDomainID=DM2.numDomainID and DM1.numDomainID='+@numDomainID+' and  (numAssociateFromDivisionID='+@numDivisionID+' or Ass.numDivisionID='+@numDivisionID +')'     
    

print(@strSql)       
   exec( @strSql)
GO
