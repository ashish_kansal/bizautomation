/****** Object:  StoredProcedure [dbo].[USP_SaveProcurementBudgetHeader]    Script Date: 07/26/2008 16:21:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created By Siva                  
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_saveprocurementbudgetheader')
DROP PROCEDURE usp_saveprocurementbudgetheader
GO
CREATE PROCEDURE [dbo].[USP_SaveProcurementBudgetHeader]                  
@intFiscalYear as numeric(9)=0,                  
@bitPurchaseDeal as bit,                  
@numDomainId as numeric(9)=0,                  
@numUserCntId as numeric(9)=0                  
As                  
Begin         
Declare @numProcurementBudgetId as numeric(9)       
Set @numProcurementBudgetId=(Select isnull(numProcurementBudgetId,0) From ProcurementBudgetMaster Where numDomainId=@numDomainId)       
               
If @numProcurementBudgetId is null      
 Begin      
  Insert into ProcurementBudgetMaster(intFiscalYear,bitPurchaseDeal,numDomainId,numCreatedBy,dtCreationDate)                  
  Values(@intFiscalYear,@bitPurchaseDeal,@numDomainId,@numUserCntId,getutcdate())                  
  Set @numProcurementBudgetId = SCOPE_IDENTITY()                                                
  Select @numProcurementBudgetId        
 End                  
Else      
 Begin      
    Update ProcurementBudgetMaster Set bitPurchaseDeal=@bitPurchaseDeal,numModifiedBy=@numUserCntId,dtModifiedDate=getutcdate()      
 Where numProcurementBudgetId=@numProcurementBudgetId  And numDomainId=@numDomainId    
 Select @numProcurementBudgetId      
 End            
End
GO
