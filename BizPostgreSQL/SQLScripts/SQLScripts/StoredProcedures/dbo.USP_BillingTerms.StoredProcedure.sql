
--created by anoop jayaraj

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_billingterms')
DROP PROCEDURE usp_billingterms
GO
CREATE PROCEDURE USP_BillingTerms 
	@nmDivisionID as numeric(9)
as
BEGIN
	SELECT 
		ISNULL(tintBillingTerms,0) as tintBillingTerms
		,ISNULL(numBillingDays,0) as numBillingDays
		,ISNULL(tintInterestType,0) as tintInterestType
		,ISNULL(fltInterest ,0) as fltInterest
		,ISNULL(numAssignedTo,0) numAssignedTo
		,intShippingCompany
		,numDefaultShippingServiceID
	FROM 
		DivisionMaster 
	WHERE 
		numDivisionID=@nmDivisionID
END