/****** Object:  StoredProcedure [dbo].[USP_OppUpDateBizDocs_AmountPaid]    Script Date: 07/26/2008 16:20:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created By Siva                                
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_oppupdatebizdocs_amountpaid')
DROP PROCEDURE usp_oppupdatebizdocs_amountpaid
GO
CREATE PROCEDURE [dbo].[USP_OppUpDateBizDocs_AmountPaid]                                                      
(                                                      
 @numOppBizDocsId as numeric(9)=null,                                                      
 @UserCntID as numeric(9) =null,                                                      
 @amountPaid as DECIMAL(20,5)=null,                                                      
 @numPaymentMethod as numeric=0,                                      
 @numDepoistToChartAcntId as numeric(9)=0,                                    
 @numDomainId as numeric(9)=0,                                  
 @vcReference as varchar(200)=null,                                  
 @vcMemo as varchar(50)=null,                            
 @bitIntegratedToAcnt as bit,                        
 @numBizDocsPaymentDetId as numeric(9)=0 output,
 @bitDeferredIncome as bit,                
 @sintDeferredIncomePeriod as smallint=0,                
 @dtDeferredIncomeStartDate as datetime,                
 @numContractId as numeric(9)=0,          
 @bitSalesDeferredIncome as bit,        
 @bitDisable as BIT,
 @numCardTypeID NUMERIC(9),
 @bitCardAuthorized BIT=0,
 @bitAmountCaptured BIT=0,
 @vcSignatureFile varchar(100)=''
)                                                      
as   
  DECLARE @AmtPaidComplete AS BIT
  DECLARE @OppID AS NUMERIC(9)                            
  DECLARE @OldAmountPaid AS DECIMAL(20,5)
  DECLARE @TotalAmtPaid AS DECIMAL(20,5)                     
  DECLARE @tintOppType AS TINYINT                    
  DECLARE @numBizDocId AS NUMERIC(9) 
  DECLARE @numAuthoritativeBizDocsId AS NUMERIC(9)                    
  DECLARE @numCurrentBizDocsPaymentDetId AS NUMERIC(9) 
  DECLARE @numCurrencyID AS NUMERIC(9)
  DECLARE @fltExchangeRate AS FLOAT
  DECLARE @monAmount AS DECIMAL(20,5)              
  DECLARE @i AS INTEGER              
  DECLARE @dtDueDate AS DATETIME              
   Declare @numSignID as numeric;SET @numSignID=NULL
  Declare @numReferenceID as numeric;SET @numReferenceID=NULL


	SET @monAmount = 0              
	SET @AmtPaidComplete = 0
	IF @numContractId = 0 
	SET @numContractId = NULL                
	IF @sintDeferredIncomePeriod = 0 
	SET @sintDeferredIncomePeriod = 1                
	IF @dtDeferredIncomeStartDate = 'Jan  1 1753 12:00AM' 
	SET @dtDeferredIncomeStartDate = GETUTCDATE()  
	        
	select @OppID=numOppId,@numBizDocId=numBizDocId from OpportunityBizDocs where numOppBizDocsId=@numOppBizDocsId                      
	Select @OldAmountPaid=monAmount From OpportunityBizDocsDetails Where numBizDocsPaymentDetId=@numBizDocsPaymentDetId                      
	Select @tintOppType=tintOppType,@numCurrencyID=isnull(numCurrencyID,0),@numReferenceID=numContactId From OpportunityMaster Where numOppId=@OppID                     

 IF len(@vcSignatureFile)>0
 BEGIN
	IF (select count(*) from SignatureDetail where numDomainId=@numDomainId	and numReferenceID=@numReferenceID and tintModuleType=1)>0
	BEGIN
		UPDATE SignatureDetail set vcSignatureFile=@vcSignatureFile where numDomainId=@numDomainId and numReferenceID=@numReferenceID and tintModuleType=1
		SELECT @numSignID=numSignID from SignatureDetail where numDomainId=@numDomainId	and numReferenceID=@numReferenceID and tintModuleType=1
	END
	ELSE
	BEGIN
	    insert into SignatureDetail ([vcSignatureFile],[tintModuleType],[numReferenceID],[numDomainID])
			values(@vcSignatureFile,1,@numReferenceID,@numDomainId)

		set @numSignID=@@Identity 
	END
 END

	if @numCurrencyID=0 set   @fltExchangeRate=1
	else set @fltExchangeRate=dbo.GetExchangeRate(@numDomainID,@numCurrencyID)                   
	              
	If @tintOppType=1                
	Select @numAuthoritativeBizDocsId=isnull(numAuthoritativeSales,0) From AuthoritativeBizDocs Where numDomainId=@numDomainId
	Else if  @tintOppType=2                  
	Select @numAuthoritativeBizDocsId=isnull(numAuthoritativePurchase,0) From AuthoritativeBizDocs Where numDomainId=@numDomainId                     
	--  print @numAuthoritativeBizDocsId                    
	--  print @numBizDocID             



       
  If @numBizDocsPaymentDetId = 0                         
   Begin          
    Update OpportunityBizDocs set monAmountPaid=isnull(monAmountPaid,0)+@amountPaid,                      
    numModifiedBy=@UserCntID,dtModifiedDate=getutcdate()                    
    Where numOppId=@OppID And numOppBizDocsId=@numOppBizDocsId                  
     
   select @TotalAmtPaid=isnull(monAmountPaid,0) from OpportunityBizDocs where numOppBizDocsId=@numOppBizDocsId              
     
   if  @TotalAmtPaid>=isnull(dbo.GetDealAmount(@OppID,getutcdate(),@numOppBizDocsId),0) 
   begin
		update OpportunityBizDocs set dtShippedDate=getutcdate() where  numOppBizDocsId=@numOppBizDocsId
   end

   -- To update AmountPaid only if it is Authoritative BizDocs                    
    If @numAuthoritativeBizDocsId=@numBizDocID                                              
     Begin    
      set @AmtPaidComplete=1                
--      Print '@numBizDocId==='+convert(varchar(100),@numBizDocId)
      Update OpportunityMaster set numPClosingPercent=isnull(numPClosingPercent,0)+@amountPaid  Where numOppId=@OppID                         
      -- To insert Payment Details in OpportunityBizDocsDetails Table for Accounting Purpose                                         
      Insert into OpportunityBizDocsDetails(numBizDocsId,numPaymentMethod,monAmount,numDepoistToChartAcntId,numDomainId,                    
      vcReference,vcMemo,bitIntegratedToAcnt,bitAuthoritativeBizDocs,bitSalesDeferredIncome,bitDeferredIncome,
		sintDeferredIncomePeriod,dtDeferredIncomeStartDate,numContractId,numCreatedBy,dtCreationDate,numCurrencyID,fltExchangeRate,tintPaymentType,numCardTypeID,bitCardAuthorized,bitAmountCaptured,numSignID)                       
      Values(@numOppBizDocsId,@numPaymentMethod,@amountPaid,@numDepoistToChartAcntId,@numDomainId,@vcReference,@vcMemo,                    
      @bitIntegratedToAcnt,1,@bitSalesDeferredIncome,@bitDeferredIncome,@sintDeferredIncomePeriod,@dtDeferredIncomeStartDate,@numContractId,@UserCntID,getutcdate(),@numCurrencyID,@fltExchangeRate,1,@numCardTypeID,@bitCardAuthorized,@bitAmountCaptured,@numSignID)
      Set @numCurrentBizDocsPaymentDetId=@@Identity               
      SET @numBizDocsPaymentDetId=@numCurrentBizDocsPaymentDetId 
              Print '@numCurrentBizDocsPaymentDetId========='+Convert(varchar(1000),@numCurrentBizDocsPaymentDetId)                                     
      set @i=0              
      Set @monAmount=@amountPaid/@sintDeferredIncomePeriod              
      While @i<@sintDeferredIncomePeriod              
       Begin               
        Set @dtDueDate=dateadd(month,@i,@dtDeferredIncomeStartDate)              
        Insert into OpportunityBizDocsPaymentDetails(numBizDocsPaymentDetId,monAmount,dtDueDate,bitIntegrated)              
        Values(@numCurrentBizDocsPaymentDetId,@monAmount,@dtDueDate,0)              
        Set @i=@i+1              
       End              
                   
     End                    
    Else                    
     Begin                    
      Insert into OpportunityBizDocsDetails(numBizDocsId,numPaymentMethod,monAmount,numDepoistToChartAcntId,numDomainId,                    
      vcReference,vcMemo,bitIntegratedToAcnt,bitAuthoritativeBizDocs,numCreatedBy,dtCreationDate,numCurrencyID,fltExchangeRate,tintPaymentType,numCardTypeID,bitCardAuthorized,bitAmountCaptured,numSignID)
      Values(@numOppBizDocsId,@numPaymentMethod,@amountPaid,@numDepoistToChartAcntId,@numDomainId,@vcReference,@vcMemo,                    
      @bitIntegratedToAcnt,0,@UserCntID,getutcdate(),@numCurrencyID,@fltExchangeRate,1,@numCardTypeID,@bitCardAuthorized,@bitAmountCaptured,@numSignID)
      SET @numBizDocsPaymentDetId=@@IDENTITY 
     End                    
                    
   End                        
   Else                        
    Begin             
  if @bitDisable=0        
   Begin                
     Update OpportunityBizDocs Set monAmountPaid=isnull(monAmountPaid,0)-@OldAmountPaid+@amountPaid,                                                      
     numModifiedBy=@UserCntID,dtModifiedDate=getutcdate()                    
     Where numOppId=@OppID And numOppBizDocsId=@numOppBizDocsId   

     select @TotalAmtPaid=isnull(monAmountPaid,0) from OpportunityBizDocs where       numOppBizDocsId=@numOppBizDocsId              
     
      if  @TotalAmtPaid>=isnull(dbo.GetDealAmount(@OppID,getutcdate(),@numOppBizDocsId),0) 
	   begin
	   update OpportunityBizDocs set dtShippedDate=getutcdate() where  numOppBizDocsId=@numOppBizDocsId
			
	   end                 
                       
     -- To update AmountPaid only if it is Authoritative BizDocs                    
     If @numAuthoritativeBizDocsId=@numBizDocID     
		set @AmtPaidComplete=1                                          
      Update OpportunityMaster Set  numPClosingPercent=isnull(numPClosingPercent,0)-@OldAmountPaid +@amountPaid Where numOppId=@OppID                         
                         
     --To Update in Payment Details OpportunityBizDocsDetails                      
      Update OpportunityBizDocsDetails Set numPaymentMethod=@numPaymentMethod,monAmount=@amountPaid,vcReference=@vcReference,vcMemo=@vcMemo,bitSalesDeferredIncome=@bitSalesDeferredIncome,              
      bitDeferredIncome=@bitDeferredIncome,sintDeferredIncomePeriod=@sintDeferredIncomePeriod,dtDeferredIncomeStartDate=@dtDeferredIncomeStartDate,                
      numContractId=@numContractId Where numBizDocsPaymentDetId=@numBizDocsPaymentDetId                        
                 
     --To Update in Payment Details OpportunityBizDocsPaymentDetails                  
      Delete OpportunityBizDocsPaymentDetails Where numBizDocsPaymentDetId=@numBizDocsPaymentDetId              
                        
                       
      set @i=0              
      Set @monAmount=@amountPaid/@sintDeferredIncomePeriod              
      While @i<@sintDeferredIncomePeriod              
     Begin               
      Set @dtDueDate=dateadd(month,@i,@dtDeferredIncomeStartDate)              
      Insert into OpportunityBizDocsPaymentDetails(numBizDocsPaymentDetId,monAmount,dtDueDate,bitIntegrated)              
      Values(@numBizDocsPaymentDetId,@monAmount,@dtDueDate,0)              
      Set @i=@i+1              
     End              
   End                 
   Else        
    Begin        
     --To Update in Payment Details OpportunityBizDocsDetails                      
      Update OpportunityBizDocsDetails Set numPaymentMethod=@numPaymentMethod,vcReference=@vcReference,vcMemo=@vcMemo Where numBizDocsPaymentDetId=@numBizDocsPaymentDetId                        
                 
    End           
 End 

select @AmtPaidComplete                    

GO
