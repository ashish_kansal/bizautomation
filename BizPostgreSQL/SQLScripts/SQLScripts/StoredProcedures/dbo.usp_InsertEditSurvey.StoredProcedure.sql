/****** Object:  StoredProcedure [dbo].[usp_InsertEditSurvey]    Script Date: 07/26/2008 16:19:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created By: Debasish Tapan Nag                                      
--Purpose: Creates a New Survey if it does not exist already, else it updates the existing survey                
--Created Date: 09/17/2005                            
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_inserteditsurvey')
DROP PROCEDURE usp_inserteditsurvey
GO
CREATE PROCEDURE [dbo].[usp_InsertEditSurvey]                  
 @numSurID numeric,                  
 @numDomainID numeric,                 
 @numUserID numeric,                  
 @vcSurveyInformation NText                
AS                  
--This procedure will Save Survey based on input parameters.                  
BEGIN                  
 DECLARE @iDoc int                        
                
 IF @numSurID = 0                  
 BEGIN                  
   EXECUTE sp_xml_preparedocument @iDoc OUTPUT, @vcSurveyInformation                    
                   
   INSERT INTO SurveyMaster(                
   vcSurName,                        
   vcRedirectURL,                 
   numDomainID,                
   numCreatedBy,                
   bintCreatedOn,                
   numModifiedBy,                
   bintModifiedOn,                
   bitSkipRegistration,                
   bitRandomAnswers,                
   bitSinglePageSurvey,                
   bitPostRegistration,
   bitRelationshipProfile,
   bitSurveyRedirect,
   bitEmailTemplateContact,
   bitEmailTemplateRecordOwner,numRelationShipId,numEmailTemplate1Id,numEmailTemplate2Id,bitLandingPage,vcLandingPageYes,vcLandingPageNo,vcSurveyRedirect,tIntCRMType,numGrpId,numRecOwner,bitCreateRecord,numProfileId
   )                
   SELECT vcSurName, vcRedirectURL, @numDomainID, @numUserID,                
   getutcdate(), @numUserID, getutcdate(), bitSkipRegistration, bitRandomAnswers, bitSinglePageSurvey,  
   bitPostRegistration, bitRelationshipProfile,bitSurveyRedirect,bitEmailTemplateContact,bitEmailTemplateRecordOwner,
   numRelationShipId,numEmailTemplate1Id,numEmailTemplate2Id,bitLandingPage ,vcLandingPageYes,vcLandingPageNo,vcSurveyRedirect  ,tIntCRMType,numGrpId,numRecOwner,bitCreateRecord,numProfileId  
   FROM OpenXML(@iDoc, '/NewDataSet/SurveyMaster', 2)                        
   WITH                        
   (                
    vcSurName VARCHAR(100),                
    vcRedirectURL VARCHAR(225),                
    bitSkipRegistration Bit,                
    bitRandomAnswers Bit,                
    bitSinglePageSurvey Bit,                
    bitPostRegistration BIT,
    bitRelationshipProfile BIT, bitSurveyRedirect BIT,bitEmailTemplateContact BIT,bitEmailTemplateRecordOwner  BIT,
    numRelationShipId Numeric,numEmailTemplate1Id Numeric,numEmailTemplate2Id NUMERIC,bitLandingPage BIT,
    vcLandingPageYes VARCHAR(225),vcLandingPageNo   VARCHAR(225),vcSurveyRedirect VARCHAR(255) ,
    tIntCRMType tinyint,numGrpId Numeric,numRecOwner NUMERIC,bitCreateRecord BIT,numProfileId numeric      
   )                
                 
   SELECT @numSurID = SCOPE_IDENTITY()                  
                 
   INSERT INTO SurveyQuestionMaster(                 
   numQID,                       
   numSurID,                
   vcQuestion,                
   tIntAnsType,                
   intNumOfAns,boolMatrixColumn,intNumOfMatrixColumn           
   )                
   SELECT numQID, @numSurID as numSurID, vcQuestion, tIntAnsType, intNumOfAns,boolMatrixColumn,intNumOfMatrixColumn  FROM OpenXML(@iDoc, '/NewDataSet/SurveyQuestionMaster', 2)                        
   WITH                        
   (                
    numQID Numeric,                  
    vcQuestion VARCHAR(250),                
    tIntAnsType tinyInt,                
    intNumOfAns Int,                
    boolDeleted BIT,boolMatrixColumn BIT,intNumOfMatrixColumn INT
   )WHERE boolDeleted = 0                
                 
   INSERT INTO SurveyAnsMaster(                 
   numQID,                
   numAnsID,                       
   numSurID,                
   vcAnsLabel,                
   boolSurveyRuleAttached                
   )                
   SELECT numQID, numAnsID, @numSurID as numSurID, vcAnsLabel, boolSurveyRuleAttached FROM OpenXML(@iDoc, '/NewDataSet/SurveyAnsMaster', 2)                        
   WITH                        
   (                
    numQID Numeric,                  
    numAnsID Numeric,                  
    vcAnsLabel VARCHAR(100),                
    boolSurveyRuleAttached Bit,                
    boolDeleted Bit                
   )WHERE boolDeleted = 0                
                 
                 
   INSERT INTO SurveyWorkflowRules(                 
   numAnsID,                
   numRuleID,                
   numQID,                
   numSurID,                
   boolActivation,                
   numEventOrder,                
   vcQuestionList,          
   numLinkedSurID,                
   numResponseRating,                
   vcPopUpURL,                
   numGrpId,                
   numCompanyType,                
   tIntCRMType,                
   numRecOwner  ,
tIntRuleFourRadio,
tIntCreateNewID,
vcRuleFourSelectFields,
boolPopulateSalesOpportunity,
vcRuleFiveSelectFields,numMatrixID,
vcItem
              
   )             
   SELECT numAnsID, numRuleID, numQID, @numSurID as numSurID, boolActivation,                
   numEventOrder, vcQuestionList, numLinkedSurID, numResponseRating, vcPopUpURL,                
   numGrpId, numCompanyType, tIntCRMType, numRecOwner , tIntRuleFourRadio,tIntCreateNewID,vcRuleFourSelectFields,
boolPopulateSalesOpportunity, vcRuleFiveSelectFields,numMatrixID,vcItem FROM OpenXML(@iDoc, '/NewDataSet/SurveyWorkflowRules', 2)                        
   WITH                        
   (                
    numAnsID Numeric,                   
    numRuleID Numeric,                
    numQID Numeric,                 
    numSurID Numeric,          
    boolActivation Bit,                
    numEventOrder Int,                
    vcQuestionList Nvarchar(50),                
    numLinkedSurID Float,               
    numResponseRating tinyInt,                 
    vcPopUpURL Nvarchar(100),                
    numGrpId Numeric,                  
    numCompanyType Numeric,                
    tIntCRMType tinyInt,                 
    numRecOwner Numeric,
	tIntRuleFourRadio tinyint,
	tIntCreateNewID tinyint,
	vcRuleFourSelectFields Nvarchar(500),
	boolPopulateSalesOpportunity Bit,
	vcRuleFiveSelectFields Nvarchar(500),
	vcItem Nvarchar(500),numMatrixID Numeric             
   ) WHERE boolActivation = 1                
         
  INSERT INTO SurveyMatrixAnsMaster(                 
   numQID,                
   numMatrixID,                       
   numSurID,                
   vcAnsLabel,                
   boolSurveyRuleAttached                
   )                
   SELECT numQID, numMatrixID, @numSurID as numSurID, vcAnsLabel, boolSurveyRuleAttached FROM OpenXML(@iDoc, '/NewDataSet/SurveyMatrixMaster', 2)                        
   WITH                        
   (                
    numQID Numeric,                  
    numMatrixID Numeric,                  
    vcAnsLabel VARCHAR(100),                
    boolSurveyRuleAttached Bit,                
    boolDeleted Bit                
   )WHERE boolDeleted = 0     
       
   INSERT INTO SurveyCreateRecord(                 
   numSurID,                
   numCreateRecordId, numRatingMin ,numRatingMax,                    
   numRelationShipId,  numGrpId     ,numRecOwner,numProfileId,tIntCRMType,numFollowUpStatus,bitDefault    
   )                
   SELECT @numSurID as numSurID,numCreateRecordId,numRatingMin,numRatingMax,numRelationShipId,
    numGrpId,numRecOwner,numProfileId,tIntCRMType,numFollowUpStatus,bitDefault
    FROM OpenXML(@iDoc, '/NewDataSet/SurveyCreateRecord', 2)                        
   WITH                        
   (                
    numCreateRecordId Numeric,                  
    numRatingMin Numeric,                  
    numRatingMax Numeric,                
    numRelationShipId Numeric,                
    numGrpId Numeric,                
    numRecOwner NUMERIC,tIntCRMType TINYINT,numProfileId NUMERIC,numFollowUpStatus NUMERIC,bitDefault bit        
   )
       
   EXECUTE sp_xml_removedocument @iDoc                  
   --Return the New ID.                  
   SELECT @numSurID               
  END                 
 Else              
   BEGIN              
   EXECUTE sp_xml_preparedocument @iDoc OUTPUT, @vcSurveyInformation                    
               
   UPDATE SurveyMaster              
   SET SurveyMaster.vcSurName = XMLTABLE.vcSurName,               
   SurveyMaster.vcRedirectURL = XMLTABLE.vcRedirectURL,               
   SurveyMaster.numModifiedBy = @numUserID,               
   SurveyMaster.bintModifiedOn =  getutcdate(),               
   SurveyMaster.bitSkipRegistration = XMLTABLE.bitSkipRegistration,                
   SurveyMaster.bitRandomAnswers = XMLTABLE.bitRandomAnswers,                
   SurveyMaster.bitSinglePageSurvey = XMLTABLE.bitSinglePageSurvey,                
   SurveyMaster.bitPostRegistration = XMLTABLE.bitPostRegistration,
   SurveyMaster.bitRelationshipProfile=XMLTABLE.bitRelationshipProfile,
   SurveyMaster.bitSurveyRedirect=XMLTABLE.bitSurveyRedirect,
   SurveyMaster.bitEmailTemplateContact=XMLTABLE.bitEmailTemplateContact,
   SurveyMaster.bitEmailTemplateRecordOwner=XMLTABLE.bitEmailTemplateRecordOwner,
   SurveyMaster.numRelationShipId=XMLTABLE.numRelationShipId,
   SurveyMaster.numEmailTemplate1Id=XMLTABLE.numEmailTemplate1Id,
   SurveyMaster.numEmailTemplate2Id=XMLTABLE.numEmailTemplate2Id,
   SurveyMaster.bitLandingPage=XMLTABLE.bitLandingPage
   ,SurveyMaster.vcLandingPageYes=XMLTABLE.vcLandingPageYes,
   SurveyMaster.vcLandingPageNo =XMLTABLE.vcLandingPageNo,
   SurveyMaster.vcSurveyRedirect=XMLTABLE.vcSurveyRedirect,
   SurveyMaster.tIntCRMType=XMLTABLE.tIntCRMType,
   SurveyMaster.numGrpId=XMLTABLE.numGrpId,
   SurveyMaster.numRecOwner=XMLTABLE.numRecOwner,
   SurveyMaster.bitCreateRecord=XMLTABLE.bitCreateRecord,
   SurveyMaster.numProfileId=XMLTABLE.numProfileId
   FROM OPENXML(@iDoc, '/NewDataSet/SurveyMaster', 2)               
   WITH SurveyMaster XMLTABLE              
   WHERE SurveyMaster.numSurID = @numSurID              
              
                
   DELETE FROM SurveyQuestionMaster Where numSurID = @numSurID              
                 
   INSERT INTO SurveyQuestionMaster(                 
   numQID,                       
   numSurID,                
   vcQuestion,                
   tIntAnsType,                
   intNumOfAns,boolMatrixColumn,intNumOfMatrixColumn               
   )                 
   SELECT numQID, @numSurID as numSurID, vcQuestion, tIntAnsType, intNumOfAns,boolMatrixColumn,intNumOfMatrixColumn  FROM OpenXML(@iDoc, '/NewDataSet/SurveyQuestionMaster', 2)                        
   WITH                        
   (                
    numQID Numeric,                  
    vcQuestion VARCHAR(250),                
    tIntAnsType tinyInt,                
    intNumOfAns Int,                
    boolDeleted BIT,boolMatrixColumn BIT,intNumOfMatrixColumn INT
   )WHERE boolDeleted = 0                
                 
   DELETE FROM SurveyAnsMaster Where numSurID = @numSurID              
              
   INSERT INTO SurveyAnsMaster(                 
   numQID,                
   numAnsID,                       
   numSurID,                
   vcAnsLabel,                
   boolSurveyRuleAttached                
   )                 
   SELECT numQID, numAnsID, @numSurID as numSurID, vcAnsLabel, boolSurveyRuleAttached FROM OpenXML(@iDoc, '/NewDataSet/SurveyAnsMaster', 2)                        
   WITH                        
   (                
    numQID Numeric,                  
    numAnsID Numeric,                  
    vcAnsLabel VARCHAR(100),                
    boolSurveyRuleAttached Bit,                
    boolDeleted Bit                
   )WHERE boolDeleted = 0                
                 
                 
   DELETE FROM SurveyWorkflowRules WHERE numSurId = @numSurId            
            
   INSERT INTO SurveyWorkflowRules(                 
   numAnsID,                
   numRuleID,                
   numQID,                
   numSurID,                
   boolActivation,                
   numEventOrder,                
   vcQuestionList,          
   numLinkedSurID,                
   numResponseRating,                
   vcPopUpURL,                
   numGrpId,               
   numCompanyType,                
   tIntCRMType,                
   numRecOwner,
tIntRuleFourRadio,
tIntCreateNewID,
vcRuleFourSelectFields,
boolPopulateSalesOpportunity,
vcRuleFiveSelectFields,numMatrixID,
vcItem                
   )                 
   SELECT numAnsID, numRuleID, numQID, @numSurID as numSurID, boolActivation,                
   numEventOrder, vcQuestionList, numLinkedSurID, numResponseRating, vcPopUpURL,                
   numGrpId, numCompanyType, tIntCRMType, numRecOwner, tIntRuleFourRadio,tIntCreateNewID,vcRuleFourSelectFields,
boolPopulateSalesOpportunity, vcRuleFiveSelectFields,numMatrixID,vcItem FROM OpenXML(@iDoc, '/NewDataSet/SurveyWorkflowRules', 2)                        
   WITH                        
   (                
    numAnsID Numeric,                   
    numRuleID Numeric,                
    numQID Numeric,                 
    numSurID Numeric,          
    boolActivation Bit,                
    numEventOrder Int,                
    vcQuestionList Nvarchar(50),                
    numLinkedSurID Float,               
    numResponseRating tinyInt,                 
    vcPopUpURL Nvarchar(100),                
    numGrpId Numeric,                  
    numCompanyType Numeric,        
    tIntCRMType tinyInt,                 
    numRecOwner Numeric,
tIntRuleFourRadio tinyint,
	tIntCreateNewID tinyint,
	vcRuleFourSelectFields Nvarchar(500),
	boolPopulateSalesOpportunity Bit,
	vcRuleFiveSelectFields Nvarchar(500),
	vcItem Nvarchar(500),numMatrixID Numeric                   
   ) WHERE boolActivation = 1                  
          
      
   DELETE FROM SurveyMatrixAnsMaster Where numSurID = @numSurID              
              
   INSERT INTO SurveyMatrixAnsMaster(                 
   numQID,                
   numMatrixID,                       
   numSurID,                
   vcAnsLabel,                
   boolSurveyRuleAttached                
   )                 
   SELECT numQID, numMatrixID, @numSurID as numSurID, vcAnsLabel, boolSurveyRuleAttached FROM OpenXML(@iDoc, '/NewDataSet/SurveyMatrixMaster', 2)                        
   WITH                        
   (                
    numQID Numeric,                  
    numMatrixID Numeric,                  
    vcAnsLabel VARCHAR(100),                
    boolSurveyRuleAttached Bit,                
    boolDeleted Bit                
   )WHERE boolDeleted = 0 
    
     DELETE FROM SurveyCreateRecord Where numSurID = @numSurID              
    
    
      INSERT INTO SurveyCreateRecord(                 
   numSurID,                
   numCreateRecordId, numRatingMin ,numRatingMax,                    
   numRelationShipId,  numGrpId     ,numRecOwner,numProfileId,tIntCRMType,numFollowUpStatus ,bitDefault     
   )                
   SELECT @numSurID as numSurID,numCreateRecordId,numRatingMin,numRatingMax,numRelationShipId,
    numGrpId,numRecOwner,numProfileId,tIntCRMType,numFollowUpStatus,bitDefault
    FROM OpenXML(@iDoc, '/NewDataSet/SurveyCreateRecord', 2)                        
   WITH                        
   (                
    numCreateRecordId Numeric,                  
    numRatingMin Numeric,                  
    numRatingMax Numeric,                
    numRelationShipId Numeric,                
    numGrpId Numeric,                
    numRecOwner NUMERIC,tIntCRMType TINYINT,numProfileId NUMERIC,numFollowUpStatus NUMERIC,bitDefault bit            
   )

   EXECUTE sp_xml_removedocument @iDoc                  
   --Return the New ID.                  
   SELECT @numSurID               
   END               
 END
