--created by anoop jayaraj            
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getcourses')
DROP PROCEDURE usp_getcourses
GO
CREATE PROCEDURE [dbo].[USP_GetCourses]            
@numDomainID as numeric(9)=0,            
@numCountry as numeric(9)=0,            
@numState as numeric(9)=0,            
@SearchText as varchar(100)='',            
@FromDate as datetime=null,            
@ToDate as datetime=null,            
@Miles as integer=0,            
@ZipCode as varchar(15)='',          
@numCategory as numeric(9)=0,      
@byteMode as tinyint,      
@ItemID as numeric(9)=0 ,      
@ModelID as varchar(200),      
@strWarehouse as varchar(100),      
@strState as varchar(2),      
@chapter as varchar(50),      
@subchapter as varchar(50)       
            
as          
          
If @FromDate='1753-01-01 00:00:00:000' set @FromDate=null          
If @ToDate='1753-01-01 00:00:00:000' set @ToDate=null          
          
declare @strSQL as varchar(2000)       
      
      
If @byteMode=0      
begin      
      
 set @strSQL='      
    with HierarchyTable (numCategoryID, ParentID, name) as       
 (select numCategoryID, numDepCategory, vcCategoryName      
 from Category      
 where numCategoryID = '+ convert(varchar(20),@numCategory)+'      
 union all       
 select Category.numCategoryID, Category.numDepCategory ,Category.vcCategoryName      
 from Category       
 inner join HierarchyTable       
 on Category.numDepCategory = HierarchyTable.numCategoryID)      
    select distinct(vcModelID) as CourseCode,vcItemName as Title,          
 monWListPrice  from Item I            
 Join WareHouseItems WI            
 on WI.numItemID=I.numItemCode            
 Join Warehouses W            
 on W.numWareHouseID=WI.numWareHouseID      
    join   RemarketDTL R      
    on  R.numItemID=I.numItemCode and R.numWareHouseItemID=WI.numWareHouseItemID      
 where I.numDomainID=' + convert(varchar(20),@numDomainID)          
           
            
 if @numCountry>0 set @strSQL=@strSQL+' and W.numWCountry='+ convert(varchar(20),@numCountry)          
 if  @numState>0  set @strSQL=@strSQL+' and W.numWState='+ convert(varchar(20),@numState)          
 if  @FromDate is not null and @ToDate is null  set @strSQL=@strSQL+' and numItemCode in (select RecId from CFW_FLD_Values_Item where Fld_ID=150 and Fld_Value <='''+ convert(varchar(20),@FromDate)+''')'         
 if  @FromDate is null and @ToDate is not  null  set @strSQL=@strSQL+' and numItemCode in (select RecId from CFW_FLD_Values_Item where Fld_ID=151 and Fld_Value >='''+ convert(varchar(20),@ToDate)+''')'      
 if  @FromDate is not null and @ToDate is not  null  set @strSQL=@strSQL +' and numItemCode in (select RecId from CFW_FLD_Values_Item where ((Fld_ID=151 and Fld_Value >='''+ convert(varchar(20),@ToDate)+''') or (Fld_ID=150 and Fld_Value <='''+   
convert(varchar(20),@FromDate)+''')) group by RecId having count(*)>1)'      
 if  @SearchText <>''  set @strSQL=@strSQL+' and ( vcSKU like ''%'+ @SearchText +'%'' or vcModelID like ''%'+ @SearchText +'%'' or vcItemName like ''%'+ @SearchText +'%'' or txtItemDesc like ''%'+ @SearchText +'%'')'          
 If @Miles <> 0 And @ZipCode <> ''           
  begin          
   If IsNumeric(@ZipCode)=1  set @strSQL=@strSQL+' and vcWPinCode in (SELECT * FROM dbo.funZips(' + @ZipCode + ',' + convert(varchar(20),@Miles) + '))'          
  end         
 if @numCategory>0 set @strSQL=@strSQL+' and numItemCode in (select numItemID from ItemCategory where numCategoryID in (select numCategoryID from HierarchyTable))'          
 set @strSQL=@strSQL+' order by vcModelID'      
    print @strSQL      
 exec (@strSQL)      
      
      
end      
else If @byteMode=1      
begin      
 set @strSQL='Select * from(select numItemCode,WI.numWareHouseItemID,vcSKU as vcClassNum,vcModelID as CourseCode,vcItemName as Title,vcWarehouse,          
 convert(varchar(11),cast(dbo.GetCustFldValue(150,5,numItemCode) as datetime),100)+'' - ''+ convert(varchar(11),cast(dbo.GetCustFldValue(151,5,numItemCode) as datetime),100) as CourseDate,          
 dbo.GetListIemName(dbo.GetCustFldValue(153,5,numItemCode)) as CLASSAT,vcWCity as CITY,dbo.fn_GetState(numWState) as STATE,monWListPrice,vcWareHouse as FACNAME  from Item I            
 Join WareHouseItems WI            
 on WI.numItemID=I.numItemCode            
 Join Warehouses W       
 on W.numWareHouseID=WI.numWareHouseID       
     join   RemarketDTL R      
    on  R.numItemID=I.numItemCode   and R.numWareHouseItemID=WI.numWareHouseItemID       
 where I.numDomainID=' + convert(varchar(20),@numDomainID) +       
 'and I.vcModelID='''+ @ModelID +''''         
           
            
 if @numCountry>0 set @strSQL=@strSQL+' and W.numWCountry='+ convert(varchar(20),@numCountry)          
 if  @numState>0  set @strSQL=@strSQL+' and W.numWState='+ convert(varchar(20),@numState)          
 if  @FromDate is not null and @ToDate is null  set @strSQL=@strSQL+' and numItemCode in (select RecId from CFW_FLD_Values_Item where Fld_ID=150 and Fld_Value <='''+ convert(varchar(20),@FromDate)+''')'         
 if  @FromDate is null and @ToDate is not  null  set @strSQL=@strSQL+' and numItemCode in (select RecId from CFW_FLD_Values_Item where Fld_ID=151 and Fld_Value >='''+ convert(varchar(20),@ToDate)+''')'      
 if  @FromDate is not null and @ToDate is not  null  set @strSQL=@strSQL +' and numItemCode in (select RecId from CFW_FLD_Values_Item where ((Fld_ID=151 and Fld_Value >='''+ convert(varchar(20),@ToDate)+''') or (Fld_ID=150 and Fld_Value <='''+     
convert(varchar(20),@FromDate)+'''))  group by RecId having count(*)>1 )'      
 if  @SearchText <>''  set @strSQL=@strSQL+' and ( vcSKU like ''%'+ @SearchText +'%'' or vcModelID like ''%'+ @SearchText +'%'' or vcItemName like ''%'+ @SearchText +'%'' or txtItemDesc like ''%'+ @SearchText +'%'')'          
 If @Miles <> 0 And @ZipCode <> ''           
  begin          
   If IsNumeric(@ZipCode)=1  set @strSQL=@strSQL+' and vcWPinCode in (SELECT * FROM dbo.funZips(' + @ZipCode + ',' + convert(varchar(20),@Miles) + '))'          
  end         
 if @numCategory>0 set @strSQL=@strSQL+' and numItemCode in (select numItemID from ItemCategory where numCategoryID='+ convert(varchar(20),@numCategory) +')'          
 set @strSQL=@strSQL+' )X order by X.CourseCode,cast(dbo.GetCustFldValue(151,5,X.numItemCode) as datetime),X.CITY Asc'      
    exec (@strSQL)      
end       
else If @byteMode=2      
begin      
 set @strSQL='Select *,X.[Start Date]+'' - ''+X.[End Date] as CourseDate from(select numItemCode,WI.numWareHouseItemID,vcSKU as vcClassNum,vcModelID as CourseCode,vcItemName as Title,vcWarehouse,          
 convert(varchar(11),cast(dbo.GetCustFldValue(150,5,numItemCode) as datetime),100) as [Start Date],convert(varchar(11),cast(dbo.GetCustFldValue(151,5,numItemCode) as datetime),100) as [End Date],          
 dbo.GetListIemName(dbo.GetCustFldValue(153,5,numItemCode)) as CLASSAT,vcWCity as CITY,dbo.fn_GetState(numWState) as STATE,monWListPrice,vcWareHouse as FACNAME  from Item I            
 Join WareHouseItems WI            
 on WI.numItemID=I.numItemCode            
 Join Warehouses W            
 on W.numWareHouseID=WI.numWareHouseID       
    join   RemarketDTL R      
    on  R.numItemID=I.numItemCode    and R.numWareHouseItemID=WI.numWareHouseItemID      
 where I.numDomainID=' + convert(varchar(20),@numDomainID)       
      
      
 if  @ModelID<>''  set @strSQL=@strSQL+' and I.vcModelID='''+ @ModelID +''''       
    if  @chapter<>''  set @strSQL=@strSQL+' and dbo.GetCustFldValue(133,5,numItemCode) in (select numListItemID from ListDetails where  numListId=118 and vcData like ''%' + convert(varchar(100),@chapter) +'%'' and numDomainID=' +     
convert(varchar(20),@numDomainID) +')'       
 if  @subchapter<>''  set @strSQL=@strSQL+' and dbo.GetCustFldValue(134,5,numItemCode) in (select numListItemID from ListDetails where numListId=119 and  vcData like ''%' + convert(varchar(100),@subchapter) +'%'' and numDomainID=' +     
convert(varchar(20),@numDomainID) +')'       
 if  @strState<>''  set @strSQL=@strSQL+' and W.numWState in (select numStateID from State where vcState=''' + convert(varchar(2),@strState) +''' and numDomainID=' + convert(varchar(20),@numDomainID) +')'       
    if  @strWarehouse<>''  set @strSQL=@strSQL+' and W.vcWarehouse like  ''%' + convert(varchar(100),@strWarehouse) +'%'''   
 if  @FromDate is not null and @ToDate is null  set @strSQL=@strSQL+' and numItemCode in (select RecId from CFW_FLD_Values_Item where Fld_ID=150 and Fld_Value <='''+ convert(varchar(20),@FromDate)+''')'         
 if  @FromDate is null and @ToDate is not  null  set @strSQL=@strSQL+' and numItemCode in (select RecId from CFW_FLD_Values_Item where Fld_ID=151 and Fld_Value >='''+ convert(varchar(20),@ToDate)+''')'      
 if  @FromDate is not null and @ToDate is not  null  set @strSQL=@strSQL +' and numItemCode in (select RecId from CFW_FLD_Values_Item where ((Fld_ID=151 and Fld_Value >='''+ convert(varchar(20),@ToDate)+''') or (Fld_ID=150 and Fld_Value <='''+     
convert(varchar(20),@FromDate)+'''))  group by RecId having count(*)>1 )'      
       
 if @numCategory>0 set @strSQL=@strSQL+' and numItemCode in (select numItemID from ItemCategory where numCategoryID='+ convert(varchar(20),@numCategory) +')'          
 set @strSQL=@strSQL+' )X order by X.CourseCode,X.[Start Date],X.CITY Asc'      
    print @strSQL      
 exec (@strSQL)      
end  