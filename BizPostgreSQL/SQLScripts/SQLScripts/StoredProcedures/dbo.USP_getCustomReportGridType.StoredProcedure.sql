/****** Object:  StoredProcedure [dbo].[USP_getCustomReportGridType]    Script Date: 07/26/2008 16:17:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by Ganga  
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getcustomreportgridtype')
DROP PROCEDURE usp_getcustomreportgridtype
GO
CREATE PROCEDURE [dbo].[USP_getCustomReportGridType]     
@numUserCntID as numeric(9),    
@numDomainId as numeric(9),
@bitGridType as bit    
as 

    
select vcReportName,numcustomReportId as numReportId from customreport   
where numCreatedBy = @numUserCntID and numDomainId = @numDomainId and bitGridType=@bitGridType
GO
