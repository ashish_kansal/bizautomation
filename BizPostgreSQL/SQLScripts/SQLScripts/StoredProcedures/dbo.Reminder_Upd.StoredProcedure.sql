/****** Object:  StoredProcedure [dbo].[Reminder_Upd]    Script Date: 07/26/2008 16:14:33 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
/*
**  Updates an Reminder notification information on an individual activity,
**  such as for instance, when the LoggedOnUserName chooses to Snooze or
**  ultimately Dismiss the Reminder.
*/
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='reminder_upd')
DROP PROCEDURE reminder_upd
GO
CREATE PROCEDURE [dbo].[Reminder_Upd]
	@Status		integer,		-- the new status of the Reminder notification for this Activity
						-- N.B., since OLE DB shares this ParameterEntry[] and it offers
						--      positional parameters only, the Status argument must pre-
						--      cede the ActivityID in params as it appears in the SQL.
	@ActivityID	integer			-- the key of the Activity to be updated
AS
BEGIN
	UPDATE
		[Activity]
	SET 
		[Status] = @Status,
		[EnableReminder] = 0 
	WHERE
		( ( [Activity].[ActivityID] = @ActivityID ) AND
		  ( [Activity].[RecurrenceID] = (-999) ) );
END
GO
