GO
/****** Object:  StoredProcedure [dbo].[usp_getCompanyAssets]    Script Date: 03/05/2010 17:41:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getcompanyassets')
DROP PROCEDURE usp_getcompanyassets
GO
CREATE PROCEDURE  [dbo].[usp_getCompanyAssets]
@numDivId as numeric(18),
@numDomainId as numeric(18),
@CurrentPage as numeric(18),
@TotRecs int output,
@numItemClassification numeric(18)=0,
@vcKeyWord varchar(50)='',
@vcSortColumn varchar(50)='',
@vcSort varchar(10)='',
@vcSortChar varchar(1)='0',
@PageSize INT=20
as 

create table #Temp_CompAsset
(ID INT IDENTITY PRIMARY KEY,
numItemCode numeric(18,0),
vcItemName varchar(200),
vcModelId varchar(100),
txtItemDesc VARCHAR(2000),
vcSKU varchar(100),
numBarCodeId varchar(50),
Department varchar(100),
dtPurchase datetime,
dtWarrentyTill datetime,
vcPathForTImage varchar(250),
Vendor varchar(250),
Cost DECIMAL(20,5))


declare @strSql varchar(2000);
--kishan
set @strSql=
'SELECT 
IT.numItemCode,
IT.vcItemName,
isnull(vcModelId,''''),
isnull(txtItemDesc,'''') as txtItemDesc, 
isnull(vcSKU,'''') as vcSKU,
isnull( cast(numBarCodeId as varchar(50)),'''') as numBarCodeId,
isnull(dbo.fn_GetListItemName(numDeptId),'''') as Department,
dtPurchase as dtPurchase ,
dtWarrentyTill as dtWarrentyTill,	
isnull(
  (SELECT TOP 1 II.vcPathForTImage 
    FROM dbo.ItemImages II 
        WHERE II.numItemCode = IT.numItemCode 
    AND II.bitDefault = 1 
    AND II.numDomainID ='+cast(@numDomainId as varchar(50))+'),'''')   as vcPathForTImage,
dbo.fn_GetComapnyName(IT.numVendorID) as Vendor,
isnull(V.monCost,0) as Cost
FROM companyassets CA inner join Item IT
on IT.numItemcode=CA.numItemcode 
left outer join DivisionMaster DV on dv.numDivisionId=numVendorID 
left outer join CompanyInfo CI on CI.numCompanyId=DV.numCompanyId
left outer join Vendor V  on V.numDomainId = CA.numDomainID and v.numVendorID=IT.numVendorID and V.numItemCode=IT.numItemCode 
 where (numOppId is null or numOppId =0) and 
It.numDomainId=CA.numDomainID'
if @numDivId<>0 set @strSql=@strSql + ' and CA.numDivId=' + cast(@numDivId as varchar(50))
if @numItemClassification<>0 set @strSql=@strSql + ' and IT.numItemClassification=' + cast(@numItemClassification as varchar(50))
set @strSql=@strSql + ' and It.numDomainID=' + cast(@numDomainId as varchar(50))
if @vcSortChar<>'0' set @strSql=@strSql + ' and vcItemName like ''' + @vcSortChar + '%'''
if @vcKeyWord<>'' set @strSql=@strSql + ' and ' + @vcKeyWord
if @vcSortColumn<>'' set @strSql=@strSql + ' order by ' + @vcSortColumn
if @vcSort<>'' and @vcSortColumn <>'' set @strSql=@strSql + ' ' + @vcSort
--SELECT * FROM #Temp_CompAsset 

print @strSql;

--select * from Companyinfo

insert into #Temp_CompAsset exec(@strSql);


declare @firstRec as integer                     
declare @lastRec as integer                    




 set @firstRec= (@CurrentPage-1) * @PageSize                    
     set @lastRec= (@CurrentPage*@PageSize+1)       
   

select * from #Temp_CompAsset where ID > @firstRec and ID < @lastRec                    

SeLECT numAssetItemId,isnull(vcSerialNo,'') as vcSerialNo, isnull(vcModelId,'') as vcModelId,
isnull(CAST(numBarCodeId AS VARCHAR(100)),'') as vcBarCodeId, dtPurchase,dtWarrante,vcLocation
from CompanyAssetSerial where numAssetItemId in (select numItemCode from #Temp_CompAsset where ID > @firstRec and ID < @lastRec );

set @TotRecs=(select count(*) from #Temp_CompAsset) 

DROP TABLE #Temp_CompAsset;