GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_GetPageNavigationAuthorizationDetails' ) 
    DROP PROCEDURE USP_GetPageNavigationAuthorizationDetails
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- EXEC USP_GetPageNavigationAuthorizationDetails 1,1
CREATE PROCEDURE USP_GetPageNavigationAuthorizationDetails
    (
      @numGroupID NUMERIC(18, 0),
      @numTabID NUMERIC(18, 0),
      @numDomainID NUMERIC(18, 0)
    )
AS 
    SET NOCOUNT ON
    SET TRANSACTION ISOLATION LEVEL READ COMMITTED
  
    BEGIN
		/*
		SELECT * FROM dbo.TabMaster
		SELECT * FROM dbo.ModuleMaster
		SELECT * FROM 
		*/
        PRINT @numTabID 
        DECLARE @numModuleID AS NUMERIC(18, 0)
        SELECT TOP 1
                @numModuleID = numModuleID
        FROM    dbo.PageNavigationDTL
        WHERE   numTabID = @numTabID 
        PRINT @numModuleID
        
        IF @numModuleID = 10 
            BEGIN
                PRINT 10
                SELECT  *
                FROM    ( SELECT    ISNULL(( SELECT TOP 1
                                                    TNA.[numPageAuthID]
                                             FROM   dbo.TreeNavigationAuthorization TNA
                                             WHERE  TNA.numPageNavID = PND.[numPageNavID]
                                                    AND TNA.numDomainID = @numDomainID
                                                    AND TNA.numGroupID = @numGroupID
                                                    AND TNA.numTabID = @numTabID
                                           ), 0) AS [numPageAuthID],
                                    ISNULL(@numGroupID, 0) AS [numGroupID],
                                    ISNULL(PND.[numTabID], 0) AS [numTabID],
                                    ISNULL(PND.[numPageNavID], 0) AS [numPageNavID],
                                    ISNULL(( SELECT TOP 1
                                                    TNA.[bitVisible]
                                             FROM   dbo.TreeNavigationAuthorization TNA
                                             WHERE  TNA.numPageNavID = PND.[numPageNavID]
                                                    AND TNA.numDomainID = @numDomainID
                                                    AND TNA.numGroupID = @numGroupID
                                                    AND TNA.numTabID = @numTabID
                                           ), 0) AS [bitVisible],
                                    @numDomainID AS [numDomainID],
                                    ( SELECT TOP 1
                                                vcPageNavName
                                      FROM      dbo.PageNavigationDTL
                                      WHERE     PageNavigationDTL.numPageNavID = PND.numPageNavID
                                    ) AS [vcNodeName],
                                    ISNULL(( SELECT TOP 1
                                                    TNA.tintType
                                             FROM   dbo.TreeNavigationAuthorization TNA
                                             WHERE  TNA.numPageNavID = PND.[numPageNavID]
                                                    AND TNA.numDomainID = @numDomainID
                                                    AND TNA.numGroupID = @numGroupID
                                                    AND TNA.numTabID = @numTabID
                                           ), 1) AS [tintType]
                          FROM      dbo.PageNavigationDTL PND
                          WHERE     ISNULL(PND.bitVisible, 0) = 1
                                    AND PND.numTabID = @numTabID
                                    AND numModuleID = @numModuleID
                          UNION ALL
                          SELECT    ISNULL(( SELECT TOP 1
                                                    TNA.[numPageAuthID]
                                             FROM   dbo.TreeNavigationAuthorization TNA
                                             WHERE  TNA.numPageNavID = LD.[numListItemID]
                                                    AND TNA.numDomainID = @numDomainID
                                                    AND TNA.numGroupID = @numGroupID
                                                    AND TNA.numTabID = @numTabID
                                           ), 0) AS [numPageAuthID],
                                    @numGroupID AS [numGroupID],
                                    1 AS [numTabID],
                                    ISNULL(LD.[numListItemID], 0) AS [numPageNavID],
                                    ISNULL(( SELECT TOP 1
                                                    TNA.[bitVisible]
                                             FROM   dbo.TreeNavigationAuthorization TNA
                                             WHERE  TNA.numPageNavID = LD.[numListItemID]
                                                    AND TNA.numDomainID = @numDomainID
                                                    AND TNA.numGroupID = @numGroupID
                                                    AND TNA.numTabID = @numTabID
                                           ), 0) AS [bitVisible],
                                    ISNULL(LD.[numDomainID], 0) AS [numDomainID],
                                    ISNULL(vcData, '') + 's' AS [vcNodeName],
                                    ISNULL(( SELECT TOP 1
                                                    TNA.tintType
                                             FROM   dbo.TreeNavigationAuthorization TNA
                                             WHERE  TNA.numPageNavID = LD.[numListItemID]
                                                    AND TNA.numDomainID = @numDomainID
                                                    AND TNA.numGroupID = @numGroupID
                                                    AND TNA.numTabID = @numTabID
                                           ), 1) AS [tintType]
                          FROM      dbo.ListDetails LD
						--LEFT JOIN dbo.TreeNavigationAuthorization TNA ON LD.numListItemID = TNA.numPageNavID 
                          WHERE     numListID = 27
                                    AND LD.numDomainID = @numDomainID
                                    AND ISNULL(constFlag, 0) = 0
						--AND ( numTabID = (CASE WHEN @numModuleID = 10 THEN 1 ELSE 0 END) OR ISNULL(numTabID,0) = 0)
                          UNION ALL
                          SELECT    ISNULL(( SELECT TNA.[numPageAuthID]
                                             FROM   dbo.TreeNavigationAuthorization TNA
                                             WHERE  TNA.numPageNavID = LD.[numListItemID]
                                                    AND TNA.numDomainID = @numDomainID
                                                    AND TNA.numGroupID = @numGroupID
                                                    AND TNA.numTabID = @numTabID
                                           ), 0) AS [numPageAuthID],
                                    @numGroupID AS [numGroupID],
                                    1 AS [numTabID],
                                    ISNULL(LD.[numListItemID], 0) AS [numPageNavID],
                                    ISNULL(( SELECT TNA.[bitVisible]
                                             FROM   dbo.TreeNavigationAuthorization TNA
                                             WHERE  TNA.numPageNavID = LD.[numListItemID]
                                                    AND TNA.numDomainID = @numDomainID
                                                    AND TNA.numGroupID = @numGroupID
                                                    AND TNA.numTabID = @numTabID
                                           ), 0) AS [bitVisible],
                                    @numDomainID AS [numDomainID],
                                    ISNULL(vcData, '') + 's' AS [vcNodeName],
                                    ISNULL(( SELECT TNA.tintType
                                             FROM   dbo.TreeNavigationAuthorization TNA
                                             WHERE  TNA.numPageNavID = LD.[numListItemID]
                                                    AND TNA.numDomainID = @numDomainID
                                                    AND TNA.numGroupID = @numGroupID
                                                    AND TNA.numTabID = @numTabID
                                           ), 1) AS [tintType]
                          FROM      dbo.ListDetails LD
						--LEFT JOIN dbo.TreeNavigationAuthorization TNA ON LD.numListItemID = TNA.numPageNavID AND ISNULL(TNA.[numDomainID], 0) = @numDomainID
                          WHERE     numListID = 27
                                    AND ISNULL(constFlag, 0) = 1
                        ) TABLE1
                ORDER BY numPageNavID
            END
		
        IF @numModuleID = 14 
            BEGIN
                IF NOT EXISTS ( SELECT  *
                                FROM    TreeNavigationAuthorization
                                WHERE   numTabID = 8
                                        AND numDomainID = 1
                                        AND numGroupID = 1 ) 
                    BEGIN
                        SELECT  TNA.numPageAuthID,
                                TNA.numGroupID,
                                TNA.numTabID,
                                TNA.numPageNavID,
                                TNA.bitVisible,
                                TNA.numDomainID,
                                ISNULL(PND.vcPageNavName, '') AS [vcNodeName],
                                tintType
                        FROM    PageNavigationDTL PND
                                JOIN dbo.TreeNavigationAuthorization TNA ON PND.numPageNavID = TNA.numPageNavID  --AND PND.numTabID = TNA.numTabID       
                        WHERE   1 = 1
                                --AND ISNULL(TNA.bitVisible, 0) = 1
                                AND ISNULL(PND.bitVisible, 0) = 1
                                AND numModuleID = @numModuleID
                                AND TNA.numDomainID = @numDomainID
                                AND numGroupID = @numGroupID
                        UNION
                        SELECT  0 numPageAuthID,
                                @numGroupID numGroupID,
                                @numTabID numTabID,
                                1111 numPageNavID,
                                1 bitVisible,
                                @numDomainID numDomainID,
                                'Regular Documents' AS [vcNodeName],
                                1 tintType
                        UNION
                        SELECT  0 numPageAuthID,
                                @numGroupID numGroupID,
                                @numTabID numTabID,
                                Ld.numListItemId,
                                1 bitVisible,
                                @numDomainID numDomainID,
                                ISNULL(vcData, '') AS [vcNodeName],
                                1 tintType
                        FROM    listdetails Ld
                                LEFT JOIN listorder LO ON Ld.numListItemID = LO.numListItemID
                                                          AND Lo.numDomainId = @numDomainID
                        WHERE   Ld.numListID = 29
                                AND ( constFlag = 1
                                      OR Ld.numDomainID = @numDomainID
                                    )
                        UNION
                        SELECT  0 numPageAuthID,
                                @numGroupID numGroupID,
                                @numTabID numTabID,
                                ld.numListItemID numPageNavID,
                                1 bitVisible,
                                @numDomainID numDomainID,
                                ISNULL(vcData, '') AS [vcNodeName],
                                1 tintType
                        FROM    listdetails Ld
                                LEFT JOIN listorder LO ON Ld.numListItemID = LO.numListItemID
                                                          AND lo.numDomainId = @numDomainID
                                INNER JOIN AuthoritativeBizDocs AB ON ( LD.numListItemID = AB.numAuthoritativeSales )
                        WHERE   LD.numListID = 27
                                AND ( constFlag = 1
                                      OR LD.numDomainID = @numDomainID
                                    )
                                AND AB.numDomainID = @numDomainID
                        UNION
                        SELECT  0 numPageAuthID,
                                @numGroupID numGroupID,
                                @numTabID numTabID,
                                ld.numListItemID numPageNavID,
                                1 bitVisible,
                                @numDomainID numDomainID,
                                ISNULL(vcData, '') AS [vcNodeName],
                                1 tintType
                        FROM    listdetails Ld
                                LEFT JOIN listorder LO ON Ld.numListItemID = LO.numListItemID
                                                          AND lo.numDomainId = @numDomainID
                                INNER JOIN AuthoritativeBizDocs AB ON ( ld.numListItemID = AB.numAuthoritativePurchase )
                        WHERE   LD.numListID = 27
                                AND ( constFlag = 1
                                      OR LD.numDomainID = @numDomainID
                                    )
                                AND AB.numDomainID = @numDomainID
                        UNION
                        SELECT  0 numPageAuthID,
                                @numGroupID numGroupID,
                                @numTabID numTabID,
                                0 numPageNavID,
                                1 bitVisible,
                                @numDomainID numDomainID,
                                ISNULL(LT.vcData, '') AS [vcNodeName],
                                1 tintType
                        FROM    ListDetails LT
						OUTER APPLY
						(SELECT * FROM listdetails WHERE numListID = 27
                                AND (constFlag = 1 OR numDomainID = @numDomainID)
                                AND (numListItemID IN (
                                      SELECT    AB.numAuthoritativeSales
                                      FROM      AuthoritativeBizDocs AB
                                      WHERE     AB.numDomainId = @numDomainID )
                                      OR numListItemID IN (
                                      SELECT    AB.numAuthoritativePurchase
                                      FROM      AuthoritativeBizDocs AB
                                      WHERE     AB.numDomainId = @numDomainID )
                                    )) ld
                        WHERE   LT.numListID = 11
                                AND Lt.numDomainID = @numDomainID
                        UNION
                        SELECT  0 numPageAuthID,
                                @numGroupID numGroupID,
                                @numTabID numTabID,
                                2222 numPageNavID,
                                1 bitVisible,
                                @numDomainID numDomainID,
                                'Other BizDocs' AS [vcNodeName],
                                1 tintType
                        UNION
                        SELECT  0 numPageAuthID,
                                @numGroupID numGroupID,
                                @numTabID numTabID,
                                ld.numListItemID numPageNavID,
                                1 bitVisible,
                                @numDomainID numDomainID,
                                ISNULL(vcData, '') AS [vcNodeName],
                                1 tintType
                        FROM    listdetails Ld
                        WHERE   ld.numListID = 27
                                AND ( constFlag = 1
                                      OR LD.numDomainID = @numDomainID
                                    )
                                AND LD.numListItemID NOT IN (
                                SELECT  AB.numAuthoritativeSales
                                FROM    AuthoritativeBizDocs AB
                                WHERE   AB.numDomainID = @numDomainID )
                                AND LD.numListItemID NOT IN (
                                SELECT  AB.numAuthoritativePurchase
                                FROM    AuthoritativeBizDocs AB
                                WHERE   AB.numDomainID = @numDomainID )
                        UNION
                        SELECT  0 numPageAuthID,
                                @numGroupID numGroupID,
                                @numTabID numTabID,
                                ls.numListItemID numPageNavID,
                                1 bitVisible,
                                @numDomainID numDomainID,
                                ISNULL(ls.vcData, '') AS [vcNodeName],
                                1 tintType
                        FROM    listdetails Ld,
                                ( SELECT    *
                                  FROM      ListDetails LS
                                  WHERE     LS.numDomainID = @numDomainID
                                            AND LS.numListID = 11
                                ) LS
                        WHERE   ld.numListID = 27
                                AND ( ld.constFlag = 1
                                      OR LD.numDomainID = @numDomainID
                                    )
                                AND LD.numListItemID NOT IN (
                                SELECT  AB.numAuthoritativeSales
                                FROM    AuthoritativeBizDocs AB
                                WHERE   AB.numDomainID = @numDomainID
                                UNION
                                SELECT  AB.numAuthoritativePurchase
                                FROM    AuthoritativeBizDocs AB
                                WHERE   AB.numDomainID = @numDomainID )
                        ORDER BY tintType,
                                numPageNavID
                    END
            END
        ELSE 
            BEGIN
				
                SELECT  *
                FROM    ( SELECT    TNA.numPageAuthID,
                                    TNA.numGroupID,
                                    TNA.numTabID,
                                    TNA.numPageNavID,
                                    TNA.bitVisible,
                                    TNA.numDomainID,
                                    ISNULL(PND.vcPageNavName, '') AS [vcNodeName],
                                    tintType
                          FROM      PageNavigationDTL PND
                                    JOIN dbo.TreeNavigationAuthorization TNA ON PND.numPageNavID = TNA.numPageNavID  --AND PND.numTabID = TNA.numTabID       
                          WHERE     1 = 1
                                    --AND ISNULL(TNA.bitVisible, 0) = 1
                                    AND ISNULL(PND.bitVisible, 0) = 1
                                    AND numModuleID = @numModuleID
									AND TNA.numTabID = @numTabID
                                    AND TNA.numDomainID = @numDomainID
                                    AND numGroupID = @numGroupID
                          UNION
                          SELECT    0 numPageAuthID,
                                    @numGroupID numGroupID,
                                    @numTabID numTabID,
                                    1111 numPageNavID,
                                    1 bitVisible,
                                    @numDomainID numDomainID,
                                    'Regular Documents' AS [vcNodeName],
                                    1 tintType
                          UNION
                          SELECT    0 numPageAuthID,
                                    @numGroupID numGroupID,
                                    @numTabID numTabID,
                                    Ld.numListItemId,
                                    1 bitVisible,
                                    @numDomainID numDomainID,
                                    ISNULL(vcData, '') AS [vcNodeName],
                                    1 tintType
                          FROM      listdetails Ld
                                    LEFT JOIN listorder LO ON Ld.numListItemID = LO.numListItemID
                                                              AND Lo.numDomainId = @numDomainID
                          WHERE     Ld.numListID = 29
                                    AND ( constFlag = 1
                                          OR Ld.numDomainID = @numDomainID
                                        )
                          UNION
                          SELECT    0 numPageAuthID,
                                    @numGroupID numGroupID,
                                    @numTabID numTabID,
                                    ld.numListItemID numPageNavID,
                                    1 bitVisible,
                                    @numDomainID numDomainID,
                                    ISNULL(vcData, '') AS [vcNodeName],
                                    1 tintType
                          FROM      listdetails Ld
                                    LEFT JOIN listorder LO ON Ld.numListItemID = LO.numListItemID
                                                              AND lo.numDomainId = @numDomainID
                                    INNER JOIN AuthoritativeBizDocs AB ON ( LD.numListItemID = AB.numAuthoritativeSales )
                          WHERE     LD.numListID = 27
                                    AND ( constFlag = 1
                                          OR LD.numDomainID = @numDomainID
                                        )
                                    AND AB.numDomainID = @numDomainID
                          UNION
                          SELECT    0 numPageAuthID,
                                    @numGroupID numGroupID,
                                    @numTabID numTabID,
                                    ld.numListItemID numPageNavID,
                                    1 bitVisible,
                                    @numDomainID numDomainID,
                                    ISNULL(vcData, '') AS [vcNodeName],
                                    1 tintType
                          FROM      listdetails Ld
                                    LEFT JOIN listorder LO ON Ld.numListItemID = LO.numListItemID
                                                              AND lo.numDomainId = @numDomainID
                                    INNER JOIN AuthoritativeBizDocs AB ON ( ld.numListItemID = AB.numAuthoritativePurchase )
                          WHERE     LD.numListID = 27
                                    AND ( constFlag = 1
                                          OR LD.numDomainID = @numDomainID
                                        )
                                    AND AB.numDomainID = @numDomainID
                          UNION
                          SELECT    0 numPageAuthID,
                                    @numGroupID numGroupID,
                                    @numTabID numTabID,
                                    0 numPageNavID,
                                    1 bitVisible,
                                    @numDomainID numDomainID,
                                    ISNULL(LT.vcData, '') AS [vcNodeName],
                                    1 tintType
                          FROM      ListDetails LT,
                                    listdetails ld
                          WHERE     LT.numListID = 11
                                    AND Lt.numDomainID = @numDomainID
                                    AND LD.numListID = 27
                                    AND ( ld.constFlag = 1
                                          OR LD.numDomainID = @numDomainID
                                        )
                                    AND ( LD.numListItemID IN (
                                          SELECT    AB.numAuthoritativeSales
                                          FROM      AuthoritativeBizDocs AB
                                          WHERE     AB.numDomainId = @numDomainID )
                                          OR ld.numListItemID IN (
                                          SELECT    AB.numAuthoritativePurchase
                                          FROM      AuthoritativeBizDocs AB
                                          WHERE     AB.numDomainId = @numDomainID )
                                        )
                          UNION
                          SELECT    0 numPageAuthID,
                                    @numGroupID numGroupID,
                                    @numTabID numTabID,
                                    2222 numPageNavID,
                                    1 bitVisible,
                                    @numDomainID numDomainID,
                                    'Other BizDocs' AS [vcNodeName],
                                    1 tintType
                          UNION
                          SELECT    0 numPageAuthID,
                                    @numGroupID numGroupID,
                                    @numTabID numTabID,
                                    ld.numListItemID numPageNavID,
                                    1 bitVisible,
                                    @numDomainID numDomainID,
                                    ISNULL(vcData, '') AS [vcNodeName],
                                    1 tintType
                          FROM      listdetails Ld
                          WHERE     ld.numListID = 27
									AND @numTabID <> 133
                                    AND ( constFlag = 1
                                          OR LD.numDomainID = @numDomainID
                                        )
                                    AND LD.numListItemID NOT IN (
                                    SELECT  AB.numAuthoritativeSales
                                    FROM    AuthoritativeBizDocs AB
                                    WHERE   AB.numDomainID = @numDomainID )
                                    AND LD.numListItemID NOT IN (
                                    SELECT  AB.numAuthoritativePurchase
                                    FROM    AuthoritativeBizDocs AB
                                    WHERE   AB.numDomainID = @numDomainID )
                          UNION
                          SELECT    0 numPageAuthID,
                                    @numGroupID numGroupID,
                                    @numTabID numTabID,
                                    ls.numListItemID numPageNavID,
                                    1 bitVisible,
                                    @numDomainID numDomainID,
                                    ISNULL(ls.vcData, '') AS [vcNodeName],
                                    1 tintType
                          FROM      listdetails Ld,
                                    ( SELECT    *
                                      FROM      ListDetails LS
                                      WHERE     LS.numDomainID = @numDomainID
                                                AND LS.numListID = 11
                                    ) LS
                          WHERE     ld.numListID = 27
									AND @numTabID <> 133
                                    AND ( ld.constFlag = 1
                                          OR LD.numDomainID = @numDomainID
                                        )
                                    AND LD.numListItemID NOT IN (
                                    SELECT  AB.numAuthoritativeSales
                                    FROM    AuthoritativeBizDocs AB
                                    WHERE   AB.numDomainID = @numDomainID
                                    UNION
                                    SELECT  AB.numAuthoritativePurchase
                                    FROM    AuthoritativeBizDocs AB
                                    WHERE   AB.numDomainID = @numDomainID )
                        ) PageNavTable
                        JOIN dbo.TreeNavigationAuthorization TreeNav ON PageNavTable.numPageNavID = TreeNav.numPageNavID
                                                                        AND PageNavTable.numGroupID = TreeNav.numGroupID
                                                                        AND PageNavTable.numTabID = TreeNav.numTabID
                ORDER BY TreeNav.tintType,
                        TreeNav.numPageNavID

            END

        SELECT  *
        FROM    ( SELECT    ISNULL(( SELECT TOP 1
                                            TNA.[numPageAuthID]
                                     FROM   dbo.TreeNavigationAuthorization TNA
                                     WHERE  TNA.numPageNavID = PND.[numPageNavID]
                                            AND TNA.numDomainID = @numDomainID
                                            AND TNA.numGroupID = @numGroupID
                                            AND TNA.numTabID = @numTabID
                                   ), 0) AS [numPageAuthID],
                            ISNULL(@numGroupID, 0) AS [numGroupID],
                            ISNULL(PND.[numTabID], 0) AS [numTabID],
                            ISNULL(PND.[numPageNavID], 0) AS [numPageNavID],
                            ISNULL(( SELECT TOP 1
                                            TNA.[bitVisible]
                                     FROM   dbo.TreeNavigationAuthorization TNA
                                     WHERE  TNA.numPageNavID = PND.[numPageNavID]
                                            AND TNA.numDomainID = @numDomainID
                                            AND TNA.numGroupID = @numGroupID
                                            AND TNA.numTabID = @numTabID
                                   ), 0) AS [bitVisible],
                            @numDomainID AS [numDomainID],
                            ( SELECT TOP 1
                                        vcPageNavName
                              FROM      dbo.PageNavigationDTL
                              WHERE     PageNavigationDTL.numPageNavID = PND.numPageNavID
                            ) AS [vcNodeName],
                            ISNULL(( SELECT TOP 1
                                            TNA.tintType
                                     FROM   dbo.TreeNavigationAuthorization TNA
                                     WHERE  TNA.numPageNavID = PND.[numPageNavID]
                                            AND TNA.numDomainID = @numDomainID
                                            AND TNA.numGroupID = @numGroupID
                                            AND TNA.numTabID = @numTabID
                                   ), 1) AS [tintType]
                  FROM      dbo.PageNavigationDTL PND
                  WHERE     ISNULL(PND.bitVisible, 0) = 1
                            AND PND.numTabID = @numTabID
                            AND numModuleID = @numModuleID
                  UNION ALL
                  SELECT    ISNULL([numPageAuthID], 0) AS [numPageAuthID],
                            @numGroupID AS [numGroupID],
                            @numTabID AS [numTabID],
                            ISNULL(LD.[numListItemID], 0) AS [numPageNavID],
                            ISNULL(TNA.[bitVisible], 0) AS [bitVisible],
                            ISNULL(LD.[numDomainID], 0) AS [numDomainID],
                            vcData AS [vcNodeName],
                            1 AS [tintType]
                  FROM      dbo.ListDetails LD
                            LEFT JOIN dbo.TreeNavigationAuthorization TNA ON LD.numListItemID = TNA.numPageNavID
                  WHERE     numListID = 27
                            AND LD.numDomainID = @numDomainID
                            AND ISNULL(constFlag, 0) = 0
                            AND numTabID = ( CASE WHEN @numModuleID = 10
                                                  THEN 1
                                                  ELSE 0
                                             END )
                  UNION ALL
                  SELECT    ISNULL([numPageAuthID], 0) AS [numPageAuthID],
                            @numGroupID AS [numGroupID],
                            @numTabID AS [numTabID],
                            ISNULL(LD.[numListItemID], 0) AS [numPageNavID],
                            ISNULL(TNA.[bitVisible], 0) AS [bitVisible],
                            ISNULL(TNA.[numDomainID], 0) AS [numDomainID],
                            vcData AS [vcNodeName],
                            1 AS [tintType]
                  FROM      dbo.ListDetails LD
                            LEFT JOIN dbo.TreeNavigationAuthorization TNA ON LD.numListItemID = TNA.numPageNavID
                                                                             AND ISNULL(TNA.[numDomainID], 0) = @numDomainID
                  WHERE     numListID = 27
                            AND ISNULL(constFlag, 0) = 1
                            AND numTabID = @numTabID
                ) TABLE2
        ORDER BY numPageNavID  
    END
