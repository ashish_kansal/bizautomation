/****** Object:  StoredProcedure [dbo].[USP_GetCaseReportForAgents]    Script Date: 07/26/2008 16:16:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created By Anoop jayaraj                      
                      
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getcasereportforagents')
DROP PROCEDURE usp_getcasereportforagents
GO
CREATE PROCEDURE [dbo].[USP_GetCaseReportForAgents]                        
        @dtFromDate datetime,                            
        @dtToDate datetime,                            
        @numUserCntID numeric=0,        
        @intStatus as integer,     
        @tintRights as tinyint,    
        @numDomainID as bigint,    
        @tintType as bigint                     
                        
as     
    
If @tintRights<>3    
Begin    
    
If @intStatus=0         
Begin                     
SELECT                                
   CMP.vcCompanyName+', ' +Div.vcDivisionName as Company,Div.numDivisionID,CMP.numCompanyID,dbo.fn_GetPrimaryContact(Div.numDivisionID) as ContactID,Div.tintCRMType, numCaseid,                             
   Cs.vcCaseNumber,                       
  Cs.bintCreatedDate as bintCreatedDate,                      
   Cs.textSubject,                           
   Cs.intTargetResolveDate,                                 
   isnull(lst.vcdata,'-') as Status                                          
 FROM                             
   Cases  Cs                                
 JOIN DivisionMaster Div                             
  ON Cs.numDivisionId = Div.numDivisionID                             
  JOIN CompanyInfo CMP                           
 ON Div.numCompanyID = CMP.numCompanyId                           
 left join listdetails lst                          
 on lst.numListItemID= cs.numStatus                          
where Cs.numAssignedTo=@numUserCntID and Cs.bintCreatedDate between   @dtFromDate and @dtToDate                
End        
             
If @intStatus<>0         
Begin                     
SELECT                                
   CMP.vcCompanyName+', ' +Div.vcDivisionName as Company,Div.numDivisionID,CMP.numCompanyID,dbo.fn_GetPrimaryContact(Div.numDivisionID) as ContactID,Div.tintCRMType, numCaseid,                             
   Cs.vcCaseNumber,                       
  Cs.bintCreatedDate as bintCreatedDate,                      
   Cs.textSubject,                           
   Cs.intTargetResolveDate,                                 
   isnull(lst.vcdata,'-') as Status                                          
 FROM                             
   Cases  Cs                                
 JOIN DivisionMaster Div                             
  ON Cs.numDivisionId = Div.numDivisionID                             
  JOIN CompanyInfo CMP                           
 ON Div.numCompanyID = CMP.numCompanyId                           
 left join listdetails lst                          
 on lst.numListItemID= cs.numStatus                          
 where Cs.numAssignedTo=@numUserCntID And Cs.numStatus = @intStatus        
 And Cs.bintCreatedDate between   @dtFromDate and @dtToDate                
End        
End      
    
    
If @tintRights=3    
Begin    
    
If @intStatus=0         
Begin                     
SELECT                                
   CMP.vcCompanyName+', ' +Div.vcDivisionName as Company,Div.numDivisionID,CMP.numCompanyID,dbo.fn_GetPrimaryContact(Div.numDivisionID) as ContactID,Div.tintCRMType, numCaseid,                             
   Cs.vcCaseNumber,                       
  Cs.bintCreatedDate as bintCreatedDate,                      
   Cs.textSubject,                           
   Cs.intTargetResolveDate,                                 
   isnull(lst.vcdata,'-') as Status                                          
 FROM                             
   Cases  Cs                                
 JOIN DivisionMaster Div                             
  ON Cs.numDivisionId = Div.numDivisionID                             
 JOIN CompanyInfo CMP                           
 ON Div.numCompanyID = CMP.numCompanyId     
 Join AdditionalContactsInformation ADC on    
  Cs.numAssignedTo=ADC.numContactId                          
 left join listdetails lst                          
 on lst.numListItemID= cs.numStatus                          
where Cs.numAssignedTo=@numUserCntID and Cs.bintCreatedDate between   @dtFromDate and @dtToDate      
 and Div.numTerId   in(select F.numTerritory from ForReportsByTerritory F                  
 where F.numUserCntID=@numUserCntID and F.numDomainid=@numDomainID and F.tintType=@tintType)   
End        
             
If @intStatus<>0         
Begin                     
SELECT                                
   CMP.vcCompanyName+', ' +Div.vcDivisionName as Company,Div.numDivisionID,CMP.numCompanyID,dbo.fn_GetPrimaryContact(Div.numDivisionID) as ContactID,Div.tintCRMType, numCaseid,                             
   Cs.vcCaseNumber,                       
  Cs.bintCreatedDate as bintCreatedDate,                      
   Cs.textSubject,                           
   Cs.intTargetResolveDate,                                 
   isnull(lst.vcdata,'-') as Status                                          
 FROM                             
   Cases  Cs                                
 JOIN DivisionMaster Div                             
  ON Cs.numDivisionId = Div.numDivisionID                             
 JOIN CompanyInfo CMP                           
 ON Div.numCompanyID = CMP.numCompanyId     
 Join AdditionalContactsInformation ADC on    
    Cs.numAssignedTo=ADC.numContactId    
 left join listdetails lst                          
 on lst.numListItemID= cs.numStatus                          
 where  Cs.numAssignedTo=@numUserCntID and Cs.numStatus = @intStatus        
 And Cs.bintCreatedDate between   @dtFromDate and @dtToDate      
 and Div.numTerId   in(select F.numTerritory from ForReportsByTerritory F                                       
 where F.numUserCntID=@numUserCntID and F.numDomainid=@numDomainID and F.tintType=@tintType)     
End        
    
End
GO
