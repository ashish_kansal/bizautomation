/****** Object:  StoredProcedure [dbo].[USP_ProjectManage]    Script Date: 07/26/2008 16:20:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created By Anoop Jayaraj                      
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_projectmanage')
DROP PROCEDURE usp_projectmanage
GO
CREATE PROCEDURE [dbo].[USP_ProjectManage]                        
(                        
	@numProId as numeric OUTPUT,
	@numDivisionId numeric(9)=null,
	@numIntPrjMgr numeric(9)=null,
	@numOppId numeric(9)=null,
	@numCustPrjMgr numeric(9)=null,
	@vcPProName Varchar(100)='' OUTPUT,
	@Comments varchar(1000)='',
	@numUserCntID numeric(9)=0,
	@numAssignedTo numeric(9)=0,
	@numDomainId numeric(9)=0,
	@dtDueDate as datetime,
	@numContractId as numeric(9) =0,
	@vcProjectName VARCHAR(100),
	@numProjectType NUMERIC(9),
	@numProjectStatus NUMERIC(9),
	@dtmStartDate DATETIME,
	@dtmEndDate DATETIME
)                        
as                        
           
IF DATEPART(YEAR,@dtDueDate) = 1753 set  @dtDueDate=null
                     
                        
declare @TotalAmount as integer                        
if @numProId = 0                        
begin                        
declare @intProTcode as numeric(9)                        
Select @intProTcode=max(numProId)from ProjectsMaster                        
set @intProTcode =isnull(@intProTcode,0) + 1                        
set @vcPProName=@vcPProName+ '-'+convert(varchar(4),year(getutcdate()))  +'-A'+ convert(varchar(10),@intProTcode)                        
                        
insert into ProjectsMaster                        
  (                        
  vcProjectID,
  numIntPrjMgr,                        
  numOppId,                        
  numCustPrjMgr,                                 
  numDivisionId,                        
  txtComments,                        
  numCreatedBy,                        
  bintCreatedDate,                        
  numModifiedBy,                        
  bintModifiedDate,                        
  numDomainId,                        
  numRecOwner,                      
  intDueDate,              
  numAssignedby,              
  numAssignedTo ,        
  numContractId,
  vcProjectName,
  numProjectType,numProjectStatus,dtmStartDate,dtmEndDate                  
  )                        
 Values                        
  (                        
  @vcPProName,                        
  @numIntPrjMgr,                        
  @numOppId,                        
  @numCustPrjMgr,                                  
  @numDivisionId,                        
  @Comments,                        
  @numUserCntID,                        
  getutcdate(),                        
  @numUserCntID,                        
  getutcdate(),                        
  @numDomainId,                        
  @numUserCntID,                      
  @dtDueDate,              
  0,              
  0 ,        
 @numContractId,
 @vcProjectName,
 @numProjectType,@numProjectStatus,@dtmStartDate,@dtmEndDate          
  )                        
                        
 set @numProId=SCOPE_IDENTITY()                        
 
 --Map Custom Field	
  DECLARE @tintPageID AS TINYINT;SET @tintPageID=11
  	
  EXEC dbo.USP_AddParentChildCustomFieldMap
	@numDomainID = @numDomainID, --  numeric(18, 0)
	@numRecordID = @numProId, --  numeric(18, 0)
	@numParentRecId = @numDivisionId, --  numeric(18, 0)
	@tintPageID = @tintPageID --  tinyint

EXEC dbo.usp_ProjectDefaultAssociateContacts @numProId=@numProId,@numDomainId=@numDomainId
  insert into [ProjectsStageDetails] 
  (                                                                    
  [numProId],                                                                    
  numstagepercentage,                                                                    
  vcStageDetail,                                                                    
  vcComments,                            
  numDomainId,                                                                    
  numCreatedBy,                                                                    
  bintCreatedDate,                                                                    
  numModifiedBy,                                                                    
  bintModifiedDate,                                                                    
  numAssignTo,                                                                    
  bintStageComDate,                                                                    
  bintDueDate,                                                                    
  bitAlert,                                                                    
  bitStageCompleted                                                                    
  )                                                                          
  values                      
  (                      
  @numProId,                                                                    
  100,                      
  'Project Completed',                      
  '',                                                                    
  @numDomainId,                                                                    
  @numUserCntID,                                                                    
  getutcdate(),                                   
  @numUserCntID,                                                                    
  getutcdate(),                                                                    
  0,                                                                    
  0,                                                                    
  0,                                                                    
  0,                                                                    
  0--@DealCompleted
  )                                                                     
  insert into ProjectsStageDetails                                  
  (                                                             
  numProId,                                                                    
  numstagepercentage,                                                                    
  vcStageDetail,                                                                    
  vcComments,                                                                
  numDomainId,                                                                    
  numCreatedBy,                                                                    
  bintCreatedDate,                                                                    
  numModifiedBy,                                                            
  bintModifiedDate,                                                                    
  numAssignTo,                                                                  
  bintStageComDate,                                                                    
  bintDueDate,                                                                    
  bitAlert,                                                                    
  bitStageCompleted                                                                    
  )                                 
  values                      
  (                  
  @numProId,                                                                    
  0,                                                                    
  'Project Lost',                                 
  '',                                                                    
 @numDomainId,                                                                    
  @numUserCntID,                                                                    
  getutcdate(),                                                                    
  @numUserCntID,                                                                    
  getutcdate(),                        
   0,                                                                    
   0,                                                                
  0,                                                                    
  0,                                                                    
  0                                                                    
  )        
                                           
end                        
else                                          
begin                        
		-- Checking For Incidents
		declare @update as bit 
		set @update = 0
		if @numContractId <> 0
		begin
			IF (SELECT bitIncidents FROM [ContractManagement] WHERE numdomainId = @numDomainID  and numcontractId = @numContractId) = 1
			BEGIN
				declare @Usedincidents as numeric(9)
			set @Usedincidents = 0
			declare @Availableincidents as numeric(9)
			

			select @Usedincidents=@Usedincidents+count(*) from cases 
				where numcontractId = @numContractId and numdomainId = @numDomainID 

			select @Usedincidents=@Usedincidents+count(*) from projectsmaster 
				where numcontractId = @numContractId and numdomainId = @numDomainID and numProId <> @numProId


			select @Availableincidents=isnull(numincidents,0) from contractManagement 
			where numdomainId = @numDomainID  and numcontractId = @numContractId
				print '@Usedincidents-----'
				print @Usedincidents
				print @Availableincidents
			if @Usedincidents >=@Availableincidents
				set @update = 1	
			end
		end

if @update = 0
	begin 
		  DECLARE @numOldProjectStatus AS NUMERIC	
		  SELECT @numOldProjectStatus=ISNULL(numProjectStatus,0) FROM ProjectsMaster where numProId=@numProId
		  	
		  update ProjectsMaster                        
		  set  
		   vcProjectID=@vcPProName,  numIntPrjMgr=@numIntPrjMgr,                        
		   numOppId=@numOppId,                        
		   numCustPrjMgr=@numCustPrjMgr,                        
		   txtComments=@Comments,                        
		   numModifiedBy=@numUserCntID,                        
		   bintModifiedDate=getutcdate(),                      
		   intDueDate =@dtDueDate ,         
		   numContractId=@numContractId,
		   vcProjectName=@vcProjectName,
		   numProjectType=@numProjectType,numProjectStatus=@numProjectStatus,dtmStartDate=@dtmStartDate,dtmEndDate=@dtmEndDate
		  where numProId=@numProId 

		IF @numOldProjectStatus != @numProjectStatus
		BEGIN
			IF @numProjectStatus=27492  ---Updating All Statge to 100% if Completed
			BEGIN
					UPDATE PM SET dtCompletionDate=GETUTCDATE(),monTotalExpense=PIE.monExpense,
					monTotalIncome=PIE.monIncome,monTotalGrossProfit=PIE.monBalance
					FROM ProjectsMaster PM,dbo.GetProjectIncomeExpense(@numDomainId,@numProId) PIE
					WHERE PM.numProId=@numProId
					
					EXEC USP_ManageProjectCommission @numDomainId,@numProId
					
					Update StagePercentageDetails set tinProgressPercentage=100,bitclose=1 where numProjectID=@numProId

					EXEC dbo.USP_GetProjectTotalProgress
							@numDomainID = @numDomainID, --  numeric(9, 0)
							@numProId = @numProId, --  numeric(9, 0)
							@tintMode = 1 --  tinyint
			END
			ELSE
			BEGIN
				DELETE from BizDocComission WHERE numDomainID = @numDomainID AND numProId=@numProId AND numComissionID 
					NOT IN (SELECT BC.numComissionID FROM dbo.BizDocComission BC JOIN dbo.PayrollTracking PT 
						ON BC.numComissionID = PT.numComissionID AND BC.numDomainId = PT.numDomainId WHERE BC.numDomainID = @numDomainID AND ISNULL(BC.numProId,0) = @numProId)
			END
		END
	end       
	else
			set @numProId = 0	                        
                       
   PRINT '@numProId : ' + CONVERT(VARCHAR(10),@numProId)            
   IF ISNULL(@numProId,0) > 0
	BEGIN
		DECLARE @numPrimaryAddressID AS NUMERIC(18,0)
		DECLARE @numAddressID AS NUMERIC(18,0)
		
		SELECT @numPrimaryAddressID = ISNULL(numAddressID,0) FROM dbo.AddressDetails WHERE numRecordID = @numDivisionId  AND ISNULL(bitIsPrimary,0) = 1
		PRINT '@numPrimaryAddressID : ' + CONVERT(VARCHAR(10),@numPrimaryAddressID)
		
		INSERT INTO dbo.AddressDetails 
		(vcAddressName,vcStreet,vcCity,vcPostalCode,numState,numCountry,bitIsPrimary,tintAddressOf,tintAddressType,numRecordID,numDomainID )
		SELECT vcAddressName,vcStreet,vcCity,vcPostalCode,numState,numCountry,bitIsPrimary,4,tintAddressType,@numProId ,numDomainID
		FROM dbo.AddressDetails
		WHERE numAddressID = @numPrimaryAddressID
		
		--SELECT @numAddressID = numAddressID FROM dbo.AddressDetails WHERE numRecordID = @numProId AND ISNULL(bitIsPrimary,0) = 1  AND ISNULL(tintAddressOf,0) = 4
		SET @numAddressID = SCOPE_IDENTITY()  
		PRINT '@numAddressID : ' + CONVERT(VARCHAR(10),@numAddressID)
		UPDATE ProjectsMaster SET numAddressID = @numAddressID 	WHERE numProId=@numProId         	
	END 
	                     
    ---Updating if Project is assigned to someone                      
  declare @tempAssignedTo as numeric(9)                    
  set @tempAssignedTo=null                     
  select @tempAssignedTo=isnull(numAssignedTo,0) from ProjectsMaster where numProId=@numProId                     
print @tempAssignedTo                    
  if (@tempAssignedTo<>@numAssignedTo and  @numAssignedTo<>'0')                    
  begin                      
    update ProjectsMaster set numAssignedTo=@numAssignedTo ,numAssignedBy=@numUserCntID where numProId=@numProId                      
  end                     
  else if  (@numAssignedTo =0)                    
  begin                    
   update ProjectsMaster set numAssignedTo=0 ,numAssignedBy=0 where numProId=@numProId                   
  end                                  
                        
set @vcPProName='erer'                        
                
end
GO
