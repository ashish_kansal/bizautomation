/****** Object:  StoredProcedure [dbo].[usp_ManageAssociations]    Script Date: 07/26/2008 16:19:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Modified By: Debasish                    
--Modified Date: 30th Dec 2005                    
--Purpose for Modification: To insert new associations/ modifying existing ones for the Organizations                    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_manageassociations')
DROP PROCEDURE usp_manageassociations
GO
CREATE PROCEDURE [dbo].[usp_ManageAssociations]                          
 @numAssociationID numeric=0,                          
 @numAssociatedFromDivID numeric,                          
 @numCompID numeric,                          
 @numDivID numeric,                          
 @bitParentOrg bit,                           
 @bitChildOrg bit,                          
 @numTypeID numeric,                          
 @bitDeleted bit = 0,                          
 @numUserCntID numeric,                           
 @numDomainID numeric,  
 @bitShareportal BIT,
 @bitTypeLabel BIT,
 @numAssoTypeLabel NUMERIC(9)=0,
 @numContactID AS NUMERIC(9)
AS                      
  DECLARE @Count As Integer         
  DECLARE @vcParentOrgName As NVarchar(100)          

  SELECT @numCompID = [numCompanyID] FROM [DivisionMaster] WHERE [numDivisionID]=@numDivID
  
  IF EXISTS(SELECT 1 FROM CompanyAssociations                          
  WHERE numDomainID = @numDomainID AND numAssociateFromDivisionID != @numAssociatedFromDivID AND numDivisionID=@numDivID AND ISNULL(bitChildOrg,0)=1 AND @bitChildOrg = 1)
  BEGIN
  	SELECT 'DuplicateChild'
	RETURN 
  END
  ELSE IF EXISTS(SELECT 1 FROM CompanyAssociations                          
  WHERE numDomainID = @numDomainID AND numAssociateFromDivisionID = @numAssociatedFromDivID AND numDivisionID=@numDivID)
  BEGIN
 -- 	SELECT 'Duplicate'
		UPDATE CompanyAssociations SET 
	   numReferralType = @numTypeID,            
	   bitDeleted = 0,           
	   bitParentOrg = @bitParentOrg,             
	   bitChildOrg = @bitChildOrg,             
	   numModifiedBy = @numUserCntID,               
	   numModifiedOn = getutcdate(),  
	   bitShareportal  =@bitShareportal           
	   WHERE numDomainID = @numDomainID AND numAssociateFromDivisionID = @numAssociatedFromDivID AND numDivisionID=@numDivID  
	RETURN 
  END
  ELSE
  BEGIN         
--      IF  @bitParentOrg = 1        
--          SELECT @vcParentOrgName = dbo.fn_GetParentOrgName(@numAssociatedFromDivID)        
--      ELSE IF  @bitChildOrg = 1        
--          SELECT @vcParentOrgName = dbo.fn_GetParentOrgName(@numDivID)        
--        
--      IF @vcParentOrgName IS NULL             
--      BEGIN       
		IF @bitTypeLabel =1 
        BEGIN
			UPDATE [CompanyAssociations] SET [bitTypeLabel]=0 WHERE [numAssociateFromDivisionID]=@numAssociatedFromDivID
		END	
        
            INSERT INTO CompanyAssociations (numAssociateFromDivisionID, numCompanyID, numDivisionID, bitParentOrg,                    
            bitChildOrg, numReferralType, bitDeleted, numCreateBy, numCreatedOn,                     
            numModifiedBy, numModifiedOn, numDomainID,bitShareportal,[bitTypeLabel],[numAssoTypeLabel],numContactID)                           
            VALUES (@numAssociatedFromDivID, @numCompID, @numDivID, @bitParentOrg,                          
            @bitChildOrg, @numTypeID, @bitDeleted, @numUserCntID, getutcdate(),                           
            @numUserCntID, getutcdate(), @numDomainID,@bitShareportal,@bitTypeLabel,@numAssoTypeLabel,@numContactID)
        
        
        
            SELECT '0'           
--       END        
--       ELSE             
--       BEGIN          
--          DECLARE @numDivisionId Numeric        
--          IF  @bitParentOrg = 1          
--            SET @numDivisionId = @numAssociatedFromDivID        
--          ELSE IF  @bitChildOrg = 1        
--            SET @numDivisionId = @numDivId        
--        
--          SELECT vcCompanyName FROM        
--          (        
--             SELECT ci.vcCompanyName FROM CompanyAssociations ca, CompanyInfo ci, DivisionMaster dm        
--             WHERE ca.numAssociateFromDivisionId = @numDivisionId AND              
--             ca.numAssociateFromDivisionId = DM.numDivisionId AND        
--             dm.numCompanyId = ci.numCompanyId AND        
--             ca.bitParentOrg=1          
--             UNION        
--             SELECT ci.vcCompanyName FROM CompanyAssociations ca, CompanyInfo ci        
--             WHERE ca.numDivisionId = @numDivisionId AND        
--             ca.numCompanyId = ci.numCompanyId AND        
--             ca.bitChildOrg = 1         
--          ) As tmpTable        
--       END         
  END             
--  ELSE              
--  BEGIN              
--   UPDATE CompanyAssociations SET              
--   bitDeleted = 0,           
--   bitParentOrg = @bitParentOrg,             
--   bitChildOrg = @bitChildOrg,             
--   numModifiedBy = @numUserCntID,               
--   numModifiedOn = getutcdate(),  
--   bitShareportal  =@bitShareportal           
--   WHERE numAssociateFromDivisionID = @numAssociatedFromDivID AND                          
--   numCompanyID = @numCompID AND                          
--   numDivisionID = @numDivID AND              
--   numReferralType = @numTypeID    
--              
--   SELECT '0'            
--  END

