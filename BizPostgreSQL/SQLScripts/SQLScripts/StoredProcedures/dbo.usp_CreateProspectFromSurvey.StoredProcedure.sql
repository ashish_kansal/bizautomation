/****** Object:  StoredProcedure [dbo].[usp_CreateProspectFromSurvey]    Script Date: 07/26/2008 16:15:24 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_createprospectfromsurvey')
DROP PROCEDURE usp_createprospectfromsurvey
GO
CREATE PROCEDURE [dbo].[usp_CreateProspectFromSurvey]
	@vcCompanyName varchar(250),
	@vcFirstName varchar(250) = "",
	@vcLastName varchar(250) = "",
	@vcEmail varchar(200) = ""
--

AS
BEGIN

	DECLARE @revsql varchar(250)
	DECLARE @x int
	DECLARE @LastPivot varchar(100)
	DECLARE @FirstPivot varchar(100)
	declare @companyid int
	declare @divisionid int
	update surveyrespondants set bitpromoted = 1 where vcCompanyName+', '+vcDivisionName = @vcCompanyName
	--select vccompanyname from companyinfo where vccompanyname = 
	--Reverse the string	

	Set @revsql = Reverse(@vcCompanyName)	--Search for last occurence of WHEN	
	Set @x = charindex(',', @RevSql)	--Reverse the string again	

	Set @LastPivot = Reverse(Left(@RevSql, @x -1))	--Reset @sql to remove the imcomplete item	
	Set @LastPivot = Reverse(Left(@RevSql, @x -1))	--Reset @sql to remove the imcomplete item	
	Set @vcCompanyName = Left(@vcCompanyName, Len(@vcCompanyName) - (@x - 1))
	Set @FirstPivot = Reverse(Right(@RevSql, @x -1))	--Reset @sql to remove the imcomplete item	
--	Set @LastPivot = Substring(@LastPivot, 4, charindex("' ", @LastPivot) - 4)
	
	select @companyid =  numcompanyid from companyinfo where vccompanyname = @FirstPivot 	
	select @divisionid = numdivisionid from divisionmaster where numcompanyid = @companyid and vcdivisionname = @lastpivot
	if @vcFirstName = ''
	Begin
		update divisionmaster set tintcrmtype = 1 where numdivisionid = @divisionid
	End
	Else
	Begin
		update divisionmaster set tintcrmtype = 1 where numdivisionid = (Select numDivisionID from Additionalcontactsinformation where vcFirstName = @vcFirstName and vcLastName = @vcLastName and vcEmail = @vcEmail)
	End

--	INSERT INTO Forecast(sintYear,	tintquarter,tintMonth,vcQuota,numClosed,numPipeline,decProbability,numForecastAmount,numCreatedBy,bintCreatedDate,numDomainID)
--	VALUES(@sintYear,@tintquarter,@tintmonth,@vcQuota,@numClosed,@numPipeline,@decProbability,@numForecastAmount,@numCreatedBy,@bintCreatedDate,@numDomainID)
	
END
GO
