/****** Object:  StoredProcedure [dbo].[Usp_DeleteProject]    Script Date: 07/26/2008 16:15:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj      
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_deleteproject')
DROP PROCEDURE usp_deleteproject
GO
CREATE PROCEDURE [dbo].[Usp_DeleteProject]        
@numProId as numeric(9),  
@numDomainID as numeric(9)        
as     
  
if exists (select * from ProjectsMaster where numProID=@numProId and numDomainID=@numDomainID)  
begin     
 
 DELETE FROM [ProjectsOpportunities] WHERE [numProId]=@numProId AND [numDomainId]=@numDomainID
 delete RECENTITEMS where numRecordID =  @numProId and chrRecordType='P'   
 DELETE FROM dbo.ProjectProgress WHERE [numProId]=@numProId AND [numDomainId]=@numDomainID    
 delete from ProjectsStageDetails where numProId=@numProId        
-- delete from ProjectsSubStageDetails where numProId=@numProId              Not being used
 delete from TimeAndExpense where numProId=@numProId        
 delete from ProjectsDependency where numProId=@numProId        
 delete from ProjectsContacts where numProId=@numProId         
 delete from ProjectsMaster where numProId=@numProId    
end
GO
