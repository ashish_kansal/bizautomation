/****** Object:  StoredProcedure [dbo].[Select_Action_Template__UI_Data]    Script Date: 07/26/2008 16:14:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='select_action_template__ui_data')
DROP PROCEDURE select_action_template__ui_data
GO
CREATE PROCEDURE [dbo].[Select_Action_Template__UI_Data]
As

-- get status or Priority list data
SELECT  vcData , numListItemID FROM  ListDetails WHERE (numListID = 33)

-- get activity data
SELECT  vcData , numListItemID FROM  ListDetails WHERE (numListID = 32)
GO
