/****** Object:  StoredProcedure [dbo].[USP_ExtranetDetails]    Script Date: 07/26/2008 16:15:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj   
--exec USP_ExtranetDetails 1,1    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_extranetdetails')
DROP PROCEDURE usp_extranetdetails
GO
CREATE PROCEDURE [dbo].[USP_ExtranetDetails]        
@numExtranetID as numeric(9),  
@numDomainID as numeric(9)        
as        
  
  
select numExtranetID,vcCompanyName as Company,numGroupid,div.numDivisionID,numPartnerGroupID,((select isnull(intNoOfPartners,0) from  Subscribers where numTargetDomainID=@numDomainID)-(select count(*) from ExtranetAccountsDtl where bitPartnerAccess=1 and 
numDomainID=@numDomainID) ) as NoRemPartLic from ExtarnetAccounts Ext        
join DivisionMaster Div        
on Div.numDivisionID=Ext.numDivisionID        
join CompanyInfo Com        
on Com.numCompanyID=Div.numCompanyID        
where numExtranetID=@numExtranetID  and Ext.numDomainID=@numDomainID  
  
  
  
select isnull(numExtranetDtlID,0) as numExtranetDtlID,vcFirstName+ ' ' +vcLastName as ContactName,A.numContactid,vcemail,isnull(tintAccessAllowed,0) as PortalAccess,      
isnull(convert(tinyint,bitPartnerAccess),0) as PartnerAccess,      
isnull(vcPassword,'') as [Password] from ExtarnetAccounts Ext  
join AdditionalContactsInformation A   on A.numDivisionID=Ext.numDivisionID   
left join ExtranetAccountsDtl E on Ext.numExtranetID=E.numExtranetID and A.numContactID=E.numContactID             
where vcemail<>''  and Ext.numExtranetID=@numExtranetID
GO
