/****** Object:  StoredProcedure [dbo].[Delete_This_Action_Template_UI_Data]    Script Date: 07/26/2008 16:14:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='delete_this_action_template_ui_data')
DROP PROCEDURE delete_this_action_template_ui_data
GO
CREATE PROCEDURE [dbo].[Delete_This_Action_Template_UI_Data]
@rowID As Int
As
Delete From TblActionItemdata where RowID = @rowID
GO
