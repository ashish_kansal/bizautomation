/****** Object:  StoredProcedure [dbo].[USP_GetAlertsConAndCom]    Script Date: 07/26/2008 16:16:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created By Anoop Jayaraj
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getalertsconandcom')
DROP PROCEDURE usp_getalertsconandcom
GO
CREATE PROCEDURE [dbo].[USP_GetAlertsConAndCom]
@numAlerDTLId as numeric(9)=0,
@byteMode as tinyint=0
as
if @byteMode=0
begin

	select Alt.numAlertConID,Com.vcCompanyName,Div.vcDivisionName,
	AddC.vcFirstName + ' '+ AddC.vcLastName as [Name],isnull(Lst.vcData,'-') as Rating
	from AlertContactsAndCompany Alt
	join AdditionalContactsInformation AddC
	on AddC.numContactID=Alt.numContactID
	join DivisionMaster Div
	on Div.numDivisionID=AddC.numDivisionID
	join CompanyInfo Com
	on Com.numCompanyID=Div.numCompanyID
	left join ListDetails Lst
	on Lst.numListItemID=Com.numCompanyRating
	where numAlerDTLId=@numAlerDTLId
end
if @byteMode=1
begin

	select Alt.numAlertConID,Com.vcCompanyName,Div.vcDivisionName,isnull(Lst.vcData,'-') as Rating from AlertContactsAndCompany Alt
	join DivisionMaster Div
	on Div.numDivisionID=Alt.numContactID
	join CompanyInfo Com
	on Com.numCompanyID=Div.numCompanyID
	left join ListDetails Lst
	on Lst.numListItemID=Com.numCompanyRating
	where numAlerDTLId=@numAlerDTLId
end
GO
