/****** Object:  StoredProcedure [dbo].[USP_SaveLayoutListddl]    Script Date: 07/26/2008 16:21:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_savelayoutlistddl')
DROP PROCEDURE usp_savelayoutlistddl
GO
CREATE PROCEDURE [dbo].[USP_SaveLayoutListddl]                          
 @numUserCntID Numeric=0,                          
 @numDomainID Numeric=0,                 
@strRow as text='',              
@intcoulmn numeric=0,          
@Ctype as char                  
AS                    
                
delete  pagelayoutdtl where numUserCntId=@numUsercntId and numDomainId=@numDomainId and intcoulmn >= @intcoulmn  and numfieldId  in (select numfieldId from pagelayout where ctype=@ctype)              
                
if convert(varchar(10),@strRow) <> ''                    
begin                                                
   DECLARE @hDoc4 int                                                        
   EXEC sp_xml_preparedocument @hDoc4 OUTPUT, @strRow                                                        
                                                          
    insert into  PageLayoutDTL(numfieldId,tintRow,intcoulmn,numUserCntId,numDomainId,bitCustomField)                
 select * from (                   
SELECT * FROM OPENXML (@hDoc4,'/NewDataSet/Table',2)                                                        
 WITH  (                                                    
  numFieldID numeric(9),                
  tintRow numeric(9),                
  intColumn numeric(9),                
  numUserCntId numeric(9),                
  numDomainID numeric(9),
bitCustomField bit))X                 
                       
                   
end                  
               
        
EXEC sp_xml_removedocument @hDoc4
GO
