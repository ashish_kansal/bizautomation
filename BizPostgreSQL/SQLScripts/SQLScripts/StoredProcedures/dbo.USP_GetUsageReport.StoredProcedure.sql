/****** Object:  StoredProcedure [dbo].[USP_GetUsageReport]    Script Date: 07/26/2008 16:18:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj          
--exec USP_GetUsageReport 1,'',3          
          
          
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getusagereport')
DROP PROCEDURE usp_getusagereport
GO
CREATE PROCEDURE [dbo].[USP_GetUsageReport]          
@numDomainID as numeric(9)=0,          
@SearchWord as varchar(100),          
@tintLastLoggedIn as tinyint,      
@ClientTimeZoneOffset Int,
@vcSortColumn VARCHAR(200),
@vcSortOrder VARCHAR(4)         
as          
          
declare @strSQL as varchar(2000)          
          
          
          
set @strSQL='select numSubscriberID,vcCompanyName,isnull(vcFirstname,'''')+ '' ''+isnull(vcLastname,'''') as [Name],vcPosition,          
dbo.FormatedDateTimeFromDate(DateAdd(minute,'+Convert(varchar(10), -@ClientTimeZoneOffset)+',dtLastLoggedIn),'+Convert(varchar(10),@numDomainID)+') as dtLastLoggedIn,(select count(*) from UserAccessedDTL where numDivisionID=A.numDivisionID           
and numDomainID=S.numTargetDomainID and bitPortal=0'           
          
if @tintLastLoggedIn=1 set @strSQL=@strSQL + ' and dtLoggedInTime between '''+convert(varchar(30),dateadd(day,-7,getutcdate()))          
+''' and '''+convert(varchar(30),dateadd(day,1,getutcdate()))+''''          
          
if @tintLastLoggedIn=2 set @strSQL=@strSQL + ' and dtLoggedInTime between '''+convert(varchar(30),dateadd(day,-14,getutcdate()))          
+''' and '''+convert(varchar(30),dateadd(day,1,getutcdate()))+''''          
          
if @tintLastLoggedIn=3 set @strSQL=@strSQL + ' and dtLoggedInTime between '''+convert(varchar(30),dateadd(day,-30,getutcdate()))          
+''' and '''+convert(varchar(30),dateadd(day,1,getutcdate()))+''''          
          
          
          
set @strSQL=@strSQL +') as TotalCount,          
intNoofUsersSubscribed,intNoOfPartners,dbo.FormatedDateTimeFromDate(dtSubStartDate,'+Convert(varchar(10),@numDomainID)+') as dtSubStartDate,  
dbo.FormatedDateTimeFromDate(dtSubEndDate,'+Convert(varchar(10),@numDomainId)+') as dtSubEndDate ,bitTrial          
from Subscribers S           
join DivisionMaster D          
on D.numDivisionID=S.numDivisionID          
Join CompanyInfo C          
on C.numCompanyID = D.numCompanyID          
Join AdditionalContactsInformation A          
on A.numContactId= S.numAdminContactID          
where D.numDomainID=' + convert(varchar(15),@numDomainID)          
          
if @tintLastLoggedIn=1 set @strSQL=@strSQL + ' and dtLastLoggedIn between '''+convert(varchar(30),dateadd(day,-7,getutcdate()))          
+''' and '''+convert(varchar(30),dateadd(day,1,getutcdate()))+''''          
          
if @tintLastLoggedIn=2 set @strSQL=@strSQL + ' and dtLastLoggedIn between '''+convert(varchar(30),dateadd(day,-14,getutcdate()))        
+''' and '''+convert(varchar(30),dateadd(day,1,getutcdate()))+''''          
          
if @tintLastLoggedIn=3 set @strSQL=@strSQL + ' and dtLastLoggedIn between '''+convert(varchar(30),dateadd(day,-30,getutcdate()))          
+''' and '''+convert(varchar(30),dateadd(day,1,getutcdate()))+''''          
          
if @SearchWord<>'' set @strSQL=@strSQL + ' and vcCompanyName like ''%'+@SearchWord+'%'''          
  
IF ISNULL(@vcSortColumn,'') = ''
BEGIN
	SET @strSQL=@strSQL + ' ORDER BY vcCompanyName ASC'
END
ELSE
BEGIN
	IF @vcSortColumn='dtSubStartDate' OR @vcSortColumn='dtSubEndDate'  OR @vcSortColumn='dtLastLoggedIn'
	BEGIN
		SET @vcSortColumn = CONCAT('S.',@vcSortColumn)
	END

	SET @strSQL = CONCAT(@strSQL,' ORDER BY ',@vcSortColumn,' ',@vcSortOrder)
END
     
PRINT CAST(@strSQL AS NTEXT)
	      
exec (@strSQL)
GO
