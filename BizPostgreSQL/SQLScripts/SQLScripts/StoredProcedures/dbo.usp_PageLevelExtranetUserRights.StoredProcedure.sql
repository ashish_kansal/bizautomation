/****** Object:  StoredProcedure [dbo].[usp_PageLevelExtranetUserRights]    Script Date: 07/26/2008 16:20:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_pagelevelextranetuserrights')
DROP PROCEDURE usp_pagelevelextranetuserrights
GO
CREATE PROCEDURE [dbo].[usp_PageLevelExtranetUserRights]      
 @numContactID numeric,      
 @vcFileName varchar (100),      
 @numModuleID numeric=0,      
 @numPageID numeric=0         
--      
As   
  
SELECT GA.numModuleID,ExtDtl.numContactID, PM.vcFileName,  
  GA.numModuleID, GA.numPageID, MAX(GA.intExportAllowed) As intExportAllowed,   
  MAX(GA.intPrintAllowed) As intPrintAllowed, MAX(GA.intViewAllowed) As intViewAllowed,   
  MAX(GA.intAddAllowed) As intAddAllowed, MAX(GA.intUpdateAllowed) As intUpdateAllowed,   
  MAX(GA.intDeleteAllowed) As intDeleteAllowed  
  FROM ExtarnetAccounts Ext 
  INNER JOIN GroupAuthorization GA ON (GA.numGroupID=Ext.numGroupID  or GA.numGroupID=Ext.numPartnerGroupID) 
  INNER JOIN PageMaster PM ON PM.numPageID=GA.numPageID AND PM.numModuleID=GA.numModuleID 
  INNER JOIN ExtranetAccountsDtl ExtDtl ON Ext.numExtranetID=ExtDtl.numExtranetID AND ExtDtl.numContactId=@numContactID
  WHERE 
   PM.vcFileName=@vcFileName      
   AND GA.numModuleID=@numModuleID      
   AND GA.numPageID=@numPageID   
  GROUP BY GA.numModuleID,ExtDtl.numContactID,PM.vcFileName, GA.numModuleID, GA.numPageID
GO
