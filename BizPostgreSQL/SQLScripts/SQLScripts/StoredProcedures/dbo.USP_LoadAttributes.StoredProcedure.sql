/****** Object:  StoredProcedure [dbo].[USP_LoadAttributes]    Script Date: 07/26/2008 16:19:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--exec USP_LoadAttributes 14,66,'834,837'    
--created by anoop jayaraj    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_loadattributes')
DROP PROCEDURE usp_loadattributes
GO
CREATE PROCEDURE [dbo].[USP_LoadAttributes]    
@numItemCode as numeric(9)=0,    
@numListID as numeric(9)=0,    
@strAtrr as varchar(1000)=''    
as    
    
declare @bitSerialize as bit    
declare @ItemGroup as numeric(9)    
declare @strSQL as varchar(1000)    
select @bitSerialize=bitSerialized,@ItemGroup=numItemgroup from item where numItemCode=@numItemCode    
    
    
if @strAtrr!=''    
begin    
 Declare @Cnt int    
 Set @Cnt = 1    
 declare @SplitOn char(1)    
 set @SplitOn=','    
 set @strSQL='SELECT recid FROM CFW_Fld_Values_Serialized_Items where  bitSerialized ='+ convert(varchar(1),@bitSerialize)    
 While (Charindex(@SplitOn,@strAtrr)>0)    
 Begin    
  if  @Cnt=1 set @strSQL=@strSQL+' and fld_value=''' + (ltrim(rtrim(Substring(@strAtrr,1,Charindex(@SplitOn,@strAtrr)-1))))+''''    
  else set @strSQL=@strSQL+' or fld_value=''' + (ltrim(rtrim(Substring(@strAtrr,1,Charindex(@SplitOn,@strAtrr)-1))))+''''    
  Set @strAtrr = Substring(@strAtrr,Charindex(@SplitOn,@strAtrr)+1,len(@strAtrr))    
  Set @Cnt = @Cnt + 1    
 End    
 if @Cnt=1 set @strSQL=@strSQL+' and fld_value=''' + @strAtrr + ''' GROUP BY recid HAVING count(*) > '+convert(varchar(10), @Cnt-1)    
 else set @strSQL=@strSQL+' or fld_value=''' + @strAtrr + ''' GROUP BY recid HAVING count(*) > '+convert(varchar(10), @Cnt-1)    
end    
    
    
if @bitSerialize=0 and @numItemCode>0    
begin    
 if @strAtrr=''    
  select numListItemID,vcData from listdetails where numlistitemid in(select distinct(fld_value) from WareHouseItems W    
  join CFW_Fld_Values_Serialized_Items CSI on W.numWareHouseItemID=CSI.RecId    
  join CFW_Fld_Master M on CSI.Fld_ID=M.Fld_ID                   
  join ItemGroupsDTL on CSI.Fld_ID=ItemGroupsDTL.numOppAccAttrID    
  where  M.numListID=@numListID and tintType=2 and CSI.bitSerialized=0 and numItemID=@numItemCode)    
 else    
 begin    
  set @strSQL ='select numListItemID,vcData from listdetails where numlistitemid in(select distinct(fld_value) from WareHouseItems W    
  join CFW_Fld_Values_Serialized_Items CSI on W.numWareHouseItemID=CSI.RecId    
  join CFW_Fld_Master M on CSI.Fld_ID=M.Fld_ID    
  join ItemGroupsDTL on CSI.Fld_ID=ItemGroupsDTL.numOppAccAttrID   
  where  M.numListID='+convert(varchar(20),@numListID)+' and tintType=2 and CSI.bitSerialized=0 and numItemID='+convert(varchar(20),@numItemCode)+' and fld_value!=''0'' and fld_value!=''''    
  and numWareHouseItemID in ('+@strSQL+'))'    
  exec (@strSQL)    
 end    
        
end    
else if @bitSerialize=1 and @numItemCode>0    
begin    
 if @strAtrr=''    
  select numListItemID,vcData from listdetails where numlistitemid in(select distinct(fld_value) from WareHouseItems W    
  join WareHouseItmsDTL WDTL on WDTL.numWareHouseItemID=W.numWareHouseItemID    
  join CFW_Fld_Values_Serialized_Items CSI on WDTL.numWareHouseItmsDTLID=CSI.RecId    
  join CFW_Fld_Master M on CSI.Fld_ID=M.Fld_ID    
  join ItemGroupsDTL on CSI.Fld_ID=ItemGroupsDTL.numOppAccAttrID     
  where  M.numListID=@numListID and tintType=2 and ISNULL(WDTL.numQty,0) > 0 and CSI.bitSerialized=1 and numItemID=@numItemCode     
  and fld_value!='0' and fld_value!='')    
 else    
 begin    
  set @strSQL ='select numListItemID,vcData from listdetails where numlistitemid in(select distinct(fld_value) from WareHouseItems W    
  join WareHouseItmsDTL WDTL on WDTL.numWareHouseItemID=W.numWareHouseItemID    
  join CFW_Fld_Values_Serialized_Items CSI on WDTL.numWareHouseItmsDTLID=CSI.RecId    
  join CFW_Fld_Master M on CSI.Fld_ID=M.Fld_ID    
  join ItemGroupsDTL on CSI.Fld_ID=ItemGroupsDTL.numOppAccAttrID   
  where  M.numListID='+convert(varchar(20),@numListID)+' and tintType=2 and ISNULL(WDTL.numQty,0) > 0 and CSI.bitSerialized=1 and numItemID='+convert(varchar(20),@numItemCode)+'     
  and fld_value!=''0'' and fld_value!='''' and WDTL.numWareHouseItmsDTLID in     
  ('+@strSQL+'))'    
  exec (@strSQL)    
 end    
end    
else select 0
GO
