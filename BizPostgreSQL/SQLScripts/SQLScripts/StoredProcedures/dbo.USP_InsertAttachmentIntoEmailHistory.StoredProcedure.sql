/****** Object:  StoredProcedure [dbo].[USP_InsertAttachmentIntoEmailHistory]    Script Date: 07/26/2008 16:18:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created BY ANoop Jayaraj      
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_insertattachmentintoemailhistory')
DROP PROCEDURE usp_insertattachmentintoemailhistory
GO
CREATE PROCEDURE [dbo].[USP_InsertAttachmentIntoEmailHistory]      
@numEmailHstrID as numeric(9)=0, 
@fileName as varchar(500)='',
@vcLocation as varchar(1000)='' ,
@vcFileSize AS VARCHAR(200)='',
@vcFileType AS VARCHAR(200)='' 
as      
      
Insert into EmailHstrAttchDtls(numEmailHstrID,vcFileName,vcLocation,vcAttachmentType,vcSize)      
values      
(@numEmailHstrID,@fileName,@vcLocation,@vcFileType,@vcFileSize)
GO
