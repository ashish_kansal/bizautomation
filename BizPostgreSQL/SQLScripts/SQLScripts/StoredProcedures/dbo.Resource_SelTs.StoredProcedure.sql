/****** Object:  StoredProcedure [dbo].[Resource_SelTs]    Script Date: 07/26/2008 16:14:35 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
/*
**  Gets the timestamp of an individual Resource.
**
**  This stored procedure can be used to retrieve the timestamp of
**  a Resource by it's key, ID.
*/
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='resource_selts')
DROP PROCEDURE resource_selts
GO
CREATE PROCEDURE [dbo].[Resource_SelTs]
	@ResourceID		integer		-- primary key number of the resource
AS
BEGIN
	SELECT
		[_ts] 
	FROM 
		[Resource]
	WHERE
		( [Resource].[ResourceID] = @ResourceID );
END
GO
