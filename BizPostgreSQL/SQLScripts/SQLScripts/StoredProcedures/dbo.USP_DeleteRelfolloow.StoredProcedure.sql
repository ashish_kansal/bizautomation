/****** Object:  StoredProcedure [dbo].[USP_DeleteRelfolloow]    Script Date: 07/26/2008 16:15:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_deleterelfolloow')
DROP PROCEDURE usp_deleterelfolloow
GO
CREATE PROCEDURE [dbo].[USP_DeleteRelfolloow]    
@numRelFolID as numeric(9)    
as    
delete  from RelFollowupStatus  where numRelFolID=@numRelFolID
GO
