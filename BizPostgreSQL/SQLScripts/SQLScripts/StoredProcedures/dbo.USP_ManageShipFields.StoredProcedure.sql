/****** Object:  StoredProcedure [dbo].[USP_ManageShipFields]    Script Date: 07/26/2008 16:19:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_manageshipfields')
DROP PROCEDURE usp_manageshipfields
GO
CREATE PROCEDURE [dbo].[USP_ManageShipFields]
@numDomainID as numeric(9),
@numLIstItemID as numeric(9),
@str as text
as

delete from ShippingFieldValues where numDomainID=@numDomainID and numListItemID=@numLIstItemID
DECLARE @hDocItem int  
EXEC sp_xml_preparedocument @hDocItem OUTPUT, @str                       
   insert into                       
   ShippingFieldValues                                                                          
   (intShipFieldID,vcShipFieldValue,numDomainID,numListItemID)                      
   select X.intShipFieldID,X.vcShipFieldValue,@numDomainID,@numLIstItemID from(                                                                          
   SELECT *FROM OPENXML (@hDocItem,'/NewDataSet/ShipDTL',2)                                                                          
   WITH                       
   (                                                                          
   intShipFieldID integer,                                     
   vcShipFieldValue varchar(200)            
   ))X    

  EXEC sp_xml_removedocument @hDocItem
GO
