/****** Object:  StoredProcedure [dbo].[USP_GetContactsFromDomain]    Script Date: 07/26/2008 16:16:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj  
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getcontactsfromdomain')
DROP PROCEDURE usp_getcontactsfromdomain
GO
CREATE PROCEDURE [dbo].[USP_GetContactsFromDomain]  
@numDomainId as numeric(9)=0  
as  
	select numContactID,vcFirstname+' '+vcLastname as [Name] from AdditionalContactsInformation A  
	join Domain D  
	on D.numDomainID=A.numDomainID   
	where A.numDivisionID=D.numDivisionID  
	and D.numDomainID=@numDomainID
GO
