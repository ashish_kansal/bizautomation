/****** Object:  StoredProcedure [dbo].[usp_GetAverageSaleCycle]    Script Date: 07/26/2008 16:16:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- Created By Maha          
-- Last Modified By Anoop Jayaraj          
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getaveragesalecycle')
DROP PROCEDURE usp_getaveragesalecycle
GO
CREATE PROCEDURE [dbo].[usp_GetAverageSaleCycle]               
 @numDomainID numeric,                
 @dtDateFrom datetime,                
 @dtDateTo datetime,                
 @numUserCntID numeric=0,                            
 @intType numeric=0,              
 @tintRights TINYINT=1              
  --              
AS                
BEGIN                
 If @tintRights=1              
 --All Records Rights                
 BEGIN                
              
 SELECT vcFirstName +' '+vcLastName  as Employee,(select count(*)  from OpportunityMaster OM where OM.tintOpptype=1 and OM.tintOppstatus=1 
and OM.numRecOwner=A.numContactID and OM.numdomainid=@numDomainID and  OM.bintAccountClosingDate BETWEEN @dtDateFrom  
 AND @dtDateTo    
 ) AS DealsWon,              
 dbo.GetOppSuccesfullPercentage(numContactID,@numDomainID,@dtDateFrom,@dtDateTo)  AS Closing,        
 dbo.GetOppAmount(numContactID,@numDomainID,@dtDateFrom,@dtDateTo) as Amount              
 FROM AdditionalContactsInformation A where A.numContactID=@numUserCntID        
          
              
 END                
                
 If @tintRights=2              
 BEGIN                
              
 SELECT vcFirstName +' '+vcLastName  as Employee,  
(select count(*)  from OpportunityMaster OM where OM.tintOpptype=1 and OM.tintOppstatus=1 and OM.numRecOwner=A.numContactID and   
OM.numdomainid=@numDomainID and  OM.bintAccountClosingDate BETWEEN @dtDateFrom  
 AND @dtDateTo    
 ) AS DealsWon,              
 dbo.GetOppSuccesfullPercentage(numContactID,@numDomainID,@dtDateFrom,@dtDateTo)  AS Closing,        
 dbo.GetOppAmount(numContactID,@numDomainID,@dtDateFrom,@dtDateTo) as Amount              
 FROM AdditionalContactsInformation A where A.numTeam in(select F.numTeam from ForReportsByTeam F                   
 where F.numuserCntId=@numUserCntID and F.numDomainid=@numDomainId and F.tintType=@intType )                     
              
 END                
                
 If @tintRights=3              
 BEGIN                
	SELECT 
		vcFirstName + ' ' + vcLastName  AS Employee
		,(
			SELECT 
				COUNT(*) 
			FROM 
				OpportunityMaster OM 
			INNER JOIN 
				DivisionMaster DM 
			ON 
				OM.numDivisionId=DM.numDivisionID
			WHERE 
				OM.tintOpptype=1 
				AND OM.tintOppstatus=1 
				AND OM.numRecOwner=A.numContactID 
				AND OM.numdomainid=@numDomainID
				AND OM.bintAccountClosingDate BETWEEN  @dtDateFrom AND @dtDateTo
		) AS DealsWon
		,dbo.GetOppSuccesfullPercentage(numContactID,@numDomainID,@dtDateFrom,@dtDateTo)  AS Closing
		,dbo.GetOppAmount(numContactID,@numDomainID,@dtDateFrom,@dtDateTo) as Amount              
	FROM 
		AdditionalContactsInformation A  
	LEFT OUTER JOIN 
		DivisionMaster DM 
	ON 
		A.numDivisionId = DM.numDivisionID  
	WHERE 
		DM.numTerId IN (SELECT F.numTerritory FROM ForReportsByTerritory F WHERE F.numuserCntId=@numUserCntID and F.numDomainid=@numDomainID and F.tintType=@intType)              
              
 END                
 END
GO
