/****** Object:  StoredProcedure [dbo].[USP_UpdateJournalDetails]    Script Date: 07/26/2008 16:21:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
---Created By Siva                                            
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_updatejournaldetails')
DROP PROCEDURE usp_updatejournaldetails
GO
CREATE PROCEDURE [dbo].[USP_UpdateJournalDetails]                                                    
@numTransactionId as numeric(9)=0,                                                   
@numDebitAmt as DECIMAL(20,5),                                                    
@numCreditAmt as DECIMAL(20,5),                                                    
@varDescription as varchar(8000),                                                    
@numCustomerId as numeric(9)=0,                                                   
@numJournalId as numeric(9)=0,                                               
@datEntry_Date as datetime,                                              
@numUpdateType as numeric(9)=0,                                  
@numBalance as DECIMAL(20,5),                              
@numCheckId as numeric(9)=0,                            
@numCashCreditCardId as numeric(9)=0,      
@numDomainId as numeric(9)=0                                                
as                                                     
Begin               
Declare @bitOpeningBalance as bit                  
Declare @AccountId as numeric(9)                                                 
--Create table #tempTable ( numTransactionId numeric(9),                                                                                                          
--      numJournalId numeric(9),                                                                
--      numDebitAmt DECIMAL(20,5),                                                                
--      numCreditAmt DECIMAL(20,5),                                                                
--      numChartAcntId numeric(9),                                                                
--      varDescription varchar(8000),                                                                
--      numCustomerId numeric(9),                                                                
--      numDomainId numeric(9),                                                                
--      numBalance DECIMAL(20,5))                                                                 
--                                                                
-- Insert into #tempTable (numTransactionId,numJournalId,numDebitAmt,numCreditAmt,numChartAcntId,varDescription,                                                                
--     numCustomerId,numDomainId,numBalance)  Select numTransactionId,numJournalId,numDebitAmt,numCreditAmt,numChartAcntId,varDescription,                                                                
--     numCustomerId,numDomainId,numBalance                                                                
--      from  General_Journal_Details Where numJournalId=@numJournalId And numDomainId=@numDomainId                                                     
--To Update Entry_Date                                              
Update General_Journal_Header Set datEntry_Date=@datEntry_Date,numAmount=@numDebitAmt Where numJournal_Id=@numJournalId And numDomainId=@numDomainId      
                                              
Update General_Journal_Details  Set numDebitAmt=@numDebitAmt,numCreditAmt=@numCreditAmt,                                                      
varDescription=@varDescription,numCustomerId=@numCustomerId,numBalance=@numBalance                                                       
Where numTransactionId=@numTransactionId And numDomainId=@numDomainId             
            
                         
            
                 
--To Update Check Details Amount                              
if @numCheckId<>0                               
Begin                              
Update CheckDetails Set datCheckDate=@datEntry_Date,varMemo=@varDescription Where numCheckId=@numCheckId And numDomainId=@numDomainId                             
End                         
                               
                        
-- To update CashCreditCard Amount                            
                            
if @numCashCreditCardId<>0                
Begin                            
Update CashCreditCardDetails Set datEntry_Date=@datEntry_Date,vcMemo=@varDescription,numPurchaseId=@numCustomerId Where numCashCreditId=@numCashCreditCardId And numDomainId=@numDomainId                           
End                            
                             
                                    
if @numUpdateType=1                                                 
Begin                                                   
if @numDebitAmt=0                                                   
Begin                       
Update General_Journal_Details  Set numDebitAmt=@numCreditAmt,numCreditAmt=0,numBalance=@numCreditAmt                                                 
Where numTransactionId<>@numTransactionId And numJournalId=@numJournalId And numDomainId=@numDomainId                                  
                              
--To Update Check Details Amount                              
if @numCheckId<>0                              
Begin                              
Update CheckDetails Set monCheckAmt=@numCreditAmt Where numCheckId=@numCheckId And numDomainId=@numDomainId             
Update CheckCompanyDetails Set monAmount=@numCreditAmt, vcMemo=@varDescription Where numCheckCompanyId=(Select numCheckCompanyId From CheckDetails Where numCheckId=@numCheckId And numDomainId=@numDomainId) And numDomainId=@numDomainId      
End                              
                                 
-- To update CashCreditCard Amount                            
                            
if @numCashCreditCardId<>0                             
Begin                            
Update CashCreditCardDetails Set monAmount=@numCreditAmt Where numCashCreditId=@numCashCreditCardId And numDomainId=@numDomainId            
            
-- To update Opening Balance in Chart_of_Accounts table            
            
Select @AccountId=isnull(numAcntTypeId,0) From CashCreditCardDetails Where numCashCreditId=@numCashCreditCardId And numDomainId=@numDomainId              
Select @bitOpeningBalance=isnull(bitOpeningBalance,0) From  General_Journal_Header Where numJournal_Id=@numJournalId And numDomainId=@numDomainId           
if @bitOpeningBalance=1                  
 Begin                  
    Update Chart_of_Accounts Set numOriginalOpeningBal=@numCreditAmt Where numAccountId=@AccountId  And numDomainId=@numDomainId                     
 End                   
                           
End                           
End                                                  
                              
if @numCreditAmt=0                                                   
Begin                                                  
Update General_Journal_Details  Set numDebitAmt=0,numCreditAmt=@numDebitAmt,numBalance=@numCreditAmt                                                      
Where numTransactionId<>@numTransactionId And numJournalId=@numJournalId  And numDomainId=@numDomainId                                                    
                              
--To Update Check Details Amount                              
if @numCheckId<>0                               
Begin                              
--Update CheckDetails Set monCheckAmt=@numDebitAmt Where numCheckId=@numCheckId                              
Update CheckDetails Set monCheckAmt=@numDebitAmt,datCheckDate=@datEntry_Date,varMemo=@varDescription,numCustomerId=@numCustomerId                           
Where numCheckId=@numCheckId  And numDomainId=@numDomainId                           
End                              
                            
-- To update CashCreditCard Amount                            
                            
if @numCashCreditCardId<>0                             
Begin                            
--Update CashCreditCardDetails Set monAmount=@numDebitAmt Where numCashCreditId=@numCashCreditCardId                            
Update CashCreditCardDetails Set monAmount=@numDebitAmt,datEntry_Date=@datEntry_Date,vcMemo=@varDescription,numPurchaseId=@numCustomerId                           
Where numCashCreditId=@numCashCreditCardId  And numDomainId=@numDomainId                           
End                 
                       
-- To update Opening Balance in Chart_of_Accounts table            
             
Select @AccountId=isnull(numAcntTypeId,0) From CashCreditCardDetails Where numCashCreditId=@numCashCreditCardId And numDomainId=@numDomainId                 
Select @bitOpeningBalance=isnull(bitOpeningBalance,0) From  General_Journal_Header Where numJournal_Id=@numJournalId And numDomainId=@numDomainId           
if @bitOpeningBalance=1                  
 Begin                  
 Update Chart_of_Accounts Set numOriginalOpeningBal=@numDebitAmt Where numAccountId=@AccountId  And numDomainId=@numDomainId                     
 End                            
                              
End                                     
                                            
--                                            
--Declare @numChartAcntId as  varchar(8000)                                                          
--Declare @numTransactionId1 as numeric(9)                                                        
--Declare @numDebitAmtBefSave as DECIMAL(20,5)                                                          
--Declare @numCreditAmtBefSave as DECIMAL(20,5)                                                         
--Declare @strSQl as varchar(8000)                                                         
--Declare @strSQl1 as varchar(8000)              
--Declare @strSQl2 as varchar(8000)                     
--Declare @strSQlEquity as varchar(8000)                     
--                           
--Declare @numChartAcntTypeId as numeric(9)                                                  
--Set @strSQl1=''                                                         
--Set @strSQl=''                                                          
--Set @strSQl2=''                        
--Set @strSQlEquity=''                     
--                                            
--                                                           
--Select @numTransactionId1=min(numTransactionId) From General_Journal_Details Where numJournalId=@numJournalId  And numDomainId=@numDomainId                                                             
--                                                            
--While @numTransactionId1 <> 0                                                            
--Begin                                                            
--                                                               
--   Select @numChartAcntId=numChartAcntId,@numDebitAmtBefSave=numDebitAmt,@numCreditAmtBefSave=numCreditAmt From #tempTable Where numTransactionId=@numTransactionId1                                                             
--   Select * from #tempTable                                                  
--   print @numChartAcntId                                                        
--   Set @strSQl=dbo.fn_ParentCategory(@numChartAcntId,@numDomainId)                        
--Select @numChartAcntTypeId =numAcntTypeID  From Chart_Of_Accounts Where numAccountId=@numChartAcntId  And numDomainId=@numDomainId                           
--   print @numChartAcntTypeId                                       
--    Print('STRSQL'+@strSQl)                                                             
--    if @numDebitAmtBefSave <> 0                                                
--    Begin                                                          
--      if @strSQl <>''                          
--        Begin                          
--          if @numChartAcntTypeId = 813 Or @numChartAcntTypeId = 814 Or @numChartAcntTypeId = 817 Or @numChartAcntTypeId = 818 Or @numChartAcntTypeId = 819 Or @numChartAcntTypeId = 823 Or @numChartAcntTypeId = 824 Or @numChartAcntTypeId = 826             
--   
--    
--      
--       
--          
--            
--              
--           set @strSQl2='update chart_of_accounts  set numopeningbal=isnull(numopeningbal,0) - ' + convert(varchar(30),@numDebitAmtBefSave) + ' where  numAccountId in ('+@numChartAcntId+','+@strSQl+') And numDomainId='+Convert(varchar(10),@numDomainId)   
--  
--    
--                                          
--          else                      
--            set @strSQl2='update chart_of_accounts  set numopeningbal=isnull(numopeningbal,0) + ' + convert(varchar(30),@numDebitAmtBefSave) + ' where  numAccountId in ('+@numChartAcntId+','+@strSQl+') And numDomainId='+Convert(varchar(10),@numDomainId)  
--  
--    
--                                            
--            End                      
--      else                       
--         Begin                          
--         if @numChartAcntTypeId = 813 Or @numChartAcntTypeId = 814 Or @numChartAcntTypeId = 817 Or @numChartAcntTypeId = 818 Or @numChartAcntTypeId = 819 Or @numChartAcntTypeId = 823 Or @numChartAcntTypeId = 824 Or @numChartAcntTypeId = 826               
--  
--    
--      
--        
--          
--            
--            set @strSQl2='update chart_of_accounts  set numopeningbal=isnull(numopeningbal,0) - ' + convert(varchar(30),@numDebitAmtBefSave) + ' where  numAccountId in ('+@numChartAcntId+') And numDomainId='+Convert(varchar(10),@numDomainId)              
--  
--    
--         
--          else                      
--             set @strSQl2='update chart_of_accounts  set numopeningbal=isnull(numopeningbal,0) + ' + convert(varchar(30),@numDebitAmtBefSave) + ' where  numAccountId in ('+@numChartAcntId+')And numDomainId='+Convert(varchar(10),@numDomainId)              
--  
--    
--       
--          End                    
-- print(@strSQl2)                                                                           
--     Exec(@strSQl2)                     
--                                                             
--    End                                                            
--    if @numCreditAmtBefSave <> 0                                                             
--    Begin                       
--                                                            
--    if @strSQl<>''                           
--      Begin                          
--         If @numChartAcntTypeId = 813 Or @numChartAcntTypeId = 814 Or @numChartAcntTypeId = 817 Or @numChartAcntTypeId = 818 Or @numChartAcntTypeId = 819 Or @numChartAcntTypeId = 823 Or @numChartAcntTypeId = 824 Or @numChartAcntTypeId = 826               
--  
--    
--     
--         
--          
--            
--            set @strSQl1='update chart_of_accounts  set numopeningbal=isnull(numopeningbal,0) + ' + convert(varchar(30),@numCreditAmtBefSave) + ' where  numAccountId in ('+@numChartAcntId+','+@strSQl+') And numDomainId='+Convert(varchar(10),@numDomainId) 
--  
--    
--                            
--         Else                    
--              set @strSQl1='update chart_of_accounts  set numopeningbal=isnull(numopeningbal,0) - ' + convert(varchar(30),@numCreditAmtBefSave) + ' where  numAccountId in ('+@numChartAcntId+','+@strSQl+') And numDomainId='+Convert(varchar(10),@numDomainId
--  
--    
--)                            
--       End                      
--    else                        
--      Begin                          
-- If @numChartAcntTypeId = 813 Or @numChartAcntTypeId = 814 Or @numChartAcntTypeId = 817 Or @numChartAcntTypeId = 818 Or @numChartAcntTypeId = 819 Or @numChartAcntTypeId = 823 Or @numChartAcntTypeId = 824 Or @numChartAcntTypeId = 826                       
--  
--    
--            set @strSQl1='update chart_of_accounts  set numopeningbal=isnull(numopeningbal,0) + ' + convert(varchar(30),@numCreditAmtBefSave) + ' where  numAccountId in ('+@numChartAcntId+') And numDomainId='+Convert(varchar(10),@numDomainId)             
--  
--    
--                                                         
--        
--          
--            
--             
--         Else                      
--             set @strSQl1='update chart_of_accounts  set numopeningbal=isnull(numopeningbal,0) - ' + convert(varchar(30),@numCreditAmtBefSave) + ' where  numAccountId in ('+@numChartAcntId+') And numDomainId='+Convert(varchar(10),@numDomainId)            
--  
--    
--                        
--        
--          
--            
--              
--         End                       
--         print(@strSQl1)                                                    
--    Exec(@strSQl1)                          
--    End                                                            
--                                                             
--    print (@numChartAcntId)                                                            
--                           
--   Select @numTransactionId1=min(numTransactionId) From General_Journal_Details Where numJournalId=@numJournalId And  numTransactionId>@numTransactionId1                                                            
--   Print (@numTransactionId1)                                      
--End                                                            
--                
--                                               
--Exec USP_UpdateChartAcntOpnBalance @JournalId=@numJournalId,@numDomainId=@numDomainId                               
--drop table #tempTable                                              
                                                
End                                                    
End
GO
