/****** Object:  StoredProcedure [dbo].[usp_InsertAlertRules]    Script Date: 07/26/2008 16:18:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_insertalertrules')
DROP PROCEDURE usp_insertalertrules
GO
CREATE PROCEDURE [dbo].[usp_InsertAlertRules]
	@emailAlertID NUMERIC(9)=0, 
	@numContactid NUMERIC(9)=0,
	@numalerttype SMALLINT=1,
	@numage INT=0,
	@numbirthdate BIGINT=0,
	@charemailid VARCHAR(50)='',
	@numCreatedBy NUMERIC(18)=1,
	@bintCreatedDate BIGINT=0,
	@charFlag char(1)  --This is for checking Insert(I),Delete(D),Update(U)
--
AS
BEGIN
	IF (@charFlag='D')
	BEGIN
		UPDATE emailAlert set alertDeletedBy=@numCreatedBy,alertDeletedDate=@bintCreatedDate where emailalertid=@emailAlertID
	
	END
	ELSE IF (@charFlag='U')
	BEGIN
		UPDATE emailAlert set numContactId=@numContactid,alerttype=@numalerttype,age=@numage,
		emailid=@charemailid,alertModifiedBy=@numCreatedBy,alertModifiedDate=@bintCreatedDate where emailalertid=@emailAlertID
	END
	ELSE
	BEGIN
		INSERT INTO emailalert VALUES
		(@numContactid,@numalerttype,@numage,@charemailid,@numCreatedBy,@bintCreatedDate,0,0,0,0,0)
	END
END
GO
