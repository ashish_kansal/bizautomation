GO
/****** Object:  StoredProcedure [dbo].[USP_ManageOrderAutoRule]    Script Date: 03/04/2010 15:35:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_manageorderautorule')
DROP PROCEDURE usp_manageorderautorule
GO
CREATE PROCEDURE [dbo].[USP_ManageOrderAutoRule]
@numMode int,
@numDomainID int,
@numRuleID int,
@numBillToID int,
@numShipToID int,
@btFullPaid bit,
@numBizDocStatus numeric,
@btActive bit

as

if @numMode=0 
	begin
		delete from OrderAutoRuleDetails where numRuleID=@numRuleID
		insert into OrderAutoRule select @numDomainID,@numBillToID,@numShipToID,@btFullPaid,@numBizDocStatus,@btActive
		select @@Identity
	end
else if @numMode=1
	begin
		delete from OrderAutoRuleDetails where numRuleID=@numRuleID
		update OrderAutoRule set numBillToID=@numBillToID,numShipToID=@numShipToID,btFullPaid=@btFullPaid,numBizDocStatus=@numBizDocStatus,btActive=@btActive where numDomainID=@numDomainID and numRuleID=@numRuleID
		select @numRuleID
	end
else if @numMode=2
	begin
		delete from OrderAutoRuleDetails where numRuleID=@numRuleID
		DELETE FROM OrderAutoRule WHERE  numDomainID=@numDomainID and numRuleID=@numRuleID
		select @numRuleID
	end