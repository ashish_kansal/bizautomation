/****** Object:  StoredProcedure [dbo].[usp_getCustReportsummationlist]    Script Date: 07/26/2008 16:17:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getcustreportsummationlist')
DROP PROCEDURE usp_getcustreportsummationlist
GO
CREATE PROCEDURE [dbo].[usp_getCustReportsummationlist]
@ReportId as numeric =0  
as
select * from CustReportSummationlist where numCustomReportId = @ReportId
GO
