/****** Object:  StoredProcedure [dbo].[USP_GetChartAcntDetailsForCashFlow]    Script Date: 07/26/2008 16:16:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- Created by Siva                                                        
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getchartacntdetailsforcashflow')
DROP PROCEDURE usp_getchartacntdetailsforcashflow
GO
CREATE PROCEDURE [dbo].[USP_GetChartAcntDetailsForCashFlow]                                                
@numDomainId as numeric(9),                                            
@dtFromDate as datetime,                                          
@dtToDate as datetime,        
@tintByteMode as tinyint                                                      
As                                                          
Begin                                                          
 Declare @strSQL as varchar(2000)                                          
 Declare @i as integer                                      
 Set @strSQL=''    
     
   
  Declare @numParentAccountId as numeric(9)      
  Set @numParentAccountId=(Select numAccountId From Chart_Of_Accounts Where numParntAcntTypeId is null and numAcntTypeID is null and numDomainId = @numDomainId) --and numAccountId = 1       
                                          
 Select @i=count(*) From General_Journal_Header GJH Where --GJH.datEntry_Date>='' + convert(varchar(300),@dtFromDate) +''  And                                 
 GJH.numDomainId=@numDomainId And          
    GJH.datEntry_Date<='' + convert(varchar(300),@dtToDate) +''           
     
 If @tintByteMode=1        
  Begin                             
   If @i=0                                    
    Begin                                    
       Set @strSQL = ' Select distinct  COA.numAcntType as numAcntType  From Chart_Of_Accounts COA                        
      Where  COA.numDomainId=' + Convert(varchar(5),@numDomainId) + 'And COA.numAccountId <>'+convert(varchar(10),@numParentAccountId)+' And COA.numParntAcntId='+convert(varchar(10),@numParentAccountId)  
      +' And (COA.numAcntType=814 Or COA.numAcntType=815 Or COA.numAcntType=816 Or COA.numAcntType=817 Or COA.numAcntType=827)'                                          
       Set @strSQL=@strSQL+ '  And COA.dtOpeningDate>=''' + convert(varchar(300),@dtFromDate) +''' And  COA.dtOpeningDate<=''' + convert(varchar(300),@dtToDate) +''''                                             
    End                                     
   Else                                    
    Begin                                    
       Set @strSQL = ' Select distinct COA.numAcntType as numAcntType  From Chart_Of_Accounts COA                       
     Where  COA.numDomainId=' + Convert(varchar(5),@numDomainId) + 'And COA.numAccountId <>'+convert(varchar(10),@numParentAccountId)+' And CoA.numParntAcntId='+convert(varchar(10),@numParentAccountId)  
     +' And(COA.numAcntType=814 Or COA.numAcntType=815 Or COA.numAcntType=816 Or COA.numAcntType=817 Or COA.numAcntType=827)'                                          
    End                                    
  End        
  if @tintByteMode=2        
   Begin                             
    if @i=0                                    
     Begin                                    
      Set @strSQL = ' Select distinct  COA.numAcntType as numAcntType  From Chart_Of_Accounts COA                        
       Where  COA.numDomainId=' + Convert(varchar(5),@numDomainId) + 'And COA.numAccountId <>'+convert(varchar(10),@numParentAccountId)+' And COA.numParntAcntId ='+convert(varchar(10),@numParentAccountId)  
       +' And  (COA.numAcntType=818 Or COA.numAcntType=819)'                                          
          Set @strSQL=@strSQL+ '  And COA.dtOpeningDate>=''' + convert(varchar(300),@dtFromDate) +''' And  COA.dtOpeningDate<=''' + convert(varchar(300),@dtToDate) +''''                                             
     End                                     
    Else                                    
     Begin                                    
      Set @strSQL = ' Select distinct COA.numAcntType as numAcntType  From Chart_Of_Accounts COA                       
      Where  COA.numDomainId=' + Convert(varchar(5),@numDomainId) + 'And COA.numAccountId <>'+convert(varchar(10),@numParentAccountId)+' And CoA.numParntAcntId='+convert(varchar(10),@numParentAccountId)                           
      +' And(COA.numAcntType=818 Or COA.numAcntType=819)'                                          
    End                                    
   End        
        
  if @tintByteMode=3        
   Begin                             
    if @i=0                                    
     Begin                                    
        Set @strSQL = ' Select distinct  COA.numAcntType as numAcntType  From Chart_Of_Accounts COA                        
       Where  COA.numDomainId=' + Convert(varchar(5),@numDomainId) + 'And COA.numAccountId <>'+convert(varchar(10),@numParentAccountId)+' And COA.numParntAcntId='+convert(varchar(10),@numParentAccountId)  
       +' And (COA.numAcntType=820 Or COA.numAcntType=821) '                                          
        Set @strSQL=@strSQL+ '  And COA.dtOpeningDate>=''' + convert(varchar(300),@dtFromDate) +''' And  COA.dtOpeningDate<=''' + convert(varchar(300),@dtToDate) +''''                                             
     End                                     
    Else                                    
     Begin                                    
        Set @strSQL = ' Select distinct COA.numAcntType as numAcntType  From Chart_Of_Accounts COA                       
      Where  COA.numDomainId=' + Convert(varchar(5),@numDomainId) + 'And COA.numAccountId <>'+convert(varchar(10),@numParentAccountId)+ ' And CoA.numParntAcntId='+convert(varchar(10),@numParentAccountId)  
      +' And  (COA.numAcntType=820 Or COA.numAcntType=821)'                                          
     End                                    
   End        
   print @strSQL                                          
   exec (@strSQL)                                          
 End
GO
