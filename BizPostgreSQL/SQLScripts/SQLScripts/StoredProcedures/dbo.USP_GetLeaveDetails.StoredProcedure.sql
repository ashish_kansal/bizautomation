/****** Object:  StoredProcedure [dbo].[USP_GetLeaveDetails]    Script Date: 07/26/2008 16:17:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--exec USP_GetLeaveDetails 1,1,'1/1/2000','1/1/2010'
--created by anoop jayaraj
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getleavedetails')
DROP PROCEDURE usp_getleavedetails
GO
CREATE PROCEDURE [dbo].[USP_GetLeaveDetails]
@numUserCntID as numeric(9)=0,
@numDomainID as numeric(9)=0,
@FromDate as datetime,
@ToDate as datetime
as	

	select datediff(dd,dtFromDate,dateadd(dd,1,dtToDate))-
	(case when bitFromFullDay=1 then 0 else .5 end )- 
	(case when bitToFullDay=1 then 0 else .5 end) as NoOfdays,dtFromDate,dtToDate,
	numType,bitApproved,bitFromFullDay,bitToFullDay,
	txtDesc,A.vcFirstName+' '+A.vcLastName as [Name],
	case when MGR.vcFirstName is null then '-' else  MGR.vcFirstName+' '+MGR.vcLastName end as [MGRName],isnull(U1.numUserID,0) as MGRUserID,numCategoryHDRID 
	from TimeAndExpense T 
        join UserMaster U
        on U.numUserDetailId=T.numUserCntID
	join AdditionalContactsInformation A
  	on A.numContactID=U.numUserDetailId
        left join AdditionalContactsInformation MGR
        on MGR.numContactID=A.numManagerID
        left join Usermaster U1
        on MGR.numContactID=U1.numUserDetailId
       	where numCategory=3  and ((dtFromDate between @FromDate and @ToDate) or (dtToDate between @FromDate and @ToDate))
        and T.numUserCntID=@numUserCntID
GO
