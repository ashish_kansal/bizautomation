/****** Object:  StoredProcedure [dbo].[USP_ProcurementBudgetForOpenDeal]    Script Date: 07/26/2008 16:20:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created By Siva    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_procurementbudgetforopendeal')
DROP PROCEDURE usp_procurementbudgetforopendeal
GO
CREATE PROCEDURE [dbo].[USP_ProcurementBudgetForOpenDeal]    
(@numItemCode as numeric(9)=0,    
 @numDomainId as numeric(9)=0,    
 @bitPurchaseDeal as bit output,    
 @monmonthlyAmt as float=0 Output,    
 @monTotalYearlyAmt as float=0 Output    
 )    
As    
Begin    
 
 Declare @OppMonthAmount as DECIMAL(20,5)    
 Declare @OppAmount as DECIMAL(20,5)    
 Declare @BudgetAmt as DECIMAL(20,5)    
 Declare @TotalBudgetAmt as DECIMAL(20,5)    
 Declare @ItemGroupId as numeric(9)    
 --Select @numProcurementBudgetId=numProcurementBudgetId,@bitPurchaseDeal=bitPurchaseDeal From ProcurementBudgetMaster Where  intFiscalYear=year(getutcdate()) And numDomainId=@numDomainId  
 Select @bitPurchaseDeal=bitPurchaseDeal From ProcurementBudgetMaster Where  numDomainId=@numDomainId    --intFiscalYear=year(getutcdate()) And
 Select @ItemGroupId=isnull(numItemGroup,0) From Item Where numItemCode=@numItemCode And numDomainID=@numDomainID    
 If @bitPurchaseDeal=1    
 Begin    
      
  Select @OppMonthAmount= sum(monPAmount) From OpportunityMaster Opp    
  inner join OpportunityItems OI on Opp.numOppId=OI.numOppId     
  Where Opp.tintOppType=2 And Opp.tintOppStatus=1 And OI.numItemCode=@numItemCode And Opp.numDomainId=@numDomainId    
  And month(Opp.bintAccountClosingDate)=month(getutcdate()) and Year(Opp.bintAccountClosingDate)=Year(getutcdate())    
    
  Select @OppAmount= sum(monPAmount) From OpportunityMaster Opp    
  inner join OpportunityItems OI on Opp.numOppId=OI.numOppId     
  Where Opp.tintOppType=2 And Opp.tintOppStatus=1 And OI.numItemCode=@numItemCode And Opp.numDomainId=@numDomainId      
  And month(Opp.bintAccountClosingDate)<=month(getutcdate()) and Year(Opp.bintAccountClosingDate)=Year(getutcdate())    
    
  Select @BudgetAmt=isnull(monAmount,0) From ProcurementbudgetMaster PBM    
  inner join ProcurementBudgetDetails PBD on PBM.numProcurementBudgetId=PBD.numProcurementId    
  Where PBD.intYear=year(getutcdate()) And PBD.tintMonth=month(getutcdate()) And PBD.numItemGroupId=@ItemGroupId And PBM.numDomainId=@numDomainId

   Select @TotalBudgetAmt=sum(isnull(monAmount,0)) From ProcurementbudgetMaster PBM    
  inner join ProcurementBudgetDetails PBD on PBM.numProcurementBudgetId=PBD.numProcurementId    
  Where PBD.intYear=year(getutcdate()) And PBD.numItemGroupId=@ItemGroupId And PBM.numDomainId=@numDomainId

   
    
----     select @BudgetAmt=(case when  month(getutcdate())=1 then monJan       
----  when month(getutcdate())=2 then monFeb    
----  when month(getutcdate())=3 then monMar    
----  when month(getutcdate())=4 then monApr    
----  when month(getutcdate())=5 then monMay    
----  when month(getutcdate())=6 then monJune    
----  when month(getutcdate())=7 then monJuly    
----  when month(getutcdate())=8 then monAug    
----  when month(getutcdate())=9 then monSep    
----  when month(getutcdate())=10 then monOct    
----  when month(getutcdate())=11 then monNov    
----  when month(getutcdate())=12 then monDec    
----  End) ,@TotalBudgetAmt=monTotal From ProcurementbudgetMaster PBM    
----  inner join ProcurementBudgetDetails PBD on PBM.numProcurementBudgetId=PBD.numProcurementId    
----  Where PBM.intFiscalYear=year(getutcdate()) And PBD.numItemGroupId=@ItemGroupId    
      
  Set @monmonthlyAmt = isnull(@BudgetAmt,0)-isnull(@OppMonthAmount,0)  
  print '@monmonthlyAmt=='+ Convert(varchar(10),@monmonthlyAmt)    
--  print  @TotalBudgetAmt    
  Set @monTotalYearlyAmt=isnull(@TotalBudgetAmt,0)-isnull(@OppAmount,0)  
   Print '@monTotalYearlyAmt=='+ Convert(varchar(10),@monTotalYearlyAmt)    
  Set @bitPurchaseDeal=@bitPurchaseDeal    
    END    
       
End
GO
