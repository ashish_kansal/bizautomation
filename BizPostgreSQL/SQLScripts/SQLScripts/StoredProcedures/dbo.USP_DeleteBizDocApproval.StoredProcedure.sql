GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_deletebizdocapproval')
DROP PROCEDURE usp_deletebizdocapproval
GO
CREATE PROCEDURE [dbo].[USP_DeleteBizDocApproval]
@numDomainID int
as
DELETE FROM BizDocApprovalRuleEmployee WHERE numBizDocAppID 
   in (select numBizDocAppID from BizDocApprovalRule where numDomainID=@numDomainID);

   DELETE FROM BizDocStatusApprove WHERE numDomainID=@numDomainID;
   DELETE FROM BizDocApprovalRule WHERE numDomainID=@numDomainID;
   SELECT 1