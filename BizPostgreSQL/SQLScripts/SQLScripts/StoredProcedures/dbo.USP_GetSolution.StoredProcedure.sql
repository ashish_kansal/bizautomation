/****** Object:  StoredProcedure [dbo].[USP_GetSolution]    Script Date: 07/26/2008 16:18:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getsolution')
DROP PROCEDURE usp_getsolution
GO
CREATE PROCEDURE [dbo].[USP_GetSolution]        
@numCategory as numeric(9)=null,        
@KeyWord as varchar(50),        
@SortChar char(1)='0',        
@columnName as Varchar(50),        
@columnSortOrder as Varchar(10),        
@TotRecs int output,    
@numDomainID as numeric(9)=0        
as        
declare @strSql as varchar(8000)        
set @strSql='select numSolnID,        
 vcSolnTitle,txtSolution         
 from SolutionMaster         
 where numDomainID='+Convert(varchar(15),@numDomainID)

if @KeyWord<>'' set @strSql=@strSql+'and (vcSolnTitle like +''%'+ @KeyWord +'%''        
 or txtSolution like +''%'+ @KeyWord +'%'')'        
      
if @numCategory<> 0 set @strSql=@strSql + ' and  numCategory='+ convert(varchar(15),@numCategory)       
if @SortChar<>'0' set @strSql=@strSql + ' And vcSolnTitle like '''+@SortChar+'%'''         
if @columnName is not null set @strSql=@strSql + ' ORDER BY ' + @columnName +' '+ @columnSortOrder        
      
exec (@strSql)        
        
set @TotRecs=@@rowcount
GO
