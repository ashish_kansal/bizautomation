/****** Object:  StoredProcedure [dbo].[USP_GetProfitLossDetailsForCashFlow]    Script Date: 07/26/2008 16:18:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- Created by Siva                                                            
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getprofitlossdetailsforcashflow')
DROP PROCEDURE usp_getprofitlossdetailsforcashflow
GO
CREATE PROCEDURE [dbo].[USP_GetProfitLossDetailsForCashFlow]                                                          
@numDomainId as numeric(9),                                                
@dtFromDate as datetime,                                              
@dtToDate as datetime                                                           
As                                                              
Begin                                                              
 Declare @strSQL as varchar(2000)                                              
 Declare @i as integer                                          
 Set @strSQL=''              
   Declare @numParntAcntId as numeric(9)            
  Set @numParntAcntId=(Select numAccountId From Chart_Of_Accounts Where [numParntAcntTypeId] is null and [numAcntTypeId] is null and numDomainId = @numDomainId) --and numAccountId = 1             
                                              
 Select @i=count(*) From General_Journal_Header GJH Where --GJH.datEntry_Date>='' + convert(varchar(300),@dtFromDate) +''  And                                     
 numDomainId=@numDomainId And              
 GJH.datEntry_Date<='' + convert(varchar(300),@dtToDate) +''                                    
 if @i=0                                        
  Begin                                        
   Set @strSQL = ' Select COA.numAcntTypeId as numAcntType,COA.numParntAcntTypeId as numParntAcntId,          
     COA.numAccountId as numAccountId,COA.vcAccountName as AcntTypeDescription,                                  
     dbo.fn_GetCurrentOpeningBalanceForProfitLossForCashFlow(COA.numAccountId,'''+Convert(varchar(300),@dtFromDate)+''','''+convert(varchar(300), @dtToDate)+''','+Convert(varchar(10),@numDomainId)+') As Amount           
              From Chart_Of_Accounts COA            
  inner join ListDetails LD on COA.numAcntTypeId=LD.numListItemID                            
       Where COA.numDomainId=' + Convert(varchar(5),@numDomainId) + 'And COA.numAccountId <>'+Convert(varchar(10),@numParntAcntId) +                             
       'And (COA.numAcntTypeId=822 Or COA.numAcntTypeId=823 Or COA.numAcntTypeId=824 Or COA.numAcntTypeId=825 Or COA.numAcntTypeId=826 )'                                              
   Set @strSQL=@strSQL+ '  And COA.dtOpeningDate>=''' + convert(varchar(300),@dtFromDate) +''' And  COA.dtOpeningDate<=''' + convert(varchar(300),@dtToDate) +''''                                                 
  End                                         
  Else                                        
   Begin                                        
    Set @strSQL = ' Select COA.numAcntTypeId as numAcntType,COA.numParntAcntTypeId as numParntAcntId,           
   COA.numAccountId as numAccountId,COA.vcAccountName as AcntTypeDescription,                                  
   dbo.fn_GetCurrentOpeningBalanceForProfitLossForCashFlow(COA.numAccountId,'''+Convert(varchar(300),@dtFromDate)+''','''+convert(varchar(300), @dtToDate)+''','+Convert(varchar(10),@numDomainId)+') As Amount            
         From Chart_Of_Accounts COA          
        inner join ListDetails LD on COA.numAcntTypeId=LD.numListItemID                           
        Where COA.numDomainId=' + Convert(varchar(5),@numDomainId) + 'And COA.numAccountId <>'+convert(varchar(10),@numParntAcntId)+             
        +'And(COA.numAcntTypeId=822 Or COA.numAcntTypeId=823 Or COA.numAcntTypeId=824 Or COA.numAcntTypeId=825 Or COA.numAcntTypeId=826 )'                                              
   End                                        
  print @strSQL                                              
  exec (@strSQL)                                              
End
GO
