/****** Object:  StoredProcedure [dbo].[USP_DeleteInktomi]    Script Date: 07/26/2008 16:15:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
---exec USP_DeleteInktomi
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_deleteinktomi')
DROP PROCEDURE usp_deleteinktomi
GO
CREATE PROCEDURE [dbo].[USP_DeleteInktomi]

as

declare @numTrackinID as numeric(9)
set @numTrackinID=0
select top 1 @numTrackinID=numTrackingID from TrackingVisitorsHDR where (vcUserDomain like '%inktomisearch.com' or vcUserDomain like '%ask.com'  or vcUserDomain like  '%yahoo.net' or vcUserDomain like  '%chello.nl' or vcUserDomain like  '%linksmanager.com' or vcUserDomain like  '%googlebot.com') and vcCrawler='False'
while @numTrackinID>0
begin
	
	update TrackingVisitorsHDR set vcCrawler='True' where numTrackingID=@numTrackinID
	select top 1 @numTrackinID=numTrackingID from TrackingVisitorsHDR 
	where (vcUserDomain like '%inktomisearch.com' or vcUserDomain like '%ask.com'  or vcUserDomain like  '%yahoo.net' or vcUserDomain like  '%chello.nl' or vcUserDomain like  '%linksmanager.com' or vcUserDomain like  '%googlebot.com' ) and vcCrawler='False' and numTrackingID>@numTrackinID
           
         if @@rowcount =0 set @numTrackinID=0
end
GO
