/****** Object:  StoredProcedure [dbo].[USP_ManageSalesprocessList]    Script Date: 07/26/2008 16:19:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj        
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_managesalesprocesslist')
DROP PROCEDURE usp_managesalesprocesslist
GO
CREATE PROCEDURE [dbo].[USP_ManageSalesprocessList]
    @numSalesProsID AS NUMERIC(9) = 0,
    @vcSlpName AS VARCHAR(50) = '',
    @numUserCntID AS NUMERIC(9) = 0,
    @tintProType AS TINYINT,
    @numDomainID AS NUMERIC(9) = 0,
    @numStageNumber AS NUMERIC = 0,
	@bitAssigntoTeams AS BIT=0,
	@bitAutomaticStartTimer AS BIT=0,
	@numOppId AS NUMERIC(18,0)=0,
	@numTaskValidatorId AS NUMERIC(18,0)=0,
	@numBuildManager NUMERIC(18,0) = 0
AS 
IF @numSalesProsID = 0 
    BEGIN        
        INSERT  INTO Sales_process_List_Master ( Slp_Name,
                                                 numdomainid,
                                                 pro_type,
                                                 numCreatedby,
                                                 dtCreatedon,
                                                 numModifedby,
                                                 dtModifiedOn,tintConfiguration,bitAutomaticStartTimer,bitAssigntoTeams,numOppId,numTaskValidatorId,numBuildManager)
        VALUES  (
                  @vcSlpName,
                  @numDomainID,
                  @tintProType,
                  @numUserCntID,
                  GETUTCDATE(),
                  @numUserCntID,
                  GETUTCDATE(),@numStageNumber,@bitAutomaticStartTimer,@bitAssigntoTeams,@numOppId,@numTaskValidatorId,@numBuildManager
                )        
        SET @numSalesProsID = SCOPE_IDENTITY()        
 
        SELECT  @numSalesProsID     
    END        
ELSE 
    BEGIN   
		IF(@numOppId>0)
		BEGIN
			UPDATE  Sales_process_List_Master
			SET     Slp_Name = @vcSlpName
			WHERE   Slp_Id = @numSalesProsID        
			SELECT  @numSalesProsID        
		END
		ELSE
		BEGIN  
			UPDATE  Sales_process_List_Master
			SET     Slp_Name = @vcSlpName,
					numModifedby = @numUserCntID,
					dtModifiedOn = GETUTCDATE(),
					bitAssigntoTeams=@bitAssigntoTeams,
					bitAutomaticStartTimer=@bitAutomaticStartTimer,
					numTaskValidatorId=@numTaskValidatorId,
					numBuildManager=@numBuildManager
			WHERE   Slp_Id = @numSalesProsID        
			SELECT  @numSalesProsID        
		END
    END
GO
