/****** Object:  StoredProcedure [dbo].[usp_DeleteModulePage]    Script Date: 07/26/2008 16:15:36 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_deletemodulepage')
DROP PROCEDURE usp_deletemodulepage
GO
CREATE PROCEDURE [dbo].[usp_DeleteModulePage]
	@numModuleID INT,
	@numPageID INT   
--
AS
	BEGIN
		DELETE FROM PageMaster
		WHERE numModuleID = @numModuleID
		AND numPageID = @numPageID
	END
GO
