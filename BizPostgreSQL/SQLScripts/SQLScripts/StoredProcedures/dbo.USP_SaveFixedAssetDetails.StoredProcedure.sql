/****** Object:  StoredProcedure [dbo].[USP_SaveFixedAssetDetails]    Script Date: 07/26/2008 16:21:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created By Siva      
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_savefixedassetdetails')
DROP PROCEDURE usp_savefixedassetdetails
GO
CREATE PROCEDURE [dbo].[USP_SaveFixedAssetDetails]      
@numFixedAssetId as numeric(9)=0,      
@numAssetClass as numeric(9)=0,      
@varAssetName as varchar(50),      
@varAssetDescription as varchar(100),      
@strLocation as ntext,      
@varSerialNo as varchar(50),      
@monCost as DECIMAL(20,5),      
@dtDatePurchased as datetime,      
@decDepreciationPer as decimal,      
@numAssetAccount as numeric(9)=0,      
@numVendor as numeric(9)=0,      
@monFiscalDepOrAppAmt as DECIMAL(20,5),      
@monCurrentValue as DECIMAL(20,5),      
@numUserCntID as numeric(9)=0,
@numDomainId as numeric(9)=0       
As      
Begin      
If @numFixedAssetId=0      
Begin      
    Insert Into FixedAssetDetails (numAssetClass,vcAssetName,vcAssetDescription,ntxtLocation,vcSerialNo,      
    dtDatePurchased,monCost,decDep_Year_Per,numVendor,monFiscalDepOrAppAmt,      
    monCurrentValue,numCreatedBy,dtCreatedOn,numDomainId) Values(@numAssetClass,@varAssetName,@varAssetDescription,      
    @strLocation,@varSerialNo,@dtDatePurchased,@monCost,@decDepreciationPer,@numVendor,  
    @monFiscalDepOrAppAmt,@monCurrentValue,@numUserCntID,getutcdate(),@numDomainId)       
End      
Else If  @numFixedAssetId>0      
Begin      
  Update FixedAssetDetails Set numAssetClass=@numAssetClass,vcAssetName=@varAssetName,vcAssetDescription=@varAssetDescription,      
  ntxtLocation=@strLocation,vcSerialNo=@varSerialNo,dtDatePurchased=@dtDatePurchased,monCost=@monCost,decDep_Year_Per=@decDepreciationPer,  
  numVendor=@numVendor,monFiscalDepOrAppAmt=@monFiscalDepOrAppAmt,monCurrentValue=@monCurrentValue,numDomainId=@numDomainId Where numFixedAssetId=@numFixedAssetId      
End      
End
GO
