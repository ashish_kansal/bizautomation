/****** Object:  StoredProcedure [dbo].[USP_DeleteJournalDetails]    Script Date: 07/26/2008 16:15:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created By Siva                          
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_deletejournaldetails')
DROP PROCEDURE usp_deletejournaldetails
GO
CREATE PROCEDURE [dbo].[USP_DeleteJournalDetails]    
@numDomainID as numeric(9)=0,                        
@numJournalID as numeric(9)=0,                          
@numBillPaymentID as numeric(9)=0,                    
@numDepositID as numeric(9)=0,          
@numCheckHeaderID as numeric(9)=0,
@numBillID as numeric(9)=0,
@numCategoryHDRID AS NUMERIC(9)=0,
@numReturnID AS NUMERIC(9)=0,
@numUserCntID AS NUMERIC(9)=0                        
As                          
BEGIN

	           
BEGIN TRY
   BEGIN TRANSACTION 

--   DECLARE @vcCompanyName varchar(200)
--   DECLARE @vcAmount varchar(200)
--   DECLARE @vcDesc varchar(200)
--   DECLARE @vcHDesc varchar(200)

--   SELECT TOP 1 
--		@vcCompanyName=dbo.fn_GetComapnyName(DM.numDivisionID),
--		@vcAmount=DM.monDepositAmount
--		FROM DepositMaster as DM  WHERE numDepositId=@numDepositID

--	SET @vcDesc=(select TOP 1 varDescription from General_Journal_Details where numJournalId=@numJournalID)
--	SET @vcHDesc=(SELECT TOP 1
--CASE 
--WHEN isnull(dbo.General_Journal_Header.numCheckHeaderID, 0) <> 0 THEN + 'Checks' 
--WHEN isnull(dbo.General_Journal_Header.numDepositId, 0) <> 0 AND DM.tintDepositePage = 1 THEN 'Deposit' 
--WHEN isnull(dbo.General_Journal_Header.numDepositId, 0) <> 0 AND DM.tintDepositePage = 2 THEN 'Receved Pmt' 
--WHEN isnull(dbo.General_Journal_Header.numOppId, 0) <> 0 AND isnull(dbo.General_Journal_Header.numOppBizDocsId, 0) <> 0 AND isnull(dbo.General_Journal_Header.numBizDocsPaymentDetId, 0) = 0 AND 
--dbo.OpportunityMaster.tintOppType = 1 THEN 'BizDocs Invoice' 
--WHEN isnull(dbo.General_Journal_Header.numOppId, 0) <> 0 AND isnull(dbo.General_Journal_Header.numOppBizDocsId, 0) <> 0 AND isnull(dbo.General_Journal_Header.numBizDocsPaymentDetId, 0) = 0 AND 
--dbo.OpportunityMaster.tintOppType = 2 THEN 'BizDocs Purchase' 
--WHEN isnull(dbo.General_Journal_Header.numCategoryHDRID, 0) <> 0 THEN 'Time And Expenses' 
--WHEN ISNULL(General_Journal_Header.numBillId,0) <>0 AND isnull(BH.bitLandedCost,0)=1 then 'Landed Cost' 
--WHEN isnull(dbo.General_Journal_Header.numBillID, 0) <> 0 THEN 'Bill'
--WHEN isnull(dbo.General_Journal_Header.numBillPaymentID, 0) <> 0 THEN 'Pay Bill'
--WHEN isnull(dbo.General_Journal_Header.numReturnID, 0) <> 0 THEN 
--																CASE 
--																	WHEN RH.tintReturnType=1 AND RH.tintReceiveType=1 THEN 'Sales Return Refund'
--																	WHEN RH.tintReturnType=1 AND RH.tintReceiveType=2 THEN 'Sales Return Credit Memo'
--																	WHEN RH.tintReturnType=2 AND RH.tintReceiveType=2 THEN 'Purchase Return Credit Memo'
--																	WHEN RH.tintReturnType=3 AND RH.tintReceiveType=2 THEN 'Credit Memo'
--																	WHEN RH.tintReturnType=4 AND RH.tintReceiveType=1 THEN 'Refund Against Credit Memo'
--																END 
--WHEN isnull(dbo.General_Journal_Header.numOppId, 0) <> 0 AND isnull(dbo.General_Journal_Header.numOppBizDocsId, 0) = 0 THEN 'PO Fulfillment'
--WHEN dbo.General_Journal_Header.numJournal_Id <> 0 THEN 'Journal' 
--END AS TransactionType
--FROM dbo.General_Journal_Header INNER JOIN
--dbo.General_Journal_Details ON dbo.General_Journal_Header.numJournal_Id = dbo.General_Journal_Details.numJournalId INNER JOIN
--dbo.Chart_Of_Accounts ON dbo.General_Journal_Details.numChartAcntId = dbo.Chart_Of_Accounts.numAccountId LEFT OUTER JOIN
--dbo.TimeAndExpense ON dbo.General_Journal_Header.numCategoryHDRID = dbo.TimeAndExpense.numCategoryHDRID 
--LEFT OUTER JOIN dbo.DivisionMaster ON dbo.General_Journal_Details.numCustomerId = dbo.DivisionMaster.numDivisionID and 
--dbo.General_Journal_Details.numDomainID = dbo.DivisionMaster.numDomainID
--LEFT OUTER JOIN dbo.CompanyInfo ON dbo.DivisionMaster.numCompanyID = dbo.CompanyInfo.numCompanyId 
--LEFT OUTER JOIN dbo.OpportunityMaster ON dbo.General_Journal_Header.numOppId = dbo.OpportunityMaster.numOppId LEFT OUTER JOIN
--dbo.OpportunityBizDocs ON dbo.General_Journal_Header.numOppBizDocsId = dbo.OpportunityBizDocs.numOppBizDocsId LEFT OUTER JOIN
--dbo.VIEW_JOURNALCHEQ ON (dbo.General_Journal_Header.numCheckHeaderID = dbo.VIEW_JOURNALCHEQ.numCheckHeaderID 
--or (dbo.General_Journal_Header.numBillPaymentID=dbo.VIEW_JOURNALCHEQ.numReferenceID and dbo.VIEW_JOURNALCHEQ.tintReferenceType=8))LEFT OUTER JOIN
--dbo.VIEW_BIZPAYMENT ON dbo.General_Journal_Header.numBizDocsPaymentDetId = dbo.VIEW_BIZPAYMENT.numBizDocsPaymentDetId LEFT OUTER JOIN
--dbo.DepositMaster DM ON DM.numDepositId = dbo.General_Journal_Header.numDepositId  LEFT OUTER JOIN
--dbo.ReturnHeader RH ON RH.numReturnHeaderID = dbo.General_Journal_Header.numReturnID LEFT OUTER JOIN
--dbo.CheckHeader CH ON CH.numReferenceID=RH.numReturnHeaderID AND CH.tintReferenceType=10
--LEFT OUTER JOIN BillHeader BH  ON ISNULL(General_Journal_Header.numBillId,0) = BH.numBillId
--WHERE General_Journal_Header.numJournal_Id=@numJournalID)

--   INSERT INTO RecordHistory VALUES(@numDomainID,@numUserCntID,ISNULL(GETDATE(),GETUTCDATE()),@numDepositID,7,'Removed Ledger Payment',NULL,NULL,0,@vcCompanyName,@vcAmount,@vcDesc,@vcHDesc)
 
IF @numDepositID>0
BEGIN
	--Throw error messae if Deposite to Default Undeposited Funds Account
	IF EXISTS(SELECT numDepositID FROM dbo.DepositMaster WHERE numDepositID=@numDepositID And numDomainId=@numDomainId 
			AND tintDepositeToType=2 AND tintDepositePage IN(2,3) AND bitDepositedToAcnt=1)
	BEGIN
		raiserror('Undeposited_Account',16,1); 
		RETURN;
	END
	
	--Throw error messae if Deposite use in Refund
	IF EXISTS(SELECT numDepositID FROM dbo.DepositMaster WHERE numDepositID=@numDepositID And monRefundAmount>0)
	BEGIN
		raiserror('Refund_Payment',16,1); 
		RETURN;
	END
	
	DECLARE @datEntry_Date DATETIME
	SELECT @datEntry_Date = ISNULL([DM].[dtDepositDate],[DM].[dtCreationDate]) FROM [dbo].[DepositMaster] AS DM WHERE DM.[numDepositId] = @numDepositID AND [DM].[numDomainId] = @numDomainID
	--Validation of closed financial year
	PRINT @datEntry_Date
	EXEC dbo.USP_ValidateFinancialYearClosingDate @numDomainID,@datEntry_Date

	--Update SO bizdocs amount Paid
	UPDATE OBD SET monAmountPaid= OBD.monAmountPaid - DD.monAmountPaid 
		FROM DepositMaster DM JOIN DepositeDetails DD ON DM.numDepositID=DD.numDepositID 
							  JOIN OpportunityBizDocs OBD ON DD.numOppBizDocsID=OBD.numOppBizDocsID
	WHERE DM.numDomainId=@numDomainId AND DM.numDepositID=@numDepositID AND DM.tintDepositePage IN(2,3)
	
	
	--Reset Integration with accounting
	UPDATE dbo.DepositMaster SET bitDepositedToAcnt =0 WHERE numDepositId IN (SELECT numChildDepositID FROM dbo.DepositeDetails WHERE numDepositID=@numDepositID AND numChildDepositID>0)
	
	--Delete Transaction History
	DELETE FROM TransactionHistory WHERE numTransHistoryID IN (SELECT numTransHistoryID FROM DepositMaster WHERE numDepositID=@numDepositID And numDomainId=@numDomainId)
	
	DELETE FROM DepositeDetails WHERE numDepositID=@numDepositID   
	
	IF EXISTS (SELECT 1 FROM DepositMaster WHERE numDepositID=@numDepositID And numDomainId=@numDomainId AND ISNULL(numReturnHeaderID,0)>0)
	BEGIN
		UPDATE DepositMaster SET monAppliedAmount=0 WHERE numDepositID=@numDepositID And numDomainId=@numDomainId 
	END 
	ELSE
	BEGIN  
		IF @numJournalId=0
		BEGIN
			SELECT @numJournalId=numJournal_Id FROM dbo.General_Journal_Header WHERE numDepositID=@numDepositID And numDomainId=@numDomainId
		END
	 
		Delete From General_Journal_Details Where numJournalId=@numJournalId And numDomainId=@numDomainId                          
		Delete From General_Journal_Header Where numJournal_Id=@numJournalId And numDomainId=@numDomainId  
	
		DELETE FROM DepositMaster WHERE numDepositID=@numDepositID And numDomainId=@numDomainId     
	END
END    

ELSE IF @numBillID>0
BEGIN
	--Throw error message if amount paid
	IF EXISTS (SELECT numBillID FROM BillHeader WHERE numBillID=@numBillID AND ISNULL(monAmtPaid,0)>0)
	BEGIN
		raiserror('BILL_PAID',16,1); --Bill is paid, can not delete bill
		RETURN;
	END

	IF @numJournalId=0
	BEGIN
		 SELECT @numJournalId=numJournal_Id FROM dbo.General_Journal_Header WHERE numBillID=@numBillID And numDomainId=@numDomainId
	END
	
	Delete From General_Journal_Details Where numJournalId=@numJournalId And numDomainId=@numDomainId                          
	Delete From General_Journal_Header Where numJournal_Id=@numJournalId And numDomainId=@numDomainId  

	DELETE FROM ProjectsOpportunities WHERE numBillId=@numBillID AND numDomainId=@numDomainID

	DELETE FROM BillDetails WHERE numBillID=@numBillID     
	DELETE FROM BillHeader WHERE numBillID=@numBillID And numDomainId=@numDomainId     
END 

ELSE IF @numBillPaymentID>0
BEGIN
	--Throw error messae if Bill Payment use in Refund
	IF EXISTS(SELECT numBillPaymentID FROM dbo.BillPaymentHeader WHERE numBillPaymentID=@numBillPaymentID And monRefundAmount>0)
	BEGIN
		raiserror('Refund_Payment',16,1); 
		RETURN;
	END	
		
	UPDATE dbo.BillHeader SET bitIsPaid =0 WHERE numBillID IN (SELECT numBillID FROM dbo.BillPaymentDetails WHERE numBillPaymentID=@numBillPaymentID AND numBillID>0)


	--Update Add Bill amount Paid
	UPDATE BH SET monAmtPaid= monAmtPaid - BPD.monAmount FROM BillPaymentDetails BPD JOIN BillHeader BH ON BPD.numBillID=BH.numBillID
	WHERE numBillPaymentID=@numBillPaymentID AND BPD.tintBillType=2

	--Update PO bizdocs amount Paid
	UPDATE OBD SET monAmountPaid= OBD.monAmountPaid - BPD.monAmount FROM BillPaymentDetails BPD JOIN OpportunityBizDocs OBD ON BPD.numOppBizDocsId=OBD.numOppBizDocsId
	WHERE numBillPaymentID=@numBillPaymentID AND BPD.tintBillType=1
	
	--Delete if check entry
	DELETE FROM CheckHeader WHERE tintReferenceType=8 AND numReferenceID=@numBillPaymentID And numDomainId=@numDomainId     
	
	IF @numJournalId=0
	BEGIN
		SELECT @numJournalId=numJournal_Id FROM dbo.General_Journal_Header WHERE numBillPaymentID=@numBillPaymentID And numDomainId=@numDomainId
	END
     
    Delete From General_Journal_Details Where numJournalId=@numJournalId And numDomainId=@numDomainId                          
	Delete From General_Journal_Header Where numJournal_Id=@numJournalId And numDomainId=@numDomainId 
	 
	DELETE FROM BillPaymentDetails WHERE numBillPaymentID=@numBillPaymentID     
	DELETE FROM BillPaymentHeader WHERE numBillPaymentID=@numBillPaymentID And numDomainId=@numDomainId AND ISNULL(numReturnHeaderID,0)=0
	UPDATE BillPaymentHeader SET monAppliedAmount=0 WHERE numBillPaymentID=@numBillPaymentID And numDomainId=@numDomainId AND ISNULL(numReturnHeaderID,0)!=0
	
END 

ELSE IF @numCheckHeaderID>0
BEGIN
	IF @numJournalId=0
	BEGIN
		SELECT @numJournalId=numJournal_Id FROM dbo.General_Journal_Header WHERE numCheckHeaderID=@numCheckHeaderID And numDomainId=@numDomainId
	END

	Delete From General_Journal_Details Where numJournalId=@numJournalId And numDomainId=@numDomainId                          
	Delete From General_Journal_Header Where numJournal_Id=@numJournalId And numDomainId=@numDomainId 

	DELETE FROM CheckDetails WHERE numCheckHeaderID=@numCheckHeaderID
	DELETE FROM CheckHeader WHERE numCheckHeaderID=@numCheckHeaderID And numDomainId=@numDomainId     
END 

ELSE IF @numCategoryHDRID>0
BEGIN
	EXEC dbo.USP_DeleteTimExpLeave
	@numCategoryHDRID = @numCategoryHDRID, --  numeric(9, 0)
	@numDomainId = @numDomainId --  numeric(9, 0)
END

ELSE IF @numReturnID>0
BEGIN
	EXEC dbo.USP_DeleteSalesReturnDetails
	@numReturnHeaderID = @numReturnID, --  numeric(9, 0)
	@numReturnStatus = 14879, --  numeric(18, 0) Received
	@numDomainId = @numDomainId, --  numeric(9, 0)
	@numUserCntID = @numUserCntID --  numeric(9, 0)
END


IF EXISTS(SELECT numItemID FROM dbo.General_Journal_Details WHERE numJournalId=@numJournalId And numDomainId=@numDomainId AND (chBizDocItems='IA1' OR chBizDocItems='OE1') AND (ISNULL(numCreditAmt,0) <> 0 OR ISNULL(numDebitAmt,0) <> 0))
BEGIN
 		raiserror('InventoryAdjustment',16,1); 
		RETURN;
END

IF EXISTS(SELECT numJournalId FROM dbo.General_Journal_Details WHERE numJournalId=@numJournalId And numDomainId=@numDomainId AND (chBizDocItems='OE')  )
BEGIN
 		raiserror('OpeningBalance',16,1); 
		RETURN;
END



Delete From General_Journal_Details Where numJournalId IN (SELECT numJournal_Id FROM General_Journal_Header WHERE numJournal_Id=@numJournalId And numDomainId=@numDomainId 
				AND isnull(numBillID,0)=0 AND isnull(numBillPaymentID,0)=0 AND isnull(numPayrollDetailID,0)=0
				AND isnull(numCheckHeaderID,0)=0 AND isnull(numReturnID,0)=0 
				AND isnull(numDepositId,0)=0)
				                         
Delete From General_Journal_Header Where numJournal_Id=@numJournalId And numDomainId=@numDomainId 
			    AND isnull(numBillID,0)=0 AND isnull(numBillPaymentID,0)=0 AND isnull(numPayrollDetailID,0)=0
				AND isnull(numCheckHeaderID,0)=0 AND isnull(numReturnID,0)=0 
				AND isnull(numDepositId,0)=0                      

 COMMIT
 
END TRY
BEGIN CATCH
  -- Whoops, there was an error
  IF @@TRANCOUNT > 0
     ROLLBACK

  -- Raise an error with the details of the exception
  DECLARE @ErrMsg nvarchar(4000), @ErrSeverity int
  SELECT @ErrMsg = ERROR_MESSAGE(),
         @ErrSeverity = ERROR_SEVERITY()

  RAISERROR(@ErrMsg, @ErrSeverity, 1)
END CATCH
 
 
 

	
--DECLARE @numBizDocsPaymentDetId NUMERIC 
--DECLARE @bitIntegratedToAcnt BIT
--DECLARE @tintPaymentType TINYINT
--SELECT @numBizDocsPaymentDetId=ISNULL(numBizDocsPaymentDetId,0) FROM dbo.General_Journal_Header WHERE numJournal_Id = @numJournalId AND numDomainId=@numDomainId
--IF @numBizDocsPaymentDetId>0
--BEGIN
--	SELECT @bitIntegratedToAcnt=bitIntegratedToAcnt,@tintPaymentType=tintPaymentType 
--	From OpportunityBizDocsDetails Where numBizDocsPaymentDetId=@numBizDocsPaymentDetId
--	AND numBillPaymentJournalID <>@numJournalId
--
--	-- @tintPaymentType - 2 means Bill against vendor, 4 Bill Against liability
--	IF (@tintPaymentType =2 OR @tintPaymentType =4) AND @bitIntegratedToAcnt=1
--	BEGIN
--		raiserror('BILL_PAID',16,1); --Bill is paid, can not delete bill
--		RETURN;
--	END
--	
--	
--END
--
--
--
-- -- if bitOpeningBalance=0 then it is not Opening Balance         
-- Declare @bitOpeningBalance as bit        
-- Select @bitOpeningBalance=isnull(bitOpeningBalance,0) From General_Journal_Header Where numJournal_Id=@numJournalId And numDomainId=@numDomainId          
--  Print @bitOpeningBalance    
-- if @bitOpeningBalance=0
-- Begin                       
--   Delete From General_Journal_Details Where numJournalId=@numJournalId And numDomainId=@numDomainId                          
--   Delete From General_Journal_Header Where numJournal_Id=@numJournalId And numDomainId=@numDomainId                       
--                        
--   --To Delete the CheckDetails
--   Delete From CheckDetails  Where numCheckId=@numCheckId And numDomainId=@numDomainId                      
--
--   -- To Delete the CashCreditCardDetails
--   Delete From CashCreditCardDetails Where numCashCreditId=@numCashCreditCardId And numDomainId=@numDomainId             
--            
--   -- To Delete DepositDetails    
--   UPDATE dbo.DepositMaster SET bitDepositedToAcnt = 0 WHERE numDepositId IN (SELECT numChildDepositID FROM dbo.DepositeDetails WHERE numDepositID=@numDepositId)      
--   Delete From dbo.DepositeDetails Where numDepositId=@numDepositId 
--   DELETE FROM dbo.DepositMaster WHERE numDepositId =@numDepositId And numDomainId=@numDomainId       
--   --Update Vendor Payement reference Journal ID if exist
--   UPDATE  dbo.OpportunityBizDocsDetails SET numBillPaymentJournalID=NULL WHERE numBillPaymentJournalID= @numJournalId AND numBizDocsPaymentDetId=@numBizDocsPaymentDetId
--   
-- End          
--Else
-- Begin
--  Update General_Journal_Header Set numAmount=0 Where numJournal_Id=@numJournalId And numDomainId=@numDomainId          
--  Update General_Journal_Details Set numDebitAmt=0,numCreditAmt=0 Where numJournalId=@numJournalId  And numDomainId=@numDomainId       
--  Update dbo.DepositMaster Set monDepositAmount=0 Where numDepositId=@numDepositId And numDomainId=@numDomainId       
--  Update CashCreditCardDetails Set monAmount=0 Where numCashCreditId=@numCashCreditCardId And numDomainId=@numDomainId        
-- End        
End
GO
