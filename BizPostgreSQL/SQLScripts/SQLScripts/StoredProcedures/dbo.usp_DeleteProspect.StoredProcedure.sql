/****** Object:  StoredProcedure [dbo].[usp_DeleteProspect]    Script Date: 07/26/2008 16:15:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_deleteprospect')
DROP PROCEDURE usp_deleteprospect
GO
CREATE PROCEDURE [dbo].[usp_DeleteProspect]
	@numCompanyID numeric,
	@numDivisionID numeric,
	@numUserID numeric,
	@CRMType tinyint   
--
AS
BEGIN
	DECLARE @RowCount numeric
	SELECT @RowCount = 0
	BEGIN TRANSACTION
		DELETE FROM Communication WHERE numDivisionID=@numDivisionID AND numContactId IN (SELECT numContactId FROM AdditionalContactsInformation WHERE numDivisionID = @numDivisionID)
		DELETE FROM TimeAndExpense WHERE numOppID IN (SELECT numOppId FROM OpportunityMaster WHERE numDivisionID = @numDivisionID)
		DELETE FROM OpportunityItems WHERE numOppID IN (SELECT numOppId FROM OpportunityMaster WHERE numDivisionID = @numDivisionID)
		DELETE FROM OpportunityMaster WHERE numDivisionID = @numDivisionID
		DELETE FROM AdditionalContactsInformation WHERE numDivisionID=@numDivisionID
		DELETE FROM DivisionMaster WHERE numDivisionID=@numDivisionID
	
		SELECT @RowCount = Count(*) FROM DivisionMaster WHERE numCompanyID=@numCompanyID
		IF @RowCount=0
		BEGIN
			DELETE FROM CompanyInfo WHERE numCompanyID=@numCompanyID AND numCreatedBy=@numUserID
		END
	COMMIT
END
GO
