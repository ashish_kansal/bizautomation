/****** Object:  StoredProcedure [dbo].[USP_CreateRecurring]    Script Date: 07/26/2008 16:15:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created by Siva                              
--For Creating Recurring for Checks,Journal Entry,Cash or Credit Card                              
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_createrecurring')
DROP PROCEDURE usp_createrecurring
GO
CREATE PROCEDURE [dbo].[USP_CreateRecurring]                              
As                              
Begin                              
                          
--Variable Declaration for CheckDetails Table                              
Declare @numCheckId as numeric(9)                              
Declare @numAcntTypeId as numeric(9)                              
Declare @monCheckAmt as DECIMAL(20,5)                              
Declare @varMemo as varchar(50)                              
Declare @numCustomerId as numeric(9)                              
Declare @numDomainId as numeric(9)                              
Declare @numMaxCheckId as numeric(9)                            
Declare @numCurrentCheckId as numeric(9)             
Declare @numCheckCompanyId as numeric(9)                          
Declare @LastRecurringDate as datetime            
Declare @numUserCntID as numeric(9)              
Declare @numNoTransactions as numeric(9)                       
                             
Declare @Day as tinyint                          
Declare @Month as  tinyint                          
Declare @Year as integer                          
--Declare @Date as varchar(100)                       
Declare @MonthNextDate as tinyint                         
                          
                            
--Variable Declaration for Recurring Table                               
Declare @numRecurringId as numeric(9)                              
Declare @varRecurringTemplateName as varchar(50)                              
Declare @chrIntervalType as char(1)                              
Declare @tintIntervalDays as tinyint                              
Declare @tintMonthlyType as tinyint                              
Declare @tintFirstDet as tinyint                              
Declare @tintWeekDays as tinyint                              
Declare @tintMonths as tinyint                              
Declare @dtStartDate as datetime                              
Declare @dtEndDate as datetime          
Declare @numNoTransactionsRecurring as numeric(9)                              
Declare @bitEndType as bit             
Declare @bitEndTransactionType as bit                           
Declare @DNextDate as datetime                              
Declare @DLastDate as datetime                              
Declare @DateDiff as integer                              
Declare @CurrentDate as datetime             
Declare @numCheckNo as numeric(9)          
Declare @numBankRoutingNo as numeric(9)          
Declare @numBankAccountNo as numeric(9)          
                            
                            
--Variable Declaration for General_Journal_Header                            
Declare @numJournal_Id as numeric(9)                            
Declare @varDescription as varchar(50)                            
Declare @numAmount as DECIMAL(20,5)                            
Declare @numJournalHeaderDomainId as numeric(9)                            
                            
                            
----------------------------------------For Checks------------------------------------------------------                             
                               
Select top 1 @numCheckId=numCheckId,@numRecurringId=numRecurringId,@numAcntTypeId=numAcntTypeId,@monCheckAmt=monCheckAmt,                              
    @varMemo=varMemo,@numCustomerId=numCustomerId,@numDomainId=numDomainId,@LastRecurringDate=isnull(LastRecurringDate,'Jan  1 1753 12:00AM'),          
    @numUserCntID=numCreatedBy,@numNoTransactions=isnull(numNoTransactions,0),          
         @numBankRoutingNo=numBankRoutingNo,@numBankAccountNo=numBankAccountNo From CheckDetails Where isnull(numRecurringId,0)<>0                              
                              
Select @numMaxCheckId=max(numCheckId) From CheckDetails Where isnull(numRecurringId,0)<>0                             
print @numMaxCheckId                          
print @numCheckId                          
While @numCheckId>0                               
Begin                              
   Print 'CheckId = ' +convert(varchar(10),@numCheckId)                     
   Print '@numRecurringId'+convert(varchar(10),@numRecurringId)                     
                          
    If @numRecurringId <> 0                              
      Begin                              
   Select @varRecurringTemplateName=varRecurringTemplateName,@chrIntervalType=chrIntervalType,@tintIntervalDays=tintIntervalDays,                              
   @tintMonthlyType=tintMonthlyType,@tintFirstDet=tintFirstDet,@tintWeekDays=tintWeekDays,@tintMonths=tintMonths,@dtStartDate=dtStartDate,                              
   @dtEndDate=dtEndDate,@bitEndType=bitEndType,@bitEndTransactionType=bitEndTransactionType,@numNoTransactionsRecurring=isnull(numNoTransaction,0)          
   From RecurringTemplate Where numRecurringId=@numRecurringId                
         Print ' @chrIntervalType'+ convert(varchar(10), @chrIntervalType)                    
   Print '@dtStartDate'+ convert(varchar(30),@dtStartDate)    
 Print 'getutcdate()==='+convert(varchar(50),getutcdate())  
 PRINT 'ssssstART'+convert(varchar(1000),datediff(day,@dtStartDate,getutcdate())                )  
   If datediff(day,@dtStartDate,getutcdate())>=0                           
            Begin  ---For Daily     
                          
                        
               If @chrIntervalType='D'                               
                   Begin -- 'Begin for Daily                              
                       If @LastRecurringDate='Jan  1 1753 12:00AM'                           
                          Begin                          
                            Set @DNextDate=@dtStartDate            
       print '@DNextDate=='+convert(varchar(100),@DNextDate)                     
       If @DNextDate < getutcdate()                   
                              Set @DNextDate=getutcdate()                       
                       End                          
                       Else                          
                        Begin                          
                          Set @DNextDate=DATEADD(Day, convert(numeric,@tintIntervalDays),  Convert(datetime, @LastRecurringDate))                            
                        End                          
                       print @DNextDate                          
        Set @DateDiff = datediff (day,@DNextDate,getutcdate())                              
                       Print @DateDiff                              
                       Print @bitEndType                          
                       Print @dtEndDate            
              --  Print '@numNoTransactionsRecurring=='+Convert(varchar(5),@numNoTransactionsRecurring)          
                       -- Print '@numNoTransactions=='+convert(varchar(5),@numNoTransactions)                      
                       --Print getutcdate()                          
                       Declare @EndDateDiff as integer                          
                       Set @EndDateDiff=datediff (day,@dtEndDate,getutcdate())                             
                       Print @EndDateDiff             
--                     Print '@bitEndType=='+convert(varchar(2),@bitEndType)          
--        Print '@DateDiff=='+convert(varchar(5),@DateDiff)          
--        Print '@bitEndTransactionType=='+convert(varchar(5),@bitEndTransactionType)          
--        Print '@numNoTransactionsRecurring=='+convert(varchar(5),@numNoTransactionsRecurring)          
--        Print '@numNoTransactions=='+convert(varchar(5),@numNoTransactions)          
                       If @DateDiff=0 And (@bitEndType=1 Or((@bitEndType=0 and @EndDateDiff<=0) Or (@bitEndTransactionType=1 And @numNoTransactions<@numNoTransactionsRecurring)))                              
                          Begin                             
        Set @CurrentDate=getutcdate()             
                  
        --To insert values into CheckCompanyDetails           
        Insert into CheckCompanyDetails(numCompanyId,monAmount,vcMemo,numDomainId,numCreatedBy,dtCreatedDate) Values                
        (@numCustomerId,@monCheckAmt,@varMemo,@numDomainId,@numUserCntID,getutcdate())           
                  
        Select @numCheckCompanyId=max(numCheckCompanyId) From CheckCompanyDetails Where numDomainId=@numDomainId           
                             Print '@numCheckCompanyId=========='+convert(varchar(1000),@numCheckCompanyId)                        
        --To insert values into Checks Header               
        Select @numCheckNo=isnull(numCheckNo,0)+1 From CheckDetails Where numCheckId in (Select isnull(max(numCheckId),0) From checkDetails Where numDomainId=@numDomainId) And numDomainId=@numDomainId          
                   
        Exec USP_InsertCheckHeaderDet @datEntry_Date=@CurrentDate,@numAmount=@monCheckAmt,@varMemo=@varMemo,@numCustomerId=@numCustomerId,          
        @numAcntTypeId=@numAcntTypeId,@numCheckNo=@numCheckNo,@numBankRoutingNo=@numBankRoutingNo,@numBankAccountNo=@numBankAccountNo,@numDomainId=@numDomainId,@numUserCntID=@numUserCntID,@numCheckCompanyId=@numCheckCompanyId                             
        Select @numCurrentCheckId=max(numCheckId) From CheckDetails Where numDomainId=@numDomainId                            
        --To insert values into Joural Entry Header Table                              
        Select @numJournal_Id=numJournal_Id,@numAmount=numAmount,@numDomainId=numDomainId From General_Journal_Header Where numCheckId=@numCheckId And numDomainId=@numDomainId                            
        Exec USP_InsertJournalEntryHeaderForChecks @datEntry_Date=@CurrentDate,@numAmount=@numAmount,@numJournal_Id=0,          
          @numDomainId=@numDomainId,@numCheckId=@numCurrentCheckId,@numRecurringId=0,@numUserCntID=@numUserCntID                           
        --To insert Values into Journal Entry Details table                            
        Exec USP_SaveJournalDetails @numJournalId=@numJournal_Id,@strRow='',@Mode=0,@RecurringMode=1,@numDomainId=@numDomainId                        
        --To update LastRecurringDate to CheckDetails table                            
        Update CheckDetails set LastRecurringDate=getutcdate()  Where numCheckId=@numCheckId And numDomainId=@numDomainId                     
        If @bitEndTransactionType=1 and @numNoTransactions<=@numNoTransactionsRecurring          
        Update CheckDetails set numNoTransactions=isnull(numNoTransactions,0)+1 Where numCheckId=@numCheckId And numDomainId=@numDomainId                      
        -- To update bitRecurring to RecurringTemplate table                  
        Update RecurringTemplate Set bitRecurring=1 Where numRecurringId=@numRecurringId And numDomainId=@numDomainId                 
         -- To Save the RecurringDetails to ChecksRecurringTable                        
           Exec USP_SaveChecksRecurringDetails @numCheckId,@CurrentDate                         
                                     
                         End                              
                  End -- 'End for Daily                            
                        
                        
                   --For Monthly                          
                  If @chrIntervalType='M'                          
                     Begin                          
                        Print @chrIntervalType     
      Print 'SP'                       
                        Print 'Monthly Type'+ Convert(varchar(3),@tintMonthlyType)                          
                        Print 'Last RecurringDate'+ Convert(varchar(20),@LastRecurringDate)                          
                        If @tintMonthlyType=0                          
                          Begin -- tinymonthly Begin                          
                             If @LastRecurringDate='Jan  1 1753 12:00AM'                           
                                Begin                         
                                    Set @DNextDate=@dtStartDate                      
                                    If @DNextDate < getutcdate()                   
                                      Set @DNextDate=getutcdate()                        
                                              
                                    Set @Day = DATEPART(Day, @DNextDate)                          
    Set @Month = DATEPART(Month, @DNextDate)                          
                                    Set @Year= DATEPART(Year, @DNextDate)                          
                                    Print @Day                          
                                    Print @Month                          
         Print @Year                         
                                    Set @DNextDate=Dateadd(month,((@Year-1900)*12)+@Month-1,@tintFirstDet-1)                          
                                    Print @DNextDate                          
           
                              End                            
                              Else         
                                Begin                          
                                  Set @DNextDate=DATEADD(Month, convert(numeric,@tintIntervalDays),  Convert(datetime, @LastRecurringDate))                           
                              End                           
                              Set @DateDiff = datediff (day,@DNextDate,getutcdate())                              
                              Print 'DateDiff'+convert(varchar(10),@DateDiff)                          
                              If @DateDiff=0 And (@bitEndType=1 Or ((@bitEndType=0 and getutcdate()<=@dtEndDate) Or (@bitEndTransactionType=1 And @numNoTransactions<@numNoTransactionsRecurring)))          
                                  Begin                           
                                    Set @CurrentDate=getutcdate()              
         --To insert values into CheckCompanyDetails           
          Insert into CheckCompanyDetails(numCompanyId,monAmount,vcMemo,numDomainId,numCreatedBy,dtCreatedDate) Values                
          (@numCustomerId,@monCheckAmt,@varMemo,@numDomainId,@numUserCntID,getutcdate())           
          Set @numCheckCompanyId=@@Identity                           
         --To insert values into Checks Header           
         Select @numCheckNo= isnull(numCheckNo,0)+1 From CheckDetails Where numCheckId in (Select isnull(max(numCheckId),0) From checkDetails Where numDomainId=@numDomainId) And numDomainId=@numDomainId          
         exec USP_InsertCheckHeaderDet  @datEntry_Date=@CurrentDate, @numAmount=@monCheckAmt, @varMemo = @varMemo, @numCustomerId=@numCustomerId, @numAcntTypeId=@numAcntTypeId,          
         @numCheckNo=@numCheckNo,@numBankRoutingNo=@numBankRoutingNo,@numBankAccountNo=@numBankAccountNo, @numDomainId = @numDomainId,@numUserCntID=@numUserCntID ,@numCheckCompanyId=@numCheckCompanyId                        
                  Select @numCurrentCheckId=max(numCheckId) From CheckDetails Where numDomainId=@numDomainId                            
         --To insert values into Joural Entry Header Table                              
         Select @numJournal_Id=numJournal_Id,@numAmount=numAmount,@numDomainId=numDomainId From General_Journal_Header Where numCheckId=@numCheckId And numDomainId=@numDomainId                         
               Exec USP_InsertJournalEntryHeaderForChecks  @datEntry_Date=@CurrentDate,@numAmount=@numAmount,@numJournal_Id=0,@numDomainId=@numDomainId,@numCheckId=@numCurrentCheckId,@numRecurringId=0,@numUserCntID=@numUserCntID                           
  
   
         --To insert Values into Journal Entry Details table                            
         Exec USP_SaveJournalDetails @numJournalId=@numJournal_Id,@strRow='',@Mode=0,@RecurringMode=1, @numDomainId=@numDomainId                            
         --To update LastRecurringDate to CheckDetails table                            
         Update CheckDetails set LastRecurringDate=getutcdate() Where numCheckId=@numCheckId And numDomainId=@numDomainId             
         If @bitEndTransactionType=1 and @numNoTransactions<=@numNoTransactionsRecurring          
          Update CheckDetails set numNoTransactions=isnull(numNoTransactions,0)+1 Where numCheckId=@numCheckId And numDomainId=@numDomainId                     
         -- To update bitRecurring to RecurringTemplate table                  
Update RecurringTemplate Set bitRecurring=1 Where numRecurringId=@numRecurringId And numDomainId=@numDomainId                     
         -- To Save the RecurringDetails to ChecksRecurringTable                        
         Exec USP_SaveChecksRecurringDetails @numCheckId,@CurrentDate                         
                                 End                              
                         End   -- tinymonthly End--0                           
                          
                        If @tintMonthlyType=1                          
                           Begin                          
                              If @LastRecurringDate='Jan  1 1753 12:00AM'                           
                                 Begin                          
          Set @DNextDate=@dtStartDate                      
      If @DNextDate < getutcdate()                   
             Set @DNextDate=getutcdate()                        
          Set @Month=DATEPART(Month, @DNextDate)                          
          Set  @Year=DATEPART(Year, @DNextDate)                          
          Print '@Month = '+ Convert(varchar(10),@Month)                        
          Print @Year                           
          Print '@tintFirstDet=' + Convert(varchar(10),@tintFirstDet)              
          Print '@tintWeekDays=' + Convert(varchar(10),@tintWeekDays)                      
          Print '@DNextDate=' + Convert(varchar(30),@DNextDate)                        
                         Set @DNextDate=dateadd(month,((@Year-1900)*12)+@Month-1,@tintFirstDet-1)                          
             Print 'DNextDate==' + convert(varchar(30),@DNextDate)                    
          Set @DNextDate=dbo.fn_RecurringTemplate(@tintFirstDet,@tintWeekDays,@DNextDate)                      
                                     Print @DNextDate                     
                                     Set @MonthNextDate=DATEPART(Month, @DNextDate)                       
                                     Print '@MonthNextDate='+Convert(varchar(20),@MonthNextDate)                      
                                 End                            
                                Else                          
                                  Begin                          
                                     Set @DNextDate=DATEADD(Month, convert(numeric,@tintIntervalDays),  Convert(datetime, @LastRecurringDate))                           
                                     Set @Month=DATEPART(Month, getutcdate())                     
                                     Print '@Month = '+ Convert(varchar(10),@Month)                       
                                     Set @MonthNextDate=DATEPART(Month, @DNextDate)                     
                                     Print '@MonthNextDate='+Convert(varchar(20),@MonthNextDate)                    
                                     Set @DNextDate=dbo.fn_RecurringTemplate(@tintFirstDet,@tintWeekDays,@DNextDate)                          
                                     Print @DNextDate                          
                                  End                          
                               Set @DateDiff = datediff (day,@DNextDate,getutcdate())                              
                               Print 'DateDifference'+Convert(varchar(30),@DateDiff)                          
                      If @DateDiff=0 And @Month=@MonthNextDate And ((@bitEndType=1 Or (@bitEndType=0 And getutcdate()<=@dtEndDate) Or (@bitEndTransactionType=1 And @numNoTransactions<@numNoTransactionsRecurring)))                                      
   
      
        
             
         Begin                              
            Set @CurrentDate=getutcdate()                
            --To insert values into CheckCompanyDetails           
          Insert into CheckCompanyDetails(numCompanyId,monAmount,vcMemo,numDomainId,numCreatedBy,dtCreatedDate) Values                
             (@numCustomerId,@monCheckAmt,@varMemo,@numDomainId,@numUserCntID,getutcdate())           
             Set @numCheckCompanyId=@@Identity                          
            --To insert values into Checks Header          
          Select @numCheckNo=isnull(numCheckNo,0)+1 From CheckDetails Where numCheckId in (Select isnull(max(numCheckId),0) From checkDetails Where numDomainId=@numDomainId) And numDomainId=@numDomainId                            
             Exec USP_InsertCheckHeaderDet  @datEntry_Date=@CurrentDate, @numAmount =@monCheckAmt, @varMemo = @varMemo, @numCustomerId=@numCustomerId, @numAcntTypeId = @numAcntTypeId,           
          @numCheckNo=@numCheckNo,@numBankRoutingNo=@numBankRoutingNo,@numBankAccountNo=@numBankAccountNo,@numDomainId = @numDomainId,@numUserCntID=@numUserCntID,@numCheckCompanyId=@numCheckCompanyId                      
                      Select @numCurrentCheckId=max(numCheckId) From CheckDetails   Where numDomainId=@numDomainId                          
             --To insert values into Joural Entry Header Table                              
          Select @numJournal_Id=numJournal_Id,@numAmount=numAmount,@numDomainId=numDomainId From General_Journal_Header Where numCheckId=@numCheckId And numDomainId=@numDomainId                          
          Exec USP_InsertJournalEntryHeaderForChecks  @datEntry_Date=@CurrentDate,@numAmount=@numAmount,@numJournal_Id=0,@numDomainId=@numDomainId,@numCheckId=@numCurrentCheckId,@numRecurringId=0,@numUserCntID=@numUserCntID                              
          --To insert Values into Journal Entry Details table                  
          Exec USP_SaveJournalDetails @numJournalId=@numJournal_Id,@strRow='',@Mode=0,@RecurringMode=1, @numDomainId=@numDomainId                            
          --To update LastRecurringDate to CheckDetails table                            
          Update CheckDetails set LastRecurringDate=getutcdate() Where numCheckId=@numCheckId And numDomainId=@numDomainId                   
           If @bitEndTransactionType=1 and @numNoTransactions<=@numNoTransactionsRecurring          
           Update CheckDetails set numNoTransactions=isnull(numNoTransactions,0)+1 Where numCheckId=@numCheckId And numDomainId=@numDomainId          
          -- To update bitRecurring to RecurringTemplate table                  
          Update RecurringTemplate Set bitRecurring=1 Where numRecurringId=@numRecurringId And numDomainId=@numDomainId                      
          -- To Save the RecurringDetails to ChecksRecurringTable                        
          Exec USP_SaveChecksRecurringDetails @numCheckId,@CurrentDate                         
         End                              
                                      
                         End   --End For @tintMonthlyType=1                          
                   End  --End for Monthly                          
                                 
            --- For Yearly                          
                                       
                  If @chrIntervalType='Y'                          
                      Begin                          
                         Print @chrIntervalType                        
                         Print @LastRecurringDate                        
                         If @LastRecurringDate='Jan  1 1753 12:00AM'                           
             Begin                          
                               If @dtStartDate < getutcdate()                   
                                 Set @dtStartDate=getutcdate()                  
                               Set @Year= DATEPART(Year, @dtStartDate)                          
                               Print @Year                           
                               Set @DNextDate=dateadd(month,((@Year-1900)*12)+@tintMonths-1,@tintFirstDet-1)                          
                               Print @DNextDate                          
                             End                          
                         Else                          
                           Begin                           
                             Set @DNextDate=DATEADD(Year, convert(numeric,1),  Convert(datetime, @LastRecurringDate))                        
                             Print @DNextDate                            
                           End                          
                        Set @DateDiff = datediff (day,@DNextDate,getutcdate())                              
                        -- print 'Yearly Diff' + Convert(varchar(2),@DateDiff)                          
                        Print @DateDiff                        
                        If @DateDiff=0 and (@bitEndType=1 Or ((@bitEndType=0 And getutcdate()<=@dtEndDate) Or (@bitEndTransactionType=1 And @numNoTransactions<@numNoTransactionsRecurring)))                                                
                           Begin                              
                               Set @CurrentDate=getutcdate()                
          --To insert values into CheckCompanyDetails           
         Insert into CheckCompanyDetails(numCompanyId,monAmount,vcMemo,numDomainId,numCreatedBy,dtCreatedDate) Values                
         (@numCustomerId,@monCheckAmt,@varMemo,@numDomainId,@numUserCntID,getutcdate())           
        Set @numCheckCompanyId=@@Identity                     
                               --To insert values into Checks Header             
        Select @numCheckNo=isnull(numCheckNo,0)+1 From CheckDetails Where numCheckId in (Select isnull(max(numCheckId),0) From checkDetails Where numDomainId=@numDomainId) And numDomainId=@numDomainId                            
                               Exec USP_InsertCheckHeaderDet  @datEntry_Date=@CurrentDate,@numAmount=@monCheckAmt,@varMemo=@varMemo,@numCustomerId=@numCustomerId,@numAcntTypeId=@numAcntTypeId,          
          @numCheckNo=@numCheckNo,@numBankRoutingNo=@numBankRoutingNo,@numBankAccountNo=@numBankAccountNo,@numDomainId=@numDomainId,@numUserCntID=@numUserCntID,@numCheckCompanyId=@numCheckCompanyId                               
                               Select @numCurrentCheckId=max(numCheckId) From CheckDetails  Where numDomainId=@numDomainId                           
                               --To insert values into Joural Entry Header Table                              
                               Select @numJournal_Id=numJournal_Id,@numAmount=numAmount,@numDomainId=numDomainId From General_Journal_Header Where numCheckId=@numCheckId And numDomainId=@numDomainId                           
                               Exec USP_InsertJournalEntryHeaderForChecks  @datEntry_Date=@CurrentDate,@numAmount=@numAmount,@numJournal_Id=0,@numDomainId=@numDomainId,@numCheckId=@numCurrentCheckId,@numRecurringId=0,@numUserCntID=@numUserCntID           
  
   
      
        
                    
                               --To insert Values into Journal Entry Details table                     
                               Exec USP_SaveJournalDetails @numJournalId=@numJournal_Id,@strRow='',@Mode=0,@RecurringMode=1,@numDomainId=@numDomainId                           
                               --To update LastRecurringDate to CheckDetails table                            
                               Update CheckDetails set LastRecurringDate=getutcdate() Where numCheckId=@numCheckId And numDomainId=@numDomainId          
          If @bitEndTransactionType=1 and @numNoTransactions<=@numNoTransactionsRecurring          
         Update CheckDetails set numNoTransactions=isnull(numNoTransactions,0)+1 Where numCheckId=@numCheckId And numDomainId=@numDomainId                 
                               -- To update bitRecurring to RecurringTemplate table                  
                               Update RecurringTemplate Set bitRecurring=1 Where numRecurringId=@numRecurringId And numDomainId=@numDomainId                          
                               -- To Save the RecurringDetails to ChecksRecurringTable                        
                               Exec USP_SaveChecksRecurringDetails @numCheckId,@CurrentDate                         
                          End             
          End  --End for Yearly                        
                          
 End  --End for StartDate<=                           
End   --End For @numRecurringId                          
  Select top 1 @numCheckId=numCheckId,@numRecurringId=numRecurringId,@numAcntTypeId=numAcntTypeId,@monCheckAmt=monCheckAmt,@varMemo=varMemo,                              
    @numCustomerId=numCustomerId,@numDomainId=numDomainId,@LastRecurringDate=isnull(LastRecurringDate,'Jan  1 1753 12:00AM'),@numUserCntID=numCreatedBy,@numNoTransactions=isnull(numNoTransactions,0),          
    @numBankRoutingNo=numBankRoutingNo,@numBankAccountNo=numBankAccountNo From CheckDetails          
    Where numCheckId>@numCheckId And numCheckId <= @numMaxCheckId And isnull(numRecurringId,0)<>0                             
   If @@rowcount=0 set @numCheckId=0                            
   -- print @numCheckId                           
End ---End for While                          
                        
------------------------------------------------End For Checks-------------------------------------------------------------------                    
                    
                    
-------------------------------------------------For Cash or Credit Cards--------------------------------------------------------                    
                    
---Variable Declaration for CashCreditCardDetails                    
                    
Declare @CCnumCashCreditId as numeric(9)                    
Declare @CCnumRecurringId as numeric(9)                    
Declare @CCnumAcntTypeId as numeric(9)                    
Declare @CCbitMoneyOut as bit                    
Declare @CCnumPurchaseId as numeric(9)                    
Declare @CCvcReference as varchar(50)                    
Declare @CCmonAmount as DECIMAL(20,5)                    
Declare @CCvcMemo as varchar(50)                    
Declare @CCbitChargeBack as bit                    
Declare @CCnumDomainId as integer                    
Declare @CCLastRecurringDate as datetime                    
Declare @CCnumMaxCashCreditId as integer                    
Declare @CCnumCurrentCashCreditCardId as integer            
Declare @CCnumUserCntID as numeric(9)          
Declare @CCnumNoTransactions as numeric(9)          
                    
                    
                               
Select top 1 @CCnumCashCreditId=numCashCreditId,@CCnumRecurringId=numRecurringId,@CCnumAcntTypeId=numAcntTypeId,@CCbitMoneyOut=bitMoneyOut,@CCnumPurchaseId=numPurchaseId,                    
    @CCvcReference=vcReference,@CCmonAmount=monAmount,@CCvcMemo=vcMemo,@CCbitChargeBack=bitChargeBack,@CCLastRecurringDate=isnull(dtLastRecurringDate,'Jan  1 1753 12:00AM'),          
    @CCnumDomainId=numDomainId,@CCnumUserCntID=numCreatedBy,@CCnumNoTransactions=isnull(numNoTransactions,0) From CashCreditCardDetails Where isnull(numRecurringId,0)<>0             
                    
Select @CCnumMaxCashCreditId=max(numCashCreditId) From CashCreditCardDetails  Where isnull(numRecurringId,0)<>0                             
          
Print @CCnumMaxCashCreditId                          
Print @CCnumCashCreditId                          
While @CCnumCashCreditId>0                               
  Begin                     
     Print 'numCashCreditId = ' +convert(varchar(10),@CCnumCashCreditId)                     
     Print '@numRecurringId'+convert(varchar(10),@CCnumRecurringId)                     
     If @CCnumRecurringId <> 0                              
        Begin                              
     Select @varRecurringTemplateName=varRecurringTemplateName,@chrIntervalType=chrIntervalType,@tintIntervalDays=tintIntervalDays,                              
     @tintMonthlyType=tintMonthlyType,@tintFirstDet=tintFirstDet,@tintWeekDays=tintWeekDays,@tintMonths=tintMonths,@dtStartDate=dtStartDate,                              
     @dtEndDate=dtEndDate,@bitEndType=bitEndType,@bitEndTransactionType=bitEndTransactionType,@numNoTransactionsRecurring=isnull(numNoTransaction,0)          
           From RecurringTemplate Where numRecurringId=@CCnumRecurringId                              
                  
     Print ' @chrIntervalType'+ convert(varchar(10), @chrIntervalType)                    
           Print '@dtStartDate'+ convert(varchar(30),@dtStartDate)                    
           If datediff(day,@dtStartDate,getutcdate())>=0                           
              Begin  ---For Daily                       
                 print  @CCLastRecurringDate                    
                 If @chrIntervalType='D'                               
                    Begin -- 'Begin for Daily                              
                      If @CCLastRecurringDate='Jan  1 1753 12:00AM'                           
                         Begin                          
                            Set @DNextDate=@dtStartDate                    
                            if @DNextDate < getutcdate()                   
                            Set @DNextDate=getutcdate()                          
                            print @DNextDate                              
                         End                          
                      Else                          
                       Begin                          
                        Set @DNextDate=DATEADD(Day, convert(numeric,@tintIntervalDays),  Convert(datetime, @CCLastRecurringDate))                            
                       End                          
                      Print @DNextDate                          
                      Set @DateDiff = datediff (day,@DNextDate,getutcdate())                              
                      Print @DateDiff                              
                      Print @bitEndType                          
                      Print @dtEndDate                          
                      Print getutcdate()                          
                      Set @EndDateDiff=datediff (day,@dtEndDate,getutcdate())                             
                      Print @EndDateDiff                          
                      If @DateDiff=0 And (@bitEndType=1 Or ((@bitEndType=0 And @EndDateDiff<=0) Or (@bitEndTransactionType=1 And @CCnumNoTransactions<@numNoTransactionsRecurring)))                             
                         Begin                             
                           Set @CurrentDate=getutcdate()                        
         --To insert values into CashCreditCardDetails                            
         Exec USP_InsertCashCreditHeaderDetails @datEntry_Date=@CurrentDate,@monAmount=@CCmonAmount,@vcMemo=@CCvcMemo,@numPurchaseId=@CCnumPurchaseId,          
           @numAcntTypeId=@CCnumAcntTypeId,@vcReferenceNo=@CCvcReference,@bitMoneyOut=@CCbitMoneyOut,@bitChargeBack=@CCbitChargeBack,          
           @numDomainId=@CCnumDomainId,@numUserCntID=@CCnumUserCntID                              
         Select @CCnumCurrentCashCreditCardId=max(numCashCreditId) From CashCreditCardDetails Where numDomainId=@CCnumDomainId                          
         --To insert values into Joural Entry Header Table                              
         Select @numJournal_Id=numJournal_Id,@numAmount=numAmount,@numDomainId=numDomainId From General_Journal_Header Where numCashCreditCardId=@CCnumCashCreditId                            
         Exec USP_InsertJournalEntryHeaderForCashCreditCard  @datEntry_Date=@CurrentDate,@numAmount=@numAmount,@numJournal_Id=0,          
           @numDomainId=@numDomainId,@numCashCreditCardId=@CCnumCurrentCashCreditCardId,@numUserCntID=@CCnumUserCntID                            
         --To insert Values into Journal Entry Details table                            
         Exec USP_SaveJournalDetailsForCashCreditCard @numJournalId=@numJournal_Id,@strRow='',@Mode=0,@RecurringMode=1,@numDomainId=@CCnumDomainId                            
         --To update LastRecurringDate to CashCreditCardDetails table                            
         Update CashCreditCardDetails set dtLastRecurringDate=getutcdate() Where numCashCreditId=@CCnumCashCreditId And numDomainId=@CCnumDomainId              
         If @bitEndTransactionType=1 and @CCnumNoTransactions<=@numNoTransactionsRecurring          
        Update CashCreditCardDetails set numNoTransactions=isnull(numNoTransactions,0)+1 Where numCashCreditId=@CCnumCashCreditId and numDomainId=@CCnumDomainId             
         -- To update bitRecurring to RecurringTemplate table                  
         Update RecurringTemplate Set bitRecurring=1 Where numRecurringId=@numRecurringId And numDomainId=@CCnumDomainId                 
         -- To Save the RecurringDetails to CashCreditCardRecurring Table                        
         Exec USP_SaveCashCreditCardRecurringDetails @CCnumCashCreditId,@CurrentDate                         
                                    
                         End                              
                  End -- 'End for Daily                            
                        
                        
                    --For Monthly                          
                   If @chrIntervalType='M'                          
          Begin                          
                        Print @chrIntervalType                          
                        Print 'Monthly Type'+ Convert(varchar(3),@tintMonthlyType)                          
                        Print 'Last RecurringDate'+ Convert(varchar(20),@CCLastRecurringDate)                          
                        If @tintMonthlyType=0                          
                           Begin -- tinymonthly Begin                          
                             If @CCLastRecurringDate='Jan  1 1753 12:00AM'                           
                                 Begin                          
                                    Set @DNextDate=@dtStartDate                        
                                    If @DNextDate < getutcdate()                   
                                       Set @DNextDate=getutcdate()          
                                    Set @Day=DATEPART(Day, @DNextDate)                          
                                    Set @Month=DATEPART(Month, @DNextDate)                          
                                    Set @Year=DATEPART(Year, @DNextDate)                          
                                    Print @Day                          
                                    Print @Month                          
                                    Print @Year                           
                                    Set @DNextDate=Dateadd(month,((@Year-1900)*12)+@Month-1,@tintFirstDet-1)                          
                                    Print @DNextDate                          
                                 End                            
                               Else                          
                                 Begin                          
      Set @DNextDate=DATEADD(Month, convert(numeric,@tintIntervalDays),Convert(datetime, @CCLastRecurringDate))                           
                                 End                           
        Set @DateDiff=Datediff (Day,@DNextDate,getutcdate())                              
                              Print 'DateDiff'+Convert(varchar(10),@DateDiff)                          
                              If @DateDiff=0 And (@bitEndType=1 Or ((@bitEndType=0 And getutcdate()<=@dtEndDate) Or (@bitEndTransactionType=1 And @CCnumNoTransactions<@numNoTransactionsRecurring)))                           
                                 Begin                           
            Set @CurrentDate=getutcdate()                              
            --To insert values into CashCreditCardDetails                            
            Exec USP_InsertCashCreditHeaderDetails @datEntry_Date=@CurrentDate,@monAmount=@CCmonAmount,@vcMemo=@CCvcMemo,@numPurchaseId=@CCnumPurchaseId,@numAcntTypeId=@CCnumAcntTypeId,@vcReferenceNo=@CCvcReference,                  
               @bitMoneyOut=@CCbitMoneyOut,@bitChargeBack=@CCbitChargeBack,@numDomainId=@CCnumDomainId,@numUserCntID=@CCnumUserCntID                               
            Select @CCnumCurrentCashCreditCardId=max(numCashCreditId) From CashCreditCardDetails Where numDomainId=@CCnumDomainId                          
           --To insert values into Joural Entry Header Table                              
            Select @numJournal_Id=numJournal_Id,@numAmount=numAmount,@numDomainId=numDomainId From General_Journal_Header Where numCashCreditCardId=@CCnumCashCreditId  And numDomainId=@CCnumDomainId                          
            Exec USP_InsertJournalEntryHeaderForCashCreditCard  @datEntry_Date=@CurrentDate,@numAmount=@numAmount,@numJournal_Id=0,          
             @numDomainId=@numDomainId,@numCashCreditCardId=@CCnumCurrentCashCreditCardId,@numUserCntID=@CCnumUserCntID                              
            --To insert Values into Journal Entry Details table                            
            Exec USP_SaveJournalDetailsForCashCreditCard @numJournalId=@numJournal_Id,@strRow='',@Mode=0,@RecurringMode=1 ,@numDomainId=@CCnumDomainId                           
            --To update LastRecurringDate to CashCreditCard table                        
               Update CashCreditCardDetails set dtLastRecurringDate=getutcdate() Where numCashCreditId=@CCnumCashCreditId And numDomainId=@CCnumDomainId           
         If @bitEndTransactionType=1 and @CCnumNoTransactions<=@numNoTransactionsRecurring          
          Update CashCreditCardDetails set numNoTransactions=isnull(numNoTransactions,0)+1 Where numCashCreditId=@CCnumCashCreditId And numDomainId=@CCnumDomainId                 
            --To update bitRecurring to RecurringTemplate table                  
            Update RecurringTemplate Set bitRecurring=1 Where numRecurringId=@numRecurringId And numDomainId=@CCnumDomainId                 
         -- To Save the RecurringDetails to CashCreditCardRecurring Table                        
         Exec USP_SaveCashCreditCardRecurringDetails @CCnumCashCreditId,@CurrentDate                         
                                End                              
            End   -- tinymonthly End--0                           
                          
    if @tintMonthlyType=1                          
     Begin                          
      if @CCLastRecurringDate='Jan  1 1753 12:00AM'                           
       Begin                          
        Set @DNextDate=@dtStartDate                   
        If @DNextDate < getutcdate()                   
         Set @DNextDate=getutcdate()                           
        Set @Month=DATEPART(Month, @DNextDate)                          
        Set @Year=DATEPART(Year, @DNextDate)                          
        Print '@Month = '+ Convert(varchar(10),@Month)                        
        Print @Year                           
        Print '@tintFirstDet='+Convert(varchar(10),@tintFirstDet)                        
        Print '@tintWeekDays='+Convert(varchar(10),@tintWeekDays)                      
 Print '@DNextDate='+Convert(varchar(30),@DNextDate)                        
                    Set @DNextDate=Dateadd(month,((@Year-1900)*12)+@Month-1,@tintFirstDet-1)                          
        print 'DNextDate=='+ convert(varchar(30),@DNextDate)                    
        Set @DNextDate=dbo.fn_RecurringTemplate(@tintFirstDet,@tintWeekDays,@DNextDate)                      
        Print @DNextDate                     
        Set @MonthNextDate=DATEPART(Month, @DNextDate)                       
        print '@MonthNextDate='+Convert(varchar(20),@MonthNextDate)                      
                                                   
          End                            
          Else                          
           Begin                          
         Set @DNextDate=DATEADD(Month, convert(numeric,@tintIntervalDays),  Convert(datetime, @CCLastRecurringDate))                           
         Set @Month=DATEPART(Month, getutcdate())                     
         Print '@Month = '+ Convert(varchar(10),@Month)                       
         Set @MonthNextDate=DATEPART(Month, @DNextDate)                     
         Print '@MonthNextDate='+Convert(varchar(20),@MonthNextDate)                    
         Set @DNextDate=dbo.fn_RecurringTemplate(@tintFirstDet,@tintWeekDays,@DNextDate)                          
         Print @DNextDate                          
        End                          
              
       Set @DateDiff = datediff (day,@DNextDate,getutcdate())                              
       print 'DateDifference'+Convert(varchar(30),@DateDiff)                          
          If @DateDiff=0 And @Month=@MonthNextDate And (@bitEndType=1 Or ((@bitEndType=0 and getutcdate()<=@dtEndDate) Or (@bitEndTransactionType=1 And @CCnumNoTransactions<@numNoTransactionsRecurring)))           
        Begin                              
         Set @CurrentDate=getutcdate()                              
                                        
         --To insert values into CashCreditCardDetails                            
         Exec USP_InsertCashCreditHeaderDetails @datEntry_Date=@CurrentDate,@monAmount=@CCmonAmount,@vcMemo=@CCvcMemo,@numPurchaseId=@CCnumPurchaseId,@numAcntTypeId=@CCnumAcntTypeId,                  
           @vcReferenceNo=@CCvcReference,@bitMoneyOut=@CCbitMoneyOut,@bitChargeBack=@CCbitChargeBack,@numDomainId=@CCnumDomainId,@numUserCntID=@CCnumUserCntID                                
         Select @CCnumCurrentCashCreditCardId=max(numCashCreditId) From CashCreditCardDetails Where numDomainId=@CCnumDomainId                       
         --To insert values into Joural Entry Header Table                              
         Select @numJournal_Id=numJournal_Id,@numAmount=numAmount,@numDomainId=numDomainId From General_Journal_Header Where numCashCreditCardId=@CCnumCashCreditId                          
         Exec USP_InsertJournalEntryHeaderForCashCreditCard  @datEntry_Date=@CurrentDate,@numAmount=@numAmount,@numJournal_Id=0,@numDomainId=@numDomainId,@numCashCreditCardId=@CCnumCurrentCashCreditCardId,@numUserCntID=@CCnumUserCntID                    
  
    
      
         
         --To insert Values into Journal Entry Details table                            
         Exec USP_SaveJournalDetailsForCashCreditCard @numJournalId=@numJournal_Id,@strRow='',@Mode=0,@RecurringMode=1,@numDomainId=@CCnumDomainId         
         --To update LastRecurringDate to CashCreditCard table                            
         Update CashCreditCardDetails set dtLastRecurringDate=getutcdate() Where numCashCreditId=@CCnumCashCreditId And numDomainId=@CCnumDomainId            
         If @bitEndTransactionType=1 and @CCnumNoTransactions<=@numNoTransactionsRecurring          
          Update CashCreditCardDetails set numNoTransactions=isnull(numNoTransactions,0)+1 Where numCashCreditId=@CCnumCashCreditId And numDomainId=@CCnumDomainId                
         -- To update bitRecurring to RecurringTemplate table   
         Update RecurringTemplate Set bitRecurring=1 Where numRecurringId=@numRecurringId And numDomainId=@CCnumDomainId                  
         -- To Save the RecurringDetails to CashCreditCardRecurring Table                        
         Exec USP_SaveCashCreditCardRecurringDetails @CCnumCashCreditId,@CurrentDate                       
        End                     
      End   --End For @tintMonthlyType=1                          
               End  --End for Monthly                        
                                 
   --- For Yearly                          
              If @chrIntervalType='Y'                          
    Begin                          
     print @chrIntervalType                        
     print @LastRecurringDate                        
                    If @LastRecurringDate='Jan  1 1753 12:00AM'                           
      Begin                          
       If @dtStartDate < getutcdate()                   
         Set @dtStartDate=getutcdate()                  
       Set @Year= DATEPART(Year, @dtStartDate)                     
       Print @Year                           
                            Set @DNextDate=dateadd(month,((@Year-1900)*12)+@tintMonths-1,@tintFirstDet-1)                          
                            Print @DNextDate                          
       End                          
                         Else                  
        Begin                           
        Set @DNextDate=DATEADD(Year, convert(numeric,1),  Convert(datetime, @CCLastRecurringDate))                        
        print @DNextDate                            
        End                          
                                    
      Set @DateDiff = datediff (day,@DNextDate,getutcdate())                              
      -- print 'Yearly Diff' + Convert(varchar(2),@DateDiff)                          
                        Print @DateDiff                        
                        If @DateDiff=0 And (@bitEndType=1 Or ((@bitEndType=0 And getutcdate()<=@dtEndDate) Or (@bitEndTransactionType=1 And @CCnumNoTransactions<@numNoTransactionsRecurring)))           
       Begin                              
        Set @CurrentDate=getutcdate()                              
        --To insert values into CashCreditCardDetails                            
        Exec USP_InsertCashCreditHeaderDetails @datEntry_Date=@CurrentDate,@monAmount=@CCmonAmount,@vcMemo=@CCvcMemo,@numPurchaseId=@CCnumPurchaseId,                  
           @numAcntTypeId=@CCnumAcntTypeId,@vcReferenceNo=@CCvcReference,@bitMoneyOut=@CCbitMoneyOut,@bitChargeBack=@CCbitChargeBack,@numDomainId=@CCnumDomainId,@numUserCntID=@CCnumUserCntID                                
        Select @CCnumCurrentCashCreditCardId=max(numCashCreditId) From CashCreditCardDetails Where numDomainId=@CCnumDomainId                          
        --To insert values into Joural Entry Header Table                              
        Select @numJournal_Id=numJournal_Id,@numAmount=numAmount,@numDomainId=numDomainId From General_Journal_Header Where numCashCreditCardId=@CCnumCashCreditId And numDomainId=@CCnumDomainId                           
        Exec USP_InsertJournalEntryHeaderForCashCreditCard  @datEntry_Date=@CurrentDate,@numAmount=@numAmount,@numJournal_Id=0,@numDomainId=@numDomainId,@numCashCreditCardId=@CCnumCurrentCashCreditCardId,@numUserCntID=@CCnumUserCntID                     
  
     
     
        
         
        --To insert Values into Journal Entry Details table                            
        Exec USP_SaveJournalDetailsForCashCreditCard @numJournalId=@numJournal_Id,@strRow='',@Mode=0,@RecurringMode=1,@numDomainId=@CCnumDomainId                          
        --To update LastRecurringDate to CashCreditCard table                            
        Update CashCreditCardDetails set dtLastRecurringDate=getutcdate() Where numCashCreditId=@CCnumCashCreditId And numDomainId=@CCnumDomainId                   
        If @bitEndTransactionType=1 and @CCnumNoTransactions<=@numNoTransactionsRecurring          
         Update CashCreditCardDetails set numNoTransactions=isnull(numNoTransactions,0)+1 Where numCashCreditId=@CCnumCashCreditId And numDomainId=@CCnumDomainId          
        -- To update bitRecurring to RecurringTemplate table                  
        Update RecurringTemplate Set bitRecurring=1 Where numRecurringId=@numRecurringId And numDomainId=@CCnumDomainId                 
        -- To Save the RecurringDetails to CashCreditCardRecurring Table                        
        Exec USP_SaveCashCreditCardRecurringDetails @CCnumCashCreditId,@CurrentDate                        
       End                                       
                   End  --End for Yearly                        
                          
 End  --End for StartDate<=                           
End   --End For @numRecurringId             
                           
Select top 1 @CCnumCashCreditId=numCashCreditId,@CCnumRecurringId=numRecurringId,@CCnumAcntTypeId=numAcntTypeId,@CCbitMoneyOut=bitMoneyOut,@CCnumPurchaseId=numPurchaseId,                    
    @CCvcReference=vcReference,@CCmonAmount=monAmount,@CCvcMemo=vcMemo,@CCbitChargeBack=bitChargeBack,@CCLastRecurringDate=isnull(dtLastRecurringDate,'Jan  1 1753 12:00AM'),@CCnumDomainId=numDomainId,@CCnumUserCntID=numCreatedBy        
 ,@CCnumNoTransactions=isnull(numNoTransactions,0)          
    From CashCreditCardDetails Where numCashCreditId>@CCnumCashCreditId And numCashCreditId <= @CCnumMaxCashCreditId  And isnull(numRecurringId,0)<>0                               
 If @@rowcount=0 set @CCnumCashCreditId=0                            
 -- print @numCheckId                           
End ---End for While                          
                     
-----------------------------------------------------End For Cash or Credit Card Details --------------------------------------------                    
                 
      
-------------------------------------------------For Deposits--------------------------------------------------------                    
                    
---Variable Declaration for Deposits                    
                    
Declare @DnumDepositId as numeric(9)                    
Declare @DnumRecurringId as numeric(9)                    
Declare @DnumAcntTypeId as numeric(9)               
Declare @DmonAmount as DECIMAL(20,5)                    
Declare @DnumDomainId as integer                    
Declare @DLastRecurringDate as datetime                    
Declare @DnumMaxDepositId as integer                    
Declare @DnumCurrentDepositId as integer            
Declare @DnumUserCntID as numeric(9)          
Declare @DnumNoTransactions as numeric(9)          
                    
                    
                               
Select top 1 @DnumDepositId=numDepositId,@DnumRecurringId=numRecurringId,@DnumAcntTypeId=numChartAcntId,                   
    @DmonAmount=monDepositAmount,@DLastRecurringDate=isnull(dtLastRecurringDate,'Jan  1 1753 12:00AM'),          
    @DnumDomainId=numDomainId,@DnumUserCntID=numCreatedBy,@DnumNoTransactions=isnull(numNoTransactions,0) From DepositDetails  Where isnull(numRecurringId,0)<>0            
                    
Select @DnumMaxDepositId=max(numDepositId) From DepositDetails Where isnull(numRecurringId,0)<>0                              
          
Print @DnumMaxDepositId                          
Print @DnumDepositId                          
While @DnumDepositId>0                               
  Begin                              
     Print '@DnumDepositId = ' +convert(varchar(10),@DnumDepositId)                     
     Print '@numRecurringId'+convert(varchar(10),@DnumRecurringId)                     
     If @DnumRecurringId <> 0                              
        Begin                              
     Select @varRecurringTemplateName=varRecurringTemplateName,@chrIntervalType=chrIntervalType,@tintIntervalDays=tintIntervalDays,                              
     @tintMonthlyType=tintMonthlyType,@tintFirstDet=tintFirstDet,@tintWeekDays=tintWeekDays,@tintMonths=tintMonths,@dtStartDate=dtStartDate,                              
     @dtEndDate=dtEndDate,@bitEndType=bitEndType,@bitEndTransactionType=bitEndTransactionType,@numNoTransactionsRecurring=isnull(numNoTransaction,0)          
           From RecurringTemplate Where numRecurringId=@DnumRecurringId                              
                  
     Print ' @chrIntervalType'+ convert(varchar(10), @chrIntervalType)                    
           Print '@dtStartDate'+ convert(varchar(30),@dtStartDate)                    
           If datediff(day,@dtStartDate,getutcdate())>=0                           
              Begin  ---For Daily                       
                 print  @CCLastRecurringDate                    
                 If @chrIntervalType='D'                               
                    Begin -- 'Begin for Daily                              
                      If @DLastRecurringDate='Jan  1 1753 12:00AM'                           
                         Begin                          
                            Set @DNextDate=@dtStartDate                    
                            if @DNextDate < getutcdate()                   
                            Set @DNextDate=getutcdate()                          
                            print @DNextDate                              
                         End                          
                      Else                          
                       Begin                          
                        Set @DNextDate=DATEADD(Day, convert(numeric,@tintIntervalDays),  Convert(datetime, @DLastRecurringDate))                            
            End                          
                      Print @DNextDate                          
                      Set @DateDiff = datediff (day,@DNextDate,getutcdate())                              
                      Print @DateDiff                              
                      Print @bitEndType                          
                      Print @dtEndDate                          
                      Print getutcdate()                          
                      Set @EndDateDiff=datediff (day,@dtEndDate,getutcdate())                             
                      Print @EndDateDiff                          
                      If @DateDiff=0 And (@bitEndType=1 Or ((@bitEndType=0 And @EndDateDiff<=0) Or (@bitEndTransactionType=1 And @DnumNoTransactions<@numNoTransactionsRecurring)))                             
                         Begin              
       Print 'SIVA---Daily '                         
                           Set @CurrentDate=getutcdate()                        
         --To insert values into DepositDetails                            
         Exec  USP_InsertDepositHeaderDet  @numDepositId=0,@numChartAcntId=@DnumAcntTypeId,@datEntry_Date=@CurrentDate,@numAmount=@DmonAmount,@numRecurringId=0,          
         @numDomainId=@DnumDomainId,@numUserCntID=@DnumUserCntID                              
         Select @DnumCurrentDepositId=max(numDepositId) From DepositDetails Where numDomainId=@DnumDomainId                          
         --To insert values into Joural Entry Header Table                              
         Select @numJournal_Id=numJournal_Id,@numAmount=numAmount,@numDomainId=numDomainId From General_Journal_Header Where numDepositId=@DnumDepositId  And numDomainId=@DnumDomainId              
         Exec USP_InsertJournalEntryHeaderForDeposit  @datEntry_Date=@CurrentDate,@numAmount=@numAmount,@numJournal_Id=0,          
         @numDomainId=@numDomainId,@numDepositId=@DnumCurrentDepositId,@numUserCntID=@DnumUserCntID                            
         --To insert Values into Journal Entry Details table                         
         print '@numJournal_Id===MAke Deposit'+Convert(varchar(10),@numJournal_Id)    
   print '@DnumDepositId===Make Deposit'+Convert(varchar(10),@DnumDomainId)    
         Exec USP_SaveJournalDetailsForMakeDeposit @numJournalId=@numJournal_Id,@strRow='',@Mode=0,@RecurringMode=1 ,@numDomainId=@DnumDomainId                           
         --To update LastRecurringDate to DepositDetails table                            
         Update DepositDetails set dtLastRecurringDate=getutcdate() Where numDepositId=@DnumDepositId  And numDomainId=@DnumDomainId               
         If @bitEndTransactionType=1 and @CCnumNoTransactions<=@numNoTransactionsRecurring          
        Update DepositDetails set numNoTransactions=isnull(numNoTransactions,0)+1 Where numDepositId=@DnumDepositId And numDomainId=@DnumDomainId              
         -- To update bitRecurring to RecurringTemplate table                  
         Update RecurringTemplate Set bitRecurring=1 Where numRecurringId=@numRecurringId And numDomainId=@DnumDomainId                    
--         -- To Save the RecurringDetails to CashCreditCardRecurring Table                        
--         Exec USP_SaveCashCreditCardRecurringDetails @CCnumCashCreditId,@CurrentDate                         
                                    
                         End                              
                  End -- 'End for Daily                            
                        
                        
                    --For Monthly                          
                   If @chrIntervalType='M'                          
                      Begin                          
                        Print @chrIntervalType                          
                        Print 'Monthly Type'+ Convert(varchar(3),@tintMonthlyType)                          
                        Print 'Last RecurringDate'+ Convert(varchar(20),@DLastRecurringDate)                          
                        If @tintMonthlyType=0                          
                           Begin -- tinymonthly Begin                          
                             If @DLastRecurringDate='Jan  1 1753 12:00AM'                           
                                 Begin                          
                                    Set @DNextDate=@dtStartDate                        
                                    If @DNextDate < getutcdate()                   
                                       Set @DNextDate=getutcdate()                     
                                    Set @Day=DATEPART(Day, @DNextDate)                          
                                    Set @Month=DATEPART(Month, @DNextDate)                          
                                    Set @Year=DATEPART(Year, @DNextDate)                          
                                    Print @Day                          
                                    Print @Month                          
                                    Print @Year                           
                                    Set @DNextDate=Dateadd(month,((@Year-1900)*12)+@Month-1,@tintFirstDet-1)                          
                                    Print @DNextDate                          
                                 End                            
                               Else                          
                          Begin                          
                                    Set @DNextDate=DATEADD(Month, convert(numeric,@tintIntervalDays),Convert(datetime, @DLastRecurringDate))                           
                          End                           
                              Set @DateDiff=Datediff (Day,@DNextDate,getutcdate())                              
                              Print 'DateDiff'+Convert(varchar(10),@DateDiff)                          
                              If @DateDiff=0 And (@bitEndType=1 Or ((@bitEndType=0 And getutcdate()<=@dtEndDate) Or (@bitEndTransactionType=1 And @DnumNoTransactions<@numNoTransactionsRecurring)))                           
                                 Begin              
         Print 'Siva------------Monthly 1'                       
            Set @CurrentDate=getutcdate()                    
           --To insert values into DepositDetails                            
           Exec  USP_InsertDepositHeaderDet  @numDepositId=0,@numChartAcntId=@DnumAcntTypeId,@datEntry_Date=@CurrentDate,@numAmount=@DmonAmount,@numRecurringId=0,          
           @numDomainId=@DnumDomainId,@numUserCntID=@DnumUserCntID                              
           Select @DnumCurrentDepositId=max(numDepositId) From DepositDetails Where numDomainId=@DnumDomainId                            
           --To insert values into Joural Entry Header Table                              
           Select @numJournal_Id=numJournal_Id,@numAmount=numAmount,@numDomainId=numDomainId From General_Journal_Header Where numDepositId=@DnumDepositId And numDomainId=@DnumDomainId            
           Exec USP_InsertJournalEntryHeaderForDeposit  @datEntry_Date=@CurrentDate,@numAmount=@numAmount,@numJournal_Id=0,          
           @numDomainId=@numDomainId,@numDepositId=@DnumCurrentDepositId,@numUserCntID=@DnumUserCntID                            
           --To insert Values into Journal Entry Details table                            
           Exec USP_SaveJournalDetailsForMakeDeposit @numJournalId=@numJournal_Id,@strRow='',@Mode=0,@RecurringMode=1,@numDomainId=@DnumDomainId                            
           --To update LastRecurringDate to DepositDetails table                            
           Update DepositDetails set dtLastRecurringDate=getutcdate() Where numDepositId=@DnumDepositId And numDomainId=@DnumDomainId              
           If @bitEndTransactionType=1 and @CCnumNoTransactions<=@numNoTransactionsRecurring          
          Update DepositDetails set numNoTransactions=isnull(numNoTransactions,0)+1 Where numDepositId=@DnumDepositId And numDomainId=@DnumDomainId               
          -- To update bitRecurring to RecurringTemplate table                  
           Update RecurringTemplate Set bitRecurring=1 Where numRecurringId=@numRecurringId And numDomainId=@DnumDomainId                  
  ----         -- To Save the RecurringDetails to CashCreditCardRecurring Table                        
  ----         Exec USP_SaveCashCreditCardRecurringDetails @CCnumCashCreditId,@CurrentDate                        
                                End                              
                End   -- tinymonthly End--0                           
                          
    if @tintMonthlyType=1                          
     Begin                          
      if @DLastRecurringDate='Jan  1 1753 12:00AM'                           
       Begin                          
        Set @DNextDate=@dtStartDate                   
        If @DNextDate < getutcdate()                   
         Set @DNextDate=getutcdate()                           
        Set @Month=DATEPART(Month, @DNextDate)                          
        Set @Year=DATEPART(Year, @DNextDate)                          
        Print '@Month = '+ Convert(varchar(10),@Month)                        
        Print @Year                           
        Print '@tintFirstDet='+Convert(varchar(10),@tintFirstDet)                        
        Print '@tintWeekDays='+Convert(varchar(10),@tintWeekDays)                      
        Print '@DNextDate='+Convert(varchar(30),@DNextDate)                        
                    Set @DNextDate=Dateadd(month,((@Year-1900)*12)+@Month-1,@tintFirstDet-1)                          
        print 'DNextDate=='+ convert(varchar(30),@DNextDate)                    
        Set @DNextDate=dbo.fn_RecurringTemplate(@tintFirstDet,@tintWeekDays,@DNextDate)                      
        Print @DNextDate                     
        Set @MonthNextDate=DATEPART(Month, @DNextDate)                       
        print '@MonthNextDate='+Convert(varchar(20),@MonthNextDate)                      
                                                   
          End                            
          Else                          
           Begin   
         Set @DNextDate=DATEADD(Month, convert(numeric,@tintIntervalDays),  Convert(datetime, @DLastRecurringDate))                           
         Set @Month=DATEPART(Month, getutcdate())                     
         Print '@Month = '+ Convert(varchar(10),@Month)                       
         Set @MonthNextDate=DATEPART(Month, @DNextDate)                     
         Print '@MonthNextDate='+Convert(varchar(20),@MonthNextDate)                    
         Set @DNextDate=dbo.fn_RecurringTemplate(@tintFirstDet,@tintWeekDays,@DNextDate)                          
         Print @DNextDate                          
        End                          
              
       Set @DateDiff = datediff (day,@DNextDate,getutcdate())                              
       print 'DateDifference'+Convert(varchar(30),@DateDiff)                          
          If @DateDiff=0 And @Month=@MonthNextDate And (@bitEndType=1 Or ((@bitEndType=0 and getutcdate()<=@dtEndDate) Or (@bitEndTransactionType=1 And @DnumNoTransactions<@numNoTransactionsRecurring)))           
        Begin                              
         Set @CurrentDate=getutcdate()                              
                    Print 'Siva------------Monthly 2'                    
          --To insert values into DepositDetails                            
         Exec  USP_InsertDepositHeaderDet  @numDepositId=0,@numChartAcntId=@DnumAcntTypeId,@datEntry_Date=@CurrentDate,@numAmount=@DmonAmount,@numRecurringId=0,          
         @numDomainId=@DnumDomainId,@numUserCntID=@DnumUserCntID                              
         Select @DnumCurrentDepositId=max(numDepositId) From DepositDetails Where numDomainId=@DnumDomainId                           
         --To insert values into Joural Entry Header Table                              
         Select @numJournal_Id=numJournal_Id,@numAmount=numAmount,@numDomainId=numDomainId From General_Journal_Header Where numDepositId=@DnumDepositId And numDomainId=@DnumDomainId            
         Exec USP_InsertJournalEntryHeaderForDeposit  @datEntry_Date=@CurrentDate,@numAmount=@numAmount,@numJournal_Id=0,          
         @numDomainId=@numDomainId,@numDepositId=@DnumCurrentDepositId,@numUserCntID=@DnumUserCntID                            
         --To insert Values into Journal Entry Details table                            
         Exec USP_SaveJournalDetailsForMakeDeposit @numJournalId=@numJournal_Id,@strRow='',@Mode=0,@RecurringMode=1,@numDomainId=@DnumDomainId                            
         --To update LastRecurringDate to DepositDetails table                            
         Update DepositDetails set dtLastRecurringDate=getutcdate() Where numDepositId=@DnumDepositId And numDomainId=@DnumDomainId                 
         If @bitEndTransactionType=1 and @CCnumNoTransactions<=@numNoTransactionsRecurring          
        Update DepositDetails set numNoTransactions=isnull(numNoTransactions,0)+1 Where numDepositId=@DnumDepositId And numDomainId=@DnumDomainId              
         -- To update bitRecurring to RecurringTemplate table                  
         Update RecurringTemplate Set bitRecurring=1 Where numRecurringId=@numRecurringId And numDomainId=@DnumDomainId                    
----         -- To Save the RecurringDetails to CashCreditCardRecurring Table                        
----         Exec USP_SaveCashCreditCardRecurringDetails @CCnumCashCreditId,@CurrentDate                          
        End                     
      End   --End For @tintMonthlyType=1                          
               End  --End for Monthly                          
                                 
   --- For Yearly                          
           If @chrIntervalType='Y'                   
    Begin                          
     print @chrIntervalType                        
     print @LastRecurringDate                        
                    If @DLastRecurringDate='Jan  1 1753 12:00AM'                           
      Begin      
       If @dtStartDate < getutcdate()                   
         Set @dtStartDate=getutcdate()                  
       Set @Year= DATEPART(Year, @dtStartDate)                     
       Print @Year                           
                            Set @DNextDate=dateadd(month,((@Year-1900)*12)+@tintMonths-1,@tintFirstDet-1)                          
                            Print @DNextDate                          
       End                          
                         Else                          
        Begin                           
        Set @DNextDate=DATEADD(Year, convert(numeric,1),  Convert(datetime, @DLastRecurringDate))                        
        print @DNextDate                            
        End                          
                                    
      Set @DateDiff = datediff (day,@DNextDate,getutcdate())                              
      -- print 'Yearly Diff' + Convert(varchar(2),@DateDiff)                          
                        Print @DateDiff                        
                        If @DateDiff=0 And (@bitEndType=1 Or ((@bitEndType=0 And getutcdate()<=@dtEndDate) Or (@bitEndTransactionType=1 And @DnumNoTransactions<@numNoTransactionsRecurring)))           
       Begin                    
       Print 'Siva------------Yearly'                    
        Set @CurrentDate=getutcdate()                              
         --To insert values into DepositDetails                            
         Exec  USP_InsertDepositHeaderDet  @numDepositId=0,@numChartAcntId=@DnumAcntTypeId,@datEntry_Date=@CurrentDate,@numAmount=@DmonAmount,@numRecurringId=0,          
         @numDomainId=@DnumDomainId,@numUserCntID=@DnumUserCntID                              
         Select @DnumCurrentDepositId=max(numDepositId) From DepositDetails Where numDomainId=@DnumDomainId                            
         --To insert values into Joural Entry Header Table                              
         Select @numJournal_Id=numJournal_Id,@numAmount=numAmount,@numDomainId=numDomainId From General_Journal_Header Where numDepositId=@DnumDepositId And numDomainId=@DnumDomainId            
         Exec USP_InsertJournalEntryHeaderForDeposit  @datEntry_Date=@CurrentDate,@numAmount=@numAmount,@numJournal_Id=0,          
         @numDomainId=@numDomainId,@numDepositId=@DnumCurrentDepositId,@numUserCntID=@DnumUserCntID                            
         --To insert Values into Journal Entry Details table                            
         Exec USP_SaveJournalDetailsForMakeDeposit @numJournalId=@numJournal_Id,@strRow='',@Mode=0,@RecurringMode=1,@numDomainId=@DnumDomainId                            
         --To update LastRecurringDate to DepositDetails table                            
         Update DepositDetails set dtLastRecurringDate=getutcdate() Where numDepositId=@DnumDepositId And numDomainId=@DnumDomainId                
         If @bitEndTransactionType=1 and @CCnumNoTransactions<=@numNoTransactionsRecurring          
        Update DepositDetails set numNoTransactions=isnull(numNoTransactions,0)+1 Where numDepositId=@DnumDepositId And numDomainId=@DnumDomainId              
         -- To update bitRecurring to RecurringTemplate table                  
         Update RecurringTemplate Set bitRecurring=1 Where numRecurringId=@numRecurringId  And numDomainId=@DnumDomainId                  
----         -- To Save the RecurringDetails to CashCreditCardRecurring Table                        
----         Exec USP_SaveCashCreditCardRecurringDetails @CCnumCashCreditId,@CurrentDate                         
       End                                       
                   End  --End for Yearly                        
                          
 End  --End for StartDate<=                           
End   --End For @numRecurringId                          
                              
Select top 1 @DnumDepositId=numDepositId,@DnumRecurringId=numRecurringId,@DnumAcntTypeId=numChartAcntId,           
 @DmonAmount=monDepositAmount,@DLastRecurringDate=isnull(dtLastRecurringDate,'Jan  1 1753 12:00AM'),          
    @DnumDomainId=numDomainId,@DnumUserCntID=numCreatedBy,@DnumNoTransactions=isnull(numNoTransactions,0) From DepositDetails             
    Where numDepositId>@DnumDepositId And numDepositId <= @DnumMaxDepositId And isnull(numRecurringId,0)<>0           
 If @@rowcount=0 set @DnumDepositId=0                            
 -- print @numCheckId                           
End ---End for While                          
                     
-----------------------------------------------------End For Deposit Details --------------------------------------------                    
              
                    
------------------------------------------------------For Journal Entry Details----------------------------------------------------                   
                  
---Variable Declaration for Journal Entry                     
                    
Declare @GJnumJournal_Id as numeric(9)                    
                  
Declare @GJnumRecurringId as numeric(9)                    
Declare @GJvarDescription as varchar(50)                    
Declare @GJnumAmount as DECIMAL(20,5)                    
Declare @GJnumDomainId as integer                    
Declare @GJLastRecurringDate as datetime                    
Declare @GJnumMaxJournalId as integer                    
Declare @GJnumCurrentJournalId as integer            
Declare @GJnumUserCntID as numeric(9)            
Declare @GJnumNoTransactions as numeric(9)          
                               
Select top 1 @GJnumJournal_Id=numJournal_Id, @GJnumRecurringId=numRecurringId,@GJvarDescription=varDescription,          
   @GJnumAmount=numAmount,@GJLastRecurringDate=isnull(dtLastRecurringDate,'Jan  1 1753 12:00AM'),          
   @GJnumDomainId=numDomainId,@GJnumUserCntID=numCreatedBy,@GJnumNoTransactions=isnull(numNoTransactions,0) From General_Journal_Header Where isnull(numRecurringId,0)<>0                  
                  
Select @GJnumMaxJournalId=max(numJournal_Id) From General_Journal_Header  Where isnull(numRecurringId,0)<>0                             
Print @GJnumMaxJournalId                          
Print @GJnumJournal_Id                          
While @GJnumJournal_Id>0                               
 Begin                              
  Print 'GJnumJournal_Id = ' +convert(varchar(10),@GJnumJournal_Id)                     
  Print '@numRecurringId'+convert(varchar(10),@GJnumRecurringId)                     
                          
  If @GJnumRecurringId <> 0                              
   Begin                              
    Select @varRecurringTemplateName=varRecurringTemplateName,@chrIntervalType=chrIntervalType,@tintIntervalDays=tintIntervalDays,                              
      @tintMonthlyType=tintMonthlyType,@tintFirstDet=tintFirstDet,@tintWeekDays=tintWeekDays,@tintMonths=tintMonths,@dtStartDate=dtStartDate,                              
      @dtEndDate=dtEndDate,@bitEndType=bitEndType,@bitEndTransactionType=bitEndTransactionType,@numNoTransactionsRecurring=isnull(numNoTransaction,0)          
      From RecurringTemplate Where numRecurringId=@GJnumRecurringId                              
              
    Print ' @chrIntervalType'+ convert(varchar(10), @chrIntervalType)                    
    Print '@dtStartDate'+ convert(varchar(30),@dtStartDate)                    
                          
    If datediff(day,@dtStartDate,getutcdate())>=0                           
     Begin  ---For Daily                       
      Print 'Sivaprakasam'                       
      Print  @GJLastRecurringDate                    
      If @chrIntervalType='D'                               
       Begin -- 'Begin for Daily                              
        If @GJLastRecurringDate='Jan  1 1753 12:00AM'                           
         Begin                
                               Set @DNextDate=@dtStartDate                    
          if @DNextDate < getutcdate()                   
           Set @DNextDate=getutcdate()                          
                print @DNextDate                              
         End                          
        Else                          
         Begin                          
          Set @DNextDate=DATEADD(Day, convert(numeric,@tintIntervalDays),  Convert(datetime, @GJLastRecurringDate))                            
         End                          
        Print @DNextDate                          
        Set @DateDiff = Datediff (day,@DNextDate,getutcdate())                              
        Print @DateDiff                              
        Print @bitEndType                          
        Print @dtEndDate                          
        Print getutcdate()                          
                       Set @EndDateDiff=Datediff (day,@dtEndDate,getutcdate())                             
              Print @EndDateDiff                          
        If @DateDiff=0 And (@bitEndType=1 Or ((@bitEndType=0 And @EndDateDiff<=0) Or (@bitEndTransactionType=1 And @GJnumNoTransactions<@numNoTransactionsRecurring)))                               
         Begin        
          print 'Daily===========Journal Entry'                           
          Set @CurrentDate=getutcdate()                        
          -- To insert values into Journal Entry Header table             
                                       print '@GJnumDomainId======='+Convert(varchar(1000),@GJnumDomainId)               
          Exec USP_InsertJournalEntryHeader  @datEntry_Date=@CurrentDate,@numAmount=@GJnumAmount,@numJournal_Id=0,          
             @numDomainId=@GJnumDomainId,@numRecurringId=0,@numUserCntID=@GJnumUserCntID            
          --To insert Values into Journal Entry Details table                            
          Exec USP_SaveJournalDetails @numJournalId=@GJnumJournal_Id,@strRow='',@Mode=0,@RecurringMode=1 ,@numDomainId=@GJnumDomainId                           
          --To update LastRecurringDate to General_Journal_Header table               
          Update General_Journal_Header set dtLastRecurringDate=getutcdate() Where numJournal_Id=@GJnumJournal_Id And numDomainId=@GJnumDomainId              
          If @bitEndTransactionType=1 and @GJnumNoTransactions<=@numNoTransactionsRecurring          
           Update General_Journal_Header set numNoTransactions=isnull(numNoTransactions,0)+1 Where numJournal_Id=@GJnumJournal_Id And numDomainId=@GJnumDomainId                  
          -- To update bitRecurring to RecurringTemplate table                  
          Update RecurringTemplate Set bitRecurring=1 Where numRecurringId=@numRecurringId    And numDomainId=@GJnumDomainId                
          -- To Save the RecurringDetails to CashCreditCardRecurring Table                        
          --  Exec USP_SaveCashCreditCardRecurringDetails @CCnumCashCreditId,@CurrentDate                         
            End                              
   End -- 'End for Daily                            
                     
    --For Monthly                          
            If @chrIntervalType='M'                          
    Begin                          
     Print @chrIntervalType                          
     Print 'Monthly Type'+ Convert(varchar(3),@tintMonthlyType)                   
                    Print 'Last RecurringDate'+ Convert(varchar(20),@GJLastRecurringDate)                          
     If @tintMonthlyType=0                          
      Begin -- tinymonthly Begin                          
       If @GJLastRecurringDate='Jan  1 1753 12:00AM'                           
        Begin                          
         Set @DNextDate=@dtStartDate                        
                                    If @DNextDate < getutcdate()                   
          Set @DNextDate=getutcdate()                     
                                    Set @Day=DATEPART(Day, @DNextDate)                          
                                    Set @Month=DATEPART(Month, @DNextDate)                          
                                    Set @Year=DATEPART(Year, @DNextDate)                          
                                    Print @Day                          
                                    Print @Month                          
                                    Print @Year                           
                                    Set @DNextDate=Dateadd(month,((@Year-1900)*12)+@Month-1,@tintFirstDet-1)                          
         Print @DNextDate                          
                                 End                            
        Else                          
         Begin                          
          Set @DNextDate=DATEADD(Month, convert(numeric,@tintIntervalDays),Convert(datetime, @GJLastRecurringDate))                           
            End                           
       Set @DateDiff=Datediff (day,@DNextDate,getutcdate())                              
       Print 'DateDiff'+convert(varchar(10),@DateDiff)                          
       If @DateDiff=0 And (@bitEndType=1 Or ((@bitEndType=0 And getutcdate()<=@dtEndDate) Or (@bitEndTransactionType=1 And @GJnumNoTransactions<@numNoTransactionsRecurring)))           
        Begin                           
         Set @CurrentDate=getutcdate()                              
                                
         -- To insert values into Journal Entry Header table                  
         Exec USP_InsertJournalEntryHeader  @datEntry_Date=@CurrentDate,@numAmount=@GJnumAmount,@numJournal_Id=0,          
            @numDomainId=@GJnumDomainId,@numRecurringId=0,@numUserCntID=@GJnumUserCntID              
         --To insert Values into Journal Entry Details table                            
         Exec USP_SaveJournalDetails @numJournalId=@GJnumJournal_Id,@strRow='',@Mode=0,@RecurringMode=1,@numDomainId=@GJnumDomainId                            
         --To update LastRecurringDate to General_Journal_Header table                            
         Update General_Journal_Header set dtLastRecurringDate=getutcdate() Where numJournal_Id=@GJnumJournal_Id And numDomainId=@GJnumDomainId           
         If @bitEndTransactionType=1 and @GJnumNoTransactions<=@numNoTransactionsRecurring          
           Update General_Journal_Header set numNoTransactions=isnull(numNoTransactions,0)+1 Where numJournal_Id=@GJnumJournal_Id  And numDomainId=@GJnumDomainId                    
         -- To update bitRecurring to RecurringTemplate table                  
         Update RecurringTemplate Set bitRecurring=1 Where numRecurringId=@numRecurringId  And numDomainId=@GJnumDomainId                  
         -- To Save the RecurringDetails to CashCreditCardRecurring Table                        
         --  Exec USP_SaveCashCreditCardRecurringDetails @CCnumCashCreditId,@CurrentDate                        
               End                              
                End   -- tinymonthly End--0                           
                          
    If @tintMonthlyType=1                          
     Begin                          
      If @GJLastRecurringDate='Jan  1 1753 12:00AM'                           
       Begin                          
        Set @DNextDate=@dtStartDate                   
        if @DNextDate < getutcdate()                   
        Set @DNextDate=getutcdate()                           
        Set @Month=DATEPART(Month, @DNextDate)                          
        Set  @Year=DATEPART(Year, @DNextDate)                          
        Print '@Month = '+ Convert(varchar(10),@Month)                        
        Print @Year                           
        Print '@tintFirstDet=' + Convert(varchar(10),@tintFirstDet)                        
        Print '@tintWeekDays=' + Convert(varchar(10),@tintWeekDays)                      
        Print '@DNextDate=' + Convert(varchar(30),@DNextDate)                        
        Set @DNextDate=dateadd(month,((@Year-1900)*12)+@Month-1,@tintFirstDet-1)                          
        Print 'DNextDate==' + convert(varchar(30),@DNextDate)                    
        Set @DNextDate=dbo.fn_RecurringTemplate(@tintFirstDet,@tintWeekDays,@DNextDate)                      
        Print @DNextDate                     
        Set @MonthNextDate=DATEPART(Month, @DNextDate)                       
        Print '@MonthNextDate='+Convert(varchar(20),@MonthNextDate)                      
       End                            
                            Else                          
        Begin                          
         Set @DNextDate=DATEADD(Month, convert(numeric,@tintIntervalDays),  Convert(datetime, @GJLastRecurringDate))                           
         Set @Month=DATEPART(Month, getutcdate())                     
         Print '@Month = '+ Convert(varchar(10),@Month)                       
         Set @MonthNextDate=DATEPART(Month, @DNextDate)                     
                                    Print '@MonthNextDate='+Convert(varchar(20),@MonthNextDate)                    
                                    Set @DNextDate=dbo.fn_RecurringTemplate(@tintFirstDet,@tintWeekDays,@DNextDate)                          
                    Print @DNextDate                          
              End                          
                            Set @DateDiff = datediff (day,@DNextDate,getutcdate())                              
       Print 'DateDifference'+Convert(varchar(30),@DateDiff)                          
       If @DateDiff=0 And @Month=@MonthNextDate And (@bitEndType=1 Or ((@bitEndType=0 And getutcdate()<=@dtEndDate) Or (@bitEndTransactionType=1 And @GJnumNoTransactions<@numNoTransactionsRecurring)))                              
        Begin                              
         Set @CurrentDate=getutcdate()                              
                                        
         -- To insert values into Journal Entry Header table                  
         Exec USP_InsertJournalEntryHeader  @datEntry_Date=@CurrentDate,@numAmount=@GJnumAmount,@numJournal_Id=0,          
         @numDomainId=@GJnumDomainId,@numRecurringId=0,@numUserCntID=@GJnumUserCntID                   
         --To insert Values into Journal Entry Details table                            
         Exec USP_SaveJournalDetails @numJournalId=@GJnumJournal_Id,@strRow='',@Mode=0,@RecurringMode=1,@numDomainId=@GJnumDomainId                
         --To update LastRecurringDate to General_Journal_Header table                            
         Update General_Journal_Header set dtLastRecurringDate=getutcdate() Where numJournal_Id=@GJnumJournal_Id  And numDomainId=@GJnumDomainId               
         If @bitEndTransactionType=1 and @GJnumNoTransactions<=@numNoTransactionsRecurring          
         Update General_Journal_Header set numNoTransactions=isnull(numNoTransactions,0)+1 Where numJournal_Id=@GJnumJournal_Id  And numDomainId=@GJnumDomainId               
         -- To update bitRecurring to RecurringTemplate table                  
         Update RecurringTemplate Set bitRecurring=1 Where numRecurringId=@numRecurringId And numDomainId=@GJnumDomainId                 
         -- To Save the RecurringDetails to CashCreditCardRecurring Table                        
         --  Exec USP_SaveCashCreditCardRecurringDetails @CCnumCashCreditId,@CurrentDate                          
        End                     
                  End   --End For @tintMonthlyType=1                          
          End  --End for Monthly                          
                                
          --- For Yearly                          
          If @chrIntervalType='Y'                          
   Begin                          
    Print @chrIntervalType                        
    Print @GJLastRecurringDate                        
                If @GJLastRecurringDate='Jan  1 1753 12:00AM'                           
     Begin                          
      If @dtStartDate < getutcdate()                   
       Set @dtStartDate=getutcdate()                  
      Set @Year= DATEPART(Year, @dtStartDate)                     
      Print @Year                           
                        Set @DNextDate=dateadd(month,((@Year-1900)*12)+@tintMonths-1,@tintFirstDet-1)                          
                        Print @DNextDate                          
                     End                          
                     Else                          
      Begin                           
       Set @DNextDate=DATEADD(Year, convert(numeric,1),  Convert(datetime, @GJLastRecurringDate))                        
                            Print @DNextDate                            
                         End                          
                                    
    Set @DateDiff = datediff (day,@DNextDate,getutcdate())                              
                -- print 'Yearly Diff' + Convert(varchar(2),@DateDiff)                          
    Print @DateDiff                        
                If @DateDiff=0 And (@bitEndType=1 Or ((@bitEndType=0 And getutcdate()<=@dtEndDate) Or (@bitEndTransactionType=1 And @GJnumNoTransactions<@numNoTransactionsRecurring)))                            
     Begin                        
      Set @CurrentDate=getutcdate()                              
      -- To insert values into Journal Entry Header table                  
      Exec USP_InsertJournalEntryHeader  @datEntry_Date=@CurrentDate,@numAmount=@GJnumAmount,@numJournal_Id=0,          
        @numDomainId=@GJnumDomainId,@numRecurringId=0,@numUserCntID=@GJnumUserCntID                    
      --To insert Values into Journal Entry Details table       
                               
      Exec USP_SaveJournalDetails @numJournalId=@GJnumJournal_Id,@strRow='',@Mode=0,@RecurringMode=1,@numDomainId=@GJnumDomainId                            
      --To update LastRecurringDate to General_Journal_Header table                            
      Update General_Journal_Header set dtLastRecurringDate=getutcdate() Where numJournal_Id=@GJnumJournal_Id And numDomainId=@GJnumDomainId          
      If @bitEndTransactionType=1 and @GJnumNoTransactions<=@numNoTransactionsRecurring          
      Update General_Journal_Header set numNoTransactions=isnull(numNoTransactions,0)+1 Where numJournal_Id=@GJnumJournal_Id  And numDomainId=@GJnumDomainId                   
      -- To update bitRecurring to RecurringTemplate table                  
      Update RecurringTemplate Set bitRecurring=1 Where numRecurringId=@numRecurringId  And numDomainId=@GJnumDomainId                  
      -- To Save the RecurringDetails to CashCreditCardRecurring Table                        
      --  Exec USP_SaveCashCreditCardRecurringDetails @CCnumCashCreditId,@CurrentDate                     
     End                                       
      End  --End for Yearly                        
                          
 End  --End for StartDate<=                           
End   --End For @numRecurringId                          
                           
Select top 1 @GJnumJournal_Id=numJournal_Id, @GJnumRecurringId=numRecurringId,@GJvarDescription=varDescription,@GJnumAmount=numAmount,          
   @GJLastRecurringDate=isnull(dtLastRecurringDate,'Jan  1 1753 12:00AM'),@GJnumDomainId=numDomainId,@GJnumUserCntID=numCreatedBy,@GJnumNoTransactions=isnull(numNoTransactions,0)          
   From General_Journal_Header Where numJournal_Id>@GJnumJournal_Id And numJournal_Id<= @GJnumMaxJournalId  And isnull(numRecurringId,0)<>0                            
                                
If @@rowcount=0 set @GJnumJournal_Id=0                            
-- print @numCheckId                           
          
End ---End for While                    
                   
                    
------------------------------------------------------End For Journal Entry Details------------------------------------------------                    
           
             
                   
End   --End for procedure                          
                              
                           
--Exec USP_CreateRecurring                        
                       
--Select * from CashCreditCardDetails                  
--Select * from RecurringTemplate                  
--Select * from CheckDetails                  
              
--Select * From DepositDetails          
          
----Select * From General_Journal_Header          
----Select * From General_Journal_Details
GO
