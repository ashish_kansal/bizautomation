/****** Object:  StoredProcedure [dbo].[USP_ForRepByTeam]    Script Date: 07/26/2008 16:16:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj        
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_forrepbyteam')
DROP PROCEDURE usp_forrepbyteam
GO
CREATE PROCEDURE [dbo].[USP_ForRepByTeam]          
@numUserCntId as numeric(9)=null,          
@numDomainID as numeric(9)=null,          
@tintType as tinyint          
as          
begin          
select A.numContactID,A.vcFirstName+' '+A.vcLastName as vcGivenName,A.numTeam,lst.vcdata from usermaster U          
join AdditionalContactsInformation A          
on U.numUserdetailId=A.numcontactid            
left Join listdetails lst          
on lst.numListitemid=A.numteam          
where A.numTeam is not null           
and A.numTeam in(select F.numTeam from ForReportsByTeam F           
where F.numUserCntId=@numUserCntId and F.numDomainid=@numDomainID and F.tintType=@tintType)          
order by A.numTeam          
end
GO
