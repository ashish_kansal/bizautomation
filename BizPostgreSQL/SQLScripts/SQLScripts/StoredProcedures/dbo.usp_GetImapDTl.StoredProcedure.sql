/****** Object:  StoredProcedure [dbo].[usp_GetImapDTl]    Script Date: 07/26/2008 16:17:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getimapdtl')
DROP PROCEDURE usp_getimapdtl
GO
CREATE PROCEDURE [dbo].[usp_GetImapDTl]
    @numUserCntId AS NUMERIC(9),
    @numDomainId AS NUMERIC(9)
AS 

IF @numUserCntId = -1 AND @numDomainID > 0 --Support Email
 BEGIN                            
	  SELECT  ISNULL(bitImap, 0) bitImap,
        vcImapServerUrl,
        vcImapPassword,
        ISNULL(bitSSl, 0) bitSSl,
        numPort,
        vcImapUserName AS vcEmailID,
        ISNULL(numLastUID,0) AS LastUID,
        ISNULL(bitUseUserName, 0) bitUseUserName,isnull(vcGoogleRefreshToken,'') as vcGoogleRefreshToken
	FROM [ImapUserDetails] ImapUserDTL   
	where ImapUserDTL.numUserCntId = @numUserCntId 
	AND ImapUserDTL.numDomainID = @numDomainID
 END   
 ELSE
 BEGIN
SELECT  ISNULL(bitImap, 0) bitImap,
        vcImapServerUrl,
        vcImapPassword,
        ISNULL(bitSSl, 0) bitSSl,
        numPort,
        vcEmailID,
        ISNULL(numLastUID,0) AS LastUID,
        ISNULL(bitUseUserName, 0) bitUseUserName,isnull(vcGoogleRefreshToken,'') as vcGoogleRefreshToken
FROM    [ImapUserDetails]
        JOIN usermaster ON numUserDetailId = numUserCntId
WHERE   numUserCntId = @numUserCntId
        AND [ImapUserDetails].numDomainId = @numDomainId
END        
GO
