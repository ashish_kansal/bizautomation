/****** Object:  StoredProcedure [dbo].[USP_CampaignOppList]    Script Date: 07/26/2008 16:15:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--- Created By Anoop jayaraj    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_campaignopplist')
DROP PROCEDURE usp_campaignopplist
GO
CREATE PROCEDURE [dbo].[USP_CampaignOppList]
    @numCampaignid AS NUMERIC(9),
    @tintComptype AS TINYINT,
    @NoofRecds AS INTEGER OUTPUT
AS 
--    DECLARE @intLaunchDate AS VARCHAR(20)    
--    DECLARE @intEndDate AS VARCHAR(20)
    DECLARE @bitIsOnline AS BIT
    DECLARE @strSql AS VARCHAR(8000)
    DECLARE @strWhere AS VARCHAR(8000)
    DECLARE @numDomainID NUMERIC(9)
    
    SELECT TOP 1
--            @intLaunchDate = intLaunchDate,
--            @intEndDate = intEndDate,
--            @bitIsOnline = bitIsOnline,
             @numDomainID = [numDomainID]
    FROM    campaignmaster
    WHERE   numCampaignid = @numCampaignid        

--    IF @bitIsOnline = 1 
--        BEGIN
--            SET @strSql = '
--					SELECT  C.numCompanyID,
--							Div.numDivisionID,
--							ADC.numContactID,
--							Div.tintCRMType,
--							Opp.numOppId,
--							Opp.vcPOppName AS Name,
--							C.vcCompanyName + '', '' + Div.vcDivisionName AS Company,
--							dbo.GetOppLstStage(Opp.numOppId) AS Stage,
--							dbo.GetOppLstMileStonePercentage(numOppId) AS Milestone,
--							Opp.monPAmount
--					FROM    OpportunityMaster Opp
--							INNER JOIN AdditionalContactsInformation ADC ON Opp.numContactId = ADC.numContactId
--							INNER JOIN DivisionMaster Div ON Opp.numDivisionId = Div.numDivisionID
--															 AND ADC.numDivisionId = Div.numDivisionID
--							INNER JOIN CompanyInfo C ON Div.numCompanyID = C.numCompanyId
--							INNER JOIN [TrackingCampaignData] T ON T.numDivisionID = Div.[numDivisionID]
--					WHERE   opp.bintCreatedDate >= '''+ convert(varchar(23),@intLaunchDate) + '''
--							AND opp.bintCreatedDate < '''+ convert(varchar(23),@intEndDate) + '''
--							AND T.numCampaignID = ' + CONVERT(VARCHAR,@numCampaignid)
--        END
--    ELSE 
--        BEGIN
            SET @strSql = ' 
           SELECT  C.numCompanyID,
                    Div.numDivisionID,
                    ADC.numContactID,
                    Div.tintCRMType,
                    Opp.numOppId,
                    Opp.vcPOppName AS Name,
                    C.vcCompanyName + '', '' + Div.vcDivisionName AS Company,
--                    dbo.GetOppLstStage(Opp.numOppId) AS Stage,
                    dbo.GetOppLstMileStonePercentage(numOppId) AS Milestone,
                    Opp.monPAmount
            FROM    OpportunityMaster Opp
                    INNER JOIN AdditionalContactsInformation ADC ON Opp.numContactId = ADC.numContactId
                    INNER JOIN DivisionMaster Div ON Opp.numDivisionId = Div.numDivisionID
                                                     AND ADC.numDivisionId = Div.numDivisionID
                    INNER JOIN CompanyInfo C ON Div.numCompanyID = C.numCompanyId
                    LEFT JOIN CampaignMaster CAMP ON CAMP.numCampaignId = Opp.numCampainID AND CAMP.numDomainID = Opp.numDomainID
            WHERE   Div.numDomainID = ' + convert(varchar,@numDomainID) + ' and 
					Opp.numCampainID = ' + CONVERT(VARCHAR,@numCampaignid)
                    --(CAMP.numCampaignID = ' + CONVERT(VARCHAR,@numCampaignid) + ' OR Div.numCampaignID = ' + CONVERT(VARCHAR,@numCampaignid) + ')'
                    
--        END
--    
    
    
    IF @tintComptype = 0 -- All Records    
        BEGIN     
            PRINT @strSql
			EXEC(@strSql)
            SET @NoofRecds = @@rowcount    
        END    
    
    IF @tintComptype = 1 -- Sales Open opportunities    
        BEGIN     
            
			SET @strWhere = ' AND opp.tintOppstatus = 0 AND opp.tintOppType = 1 '
			SET @strSql = @strSql + @strWhere;
			
			PRINT @strSql
			EXEC(@strSql)                   
            SET @NoofRecds = @@rowcount    
        END    
    
    IF @tintComptype = 2 -- Purchase Open opportunities    
        BEGIN     
             
			SET @strWhere = ' AND opp.tintOppstatus = 0 AND opp.tintOppType = 2 '
			SET @strSql = @strSql + @strWhere;
			
			PRINT @strSql
			EXEC(@strSql)              
            SET @NoofRecds = @@rowcount    
        END    
    
    IF @tintComptype = 3 -- Sales Deal Won    
        BEGIN     
        
            SET @strWhere = ' AND opp.tintOppstatus = 1 AND opp.tintOppType = 1 '
			SET @strSql = @strSql + @strWhere;
			
			PRINT @strSql
			EXEC(@strSql)         
            SET @NoofRecds = @@rowcount    
        END    
    
    IF @tintComptype = 4 -- Purchase Deal Won    
        BEGIN     
			SET @strWhere = ' AND opp.tintOppstatus = 1 AND opp.tintOppType = 2 '
			SET @strSql = @strSql + @strWhere;
			
			PRINT @strSql
			EXEC(@strSql)                       
            SET @NoofRecds = @@rowcount    
        END    
    
    IF @tintComptype = 5 -- Sales Deal Lost    
        BEGIN     
			SET @strWhere = ' AND opp.tintOppstatus = 2 AND opp.tintOppType = 1 '
			SET @strSql = @strSql + @strWhere;
			
			PRINT @strSql
			EXEC(@strSql)
            SET @NoofRecds = @@rowcount    
        END    
    
    IF @tintComptype = 6 -- Purchase Deal Lost    
        BEGIN     
            
			SET @strWhere = ' AND opp.tintOppstatus = 2 AND opp.tintOppType = 2 '
			SET @strSql = @strSql + @strWhere;
			
			PRINT @strSql
			EXEC(@strSql)                    
            SET @NoofRecds = @@rowcount    
        END
GO
