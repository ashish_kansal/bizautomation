/****** Object:  StoredProcedure [dbo].[usp_DeleteAssociations]    Script Date: 07/26/2008 16:15:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_deleteassociations')
DROP PROCEDURE usp_deleteassociations
GO
CREATE PROCEDURE [dbo].[usp_DeleteAssociations]    
@numAssociationID numeric,    
@numUserCntID numeric    
--    
as    
    
--update CompanyAssociations set bitDeleted = 1, numModifiedBy=@numUserCntID, numModifiedOn=getutcdate()  where numAssociationID = @numAssociationID
DELETE FROM [CompanyAssociations] WHERE [numAssociationID]=@numAssociationID

