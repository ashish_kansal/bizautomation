
GO
/****** Object:  StoredProcedure [dbo].[usp_GetStageItemDetails]    Script Date: 09/23/2010 15:24:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--usp_GetStageItemDetails              
              
--This procedure will return the values from the stagepercentagedetails table. If the stage number is passed then              
--the details pertaining to that stge will be passed else all the records will be passed              
--EXEC usp_GetStageItemDetails 1,133,1
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getstageitemdetails')
DROP PROCEDURE usp_getstageitemdetails
GO
CREATE PROCEDURE [dbo].[usp_GetStageItemDetails]
--    @numStageNo NUMERIC = 0,
    @numDomainid NUMERIC = 0,
    @slpid NUMERIC = 0 ,
    @tintMode TINYINT=0,
	@numOppId NUMERIC=0,
	@numProjectId NUMERIC=0
--              
AS 
IF @tintMode = 0 
BEGIN              
        SELECT  numStageDetailsId,
                numStagePercentageId,
                vcStageName,
                sp.numDomainId,
                sp.numCreatedBy,
                sp.bintCreatedDate,
                sp.numModifiedBy,
                sp.bintModifiedDate,
                slp_id,
                sp.numAssignTo,
                vcMilestoneName,
				ISNULL((select
				   DISTINCT
					stuff((
						select ',' + ST.vcTaskName+'('+ISNULL((SELECT TOP 1 (AU.vcFirstName+' '+AU.vcLastName) FROM AdditionalContactsInformation AS AU WHERE AU.numContactID=ST.numAssignTo),'-')+')'
						from StagePercentageDetailsTask ST
						where ST.numStageDetailsId = STU.numStageDetailsId AND ISNULL(ST.numOppId,0)=0 AND  ISNULL(ST.numProjectId,0)=0
						order by ST.numTaskId
						for xml path('')
					),1,1,'') as TaskName
				from StagePercentageDetailsTask AS STU  WHERE STU.numStageDetailsID=sp.numStageDetailsId AND ISNULL(STU.numOppId,0)=0 AND  ISNULL(STU.numProjectId,0)=0
				group by numStageDetailsId),'-') As TaskName,
				(SELECT ISNULL(SUM(numHours),0) FROM StagePercentageDetailsTask WHERE numStageDetailsId=sp.numStageDetailsId) As numHours,
				(SELECT ISNULL(SUM(numMinutes),0) FROM StagePercentageDetailsTask WHERE numStageDetailsId=sp.numStageDetailsId)  As numMinutes,
                CASE WHEN sp.numAssignTo = 0 THEN '--Select One--'
                     ELSE A.vcFirstName + ' ' + A.vcLastName
                END AS vcAssignTo,
                ISNULL(tintPercentage, '') tintPercentage,
                ISNULL(bitClose, 0) bitClose,
                tintConfiguration,
                ISNULL(vcDescription,'') vcDescription,
                ISNULL(numParentStageID,0) numParentStageID,
				ISNULL(bitIsDueDaysUsed,0) AS bitIsDueDaysUsed,
				ISNULL(numTeamId,0) AS numTeamId,
				ISNULL(bitRunningDynamicMode,0) AS bitRunningDynamicMode,
                ISNULL(intDueDays,0) AS intDueDays,(select count(*) from StageDependency where numStageDetailID=sp.numStageDetailsId) numDependencyCount,
				(convert(varchar(10),sp.dtStartDate, 101) + right(convert(varchar(32),sp.dtStartDate,100),8)) AS StageStartDate,
				(select dbo.fn_calculateTotalHoursMinutesByStageDetails(sp.numStageDetailsId,@numOppId,@numProjectId)) as dayshoursTaskDuration,
				ISNULL(SP.numStageOrder,0) As numStageOrder,
				CASE WHEN @numProjectId>0 THEN 
				ISNULL((SELECT ISNULL(intTotalProgress,0) FROM dbo.ProjectProgress WHERE numDomainId=@numDomainID AND numProId=@numProjectId),0)
				WHEN @numOppId>0 THEN
				ISNULL((SELECT ISNULL(intTotalProgress,0) FROM dbo.ProjectProgress WHERE numDomainId=@numDomainID AND numOppId=@numOppId),0) ELSE 0 END AS intTotalProgress
        FROM    stagepercentagedetails sp
                LEFT JOIN AdditionalContactsInformation A ON sp.numAssignTo = A.numContactID
        WHERE  
                sp.numdomainid = @numDomainid
                AND slp_id = @slpid
                AND ISNULL(sp.numProjectID,0)= @numProjectId
                AND ISNULL(sp.numOppID,0)= @numOppId
        ORDER BY numstagepercentageid,
                numStageOrder ASC              
    END              
ELSE IF @tintMode = 1 -- Fill dropdown
SELECT numStageDetailsId,vcStageName FROM dbo.StagePercentageDetails WHERE slp_id = @slpid AND numDomainId=@numDomainid
	AND LEN(vcStageName) > 1

ELSE IF  @tintMode = 2
SELECT [numStageDetailsId],[numStagePercentageId],tintConfiguration
      ,[vcStageName],[numDomainId],[numCreatedBy],
	(Case bitFromTemplate when 0 then isnull(dbo.fn_GetContactName([numCreatedBy]),'-') else 'Template' end) as vcCreatedBy,dbo.[FormatedDateFromDate](ISNULL(bintCreatedDate,GETDATE()),@numDomainId) bintCreatedDate
      ,[numModifiedBy],isnull(dbo.fn_GetContactName([numModifiedBy]),'-') as vcModifiedBy
      ,dbo.[FormatedDateTimeFromDate](ISNULL(bintModifiedDate,GETDATE()),@numDomainId) bintModifiedDate,[slp_id]
      ,[numAssignTo],isnull(dbo.fn_GetContactName(numAssignTo),'-') as vcAssignTo
      ,[vcMileStoneName],[tintPercentage],isnull([tinProgressPercentage],0) tinProgressPercentage,[bitClose]
      ,dbo.[FormatedDateFromDate](ISNULL(dtStartDate,GETDATE()),@numDomainId) dtStartDate,dbo.[FormatedDateFromDate](ISNULL(dtEndDate,GETDATE()),@numDomainId) dtEndDate,[numParentStageID],[intDueDays]
      ,[numProjectID],[numOppID],[vcDescription], 
  isnull(dbo.fn_GetExpenseDtlsbyProStgID(numProjectID,numStageDetailsId,0),0) as numExpense,                        
  isnull(dbo.fn_GeTimeDtlsbyProStgID_BT_NBT(numProjectID,numStageDetailsId,1),'Billable Time (0)  Non Billable Time (0)') as numTime,
(select count(*) from dbo.GenericDocuments where numRecID=numStageDetailsId and vcDocumentSection='PS') numDocuments,
(select count(*) from [StagePercentageDetails] where numParentStageId=p.numStageDetailsId and numProjectID=p.numProjectID) as numChildCount,
isnull(bitTimeBudget,0) as bitTimeBudget,isnull(bitExpenseBudget,0) as bitExpenseBudget,isnull(monTimeBudget,0) as monTimeBudget,isnull(monExpenseBudget,0) as monExpenseBudget
  FROM [StagePercentageDetails] p where [numStageDetailsId]=@slpid

ELSE IF  @tintMode = 3
SELECT count(*) Total
  FROM [StagePercentageDetails] p where numProjectID=@slpid and numDomainid=@numDomainid

ELSE IF  @tintMode =4
SELECT count(*) Total
  FROM [StagePercentageDetails] p where numOppID=@slpid and numDomainid=@numDomainid

ELSE
BEGIN
	SELECT numStageDetailsId,vcStageName FROM dbo.StagePercentageDetails WHERE slp_id = @slpid AND numDomainId=@numDomainid
	AND LEN(vcStageName) > 1
END
--    BEGIN              
--        SELECT  numStageDetailsId,
--                numStagePercentageId,
--                numStageNumber,
--                vcStageDetail,
--                sp.numDomainId,
--                sp.numCreatedBy,
--                sp.bintCreatedDate,
--                sp.numModifiedBy,
--                sp.bintModifiedDate,
--                slp_id,
--                sp.numAssignTo,
--                vcStagePercentageDtl,
--                CASE WHEN sp.numAssignTo = 0 THEN '--Select One--'
--                     ELSE A.vcFirstName + ' ' + A.vcLastName
--                END AS vcAssignTo,
--                numTemplateId,
--                ISNULL(tintPercentage, '') tintPercentage,
--                ISNULL(bitClose, 0) bitClose
--        FROM    stagepercentagedetails sp
--                LEFT JOIN AdditionalContactsInformation A ON sp.numAssignTo = A.numContactID
--        WHERE   sp.numdomainid = @numDomainid
--                AND slp_id = @slpid
--        ORDER BY numstagepercentageid,
--                numStageDetailsId ASC              
            
            
   --SELECT * FROM stagepercentagedetails             
            
--    END
