/****** Object:  StoredProcedure [dbo].[usp_GetCompanyInfo]    Script Date: 07/26/2008 16:16:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--modified by anoop jayaraj                                    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getcompanyinfo')
DROP PROCEDURE usp_getcompanyinfo
GO
CREATE PROCEDURE [dbo].[usp_GetCompanyInfo]                                                                                                   
 @numDivisionID numeric,        
 @numDomainID as numeric(9)   ,        
@ClientTimeZoneOffset  int           
    --                                                         
AS                                                          
BEGIN                                                          
 SELECT CMP.numCompanyID, CMP.vcCompanyName,DM.numStatusID as numCompanyStatus,                                                          
  CMP.numCompanyRating, CMP.numCompanyType, DM.bitPublicFlag, DM.numDivisionID,                                                           
  DM. vcDivisionName, 
  ISNULL(AD1.numAddressID,0) as numBillAddressID,
  isnull(AD1.vcStreet,'') as vcBillStreet, 
  isnull(AD1.vcCity,'') as vcBillCity, 
  isnull(dbo.fn_GetState(AD1.numState),'') as vcBilState,
  isnull(AD1.vcPostalCode,'') as vcBillPostCode,
  isnull(dbo.fn_GetListName(AD1.numCountry,0),'')  as vcBillCountry,
  ISNULL(AD2.numAddressID,0) as numShipAddressID,
  isnull(AD2.vcStreet,'') as vcShipStreet,
  isnull(AD2.vcCity,'') as vcShipCity,
  isnull(dbo.fn_GetState(AD2.numState),'')  as vcShipState,
  isnull(AD2.vcPostalCode,'') as vcShipPostCode, 
  AD2.numCountry vcShipCountry,
  numFollowUpStatus,
  CMP.vcWebLabel1, CMP.vcWebLink1, CMP.vcWebLabel2, CMP.vcWebLink2,                                                           
  CMP.vcWebLabel3, CMP.vcWebLink3, CMP.vcWeblabel4, CMP.vcWebLink4,  
   tintBillingTerms,numBillingDays,
   --ISNULL(dbo.fn_GetListItemName(isnull(numBillingDays,0)),0) AS numBillingDaysName,
   (SELECT numNetDueInDays FROM dbo.BillingTerms WHERE numTermsID = numBillingDays AND numDomainID = DM.numDomainID) AS numBillingDaysName,
   tintInterestType,fltInterest,                                                          
   DM.bitPublicFlag, ISNULL(DM.numTerID,0) numTerID,                                                           
  CMP.numCompanyIndustry, CMP.numCompanyCredit,                                                           
  isnull(CMP.txtComments,'') as txtComments, isnull(CMP.vcWebSite,'') vcWebSite, CMP.numAnnualRevID, CMP.numNoOfEmployeesID, dbo.fn_GetListItemName(CMP.numNoOfEmployeesID) as NoofEmp,                                                         
  CMP.vcProfile, DM.numCreatedBy, ISNULL(DM.numRecOwner,0) AS numRecOwner, dbo.fn_GetContactName(DM.numRecOwner) as RecOwner,       
dbo.fn_GetContactName(DM.numCreatedBy)+' ' +convert(varchar(20),dateadd(minute,-@ClientTimeZoneOffset,DM.bintCreatedDate)) as vcCreatedBy,                                                    
 dbo.fn_GetContactName(DM.numModifiedBy)+' ' +convert(varchar(20),dateadd(minute,-@ClientTimeZoneOffset,DM.bintModifiedDate)) as vcModifiedBy,vcComPhone,vcComFax,                                                 
  DM.numGrpID, CMP.vcHow,                                                          
  DM.tintCRMType,isnull(DM.bitNoTax,0) bitNoTax ,                                                      
   numCampaignID,numAssignedBy,numAssignedTo,            
(select  count(*) from dbo.GenericDocuments where numRecID= @numDivisionID and  vcDocumentSection='A' AND tintDocumentType=2) as DocumentCount ,          
(SELECT count(*)from CompanyAssociations where numDivisionID=@numDivisionID and bitDeleted=0 ) as AssociateCountFrom,          
(SELECT count(*)from CompanyAssociations where numAssociateFromDivisionID=@numDivisionID and bitDeleted=0 ) as AssociateCountTo,
 ISNULL(DM.numCurrencyID,0) AS numCurrencyID,ISNULL(DM.numDefaultPaymentMethod,0) AS numDefaultPaymentMethod,ISNULL(DM.numDefaultCreditCard,0) AS numDefaultCreditCard,ISNULL(DM.bitOnCreditHold,0) AS bitOnCreditHold,
 ISNULL(vcShippersAccountNo,'') AS [vcShippersAccountNo],
 ISNULL(intShippingCompany,0) AS [intShippingCompany],ISNULL(DM.bitEmailToCase,0) AS bitEmailToCase,
 ISNULL(numDefaultExpenseAccountID,0) AS [numDefaultExpenseAccountID],
 ISNULL(numDefaultShippingServiceID,0) AS [numDefaultShippingServiceID],
 ISNULL(numAccountClassID,0) As numAccountClassID,
 ISNULL(tintPriceLevel,0) AS tintPriceLevel,DM.vcPartnerCode as vcPartnerCode,
 bitSaveCreditCardInfo,ISNULL(bitShippingLabelRequired,0) bitShippingLabelRequired,
 ISNULL(DM.tintInbound850PickItem,0) AS tintInbound850PickItem,
 ISNULL(bitAllocateInventoryOnPickList,0) AS bitAllocateInventoryOnPickList,
 ISNULL(DM.bitAutoCheckCustomerPart,0) AS  bitAutoCheckCustomerPart
 FROM  CompanyInfo CMP                                                          
 join DivisionMaster DM    
	on DM.numCompanyID=CMP.numCompanyID
 LEFT JOIN dbo.AddressDetails AD1 ON AD1.numDomainID=DM.numDomainID 
   AND AD1.numRecordID=DM.numDivisionID AND AD1.tintAddressOf=2 AND AD1.tintAddressType=1 AND AD1.bitIsPrimary=1
   LEFT JOIN dbo.AddressDetails AD2 ON AD1.numDomainID=DM.numDomainID 
   AND AD2.numRecordID= DM.numDivisionID AND AD2.tintAddressOf=2 AND AD2.tintAddressType=2 AND AD2.bitIsPrimary=1
   left join Domain on Domain.numDomainId=DM.numDomainID
where                                            
   DM.numDivisionID=@numDivisionID   and DM.numDomainID=@numDomainID                                                                        
END
GO
