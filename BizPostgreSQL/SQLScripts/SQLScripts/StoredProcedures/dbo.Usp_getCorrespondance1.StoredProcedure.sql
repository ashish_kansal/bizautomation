/****** Object:  StoredProcedure [dbo].[Usp_getCorrespondance1]    Script Date: 07/26/2008 16:17:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getcorrespondance1')
DROP PROCEDURE usp_getcorrespondance1
GO
CREATE PROCEDURE [dbo].[Usp_getCorrespondance1]  
@FromDate as datetime,  
@toDate as datetime,  
@numContactId as numeric,  
@vcMessageFrom as varchar(200)='' ,
@tintSortOrder as numeric,
@SeachKeyword as varchar(100),
@CurrentPage as numeric,
@PageSize as numeric,
@TotRecs as numeric output,
@columnName as varchar(100),
@columnSortOrder as varchar(50)
 
as  

Create table #tempTable ( ID INT IDENTITY PRIMARY KEY,numEmailHstrId VARCHAR(15),[date] varchar(100),notes  varchar(1000),
[type] varchar(100),phone  varchar(200),assignedto  varchar(100),caseid  varchar(50),vcCasenumber  varchar(50)
,caseTimeid  varchar(100),caseExpid  varchar(100),tinttype  varchar(100))                                                 
declare @strSql as varchar(3000)   
set @strSql= '
(select HDR.numEmailHstrID,  
bintCreatedOn as [date],vcFromEmail +'' to '' +dbo.GetEmaillAdd(hdr.numEmailHstrID,1) +'' , ''+ vcsubject as notes,  
''Email'' as [type] ,  
'''' as phone,'''' as assignedto ,  
''0'' as caseid,     
null as vcCasenumber,         
''0''as caseTimeid,        
''0''as caseExpid ,  
hdr.tinttype   
from EmailHistory HDR            
  left join EmailHStrToBCCAndCC DTL            
  on DTL.numEmailHstrID=HDR.numEmailHstrID      '                       
  set  @strSql=@strSql +'where 
(hdr.tinttype=1 or hdr.tinttype=2) '
 set  @strSql=@strSql +' and (bintCreatedOn between '''+ convert(varchar(30),@FromDate)+''' and '''+ convert(varchar(30),@ToDate)+''')   
and                                      
 (vcFromEmail ='''+ convert(varchar(100),@vcMessageFrom)+''' or vcEmail = '''+ convert(varchar(100),@vcMessageFrom)+''')  
union 
 
select  c.numCommId,  

case when c.bitTask=4 then dtCreatedDate else dtEndtime end AS CreatedDate,                                           
convert(varchar(1000),textdetails) as notes,   
                                                                        
case when c.bitTask=1 then ''Communication'' when c.bitTask=3 then ''Task'' when c.bitTask=4 then ''Notes'' when c.bitTask=5 then ''Follow up Status'' end AS [Type],                                           
A1.vcFirstName +'' ''+  
A1.vcLastName +'' ''+                
case when A1.numPhone<>'''' then + A1.numPhone +case when A1.numPhoneExtension<>'''' then '' - '' + A1.numPhoneExtension else '''' end  else '''' end as [Phone],  
                                            
  A2.vcFirstName+                                                     
A2.vcLastName  as assignedto,   
c.caseid,     
(select top 1 vcCaseNumber from cases where cases.numcaseid= c.caseid )as vcCasenumber,         
isnull(caseTimeid,0)as caseTimeid,        
isnull(caseExpid,0)as caseExpid  ,  
1 as tinttype                                              
from  Communication C                      
JOIN AdditionalContactsInformation  A1                   
ON c.numContactId = A1.numContactId                                                     
left join AdditionalContactsInformation A2 on A2.numContactID=c.numAssign                                                    
left join listdetails on numActivity=numListID   
 where  C.numContactId= '+ convert(varchar(15),@numContactId    )+'
and dtStartTime  >= '''+ convert(varchar(30),@FromDate)+''' and dtStartTime  <= '''+ convert(varchar(30),@ToDate)  +'''
  
  )  
order by 2 desc      '
print(@strsql)
insert into #tempTable           
 exec( @strSql)   

declare @firstRec as integer                                              
declare @lastRec as integer                      
 set @firstRec= (@CurrentPage-1) * @PageSize                                              
 set @lastRec= (@CurrentPage*@PageSize+1)   

select * 
from #tempTable T                    
where ID > @firstRec and ID < @lastRec order by  numEmailHstrId  Desc                                                          
                                            
set @TotRecs=(select count(*) from #tempTable)
GO
