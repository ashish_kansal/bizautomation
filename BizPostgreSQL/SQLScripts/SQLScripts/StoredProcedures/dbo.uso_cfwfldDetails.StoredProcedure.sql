/****** Object:  StoredProcedure [dbo].[uso_cfwfldDetails]    Script Date: 07/26/2008 16:14:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='uso_cfwflddetails')
DROP PROCEDURE uso_cfwflddetails
GO
CREATE PROCEDURE [dbo].[uso_cfwfldDetails]  
@fldid as numeric(9)=null  
as  
SELECT  fld_type,
        fld_label,
        fld_tab_ord,
        grp_id,
        subgrp,
        vcURL,
		DTL.bitIsRequired,
		DTL.bitIsNumeric,
		DTL.bitIsAlphaNumeric,
		DTL.bitIsEmail,
		DTL.bitIsLengthValidation,
		DTL.intMaxLength,
		DTL.intMinLength,
		DTL.bitFieldMessage,
		DTL.vcFieldMessage
		,ISNULL(M.vcToolTip,'') AS vcToolTip
		,ISNULL(bitAutocomplete,0) bitAutocomplete
FROM    CFW_Fld_Master M
        LEFT JOIN dbo.CFW_Validation DTL ON M.Fld_id = DTL.numFieldId
WHERE   fld_id = @fldid
GO
