/*
-- Added Support for Bills in AP Aging summary,by chintan
*/

-- [dbo].[USP_GetAccountPayableAging] 1,null 6719
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getaccountpayableaging')
DROP PROCEDURE usp_getaccountpayableaging
GO
set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go

CREATE PROCEDURE [dbo].[USP_GetAccountPayableAging]
(
    @numDomainId AS NUMERIC(9) = 0,
    @numDivisionId AS NUMERIC(9) = NULL,
    @numAccountClass AS NUMERIC(9)=0,
	@dtFromDate AS DATETIME,
	@dtToDate AS DATETIME,
	@ClientTimeZoneOffset INT = 0
)
AS 
BEGIN 
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	DECLARE @numTempDomainId NUMERIC(18,0)
	DECLARE @numTempDivisionId NUMERIC(18,0)
	DECLARE @numTempAccountClass NUMERIC(18,0)
	DECLARE @dtTempFromDate AS DATETIME
	DECLARE @dtTempToDate AS DATETIME


	SET @numTempDomainId = @numDomainId
	SET @numTempDivisionId = @numDivisionId
	SET @numTempAccountClass = @numAccountClass
	SET @dtTempFromDate = @dtFromDate
	SET @dtTempToDate = @dtToDate

		IF @dtTempFromDate IS NULL
			SET @dtTempFromDate = '1990-01-01 00:00:00.000'
		IF @dtTempToDate IS NULL
			SET @dtTempToDate = GETUTCDATE()

        CREATE TABLE #TempAPRecord
            (
              [ID] [int] IDENTITY(1, 1)
                         NOT NULL,
              [numUserCntID] [numeric](18, 0) NOT NULL,
              [numOppId] [numeric](18, 0) NULL,
              [numDivisionId] [numeric](18, 0) NULL,
              [numOppBizDocsId] [numeric](18, 0) NULL,
              [DealAmount] [float] NULL,
              [numBizDocId] [numeric](18, 0) NULL,
              dtDueDate DATETIME NULL,
              [numCurrencyID] [numeric](18, 0) NULL,AmountPaid FLOAT NULL,monUnAppliedAmount DECIMAL(20,5) NULL 
            ) ;
  
  
        CREATE TABLE #TempAPRecord1
            (
              [numID] [numeric](18, 0) IDENTITY(1, 1)
                                       NOT NULL,
              [numUserCntID] [numeric](18, 0) NOT NULL,
              [numDivisionId] [numeric](18, 0) NULL,
              [tintCRMType] [tinyint] NULL,
              [vcCompanyName] [varchar](200) NULL,
              [vcCustPhone] [varchar](50) NULL,
              [numContactId] [numeric](18, 0) NULL,
              [vcEmail] [varchar](100) NULL,
              [numPhone] [varchar](50) NULL,
              [vcContactName] [varchar](100) NULL,
			  [numCurrentDays] [numeric](18, 2) NULL,
              [numThirtyDays] [numeric](18, 2) NULL,
              [numSixtyDays] [numeric](18, 2) NULL,
              [numNinetyDays] [numeric](18, 2) NULL,
              [numOverNinetyDays] [numeric](18, 2) NULL,
              [numThirtyDaysOverDue] [numeric](18, 2) NULL,
              [numSixtyDaysOverDue] [numeric](18, 2) NULL,
              [numNinetyDaysOverDue] [numeric](18, 2) NULL,
              [numOverNinetyDaysOverDue] [numeric](18, 2) NULL,
              [numCompanyID] [numeric](18, 2) NULL,
              [numDomainID] [numeric](18, 2) NULL,
			  [intCurrentDaysCount] [int] NULL,
              [intThirtyDaysCount] [int] NULL,
              [intThirtyDaysOverDueCount] [int] NULL,
              [intSixtyDaysCount] [int] NULL,
              [intSixtyDaysOverDueCount] [int] NULL,
              [intNinetyDaysCount] [int] NULL,
              [intOverNinetyDaysCount] [int] NULL,
              [intNinetyDaysOverDueCount] [int] NULL,
              [intOverNinetyDaysOverDueCount] [int] NULL,
			  [numCurrentDaysPaid] [numeric](18, 2) NULL,
              [numThirtyDaysPaid] [numeric](18, 2) NULL,
              [numSixtyDaysPaid] [numeric](18, 2) NULL,
              [numNinetyDaysPaid] [numeric](18, 2) NULL,
              [numOverNinetyDaysPaid] [numeric](18, 2) NULL,
              [numThirtyDaysOverDuePaid] [numeric](18, 2) NULL,
              [numSixtyDaysOverDuePaid] [numeric](18, 2) NULL,
              [numNinetyDaysOverDuePaid] [numeric](18, 2) NULL,
              [numOverNinetyDaysOverDuePaid] [numeric](18, 2) NULL
              ,monUnAppliedAmount DECIMAL(20,5) NULL,numTotal DECIMAL(20,5) NULL 
            ) ;
  
  
  
        DECLARE @AuthoritativePurchaseBizDocId AS INTEGER
        SELECT  @AuthoritativePurchaseBizDocId = isnull(numAuthoritativePurchase,
                                                        0)
        FROM    AuthoritativeBizDocs
        WHERE   numDomainId = @numTempDomainId ;

		DECLARE @TEMPBillPayments TABLE
		(
			numBillID NUMERIC(18,0)
			,numOppBizDocsID NUMERIC(18,0)
			,monAmount DECIMAL(20,5)
		)

		INSERT INTO @TEMPBillPayments
		(
			numBillID
			,numOppBizDocsID
			,monAmount
		)
		SELECT 
			BPD.numBillID
			,BPD.numOppBizDocsID
			,BPD.monAmount
		FROM
			BillPaymentHeader BPH
		INNER JOIN
			BillPaymentDetails BPD
		ON
			BPH.numBillPaymentID=BPD.numBillPaymentID
		WHERE
			BPH.numDomainID=@numTempDomainId
			AND CAST(BPH.dtPaymentDate AS DATE) <= @dtTempToDate        
     
        INSERT  INTO [#TempAPRecord]
                (
                  numUserCntID,
                  [numOppId],
                  [numDivisionId],
                  [numOppBizDocsId],
                  [DealAmount],
                  [numBizDocId],
                  dtDueDate,
                  [numCurrencyID],AmountPaid
                )
                SELECT  @numTempDomainId,
                        OB.numOppId,
                        Opp.numDivisionId,
                        OB.numOppBizDocsId,
                        ISNULL(OB.monDealAmount --dbo.GetDealAmount(OB.numOppID,getutcdate(),OB.numOppBizDocsId)
                               * ISNULL(Opp.fltExchangeRate, 1), 0)
                        - ISNULL(ISNULL(SUM(TBP.monAmount),0)
                                 * ISNULL(Opp.fltExchangeRate, 1), 0) DealAmount,
                        OB.numBizDocId,
                        DATEADD(DAY,
                                CASE WHEN Opp.bitBillingTerms = 1
                                     --THEN convert(int, ISNULL(dbo.fn_GetListItemName(isnull(Opp.intBillingDays, 0)), 0))
                                     THEN convert(int, ISNULL((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(Opp.intBillingDays,0)), 0))
                                     ELSE 0
                                END, dtFromDate) AS dtDueDate,
                        ISNULL(Opp.numCurrencyID, 0) numCurrencyID,
                        ISNULL(ISNULL(SUM(TBP.monAmount),0) * ISNULL(Opp.fltExchangeRate, 1), 0) AmountPaid
                FROM    OpportunityMaster Opp
                        JOIN OpportunityBizDocs OB ON OB.numOppId = Opp.numOppID
                        LEFT OUTER JOIN [Currency] C ON Opp.[numCurrencyID] = C.[numCurrencyID]
						LEFT JOIN @TEMPBillPayments TBP ON OB.numOppBizDocsId=TBP.numOppBizDocsID
                WHERE   tintOpptype = 2
                        AND tintoppStatus = 1
                        AND opp.numDomainID = @numTempDomainId
                        AND OB.bitAuthoritativeBizDocs = 1
                        AND ISNULL(OB.tintDeferred, 0) <> 1
                        AND numBizDocId = @AuthoritativePurchaseBizDocId
                        AND ( Opp.numDivisionId = @numTempDivisionId
                              OR @numTempDivisionId IS NULL
                            )
                        AND (Opp.numAccountClass=@numTempAccountClass OR @numTempAccountClass=0)
						AND OB.[dtCreatedDate] <= @dtTempToDate
                GROUP BY OB.numOppId,
                        Opp.numDivisionId,
                        OB.numOppBizDocsId,
                        Opp.fltExchangeRate,
                        OB.numBizDocId,
                        OB.dtCreatedDate,
                        Opp.bitBillingTerms,
                        Opp.intBillingDays,
                        OB.monDealAmount,
                        Opp.numCurrencyID,
                        OB.[dtFromDate]
                HAVING  ( ISNULL(OB.monDealAmount * ISNULL(Opp.fltExchangeRate,
                                                           1), 0)
                          - ISNULL(ISNULL(SUM(TBP.monAmount),0)
                                   * ISNULL(Opp.fltExchangeRate, 1), 0) ) > 0
                UNION ALL 
		--Add Bill Amount
                SELECT
					@numTempDomainId,
					0 numOppId,
					numDivisionId,
					0 numBizDocsId,
					ISNULL(SUM(monAmountDue), 0) - ISNULL(SUM(monAmountPaid),0) DealAmount,
					0 numBizDocId,
					dtDueDate AS dtDueDate,
					0 numCurrencyID
					,ISNULL(SUM(monAmountPaid),0) AS AmountPaid
				FROM
				(
					SELECT 
						BH.numBillID
						,BH.numDivisionId
						,BH.dtDueDate
						,BH.monAmountDue
						,ISNULL(SUM(TEMPPaid.monAmount),0) monAmountPaid
					FROM
						BillHeader BH
					LEFT JOIN
						@TEMPBillPayments TEMPPaid
					ON
						BH.numBillID = TEMPPaid.numBillID
					WHERE
						BH.numDomainID = @numTempDomainId
						AND (BH.numDivisionId = @numTempDivisionId OR @numTempDivisionId IS NULL)
						AND (BH.numAccountClass=@numTempAccountClass OR @numTempAccountClass=0)
						AND BH.[dtBillDate] <= @dtTempToDate
					GROUP BY
						BH.numBillID
						,BH.numDivisionId
						,BH.dtDueDate
						,BH.monAmountDue
					HAVING
						 ISNULL(BH.monAmountDue, 0) - ISNULL(SUM(TEMPPaid.monAmount),0) > 0  
				) TEMP
				GROUP BY 
					numDivisionId,
					dtDueDate
				HAVING
					ISNULL(SUM(monAmountDue), 0) - ISNULL(SUM(monAmountPaid),0) > 0 
                UNION ALL
		--Add Write Check entry (As Amount Paid)
                SELECT  @numTempDomainId,
                        0 numOppId,
                        CD.numCustomerId as numDivisionId,
                        0 numBizDocsId,
                        0 DealAmount,
                        0 numBizDocId,
                        CH.dtCheckDate AS dtDueDate,
                        0 numCurrencyID,ISNULL(SUM(CD.monAmount), 0) AS AmountPaid
                FROM    CheckHeader CH
						JOIN  dbo.CheckDetails CD ON CH.numCheckHeaderID=CD.numCheckHeaderID
                        INNER JOIN dbo.Chart_Of_Accounts COA ON COA.numAccountId = CD.numChartAcntId
                WHERE   CH.numDomainID = @numTempDomainId AND CH.tintReferenceType=1
						AND COA.vcAccountCode LIKE '01020102%'
                        AND ( CD.numCustomerId = @numTempDivisionId
                              OR @numTempDivisionId IS NULL
                            )
                        AND (CH.numAccountClass=@numTempAccountClass OR @numTempAccountClass=0)
						AND CH.[dtCheckDate] <= @dtTempToDate
                GROUP BY CD.numCustomerId,
                        CH.dtCheckDate
		--Show Impact of AP journal entries in report as well 
                UNION ALL
                SELECT  @numTempDomainId,
                        0,
                        GJD.numCustomerId,
                        0,
                        CASE WHEN GJD.numDebitAmt > 0 THEN -1 * GJD.numDebitAmt
                             ELSE GJD.numCreditAmt
                        END,
                        0,
                        GJH.datEntry_Date,
                        GJD.[numCurrencyID],0 AS AmountPaid
                FROM    dbo.General_Journal_Header GJH
                        INNER JOIN dbo.General_Journal_Details GJD ON GJH.numJournal_Id = GJD.numJournalId
                                                                      AND GJH.numDomainId = GJD.numDomainId
                        INNER JOIN dbo.Chart_Of_Accounts COA ON COA.numAccountId = GJD.numChartAcntId
                WHERE   GJH.numDomainId = @numTempDomainId 
                        AND COA.vcAccountCode LIKE '01020102%'
                        AND ISNULL(numOppId, 0) = 0
                        AND ISNULL(numOppBizDocsId, 0) = 0
                        AND ISNULL(GJH.numBillID,0)=0 AND ISNULL(GJH.numBillPaymentID,0)=0 AND ISNULL(GJH.numCheckHeaderID,0)=0 AND ISNULL(GJH.numReturnID,0)=0
                        AND ISNULL(GJH.numPayrollDetailID,0)=0
                         AND ( GJD.numCustomerId = @numTempDivisionId
                              OR @numTempDivisionId IS NULL
                            )
                            AND (GJH.numAccountClass=@numTempAccountClass OR @numTempAccountClass=0)
						AND [GJH].[datEntry_Date] <= @dtTempToDate
                UNION ALL
				--Add Commission Amount
                SELECT  @numTempDomainId,
                        OBD.numOppId numOppId,
                        D.numDivisionId,
                        BDC.numOppBizDocId numBizDocsId,
                       isnull(sum(BDC.numComissionAmount - ISNULL(PT.monCommissionAmt,0)),0)  DealAmount,
                        0 numBizDocId,
                        OBD.dtCreatedDate AS dtDueDate,
                        ISNULL(OPP.numCurrencyID, 0) numCurrencyID,0 AS AmountPaid
                FROM    BizDocComission BDC
                        JOIN OpportunityBizDocs OBD ON BDC.numOppBizDocId = OBD.numOppBizDocsId
                        join OpportunityMaster Opp on OPP.numOppId = OBD.numOppId
                        JOIN Domain D on D.numDomainID = BDC.numDomainID
                        LEFT JOIN dbo.PayrollTracking PT ON BDC.numComissionID=PT.numComissionID
                WHERE   OPP.numDomainID = @numTempDomainId
                        and BDC.numDomainID = @numTempDomainId
                        AND (D.numDivisionId = @numTempDivisionId OR @numTempDivisionId IS NULL)
                        AND (Opp.numAccountClass=@numTempAccountClass OR @numTempAccountClass=0)
						AND OBD.[dtCreatedDate] <= @dtTempToDate
						AND ISNULL(BDC.bitCommisionPaid,0)=0
                        AND OBD.bitAuthoritativeBizDocs = 1
                GROUP BY D.numDivisionId,
                        OBD.numOppId,
                        BDC.numOppBizDocId,
                        OPP.numCurrencyID,
                        BDC.numComissionAmount,
                        OBD.dtCreatedDate
                HAVING  ISNULL(BDC.numComissionAmount, 0) > 0
			UNION ALL --Standalone Refund against Account Receivable
			 SELECT @numTempDomainId,
					0 AS numOppID,
					RH.numDivisionId,
					0,
					CASE WHEN GJD.numDebitAmt > 0 THEN GJD.numDebitAmt * -1 ELSE GJD.numCreditAmt END DealAmount,
					0 numBizDocId,
					GJH.datEntry_Date,GJD.[numCurrencyID],0 AS AmountPaid
			 FROM   dbo.ReturnHeader RH JOIN dbo.General_Journal_Header GJH ON RH.numReturnHeaderID = GJH.numReturnID
			JOIN General_Journal_Details GJD ON GJH.numJournal_Id = GJD.numJournalId 
			INNER JOIN dbo.Chart_Of_Accounts COA ON COA.numAccountId = GJD.numChartAcntId
			WHERE RH.tintReturnType=4 AND RH.numDomainId=@numTempDomainId  AND COA.vcAccountCode LIKE '01020102%'
					AND (RH.numDivisionId = @numTempDivisionId OR @numTempDivisionId IS NULL)
					AND (monBizDocAmount > 0) AND ISNULL(RH.numBillPaymentIDRef,0)>0
					AND (RH.numAccountClass=@numTempAccountClass OR @numTempAccountClass=0)
					AND GJH.datEntry_Date <= @dtTempToDate
					--GROUP BY DM.numDivisionId,DM.dtDepositDate
		
		
---SELECT  SUM(monUnAppliedAmount) FROM    [#TempAPRecord]

   INSERT  INTO [#TempAPRecord]
        (
          numUserCntID,
          [numOppId],
          [numDivisionId],
          [numOppBizDocsId],
          [DealAmount],
          [numBizDocId],
          [numCurrencyID],
          [dtDueDate],AmountPaid,monUnAppliedAmount
        )
SELECT @numTempDomainId,0, BPH.numDivisionId,0,0,0,0,NULL,0,sum(ISNULL(monPaymentAmount,0) - ISNULL(monAppliedAmount,0))       
 FROM BillPaymentHeader BPH
		WHERE BPH.numDomainId=@numTempDomainId   
		AND (BPH.numDivisionId = @numTempDivisionId
                            OR @numTempDivisionId IS NULL)
		AND (ISNULL(monPaymentAmount,0) - ISNULL(monAppliedAmount,0)) > 0
		AND (BPH.numAccountClass=@numTempAccountClass OR @numTempAccountClass=0)
		AND [BPH].[dtPaymentDate] <= @dtTempToDate
		GROUP BY BPH.numDivisionId
		

--SELECT  GETDATE()
        INSERT  INTO [#TempAPRecord1]
                (
                  [numUserCntID],
                  [numDivisionId],
                  [tintCRMType],
                  [vcCompanyName],
                  [vcCustPhone],
                  [numContactId],
                  [vcEmail],
                  [numPhone],
                  [vcContactName],
				  [numCurrentDays],
                  [numThirtyDays],
                  [numSixtyDays],
                  [numNinetyDays],
                  [numOverNinetyDays],
                  [numThirtyDaysOverDue],
                  [numSixtyDaysOverDue],
                  [numNinetyDaysOverDue],
                  [numOverNinetyDaysOverDue],
                  numCompanyID,
                  numDomainID,
				  [intCurrentDaysCount],
                  [intThirtyDaysCount],
                  [intThirtyDaysOverDueCount],
                  [intSixtyDaysCount],
                  [intSixtyDaysOverDueCount],
                  [intNinetyDaysCount],
                  [intOverNinetyDaysCount],
                  [intNinetyDaysOverDueCount],
                  [intOverNinetyDaysOverDueCount],
				  [numCurrentDaysPaid],
                  [numThirtyDaysPaid],
                  [numSixtyDaysPaid],
                  [numNinetyDaysPaid],
                  [numOverNinetyDaysPaid],
                  [numThirtyDaysOverDuePaid],
                  [numSixtyDaysOverDuePaid],
                  [numNinetyDaysOverDuePaid],
                  [numOverNinetyDaysOverDuePaid],monUnAppliedAmount
                )
                SELECT DISTINCT
                        @numTempDomainId,
                        Div.numDivisionID,
                        tintCRMType AS tintCRMType,
                        vcCompanyName AS vcCompanyName,
                        '',
                        NULL,
                        '',
                        '',
                        '',
						0 numCurrentDays,
                        0 numThirtyDays,
                        0 numSixtyDays,
                        0 numNinetyDays,
                        0 numOverNinetyDays,
                        0 numThirtyDaysOverDue,
                        0 numSixtyDaysOverDue,
                        0 numNinetyDaysOverDue,
                        0 numOverNinetyDaysOverDue,
                        C.numCompanyID,
                        Div.numDomainID,
						0 [intCurrentDaysCount],
                        0 [intThirtyDaysCount],
                        0 [intThirtyDaysOverDueCount],
                        0 [intSixtyDaysCount],
                        0 [intSixtyDaysOverDueCount],
                        0 [intNinetyDaysCount],
                        0 [intOverNinetyDaysCount],
                        0 [intNinetyDaysOverDueCount],
                        0 [intOverNinetyDaysOverDueCount],
						0 numCurrentDaysPaid,
                         0 numThirtyDaysPaid,
                        0 numSixtyDaysPaid,
                        0 numNinetyDaysPaid,
                        0 numOverNinetyDaysPaid,
                        0 numThirtyDaysOverDuePaid,
                        0 numSixtyDaysOverDuePaid,
                        0 numNinetyDaysOverDuePaid,
                        0 numOverNinetyDaysOverDuePaid,0 monUnAppliedAmount
                FROM    Divisionmaster Div
                        INNER JOIN CompanyInfo C ON C.numCompanyId = Div.numCompanyID
                    --JOIN [#TempAPRecord] t ON Div.[numDivisionID] = t.[numDivisionId]
                WHERE   Div.[numDomainID] = @numTempDomainId
                        AND Div.[numDivisionID] IN ( SELECT DISTINCT
                                                            [numDivisionID]
                                                     FROM   [#TempAPRecord] )


--Update Custome Phone 
        UPDATE  #TempAPRecord1
        SET     [vcCustPhone] = X.[vcCustPhone]
        FROM    ( SELECT    CASE WHEN numPhone IS NULL THEN ''
                                 WHEN numPhone = '' THEN ''
                                 ELSE ', ' + numPhone
                            END AS vcCustPhone,
                            numDivisionId
                  FROM      AdditionalContactsInformation
                  WHERE     bitPrimaryContact = 1
                            AND numDivisionId IN (
                            SELECT DISTINCT
                                    numDivisionID
                            FROM    [#TempAPRecord1]
                            WHERE   numUserCntID = @numTempDomainId )
                ) X
        WHERE   #TempAPRecord1.numDivisionID = X.numDivisionID

--Update Customer info 
        UPDATE  #TempAPRecord1
        SET     [numContactId] = X.[numContactID],
                [vcEmail] = X.[vcEmail],
                [vcContactName] = X.[vcContactName],
                numPhone = X.numPhone
        FROM    ( SELECT  numContactId AS numContactID,
                            ISNULL(vcEmail, '') AS vcEmail,
                            ISNULL(vcFirstname, '') + ' ' + ISNULL(vcLastName, '') AS vcContactName,
                            CAST(ISNULL(CASE WHEN numPhone IS NULL THEN ''
                                             WHEN numPhone = '' THEN ''
                                             ELSE ', ' + numPhone
                                        END, '') AS VARCHAR) AS numPhone,
                            [numDivisionId]
                  FROM      AdditionalContactsInformation
                  WHERE     (vcPosition = 843 or isnull(bitPrimaryContact,0)=1)
                            AND numDivisionId IN (
                            SELECT DISTINCT
                                    numDivisionID
                            FROM    [#TempAPRecord1]
                            WHERE   numUserCntID = @numTempDomainId )
                ) X
        WHERE   #TempAPRecord1.numDivisionID = X.numDivisionID

                    
--SELECT  GETDATE()
------------------------------------------      
        DECLARE @baseCurrency AS NUMERIC
        SELECT  @baseCurrency = numCurrencyID
        FROM    dbo.Domain
        WHERE   numDomainId = @numTempDomainId
/*when any Order is placed in Foreign currency (other than base currency)then show it in purple color */

------------------------------------------

-----------------------------------
        UPDATE  #TempAPRecord1
        SET     numCurrentDays = X.numCurrentDays
        FROM    ( SELECT    SUM(DealAmount) AS numCurrentDays,
                            [numDivisionId]
                  FROM      [#TempAPRecord]
                  WHERE     numUserCntID = @numTempDomainId AND
                            DATEDIFF(DAY,dbo.GetUTCDateWithoutTime(),[dtDueDate]) >= 0

                  GROUP BY  numDivisionId
                ) X
        WHERE   #TempAPRecord1.numDivisionID = X.numDivisionID
        
         UPDATE  #TempAPRecord1
        SET     numCurrentDaysPaid = X.numCurrentDaysPaid
        FROM    ( SELECT    SUM(AmountPaid) AS numCurrentDaysPaid,
                            [numDivisionId]
                  FROM      [#TempAPRecord]
                  WHERE     numUserCntID = @numTempDomainId
                            AND DATEDIFF(DAY,dbo.GetUTCDateWithoutTime(),[dtDueDate]) >= 0
                  GROUP BY  numDivisionId
                ) X
        WHERE   #TempAPRecord1.numDivisionID = X.numDivisionID
        
        UPDATE  #TempAPRecord1
        SET     intCurrentDaysCount = 2
        WHERE   numDivisionId IN (
                SELECT DISTINCT
                        [numDivisionId]
                FROM    [#TempAPRecord]
                WHERE   numUserCntID = @numTempDomainId
                        AND DATEDIFF(DAY,dbo.GetUTCDateWithoutTime(),[dtDueDate]) >= 0
                        AND numCurrencyID <> @baseCurrency )
------------------------------------------

        UPDATE  #TempAPRecord1
        SET     numThirtyDays = X.numThirtyDays
        FROM    ( SELECT    SUM(DealAmount) AS numThirtyDays,
                            [numDivisionId]
                  FROM      [#TempAPRecord]
                  WHERE     numUserCntID = @numTempDomainId
                            AND DATEDIFF(DAY, dbo.GetUTCDateWithoutTime(),
                                         [dtDueDate]) BETWEEN 1
                                                      AND     30
                  GROUP BY  numDivisionId
                ) X
        WHERE   #TempAPRecord1.numDivisionID = X.numDivisionID
        
         UPDATE  #TempAPRecord1
        SET     numThirtyDaysPaid = X.numThirtyDaysPaid
        FROM    ( SELECT    SUM(AmountPaid) AS numThirtyDaysPaid,
                            [numDivisionId]
                  FROM      [#TempAPRecord]
                  WHERE     numUserCntID = @numTempDomainId
                            AND DATEDIFF(DAY, dbo.GetUTCDateWithoutTime(),
                                         [dtDueDate]) BETWEEN 1
                                                      AND     30
                  GROUP BY  numDivisionId
                ) X
        WHERE   #TempAPRecord1.numDivisionID = X.numDivisionID
        
        UPDATE  #TempAPRecord1
        SET     intThirtyDaysCount = 2
        WHERE   numDivisionId IN (
                SELECT DISTINCT
                        [numDivisionId]
                FROM    [#TempAPRecord]
                WHERE   numUserCntID = @numTempDomainId
                        AND DATEDIFF(DAY, dbo.GetUTCDateWithoutTime(),
                                     [dtDueDate]) BETWEEN 1
                                                  AND     30
                        AND numCurrencyID <> @baseCurrency )
------------------------------------------
        UPDATE  #TempAPRecord1
        SET     numSixtyDays = X.numSixtyDays
        FROM    ( SELECT    SUM(DealAmount) AS numSixtyDays,
                            numDivisionId
                  FROM      [#TempAPRecord]
                  WHERE     numUserCntID = @numTempDomainId
                            AND DATEDIFF(DAY, dbo.GetUTCDateWithoutTime(),
                                         [dtDueDate]) BETWEEN 31
                                                      AND     60
                            AND dbo.GetUTCDateWithoutTime() <= [dtDueDate]
                  GROUP BY  numDivisionId
                ) X
        WHERE   #TempAPRecord1.numDivisionID = X.numDivisionID
        
        UPDATE  #TempAPRecord1
        SET     numSixtyDaysPaid = X.numSixtyDaysPaid
        FROM    ( SELECT    SUM(AmountPaid) AS numSixtyDaysPaid,
                            numDivisionId
                  FROM      [#TempAPRecord]
                  WHERE     numUserCntID = @numTempDomainId
                            AND DATEDIFF(DAY, dbo.GetUTCDateWithoutTime(),
                                         [dtDueDate]) BETWEEN 31
                                                      AND     60
                            AND dbo.GetUTCDateWithoutTime() <= [dtDueDate]
                  GROUP BY  numDivisionId
                ) X
        WHERE   #TempAPRecord1.numDivisionID = X.numDivisionID
        
        UPDATE  #TempAPRecord1
        SET     intSixtyDaysCount = 2
        WHERE   numDivisionId IN (
                SELECT DISTINCT
                        [numDivisionId]
                FROM    [#TempAPRecord]
                WHERE   numUserCntID = @numTempDomainId
                        AND DATEDIFF(DAY, dbo.GetUTCDateWithoutTime(),
                                     [dtDueDate]) BETWEEN 31
                                                  AND     60
                        AND dbo.GetUTCDateWithoutTime() <= [dtDueDate]
                        AND numCurrencyID <> @baseCurrency )

------------------------------------------
        UPDATE  #TempAPRecord1
        SET     numNinetyDays = X.numNinetyDays
        FROM    ( SELECT    SUM(DealAmount) AS numNinetyDays,
                            numDivisionId
                  FROM      #TempAPRecord
                  WHERE     numUserCntID = @numTempDomainId
                            AND DATEDIFF(DAY, dbo.GetUTCDateWithoutTime(),
                                         [dtDueDate]) BETWEEN 61
                                                      AND     90
                            AND dbo.GetUTCDateWithoutTime() <= [dtDueDate]
                  GROUP BY  numDivisionId
                ) X
        WHERE   #TempAPRecord1.numDivisionID = X.numDivisionID
        
         UPDATE  #TempAPRecord1
        SET     numNinetyDaysPaid = X.numNinetyDaysPaid
        FROM    ( SELECT    SUM(AmountPaid) AS numNinetyDaysPaid,
                            numDivisionId
                  FROM      #TempAPRecord
                  WHERE     numUserCntID = @numTempDomainId
                            AND DATEDIFF(DAY, dbo.GetUTCDateWithoutTime(),
                                         [dtDueDate]) BETWEEN 61
                                                      AND     90
                            AND dbo.GetUTCDateWithoutTime() <= [dtDueDate]
                  GROUP BY  numDivisionId
                ) X
        WHERE   #TempAPRecord1.numDivisionID = X.numDivisionID
        
        UPDATE  #TempAPRecord1
        SET     intNinetyDaysCount = 2
        WHERE   numDivisionId IN (
                SELECT DISTINCT
                        [numDivisionId]
                FROM    [#TempAPRecord]
                WHERE   numUserCntID = @numTempDomainId
                        AND DATEDIFF(DAY, dbo.GetUTCDateWithoutTime(),
                                     [dtDueDate]) BETWEEN 61
                                                  AND     90
                        AND dbo.GetUTCDateWithoutTime() <= [dtDueDate]
                        AND numCurrencyID <> @baseCurrency )

------------------------------------------
        UPDATE  #TempAPRecord1
        SET     numOverNinetyDays = X.numOverNinetyDays
        FROM    ( SELECT    SUM(DealAmount) AS numOverNinetyDays,
                            numDivisionId
                  FROM      #TempAPRecord
                  WHERE     numUserCntID = @numTempDomainId
                            AND [dtDueDate] >= DATEADD(DAY, 91,
                                                       dbo.GetUTCDateWithoutTime())
                            AND dbo.GetUTCDateWithoutTime() <= [dtDueDate]
                  GROUP BY  numDivisionId
                ) X
        WHERE   #TempAPRecord1.numDivisionID = X.numDivisionID
        
        UPDATE  #TempAPRecord1
        SET     numOverNinetyDaysPaid = X.numOverNinetyDaysPaid
        FROM    ( SELECT    SUM(AmountPaid) AS numOverNinetyDaysPaid,
                            numDivisionId
                  FROM      #TempAPRecord
                  WHERE     numUserCntID = @numTempDomainId
                            AND [dtDueDate] >= DATEADD(DAY, 91,
                                                       dbo.GetUTCDateWithoutTime())
                            AND dbo.GetUTCDateWithoutTime() <= [dtDueDate]
                  GROUP BY  numDivisionId
                ) X
        WHERE   #TempAPRecord1.numDivisionID = X.numDivisionID
        
        UPDATE  #TempAPRecord1
        SET     intOverNinetyDaysCount = 2
        WHERE   numDivisionId IN (
                SELECT DISTINCT
                        [numDivisionId]
                FROM    [#TempAPRecord]
                WHERE   numUserCntID = @numTempDomainId
                        AND [dtDueDate] >= DATEADD(DAY, 91,
                                                   dbo.GetUTCDateWithoutTime())
                        AND dbo.GetUTCDateWithoutTime() <= [dtDueDate]
                        AND numCurrencyID <> @baseCurrency )

------------------------------------------
        UPDATE  #TempAPRecord1
        SET     numThirtyDaysOverDue = X.numThirtyDaysOverDue
        FROM    ( SELECT    SUM(DealAmount) AS numThirtyDaysOverDue,
                            numDivisionId
                  FROM      #TempAPRecord
                  WHERE     numUserCntID = @numTempDomainId
                            AND dbo.GetUTCDateWithoutTime() > [dtDueDate]
                            AND DATEDIFF(DAY, [dtDueDate],
                                         dbo.GetUTCDateWithoutTime()) BETWEEN 1
                                                                      AND     30
                  GROUP BY  numDivisionId
                ) X
        WHERE   #TempAPRecord1.numDivisionID = X.numDivisionID
        
         UPDATE  #TempAPRecord1
        SET     numThirtyDaysOverDuePaid = X.numThirtyDaysOverDuePaid
        FROM    ( SELECT    SUM(AmountPaid) AS numThirtyDaysOverDuePaid,
                            numDivisionId
                  FROM      #TempAPRecord
                  WHERE     numUserCntID = @numTempDomainId
                            AND dbo.GetUTCDateWithoutTime() > [dtDueDate]
                            AND DATEDIFF(DAY, [dtDueDate],
                                         dbo.GetUTCDateWithoutTime()) BETWEEN 1
                                                                      AND     30
                  GROUP BY  numDivisionId
                ) X
        WHERE   #TempAPRecord1.numDivisionID = X.numDivisionID
        
        UPDATE  #TempAPRecord1
        SET     intThirtyDaysOverDueCount = 2
        WHERE   numDivisionId IN (
                SELECT DISTINCT
                        [numDivisionId]
                FROM    [#TempAPRecord]
                WHERE   numUserCntID = @numTempDomainId
                        AND dbo.GetUTCDateWithoutTime() > [dtDueDate]
                        AND DATEDIFF(DAY, [dtDueDate],
                                     dbo.GetUTCDateWithoutTime()) BETWEEN 1
                                                                  AND     30
                        AND numCurrencyID <> @baseCurrency )

----------------------------------
        UPDATE  #TempAPRecord1
        SET     numSixtyDaysOverDue = X.numSixtyDaysOverDue
        FROM    ( SELECT    SUM(DealAmount) AS numSixtyDaysOverDue,
                            numDivisionId
                  FROM      #TempAPRecord
                  WHERE     numUserCntID = @numTempDomainId
                            AND dbo.GetUTCDateWithoutTime() > [dtDueDate]
                            AND DATEDIFF(DAY, [dtDueDate],
                                         dbo.GetUTCDateWithoutTime()) BETWEEN 31
                                                                      AND     60
                  GROUP BY  numDivisionId
                ) X
        WHERE   #TempAPRecord1.numDivisionID = X.numDivisionID
        
          UPDATE  #TempAPRecord1
        SET     numSixtyDaysOverDuePaid = X.numSixtyDaysOverDuePaid
        FROM    ( SELECT    SUM(AmountPaid) AS numSixtyDaysOverDuePaid,
                            numDivisionId
                  FROM      #TempAPRecord
                  WHERE     numUserCntID = @numTempDomainId
                            AND dbo.GetUTCDateWithoutTime() > [dtDueDate]
                            AND DATEDIFF(DAY, [dtDueDate],
                                         dbo.GetUTCDateWithoutTime()) BETWEEN 31
                                                                      AND     60
                  GROUP BY  numDivisionId
                ) X
        WHERE   #TempAPRecord1.numDivisionID = X.numDivisionID
        
        UPDATE  #TempAPRecord1
        SET     intSixtyDaysOverDueCount = 2
        WHERE   numDivisionId IN (
                SELECT DISTINCT
                        [numDivisionId]
                FROM    [#TempAPRecord]
                WHERE   numUserCntID = @numTempDomainId
                        AND dbo.GetUTCDateWithoutTime() > [dtDueDate]
                        AND DATEDIFF(DAY, [dtDueDate],
                                     dbo.GetUTCDateWithoutTime()) BETWEEN 31
                                                                  AND     60
                        AND numCurrencyID <> @baseCurrency )
------------------------------------------
        UPDATE  #TempAPRecord1
        SET     numNinetyDaysOverDue = X.numNinetyDaysOverDue
        FROM    ( SELECT    SUM(DealAmount) AS numNinetyDaysOverDue,
                            numDivisionId
                  FROM      #TempAPRecord
                  WHERE     numUserCntID = @numTempDomainId
                            AND dbo.GetUTCDateWithoutTime() > [dtDueDate]
                            AND DATEDIFF(DAY, [dtDueDate],
                                         dbo.GetUTCDateWithoutTime()) BETWEEN 61
                                                                      AND     90
                  GROUP BY  numDivisionId
                ) X
        WHERE   #TempAPRecord1.numDivisionID = X.numDivisionID
        
         UPDATE  #TempAPRecord1
        SET     numNinetyDaysOverDuePaid = X.numNinetyDaysOverDuePaid
        FROM    ( SELECT    SUM(AmountPaid) AS numNinetyDaysOverDuePaid,
                            numDivisionId
                  FROM      #TempAPRecord
                  WHERE     numUserCntID = @numTempDomainId
                            AND dbo.GetUTCDateWithoutTime() > [dtDueDate]
                            AND DATEDIFF(DAY, [dtDueDate],
                                         dbo.GetUTCDateWithoutTime()) BETWEEN 61
                                                                      AND     90
                  GROUP BY  numDivisionId
                ) X
        WHERE   #TempAPRecord1.numDivisionID = X.numDivisionID
        
        UPDATE  #TempAPRecord1
        SET     intNinetyDaysOverDueCount = 2
        WHERE   numDivisionId IN (
                SELECT DISTINCT
                        [numDivisionId]
                FROM    [#TempAPRecord]
                WHERE   numUserCntID = @numTempDomainId
                        AND dbo.GetUTCDateWithoutTime() > [dtDueDate]
                        AND DATEDIFF(DAY, [dtDueDate],
                                     dbo.GetUTCDateWithoutTime()) BETWEEN 61
                                                                  AND     90
                        AND numCurrencyID <> @baseCurrency )
------------------------------------------
        UPDATE  #TempAPRecord1
        SET     numOverNinetyDaysOverDue = X.numOverNinetyDaysOverDue
        FROM    ( SELECT    SUM(DealAmount) AS numOverNinetyDaysOverDue,
                            numDivisionId
                  FROM      #TempAPRecord
                  WHERE     numUserCntID = @numTempDomainId
                            AND dbo.GetUTCDateWithoutTime() > [dtDueDate]
                            AND DATEDIFF(DAY, [dtDueDate],
                                         dbo.GetUTCDateWithoutTime()) > 90
                  GROUP BY  numDivisionId
                ) X
        WHERE   #TempAPRecord1.numDivisionID = X.numDivisionID
        
           UPDATE  #TempAPRecord1
        SET     numOverNinetyDaysOverDuePaid = X.numOverNinetyDaysOverDuePaid
        FROM    ( SELECT    SUM(AmountPaid) AS numOverNinetyDaysOverDuePaid,
                            numDivisionId
                  FROM      #TempAPRecord
                  WHERE     numUserCntID = @numTempDomainId
                            AND dbo.GetUTCDateWithoutTime() > [dtDueDate]
                            AND DATEDIFF(DAY, [dtDueDate],
                                         dbo.GetUTCDateWithoutTime()) > 90
                  GROUP BY  numDivisionId
                ) X
        WHERE   #TempAPRecord1.numDivisionID = X.numDivisionID
        
        UPDATE  #TempAPRecord1
        SET     intOverNinetyDaysOverDueCount = 2
        WHERE   numDivisionId IN (
                SELECT DISTINCT
                        [numDivisionId]
                FROM    [#TempAPRecord]
                WHERE   numUserCntID = @numTempDomainId
                        AND dbo.GetUTCDateWithoutTime() > [dtDueDate]
                        AND DATEDIFF(DAY, [dtDueDate],
                                     dbo.GetUTCDateWithoutTime()) > 90
                        AND numCurrencyID <> @baseCurrency )
             


UPDATE  
	TR1
SET
	monUnAppliedAmount = ISNULL((SELECT SUM(ISNULL(monUnAppliedAmount,0)) FROM  #TempAPRecord TR2 WHERE TR2.numDivisionId=TR1.numDivisionId),0)
FROM
	#TempAPRecord1 TR1

UPDATE  #TempAPRecord1 SET numTotal=  isnull(numCurrentDays, 0) + --isnull(numThirtyDays, 0) + 
				isnull(numThirtyDaysOverDue, 0)
                --+ isnull(numSixtyDays, 0) 
				+ isnull(numSixtyDaysOverDue, 0)
                --+ isnull(numNinetyDays, 0) 
				+ isnull(numNinetyDaysOverDue, 0)
                --+ isnull(numOverNinetyDays, 0)
                + isnull(numOverNinetyDaysOverDue, 0) 			
             
        SELECT  [numDivisionId],
                [numContactId],
                [tintCRMType],
                [vcCompanyName],
                [vcCustPhone],
                [vcContactName] as vcApName,
                [vcEmail],
                [numPhone] as vcPhone,
				[numCurrentDays],
                [numThirtyDays],
                [numSixtyDays],
                [numNinetyDays],
                [numOverNinetyDays],
                [numThirtyDaysOverDue],
                [numSixtyDaysOverDue],
                [numNinetyDaysOverDue],
                [numOverNinetyDaysOverDue],
             ISNULL(numTotal,0) - ISNULL(monUnAppliedAmount,0) AS numTotal,
                numCompanyID,
                numDomainID,
				[intCurrentDaysCount],
                [intThirtyDaysCount],
                [intThirtyDaysOverDueCount],
                [intSixtyDaysCount],
                [intSixtyDaysOverDueCount],
                [intNinetyDaysCount],
                [intOverNinetyDaysCount],
                [intNinetyDaysOverDueCount],
                [intOverNinetyDaysOverDueCount],
				[numCurrentDaysPaid],
                [numThirtyDaysPaid],
                [numSixtyDaysPaid],
                [numNinetyDaysPaid],
                [numOverNinetyDaysPaid],
                [numThirtyDaysOverDuePaid],
                [numSixtyDaysOverDuePaid],
                [numNinetyDaysOverDuePaid],
                [numOverNinetyDaysOverDuePaid],ISNULL(monUnAppliedAmount,0) AS monUnAppliedAmount
        FROM    #TempAPRecord1
        WHERE   numUserCntID = @numTempDomainId AND (numDivisionId = @numTempDivisionId OR @numTempDivisionId IS NULL)
        ORDER BY [vcCompanyName]

        drop table #TempAPRecord1
        drop table #TempAPRecord 

END
GO