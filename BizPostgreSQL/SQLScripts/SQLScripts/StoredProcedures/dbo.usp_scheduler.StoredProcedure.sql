
GO
/****** Object:  StoredProcedure [dbo].[usp_scheduler]    Script Date: 06/04/2009 15:22:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_scheduler')
DROP PROCEDURE usp_scheduler
GO
CREATE PROCEDURE [dbo].[usp_scheduler]              
as                      
                        
--Variable Declaration for Recurring Table        
Declare @numMaxScheduleId as numeric(9)                        
Declare @numMinScheduleId as numeric(9)     
Declare @numScheduleId as numeric(9)                          
Declare @varRecurringTemplateName as varchar(50)                          
Declare @chrIntervalType as char(1)                          
Declare @tintIntervalDays as tinyint                          
Declare @tintMonthlyType as tinyint                          
Declare @tintFirstDet as tinyint                          
Declare @tintWeekDays as tinyint                          
Declare @tintMonths as tinyint                          
Declare @dtStartDate as datetime                          
Declare @dtEndDate as datetime      
Declare @numNoTransactionsRecurring as numeric(9)       
Declare @numNoTransactions as numeric(9)      
Declare @bitEndType as bit         
Declare @bitEndTransactionType as bit                       
Declare @DNextDate as datetime                          
Declare @DLastDate as datetime                          
Declare @DateDiff as integer                          
Declare @CurrentDate as datetime                          
Declare @LastRecurringDate as datetime                       
declare @ReportId as integer    
declare @ContactName as varchar(50)    
declare @ContactEmail as varchar(200)    
declare @ReportName as varchar(200)    
DECLARE @numDomainID AS NUMERIC(9)    
                         
Declare @Day as tinyint                      
Declare @Month as  tinyint                      
Declare @Year as integer                      
--Declare @Date as varchar(100)                   
Declare @MonthNextDate as tinyint      
    
    
--declare #ScheduleTempTable TABLE (    
--ScheduleID numeric (5) NOT NULL ,    
--ReportId numeric (5) NOT NULL     
--)    
create table #ScheduleTempTable (  ID INT IDENTITY PRIMARY KEY,   
	                                         
    ScheduleID numeric (5) NOT NULL ,    
 ReportId numeric (5) NOT NULL ,    
 ContactName  varchar(50),    
 ContactEmail  varchar(200),    
 ReportName  varchar(50),
 ToEmail varchar(1000),
 DomainID numeric(9)
 )    
                       
                         
                          
Select @numMaxScheduleId=max(numscheduleId),@numMinScheduleId = min(numscheduleId) From [CustRptScheduler]     
  where  DAY([dtStartDate]) <= DAY(getutcdate())     
  --AND DAY([dtEndDate]) >= DAY(getutcdate())    
  and (bitrecurring is null or      bitrecurring =0)                
    
set @numScheduleId = @numMinScheduleId                     
print '@numMaxScheduleId  ' + convert(varchar(10),@numMaxScheduleId)    
print '@numMinScheduleId  ' + convert(varchar(10),@numMinScheduleId)    
    
While @numScheduleId > 0                           
Begin      
   Print '====================================================================='                             
   Print '====================================================================='                             
   Print '@numScheduleId '+convert(varchar(10),@numScheduleId)                                   
       
    If @numScheduleId <> 0                          
      Begin                          
     Select @ReportName=vcReportName,@ReportId=numReportId,@chrIntervalType=chrIntervalType,@tintIntervalDays=tintIntervalDays,                          
     @tintMonthlyType=tintMonthlyType,@tintFirstDet=tintFirstDet,@tintWeekDays=tintWeekDays,@tintMonths=tintMonths,@dtStartDate=dtStartDate,                          
     @dtEndDate=dtEndDate,@bitEndType=bitEndType,@bitEndTransactionType=bitEndTransactionType    
   ,@numNoTransactionsRecurring=isnull(numNoTransaction,0)     
     ,@LastRecurringDate=isnull(LastRecurringDate,'Jan  1 1753 12:00AM')     
   ,@numNoTransactions = isnull(numNoTransactionCmp,0) ,    
   @ContactName =dbo.fn_GetContactName(numCreatedby)  ,    
   @ContactEmail=dbo.fn_GetContactEmail(1,0,numCreatedby),
   @numDomainID = CustRptScheduler.numDomainId
   From [CustRptScheduler]     
    join CustomReport on [CustRptScheduler].numReportId = CustomReport.numCustomReportId    
   Where numScheduleId=@numScheduleId    
    
        Print '@chrIntervalType  '+ convert(varchar(10), @chrIntervalType)                
     Print '@dtStartDate  '+ convert(varchar(30),@dtStartDate)      
        
    If @chrIntervalType='D'                           
                   Begin -- 'Begin for Daily                          
                       If @LastRecurringDate='Jan  1 1753 12:00AM'                       
                          Begin                      
                            Set @DNextDate=@dtStartDate        
       print '@DNextDate=='+convert(varchar(100),@DNextDate)                 
       If @DNextDate < getutcdate()               
        Set @DNextDate=getutcdate()                   
       End                      
                       Else                      
        Begin                      
       Set @DNextDate=DATEADD(Day, convert(numeric,@tintIntervalDays),  Convert(datetime, @LastRecurringDate))                        
        End                      
                           
        print @DNextDate                      
        Set @DateDiff = datediff (day,@DNextDate,getutcdate())                          
                       Print @DateDiff                          
                       Print @bitEndType                      
                       Print @dtEndDate                         
                       Declare @EndDateDiff as integer                      
     
                       Set @EndDateDiff=datediff (day,@dtEndDate,getutcdate())                         
                       Print @EndDateDiff          
       If @DateDiff=0 And (@bitEndType=1 Or((@bitEndType=0 and @EndDateDiff<=0) Or (@bitEndTransactionType=1 And @numNoTransactions<@numNoTransactionsRecurring)))                          
                          Begin                         
       Set @CurrentDate=getutcdate()    
        print 'in'    
        print @numScheduleId    
        print @ReportId    

       insert into #ScheduleTempTable (ScheduleId,ReportId,ContactName,ContactEmail,ReportName,ToEmail,DomainID)  
			 values (@numScheduleId,@ReportId,@ContactName,@ContactEmail,@ReportName,dbo.GetScheduledEmails(@numScheduleId),@numDomainID)
    --    Update CheckDetails set LastRecurringDate=getutcdate()  Where numCheckId=@numCheckId        
                          End     
             End -- 'End for Daily     
    
    
   If @chrIntervalType='M'                      
                     Begin                      
                        Print @chrIntervalType                      
                        Print 'Monthly Type'+ Convert(varchar(3),@tintMonthlyType)                      
                        Print 'Last RecurringDate'+ Convert(varchar(20),@LastRecurringDate)                      
                        If @tintMonthlyType=0                      
                          Begin -- tinymonthly Begin                      
                             If @LastRecurringDate='Jan  1 1753 12:00AM'                       
                                Begin                     
                                    Set @DNextDate=@dtStartDate                  
                                    If @DNextDate < getutcdate()               
                                      Set @DNextDate=getutcdate()                    
                                          
                                    Set @Day = DATEPART(Day, @DNextDate)                      
                                    Set @Month = DATEPART(Month, @DNextDate)                      
                                    Set @Year= DATEPART(Year, @DNextDate)                      
                                    Print @Day                      
                                    Print @Month                      
         Print @Year                       
                                    Set @DNextDate=Dateadd(month,((@Year-1900)*12)+@Month-1,@tintFirstDet-1)                      
                                    Print @DNextDate                      
       
                              End                        
                              Else                      
                              Begin                      
                                  Set @DNextDate=DATEADD(Month, convert(numeric,@tintIntervalDays),  Convert(datetime, @LastRecurringDate))                       
        End                       
          Set @DateDiff = datediff (day,@DNextDate,getutcdate())                          
          Print 'DateDiff'+convert(varchar(10),@DateDiff)      
         If @DateDiff=0 And (@bitEndType=1 Or((@bitEndType=0 and @EndDateDiff<=0) Or (@bitEndTransactionType=1 And @numNoTransactions<@numNoTransactionsRecurring)))                          
         Begin                         
          Set @CurrentDate=getutcdate()      
          print 'in'    
          print @numScheduleId    
          print @ReportId    
           insert into #ScheduleTempTable (ScheduleId,ReportId,ContactName,ContactEmail,ReportName,ToEmail,DomainID)  
			 values (@numScheduleId,@ReportId,@ContactName,@ContactEmail,@ReportName,dbo.GetScheduledEmails(@numScheduleId),@numDomainID)    
               End                     
                                             
                         End   -- tinymonthly End--0                       
                      
                        If @tintMonthlyType=1                      
                           Begin                      
                              If @LastRecurringDate='Jan  1 1753 12:00AM'                       
                                 Begin                      
           Set @DNextDate=@dtStartDate                  
           If @DNextDate < getutcdate()               
           Set @DNextDate=getutcdate()                    
           Set @Month=DATEPART(Month, @DNextDate)                      
           Set  @Year=DATEPART(Year, @DNextDate)                      
           Print '@Month = '+ Convert(varchar(10),@Month)                    
           Print @Year                       
           Print '@tintFirstDet=' + Convert(varchar(10),@tintFirstDet)                    
           Print '@tintWeekDays=' + Convert(varchar(10),@tintWeekDays)                  
           Print '@DNextDate=' + Convert(varchar(30),@DNextDate)                    
              Set @DNextDate=dateadd(month,((@Year-1900)*12)+@Month-1,@tintFirstDet-1)                      
           Print 'DNextDate==' + convert(varchar(30),@DNextDate)                
           Set @DNextDate=dbo.fn_RecurringTemplate(@tintFirstDet,@tintWeekDays,@DNextDate)                  
                 Print @DNextDate                 
                 Set @MonthNextDate=DATEPART(Month, @DNextDate)                   
                 Print '@MonthNextDate='+Convert(varchar(20),@MonthNextDate)                  
                                  End                        
                                Else                      
                                  Begin                      
                                     Set @DNextDate=DATEADD(Month, convert(numeric,@tintIntervalDays),  Convert(datetime, @LastRecurringDate))                       
                                     Set @Month=DATEPART(Month, getutcdate())                 
                                     Print '@Month = '+ Convert(varchar(10),@Month)                   
                                     Set @MonthNextDate=DATEPART(Month, @DNextDate)                 
                                     Print '@MonthNextDate='+Convert(varchar(20),@MonthNextDate)                
                                     Set @DNextDate=dbo.fn_RecurringTemplate(@tintFirstDet,@tintWeekDays,@DNextDate)                      
                                     Print @DNextDate                      
                                  End                      
                               Set @DateDiff = datediff (day,@DNextDate,getutcdate())                          
                               Print 'DateDifference'+Convert(varchar(30),@DateDiff)                      
                               If @DateDiff=0 And @Month=@MonthNextDate And ((@bitEndType=1 Or (@bitEndType=0 And getutcdate()<=@dtEndDate) Or (@bitEndTransactionType=1 And @numNoTransactions<@numNoTransactionsRecurring)))                                    
  
    
        
         Begin                          
         Set @CurrentDate=getutcdate()     
          print 'in'     
          print @numScheduleId    
          print @ReportId  
		declare @ToMailId as varchar(1000)                      
		set @ToMailId = dbo.GetScheduledEmails(@numScheduleId)
              insert into #ScheduleTempTable (ScheduleId,ReportId,ContactName,ContactEmail,ReportName,ToEmail,DomainID)  
			 values (@numScheduleId,@ReportId,@ContactName,@ContactEmail,@ReportName,@ToMailId,@numDomainID)   
         End                          
        
                         End   --End For @tintMonthlyType=1                      
                   End  --End for Monthly      
        
    
    
     If @chrIntervalType='Y'                      
                      Begin                      
                         Print @chrIntervalType                    
                         Print @LastRecurringDate                    
                         If @LastRecurringDate='Jan  1 1753 12:00AM'                       
                             Begin                      
                               If @dtStartDate < getutcdate()               
                                 Set @dtStartDate=getutcdate()              
                               Set @Year= DATEPART(Year, @dtStartDate)                      
                               Print @Year                       
                               Set @DNextDate=dateadd(month,((@Year-1900)*12)+@tintMonths-1,@tintFirstDet-1)                      
                               Print @DNextDate                      
                             End                      
                         Else                      
                           Begin                       
                             Set @DNextDate=DATEADD(Year, convert(numeric,1),  Convert(datetime, @LastRecurringDate))                    
                             Print @DNextDate                        
                           End                      
                        Set @DateDiff = datediff (day,@DNextDate,getutcdate())                          
                        -- print 'Yearly Diff' + Convert(varchar(2),@DateDiff)                      
                        Print @DateDiff                    
                          If @DateDiff=0 And (@bitEndType=1 Or((@bitEndType=0 and @EndDateDiff<=0) Or (@bitEndTransactionType=1 And @numNoTransactions<@numNoTransactionsRecurring)))                          
                          Begin                         
       Set @CurrentDate=getutcdate()    
        print 'in'    
        print @numScheduleId    
        print @ReportId    
     insert into #ScheduleTempTable (ScheduleId,ReportId,ContactName,ContactEmail,ReportName,ToEmail,[DomainID])  
			 values (@numScheduleId,@ReportId,@ContactName,@ContactEmail,@ReportName,dbo.GetScheduledEmails(@numScheduleId),@numDomainID)   
    --    Update CheckDetails set LastRecurringDate=getutcdate()  Where numCheckId=@numCheckId        
                          End                               
      End  --End for Yearly     
                            
   End --End Of ScheduleId <> 0     
   Select top 1 @numScheduleId = numScheduleId       
   From [CustRptScheduler] Where numScheduleId > @numScheduleId  And numScheduleId <= @numMaxScheduleId                     
                       
   If @@rowcount=0 set @numScheduleId=0                        
       
End    


--declare @Id as numeric(9)    
--declare    @SScheduleID numeric(9) 
--declare  @SReportId numeric(9)  
--declare  @SContactName  as varchar(20) 
--declare  @SContactEmail as varchar(20)  
----declare  @SReportName as varchar(20)
--select top 1 @Id=Id,@SScheduleID=ScheduleID,@SReportId=ReportId,@SReportName=ReportName from  #ScheduleTempTable   
select * from #ScheduleTempTable


 
--    
DROP TABLE #ScheduleTempTable
