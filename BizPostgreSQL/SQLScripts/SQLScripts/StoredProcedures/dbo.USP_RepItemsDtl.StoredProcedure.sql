/****** Object:  StoredProcedure [dbo].[USP_RepItemsDtl]    Script Date: 07/26/2008 16:20:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- created by anoop jayaraj    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_repitemsdtl')
DROP PROCEDURE usp_repitemsdtl
GO
CREATE PROCEDURE [dbo].[USP_RepItemsDtl]     
  @numDomainID numeric,            
  @dtFromDate DATETIME,            
  @dtToDate DATETIME,            
  @numUserCntID numeric=0,                 
  @tintType numeric,    
  @SortOrder as tinyint,    
  @ItemCode as numeric     
      
as      
      
if  @SortOrder=1    
begin     
SELECT dbo.FormatedDateFromDate(mst.bintCreatedDate,mst.numDomainId) as [Purchase Deal Date],    
vcPOppName as [Deal ID]

--Commented by chintan- don't know if its required here or not. 
--'' as [Shipping Company],    
--'' as [Tracking URL]    
from OpportunityMaster mst     
join OpportunityItems Itm    
on Itm.numOppId=mst.numOppId        
where numItemCode=@ItemCode and  mst.tintOppstatus=1 and tintShipped=0 and tintOppType=2    
and mst.numDomainID=@numDomainID    
    
end    
if  @SortOrder=2    
begin     
select dbo.FormatedDateFromDate(mst.bintCreatedDate,mst.numDomainId) as [Sales Deal Date],    
vcPOppName as [Deal ID]    
from OpportunityMaster mst     
join OpportunityItems Itm    
on Itm.numOppId=mst.numOppId    
where numItemCode=@ItemCode and  mst.tintOppstatus=1 and tintShipped=0 and tintOppType=1    
and mst.numDomainID=@numDomainID    
    
end    
if  @SortOrder=3    
begin     
select dbo.FormatedDateFromDate(mst.bintCreatedDate,mst.numDomainId) as [Sales Deal Date],    
vcPOppName as [Deal ID]
--Commented by chintan- don't know if its required here or not.   
--'' as [Shipping Company],    
--'' as [Tracking URL]    
from OpportunityMaster mst     
join OpportunityItems Itm    
on Itm.numOppId=mst.numOppId       
where numItemCode=@ItemCode and  mst.tintOppstatus=1 and tintShipped=0 and tintOppType=1    
and mst.numDomainID=@numDomainID    
    
end
GO
