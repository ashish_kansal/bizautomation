/****** Object:  StoredProcedure [dbo].[usp_GetOpportunityOpenForReport]    Script Date: 07/26/2008 16:18:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by Siva  
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getopportunityopenforreport')
DROP PROCEDURE usp_getopportunityopenforreport
GO
CREATE PROCEDURE [dbo].[usp_GetOpportunityOpenForReport]    
@numDomainId numeric,    
@intSalesPurchase numeric=0,    
@numUserCntId numeric=0,    
@intType tinyint=0,    
@tintRights tinyint=1    
    
AS    
    
BEGIN    
IF @tintRights=1    
--All Records Rights    
    
BEGIN    
    
 SELECT OM.numOppId as numOppID,OM.vcPOppName as OpportunityName,isnull(dbo.GetOppLstMileStoneCompltd(numOppId),'') as LastMileStoneCompleted,OM.bintCreatedDate as bintCreatedDate,    
                
 isnull(dbo.GetOppLstStage(numOppId),'') as LastStageCompleted, dbo.fn_GetContactName(OM.numRecOwner) as numRecOwner,    
dbo.fn_GetContactName(OM.numAssignedTo)  as numAssignedTo, OM.monPAmount as monPAmount    
 FROM OpportunityMaster OM    
 WHERE OM.numDomainID=@numDomainID             
 AND OM.tintOppStatus=0               
 AND OM.tintOppType=@intSalesPurchase    
    
END    
        
IF @tintRights=2     
    
BEGIN    
 SELECT OM.numOppId as numOppID,OM.vcPOppName as OpportunityName,isnull(dbo.GetOppLstMileStoneCompltd(numOppId),'') as LastMileStoneCompleted,OM.bintCreatedDate as bintCreatedDate,                
 isnull(dbo.GetOppLstStage(numOppId),'') as LastStageCompleted, dbo.fn_GetContactName(OM.numRecOwner) as numRecOwner,    
 dbo.fn_GetContactName(OM.numAssignedTo) as numAssignedTo, OM.monPAmount as monPAmount     
 FROM OpportunityMaster OM    
 join AdditionalContactsInformation ADC  
 on ADC.numContactID=OM.numRecOwner  
 WHERE OM.numDomainID=@numDomainID             
 AND OM.tintOppStatus=0               
 AND OM.tintOppType=@intSalesPurchase    
 AND ADC.numTeam in(SELECT F.numTeam FROM ForReportsByTeam F                     
  WHERE F.numUserCntID=@numUserCntID and F.numDomainid=@numDomainId and F.tintType=@intType)  
END    
    
if @tintRights=3    
    
BEGIN    
    
SELECT OM.numOppId as numOppID,OM.vcPOppName as OpportunityName,isnull(dbo.GetOppLstMileStoneCompltd(numOppId),'') as LastMileStoneCompleted,OM.bintCreatedDate as bintCreatedDate,               
 isnull(dbo.GetOppLstStage(numOppId),'') as LastStageCompleted, dbo.fn_GetContactName(OM.numRecOwner) as numRecOwner,    
 dbo.fn_GetContactName(OM.numAssignedTo) as numAssignedTo, OM.monPAmount as monPAmount     
 FROM OpportunityMaster OM   
 join DivisionMaster D  
 on D.numDivisionID=OM.numDivisionID   
 WHERE OM.numDomainID=@numDomainID             
 AND OM.tintOppStatus=0               
 AND OM.tintOppType=@intSalesPurchase    
 AND D.numTerID in(SELECT F.numTerritory FROM ForReportsByTerritory F                   
 WHERE F.numUserCntID=@numUserCntID and F.numDomainid=@numDomainID and F.tintType=@intType)  
    
END    
    
END
GO
