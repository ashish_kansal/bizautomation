/****** Object:  StoredProcedure [dbo].[usp_DeleteOpportunityStageComments]    Script Date: 07/26/2008 16:15:37 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_deleteopportunitystagecomments')
DROP PROCEDURE usp_deleteopportunitystagecomments
GO
CREATE PROCEDURE [dbo].[usp_DeleteOpportunityStageComments]
	@numOppId numeric
	
--
AS
	
       Delete from OpptStageComments where numoppid=@numOppId
GO
