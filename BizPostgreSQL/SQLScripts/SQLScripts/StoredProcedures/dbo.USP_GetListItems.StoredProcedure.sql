/****** Object:  StoredProcedure [dbo].[USP_GetListItems]    Script Date: 07/26/2008 16:17:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--modified by anoop jayaraj      
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getlistitems')
DROP PROCEDURE usp_getlistitems
GO
CREATE PROCEDURE [dbo].[USP_GetListItems]        
@Listdata as text ='',
@numDomainID as numeric(9)=0         
      
as 
DECLARE @hDoc1 int                                                
EXEC sp_xml_preparedocument @hDoc1 OUTPUT, @Listdata  
                                              
SELECT  numListID,numListItemID,vcData FROM listdetails       
WHERE (constFlag=1 or numDomainID=@numDomainID) and numListID in                                                 
          
 (select * from (           
SELECT * FROM OPENXML (@hDoc1,'/NewDataSet/Table',2)                                                
 WITH  (                                            
  list numeric(9)        
 ))X    ) 

EXEC sp_xml_removedocument @hDoc1
GO
