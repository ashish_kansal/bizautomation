/****** Object:  StoredProcedure [dbo].[usp_GetSearchableColumns4SearchRecords]    Script Date: 07/26/2008 16:18:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
--Created By: Debasish Tapan Nag                  
--Purpose: This returns all the fields in the fields in the view
--DynamicAdvSearchView which are available for Advance Search
--Created Date: 07/13/2005             
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getsearchablecolumns4searchrecords')
DROP PROCEDURE usp_getsearchablecolumns4searchrecords
GO
CREATE PROCEDURE [dbo].[usp_GetSearchableColumns4SearchRecords]
AS                          
BEGIN                          
  SELECT * FROM dbo.DynamicAdvSearchView WHERE 1 = 2
END
GO
