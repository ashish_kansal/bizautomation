/****** Object:  StoredProcedure [dbo].[USP_GetAlertDtlforPastDueBills]    Script Date: 07/26/2008 16:16:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created By Anoop Jayaraj    
    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getalertdtlforpastduebills')
DROP PROCEDURE usp_getalertdtlforpastduebills
GO
CREATE PROCEDURE [dbo].[USP_GetAlertDtlforPastDueBills]    
@numDepartMentID as numeric(9)=0 ,
@numDomainId as numeric(9)   =1
as    
    
select ALT.numAlertDTLid,ADDT.numEmailTemplate,ADDT.tintAlertOn,ADDT.numDaysAfterDue,ADDT.numBizDocs,ADDT.numDepartment,lst1.vcData as BizDoc
,vcDocName as EmailTemplate  from AlertDTL Alt  
join   AlertDomainDTL ADDT on ADDT.numAlertDTLid =Alt.numAlertDTLid
join listDetails as lst1    
on lst1.numListItemID=ADDT.numBizDocs    
join GenericDocuments as Doc    
on Doc.numGenericDocID=ADDT.numEmailTemplate    
where numAlertID=8 and ADDT.numDepartMent=@numDepartMentID    and ADDT.numDomainId=@numDomainId
GO
