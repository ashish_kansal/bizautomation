/****** Object:  StoredProcedure [dbo].[USP_GetContactEmailAddress]    Script Date: 07/26/2008 16:16:47 ******/
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Anoop jayaraj>
-- Create date: <Create Date,,NA>
-- Description:	<Description,,Fetch ContactDetails>
--Modified By:Sachin Sadhu
--Modified date: <Modified Date,,12-Dec-13>
--Description: Added Filteration CompanyName
-- =============================================
SET QUOTED_IDENTIFIER ON
GO
--Created By Anoop jayaraj              
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getcontactemailaddress')
DROP PROCEDURE usp_getcontactemailaddress
GO
CREATE PROCEDURE [dbo].[USP_GetContactEmailAddress]                   
-- @numUserCntID numeric(9)=0,                    
 @numDomainID numeric(9)=0,                    
-- @tintUserRightType tinyint=0,                                         
@keyWord varchar(100)='',                  
@CurrentPage int,                  
@PageSize int,                  
@TotRecs int output,                  
@columnName as Varchar(50),                  
@columnSortOrder as Varchar(10),
@bitFlag AS BIT=0
--                    
as       
declare @strSql as NVARCHAR(4000)              
 IF @bitFlag = 0
 BEGIN
 	set  @strSql='SELECT TOP '+ CAST (@PageSize AS VARCHAR(10))+ ' * FROM dbo.View_SelectEmailContact WHERE (vcEmail LIKE @keyWord + ''%'' OR vcFirstName LIKE @keyWord + ''%'' OR vcLastName LIKE @keyWord + ''%'' OR CompanyName LIKE @keyword+''%'') and numDomainID= ' + CONVERT(VARCHAR(10),@numDomainID);
IF @numDomainID = 0 
	set  @strSql=@strSql + ' and 1=0 ';

PRINT @strSql;
 EXECUTE sp_executeSQL @strSql, N'@keyWord varchar(100)', 
		     @keyWord;
 END     
 ELSE
 BEGIN
 	
            
Create table #tempTable (                           
 numContactID NUMERIC,
 CompanyName VARCHAR(255), 
 Company VARCHAR(200),                     
 vcEmail varchar(50),                  
 vcFirstName varchar(50),                  
 vcLastName varchar(50),
 numDomainId NUMERIC(18,0),
 RowNumber NUMERIC(18,0),
 bintCreatedDate DATETIME                           
 )                  
 SET @strSql =''          
 IF @columnName = 'bintCreatedDate'
 BEGIN
 SET @columnName ='ADC.' + @columnName 	
 END
 ELSE
 BEGIN
 	SET @columnName ='vwSM.' + @columnName 
 END
 
set  @strSql='insert into #tempTable SELECT vwSM.*,ROW_NUMBER() OVER (Order by '+@columnName+ '  '+ @columnSortOrder+') AS RowNumber,ADC.bintCreatedDate   FROM dbo.View_SelectEmailContact vwSM left join dbo.AdditionalContactsInformation ADC on ADC.numContactId =  vwSM.numContactId WHERE (vwSM.vcEmail LIKE @keyWord + ''%'' OR vwSM.vcFirstName LIKE @keyWord + ''%'' OR vwSM.vcLastName LIKE @keyWord + ''%''   OR vwSM.CompanyName LIKE @keyword+''%'') and vwSM.numDomainID= ' + CONVERT(VARCHAR(10),@numDomainID) + ' And Datalength(vwSM.vcEmail) > 3'
IF @numDomainID = 0 
	set  @strSql=@strSql + ' and 1=0 '

SET @strSql = @strSql + ' ORDER BY '+@columnName+ ' '+ @columnSortOrder+';'
PRINT @strSql;
 EXECUTE sp_executeSQL @strSql, N'@keyWord varchar(100)', 
		     @keyWord;
declare @firstRec as integer
declare @lastRec as integer
set @firstRec= (@CurrentPage-1) * @PageSize                  
set @lastRec= (@CurrentPage*@PageSize+1)                  
SET @strSql = '              
select * from #tempTable   WHERE RowNumber > '+ convert(varchar(10),@firstRec) + ' and RowNumber < ' + convert(varchar(10),@lastRec)+ ''
--SELECT * FROM  #tempTable
PRINT @strSql
EXECUTE(@strSql)

           
set @TotRecs=(select count(*) from #tempTable)   
PRINT @TotRecs;                   
drop table #tempTable
END
GO
