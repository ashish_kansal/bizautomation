/****** Object:  StoredProcedure [dbo].[USP_SaveReconcileDetails]    Script Date: 07/26/2008 16:21:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created By Siva    
/* We are no longer using this procedure ..-- by chintan*/
            
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_savereconciledetails')
DROP PROCEDURE usp_savereconciledetails
GO
CREATE PROCEDURE [dbo].[USP_SaveReconcileDetails]                
(@numChartAcntId as numeric(9)=0,                
 @numAcntType as numeric(9)=0,                
@dtAdjustmentDate as datetime,                
@monAdjustmentAmt as DECIMAL(20,5),                
@numUserCntID as numeric(9)=0,                
@numDomainId as numeric(9)=0)                
As                
Begin                
 Declare @numServiceAcntId as numeric(9)                
 Declare @dtServiceCharge as datetime                
 Declare @monserviceCharges as DECIMAL(20,5)                
 Declare @numInterestEarnedAcntId as numeric(9)                
 Declare @dtInternestEarned as datetime                
 Declare @monInterestEarned as DECIMAL(20,5)                
 Declare @numFinancialAntId as numeric(9)                
 Declare @dtFinancialCharges as datetime                
 Declare @monFinancialCharges as DECIMAL(20,5)                
 Declare @numCheckId as numeric(9)                
 Declare @numCheckJournalId as numeric(9)                
 Declare @numDepositId as numeric(9)                
 Declare @numDepositJournalId as numeric(9)                
 Declare @numCashCreditId as numeric(9)                
 Declare @numCashCreditJournalId as numeric(9)                
 Declare @numAdjustmentJournalId as numeric(9)              
 Declare @monEndingBalance as DECIMAL(20,5)          
 Declare @numAccountIdOpeningEquity as numeric(9)      
 Declare @numCheckNo as numeric(9)
 Declare @numBankRoutingNo as numeric(9)
 Declare @numBankAccountNo as numeric(9)
          
-- Set @numAccountIdOpeningEquity=(Select numAccountId From Chart_Of_Accounts Where isnull(bitOpeningBalanceEquity,0)=1  and numDomainId = @numDomainId)       
--         
--               
-- Select @numServiceAcntId=isnull(numServiceAcntId,0), @dtServiceCharge=dtServiceCharge,@monserviceCharges=monserviceCharges,                
-- @numInterestEarnedAcntId=isnull(numInterestEarnedAcntId,0),@dtInternestEarned=dtInternestEarned,@monInterestEarned=monInterestEarned,                
-- @numFinancialAntId=isnull(numFinancialAntId,0),@dtFinancialCharges=dtFinancialCharges,@monFinancialCharges=monFinancialCharges,          
-- @monEndingBalance=isnull(monEndingBal,0) From Chart_Of_Accounts Where numAccountId=@numChartAcntId                
--            
--    --Journal Entry for Service Charges                
-- If @numServiceAcntId<>0                 
--  Begin                
--   ---CheckDetails table     
--
--    Select @numCheckNo=isnull(numCheckNo,0) +1,@numBankRoutingNo=numBankRoutingNo,@numBankAccountNo=numBankAccountNo from CheckDetails Where numDomainId=@numDomainId 
--    And numCheckId=(Select max(isnull(numCheckId,0)) from  CheckDetails Where numDomainId=@numDomainId)             
--    If @numCheckNo is null Set @numCheckNo=1
--    If @numBankRoutingNo is null Set @numBankRoutingNo=1
--    If @numBankAccountNo is null Set @numBankAccountNo=1
--   Insert into CheckDetails(numAcntTypeId,datCheckDate,varMemo,monCheckAmt,numDomainId,numCheckNo,numBankRoutingNo,numBankAccountNo,numCreatedBy,dtCreatedDate) Values                
--   (@numChartAcntId,@dtServiceCharge,'Service Charges',@monserviceCharges,@numDomainId,@numCheckNo,@numBankRoutingNo,@numBankAccountNo,@numUserCntID,getutcdate())                
--    Set @numCheckId = @@Identity                 
--                   
--   --General_Journal_Header table                
--            Insert into General_Journal_Header(datEntry_Date,varDescription,numAmount,numDomainId,numCheckId,numCreatedBy,datCreatedDate) Values                
--   (@dtServiceCharge,'Service Charges',@monserviceCharges,@numDomainId,@numCheckId,@numUserCntID,getutcdate())                
--   Set @numCheckJournalId=@@Identity                
--                
--   --General_Journal_Details Table -- For Main Checks                
--   Insert into General_Journal_Details(numJournalId,numDebitAmt,numCreditAmt,numChartAcntId,varDescription,numDomainId,bitMainCheck,bitReconcile) Values                
--   (@numCheckJournalId,0,@monserviceCharges,@numChartAcntId,'Service Charges',@numDomainId,1,0)                
--                  
--   Insert into General_Journal_Details(numJournalId,numDebitAmt,numCreditAmt,numChartAcntId,varDescription,numDomainId,bitMainCheck,bitReconcile) Values                
--   (@numCheckJournalId,@monserviceCharges,0,@numServiceAcntId,'Service Charges',@numDomainId,0,0)                
-- Exec USP_UpdateChartAcntOpnBalanceForJournalEntry  @JournalId=@numCheckJournalId,@numDomainId=@numDomainId                     
--  End                
--                
--   --Journal Entry for Interest Charges                
-- If @numInterestEarnedAcntId<>0                 
--  Begin                
--            ---DepositDetails table                
--   Insert into DepositDetails(numChartAcntId,dtDepositDate,monDepositAmount,numDomainId,numCreatedBy,dtCreationDate) Values           
--   (@numChartAcntId,@dtInternestEarned,@monInterestEarned,@numDomainId,@numUserCntID,getutcdate())                
--    Set @numDepositId = @@Identity                 
--                   
--   --General_Journal_Header table                
--            Insert into General_Journal_Header(datEntry_Date,varDescription,numAmount,numDomainId,numDepositId,numCreatedBy,datCreatedDate) Values                
--   (@dtInternestEarned,'Interest',@monInterestEarned,@numDomainId,@numDepositId,@numUserCntID,getutcdate())                
--   Set @numDepositJournalId=@@Identity                
--                
--   --General_Journal_Details Table For Main Deposit                
--   Insert into General_Journal_Details(numJournalId,numDebitAmt,numCreditAmt,numChartAcntId,varDescription,numDomainId,bitMainDeposit,bitReconcile) Values                
--   (@numDepositJournalId,@monInterestEarned,0,@numChartAcntId,'Interest',@numDomainId,1,0)                
--                   
--   Insert into General_Journal_Details(numJournalId,numDebitAmt,numCreditAmt,numChartAcntId,varDescription,numDomainId,bitMainDeposit,bitReconcile) Values                
--   (@numDepositJournalId,0,@monInterestEarned,@numInterestEarnedAcntId,'Interest',@numDomainId,0,0)                
--   Exec USP_UpdateChartAcntOpnBalanceForJournalEntry  @JournalId=@numDepositJournalId ,@numDomainId=@numDomainId                   
--  End                
--                 
-- --Journal Entry for Finance Charges                
-- If @numFinancialAntId<>0                 
--  Begin                
--     ---CashCreditCardDetails table                
--     Insert into CashCreditCardDetails(datEntry_Date,monAmount,vcMemo,numAcntTypeId,bitMoneyOut,bitChargeBack,numDomainId,numCreatedBy,dtCreatedDate) Values                
--     (@dtFinancialCharges,@monFinancialCharges,'Finance Charge',@numChartAcntId,1,1,@numDomainId,@numUserCntID,getutcdate())                                            
--     Set @numCashCreditId = @@IDENTITY                                            
--                      
--                
--   --General_Journal_Header table                
--            Insert into General_Journal_Header(datEntry_Date,varDescription,numAmount,numDomainId,numCashCreditCardId,numCreatedBy,datCreatedDate) Values                
--   (@dtFinancialCharges,'Finance Charge',@monFinancialCharges,@numDomainId,@numCashCreditId,@numUserCntID,getutcdate())                
--   Set @numCashCreditJournalId=@@Identity                
--                
--   --General_Journal_Details Table For Main Deposit                
--   Insert into General_Journal_Details(numJournalId,numDebitAmt,numCreditAmt,numChartAcntId,varDescription,numDomainId,bitMainCashCredit,bitReconcile) Values                
--   (@numCashCreditJournalId,0,@monFinancialCharges,@numChartAcntId,'Finance Charge',@numDomainId,1,0)                
--                   
--   Insert into General_Journal_Details(numJournalId,numDebitAmt,numCreditAmt,numChartAcntId,varDescription,numDomainId,bitMainCashCredit,bitReconcile) Values                
--   (@numCashCreditJournalId,@monFinancialCharges,0,@numFinancialAntId,'Finance Charge',@numDomainId,0,0)                
--   Exec USP_UpdateChartAcntOpnBalanceForJournalEntry  @JournalId=@numCashCreditJournalId,@numDomainId=@numDomainId                  
--  End                     
--                 
-- --Journal for Adjustment Entry               
-- print @monAdjustmentAmt             
-- if @monAdjustmentAmt<0                 
--                  
-- Begin                
--  Set @monAdjustmentAmt=replace(@monAdjustmentAmt, '-', '')            
--  print @monAdjustmentAmt            
--  --General_Journal_Header table                
--        Insert into General_Journal_Header(datEntry_Date,varDescription,numAmount,numDomainId,numCreatedBy,datCreatedDate) Values                
--  (@dtAdjustmentDate,'Balance Adjustment',@monAdjustmentAmt,@numDomainId,@numUserCntID,getutcdate())                
--  Set @numAdjustmentJournalId=@@Identity                
--                  
--  --General_Journal_Details Table For Adjustment Entry                
--  Insert into General_Journal_Details(numJournalId,numDebitAmt,numCreditAmt,numChartAcntId,varDescription,numDomainId,bitReconcile) Values                
--  (@numAdjustmentJournalId,0,@monAdjustmentAmt,@numChartAcntId,'Balance Adjustment',@numDomainId,0)                
--                  
--  Insert into General_Journal_Details(numJournalId,numDebitAmt,numCreditAmt,numChartAcntId,varDescription,numDomainId,bitReconcile) Values                
--  (@numAdjustmentJournalId,@monAdjustmentAmt,0,@numAccountIdOpeningEquity,'Balance Adjustment',@numDomainId,0)                
--  Exec USP_UpdateChartAcntOpnBalanceForJournalEntry  @JournalId=@numAdjustmentJournalId,@numDomainId=@numDomainId                   
-- End                
-- Else if @monAdjustmentAmt>0                 
--                  
-- Begin                
--  --General_Journal_Header table                
--        Insert into General_Journal_Header(datEntry_Date,varDescription,numAmount,numDomainId,numCreatedBy,datCreatedDate) Values                
--  (@dtAdjustmentDate,'Balance Adjustment',@monAdjustmentAmt,@numDomainId,@numUserCntID,getutcdate())                
--  Set @numAdjustmentJournalId=@@Identity                
--                  
--  --General_Journal_Details Table For Adjustment Entry                
--  Insert into General_Journal_Details(numJournalId,numDebitAmt,numCreditAmt,numChartAcntId,varDescription,numDomainId,bitReconcile) Values                
--  (@numAdjustmentJournalId,@monAdjustmentAmt,0,@numChartAcntId,'Balance Adjustment',@numDomainId,0)                
--                  
--  Insert into General_Journal_Details(numJournalId,numDebitAmt,numCreditAmt,numChartAcntId,varDescription,numDomainId,bitReconcile) Values                
--  (@numAdjustmentJournalId,0,@monAdjustmentAmt,@numAccountIdOpeningEquity,'Balance Adjustment',@numDomainId,0)                
--  Exec USP_UpdateChartAcntOpnBalanceForJournalEntry  @JournalId=@numAdjustmentJournalId,@numDomainId=@numDomainId                   
-- End                
--              
-- Update General_Journal_Details Set bitReconcile=1 Where numChartAcntId=@numChartAcntId And numDomainId=@numDomainId              
--          
-- Update Chart_of_Accounts Set monEndingOpeningBal=@monEndingBalance,monEndingBal=null,numServiceAcntId=null,dtServiceCharge=null,monserviceCharges=null,numInterestEarnedAcntId=null,            
-- dtInternestEarned=null,monInterestEarned=null,numFinancialAntId=null,dtFinancialCharges=null,monFinancialCharges=null            
-- Where numAccountId=@numChartAcntId And numDomainId=@numDomainId             
End
GO
