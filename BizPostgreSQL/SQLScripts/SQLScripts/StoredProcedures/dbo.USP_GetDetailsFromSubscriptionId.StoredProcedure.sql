/****** Object:  StoredProcedure [dbo].[USP_GetDetailsFromSubscriptionId]    Script Date: 07/26/2008 16:17:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created by anoop jayaraj                                    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getdetailsfromsubscriptionid')
DROP PROCEDURE usp_getdetailsfromsubscriptionid
GO
CREATE PROCEDURE [dbo].[USP_GetDetailsFromSubscriptionId]      
(                                                              
@SubscriptionId as varchar(150)=''                                                              
)                                        
as                                        
                                          
 SELECT top 1 U.numUserID,numUserDetailId,isnull(vcEmailID,'') vcEmailID,isnull(resourceId,0 ) resourceId,  
 isnull(U.numDomainID,0) numDomainID,        
 isnull(Div.numDivisionID,0) numDivisionID,isnull(E.bitExchangeIntegration,0) bitExchangeIntegration,        
 isnull(E.bitAccessExchange,0) bitAccessExchange,isnull(E.vcExchPath,'') vcExchPath,      
isnull(E.vcExchDomain,'') vcExchDomain,        
 isnull(D.vcExchUserName,'') vcExchUserName ,isnull(vcFirstname,'')+' '+isnull(vcLastName,'') as ContactName,        
 Case when E.bitAccessExchange=0 then  E.vcExchPassword when E.bitAccessExchange=1 then D.vcExchPassword end as vcExchPassword ,       
Case when E.bitAccessExchange=0 then  E.vcExchPath when E.bitAccessExchange=1 then D.vcExchPath end as vcExchPath       
       
--,Case when E.bitAccessExchange=0 then  E.vcExchDomain when E.bitAccessExchange=1 then D.vcExchDomain end as vcExchDomain       
  FROM UserMaster U        
 left join ExchangeUserDetails E        
 on E.numUserID=U.numUserID        
 Join Domain D        
 on D.numDomainID=U.numDomainID                     
 left join DivisionMaster Div                      
 on D.numDivisionID=Div.numDivisionID         
 left join CompanyInfo C        
 on C.numCompanyID=Div.numDivisionID         
 left join AdditionalContactsInformation A        
 on  A.numContactID=U.numUserDetailId        
left join resource R        
 on  R.numUserCntID=U.numUserDetailId        
 WHERE SubscriptionId = @SubscriptionId
GO
