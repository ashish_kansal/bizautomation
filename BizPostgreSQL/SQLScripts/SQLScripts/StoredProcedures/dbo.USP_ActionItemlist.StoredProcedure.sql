/****** Object:  StoredProcedure [dbo].[USP_ActionItemlist]    Script Date: 07/26/2008 16:14:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- Created By Anoop jayaraj                                          
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_actionitemlist')
DROP PROCEDURE usp_actionitemlist
GO
CREATE PROCEDURE [dbo].[USP_ActionItemlist]                                          
@numUserCntID as numeric(9)=null,                                          
@numDomainID as numeric(9)=null,                                          
@dtStartDate datetime,                                                
@dtEndDate datetime,                                           
@RepType as tinyint,                                          
@Condition as Char(1),                                          
@bitTask as numeric(9),                                          
@CurrentPage int,                                            
@PageSize int,                                            
@TotRecs int output,                                            
@columnName as Varchar(50),                                            
@columnSortOrder as Varchar(10),                                        
@strUserIDs as varchar(1000),                
@TeamType as tinyint                                          
as                                           
 if @strUserIDs='' set @strUserIDs='0'                                           
                                            
  declare @strSql as varchar(8000)                                          
if @RepType<>3                                        
begin                                        
create table #tempTable ( ID INT IDENTITY PRIMARY KEY,                                             
      closeDate varchar(20),                                            
      itemDesc varchar(1000),       
      Phone varchar(100),                                           
      Name VARCHAR(200),                                            
      vcCompanyName varchar(110),                                            
      vcEmail varchar(50),                                          
      Task varchar(20)                                            
 )                                            
                                            
                                            
                                        
                                          
 set @strSql='SELECT                                           
 Comm.dtStartTime AS closeDate,                                           
 convert(varchar(1000),Comm.textDetails) AS itemDesc,                                          
 AddC.vcFirstName + '' '' + AddC.vcLastName as [Name],        
  case when AddC.numPhone<>'''' then + AddC.numPhone +case when AddC.numPhoneExtension<>'''' then '' - '' + AddC.numPhoneExtension else '''' end  else '''' end as [Phone],                                                                                    
  
    
      
                                                                  
  Comp.vcCompanyName,                                           
 AddC.vcEmail,                                          
 case when Comm.bitTask=971 then  ''Communication''  when Comm.bitTask=972 then  ''Task''  else ''Unknown'' end   AS Task                                           
 FROM  Communication Comm                                           
 JOIN AdditionalContactsInformation AddC                                           
 ON Comm.numContactId = AddC.numContactId                                           
 JOIN DivisionMaster Div                                           
 ON AddC.numDivisionId = Div.numDivisionID                                           
 INNER JOIN COMPANYiNFO  Comp                                           
 ON Div.numCompanyID = Comp.numCompanyId                                            
 WHERE Comm.numDomainID =' + convert(varchar(15),@numDomainID)                                          
                                          
-- Action Item  scheduled                                          
 if @Condition='S' and @RepType=1  set @strSql=@strSql +' and bitTask=' + convert(varchar(20),@bitTask)+' and Comm.dtCreatedDate between ''' + convert(varchar(20),@dtStartDate)+'''                                 
        and ''' + convert(varchar(20),@dtEndDate)+''' and Comm.numCreatedBy  in (select numContactID from AdditionalContactsInformation                                                
 where numTeam in(select numTeam from ForReportsByTeam where numUserCntID=' + convert(varchar(15),@numUserCntID)+' and numDomainID=' + convert(varchar(15),@numDomainId)+' and tintType=' + convert(varchar(15),@TeamType)+'))'                                
 
     
      
        
          
 if @Condition='S' and @RepType=2 set @strSql=@strSql +' and bitTask=' + convert(varchar(20),@bitTask)+' and Comm.dtCreatedDate between ''' + convert(varchar(20),@dtStartDate)+'''                                            
        and ''' + convert(varchar(20),@dtEndDate)+''' and Comm.numCreatedBy =' + convert(varchar(15),@numUserCntID)                                          
                                          
-- Action Item  Completed                                          
 if @Condition='C' and @RepType=1  set @strSql=@strSql +' and Comm.bitTask=' + convert(varchar(20),@bitTask)+' and Comm.dtCreatedDate between ''' + convert(varchar(20),@dtStartDate)+'''                                            
 and ''' + convert(varchar(20),@dtEndDate)+''' and Comm.bitClosedFlag=1 and Comm.numCreatedBy in (select numContactID from AdditionalContactsInformation                                              
 where numTeam in(select numTeam from ForReportsByTeam where numUserCntID=' + convert(varchar(15),@numUserCntID)+' and numDomainID=' + convert(varchar(15),@numDomainId)+' and tintType=' + convert(varchar(15),@TeamType)+'))'                                
  
    
      
        
                        
                                           
 if @Condition='C' and @RepType=2  set @strSql=@strSql +' and bitTask=' + convert(varchar(20),@bitTask)+' and Comm.dtCreatedDate between ''' + convert(varchar(20),@dtStartDate)+'''                                            
 and ''' + convert(varchar(20),@dtEndDate)+''' and Comm.bitClosedFlag=1 and Comm.numCreatedBy =' + convert(varchar(15),@numUserCntID)                                          
                                          
-- Action Item  Past Due                                          
 if @Condition='P' and @RepType=1  set @strSql=@strSql +' and Comm.bitTask=' + convert(varchar(20),@bitTask)+'                                           
 and Comm.dtCreatedDate between ''' + convert(varchar(20),@dtStartDate)+''' and ''' + convert(varchar(20),@dtEndDate)+'''                                           
 and Comm.bitClosedFlag=0 and Comm.numCreatedBy in (select numContactID from AdditionalContactsInformation                                              
where numTeam in(select numTeam from ForReportsByTeam where numUserCntID=' + convert(varchar(15),@numUserCntID)+'                                           
and numDomainID=' + convert(varchar(15),@numDomainId)+' and tintType=' + convert(varchar(15),@TeamType)+')) and dtStartTime< ''' + convert(varchar(20),getutcdate())+''''                                                                         
                                          
 if @Condition='P' and @RepType=2  set @strSql=@strSql +' and Comm.bitTask=' + convert(varchar(20),@bitTask)+'                
 and dtStartTime< ''' + convert(varchar(20),getutcdate())+'''                                          
 and Comm.dtCreatedDate between ''' + convert(varchar(20),@dtStartDate)+''' and ''' + convert(varchar(20),@dtEndDate)+'''                                           
 and Comm.bitClosedFlag=0 and Comm.numCreatedBy =' + convert(varchar(15),@numUserCntID)                                          
                                          
                                          
 set @strSql=@strSql +' ORDER BY ' + @columnName +' '+ @columnSortOrder                                              
  print    @strSql                                     
insert into #tempTable(                                            
 closeDate,                                            
      itemDesc,                                            
      Name,      
      Phone,                                            
      vcCompanyName,                                            
      vcEmail,                                          
      Task)                                          
exec (@strSql)                                             
                                            
  declare @firstRec as integer                                             
  declare @lastRec as integer                                            
 set @firstRec= (@CurrentPage-1) * @PageSize                                            
     set @lastRec= (@CurrentPage*@PageSize+1)                                            
select * from #tempTable where ID > @firstRec and ID < @lastRec                                            
set @TotRecs=(select count(*) from #tempTable)                                            
drop table #tempTable                                 
                                        
end                                        
if @RepType=3                                        
begin                                        
                              set @strSql='SELECT ADC1.numContactId,isnull(vcFirstName,''-'') as vcFirstName,isnull(vcLastName,''-'') as vcLastName,(SELECT                                           
 count(*)                                        
 FROM  Communication Comm '                                
                                         
                                        
if @Condition='S'  set @strSql=@strSql +' where bitTask=' + convert(varchar(20),@bitTask)+' and Comm.dtCreatedDate between ''' + convert(varchar(20),@dtStartDate)+'''                                            
        and ''' + convert(varchar(20),@dtEndDate)+''' and Comm.numCreatedBy =ADC1.numContactId) as NoofOpp FROM  AdditionalContactsInformation ADC1                                           
WHERE  ADC1.numContactId in ('+@strUserIDs+')'                                        
                         
                                        
if @Condition='C'   set @strSql=@strSql +' where bitTask=' + convert(varchar(20),@bitTask)+' and Comm. dtCreatedDate between ''' + convert(varchar(20),@dtStartDate)+'''                                            
 and ''' + convert(varchar(20),@dtEndDate)+''' and Comm.bitClosedFlag=1 and Comm.numCreatedBy =ADC1.numContactId) as NoofOpp FROM  AdditionalContactsInformation ADC1                                            
WHERE  ADC1.numContactId in ('+@strUserIDs+')'                                           
                                        
                                        
 if @Condition='P'  set @strSql=@strSql +' where Comm.bitTask=' + convert(varchar(20),@bitTask)+'               
 and dtStartTime< ''' + convert(varchar(20),getutcdate())+'''                                           
 and Comm.dtCreatedDate between ''' + convert(varchar(20),@dtStartDate)+''' and ''' + convert(varchar(20),@dtEndDate)+'''                                           
 and Comm.bitClosedFlag=0 and Comm.numCreatedBy =ADC1.numContactId) as NoofOpp FROM  AdditionalContactsInformation ADC1                                            
 WHERE  ADC1.numContactId in ('+@strUserIDs+')'                                           
                                        
print @strSql                                        
exec (@strSql)                                        
end
GO
