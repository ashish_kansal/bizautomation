/****** Object:  StoredProcedure [dbo].[USP_GetTotalBankBalance]    Script Date: 07/26/2008 16:18:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created By Siva
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_gettotalbankbalance')
DROP PROCEDURE usp_gettotalbankbalance
GO
CREATE PROCEDURE [dbo].[USP_GetTotalBankBalance]
(@numDomainId as numeric(9),
@monTotalBankBal as DECIMAL(20,5) output)
As
Begin
	Declare @monDebitBankAmt as DECIMAL(20,5)        
	Declare @monCreditBankAmt as DECIMAL(20,5)  

	Select @monDebitBankAmt=sum(isnull(GJD.numDebitAmt,0)), @monCreditBankAmt=sum(isnull(GJD.numCreditAmt,0)) From General_Journal_Header GJH          
	inner join General_Journal_Details GJD on GJH.numJournal_Id=GJD.numJournalId          
	inner join Chart_of_Accounts CA on GJD.numChartAcntId = CA.numAccountId           
	Where GJH.numDomainId=@numDomainId And CA.numAcntTypeID = 813 And datediff(day,getutcdate(),GJH.datEntry_Date)<=0 And Year(GJH.datEntry_Date)=Year(getutcdate()) 
	set @monTotalBankBal=isnull(@monDebitBankAmt,0)-isnull(@monCreditBankAmt,0)
End
GO
