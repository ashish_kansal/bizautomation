/****** Object:  StoredProcedure [dbo].[USP_PreDefinedReptLinks]    Script Date: 07/26/2008 16:20:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_predefinedreptlinks')
DROP PROCEDURE usp_predefinedreptlinks
GO
CREATE PROCEDURE [dbo].[USP_PreDefinedReptLinks]
@rpTGroup as numeric(9)=0
as
select RPTHeading,RptDesc,vcFileName from ReportList
join PageMaster
on numPageID=NumID
where rptGroup=@rpTGroup and numModuleID=8 AND [bitEnable]=1
order by rptSequence
GO
