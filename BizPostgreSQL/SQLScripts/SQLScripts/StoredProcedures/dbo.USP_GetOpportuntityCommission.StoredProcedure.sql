GO
/****** Object:  StoredProcedure [dbo].[USP_GetOpportuntityCommission]    Script Date: 02/28/2009 13:57:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created by Siva
-- [dbo].[USP_GetOpportuntityCommission] 1,1,1,-333,'1/1/2009','1/15/2009'
-- exec USP_GetOpportuntityCommission @numUserId=372,@numUserCntID=82979,@numDomainId=1,@ClientTimeZoneOffset=-330,@dtStartDate='2010-03-01 00:00:00:000',@dtEndDate='2010-04-01 00:00:00:000'
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getopportuntitycommission')
DROP PROCEDURE usp_getopportuntitycommission
GO
CREATE PROCEDURE [dbo].[USP_GetOpportuntityCommission]
(
	@numUserId AS NUMERIC(18,0)  = 0,
	@numUserCntID AS NUMERIC(18,0)  = 0,
	@numDomainId AS NUMERIC(18,0)  = 0,
	@numComPayPeriodID NUMERIC(18,0),
	@ClientTimeZoneOffset AS INT,
	@numOppBizDocsId AS NUMERIC(9) =0,
	@tintMode AS tinyint,
	@numComRuleID AS NUMERIC(18,0) = NULL,
	@numDivisionID NUMERIC(18,0) = NULL
)
AS
BEGIN
	DECLARE @bitIncludeShippingItem AS BIT
	DECLARE @numShippingItemID AS NUMERIC(18,0)
	DECLARE @numDiscountItemID AS NUMERIC(18,0)
	DECLARE @tintCommissionType TINYINT
	
	SELECT 
		@bitIncludeShippingItem=bitIncludeTaxAndShippingInCommission
		,@numShippingItemID=numShippingServiceItemID
		,@numDiscountItemID=ISNULL(numDiscountServiceItemID,0)
		,@tintCommissionType=tintCommissionType
	FROM 
		Domain 
	WHERE 
		numDomainID = @numDomainId


	IF @tintMode=0  -- Paid Invoice
	BEGIN
		SELECT
			CompanyInfo.vcCompanyName,
			LD.vcData as BizDoc,
			OpportunityBizDocs.numBizDocID,
			OpportunityMaster.numOppId,
			OpportunityBizDocs.numOppBizDocsId,
			ISNULL((SELECT SUM(monOrderSubTotal) FROM (SELECT DISTINCT BDCInner.numOppItemID,BDCInner.numOppBizDocItemID,BDCInner.monOrderSubTotal FROM BizDocComission BDCInner WHERE BDCInner.numOppID=OpportunityMaster.numOppId AND BDCInner.numOppBizDocId= OpportunityBizDocs.numOppBizDocsId GROUP BY BDCInner.numOppItemID,BDCInner.numOppBizDocItemID,BDCInner.monOrderSubTotal) TEMP),0) AS monSOSubTotal,
			isnull(OpportunityBizDocs.monAmountPaid,0) monAmountPaid,
			TEMPDEPOSIT.dtDepositDate AS bintCreatedDate,
			OpportunityMaster.vcPOppName AS Name,
			Case when OpportunityMaster.tintOppStatus=0 then 'Open' Else 'Close' End as OppStatus, 
			(CASE WHEN ISNULL(OpportunityMaster.fltExchangeRate,0)=0 OR ISNULL(OpportunityMaster.fltExchangeRate,0)=1 THEN ISNULL(OpportunityBizDocs.monDealAmount,0) ELSE ROUND(ISNULL(OpportunityBizDocs.monDealAmount,0) * OpportunityMaster.fltExchangeRate,2) END) as DealAmount,
			Case When (OpportunityMaster.numRecOwner=@numUserCntID and OpportunityMaster.numAssignedTo=@numUserCntID) then  'Deal Owner/Assigned'
					When OpportunityMaster.numRecOwner=@numUserCntID then 'Deal Owner' 
					When OpportunityMaster.numAssignedTo=@numUserCntID then 'Deal Assigned' 
			end as EmpRole
			,SUM(ISNULL(numComissionAmount,0)) as decTotalCommission,
			OpportunityBizDocs.vcBizDocID,
			ISNULL((SELECT SUM(monInvoiceSubTotal) FROM (SELECT DISTINCT BDCInner.numOppItemID,BDCInner.numOppBizDocItemID,BDCInner.monInvoiceSubTotal FROM BizDocComission BDCInner WHERE BDCInner.numOppID=OpportunityMaster.numOppId AND BDCInner.numOppBizDocId= OpportunityBizDocs.numOppBizDocsId GROUP BY BDCInner.numOppItemID,BDCInner.numOppBizDocItemID,BDCInner.monInvoiceSubTotal) TEMP),0) AS monSubTotAMount
		FROM
			BizDocComission
		INNER JOIN
			OpportunityBizDocs
		ON
			BizDocComission.numOppBizDocId = OpportunityBizDocs.numOppBizDocsId
		INNER JOIN
			OpportunityMaster
		ON
			BizDocComission.numOppID = OpportunityMaster.numOppId
		INNER JOIN 
			DivisionMaster 
		ON 
			OpportunityMaster.numDivisionId = DivisionMaster.numDivisionID
		INNER JOIN 
			CompanyInfo 
		ON 
			DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
		LEFT JOIN 
			ListDetails LD 
		ON 
			LD.numListItemID = OpportunityBizDocs.numBizDocId
		OUTER APPLY
		(
			SELECT 
				MAX(DM.dtDepositDate) dtDepositDate
			FROM 
				DepositMaster DM 
			JOIN dbo.DepositeDetails DD	ON DM.numDepositId=DD.numDepositID 
			WHERE 
				DM.tintDepositePage=2 
				AND DM.numDomainId=@numDomainId
				AND DD.numOppID=OpportunityBizDocs.numOppID 
				AND DD.numOppBizDocsID =OpportunityBizDocs.numOppBizDocsID
		) TEMPDEPOSIT
		WHERE
			numComPayPeriodID=@numComPayPeriodID
			AND ((BizDocComission.numUserCntID = @numUserCntID AND ISNULL(tintAssignTo,0) <> 3) OR (BizDocComission.numUserCntID = @numDivisionID AND tintAssignTo = 3))
			AND ISNULL(OpportunityBizDocs.monAmountPaid,0) >= OpportunityBizDocs.monDealAmount
			AND (ISNULL(@numComRuleID,0)=0 OR BizDocComission.numComRuleID=@numComRuleID)
		GROUP BY
			CompanyInfo.vcCompanyName
			,LD.vcData
			,OpportunityBizDocs.numBizDocID
			,OpportunityMaster.numOppId
			,OpportunityBizDocs.numOppBizDocsId
			,OpportunityMaster.fltExchangeRate
			,OpportunityMaster.monDealAmount
			,OpportunityMaster.vcPOppName
			,OpportunityBizDocs.monAmountPaid
			,OpportunityMaster.tintOppStatus
			,OpportunityBizDocs.monDealAmount
			,OpportunityMaster.numRecOwner
			,OpportunityMaster.numAssignedTo
			,OpportunityBizDocs.vcBizDocID
			,OpportunityBizDocs.dtCreatedDate
			,TEMPDEPOSIT.dtDepositDate
		ORDER BY
			OpportunityBizDocs.dtCreatedDate
	END
	ELSE IF @tintMode=1 -- Non Paid Invoice
	BEGIN
		SELECT
			CompanyInfo.vcCompanyName,
			LD.vcData as BizDoc,
			OpportunityBizDocs.numBizDocID,
			OpportunityMaster.numOppId,
			OpportunityBizDocs.numOppBizDocsId,
			ISNULL((SELECT SUM(monOrderSubTotal) FROM (SELECT DISTINCT BDCInner.numOppItemID,BDCInner.numOppBizDocItemID,BDCInner.monOrderSubTotal FROM BizDocComission BDCInner WHERE BDCInner.numOppID=OpportunityMaster.numOppId AND BDCInner.numOppBizDocId= OpportunityBizDocs.numOppBizDocsId GROUP BY BDCInner.numOppItemID,BDCInner.numOppBizDocItemID,BDCInner.monOrderSubTotal) TEMP),0) AS monSOSubTotal,
			isnull(OpportunityBizDocs.monAmountPaid,0) monAmountPaid,
			DATEADD(MINUTE,@ClientTimeZoneOffset * -1,OpportunityBizDocs.dtCreatedDate) AS bintCreatedDate,
			OpportunityMaster.vcPOppName AS Name,
			Case when OpportunityMaster.tintOppStatus=0 then 'Open' Else 'Close' End as OppStatus, 
			(CASE WHEN ISNULL(OpportunityMaster.fltExchangeRate,0)=0 OR ISNULL(OpportunityMaster.fltExchangeRate,0)=1 THEN ISNULL(OpportunityBizDocs.monDealAmount,0) ELSE ROUND(ISNULL(OpportunityBizDocs.monDealAmount,0) * OpportunityMaster.fltExchangeRate,2) END) as DealAmount,
			Case When (OpportunityMaster.numRecOwner=@numUserCntID and OpportunityMaster.numAssignedTo=@numUserCntID) then  'Deal Owner/Assigned'
					When OpportunityMaster.numRecOwner=@numUserCntID then 'Deal Owner' 
					When OpportunityMaster.numAssignedTo=@numUserCntID then 'Deal Assigned' 
			end as EmpRole
			,SUM(ISNULL(numComissionAmount,0)) as decTotalCommission,
			OpportunityBizDocs.vcBizDocID,
			ISNULL((SELECT SUM(monInvoiceSubTotal) FROM (SELECT DISTINCT BDCInner.numOppItemID,BDCInner.numOppBizDocItemID,BDCInner.monInvoiceSubTotal FROM BizDocComission BDCInner WHERE BDCInner.numOppID=OpportunityMaster.numOppId AND BDCInner.numOppBizDocId= OpportunityBizDocs.numOppBizDocsId GROUP BY BDCInner.numOppItemID,BDCInner.numOppBizDocItemID,BDCInner.monInvoiceSubTotal) TEMP),0) AS monSubTotAMount
		FROM
			BizDocComission
		INNER JOIN
			OpportunityBizDocs
		ON
			BizDocComission.numOppBizDocId = OpportunityBizDocs.numOppBizDocsId
		INNER JOIN
			OpportunityMaster
		ON
			BizDocComission.numOppID = OpportunityMaster.numOppId
		INNER JOIN 
			DivisionMaster 
		ON 
			OpportunityMaster.numDivisionId = DivisionMaster.numDivisionID
		INNER JOIN 
			CompanyInfo 
		ON 
			DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
		LEFT JOIN 
			ListDetails LD 
		ON 
			LD.numListItemID = OpportunityBizDocs.numBizDocId
		WHERE
			numComPayPeriodID=@numComPayPeriodID
			AND ((BizDocComission.numUserCntID = @numUserCntID AND ISNULL(tintAssignTo,0) <> 3) OR (BizDocComission.numUserCntID = @numDivisionID AND tintAssignTo = 3))
			AND ISNULL(OpportunityBizDocs.monAmountPaid,0) < OpportunityBizDocs.monDealAmount
			AND (ISNULL(@numComRuleID,0)=0 OR BizDocComission.numComRuleID=@numComRuleID)
		GROUP BY
			CompanyInfo.vcCompanyName
			,LD.vcData
			,OpportunityBizDocs.numBizDocID
			,OpportunityMaster.numOppId
			,OpportunityBizDocs.numOppBizDocsId
			,OpportunityMaster.fltExchangeRate
			,OpportunityMaster.monDealAmount
			,OpportunityMaster.vcPOppName
			,OpportunityBizDocs.monAmountPaid
			,OpportunityMaster.tintOppStatus
			,OpportunityBizDocs.monDealAmount
			,OpportunityMaster.numRecOwner
			,OpportunityMaster.numAssignedTo
			,OpportunityBizDocs.vcBizDocID
			,OpportunityBizDocs.dtCreatedDate
		ORDER BY
			OpportunityBizDocs.dtCreatedDate
  END
	ELSE IF @tintMode=2  -- Paid Invoice Detail
	BEGIN
        SELECT * FROM (Select CI.vcCompanyName,BC.numComissionID,oppBiz.numOppBizDocsId,BC.decCommission,Case when BC.tintComBasedOn = 1 then 'Amount' when BC.tintComBasedOn = 2 then 'Units' end AS BasedOn,
			Case when BC.tintComType = 1 then 'Percentage' when BC.tintComType = 2 then 'Flat' end as CommissionType, 
                        numComissionAmount  as CommissionAmt,I.vcItemName,BDI.numUnitHour
						,(CASE WHEN ISNULL(Opp.fltExchangeRate,0)=0 OR ISNULL(Opp.fltExchangeRate,0)=1 THEN ISNULL(BDI.monPrice,0) ELSE ROUND(ISNULL(BDI.monPrice,0) * Opp.fltExchangeRate,2) END) monPrice
						,(CASE WHEN ISNULL(Opp.fltExchangeRate,0)=0 OR ISNULL(Opp.fltExchangeRate,0)=1 THEN ISNULL(BDI.monTotAmount,0) ELSE ROUND(ISNULL(BDI.monTotAmount,0) * Opp.fltExchangeRate,2) END) monTotAmount
						,ISNULL(OT.monVendorCost, 0)* ISNULL(BDI.[numUnitHour],0) AS VendorCost,
						ISNULL(OT.monAvgCost, 0)* ISNULL(BDI.[numUnitHour],0) AS monAvgCost         
                              From OpportunityMaster Opp 
							  INNER JOIN DivisionMaster DM ON Opp.numDivisionId = DM.numDivisionID
							  INNER JOIN CompanyInfo CI ON DM.numCompanyID = CI.numCompanyId
							  INNER JOIN OpportunityBizDocs oppBiz on oppBiz.numOppId=opp.numOppId 
                              INNER JOIN BizDocComission BC on BC.numOppBizDocId=oppBiz.numOppBizDocsId and ((BC.numUserCntID = @numUserCntID AND ISNULL(BC.tintAssignTo,0) <> 3) OR (BC.numUserCntID = @numDivisionID AND BC.tintAssignTo = 3))
                              INNER JOIN OpportunityBizDocItems BDI ON oppBiz.numOppBizDocsId=BDI.numOppBizDocID AND BC.numOppBizDocItemID=BDI.numOppBizDocItemID
							  INNER JOIN OpportunityItems OT ON OT.numOppItemtCode=BDI.numOppItemID
							  INNER JOIN Item I ON BDI.numItemCode=I.numItemCode
                              Where oppBiz.bitAuthoritativeBizDocs = 1 and Opp.tintOppStatus=1 And Opp.tintOppType=1 And (Opp.numRecOwner=@numUserCntID OR Opp.numAssignedTo=@numUserCntID OR Opp.numPartner=@numDivisionID)
                              And Opp.numDomainId=@numDomainId 
							  AND BC.numComPayPeriodID = @numComPayPeriodID 
							  AND oppBiz.numOppBizDocsId=@numOppBizDocsId
							  AND (ISNULL(@numComRuleID,0)=0 OR BC.numComRuleID=@numComRuleID)
							  AND oppBiz.bitAuthoritativeBizDocs = 1 AND 
							  1=(CASE WHEN @tintMode=2 THEN CASE WHEN isnull(oppBiz.monAmountPaid,0) >=oppBiz.monDealAmount THEN 1 ELSE 0 END
							       WHEN @tintMode=3 THEN CASE WHEN isnull(oppBiz.monAmountPaid,0) <oppBiz.monDealAmount THEN 1 ELSE 0 END END)) A					     
    END
    ELSE IF @tintMode=3 -- Non Paid Invoice Detail
	BEGIN
        Select CI.vcCompanyName,BC.numComissionID,oppBiz.numOppBizDocsId,BC.decCommission,Case when BC.tintComBasedOn = 1 then 'Amount' when BC.tintComBasedOn = 2 then 'Units' end AS BasedOn,
			Case when BC.tintComType = 1 then 'Percentage' when BC.tintComType = 2 then 'Flat' end as CommissionType, 
                        numComissionAmount  as CommissionAmt,I.vcItemName,BDI.numUnitHour
						,(CASE WHEN ISNULL(Opp.fltExchangeRate,0)=0 OR ISNULL(Opp.fltExchangeRate,0)=1 THEN ISNULL(BDI.monPrice,0) ELSE ROUND(ISNULL(BDI.monPrice,0) * Opp.fltExchangeRate,2) END) monPrice
						,(CASE WHEN ISNULL(Opp.fltExchangeRate,0)=0 OR ISNULL(Opp.fltExchangeRate,0)=1 THEN ISNULL(BDI.monTotAmount,0) ELSE ROUND(ISNULL(BDI.monTotAmount,0) * Opp.fltExchangeRate,2) END) monTotAmount
						,ISNULL(OT.monVendorCost, 0)* ISNULL(BDI.[numUnitHour],0) AS VendorCost,
						ISNULL(OT.monAvgCost, 0)* ISNULL(BDI.[numUnitHour],0) AS monAvgCost       
                              From OpportunityMaster Opp 
							  INNER JOIN DivisionMaster DM ON Opp.numDivisionId = DM.numDivisionID
							  INNER JOIN CompanyInfo CI ON DM.numCompanyID = CI.numCompanyId
							  INNER JOIN OpportunityBizDocs oppBiz on oppBiz.numOppId=opp.numOppId 
                              INNER JOIN BizDocComission BC on BC.numOppBizDocId=oppBiz.numOppBizDocsId and ((BC.numUserCntID = @numUserCntID AND ISNULL(BC.tintAssignTo,0) <> 3) OR (BC.numUserCntID = @numDivisionID AND BC.tintAssignTo = 3))
                              INNER JOIN OpportunityBizDocItems BDI ON oppBiz.numOppBizDocsId=BDI.numOppBizDocID AND BC.numOppBizDocItemID=BDI.numOppBizDocItemID
							  INNER JOIN OpportunityItems OT ON OT.numOppItemtCode=BDI.numOppItemID
							  INNER JOIN Item I ON BDI.numItemCode=I.numItemCode
                              Where oppBiz.bitAuthoritativeBizDocs = 1 and Opp.tintOppStatus=1 And Opp.tintOppType=1 And (Opp.numRecOwner=@numUserCntID OR Opp.numAssignedTo=@numUserCntID OR Opp.numPartner=@numDivisionID)
                              And Opp.numDomainId=@numDomainId 
							  AND BC.numComPayPeriodID = @numComPayPeriodID
							  AND oppBiz.numOppBizDocsId=@numOppBizDocsId
							  AND (ISNULL(@numComRuleID,0)=0 OR BC.numComRuleID=@numComRuleID)
							  AND oppBiz.bitAuthoritativeBizDocs = 1 AND 
							  1=(CASE WHEN @tintMode=2 THEN CASE WHEN isnull(oppBiz.monAmountPaid,0) >=oppBiz.monDealAmount THEN 1 ELSE 0 END
							       WHEN @tintMode=3 THEN CASE WHEN isnull(oppBiz.monAmountPaid,0) <oppBiz.monDealAmount THEN 1 ELSE 0 END END)
    END
	--ELSE IF @tintMode = 4
	--BEGIN
	--	SELECT
	--		vcCompanyName
	--		,'' AS BizDoc
	--		,0 numBizDocID
	--		,OpportunityMaster.numOppId
	--		,0 AS numOppBizDocsId
	--		,ISNULL(OpportunityMaster.monDealAmount,0) AS monSOSubTotal
	--		,CAST(ISNULL(TableGrossProfit.monGPVendorCost,0) AS DECIMAL(20,5)) AS monGPVendorCost
	--		,CAST(ISNULL(TableGrossProfit.monGPAverageCost,0) AS DECIMAL(20,5)) AS monGPAevrageCost
	--		,isnull(OpportunityMaster.monPAmount,0) monAmountPaid
	--		,DATEADD(MINUTE,@ClientTimeZoneOffset * -1,OpportunityMaster.bintCreatedDate) AS bintCreatedDate
	--		,OpportunityMaster.vcPOppName AS Name
	--		,Case when OpportunityMaster.tintOppStatus=0 then 'Open' Else 'Close' End as OppStatus
	--		,isnull(OpportunityMaster.monDealAmount,0) as DealAmount
	--		,Case When (OpportunityMaster.numRecOwner=@numUserCntID and OpportunityMaster.numAssignedTo=@numUserCntID) then  'Deal Owner/Assigned'
	--				When OpportunityMaster.numRecOwner=@numUserCntID then 'Deal Owner' 
	--				When OpportunityMaster.numAssignedTo=@numUserCntID then 'Deal Assigned' 
	--		end as EmpRole
	--		,SUM(ISNULL(numComissionAmount,0)) as decTotalCommission
	--		,'' vcBizDocID
	--		,SUM(ISNULL(monOrderSubTotal,0)) AS monSubTotAMount
	--	FROM
	--		BizDocComission
	--	INNER JOIN
	--		OpportunityMaster
	--	ON
	--		BizDocComission.numOppID = OpportunityMaster.numOppId
	--	INNER JOIN 
	--		DivisionMaster 
	--	ON 
	--		OpportunityMaster.numDivisionId = DivisionMaster.numDivisionID
	--	INNER JOIN 
	--		CompanyInfo 
	--	ON 
	--		DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
	--	OUTER APPLY
	--	(
	--		SELECT
	--			SUM(ISNULL(monTotAmount,0) - (ISNULL(numUnitHour,0) * ISNULL(Vendor.monCost,0) * dbo.fn_UOMConversion(Item.numBaseUnit,Item.numItemCode,@numDomainID,Item.numPurchaseUnit))) monGPVendorCost
	--			,SUM(ISNULL(monTotAmount,0) - (ISNULL(numUnitHour,0) * ISNULL(monAverageCost,0))) monGPAverageCost
	--		FROM
	--			OpportunityItems
	--		INNER JOIN
	--			Item
	--		ON
	--			OpportunityItems.numItemCode = Item.numItemCode
	--		LEFT JOIN
	--			Vendor
	--		ON
	--			Item.numVendorID = Vendor.numVendorID
	--			AND Item.numItemCode = Vendor.numItemCode
	--		WHERE
	--			OpportunityItems.numOppId = OpportunityMaster.numOppId
	--			AND ISNULL(Item.bitContainer,0) = 0
	--			AND 1  = (CASE WHEN @bitIncludeShippingItem=1 THEN 1 ELSE (CASE WHEN Item.numItemCode = ISNULL(@numShippingItemID,0) THEN 0 ELSE 1 END) END)
	--			AND Item.numItemCode <> ISNULL(@numDiscountItemID,0)
	--	) AS TableGrossProfit
	--	WHERE
	--		numComPayPeriodID=@numComPayPeriodID
	--	GROUP BY
	--		OpportunityMaster.numOppId
	--		,OpportunityMaster.vcPOppName
	--		,OpportunityMaster.monDealAmount
	--		,OpportunityMaster.monPAmount
	--		,OpportunityMaster.tintOppStatus
	--		,OpportunityMaster.numRecOwner
	--		,OpportunityMaster.numAssignedTo
	--		,OpportunityMaster.bintCreatedDate
	--		,CompanyInfo.vcCompanyName
	--		,TableGrossProfit.monGPVendorCost
	--		,TableGrossProfit.monGPAverageCost
	--	ORDER BY
	--		OpportunityMaster.bintCreatedDate
	--END
END
GO