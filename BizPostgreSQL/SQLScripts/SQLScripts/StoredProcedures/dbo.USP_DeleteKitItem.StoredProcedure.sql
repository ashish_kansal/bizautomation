/****** Object:  StoredProcedure [dbo].[USP_DeleteKitItem]    Script Date: 07/26/2008 16:15:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_deletekititem')
DROP PROCEDURE usp_deletekititem
GO
CREATE PROCEDURE [dbo].[USP_DeleteKitItem]     
@numItemDetailID as NUMERIC(18,0) 
as    
     
DELETE FROM ItemDetails where numItemDetailID = @numItemDetailID

GO
