/****** Object:  StoredProcedure [dbo].[USP_GetCaseList1]    Script Date: 07/26/2008 16:16:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--  By Anoop Jayaraj                                                            
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getcaselist1')
DROP PROCEDURE usp_getcaselist1
GO
CREATE PROCEDURE [dbo].[USP_GetCaseList1]                                                               
	 @numUserCntId numeric(9)=0,                                            
	 @tintSortOrder numeric=0,                                                                                
	 @numDomainID numeric(9)=0,                                                            
	 @tintUserRightType tinyint,                                                            
	 @SortChar char(1)='0' ,                                                                                                                   
	 @CurrentPage int,                                                            
	 @PageSize int,                                                            
	 @TotRecs int output,                                                            
	 @columnName as Varchar(50),                                                            
	 @columnSortOrder as Varchar(10),                                                          
	 @numDivisionID as numeric(9)=0,                              
	 @bitPartner as bit=0,
	 @vcRegularSearchCriteria varchar(1000)='',
	 @vcCustomSearchCriteria varchar(1000)='' ,
	 @ClientTimeZoneOffset as INT,                                                          
	 @numStatus AS BIGINT = 0,
	 @SearchText VARCHAR(MAX) = ''
AS        
	
	CREATE TABLE #tempForm (tintOrder TINYINT,vcDbColumnName nvarchar(200),vcOrigDbColumnName nvarchar(50),vcFieldName nvarchar(50),vcAssociatedControlType nvarchar(50),
  vcListItemType varchar(3),numListID numeric(9),vcLookBackTableName varchar(50),bitCustomField BIT,
  numFieldId NUMERIC,bitAllowSorting BIT,bitAllowEdit BIT,bitIsRequired BIT,bitIsEmail BIT,bitIsAlphaNumeric BIT,bitIsNumeric BIT,
  bitIsLengthValidation BIT,intMaxLength int,intMinLength int,bitFieldMessage BIT,vcFieldMessage VARCHAR(500),ListRelID numeric(9),intColumnWidth int,bitAllowFiltering BIT,vcFieldDataType CHAR(1))


	DECLARE @Nocolumns AS TINYINT                
	SET @Nocolumns=0                
 
	SELECT @Nocolumns=isnull(sum(TotalRow),0)  FROM(            
	SELECT COUNT(*) TotalRow from View_DynamicColumns where numFormId=12 and numUserCntID=@numUserCntID and numDomainID=@numDomainID AND tintPageType=1
	UNION 
	SELECT count(*) TotalRow from View_DynamicCustomColumns where numFormId=12 and numUserCntID=@numUserCntID and numDomainID=@numDomainID AND tintPageType=1) TotalRows

	--Check if Master Form Configuration is created by administrator if NoColumns=0
	DECLARE @IsMasterConfAvailable BIT = 0
	DECLARE @numUserGroup NUMERIC(18,0)

	IF @Nocolumns=0
	BEGIN
		SELECT @numUserGroup = numGroupID FROM UserMaster WHERE numUserDetailId = @numUserCntID 

		IF (SELECT COUNT(*) FROM BizFormWizardMasterConfiguration WHERE numDomainID = @numDomainID AND numFormID = 12 AND bitGridConfiguration = 1 AND numGroupID = @numUserGroup) > 0
		BEGIN
			SET @IsMasterConfAvailable = 1
		END
	END


	--If MasterConfiguration is available then load it otherwise load default columns
	IF @IsMasterConfAvailable = 1
	BEGIN
		INSERT INTO DycFormConfigurationDetails 
		(
			numFormID,numFieldID,intColumnNum,intRowNum,numDomainID,numUserCntID,numRelCntType,tintPageType,bitCustom,intColumnWidth
		)
		SELECT 
			12,numFieldId,intColumnNum,intRowNum,@numDomainID,@numUserCntID,NULL,1,0,intColumnWidth
		FROM 
			View_DynamicColumnsMasterConfig
		WHERE
			View_DynamicColumnsMasterConfig.numFormId=12 AND 
			View_DynamicColumnsMasterConfig.numDomainID=@numDomainID AND 
			View_DynamicColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
			View_DynamicColumnsMasterConfig.bitGridConfiguration = 1 AND
			ISNULL(View_DynamicColumnsMasterConfig.bitSettingField,0)=1 AND 
			ISNULL(View_DynamicColumnsMasterConfig.bitCustom,0)=0
		UNION
		SELECT 
			12,numFieldId,intColumnNum,intRowNum,@numDomainID,@numUserCntID,NULL,1,1,intColumnWidth
		FROM 
			View_DynamicCustomColumnsMasterConfig
		WHERE 
			View_DynamicCustomColumnsMasterConfig.numFormId=12 AND 
			View_DynamicCustomColumnsMasterConfig.numDomainID=@numDomainID AND 
			View_DynamicCustomColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
			View_DynamicCustomColumnsMasterConfig.bitGridConfiguration = 1 AND
			ISNULL(View_DynamicCustomColumnsMasterConfig.bitCustom,0)=1

	INSERT INTO #tempForm
	SELECT 
		(intRowNum + 1) as tintOrder, vcDbColumnName,	vcOrigDbColumnName,	ISNULL(vcCultureFieldName,vcFieldName), vcAssociatedControlType,
		vcListItemType, numListID, vcLookBackTableName,	bitCustom, numFieldId, bitAllowSorting,	bitInlineEdit, bitIsRequired,
		bitIsEmail,	bitIsAlphaNumeric, bitIsNumeric, bitIsLengthValidation,	intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage vcFieldMessage,
		ListRelID,intColumnWidth,bitAllowFiltering,vcFieldDataType 
	FROM 
		View_DynamicColumnsMasterConfig
	WHERE
		View_DynamicColumnsMasterConfig.numFormId=12 AND 
		View_DynamicColumnsMasterConfig.numDomainID=@numDomainID AND 
		View_DynamicColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
		View_DynamicColumnsMasterConfig.bitGridConfiguration = 1 AND
		ISNULL(View_DynamicColumnsMasterConfig.bitSettingField,0)=1 AND 
		ISNULL(View_DynamicColumnsMasterConfig.bitCustom,0)=0
	UNION
    SELECT 
		tintRow+1 as tintOrder,vcDbColumnName,vcDbColumnName,vcFieldName, vcAssociatedControlType,'' as vcListItemType,numListID,'',
		bitCustom,numFieldId,bitAllowSorting,bitAllowEdit,bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
		intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage,ListRelID,intColumnWidth,0,''
	FROM 
		View_DynamicCustomColumnsMasterConfig
	WHERE 
		View_DynamicCustomColumnsMasterConfig.numFormId=12 AND 
		View_DynamicCustomColumnsMasterConfig.numDomainID=@numDomainID AND 
		View_DynamicCustomColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
		View_DynamicCustomColumnsMasterConfig.bitGridConfiguration = 1 AND
		ISNULL(View_DynamicCustomColumnsMasterConfig.bitCustom,0)=1
	ORDER BY 
		tintOrder ASC  
	END
	ELSE                                        
	BEGIN
	if @Nocolumns=0
	BEGIN
		INSERT INTO DycFormConfigurationDetails (numFormID,numFieldID,intColumnNum,intRowNum,numDomainID,numUserCntID,numRelCntType,tintPageType,bitCustom,intColumnWidth)
		select 12,numFieldId,0,Row_number() over(order by tintRow desc),@numDomainID,@numUserCntID,NULL,1,0,intColumnWidth
		 FROM View_DynamicDefaultColumns
		 where numFormId=12 and bitDefault=1 AND ISNULL(bitSettingField,0)=1 and numDomainID=@numDomainID
		order by tintOrder asc      
	END

	INSERT INTO #tempForm
	select tintRow+1 as tintOrder,vcDbColumnName,vcOrigDbColumnName,ISNULL(vcCultureFieldName,vcFieldName),                  
	 vcAssociatedControlType,vcListItemType,numListID,vcLookBackTableName                                                 
	,bitCustom,numFieldId,bitAllowSorting,bitInlineEdit,
	bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
	intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage vcFieldMessage,ListRelID,intColumnWidth,bitAllowFiltering,vcFieldDataType  
	 FROM View_DynamicColumns 
	 where numFormId=12 and numUserCntID=@numUserCntID and numDomainID=@numDomainID AND tintPageType=1 
	  AND ISNULL(bitSettingField,0)=1 AND ISNULL(bitCustom,0)=0
       
		   UNION
    
		   select tintRow+1 as tintOrder,vcDbColumnName,vcDbColumnName,vcFieldName,                  
	 vcAssociatedControlType,'' as vcListItemType,numListID,''                                                 
	,bitCustom,numFieldId,bitAllowSorting,bitAllowEdit,
	bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
	intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage,ListRelID,intColumnWidth,0,''
	 from View_DynamicCustomColumns
	where numFormId=12 and numUserCntID=@numUserCntID and numDomainID=@numDomainID AND tintPageType=1 
	 AND ISNULL(bitCustom,0)=1
 
	  order by tintOrder asc  
  
END     

	DECLARE  @strColumns AS VARCHAR(MAX)

	SET @strColumns=' ADC.numContactId,DM.numDivisionID,isnull(DM.numTerID,0) numTerID,cs.numRecOwner,ISNULL(cs.numAssignedTo,0) AS numAssignedTo,DM.tintCRMType,cs.numCaseID,cs.vcCaseNumber,ADC.vcFirstName+'' ''+ADC.vcLastName AS vcContactName,ADC.numPhone AS numContactPhone,ADC.numPhoneExtension AS numContactPhoneExtension,ADC.vcEmail AS vcContactEmail '                

	DECLARE @tintOrder as tinyint                                                  
	DECLARE @vcFieldName as varchar(50)                                                  
	DECLARE @vcListItemType as varchar(3)                                             
	DECLARE @vcListItemType1 as varchar(1)                                                 
	DECLARE @vcAssociatedControlType varchar(20)                                                  
	DECLARE @numListID AS numeric(9)                                                  
	DECLARE @vcDbColumnName varchar(200)                      
	DECLARE @WhereCondition varchar(2000)                       
	DECLARE @vcLookBackTableName varchar(2000)                
	DECLARE @bitCustom as bit          
	DECLARE @numFieldId as numeric  
	DECLARE @bitAllowSorting AS CHAR(1)   
	DECLARE @bitAllowEdit AS CHAR(1)                 
	DECLARE @vcColumnName AS VARCHAR(500)                  
    DECLARE @SearchQuery AS VARCHAR(MAX) = ''            
	SET @tintOrder=0                                                  
	SET @WhereCondition =''    
	--set @DefaultNocolumns=  @Nocolumns              
	Declare @ListRelID as numeric(9) 

	  SELECT TOP 1 
			@tintOrder=tintOrder+1,@vcDbColumnName=vcDbColumnName,@vcFieldName=vcFieldName,                
			@vcAssociatedControlType=vcAssociatedControlType,@vcListItemType=vcListItemType,@numListID=numListID,@vcLookBackTableName=vcLookBackTableName,                                               
			@bitCustom=bitCustomField,@numFieldId=numFieldId,@bitAllowSorting=bitAllowSorting,@bitAllowEdit=bitAllowEdit,@ListRelID=ListRelID
	  FROM  
			#tempForm 
	  ORDER BY 
			tintOrder 
	  ASC            
                                        
WHILE @tintOrder>0                                
BEGIN                                                  
 if @bitCustom = 0        
 begin        
    declare @Prefix as varchar(5)                
      if @vcLookBackTableName = 'AdditionalContactsInformation'                
    set @Prefix = 'ADC.'                
      if @vcLookBackTableName = 'DivisionMaster'                
    set @Prefix = 'DM.'                
      if @vcLookBackTableName = 'Cases'                
    set @PreFix ='Cs.'      
    
    SET @vcColumnName= @vcDbColumnName+'~'+ convert(varchar(10),@numFieldId) +'~0'
    
     if @vcAssociatedControlType='SelectBox'                                                  
     begin                                                  
                                     
     if @vcListItemType='LI'                                                   
     begin                                                  
      set @strColumns=@strColumns+',L'+ convert(varchar(3),@tintOrder)+'.vcData'+' ['+ @vcColumnName+']'
	    IF LEN(@SearchText) > 0
		  BEGIN 
			SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + 'L' + CONVERT(VARCHAR(3),@tintOrder) + '.vcData LIKE ''%' + @SearchText + '%'' '     
		  END                                                     
      set @WhereCondition= @WhereCondition +' left Join ListDetails L'+ convert(varchar(3),@tintOrder)+ ' on L'+convert(varchar(3),@tintOrder)+ '.numListItemID='+@vcDbColumnName                                                  
     end                                                  
     else if @vcListItemType='S'                                                   
     begin                                                  
      set @strColumns=@strColumns+',S'+ convert(varchar(3),@tintOrder)+'.vcState'+' ['+ @vcColumnName+']'     
	    IF LEN(@SearchText) > 0
		  BEGIN 
			SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + 'S' + CONVERT(VARCHAR(3),@tintOrder) + '.vcState LIKE ''%' + @SearchText + '%'' '     
		  END                                                
      set @WhereCondition= @WhereCondition +' left join State S'+ convert(varchar(3),@tintOrder)+ ' on S'+convert(varchar(3),@tintOrder)+ '.numStateID='+@vcDbColumnName                                                  
     end                                                  
     else if @vcListItemType='T'                                                   
     begin                                                  
      set @strColumns=@strColumns+',L'+ convert(varchar(3),@tintOrder)+'.vcData'+' ['+ @vcColumnName+']' 
	   IF LEN(@SearchText) > 0
		  BEGIN
			SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + 'L' + CONVERT(VARCHAR(3),@tintOrder) + '.vcData LIKE ''%' + @SearchText + '%'' '
		  END                                                    
      set @WhereCondition= @WhereCondition +' left Join ListDetails L'+ convert(varchar(3),@tintOrder)+ ' on L'+convert(varchar(3),@tintOrder)+ '.numListItemID='+@vcDbColumnName                                                  
     end                 
    ELSE IF   @vcListItemType='U'                                               
		BEGIN        
			SET @strColumns=@strColumns+',dbo.fn_GetContactName('+@Prefix+@vcDbColumnName+') ['+ @vcColumnName+']'
			  IF LEN(@SearchText) > 0
		  BEGIN
			SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + 'dbo.fn_GetContactName(' + @Prefix + @vcDbColumnName + ') LIKE ''%' + @SearchText + '%'' '
		  END                                                     
		END                
    END           
    ELSE IF @vcAssociatedControlType='DateField'                                                  
   begin            
		IF @vcDbColumnName='intTargetResolveDate'
		BEGIN
			 set @strColumns=@strColumns+',case when convert(varchar(11),'+@Prefix+@vcDbColumnName+')= convert(varchar(11),getdate()) then ''<b><font color=red>Today</font></b>'''      
			 set @strColumns=@strColumns+'when convert(varchar(11),'+@Prefix+@vcDbColumnName+')= convert(varchar(11),dateadd(day,-1,getdate())) then ''<b><font color=purple>YesterDay</font></b>'''      
			 set @strColumns=@strColumns+'when convert(varchar(11),'+@Prefix+@vcDbColumnName+')= convert(varchar(11),dateadd(day,1,getdate())) then''<b><font color=orange>Tommorow</font></b>'' '      
			 set @strColumns=@strColumns+'else dbo.FormatedDateFromDate('+@Prefix+@vcDbColumnName+','+convert(varchar(10),@numDomainId)+') end  ['+ @vcColumnName+']'               
			IF LEN(@SearchText) > 0
			BEGIN
			SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + @Prefix + @vcDbColumnName + ' LIKE ''%' + @SearchText + '%'' '
			END
		END
	ELSE
	BEGIN
     set @strColumns=@strColumns+',case when convert(varchar(11),DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+',' +@Prefix+@vcDbColumnName+') )= convert(varchar(11),getdate()) then ''<b><font color=red>Today</font></b>'''      
     set @strColumns=@strColumns+'when convert(varchar(11),DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+',' +@Prefix+@vcDbColumnName+') )= convert(varchar(11),dateadd(day,-1,getdate())) then ''<b><font color=purple>YesterDay</font></b>'''      
     set @strColumns=@strColumns+'when convert(varchar(11),DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+',' +@Prefix+@vcDbColumnName+') )= convert(varchar(11),dateadd(day,1,getdate())) then''<b><font color=orange>Tommorow</font></b>'' ' 
     set @strColumns=@strColumns+'else dbo.FormatedDateFromDate(DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+',' +@Prefix+@vcDbColumnName+'),'+convert(varchar(10),@numDomainId)+') end  ['+ @vcColumnName+']'
	 IF LEN(@SearchText) > 0
		BEGIN
			SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + @Prefix + @vcDbColumnName + ' LIKE ''%' + @SearchText + '%'' '
		END
	END
   -- end      
--  else      
--     set @strSql=@strSql+',dbo.FormatedDateFromDate('+@vcDbColumnName+','++convert(varchar(10),@numDomainId)+') ['+ @vcFieldName+'~'+ @vcDbColumnName+']'            
   END          
    ELSE IF @vcAssociatedControlType='TextBox'                                                  
	   BEGIN          
	    IF @vcDbColumnName = 'vcCompactContactDetails'
		BEGIN
			SET @strColumns=@strColumns+ ' ,'''' AS vcCompactContactDetails'   
		END 
		ELSE
		BEGIN 
			 SET @strColumns=@strColumns+','+ case                
			WHEN @vcDbColumnName='numAge' then 'year(getutcdate())-year(bintDOB)'               
			WHEN @vcDbColumnName='numDivisionID' then 'DM.numDivisionID'               
			WHEN @vcDbColumnName='textSubject' then 'convert(varchar(max),textSubject)' 
			WHEN @vcDbColumnName='textDesc' then    'convert(varchar(max),textDesc)'           
			WHEN @vcDbColumnName='textInternalComments' then 'convert(varchar(max),textInternalComments)'
			ELSE @vcDbColumnName end+' ['+ @vcColumnName+']'     
		END
		IF LEN(@SearchText) > 0
		 BEGIN
			SET @SearchQuery = @SearchQuery +  (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + (case                
			WHEN @vcDbColumnName='numAge' then 'year(getutcdate())-year(bintDOB)'               
			WHEN @vcDbColumnName='numDivisionID' then 'DM.numDivisionID'               
			WHEN @vcDbColumnName='textSubject' then 'convert(varchar(max),textSubject)' 
			WHEN @vcDbColumnName='textDesc' then    'convert(varchar(max),textDesc)'           
			WHEN @vcDbColumnName='textInternalComments' then 'convert(varchar(max),textInternalComments)'
			ELSE @vcDbColumnName end) + ' LIKE ''%' + @SearchText + '%'' '
		 END       
	   END  
  ELSE IF  @vcAssociatedControlType='TextArea'                                              
  begin  
       set @strColumns=@strColumns+', '+ @vcDbColumnName +' ['+ @vcColumnName+']'            
   end 
    	else if @vcAssociatedControlType='Popup' and @vcDbColumnName='numShareWith'                                                   
		begin      
			 set @strColumns=@strColumns+',(SELECT SUBSTRING(
	(SELECT '','' + A.vcFirstName+'' ''+A.vcLastName + CASE WHEN ISNULL(SR.numContactType,0)>0 THEN ''('' + dbo.fn_GetListItemName(SR.numContactType) + '')'' ELSE '''' END
	FROM ShareRecord SR JOIN UserMaster UM ON UM.numUserDetailId=SR.numAssignedTo 
							 JOIN AdditionalContactsInformation A ON UM.numUserDetailId=A.numContactID 
		 WHERE SR.numDomainID=Cs.numDomainID AND SR.numModuleID=7 AND SR.numRecordID=Cs.numCaseID
				AND UM.numDomainID=Cs.numDomainID and UM.numDomainID=A.numDomainID FOR XML PATH('''')),2,200000)) ['+ @vcColumnName+']'                
		 end     
 end                                            
 Else                                                    
 Begin        
   SET @vcColumnName= @vcDbColumnName +'~'+ convert(varchar(10),@numFieldId)+'~1'
       
        
    if @vcAssociatedControlType = 'TextBox' or @vcAssociatedControlType = 'TextArea'          
   begin         
           
    set @strColumns= @strColumns+',CFW'+ convert(varchar(3),@tintOrder)+'.Fld_Value  ['+ @vcColumnName + ']'           
    set @WhereCondition= @WhereCondition +' left Join CFW_FLD_Values_Case CFW'+ convert(varchar(3),@tintOrder)+ '         
    on CFW'+ convert(varchar(3),@tintOrder)+ '.Fld_Id='+convert(varchar(10),@numFieldId) +'and CFW'+ convert(varchar(3),@tintOrder)+ '.RecId=cs.numCaseId   '                                                 
   end        
           
   else if @vcAssociatedControlType = 'CheckBox'          
   begin          
             
    set @strColumns= @strColumns+',case when isnull(CFW'+ convert(varchar(3),@tintOrder)+'.Fld_Value,0)=0 then ''No'' when isnull(CFW'+ convert(varchar(3),@tintOrder)+'.Fld_Value,0)=1 then ''Yes'' end   ['+ @vcColumnName + ']'             
    set @WhereCondition= @WhereCondition +' left Join CFW_FLD_Values_Case CFW'+ convert(varchar(3),@tintOrder)+ '           
    on CFW'+ convert(varchar(3),@tintOrder)+ '.Fld_Id='+convert(varchar(10),@numFieldId) +'and CFW'+ convert(varchar(3),@tintOrder)+ '.RecId=cs.numCaseId   '                                                   
   end          
   else if @vcAssociatedControlType = 'DateField'           
   begin        
           
    set @strColumns= @strColumns+',dbo.FormatedDateFromDate(CFW'+ convert(varchar(3),@tintOrder)+'.Fld_Value,'+convert(varchar(10),@numDomainId)+')  ['+ @vcColumnName + ']'           
    set @WhereCondition= @WhereCondition +' left Join CFW_FLD_Values_Case CFW'+ convert(varchar(3),@tintOrder)+ '         
    on CFW'+ convert(varchar(3),@tintOrder)+ '.Fld_Id='+convert(varchar(10),@numFieldId) +'and CFW'+ convert(varchar(3),@tintOrder)+ '.RecId=cs.numCaseId   '                                                 
   end        
   else if @vcAssociatedControlType = 'SelectBox'            
   begin          
    set @vcDbColumnName = 'DCust'+Convert(varchar(10),@numFieldId)        
    set @strColumns=@strColumns+',L'+ convert(varchar(3),@tintOrder)+'.vcData'+' ['+ @vcColumnName + ']'                                                  
    set @WhereCondition= @WhereCondition +' left Join CFW_FLD_Values_Case CFW'+ convert(varchar(3),@tintOrder)+ '         
     on CFW'+ convert(varchar(3),@tintOrder)+ '.Fld_Id='+convert(varchar(10),@numFieldId) +'and CFW'+ convert(varchar(3),@tintOrder)+ '.RecId=cs.numCaseId   '                                                 
    set @WhereCondition= @WhereCondition +' left Join ListDetails L'+ convert(varchar(3),@tintOrder)+ ' on L'+convert(varchar(3),@tintOrder)+ '.numListItemID=CFW'+ convert(varchar(3),@tintOrder)+'.Fld_Value'        
   end           
 End        
                                                
   select top 1 @tintOrder=tintOrder+1,@vcDbColumnName=vcDbColumnName,@vcFieldName=vcFieldName,                
  @vcAssociatedControlType=vcAssociatedControlType,@vcListItemType=vcListItemType,@numListID=numListID,@vcLookBackTableName=vcLookBackTableName                                               
,@bitCustom=bitCustomField,@numFieldId=numFieldId,@bitAllowSorting=bitAllowSorting,@bitAllowEdit=bitAllowEdit,@ListRelID=ListRelID
  from #tempForm WHERE tintOrder > @tintOrder-1 order by tintOrder asc            
 
  if @@rowcount=0 set @tintOrder=0 
           
end    

	DECLARE @strShareRedordWith AS VARCHAR(300);
	SET @strShareRedordWith=' Cs.numCaseID in (select SR.numRecordID FROM ShareRecord SR WHERE SR.numDomainID='+ convert(varchar(15),@numDomainID )+' AND SR.numModuleID=7 AND SR.numAssignedTo='+convert(varchar(15),@numUserCntId)+') '
          
	DECLARE @strExternalUser AS VARCHAR(MAX) = ''
	SET @strExternalUser = CONCAT(' (NOT EXISTS (SELECT numUserDetailID FROM UserMaster WHERE numDomainID=',@numDomainID,' AND numUserDetailID=',@numUserCntId,') AND EXISTS (SELECT EAD.numExtranetDtlID FROM ExtranetAccountsDtl EAD INNER JOIN ExtarnetAccounts EA ON EAD.numExtranetID=EA.numExtranetID WHERE EAD.numDomainID=',@numDomainID,' AND EAD.numContactID=',@numUserCntID,' AND EA.numDivisionID=DM.numDivisionID))')
		                                                   
	DECLARE @StrSql AS VARCHAR(MAX) = ''
	                                                        
	 set @StrSql=@StrSql+'  FROM AdditionalContactsInformation ADC                                                             
	 JOIN Cases  Cs                                                              
	   ON ADC.numContactId =Cs.numContactId                                                             
	 JOIN DivisionMaster DM                                                              
	  ON ADC.numDivisionId = DM.numDivisionID                                                             
	  JOIN CompanyInfo CMP                                                             
	 ON DM.numCompanyID = CMP.numCompanyId                                                             
	 left join listdetails lst                                                            
	 on lst.numListItemID= cs.numPriority
	 left join listdetails lst1                                                            
	 on lst1.numListItemID= cs.numStatus                                                           
	 left join AdditionalContactsInformation ADC1                          
	 on Cs.numAssignedTo=ADC1.numContactId                                 
	  ' + ISNULL(@WhereCondition,'')  
	 
	IF @bitPartner=1 
		SET @strSql=@strSql+' left join CaseContacts CCont on CCont.numCaseID=Cs.numCaseID and CCont.bitPartner=1 and CCont.numContactId='+convert(varchar(15),@numUserCntID)+ ''                              

	if @columnName like 'CFW.Cust%'         
	begin        
		set @columnName='CFW.Fld_Value'        
		set @StrSql= @StrSql + ' left Join CFW_FLD_Values_Case CFW on CFW.RecId=cs.numCaseId  and CFW.fld_id= '+replace(@columnName,'CFW.Cust','')
	end                                 
	if @columnName like 'DCust%'        
	begin        
		set @columnName='LstCF.vcData'                                                  
		set @StrSql= @StrSql + ' left Join CFW_FLD_Values_Case CFW on CFW.RecId=cs.numCaseId and CFW.fld_id= '+replace(@columnName,'DCust','')                                                                                           
		set @StrSql= @StrSql + ' left Join ListDetails LstCF on LstCF.numListItemID=CFW.Fld_Value'        
	end 

	       
-------Change Row Color-------
Declare @vcCSOrigDbCOlumnName as varchar(50),@vcCSLookBackTableName as varchar(50),@vcCSAssociatedControlType as varchar(50)
SET @vcCSOrigDbCOlumnName=''
SET @vcCSLookBackTableName=''

--select top 1 @vcCSOrigDbCOlumnName=DFM.vcOrigDbCOlumnName,@vcCSLookBackTableName=DFM.vcLookBackTableName from DycFieldColorScheme DFCS join DycFieldMaster DFM on 
--DFCS.numModuleID=DFM.numModuleID and DFCS.numFieldID=DFM.numFieldID
--where DFCS.numDomainID=@numDomainID and DFM.numModuleID=1

Create table #tempColorScheme(vcOrigDbCOlumnName varchar(50),vcLookBackTableName varchar(50),vcFieldValue varchar(50),vcFieldValue1 varchar(50),vcColorScheme varchar(50),vcAssociatedControlType varchar(50))

insert into #tempColorScheme select DFM.vcOrigDbCOlumnName,DFM.vcLookBackTableName,DFCS.vcFieldValue,DFCS.vcFieldValue1,DFCS.vcColorScheme,DFFM.vcAssociatedControlType 
from DycFieldColorScheme DFCS join DycFormField_Mapping DFFM on DFCS.numFieldID=DFFM.numFieldID
join DycFieldMaster DFM on DFM.numModuleID=DFFM.numModuleID and DFM.numFieldID=DFFM.numFieldID
where DFCS.numDomainID=@numDomainID and DFFM.numFormID=12 AND DFCS.numFormID=12 and isnull(DFFM.bitAllowGridColor,0)=1

IF(SELECT COUNT(*) FROM #tempColorScheme)>0
BEGIN
   select top 1 @vcCSOrigDbCOlumnName=vcOrigDbCOlumnName,@vcCSLookBackTableName=vcLookBackTableName,@vcCSAssociatedControlType=vcAssociatedControlType from #tempColorScheme
END   
---------------------------- 
 
IF len(@vcCSOrigDbCOlumnName)>0 and len(@vcCSLookBackTableName)>0
BEGIN
set @strColumns=@strColumns+',tCS.vcColorScheme'
END
       

IF len(@vcCSOrigDbCOlumnName)>0 and len(@vcCSLookBackTableName)>0
BEGIN
	 if @vcCSLookBackTableName = 'AdditionalContactsInformation'                  
		set @Prefix = 'ADC.'                  
     if @vcCSLookBackTableName = 'DivisionMaster'                  
		set @Prefix = 'DM.'
	 if @vcCSLookBackTableName = 'CompanyInfo'                  
		set @Prefix = 'CMP.' 
	 if @vcCSLookBackTableName = 'Cases'                  
		set @Prefix = 'Cs.'   
 
	IF @vcCSAssociatedControlType='DateField'
	BEGIN
			set @strSql=@strSql+' left join #tempColorScheme tCS on CONVERT(VARCHAR(10),' + @Prefix + @vcCSOrigDbCOlumnName + ',111) >= CONVERT(VARCHAR(10),DateAdd(day,cast(tCS.vcFieldValue as int),GETUTCDATE()),111)
			 and CONVERT(VARCHAR(10),' + @Prefix + @vcCSOrigDbCOlumnName + ',111) <= CONVERT(VARCHAR(10),DateAdd(day,cast(tCS.vcFieldValue1 as int),GETUTCDATE()),111)'
	END
	ELSE IF @vcCSAssociatedControlType='SelectBox' OR @vcCSAssociatedControlType='ListBox' OR @vcCSAssociatedControlType='CheckBox'
	BEGIN
		set @strSql=@strSql+' left join #tempColorScheme tCS on tCS.vcFieldValue=' + @Prefix + @vcCSOrigDbCOlumnName
	END
END

	SET @strSql = @StrSql + ' Where cs.numDomainID='+ convert(varchar(15),@numDomainID ) + ''
 
 
	IF @numStatus <> 0
	BEGIN
		SET @strSql= @strSql + ' AND cs.numStatus = ' + CONVERT(VARCHAR(10),@numStatus)                                      	
	END
	ELSE IF @tintSortOrder <> 5
	BEGIN
 		SET @strSql= @strSql + ' AND cs.numStatus <> 136 ' 
	END
                                                 
	if @numDivisionID <>0 set @strSql=@strSql + ' And div.numDivisionID =' + convert(varchar(15),@numDivisionID)                                                      
	if @SortChar<>'0' set @strSql=@strSql + ' And (ADC.vcFirstName like '''+@SortChar+'%'''                                                              
	if @SortChar<>'0' set @strSql=@strSql + ' or ADC.vcLastName like '''+@SortChar+'%'')'                                                             

	IF @tintSortOrder!=6
	BEGIN
		if @tintUserRightType=1 set @strSql=@strSql + ' AND (Cs.numRecOwner = '+convert(varchar(15),@numUserCntId)+ ' or Cs.numAssignedTo='+convert(varchar(15),@numUserCntID) + 
		+ case when @tintSortOrder=1 THEN ' OR (Cs.numRecOwner = -1 or Cs.numAssignedTo= -1)' ELSE '' end
		+ case when @bitPartner=1 then ' or (CCont.bitPartner=1 and CCont.numContactId='+convert(varchar(15),@numUserCntID)+ ')' else '' end  + ' or ' + @strShareRedordWith +' OR ' + @strExternalUser +')'                
		else if @tintUserRightType=2 set @strSql=@strSql + ' AND (DM.numTerID in (select numTerritoryID from  UserTerritory where numUserCntID= '+convert(varchar(15),@numUserCntId)+' ) or Cs.numAssignedTo='+convert(varchar(15),@numUserCntID)+ ' or ' + @strShareRedordWith + ')'                   
	END
	ELSE
	BEGIN
		SET @strSql=@strSql + ' AND (Cs.numRecOwner = -1 or Cs.numAssignedTo= -1)'
	END                 
                                                   
	if @tintSortOrder=0  set @strSql=@strSql + ' AND (Cs.numRecOwner = '+convert(varchar(15),@numUserCntId)+' or Cs.numAssignedTo = '+convert(varchar(15),@numUserCntId)+ ' or ' + @strShareRedordWith +' OR ' + @strExternalUser +')'                                                                                                     
	ELSE if @tintSortOrder=2  set @strSql=@strSql + 'And cs.numStatus<>136  ' + ' AND cs.bintCreatedDate> '''+convert(varchar(20),dateadd(day,-7,getutcdate()))+''''                                 
	else if @tintSortOrder=3  set @strSql=@strSql+'And cs.numStatus<>136  ' + ' and cs.numCreatedBy ='+ convert(varchar(15),@numUserCntId)       
	else if @tintSortOrder=4  set @strSql=@strSql+ 'And cs.numStatus<>136  ' + ' and cs.numModifiedBy ='+ convert(varchar(15),@numUserCntId)                                                            
	else if @tintSortOrder=5  set @strSql=@strSql+ 'And cs.numStatus=136  ' --+ ' and cs.numModifiedBy ='+ convert(varchar(15),@numUserCntId)                                                            
 
	IF @vcRegularSearchCriteria<>'' set @strSql=@strSql+' and ' + @vcRegularSearchCriteria 
	IF @vcCustomSearchCriteria<>'' set @strSql=@strSql+' and cs.numCaseId in (select distinct CFW.RecId from CFW_FLD_Values_Case CFW where ' + @vcCustomSearchCriteria + ')'


	IF LEN(@SearchQuery) > 0
	BEGIN
		SET @StrSql = @StrSql + ' AND (' + @SearchQuery + ') '
	END	
	
	declare @firstRec as integer                                                            
	declare @lastRec as integer                                                            
	set @firstRec= (@CurrentPage-1) * @PageSize                                                            
	set @lastRec= (@CurrentPage*@PageSize+1)  

	-- GETS ROWS COUNT
	DECLARE @strTotal NVARCHAR(MAX) = 'SELECT @TotalRecords = COUNT(*) ' + @StrSql
	exec sp_executesql @strTotal, N'@TotalRecords INT OUT', @TotRecs OUT

	

	-- EXECUTE FINAL QUERY
	DECLARE @strFinal AS VARCHAR(MAX)
	SET @strFinal = CONCAT('SELECT ROW_NUMBER() OVER(ORDER BY ',@columnName,' ',@columnSortOrder,') ID, ',@strColumns,' INTO #tempTable',@StrSql,'; SELECT * FROM #tempTable WHERE ID > ',@firstRec,' and ID <',@lastRec,'; DROP TABLE #tempTable;')
	
	PRINT @strFinal
	EXEC (@strFinal) 

	SELECT * FROM #tempForm

	DROP TABLE #tempForm

	DROP TABLE #tempColorScheme
GO
