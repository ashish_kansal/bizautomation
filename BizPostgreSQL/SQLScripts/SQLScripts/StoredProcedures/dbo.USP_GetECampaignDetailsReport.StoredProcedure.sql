GO
/****** Object:  StoredProcedure [dbo].[USP_GetECampaignDetailsReport]    Script Date: 06/04/2009 16:23:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Pratik  Vasani
-- Create date: 13/DEC/2008
-- Description:	This Stored proc gets the table for display in Ecampaign report
-- =============================================

--declare @p4 int
--set @p4=0
--exec USP_GetECampaignDetailsReport @numDomainId=1,@CurrentPage=1,@PageSize=20,@TotRecs=@p4 output
--select @p4
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getecampaigndetailsreport')
DROP PROCEDURE usp_getecampaigndetailsreport
GO
CREATE PROCEDURE [dbo].[USP_GetECampaignDetailsReport]
               @numDomainId NUMERIC(9,0)  = 0,
               @CurrentPage INT,
               @PageSize    INT,
               @TotRecs     INT  OUTPUT,
			   @numECampaingID NUMERIC(18,0) = 0
AS
  BEGIN
    CREATE TABLE #tempTable (
       ID INT IDENTITY PRIMARY KEY,
	  numECampaignID NUMERIC(9,0),
	  vcECampName VARCHAR(200),
	  numConEmailCampID NUMERIC(9,0),
      numContactID NUMERIC(9,0),
      CompanyName VARCHAR(100),
      ContactName VARCHAR(100),
	  Recipient VARCHAR(100),
      [Status] VARCHAR(20),
	  Template VARCHAR(500),
	  dtSentDate VARCHAR(80),
	  SendStatus VARCHAR(20),
	  dtNextStage VARCHAR(80),
	  Stage INT,
	  NoOfStagesCompleted INT,
	  NextTemplate VARCHAR(100),
	  FromEmail VARCHAR(100),
	  NoOfEmailSent INT,
	  NoOfEmailOpened INT
      )
    INSERT INTO #tempTable
  SELECT
		ECampaign.numECampaignID,
		ECampaign.vcECampName,
		ConECampaign.numConEmailCampID,
		ConECampaign.numContactID,
		dbo.GetCompanyNameFromContactID(ConECampaign.numContactID,ECampaign.numDomainId) AS CompanyName,
		dbo.fn_GetContactName(ConECampaign.numContactID) AS ContactName,
		(ISNULL(AdditionalContactsInformation.vcEmail,'')) +'('+ CAST((SELECT COUNT(*) FROM ConECampaignDTL INNER JOIN ECampaignDTLs ON ConECampaignDTL.numECampDTLID = ECampaignDTLs.numECampDTLId WHERE ECampaignDTLs.numEmailTemplate IS NOT NULL AND numConECampID = ConECampaign.numECampaignID AND bitSend = 1 AND ISNULL(tintDeliveryStatus,0) = 1 AND ISNULL(bitEmailRead,0) = 1) AS varchar) +')' AS Recipient,
		(CASE 
			WHEN ISNULL(ECampaign.bitDeleted,0) = 0 
			THEN CASE WHEN ISNULL(ConECampaign.bitEngaged,0) = 0 THEN 'Disengaged' ELSE 'Engaged' END
			ELSE 'Campaign Deleted'
		END) AS Status,
		ISNULL(CampaignExecutedDetail.Template,''),
		ISNULL(dbo.FormatedDateFromDate(CampaignExecutedDetail.bintSentON,@numDomainID),''),
		(CASE 
			WHEN ISNULL(CampaignExecutedDetail.bitSend,0)=1 AND ISNULL(CampaignExecutedDetail.tintDeliveryStatus,0)=0 THEN 'Failed'
			WHEN ISNULL(CampaignExecutedDetail.bitSend,0)=1 AND ISNULL(CampaignExecutedDetail.tintDeliveryStatus,0)=1 THEN 'Success'
			ELSE 'Pending'
		END) AS SendStatus
		,
		 dbo.FormatedDateFromDate(DATEADD(MINUTE,ISNULL(ECampaign.fltTimeZone,0) * 60,CampaignNextExecutationDetail.dtExecutionDate),@numDomainID),
		 ISNULL((SELECT COUNT(*) FROM ConECampaignDTL WHERE numConECampID = ConECampaign.numConEmailCampID),0) AS NoOfStages,
		 ISNULL((SELECT COUNT(*) FROM ConECampaignDTL WHERE numConECampID = ConECampaign.numConEmailCampID AND ISNULL(bitSend,0)=1),0) AS NoOfStagesCompleted,

		 -----------------Added By Priya-----(04/01/2018)
		 (select gn.vcDocName from (
										select *, ROW_NUMBER() over(order by numEcampID) as RowNumber
										from ECampaignDTLs 
										where  (isnull(numEmailTemplate,0) <> 0 or isnull(numActionItemTemplate,0) <> 0)
										and numECampID = ECampaign.numECampaignID ) as Ecamp
										INNER JOIN  GenericDocuments gn
										on gn.numGenericDocID = 
										(case when Ecamp.numActionItemTemplate > 0 then Ecamp.numActionItemTemplate else Ecamp.numEmailTemplate end) where RowNumber = ISNULL((SELECT COUNT(*) FROM ConECampaignDTL WHERE numConECampID = ConECampaign.numConEmailCampID AND ISNULL(bitSend,0)=1),0) +1 )
										
		+ '(' + 
		CAST(ISNULL((SELECT COUNT(*) FROM ConECampaignDTL WHERE numConECampID = ConECampaign.numConEmailCampID AND ISNULL(bitSend,0)=1),0) AS varchar) + ' Of ' +
		 CAST(ISNULL((SELECT COUNT(*) FROM ConECampaignDTL WHERE numConECampID = ConECampaign.numConEmailCampID),0) AS VARCHAR)
		  + ')'  AS NextTemplate,
		  
		  	(SELECT vcEmail from AdditionalContactsInformation 
				WHERE AdditionalContactsInformation.numContactId = ConECampaign.numRecOwner) AS FromEmail,
		 -----------------Added By Priya-----(04/01/2018)		 

		 (SELECT COUNT(*) FROM ConECampaignDTL INNER JOIN ECampaignDTLs ON ConECampaignDTL.numECampDTLID = ECampaignDTLs.numECampDTLId WHERE ECampaignDTLs.numEmailTemplate IS NOT NULL AND numConECampID = ConECampaign.numECampaignID AND bitSend = 1 AND ISNULL(tintDeliveryStatus,0) = 1) AS NoOfEmailSent,
		 (SELECT COUNT(*) FROM ConECampaignDTL INNER JOIN ECampaignDTLs ON ConECampaignDTL.numECampDTLID = ECampaignDTLs.numECampDTLId WHERE ECampaignDTLs.numEmailTemplate IS NOT NULL AND numConECampID = ConECampaign.numECampaignID AND bitSend = 1 AND ISNULL(tintDeliveryStatus,0) = 1 AND ISNULL(bitEmailRead,0) = 1) AS NoOfEmailOpened
	
	FROM
		ConECampaign
	INNER JOIN
		ECampaign
	ON
		ConECampaign.numECampaignID = ECampaign.numECampaignID
	LEFT JOIN
		AdditionalContactsInformation
	ON
		ConECampaign.numContactID = AdditionalContactsInformation.numContactId
	OUTER APPLY
		(
			SELECT 
				TOP 1
				(CASE 
					WHEN ECampaignDTLs.numEmailTemplate IS NOT NULL THEN GenericDocuments.vcDocName
					WHEN ECampaignDTLs.numActionItemTemplate IS NOT NULL THEN tblActionItemData.TemplateName
					ELSE ''
				END) AS Template,
				ConECampaignDTL.bitSend,
				ConECampaignDTL.bintSentON,
				ConECampaignDTL.tintDeliveryStatus
			FROM 
				ConECampaignDTL 
			INNER JOIN 
				ECampaignDTLs 
			ON 
				ConECampaignDTL.numECampDTLID = ECampaignDTLs.numECampDTLId
			LEFT JOIN 
				GenericDocuments 
			ON 
				GenericDocuments.numGenericDocID = ECampaignDTLs.numEmailTemplate
			LEFT JOIN 
				tblActionItemData
			ON
				tblActionItemData.RowID = ECampaignDTLs.numActionItemTemplate
			WHERE
				ECampaignDTLs.numECampID = ECampaign.numECampaignID AND
				ConECampaignDTL.numConECampID = ConECampaign.numConEmailCampID AND
				ConECampaignDTL.bitSend IS NOT NULL
			ORDER BY
				numConECampDTLID DESC
		) CampaignExecutedDetail
	OUTER APPLY
		(
			SELECT 
				TOP 1
				ConECampaignDTL.dtExecutionDate
			FROM 
				ConECampaignDTL 
			INNER JOIN 
				ECampaignDTLs 
			ON 
				ConECampaignDTL.numECampDTLID = ECampaignDTLs.numECampDTLId
			LEFT JOIN 
				GenericDocuments 
			ON 
				ECampaignDTLs.numEmailTemplate = GenericDocuments.numGenericDocID
			LEFT JOIN 
				tblActionItemData
			ON
				ECampaignDTLs.numActionItemTemplate = tblActionItemData.RowID
			WHERE
				ECampaignDTLs.numECampID = ECampaign.numECampaignID AND
				ConECampaignDTL.numConECampID = ConECampaign.numConEmailCampID AND
				ConECampaignDTL.bitSend IS NULL
			ORDER BY
				numConECampDTLID
		) CampaignNextExecutationDetail
	WHERE
		ECampaign.numDomainID = @numDomainId AND
		(ECampaign.numECampaignID = @numECampaingID OR ISNULL(@numECampaingID,0) = 0)
		AND bitEngaged = 1 -- Added by Priya(06/01/2018)
    
   
            
    DECLARE  @firstRec  AS INTEGER
    DECLARE  @lastRec  AS INTEGER
    SET @firstRec = (@CurrentPage
                       - 1)
                      * @PageSize
    SET @lastRec = (@CurrentPage
                      * @PageSize
                      + 1)
    SET @TotRecs = (SELECT COUNT(* )
                    FROM   #tempTable)
    SELECT   *
    FROM     #tempTable
    WHERE    ID > @firstRec
             AND ID < @lastRec
    ORDER BY id

    DROP TABLE #tempTable
    SELECT @TotRecs AS TotRecs
  END
