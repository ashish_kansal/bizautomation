/****** Object:  StoredProcedure [dbo].[USP_GetCheckJournalId]    Script Date: 07/26/2008 16:16:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created By Siva            
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getcheckjournalid')
DROP PROCEDURE usp_getcheckjournalid
GO
CREATE PROCEDURE [dbo].[USP_GetCheckJournalId]           
@numCheckId as numeric(9)=0,            
@numDomainId as numeric(9)=0            
As            
Begin             
 Declare @numJournalId as numeric(9)          
       
 Select numJournal_Id From General_Journal_Header Where numCheckId=@numCheckId  and numDomainId=@numDomainId     
End
GO
