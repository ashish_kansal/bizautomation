/****** Object:  StoredProcedure [dbo].[usp_DeleteRule]    Script Date: 07/26/2008 16:15:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_deleterule')
DROP PROCEDURE usp_deleterule
GO
CREATE PROCEDURE [dbo].[usp_DeleteRule]  
 @numRuleID numeric     
--  
AS  
--This procedure will delete the rule pertaining to the Rule ID passed as parameter.  
BEGIN  

 delete from RoutingLeadDetails WHERE numRoutID=@numRuleID 
 delete from RoutinLeadsValues WHERE numRoutID=@numRuleID 
 DELETE FROM RoutingLeads  
  WHERE  
   numRoutID=@numRuleID  
END
GO
