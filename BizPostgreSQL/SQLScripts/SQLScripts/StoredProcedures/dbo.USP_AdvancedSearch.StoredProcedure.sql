/****** Object:  StoredProcedure [dbo].[USP_AdvancedSearch]    Script Date: 05/07/2009 22:04:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj                              
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_advancedsearch')
DROP PROCEDURE usp_advancedsearch
GO
CREATE PROCEDURE [dbo].[USP_AdvancedSearch] 
@WhereCondition as varchar(4000)='',                            
@AreasofInt as varchar(100)='',                              
@tintUserRightType as tinyint,                              
@numDomainID as numeric(9)=0,                              
@numUserCntID as numeric(9)=0,                                                        
@CurrentPage int,                                                                    
@PageSize int,                                                                    
@TotRecs int output,                                                                                                             
@columnSortOrder as Varchar(10),                              
@ColumnSearch as varchar(100)='',                           
@ColumnName as varchar(20)='',                             
@GetAll as bit,                              
@SortCharacter as char(1),                        
@SortColumnName as varchar(100)='',        
@bitMassUpdate as bit,        
@LookTable as varchar(200)='' ,        
@strMassUpdate as varchar(MAX)='',
@vcRegularSearchCriteria varchar(MAX)='',
@vcCustomSearchCriteria varchar(MAX)='',
@ClientTimeZoneOffset as int,
@vcDisplayColumns VARCHAR(MAX)
as                              
BEGIN
	DECLARE @tintOrder  AS TINYINT
	DECLARE @vcFormFieldName  AS VARCHAR(50)
	DECLARE @vcListItemType  AS VARCHAR(3)
	DECLARE @vcListItemType1  AS VARCHAR(3)
	DECLARE @vcAssociatedControlType VARCHAR(20)
	DECLARE @numListID  AS NUMERIC(9)
	DECLARE @vcDbColumnName VARCHAR(50)
	DECLARE @vcLookBackTableName VARCHAR(50)
	DECLARE @WCondition VARCHAR(1000)
	DECLARE @bitContactAddressJoinAdded AS BIT;
	DECLARE @bitBillAddressJoinAdded AS BIT;
	DECLARE @bitShipAddressJoinAdded AS BIT;
	DECLARE @vcOrigDbColumnName as varchar(50)
	DECLARE @bitAllowSorting AS CHAR(1)
	DECLARE @bitAllowEdit AS CHAR(1)
	DECLARE @bitCustomField AS BIT;
	DECLARE @ListRelID AS NUMERIC(9)
	DECLARE @intColumnWidth INT
	DECLARE @bitAllowFiltering AS BIT;
	DECLARE @vcfieldatatype CHAR(1)
	DECLARE @zipCodeSearch AS VARCHAR(100) = ''


	IF CHARINDEX('$SZ$', @WhereCondition)>0
	BEGIN
		SELECT @zipCodeSearch=SUBSTRING(@WhereCondition, CHARINDEX('$SZ$', @WhereCondition)+4,CHARINDEX('$EZ$', @WhereCondition) - CHARINDEX('$SZ$', @WhereCondition)-4) 

		CREATE TABLE #tempAddress
		(
			[numAddressID] [numeric](18, 0) NOT NULL,
			[vcAddressName] [varchar](50) NULL,
			[vcStreet] [varchar](100) NULL,
			[vcCity] [varchar](50) NULL,
			[vcPostalCode] [varchar](15) NULL,
			[numState] [numeric](18, 0) NULL,
			[numCountry] [numeric](18, 0) NULL,
			[bitIsPrimary] [bit] NOT NULL CONSTRAINT [DF_AddressMaster_bitIsPrimary]  DEFAULT ((0)),
			[tintAddressOf] [tinyint] NOT NULL,
			[tintAddressType] [tinyint] NOT NULL CONSTRAINT [DF_AddressMaster_tintAddressType]  DEFAULT ((0)),
			[numRecordID] [numeric](18, 0) NOT NULL,
			[numDomainID] [numeric](18, 0) NOT NULL
		)

		IF Len(@zipCodeSearch)>0
		BEGIN
			Declare @Strtemp as nvarchar(500);
			SET @Strtemp='insert into #tempAddress select * from AddressDetails  where numDomainID=@numDomainID1  
							and vcPostalCode in (SELECT REPLICATE(''0'', 5 - LEN(ZipCode)) + ZipCode  FROM ' + @zipCodeSearch +')';
	
			EXEC sp_executesql @Strtemp,N'@numDomainID1 numeric(18)',@numDomainID1=@numDomainID

			set @WhereCondition=replace(@WhereCondition,'$SZ$' + @zipCodeSearch + '$EZ$',
			' (ADC.numContactID in (select  numRecordID from #tempAddress where tintAddressOf=1 AND tintAddressType=0)   
			or (DM.numDivisionID in (select  numRecordID from #tempAddress where tintAddressOf=2 AND tintAddressType in(1,2))))')
		END
	END

	SET @WCondition = ''
   
	IF (@SortCharacter <> '0' AND @SortCharacter <> '')
	  SET @ColumnSearch = @SortCharacter

	CREATE TABLE #temptable 
	(
		id INT IDENTITY PRIMARY KEY,
		numcontactid VARCHAR(15)
	)


---------------------Find Duplicate Code Start-----------------------------
	DECLARE  @Sql           VARCHAR(8000),
			 @numMaxFieldId NUMERIC(9),
			 @Where         NVARCHAR(1000),
			 @OrderBy       NVARCHAR(1000),
			 @GroupBy       NVARCHAR(1000),
			 @SelctFields   NVARCHAR(4000),
			 @InneJoinOn    NVARCHAR(1000)
	SET @InneJoinOn = ''
	SET @OrderBy = ''
	SET @Sql = ''
	SET @Where = ''
	SET @GroupBy = ''
	SET @SelctFields = ''
    
	IF (@WhereCondition = 'DUP' OR @WhereCondition = 'DUP-PRIMARY')
	BEGIN
		SELECT @numMaxFieldId= MAX([numFieldID]) FROM View_DynamicColumns
		WHERE [numFormID] =24 AND [numDomainId] = @numDomainID
		--PRINT @numMaxFieldId
		WHILE @numMaxFieldId > 0
		BEGIN
			SELECT @vcFormFieldName=[vcFieldName],@vcDbColumnName=[vcDbColumnName],@vcLookBackTableName=[vcLookBackTableName] 
			FROM View_DynamicColumns 
			WHERE [numFieldID] = @numMaxFieldId and [numFormID] =24 AND [numDomainId] = @numDomainID
			--	PRINT @vcDbColumnName
				SET @SelctFields = @SelctFields +  @vcDbColumnName + ','
				IF(@vcLookBackTableName ='AdditionalContactsInformation')
				BEGIN
					SET @InneJoinOn = @InneJoinOn + 'ADC.'+ @vcDbColumnName +' = a1.' + @vcDbColumnName + ' and '
					SET @GroupBy = @GroupBy + 'ADC.'+  @vcDbColumnName + ','
				END
				ELSE IF (@vcLookBackTableName ='DivisionMaster')
				BEGIN
					SET @InneJoinOn = @InneJoinOn + 'DM.'+ @vcDbColumnName +' = a1.' + @vcDbColumnName + ' and '
					SET @GroupBy = @GroupBy + 'DM.'+  @vcDbColumnName + ','
				END
				ELSE
				BEGIN
					SET @InneJoinOn = @InneJoinOn + 'C.' + @vcDbColumnName +' = a1.' + @vcDbColumnName + ' and '
					SET @GroupBy = @GroupBy + 'C.' + @vcDbColumnName + ','
				END
				SET @Where = @Where + 'and LEN(' + @vcDbColumnName + ')>0 '

	
			SELECT @numMaxFieldId = MAX([numFieldID]) FROM View_DynamicColumns 
			WHERE [numFormID] =24 AND [numDomainId] = @numDomainID AND [numFieldID] < @numMaxFieldId
		END
			
				IF(LEN(@Where)>2)
				BEGIN
					SET @Where =RIGHT(@Where,LEN(@Where)-3)
					SET @OrderBy =  LEFT(@GroupBy,(Len(@GroupBy)- 1))
					SET @GroupBy =  LEFT(@GroupBy,(Len(@GroupBy)- 1))
					SET @InneJoinOn = LEFT(@InneJoinOn,LEN(@InneJoinOn)-3)
					SET @SelctFields = LEFT(@SelctFields,(Len(@SelctFields)- 1))
					SET @Sql = @Sql + ' INSERT INTO [#tempTable] ([numContactID])SELECT  ADC.numContactId FROM additionalcontactsinformation ADC JOIN DivisionMaster DM ON ADC.numDivisionId = DM.numDivisionID JOIN CompanyInfo C ON DM.numCompanyID = C.numCompanyId  
						JOIN (SELECT   '+ @SelctFields + '
					   FROM  additionalcontactsinformation ADC JOIN DivisionMaster DM ON ADC.numDivisionId = DM.numDivisionID JOIN CompanyInfo C ON DM.numCompanyID = C.numCompanyId  
					   WHERE  ' + @Where + ' AND C.[numDomainID] = '+convert(varchar(15),@numDomainID)+ ' AND ADC.[numDomainID] = '+convert(varchar(15),@numDomainID)+ ' AND DM.[numDomainID] = '+convert(varchar(15),@numDomainID)+ 
					   ' GROUP BY ' + @GroupBy + '
					   HAVING   COUNT(* ) > 1) a1
						ON  '+ @InneJoinOn + ' where ADC.[numDomainID] = '+convert(varchar(15),@numDomainID) + ' and DM.[numDomainID] = '+convert(varchar(15),@numDomainID) + ' ORDER BY '+ @OrderBy
				END	
				ELSE
				BEGIN
					SET @Where = ' 1=0 '
					SET @Sql = @Sql + ' INSERT INTO [#tempTable] ([numContactID])SELECT  ADC.numContactId FROM additionalcontactsinformation ADC  '
								+ ' Where ' + @Where
				END 	
			   PRINT @Sql
			   EXEC( @Sql)
			   SET @Sql = ''
			   SET @GroupBy = ''
			   SET @SelctFields = ''
			   SET @Where = ''
	
		IF (@WhereCondition = 'DUP-PRIMARY')
		  BEGIN
			SET @WCondition = ' AND ADC.numContactId IN (SELECT DISTINCT numContactId FROM #tempTable) and ISNULL(ADC.bitPrimaryContact,0)=1'
		  END
		ELSE
		  BEGIN
			SET @WCondition = ' AND ADC.numContactId IN (SELECT DISTINCT numContactId FROM #tempTable)'
		  END
    
		SET @Sql = ''
		SET @WhereCondition =''
		SET @Where= 'DUP' 
	END
-----------------------------End Find Duplicate ------------------------

	declare @strSql as varchar(8000)                                                              
	 set  @strSql='Select ADC.numContactId                    
	  FROM AdditionalContactsInformation ADC                                                   
	  JOIN DivisionMaster DM ON ADC.numDivisionId = DM.numDivisionID                                                  
	  JOIN CompanyInfo C ON DM.numCompanyID = C.numCompanyId'   

	IF @SortColumnName='vcStreet' OR @SortColumnName='vcCity' OR @SortColumnName='vcPostalCode' OR @SortColumnName ='numCountry' OR @SortColumnName ='numState'
	   OR @ColumnName='vcStreet' OR @ColumnName='vcCity' OR @ColumnName='vcPostalCode' OR @ColumnName ='numCountry' OR @ColumnName ='numState'	
	BEGIN 
		set @strSql= @strSql +' left Join AddressDetails AD on AD.numRecordId= ADC.numContactID and AD.tintAddressOf=1 and AD.tintAddressType=0 AND AD.bitIsPrimary=1 and AD.numDomainID= ADC.numDomainID'	
	END 
	ELSE IF @SortColumnName='vcBillStreet' OR @SortColumnName='vcBillCity' OR @SortColumnName='vcBillPostCode' OR @SortColumnName ='numBillCountry' OR @SortColumnName ='numBillState'
		 OR @ColumnName='vcBillStreet' OR @ColumnName='vcBillCity' OR @ColumnName='vcBillPostCode' OR @ColumnName ='numBillCountry' OR @ColumnName ='numBillState'
	BEGIN 	
		set @strSql= @strSql +' left Join AddressDetails AD1 on AD1.numRecordId= DM.numDivisionID and AD1.tintAddressOf=2 and AD1.tintAddressType=1 AND AD1.bitIsPrimary=1 and AD1.numDomainID= ADC.numDomainID '
	END	
	ELSE IF @SortColumnName='vcShipStreet' OR @SortColumnName='vcShipCity' OR @SortColumnName='vcShipPostCode' OR @SortColumnName ='numShipCountry' OR @SortColumnName ='numShipState'
		 OR @ColumnName='vcShipStreet' OR @ColumnName='vcShipCity' OR @ColumnName='vcShipPostCode' OR @ColumnName ='numShipCountry' OR @ColumnName ='numShipState'
	BEGIN
		set @strSql= @strSql +' left Join AddressDetails AD2 on AD2.numRecordId= DM.numDivisionID and AD2.tintAddressOf=2 and AD2.tintAddressType=2 AND AD2.bitIsPrimary=1 and AD2.numDomainID= ADC.numDomainID '
	END	
                         
	if (@ColumnName<>'' and  @ColumnSearch<>'')                        
	begin                          
		SELECT @vcListItemType=vcListItemType,@numListID=numListID from View_DynamicDefaultColumns where vcDbColumnName=@ColumnName and numFormID=1 and numDomainId=@numDomainId                         
    
		if @vcListItemType='LI'                               
		begin                      
			IF @numListID=40--Country
			BEGIN
				IF @ColumnName ='numCountry'
				BEGIN
						set @strSql= @strSql +' left Join ListDetails L1 on L1.numListItemID=AD.numCountry' 
				END
				ELSE IF @ColumnName ='numBillCountry'
				BEGIN
						set @strSql= @strSql +' left Join ListDetails L1 on L1.numListItemID=AD1.numCountry' 
				END
				ELSE IF @ColumnName ='numShipCountry'
				BEGIN
						set @strSql= @strSql +' left Join ListDetails L1 on L1.numListItemID=AD2.numCountry' 
				END
			END                        
			ELSE
			BEGIN
				set @strSql= @strSql +' left Join ListDetails L1 on L1.numListItemID='+@ColumnName                              
			END   
		end                              
		else if @vcListItemType='S'                               
		begin           
		   IF @ColumnName ='numState'
			BEGIN
   				set @strSql= @strSql +' left join State S1 on S1.numStateID=AD.numState'                            
			END
			ELSE IF @ColumnName ='numBillState'
			BEGIN
   				set @strSql= @strSql +' left join State S1 on S1.numStateID=AD1.numState'                            
			END
			ELSE IF @ColumnName ='numShipState'
			BEGIN
   				set @strSql= @strSql +' left join State S1 on S1.numStateID=AD2.numState'                           
			END
		end                              
		else if @vcListItemType='T'                               
		begin                              
		  set @strSql= @strSql +' left Join ListDetails L1 on L1.numListItemID='+@ColumnName                             
		end                           
	end       
                      
	if (@SortColumnName<>'')                        
	begin                          
		select @vcListItemType1=vcListItemType,@numListID=numListID from View_DynamicDefaultColumns where vcDbColumnName=@SortColumnName and numFormID=1 and numDomainId=@numDomainId                         
    
		if @vcListItemType1='LI'                   
		begin     
			IF @numListID=40--Country
			BEGIN
				IF @SortColumnName ='numCountry'
				BEGIN
						set @strSql= @strSql +' left Join ListDetails L2 on L2.numListItemID=AD.numCountry' 
				END
				ELSE IF @SortColumnName ='numBillCountry'
				BEGIN
						set @strSql= @strSql +' left Join ListDetails L2 on L2.numListItemID=AD1.numCountry' 
				END
				ELSE IF @SortColumnName ='numShipCountry'
				BEGIN
						set @strSql= @strSql +' left Join ListDetails L2 on L2.numListItemID=AD2.numCountry' 
				END
			  END                        
			ELSE
			BEGIN
				set @strSql= @strSql +' left Join ListDetails L2 on L2.numListItemID='+@SortColumnName                              
			END
		END                              
		else if @vcListItemType1='S'           
		begin                
			IF @SortColumnName ='numState'
			BEGIN
   				set @strSql= @strSql +' left join State S2 on S2.numStateID=AD.numState'                            
			END
			ELSE IF @SortColumnName ='numBillState'
			BEGIN
   				set @strSql= @strSql +' left join State S2 on S2.numStateID=AD1.numState'                            
			END
			ELSE IF @SortColumnName ='numShipState'
			BEGIN
   				set @strSql= @strSql +' left join State S2 on S2.numStateID=AD2.numState'                           
			END
		end                              
		else if @vcListItemType1='T'                  
		begin                              
			set @strSql= @strSql +' left Join ListDetails L2 on L2.numListItemID='+@SortColumnName                               
		end                           
	end                          
  
	set @strSql=@strSql+' where DM.numDomainID  = '+convert(varchar(15),@numDomainID)+   @WhereCondition                                  
                 
--if @tintUserRightType=1 set @strSql=@strSql + ' AND (ADC.numRecOwner = '+convert(varchar(15),@numUserCntID)+ ')'                                       
--else if @tintUserRightType=2 set @strSql=@strSql + ' AND (DM.numTerID in (select numTerritoryID from  UserTerritory where numUserCntID= '+convert(varchar(15),@numUserCntID)+' ) or DM.numTerID=0) '
                        
IF @vcRegularSearchCriteria<>'' set @strSql=@strSql+' and ' + @vcRegularSearchCriteria 
IF @vcCustomSearchCriteria<>'' set @strSql=@strSql+' and ' + @vcCustomSearchCriteria                          
                                            
if    @AreasofInt<>'' set  @strSql=@strSql+ ' and ADC.numContactID in (select numContactID from  AOIContactLink where numAOIID in('+ @AreasofInt+'))'
                             
if (@ColumnName<>'' and  @ColumnSearch<>'')                         
begin                          
  if @vcListItemType='LI'                               
    begin                                
      set @strSql= @strSql +' and L1.vcData like '''+@ColumnSearch +'%'''                             
    end                              
    else if @vcListItemType='S'                               
    begin                                  
      set @strSql= @strSql +' and S1.vcState like '''+@ColumnSearch+'%'''                          
    end                              
    else if @vcListItemType='T'                               
    begin                              
      set @strSql= @strSql +' and T1.vcTerName like '''+@ColumnSearch  +'%'''                            
    end   
    else  
       BEGIN     
		   IF @ColumnName='vcStreet' OR @ColumnName='vcBillStreet' OR @ColumnName='vcShipStreet'
				set @strSql= @strSql +' and vcStreet like '''+@ColumnSearch  +'%'''                            
		   ELSE IF @ColumnName='vcCity' OR @ColumnName='vcBillCity' OR @ColumnName='vcShipCity'
				set @strSql= @strSql +' and vcCity like '''+@ColumnSearch  +'%'''                            
		   ELSE IF @ColumnName='vcPostalCode' OR @ColumnName='vcBillPostCode' OR @ColumnName='vcShipPostCode'
				set @strSql= @strSql +' and vcPostalCode like '''+@ColumnSearch  +'%'''                            
		   else		                      
				set @strSql= @strSql +' and '+@ColumnName+' like '''+@ColumnSearch  +'%'''                            
        END   
                          
end                          
   
	if (@SortColumnName<>'')                        
	begin                           
		if @vcListItemType1='LI'                               
		begin                                
			set @strSql= @strSql +' order by L2.vcData '+@columnSortOrder                              
		end                              
		else if @vcListItemType1='S'                               
		begin                                  
			set @strSql= @strSql +' order by S2.vcState '+@columnSortOrder                              
		end                              
		else if @vcListItemType1='T'                               
		begin                              
			set @strSql= @strSql +' order by T2.vcTerName '+@columnSortOrder                              
		end  
		else  
       BEGIN     
		   IF @SortColumnName='vcStreet' OR @SortColumnName='vcBillStreet' OR @SortColumnName='vcShipStreet'
				set @strSql= @strSql +' order by vcStreet ' +@columnSortOrder                            
		   ELSE IF @SortColumnName='vcCity' OR @SortColumnName='vcBillCity' OR @SortColumnName='vcShipCity'
				set @strSql= @strSql +' order by vcCity ' +@columnSortOrder                            
		   ELSE IF @SortColumnName='vcPostalCode' OR @SortColumnName='vcBillPostCode' OR @SortColumnName='vcShipPostCode'
				set @strSql= @strSql +' order by vcPostalCode ' +@columnSortOrder
		   else if @SortColumnName='numRecOwner'
				set @strSql= @strSql + ' order by dbo.fn_GetContactName(ADC.numRecOwner) ' +@columnSortOrder
			ELSE IF @SortColumnName='Opp.vcLastSalesOrderDate'
				SET @strSql= @strSql + ' order by (' + CONCAT('SELECT MAX(bintCreatedDate) AS bintCreatedDate FROM OpportunityMaster WHERE numDomainID = ',@numDomainID,' AND numDivisionID=DM.numDivisionID AND tintOppType = 1 AND tintOppStatus = 1') + ') ' +@columnSortOrder	                            
		   else		                      
				set @strSql= @strSql +' order by '+ @SortColumnName +' ' +@columnSortOrder                            
        END                        
	END
	ELSE 
		set @strSql= @strSql +' order by  ADC.numContactID asc '
IF(@Where <>'DUP')
BEGIN
    PRINT @strSql

	INSERT INTO #temptable (numcontactid)
	EXEC( @strSql)
	
	SET @strSql = ''
END

SET @bitBillAddressJoinAdded=0;
SET @bitShipAddressJoinAdded=0;
SET @bitContactAddressJoinAdded=0;
set @tintOrder=0;
set @WhereCondition ='';
set @strSql='select ADC.numContactId,DM.numDivisionID,DM.numTerID,ADC.numRecOwner,DM.tintCRMType '                              
DECLARE @Prefix AS VARCHAR(5)
DECLARE @bitCustom BIT
DECLARE @numFieldGroupID AS INT
DECLARE @vcColumnName AS VARCHAR(200)
DECLARE @numFieldID AS NUMERIC(18,0)

	DECLARE @TEMPSelectedColumns TABLE
	(
		numFieldID NUMERIC(18,0)
		,bitCustomField BIT
		,tintOrder INT
		,intColumnWidth FLOAT
	)
	DECLARE @vcLocationID VARCHAR(100) = '0'
	SELECT @vcLocationID =isnull(vcLocationID,'0') from dynamicFormMaster where numFormID=1    

	-- IF USER HAS SELECTED ITS OWN DISPLAY COLUMNS THEN USE IT
	IF LEN(ISNULL(@vcDisplayColumns,'')) > 0
	BEGIN
		DECLARE @TempIDs TABLE
		(
			ID INT IDENTITY(1,1),
			vcFieldID VARCHAR(300)
		)

		INSERT INTO @TempIDs
		(
			vcFieldID
		)
		SELECT 
			OutParam 
		FROM 
			SplitString(@vcDisplayColumns,',')

		INSERT INTO @TEMPSelectedColumns
		(
			numFieldID,
			bitCustomField,
			tintOrder,
			intColumnWidth
		)
		SELECT
			numFormFieldID
			,bitCustomField
			,(SELECT (ID-1) FROM @TempIDs T3 WHERE T3.vcFieldID = TEMP.vcFieldID)
			,0
		FROM
		(
			SELECT  
				numFieldID as numFormFieldID,
				0 bitCustomField,
				CONCAT(numFieldID,'~0') vcFieldID
			FROM    
				View_DynamicDefaultColumns
			WHERE   
				numFormID = 1
				AND bitInResults = 1
				AND bitDeleted = 0 
				AND numDomainID = @numDomainID
			UNION 
			SELECT  
				c.Fld_id AS numFormFieldID
				,1 bitCustomField
				,CONCAT(Fld_id,'~1') vcFieldID
			FROM    
				CFW_Fld_Master C 
			LEFT JOIN 
				CFW_Validation V ON V.numFieldID = C.Fld_id
			JOIN 
				CFW_Loc_Master L on C.GRP_ID=L.Loc_Id
			WHERE   
				C.numDomainID = @numDomainID
				AND GRP_ID IN (SELECT ID FROM dbo.SplitIDs(@vcLocationID,','))
		) TEMP
		WHERE
			vcFieldID IN (SELECT vcFieldID FROM @TempIDs)
	END

	-- IF USER SELECTED DISPLAY COLUMNS IS NOT AVAILABLE THAN GET COLUMNS CONFIGURED FROM SEARCH RESULT GRID
	IF (SELECT COUNT(*) FROM @TEMPSelectedColumns) = 0
	BEGIN
		INSERT INTO @TEMPSelectedColumns
		(
			numFieldID,
			bitCustomField,
			tintOrder,
			intColumnWidth
		)
		SELECT
			numFormFieldID
			,bitCustomField
			,tintOrder
			,intColumnWidth
		FROM
		(
			SELECT  
				A.numFormFieldID,
				0 bitCustomField,
				A.tintOrder,
				A.intColumnWidth
			FROM    AdvSerViewConf A
					JOIN View_DynamicDefaultColumns D ON D.numFieldID = A.numFormFieldID
			WHERE   A.numFormID = 1
					AND D.bitInResults = 1
					AND D.bitDeleted = 0
					AND D.numDomainID = @numDomainID 
					AND D.numFormID = 1
					AND A.numDomainID = @numDomainID
					AND A.numUserCntID=@numUserCntID
					AND ISNULL(A.bitCustom,0) = 0
			UNION
			SELECT  
				A.numFormFieldID,
				1 bitCustomField,
				A.tintOrder,
				A.intColumnWidth
			FROM    AdvSerViewConf A
					JOIN CFW_Fld_Master C ON C.Fld_id = A.numFormFieldID
			WHERE   A.numFormID = 1
					AND C.numDomainID = @numDomainID 
					AND A.numDomainID = @numDomainID
					AND A.numUserCntID=@numUserCntID
					AND ISNULL(A.bitCustom,0) = 1
		) T1
		ORDER BY
			 tintOrder
	END

	-- IF USER HASN'T CONFIGURED COLUMNS FROM SEARCH RESULT GRID THEN RETURN DEFAULT COLUMNS
	IF (SELECT COUNT(*) FROM @TEMPSelectedColumns) = 0
	BEGIN
		INSERT INTO @TEMPSelectedColumns
		(
			numFieldID,
			bitCustomField,
			tintOrder,
			intColumnWidth
		)
		SELECT  
			numFieldID,
			0,
			1,
			0
		FROM    
			View_DynamicDefaultColumns
		WHERE   
			numFormID = 1
			AND numDomainID = @numDomainID
			AND numFieldID = 3
	END

	SELECT TOP 1
		@tintOrder=tintOrder,
		@numFieldID=numFieldID,
		@vcDbColumnName=vcDbColumnName,
		@vcFormFieldName=vcFieldName,
		@vcAssociatedControlType=vcAssociatedControlType,
		@vcListItemType=vcListItemType,
		@numListID=numListID,
		@vcLookBackTableName=vcLookBackTableName,
		@bitCustom=bitCustom,
		@vcOrigDbColumnName=vcOrigDbColumnName,
		@bitAllowSorting=bitAllowSorting,
		@bitAllowEdit= bitAllowEdit,
		@ListRelID=ListRelID,
        @intColumnWidth=intColumnWidth,
		@bitAllowFiltering=bitAllowFiltering,
		@vcfieldatatype=vcFieldDataType,
		@numFieldGroupID=numGroupID
	FROM
	(
		SELECT
			T1.tintOrder+1 tintOrder,
			D.numFieldID,
			vcDbColumnName,
			vcFieldName,
			vcAssociatedControlType,
			vcListItemType,
			numListID,
			vcLookBackTableName,
			0 AS bitCustom,
			vcOrigDbColumnName,
			bitAllowSorting,
			bitAllowEdit,			
			ListRelID,			
			T1.intColumnWidth as intColumnWidth,
			bitAllowFiltering,
			vcFieldDataType,			
			0 AS numGroupID
		FROM 
			View_DynamicDefaultColumns D
		INNER JOIN
			@TEMPSelectedColumns T1
		ON 
			D.numFieldID=T1.numFieldID                                                     
		WHERE 
			D.numDomainID = @numDomainID 
			AND D.numFormID = 1 
			AND ISNULL(T1.bitCustomField,0) = 0
			AND d.vcDbColumnName not in ('numBillShipCountry','numBillShipState')
		UNION
		SELECT
			T1.tintOrder+1 tintOrder,
			Fld_id AS numFieldID,
			CONCAT('Cust',Fld_id),
			Fld_label,
			c.Fld_type,
			'',
			c.numlistid,
			'',			
			1 AS bitCustom,
			'',
			null,
			null,
			null,
			null,
			null,
			'',
			Grp_id
		FROM    
			CFW_Fld_Master C 
		INNER JOIN
			@TEMPSelectedColumns T1
		ON 
			C.Fld_id=T1.numFieldID 
		WHERE   
			C.numDomainID = @numDomainID 
			AND ISNULL(T1.bitCustomField,0) = 1
	) T1
	ORDER BY 
		tintOrder asc                              

while @tintOrder>0                              
begin                              
     
	
    IF @vcLookBackTableName = 'AdditionalContactsInformation' 
        SET @Prefix = 'ADC.'
    IF @vcLookBackTableName = 'DivisionMaster' 
        SET @Prefix = 'DM.'
     
	IF @bitCustom = 0 
	BEGIN                           
		SET @vcColumnName= @vcDbColumnName+'~'+ convert(varchar(10),@numFieldId) +'~0'

		if @vcAssociatedControlType='SelectBox'                              
		begin        
		
		print @vcDbColumnName
					IF @vcDbColumnName='numDefaultShippingServiceID'
				BEGIN
					SET @strSql=@strSql+ ' ,ISNULL((SELECT vcShipmentService FROM ShippingService WHERE (numDomainID=DM.numDomainID OR ISNULL(numDomainID,0)=0) AND numShippingServiceID = DM.numDefaultShippingServiceID),'''')' + ' [' + @vcColumnName + ']' 
				END                      
                                              
			 else if @vcListItemType='LI'                               
			  begin    
				  IF @numListID=40--Country
				  BEGIN
	  				set @strSql=@strSql+',L'+ convert(varchar(3),@tintOrder)+'.vcData'+' ['+ @vcColumnName+']'                              
					IF @vcDbColumnName ='numCountry'
					BEGIN
						IF @bitContactAddressJoinAdded=0 
					  BEGIN
							set @WhereCondition= @WhereCondition +' left Join AddressDetails AD on AD.numRecordId= ADC.numContactID and AD.tintAddressOf=1 and AD.tintAddressType=0 AND AD.bitIsPrimary=1 and AD.numDomainID= ADC.numDomainID'	
							SET @bitContactAddressJoinAdded=1;
					  END
					  set @WhereCondition= @WhereCondition +' left Join ListDetails L'+ convert(varchar(3),@tintOrder)+ ' on L'+convert(varchar(3),@tintOrder)+ '.numListItemID=AD.numCountry'	
					END
					ELSE IF @vcDbColumnName ='numBillCountry'
					BEGIN
						IF @bitBillAddressJoinAdded=0 
						BEGIN
							set @WhereCondition= @WhereCondition +' left Join AddressDetails AD1 on AD1.numRecordId= DM.numDivisionID and AD1.tintAddressOf=2 and AD1.tintAddressType=1 AND AD1.bitIsPrimary=1 and AD1.numDomainID= ADC.numDomainID '
					
							SET @bitBillAddressJoinAdded=1;
						END
						set @WhereCondition= @WhereCondition +' left Join ListDetails L'+ convert(varchar(3),@tintOrder)+ ' on L'+convert(varchar(3),@tintOrder)+ '.numListItemID=AD1.numCountry'
					END
					ELSE IF @vcDbColumnName ='numShipCountry'
					BEGIN
						IF @bitShipAddressJoinAdded=0 
						BEGIN
							set @WhereCondition= @WhereCondition +' left Join AddressDetails AD2 on AD2.numRecordId= DM.numDivisionID and AD2.tintAddressOf=2 and AD2.tintAddressType=2 AND AD2.bitIsPrimary=1 and AD2.numDomainID= ADC.numDomainID '
					
							SET @bitShipAddressJoinAdded=1;
						END
						set @WhereCondition= @WhereCondition +' left Join ListDetails L'+ convert(varchar(3),@tintOrder)+ ' on L'+convert(varchar(3),@tintOrder)+ '.numListItemID=AD2.numCountry'
					END
				  END                        
				  ELSE
				  BEGIN
						set @strSql=@strSql+',L'+ convert(varchar(3),@tintOrder)+'.vcData'+' ['+ @vcColumnName +']'                              
						set @WhereCondition= @WhereCondition +' left Join ListDetails L'+ convert(varchar(3),@tintOrder)+ ' on L'+convert(varchar(3),@tintOrder)+ '.numListItemID='+@vcDbColumnName	
				  END
			  end                              
	                      
			  else if @vcListItemType='T'                               
			  begin                              
				  set @strSql=@strSql+',L'+ convert(varchar(3),@tintOrder)+'.vcData'+' ['+ @vcColumnName +']'                              
			   set @WhereCondition= @WhereCondition +' left Join ListDetails L'+ convert(varchar(3),@tintOrder)+ ' on L'+convert(varchar(3),@tintOrder)+ '.numListItemID='+@vcDbColumnName                              
			  end  
			  else if   @vcListItemType='U'                                                     
				begin           
					set @strSql=@strSql+',dbo.fn_GetContactName('+ @Prefix + @vcDbColumnName+') ['+ @vcColumnName +']'
				end   
	
		end  
		ELSE IF @vcAssociatedControlType='ListBox'   
		BEGIN
			if @vcListItemType='S'                               
			  begin                         
				set @strSql=@strSql+',S'+ convert(varchar(3),@tintOrder)+'.vcState'+' ['+ @vcColumnName +']'
				IF @vcDbColumnName ='numState'
				BEGIN
					IF @bitContactAddressJoinAdded=0 
					   BEGIN
							set @WhereCondition= @WhereCondition +' left Join AddressDetails AD on AD.numRecordId= ADC.numContactID and AD.tintAddressOf=1 and AD.tintAddressType=0 AND AD.bitIsPrimary=1 and AD.numDomainID= ADC.numDomainID'	
							SET @bitContactAddressJoinAdded=1;
					  END	
					  set @WhereCondition= @WhereCondition +' left join State S'+ convert(varchar(3),@tintOrder)+ ' on S'+convert(varchar(3),@tintOrder)+ '.numStateID=AD.numState'
				END
				ELSE IF @vcDbColumnName ='numBillState'
				BEGIN
					IF @bitBillAddressJoinAdded=0 
					BEGIN
						set @WhereCondition= @WhereCondition +' left Join AddressDetails AD1 on AD1.numRecordId= DM.numDivisionID and AD1.tintAddressOf=2 and AD1.tintAddressType=1 AND AD1.bitIsPrimary=1 and AD1.numDomainID= ADC.numDomainID '
						SET @bitBillAddressJoinAdded=1;
					END
					set @WhereCondition= @WhereCondition +' left join State S'+ convert(varchar(3),@tintOrder)+ ' on S'+convert(varchar(3),@tintOrder)+ '.numStateID=AD1.numState'
				END
				ELSE IF @vcDbColumnName ='numShipState'
				BEGIN
					IF @bitShipAddressJoinAdded=0 
					BEGIN
						set @WhereCondition= @WhereCondition +' left Join AddressDetails AD2 on AD2.numRecordId= DM.numDivisionID and AD2.tintAddressOf=2 and AD2.tintAddressType=2 AND AD2.bitIsPrimary=1 and AD2.numDomainID= ADC.numDomainID '
						SET @bitShipAddressJoinAdded=1;
					END
					set @WhereCondition= @WhereCondition +' left join State S'+ convert(varchar(3),@tintOrder)+ ' on S'+convert(varchar(3),@tintOrder)+ '.numStateID=AD2.numState'
				END
			  end         
		END 
		ELSE IF @vcAssociatedControlType='TextBox'   
		BEGIN
			IF @vcDbColumnName='vcStreet' OR @vcDbColumnName='vcCity' OR @vcDbColumnName='vcPostalCode'
			begin                              
			   set @strSql=@strSql+',AD.'+ @vcDbColumnName+' ['+ @vcColumnName +']'                              
			   IF @bitContactAddressJoinAdded=0 
			   BEGIN
					set @WhereCondition= @WhereCondition +' left Join AddressDetails AD on AD.numRecordId= ADC.numContactID and AD.tintAddressOf=1 and AD.tintAddressType=0 AND AD.bitIsPrimary=1 and AD.numDomainID= ADC.numDomainID'	
					SET @bitContactAddressJoinAdded=1;
			   END
			end 
			ELSE IF @vcDbColumnName='vcBillStreet' OR @vcDbColumnName='vcBillCity' OR @vcDbColumnName='vcBillPostCode'
			begin                              
			   set @strSql=@strSql+',AD1.'+ REPLACE(REPLACE(@vcDbColumnName,'Bill',''),'PostCode','PostalCode') +' ['+ @vcColumnName +']'                              
			   IF @bitBillAddressJoinAdded=0 
			   BEGIN 
	   				set @WhereCondition= @WhereCondition +' left Join AddressDetails AD1 on AD1.numRecordId= DM.numDivisionID and AD1.tintAddressOf=2 and AD1.tintAddressType=1 AND AD1.bitIsPrimary=1 and AD1.numDomainID= ADC.numDomainID '
	   				SET @bitBillAddressJoinAdded=1;
			   END
			end 
			ELSE IF @vcDbColumnName='vcShipStreet' OR @vcDbColumnName='vcShipCity' OR @vcDbColumnName='vcShipPostCode'
			begin                              
			   set @strSql=@strSql+',AD2.'+ REPLACE(REPLACE(@vcDbColumnName,'Ship',''),'PostCode','PostalCode') +' ['+ @vcColumnName +']'                              
			   IF @bitShipAddressJoinAdded=0 
			   BEGIN
	   				set @WhereCondition= @WhereCondition +' left Join AddressDetails AD2 on AD2.numRecordId= DM.numDivisionID and AD2.tintAddressOf=2 and AD2.tintAddressType=2  AND AD2.bitIsPrimary=1 and AD2.numDomainID= ADC.numDomainID '
	   				SET @bitShipAddressJoinAdded=1;
			   END
			end 
			ELSE
			BEGIN
				set @strSql=@strSql+','+ case  when @vcDbColumnName='numAge' then 'year(getutcdate())-year(bintDOB)' when @vcDbColumnName='numDivisionID' then 'DM.numDivisionID' when @vcDbColumnName='bintCreatedDate' then 'dbo.[FormatedDateFromDate](DateAdd(minute, ' + CAST(-@ClientTimeZoneOffset AS VARCHAR) + ',DM.bintCreatedDate),DM.numDomainID)' else @vcDbColumnName end+' ['+ @vcColumnName +']'		
			END
		END
		ELSE IF @vcAssociatedControlType='DateField' and @vcDbColumnName='vcLastSalesOrderDate'
		BEGIN
			SET @WhereCondition = @WhereCondition
											+ ' OUTER APPLY(' + CONCAT('SELECT MAX(bintCreatedDate) AS bintCreatedDate FROM OpportunityMaster WHERE numDomainID = ',@numDomainID,' AND numDivisionID=DM.numDivisionID AND tintOppType = 1 AND tintOppStatus = 1') + ' ) AS TempLastOrder '

			set @strSql=@strSql+','+ CONCAT('ISNULL(dbo.FormatedDateFromDate(DateAdd(minute, ',-@ClientTimeZoneOffset,',TempLastOrder.bintCreatedDate),',@numDomainID,'),'''')') +' ['+ @vcColumnName+']'
		END	                    
		ELSE 
			SET @strSql=@strSql+','+ case  when @vcDbColumnName='numAge' then 'year(getutcdate())-year(bintDOB)' when @vcDbColumnName='numDivisionID' then 'DM.numDivisionID' when @vcDbColumnName='bintCreatedDate' then 'dbo.[FormatedDateFromDate](DM.bintCreatedDate,DM.numDomainID)' else @vcDbColumnName end+' ['+ @vcColumnName +']'
	END
	ELSE IF @bitCustom = 1                
	BEGIN
		--SET @vcColumnName= CONCAT(@vcFormFieldName,'~','Cust',@numFieldId)
		SET @vcColumnName= CONCAT('Cust',@numFieldId,'~',@numFieldId,'~',1)
			
		IF @vcAssociatedControlType = 'TextBox' or @vcAssociatedControlType = 'TextArea'            
		BEGIN
			SET @strSql= @strSql+',CFW'+ convert(VARCHAR(3),@tintOrder)+'.Fld_Value  ['+ @vcColumnName +']'                   
				
			SET @WhereCondition= @WhereCondition 
								+' left Join ' + (CASE WHEN @numFieldGroupID = 1 THEN 'CFW_FLD_Values' WHEN @numFieldGroupID = 4 THEN 'CFW_FLD_Values_Cont' ELSE 'CFW_FLD_Values' END) + ' CFW'+ convert(varchar(3),@tintOrder)
								+ ' on CFW' + convert(varchar(3),@tintOrder) 
								+ '.Fld_Id='+convert(varchar(10),@numFieldId) 
								+ 'and CFW' + convert(varchar(3),@tintOrder) 
								+ '.RecId= (CASE WHEN ' + CAST(@numFieldGroupID AS VARCHAR) + ' = 1 THEN DM.numDivisionId WHEN ' + CAST(@numFieldGroupID AS VARCHAR) + ' = 4 THEN ADC.numContactID ELSE 0 END) '                                                         
		END   
		ELSE IF @vcAssociatedControlType = 'CheckBox'       
		BEGIN
			set @strSql= @strSql+',case when isnull(CFW' + CONVERT(VARCHAR(3), @tintOrder)+ '.Fld_Value,0)='''' then 0 else  isnull(CFW'
											+ CONVERT(VARCHAR(3), @tintOrder)+ '.Fld_Value,0) end ['+ @vcColumnName + ']'
 
			set @WhereCondition= @WhereCondition +' left Join ' + (CASE WHEN @numFieldGroupID = 1 THEN 'CFW_FLD_Values' WHEN @numFieldGroupID = 4 THEN 'CFW_FLD_Values_Cont' ELSE 'CFW_FLD_Values' END) + ' CFW'+ convert(varchar(3),@tintOrder)+ '             
			on CFW'+ convert(varchar(3),@tintOrder)+ '.Fld_Id='+convert(varchar(10),@numFieldId) +'and CFW'+ convert(varchar(3),@tintOrder)+ '.RecId= (CASE WHEN ' + CAST(@numFieldGroupID AS VARCHAR) + ' = 1 THEN DM.numDivisionId WHEN ' + CAST(@numFieldGroupID AS VARCHAR) + ' = 4 THEN ADC.numContactID ELSE 0 END) '                                                     
		END                
		ELSE IF @vcAssociatedControlType = 'DateField'            
		BEGIN
			set @strSql= @strSql+',dbo.FormatedDateFromDate(CFW'+ convert(varchar(3),@tintOrder)+'.Fld_Value,'+convert(varchar(10),@numDomainId)+')  ['+ @vcColumnName +']'                   
			set @WhereCondition= @WhereCondition +' left Join ' + (CASE WHEN @numFieldGroupID = 1 THEN 'CFW_FLD_Values' WHEN @numFieldGroupID = 4 THEN 'CFW_FLD_Values_Cont' ELSE 'CFW_FLD_Values' END) + ' CFW'+ convert(varchar(3),@tintOrder)+ '                 
			on CFW'+ convert(varchar(3),@tintOrder)+ '.Fld_Id='+convert(varchar(10),@numFieldId) +'and CFW'+ convert(varchar(3),@tintOrder)+ '.RecId= (CASE WHEN ' + CAST(@numFieldGroupID AS VARCHAR) + ' = 1 THEN DM.numDivisionId WHEN ' + CAST(@numFieldGroupID AS VARCHAR) + ' = 4 THEN ADC.numContactID ELSE 0 END) '                                                         
		END                
		ELSE IF @vcAssociatedControlType = 'SelectBox'             
		BEGIN
			set @vcDbColumnName = 'DCust'+Convert(varchar(10),@numFieldId)                
			set @strSql=@strSql+',L'+ convert(varchar(3),@tintOrder)+'.vcData'+' ['+ @vcColumnName +']'                                                          
			set @WhereCondition= @WhereCondition +' left Join ' + (CASE WHEN @numFieldGroupID = 1 THEN 'CFW_FLD_Values' WHEN @numFieldGroupID = 4 THEN 'CFW_FLD_Values_Cont' ELSE 'CFW_FLD_Values' END) + ' CFW'+ convert(varchar(3),@tintOrder)+ '                 
				on CFW'+ convert(varchar(3),@tintOrder)+ '.Fld_Id='+convert(varchar(10),@numFieldId) +'and CFW'+ convert(varchar(3),@tintOrder)+ '.RecId= (CASE WHEN ' + CAST(@numFieldGroupID AS VARCHAR) + ' = 1 THEN DM.numDivisionId WHEN ' + CAST(@numFieldGroupID AS VARCHAR) + ' = 4 THEN ADC.numContactID ELSE 0 END) '                                                        
			set @WhereCondition= @WhereCondition +' left Join ListDetails L'+ convert(varchar(3),@tintOrder)+ ' on L'+convert(varchar(3),@tintOrder)+ '.numListItemID=CFW'+ convert(varchar(3),@tintOrder)+'.Fld_Value'                
		END         
		ELSE IF @vcAssociatedControlType = 'CheckBoxList'
		BEGIN
			PRINT 'Custom1'

			SET @strSql= @strSql+',(SELECT STUFF((SELECT CONCAT('', '', vcData) FROM ListDetails WHERE numListID= ' + CAST(@numListID AS varchar) + ' AND numlistitemid IN (SELECT ISNULL(Id,0) FROM dbo.SplitIDs(CFW'+ convert(VARCHAR(3),@tintOrder)+'.Fld_Value,'','')) FOR XML PATH('''')), 1, 1, ''''))  ['+ @vcColumnName +']'   

			SET @WhereCondition= @WhereCondition 
								+' left Join ' + (CASE WHEN @numFieldGroupID = 1 THEN 'CFW_FLD_Values' WHEN @numFieldGroupID = 4 THEN 'CFW_FLD_Values_Cont' ELSE 'CFW_FLD_Values' END) + ' CFW'+ convert(varchar(3),@tintOrder)
								+ ' on CFW' + convert(varchar(3),@tintOrder) 
								+ '.Fld_Id='+convert(varchar(10),@numFieldId) 
								+ 'and CFW' + convert(varchar(3),@tintOrder) 
								+ '.RecId= (CASE WHEN ' + CAST(@numFieldGroupID AS VARCHAR) + ' = 1 THEN DM.numDivisionId WHEN ' + CAST(@numFieldGroupID AS VARCHAR) + ' = 4 THEN ADC.numContactID ELSE 0 END) '										
		
		END        
	END          
    
	SELECT TOP 1
		@tintOrder=tintOrder,
		@numFieldID=numFieldID,
		@vcDbColumnName=vcDbColumnName,
		@vcFormFieldName=vcFieldName,
		@vcAssociatedControlType=vcAssociatedControlType,
		@vcListItemType=vcListItemType,
		@numListID=numListID,
		@vcLookBackTableName=vcLookBackTableName,
		@bitCustom=bitCustom,
		@vcOrigDbColumnName=vcOrigDbColumnName,
		@bitAllowSorting=bitAllowSorting,
		@bitAllowEdit= bitAllowEdit,
		@ListRelID=ListRelID,
        @intColumnWidth=intColumnWidth,
		@bitAllowFiltering=bitAllowFiltering,
		@vcfieldatatype=vcFieldDataType,
		@numFieldGroupID=numGroupID
	FROM
	(
		SELECT
			T1.tintOrder+1 tintOrder,
			D.numFieldID,
			vcDbColumnName,
			vcFieldName,
			vcAssociatedControlType,
			vcListItemType,
			numListID,
			vcLookBackTableName,
			0 AS bitCustom,
			vcOrigDbColumnName,
			bitAllowSorting,
			bitAllowEdit,			
			ListRelID,			
			T1.intColumnWidth as intColumnWidth,
			bitAllowFiltering,
			vcFieldDataType,			
			0 AS numGroupID
		FROM 
			View_DynamicDefaultColumns D
		INNER JOIN
			@TEMPSelectedColumns T1
		ON 
			D.numFieldID=T1.numFieldID                                                     
		WHERE 
			D.numDomainID = @numDomainID 
			AND D.numFormID = 1 
			AND ISNULL(T1.bitCustomField,0) = 0
			AND d.vcDbColumnName not in ('numBillShipCountry','numBillShipState')
			AND T1.tintOrder > @tintOrder-1
		UNION
		SELECT
			T1.tintOrder+1 tintOrder,
			Fld_id AS numFieldID,
			CONCAT('Cust',Fld_id),
			Fld_label,
			c.Fld_type,
			'',
			c.numlistid,
			'',			
			1 AS bitCustom,
			'',
			null,
			null,
			null,
			null,
			null,
			'',
			Grp_id
		FROM    
			CFW_Fld_Master C 
		INNER JOIN
			@TEMPSelectedColumns T1
		ON 
			C.Fld_id=T1.numFieldID 
		WHERE   
			C.numDomainID = @numDomainID 
			AND ISNULL(T1.bitCustomField,0) = 1
			AND T1.tintOrder > @tintOrder-1
	) T1
	ORDER BY 
		tintOrder asc                    
                                      
 if @@rowcount=0 set @tintOrder=0                              
end                              

                                                                  
  declare @firstRec as integer                                                                    
  declare @lastRec as integer                                                                    
 set @firstRec= (@CurrentPage-1) * @PageSize                      
 set @lastRec= (@CurrentPage*@PageSize+1)                                                                     
set @TotRecs=(select count(*) from #tempTable)                                                   
                                          
if @GetAll=0         
begin        
        
 if @bitMassUpdate=1            
 begin  

   Declare @strReplace as varchar(8000) = ''
    set @strReplace = case when @LookTable='DivisionMaster' then 'DM.numDivisionID' when @LookTable='AdditionalContactsInformation' then 'ADC.numContactId' when @LookTable='ContactAddress' then 'ADC.numContactId' when @LookTable='ShipAddress' then 'DM.numDivisionID' when @LookTable='BillAddress' then 'DM.numDivisionID' when @LookTable='CompanyInfo' then 'C.numCompanyId' end           
   +' FROM AdditionalContactsInformation ADC inner join DivisionMaster DM on ADC.numDivisionID = DM.numDivisionID                  
   JOIN CompanyInfo C ON DM.numCompanyID = C.numCompanyId                                
    '+@WhereCondition        
    set @strMassUpdate=replace(@strMassUpdate,'@$replace',@strReplace)      
    PRINT CONCAT('MassUPDATE:',@strMassUpdate)
  exec (@strMassUpdate)         
 end         
 
 
 IF (@Where = 'DUP')
 BEGIN
	set @strSql=@strSql+' FROM AdditionalContactsInformation ADC
	  JOIN DivisionMaster DM ON ADC.numDivisionId = DM.numDivisionID
	  JOIN CompanyInfo C ON DM.numCompanyID = C.numCompanyId    
	  JOIN OpportunityMaster OM on OM.numDivisionID= DM.numDivisionID                  
	  inner join #tempTable T on T.numContactId=ADC.numContactId ' 
	  + @WhereCondition +'
	  WHERE ID > '+convert(varchar(10),@firstRec)+ ' and ID <'+ convert(varchar(10),@lastRec) +  @WCondition + ' order by T.ID, C.vcCompanyName' -- @OrderBy
	  PRINT @strSql
	  exec(@strSql)                                                                 
	 drop table #tempTable
	 RETURN	
 END
ELSE
BEGIN
	SET @strSql = @strSql + ' FROM AdditionalContactsInformation ADC
		JOIN DivisionMaster DM ON ADC.numDivisionId = DM.numDivisionID   
	  JOIN CompanyInfo C ON DM.numCompanyID = C.numCompanyId                              
	   '+@WhereCondition+' inner join #tempTable T on T.numContactId=ADC.numContactId                                                               
	  WHERE ID > '+convert(varchar(10),@firstRec)+ ' and ID <'+ convert(varchar(10),@lastRec)
	  
	   
	SET @strSql=@strSql + ' order by T.ID, C.vcCompanyName ' 
END
             
end                           
 else        
 begin                      
     set @strSql=@strSql+' FROM AdditionalContactsInformation ADC                                                   
  JOIN DivisionMaster DM ON ADC.numDivisionId = DM.numDivisionID                                                                   
  JOIN CompanyInfo C ON DM.numCompanyID = C.numCompanyId                              
  '+@WhereCondition+' join #tempTable T on T.numContactId=ADC.numContactId'        
  
	   SET @strSql=@strSql + ' order by T.ID, C.vcCompanyName'   
	end       
print @strSql                                                      
exec(@strSql) 
drop table #tempTable


	SELECT
		tintOrder,
		vcDbColumnName,
		vcFieldName,
		vcAssociatedControlType,
		vcListItemType,
		numListID,
		vcLookBackTableName,
		numFieldID,
		numFieldID AS numFormFieldID,
		intColumnWidth,
		bitCustom AS bitCustomField,
		vcOrigDbColumnName,
		bitAllowSorting,
		bitAllowEdit,
		ListRelID,
		bitAllowFiltering,
		vcFieldDataType
	FROM
	(
		SELECT
			T1.tintOrder+1 tintOrder,
			D.numFieldID,
			vcDbColumnName,
			vcFieldName,
			vcAssociatedControlType,
			vcListItemType,
			numListID,
			vcLookBackTableName,
			0 AS bitCustom,
			vcOrigDbColumnName,
			bitAllowSorting,
			bitAllowEdit,			
			ListRelID,			
			T1.intColumnWidth as intColumnWidth,
			bitAllowFiltering,
			vcFieldDataType,			
			0 AS numGroupID
		FROM 
			View_DynamicDefaultColumns D
		INNER JOIN
			@TEMPSelectedColumns T1
		ON 
			D.numFieldID=T1.numFieldID                                                     
		WHERE 
			D.numDomainID = @numDomainID 
			AND D.numFormID = 1 
			AND ISNULL(T1.bitCustomField,0) = 0
			AND d.vcDbColumnName not in ('numBillShipCountry','numBillShipState')
		UNION
		SELECT
			T1.tintOrder+1 tintOrder,
			Fld_id AS numFieldID,
			CONCAT('Cust',Fld_id),
			Fld_label,
			c.Fld_type,
			'',
			c.numlistid,
			'',			
			1 AS bitCustom,
			'',
			null,
			null,
			null,
			null,
			null,
			'',
			Grp_id
		FROM    
			CFW_Fld_Master C 
		INNER JOIN
			@TEMPSelectedColumns T1
		ON 
			C.Fld_id=T1.numFieldID 
		WHERE   
			C.numDomainID = @numDomainID 
			AND ISNULL(T1.bitCustomField,0) = 1
	) T1
	ORDER BY 
		tintOrder asc       

IF OBJECT_ID('tempdb..#tempAddress') IS NOT NULL
BEGIN
    DROP TABLE #tempAddress
END
END