/****** Object:  StoredProcedure [dbo].[USP_PurchaseItems]    Script Date: 07/26/2008 16:20:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj      
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_purchaseitems')
DROP PROCEDURE usp_purchaseitems
GO
CREATE PROCEDURE [dbo].[USP_PurchaseItems]      
@numDomainID as numeric(9)=0,      
@numDivisionID as numeric(9)=0      
as      
      
select I.numItemCode,vcItemName,monListPrice as Price,vcSKU as SKU,vcModelID as [Model ID] from item I      
join Vendor V      
on V.numItemCode=I.numItemCode      
where V.numVendorID=@numDivisionID
GO
