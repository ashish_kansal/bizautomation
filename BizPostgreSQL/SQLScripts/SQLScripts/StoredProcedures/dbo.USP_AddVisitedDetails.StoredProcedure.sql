/****** Object:  StoredProcedure [dbo].[USP_AddVisitedDetails]    Script Date: 07/26/2008 16:14:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--exec USP_AddVisitedDetails @numRecordID = 3053, @chrRecordType = 'C', @numUserCntID = 0
--created by anoop jayaraj        
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_addvisiteddetails')
DROP PROCEDURE usp_addvisiteddetails
GO
CREATE PROCEDURE [dbo].[USP_AddVisitedDetails]        
@numRecordID as numeric(9)=0,        
@chrRecordType as char(1)='',        
@numUserCntID as numeric(9)=0        
as        
IF @numUserCntID = 0 
	RETURN
 --Commented by chintan as it creates junk data which not being used any where else. so its better to delete row insted seting bitdeleted=1
--update    RecentItems set bitdeleted = 1 where numRecordID=@numRecordID  and chrRecordType= @chrRecordType
DELETE [RecentItems] WHERE numRecordID=@numRecordID  and chrRecordType= @chrRecordType AND [numUserCntID]=@numUserCntID

declare @numCheckRec as numeric(9)        
        
--select top 1 @numCheckRec=numRecordID  from RecentItems        
--where numUserCntID=@numUserCntID  order by numRecentItemsID desc         
--if @@rowcount=0 set @numCheckRec=0   
--begin       
-- if @numCheckRec<>@numRecordID        
-- begin          
  insert into RecentItems(numRecordID,chrRecordType,bintVisitedDate,numUserCntID)        
  values(@numRecordID,@chrRecordType,getutcdate(),@numUserCntID)        
-- end        
--end
GO
