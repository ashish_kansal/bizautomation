/****** Object:  StoredProcedure [dbo].[usp_GetContactInformation]    Script Date: 07/26/2008 16:16:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getcontactinformation')
DROP PROCEDURE usp_getcontactinformation
GO
CREATE PROCEDURE [dbo].[usp_GetContactInformation]    
 @numContactID numeric=0  
AS    
 SELECT vcLastName, vcFirstName,vcGivenName, vcPosition, vcEmail, vcDepartment,    
    numPhone, numPhoneExtension, vcFax, numContacttype,   vcData ,  
    vcState, vcCity, vcStreet, vcCountry, intPostalCode,    
    AC.numDivisionId, bintDOB, txtNotes, AC.numCreatedBy,VcAsstFirstName+ ' '+vcAsstLastName as AsstName,numCell,NumHomePhone,  numAsstPhone,  
   DM.numTerID, --New    
   DM.numCreatedBy --New    
   FROM AdditionalContactsInformation AC join     
   divisionmaster DM on DM.numDivisionID=AC.numDivisionID
   left join contactAddress Addc on  Addc.numContactID=AC.numContactID  
 left join ListDetails lst on ac.numContacttype=lst.numListItemID  
   WHERE AC.numContactID=@numContactID
GO
