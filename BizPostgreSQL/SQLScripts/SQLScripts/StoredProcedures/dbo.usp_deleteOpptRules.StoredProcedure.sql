/****** Object:  StoredProcedure [dbo].[usp_deleteOpptRules]    Script Date: 07/26/2008 16:15:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
--This procedure is for deleting the OpptRules
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_deleteopptrules')
DROP PROCEDURE usp_deleteopptrules
GO
CREATE PROCEDURE [dbo].[usp_deleteOpptRules]

@numRuleId numeric(18),
@numDetsDel numeric=0   
--

AS

--DELETE FROM THE DETAIL TABLE
IF @numDetsDel=0  -- If the value is 1 then it means that the details have to be
--deleted from detail table only
BEGIN	
	DELETE FROM OpptRuleDetails WHERE numruleid=@numRuleId
	DELETE FROM OpptRuleMaster WHERE numruleid=@numRuleId
END
ELSE
BEGIN
	DELETE FROM OpptRuleDetails WHERE numruleid=@numRuleId
END
GO
