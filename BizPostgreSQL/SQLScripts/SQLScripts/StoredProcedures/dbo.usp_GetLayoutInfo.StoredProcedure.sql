/****** Object:  StoredProcedure [dbo].[usp_GetLayoutInfo]    Script Date: 07/26/2008 16:17:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getlayoutinfo')
DROP PROCEDURE usp_getlayoutinfo
GO
CREATE PROCEDURE [dbo].[usp_GetLayoutInfo]                      
@numUserCntId as numeric(9)=0,                    
@intcoulmn as int,                    
@numDomainID as numeric(9)=0 ,          
@Ctype as char                     
as              
            
 if( @intcoulmn <> 0 )                
begin               
                 
SELECT DTL.intCoulmn,PL.vcFieldName,DTL.numFieldId  FROM PageLayoutDTL DTL                     
join PageLayout PL on DTL.numFieldId=PL.numFieldId                
WHERE DTL.numUserCntID=@numUserCntId and  DTL.numDomainID=@numDomainID  and DTL.intcoulmn = @intcoulmn and PL.Ctype=@Ctype                
order by DTL.intCoulmn                
     
           
end              
              
                
if @intcoulmn = 0                
begin                
            
select vcfieldName,numFieldId from PageLayout where CType = @Ctype and numFieldId not in           
(select DTL.numFieldID from PageLayoutDTL DTL join Pagelayout PL on Pl.numFieldId = DTL.numFieldId where Pl.Ctype=@Ctype and DTL.numUserCntID=@numUserCntId and  DTL.numDomainID=@numDomainID ) order by intColumn,tintrow               
            
end
GO
