--
GO
/****** Object:  StoredProcedure [dbo].[usp_SaveAssets]    Script Date: 02/08/2010 20:33:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_saveassets')
DROP PROCEDURE usp_saveassets
GO
CREATE PROCEDURE [dbo].[usp_SaveAssets]  
@numDivId as numeric,  
@strItems as varchar(8000),  
@numDomainId as numeric,
@intType as int=1,
@numItemCode as numeric(18),
@numDeptId as numeric(18),
@btApp as bit,
@btDep as bit,
@numAppValue as numeric(18,2),
@numDepValue as numeric(18,2),
@dtPurchaseDate as datetime,
@dtWarrentyDate as datetime
 
  
  
as   
-- delete  CompanyAssets where numDivId = @numDivId and numDomainId = @numDomainId       
  DECLARE @hDocItem int    
  declare @Count int
if @intType=0
	begin
		  if convert(varchar(10),@strItems) <>''                                                            
		 begin     
		  EXEC sp_xml_preparedocument @hDocItem OUTPUT, @strItems    
		       
			 insert into         
			 CompanyAssets                                                            
			 (numDivId,numDomainId ,numWarehouseItmsDtlID,numItemCode,bitSerializable,numUnit,numOppId)        
			 select @numDivId,@numDomainId,isnull(X.numWareHouseItmsDtlId,0),x.numItemCode,x.[Type],X.Unit,X.numOppId from(                                                            
			 SELECT *FROM OPENXML (@hDocItem,'/NewDataSet/Table',2)                                                            
			 WITH         
			 (                                                            
			numWareHouseItmsDtlId numeric(9),       
			numItemCode numeric(9),  
			type bit,    
			unit numeric(9),
			numOppId numeric(9)                                                           
			 ))X        
		  EXEC sp_xml_removedocument @hDocItem        
			 end
	end
else
	begin
	set @Count=0;
	set @Count=isnull((select count(*) from CompanyAssets where numItemCode=@numItemCode),0);
	
	if @Count=0
		begin
			insert into CompanyAssets
			(numDivId,numDomainId ,numWarehouseItmsDtlID,numItemCode,bitSerializable,numUnit,numOppId,numDeptId,btAppreciation,btDepreciation,numAppValue,numDepValue,dtPurchase,dtWarrentyTill) 
			select @numDivId,@numDomainId,0,@numItemCode,0,0,0,@numDeptId,@btApp,@btDep,@numAppValue,@numDepValue,@dtPurchaseDate,@dtWarrentyDate
		end
	else
		begin
			update CompanyAssets
				set numDivId=@numDivId,
					numDeptId=@numDeptId,
					btAppreciation=@btApp,
					btDepreciation=@btDep,
					numAppValue=@numAppValue,
					numDepValue=@numDepValue,
					dtPurchase=@dtPurchaseDate,
					dtWarrentyTill=@dtWarrentyDate
			where numItemCode=@numItemCode;
		end
	end
