/****** Object:  StoredProcedure [dbo].[USP_GetLeadList]    Script Date: 07/26/2008 16:17:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--- Created By Anoop Jayaraj                                                  
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getleadlist')
DROP PROCEDURE usp_getleadlist
GO
CREATE PROCEDURE [dbo].[USP_GetLeadList]                                                            
 @numGrpID numeric,                                                                
 @numUserCntID numeric,                                                                
 @tintUserRightType tinyint,                                                                
 @CustName varchar(100)='',                                                                
 @FirstName varChar(100)= '',                                                                
 @LastName varchar(100)='',                                                              
 @SortChar char(1)='0' ,                                                              
 @numFollowUpStatus as numeric(9)=0 ,                                                              
 @CurrentPage int,                                                              
 @PageSize int,                                                              
 @TotRecs int output,                                                              
 @columnName as Varchar(50)='',                                                              
 @columnSortOrder as Varchar(10)='',                                      
 @ListType as tinyint=0,                                      
 @TeamType as tinyint=0,                                       
 @TeamMemID as numeric(9)=0,                                  
 @numDomainID as numeric(9)=0    ,    
@ClientTimeZoneOffset as int  ,
@bitPartner as bit                                                                    
                                                                
AS                                                                
declare @strSql as varchar(7100)                                                               
Create table #tempTable  (ID INT IDENTITY PRIMARY KEY ,                                                            
 numDivisionID numeric(9)  )                                                                      
                                                              
                                                
 set @strSql= 'SELECT                                                                                   
   DM.numDivisionID                                                                          
  FROM                                                                  
   CompanyInfo cmp join divisionmaster DM on cmp.numCompanyID=DM.numCompanyID                                   
 left join ListDetails lst on lst.numListItemID=DM.numFollowUpStatus                                   
 join AdditionalContactsInformation ADC on ADC.numDivisionID=DM.numDivisionID      
 left join AdditionalContactsInformation ADC1 on ADC1.numContactID=DM.numAssignedTo    
 left join AdditionalContactsInformation ADC2 on ADC2.numContactID=DM.numRecOwner                                                                         
  WHERE                                                                 
   DM.tintCRMType=0  and ISNULL(ADC.bitPrimaryContact,0)=1                                                                                        
 AND DM.numGrpID='+convert(varchar(15),@numGrpID)+ '                                   
 AND DM.numDomainID='+convert(varchar(15),@numDomainID)+ '' 

if @bitPartner = 1 set @strSql=@strSql+' and (DM.numAssignedTo='+convert(varchar(15),@numUserCntID)+ 
											' or DM.numCreatedBy='+convert(varchar(15),@numUserCntID)+ ') '
                                                                      
 if @FirstName<>'' set @strSql=@strSql+' and ADC.vcFirstName  like '''+@FirstName+'%'''                              
 if @LastName<>'' set @strSql=@strSql+' and ADC.vcLastName like '''+@LastName+'%'''                              
 if @CustName<>'' set @strSql=@strSql+' and CMP.vcCompanyName like '''+@CustName+'%'''                                                             
                                                                     
if @numFollowUpStatus<>0 set @strSql=@strSql + '  AND DM.numFollowUpStatus='+convert(varchar(15),@numFollowUpStatus)                                                               
if @SortChar<>'0' set @strSql=@strSql + ' And CMP.vcCompanyName like '''+@SortChar+'%'''              
if @tintUserRightType=1 set @strSql=@strSql + ' AND (DM.numRecOwner = '+convert(varchar(15),@numUserCntID)+ ' or DM.numAssignedTo='+convert(varchar(15),@numUserCntID)+ ')'                                                                
else if @tintUserRightType=2 set @strSql=@strSql + ' AND (DM.numTerID in (select numTerritoryID from  UserTerritory where numUserCntID= '+convert(varchar(15),@numUserCntID)+' ) or DM.numTerID=0 or DM.numAssignedTo='+convert(varchar(15),@numUserCntID)+ ') 
 
'    
      
                                             
if @ListType=0                                     
begin                                      
if @numGrpID=1 set @strSql=@strSql + ' AND (DM.numRecOwner = '+convert(varchar(15),@numUserCntID)+ ' or DM.numAssignedTo='+convert(varchar(15),@numUserCntID)+ ')'                                                   
if @numGrpID=5 set @strSql=@strSql + ' AND (DM.numRecOwner = '+convert(varchar(15),@numUserCntID)+ ' or DM.numAssignedTo='+convert(varchar(15),@numUserCntID)+ ')'                                        
end                            
                        
else if @ListType=1                                       
begin                                     
if @TeamMemID=0                                     
 begin                                    
 if @numGrpID<>2 set @strSql=@strSql + '  AND (DM.numRecOwner  in(select numContactID from  AdditionalContactsInformation                        
where numTeam in(select numTeam from ForReportsByTeam where numUserCntID='+convert(varchar(15),@numUserCntID)+' and tintType='+convert(varchar(15),@TeamType)+') ) or DM.numAssignedTo in(select numContactID from  AdditionalContactsInformation              
  
    
          
where numTeam in(select numTeam from ForReportsByTeam where numUserCntID='+convert(varchar(15),@numUserCntID)+' and tintType='+convert(varchar(15),@TeamType)+')))'                                                       
                                     
 end                                    
else                                 
 begin                                    
 if @numGrpID<>2 set @strSql=@strSql + '  AND ( DM.numRecOwner='+convert(varchar(15),@TeamMemID) +'  or  DM.numAssignedTo='+convert(varchar(15),@TeamMemID) +') '                                          
                                     
 end                                     
end                                      
                                      
set  @strSql=@strSql + ' order by ' + @columnName +' '+ @columnSortOrder                        
                      
                      
print @strSql                                              
insert into #tempTable(numDivisionID)                                              
exec(@strSql)                                              
                                                             
                                                                         
declare @firstRec as integer                                                              
  declare @lastRec as integer                                                              
 set @firstRec= (@CurrentPage-1) * @PageSize                                                             
     set @lastRec= (@CurrentPage*@PageSize+1)                                                              
                                                             
set @TotRecs=(select count(*) from #tempTable)                                                              
                                              
                                              
                                              
                                              
                                              
SELECT                                                                 
   dateadd(minute,-@ClientTimeZoneOffset,DM.bintCreatedDate) as bintCreatedDate,                          
   cmp.numCompanyID,                                                                
   cmp.vcCompanyName,                                                                
   DM.vcDivisionName,                                         
   DM.numDivisionID,                                                                
   ADC.numContactId,                                                
   dbo.fn_GetListItemName(CMP.numNoOfEmployeesId) as Employees,                                         
   ADC.vcFirstName+' ' + ADC.vcLastName as [Name],                                 
   case when ADC.numPhone<>'' then + ADC.numPhone +case when ADC.numPhoneExtension<>'' then ' - ' + ADC.numPhoneExtension else '' end  else '' end as [numPhone],                                                                                       
   ADC.vcEmail,                                                                
   DM.numRecOwner,                                                 
   DM.numGrpID,                                                            
   DM.numTerId,                                                              
   lst.vcdata,                       
   ADC1.vcFirstName+' '+ADC1.vcLastName as vcUserName,                            
   dbo.fn_GetContactName(numAssignedTo)+'/'+dbo.fn_GetContactName(numAssignedBy) AssignedToBy                                  
  FROM                                                                  
   CompanyInfo cmp                                                                 
   join divisionmaster DM                                          
   on cmp.numCompanyID=DM.numCompanyID                                          
   left join ListDetails lst                                           
on lst.numListItemID=DM.numFollowUpStatus                                                                 
   join AdditionalContactsInformation ADC                                          
   on ADC.numDivisionID=DM.numDivisionID                                  
   left join AdditionalContactsInformation ADC1 on ADC1.numContactID=DM.numRecOwner                                          
   join #tempTable T                                          
   on T.numDivisionID=DM.numDivisionID                                    
  WHERE                                                                 
   DM.tintCRMType=0 and ISNULL(ADC.bitPrimaryContact,0)=1                                              
   and ID > @firstRec and ID < @lastRec order by ID                                          
                                                    
                          
                                              
                                              
                                              
drop table #tempTable
GO
