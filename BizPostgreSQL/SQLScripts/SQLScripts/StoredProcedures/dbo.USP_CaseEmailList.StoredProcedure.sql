/****** Object:  StoredProcedure [dbo].[USP_CaseEmailList]    Script Date: 07/26/2008 16:15:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--- Created By Anoop Jayaraj                
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_caseemaillist')
DROP PROCEDURE usp_caseemaillist
GO
CREATE PROCEDURE [dbo].[USP_CaseEmailList]                                 
@tintSortOrder numeric=4,                  
@dtFromDate datetime,                 
@dtToDate datetime,                
@CaseNo Varchar(100)='',                
@SeachKeyword varChar(100)= '',                  
@CurrentPage int,                
@PageSize int,                
@TotRecs int output,                
@columnName as Varchar(50),                
@columnSortOrder as Varchar(10)              
as                
                
               
--Create a Temporary table to hold data                
Create table #tempTable ( ID INT IDENTITY PRIMARY KEY,numEmailHstrID VARCHAR(15))                
                 
                 
                
                
declare @strSql as varchar(5000)                
                
                
declare @SelColumn as varchar(20)      
  set  @SelColumn=','+@columnName      
  if @columnName='numEmailHstrID' set  @SelColumn=''      
 set @strSql='select distinct(X.numEmailHstrID) from (select HDR.numEmailHstrID'+@SelColumn+' from EmailHistory HDR      
  left join EmailHStrToBCCAndCC DTL      
  on DTL.numEmailHstrID=HDR.numEmailHstrID  
join EmailMaster EM on DTL.numEmailId = EM.numEmailId                     
  where (bintCreatedOn between '''+ convert(varchar(20),@dtFromDate)+''' and '''+ convert(varchar(20),@dtToDate)+''')             
and (vcBody like ''%'+@CaseNo+'%''                
or vcSubject like ''%'+@CaseNo +'%'')'      
      
      
if @SeachKeyword<>''      
 begin                                  
   if @tintSortOrder=0  set @strSql=@strSql + '                 
  and (Em.vcEmailId like ''%'+ @SeachKeyword+'%''                                  
   or vcBody like ''%'+@SeachKeyword+'%''                              
   or vcSubject like ''%'+@SeachKeyword +'%'')'                                
  else if @tintSortOrder=2  set @strSql=@strSql + ' and Em.vcEmailId like ''%'+ @SeachKeyword+'%'''                     
  else if @tintSortOrder=3  set @strSql=@strSql + ' and tintType=1 and vcEmail like ''%'+@SeachKeyword+'%'''               
  else if @tintSortOrder=4  set @strSql=@strSql + ' and vcSubject like ''%'+@SeachKeyword+'%'''                
  else if @tintSortOrder=5  set @strSql=@strSql + ' and vcBody like ''%'+@SeachKeyword +'%'''          
 end                   
                 
               
                
          set @strSql=@strSql + ')X ORDER BY X.' + @columnName +' '+ @columnSortOrder    
                                                                               
        print @strSql                             
 insert into #tempTable (numEmailHstrID)       
 exec( @strSql)                 
                
  declare @firstRec as integer                                
  declare @lastRec as integer                                
 set @firstRec= (@CurrentPage-1) * @PageSize                                
     set @lastRec= (@CurrentPage*@PageSize+1)                                
select E.numEmailHstrID,dbo.GetEmaillAdd(E.numEmailHstrID,4) as vcFromEmail, dbo.GetEmaillAdd(E.numEmailHstrID,1) as vcMessageTo,VcSubject, bintCreatedOn,bintCreatedOn as CreatedOn,isnull(convert(varchar(15),numNoofTimes),'-') as numNoofTimes from #tempTable T        
JOIN EmailHistory E        
ON E.numEmailHstrID=T.numEmailHstrID        
where ID > @firstRec and ID < @lastRec                                
set @TotRecs=(select count(*) from #tempTable)                                
drop table #tempTable
GO
