/****** Object:  StoredProcedure [dbo].[USP_GetWareHouseID]    Script Date: 07/26/2008 16:18:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getwarehouseid')
DROP PROCEDURE usp_getwarehouseid
GO
CREATE PROCEDURE [dbo].[USP_GetWareHouseID]
@numWareHouseItemID as numeric(9)
as
select numWareHouseID a from WareHouseItems 
where numWareHouseItemID=@numWareHouseItemID
GO
