/****** Object:  StoredProcedure [dbo].[usp_DeleteSurveyQuestion]    Script Date: 07/26/2008 16:15:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_deletesurveyquestion')
DROP PROCEDURE usp_deletesurveyquestion
GO
CREATE PROCEDURE [dbo].[usp_DeleteSurveyQuestion]
	@numQID numeric=0 
--  		
AS
--This SP will Delete a question for a Survey.
BEGIN
	DECLARE @numCount numeric
	SET @numCount=0

		SELECT @numCount=COUNT(numQuestionID) 
			FROM SurveyResponse 
				WHERE numQuestionID=@numQID
		IF @numCount=0
		BEGIN
	
			DELETE FROM SurveyAnsMaster 
				WHERE numQID=@numQID

			DELETE SurveyQuestionMaster 
				WHERE numQID=@numQID
	
			SELECT 0
		END
		ELSE
		BEGIN
			SELECT 1
		END
END
GO
