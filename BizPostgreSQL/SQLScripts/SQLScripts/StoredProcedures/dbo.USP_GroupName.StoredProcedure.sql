/****** Object:  StoredProcedure [dbo].[USP_GroupName]    Script Date: 07/26/2008 16:18:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_groupname')
DROP PROCEDURE usp_groupname
GO
CREATE PROCEDURE [dbo].[USP_GroupName]
@numGroupID as numeric(9)=0
as

select vcGrpName from Groups where numGrpId=@numGroupID
GO
