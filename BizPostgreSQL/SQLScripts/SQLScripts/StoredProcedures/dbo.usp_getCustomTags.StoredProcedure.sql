/****** Object:  StoredProcedure [dbo].[usp_getCustomTags]    Script Date: 07/26/2008 16:17:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getcustomtags')
DROP PROCEDURE usp_getcustomtags
GO
CREATE PROCEDURE [dbo].[usp_getCustomTags]
as 
select vcMergeField,vcMergeFieldValue from EmailMergeFields
GO
