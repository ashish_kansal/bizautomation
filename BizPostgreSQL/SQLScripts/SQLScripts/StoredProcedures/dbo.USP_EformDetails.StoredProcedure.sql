/****** Object:  StoredProcedure [dbo].[USP_EformDetails]    Script Date: 07/26/2008 16:15:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_eformdetails')
DROP PROCEDURE usp_eformdetails
GO
CREATE PROCEDURE [dbo].[USP_EformDetails]           
@numSpecDocID as numeric(9)=0,          
@strSql as varchar(5000),          
@byteMode as tinyint,        
@vcColumnName as varchar(50)        
as          
declare @str as varchar(5000)          
declare @ContactID as varchar(20)    
declare @DivisionID as varchar(20)    
declare @CompanyID as varchar(20)       
Declare @ListID as varchar(20)        
if @byteMode=0           
begin            
select @ContactID=numRecID from SpecificDocuments where numSpecificDocID=@numSpecDocID          
set @str= 'select ' + @strSql + ' from AdditionalContactsInformation where numContactId=' + @ContactID          
exec (@str)          
end         
        
if @byteMode=1          
begin          
          
select @ContactID=numRecID from SpecificDocuments where numSpecificDocID=@numSpecDocID          
set @str= 'update AdditionalContactsInformation set ' + @strSql + ' where numContactId=' + @ContactID          
exec (@str)         
select 1         
end          
        
if @byteMode=2        
begin          
          
select @ContactID=numRecID from SpecificDocuments where numSpecificDocID=@numSpecDocID         
select @ListID=isnull(numListItemID,0) from listdetails where vcData=@strSql         
if @ListID>0        
begin        
    set @str= 'update AdditionalContactsInformation set ' + @vcColumnName +' = ' + @ListID + ' where numContactId=' + @ContactID          
end        
exec (@str)         
select 1         
end         
        
if @byteMode=3        
begin          
        
select @ContactID=numRecID from SpecificDocuments where numSpecificDocID=@numSpecDocID         
set @str='        
declare @CoulmnValue as varchar(20)        
declare @numListID as varchar(20)        
select @CoulmnValue= isnull( '+@vcColumnName+',0) from AdditionalContactsInformation where numContactID= '+ @ContactID +'        
        
if @CoulmnValue=0        
begin        
 select @numListID=isnull(numListItemID,0) from listdetails where vcData='''+ @strSql +'''        
 if @numListID>0        
 begin        
     update AdditionalContactsInformation set ' + @vcColumnName +' = @numListID where numContactId=' + @ContactID +'        
 end        
         
end'        
exec (@str)         
select 1         
end        
--updating Contacts Customfields      
if @byteMode=4       
begin          
          
select @ContactID=numRecID from SpecificDocuments where numSpecificDocID=@numSpecDocID         
select @ListID=@vcColumnName       
if @ListID>0        
begin        
     update CFW_FLD_Values_Cont set Fld_Value=@strSql where RecId=@ContactID and Fld_ID=@ListID      
end        
exec (@str)         
select 1         
end        
      
if @byteMode=5      
begin          
          
select @ContactID=numRecID from SpecificDocuments where numSpecificDocID=@numSpecDocID         
select @ListID=isnull(numListItemID,0) from listdetails where vcData=@strSql        
if @ListID>0        
begin        
     update CFW_FLD_Values_Cont set Fld_Value=@ListID where RecId=@ContactID and Fld_ID=@vcColumnName      
end        
exec (@str)         
select 1         
end        
      
--updating Contacts Customfields      
if @byteMode=6      
begin          
declare @check as varchar(100)           
select @ContactID=numRecID from SpecificDocuments where numSpecificDocID=@numSpecDocID         
select @ListID=@vcColumnName       
select @check=isnull(fld_value,'') from CFW_FLD_Values_Cont where RecId=@ContactID and Fld_ID=@ListID      
if @check =''      
begin        
     update CFW_FLD_Values_Cont set Fld_Value=@strSql where RecId=@ContactID and Fld_ID=@ListID      
end        
exec (@str)         
select 1         
end       
      
if @byteMode=7      
begin          
 declare @checkCon as varchar(10)        
select @ContactID=numRecID from SpecificDocuments where numSpecificDocID=@numSpecDocID         
select @ListID=isnull(numListItemID,0) from listdetails where vcData=@strSql        
select @checkCon=isnull(fld_value,0) from CFW_FLD_Values_Cont where RecId=@ContactID and Fld_ID=@vcColumnName      
if @checkCon=0        
begin        
     update CFW_FLD_Values_Cont set Fld_Value=@ListID where RecId=@ContactID and Fld_ID=@vcColumnName      
end        
exec (@str)         
select 1         
end   
  
  
--- Updating Division Master and Company Info  
if @byteMode=8           
begin            
select @DivisionID=numRecID from SpecificDocuments where numSpecificDocID=@numSpecDocID          
set @str= 'select ' + @strSql + ' from Divisionmaster div  
join companyinfo com  
on div.numCompanyID= com.numCompanyID  
where numdivisionid=' + @DivisionID          
exec (@str)          
end    
  
if @byteMode=9        
begin          
          
select @DivisionID=numRecID from SpecificDocuments where numSpecificDocID=@numSpecDocID         
select @CompanyID= numCompanyID from divisionmaster where numDivisionID=@DivisionID  
select @ListID=isnull(numListItemID,0) from listdetails where vcData=@strSql         
if @ListID>0        
begin        
    set @str= 'update CompanyInfo set ' + @vcColumnName +' = ' + @ListID + ' where numCompanyId=' + @CompanyID          
end        
exec (@str)         
select 1         
end    
  
if @byteMode=10        
begin          
          
select @DivisionID=numRecID from SpecificDocuments where numSpecificDocID=@numSpecDocID         
select @ListID=isnull(numListItemID,0) from listdetails where vcData=@strSql         
if @ListID>0        
begin        
    set @str= 'update divisionmaster set ' + @vcColumnName +' = ' + @ListID + ' where numDivisionId=' + @DivisionID          
end        
exec (@str)         
select 1         
end    
  
-- updating Division Master Custom Fields  
if @byteMode=11      
begin          
          
select @DivisionID=numRecID from SpecificDocuments where numSpecificDocID=@numSpecDocID         
select @ListID=@vcColumnName       
if @ListID>0        
begin        
     update CFW_FLD_Values set Fld_Value=@strSql where RecId=@DivisionID and Fld_ID=@ListID      
end        
exec (@str)         
select 1         
end   
  
if @byteMode=12     
begin          
          
select @DivisionID=numRecID from SpecificDocuments where numSpecificDocID=@numSpecDocID         
select @ListID=isnull(numListItemID,0) from listdetails where vcData=@strSql        
if @ListID>0        
begin        
     update CFW_FLD_Values set Fld_Value=@ListID where RecId=@DivisionID and Fld_ID=@vcColumnName      
end        
exec (@str)         
select 1         
end    
  
if @byteMode=13          
begin          
          
select @DivisionID=numRecID from SpecificDocuments where numSpecificDocID=@numSpecDocID
select @CompanyID= numCompanyID from divisionmaster where numDivisionID=@DivisionID            
set @str= 'update CompanyInfo set ' + @strSql + ' where numCompanyId=' + @CompanyID          
exec (@str)         
select 1         
end    
  
  
if @byteMode=14        
begin          
          
select @DivisionID=numRecID from SpecificDocuments where numSpecificDocID=@numSpecDocID          
set @str= 'update DivisionMaster set ' + @strSql + ' where numDivisionId=' + @DivisionID          
exec (@str)         
select 1         
end    
  
--updating Contacts Customfields      
if @byteMode=15      
begin          
declare @checkDivi as varchar(100)           
select @DivisionID=numRecID from SpecificDocuments where numSpecificDocID=@numSpecDocID         
select @ListID=@vcColumnName       
select @checkDivi=isnull(fld_value,'') from CFW_FLD_Values where RecId=@DivisionID and Fld_ID=@ListID      
if @checkDivi =''      
begin        
     update CFW_FLD_Values set Fld_Value=@strSql where RecId=@DivisionID and Fld_ID=@ListID      
end        
exec (@str)         
select 1         
end       
      

if @byteMode=16    
begin          
 declare @checkDiv as varchar(10)        
select @DivisionID=numRecID from SpecificDocuments where numSpecificDocID=@numSpecDocID         
select @ListID=isnull(numListItemID,0) from listdetails where vcData=@strSql        
select @checkDiv=isnull(fld_value,0) from CFW_FLD_Values where RecId=@DivisionID and Fld_ID=@vcColumnName      
if @checkCon=0        
begin        
     update CFW_FLD_Values set Fld_Value=@ListID where RecId=@DivisionID and Fld_ID=@vcColumnName      
end        
exec (@str)         
select 1         
end
GO
