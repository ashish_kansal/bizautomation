/****** Object:  StoredProcedure [dbo].[USP_GetReceivePaymentDetailsForOpportunity]    Script Date: 07/26/2008 16:18:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created By Siva          
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getreceivepaymentdetailsforopportunity')
DROP PROCEDURE usp_getreceivepaymentdetailsforopportunity
GO
CREATE PROCEDURE [dbo].[USP_GetReceivePaymentDetailsForOpportunity]
(@numDomainId as numeric(9)=0,    
 @numOppId as numeric(9)=0    
)          
As          
Begin          
	  Select Count(*) From OpportunityBizDocsDetails OppBizDocsDet          
	  Inner Join OpportunityBizDocs OppBizDocs on OppBizDocsDet.numBizDocsId=OppBizDocs.numOppBizDocsId        
	  Inner Join OpportunityMaster Opp On OppBizDocs.numOppId = Opp.numOppId                                               
	  Inner Join AdditionalContactsInformation ADC On Opp.numContactId = ADC.numContactId                                               
	  Inner Join DivisionMaster Div On Opp.numDivisionId = Div.numDivisionID AND ADC.numDivisionId = Div.numDivisionID                                               
	  Inner Join CompanyInfo C On Div.numCompanyID = C.numCompanyId           
	  Where OppBizDocsDet.numDomainId=@numDomainId And Opp.numOppId=@numOppId --And OppBizDocsDet.bitIntegratedToAcnt=0      
End
GO
