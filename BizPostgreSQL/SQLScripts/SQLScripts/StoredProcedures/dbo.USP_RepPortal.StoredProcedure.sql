/****** Object:  StoredProcedure [dbo].[USP_RepPortal]    Script Date: 07/26/2008 16:20:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- Created By Anoop jayaraj          
          
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_repportal')
DROP PROCEDURE usp_repportal
GO
CREATE PROCEDURE [dbo].[USP_RepPortal]          
  @numDomainID numeric,                
  @dtFromDate datetime,                
  @dtToDate datetime,                
  @numUserCntID numeric=0,                     
  @tintType numeric          
as          
          
select Com.numCompanyID,div.tintCRMtype,dbo.fn_GetPrimaryContact(div.numDivisionID) as ContactID,div.numDivisionID,(select max(bintLastLoggedIn) from ExtranetAccountsDtl E where E.numExtranetID=Ext.numExtranetID) as LastLoggedIn,          
vcCompanyName+' ,'+vcDivisionName as Company,          
dbo.fn_GetListItemName(numCompanyType) as RelationShip,          
isnull((select sum(numNoOfTimes) from ExtranetAccountsDtl E where E.numExtranetID=Ext.numExtranetID),0) as Times,          
(select count(*) from OpportunityMaster where tintOppType=1 and tintOppStatus=1 and numDivisionID=Div.numDivisionID) as NoofDeals,          
isnull((select sum(monPAmount) from OpportunityMaster where tintOppType=1 and tintOppStatus=1 and numDivisionID=Div.numDivisionID),0) as AmtWon          
from ExtarnetAccounts Ext          
join divisionMaster Div          
on Div.numDivisionID=Ext.numDivisionID          
join CompanyInfo Com          
on Com.numCompanyID=Div.numCompanyID          
where div.numTerID in(select F.numTerritory from ForReportsByTerritory F                   
where F.numUserCntID=@numUserCntID and F.numDomainid=@numDomainID and F.tintType=@tintType)          
and  div.bintCreatedDate >= @dtFromDate AND div.bintCreatedDate <= @dtToDate           
and div.numDomainID=@numDomainID
GO
