/****** Object:  StoredProcedure [dbo].[USP_GetDivisionId]    Script Date: 07/26/2008 16:17:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created By Siva  
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getdivisionid')
DROP PROCEDURE usp_getdivisionid
GO
CREATE PROCEDURE [dbo].[USP_GetDivisionId]  
@numDomainId as numeric(9)=0,
@numOppId as numeric(9)=0
As  
Begin  
  Select isnull(numDivisionId,0) From OpportunityMaster Where numDomainId=@numDomainId  And numOppId=@numOppId
 End
GO
