/****** Object:  StoredProcedure [dbo].[usp_GetWarehouseDetails]    Script Date: 07/26/2008 16:18:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getwarehousedetails')
DROP PROCEDURE usp_getwarehousedetails
GO
CREATE PROCEDURE [dbo].[usp_GetWarehouseDetails]  
@numDomainId as numeric(9)  
as  
  
  
SELECT [numWareHouseDetailID],  
  [dbo].[fn_GetListName](numCountryID,0) as country  
  ,case when numStateID = 0 then 'All' else [dbo].[fn_GetState](numStateID) end as [state]   
      ,(select vcWareHouse from [Warehouses] where numWareHouseID = Dtl.numWareHouse ) as WareHouse  
      ,[numDomainId],numFromPinCode,  numToPinCode
  FROM [WareHouseDetails] Dtl  
 where numDomainid = @numDomainId
GO
