/****** Object:  StoredProcedure [dbo].[USP_GetMasterListItemsForBizDocs]    Script Date: 07/26/2008 16:17:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created By Siva              
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getmasterlistitemsforbizdocs')
DROP PROCEDURE usp_getmasterlistitemsforbizdocs
GO
CREATE PROCEDURE [dbo].[USP_GetMasterListItemsForBizDocs]                  
@ListID as numeric(9)=0,                
@numDomainID as numeric(9)=0,            
@numOppId as numeric(9)=0             
as              
Begin               
Declare @numAuthoritativeId as numeric(9)            
Declare @tintOppType as tinyint            
Declare @tintOppStatus as tinyint      
Declare @tintshipped  as tinyint    
set @numAuthoritativeId=0    
 
 Select @tintOppType=tintOppType,@tintOppStatus=tintOppStatus,@tintshipped=tintshipped From OpportunityMaster Where numOppId=@numOppId And  numDomainID=@numDomainID         
             
If @tintOppType=1           
 Select @numAuthoritativeId=numAuthoritativeSales from AuthoritativeBizDocs Where numDomainId=@numDomainID           
Else if @tintOppType=2
 Select @numAuthoritativeId=numAuthoritativePurchase from AuthoritativeBizDocs Where numDomainId=@numDomainID            
 

if @tintOppStatus=1         
Begin        

IF ((SELECT COUNT(*) FROM BizDocFilter WHERE numDomainID=@numDomainID)=0)
BEGIN
	 --SELECT numListItemID,case when @numAuthoritativeId=numListItemID then  vcData +' - Authoritative' Else vcData   End as  vcData 
	 SELECT Ld.numListItemID, CASE WHEN LDN.vcName IS NOT NULL THEN 
												CASE WHEN @numAuthoritativeId = LDN.numListItemID 
													THEN  vcName +' - Authoritative' 
												ELSE vcName
												END 
			ELSE CASE WHEN @numAuthoritativeId = Ld.numListItemID 
					THEN vcData +' - Authoritative' 
				ELSE vcData
				END
			END AS vcData 
	 FROM listdetails LD
	 LEFT JOIN ListDetailsName LDN ON LDN.numListItemID = LD.numListItemID AND LDN.numDomainId = @numDomainID                 
	 WHERE LD.numListID=@ListID 
		and (constFlag=1 or LD.numDomainID=@numDomainID)  
END
ELSE
Begin
  --SELECT Ld.numListItemID,case when @numAuthoritativeId=Ld.numListItemID then  isnull(vcRenamedListName,vcData) +' - Authoritative' Else isnull(vcRenamedListName,vcData)   End as  vcData 
  SELECT Ld.numListItemID, CASE WHEN LDN.vcName IS NOT NULL THEN 
												CASE WHEN @numAuthoritativeId = LDN.numListItemID 
													THEN  ISNULL(vcRenamedListName,vcName ) +' - Authoritative' 
												ELSE ISNULL(vcRenamedListName,vcName)  
												END 
			ELSE CASE WHEN @numAuthoritativeId = Ld.numListItemID 
					THEN ISNULL(vcRenamedListName,vcData) +' - Authoritative' 
				ELSE ISNULL(vcRenamedListName,vcData)  
				END
			END AS vcData ,
						(SELECT COUNT(noOfRecord) FROM(
							SELECT 
(CASE WHEN ISNULL(OI.numUnitHour,0) > 0 THEN ((OI.numUnitHour - ISNULL(SUM(OBI.numUnitHour),0)) / OI.numUnitHour) * 100 ELSE 0 END) As noOfRecord FROM 
OpportunityItems OI
                                LEFT JOIN OpportunityMaster OM ON OI.numOppID = oM.numOppID
                                LEFT JOIN OpportunityBizDocs OB ON OB.numOppId = OI.numOppId
                                                                    AND OB.numBizDocId = BDF.numBizDoc
                                LEFT JOIN OpportunityBizDocItems OBI ON OB.numOppBizDocsId = OBI.numOppBizDocId
                                                                        AND OBI.numOppItemID = OI.numoppitemtCode
WHERE OM.numOppId=@numOppId AND numBizDocId=BDF.numBizDoc
GROUP BY OB.numBizDocId,OBI.numUnitHour,OI.numUnitHour
HAVING (CASE WHEN ISNULL(OI.numUnitHour,0) > 0 THEN ((OI.numUnitHour - ISNULL(SUM(OBI.numUnitHour),0)) / OI.numUnitHour) * 100 ELSE 0 END) BETWEEN 1 AND 100) T)  AS totalPendingInvoice
  FROM BizDocFilter BDF 
  INNER JOIN listdetails Ld ON BDF.numBizDoc=Ld.numListItemID
  left join listorder LO on Ld.numListItemID= LO.numListItemID AND Lo.numDomainId = @numDomainID
  LEFT JOIN ListDetailsName LDN ON LDN.numListItemID = LD.numListItemID AND LDN.numDomainId = @numDomainID   
  WHERE Ld.numListID=@ListID AND BDF.tintBizocType=@tintOppType AND BDF.numDomainID=@numDomainID 
  AND (constFlag=1 or Ld.numDomainID=@numDomainID) ORDER BY totalPendingInvoice desc, LO.intSortOrder        
End        
End               
Else        
Begin        
IF ((SELECT COUNT(*) FROM BizDocFilter WHERE numDomainID=@numDomainID)=0)
BEGIN
 --SELECT numListItemID,vcData as  vcData 
 SELECT LD.numListItemID, CASE WHEN LDN.vcName IS NOT NULL THEN LDN.vcName 
							ELSE LD.vcData 
							END AS vcData
 FROM ListDetails LD       
	LEFT JOIN ListDetailsName LDN ON LDN.numListItemID = LD.numListItemID  AND LDN.numDomainId = @numDomainID          
 WHERE LD.numListID=@ListID and (constFlag=1 or LD.numDomainID=@numDomainID)         
    And LD.numListItemID <> @numAuthoritativeId          
END
ELSE
Begin
  --SELECT Ld.numListItemID, isnull(vcRenamedListName,vcData) as vcData 
  SELECT Ld.numListItemID, CASE WHEN LDN.vcName IS NOT NULL THEN ISNULL(vcRenamedListName,LDN.vcName) 
							ELSE ISNULL(vcRenamedListName,vcData)
							END AS vcData,
							
													(SELECT COUNT(noOfRecord) FROM(
							SELECT 
(CASE WHEN ISNULL(OI.numUnitHour,0) > 0 THEN ((OI.numUnitHour - ISNULL(SUM(OBI.numUnitHour),0)) / OI.numUnitHour) * 100 ELSE 0 END) As noOfRecord FROM 
OpportunityItems OI
                                LEFT JOIN OpportunityMaster OM ON OI.numOppID = oM.numOppID
                                LEFT JOIN OpportunityBizDocs OB ON OB.numOppId = OI.numOppId
                                                                    AND OB.numBizDocId = BDF.numBizDoc
                                LEFT JOIN OpportunityBizDocItems OBI ON OB.numOppBizDocsId = OBI.numOppBizDocId
                                                                        AND OBI.numOppItemID = OI.numoppitemtCode
WHERE OM.numOppId=@numOppId AND numBizDocId=BDF.numBizDoc
GROUP BY OB.numBizDocId,OBI.numUnitHour,OI.numUnitHour
HAVING (CASE WHEN ISNULL(OI.numUnitHour,0) > 0 THEN ((OI.numUnitHour - ISNULL(SUM(OBI.numUnitHour),0)) / OI.numUnitHour) * 100 ELSE 0 END) BETWEEN 1 AND 100) T)  AS totalPendingInvoice
  FROM BizDocFilter BDF 
	  INNER JOIN listdetails Ld ON BDF.numBizDoc = Ld.numListItemID
	  LEFT JOIN listorder LO ON Ld.numListItemID= LO.numListItemID 
		AND Lo.numDomainId = @numDomainID
	  LEFT JOIN ListDetailsName LDN ON LDN.numListItemID = LD.numListItemID AND LDN.numDomainId = @numDomainID   
  WHERE Ld.numListID=@ListID 
	AND BDF.tintBizocType=@tintOppType 
	AND BDF.numDomainID=@numDomainID 
	AND (constFlag=1 OR Ld.numDomainID=@numDomainID) 
	AND Ld.numListItemID <> @numAuthoritativeId 
	AND Ld.numListItemID NOT IN (296)  ORDER BY totalPendingInvoice desc, LO.intSortOrder
  End    
End        
End
GO
