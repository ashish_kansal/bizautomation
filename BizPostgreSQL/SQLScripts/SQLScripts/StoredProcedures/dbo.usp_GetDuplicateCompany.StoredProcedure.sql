/****** Object:  StoredProcedure [dbo].[usp_GetDuplicateCompany]    Script Date: 07/26/2008 16:17:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getduplicatecompany')
DROP PROCEDURE usp_getduplicatecompany
GO
CREATE PROCEDURE [dbo].[usp_GetDuplicateCompany]
@numCompanyID numeric,
@vcCompanyName varchar(100),
@numDivisionID numeric
--
AS
BEGIN

select CI.numCompanyID, CI.vcCompanyName, DM.vcDivisionName
from dbo.CompanyInfo CI INNER JOIN DivisionMaster DM ON CI.numCompanyId = DM.numCampaignID
where CI.vcCompanyName = @vcCompanyName
And DM.numDivisionID <> @numDivisionID and DM.bitPublicFlag=0 and DM.tintCRMType in (1,2)
Group by CI.numCompanyID, CI.vcCompanyName, DM.vcDivisionName

END
GO
