/****** Object:  StoredProcedure [dbo].[USP_ManagegeDashboard]    Script Date: 07/26/2008 16:19:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayara
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_managegedashboard')
DROP PROCEDURE usp_managegedashboard
GO
CREATE PROCEDURE [dbo].[USP_ManagegeDashboard]
@numUserGroupID as numeric(9)=0,
@numUserCntID as numeric(9)=0,
@tintReportType as tinyint,
@vcHeader as varchar(100)='',
@vcFooter as varchar(100)='',
@tintChartType as tinyint,
@numDashBoardReptID as numeric(9)=0,
@numReportID as numeric(9)=0,
@tintColumn as tinyint
as

if @numUserGroupID >0 
begin
	if @numDashBoardReptID=0 
	begin
		update Dashboard set tintRow=tintRow+1 where numGroupUserCntID=@numUserGroupID and tintColumn=@tintColumn  and bitGroup=1
		insert into Dashboard (numReportID, numGroupUserCntID, tintRow, tintColumn, tintReportType, vcHeader, vcFooter, tintChartType, bitGroup)
		values (@numReportID, @numUserGroupID, 1, @tintColumn, @tintReportType, @vcHeader, @vcFooter, @tintChartType, 1)
		set @numDashBoardReptID=@@identity
        
	end
	else if @numDashBoardReptID>0
	begin
		update Dashboard set
			numReportID=@numReportID, 
			numGroupUserCntID=@numUserGroupID, 
			tintReportType=@tintReportType, 
			vcHeader=@vcHeader, 
			vcFooter=@vcFooter, 
			tintChartType=@tintChartType, 
			bitGroup=1 where numDashBoardReptID=@numDashBoardReptID
	end
end
else if @numUserGroupID=0 
begin
	if @numDashBoardReptID=0 
	begin
		update Dashboard set tintRow=tintRow+1 where numGroupUserCntID=@numUserCntID and tintColumn=@tintColumn and bitGroup=0
		insert into Dashboard (numReportID, numGroupUserCntID, tintRow, tintColumn, tintReportType, vcHeader, vcFooter, tintChartType, bitGroup)
		values (@numReportID, @numUserCntID, 1, @tintColumn, @tintReportType, @vcHeader, @vcFooter, @tintChartType, 0)
		set @numDashBoardReptID=@@identity
	end
	else if @numDashBoardReptID>0
	begin
		update Dashboard set
			numReportID=@numReportID, 
			numGroupUserCntID=@numUserCntID, 
			tintReportType=@tintReportType, 
			vcHeader=@vcHeader, 
			vcFooter=@vcFooter, 
			tintChartType=@tintChartType, 
			bitGroup=0 where numDashBoardReptID=@numDashBoardReptID
	end
end

select @numDashBoardReptID
GO
