/****** Object:  StoredProcedure [dbo].[usp_getDynamicFormFields]    Script Date: 07/26/2008 16:17:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
----Created By: Debasish Tapan Nag                                                                          
----Purpose: Returns the available form fields from the database                                                                              
----Created Date: 07/09/2005                                
----exec usp_getDynamicFormFields 1,'',1,1,0                            
----Modified by anoop jayaraj                                                                         
----Modified by Gangadhar
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'usp_getdynamicformfields' ) 
    DROP PROCEDURE usp_getdynamicformfields
GO
CREATE PROCEDURE [dbo].[usp_getDynamicFormFields]
    @numDomainId NUMERIC(9),
    @cCustomFieldsAssociated VARCHAR(10),
    @numFormId INT,
	@numSubFormId NUMERIC(9),
    @numAuthGroupId NUMERIC(9),
	@numBizDocTemplateID numeric(9)
AS 
	DECLARE @vcLocationID VARCHAR(100);SET @vcLocationID='0'
	Select @vcLocationID =isnull(vcLocationID,'0') from dynamicFormMaster where numFormID=@numFormId

    CREATE TABLE #tempAvailableFields(numFormFieldId  NVARCHAR(20),vcFormFieldName NVARCHAR(100),
        vcFieldType CHAR(1),vcAssociatedControlType NVARCHAR(50),numListID NUMERIC(18),vcDbColumnName NVARCHAR(50),
        vcListItemType CHAR(3),intColumnNum INT,intRowNum INT,boolRequired tinyint, numAuthGroupID NUMERIC(18),
          vcFieldDataType CHAR(1),boolAOIField tinyint,numFormID NUMERIC(18),vcLookBackTableName NVARCHAR(50),vcFieldAndType NVARCHAR(50),numSubFormId NUMERIC(18),numFormGroupId NUMERIC(18))
   
   
   CREATE TABLE #tempAddedFields(numFormFieldId  NVARCHAR(20),vcFormFieldName NVARCHAR(100),
        vcFieldType CHAR(1),vcAssociatedControlType NVARCHAR(50),numListID NUMERIC(18),vcDbColumnName NVARCHAR(50),
        vcListItemType CHAR(3),intColumnNum INT,intRowNum INT,boolRequired tinyint, numAuthGroupID NUMERIC(18),
          vcFieldDataType CHAR(1),boolAOIField tinyint,numFormID NUMERIC(18),vcLookBackTableName NVARCHAR(50),vcFieldAndType NVARCHAR(50),numSubFormId NUMERIC(18),numFormGroupId NUMERIC(18))
 
   INSERT INTO #tempAddedFields (numFormFieldId,vcFormFieldName,vcFieldType,vcAssociatedControlType
        ,numListID,vcDbColumnName,vcListItemType,intColumnNum,intRowNum,boolRequired,numAuthGroupID,
          vcFieldDataType,boolAOIField,numFormID,vcLookBackTableName,vcFieldAndType,numSubFormId,numFormGroupId)
     SELECT  CONVERT(VARCHAR(15), numFieldId) + vcFieldType AS numFormFieldId,
                    ISNULL(vcCultureFieldName,vcFieldName) AS vcFormFieldName,
                    'R' as vcFieldType,
                    vcAssociatedControlType,
                    numListID,
                    vcDbColumnName,
                    vcListItemType,
                    tintColumn as intColumnNum,
                    tintRow as intRowNum,
                    bitIsRequired as boolRequired,
                    ISNULL(numAuthGroupID, 0) numAuthGroupID,
                    CASE WHEN vcAssociatedControlType = 'DateField' THEN 'D' ELSE vcFieldDataType END vcFieldDataType,
                    ISNULL(boolAOIField, 0) boolAOIField,
                    numFormID,
                    vcLookBackTableName,
                    CONVERT(VARCHAR(15), numFieldId) + '~' +  CONVERT(VARCHAR(15),ISNULL(bitCustom,0)) + '~' + CONVERT(VARCHAR(15),ISNULL(tintOrder,0)) AS vcFieldAndType
					,ISNULL(numSubFormId,0) AS numSubFormId,
					0 AS numFormGroupId
			FROM    View_DynamicColumns
            WHERE   numFormID = @numFormId 
					AND numAuthGroupID = @numAuthGroupId
                    AND numDomainID = @numDomainId
                    AND isnull(numRelCntType,0)=@numBizDocTemplateID
					AND ISNULL(numSubFormID,0)=ISNULL(@numSubFormId,0)
					AND bitDeleted=0 

            UNION

            SELECT  CONVERT(VARCHAR(15), numFieldId) + 'C' AS numFormFieldId,
                    vcFieldName AS vcFormFieldName,
                    vcFieldType,
					 vcAssociatedControlType,
                    ISNULL(numListID, 0) numListID,
                    vcFieldName vcDbColumnName,
                    CASE WHEN numListID > 0 THEN 'LI' ELSE '' END vcListItemType,
                    ISNULL(tintColumn,0) as intColumnNum,
                    isnull(tintRow,0) as intRowNum,
                    ISNULL(bitIsRequired,0) as boolRequired,
                    ISNULL(numAuthGroupID, 0) numAuthGroupID,
                    CASE WHEN numListID > 0 THEN 'N' WHEN vcAssociatedControlType = 'DateField' THEN 'D' ELSE 'V' END vcFieldDataType,
                    ISNULL(boolAOIField, 0),
                    numFormID,
                    '' AS vcLookBackTableName,
                    CONVERT(VARCHAR(15), numFieldId) + '~' + CONVERT(VARCHAR(15),ISNULL(bitCustom,0)) + '~' + CONVERT(VARCHAR(15),ISNULL(tintOrder,0)) AS vcFieldAndType
					,ISNULL(numSubFormId,0) AS numSubFormId
					,0 AS numFormGroupId
			FROM    View_DynamicCustomColumns                                            
            WHERE   numDomainID = @numDomainId
                    AND numFormID = @numFormId
                    AND numAuthGroupID = @numAuthGroupId
					AND ISNULL(numSubFormID,0)=ISNULL(@numSubFormId,0)
                    AND GRP_ID IN(	SELECT ID FROM dbo.SplitIDs(@vcLocationID,','))     
					 and isnull(numRelCntType,0)=@numBizDocTemplateID         


        INSERT INTO #tempAvailableFields (numFormFieldId,vcFormFieldName,vcFieldType,vcAssociatedControlType
        ,numListID,vcDbColumnName,vcListItemType,intColumnNum,intRowNum,boolRequired,numAuthGroupID,
          vcFieldDataType,boolAOIField,numFormID,vcLookBackTableName,vcFieldAndType,numSubFormId,numFormGroupId)                         
            SELECT  CONVERT(VARCHAR(15),numFieldId) + vcFieldType AS numFormFieldId,
                    ISNULL(vcCultureFieldName,vcFieldName) AS vcFormFieldName,
                    'R' as vcFieldType,
                    vcAssociatedControlType,
                    numListID,
                    vcDbColumnName,
                    vcListItemType,
                    0 intColumnNum,
                    0 intRowNum,
                    ISNULL(bitRequired,0) boolRequired,
                    0 numAuthGroupID,
                    CASE WHEN vcAssociatedControlType = 'DateField' THEN 'D' ELSE vcFieldDataType END vcFieldDataType,
                    0 boolAOIField,
                    numFormID,
                    vcLookBackTableName,
                    CONVERT(VARCHAR(15), numFieldId) + '~' + CONVERT(VARCHAR(15),ISNULL(bitCustom,0)) + '~' + CONVERT(VARCHAR(15),ISNULL(tintOrder,0)) AS vcFieldAndType
					,0 AS numSubFormId
					,0 AS numFormGroupId
			FROM    View_DynamicDefaultColumns
            WHERE   numFormID = @numFormId 
					AND bitDeleted=0
					AND numDomainID=@numDomainID
                    AND numFieldId NOT IN (
                    SELECT  numFieldId
                    FROM    View_DynamicColumns
                    WHERE   numFormID = @numFormId
							AND ISNULL(numSubFormID,0)=ISNULL(@numSubFormId,0)
                            AND numAuthGroupID = @numAuthGroupId
                            AND numDomainID = @numDomainId
                            and isnull(numRelCntType,0)=@numBizDocTemplateID)
           
			UNION
            
			SELECT  CONVERT(VARCHAR(15), Fld_id) + 'C' AS numFormFieldId,
                    Fld_label AS vcFormFieldName,
                    isnull(L.vcFieldType,'C') as vcFieldType,
                    fld_type as vcAssociatedControlType,
                    ISNULL(C.numListID, 0) numListID,
                    Fld_label vcDbColumnName,
                    CASE WHEN C.numListID > 0 THEN 'LI' ELSE '' END vcListItemType,
                    0 intColumnNum,
                    0 intRowNum,
                    ISNULL(V.bitIsRequired,0) as boolRequired,
                    0 numAuthGroupID,
                    CASE WHEN C.numListID > 0 THEN 'N' ELSE 'V' END vcFieldDataType,
                    0 boolAOIField,
                    @numFormId AS numFormID,
                    '' AS vcLookBackTableName,
                    CONVERT(VARCHAR(15), Fld_id) + '~1' + '~0' AS vcFieldAndType
					,0 AS numSubFormId
					,0 AS numFormGroupId
            FROM    CFW_Fld_Master C LEFT JOIN CFW_Validation V ON V.numFieldID = C.Fld_id
					JOIN CFW_Loc_Master L on C.GRP_ID=L.Loc_Id
            WHERE   C.numDomainID = @numDomainId
					AND GRP_ID IN(	SELECT ID FROM dbo.SplitIDs(@vcLocationID,','))
                    AND Fld_id NOT IN (
                    SELECT  numFieldId
                    FROM    View_DynamicCustomColumns
                    WHERE   numFormID = @numFormId
							AND ISNULL(numSubFormID,0)=ISNULL(@numSubFormId,0)
							AND GRP_ID IN(	SELECT ID FROM dbo.SplitIDs(@vcLocationID,','))
                            AND numAuthGroupID = @numAuthGroupId
                            AND numDomainID = @numDomainId and isnull(numRelCntType,0)=@numBizDocTemplateID)
        

		SELECT * FROM #tempAvailableFields WHERE numFormFieldId NOT IN (SELECT numFormFieldId FROM #tempAddedFields)
		AND #tempAvailableFields.vcLookBackTableName = (CASE @numAuthGroupId WHEN 40911 THEN 'CSGrid' ELSE REPLACE(#tempAvailableFields.vcLookBackTableName,'CSGrid','') END)
		 UNION            
        SELECT * FROM #tempAddedFields WHERE 1=1
		AND #tempAddedFields.vcLookBackTableName = (CASE @numAuthGroupId WHEN 40911 THEN 'CSGrid' ELSE REPLACE(#tempAddedFields.vcLookBackTableName,'CSGrid','') END)
        
        DROP TABLE #tempAvailableFields
        DROP TABLE #tempAddedFields
GO
