/****** Object:  StoredProcedure [dbo].[USP_GetOpeningBalance]    Script Date: 07/26/2008 16:17:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created by Siva  
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getopeningbalance')
DROP PROCEDURE usp_getopeningbalance
GO
CREATE PROCEDURE [dbo].[USP_GetOpeningBalance]    
@numAccountId as numeric(9),    
@numDomainId as numeric(9)    
    
AS    
Begin    
	Select isnull(numOpeningBal,0) From Chart_Of_Accounts 
	Where numAccountId=@numAccountId And numDomainId=@numDomainId    
End
GO
