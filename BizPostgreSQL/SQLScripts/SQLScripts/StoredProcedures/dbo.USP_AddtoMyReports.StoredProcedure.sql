/****** Object:  StoredProcedure [dbo].[USP_AddtoMyReports]    Script Date: 07/26/2008 16:14:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj  
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_addtomyreports')
DROP PROCEDURE usp_addtomyreports
GO
CREATE PROCEDURE [dbo].[USP_AddtoMyReports]    
@numRPTId as numeric(9)=0,    
@numUserCntID as numeric(9)=0    
    
as    
declare @tintRptSequence as tinyint    
select @tintRptSequence=isnull(max(tintRptSequence),0)from MyReportList where numUserCntID=@numUserCntID    
set @tintRptSequence=@tintRptSequence+1    
delete from MyReportList where numRPTId=@numRPTId and numUserCntID=@numUserCntID    and tintType =0
insert into MyReportList(numRPTId,numUserCntID,tintRptSequence,tintType)    
values(@numRPTId,@numUserCntID,@tintRptSequence,0)
GO
