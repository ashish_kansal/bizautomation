/****** Object:  StoredProcedure [dbo].[usp_GetMyLeadBoxDetailsFor]    Script Date: 07/26/2008 16:17:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getmyleadboxdetailsfor')
DROP PROCEDURE usp_getmyleadboxdetailsfor
GO
CREATE PROCEDURE [dbo].[usp_GetMyLeadBoxDetailsFor] 
	@numUserID numeric(9)=0  
--
AS
BEGIN
	SELECT 
		LD.vcInterested1,
		LD.vcInterested2,
		LD.vcInterested3,
		LD.vcInterested4,
		LD.vcInterested5,
		LD.vcInterested6,
		LD.vcInterested7,
		LD.vcInterested8,
		vcRedirectURL,
		bitWebSite,
		bitPosition,
		bitNotes,
		bitAnnualRevenue,
		bitNumEmployees,
		bitHowHeard,
		bitProfile,
		bitPublicLead
		
	FROM 	

		LeadBox LD
		
	--WHERE 
		 --numUserID=@numUserID
		
END
GO
