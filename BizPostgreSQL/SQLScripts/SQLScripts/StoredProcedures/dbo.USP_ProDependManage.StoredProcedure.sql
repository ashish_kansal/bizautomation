/****** Object:  StoredProcedure [dbo].[USP_ProDependManage]    Script Date: 07/26/2008 16:20:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_prodependmanage')
DROP PROCEDURE usp_prodependmanage
GO
CREATE PROCEDURE [dbo].[USP_ProDependManage]
(
@byteMode as tinyint=null,
@strDependency as text='',
@numProId as numeric(9)=null,
@numProStageId as numeric(9)=null
)
as

if @byteMode= 0 
begin

	delete from ProjectsDependency where numProId=@numProId and numProStageId=@numProStageId

	declare @hDoc  int
	EXEC sp_xml_preparedocument @hDoc OUTPUT, @strDependency

	insert into  ProjectsDependency
	(
	numProId,
	numProStageId,
	numProcessStageId,
	vcType
	)
	select @numProId,@numProStageId,X.* from(SELECT *FROM OPENXML (@hDoc,'/NewDataSet/Table',2)
	WITH (	numProcessStageId numeric(9),vcType varchar(1)
		))X



	EXEC sp_xml_removedocument @hDoc
	
end

if @byteMode= 1 
begin



	select dep.numProcessStageId,dtl.vcstagedetail,vcType,
	'Milestone - ' + convert(varchar(4),dtl.numstagepercentage) +'%' as numstagepercentage 
	,mst.vcPOppName as vcPOppName,case when dtl.bitStageCompleted =0 then '<font color=red>Pending</font>'  when dtl.bitStageCompleted =1 then '<font color=blue>Completed</font>' end as Status 
	from ProjectsDependency dep
	join OpportunityStageDetails dtl
	on dtl.numOppStageID=dep.numProcessStageId
	join OpportunityMaster mst on 
	dtl.numOppId=mst.numOppId 
	where dep.numProId=@numProId and dep.numProStageId=@numProStageId and vcType='O'
 
	union 
	select dep.numProcessStageId,dtl.vcstagedetail,vcType,
	'Milestone - ' + convert(varchar(4),dtl.numstagepercentage) +'%' as numstagepercentage ,
	mst.VcProjectName as vcPOppName,
	case when dtl.bitStageCompleted =0 then '<font color=red>Pending</font>'  when dtl.bitStageCompleted =1 then '<font color=blue>Completed</font>' end as Status 
	from ProjectsDependency dep
	join ProjectsStageDetails dtl
	on dtl.numProStageID=dep.numProcessStageId
	join ProjectsMaster mst on 
	dtl.numProId=mst.numProId
	where  vcType='P' and dep.numProId=@numProId and dep.numProStageId=@numProStageId


end
GO
