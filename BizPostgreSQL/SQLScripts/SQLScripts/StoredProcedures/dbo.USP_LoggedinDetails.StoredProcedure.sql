GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created by anoop jayaraj                                                
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_loggedindetails')
DROP PROCEDURE usp_loggedindetails
GO
CREATE PROCEDURE [dbo].[USP_LoggedinDetails]                                                    
(                                                                          
	@numDivisionID NUMERIC(18,0),      
	@numContactID NUMERIC(18,0),      
	@numDomainID NUMERIC(18,0)       
)      
AS
BEGIN    
	DECLARE @numSubID NUMERIC(18,0)    
       
    SELECT 
		@numSubID=numSubscriberID 
	FROM 
		Subscribers 
	WHERE 
		numTargetDomainID=@numDomainID   
     
	UPDATE 
		Subscribers 
	SET 
		dtLastLoggedIn=GETUTCDATE() 
	WHERE 
		numSubscriberID=@numSubID  
     
	INSERT INTO UserAccessedDTL
	(
		numSubscriberID
		,numDivisionID
		,numContactID
		,dtLoggedInTime
		,bitPortal
		,numDomainID
	)      
    VALUES 
	(
		@numSubID
		,@numDivisionID
		,@numContactID
		,GETUTCDATE()
		,0
		,@numDomainID
	)      
         
	SELECT SCOPE_IDENTITY()
END
GO
