/****** Object:  StoredProcedure [dbo].[usp_GetContractHrsAmount]    Script Date: 07/26/2008 16:16:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getcontracthrsamount')
DROP PROCEDURE usp_getcontracthrsamount
GO
CREATE PROCEDURE [dbo].[usp_GetContractHrsAmount]                                                                                                   
@numContractID  bigint,                                                                          
@numDomainID numeric(9)=0,
@numCategoryHDRID numeric(9)=0 

as                                                                                                    

declare @hrs as decimal(10,2)
declare @Amount as decimal(10,2)
set @Amount = 0 
set @hrs = 0 

if @numContractID <> 0
 begin
select 
@Amount=
case when numCategory = 2 then isnull(monamount,0) else 0 end,
 @hrs =
case when numCategory = 1 then convert(float,datediff(minute,dtFromdate,dtToDate))/60 else 0 end
from timeandexpense
where numDomainid = @numDomainId and numCategoryHDRID = @numCategoryHDRID
end


  
  
select  
bitamount,                              
bitHour,                              
bitdays,                              
bitincidents ,  
(@hrs+dbo.getContractRemainingHrsAmt(0,@numContractID)) as RemHours,                                 
(@Amount+dbo.getContractRemainingHrsAmt(1,@numContractID)) as RemAmount ,  
decrate  
  
from ContractManagement c                                                                
                                              
where  c.numDomainId =@numDomainId and c.numcontractId = @numContractID
GO
