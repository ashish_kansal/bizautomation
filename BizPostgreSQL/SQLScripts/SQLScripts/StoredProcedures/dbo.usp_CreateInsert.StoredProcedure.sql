/****** Object:  StoredProcedure [dbo].[usp_CreateInsert]    Script Date: 07/26/2008 16:15:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_createinsert')
DROP PROCEDURE usp_createinsert
GO
CREATE PROCEDURE [dbo].[usp_CreateInsert]
@strTableName Varchar(100)    
--     
as         
Declare @StrColumns Varchar(8000) , @EXECStatMent Varchar(8000)        
/*Declare  @strTableName Varchar(100)         
SET  @strTableName ='BATCH_MASTER'  */      
SET @StrColumns  = ''        
Select 
	@StrColumns  = @StrColumns + sysColumns.NAme  + ', ' 
from 
	sysColumns 
INNER JOIN 
	sysobjects         
ON
	sysColumns.ID = sysobjects.ID
Where         
	sysobjects.Name = @strTableName        
        
SET  @StrColumns  = SUBSTRING(@StrColumns ,1,LEN(@StrColumns  )-1)        
SET @EXECStatMent = ' SELECT ''INSERT INTO ' + @strTableName + ' ( ' + @StrColumns   + ' ) VALUES ( '''''' + Convert(Varchar,' + REPLACE(@StrColumns ,',', ') +  ''''''  ,''''''  + Convert(Varchar,') + ' ) +'''''')'' From '+ @strTableName        
print @EXECStatMent  
Exec(   @EXECStatMent )
GO
