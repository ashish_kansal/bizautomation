
GO
/****** Object:  StoredProcedure [dbo].[USP_ManagerBizDocAppEMployee]    Script Date: 02/19/2010 17:47:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--SELECT * FROM BizDocApprovalRuleEmployee


GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_managerbizdocappemployee')
DROP PROCEDURE usp_managerbizdocappemployee
GO
CREATE PROCEDURE [dbo].[USP_ManagerBizDocAppEMployee]
@numBizDocAppId numeric(18,0),
@numEmployeeId numeric(18,0)

as
INSERT INTO BizDocApprovalRuleEmployee SELECT @numBizDocAppId,@numEmployeeId
SELECT SCOPE_IDENTITY()
