/****** Object:  StoredProcedure [dbo].[USP_ManageGatewayDTLs]    Script Date: 07/26/2008 16:19:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj  
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_managegatewaydtls')
DROP PROCEDURE usp_managegatewaydtls
GO
CREATE PROCEDURE [dbo].[USP_ManageGatewayDTLs]  
@numDomainID as numeric(9), 
@str as text,
@numSiteId as int,
@numPaymentGatewayID NUMERIC(18,0)
as  

	IF ISNULL(@numSiteId,0) > 0
	BEGIN
		UPDATE Sites SET intPaymentGateWay=@numPaymentGatewayID WHERE numSiteID=@numSiteId AND numDomainID=@numDomainID
	END
	ELSE
	BEGIN
		UPDATE Domain SET intPaymentGateWay=@numPaymentGatewayID WHERE numDomainID=@numDomainID
	END
  
delete from PaymentGatewayDTLID where ISNULL(numSiteId,0)=ISNULL(@numSiteId,0) AND numDomainID=@numDomainID
 DECLARE @hDocItem int  
  
 EXEC sp_xml_preparedocument @hDocItem OUTPUT, @str                     
   insert into                     
   PaymentGatewayDTLID                                                                        
   (intPaymentGateWay,vcFirstFldValue,vcSecndFldValue,vcThirdFldValue,numDomainID,bitTest,numSiteId)                    
   select X.intPaymentGateWay,X.vcFirstFldValue,x.vcSecndFldValue,x.vcThirdFldValue,@numDomainID,X.bitTest,@numSiteId from(                                                                        
   SELECT *FROM OPENXML (@hDocItem,'/NewDataSet/Table',2)                                                                        
   WITH                     
   (                                                                        
   intPaymentGateWay numeric(9),                                   
   vcFirstFldValue varchar(1000),                                                                        
   vcSecndFldValue varchar(1000),
   vcThirdFldValue varchar(1000),
   bitTest bit          
   ))X  
  
  
  EXEC sp_xml_removedocument @hDocItem
GO
