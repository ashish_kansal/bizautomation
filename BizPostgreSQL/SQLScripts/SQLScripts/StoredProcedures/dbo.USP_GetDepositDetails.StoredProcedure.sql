/****** Object:  StoredProcedure [dbo].[USP_GetDepositDetails]    Script Date: 07/26/2008 16:17:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created By Siva    
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'usp_getdepositdetails' ) 
    DROP PROCEDURE usp_getdepositdetails
GO
CREATE PROCEDURE [dbo].[USP_GetDepositDetails]
    @numDepositId AS NUMERIC(9) = 0 ,
    @numDomainId AS NUMERIC(9) = 0 ,
    @tintMode AS TINYINT = 0, -- 1= Get Undeposited Payments 
    @numCurrencyID numeric(9) =0,
    @CurrentPage INT,
    @PageSize INT,
    @TotRecs INT OUTPUT,
    @numAccountClass NUMERIC(18,0)=0
AS 
    BEGIN  
      				
	    DECLARE @numBaseCurrencyID NUMERIC
	    DECLARE @BaseCurrencySymbol nVARCHAR(3)
	    
        SELECT @numBaseCurrencyID = ISNULL(numCurrencyID,0) FROM dbo.Domain WHERE numDomainId=@numDomainId
        SELECT @BaseCurrencySymbol = ISNULL(varCurrSymbol,'') FROM dbo.Currency WHERE numCurrencyID = @numBaseCurrencyID
      
		DECLARE @firstRec AS INTEGER
		DECLARE @lastRec AS INTEGER
		SET @firstRec = ( @CurrentPage - 1 ) * @PageSize
		SET @lastRec = ( @CurrentPage * @PageSize + 1 )
			  
        IF @tintMode = 0 
            BEGIN 
    	
    
                SELECT  numDepositId ,
                        dbo.fn_GetComapnyName(numDivisionID) vcCompanyName ,
                        numDivisionID ,
                        numChartAcntId ,
                        dbo.FormatedDateFromDate(dtDepositDate, DM.numDomainId) dtDepositDate ,
                        dtDepositDate AS [dtOrigDepositDate],
                        monDepositAmount ,
                        numPaymentMethod ,
                        vcReference ,
                        vcMemo ,
                        tintDepositeToType,tintDepositePage,bitDepositedToAcnt
                        ,DM.numCurrencyID
						,ISNULL(C.varCurrSymbol,'') varCurrSymbol
						,ISNULL(NULLIF(DM.fltExchangeRate,0),1) fltExchangeRateReceivedPayment
						,@BaseCurrencySymbol BaseCurrencySymbol,ISNULL(monRefundAmount,0) AS monRefundAmount
						,ISNULL(DM.numAccountClass,0) AS numAccountClass
                FROM    dbo.DepositMaster DM
						LEFT JOIN dbo.Currency C ON C.numCurrencyID = DM.numCurrencyID
                WHERE   numDepositId = @numDepositId
                        AND DM.numDomainId = @numDomainId
        
                SELECT  DD.numDepositeDetailID ,
                        DD.numDepositID ,
                        DD.numOppBizDocsID ,
                        DD.numOppID ,
                        DD.monAmountPaid ,
                        DD.numChildDepositID ,
                        DD.numPaymentMethod ,
                        DD.vcMemo ,
                        DD.vcReference ,
                        DD.numClassID ,
                        DD.numProjectID ,
                        DD.numReceivedFrom ,
                        DD.numAccountID,
                        GJD.numTransactionId AS numTransactionID,
                        GJD1.numTransactionId AS numTransactionIDHeader
                        ,(CASE WHEN ISNULL(DD.numChildDepositID,0) = 0 THEN ISNULL(DMO.numCurrencyID,0) ELSE ISNULL(DM.numCurrencyID,0) END) numCurrencyID
						,(CASE WHEN ISNULL(DD.numChildDepositID,0) = 0 THEN ISNULL(NULLIF(DMO.fltExchangeRate,0),1) ELSE ISNULL(NULLIF(DM.fltExchangeRate,0),1) END) fltExchangeRateReceivedPayment
						
                FROM    dbo.DepositeDetails DD
				INNER JOIN DepositMaster DMO ON DD.numDepositID = DMO.numDepositId
                LEFT JOIN dbo.DepositMaster DM ON DM.numDepositId = DD.numChildDepositID
                LEFT JOIN dbo.General_Journal_Details GJD ON GJD.numReferenceID = DD.numDepositeDetailID AND GJD.tintReferenceType = 7 
                LEFT JOIN dbo.General_Journal_Details GJD1 ON GJD1.numReferenceID = DD.numDepositID AND GJD1.tintReferenceType = 6 
                WHERE   DD.numDepositID = @numDepositId
            END        
        ELSE 
            IF @tintMode = 1 
                BEGIN
                
                --Get Undeposited payments
                SELECT * INTO #tempDeposits FROM 
				(
				--Get Saved deposit entries for edit mode
				SELECT  Row_number() OVER ( ORDER BY DM.dtDepositDate DESC ) AS row,
							DD.numDepositeDetailID ,
							DM.numDepositId ,
							DM.numChartAcntId ,
							dbo.FormatedDateFromDate(dtDepositDate, DM.numDomainId) dtDepositDate ,
							DD.monAmountPaid AS monDepositAmount,
							DD.numPaymentMethod ,
							DD.vcReference ,
							DD.vcMemo ,
							DD.numReceivedFrom AS numDivisionID,
							dbo.fn_GetComapnyName(DM.numDivisionID) ReceivedFrom,
							bitDepositedToAcnt,
							 (SELECT 
							CASE tintSourceType 
							WHEN 1 THEN dbo.GetListIemName(tintSource) 
							WHEN 2 THEN (SELECT TOP 1 vcSiteName FROM dbo.Sites WHERE numSiteID = tintSource) 
							WHEN 3 THEN (SELECT TOP 1 vcProviderName FROM dbo.WebAPI WHERE WebApiId = tintSource)  
							ELSE '' END 
							FROM dbo.OpportunityMaster WHERE numOppId IN ( SELECT TOP 1 numOppID FROM dbo.DepositeDetails WHERE numDepositID = DM.numDepositId AND numOppID>0) ) AS vcSource,
							CASE WHEN DM.numTransHistoryID >0 THEN dbo.GetListIemName(ISNULL(TH.numCardType,0)) ELSE '' END  AS vcCardType
							,DM.numCurrencyID
							,ISNULL(C.varCurrSymbol,'') varCurrSymbol
							,ISNULL(NULLIF(DM.fltExchangeRate,0),1) fltExchangeRateReceivedPayment
							,@BaseCurrencySymbol BaseCurrencySymbol
					FROM    dbo.DepositeDetails DD
							INNER JOIN dbo.DepositMaster DM ON DD.numChildDepositID = DM.numDepositId
							LEFT JOIN dbo.TransactionHistory TH ON TH.numTransHistoryID= DM.numTransHistoryID
							LEFT JOIN dbo.Currency C ON C.numCurrencyID = DM.numCurrencyID
					WHERE   DD.numDepositID = @numDepositId AND (ISNULL(DM.numAccountClass,0)=@numAccountClass OR @numAccountClass=0)
					UNION
					SELECT Row_number() OVER ( ORDER BY DM.dtDepositDate DESC ) AS row,
							0 numDepositeDetailID ,
							numDepositId ,
							numChartAcntId ,
							dbo.FormatedDateFromDate(dtDepositDate, DM.numDomainId) dtDepositDate ,
							monDepositAmount ,
							numPaymentMethod ,
							vcReference ,
							vcMemo ,
							DM.numDivisionID,
							dbo.fn_GetComapnyName(DM.numDivisionID) ReceivedFrom,
							bitDepositedToAcnt,
							(SELECT 
							CASE tintSourceType 
							WHEN 1 THEN dbo.GetListIemName(tintSource) 
							WHEN 2 THEN (SELECT TOP 1 vcSiteName FROM dbo.Sites WHERE numSiteID = tintSource) 
							WHEN 3 THEN (SELECT TOP 1 vcProviderName FROM dbo.WebAPI WHERE WebApiId = tintSource)  
							ELSE '' END 
							FROM dbo.OpportunityMaster WHERE numOppId IN ( SELECT TOP 1 numOppID FROM dbo.DepositeDetails WHERE numDepositID = DM.numDepositId AND numOppID>0) ) AS vcSource,
							CASE WHEN DM.numTransHistoryID >0 THEN dbo.GetListIemName(ISNULL(TH.numCardType,0)) ELSE '' END  AS vcCardType
							,DM.numCurrencyID
							,ISNULL(C.varCurrSymbol,'') varCurrSymbol
							,ISNULL(NULLIF(DM.fltExchangeRate,0),1) fltExchangeRateReceivedPayment
							,@BaseCurrencySymbol BaseCurrencySymbol
					FROM    dbo.DepositMaster DM
							INNER JOIN dbo.Chart_Of_Accounts COA ON DM.numChartAcntId = COA.numAccountId
							LEFT JOIN dbo.TransactionHistory TH ON DM.numTransHistoryID = TH.numTransHistoryID
							LEFT JOIN dbo.Currency C ON C.numCurrencyID = DM.numCurrencyID
					WHERE   DM.numDomainId = @numDomainID
							AND tintDepositeToType = 2
							AND ISNULL(bitDepositedToAcnt,0) = 0 
							AND (ISNULL(DM.numCurrencyID,@numBaseCurrencyID) = @numCurrencyID OR @numCurrencyID =0)
							AND (ISNULL(DM.numAccountClass,0)=@numAccountClass OR @numAccountClass=0)
			  )  [Deposits]
			
			SELECT  @TotRecs = COUNT(*) FROM #tempDeposits
						
			SELECT  * 
			FROM    #tempDeposits
			WHERE   row > @firstRec
					AND row < @lastRec ;
            
            DROP TABLE #tempDeposits        
					
			-- get new deposite Entries
					SELECT  DD.numDepositeDetailID ,
							DM.numDepositId ,
							dbo.FormatedDateFromDate(dtDepositDate, DM.numDomainId) dtDepositDate ,
							DD.monAmountPaid ,
							DD.numPaymentMethod ,
							DD.vcReference ,
							DD.vcMemo ,
							DD.numAccountID,
--							 COA.vcAccountCode numAcntType,
							DD.numClassID,
							DD.numProjectID,
							DD.numReceivedFrom,
							dbo.fn_GetComapnyName(DD.numReceivedFrom) ReceivedFrom,
							bitDepositedToAcnt
					FROM    dbo.DepositeDetails DD
							INNER JOIN dbo.DepositMaster DM ON DD.numDepositID = DM.numDepositId
--							LEFT JOIN dbo.Chart_Of_Accounts COA ON DD.numAccountID = COA.numAccountId
					WHERE   DD.numDepositID = @numDepositId AND ISNULL(numChildDepositID,0) = 0
					AND (ISNULL(DM.numAccountClass,0)=@numAccountClass OR @numAccountClass=0)
					
						
                END
    END
GO
