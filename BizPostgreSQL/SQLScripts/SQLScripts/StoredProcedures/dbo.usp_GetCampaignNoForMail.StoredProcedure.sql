/****** Object:  StoredProcedure [dbo].[usp_GetCampaignNoForMail]    Script Date: 07/26/2008 16:16:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getcampaignnoformail')
DROP PROCEDURE usp_getcampaignnoformail
GO
CREATE PROCEDURE [dbo].[usp_GetCampaignNoForMail] 
	@numCreatedBy numeric(9)=0,
	@numDomainID numeric(9)=0   
--
AS
BEGIN
	SELECT 
		vcCampaignNumber
				
	FROM 	
		Campaign
	WHERE numCreatedBy=@numCreatedBy
	and numDomainID=@numDomainID
		 
	
END
GO
