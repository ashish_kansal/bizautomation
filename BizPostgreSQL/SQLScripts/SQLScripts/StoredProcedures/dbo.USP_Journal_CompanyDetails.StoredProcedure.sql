
GO
/****** Object:  StoredProcedure [dbo].[USP_Journal_CompanyDetails]    Script Date: 05/07/2009 22:03:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created By Siva    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_journal_companydetails')
DROP PROCEDURE usp_journal_companydetails
GO
CREATE PROCEDURE [dbo].[USP_Journal_CompanyDetails]          
@numDomainID numeric(9)=0,      
@varCompanyName varchar(800)      
As           
Begin          
Declare @strSQL as varchar(2000)    
Set @strSQL=''    
    
Set @strSQL = 'Select DM.numDivisionID AS numDivisionID,CI.vcCompanyName AS vcCompanyName,(CONVERT(VARCHAR(10),DM.numDivisionID )+ ''~'' +ISNULL([vcFirstName],'''')+ '' '' +  ISNULL([vcLastName],'''')) AS PrimaryContact
From CompanyInfo CI join Divisionmaster DM 
	ON  CI.numCompanyid=DM.numCompanyid                              
LEFT OUTER JOIN [AdditionalContactsInformation] ACI
	ON ACI.[numDivisionId] = DM.[numDivisionID]
Where ISNULL(ACI.bitPrimaryContact,0)=1 and CI.numDomainID=' + Convert(varchar(5),@numDomainID)    
if @varCompanyName <> ''     
Set @strSQL=@strSQL + ' And CI.vcCompanyName like ''' +@varCompanyName +'%'''    
print @strSQL      
Exec (@strSQL)    
End



