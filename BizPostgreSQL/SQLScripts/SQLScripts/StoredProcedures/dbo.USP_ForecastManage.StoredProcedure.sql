/****** Object:  StoredProcedure [dbo].[USP_ForecastManage]    Script Date: 07/26/2008 16:15:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj      
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_forecastmanage')
DROP PROCEDURE usp_forecastmanage
GO
CREATE PROCEDURE [dbo].[USP_ForecastManage]        
@strForecast as text ='',        
@tintForecastType as tinyint=null,        
@numUserCntID as numeric(9)=null,        
@numItemCode as numeric(9)=null,        
@sintYear as smallint=null,        
@tintquarter as tinyint =null,        
@numDomainId as numeric(9)=null        
as        
if @tintForecastType=1         
begin        
delete from Forecast where numCreatedBy=@numUserCntID         
and sintYear=@sintYear and tintquarter=@tintquarter        
and tintForecastType=@tintForecastType        
and numDomainId=@numDomainId        
end        
else        
begin        
delete from Forecast where numCreatedBy=@numUserCntID         
and sintYear=@sintYear and tintquarter=@tintquarter        
and tintForecastType=@tintForecastType        
and numItemCode=@numItemCode and numDomainId=@numDomainId        
end        
declare @hDoc  int        
        
EXEC sp_xml_preparedocument @hDoc OUTPUT, @strForecast        
        
insert into Forecast (        
  sintYear,        
  tintquarter,        
  tintMonth,        
  monQuota,        
  monForecastAmount,        
  numItemCode,        
  tintForecastType,        
  numCreatedBy,         
  numDomainId        
  )        
 select X.*,@tintForecastType,@numUserCntID,@numDomainId from(SELECT *FROM OPENXML (@hDoc,'/NewDataSet/Table',2)        
 WITH  (        
 sintYear smallint,        
 tintquarter tinyint,        
 tintMonth tinyint,        
 monQuota DECIMAL(20,5),         
 monForecastAmount DECIMAL(20,5),        
 numItemCode numeric(9)        
  ))X        
 EXEC sp_xml_removedocument @hDoc
GO
