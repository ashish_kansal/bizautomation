/****** Object:  StoredProcedure [dbo].[usp_TicklerStages]    Script Date: 07/26/2008 16:21:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj      
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_ticklerstages')
DROP PROCEDURE usp_ticklerstages
GO
CREATE PROCEDURE [dbo].[usp_TicklerStages]          
@numUserCntID as numeric(9)=null,          
@numDomainID as numeric(9)=null,          
@startDate as datetime,          
@endDate as datetime,          
@numOppId as numeric(9)=null,        
@OppType as tinyint          
as           
          
if  @OppType=1 -- Sales or Purchase        
begin        
select vcStageDetail from OpportunityMaster opp          
 join OpportunityStageDetails stg          
 on stg.numOppId=opp.numOppId          
where stg.numAssignTo=@numUserCntID and opp.numDomainID=@numDomainID and opp.numOppId=@numOppId          
and bitStageCompleted=0 --AND (bintDueDate  >= @startDate and bintDueDate  <= @endDate)          
end        
if  @OppType=2 --- Projects        
begin        
select vcStageDetail from ProjectsMaster Pro          
 join ProjectsStageDetails stg          
 on stg.numProId=Pro.numProId          
where stg.numAssignTo=@numUserCntID and Pro.numDomainID=@numDomainID and Pro.numProId=@numOppId          
and bitStageCompleted=0 --AND (bintDueDate  >= @startDate and bintDueDate  <= @endDate)          
end
GO
