/****** Object:  StoredProcedure [dbo].[USP_CaseComments]    Script Date: 07/26/2008 16:15:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj      
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_comments')
DROP PROCEDURE usp_comments
GO
CREATE PROCEDURE [dbo].[USP_Comments]           
@byteMode as tinyint=0,    
@numCaseId as numeric(9)=0,             
@vcHeading as varchar(1000)='',    
@txtComments as text='',    
@numContactID as numeric(9)=0,    
@numCommentID as numeric(9)=0,
@numStageID AS NUMERIC(9)=0,
@numDomainId as NUmeric(9)=0
as            
    
if @byteMode=0    
begin           
	IF @numCaseId>0
	BEGIN  
		select numCommentID,vcHeading+' ('+convert(varchar(20),dbo.[FormatedDateFromDate]((bintCreatedDate),@numDomainId))+') - '+ dbo.fn_GetContactName(numCreatedby) as vcHeading,txtComments from Comments where numCaseId=@numCaseId  order by    numCommentID desc     	
	END
	ELSE IF @numStageID>0
	BEGIN
		select numCommentID,vcHeading+' ('+convert(varchar(20),dbo.[FormatedDateFromDate]((bintCreatedDate),@numDomainId))+') - '+ dbo.fn_GetContactName(numCreatedby) as vcHeading,txtComments from Comments where numStageID=@numStageID order by    numCommentID desc     
	END

end    
    
if @byteMode=1    
begin    
  IF @numStageID =0 SET @numStageID = NULL
  IF @numCaseId =0 SET @numCaseId= NULL
  IF @numCommentID IS NULL SET @numCommentID = 0
if @numCommentID=0   
begin  
insert into Comments(numCaseID,vcHeading,txtComments,numCreatedby,bintCreatedDate,numStageID)    
values(@numCaseId,@vcHeading,@txtComments,@numContactID,getutcdate(),@numStageID)    
select SCOPE_IDENTITY()   
end  
else  
begin  
update Comments set vcHeading=@vcHeading,txtComments=@txtComments where numCommentID=@numCommentID  
select @numCommentID  
end   
end       
    
if @byteMode=2     
begin    
	delete from Comments where numCommentID=@numCommentID    
select 1    
end
GO
