/****** Object:  StoredProcedure [dbo].[usp_LeadDetails]    Script Date: 07/26/2008 16:19:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- modified by anoop jayaraj                               
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_leaddetails')
DROP PROCEDURE usp_leaddetails
GO
CREATE PROCEDURE [dbo].[usp_LeadDetails]                                                                                   
 @numContactID numeric(9),  
 @numDomainID as numeric(9),  
@ClientTimeZoneOffset  int                                        
AS                                                
BEGIN                                                
 SELECT                                                 
  cmp.vcCompanyName,                                                
  DM.vcDivisionName,                                                
  ADC.vcFirstName,                                                
  ADC.vcLastName,                                                
  cmp.vcWebSite,                                                
  ADC.vcEmail,                                                
--  ConAdd.vcStreet,                                      
  DM.numFollowUpStatus,        
  DM.numTerID,                                                
--  ConAdd.vcCity,                                                
--  ConAdd.vcState,                                                
--  ConAdd.intPostalCode,                                                
--  ConAdd.vcCountry,                                                
  ADC.vcPosition,                                                
  ADC.numPhone,                                                
  ADC.numPhoneExtension,                                                
  cmp.numNoOfEmployeesId,                                                
  cmp.numAnnualRevID,                                                
  cmp.vcHow,                                                
  cmp.vcProfile,                                                
  ADC.txtNotes,                                              
  DM.numCreatedBy,                                       
  dm.numgrpid ,                           
  cmp.txtComments,                             
  cmp.numCompanyType,                         
 ADC.vcTitle,                        
  DM.vcComPhone,                        
  DM.vcComFax,                                  
  ISNULL(dbo.fn_GetContactName(DM.numCreatedBy), '&nbsp;&nbsp;-')+' '+convert(varchar(20),dateadd(minute,-@ClientTimeZoneOffset,DM.bintCreatedDate)) AS vcCreatedBy,                                                                
  DM.numModifiedBy,                                        
  ISNULL(dbo.fn_GetContactName(DM.numModifiedBy), '&nbsp;&nbsp;-')+' '+ convert(varchar(20),dateadd(minute,-@ClientTimeZoneOffset,DM.bintModifiedDate)) AS vcModifiedBy,                                                           
  DM.numRecOwner,                                        
  ISNULL(dbo.fn_GetContactName(DM.numRecOwner), '&nbsp;&nbsp;-') AS vcRecordOwner,               
  DM.numAssignedBy,              
  DM.numAssignedTo,                                       
  DM.numCampaignID   ,      
    dbo.getCompanyAddress(dm.numdivisionId,1,dm.numDomainID) as address,DM.vcPartnerCode AS vcPartnerCode   ,
	ISNULL(DM.numPartenerSource,0) AS numPartenerSourceId,
 D3.vcPartnerCode+'-'+C3.vcCompanyName as numPartenerSource,
 ISNULL(DM.numPartenerContact,0) AS numPartenerContactId,
 A1.vcFirstName+' '+A1.vcLastName AS numPartenerContact,
 (select  count(*) from dbo.GenericDocuments where numRecID= DM.numDivisionID and  vcDocumentSection='A' AND tintDocumentType=2) as DocumentCount,
(CASE WHEN ADC.numECampaignID > 0 THEN dbo.fn_FollowupDetailsInOrgList(ADC.numContactID,1,@numDomainID) ELSE '' END) vcLastFollowup,
(CASE WHEN ADC.numECampaignID > 0 THEN dbo.fn_FollowupDetailsInOrgList(ADC.numContactID,2,@numDomainID) ELSE '' END) vcNextFollowup,
ISNULL(DM.vcBuiltWithJson,'') vcBuiltWithJson
--case when DM.vcBillStreet is null then '' when DM.vcBillStreet = ''then '' else vcBillStreet end +         
-- case when DM.vcBillCity is null then '' when DM.vcBillCity ='' then '' else DM.vcBillCity end +         
-- case when dbo.fn_GetState(DM.vcBilState) is null then '' when dbo.fn_GetState(DM.vcBilState) ='' then '' else ','+ dbo.fn_GetState(DM.vcBilState)end +         
-- case when DM.vcBillPostCode is null then '' when DM.vcBillPostCode ='' then '' else ','+DM.vcBillPostCode end +         
-- case when dbo.fn_GetListName(DM.vcBillCountry,0) is null then '' when dbo.fn_GetListName(DM.vcBillCountry,0) ='' then '' else ','+dbo.fn_GetListName(DM.vcBillCountry,0) end  as address                                         
 FROM                                                  
  CompanyInfo cmp join DivisionMaster DM                                                 
  on cmp.numCompanyID=DM.numCompanyID                                 
  join  AdditionalContactsInformation ADC                                                   
   on  DM.numDivisionID=ADC.numDivisionID    
     LEFT JOIN divisionMaster D3 ON DM.numPartenerSource = D3.numDivisionID
  LEFT JOIN CompanyInfo C3 ON C3.numCompanyID = D3.numCompanyID    
  LEFT JOIN AdditionalContactsInformation A1 ON DM.numPartenerContact = A1.numContactId                       
 WHERE                                                                  
  DM.tintCRMType = 0                                                                          
  AND ADC.numContactId = @numContactID  and ADC.numDomainID=@numDomainID                                              
END
GO
