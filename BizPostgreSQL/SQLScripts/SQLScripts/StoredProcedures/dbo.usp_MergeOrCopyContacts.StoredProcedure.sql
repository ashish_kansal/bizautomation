/****** Object:  StoredProcedure [dbo].[usp_MergeOrCopyContacts]    Script Date: 07/26/2008 16:20:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created By: Debasish Tapan Nag                                                          
--Purpose: Merge or Copy Contacts to another parent                    
--Created Date: 01/16/2006                    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_mergeorcopycontacts')
DROP PROCEDURE usp_mergeorcopycontacts
GO
CREATE PROCEDURE    [dbo].[usp_MergeOrCopyContacts]                  
 @vcContactIDs NVarchar(400),                      
 @numCompanyId Numeric,                    
 @numDivisionId Numeric,                    
 @numUserCntId Numeric,   --The User who is performing the action                    
 @vcAction Char(1)   --C: Copy/ M: merge                    
AS                                                
BEGIN                    
   IF @vcAction = 'C' --Request is for Copying Contacts                    
   BEGIN                   
       INSERT INTO AdditionalContactsInformation(vcDepartment, vcCategory, vcGivenName, vcFirstName, vcLastName,                     
       numDivisionId, numContactType, numTeam, numPhone, numPhoneExtension,                    
       numCell, numHomePhone, vcFax, vcEmail, vcAsstFirstName, vcAsstLastName,                    
       numAsstPhone, numAsstExtn, vcAsstEmail,     
       charSex, bintDOB, vcPosition, txtNotes,                    
       numCreatedBy, bintCreatedDate,numModifiedby,bintModifiedDate,                     
       numDomainID, bitOptOut,bitPrimaryContact)                  
              SELECT vcDepartment, vcCategory, vcGivenName, vcFirstName, vcLastName,                     
              @numDivisionId AS numDivisionId, numContactType, numTeam, numPhone, numPhoneExtension,                    
              numCell, numHomePhone, vcFax, vcEmail, vcAsstFirstName, vcAsstLastName,                    
              numAsstPhone, numAsstExtn, vcAsstEmail,     
              charSex, bintDOB, vcPosition, txtNotes,                    
              @numUserCntId, getutcdate(),@numUserCntId, getutcdate(),    
              numDomainID, bitOptOut,bitPrimaryContact                    
              FROM AdditionalContactsInformation WHERE numContactId IN (SELECT Items FROM dbo.Split(@vcContactIDs,','))      
    
      
      /*    
       INSERT INTO ContactAddress(numContactId, vcStreet, vcCity, intPostalCode,                    
       vcState, vcCountry, vcContactLocation, vcContactLocation1, vcStreet1, vcCity1,                    
       vcPostalCode1, vcState1, vcCountry1, vcContactLocation2, vcStreet2, vcCity2,                    
       vcPostalCode2, vcState2, vcCountry2, vcContactLocation3, vcStreet3, vcCity3,                    
       vcPostalCode3, vcState3, vcCountry3, vcContactLocation4, vcStreet4, vcCity4,                    
       vcPostalCode4, vcState4, vcCountry4,                     
       vcPStreet, vcPCity, vcPPostalCode, vcPState, vcPCountry)    
      */            
      UPDATE AdditionalContactsInformation SET bitOptOut = 1, numEmpStatus = 3,                  
      numModifiedBy = @numUserCntId, bintModifiedDate = getutcdate()                  
      WHERE numContactId IN (SELECT Items FROM dbo.Split(@vcContactIDs,','))                  
      
      if (select count(*) from AdditionalContactsInformation where numDivisionID= @numDivisionId and  ISNULL(bitPrimaryContact,0)=1)>1
      begin
		  UPDATE AdditionalContactsInformation SET bitPrimaryContact = 0      
		  WHERE numContactId IN (SELECT Items FROM dbo.Split(@vcContactIDs,','))        
		  AND ISNULL(bitPrimaryContact,0)=1
	  end     
   END                    
   ELSE IF @vcAction = 'M' --Request is for Merging Contacts                    
   BEGIN                   
       PRINT 'In Merge'                   
       --Changing the Division Id of the Contact in Communication                  
       UPDATE Communication SET numDivisionId = @numDivisionId, numModifiedBy = @numUserCntId, dtModifiedDate = getutcdate()               
       WHERE numContactId IN (SELECT Items FROM dbo.Split(@vcContactIDs,','))                  
       --Changing the Company Id in CampaignDetails                   
       UPDATE CampaignDetails SET numCompanyId = @numCompanyId           
       WHERE numContactId IN (SELECT Items FROM dbo.Split(@vcContactIDs,','))                  
       --Changing the Division Id of the Contact in Cases                  
    UPDATE Cases SET numDivisionId = @numDivisionId, numModifiedBy = @numUserCntId, bintModifiedDate = getutcdate()                  
       WHERE numContactId IN (SELECT Items FROM dbo.Split(@vcContactIDs,','))                  
       --Changing the Division Id for the contact in OpportunityMaster                  
       UPDATE OpportunityMaster SET numDivisionId = @numDivisionId, numModifiedBy = @numUserCntId, bintModifiedDate = getutcdate()                  
       WHERE numContactId IN (SELECT Items FROM dbo.Split(@vcContactIDs,','))                  
       --Changing the Division Id for the contact in AdditionalContactsInformation                  
       UPDATE AdditionalContactsInformation SET numDivisionId = @numDivisionId, numModifiedBy = @numUserCntId, bintModifiedDate = getutcdate()                  
       WHERE numContactId IN (SELECT Items FROM dbo.Split(@vcContactIDs,','))                   
      
       UPDATE AdditionalContactsInformation SET bitPrimaryContact = 0      
       WHERE numContactId IN (SELECT Items FROM dbo.Split(@vcContactIDs,','))        
       AND ISNULL(bitPrimaryContact,0)=1      
   END                  
END
GO
