/****** Object:  StoredProcedure [dbo].[usp_GetAllGroups]    Script Date: 07/26/2008 16:16:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Modified: Debasish Nag  
--Modified On: 4th July 2006  
--Reason Modified: Groups table structually changed and columns removed  
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getallgroups')
DROP PROCEDURE usp_getallgroups
GO
CREATE PROCEDURE [dbo].[usp_GetAllGroups]    
 @numUserID numeric=0       
AS    
--This procedure will return all Groups for the selected Domain.    
--If UserId is not 0 then turn all groups for the user.    
BEGIN    
  SELECT numGrpID, vcGrpName     
   FROM groups     
   --WHERE bitPublic=1    
   ORDER BY vcGrpName 
    
    
END
GO
