/****** Object:  StoredProcedure [dbo].[USP_GetOpportunityBizDocsCount]    Script Date: 07/26/2008 16:17:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created By Siva
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getopportunitybizdocscount')
DROP PROCEDURE usp_getopportunitybizdocscount
GO
CREATE PROCEDURE [dbo].[USP_GetOpportunityBizDocsCount]
(
@numOppId AS NUMERIC(9),
@numDomainId AS NUMERIC(9),
@numBizDocsId as numeric(9))
As
BEGIN

 DECLARE @tintOppType AS TINYINT;	
 SELECT @tintOppType=tintOppType FROM dbo.OpportunityMaster WHERE numDomainId=@numDomainId AND numOppId=@numOppId
 
 IF @tintOppType=1 --Sales
 BEGIN
	SELECT Count(*) FROM dbo.DepositeDetails WHERE numOppBizDocsID = @numBizDocsId
 END
 ELSE --Purchase
 BEGIN
	SELECT Count(*) FROM dbo.BillPaymentDetails WHERE ISNULL(numOppBizDocsID,0)>0 AND numOppBizDocsID = @numBizDocsId
 END
 
End
GO
