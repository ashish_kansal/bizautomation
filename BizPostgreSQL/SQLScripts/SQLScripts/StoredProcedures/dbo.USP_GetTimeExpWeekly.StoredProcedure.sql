/****** Object:  StoredProcedure [dbo].[USP_GetTimeExpWeekly]    Script Date: 07/26/2008 16:18:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--exec USP_GetTimeExpWeekly 1,1,'8/6/2006'                
--created by anoop jayaraj                
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_gettimeexpweekly')
DROP PROCEDURE usp_gettimeexpweekly
GO
CREATE PROCEDURE [dbo].[USP_GetTimeExpWeekly]                
@numUserCntID as numeric(9)=0,                
@numDomainID as numeric(9)=0,                
@FromDate as datetime ,              
@numCategoryType as int=0 ,        
@ClientTimeZoneOffset as int              
as          
declare  @strDate as datetime     
set @strDate = @fromDate    

IF ISNULL(@numUserCntID,0) > 0
BEGIN
	
select      
'-'   as day0,    
 dbo.GetDaydetails(@numUserCntID,@strDate,@numCategoryType,@ClientTimeZoneOffset) as Day1,                
 dbo.GetDaydetails(@numUserCntID,convert(char(10),dateadd(day,1,@strDate),101),@numCategoryType,@ClientTimeZoneOffset)   as Day2,                
 dbo.GetDaydetails(@numUserCntID,convert(char(10),dateadd(day,2,@strDate),101),@numCategoryType,@ClientTimeZoneOffset)   as Day3,                
 dbo.GetDaydetails(@numUserCntID,convert(char(10),dateadd(day,3,@strDate),101),@numCategoryType,@ClientTimeZoneOffset)   as Day4,                
 dbo.GetDaydetails(@numUserCntID,convert(char(10),dateadd(day,4,@strDate),101),@numCategoryType,@ClientTimeZoneOffset)   as Day5,                
 dbo.GetDaydetails(@numUserCntID,convert(char(10),dateadd(day,5,@strDate),101),@numCategoryType,@ClientTimeZoneOffset)   as Day6,                
 dbo.GetDaydetails(@numUserCntID,convert(char(10),dateadd(day,6,@strDate),101),@numCategoryType,@ClientTimeZoneOffset)   as Day7,                
 dbo.GetDaydetails(@numUserCntID,convert(char(10),dateadd(day,7,@strDate),101),@numCategoryType,@ClientTimeZoneOffset)   as Day8,    
 dbo.GetDaydetails(@numUserCntID,convert(char(10),dateadd(day,8,@strDate),101),@numCategoryType,@ClientTimeZoneOffset)   as Day9,    
 dbo.GetDaydetails(@numUserCntID,convert(char(10),dateadd(day,9,@strDate),101),@numCategoryType,@ClientTimeZoneOffset)   as Day10,    
 dbo.GetDaydetails(@numUserCntID,convert(char(10),dateadd(day,10,@strDate),101),@numCategoryType,@ClientTimeZoneOffset)  as Day11,    
 dbo.GetDaydetails(@numUserCntID,convert(char(10),dateadd(day,11,@strDate),101),@numCategoryType,@ClientTimeZoneOffset)  as Day12,                
 dbo.GetDaydetails(@numUserCntID,convert(char(10),dateadd(day,12,@strDate),101),@numCategoryType,@ClientTimeZoneOffset)  as Day13,                
 dbo.GetDaydetails(@numUserCntID,convert(char(10),dateadd(day,13,@strDate),101),@numCategoryType,@ClientTimeZoneOffset)  as Day14,                
 dbo.GetDaydetails(@numUserCntID,convert(char(10),dateadd(day,14,@strDate),101),@numCategoryType,@ClientTimeZoneOffset)  as Day15,                
 dbo.GetDaydetails(@numUserCntID,convert(char(10),dateadd(day,15,@strDate),101),@numCategoryType,@ClientTimeZoneOffset)  as Day16,                
 dbo.GetDaydetails(@numUserCntID,convert(char(10),dateadd(day,16,@strDate),101),@numCategoryType,@ClientTimeZoneOffset)  as Day17,                
 dbo.GetDaydetails(@numUserCntID,convert(char(10),dateadd(day,17,@strDate),101),@numCategoryType,@ClientTimeZoneOffset)  as Day18,    
 dbo.GetDaydetails(@numUserCntID,convert(char(10),dateadd(day,18,@strDate),101),@numCategoryType,@ClientTimeZoneOffset)  as Day19,    
 dbo.GetDaydetails(@numUserCntID,convert(char(10),dateadd(day,19,@strDate),101),@numCategoryType,@ClientTimeZoneOffset)  as Day20,    
 dbo.GetDaydetails(@numUserCntID,convert(char(10),dateadd(day,20,@strDate),101),@numCategoryType,@ClientTimeZoneOffset)  as Day21,    
 dbo.GetDaydetails(@numUserCntID,convert(char(10),dateadd(day,21,@strDate),101),@numCategoryType,@ClientTimeZoneOffset)  as Day22,                
 dbo.GetDaydetails(@numUserCntID,convert(char(10),dateadd(day,22,@strDate),101),@numCategoryType,@ClientTimeZoneOffset)  as Day23,                
 dbo.GetDaydetails(@numUserCntID,convert(char(10),dateadd(day,23,@strDate),101),@numCategoryType,@ClientTimeZoneOffset)  as Day24,                
 dbo.GetDaydetails(@numUserCntID,convert(char(10),dateadd(day,24,@strDate),101),@numCategoryType,@ClientTimeZoneOffset)  as Day25,                
 dbo.GetDaydetails(@numUserCntID,convert(char(10),dateadd(day,25,@strDate),101),@numCategoryType,@ClientTimeZoneOffset)  as Day26,                
 dbo.GetDaydetails(@numUserCntID,convert(char(10),dateadd(day,26,@strDate),101),@numCategoryType,@ClientTimeZoneOffset)  as Day27,                
 dbo.GetDaydetails(@numUserCntID,convert(char(10),dateadd(day,27,@strDate),101),@numCategoryType,@ClientTimeZoneOffset)  as Day28,    
 dbo.GetDaydetails(@numUserCntID,convert(char(10),dateadd(day,28,@strDate),101),@numCategoryType,@ClientTimeZoneOffset)  as Day29,    
 dbo.GetDaydetails(@numUserCntID,convert(char(10),dateadd(day,29,@strDate),101),@numCategoryType,@ClientTimeZoneOffset)  as Day30,    
 dbo.GetDaydetails(@numUserCntID,convert(char(10),dateadd(day,30,@strDate),101),@numCategoryType,@ClientTimeZoneOffset)  as Day31,    
     
    
(        
 select isnull(sum(convert(float,DATEDIFF(mi, dtFromDate, dtToDate))/60),0)  as [time]               
 from TimeAndExpense where (dateadd(minute,-@ClientTimeZoneOffset,dtFromDate) between @strDate and dateadd(day,30,@strDate))                 
 and numCategory=1 and numUserCntID=@numUserCntID and numType=1 )        
        
as BillableHours,          
           
(        
 select isnull(sum(convert(float,DATEDIFF(mi, dtFromDate, dtToDate))/60),0)  as [time]               
 from TimeAndExpense where (dateadd(minute,-@ClientTimeZoneOffset,dtFromDate) between @strDate and dateadd(day,30,@strDate))                 
 and numCategory=1 and numUserCntID=@numUserCntID and numType=2 )        
        
as NonBillableHours,             
 (        
 select round(isnull(sum(monAmount),0),1)    as  monAmount           
 from TimeAndExpense                 
 where     
( dtFromDate   
between @strDate and dateadd(day,30,@strDate))                 
 and numCategory=2 and numUserCntID=@numUserCntID and numType IN (1,6) )        
        
        
 as BillableAmount,                
 (        
 select round(isnull(sum(monAmount),0),1)    as  monAmount           
 from TimeAndExpense                 
 where ( dtFromDate   
 between @strDate and dateadd(day,30,@strDate))                 
 and numCategory=2 and numUserCntID=@numUserCntID and numType=2 )  as NonBillableAmount

END

ELSE
BEGIN

select      
'-'   as day0,   		
 dbo.GetDayDetailsForDomain(@numDomainID,@strDate,@numCategoryType,@ClientTimeZoneOffset) as Day1,                
 dbo.GetDayDetailsForDomain(@numDomainID,convert(char(10),dateadd(day,1,@strDate),101),@numCategoryType,@ClientTimeZoneOffset)   as Day2,                
 dbo.GetDayDetailsForDomain(@numDomainID,convert(char(10),dateadd(day,2,@strDate),101),@numCategoryType,@ClientTimeZoneOffset)   as Day3,                
 dbo.GetDayDetailsForDomain(@numDomainID,convert(char(10),dateadd(day,3,@strDate),101),@numCategoryType,@ClientTimeZoneOffset)   as Day4,                
 dbo.GetDayDetailsForDomain(@numDomainID,convert(char(10),dateadd(day,4,@strDate),101),@numCategoryType,@ClientTimeZoneOffset)   as Day5,                
 dbo.GetDayDetailsForDomain(@numDomainID,convert(char(10),dateadd(day,5,@strDate),101),@numCategoryType,@ClientTimeZoneOffset)   as Day6,                
 dbo.GetDayDetailsForDomain(@numDomainID,convert(char(10),dateadd(day,6,@strDate),101),@numCategoryType,@ClientTimeZoneOffset)   as Day7,                
 dbo.GetDayDetailsForDomain(@numDomainID,convert(char(10),dateadd(day,7,@strDate),101),@numCategoryType,@ClientTimeZoneOffset)   as Day8,    
 dbo.GetDayDetailsForDomain(@numDomainID,convert(char(10),dateadd(day,8,@strDate),101),@numCategoryType,@ClientTimeZoneOffset)   as Day9,    
 dbo.GetDayDetailsForDomain(@numDomainID,convert(char(10),dateadd(day,9,@strDate),101),@numCategoryType,@ClientTimeZoneOffset)   as Day10,    
 dbo.GetDayDetailsForDomain(@numDomainID,convert(char(10),dateadd(day,10,@strDate),101),@numCategoryType,@ClientTimeZoneOffset)  as Day11,    
 dbo.GetDayDetailsForDomain(@numDomainID,convert(char(10),dateadd(day,11,@strDate),101),@numCategoryType,@ClientTimeZoneOffset)  as Day12,                
 dbo.GetDayDetailsForDomain(@numDomainID,convert(char(10),dateadd(day,12,@strDate),101),@numCategoryType,@ClientTimeZoneOffset)  as Day13,                
 dbo.GetDayDetailsForDomain(@numDomainID,convert(char(10),dateadd(day,13,@strDate),101),@numCategoryType,@ClientTimeZoneOffset)  as Day14,                
 dbo.GetDayDetailsForDomain(@numDomainID,convert(char(10),dateadd(day,14,@strDate),101),@numCategoryType,@ClientTimeZoneOffset)  as Day15,                
 dbo.GetDayDetailsForDomain(@numDomainID,convert(char(10),dateadd(day,15,@strDate),101),@numCategoryType,@ClientTimeZoneOffset)  as Day16,                
 dbo.GetDayDetailsForDomain(@numDomainID,convert(char(10),dateadd(day,16,@strDate),101),@numCategoryType,@ClientTimeZoneOffset)  as Day17,                
 dbo.GetDayDetailsForDomain(@numDomainID,convert(char(10),dateadd(day,17,@strDate),101),@numCategoryType,@ClientTimeZoneOffset)  as Day18,    
 dbo.GetDayDetailsForDomain(@numDomainID,convert(char(10),dateadd(day,18,@strDate),101),@numCategoryType,@ClientTimeZoneOffset)  as Day19,    
 dbo.GetDayDetailsForDomain(@numDomainID,convert(char(10),dateadd(day,19,@strDate),101),@numCategoryType,@ClientTimeZoneOffset)  as Day20,    
 dbo.GetDayDetailsForDomain(@numDomainID,convert(char(10),dateadd(day,20,@strDate),101),@numCategoryType,@ClientTimeZoneOffset)  as Day21,    
 dbo.GetDayDetailsForDomain(@numDomainID,convert(char(10),dateadd(day,21,@strDate),101),@numCategoryType,@ClientTimeZoneOffset)  as Day22,                
 dbo.GetDayDetailsForDomain(@numDomainID,convert(char(10),dateadd(day,22,@strDate),101),@numCategoryType,@ClientTimeZoneOffset)  as Day23,                
 dbo.GetDayDetailsForDomain(@numDomainID,convert(char(10),dateadd(day,23,@strDate),101),@numCategoryType,@ClientTimeZoneOffset)  as Day24,                
 dbo.GetDayDetailsForDomain(@numDomainID,convert(char(10),dateadd(day,24,@strDate),101),@numCategoryType,@ClientTimeZoneOffset)  as Day25,                
 dbo.GetDayDetailsForDomain(@numDomainID,convert(char(10),dateadd(day,25,@strDate),101),@numCategoryType,@ClientTimeZoneOffset)  as Day26,                
 dbo.GetDayDetailsForDomain(@numDomainID,convert(char(10),dateadd(day,26,@strDate),101),@numCategoryType,@ClientTimeZoneOffset)  as Day27,                
 dbo.GetDayDetailsForDomain(@numDomainID,convert(char(10),dateadd(day,27,@strDate),101),@numCategoryType,@ClientTimeZoneOffset)  as Day28,    
 dbo.GetDayDetailsForDomain(@numDomainID,convert(char(10),dateadd(day,28,@strDate),101),@numCategoryType,@ClientTimeZoneOffset)  as Day29,    
 dbo.GetDayDetailsForDomain(@numDomainID,convert(char(10),dateadd(day,29,@strDate),101),@numCategoryType,@ClientTimeZoneOffset)  as Day30,    
 dbo.GetDayDetailsForDomain(@numDomainID,convert(char(10),dateadd(day,30,@strDate),101),@numCategoryType,@ClientTimeZoneOffset)  as Day31,    
    
(        
 select isnull(sum(convert(float,DATEDIFF(mi, dtFromDate, dtToDate))/60),0)  as [time]               
 from TimeAndExpense where (dateadd(minute,-330,dtFromDate) between @strDate and dateadd(day,30,@strDate))                 
 and numCategory=1 and numDomainID=@numDomainID and numType=1 )        
        
as BillableHours,          
           
(        
 select isnull(sum(convert(float,DATEDIFF(mi, dtFromDate, dtToDate))/60),0)  as [time]               
 from TimeAndExpense where (dateadd(minute,-330,dtFromDate) between @strDate and dateadd(day,30,@strDate))                 
 and numCategory=1 and numDomainID=@numDomainID and numType=2 )        
        
as NonBillableHours,             
 (        
 select round(isnull(sum(monAmount),0),1)    as  monAmount           
 from TimeAndExpense                 
 where     
( dtFromDate   
between @strDate and dateadd(day,30,@strDate))                 
 and numCategory=2 and numDomainID=@numDomainID and numType IN(1,6) )        
        
        
 as BillableAmount,                
 (        
 select round(isnull(sum(monAmount),0),1)    as  monAmount           
 from TimeAndExpense                 
 where ( dtFromDate   
 between @strDate and dateadd(day,30,@strDate))                 
 and numCategory=2 and numDomainID=@numDomainID and numType=2 )  as NonBillableAmount

END 
GO
