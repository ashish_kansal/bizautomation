/****** Object:  StoredProcedure [dbo].[usp_GetAllItems]    Script Date: 07/26/2008 16:16:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created By: Debasish Tapan Nag                                          
--Purpose: Return Item List          
--Created Date: 06/22/2006                                
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getallitems')
DROP PROCEDURE usp_getallitems
GO
CREATE PROCEDURE [dbo].[usp_GetAllItems]  
@numDomainID as numeric(9)=0                
AS                                       
BEGIN   
 SELECT numItemCode, vcItemName FROM Item 
 where numDomainID=@numDomainID 
 ORDER BY vcItemName  
END
GO
