/****** Object:  StoredProcedure [dbo].[usp_GetListDetails]    Script Date: 07/26/2008 16:17:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getlistdetails')
DROP PROCEDURE usp_getlistdetails
GO
CREATE PROCEDURE [dbo].[usp_GetListDetails]
@vcListName varchar(100) = ''
as
Begin
Declare @numListId numeric
Select @numListId = numListId from ListMaster where vcListName = @vcListName
Select numListItemID, vcData from ListDetails where numListID = @numListID 
End
GO
