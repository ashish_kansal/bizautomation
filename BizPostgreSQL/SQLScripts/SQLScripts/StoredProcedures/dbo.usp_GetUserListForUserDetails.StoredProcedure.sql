/****** Object:  StoredProcedure [dbo].[usp_GetUserListForUserDetails]    Script Date: 07/26/2008 16:18:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj      
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getuserlistforuserdetails')
DROP PROCEDURE usp_getuserlistforuserdetails
GO
CREATE PROCEDURE [dbo].[usp_GetUserListForUserDetails]         
@numUserID NUMERIC(9) = 0,    
@numDomainID numeric(9)=0        
     
AS                 
          
  
select A.numContactId, ISNULL(A.vcfirstname,'') +' '+ ISNULL(A.vclastname,'') as vcGivenName       
From AdditionalContactsInformation A   
left join Domain D  
on D.numDomainID=A.numDomainID       
where A.numDivisionID=D.numDivisionID and A.numDomainID=@numDomainID 
and A.numContactId not in (select distinct(numUserDetailId) from UserMaster where numDomainID=@numDomainID and numUserID<>@numUserID)

