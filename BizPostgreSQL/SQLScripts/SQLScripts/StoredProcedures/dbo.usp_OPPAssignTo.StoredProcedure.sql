/****** Object:  StoredProcedure [dbo].[usp_OPPAssignTo]    Script Date: 07/26/2008 16:20:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created By Anoop jayaraj 
     
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_oppassignto')
DROP PROCEDURE usp_oppassignto
GO
CREATE PROCEDURE [dbo].[usp_OPPAssignTo]                    
 @numDomainID numeric(9)=null,                    
 @numContactType numeric(9)=null              
AS                    
                 
                             
  BEGIN                    

 SELECT A.numContactID as ContactID,A.vcFirstName+' '+A.vcLastName as vcName        
 from UserMaster UM       
 join AdditionalContactsInformation A      
 on UM.numUserDetailId=A.numContactID        
 where UM.numDomainID=@numDomainID   and A.numDomainID=UM.numDomainID 
 union  
 select  A.numContactID,+A.vcFirstName+' '+A.vcLastName+' - '+ vcCompanyName  
 from AdditionalContactsInformation A   
 join DivisionMaster D  
 on D.numDivisionID=A.numDivisionID  
 join ExtarnetAccounts E   
 on E.numDivisionID=D.numDivisionID  
 join ExtranetAccountsDtl DTL  
 on DTL.numExtranetID=E.numExtranetID  
 join CompanyInfo C  
 on C.numCompanyID=D.numCompanyID  
 where A.numDomainID=@numDomainID
 and A.numDomainID=D.numDomainID
 and D.numDomainID=E.numDomainID  
 and D.numDivisionID <>(select numDivisionID from domain where numDomainID=@numDomainID) 
                
  END
GO
