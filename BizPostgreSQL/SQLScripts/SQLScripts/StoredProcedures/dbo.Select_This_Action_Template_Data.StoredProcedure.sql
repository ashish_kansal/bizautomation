/****** Object:  StoredProcedure [dbo].[Select_This_Action_Template_Data]    Script Date: 07/26/2008 16:14:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='select_this_action_template_data')
DROP PROCEDURE select_this_action_template_data
GO
CREATE PROCEDURE [dbo].[Select_This_Action_Template_Data]    
(     
@rowID Int     
)    
As    
Select TemplateName , DueDays , Priority , Type , Activity , isnull(Comments,'') as Comments,isnull(bitSendEmailTemplate,0) bitSendEmailTemp,isnull(numEmailTemplate,0) numEmailTemplate,isnull(tintHours,0) tintHours,isnull(bitAlert,0) bitAlert,
isnull(numTaskType,0) as numTaskType,ISNULL(bitRemind,0) AS bitRemind,ISNULL(numRemindBeforeMinutes,0) AS numRemindBeforeMinutes
From tblActionItemData Where RowID = @rowID
GO
