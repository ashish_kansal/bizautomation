set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go

--created by anoop jayaraj   
--EXEC [USP_GetPageNavigation] @numModuleID=9,@numDomainID=1

         
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getpagenavigation')
DROP PROCEDURE usp_getpagenavigation
GO
CREATE PROCEDURE [dbo].[USP_GetPageNavigation]            
@numModuleID as numeric(9),      
@numDomainID as numeric(9),
@numGroupID NUMERIC(18, 0)        
as            
if @numModuleID = 14      
begin
           
select  PND.numPageNavID,
        numModuleID,
        numParentID,
        vcPageNavName,
        isnull(vcNavURL, '') as vcNavURL,
        vcImageURL,
        0 intSortOrder
from    PageNavigationDTL PND
        JOIN dbo.TreeNavigationAuthorization TNA ON PND.numPageNavID = TNA.numPageNavID  --AND PND.numTabID = TNA.numTabID       
 WHERE  1 = 1
        AND ( ISNULL(TNA.bitVisible, 0) = 1
              --OR ISNULL(numParentID, 0) = 0
              --OR ISNULL(TNA.bitVisible, 0) = 0
            )
        AND ISNULL(PND.bitVisible, 0) = 1
        AND numModuleID = 14
        AND TNA.numDomainID = @numDomainID
        AND numGroupID = @numGroupID
union
select  1111 numPageNavID,
        14 numModuleID,
        133 numParentID,
        'Regular Documents' as vcPageNavName,
        '../Documents/frmRegularDocList.aspx?Category=0',
        '../images/tf_note.gif' vcImageURL,
        1
union
SELECT  Ld.numListItemId,
        14 numModuleID,
        1111 numParentID,
        vcData vcPageNavName,
        '../Documents/frmRegularDocList.aspx?Category='
        + cast(Ld.numListItemId as varchar(10)) vcImageURL,
        '../images/tf_note.gif',
        ISNULL(intSortOrder, LD.sintOrder) SortOrder
FROM    listdetails Ld
        left join listorder LO on Ld.numListItemID = LO.numListItemID
                                  and Lo.numDomainId = @numDomainID
WHERE   Ld.numListID = 29
        and ( constFlag = 1
              or Ld.numDomainID = @numDomainID
            )   
union
SELECT  ld.numListItemID AS numPageNavID,
        14 numModuleID,
        133 numParentID,
        vcData vcPageNavName,
        '../Documents/frmDocList.aspx?Category='
        + convert(varchar(10), LD.numListItemID) vcNavURL,
        '../images/tf_note.gif' vcImageURL,
        -3
FROM    listdetails Ld
        left join listorder LO on Ld.numListItemID = LO.numListItemID
                                  and lo.numDomainId = @numDomainId
        inner join AuthoritativeBizDocs AB on ( LD.numListItemID = AB.numAuthoritativeSales )
WHERE   LD.numListID = 27
        and ( constFlag = 1
              or LD.numDomainID = @numDomainID
            )
        and AB.numDomainID = @numDomainID
union
SELECT  ld.numListItemID AS numPageNavID,
        14 numModuleID,
        133 numParentID,
        vcData vcPageNavName,
        '../Documents/frmDocList.aspx?Category='
        + convert(varchar(10), LD.numListItemID) vcNavURL,
        '../images/tf_note.gif' vcImageURL,
        -3
FROM    listdetails Ld
        left join listorder LO on Ld.numListItemID = LO.numListItemID
                                  and lo.numDomainId = @numDomainId
        inner join AuthoritativeBizDocs AB on ( ld.numListItemID = AB.numAuthoritativePurchase )
WHERE   LD.numListID = 27
        and ( constFlag = 1
              or LD.numDomainID = @numDomainID
            )
        and AB.numDomainID = @numDomainID
UNION
select  0 numPageNavID,
        14 numModuleID,
        LD.numListItemID AS numParentID,
        LT.vcData as vcPageNavName,
        '../Documents/frmDocList.aspx?Category='
        + convert(varchar(10), Ld.numListItemID) + '&Status='
        + convert(varchar(10), LT.numListItemID) vcNavURL,
        '../images/Text.gif' vcImageURL,
        0
from    
	ListDetails LT
OUTER APPLY
(
	SELECT
		*
	FROM
        listdetails 
	WHERE
		numListID = 27
        AND (constFlag = 1 OR numDomainID = @numDomainID)
        AND (numListItemID IN (SELECT AB.numAuthoritativeSales FROM AuthoritativeBizDocs AB WHERE AB.numDomainId = @numDomainID)
              or numListItemID IN (SELECT AB.numAuthoritativePurchase FROM AuthoritativeBizDocs AB WHERE AB.numDomainId = @numDomainID))
) ld
where   LT.numListID = 11
        and Lt.numDomainID = @numDomainID 
union
select  2222 numPageNavID,
        14 numModuleID,
        133 numParentID,
        'Other BizDocs' as vcPageNavName,
        '',
        '../images/tf_note.gif' vcImageURL,
        -1
union
SELECT  ld.numListItemID AS numPageNavID,
        14 numModuleID,
        2222 numParentID,
        vcData vcPageNavName,
        '../Documents/frmDocList.aspx?Category='
        + convert(varchar(10), LD.numListItemID) vcNavURL,
        '../images/Text.gif' vcImageURL,
        -1
FROM    listdetails Ld
where   ld.numListID = 27
        and ( constFlag = 1
              or LD.numDomainID = @numDomainID
            )
        and LD.numListItemID not in ( select    AB.numAuthoritativeSales
                                      from      AuthoritativeBizDocs AB
                                      where     AB.numDomainID = @numDomainID )
        and LD.numListItemID not in ( select    AB.numAuthoritativePurchase
                                      from      AuthoritativeBizDocs AB
                                      where     AB.numDomainID = @numDomainID )
union
SELECT  ls.numListItemID AS numPageNavID,
        14 numModuleID,
        ld.numListItemID as numParentID,
        ls.vcData vcPageNavName,
        '../Documents/frmDocList.aspx?Category='
        + convert(varchar(10), LD.numListItemID) vcNavURL,
        null vcImageURL,
        0
FROM    
	listdetails Ld
OUTER APPLY
( 
	SELECT  
		*
    FROM 
		ListDetails LS
    WHERE
		LS.numDomainID = @numDomainID
        AND LS.numListID = 11
) LS
WHERE 
	ld.numListID = 27
    AND (ld.constFlag = 1 OR LD.numDomainID = @numDomainID)
    AND LD.numListItemID not in ( select    AB.numAuthoritativeSales
                                    from      AuthoritativeBizDocs AB
                                    where     AB.numDomainID = @numDomainID
                                    union
                                    select    AB.numAuthoritativePurchase
                                    from      AuthoritativeBizDocs AB
                                    where     AB.numDomainID = @numDomainID )
ORDER BY 
	numParentID,
    numPageNavID

END
ELSE IF @numModuleID = 35 
BEGIN
	select PND.numPageNavID,numModuleID,numParentID,vcPageNavName,isnull(vcNavURL,'') as vcNavURL,vcImageURL ,0 sintOrder      
	from PageNavigationDTL PND
	JOIN dbo.TreeNavigationAuthorization TNA ON PND.numPageNavID = TNA.numPageNavID  --AND PND.numTabID = TNA.numTabID       
	WHERE  1 = 1
        AND ( ISNULL(TNA.bitVisible, 0) = 1
              --OR ISNULL(numParentID, 0) = 0
              --OR ISNULL(TNA.bitVisible, 0) = 0
            ) 
        AND ISNULL(PND.bitVisible, 0) = 1
	AND numModuleID=@numModuleID --AND bitVisible=1  
	AND TNA.numDomainID = @numDomainID
	AND numGroupID = @numGroupID
	    
	UNION 
	 SELECT numFRID,35,CASE numFinancialViewID WHEN 26770 THEN 96 /*Profit & Loss*/
	WHEN 26769 THEN 176 /*Income & Expense*/
	WHEN 26768 THEN 98 /*Cash Flow Statement*/
	WHEN 26767 THEN 97 /*Balance Sheet*/
	WHEN 26766 THEN 81 /*A/R & A/P Aging*/
	WHEN 26766 THEN 82 /*A/R & A/P Aging*/
	ELSE 0
	END 
	  ,vcReportName,CASE numFinancialViewID 
	WHEN 26770 THEN (SELECT vcNavURL FROM dbo.PageNavigationDTL WHERE numPageNavID =96 AND numModuleID=35)  /*Profit & Loss*/
	WHEN 26769 THEN (SELECT vcNavURL FROM dbo.PageNavigationDTL WHERE numPageNavID =176 AND numModuleID=35) /*Income & Expense*/
	WHEN 26768 THEN (SELECT vcNavURL FROM dbo.PageNavigationDTL WHERE numPageNavID =98 AND numModuleID=35) /*Cash Flow Statement*/
	WHEN 26767 THEN (SELECT vcNavURL FROM dbo.PageNavigationDTL WHERE numPageNavID =97 AND numModuleID=35) /*Balance Sheet*/
	WHEN 26766 THEN (SELECT vcNavURL FROM dbo.PageNavigationDTL WHERE numPageNavID =81 AND numModuleID=35) /*A/R & A/P Aging*/
	WHEN 26766 THEN (SELECT vcNavURL FROM dbo.PageNavigationDTL WHERE numPageNavID =82 AND numModuleID=35) /*A/R & A/P Aging*/
	ELSE ''
	END + 
	CASE CHARINDEX('?',(CASE numFinancialViewID 
	WHEN 26770 THEN (SELECT vcNavURL FROM dbo.PageNavigationDTL WHERE numPageNavID =96 AND numModuleID=35)  /*Profit & Loss*/
	WHEN 26769 THEN (SELECT vcNavURL FROM dbo.PageNavigationDTL WHERE numPageNavID =176 AND numModuleID=35) /*Income & Expense*/
	WHEN 26768 THEN (SELECT vcNavURL FROM dbo.PageNavigationDTL WHERE numPageNavID =98 AND numModuleID=35) /*Cash Flow Statement*/
	WHEN 26767 THEN (SELECT vcNavURL FROM dbo.PageNavigationDTL WHERE numPageNavID =97 AND numModuleID=35) /*Balance Sheet*/
	WHEN 26766 THEN (SELECT vcNavURL FROM dbo.PageNavigationDTL WHERE numPageNavID =81 AND numModuleID=35) /*A/R & A/P Aging*/
	WHEN 26766 THEN (SELECT vcNavURL FROM dbo.PageNavigationDTL WHERE numPageNavID =82 AND numModuleID=35) /*A/R & A/P Aging*/
	ELSE ''
	END)) 
	WHEN 0 THEN '?FRID='
	ELSE '&FRID='
	END
	 + CAST(numFRID AS VARCHAR)
	 'url',NULL,0 FROM dbo.FinancialReport WHERE numDomainID=@numDomainID
	order by numParentID,numPageNavID  
END
-------------- ADDED BY MANISH ANJARA ON : 6th April,2012
ELSE IF @numModuleID = -1 AND @numDomainID = -1 
BEGIN 
	SELECT PND.numPageNavID,numModuleID,numParentID,vcPageNavName,isnull(vcNavURL,'') as vcNavURL,vcImageURL ,0 sintOrder      
	FROM PageNavigationDTL PND
	JOIN dbo.TreeNavigationAuthorization TNA ON PND.numPageNavID = TNA.numPageNavID  --AND PND.numTabID = TNA.numTabID       
	WHERE  1 = 1
        AND ( ISNULL(TNA.bitVisible, 0) = 1
              --OR ISNULL(numParentID, 0) = 0
              --OR ISNULL(TNA.bitVisible, 0) = 0
            )
        AND ISNULL(PND.bitVisible, 0) = 1
	--AND TNA.numDomainID = @numDomainID
	AND numGroupID = @numGroupID
	--WHERE bitVisible=1  
	ORDER BY numParentID,numPageNavID 
END
--------------------------------------------------------- 
-------------- ADDED BY MANISH ANJARA ON : 12th Oct,2012
ELSE IF @numModuleID = 10 
BEGIN 
	DECLARE @numParentID AS NUMERIC(18,0)
	SELECT TOP 1 @numParentID = numParentID FROM dbo.PageNavigationDTL WHERE numModuleID = 10
	PRINT @numParentID

	SELECT * FROM (
	SELECT PND.numPageNavID,numModuleID,numParentID,vcPageNavName,isnull(vcNavURL,'') as vcNavURL,vcImageURL ,0 sintOrder      
	FROM PageNavigationDTL PND
	JOIN dbo.TreeNavigationAuthorization TNA ON PND.numPageNavID = TNA.numPageNavID  --AND PND.numTabID = TNA.numTabID       
	WHERE  1 = 1
        AND ( ISNULL(TNA.bitVisible, 0) = 1
              --OR ISNULL(numParentID, 0) = 0
              --OR ISNULL(TNA.bitVisible, 0) = 0
            )
        AND ISNULL(PND.bitVisible, 0) = 1
	AND numModuleID = @numModuleID
	AND TNA.numDomainID = @numDomainID
	AND numGroupID = @numGroupID
	
	UNION ALL

	SELECT  numListItemID AS numPageNavID,
		    (SELECT numModuleID FROM ModuleMaster WHERE vcModuleName = 'Opportunities') AS [numModuleID],
			@numParentID AS numParentID,
			ISNULL(vcData,'') + 's' [vcPageNavName],
			'../Opportunity/frmOpenBizDocs.aspx?BizDocName=' + vcData + '&BizDocID=' + CONVERT(VARCHAR(10),LD.numListItemID) AS [vcNavURL],
			'../images/Text.gif' AS [vcImageURL],
			1 AS [bitVisible] 
	FROM dbo.ListDetails LD
	JOIN dbo.TreeNavigationAuthorization TNA ON LD.numListItemID = TNA.numPageNavID  AND ISNULL(bitVisible,0) = 1  
	WHERE numListID = 27 
	AND ISNULL(TNA.[numDomainID], 0) = @numDomainID
	AND ISNULL(TNA.[numGroupID], 0) = @numGroupID) AS [FINAL]
	ORDER BY numParentID,FINAL.numPageNavID 
END
ELSE IF (@numModuleID = 7 OR @numModuleID = 36)
BEGIN
  select    PND.numPageNavID,
            numModuleID,
            numParentID,
            vcPageNavName,
            isnull(vcNavURL, '') as vcNavURL,
            vcImageURL,
            0 sintOrder
  from      PageNavigationDTL PND
  WHERE     1 = 1
            AND ISNULL(PND.bitVisible, 0) = 1
            AND numModuleID = @numModuleID
  order by  numParentID,
            numPageNavID 
END	
ELSE IF @numModuleID = 13
BEGIN
	select PND.numPageNavID,
        PND.numModuleID,
        numParentID,
        vcPageNavName,
        isnull(vcNavURL, '') as vcNavURL,
        vcImageURL,
        ISNULL(PND.intSortOrder,0) sintOrder
	 from   PageNavigationDTL PND
			JOIN dbo.TreeNavigationAuthorization TNA ON PND.numPageNavID = TNA.numPageNavID  --AND PND.numTabID = TNA.numTabID       
	 WHERE  1 = 1
			AND ( ISNULL(TNA.bitVisible, 0) = 1
				  --OR ISNULL(numParentID, 0) = 0
				  --OR ISNULL(TNA.bitVisible, 0) = 0
				)
			AND ISNULL(PND.bitVisible, 0) = 1
			AND PND.numModuleID = @numModuleID --AND bitVisible=1  
			AND numGroupID = @numGroupID
	 order by 
	    sintOrder,
		(CASE WHEN (@numModuleID = 13 AND PND.numPageNavID=79) THEN 2000 ELSE 0 END),
		numParentID,
        numPageNavID   
END
else      
begin            
 select PND.numPageNavID,
        PND.numModuleID,
        numParentID,
        vcPageNavName,
        isnull(vcNavURL, '') as vcNavURL,
        vcImageURL,
        0 sintOrder
 from   PageNavigationDTL PND
        JOIN dbo.TreeNavigationAuthorization TNA ON PND.numPageNavID = TNA.numPageNavID  --AND PND.numTabID = TNA.numTabID       
 WHERE  1 = 1
        AND ( ISNULL(TNA.bitVisible, 0) = 1
              --OR ISNULL(numParentID, 0) = 0
              --OR ISNULL(TNA.bitVisible, 0) = 0
            )
        AND ISNULL(PND.bitVisible, 0) = 1
        AND PND.numModuleID = @numModuleID --AND bitVisible=1  
        AND numGroupID = @numGroupID
 order by numParentID,
		intSortOrder,
        numPageNavID
		    
END