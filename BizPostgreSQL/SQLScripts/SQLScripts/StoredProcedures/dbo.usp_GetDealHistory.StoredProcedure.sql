/****** Object:  StoredProcedure [dbo].[usp_GetDealHistory]    Script Date: 07/26/2008 16:17:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getdealhistory')
DROP PROCEDURE usp_getdealhistory
GO
CREATE PROCEDURE [dbo].[usp_GetDealHistory]        
 @numDomainID numeric,        
 @dtDateFrom datetime,        
 @dtDateTo datetime,        
 @numTerID numeric=0,        
 @numUserCntID numeric=0,        
 @tintRights tinyint=3,        
 @intType numeric=0           
--        
AS        
BEGIN        
 IF @tintRights=3  --ALL Records        
 BEGIN        
   IF @intType = 0        
   BEGIN        
    SELECT top 10 OM.vcPoppName,         
    CI.vcCompanyName + ' - (' + DM.vcDivisionName + ')' As CompanyName,              
    OM.bintAccountClosingDate,         
    (SELECT dbo.GetOppStatus(OM.numOppId)) As Stage,       
    LD.vcData As Reason, OM.monPAmount
    FROM OpportunityMaster OM         
     join  DivisionMaster DM      
     on DM.numDivisionID=OM.numDivisionID       
     join CompanyInfo CI      
     on CI.numCompanyID=DM.numCompanyID       
     left join  ListDetails LD      
     on LD.numListItemId = OM.lngPConclAnalysis        
    WHERE (OM.bintAccountClosingDate BETWEEN @dtDateFrom AND @dtDateTo)      
     AND OM.tintOppStatus=1      
     AND OM.numDomainID = @numDomainID       
   ORDER BY OM.bintAccountClosingDate DESC      
      
   END        
   ELSE        
   BEGIN        
    SELECT top 10 OM.vcPoppName,         
    CI.vcCompanyName + ' - (' + DM.vcDivisionName + ')' As CompanyName,         
    OM.bintAccountClosingDate,         
    (SELECT dbo.GetOppStatus(OM.numOppId)) As Stage,       
    LD.vcData As Reason, OM.monPAmount 
    FROM OpportunityMaster OM      
     join  DivisionMaster DM      
     on DM.numDivisionID=OM.numDivisionID       
     join CompanyInfo CI      
     on CI.numCompanyID=DM.numCompanyID       
     JOIN AdditionalContactsInformation ACI      
     ON ACI.numCreatedBy = OM.numCreatedBy        
     left join  ListDetails LD      
     on LD.numListItemId = OM.lngPConclAnalysis        
    WHERE (OM.bintAccountClosingDate BETWEEN @dtDateFrom AND @dtDateTo)      
     AND OM.tintOppStatus=1      
     AND OM.numDomainID = @numDomainID       
     AND ACI.numTeam is not null           
     AND ACI.numTeam in(select F.numTeam from ForReportsByTeam F           
     where F.numuserCntId=@numUserCntID and F.numDomainid=@numDomainId and F.tintType=@intType)          
   ORDER BY OM.bintAccountClosingDate DESC      
      
       
   END        
 END        
         
 IF @tintRights=2 --ALL Records related to teritory        
 BEGIN        
   IF @intType = 0        
   BEGIN        
    SELECT top 10 OM.vcPoppName,         
    CI.vcCompanyName + ' - (' + DM.vcDivisionName + ')' As CompanyName,         
    OM.bintAccountClosingDate,         
    (SELECT dbo.GetOppStatus(OM.numOppId)) As Stage,       
    LD.vcData As Reason, OM.monPAmount
    FROM OpportunityMaster OM      
         
     join  DivisionMaster DM      
     on DM.numDivisionID=OM.numDivisionID       
     join CompanyInfo CI      
     on CI.numCompanyID=DM.numCompanyID       
     left join  ListDetails LD      
     on LD.numListItemId = OM.lngPConclAnalysis        
    WHERE (OM.bintAccountClosingDate BETWEEN @dtDateFrom AND @dtDateTo)      
     AND OM.tintOppStatus=1      
     AND OM.numDomainID = @numDomainID       
     AND DM.numTerID in (SELECT numTerritoryID FROM UserTerritory WHERE numUserCntID=@numUserCntID)        
   ORDER BY OM.bintAccountClosingDate DESC      
      
   END        
   ELSE        
   BEGIN        
    SELECT top 10 OM.vcPoppName,         
    CI.vcCompanyName + ' - (' + DM.vcDivisionName + ')' As CompanyName,         
    OM.bintAccountClosingDate,         
    (SELECT dbo.GetOppStatus(OM.numOppId)) As Stage,       
    LD.vcData As Reason, OM.monPAmount
    FROM OpportunityMaster OM        
     join  DivisionMaster DM      
     on DM.numDivisionID=OM.numDivisionID       
     join CompanyInfo CI      
     on CI.numCompanyID=DM.numCompanyID       
     JOIN AdditionalContactsInformation ACI      
     ON ACI.numCreatedBy = OM.numCreatedBy        
     left join  ListDetails LD      
     on LD.numListItemId = OM.lngPConclAnalysis        
    WHERE (OM.bintAccountClosingDate BETWEEN @dtDateFrom AND @dtDateTo)      
     AND OM.tintOppStatus=1      
     AND OM.numDomainID = @numDomainID       
     AND ACI.numTeam is not null           
     AND ACI.numTeam in(select F.numTeam from ForReportsByTeam F           
     where F.numuserCntId=@numUserCntID and F.numDomainid=@numDomainId and F.tintType=@intType)          
     AND DM.numTerID in (SELECT numTerritoryID FROM UserTerritory WHERE numUserCntID=@numUserCntID)        
   ORDER BY OM.bintAccountClosingDate DESC      
        
   END        
             
 END        
         
 IF @tintRights=1 --Owner Records        
 BEGIN        
   IF @intType = 0        
   BEGIN        
    SELECT top 10 OM.vcPoppName,         
    CI.vcCompanyName + ' - (' + DM.vcDivisionName + ')' As CompanyName,           
    OM.bintAccountClosingDate,         
    (SELECT dbo.GetOppStatus(OM.numOppId)) As Stage,       
    LD.vcData As Reason, OM.monPAmount ,dbo.fn_getContactName(OM.numRecOwner) as vcUserDesc
    FROM OpportunityMaster OM         
     join  DivisionMaster DM      
     on DM.numDivisionID=OM.numDivisionID       
     join CompanyInfo CI      
     on CI.numCompanyID=DM.numCompanyID       
     left join  ListDetails LD      
     on LD.numListItemId = OM.lngPConclAnalysis        
    WHERE (OM.bintAccountClosingDate BETWEEN @dtDateFrom AND @dtDateTo)      
     AND OM.tintOppStatus=1      
     AND OM.numDomainID = @numDomainID       
     AND OM.numRecOwner=@numUserCntID        
   ORDER BY OM.bintAccountClosingDate DESC      
      
   END        
   ELSE        
   BEGIN       
    SELECT top 10 OM.vcPoppName,         
    CI.vcCompanyName + ' - (' + DM.vcDivisionName + ')' As CompanyName,            
    OM.bintAccountClosingDate,         
    (SELECT dbo.GetOppStatus(OM.numOppId)) As Stage,       
    LD.vcData As Reason, OM.monPAmount,dbo.fn_getContactName(OM.numRecOwner)as vcUserDesc
    FROM OpportunityMaster OM         
     join  DivisionMaster DM      
     on DM.numDivisionID=OM.numDivisionID       
     join CompanyInfo CI      
     on CI.numCompanyID=DM.numCompanyID       
     JOIN AdditionalContactsInformation ACI      
     ON ACI.numCreatedBy = OM.numCreatedBy        
     left join  ListDetails LD      
     on LD.numListItemId = OM.lngPConclAnalysis        
    WHERE (OM.bintAccountClosingDate BETWEEN @dtDateFrom AND @dtDateTo)      
     AND OM.tintOppStatus=1      
     AND OM.numDomainID = @numDomainID       
      AND OM.numRecOwner=@numUserCntID        
     AND ACI.numTeam is not null           
     AND ACI.numTeam in(select F.numTeam from ForReportsByTeam F           
     where F.numUserCntID=@numUserCntID and F.numDomainid=@numDomainId and F.tintType=@intType)          
   ORDER BY OM.bintAccountClosingDate DESC      
      
        
   END        
 END        
         
END
GO
