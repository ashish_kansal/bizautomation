/****** Object:  StoredProcedure [dbo].[USP_GetRootNode]    Script Date: 07/26/2008 16:18:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created By Siva     
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getrootnode')
DROP PROCEDURE usp_getrootnode
GO
CREATE PROCEDURE [dbo].[USP_GetRootNode]            
@numDomainId as numeric(9)        
 As    
Begin    
    Select numAccountId From Chart_Of_Accounts Where numParntAcntTypeId is null and numAcntTypeID is null and numDomainId = @numDomainId     
End
GO
