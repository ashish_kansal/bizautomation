/****** Object:  StoredProcedure [dbo].[USP_GetLeadListReport]    Script Date: 07/26/2008 16:17:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--- Created By Gangadhar                                            
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getleadlistreport')
DROP PROCEDURE usp_getleadlistreport
GO
CREATE PROCEDURE [dbo].[USP_GetLeadListReport]                                                     
 @numGrpID numeric,                                                          
 @numUserCntID numeric,                                                          
 @tintUserRightType tinyint,                                                                                                   
 @numFollowUpStatus as numeric(9)=0 ,                                                        
 @TotRecs int output,                                                        
 @columnName as Varchar(50)='',                                                        
 @columnSortOrder as Varchar(10)='',                                
 @ListType as tinyint=0,                                
 @TeamType as integer=0,   
 @TeamMemID as numeric(9)=0,                            
 @numDomainID as numeric(9)=0,          
 @dtFromDate as Datetime,      
 @dtToDate as Datetime      
                                                    
                                                          
AS                                                          
declare @strSql as varchar(7100)                                                                         
                                          
 set @strSql= 'SELECT                                                                             
   DM.bintCreatedDate,                                                          
   cmp.numCompanyID,                                                          
   cmp.vcCompanyName,                                                          
   DM.vcDivisionName,                                                          
   DM.numDivisionID,                                                          
   ADC.numContactId,                                          
   dbo.fn_GetListItemName(CMP.numNoOfEmployeesId) as Employees,                                                                                     
   DM.numRecOwner,                                                          
   G.vcGrpName,                                                    
   DM.numTerId,                                                         
   lst.vcdata as FolStatus,                             
   ADC1.vcFirstName+'' ''+ADC1.vcLastName as vcUserName,                      
   dbo.fn_GetContactName(numAssignedTo) as AssignedTo                                                                   
  FROM                                                            
  CompanyInfo cmp                                                           
   join divisionmaster DM                                    
   on cmp.numCompanyID=DM.numCompanyID                                    
   left join ListDetails lst                                     
   on lst.numListItemID=DM.numFollowUpStatus                                                 
   join AdditionalContactsInformation ADC                                    
   on ADC.numDivisionID=DM.numDivisionID                            
   join AdditionalContactsInformation ADC1     
   on ADC1.numContactID=DM.numRecOwner     
   join Groups G    
   on G.numGrpID= DM.numGrpID                                                                  
  WHERE                                                           
   DM.tintCRMType=0  and ISNULL(ADC.bitPrimaryContact,0)=1                                                                                                  
 AND DM.numDomainID='+convert(varchar(15),@numDomainID)+ '    
and DM.bintCreatedDate >= '''+convert(varchar(20),@dtFromDate)+'''    
and DM.bintCreatedDate <= '''+convert(varchar(20),@dtToDate)+'''    
'    
    
 if @numGrpID<>0 set @strSql=@strSql+' and DM.numGrpId  like '''+convert(varchar(15),@numGrpID)+'%'''                          
                                                               

if @numFollowUpStatus<>0 set @strSql=@strSql + '  AND DM.numFollowUpStatus='+convert(varchar(15),@numFollowUpStatus)                                                         
                                 
    
if @tintUserRightType=1 set @strSql=@strSql + ' AND (DM.numRecOwner = '+convert(varchar(15),@numUserCntID)+ ')'    
if @tintUserRightType=2 set  @strSql=@strSql + ' and ADC1.numTeam in(select F.numTeam from ForReportsByTeam F     
where F.numuserCntid='+convert(varchar(15),@numUserCntID)+' and F.numDomainid='+convert(varchar(15),@numDomainId)+' and F.tintType='+convert(varchar(5),@TeamType)+')'                
else if @tintUserRightType=3 set @strSql=@strSql + ' AND (DM.numTerID in (select numTerritoryID from  UserTerritory where numUserCntID= '+convert(varchar(15),@numUserCntID)+' ) or DM.numTerID=0) '                                        
      
if @ListType=0                                 
begin                                
if @numGrpID=1 set @strSql=@strSql + ' AND (DM.numRecOwner = '+convert(varchar(15),@numUserCntID)+ ' or DM.numAssignedTo='+convert(varchar(15),@numUserCntID)+ ')'                                             
if @numGrpID=5 set @strSql=@strSql + ' AND (DM.numRecOwner = '+convert(varchar(15),@numUserCntID)+ ' or DM.numAssignedTo='+convert(varchar(15),@numUserCntID)+ ')'                                  
end                      
                  
else if @ListType=1                                 
begin                               
if @TeamMemID=0                               
 begin                              
 if @numGrpID<>2 set @strSql=@strSql + '  AND (DM.numRecOwner  in(select distinct numUserDetailId from UserMaster join AdditionalContactsInformation on numContactID=numUserDetailId                 
where numTeam in(select numTeam from ForReportsByTeam where numUserCntID='+convert(varchar(15),@numUserCntID)+' and tintType='+convert(varchar(15),@TeamType)+') ) or DM.numAssignedTo in(select distinct numUserDetailId from UserMaster join AdditionalContactsInformation on numContactID=numUserDetailId                 
where numTeam in(select numTeam from ForReportsByTeam where numUserCntID='+convert(varchar(15),@numUserCntID)+' and tintType='+convert(varchar(15),@TeamType)+')))'                                                 
                               
 end                              
else                           
 begin                              
 if @numGrpID<>2 set @strSql=@strSql + '  AND DM.numRecOwner='+convert(varchar(15),@TeamMemID) +'  or  DM.numAssignedTo='+convert(varchar(15),@TeamMemID)                                     
                               
 end                               
end                                        
                
                
print @strSql                                        
                                       
exec(@strSql)
GO
