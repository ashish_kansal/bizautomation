/****** Object:  StoredProcedure [dbo].[USP_SimpleSearchfields]    Script Date: 07/26/2008 16:21:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--exec USP_SimpleSearchfields 1,1      
--created by anoop jayaraj      
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_simplesearchfields')
DROP PROCEDURE usp_simplesearchfields
GO
CREATE PROCEDURE [dbo].[USP_SimpleSearchfields]      
@numGroupID as numeric(9)=0,      
@numDomainID as numeric(9)=0      
as      
      
     
      
 select convert(varchar(15),numFieldID)+'~'+vcDbColumnName+'~'+vcAssociatedControlType
 +'~'+vcFieldType+'~'+convert(varchar(15),numListID)+'~'+vcListItemType as ID,vcFieldName ,tintRow as intRownum     
 from View_DynamicColumns     
 where numFormID=6  and numAuthGroupID=@numGroupID and isnull(bitCustom,0)=0 and numDomainID=@numDomainID 
 
 union      
 
   select  convert(varchar(15),numFieldID)+'~'+ vcFieldName+'~'+vcAssociatedControlType
   +'~'+ vcFieldType +'~'+convert(varchar(15),ISNULL(numListID,0))+'~L' as ID,vcFieldName,tintRow as intRownum       -- 'C' to vcFieldType changed by chitnan to fix GIS bug in simple search
from View_DynamicCustomColumns
 where numFormId=6  AND numAuthGroupID=@numGroupID AND isnull(bitCustom,0)=1
 AND numDomainID=@numDomainID order by intRownum 
 
 
GO
