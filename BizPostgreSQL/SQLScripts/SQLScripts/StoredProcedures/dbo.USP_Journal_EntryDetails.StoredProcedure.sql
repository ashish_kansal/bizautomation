/****** Object:  StoredProcedure [dbo].[USP_Journal_EntryDetails]    Script Date: 07/26/2008 16:19:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created By Siva      
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'usp_journal_entrydetails' ) 
    DROP PROCEDURE usp_journal_entrydetails
GO
CREATE PROCEDURE [dbo].[USP_Journal_EntryDetails]
    @numDomainId AS NUMERIC(9) = 0,
    @numJournalId AS NUMERIC(9) = 0
AS 
    BEGIN                
        SELECT  GJD.numTransactionId AS TransactionId,
                GJD.numChartAcntId AS numChartAcntId,
                CA.[numParntAcntTypeId] AS numAcntType,
                GJD.numDebitAmt AS numDebitAmt,
                GJD.numCreditAmt AS numCreditAmt,
                GJD.varDescription AS varDescription,
                GJD.numCustomerId AS numCustomerId,
                GJD.numJournalId,
                dbo.FormatedDateFromDate(GJH.datEntry_Date,GJH.numDomainId) datEntry_Date,
				GJH.datEntry_Date AS datEntryDate,
                dbo.fn_GetRelationship_DivisionId(CI.numCompanyType,
                                                  CI.numDomainID) + ' - '
                + CONVERT(VARCHAR(400), CI.numCompanyId) AS varRelation,
                GJD.numCampaignID AS numCampaignID,ISNULL(GJD.numProjectId,0) AS numProjectId, ISNULL(GJD.numClassID,0) AS numClassId,
                ISNULL(GJD.bitReconcile,0) AS bitReconcile,ISNULL(GJD.bitCleared,0) AS bitCleared,ISNULL(GJD.numReconcileID,0) AS numReconcileID,
                GJH.numJournalReferenceNo,
                ISNULL(GJD.chBizDocItems,'') chBizDocItems,
                GJH.varDescription AS vcNarration,
                ISNULL(GJH.numOppId,0) AS numOppId,ISNULL(GJH.numOppBizDocsId,0) AS numOppBizDocsId,ISNULL(GJH.numDepositId,0) AS numDepositId,
                ISNULL(GJH.numReturnID,0) AS numReturnID,ISNULL(GJH.numCheckHeaderID,0) AS numCheckHeaderID,ISNULL(GJH.numCategoryHDRID,0) AS numCategoryHDRID,
                ISNULL(GJH.numBillID,0) AS numBillID,ISNULL(GJH.numBillPaymentID,0) AS numBillPaymentID,ISNULL(GJH.numPayrollDetailID,0) AS numPayrollDetailID
        FROM    General_Journal_Header GJH
                INNER JOIN General_Journal_Details GJD ON GJH.numJournal_Id = GJD.numJournalId
                INNER JOIN Chart_of_Accounts CA ON GJD.numChartAcntId = CA.numAccountId
                LEFT OUTER JOIN Companyinfo CI ON GJD.numCustomerId = CI.numCompanyId
        WHERE   GJH.numDomainId = @numDomainId
                AND GJH.numJournal_Id = @numJournalId              
    END
GO