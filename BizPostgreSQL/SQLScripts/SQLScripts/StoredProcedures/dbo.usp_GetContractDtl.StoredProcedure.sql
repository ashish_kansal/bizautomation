/****** Object:  StoredProcedure [dbo].[usp_GetContractDtl]    Script Date: 07/26/2008 16:16:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getcontractdtl')
DROP PROCEDURE usp_getcontractdtl
GO
CREATE PROCEDURE [dbo].[usp_GetContractDtl]                                                                                                       
@numContractID  bigint,                                                  
@numUserCntID numeric(9)=0,                                                                                                        
 @numDomainID numeric(9)=0                                                                                                    
as                                                                                                        
                                                                                                      
 DECLARE @vcRetValue varchar (250)                                                   
select      
[dbo].[fn_GetComapnyName](numDivisionID) as       CompanyName,                                           
c.numContractId,                                                  
numDivisionId,                                                  
vcContractName,                                 
--case when bitdays=1 then                                                 
--datediff(day,bintStartDate,bintExpDate)                                 
--else                                
--0                                
--end as Days,                                                  
bintStartDate,    
dbo.FormatedDateFromDate(bintStartDate,numDOmainID) AS ExpDate,
bintExpDate,                                                  
numIncidents,                                                  
c.numhours,                                                  
vcNotes,                                                  
numAmount,                                                  
bintCreatedOn,        
case when bithour = 1 then                                                          
dbo.getContractRemainingHrsAmt(0,@numContractID)      
else 0 end as RemHours,                                     
case when bitamount =1 then      
dbo.getContractRemainingHrsAmt(1,@numContractID)       
else 0 end as RemAmount ,                                               
case when bitdays =1 then                                
isnull(convert(varchar(100),            
case when 
 datediff(day,getutcdate(),bintExpdate) > 0 then datediff(day,getutcdate(),bintExpdate) else '0' end ),'0')                              
                              
else '0' end as Days,                                                       
      
case when bitincidents =1 then                                
convert(varchar(100),convert(decimal(10,2),numincidents) -            
 convert(decimal(10,2),dbo.GetIncidents(@numContractID,@numDomainId ,c.numdivisionid )))                              
else '0' end as Incidents,                              
bitamount,                                  
bitHour,                                  
bitdays,                                  
bitincidents ,                
bitEHour,                                  
bitEdays,                                  
bitEincidents  ,                
numEmailDays ,                    
numEmailIncidents  ,                    
numEmailHours,                
numTemplateId ,decrate,        
(Select isnull(sum(monAmount),0) From OpportunityBizDocsDetails Where numDomainId=c.numDomainId And numContractId= c.numcontractId) As BalanceAmt        
from ContractManagement c                                                                    
                                                  
where c.numDomainId =@numDomainId and c.numcontractId = @numContractID
GO
