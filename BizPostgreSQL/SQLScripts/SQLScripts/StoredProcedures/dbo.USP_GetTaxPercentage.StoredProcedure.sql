/****** Object:  StoredProcedure [dbo].[USP_GetTaxPercentage]    Script Date: 07/26/2008 16:18:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
---created by anoop jayaraj

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_gettaxpercentage')
DROP PROCEDURE usp_gettaxpercentage
GO
CREATE PROCEDURE [dbo].[USP_GetTaxPercentage]
@DivisionID as numeric(9)=0,
@numBillCountry as numeric(9)=0,
@numBillState  as numeric(9)=0,
@numDomainID as numeric(9),
@numTaxItemID as numeric(9),
@tintBaseTaxCalcOn TINYINT=2, -- Base tax calculation on Shipping Address(2) or Billing Address(1)
@tintBaseTaxOnArea TINYINT=0,
@vcCity varchar(100),
@vcZipPostal varchar(20)
as

  
DECLARE @TaxPercentage as float 
DECLARE @tintTaxType AS TINYINT

declare @bitTaxApplicable as bit 
set @TaxPercentage=0 
 
if @DivisionID>0 --Existing Customer
BEGIN
	IF @tintBaseTaxCalcOn = 1 --Billing Address
			select @numBillState=isnull(numState,0), @numBillCountry=isnull(numCountry,0),@vcCity=isnull(vcCity,''),@vcZipPostal=isnull(vcPostalCode,'')  from DivisionMaster DM LEFT JOIN dbo.AddressDetails AD1 ON AD1.numDomainID=DM.numDomainID 
			AND AD1.numRecordID=DM.numDivisionID AND AD1.tintAddressOf=2 AND AD1.tintAddressType=1 where numDivisionID=@DivisionID
	ELSE --Shipping Address
			select @numBillState=isnull([numState],0), @numBillCountry=isnull([numCountry],0),@vcCity=isnull(vcCity,''),@vcZipPostal=isnull(vcPostalCode,'')  from DivisionMaster DM  LEFT JOIN dbo.AddressDetails AD2 ON AD2.numDomainID=DM.numDomainID 
			AND AD2.numRecordID= DM.numDivisionID AND AD2.tintAddressOf=2 AND AD2.tintAddressType=2 AND AD2.bitIsPrimary=1 where numDivisionID=@DivisionID
      
	IF @numTaxItemID=0 AND NOT EXISTS(SELECT numTaxItemID FROM DivisionTaxTypes WHERE numDivisionID=  @DivisionID and numTaxItemID=@numTaxItemID)         
	BEGIN
		SELECT @bitTaxApplicable=(CASE WHEN ISNULL(bitNoTax,0)=1 THEN 0 ELSE 1 END) FROM DivisionMaster WHERE numDivisionID=@DivisionID
	END
	ELSE
	BEGIN
		SELECT @bitTaxApplicable=bitApplicable FROM DivisionTaxTypes WHERE numDivisionID=  @DivisionID and numTaxItemID=@numTaxItemID
	END
end 
else --New Customer
begin
	SET @bitTaxApplicable=1
END


SELECT @tintBaseTaxOnArea=tintBaseTaxOnArea FROM [Domain] WHERE [numDomainId] = @numDomainID

IF EXISTS (SELECT tintBaseTaxOnArea FROM TaxCountryConfi WHERE numDomainID=@numDomainID AND numCountry=@numBillCountry)
BEGIN
	--TAKE FROM TAXCOUNTRYCONFI IF NOT THEN USE DEFAULT FROM DOMAIN
	SELECT @tintBaseTaxOnArea=tintBaseTaxOnArea FROM TaxCountryConfi WHERE numDomainID=@numDomainID and numCountry=@numBillCountry 
END

if  @bitTaxApplicable=1 --If Tax Applicable
	begin
		if @numBillCountry >0 and @numBillState>0            
		begin            
			if @numBillState>0            
			begin            
				if exists(select * from TaxDetails where numCountryID=@numBillCountry and numStateID=@numBillState and 
						(1=(Case 
								when @tintBaseTaxOnArea=0 then 1 --State
								when @tintBaseTaxOnArea=1 then Case When vcCity like '%' + @vcCity + '%' then 1 else 0 end --City
								when @tintBaseTaxOnArea=2 then Case When vcZipPostal=@vcZipPostal then 1 else 0 end --Zip/Postal
								else 0 end)) and numDomainID= @numDomainID and numTaxItemID=@numTaxItemID)            
						BEGIN
							select top 1 @TaxPercentage=decTaxPercentage, @tintTaxType=tintTaxType from TaxDetails where numCountryID=@numBillCountry and numStateID=@numBillState and 
								(1=(Case 
								when @tintBaseTaxOnArea=0 then 1 --State
								when @tintBaseTaxOnArea=1 then Case When vcCity like '%' + @vcCity +'%' then 1 else 0 end --City
								when @tintBaseTaxOnArea=2 then Case When vcZipPostal=@vcZipPostal then 1 else 0 end --Zip/Postal
								else 0 end)) and numDomainID= @numDomainID and numTaxItemID=@numTaxItemID           
						END
				else if exists(select * from TaxDetails where numCountryID=@numBillCountry and numStateID=@numBillState 
							and numDomainID= @numDomainID and numTaxItemID=@numTaxItemID and isnull(vcCity,'')='' and isnull(vcZipPostal,'')='')            
						BEGIN
							select top 1 @TaxPercentage=decTaxPercentage, @tintTaxType=tintTaxType from TaxDetails where numCountryID=@numBillCountry and numStateID=@numBillState
							 and numDomainID= @numDomainID and numTaxItemID=@numTaxItemID  and isnull(vcCity,'')='' and isnull(vcZipPostal,'')=''         
						END
				 else             
					select top 1 @TaxPercentage=decTaxPercentage, @tintTaxType=tintTaxType from TaxDetails where numCountryID=@numBillCountry and numStateID=0 and isnull(vcCity,'')='' and isnull(vcZipPostal,'')='' and numDomainID= @numDomainID   and numTaxItemID=@numTaxItemID          
		   end                
		end            
		else if @numBillCountry >0            
			 select top 1 @TaxPercentage=decTaxPercentage, @tintTaxType=tintTaxType from TaxDetails where numCountryID=@numBillCountry and numStateID=0 and isnull(vcCity,'')='' and isnull(vcZipPostal,'')='' and numDomainID= @numDomainID  and numTaxItemID=@numTaxItemID
	end

select CONCAT(ISNULL(@TaxPercentage,0),'#',ISNULL(@tintTaxType,1))
GO
