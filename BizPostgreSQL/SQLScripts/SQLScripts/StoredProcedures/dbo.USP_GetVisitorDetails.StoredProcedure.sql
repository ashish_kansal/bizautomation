/****** Object:  StoredProcedure [dbo].[USP_GetVisitorDetails]    Script Date: 07/26/2008 16:18:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj          
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getvisitordetails')
DROP PROCEDURE usp_getvisitordetails
GO
CREATE PROCEDURE [dbo].[USP_GetVisitorDetails]          
@numTrackingID as numeric(9)=0          
as          
select           
vcUserHostAddress,          
vcUserDomain,          
vcUserAgent,          
vcBrowser,          
vcCrawler, 
replace(replace(replace(replace(isnull(vcURL,'-'),'%3A',':'),'%3F','?'),'%3D','='),'%26','&')           
as vcURL,    
replace(replace(replace(replace(isnull(vcReferrer,'-'),'%3A',':'),'%3F','?'),'%3D','='),'%26','&')         
 as vcReferrer,          
isnull(vcSearchTerm,'-') as vcSearchTerm,          
numVisits,  
replace(replace(replace(replace(isnull(vcOrginalURL,'-'),'%3A',':'),'%3F','?'),'%3D','='),'%26','&')        
 as vcOrginalURL,          
replace(replace(replace(replace(isnull(vcOrginalRef,'-'),'%3A',':'),'%3F','?'),'%3D','='),'%26','&')        
as vcOrginalRef,          
numVistedPages,          
convert(varchar(8),vcTotalTime) as vcTotalTime,          
dtCreated,          
replace(replace(replace(replace(isnull(vcPageName,'-'),'%3A',':'),'%3F','?'),'%3D','='),'%26','&')  as   vcPageName,       
vcElapsedTime,      
dtCreated,    
dtTime          
from TrackingVisitorsHDR          
join TrackingVisitorsDTL          
on numTrackingID=numTracVisitorsHDRID        
--left join PageMasterWebAnlys P        
--on P.numPageID=TrackingVisitorsDTL.numPageID        
where numTrackingID=@numTrackingID
GO
