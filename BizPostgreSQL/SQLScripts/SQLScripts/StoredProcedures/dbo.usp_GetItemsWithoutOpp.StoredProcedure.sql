/****** Object:  StoredProcedure [dbo].[usp_GetItemsWithoutOpp]    Script Date: 07/26/2008 16:17:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getitemswithoutopp')
DROP PROCEDURE usp_getitemswithoutopp
GO
CREATE PROCEDURE [dbo].[usp_GetItemsWithoutOpp]
	@numoppid numeric,
	@numDomainId numeric  
-- 
AS
	IF @numoppid > 0
	
		SELECT a.numoppid,b.vcItemName,b.txtItemDesc,b.numitemcode,b.charitemType FROM opportunityitems a INNER JOIN Item b ON a.numitemcode<>b.numitemcode
		WHERE a.numoppid=@numOppId and b.numDomainid=@numdomainid order by b.vcItemName
	ELSE
		SELECT * from item where numdomainid=@numdomainid order by vcItemName
GO
