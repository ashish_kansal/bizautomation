/****** Object:  StoredProcedure [dbo].[USP_ManagePortalBizDocs]    Script Date: 07/26/2008 16:19:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_manageportalbizdocs')
DROP PROCEDURE usp_manageportalbizdocs
GO
CREATE PROCEDURE  [dbo].[USP_ManagePortalBizDocs]      
@strFiledId as varchar(4000)='',
@numDomainID as numeric(9)     
as      
begin      
	delete from PortalBizDocs   WHERE numDomainID=@numDomainID
     insert into PortalBizDocs(numBizDocID,numDomainID)      
	 SELECT Id,@numDomainID FROM dbo.SplitIDs(@strFiledId,',')
       
end
GO
