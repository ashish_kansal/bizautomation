/****** Object:  StoredProcedure [dbo].[USP_EditJournalEntryHeader]    Script Date: 07/26/2008 16:15:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created By Siva                
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_editjournalentryheader')
DROP PROCEDURE usp_editjournalentryheader
GO
CREATE PROCEDURE [dbo].[USP_EditJournalEntryHeader]                            
@numJournal_Id as numeric(9)=0,                      
@numRecurringId as numeric(9)=0,            
@datEntry_Date as datetime,            
@numAmount as DECIMAL(20,5),        
@AccountId as numeric(9)=0,  
@numDomainId as numeric(9)=0,
@vcDescription AS VARCHAR(1000),
@numClassID NUMERIC=null
               
As                             
Begin     
 if @numRecurringId=0 Set @numRecurringId=null      
Declare @bitOpeningBalance as bit        
Select @bitOpeningBalance=isnull(bitOpeningBalance,0) From  General_Journal_Header Where numJournal_Id=@numJournal_Id  And numDomainId=@numDomainId              
  Update General_Journal_Header Set datEntry_Date=@datEntry_Date, numAmount=@numAmount,numRecurringId=@numRecurringId,varDescription=@vcDescription ,numClassID = @numClassID Where numJournal_Id=@numJournal_Id And numDomainId=@numDomainId
if @bitOpeningBalance=1        
 Begin        
 print @bitOpeningBalance        
 print 'SP'        
 print @numAmount        
       
-- Update Chart_of_Accounts Set numOriginalOpeningBal=@numAmount Where numAccountId=@AccountId   And numDomainId=@numDomainId          
 End           
End
GO
