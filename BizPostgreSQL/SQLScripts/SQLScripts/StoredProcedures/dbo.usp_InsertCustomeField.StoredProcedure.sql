/****** Object:  StoredProcedure [dbo].[usp_InsertCustomeField]    Script Date: 07/26/2008 16:19:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj  
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_insertcustomefield')
DROP PROCEDURE usp_insertcustomefield
GO
CREATE PROCEDURE [dbo].[usp_InsertCustomeField]    
 @numFldID  numeric(9)=0,    
 @vcFldValue varchar(100),    
 @numRecId  numeric(9)=0    
     
    
AS    
  
  
declare @PageID as tinyint  
    
    
select @PageID=Grp_id from CFW_Fld_Master where Fld_id=@numFldID  
  
  if @PageID=1  OR @PageID=12 OR @PageID=13 OR @PageID=14
  BEGIN
  		 DELETE FROM dbo.CFW_FLD_Values WHERE RecID=@numRecId AND Fld_ID=@numFldID
		 INSERT INTO CFW_FLD_Values(Fld_ID,Fld_Value,RecId)    
		 VALUES(@numFldID,@vcFldValue,@numRecId)    
  END
	
  
 if @PageID=4  
 BEGIN
	DELETE FROM dbo.CFW_FLD_Values_Cont WHERE  RecID=@numRecId AND Fld_ID=@numFldID
	INSERT INTO CFW_FLD_Values_Cont(Fld_ID,Fld_Value,RecId)    
	VALUES(@numFldID,@vcFldValue,@numRecId)  	
 END
 


 if @PageID=5
 BEGIN
	 DELETE FROM dbo.CFW_FLD_Values_Item WHERE  RecID=@numRecId AND Fld_ID=@numFldID
	 INSERT INTO CFW_FLD_Values_Item(Fld_ID,Fld_Value,RecId)    
	 VALUES(@numFldID,@vcFldValue,@numRecId)
 END 
GO
