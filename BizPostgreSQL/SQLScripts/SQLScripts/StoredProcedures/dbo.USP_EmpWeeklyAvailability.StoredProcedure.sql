/****** Object:  StoredProcedure [dbo].[USP_EmpWeeklyAvailability]    Script Date: 07/26/2008 16:15:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj                   
--exec USP_EmpAvailability 1,20060412                         
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_empweeklyavailability')
DROP PROCEDURE usp_empweeklyavailability
GO
CREATE PROCEDURE [USP_EmpWeeklyAvailability]                              
@numUserCntId as numeric(9),                  
@dtDate as datetime,               
@ClientTimeZoneOffset int,      
@TeamType as int,      
@numDomainID as numeric(9)=0,
@Duration AS BIGINT = 0                                       
as                              
--Create a Temporary table to hold data                                        
Create table #tempTable (                                                  
 numContactID numeric(9),  
 [Name] varchar(100)                                  
 )     
   
declare @dtWeekStartDate as DATETIME, @dtWeekEndDate as datetime    
declare @dtDurationEnd as datetime  

SELECT @dtWeekStartDate=CONVERT(VARCHAR(10),DATEADD(ww, DATEDIFF(ww,0,@dtDate), 0),111) + ' 00:00:00',
		@dtWeekEndDate=CONVERT(VARCHAR(10),DATEADD(ww, DATEDIFF(ww,0,@dtDate), 6),111) + ' 23:59:59'

PRINT @dtWeekStartDate
PRINT @dtWeekEndDate

IF ISNULL(@Duration,0) = 0
		SET @dtDurationEnd = @dtDate		
ELSE
		SET @dtDurationEnd = DATEADD(MINUTE,@Duration,@dtDate)		

insert into    #tempTable                                
SELECT numContactID,[Name] FROM 
(select distinct ACI.numContactID,ACI.vcFirstName + ' '+ ACI.vcLastname as [Name]          
from  dbo.AdditionalContactsInformation ACI 
join UserMaster UM on ACI.numContactID=UM.numUserDetailID
JOIN [UserTeams] UT ON UT.[numUserCntID] = UM.[numUserDetailId] 
where bitActivateFlag=1 and ACI.numDomainID= @numDomainID AND UM.numDomainID= @numDomainID  AND UT.numDomainID= @numDomainID
and UT.numTeam in (SELECT numTeam FROM ForReportsByTeam WHERE numDomainID = @numDomainID AND numUserCntID = @numUserCntID AND tintType=@TeamType) 
) TABLE1
WHERE (numContactId NOT IN (
	SELECT numAssign FROM dbo.Communication
    WHERE numDomainID=@numDomainID
    AND ((DATEADD(second,1,DateAdd(minute,-@ClientTimeZoneOffset, dtStartTime)) BETWEEN @dtDate AND @dtDurationEnd)
	OR (DATEADD(second,-1,DateAdd(minute,-@ClientTimeZoneOffset, dtEndTime)) BETWEEN @dtDate AND @dtDurationEnd))
	UNION 
	SELECT numUserCntId from activity A join ActivityResource AR on AR.ActivityID=A.ActivityID 
		join Resource R on R.ResourceID=AR.ResourceID  
	where (DATEADD(second,1,DateAdd(minute,-@ClientTimeZoneOffset, StartDateTimeUtc))>@dtDate 
		and  DATEADD(second,1,DateAdd(minute,-@ClientTimeZoneOffset, StartDateTimeUtc))<@dtDurationEnd)
		or (DATEADD(second,-1,DateAdd(minute, -@ClientTimeZoneOffset, dateadd(second,duration,startdatetimeutc))) >@dtDate
		AND DATEADD(second,-1,DateAdd(minute, -@ClientTimeZoneOffset, dateadd(second,duration,startdatetimeutc))) <@dtDurationEnd)
) OR ISNULL(@Duration,0) = 0)
  
select * from #tempTable  
  
SET @dtDate=CONVERT(VARCHAR(10),@dtDate,111) + ' 00:00:00' --will return date like 'XXXX-XX-XX 00:00:00'
  
SELECT [numCommId],CAST(0 AS INT) AS ActivityID,dtStartTime,dtEndTime,numContactID,vcdata,right(convert(varchar, dtStartTime), 8) +'-'+right(convert(varchar, dtEndTime), 8) as Schedule,[bitTask],ActivityDescription from (select [numCommId],DateAdd(minute,-@ClientTimeZoneOffset, dtStartTime) as dtStartTime,
DateAdd(minute, -@ClientTimeZoneOffset, CASE WHEN [bitTask]=974 THEN dtStartTime ELSE dtEndTime END ) as dtEndTime,T.numContactID,vcdata,[bitTask],textDetails as ActivityDescription from Communication     
left join ListDetails L  
on L.numListItemID=bitTask  
join #tempTable T  
on T.numContactID=numAssign  
where  ((dtStartTime between @dtWeekStartDate and @dtWeekEndDate) or   
(dtEndTime between @dtWeekStartDate and @dtWeekEndDate)
OR ( bitTask =974 AND DateAdd(minute,-@ClientTimeZoneOffset, dtStartTime) BETWEEN @dtWeekStartDate AND @dtWeekEndDate) ) and bitClosedFlag=0)X   --974 -- Follow-up Anytime
 
 
SELECT CAST(0 AS NUMERIC(18)) AS numCommId,ActivityID,dtStartTime,dtEndTime,numContactID,vcdata,right(convert(varchar, dtStartTime), 8) +'-'+right(convert(varchar, dtEndTime), 8) as Schedule,CAST(0 AS NUMERIC(18)) AS bitTask,ActivityDescription from (select A.ActivityID,DateAdd(minute,-@ClientTimeZoneOffset, StartDateTimeUtc) as dtStartTime,
DateAdd(minute, -@ClientTimeZoneOffset, dateadd(second,duration,startdatetimeutc)) as dtEndTime,T.numContactID,'Outlook - '+ [Subject] as vcdata,ActivityDescription     
from activity A   
join ActivityResource AR  
on AR.ActivityID=A.ActivityID  
join  Resource R  
on R.ResourceID=AR.ResourceID  
join #tempTable T  
on T.numContactID=numUserCntId  
where  ((startdatetimeutc between @dtWeekStartDate and @dtWeekEndDate) 
or (dateadd(second,duration,startdatetimeutc) between @dtWeekStartDate and @dtWeekEndDate))
and A.activityid not in 
(select distinct(numActivityId) from communication Comm where Comm.numDomainID = @numDomainID)
)X  
  
  
drop table #tempTable
GO
