/****** Object:  StoredProcedure [dbo].[USP_SaveImagePath]    Script Date: 07/26/2008 16:21:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Create By Siva
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_saveimagepath')
DROP PROCEDURE usp_saveimagepath
GO
CREATE PROCEDURE [dbo].[USP_SaveImagePath]
(@numDomainId as numeric(9)=0,
@vcImagePath as varchar(100)='',
@sintbyteMode as smallint=0)
As
Begin
	If @sintbyteMode=0 
		Update Domain Set vcBizDocImagePath=@vcImagePath Where numDomainId=@numDomainId
	Else If @sintbyteMode=1
		Update Domain Set vcCompanyImagePath=@vcImagePath Where numDomainId=@numDomainId
	Else If @sintbyteMode=2
		Update Domain Set vcBankImagePath=@vcImagePath Where numDomainId=@numDomainId
End
GO
