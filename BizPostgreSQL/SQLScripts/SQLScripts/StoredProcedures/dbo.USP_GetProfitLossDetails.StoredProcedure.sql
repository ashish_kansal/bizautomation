/****** Object:  StoredProcedure [dbo].[USP_GetProfitLossDetails]    Script Date: 07/26/2008 16:18:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- Created by Siva                                                          
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getprofitlossdetails')
DROP PROCEDURE usp_getprofitlossdetails
GO
CREATE PROCEDURE [dbo].[USP_GetProfitLossDetails]                                                         
@numDomainId as numeric(9),                                              
@dtFromDate as datetime,                                            
@dtToDate as datetime                                                         
As                                                            
Begin                                                            
 Declare @strSQL as varchar(2000)                                            
 Declare @i as integer                                        
 Set @strSQL=''            
   Declare @numParntAcntId as numeric(9)          
  Set @numParntAcntId=(Select numAccountId From Chart_Of_Accounts Where numParntAcntTypeId is null and numAcntTypeID is null and numDomainId = @numDomainId) --and numAccountId = 1           
                                            
 Select @i=count(*) From General_Journal_Header GJH Where --GJH.datEntry_Date>='' + convert(varchar(300),@dtFromDate) +''  And                                   
 GJH.numDomainId=@numDomainId And            
 GJH.datEntry_Date<='' + convert(varchar(300),@dtToDate) +''                                  
 if @i=0                                      
  Begin                                      
   Set @strSQL = ' Select COA.numAcntType as numAcntType,COA.numParntAcntId as numParntAcntId,        
     COA.numAccountId as numAccountId,COA.vcCatgyName as AcntTypeDescription,                                
     dbo.fn_GetCurrentOpeningBalanceForProfitLoss(COA.numAccountId,'''+Convert(varchar(300),@dtFromDate)+''','''+convert(varchar(300), @dtToDate)+''','+Convert(varchar(300),@numDomainId)+') As Amount         
              From Chart_Of_Accounts COA          
  inner join ListDetails LD on COA.numAcntType=LD.numListItemID                          
       Where COA.numDomainId=' + Convert(varchar(5),@numDomainId) + 'And COA.numAccountId <>'+Convert(varchar(10),@numParntAcntId) +                           
       'And (COA.numAcntType=822 Or COA.numAcntType=823 Or COA.numAcntType=824 Or COA.numAcntType=825 Or COA.numAcntType=826 )'                                            
   Set @strSQL=@strSQL+ '  And COA.dtOpeningDate>=''' + convert(varchar(300),@dtFromDate) +''' And  COA.dtOpeningDate<=''' + convert(varchar(300),@dtToDate) +''''                                               
  End                                       
  Else                                      
   Begin                                      
    Set @strSQL = ' Select COA.numAcntType as numAcntType,COA.numParntAcntId as numParntAcntId,         
   COA.numAccountId as numAccountId,COA.vcCatgyName as AcntTypeDescription,                                
   dbo.fn_GetCurrentOpeningBalanceForProfitLoss(COA.numAccountId,'''+Convert(varchar(300),@dtFromDate)+''','''+convert(varchar(300), @dtToDate)+''','+Convert(varchar(300),@numDomainId)+') As Amount          
         From Chart_Of_Accounts COA        
        inner join ListDetails LD on COA.numAcntType=LD.numListItemID                         
        Where COA.numDomainId=' + Convert(varchar(5),@numDomainId) + 'And COA.numAccountId <>'+convert(varchar(10),@numParntAcntId)+           
        +'And(COA.numAcntType=822 Or COA.numAcntType=823 Or COA.numAcntType=824 Or COA.numAcntType=825 Or COA.numAcntType=826 )'                                            
   End                                      
  print @strSQL                                            
  exec (@strSQL)                                            
End
GO
