/****** Object:  StoredProcedure [dbo].[USP_ManageBizDocSummary]    Script Date: 07/26/2008 16:19:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_managebizdocsummary')
DROP PROCEDURE usp_managebizdocsummary
GO
CREATE PROCEDURE [dbo].[USP_ManageBizDocSummary]
@numDomainID as numeric(9),
@numFormID as numeric(9),
@numBizDocID as numeric(9),
@strFormID as varchar(1000),
@numBizDocTempID as numeric(9)
as

declare @separator_position as integer                            
declare @strPosition as varchar(1000) 
delete from DycFrmConfigBizDocsSumm where numFormID = @numFormID and  numBizDocID=@numBizDocID and numDomainID=@numDomainID and isnull(numBizDocTempID,0)=@numBizDocTempID
                                     
   while patindex('%,%' , @strFormID) <> 0                       
    begin -- Begin for While Loop                            
      select @separator_position =  patindex('%,%' , @strFormID)                            
    select @strPosition = left(@strFormID, @separator_position - 1)                            
       select @strFormID = stuff(@strFormID, 1, @separator_position,'')                            
     insert into DycFrmConfigBizDocsSumm (numFormID,numBizDocID,numFieldID,numDomainID,numBizDocTempID)                            
     values (@numFormID,@numBizDocID,@strPosition,@numDomainID,@numBizDocTempID)                            
                           
                              
   end  

CREATE TABLE #temp(
	ID int IDENTITY(1,1) NOT NULL,
	[numFormID] [numeric](18, 0) NOT NULL,
	[numBizDocID] [numeric](18, 0) NOT NULL,
	[numFieldID] [numeric](18, 0) NOT NULL,
	[numDomainID] [numeric](18, 0) NOT NULL,
	[numBizDocTempID] [numeric](18, 0) NULL
) 

insert into #temp
select * from DycFrmConfigBizDocsSumm Ds where numBizDocID=@numBizDocID and Ds.numFormID=@numFormID 
	and isnull(Ds.numBizDocTempID,0)=@numBizDocTempID and Ds.numDomainID=@numDomainID



select Ds.numFieldID as numFormFieldId,isnull(DFFM.vcFieldName,DFM.vcFieldName)  as vcFormFieldName 
from DycFormField_Mapping DFFM 
JOIN DycFieldMaster DFM on DFFM.numModuleID=DFFM.numModuleID and DFFM.numFieldID=DFM.numFieldID
join #temp Ds on Ds.numFieldID=DFFM.numFieldID 
where numBizDocID=@numBizDocID and Ds.numFormID=@numFormID and (DFFM.numFormID=@numFormID or DFFM.numDomainID is null)
and isnull(Ds.numBizDocTempID,0)=@numBizDocTempID and Ds.numDomainID=@numDomainID order by Ds.id
GO
