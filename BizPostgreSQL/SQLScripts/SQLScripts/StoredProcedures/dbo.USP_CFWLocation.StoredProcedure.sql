/****** Object:  StoredProcedure [dbo].[USP_CFWLocation]    Script Date: 07/26/2008 16:15:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_cfwlocation')
DROP PROCEDURE usp_cfwlocation
GO
CREATE PROCEDURE [dbo].[USP_CFWLocation]  
  
as  
  
select loc_id,loc_name from CFW_Loc_Master
GO
