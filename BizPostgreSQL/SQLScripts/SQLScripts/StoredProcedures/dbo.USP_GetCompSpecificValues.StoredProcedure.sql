/****** Object:  StoredProcedure [dbo].[USP_GetCompSpecificValues]    Script Date: 07/26/2008 16:16:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj        
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getcompspecificvalues')
DROP PROCEDURE usp_getcompspecificvalues
GO
CREATE PROCEDURE [dbo].[USP_GetCompSpecificValues]        
@numCommID as numeric(9)=0,        
@numOppID as numeric(9)=0,        
@numProID as numeric(9)=0,        
@numCasesID as numeric(9)=0,        
@numDivID as numeric(9)=0 output,        
@numCompID as numeric(9)=0 output,        
@numContID as numeric(9)=0 output,        
@tintCRMType as numeric(9)=0 output,        
@numTerID as numeric(9)=0 output,        
@vcCompanyName as varchar(100)='' output,        
@vcContactName as varchar(100)='' output,        
@vcEmail as varchar(100)='' output,        
@vcPhone as varchar(20)='' output,
@charModule as char(1),        
@vcNoOfEmployeesId AS VARCHAR(30)='' OUTPUT,
@numCurrencyID AS numeric(9)=0 OUTPUT,
@bitOnCreditHold AS BIT=0 OUTPUT,
@numBillingDays AS BIGINT = 0 OUTPUT,
@numDefaultExpenseAccountID NUMERIC(18,0) = 0 OUTPUT,
@numAccountClassID NUMERIC(18,0) = 0 OUTPUT,
@numRecordOwner NUMERIC(18,0) = 0 OUTPUT,
@vcPhoneExt VARCHAR(500)='' OUTPUT,
@vcCustomerRelation VARCHAR(500)='' OUTPUT
as        
        
if @charModule='A'         
begin        
 select @numDivID=isnull(D.numDivisionID,0),        
 @numCompID=isnull(D.numCompanyID,0),         
 @numContID=isnull(ADC.numContactID,0),        
  @tintCRMType=isnull(tintCRMType,0),        
 @numTerID=isnull(numTerID,0),        
        @vcCompanyName=isnull(vcCompanyName,''),        
        @vcContactName=isnull(vcFirstName,'')+' '+isnull(vcLastName,''),        
        @vcEmail=isnull(vcEmail,''),        
        @vcPhone=isnull(numPhone,''),
        @vcPhoneExt=isnull(numPhoneExtension,''),
        @vcCustomerRelation=(SELECT TOP 1 ISNULL(vcData,'') FROM ListDetails WHERE numListID=5 AND numListItemID=Com.numCompanyType),
        @vcNoOfEmployeesId=dbo.GetListIemName(Com.numNoOfEmployeesId),
        @numCurrencyID=ISNULL(D.numCurrencyID,0),
        @bitOnCreditHold=ISNULL(D.bitOnCreditHold,0),
        @numBillingDays = ISNULL(D.numBillingDays,0),
        @numDefaultExpenseAccountID = ISNULL(numDefaultExpenseAccountID,0)
 from Communication C        
 join AdditionalContactsInformation ADC        
 on ADC.numContactID=C.numContactID        
 join DivisionMaster D        
 on D.numDivisionID=ADC.numDivisionID        
 join CompanyInfo Com        
        on Com.numCompanyID=D.numCompanyID        
 where numCommID=@numCommID        
end        
else        
        
if @charModule='O'       
begin        
 select @numDivID=isnull(D.numDivisionID,0),        
 @numCompID=isnull(D.numCompanyID,0),         
 @numContID=isnull(ADC.numContactID,0),        
  @tintCRMType=isnull(tintCRMType,0),        
 @numTerID=isnull(numTerID,0),        
 @vcCompanyName=isnull(vcCompanyName,'-'),        
        @vcContactName=isnull(vcFirstName,'-')+' '+isnull(vcLastName,''),        
        @vcEmail=isnull(vcEmail,''),        
        @vcPhone=isnull(numPhone,''),
        @vcNoOfEmployeesId=dbo.GetListIemName(Com.numNoOfEmployeesId),
        @numCurrencyID=ISNULL(D.numCurrencyID,0),
        @bitOnCreditHold=ISNULL(D.bitOnCreditHold,0),
        @numBillingDays = ISNULL(D.numBillingDays,0),
        @numDefaultExpenseAccountID = ISNULL(numDefaultExpenseAccountID,0) 
 from OpportunityMaster O        
 join AdditionalContactsInformation ADC        
 on ADC.numContactID=O.numContactID        
 join DivisionMaster D        
 on D.numDivisionID=ADC.numDivisionID        
 join CompanyInfo Com        
        on Com.numCompanyID=D.numCompanyID        
 where numOppID=@numOppID        
end        
else        
        
if @charModule='P'         
begin        
 select @numDivID=isnull(D.numDivisionID,0),        
 @numCompID=isnull(D.numCompanyID,0),         
 @numContID=isnull(ADC.numContactID,0),        
  @tintCRMType=isnull(tintCRMType,0),        
 @numTerID=isnull(numTerID,0),        
 @vcCompanyName=isnull(vcCompanyName,'-'),        
        @vcContactName=isnull(vcFirstName,'-')+' '+isnull(vcLastName,''),        
        @vcEmail=isnull(vcEmail,''),        
        @vcPhone=isnull(numPhone,''),
        @vcNoOfEmployeesId=dbo.GetListIemName(Com.numNoOfEmployeesId)  ,
        @numCurrencyID=ISNULL(D.numCurrencyID,0),
        @bitOnCreditHold=ISNULL(D.bitOnCreditHold,0),
        @numBillingDays = ISNULL(D.numBillingDays,0),
        @numDefaultExpenseAccountID = ISNULL(numDefaultExpenseAccountID,0)
 from ProjectsMaster P      
 left join AdditionalContactsInformation ADC      
 on P.numCustPrjMgr=ADC.numContactID        
 join DivisionMaster D        
 on D.numDivisionID=P.numDivisionID        
 join CompanyInfo Com        
        on Com.numCompanyID=D.numCompanyID        
 where numProID=@numProID        
end        
else        
        
if @charModule='S'         
begin        
 select @numDivID=isnull(D.numDivisionID,0),        
 @numCompID=isnull(D.numCompanyID,0),         
 @numContID=isnull(ADC.numContactID,0),        
  @tintCRMType=isnull(tintCRMType,0),        
 @numTerID=isnull(numTerID,0),        
 @vcCompanyName=isnull(vcCompanyName,'-'),        
        @vcContactName=isnull(vcFirstName,'-')+' '+isnull(vcLastName,''),        
        @vcEmail=isnull(vcEmail,''),        
        @vcPhone=isnull(numPhone,''),
        @vcNoOfEmployeesId=dbo.GetListIemName(Com.numNoOfEmployeesId) ,
        @numCurrencyID=ISNULL(D.numCurrencyID,0),
        @bitOnCreditHold=ISNULL(D.bitOnCreditHold,0),
        @numBillingDays = ISNULL(D.numBillingDays,0),
        @numDefaultExpenseAccountID = ISNULL(numDefaultExpenseAccountID,0)      
 from Cases C        
 join AdditionalContactsInformation ADC        
 on ADC.numContactID=C.numContactID        
 join DivisionMaster D        
 on D.numDivisionID=ADC.numDivisionID        
 join CompanyInfo Com        
        on Com.numCompanyID=D.numCompanyID        
 where numCaseID=@numCasesID        
end       
else        
        
if @charModule='D'         
begin        
 select @numDivID=isnull(D.numDivisionID,0),        
 @numCompID=isnull(D.numCompanyID,0),         
 @numContID=isnull(ADC.numContactID,0),        
  @tintCRMType=isnull(tintCRMType,0),        
 @numTerID=isnull(numTerID,0),        
 @vcCompanyName=isnull(vcCompanyName,'-'),        
        @vcContactName=isnull(vcFirstName,'-')+' '+isnull(vcLastName,''),        
        @vcEmail=isnull(vcEmail,''),        
        @vcPhone=isnull(numPhone,''),
        @vcNoOfEmployeesId=dbo.GetListIemName(Com.numNoOfEmployeesId) ,
        @numCurrencyID=ISNULL(D.numCurrencyID,0),
        @bitOnCreditHold=ISNULL(D.bitOnCreditHold,0),
        @numBillingDays = ISNULL(D.numBillingDays,0),
        @numDefaultExpenseAccountID = ISNULL(numDefaultExpenseAccountID,0),
		@numAccountClassID = numAccountClassID,
		@numRecordOwner = ISNULL(D.numRecOwner,0)
 from DivisionMaster D
 join CompanyInfo Com        
 on Com.numCompanyID=D.numCompanyID     
 left join AdditionalContactsInformation ADC        
 on ADC.numDivisionID=D.numDivisionID
 AND ISNULL(ADC.bitPrimaryContact,0)=1
 where D.numDivisionID=@numDivID       
end       
if @charModule='C'         
begin        
 select @numDivID=isnull(D.numDivisionID,0),        
 @numCompID=isnull(D.numCompanyID,0),         
 @numContID=isnull(ADC.numContactID,0),        
  @tintCRMType=isnull(tintCRMType,0),        
 @numTerID=isnull(numTerID,0),        
 @vcCompanyName=isnull(vcCompanyName,'-'),        
        @vcContactName=isnull(vcFirstName,'-')+' '+isnull(vcLastName,''),        
        @vcEmail=isnull(vcEmail,''),        
        @vcPhone=isnull(numPhone,''),
        @vcNoOfEmployeesId=dbo.GetListIemName(Com.numNoOfEmployeesId)   ,
        @numCurrencyID=ISNULL(D.numCurrencyID,0),
        @bitOnCreditHold=ISNULL(D.bitOnCreditHold,0),
        @numBillingDays = ISNULL(D.numBillingDays,0),
        @numDefaultExpenseAccountID = ISNULL(numDefaultExpenseAccountID,0)  
 from DivisionMaster D        
 join AdditionalContactsInformation ADC        
 on ADC.numDivisionID=D.numDivisionID         
 join CompanyInfo Com        
 on Com.numCompanyID=D.numCompanyID        
 where ADC.numContactID=@numContID      
end
GO
