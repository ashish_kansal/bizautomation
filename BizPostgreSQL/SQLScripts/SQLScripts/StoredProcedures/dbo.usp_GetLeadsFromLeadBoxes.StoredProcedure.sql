/****** Object:  StoredProcedure [dbo].[usp_GetLeadsFromLeadBoxes]    Script Date: 07/26/2008 16:17:44 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
--This will get the leads for the specific domain whcih have been created from the 
--LeadBox
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getleadsfromleadboxes')
DROP PROCEDURE usp_getleadsfromleadboxes
GO
CREATE PROCEDURE [dbo].[usp_GetLeadsFromLeadBoxes]
	@numDomainId numeric 
--  
AS
	SELECT 
		cmp.numCompanyID,
		cmp.vcCompanyName,
		DM.vcDivisionName,
		DM.numDivisionID,
		ADC.numContactId,
		ADC.vcFirstName,
		ADC.vcLastName,
		ADC.numPhone + ' (' + ADC.numPhoneExtension + ')' AS numPhone, 
		ADC.vcEmail
	FROM 	
		CompanyInfo cmp
	INNER JOIN
		divisionmaster DM
	ON
		cmp.numCompanyID=DM.numCompanyID
	INNER JOIN
		AdditionalContactsInformation ADC
	ON
		ADC.numDivisionID=DM.numDivisionID
	WHERE 
		DM.tintCRMType=0
		AND DM.bitleadboxflg=1
		AND DM.numdomainid=@numDomainid
	ORDER BY
		cmp.vcCompanyName
GO
