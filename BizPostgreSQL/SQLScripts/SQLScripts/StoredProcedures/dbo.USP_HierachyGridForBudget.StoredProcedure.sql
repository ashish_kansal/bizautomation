/****** Object:  StoredProcedure [dbo].[USP_HierachyGridForBudget]    Script Date: 07/26/2008 16:18:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created By Siva                                               
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_hierachygridforbudget')
DROP PROCEDURE usp_hierachygridforbudget
GO
CREATE PROCEDURE [dbo].[USP_HierachyGridForBudget]                          
@numDomainId as numeric(9)=0,                                              
@strRow as text='',        
@tintType as int=0                                                  
as                                                 
Begin                                                
Declare @hDoc3 int                 
Declare @strSQL as varchar(8000)                 
Declare @strMonSQL as varchar(8000)                 
Set @strMonSQL=''                
Declare @numMonth as tinyint              
Set @numMonth =1                          
  Create table #tempTable(numAcntId numeric(9),numParentAcntId numeric(9))                  
 EXEC sp_xml_preparedocument @hDoc3 OUTPUT, @strRow                     
 Insert Into #tempTable(numAcntId,numParentAcntId)                                           
 Select * from (                                                                                                                                                       
 Select * from OPenXml (@hdoc3,'/NewDataSet/Table1',2)                                                                                                                                                        
 With(                                                   
 numAcntId numeric(9),                                                                                                                                                      
 numParentAcntId numeric(9)                                                    
 ))X                   
   Declare @Date as datetime        
 set @Date = dateadd(year,@tintType,getutcdate())             
 While @numMonth <=12                      
  Begin         
         
----  Declare @dtFiscalDate as datetime                    
----  Set @dtFiscalDate ='01/01/'+Convert(varchar(4),year(getutcdate()))                    
----  set @dtFiscalDate=dateadd(month,@numMonth-1,@dtFiscalDate)                    
----  Set @strMonSQL=@strMonSQL+' '+  ' '''' as ' +Convert(varchar(3),DateName(month,convert(varchar(100),@dtFiscalDate)))+','                      
----  Set @numMonth=@numMonth+1              
  Declare @dtFiscalStDate as datetime                  
  Declare @dtFiscalEndDate as datetime             
 Set @dtFiscalStDate =dbo.GetFiscalStartDate(dbo.GetFiscalyear(@Date,@numDomainId),@numDomainId)               
 set @dtFiscalStDate=dateadd(month,@numMonth-1,@dtFiscalStDate)            
 set @dtFiscalEndDate =  dateadd(day,-1,dateadd(year,1,dbo.GetFiscalStartDate(dbo.GetFiscalyear(@Date,@numDomainId),@numDomainId)))                     
    Set @strMonSQL=@strMonSQL+' '+  '''''' + ' as ''' +Convert(varchar(2),month(@dtFiscalStDate))+'~'+Convert(varchar(4),year(@dtFiscalStDate))+''','             
             
    Set @numMonth=@numMonth+1              
 End                     
    Set @strMonSQL = @strMonSQL +   '''''' + ' as Total'+ ',''''' + ' as  Comments'                   
      print @strMonSQL                             
 Set @strSQL='  with RecursionCTE (RecordID,ParentRecordID,vcCatgyName,TOC,T)                
 as                
 (                
 select numAccountId,numParntAcntId,vcCatgyName,convert(varchar(1000),'''') TOC,convert(varchar(1000),'''') T                
    from Chart_Of_Accounts                
    where numParntAcntId is null and numDomainID='+Convert(varchar(10),@numDomainId)             
    +' union all                
   select R1.numAccountId,                
          R1.numParntAcntId,                
    R1.vcCatgyName,                
          case when DataLength(R2.TOC) > 0                
                    then convert(varchar(1000),case when CHARINDEX(''.'',R2.TOC)>0 then R2.TOC else R2.TOC +''.'' end                
                                 + cast(R1.numAccountId as varchar(10)))                 
                    else convert(varchar(1000),cast(R1.numAccountId as varchar(10)))                 
                    end as TOC,case when DataLength(R2.TOC) > 0                
                    then  convert(varchar(1000),''&nbsp;&nbsp;''+ R2.T)                
else convert(varchar(1000),'''')                
                    end as T                
      from Chart_Of_Accounts as R1                      
      join RecursionCTE as R2 on R1.numParntAcntId = R2.RecordID and R1.numDomainID='+Convert(varchar(10),@numDomainId) +' and R1.numAccountId in(Select numAcntId From  #tempTable)                
  )                 
select RecordID as numChartAcntId,ParentRecordID as numParentAcntId,T+vcCatgyName as vcCategoryName,TOC,'+Convert(varchar(8000),@strMonSQL)+' from RecursionCTE Where ParentRecordID is not null order by TOC asc'                
      print @strSQL                             
  Exec (@strSQL)                    
End
GO
