/****** Object:  StoredProcedure [dbo].[USP_DeletePaymentDetails]    Script Date: 07/26/2008 16:15:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created By Siva            
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_deletepaymentdetails')
DROP PROCEDURE usp_deletepaymentdetails
GO
CREATE PROCEDURE [dbo].[USP_DeletePaymentDetails]            
(      
@numBizDocsPaymentDetId as numeric(9)=0,            
@numBizDocsId as numeric(9)=0,            
@numOppId as numeric(9)=0)         
As            
Begin            
 Declare @bitIntegratedToAcnt as bit          
 Declare @monAmount as DECIMAL(20,5)   
 DECLARE @tintPaymentType AS TINYINT
 
  --Added by chintan to Update Embedded cost foreign key reference before deleteting primary key
 UPDATE [EmbeddedCost] SET [numBizDocsPaymentDetId] = NULL WHERE [numBizDocsPaymentDetId] = @numBizDocsPaymentDetId  
 

 --- Added by sojan to delete the BizDoc Comission on  03 May 2010
 
 create table #TempBizDocCom
 ( numJournalId numeric(18)); 
 
     
 Set @monAmount=0      
 
 Select @bitIntegratedToAcnt=bitIntegratedToAcnt,@monAmount=isnull(monAmount,0),@tintPaymentType=tintPaymentType From OpportunityBizDocsDetails Where numBizDocsPaymentDetId=@numBizDocsPaymentDetId          
 
 
 If @bitIntegratedToAcnt=0           
  Begin          
-- Added condition by chintan..reason. When deleting vendor payment entry.. it does not delete Bill entry. it moves back bill to DECIMAL(20,5) Out 
	IF @numBizDocsId >0 AND @numOppId>0
	BEGIN
		 Delete From OpportunityBizDocsPaymentDetails Where numBizDocsPaymentDetId=@numBizDocsPaymentDetId            
		 Delete From OpportunityBizDocsDetails where numBizDocsPaymentDetId=@numBizDocsPaymentDetId
		 IF @tintPaymentType <> 5 --Commission Expense amount should not be updated
		 BEGIN
			 Update OpportunityBizDocs Set monAmountPaid=isnull(monAmountPaid,0)-@monAmount Where  numOppBizDocsId=@numBizDocsId And numOppId=@numOppId      
			 Update OpportunityMaster Set numPClosingPercent=isnull(numPClosingPercent,0)-@monAmount Where numOppId=@numOppId         	
		 END
	END

-- @tintPaymentType - 2 means Bill against vendor, 4 Bill Against liability
IF (@tintPaymentType =2 OR @tintPaymentType =4) AND @bitIntegratedToAcnt=0
AND (SELECT COUNT(*) FROM dbo.General_Journal_Header WHERE numBizDocsPaymentDetId =@numBizDocsPaymentDetId)=0
BEGIN
	Delete From OpportunityBizDocsPaymentDetails Where numBizDocsPaymentDetId=@numBizDocsPaymentDetId            
	Delete From OpportunityBizDocsDetails where numBizDocsPaymentDetId=@numBizDocsPaymentDetId
END


     
     ---- Added by Sojan to delete the BizDocComission & Its Journals 03 May 2010
  insert into #TempBizDocCom  
 select distinct GD.numJournalId from General_Journal_Details GD where numcontactid in 
(select BC.numComissionID from BizDocComission BC where BC.numOppBizDocId=@numBizDocsId)
 
 delete from CheckDetails  where  numCheckId 
 in (select [numCheckId] from General_Journal_Header GH where numJournal_Id in 
 (select numJournalId from #TempBizDocCom));
 
 delete from General_Journal_Details where numJournalId in (select numJournalId from #TempBizDocCom);
 
 delete from General_Journal_Header where numJournal_Id in (select numJournalId from #TempBizDocCom);
 
 --delete from BizDocComission where numOppBizDocId = @numBizDocsId
 
 DROP TABLE #TempBizDocCom
 -------------------- ********** End of BizDoc Deletion**********-----------------
 
     
     
     DECLARE @numJournal_ID AS NUMERIC(9)
     DECLARE @numDomainID AS NUMERIC(9)
     SELECT @numJournal_ID=[numJournalId],@numDomainID=[numDomainId] FROM [General_Journal_Details] WHERE [numBizDocsPaymentDetailsId] = @numBizDocsPaymentDetId
     
     exec USP_DeleteJournalDetails @numJournalId=@numJournal_ID,@numDomainId=@numDomainId,@numCheckId=0,@numCashCreditCardId=0,@numDepositId=0
--     DELETE FROM [General_Journal_Details] WHERE [numJournalId]=@numJournalID
--     DELETE FROM [General_Journal_Header] WHERE [numJournal_Id]=@numJournalID
-- 
  End          
  --Update OpportunityMaster Set numPClosingPercent=isnull(monAmountPaid,0)-@monAmount            
End

GO
