/****** Object:  StoredProcedure [dbo].[usp_InsertQBSynch]    Script Date: 07/26/2008 16:19:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_insertqbsynch')
DROP PROCEDURE usp_insertqbsynch
GO
CREATE PROCEDURE [dbo].[usp_InsertQBSynch]
	@numOppID numeric=0,
	@numCompanyID numeric=0,
	@numDomainID numeric=0
--
AS
	INSERT INTO QBSynch(numOppID,numCompanyID,numDomainID)
	VALUES(@numOppID,@numCompanyID,@numDomainID)
GO
