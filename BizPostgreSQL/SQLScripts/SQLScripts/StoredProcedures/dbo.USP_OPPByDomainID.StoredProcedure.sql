/****** Object:  StoredProcedure [dbo].[USP_OPPByDomainID]    Script Date: 07/26/2008 16:20:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_oppbydomainid')
DROP PROCEDURE usp_oppbydomainid
GO
CREATE PROCEDURE [dbo].[USP_OPPByDomainID]  
(  
 @numDomainId numeric(9)=null  
)  
as  
begin  
if @numDomainId=0 
begin
Select vcItemname,numItemcode from item ORDER by vcItemName 
end
else
begin
Select vcItemname,numItemcode from item where numdomainId=@numDomainId ORDER by vcItemName 
end
  
end
GO
