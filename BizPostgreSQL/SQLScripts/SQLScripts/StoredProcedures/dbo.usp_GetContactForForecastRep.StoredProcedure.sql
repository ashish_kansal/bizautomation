/****** Object:  StoredProcedure [dbo].[usp_GetContactForForecastRep]    Script Date: 07/26/2008 16:16:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getcontactforforecastrep')
DROP PROCEDURE usp_getcontactforforecastrep
GO
CREATE PROCEDURE [dbo].[usp_GetContactForForecastRep] 
	@numCreatedBy numeric(9)=0,
	@numDomainID numeric(9)   
--
AS
BEGIN
	SELECT     numContactId, vcFirstName, vcLastName, numPhone, numPhoneExtension, vcPosition
FROM         dbo.AdditionalContactsInformation
WHERE numCreatedBy=@numCreatedBy
AND numDomainID=@numDomainID
	
END
GO
