/****** Object:  StoredProcedure [dbo].[USP_GetChildItems]    Script Date: 07/26/2008 16:16:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- [dbo].[USP_GetChildItems]  173031   
--created by Anoop jayaraj                      
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getchilditems')
DROP PROCEDURE usp_getchilditems
GO
CREATE PROCEDURE [dbo].[USP_GetChildItems]                             
@numKitId as numeric(9)                            
as                            
select numItemCode,vcItemName,txtItemDesc,DTL.numQtyItemsReq,0 as numOppChildItemID,
Item.monListPrice monListPrice, 
convert(varchar(30),Item.monListPrice)+'/'+isnull(UOM.vcUnitName,'Units') as UnitPrice,            
case when charItemType='P' then 'Inventory Item' when charItemType='S' then 'Service' when charItemType='A' then 'Accessory' when charItemType='N' then 'Non-Inventory Item' end as charItemType,charItemType as ItemType
from item                                
join ItemDetails Dtl                 
on numChildItemID=numItemCode                    
LEFT JOIN UOM ON UOM.numUOMId=item.numBaseUnit
where  numItemKitID=@numKitId
GO
