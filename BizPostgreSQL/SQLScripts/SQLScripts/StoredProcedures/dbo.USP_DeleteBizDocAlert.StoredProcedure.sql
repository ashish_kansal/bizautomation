/****** Object:  StoredProcedure [dbo].[USP_DeleteBizDocAlert]    Script Date: 07/26/2008 16:15:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_deletebizdocalert')
DROP PROCEDURE usp_deletebizdocalert
GO
CREATE PROCEDURE [dbo].[USP_DeleteBizDocAlert]
@numBizDocAlertID as numeric(9)
as


delete from BizDocAlerts where numBizDocAlertID=@numBizDocAlertID
GO
