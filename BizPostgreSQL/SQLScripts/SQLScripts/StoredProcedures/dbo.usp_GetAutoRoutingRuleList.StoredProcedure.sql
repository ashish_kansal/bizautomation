/****** Object:  StoredProcedure [dbo].[usp_GetAutoRoutingRuleList]    Script Date: 07/26/2008 16:16:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--modified by anoop jayaraj                         
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getautoroutingrulelist')
DROP PROCEDURE usp_getautoroutingrulelist
GO
CREATE PROCEDURE [dbo].[usp_GetAutoRoutingRuleList]        
 @numDomainID numeric=0,    
 @numUserCntID as numeric(9)=0              
AS                  
BEGIN        
  IF NOT EXISTS(SELECT numRoutID FROM RoutingLeads WHERE numDomainID=@numDomainID AND bitDefault = 1)        
  BEGIN        
     DECLARE @numEmpId Numeric(9)        
     --SELECT @numEmpId = um.numUserID FROM UserMaster UM WHERE um.numDomainID=@numDomainID         
        
     INSERT INTO RoutingLeads (numDomainId, vcRoutName,tintEqualTo, numCreatedBy, dtCreatedDate,numModifiedBy, dtModifiedDate,bitDefault, tintPriority)        
                         VALUES(@numDomainID, 'Default',0, @numUserCntID, getutcdate(), @numUserCntID, getutcdate(),  @numUserCntID, 0)        
        
     INSERT INTO RoutingLeadDetails (numRoutID, numEmpId, intAssignOrder) VALUES(@@IDENTITY, 0, 0)        
  END        
                  
  SELECT numRoutID, vcRoutName, bitDefault,isNull(tintPriority,0) AS tintPriority               
   FROM RoutingLeads                
   WHERE                  
    numDomainID=@numDomainID                  
   ORDER BY tintPriority                
END
GO
