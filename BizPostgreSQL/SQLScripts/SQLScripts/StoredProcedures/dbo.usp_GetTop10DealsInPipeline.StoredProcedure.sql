/****** Object:  StoredProcedure [dbo].[usp_GetTop10DealsInPipeline]    Script Date: 07/26/2008 16:18:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_gettop10dealsinpipeline')
DROP PROCEDURE usp_gettop10dealsinpipeline
GO
CREATE PROCEDURE [dbo].[usp_GetTop10DealsInPipeline]                
 @numDomainID numeric,                
 @dtDateFrom datetime,                
 @dtDateTo datetime,                
 @numTerID numeric=0,                
 @numUserCntID numeric=0,                
 @tintRights tinyint=3,                
 @intType numeric=0                   
--                
                 
                 
AS                
BEGIN                
 IF @tintRights=3 --All Records                
 BEGIN                
   IF @intType = 0                
   BEGIN              
   SELECT TOP 10 OM.vcPOppName,                
    CI.vcCompanyName + ' - (' + DM.vcDivisionName + ')' As CompanyName,                
    ACI.vcFirstName + ' ' + ACI.vcLastName As Contact,                
    dbo.GetOppLstMileStonePercentage(OM.numOppID) as Mile, OM.monPAmount As Amount,                 
    OM.numCreatedBy, OM.numPclosingPercent, OM.numOppID ,dbo.fn_GetContactName(OM.numRecOwner) as vcUserName               
    FROM OpportunityMaster OM                     
    JOIN DivisionMaster DM              
     ON DM.numDivisionID=OM.numDivisionID                
    JOIN CompanyInfo CI              
    ON CI.numCompanyID=DM.numCompanyID                   
    JOIN  AdditionalContactsInformation ACI              
     ON ACI.numContactID=OM.numContactID                
    WHERE OM.tintOppStatus=0  and OM.tintOpptype=1               
     AND OM.intPEstimatedCloseDate BETWEEN @dtDateFrom AND @dtDateTo                
     AND OM.numDomainID=@numDomainID              
    ORDER BY Amount DESC                
              
   END                
   ELSE                
   BEGIN                
   SELECT TOP 10 OM.vcPOppName,                
    CI.vcCompanyName + ' - (' + DM.vcDivisionName + ')' As CompanyName,                
    ACI.vcFirstName + ' ' + ACI.vcLastName As Contact,                
     dbo.GetOppLstMileStonePercentage(OM.numOppID) as Mile, OM.monPAmount As Amount,                 
    OM.numCreatedBy, OM.numPclosingPercent, OM.numOppID    ,ACI1.vcFirstName+' '+ACI1.vcLastName as vcUserName               
    FROM OpportunityMaster OM                       
    JOIN DivisionMaster DM              
     ON DM.numDivisionID=OM.numDivisionID                
    JOIN CompanyInfo CI              
    ON CI.numCompanyID=DM.numCompanyID                   
    JOIN  AdditionalContactsInformation ACI              
     ON ACI.numContactID=OM.numContactID 
     JOIN  AdditionalContactsInformation ACI1              
     ON ACI1.numContactID=OM.numRecOwner                
    WHERE OM.tintOppStatus=0  and OM.tintOpptype=1                 
     AND OM.intPEstimatedCloseDate BETWEEN @dtDateFrom AND @dtDateTo                
     AND OM.numDomainID=@numDomainID                         
     AND ACI1.numTeam is not null                   
     AND ACI1.numTeam in(select F.numTeam from ForReportsByTeam F                   
     where F.numuserCntId=@numUserCntID and F.numDomainid=@numDomainId and F.tintType=@intType)                  
    ORDER BY Amount DESC                
              
                
   END                
 END                
                
 IF @tintRights=2 --Territory Records                
 BEGIN                
   IF @intType = 0                
   BEGIN                
   SELECT TOP 10 OM.vcPOppName,                
    CI.vcCompanyName + ' - (' + DM.vcDivisionName + ')' As CompanyName,                
    ACI.vcFirstName + ' ' + ACI.vcLastName As Contact,                
    dbo.GetOppLstMileStonePercentage(OM.numOppID) as Mile, OM.monPAmount As Amount,                 
    OM.numCreatedBy, OM.numPclosingPercent, OM.numOppID      ,dbo.fn_GetContactName(OM.numRecOwner) as vcUserName             
    FROM OpportunityMaster OM                       
    JOIN DivisionMaster DM              
     ON DM.numDivisionID=OM.numDivisionID                
    JOIN CompanyInfo CI              
    ON CI.numCompanyID=DM.numCompanyID                   
    JOIN  AdditionalContactsInformation ACI              
     ON ACI.numContactID=OM.numContactID                
    WHERE OM.tintOppStatus=0  and OM.tintOpptype=1                
     AND OM.intPEstimatedCloseDate BETWEEN @dtDateFrom AND @dtDateTo                
     AND OM.numDomainID=@numDomainID              
     AND DM.numTerID in (SELECT numTerritoryID FROM UserTerritory WHERE numUserCntID=@numUserCntID)                
  ORDER BY Amount DESC                
              
   END                
   ELSE                
   BEGIN                
   SELECT TOP 10 OM.vcPOppName,                
    CI.vcCompanyName + ' - (' + DM.vcDivisionName + ')' As CompanyName,                
    ACI.vcFirstName + ' ' + ACI.vcLastName As Contact,                
     dbo.GetOppLstMileStonePercentage(OM.numOppID) as Mile, OM.monPAmount As Amount,                 
    OM.numCreatedBy, OM.numPclosingPercent, OM.numOppID,ACI1.vcFirstName+' '+ACI1.vcLastName as vcUserName               
    FROM OpportunityMaster OM              
               
    JOIN DivisionMaster DM              
     ON DM.numDivisionID=OM.numDivisionID                
    JOIN CompanyInfo CI              
    ON CI.numCompanyID=DM.numCompanyID                   
    JOIN  AdditionalContactsInformation ACI              
     ON ACI.numContactID=OM.numContactID 
     JOIN  AdditionalContactsInformation ACI1              
     ON ACI1.numContactID=OM.numRecOwner                
    WHERE OM.tintOppStatus=0  and OM.tintOpptype=1              
     AND OM.intPEstimatedCloseDate BETWEEN @dtDateFrom AND @dtDateTo                
     AND OM.numDomainID=@numDomainID                     
     AND DM.numTerID in (SELECT numTerritoryID FROM UserTerritory WHERE numUserCntID=@numUserCntID)                
     AND ACI.numTeam is not null                   
     AND ACI.numTeam in(select F.numTeam from ForReportsByTeam F                   
     where F.numuserCntId=@numUserCntID and F.numDomainid=@numDomainId and F.tintType=@intType)                  
    ORDER BY Amount DESC                
              
                
   END                
 END                
                
 IF @tintRights=1 --Owner Records                
 BEGIN                
   IF @intType = 0                
   BEGIN                
   SELECT TOP 10 OM.vcPOppName,                
    CI.vcCompanyName + ' - (' + DM.vcDivisionName + ')' As CompanyName,                
    ACI.vcFirstName + ' ' + ACI.vcLastName As Contact,                
    dbo.GetOppLstMileStonePercentage(OM.numOppID) as Mile, OM.monPAmount As Amount,                 
    OM.numCreatedBy, OM.numPclosingPercent, OM.numOppID,dbo.fn_GetContactName(OM.numRecOwner) as vcUserName                   
    FROM OpportunityMaster OM                       
    JOIN DivisionMaster DM              
     ON DM.numDivisionID=OM.numDivisionID                
    JOIN CompanyInfo CI              
    ON CI.numCompanyID=DM.numCompanyID                   
    JOIN  AdditionalContactsInformation ACI              
     ON ACI.numContactID=OM.numContactID                
    WHERE OM.tintOppStatus=0  and OM.tintOpptype=1                 
     AND OM.intPEstimatedCloseDate BETWEEN @dtDateFrom AND @dtDateTo                
     AND OM.numDomainID=@numDomainID              
     AND OM.numRecOwner=@numUserCntID                
    ORDER BY Amount DESC                
              
   END                
   ELSE                
   BEGIN                
   SELECT TOP 10 OM.vcPOppName,                
    CI.vcCompanyName + ' - (' + DM.vcDivisionName + ')' As CompanyName,                
    ACI.vcFirstName + ' ' + ACI.vcLastName As Contact,                
     dbo.GetOppLstMileStonePercentage(OM.numOppID) as Mile, OM.monPAmount As Amount,                 
    OM.numCreatedBy, OM.numPclosingPercent, OM.numOppID  ,ACI1.vcFirstName+' '+ACI1.vcLastName as vcUserName                  
    FROM OpportunityMaster OM                        
    JOIN DivisionMaster DM              
     ON DM.numDivisionID=OM.numDivisionID                
    JOIN CompanyInfo CI              
    ON CI.numCompanyID=DM.numCompanyID                   
    JOIN  AdditionalContactsInformation ACI              
     ON ACI.numContactID=OM.numContactID 
     JOIN  AdditionalContactsInformation ACI1              
     ON ACI1.numContactID=OM.numRecOwner                
    WHERE OM.tintOppStatus=0  and OM.tintOpptype=1                   
     AND OM.intPEstimatedCloseDate BETWEEN @dtDateFrom AND @dtDateTo                
     AND OM.numDomainID=@numDomainID                      
     AND OM.numRecOwner=@numUserCntID                
       AND ACI1.numTeam is not null                   
     AND ACI1.numTeam in(select F.numTeam from ForReportsByTeam F                   
     where F.numuserCntId=@numUserCntID and F.numDomainid=@numDomainId and F.tintType=@intType)       
    ORDER BY Amount DESC                
              
                
   END                
 END                
                
END
GO
