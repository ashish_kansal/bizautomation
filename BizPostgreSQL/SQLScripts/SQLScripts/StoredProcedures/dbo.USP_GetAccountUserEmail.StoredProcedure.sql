GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getaccountuseremail')
DROP PROCEDURE usp_getaccountuseremail
GO
CREATE PROCEDURE [dbo].[USP_GetAccountUserEmail]
@byteMode as tinyint,
@numUserID as numeric(9)=0,
@numContactID as numeric(9)=0
as
if @byteMode=0 --- Based on UserID
begin
	select isnull(vcEmail,'') from UserMaster
	join AdditionalContactsInformation
	on numContactId=numUserDetailId
	where numUserID=@numUserID

end
if @byteMode=1  --- Based on COntactID
begin
	select isnull(vcEmail,'') from UserMaster
	join AdditionalContactsInformation
	on numContactId=numUserDetailId
	where numContactID=@numContactID
end


