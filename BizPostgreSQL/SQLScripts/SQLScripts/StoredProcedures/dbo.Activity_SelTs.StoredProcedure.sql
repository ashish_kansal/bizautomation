/****** Object:  StoredProcedure [dbo].[Activity_SelTs]    Script Date: 07/26/2008 16:14:24 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
/*
**  Gets the timestamp of an individual Activity.
**
**  This stored procedure can be used to retrieve the timestamp of
**  a Activity by it's key, ID.
*/
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='activity_selts')
DROP PROCEDURE activity_selts
GO
CREATE PROCEDURE [dbo].[Activity_SelTs]
	@ActivityID		integer		-- primary key number of the activity
AS
BEGIN
	SELECT
		[_ts] 
	FROM 
		[Activity]
	WHERE
		( [Activity].[ActivityID] = @ActivityID );
END
GO
