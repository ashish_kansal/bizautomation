/****** Object:  StoredProcedure [dbo].[USP_SaveCusfldForLeadBox]    Script Date: 07/26/2008 16:20:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj  
--exec USP_SaveCusfldForLeadBox @numContactID = 4891, @numDivisionID = 4862, @fld_id = 17, @Fld_Value = '667'
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_savecusfldforleadbox')
DROP PROCEDURE usp_savecusfldforleadbox
GO
CREATE PROCEDURE [dbo].[USP_SaveCusfldForLeadBox]      
      
@numContactID as numeric(9)=null, 
@numDivisionID as numeric(9)=null,     
@fld_id as numeric(9),
@Fld_Value as varchar(100)     
    
as           
declare   @PageId as integer 
declare   @RecordId as numeric(9) 
select @PageId=Grp_id from CFW_Fld_Master where Fld_id=@fld_id
if @PageId=1 set @RecordId=@numDivisionID
else if @PageId=4 set @RecordId=@numContactID

if @PageId=1      
begin      
      
insert into CFW_FLD_Values (Fld_ID,Fld_Value,RecId)      
values(@fld_id,@Fld_Value,@RecordId)   
      
end      
      
if @PageId=4      
begin            
insert into CFW_FLD_Values_Cont (Fld_ID,Fld_Value,RecId)      
values(@fld_id,@Fld_Value,@RecordId)   
      
end
GO
