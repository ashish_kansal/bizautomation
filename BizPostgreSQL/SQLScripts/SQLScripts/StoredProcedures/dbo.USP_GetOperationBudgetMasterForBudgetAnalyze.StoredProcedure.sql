/****** Object:  StoredProcedure [dbo].[USP_GetOperationBudgetMasterForBudgetAnalyze]    Script Date: 07/26/2008 16:17:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created By Siva                    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getoperationbudgetmasterforbudgetanalyze')
DROP PROCEDURE usp_getoperationbudgetmasterforbudgetanalyze
GO
CREATE PROCEDURE [dbo].[USP_GetOperationBudgetMasterForBudgetAnalyze]                    
(@numBudgetId as numeric(9)=0,          
 @numChartAcntId as numeric(9)=0,                  
 @sintByte as smallint=0,                 
 @sintForecast as smallint=0,             
 @intFiscalYear as int=0,            
 @numUserCntID as numeric(9)=0,        
 @numDomainId as numeric(9)=0,  
 @intType as int=0           
 )                    
As                    
Begin                    
             
Create table #temp(numBudgetId numeric(9),        
vcmonth varchar(100),       
vcmonthName varchar(10),        
monOriginalAmt DECIMAL(20,5),        
monAmount DECIMAL(20,5),        
monCashInflow DECIMAL(20,5),        
monSalesForecast DECIMAL(20,5),        
monTotalBudgetBalance DECIMAL(20,5),        
monProjectedBankBalance DECIMAL(20,5)        
)                    
 Declare @month as int                          
 Declare @monAmount as DECIMAL(20,5)                          
 Declare @vcMonDescription as varchar(200)       
 Declare @Date as datetime         
 Declare @lstrSQL as varchar(8000)        
 Set @lstrSQL=''            
 Declare @dtStartDate as datetime        
 Declare @intCurrentYear as int        
 Set @month=month(getutcdate())       
 Set @Date = dateadd(year,@intType,getutcdate())                    
if @sintByte=0                      
	Begin                      
		While @month<=24                          
			Begin          
				 Set @dtStartDate='01/01/'+convert(varchar(4),year(getutcdate()))         
				 Set @dtStartDate=dateadd(month,@month-1,@dtStartDate)        
				 --Set @lstrSQL=@lstrSQL+(convert(varchar(3), DATENAME(month, @dtStartDate)))          
				 Insert into #temp(numBudgetId,vcmonth,vcmonthName,monOriginalAmt,monAmount,monCashInflow,monSalesForecast,monTotalBudgetBalance,monProjectedBankBalance) Values     
				 (@numBudgetId,convert(varchar(100),month(@dtStartDate))+'~'+convert(varchar(4),year(@dtStartDate)),      
				 convert(varchar(3),DateName(month,@dtStartDate)),dbo.fn_GetBudgetMonthDet(@numBudgetId,month(@dtStartDate),year(@dtStartDate),@numDomainId,@numChartAcntId),dbo.fn_GetBudgetMonthDet(@numBudgetId,month(@dtStartDate),year(@dtStartDate),@numDomainId,@numChartAcntId)        
				 ,dbo.GetCashInflowForMonths(month(@dtStartDate),Year(@dtStartDate),@numDomainId),dbo.GetSalesForeCastForMonths(month(@dtStartDate),year(@dtStartDate),@numDomainId,@numUserCntID,@sintForecast), dbo.GetTotalBudgetBalanceForMonths(month(@dtStartDate),      
				 year(@dtStartDate),dbo.fn_GetBudgetMonthDet(@numBudgetId,month(@dtStartDate),year(@dtStartDate),@numDomainId,@numChartAcntId),@numDomainId,@numUserCntID,@sintForecast),                
				 dbo.GetProjectedBankBalance(@intType,@numBudgetId,month(@dtStartDate),Year(@dtStartDate),@numDomainId,@numUserCntID,@sintForecast,@numChartAcntId))                  
				                                  
				 Set @month=@month+1                           
			End          
  --Print '@lstrSQL======'+Convert(varchar(1000),@lstrSQL)                        
	End                      
--For Market Budget    
if @sintByte=1                      
	Begin                      
		While @month<=24                          
			Begin          
				 Set @dtStartDate='01/01/'+convert(varchar(4),year(@Date))         
				 Set @dtStartDate=dateadd(month,@month-1,@dtStartDate)        
				 --Set @lstrSQL=@lstrSQL+(convert(varchar(3), DATENAME(month, @dtStartDate)))          
				 Insert into #temp(numBudgetId,vcmonth,vcmonthName,monOriginalAmt,monAmount,monCashInflow,monSalesForecast,monTotalBudgetBalance,monProjectedBankBalance)     
				 Values (@numBudgetId,convert(varchar(100),month(@dtStartDate))+'~'+convert(varchar(4),year(@dtStartDate)),      
				 convert(varchar(3),DateName(month,@dtStartDate)),dbo.fn_GetMarketBudgetMonthDetail(@numBudgetId,month(@dtStartDate),year(@dtStartDate),@numDomainId,@numChartAcntId),dbo.fn_GetMarketBudgetMonthDetail(@numBudgetId,month(@dtStartDate),year(@dtStartDate),@numDomainId,@numChartAcntId)
				        
				 ,dbo.GetCashInflowForMonths(month(@dtStartDate),Year(@dtStartDate),@numDomainId),dbo.GetSalesForeCastForMonths(month(@dtStartDate),year(@dtStartDate),@numDomainId,@numUserCntID,@sintForecast), dbo.GetTotalBudgetBalanceForMonths(month(@dtStartDate),      
				 year(@dtStartDate),dbo.fn_GetMarketBudgetMonthDetail(@numBudgetId,month(@dtStartDate),year(@dtStartDate),@numDomainId,@numChartAcntId),@numDomainId,@numUserCntID,@sintForecast),                
				 dbo.GetProjectedBankBalance(@intType,@numBudgetId,month(@dtStartDate),Year(@dtStartDate),@numDomainId,@numUserCntID,@sintForecast,@numChartAcntId))                  
				                                  
				 Set @month=@month+1                           
			End          
--  Print '@lstrSQL======'+Convert(varchar(1000),@lstrSQL)                        
	End                      
    
    
--For Procurement Budget    
if @sintByte=2                      
	Begin                      
		While @month<=24                          
			Begin          
				 Set @dtStartDate='01/01/'+convert(varchar(4),year(@Date))         
				 Set @dtStartDate=dateadd(month,@month-1,@dtStartDate)        
				 --Set @lstrSQL=@lstrSQL+(convert(varchar(3), DATENAME(month, @dtStartDate)))          
				 Insert into #temp(numBudgetId,vcmonth,vcmonthName,monOriginalAmt,monAmount,monCashInflow,monSalesForecast,monTotalBudgetBalance,monProjectedBankBalance)     
				 Values (@numBudgetId,convert(varchar(100),month(@dtStartDate))+'~'+convert(varchar(4),year(@dtStartDate)),      
				 convert(varchar(3),DateName(month,@dtStartDate)),dbo.fn_GetProcurementBudgetMonthDetail(@numBudgetId,month(@dtStartDate),year(@dtStartDate),@numDomainId,@numChartAcntId),dbo.fn_GetProcurementBudgetMonthDetail(@numBudgetId,month(@dtStartDate),year(@dtStartDate),@numDomainId,@numChartAcntId)
				        
				 ,dbo.GetCashInflowForMonths(month(@dtStartDate),Year(@dtStartDate),@numDomainId),dbo.GetSalesForeCastForMonths(month(@dtStartDate),year(@dtStartDate),@numDomainId,@numUserCntID,@sintForecast), dbo.GetTotalBudgetBalanceForMonths(month(@dtStartDate),      
				 year(@dtStartDate),dbo.fn_GetProcurementBudgetMonthDetail(@numBudgetId,month(@dtStartDate),year(@dtStartDate),@numDomainId,@numChartAcntId),@numDomainId,@numUserCntID,@sintForecast),                
				 dbo.GetProjectedBankBalance(@intType,@numBudgetId,month(@dtStartDate),Year(@dtStartDate),@numDomainId,@numUserCntID,@sintForecast,@numChartAcntId))                  
				                                  
				 Set @month=@month+1                           
		End          
                        
	End                      
    
Select * From #temp                          
Drop table #temp                           
End
GO
