/****** Object:  StoredProcedure [dbo].[ActivityDelByItemId]    Script Date: 07/26/2008 16:14:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='activitydelbyitemid')
DROP PROCEDURE activitydelbyitemid
GO
CREATE PROCEDURE [dbo].[ActivityDelByItemId]
@itemid as varchar(500)
as

delete tmpCalandarTbl where itemId= @ItemId


delete activityresource where activityid in 
(select activityId from  activity
where itemId= @ItemId
)

delete recurrence where recurrenceid in 
(
select recurrenceId from  activity
where itemId= @ItemId and recurrenceId<>-999
)

delete activity
where itemId= @ItemId
GO
