/****** Object:  StoredProcedure [dbo].[usp_UpdateSignature]    Script Date: 07/26/2008 16:21:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj  
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_updatesignature')
DROP PROCEDURE usp_updatesignature
GO
CREATE PROCEDURE [dbo].[usp_UpdateSignature]  
@numUserID as numeric(9),  
@txtSignature varchar(8000)=''  
as  
  
update UserMaster set txtSignature=@txtSignature where numUserId=@numUserID
GO
