/****** Object:  StoredProcedure [dbo].[USP_GetMergeFieldsByAlertId]    Script Date: 07/26/2008 16:17:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getmergefieldsbyalertid')
DROP PROCEDURE usp_getmergefieldsbyalertid
GO
CREATE PROCEDURE [dbo].[USP_GetMergeFieldsByAlertId]  
@AlertDTLID as numeric(18,0)  
as  
SELECT  vcMergeField,   vcMergeFieldValue  
FROM         AlertEmailMergeFlds   
join EmailMergeFields on intMergeFldID= intEmailMergeFields  
where numEmailAlertDTLID=@AlertDTLID
GO
