/****** Object:  StoredProcedure [dbo].[USP_GetPayPeriod]    Script Date: 07/26/2008 16:18:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created by Siva
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getpayperiod')
DROP PROCEDURE usp_getpayperiod
GO
CREATE PROCEDURE [dbo].[USP_GetPayPeriod]
(@numDomainId as numeric(9)=0)
As
Begin
	Select isnull(tintPayPeriod,0) From Domain Where numDomainId=@numDomainId
End
GO
