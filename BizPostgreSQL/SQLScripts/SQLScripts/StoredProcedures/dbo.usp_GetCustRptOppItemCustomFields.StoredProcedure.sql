/****** Object:  StoredProcedure [dbo].[usp_GetCustRptOppItemCustomFields]    Script Date: 07/26/2008 16:17:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created By: Debasish Tapan Nag                        
--Purpose: Get a list of custom fields for the Opportunity and Items Report as applicable              
--Created Date: 10/31/2005                           
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getcustrptoppitemcustomfields')
DROP PROCEDURE usp_getcustrptoppitemcustomfields
GO
CREATE PROCEDURE [dbo].[usp_GetCustRptOppItemCustomFields] 
  @numDomainId Numeric(9)               
AS                  
BEGIN                
SELECT CASE Mst.Grp_id WHEN 1 THEN 1 WHEN 2 THEN 3 WHEN 4 THEN 2 ELSE 4 END AS numFieldGroupID, 'CFld' + CONVERT(NVarchar, Mst.Fld_id) AS vcDbFieldName, Mst.Fld_label AS vcScrFieldName,   
                      Dtl.numOrder AS numOrderAppearance, 0 AS boolSummationPossible, 0 AS boolDefaultColumnSelection  
FROM         dbo.CFW_Fld_Master Mst LEFT OUTER JOIN  
                      dbo.CFW_Fld_Dtl Dtl ON Mst.Fld_id = Dtl.numFieldId AND Mst.numDomainId = @numDomainId
GROUP BY Mst.Grp_id, 'CFld' + CONVERT(NVarchar, Mst.Fld_id), Mst.Fld_label, Dtl.numOrder, Mst.Fld_id  
HAVING      (Mst.Grp_id IN (1, 2, 4, 5))  
 --1: Organization; 2: Opportunities; 4: Contacts; 5: Items    
END
GO
