/****** Object:  StoredProcedure [dbo].[usp_ProCaseOppLinked]    Script Date: 07/26/2008 16:20:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_procaseopplinked')
DROP PROCEDURE usp_procaseopplinked
GO
CREATE PROCEDURE [dbo].[usp_ProCaseOppLinked]
@byteMode as int,
@numProId as numeric(9),
@numCaseId as numeric(9)
as
if @byteMode =0
	begin
		select PO.numOppId,vcPOppName from ProjectsOpportunities PO 
		join opportunitymaster OM on PO.numoppid = OM.numOppId where numproid = @numProId
	end
else
	begin
		select CO.numOppId,vcPOppName from CaseOpportunities CO 
		join opportunitymaster OM on CO.numoppid = OM.numOppId where numCaseid = @numCaseId
	end
GO
