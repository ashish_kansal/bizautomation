/****** Object:  StoredProcedure [dbo].[usp_getCustReportFilterlist]    Script Date: 07/26/2008 16:17:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getcustreportfilterlist')
DROP PROCEDURE usp_getcustreportfilterlist
GO
CREATE PROCEDURE [dbo].[usp_getCustReportFilterlist]
@ReportId as numeric =0  
as
select * from CustReportFilterlist where numCustomReportId = @ReportId
GO
