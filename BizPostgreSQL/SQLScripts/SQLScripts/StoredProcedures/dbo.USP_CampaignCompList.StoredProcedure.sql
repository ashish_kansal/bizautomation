/****** Object:  StoredProcedure [dbo].[USP_CampaignCompList]    Script Date: 07/26/2008 16:14:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--- Created By Anoop jayaraj        
/*

exec USP_CampaignCompList 61,4,0 


--Change In logic.

 don't validate Online/Offline campaign based on dates.. use campaignid 
 
*/


GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_campaigncomplist')
DROP PROCEDURE usp_campaigncomplist
GO
CREATE PROCEDURE [dbo].[USP_CampaignCompList]
    @numCampaignid as numeric(9),
    @tintComptype as integer,
    @NoofRecds as integer output,
	@dtStartDate DATETIME,
    @dtEndDate   DATETIME
as 
--    declare @intLaunchDate as varchar(20)        
--    declare @intEndDate as varchar(20) 
--DECLARE @vcLandingPage VARCHAR(200)
    DECLARE @numDomainID NUMERIC(9)
    DECLARE @bitOnline AS BIT

    select top 1
--            @intLaunchDate = intLaunchDate,
--            @intEndDate = intEndDate,
            @bitOnline = [bitIsOnline],
--@vcLandingPage=vcLandingPage,
            @numDomainID = [numDomainID]
    from    campaignmaster
    where   numCampaignid = @numCampaignid    


    DECLARE @strSql AS VARCHAR(4000)

    SET  @strSql =
'
select  CMP.numCompanyID,DM.numDivisionID,ADC.numContactID,DM.tintCRMType,CMP.vcCompanyName as CompanyName,           
		 ADC.vcLastName + '' '' + ADC.vcFirstName as PrimaryContact,          
		 isnull(LD2.vcData,''-'') as Follow,          
		 dbo.GetListIemName(CMP.numNoOfEmployeesId) AS numNoOfEmployeesId,
		dbo.FormatedDateFromDate(DM.bintCreatedDate,DM.numDomainID) as bintCreatedDate,
		CMP.txtComments as OrganizationComments       
		FROM  CompanyInfo CMP        
		join DivisionMaster DM on DM.numCompanyID=CMP.numCompanyID           
		join AdditionalContactsInformation ADC on ADC.numDivisionID=DM.numDivisionID        
		left join ListDetails LD2 on LD2.numListItemID= DM.numFollowUpStatus        
	  WHERE         
	 ISNULL(ADC.bitPrimaryContact,0)=1
	 and DM.numCampaignID= ' + convert(varchar(10), @numCampaignid) + '
	 and DM.bintCreatedDate between ''' + convert(varchar(25), @dtStartDate) + ''' And ''' + convert(varchar(25), @dtEndDate) + ''''
    
--    if @tintComptype = 4 -- All company Types        
--        begin        
--        end        
    if @tintComptype < 4 
        begin    
            SET @strSql = @strSql + ' and DM.tintCRMType=' + convert(varchar(10), @tintComptype)
        end        
      
    if @tintComptype > 4 
        begin  
            SET @strSql = @strSql + ' and CMP.numCompanyType=' + convert(varchar(10), @tintComptype)
        END

    PRINT @strSql ;
    EXEC (@strSql)
    select  @NoofRecds = @@rowcount       


GO
