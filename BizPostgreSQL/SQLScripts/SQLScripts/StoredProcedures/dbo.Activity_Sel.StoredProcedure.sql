/****** Object:  StoredProcedure [dbo].[Activity_Sel]    Script Date: 07/26/2008 16:14:23 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
/*
**  Selects the set of Activities having the given primary Resource. 
**
**  Use this stored procedure to access the entire set of Activities
**  belonging to a Resource by their name; use other stored procs
**  to retrieve Activities belonging to the Resource limited to a time
**  period, or those that are due for Reminder.
*/
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='activity_sel')
DROP PROCEDURE activity_sel
GO
CREATE PROCEDURE [dbo].[Activity_Sel]
	@ResourceName	nvarchar(64)	-- resource name (will be used to lookup ID)
AS
BEGIN
	--
	-- First step is to get the ResourceID for the primary resource name supplied.
	--
 DECLARE @ResourceID AS integer;
if isnumeric(@ResourceName) =1
begin
   set @ResourceID=convert(numeric(9), @ResourceName)
end
else
begin 
	set @ResourceID =-999	
end 
	--
	-- Second step is to retrieve the Activities organized (or owned by) this resource.
	-- They will be the ones in the ActivityResource table where ResourceID is the
	-- key number I've just identified. Exclude variances by only retrieving root
	-- Activities (those with a null OriginalStartDateTimeUTC). See Variance_Sel.
	--
	SELECT 
		[Activity].[AllDayEvent], 
		[Activity].[ActivityDescription], 
		[Activity].[Duration], 
		[Activity].[Location], 
		[Activity].[ActivityID], 
		[Activity].[StartDateTimeUtc], 
		[Activity].[Subject], 
		[Activity].[EnableReminder], 
		[Activity].[ReminderInterval],
		[Activity].[ShowTimeAs],
		[Activity].[Importance],
		[Activity].[Status],
		[Activity].[RecurrenceID],
		[Activity].[VarianceID],
		[ActivityResource].[ResourceID],
		[Activity].[_ts],
		[Activity].[ItemId],
		[Activity].[ChangeKey]

	FROM 
		[Activity] INNER JOIN [ActivityResource] ON [Activity].[ActivityID] = [ActivityResource].[ActivityID]
	WHERE 
		( ([Activity].[ActivityID] = [ActivityResource].[ActivityID]) AND ([ActivityResource].[ResourceID] = @ResourceID) AND ( [Activity].[OriginalStartDateTimeUtc] IS NULL) );
END
GO
