/****** Object:  StoredProcedure [dbo].[USP_ManageState]    Script Date: 07/26/2008 16:20:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_managestate')
DROP PROCEDURE usp_managestate
GO
CREATE PROCEDURE [dbo].[USP_ManageState]   
@numStateID as numeric(9)=0,    
@numDomainID as numeric(9)=0,       
@vcState as varchar(100)='',  
@numCountryID as numeric(9),      
@numUserID as numeric(9)  ,
@vcAbbreviations varchar(200),
@numShippingZone NUMERIC(18,0) = 0
AS
	IF @numStateID = 0     
	BEGIN      
		INSERT INTO STATE
		(
			numCountryID,vcState,vcAbbreviations,numDomainID,numCreatedBy,numModifiedBy,bintCreatedDate,bintModifiedDate,numShippingZone
		)
		VALUES
		(
			@numCountryID,@vcState,@vcAbbreviations,@numDomainID,@numUserID,@numUserID,getutcdate(),getutcdate(),@numShippingZone
		)      
	END      
	ELSE      
	BEGIN      
		UPDATE 
			State 
		SET 
			vcState=@vcState
			,numCountryID=@numCountryID
			,numDomainID=@numDomainID
			,numModifiedBy=@numUserID
			,vcAbbreviations=@vcAbbreviations
			,numShippingZone=@numShippingZone
			,bintModifiedDate=getutcdate()      
		WHERE 
			numStateID=@numStateID 
			AND numDomainID=@numDomainID  
         
	end
GO
