/****** Object:  StoredProcedure [dbo].[usp_CheckIsPrimaryContact]    Script Date: 07/26/2008 16:15:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
--This will check whether the contact which is being added is a primary contact or not
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_checkisprimarycontact')
DROP PROCEDURE usp_checkisprimarycontact
GO
CREATE PROCEDURE [dbo].[usp_CheckIsPrimaryContact]
	@numContactID numeric=0,
	@numDivId numeric=0   
	--@numCheckFlg numeric=0
--
AS
	--If the checkflag is passed then it means that we need to check whether a particular contact is of the type
	--Primary Contact or not.
	IF @numContactID <> 0
		BEGIN
			SELECT a.numcontacttype from additionalcontactsinformation a INNER JOIN listdetails b ON a.numcontacttype=b.numlistitemid where
			 ISNULL(a.bitPrimaryContact,0)=1 and a.numcontactid=@numContactID
		END
	--If not then we need to get the contactId for whom the mails are to be sent
	ELSE
		BEGIN
		SELECT vcFirstname,vcLastName,numcontactId FROM additionalcontactsinformation WHERE ISNULL(bitPrimaryContact,0)=1 and numdivisionId=@numDivId
		END
GO
