/****** Object:  StoredProcedure [dbo].[USP_GetEFormConfiguration]    Script Date: 07/26/2008 16:17:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_geteformconfiguration')
DROP PROCEDURE usp_geteformconfiguration
GO
CREATE PROCEDURE [dbo].[USP_GetEFormConfiguration]  
@numSpecDocID as numeric(9)=0  
as  
  
select * from  EformConfiguration   where numSpecDocID=@numSpecDocID
GO
