/****** Object:  StoredProcedure [dbo].[USP_GetContactList2]    Script Date: 07/26/2008 16:16:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getcontactlist2')
DROP PROCEDURE usp_getcontactlist2
GO
CREATE PROCEDURE [dbo].[USP_GetContactList2]                                                                   
 @numUserCntID numeric(9)=0,                                                                    
 @numDomainID numeric(9)=0,                                                                    
 @tintUserRightType tinyint=0,                                                                    
 @tintSortOrder tinyint=4,                                                                    
 @SortChar char(1)='0',                                                                   
 @FirstName varChar(100)= '',                                                                  
 @LastName varchar(100)='',                                                                  
 @CustName varchar(100)='',                                                                  
 @CurrentPage int,                                                                  
@PageSize int,                                                                  
@TotRecs int output,                                                                  
@columnName as Varchar(50),                                                                  
@columnSortOrder as Varchar(10),                                                                  
@numDivisionID as numeric(9)=0,                              
@bitPartner as bit ,                                   
@inttype as numeric(9)=0                                                             
--                                                                    
as  
       
declare @join as varchar(400)  
set @join = '' 
        
 if @columnName like 'Cust%' 
begin

	
	set @join= ' left Join CFW_FLD_Values_Cont CFW on CFW.RecId=ADC.numcontactId  and CFW.fld_id= '+replace(@columnName,'Cust','') +' '                                         
	set @columnName='CFW.Fld_Value'
		
end                         
if @columnName like 'DCust%'
begin
                                       
	set @join= ' left Join CFW_FLD_Values_Cont CFW on CFW.RecId=ADC.numcontactId  and CFW.fld_id= '+replace(@columnName,'DCust','')                                                                                   
	set @join= @join +' left Join ListDetails LstCF on LstCF.numListItemID=CFW.Fld_Value  '
	set @columnName='LstCF.vcData'  

end                                                              
                                                                  
declare @firstRec as integer                                                                  
declare @lastRec as integer                                                                  
set @firstRec= (@CurrentPage-1) * @PageSize              
set @lastRec= (@CurrentPage*@PageSize+1)                                                                   
                                                                  
                                                                  
declare @strSql as varchar(8000)                                                            
 set  @strSql='with tblcontacts as (Select '                                                          
      if (@tintSortOrder=5 or @tintSortOrder=6) set  @strSql= @strSql+' top 20 '                                                           
                                                                    
   set  @strSql= @strSql+ ' ROW_NUMBER() OVER (ORDER BY '+@columnName+' '+ @columnSortOrder+') AS RowNumber, ADC.numContactId                                                              
  FROM AdditionalContactsInformation ADC                                                 
  JOIN DivisionMaster DM ON ADC.numDivisionId = DM.numDivisionID                                                
  JOIN CompanyInfo C ON DM.numCompanyID = C.numCompanyId'  +@join+  '                                                  
  left join ListDetails LD on LD.numListItemID=C.numCompanyRating                                                           
  left join ListDetails LD1 on LD1.numListItemID= ADC.numEmpStatus'                           
  if @bitPartner=1 set @strSql=@strSql + ' left join CompanyAssociations CA on CA.numAssociateFromDivisionID=DM.numDivisionID and bitShareportal=1 and CA.bitDeleted=0                                 
and CA.numDivisionID=(select numDivisionID from AdditionalContactsInformation where numContactID='+convert(varchar(15),@numUserCntID)+ ')'                                                   
                                                    
 if @tintSortOrder= 7 set @strSql=@strSql+' join Favorites F on F.numContactid=ADC.numContactID '                                                     
                                       
  set  @strSql= @strSql +'                                                
        where DM.tintCRMType<>0                              
    and DM.numDomainID  = '+convert(varchar(15),@numDomainID)+''             
 if @inttype<>0  set @strSql=@strSql+' and ADC.numContactType  = '+convert(varchar(10),@inttype)            
    if @FirstName<>'' set @strSql=@strSql+' and ADC.vcFirstName  like '''+@FirstName+'%'''                                             
    if @LastName<>'' set @strSql=@strSql+' and ADC.vcLastName like '''+@LastName+'%'''                              
    if @CustName<>'' set @strSql=@strSql+' and c.vcCompanyName like '''+@CustName+'%'''                                                                  
if @SortChar<>'0' set @strSql=@strSql + ' And ADC.vcFirstName like '''+@SortChar+'%'''                                                                           
if @tintUserRightType=1 set @strSql=@strSql + ' AND (ADC.numRecOwner = '+convert(varchar(15),@numUserCntID)+ ')' + case when @bitPartner=1 then ' or (CA.bitShareportal=1 and  CA.bitDeleted=0)' else '' end                                                  
else if @tintUserRightType=2 set @strSql=@strSql + ' AND (DM.numTerID in (select numTerritoryID from  UserTerritory where numUserCntID= '+convert(varchar(15),@numUserCntID)+' ) or DM.numTerID=0) '                                                           
if @numDivisionID <> 0  set @strSql=@strSql + ' And DM.numDivisionID ='+ convert(varchar(10),@numDivisionID)      
                                                                         
if @tintSortOrder=2  set @strSql=@strSql + ' AND (ADC.numRecOwner = '+convert(varchar(15),@numUserCntID)+ ')'                                                                      
else if @tintSortOrder=3  set @strSql=@strSql + '  AND ADC.numContactType=92  '                                                          
else if @tintSortOrder=4  set @strSql=@strSql + ' AND ADC.bintCreatedDate > '''+convert(varchar(20),dateadd(day,-7,getutcdate()))+''''                                                        
else if @tintSortOrder=5  set @strSql=@strSql + ' and ADC.numCreatedby='+convert(varchar(15),@numUserCntID)      
                                                       
else if @tintSortOrder=6  set @strSql=@strSql + ' and ADC.numModifiedby='+convert(varchar(15),@numUserCntID)      
                                                                 
else if @tintSortOrder=7  set @strSql=@strSql + ' and F.numUserCntID='+convert(varchar(15),@numUserCntID)                                                             
    
 if (@tintSortOrder=5 and @columnName!='ADC.bintcreateddate')  set @strSql='select * from ('+@strSql+')X order by '+ @columnName +' '+ @columnSortOrder                                                               
else if (@tintSortOrder=6 and @columnName!='ADC.bintcreateddate')  set @strSql='select * from ('+@strSql+')X order by '+ @columnName +' '+ @columnSortOrder                                                     
else if @tintSortOrder=7  set @strSql=@strSql + ' and cType=''C'' '      
                                                                
    
set @strSql=@strSql + ')'      
                                                                 
declare @tintOrder as tinyint                                        
declare @vcFormFieldName as varchar(50)                                        
declare @vcListItemType as varchar(1)                                   
declare @vcListItemType1 as varchar(1)                                       
declare @vcAssociatedControlType varchar(10)                                        
declare @numListID AS numeric(9)                                        
declare @vcDbColumnName varchar(20)            
declare @WhereCondition varchar(2000)             
declare @Table varchar(2000)      
Declare @bitCustom as bit  
Declare @numFormFieldId as numeric  
          
set @tintOrder=0                                        
set @WhereCondition =''       
         
declare @Nocolumns as tinyint      
declare @DefaultNocolumns as tinyint   
set @Nocolumns=0      
select @Nocolumns=isnull(count(*),0)      
from [InitialListColumnConf] A         
where A.numFormId=10 and numUserCntID=@numUserCntID and numDomainID=@numDomainID  and numtype =@inttype          
 set @DefaultNocolumns=  @Nocolumns    
declare @strColumns as varchar(2000)      
set @strColumns=''      
while @DefaultNocolumns>0      
begin      
  
 set @strColumns=@strColumns+',null'      
 set @DefaultNocolumns=@DefaultNocolumns-1      
end   
     
      
set @strSql=@strSql+' select ADC.numContactId,DM.numDivisionID,DM.numTerID,ADC.numRecOwner,DM.tintCRMType,RowNumber'      
    
if @Nocolumns>0    
begin     
                                   
 select top 1 @tintOrder=tintOrder+1,@vcDbColumnName=vcDbColumnName,@vcFormFieldName=vcFormFieldName,          
 @vcAssociatedControlType=vcAssociatedControlType,@vcListItemType=vcListItemType,@numListID=numListID,@Table=vcLookBackTableName
	,@bitCustom=a.bitCustom,@numFormFieldId=A.numFormFieldId                                            
 from [InitialListColumnConf] A                                        
 left join DynamicFormFieldMaster D                                         
 on D.numFormFieldId=A.numFormFieldId                                        
 where A.numFormId=10 and numUserCntID=@numUserCntID and A.numDomainID=@numDomainID  and numtype =@inttype          
 order by tintOrder asc                                        
end    
else    
begin   
   
  select @DefaultNocolumns=isnull(Count(*),0)  
  from  DynamicFormFieldMaster D    
  where D.numFormId=10   and bitDefault = 1   
  
  while @DefaultNocolumns>0      
  begin    
  print  @Nocolumns         
  set @strColumns=@strColumns+',null'      
  set @DefaultNocolumns=@DefaultNocolumns-1      
  end  
  
   
  select top 1 @tintOrder=[Order]+1,@vcDbColumnName=vcDbColumnName,@vcFormFieldName=vcFormFieldName,          
  @vcAssociatedControlType=vcAssociatedControlType,@vcListItemType=vcListItemType,@numListID=numListID,@Table=vcLookBackTableName                                         
,@bitCustom=0,@numFormFieldId=D.numFormFieldId   
  from  DynamicFormFieldMaster D    
  where D.numFormId=10   and bitDefault = 1        
  order by [Order] asc                                        
end    
  set @DefaultNocolumns=  @Nocolumns     
while @tintOrder>0                                        
begin                                        
                                   
   if @bitCustom = 0
	begin                                       
 if @vcAssociatedControlType='SelectBox'                                        
        begin                                        
			                                                        
			  if @vcListItemType='L'                                         
			  begin                                        
			   set @strSql=@strSql+',L'+ convert(varchar(3),@tintOrder)+'.vcData'+' ['+ @vcFormFieldName+'~'+ @vcDbColumnName+']'                                        
			   set @WhereCondition= @WhereCondition +' left Join ListDetails L'+ convert(varchar(3),@tintOrder)+ ' on L'+convert(varchar(3),@tintOrder)+ '.numListItemID='+@vcDbColumnName                                        
			  end                                        
			  else if @vcListItemType='S'                                         
			  begin                                        
			   set @strSql=@strSql+',S'+ convert(varchar(3),@tintOrder)+'.vcState'+' ['+ @vcFormFieldName+'~'+ @vcDbColumnName+']'                                        
			   set @WhereCondition= @WhereCondition +' left join State S'+ convert(varchar(3),@tintOrder)+ ' on S'+convert(varchar(3),@tintOrder)+ '.numStateID='+@vcDbColumnName                                        
			  end                                        
			  else if @vcListItemType='T'                                         
			  begin                                        
				  set @strSql=@strSql+',L'+ convert(varchar(3),@tintOrder)+'.vcData'+' ['+ @vcFormFieldName+'~'+ @vcDbColumnName+']'                                        
			   set @WhereCondition= @WhereCondition +' left Join ListDetails L'+ convert(varchar(3),@tintOrder)+ ' on L'+convert(varchar(3),@tintOrder)+ '.numListItemID='+@vcDbColumnName                                        
			  end       
			  else if   @vcListItemType='U'                                     
			 begin       
			   declare @Prefix as varchar(5)      
			   if @Table = 'Contacts'      
				set @Prefix = 'ADC.'      
			   if @Table = 'Division'      
				set @Prefix = 'DM.'      
			         
				set @strSql=@strSql+',dbo.fn_GetContactName('+@Prefix+@vcDbColumnName+') ['+ @vcFormFieldName+'~'+ @vcDbColumnName+']'                                        
			 end      
			 end                                        
			  else set @strSql=@strSql+','+ case  when @vcDbColumnName='numAge' then 'year(getutcdate())-year(bintDOB)' when @vcDbColumnName='numDivisionID' then 'DM.numDivisionID' else @vcDbColumnName end+' ['+ @vcFormFieldName+'~'+ @vcDbColumnName+']'              
end
else
begin
			
			select @vcFormFieldName=FLd_label,@vcAssociatedControlType=fld_type,@vcDbColumnName='Cust'+Convert(varchar(10),Fld_Id)               
			 from CFW_Fld_Master join CFW_Fld_Dtl                                
			on Fld_id=numFieldId                                
			left join CFw_Grp_Master                                 
			on subgrp=CFw_Grp_Master.Grp_id                                
			where CFW_Fld_Master.grp_id=4 and numRelation=@inttype and CFW_Fld_Master.numDomainID=@numDomainID  and CFW_Fld_Master.Fld_Id = @numFormFieldId 


			print @vcAssociatedControlType
			if @vcAssociatedControlType <> 'Drop Down list Box'
			begin
			
				set @strSql= @strSql+',CFW'+ convert(varchar(3),@tintOrder)+'.Fld_Value  ['+ @vcFormFieldName+'~'+ @vcDbColumnName+']'   
				set @WhereCondition= @WhereCondition +' left Join CFW_FLD_Values_Cont CFW'+ convert(varchar(3),@tintOrder)+ ' 
				on CFW'+ convert(varchar(3),@tintOrder)+ '.Fld_Id='+convert(varchar(10),@numFormFieldId) +'and CFW'+ convert(varchar(3),@tintOrder)+ '.RecId=ADC.numContactId   '                                         
			end
			if @vcAssociatedControlType = 'Drop Down list Box'
			begin
				set @vcDbColumnName = 'DCust'+Convert(varchar(10),@numFormFieldId)
				set @strSql=@strSql+',L'+ convert(varchar(3),@tintOrder)+'.vcData'+' ['+ @vcFormFieldName+'~'+ @vcDbColumnName+']'                                          
				set @WhereCondition= @WhereCondition +' left Join CFW_FLD_Values_Cont CFW'+ convert(varchar(3),@tintOrder)+ ' 
					on CFW'+ convert(varchar(3),@tintOrder)+ '.Fld_Id='+convert(varchar(10),@numFormFieldId) +'and CFW'+ convert(varchar(3),@tintOrder)+ '.RecId=ADC.numContactId   '                                         
				set @WhereCondition= @WhereCondition +' left Join ListDetails L'+ convert(varchar(3),@tintOrder)+ ' on L'+convert(varchar(3),@tintOrder)+ '.numListItemID=CFW'+ convert(varchar(3),@tintOrder)+'.Fld_Value'
			end	
end		  
    
                   
 if @DefaultNocolumns>0    
 begin                         
                                                 
  select top 1 @tintOrder=tintOrder+1,@vcDbColumnName=vcDbColumnName,@vcFormFieldName=vcFormFieldName,      
  @vcAssociatedControlType=vcAssociatedControlType,@vcListItemType=vcListItemType,@numListID=numListID  ,@Table=vcLookBackTableName       
,	@bitCustom=a.bitCustom,@numFormFieldId=A.numFormFieldId   
  from [InitialListColumnConf] A                                        
  join DynamicFormFieldMaster D                                         
  on D.numFormFieldId=A.numFormFieldId    
  where a.numFormId=10 and numUserCntID=@numUserCntID and A.numDomainID=@numDomainID  and numtype =@inttype and           
  tintOrder > @tintOrder-1 order by tintOrder asc                                        
  if @@rowcount=0 set @tintOrder=0      
 end                                      
 else    
 Begin    
  select top 1 @tintOrder=[Order]+1,@vcDbColumnName=vcDbColumnName,@vcFormFieldName=vcFormFieldName,      
  @vcAssociatedControlType=vcAssociatedControlType,@vcListItemType=vcListItemType,@numListID=numListID  ,@Table=vcLookBackTableName       
	,@bitCustom=0,@numFormFieldId=D.numFormFieldId   
  from DynamicFormFieldMaster D                                         
  where D.numFormId=10 and bitDefault=1  and         
  [Order] > @tintOrder-1 order by [Order] asc                                        
  if @@rowcount=0 set @tintOrder=0       
 End    
end             
          
          
                  
set @strSql=@strSql+' FROM AdditionalContactsInformation ADC                                                             
  JOIN DivisionMaster DM ON ADC.numDivisionId = DM.numDivisionID                                                            
  JOIN CompanyInfo CI ON DM.numCompanyID = CI.numCompanyId                                        
  left JOIN ContactAddress CA on CA.numContactID=ADC.numContactID '+@WhereCondition+      
' join tblcontacts T on T.numContactId=ADC.numContactId                                                                         
  WHERE RowNumber > '+convert(varchar(10),@firstRec)+ ' and RowNumber <'+ convert(varchar(10),@lastRec)+      
' union select count(*),null,null,null,null,null '+@strColumns+' from tblcontacts order by RowNumber'                                 
                                     
print @strSql              
      
exec (@strSql)
GO
