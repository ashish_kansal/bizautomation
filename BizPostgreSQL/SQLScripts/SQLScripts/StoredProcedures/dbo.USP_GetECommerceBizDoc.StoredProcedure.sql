/****** Object:  StoredProcedure [dbo].[USP_GetECommerceBizDoc]    Script Date: 07/26/2008 16:17:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created By Anoop Jayaraj    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getecommercebizdoc')
DROP PROCEDURE usp_getecommercebizdoc
GO
CREATE PROCEDURE [dbo].[USP_GetECommerceBizDoc]      
@numDomainId as numeric(9)=0      
As      
 Select isnull(numAuthoritativeSales,0) From AuthoritativeBizDocs Where numDomainId=@numDomainId
GO
