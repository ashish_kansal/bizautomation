/****** Object:  StoredProcedure [dbo].[USP_DeleteMasterTabs]    Script Date: 07/26/2008 16:15:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_deletemastertabs')
DROP PROCEDURE usp_deletemastertabs
GO
CREATE PROCEDURE [dbo].[USP_DeleteMasterTabs]
@numTabId as numeric(9)=0
as


delete from TabMaster where numTabId=@numTabId and bitFixed=0
GO
