GO
/****** Object:  StoredProcedure [dbo].[USP_ItemDisplayFieldsForBarcode]    Script Date: 15/08/2013 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--- Created By Manish Anjara
GO
/*
EXEC USP_ItemDisplayFieldsForBarcode 1,'696268,700870,701267,701272,721554,723915,734575,734676',0,1
*/
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_ItemDisplayFieldsForBarcode' ) 
    DROP PROCEDURE USP_ItemDisplayFieldsForBarcode
GO
CREATE PROCEDURE [dbo].[USP_ItemDisplayFieldsForBarcode]
    (
      @numDomainID AS NUMERIC(9) = 0,
      @strItemIdList AS TEXT,
      @bitAllItems AS BIT = 0,
      @numUserCntID AS NUMERIC(9) = 0	
    )
AS 
     
    DECLARE @join AS VARCHAR(8000)                  
    SET @join = ''                                                        
           
    DECLARE @Nocolumns AS TINYINT               
    SET @Nocolumns = 0                
 
    SELECT  @Nocolumns = ISNULL(SUM(TotalRow), 0)
    FROM    ( SELECT    COUNT(*) TotalRow
              FROM      View_DynamicColumns
              WHERE     numFormId = 67
                        --AND numUserCntID = @numUserCntID
                        AND numDomainID = @numDomainID
                        --AND tintPageType = 1
                        AND ISNULL(numRelCntType, 0) = 0
              UNION
              SELECT    COUNT(*) TotalRow
              FROM      View_DynamicCustomColumns
              WHERE     numFormId = 67
                        --AND numUserCntID = @numUserCntID
                        AND numDomainID = @numDomainID
                        --AND tintPageType = 1
                        AND ISNULL(numRelCntType, 0) = 0
            ) TotalRows
       
        
    IF @Nocolumns = 0 
        BEGIN
            INSERT  INTO DycFormConfigurationDetails
                    (
                      numFormID,
                      numFieldID,
                      intColumnNum,
                      intRowNum,
                      numDomainID,
                      numUserCntID,
                      numRelCntType,
                      tintPageType,
                      bitCustom,
                      intColumnWidth,
                      numViewID
                    )
                    SELECT  67,
                            numFieldId,
                            0,
                            Row_number() OVER ( ORDER BY tintRow DESC ),
                            @numDomainID,
                            @numUserCntID,
                            0,
                            1,
                            0,
                            intColumnWidth,
                            0
                    FROM    View_DynamicDefaultColumns
                    WHERE   numFormId = 67
                            AND bitDefault = 1
                            AND ISNULL(bitSettingField, 0) = 1
                            AND numDomainID = @numDomainID
                    ORDER BY tintOrder ASC 
        END

    DECLARE @bitLocation AS BIT  
    SELECT  @bitLocation = ( CASE WHEN COUNT(*) > 0 THEN 1
                                  ELSE 0
                             END )
    FROM    View_DynamicColumns
    WHERE   numFormId = 67
            AND numUserCntID = @numUserCntID
            AND numDomainID = @numDomainID
            AND tintPageType = 1
            AND bitCustom = 0
            AND ISNULL(bitSettingField, 0) = 1
            AND vcDbColumnName = 'vcWareHouse'
                                                                   
                                        
    DECLARE @strSql AS NVARCHAR(Max)
    DECLARE @strWhere AS NVARCHAR(Max)
    SET @strWhere = ' AND 1=1'
    
    IF @bitAllItems = 0 
    	SET @strWhere = @strWhere + ' AND numItemCode IN (SELECT Item FROM dbo.DelimitedSplit8K(''' + CONVERT(VARCHAR(MAX),@strItemIdList) + ''','',''))' 
    ELSE	
		SET @strWhere = @strWhere + ' AND charItemType = ''P'''
        
    SET @strSql = ' declare @numDomain numeric set @numDomain = ' + CONVERT(VARCHAR(15), @numDomainID) + '; 
					With tblItem AS (SELECT COUNT(*) OVER(PARTITION BY NULL) AS TotalRowCount,numItemCode'       
	           
    SET @strWhere = @strWhere + ' group by numItemCode '  
  
    IF @bitLocation = 1 
        SET @strWhere = @strWhere + '  ,vcWarehouse'
        
    SET @join = @join
        + ' LEFT JOIN WareHouseItems ON numItemID = numItemCode '
    SET @join = @join
        + ' LEFT JOIN Warehouses W ON W.numWareHouseID = WareHouseItems.numWareHouseID '
        
      
    SET @strSql = @strSql + ' FROM Item  ' + @join + '   
	LEFT JOIN ListDetails LD ON LD.numListItemID = Item.numShipClass
	LEFT JOIN ItemCategory IC ON IC.numItemID = Item.numItemCode
	WHERE Item.numDomainID= @numDomain
	AND ( WareHouseItems.numDomainID = ' + CONVERT(VARCHAR(15), @numDomainID) + ' OR WareHouseItems.numWareHouseItemID IS NULL) ' + @strWhere
    
    SET @strSql = @strSql + ')'
 
    SET @strSql = @strSql
        + ' select min(TotalRowCount) as TotalRowCount,I.numItemCode,I.vcItemName,I.txtItemDesc,I.charItemType,                                     
	case when charItemType=''P'' then ''Inventory Item'' when charItemType=''N'' then ''Non Inventory Item'' when charItemType=''S'' then ''Service'' when charItemType=''A'' then ''Accessory'' end as ItemType,                                      
	isnull(sum(numOnHand),0) as numOnHand,isnull(sum(numOnOrder),0) as numOnOrder,isnull(sum(numReorder),0) as numReorder,isnull(sum(numBackOrder),0) as numBackOrder,                  
	isnull(sum(numAllocation),0) as numAllocation,vcCompanyName,CAST(ISNULL(monAverageCost,0) AS DECIMAL(10,2)) AS monAverageCost ,CAST(ISNULL(monAverageCost,0) * SUM(numOnHand) AS DECIMAL(10,2)) AS monStockValue,I.vcModelID,
	Cast(CASE WHEN charItemType=''P'' THEN ISNULL(WareHouseItems.monWListPrice,0) 
	     WHEN charItemType=''N'' THEN ISNULL(I.monListPrice,0)  
	     WHEN charItemType=''S'' THEN ISNULL(I.monListPrice,0)
	     ELSE ISNULL(I.monListPrice,0)
	END as decimal(18,2)) AS monListPrice ,
	I.vcManufacturer,I.numBarCodeId,I.fltLength,I.fltWidth,I.fltHeight,I.fltWeight,I.vcSKU,LD.vcData AS numShipClass,
	ISNULL(I.numItemClassification,0) As [numItemClassification],ISNULL(LDC.vcData,'''') AS [ItemClassification],ISNULL(I.bitTaxable,0) As [bitTaxable],ISNULL(I.bitKitParent,0) [bitKitParent],
	ISNULL(I.numVendorID,0) [numVendorID],ISNULL(I.bitAllowBackOrder,0) [bitAllowBackOrder],ISNULL(I.bitSerialized,0) [bitSerialized],ISNULL(I.numItemGroup,0) [numItemGroup],
	ISNULL(I.monCampaignLabourCost,0) [monCampaignLabourCost],ISNULL(I.bitFreeShipping,0) [bitFreeShipping],ISNULL(I.vcUnitofMeasure,'''') [vcUnitofMeasure],
	ISNULL(I.bitLotNo,0) [bitLotNo],ISNULL(I.IsArchieve,0) [IsArchieve],ISNULL(I.numItemClass,0) [numItemClass],ISNULL(I.bitArchiveItem,0) [bitArchiveItem],
	ISNULL(UOM1.vcUnitName,'''') [vcBaseUnit],ISNULL(UOM2.vcUnitName,'''') [vcSaleUnit],ISNULL(UOM3.vcUnitName,'''') [vcPurchaseUnit],ISNULL(com.vcCompanyName,'''') [vcVendor],
	ISNULL(V.vcPartNo,'''') [vcPartNo],ISNULL(V.intMinQty,0) [intMinQty],ISNULL(I.numBaseUnit,0) [numBaseUnit],ISNULL(I.numSaleUnit,0) [numSaleUnit],
	ISNULL(I.numPurchaseUnit,0) [numPurchaseUnit],ISNULL(I.bitAssembly,0) As [bitAssembly],ISNULL(vcItemGroup,'''') AS [vcItemGroup] '
	
    IF @bitLocation = 1 
        SET @strSql = @strSql + '  ,vcWarehouse'  

    SET @strSql = @strSql
        + ' INTO #tblItem FROM tblItem JOIN ITEM I ON tblItem.numItemCode=I.numItemCode 
	left join WareHouseItems on numItemID=I.numItemCode                                  
	left join divisionmaster div  on I.numVendorid = div.numDivisionId                                    
	left join companyInfo com on com.numCompanyid=div.numcompanyID   
	left join Warehouses W  on W.numWareHouseID=WareHouseItems.numWareHouseID 
	LEFT JOIN ListDetails LD ON LD.numListItemID = i.numShipClass
	LEFT JOIN ListDetails LDC ON LDC.numListItemID = i.numItemClassification
	LEFT JOIN UOM UOM1 ON UOM1.numUOMID = I.numBaseUnit
	LEFT JOIN UOM UOM2 ON UOM2.numUOMID = I.numSaleUnit
	LEFT JOIN UOM UOM3 ON UOM3.numUOMID = I.numPurchaseUnit
	LEFT JOIN Vendor V ON V.numVendorID = I.numVendorID AND V.numVendorID = div.numDivisionId  
	LEFT JOIN dbo.ItemGroups IG ON  IG.numItemGroupID = I.numItemGroup  
	group by I.numItemCode,vcItemName,txtItemDesc,charItemType,vcCompanyName,vcModelID,monListPrice,vcManufacturer,numBarCodeId,fltLength,fltWidth,fltHeight
	,fltWeight,monAverageCost,I.vcSKU,LD.vcData,numItemClassification,bitTaxable,vcSKU,bitKitParent,I.numVendorID,bitAllowBackOrder,bitSerialized,numItemGroup,
	monCampaignLabourCost,bitFreeShipping,vcUnitofMeasure,bitLotNo,IsArchieve,numItemClass,bitArchiveItem,vcCompanyName,vcPartNo,intMinQty,UOM1.vcUnitName 
	,UOM2.vcUnitName,UOM3.vcUnitName,I.numBaseUnit,I.numSaleUnit,I.numPurchaseUnit,I.bitAssembly,vcItemGroup,LDC.vcData,WareHouseItems.monWListPrice '  
																																		   
    IF @bitLocation = 1																													   
        SET @strSql = @strSql + '  ,vcWarehouse'  
        
    --SET @strSql = @strSql + ' order by RunningCount '   
          
    DECLARE @tintOrder AS TINYINT                                                  
    DECLARE @vcFieldName AS VARCHAR(50)                                                  
    DECLARE @vcListItemType AS VARCHAR(3)                                             
    DECLARE @vcListItemType1 AS VARCHAR(1)                                                 
    DECLARE @vcAssociatedControlType VARCHAR(10)                                                  
    DECLARE @numListID AS NUMERIC(9)                                                  
    DECLARE @vcDbColumnName VARCHAR(20)                      
    DECLARE @WhereCondition VARCHAR(8000)                       
    DECLARE @vcLookBackTableName VARCHAR(2000)                
    DECLARE @bitCustom AS BIT                  
    DECLARE @numFieldId AS NUMERIC  
    DECLARE @bitAllowSorting AS CHAR(1)   
    DECLARE @bitAllowEdit AS CHAR(1)                   
                 
    SET @tintOrder = 0                                                  
    SET @WhereCondition = ''                 
   
    CREATE TABLE #tempForm
        (
          tintOrder TINYINT,
          vcDbColumnName NVARCHAR(50),
          vcFieldName NVARCHAR(50),
          vcAssociatedControlType NVARCHAR(50),
          vcListItemType CHAR(3),
          numListID NUMERIC(9),
          vcLookBackTableName VARCHAR(50),
          bitCustomField BIT,
          numFieldId NUMERIC,
          bitAllowSorting BIT,
          bitAllowEdit BIT,
          bitIsRequired BIT,
          bitIsEmail BIT,
          bitIsAlphaNumeric BIT,
          bitIsNumeric BIT,
          bitIsLengthValidation BIT,
          intMaxLength INT,
          intMinLength INT,
          bitFieldMessage BIT,
          vcFieldMessage VARCHAR(500),
          ListRelID NUMERIC(9)
        )
        

    INSERT  INTO #tempForm
            SELECT  tintRow + 1 AS tintOrder,
                    vcDbColumnName,
                    ISNULL(vcCultureFieldName, vcFieldName),
                    vcAssociatedControlType,
                    vcListItemType,
                    numListID,
                    vcLookBackTableName,
                    bitCustom,
                    numFieldId,
                    bitAllowSorting,
                    bitAllowEdit,
                    bitIsRequired,
                    bitIsEmail,
                    bitIsAlphaNumeric,
                    bitIsNumeric,
                    bitIsLengthValidation,
                    intMaxLength,
                    intMinLength,
                    bitFieldMessage,
                    vcFieldMessage vcFieldMessage,
                    ListRelID
            FROM    View_DynamicColumns
            WHERE   numFormId = 67
                    --AND numUserCntID = @numUserCntID
                    AND numDomainID = @numDomainID
                    --AND tintPageType = 1
                    AND ISNULL(bitSettingField, 0) = 1
                    AND ISNULL(bitCustom, 0) = 0
                    AND ISNULL(numRelCntType, 0) = 0
            UNION
            SELECT  tintRow + 1 AS tintOrder,
                    vcDbColumnName,
                    vcFieldName,
                    vcAssociatedControlType,
                    '' AS vcListItemType,
                    numListID,
                    '',
                    bitCustom,
                    numFieldId,
                    bitAllowSorting,
                    bitAllowEdit,
                    bitIsRequired,
                    bitIsEmail,
                    bitIsAlphaNumeric,
                    bitIsNumeric,
                    bitIsLengthValidation,
                    intMaxLength,
                    intMinLength,
                    bitFieldMessage,
                    vcFieldMessage,
                    ListRelID
            FROM    View_DynamicCustomColumns
            WHERE   numFormId = 67
                    --AND numUserCntID = @numUserCntID
                    AND numDomainID = @numDomainID
                    --AND tintPageType = 1
                    AND ISNULL(bitCustom, 0) = 1
                    AND ISNULL(numRelCntType, 0) = 0
            ORDER BY tintOrder ASC      
            
    
            
    SET @strSql = @strSql
        + ' select  TotalRowCount,temp.numItemCode,vcItemName,txtItemDesc,charItemType,
					CASE WHEN charItemType = ''P'' THEN ''Inventory Item'' 
						 WHEN charItemType = ''N'' THEN ''Non Inventory Item''
						 WHEN charItemType = ''A'' THEN ''Assembly Item''
						 WHEN charItemType = ''S'' THEN ''Service Item'' 
						 ELSE '''' 
					END AS [ItemType],numOnHand, LD.vcData AS numShipClass,numOnOrder,numReorder,numBackOrder,numAllocation,vcCompanyName,
				   vcModelID,monListPrice,vcManufacturer,numBarCodeId,fltLength,fltWidth,fltHeight,fltWeight,
				   (Select SUM(WO.numQtyItemsReq) from WorkOrder WO where WO.numItemCode=temp.numItemCode and WO.numWOStatus=0) as WorkOrder,
				   monAverageCost,monStockValue,vcSKU,numItemClassification,CASE WHEN bitTaxable = 1 THEN ''Yes'' ELSE ''No'' END AS bitTaxable,
				   CASE WHEN bitKitParent = 1 THEN ''Yes'' ELSE ''No'' END AS bitKitParent,numVendorID,
				   CASE WHEN bitAllowBackOrder = 1 THEN ''Yes'' ELSE ''No'' END AS bitAllowBackOrder,
				   CASE WHEN bitSerialized = 1 THEN ''Yes'' ELSE ''No'' END AS bitSerialized,numItemGroup,vcItemGroup,monCampaignLabourCost,
				   CASE WHEN bitFreeShipping = 1 THEN ''Yes'' ELSE ''No'' END AS bitFreeShipping,vcUnitofMeasure,
				   CASE WHEN bitLotNo = 1 THEN ''Yes'' ELSE ''No'' END AS bitLotNo,CASE WHEN IsArchieve = 1 THEN ''Yes'' ELSE ''No'' END AS IsArchieve,numItemClass,
				   CASE WHEN bitArchiveItem = 1 THEN ''Yes'' ELSE ''No'' END AS bitArchiveItem,vcBaseUnit,vcSaleUnit,vcPurchaseUnit,vcVendor,vcPartNo,intMinQty,
				   CASE WHEN bitAssembly = 1 THEN ''Yes'' ELSE ''No'' END AS bitAssembly,numBaseUnit,numSaleUnit,numPurchaseUnit,ItemClassification '     
  
    IF @bitLocation = 1 
        SET @strSql = @strSql + '  ,vcWarehouse'                                               

    DECLARE @ListRelID AS NUMERIC(9) 
  
    SELECT TOP 1
            @tintOrder = tintOrder,
            @vcDbColumnName = vcDbColumnName,
            @vcFieldName = vcFieldName,
            @vcAssociatedControlType = vcAssociatedControlType,
            @vcListItemType = vcListItemType,
            @numListID = numListID,
            @vcLookBackTableName = vcLookBackTableName,
            @bitCustom = bitCustomField,
            @numFieldId = numFieldId,
            @bitAllowSorting = bitAllowSorting,
            @bitAllowEdit = bitAllowEdit,
            @ListRelID = ListRelID
    FROM    #tempForm --WHERE bitCustomField=1
    ORDER BY tintOrder ASC            

    WHILE @tintOrder > 0                                                  
        BEGIN                                                  
            IF @bitCustom = 0 
                BEGIN
                    PRINT @vcDbColumnName
                    IF @vcDbColumnName = 'vcPathForTImage' 
                        BEGIN
                            SET @strSql = @strSql
                                + ',ISNULL(II.vcPathForTImage,'''') AS [vcPathForTImage]'                   
                            SET @WhereCondition = @WhereCondition
                                + ' LEFT JOIN ItemImages II ON II.numItemCode = temp.numItemCode AND bitDefault = 1'
                        END
                END                              
			   
            ELSE 
	            PRINT @vcDbColumnName
                IF @bitCustom = 1 
                    BEGIN      
                  
                        SELECT  @vcFieldName = FLd_label,
                                @vcAssociatedControlType = fld_type,
                                @vcDbColumnName = 'Cust'
                                + CONVERT(VARCHAR(10), Fld_Id)
                        FROM    CFW_Fld_Master
                        WHERE   CFW_Fld_Master.Fld_Id = @numFieldId                 
     
              
--    print @vcAssociatedControlType                
                        IF @vcAssociatedControlType = 'TextBox'
                            OR @vcAssociatedControlType = 'TextArea' 
                            BEGIN                
                   
                                SET @strSql = @strSql + ',CFW'
                                    + CONVERT(VARCHAR(3), @tintOrder)
                                    + '.Fld_Value  [' + @vcDbColumnName + ']'                   
                                SET @WhereCondition = @WhereCondition
                                    + ' left Join CFW_FLD_Values_Item CFW'
                                    + CONVERT(VARCHAR(3), @tintOrder)
                                    + '                 
    on CFW' + CONVERT(VARCHAR(3), @tintOrder) + '.Fld_Id='
                                    + CONVERT(VARCHAR(10), @numFieldId)
                                    + 'and CFW'
                                    + CONVERT(VARCHAR(3), @tintOrder)
                                    + '.RecId=temp.numItemCode   '                                                         
                            END   
                        ELSE 
                            IF @vcAssociatedControlType = 'CheckBox' 
                                BEGIN            
               
                                    SET @strSql = @strSql
                                        + ',case when isnull(CFW'
                                        + CONVERT(VARCHAR(3), @tintOrder)
                                        + '.Fld_Value,0)=0 then ''No'' when isnull(CFW'
                                        + CONVERT(VARCHAR(3), @tintOrder)
                                        + '.Fld_Value,0)=1 then ''Yes'' end   [' + @vcDbColumnName + ']'              
 
                                    SET @WhereCondition = @WhereCondition
                                        + ' left Join CFW_FLD_Values_Item CFW'
                                        + CONVERT(VARCHAR(3), @tintOrder)
                                        + '             
    on CFW' + CONVERT(VARCHAR(3), @tintOrder) + '.Fld_Id='
                                        + CONVERT(VARCHAR(10), @numFieldId)
                                        + 'and CFW'
                                        + CONVERT(VARCHAR(3), @tintOrder)
                                        + '.RecId=temp.numItemCode   '                                                     
                                END                
                            ELSE 
                                IF @vcAssociatedControlType = 'DateField' 
                                    BEGIN              
                   
                                        SET @strSql = @strSql
                                            + ',dbo.FormatedDateFromDate(CFW'
                                            + CONVERT(VARCHAR(3), @tintOrder)
                                            + '.Fld_Value,'
                                            + CONVERT(VARCHAR(10), @numDomainId)
                                            + ')  [' + @vcDbColumnName + ']'                   
                                        SET @WhereCondition = @WhereCondition
                                            + ' left Join CFW_FLD_Values_Item CFW'
                                            + CONVERT(VARCHAR(3), @tintOrder)
                                            + '                 
    on CFW' + CONVERT(VARCHAR(3), @tintOrder) + '.Fld_Id='
                                            + CONVERT(VARCHAR(10), @numFieldId)
                                            + 'and CFW'
                                            + CONVERT(VARCHAR(3), @tintOrder)
                                            + '.RecId=temp.numItemCode   '                                                         
                                    END                
                                ELSE 
                                    IF @vcAssociatedControlType = 'SelectBox' 
                                        BEGIN                
                                            SET @vcDbColumnName = 'Cust'
                                                + CONVERT(VARCHAR(10), @numFieldId)                
                                            SET @strSql = @strSql + ',L'
                                                + CONVERT(VARCHAR(3), @tintOrder)
                                                + '.vcData' + ' [' + @vcDbColumnName + ']'                                                          
                                            SET @WhereCondition = @WhereCondition
                                                + ' left Join CFW_FLD_Values_Item CFW'
                                                + CONVERT(VARCHAR(3), @tintOrder)
                                                + '                 
     on CFW' + CONVERT(VARCHAR(3), @tintOrder) + '.Fld_Id='
                                                + CONVERT(VARCHAR(10), @numFieldId)
                                                + 'and CFW'
                                                + CONVERT(VARCHAR(3), @tintOrder)
                                                + '.RecId=temp.numItemCode    '                                                         
                                            SET @WhereCondition = @WhereCondition
                                                + ' left Join ListDetails L'
                                                + CONVERT(VARCHAR(3), @tintOrder)
                                                + ' on L'
                                                + CONVERT(VARCHAR(3), @tintOrder)
                                                + '.numListItemID=CFW'
                                                + CONVERT(VARCHAR(3), @tintOrder)
                                                + '.Fld_Value'                
                                        END                 
                    END          
  
            SELECT TOP 1
                    @tintOrder = tintOrder ,
                    @vcDbColumnName = vcDbColumnName,
                    @vcFieldName = vcFieldName,
                    @vcAssociatedControlType = vcAssociatedControlType,
                    @vcListItemType = vcListItemType,
                    @numListID = numListID,
                    @vcLookBackTableName = vcLookBackTableName,
                    @bitCustom = bitCustomField,
                    @numFieldId = numFieldId,
                    @bitAllowSorting = bitAllowSorting,
                    @bitAllowEdit = bitAllowEdit,
                    @ListRelID = ListRelID
            FROM    #tempForm
            WHERE   tintOrder > @tintOrder --AND bitCustomField=1
            ORDER BY tintOrder ASC            
 
            IF @@rowcount = 0 
                SET @tintOrder = 0 
            
        END                       
      
    PRINT @bitLocation
    SET @strSql = @strSql
        + ' from #tblItem temp
			LEFT JOIN ListDetails LD ON LD.vcData = temp.numShipClass AND LD.numListID=461 and LD.numDomainID = '
        + CONVERT(VARCHAR(20), @numDomainID) + ' ' + @WhereCondition

    SET @strSql = REPLACE(@strSql, '|', ',') 
    PRINT @strSql
    EXEC ( @strSql
        )  

    UPDATE  #tempForm
    SET     vcDbColumnName = CASE WHEN bitCustomField = 1
                                  THEN vcDbColumnName
                                  ELSE vcDbColumnName
                             END 
    SELECT  *
    FROM    #tempForm

    DROP TABLE #tempForm

