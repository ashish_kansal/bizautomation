/****** Object:  StoredProcedure [dbo].[USP_ProjectsDTLPL]    Script Date: 07/26/2008 16:20:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_projectsdtlpl')
DROP PROCEDURE usp_projectsdtlpl
GO
CREATE PROCEDURE [dbo].[USP_ProjectsDTLPL]                                   
(                                    
@numProId numeric(9)=null  ,                  
@numDomainID numeric(9),    
@ClientTimeZoneOffset as int                                     
)                                    
as                                    
                                    
begin                                    
                                                    
                                    
 select  pro.vcProjectName,
		 pro.numintPrjMgr,
		 dbo.fn_GetContactName(pro.numintPrjMgr) as numintPrjMgrName,                              
		 pro.intDueDate,                                    
		 dbo.fn_GetContactName(pro.numCustPrjMgr) as numCustPrjMgrName,
		 pro.numCustPrjMgr,                                    
         pro.txtComments,
		 [dbo].[fn_GetContactName](pro.numAssignedTo ) as numAssignedToName, 
		 pro.numAssignedTo,                  
        (select  count(*) from GenericDocuments   where numRecID=@numProId and  vcDocumentSection='P') as DocumentCount,
		CASE 
           WHEN tintProStatus = 1 THEN 100
           ELSE isnull((SELECT SUM(tintPercentage)
                 FROM   ProjectsStageDetails
                 WHERE  numProId = @numProId
                        AND bitStageCompleted = 1),0)
         END AS TProgress,
         (select  count(*) from [ProjectsOpportunities] PO Left JOIN [OpportunityMaster] OM ON OM.[numOppId] = PO.[numOppId] WHERE [numProId]=@numProId) as LinkedOrderCount,
         vcProjectID,
         '' TimeAndMaterial,
         numContractId,
         ISNULL((SELECT vcContractName FROM dbo.ContractManagement WHERE numContractId=pro.numContractId),'') numContractIdName,
         isnull(numProjectType,0) numProjectType,
         dbo.fn_GetListItemName(numProjectType) vcProjectTypeName,isnull(Pro.numProjectStatus,0) as numProjectStatus,
dbo.fn_GetListItemName(numProjectStatus) vcProjectStatusName,
(SELECT SUBSTRING(
	(SELECT ','+  A.vcFirstName+' ' +A.vcLastName + CASE WHEN ISNULL(SR.numContactType,0)>0 THEN '(' + dbo.fn_GetListItemName(SR.numContactType) + ')' ELSE '' END
	FROM ShareRecord SR JOIN UserMaster UM ON UM.numUserDetailId=SR.numAssignedTo 
							 JOIN AdditionalContactsInformation A ON UM.numUserDetailId=A.numContactID 
		 WHERE SR.numDomainID=pro.numDomainID AND SR.numModuleID=5 AND SR.numRecordID=pro.numProId
				AND UM.numDomainID=pro.numDomainID and UM.numDomainID=A.numDomainID FOR XML PATH('')),2,200000)) AS numShareWith ,
	isnull(AD.vcStreet,'')+ 
	' <br/>'+ isnull(AD.vcCity,'')+ ' ,'
	         + isnull(dbo.fn_GetState(AD.numState),'')+ ' '
	         + isnull(AD.vcPostalCode,'')+ ' <br>' 
	         + isnull(dbo.fn_GetListItemName(AD.numCountry),'') as ShippingAddress,
pro.dtCompletionDate AS vcProjectedFinish,
NULL AS dtActualStartDate,
DATEADD(MINUTE,-1 * @ClientTimeZoneOffset,pro.dtmEndDate) dtmEndDate,
DATEADD(MINUTE,-1 * @ClientTimeZoneOffset,pro.dtmStartDate) dtmStartDate,
ISNULL(pro.numClientBudget,0) AS numClientBudget,
0 AS numClientBudgetBalance
from ProjectsMaster pro    
LEFT JOIN dbo.AddressDetails AD ON AD.numAddressID = pro.numAddressID                                 
where numProId=@numProId     and pro.numdomainID=  @numDomainID                             
                                    
end
GO
