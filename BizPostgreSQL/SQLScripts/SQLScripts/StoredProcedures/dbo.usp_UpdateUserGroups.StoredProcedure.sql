/****** Object:  StoredProcedure [dbo].[usp_UpdateUserGroups]    Script Date: 07/26/2008 16:21:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_updateusergroups')
DROP PROCEDURE usp_updateusergroups
GO
CREATE PROCEDURE [dbo].[usp_UpdateUserGroups]
	@numGroupID numeric, 
	@numUserID numeric   
--
AS
BEGIN
	DECLARE @numRecCount numeric
	SELECT @numRecCount=COUNT(numGroupID) FROM UserGroups
		WHERE numGroupID=@numGroupID
		AND numUserID=@numUserID

	IF @numRecCount=0
	BEGIN
		INSERT INTO UserGroups 
			VALUES(@numGroupID, @numUserID)
	END

END
GO
