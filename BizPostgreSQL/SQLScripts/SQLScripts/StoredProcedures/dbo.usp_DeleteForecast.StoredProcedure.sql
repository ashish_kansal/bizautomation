/****** Object:  StoredProcedure [dbo].[usp_DeleteForecast]    Script Date: 07/26/2008 16:15:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_deleteforecast')
DROP PROCEDURE usp_deleteforecast
GO
CREATE PROCEDURE [dbo].[usp_DeleteForecast]
	@numForecastID1 numeric(9),
	@numForecastID2 numeric(9),
	@numForecastID3 numeric(9),
	@numUserID numeric(9),
	@numDomainID numeric(9)=0 
--  
 AS

	DELETE from Forecast where numForecastID in (@numForecastID1,@numForecastID2,@numForecastID3) and numCreatedBy=@numUserID
	and numDomainID=@numDomainID
GO
