/****** Object:  StoredProcedure [dbo].[USP_updateProfileListOrder]    Script Date: 07/26/2008 16:21:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_updateprofilelistorder')
DROP PROCEDURE usp_updateprofilelistorder
GO
CREATE PROCEDURE [dbo].[USP_updateProfileListOrder]
@xmlStr as NText
as 




DECLARE @tblTemp TABLE (    
        numRelProID numeric(9),     
        tintOrder numeric(9)    
         
    )    
                           
       
                                                                           
DECLARE @hDoc4 int                   --inserting data in temp table                                                   
EXEC sp_xml_preparedocument @hDoc4 OUTPUT, @xmlStr    
      
insert into @tblTemp     
select* from (                            
SELECT * FROM OPENXML (@hDoc4,'/NewDataSet/Table',2)                                                                      
 WITH  (                                                                  
  numRelProID numeric(9),                              
  tintOrder numeric(9)                              
     
))X                               
                                
                
EXEC sp_xml_removedocument @hDoc4        
--inserting data from temp table to main table(listdetails)    
UPDATE RelProfile SET numorder = t.tintOrder FROM  @tblTemp t, RelProfile dtl      
 WHERE dtl.numRelProID = t.numRelProID
GO
