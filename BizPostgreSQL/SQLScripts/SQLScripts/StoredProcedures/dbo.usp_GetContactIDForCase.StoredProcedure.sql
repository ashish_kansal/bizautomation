/****** Object:  StoredProcedure [dbo].[usp_GetContactIDForCase]    Script Date: 07/26/2008 16:16:48 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getcontactidforcase')
DROP PROCEDURE usp_getcontactidforcase
GO
CREATE PROCEDURE [dbo].[usp_GetContactIDForCase]
	@numDivisionID numeric=0   
--
AS
SELECT DISTINCT 
                      
                      AdditionalContactsInformation.numContactId
FROM         OpportunityMaster INNER JOIN
                      AdditionalContactsInformation ON OpportunityMaster.numContactId = AdditionalContactsInformation.numContactId
WHERE     (OpportunityMaster.numDivisionId =@numDivisionID)
GO
