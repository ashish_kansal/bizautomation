/****** Object:  StoredProcedure [dbo].[Resource_Upd]    Script Date: 07/26/2008 16:14:35 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
/*
**  Updates an existing Resource by its primary key number.
**
**  This stored procedure is not ordinarily required by interactive users.
**  It supports "administrative" mode applications where a Resource
**  row must be modified.
*/
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='resource_upd')
DROP PROCEDURE resource_upd
GO
CREATE PROCEDURE [dbo].[Resource_Upd]
	@ResourceID		integer,	-- the key of the Resource to be updated
	@ResourceName		nvarchar(64),	-- changed name of the Resource
	@ResourceDesc		ntext,		-- changed description of the Resource
	@ResourceEmail		nvarchar(64),	-- changed e-mail contact information of the Resource
	@EnableEmailReminders	integer		-- resource preference for e-mail reminders
AS
BEGIN
	BEGIN TRANSACTION

	DECLARE @ResourcePreferenceID integer;

	UPDATE
		[Resource]
	SET 
		[ResourceName] = @ResourceName,
		[ResourceDescription] = @ResourceDesc,
		[EmailAddress] = @ResourceEmail
	WHERE
		( [ResourceID] = @ResourceID );

	SELECT
		@ResourcePreferenceID = [ResourcePreference].[ResourceID]
	FROM
		[ResourcePreference]
	WHERE
		( [ResourcePreference].[ResourceID] = @ResourceID );

	IF ( @ResourcePreferenceID IS NOT NULL )
	BEGIN
		UPDATE 
			[ResourcePreference]
		SET
			[EnableEmailReminders] = @EnableEmailReminders
		WHERE
			( [ResourceID] = @ResourcePreferenceID );
	END
	ELSE IF ( @EnableEmailReminders <> 0 )
	BEGIN
		INSERT INTO [ResourcePreference] (
			[ResourceID],
			[EnableEmailReminders]
		) VALUES (
			@ResourceID,
			@EnableEmailReminders
		);
	END

	COMMIT	
END
GO
