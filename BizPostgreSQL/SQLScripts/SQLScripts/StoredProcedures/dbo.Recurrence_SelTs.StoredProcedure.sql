/****** Object:  StoredProcedure [dbo].[Recurrence_SelTs]    Script Date: 07/26/2008 16:14:31 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
/*
**  Gets the timestamp of an individual Recurrence.
**
**  This stored procedure can be used to retrieve the timestamp of
**  a Recurrence by it's key, ID.
*/
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='recurrence_selts')
DROP PROCEDURE recurrence_selts
GO
CREATE PROCEDURE [dbo].[Recurrence_SelTs]
	@RecurrenceID		integer		-- primary key number of the recurrence
AS
BEGIN
	SELECT
		[_ts] 
	FROM 
		[Recurrence]
	WHERE
		( [Recurrence].[RecurrenceID] = @RecurrenceID );
END
GO
