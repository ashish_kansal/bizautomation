/****** Object:  StoredProcedure [dbo].[USP_ProjectList]    Script Date: 07/26/2008 16:20:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj                                
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_projectlist')
DROP PROCEDURE usp_projectlist
GO
CREATE PROCEDURE [dbo].[USP_ProjectList]                                    
 @numUserCntID NUMERIC(9)=0,                                    
 @numDomainID NUMERIC(9)=0,                                    
 @tintUserRightType TINYINT=0,                                    
 @tintSortOrder TINYINT=4,                                          
 @SortChar CHAR(1)='0',                                    
 @FirstName VARCHAR(100)= '',                                    
 @LastName VARCHAR(100)='',                                    
 @CustName VARCHAR(100)='' ,                                    
 @CurrentPage INT,                                    
 @PageSize INT,                                    
 @TotRecs INT OUTPUT,                                    
 @columnName AS VARCHAR(50),                                    
 @columnSortOrder AS VARCHAR(10),                                    
 @numDivisionID AS NUMERIC(9) ,                                   
 @bitPartner AS BIT=0,
 @ClientTimeZoneOffset AS INT,
 @numProjectStatus AS NUMERIC(9),
 @bitOpenProjects BIT=0,
 @intMode TINYINT = 0
                                 
AS                         

DECLARE @strSql AS VARCHAR(8000)                                                            
DECLARE @firstRec AS INTEGER                                     
DECLARE @lastRec AS INTEGER   

IF @intMode = 0
BEGIN
 
	DECLARE @column AS VARCHAR(50)           
	SET @column = @columnName                  
	DECLARE @join AS VARCHAR(400)            
	SET @join = ''           
	DECLARE @lookbckTable AS VARCHAR(50)                
	SET @lookbckTable = ''            
	            
	IF @column LIKE 'CFW.Cust%'           
	BEGIN          
	          
	 SET @column='CFW.Fld_Value'          
	 SET @join= ' left Join CFW_FLD_Values_Pro CFW on CFW.RecId=pro.numProid  and CFW.fld_id= '+REPLACE(@columnName,'CFW.Cust','')                                                    
	          
	            
	END                                   
	IF @Column LIKE 'DCust%'          
	BEGIN          
	          
	 SET @column='LstCF.vcData'                                                    
	 SET @join= ' left Join CFW_FLD_Values_Pro CFW on CFW.RecId=pro.numProid and CFW.fld_id= '+REPLACE(@columnName,'DCust','')                                                                                             
	 SET @join= @join +' left Join ListDetails LstCF on LstCF.numListItemID=CFW.Fld_Value'          
	          
	END           
	                                                               
	SET @firstRec= (@CurrentPage-1) * @PageSize                                                              
	SET @lastRec= (@CurrentPage*@PageSize+1)           

	DECLARE @strShareRedordWith AS VARCHAR(300);
	SET @strShareRedordWith=' pro.numProId in (select SR.numRecordID FROM ShareRecord SR WHERE SR.numDomainID='+ CONVERT(VARCHAR(15),@numDomainID )+' AND SR.numModuleID=5 AND SR.numAssignedTo='+CONVERT(VARCHAR(15),@numUserCntId)+') '
	                                              
	SET @strSql=''       
	SET @strSql='
	DECLARE @numDomain NUMERIC
	SET @numDomain = '+ CONVERT(VARCHAR(15),@numDomainID)+';

	with tblProjects as (SELECT '                
	IF (@tintSortOrder=5 OR @tintSortOrder=6) 
		SET @strSql=@strSql+ ' top 20 '                

	SET  @strSql= @strSql+ ' ROW_NUMBER() OVER (ORDER BY ' + @column + ' '+ @columnSortOrder+') AS RowNumber,pro.numProId,
							 COUNT(*) OVER(PARTITION BY NULL) AS TotalRowCount                            
							 FROM ProjectsMaster pro'                              
    
    IF @bitPartner=1 SET @strSql=@strSql+' left join ProjectsContacts ProCont on ProCont.numProId=Pro.numProId         
										   and ProCont.bitPartner=1 and ProCont.numContactId='+CONVERT(VARCHAR(15),@numUserCntID)+ ''                              
    SET @strSql=@strSql+' join additionalContactsinformation addCon on addCon.numContactId = pro.numCustPrjMgr                                              
						  join DivisionMaster Div on Div.numDivisionID=addCon.numDivisionID                                              
						  join CompanyInfo cmp on cmp.numCompanyID=div.numCompanyID 
						  left join ProjectProgress PP on PP.numProID=pro.numProID ' + @join                                                                        
	SET @strSql=@strSql+' where tintProStatus = 0 and Div.numDomainID=@numDomain '                                  

    IF @FirstName<>'' SET @strSql=@strSql + 'and addCon.vcFirstName  like '''+ISNULL(@FirstName,'')+'%'''                                              
	IF @LastName<>'' SET @strSql=@strSql+ 'and addCon.vcLastName like '''+ISNULL(@LastName,'')+'%'''                                    
	IF @CustName<>'' SET @strSql=@strSql+ 'and cmp.vcCompanyName like '''+ISNULL(@CustName,'')+'%'''                                                
	 
	--SET @strSql = @strSql + 'and (isnull(pro.numProjectStatus,0) = '+ convert(varchar(10),@numProjectStatus) + ' OR (' + convert(varchar(10),@numProjectStatus) + ' = 0 and isnull(pro.numProjectStatus,0) <> 27492))'
	SET @strSql = @strSql + 'and (isnull(pro.numProjectStatus,0) = '+ CONVERT(VARCHAR(10),@numProjectStatus) + ' OR ' + CONVERT(VARCHAR(10),@numProjectStatus) +'=0)'
	IF @bitOpenProjects=1 SET  @strSql=@strSql + 'and isnull(pro.numProjectStatus,0) <> 27492'       
	                                       
	IF @numDivisionID <>0 SET @strSql=@strSql + ' And div.numDivisionID =' + CONVERT(VARCHAR(15),@numDivisionID)                                               
	IF @SortChar<>'0' SET @strSql=@strSql + ' And Pro.vcProjectName like '''+@SortChar+'%'''                                               
	IF @tintUserRightType=1 SET @strSql=@strSql + ' AND (Pro.numRecOwner = '+CONVERT(VARCHAR(15),@numUserCntID)+' or Pro.numAssignedTo = '+CONVERT(VARCHAR(15),@numUserCntID) +' or  Pro.numProId in (select distinct(numProID) from ProjectsStageDetails         
	where numAssignTo ='+ CONVERT(VARCHAR(15),@numUserCntID)+')' + CASE WHEN @bitPartner=1 THEN ' or ( ProCont.bitPartner=1 and ProCont.numContactId='+CONVERT(VARCHAR(15),@numUserCntID)+ ')' ELSE '' END  + ' or ' + @strShareRedordWith +')'                                                
	ELSE IF @tintUserRightType=2 SET @strSql=@strSql + ' and  (Div.numTerID in (select numTerritoryID from  UserTerritory where numUserCntID= '+CONVERT(VARCHAR(15),@numUserCntID)+' ) or Div.numTerID=0 or Pro.numAssignedTo= '+ CONVERT(VARCHAR(15),@numUserCntID
	)  +' or  Pro.numProId in (select distinct(numProID) from ProjectsStageDetails                     
	where numAssignTo ='+ CONVERT(VARCHAR(15),@numUserCntID)+')' + ' or ' + @strShareRedordWith +')'                                             
	                  
	IF @tintSortOrder=1  SET @strSql=@strSql + ' AND (Pro.numRecOwner= '+ CONVERT(VARCHAR(15),@numUserCntID)  +' or Pro.numAssignedTo= '+ CONVERT(VARCHAR(15),@numUserCntID)  +' or  Pro.numProId in (select distinct(numProID) from ProjectsStageDetails          
	where numAssignTo ='+ CONVERT(VARCHAR(15),@numUserCntID)+')' + ' or ' + @strShareRedordWith +')'                   
	ELSE IF @tintSortOrder=2  SET @strSql=@strSql  + ' AND (Pro.numRecOwner in (select A.numContactID from AdditionalContactsInformation A                                                         
	where A.numManagerID='+ CONVERT(VARCHAR(15),@numUserCntID)+') or Pro.numAssignedTo in (select A.numContactID from AdditionalContactsInformation A                                                         
	where A.numManagerID='+ CONVERT(VARCHAR(15),@numUserCntID)+') or  Pro.numProId in (select distinct(numProID) from ProjectsStageDetails                     
	where numAssignTo in (select A.numContactID from AdditionalContactsInformation A                                                         
	where A.numManagerID='+ CONVERT(VARCHAR(15),@numUserCntID)+'))' + ' or ' + @strShareRedordWith +')'                                           
	ELSE IF @tintSortOrder=4  SET @strSql=@strSql + ' AND Pro.bintCreatedDate > '''+CONVERT(VARCHAR(20),DATEADD(DAY,-8,GETUTCDATE()))+''''                                              
	ELSE IF @tintSortOrder=6  SET @strSql=@strSql + ' and Pro.numModifiedBy= '+ CONVERT(VARCHAR(15),@numUserCntID)        
	                                          
	                                                
	ELSE IF @tintSortOrder=4  SET @strSql=@strSql + ' and Pro.numRecOwner= '+ CONVERT(VARCHAR(15),@numUserCntID)                                            
	ELSE IF @tintSortOrder=5  SET @strSql=@strSql + ' and Pro.numRecOwner= '+ CONVERT(VARCHAR(15),@numUserCntID)                                             
	 
	--IF @vcRegularSearchCriteria<>'' set @strSql=@strSql+' and ' + @vcRegularSearchCriteria 
	--IF @vcCustomSearchCriteria<>'' set @strSql=@strSql+' and pro.numProid in (select distinct CFW.RecId from CFW_FLD_Values_Pro CFW where ' + @vcCustomSearchCriteria + ')'
	                                                    
		 SET @strSql=@strSql + ')'            
	                                    
	                                                               
	DECLARE @tintOrder AS TINYINT                                                    
	DECLARE @vcFieldName AS VARCHAR(50)                                                    
	DECLARE @vcListItemType AS VARCHAR(3)                                               
	DECLARE @vcListItemType1 AS VARCHAR(1)                                                   
	DECLARE @vcAssociatedControlType VARCHAR(20)                                                    
	DECLARE @numListID AS NUMERIC(9)                                                    
	DECLARE @vcDbColumnName VARCHAR(20)                        
	DECLARE @WhereCondition VARCHAR(2000)                         
	DECLARE @vcLookBackTableName VARCHAR(2000)                  
	DECLARE @bitCustom AS BIT  
	DECLARE @bitAllowEdit AS CHAR(1)                 
	DECLARE @numFieldId AS NUMERIC  
	DECLARE @bitAllowSorting AS CHAR(1) 
	DECLARE @vcColumnName AS VARCHAR(500)    
	                  
	SET @tintOrder=0                                                    
	SET @WhereCondition =''                   
	                     
	DECLARE @Nocolumns AS TINYINT                  
	SET @Nocolumns=0                  

	SELECT @Nocolumns=ISNULL(SUM(TotalRow),0)  FROM(            
	SELECT COUNT(*) TotalRow FROM View_DynamicColumns 
	WHERE numFormId=82 
	--AND numUserCntID=@numUserCntID 
	AND numDomainID=@numDomainID 
	AND tintPageType=1
	UNION 
	SELECT COUNT(*) TotalRow FROM View_DynamicCustomColumns 
	WHERE numFormId=82 
	--AND numUserCntID=@numUserCntID 
	AND numDomainID=@numDomainID 
	AND tintPageType=1
	) TotalRows


	 CREATE TABLE #tempForm (tintOrder TINYINT,vcDbColumnName NVARCHAR(50),vcOrigDbColumnName NVARCHAR(50),vcFieldName NVARCHAR(50),vcAssociatedControlType NVARCHAR(50),
	  vcListItemType VARCHAR(3),numListID NUMERIC(9),vcLookBackTableName VARCHAR(50),bitCustomField BIT,
	  numFieldId NUMERIC,bitAllowSorting BIT,bitAllowEdit BIT,bitIsRequired BIT,bitIsEmail BIT,bitIsAlphaNumeric BIT,bitIsNumeric BIT,
	  bitIsLengthValidation BIT,intMaxLength INT,intMinLength INT,bitFieldMessage BIT,vcFieldMessage VARCHAR(500),ListRelID NUMERIC(9),intColumnWidth INT,bitAllowFiltering BIT,vcFieldDataType CHAR(1))
	                
	SET @strSql=@strSql+' select ADC.numContactId,DM.numDivisionID,isnull(DM.numTerID,0) as numTerID,pro.numCustPrjMgr,DM.tintCRMType,pro.numProId,RowNumber,Pro.numRecOwner'                  
	   
	IF @Nocolumns=0
	BEGIN
	INSERT INTO DycFormConfigurationDetails (numFormID,numFieldID,intColumnNum,intRowNum,numDomainID,numUserCntID,numRelCntType,tintPageType,bitCustom,intColumnWidth)
	SELECT 82,numFieldId,0,ROW_NUMBER() OVER(ORDER BY tintRow DESC),@numDomainID,@numUserCntID,NULL,1,0,intColumnWidth
	 FROM View_DynamicDefaultColumns
	  WHERE numFormId=82 AND bitDefault=1 AND ISNULL(bitSettingField,0)=1 AND numDomainID=@numDomainID                
	  ORDER BY tintOrder ASC 

	END
	   
	INSERT INTO #tempForm
	SELECT  tintRow+1 AS tintOrder,vcDbColumnName,vcOrigDbColumnName,ISNULL(vcCultureFieldName,vcFieldName), vcAssociatedControlType,vcListItemType,
			numListID,vcLookBackTableName ,bitCustom,numFieldId,bitAllowSorting,bitAllowEdit,bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,
			bitIsLengthValidation,intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage vcFieldMessage,ListRelID,intColumnWidth,bitAllowFiltering,
			vcFieldDataType  
	FROM View_DynamicColumns 
	WHERE numFormId=82 
	--AND numUserCntID=@numUserCntID 
	AND numDomainID=@numDomainID 
	AND tintPageType=1 
	AND ISNULL(bitSettingField,0)=1 AND ISNULL(bitCustom,0)=0
	       
	UNION
	    
	SELECT tintRow+1 AS tintOrder,vcDbColumnName,vcDbColumnName,vcFieldName,vcAssociatedControlType,'' AS vcListItemType,numListID,'',bitCustom,numFieldId,
		   bitAllowSorting,bitAllowEdit,bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,intMaxLength,intMinLength,
		   bitFieldMessage,vcFieldMessage,ListRelID,intColumnWidth,0,'' 
	FROM View_DynamicCustomColumns
	WHERE numFormId=82 
	--AND numUserCntID=@numUserCntID 
	AND numDomainID=@numDomainID 
	AND tintPageType=1 
	 AND ISNULL(bitCustom,0)=1

	  ORDER BY tintOrder ASC  
	
	--set @DefaultNocolumns=  @Nocolumns                
	DECLARE @ListRelID AS NUMERIC(9) 
	
--	SELECT TOP 1 tintOrder+1,vcDbColumnName,vcFieldName,                
--	  vcAssociatedControlType,vcListItemType,numListID,vcLookBackTableName                                               
--	,bitCustomField,numFieldId,bitAllowSorting,bitAllowEdit,
--	ListRelID
--	  FROM  #tempForm ORDER BY tintOrder ASC 

		SELECT TOP 1 @tintOrder=tintOrder+1,@vcDbColumnName=vcDbColumnName,@vcFieldName=vcFieldName,                
	  @vcAssociatedControlType=vcAssociatedControlType,@vcListItemType=vcListItemType,@numListID=numListID,@vcLookBackTableName=vcLookBackTableName                                               
	,@bitCustom=bitCustomField,@numFieldId=numFieldId,@bitAllowSorting=bitAllowSorting,@bitAllowEdit=bitAllowEdit,
	@ListRelID=ListRelID
	  FROM  #tempForm ORDER BY tintOrder ASC            

	PRINT @tintOrder
	WHILE @tintOrder>0                                                    
	BEGIN                                                      
--	print @vcAssociatedControlType        
--	print @vcFieldName  
--	print @vcDbColumnName 

	 IF @bitCustom = 0          
	 BEGIN          

	  DECLARE @Prefix AS VARCHAR(5)                  
		  IF @vcLookBackTableName = 'AdditionalContactsInformation'                  
		SET @Prefix = 'ADC.'                  
		  IF @vcLookBackTableName = 'DivisionMaster'                  
		SET @Prefix = 'DM.'                  
		  IF @vcLookBackTableName = 'ProjectsMaster'                  
		SET @PreFix ='Pro.'        
	    
		SET @vcColumnName = @vcDbColumnName+'~'+ CONVERT(VARCHAR(10),@numFieldId) +'~0'
		
		print @vcAssociatedControlType        
		 	PRINT @Prefix
			print @vcDbColumnName 
			print @vcColumnName	

		 IF @vcAssociatedControlType='SelectBox'                                                    
		 BEGIN                                                    
	                                                                       
		 IF @vcListItemType='LI'                                                     
		 BEGIN                                                    
		  SET @strSql=@strSql+',L'+ CONVERT(VARCHAR(3),@tintOrder)+'.vcData'+' ['+ @vcColumnName+']'                                                    
		  SET @WhereCondition= @WhereCondition +' left Join ListDetails L'+ CONVERT(VARCHAR(3),@tintOrder)+ ' on L'+CONVERT(VARCHAR(3),@tintOrder)+ '.numListItemID='+@vcDbColumnName                                                    
		 END                                                    
		 ELSE IF @vcListItemType='S'                                                     
		 BEGIN                                                    
		  SET @strSql=@strSql+',S'+ CONVERT(VARCHAR(3),@tintOrder)+'.vcState'+' ['+ @vcColumnName+']'                                                    
		  SET @WhereCondition= @WhereCondition +' left join State S'+ CONVERT(VARCHAR(3),@tintOrder)+ ' on S'+CONVERT(VARCHAR(3),@tintOrder)+ '.numStateID='+@vcDbColumnName                                                    
		 END   
		 ELSE IF @vcListItemType='PP'
		 BEGIN                                                    
		  SET @strSql=@strSql+',ISNULL(PP.intTotalProgress,0)'+' ['+ @vcColumnName+']'                                                    
		  SET @WhereCondition= @WhereCondition +' LEFT JOIN ProjectProgress PP on Pro.numProID = PP.numProID '
		 END   
		 ELSE IF @vcListItemType='T'                                                     
		 BEGIN                                                    
		  SET @strSql=@strSql+',L'+ CONVERT(VARCHAR(3),@tintOrder)+'.vcData'+' ['+ @vcColumnName+']'                                                    
		  SET @WhereCondition= @WhereCondition +' left Join ListDetails L'+ CONVERT(VARCHAR(3),@tintOrder)+ ' on L'+CONVERT(VARCHAR(3),@tintOrder)+ '.numListItemID='+@vcDbColumnName                                                    
		 END                   
		 ELSE IF   @vcListItemType='U'                                               
		 BEGIN
--			print @vcAssociatedControlType        
--			PRINT @Prefix
--			print @vcDbColumnName 
--			print @vcColumnName 

			SET @strSql=@strSql+',dbo.fn_GetContactName('+@Prefix+@vcDbColumnName+') ['+ @vcColumnName+']'                                                    
		 END                  
		END             
		ELSE IF @vcAssociatedControlType='DateField'                                                    
	   BEGIN             
									IF @vcDbColumnName='intDueDate'
									BEGIN	
										SET @strSql=@strSql+',case when convert(varchar(11),'+@Prefix+@vcDbColumnName+' )= convert(varchar(11),getdate()) then ''<b><font color=red>Today</font></b>'''      
										SET @strSql=@strSql+'when convert(varchar(11),'+@Prefix+@vcDbColumnName+'  )= convert(varchar(11),dateadd(day,-1,getdate())) then ''<b><font color=purple>YesterDay</font></b>'''      
										SET @strSql=@strSql+'when convert(varchar(11),'+@Prefix+@vcDbColumnName+'  )= convert(varchar(11),dateadd(day,1,getdate())) then''<b><font color=orange>Tommorow</font></b>'' ' 
										SET @strSql=@strSql+'else dbo.FormatedDateFromDate(' +@Prefix+@vcDbColumnName+','+CONVERT(VARCHAR(10),@numDomainId)+') end  ['+ @vcColumnName+']'
									END
									ELSE
									BEGIN
									 SET @strSql=@strSql+',case when convert(varchar(11),DateAdd(minute, '+CONVERT(VARCHAR(15),-@ClientTimeZoneOffset)+','+@Prefix+@vcDbColumnName+') )= convert(varchar(11),getdate()) then ''<b><font color=red>Today</font></b>'''    
									 SET @strSql=@strSql+'when convert(varchar(11),DateAdd(minute, '+CONVERT(VARCHAR(15),-@ClientTimeZoneOffset)+','+@Prefix+@vcDbColumnName+') )= convert(varchar(11),dateadd(day,-1,getdate())) then ''<b><font color=purple>YesterDay</font></b>'''    
									 SET @strSql=@strSql+'when convert(varchar(11),DateAdd(minute, '+CONVERT(VARCHAR(15),-@ClientTimeZoneOffset)+','+@Prefix+@vcDbColumnName+') )= convert(varchar(11),dateadd(day,1,getdate())) then''<b><font color=orange>Tommorow</font></b>'' '    
									 SET @strSql=@strSql+'else dbo.FormatedDateFromDate(DateAdd(minute, '+CONVERT(VARCHAR(15),-@ClientTimeZoneOffset)+','+@Prefix+@vcDbColumnName+'),'+CONVERT(VARCHAR(10),@numDomainId)+') end  ['+ @vcColumnName+']'             
	     
									END  
	      
	   END            
		ELSE IF @vcAssociatedControlType='TextBox'                                                    
	   BEGIN             
		 SET @strSql=@strSql+','+ CASE                  
		WHEN @vcDbColumnName='numAge' THEN 'year(getutcdate())-year(bintDOB)'                 
		WHEN @vcDbColumnName='numDivisionID' THEN 'DM.numDivisionID'                 
		WHEN @vcDbColumnName='textSubject' THEN 'convert(varchar(max),textSubject)'                 
		ELSE @PreFix+@vcDbColumnName END+' ['+@vcColumnName+']'              
	   END  
	 ELSE IF  @vcAssociatedControlType='TextArea'                                              
	BEGIN  
	 SET @strSql=@strSql+', '''' ' +' ['+ @vcColumnName+']'            
	     
	 END 
	ELSE  IF @vcAssociatedControlType='CheckBox'                                                
	BEGIN      
	 SET @strSql=@strSql+', isnull('+ @vcDbColumnName +',0) ['+ @vcColumnName+']'                
	         
	 END                                               
    		ELSE IF @vcAssociatedControlType='Popup' AND @vcDbColumnName='numShareWith'                                                   
			BEGIN      
				 SET @strSql=@strSql+',(SELECT SUBSTRING(
		(SELECT '','' + A.vcFirstName+'' ''+A.vcLastName + CASE WHEN ISNULL(SR.numContactType,0)>0 THEN ''('' + dbo.fn_GetListItemName(SR.numContactType) + '')'' ELSE '''' END
		FROM ShareRecord SR JOIN UserMaster UM ON UM.numUserDetailId=SR.numAssignedTo 
								 JOIN AdditionalContactsInformation A ON UM.numUserDetailId=A.numContactID 
			 WHERE SR.numDomainID=pro.numDomainID AND SR.numModuleID=5 AND SR.numRecordID=pro.numProId
					AND UM.numDomainID=pro.numDomainID and UM.numDomainID=A.numDomainID FOR XML PATH('''')),2,200000)) ['+ @vcColumnName+']'                
			 END       
	 END                                              
	ELSE IF @bitCustom = 1        
	 BEGIN          
	--   select @vcFieldName=FLd_label,@vcAssociatedControlType=fld_type,@vcDbColumnName='Cust'+Convert(varchar(10),Fld_Id)  from CFW_Fld_Master                                           
	--   where CFW_Fld_Master.Fld_Id = @numFieldId                                      
	      
		  SET @vcColumnName= @vcDbColumnName +'~'+ CONVERT(VARCHAR(10),@numFieldId)+'~1'
	    
		IF @vcAssociatedControlType = 'TextBox' OR @vcAssociatedControlType = 'TextArea'
	   BEGIN          
	             
		SET @strSql= @strSql+',CFW'+ CONVERT(VARCHAR(3),@tintOrder)+'.Fld_Value  ['+ @vcColumnName + ']'             
		SET @WhereCondition= @WhereCondition +' left Join CFW_FLD_Values_Pro CFW'+ CONVERT(VARCHAR(3),@tintOrder)+ '           
		on CFW'+ CONVERT(VARCHAR(3),@tintOrder)+ '.Fld_Id='+CONVERT(VARCHAR(10),@numFieldId) +'and CFW'+ CONVERT(VARCHAR(3),@tintOrder)+ '.RecId=pro.numProid   '                                                   
	   END          
	             
	   ELSE IF @vcAssociatedControlType = 'Checkbox'         
	   BEGIN          
	             
		SET @strSql= @strSql+',case when isnull(CFW'+ CONVERT(VARCHAR(3),@tintOrder)+'.Fld_Value,0)=0 then ''No'' when isnull(CFW'+ CONVERT(VARCHAR(3),@tintOrder)+'.Fld_Value,0)=1 then ''Yes'' end   ['+ @vcColumnName + ']'             
		SET @WhereCondition= @WhereCondition +' left Join CFW_FLD_Values_Pro CFW'+ CONVERT(VARCHAR(3),@tintOrder)+ '           
		on CFW'+ CONVERT(VARCHAR(3),@tintOrder)+ '.Fld_Id='+CONVERT(VARCHAR(10),@numFieldId) +'and CFW'+ CONVERT(VARCHAR(3),@tintOrder)+ '.RecId=pro.numProid   '                                                   
	   END          
	   ELSE IF @vcAssociatedControlType = 'DateField'         
	   BEGIN           
	             
		SET @strSql= @strSql+',dbo.FormatedDateFromDate(CFW'+ CONVERT(VARCHAR(3),@tintOrder)+'.Fld_Value,'+CONVERT(VARCHAR(10),@numDomainId)+')  ['+ @vcColumnName + ']'             
		SET @WhereCondition= @WhereCondition +' left Join CFW_FLD_Values_Pro CFW'+ CONVERT(VARCHAR(3),@tintOrder)+ '           
		on CFW'+ CONVERT(VARCHAR(3),@tintOrder)+ '.Fld_Id='+CONVERT(VARCHAR(10),@numFieldId) +'and CFW'+ CONVERT(VARCHAR(3),@tintOrder)+ '.RecId=pro.numProid   '                                                   
	   END          
	   ELSE IF @vcAssociatedControlType = 'SelectBox'       
	   BEGIN        
		SET @vcDbColumnName = 'DCust'+CONVERT(VARCHAR(10),@numFieldId)          
		SET @strSql=@strSql+',L'+ CONVERT(VARCHAR(3),@tintOrder)+'.vcData'+' ['+ @vcColumnName + ']'                                                    
		SET @WhereCondition= @WhereCondition +' left Join CFW_FLD_Values_Pro CFW'+ CONVERT(VARCHAR(3),@tintOrder)+ '           
		 on CFW'+ CONVERT(VARCHAR(3),@tintOrder)+ '.Fld_Id='+CONVERT(VARCHAR(10),@numFieldId) +'and CFW'+ CONVERT(VARCHAR(3),@tintOrder)+ '.RecId=pro.numProid   '                                                   
		SET @WhereCondition= @WhereCondition +' left Join ListDetails L'+ CONVERT(VARCHAR(3),@tintOrder)+ ' on L'+CONVERT(VARCHAR(3),@tintOrder)+ '.numListItemID=CFW'+ CONVERT(VARCHAR(3),@tintOrder)+'.Fld_Value'          
	   END             
	 END          
	                                                  
	   SELECT TOP 1 @tintOrder=tintOrder+1,@vcDbColumnName=vcDbColumnName,@vcFieldName=vcFieldName,                
	  @vcAssociatedControlType=vcAssociatedControlType,@vcListItemType=vcListItemType,@numListID=numListID,@vcLookBackTableName=vcLookBackTableName                                               
	,@bitCustom=bitCustomField,@numFieldId=numFieldId,@bitAllowSorting=bitAllowSorting,@bitAllowEdit=bitAllowEdit,
	@ListRelID=ListRelID
	  FROM #tempForm WHERE tintOrder > @tintOrder-1 ORDER BY tintOrder ASC            
	 
	  IF @@ROWCOUNT=0 SET @tintOrder=0 
	                  	                     
	END                         
                                                                
	SET @strSql=@strSql+' ,TotalRowCount                                                                  
	FROM ProjectsMaster pro                          
	join additionalContactsinformation ADC                                              
	on ADC.numContactId=pro.numCustPrjMgr                                              
	join DivisionMaster DM                                               
	on DM.numDivisionID=ADC.numDivisionID                                              
	join CompanyInfo cmp                                             
	on cmp.numCompanyID=DM.numCompanyID ' + @WhereCondition+  ' join tblProjects T on T.numProID=Pro.numProID'                                                              
	
	 SET @strSql=@strSql+' WHERE RowNumber > '+CONVERT(VARCHAR(10),@firstRec)+ ' and RowNumber <'+ CONVERT(VARCHAR(10),@lastRec) + ' order by RowNumber'
                                                      
	PRINT @strSql                                            
	EXEC (@strSql)  

	SELECT * FROM #tempForm
	--SET @TotRecs = (SELECT COUNT(*) FROM #tempForm)                                    
	DROP TABLE #tempForm

END
ELSE
BEGIN                                    

CREATE TABLE #tempTable ( ID INT IDENTITY PRIMARY KEY,                                     
      numProId VARCHAR(15),                                    
      Name VARCHAR(100),                                    
      DealID VARCHAR(100),                                    
      numOppId VARCHAR(15),                                    
 numDivisionID VARCHAR(15),                                    
 numCompanyID VARCHAR(100),                                    
 tintCRMType VARCHAR(10),                                    
 numContactID VARCHAR(15),                                    
 CustJobContact VARCHAR(110),                                    
 LstCmtMilestone  VARCHAR(100),                                    
 lstCompStage VARCHAR(100),                                    
 numTerId VARCHAR(15),                                    
 numRecOwner VARCHAR(15),                      
 AssignedToBy VARCHAR(50),
 DueDate VARCHAR(50),
 intTotalProgress INT 
 )                                    
                                    
                                    
--declare @strSql as varchar(8000)                                                  
SET @strSql=''                                                           
SET @strSql='SELECT '      
IF (@tintSortOrder=5 OR @tintSortOrder=6) SET @strSql=@strSql+ ' top 20 '      
      
  SET @strSql=@strSql+' pro.numProId,                                     
    pro.vcProjectName AS Name,
    dbo.FormatedDateFromDate(pro.intDueDate,pro.numDomainID) AS DueDate, 
    isnull(PP.intTotalProgress,0) intTotalProgress,
    opp.vcPOppName as DealID,                                    
    isnull(opp.numOppId,0) numOppId,                                    
    div.numDivisionID,                                    
    div.numCompanyID,                                    
    div.tintCRMType,                                    
    addCon.numContactID,                                    
    addCon.vcFirstName + '' '' + addCon.vcLastName AS CustJobContact,                                     
    dbo.GetProLstMileStoneCompltd(pro.numProId) as LstCmtMilestone,                                    
    dbo.GetProLstStageCompltd(pro.numProId,'+CONVERT(VARCHAR(10),@ClientTimeZoneOffset)+') as lstCompStage,                                    
    div.numTerId,                                    
    pro.numRecOwner,                      
     dbo.fn_GetContactName(Pro.numAssignedTo)+''/ ''+   dbo.fn_GetContactName(Pro.numAssignedBy) AssignedToBy                               
   FROM ProjectsMaster pro                                    
   left join  OpportunityMaster opp                                    
   on opp.numOppId=pro.numOppId 
   left join ProjectProgress PP 
   on PP.numProID = pro.numProID '                    
   IF @bitPartner=1 SET @strSql=@strSql+' left join ProjectsContacts ProCont on ProCont.numProId=Pro.numProId and ProCont.bitPartner=1 and ProCont.numContactId='+CONVERT(VARCHAR(15),@numUserCntID)+ ''                    
  SET @strSql=@strSql+' join additionalContactsinformation addCon                                    
   on addCon.numContactId=pro.numCustPrjMgr                                    
  join DivisionMaster Div                                     
   on Div.numDivisionID=addCon.numDivisionID                                    
  join CompanyInfo C                                    
   on C.numCompanyID=div.numCompanyID '                                                               
  SET @strSql=@strSql+' where tintProStatus=0            
  and Div.numDomainID=' + CONVERT(VARCHAR(15),@numDomainID)+''                        
  IF @FirstName<>'' SET @strSql=@strSql + 'and addCon.vcFirstName  like '''+ISNULL(@FirstName,'')+'%'''                                    
  IF @LastName<>'' SET @strSql=@strSql+ 'and addCon.vcLastName like '''+ISNULL(@LastName,'')+'%'''                          
  IF @CustName<>'' SET @strSql=@strSql+ 'and C.vcCompanyName like '''+ISNULL(@CustName,'')+'%'''                                      
      
 SET @strSql=@strSql+ 'and (isnull(pro.numProjectStatus,0) = '+ CONVERT(VARCHAR(10),@numProjectStatus) + ' OR ' + CONVERT(VARCHAR(10),@numProjectStatus) +'=0)'
 IF @bitOpenProjects=1 SET  @strSql=@strSql + 'and isnull(pro.numProjectStatus,0) <> 27492'       
                
IF @numDivisionID <>0 SET @strSql=@strSql + ' And div.numDivisionID =' + CONVERT(VARCHAR(15),@numDivisionID)                                     
IF @SortChar<>'0' SET @strSql=@strSql + ' And Pro.vcProjectName like '''+@SortChar+'%'''                                     
IF @tintUserRightType=1 SET @strSql=@strSql + ' AND (Pro.numRecOwner = '+CONVERT(VARCHAR(15),@numUserCntID)+' or Pro.numAssignedTo = '+CONVERT(VARCHAR(15),@numUserCntID) +' or  Pro.numProId in (select distinct(numProID) from ProjectsStageDetails          
 
where numAssignTo ='+ CONVERT(VARCHAR(15),@numUserCntID)+')' + CASE WHEN @bitPartner=1 THEN ' or ( ProCont.bitPartner=1 and ProCont.numContactId='+CONVERT(VARCHAR(15),@numUserCntID)+ ')' ELSE '' END  +')'                                       
ELSE IF @tintUserRightType=2 SET @strSql=@strSql + ' and  (Div.numTerID in (select numTerritoryID from  UserTerritory where numUserCntID= '+CONVERT(VARCHAR(15),@numUserCntID)+' ) or Div.numTerID=0 or Pro.numAssignedTo= '+ CONVERT(VARCHAR(15),@numUserCntID
  
    
)  +' or  Pro.numProId in (select distinct(numProID) from ProjectsStageDetails           
where numAssignTo ='+ CONVERT(VARCHAR(15),@numUserCntID)+'))'                                   
        
IF @tintSortOrder=1  SET @strSql=@strSql + ' AND (Pro.numRecOwner= '+ CONVERT(VARCHAR(15),@numUserCntID)  +' or Pro.numAssignedTo= '+ CONVERT(VARCHAR(15),@numUserCntID)  +' or  Pro.numProId in (select distinct(numProID) from ProjectsStageDetails          
 
where numAssignTo ='+ CONVERT(VARCHAR(15),@numUserCntID)+'))'         
ELSE IF @tintSortOrder=2  SET @strSql=@strSql  + ' AND (Pro.numRecOwner in (select A.numContactID from AdditionalContactsInformation A                                               
where A.numManagerID='+ CONVERT(VARCHAR(15),@numUserCntID)+') or Pro.numAssignedTo in (select A.numContactID from AdditionalContactsInformation A                                               
where A.numManagerID='+ CONVERT(VARCHAR(15),@numUserCntID)+') or  Pro.numProId in (select distinct(numProID) from ProjectsStageDetails           
where numAssignTo in (select A.numContactID from AdditionalContactsInformation A                                               
where A.numManagerID='+ CONVERT(VARCHAR(15),@numUserCntID)+')))'                                                                   
ELSE IF @tintSortOrder=4  SET @strSql=@strSql + ' AND Pro.bintCreatedDate > '''+CONVERT(VARCHAR(20),DATEADD(DAY,-8,GETUTCDATE()))+''''                                    
ELSE IF @tintSortOrder=6  SET @strSql=@strSql + ' and Pro.numModifiedBy= '+ CONVERT(VARCHAR(15),@numUserCntID) +'  ORDER BY pro.bintmodifieddate desc '                                    
                                
IF @tintSortOrder=1  SET @strSql=@strSql + ' ORDER BY ' + @columnName +' '+ @columnSortOrder                                    
ELSE IF @tintSortOrder=2  SET @strSql=@strSql + ' ORDER BY ' + @columnName +' '+ @columnSortOrder                                            
ELSE IF @tintSortOrder=3  SET @strSql=@strSql + ' ORDER BY ' + @columnName +' '+ @columnSortOrder                                        
ELSE IF @tintSortOrder=4  SET @strSql=@strSql + ' and Pro.numRecOwner= '+ CONVERT(VARCHAR(15),@numUserCntID)+' ORDER BY ' + @columnName +' '+ @columnSortOrder                                    
ELSE IF @tintSortOrder=5  SET @strSql=@strSql + ' and Pro.numRecOwner= '+ CONVERT(VARCHAR(15),@numUserCntID)+ ' ORDER BY ' + @columnName +' '+ @columnSortOrder                                    
ELSE IF @tintSortOrder=6  SET @strSql=@strSql +' , ' + @columnName +' '+ @columnSortOrder                                              
  PRINT @strSql                                
INSERT INTO #tempTable(                                    
 numProId,                                    
      Name,                                    
      DueDate,
      intTotalProgress,
      DealID,                                    
      numOppId,                                    
 numDivisionID,                                    
 numCompanyID,                                    
 tintCRMType,                                    
 numContactID,                                    
 CustJobContact,                                    
 LstCmtMilestone,                                    
 lstCompStage,                                    
 numTerId,                                    
 numRecOwner,                      
 AssignedToBy)                                    
EXEC (@strSql)                                     
                                                                     
 SET @firstRec= (@CurrentPage-1) * @PageSize                                    
     SET @lastRec= (@CurrentPage*@PageSize+1)                                    
SELECT * FROM #tempTable WHERE ID > @firstRec AND ID < @lastRec                                    
SET @TotRecs=(SELECT COUNT(*) FROM #tempTable)                                    
DROP TABLE #tempTable

END 
GO
