/****** Object:  StoredProcedure [dbo].[USP_GetOpportunityType]    Script Date: 07/26/2008 16:18:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created By Siva  
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getopportunitytype')
DROP PROCEDURE usp_getopportunitytype
GO
CREATE PROCEDURE [dbo].[USP_GetOpportunityType]  
@numDomainId as numeric(9)=0,
@numOppId as numeric(9)=0
As  
Begin  
 Select tintOppType From OpportunityMaster Where numDomainId=@numDomainId  And numOppId=@numOppId
End
GO
