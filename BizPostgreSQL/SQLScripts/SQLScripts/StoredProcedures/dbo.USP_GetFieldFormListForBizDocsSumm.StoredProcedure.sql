/****** Object:  StoredProcedure [dbo].[USP_GetFieldFormListForBizDocsSumm]    Script Date: 07/26/2008 16:17:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj


GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getfieldformlistforbizdocssumm')
DROP PROCEDURE usp_getfieldformlistforbizdocssumm
GO
CREATE PROCEDURE [dbo].[USP_GetFieldFormListForBizDocsSumm] 
@numDomainID as numeric(9),
@BizDocID as numeric(9),
@numFormID as numeric(9),
@numBizDocTempID as numeric(9)
as

select DFM.numFieldID as numFormFieldId,isnull(DFFM.vcFieldName,DFM.vcFieldName) as vcFormFieldName,DFM.vcDbColumnName
from DycFormField_Mapping DFFM 
JOIN DycFieldMaster DFM on DFFM.numModuleID=DFFM.numModuleID and DFFM.numFieldID=DFM.numFieldID
where numFormID=16 and (DFFM.numDomainID is null or DFFM.numDomainID =@numDomainID)
AND ISNULL(DFM.bitDeleted,0) = 0

CREATE TABLE #temp(
	ID int IDENTITY(1,1) NOT NULL,
	[numFormID] [numeric](18, 0) NOT NULL,
	[numBizDocID] [numeric](18, 0) NOT NULL,
	[numFieldID] [numeric](18, 0) NOT NULL,
	[numDomainID] [numeric](18, 0) NOT NULL,
	[numBizDocTempID] [numeric](18, 0) NULL
) 

--IF(SELECT COUNT(*) FROM DycFrmConfigBizDocsSumm Ds where numBizDocID=@BizDocID and Ds.numFormID=@numFormID 
--	and isnull(Ds.numBizDocTempID,0)=@numBizDocTempID and Ds.numDomainID=@numDomainID)=0
--BEGIN
--	INSERT INTO DycFrmConfigBizDocsSumm (numFormID,numBizDocID,numFieldID,numDomainID,numBizDocTempID)
--	 SELECT @numFormID,@BizDocID,DFM.numFieldID,@numDomainID,@numBizDocTempID FROM
--	 DycFormField_Mapping DFFM 
--		JOIN DycFieldMaster DFM on DFFM.numModuleID=DFFM.numModuleID and DFFM.numFieldID=DFM.numFieldID
--		where numFormID=16 and DFFM.numDomainID is null
--END	


insert into #temp
select * from DycFrmConfigBizDocsSumm Ds where numBizDocID=@BizDocID and Ds.numFormID=@numFormID 
	and isnull(Ds.numBizDocTempID,0)=@numBizDocTempID and Ds.numDomainID=@numDomainID


select DFM.numFieldID as numFormFieldId,isnull(DFFM.vcFieldName,DFM.vcFieldName) as vcFormFieldName,DFM.vcDbColumnName
from DycFormField_Mapping DFFM 
JOIN DycFieldMaster DFM on DFFM.numModuleID=DFFM.numModuleID and DFFM.numFieldID=DFM.numFieldID
join #temp Ds on Ds.numFieldID=DFFM.numFieldID 
where numBizDocID=@BizDocID and Ds.numFormID=@numFormID and (DFFM.numFormID=@numFormID or DFFM.numDomainID is null)
and isnull(Ds.numBizDocTempID,0)=@numBizDocTempID and Ds.numDomainID=@numDomainID order by Ds.id
GO
 