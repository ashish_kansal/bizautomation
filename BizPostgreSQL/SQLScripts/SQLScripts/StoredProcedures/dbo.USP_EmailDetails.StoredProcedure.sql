/****** Object:  StoredProcedure [dbo].[USP_EmailDetails]    Script Date: 07/26/2008 16:15:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created bY Anoop jayaraj              
              
--created by anoop jayaraj          
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_emaildetails')
DROP PROCEDURE usp_emaildetails
GO
CREATE PROCEDURE [dbo].[USP_EmailDetails]              
@numEmailHstrID as numeric(9),              
@bintCreatedOn as datetime              
as              
              
select dbo.GetEmaillAdd(numEmailHstrID,4) as vcMessageFrom,              
dbo.GetEmaillAdd(numEmailHstrID,1) as vcMessageTo,         
dbo.GetEmaillAdd(numEmailHstrID,2) as vcCC,        
dbo.GetEmaillAdd(numEmailHstrID,3) as vcBCC,             
isnull(vcSubject,'') as vcSubject,              
isnull(vcBody,'') as vcBody,chrSource,tinttype,numuid,numUserCntId from emailHistory               
where numEmailHstrID=@numEmailHstrID
GO
