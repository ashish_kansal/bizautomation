/****** Object:  StoredProcedure [dbo].[USP_DeleteFollowUpHstr]    Script Date: 07/26/2008 16:15:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_deletefollowuphstr')
DROP PROCEDURE usp_deletefollowuphstr
GO
CREATE PROCEDURE [dbo].[USP_DeleteFollowUpHstr]  
@numFollowUpStatusID as numeric(9),
@numDomainId as numeric(9) 
as  
  
delete from FollowUpHistory where numFollowUpStatusID=@numFollowUpStatusID  and numDomainID= @numDomainId
GO
