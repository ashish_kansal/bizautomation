/****** Object:  StoredProcedure [dbo].[USP_LeadsUpdateConInfo]    Script Date: 07/26/2008 16:19:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj    
    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_leadsupdateconinfo')
DROP PROCEDURE usp_leadsupdateconinfo
GO
CREATE PROCEDURE [dbo].[USP_LeadsUpdateConInfo]
@vcFirstName as varchar(50),        
@vcLastName as varchar(50),        
@vcEmail as varchar(50),        
@numFollowUpStatus as numeric(9),        
@numPosition as numeric(9),        
@numPhone as varchar(15),        
@numPhoneExtension as varchar(7),        
@Comments as Text='',        
@numContactId as numeric(9),        
--@vcStreet as varchar(50),        
--@vcCity as varchar(50),        
--@intPostalCode as varchar(12),        
--@vcState as numeric(9),        
--@vcCountry as numeric(9),     
@vcTitle as varchar(100),        
@vcAltEmail as varchar(100),
@numECampaignID NUMERIC,
@numUserCntID NUMERIC
as        
 update AdditionalContactsInformation        
         
 set  vcFirstName=@vcFirstName,        
  vcLastName=@vcLastName,        
  vcEmail=@vcEmail,         
  vcPosition=@numPosition,        
  numPhone=@numPhone,        
  numPhoneExtension=@numPhoneExtension,        
  txtNotes=@Comments,          
  vcTitle=@vcTitle,    
  vcAltEmail=@vcAltEmail,
  numECampaignID=@numECampaignID,
  [numModifiedBy]=@numUserCntID
 where numContactId=@numContactId  

/*Added by chintan BugID-262*/
	 DECLARE @Date AS DATETIME 
	 SELECT @Date = dbo.[GetUTCDateWithoutTime]()
	 IF @numECampaignID = -1 
		SET @numECampaignID=0
	 
 	EXEC [USP_ManageConEmailCampaign]
	@numConEmailCampID = 0, --  numeric(9, 0)
	@numContactID = @numContactID, --  numeric(9, 0)
	@numECampaignID = @numECampaignID, --  numeric(9, 0)
	@intStartDate = @Date, --  datetime
	@bitEngaged = 1, --  bit
	@numUserCntID = @numUserCntID --  numeric(9, 0)
  
--  
--update ContactAddress set  vcStreet=@vcStreet,        
--  vcCity=@vcCity,        
--  intPostalCode =@intPostalCode,        
--  vcState=@vcState,        
--  vcCountry=@vcCountry where   numContactId=@numContactId
GO
