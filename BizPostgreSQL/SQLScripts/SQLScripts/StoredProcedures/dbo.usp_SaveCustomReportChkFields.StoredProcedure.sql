/****** Object:  StoredProcedure [dbo].[usp_SaveCustomReportChkFields]    Script Date: 07/26/2008 16:20:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_savecustomreportchkfields')
DROP PROCEDURE usp_savecustomreportchkfields
GO
CREATE PROCEDURE [dbo].[usp_SaveCustomReportChkFields]
@ReportId as numeric(9)=0,
@strCheckedFields as text=''
as
 delete CustReportFields where numCustomReportId =@ReportId
 declare @hDoc  int              

   EXEC sp_xml_preparedocument @hDoc OUTPUT, @strCheckedFields    
                 
    insert into CustReportFields              
     (numCustomReportId,vcFieldName,vcValue)              
                  
    select @ReportId,
     X.*   from(SELECT * FROM OPENXML (@hDoc,'/NewDataSet/Table',2)              
    WITH  (              
    vcField varchar(200),    
    vcValue bit))X 
    EXEC sp_xml_removedocument @hDoc
GO
