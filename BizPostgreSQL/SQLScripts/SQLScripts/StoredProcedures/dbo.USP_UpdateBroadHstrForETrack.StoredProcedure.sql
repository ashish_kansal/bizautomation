/****** Object:  StoredProcedure [dbo].[USP_UpdateBroadHstrForETrack]    Script Date: 07/26/2008 16:21:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_updatebroadhstrforetrack')
DROP PROCEDURE usp_updatebroadhstrforetrack
GO
CREATE PROCEDURE [dbo].[USP_UpdateBroadHstrForETrack]
@numBroadHstrID as numeric(9)=0,
@numContactID as numeric(9)=0
as

update BroadCastDtls set numNoofTimes=(isnull(numNoofTimes,0)+1) where numBroadCastID=@numBroadHstrID and numContactID=@numContactID
GO
