/****** Object:  StoredProcedure [dbo].[usp_GetDivisionName]    Script Date: 07/26/2008 16:17:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getdivisionname')
DROP PROCEDURE usp_getdivisionname
GO
CREATE PROCEDURE [dbo].[usp_GetDivisionName]
@numDivisionId numeric   
--
AS
SELECT vcDivisionName FROM divisionMaster WHERE numDivisionid=@numDivisionId
GO
