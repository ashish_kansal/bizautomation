
GO
/****** Object:  StoredProcedure [dbo].[usp_getActItemsMail]    Script Date: 06/04/2009 16:02:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getactitemsmail')
DROP PROCEDURE usp_getactitemsmail
GO
CREATE PROCEDURE [dbo].[usp_getActItemsMail]
          @Mode     AS BIT  = 0,
          @Time     AS NUMERIC(9)  = 30,
          @strComId AS VARCHAR(1000)
AS
  IF @Mode = 0
    BEGIN
      SELECT numCommId,
             vcSubject,
             REPLACE(REPLACE(CONVERT(VARCHAR(MAX),vcDocDesc),'##CommName##',
                             dbo.fn_GetListItemName(bitTask)),
                     '##DueDate##',dbo.FormatedDateTimeFromDate(dtendTime,communication.numDomainId)) AS vcbody,
             numEmailTemplate numEmailTemplate,
             vcEmail,
             ACI.[numDomainID] ---Added Domain ID for Fetching SMTP details for domain
      FROM   communication
             JOIN additionalcontactsinformation ACI
               ON numAssign = ACI.numContactId
             JOIN genericdocuments
               ON numEmailTemplate = numGenericDocID
      WHERE  bitalert = 'true'
             AND bitEmailSent = 0
             AND CASE 
                   WHEN bitsendEmailTemp = 1 THEN dateadd(HOUR,tinthours,dtendTime)
                   ELSE dateadd(HOUR,-tinthours,dtendTime)
                 END BETWEEN dateadd(MINUTE,0,getutcdate()) AND dateadd(MINUTE,@Time,getutcdate())
    END
  ELSE
    BEGIN
      IF @strComId <> ''
        BEGIN
          UPDATE communication
          SET    bitEmailSent = 1
          WHERE  numcommid IN (SELECT *
                               FROM   dbo.split(@strComId,','))
        END
    END
