/****** Object:  StoredProcedure [dbo].[usp_GetDuplicateDivision]    Script Date: 07/26/2008 16:17:23 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getduplicatedivision')
DROP PROCEDURE usp_getduplicatedivision
GO
CREATE PROCEDURE [dbo].[usp_GetDuplicateDivision]
@numCompanyID numeric,
@vcDivisionName varchar(100)
--
AS
BEGIN

--select * from DivisionMaster where 
--numCompanyID = @numCompanyIDand 
--vcDivisionName = @vcDivisionName
--and DivisionMaster.tintCRMType in (1,2)



select DM.vcDivisionName, CI.vcCompanyName, 
  isnull(AD1.vcStreet,'') as vcBillStreet, 
  isnull(AD1.vcCity,'') as vcBillCity, 
  isnull(AD1.numState,0) as vcBilState,
  isnull(AD1.vcPostalCode,'') as vcBillPostCode,
  isnull(AD1.numCountry,0)  as vcBillCountry,
DM.tintCRMType,  CI.numCompanyID, DM.numDivisionID
from dbo.CompanyInfo CI, DivisionMaster DM
  LEFT JOIN dbo.AddressDetails AD1 ON AD1.numDomainID=DM.numDomainID 
   AND AD1.numRecordID=DM.numDivisionID AND AD1.tintAddressOf=2 AND AD1.tintAddressType=1 AND AD1.bitIsPrimary=1
where CI.numCompanyID=DM.numCompanyID 
and CI.numCompanyID =@numCompanyID  and DM.vcDivisionName = @vcDivisionName and DM.bitPublicFlag=0 and DM.tintCRMType in (1,2)

END
GO
