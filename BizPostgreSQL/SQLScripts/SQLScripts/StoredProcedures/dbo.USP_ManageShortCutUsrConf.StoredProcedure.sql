/****** Object:  StoredProcedure [dbo].[USP_ManageShortCutUsrConf]    Script Date: 07/26/2008 16:20:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj          
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_manageshortcutusrconf')
DROP PROCEDURE usp_manageshortcutusrconf
GO
CREATE PROCEDURE [dbo].[USP_ManageShortCutUsrConf]            
@numGroupID as numeric(9)=0,            
@numDomainId as numeric(9)=0,            
@numContactId as numeric(9)=0,  
@strFav as text='',        
@strRec as text=''       
  
as            
        
  
------------------------------------------------------------------------------------------  
--For Links  
------------------------------------------------------------------------------------------            
IF (select count(*) from ShortCutUsrconf where bittype=1 and numDomainId = @numDomainId and numGroupID =@numGroupID and numcontactId= @numContactId ) > 0  
  
 begin   
  delete ShortCutUsrconf where bittype=1 and numDomainId = @numDomainId and numGroupID =@numGroupID  and numcontactId= @numContactId
 end  
  
  
   declare @hDoc  int            
               
   EXEC sp_xml_preparedocument @hDoc OUTPUT, @strFav  
               
    insert into ShortCutUsrconf            
     (numGroupId,numDomainId,numContactId,bittype,numOrder,numLinkId)            
                
    select @numGroupID,@numDomainId,@numContactId,1,            
     X.*   from(SELECT * FROM OPENXML (@hDoc,'/NewDataSet/Table',2)            
    WITH  (            
    numOrder numeric,  
    numLinkId numeric))X            
               
               
               
    EXEC sp_xml_removedocument @hDoc            
      
------------------------------------------------------------------------------------------  
--For New Items  
------------------------------------------------------------------------------------------            
            
IF (select count(*) from ShortCutUsrconf where bittype=0 and numDomainId = @numDomainId and numGroupID =@numGroupID and numcontactId= @numContactId) > 0  
  
 begin   
  delete ShortCutUsrconf where bittype=0 and numDomainId = @numDomainId and numGroupID =@numGroupID  and numcontactId= @numContactId
 end  
  
   declare @hDoc1  int            
               
   EXEC sp_xml_preparedocument @hDoc1 OUTPUT, @strRec  
               
    insert into ShortCutUsrconf            
     (numGroupId,numDomainId,numcontactid,bittype,numOrder,numLinkId)            
                
    select @numGroupID,@numDomainId,@numContactId,0,            
     X.*   from(SELECT * FROM OPENXML (@hDoc1,'/NewDataSet/Table',2)            
    WITH  (            
    numOrder numeric,  
    numLinkId numeric))X            
               
               
               
    EXEC sp_xml_removedocument @hDoc1
GO
