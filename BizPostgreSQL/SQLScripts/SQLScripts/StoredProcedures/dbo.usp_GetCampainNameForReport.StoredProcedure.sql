SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
go

--created By Anoop Jayaraj      
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getcampainnameforreport')
DROP PROCEDURE usp_getcampainnameforreport
GO
CREATE PROCEDURE [dbo].[usp_GetCampainNameForReport]
    @byteMode AS TINYINT,
    @numUserCntID AS NUMERIC=0,
    @numDomainID AS NUMERIC
AS 
    IF @byteMode = 0 
        SELECT  numCampaignID,
                vcCampaignName + '(' + CONVERT(VARCHAR(20), intLaunchDate) + '-'
                + CONVERT(VARCHAR(20), intEndDate) + ')' AS CampaignName
        FROM    CampaignMaster
                WHERE CampaignMaster.numDomainID = @numDomainID    
    
    IF @byteMode = 1 
        SELECT  Cam.numCampaignID,
                vcCampaignName + '(' + CONVERT(VARCHAR(20), intLaunchDate) + '-'
                + CONVERT(VARCHAR(20), intEndDate) + ')' AS CampaignName
        FROM    ForReportsByCampaign ReptCam
                JOIN CampaignMaster Cam ON Cam.numCampaignID = ReptCam.numCampaignID
        WHERE   ReptCam.numUserCntID = @numUserCntID
                AND Cam.numDomainID = @numDomainID
                
    IF @byteMode = 2 
        SELECT  numCampaignID,
                --vcCampaignName + CASE bitIsOnline WHEN 1 THEN ' (Online)' ELSE ' (Offline)' END AS vcCampaignName
                vcCampaignName 
        FROM    CampaignMaster
        WHERE   numDomainID = @numDomainID
        AND dbo.CampaignMaster.bitActive = 1
--        AND [bitIsOnline] <> 1 

