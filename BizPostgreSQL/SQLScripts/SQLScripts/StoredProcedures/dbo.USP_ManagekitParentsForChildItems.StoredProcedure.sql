/****** Object:  StoredProcedure [dbo].[USP_ManagekitParentsForChildItems]    Script Date: 07/26/2008 16:19:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--create by anoop jayaraj  
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_managekitparentsforchilditems')
DROP PROCEDURE usp_managekitparentsforchilditems
GO
CREATE PROCEDURE [dbo].[USP_ManagekitParentsForChildItems]
@numItemDetailID NUMERIC(18,0),
@numItemKitID NUMERIC(18,0),  
@numChildItemID NUMERIC(18,0),  
@numQtyItemsReq NUMERIC(18,0),  
@byteMode TINYINT
AS  
IF @byteMode=0  
BEGIN
	IF EXISTS (SELECT numItemCode FROM Item WHERE numItemCode=@numItemKitID AND (ISNULL(bitAssembly,0) = 1 OR ISNULL(bitKitParent,0) = 1))
	BEGIN
		IF @numItemKitID = @numChildItemID
		BEGIN
			RAISERROR('CAN_NOT_ADD_PARENT_AS_CHILD',16,1)
		END
		ELSE IF EXISTS(SELECT numItemDetailID FROM ItemDetails WHERE numItemKitID=@numItemKitID AND numChildItemID=@numChildItemID)
		BEGIN
			RAISERROR('CHILD_ITEM_ALREADY_ADDED',16,1)
		END
		ELSE IF EXISTS (SELECT numItemCode FROM Item WHERE numItemCode=@numItemKitID AND ISNULL(bitAssembly,0) = 1) AND EXISTS (SELECT numItemCode FROM Item WHERE numItemCode=@numChildItemID AND ISNULL(bitKitParent,0) = 1)
		BEGIN
			RAISERROR('CAN_NOT_ADD_KIT_AS_ASSEMBLY_CHILD',16,1)
		END
		ELSE IF EXISTS (SELECT numItemCode FROM Item WHERE numItemCode=@numItemKitID AND ISNULL(bitKitParent,0) = 1) AND EXISTS (SELECT numItemCode FROM Item WHERE numItemCode=@numChildItemID AND ISNULL(bitKitParent,0) = 1) AND EXISTS (SELECT numItemDetailID FROM ItemDetails WHERE numChildItemID=@numItemKitID)
		BEGIN
			RAISERROR('CAN_NOT_ADD_KIT_AS_KIT_IS_ALREADY_CHILD_OTHER_ITEM',16,1)
		END
		ELSE 
		BEGIN
			INSERT INTO ItemDetails 
			(numItemKitID,numChildItemID,numQtyItemsReq,sintOrder)  
			VALUES
			(@numItemKitID,@numChildItemID,@numQtyItemsReq,ISNULL((SELECT MAX(sintOrder) FROM ItemDetails WHERE numItemKitID=@numItemKitID),0) + 1)
		END
	END

	
END  
  
IF @byteMode=1  
BEGIN  
	DELETE FROM ItemDetails WHERE numItemKitID=@numItemKitID and numChildItemID=@numChildItemID
END
IF @byteMode=2
BEGIN  
	DELETE FROM ItemDetails WHERE numItemDetailID=@numItemDetailID
END
GO
