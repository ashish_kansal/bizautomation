GO
/****** Object:  StoredProcedure [dbo].[USP_AssCntCase]    Script Date: 07/26/2008 16:14:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--modified by anoop jayaraj      
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_asscntcase')
DROP PROCEDURE usp_asscntcase
GO
CREATE PROCEDURE [dbo].[USP_AssCntCase]            
@numCaseId numeric(9)=0 ,
@numDomainID as numeric(9)=0           
--          
AS          

       
 SELECT  D.numDivisionID,a.numContactId,vcCompanyName+', '+isnull(Lst.vcData,'-') as Company,       
  a.vcFirstname +' '+ a.vcLastName as [Name],        
  --a.numPhone +', '+ a.numPhoneExtension as Phone,  
  case when a.numPhone<>'' then + a.numPhone +case when a.numPhoneExtension<>'' then ' - ' + a.numPhoneExtension else '' end  else '' end as [Phone],  
  a.vcEmail as Email,        
  b.vcData as ContactRole,        
  CCont.numRole as ContRoleId,       
  convert(integer,isnull(bitPartner,0)) as bitPartner,       
  a.numcontacttype,CASE WHEN CCont.bitSubscribedEmailAlert=1 THEN 'Yes' ELSE 'No' END AS SubscribedEmailAlert FROM CaseContacts CCont        
  join additionalContactsinformation a on        
  a.numContactId=CCont.numContactId      
  join DivisionMaster D      
  on a.numDivisionID =D.numDivisionID      
  join CompanyInfo C      
  on D.numCompanyID=C.numCompanyID       
  left join listdetails b         
  on b.numlistitemid=CCont.numRole      
  left join listdetails Lst      
  on Lst.numlistitemid=C.numCompanyType      
  WHERE CCont.numCaseID=@numCaseId    and A.numDomainID= @numDomainID
GO
