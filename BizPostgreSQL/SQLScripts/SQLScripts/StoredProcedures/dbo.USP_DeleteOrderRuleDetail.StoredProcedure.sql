GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_deleteorderruledetail')
DROP PROCEDURE usp_deleteorderruledetail
GO
CREATE PROCEDURE USP_DeleteOrderRuleDetail
@numRuleDetailsId numeric(9)

as

begin

DELETE FROM OrderAutoRuleDetails WHERE numRuleDetailsId=@numRuleDetailsId
SELECT 1

end