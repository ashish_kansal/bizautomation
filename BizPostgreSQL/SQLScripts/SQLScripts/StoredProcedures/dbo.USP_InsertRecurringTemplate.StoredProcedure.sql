
GO
/****** Object:  StoredProcedure [dbo].[USP_InsertRecurringTemplate]    Script Date: 03/25/2009 15:20:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created by Siva      
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_insertrecurringtemplate')
DROP PROCEDURE usp_insertrecurringtemplate
GO
CREATE PROCEDURE [dbo].[USP_InsertRecurringTemplate]      
@numRecurringId as numeric(9)=0,    
@varRecurringTemplateName as varchar(50),      
@chrIntervalType as char(1),      
@tintIntervalDays as tinyInt,      
@tintMonthlyType as tinyint,      
@tintFirstDet as tinyint,      
@tintWeekDays as tinyint,      
@tintMonths as tinyint,      
@dtStartDate as datetime,      
@dtEndDate as datetime,      
@bitEndType as bit,  
@bitEndTransactionType as bit,  
@numNoTransaction as numeric(9),
@numDomainId as numeric(9),
@intBillingBreakup INT=0,
@vcBreakupPercentage VARCHAR(2000) ,
@bitBillingTerms BIT=0,
@numBillingDays NUMERIC(9),
@bitBizDocBreakup bit
As      
Begin      
if @dtEndDate ='Jan  1 1753 12:00AM' set  @dtEndDate=null     
    
--Insert    
if @numRecurringId=0     
Begin    
Insert into RecurringTemplate(varRecurringTemplateName,chrIntervalType,tintIntervalDays,tintMonthlyType,tintFirstDet,tintWeekDays,tintMonths,dtStartDate,dtEndDate,bitEndType,bitEndTransactionType,numNoTransaction,numDomainId, intBillingBreakup,vcBreakupPercentage,bitBillingTerms,numBillingDays,bitBizDocBreakup)      
Values(@varRecurringTemplateName,@chrIntervalType,@tintIntervalDays,@tintMonthlyType,@tintFirstDet,@tintWeekDays,@tintMonths,@dtStartDate,@dtEndDate,@bitEndType,@bitEndTransactionType,@numNoTransaction,@numDomainId, @intBillingBreakup,@vcBreakupPercentage,@bitBillingTerms,@numBillingDays,@bitBizDocBreakup)
End    
    
---Update    
if @numRecurringId<>0     
Begin 
IF(SELECT COUNT(*) FROM [OpportunityRecurring] WHERE [numRecurringId] =@numRecurringId)>0
BEGIN
	RAISERROR ('INUSE',16,1);
	RETURN;
END
Update RecurringTemplate Set varRecurringTemplateName=@varRecurringTemplateName,chrIntervalType=@chrIntervalType,tintIntervalDays=@tintIntervalDays,tintMonthlyType=@tintMonthlyType,    
tintFirstDet=@tintFirstDet,tintWeekDays=@tintWeekDays,tintMonths=@tintMonths,dtStartDate=@dtStartDate,dtEndDate=@dtEndDate,bitEndType=@bitEndType,bitEndTransactionType=@bitEndTransactionType,  
numNoTransaction=@numNoTransaction,numDomainId=@numDomainId,
intBillingBreakup=@intBillingBreakup,vcBreakupPercentage=@vcBreakupPercentage,bitBillingTerms=@bitBillingTerms,numBillingDays=@numBillingDays,bitBizDocBreakup=@bitBizDocBreakup Where numRecurringId=@numRecurringId
End    
      
End
