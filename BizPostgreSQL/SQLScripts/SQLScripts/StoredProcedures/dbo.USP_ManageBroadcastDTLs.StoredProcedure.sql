SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj  
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_managebroadcastdtls')
DROP PROCEDURE usp_managebroadcastdtls
GO
CREATE PROCEDURE [dbo].[USP_ManageBroadcastDTLs]   

@numBroID as numeric(9)=0,  
@strBroadCast as text ,  
@numTotalSussessfull as numeric(9)=0   
as

delete from BroadCastDtls where numBroadcastID = @numBroID         
--  update BroadCast set numTotalSussessfull=@numTotalSussessfull where numBroadcastID=@numBroID  
declare @hDoc int    
 EXEC sp_xml_preparedocument @hDoc OUTPUT, @strBroadCast        
        
 insert into BroadCastDtls        
  (numBroadCastID,numDivisionID,numContactID,tintSucessfull)        
         
 select @numBroID,X.numDivisionId,x.numContactId,0 from(        
 SELECT *FROM OPENXML (@hDoc,'/NewDataSet/Table',2)        
 WITH  (        
  numDivisionId numeric(9),        
  numContactId numeric(9),        
  Status tinyint       
  ))X               
 EXEC sp_xml_removedocument @hDoc

