/****** Object:  StoredProcedure [dbo].[usp_GetDivisionFromCompanyForCase]    Script Date: 07/26/2008 16:17:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getdivisionfromcompanyforcase')
DROP PROCEDURE usp_getdivisionfromcompanyforcase
GO
CREATE PROCEDURE [dbo].[usp_GetDivisionFromCompanyForCase]
	@numDomainID numeric(9)=0,
	@vcCompanyName varchar(100)='' 
--  
AS
SELECT     DivisionMaster.vcDivisionName,DivisionMaster.numDivisionID
FROM         DivisionMaster INNER JOIN
                      CompanyInfo ON DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
                      
WHERE CompanyInfo.vcCompanyName=@vcCompanyName
GO
