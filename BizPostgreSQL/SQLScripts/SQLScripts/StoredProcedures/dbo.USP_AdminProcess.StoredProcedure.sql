/****** Object:  StoredProcedure [dbo].[USP_AdminProcess]    Script Date: 07/26/2008 16:14:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_adminprocess')
DROP PROCEDURE usp_adminprocess
GO
CREATE PROCEDURE [dbo].[USP_AdminProcess]
(
@byteMode as tinyint=null,
@numProcessId as numeric(9)=null,
@numSubStageId as numeric(9)=null
)
as
if @byteMode=0 
begin
select numSubStageHDRId ,vcSubStageName as SubStage from SubStageHDR where numProcessId=@numProcessId
end

if @byteMode=1
begin
delete from SubStageHDR where numSubStageHdrID=@numSubStageId
delete from SubStageDetails where numSubStageHdrID =@numSubStageId
end


if @byteMode=2
begin
select numSubStageHdrID,vcSubStageName from SubStageHDR
end
GO
