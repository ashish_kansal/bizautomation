/****** Object:  StoredProcedure [dbo].[USP_GetRecurringPart]    Script Date: 07/26/2008 16:18:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetRecurringPart')
DROP PROCEDURE USP_GetRecurringPart
GO
CREATE PROCEDURE [dbo].[USP_GetRecurringPart]  
(@numoppId as numeric(9)=0,  
@numDomainId as numeric(9)=0,  
@bitVisible as bit output)  
As  
Begin  
 
IF EXISTS(SELECT * FROM RecurrenceConfiguration WHERE numType=1 AND numOppId=@numoppId AND numDomainID=@numDomainId)
 SET @bitVisible = 0 
ELSE
 SET @bitVisible=1

End
GO
