/****** Object:  StoredProcedure [dbo].[USP_GetBudgetDetails]    Script Date: 07/26/2008 16:16:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getbudgetdetails')
DROP PROCEDURE usp_getbudgetdetails
GO
CREATE PROCEDURE [dbo].[USP_GetBudgetDetails] 
(@numDomainId as numeric(9)=0)
As
Begin
 Select numBudgetId,vcBudgetName From  OperationBudgetMaster Where numDomainId=@numDomainId
End
GO
