/****** Object:  StoredProcedure [dbo].[USP_DeleteCampaign]    Script Date: 07/26/2008 16:15:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created ny anoop jayaraj  
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_deletecampaign')
DROP PROCEDURE usp_deletecampaign
GO
CREATE PROCEDURE [dbo].[USP_DeleteCampaign]  
@numCampaignId as numeric(9)=0  ,
@numDomainID  as numeric(9)
as  
  
delete from CampaignMaster where numCampaignId=@numCampaignId  and numDomainID= @numDomainID
GO
