/****** Object:  StoredProcedure [dbo].[Variance_RemAll]    Script Date: 07/26/2008 16:22:01 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
/*
**  Clears all Variances.
**
**  This stored procedure removes the root activity's association with
**  its collection of variances, and deletes all old variances that had
**  been associated with that root activity.
**
**  A clear must happen when the recurrence pattern has changed if
**  variances already exist. Variances are located by their OSDTU,
**  but the new recurrence pattern going into effect no longer
**  coincides with every old variance start date. Additionally,
**  the user's original intentions for creating each variance
**  are not likely applicable in a new recurrence pattern.
*/
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='variance_remall')
DROP PROCEDURE variance_remall
GO
CREATE PROCEDURE [dbo].[Variance_RemAll]
	@ActivityID	int	-- recurring root activity id
AS
BEGIN
	DECLARE @VarianceKey uniqueidentifier;

	-- Read the variance correlation ID from the root activity.
	--
	SELECT
		@VarianceKey = [Activity].[VarianceID]
	FROM
		[Activity]
	WHERE
		( [Activity].[ActivityID] = @ActivityID );

	-- When the root activity has no variances nothing needs to
	-- be done.
	--
	IF ( NOT @VarianceKey IS NULL )
	BEGIN
		BEGIN TRANSACTION

		-- Clear the variance correlation ID off the root
		-- activity first, so it is not picked up by the
		-- delete.
		--
		UPDATE
			[Activity]
		SET
			[Activity].[VarianceID] = NULL
		WHERE
			( [Activity].[ActivityID] = @ActivityID );

		-- Delete any variance rows still associated with
		-- the root activity.
		--
		DELETE FROM [Activity]
		WHERE ( [Activity].[VarianceID] = @VarianceKey );

		COMMIT
	END
END
GO
