/****** Object:  StoredProcedure [dbo].[USP_GetReportTerritories]    Script Date: 07/26/2008 16:18:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getreportterritories')
DROP PROCEDURE usp_getreportterritories
GO
CREATE PROCEDURE [dbo].[USP_GetReportTerritories]        
@numUserCntID as numeric(9),        
@numDomainId AS NUMERIc(9),        
@tintType as tinyint        
as        
select numListItemID as numTerID,vcData as vcTerName from ForReportsByTerritory F     
join ListDetails L         
on L.numListItemID=F.numterritory       
where F.numUserCntID=@numUserCntID  and L.numListID=78     
and F.numDomainId=@numDomainID      
and tintType=@tintType
GO
