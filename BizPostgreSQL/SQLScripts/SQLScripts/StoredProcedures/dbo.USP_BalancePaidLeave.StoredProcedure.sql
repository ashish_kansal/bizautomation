/****** Object:  StoredProcedure [dbo].[USP_BalancePaidLeave]    Script Date: 07/26/2008 16:14:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created By Siva    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_balancepaidleave')
DROP PROCEDURE usp_balancepaidleave
GO
CREATE PROCEDURE [dbo].[USP_BalancePaidLeave]    
(@numUserId as numeric(9)=0,    
@numUserCntId as numeric(9)=0,    
@numDomainId as numeric(9)=0,    
@decCurrentBalancePaidLeaveHrs as float=0 output)    
As    
Begin    
  Declare @decRegularHrsWorked as decimal(10,2)    
  Declare @decOverHrsWorked as decimal(10,2)    
  Declare @decLimDailHrs as decimal(10,2)    
  Declare @decPaidLeaveHrs as decimal(10,2)    
      
  Declare @decTotalHrsWorked as decimal(10,2)    
  Declare @decCurrentPaidLeaveHrs as decimal(10,2)    
      
      
  Select @decLimDailHrs=isnull(numLimDailHrs,0),@decPaidLeaveHrs=isnull(fltPaidLeaveHrs,0) From UserMaster Where numUserId=@numUserId And numDomainID=@numDomainID    
  Select @decRegularHrsWorked=Sum(Cast(datediff(minute,dtfromdate,dttodate) as float)/Cast(60 as float)) from TimeAndExpense Where (numType=1 Or numType=2)  And numCategory=1  And numUserCntID=@numUserCntId And numDomainID=@numDomainId                    
      
  Select @decCurrentPaidLeaveHrs= Sum(Cast(datediff(minute,dtfromdate,dttodate) as float)/Cast(60 as float)) from TimeAndExpense Where numType=3 And numCategory=3 And numUserCntID=@numUserCntId And numDomainID=@numDomainId    
    
     
  Set @decTotalHrsWorked=isnull(@decRegularHrsWorked,0)    
     
  Set @decpaidLeaveHrs=@decTotalHrsWorked/@decPaidLeaveHrs    
    
  Set @decCurrentBalancePaidLeaveHrs=isnull(@decpaidLeaveHrs,0)-isnull(@decCurrentPaidLeaveHrs,0)    
    
      
End
GO
