/****** Object:  StoredProcedure [dbo].[usp_GetNoOfInterest]    Script Date: 07/26/2008 16:17:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getnoofinterest')
DROP PROCEDURE usp_getnoofinterest
GO
CREATE PROCEDURE [dbo].[usp_GetNoOfInterest]
	@numCreatedBy numeric(9)=0,
	@numDomainID numeric(9)=0   
--
AS
BEGIN
	SELECT 
		vcInterested1,
		vcInterested2,
		vcInterested3,
		vcInterested4,
		vcInterested5,
		vcInterested6,
		vcInterested7,
		vcInterested8
		
	FROM 	
		LeadBox
	WHERE numUserID=@numCreatedBy
	and numDomainID=@numDomainID
		 
	
END
GO
