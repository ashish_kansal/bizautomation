/****** Object:  StoredProcedure [dbo].[USP_GetChartAcntTypeId]    Script Date: 07/26/2008 16:16:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created by Siva  
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getchartacnttypeid')
DROP PROCEDURE usp_getchartacnttypeid
GO
CREATE PROCEDURE [dbo].[USP_GetChartAcntTypeId]  
@numChartAcntId as numeric(9)=0,  
@numDomainID as numeric(9)=0  
As  
Begin  
	Select numAcntTypeID from Chart_Of_Accounts Where numAccountId=@numChartAcntId And numDomainID=@numDomainID  
End
GO
