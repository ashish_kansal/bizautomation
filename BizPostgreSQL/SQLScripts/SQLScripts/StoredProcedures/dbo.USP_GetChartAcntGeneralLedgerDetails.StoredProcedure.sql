/****** Object:  StoredProcedure [dbo].[USP_GetChartAcntGeneralLedgerDetails]    Script Date: 07/26/2008 16:16:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created by Siva    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getchartacntgeneralledgerdetails')
DROP PROCEDURE usp_getchartacntgeneralledgerdetails
GO
CREATE PROCEDURE [dbo].[USP_GetChartAcntGeneralLedgerDetails]    
As    
Begin    
    
    
Declare @strSQL as varchar(8000)                                                                                  
Declare @strChildCategory as varchar(5000)                                          
Declare @numAcntTypeId as integer        
Declare @numChartAcntId as integer                                                                         
Declare @OpeningBal as varchar(8000)                                                                              
Set @openingBal='Opening Balance'                                                              
Set @strSQL =''         
    
    
--Create a Temporary table to hold data                                                                                                  
Create table #tempTable ( ID INT IDENTITY PRIMARY KEY,                                                                                                            
 numCustomerId numeric(9),                                                              
 JournalId numeric(9),          
 TransactionType varchar(500),                                                            
 EntryDate datetime,                                                              
 CompanyName varchar(8000),                                                    
 CheckId numeric(9),                                                  
 CashCreditCardId numeric(9),                                                              
 Memo varchar(8000),                                                              
 Payment DECIMAL(20,5),                                                              
 Deposit DECIMAL(20,5),                                                              
 numTransactionId numeric(9),                                                              
 numBalance DECIMAL(20,5),                                              
 numAccountId numeric(9))                                                                                                  
                                                                           
                                                                    
   Set @strSQL=@strSQL + ' Select GJD.numCustomerId AS numCustomerId,GJH.numJournal_Id as JournalId,           
   case when isnull(GJH.numCheckId,0) <> 0 then +''Checks'' Else case when isnull(GJH.numCashCreditCardId,0) <> 0 And CCCD.bitMoneyOut=0 then ''Cash''      
   ELse Case when isnull(GJH.numCashCreditCardId,0) <> 0 And CCCD.bitMoneyOut=1 And CCCD.bitChargeBack=1  then ''Charge''       
   ELse Case when isnull(GJH.numCashCreditCardId,0) <> 0 And CCCD.bitMoneyOut=1 And CCCD.bitChargeBack=0 then ''Credit''       
   Else Case When GJH.numJournal_Id <>0 then ''Journal'' End  End End  End End  as TransactionType,                                
   GJH.datEntry_Date as EntryDate,CI.vcCompanyName AS CompanyName,GJH.numCheckId As CheckId,                                                  
   GJH.numCashCreditCardId as CashCreditCardId,                                     
   GJD.varDescription as Memo,                                                                                            
   (case when GJD.numCreditAmt=0 then GJD.numdebitAmt  end) as Deposit ,                                        
   (case when GJD.numdebitAmt=0 then GJD.numCreditAmt end) as Payment,                                         
   GJD.numTransactionId as numTransactionId,                                                              
   null numBalance,                                              
   isnull(GJD.numChartAcntId,0) as numAcntType                                                                          
   From General_Journal_Header as GJH                                                                                          
   Left Outer Join General_Journal_Details as GJD  on GJH.numJournal_Id=GJD.numJournalId                                                                                   
   Left outer join DivisionMaster as DM on GJD.numCustomerId=DM.numDivisionID                                                             
   Left outer join CompanyInfo as CI on  DM.numCompanyID=CI.numCompanyId                                                                       
   Left outer join Chart_Of_Accounts CA on GJD.numChartAcntId=CA.numAccountId        
   Left outer join CashCreditCardDetails CCCD on GJH.numCashCreditCardId=CCCD.numCashCreditId        
   Left outer join CheckDetails CD on GJH.numCheckId=CD.numCheckId '       
                                                                                         
                                
Print (@strChildCategory)                                                                                  
Print (@strSQL)                                                                                  
Exec (@strSQL)                                                              
                                                          
                         
End
GO
