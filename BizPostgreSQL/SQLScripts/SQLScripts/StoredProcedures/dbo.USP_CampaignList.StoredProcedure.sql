/****** Object:  StoredProcedure [dbo].[USP_CampaignList]    Script Date: 07/26/2008 16:14:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj      
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_campaignlist')
DROP PROCEDURE usp_campaignlist
GO
CREATE PROCEDURE [dbo].[USP_CampaignList]          
          
 @numUserCntID numeric(9)=0,          
 @numDomainID numeric(9)=0,         
 @numRegion numeric(9)=0,          
 @numCampaignType numeric(9)=0,         
 @SortChar char(1)='0',         
 @CurrentPage int,          
 @PageSize int,          
 @TotRecs int output,          
 @columnName as Varchar(50),          
 @columnSortOrder as Varchar(10)         
           
--          
as          
          
          
             
           
          
create table #tempTable ( ID INT IDENTITY PRIMARY KEY,           
 numCampaignId VARCHAR(15),          
 intLaunchDate VARCHAR(25),          
 intEndDate VARCHAR(25),          
 vcCampaignName varchar(200),          
 CampaignType varchar(50),          
 CampaignStatus varchar(50),          
 CampaignRegion varchar(50),          
 monCampaignCost DECIMAL(20,5),          
 income DECIMAL(20,5),          
 ROI  DECIMAL(20,5)         
 )          
          
          
declare @strSql as varchar(8000)          
        
        
-- REPLACE THIS CODE WITH ISNULL(vcCampaignName, '''') AS vcCampaignName if need online/offline text back
--CASE bitIsOnline WHEN 1 THEN  ISNULL(vcCampaignName, '''') + ''<font color="green"> (online) </font>''
--        WHEN 0 THEN ISNULL(vcCampaignName, '''') + ''<font color="purple"> (offline)</font>''
--        END vcCampaignName,
          
set @strSql='select numCampaignId,        
dbo.FormatedDateFromDate(intLaunchDate,'+ Convert(varchar(15),@numDomainID)+') as intLaunchDate,        
dbo.FormatedDateFromDate(intEndDate,'+ Convert(varchar(15),@numDomainID)+') as intEndDate,        
ISNULL(vcCampaignName, '''') AS vcCampaignName,
dbo.fn_GetListItemName(numCampaignType)as CampaignType,        
dbo.fn_GetListItemName(numCampaignStatus)as CampaignStatus,        
dbo.fn_GetListItemName(numRegion)as CampaignRegion,        
--monCampaignCost AS [monCampaignCost1],        
ISNULL((SELECT SUM(monAmount) FROM BillDetails BD JOIN BillHeader BH ON BD.numBillID = BH.numBillID
WHERE numCampaignID = CampaignMaster.numCampaignId AND numDomainID = CampaignMaster.numDomainID ),0) AS [monCampaignCost],
dbo.GetIncomeforCampaign(numCampaignId,2,null,null) as income,        
dbo.GetIncomeforCampaign(numCampaignId,4,null,null) as ROI        
from CampaignMaster        
where CampaignMaster.numDomainID='+ Convert(varchar(15),@numDomainID)         
          
          
if @numRegion <>0 set @strSql=@strSql + ' And numRegion =' + convert(varchar(15),@numRegion)           
if @numCampaignType<>0 set @strSql=@strSql + ' And numCampaignType =' + convert(varchar(15),@numCampaignType)        
if @SortChar <>'0' set @strSql=@strSql + ' And vcCampaignName like '''+@SortChar+'%'''        
if @columnName<>''  set @strSql=@strSql + ' ORDER BY ' + @columnName +' '+ @columnSortOrder        
print @strSql      
insert into #tempTable(          
  numCampaignId ,          
 intLaunchDate,          
 intEndDate,          
 vcCampaignName,          
 CampaignType,          
 CampaignStatus,          
 CampaignRegion,          
 monCampaignCost,          
 income,          
 ROI)          
exec (@strSql)           
          
  declare @firstRec as integer           
  declare @lastRec as integer          
 set @firstRec= (@CurrentPage-1) * @PageSize          
     set @lastRec= (@CurrentPage*@PageSize+1)          
select * from #tempTable where ID > @firstRec and ID < @lastRec          
set @TotRecs=(select count(*) from #tempTable)          
drop table #tempTable
GO
