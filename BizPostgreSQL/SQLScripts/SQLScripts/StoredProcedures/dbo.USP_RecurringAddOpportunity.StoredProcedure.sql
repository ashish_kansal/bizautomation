
GO
/****** Object:  StoredProcedure [dbo].[USP_RecurringAddOpportunity]    Script Date: 03/25/2009 15:12:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Pratik Vasani
-- Create date: Create Date : 23/11/2008
-- Description:	This Stored procedure adds a transaction to Recurring transaction
-- =============================================
-- exec [dbo].[USP_RecurringAddOpportunity] 2157,50,false,2,0,0,null
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_RecurringAddOpportunity')
DROP PROCEDURE USP_RecurringAddOpportunity
GO
CREATE PROCEDURE [dbo].[USP_RecurringAddOpportunity]
               @numOppId               AS BIGINT,
               @numOppBizDocID AS NUMERIC,
               @numRecurringId         AS BIGINT,
               @tintRecurringType   AS TINYINT,
               @dtRecurringDate AS DATETIME,--Start recurring from given date
               @bitRecurringZeroAmt AS BIT=null,
               @numDomainID AS NUMERIC,
               @bitBillingTerms BIT,
               @numBillingDays NUMERIC(9),
               @fltBreakupPercentage FLOAT,
               @ValidationMessage AS VARCHAR(200) =NULL output
AS
  BEGIN
  
    DECLARE @maxPossibleRecurrence INT
    DECLARE @numNoTransaction INT
--    DECLARE @numRecurringId NUMERIC(9)
    DECLARE @bitEndTransactionType BIT
    DECLARE @dtEndDate DATETIME
    DECLARE @vcBreakupPercentage VARCHAR(2000)
    DECLARE @bitBillingTerms1 AS BIT
    DECLARE @numBillingDays1 AS numeric
--    --for BizDocs Recurring Do validation before saving it.
--    IF(@tintRecurringType=2)
--    BEGIN
--    
--    SELECT @maxPossibleRecurrence=MIN(numUnitHour) FROM OpportunityItems
--	WHERE numOppId = @numOppId
--	
	SELECT @bitEndTransactionType=bitEndTransactionType,@numNoTransaction=numNoTransaction,@dtEndDate=dtEndDate,@vcBreakupPercentage=vcBreakupPercentage,
	@bitBillingTerms1=bitBillingTerms,@numBillingDays1=numBillingDays
	FROM RecurringTemplate WHERE numRecurringId = @numRecurringId
	
--	
--		IF (@bitEndTransactionType = 1)
--		  BEGIN
--			-- check no of transaxtion
--			PRINT @maxPossibleRecurrence 
--			IF (@numNoTransaction < 1)
--			  BEGIN
----				RAISERROR ('No of transaction is not specified in Recurring Template',16,1);
--				SET @ValidationMessage ='No of transaction is not specified in Recurring Template'
--				RETURN 
--			  END
--			  IF(@numNoTransaction > @maxPossibleRecurrence)
--			  BEGIN
--			  	SET @ValidationMessage ='No of Possible recurrence in template does not match with product quantity'
--			  	PRINT @ValidationMessage
--				RETURN 
--			  END
--		  END
--		IF (@bitEndTransactionType = 0)
--		  BEGIN
--			--check for End date
----			IF (@dtEndDate IS NULL)
----			  BEGIN
------				RAISERROR ('End date is not specified in selected template',16,1);
----				SET @ValidationMessage ='End date is not specified in selected template'
----				RETURN 
----			  END
--			IF (@dtEndDate < getutcdate())
--			  BEGIN
----				RAISERROR ('End date is expired in selected template',16,1);
--				SET @ValidationMessage ='End date is expired in selected template'
--				RETURN 
--			  END
--		  END
--		END

	
	
    
   
      
      
      --Auth BizDoc Recurring
      IF @tintRecurringType = 2 
      BEGIN
      
			 IF (SELECT COUNT(* ) FROM   opportunityRecurring OPR WHERE  OPR.numOppId = @numOppId) > 0
		  BEGIN
			IF @numOppId <> 0
			  BEGIN
	           
				  IF @numRecurringId =0
				  BEGIN
				 
					IF  EXISTS(SELECT * FROM OpportunityRecurring WHERE numOppId=@numOppID AND ISNULL(numOppBizDocID,0)>0 )
					BEGIN
					
						SET @ValidationMessage ='Can not turn off recurring, your option is to remove existing bizdocs created by service and try again.'
						RETURN
					END
				  
		  			DELETE FROM dbo.OpportunityRecurring WHERE numOppId=@numOppID
		  			RETURN 
				  END
					
				
					UPDATE opportunityRecurring
					SET    numRecurringId = @numRecurringId,
						   bitRecurringZeroAmt = @bitRecurringZeroAmt,
						   tintRecurringType = @tintRecurringType
					WHERE  numOppId = @numOppId
			  END
		  END
      
      
		
		IF NOT EXISTS(SELECT * FROM dbo.OpportunityRecurring WHERE numOppId=@numOppID)
		BEGIN
		
		--Validations
		IF  EXISTS(SELECT * FROM dbo.OpportunityBizDocs WHERE numOppId=@numOppID AND bitAuthoritativeBizDocs=1)
				BEGIN
					SET @ValidationMessage ='Can not turn on recurring, your option is to remove existing autoritative bizdocs and try again.'
					RETURN
				END
		
		
		
		  CREATE TABLE #Temp
		   (RowNumber INT NOT NULL IDENTITY(1,1),
		   ID FLOAT
		   )
		   INSERT INTO #Temp(id)
		   SELECT items FROM dbo.split(@vcBreakupPercentage,'~')
		
		
		 INSERT INTO dbo.OpportunityRecurring (
		  	numOppId,
		  	numOppBizDocID,
		  	numRecurringId,
		  	tintRecurringType,
		  	dtRecurringDate,
		  	bitRecurringZeroAmt,
		  	numNoTransactions,
		  	bitBillingTerms,
		  	numBillingDays,
		  	fltBreakupPercentage,
		  	numDomainID
		  ) 
		   SELECT @numOppID,null,@numRecurringId,@tintRecurringType,[dbo].[GetNthRecurringDate](@dtRecurringDate,X.rownumber,@numRecurringId),@bitRecurringZeroAmt,0,@bitBillingTerms1,@numBillingDays1,X.id AS BreakupPercentage,@numDomainID 
		   FROM 
		   #Temp X
		   
		   DROP TABLE #Temp
		   	
		END
	  END
	  	
--Manuall BizDoc BreakDown
	  	IF @tintRecurringType = 3 
      BEGIN
      DECLARE @fltTotalPercentage FLOAT
      
      SELECT @fltTotalPercentage =SUM(fltBreakupPercentage)  FROM dbo.OpportunityRecurring WHERE numOppId=@numOppID
      
      SET @fltTotalPercentage = ISNULL(@fltTotalPercentage,0)+ @fltBreakupPercentage 
		IF @fltTotalPercentage >100
		BEGIN
					SET @ValidationMessage ='Total Breakup Percentage is higher than 100%,Please reduce Breakup Percentage and try again.'
					RETURN
		END
		--Validations
--		IF  EXISTS(SELECT * FROM dbo.OpportunityBizDocs WHERE numOppId=@numOppID)
--				BEGIN
--					SET @ValidationMessage ='Can not turn on recurring, your option is to remove existing autoritative bizdocs and try again.'
--					RETURN
--		END
--		
		
		
		
		
			 INSERT INTO dbo.OpportunityRecurring (
		  		numOppId,
		  		numOppBizDocID,
		  		numRecurringId,
		  		tintRecurringType,
		  		dtRecurringDate,
		  		bitRecurringZeroAmt,
		  		numNoTransactions,
		  		bitBillingTerms,
		  		numBillingDays,
		  		fltBreakupPercentage,
		  		numDomainID
			  ) 
			 SELECT @numOppID,
					null,
					null,
					@tintRecurringType,
					@dtRecurringDate,
					@bitRecurringZeroAmt,
					0,
					@bitBillingTerms,
					@numBillingDays,
					@fltBreakupPercentage,
					@numDomainID 
			   
		   	
		END

	--Deferred BizDocs
	ELSE IF @tintRecurringType = 4
      BEGIN
			 INSERT INTO dbo.OpportunityRecurring (
		  		numOppId,
		  		numOppBizDocID,
		  		numRecurringId,
		  		tintRecurringType,
		  		dtRecurringDate,
		  		bitRecurringZeroAmt,
		  		numNoTransactions,
		  		bitBillingTerms,
		  		numBillingDays,
		  		fltBreakupPercentage,
		  		numDomainID
			  ) 
			 SELECT @numOppID,
					@numOppBizDocID,
					null,
					@tintRecurringType,
					@dtRecurringDate,
					@bitRecurringZeroAmt,
					0,
					@bitBillingTerms,
					@numBillingDays,
					@fltBreakupPercentage,
					@numDomainID 
			   
		   	
		END 
	END
    
      
      
  
