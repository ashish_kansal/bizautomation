/****** Object:  StoredProcedure [dbo].[USP_SubScriberList]    Script Date: 07/26/2008 16:21:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
declare @p8 int
set @p8=0
exec USP_SubScriberList @numUserCntID=1,@tintSortOrder=1,@SortChar='0',@numDomainID=1,@SearchWord=NULL,@CurrentPage=1,@PageSize=20,@TotRecs=@p8 
output,@columnName='numSubscriberID',@columnSortOrder='Desc',@ClientTimeZoneOffset=-330
select @p8
*/
--- Created By Anoop Jayaraj                                                      
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_subscriberlist')
DROP PROCEDURE usp_subscriberlist
GO
CREATE PROCEDURE [dbo].[USP_SubScriberList]                                                                                             
@numUserCntID numeric,                                                                                           
@tintSortOrder tinyint,                                                                            
@SortChar char(1)='0',                                  
@numDomainID as numeric(9)=0,                                                        
@SearchWord varChar(100)= '',                                                                                            
@CurrentPage int,                                                      
@PageSize int,                                                      
@TotRecs int output,                                                      
@columnName as Varchar(50),                                                      
@columnSortOrder as Varchar(10),        
@ClientTimeZoneOffset Int ,
@ActiveUsers float output
as                                                      


 declare @firstRec as integer                                                      
 declare @lastRec as integer                                                      
 set @firstRec= (@CurrentPage-1) * @PageSize                                                      
 set @lastRec= (@CurrentPage*@PageSize+1)                                                      
 
 
 
                                                     
                     
declare @strSql as varchar(8000)                    
                  
                  
set @strSql='With tblSubscriber AS (SELECT ROW_NUMBER() OVER (ORDER BY '+@columnName+' '+ @columnSortOrder+') AS RowNumber,numSubscriberID,  CASE WHEN Subscribers.numTargetDomainID IN (1,72) THEN 0 ELSE Subscribers.numTargetDomainID END AS numDomainID                
 FROM Subscribers '                  
 set @strSql=@strSql +' Join Divisionmaster D on D.numDivisionID= Subscribers.numDivisionID Join Companyinfo C                  
on C.numCompanyID=D.numCompanyID '                  
                  
  set @strSql=@strSql +' Left Join AdditionalContactsInformation A on A.numContactID= Subscribers.numAdminContactID '                  
                  
                  
    set @strSql=@strSql +' where bitDeleted=0 '                  
                  
if @SortChar<>'0' set @strSql=@strSql +' and Subscribers.numDivisionID in (select D.numDivisionID from DivisionMaster D                  
 Join CompanyInfo C on C.numCompanyID=D.numCompanyID where vcCompanyName like '''+ @SortChar+'%'')'                   
                  
if @SearchWord<>''                  
begin                  
 if @tintSortOrder=1 set @strSql=@strSql +' and D.numDivisionID in (select numDivisionID from DivisionMaster D                  
 Join CompanyInfo C on C.numCompanyID=D.numCompanyID where vcCompanyName like ''%'+ @SearchWord+'%'')'                  
 else if @tintSortOrder=2 set @strSql=@strSql +' and D.numDivisionID in (select numDivisionID from AdditionalContactsInformation A                  
 where vcEmail like ''%'+ @SearchWord+'%'')'                  
end
 if @tintSortOrder=3 set @strSql=@strSql +' and bitTrial =1 '                  
 else if @tintSortOrder=4 set @strSql=@strSql +' and bitActive =1 '                  
 else if @tintSortOrder=5 set @strSql=@strSql +' and bitActive =0 '                  
 else if @tintSortOrder=6 set @strSql=@strSql +' and bitActive =0 and dtSuspendedDate <='''+ convert(varchar(30),dateadd(day,30,getutcdate()))+''''                  
 else if @tintSortOrder=7 set @strSql=@strSql +' and bitActive =0 and dtSuspendedDate >='''+ convert(varchar(30),dateadd(day,30,getutcdate()))+''''                  
                   
                  
set @strSql=@strSql +')                     
 select RowNumber,T.numSubscriberID,S.numDivisionID,numContactID,vcCompanyName,                  
 COALESCE(vcFirstName,'''')+ '' ''+COALESCE(vcLastname,'''') AdminContact,COALESCE(intNoofUsersSubscribed,0) + COALESCE(intNoofPartialSubscribed,0)/2 + COALESCE(intNoofMinimalSubscribed,0)/10 as intNoofUsersSubscribed,                  
 intNoOfPartners,FormatedDateTimeFromDate(dtSubStartDate,'+Convert(varchar(15),@numDomainId) +') as dtSubStartDate,    
FormatedDateTimeFromDate(dtSubEndDate,'+Convert(varchar(15),@numDomainId) +') as dtSubEndDate,case when bitTrial=0 then ''No'' when bitTrial      
=1 then ''Yes'' else ''No'' End as Trial,            
 case when bitActive=0 then ''Suspended'' when bitActive=1 then ''Active'' else ''Trial'' End as  Status,FormatedDateTimeFromDate(dtSuspendedDate+ INTERVAL '+Convert(varchar(10), -@ClientTimeZoneOffset)+' minute'','+Convert(varchar(15),@numDomainId)+
  
    
') as dtSuspendedDate,              
 vcSuspendedReason,vcEmail, COALESCE((SELECT SUM(COALESCE(SpaceOccupied,0)) FROM View_InboxCount where numDomainID=S.numTargetDomainID),0) as TotalSize, T.numDomainID 
 from tblSubscriber T                  
 join Subscribers S                  
 on S.numSubscriberID=T.numSubscriberID           
 join DivisionMaster D                  
 on D.numDivisionID=S.numDivisionID                  
 join Companyinfo C                  
 on C.numCompanyID=D.numCompanyID                  
 Left join  AdditionalContactsInformation A                  
 on A.numContactID=S.numAdminContactID                  
 Where RowNumber > '+convert(varchar(10),@firstRec)+' and RowNumber < '+convert(varchar(10),@lastRec)+'                   
 union select 0,count(*),null,null,null,null,null,null,null,null,null,null,null,null,null,null,null from tblSubscriber order by RowNumber'                  
print @strSql                  
exec (@strSql)

SELECT @ActiveUsers =COALESCE(SUM( COALESCE(intNoofUsersSubscribed,0) + COALESCE(intNoofPartialSubscribed,0)/2 + COALESCE(intNoofMinimalSubscribed,0)/10) ,0)  
 from Subscribers S
 JOIN Divisionmaster D ON D.numDivisionID = S.numDivisionID
                        JOIN Companyinfo C ON C.numCompanyID = D.numCompanyID
                        JOIN AdditionalContactsInformation A ON A.numContactID = S.numAdminContactID
 WHERE S.bitActive=1 AND S.bitDeleted = 0
 
 
GO
