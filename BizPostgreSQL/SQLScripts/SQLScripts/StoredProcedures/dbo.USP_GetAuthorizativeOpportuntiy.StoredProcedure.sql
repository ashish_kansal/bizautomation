/****** Object:  StoredProcedure [dbo].[USP_GetAuthorizativeOpportuntiy]    Script Date: 07/26/2008 16:16:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created By Siva    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getauthorizativeopportuntiy')
DROP PROCEDURE usp_getauthorizativeopportuntiy
GO
CREATE PROCEDURE [dbo].[USP_GetAuthorizativeOpportuntiy]    
@numDomainId as numeric(9)=0,  
@numOppId as numeric(9)=0    
As    
Begin   
Declare @tintOppType as tinyint


 Select @tintOppType=tintOppType From OpportunityMaster Where numOppId=@numOppId And  numDomainID=@numDomainID         
    Print @tintOppType 
 
 If @tintOppType=1
  Select isnull(numAuthoritativeSales,0) From AuthoritativeBizDocs Where numDomainId=@numDomainId    
 Else If @tintOppType=2 
  Select isnull(numAuthoritativePurchase,0) From AuthoritativeBizDocs Where numDomainId=@numDomainId 
End
GO
