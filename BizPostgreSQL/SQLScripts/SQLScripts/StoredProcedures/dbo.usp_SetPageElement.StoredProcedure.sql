/****** Object:  StoredProcedure [dbo].[usp_SetPageElement]    Script Date: 07/26/2008 16:21:17 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_setpageelement')
DROP PROCEDURE usp_setpageelement
GO
CREATE PROCEDURE [dbo].[usp_SetPageElement]
	@numModuleID INT,
	@numPageID INT,
	@vcOldElementInitiatingAction VARCHAR(20) = '',
	@vcElementInitiatingAction VARCHAR(20) = '',
	@vcElementInitiatingActionDesc VARCHAR(50) = '',
	@vcElementInitiatingActionType VARCHAR(30) = '',
	@vcOldChildElementInitiatingAction VARCHAR(50) = '',
	@vcChildElementInitiatingAction VARCHAR(50) = '',
	@vcChildElementInitiatingActionType VARCHAR(30) = '',
	@vcActionName VARCHAR(15),
	@vcActionIndicator CHAR (1)   
--
AS
	IF @vcActionIndicator = 'A'
		BEGIN
			INSERT INTO PageActionElements (numModuleID, numPageID, vcActionName, vcElementInitiatingAction, 
					vcElementInitiatingActionDesc, vcElementInitiatingActionType, vcChildElementInitiatingAction,
					vcChildElementInitiatingActionType)
				VALUES(@numModuleID, @numPageID, @vcActionName, @vcElementInitiatingAction,
					@vcElementInitiatingActionDesc, @vcElementInitiatingActionType, @vcChildElementInitiatingAction,
					@vcChildElementInitiatingActionType)
		END
	ELSE
		BEGIN
			UPDATE PageActionElements 
				SET vcActionName = @vcActionName,
				vcElementInitiatingAction = @vcElementInitiatingAction,
				vcElementInitiatingActionDesc = @vcElementInitiatingActionDesc,
				vcElementInitiatingActionType = @vcElementInitiatingActionType,
				vcChildElementInitiatingAction = @vcChildElementInitiatingAction,
				vcChildElementInitiatingActionType = @vcChildElementInitiatingActionType
			WHERE numModuleID = @numModuleID
			AND numPageID = @numPageID
			AND vcElementInitiatingAction = @vcOldElementInitiatingAction
			AND vcChildElementInitiatingAction = @vcOldChildElementInitiatingAction
		END
GO
