/****** Object:  StoredProcedure [dbo].[usp_GetAvailableFieldsUser1]    Script Date: 07/26/2008 16:16:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getavailablefieldsuser1')
DROP PROCEDURE usp_getavailablefieldsuser1
GO
CREATE PROCEDURE  [dbo].[usp_GetAvailableFieldsUser1]          
@numGroupType numeric,          
@numDomainId numeric,          
@numContactId numeric          
          
as         
          
		 if (select count(*) from ShortCutUsrcnf where bittype=1 and numDomainId = @numDomainId and numGroupID =@numGroupType and numContactId =@numContactId ) > 0            
			begin          
			      
					 (Select Hdr.id,Hdr.vclinkname,sconf.numOrder as [Order] from ShortCutGrpconf Sconf join  ShortCutBar Hdr on  Hdr.id =Sconf.numLinkId               
					   where sconf.bittype=1 and sconf.numDomainId = @numDomainId and sconf.numGroupID =@numGroupType  and numContactId = 0  and Sconf.numLinkId not in        
					 (select numLinkId from ShortCutUsrcnf where bittype=1 and numDomainId = @numDomainId and numGroupID =@numGroupType     
						and numContactId =@numContactId)          
						union     
					    
					 select Hdr.id,Hdr.vclinkname,[order] from  ShortCutBar Hdr where hdr.bittype=1 and hdr.numDomainId = @numDomainId and hdr.numGroupID =@numGroupType    
					  and numContactId =@numContactId and id not in     
					(select numLinkId from ShortCutUsrcnf where bittype=1 and numDomainId = @numDomainId and numGroupID =@numGroupType     
						and numContactId =@numContactId)    
					 ) order by [order]    
					    
					    
						Select Hdr.id,Hdr.vclinkname from ShortCutUsrcnf Sconf join  ShortCutBar Hdr on  Hdr.id =Sconf.numLinkId               
					   where sconf.bittype=1 and sconf.numDomainId = @numDomainId and sconf.numGroupID =@numGroupType  and sconf.numContactId =@numContactId  order by sconf.numOrder            
					        
			 end          
		 else          
		  begin          
				 (select Hdr.id,Hdr.vclinkname,numorder as [order] from ShortCutGrpConf  Sconf  join  ShortCutBar Hdr  on  Hdr.id=Sconf.numLinkId  where Sconf.bitType=1 And Sconf.numGroupID =@numGroupType        
				 and Sconf.numDomainId = @numDomainId and numContactId =0     
					union    
				  select id,vclinkname,[order] from ShortCutBar where bitType=1 and bitdefault = 0 and numGroupID =@numGroupType        
				 and numDomainId = @numDomainId   and numContactId =@numcontactId ) order by [order    ]    
				            
				 select Hdr.id,Hdr.vclinkname from ShortCutGrpConf  Sconf  join  ShortCutBar Hdr  on  Hdr.id=Sconf.numLinkId  where  Sconf.numGroupID =0        
				        
				        
		  end          
      
       
        
        
 if (select count(*) from ShortCutUsrcnf where bittype=0 and numDomainId = @numDomainId and numGroupID =@numGroupType  and numContactId =@numContactId) > 0              
	  begin              
	                  
	   Select Hdr.id,Hdr.vclinkname from ShortCutGrpconf Sconf join  ShortCutBar Hdr on  Hdr.id =Sconf.numLinkId               
	   where sconf.bittype=0 and sconf.numDomainId = @numDomainId and sconf.numGroupID =@numGroupType   and numContactId =0      
		 and Sconf.numLinkId not in        
		 (select numLinkId from ShortCutUsrcnf sconf where sconf.bittype=0 and sconf.numDomainId = @numDomainId and sconf.numGroupID =@numGroupType  and numContactId =@numContactId)        
		 order by sconf.numOrder              
	           
	   Select Hdr.id,Hdr.vclinkname from ShortCutUsrcnf Sconf join  ShortCutBar Hdr on  Hdr.id =Sconf.numLinkId               
	   where sconf.bittype=0 and sconf.numDomainId = @numDomainId and sconf.numGroupID =@numGroupType  and sconf.numContactId =@numContactId order by sconf.numOrder            
	          
               
end              
               
               
               
 else              
  begin              
 select Hdr.id,Hdr.vclinkname from ShortCutGrpConf  Sconf  join  ShortCutBar Hdr  on  Hdr.id=Sconf.numLinkId  where Sconf.bitType=0 And Sconf.numGroupID =@numGroupType        
 and numContactId =0 and Sconf.numDomainId = @numDomainId  order by sconf.numorder        
        
            
 select Hdr.id,Hdr.vclinkname from ShortCutGrpConf  Sconf  join  ShortCutBar Hdr  on  Hdr.id=Sconf.numLinkId  where  Sconf.numGroupID =0             
  end
GO
