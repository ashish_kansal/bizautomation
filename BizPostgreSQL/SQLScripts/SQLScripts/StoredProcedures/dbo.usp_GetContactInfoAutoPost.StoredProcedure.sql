/****** Object:  StoredProcedure [dbo].[usp_GetContactInfoAutoPost]    Script Date: 07/26/2008 16:16:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getcontactinfoautopost')
DROP PROCEDURE usp_getcontactinfoautopost
GO
CREATE PROCEDURE [dbo].[usp_GetContactInfoAutoPost]          
 @vcCompanyname varchar(50)          

 as

 SELECT vcLastName, vcFirstName,vcFirstName+' '+vcLastName as [Name],vcGivenName, vcPosition, vcEmail, vcDepartment,          
    numPhone, numPhoneExtension, vcFax, numContacttype,   vcData ,            
    numContactId, bintDOB, txtNotes, AC.numCreatedBy,          
   DM.numTerID, --New          
   DM.numCreatedBy --New          
   FROM AdditionalContactsInformation AC join           
   divisionmaster DM on DM.numDivisionID=AC.numDivisionID         
 left join ListDetails lst on ac.numContacttype=lst.numListItemID        
   WHERE AC.numDivisionID in (select d.numdivisionid from companyinfo a join divisionmaster d                        
   on  a.numCompanyid=d.numCompanyid  where vcCompanyname like @vcCompanyname+'%')
GO
