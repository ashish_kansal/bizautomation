/****** Object:  StoredProcedure [dbo].[USP_OPPContactInfo]    Script Date: 07/26/2008 16:20:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--modified by anoop jayaraj        
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_oppcontactinfo')
DROP PROCEDURE usp_oppcontactinfo
GO
CREATE PROCEDURE [dbo].[USP_OPPContactInfo]                     
@numOppId as numeric(9)=0,
@numDomainId as numeric(9)           
AS                     
                   
 SELECT  D.numDivisionID,a.numContactId,vcCompanyName+', '+isnull(Lst.vcData,'-') as Company,         
  a.vcFirstname +' '+ a.vcLastName as [Name],          
  case when a.numPhone<>'' then + a.numPhone +case when a.numPhoneExtension<>'' then ' - ' + a.numPhoneExtension else '' end  else '' end as [Phone],                                                                                                          
 a.vcEmail as Email,          
  b.vcData as ContactRole,          
  opp.numRole as ContRoleId,         
  convert(integer,isnull(bitPartner,0)) as bitPartner,         
  a.numcontacttype,CASE WHEN opp.bitSubscribedEmailAlert=1 THEN 'Yes' ELSE 'No' END AS SubscribedEmailAlert FROM OpportunityContact opp          
  join additionalContactsinformation a on          
  a.numContactId=opp.numContactId        
  join DivisionMaster D        
  on a.numDivisionID =D.numDivisionID        
  join CompanyInfo C        
  on D.numCompanyID=C.numCompanyID         
  left join listdetails b           
  on b.numlistitemid=opp.numRole        
  left join listdetails Lst        
  on Lst.numlistitemid=C.numCompanyType        
  WHERE opp.numOppId=@numOppId and A.numDomainID=@numDomainId
GO
