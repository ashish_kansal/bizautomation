/****** Object:  StoredProcedure [dbo].[USP_GetBroadcastDTLs]    Script Date: 07/26/2008 16:16:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getbroadcastdtls')
DROP PROCEDURE usp_getbroadcastdtls
GO
CREATE PROCEDURE [dbo].[USP_GetBroadcastDTLs]
@numBroadCastID as numeric(9)=0
as 
select vcFirstName+' '+vcLastname as [Name],vcCompanyName+', '+vcDivisionName as Company,vcEmail,case when tintSucessfull=0 then 'No' when tintSucessfull=1 then 'Yes' end as Sent,isnull(convert(varchar(15),numNoofTimes),'-') as numNoofTimes ,intNoOfClick ,bitUnsubscribe from BroadCastDtls Dtl
join Broadcast B 
on B.numBroadCastId=Dtl.numBroadCastID
join AdditionalContactsInformation A
on DTL.numContactID=A.numContactID
join DivisionMaster D
on D.numDivisionID=DTL.numDivisionID
join CompanyInfo C
on D.numCompanyID=C.numCompanyID
where DTL.numBroadCastID= @numBroadCastID
GO
