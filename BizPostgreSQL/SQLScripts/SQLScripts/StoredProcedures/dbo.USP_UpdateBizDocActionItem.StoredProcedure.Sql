
GO
/****** Object:  StoredProcedure [dbo].[USP_UpdateBizDocActionItem]    Script Date: 03/03/2010 12:28:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_updatebizdocactionitem')
DROP PROCEDURE usp_updatebizdocactionitem
GO
CREATE PROCEDURE [dbo].[USP_UpdateBizDocActionItem]
(@numDomainId numeric(18),
@numBizActionId numeric(18),
@numOppBizDocsId numeric(18),
@numStatus numeric(1),
@btType bit,
@vcComment varchar(1000) = Null)
as
begin

declare @BizActionCount numeric(18);

update BizActionDetails set btStatus=@numStatus,dtApprovedOn=getutcdate(),vcComment=@vcComment where numBizActionId=@numBizActionId and numOppBizDocsId=@numOppBizDocsId and btDocType=@btType;

set  @BizActionCount=0;

select @BizActionCount = count(btStatus) from BizActionDetails where numBizActionId=@numBizActionId and btStatus = 0;

update BizDocAction set numStatus= (Case @BizActionCount when 0 then 1 else 0 end) where numBizActionId=@numBizActionId;

if @btType=0
	begin
		update Documentworkflow set tintApprove=@numStatus , dtApprovedOn=getutcdate(),vcComment=@vcComment  where numDocID=@numOppBizDocsId 
		and numContactID in (select numContactId from BizDocAction where numBizActionId=@numBizActionId )
	end
else
	begin
		update Documentworkflow set tintApprove=@numStatus , dtApprovedOn=getutcdate(),vcComment=@vcComment  where numDocID=@numOppBizDocsId 
		and numContactID in (select numContactId from BizDocAction where numBizActionId=@numBizActionId ) and 
		cDocType='B'
	end


declare @AcceptCount numeric(8);
declare @DeclineCount numeric(8);

set @DeclineCount=0;
SELECT @DeclineCount=COUNT(btStatus) from BizActionDetails where numOppBizDocsId=@numOppBizDocsId and btDocType=@btType and btStatus=2

if @BizActionCount=0
	begin
		--SELECT @AcceptCount=COUNT(btStatus) from BizActionDetails where numOppBizDocsId=@numOppBizDocsId and btDocType=@btType and btStatus=1
		
		if @DeclineCount>0 
			begin
				update OpportunityBizDocs set 
				numBizDocStatus=(select (case @DeclineCount when 0 then numApproveDocStatusID  else numDeclainDocStatusID end ) from  BizDocApprovalRule where numBizDocAppId in 
				(select numBizDocAppId from BizDocAction where numBizActionId=@numBizActionId))
				where numOppBizDocsId=@numOppBizDocsId;
			end
	end 
else
	begin
		update OpportunityBizDocs set 
				numBizDocStatus=(select (case @DeclineCount when 0 then null  else numDeclainDocStatusID end ) from  BizDocApprovalRule where numBizDocAppId in 
				(select numBizDocAppId from BizDocAction where numBizActionId=@numBizActionId))
				where numOppBizDocsId=@numOppBizDocsId;
	end

--if @BizActionCount=0
--	begin
--		update BizDocAction set numStatus=1 where numBizActionId=@numBizActionId;
--	end
--else
--	begin
--		update BizDocAction set numStatus=0 where numBizActionId=@numBizActionId;
--	end

SELECT 'I'

end

--select * from OpportunityBizDocs where numOppBizDocsId=2455
--SELECT * FROM BizActionDetails
--
--SELECT * FROM BizDocAction
--
--update BizDocAction set numStatus=0 where numBizActionId=1;
--
--update BizActionDetails set btStatus=0 where numBizActionId=1 and numOppBizDocsid=342
--select * from Documentworkflow where numDocId=342
--select * from BizDocApprovalRule
--select * from BizActionDetails