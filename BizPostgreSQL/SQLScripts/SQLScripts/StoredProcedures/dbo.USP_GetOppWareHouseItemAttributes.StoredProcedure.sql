/****** Object:  StoredProcedure [dbo].[USP_GetOppWareHouseItemAttributes]    Script Date: 07/26/2008 16:18:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getoppwarehouseitemattributes')
DROP PROCEDURE usp_getoppwarehouseitemattributes
GO
CREATE PROCEDURE [dbo].[USP_GetOppWareHouseItemAttributes]
@numRecID as numeric(9)=0,
@bitSerialize as bit
AS
BEGIN
	IF ISNULL(@numRecID,0) > 0
	BEGIN
		SELECT dbo.fn_GetAttributes(@numRecID,@bitSerialize)
	END
	ELSE
	BEGIN
		SELECT ''
	END
END
GO
