/****** Object:  StoredProcedure [dbo].[USP_RollUpBudgets]    Script Date: 07/26/2008 16:20:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created by Siva
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_rollupbudgets')
DROP PROCEDURE usp_rollupbudgets
GO
CREATE PROCEDURE [dbo].[USP_RollUpBudgets]
(@numDomainId as numeric(9)=0,                                                
 @intFiscalYear as int=0,     
 @tintType as int=0
   ) 
As
Begin

  Declare @strSQL as varchar(max)                   
 Declare @strMonSQL as varchar(8000)     
 Declare @strProcureSQL as varchar(8000)   
 Declare @strMarketSQL as varchar(8000)  
 Declare @dtFiscalStDate as datetime                          
 Declare @dtFiscalEndDate as datetime                 
 Set @strMonSQL='' 
 Set @strProcureSQL=''   
                
 Set @strMarketSQL=''
 Declare @numMonth as tinyint  
Declare @len as varchar(1000)                   
 set @numMonth =1                                
 --Select @numMonth=tintFiscalStartMonth From Domain Where numDomainId=@numDomainId                            
    Declare @Date as datetime                
 set @Date = dateadd(year,@tintType,getutcdate())          
     
 While @numMonth <=12                            
  Begin                       
                    
                   
     Set @dtFiscalStDate =dbo.GetFiscalStartDate(dbo.GetFiscalyear(@Date,@numDomainId),@numDomainId)                       
      Set @dtFiscalStDate=dateadd(month,@numMonth-1,@dtFiscalStDate)   
    Set @dtFiscalEndDate =  dateadd(day,-1,dateadd(year,1,dbo.GetFiscalStartDate(dbo.GetFiscalyear(@Date,@numDomainId),@numDomainId)))                    
    
       Set @strMonSQL=@strMonSQL+' '+  '(Select sum(monAmount) from OperationBudgetMaster OBM inner join OperationBudgetDetails OBD On OBM.numBudgetId=OBD.numBudgetId 
                                        Where OBM.numDomainId='+Convert(varchar(10),@numDomainId)+' And OBD.numChartAcntId=RecordID And OBD.tintmonth='+Convert(varchar(10),month(@dtFiscalStDate)) +' And OBD.intYear='+Convert(varchar(10),year(@dtFiscalStDate))+')'
										+ ' as ''' + Convert(varchar(2),month(@dtFiscalStDate)) +'~'+Convert(varchar(4),year(@dtFiscalStDate))+''','                           
              
      Set  @strProcureSQL=@strProcureSQL+' '+ ' (Select sum(monAmount) From ProcurementBudgetMaster PBM Inner join ProcurementBudgetDetails PBD on PBM.numProcurementBudgetId=PBD.numProcurementId Where PBD.tintMonth='+Convert(varchar(2),month(@dtFiscalStDate))+' And PBD.intYear='+Convert(varchar(4),year(@dtFiscalStDate))
							+' And PBD.numItemGroupId=IG.numItemGroupID And PBM.numDomainId='+Convert(varchar(10),@numDomainId)+')'
							 + ' as ''' +Convert(varchar(2),month(@dtFiscalStDate)) +'~'+Convert(varchar(4),year(@dtFiscalStDate))+''','  
      Set @strMarketSQL=@strMarketSQL +''+' (Select Sum(monAmount) From MarketBudgetMaster MBM Inner join MarketBudgetDetails MBD on   MBM.numMarketBudgetId=MBD.numMarketId Where MBD.tintMonth='+Convert(varchar(2),month(@dtFiscalStDate))+' And MBD.intYear='+Convert(varchar(4),year(@dtFiscalStDate))   
								+' And MBD.numListItemID=LD.numListItemID And MBM.numDomainId='+Convert(varchar(10),@numDomainId)+')'
							 + ' as ''' +Convert(varchar(2),month(@dtFiscalStDate)) +'~'+Convert(varchar(4),year(@dtFiscalStDate))+''','  
     Set @numMonth=@numMonth+1                            
  End       
     Set @dtFiscalStDate =dbo.GetFiscalStartDate(dbo.GetFiscalyear(@Date,1),1)                  
     
Set @strMonSQL = @strMonSQL + '(  Select Sum(OBD.monAmount)  From OperationBudgetMaster OBM
inner join OperationBudgetDetails OBD on OBM.numBudgetId=OBD.numBudgetId
 Where OBM.numDomainId='+Convert(varchar(10),@numDomainId)+' And OBD.numChartAcntId=RecordID
   And (((OBD.tintMonth between Month('''+Convert(varchar(100),@dtFiscalStDate)+''') and 12) and OBD.intYear=Year('''+Convert(varchar(100),@dtFiscalStDate)+''')) Or ((OBD.tintMonth between 1 and month('''+Convert(varchar(100),@dtFiscalEndDate)+''')) and OBD.intYear=Year('''+Convert(varchar(100),@dtFiscalEndDate)+'''))))  as Total'                
 

 Set @strProcureSQL = @strProcureSQL + '(  Select Sum(PBD.monAmount)  From ProcurementBudgetMaster PBM
inner join ProcurementBudgetDetails PBD on PBM.numProcurementBudgetId=PBD.numProcurementId
 Where PBM.numDomainId='+Convert(varchar(10),@numDomainId)+' And PBD.numItemGroupId=IG.numItemGroupID  And (((PBD.tintMonth between Month('''+Convert(varchar(100),@dtFiscalStDate)+''') and 12) and PBD.intYear=Year('''+Convert(varchar(100),@dtFiscalStDate)+''')) Or ((PBD.tintMonth between 1 and month('''+Convert(varchar(100),@dtFiscalEndDate)+''')) and PBD.intYear=Year('''+Convert(varchar(100),@dtFiscalEndDate)+'''))))  as Total'                
     Set @strMarketSQL = @strMarketSQL + '(  Select Sum(MBD.monAmount)  From MarketBudgetMaster MBM
inner join MarketBudgetDetails MBD on MBM.numMarketBudgetId=MBD.numMarketId
 Where MBM.numDomainId='+Convert(varchar(10),@numDomainId)+' And MBD.numListItemID=LD.numListItemID  And (((MBD.tintMonth between Month('''+Convert(varchar(100),@dtFiscalStDate)+''') and 12) and MBD.intYear=Year('''+Convert(varchar(100),@dtFiscalStDate)+''')) Or ((MBD.tintMonth between 1 and month('''+Convert(varchar(100),@dtFiscalEndDate)+''')) and MBD.intYear=Year('''+Convert(varchar(100),@dtFiscalEndDate)+'''))))  as Total'                
                                      
Set @strSQL='  with RecursionCTE (RecordID,ParentRecordID,vcCatgyName,TOC,T)                      
     as (select numAccountId,numParntAcntId,vcCatgyName,convert(varchar(1000),'''') TOC,convert(varchar(1000),'''') T                      
      from Chart_Of_Accounts                      
     where numParntAcntId is null and numDomainID='+Convert(varchar(10),@numDomainId)                      
       +' union all                      
       select R1.numAccountId,R1.numParntAcntId,R1.vcCatgyName,case when DataLength(R2.TOC) > 0 
		then convert(varchar(1000),case when CHARINDEX(''.'',R2.TOC)>0 then R2.TOC else R2.TOC +''.'' end                      
       + cast(R1.numAccountId as varchar(10)))else convert(varchar(1000),cast(R1.numAccountId as varchar(10)))                       
         end as TOC,case when DataLength(R2.TOC) > 0 then  convert(varchar(1000),''&nbsp;&nbsp;''+ R2.T)                      
         else convert(varchar(1000),'''') end as T  from Chart_Of_Accounts as R1 
        join RecursionCTE as R2 on R1.numParntAcntId = R2.RecordID and R1.numDomainID='+ Convert(varchar(10),@numDomainId) +' and R1.numAccountId in(Select OBD.numChartAcntId From  OperationBudgetMaster OBM                        
      inner join OperationBudgetDetails OBD on OBM.numBudgetId=OBD.numBudgetId                                          
      Where  OBD.intYear='+Convert(varchar(5),@intFiscalYear)+        
     ' And OBM.numDomainId='+convert(varchar(5),@numDomainId)+'))                       
    select RecordID as numChartAcntId,ParentRecordID as numParentAcntId,T+vcCatgyName as vcCategoryName,TOC,'+Convert(varchar(8000),@strMonSQL)+' from RecursionCTE  Where ParentRecordID is not null '                      
 
  Set @strSQL=@strSQL +' union all 

 Select IG.numItemGroupID as numChartAcntId,null as numParentAcntId,IG.vcItemGroup as vcCategoryName,null as TOC,' +Convert(varchar(8000),@strProcureSQL)      
            +' From ItemGroups IG Where IG.numDomainID='+Convert(varchar(10),@numDomainId)

  Set @strSQL=@strSQL +' union all 
     Select LD.numListItemID as numChartAcntId,null as numParentAcntId,LD.vcData as vcCategoryName,null as TOC,' +Convert(varchar(8000),@strMarketSQL)      
            +' From ListDetails LD Where LD.numListId=22 And LD.numDomainID='+Convert(varchar(10),@numDomainId)+ ' Order by TOC'          
   
  print @strSQL                                   
  Exec (@strSQL)            
End
GO
