/****** Object:  StoredProcedure [dbo].[usp_GetKeyFromOpportunityForCase]    Script Date: 07/26/2008 16:17:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getkeyfromopportunityforcase')
DROP PROCEDURE usp_getkeyfromopportunityforcase
GO
CREATE PROCEDURE [dbo].[usp_GetKeyFromOpportunityForCase]  
 @numDomainID numeric(9)=0,  
 @numDivisionID numeric(9)=0     
--  
AS  
IF @numDivisionID>0  
BEGIN  
SELECT numOppID   
 FROM opportunitymaster   
 WHERE numcontactid IN   
  (SELECT numcontactid   
   FROM additionalcontactsinformation   
   WHERE numdomainid=@numDomainID)   
   AND numDivisionID=@numDivisionID
   ORDER BY OpportunityMaster.numOppID  
END  
ELSE  
BEGIN  
SELECT numOppID   
 FROM opportunitymaster   
 WHERE numcontactid IN   
  (SELECT numcontactid   
   FROM additionalcontactsinformation   
   WHERE numdomainid=1)   
   ORDER BY OpportunityMaster.numOppID  
  
END  
  
     
   /*(select numlistitemid from stagelistitemdetails   
where numstagedetailsid in(select numstagedetailsid from stagepercentagedetails where numstagepercentageid=12)  
AND numOppID IS NOT NULL)*/  
--Modified in tintPStage 19-apr-03
GO
