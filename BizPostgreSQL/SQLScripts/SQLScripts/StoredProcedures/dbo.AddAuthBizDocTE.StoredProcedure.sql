/****** Object:  StoredProcedure [dbo].[AddAuthBizDocTE]    Script Date: 07/26/2008 16:14:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='addauthbizdocte')
DROP PROCEDURE addauthbizdocte
GO
CREATE PROCEDURE [dbo].[AddAuthBizDocTE]
    @numOppId AS NUMERIC(9),
    @numUserCntID AS NUMERIC(9),
    @numOppBizDocsId AS NUMERIC(9)
AS 
    DECLARE @numBizDocId AS NUMERIC

    SELECT  @numBizDocId = [numAuthoritativeSales]
    FROM    AuthoritativeBizDocs
    WHERE   [numDomainId] = ( SELECT    [numDomainId]
                              FROM      [OpportunityMaster]
                              WHERE     [numOppId] = @numOppId
                            )

    IF ( SELECT COUNT(*) FROM   OpportunityBizDocs WHERE  numoppid = @numoppid AND numBizDocId = @numBizDocId AND bitAuthoritativeBizDocs=1 ) = 0 
        BEGIN
            DECLARE @vcBizDocID AS VARCHAR(100)                  
            DECLARE @numBizMax AS NUMERIC(9)                  
            SELECT  @vcBizDocID = vcpOppName FROM    OpportunityMaster WHERE   numOppId = @numOppId                  
            SELECT  @numBizMax = COUNT(*) FROM    OpportunityBizDocs                  
            IF @numBizMax > 0 
                BEGIN                  
                    SELECT  @numBizMax = MAX(numOppBizDocsId)
                    FROM    OpportunityBizDocs                  
                END                  
            SET @numBizMax = @numBizMax + 1                  
            SET @vcBizDocID = @vcBizDocID + '-BD-' + CONVERT(VARCHAR(10), @numBizMax)
            PRINT @vcBizDocID          
            INSERT  INTO OpportunityBizDocs
                    (
                      numOppId,
                      numBizDocId,
                      vcBizDocID,
                      numCreatedBy,
                      dtCreatedDate,
                      numModifiedBy,
                      dtModifiedDate ,
                      [dtFromDate],
					  bitAuthoritativeBizDocs		
                    )
            VALUES  (
                      @numOppId,
                      @numBizDocId,
                      @vcBizDocID,
                      @numUserCntID,
                      GETUTCDATE(),
                      @numUserCntID,
                      GETUTCDATE(),
                      GETUTCDATE(),
                      1
                    )               
            SELECT  SCOPE_IDENTITY()               
        END

