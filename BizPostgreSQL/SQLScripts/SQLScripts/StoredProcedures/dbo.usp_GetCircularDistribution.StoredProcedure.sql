/****** Object:  StoredProcedure [dbo].[usp_GetCircularDistribution]    Script Date: 07/26/2008 16:16:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created By: Debasish Nag        
--Purpose: To return the Employees who can bee assigned Leads        
--and who are not already in the circular distribution list        
--Created On: 22nd July 2004        
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getcirculardistribution')
DROP PROCEDURE usp_getcirculardistribution
GO
CREATE PROCEDURE [dbo].[usp_GetCircularDistribution]    
 @numDomainID numeric,        
 @numRoutID numeric        
AS                    
BEGIN                    
 SELECT ACI.numContactID numUserID, aci.vcFirstName + ' '+ aci.vcLastname AS vcUserName       
 FROM UserMaster UM INNER JOIN AdditionalContactsInformation ACI ON um.numDomainId = ACI.numDomainId
 WHERE um.numDomainID=@numDomainID
 AND numContactID=numUserDetailID         
 AND ACI.numContactID NOT IN        
 (        
  SELECT numEmpId FROM RoutingLeadDetails     
  WHERE numRoutID = @numRoutID        
 )        
 ORDER BY aci.vcFirstName      
END
GO
