/****** Object:  StoredProcedure [dbo].[usp_DeleteOppDocs]    Script Date: 07/26/2008 16:15:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_deleteoppdocs')
DROP PROCEDURE usp_deleteoppdocs
GO
CREATE PROCEDURE [dbo].[usp_DeleteOppDocs]
	@numDocID as numeric(9)=0  
-- 
AS
	DELETE FROM  opptdocuments WHERE numDocID=@numDocID
GO
