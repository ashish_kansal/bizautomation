/****** Object:  StoredProcedure [dbo].[USP_ProContacts]    Script Date: 07/26/2008 16:20:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_procontacts')
DROP PROCEDURE usp_procontacts
GO
CREATE PROCEDURE [dbo].[USP_ProContacts]     
@numProId numeric(9)=0 ,
@numDomainId as numeric(9)=0       
--      
AS      
begin    
    
SELECT  D.numDivisionID,a.numContactId,vcCompanyName+', '+isnull(Lst.vcData,'-') as Company,     
  a.vcFirstname +' '+ a.vcLastName as [Name],      
  a.numPhone +', '+ a.numPhoneExtension as Phone,a.vcEmail as Email,      
  b.vcData as ContactRole,      
  pro.numRole as ContRoleId,     
  convert(integer,isnull(bitPartner,0)) as bitPartner,     
  a.numcontacttype,CASE WHEN pro.bitSubscribedEmailAlert=1 THEN 'Yes' ELSE 'No' END AS SubscribedEmailAlert FROM ProjectsContacts pro    
  join additionalContactsinformation a on      
  a.numContactId=pro.numContactId    
  join DivisionMaster D    
  on a.numDivisionID =D.numDivisionID    
  join CompanyInfo C    
  on D.numCompanyID=C.numCompanyID     
  left join listdetails b       
  on b.numlistitemid=pro.numRole    
  left join listdetails Lst    
  on Lst.numlistitemid=C.numCompanyType    
  WHERE pro.numProId=@numProId and a.numDomainID=@numDomainId      
end
GO
