/****** Object:  StoredProcedure [dbo].[USP_ManageTimeAndExpense]    Script Date: 07/26/2008 16:20:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj                                    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_managetimeandexpense')
DROP PROCEDURE usp_managetimeandexpense
GO
CREATE PROCEDURE [dbo].[USP_ManageTimeAndExpense]
    @numCategoryHDRID AS NUMERIC(18,0) = 0 OUTPUT,
    @tintTEType AS TINYINT = 0,
    @numCategory AS NUMERIC(9) = 0,
    @dtFromDate AS DATETIME,
    @dtToDate AS DATETIME,
    @bitFromFullDay AS BIT,
    @bitToFullDay AS BIT,
    @monAmount AS DECIMAL(20,5),
    @numOppId AS NUMERIC(9) = 0,
    @numDivisionID AS NUMERIC(9) = 0,
    @txtDesc AS TEXT = '',
    @numType AS NUMERIC(9) = 0,
    @numUserCntID AS NUMERIC(9) = 0,
    @numDomainID AS NUMERIC(9) = 0,
    @numCaseId AS NUMERIC = 0,
    @numProId AS NUMERIC = 0,
    @numContractId AS NUMERIC = 0,
    @numOppBizDocsId AS NUMERIC = 0,
    @numStageId AS NUMERIC = 0,
    @bitReimburse AS BIT = 0,
    @bitLeaveTaken AS BIT = 0 OUTPUT,
    @numOppItemID AS NUMERIC = 0,
	@numExpId AS NUMERIC=0,
	@numApprovalComplete AS INT=0,
	@numServiceItemID AS INT=0,
	@numClassID AS INT=0,
	@numCreatedBy AS INT=0
AS 
BEGIN
	DECLARE @records AS NUMERIC(10)      
    DECLARE @monTotAmount AS DECIMAL(20,5)
    DECLARE @numJournalId AS NUMERIC(18,0)   
	
	EXEC dbo.USP_ValidateFinancialYearClosingDate @numDomainID,@dtFromDate


	IF @numCategory=1 AND  @numType NOT IN (3,4) AND EXISTS (SELECT 
																numCategoryHDRID
															FROM 
																TimeAndExpense 
															WHERE 
																numDomainID=@numDomainID 
																AND numUserCntID=@numUserCntID 
																AND numCategoryHDRID <> @numCategoryHDRID
																AND numCategory IN (3,4) 
																AND CAST(dtFromDate AS DATE) = CAST(@dtFromDate AS DATE))
	BEGIN
		RAISERROR('LEAVE_ENTRY_EXISTS',16,1)
	END
	ELSE IF ISNULL(@numCategoryHDRID,0)=0 AND  (@numCategory <> 1 OR (@numCategory=1 AND (@numType NOT IN (3,4) OR (@numCategory IN (3,4) AND NOT EXISTS (SELECT 
																										numCategoryHDRID
																									FROM 
																										TimeAndExpense 
																									WHERE 
																										numDomainID=@numDomainID 
																										AND numUserCntID=@numUserCntID 
																										AND numCategoryHDRID <> @numCategoryHDRID
																										AND numCategory IN (3,4) 
																										AND CAST(dtFromDate AS DATE) = CAST(@dtFromDate AS DATE))))))
	BEGIN
		INSERT INTO TimeAndExpense
        (
            tintTEType,
            numCategory,
            dtFromDate,
            dtToDate,
            bitFromFullDay,
            bitToFullDay,
            monAmount,
            numOppId,
            numDivisionID,
            txtDesc,
            numType,
            numUserCntID,
            numDomainID,
            numCaseId,
            numProId,
            numContractId,
            numOppBizDocsId,
            numtCreatedBy,
            numtModifiedBy,
            dtTCreatedOn,
            dtTModifiedOn,
            numStageId,
            bitReimburse,
			numOppItemID,
			numExpId,
			numApprovalComplete,
			numServiceItemID,
			numClassID    
        )
        VALUES  
		(
            @tintTEType,
            @numCategory,
            @dtFromDate,
            @dtToDate,
            @bitFromFullDay,
            @bitToFullDay,
            @monAmount,
            @numOppId,
            @numDivisionID,
            @txtDesc,
            @numType,
            @numUserCntID,
            @numDomainID,
            @numCaseId,
            @numProId,
            @numContractId,
            @numOppBizDocsId,
            @numCreatedBy,
            @numCreatedBy,
            GETUTCDATE(),
            GETUTCDATE(),
            @numStageId,
            @bitReimburse,
			@numOppItemID,
			@numExpId,
			@numApprovalComplete,
			@numServiceItemID,
			@numClassID         
        )             
         
		SET @numCategoryHDRID=SCOPE_IDENTITY()
	END 
	ELSE IF ISNULL(@numCategoryHDRID,0) > 0
	BEGIN
		UPDATE
			TimeAndExpense
		SET
			dtFromDate=@dtFromDate,
			dtToDate=@dtToDate,
			monAmount=@monAmount,
			numServiceItemID=@numServiceItemID,
			numClassID=@numClassID,
			numDivisionID=@numDivisionID,
			numOppId=@numOppId
		WHERE
			numCategoryHDRID=@numCategoryHDRID
	END
END
GO
