/****** Object:  StoredProcedure [dbo].[usp_GetCompanydetails]    Script Date: 07/26/2008 16:16:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj          
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getcompanydetails')
DROP PROCEDURE usp_getcompanydetails
GO
CREATE PROCEDURE [dbo].[usp_GetCompanydetails]              
 @numcntid numeric=0,        
 @numDivisionID numeric(9)=0               
--              
AS              
  if  @numcntid>0        
begin        
 SELECT CMP.numCompanyID, CMP.vcCompanyName,               
  dbo.fn_GetListItemName(DM.numStatusID) as status,              
--  (select vctername from TerritoryMaster where  TerritoryMaster.numterid=DM.numTerID) as Territory, 
'' as Territory,             
  dbo.fn_GetListItemName(CMP.numcompanyrating) as Rating,              
  dbo.fn_GetListItemName(CMP.numcompanyindustry) as Industry,              
  dbo.fn_GetListItemName(CMP.numcompanycredit) as Credit,              
  dbo.fn_GetListItemName(CMP.numcompanytype) as Type,              
  (select vcgrpname from groups where groups.numgrpid=DM.numgrpid) grpname,              
  DM. vcDivisionName, 
   AD1.vcstreet vcBillstreet,                
   AD1.vcCity vcBillCity,                
   AD1.numState vcBilState,                  
   AD1.vcPostalCode vcBillPostCode,                
   AD1.numCountry vcBillCountry,                
   AD2.vcstreet vcShipStreet,                
   AD2.vcCity vcShipCity,                
   AD2.numState vcShipState,                
   AD2.vcPostalCode vcShipPostCode,                
   AD2.numCountry vcShipCountry,        
--  DM.vcBillStreet, DM.vcBillCity, DM.vcBilState,               
--  DM.vcBillPostCode, DM.vcBillCountry, DM.bitSameAddr,               
--  DM.vcShipStreet, DM.vcShipCity, DM.vcShipState,
--DM.vcShipPostCode, DM.vcShipCountry,
  CMP.vcwebsite,              
  CMP.vcWebLabel1, CMP.vcWebLink1, CMP.vcWebLabel2, CMP.vcWebLink2,               
  CMP.vcWebLabel3, CMP.vcWebLink3, CMP.vcWeblabel4, CMP.vcWebLink4,              
  DM.numTerID,               
  CMP.numCompanyIndustry, CMP.numCompanyCredit,               
  CMP.txtComments, isnull(CMP.vcWebSite,'-') as vcWebSite, CMP.numAnnualRevID, CMP.numNoOfEmployeesID,               
  dbo.fn_GetListItemName(cmp.vcProfile) as vcProfile,-- AI.numDivisionID,              
  DM.numGrpID, CMP.vcHow,CMP.numCompanyID,DM.numDivisionID,DM.tintCRMType,AI.numContactID             
 FROM  companyinfo CMP INNER JOIN divisionmaster DM ON CMP.numcompanyid=DM.numcompanyid
    LEFT JOIN dbo.AddressDetails AD1 ON AD1.numDomainID=DM.numDomainID 
   AND AD1.numRecordID=DM.numDivisionID AND AD1.tintAddressOf=2 AND AD1.tintAddressType=1 AND AD1.bitIsPrimary=1
   LEFT JOIN dbo.AddressDetails AD2 ON AD1.numDomainID=DM.numDomainID 
   AND AD2.numRecordID= DM.numDivisionID AND AD2.tintAddressOf=2 AND AD2.tintAddressType=2 AND AD2.bitIsPrimary=1
   LEFT JOIN AdditionalContactsInformation AI ON dm.numdivisionid=AI.numdivisionid
 WHERE
    numcontactid=@numcntid
            
 end
        
  if  @numDivisionID>0        
begin        
 SELECT CMP.numCompanyID, CMP.vcCompanyName,               
  dbo.fn_GetListItemName(DM.numStatusID) as status,              
'' as Territory,              
  dbo.fn_GetListItemName(CMP.numcompanyrating) as Rating,              
  dbo.fn_GetListItemName(CMP.numcompanyindustry) as Industry,              
  dbo.fn_GetListItemName(CMP.numcompanycredit) as Credit,              
  dbo.fn_GetListItemName(CMP.numcompanytype) as Type,              
  (select vcgrpname from groups where groups.numgrpid=DM.numgrpid) grpname,              
  DM. vcDivisionName, 
--  DM.vcBillStreet, DM.vcBillCity, DM.vcBilState,               
--  DM.vcBillPostCode, DM.vcBillCountry, DM.bitSameAddr,               
--  DM.vcShipStreet, DM.vcShipCity, DM.vcShipState, 
--  DM.vcShipPostCode, DM.vcShipCountry, 
  CMP.vcwebsite,              
  CMP.vcWebLabel1, CMP.vcWebLink1, CMP.vcWebLabel2, CMP.vcWebLink2,               
  CMP.vcWebLabel3, CMP.vcWebLink3, CMP.vcWeblabel4, CMP.vcWebLink4,              
    DM.numTerID,               
  CMP.numCompanyIndustry, CMP.numCompanyCredit,               
  CMP.txtComments,isnull(CMP.vcWebSite,'-') as vcWebSite, CMP.numAnnualRevID, CMP.numNoOfEmployeesID,               
  dbo.fn_GetListItemName(cmp.vcProfile) as vcProfile,-- AI.numDivisionID,              
  DM.numGrpID, CMP.vcHow,CMP.numCompanyID,DM.numDivisionID,DM.tintCRMType   , AI.numContactID,CMP.numcompanytype            
 FROM  companyinfo CMP
 INNER JOIN divisionmaster DM ON CMP.numcompanyid=DM.numcompanyid 
 LEFT JOIN AdditionalContactsInformation AI ON dm.numdivisionid=AI.numdivisionid        
 WHERE              
   dm.numdivisionid=@numDivisionID and ISNULL(Ai.bitPrimaryContact,0)=1       
            
 end
GO
