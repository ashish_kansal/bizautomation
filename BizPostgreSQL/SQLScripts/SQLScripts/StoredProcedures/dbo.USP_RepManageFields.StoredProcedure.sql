/****** Object:  StoredProcedure [dbo].[USP_RepManageFields]    Script Date: 07/26/2008 16:20:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj  
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_repmanagefields')
DROP PROCEDURE usp_repmanagefields
GO
CREATE PROCEDURE [dbo].[USP_RepManageFields]        
@strFields as varchar(8000)      
as      
      
      
declare @separator_position as integer      
declare @strPosition as varchar(1000)      
          
     update ReportFields set tintSelected=0  
      
   while patindex('%,%' , @strFields) <> 0      
    begin -- Begin for While Loop      
      select @separator_position =  patindex('%,%' , @strFields)      
    select @strPosition = left(@strFields, @separator_position - 1)      
       select @strFields = stuff(@strFields, 1, @separator_position,'')      
      
     update ReportFields set tintSelected=1 where numRepFieldID=@strPosition  
        
   end
GO
