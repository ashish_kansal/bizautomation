/****** Object:  StoredProcedure [dbo].[USP_AdvancedSearchGroup]    Script Date: 07/26/2008 16:14:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj                  
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_advancedsearchgroup')
DROP PROCEDURE usp_advancedsearchgroup
GO
CREATE PROCEDURE [dbo].[USP_AdvancedSearchGroup]                 
@WhereCondition as varchar(1000)='',                  
@ViewID as tinyint,                  
@AreasofInt as varchar(100)='',                  
@tintUserRightType as tinyint,                  
@numDomainID as numeric(9)=0,                  
@numUserCntID as numeric(9)=0,                  
@numGroupID as numeric(9)=0,                  
@CurrentPage int,                                                        
@PageSize int,                                                        
@TotRecs int output,                                                                                                 
@columnSortOrder as Varchar(10),                  
@ColumnSearch as varchar(100)='',               
@ColumnName as varchar(20)='',                 
@GetAll as bit,                  
@SortCharacter as char(1),            
@SortColumnName as varchar(20)=''                  
as                  
declare @tintOrder as tinyint                  
declare @vcFormFieldName as varchar(50)                  
declare @vcListItemType as varchar(3)             
declare @vcListItemType1 as varchar(3)                 
declare @vcAssociatedControlType varchar(10)                  
declare @numListID AS numeric(9)                  
declare @vcDbColumnName varchar(20)                   
            
if (@SortCharacter<> '0' and @SortCharacter<>'') set @ColumnSearch=@SortCharacter            
            
Create table #tempTable ( ID INT IDENTITY PRIMARY KEY,                                                         
      numContactID varchar(15)                                                        
 )                                                        
                                                        
declare @strSql as varchar(8000)                                                  
 set  @strSql='Select ADC.numContactId        
  FROM AdditionalContactsInformation ADC                                       
  JOIN DivisionMaster DM ON ADC.numDivisionId = DM.numDivisionID                                      
  JOIN CompanyInfo C ON DM.numCompanyID = C.numCompanyId   '              
  if (@ColumnName<>'' and  @ColumnSearch<>'')            
  begin              
 select @vcListItemType=vcListItemType from View_DynamicDefaultColumns where vcDbColumnName=@ColumnName and numFormID=1 and numDomainId=@numDomainId             
    if @vcListItemType='LI'                   
    begin                    
      set @strSql= @strSql +' left Join ListDetails L1 on L1.numListItemID='+@ColumnName                  
    end                  
    else if @vcListItemType='S'                   
    begin                      
      set @strSql= @strSql +' left join State S1 on S1.numStateID='+@ColumnName                  
    end                  
    else if @vcListItemType='T'                   
    begin                  
      set @strSql= @strSql +' left join TerritoryMaster T1 on T1.numTerID='+@ColumnName                  
    end               
   end              
   if (@SortColumnName<>'')            
  begin              
 select @vcListItemType1=vcListItemType from View_DynamicDefaultColumns where vcDbColumnName=@SortColumnName and numFormID=1 and numDomainId=@numDomainId              
    if @vcListItemType1='LI'                   
    begin                    
      set @strSql= @strSql +' left Join ListDetails L2 on L2.numListItemID='+@SortColumnName                  
    end                  
    else if @vcListItemType1='S'                   
    begin                      
      set @strSql= @strSql +' left join State S2 on S2.numStateID='+@SortColumnName                  
    end                  
    else if @vcListItemType1='T'                   
    begin                  
      set @strSql= @strSql +' left join TerritoryMaster T2 on T2.numTerID='+@SortColumnName                  
    end               
  end              
  set @strSql=@strSql+' where DM.numDomainID  = '+convert(varchar(15),@numDomainID)+   @WhereCondition                      
                                                                                                       if @tintUserRightType=1 set @strSql=@strSql + ' AND (ADC.numRecOwner = '+convert(varchar(15),@numUserCntID)+ ')'                             
else if @tintUserRightType=2 set @strSql=@strSql + ' AND (DM.numTerID in (select numTerritoryID from  UserTerritory where numUserCntID= '+convert(varchar(15),@numUserCntID)+' ) or DM.numTerID=0) '                                   
            
              
                
                                
if    @AreasofInt<>'' set  @strSql=@strSql+ ' and ADC.numContactID in (select numContactID from  AOIContactLink where numAOIID in('+ @AreasofInt+'))'                     
if (@ColumnName<>'' and  @ColumnSearch<>'')             
begin              
  if @vcListItemType='LI'                   
    begin                    
      set @strSql= @strSql +' and L1.vcData like '''+@ColumnSearch +'%'''                 
    end                  
    else if @vcListItemType='S'                   
    begin                      
      set @strSql= @strSql +' and S1.vcState like '''+@ColumnSearch+'%'''              
    end                  
    else if @vcListItemType='T'                   
    begin                  
      set @strSql= @strSql +' and T1.vcTerName like '''+@ColumnSearch  +'%'''                
    end               
          else set @strSql= @strSql +' and '+@ColumnName+' like '''+@ColumnSearch  +'%'''                
              
end              
   if (@SortColumnName<>'')            
  begin               
    if @vcListItemType1='LI'                   
    begin                    
      set @strSql= @strSql +' order by L2.vcData '+@columnSortOrder                  
    end                  
    else if @vcListItemType1='S'                   
    begin                      
      set @strSql= @strSql +' order by S2.vcState '+@columnSortOrder                  
    end                  
    else if @vcListItemType1='T'                   
    begin                  
      set @strSql= @strSql +' order by T2.vcTerName '+@columnSortOrder                  
    end              
           else  set @strSql= @strSql +' order by '+ @SortColumnName +' ' +@columnSortOrder            
  end            
             
                   
print @strSql                  
                     
insert into #tempTable(numContactID)                                                        
exec(@strSql)                   
set @strSql=''                  
                  
                 
set @tintOrder=0                  
set @WhereCondition =''                  
set @strSql='select ADC.numContactId,ADC.VcFirstName,ADC.vcLastName,c.vcCompanyName,adc.vcEmail '                  
select top 1 @tintOrder=A.tintOrder+1,@vcDbColumnName=vcDbColumnName,@vcFormFieldName=vcFieldName,@vcAssociatedControlType=vcAssociatedControlType,@vcListItemType=vcListItemType,@numListID=numListID                   
from AdvSerViewConf A                  
join View_DynamicDefaultColumns D                   
on D.numFieldId=A.numFormFieldId                  
where tintViewID=@ViewID and numGroupID=@numGroupID AND D.numDomainID= @numDomainID AND A.numDomainID=@numDomainID 
order by A.tintOrder asc      
            
while @tintOrder>0                  
begin                  
                  
                   
 if @vcAssociatedControlType='SelectBox'                  
        begin                  
                                  
  if @vcListItemType='LI'                   
  begin                  
   set @strSql=@strSql+',L'+ convert(varchar(3),@tintOrder)+'.vcData'+' ['+ @vcFormFieldName+'~'+ @vcDbColumnName+']'                  
   set @WhereCondition= @WhereCondition +' left Join ListDetails L'+ convert(varchar(3),@tintOrder)+ ' on L'+convert(varchar(3),@tintOrder)+ '.numListItemID='+@vcDbColumnName                  
  end                  
  else if @vcListItemType='S'                   
  begin                  
   set @strSql=@strSql+',S'+ convert(varchar(3),@tintOrder)+'.vcState'+' ['+ @vcFormFieldName+'~'+ @vcDbColumnName+']'                  
   set @WhereCondition= @WhereCondition +' left join State S'+ convert(varchar(3),@tintOrder)+ ' on S'+convert(varchar(3),@tintOrder)+ '.numStateID='+@vcDbColumnName                  
  end                  
  else if @vcListItemType='T'                   
  begin                  
   set @strSql=@strSql+',T'+ convert(varchar(3),@tintOrder)+'.vcTerName'+' ['+ @vcFormFieldName+'~'+ @vcDbColumnName+']'                  
   set @WhereCondition= @WhereCondition +' left join TerritoryMaster T'+ convert(varchar(3),@tintOrder)+ ' on T'+convert(varchar(3),@tintOrder)+ '.numTerID='+@vcDbColumnName                  
  end                  
 end                  
  else set @strSql=@strSql+','+@vcDbColumnName+' ['+ @vcFormFieldName+'~'+ @vcDbColumnName+']'                   
                          
 select top 1 @tintOrder=A.tintOrder+1,@vcDbColumnName=vcDbColumnName,@vcFormFieldName=vcFieldName,@vcAssociatedControlType=vcAssociatedControlType,@vcListItemType=vcListItemType,@numListID=numListID                   
 from AdvSerViewConf A                  
 join View_DynamicDefaultColumns D                   
 on D.numFieldId=A.numFormFieldId                  
 where tintViewID=@ViewID and numGroupID=@numGroupID AND D.numDomainID= @numDomainID AND A.numDomainID=@numDomainID and A.tintOrder > @tintOrder-1 
order by A.tintOrder asc 
                 
 if @@rowcount=0 set @tintOrder=0                  
end                  
                  
                  
                  
                  
                                     
                                                        
  declare @firstRec as integer                                                        
  declare @lastRec as integer                                                        
 set @firstRec= (@CurrentPage-1) * @PageSize                                                        
 set @lastRec= (@CurrentPage*@PageSize+1)                                                         
set @TotRecs=(select count(*) from #tempTable)                                       
                              
if @GetAll=0                  
                  
 set @strSql=@strSql+'FROM AdditionalContactsInformation ADC                                       
  JOIN DivisionMaster DM ON ADC.numDivisionId = DM.numDivisionID                                      
  JOIN CompanyInfo C ON DM.numCompanyID = C.numCompanyId                  
   '+@WhereCondition+' join #tempTable T on T.numContactId=ADC.numContactId                                                   
   WHERE ID > '+convert(varchar(10),@firstRec)+ 'and ID <'+ convert(varchar(10),@lastRec)                  
 else                  
     set @strSql=@strSql+'FROM AdditionalContactsInformation ADC                                       
  JOIN DivisionMaster DM ON ADC.numDivisionId = DM.numDivisionID                                      
  JOIN CompanyInfo C ON DM.numCompanyID = C.numCompanyId                  
   '+@WhereCondition+' join #tempTable T on T.numContactId=ADC.numContactId'                                  
print @strSql                     
exec(@strSql)                                                     
drop table #tempTable
GO
