/****** Object:  StoredProcedure [dbo].[USP_ManagaeAlertsConAndCom]    Script Date: 07/26/2008 16:19:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created By Anoop Jayaraj
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_managaealertsconandcom')
DROP PROCEDURE usp_managaealertsconandcom
GO
CREATE PROCEDURE [dbo].[USP_ManagaeAlertsConAndCom]
@numAlerDTLId as numeric(9)=0,
@numContactID as numeric(9)=0,
@numAlertConID as numeric(9)=0,
@byteMode as tinyint=0
as
if @byteMode=0
begin

	insert into AlertContactsAndCompany(numAlerDTLId,numContactID)
	values(@numAlerDTLId,@numContactID)
end
if @byteMode=1
begin

	delete from AlertContactsAndCompany where numAlertConID=@numAlertConID
end
GO
