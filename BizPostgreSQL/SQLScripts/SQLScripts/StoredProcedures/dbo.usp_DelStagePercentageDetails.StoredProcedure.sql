/****** Object:  StoredProcedure [dbo].[usp_DelStagePercentageDetails]    Script Date: 07/26/2008 16:15:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_delstagepercentagedetails')
DROP PROCEDURE usp_delstagepercentagedetails
GO
CREATE PROCEDURE [dbo].[usp_DelStagePercentageDetails]    
@slpid numeric     
--    
    
AS    
DELETE from stagepercentagedetails where slp_id=@slpid
GO
