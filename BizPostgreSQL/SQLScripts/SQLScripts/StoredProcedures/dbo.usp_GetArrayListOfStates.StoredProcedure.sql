/****** Object:  StoredProcedure [dbo].[usp_GetArrayListOfStates]    Script Date: 07/26/2008 16:16:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created By: Debasish Tapan Nag                                
--Purpose: Gets the list of states from the database    
--Created Date: 11/20/2005                      
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getarraylistofstates')
DROP PROCEDURE usp_getarraylistofstates
GO
CREATE PROCEDURE [dbo].[usp_GetArrayListOfStates]  
 @numDomainId Numeric (9)            
AS    
DECLARE @vcState NVarchar    
SELECT Distinct numCountryId, dbo.fn_GetStateInACountry(numCountryId,@numDomainId,'NM') AS vcState,    
dbo.fn_GetStateInACountry(numCountryId,@numDomainId,'ID') AS vcStateIds    
FROM AllCountriesAndStates AllStates    
WHERE (numDomainID = @numDomainID  OR constFlag = 1)
ORDER BY numCountryId
GO
