/****** Object:  StoredProcedure [dbo].[usp_InsertCampaign]    Script Date: 07/26/2008 16:18:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_insertcampaign')
DROP PROCEDURE usp_insertcampaign
GO
CREATE PROCEDURE [dbo].[usp_InsertCampaign] 
	@textMsgDocument text='',
	@textSubject text='',
	@vcEmailId varchar(100)='',
	@vcCampaignNumber varchar(50)='',
	@intNoOfContacts int=0,
	@numCreatedBy numeric(9)=0,
	@bintCreatedDate bigint=0,
	@numModifiedBy numeric(9)=0,
	@bintModifiedDate bigint=0,
	@numDomainID numeric(9)=0,
	@textProfile text=''   
--
AS
BEGIN
	INSERT INTO Campaign(textMsgDocument,textSubject,vcEmailId,vcCampaignNumber,intNoOfContacts,numCreatedBy,
	bintCreatedDate,numModifiedBy,bintModifiedDate,numDomainID,textProfile)
	VALUES(@textMsgDocument,@textSubject,@vcEmailId,@vcCampaignNumber,@intNoOfContacts,@numCreatedBy,
	@bintCreatedDate,@numModifiedBy,@bintModifiedDate,@numDomainID,@textProfile)
	
END
GO
