/****** Object:  StoredProcedure [dbo].[usp_SaveCustomReport]    Script Date: 07/26/2008 16:20:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_savecustomreport')
DROP PROCEDURE usp_savecustomreport
GO
CREATE PROCEDURE [dbo].[usp_SaveCustomReport]                  
@ReportId as numeric,                  
@ReportName as varchar(200),                  
@ReportDesc as varchar(200),                  
@sql as text,                  
@numDomainId as numeric(9),                  
@numUserCntId as numeric(9) ,                 
@ModuleId as numeric(9) ,                
@GroupId as numeric(9),              
@FilterRel as bit ,            
@stdFieldId as varchar(200),            
@custFilter as varchar(200),            
@FromDate as varchar(200)= '',            
@ToDate as varchar(200)='',            
@Rows as numeric(9)  ,          
@GridType as bit = 0,          
@strGrpfld as varchar(200)='',           
@strGrpOrd as varchar(200)='',           
@strGrpflt as varchar(200)='' ,          
@sqlGrp as text  ,      
@FilterType as int ,      
@AdvFilterStr   as varchar(200)=''  ,    
@OrderbyFld as varchar(200),    
@Orderf as varchar(200)  ,  
@MttId as int ,    
@MttValue as varchar(200)  ,
@CompFilter1  as varchar(200)  ,
@CompFilter2 as varchar(200)  ,
@CompFilterOpr as varchar(200),
@bitDrillDown as bit,
@bitSortRecCount as bit
as              
--bitGridType bit Unchecked          
--varGrpfld varchar(200) Checked          
--varGrpOrd varchar(200) Checked          
--varGrpflt varchar(200) Checked          
               
if @reportId = 0                  
 begin                  
   insert into CustomReport (vcReportName,vcReportDescription,textQuery,numCreatedBy,numDomainID,numModifiedBy,numModuleId,numGroupId            
    ,bitFilterRelAnd,varstdFieldId,custFilter,FromDate,ToDate,bintRows,bitGridType,varGrpfld,varGrpOrd,varGrpflt,textQueryGrp,      
 FilterType,AdvFilterStr,OrderbyFld,Orderf,MttId,MttValue,CompFilter1,CompFilter2,CompFilterOpr,bitDrillDown,bitSortRecCount)                  
   values                  
   (@ReportName,@ReportDesc,@sql,@numUserCntId,@numDomainId,@numUserCntId,@ModuleId,@GroupId,@FilterRel,@stdFieldId,@custFilter
	,@FromDate,@ToDate,@Rows,@GridType,@strGrpfld,@strGrpOrd,@strGrpflt,@sqlGrp,@FilterType,@AdvFilterStr,@OrderbyFld,@Orderf
	,@MttId,@MttValue,@CompFilter1,@CompFilter2,@CompFilterOpr,@bitDrillDown,@bitSortRecCount)                  
    SELECT                    
   @ReportId = SCOPE_IDENTITY();                    
 end                  
else                  
 begin                  
   update CustomReport set                   
  vcReportName = @ReportName,                  
  vcReportDescription = @ReportDesc,                  
  textQuery = @sql,                  
  numDomainID = @numDomainId,                  
  numModifiedBy =@numUserCntId,                
  numModuleId =   @ModuleId,                
  numGroupId = @GroupId  ,              
  bitFilterRelAnd = @FilterRel,              
  varstdFieldId=@stdFieldId,            
  custFilter=@custFilter,            
  FromDate=@FromDate,            
  ToDate=@ToDate,            
  bintRows =@Rows  ,          
  bitGridType=@GridType,          
  varGrpfld=@strGrpfld,          
  varGrpOrd=@strGrpOrd,          
  varGrpflt=@strGrpflt,          
  textQueryGrp=@sqlGrp  ,      
  FilterType=@FilterType,      
  AdvFilterStr=@AdvFilterStr  ,    
  OrderbyFld=@OrderbyFld,    
  Orderf  =@Orderf  ,  
  MttId=@MttId,  
  MttValue=@MttValue  ,
  CompFilter1=@CompFilter1,
  CompFilter2=@CompFilter2,
  CompFilterOpr=@CompFilterOpr,
  bitDrillDown=@bitDrillDown,
   bitSortRecCount=@bitSortRecCount
   where numCustomReportID = @ReportId                  
 end                  
                  
  SELECT                    
  @ReportId
GO
