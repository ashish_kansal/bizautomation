/****** Object:  StoredProcedure [dbo].[usp_GetUserAssign]    Script Date: 07/26/2008 16:18:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getuserassign')
DROP PROCEDURE usp_getuserassign
GO
CREATE PROCEDURE [dbo].[usp_GetUserAssign]     
  @numDomainID numeric(9)=null      
AS                               
  BEGIN                    

 SELECT A.numContactID as numUserId,A.vcFirstName+' '+A.vcLastName as vcGivenName        
 from UserMaster UM       
 join AdditionalContactsInformation A      
 on UM.numUserDetailId=A.numContactID        
 where UM.numDomainID=@numDomainID  
 union  
 select  A.numContactID,+A.vcFirstName+' '+A.vcLastName+' - '+ vcCompanyName  
 from AdditionalContactsInformation A   
 join DivisionMaster D  
 on D.numDivisionID=A.numDivisionID  
 join ExtarnetAccounts E   
 on E.numDivisionID=D.numDivisionID  
 join ExtranetAccountsDtl DTL  
 on DTL.numExtranetID=E.numExtranetID  
 join CompanyInfo C  
 on C.numCompanyID=D.numCompanyID  
 where A.numDomainID=@numDomainID   
 and D.numDivisionID <>(select numDivisionID from domain where numDomainID=@numDomainID) 
                
  END
GO
