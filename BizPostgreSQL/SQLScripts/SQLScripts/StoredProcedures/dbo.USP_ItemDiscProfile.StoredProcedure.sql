/****** Object:  StoredProcedure [dbo].[USP_ItemDiscProfile]    Script Date: 07/26/2008 16:19:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_itemdiscprofile')
DROP PROCEDURE usp_itemdiscprofile
GO
CREATE PROCEDURE [dbo].[USP_ItemDiscProfile]
@numItemID as numeric(9)=0,
@byteMode as tinyint=0,
@numRelID as numeric(9)=0,
@numProID as numeric(9)=0
as
if @byteMode=0
begin
select numDiscProfileID,numItemID,I.numDivisionID,vcCompanyName+', '+vcDivisionName as Company,decDisc,bitApplicable 
from  ItemDiscountProfile I
join DivisionMaster D
on D.numDivisionID=I.numDivisionID
join CompanyInfo C 
on C.numCompanyID=D.numCompanyID
where numItemID= @numItemID and I.numDivisionID>0
end
if @byteMode=1
begin
select numDiscProfileID,numItemID,numRelID,numProID,decDisc,bitApplicable,dbo.fn_GetListItemName(numRelID)+', '+dbo.fn_GetListItemName(numProID) as RelPro
from  ItemDiscountProfile where numItemID=@numItemID and numProID>0
end
GO
