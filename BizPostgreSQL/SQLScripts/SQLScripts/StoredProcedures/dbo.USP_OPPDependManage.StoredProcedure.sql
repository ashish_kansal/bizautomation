/****** Object:  StoredProcedure [dbo].[USP_OPPDependManage]    Script Date: 07/26/2008 16:20:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_oppdependmanage')
DROP PROCEDURE usp_oppdependmanage
GO
CREATE PROCEDURE [dbo].[USP_OPPDependManage]
(
@byteMode as tinyint=null,
@strDependency as text='',
@OppID as numeric(9)=null,
@OppStageID as numeric(9)=null
)
as

if @byteMode= 0 
begin

	delete from OpportunityDependency where numOpportunityid=@OppID and numOppstageid=@OppStageID

	declare @hDoc  int
	EXEC sp_xml_preparedocument @hDoc OUTPUT, @strDependency
	insert into  OpportunityDependency
	(
	numOpportunityId,
	numOppStageId,
	numProcessStageId,
	vcType
	)
	select @OppID,@OppStageID,X.* from(SELECT *FROM OPENXML (@hDoc,'/NewDataSet/Table',2)
	WITH (	numProcessStageId numeric(9),vcType varchar(1)
		))X

	


	EXEC sp_xml_removedocument @hDoc
	
end

if @byteMode= 1 
begin



	select dep.numProcessStageId,dtl.vcstagedetail,vcType,
	'Milestone - ' + convert(varchar(4),dtl.numstagepercentage) +'%' as numstagepercentage ,
	mst.vcPOppName as vcPOppName,
	case when dtl.bitStageCompleted =0 then '<font color=red>Pending</font>'  when dtl.bitStageCompleted =1 then '<font color=blue>Completed</font>' end as Status 
	from OpportunityDependency dep
	join OpportunityStageDetails dtl
	on dtl.numOppStageID=dep.numProcessStageId
	join OpportunityMaster mst on 
	dtl.numOppId=mst.numOppId
	where  vcType='O' and dep.numOpportunityid=@OppID and dep.numOppstageid=@OppStageID  
	union 
	select dep.numProcessStageId,dtl.vcstagedetail,vcType,
	'Milestone - ' + convert(varchar(4),dtl.numstagepercentage) +'%' as numstagepercentage ,
	mst.VcProjectName as vcPOppName,
	case when dtl.bitStageCompleted =0 then '<font color=red>Pending</font>'  when dtl.bitStageCompleted =1 then '<font color=blue>Completed</font>' end as Status 
	from OpportunityDependency dep
	join ProjectsStageDetails dtl
	on dtl.numProStageID=dep.numProcessStageId
	join ProjectsMaster mst on 
	dtl.numProId=mst.numProId
	where  vcType='P' and dep.numOpportunityId=@OppID and dep.numOppstageid=@OppStageID
	



end
GO
