/****** Object:  StoredProcedure [dbo].[USP_GetBizDocId]    Script Date: 07/26/2008 16:16:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created By Siva    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getbizdocid')
DROP PROCEDURE usp_getbizdocid
GO
CREATE PROCEDURE [dbo].[USP_GetBizDocId]    
@numOppBizDocsId as numeric(9)=0 
As    
Begin    
  Select numBizDocId From OpportunityBizDocs Where numOppBizDocsId=@numOppBizDocsId 
End
GO
