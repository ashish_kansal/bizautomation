/****** Object:  StoredProcedure [dbo].[USP_GetChartAcntChildDetails]    Script Date: 07/26/2008 16:16:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
---Created by Siva                          
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getchartacntchilddetails')
DROP PROCEDURE usp_getchartacntchilddetails
GO
CREATE PROCEDURE [dbo].[USP_GetChartAcntChildDetails]                                         
@numDomainId as numeric(9),                              
@dtFromDate as datetime,                            
@dtToDate as datetime,                          
@numParntAcntId as numeric(9)                                        
As                                            
Begin                                            
 Declare @strSQL as varchar(2000)        
 Declare @strCount as varchar(4000)      
 Set @strCount=''                          
 Set @strSQL=''                           
 Declare @i as varchar(12)      
 Declare @strChild as varchar(8000)      
   Declare @numParentAccountId as numeric(9)        
  Set @numParentAccountId=(Select numAccountId From Chart_Of_Accounts Where numParntAcntTypeId is null and numAcntTypeID is null and numDomainId = @numDomainId) --and numAccountId = 1         
         
         
 Set @strChild=dbo.[fn_ChildCategory](@numParntAcntId,@numDomainId) + convert(varchar(4),@numParntAcntId)         
        
  Set @strCount = ' Declare @i as varchar(12)      
     set @i=0      
     Select @i=count(*) From General_Journal_Header as GJH inner join General_Journal_Details GJD on GJH.numJournal_Id=GJD.numJournalId                   
     Where GJD.numChartAcntId in (' + convert(varchar(3000),@strChild)+')      
     And GJH.datEntry_Date<=''' + convert(varchar(300),@dtToDate)+'''      
     print @i'            
 print @strCount      
 exec (@strCount)      
 If @i=0                        
  Begin                        
   Set @strSQL = ' Select distinct numAccountId as numAccountId,vcCatgyName as CategoryName,numOpeningBal as OpeningBalance,isnull(bitOpeningBalanceEquity,0) as bitOpeningBalanceEquity                             
       From Chart_Of_Accounts COA Left outer join General_Journal_Details GJD on COA.numAccountId=GJD.numChartAcntId                                
       Where  COA.numDomainId=' + Convert(varchar(5),@numDomainId) + ' And numParntAcntId='+ convert(varchar(5),@numParntAcntId)+ ' And COA.numAccountId <>'+convert(varchar(10),@numParentAccountId)+' And COA.numOpeningBal is not null'                     
  
       
   Set @strSQL=@strSQL+ '  And COA.dtOpeningDate>=''' + convert(varchar(300),@dtFromDate) +''' And  COA.dtOpeningDate<=''' + convert(varchar(300),@dtToDate) +''''                               
  End                        
 Else                        
  Begin                        
   Set @strSQL = ' Select distinct numAccountId as numAccountId,vcCatgyName as CategoryName,numOpeningBal as OpeningBalance,isnull(bitOpeningBalanceEquity,0) as bitOpeningBalanceEquity                                
       From Chart_Of_Accounts COA Left outer join General_Journal_Details GJD on COA.numAccountId=GJD.numChartAcntId                                
       Where  COA.numDomainId=' + Convert(varchar(5),@numDomainId) + ' And numParntAcntId='+ convert(varchar(5),@numParntAcntId)+ ' And COA.numAccountId <>'+convert(varchar(10),@numParentAccountId)+' And COA.numOpeningBal is not null'                     
  
       
  End                        
 print @strSQL                            
 exec (@strSQL)                            
End
GO
