/****** Object:  StoredProcedure [dbo].[USP_GetAllReferringPage]    Script Date: 07/26/2008 16:16:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj                    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getallreferringpage')
DROP PROCEDURE usp_getallreferringpage
GO
CREATE PROCEDURE [dbo].[USP_GetAllReferringPage]                    
@ReferringPage as varchar(100),                    
@searchTerm as varchar(100),                    
@From datetime,                    
@To datetime,                    
@CurrentPage int,                            
@PageSize int,                            
@TotRecs int output,                            
@columnName as Varchar(50),                            
@columnSortOrder as Varchar(10),            
@strCrawler as varchar(5)='',          
@MinimumPages as varchar(5),          
@numDomainID  as numeric(9),  
@ClientOffsetTime as integer            
as                            
                          
--Create a Temporary table to hold data                            
                           
                   
                    
                         
declare @strSql as varchar(5000)                            
set @strSql='select DTL.dtTime        
from TrackingVisitorsHDR THDR         
Join TrackingVisitorsDTL DTL on THDR.numTrackingID = DTL.numTracVisitorsHDRID        
        
where THDR.numDomainID ='''+convert(varchar(10),@numDomainID)+''' and 
(THDR.dtCreated between '''+convert(varchar(50),dateadd(minute,@ClientOffsetTime,@From))+''' and 
'''+convert(varchar(50),dateadd(minute,@ClientOffsetTime,@To))+''')and THDR.numVistedPages >= '''+@MinimumPages+''''        
          
                
                
if (@searchTerm<>'' and @searchTerm is not null)                    
begin                    
set @strSql=@strSql + ' and THDR.vcSearchTerm like ''%'+@searchTerm+'%'''                     
end             
if @strCrawler<>''  set @strSql=@strSql + ' and THDR.vcCrawler= '''+@strCrawler+''''               
if (@ReferringPage<>'' and @ReferringPage is not null)                    
begin                    
set @strSql=@strSql + ' and THDR.vcOrginalRef like ''%'+@ReferringPage+'%'''                     
end                             
 set  @strSql=@strSql +' ORDER BY ' + @columnName +' '+ @columnSortOrder                          
print @strSql                      
             
exec( @strSql)
GO
