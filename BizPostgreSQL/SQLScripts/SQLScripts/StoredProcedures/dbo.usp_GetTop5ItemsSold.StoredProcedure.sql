/****** Object:  StoredProcedure [dbo].[usp_GetTop5ItemsSold]    Script Date: 07/26/2008 16:18:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_gettop5itemssold')
DROP PROCEDURE usp_gettop5itemssold
GO
CREATE PROCEDURE [dbo].[usp_GetTop5ItemsSold]          
 @numDomainID numeric,          
 @dtDateFrom datetime,          
 @dtDateTo datetime,          
 @numTerID numeric=0,          
 @numUserCntID numeric=0,          
 @tintRights tinyint=3,          
 @intType numeric=0          
--          
AS          
BEGIN          
 IF @tintRights=3 --All Records          
 BEGIN          
  IF @intType = 0          
  BEGIN          
  SELECT TOP 5 IT.vcItemName, SUM(OI.montotAmount) AS Amount          
   FROM OpportunityItems OI INNER JOIN Item IT ON IT.numItemCode=OI.numItemCode INNER JOIN OpportunityMaster OM ON OM.numOppID=OI.numOppID  
   WHERE        
	IT.numDomainID=@numDomainID
	AND OM.numDomainId = @numDomainID      
    AND OM.tintOppStatus=1        
    AND OM.bintAccountClosingDate BETWEEN @dtDateFrom AND @dtDateTo          
             
   GROUP BY IT.vcItemName          
   ORDER BY Amount DESC          
  END          
  ELSE          
  BEGIN          
  SELECT TOP 5 IT.vcItemName, SUM(OI.montotAmount) AS Amount          
   FROM 
	AdditionalContactsInformation AI        
	INNER JOIN
		OpportunityMaster OM 
	ON
		AI.numContactID = OM.numRecOwner
	INNER JOIN 
        OpportunityItems OI 
	ON 
		OM.numOppID=OI.numOppID		
	INNER JOIN 
		Item IT 
	ON 
		IT.numItemCode=OI.numItemCode
	WHERE        
		IT.numDomainID=@numDomainID          
		AND OM.numDomainId = @numDomainID
		AND OM.tintOppStatus=1        
		AND OM.bintAccountClosingDate BETWEEN @dtDateFrom AND @dtDateTo                        
		AND AI.numTeam is not null             
		AND AI.numTeam in(select F.numTeam from ForReportsByTeam F             
		where F.numuserCntId=@numUserCntID and F.numDomainid=@numDomainId and F.tintType=@intType)            
	GROUP BY IT.vcItemName          
	ORDER BY Amount DESC          
          
  END          
 END          
          
 IF @tintRights=2 --Territory Records          
 BEGIN          
  IF @intType = 0           
  BEGIN          
  SELECT TOP 5 IT.vcItemName, SUM(OI.montotAmount) AS Amount          
   FROM OpportunityItems OI INNER JOIN Item IT ON IT.numItemCode=OI.numItemCode INNER JOIN OpportunityMaster OM ON OM.numOppID=OI.numOppID INNER JOIN DivisionMAster DM ON DM.numDivisionID = OM.numDivisionID         
   WHERE IT.numDomainID=@numDomainID 
	AND OM.numDomainId = @numDomainID        
    AND OM.tintOppStatus=1        
    AND OM.bintAccountClosingDate BETWEEN @dtDateFrom AND @dtDateTo              
    AND DM.numTerID in (SELECT numTerritoryID FROM UserTerritory WHERE numUserCntID=@numUserCntID)         
   GROUP BY IT.vcItemName          
   ORDER BY Amount DESC          
  END          
  ELSE          
  BEGIN          
  SELECT TOP 5 IT.vcItemName, SUM(OI.montotAmount) AS Amount          
   FROM 
	AdditionalContactsInformation AI        
	INNER JOIN
		OpportunityMaster OM 
	ON
		AI.numContactID = OM.numCreatedBy
	INNER JOIN 
        OpportunityItems OI 
	ON 
		OM.numOppID=OI.numOppID		
	INNER JOIN 
		Item IT 
	ON 
		IT.numItemCode=OI.numItemCode 
	INNER JOIN
		DivisionMAster DM
	ON
		DM.numDivisionID = OM.numDivisionID
   WHERE IT.numDomainID=@numDomainID
	AND OM.numDomainId = @numDomainID 
    AND OM.tintOppStatus=1        
    AND OM.bintAccountClosingDate BETWEEN @dtDateFrom AND @dtDateTo          
    AND OM.numOppID=OI.numOppID                   
    AND AI.numTeam is not null             
    AND DM.numTerID in (SELECT numTerritoryID FROM UserTerritory WHERE numUserCntID=@numUserCntID)         
    AND AI.numTeam in(select F.numTeam from ForReportsByTeam F             
    where F.numuserCntId=@numUserCntID and F.numDomainid=@numDomainId and F.tintType=@intType)            
   GROUP BY IT.vcItemName          
   ORDER BY Amount DESC          
          
  END          
          
 END          
          
 IF @tintRights=1 --Owner Records          
 BEGIN          
   IF @intType = 0          
   BEGIN          
  SELECT TOP 5 IT.vcItemName, SUM(OI.montotAmount) AS Amount          
   FROM OpportunityItems OI INNER JOIN Item IT ON IT.numItemCode=OI.numItemCode INNER JOIN OpportunityMaster OM ON OM.numOppID=OI.numOppID          
   WHERE IT.numDomainID=@numDomainID
	AND OM.numDomainId = @numDomainID          
    AND OM.tintOppStatus=1          
    AND OM.bintAccountClosingDate BETWEEN @dtDateFrom AND @dtDateTo          
    AND OM.numRecOwner=@numUserCntID          
   GROUP BY IT.vcItemName          
   ORDER BY Amount DESC          
   END          
   ELSE          
   BEGIN          
  SELECT TOP 5 IT.vcItemName, SUM(OI.montotAmount) AS Amount          
   FROM AdditionalContactsInformation AI        
	INNER JOIN
		OpportunityMaster OM 
	ON
		AI.numContactID = OM.numRecOwner
	INNER JOIN 
        OpportunityItems OI 
	ON 
		OM.numOppID=OI.numOppID		
	INNER JOIN 
		Item IT 
	ON 
		IT.numItemCode=OI.numItemCode          
   WHERE IT.numDomainID=@numDomainID          
    AND OM.tintOppStatus=1
	AND OM.numDomainId=@numDomainID          
    AND OM.bintAccountClosingDate BETWEEN @dtDateFrom AND @dtDateTo                 
    AND OM.numRecOwner=@numUserCntID                 
    AND AI.numTeam is not null             
    AND AI.numTeam in(select F.numTeam from ForReportsByTeam F             
    where F.numuserCntId=@numUserCntID and F.numDomainid=@numDomainId and F.tintType=@intType)            
   GROUP BY IT.vcItemName          
   ORDER BY Amount DESC          
          
   END          
 END          
          
END
GO
