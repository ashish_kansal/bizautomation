/****** Object:  StoredProcedure [dbo].[Reminder_Snooze]    Script Date: 07/26/2008 16:14:33 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
/*
**  Updates an Reminder notification information on an individual activity,
**  such as for instance, when the LoggedOnUserName chooses to Snooze or
**  ultimately Dismiss the Reminder.
*/
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='Reminder_Snooze')
DROP PROCEDURE Reminder_Snooze
GO
CREATE PROCEDURE [dbo].[Reminder_Snooze]
	@LastSnoozDateTimeUtc datetime, 
	@ActivityID	integer			-- the key of the Activity to be updated
AS
BEGIN
	UPDATE
		[Activity]
	SET 
		LastSnoozDateTimeUtc = @LastSnoozDateTimeUtc
	WHERE
		 [Activity].[ActivityID] = @ActivityID 
END
GO
