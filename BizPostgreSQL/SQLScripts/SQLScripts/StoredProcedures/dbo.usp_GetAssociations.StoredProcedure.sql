GO
/****** Object:  StoredProcedure [dbo].[usp_GetAssociations]    Script Date: 03/05/2010 20:15:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--Modified By: Debasish              
--Modified Date: 30th Dec 2005              
--Purpose for Modification: To get the associations for the company              
-- EXEC usp_GetAssociations 91300 ,1
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getassociations')
DROP PROCEDURE usp_getassociations
GO
CREATE PROCEDURE [dbo].[usp_GetAssociations]
    @numDivID AS NUMERIC(9),
    @bitGetAssociatedTo BIT = 1,
	@numAssociationId AS NUMERIC(9)=0,
	@numDominId AS NUMERIC(9)=0
AS 
    BEGIN
		IF @numAssociationID > 0
		BEGIN
		     SELECT CA.*, CMP.vcCompanyName FROM [dbo].[CompanyAssociations] AS CA 
			 LEFT OUTER JOIN [DivisionMaster] DIV ON DIV.numDivisionID = CA.numDivisionID
                        LEFT OUTER JOIN CompanyInfo CMP ON DIV.numCompanyID = CMP.numCompanyId
			 WHERE [CA].[numAssociationID]=@numAssociationID AND [CA].[numDomainID]=@numDominId
		END			                
        ELSE IF @bitGetAssociatedTo = 1 
            BEGIN                    
				
                SELECT  numAssociationID,
                        CMP.vcCompanyName,
                        CA.numCompanyID,
                        DIV.vcDivisionName,
                        CA.numDivisionID,
                        ( SELECT    vcCompanyName
                          FROM      DivisionMaster INNER JOIN CompanyInfo ON DivisionMaster.numCompanyID = CompanyInfo.numCompanyID
                          WHERE     DivisionMaster.numDivisionID = CA.numDivisionID
                        ) AS TargetCompany,
                        ISNULL(ACI.numContactID,ACIDiv.numContactId) numContactID,
                        ISNULL(ACI.vcFirstname, ISNULL(ACIDiv.vcFirstname,'')) + ' '
                        + ISNULL(ACI.vcLastName, ISNULL(ACIDiv.vcLastName,'')) AS ContactName,
                        ISNULL(ACI.numPhone, ISNULL(ACIDiv.numPhone,'')) + ', '
                        + ISNULL(ACI.numPhoneExtension, ISNULL(ACIDiv.numPhoneExtension,'')) AS numPhone,
                        ISNULL(ACI.vcEmail,ISNULL(ACIDiv.numPhoneExtension,'')) vcEmail,
                        LD.vcData,
                        LD1.vcData AS Relationship,
                        LD2.vcData AS OrgProfile,
                        DIV.tintCRMType,
                        CASE WHEN CA.bitChildOrg = 1 THEN '(Child)'
                             --WHEN CA.bitChildOrg = 1 THEN '(Child)'
                             ELSE ''
                        END AS bitParentOrg,
                        CASE WHEN bitShareportal = 0 THEN 'No'
                             ELSE 'Yes'
                        END AS Shareportal,isnull(dbo.fn_GetState(AD2.numState),'') AS ShipTo,
						CAST(1 AS BIT) AS Allowedit
                FROM    CompanyAssociations CA
                        LEFT OUTER JOIN [DivisionMaster] DIV ON DIV.numDivisionID = CA.numAssociateFromDivisionID
                        LEFT OUTER JOIN CompanyInfo CMP ON DIV.numCompanyID = CMP.numCompanyId
                        LEFT OUTER JOIN [AdditionalContactsInformation] ACI ON ACI.[numContactId] = CA.[numContactID]
                        LEFT OUTER JOIN ListDetails LD ON LD.numListItemID = CA.numReferralType
						LEFT OUTER JOIN [DivisionMaster] DIV1 ON DIV1.numDivisionID = CA.numDivisionID
                        LEFT OUTER JOIN CompanyInfo CMP1 ON CMP1.numCompanyID = CA.numCompanyId AND Div1.numCompanyID=CMP1.numCompanyId
						LEFT OUTER JOIN [AdditionalContactsInformation] ACIDiv ON ACIDiv.numDivisionId = DIV1.numDivisionID AND ISNULL(ACIDiv.bitPrimaryContact,0) = 1
                        LEFT OUTER JOIN ListDetails LD1 ON LD1.numListItemID = CMP1.[numCompanyType]
                        LEFT OUTER JOIN ListDetails LD2 ON LD2.numListItemID = CMP1.[vcProfile]
						LEFT JOIN dbo.AddressDetails AD2 ON AD2.numDomainID=DIV.numDomainID 
						AND AD2.numRecordID= CA.numDivisionID AND AD2.tintAddressOf=2 AND AD2.tintAddressType=2 AND AD2.bitIsPrimary=1
                WHERE   CA.numAssociateFromDivisionID = @numDivID
                        AND CA.bitDeleted = 0

              
                
				union
				
						SELECT  numAssociationID,
                        CMP.vcCompanyName,
                        CA.numCompanyID,
                        DIV.vcDivisionName,
                        CA.numAssociateFromDivisionID as numDivisionID,
                        ( SELECT    vcCompanyName
                          FROM      DivisionMaster INNER JOIN CompanyInfo ON DivisionMaster.numCompanyID = CompanyInfo.numCompanyID
                          WHERE     DivisionMaster.numDivisionID = CA.numAssociateFromDivisionID
                        ) AS TargetCompany,
                        ACI.numContactID,
                        ISNULL(ACI.vcFirstname, '') + ' '
                        + ISNULL(ACI.vcLastName, '') AS ContactName,
                        ISNULL(ACI.numPhone, '') + ', '
                        + ISNULL(ACI.numPhoneExtension, '') AS numPhone,
                        ACI.vcEmail,
                        LD.vcData,
                        LD1.vcData AS OrgProfile,
                        LD2.vcData AS Relationship ,
                        DIV.tintCRMType,
                        CASE --WHEN CA.bitParentOrg = 1 THEN 'Child'
                             WHEN CA.bitChildOrg = 1 THEN '(Parent)'
                             ELSE ''
                        END AS bitParentOrg,
                        CASE WHEN bitShareportal = 0 THEN 'No'
                             ELSE 'Yes'
                        END AS Shareportal,isnull(dbo.fn_GetState(AD2.numState),'') AS ShipTo,
						--CAST((CASE WHEN CA.bitChildOrg = 1 THEN 0 ELSE 1 END)AS BIT) 
						CAST(0 AS BIT) AS Allowedit
                FROM    CompanyAssociations CA
                        LEFT OUTER JOIN [DivisionMaster] DIV ON DIV.numDivisionID = CA.numDivisionID
                        LEFT OUTER JOIN CompanyInfo CMP ON DIV.numCompanyID = CMP.numCompanyId
                        LEFT OUTER JOIN [AdditionalContactsInformation] ACI ON ACI.[numContactId] = CA.[numContactID]
                        LEFT OUTER JOIN ListDetails LD ON LD.numListItemID = CA.numReferralType --numReferralType
                        LEFT OUTER JOIN CompanyInfo CMP1 ON CMP1.numCompanyID = CA.numCompanyId
                        LEFT OUTER JOIN ListDetails LD1 ON LD1.numListItemID = CMP1.[numCompanyType]
                        LEFT OUTER JOIN ListDetails LD2 ON LD2.numListItemID = CMP1.[vcProfile]
						LEFT JOIN dbo.AddressDetails AD2 ON AD2.numDomainID=DIV.numDomainID 
						AND AD2.numRecordID= CA.numAssociateFromDivisionID AND AD2.tintAddressOf=2 AND AD2.tintAddressType=2 AND AD2.bitIsPrimary=1
                WHERE   CA.numDivisionID = @numDivID
                        AND CA.bitDeleted = 0 --AND isnull(CA.numAssoTypeLabel,0)<>0
                ORDER BY TargetCompany


                SELECT  CompanyInfo.vcCompanyName,
						DIV.[numDivisionID],
                        LD.vcData AS OrgProfile,
                        LD1.vcData AS Relationship,
                        ISNULL(ACI.vcFirstname, '') + ' '
                        + ISNULL(ACI.vcLastName, '') AS ContactName,
                        ISNULL(ACI.numPhone, '') + ', '
                        + ISNULL(ACI.numPhoneExtension, '') AS numPhone,
                        ACI.vcEmail,
                        ACI.[numContactId],isnull(dbo.fn_GetState(AD2.numState),'') AS ShipTo
                FROM    DivisionMaster AS DIV
                        LEFT OUTER JOIN CompanyInfo ON DIV.numCompanyID = CompanyInfo.numCompanyId
                        LEFT OUTER JOIN ListDetails AS LD ON [CompanyInfo].[vcProfile] = LD.[numListItemID]
                        LEFT OUTER JOIN ListDetails AS LD1 ON CompanyInfo.numCompanyType = LD1.numListItemID
                        LEFT OUTER JOIN AdditionalContactsInformation AS ACI ON DIV.numDivisionID = ACI.[numDivisionId]
						LEFT JOIN dbo.AddressDetails AD2 ON AD2.numDomainID=DIV.numDomainID 
						AND AD2.numRecordID= DIV.numDivisionID AND AD2.tintAddressOf=2 AND AD2.tintAddressType=2 AND AD2.bitIsPrimary=1
                WHERE   DIV.[numDivisionID] = @numDivID
                        AND ACI.bitPrimaryContact = 1
           
            END       
        ELSE 
            BEGIN      
                SELECT  numAssociationID,
                        CMP.vcCompanyName AS Company,
                        LD.vcData
                FROM    
					CompanyAssociations CA
				LEFT OUTER JOIN [DivisionMaster] DIV ON DIV.numDivisionID = CA.numDivisionID
                LEFT OUTER JOIN CompanyInfo CMP ON DIV.numCompanyID = CMP.numCompanyId
				LEFT OUTER JOIN ListDetails LD ON LD.numListItemID = CA.numReferralType --numReferralType
                WHERE   CA.bitDeleted = 0
                        AND CA.numDivisionID = @numDivID
                        AND DIV.numDivisionID = CA.numAssociateFromDivisionID
                        AND CMP.numCompanyID = DIV.numCompanyID
                        AND LD.numListItemID = CA.numReferralType
                GROUP BY numAssociationID,
                        CMP.vcCompanyName,
                        DIV.vcDivisionName,
                        LD.vcData
                ORDER BY vcCompanyName,
                        vcDivisionName       
            END           
    END

