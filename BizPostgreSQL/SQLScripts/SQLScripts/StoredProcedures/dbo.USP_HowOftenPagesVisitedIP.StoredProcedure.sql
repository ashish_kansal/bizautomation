/****** Object:  StoredProcedure [dbo].[USP_HowOftenPagesVisitedIP]    Script Date: 07/26/2008 16:18:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_howoftenpagesvisitedip')
DROP PROCEDURE usp_howoftenpagesvisitedip
GO
CREATE PROCEDURE [dbo].[USP_HowOftenPagesVisitedIP]             
@From varchar(12),              
@To varchar(12),              
@PageId as varchar(10)      
as                      
    
                 
declare @strSql as varchar(5000)                      
set @strSql='select  distinct(vcUserHostAddress),vcUserDomain from TrackingVisitorsHDR R Join TrackingVisitorsDTL T on T.numTracVisitorsHDRID = R.numTrackingID      
where T.numPageID='''+@PageId+''' and (dtCreated between '''+@From+''' and '''+@To+''')'      
                            
print @strSql                
    
exec( @strSql)
GO
