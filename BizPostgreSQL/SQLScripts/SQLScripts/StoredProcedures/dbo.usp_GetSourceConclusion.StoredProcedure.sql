/****** Object:  StoredProcedure [dbo].[usp_GetSourceConclusion]    Script Date: 07/26/2008 16:18:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- Created By Maha                
-- Last Modified By Anoop Jayaraj                
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getsourceconclusion')
DROP PROCEDURE usp_getsourceconclusion
GO
CREATE PROCEDURE [dbo].[usp_GetSourceConclusion]                  
 @numDomainID numeric,                  
 @dtFromDate datetime,                  
 @dtToDate datetime,                  
 @numUserCntID numeric = 0,                                
 @tintType numeric,                  
 @tintRights numeric=0,                  
 @intDealStatus numeric=1                      
--                  
AS                  
BEGIN                  
 If @tintRights=0                  
 -- ALL RECORDS RIGHTS                  
 BEGIN                  
   SELECT  isnull(LD.vcData,'None') as ConclusionBasis, COUNT(DM.numDivisionID) Total,                 
   (COUNT(DM.numDivisionID) * 100.00) /                  
    (select count(*) from DivisionMaster DM  
     join CompanyInfo CI   
     on DM.numCompanyID=CI.numCompanyID   
     left join ListDetails LD   
      on LD.numListItemID = CI.vcHow                   
    WHERE DM.numDomainID = @numDomainID            
     and DM.numCompanyID=CI.numCompanyID             
     AND DM.bintCreatedDate BETWEEN @dtFromDate AND @dtToDate                
     and DM.numRecOwner=@numUserCntID -- Added By Anoop              
    ) as Percentage                  
   FROM DivisionMaster DM
   join CompanyInfo CI   
   on DM.numCompanyID=CI.numCompanyID   
   left join ListDetails LD   
   on LD.numListItemID = CI.vcHow               
   WHERE DM.numDomainID = @numDomainID            
     AND DM.bintCreatedDate BETWEEN @dtFromDate AND @dtToDate                
     and DM.numRecOwner=@numUserCntID -- Added By Anoop            
     -- and CI.vcHow is not null -- Commented by Siva  
   GROUP BY  LD.vcData    
                             
                
 END                  
                  
 If @tintRights=1                  
 -- OWNER RECORDS RIGHTS                  
 BEGIN                 
              
    SELECT  isnull(LD.vcData,'None') as ConclusionBasis, COUNT(DM.numDivisionID) Total,                   
   (COUNT(DM.numDivisionID) * 100.00) /                    
    (select count(*) from DivisionMaster DM   
     join CompanyInfo CI   
     on DM.numCompanyID=CI.numCompanyID   
     left join ListDetails LD   
      on LD.numListItemID = CI.vcHow   
     join AdditionalContactsInformation  ADC   
     on ADC.numContactId= DM.numRecOwner                
    WHERE DM.numDomainID = @numDomainID            
     AND DM.bintCreatedDate BETWEEN @dtFromDate AND @dtToDate                
     and  ADC.numTeam  in(select F.numTeam from ForReportsByTeam F                     
     where F.numUserCntID=@numUserCntID and F.numDomainid=@numDomainId and F.tintType=@tintType)                  
    ) as Percentage                  
     FROM DivisionMaster DM  
     join CompanyInfo CI   
     on DM.numCompanyID=CI.numCompanyID   
     left join ListDetails LD   
     on LD.numListItemID = CI.vcHow   
     join AdditionalContactsInformation  ADC   
     on ADC.numContactId= DM.numRecOwner    
              
   WHERE DM.numDomainID = @numDomainID            
     --AND CI.vcHow is not null       
     AND DM.bintCreatedDate BETWEEN @dtFromDate AND @dtToDate                
     and ADC.numTeam in(select F.numTeam from ForReportsByTeam F                     
     where F.numUserCntID=@numUserCntID and F.numDomainid=@numDomainId and F.tintType=@tintType)                   
    GROUP BY  LD.vcData                  
 END                  
                  
 If @tintRights=2                  
 -- TERRITORY RECORDS RIGHTS                  
 BEGIN                 
 SELECT  isnull(LD.vcData,'None') as ConclusionBasis, COUNT(DM.numDivisionID) Total,                    
   (COUNT(DM.numDivisionID) * 100.00) /                    
    (select count(*) from DivisionMaster DM    
    join CompanyInfo CI   
    on DM.numCompanyID=CI.numCompanyID   
    left join ListDetails LD   
    on LD.numListItemID = CI.vcHow   
    join AdditionalContactsInformation  ADC   
    on ADC.numContactId= DM.numRecOwner  
                  
    WHERE DM.numDomainID = @numDomainID            
     --and DM.numCompanyID=CI.numCompanyID             
   AND DM.bintCreatedDate BETWEEN @dtFromDate AND @dtToDate                
     --and DM.numRecOwner in(select distinct(numUserCntID) from userterritory where numTerritoryID in(select F.numTerritory from ForReportsByTerritory F      
     and DM.numTerId  in(select F.numTerritory from ForReportsByTerritory F                                    
     where F.numUserCntID=@numUserCntID and F.numDomainid=@numDomainId and F.tintType=@tintType)                  
    ) as Percentage                  
   FROM DivisionMaster DM  
   join CompanyInfo CI   
    on DM.numCompanyID=CI.numCompanyID   
    left join ListDetails LD   
    on LD.numListItemID = CI.vcHow   
    join AdditionalContactsInformation  ADC   
    on ADC.numContactId= DM.numRecOwner  
                           
   WHERE DM.numDomainID = @numDomainID            
     --and CI.vcHow is not null                   
     AND DM.bintCreatedDate BETWEEN @dtFromDate AND @dtToDate                
     and DM.numTerId in(select F.numTerritory from ForReportsByTerritory F                     
     where F.numUserCntID=@numUserCntID and F.numDomainid=@numDomainId and F.tintType=@tintType)                   
   GROUP BY LD.vcData                 
 END                  
END
GO
