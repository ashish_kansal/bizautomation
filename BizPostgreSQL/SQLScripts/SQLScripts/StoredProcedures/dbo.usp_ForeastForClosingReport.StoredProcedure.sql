/****** Object:  StoredProcedure [dbo].[usp_ForeastForClosingReport]    Script Date: 07/26/2008 16:15:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_foreastforclosingreport')
DROP PROCEDURE usp_foreastforclosingreport
GO
CREATE PROCEDURE [dbo].[usp_ForeastForClosingReport]
	@sintYear smallint=0,
	@tintQuarter tinyint=0,
	@numDomainID numeric(9)=0   
--
AS
SELECT Distinct Forecast.numForecastID, Forecast.numCreatedBy 
FROM Forecast INNER JOIN AdditionalContactsInformation ON Forecast.numCreatedBy = AdditionalContactsInformation.numCreatedBy 
where Forecast.sintYear=@sintYear  and 
Forecast.tintQuarter=@tintQuarter
and AdditionalContactsInformation.numDomainID=@numDomainID
GO
