/****** Object:  StoredProcedure [dbo].[USP_JournalEntryDetailsCount]    Script Date: 07/26/2008 16:19:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created by Siva
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_journalentrydetailscount')
DROP PROCEDURE usp_journalentrydetailscount
GO
CREATE PROCEDURE [dbo].[USP_JournalEntryDetailsCount]  
@numJournalId as numeric(9),  
@numDomainId as numeric(9)  
AS   
Begin  
 Select count(numTransactionId) From General_Journal_Details   
 Where numJournalId=@numJournalId And numDomainId=@numDomainId  
End
GO
