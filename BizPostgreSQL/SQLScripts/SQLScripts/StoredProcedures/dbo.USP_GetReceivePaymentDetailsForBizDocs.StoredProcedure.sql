/****** Object:  StoredProcedure [dbo].[USP_GetReceivePaymentDetailsForBizDocs]    Script Date: 07/26/2008 16:18:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created By Siva            
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getreceivepaymentdetailsforbizdocs')
DROP PROCEDURE usp_getreceivepaymentdetailsforbizdocs
GO
CREATE PROCEDURE [dbo].[USP_GetReceivePaymentDetailsForBizDocs]            
(@numDomainId as numeric(9)=0,      
 @numOppId as numeric(9)=0,  
 @OppBizDocID as numeric(9)=0,
 @ClientTimeZoneOffset as INT,
 @dtFromDate DATETIME =NULL,
 @dtToDate DATETIME =NULL,
 @tintType TINYINT=0,
 @numDivisionId AS NUMERIC(9)=NULL      
)            
As            
Begin   

--IF @tintType=0       --Specific Order
--BEGIN
--
--SELECT @tintType=tintOppType FROM OpportunityMaster WHERE numDomainId=@numDomainId AND numOppId=@numOppId
--
--IF @tintType=1 --Sales
--BEGIN
--	 Select DD.numDepositeDetailID as numBizDocsPaymentDetId,Opp.numOppId as numOppId, Opp.numDivisionId as numDivisionId,            
--   DD.numOppBizDocsId as numBizDocsId, Opp.vcPOppName,DD.monAmountPaid As Amount,OppBizDocs.vcBizDocID,
--	dbo.fn_GetListItemName(DM.numPaymentMethod)  as PaymentMethod,   
--   dbo.fn_GetContactName(DM.numCreatedBy) +' - '+dbo.FormatedDateTimeFromDate(dateadd(minute,-@ClientTimeZoneOffset,DM.dtCreationDate),@numDomainId )  As CreatedBy,
--   DD.vcMemo as Memo,ISNULL(DD.vcReference,'') + ' ' + OppBizDocs.vcRefOrderNo as Reference,opp.tintopptype,C.vcCompanyName,0 AS bitCardAuthorized,0 as bitAmountCaptured,
--0 AS bitIntegratedToAcnt,0 as numComissionID 
--   FROM dbo.DepositMaster DM JOIN dbo.DepositeDetails DD ON DM.numDepositId=DD.numDepositId 
--Inner Join OpportunityBizDocs OppBizDocs on DD.numOppBizDocsID=OppBizDocs.numOppBizDocsId  
--   Inner Join OpportunityMaster Opp On OppBizDocs.numOppId = Opp.numOppId                                                 
--   Inner Join AdditionalContactsInformation ADC On Opp.numContactId = ADC.numContactId                                                 
--   Inner Join DivisionMaster Div On Opp.numDivisionId = Div.numDivisionID AND ADC.numDivisionId = Div.numDivisionID                                                 
--   Inner Join CompanyInfo C On Div.numCompanyID = C.numCompanyId             
--WHERE OppBizDocs.bitAuthoritativeBizDocs = 1 AND  DM.numDomainId=@numDomainId  
--   And Opp.numOppId=@numOppId and DD.numOppBizDocsId =@OppBizDocID
--   
----   UNION
----
----   Select OppBizDocsDet.numBizDocsPaymentDetId as numBizDocsPaymentDetId,Opp.numOppId as numOppId, Opp.numDivisionId as numDivisionId,            
----   BDC.numOppBizDocId as numBizDocsId, Opp.vcPOppName As Name,monAmount As Amount,OBD.vcBizDocID,
----	dbo.fn_GetListItemName(OppBizDocsDet.numPaymentMethod)  as PaymentMethod,   
----   dbo.fn_GetContactName(OppBizDocsDet.numCreatedBy) +' - '+dbo.FormatedDateTimeFromDate(dateadd(minute,-@ClientTimeZoneOffset,OppBizDocsDet.dtCreationDate),@numDomainId)  As CreatedBy,
----   '' as Memo,'Commission Expenses (' +I.vcItemName + ') - ' + dbo.fn_GetContactName(BDC.numUserCntID)   as Reference,
----   opp.tintopptype,C.vcCompanyName,ISNULL(OppBizDocsDet.bitCardAuthorized,0) bitCardAuthorized,ISNULL(OppBizDocsDet.bitAmountCaptured,0) bitAmountCaptured,isnull(OppBizDocsDet.bitIntegratedToAcnt,0) bitIntegratedToAcnt,BDC.numComissionID
----   FROM  BizDocComission BDC JOIN OpportunityBizDocs OBD ON BDC.numOppBizDocId = OBD.numOppBizDocsId
----		 join OpportunityMaster Opp on OPP.numOppId=OBD.numOppId
----		 JOIN OpportunityBizDocItems OBDI on OBDI.numOppBizDocID=OBD.numOppBizDocsId and BDC.numOppBizDocItemID=OBDI.numOppBizDocItemID
----		 join Item I on I.numItemCode=OBDI.numItemCode and I.numDomainID = @numDomainId
----		 JOIN Domain D on D.numDomainID=BDC.numDomainID
----		 JOIN OpportunityBizDocsDetails OppBizDocsDet ON BDC.numBizDocsPaymentDetId=isnull(OppBizDocsDet.numBizDocsPaymentDetId,0)
----		 Inner Join AdditionalContactsInformation ADC On Opp.numContactId = ADC.numContactId                                                 
----		 Inner Join DivisionMaster Div On Opp.numDivisionId = Div.numDivisionID AND ADC.numDivisionId = Div.numDivisionID                                                 
----		 Inner Join CompanyInfo C On Div.numCompanyID = C.numCompanyId
----	    WHERE   OPP.numDomainID = @numDomainId and BDC.numDomainID = @numDomainId
----				--AND BDC.bitCommisionPaid=0
----				AND OBD.bitAuthoritativeBizDocs = 1
----				--AND isnull(OppBizDocsDet.bitIntegratedToAcnt,0) = 1
----				And BDC.numOppId=@numOppId And BDC.numOppBizDocId=@OppBizDocID 
--END
--
--ELSE IF @tintType=2 --Purchase
--BEGIN
--	 Select BPD.numBillPaymentDetailID as numBizDocsPaymentDetId,Opp.numOppId as numOppId, Opp.numDivisionId as numDivisionId,            
--   BPD.numOppBizDocsId as numBizDocsId, Opp.vcPOppName,BPD.monAmount As Amount,OppBizDocs.vcBizDocID,
--	dbo.fn_GetListItemName(BPH.numPaymentMethod)  as PaymentMethod,   
--   dbo.fn_GetContactName(BPH.numCreatedBy) +' - '+dbo.FormatedDateTimeFromDate(dateadd(minute,-@ClientTimeZoneOffset,BPH.dtCreateDate),@numDomainId )  As CreatedBy,
--   '' as Memo,'' as Reference,opp.tintopptype,C.vcCompanyName,0 AS bitCardAuthorized,0 as bitAmountCaptured,0 AS bitIntegratedToAcnt,0 as numComissionID   FROM 
--dbo.BillPaymentHeader BPH JOIN dbo.BillPaymentDetails BPD ON BPH.numBillPaymentID=BPD.numBillPaymentID 
--LEFT OUTER JOIN dbo.CheckHeader CH ON CH.numReferenceID=BPH.numBillPaymentID AND CH.numReferenceID=8
--Inner Join OpportunityBizDocs OppBizDocs on BPD.numOppBizDocsID=OppBizDocs.numOppBizDocsId  
--   Inner Join OpportunityMaster Opp On OppBizDocs.numOppId = Opp.numOppId                                                 
--   Inner Join AdditionalContactsInformation ADC On Opp.numContactId = ADC.numContactId                                                 
--   Inner Join DivisionMaster Div On Opp.numDivisionId = Div.numDivisionID AND ADC.numDivisionId = Div.numDivisionID                                                 
--   Inner Join CompanyInfo C On Div.numCompanyID = C.numCompanyId             
--WHERE OppBizDocs.bitAuthoritativeBizDocs = 1 AND  BPH.numDomainId=@numDomainId  
--   And Opp.numOppId=@numOppId and BPD.numOppBizDocsId= @OppBizDocID
--
----UNION
--
----   Select OppBizDocsDet.numBizDocsPaymentDetId as numBizDocsPaymentDetId,Opp.numOppId as numOppId, Opp.numDivisionId as numDivisionId,            
----   BDC.numOppBizDocId as numBizDocsId, Opp.vcPOppName As Name,monAmount As Amount,OBD.vcBizDocID,
----	dbo.fn_GetListItemName(OppBizDocsDet.numPaymentMethod)  as PaymentMethod,   
----   dbo.fn_GetContactName(OppBizDocsDet.numCreatedBy) +' - '+dbo.FormatedDateTimeFromDate(dateadd(minute,-@ClientTimeZoneOffset,OppBizDocsDet.dtCreationDate),@numDomainId)  As CreatedBy,
----   '' as Memo,'Commission Expenses (' +I.vcItemName + ') - ' + dbo.fn_GetContactName(BDC.numUserCntID)   as Reference,
----   opp.tintopptype,C.vcCompanyName,ISNULL(OppBizDocsDet.bitCardAuthorized,0) bitCardAuthorized,ISNULL(OppBizDocsDet.bitAmountCaptured,0) bitAmountCaptured,isnull(OppBizDocsDet.bitIntegratedToAcnt,0) bitIntegratedToAcnt,BDC.numComissionID
----   FROM  BizDocComission BDC JOIN OpportunityBizDocs OBD ON BDC.numOppBizDocId = OBD.numOppBizDocsId
----		 join OpportunityMaster Opp on OPP.numOppId=OBD.numOppId
----		 JOIN OpportunityBizDocItems OBDI on OBDI.numOppBizDocID=OBD.numOppBizDocsId and BDC.numOppBizDocItemID=OBDI.numOppBizDocItemID
----		 join Item I on I.numItemCode=OBDI.numItemCode and I.numDomainID = @numDomainId
----		 JOIN Domain D on D.numDomainID=BDC.numDomainID
----		 JOIN OpportunityBizDocsDetails OppBizDocsDet ON BDC.numBizDocsPaymentDetId=isnull(OppBizDocsDet.numBizDocsPaymentDetId,0)
----		 Inner Join AdditionalContactsInformation ADC On Opp.numContactId = ADC.numContactId                                                 
----		 Inner Join DivisionMaster Div On Opp.numDivisionId = Div.numDivisionID AND ADC.numDivisionId = Div.numDivisionID                                                 
----		 Inner Join CompanyInfo C On Div.numCompanyID = C.numCompanyId
----	    WHERE   OPP.numDomainID = @numDomainId and BDC.numDomainID = @numDomainId
----				--AND BDC.bitCommisionPaid=0
----				AND OBD.bitAuthoritativeBizDocs = 1
----				--AND isnull(OppBizDocsDet.bitIntegratedToAcnt,0) = 1
----				And BDC.numOppId=@numOppId And BDC.numOppBizDocId=@OppBizDocID  
--END
--
--   
--
--END

--Else 
IF @tintType=1 OR @tintType=2     -- 1 : SO Receivable,2 : PO Payable
BEGIN

--SET @dtFromDate = DateAdd(minute, @ClientTimeZoneOffset, @dtFromDate);
--SET @dtToDate = DateAdd(minute, @ClientTimeZoneOffset, @dtToDate);

IF @tintType=1 --Sales
BEGIN
	 Select DD.numDepositId AS numReferenceID,DD.numDepositeDetailID as numReferenceDetailID,Opp.numOppId as numOppId, Opp.numDivisionId as numDivisionId,            
   DD.numOppBizDocsId as numBizDocsId, Opp.vcPOppName,DD.monAmountPaid As Amount,OppBizDocs.vcBizDocID,
	dbo.fn_GetListItemName(DM.numPaymentMethod)  as PaymentMethod,   
   dbo.fn_GetContactName(DM.numCreatedBy) +' - '+dbo.FormatedDateTimeFromDate(dateadd(minute,-@ClientTimeZoneOffset,DM.dtCreationDate),@numDomainId )  As CreatedBy,
   DD.vcMemo as Memo,ISNULL(DD.vcReference,'') + ' ' + OppBizDocs.vcRefOrderNo as Reference,opp.tintopptype,C.vcCompanyName,[dbo].[FormatedDateFromDate](DM.[dtDepositDate],@numDomainId) AS dtPaymentDate
   FROM dbo.DepositMaster DM JOIN dbo.DepositeDetails DD ON DM.numDepositId=DD.numDepositId 
Inner Join OpportunityBizDocs OppBizDocs on DD.numOppBizDocsID=OppBizDocs.numOppBizDocsId  
   Inner Join OpportunityMaster Opp On OppBizDocs.numOppId = Opp.numOppId                                                 
   Inner Join AdditionalContactsInformation ADC On Opp.numContactId = ADC.numContactId                                                 
   Inner Join DivisionMaster Div On Opp.numDivisionId = Div.numDivisionID AND ADC.numDivisionId = Div.numDivisionID                                                 
   Inner Join CompanyInfo C On Div.numCompanyID = C.numCompanyId             
WHERE OppBizDocs.bitAuthoritativeBizDocs = 1 AND  DM.numDomainId=@numDomainId  
AND DM.tintDepositePage=2
   and DM.dtDepositDate <= @dtToDate AND  DM.dtDepositDate >= @dtFromDate 
   AND Opp.tintOppType=1 AND (Opp.numOppId=@numOppId OR @numOppId=0) and (OppBizDocs.numOppBizDocsId =@OppBizDocID OR @OppBizDocID=0)
   AND (Opp.numDivisionId = @numDivisionId OR @numDivisionId IS NULL)
END

ELSE IF @tintType=2 --Purchase
BEGIN
	 Select BPH.numBillPaymentID AS numReferenceID,BPD.numBillPaymentDetailID as numReferenceDetailID,Opp.numOppId as numOppId, Opp.numDivisionId as numDivisionId,            
   BPD.numOppBizDocsId as numBizDocsId, Opp.vcPOppName,BPD.monAmount As Amount,OppBizDocs.vcBizDocID,
	dbo.fn_GetListItemName(BPH.numPaymentMethod)  as PaymentMethod,   
   dbo.fn_GetContactName(BPH.numCreatedBy) +' - '+dbo.FormatedDateTimeFromDate(dateadd(minute,-@ClientTimeZoneOffset,BPH.dtCreateDate),@numDomainId )  As CreatedBy,
   '' as Memo,ISNULL(OppBizDocs.vcRefOrderNo,'') as Reference,opp.tintopptype,C.vcCompanyName,[dbo].[FormatedDateFromDate]([BPH].[dtPaymentDate],@numDomainId) AS dtPaymentDate   FROM 
dbo.BillPaymentHeader BPH JOIN dbo.BillPaymentDetails BPD ON BPH.numBillPaymentID=BPD.numBillPaymentID 
LEFT OUTER JOIN dbo.CheckHeader CH ON CH.numReferenceID=BPH.numBillPaymentID AND CH.numReferenceID=8
Inner Join OpportunityBizDocs OppBizDocs on BPD.numOppBizDocsID=OppBizDocs.numOppBizDocsId  
   Inner Join OpportunityMaster Opp On OppBizDocs.numOppId = Opp.numOppId                                                 
   Inner Join AdditionalContactsInformation ADC On Opp.numContactId = ADC.numContactId                                                 
   Inner Join DivisionMaster Div On Opp.numDivisionId = Div.numDivisionID AND ADC.numDivisionId = Div.numDivisionID                                                 
   Inner Join CompanyInfo C On Div.numCompanyID = C.numCompanyId             
WHERE OppBizDocs.bitAuthoritativeBizDocs = 1 AND  BPH.numDomainId=@numDomainId  
   and BPH.dtPaymentDate <= @dtToDate AND  BPH.dtPaymentDate >= @dtFromDate 
   AND Opp.tintOppType=2 AND (Opp.numDivisionId = @numDivisionId OR @numDivisionId IS NULL)

END
--   Select OppBizDocsDet.numBizDocsPaymentDetId as numBizDocsPaymentDetId,Opp.numOppId as numOppId, Opp.numDivisionId as numDivisionId,            
--   OppBizDocsDet.numBizDocsId as numBizDocsId, Opp.vcPOppName As Name,monAmount As Amount,
--	dbo.fn_GetListItemName(OppBizDocsDet.numPaymentMethod)  as PaymentMethod,   
--   dbo.fn_GetContactName(OppBizDocsDet.numCreatedBy) +' - '+dbo.FormatedDateTimeFromDate(dateadd(minute,-@ClientTimeZoneOffset,OppBizDocsDet.dtCreationDate),@numDomainId)  As CreatedBy,isnull(OppBizDocsDet.vcMemo,'') as Memo,isnull(OppBizDocsDet.vcReference,'') as Reference,
--   Opp.vcPOppName,opp.tintopptype,C.vcCompanyName,isnull(OppBizDocsDet.bitIntegratedToAcnt,0) bitIntegratedToAcnt,0 as numComissionID  
--   From OpportunityBizDocsDetails OppBizDocsDet  
--   Inner Join OpportunityBizDocs OppBizDocs on OppBizDocsDet.numBizDocsId=OppBizDocs.numOppBizDocsId  
--   Inner Join OpportunityMaster Opp On OppBizDocs.numOppId = Opp.numOppId                                                 
--   Inner Join AdditionalContactsInformation ADC On Opp.numContactId = ADC.numContactId                                                 
--   Inner Join DivisionMaster Div On Opp.numDivisionId = Div.numDivisionID AND ADC.numDivisionId = Div.numDivisionID                                                 
--   Inner Join CompanyInfo C On Div.numCompanyID = C.numCompanyId             
--   WHERE OppBizDocs.bitAuthoritativeBizDocs = 1 AND  OppBizDocsDet.numDomainId=@numDomainId 
--   and OppBizDocsDet.dtCreationDate <= @dtToDate AND  OppBizDocsDet.dtCreationDate >=@dtFromDate 
--   AND Opp.tintOppType=@tintType        
END

End
GO
