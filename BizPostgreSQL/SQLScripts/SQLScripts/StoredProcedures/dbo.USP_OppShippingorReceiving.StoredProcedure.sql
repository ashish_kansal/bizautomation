/****** Object:  StoredProcedure [dbo].[USP_OppShippingorReceiving]    Script Date: 07/26/2008 16:20:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- created by anoop jayaraj            
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_oppshippingorreceiving')
DROP PROCEDURE usp_oppshippingorreceiving
GO
CREATE PROCEDURE [dbo].[USP_OppShippingorReceiving]            
@OppID as numeric(9),
@numUserCntID AS NUMERIC(9)          
as
BEGIN TRY
	declare @status as TINYINT            
	declare @OppType as TINYINT   
	declare @bitStockTransfer as BIT
	DECLARE @fltExchangeRate AS FLOAT 
	DECLARE @numDomain AS NUMERIC(18,0)
	SELECT @numDomain = OM.numDomainID,@status=tintOppStatus,@OppType=tintOppType,@bitStockTransfer=ISNULL(bitStockTransfer,0),@fltExchangeRate=(CASE WHEN ISNULL(fltExchangeRate,0)=0 THEN 1 ELSE fltExchangeRate END) FROM [dbo].[OpportunityMaster] AS OM WHERE [OM].[numOppId] = @OppID
	
	-- Added by Manish Anjara : 17th Sep,2014
	-- Check whether order is already closed or not. If closed then show a warning message
	IF (SELECT ISNULL(tintshipped,0) FROM [dbo].[OpportunityMaster] WHERE [OpportunityMaster].[numOppId] = @OppID AND [OpportunityMaster].[numDomainId] = @numDomain) = 1
	BEGIN
		RAISERROR('ORDER_CLOSED', 16, 1)
	END		

	IF @OppType = 2 AND @status=1 AND ISNULL(@bitStockTransfer,0)=0 AND (SELECT 
			COUNT(*) 
		FROM 
			OpportunityMaster OM 
		INNER JOIN 
			OpportunityItems OI 
		ON 
			OM.numOppId=OI.numOppId 
		LEFT JOIN 
			WareHouseItems WI 
		ON 
			OI.numWarehouseItmsID=WI.numWareHouseItemID 
		WHERE 
			OM.numDomainId=@numDomain 
			AND OM.numOppId=@OppID
			AND WI.numWLocationID = -1
			AND OI.numUnitHour <> OI.numUnitHourReceived) > 0
	BEGIN
		RAISERROR('GLOCAL_LOCATION_PENDING_FULFILLMENT', 16, 1)
	END

   BEGIN TRANSACTION 
			
			update OpportunityMaster set tintshipped=1,bintAccountClosingDate=GETUTCDATE(), bintClosedDate=GETUTCDATE() where  numOppId=@OppID 
			UPDATE [OpportunityBizDocs] SET [dtShippedDate] = GETUTCDATE() WHERE [numOppId]=@OppID AND numBizDocID <> 296

			UPDATE [OpportunityBizDocs] SET [dtShippedDate] = GETUTCDATE() WHERE [numOppId]=@OppID AND numBizDocID = 296 AND dtShippedDate IS NULL
			
						
			
       
			
			-- Archive item based on Item's individual setting
			IF @OppType = 1
			BEGIN
				UPDATE Item SET IsArchieve = CASE WHEN EXISTS(SELECT OpportunityItems.numItemCode FROM dbo.OpportunityItems WHERE OpportunityItems.numItemCode = I.numItemCode) 
												  THEN 1
												  ELSE 0 
											 END 
				FROM Item I JOIN dbo.OpportunityItems OI ON I.numItemCode = OI.numItemCode  
				WHERE numOppId = @OppID 
				AND I.[numDomainID] = @numDomain

				AND ISNULL(I.bitArchiveItem,0) = 1 
			END
			----------------------------------------------------------------------------            
			--Updating Item Details            
			-----------------------------------------------------------------------            
            
			---Updating the inventory Items once the deal is won
			declare @numoppitemtCode as numeric
			declare @numUnits as FLOAT
			declare @numWarehouseItemID as numeric       
			declare @numToWarehouseItemID as numeric(9) --to be used with stock transfer
			declare @itemcode as numeric        
			declare @QtyShipped as FLOAT
			declare @QtyReceived as FLOAT
			Declare @monPrice as DECIMAL(20,5)
			declare @Kit as bit
			declare @bitWorkOrder AS BIT
			declare @bitSerialized AS BIT
			declare @bitLotNo AS BIT
    
			 select top 1 @numoppitemtCode=numoppitemtCode,@itemcode=OI.numItemCode,@numUnits=ISNULL(numUnitHour,0),@qtyShipped=ISNULL([numQtyShipped],0),@qtyReceived=ISNULL([numUnitHourReceived],0),
			 @numWareHouseItemID=ISNULL(numWarehouseItmsID,0),@numToWarehouseItemID=numToWarehouseItemID,@monPrice=isnull(monPrice,0) * @fltExchangeRate,@Kit= (case when bitKitParent=1 and bitAssembly=1 then 0 when bitKitParent=1 then 1 else 0 end),
			 @bitWorkOrder = ISNULL(bitWorkOrder,0),@bitSerialized=ISNULL(bitSerialized,0),@bitLotNo=ISNULL(bitLotNo,0)
			 from OpportunityItems OI join Item I                                                
			 on OI.numItemCode=I.numItemCode and numOppId=@OppID and (bitDropShip=0 or bitDropShip is null)                                            
			 where (charitemtype='P' OR 1=(CASE WHEN @OppType=1 THEN 
							CASE WHEN ISNULL(I.bitKitParent,0) = 1 AND ISNULL(I.bitAssembly,0) = 1 THEN 0
								 WHEN ISNULL(I.bitKitParent,0) = 1 THEN 1
								 ELSE 0 END 
							ELSE 0 END))  
			 AND I.[numDomainID] = @numDomain	
							order by OI.numoppitemtCode      

			 while @numoppitemtCode>0                
			 begin   
 
--			  if @Kit = 1
--			  begin
--					exec USP_UpdateKitItems @numoppitemtCode,@numUnits,@OppType,2,@OppID
--			  end
             
             
				IF @bitStockTransfer = 1
				BEGIN
					--Make inventory changes as if sales order was closed , using OpportunityItems.numWarehouseItmsID
					EXEC usp_ManageInventory @itemcode,@numWareHouseItemID,0,1,@numUnits,@qtyShipped,@qtyReceived,@monPrice,2,@OppID,@numoppitemtCode,@numUserCntID,@bitWorkOrder
					--Make inventory changes as if purchase order was closed, using OpportunityItems.numToWarehouseItemID
					EXEC usp_ManageInventory @itemcode,@numToWarehouseItemID,0,2,@numUnits,@qtyShipped,@qtyReceived,@monPrice,2,@OppID,@numoppitemtCode,@numUserCntID,@bitWorkOrder
					If @bitSerialized = 1 OR @bitLotNo = 1
					BEGIN
						--Transfer Serial/Lot Numbers
						EXEC USP_WareHouseItmsDTL_TransferSerialLotNo @OppID,@numoppitemtCode,@numWareHouseItemID,@numToWarehouseItemID,@bitSerialized,@bitLotNo
					END
				END
				ELSE
				BEGIN
					EXEC usp_ManageInventory @itemcode,@numWareHouseItemID,0,@OppType,@numUnits,@qtyShipped,@qtyReceived,@monPrice,2,@OppID,@numoppitemtCode,@numUserCntID,@bitWorkOrder
				END
        
			  select top 1 @numoppitemtCode=numoppitemtCode,@itemcode=OI.numItemCode,@numUnits=ISNULL(numUnitHour,0),@qtyShipped=ISNULL([numQtyShipped],0),@qtyReceived=ISNULL([numUnitHourReceived],0),
			 @numWareHouseItemID=ISNULL(numWarehouseItmsID,0),@numToWarehouseItemID=numToWarehouseItemID,@monPrice=isnull(monPrice,0) * @fltExchangeRate,@Kit= (case when bitKitParent=1 and bitAssembly=1 then 0 when bitKitParent=1 then 1 else 0 end),
			 @bitWorkOrder = ISNULL(bitWorkOrder,0),@bitSerialized=ISNULL(bitSerialized,0),@bitLotNo=ISNULL(bitLotNo,0)
			  from OpportunityItems OI
			  join Item I
			  on OI.numItemCode=I.numItemCode and numOppId=@OppID                                              
			  where (charitemtype='P' OR 1=(CASE WHEN @OppType=1 THEN 
							CASE WHEN ISNULL(I.bitKitParent,0) = 1 AND ISNULL(I.bitAssembly,0) = 1 THEN 0
								 WHEN ISNULL(I.bitKitParent,0) = 1 THEN 1
								 ELSE 0 END 
							ELSE 0 END)) and OI.numoppitemtCode>@numoppitemtCode and (bitDropShip=0 or bitDropShip is null) 
			  AND I.numDomainID = @numDomain
			  ORDER by OI.numoppitemtCode                                                
			  if @@rowcount=0 set @numoppitemtCode=0     
  
			 END
 COMMIT
END TRY
BEGIN CATCH
  -- Whoops, there was an error
  IF @@TRANCOUNT > 0
     ROLLBACK

  -- Raise an error with the details of the exception
  DECLARE @ErrMsg nvarchar(4000), @ErrSeverity int
  SELECT @ErrMsg = ERROR_MESSAGE(),
         @ErrSeverity = ERROR_SEVERITY()

  RAISERROR(@ErrMsg, @ErrSeverity, 1)
END CATCH

GO
