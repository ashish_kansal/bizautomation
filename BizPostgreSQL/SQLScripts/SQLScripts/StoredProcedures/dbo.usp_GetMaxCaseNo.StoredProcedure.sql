/****** Object:  StoredProcedure [dbo].[usp_GetMaxCaseNo]    Script Date: 07/26/2008 16:17:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getmaxcaseno')
DROP PROCEDURE usp_getmaxcaseno
GO
CREATE PROCEDURE [dbo].[usp_GetMaxCaseNo]
	@numDomainID numeric(9)=0   
--
AS
			SELECT vcCaseNumber
			FROM Cases
			WHERE numDomainID=@numDomainID
ORDER BY vcCaseNumber DESC


--END
GO
