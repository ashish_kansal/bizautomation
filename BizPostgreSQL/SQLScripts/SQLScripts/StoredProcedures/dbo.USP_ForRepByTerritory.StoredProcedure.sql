/****** Object:  StoredProcedure [dbo].[USP_ForRepByTerritory]    Script Date: 07/26/2008 16:16:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj            
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_forrepbyterritory')
DROP PROCEDURE usp_forrepbyterritory
GO
CREATE PROCEDURE [dbo].[USP_ForRepByTerritory]              
@numUserCntId as numeric(9)=null,              
@numDomainID as numeric(9)=null,              
@tintType as tinyint             
as              
begin              
    
select D.numDivisionID,C.vcCompanyName,vcData as vcTerName,L.numListItemID as numTerID from ExtarnetAccounts E    
join ExtranetAccountsDtl DTL    
on DTL.numExtranetID=E.numExtranetID    
join DivisionMaster D     
on D.numDivisionID=E.numDivisionID    
join CompanyInfo C    
on C.numCompanyID=D.numCompanyID    
join ListDetails L  
on L.numListItemID=D.numTerID    
where bitPartnerAccess=1 and  D.numTerID in (select F.numTerritory from ForReportsByTerritory F               
where F.numUserCntId=@numUserCntId and F.numDomainid=@numDomainID and F.tintType=@tintType)    
           
end
GO
