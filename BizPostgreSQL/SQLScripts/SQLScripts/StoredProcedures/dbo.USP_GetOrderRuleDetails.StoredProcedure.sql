GO
/****** Object:  StoredProcedure [dbo].[USP_GetOrderRuleDetails]    Script Date: 03/04/2010 11:39:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--EXEC USP_GetOrderRuleDetails @numDomainID=72,@numRuleID=1
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getorderruledetails')
DROP PROCEDURE usp_getorderruledetails
GO
CREATE PROCEDURE [dbo].[USP_GetOrderRuleDetails]
@numDomainID numeric,
@numRuleID numeric(18,0)

as

	create table #TempItemType
	(numTypeID numeric,
	vcType varchar(50),
	);
	
	insert into #TempItemType 
	select 21,'-Select One--'
	union
	select 1,'Services'
	union
	select 2,'Inventory Items'
	union
	select 3,'Non-Inventory Items'
	union
	select 4,'Serialized Items'
	union
	select 5,'Kits'
	union
	select 6,'Assemblies'
	union
	select 7,'Matrix Items'

	select TIT.vcType as [Item Type], ITC.vcData as [Item Classification],(case numBillToID when 0 then 'Employer' when 1 then 'Customer' when 2 then 'Other' end) as [Bill To],
	(case numShipToID when 0 then 'Employer' when 1 then 'Customer' when 2 then 'Other' end) as [Ship To],
	(case btFullPaid when 0 then 'No' when 1 then 'Yes' end) as [Amount Paid Full],
	(case numBizDocStatus when 0 then 'No' else ITB.vcData end) AS [Biz Doc Status],
	OAR.numItemTypeID,	
	OAR.numItemClassID,
	numBillToID,
	numShipToID,
	btFullPaid,
	numBizDocStatus,
	OAR.numRuleDetailsID,
	isnull(btActive,0) as btActive
	from  OrderAutoRuleDetails OAR INNER JOIN #TempItemType TIT ON TIT.numTypeID=OAR.numItemTypeID
			INNER JOIN 
		(select LD.vcData,LD.numListItemID from ListDetails LD,LISTMASTER LM  where LM.numListID=36 
		and LD.numListID=LM.numListID AND LD.numDomainID=@numDomainID) ITC ON 
		ITC.numListItemID=OAR.numItemClassID  
		LEFT OUTER JOIN
		(select LD.vcData,LD.numListItemID from ListDetails LD,LISTMASTER LM  where LM.numListID=11 
		and LD.numListID=LM.numListID AND LD.numDomainID=@numDomainID)	ITB
		ON ITB.numListItemID=numBizDocStatus 
		
		
		where numRuleID=@numRuleID

DROP TABLE #TempItemType;



