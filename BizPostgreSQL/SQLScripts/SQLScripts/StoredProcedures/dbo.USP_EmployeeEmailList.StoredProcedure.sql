/****** Object:  StoredProcedure [dbo].[USP_EmployeeEmailList]    Script Date: 07/26/2008 16:15:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- Created By Anoop Jayaraj    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_employeeemaillist')
DROP PROCEDURE usp_employeeemaillist
GO
CREATE PROCEDURE [dbo].[USP_EmployeeEmailList]      
@numUserID as numeric(9)      
as      
SELECT UM.vcEmailID as numUserID
from UserMaster UM        
where UM.numUserID=@numUserID
GO
