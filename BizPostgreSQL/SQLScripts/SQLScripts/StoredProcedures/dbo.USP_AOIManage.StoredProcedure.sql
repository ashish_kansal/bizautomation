/****** Object:  StoredProcedure [dbo].[USP_AOIManage]    Script Date: 07/26/2008 16:14:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_aoimanage')
DROP PROCEDURE usp_aoimanage
GO
CREATE PROCEDURE [dbo].[USP_AOIManage]  
@numContactID as numeric(9)=null,  
@strAOI as text=''  
as  
  
declare @hDoc  int  
delete from AOIContactLink where numContactID=@numContactID  
EXEC sp_xml_preparedocument @hDoc OUTPUT, @strAOI  
insert into AOIContactLink (numAOIID,numContactID,tintSelected)   
 select X.numAOIId,@numContactID,X.Status  from(SELECT *FROM OPENXML (@hDoc,'/NewDataSet/Table',2)  
 WITH ( numAOIId numeric(9),  
  Status tinyint  
  ))X  
  
 EXEC sp_xml_removedocument @hDoc
GO
