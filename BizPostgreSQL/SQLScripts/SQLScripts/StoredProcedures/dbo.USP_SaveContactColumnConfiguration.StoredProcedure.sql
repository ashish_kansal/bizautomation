/****** Object:  StoredProcedure [dbo].[USP_SaveContactColumnConfiguration]    Script Date: 07/26/2008 16:20:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_savecontactcolumnconfiguration')
DROP PROCEDURE usp_savecontactcolumnconfiguration
GO
CREATE PROCEDURE [dbo].[USP_SaveContactColumnConfiguration]  
 @numDomainID Numeric(9),      
 @numUserCntId Numeric(9),    
 @FormId tinyint ,  
@numtype numeric(9) ,  
@strXml   as text =''    
AS   
  
  
delete from [InitialListColumnConf] where numDomainID=@numDomainID and [numFormId]= @FormId    
and [numUserCntId]= @numUserCntId and numtype=@numtype  
  
  
DECLARE @hDoc1 int                                    
 EXEC sp_xml_preparedocument @hDoc1 OUTPUT, @strXml                                    
                                    
INSERT INTO [InitialListColumnConf]  
           ([numDomainId]  
           ,[numUserCntId]  
           ,[numFormId]  
           ,[numFormFieldID]  
           ,[numType]  
           ,[tintOrder])                                 
                                     
 select @numDomainID,@numUserCntId,@FormId,X.numFormFieldID,@numtype,X.tintOrder from (SELECT *FROM OPENXML (@hDoc1,'/NewDataSet/Table',2)                                    
 WITH (numFormFieldID numeric(9),    
       tintOrder tinyint))X                                   
                                    
                                    
 EXEC sp_xml_removedocument @hDoc1
GO
