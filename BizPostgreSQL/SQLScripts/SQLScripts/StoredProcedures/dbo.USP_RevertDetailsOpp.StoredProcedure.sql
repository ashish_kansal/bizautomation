/****** Object:  StoredProcedure [dbo].[USP_RevertDetailsOpp]    Script Date: 07/26/2008 16:20:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj      
/* deducts Qty from allocation and Adds back to onhand */
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_RevertDetailsOpp')
DROP PROCEDURE USP_RevertDetailsOpp
GO
CREATE PROCEDURE [dbo].[USP_RevertDetailsOpp] 
@numOppId NUMERIC(18,0),
@numOppItemId NUMERIC(18,0),
@tintMode AS TINYINT=0, -- 0:Add/Edit, 1:Delete, 2: Demoted to opportunity
@numUserCntID AS NUMERIC(9)
AS ---reverting back to previous state if deal is being edited
BEGIN 
	DECLARE @numDomain AS NUMERIC(18,0)
	DECLARE @tintCommitAllocation AS TINYINT
	SELECT @numDomain = OM.numDomainID, @tintCommitAllocation=ISNULL(tintCommitAllocation,1)  FROM [dbo].[OpportunityMaster] AS OM INNER JOIN Domain D ON OM.numDomainId = D.numDomainId WHERE [OM].[numOppId] = @numOppID
    
	DECLARE @OppType AS VARCHAR(2)              
    DECLARE @itemcode AS NUMERIC       
    DECLARE @numWareHouseItemID AS NUMERIC
	DECLARE @numWLocationID NUMERIC(18,0)                     
    DECLARE @numToWarehouseItemID AS NUMERIC 
    DECLARE @numUnits AS FLOAT                                              
    DECLARE @onHand AS FLOAT                                            
    DECLARE @onOrder AS FLOAT                                            
    DECLARE @onBackOrder AS FLOAT                                              
    DECLARE @onAllocation AS FLOAT
    DECLARE @numQtyShipped AS FLOAT
    DECLARE @numUnitHourReceived AS FLOAT
	DECLARE @numDeletedReceievedQty AS FLOAT
    DECLARE @numoppitemtCode AS NUMERIC(9) 
    DECLARE @monAmount AS DECIMAL(20,5) 
    DECLARE @monAvgCost AS DECIMAL(20,5)   
    DECLARE @Kit AS BIT                                        
    DECLARE @bitKitParent BIT
    DECLARE @bitStockTransfer BIT 
    DECLARE @numOrigUnits AS FLOAT			
    DECLARE @description AS VARCHAR(100)
	DECLARE @bitWorkOrder AS BIT
	--Added by :Sachin Sadhu||Date:18thSept2014
	--For Rental/Asset Project
	Declare @numRentalIN as FLOAT
	Declare @numRentalOut as FLOAT
	Declare @numRentalLost as FLOAT
	DECLARE @bitAsset as BIT
	--end sachin

    						
    SELECT TOP 1
            @numoppitemtCode = numoppitemtCode,
            @itemcode = OI.numItemCode,
            @numUnits = ISNULL(numUnitHour,0),
            @numWareHouseItemID = ISNULL(numWarehouseItmsID,0),
			@numWLocationID = ISNULL(numWLocationID,0),
            @Kit = ( CASE WHEN bitKitParent = 1
                               AND bitAssembly = 1 THEN 0
                          WHEN bitKitParent = 1 THEN 1
                          ELSE 0
                     END ),
            @monAmount = ISNULL(monTotAmount,0) * (CASE WHEN ISNULL(fltExchangeRate,0)=0 THEN 1 ELSE fltExchangeRate END),
            @monAvgCost = ISNULL(monAverageCost,0),
            @numQtyShipped = ISNULL(numQtyShipped,0),
            @numUnitHourReceived = ISNULL(numUnitHourReceived,0),
			@numDeletedReceievedQty = ISNULL(numDeletedReceievedQty,0),
            @bitKitParent=ISNULL(bitKitParent,0),
            @numToWarehouseItemID =OI.numToWarehouseItemID,
            @bitStockTransfer = ISNULL(OM.bitStockTransfer,0),
            @OppType = tintOppType,
		    @numRentalIN=ISNULL(oi.numRentalIN,0),
			@numRentalOut=Isnull(oi.numRentalOut,0),
			@numRentalLost=Isnull(oi.numRentalLost,0),
			@bitAsset =ISNULL(I.bitAsset,0),
			@bitWorkOrder = ISNULL(OI.bitWorkOrder,0)
    FROM    OpportunityItems OI
			LEFT JOIN WareHouseItems WI ON OI.numWarehouseItmsID=WI.numWareHouseItemID
			JOIN dbo.OpportunityMaster OM ON OI.numOppId = OM.numOppId
            JOIN Item I ON OI.numItemCode = I.numItemCode
    WHERE 
		(ISNULL(@numOppItemId,0) = 0 OR OI.numoppitemtCode=@numOppItemId) 
		AND (charitemtype='P' OR 1=(CASE WHEN tintOppType=1 THEN 
							CASE WHEN ISNULL(I.bitKitParent,0) = 1 AND ISNULL(I.bitAssembly,0) = 1 THEN 0
								 WHEN ISNULL(I.bitKitParent,0) = 1 THEN 1
								 ELSE 0 END 
							ELSE 0 END)) AND OI.numOppId = @numOppId
                           AND ( bitDropShip = 0
                                 OR bitDropShip IS NULL
                               ) 
    ORDER BY OI.numoppitemtCode

	
    WHILE @numoppitemtCode > 0                                  
    BEGIN    
        SET @numOrigUnits=@numUnits
            
        IF @bitStockTransfer=1
        BEGIN
			SET @OppType = 1
		END
                 
        IF @numWareHouseItemID>0
        BEGIN                                
			SELECT  @onHand = ISNULL(numOnHand, 0),
					@onAllocation = ISNULL(numAllocation, 0),
					@onOrder = ISNULL(numOnOrder, 0),
					@onBackOrder = ISNULL(numBackOrder, 0)
			FROM    WareHouseItems
			WHERE   numWareHouseItemID = @numWareHouseItemID                                               
        END
            
        IF @OppType = 1 AND @tintCommitAllocation=1
        BEGIN
			IF @tintMode = 2
			BEGIN
				SET @description=CONCAT('SO DEMOTED TO OPPORTUNITY (Qty:',@numUnits,' Shipped:',@numQtyShipped,')')
			END
			ELSE
			BEGIN
				SET @description=CONCAT('SO Deleted (Qty:',@numUnits,' Shipped:',@numQtyShipped,')')
			END
                
            IF @Kit = 1
			BEGIN
				exec USP_UpdatingInventoryForEmbeddedKits @numoppitemtCode,@numOrigUnits,1,@numOppID,@tintMode,@numUserCntID
			END	
			ELSE
			BEGIN
				IF @numQtyShipped>0
				BEGIN
					IF @tintMode=0
						SET @numUnits = @numUnits - @numQtyShipped
					ELSE IF @tintmode=1 OR @tintMode=2
						SET @onAllocation = @onAllocation + @numQtyShipped 
				END 
								                    
                IF @numUnits >= @onBackOrder 
                BEGIN
                    SET @numUnits = @numUnits - @onBackOrder
                    SET @onBackOrder = 0
                            
                    IF (@onAllocation - @numUnits >= 0)
						SET @onAllocation = @onAllocation - @numUnits

					IF @bitAsset=1--Not Asset
					BEGIN
						SET @onHand = @onHand +@numRentalIN+@numRentalLost+@numRentalOut     
					END
					ELSE
					BEGIN
						SET @onHand = @onHand + @numUnits     
					END                                         
                END                                            
                ELSE IF @numUnits < @onBackOrder 
                BEGIN
					IF (@onBackOrder - @numUnits >0)
						SET @onBackOrder = @onBackOrder - @numUnits
                END 
                 	
				UPDATE
					WareHouseItems
				SET 
					numOnHand = @onHand ,
					numAllocation = @onAllocation,
					numBackOrder = @onBackOrder,
					dtModified = GETDATE()
				WHERE 
					numWareHouseItemID = @numWareHouseItemID              
			END
				

			IF @numWareHouseItemID>0 AND @description <> 'DO NOT ADD WAREHOUSE TRACKING'
			BEGIN 
					EXEC dbo.USP_ManageWareHouseItems_Tracking
					@numWareHouseItemID = @numWareHouseItemID, --  numeric(9, 0)
					@numReferenceID = @numOppId, --  numeric(9, 0)
					@tintRefType = 3, --  tinyint
					@vcDescription = @description, --  varchar(100)
					@numModifiedBy = @numUserCntID,
					@numDomainID = @numDomain 
			END		
		END      
          
        IF @bitStockTransfer=1
        BEGIN
			SET @numWareHouseItemID = @numToWarehouseItemID;
			SET @OppType = 2
			SET @numUnits = @numOrigUnits

			SELECT
				@onHand = ISNULL(numOnHand, 0),
                @onAllocation = ISNULL(numAllocation, 0),
                @onOrder = ISNULL(numOnOrder, 0),
                @onBackOrder = ISNULL(numBackOrder, 0)
			FROM 
				WareHouseItems
			WHERE
				numWareHouseItemID = @numWareHouseItemID   
		END
          
        IF @OppType = 2 
        BEGIN 
			IF (@tintmode = 1 OR @tintMode=2) AND ISNULL(@bitStockTransfer,0)=0 AND EXISTS (SELECT ID FROM OpportunityItemsReceievedLocation WHERE numDomainID=@numDomain AND numOppID=@numOppId AND numOppItemID=@numoppitemtCode)
			BEGIN
				DECLARE @TEMPReceievedItems TABLE
				(
					ID INT,
					numOIRLID NUMERIC(18,0),
					numWarehouseItemID NUMERIC(18,0),
					numUnitReceieved FLOAT,
					numDeletedReceievedQty FLOAT
				)

				DELETE FROM @TEMPReceievedItems

				INSERT INTO
					@TEMPReceievedItems
				SELECT
					ROW_NUMBER() OVER(ORDER BY ID),
					ID,
					numWarehouseItemID,
					numUnitReceieved,
					ISNULL(numDeletedReceievedQty,0)
				FROM
					OpportunityItemsReceievedLocation 
				WHERE
					numDomainID=@numDomain
					AND numOppID=@numOppId
					AND numOppItemID=@numoppitemtCode

				DECLARE @i AS INT = 1
				DECLARE @COUNT AS INT
				DECLARE @numTempOIRLID NUMERIC(18,0)
				DECLARE @numTempOnHand FLOAT
				DECLARE @numTempWarehouseItemID NUMERIC(18,0)
				DECLARE @numTempUnitReceieved FLOAT
				DECLARE @numTempDeletedReceievedQty FLOAT

				SELECT @COUNT=COUNT(*) FROM @TEMPReceievedItems

				WHILE @i <= @COUNT
				BEGIN
					SELECT 
						@numTempOIRLID=TRI.numOIRLID,
						@numTempOnHand= ISNULL(numOnHand,0),
						@numTempWarehouseItemID=ISNULL(TRI.numWarehouseItemID,0),
						@numTempUnitReceieved=ISNULL(numUnitReceieved,0),
						@numTempDeletedReceievedQty=ISNULL(numDeletedReceievedQty,0)
					FROM 
						@TEMPReceievedItems TRI
					INNER JOIN
						WareHouseItems WI
					ON
						TRI.numWarehouseItemID=WI.numWareHouseItemID
					WHERE 
						ID=@i

					IF @numTempOnHand >= (@numTempUnitReceieved - @numTempDeletedReceievedQty)
					BEGIN
						UPDATE
							WareHouseItems
						SET
							numOnHand = ISNULL(numOnHand,0) - (@numTempUnitReceieved - @numTempDeletedReceievedQty),
							dtModified = GETDATE()
						WHERE
							numWareHouseItemID=@numTempWarehouseItemID

						UPDATE
							OpportunityItems
						SET
							numDeletedReceievedQty = ISNULL(numDeletedReceievedQty,0) + (@numTempUnitReceieved - @numTempDeletedReceievedQty)
						WHERE
							numoppitemtCode=@numoppitemtCode					
					END
					ELSE
					BEGIN
						RAISERROR('INSUFFICIENT_ONHAND_QTY',16,1)
						RETURN
					END

					IF @tintMode = 2
					BEGIN
						SET @description=CONCAT('PO DEMOTED TO OPPORTUNITY (Total Qty:',@numUnits,' Deleted Qty: ',(@numTempUnitReceieved - @numTempDeletedReceievedQty),' Received:',@numTempUnitReceieved,')')
					END
					ELSE
					BEGIN
						SET @description=CONCAT('PO Deleted (Total Qty:',@numUnits,' Deleted Qty: ',(@numTempUnitReceieved - @numTempDeletedReceievedQty),' Received:',@numTempUnitReceieved,')')
					END

					EXEC dbo.USP_ManageWareHouseItems_Tracking
						@numWareHouseItemID = @numTempWarehouseItemID,
						@numReferenceID = @numOppId,
						@tintRefType = 4,
						@vcDescription = @description,
						@numModifiedBy = @numUserCntID,
						@numDomainID = @numDomain

					DELETE FROM OpportunityItemsReceievedLocation WHERE ID=@numTempOIRLID

					SET @i = @i + 1
				END
			END
		
			--WE ARE FETCHING VALUE AGAIN BECAUSE IF ITEMS ARE RECEIVED TO DIFFERENT LOCATION THAN IT'S CHANGE VALUE FROM CODE ABOVE
			SELECT @numDeletedReceievedQty=ISNULL(numDeletedReceievedQty,0) FROM OpportunityItems WHERE	numoppitemtCode=@numoppitemtCode

			IF @tintMode = 2
			BEGIN
				SET @description=CONCAT('PO DEMOTED TO OPPORTUNITY (Qty:',@numUnits,' Received:',@numUnitHourReceived,' Deleted:',@numDeletedReceievedQty,')')
			END
			ELSE
			BEGIN
				SET @description=CONCAT('PO Deleted (Qty:',@numUnits,' Received:',@numUnitHourReceived,' Deleted:',@numDeletedReceievedQty,')')
			END

			--Partial Fulfillment
			IF (@tintmode=1 OR @tintMode=2) and  @onHand >= (@numUnitHourReceived - @numDeletedReceievedQty)
			BEGIN
				SET @onHand= @onHand - (@numUnitHourReceived - @numDeletedReceievedQty)
			END
						
			SET @numUnits = @numUnits - @numUnitHourReceived

			IF (@onOrder - @numUnits)>=0
			BEGIN
				--Causing Negative Inventory Bug ID:494
				SET @onOrder = @onOrder - @numUnits	
			END
			ELSE IF (@onHand + @onOrder) - @numUnits >= 0
			BEGIN						
				SET @onHand = @onHand - (@numUnits-@onOrder)
				SET @onOrder = 0
			END
			ELSE IF  (@onHand + @onOrder + @onAllocation) - @numUnits >= 0
			BEGIN
				Declare @numDiff numeric
	
				SET @numDiff = @numUnits - @onOrder
				SET @onOrder = 0

				SET @numDiff = @numDiff - @onHand
				SET @onHand = 0

				SET @onAllocation = @onAllocation - @numDiff
				SET @onBackOrder = @onBackOrder + @numDiff
			END
					    
			UPDATE
				WareHouseItems
			SET 
				numOnHand = @onHand,
				numAllocation = @onAllocation,
				numBackOrder = @onBackOrder,
				numOnOrder = @onOrder,
				dtModified = GETDATE()
			WHERE 
				numWareHouseItemID = @numWareHouseItemID 
			
			IF (@tintmode = 1 OR @tintMode=2)
			BEGIN
				UPDATE
					OpportunityItems
				SET
					numUnitHourReceived = 0
					,numDeletedReceievedQty = 0
				WHERE
					numoppitemtCode=@numoppitemtCode
			END
				       
			EXEC dbo.USP_ManageWareHouseItems_Tracking
			@numWareHouseItemID = @numWareHouseItemID, --  numeric(9, 0)
			@numReferenceID = @numOppId, --  numeric(9, 0)
			@tintRefType = 4, --  tinyint
			@vcDescription = @description, --  varchar(100)
			@numModifiedBy = @numUserCntID,
			@numDomainID = @numDomain                                      
		END   
                                                                
        SELECT TOP 1
                @numoppitemtCode = numoppitemtCode,
                @itemcode = OI.numItemCode,
                @numUnits = numUnitHour,
                @numWareHouseItemID = ISNULL(numWarehouseItmsID,0),
				@numWLocationID = ISNULL(numWLocationID,0),
                @Kit = ( CASE WHEN bitKitParent = 1
                                    AND bitAssembly = 1 THEN 0
                                WHEN bitKitParent = 1 THEN 1
                                ELSE 0
                            END ),
                @monAmount = ISNULL(monTotAmount,0)  * (CASE WHEN ISNULL(fltExchangeRate,0)=0 THEN 1 ELSE fltExchangeRate END),
                @monAvgCost = monAverageCost,
                @numQtyShipped = ISNULL(numQtyShipped,0),
				@numUnitHourReceived = ISNULL(numUnitHourReceived,0),
				@numDeletedReceievedQty = ISNULL(numDeletedReceievedQty,0),
				@bitKitParent=ISNULL(bitKitParent,0),
				@numToWarehouseItemID =OI.numToWarehouseItemID,
				@bitStockTransfer = ISNULL(OM.bitStockTransfer,0),
				@OppType = tintOppType,
				@numRentalIN=ISNULL(oi.numRentalIN,0),
				@numRentalOut=Isnull(oi.numRentalOut,0),
				@numRentalLost=Isnull(oi.numRentalLost,0),
				@bitAsset =ISNULL(I.bitAsset,0),
				@bitWorkOrder = ISNULL(OI.bitWorkOrder,0)
        FROM    OpportunityItems OI
				LEFT JOIN WareHouseItems WI ON OI.numWarehouseItmsID=WI.numWareHouseItemID
				JOIN dbo.OpportunityMaster OM ON OI.numOppId = OM.numOppId
                JOIN Item I ON OI.numItemCode = I.numItemCode
                                   
        WHERE 
			(ISNULL(@numOppItemId,0) = 0 OR OI.numoppitemtCode=@numOppItemId) 
			AND (charitemtype='P' OR 1=(CASE WHEN tintOppType=1 THEN 
						CASE WHEN ISNULL(I.bitKitParent,0) = 1 AND ISNULL(I.bitAssembly,0) = 1 THEN 0
								WHEN ISNULL(I.bitKitParent,0) = 1 THEN 1
								ELSE 0 END 
						ELSE 0 END))  
						AND OI.numOppId = @numOppId 
                AND OI.numoppitemtCode > @numoppitemtCode
                AND ( bitDropShip = 0
                        OR bitDropShip IS NULL
                    )
        ORDER BY OI.numoppitemtCode                                              
        
		IF @@rowcount = 0 
            SET @numoppitemtCode = 0      
	END
END
GO