/****** Object:  StoredProcedure [dbo].[usp_DeleteOpportunityStageDetails]    Script Date: 07/26/2008 16:15:37 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_deleteopportunitystagedetails')
DROP PROCEDURE usp_deleteopportunitystagedetails
GO
CREATE PROCEDURE [dbo].[usp_DeleteOpportunityStageDetails]
@numOppId numeric(9)
AS
BEGIN

			DELETE FROM opportunitySubstagedetails WHERE numoppid=@numOppid
			DELETE FROM OpptStageComments WHERE numoppid=@numOppid
                                        delete from stageOpportunity where numOppID=@numOppID
			DELETE FROM opportunitystagedetails WHERE numoppid=@numOppid
END
GO
