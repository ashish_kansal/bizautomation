SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_DeleteSalesReturnDetails' ) 
    DROP PROCEDURE USP_DeleteSalesReturnDetails
GO

CREATE PROCEDURE [dbo].[USP_DeleteSalesReturnDetails]
    (
      @numReturnHeaderID NUMERIC(9),
      @numReturnStatus	NUMERIC(18,0),
      @numDomainId NUMERIC(9),
      @numUserCntID NUMERIC(9)
    )
AS 
BEGIN
BEGIN TRY
BEGIN TRANSACTION

    DECLARE @tintReturnType TINYINT,@tintReceiveType TINYINT
    
    SELECT @tintReturnType=tintReturnType,@tintReceiveType=tintReceiveType FROM ReturnHeader WHERE numReturnHeaderID = @numReturnHeaderID    
   
    IF (@tintReturnType=1 AND @tintReceiveType=2) OR @tintReturnType=3
    BEGIN
		IF EXISTS(SELECT numDepositID FROM DepositMaster WHERE tintDepositePage=3 AND numReturnHeaderID=@numReturnHeaderID AND ISNULL(monAppliedAmount,0)>0)
		BEGIN
			RAISERROR ( 'CreditMemo_PAID', 16, 1 ) ;
			RETURN ;
		END	
    END
     
    IF (@tintReturnType=2 AND @tintReceiveType=2) OR @tintReturnType=3
    BEGIN
		IF EXISTS(SELECT numReturnHeaderID FROM BillPaymentHeader WHERE numReturnHeaderID=@numReturnHeaderID AND ISNULL(monAppliedAmount,0)>0)
		BEGIN
			RAISERROR ( 'CreditMemo_PAID', 16, 1 ) ;
			RETURN ;
		END	
    END
 
    IF (@tintReturnType=1 or @tintReturnType=2) 
    BEGIN
		DECLARE @i INT = 1
		DECLARE @COUNT AS INT 
		DECLARE @numTempOppID AS NUMERIC(18,0)
		DECLARE @numTempOppItemID AS NUMERIC(18,0)
		DECLARE @bitTempLotNo AS BIT
		DECLARE @numTempQty AS INT 
		DECLARE @numTempWarehouseItemID AS INT
		DECLARE @numTempWareHouseItmsDTLID AS INT
			
		DECLARE @TempOppSerial TABLE
		(
			ID INT IDENTITY(1,1),
			numOppId NUMERIC(18,0),
			numOppItemID NUMERIC(18,0),
			numWarehouseItemID NUMERIC(18,0),
			numWarehouseItmsDTLID NUMERIC(18,0),
			numQty INT,
			bitLotNo NUMERIC(18,0)
		)

		INSERT INTO @TempOppSerial
		(
			numOppId,
			numOppItemID,
			numWarehouseItemID,
			numWarehouseItmsDTLID,
			numQty,
			bitLotNo
		)
		SELECT	
			RH.numOppId,
			RI.numOppItemID,
			OWSIReturn.numWarehouseItmsID,
			OWSIReturn.numWarehouseItmsDTLID,
			OWSIReturn.numQty,
			I.bitLotNo
		FROM
			ReturnHeader RH
		INNER JOIN
			ReturnItems RI
		ON
			RH.numReturnHeaderID = RI.numReturnHeaderID
		INNER JOIN
			OppWarehouseSerializedItem OWSIReturn
		ON
			RH.numReturnHeaderID = OWSIReturn.numReturnHeaderID
			AND RI.numReturnItemID = OWSIReturn.numReturnItemID
		INNER JOIN
			OpportunityItems OI
		ON
			RI.numOppItemID = OI.numoppitemtCode
		INNER JOIN
			Item I
		ON
			OI.numItemCode = I.numItemCode
		WHERE
			RH.numReturnHeaderID = @numReturnHeaderID

		IF @tintReturnType = 1 --SALES RETURN
		BEGIN
			--CHECK IF SERIAL/LOT# ARE NOT USED IN SALES ORDER
			IF (SELECT
						COUNT(*)
				FROM
					WareHouseItmsDTL WHIDL
				INNER JOIN
					OppWarehouseSerializedItem OWSI
				ON
					WHIDL.numWareHouseItmsDTLID = OWSI.numWarehouseItmsDTLID
					AND WHIDL.numWareHouseItemID = OWSI.numWarehouseItmsID
				INNER JOIN
					WareHouseItems
				ON
					WHIDL.numWareHouseItemID = WareHouseItems.numWareHouseItemID
				INNER JOIN
					Item
				ON
					WareHouseItems.numItemID = Item.numItemCode
				WHERE
					numReturnHeaderID=@numReturnHeaderID
					AND 1 = (CASE 
								WHEN Item.bitLotNo = 1 AND ISNULL(WHIDL.numQty,0) < ISNULL(OWSI.numQty,0) THEN 1 
								WHEN Item.bitSerialized = 1 AND ISNULL(WHIDL.numQty,0) = 0 THEN 1 
								ELSE 0 
							END)
				) > 0
			BEGIN
				RAISERROR ('Serial_LotNo_Used', 16, 1 ) ;
			END
			ELSE
			BEGIN
				-- MAKE SERIAL QTY 0 OR DECREASE LOT QUANTITY BECAUSE IT IS USED IN ORDER 
				UPDATE WHIDL
					SET WHIDL.numQty = (CASE WHEN Item.bitLotNo = 1 THEN ISNULL(WHIDL.numQty,0) - ISNULL(OWSI.numQty,0) ELSE 0 END)
				FROM 
					WareHouseItmsDTL WHIDL
				INNER JOIN
					OppWarehouseSerializedItem OWSI
				ON
					WHIDL.numWareHouseItmsDTLID = OWSI.numWarehouseItmsDTLID
					AND WHIDL.numWareHouseItemID = OWSI.numWarehouseItmsID
				INNER JOIN
					WareHouseItems
				ON
					WHIDL.numWareHouseItemID = WareHouseItems.numWareHouseItemID
				INNER JOIN
					Item
				ON
					WareHouseItems.numItemID = Item.numItemCode
				WHERE
					OWSI.numReturnHeaderID=@numReturnHeaderID
			END
		END
		ELSE IF @tintReturnType = 2 --PURCHASE RETURN
		BEGIN
			-- MAKE SERIAL QTY 1 OR INCREASE LOT QUANTITY BECAUSE IT IS RETURNED TO WAREHOUSE
			UPDATE WHIDL
				SET WHIDL.numQty = (CASE WHEN Item.bitLotNo = 1 THEN ISNULL(WHIDL.numQty,0) + ISNULL(OWSI.numQty,0) ELSE 1 END)
			FROM 
				WareHouseItmsDTL WHIDL
			INNER JOIN
				OppWarehouseSerializedItem OWSI
			ON
				WHIDL.numWareHouseItmsDTLID = OWSI.numWarehouseItmsDTLID
				AND WHIDL.numWareHouseItemID = OWSI.numWarehouseItmsID
			INNER JOIN
				WareHouseItems
			ON
				WHIDL.numWareHouseItemID = WareHouseItems.numWareHouseItemID
			INNER JOIN
				Item
			ON
				WareHouseItems.numItemID = Item.numItemCode
			WHERE
				OWSI.numReturnHeaderID=@numReturnHeaderID
		END

		--RE INSERT ENTERIES SERIAL/LOT# ENTERIES WITH ORDER IN OppWarehouseSerializedItem TABLE WHICH ARE ASSOCIATED BECAUSE NOW SERIALS ARE RETURNED TO/FROM WAREHOUSE
		--SELECT @COUNT = COUNT(*) FROM @TempOppSerial

		--WHILE @i <= @COUNT
		--BEGIN
		--	SELECT
		--		@bitTempLotNo = bitLotNo,
		--		@numTempQty = numQty,
		--		@numTempOppID = numOppId,
		--		@numTempOppItemID = numOppItemID,
		--		@numTempWareHouseItmsDTLID = numWareHouseItmsDTLID,
		--		@numTempWarehouseItemID = numWarehouseItemID
		--	FROM
		--		@TempOppSerial
		--	WHERE
		--		ID = @i


		--	IF EXISTS (SELECT * FROM OppWarehouseSerializedItem WHERE numOppID=@numTempOppID AND numOppItemID=@numTempOppItemID AND numWarehouseItmsID=@numTempWarehouseItemID AND numWarehouseItmsDTLID=@numTempWareHouseItmsDTLID)
		--	BEGIN
		--		UPDATE 
		--			OppWarehouseSerializedItem
		--		SET 
		--			numQty = ISNULL(numQty,0) + ISNULL(@numTempQty,0)
		--		WHERE
		--			numOppID=@numTempOppID 
		--			AND numOppItemID=@numTempOppItemID 
		--			AND numWarehouseItmsID=@numTempWarehouseItemID
		--			AND numWarehouseItmsDTLID=@numTempWareHouseItmsDTLID
		--	END
		--	ELSE
		--	BEGIN
		--		INSERT INTO OppWarehouseSerializedItem
		--		(
		--			numWarehouseItmsDTLID,
		--			numOppID,
		--			numOppItemID,
		--			numWarehouseItmsID,
		--			numQty
		--		)
		--		VALUES
		--		(
		--			@numTempWareHouseItmsDTLID,
		--			@numTempOppID,
		--			@numTempOppItemID,
		--			@numTempWarehouseItemID,
		--			@numTempQty
		--		)
		--	END

		--	SET @i = @i + 1
		--END
    END
	
	IF @tintReturnType=1 OR @tintReturnType=2 
    BEGIN
		EXEC usp_ManageRMAInventory @numReturnHeaderID,@numDomainId,@numUserCntID,2
    END
         	              
    UPDATE  
		dbo.ReturnHeader
    SET     
		numReturnStatus = @numReturnStatus,
        numBizdocTempID = CASE WHEN @tintReturnType=1 OR @tintReturnType=2 THEN NULL ELSE numBizdocTempID END,
        vcBizDocName =CASE WHEN @tintReturnType=1 OR @tintReturnType=2 THEN '' ELSE vcBizDocName END ,
        IsCreateRefundReceipt = 0,
        tintReceiveType=0,monBizDocAmount=0, numItemCode = 0
    WHERE   
		numReturnHeaderID = @numReturnHeaderID
        AND	numDomainId = @numDomainId
    
     IF (@tintReturnType=1 AND @tintReceiveType=2) OR @tintReturnType=3
     BEGIN
		DELETE FROM DepositMaster WHERE tintDepositePage=3 AND numReturnHeaderID=@numReturnHeaderID
	 END	
     
     IF (@tintReturnType=2 AND @tintReceiveType=2) OR @tintReturnType=3
     BEGIN
		DELETE FROM BillPaymentHeader WHERE numReturnHeaderID=@numReturnHeaderID
     END
    
	PRINT @tintReceiveType 
	PRINT @tintReturnType

	 IF @tintReceiveType = 1 AND @tintReturnType = 4
	 BEGIN
		DECLARE @monAppliedAmount AS DECIMAL(20,5)
		DECLARE @numDepositIDRef AS NUMERIC(18,0)
		DECLARE @numParentID NUMERIC(18,0)

		SELECT @numDepositIDRef = ISNULL(numDepositIDRef,0),@numParentID=ISNULL(numParentID,0) FROM [dbo].[ReturnHeader] AS RH WHERE [RH].[numReturnHeaderID] = @numReturnHeaderID

		SELECT 
			@monAppliedAmount = ISNULL([RH].[monAmount],0)
		FROM 
			[dbo].[ReturnHeader] AS RH 
		WHERE 
			[RH].[numReturnHeaderID] = @numReturnHeaderID 
			AND [RH].[numDomainId] = @numDomainId

		IF @numDepositIDRef > 0			
		BEGIN
			UPDATE 
				[dbo].[DepositMaster] 
			SET 
				[monAppliedAmount] = ISNULL([monAppliedAmount],0) - @monAppliedAmount
				,[monRefundAmount] = ISNULL(monRefundAmount,0) + ISNULL([monDepositAmount],0)
				,[numReturnHeaderID] = (CASE WHEN ISNULL(@numParentID,0) > 0 THEN @numParentID ELSE NULL END)
			WHERE 
				[numDepositId] = @numDepositIDRef 
				AND [numDomainId] = @numDomainId
		END
	END
	
	DELETE FROM [dbo].[CheckHeader] WHERE [CheckHeader].[numReferenceID] = @numReturnHeaderID
    
	DELETE FROM 
		dbo.General_Journal_Details 
	WHERE 
		numJournalId IN (SELECT numJournal_Id FROM General_Journal_Header WHERE numReturnID=@numReturnHeaderID AND [General_Journal_Header].[numDomainId] = @numDomainId)
		AND [General_Journal_Details].[numDomainId] = @numDomainId
    
	DELETE FROM dbo.General_Journal_Header WHERE numReturnID=@numReturnHeaderID AND [numDomainId] = @numDomainId
COMMIT
END TRY
BEGIN CATCH
	DECLARE @ErrorMessage NVARCHAR(4000)
	DECLARE @ErrorNumber INT
	DECLARE @ErrorSeverity INT
	DECLARE @ErrorState INT
	DECLARE @ErrorLine INT
	DECLARE @ErrorProcedure NVARCHAR(200)

	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;

	SELECT 
		@ErrorMessage = ERROR_MESSAGE(),
		@ErrorNumber = ERROR_NUMBER(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE(),
		@ErrorLine = ERROR_LINE(),
		@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

	RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);
END CATCH
END