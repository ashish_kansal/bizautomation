/****** Object:  StoredProcedure [dbo].[usp_SetUsersWithDomains]    Script Date: 07/26/2008 16:21:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj                      
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_setuserswithdomains')
DROP PROCEDURE usp_setuserswithdomains
GO
CREATE  PROCEDURE [dbo].[usp_SetUsersWithDomains]
	@numUserID NUMERIC(9),                                    
	@vcUserName VARCHAR(50),                                    
	@vcUserDesc VARCHAR(250),                              
	@numGroupId as numeric(9),                                           
	@numUserDetailID numeric ,                              
	@numUserCntID as numeric(9),                              
	@strTerritory as varchar(4000) ,                              
	@numDomainID as numeric(9),
	@strTeam as varchar(4000),
	@vcEmail as varchar(100),
	@vcPassword as varchar(100),          
	@Active as BIT,
	@numDefaultClass NUMERIC,
	@numDefaultWarehouse as numeric(18),
	@vcLinkedinId varchar(300)=null,
	@intAssociate int=0,
	@ProfilePic varchar(100)=null
	,@bitPayroll BIT = 0
	,@monHourlyRate DECIMAL(20,5) = 0
	,@tintPayrollType TINYINT = 0
	,@tintHourType TINYINT = 0
	,@monOverTimeRate DECIMAL(20,5) = 0
	,@bitOauthImap BIT = 0
	,@tintMailProvider TINYINT = 3
AS
BEGIN
	DECLARE @numNewFullUsers AS INT
	DECLARE @numNewLimitedAccessUsers AS INT

	SET @numNewFullUsers = ISNULL((SELECT
										COUNT(*)
									FROM
										UserMaster 
									INNER JOIN
										AuthenticationGroupMaster
									ON
										UserMaster.numGroupID = AuthenticationGroupMaster.numGroupID
									WHERE
										UserMaster.numDomainID = @numDomainID
										AND AuthenticationGroupMaster.tintGroupType=1
										AND ISNULL(UserMaster.bitActivateFlag,0)=1
										AND UserMaster.numUserId <> ISNULL(@numUserID,0)),0)

	SET @numNewLimitedAccessUsers = ISNULL((SELECT
										COUNT(*)
									FROM
										UserMaster 
									INNER JOIN
										AuthenticationGroupMaster
									ON
										UserMaster.numGroupID = AuthenticationGroupMaster.numGroupID
									WHERE
										UserMaster.numDomainID = @numDomainID
										AND AuthenticationGroupMaster.tintGroupType=4
										AND ISNULL(UserMaster.bitActivateFlag,0)=1
										AND UserMaster.numUserId <> ISNULL(@numUserID,0)),0)
		
	IF ISNULL(@Active,0)=1 AND ISNULL((SELECT tintGroupType FROM AuthenticationGroupMaster WHERE numGroupID=@numGroupId),0)=1 AND EXISTS (SELECT numSubscriberID FROM Subscribers WHERE numTargetDomainID=@numDomainID AND (@numNewFullUsers + 1) > ISNULL(intNoofUsersSubscribed,0))
	BEGIN
		RAISERROR('FULL_USERS_EXCEED',16,1)
		RETURN
	END
	ELSE IF ISNULL(@Active,0)=1 AND ISNULL((SELECT tintGroupType FROM AuthenticationGroupMaster WHERE numGroupID=@numGroupId),0)=4 AND EXISTS (SELECT numSubscriberID FROM Subscribers WHERE numTargetDomainID=@numDomainID AND (@numNewLimitedAccessUsers + 1) > ISNULL(intNoofLimitedAccessUsers,0))
	BEGIN
		RAISERROR('LIMITED_ACCESS_USERS_EXCEED',16,1)
		RETURN
	END

	IF @numUserID=0             
	BEGIN 
		DECLARE @APIPublicKey varchar(20)
		SELECT @APIPublicKey = dbo.GenerateBizAPIPublicKey() 
           
		INSERT INTO UserMaster
		(
			vcUserName
			,vcUserDesc
			,numGroupId
			,numUserDetailId
			,numModifiedBy
			,bintModifiedDate
			,vcEmailID
			,vcPassword
			,numDomainID
			,numCreatedBy
			,bintCreatedDate
			,bitActivateFlag
			,numDefaultClass
			,numDefaultWarehouse
			,vcBizAPIPublicKey
			,vcBizAPISecretKey
			,bitBizAPIAccessEnabled
			,vcLinkedinId
			,intAssociate
			,bitPayroll
			,monHourlyRate
			,tintPayrollType
			,tintHourType
			,monOverTimeRate
			,bitOauthImap
			,tintMailProvider
		)
		VALUES
		(
			@vcUserName
			,@vcUserDesc
			,@numGroupId
			,@numUserDetailID
			,@numUserCntID
			,GETUTCDATE()
			,@vcEmail
			,@vcPassword
			,@numDomainID
			,@numUserCntID
			,GETUTCDATE()
			,@Active
			,@numDefaultClass
			,@numDefaultWarehouse
			,@APIPublicKey
			,NEWID()
			,0
			,@vcLinkedinId
			,@intAssociate
			,@bitPayroll
			,@monHourlyRate
			,@tintPayrollType
			,@tintHourType
			,(CASE WHEN ISNULL(@monOverTimeRate,0) = 0 THEN @monHourlyRate ELSE @monOverTimeRate END)
			,@bitOauthImap
			,ISNULL(@tintMailProvider,3)
		)            
		
		SET @numUserID=@@identity          
           
		INSERT INTO BizAPIThrottlePolicy 
		(
			[RateLimitKey],
			[bitIsIPAddress],
			[numPerSecond],
			[numPerMinute],
			[numPerHour],
			[numPerDay],
			[numPerWeek]
		)
		VALUES
		(
			@APIPublicKey,
			0,
			3,
			60,
			1200,
			28800,
			200000
		)
    
		EXECUTE [dbo].[Resource_Add]   @vcUserName  ,@vcUserDesc  ,@vcEmail  ,1  ,@numDomainID  ,@numUserDetailID  
	END
	ELSE            
	BEGIN
		UPDATE 
			UserMaster 
		SET 
			vcUserName = @vcUserName,                                    
			vcUserDesc = @vcUserDesc,                              
			numGroupId =@numGroupId,                                    
			numUserDetailId = @numUserDetailID ,                              
			numModifiedBy= @numUserCntID ,                              
			bintModifiedDate= getutcdate(),                          
			vcEmailID=@vcEmail,            
			vcPassword=@vcPassword,          
			bitActivateFlag=@Active,
			numDefaultClass=@numDefaultClass,
			numDefaultWarehouse=@numDefaultWarehouse,
			vcLinkedinId=@vcLinkedinId,
			intAssociate=@intAssociate
			,bitPayroll=@bitPayroll
			,monHourlyRate=@monHourlyRate
			,tintPayrollType=@tintPayrollType
			,tintHourType=@tintHourType
			,monOverTimeRate=(CASE WHEN ISNULL(@monOverTimeRate,0) = 0 THEN @monHourlyRate ELSE @monOverTimeRate END)
			,bitOauthImap=@bitOauthImap
			,tintMailProvider = ISNULL(@tintMailProvider,3)
		WHERE 
			numUserID = @numUserID          
          
  
		IF NOT EXISTS (SELECT * FROM RESOURCE WHERE numUserCntId = @numUserDetailID and numdomainId = @numDomainId)  
		BEGIN   
			EXECUTE [dbo].[Resource_Add]   @vcUserName  ,@vcUserDesc  ,@vcEmail  ,1  ,@numDomainID  ,@numUserDetailID     
		END      
	END
                       
	DECLARE @separator_position AS INTEGER
	DECLARE @strPosition AS VARCHAR(1000)                 
                                   
	DELETE FROM UserTerritory WHERE numUserCntID = @numUserDetailID and numDomainId=@numDomainID                              
          
	WHILE PATINDEX('%,%' , @strTerritory) <> 0                              
    BEGIN -- Begin for While Loop                              
		SELECT @separator_position = PATINDEX('%,%' , @strTerritory)                        
		SELECT @strPosition = LEFT(@strTerritory, @separator_position - 1)      
		SELECT @strTerritory = STUFF(@strTerritory, 1, @separator_position,'')                              
     
		INSERT INTO UserTerritory 
		(
			numUserCntID
			,numTerritoryID
			,numDomainID
		)                              
		VALUES
		(
			@numUserDetailID
			,@strPosition
			,@numDomainID
		)                        
	END                              
                                           
	DELETE FROM UserTeams WHERE numUserCntID = @numUserDetailID AND numDomainId=@numDomainID                              
                               
	WHILE PATINDEX('%,%' , @strTeam) <> 0                              
    BEGIN -- Begin for While Loop                              
		SELECT @separator_position =  PATINDEX('%,%' , @strTeam)                              
		SELECT @strPosition = left(@strTeam, @separator_position - 1)                  
		SELECT @strTeam = stuff(@strTeam, 1, @separator_position,'')                              
     
		INSERT INTO UserTeams 
		(
			numUserCntID
			,numTeam
			,numDomainID
		)                              
		VALUES 
		(
			@numUserDetailID
			,@strPosition
			,@numDomainID
		)                        
	END              
                
                                      
                              
	DELETE FROM 
		ForReportsByTeam 
	WHERE 
		numUserCntID = @numUserDetailID                              
		AND numDomainId=@numDomainID AND numTeam NOT IN (SELECT numTeam FROM UserTeams WHERE numUserCntID = @numUserCntID AND numDomainId=@numDomainID)                                                
END
GO
