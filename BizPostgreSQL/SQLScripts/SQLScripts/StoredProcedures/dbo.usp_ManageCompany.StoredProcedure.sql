/****** Object:  StoredProcedure [dbo].[usp_ManageCompany]    Script Date: 07/26/2008 16:19:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--modified by chintan
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_managecompany')
DROP PROCEDURE usp_managecompany
GO
CREATE PROCEDURE [dbo].[usp_ManageCompany]                                
 @numCompanyId numeric=0 ,                
 @vcCompanyName varchar (100)='',                
 @numCompanyType numeric=0,                
 @numCompanyRating numeric=0,                      
 @numCompanyIndustry numeric=0,                
 @numCompanyCredit numeric=0,                      
 @txtComments text='',                
 @vcWebSite varchar (255)='',                
 @vcWebLabel1 varchar (100)='',                
 @vcWebLink1 varchar (255)='',                
 @vcWebLabel2 varchar (100)='',                
 @vcWebLink2 varchar (255)='',                
 @vcWebLabel3 varchar (100)='',                
 @vcWebLink3 varchar (255)='',                
 @vcWebLabel4 varchar (100)='',                
 @vcWebLink4 varchar (255)='',                
 @numAnnualRevID numeric=0,                
 @numNoOfEmployeesId numeric=0,                
 @vcHow numeric=0,                
 @vcProfile numeric=0,                
 @numUserCntID numeric=0,                                               
 @bitPublicFlag bit = 0,                         
 @numDomainID numeric=0,
 @bitSelectiveUpdate BIT=0
--                
AS                
BEGIN                      
 IF @numCompanyId=0                      
  BEGIN       
	IF @numDomainID=170 AND (CHARINDEX('www.',@vcCompanyName) > 0 OR CHARINDEX('http://',@vcCompanyName) > 0 OR CHARINDEX('https://',@vcCompanyName) > 0)
	BEGIN
		RAISERROR('INVALID',16,1)
		RETURN
	END
                 
    INSERT INTO CompanyInfo      
   (      
   vcCompanyName,      
   numCompanyType,      
   numCompanyRating,       
   numCompanyIndustry,      
   numCompanyCredit,      
   txtComments,      
   vcWebSite,      
   vcWebLabel1,      
   vcWebLink1,      
   vcWebLabel2,      
   vcWebLink2,      
   vcWebLabel3,      
   vcWebLink3,      
   vcWeblabel4,      
   vcWebLink4,      
   numAnnualRevID,      
   numNoOfEmployeesId,      
   vcHow,      
   vcProfile,      
   numCreatedBy,      
   bintCreatedDate,      
   numModifiedBy,      
   bintModifiedDate,      
   bitPublicFlag,      
   numDomainID)       
 values      
   (      
   @vcCompanyName,       
   @numCompanyType,       
   @numCompanyRating,                   
   @numCompanyIndustry,       
   @numCompanyCredit,                  
   @txtComments,       
   @vcWebSite,       
   @vcWebLabel1,       
   @vcWebLink1,       
   @vcWebLabel2,       
   @vcWebLink2,                 
   @vcWebLabel3,       
   @vcWebLink3,       
   @vcWebLabel4,       
   @vcWebLink4,        
   @numAnnualRevID,                 
   @numNoOfEmployeesId,       
   @vcHow,       
   @vcProfile,       
   @numUserCntID,       
   getutcdate(),       
   @numUserCntID,       
   getutcdate(),                
   @bitPublicFlag,       
   @numDomainID      
   )                
              set @numCompanyId =SCOPE_IDENTITY()            
           select @numCompanyId                
  END                
 ELSE if  @numCompanyId>0                   
  BEGIN                

	IF @bitSelectiveUpdate = 0 
	BEGIN
		UPDATE CompanyInfo SET                
		vcCompanyName = @vcCompanyName,                
		numCompanyType = @numCompanyType,                
		numCompanyRating = @numCompanyRating,                     
		numCompanyIndustry = @numCompanyIndustry,                
		numCompanyCredit = @numCompanyCredit,               
		txtComments = @txtComments,                
		vcWebSite = @vcWebSite,                
		vcWebLabel1 = @vcWebLabel1,                 
		vcWebLabel2 = @vcWebLabel2,                 
		vcWebLabel3 = @vcWebLabel3,                
		vcWebLabel4 = @vcWebLabel4,                
		vcWebLink1 = @vcWebLink1,                
		vcWebLink2 = @vcWebLink2,                
		vcWebLink3 = @vcWebLink3,                
		vcWebLink4 = @vcWebLink4,                         
		numAnnualRevID = @numAnnualRevID,                
		numNoOfEmployeesId = @numNoOfEmployeesId,                
		vcHow = @vcHow,                
		vcProfile = @vcProfile,                
		numModifiedBy = @numUserCntID,                
		bintModifiedDate = getutcdate(),                
		bitPublicFlag = @bitPublicFlag                
	   WHERE                 
		numCompanyId=@numCompanyId    and numDomainID=@numDomainID       		
		
		SELECT @numcompanyId
	END	
	ELSE IF @bitSelectiveUpdate = 1
	BEGIN
		DECLARE @sql VARCHAR(1000)
		SET @sql = 'update CompanyInfo Set '

		IF(LEN(@vcCompanyName)>0)
		BEGIN
			SET @sql =@sql + ' vcCompanyName=''' + @vcCompanyName + ''', '
		END
		IF(LEN(@vcWebSite)>0)
		BEGIN
			SET @sql =@sql + ' vcWebSite=''' + @vcWebSite + ''', '
		END
		
		IF(@numUserCntID>0)
		BEGIN
			SET @sql =@sql + ' numModifiedBy='+ CONVERT(VARCHAR(10),@numUserCntID) +', '
		END
	
		SET @sql=SUBSTRING(@sql,0,LEN(@sql)) + ' '
		SET @sql =@sql + 'where numCompanyId= '+CONVERT(VARCHAR(10),@numCompanyId)

		PRINT @sql
		EXEC(@sql) 
	END
    
  END              
            
         
END
GO
