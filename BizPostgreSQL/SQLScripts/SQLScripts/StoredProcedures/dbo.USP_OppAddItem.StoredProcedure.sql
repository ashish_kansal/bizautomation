/****** Object:  StoredProcedure [dbo].[USP_OppAddItem]    Script Date: 07/26/2008 16:20:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_oppadditem')
DROP PROCEDURE usp_oppadditem
GO
CREATE PROCEDURE [dbo].[USP_OppAddItem]
@numItemCode as numeric(9),
@vcType as varchar(50)
as

 insert into OpportunityItems(numItemCode,vcType,numUnitHour,monPrice,monTotAmount,vcModelID,vcManufacturer,vcPathForTImage,monVendorCost,monAvgCost)
 --kishan
 select @numItemCode,@vcType,0,0,0,(SELECT vcModelID FROM item WHERE numItemCode = @numItemCode),(SELECT vcManufacturer FROM item WHERE numItemCode = @numItemCode),(SELECT TOP 1  vcPathForTImage FROM dbo.ItemImages WHERE numItemCode = @numItemCode AND BitDefault = 1 ),(select isnull(VN.monCost,0) from Vendor VN INNER JOIN Item IT ON IT.numItemCode=VN.numItemCode and VN.numVendorID=IT.numVendorID where VN.numItemCode=@numItemCode),
 (SELECT (CASE WHEN ISNULL(I.bitVirtualInventory,0) = 1 THEN 0 ELSE ISNULL(I.monAverageCost,0) END) FROM Item I WHERE I.numItemCode=@numItemCode)

  select SCOPE_IDENTITY()
GO
