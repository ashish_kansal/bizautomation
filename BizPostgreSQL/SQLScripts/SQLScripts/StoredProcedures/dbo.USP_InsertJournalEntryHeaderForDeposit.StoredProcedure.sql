/****** Object:  StoredProcedure [dbo].[USP_InsertJournalEntryHeaderForDeposit]    Script Date: 07/26/2008 16:19:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created by Siva                    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_insertjournalentryheaderfordeposit')
DROP PROCEDURE usp_insertjournalentryheaderfordeposit
GO
CREATE PROCEDURE [dbo].[USP_InsertJournalEntryHeaderForDeposit]                              
@datEntry_Date as datetime,                                
@numAmount as DECIMAL(20,5),                              
@numJournal_Id as numeric(9)=0,                          
@numDepositId as numeric(9)=0,        
@numDomainId as numeric(9)=0,                  
@numRecurringId as numeric(9)=0,              
@numUserCntID as numeric(9)=0        
As                                
Begin 
 if @numRecurringId=0 Set @numRecurringId=null                                
 if @numJournal_Id=0                        
  Begin                        
   Insert into General_Journal_Header(datEntry_Date,numAmount,numDomainId,numDepositId,numRecurringId,numCreatedBy,datCreatedDate)        
   Values(@datEntry_Date,@numAmount,@numDomainId,@numDepositId,@numRecurringId,@numUserCntID,getutcdate())                              
   Set @numJournal_Id = SCOPE_IDENTITY()                              
   Select  @numJournal_Id                          
  End                        
End
GO
