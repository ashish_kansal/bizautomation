/****** Object:  StoredProcedure [dbo].[usp_GetFieldType]    Script Date: 07/26/2008 16:17:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getfieldtype')
DROP PROCEDURE usp_getfieldtype
GO
CREATE PROCEDURE [dbo].[usp_GetFieldType]
	@vcTableName varchar(100)=''   
--
AS
	/*select  syscolumns.NAme ,sysproperties.Value,systypes.name as DataType   from syscolumns , sysobjects  ,sysproperties,systypes where
 syscolumns.id  = sysobjects.id 
and sysproperties.id =  sysobjects.id 
and sysproperties.SmallId = syscolumns.Colid
and systypes.xtype=syscolumns.xtype
and sysobjects.Name  =  @vcTableName */
SELECT  
	syscolumns.NAme,
	systypes.name as DataType   
FROM 
	syscolumns 
INNER JOIN 
	sysobjects 
ON
	syscolumns.id  = sysobjects.id 
INNER JOIN 
	systypes 
ON
	systypes.xtype=syscolumns.xtype
where
	sysobjects.Name  =@vcTableName
	and systypes.xtype in (106,62,60,108,59,52,122,48)
GO
