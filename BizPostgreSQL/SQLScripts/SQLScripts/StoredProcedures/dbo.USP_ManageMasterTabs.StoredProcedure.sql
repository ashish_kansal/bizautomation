/****** Object:  StoredProcedure [dbo].[USP_ManageMasterTabs]    Script Date: 07/28/2009 22:16:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj  
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_managemastertabs')
DROP PROCEDURE usp_managemastertabs
GO
CREATE PROCEDURE [dbo].[USP_ManageMasterTabs]  
@numTabId as numeric(9)=0,  
@numTabName as varchar(50)='',  
@tintTabType as int,  
@vcURL as varchar(1000),  
@numDomainID as numeric(9)=0  
as  
  
if @numTabId=0  
begin  
  
 insert into TabMaster(numTabName,tintTabType,vcURL,numDomainID)  
 values(@numTabName,@tintTabType,@vcURL,@numDomainID)  
end  
else  
begin 
	declare @bitFixed as bit
	set @bitFixed = 0
	select @bitFixed=isnull(bitFixed,0) from TabMaster where numTabid=@numTabId and tintTabType=@tintTabType  
	print @bitFixed
	if @bitFixed = 1
	begin
		if exists (select * from tabDefault where numDomainId=@numDomainId and numTabid=@numTabId and tintTabType=@tintTabType )
		begin
			print 'exists'
			 update tabDefault set numTabName=@numTabName  
			 where numDomainId=@numDomainId and numTabid=@numTabId and tintTabType=@tintTabType  
		end
		else
		begin
			print 'insert'
			 INSERT INTO [TabDefault](numTabId,numTabName,tintTabType,numDomainId)
			 VALUES(@numTabid,@numTabName,@tintTabType,@numDomainId)
		end
	end
	else
	begin
			update tabMaster set numTabName=@numTabName , vcURL=@vcURL
		    where numDomainId=@numDomainId and numTabid=@numTabId and tintTabType=@tintTabType 
	end
  
end
