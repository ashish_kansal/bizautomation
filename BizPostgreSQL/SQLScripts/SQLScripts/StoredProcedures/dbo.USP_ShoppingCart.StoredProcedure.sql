/****** Object:  StoredProcedure [dbo].[USP_ShoppingCart]    Script Date: 05/07/2009 22:09:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj                  
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_shoppingcart')
DROP PROCEDURE usp_shoppingcart
GO
CREATE PROCEDURE [dbo].[USP_ShoppingCart]                            
@numDivID as numeric(18),                            
@numOppID as numeric(18)=null output ,                                              
@vcPOppName as varchar(200)='' output,  
@numContactId as numeric(18),                            
@numDomainId as numeric(18),
@vcSource VARCHAR(50)=NULL,
@numCampainID NUMERIC(9)=0,
@numCurrencyID as numeric(9),
@numBillAddressId numeric(9),
@numShipAddressId numeric(9),
@bitDiscountType BIT,
@fltDiscount FLOAT,
@numProId NUMERIC(9),
@numSiteID NUMERIC(9),
@tintOppStatus INT,
@monShipCost DECIMAL(20,5),
@tintSource AS BIGINT ,
@tintSourceType AS TINYINT ,                          
@numPaymentMethodId NUMERIC(9),
@txtComments varchar(1000),
@numPartner NUMERIC(18,0),
@txtFuturePassword VARCHAR(50) = NULL,
@intUsedShippingCompany NUMERIC(18,0)=0,
@vcPartenerContact VARCHAR(200)='',
@numShipmentMethod NUMERIC(18,0)=0,
@vcOppRefOrderNo VARCHAR(200) = '',
@bitBillingTerms as bit,
@intBillingDays as integer,
@bitInterestType as bit,
@fltInterest as float,
@numOrderPromotionID NUMERIC(18,0),
@numOrderPromotionDiscountID NUMERIC(18,0)
as                            
BEGIN TRY
BEGIN TRANSACTION          
	DECLARE @CRMType AS INTEGER               
	DECLARE @numRecOwner AS INTEGER       
	DECLARE @numPartenerContact NUMERIC(18,0)=0
	DECLARE @bitAllocateInventoryOnPickList AS BIT
	DECLARE @numDiscountItemID NUMERIC(18,0)
	DECLARE @numRelationship NUMERIC(18,0) = 0
	DECLARE @numProfile NUMERIC(18,0) = 0
	DECLARE @tintCommitAllocation TINYINT
	DECLARE @tintDefaultClassType AS INT = 0
	
	SELECT 
		@numDiscountItemID=ISNULL(numDiscountServiceItemID,0)
		,@tintCommitAllocation=ISNULL(tintCommitAllocation,1)
		,@tintDefaultClassType = ISNULL(tintDefaultClassType,0) 
	FROM 
		Domain 
	WHERE 
		numDomainId=@numDomainId

	SET @numPartenerContact=(SELECT TOP 1 numContactId FROM AdditionalContactsInformation WHERE numDomainId=@numDomainID AND numDivisionId=@numPartner AND vcEmail=@vcPartenerContact)
	
	SELECT 
		@CRMType=tintCRMType
		,@numRecOwner=numRecOwner 
		,@bitAllocateInventoryOnPickList=ISNULL(bitAllocateInventoryOnPickList,0)
		,@numRelationship=ISNULL(numCompanyType,0)
		,@numProfile=ISNULL(vcProfile,0) 
	FROM 
		DivisionMaster
	INNER JOIN 
		CompanyInfo 
	ON 
		DivisionMaster.numCompanyID=CompanyInfo.numCompanyId
	WHERE 
		numDivisionID=@numDivID


	--VALIDATE PROMOTION OF COUPON CODE
	DECLARE @TEMP TABLE
	(
		ID INT IDENTITY(1,1),
		numCartID NUMERIC(18,0),
		numPromotionID NUMERIC(18,0)
	)

	INSERT INTO @TEMP
	(
		numCartID,
		numPromotionID
	)
	SELECT
		numCartId,
		PromotionID
	FROM
		CartItems 
	WHERE 
		numUserCntId=@numContactId
		AND ISNULL(PromotionID,0) > 0

	IF (SELECT COUNT(*) FROM @TEMP) > 0
	BEGIN
		-- IF ITEM PROMOTION USED IN ORDER EXIPIRES THAN RAISE ERROR
		IF  (SELECT 
					COUNT(PO.numProId)
				FROM 
					PromotionOffer PO
				LEFT JOIN
					PromotionOffer POOrder
				ON
					PO.numOrderPromotionID = POOrder.numProId
				INNER JOIN
					@TEMP T1
				ON
					PO.numProId = T1.numPromotionID
				WHERE 
					PO.numDomainId=@numDomainID 
					AND ISNULL(PO.IsOrderBasedPromotion,0) = 0
					AND ISNULL(PO.bitEnabled,0) = 1
					AND 1 = (CASE 
								WHEN ISNULL(PO.numOrderPromotionID,0) > 0 
								THEN (CASE WHEN POOrder.bitNeverExpires = 1 THEN 1 ELSE (CASE WHEN GETUTCDATE()>=POOrder.dtValidFrom AND GETUTCDATE()<=POOrder.dtValidTo THEN 1 ELSE 0 END) END)
								ELSE (CASE WHEN PO.bitNeverExpires = 1 THEN 1 ELSE (CASE WHEN GETUTCDATE()>=PO.dtValidFrom AND GETUTCDATE()<=PO.dtValidTo THEN 1 ELSE 0 END) END)
							END)
					AND 1 = (CASE 
								WHEN ISNULL(PO.numOrderPromotionID,0) > 0 
								THEN
									(CASE WHEN (SELECT COUNT(*) FROM PromotionOfferOrganizations WHERE numProId=PO.numOrderPromotionID AND tintType=2 AND numRelationship=@numRelationship AND numProfile=@numProfile) > 0 THEN 1 ELSE 0 END)
								ELSE
									(CASE PO.tintCustomersBasedOn 
										WHEN 1 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferOrganizations WHERE numProId=PO.numProId AND tintType=1 AND numDivisionID=@numDivID) > 0 THEN 1 ELSE 0 END)
										WHEN 2 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferOrganizations WHERE numProId=PO.numProId AND tintType=2 AND numRelationship=@numRelationship AND numProfile=@numProfile) > 0 THEN 1 ELSE 0 END)
										WHEN 3 THEN 1
										ELSE 0
									END)
							END)
			) <> (SELECT COUNT(*) FROM @TEMP)
		BEGIN
			RAISERROR('ITEM_PROMOTION_EXPIRED',16,1)
			RETURN
		END

		-- NOW IF COUPON BASED ITEM PROMOTION IS USED THEN VALIDATE COUPON AND ITS USAGE
		IF 
		(
			SELECT 
				COUNT(*)
			FROM
				PromotionOffer PO
			INNER JOIN
				@TEMP T1
			ON
				PO.numProId = T1.numPromotionID
			WHERE
				PO.numDomainId = @numDomainId
				AND ISNULL(IsOrderBasedPromotion,0) = 0
				AND ISNULL(numOrderPromotionID,0) > 0 
		) = 1
		BEGIN
			IF ISNULL(@numOrderPromotionDiscountID,0) > 0
			BEGIN
				IF (SELECT 
						COUNT(*)
					FROM
						DiscountCodes DC
					WHERE
						Dc.numDiscountId = @numOrderPromotionDiscountID
					) = 0
				BEGIN
					RAISERROR('INVALID_COUPON_CODE_ITEM_PROMOTION',16,1)
					RETURN
				END
				ELSE
				BEGIN
					-- IF THERE IS COUPON CODE USAGE LIMIT THAN VALIDATE USAGE LIMIT
					IF (SELECT 
							COUNT(*)
						FROM
							PromotionOffer PO
						INNER JOIN 
							PromotionOffer POOrder
						ON
							PO.numOrderPromotionID = POOrder.numProId
						INNER JOIN
							@TEMP T1
						ON
							PO.numProId = T1.numPromotionID
						INNER JOIN
							DiscountCodes DC
						ON
							POOrder.numProId = DC.numPromotionID
						WHERE
							PO.numDomainId = @numDomainId
							AND ISNULL(PO.IsOrderBasedPromotion,0) = 0
							AND DC.numDiscountId = @numOrderPromotionDiscountID
							AND 1 = (CASE WHEN ISNULL(DC.CodeUsageLimit,0) <> 0 THEN 1 ELSE 0 END)
							AND ISNULL((SELECT intCodeUsed FROM DiscountCodeUsage WHERE DiscountCodeUsage.numDiscountId=DC.numDiscountId AND numDivisionId=@numDivID),0) >= DC.CodeUsageLimit) > 0
					BEGIN
						RAISERROR('ITEM_PROMOTION_COUPON_CODE_USAGE_LIMIT_EXCEEDED',16,1)
						RETURN
					END
				END
			END
			ELSE
			BEGIN
				RAISERROR('COUPON_CODE_REQUIRED_ITEM_PROMOTION',16,1)
				RETURN
			END
		END
	END

	IF ISNULL(@numOrderPromotionID,0) > 0
	BEGIN
		-- VALIDTE IF ITS ORDER BASED PROMOTION (BOTH ITEM AND ORDER BASED PROMOTION CAN HAVE COUPON CODE)
		IF EXISTS (SELECT numProId FROM PromotionOffer WHERE numDomainId=@numDomainId AND numProId=@numOrderPromotionID AND ISNULL(IsOrderBasedPromotion,0) = 1)
		BEGIN
			-- CHECK IF ORDER PROMOTION IS STILL VALID
			IF NOT EXISTS (SELECT 
								PO.numProId
							FROM 
								PromotionOffer PO
							INNER JOIN 
								PromotionOfferOrganizations PORG
							ON 
								PO.numProId = PORG.numProId
							WHERE 
								numDomainId=@numDomainID 
								AND PO.numProId=@numOrderPromotionID
								AND ISNULL(IsOrderBasedPromotion,0) = 1
								AND ISNULL(bitEnabled,0) = 1
								AND 1 = (CASE WHEN bitNeverExpires = 1 THEN 1 ELSE (CASE WHEN GETUTCDATE()>=dtValidFrom AND GETUTCDATE()<=dtValidTo THEN 1 ELSE 0 END) END)
								AND numRelationship=@numRelationship 
								AND numProfile=@numProfile
			)
			BEGIN
				RAISERROR('ORDER_PROMOTION_EXPIRED',16,1)
				RETURN
			END
			ELSE IF ISNULL((SELECT ISNULL(bitRequireCouponCode,0) FROM PromotionOffer WHERE numDomainId=@numDomainId AND numProId=@numOrderPromotionID),0) = 1
			BEGIN
				IF ISNULL(@numOrderPromotionDiscountID,0) > 0
				BEGIN
					IF (SELECT 
							COUNT(*)
						FROM
							PromotionOffer PO
						INNER JOIN
							DiscountCodes DC
						ON
							PO.numProId = DC.numPromotionID
						WHERE
							PO.numDomainId = @numDomainId
							AND PO.numProId = @numOrderPromotionID
							AND ISNULL(IsOrderBasedPromotion,0) = 1
							AND ISNULL(bitRequireCouponCode,0) = 1
							AND ISNULL(DC.numDiscountId,0) = @numOrderPromotionDiscountID) = 0
					BEGIN
						RAISERROR('INVALID_COUPON_CODE_ORDER_PROMOTION',16,1)
						RETURN
					END
					ELSE
					BEGIN
						-- IF THERE IS COUPON CODE USAGE LIMIT THAN VALIDATE USAGE LIMIT
						IF (SELECT 
								COUNT(*)
							FROM
								PromotionOffer PO
							INNER JOIN
								DiscountCodes DC
							ON
								PO.numProId = DC.numPromotionID
							WHERE
								PO.numDomainId = @numDomainId
								AND PO.numProId = @numOrderPromotionID
								AND ISNULL(IsOrderBasedPromotion,0) = 1
								AND ISNULL(bitRequireCouponCode,0) = 1
								AND ISNULL(DC.numDiscountId,0) = @numOrderPromotionDiscountID
								AND 1 = (CASE WHEN ISNULL(DC.CodeUsageLimit,0) <> 0 THEN 1 ELSE 0 END)
								AND ISNULL((SELECT intCodeUsed FROM DiscountCodeUsage WHERE DiscountCodeUsage.numDiscountId=DC.numDiscountId AND numDivisionId=@numDivID),0) >= DC.CodeUsageLimit) > 0
						BEGIN
							RAISERROR('ORDER_PROMOTION_COUPON_CODE_USAGE_LIMIT_EXCEEDED',16,1)
							RETURN
						END
						ELSE
						BEGIN	
							IF EXISTS (SELECT DC.numDiscountID FROM DiscountCodes DC INNER JOIN DiscountCodeUsage DCU ON DC.numDiscountID=DCU.numDIscountID WHERE DC.numPromotionID=@numOrderPromotionID AND DCU.numDivisionID=@numDivID AND DC.numDiscountId=@numOrderPromotionDiscountID)
							BEGIN
								UPDATE
									DCU
								SET
									DCU.intCodeUsed = ISNULL(DCU.intCodeUsed,0) + 1
								FROM 
									DiscountCodes DC 
								INNER JOIN 
									DiscountCodeUsage DCU 
								ON 
									DC.numDiscountID=DCU.numDIscountID 
								WHERE 
									DC.numPromotionID=@numOrderPromotionID 
									AND DCU.numDivisionID=@numDivID 
									AND DC.numDiscountId=@numOrderPromotionDiscountID
							END
							ELSE
							BEGIN
								INSERT INTO DiscountCodeUsage
								(
									numDiscountId
									,numDivisionId
									,intCodeUsed
								)
								SELECT 
									DC.numDiscountId
									,@numDivID
									,1
								FROM
									DiscountCodes DC
								WHERE
									DC.numPromotionID=@numOrderPromotionID
									AND DC.numDiscountId=@numOrderPromotionDiscountID
							END
						END
					END
				END
				ELSE
				BEGIN
					RAISERROR('COUPON_CODE_REQUIRED_ORDER_PROMOTION',16,1)
					RETURN
				END
			END
		END
	END
	ELSE
	BEGIN
		-- IF PROMOTION ID NOT AVAILABLE THAN REMOVE ORDER PROMOTION DISCOUNT ITEM
		DELETE FROM CartItems WHERE numUserCntId =@numContactId AND numItemCode=@numDiscountItemID
	END                        
		
	IF @CRMType= 0                             
	BEGIN
		UPDATE 
			DivisionMaster                               
		SET 
			tintCRMType=1                              
		WHERE 
			numDivisionID=@numDivID
	END                            

	DECLARE @TotalAmount AS FLOAT
       

	IF LEN(ISNULL(@txtFuturePassword,'')) > 0
	BEGIN
		DECLARE @numExtranetID NUMERIC(18,0)

		SELECT @numExtranetID=numExtranetID FROM ExtarnetAccounts WHERE numDivisionID=@numDivID

		UPDATE ExtranetAccountsDtl SET vcPassword=@txtFuturePassword WHERE numExtranetID=@numExtranetID
	END                      

	DECLARE @numTemplateID NUMERIC
	SELECT 
		@numTemplateID= numMirrorBizDocTemplateId 
	FROM 
		eCommercePaymentConfig 
	WHERE 
		numSiteId=@numSiteID 
		AND numPaymentMethodId = @numPaymentMethodId 
		AND numDomainID = @numDomainID   

	DECLARE @fltExchangeRate FLOAT                                 
    
	IF @numCurrencyID=0 
	BEGIN
		SELECT 
			@numCurrencyID=ISNULL(numCurrencyID,0) 
		FROM 
			Domain 
		WHERE 
			numDomainID=@numDomainId                            
	END
		
	IF @numCurrencyID=0 
	BEGIN
		SET @fltExchangeRate=1
	END
	ELSE 
	BEGIN
		SET @fltExchangeRate=dbo.GetExchangeRate(@numDomainID,@numCurrencyID)                          
	END                      
                           
	DECLARE @numAccountClass AS NUMERIC(18) = 0

	IF @tintDefaultClassType <> 0
	BEGIN
		SELECT 
			@numAccountClass=ISNULL(numDefaultClass,0) 
		FROM 
			dbo.UserMaster UM 
		JOIN 
			dbo.Domain D 
		ON 
			UM.numDomainID=D.numDomainId
		WHERE 
			D.numDomainId=@numDomainId 
			AND UM.numUserDetailId=@numContactId
	END
	ELSE IF @tintDefaultClassType = 2 --COMPANY
	BEGIN
		SELECT 
			@numAccountClass=ISNULL(numAccountClassID,0) 
		FROM 
			dbo.DivisionMaster DM 
		WHERE 
			DM.numDomainId=@numDomainId 
			AND DM.numDivisionID=@numDivID
	END
	ELSE
	BEGIN
		SET @numAccountClass = 0
	END

	SET @numAccountClass=(SELECT TOP 1 numDefaultClass FROM dbo.eCommerceDTL WHERE numSiteId=@numSiteID AND numDomainId=@numDomainId)                  

	INSERT INTO OpportunityMaster
	(                              
		numContactId,numDivisionId,txtComments,numCampainID,bitPublicFlag,tintSource,tintSourceType,vcPOppName,monPAmount,                              
		numCreatedBy,bintCreatedDate,numModifiedBy,bintModifiedDate,numDomainId,tintOppType,tintOppStatus,intPEstimatedCloseDate,              
		numRecOwner,bitOrder,numCurrencyID,fltExchangeRate,fltDiscountTotal,bitDiscountTypeTotal,monShipCost,numOppBizDocTempID
		,numAccountClass,numPartner,intUsedShippingCompany,numPartenerContact,numShippingService,dtReleaseDate,vcOppRefOrderNo,
		bitBillingTerms,intBillingDays,bitInterestType,fltInterest,numDiscountID
	)                              
	VALUES                              
	(                              
		@numContactId,@numDivID,@txtComments,@numCampainID,0,@tintSource,@tintSourceType,ISNULL(@vcPOppName,'SO'),0,                                
		@numContactId,GETUTCDATE(),@numContactId,GETUTCDATE(),@numDomainId,1,@tintOppStatus,GETUTCDATE(),@numRecOwner,
		1,@numCurrencyID,@fltExchangeRate,@fltDiscount,@bitDiscountType,@monShipCost,@numTemplateID,@numAccountClass,@numPartner
		,@intUsedShippingCompany,@numPartenerContact,@numShipmentMethod,GETDATE(),@vcOppRefOrderNo,@bitBillingTerms,@intBillingDays,
		@bitInterestType,@fltInterest,@numOrderPromotionDiscountID
	)
		
	SET @numOppID=scope_identity()                             

	EXEC dbo.USP_UpdateNameTemplateValue 1,@numDomainId,@numOppID
	SELECT @vcPOppName = vcPOppName FROM OpportunityMaster WHERE numOppID= @numOppID

	INSERT INTO OpportunityItems
	(
		numOppId,numItemCode,numUnitHour,monPrice,monTotAmount,vcItemDesc,numWarehouseItmsID,vcType,vcAttributes,vcAttrValues,[vcItemName],
		[vcModelID],[vcManufacturer],vcPathForTImage, monVendorCost,numUOMId,bitDiscountType,fltDiscount,monTotAmtBefDiscount,numPromotionID,
		bitPromotionTriggered,bitDropShip,vcPromotionDetail,vcChildKitSelectedItems
	)                                                                                                              
	SELECT 
		@numOppID,X.numItemCode,x.numUnitHour * x.decUOMConversionFactor,x.monPrice/x.decUOMConversionFactor,x.monTotAmount,X.vcItemDesc,
		NULLIF(X.numWarehouseItmsID,0),X.vcItemType,X.vcAttributes,X.vcAttrValues,(SELECT vcItemName FROM item WHERE numItemCode = X.numItemCode),
		(SELECT vcModelID FROM item WHERE numItemCode = X.numItemCode),(SELECT vcManufacturer FROM item WHERE numItemCode = X.numItemCode),
		(SELECT TOP 1  vcPathForTImage FROM dbo.ItemImages WHERE numItemCode = X.numItemCode AND bitDefault = 1 AND numDomainId = @numDomainId),
		(SELECT ISNULL(VN.monCost,0) FROM Vendor VN INNER JOIN Item IT ON IT.numItemCode=VN.numItemCode and VN.numVendorID=IT.numVendorID WHERE VN.numItemCode=X.numItemCode), 
		numUOM,bitDiscountType,fltDiscount,monTotAmtBefDiscount,X.PromotionID,X.bitPromotionTrigerred
		,ISNULL((SELECT bitAllowDropShip FROM Item WHERE numItemCode=X.numItemCode),0),x.PromotionDesc,ISNULL(vcChildKitItemSelection,'')
	FROM 
		dbo.CartItems X 
	WHERE 
		numUserCntId =@numContactId


	--INSERT KIT ITEMS 
	IF (SELECT 
			COUNT(*) 
		FROM 
			OpportunityItems OI 
		LEFT JOIN
			WarehouseItems WI
		ON
			OI.numWarehouseItmsID = WI.numWarehouseItemID
		LEFT JOIN
			Warehouses W
		ON
			WI.numWarehouseID = W.numWarehouseID
		JOIN 
			ItemDetails ID 
		ON 
			OI.numItemCode=ID.numItemKitID 
		JOIN
			Item I
		ON
			ID.numChildItemID = I.numItemCode
		WHERE 
			OI.numOppId=@numOppId  
			AND ISNULL(OI.bitDropShip,0) = 0
			AND ISNULL(OI.numWarehouseItmsID,0) > 0
			AND I.charItemType = 'P'
			AND ISNULL((SELECT TOP 1 numWarehouseItemID FROM WareHouseItems WHERE numItemID=ID.numChildItemID AND numWareHouseID = W.numWarehouseID),0) = 0) > 0
	BEGIN
		RAISERROR('SELECTED_WAREHOUSE_IS_NOT_AVAILABLE_FOR_ALL_CHILD_ITEMS',16,1)
	END

			                
	INSERT INTO OpportunityKitItems
	(
		numOppId,
		numOppItemID,
		numChildItemID,
		numWareHouseItemId,
		numQtyItemsReq,
		numQtyItemsReq_Orig,
		numUOMId,
		numQtyShipped,
		monAvgCost
	)
	SELECT
		@numOppId,
		OI.numoppitemtCode,
		ID.numChildItemID,
		(SELECT TOP 1 numWarehouseItemID FROM WareHouseItems WHERE numItemID=ID.numChildItemID AND WareHouseItems.numWareHouseID = W.numWarehouseID ORDER BY numWareHouseItemID),
		(ID.numQtyItemsReq * ISNULL(dbo.fn_UOMConversion(ID.numUOMId,ID.numChildItemID,@numDomainID,I.numBaseUnit),1)) * OI.numUnitHour,
		ID.numQtyItemsReq,
		ID.numUOMId,
		0,
		ISNULL(I.monAverageCost,0)
	FROM 
		OpportunityItems OI 
	LEFT JOIN
		WarehouseItems WI
	ON
		OI.numWarehouseItmsID = WI.numWarehouseItemID
	LEFT JOIN
		Warehouses W
	ON
		WI.numWarehouseID = W.numWarehouseID
	JOIN 
		ItemDetails ID 
	ON 
		OI.numItemCode=ID.numItemKitID 
	JOIN
		Item I
	ON
		ID.numChildItemID = I.numItemCode
	WHERE 
		OI.numOppId=@numOppId 

	--INSERT KIT ITEM HAS ANOTHER KIT AS CHILD THEN SAVE CONFIGURATION OF KIT
	DECLARE @TempKitConfiguration TABLE
	(
		numOppItemID NUMERIC(18,0),
		numItemCode NUMERIC(18,0),
		KitChildItems VARCHAR(MAX)
	)

	INSERT INTO 
		@TempKitConfiguration
	SELECT
		numoppitemtCode,
		numItemCode,
		vcChildKitSelectedItems
	FROM
		OpportunityItems
	WHERE
		numOppId = @numOppID
		AND LEN(ISNULL(vcChildKitSelectedItems,'')) > 0
					

	IF (SELECT 
			COUNT(*) 
		FROM 
		(
			SELECT 
				t1.numOppItemID
				,t1.numItemCode
				,SUBSTRING(items,0,CHARINDEX('-',items)) AS numKitItemID
				,SUBSTRING(items,CHARINDEX('-',items) + 1,LEN(items) - CHARINDEX('-',items)) As numKitChildItemID
			FROM 
				@TempKitConfiguration t1
			CROSS APPLY
			(
				SELECT
					*
				FROM 
					dbo.Split((SELECT KitChildItems FROM @TempKitConfiguration t3 WHERE t3.numOppItemID=t1.numOppItemID AND t3.numItemCode=t1.numItemCode),',') 
			) as t2
		) TempChildItems
		INNER JOIN
			OpportunityKitItems OKI
		ON
			OKI.numOppId=@numOppId
			AND OKI.numOppItemID=TempChildItems.numOppItemID
			AND OKI.numChildItemID=TempChildItems.numKitItemID
		INNER JOIN
			OpportunityItems OI
		ON
			OI.numItemCode = TempChildItems.numItemCode
			AND OI.numoppitemtcode = OKI.numOppItemID
		LEFT JOIN
			WarehouseItems WI
		ON
			OI.numWarehouseItmsID = WI.numWarehouseItemID
		LEFT JOIN
			Warehouses W
		ON
			WI.numWarehouseID = W.numWarehouseID
		INNER JOIN
			ItemDetails
		ON
			TempChildItems.numKitItemID = ItemDetails.numItemKitID
			AND TempChildItems.numKitChildItemID = ItemDetails.numChildItemID
		INNER JOIN
			Item 
		ON
			ItemDetails.numChildItemID = Item.numItemCode
		WHERE
			Item.charItemType = 'P'
			AND ISNULL((SELECT TOP 1 numWarehouseItemID FROM WareHouseItems WHERE numItemID=ItemDetails.numChildItemID AND WareHouseItems.numWareHouseID = W.numWarehouseID),0) = 0) > 0
	BEGIN
		RAISERROR('SELECTED_WAREHOUSE_IS_NOT_AVAILABLE_FOR_ALL_CHILD_ITEMS',16,1)
	END

	INSERT INTO OpportunityKitChildItems
	(
		numOppID,
		numOppItemID,
		numOppChildItemID,
		numItemID,
		numWareHouseItemId,
		numQtyItemsReq,
		numQtyItemsReq_Orig,
		numUOMId,
		numQtyShipped,
		monAvgCost
	)
	SELECT 
		@numOppID,
		OKI.numOppItemID,
		OKI.numOppChildItemID,
		ItemDetails.numChildItemID,
		(SELECT TOP 1 numWarehouseItemID FROM WareHouseItems WHERE numItemID=ItemDetails.numChildItemID AND WareHouseItems.numWareHouseID = W.numWarehouseID ORDER BY numWareHouseItemID),
		(ItemDetails.numQtyItemsReq * ISNULL(dbo.fn_UOMConversion(ItemDetails.numUOMId,ItemDetails.numChildItemID,@numDomainID,Item.numBaseUnit),1)) * OKI.numQtyItemsReq,
		ItemDetails.numQtyItemsReq,
		ItemDetails.numUOMId,
		0,
		ISNULL(Item.monAverageCost,0)
	FROM
	(
		SELECT 
			t1.numOppItemID
			,t1.numItemCode
			,SUBSTRING(items,0,CHARINDEX('-',items)) AS numKitItemID
			,SUBSTRING(items,CHARINDEX('-',items) + 1,LEN(items) - CHARINDEX('-',items)) As numKitChildItemID
		FROM 
			@TempKitConfiguration t1
		CROSS APPLY
		(
			SELECT
				*
			FROM 
				dbo.Split((SELECT KitChildItems FROM @TempKitConfiguration t3 WHERE t3.numOppItemID=t1.numOppItemID AND t3.numItemCode=t1.numItemCode),',') 
		) as t2
	) TempChildItems
	INNER JOIN
		OpportunityKitItems OKI
	ON
		OKI.numOppId=@numOppId
		AND OKI.numOppItemID=TempChildItems.numOppItemID
		AND OKI.numChildItemID=TempChildItems.numKitItemID
	INNER JOIN
		OpportunityItems OI
	ON
		OI.numItemCode = TempChildItems.numItemCode
		AND OI.numoppitemtcode = OKI.numOppItemID
	LEFT JOIN
		WarehouseItems WI
	ON
		OI.numWarehouseItmsID = WI.numWarehouseItemID
	LEFT JOIN
		Warehouses W
	ON
		WI.numWarehouseID = W.numWarehouseID
	INNER JOIN
		ItemDetails
	ON
		TempChildItems.numKitItemID = ItemDetails.numItemKitID
		AND TempChildItems.numKitChildItemID = ItemDetails.numChildItemID
	INNER JOIN
		Item 
	ON
		ItemDetails.numChildItemID = Item.numItemCode

	BEGIN TRY
		DECLARE @TEMPContainer TABLE
		(
			numConainer NUMERIC(18,0)
			,numNoItemIntoContainer NUMERIC(18,0)
			,numWarehouseID NUMERIC(18,0)
			,monPrice DECIMAL(20,5)
			,monAverageCost DECIMAL(20,5)
			,numQtyToFit FLOAT
			,vcItemName VARCHAR(300)
			,vcItemDesc VARCHAR(1000)
			,vcModelID VARCHAR(300)
		)

		INSERT INTO @TEMPContainer
		(
			numConainer
			,numNoItemIntoContainer
			,numWarehouseID
			,monPrice
			,monAverageCost
			,numQtyToFit
			,vcItemName
			,vcItemDesc
			,vcModelID
		)
		SELECT
			I.numContainer
			,I.numNoItemIntoContainer
			,WI.numWarehouseID
			,ISNULL(IContainer.monListPrice,0)
			,ISNULL(IContainer.monAverageCost,0)
			,SUM(numUnitHour)
			,ISNULL(IContainer.vcItemName,'')
			,ISNULL(IContainer.txtItemDesc,'')
			,ISNULL(IContainer.vcModelID,'')
		FROM
			OpportunityItems OI
		INNER JOIN
			Item I
		ON
			OI.numItemCode = I.numItemCode
		INNER JOIN
			Item IContainer
		ON
			I.numContainer = IContainer.numItemCode
		LEFT JOIN
			WareHouseItems WI
		ON
			OI.numWarehouseItmsID = WI.numWareHouseItemID
		WHERE
			numOppID = @numOppID
			AND ISNULL(OI.bitDropShip,0) = 0
			AND ISNULL(I.bitContainer,0) = 0
			AND ISNULL(IContainer.bitContainer,0) = 1
			AND ISNULL(I.numContainer,0) > 0
			AND ISNULL(I.numNoItemIntoContainer,0) > 0
		GROUP BY
			I.numContainer
			,I.numNoItemIntoContainer
			,IContainer.monListPrice
			,IContainer.monAverageCost
			,WI.numWarehouseID
			,IContainer.vcItemName
			,IContainer.txtItemDesc
			,IContainer.vcModelID

		INSERT INTO OpportunityItems
		(
			numOppId
			,numItemCode
			,numWarehouseItmsID
			,numUnitHour
			,monPrice
			,fltDiscount
			,bitDiscountType
			,monTotAmount
			,monTotAmtBefDiscount
			,monAvgCost
			,vcItemName
			,vcItemDesc
			,vcModelID
		)
		SELECT
			@numOppID
			,numConainer
			,(SELECT TOP 1 numWarehouseItemID FROM WarehouseItems WHERE numItemID=numConainer AND WarehouseItems.numWarehouseID=T1.numWarehouseID ORDER BY numWareHouseItemID)
			,(CASE WHEN numQtyToFit > numNoItemIntoContainer THEN CEILING(numQtyToFit/numNoItemIntoContainer) ELSE 1 END)
			,monPrice
			,0
			,1
			,(CASE WHEN numQtyToFit > numNoItemIntoContainer THEN CEILING(numQtyToFit/numNoItemIntoContainer) ELSE 1 END) * monPrice
			,(CASE WHEN numQtyToFit > numNoItemIntoContainer THEN CEILING(numQtyToFit/numNoItemIntoContainer) ELSE 1 END) * monPrice
			,monAverageCost
			,vcItemName
			,vcItemDesc
			,vcModelID
		FROM
			@TEMPContainer T1

		DECLARE @tintShipped AS TINYINT      
		DECLARE @tintOppType AS TINYINT
	
		SELECT 
			@tintOppStatus=tintOppStatus
			,@tintOppType=tintOppType
			,@tintShipped=tintShipped 
		FROM 
			OpportunityMaster 
		WHERE 
			numOppID=@numOppID
	END TRY
	BEGIN CATCH
		-- DO NOT RAISE ERROR
	END CATCH           
	
	EXEC USP_OpportunityItems_UpdateWarehouseMapping @numDomainID,@numOppID

	IF @tintOppStatus=1 AND @tintCommitAllocation=1               
	BEGIN         
		EXEC USP_UpdatingInventoryonCloseDeal @numOppID,0,@numContactId
	END
	
	EXEC USP_OpportunityItems_CheckWarehouseMapping @numDomainID,@numOppID                       
                  
	SELECT
		@TotalAmount=SUM(monTotAmount)
	FROM 
		OpportunityItems 
	WHERE 
		numOppId=@numOppID                            
 
	UPDATE 
		OpportunityMaster 
	SET 
		monPamount = @TotalAmount - @fltDiscount                           
	WHERE 
		numOppId=@numOppID                           
                                               
	IF(@vcSource IS NOT NULL)
	BEGIN
		insert into OpportunityLinking ([numParentOppID],[numChildOppID],[vcSource],[numParentOppBizDocID],numPromotionId,numSiteID)  values(null,@numOppID,@vcSource,NULL,@numProId,@numSiteID);
	END

	--Add/Update Address Details
	DECLARE @vcStreet  varchar(100),@vcCity  varchar(50),@vcPostalCode  varchar(15),@vcCompanyName  varchar(100),@vcAddressName VARCHAR(50), @vcAltContact VARCHAR(200)     
	DECLARE @numState  numeric(9),@numCountry  numeric(9), @numCompanyId  numeric(9), @numContact NUMERIC(18,0)
	DECLARE @bitIsPrimary BIT, @bitAltContact BIT;

	--Bill Address
	IF @numBillAddressId>0
	BEGIN
		SELECT
			@vcStreet=ISNULL(vcStreet,'')
			,@vcCity=ISNULL(vcCity,'')
			,@vcPostalCode=ISNULL(vcPostalCode,'')
			,@numState=ISNULL(numState,0)
			,@numCountry=ISNULL(numCountry,0)
			,@bitIsPrimary=bitIsPrimary
			,@vcAddressName=vcAddressName
			,@numContact=numContact
			,@bitAltContact=bitAltContact
			,@vcAltContact=vcAltContact
		FROM 
			dbo.AddressDetails 
		WHERE 
			numDomainID = @numDomainID 
			AND numAddressID = @numBillAddressId 
            
  
  		EXEC dbo.USP_UpdateOppAddress
		@numOppID = @numOppID, --  numeric(9, 0)
		@byteMode = 0, --  tinyint
		@vcStreet = @vcStreet, --  varchar(100)
		@vcCity = @vcCity, --  varchar(50)
		@vcPostalCode = @vcPostalCode, --  varchar(15)
		@numState = @numState, --  numeric(9, 0)
		@numCountry = @numCountry, --  numeric(9, 0)
		@vcCompanyName = '', --  varchar(100)
		@numCompanyId = 0, --  numeric(9, 0)
		@vcAddressName =@vcAddressName,
		@bitCalledFromProcedure=1,
		@numContact = @numContact,
		@bitAltContact = @bitAltContact,
		@vcAltContact = @vcAltContact
        
	END
  
  
	--Ship Address
	IF @numShipAddressId>0
	BEGIN
   		SELECT 
			@vcStreet=ISNULL(vcStreet,'')
			,@vcCity=ISNULL(vcCity,'')
			,@vcPostalCode=ISNULL(vcPostalCode,'')
			,@numState=ISNULL(numState,0)
			,@numCountry=ISNULL(numCountry,0)
			,@bitIsPrimary=bitIsPrimary
			,@vcAddressName=vcAddressName
			,@numContact=numContact
			,@bitAltContact=bitAltContact
			,@vcAltContact=vcAltContact
		FROM 
			dbo.AddressDetails 
		WHERE 
			numDomainID = @numDomainID 
			AND numAddressID = @numShipAddressId 
  
		select  @vcCompanyName=vcCompanyName,@numCompanyId=div.numCompanyID from CompanyInfo Com                            
		join divisionMaster Div                            
		on div.numCompanyID=com.numCompanyID                            
		where div.numdivisionID=@numDivID

  		EXEC dbo.USP_UpdateOppAddress
		@numOppID = @numOppID, --  numeric(9, 0)
		@byteMode = 1, --  tinyint
		@vcStreet = @vcStreet, --  varchar(100)
		@vcCity = @vcCity, --  varchar(50)
		@vcPostalCode = @vcPostalCode, --  varchar(15)
		@numState = @numState, --  numeric(9, 0)
		@numCountry = @numCountry, --  numeric(9, 0)
		@vcCompanyName = @vcCompanyName, --  varchar(100)
		@numCompanyId =@numCompanyId, --  numeric(9, 0)
		@vcAddressName =@vcAddressName,
		@bitCalledFromProcedure=1,
		@numContact = @numContact,
		@bitAltContact = @bitAltContact,
		@vcAltContact = @vcAltContact
   	
	END

	-- IMPORTANT: KEEP THIS BELOW ADDRESS UPDATE LOGIC
	--Insert Tax for Division                       
	INSERT dbo.OpportunityMasterTaxItems 
	(
		numOppId,
		numTaxItemID,
		fltPercentage,
		tintTaxType,
		numTaxID
	) 
	SELECT 
		@numOppID,
		TI.numTaxItemID,
		TEMPTax.decTaxValue,
		TEMPTax.tintTaxType,
		0
	FROM 
		TaxItems TI 
	JOIN 
		DivisionTaxTypes DTT 
	ON 
		TI.numTaxItemID = DTT.numTaxItemID
	CROSS APPLY
	(
		SELECT
			decTaxValue,
			tintTaxType,
			numTaxID
		FROM
			dbo.fn_CalItemTaxAmt(@numDomainID,@numDivID,0,TI.numTaxItemID,@numOppId,0,NULL)
	) AS TEMPTax
	WHERE 
		DTT.numDivisionID=@numDivID AND DTT.bitApplicable=1
	UNION 
	SELECT 
		@numOppID,
		0,
		TEMPTax.decTaxValue,
		TEMPTax.tintTaxType,
		0
	FROM 
		dbo.DivisionMaster 
	CROSS APPLY
	(
		SELECT
			decTaxValue,
			tintTaxType,
			numTaxID
		FROM
			dbo.fn_CalItemTaxAmt(@numDomainID,@numDivID,0,0,@numOppId,1,NULL)
	) AS TEMPTax
	WHERE 
		bitNoTax=0 
		AND numDivisionID=@numDivID
	UNION 
	SELECT
		@numOppId
		,1
		,decTaxValue
		,tintTaxType
		,numTaxID
	FROM
		dbo.fn_CalItemTaxAmt(@numDomainID,@numDivID,0,1,@numOppId,1,NULL)
  
	--Delete Tax for Opportunity Items if item deleted 
	DELETE FROM OpportunityItemsTaxItems WHERE numOppId=@numOppId AND numOppItemID NOT IN (SELECT numoppitemtCode FROM OpportunityItems WHERE numOppId=@numOppID)

	--Insert Tax for Opportunity Items
	INSERT INTO dbo.OpportunityItemsTaxItems 
	(
		numOppId,
		numOppItemID,
		numTaxItemID,
		numTaxID
	) 
	SELECT 
		@numOppId,
		OI.numoppitemtCode,
		TI.numTaxItemID,
		0 
	FROM 
		dbo.OpportunityItems OI 
	JOIN 
		dbo.ItemTax IT 
	ON 
		OI.numItemCode=IT.numItemCode 
	JOIN
		TaxItems TI 
	ON 
		TI.numTaxItemID=IT.numTaxItemID 
	WHERE 
		OI.numOppId=@numOppID 
		AND IT.bitApplicable=1 
		AND OI.numoppitemtCode NOT IN (SELECT numOppItemID FROM OpportunityItemsTaxItems WHERE numOppId=@numOppID)
	UNION
	SELECT 
		@numOppId,
		OI.numoppitemtCode,
		0,
		0 
	FROM 
		dbo.OpportunityItems OI 
	JOIN 
		dbo.Item I 
	ON 
		OI.numItemCode=I.numItemCode
	WHERE 
		OI.numOppId=@numOppID 
		AND I.bitTaxable=1 
		AND OI.numoppitemtCode NOT IN (SELECT numOppItemID FROM OpportunityItemsTaxItems WHERE numOppId=@numOppID)
	UNION
	SELECT
		@numOppId,
		OI.numoppitemtCode,
		1,
		TD.numTaxID
	FROM
		dbo.OpportunityItems OI 
	INNER JOIN
		ItemTax IT
	ON
		IT.numItemCode = OI.numItemCode
	INNER JOIN
		TaxDetails TD
	ON
		TD.numTaxID = IT.numTaxID
		AND TD.numDomainId = @numDomainId
	WHERE
		OI.numOppId = @numOppID
		AND OI.numoppitemtCode NOT IN (SELECT numOppItemID FROM OpportunityItemsTaxItems WHERE numOppId=@numOppID)

	UPDATE 
		OI
	SET
		OI.vcInclusionDetail = [dbo].[GetOrderAssemblyKitInclusionDetails](OI.numOppID,OI.numoppitemtCode,OI.numUnitHour,1,1)
	FROM 
		OpportunityItems OI
	INNER JOIN
		Item I
	ON
		OI.numItemCode = I.numItemCode
	WHERE
		numOppId=@numOppID
		AND ISNULL(I.bitKitParent,0) = 1
COMMIT
END TRY
BEGIN CATCH
	DECLARE @ErrorMessage NVARCHAR(4000)
	DECLARE @ErrorNumber INT
	DECLARE @ErrorSeverity INT
	DECLARE @ErrorState INT
	DECLARE @ErrorLine INT
	DECLARE @ErrorProcedure NVARCHAR(200)

  -- Whoops, there was an error
  IF @@TRANCOUNT > 0
     ROLLBACK

  SELECT 
		@ErrorMessage = ERROR_MESSAGE(),
		@ErrorNumber = ERROR_NUMBER(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE(),
		@ErrorLine = ERROR_LINE(),
		@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

	RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);
END CATCH
