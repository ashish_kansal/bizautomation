/****** Object:  StoredProcedure [dbo].[USP_Journal_CompanyName]    Script Date: 07/26/2008 16:19:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created By Siva  
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_journal_companyname')
DROP PROCEDURE usp_journal_companyname
GO
CREATE PROCEDURE [dbo].[USP_Journal_CompanyName]  
@numDomainID numeric(9)=0,    
@numDivisionId  numeric(9)=0  
As         
Begin        
Select CI.vcCompanyName From CompanyInfo CI        
join Divisionmaster DM on  CI.numCompanyid=DM.numCompanyid                            
Where CI.numDomainID=@numDomainID And DM.numDivisionID=@numDivisionId  
End
GO
