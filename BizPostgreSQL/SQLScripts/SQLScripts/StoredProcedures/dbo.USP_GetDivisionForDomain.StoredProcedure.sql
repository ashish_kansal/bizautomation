/****** Object:  StoredProcedure [dbo].[USP_GetDivisionForDomain]    Script Date: 07/26/2008 16:17:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getdivisionfordomain')
DROP PROCEDURE usp_getdivisionfordomain
GO
CREATE PROCEDURE [dbo].[USP_GetDivisionForDomain]      
@numDomainId as numeric(9)      
as      
SELECT Dom.numDivisionId as numDivisionId,LD.vcData as vcData    
   FROM  DomainDivision Dom        
 join ListDetails LD        
 on Dom.numDivisionId=LD.numListItemID        
   WHERE Dom.numdomainid=@numDomainId
GO
