/****** Object:  StoredProcedure [dbo].[usp_InsertNewChartAccountDetails]    Script Date: 09/25/2009 16:15:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created By Siva                                
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'usp_insertnewchartaccountdetails' ) 
    DROP PROCEDURE usp_insertnewchartaccountdetails
GO
CREATE PROCEDURE [dbo].[usp_InsertNewChartAccountDetails]
    @numAcntTypeId NUMERIC(9) = 0,
    @numParntAcntTypeID NUMERIC(9) = 0,
    @vcAccountName VARCHAR(100),
    @vcAccountDescription VARCHAR(100),
    @monOriginalOpeningBal DECIMAL(20,5),
    @monOpeningBal DECIMAL(20,5),
    @dtOpeningDate DATETIME,
    @bitActive BIT,
    @numAccountId NUMERIC(9) = 0 OUTPUT,
    @bitFixed BIT,
    @numDomainId NUMERIC(9) = 0,
    @numListItemID NUMERIC(9) = 0,
    @numUserCntId NUMERIC(9) = 0,
    @bitProfitLoss BIT,
    @bitDepreciation BIT,
    @dtDepreciationCostDate DATETIME,
    @monDepreciationCost DECIMAL(20,5),
    @vcNumber VARCHAR(50),
    @bitIsSubAccount BIT,
    @numParentAccId NUMERIC(18, 0),
    @IsBankAccount			BIT	=0,
	@vcStartingCheckNumber	VARCHAR(50)=NULL ,
	@vcBankRountingNumber	VARCHAR(50)=NULL ,
	@bitIsConnected				BIT =0,
	@numBankDetailID			NUMERIC(18,0)=NULL

AS 
BEGIN 
	BEGIN TRY
	BEGIN TRANSACTION

    
        DECLARE @numAccountId1 AS NUMERIC(9)                              
        DECLARE @strSQl AS VARCHAR(1000)                             
        DECLARE @strSQLBalance AS VARCHAR(1000)                           
        DECLARE @numJournalId AS NUMERIC(9) 
        DECLARE @numDepositId AS NUMERIC(9)     
        DECLARE @numDepositJournalId AS NUMERIC(9)  
        DECLARE @numCashCreditJournalId AS NUMERIC(9)  
        DECLARE @numCashCreditId AS NUMERIC(9)   
        DECLARE @numAccountIdOpeningEquity AS NUMERIC(9)
        DECLARE @vcAccountCode VARCHAR(50)
        DECLARE @intLevel AS INT
        DECLARE @intFlag INT
        DECLARE @vcAccountNameWithNumber AS VARCHAR(100)
        
    
        SET @strSQl = ''                          
        SET @strSQLBalance = ''   
        
        SET @intFlag = 1 
        IF ISNULL(@vcNumber,'') = '-1' --@vcNumber will have -1 value only when its being called from [USP_ChartAcntDefaultValues], as well cursor below creates time out.
		BEGIN
			SET @intFlag = 0 
			SET @vcNumber= ''
			PRINT @intFlag
		END
		  
        IF ISNULL(@vcNumber, '') = '' 
            SET @vcAccountNameWithNumber = ''	
        ELSE 
            SET @vcAccountNameWithNumber = @vcNumber + ' '   
	
		IF @numAcntTypeId = 0 
		BEGIN
			DECLARE @AccountTypeCode VARCHAR(4)
			(SELECT @AccountTypeCode = SUBSTRING(vcAccountCode ,1,4) FROM dbo.AccountTypeDetail WHERE numAccountTypeID = @numParntAcntTypeID)
			SELECT @numAcntTypeId = numAccountTypeID FROM dbo.AccountTypeDetail WHERE numDomainID=@numDomainID AND vcAccountCode =  @AccountTypeCode
		END
	                             
		--Check for Current Fiancial Year
        IF (SELECT COUNT([numFinYearId]) FROM [FinancialYear] WHERE [numDomainId] = @numDomainId AND [bitCurrentYear] = 1) = 0 
        BEGIN
            RAISERROR ( 'NoCurrentFinancialYearSet', 16, 1 ) ;
            RETURN ;
        END
	
		  
        IF @numAccountId = 0 
        BEGIN 
			IF ISNULL(@vcNumber, '') <> '' 
			BEGIN
				IF EXISTS(SELECT * FROM Chart_Of_Accounts WHERE vcNumber = @vcNumber AND numDomainID =  @numDomainId)	
				BEGIN
					RAISERROR ( 'DuplicateNumber', 16, 1 ) ;
					RETURN ;
				END
			END 
				 
            IF @numParntAcntTypeID = 0 
			BEGIN
                SET @numParntAcntTypeID = (SELECT TOP 1 numAccountId FROM dbo.Chart_Of_Accounts WHERE Chart_Of_Accounts.numParntAcntTypeID IS NULL AND Chart_Of_Accounts.numDomainId = @numDomainId)
			END

            SET @numAccountIdOpeningEquity = (SELECT TOP 1 numAccountId FROM dbo.Chart_Of_Accounts WHERE bitProfitLoss = 1 AND Chart_Of_Accounts.numDomainId = @numDomainId) 
            
         
            IF (ISNULL(@numAccountIdOpeningEquity, 0) = 0 AND @bitProfitLoss = 0)
            BEGIN
                RAISERROR ( 'NoProfitLossACC', 16, 1 ) ;
                RETURN 
            END
			
            SELECT  @vcAccountCode = dbo.GetAccountTypeCode(@numDomainId, @numParntAcntTypeID, 1)
                
            IF (@bitIsSubAccount=1)
			BEGIN
				SELECT  @vcAccountCode = dbo.GetAccountTypeCode(@numDomainId, @numParentAccId, 3)
				SELECT @numParntAcntTypeID = numParntAcntTypeId FROM Chart_Of_Accounts WHERE numDomainId=@numDomainId AND numAccountId = @numParentAccId
			END
                
            SELECT  @intLevel = CASE WHEN @numParentAccId = 0 AND @bitIsSubAccount = 0 THEN 0 ELSE ISNULL(MAX(intLevel),0) + 1 END  FROM dbo.Chart_Of_Accounts WHERE numAccountId = @numParentAccId 
            PRINT @intLevel
									   
--			PRINT @vcAccountCode
            INSERT  INTO dbo.Chart_Of_Accounts
            (
                numAcntTypeId,
                numParntAcntTypeID,
                vcAccountCode,
                vcAccountName,
                vcAccountDescription,
                numOriginalOpeningBal,
                numOpeningBal,
                dtOpeningDate,
                bitActive,
                bitFixed,
                numDomainId,
                numListItemID,
                bitProfitLoss,
                [bitDepreciation],
                [dtDepreciationCostDate],
                [monDepreciationCost],
                vcNumber,
                bitIsSubAccount,
                numParentAccId,
                intLevel,
                IsBankAccount,
                vcStartingCheckNumber,
                vcBankRountingNumber,
                IsConnected,
                numBankDetailID
            )
            VALUES  
			(
                @numAcntTypeId,
                @numParntAcntTypeID,
                @vcAccountCode,
                @vcAccountNameWithNumber + @vcAccountName,
                @vcAccountDescription,
                @monOriginalOpeningBal,
                @monOpeningBal,
                @dtOpeningDate,
                @bitActive,
                @bitFixed,
                @numDomainId,
                @numListItemID,
                @bitProfitLoss,
                @bitDepreciation,
                @dtDepreciationCostDate,
                @monDepreciationCost,
                @vcNumber,
                @bitIsSubAccount,
                @numParentAccId,
                @intLevel,
                @IsBankAccount,
                @vcStartingCheckNumber,
                @vcBankRountingNumber,
                @bitIsConnected,
                @numBankDetailID
            )
                
				
			SET @numAccountId1 = SCOPE_IDENTITY()                                                  
            SET @numAccountId = @numAccountId1 --used at bottom
            SELECT @numAccountId1                            
				
			
			
			--Added By Chintan
            EXEC USP_ManageChartAccountOpening @numDomainId, @numAccountId1, @monOriginalOpeningBal, @dtOpeningDate                                    
        END
        ELSE IF @numAccountId <> 0 
        BEGIN
            IF EXISTS(SELECT * FROM Chart_Of_Accounts WHERE numAccountId = @numAccountId AND numDomainId = @numDomainId AND (numAcntTypeId != @numAcntTypeId OR numParntAcntTypeId != @numParntAcntTypeID))
			BEGIN
				IF NOT ((SELECT COUNT(*) FROM AccountTypeDetail WHERE numAccountTypeID=@numParntAcntTypeID AND vcAccountcode LIKE '0106%' AND numDomainId = @numDomainId )=1
					AND (SELECT COUNT(*) FROM AccountTypeDetail WHERE numAccountTypeID=(SELECT numParntAcntTypeId FROM Chart_Of_Accounts WHERE numAccountId = @numAccountId AND numDomainId = @numDomainId) AND vcAccountcode LIKE '0104%' AND numDomainId = @numDomainId)=1)
				BEGIN
					IF EXISTS (SELECT GJD.numJournalId FROM dbo.General_Journal_Details GJD WHERE numDomainId = @numDomainId AND numChartAcntId=@numAccountId)
					BEGIN
						RAISERROR ( 'JournalEntryExists', 16, 1 ) ;
						RETURN ;
					END
				END
			END
					
			IF ISNULL(@vcNumber, '') <> '' 
			BEGIN
				IF EXISTS(SELECT * FROM Chart_Of_Accounts WHERE vcNumber = @vcNumber AND numDomainID =  @numDomainId AND numAccountId <> @numAccountId)	
				BEGIN
					RAISERROR ( 'DuplicateNumber', 16, 1 ) ;
					RETURN ;
				END
			END 

            IF @numParntAcntTypeID = 0 
			BEGIN
                SET @numParntAcntTypeID = (SELECT Chart_Of_Accounts.numAccountId FROM dbo.Chart_Of_Accounts WHERE Chart_Of_Accounts.numParntAcntTypeID IS NULL AND Chart_Of_Accounts.numDomainId = @numDomainId) 
			END
                            
            DECLARE @OldOriginalOpeningBalance AS DECIMAL(20,5) 
            DECLARE @OldOpeningBalance AS DECIMAL(20,5) 
            DECLARE @NewOpeningBalance AS DECIMAL(20,5)
            DECLARE @OldParentAcntTypeID AS NUMERIC
			DECLARE @OldIsSubAccount AS NUMERIC
					
            SELECT  @OldOriginalOpeningBalance = numOriginalOpeningBal,
                    @OldOpeningBalance = numOpeningBal,
                    @OldParentAcntTypeID = numParntAcntTypeId,
                    @vcAccountCode = vcAccountCode,
                    @OldIsSubAccount = ISNULL(bitIsSubAccount,0)
            FROM    dbo.Chart_Of_Accounts
            WHERE   numAccountId = @numAccountID
                    AND numDomainId = @numDomainID
			
            IF ( @OldParentAcntTypeID != @numParntAcntTypeID ) --bug fix 1439
            BEGIN
                SELECT  @vcAccountCode = dbo.GetAccountTypeCode(@numDomainId, @numParntAcntTypeID, 1)
                SET @numParentAccId = 0
                SET @bitIsSubAccount = 0
            END
                        
			IF (@bitIsSubAccount=1)
			BEGIN
				SELECT  @vcAccountCode = dbo.GetAccountTypeCode(@numDomainId, @numParentAccId, 3)
				SELECT @numParntAcntTypeID = numParntAcntTypeId FROM Chart_Of_Accounts WHERE numDomainId=@numDomainId AND numAccountId = @numParentAccId
			END
			
            UPDATE  
				Chart_Of_Accounts
            SET     
				numAcntTypeId = @numAcntTypeId,
                numParntAcntTypeId = @numParntAcntTypeID,
                vcAccountName = @vcAccountNameWithNumber
                + @vcAccountName,
                vcAccountDescription = @vcAccountDescription,
                bitActive = @bitActive,
                bitProfitLoss = @bitProfitLoss,
                [numOriginalOpeningBal] = @monOriginalOpeningBal,
                [dtOpeningDate] = @dtOpeningDate,
                [bitDepreciation] = @bitDepreciation,
                [dtDepreciationCostDate] = @dtDepreciationCostDate,
                [monDepreciationCost] = @monDepreciationCost,
                vcAccountCode = @vcAccountCode,
                bitIsSubAccount = @bitIsSubAccount,
                vcNumber = @vcNumber,
                numParentAccId = @numParentAccId,
				IsBankAccount = @IsBankAccount,
				vcStartingCheckNumber = @vcStartingCheckNumber,
				vcBankRountingNumber = @vcBankRountingNumber,
				IsConnected = @bitIsConnected,
				numBankDetailID = @numBankDetailID
            WHERE   
				(numAccountId = @numAccountId) 
				AND ( numDomainId = @numDomainId )
			
		--UPDATE dbo.Chart_Of_Accounts SET intLevel = 0 WHERE numAccountId = @numAccountId
            UPDATE  
				dbo.Chart_Of_Accounts
            SET 
				intLevel = CASE 
							WHEN numParentAccId = 0 AND bitIsSubAccount = 0 THEN 0
							ELSE ( SELECT ISNULL(intLevel,0) + 1 FROM dbo.Chart_Of_Accounts WHERE numAccountId = @numParentAccId )
							END
            WHERE (bitIsSubAccount = 1 OR bitIsSubAccount = 0) AND numAccountId = @numAccountId

            EXEC USP_ManageChartAccountOpening @numDomainId,@numAccountId, @monOriginalOpeningBal, @dtOpeningDate                                                                      
            
		END
    
    
		--Maintain Sorting by Account code when adding new account with sub account  or  updating Account numeber
		PRINT @intFlag
		if (@intFlag =1) -- @vcNumber will have -1 value only when its being called from [USP_ChartAcntDefaultValues], as well cursor below creates time out.
		BEGIN
			UPDATE dbo.Chart_Of_Accounts SET vcAccountCode='' WHERE numParntAcntTypeId = @numParntAcntTypeID  AND numDomainId=@numDomainID


			DECLARE @TEMP TABLE
			(
				ID INT IDENTITY(1,1)
				,numAccountId NUMERIC(18,0)
				,bitIsSubAccount BIT
				,numParentAccId NUMERIC(18,0)
			)

			;WITH CTE(tintLevel, numAccountId, vcAccountName, bitIsSubAccount, numParentAccId) AS
			(
				SELECT 
					0
					,numAccountId
					,vcAccountName
					,bitIsSubAccount
					,numParentAccId
				FROM 
					dbo.Chart_Of_Accounts 
				WHERE 
					numParntAcntTypeId = @numParntAcntTypeID 
					AND ISNULL(numParentAccId,0) = 0
				UNION ALL
				SELECT 
					c.tintLevel + 1
					,COA.numAccountId
					,COA.vcAccountName
					,COA.bitIsSubAccount
					,COA.numParentAccId
				FROM
					dbo.Chart_Of_Accounts  COA
				INNER JOIN
					CTE c
				ON
					COA.numParentAccId=C.numAccountId
				WHERE 
					numParntAcntTypeId = @numParntAcntTypeID 
			)

			INSERT INTO @TEMP
			(
				numAccountId
				,bitIsSubAccount
				,numParentAccId
			)
			SELECT
				numAccountId
				,bitIsSubAccount
				,numParentAccId
			FROM 
				CTE 
			ORDER BY 
				tintLevel,vcAccountName



			Declare @numTempAccountID NUMERIC
			Declare @bitTempIsSubAccount bit
			Declare @numTempParentAccId NUMERIC
			DECLARE @vcTempAccountCode VARCHAR(100)


			DECLARE @i AS INT  = 1
			DECLARE @iCount AS INT

			SELECT @iCount=COUNT(*) FROM @TEMP

			WHILE @i <= @iCount
			BEGIN
				SELECT @numTempAccountID=numAccountId,@bitTempIsSubAccount=bitIsSubAccount,@numTempParentAccId=numParentAccId  FROM @TEMP WHERE ID=@i
				

				IF @bitTempIsSubAccount =1 
					SELECT @vcTempAccountCode = dbo.GetAccountTypeCode(@numDomainId, @numTempParentAccId, 3)
				ELSE
					SELECT @vcTempAccountCode = dbo.GetAccountTypeCode(@numDomainId, @numParntAcntTypeID, 1)


				UPDATE dbo.Chart_Of_Accounts SET vcAccountCode=@vcTempAccountCode WHERE numAccountId=@numTempAccountID

				SET @i = @i + 1
			END
		END

		--Maintain only one profit and loss account through out system
		IF @bitProfitLoss = 1 
        BEGIN
            UPDATE  dbo.Chart_Of_Accounts
            SET     bitProfitLoss = 0
            WHERE   numAccountId <> @numAccountId
                    AND numDomainId = @numDomainID
        END
	COMMIT
	END TRY
	BEGIN CATCH
		DECLARE @ErrorMessage NVARCHAR(4000)
		DECLARE @ErrorNumber INT
		DECLARE @ErrorSeverity INT
		DECLARE @ErrorState INT
		DECLARE @ErrorLine INT
		DECLARE @ErrorProcedure NVARCHAR(200)

		IF @@TRANCOUNT > 0
			ROLLBACK TRANSACTION;

		SELECT 
			@ErrorMessage = ERROR_MESSAGE(),
			@ErrorNumber = ERROR_NUMBER(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE(),
			@ErrorLine = ERROR_LINE(),
			@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

		RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);
	END CATCH
END