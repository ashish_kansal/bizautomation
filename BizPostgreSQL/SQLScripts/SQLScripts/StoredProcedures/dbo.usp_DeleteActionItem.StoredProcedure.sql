/****** Object:  StoredProcedure [dbo].[usp_DeleteActionItem]    Script Date: 07/26/2008 16:15:26 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_deleteactionitem')
DROP PROCEDURE usp_deleteactionitem
GO
CREATE PROCEDURE [dbo].[usp_DeleteActionItem] 
        	@numQBCalID numeric(9)   
--
AS

--DELETE FROM QBActionItem where numQBCalID=@numQBCalID 
UPDATE QBActionItem set bitStatus=1 where numQBCalID=@numQBCalID
GO
