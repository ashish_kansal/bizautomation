/****** Object:  StoredProcedure [dbo].[usp_ManageDefaultTabName]    Script Date: 07/26/2008 16:19:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_managedefaulttabname')
DROP PROCEDURE usp_managedefaulttabname
GO
CREATE PROCEDURE [dbo].[usp_ManageDefaultTabName]  
@numDomainId as numeric,  
@Mode as numeric,  
@vcLead as varchar(50)='',  
@vcContact as varchar(50)='',  
@vcProspect as varchar(50)='',  
@vcAccount as varchar(50)='',
@vcLeadPlural as varchar(50)='',  
@vcContactPlural as varchar(50)='',  
@vcProspectPlural as varchar(50)='',  
@vcAccountPlural as varchar(50)=''
  
as  
--Insert into table  
if @Mode =1   
begin  
 if (select count(*) from DefaultTabs where numDomainId = @numDomainId) > 0  
  begin   
   update DefaultTabs set   
   vcLead=@vcLead,
   vcContact=@vcContact,
   vcProspect=@vcProspect,
   vcAccount=@vcAccount,
   vcLeadPlural=@vcLeadPlural,
   vcContactPlural=@vcContactPlural,
   vcProspectPlural=@vcProspectPlural,
   vcAccountPlural=@vcAccountPlural  
   where  numDomainId = @numDomainId  
  end  
 else  
  begin  
   insert into DefaultTabs   
   (numDomainId,vcLead,vcContact,vcProspect,vcAccount)  
      values  
   (@numDomainId,@vcLead,@vcContact,@vcProspect,@vcAccount)  
  end   
End  
  
--delete 
if @Mode =2  
begin  
 delete DefaultTabs   
 where numDomainId = @numDomainId  
End
GO
