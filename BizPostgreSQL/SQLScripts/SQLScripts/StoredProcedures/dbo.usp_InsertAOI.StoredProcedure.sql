/****** Object:  StoredProcedure [dbo].[usp_InsertAOI]    Script Date: 07/26/2008 16:18:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
--modified by anoop jayaraj
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_insertaoi')
DROP PROCEDURE usp_insertaoi
GO
CREATE PROCEDURE [dbo].[usp_InsertAOI]  
 @vcAOIName varchar(50),
 @numUserID numeric,  
 @numDomainID numeric      
--  
AS  
BEGIN  
 INSERT INTO AOIMaster Values(@vcAOIName, @numDomainID,   
   @numUserID, getutcdate(), NULL, NULL, 0)  
  
END
GO
