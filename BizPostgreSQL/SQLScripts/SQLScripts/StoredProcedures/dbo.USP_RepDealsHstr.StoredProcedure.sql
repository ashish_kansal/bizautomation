/****** Object:  StoredProcedure [dbo].[USP_RepDealsHstr]    Script Date: 07/26/2008 16:20:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- created by anoop jayaraj               
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_repdealshstr')
DROP PROCEDURE usp_repdealshstr
GO
CREATE PROCEDURE [dbo].[USP_RepDealsHstr]                   
  @numDomainID numeric,                    
  @numFromDate datetime,                    
  @numToDate datetime,                    
  @numUserCntID numeric=0,                         
  @tintType numeric,             
  @tintSortOrder tinyint,            
  @tintRights tinyint,          
  @DivisionID as numeric(9)                
as                    
declare @strSql as varchar(8000)                    
set @strSql='SELECT  Opp.numOppId,                     
  Opp.vcPOppName AS Name,                     
  dbo.GetOppStatus(Opp.numOppId) as STAGE,                     
  Opp.intPEstimatedCloseDate AS CloseDate,                     
  isnull(Opp.numcreatedby,0) ,                    
  ADC.vcFirstName + '' '' + ADC.vcLastName AS Contact,                     
  C.vcCompanyName AS Company,                    
  C.numCompanyID, case when opp.tintOppType=1 then ''Sales'' when opp.tintOppType=2 then ''Purchase'' end as [Type],                
  Div.tintCRMType,                    
  ADC.numContactID,                    
  Div.numDivisionID,                    
  Div.numTerID,                    
  Opp.monPAmount,                    
  ADC1.vcFirstName+'' ''+ADC1.vcLastName  vcUserName                    
  FROM OpportunityMaster Opp                     
  INNER JOIN AdditionalContactsInformation ADC                     
  ON Opp.numContactId = ADC.numContactId                     
  INNER JOIN DivisionMaster Div                     
  ON Opp.numDivisionId = Div.numDivisionID AND ADC.numDivisionId = Div.numDivisionID                     
  INNER JOIN CompanyInfo C                     
  ON Div.numCompanyID = C.numCompanyId   
  join    AdditionalContactsInformation  ADC1    
  on ADC1.numContactId= Opp.numRecOwner  
  WHERE Opp.tintOppstatus<>0 and Opp.tintOppstatus<>3 and Opp.bintCreatedDate Between '''+ convert(varchar(20),@numFromDate) +''' And '''+ convert(varchar(20),@numToDate)+''''                      
  if (@tintSortOrder=1 or  @tintSortOrder=2)  set @strSql=@strSql + ' and Opp.tintOppStatus<>2 AND Opp.tintOppType= '+ convert(varchar(1),@tintSortOrder)                    
  if @tintSortOrder=3  set @strSql=@strSql + ' and Opp.tintOppStatus=2 AND Opp.tintOppType= 1'            
  if @tintSortOrder=4  set @strSql=@strSql + ' and Opp.tintOppStatus=2 AND Opp.tintOppType= 2'           
  if @tintSortOrder=5  set @strSql=@strSql + ' and Opp.numDivisionID='+ convert(varchar(15),@DivisionID)           
  if @tintRights=1 set  @strSql=@strSql + ' and Opp.numRecOwner= '+ convert(varchar(15),@numUserCntID)                    
  if @tintRights=2 set  @strSql=@strSql + ' and ADC1.numTeam in(select F.numTeam from ForReportsByTeam F                 
  where F.numuserCntid='+convert(varchar(15),@numUserCntID)+' and F.numDomainid='+convert(varchar(15),@numDomainId)+' and F.tintType='+convert(varchar(5),@tintType)+')'            
  if @tintRights=3 set  @strSql=@strSql + ' and Div.numTerId in(select F.numTerritory from ForReportsByTerritory F                        
  where F.numuserCntid='+convert(varchar(15),@numUserCntID)+' and F.numDomainid='+convert(varchar(15),@numDomainID)+' and F.tintType='+convert(varchar(5),@tintType)+')'                 
print @strSQL
 exec  (@strSql)
GO
