/****** Object:  StoredProcedure [dbo].[USP_ActItemAlert]    Script Date: 07/26/2008 16:14:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--exec USP_ActItemAlert @numDomainID = 1, @numUserID = 1                    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_actitemalert')
DROP PROCEDURE usp_actitemalert
GO
CREATE PROCEDURE [dbo].[USP_ActItemAlert]                    
@numDomainID as numeric(9)=null,                    
@numUserCntID as numeric(9)=null,              
@ClientTimeZoneOffset Int                 
                    
as                    
                    
declare @GetDate as datetime                     
select @GetDate=getutcdate()                  
                  
 SELECT  com.numCommId AS Id,                       
  AddC.numContactId,                           
  Comp.numCompanyId,                          
  Div.numDivisionID                 
 FROM  Communication Com                    
 JOIN  AdditionalContactsInformation AddC                    
 ON com.numContactId = AddC.numContactId                     
 JOIN  DivisionMaster Div                    
 ON AddC.numDivisionId = Div.numDivisionID                     
 JOIN   CompanyInfo Comp                    
 ON Div.numCompanyID = Comp.numCompanyId                        
 where com.bitclosedflag=0                                        
 and intSnoozeMins<>0                    
 and com.numAssign =@numUserCntID                   
 and tintSnoozeStatus=1 and DateAdd(minute, intSnoozeMins, dtEndTime)  <=@GetDate                    
union                   
 SELECT  com.numCommId AS Id,                        
  AddC.numContactId,                           
  Comp.numCompanyId,                          
  Div.numDivisionID                    
 FROM  Communication Com                    
 JOIN  AdditionalContactsInformation AddC                    
 ON com.numContactId = AddC.numContactId                     
 JOIN  DivisionMaster Div                    
 ON AddC.numDivisionId = Div.numDivisionID                     
 JOIN   CompanyInfo Comp                    
 ON Div.numCompanyID = Comp.numCompanyId                        
 where com.bitclosedflag=0                                            
 and intRemainderMins<>0                    
 and com.numAssign = @numUserCntID                  
 and tintRemStatus=1 and DateAdd(minute, -intRemainderMins, dtStartTime)  <=@GetDate
GO
