/****** Object:  StoredProcedure [dbo].[USP_Solution_Manage]    Script Date: 07/26/2008 16:21:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_solution_manage')
DROP PROCEDURE usp_solution_manage
GO
CREATE PROCEDURE [dbo].[USP_Solution_Manage]
@numCaseID as numeric(9)=null,
@numSolnID as numeric(9)=NULL,
@mode AS BIT
as

IF @mode = 0 
BEGIN
	IF NOT EXISTS (SELECT * FROM CaseSolutions where numCaseID=@numCaseID and numSolnID=@numSolnID)
	BEGIN
		insert into CaseSolutions (numCaseID,numSolnID)
		values (@numCaseID,@numSolnID)	
	END
END

IF @mode = 1 
BEGIN
	delete from CaseSolutions where numCaseID=@numCaseID and numSolnID=@numSolnID	
END