/****** Object:  StoredProcedure [dbo].[Usp_UpdateScheduledOccurance]    Script Date: 07/26/2008 16:21:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_updatescheduledoccurance')
DROP PROCEDURE usp_updatescheduledoccurance
GO
CREATE PROCEDURE [dbo].[Usp_UpdateScheduledOccurance]
@numScheduleId as numeric(9)
as


declare @Transactiontype bit
declare @DtEndDate datetime
declare @noTransaction numeric
declare @noTracsactioncmp numeric
Declare @DateDiff as integer    

select @Transactiontype=bitEndTransactionType,@DtEndDate=isnull(dtEndDate,'Jan  1 1753 12:00AM'),
@noTransaction=isnull(numNoTransaction,0),@noTracsactioncmp=isnull(numNoTransactionCmp,0) 
from CustRptScheduler
where numScheduleid = @numScheduleId

print @Transactiontype
print @DtEndDate
print @noTransaction
print @noTracsactioncmp+1

if @Transactiontype = 0 --indicates its enddate tyep
	begin
		 Set @DateDiff = datediff (day,@DtEndDate,getutcdate())  
		 if @DateDiff >=0
			Begin
				print @DateDiff
				print 'expired'
				update CustRptScheduler set
					bitrecurring = 1 ,
					LastRecurringDate= getutcdate()
				where numScheduleid = @numScheduleId
			End
		 else
			Begin
				print 'recurring'
				update CustRptScheduler set					
					LastRecurringDate= getutcdate()
				where numScheduleid = @numScheduleId
			End
	end
else				--indicates its incidents type
	begin
		if @noTracsactioncmp+1 >= @noTransaction
				Begin
				print 'expired'
				update CustRptScheduler set
					 bitrecurring =1,
					 LastRecurringDate= getutcdate(),
					 numNoTransactionCmp = @noTracsactioncmp+1
				where numScheduleid = @numScheduleId
			End
		 else
			Begin
				print 'recurring'
				update CustRptScheduler set					 
					 LastRecurringDate= getutcdate(),
					 numNoTransactionCmp = @noTracsactioncmp+1
				where numScheduleid = @numScheduleId
			End
	end
GO
