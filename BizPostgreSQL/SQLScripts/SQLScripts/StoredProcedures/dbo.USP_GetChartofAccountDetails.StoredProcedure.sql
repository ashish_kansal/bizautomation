/****** Object:  StoredProcedure [dbo].[USP_GetChartofAccountDetails]    Script Date: 07/26/2008 16:16:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created by Siva     
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getchartofaccountdetails')
DROP PROCEDURE usp_getchartofaccountdetails
GO
CREATE PROCEDURE  [dbo].[USP_GetChartofAccountDetails]        
@numAcntId as numeric(9)=0,      
@numDomainId as numeric(9)=0        
as    
Begin      
 Select monEndingBal as monEndingBal,case when monEndingOpeningBal is null then  numOriginalOpeningBal else monEndingOpeningBal End  as monEndingOpeningBal
From Chart_Of_Accounts Where numAccountId=@numAcntId and numDomainId=@numDomainId      
End
GO
