/****** Object:  StoredProcedure [dbo].[USP_GetOpenCases]    Script Date: 07/26/2008 16:17:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getopencases')
DROP PROCEDURE usp_getopencases
GO
CREATE PROCEDURE [dbo].[USP_GetOpenCases]     
@numDivisionID as numeric(9)     
as      
select numCaseId AS numCaseId1,ISNULL(vcCaseNumber,'')+', ' +ISNULL(dbo.fn_GetContactName(numCreatedBy),'') +', ' + ISNULL(convert(varchar(20),bintCreatedDate),'')+', ' + ISNULL(convert(varchar,textSubject),'') as vcCaseNumber,
convert(varchar(15),numCaseId) +'~' +convert(varchar(15),numDivisionID)+ '~'+convert(varchar(15),numContactId) as numCaseId
from Cases where numStatus<>36  and numDivisionID=@numDivisionID
GO
