--Created By Anoop Jayaraj

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getwarehouseforsf')
DROP PROCEDURE usp_getwarehouseforsf
GO
CREATE PROCEDURE USP_GetWarehouseForSF
@numDomainID as numeric(9)=0
as


select numWareHouseID,vcWareHouse from Warehouses W
where numDomainID=@numDomainID



select WS.numWareHouseID,vcWareHouse from Warehouses W
Join WareHouseForSalesFulfillment WS
on WS.numWareHouseID=W.numWareHouseID
where WS.numDomainID=@numDomainID