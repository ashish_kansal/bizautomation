/****** Object:  StoredProcedure [dbo].[usp_GetTop5SalesRep]    Script Date: 07/26/2008 16:18:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_gettop5salesrep')
DROP PROCEDURE usp_gettop5salesrep
GO
CREATE PROCEDURE [dbo].[usp_GetTop5SalesRep]              
 @numDomainID numeric,              
 @dtDateFrom datetime,              
 @dtDateTo datetime,              
 @numTerID numeric=0,              
 @tintRights tinyint=3,    --ByDefault show all records.              
 @intType numeric=0,              
 @numUserCntId numeric              
--              
AS              
BEGIN              
 IF @tintRights=3  --ALL RECORDS (As top 5 Sales people cannot be divided as per the Owner records)              
 BEGIN              
   IF @intType = 0               
   BEGIN           
          
      select top 5 A.vcFirstName+' '+A.vcLastName as [Name],sum(monPAmount) as Amount  from OpportunityMaster OM             
  join AdditionalContactsInformation A on A.numContactId=OM.numRecOwner           
  where OM.tintOppStatus=1              
      AND OM.numDomainID=@numDomainID                
      AND OM.bintAccountClosingDate BETWEEN @dtDateFrom AND @dtDateTo                
      group by A.vcFirstName,A.vcLastName order by Amount desc             
               
   END              
   ELSE              
   BEGIN              
             
 select top 5 A.vcFirstName+' '+A.vcLastName as [Name],sum(monPAmount) as Amount  from OpportunityMaster OM               
 join AdditionalContactsInformation A on A.numContactId=OM.numRecOwner           
 where OM.tintOppStatus=1              
     AND OM.numDomainID=@numDomainID                
     AND OM.bintAccountClosingDate BETWEEN @dtDateFrom AND @dtDateTo                
     AND OM.numRecOwner=@numUserCntId          
     AND A.numTeam is not null                   
     AND A.numTeam in(select F.numTeam from ForReportsByTeam F                   
     where F.numUserCntID=@numUserCntId and F.numDomainid=@numDomainId and F.tintType=@intType)            
     group by A.vcFirstName,A.vcLastName  order by Amount desc              
            
   END              
 END              
 IF @tintRights=2 --TERRITORY RECORDS              
 BEGIN              
   IF @intType = 0              
   BEGIN              
  select top 5 A.vcFirstName+' '+A.vcLastName as [Name],sum(monPAmount) as Amount  from OpportunityMaster OM             
    join AdditionalContactsInformation A on A.numContactId=OM.numRecOwner              
    JOIN DivisionMaster DM            
    ON DM.numDivisionID=OM.numDivisionID              
    WHERE OM.tintOppStatus=1              
     AND OM.numDomainID=@numDomainID              
     AND OM.bintAccountClosingDate BETWEEN @dtDateFrom AND @dtDateTo              
     AND DM.numTerId in (Select numTerritoryId FROM UserTerritory WHERE numUserCntId=@numUserCntId)           
    group by A.vcFirstName,A.vcLastName order by Amount desc              
            
   END              
   ELSE              
   BEGIN                 
   select top 5 A.vcFirstName+' '+A.vcLastName as [Name],sum(monPAmount) as Amount  from OpportunityMaster OM               
 join AdditionalContactsInformation A on A.numContactId=OM.numRecOwner              
    JOIN DivisionMaster DM            
    ON DM.numDivisionID=OM.numDivisionID              
    WHERE OM.tintOppStatus=1              
     AND OM.numDomainID=@numDomainID              
     AND OM.bintAccountClosingDate BETWEEN @dtDateFrom AND @dtDateTo              
     AND DM.numTerId in (Select numTerritoryId FROM UserTerritory WHERE numUserCntId=@numUserCntId)           
    AND A.numTeam is not null                   
     AND A.numTeam in(select F.numTeam from ForReportsByTeam F                   
     where F.numuserCntId=@numUserCntId and F.numDomainid=@numDomainId and F.tintType=@intType)           
    group by A.vcFirstName,A.vcLastName order by Amount desc               
            
                 
   END              
 END              
            
            
 IF @tintRights=1  --ALL RECORDS (As top 5 Sales people cannot be divided as per the Owner records)              
 BEGIN              
   IF @intType = 0               
   BEGIN              
  select top 5 A.vcFirstName+' '+A.vcLastName as [Name],sum(monPAmount) as Amount   from OpportunityMaster OM          
 join AdditionalContactsInformation A on A.numContactId=OM.numRecOwner           
 where OM.tintOppStatus=1              
     AND OM.numDomainID=@numDomainID          
     and OM.numRecOwner=@numUserCntId                
     AND OM.bintAccountClosingDate BETWEEN @dtDateFrom AND @dtDateTo                
   group by A.vcFirstName,A.vcLastName order by Amount desc              
   END              
   ELSE              
   BEGIN              
  select top 5 A.vcFirstName+' '+A.vcLastName as [Name],sum(monPAmount) as Amount  from OpportunityMaster OM               
 join AdditionalContactsInformation A on A.numContactId=OM.numRecOwner           
 where OM.tintOppStatus=1              
     AND OM.numDomainID=@numDomainID          
        and OM.numRecOwner=@numUserCntId                
     AND OM.bintAccountClosingDate BETWEEN @dtDateFrom AND @dtDateTo           
        AND A.numTeam is not null                   
     AND A.numTeam in(select F.numTeam from ForReportsByTeam F                   
     where F.numuserCntId=@numUserCntId and F.numDomainid=@numDomainId and F.tintType=@intType)               
     group by A.vcFirstName,A.vcLastName order by Amount desc              
            
   END              
 END              
            
            
              
END
GO
