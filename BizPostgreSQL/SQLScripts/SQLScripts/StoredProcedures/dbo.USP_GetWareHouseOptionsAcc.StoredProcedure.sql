/****** Object:  StoredProcedure [dbo].[USP_GetWareHouseOptionsAcc]    Script Date: 07/26/2008 16:18:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--exec USP_GetWareHouseOptionsAcc '6,7'                  
--created by anoop jayaraj                    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getwarehouseoptionsacc')
DROP PROCEDURE usp_getwarehouseoptionsacc
GO
CREATE PROCEDURE [dbo].[USP_GetWareHouseOptionsAcc]                    
@numItemCode as numeric(9)=0                  
as                    
select vcItemName,numItemCode from  Item              
where numItemcode in(select numOppAccAttrID from  ItemGroupsDTL         
join Item on Item.numItemGroup=numItemGroupID where         
numItemCode =@numItemCode and tintType=1)
GO
