/****** Object:  StoredProcedure [dbo].[usp_GetCaseDetailsForAccounthstr]    Script Date: 07/26/2008 16:16:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--modified by anoop jayaraj  
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getcasedetailsforaccounthstr')
DROP PROCEDURE usp_getcasedetailsforaccounthstr
GO
CREATE PROCEDURE [dbo].[usp_GetCaseDetailsForAccounthstr]      
 @numDivisionID numeric(9)=0         
--      
AS      
 SELECT     Cases.numCaseid,Cases.vcCaseNumber, intTargetResolveDate, Cases.textSubject, Cases.numStatus,dbo.fn_GetListItemName(Cases.numStatus) AS vcCaseStatus , Cases.textDesc,       
                      Cases.textInternalComments, Cases.numCreatedBy, DivisionMaster.numDivisionID      
FROM         AdditionalContactsInformation INNER JOIN      
                      Cases ON AdditionalContactsInformation.numContactId = Cases.numContactId INNER JOIN      
                      DivisionMaster ON AdditionalContactsInformation.numDivisionId = DivisionMaster.numDivisionID      
WHERE DivisionMaster.numDivisionID=@numDivisionID and Cases.numstatus=136
GO
