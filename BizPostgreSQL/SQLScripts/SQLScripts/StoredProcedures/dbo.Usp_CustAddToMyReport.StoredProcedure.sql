/****** Object:  StoredProcedure [dbo].[Usp_CustAddToMyReport]    Script Date: 07/26/2008 16:15:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_custaddtomyreport')
DROP PROCEDURE usp_custaddtomyreport
GO
CREATE PROCEDURE [dbo].[Usp_CustAddToMyReport]
@numReportId as numeric(9),
@numUserCntId as numeric(9),
@numDomainId as numeric(9)
as

declare @tintRptSequence as tinyint    
select @tintRptSequence=isnull(max(tintRptSequence),0)from MyReportList where  numUserCntID=@numUserCntID    
set @tintRptSequence=@tintRptSequence+1    
delete from MyReportList where numRPTId=@numReportId and numUserCntID=@numUserCntID  and tintType =1  
insert into MyReportList(numRPTId,numUserCntID,tintRptSequence,tintType)    
values(@numReportId,@numUserCntID,@tintRptSequence,1)
GO
