/****** Object:  StoredProcedure [dbo].[USP_GetPriceBookRule]    Script Date: 07/26/2008 16:18:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj    
-- exec USP_GetPriceBookRule @numPricRuleID=353,@byteMode=0,@numDomainID=0,@bitSort=0
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getpricebookrule')
DROP PROCEDURE usp_getpricebookrule
GO
CREATE PROCEDURE [dbo].[USP_GetPriceBookRule]
    @numPricRuleID AS NUMERIC(9) = 0,
    @byteMode AS TINYINT,
    @numDomainID AS NUMERIC(9) = 0,
    @bitSort AS BIT,
    @numItemCode AS NUMERIC(9)=0,
    @numDivisionID AS NUMERIC(9)=0
AS 
    IF @byteMode = 0 
        BEGIN    
            SELECT  [numPricRuleID],
                    [vcRuleName],
                    ISNULL([tintRuleType], 0) tintRuleType,
                    CASE WHEN [tintDiscountType] = 1 THEN  CAST(CONVERT(DECIMAL(9, 2), ISNULL(decDiscount,0)) AS Varchar)
                    ELSE CAST(CONVERT(int,ISNULL(decDiscount,0)) AS VARCHAR) END  AS decDiscount,
                    CASE WHEN [intQntyItems] = 0 THEN 1
                    ELSE intQntyItems
                    END AS intQntyItems,
                    [decMaxDedPerAmt],
                    [numDomainID],
                    ISNULL([vcRuleDescription], '') vcRuleDescription,
                    ISNULL([tintDiscountType], 0) tintDiscountType,
                    ISNULL([tintPricingMethod], 0) tintPricingMethod,
                    ISNULL([tintStep2], 0) tintStep2,
                    ISNULL([tintStep3], 0) tintStep3,
                    CASE ISNULL([tintStep2], 0)
                    WHEN 1 THEN (SELECT COUNT(*) FROM [PriceBookRuleItems] PD WHERE PD.[numRuleID] = [numPricRuleID] AND PD.tintType=1)
                    WHEN 2 THEN (SELECT COUNT(*) FROM [PriceBookRuleItems] PD WHERE PD.[numRuleID] = [numPricRuleID] AND PD.tintType=2)
                    END AS Step2Count,
                    CASE ISNULL([tintStep3], 0)
                    WHEN 1 THEN (SELECT COUNT(*) FROM [PriceBookRuleDTL] PD WHERE PD.[numRuleID] = [numPricRuleID] AND PD.tintType=3)
                    WHEN 2 THEN (SELECT COUNT(*) FROM [PriceBookRuleDTL] PD WHERE PD.[numRuleID] = [numPricRuleID] AND PD.tintType=4)
                    END AS Step3Count,tintRuleFor
            FROM    PriceBookRules
            WHERE   numPricRuleID = @numPricRuleID    
    
        END    
    ELSE 
        IF @byteMode = 1
            AND @bitSort = 1 
            BEGIN    
                SELECT  numPricRuleID,
                        vcRuleName,
                        'Deduct '
                        + CASE WHEN tintDiscountType = 0
                               THEN CONVERT(VARCHAR(20), decDiscount) + '%'
                               ELSE CONVERT(VARCHAR(20), CAST(decDiscount AS int))
                          END + ' for every '
                        + CONVERT(VARCHAR(20), CASE WHEN [intQntyItems] = 0
                                                    THEN 1
                                                    ELSE intQntyItems
                                               END)
                        + ' units, until maximum of '
                        + CONVERT(VARCHAR(20), decMaxDedPerAmt) + ' %' AS PricingRule,
                        ISNULL(PP.[Priority], 0) Priority,
                        CASE WHEN tintStep2 = 3 THEN 'All Items'
                             ELSE dbo.GetPriceBookItemList([numPricRuleID],
                                                           [tintStep2])
                        END ItemList,
                        CASE WHEN tintStep3 = 3 THEN 'All Customers'
                             ELSE dbo.GetPriceBookItemList([numPricRuleID],
                                                           CASE [tintStep3]
                                                             WHEN 1 THEN 3
                                                             WHEN 2 THEN 4
                                                           END)
                        END CustomerList,CASE WHEN PB.tintRuleFor=2 THEN 'Purchase' ELSE 'Sale' END AS  vcRuleFor
                FROM    PriceBookRules PB
                        LEFT OUTER JOIN [PriceBookPriorities] PP ON PP.[Step2Value] = PB.[tintStep2]
                                                                    AND PP.[Step3Value] = PB.[tintStep3]
                WHERE   numDomainID = @numDomainID AND 1=(CASE WHEN @numItemCode>0 THEN 
              CASE WHEN PB.numPricRuleID IN (SELECT * FROM dbo.fn_GetItemPriceRule(@numDomainID,2,@numItemCode,@numDivisionID)) THEN 1 ELSE 0 END ELSE 1 END)
                ORDER BY PP.[Priority],vcRuleName ASC
            END
        ELSE 
            IF @byteMode = 1
                AND @bitSort = 0 
                BEGIN
                    SELECT  numPricRuleID,
                            vcRuleName,
                            CASE WHEN [tintPricingMethod] = 1
                                 THEN '<a href="#" onclick="return PriceTable('
                                      + CONVERT(VARCHAR, numPricRuleID)
                                      + ')">' + Case PB.tintRuleFor when 2 then 'Costing Table' else 'Pricing Table' end
                                 WHEN [tintPricingMethod] = 2 then  CASE tintRuleType
                                        WHEN 1 THEN 'Deduct from List price '
                                        WHEN 2 THEN 'Add to primary vendor cost '
                                        ELSE ''
                                      END
                                      + CASE WHEN tintDiscountType = 1
                                             THEN CONVERT(VARCHAR(20), decDiscount)
                                                  + '%'
                                             ELSE 'flat ' + CONVERT(VARCHAR(20), CAST(decDiscount AS int))
                                        END + ' for every '
                                      + CONVERT(VARCHAR(20), CASE WHEN [intQntyItems] = 0 THEN 1
                                                                  ELSE intQntyItems
                                                             END)
                                      + ' units, until maximum of '
                                      + CONVERT(VARCHAR(20), decMaxDedPerAmt)
                                      + CASE WHEN tintDiscountType = 1 THEN ' %' ELSE ' Flat'  END 
							else ''
                            END AS PricingRule,
                            ISNULL(PP.[Priority], 0) Priority,
                            CASE WHEN tintStep2 = 3 THEN 'All Items'
                                 ELSE dbo.GetPriceBookItemList([numPricRuleID], [tintStep2])
                            END ItemList,
                            CASE WHEN tintStep3 = 3 THEN 'All Customers'
                                 ELSE dbo.GetPriceBookItemList([numPricRuleID], CASE [tintStep3]
                                                                                  WHEN 1 THEN 3
                                                                                  WHEN 2 THEN 4
                                                                                END)
                            END CustomerList,CASE WHEN PB.tintRuleFor=2 THEN 'Purchase' ELSE 'Sale' END AS  vcRuleFor
                    FROM    PriceBookRules PB
                            LEFT OUTER JOIN [PriceBookPriorities] PP ON PP.[Step2Value] = PB.[tintStep2]
                                                                        AND PP.[Step3Value] = PB.[tintStep3]
                    WHERE   numDomainID = @numDomainID AND 1=(CASE WHEN @numItemCode>0 THEN 
							CASE WHEN PB.numPricRuleID IN (SELECT * FROM dbo.fn_GetItemPriceRule(@numDomainID,2,@numItemCode,@numDivisionID)) THEN 1 ELSE 0 END ELSE 1 END)
                    ORDER BY PP.[Priority],vcRuleName DESC
                END
GO
