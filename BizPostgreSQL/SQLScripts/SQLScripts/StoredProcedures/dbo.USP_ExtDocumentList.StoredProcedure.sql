/****** Object:  StoredProcedure [dbo].[USP_ExtDocumentList]    Script Date: 07/26/2008 16:15:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj          
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'usp_extdocumentlist' ) 
    DROP PROCEDURE usp_extdocumentlist
GO
CREATE PROCEDURE [dbo].[USP_ExtDocumentList]
    @numDivisionID AS NUMERIC(9) = 0,
    @numSearchCondition AS NUMERIC(9) = 0,
    @cSection CHAR(1) = '',
    @SortChar CHAR(1) = '0',
    @CurrentPage INT,
    @PageSize INT,
    @TotRecs INT OUTPUT,
    @columnName AS VARCHAR(50),
    @columnSortOrder AS VARCHAR(10),
    @tintMode AS TINYINT = 0,
    @numUserCntID NUMERIC(9) = 0
AS 
    IF @tintMode = 0 
        BEGIN

            CREATE TABLE #tempTable
                (
                  ID INT IDENTITY
                         PRIMARY KEY,
                  VcFileName VARCHAR(100),
                  vcDocName VARCHAR(100),
                  Category VARCHAR(100),
                  CreatedDate VARCHAR(100),
                  [Date] VARCHAR(15),
                  Modified VARCHAR(150),
                  [Section] VARCHAR(50),
                  [Name] VARCHAR(100),
                  vcFileType VARCHAR(10),
                  numGenericDocID VARCHAR(15),
                  numRecID VARCHAR(15),
                  vcDocumentSection VARCHAR(1),
                  numDocCategory VARCHAR(15),
                  Approved INT,
                  Declined INT,
                  Pending INT
                )           
          
            DECLARE @strSql AS VARCHAR(8000)           
          
            SET @strSql = 'select * from (select VcFileName,vcDocName,          
dbo.fn_GetListItemName(numDocCategory) as Category,          
GenericDocuments.bintCreatedDate  as CreatedDate,          
GenericDocuments.bintCreatedDate as [Date],          
convert(varchar(20),GenericDocuments.bintModifiedDate)+'' ''+dbo.fn_GetContactName(GenericDocuments.numModifiedBy) as Modified,          
''Organization'' as [Section],dbo.fn_GetComapnyName(numRecID) as [Name],          
vcFileType,numGenericDocID,numRecID,vcDocumentSection,numDocCategory,
(select count(*) from DocumentWorkflow  where numDocID=numGenericDocID and cDocType=vcDocumentSection and tintApprove=1) as Approved,
(select count(*) from DocumentWorkflow where numDocID=numGenericDocID and cDocType=vcDocumentSection and tintApprove=2) as Declined,
(select count(*) from DocumentWorkflow where numDocID=numGenericDocID and cDocType=vcDocumentSection and tintApprove=0) as Pending
from GenericDocuments          
where vcDocumentSection=''A'' and numRecID='
                + CONVERT(VARCHAR(15), @numDivisionID)
                + '          
union           
select VcFileName,vcDocName,          
dbo.fn_GetListItemName(numDocCategory) as Category,          
GenericDocuments.bintCreatedDate as CreatedDate,          
GenericDocuments.bintCreatedDate as [Date],          
convert(varchar(20),GenericDocuments.bintModifiedDate)+'' ''+dbo.fn_GetContactName(GenericDocuments.numModifiedBy) as Modified,          
''Contact'' as [Section],vcFirstName+'' ''+VcLastName as [Name],          
vcFileType,numGenericDocID,numRecID,vcDocumentSection,numDocCategory,
(select count(*) from DocumentWorkflow  where numDocID=numGenericDocID and cDocType=vcDocumentSection and tintApprove=1) as Approved,
(select count(*) from DocumentWorkflow where numDocID=numGenericDocID and cDocType=vcDocumentSection and tintApprove=2) as Declined,
(select count(*) from DocumentWorkflow where numDocID=numGenericDocID and cDocType=vcDocumentSection and tintApprove=0) as Pending
from GenericDocuments          
join AdditionalContactsInformation          
on numRecID=numContactID          
where vcDocumentSection=''C'' and numDivisionID='
                + CONVERT(VARCHAR(15), @numDivisionID)
                + '          
union           
select VcFileName,vcDocName,          
dbo.fn_GetListItemName(numDocCategory) as Category,          
GenericDocuments.bintCreatedDate as CreatedDate,          
GenericDocuments.bintCreatedDate as [Date],          
convert(varchar(20),GenericDocuments.bintModifiedDate)+'' ''+dbo.fn_GetContactName(GenericDocuments.numModifiedBy) as Modified,          
''Opportunity'' as [Section],vcPOppName as [Name],          
vcFileType,numGenericDocID,numRecID,vcDocumentSection,numDocCategory,
(select count(*) from DocumentWorkflow  where numDocID=numGenericDocID and cDocType=vcDocumentSection and tintApprove=1) as Approved,
(select count(*) from DocumentWorkflow where numDocID=numGenericDocID and cDocType=vcDocumentSection and tintApprove=2) as Declined,
(select count(*) from DocumentWorkflow where numDocID=numGenericDocID and cDocType=vcDocumentSection and tintApprove=0) as Pending
from GenericDocuments          
join OpportunityMaster          
on numRecID=numOppID          
where vcDocumentSection=''O'' and numDivisionID='
                + CONVERT(VARCHAR(15), @numDivisionID)
                + '          
union           
select VcFileName,vcDocName,          
dbo.fn_GetListItemName(numDocCategory) as Category,          
GenericDocuments.bintCreatedDate as CreatedDate,           
GenericDocuments.bintCreatedDate as [Date],          
convert(varchar(20),GenericDocuments.bintModifiedDate)+'' ''+dbo.fn_GetContactName(GenericDocuments.numModifiedBy) as Modified,          
''Project'' as [Section],vcProjectName as [Name],          
vcFileType,numGenericDocID,0 as numOppBizDocsId,vcDocumentSection,numDocCategory,
(select count(*) from DocumentWorkflow  where numDocID=numGenericDocID and cDocType=vcDocumentSection and tintApprove=1) as Approved,
(select count(*) from DocumentWorkflow where numDocID=numGenericDocID and cDocType=vcDocumentSection and tintApprove=2) as Declined,
(select count(*) from DocumentWorkflow where numDocID=numGenericDocID and cDocType=vcDocumentSection and tintApprove=0) as Pending
from GenericDocuments          
join ProjectsMaster          
on numRecID=numProID          
where vcDocumentSection=''P'' and numDivisionID='
                + CONVERT(VARCHAR(15), @numDivisionID)
                + '          
union          
select '''' as VcFileName, vcBizDocID  as vcDocName,          
dbo.fn_GetListItemName(numBizDocId) as Category,          
dtCreatedDate as CreatedDate,          
dtCreatedDate as [Date],          
convert(varchar(20),Biz.dtModifiedDate)+'' ''+dbo.fn_GetUserName(Biz.numModifiedBy) as Modified,          
''BizDocs'' as [Section],vcPOppName as [Name],''-'' as vcFileType,          
Biz.numOppId as numGenericDocID,numOppBizDocsId as numRecID,''B'' as vcDocumentSection, numBizDocID as numDocCategory,
0 Approved,
0 Declined,
0 Pending
from OpportunityBizDocs Biz          
join OpportunityMaster Opp          
on Opp.numOppId=Biz.numOppId          
where numDivisionID=' + CONVERT(VARCHAR(15), @numDivisionID) + ')X Where 1=1'          
          
            IF @SortChar <> '0' 
                SET @strSql = @strSql + ' And X.vcDocName like ''' + @SortChar
                    + '%'''            
            IF @numSearchCondition <> 0 
                SET @strSql = @strSql + 'and X.numDocCategory='
                    + CONVERT(VARCHAR(15), @numSearchCondition)          
            IF @cSection <> '0' 
                SET @strSql = @strSql + 'and X.vcDocumentSection='''
                    + @cSection + ''''          
            SET @strSql = @strSql + ' ORDER BY ' + @columnName + ' '
                + @columnSortOrder          
          
          
            PRINT @strSql          
          
            INSERT  INTO #tempTable
                    (
                      VcFileName,
                      vcDocName,
                      Category,
                      CreatedDate,
                      [Date],
                      Modified,
                      [Section],
                      [Name],
                      vcFileType,
                      numGenericDocID,
                      numRecID,
                      vcDocumentSection,
                      numDocCategory,
                      Approved,
                      Declined,
                      Pending
                    )
                    EXEC ( @strSql
                        )               
              
            DECLARE @firstRec AS INTEGER               
            DECLARE @lastRec AS INTEGER              
            SET @firstRec = ( @CurrentPage - 1 ) * @PageSize              
            SET @lastRec = ( @CurrentPage * @PageSize + 1 )              
            SELECT  *
            FROM    #tempTable
            WHERE   ID > @firstRec
                    AND ID < @lastRec              
            SET @TotRecs = ( SELECT COUNT(*)
                             FROM   #tempTable
                           )              
            DROP TABLE #tempTable
        END 


-- Get All Pending documents which requires users approval / declined 
    IF @tintMode = 1 
        BEGIN
            SELECT  numDocID,
                    numContactID,
                    tintApprove,
                    dtApprovedOn,
                    cDocType,
                    vcComment,
                    dtCreatedDate,
                    SD.vcDocName,
                    SD.numGenericDocID,
                    SD.vcDocumentSection,
                    SD.numRecID,
                    dbo.fn_GetListItemName(numDocCategory) AS Category,
                    SD.numCreatedBy AS numRecOwner
            FROM    dbo.DocumentWorkflow DW
                    INNER JOIN dbo.GenericDocuments SD ON DW.numDocID = SD.numGenericDocID
            WHERE   numContactID = @numUserCntID
                    AND tintApprove = 0
                
--		UNION 
--		  SELECT  numDocID,
--                numContactID,
--                tintApprove,
--                dtApprovedOn,
--                cDocType,
--                vcComment,
--                dtCreatedDate,
--                GD.vcDocName,
--                GD.numGenericDocID numGenericDocID,
--                GD.vcFileType,
--                0 AS numRecID,
--                dbo.fn_GetListItemName(numDocCategory) as Category  
--        FROM    dbo.DocumentWorkflow DW
--				INNER JOIN dbo.GenericDocuments GD ON DW.numDocID=GD.numGenericDocID
--        WHERE   numContactID = @numUserCntID
--                AND tintApprove = 0
--                
            UNION
            SELECT  numDocID,
                    DW.numContactID,
                    tintApprove,
                    dtApprovedOn,
                    cDocType,
                    vcComment,
                    DW.dtCreatedDate,
                    BD.vcBizDocID,
                    BD.numOppBizDocsId numGenericDocID,
                    cDocType AS vcFileType,
                    BD.numOppId AS numRecID,
                    LD.vcData AS Category,
                    OM.numRecOwner AS numRecOwner
            FROM    dbo.DocumentWorkflow DW
                    INNER JOIN dbo.OpportunityBizDocs BD ON DW.numDocID = BD.numOppBizDocsId
                    INNER JOIN dbo.OpportunityMaster OM ON OM.numOppId = BD.numOppId
                    INNER JOIN dbo.ListDetails LD ON LD.numListItemID = BD.numBizDocId
                                                     AND LD.numListID = 27
            WHERE   DW.numContactID = @numUserCntID
                    AND cDocType = 'B'
                    AND tintApprove = 0
       
                
        END
