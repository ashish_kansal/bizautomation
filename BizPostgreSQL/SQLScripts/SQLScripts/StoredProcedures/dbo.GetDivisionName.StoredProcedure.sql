/****** Object:  StoredProcedure [dbo].[GetDivisionName]    Script Date: 07/26/2008 16:14:28 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='getdivisionname')
DROP PROCEDURE getdivisionname
GO
CREATE PROCEDURE  [dbo].[GetDivisionName]
@numDivisionID numeric 

  AS


  Select vcDivisionName from DivisionMaster where numDivisionID=@numDivisionID
GO
