/****** Object:  StoredProcedure [dbo].[USP_GetContactFromEmail]    Script Date: 07/26/2008 16:16:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getcontactfromemail')
DROP PROCEDURE usp_getcontactfromemail
GO
CREATE PROCEDURE [dbo].[USP_GetContactFromEmail]            
@Email as varchar(50) ,    
@DomainId as numeric        
as        

 select adc.numContactID,adc.numDivisionID ,DM.tintCRMtype
from AdditionalContactsInformation adc
join divisionmaster DM on ADC.numdivisionId = Dm.numDivisionId
where ADC.vcEmail like '%'+ LTRIM(RTRIM(@Email))+'%' and ADC.numDomainId =@DomainId
GO
