/****** Object:  StoredProcedure [dbo].[USP_OPPSalesProcess]    Script Date: 07/26/2008 16:20:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_oppsalesprocess')
DROP PROCEDURE usp_oppsalesprocess
GO
CREATE PROCEDURE [dbo].[USP_OPPSalesProcess]  
(  
 @numProType as tinyint,
 @numDomainID as numeric(9)=0  
)  
as  
begin  
  
select slp_id,slp_name from Sales_process_List_Master where pro_type=@numProType and numDomainID=@numDomainID  
end
GO
