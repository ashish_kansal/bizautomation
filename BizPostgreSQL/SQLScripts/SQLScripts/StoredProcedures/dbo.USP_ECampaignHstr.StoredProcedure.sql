/****** Object:  StoredProcedure [dbo].[USP_ECampaignHstr]    Script Date: 07/26/2008 16:15:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_ecampaignhstr')
DROP PROCEDURE usp_ecampaignhstr
GO
CREATE PROCEDURE [dbo].[USP_ECampaignHstr] 
    @numConatctID AS NUMERIC(9),
    @numConEmailCampID NUMERIC(9),
	@ClientTimeZoneOffset Int 
AS 
		SELECT
			ECampaign.vcECampName,
			(CASE 
				WHEN ISNULL(ECampaign.bitDeleted,0) = 0 
				THEN CASE WHEN ISNULL(ConECampaign.bitEngaged,0) = 0 THEN 'Disengaged' ELSE 'Engaged' END
				ELSE 'Campaign Deleted'
			END) AS [Status],
			(CASE 
				WHEN ISNULL(ECampaignDTLs.numEmailTemplate,0) > 0 THEN GenericDocuments.vcDocName
				WHEN ISNULL(ECampaignDTLs.numActionItemTemplate,0) > 0 THEN tblActionItemData.TemplateName
				ELSE ''
			END) AS Template,
			ISNULL(dbo.FormatedDateTimeFromDate(DATEADD(MINUTE,ISNULL(ECampaign.fltTimeZone,0) * 60,ConECampaignDTL.dtExecutionDate),ACI.[numDomainID]),'') vcExecutionDate,
			(CASE 
				WHEN ISNULL(ConECampaignDTL.bitSend,0)=1 AND ISNULL(ConECampaignDTL.tintDeliveryStatus,0)=0 THEN 'Failed'
				WHEN ISNULL(ConECampaignDTL.bitSend,0)=1 AND ISNULL(ConECampaignDTL.tintDeliveryStatus,0)=1 THEN 'Success'
				ELSE 'Pending'
			END) AS SendStatus,
			--ISNULL(dbo.FormatedDateTimeFromDate(DATEADD(MINUTE,ISNULL(ECampaign.fltTimeZone,0)*60, ConECampaignDTL.[bintSentON]),ACI.[numDomainID]),'') vcSendDate,
			convert(varchar(20),dateAdd(minute, -@ClientTimeZoneOffset,ConECampaignDTL.[bintSentON] )) as vcSendDate, 

			ISNULL(ConECampaignDTL.bitEmailRead,0) bitEmailRead,
			(SELECT COUNT(*) FROM ConECampaignDTLLinks WHERE numConECampaignDTLID = ConECampaignDTL.numConECampDTLID) AS NoOfLinks,
			(SELECT COUNT(*) FROM ConECampaignDTLLinks WHERE ISNULL(bitClicked,0) = 1 AND numConECampaignDTLID = ConECampaignDTL.numConECampDTLID) AS NoOfLinkClicked
		FROM ConECampaignDTL
		INNER JOIN ECampaignDTLs ON ConECampaignDTL.numECampDTLID = ECampaignDTLs.numECampDTLId
		INNER JOIN ConECampaign	ON ConECampaignDTL.numConECampID = ConECampaign.numConEmailCampID
		INNER JOIN ECampaign ON	ConECampaign.numECampaignID = ECampaign.numECampaignID
		INNER JOIN [AdditionalContactsInformation] ACI ON ACI.[numContactId] = ConECampaign.[numContactID]
		LEFT JOIN GenericDocuments ON GenericDocuments.numGenericDocID = ECampaignDTLs.numEmailTemplate
		LEFT JOIN tblActionItemData ON tblActionItemData.RowID = ECampaignDTLs.numActionItemTemplate
		WHERE 
			(ConECampaign.numConEmailCampID = @numConEmailCampID OR ISNULL(@numConEmailCampID,0)=0) AND
			(ACI.numContactID = @numConatctID OR ISNULL(@numConatctID,0)=0)
		ORDER BY [numConECampDTLID] ASC 
GO
