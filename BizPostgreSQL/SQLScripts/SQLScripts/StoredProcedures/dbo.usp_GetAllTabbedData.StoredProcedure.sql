/****** Object:  StoredProcedure [dbo].[usp_GetAllTabbedData]    Script Date: 07/26/2008 16:16:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj   
-- exec usp_GetAllTabbedData @numGroupID=103,@numRelationShip=46,@numProfileID=0,@numDomainID=72
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'usp_getalltabbeddata' ) 
    DROP PROCEDURE usp_getalltabbeddata
GO
CREATE PROCEDURE [dbo].[usp_GetAllTabbedData]
    @numGroupID NUMERIC(9) = 0,
    @numRelationShip AS NUMERIC(9) = 0,
    @numProfileID AS NUMERIC(9) = 0,
    @numDomainID AS NUMERIC(9) = 0,
    @tintMode TINYINT = 0           
--        
AS 
    IF @numGroupID = 0 
        BEGIN        
        
            SELECT  numgroupid,
                    numtabid,
                    '' AS numTabName,
                    bitallowed
            FROM    grouptabdetails
            WHERE   ISNULL([tintType], 0) <> 1
            ORDER BY numtabid       
         
        END        
    ELSE 
        BEGIN 
            IF @tintMode = 0 
                BEGIN
                    SELECT  T.numTabID,
                            CASE WHEN bitFixed = 1
                                 THEN CASE WHEN EXISTS ( SELECT numTabName
                                                         FROM   TabDefault
                                                         WHERE  numDomainid = @numDomainID
                                                                AND numTabId = T.numTabId
                                                                AND tintTabType = T.tintTabType )
                                           THEN ( SELECT TOP 1
                                                            numTabName
                                                  FROM      TabDefault
                                                  WHERE     numDomainid = @numDomainID
                                                            AND numTabId = T.numTabId
                                                            AND tintTabType = T.tintTabType
                                                )
                                           ELSE T.numTabName
                                      END
                                 ELSE T.numTabName
                            END numTabname,
                            vcURL,
                            bitFixed,
                            G.[tintType],
                            T.vcImage
                    FROM    TabMaster T
                            JOIN GroupTabDetails G ON G.numTabId = T.numTabId
                    WHERE   ( numDomainID = @numDomainID
                              OR bitFixed = 1
                            )
                            AND numGroupID = @numGroupID
                            AND ISNULL(numRelationShip, 0) = @numRelationShip
                            AND ISNULL([numProfileID], 0) = @numProfileID
                            AND ISNULL(G.[tintType], 0) <> 1
                    ORDER BY numOrder    
                END      
            ELSE 
                BEGIN
			
		
                    SELECT  T.numTabID,
                            CASE WHEN bitFixed = 1
                                 THEN CASE WHEN EXISTS ( SELECT numTabName
                                                         FROM   TabDefault
                                                         WHERE  numDomainid = @numDomainID
                                                                AND numTabId = T.numTabId
                                                                AND tintTabType = T.tintTabType )
                                           THEN ( SELECT TOP 1
                                                            numTabName
                                                  FROM      TabDefault
                                                  WHERE     numDomainid = @numDomainID
                                                            AND numTabId = T.numTabId
                                                            AND tintTabType = T.tintTabType
                                                )
                                           ELSE T.numTabName
                                      END
                                 ELSE T.numTabName
                            END numTabname,
                            vcURL,
                            bitFixed,
                            G.[tintType],
                            T.vcImage
                    FROM    TabMaster T
                            JOIN GroupTabDetails G ON G.numTabId = T.numTabId
                    WHERE   ( numDomainID = @numDomainID
                              OR bitFixed = 1
                            )
                            AND numGroupID = @numGroupID
                            AND ISNULL(numRelationShip, 0) = @numRelationShip
                            AND ISNULL([numProfileID], 0) = @numProfileID
                            AND ISNULL(G.[tintType], 0) <> 1
					
					UNION ALL
					SELECT -1 AS [numTabID],
                           'Administration' numTabname,
                           '' vcURL,
                           1 bitFixed,
                           0 AS tintType,
                           '' AS vcImage
					
					UNION ALL
					SELECT -3 AS [numTabID],
                           'Advanced Search' numTabname,
                           '' vcURL,
                           1 bitFixed,
                           0 AS tintType,
                           '' AS vcImage                                                        
                    
                END
       
        END

