/****** Object:  StoredProcedure [dbo].[usp_UpdateCaseStatus]    Script Date: 07/26/2008 16:21:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_updatecasestatus')
DROP PROCEDURE usp_updatecasestatus
GO
CREATE PROCEDURE [dbo].[usp_UpdateCaseStatus]
	@numDomainID numeric=0,
	@intTargetResolveDate int=0   
--
AS
	--Insertion into Table  

	Update Cases set numStatus=135
	WHERE numDomainID=@numDomainID AND intTargetResolveDate<@intTargetResolveDate
	AND Cases.numStatus<>136 --to avoid the closed cases
GO
