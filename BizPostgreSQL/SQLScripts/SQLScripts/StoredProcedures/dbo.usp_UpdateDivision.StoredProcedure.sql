/****** Object:  StoredProcedure [dbo].[usp_UpdateDivision]    Script Date: 07/26/2008 16:21:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_updatedivision')
DROP PROCEDURE usp_updatedivision
GO
CREATE PROCEDURE [dbo].[usp_UpdateDivision]
@numCmpID numeric,
@numDivID numeric
AS
Declare @numOldCmpId numeric
Declare @numCntDivID numeric
Set @numCntDivID = 0

Select @numOldCmpId = numCompanyID from DivisionMaster where numCompanyID = @numDivID
Update DivisionMaster set numCompanyID = @numCmpID where numCompanyID = @numDivID

Select @numCntDivID = count(numDivisionID) from DivisionMaster where numCompanyID = @numOldCmpId
IF @numCntDivID = 0
Begin
	Delete from companyinfo where numCompanyID = @numOldCmpId
End
GO
