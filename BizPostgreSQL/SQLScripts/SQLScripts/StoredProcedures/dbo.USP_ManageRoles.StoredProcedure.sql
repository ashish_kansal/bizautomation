/****** Object:  StoredProcedure [dbo].[USP_ManageRoles]    Script Date: 07/26/2008 16:19:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
---created by anoop jayaraj  
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_manageroles')
DROP PROCEDURE usp_manageroles
GO
CREATE PROCEDURE [dbo].[USP_ManageRoles]  
@byteMode as tinyint,  
@RolesID as numeric(9)=0,  
@numUserCntID as numeric(9)=0,  
@fltPercentage as float=0  
as  
  
if @byteMode=0  
begin  
 delete from UserRoles where numUserCntID =@numUserCntID and numRole=@RolesID  
        insert into UserRoles (numRole,numUserCntID,fltPercentage)  
        values(@RolesID,@numUserCntID,@fltPercentage)  
        select 0  
end  
  
if @byteMode=1  
begin  
 delete from UserRoles where numUserCntID=@numUserCntID and numRole=@RolesID  
       select 1  
end  
  
if @byteMode=2  
begin  
 select * from UserRoles  
         join Listdetails on  
         numRole=numListItemID where numUserCntID=@numUserCntID   
         
end
GO
