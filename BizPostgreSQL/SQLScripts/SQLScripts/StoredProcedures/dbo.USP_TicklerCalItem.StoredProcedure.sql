/****** Object:  StoredProcedure [dbo].[USP_TicklerCalItem]    Script Date: 07/26/2008 16:21:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_ticklercalitem')
DROP PROCEDURE usp_ticklercalitem
GO
CREATE PROCEDURE [dbo].[USP_TicklerCalItem]            
@numUserCntID as varchar(200),        
@startDate as datetime,            
@endDate as datetime,                                                          
@ClientTimeZoneOffset Int = -330
as             
select              
0,0,0,ac.activityid as id ,case when alldayevent = 1 then '5' else '0' end as bitTask,            
DateAdd(minute, -@ClientTimeZoneOffset, startdatetimeutc) startdate,            
DateAdd(minute, -@ClientTimeZoneOffset, dateadd(second,duration,startdatetimeutc)) as endtime,            
activitydescription as itemdesc,            
res.resourceName name,            
 case when ACI.numPhone<>'' then + ACI.numPhone +case when ACI.numPhoneExtension<>'' then ' - ' + ACI.numPhoneExtension else '' end  else '' end as [Phone],            
 Substring ( Comp.vcCompanyName , 0 , 12 ) + '..' As vcCompanyName ,          
EmailAddress as vcEmail,            
'Calandar' as task,            
[subject] as Activity,            
case when importance =0 then 'Low' when importance =1 then 'Normal' when importance =2 then 'High' end as status,            
0 as numcreatedby,            
0 as numterid,            
'-/-' as assignedto,            
'0' as caseid,            
null as vccasenumber,            
0 as casetimeid,            
0 as caseexpid,            
1 as type ,        
itemid  ,        
changekey           
from activity ac join 
activityresource ar on  ac.activityid=ar.activityid 
join resource res on ar.resourceid=res.resourceid             
--join usermaster UM on um.numUserId = res.numUserCntId    
join AdditionalContactsInformation ACI on ACI.numContactId = res.numUserCntId    
 JOIN DivisionMaster Div                                                             
 ON ACI.numDivisionId = Div.numDivisionID                       
 JOIN CompanyInfo  Comp                                                             
 ON Div.numCompanyID = Comp.numCompanyId     
 where  res.numUserCntId = @numUserCntID    and (startdatetimeutc between @startDate and @endDate) and [status] <> -1
GO
