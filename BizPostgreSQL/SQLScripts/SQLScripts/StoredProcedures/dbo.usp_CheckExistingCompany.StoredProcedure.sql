/****** Object:  StoredProcedure [dbo].[usp_CheckExistingCompany]    Script Date: 07/26/2008 16:15:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_checkexistingcompany')
DROP PROCEDURE usp_checkexistingcompany
GO
CREATE PROCEDURE [dbo].[usp_CheckExistingCompany]
 @numCompanyID numeric,
 @vcCompanyName varchar(200),
 @numDivisionID numeric
AS
	SELECT 
		CI.vcCompanyName 
	FROM 
		CompanyInfo CI
	INNER JOIN
		DivisionMaster DM
	ON
		 CI.numCompanyID = DM.numCompanyId
	Where
		DM.numDivisionID <> @numDivisionID
		AND CI.vcCompanyName = @vcCompanyName
		AND CI.numCompanyID <> @numCompanyID
		AND DM.bitPublicFlag=0 
		AND DM.tintCRMType in (1,2)
GO
