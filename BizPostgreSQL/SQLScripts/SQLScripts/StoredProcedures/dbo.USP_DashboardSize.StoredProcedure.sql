/****** Object:  StoredProcedure [dbo].[USP_DashboardSize]    Script Date: 07/26/2008 16:15:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_dashboardsize')
DROP PROCEDURE usp_dashboardsize
GO
CREATE PROCEDURE  [dbo].[USP_DashboardSize] 
@tintColumn as tinyint, 
@tintSize as tinyint,  
@numGroupID as numeric(9)=0,
@numUserCntID as numeric(9)=0

as
if @numGroupID>0 
begin
	update DashBoardSize set
		 tintSize=@tintSize
	where numGroupUserCntID=@numGroupID and bitGroup=1 and tintColumn=@tintColumn
end
else if @numUserCntID>0
begin
update DashBoardSize set
		 tintSize=@tintSize
	where numGroupUserCntID=@numUserCntID and bitGroup=0  and tintColumn=@tintColumn 
end
GO
