/****** Object:  StoredProcedure [dbo].[USP_GetItemsAttrWarehouse]    Script Date: 07/26/2008 16:17:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--exec USP_GetWareHouseItems 6                            
--created by anoop jayaraj                              
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getitemsattrwarehouse')
DROP PROCEDURE usp_getitemsattrwarehouse
GO
CREATE PROCEDURE [dbo].[USP_GetItemsAttrWarehouse]                              
@numItemCode as numeric(9)=0,          
@strAtrr as varchar(100),          
@numWarehouseItemID as numeric(9)=0                            
as                              
                              
                          
                              
declare @bitSerialize as bit                      
declare @str as varchar(2000)                    
set @str=''                       
                        
                        
declare @numItemGroupID as numeric(9)                        
                        
select @numItemGroupID=numItemGroup,@bitSerialize=bitSerialized from Item where numItemCode=@numItemCode                           
              
                        
                       
select numWareHouseItemID,vcWarehouse,W.numWareHouseID,isnull(numOnHand,0) as [OnHand] ,isnull(numOnOrder,0) as [OnOrder] ,isnull(numReorder,0) as [Reorder] ,isnull(numAllocation,0) as [Allocation] ,isnull(numBackOrder,0) as [BackOrder],            
 0 as Op_Flag from WareHouseItems                           
 join Warehouses W                             
 on W.numWareHouseID=WareHouseItems.numWareHouseID                                    
 where numWarehouseItemID=@numWarehouseItemID          
          
if @bitSerialize=1          
begin          
declare @strSQL varchar(1000)          
set @strSQL=''          
if @strAtrr!=''            
begin            
            
 Declare @Cnt int            
   Set @Cnt = 1            
  declare @SplitOn char(1)            
  set @SplitOn=','            
  set @strSQL='SELECT recid            
     FROM CFW_Fld_Values_Serialized_Items where  bitSerialized ='+ convert(varchar(1),@bitSerialize)            
   While (Charindex(@SplitOn,@strAtrr)>0)            
   Begin            
    if  @Cnt=1 set @strSQL=@strSQL+' and fld_value=''' + (ltrim(rtrim(Substring(@strAtrr,1,Charindex(@SplitOn,@strAtrr)-1))))+''''            
    else set @strSQL=@strSQL+' or fld_value=''' + (ltrim(rtrim(Substring(@strAtrr,1,Charindex(@SplitOn,@strAtrr)-1))))+''''            
    Set @strAtrr = Substring(@strAtrr,Charindex(@SplitOn,@strAtrr)+1,len(@strAtrr))            
    Set @Cnt = @Cnt + 1            
   End            
  if  @Cnt=1 set @strSQL=@strSQL+' and fld_value=''' + @strAtrr + ''' GROUP BY recid            
   HAVING count(*) > '+convert(varchar(10), @Cnt-1)             
    else set @strSQL=@strSQL+' or fld_value=''' + @strAtrr + ''' GROUP BY recid            
   HAVING count(*) > '+convert(varchar(10), @Cnt-1)            
             
            
            
end           
          
          
 --Create a Temporary table to hold data                                                            
 Create table #tempTable ( ID INT IDENTITY PRIMARY KEY,                                                                      
  numCusFlDItemID numeric(9)                                                         
  )                         
                         
 insert into #tempTable                         
 (numCusFlDItemID)                                                            
 select distinct(numOppAccAttrID) from ItemGroupsDTL where numItemGroupID=@numItemGroupID and tintType=2                    
                           
                         
  declare @ID as numeric(9)                        
  declare @numCusFlDItemID as varchar(20)                        
  declare @fld_label as varchar(100)                        
  set @ID=0                        
  select top 1 @ID=ID,@numCusFlDItemID=numCusFlDItemID,@fld_label=fld_label from #tempTable                         
  join CFW_Fld_Master on numCusFlDItemID=Fld_ID                        
                          
  while @ID>0                        
  begin                        
                           
    set @str=@str+',  dbo.GetCustFldItems('+@numCusFlDItemID+',9,WDTL.numWareHouseItemID,0) as ['+ @fld_label+']'                                        
                           
    select top 1 @ID=ID,@numCusFlDItemID=numCusFlDItemID,@fld_label=fld_label from #tempTable                         
    join CFW_Fld_Master on numCusFlDItemID=Fld_ID and ID >@ID                        
    if @@rowcount=0 set @ID=0                        
                           
  end          
          
          
 set @str='select numWareHouseItmsDTLID,WDTL.numWareHouseItemID, vcSerialNo,0 as Op_Flag,WDTL.vcComments as Comments'+ case when @bitSerialize=1 then @str else '' end +' from WareHouseItmsDTL WDTL                             
 join WareHouseItems WI                             
 on WDTL.numWareHouseItemID=WI.numWareHouseItemID                              
 where ISNULL(WDTL.numQty,0)>0 and WI.numWarehouseItemID='+ convert(varchar(15),@numWarehouseItemID)              
    if @strAtrr!=''  set @str=@str+' and  numWareHouseItmsDTLID in ('+ @strSQL +')'          
                        
 print @str                       
 exec (@str)          
          
          
                      
 select Fld_label,fld_id,fld_type,numlistid,vcURL from #tempTable                         
  join CFW_Fld_Master on numCusFlDItemID=Fld_ID    
                      
 drop table #tempTable             
end            
else        
begin      
 select numWareHouseItmsDTLID,numWareHouseItemID, vcSerialNo,0 as Op_Flag,vcComments as Comments from    WareHouseItmsDTL where      numWareHouseItemID=@numWarehouseItemID    
     
end
GO
