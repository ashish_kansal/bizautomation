/****** Object:  StoredProcedure [dbo].[USP_ManageRelProfile]    Script Date: 07/26/2008 16:19:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_managerelprofile')
DROP PROCEDURE usp_managerelprofile
GO
CREATE PROCEDURE [dbo].[USP_ManageRelProfile]    
@numRelationship as numeric(9),    
@numProfile as numeric(9),    
@numRelProID as numeric(9)    ,
@numDomainID as numeric(9)
as    
if @numRelProID>0     
begin    
update  RelProfile set numRelationshipID=@numRelationship,numProfileID=@numProfile where numRelProID=@numRelProID    
end    
else    
begin    
declare @maxOrder as integer   
  
 select @maxOrder=isnull(max(numOrder),0) from RelProfile      
 join ListDetails L1      
 on numRelationshipID=L1.numListItemID      
 join ListDetails L2         
 on numProfileID=L2.numListItemID      
 where numRelationshipID=@numRelationship and L2.numDomainID=@numDomainID    
  
 insert into RelProfile(numRelationshipID,numProfileID,numOrder) values(@numRelationship,@numProfile,@maxOrder+1)    
end
GO
