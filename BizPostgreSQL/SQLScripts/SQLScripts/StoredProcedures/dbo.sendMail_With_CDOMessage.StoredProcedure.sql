/****** Object:  StoredProcedure [dbo].[sendMail_With_CDOMessage]    Script Date: 07/26/2008 16:14:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='sendmail_with_cdomessage')
DROP PROCEDURE sendmail_with_cdomessage
GO
CREATE PROCEDURE [dbo].[sendMail_With_CDOMessage]    
    @to VARCHAR(64),   
    @subject VARCHAR(1000)='',   
    @body VARCHAR(8000)='',  
    @CC varchar(1000)='',
    @From varchar(64)=''   
AS    
BEGIN    
    SET NOCOUNT ON    
   
    DECLARE    
        @handle INT,    
        @return INT,    
        @s VARCHAR(64),    
        @sc VARCHAR(64),    
        @up CHAR(27),        
        @server VARCHAR(255),    
        @filename VARCHAR(255)    
   
    SET @s = '"http://schemas.microsoft.com/cdo/configuration/'    
       

    if @From=null set @From=''
    if @From='' set @From= 'administrator@bizautomation.com'
    SELECT    
        @s = 'Configuration.Fields(' + @s,    
        @up = 'Configuration.Fields.Update',
        @server = '127.0.0.1'   
        -- or IP address, e.g. '127.0.0.1'   
       
        -- if you want an attachment:    
        ,@filename = 'C:\folder\file.ext'    
       
       
    EXEC @return = sp_OACreate 'CDO.Message', @handle OUT    
    SET @sc = @s + 'sendusing").Value'    
    EXEC @return = sp_OASetProperty @handle, @sc, '2'    
    SET @sc = @s + 'smtpserver").Value'    
    EXEC @return = sp_OASetProperty @handle, @sc, @server    
    EXEC @return = sp_OAMethod @handle, @up, NULL    
    EXEC @return = sp_OASetProperty @handle, 'To', @to    
    EXEC @return = sp_OASetProperty @handle, 'From', @From    
    EXEC @return = sp_OASetProperty @handle, 'Subject', @subject    
    EXEC @return = sp_OASetProperty @handle, 'htmlbody', @body  
    EXEC @return = sp_OASetProperty @handle, 'CC', @CC  
       
    IF @filename IS NOT NULL    
        EXEC @return = sp_OAMethod @handle, 'AddAttachment', NULL, @filename    
   
    EXEC @return = sp_OAMethod @handle, 'Send', NULL  

    IF @return <> 0    
    BEGIN    
        PRINT 'Mail failed.'    
        IF @From IS NULL    
            PRINT 'From address undefined.'    
        ELSE    
            PRINT 'Check that server is valid.'    
    
       
    EXEC @return = sp_OADestroy @handle    
END    
end
GO
