/****** Object:  StoredProcedure [dbo].[usp_GetSpecficDocDtl]    Script Date: 07/26/2008 16:18:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getspecficdocdtl')
DROP PROCEDURE usp_getspecficdocdtl
GO
CREATE PROCEDURE  [dbo].[usp_GetSpecficDocDtl]
@numRecId numeric

as 
select 
numGenericDocID,            
  VcFileName ,            
  vcDocName ,
numDocCategory     
from dbo.GenericDocuments 
 where numGenericDocID = @numRecId
GO
