GO
/****** Object:  StoredProcedure [dbo].[USP_ManageOrderAutoRuleDetails]    Script Date: 03/04/2010 15:32:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--select * from OrderAutoRuleDetails

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_manageorderautoruledetails')
DROP PROCEDURE usp_manageorderautoruledetails
GO
CREATE PROCEDURE [dbo].[USP_ManageOrderAutoRuleDetails]
@numRuleID int,
@numItemTypeID numeric,
@numItemClassID numeric,
@numBillToID int,
@numShipToID int,
@btFullPaid bit,
@numBizDocStatus numeric,
@btActive bit

as

INSERT INTO OrderAutoRuleDetails SELECT @numRuleID,@numItemTypeID,@numItemClassID,@numBillToID,@numShipToID,@btFullPaid,@numBizDocStatus,@btActive
SELECT SCOPE_IDENTITY()


