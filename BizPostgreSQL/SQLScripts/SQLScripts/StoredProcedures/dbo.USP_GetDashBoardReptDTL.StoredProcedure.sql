/****** Object:  StoredProcedure [dbo].[USP_GetDashBoardReptDTL]    Script Date: 07/26/2008 16:17:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj                  
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getdashboardreptdtl')
DROP PROCEDURE usp_getdashboardreptdtl
GO
CREATE PROCEDURE [dbo].[USP_GetDashBoardReptDTL]                  
@numDashBoardReptID as numeric(9)                 
as  

 DECLARE @tintReportCategory AS TINYINT,@numReportID NUMERIC(9),@numGroupUserCntID NUMERIC(9)
 
 SELECT @tintReportCategory=tintReportCategory,@numReportID=numReportID,@numGroupUserCntID=numGroupUserCntID FROM Dashboard WHERE numDashBoardReptID=@numDashBoardReptID
 
 IF @tintReportCategory =1
 BEGIN             
	select numReportID,numGroupUserCntID,tintReportType,vcHeader,vcFooter,                 
 tintChartType,bitGroup,tintColumn,bintrows,tintRow,          
 (select max(D.tintRow) from Dashboard D where D.numGroupUserCntID =D1.numGroupUserCntID and D.bitGroup=D1.bitGroup and D.tintColumn=D1.tintColumn) as MaxRow ,          
 (select min(D.tintRow) from Dashboard D where D.numGroupUserCntID =D1.numGroupUserCntID and D.bitGroup=D1.bitGroup and D.tintColumn=D1.tintColumn) as MinRow,        
  bitGridType,varGrpflt,textQueryGrp,textQuery ,tintReportCategory       
from                 
 CustomReport Cus         
 Join  Dashboard D1        
 on Cus.numCustomReportID=D1.numReportID              
where numDashBoardReptID=@numDashBoardReptID
END

ELSE IF @tintReportCategory =2
 BEGIN 
 
DECLARE @tintComAppliesTo TINYINT
SELECT  @tintComAppliesTo = ISNULL(tintComAppliesTo,0) FROM domain WHERE numDomainID = (SELECT UM.numDomainID FROM 
CommissionReports Cus Join Dashboard D1  on Cus.numComReports=D1.numReportID JOIN UserMaster UM ON UM.numUserDetailId= Cus.numContactID        
where numDashBoardReptID=@numDashBoardReptID)

if @tintComAppliesTo=1    
begin   

select numReportID,numGroupUserCntID,tintReportType,I.vcItemName + CASE cus.tintAssignTo WHEN 1 THEN '(Assign To)' WHEN 2 THEN '(Record Owner)' END + ' - Commission Report' AS vcHeader,vcFooter,                 
 tintChartType,tintColumn,tintRow,          
 (select max(D.tintRow) from Dashboard D where D.numGroupUserCntID =D1.numGroupUserCntID and D.bitGroup=D1.bitGroup and D.tintColumn=D1.tintColumn) as MaxRow ,          
 (select min(D.tintRow) from Dashboard D where D.numGroupUserCntID =D1.numGroupUserCntID and D.bitGroup=D1.bitGroup and D.tintColumn=D1.tintColumn) as MinRow,        
 'exec GetCommissionItemReport @numDomainId,@numUserCntID,@numItemCode=' + CAST(cus.numItemCode AS VARCHAR(10)) + ',@tintAssignTo=' + CAST(cus.tintAssignTo AS VARCHAR(10)) +',@bitCommContact=0' AS textQueryGrp,tintReportCategory       
from                 
 CommissionReports Cus         
 Join  Dashboard D1  on Cus.numComReports=D1.numReportID   
 JOIN Item I ON Cus.numItemCode=I.numItemCode        
where numDashBoardReptID=@numDashBoardReptID  
    
end    
ELSE if @tintComAppliesTo=2    
begin    

select numReportID,numGroupUserCntID,tintReportType,dbo.[GetListIemName]([numItemCode]) + CASE cus.tintAssignTo WHEN 1 THEN '(Assign To)' WHEN 2 THEN '(Record Owner)' END + ' - Commission Report' AS vcHeader,vcFooter,                 
 tintChartType,tintColumn,tintRow,          
 (select max(D.tintRow) from Dashboard D where D.numGroupUserCntID =D1.numGroupUserCntID and D.bitGroup=D1.bitGroup and D.tintColumn=D1.tintColumn) as MaxRow ,          
 (select min(D.tintRow) from Dashboard D where D.numGroupUserCntID =D1.numGroupUserCntID and D.bitGroup=D1.bitGroup and D.tintColumn=D1.tintColumn) as MinRow,        
 'exec GetCommissionItemReport @numDomainId,@numUserCntID,@numItemCode=' + CAST(cus.numItemCode AS VARCHAR(10)) + ',@tintAssignTo=' + CAST(cus.tintAssignTo AS VARCHAR(10)) +',@bitCommContact=0' AS textQueryGrp,tintReportCategory       
from                 
 CommissionReports Cus         
 Join  Dashboard D1  on Cus.numComReports=D1.numReportID   
where numDashBoardReptID=@numDashBoardReptID  
 
end  
ELSE if @tintComAppliesTo=3    
begin    

select numReportID,numGroupUserCntID,tintReportType,'All Items' + CASE cus.tintAssignTo WHEN 1 THEN '(Assign To)' WHEN 2 THEN '(Record Owner)' END + ' - Commission Report' AS vcHeader,vcFooter,                 
 tintChartType,tintColumn,tintRow,          
 (select max(D.tintRow) from Dashboard D where D.numGroupUserCntID =D1.numGroupUserCntID and D.bitGroup=D1.bitGroup and D.tintColumn=D1.tintColumn) as MaxRow ,          
 (select min(D.tintRow) from Dashboard D where D.numGroupUserCntID =D1.numGroupUserCntID and D.bitGroup=D1.bitGroup and D.tintColumn=D1.tintColumn) as MinRow,        
 'exec GetCommissionItemReport @numDomainId,@numUserCntID,@numItemCode=' + CAST(cus.numItemCode AS VARCHAR(10)) + ',@tintAssignTo=' + CAST(cus.tintAssignTo AS VARCHAR(10)) +',@bitCommContact=0' AS textQueryGrp,tintReportCategory       
from                 
 CommissionReports Cus         
 Join  Dashboard D1  on Cus.numComReports=D1.numReportID   
where numDashBoardReptID=@numDashBoardReptID  
 
end 

   
     
 END
GO
