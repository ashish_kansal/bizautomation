/****** Object:  StoredProcedure [dbo].[usp_RenameAOI]    Script Date: 07/26/2008 16:20:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--modified by anoop jayaraj
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_renameaoi')
DROP PROCEDURE usp_renameaoi
GO
CREATE PROCEDURE [dbo].[usp_RenameAOI]  
 @numAOIID numeric,  
 @vcNewName varchar(50),  
 @numUserID numeric   
AS  

BEGIN  
 UPDATE AOIMaster   
  SET vcAOIName=@vcNewName,  
   numModifiedBy=@numUserID,  
   bintmodifiedDate=getutcdate()  
  WHERE numAOIID=@numAOIID  
  
END
GO
