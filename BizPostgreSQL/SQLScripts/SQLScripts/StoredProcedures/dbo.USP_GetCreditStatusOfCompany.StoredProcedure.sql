/****** Object:  StoredProcedure [dbo].[USP_GetCreditStatusOfCompany]    Script Date: 07/26/2008 16:17:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getcreditstatusofcompany')
DROP PROCEDURE usp_getcreditstatusofcompany
GO
CREATE PROCEDURE [dbo].[USP_GetCreditStatusOfCompany]    
@numDivisionID as numeric(9)    
as    
    
declare @numDomainID as numeric(9)
declare @bitCheckCreditStatus as bit
select @numDomainID=numDomainID from divisionMaster where numDivisionID=@numDivisionID
select @bitCheckCreditStatus=isnull(bitCheckCreditStatus,0) from eCommerceDTL where numDomainID=@numDomainID

if @bitCheckCreditStatus=1
BEGIN
	SELECT 
		ISNULL(vcData,0) 
	FROM 
		companyinfo Com    
	JOIN 
		divisionmaster Div    
	ON 
		Com.numCompanyID=Div.numCompanyID  
	LEFT JOIN 
		listdetails   
	ON
		numListID=3 AND numListItemID=numCompanyCredit  
	WHERE 
		div.numDivisionID=@numDivisionID  
END
else
BEGIN
	SELECT 10000000
END
GO
