/****** Object:  StoredProcedure [dbo].[Activity_Rem]    Script Date: 07/26/2008 16:14:23 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
/*
**  Removes the Activity identified by the given ID number, which
**  consequently cleans up any references between the Activity
**  and it's related Resource(s).
**
*/
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='activity_rem')
DROP PROCEDURE activity_rem
GO
CREATE PROCEDURE [dbo].[Activity_Rem]
	@DataKey	integer		-- activity primary key number to be deleted
AS
BEGIN
	DECLARE @RecurrenceKey	integer;
	DECLARE @VarianceKey    uniqueidentifier;

	-- Start a transaction to commit all row deletions simultaneously.
	BEGIN TRANSACTION

	-- Get the keys identifying the Recurrence and Variances, if any,
	-- before deleting this Activity row.
	--
	SELECT @RecurrenceKey = [Activity].[RecurrenceID],
               @VarianceKey = [Activity].[VarianceID]
	FROM   [Activity]
	WHERE ( [Activity].[ActivityID] = @DataKey );

	-- Delete the relationship between this Activity and it's Resource(s).
	--
	-- modified to delete the child activities record 
IF ( NOT @VarianceKey IS NULL )
begin
	DELETE FROM ActivityResource
	WHERE [ActivityResource].[ActivityID] in (select [ActivityID]  FROM [Activity] WHERE  [Activity].[VarianceID] = @VarianceKey);
end
IF (@VarianceKey IS NULL )
begin
	DELETE FROM ActivityResource
	WHERE [ActivityResource].[ActivityID] = @DataKey;
end

	--REMOVE ACTIVITYID FROM COMMUNICATION
	UPDATE Communication SET numActivityId = 0,bitOutlook=0 WHERE numActivityId = @DataKey

	-- Delete this Activity row.
	--
	DELETE FROM Activity
	WHERE ( [Activity].[ActivityID] = @DataKey );

	-- If this Activity had Variances, delete all of them (they will share
	-- the same VarianceID uniqueidentifier.)
	--
	IF ( NOT @VarianceKey IS NULL )
	BEGIN
		DELETE FROM [Activity]
		WHERE ( [Activity].[VarianceID] = @VarianceKey );
	END

	-- If this Activity had a Recurrence, delete it.
	--
	IF ( (-999) <> @RecurrenceKey )
	BEGIN
		DELETE FROM [Recurrence]
		WHERE ( [Recurrence].[RecurrenceID] = @RecurrenceKey );
	END

	COMMIT
END
GO
