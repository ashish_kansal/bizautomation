/****** Object:  StoredProcedure [dbo].[usp_GetCompany]    Script Date: 07/26/2008 16:16:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Modified By Anoop jayaraj                    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getcompany')
DROP PROCEDURE usp_getcompany
GO
CREATE PROCEDURE [dbo].[usp_GetCompany]                          
 @numDomainId numeric=0,                                            
 @vcCmpFilter varchar(100)='%',                    
 @numUserCntID as numeric(9)=0                          
--                        
AS                             
-- checking the company rights                    
                    
Declare @ProspectsRights as tinyint                    
Declare @accountsRights as  tinyint                   
Declare @leadsRights as  tinyint 

PRINT @vcCmpFilter
set @vcCmpFilter=replace(@vcCmpFilter,'''','|')
PRINT @vcCmpFilter
               
set @ProspectsRights=(SELECT top 1 MAX(GA.intViewAllowed) As intViewAllowed                    
  FROM UserMaster UM INNER JOIN GroupAuthorization GA ON GA.numGroupID=UM.numGroupID INNER JOIN PageMaster PM ON PM.numPageID=GA.numPageID AND PM.numModuleID=GA.numModuleID
  WHERE PM.numpageID=2 and PM.numModuleID=3 and UM.numUserID=(select numUserID from userMaster where numUserDetailId=@numUserCntID)                  
  GROUP BY UM.numUserId, UM.vcUserName,                     
   PM.vcFileName, GA.numModuleID, GA.numPageID)                        
                    
set @accountsRights=(SELECT top 1 MAX(GA.intViewAllowed) As intViewAllowed                    
  FROM UserMaster UM INNER JOIN GroupAuthorization GA ON GA.numGroupID=UM.numGroupID INNER JOIN PageMaster PM ON PM.numPageID=GA.numPageID AND PM.numModuleID=GA.numModuleID
  WHERE PM.numpageID=2 and PM.numModuleID=4 and UM.numUserID=(select numUserID from userMaster where numUserDetailId=@numUserCntID)                     
  GROUP BY UM.numUserId, UM.vcUserName,                     
   PM.vcFileName, GA.numModuleID, GA.numPageID)                
              
set @leadsRights=(SELECT top 1 MAX(GA.intViewAllowed) As intViewAllowed                    
  FROM UserMaster UM INNER JOIN GroupAuthorization GA ON GA.numGroupID=UM.numGroupID INNER JOIN PageMaster PM ON PM.numPageID=GA.numPageID AND PM.numModuleID=GA.numModuleID
  WHERE PM.numpageID=2 and PM.numModuleID=2 and UM.numUserID=(select numUserID from userMaster where numUserDetailId=@numUserCntID)                     
  GROUP BY UM.numUserId, UM.vcUserName,                    
   PM.vcFileName, GA.numModuleID, GA.numPageID)                   
                    
declare @strSQL as varchar(8000)                      
                        
set @strSQL='SELECT a.vcCompanyname + Case when isnull(d.numCompanyDiff,0)>0 then  ''  '' + dbo.fn_getlistitemname(d.numCompanyDiff) + '':'' + isnull(d.vcCompanyDiff,'''') else '''' end as vcCompanyname,d.numDivisionID,a.numCompanyType                           
   FROM companyinfo a                      
   join divisionmaster d                      
   on  a.numCompanyid=d.numCompanyid 
   join AdditionalContactsInformation AC
   ON AC.numDivisionID=D.numDivisionID AND ISNULL(AC.bitPrimaryContact,0)=1                  
   WHERE a.numdomainid='+convert(varchar(15),@numdomainid)+' and d.tintCRMType<>0 and d.tintCRMType=1                       
   AND replace(a.vcCompanyname,'''''''',' + '''|'')like '''+  @vcCmpFilter + ''''     
 if @ProspectsRights=0  set @strSQL=@strSQL+' and d.numRecOwner=0 '                    
 if @ProspectsRights=1  set @strSQL=@strSQL+' and d.numRecOwner='+convert(varchar(15),@numUserCntID)                     
 if @ProspectsRights=2  set @strSQL=@strSQL+' and (d.numTerID in (select numTerritoryID  from UserTerritory where numUserCntID='+convert(varchar(15),@numUserCntID)+') or d.numTerID=0)'                    
set @strSQL=@strSQL+' union SELECT a.vcCompanyname + Case when isnull(d.numCompanyDiff,0)>0 then  ''  '' + dbo.fn_getlistitemname(d.numCompanyDiff) + '':'' + isnull(d.vcCompanyDiff,'''') else '''' end as vcCompanyname,d.numDivisionID,a.numCompanyType                           
   FROM companyinfo a                      
   join divisionmaster d                      
   on  a.numCompanyid=d.numCompanyid
	join AdditionalContactsInformation AC
   ON AC.numDivisionID=D.numDivisionID AND ISNULL(AC.bitPrimaryContact,0)=1                       
   WHERE a.numdomainid='+convert(varchar(15),@numdomainid)+' and d.tintCRMType<>0 and d.tintCRMType=2                      
   AND replace(a.vcCompanyname,'''''''',' + '''|'')like '''+  @vcCmpFilter + ''''     
 if @accountsRights=0  set @strSQL=@strSQL+' and d.numRecOwner=0 '                    
 if @accountsRights=1  set @strSQL=@strSQL+' and d.numRecOwner='+convert(varchar(15),@numUserCntID)                     
 if @accountsRights=2  set @strSQL=@strSQL+' and (d.numTerID in (select numTerritoryID  from UserTerritory where numUserCntID='+convert(varchar(15),@numUserCntID)+') or d.numTerID=0)'                 
set @strSQL=@strSQL+' union SELECT a.vcCompanyname + Case when isnull(d.numCompanyDiff,0)>0 then ''  '' + dbo.fn_getlistitemname(d.numCompanyDiff) + '':'' + isnull(d.vcCompanyDiff,'''') else '''' end as vcCompanyname,d.numDivisionID,a.numCompanyType                           
   FROM companyinfo a                      
   join divisionmaster d            
   on  a.numCompanyid=d.numCompanyid   
	join AdditionalContactsInformation AC
   ON AC.numDivisionID=D.numDivisionID AND ISNULL(AC.bitPrimaryContact,0)=1                    
   WHERE a.numdomainid='+convert(varchar(15),@numdomainid)+' and d.tintCRMType=0                       
   AND replace(a.vcCompanyname,'''''''',' + '''|'')like '''+  @vcCmpFilter + ''''                     
 if @leadsRights=0  set @strSQL=@strSQL+' and d.numRecOwner=0 '                    
 if @leadsRights=1  set @strSQL=@strSQL+' and d.numRecOwner='+convert(varchar(15),@numUserCntID)                     
 if @leadsRights=2  set @strSQL=@strSQL+' and (d.numTerID in (select numTerritoryID  from UserTerritory where numUserCntID='+convert(varchar(15),@numUserCntID)+') or d.numTerID=0)'                   
 set @strSQL=@strSQL+' ORDER BY vcCompanyname'    

print(@strSQL)
exec (@strSQL)
GO
GO
