/****** Object:  StoredProcedure [dbo].[usp_DemoteAccount]    Script Date: 07/26/2008 16:15:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_demoteaccount')
DROP PROCEDURE usp_demoteaccount
GO
CREATE PROCEDURE [dbo].[usp_DemoteAccount]      
 @numDivisionID numeric=0,      
 @byteMode as tinyint=0,
  @numDomainID as numeric(9)=0,
  @numUserCntID numeric    
AS      
BEGIN
	DECLARE @tintCurrentCRMType TINYINT    
    
	if @byteMode=0    
	begin     
		declare @numGRPID as numeric 
	
		select @numGRPID=isnull(numGrpId,0),@tintCurrentCRMType=tintCRMType from DivisionMaster 
		where numDivisionId =@numDivisionID and numDomainID=@numDomainID
		if @numGRPID=0 
		begin
		 UPDATE dbo.DivisionMaster SET numGrpId=2 ,numModifiedBy=@numUserCntID,bintModifiedDate=getutcdate()    
		 WHERE numDivisionId=@numDivisionID  and numDomainID=@numDomainID
		end
	 
		INSERT INTO DivisionMasterPromotionHistory
		(
			numDomainID
			,numDivisionID
			,tintPreviousCRMType
			,tintNewCRMType
			,dtPromotedBy
			,dtPromotionDate
		)
		VALUES
		(
			@numDomainID
			,@numDivisionID
			,@tintCurrentCRMType
			,0
			,@numUserCntID
			,GETUTCDATE()
		)       

		 UPDATE dbo.DivisionMaster SET tintCRMType=0,numModifiedBy=@numUserCntID,bintModifiedDate=getutcdate()      
		 WHERE numDivisionId=@numDivisionID   and numDomainID=@numDomainID  
	end     
	if @byteMode=1    
	begin       
		SELECT @tintCurrentCRMType=tintCRMType FROM DivisionMaster WHERE numDivisionID=@numDivisionID     
	
		INSERT INTO DivisionMasterPromotionHistory
		(
			numDomainID
			,numDivisionID
			,tintPreviousCRMType
			,tintNewCRMType
			,dtPromotedBy
			,dtPromotionDate
		)
		VALUES
		(
			@numDomainID
			,@numDivisionID
			,@tintCurrentCRMType
			,1
			,@numUserCntID
			,GETUTCDATE()
		)       

		 UPDATE dbo.DivisionMaster SET tintCRMType=1,numModifiedBy=@numUserCntID,bintModifiedDate=getutcdate()     
		 WHERE numDivisionId=@numDivisionID    and numDomainID=@numDomainID 
	end
	if @byteMode=2    
	begin       
		SELECT @tintCurrentCRMType=tintCRMType FROM DivisionMaster WHERE numDivisionID=@numDivisionID     
	
		INSERT INTO DivisionMasterPromotionHistory
		(
			numDomainID
			,numDivisionID
			,tintPreviousCRMType
			,tintNewCRMType
			,dtPromotedBy
			,dtPromotionDate
		)
		VALUES
		(
			@numDomainID
			,@numDivisionID
			,@tintCurrentCRMType
			,2
			,@numUserCntID
			,GETUTCDATE()
		)      


		 UPDATE 
			dbo.DivisionMaster 
		SET 
			tintCRMType=2, 
			bintProsProm = getutcdate()
			,bintProsPromBy = @numUserCntID
			,numModifiedBy=@numUserCntID
			,bintModifiedDate=getutcdate()       
		 WHERE 
			numDivisionId=@numDivisionID 
			AND numDomainID=@numDomainID 
	end
END
GO
