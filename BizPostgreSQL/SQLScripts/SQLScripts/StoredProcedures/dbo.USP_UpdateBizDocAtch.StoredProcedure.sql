/****** Object:  StoredProcedure [dbo].[USP_UpdateBizDocAtch]    Script Date: 07/26/2008 16:21:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_updatebizdocatch')
DROP PROCEDURE usp_updatebizdocatch
GO
CREATE PROCEDURE [dbo].[USP_UpdateBizDocAtch]
	@vcOrder as varchar(300)='',
	@numDomainID as numeric(9)=0,
	@numBizDocID as numeric(9)=0,
	@numBizDocTempID as numeric(9)=0
as
	
declare @hDoc  int 
          
EXEC sp_xml_preparedocument @hDoc OUTPUT, @vcOrder

update   BizDocAttachments set numOrder=X.numOrder        
from (SELECT * FROM OPENXML (@hDoc,'/NewDataSet/Table',2)          
 WITH(numBizAtchID numeric(9),          
      numOrder numeric(9)))X 
 where numAttachmntID= X.numBizAtchID        
          
          
 EXEC sp_xml_removedocument @hDoc
GO
