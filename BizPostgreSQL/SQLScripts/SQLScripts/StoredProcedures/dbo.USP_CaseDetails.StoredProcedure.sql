/****** Object:  StoredProcedure [dbo].[USP_CaseDetails]    Script Date: 07/26/2008 16:15:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--exec USP_CaseDetails @numCaseId = 34              
              
              
--sp_helptext USP_CaseDetails              
              
              
 --created by anoop jayaraj                            
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_casedetails')
DROP PROCEDURE usp_casedetails
GO
CREATE PROCEDURE [dbo].[USP_CaseDetails]                                  
@numCaseId as numeric(9) ,    
@numDomainID as numeric(9),    
@ClientTimeZoneOffset as int                                   
                                  
as                                  
                                  
select C.numContactID,vcCaseNumber,
 intTargetResolveDate,textSubject,Div.numDivisionID,                                  
 numStatus,dbo.GetListIemName(numStatus) AS vcCaseStatus, numPriority,textDesc,tintCRMType,                                  
 textInternalComments,numReason,C.numContractID,
 ISNULL((SELECT cm.vcContractName FROM dbo.ContractManagement cm WHERE cm.numContractId = C.numContractId),'') AS vcContractName,
 numOrigin,numType,                       
 dbo.fn_GetContactName(C.numCreatedby)+' ' + convert(varchar(20),dateadd(minute,-@ClientTimeZoneOffset,C.bintCreatedDate)) as CreatedBy,                                  
 dbo.fn_GetContactName(C.numModifiedBy)+' ' + convert(varchar(20),dateadd(minute,-@ClientTimeZoneOffset,C.bintModifiedDate)) as ModifiedBy,                      
 dbo.fn_GetContactName(C.numRecOwner) as RecOwner,                                
 C.numRecOwner,
 tintSupportKeyType,                                  
 C.numAssignedTo,C.numAssignedBy,isnull(Addc.vcFirstName,'') + ' ' + isnull(Addc.vcLastName,'') as vcName,                                  
 isnull(vcEmail,'') as vcEmail,isnull(numPhone,'') + case when numPhoneExtension is null then '' when numPhoneExtension ='' then '' else ', '+ numPhoneExtension end  as Phone,   
         isnull(numPhone,'') AS numPhone,
         isnull(numPhoneExtension,'') AS PhoneExtension,
		 		 (SELECT TOP 1 ISNULL(vcData,'') FROM ListDetails WHERE numListID=5 AND numListItemID=com.numCompanyType AND numDomainId=@numDomainID) AS vcCompanyType,
 Com.numCompanyId,Com.vcCompanyName,(select count(*) from Comments where numCaseID =@numCaseId)  as NoofCases,
 (SELECT COUNT(*) FROM   dbo.GenericDocuments WHERE  numRecID = @numCaseId AND vcDocumentSection = 'CS') AS DocumentCount, Div.numTerID,
 ISNULL((SELECT (ISNULL(C.numIncidents,0)  - ISNULL(C.numIncidentsUsed,0)) FROM Contracts AS C WHERE C.numDivisonId=Div.numDivisionID AND numDomainId=@numDomainID AND intType=3),0) AS numIncidentLeft,

  ISNULL(((SELECT TOP 1 CASE WHEN  ISNULL((SELECT numWarrantyDays FROM Contracts AS C WHERE  numDomainId=@numDomainID AND I.numItemClassification=C.vcItemClassification AND intType=2),0)>0 THEN ISNULL((SELECT vcNotes FROM Contracts AS C WHERE  numDomainId=@numDomainID AND I.numItemClassification=C.vcItemClassification AND intType=2),'-') ELSE '-' END  FROM OpportunityItems AS OI LEFT JOIN OpportunityMaster AS O ON O.numOppId=OI.numOppId LEFT JOIN Item I ON OI.numItemCode=I.numItemCode WHERE O.numDivisionId=Addc.numDivisionId AND O.numDomainId=@numDomainID AND
 1 =(CASE WHEN OI.numoppitemtCode IN (SELECT numOppItemID FROM CaseOpportunities WHERE numCaseId=C.numCaseId) THEN 1
 ELSE 0 END
 )
AND O.tintOppType=1 AND O.tintOppStatus=1 AND O.tintshipped=1 ORDER BY O.bintCreatedDate DESC)),'') AS vcContractNotes,
 ISNULL((SELECT TOP 1 CASE WHEN ISNULL((SELECT numWarrantyDays FROM Contracts AS C WHERE  numDomainId=@numDomainID AND I.numItemClassification=C.vcItemClassification  AND intType=2),0)>0 THEN ISNULL((SELECT numWarrantyDays FROM Contracts AS C WHERE  numDomainId=@numDomainID AND I.numItemClassification=C.vcItemClassification AND intType=2),0)-ISNULL(DATEDIFF(DAY, O.bintClosedDate, GETDATE()),0) ELSE 0 END  FROM OpportunityItems AS OI LEFT JOIN OpportunityMaster AS O ON O.numOppId=OI.numOppId LEFT JOIN Item I ON OI.numItemCode=I.numItemCode WHERE O.numDivisionId=Addc.numDivisionId AND O.numDomainId=@numDomainID AND
 1 =(CASE WHEN OI.numoppitemtCode IN (SELECT numOppItemID FROM CaseOpportunities WHERE numCaseId=C.numCaseId) THEN 1
 ELSE 0 END
 )
  AND O.tintOppType=1 AND O.tintOppStatus=1 AND O.tintshipped=1 ORDER BY O.bintCreatedDate DESC),0) AS numWarrantyDaysLeft,
  ISNULL((SELECT TOP 1 I.vcItemName FROM OpportunityItems AS OI LEFT JOIN OpportunityMaster AS O ON O.numOppId=OI.numOppId LEFT JOIN Item I ON OI.numItemCode=I.numItemCode WHERE O.numDivisionId=Addc.numDivisionId AND O.numDomainId=@numDomainID AND
 1 =(CASE WHEN OI.numoppitemtCode IN (SELECT numOppItemID FROM CaseOpportunities WHERE numCaseId=C.numCaseId) THEN 1
 ELSE 0 END
 )
  AND O.tintOppType=1 AND O.tintOppStatus=1 AND O.tintshipped=1 ORDER BY O.bintCreatedDate DESC),'-') AS vcWarrantyItemName,
  ISNULL((SELECT TOP 1 I.numItemCode FROM OpportunityItems AS OI LEFT JOIN OpportunityMaster AS O ON O.numOppId=OI.numOppId LEFT JOIN Item I ON OI.numItemCode=I.numItemCode WHERE O.numDivisionId=Addc.numDivisionId AND O.numDomainId=@numDomainID AND
 1 =(CASE WHEN OI.numoppitemtCode IN (SELECT numOppItemID FROM CaseOpportunities WHERE numCaseId=C.numCaseId) THEN 1
 ELSE 0 END
 )
  AND O.tintOppType=1 AND O.tintOppStatus=1 AND O.tintshipped=1 ORDER BY O.bintCreatedDate DESC),0) AS numWarrantyItemCode
 from Cases C                                  
 left join AdditionalContactsInformation Addc                                  
 on Addc.numContactId=C.numContactID                                   
 join DivisionMaster Div                                  
 on Div.numDivisionId=Addc.numDivisionId                                  
 join CompanyInfo Com                                   
 on Com.numCompanyID=div.numCompanyID                                  
                                 
 where numCaseId=@numCaseId and C.numDomainID=@numDomainID
GO
