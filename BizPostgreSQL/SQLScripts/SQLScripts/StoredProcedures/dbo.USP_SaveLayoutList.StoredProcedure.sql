/****** Object:  StoredProcedure [dbo].[USP_SaveLayoutList]    Script Date: 07/26/2008 16:21:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_savelayoutlist')
DROP PROCEDURE usp_savelayoutlist
GO
CREATE PROCEDURE [dbo].[USP_SaveLayoutList]                                  
 @numUserCntID Numeric=0,                                  
 @numDomainID Numeric=0,                         
@strRow as text='',                      
@intcoulmn numeric=0,   
@numFormID as numeric=0,                          
@numRelCntType numeric=0,
  @tintPageType TINYINT      
AS                            
                        
delete  DycFormConfigurationDetails where numUserCntId=@numUserCntID and numDomainId=@numDomainId 
			and intColumnNum >= @intcoulmn and  numRelCntType=@numRelCntType AND numFormId=@numFormID
			AND tintPageType=@tintPageType  AND ISNULL(bitDefaultByAdmin,0)=0
  
                        
if convert(varchar(10),@strRow) <> ''                            
begin                                                        
   DECLARE @hDoc4 int                                                                
   EXEC sp_xml_preparedocument @hDoc4 OUTPUT, @strRow                                                                
                                                                  
    insert into  DycFormConfigurationDetails(numFormId,numFieldId,intRowNum,intColumnNum,
    numUserCntId,numDomainId,numRelCntType,bitCustom,tintPageType)                        
 select @numFormId,*,@tintPageType from (                           
SELECT * FROM OPENXML (@hDoc4,'/NewDataSet/Table',2)                                                                
 WITH  (                                                            
  numFieldID numeric(9),                        
  tintRow numeric(9),                        
  intColumn numeric(9),                        
  numUserCntId numeric(9),                        
  numDomainID numeric(9),        
numRelCntType numeric(9),        
bitCustomField bit))X WHERE numFieldID NOT IN (SELECT numFieldID FROM DycFormConfigurationDetails where numUserCntId=@numUserCntID and numDomainId=@numDomainId 
			and intColumnNum >= @intcoulmn and  numRelCntType=@numRelCntType AND numFormId=@numFormID
			AND tintPageType=@tintPageType  AND ISNULL(bitDefaultByAdmin,0)=1)
  
  
UPDATE D  SET D.intRowNum=tintRow,D.intColumnNum=intColumn 
FROM OPENXML (@hDoc4,'/NewDataSet/Table',2)                                                                
 WITH  (                                                            
  numFieldID numeric(9),                        
  tintRow numeric(9),                        
  intColumn numeric(9),                        
  numUserCntId numeric(9),                        
  numDomainID numeric(9),        
numRelCntType numeric(9),        
bitCustomField bit)X LEFT JOIN DycFormConfigurationDetails AS D ON X.numFieldID=D.numFieldID WHERE X.numFieldID IN (SELECT numFieldID FROM DycFormConfigurationDetails where numUserCntId=@numUserCntID and numDomainId=@numDomainId 
			 and  numRelCntType=@numRelCntType AND numFormId=@numFormID
			AND tintPageType=@tintPageType  AND ISNULL(bitDefaultByAdmin,0)=1) AND tintPageType=@tintPageType AND 
			 D.numUserCntID=@numUserCntID and D.numDomainId=@numDomainId 
			 and  D.numRelCntType=@numRelCntType AND numFormId=@numFormID
			AND tintPageType=@tintPageType  AND ISNULL(bitDefaultByAdmin,0)=1

                           
end                          
                       
                
EXEC sp_xml_removedocument @hDoc4
GO
