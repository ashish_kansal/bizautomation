/****** Object:  StoredProcedure [dbo].[usp_GetAccounting]    Script Date: 07/26/2008 16:16:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getaccounting')
DROP PROCEDURE usp_getaccounting
GO
CREATE PROCEDURE [dbo].[usp_GetAccounting]
	@vcDomainName varchar(30),
	@numCompanyID numeric(9)   
--
as

select QA.* from CompanyInfo CI,QBAccounting QA,Domain D where 
 CI.vcCompanyName=QA.vcCompanyName and D.numDomainID=CI.numDomainID
and D.vcDomainName=@vcDomainName and CI.numCompanyID=@numCompanyID
 and QA.numLastUpdateDate in (SELECT Max(numLastUpdateDate) from QBAccounting where vcCompanyName=CI.vcCompanyName)
order by QA.numLastUpdateDate desc
GO
