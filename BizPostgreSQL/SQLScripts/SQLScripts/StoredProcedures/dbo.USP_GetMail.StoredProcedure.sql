/****** Object:  StoredProcedure [dbo].[USP_GetMail]    Script Date: 07/26/2008 16:17:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getmail')
DROP PROCEDURE usp_getmail
GO
CREATE PROCEDURE [dbo].[USP_GetMail]                
@numEmailHstrID NUMERIC,
@ClientTimeZoneOffset AS INT=0
as   

update  emailHistory set bitIsRead= 1 where numEmailHstrID = @numEmailHstrID 
              
select
vcFrom   as FromEmail,
vcTo   as ToEmail,
vcCC  as CCEmail,
vcBCC as BCCEmail,
EH.numEmailHstrID,                  
ROW_NUMBER() OVER (ORDER BY EH.numEmailHstrID ASC) AS  [count],                  
isnull(EH.vcSubject,'') as Subject,                          
isnull(EH.vcBody,'') as Body,                  
isnull(EH.bintCreatedOn,getutcdate()) as created,                  
isnull(EH.vcItemId,'') as ItemId,                  
isnull(EH.vcChangeKey,'') as ChangeKey,                  
isnull(EH.bitIsRead,'False') as IsRead,                  
isnull(EH.vcSize,0) as Size,                  
isnull(EH.bitHasAttachments,'False') as HasAttachments,                  
DATEADD(minute,-@ClientTimeZoneOffset,ISNULL(EH.dtReceivedOn,getutcdate())) as sent,
isnull(EH.tintType,1) as type,                  
isnull(EH.chrSource,'B') as Source ,                 
isnull(dtl.vcFileName,'') as AttachmentPath       ,          
isnull(EH.vcBodyText,'') as vcBodyText  ,  
EH.numListItemId AS numEmailStatusId,      
dtl.vcAttachmentItemId as AttachmentItemId   ,      
ISNULL(dtl.vcLocation,'') AS vcLocation,
EH.numDomainID,      
EH.numUserCntId,EH.numUid ,
isnull(EM.numContactID,0) as numContactID,
ISNULL(ADC.numDivisionId,0) AS numDivisionId,
(SELECT TOP 1 DM.tintCRMtype FROM dbo.DivisionMaster DM WHERE DM.numDivisionID = ISNULL(ADC.numDivisionId,0) ) AS tintCRMType    ,
IT.vcNodeName AS vcNodeName ,
EH.numNodeId   
from emailHistory  EH
JOIN EmailMaster EM on EH.numEMailId=EM.numEMailId    
left join AdditionalContactsInformation As ADC on ADC.numContactId=ISNULL(EM.numContactId,0)            
left join EmailHstrAttchDtls dtl on EH.numEmailHstrID = dtl.numEmailHstrID  
left join InboxTreeSort	as IT ON EH.numNodeId=IT.numNodeID            
where EH.numEmailHstrID = @numEmailHstrID
GO
