--Created By Anoop Jayaraj

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_managepickwarehouse')
DROP PROCEDURE usp_managepickwarehouse
GO
CREATE PROCEDURE USP_ManagePickWareHouse
@numDomainId as numeric(9),    
@strWarehouse as varchar(4000)  
as  
  
  
declare @separator_position as integer  
declare @strPosition as varchar(1000)  
  
delete from WareHouseForSalesFulfillment where numDomainID = @numDomainId   
   
  
   while patindex('%,%' , @strWarehouse) <> 0  
    begin -- Begin for While Loop  
      select @separator_position =  patindex('%,%' , @strWarehouse)  
    select @strPosition = left(@strWarehouse, @separator_position - 1)  
       select @strWarehouse = stuff(@strWarehouse, 1, @separator_position,'')  
     insert into WareHouseForSalesFulfillment (numWareHouseID,numDomainID)  
     values (@strPosition,@numDomainID)  
   
    
   end

