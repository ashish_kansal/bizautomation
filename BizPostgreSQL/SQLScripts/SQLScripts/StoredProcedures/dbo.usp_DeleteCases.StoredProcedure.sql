/****** Object:  StoredProcedure [dbo].[usp_DeleteCases]    Script Date: 07/26/2008 16:15:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_deletecases')
DROP PROCEDURE usp_deletecases
GO
CREATE PROCEDURE [dbo].[usp_DeleteCases]
	@numCaseID numeric(9)=0,
	@numCreatedBy numeric(9)=0   
--
 AS
BEGIN
	DELETE from Cases where numCaseID=@numCaseID and numCreatedBy=@numCreatedBy
	END
GO
