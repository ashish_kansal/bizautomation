/****** Object:  StoredProcedure [dbo].[usp_GetEmailFromUserID]    Script Date: 07/26/2008 16:17:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getemailfromuserid')
DROP PROCEDURE usp_getemailfromuserid
GO
CREATE PROCEDURE [dbo].[usp_GetEmailFromUserID]
	@numUserId numeric
AS
	Select vcEmail From AdditionalContactsInformation where numContactId = (Select numUserDetailID from userMaster where numUserID = @numUserID)
GO
