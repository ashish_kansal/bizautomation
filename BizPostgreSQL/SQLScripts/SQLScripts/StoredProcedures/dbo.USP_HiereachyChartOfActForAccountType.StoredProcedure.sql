/****** Object:  StoredProcedure [dbo].[USP_HiereachyChartOfActForAccountType]    Script Date: 07/26/2008 16:18:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created By Siva
 GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_hiereachychartofactforaccounttype')
DROP PROCEDURE usp_hiereachychartofactforaccounttype
GO
CREATE PROCEDURE [dbo].[USP_HiereachyChartOfActForAccountType]       
@numDomainID as numeric(9)=0,      
@numParentAcntID as numeric(9)=0      
as      
      
      
Delete from HierChartOfAct where numDomainID=@numDomainID      
exec USP_ArranageHierChartAct @numParentAcntID,@numDomainID      
      
select numChartOfAcntID,vcCategoryName from HierChartOfAct      
where numDomainID=@numDomainID and  numChartOfAcntID<> @numParentAcntID  And (numAcntType=813 or numAcntType =817)
GO
