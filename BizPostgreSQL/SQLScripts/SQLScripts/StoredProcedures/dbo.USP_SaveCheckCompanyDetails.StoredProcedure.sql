/****** Object:  StoredProcedure [dbo].[USP_SaveCheckCompanyDetails]    Script Date: 07/26/2008 16:20:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created By Siva    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_savecheckcompanydetails')
DROP PROCEDURE usp_savecheckcompanydetails
GO
CREATE PROCEDURE [dbo].[USP_SaveCheckCompanyDetails]    
(@numCheckCompanyId as numeric(9)=0,    
@numCompanyId as numeric(9)=0,    
@monAmount as DECIMAL(20,5)=0,    
@vcMemo as varchar(200) ='',    
@numUserCntID as numeric(9)=0,    
@numDomainID as numeric(9)=0)    
AS    
Begin    
 If @numCheckCompanyId=0    
  Begin    
   Insert into CheckCompanyDetails(numCompanyId,monAmount,vcMemo,numDomainId,numCreatedBy,dtCreatedDate) Values    
   (@numCompanyId,@monAmount,@vcMemo,@numDomainId,@numUserCntID,getutcdate())    
  End    
 Else    
  Begin    
   Update CheckCompanyDetails Set numCompanyId=@numCompanyId ,monAmount=@monAmount,vcMemo=@vcMemo,    
   numDomainId=@numDomainId,numModifiedBy=@numUserCntID,dtModifiedDate=getutcdate()    
 Where numCheckCompanyId=@numCheckCompanyId  
  End    
End
GO
