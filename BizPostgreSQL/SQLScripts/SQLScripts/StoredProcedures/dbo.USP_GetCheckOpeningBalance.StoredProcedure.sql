/****** Object:  StoredProcedure [dbo].[USP_GetCheckOpeningBalance]    Script Date: 07/26/2008 16:16:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created By Siva  
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getcheckopeningbalance')
DROP PROCEDURE usp_getcheckopeningbalance
GO
CREATE PROCEDURE  [dbo].[USP_GetCheckOpeningBalance]  
@numAccountId as numeric(9)=0,  
@numDomainId as numeric(9)=0  
As  
Begin  
	Select isnull(numOpeningBal,0) from Chart_Of_Accounts Where @numAccountId=numAccountId And numDomainId=@numDomainId  
End
GO
