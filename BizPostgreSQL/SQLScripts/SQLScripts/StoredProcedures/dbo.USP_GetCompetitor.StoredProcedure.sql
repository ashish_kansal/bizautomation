/****** Object:  StoredProcedure [dbo].[USP_GetCompetitor]    Script Date: 07/26/2008 16:16:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getcompetitor')
DROP PROCEDURE usp_getcompetitor
GO
CREATE PROCEDURE [dbo].[USP_GetCompetitor]         
@numDomainID as numeric(9),    
@numItemCode as numeric(9)        
as        
select numCompetitorID,vcPrice,vcCompanyName+ ', ' +vcDivisionName as Competitor,dtDateEntered,        
vcFirstName+ ', '+vcLastName as ConName,        
convert(varchar(15),numPhone)+ ', '+ convert(varchar(15),numPhoneExtension) as Phone          
from Competitor         
join divisionMaster div        
on div.numdivisionid=numCompetitorID        
join companyInfo com        
on com.numCompanyID=div.numCompanyID        
left join AdditionalContactsInformation ADC        
on ADC.numdivisionID=Div.numdivisionID        
where  ISNULL(ADC.bitPrimaryContact,0)=1 and Competitor.numDomainID=@numDomainID  and numItemCode=@numItemCode
GO
