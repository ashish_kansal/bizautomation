/****** Object:  StoredProcedure [dbo].[usp_GetProjectStatusForReport]    Script Date: 07/26/2008 16:18:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj             
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getprojectstatusforreport')
DROP PROCEDURE usp_getprojectstatusforreport
GO
CREATE PROCEDURE [dbo].[usp_GetProjectStatusForReport]                 
 @numDomainID numeric,                  
 @dtDateFrom datetime,                  
 @numUserCntID numeric=0,                               
 @intType numeric=0,                
 @tintRights tinyint=1,
 @ClientTimeZoneOffset AS integer               
  --                
AS                  
BEGIN              
        
      
if @dtDateFrom = 'Jan  1 1753 12:00:00:000AM' set @dtDateFrom=null      
      
 If @tintRights=1                
 BEGIN                  
 IF @dtDateFrom is null                 
 BEGIN       
             
  SELECT distinct(PM.numProID),PS.bintStageComDate as DueDate,                
  PM.vcProjectName as ProjectName,                
  dbo.GetProLstMileStoneCompltd(PM.numProId) as LastMileStoneCompleted,                 
  dbo.GetProLstStageCompltd(PM.numProId,@ClientTimeZoneOffset) as LastStageCompleted                
  FROM Projectsmaster PM               
  left join ProjectsStageDetails Ps               
  on PS.numProId=PM.numProId                
  where PM.numDomainID=@numDomainID            
  and PM.numRecOwner=@numUserCntID              
 END                
 ELSE                
 BEGIN           
         
    SELECT distinct(PM.numProID),PS.bintStageComDate as DueDate,                
  PM.vcProjectName as ProjectName,                
  dbo.GetProLstMileStoneCompltd(PM.numProId) as LastMileStoneCompleted,                 
  dbo.GetProLstStageCompltd(PM.numProId,@ClientTimeZoneOffset) as LastStageCompleted                
  FROM Projectsmaster PM               
  left join  ProjectsStageDetails Ps               
  on PS.numProId=PM.numProId                
  where PM.numDomainID=@numDomainID               
  and PM.numRecOwner= @numUserCntID              
  AND PS.bintStageComDate <= @dtDateFrom and  PS.bintStageComDate<>null              
                
 END                
                
 END                  
                  
 If @tintRights=2                
 BEGIN                  
 IF @dtDateFrom is null               
 BEGIN                
    SELECT distinct(PM.numProID),PS.bintStageComDate as DueDate,                
  PM.vcProjectName as ProjectName,                
  dbo.GetProLstMileStoneCompltd(PM.numProId) as LastMileStoneCompleted,                 
  dbo.GetProLstStageCompltd(PM.numProId,@ClientTimeZoneOffset) as LastStageCompleted                
  FROM Projectsmaster PM               
  left join ProjectsStageDetails Ps               
  on PS.numProId=PM.numProId        
  left outer join AdditionalContactsInformation ACI  
  on PM.numRecOwner=ACI.numContactId          
  where PM.numDomainID=@numDomainID  
     and ACI.numTeam in(select F.numTeam from ForReportsByTeam F       -- Added By Anoop                  
    where F.numUserCntID=@numUserCntID and F.numDomainid=@numDomainId and F.tintType=@intType)              
 END                
 ELSE                
 BEGIN                
    SELECT distinct(PM.numProID),PS.bintStageComDate as DueDate,                
  PM.vcProjectName as ProjectName,                
  dbo.GetProLstMileStoneCompltd(PM.numProId) as LastMileStoneCompleted,                 
  dbo.GetProLstStageCompltd(PM.numProId,@ClientTimeZoneOffset) as LastStageCompleted                
  FROM Projectsmaster PM               
  left join  ProjectsStageDetails Ps               
  on PS.numProId=PM.numProId       
  left outer join AdditionalContactsInformation ACI  
  on PM.numRecOwner=ACI.numContactId             
  where PM.numDomainID=@numDomainID               
  and ACI.numTeam in(select F.numTeam from ForReportsByTeam F       -- Added By Anoop                  
    where F.numUserCntID=@numUserCntID and F.numDomainid=@numDomainId and F.tintType=@intType)              
  AND PS.bintStageComDate <= @dtDateFrom and  PS.bintStageComDate<>null                 
                
 END                
                
 END                  
                  
 If @tintRights=3                
 BEGIN                  
 IF @dtDateFrom  is null                 
 BEGIN                
    SELECT distinct(PM.numProID),PS.bintStageComDate as DueDate,     
  PM.vcProjectName as ProjectName,                
  dbo.GetProLstMileStoneCompltd(PM.numProId) as LastMileStoneCompleted,                 
  dbo.GetProLstStageCompltd(PM.numProId,@ClientTimeZoneOffset) as LastStageCompleted                
  FROM Projectsmaster PM               
  left join ProjectsStageDetails Ps               
  on PS.numProId=PM.numProId     
  left outer join DivisionMaster DM  
  on PM.numDivisionId=DM.numDivisionID  
            
  and DM.numTerID in(select F.numTerritory from ForReportsByTerritory F      -- Added By Anoop                   
    where F.numUserCntID=@numUserCntID and F.numDomainid=@numDomainID and F.tintType=@intType)                
  and PM.numRecOwner= @numUserCntID              
 END                
 ELSE                
 BEGIN                
    SELECT distinct(PM.numProID),PS.bintStageComDate as DueDate,                
  PM.vcProjectName as ProjectName,                
  dbo.GetProLstMileStoneCompltd(PM.numProId) as LastMileStoneCompleted,                 
  dbo.GetProLstStageCompltd(PM.numProId,@ClientTimeZoneOffset) as LastStageCompleted                
  FROM Projectsmaster PM               
  left join  ProjectsStageDetails Ps               
  on PS.numProId=PM.numProId   
  left outer join DivisionMaster DM  
  on PM.numDivisionId=DM.numDivisionID               
  where PM.numDomainID=@numDomainID               
   and DM.numTerID in(select F.numTerritory from ForReportsByTerritory F      -- Added By Anoop                   
    where F.numUserCntID=@numUserCntID and F.numDomainid=@numDomainID and F.tintType=@intType)              
  AND PS.bintStageComDate <= @dtDateFrom  and  PS.bintStageComDate<>null               
                
 END                
                
 END                  
 END
GO
