/****** Object:  StoredProcedure [dbo].[usp_GetTableInfoDefaultPortal]    Script Date: 07/26/2008 16:18:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created By                                                
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_gettableinfodefaultportal')
DROP PROCEDURE usp_gettableinfodefaultportal
GO
CREATE PROCEDURE [dbo].[usp_GetTableInfoDefaultPortal]                                                              
@numUserCntID numeric=0,              
@numRecordID numeric=0,                          
@numDomainId numeric=0,                          
@charCoType char(1) = 'a',              
@pageId as numeric,              
@numRelCntType as numeric              
                     
                          
--                                                 
                                     
AS                            
                          
if((select count(*) from PageLayoutdtl DTL where DTL.numUserCntId=@numuserCntid and DTl.numDomainId = @numDomainId and DTl.Ctype=@charCoType and DTL.numRelCntType=@numRelCntType) <> 0 )                   
            
             
BEGIN                 
 SELECT HDR.numFieldId,HDR.vcFieldName,'' as vcURL,'' as fld_type,DTL.tintRow,DTL.intcoulmn,convert(char(1),DTL.bitCustomField)as bitCustomField,HDR.vcDBColumnName,'0' as TabId,'' as tabname from PageLayoutdtl DTL                          
join pagelayout HDR on DTl.numFieldId= HDR.numFieldId                          
 where HDR.Ctype = @charCoType and numUserCntId=@numUserCntId  and bitCustomField=0 and  numDomainId = @numDomainId and DTL.numRelCntType=@numRelCntType order by DTL.intcoulmn,DTL.tintrow                      
      
if @pageid=1 or @pageid=4      
begin      
select fld_id as numFieldId,fld_label as vcfieldName ,vcURL,fld_type,       
(SELECT DTL.tintRow  FROM PageLayoutDTL DTL WHERE DTL.numUserCntID=@numUserCntID and  DTL.numDomainID=@numDomainID       
 and DTL.numRelCntType=@numRelCntType and DTL.ctype=@charCoType and bitCustomField=1 and DTL.numFieldId =fld_id)as tintRow,        
(SELECT DTL.intcoulmn  FROM PageLayoutDTL DTL  WHERE DTL.numUserCntID=@numUserCntID and  DTL.numDomainID=@numDomainID        
and DTL.numRelCntType=@numRelCntType and DTL.ctype=@charCoType and bitCustomField=1 and DTL.numFieldId =fld_id)as intcoulmn,            
case when fld_type = 'SelectBox' then (select vcData from listdetails where numlistitemid=dbo.GetCustFldValue(fld_id,@PageId,@numRecordID))      
else dbo.GetCustFldValue(fld_id,@PageId,@numRecordID) end as vcDBColumnName,            
 '1' as bitCustomField,'0' as tabletype ,subgrp as TabId,Grp_Name as tabname from CFW_Fld_Master join CFW_Fld_Dtl                                
on Fld_id=numFieldId                                
left join CFw_Grp_Master                                 
on subgrp=CFw_Grp_Master.Grp_id                                
where CFW_Fld_Master.grp_id=@PageId and numRelation=@numRelCntType and CFW_Fld_Master.numDomainID=@numDomainID  order by subgrp,numOrder               
end      
      
if @PageId= 2 or  @PageId= 3 or @PageId= 5 or @PageId= 6 or @PageId= 7 or @PageId= 8                  
begin                          
select fld_id as numFieldId,fld_label as vcfieldName,vcURL,fld_type,      
(SELECT DTL.tintRow  FROM PageLayoutDTL DTL WHERE DTL.numUserCntID=@numUserCntID and  DTL.numDomainID=@numDomainID       
 and DTL.numRelCntType=@numRelCntType and DTL.ctype=@charCoType and bitCustomField=1 and DTL.numFieldId =fld_id)as tintRow,        
(SELECT DTL.intcoulmn  FROM PageLayoutDTL DTL  WHERE DTL.numUserCntID=@numUserCntID and  DTL.numDomainID=@numDomainID        
and DTL.numRelCntType=@numRelCntType and DTL.ctype=@charCoType and bitCustomField=1 and DTL.numFieldId =fld_id)as intcoulmn,           
case when fld_type = 'SelectBox' then (select vcData from listdetails where numlistitemid=dbo.GetCustFldValue(fld_id,@PageId,@numRecordID))      
else dbo.GetCustFldValue(fld_id,@PageId,@numRecordID) end as vcDBColumnName,          
 '1' as bitCustomField,'0' as tabletype,subgrp as TabId,Grp_Name as tabname  from CFW_Fld_Master                           
left join CFw_Grp_Master                           
on subgrp=CFw_Grp_Master.Grp_id                          
where CFW_Fld_Master.grp_id=@PageId and CFW_Fld_Master.numDomainID=@numDomainID                      
end      
      
end       
      
                         
else                          
begin       
                         
SELECT numFieldID,vcFieldName,'' as vcURL,'' as fld_type,vcDBColumnName,tintRow,intcolumn as intcoulmn,'0' as bitCustomField,Ctype,'0' as TabId ,'' as tabname from PageLayout where Ctype = @charCoType       
 order by intcoulmn,tintrow                        
if @pageid=1 or @pageid=4        
begin          
select fld_id as numFieldId,fld_label as vcfieldName ,vcURL,fld_type,             
case when fld_type = 'SelectBox' then (select vcData from listdetails where numlistitemid=dbo.GetCustFldValue(fld_id,@PageId,@numRecordID))      
else dbo.GetCustFldValue(fld_id,@PageId,@numRecordID) end as vcDBColumnName,            
convert(tinyint,0) as tintRow,convert(int,0) as intcolumn,'1' as bitCustomField,'1' as tabletype,subgrp as TabId,Grp_Name as tabname from CFW_Fld_Master join CFW_Fld_Dtl                                
on Fld_id=numFieldId                                
left join CFw_Grp_Master                                 
on subgrp=CFw_Grp_Master.Grp_id                                
where CFW_Fld_Master.grp_id=@PageId and numRelation=@numRelCntType and CFW_Fld_Master.numDomainID=@numDomainID   order by subgrp,numOrder               
end       
if @PageId= 2 or  @PageId= 3 or @PageId= 5 or @PageId= 6 or @PageId= 7 or @PageId= 8                  
begin                          
select fld_id as numFieldId,fld_label as vcfieldName ,vcURL,fld_type,           
case when fld_type = 'SelectBox' then (select vcData from listdetails where numlistitemid=dbo.GetCustFldValue(fld_id,@PageId,@numRecordID))      
else dbo.GetCustFldValue(fld_id,@PageId,@numRecordID) end as vcDBColumnName,          
convert(tinyint,0) as tintRow,convert(int,0) as intcolumn,'1' as bitCustomField,'1' as tabletype,subgrp as TabId,Grp_Name as tabname from CFW_Fld_Master                           
left join CFw_Grp_Master                           
on subgrp=CFw_Grp_Master.Grp_id                          
where CFW_Fld_Master.grp_id=@PageId and CFW_Fld_Master.numDomainID=@numDomainID                 
end              
END
GO
