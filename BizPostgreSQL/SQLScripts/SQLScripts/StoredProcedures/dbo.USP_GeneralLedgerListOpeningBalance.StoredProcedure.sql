/****** Object:  StoredProcedure [dbo].[USP_GeneralLedgerListOpeningBalance]    Script Date: 07/26/2008 16:16:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created By Siva                                                                                                                                                    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_generalledgerlistopeningbalance')
DROP PROCEDURE usp_generalledgerlistopeningbalance
GO
CREATE PROCEDURE [dbo].[USP_GeneralLedgerListOpeningBalance]                                                                                                                                                                
@dtFromDate as Datetime,                                                                      
@dtToDate as Datetime,      
@numDomainId as numeric(9),
@monBalance as DECIMAL(20,5)=0                                                                                                                                                     
As                                                                                                                                                                
Begin                                                                                                                                                           
Declare @strSQL as varchar(8000)                                                                                                                                                      
Declare @strChildCategory as varchar(5000)                                                                                                              
Declare @numAcntTypeId as integer                                                                                                                                                 
Declare @OpeningBal as varchar(8000)                                                                            
Declare @numAcntOpeningBal as integer                                                                                                                                              
Set @openingBal='Opening Balance'                                                                                                                                  
Set @strSQL =''                                                                                                                              
--Create a Temporary table to hold data                                                                                                                                                                      
Create table #tempTable ( ID INT IDENTITY PRIMARY KEY,                                                                                                                                                                                
 numCustomerId numeric(9),                                                                                                                                  
 JournalId numeric(9),                                                                              
 TransactionType varchar(500),                                                                                                                                
 EntryDate datetime,                                                                                                                                  
 CompanyName varchar(8000),                                                                                                                        
 CheckId numeric(9),                                                                                                                      
 CashCreditCardId numeric(9),                                                                                                                                  
 Memo varchar(8000),                                                                                                                                  
 Payment DECIMAL(20,5),                    
 Deposit DECIMAL(20,5),                            
 numBalance DECIMAL(20,5),                                              
 numChartAcntId numeric(9),  
 numTransactionId numeric(9),     
 AcntTypeDescription  varchar(50),                            
 numOppId numeric(9),                              
 numOppBizDocsId numeric(9),                          
 numDepositId numeric(9),                        
 numBizDocsPaymentDetId numeric(9)                        
)                                                                                                            
                                                                                                                        
   if @dtFromDate='' Set @dtFromDate='Jan  1 1753 12:00:00:000AM'                                                              
                                                                     
                                                                                             
        
                                   
Set @strSQL='  Select GJD.numCustomerId,GJH.numJournal_Id as JournalId,                                                                                
case when isnull(GJH.numCheckId,0) <> 0 then +''Checks'' Else case when isnull(GJH.numCashCreditCardId,0) <> 0 And CCCD.bitMoneyOut=0 then ''Cash''                                                                        
ELse Case when isnull(GJH.numCashCreditCardId,0) <> 0 And CCCD.bitMoneyOut=1 And CCCD.bitChargeBack=1  then  ''Charge''                                                                           
ELse Case when isnull(GJH.numCashCreditCardId,0) <> 0 And CCCD.bitMoneyOut=1 And CCCD.bitChargeBack=0 then ''Credit''                              
ELse Case when isnull(GJH.numOppId,0) <> 0 And isnull(GJH.numOppBizDocsId,0) <> 0 And isnull(GJH.numBizDocsPaymentDetId,0)=0 And Opp.tintOppType=1 then ''BizDocs Invoice''                         
ELse Case when isnull(GJH.numOppId,0) <> 0 And isnull(GJH.numOppBizDocsId,0) <> 0 And isnull(GJH.numBizDocsPaymentDetId,0)=0 And Opp.tintOppType=2 then ''BizDocs Purchase''                       
ELse Case when isnull(GJH.numDepositId,0) <> 0 then ''Deposit''                             
ELse Case when isnull(GJH.numBizDocsPaymentDetId,0)<>0 And Opp.tintOppType=1 then ''Receive Amt''                    
ELse Case when isnull(GJH.numBizDocsPaymentDetId,0)<>0 And Opp.tintOppType=2 then ''Vendor Amt''                                                                         
Else Case When GJH.numJournal_Id <>0 then ''Journal'' End End End End End End End End End End  as TransactionType,                                                                                                     
GJH.datEntry_Date as EntryDate,CI.vcCompanyName CompanyName,GJH.numCheckId As CheckId,             
GJH.numCashCreditCardId as CashCreditCardId,                                                                                                  
GJD.varDescription as Memo,                                                                                            
(case when GJD.numCreditAmt=0 then GJD.numdebitAmt  end) as Deposit ,         
(case when GJD.numdebitAmt=0 then GJD.numCreditAmt end) as Payment,                                                                                                        
null numBalance,  
isnull(GJD.numChartAcntId,0) as numChartAcntId,    
GJD.numTransactionId as numTransactionId,                                                       
LD.vcData as AcntTypeDescription,                            
GJH.numOppId as numOppId,                              
GJH.numOppBizDocsId as numOppBizDocsId,                          
GJH.numDepositId as numDepositId,                        
GJH.numBizDocsPaymentDetId as numBizDocsPaymentDetId                                                                     
From General_Journal_Header as GJH                                                                                                                             
Left Outer Join General_Journal_Details as GJD  on GJH.numJournal_Id=GJD.numJournalId                              
Left outer join DivisionMaster as DM on GJD.numCustomerId=DM.numDivisionID                                                                                           
Left outer join CompanyInfo as CI on  DM.numCompanyID=CI.numCompanyId                                                                 
Left outer join Chart_Of_Accounts CA on GJD.numChartAcntId=CA.numAccountId                                                                                
Left outer join CashCreditCardDetails CCCD on GJH.numCashCreditCardId=CCCD.numCashCreditId                                                                            
Left outer join CheckDetails CD on GJH.numCheckId=CD.numCheckId                                                                            
Left outer join ListDetails LD on CA.numAcntType= LD.numListItemID                  
Left outer join OpportunityMaster Opp on GJH.numOppId=Opp.numOppId  
Where  GJH.numDomainId='+Convert(varchar(10),@numDomainId)+                                                                  
' And (CA.numAcntType=815 or CA.numAcntType=816 or CA.numAcntType=820  or CA.numAcntType=821  or CA.numAcntType=822  or CA.numAcntType=825 or CA.numAcntType=827)
 And GJH.datEntry_Date<='''+convert(varchar(300), @dtFromDate)+'''
 order by EntryDate'                                       
                                                                                                         
                                                                                                           
                          
 Set @strSQL= ' union all Select GJD.numCustomerId,GJH.numJournal_Id as JournalId,                                                                              
case when isnull(GJH.numCheckId,0) <> 0 then +''Checks'' Else case when isnull(GJH.numCashCreditCardId,0) <> 0 And CCCD.bitMoneyOut=0 then ''Cash''                                                                          
ELse Case when isnull(GJH.numCashCreditCardId,0) <> 0 And CCCD.bitMoneyOut=1 And CCCD.bitChargeBack=1  then ''Charge''                                                            
ELse Case when isnull(GJH.numCashCreditCardId,0) <> 0 And CCCD.bitMoneyOut=1 And CCCD.bitChargeBack=0 then ''Credit''                               
ELse Case when isnull(GJH.numOppId,0) <> 0 And isnull(GJH.numOppBizDocsId,0) <> 0 And isnull(GJH.numBizDocsPaymentDetId,0)=0 And Opp.tintOppType=1 then ''BizDocs Invoice''                         
ELse Case when isnull(GJH.numOppId,0) <> 0 And isnull(GJH.numOppBizDocsId,0) <> 0 And isnull(GJH.numBizDocsPaymentDetId,0)=0 And Opp.tintOppType=2 then ''BizDocs Purchase''                            
ELse Case when isnull(GJH.numDepositId,0) <> 0 then ''Deposit''                           
ELse Case when isnull(GJH.numBizDocsPaymentDetId,0)<>0 And Opp.tintOppType=1 then ''Receive Amt''                    
ELse Case when isnull(GJH.numBizDocsPaymentDetId,0)<>0 And Opp.tintOppType=2 then ''Vendor Amt''                                                                         
Else Case When GJH.numJournal_Id <>0 then ''Journal'' End End End End End End End End End End  as TransactionType,                                                                                                    
GJH.datEntry_Date as EntryDate,CI.vcCompanyName CompanyName,GJH.numCheckId As CheckId,                                                                          
GJH.numCashCreditCardId as CashCreditCardId,                                  
GJD.varDescription as Memo,                                                             
(case when GJD.numCreditAmt<>0 then GJD.numCreditAmt end) as Payment,                                                        
(case when GJD.numdebitAmt<>0 then GJD.numDebitAmt  end) as Deposit,                                                         
null numBalance,  
isnull(GJD.numChartAcntId,0) as numChartAcntId,  
GJD.numTransactionId as numTransactionId,  
LD.vcData as AcntTypeDescription,  
GJH.numOppId as numOppId,  
GJH.numOppBizDocsId as numOppBizDocsId,                          
GJH.numDepositId as numDepositId,                        
GJH.numBizDocsPaymentDetId as numBizDocsPaymentDetId   
From General_Journal_Header as GJH                                                                                                                  
Left Outer Join General_Journal_Details as GJD on GJH.numJournal_Id=GJD.numJournalId                                                                                                           
Left outer join DivisionMaster as DM on GJD.numCustomerId=DM.numDivisionID                                                                                                                                         
Left outer join CompanyInfo as CI on  DM.numCompanyID=CI.numCompanyId                                                                  
Left outer join Chart_Of_Accounts CA on GJD.numChartAcntId=CA.numAccountId                                                                             
Left outer join CashCreditCardDetails CCCD on GJH.numCashCreditCardId=CCCD.numCashCreditId                                                                            
Left outer join CheckDetails CD on GJH.numCheckId=CD.numCheckId                  
Left outer join OpportunityMaster Opp on GJH.numOppId=Opp.numOppId                 
Left outer join ListDetails LD on CA.numAcntType= LD.numListItemID                                                                      
Where  GJH.numDomainId='+Convert(varchar(10),@numDomainId)+                                                                  
' And (CA.numAcntType=813 or CA.numAcntType=814 or CA.numAcntType=817  or CA.numAcntType=818  or CA.numAcntType=819  or CA.numAcntType=823 or CA.numAcntType=824 Or CA.numAcntType=826)
 And GJH.datEntry_Date<='''+convert(varchar(300), @dtFromDate)+'''
 order by EntryDate'                                                          
 
                                      
                                                                          
Print (@strChildCategory)                                   Print (@strSQL)                                                            
--Exec (@strSQL)                                                                                                         
                                                               
                           
exec ('insert into #tempTable' + @strSQL)                                                                                                                                  
Declare @TotRecs as  numeric(9)                                                                                                                            
Declare @i as numeric(9)                                                                                                                                  
Declare @numCustId as varchar(9)                                                                                                                           
Declare @numPayment as DECIMAL(20,5)                                                                                     
Declare @numDeposit as DECIMAL(20,5)                                                                                             
Declare @numBalanceAmt as DECIMAL(20,5)                        
Declare @bitBalance as bit                                                                                                 
Declare @numCheckId as numeric(9)             
Declare @numCashCreditCardId as numeric(9)                                                                                                
--Declare @numCashCreditCardId as numeric(9)                                                                                                                        
Set @bitBalance=0                                                                            
Declare @j as numeric(9)                                                                                                                                  
set @i=1                                                                                                                               
Set @j=0                                                                                                                        
                      
                                                                                                 
                                                                                                                             
set @TotRecs=(select count(*) from #tempTable)                                              
print (@TotRecs)                                                                                                                    
Declare @numCustomerId as numeric(9)                                                                                               
Declare @bitChargeBack as bit                                                            
Declare @bitMoneyOut as bit                                                                              
                                                                                                                           
While @i<=@TotRecs                                                                                                              
Begin                                                             
 Select @numCustId=isnull(numCustomerId,0),@numPayment=isnull(Payment,0),@numDeposit=isnull(Deposit,0),                        
 @numCheckId=isnull(CheckId,0),@numCashCreditCardId=isnull(CashCreditCardId,0) From #temptable Where Id=@i                                                                      
 Print '@numDeposit' +convert(varchar(30),@numDeposit)                                            
 Print '@numPayment' + convert(varchar(30),@numPayment)                                                          
 Print '@CheckId1' + convert(varchar(30),@numCheckId)                     
 Print '@numCashCreditCardId'+convert(varchar(50),@numCashCreditCardId)                                                                               
 Select @bitChargeBack=bitChargeBack,@bitMoneyOut=bitMoneyOut From CashCreditCardDetails Where numCashCreditId=@numCashCreditCardId                                                                              
 Print @bitChargeBack                                                              
 Print @bitMoneyOut                                                                                   
 If @i=1                                                                                            
 Begin                                                                                                                                  
   If @numPayment <> 0                                                                                   
  Begin                                                                                    
  Update  #temptable Set numBalance=-@numPayment Where Id=@i                                                                                          
     If @numAcntTypeId=815                        
       Begin                                                                
      If @numCheckId<>0 Or @numCashCreditCardId<>0                                                                                           
       Begin                                                                                        
          If  @numCashCreditCardId<>0                                                                 
            Begin                                                                              
              If @bitChargeBack=1 or @bitMoneyOut=0                                          
                   Update  #temptable Set Deposit=null,Payment=-@numPayment Where Id=@i                                                                               
              Else                                 
                     Update  #temptable Set Deposit=null,Payment=@numPayment Where Id=@i                                                                                             
             End                                                                              
            Else                                                            
                   Update  #temptable Set Deposit=null,Payment=-@numPayment Where Id=@i                                                                          
        End                                                                                        
      Else                                               
     Begin                                           
       Print 'Shlok - SP'                                          
        Update  #temptable Set Deposit=-@numPayment,Payment=null Where Id=@i                                                                                               
      End                                          
     End                                                                                            
     If @numAcntTypeId=814                                                    
      Update  #temptable Set Deposit=null,Payment=-@numPayment Where Id=@i                                                                                               
    If @numAcntTypeId <> 814 And @numAcntTypeId <> 815                                                                        
        Update  #temptable Set Deposit=null,Payment=-@numPayment Where Id=@i                                                                                                                      
  End                                                                                           
  Else                                                                          
    Begin                                                                                   
     Print 'SP'                                                                                                                       
     Update  #temptable Set numBalance=@numDeposit Where Id=@i                                                            
     Print '@numAcntTypeId' +convert(varchar(30),@numAcntTypeId)                                                          
   If @numAcntTypeId=815                                                                                                   
    Begin                                                                              
     If @numCheckId<>0 Or @numCashCreditCardId<>0                                                                                          
        Begin                                                                                        
           If  @numCashCreditCardId<>0                                                                               
            Begin                                                                              
        If @bitChargeBack=1 or @bitMoneyOut=0                           
                   Update  #temptable Set Deposit=null,Payment=@numDeposit Where Id=@i                                                                                                  
               Else                                         
                Update  #temptable Set Deposit=null,Payment=-@numDeposit Where Id=@i                                                                                                  
              End                                                                                
        Else                                                                              
            Update  #temptable Set Deposit=null,Payment=-@numDeposit Where Id=@i                                                                          
                                                                                        
       End                                                                                        
 Else                                                                                            
        Update  #temptable Set Deposit=@numDeposit,Payment=null Where Id=@i                                                                                                  
    End                      
    If @numAcntTypeId=814                                                                
    Update  #temptable Set Deposit=null,Payment=@numDeposit Where Id=@i                                                                           
    If @numAcntTypeId <> 814 And @numAcntTypeId <> 815                                                                        
       Update  #temptable Set Deposit=@numDeposit,Payment=null Where Id=@i                   
    End                                                                                                  
  End                                                             
                                                                                                  
If @i>1                               
Begin                                                                                                                    
If @numPayment <> 0                                                                                                                                  
Begin                                                                                                                      
  Set @j=@i-1                                                                                                   
   Select @numBalanceAmt=numBalance From #temptable Where id=@j                                                                                                                                  
   Update  #temptable Set numBalance=@numBalanceAmt-@numPayment Where Id=@i                                                                                               
    If @numAcntTypeId=815                                     
     Begin                                                            
      If @numCheckId<>0 Or @numCashCreditCardId<>0                                                                                            
       Begin                                                                                    
    If @numCashCreditCardId<>0                                                                               
      Begin                                                                              
       If @bitChargeBack=1 or @bitMoneyOut=0                                                                               
         Update  #temptable Set Deposit=null,Payment=-@numPayment Where Id=@i                                                                                
             Else                                                                              
         Update  #temptable Set Deposit=null,Payment=@numPayment Where Id=@i                                                          
         End                
      Else                                            
         Update  #temptable Set Deposit=null,Payment=-@numPayment Where Id=@i                                                                                
                                                                               
       End                                                        
    Else                                                
Begin                                                                                      
                                            
        Update  #temptable Set Deposit=-@numPayment,Payment=null Where Id=@i                                                                                                 
     End                                          
    End                                               
   If @numAcntTypeId=814                                                                                                             
  Update  #temptable Set Deposit=null,Payment=-@numPayment Where Id=@i                                                                        
   If @numAcntTypeId <> 814 And @numAcntTypeId <> 815                                                   
    Update  #temptable Set Deposit=null,Payment=-@numPayment Where Id=@i                                                                                     
End                                                                                                                                  
Else                                                                                                           
Begin                                                                                   
   Set @j=@i-1                                                                        
    print '@CheckId2' + convert(varchar(30),@numCheckId)                                                                                                                                  
   Select @numBalanceAmt=numBalance From #temptable Where id=@j                                                                                                         
   Update  #temptable Set numBalance=@numBalanceAmt+@numDeposit Where Id=@i                                                                                                   
    If @numAcntTypeId=815                                                        
Begin                                                                                            
     If @numCheckId<>0 Or @numCashCreditCardId<>0                                                                                           
       Begin                                                                                        
      If @numCashCreditCardId<>0                                                                               
            Begin                           
              if @bitChargeBack=1 or @bitMoneyOut=0                                                                               
           Update #temptable Set Deposit=null,Payment=-@numDeposit Where Id=@i                                                                                   
             Else                                                                              
           Update  #temptable Set Deposit=null,Payment=@numDeposit Where Id=@i                                                                                   
                                                                              
           End                                                                              
           Else                                                                              
           Update  #temptable Set Deposit=null,Payment=@numDeposit Where Id=@i                                                                                   
                                                                                         
       End                                                      
     Else                                                   
      Update  #temptable Set Deposit=@numDeposit,Payment=null Where Id=@i       
     End                                                                                            
     if @numAcntTypeId=814                                 
      Update  #temptable Set Deposit=null,Payment=@numDeposit Where Id=@i                                                                   
   If @numAcntTypeId <> 814 And @numAcntTypeId <> 815                                                                        
       Update  #temptable Set Deposit=null,Payment=@numDeposit Where Id=@i                                                                               
End                                                                           
End                                                     
Set @i=@i+1                                                                                                                                  
End             
        
Select @monBalance=numBalance From #temptable Where Id=@TotRecs  
Drop table #temptable                                                                                                                                                 
End
GO
