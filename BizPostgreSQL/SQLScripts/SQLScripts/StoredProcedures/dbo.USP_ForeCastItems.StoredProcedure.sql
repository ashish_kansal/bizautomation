/****** Object:  StoredProcedure [dbo].[USP_ForeCastItems]    Script Date: 07/26/2008 16:15:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj      usp_GetItemDetailsFor     
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_forecastitems')
DROP PROCEDURE usp_forecastitems
GO
CREATE PROCEDURE [dbo].[USP_ForeCastItems]              
 @numUserCntID numeric(9)=0,                        
 @numDomainID numeric(9)=0,                      
 @tintType tinyint=1 ,                
 @numDivisionID numeric(9)=0,        
 @tintFirstMonth tinyint=0,        
 @tintSecondMonth tinyint=0,        
 @tintThirdMonth tinyint=0,        
 @intYear as int=0 ,       
 @byteMode as tinyint,    
 @numItemCode as numeric(9)           
as            
       
      
if @byteMode=0    
begin    
 Select M.Mon,isnull(monForecastAmount,0) as ForeCastAmount,isnull(monQuota,0) as QuotaAmt,    
 isnull((SELECT sum(numUnitHour)        
 FROM   OpportunityMaster    
    join OpportunityItems    
    on OpportunityMaster.numOppID= OpportunityItems.numOppID                     
 WHERE tintOppType=1 and numItemCode=@numItemCode and  tintOppStatus=1  and  numRecOwner in (case when @numUserCntID> 0 then (case when @tintType=1 then (select @numUserCntID) else (select numContactId from additionalcontactsinformation where numManagerID
  
 =@numUserCntID) end) when @numDivisionID>0 then (select numContactId from additionalcontactsinformation where numDivisionID=@numDivisionID) else (select 0) end)                        
 and numDomainID=@numDomainID And month(intPEstimatedCloseDate)=M.Mon And year(intPEstimatedCloseDate)=@intYear),0) AS sold,    
 isnull((SELECT               
 sum(numUnitHour)         
 FROM                          
 OpportunityMaster     
    join OpportunityItems    
    on OpportunityMaster.numOppID= OpportunityItems.numOppID                      
 WHERE tintOppType=1 And  tintOppStatus=0  And  numRecOwner in (case when @numUserCntID> 0 then (case when @tintType=1 then (select @numUserCntID) else (select numContactId from additionalcontactsinformation where numManagerID =@numUserCntID) end) when @numDivisionID>0 then (select numContactId from additionalcontactsinformation where numDivisionID=@numDivisionID) else (select 0) end)                         
 and numDomainID=@numDomainID and numItemCode=@numItemCode  And  month(intPEstimatedCloseDate)=M.Mon And year(intPEstimatedCloseDate)=@intYear),0) As Pipeline,    
 isnull((Select         
 Avg(isnull(tintPercentage,0))         
 From         
 OpportunityMaster Opp        
 inner join OpportunityStageDetails OSD on Opp.numOppId=OSD.numOppId    
    join OpportunityItems    
    on Opp.numOppID= OpportunityItems.numOppID        
 Where Opp.tintOppType=1 and numItemCode=@numItemCode  And   Opp.tintOppStatus=0  And  numRecOwner in (case when @numUserCntID> 0 then (case when @tintType=1 then (select @numUserCntID) else (select numContactId from additionalcontactsinformation where numManagerID =@numUserCntID) end) when @numDivisionID>0 then (select numContactId from additionalcontactsinformation where numDivisionID=@numDivisionID) else (select 0) end)  And OSD.bitstagecompleted=1        
 and Opp.numDomainID=@numDomainID And  month(intPEstimatedCloseDate)=M.Mon And year(intPEstimatedCloseDate)=@intYear),0) As Probability,     
 isnull((Select Sum(X.Units)   From (Select   (numUnitHour*(Select sum(isnull(tintPercentage,0))  from OpportunityStageDetails OSD                      
 Where OSD.numOppId=OpportunityMaster.numOppId And OSD.bitstagecompleted=1))/100 as Units  From OpportunityMaster    
    join OpportunityItems    
    on OpportunityMaster.numOppID= OpportunityItems.numOppID                        
 Where tintOppType=1 and numItemCode=@numItemCode  And month(intPEstimatedCloseDate)=M.Mon and year(intPEstimatedCloseDate)=@intYear     
 and  numRecOwner in (case when @numUserCntID> 0 then (case when @tintType=1 then (select @numUserCntID) else (select numContactId from additionalcontactsinformation where numManagerID =@numUserCntID) end) when @numDivisionID>0 then (select numContactId from additionalcontactsinformation where numDivisionID=@numDivisionID) else (select 0) end)  And tintoppStatus=0 and tintshipped=0 and numDomainID=@numDomainID)X ),0) As ProbableAmt     
 From Forecast F    
 right join (select @tintFirstMonth as Mon union select @tintSecondMonth union select @tintThirdMonth) M    
 on F.tintMonth=M.Mon and sintYear=@intYear and numDomainID=@numDomainID and tintForecastType=2 and numItemCode=@numItemCode  and numCreatedby=@numUserCntID  
end    
else if @byteMode=1    
begin    
 Select M.Mon,isnull(monForecastAmount,0) as ForeCastAmount,isnull(monQuota,0) as QuotaAmt    
 From Forecast F    
 right join (select @tintFirstMonth as Mon union select @tintSecondMonth union select @tintThirdMonth) M    
 on F.tintMonth=M.Mon and sintYear=@intYear and numDomainID=@numDomainID and tintForecastType=2 and numItemCode=@numItemCode    
end
GO
