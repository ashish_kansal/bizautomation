/****** Object:  StoredProcedure [dbo].[usp_GetDuplicateCompany_new]    Script Date: 07/26/2008 16:17:22 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getduplicatecompany_new')
DROP PROCEDURE usp_getduplicatecompany_new
GO
CREATE PROCEDURE [dbo].[usp_GetDuplicateCompany_new]

@vcCompanyName varchar(100)
--
AS
BEGIN

select CI.numCompanyID, CI.vcCompanyName, DM.vcDivisionName
from dbo.CompanyInfo CI, DivisionMaster DM
where CI.numCompanyID=DM.numCompanyID And 
CI.vcCompanyName = @vcCompanyName
--And DM.numDivisionID <> @numDivisionID and DM.bitPublicFlag=0 and DM.tintCRMType in (1,2)
Group by 
CI.numCompanyID, 
CI.vcCompanyName
, DM.vcDivisionName

END
GO
