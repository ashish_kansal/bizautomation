/****** Object:  StoredProcedure [dbo].[usp_InsertFixedAssetCOA]    Script Date: 07/26/2008 16:19:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created By Siva                                                
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_insertfixedassetcoa')
DROP PROCEDURE usp_insertfixedassetcoa
GO
CREATE PROCEDURE [dbo].[usp_InsertFixedAssetCOA]                                                                      
@numParntAcntId numeric(9)=0,                                                                      
@numAcntType numeric(9)=0,                                                                      
@vcCatgyName varchar(100),                                                                      
@vcCatgyDescription varchar(100),                                                 
@monOriginalOpeningBal DECIMAL(20,5),                                                                      
@monOpeningBal DECIMAL(20,5),                                                                      
@dtOpeningDate datetime,                                                                      
@bitActive bit,                                                                    
@numAccountId numeric(9)=0 output,                                                                    
@bitFixed bit,                                                          
@bitDepreciation numeric(9)=0,                  
@dtDepreciationCostDate as datetime,                  
@monDepreciationCost DECIMAL(20,5),                  
@numDomainId numeric(9)=0,      
@numUserCntId numeric(9)=0      
As                                                     
Begin              
 Declare @numAccountId1 as numeric(9)                                              
 Declare @numJournalId as numeric(9)                   
 Declare @numAcntIdOriginal as numeric(9)                      
 Declare @numAcntIdDepreciation as numeric(9)                         
 Declare @numJournalId1 as numeric(9)          
 Declare @numAccountIdOpeningEquity as numeric(9)                          
                                          
                                                                
 If @numAccountId=0                                                                    
  Begin             
      if @numParntAcntId=0         
         Set @numParntAcntId=(Select numAccountId From Chart_Of_Accounts Where numParntAcntTypeID is null and numDomainId = @numDomainId) --and numAccountId = 1         
                                                                        
   Insert Into Chart_Of_Accounts([numParntAcntTypeId],[numAcntTypeId],[vcAccountName],                                                                      
   [vcAccountDescription],numOriginalOpeningBal,numOpeningBal,dtOpeningDate,bitActive,bitFixed,numDomainId) Values (@numParntAcntId,                                                                      
   @numAcntType,@vcCatgyName,@vcCatgyDescription,0,0,@dtOpeningDate,@bitActive,@bitFixed,@numDomainId)                                                          
   Set @numAccountId1 = SCOPE_IDENTITY()   
   SET @numAccountId = @numAccountId1
   --Added By Chintan
	EXEC USP_ManageChartAccountOpening @numDomainId,@numAccountId1,@monOpeningBal,@dtOpeningDate

     Set @numAccountIdOpeningEquity=(Select numAccountId From Chart_Of_Accounts Where bitOpeningBalanceEquity=1 And numDomainId = @numDomainId)         
                   
   If @bitDepreciation=1                   
    Begin                  
     -- For Original Cost                  
     -- Insert into Chart_Of_Accounts Table              
     Insert Into Chart_Of_Accounts([numParntAcntTypeId],[numAcntTypeId],[vcAccountName],                                                                      
     [vcAccountDescription],numOriginalOpeningBal,numOpeningBal,dtOpeningDate,bitActive,bitFixed,numDomainId) Values (@numAccountId1,                          
     @numAcntType,@vcCatgyName+' - Original',@vcCatgyDescription + ' - Original',0,0,@dtOpeningDate,@bitActive,@bitFixed,@numDomainId)                                                          
                  
        Set @numAcntIdOriginal=SCOPE_IDENTITY()                  
        SET @numAccountId = @numAcntIdOriginal
		--Added By Chintan
		EXEC USP_ManageChartAccountOpening @numDomainId,@numAcntIdOriginal,@monOpeningBal,@dtOpeningDate
			               
     -- Insert into General_Journal_Header Table              
     Insert Into General_Journal_Header(datEntry_Date,varDescription,numAmount,numChartAcntId,numDomainId,numCreatedBy,datCreatedDate)               
     Values(@dtOpeningDate,'Opening Balance Equity',@monOpeningBal,null,@numDomainId,@numUserCntId,getutcdate())                            
     Set @numJournalId = SCOPE_IDENTITY()                 
               
     print '@numJournalId=='+convert(varchar(10),@numJournalId)               
             
     -- Insert into General_Journal_Details Table               
   Insert Into General_Journal_Details(numJournalId,numDebitAmt,numCreditAmt,numChartAcntId,numCustomerId,numDomainId)                                        
     Values(@numJournalId,@monOpeningBal,0,@numAcntIdOriginal,0,@numDomainId)                     
                  
     Insert Into General_Journal_Details(numJournalId,numDebitAmt,numCreditAmt,numChartAcntId,numCustomerId,numDomainId)                                        
     Values(@numJournalId,0,@monOpeningBal,@numAccountIdOpeningEquity,0,@numDomainId)                     
--Commented by chintan 
--     Exec USP_--Commented by chintan  @JournalId=@numJournalId,@numDomainId=@numDomainId                
                  
     --For Depreciation                  
     -- Insert into Chart_Of_Accounts Table              
     Insert Into Chart_Of_Accounts([numParntAcntTypeId],[numAcntTypeId],[vcAccountName],[vcAccountDescription],numOriginalOpeningBal,numOpeningBal,dtOpeningDate,bitActive,bitFixed,numDomainId)              
     Values (@numAccountId1,@numAcntType,@vcCatgyName + ' - Depreciation',@vcCatgyDescription+ ' - Depreciation',0,0,@dtOpeningDate,@bitActive,@bitFixed,@numDomainId)                                                          
                  
     Set @numAcntIdDepreciation=SCOPE_IDENTITY()                  
     SET @numAccountId = @numAcntIdDepreciation
	--Added By Chintan
	 EXEC USP_ManageChartAccountOpening @numDomainId,@numAcntIdDepreciation,@monOpeningBal,@dtOpeningDate
			
     -- Insert into General_Journal_Header Table              
     Insert Into General_Journal_Header(datEntry_Date,varDescription,numAmount,numChartAcntId,numDomainId,numCreatedBy,datCreatedDate)            
     Values(@dtDepreciationCostDate,'Opening Balance Equity',@monDepreciationCost,null,@numDomainId,@numUserCntId,getutcdate())                    
                                    
        Set @numJournalId1 = SCOPE_IDENTITY()                 
                             
     -- Insert into General_Journal_Details Table               
     Insert Into General_Journal_Details(numJournalId,numDebitAmt,numCreditAmt,numChartAcntId,numCustomerId,numDomainId)                                        
     Values(@numJournalId1,0,@monDepreciationCost,@numAcntIdDepreciation,0,@numDomainId)                     
               
     Insert Into General_Journal_Details(numJournalId,numDebitAmt,numCreditAmt,numChartAcntId,numCustomerId,numDomainId)                                        
     Values(@numJournalId1,@monDepreciationCost,0,@numAccountIdOpeningEquity,0,@numDomainId)                     
              
     print '@numJournalId1=='+convert(varchar(10),@numJournalId1)                   
--Commented by chintan                
--     Exec USP_UpdateChartAcntOpnBalanceForJournalEntry @JournalId=@numJournalId1,@numDomainId=@numDomainId                 
    End                 
    Else              
     Begin              
      --Insert into General_Journal_Header Table              
      Insert Into General_Journal_Header(datEntry_Date,varDescription,numAmount,numChartAcntId,numDomainId,numCreatedBy,datCreatedDate)               
      Values(@dtOpeningDate,'Opening Balance Equity',@monOpeningBal,null,@numDomainId,@numUserCntId,getutcdate())                            
      Set @numJournalId = SCOPE_IDENTITY()                 
                 
      print '@numJournalId=='+convert(varchar(10),@numJournalId)               
                  
      -- Insert into General_Journal_Details Table               
      Insert Into General_Journal_Details(numJournalId,numDebitAmt,numCreditAmt,numChartAcntId,numCustomerId,numDomainId)                                        
      Values(@numJournalId,@monOpeningBal,0,@numAccountId1,0,@numDomainId)                     
                  
      Insert Into General_Journal_Details(numJournalId,numDebitAmt,numCreditAmt,numChartAcntId,numCustomerId,numDomainId)                                        
      Values(@numJournalId,0,@monOpeningBal,@numAccountIdOpeningEquity,0,@numDomainId)                     
      --Commented by chintan 
--      Exec USP_UpdateChartAcntOpnBalanceForJournalEntry @JournalId=@numJournalId,@numDomainId=@numDomainId                 
     End              
   End                 
 ELSE If @numAccountId<>0                                                              
  Begin                                         
  --Following updates new opening balance of newly assigned accountTypeID
  -- I think this part is not required as we just have to update account code.. rest will automatically come into parent account type
  --Commented by chintan      
--      Declare @strSQl as varchar(8000)          
--      set @strSQl= dbo.fn_ParentCategory(@numAccountId,@numDomainId)                                       
--   Declare @OpeningBal as DECIMAL(20,5)                                      
--   Select @OpeningBal=numOpeningBal From chart_of_accounts Where numAccountId=@numAccountId And numDomainId = @numDomainId         
--   set @strSQl='update chart_of_accounts  set numopeningbal=isnull(numopeningbal,0) - ' + convert(varchar(30),@OpeningBal) + ' where  numAccountId in ('+@strSQl+') And numDomainId ='+convert(varchar(10),@numDomainId)    
--   print @strSQl                                      
--   exec (@strSQl)                                                             
                        
   Update Chart_Of_Accounts Set [numParntAcntTypeId]=@numParntAcntId,[numAcntTypeId]=@numAcntType,                                                              
   [vcAccountName]=@vcCatgyName,[vcAccountDescription]=@vcCatgyDescription,                                                        
   bitActive=@bitActive Where numAccountId=@numAccountId And numDomainId =@numDomainId                 
              
    --Added By Chintan
	EXEC USP_ManageChartAccountOpening @numDomainId,@numAccountId,@monOpeningBal,@dtOpeningDate
	
   --update ListDetails Set vcData=@vcCatgyName Where numListItemID=@numListItemID              
        --Commented by chintan                                     
--   set @strSQl= dbo.fn_ParentCategory(@numAccountId,@numDomainId)                                                
--   set @strSQl='update chart_of_accounts  set numopeningbal=isnull(numopeningbal,0) + ' + convert(varchar(30),@OpeningBal) + ' where  numAccountId in ('+@strSQl+') And numDomainId ='+convert(varchar(10),@numDomainId)                                       
--  
--         
--   print @strSQl                                                
--   exec (@strSQl)                                                             
                                                                                
 End          
End
GO
