/****** Object:  StoredProcedure [dbo].[USP_DeleteCompetitor]    Script Date: 07/26/2008 16:15:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_deletecompetitor')
DROP PROCEDURE usp_deletecompetitor
GO
CREATE PROCEDURE [dbo].[USP_DeleteCompetitor]
@numCompetitorID as numeric(9),  
@numDomainID as numeric(9) ,
@numItemCode as numeric(9) 
as  
delete from Competitor   
where numCompetitorID=@numCompetitorID   
and numDomainID=@numDomainID  and numItemCode=@numItemCode
GO
