/****** Object:  StoredProcedure [dbo].[USP_GetPaymentGateways]    Script Date: 07/26/2008 16:18:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getpaymentgateways')
DROP PROCEDURE usp_getpaymentgateways
GO
CREATE PROCEDURE [dbo].[USP_GetPaymentGateways]

as

select intPaymentGateWay, vcGateWayName
from PaymentGateway
GO
