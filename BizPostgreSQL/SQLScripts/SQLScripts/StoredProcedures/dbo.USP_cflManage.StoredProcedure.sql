/****** Object:  StoredProcedure [dbo].[USP_cflManage]    Script Date: 07/26/2008 16:15:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created By Anoop Jayaraj          
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_cflmanage')
DROP PROCEDURE usp_cflmanage
GO
CREATE PROCEDURE [dbo].[USP_cflManage]          
@locid as tinyint=0,          
@fldtype as varchar (30)='',          
@fldlbl as varchar(50)='',          
@FieldOdr as tinyint=0,          
@userId as numeric(9)=0,          
@TabId as numeric(9)=0,          
@fldId as numeric(9) OUTPUT,      
@numDomainID as numeric(9)=0,  
@vcURL as varchar(1000)='',
@vcToolTip AS VARCHAR(1000)='',         
@ListId as numeric(9) OUTPUT,
@bitAutocomplete AS BIT
AS
BEGIN          
     
	 DECLARE @bitElasticSearch BIT

	SET @bitElasticSearch = ISNULL((SELECT TOP 1 bitElasticSearch FROM 
	eCommerceDTL WHERE numDomainId = @numDomainID AND bitElasticSearch = 1),0)


if @fldId =0          
begin          
  --declare @ListId as numeric(9)          
            
            
  if @fldtype='SelectBox' OR @fldtype='CheckBoxList'          
  begin          
  INSERT INTO ListMaster           
   (          
   vcListName,           
   numCreatedBy,           
   bintCreatedDate,           
   bitDeleted,           
   bitFixed ,    
   numModifiedBy,    
   bintModifiedDate,    
   numDomainID,    
   bitFlag,numModuleId         
   )           
   values          
   (          
   @fldlbl,          
   1,          
   getutcdate(),          
   0,          
   0,    
   1,    
   getutcdate(),     
   @numDomainID,    
   0,8        
   )          
            
  SET @ListId = SCOPE_IDENTITY()
         
  end          
            
  Insert into cfw_fld_master          
   (          
   Fld_type,          
   Fld_label,          
   Fld_tab_ord,          
   Grp_id,          
   subgrp,          
   numlistid,      
   numDomainID,  
	   vcURL,vcToolTip,bitAutocomplete         
   )           
   values          
   (          
   @fldtype,          
   @fldlbl,          
   @FieldOdr,          
   @locid,          
   @TabId,          
   @ListId,      
   @numDomainID,  
   @vcURL,@vcToolTip, @bitAutoComplete          
   )       
   
  SET @fldId = SCOPE_IDENTITY() --(SELECT MAX(fld_id) FROM dbo.CFW_Fld_Master WHERE numDomainID = @numDomainID)
   
   IF(@bitElasticSearch = 1)
   BEGIN
		IF (EXISTS (SELECT 1 FROM CFW_Fld_Master where fld_id=@fldId AND Grp_id IN (5,9)))
		BEGIN
			INSERT INTO ElasticSearchBizCart (numDomainID,vcValue,vcAction,vcModule)
			SELECT @numDomainID,
			CASE 
				WHEN Grp_id=5 THEN REPLACE(Fld_label,' ','') + '_C'
				WHEN Grp_id=9 THEN REPLACE(Fld_label,' ','') + '_' + CAST(Fld_Id AS VARCHAR)
			END
			,'Insert'
			,'CustomField'
			FROM CFW_Fld_Master where fld_id=@fldId AND Grp_id IN (5,9)
		END
	END

end          
          
else           
begin          
 if @fldtype='SelectBox' OR @fldtype='CheckBoxList'          
  begin         
    
    declare @ddlName as varchar(100)        
  select @ddlName=Fld_label from cfw_fld_master where Fld_id=@fldId  and numDomainID =@numDomainID     
         
  update  ListMaster set  vcListName=@fldlbl where vcListName=@ddlName  and    numDomainID =@numDomainID     
        
  end  
  
   IF(@bitElasticSearch = 1)
   BEGIN
		IF (EXISTS (SELECT 1 FROM CFW_Fld_Master where fld_id=@fldId AND Grp_id IN (5,9)))
		BEGIN
			IF(@locid NOT IN (5,9))
			BEGIN
				INSERT INTO ElasticSearchBizCart (numDomainID,vcValue,vcAction,vcModule)
				SELECT @numDomainID,
				CASE 
					WHEN Grp_id=5 THEN REPLACE(Fld_label,' ','') + '_C'
					WHEN Grp_id=9 THEN REPLACE(Fld_label,' ','') + '_' + CAST(Fld_Id AS VARCHAR)
				END
				,'Delete'
				,'CustomField'
				FROM CFW_Fld_Master where fld_id=@fldId AND Grp_id IN (5,9)
			END
			ELSE
			BEGIN
				IF(NOT EXISTS (SELECT 1 FROM CFW_Fld_Master where fld_id=@fldId AND Grp_id = @locid AND Fld_label = @fldlbl))
				BEGIN
					DECLARE @UpdateCustomField AS VARCHAR(100)  
					SELECT @UpdateCustomField = CASE 
						WHEN @locid=5 THEN REPLACE(@fldlbl,' ','') + '_C' 
						WHEN @locid=9 THEN REPLACE(@fldlbl,' ','') + '_' + CAST(@fldId AS VARCHAR)
					END

					INSERT INTO ElasticSearchBizCart (numDomainID,vcValue,vcAction,vcModule)
					SELECT @numDomainID,
					CASE 
						WHEN Grp_id=5 THEN REPLACE(Fld_label,' ','') + '_C'  
						WHEN Grp_id=9 THEN REPLACE(Fld_label,' ','') + '_' + CAST(Fld_Id AS VARCHAR) 
					END + '||' + @UpdateCustomField
					,'Update'
					,'CustomField'
					FROM CFW_Fld_Master where fld_id=@fldId AND Grp_id IN (5,9)
				END
			END
		END	
		ELSE IF(@locid IN (5,9))
		BEGIN
			INSERT INTO ElasticSearchBizCart (numDomainID,vcValue,vcAction,vcModule)
			SELECT @numDomainID,
			CASE 
				WHEN @locid=5 THEN REPLACE(@fldlbl,' ','') + '_C'
				WHEN @locid=9 THEN REPLACE(@fldlbl,' ','') + '_' + CAST(@fldId AS VARCHAR)
			END
			,'Insert'
			,'CustomField'
		END
    END

 update cfw_fld_master set           
          
  Fld_label=@fldlbl,          
  Grp_id=@locid,          
  subgrp=@TabId,  
  vcURL= @vcURL,vcToolTip=@vcToolTip, bitAutoComplete = @bitAutoComplete        
 where Fld_id=@fldId and   numDomainID =@numDomainID         
            
    
END
END
GO
