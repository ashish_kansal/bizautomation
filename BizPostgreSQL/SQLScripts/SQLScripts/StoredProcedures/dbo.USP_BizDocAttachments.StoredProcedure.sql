/****** Object:  StoredProcedure [dbo].[USP_BizDocAttachments]    Script Date: 07/26/2008 16:14:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj  
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_bizdocattachments')
DROP PROCEDURE usp_bizdocattachments
GO
CREATE PROCEDURE [dbo].[USP_BizDocAttachments]  
 @numBizDocID as numeric(9)=0,  
 @numBizDocAtID as numeric(9)=0,  
 @numDomainID as numeric(9)=0,  
 @vcURL as varchar(1000)='',  
 @vcDocName as varchar(100)='',
 @numBizDocTempID as numeric(9)=0	  
as  
 declare @numOrder as numeric(9)  
 if @numBizDocAtID=0   
 begin  
  
 select @numOrder=isnull(max(numOrder),0)+1 from BizDocAttachments where numBizDocID=@numBizDocID and numDomainID=@numDomainID and numBizDocTempID=@numBizDocTempID 
 insert into BizDocAttachments (numBizDocID,numDomainID,vcURL,numOrder,vcDocName,numBizDocTempID)  
 values (@numBizDocID,@numDomainID,@vcURL,@numOrder,@vcDocName,@numBizDocTempID)  
 if not exists(select * from BizDocAttachments where numBizDocID=@numBizDocID and numDomainID=@numDomainID and vcDocName='BizDoc' and numBizDocTempID=@numBizDocTempID)  
 begin  
  insert into BizDocAttachments (numBizDocID,numDomainID,vcURL,numOrder,vcDocName,numBizDocTempID)  
  values (@numBizDocID,@numDomainID,'BizDoc',@numOrder+1,'BizDoc',@numBizDocTempID)  
 end  
 end  
 else if @numBizDocAtID>0  
 begin  
 update BizDocAttachments set vcURL=@vcURL,vcDocName=@vcDocName,numBizDocTempID=@numBizDocTempID  
        where numAttachmntID=@numBizDocAtID  
 if not exists(select * from BizDocAttachments where numBizDocID=@numBizDocID and numDomainID=@numDomainID and vcDocName='BizDoc' and numBizDocTempID=@numBizDocTempID)  
 begin  
 select @numOrder=isnull(max(numOrder),0) from BizDocAttachments where numBizDocID=@numBizDocID and numDomainID=@numDomainID  and numBizDocTempID=@numBizDocTempID
  insert into BizDocAttachments (numBizDocID,numDomainID,vcURL,numOrder,vcDocName,numBizDocTempID)  
  values (@numBizDocID,@numDomainID,'BizDoc',@numOrder+1,'BizDoc',@numBizDocTempID)  
 end 
 end
GO
