/****** Object:  StoredProcedure [dbo].[USP_ProsProject]    Script Date: 07/26/2008 16:20:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
---created by anoop jayaraj  
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_prosproject')
DROP PROCEDURE usp_prosproject
GO
CREATE PROCEDURE [dbo].[USP_ProsProject]    
@numDivisionId as numeric(9)=null,    
@numProjectStatus as numeric(9),  
@numDomainID as numeric(9)=0  ,
@ClientTimeZoneOffset as int  
as    
     
   SELECT distinct(pro.numProId),     
    pro.vcProjectName AS Name,     
    opp.vcPOppName as DealID, addcon.numContactId,    
    addCon.vcFirstName + ' ' + addCon.vcLastName AS CustJobContact,     
    dbo.GetProLstMileStoneCompltd(pro.numProId) as LstCmtMilestone,    
    dbo.GetProLstStageCompltd(pro.numProId,@ClientTimeZoneOffset) as lstCompStage,
	(SELECT SUBSTRING(
(SELECT ',' + A.vcFirstName + ' ' + A.vcLastName + '('+ dbo.fn_GetListItemName(numProjectRole) + ')'
FROM ProjectTeamRights TR
   join AdditionalContactsInformation A on TR.numContactId=A.numContactId 
   join DivisionMaster DM on DM.numDivisionID=A.numDivisionID where
	TR.numProId=pro.numProId and TR.tintUserType=2 and DM.numDivisionId=@numDivisionId and DM.numDomainID=@numDomainID
FOR XML PATH('')),2,200000)) AS vcResource
   FROM ProjectsMaster pro    
   left join  OpportunityMaster opp    
   on opp.numOppId=pro.numOppId    
   join additionalContactsinformation addCon    
   on addCon.numContactId=pro.numCustPrjMgr     
   where (pro.numDivisionId=@numDivisionId  
	or pro.numProId  in (select distinct numProId from ProjectTeamRights TR
   join AdditionalContactsInformation A on TR.numContactId=A.numContactId 
   join DivisionMaster DM on DM.numDivisionID=A.numDivisionID where TR.tintUserType=2 and DM.numDivisionId=@numDivisionId and DM.numDomainID=@numDomainID))
   and isnull(pro.numProjectStatus,0)=@numProjectStatus  and pro.numDomainID=@numDomainID



GO
