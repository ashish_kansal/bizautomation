/****** Object:  StoredProcedure [dbo].[usp_getFormConfigFields]    Script Date: 07/26/2008 16:17:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created By: Debasish Tapan Nag                                  
--Purpose: Returns the Configured Form Fields for a Particular Form    
--Created Date: 03/02/2006                                      
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getformconfigfields')
DROP PROCEDURE usp_getformconfigfields
GO
CREATE PROCEDURE [dbo].[usp_getFormConfigFields]    
 @numDomainId numeric (9),                                     
 @numFormId int,            
 @numAuthGroupId numeric (9),
 @numBizDocTemplateID numeric(9)=0,
@numSubFormId NUMERIC(9)
as                         
BEGIN                  
 SELECT DISTINCT numFormId, Convert(NVarchar(5),numFieldId) + vcFieldType AS numFormFieldId, vcFieldName as vcFormFieldName, vcFieldName As vcNewFormFieldName,    
 vcFieldType,                       
 vcAssociatedControlType, numListID,                             
 vcDbColumnName, vcListItemType,tintColumn as intColumnNum,tintRow as intRowNum,bitRequired as boolRequired,                       
 boolAOIField, numAuthGroupId, vcFieldDataType ,isnull(vcLookBackTableName,'') vcLookBackTableName,
 ISNULL(numSubFormId,0) AS numSubFormId
 FROM View_DynamicColumns   
 WHERE numFormId=@numFormId AND numDomainId = @numDomainId    
 AND numAuthGroupId = @numAuthGroupId   
 and (numRelCntType=@numBizDocTemplateID or @numBizDocTemplateID=0)   
 AND ISNULL(numSubFormID,0)=ISNULL(@numSubFormId,0)
 AND ISNULL(vcLookBackTableName,'') = (CASE @numAuthGroupId WHEN 40911 THEN 'CSGrid' ELSE REPLACE(ISNULL(vcLookBackTableName,''),'CSGrid','') END)
    
END
GO
