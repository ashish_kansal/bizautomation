/****** Object:  StoredProcedure [dbo].[usp_GetPageDetails]    Script Date: 07/26/2008 16:18:08 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getpagedetails')
DROP PROCEDURE usp_getpagedetails
GO
CREATE PROCEDURE [dbo].[usp_GetPageDetails]
	@numPageID NUMERIC(9) = 0,
	@numModuleID  NUMERIC(9) = 0   
--
AS
	SELECT     TOP 1 numModuleID, numPageID, vcFileName, vcPageDesc, bitIsViewApplicable, bitIsAddApplicable, bitIsUpdateApplicable, bitIsDeleteApplicable, 
                      bitIsExportApplicable
	FROM         dbo.PageMaster
	WHERE     (numModuleID = @numModuleID) AND (numPageID = @numPageID)
GO
