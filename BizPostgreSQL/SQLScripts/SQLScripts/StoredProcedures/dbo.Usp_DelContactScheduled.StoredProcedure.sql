/****** Object:  StoredProcedure [dbo].[Usp_DelContactScheduled]    Script Date: 07/26/2008 16:15:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_delcontactscheduled')
DROP PROCEDURE usp_delcontactscheduled
GO
CREATE PROCEDURE [dbo].[Usp_DelContactScheduled]
@numContactID as numeric(9),
@numScheduleId as numeric(9)
as 

delete CustRptSchContacts where numScheduleId = @numScheduleId and numcontactId = @numContactID
GO
