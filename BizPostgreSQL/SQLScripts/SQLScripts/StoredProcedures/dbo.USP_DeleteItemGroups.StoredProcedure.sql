/****** Object:  StoredProcedure [dbo].[USP_DeleteItemGroups]    Script Date: 07/26/2008 16:15:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_deleteitemgroups')
DROP PROCEDURE usp_deleteitemgroups
GO
CREATE PROCEDURE [dbo].[USP_DeleteItemGroups]
@numItemGroupID as numeric(9)=0
as

delete from ItemGroupsDTL where numItemGroupID=@numItemGroupID
delete from ItemGroups where numItemGroupID=@numItemGroupID
GO
