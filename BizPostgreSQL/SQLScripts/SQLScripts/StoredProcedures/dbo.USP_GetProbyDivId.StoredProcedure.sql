/****** Object:  StoredProcedure [dbo].[USP_GetProbyDivId]    Script Date: 07/26/2008 16:18:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getprobydivid')
DROP PROCEDURE usp_getprobydivid
GO
CREATE PROCEDURE [dbo].[USP_GetProbyDivId]    
@numdivisionid numeric(9)  ,  
@byteMode TINYINT,  
@numDomainId numeric(9)  
    
as   
  
if @bytemode = 1  
begin  
select numProid,vcProjectName from projectsmaster where  numDivisionId=@numDivisionId  and tintProStatus =0  and numdomainid=@numdomainid  
  
end  
  
if @bytemode = 0  
begin  
select numProid,vcProjectName from projectsmaster where numDivisionId = @numDivisionId  and tintProStatus <> 1 and numdomainid=@numdomainid  
END

if @bytemode = 3  
begin  
select numProid,vcProjectName from projectsmaster where  tintProStatus <> 1 and numdomainid=@numdomainid  
END


IF @bytemode = 2
BEGIN  

SELECT numProid,vcProjectName 
FROM projectsmaster 
WHERE numDivisionId = @numDivisionId  
AND tintProStatus <> 1 
AND numdomainid = @numdomainid  
AND ISNULL(numAccountID,0) <> 0 

END
GO
