/****** Object:  StoredProcedure [dbo].[USP_SaveMarketBudgetHeader]    Script Date: 07/26/2008 16:21:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created By Siva                    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_savemarketbudgetheader')
DROP PROCEDURE usp_savemarketbudgetheader
GO
CREATE PROCEDURE [dbo].[USP_SaveMarketBudgetHeader]                    
@intFiscalYear as numeric(9)=0,                    
@bitMarketCampaign  as bit,                    
@numDomainId as numeric(9)=0,                    
@numUserCntId as numeric(9)=0                   
As                    
Begin           
Declare @numMarketBudgetId as numeric(9)         
Set @numMarketBudgetId=(Select isnull(numMarketBudgetId,0) From MarketBudgetMaster Where  numDomainId=@numDomainId)         
                 
If @numMarketBudgetId is null        
 Begin        
  Insert into MarketBudgetMaster(bitMarketCampaign,numDomainId,numCreatedBy,dtCreationDate)                    
  Values(@bitMarketCampaign,@numDomainId,@numUserCntId,getutcdate())                    
  Set @numMarketBudgetId = SCOPE_IDENTITY()                                                  
  Select @numMarketBudgetId          
 End                    
Else        
 Begin        
    Update MarketBudgetMaster Set bitMarketCampaign=@bitMarketCampaign,numModifiedBy=@numUserCntId,dtModifiedDate=getutcdate()        
 Where numMarketBudgetId=@numMarketBudgetId And numDomainId=@numDomainId       
 Select @numMarketBudgetId        
 End              
End
GO
