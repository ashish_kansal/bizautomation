/****** Object:  StoredProcedure [dbo].[GetChildOperationBudgetDetails]    Script Date: 07/26/2008 16:14:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created By Siva            
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='getchildoperationbudgetdetails')
DROP PROCEDURE getchildoperationbudgetdetails
GO
CREATE PROCEDURE [dbo].[GetChildOperationBudgetDetails]          
(@numDomainId as numeric(9)=0,          
@numBudgetId as numeric(9)=0,      
@sintByte as smallint=0,  
@intType as int=0,  
@Id as numeric(9)=0    
)          
As          
Begin          
 Create table #temp(numBudgetId numeric(9),          
 vcmonthName varchar(100),  
monAmount DECIMAL(20,5))          
--         
-- Declare @monAmount as DECIMAL(20,5)       
-- Declare @month as tinyint     
-- Declare @vcMonDescription as varchar(200)     
-- Declare @Date as datetime  
-- Declare @dtStartDate as datetime  
-- Set @month=month(getutcdate())        
--  Set @Date = dateadd(year,@intType,getutcdate())      
--if @sintByte=0      
--Begin      
--    While @month<=24                              
--  Begin              
--  Set @dtStartDate='01/01/'+convert(varchar(4),year(getutcdate()))             
--  Set @dtStartDate=dateadd(month,@month-1,@dtStartDate)             
--  Insert into #temp(numBudgetId,vcmonthName,monAmount) Values  
--        (@numBudgetId, convert(varchar(3),DateName(month,@dtStartDate)),dbo.fn_GetBudgetMonthDet(@numBudgetId,month(@dtStartDate),year(@dtStartDate),@Id))  
--  Set @month=@month+1
-- End  
--End  
   
Select * From #temp          
Drop table #temp          
End
GO
