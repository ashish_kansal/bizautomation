/****** Object:  StoredProcedure [dbo].[usp_deleteOppurtunitySubStage]    Script Date: 07/26/2008 16:15:38 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_deleteoppurtunitysubstage')
DROP PROCEDURE usp_deleteoppurtunitysubstage
GO
CREATE PROCEDURE [dbo].[usp_deleteOppurtunitySubStage]
	@numOppId numeric
	
--
AS
	
       Delete from OpportunitySubStageDetails where numoppid=@numOppId
GO
