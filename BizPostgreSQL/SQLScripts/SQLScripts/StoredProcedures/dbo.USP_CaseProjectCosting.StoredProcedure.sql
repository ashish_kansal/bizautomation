/****** Object:  StoredProcedure [dbo].[USP_CaseProjectCosting]    Script Date: 07/26/2008 16:15:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_caseprojectcosting')
DROP PROCEDURE usp_caseprojectcosting
GO
CREATE PROCEDURE [dbo].[USP_CaseProjectCosting]    
@numDivId as numeric,    
@numCaseId as numeric,    
@numProId as numeric,    
@numDomainId as numeric    
as    
--(select     
--vcCaseNumber as Name,    
--'Case Time' as Type,    
--convert(varchar(20),convert(decimal(10,2),datediff(minute,CT.dtStartTime,CT.dtEndTime)* 0.016666666666666666666666666666667))+'  ,'+ vcUserName as TimeEntered ,    
--case when CT.bitBill =1 then 'Billable' when CT.bitBill =0 then  'Non-Billable' end as TimeType,    
--case when CT.bitBill =1 then numRate  when CT.bitBill =0 then  0 end as RateHr,    
--convert(varchar(20),convert(decimal(10,2),datediff(minute,CT.dtStartTime,CT.dtEndTime)* 0.016666666666666666666666666666667)) as hrs,    
--case when CT.bitBill =1 then    
--convert(decimal(10,2),datediff(minute,CT.dtStartTime,CT.dtEndTime)*numRate * 0.016666666666666666666666666666667 )    
--when CT.bitBill =0 then  0 end     
--as AmountBilled,    
--case when ct.bitBill=1 then    
--    
--case when bithourlyrate =1 then    
--convert(decimal(10,2),datediff(minute,cT.dtStartTime,cT.dtEndTime)* 0.016666666666666666666666666666667* monhourlyrate )    
--else    
--0    
--end     
--when bitbill=0 then 0 end    
--as labourrate    
--    
--from CaseTime CT     
--left join Cases C on CT.numCaseId = C.numCaseId    
--join usermaster UM on CT.numcreatedby = numuserid    
--where     
-- C.numDivisionId = @numDivId and C.numDomainId = @numDomainId    
--and c.numcaseid = case when  @numCaseid > 0 then  @numCaseId else 0 end    
--    
--UNION    
--    
--select     
--vcCaseNumber as Name,    
--'Case Expense' as Type,    
--' -  ,' + vcUserName as TimeEntered ,    
--case when CE.bitBill =1 then 'Billable' when CE.bitBill =0 then  'Non-Billable' end as TimeType,    
--case when CE.bitBill =1 then monamount  when CE.bitBill =0 then  0 end as RateHr,    
--'-' as hrs ,    
--case when CE.bitBill =1 then monamount  when CE.bitBill =0 then  0 end as AmountBilled,    
--    
--case when bithourlyrate =1 then    
--monamount * monhourlyrate     
--else    
--0    
--end     
--as labourrate    
--    
--from CaseExpense CE    
--left join Cases C on CE.numCaseId = C.numCaseId    
--join usermaster UM on CE.numcreatedby = numuserid    
--where     
-- C.numDivisionId = @numDivId and C.numDomainId = @numDomainId    
--and c.numcaseid = case when  @numCaseid > 0 then  @numCaseId else 0 end    
--UNION    
--    
--select     
--vcProjectName as Name,    
--'Project Time' as Type,    
--convert(varchar(20),convert(decimal(10,2),datediff(minute,PT.dtStartTime,PT.dtEndTime)* 0.016666666666666666666666666666667)) +'  ,'+ vcUserName as TimeEntered ,    
--case when PT.bitBill =1 then 'Billable' when PT.bitBill =0 then  'Non-Billable' end as TimeType,    
--case when PT.bitBill =1 then numRate  when PT.bitBill =0 then  0 end as RateHr,    
--convert(varchar(20),convert(decimal(10,2),datediff(minute,PT.dtStartTime,PT.dtEndTime)* 0.016666666666666666666666666666667)) as hrs,    
--case when PT.bitBill =1 then    
--convert(decimal(10,2),datediff(minute,PT.dtStartTime,PT.dtEndTime)*numRate * 0.016666666666666666666666666666667 )    
--when PT.bitBill =0 then  0 end     
--as AmountBilled,    
--case when Pt.bitBill=1 then    
--    
--case when bithourlyrate =1 then    
--convert(decimal(10,2),datediff(minute,PT.dtStartTime,PT.dtEndTime)* 0.016666666666666666666666666666667* monhourlyrate )    
--else    
--0    
--end     
--when bitbill=0 then 0 end    
--as labourrate    
--from ProjectsTime PT     
--left join projectsmaster P on PT.numProId = P.numProId    
--join usermaster UM on PT.numPcreatedby = numuserid    
--where     
-- P.numDivisionId = @numDivId and P.numDomainId = @numDomainId    
--and p.numProId = case when  @numProId > 0 then  @numProId else 0 end    
--UNION    
--    
--select     
--vcProjectName as Name,    
--'Project Expense' as Type,    
--' - ,'+ vcUserName  as TimeEntered ,    
--case when PE.bitBill =1 then 'Billable' when PE.bitBill =0 then  'Non-Billable' end as TimeType,    
--case when PE.bitBill =1 then monamount  when PE.bitBill =0 then  0 end as RateHr,    
--'-' as hrs,    
--case when PE.bitBill =1 then monamount  when PE.bitBill =0 then  0 end as AmountBilled,    
--case when bithourlyrate =1 then    
--monamount * monhourlyrate     
--else    
--0    
--end     
--as labourrate    
--from ProjectsExpense PE     
--left join projectsmaster P on PE.numProId = P.numProId    
--join usermaster UM on PE.numPcreatedby = numuserid    
--where     
-- P.numDivisionId = @numDivId and P.numDomainId = @numDomainId    
--and p.numProId = case when  @numProId > 0 then  @numProId else 0 end    
--    
--) order by 1

select 
 case when TE.numCaseId <> 0 then vcCaseNumber when TE.numProId <> 0 then vcProjectName end as [name],

case when numcategory =1 then 'Time' when numCategory = 2 then 'Expense' end as Type,
case when TE.numType =1 then 'Billable' when TE.numType =2 then  'Non-Billable' end as TimeType,
case when TE.numType =1 then monamount  when TE.numType =2 then  0 end as RateHr,

case when numCategory = 1 then
convert(varchar(20),convert(decimal(10,2),datediff(minute,TE.dtFromDate,TE.dtToDate)* 0.016666666666666666666666666666667))
when numCategory =2 then '-' end 
as hrs,

case when numCategory = 1 then
	case when TE.numType =1 then    
	convert(decimal(10,2),datediff(minute,TE.dtFromDate,TE.dtToDate)*monamount * 0.016666666666666666666666666666667 )    
	when TE.numType =2 then  0 end 
when numCategory = 2 then    
	case when TE.numType =1 then monamount  when TE.numType =2 then  0 end 
end 

as AmountBilled,

case when numCategory = 1 then
	case when TE.numType=1 then    
	    
	case when bithourlyrate =1 then    
	convert(decimal(10,2),datediff(minute,TE.dtFromDate,TE.dtToDate)* 0.016666666666666666666666666666667* monhourlyrate )    
	else    
	0    
	end     
	when TE.numType=2 then 0 end    
when numCategory = 2 then 

	case when bithourlyrate =1 then    
	monamount * monhourlyrate     
	else    
	0    
	end
end     
as labourrate,




case when numCategory =1 then
	convert(varchar(20),convert(decimal(10,2),datediff(minute,TE.dtFromDate,TE.dtToDate)* 0.016666666666666666666666666666667))+'  ,'+ vcUserName    
	when numCategory = 2 then ' - ,'+ vcUserName  end as  TimeEntered


from timeandexpense TE
left join Cases C on TE.numCaseId = C.numCaseId   
left join projectsmaster P on TE.numProId = P.numProId 
join usermaster UM on TE.numTCreatedBy = numuserid
where  
(TE.numCaseId =case when  @numCaseid > 0 then  @numCaseId else 0 end 
		 or TE.numProid = case when  @numProId > 0 then  @numProId else 0 end ) and
((c.numDivisionId = @numDivId and c.numDomainId = @numDomainId )
or (P.numDivisionId = @numDivId and P.numDomainId = @numDomainId ))
GO
