/****** Object:  StoredProcedure [dbo].[USP_AddCompetitor]    Script Date: 07/26/2008 16:14:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_addcompetitor')
DROP PROCEDURE usp_addcompetitor
GO
CREATE PROCEDURE [dbo].[USP_AddCompetitor]      
@numDivisionId as numeric(9),      
@numDomainID as numeric(9),    
@numItemCode as numeric(9)     
as      
      
if not exists(select * from  Competitor where numCompetitorID=@numDivisionId and numItemCode=@numItemCode and numDomainID=@numDomainID )      
begin      
insert into Competitor (numCompetitorID,vcPrice,numDomainID,dtDateEntered,numItemCode)      
values(@numDivisionId,'',@numDomainID,getutcdate(),@numItemCode)      
end
GO
