/****** Object:  StoredProcedure [dbo].[USP_InsertJournalEntryHeaderForAuthoritativeBiz]    Script Date: 07/26/2008 16:19:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created by Siva                          
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_insertjournalentryheaderforauthoritativebiz')
DROP PROCEDURE usp_insertjournalentryheaderforauthoritativebiz
GO
CREATE PROCEDURE [dbo].[USP_InsertJournalEntryHeaderForAuthoritativeBiz]
@numAmount as DECIMAL(20,5),                                    
@numJournalId as numeric(9)=0 Output,                                
@numDomainId as numeric(9)=0,                        
@numRecurringId as numeric(9)=0,                    
@numUserCntID as numeric(9)=0,                
@numOppBizDocsId as numeric(9)=0,              
@numOppId as numeric(9)=0,
@numCategoryHDRID AS NUMERIC(9)=0 -- Added to keep link between Project Income through TimeAndExpense Table
As                                      
Begin   
if @numRecurringId=0 set @numRecurringId=null                                    
IF @numCategoryHDRID=0 SET @numCategoryHDRID = null
 If @numJournalId=0                              
  Begin                              
   Insert into General_Journal_Header(datEntry_Date,numAmount,numDomainId,numRecurringId,numCreatedBy,datCreatedDate,numOppBizDocsId,numOppId,[numCategoryHDRID])              
   Values(getutcdate(),@numAmount,@numDomainId,@numRecurringId,@numUserCntID,getutcdate(),@numOppBizDocsId,@numOppId,@numCategoryHDRID)
   Set @numJournalId = SCOPE_IDENTITY()                                    
   Select @numJournalId                         
  End                  
End
GO
