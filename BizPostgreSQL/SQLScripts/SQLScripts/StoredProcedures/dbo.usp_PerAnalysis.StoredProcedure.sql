/****** Object:  StoredProcedure [dbo].[usp_PerAnalysis]    Script Date: 07/26/2008 16:20:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--sp_helptext usp_PerAnalysis          
          
          
    --Modified By Anoop Jayaraj                      
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_peranalysis')
DROP PROCEDURE usp_peranalysis
GO
CREATE PROCEDURE [dbo].[usp_PerAnalysis]                         
 @numDomainId NUMERIC,                          
 @numUserCntId NUMERIC,                          
 @dtStartDate datetime,                          
 @dtEndDate datetime,                        
 @Type as tinyint,                       
 @tintType as tinyint                        
--- Type = 1 Team Performance =2 Individual Performance                        
AS                          
BEGIN                          
 DECLARE @numOppOpen NUMERIC                          
 DECLARE @numOpplost NUMERIC                        
 DECLARE @numOpplostAmount NUMERIC                        
 DECLARE @numOppOpenAmount NUMERIC                          
 DECLARE @numOppClose NUMERIC                          
 DECLARE @numOppCloseAmount NUMERIC                          
 DECLARE @numLeadProm NUMERIC                          
 DECLARE @numTotProspect NUMERIC                          
 DECLARE @numNoProsProm NUMERIC                          
 DECLARE @numTotNoAccount NUMERIC                          
 DECLARE @numCommSch NUMERIC                          
 DECLARE @numComComp NUMERIC                          
 DECLARE @numCommPast NUMERIC                          
 DECLARE @numTaskSch NUMERIC                          
 DECLARE @numTaskComp NUMERIC                          
 DECLARE @numTaskPast NUMERIC                          
 DECLARE @numCaseOpen NUMERIC                          
 DECLARE @numCaseComp NUMERIC                          
 DECLARE @numCasePast NUMERIC                                     
 DECLARE @numTerritoryID numeric                              
                          
                         
                         
if @Type=1                        
begin                         
	SELECT 
		@numOppOpen=count(*), @numOppOpenAmount=sum(monPAmount) 
	FROM 
		OPPORTUNITYMASTER OM 
	INNER JOIN DivisionMaster DM ON OM.numDivisionId=DM.numDivisionID 
	INNER JOIN AdditionalContactsInformation ADC ON ADC.numContactId = OM.numRecOwner
	WHERE                           
		tintOppType=1                   
		AND OM.bintCreatedDate  between @dtStartDate and @dtEndDate and ADC.numTeam in           
		(SELECT numTeam FROM ForReportsByTeam F WHERE F.numUserCntID=@numUserCntId and F.numDomainID=@numDomainId and F.tintType=@tintType)                      
                        
                        
--Opportunity Lost deals                         
	SELECT 
		@numOpplost=COUNT(*)
		,@numOpplostAmount=SUM(monPAmount) 
	FROM 
		OPPORTUNITYMASTER OM
	INNER JOIN AdditionalContactsInformation ADC ON ADC.numContactId = OM.numRecOwner
   WHERE OM.bintCreatedDate  between @dtStartDate and @dtEndDate and tintOppType=1 AND ADC.numTeam in           
   (select numTeam from ForReportsByTeam F where F.numUserCntID=@numUserCntId and F.numDomainID=@numDomainId and F.tintType=@tintType) and OM.tintOppStatus=2                        
                        
                              
  --Opportunity won deals                          
  SELECT @numOppClose=count(*), @numOppCloseAmount=sum(monPAmount) FROM OPPORTUNITYMASTER OM INNER JOIN AdditionalContactsInformation ADC ON ADC.numContactId = OM.numRecOwner
   WHERE OM.bintCreatedDate  between @dtStartDate and @dtEndDate and tintOppType=1 AND ADC.numTeam in           
  (select numTeam from ForReportsByTeam F where F.numUserCntID=@numUserCntId and F.numDomainID=@numDomainId and F.tintType=@tintType) and OM.tintOppStatus=1                        
                        
                        
  --Leads Promoted                          
  SELECT @numLeadProm=Count(*) FROM divisionMaster DM                
  join AdditionalContactsInformation ADC on           
  ADC.numDivisionID=DM.numDivisionID          
  join AdditionalContactsInformation ADC1           
  on ADC1.numContactId=DM.bintLeadPromBy          
  WHERE ISNULL(ADC.bitPrimaryContact,0)=1  and DM.tintCRMType=1 and bintLeadProm between @dtStartDate and @dtEndDate                         
  AND ADC1.numTeam in (select numTeam from ForReportsByTeam F where F.numUserCntID=@numUserCntId and F.numDomainID=@numDomainId and F.tintType=@tintType)                        
          
                       
  --Total Prospects Added       
SELECT @numTotProspect=Count(*) FROM divisionMaster D              
join AdditionalContactsInformation A                  
on A.numDivisionID =d.numDivisionID          
WHERE ISNULL(A.bitPrimaryContact,0)=1 and D.tintCRMType=1                        
and ((D.bintCreatedDate between @dtStartDate and @dtEndDate and D.bintLeadPromBy is null and D.numRecOwner in           
(select numContactID from AdditionalContactsInformation             
  where numTeam in (select numTeam from ForReportsByTeam F where F.numUserCntID=@numUserCntId and F.numDomainID=@numDomainId and F.tintType=@tintType)))                          
  or (bintLeadProm between @dtStartDate and @dtEndDate and D.bintLeadPromBy in (select numContactID from AdditionalContactsInformation             
  where numTeam in (select numTeam from ForReportsByTeam F where F.numUserCntID=@numUserCntId and F.numDomainID=@numDomainId and F.tintType=@tintType))))            
          
                        
  --No of prospects Promoted                          
  SELECT @numNoProsProm=Count(*) FROM divisionMaster DM          
  join AdditionalContactsInformation ADC on ADC.numDivisionID=DM.numDivisionID                 
  WHERE ISNULL(ADC.bitPrimaryContact,0)=1  and DM.tintCRMType=2 and bintProsProm between @dtStartDate and @dtEndDate and bintProsPromBy in          
 (select numContactID from AdditionalContactsInformation where numTeam in           
 (select numTeam from ForReportsByTeam F where F.numUserCntID=@numUserCntId and F.numDomainID=@numDomainId and F.tintType=@tintType))                       
                           
  --Total No of Accounts Added                          
SELECT @numTotNoAccount=Count(*) FROM divisionMaster D                  
join AdditionalContactsInformation A                  
on A.numDivisionID =d.numDivisionID                  
WHERE ISNULL(A.bitPrimaryContact,0)=1 and D.tintCRMType=2 and ((D.bintCreatedDate between @dtStartDate and @dtEndDate and bintProsPromBy is null and D.numRecOwner in           
(select numContactID from AdditionalContactsInformation             
where numTeam in (select numTeam from ForReportsByTeam F where F.numUserCntID=@numUserCntId and F.numDomainID=@numDomainId and F.tintType=@tintType))))                           
or (bintProsProm between @dtStartDate and @dtEndDate and bintProsPromBy in (select numContactID from AdditionalContactsInformation             
where numTeam in (select numTeam from ForReportsByTeam F where F.numUserCntID=@numUserCntId and F.numDomainID=@numDomainId and F.tintType=@tintType)))                        
                          
  --Comunications Scheduled                          
  select @numCommSch=Count(*) from Communication where bitTask=971 and dtCreatedDate between @dtStartDate and @dtEndDate and numCreatedBy in (select numContactID from AdditionalContactsInformation             
  where numTeam in (select numTeam from ForReportsByTeam F where F.numUserCntID=@numUserCntId and F.numDomainID=@numDomainId and F.tintType=@tintType))                          
  --Communication Completed                          
  select @numComComp=Count(*) from Communication where bitTask=971 and dtCreatedDate between @dtStartDate and @dtEndDate and bitClosedFlag=1 and numModifiedBy in (select numContactID from AdditionalContactsInformation             
  where numTeam in (select numTeam from ForReportsByTeam F where F.numUserCntID=@numUserCntId and F.numDomainID=@numDomainId and F.tintType=@tintType))                          
  --Communications Past Due                          
  select @numCommPast=Count(*) from Communication where bitTask=971 and dtCreatedDate between @dtStartDate and @dtEndDate and bitClosedFlag=0 and numCreatedBy in (select numContactID from AdditionalContactsInformation             
  where numTeam in (select numTeam from ForReportsByTeam F where F.numUserCntID=@numUserCntId and F.numDomainID=@numDomainId and F.tintType=@tintType)) and dtStartTime<getutcdate()                           
                
  --Tasks Scheduled                          
  select @numTaskSch=Count(*) from Communication where bitTask=972 and dtCreatedDate between @dtStartDate and @dtEndDate and numCreatedBy in (select numContactID from AdditionalContactsInformation             
  where numTeam in (select numTeam from ForReportsByTeam F where F.numUserCntID=@numUserCntId and F.numDomainID=@numDomainId and F.tintType=@tintType))                          
  --Tasks Completed                            
 select @numTaskComp=Count(*) from Communication where bitTask=972 and dtCreatedDate between @dtStartDate and @dtEndDate and bitClosedFlag=1 and numModifiedBy in (select numContactID from AdditionalContactsInformation      
        where numTeam in (select numTeam from ForReportsByTeam F where F.numUserCntID=@numUserCntId and F.numDomainID=@numDomainId and F.tintType=@tintType))                          
  --Tasks Past Due                          
  select @numTaskPast=Count(*) from Communication where bitTask=972 and dtCreatedDate between @dtStartDate and @dtEndDate and bitClosedFlag=0 and numCreatedBy in (select numContactID from AdditionalContactsInformation             
  where numTeam in (select numTeam from ForReportsByTeam F where F.numUserCntID=@numUserCntId and F.numDomainID=@numDomainId and F.tintType=@tintType)) and dtStartTime<getutcdate()         
                          
  --Cases Opened                          
  select @numCaseOpen=Count(*) from cases where bintCreatedDate between @dtStartDate and @dtEndDate and numCreatedBy in (select numContactID from AdditionalContactsInformation             
  where numTeam in (select numTeam from ForReportsByTeam F where F.numUserCntID=@numUserCntId and F.numDomainID=@numDomainId and F.tintType=@tintType))                          
  --Cased Completed                          
  select @numCaseComp=Count(*) from cases where bintCreatedDate between @dtStartDate and @dtEndDate and numStatus=136 and numModifiedBy in (select numContactID from AdditionalContactsInformation             
  where numTeam in (select numTeam from ForReportsByTeam F where F.numUserCntID=@numUserCntId and F.numDomainID=@numDomainId and F.tintType=@tintType))                    
  --Cases Past Due                          
  select @numCasePast=Count(*) from cases where bintCreatedDate between @dtStartDate and @dtEndDate and numStatus<>136 and numCreatedBy in (select numContactID from AdditionalContactsInformation             
  where numTeam in (select numTeam from ForReportsByTeam F where F.numUserCntID=@numUserCntId and F.numDomainID=@numDomainId and F.tintType=@tintType))  and intTargetResolvedate<getutcdate()         
                          
  SELECT @numOppOpen 'OppOpened',                        
 @numOppOpenAmount 'OppOpenAmt',                        
 @numOpplost 'OppLost',                        
 @numOpplostAmount 'OppLostAmt',                        
 @numOppClose 'OppClosed',                        
 @numOppCloseAmount 'OppCloseAmt',                        
 @numLeadProm 'LeadProm',                        
 @numTotProspect 'TotProsp',                          
    @numNoProsProm 'NoProspProm',                        
 @numTotNoAccount 'TotNoAccount',                        
 @numCommSch 'CommSch',                        
 @numComComp 'CommComp',                          
    @numCommPast 'CommPast',                        
 @numTaskSch 'TaskSch',                        
 @numTaskComp 'TaskComp',                       
 @numTaskPast 'TaskPast',                          
    @numCaseOpen 'CaseOpen',                        
 @numCaseComp 'CaseComp',                        
 @numCasePast 'CasePast'                          
end                        
                        
if @Type=2                        
begin                        
SELECT @numOppOpen=count(*), @numOppOpenAmount=sum(monPAmount) FROM OPPORTUNITYMASTER OM, DivisionMaster DM                           
  WHERE                           
   DM.numDivisionID = OM.numDivisionID  and tintOppType=1                          
  and OM.bintCreatedDate  between @dtStartDate and @dtEndDate and OM.numRecOwner=@numUserCntId                          
                        
                        
--Opportunity lost deals                         
  SELECT @numOpplost=count(*), @numOpplostAmount=sum(monPAmount) FROM OPPORTUNITYMASTER OM                           
   WHERE bintCreatedDate  between @dtStartDate and @dtEndDate and tintOppType=1  and OM.numRecOwner =@numUserCntId and tintOppStatus=2                        
                              
  --Opportunity won deals                          
  SELECT @numOppClose=count(*), @numOppCloseAmount=sum(monPAmount) FROM OPPORTUNITYMASTER OM                           
   WHERE bintCreatedDate  between @dtStartDate and @dtEndDate and tintOppType=1  and OM.numRecOwner =@numUserCntId and tintOppStatus=1                    
                        
                        
  --Leads Promoted                          
  SELECT @numLeadProm=Count(*) FROM divisionMaster DM          
  join AdditionalContactsInformation ADC on           
  ADC.numDivisionID=DM.numDivisionID          
 WHERE tintCRMType=1 AND ISNULL(ADC.bitPrimaryContact,0)=1 AND bintLeadProm between @dtStartDate and @dtEndDate                         
 and bintLeadPromBy =@numUserCntId             
          
                   
                          
  --Total Prospects Added                   
                     
SELECT @numTotProspect=Count(*) FROM divisionMaster D                  
join AdditionalContactsInformation A                  
on A.numDivisionID =d.numDivisionID                  
WHERE ISNULL(A.bitPrimaryContact,0)=1  and D.tintCRMType=1 and ((D.bintCreatedDate between @dtStartDate and @dtEndDate and D.numCreatedBy =@numUserCntId)                           
   or (bintLeadProm between @dtStartDate and @dtEndDate and bintLeadPromBy =@numUserCntId))                         
                          
  --No of prospects Promoted                          
  SELECT @numNoProsProm=Count(*) FROM divisionMaster WHERE tintCRMType=2 and bintProsProm between @dtStartDate and @dtEndDate and bintProsPromBy =@numUserCntId                        
                           
 --Total No of Accounts Added                          
  SELECT @numTotNoAccount=Count(*) FROM divisionMaster D                  
join AdditionalContactsInformation A                  
on A.numDivisionID =d.numDivisionID                  
WHERE ISNULL(A.bitPrimaryContact,0)=1  and D.tintCRMType=2 and ((D.bintCreatedDate between @dtStartDate and @dtEndDate and D.numCreatedBy =@numUserCntId)                         
   or (bintProsProm between @dtStartDate and @dtEndDate and bintProsPromBy =@numUserCntId))                       
  --Comunications Scheduled                          
  select @numCommSch=Count(*) from Communication where bitTask=971 and dtCreatedDate between @dtStartDate and @dtEndDate and numCreatedBy =@numUserCntId                         
  --Communication Completed                          
  select @numComComp=Count(*) from Communication where bitTask=971 and dtCreatedDate between @dtStartDate and @dtEndDate and bitClosedFlag=1 and numCreatedBy =@numUserCntId                          
  --Communications Past Due                          
  select @numCommPast=Count(*) from Communication where bitTask=971 and dtCreatedDate between @dtStartDate and @dtEndDate and bitClosedFlag=0 and numCreatedBy =@numUserCntId  and dtStartTime<getutcdate()                     
                          
  --Tasks Scheduled                          
  select @numTaskSch=Count(*) from Communication where bitTask=972 and dtCreatedDate between @dtStartDate and @dtEndDate and numCreatedBy =@numUserCntId                          
  --Tasks Completed                 
  select @numTaskComp=Count(*) from Communication where bitTask=972 and dtCreatedDate between @dtStartDate and @dtEndDate and bitClosedFlag=1 and numCreatedBy =@numUserCntId                          
  --Tasks Past Due                          
  select @numTaskPast=Count(*) from Communication where bitTask=972 and dtCreatedDate between @dtStartDate and @dtEndDate and bitClosedFlag=0 and numCreatedBy =@numUserCntId  and dtStartTime<getutcdate()                       
    
  --Cases Opened                          
  select @numCaseOpen=Count(*) from cases where bintCreatedDate between @dtStartDate and @dtEndDate and numCreatedBy =@numUserCntId                          
  --Cased Completed                          
  select @numCaseComp=Count(*) from cases where bintCreatedDate between @dtStartDate and @dtEndDate and numStatus=136 and numCreatedBy =@numUserCntId                          
  --Cases Past Due                          
  select @numCasePast=Count(*) from cases where bintCreatedDate between @dtStartDate and @dtEndDate and numStatus<>136 and numCreatedBy =@numUserCntId  and intTargetResolvedate<getutcdate()                         
                     
  SELECT @numOppOpen 'OppOpened',                        
 @numOppOpenAmount 'OppOpenAmt',                        
 @numOpplost 'OppLost',                        
 @numOpplostAmount 'OppLostAmt',                        
 @numOppClose 'OppClosed',             
 @numOppCloseAmount 'OppCloseAmt',                        
 @numLeadProm 'LeadProm',                        
 @numTotProspect 'TotProsp',                          
    @numNoProsProm 'NoProspProm',                        
 @numTotNoAccount 'TotNoAccount',                        
 @numCommSch 'CommSch',                       
 @numComComp 'CommComp',                          
    @numCommPast 'CommPast',                        
 @numTaskSch 'TaskSch',                        
 @numTaskComp 'TaskComp',                        
 @numTaskPast 'TaskPast',                          
    @numCaseOpen 'CaseOpen',                        
 @numCaseComp 'CaseComp',                  
 @numCasePast 'CasePast'                         
end                        
                          
 END
GO
