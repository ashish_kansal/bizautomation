/****** Object:  StoredProcedure [dbo].[usp_getDynamicSearchFormFieldsForAGroup]    Script Date: 07/26/2008 16:17:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Modified By:Anoop Jayaraj       
   --Created By: Debasish Tapan Nag                                            
--Purpose: Retrieves the list of available and selected fields for a Group          
--Created Date: 08/12/2005                                           
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getdynamicsearchformfieldsforagroup')
DROP PROCEDURE usp_getdynamicsearchformfieldsforagroup
GO
CREATE PROCEDURE [dbo].[usp_getDynamicSearchFormFieldsForAGroup]       
	@numDomainID NUMERIC(18,0),
	@numUserCntID NUMERIC(18,0),
	@FormId NUMERIC(18,0),
	@vcDisplayColumns VARCHAR(MAX)
AS        
BEGIN        
	DECLARE @vcLocationID VARCHAR(100) = '0'

	IF @FormId = 15
	BEGIN
		SELECT @vcLocationID ='2,6'
	END
	ELSE
	BEGIN
		SELECT @vcLocationID =isnull(vcLocationID,'0') from dynamicFormMaster where numFormID=@FormId    
	END
      
	SELECT  
		numFieldID as numFormFieldID,
		vcFieldName as vcFormFieldName,
		0 AS bitCustom,
		CONCAT(numFieldID,'~0') AS vcFieldID
	FROM    
		View_DynamicDefaultColumns
	WHERE   
		numFormID = @FormId
		AND bitInResults = 1
		AND bitDeleted = 0 
		AND numDomainID = @numDomainID
	UNION 
	SELECT  
		c.Fld_id AS numFormFieldID
		,c.Fld_label AS vcFormFieldName
		,1 AS bitCustom,
		CONCAT(c.Fld_id,'~1') AS vcFieldID
	FROM    
		CFW_Fld_Master C 
	LEFT JOIN 
		CFW_Validation V ON V.numFieldID = C.Fld_id
	JOIN 
		CFW_Loc_Master L on C.GRP_ID=L.Loc_Id
	WHERE   
		C.numDomainID = @numDomainID
		AND GRP_ID IN(	SELECT ID FROM dbo.SplitIDs(@vcLocationID,','))
	ORDER BY 
		vcFormFieldName ASC 
   
	DECLARE @TEMPSelectedColumns TABLE
	(
		numFormFieldID NUMERIC(18,0)
		,vcFormFieldName VARCHAR(300)
		,vcDbColumnName VARCHAR(300)
		,tintOrder INT
		,vcFieldID VARCHAR(300)
	)

	-- IF USER HAS SELECTED ITS OWN DISPLAY COLUMNS THEN USE IT
	IF LEN(ISNULL(@vcDisplayColumns,'')) > 0
	BEGIN
		INSERT INTO @TEMPSelectedColumns
		(
			numFormFieldID
			,vcFormFieldName
			,vcDbColumnName
			,tintOrder
			,vcFieldID
		)
		SELECT
			numFormFieldID
			,vcFormFieldName
			,vcDbColumnName
			,0
			,vcFieldID
		FROM
		(
			SELECT  
				numFieldID as numFormFieldID,
				vcFieldName as vcFormFieldName,
				0 AS bitCustom,
				CONCAT(numFieldID,'~0') AS vcFieldID,
				vcDbColumnName
			FROM    
				View_DynamicDefaultColumns
			WHERE   
				numFormID = @FormId
				AND bitInResults = 1
				AND bitDeleted = 0 
				AND numDomainID = @numDomainID
			UNION 
			SELECT  
				c.Fld_id AS numFormFieldID
				,c.Fld_label AS vcFormFieldName
				,1 AS bitCustom,
				CONCAT(c.Fld_id,'~1') AS vcFieldID,
				Fld_label AS vcDbColumnName
			FROM    
				CFW_Fld_Master C 
			LEFT JOIN 
				CFW_Validation V ON V.numFieldID = C.Fld_id
			JOIN 
				CFW_Loc_Master L on C.GRP_ID=L.Loc_Id
			WHERE   
				C.numDomainID = @numDomainID
				AND GRP_ID IN(	SELECT ID FROM dbo.SplitIDs(@vcLocationID,','))
		) TEMP
		WHERE
			vcFieldID IN (SELECT OutParam FROM SplitString(@vcDisplayColumns,','))

	END

	-- IF USER SELECTED DISPLAY COLUMNS IS NOT AVAILABLE THAN GET COLUMNS CONFIGURED FROM SEARCH RESULT GRID
	IF (SELECT COUNT(*) FROM @TEMPSelectedColumns) = 0
	BEGIN
		INSERT INTO @TEMPSelectedColumns
		(
			numFormFieldID
			,vcFormFieldName
			,vcDbColumnName
			,tintOrder
			,vcFieldID
		)
		SELECT
			numFormFieldID
			,vcFormFieldName
			,vcDbColumnName
			,tintOrder
			,vcFieldID
		FROM
		(
			SELECT  A.numFormFieldID,
					vcFieldName as vcFormFieldName,
					vcDbColumnName,
					A.tintOrder,
					CONCAT(numFieldID,'~0') AS vcFieldID
			FROM    AdvSerViewConf A
					JOIN View_DynamicDefaultColumns D ON D.numFieldID = A.numFormFieldID
			WHERE   A.numFormID = @FormId
					AND D.bitInResults = 1
					AND D.bitDeleted = 0
					AND D.numDomainID = @numDomainID 
					AND D.numFormID = @FormId
					AND A.numDomainID = @numDomainID
					AND A.numUserCntID=@numUserCntID
					AND ISNULL(A.bitCustom,0) = 0
			UNION
			SELECT  A.numFormFieldID,
					c.Fld_label as vcFormFieldName,
					c.Fld_label,
					A.tintOrder,
					CONCAT(c.Fld_id,'~1') AS vcFieldID
			FROM    AdvSerViewConf A
					JOIN CFW_Fld_Master C ON C.Fld_id = A.numFormFieldID
			WHERE   A.numFormID = @FormId
					AND C.numDomainID = @numDomainID 
					AND A.numDomainID = @numDomainID
					AND A.numUserCntID=@numUserCntID
					AND ISNULL(A.bitCustom,0) = 1
		) T1
		ORDER BY
			 tintOrder
	END

	-- IF USER HASN'T CONFIGURED COLUMNS FROM SEARCH RESULT GRID THEN RETURN DEFAULT COLUMNS
	IF (SELECT COUNT(*) FROM @TEMPSelectedColumns) = 0
	BEGIN
		INSERT INTO @TEMPSelectedColumns
		(
			numFormFieldID
			,vcFormFieldName
			,vcDbColumnName
			,tintOrder
			,vcFieldID
		)
		SELECT  
			numFieldID,
			vcFieldName,
			vcDbColumnName,
			0,
			CONCAT(numFieldID,'~0')
		FROM    
			View_DynamicDefaultColumns
		WHERE   
			numFormID = @FormId
			AND bitInResults = 1
			AND bitDeleted = 0 
			AND numDomainID = @numDomainID
			AND numFieldID = (CASE @FormId WHEN 1 THEN 3 WHEN 15 THEN 96 WHEN 19 THEN 3 WHEN 29 THEN 189 WHEN 17 THEN 127 WHEN 18 THEN 148 WHEN 59 THEN 423 END)
	END

	SELECT * FROM @TEMPSelectedColumns
END
GO

