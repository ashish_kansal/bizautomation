/****** Object:  StoredProcedure [dbo].[USP_GetPerAnaylsCaseList]    Script Date: 07/26/2008 16:18:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--  By Anoop Jayaraj                                      
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getperanaylscaselist')
DROP PROCEDURE usp_getperanaylscaselist
GO
CREATE PROCEDURE [dbo].[USP_GetPerAnaylsCaseList]                                         
 @numUserCntId numeric(9)=0,                                              
 @numDomainID numeric(9)=0,                                  
 @dtStartDate datetime,                                   
 @dtEndDate datetime,                                    
 @RepType as tinyint,                                  
 @Condition Char(1),                                       
 @CurrentPage int,                                      
 @PageSize int,                                      
 @TotRecs int output,                                      
 @columnName as Varchar(50),                                      
 @columnSortOrder as Varchar(10),                                
 @strUserIDs as varchar(1000),                  
 @TeamType as tinyint                                  
                                      
AS                                       
 if @strUserIDs='' set @strUserIDs='0'                                           
declare @strSql as varchar(5000)                                 
                                
   if   @RepType<>3                                
 begin                                
Create table #tempTable ( ID INT IDENTITY PRIMARY KEY,                                       
      [Name] VARCHAR(110),                                      
      vcCompanyName varchar(100),                                      
      numCaseID VARCHAR(15),                                      
      vcCaseNumber varchar(25),                                      
 intTargetResolveDate varchar(20),                                      
 numCreatedBy varchar(15),                                      
 textSubject text,                                      
 Priority varchar(50),                                      
 numCompanyID varchar(15),                                      
 numDivisionID varchar(15),                                      
 numTerId varchar(15),                                      
 tintCRMType  varchar(10),                                      
 ActCntID varchar(15) ,                                      
 CaseOwner varchar(110)                                      
 )                                      
                                       
                                     
set @strSql='SELECT                                         
   ADC.vcFirstName +'' ''+ADC.vcLastName as Name,                                      
   CMP.vcCompanyName,                                        
   Cs.numCaseID,                                        
   Cs.vcCaseNumber,                                        
   Cs.intTargetResolveDate,                                        
   Cs.numRecOwner as numCreatedBy,                                        
   Cs.textSubject,                                        
   lst.vcdata as Priority,                                            
   CMP.numCompanyID,                                        
   Div.numDivisionID,                                        
   Div.numTerId,                                        
   Div.tintCRMType,                                        
   ADC.numContactID as ActCntID,                                      
   ADC.vcFirstName +'' ''+ADC.vcLastName as CaseOwner                                      
 FROM                                         
   AdditionalContactsInformation ADC                                       
 JOIN Cases  Cs                                        
   ON ADC.numContactId =Cs.numRecOwner                                       
 JOIN DivisionMaster Div                                         
  ON ADC.numDivisionId = Div.numDivisionID                                         
  JOIN CompanyInfo CMP                                       
 ON Div.numCompanyID = CMP.numCompanyId                                       
 left join listdetails lst                                      
 on lst.numListItemID= cs.numPriority                                      
 Where'                               
-- cases Opened                             
if   @Condition='0' set @Condition='O'                               
if @Condition='O' and @RepType=1 set @strSql=@strSql +' Cs.bintCreatedDate between ''' + convert(varchar(20),@dtStartDate)+'''                                    
 and ''' + convert(varchar(20),@dtEndDate)+''' and Cs.numRecOwner in (select numContactId from AdditionalContactsInformation            
where numTeam in(select numTeam from ForReportsByTeam where                               
numUserCntID=' + convert(varchar(15),@numUserCntId)+' and numDomainID=' + convert(varchar(15),@numDomainId)+' and tintType=' + convert(varchar(15),@TeamType)+')) '                                  
                              
if @Condition='O' and @RepType=2 set @strSql=@strSql +' Cs.bintCreatedDate between ''' + convert(varchar(20),@dtStartDate)+'''                                    
 and ''' + convert(varchar(20),@dtEndDate)+''' and Cs.numRecOwner =' + convert(varchar(15),@numUserCntId)                                  
                                  
-- cases Completed                                  
if @Condition='C' and @RepType=1 set @strSql=@strSql +' Cs.bintCreatedDate between ''' + convert(varchar(20),@dtStartDate)+'''  and ''' + convert(varchar(20),@dtEndDate)+''' and Cs.numStatus=136                                   
and Cs.numModifiedBy in (select numContactId from AdditionalContactsInformation                     
where numTeam in(select numTeam from ForReportsByTeam where                               
numUserCntID=' + convert(varchar(15),@numUserCntId)+' and numDomainID=' + convert(varchar(15),@numDomainId)+' and tintType=' + convert(varchar(15),@TeamType)+')) '                                   
                                  
if @Condition='C' and @RepType=2 set @strSql=@strSql +' Cs.bintCreatedDate between ''' + convert(varchar(20),@dtStartDate)+'''  and ''' + convert(varchar(20),@dtEndDate)+''' and Cs.numStatus=136                                   
and Cs.numModifiedBy =' + convert(varchar(15),@numUserCntId)                                  
                                  
                                  
-- cases Completed                                  
if @Condition='P' and @RepType=1 set @strSql=@strSql +' Cs.bintCreatedDate between ''' + convert(varchar(20),@dtStartDate)+'''  and ''' + convert(varchar(20),@dtEndDate)+''' and numStatus<>136        
and intTargetResolvedate< ''' + convert(varchar(20),getutcdate())+'''                                   
and Cs.numRecOwner in (select numContactId from AdditionalContactsInformation                                     
where numTeam in(select numTeam from ForReportsByTeam where                               
numUserCntID=' + convert(varchar(15),@numUserCntId)+' and numDomainID=' + convert(varchar(15),@numDomainId)+' and tintType=' + convert(varchar(15),@TeamType)+'))'                            
                                  
if @Condition='P' and @RepType=2 set @strSql=@strSql +' Cs.bintCreatedDate between ''' + convert(varchar(20),@dtStartDate)+'''  and ''' + convert(varchar(20),@dtEndDate)+''' and numStatus<>136                                   
and intTargetResolvedate< ''' + convert(varchar(20),getutcdate())+'''        
and Cs.numRecOwner =' + convert(varchar(15),@numUserCntId)                                      
print (@strSql)                                     
insert into #tempTable ([Name] ,                                      
      vcCompanyName ,                                      
      numCaseID,                                      
      vcCaseNumber,                                      
 intTargetResolveDate,                                      
 numCreatedBy,                                      
 textSubject,                                      
 Priority,                                      
 numCompanyID,                                      
 numDivisionID,                                      
 numTerId,                      
 tintCRMType,                                      
 ActCntID,                                      
 CaseOwner)                                      
exec( @strSql)                                      
  declare @firstRec as integer                                      
  declare @lastRec as integer                                      
 set @firstRec= (@CurrentPage-1) * @PageSize                                      
     set @lastRec= (@CurrentPage*@PageSize+1)                                      
select * from #tempTable where ID > @firstRec and ID < @lastRec                                      
set @TotRecs=(select count(*) from #tempTable)                
drop table #tempTable                               
                                
end                                
                                
if @RepType =3                                
begin                                
set @strSql='SELECT ADC1.numContactId as numUserID,isnull(vcFirstName,''-'') as vcFirstName,isnull(vcLastName,''-'') as vcLastName,(SELECT                                   
 count(*)                                
 FROM  Cases Cs '                                  
                        
                                
if @Condition='0'  set @strSql=@strSql +' where Cs.bintCreatedDate between ''' + convert(varchar(20),@dtStartDate)+'''                                 
 and ''' + convert(varchar(20),@dtEndDate)+''' and Cs.numRecOwner  =ADC1.numContactId) as NoofOpp FROM  AdditionalContactsInformation ADC1                                    
WHERE  ADC1.numContactId in ('+@strUserIDs+')'                                
                                
                                
if @Condition='C' set @strSql=@strSql +'where Cs.bintCreatedDate between ''' + convert(varchar(20),@dtStartDate)+'''  and ''' + convert(varchar(20),@dtEndDate)+''' and Cs.numStatus=136                                   
and Cs.numModifiedBy   =ADC1.numContactId) as NoofOpp FROM   AdditionalContactsInformation ADC1                                   
WHERE  ADC1.numContactId in ('+@strUserIDs+')'                                
                                
                                
if @Condition='P' set @strSql=@strSql +'where Cs.bintCreatedDate between ''' + convert(varchar(20),@dtStartDate)+'''  and ''' + convert(varchar(20),@dtEndDate)+''' and numStatus<>136                                   
and intTargetResolvedate< ''' + convert(varchar(20),getutcdate())+'''        
and Cs.numRecOwner =ADC1.numContactId) as NoofOpp FROM   AdditionalContactsInformation ADC1                                   
WHERE   ADC1.numContactId in ('+@strUserIDs+')'                                    
print @strSql                                
                                
exec (@strSql)                                 
end
GO
