/****** Object:  StoredProcedure [dbo].[USP_EditCntInfo]    Script Date: 07/26/2008 16:15:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created By Anoop Jayaraj                                 
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_editcntinfo')
DROP PROCEDURE usp_editcntinfo
GO
CREATE PROCEDURE [dbo].[USP_EditCntInfo]                                                
@numContactID NUMERIC(9) ,    
@numDomainID as numeric(9)  
 ,  
@ClientTimeZoneOffset as int                     
                                              
AS                                                
BEGIN                                                
                                                
  SELECT                        
   A.numContactId, A.vcGivenName, A.vcFirstName,                                                 
                      A.vcLastName, D.numDivisionID, C.numCompanyId,                                                 
                      C.vcCompanyName, D.vcDivisionName, D.numDomainID,  
					  ISNULL((select vcdata from listdetails where numListItemID = C.numCompanyType),'') as vcCompanyTypeName,                                               
                      A.numPhone, A.numPhoneExtension, A.vcEmail,  A.numTeam,                                               
                      A.vcFax, A.numContactType,  A.charSex, A.bintDOB,                                                 
                      A.vcPosition, A.txtNotes, A.numCreatedBy,                                                
                      A.numCell,A.NumHomePhone,A.vcAsstFirstName,                                                
                      A.vcAsstLastName,A.numAsstPhone,A.numAsstExtn,                                                
                      A.vcAsstEmail,A.charSex, A.vcDepartment,                                                 
                      AD.vcStreet AS vcpStreet,                                            
                      AD.vcCity AS vcPCity, 
                      dbo.fn_GetState(AD.numState) as State, dbo.fn_GetListName(AD.numCountry,0) as Country,
                      AD.vcPostalCode AS vcPPostalCode,A.bitOptOut,vcDepartment,                                                
        A.numManagerID, A.vcCategory  , dbo.fn_GetContactName(A.numCreatedBy)+' '+ convert(varchar(20),dateadd(minute,-@ClientTimeZoneOffset,A.bintCreatedDate)) CreatedBy,                                            
          dbo.fn_GetContactName(A.numModifiedBy)+' '+ convert(varchar(20),dateadd(minute,-@ClientTimeZoneOffset,A.bintModifiedDate)) ModifiedBy,vcTitle,vcAltEmail,                                            
  dbo.fn_GetContactName(A.numRecOwner) as RecordOwner,  A.numRecOwner, D.numAssignedTo,  D.numTerID,
  C.vcCompanyName,C.vcWebSite, 
--  dbo.fn_GetState(AddC.vcPState) as State, dbo.fn_GetListName(AddC.vcPCountry,0) as Country,
   A.numEmpStatus,                          
  isnull((select top 1 case when bitEngaged=0 then 'Disengaged' when bitEngaged=1 then 'Engaged' end as ECampStatus  from ConECampaign                          
where  numContactID=@numContactID order by numConEmailCampID desc),'-') as CampStatus ,                        
dbo.GetECamLastAct(@numContactID) as Activity ,       
(select  count(*) from dbo.GenericDocuments   where numRecID=@numContactID and  vcDocumentSection='C') as DocumentCount,
(SELECT count(*)from CompanyAssociations where numDivisionID=@numContactID and bitDeleted=0 ) as AssociateCountFrom,              
(SELECT count(*)from CompanyAssociations where numAssociateFromDivisionID=@numContactID and bitDeleted=0 ) as AssociateCountTo    ,        
vcItemId ,        
vcChangeKey,              
vcCompanyName,      
tintCRMType ,isnull((select top 1 numUserId from  UserMaster where numUserDetailId=A.numContactId),0) as numUserID,
dbo.fn_GetListName(C.numNoOfEmployeesId,0) AS NoofEmp,ISNULL(vcImageName,'') vcImageName
FROM         AdditionalContactsInformation A     
INNER JOIN  DivisionMaster D ON A.numDivisionId = D.numDivisionID     
INNER JOIN  CompanyInfo C ON D.numCompanyID = C.numCompanyId                      
LEFT JOIN AddressDetails AD ON  A.numDomainID = AD.numDomainID AND AD.numRecordID= A.numContactId AND AD.tintAddressOf=1 AND AD.tintAddressType=0 AND AD.bitIsPrimary=1
WHERE     A.numContactId = @numContactID and A.numDomainID=@numDomainID                  
                                   
END
GO
