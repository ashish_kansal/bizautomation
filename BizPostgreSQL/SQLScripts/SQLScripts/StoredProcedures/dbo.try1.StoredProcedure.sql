/****** Object:  StoredProcedure [dbo].[try1]    Script Date: 07/26/2008 16:14:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='try1')
DROP PROCEDURE try1
GO
CREATE PROCEDURE [dbo].[try1] 
@sortorder as varchar(100) = 'asc'
as
begin
declare @sortcolumn as varchar(100)

declare @strsql as varchar(8000)   
set @sortcolumn = 'bittask'


set @strsql='SELECT PriceRank,bittask,textdetails,intsnoozemins
FROM
   (SELECT top 100 bittask,textdetails,intsnoozemins,
       ROW_NUMBER() OVER(ORDER BY numcommid DESC) AS PriceRank
    FROM communication 
   ) AS ProductsWithRowNumber
WHERE PriceRank > 0 AND
    PriceRank <= (0 + 20) order by '+@sortcolumn +' '+ @sortorder
print @strsql
exec(@strsql)
end
GO
