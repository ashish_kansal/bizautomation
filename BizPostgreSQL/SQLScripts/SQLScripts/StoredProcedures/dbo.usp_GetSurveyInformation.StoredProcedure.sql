
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--Created By: Debasish Nag            
--Created On: 09/13/2005            
--Purpose: Returns the Survey Master information            
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getsurveyinformation')
DROP PROCEDURE usp_getsurveyinformation
GO
CREATE PROCEDURE [dbo].[usp_GetSurveyInformation]    
 @numSurId Numeric            
AS
--This procedure will return all survey master information for the selected survey            
BEGIN              
 SELECT numQID, vcQuestion, tIntAnsType, intNumOfAns, 0 as boolDeleted,Case boolMatrixColumn When 1 Then 1 Else 0 End As boolMatrixColumn ,intNumOfMatrixColumn          
  FROM SurveyQuestionMaster              
  WHERE numSurId = @numSurId            
          
             
 SELECT numSurID, numQID, numAnsID, vcAnsLabel, Case boolSurveyRuleAttached When 1 Then 1 Else 0 End As boolSurveyRuleAttached, 0 as boolDeleted          
  FROM SurveyAnsMaster              
  WHERE  numSurId = @numSurId            
          
 SELECT numRuleID, numSurID, numAnsID, numQID, Case boolActivation When 1 Then 1 Else 0 End As boolActivation, numEventOrder,           
  IsNull(vcQuestionList,'') AS vcQuestionList, numLinkedSurID, numResponseRating, IsNull(vcPopUpURL,'') AS vcPopUpURL, numGrpId, numCompanyType,          
  tIntCRMType, numRecOwner , isnull( tIntRuleFourRadio,0) as tIntRuleFourRadio ,Isnull(tIntCreateNewID,0) as tIntCreateNewID,isnull(vcRuleFourSelectFields,'') as vcRuleFourSelectFields,
boolPopulateSalesOpportunity, ISnull( vcRuleFiveSelectFields,'' ) as vcRuleFiveSelectFields,numMatrixID,vcItem
  FROM SurveyWorkflowRules              
  WHERE  numSurId = @numSurId   
  
   SELECT numSurID, numQID, numMatrixID, vcAnsLabel, Case boolSurveyRuleAttached When 1 Then 1 Else 0 End As boolSurveyRuleAttached, 0 as boolDeleted     
  FROM SurveyMatrixAnsMaster                  
  WHERE  numSurId = @numSurId   
  
  SELECT *    
  FROM SurveyCreateRecord                  
  WHERE  numSurID = @numSurID      
END
GO

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

