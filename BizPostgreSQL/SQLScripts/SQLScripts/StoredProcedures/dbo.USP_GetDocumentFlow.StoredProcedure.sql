
GO
/****** Object:  StoredProcedure [dbo].[USP_GetDocumentFlow]    Script Date: 03/30/2010 23:46:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getdocumentflow')
DROP PROCEDURE usp_getdocumentflow
GO
CREATE PROCEDURE [dbo].[USP_GetDocumentFlow]           
@numRecID as numeric(9)=0,        
@strType as varchar(10)='',          
@numCategory as numeric(9)=0,
@tintUserRightType tinyint=0, 
@numDomainID as numeric(9)=0 ,
@tintSortOrder tinyint=4,                         
@SortChar char(1)='0',
@KeyWord as varchar(100)='', 
@CurrentPage int,  
@PageSize int ,
@columnName as Varchar(50),                    
@columnSortOrder as Varchar(10),
@numDocStatus as numeric(9)=0,
@TotRecs int output,
@numFilter int=0,
@numUserId numeric(18,0)=0
      
as          
begin

create table #TempDocFlow
(Organization varchar(200),
[Document Name]  varchar(200),
[Document Category] varchar(200),
[File Type] varchar(50),
[Document Status] varchar(150),
Pending numeric(8),
Approved numeric(8),
Declined  numeric(8),
CreatedBy varchar(150),
ModifiedOn datetime,
numGenericDocID numeric(8),
VcFileName varchar(150),
cUrlType  varchar(150),
numDivisionId numeric(8),
tintCRMType numeric(8),
numOppId numeric(8),
BizDocTypeId varchar(1))

declare @vcDocumentSection varchar(1);
set @vcDocumentSection='D';

	
declare @strSql as varchar(8000)                    
declare @intCustomPageRow numeric(8)

select @intCustomPageRow=isnull(tintCustomPagingRows,20) from Domain where numDomainID=@numDomainID;

set @strSql='select '  
  
if (@tintSortOrder=5 or @tintSortOrder=6) set @strSql=@strSql+ ' DISTINCT top  ' + cast (@intCustomPageRow  as varchar(4))
ELSE set @strSql = @strSql + ' Distinct ';

set @strSql = @strSql + 
' G.vcCompanyName,
[Order Id] ,
OrderType + cast(BizDocID as varchar(50)),NULL AS FILETYPE ,
H.vcData ,
(SELECT count(btStatus) from BizActionDetails C where c.numOppBizDocsId=a.numOppBizDocsId and btStatus=0), 
(SELECT count(btStatus) from BizActionDetails C where c.numOppBizDocsId=a.numOppBizDocsId and btStatus=1) , 
(SELECT count(btStatus) from BizActionDetails C where c.numOppBizDocsId=a.numOppBizDocsId and btStatus=2) ,
dbo.fn_GetContactName(B.BizDocCreatedBy) ,
BizDocModifiedDate ,
A.numOppBizDocsId ,
'''' as VcFileName,
'''' as cUrlType,
f.numDivisionId,
f.tintCRMType,
numOppId,
''B'' as BizDocTypeId
from BizActionDetails A, 
	View_BizDoc B, 
	BizDocAction D,
	AdditionalContactsInformation E,
	DivisionMaster F,
	CompanyInfo G,
	ListDetails H
where A.numOppBizDocsId=b.numOppBizDocsId and 
	A.numBizActionID=D.numBizActionID and 
	D.numDomainID=B.numDomainID and 
	D.numDomainID=' + cast(@numDomainID as varchar(10)) + ' and 
	D.numAssign=' + cast(@numRecID as varchar(10)) + ' and 
	A.btDocType=1  and 
	E.numContactId=BizDocCreatedBy and 
	F.numDivisionID=b.DivisionID and
	G.numCompanyId=F.numCompanyId	and 
	btDocType=1 and 
	H.numListItemid=B.numBizDocId'
	if @numDocStatus<> 0 set @strSql=@strSql + ' and b.numBizDocId=' + cast(@numDocStatus as varchar(10)) 
	if @numCategory<> 0 set 	@strSql=@strSql + ' and numBizDocStatus='+ cast(@numCategory as varchar(10)) 
	if @SortChar<>'0' set @strSql=@strSql + ' and BizDocId like ''' + @SortChar + '%''' 
	if @KeyWord<>'' set @strSql=@strSql + ' and BizDocId like ''' + @KeyWord + '%''' 
+ ' order by BizDocModifiedDate desc'
	
print @strSql

insert into #TempDocFlow exec(@strSql)
set @strSql='select '  
  

set @strSql ='select distinct 
E.vcCompanyName,
[Order Id],
OrderType,
NULL AS FILETYPE,
BizDocId,
(select count(f.tintApprove) from DocumentWorkflow F where F.cDocType=''B'' and F.numDocID=c.numDocID and f.tintApprove=0),
(select count(f.tintApprove) from DocumentWorkflow F where F.cDocType=''B'' and F.numDocID=c.numDocID and f.tintApprove=1),
(select count(f.tintApprove) from DocumentWorkflow F where F.cDocType=''B'' and F.numDocID=c.numDocID and f.tintApprove=2),
dbo.fn_GetContactName(A.BizDocCreatedBy) ,
BizDocModifiedDate ,
A.numOppBizDocsId ,
'''' as VcFileName,
'''' as cUrlType,
D.numDivisionId,
D.tintCRMType,
A.numOppId,
''B'' as BizDocTypeId
from 
View_BizDoc A inner join OpportunityMaster B on a.numOppid=b.numOppid 
inner join (select distinct numDocID from DocumentWorkflow where cDocType=''B'' and tintApprove=0) C
on C.numDocID=A.numOppBizDocsId inner join  DivisionMaster D 
on D.numDivisionId=B.numDivisionID inner join CompanyInfo E
on E.numCompanyId=D.numCompanyId and A.numOppBizDocsId not in ( select numGenericDocID from #TempDocFlow )'
if @numDocStatus<> 0 set @strSql=@strSql + ' and A.numBizDocId=' + cast(@numDocStatus as varchar(10)) 
if @numCategory<> 0 set 	@strSql=@strSql + ' and numBizDocStatus='+ cast(@numCategory as varchar(10)) 
if @SortChar<>'0' set @strSql=@strSql + ' and BizDocId like ''' + @SortChar + '%''' 
if @KeyWord<>'' set @strSql=@strSql + ' and BizDocId like ''' + @KeyWord + '%''' 
if @numFilter=1 set @strSql=@strSql + ' and BizDocCreatedBy =' +cast( @numUserId as varchar(10))
if @numFilter=2 set @strSql=@strSql + ' and BizDocCreatedBy not in (' + cast( @numUserId as varchar(10)) + ')' 
if @numFilter=4 set @strSql=@strSql + ' and dtCreatedDate >=dateadd(day,-7,getutcdate()) '  
if @numFilter=5 set @strSql=@strSql + ' and dtCreatedDate >= dateadd(day,-20,getutcdate()) and BizDocCreatedBy =' + cast( @numUserId as varchar(10)) 
if @numFilter=6 set @strSql=@strSql + ' and BizDocModifiedDate >= dateadd(day,-20,getutcdate()) and BizDocCreatedBy =' + cast( @numUserId as varchar(10)) 
 set @strSql=@strSql + ' WHERE B.numDomainID=' + cast(@numDomainID as varchar(10)) + ' order by BizDocModifiedDate desc'




print @strSql

insert into #TempDocFlow exec(@strSql)





if (@tintSortOrder=5 or @tintSortOrder=6) set @strSql=@strSql+ ' top  ' + cast (@intCustomPageRow  as varchar(4))



set @strSql ='select
vcCompanyName AS Organization,
vcDocName as [Document Name],
dbo.fn_GetListItemName(numDocCategory) as [Document Category] ,
vcfiletype as [File Type],
dbo.fn_GetListItemName(numDocStatus) as [Document Status],
(select count(*) from DocumentWorkflow             
 where numDocID=numGenericDocId and cDocType=''' + @vcDocumentSection  + ''' and tintApprove=0) as Pending,
(select count(*) from DocumentWorkflow             
 where numDocID=numGenericDocId and cDocType=''' + @vcDocumentSection  + ''' and tintApprove=1) as Approved,
 (select count(*) from DocumentWorkflow             
 where numDocID=numGenericDocId and cDocType=''' + @vcDocumentSection  + ''' and tintApprove=2) as Declined,
  dbo.fn_GetContactName(GP.numModifiedBy) as CreatedBy,
	GP.bintModifiedDate as ModifiedOn,
  numGenericDocId,
  VcFileName ,  
  cUrlType,
DV.numDivisionId,
DV.tintCRMType,
0,
''D'' as BizDocTypeId
  from GenericDocuments GP,AdditionalContactsInformation AC, DivisionMaster DV,CompanyInfo CI ,
(select distinct numDocID from DocumentWorkflow where cDocType<>''B'' and tintApprove=0) WF    
  where GP.numCreatedBy=' + cast (@numRecID as varchar(20))  
if @strType <> '' set  @strSql=@strSql+' and vcDocumentSection=' + @strType
 set @strSql=@strSql + '  and GP.numDomainID=' + cast(@numDomainID as varchar(20)) + ' and 
	GP.numCreatedBy=AC.numContactId and 
	AC.numDivisionId=DV.numDivisionId and 
	DV.numCompanyID=CI.numCompanyID	 and 
WF.numDocID=GP.numGenericDocId'



if   @KeyWord<>'' set @strSql=@strSql+' and  (vcDocName like ''%'+@KeyWord+'%''                     
 or vcfiletype like  ''%'+@KeyWord+'%''                     
 or vcDocDesc like  ''%'+@KeyWord+'%'')' 
if @numDocStatus<> 0 set @strSql=@strSql +  '   and  numDocStatus   =' + convert(varchar(15),@numDocStatus)       
if @numCategory<>0 set  @strSql=@strSql +  ' and numDocCategory=' + convert(varchar(15),@numCategory)                
if @SortChar<>'0' set @strSql=@strSql + ' And UPPER(vcDocName) like upper('''+@SortChar+'%'')'                     
if @tintUserRightType=1 set @strSql=@strSql + '  AND (GP.numCreatedBy = '+convert(varchar(15),@numRecID)+ ')'                            
                    
if @tintSortOrder=1  set @strSql=@strSql + ' AND GP.numCreatedBy= '+ convert(varchar(15),@numRecID)                     
else if @tintSortOrder=2  set @strSql=@strSql + ' AND GP.numCreatedBy in (select numContactID from AdditionalContactsInformation A                               
where A.numManagerID='+ convert(varchar(15),@numRecID)+')'                               
else if @tintSortOrder=6  set @strSql=@strSql + ' AND GP.numCreatedBy= '+ convert(varchar(15),@numRecID) 

               
if @tintSortOrder=1  set @strSql=@strSql + ' ORDER BY ' + @columnName +' '+ @columnSortOrder                    
else if @tintSortOrder=2  set @strSql=@strSql + ' ORDER BY ' + @columnName +' '+ @columnSortOrder                            
else if @tintSortOrder=3  set @strSql=@strSql + ' ORDER BY ' + @columnName +' '+ @columnSortOrder                        
else if @tintSortOrder=4  set @strSql=@strSql + ' AND GP.numCreatedBy= '+ convert(varchar(15),@numRecID) +' and GP.numCreatedBy >= dateadd(day,-7,getutcdate())  ORDER BY ' + @columnName +' '+ @columnSortOrder                    
else if @tintSortOrder=5  set @strSql=@strSql + '  AND GP.numCreatedBy= '+ convert(varchar(15),@numRecID) +' ORDER BY ' + @columnName +' '+ @columnSortOrder                    
else if @tintSortOrder=6  set @strSql=@strSql +'  ORDER BY ' + @columnName +' '+ @columnSortOrder                    
 


print(@strSql);

insert into #TempDocFlow
exec (@strSql)   


select @TotRecs = isnull(count(*),0) from  #TempDocFlow;
select * from #TempDocFlow	




 select SD.numGenericDocID, DW.dtCreatedDate as [Approval Request Date],
vcCompanyName as Company,
vcFirstName+' '+vcLastName as [Name],
cast (numPhone as varchar(100)) + ',' + cast (numPhoneExtension as varchar(100)) as [Phone],
vcEmail as [Email],
(case tintApprove when 0 then 'Pending' when 1 then 'Approved' else 'Declined' end) as Status,
vcComment as Comments,
dtApprovedOn as ApprovedOn ,
AC.numContactID
 from DocumentWorkflow DW JOIN GenericDocuments SD 
on DW.numDocID = SD.numGenericDocID join AdditionalContactsInformation AC
on DW.numContactID=AC.numContactID join DivisionMaster DM 
on DM.numDivisionId=AC.numDivisionId join CompanyInfo CI
on CI.numCompanyId=DM.numCompanyId
where SD.numCreatedBy=@numRecID and 
SD.numGenericDocID IN (SELECT numGenericDocID FROM #TempDocFlow)

union

select numOppBizDocsId,dtCreatedDate,
e.vcCompanyName,
vcFirstName+' '+vcLastName as [Name],
cast (numPhone as varchar(100)) + ',' + cast (numPhoneExtension as varchar(100)) as [Phone],
vcEmail as [Email],
(case btStatus when 0 then 'Pending' when 1 then 'Approved' else 'Declined' end) as Status,
vcComment as Comments,
dtApprovedOn,
A.numContactId
from 
	BizDocAction A
INNER JOIN
	BizActionDetails B
ON
	 A.numBizActionId=B.numBizActionId
INNER JOIN
	AdditionalContactsInformation C
ON
	A.numAssign=C.numContactId
INNER JOIN
	DivisionMaster D
ON
	C.numDivisionId=D.numDivisionId
INNER JOIN
	CompanyInfo E
ON
	E.numCompanyId=d.numCompanyId
where  
	btDocType=1 and
	numOppBizDocsId in (SELECT numGenericDocID FROM #TempDocFlow)

union
 

select numOppBizDocsId,
		a.dtCreatedDate,
		vcCompanyName,
		vcFirstName+' '+vcLastName as [Name],
cast (numPhone as varchar(100)) + ',' + cast (numPhoneExtension as varchar(100)) as [Phone],
vcEmail as [Email],
(case tintApprove when 0 then 'Pending' when 1 then 'Approved' else 'Declined' end) as Status,
vcComment as Comments,
dtApprovedOn as ApprovedOn ,A.numContactId	
 from 
	DocumentWorkflow A
INNER JOIN
	View_bizdoc b
ON
	A.numDocId=b.numOppBizDocsId
INNER JOIN
	AdditionalContactsInformation d
ON
	A.numContactId=d.numContactId
INNER JOIN
	DivisionMaster e
ON
	d.numDivisionId = e.numDivisionID
INNER JOIN
	CompanyInfo f
ON
	e.numCompanyId=f.numCompanyId
INNER JOIN
	ListDetails h
ON
	H.numListItemid=B.numBizDocId
where 
cDocType='B' and 
b.numDomainID=@numDomainID and 
--b.numBizDocStatus=@numCategory and 
b.numBizDocId=@numDocStatus and 
b.numOppBizDocsId in (SELECT numGenericDocID FROM #TempDocFlow) and 
b.numOppBizDocsId not in (select numOppBizDocsId from BizActionDetails where numOppBizDocsId in  (SELECT numGenericDocID FROM #TempDocFlow) ) 

UNION 


select numGenericDocId,
		A.dtCreatedDate,
		vcCompanyName,
		vcFirstName+' '+vcLastName as [Name],
cast (numPhone as varchar(100)) + ',' + cast (numPhoneExtension as varchar(100)) as [Phone],
vcEmail as [Email],
(case tintApprove when 0 then 'Pending' when 1 then 'Approved' else 'Declined' end) as Status,
vcComment as Comments,
dtApprovedOn as ApprovedOn ,A.numContactId

 from 
	DocumentWorkflow A
INNER JOIN 
	GenericDocuments b
ON
	A.numDocId=b.numGenericDocId
INNER JOIN
	AdditionalContactsInformation d
ON
	A.numContactId=d.numContactId
INNER JOIN
	DivisionMaster e
ON
	d.numDivisionId=e.numDivisionId
INNER JOIN
	CompanyInfo f
ON
	e.numCompanyId=f.numCompanyId
where 
cDocType='D' and 
b.numDomainID=@numDomainID and 
b.numGenericDocId in (SELECT numGenericDocID FROM #TempDocFlow)


drop table #TempDocFlow;
end

