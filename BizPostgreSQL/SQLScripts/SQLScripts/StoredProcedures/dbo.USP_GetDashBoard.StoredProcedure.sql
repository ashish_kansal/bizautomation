/****** Object:  StoredProcedure [dbo].[USP_GetDashBoard]    Script Date: 07/26/2008 16:17:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj            
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getdashboard')
DROP PROCEDURE usp_getdashboard
GO
CREATE PROCEDURE [dbo].[USP_GetDashBoard]            
@numUserCntID as numeric(9),            
@numGroupID as numeric(9),            
@numDomainID as numeric(9)            
as            
            
if @numUserCntID=0            
begin            
 select tintColumn,tintSize from DashBoardSize where bitGroup=1 and numGroupUserCntID=@numGroupID order by tintColumn            
end            
else if exists(select * from DashBoardSize where bitgroup=0 and numGroupUserCntID=@numUserCntID)            
begin            
 select tintColumn,tintSize from DashBoardSize where bitGroup=0 and numGroupUserCntID=@numUserCntID order by tintColumn            
end            
else            
begin    
 insert into DashBoardSize(tintColumn,tintSize,numGroupUserCntID,bitGroup)            
 select tintColumn,tintSize,@numUserCntID,0 from DashBoardSize where bitGroup=1 and numGroupUserCntID=@numGroupID order by tintColumn     
 select tintColumn,tintSize from DashBoardSize where bitGroup=0 and numGroupUserCntID=@numUserCntID order by tintColumn            
end            
  
  CREATE TABLE #tempDashboard(
	numDashBoardReptID NUMERIC,numReportID NUMERIC,tintColumn TINYINT,tintRow TINYINT,
	tintReportType TINYINT,tintChartType TINYINT,tintReportCategory tinyint)          
            
if @numUserCntID=0            
begin  
INSERT INTO #tempDashboard          
 select numDashBoardReptID,numReportID,tintColumn,tintRow,tintReportType,tintChartType,tintReportCategory from Dashboard   
 join CustomReport   
 on numCustomReportID=numReportID  
 where  bitGroup=1 and numGroupUserCntID=@numGroupID AND tintReportCategory =1 order by tintColumn,tintRow            
end            
else if exists(select * from Dashboard  join CustomReport   
 on numCustomReportID=numReportID where bitgroup=0 and numGroupUserCntID=@numUserCntID)            
begin  
INSERT INTO #tempDashboard                    
 select numDashBoardReptID,numReportID,tintColumn,tintRow,tintReportType,tintChartType,tintReportCategory from Dashboard  join CustomReport   
 on numCustomReportID=numReportID where  bitGroup=0 and numGroupUserCntID=@numUserCntID AND tintReportCategory =1 order by tintColumn,tintRow            
end            
else            
begin     
 insert into Dashboard(numReportID,numGroupUserCntID,tintRow,tintColumn,tintReportType,vcHeader,vcFooter,tintChartType,bitGroup,tintReportCategory)           
 select numReportID,@numUserCntID,tintRow,tintColumn,tintReportType,vcHeader,vcFooter,tintChartType,0,1 from Dashboard where  bitGroup=1 and numGroupUserCntID=@numGroupID order by tintColumn,tintRow       
 
 INSERT INTO #tempDashboard          
 select numDashBoardReptID,numReportID,tintColumn,tintRow,tintReportType,tintChartType,tintReportCategory from Dashboard  join CustomReport   
 on numCustomReportID=numReportID where  bitGroup=0 and numGroupUserCntID=@numUserCntID AND tintReportCategory =1 order by tintColumn,tintRow                 
END


IF @numUserCntID>0
BEGIN
	INSERT INTO #tempDashboard          
	select numDashBoardReptID,numReportID,tintColumn,tintRow,tintReportType,tintChartType,tintReportCategory from Dashboard  join CommissionReports   
	on numReportID=numComReports AND numGroupUserCntID=numContactID where  numGroupUserCntID=@numUserCntID AND tintReportCategory =2 AND CommissionReports.numDomainID=@numDomainID AND CommissionReports.bitCommContact=0 order by tintColumn,tintRow            
END 

SELECT * FROM #tempDashboard

DROP TABLE #tempDashboard
GO
