/****** Object:  StoredProcedure [dbo].[USP_SaveOperationBudgetDetails]    Script Date: 07/26/2008 16:21:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created By Siva                                
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_saveoperationbudgetdetails')
DROP PROCEDURE usp_saveoperationbudgetdetails
GO
CREATE PROCEDURE [dbo].[USP_SaveOperationBudgetDetails]                                
@numDomainId as numeric(9)=0,                     
@numBudgetId as numeric(9)=0,                    
@strRow as text='',              
@numDepartmentId as numeric(9)=0,                   
@numDivisionId as numeric(9)=0,                                
@numCostCenterId as numeric(9)=0,              
@intFiscalYear as numeric(9)=0,           
@Id as numeric(9)=0,         
@tinyByteMode as tinyint=0,  
@intType as int=0                               
As                                
Begin                                
                          
 Declare @hDoc3 int                                                                                                                                                                                                
 EXEC sp_xml_preparedocument @hDoc3 OUTPUT, @strRow                    
 Declare @Date as datetime                            
 Set @Date = dateadd(year,@intType,getutcdate())         
 Declare @dtFiscalStDate as datetime                                    
 Declare @dtFiscalEndDate as datetime                               
 Set @dtFiscalStDate =dbo.GetFiscalStartDate(dbo.GetFiscalyear(@Date,@numDomainId),@numDomainId)                                 
 Set @dtFiscalEndDate =  dateadd(day,-1,dateadd(year,1,dbo.GetFiscalStartDate(dbo.GetFiscalyear(@Date,@numDomainId),@numDomainId)))                           
 print @dtFiscalStDate                 
 print @dtFiscalEndDate              
--For Deleting Record                 
If @tinyByteMode=0             
 Begin            
  Delete OBD from OperationBudgetDetails OBD inner join OperationBudgetMaster OBM on OBD.numBudgetId=OBM.numBudgetId               
  Where OBD.numBudgetId=@numBudgetId And OBM.numDivisionId=@numDivisionId And OBM.numDepartmentId=@numDepartmentId And OBM.numCostCenterId=@numCostCenterId    
 -- And OBD.intYear=@intFiscalYear And OBM.numDomainId=@numDomainId      
And (((OBD.tintMonth between Month(@dtFiscalStDate) and 12) and OBD.intYear=Year(@dtFiscalStDate)) Or ((OBD.tintMonth between 1 and month(@dtFiscalEndDate) ) and OBD.intYear=Year(@dtFiscalEndDate)) )                 
           
 End            
Else            
 Begin            
   Delete OBD from OperationBudgetMaster OBM  inner join OperationBudgetDetails OBD  on OBM.numBudgetId=OBD.numBudgetId       
  Where OBD.numBudgetId=@numBudgetId And OBD.numChartAcntId=@Id  And OBM.numDomainId=@numDomainId     
  And (((OBD.tintMonth between month(@Date) and 12) and OBD.intYear=year(@Date)) Or ((OBD.tintMonth between 1 and 12) and OBD.intYear=Year(@date)+1))      
 End             
                                          
 Insert Into OperationBudgetDetails(numBudgetId,numChartAcntId,numParentAcntId,txtChartAcntname,tintMonth,intYear,monAmount,vcComments)                                                                                                                   
 Select * from (                                                                                                                                             
 Select * from OPenXml (@hdoc3,'/NewDataSet/Table1 [BudgetDetId=0]',2)                                                                                                                                              
 With(                                         
  numBudgetId numeric(9),                              
  numChartAcntId numeric(9),                           
  numParentAcntId numeric(9),                             
  vcCategoryName varchar(8000),                            
  tintMonth tinyint,                  
  intYear integer,                  
  monAmount DECIMAL(20,5),                              
  Comments varchar(100)                               
  ))X                              
                      
End
GO
