/****** Object:  StoredProcedure [dbo].[USP_GetTerritoryForUsrMst]    Script Date: 07/26/2008 16:18:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getterritoryforusrmst')
DROP PROCEDURE usp_getterritoryforusrmst
GO
CREATE PROCEDURE [dbo].[USP_GetTerritoryForUsrMst]  
@numUserID as numeric(9),  
@numDomainID as numeric(9)  
as  
select numlistitemid,vcdata from userterritories
join listdetails  
on numlistitemid=numterritory  
where numUserID=@numUserID  
and userterritories.numDomainID=@numDomainID
GO
