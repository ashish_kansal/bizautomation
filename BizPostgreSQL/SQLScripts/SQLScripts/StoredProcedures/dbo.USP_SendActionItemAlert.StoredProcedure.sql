/****** Object:  StoredProcedure [dbo].[USP_SendActionItemAlert]    Script Date: 07/26/2008 16:21:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--exec USP_SendActionItemAlert
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_sendactionitemalert')
DROP PROCEDURE usp_sendactionitemalert
GO
CREATE PROCEDURE [dbo].[USP_SendActionItemAlert]
as

declare @vcSubject as varchar(100)
declare @vcBody as varchar(8000)
declare @hours as tinyint
declare @email as varchar(100)
declare @firstname as varchar(50)
declare @lastname as varchar(50)
declare @numCommId as numeric(9)
declare @dtStartTime as datetime

set @numCommId=0

select @numCommId=numCommId,@firstname=vcFirstname,@email=vcEmail,@lastname=vcLastName,
@vcBody=vcDocDesc,@vcSubject=vcSubject,@hours=tintHours from Communication
join GenericDocuments 
on numEmailTemplate=numGenericDocID
join AdditionalContactsInformation A
on A.numContactID=numAssignedBy
where bitAlert=1 and bitClosedFlag=0 and (getutcdate()>= case when bitSendEmailTemp= 0 then dateadd(hour,-tintHours,dtStartTime) 
when bitSendEmailTemp= 1 then dateadd(hour,tintHours,dtStartTime) end)  
	while @numCommId>0
	begin
		
		
		set @vcBody=replace(@vcBody,'##vcFirstName##',@firstname)
		set @vcBody=replace(@vcBody,'##vcLastName##',@lastname)
		set @vcBody=replace(@vcBody,'##hours##',@hours)
		--print @email+@vcSubject+ @vcBody
		exec sendMail_With_CDOMessage  @email,@vcSubject,@vcBody,'','admin@bizautomation.com,'
		update 	Communication set bitAlert=0 where numCommID=@numCommId	

		select @numCommId=numCommId,@firstname=vcFirstname,@lastname=vcLastName,
		@vcBody=vcDocDesc,@vcSubject=vcSubject,@hours=tintHours from Communication
		join GenericDocuments 
		on numEmailTemplate=numGenericDocID
		join AdditionalContactsInformation A
		on A.numContactID=numAssignedBy
		where bitAlert=1 and (getutcdate()>= case when bitSendEmailTemp= 0 then dateadd(hour,-tintHours,dtStartTime) 
		when bitSendEmailTemp= 1 and bitClosedFlag=0 then dateadd(hour,tintHours,dtStartTime) end) and numCommId>@numCommId
		if @@rowcount=0 set @numCommId=0
	end
GO
