/****** Object:  StoredProcedure [dbo].[USP_GetEmailAttachments]    Script Date: 07/26/2008 16:17:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getemailattachments')
DROP PROCEDURE usp_getemailattachments
GO
CREATE PROCEDURE [dbo].[USP_GetEmailAttachments]    
@numEmailHstrID as numeric(9)=0    
as    
    
select numEmailHstrAttID,numEmailHstrID,isnull(vcFileName,'-') as vcFileName from EmailHstrAttchDtls where numEmailHstrID=@numEmailHstrID
GO
