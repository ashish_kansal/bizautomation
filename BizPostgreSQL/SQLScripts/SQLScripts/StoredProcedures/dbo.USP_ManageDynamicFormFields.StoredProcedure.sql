/****** Object:  StoredProcedure [dbo].[USP_ManageDynamicFormFields]    Script Date: 07/26/2008 16:19:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created By: Anoop jayaraj                
                    
                      
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_managedynamicformfields')
DROP PROCEDURE usp_managedynamicformfields
GO
CREATE PROCEDURE [dbo].[USP_ManageDynamicFormFields]                  
 @numFormId numeric (9),                             
 @numAuthGroupID numeric(9),  
 @numDomainId numeric (9),
 @strFomFld as TEXT,
 @numBizDocTemplateID as numeric(9)=0,
 @numSubFormId NUMERIC(18)=0,
 @numAssignTo NUMERIC(18)=0,
 @numDripCampaign NUMERIC(18)=0,
 @bitByPassRoutingRules BIT=0,
 @bitDripCampaign BIT=0
as     

IF(ISNULL(@numSubFormId,0)>0)
BEGIN
	UPDATE 
		LeadsubForms 
    SET
		numAssignTo=ISNULL(@numAssignTo,0),
		numDripCampaign=ISNULL(@numDripCampaign,0),
		bitByPassRoutingRules=@bitByPassRoutingRules,
		bitDripCampaign=@bitDripCampaign
	WHERE
		numDomainId=@numDomainId AND numSubFormId=@numSubFormId
END

 Delete from  DycFormConfigurationDetails      
 where numFormId = @numFormId AND ISNULL(numSubFormId,0)=ISNULL(@numSubFormId,0)

 and numAuthGroupId = @numAuthGroupId    
 and numDomainId = @numDomainId 
 and isnull(numRelCntType,0)=@numBizDocTemplateID

            DECLARE @hDoc1 int                                                        
			EXEC sp_xml_preparedocument @hDoc1 OUTPUT, @strFomFld                                                        
			insert into DycFormConfigurationDetails                                                        
			(numFormId,numSubFormId, numFieldId,           
			   intColumnNum, intRowNum, boolAOIField, numAuthGroupID, numDomainId,numRelCntType,bitCustom)                                                        
			select @numFormId,ISNULL(@numSubFormId,0),replace(replace(X.numFormFieldId,'R',''),'C','') ,                                                        
			X.intColumnNum,                                
			X.intRowNum,
			X.boolAOIField,@numAuthGroupID,@numDomainId,@numBizDocTemplateID,Case when CHARINDEX('C',X.numFormFieldId)>0 then 1 else 0 end from(SELECT * FROM OPENXML (@hDoc1,'/FormFields/FormField',2)                                                        
			WITH (numFormFieldId varchar(20),                                                        
			vcFieldType char(1),                                                        
			intColumnNum int,                                
			intRowNum int,
			boolAOIField bit))X      
			EXEC sp_xml_removedocument @hDoc1
GO
