/****** Object:  StoredProcedure [dbo].[USP_GetKitParentsForChildItems]    Script Date: 07/26/2008 16:17:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--create by anoop jayaraj
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getkitparentsforchilditems')
DROP PROCEDURE usp_getkitparentsforchilditems
GO
CREATE PROCEDURE [dbo].[USP_GetKitParentsForChildItems]
@numChildItemID as numeric(9)
as


select numItemKitID,numChildItemID,ItemDetails.numQtyItemsReq,vcItemName as KitParent
from ItemDetails
join Item
on numItemCode=numItemKitID
where numChildItemID=@numChildItemID
GO
