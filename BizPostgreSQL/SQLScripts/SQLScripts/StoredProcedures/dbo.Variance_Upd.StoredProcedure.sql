/****** Object:  StoredProcedure [dbo].[Variance_Upd]    Script Date: 07/26/2008 16:22:02 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
/*
**  Updates a Variance row when an end users modifies an occurrence that
**  already had a variance.
*/
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='variance_upd')
DROP PROCEDURE variance_upd
GO
CREATE PROCEDURE [dbo].[Variance_Upd]
	@AllDayEvent			bit,
	@ActivityDescription		ntext,
	@Duration			integer,
	@Location			nvarchar(64),
	@StartDateTimeUtc		datetime,
	@Subject			nvarchar(255),
	@EnableReminder			bit,
	@ReminderInterval		integer,
	@ShowTimeAs			integer,
	@Importance			integer,
	@Status				integer,
	@OriginalStartDateTimeUtc	datetime,
	@RecurrenceKey			integer,
	@DataKey			integer,
@Color tinyint=0
AS
BEGIN
	UPDATE
		[Activity] 
	SET 
		[Activity].[AllDayEvent] = @AllDayEvent,
		[Activity].[ActivityDescription] = @ActivityDescription,
		[Activity].[Duration] = @Duration, 
		[Activity].[Location] = @Location,
		[Activity].[StartDateTimeUtc] = @StartDateTimeUtc,
		[Activity].[Subject] = @Subject, 
		[Activity].[EnableReminder] = @EnableReminder,
		[Activity].[ReminderInterval] = @ReminderInterval,
		[Activity].[ShowTimeAs] = @ShowTimeAs,
		[Activity].[Importance] = @Importance,
		[Activity].[Status] = @Status,
		[Activity].[RecurrenceID] = @RecurrenceKey,
		[Activity].[OriginalStartDateTimeUtc] = @OriginalStartDateTimeUtc,
Color=@Color
	WHERE 
		( [Activity].[ActivityID] = @DataKey );
END
GO
