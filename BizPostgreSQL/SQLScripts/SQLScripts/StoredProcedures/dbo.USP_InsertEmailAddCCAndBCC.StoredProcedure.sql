/****** Object:  StoredProcedure [dbo].[USP_InsertEmailAddCCAndBCC]    Script Date: 07/26/2008 16:19:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj    
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'usp_insertemailaddccandbcc' ) 
    DROP PROCEDURE usp_insertemailaddccandbcc
GO
CREATE PROCEDURE [dbo].[USP_InsertEmailAddCCAndBCC]
    @vcEmailAdd AS VARCHAR(2000) = '',
    @Type AS TINYINT = 0,
    @numEmailHSTRID AS NUMERIC(9),
    @EmailName AS VARCHAR(1000) = ''
AS 
    DECLARE @startPos AS INTEGER    
    DECLARE @EndPos AS INTEGER    
    DECLARE @EndPos1 AS INTEGER    
    DECLARE @Email AS VARCHAR(100)    
    DECLARE @Name AS VARCHAR(100)    
    DECLARE @EmailAdd AS VARCHAR(100)    
    DECLARE @numEmailId AS NUMERIC  
  
    SET @numEmailId = 0  
  
    SELECT  @numEmailId = ISNULL(numEmailId, 0)
    FROM    emailMaster
    WHERE   vcEmailId LIKE @vcEmailAdd  
  
    IF @vcEmailAdd <> '' 
        BEGIN    
            WHILE CHARINDEX(',', @vcEmailAdd) > 0    
                BEGIN    
                    SET @Email = LTRIM(RTRIM(SUBSTRING(@vcEmailAdd, 1,
                                                       CHARINDEX(',', @vcEmailAdd)
                                                       - 1)))
    
                    SET @startPos = CHARINDEX('<', @Email)    
                    SET @EndPos = CHARINDEX('>', @Email)    
                    IF @startPos > 0 -- IF email address has ' <> ' in it
                        BEGIN  
  
                            SET @EmailAdd = LTRIM(RTRIM(SUBSTRING(@Email, @startPos + 1, @EndPos - @startPos - 1)))
                            SET @startPos = CHARINDEX('"', @Email)    
                            SET @EndPos1 = CHARINDEX('"',
                                                     SUBSTRING(@Email, @startPos + 1, LEN(@Email)))    
     
                            IF @EmailName <> '' 
                                BEGIN  
                                    SET @Name = @EmailName  
                                END  
                            ELSE 
                                BEGIN  
                                    SET @Name = SUBSTRING(@Email,
                                                          @startPos + 1,
                                                          @EndPos1 - 1)    
                                END  
   
                            IF LEN(LTRIM(RTRIM(@EmailAdd))) > 0 
                            --MStart Pinkal Patel Date:04-oct-2011 Check Email Id Exist in Email Master Table.
                                SELECT  @numEmailId = ISNULL(numEmailId, 0)
                                FROM    emailMaster
                                WHERE   vcEmailId LIKE @Email
							--MEnd Pinkal Patel Date:04-oct-2011
                            IF @numEmailId = 0 
                                BEGIN  
								
                                    INSERT  INTO EmailMaster ( vcEmailId, vcName )
                                    VALUES  ( @EmailAdd, @Name )  
                                    SET @numEmailId = SCOPE_IDENTITY()    
    
                                    INSERT  INTO EmailHStrToBCCAndCC
                                            (
                                              numEmailHstrID,
                                              vcName,
                                              tintType,
                                              numEmailId 
                                                
                                            )
                                    VALUES  (
                                              @numEmailHSTRID,
                                              @Name,
                                              @Type,
                                              @numEmailId
                                                
                                            )  
                                    SET @numEmailId = 0
                                END  
                            ELSE 
                                BEGIN  
  
                                    INSERT  INTO EmailHStrToBCCAndCC
                                            (
                                              numEmailHstrID,
                                              vcName,
                                              tintType,
                                              numEmailId 
                                                
                                            )
                                    VALUES  (
                                              @numEmailHSTRID,
                                              @Name,
                                              @Type,
                                              @numEmailId
                                                
                                            )   
  
                                END  
      
       
                        END     
                    ELSE 
                        BEGIN  -- When email address doesn't have ' <>  '
                            SET @EmailAdd = @Email    
                            SET @Name = @EmailName    
                        
                            IF LEN(LTRIM(RTRIM(@EmailAdd))) > 0 
                               --MStart Pinkal Patel Date:04-oct-2011 Check Email Id Exist in Email Master Table.
                                SELECT  @numEmailId = ISNULL(numEmailId, 0)
                                FROM    emailMaster
                                WHERE   vcEmailId LIKE @EmailAdd
							--MEnd Pinkal Patel Date:04-oct-2011
                            IF @numEmailId = 0 
                                BEGIN  
                                    INSERT  INTO EmailMaster ( vcEmailId, vcName )
                                    VALUES  ( @EmailAdd, @Name )  
                                    SET @numEmailId = SCOPE_IDENTITY()   
    
                                    INSERT  INTO EmailHStrToBCCAndCC
                                            (
                                              numEmailHstrID,
                                              vcName,
                                              tintType,
                                              numEmailId 
                                                
                                            )
                                    VALUES  (
                                              @numEmailHSTRID,
                                              @Name,
                                              @Type,
                                              @numEmailId
                                                
                                            )  
                                    SET @numEmailId = 0
                                END  
                            ELSE 
                                BEGIN  
  
                                    INSERT  INTO EmailHStrToBCCAndCC
                                            (
                                              numEmailHstrID,
                                              vcName,
                                              tintType,
                                              numEmailId 
                                                
                                            )
                                    VALUES  (
                                              @numEmailHSTRID,
                                              @Name,
                                              @Type,
                                              @numEmailId
                                                
                                            )   
  
                                END  
--   Insert into EmailHStrToBCCAndCC (numEmailHstrID,vcName,vcEmail,tintType)     
--   values(@numEmailHSTRID,@Name,@EmailAdd,@Type)    
                        END    
      
                    SET @vcEmailAdd = LTRIM(RTRIM(SUBSTRING(@vcEmailAdd,
                                                            CHARINDEX(',', @vcEmailAdd)
                                                            + 1,
                                                            LEN(@vcEmailAdd))))
                END     
            SET @startPos = CHARINDEX('<', @vcEmailAdd)    
            SET @EndPos = CHARINDEX('>', @vcEmailAdd)    
            IF @startPos > 0 
                BEGIN    
                    SET @EmailAdd = SUBSTRING(@vcEmailAdd, @startPos + 1,
                                              @EndPos - @startPos - 1)    
                    SET @startPos = CHARINDEX('"', @vcEmailAdd)    
                    SET @EndPos1 = CHARINDEX('"',
                                             SUBSTRING(@vcEmailAdd,
                                                       @startPos + 1,
                                                       LEN(@vcEmailAdd)))    
                    IF @EmailName <> '' 
                        BEGIN  
                            SET @Name = @EmailName  
                        END  
                    ELSE 
                        BEGIN  
                            SET @Name = SUBSTRING(@Email, @startPos + 1,
                                                  ISNULL(NULLIF(@EndPos1 - 1, -1),
                                                         0))    
                        END  
   
                    IF LEN(LTRIM(RTRIM(@EmailAdd))) > 0 
                        IF @numEmailId = 0 
                            BEGIN  
                                INSERT  INTO EmailMaster ( vcEmailId, vcName )
                                VALUES  ( @EmailAdd, @Name )  
                                SET @numEmailId = SCOPE_IDENTITY()   
    
                                INSERT  INTO EmailHStrToBCCAndCC
                                        (
                                          numEmailHstrID,
                                          vcName,
                                          tintType,
                                          numEmailId 
                                        )
                                VALUES  (
                                          @numEmailHSTRID,
                                          @Name,
                                          @Type,
                                          @numEmailId
                                        )  
                                SET @numEmailId = 0
                            END  
                        ELSE 
                            BEGIN  
  
                                INSERT  INTO EmailHStrToBCCAndCC
                                        (
                                          numEmailHstrID,
                                          vcName,
                                          tintType,
                                          numEmailId 
                                        )
                                VALUES  (
                                          @numEmailHSTRID,
                                          @Name,
                                          @Type,
                                          @numEmailId
                                        )   
  
                            END  
--  Insert into EmailHStrToBCCAndCC (numEmailHstrID,vcName,vcEmail,tintType)     
--  values(@numEmailHSTRID,@Name,@EmailAdd,@Type)     
                END    
            ELSE 
                BEGIN    
                    SET @EmailAdd = @vcEmailAdd    
                    SET @Name = @EmailName    
--                PRINT @EmailAdd
--                PRINT @numEmailId
                
                    IF LEN(LTRIM(RTRIM(@EmailAdd))) > 0 
                        IF @numEmailId = 0 
                            BEGIN  
    
                                INSERT  INTO EmailMaster ( vcEmailId, vcName )
                                VALUES  ( @EmailAdd, @Name )  
                                SET @numEmailId = SCOPE_IDENTITY()   
    
                                INSERT  INTO EmailHStrToBCCAndCC
                                        (
                                          numEmailHstrID,
                                          vcName,
                                          tintType,
                                          numEmailId 
                                        )
                                VALUES  (
                                          @numEmailHSTRID,
                                          @Name,
                                          @Type,
                                          @numEmailId
                                        )  
                                SET @numEmailId = 0
                            END  
                        ELSE 
                            BEGIN  
  
                                INSERT  INTO EmailHStrToBCCAndCC
                                        (
                                          numEmailHstrID,
                                          vcName,
                                          tintType,
                                          numEmailId 
                                        )
                                VALUES  (
                                          @numEmailHSTRID,
                                          @Name,
                                          @Type,
                                          @numEmailId
                                        )   
  
                            END  --  Insert into EmailHStrToBCCAndCC (numEmailHstrID,vcName,vcEmail,tintType)     
--  values(@numEmailHSTRID,@Name,@EmailAdd,@Type)    
                END     
        END
GO
