/****** Object:  StoredProcedure [dbo].[USP_SaveUserStatus]    Script Date: 07/26/2008 16:21:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_saveuserstatus')
DROP PROCEDURE usp_saveuserstatus
GO
CREATE PROCEDURE [dbo].[USP_SaveUserStatus]    
(    
 @numUserID as numeric(9),    
 @Active as tinyint,    
 @noOfLicences as numeric(9)    
    
)    
as    
if  @Active=1   
begin   
 if @noOfLicences > (select count(*) from UserMaster where bitActivateFlag=1)    
 begin    
  update UserMaster set     
    bitActivateFlag=@Active    
     where  numUserID=@numUserID    
     
 end    
end  
else  
begin  
 update UserMaster set     
    bitActivateFlag=@Active    
     where  numUserID=@numUserID    
  
end  
select count(*) from UserMaster where bitActivateFlag=1
GO
