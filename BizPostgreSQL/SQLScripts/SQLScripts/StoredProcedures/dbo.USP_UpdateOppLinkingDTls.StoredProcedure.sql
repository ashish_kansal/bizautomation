GO
/****** Object:  StoredProcedure [dbo].[USP_UpdateOppLinkingDTls]    Script Date: 05/07/2009 22:12:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_updateopplinkingdtls')
DROP PROCEDURE usp_updateopplinkingdtls
GO
CREATE PROCEDURE [dbo].[USP_UpdateOppLinkingDTls]        
@numOppID as numeric(9)=0,            
@tintOppType as tinyint,        
@tintBillToType as tinyint,        
@tintShipToType as tinyint,        
@vcBillStreet as varchar(50),                  
@vcBillCity as varchar(50),                  
@numBillState as numeric(9),                  
@vcBillPostCode as varchar(15),                  
@numBillCountry as numeric(9),                          
@vcShipStreet as varchar(50),                  
@vcShipCity as varchar(50),                  
@numShipState as numeric(9),                  
@vcShipPostCode as varchar(15),                  
@numShipCountry as numeric(9),    
@vcBillCompanyName as varchar(100),    
@vcShipCompanyName as varchar(100),
@numParentOppID as numeric(9),
@numParentOppBizDocID AS NUMERIC(9),
@numBillToAddressID AS NUMERIC(18,0) = 0,
@numShipToAddressID AS NUMERIC(18,0) = 0,
@numBillingContact NUMERIC(18,0) = 0,
@bitAltBillingContact BIT = 0,
@vcAltBillingContact VARCHAR(200) = '',
@numShippingContact NUMERIC(18,0) = 0,
@bitAltShippingContact BIT = 0,
@vcAltShippingContact VARCHAR(200) = ''
AS        
BEGIN     

	IF ISNULL(@numBillToAddressID,0) > 0
	BEGIN
		DECLARE @numBillCompany NUMERIC(18,0)

		SELECT
			@numBillCompany = (CASE WHEN tintAddressOf=2 THEN AddressDetails.numRecordID ELSE 0 END),
			@vcBillCompanyName = (CASE WHEN tintAddressOf=2 THEN ISNULL((SELECT vcCompanyName FROM DivisionMaster INNER JOIN CompanyInfo ON DivisionMaster.numCompanyID=CompanyInfo.numCompanyId WHERE numDivisionID=AddressDetails.numRecordID),'') ELSE '' END),
			@vcBillStreet = vcStreet,
			@vcBillCity = vcCity,
			@numBillState = numState,
			@vcBillPostCode = vcPostalCode,
			@numBillCountry = numCountry 
		FROM
			AddressDetails
		WHERE
			numAddressID=@numBillToAddressID

		UPDATE 
			OpportunityMaster 
		SET 
			tintBillToType=2
		WHERE 
			numOppID=@numOppID

		IF (SELECT COUNT(*) FROM OpportunityAddress WHERE [numOppID]=@numOppID) = 0
		BEGIN
			INSERT INTO OpportunityAddress (numOppID) VALUES (@numOppID)      
		END

		UPDATE
			OpportunityAddress
		SET 
			numBillCompanyID=@numBillCompany,
			vcBillCompanyName = @vcBillCompanyName,
			vcBillStreet = @vcBillStreet,
			vcBillCity = @vcBillCity,
			numBillState = @numBillState,
			vcBillPostCode = @vcBillPostCode,
			numBillCountry = @numBillCountry,
			numBillingContact = @numBillingContact,
			bitAltBillingContact=@bitAltBillingContact,
			vcAltBillingContact=@vcAltBillingContact
		WHERE 
			numOppID = @numOppID
	END
	ELSE
	BEGIN
		UPDATE 
			OpportunityMaster 
		SET 
			tintBillToType=@tintBillToType,
			numBillToAddressID = @numBillToAddressID
		WHERE 
			numOppID=@numOppID
	END

	IF ISNULL(@numShipToAddressID,0) > 0
	BEGIN
		DECLARE @numShipCompany NUMERIC(18,0)

		SELECT
			@numShipCompany = (CASE WHEN tintAddressOf=2 THEN AddressDetails.numRecordID ELSE 0 END),
			@vcShipCompanyName = (CASE WHEN tintAddressOf=2 THEN ISNULL((SELECT vcCompanyName FROM DivisionMaster INNER JOIN CompanyInfo ON DivisionMaster.numCompanyID=CompanyInfo.numCompanyId WHERE numDivisionID=AddressDetails.numRecordID),'') ELSE '' END),
			@vcShipStreet = vcStreet,
			@vcShipCity = vcCity,
			@numShipState = numState,
			@vcShipPostCode = vcPostalCode,
			@numShipCountry = numCountry 
		FROM
			AddressDetails
		WHERE
			numAddressID=@numShipToAddressID

		UPDATE 
			OpportunityMaster 
		SET
			tintShipToType=2
		WHERE 
			numOppID=@numOppID

		IF (SELECT COUNT(*) FROM OpportunityAddress WHERE [numOppID]=@numOppID) = 0
		BEGIN
			INSERT INTO OpportunityAddress (numOppID) VALUES (@numOppID)      
		END

		UPDATE 
			OpportunityAddress
		SET 
			numShipCompanyID=@numShipCompany,
			vcShipCompanyName = @vcShipCompanyName,
			vcShipStreet = @vcShipStreet,
			vcShipCity = @vcShipCity,
			numShipState = @numShipState,
			vcShipPostCode = @vcShipPostCode,
			numShipCountry = @numShipCountry,
			numShippingContact = @numShippingContact,
			bitAltShippingContact=@bitAltShippingContact,
			vcAltShippingContact=@vcAltShippingContact
		WHERE 
			numOppID = @numOppID
	END
	ELSE
	BEGIN
		UPDATE 
			OpportunityMaster 
		SET 
			tintShipToType=@tintShipToType,
			numShipToAddressID = @numShipToAddressID
		WHERE 
			numOppID=@numOppID        
	END           
	        
	IF (@tintBillToType = 2 AND ISNULL(@numBillToAddressID,0) = 0) OR (@tintShipToType = 2  AND ISNULL(@numShipToAddressID,0) = 0)
	BEGIN
		IF (SELECT COUNT(*) FROM OpportunityAddress WHERE [numOppID]=@numOppID) = 0
		BEGIN
			INSERT  INTO OpportunityAddress ( numOppID )
			VALUES  ( @numOppID )      
		END
	END

	IF @tintBillToType = 2 AND ISNULL(@numBillToAddressID,0) = 0
	BEGIN
		UPDATE  OpportunityAddress
		SET     vcBillCompanyName = @vcBillCompanyName,
				vcBillStreet = @vcBillStreet,
				vcBillCity = @vcBillCity,
				numBillState = @numBillState,
				vcBillPostCode = @vcBillPostCode,
				numBillCountry = @numBillCountry,
				bitAltBillingContact=@bitAltBillingContact,
				vcAltBillingContact=@vcAltBillingContact
		WHERE   numOppID = @numOppID
	
	END
    
	IF @tintShipToType = 2 AND ISNULL(@numShipToAddressID,0) = 0
	BEGIN
	PRINT 'hi'
		UPDATE  OpportunityAddress
		SET     vcShipCompanyName = @vcShipCompanyName,
				vcShipStreet = @vcShipStreet,
				vcShipCity = @vcShipCity,
				numShipState = @numShipState,
				vcShipPostCode = @vcShipPostCode,
				numShipCountry = @numShipCountry,
				bitAltShippingContact=@bitAltShippingContact,
				vcAltShippingContact=@vcAltShippingContact
		WHERE   numOppID = @numOppID

	END

	DECLARE @numDomainID NUMERIC(18,0)
	DECLARE @numDivisionID NUMERIC(18,0)

	SELECT 
		@numDomainID=numDomainID
		,@numDivisionID=numDivisionID
	FROM 
		OpportunityMaster 
	WHERE 
		numOppID=@numOppId

	DELETE FROM OpportunityMasterTaxItems WHERE numOppId=@numOppID

	--INSERT TAX FOR DIVISION   
	IF @tintOppType=1 OR (@tintOppType=2 and (select isnull(bitPurchaseTaxCredit,0) from Domain where numDomainId=@numDomainId)=1)
	BEGIN
		INSERT INTO dbo.OpportunityMasterTaxItems 
		(
			numOppId,
			numTaxItemID,
			fltPercentage,
			tintTaxType,
			numTaxID
		) 
		SELECT 
			@numOppID,
			TI.numTaxItemID,
			TEMPTax.decTaxValue,
			TEMPTax.tintTaxType,
			0
		FROM 
			TaxItems TI 
		JOIN 
			DivisionTaxTypes DTT 
		ON 
			TI.numTaxItemID = DTT.numTaxItemID
		CROSS APPLY
		(
			SELECT
				decTaxValue,
				tintTaxType
			FROM
				dbo.fn_CalItemTaxAmt(@numDomainID,@numDivisionID,0,TI.numTaxItemID,@numOppId,0,NULL)
		) AS TEMPTax
		WHERE 
			DTT.numDivisionID=@numDivisionId 
			AND DTT.bitApplicable=1
		UNION 
		SELECT 
			@numOppID,
			0,
			TEMPTax.decTaxValue,
			TEMPTax.tintTaxType,
			0
		FROM 
			dbo.DivisionMaster 
		CROSS APPLY
		(
			SELECT
				decTaxValue,
				tintTaxType
			FROM
				dbo.fn_CalItemTaxAmt(@numDomainID,@numDivisionID,0,0,@numOppId,1,NULL)
		) AS TEMPTax
		WHERE 
			bitNoTax=0 
			AND numDivisionID=@numDivisionID	
		UNION 
		SELECT
			@numOppId
			,1
			,decTaxValue
			,tintTaxType
			,numTaxID
		FROM
			dbo.fn_CalItemTaxAmt(@numDomainID,@numDivisionID,0,1,@numOppId,1,NULL)		
	END	


	if @numParentOppID>0 
	BEGIN
		IF @numParentOppBizDocID = 0 
			SET @numParentOppBizDocID = NULL       

		insert into OpportunityLinking (numParentOppID,numChildOppID,numParentOppBizDocID) values(@numParentOppID,@numOppID,@numParentOppBizDocID)



		If (@tintOppType = 2 OR @tintOppType = 1)
		BEGIN

			UPDATE 
				OpportunityMaster 
			SET 
				vcCustomerPO#=ISNULL((SELECT vcOppRefOrderNo FROM OpportunityMaster WHERE numOppId=@numParentOppID),'')
			WHERE 
				numOppID=@numOppID 


			IF EXISTS (SELECT numParentChildFieldID FROM ParentChildCustomFieldMap WHERE numDomainID=@numDomainID AND numParentFieldID=100 AND tintParentModule=2 AND numChildFieldID=100 AND (tintChildModule=6 OR tintChildModule = 2))
			BEGIN
				UPDATE 
					OpportunityMaster 
				SET 
					numAssignedTo=ISNULL((SELECT numAssignedTo FROM OpportunityMaster WHERE numOppId=@numParentOppID),0)
				WHERE 
					numOppID=@numOppID 
			END

			IF EXISTS (SELECT numParentChildFieldID FROM ParentChildCustomFieldMap WHERE numDomainID=@numDomainID AND numParentFieldID=122 AND tintParentModule=2 AND numChildFieldID=122 AND (tintChildModule=6 OR tintChildModule = 2))
			BEGIN
				UPDATE 
					OpportunityMaster 
				SET 
					txtComments=ISNULL((SELECT txtComments FROM OpportunityMaster WHERE numOppId=@numParentOppID),'')
				WHERE 
					numOppID=@numOppID 
			END

			IF EXISTS (SELECT numParentChildFieldID FROM ParentChildCustomFieldMap WHERE numDomainID=@numDomainID AND numParentFieldID=771 AND tintParentModule=2 AND numChildFieldID=771 AND (tintChildModule=6 OR tintChildModule = 2))
			BEGIN
				UPDATE 
					OpportunityMaster 
				SET 
					intUsedShippingCompany=ISNULL((SELECT intUsedShippingCompany FROM OpportunityMaster WHERE numOppId=@numParentOppID),0)
					,numShipmentMethod=ISNULL((SELECT numShipmentMethod FROM OpportunityMaster WHERE numOppId=@numParentOppID),0)
					,numShippingService=ISNULL((SELECT numShippingService FROM OpportunityMaster WHERE numOppId=@numParentOppID),0)
				WHERE 
					numOppID=@numOppID 
			END

			If (@tintOppType = 2)
			BEGIN
				EXEC dbo.USP_AddParentChildCustomFieldMap
							@numDomainID = @numDOmainID, --  numeric(18, 0)
							@numRecordID = @numOppID, --  numeric(18, 0)
							@numParentRecId = @numParentOppID, --  numeric(18, 0)
							@tintPageID = 6 --  tinyin

				EXEC dbo.USP_AddParentChildCustomFieldMap
							@numDomainID = @numDOmainID, --  numeric(18, 0)
							@numRecordID = @numOppID, --  numeric(18, 0)
							@numParentRecId = @numDivisionID, --  numeric(18, 0)
							@tintPageID = 6 --  tinyin
			END
			ELSE IF (@tintOppType = 1)
			BEGIN
				EXEC dbo.USP_AddParentChildCustomFieldMap
							@numDomainID = @numDOmainID, --  numeric(18, 0)
							@numRecordID = @numOppID, --  numeric(18, 0)
							@numParentRecId = @numParentOppID, --  numeric(18, 0)
							@tintPageID = 2 --  tinyin

				EXEC dbo.USP_AddParentChildCustomFieldMap
							@numDomainID = @numDOmainID, --  numeric(18, 0)
							@numRecordID = @numOppID, --  numeric(18, 0)
							@numParentRecId = @numDivisionID, --  numeric(18, 0)
							@tintPageID = 2 --  tinyin
			END
		END
	end
END
GO


