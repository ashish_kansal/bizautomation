GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetECommerceDetails')
DROP PROCEDURE USP_GetECommerceDetails
GO
CREATE PROCEDURE [dbo].[USP_GetECommerceDetails]      
	@numDomainID AS NUMERIC(18,0)=0,
	@numSiteID AS INTEGER = 0  
as
BEGIN
	IF ISNULL(@numSiteID,0) = 0
	BEGIN
		SELECT @numSiteID=numDefaultSiteID FROM Domain WHERE numDomainId=@numDomainID
	END
	              
	SELECT                               
		ISNULL(vcPaymentGateWay,'0') as vcPaymentGateWay,                
		ISNULL(vcPGWManagerUId,'')as vcPGWManagerUId ,                
		ISNULL(vcPGWManagerPassword,'') as vcPGWManagerPassword,              
		ISNULL(bitShowInStock,0) as bitShowInStock,              
		ISNULL(bitShowQOnHand,0) as bitShowQOnHand, 
		ISNULL([numDefaultWareHouseID],0) numDefaultWareHouseID,
		ISNULL(bitCheckCreditStatus,0) as bitCheckCreditStatus,
		ISNULL([numRelationshipId],0) numRelationshipId,
		ISNULL([numProfileId],0) numProfileId,
		ISNULL([bitHidePriceBeforeLogin],0) AS bitHidePriceBeforeLogin,
		ISNULL([numBizDocForCreditCard],0) AS numBizDocForCreditCard,
		ISNULL([numBizDocForCreditTerm],0) AS numBizDocForCreditTerm,
		ISNULL(bitAuthOnlyCreditCard,0) AS bitAuthOnlyCreditCard,
		ISNULL(numAuthrizedOrderStatus,0) AS numAuthrizedOrderStatus,
		ISNULL(bitSendEmail,1) AS bitSendEmail,
		ISNULL(vcGoogleMerchantID , '') AS vcGoogleMerchantID,
		ISNULL(vcGoogleMerchantKey,'') AS vcGoogleMerchantKey,
		ISNULL(IsSandbox,0) AS IsSandbox,
		ISNULL(numSiteID,0) AS numSiteID,
		ISNULL(vcPaypalUserName,'') AS vcPaypalUserName,
		ISNULL(vcPaypalPassword,'') AS vcPaypalPassword,
		ISNULL(vcPaypalSignature,'') AS vcPaypalSignature,
		ISNULL(IsPaypalSandbox,0) AS IsPaypalSandbox,
		ISNULL(bitSkipStep2,0) AS [bitSkipStep2],
		ISNULL(bitDisplayCategory,0) AS [bitDisplayCategory],
		ISNULL(bitShowPriceUsingPriceLevel,0) AS [bitShowPriceUsingPriceLevel],
		ISNULL(bitEnableSecSorting,0) AS [bitEnableSecSorting],
		ISNULL(bitSortPriceMode,0) AS [bitSortPriceMode],
		ISNULL(numPageSize,0) AS [numPageSize],
		ISNULL(numPageVariant,0) AS [numPageVariant],
		ISNULL(bitAutoSelectWarehouse,0) AS bitAutoSelectWarehouse,
		ISNULL([bitPreSellUp],0) AS [bitPreSellUp],
		ISNULL([bitPostSellUp],0) AS [bitPostSellUp],
		ISNULL([dcPostSellDiscount],0) AS [dcPostSellDiscount],
		ISNULL(numDefaultClass,0) numDefaultClass,
		vcPreSellUp
		,vcPostSellUp
		,ISNULL(vcRedirectThankYouUrl,'') AS vcRedirectThankYouUrl
		,ISNULL(tintPreLoginProceLevel,0) tintPreLoginProceLevel
		,ISNULL(bitHideAddtoCart,0) bitHideAddtoCart
		,ISNULL(bitShowPromoDetailsLink,0) bitShowPromoDetailsLink
		,ISNULL(tintWarehouseAvailability,1) tintWarehouseAvailability
		,ISNULL(bitDisplayQtyAvailable,0) bitDisplayQtyAvailable
		,ISNULL(bitElasticSearch,0) bitElasticSearch
	FROM 
		eCommerceDTL 
	WHERE 
		numDomainID=@numDomainID
		AND ([numSiteId] = @numSiteID OR ISNULL(@numSiteID,0) = 0)
END
GO