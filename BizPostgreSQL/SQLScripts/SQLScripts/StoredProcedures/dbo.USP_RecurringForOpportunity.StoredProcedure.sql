/****** Object:  StoredProcedure [dbo].[USP_RecurringForOpportunity]    Script Date: 03/25/2009 15:13:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_recurringforopportunity')
DROP PROCEDURE usp_recurringforopportunity
GO
CREATE PROCEDURE [dbo].[USP_RecurringForOpportunity]    
(@numOppId as numeric(9)=0,    
@numDomainId as numeric(9)=0,    
@numNoTransactionsRecurring as numeric(9)=0)    
As    
Begin    
        return
-- Declare @tintOppType as tinyint    
--                                
-- Declare @numMaxOppId as numeric(9)                                
-- Declare @LastRecurringDate as datetime                
-- Declare @numNoTransactions as numeric(9)                           
-- Declare @Day as tinyint                              
-- Declare @Month as  tinyint                              
-- Declare @Year as integer                              
-- Declare @MonthNextDate as tinyint                             
-- Declare @intOppTcode as numeric(9)      
-- Declare @vcCompanyName as varchar(100)                             
-- Declare @vcBizDocID as varchar(100)                      
-- Declare @numCurrentBizDocId as numeric(9)     
-- Declare @numOppBizDocsId as numeric(9)    
-- Declare @monamount as DECIMAL(20,5)    
-- Declare @numBizDocsPaymentDetId as numeric(9)          
-- Declare @numJournalId as numeric(9)    
-- Declare @numCurrentJournalId as numeric(9)        
-- Declare @numBizDocId as numeric(9)    
-- Declare @numMaxOppBizDocsId as numeric(9)    
-- Declare @currentOppId as numeric(9)    
-- Declare @AuthoritativeBizDocsId as numeric(9)         
-- Declare @bitEndTransactionType as bit          
-- --To Insert into OpportunityMaster    
--    
-- Select @vcCompanyName=CI.vcCompanyName From CompanyInfo CI              
-- join Divisionmaster DM on  CI.numCompanyid=DM.numCompanyid Where  DM.numDivisionID =(Select numDivisionId From OpportunityMaster Where   numOppId=@numOppId )                                                           
-- Select @intOppTcode=max(numOppId)from OpportunityMaster                                                                  
-- Set @intOppTcode =isnull(@intOppTcode,0) + 1     
-- Set @vcCompanyName=@vcCompanyName+'-'+Convert(varchar(100),DATENAME(month,getutcdate()))+ '-'+convert(varchar(4),year(getutcdate()))  +'-A'+ convert(varchar(10),@intOppTcode)       
-- insert into OpportunityMaster(tintOppType,numContactId,numDivisionId,bintAccountClosingDate,txtComments,bitPublicFlag,tintSource,vcPOppName,intPEstimatedCloseDate,numPClosingPercent,numCampainID,monPAmount,lngPConclAnalysis,    
-- tintActive,numCreatedBy,bintCreatedDate,numDomainId,    
-- numRecOwner,tintOppStatus,tintshipped,numSalesOrPurType,numAssignedTo,numAssignedBy,numUserClosed,tintBillToType,tintShipToType)    
-- Select  tintOppType,numContactId,numDivisionId,getutcdate(),txtComments,bitPublicFlag,tintSource,@vcCompanyName,getutcdate(),numPClosingPercent,numCampainID,monPAmount,lngPConclAnalysis,    
-- tintActive,numCreatedBy,getutcdate(),numDomainId,    
-- numRecOwner,tintOppStatus,0,numSalesOrPurType,numAssignedTo,numAssignedBy,numUserClosed,tintBillToType,tintShipToType From OpportunityMaster Where numOppId=@numOppId        
--    -- Changed tintshipped in  above line to 0
-- Set @currentOppId=@@Identity    
--     
-- --To Insert into OpportunityAddress Table    
-- Insert into OpportunityAddress(numOppID,vcBillStreet,vcBillCity,numBillState,vcBillPostCode,numBillCountry,vcShipStreet,vcShipCity,numShipState,vcShipPostCode)    
-- Select @currentOppId,vcBillStreet,vcBillCity,numBillState,vcBillPostCode,numBillCountry,vcShipStreet,vcShipCity,numShipState,vcShipPostCode From OpportunityAddress Where numOppId=@numOppId    
--     
--    
-- -- To insert into OpportunityBizDocs Tables    
-- Select @vcBizDocID=vcpOppName from OpportunityMaster where numOppId=@currentOppId    
-- Select @tintOppType=tintOppType from OpportunityMaster where numOppId=@numOppId    
-- Set @vcBizDocID= @vcBizDocID + '-BD-'       
-- If @tintOppType=1       
--  Select @AuthoritativeBizDocsId=isnull(numAuthoritativeSales,0) From AuthoritativeBizDocs Where numDomainId=@numDomainId        
-- Else      
--  Select @AuthoritativeBizDocsId=isnull(numAuthoritativePurchase,0) From AuthoritativeBizDocs Where numDomainId=@numDomainId      
--     
-- Select top 1 @numOppBizDocsId=numOppBizDocsId,@numBizDocId=numBizDocId From OpportunityBizDocs Where numOppId=@numOppId    
-- Select @numMaxOppBizDocsId=max(numOppBizDocsId) From OpportunityBizDocs Where numOppId=@numOppId    
-- print '@numMaxOppBizDocsId====='+Convert(varchar(100),@numMaxOppBizDocsId)    
-- While @numOppBizDocsId>0    
--  Begin     
--declare @bitRecurringZeroAmt as bit 
--select @bitRecurringZeroAmt=bitRecurringZeroAmt from OpportunityMaster Opp LEFT OUTER JOIN [OpportunityRecurring] OPR ON OPR.numOppId = Opp.numOppId where Opp.numOppId=@numOppId 
---- Added If condition  to take into consideration the amount
--if @bitRecurringZeroAmt = 0 
--begin
--	   insert into OpportunityBizDocs(numOppId,vcBizDocID,numBizDocId,fltDiscount,monAmountPaid,vcComments,numCreatedBy,dtCreatedDate,numViewedBy,dtViewedDate,numApprovedBy,dtApprovedDate,[dtFromDate])          
--	   Select @currentOppId,@vcBizDocID,numBizDocId,fltDiscount,monAmountPaid,vcComments,numCreatedBy,getutcdate(),numViewedBy,dtViewedDate,numApprovedBy,dtApprovedDate,GETUTCDATE() From OpportunityBizDocs 
--	Where numOppBizDocsId=@numOppBizDocsId   
--end
--else
--begin
--	insert into OpportunityBizDocs(numOppId,vcBizDocID,numBizDocId,fltDiscount,monAmountPaid,vcComments,numCreatedBy,dtCreatedDate,numViewedBy,dtViewedDate,numApprovedBy,dtApprovedDate,[dtFromDate])          
--	Select @currentOppId,@vcBizDocID,numBizDocId,fltDiscount,0,vcComments,numCreatedBy,getutcdate(),numViewedBy,dtViewedDate,numApprovedBy,dtApprovedDate,GETUTCDATE() From OpportunityBizDocs 
--	Where numOppBizDocsId=@numOppBizDocsId   
--end
--
--  
--   Set @numCurrentBizDocId=@@Identity    
--   Select @monamount=isnull(sum(monAmount),0) From OpportunityBizDocsDetails Where numBizDocsId=@numOppBizDocsId     
--   Insert into OpportunityBizDocsDetails(numBizDocsId,numPaymentMethod,monAmount,numDomainId,bitAuthoritativeBizDocs,    
--   bitIntegratedToAcnt,numCreatedBy,dtCreationDate)Values(@numCurrentBizDocId,0,@monamount,@numDomainId,1,0,1 ,getutcdate())    
--   Set @numBizDocsPaymentDetId=@@Identity    
--   print '@AuthoritativeBizDocsId==='+convert(varchar(1000),@AuthoritativeBizDocsId)    
--   Print '@numBizDocId=='+convert(varchar(100),@numBizDocId)    
--   --To insert into OpportunityBizDocsPaymentDetails    
--   if @numBizDocId=@AuthoritativeBizDocsId    
--    Begin    
--     Print '@numBizDocId=='+convert(varchar(100),@numBizDocId)    
--     -- To update bitAuthoritativeBizDocs=1 in OpportunityBizDocs Table    
--     Update OpportunityBizDocs Set bitAuthoritativeBizDocs=1 Where numOppBizDocsId=@numCurrentBizDocId    
--     Insert into OpportunityBizDocsPaymentDetails(numBizDocsPaymentDetId,monAmount,dtDueDate,bitIntegrated)    
--     Values(@numBizDocsPaymentDetId,@monamount,getutcdate(),0)    
--    End    
--      
--   Select top 1 @numOppBizDocsId=numOppBizDocsId,@numBizDocId=numBizDocId From OpportunityBizDocs Where numOppId=@numOppId And numOppBizDocsId<=@numMaxOppBizDocsId            
--      And numOppBizDocsId>@numOppBizDocsId                             
--   If @@rowcount=0 set @numOppBizDocsId=0       
--  End    
--  update OpportunityBizDocs Set vcBizDocID= @vcBizDocID +Convert(varchar(10),numOppBizDocsId)  Where numOppId=@currentOppId    
--      
--  --To Insert into OpportunityItems Table    
--  Insert into OpportunityItems(numOppId,numItemCode,numUnitHour,monPrice,monTotAmount,numSourceID,vcItemDesc,numWarehouseItmsID,vcType,vcAttributes,bitDropShip)           
--  Select @currentOppId,numItemCode,numUnitHour,monPrice,monTotAmount,numSourceID,vcItemDesc,numWarehouseItmsID,vcType,vcAttributes,bitDropShip From OpportunityItems Where numOppId=@numOppId          
--      
--  -- To Insert into TimeAndExpense Table    
--  Insert into TimeAndExpense(tintTEType,numCategory,numType,dtFromDate,dtToDate,bitFromFullDay,bitToFullDay,monAmount,numOppId,numDivisionID,txtDesc,numUserCntID,numDomainID,bitApproved,numCaseId,    
--  numProId,numStageId,numContractId,numOppBizDocsId,numTCreatedBy,dtTCreatedOn,bitReimburse)    
--  Select top 1 1,2,1,getUTCdate(),getUTCdate(),0,0,    
--  (select isnull(sum(X.amount),0)  from (Select Sum(monamount) as amount from timeandexpense  where numCategory = 2 and numtype=1 and numcontractid =0 And numOppId=@numOppId    
--  union all    
--  Select sum(cast(datediff(mi,dtfromdate,dttodate)as float)*monamount/60) as amount from timeandexpense  where numCategory = 1  and numtype=1 and numcontractid =0 And numOppId=@numOppId)X)    
--  ,@currentOppId,numDivisionID,'',numUserCntID,numDomainID,bitApproved,0,    
--  0,0,0,0,numTCreatedBy,getUTCdate(),bitReimburse From TimeAndExpense Where numOppId=@numOppId   
--    
--      
--  --To Insert into General_Journal_Header     
--    
--  Select @numJournalId=numJournal_Id From General_Journal_Header Where numOppId=@numOppId --and numOppBizDocsId=@numOppBizDocsId    
--  Print '@numJournalId-----------0000---' +Convert(varchar(1000),@numJournalId)    
--  Insert into General_Journal_Header(datEntry_Date,varDescription,numAmount,numDomainId,numOppId,numOppBizDocsId,numCreatedBy,datCreatedDate)    
--  Select getutcdate(),varDescription,numAmount,numDomainId,@currentOppId,@numCurrentBizDocId,numCreatedBy,getutcdate() From General_Journal_Header Where numJournal_Id=@numJournalId and numDomainId=@numDomainId    
--  Set @numCurrentJournalId=@@Identity    
--     
--  --To insert into General_Journal_Details    
--  Insert into General_Journal_Details(numJournalId,numDebitAmt,numCreditAmt,numChartAcntId,varDescription,numCustomerId,numDomainId,numBalance,bitMainDeposit,bitMainCheck,bitMainCashCredit,numoppitemtCode,chBizDocItems,vcReference,numPaymentMethod
--,bitReconcile)    
--  Select @numCurrentJournalId,numDebitAmt,numCreditAmt,numChartAcntId,varDescription,numCustomerId,numDomainId,numBalance,bitMainDeposit,bitMainCheck,bitMainCashCredit,numoppitemtCode,chBizDocItems,vcReference,numPaymentMethod,bitReconcile
-- From General_Journal_Details where numJournalId=@numJournalId and numDomainId=@numDomainId    
--     
--  --To update OpeningBalance in Chart_of_Accounts    
--  Exec USP_UpdateChartAcntOpnBalanceForJournalEntry @JournalId=@numCurrentJournalId,@numDomainId=@numDomainId       
--      
--  --To update LastRecurringDate to opportunityRecurring table                                
--  Update opportunityRecurring set dtLastRecurringDate=getutcdate() Where numOppId=@numOppId              
--      
--  --To update numNoTransactions to opportunityRecurring table                                
--  If @bitEndTransactionType=1 and @numNoTransactions<=@numNoTransactionsRecurring              
--   Update opportunityRecurring set numNoTransactions=isnull(numNoTransactions,0)+1 Where numOppId=@numOppId    
--End
--
--if @numOppId <> 0 and @currentOppId <> 0 
--BEGIN 
--print 'Inserted'
--	insert into RecurringTransactionReport (tintRecType,numRecTranSeedId, numRecTranOppID, dteOppCreationDate)
--values (1, @numOppId, @currentOppId , getutcdate())
END 
