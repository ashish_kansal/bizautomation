GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_UpdateOpportunityBizDocsIntegratedToAcnt')
DROP PROCEDURE usp_updateopportunitybizdocsintegratedtoacnt
GO
CREATE PROCEDURE [dbo].[USP_UpdateOpportunityBizDocsIntegratedToAcnt] 
(@numBizDocsPaymentDetId as numeric(9)=0)  
As  
Begin  
	UPDATE dbo.OpportunityBizDocsPaymentDetails SET bitIntegrated=0 WHERE numBizDocsPaymentDetId =@numBizDocsPaymentDetId
	Update OpportunityBizDocsDetails Set bitIntegratedToAcnt = 0  Where numBizDocsPaymentDetId=@numBizDocsPaymentDetId  
End
GO
