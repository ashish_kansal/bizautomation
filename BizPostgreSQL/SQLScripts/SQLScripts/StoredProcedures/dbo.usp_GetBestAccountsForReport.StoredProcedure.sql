/****** Object:  StoredProcedure [dbo].[usp_GetBestAccountsForReport]    Script Date: 07/26/2008 16:16:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--- Created By Anoop Jayaraj                        
--- modified By Anoop Jayaraj                        
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getbestaccountsforreport')
DROP PROCEDURE usp_getbestaccountsforreport
GO
CREATE PROCEDURE [dbo].[usp_GetBestAccountsForReport]                          
 @numDomainID numeric,                            
 @dtDateFrom datetime,                            
 @dtDateTo datetime,                            
 @numUserCntID numeric=0,                                        
 @intType numeric=0,                          
 @tintRights TINYINT=1                          
  --                          
AS                            
BEGIN                            
 If @tintRights=1                          
 --All Records Rights                            
 BEGIN                            
  --- Check the Condition Here                         
 SELECT  CI.vcCompanyName + '-' + DM.vcDivisionName  AS CustomerDivision,DM.numDivisionID,dbo.fn_GetPrimaryContact(DM.numDivisionID) as ContactID,CI.numCompanyID,DM.tintCRMType,                          
  dbo.fn_GetContactName(OM.numRecOwner) As AccountOwner,                          
  (select sum(M.monPAmount)from OpportunityMaster M where  M.numDivisionID=DM.numDivisionID  And tintOppType=1 and tintOppStatus=1 And bintCreatedDate BETWEEN @dtDateFrom AND @dtDateTo ) As ClosedDealsAmount,                         
  (select count(*)from OpportunityMaster M where  M.numDivisionID=DM.numDivisionID  And tintOppType=1 and tintOppStatus=1 And bintCreatedDate BETWEEN @dtDateFrom AND @dtDateTo ) as DealsClosed,                          
  (select sum(M.monPAmount)from OpportunityMaster M where  M.numDivisionID=DM.numDivisionID  And tintOppType=1 and tintOppStatus=0 And bintCreatedDate BETWEEN @dtDateFrom AND @dtDateTo  ) as OpportunitiesInPipelineAmount,                          
  (select count(*) from OpportunityMaster M where  M.numDivisionID=DM.numDivisionID  And tintOppType=1 and tintOppStatus=0 And bintCreatedDate BETWEEN @dtDateFrom AND @dtDateTo  ) as OpportunitiesInPipeline,                          
  LD.vcData as Industry                          
 FROM  OpportunityMaster OM                           
  JOIN DivisionMaster DM                          
  ON DM.numDivisionID = OM.numDivisionID                          
  JOIN AdditionalContactsInformation ACI                          
  ON ACI.numContactID = OM.numContactID                          
  JOIN CompanyInfo CI                          
  ON CI.numCompanyID = DM.numCompanyID                          
  left JOIN ListDetails LD                          
  ON LD.numListItemID = CI.numCompanyIndustry                          
  WHERE DM.numDomainID = @numDomainID and DM.numRecOwner=@numUserCntID And OM.bintCreatedDate BETWEEN @dtDateFrom AND @dtDateTo              
  GROUP BY CI.vcCompanyName, DM.vcDivisionName,CI.numCompanyID,DM.tintCRMType, DM.numDivisionID,LD.vcData,OM.numRecOwner                           
                          
 END                            
                            
 If @tintRights=2                          
 BEGIN                            
                          
SELECT  CI.vcCompanyName + '-' + DM.vcDivisionName  AS CustomerDivision, DM.numDivisionID, dbo.fn_GetPrimaryContact(DM.numDivisionID) as ContactID,CI.numCompanyID,DM.tintCRMType,                          
  dbo.fn_GetContactName(OM.numRecOwner) As AccountOwner,                           
--  SUM(OM.monPAmount) As ClosedDealsAmount,       
 (select SUM(M.monPAmount) from OpportunityMaster M INNER JOIN AdditionalContactsInformation ACI ON ACI.numContactID = M.numContactID where  M.numDivisionID=DM.numDivisionID And tintOppType=1 and M.tintOppStatus=1     
   And M.bintCreatedDate BETWEEN @dtDateFrom AND @dtDateTo ) as ClosedDealsAmount,           
 ( select count(*)from OpportunityMaster M INNER JOIN AdditionalContactsInformation ACI ON ACI.numContactID = M.numContactID where  M.numDivisionID=DM.numDivisionID And     
     tintOppType=1  and M.tintOppStatus=1 And M.bintCreatedDate BETWEEN @dtDateFrom AND @dtDateTo ) as DealsClosed,        
   --SUM(OM.numOppID) as OpportunitiesInPipelineAmount,             
  (select SUM(M.monPAmount) from OpportunityMaster M where  M.numDivisionID=DM.numDivisionID  And tintOppType=1 and  tintOppStatus=0 And bintCreatedDate BETWEEN @dtDateFrom AND @dtDateTo  ) as OpportunitiesInPipelineAmount,                                
                 
  (select count(*) from OpportunityMaster M where  M.numDivisionID=DM.numDivisionID  And tintOppType=1 and tintOppStatus=0 And bintCreatedDate BETWEEN @dtDateFrom AND @dtDateTo  ) as OpportunitiesInPipeline,                                        
  LD.vcData as Industry                          
  FROM  OpportunityMaster OM                           
  JOIN DivisionMaster DM                          
  ON DM.numDivisionID = OM.numDivisionID                          
  JOIN AdditionalContactsInformation ACI                          
  ON ACI.numContactID = OM.numContactID                          
  JOIN CompanyInfo CI                          
  ON CI.numCompanyID = DM.numCompanyID                          
  left JOIN ListDetails LD                          
  ON LD.numListItemID = CI.numCompanyIndustry                
  join AdditionalContactsInformation  ADC                  
  on ADC.numContactId= OM.numRecOwner                           
 WHERE DM.numDomainID = @numDomainID and DM.numRecOwner=@numUserCntID And OM.bintCreatedDate BETWEEN @dtDateFrom AND @dtDateTo                
 and ADC.numTeam in(select F.numTeam from ForReportsByTeam F                           
 where F.numUserCntID=@numUserCntID and F.numDomainid=@numDomainId and F.tintType=@intType)                            
 GROUP BY CI.vcCompanyName, DM.vcDivisionName,DM.numDivisionID,CI.numCompanyID,DM.tintCRMType, ACI.vcFirstName, ACI.vcLastName, ACI.numPhone, ACI.numPhoneExtension,                          
  LD.vcData,OM.numRecOwner                          
                         
                          
 END                            
                            
 If @tintRights=3                          
 BEGIN                            
      
                   
SELECT  CI.vcCompanyName + '-' + DM.vcDivisionName  AS CustomerDivision,DM.numDivisionID, dbo.fn_GetPrimaryContact(DM.numDivisionID) as ContactID,CI.numCompanyID,DM.tintCRMType,                     
  dbo.fn_GetContactName(OM.numRecOwner) As AccountOwner,                           
--  SUM(OM.monPAmount) As ClosedDealsAmount,        
  (select SUM(M.monPAmount) from OpportunityMaster M where  M.numDivisionID=DM.numDivisionID And tintOppType=1 and tintOppStatus=1 And bintCreatedDate BETWEEN @dtDateFrom AND @dtDateTo ) as ClosedDealsAmount,         
  (select count(*)from OpportunityMaster M where  M.numDivisionID=DM.numDivisionID  And tintOppType=1 and tintOppStatus=1 And bintCreatedDate BETWEEN @dtDateFrom AND @dtDateTo ) as DealsClosed,         
  --SUM(OM.numOppID) as OpportunitiesInPipelineAmount,         
  (select SUM(M.monPAmount) from OpportunityMaster M where  M.numDivisionID=DM.numDivisionID  And tintOppType=1 and tintOppStatus=0 And bintCreatedDate BETWEEN @dtDateFrom AND @dtDateTo  ) as OpportunitiesInPipelineAmount,                                 
    
   (select count(*) from OpportunityMaster M where  M.numDivisionID=DM.numDivisionID  And tintOppType=1 and tintOppStatus=0 And bintCreatedDate BETWEEN @dtDateFrom AND @dtDateTo  ) as OpportunitiesInPipeline,                                     
  LD.vcData as Industry                          
  FROM  OpportunityMaster OM                           
  JOIN DivisionMaster DM                          
  ON DM.numDivisionID = OM.numDivisionID                          
  JOIN AdditionalContactsInformation ACI                          
  ON ACI.numContactID = OM.numContactID                          
  JOIN CompanyInfo CI                          
  ON CI.numCompanyID = DM.numCompanyID                          
  left JOIN ListDetails LD                          
  ON LD.numListItemID = CI.numCompanyIndustry                          
 WHERE DM.numDomainID = @numDomainID and DM.numRecOwner=@numUserCntID  And OM.bintCreatedDate BETWEEN @dtDateFrom AND @dtDateTo               
 and DM.numTerId in(select F.numTerritory from ForReportsByTerritory F   -- Added By Siva                           
 where F.numUserCntID=@numUserCntID and F.numDomainid=@numDomainID and F.tintType=@intType)                    
 GROUP BY CI.vcCompanyName, DM.vcDivisionName,DM.numDivisionID,CI.numCompanyID,DM.tintCRMType,  ACI.vcFirstName, ACI.vcLastName, ACI.numPhone, ACI.numPhoneExtension,                        
 LD.vcData,OM.numRecOwner                         
                          
 END                            
 END
GO
