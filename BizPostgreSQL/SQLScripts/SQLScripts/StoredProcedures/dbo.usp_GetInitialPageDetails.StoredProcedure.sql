/****** Object:  StoredProcedure [dbo].[usp_GetInitialPageDetails]    Script Date: 07/26/2008 16:17:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getinitialpagedetails')
DROP PROCEDURE usp_getinitialpagedetails
GO
CREATE PROCEDURE [dbo].[usp_GetInitialPageDetails]  
    @numGroupType NUMERIC,
    @numDomainId NUMERIC,
    @numContactId NUMERIC,
	@numTabId NUMERIC
as  

if (select count(*) from ShortCutUsrCnf where numGroupId=@numGroupType and numDomainId=@numDomainId and numContactID=@numContactID and isnull(numTabId,0)=@numTabId and isnull(bitInitialPage,0)=1)>0    
begin    
   Select top 1 Hdr.id,Hdr.vclinkname as [Name],Hdr.link,Hdr.numTabId
   from ShortCutUsrcnf Sconf join  ShortCutBar Hdr on  Hdr.id =Sconf.numLinkId  and Sconf.numTabId=Hdr.numTabId         
   where sconf.numDomainId = @numDomainId and sconf.numGroupID =@numGroupType  and sconf.numContactId =@numContactId and isnull(Hdr.numTabId,0)=@numTabId and isnull(Sconf.bitInitialPage,0)=1 and isnull(Sconf.tintLinkType,0)=0        
	UNION
  Select top 1 LD.numListItemID,LD.vcData  as [Name],'../prospects/frmCompanyList.aspx?RelId='+convert(varchar(10),LD.numListItemID) as link,Sconf.numTabId
   from ShortCutUsrcnf Sconf join  ListDetails LD on  LD.numListItemID =Sconf.numLinkId           
   where LD.numListId = 5 and LD.numListItemID<>46 and(LD.numDomainID=@numDomainID or constflag=1)  and 
		sconf.numDomainId = @numDomainId and sconf.numGroupID =@numGroupType  and sconf.numContactId =@numContactId and isnull(Sconf.numTabId,0)=@numTabId and isnull(Sconf.bitInitialPage,0)=1 and isnull(Sconf.tintLinkType,0)=5        

end    
else IF (select count(*) from ShortCutGrpConf where numDomainId = @numDomainId and numGroupID =@numGroupType  and isnull(numTabId,0)=@numTabId and isnull(bitInitialPage,0)=1) > 0              
 begin    
  select top 1 Hdr.id,Hdr.vclinkname as [Name],Hdr.link,Hdr.numTabId
  from ShortCutGrpConf  Sconf  join  ShortCutBar Hdr  on  Hdr.id=Sconf.numLinkId and Sconf.numTabId=Hdr.numTabId 
  where Sconf.numGroupID =@numGroupType and Sconf.numDomainId = @numDomainId   and Hdr.numContactId =0 and isnull(Hdr.numTabId,0)=@numTabId and isnull(Sconf.bitInitialPage,0)=1 and isnull(Sconf.tintLinkType,0)=0  
		UNION
  Select top 1 LD.numListItemID,LD.vcData as [Name],'../prospects/frmCompanyList.aspx?RelId='+convert(varchar(10),LD.numListItemID) as link,Sconf.numTabId
   from ShortCutGrpConf Sconf join  ListDetails LD on  LD.numListItemID =Sconf.numLinkId           
   where LD.numListId = 5 and LD.numListItemID<>46 and(LD.numDomainID=@numDomainID or constflag=1)  and 
		sconf.numDomainId = @numDomainId and sconf.numGroupID =@numGroupType and isnull(Sconf.numTabId,0)=@numTabId and isnull(Sconf.bitInitialPage,0)=1 and isnull(Sconf.tintLinkType,0)=5        
 end  
ELSE IF (SELECT COUNT(*) FROM ShortCutBar WHERE isnull(numTabId,0)=@numTabId and isnull(bitInitialPage,0)=1) > 0
	select top 1 Hdr.id,Hdr.vclinkname as [Name],Hdr.link,Hdr.numTabId
	from ShortCutBar Hdr  
	where isnull(numTabId,0)=@numTabId and isnull(bitInitialPage,0)=1 
ELSE
BEGIN
	--NO INITIAL LINK AVAILABLE
	IF @numTabId = 80
	BEGIN
		SELECT 
			0 AS id,'All Items' as [Name],'../Items/frmItemList.aspx?Page=All Items&ItemGroup=0' AS Link,80 AS numTabId
	END
END  
-- else    
-- begin    
--  select id,vclinkname  as [Name],link,numTabId,vcImage,isnull(bitNew,0) as bitNew,isnull(bitNewPopup,0) as bitNewPopup,NewLink from ShortCutBar where bitdefault = 1 and numContactId =0 and isnull(numTabId,0)=@numTabId
-- end


--declare @strSql as varchar(500)
--set @strSql= '
--select * from RelationsDefaultFilter   
-- WHERE numUserCntId='+ convert(varchar(10),@numUserCntId)+ 'and numDomainId= '+ convert(varchar(10),@numDomainId)+'
--	 and bitInitialPage=1 '
--
--if @FormId = 14 
-- set @strSql=@strSql+' and numformId in(38,39,40,41) '
--if @FormId = 0 
--set @strSql=@strSql+' and numformId in (34,35,36,10) '
--PRINT @strSql
--exec(@strSql)
GO
