/****** Object:  StoredProcedure [dbo].[USP_GetNewsList]    Script Date: 07/26/2008 16:17:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj    
--exec USP_GetNewsList 0,0,'new'    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getnewslist')
DROP PROCEDURE usp_getnewslist
GO
CREATE PROCEDURE [dbo].[USP_GetNewsList]    
@numNewsID as numeric(9)=0,    
@bitActive as tinyint=0,    
@vcHeading as varchar(200)=''    
as    
    
if @numNewsID=0    
begin    
select numNewsID,vcNewsHeading,vcDate,    
vcDesc,vcImagePath,vcURL,vcCreatedDate,    
vcModifieddate,case when tintType=1 then 'News Coverage' when tintType=2 then 'Press Releases' when tintType=3 then 'White papers' end as type,
case when bitActive=1 then 'Active' when bitActive=0 then 'Inactive' end as Active    
from news where bitActive=@bitActive and vcNewsHeading like '%'+@vcHeading+'%' and tintType=1    
order by numNewsID desc  

select numNewsID,vcNewsHeading,vcDate,    
vcDesc,vcImagePath,vcURL,vcCreatedDate,    
vcModifieddate,case when tintType=1 then 'News Coverage' when tintType=2 then 'Press Releases' when tintType=3 then 'White papers' end as type, 
case when bitActive=1 then 'Active' when bitActive=0 then 'Inactive' end as Active    
from news where bitActive=@bitActive and vcNewsHeading like '%'+@vcHeading+'%'  and tintType=2   
order by numNewsID desc

select numNewsID,vcNewsHeading,vcDate,    
vcDesc,vcImagePath,vcURL,vcCreatedDate,    
vcModifieddate,case when tintType=1 then 'News Coverage' when tintType=2 then 'Press Releases' when tintType=3 then 'White papers' end as type,
case when bitActive=1 then 'Active' when bitActive=0 then 'Inactive' end as Active    
from news where bitActive=@bitActive and vcNewsHeading like '%'+@vcHeading+'%' and tintType=3    
order by numNewsID desc
end    
if @numNewsID>0    
begin    
select numNewsID,vcNewsHeading,vcDate,    
vcDesc,vcImagePath,vcURL,vcCreatedDate,    
vcModifieddate,bitActive,tintType    
from news where numNewsID=@numNewsID    
    
end
GO
