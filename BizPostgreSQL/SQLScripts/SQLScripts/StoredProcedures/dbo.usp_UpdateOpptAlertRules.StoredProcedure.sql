/****** Object:  StoredProcedure [dbo].[usp_UpdateOpptAlertRules]    Script Date: 07/26/2008 16:21:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_updateopptalertrules')
DROP PROCEDURE usp_updateopptalertrules
GO
CREATE PROCEDURE [dbo].[usp_UpdateOpptAlertRules]
	@numStagePercentageId numeric(18)=0,
	@monAmt DECIMAL(20,5)=0,
	@vcCondn varchar(10)='',
	@numNoOfDays numeric(18)=0,
	@numModifiedBy numeric(18)=0,
	@bintModifiedDate bigint=0,
	@numRuleId numeric=0,
	@numEmpId numeric =0,
	@numDetsFlag numeric=0   
--
AS

IF @numDetsFlag=0
	BEGIN
	UPDATE opptRuleMaster 
	SET numStagePercentageId=@numStagePercentageId,
	    monAmt=@monAmt,
	    vcCondn=@vcCondn,
	    numNoOfDays=@numNoOfDays,
	    numModifiedBy=@numModifiedBy,
	    bintModifiedDate=@bintModifiedDate
	WHERE numruleid=@numRuleId
	END
ELSE
	BEGIN
	UPDATE RuleDetails SET 
	numEmpId=@numEmpId
	WHERE numRuleId=@numRuleId
	END
GO
