/****** Object:  StoredProcedure [dbo].[USP_ReturningVisitors]    Script Date: 07/26/2008 16:20:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj      
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_returningvisitors')
DROP PROCEDURE usp_returningvisitors
GO
CREATE PROCEDURE [dbo].[USP_ReturningVisitors]      
@From datetime,      
@To datetime,      
@CurrentPage int,              
@PageSize int,              
@TotRecs int output,              
@columnName as Varchar(50),              
@columnSortOrder as Varchar(10) ,    
@numDomainID as numeric(9) ,
@ClientOffsetTime as integer             
as              
              
--Create a Temporary table to hold data              
Create table #tempTable       
    ( ID INT IDENTITY PRIMARY KEY,               
      vcUserHostAddress varchar(100),      
       vcUserDomain varchar(100),      
       [Count]  Varchar(15)       
      )              
      
declare @strSql as varchar(5000)              
set @strSql='select distinct(vcUserHostAddress),  
case when vcUserDomain is null then vcUserHostAddress when vcUserDomain=''-'' then vcUserHostAddress else vcUserDomain end as vcUserDomain,       
(select count(*) from TrackingVisitorsHDR T  where T.vcUserHostAddress=HDR.vcUserHostAddress)  as [Count]      
from   TrackingVisitorsHDR HDR       
where numDomainId='+convert(varchar(10),@numDomainID)+' and  (dtCreated between '''+convert(varchar(50),dateadd(minute,@ClientOffsetTime,@From))+''' and '''+convert(varchar(50),dateadd(minute,@ClientOffsetTime,@To))+''')'         
set  @strSql=@strSql +' ORDER BY ' + @columnName +' '+ @columnSortOrder            
print @strSql        
insert into #tempTable (vcUserHostAddress,vcUserDomain,[Count] )              
exec( @strSql)              
  declare @firstRec as integer              
  declare @lastRec as integer              
 set @firstRec= (@CurrentPage-1) * @PageSize              
     set @lastRec= (@CurrentPage*@PageSize+1)              
select * from #tempTable where ID > @firstRec and ID < @lastRec              
set @TotRecs=(select count(*) from #tempTable)              
drop table #tempTable
GO
