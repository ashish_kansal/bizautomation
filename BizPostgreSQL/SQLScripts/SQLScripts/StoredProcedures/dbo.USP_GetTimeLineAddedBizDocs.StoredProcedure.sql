/****** Object:  StoredProcedure [dbo].[USP_GetTimeLineAddedBizDocs]    Script Date: 07/26/2008 16:18:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_gettimelineaddedbizdocs')
DROP PROCEDURE usp_gettimelineaddedbizdocs
GO
CREATE PROCEDURE [dbo].[USP_GetTimeLineAddedBizDocs]
@numDivId numeric,
@numDomainId numeric
as 
select distinct TE.numOppBizDocsId,vcBizDocId,OBD.numOppid from timeandexpense TE
join OpportunityBizDocs OBD on OBD.numOppBizDocsId = TE.numOppBizDocsId
where TE.numDivisionid = @numDivId and numDomainId=  @numDomainId
GO
