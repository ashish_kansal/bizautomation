/****** Object:  StoredProcedure [dbo].[USP_GetOperationBudgetMasterForImpact]    Script Date: 07/26/2008 16:17:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created By Siva  
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getoperationbudgetmasterforimpact')
DROP PROCEDURE usp_getoperationbudgetmasterforimpact
GO
CREATE PROCEDURE [dbo].[USP_GetOperationBudgetMasterForImpact]  
(@numDomainId as numeric(9)=0)  
As  
Begin  
	Select numBudgetId,vcBudgetName From OperationBudgetMaster 
	Where numDomainId=@numDomainId  
End
GO
