/****** Object:  StoredProcedure [dbo].[USP_GetItemGroups]    Script Date: 07/26/2008 16:17:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getitemgroups')
DROP PROCEDURE usp_getitemgroups
GO
CREATE PROCEDURE [dbo].[USP_GetItemGroups]    
@numDomainID as numeric(9)=0,    
@numItemGroupID as numeric(9)=0,
@numItemCode as numeric(9)=0    
 
as    
    
if @numItemGroupID=0    
begin    
	IF @numItemCode>0
		BEGIN
			Select @numItemGroupID=numItemGroup FROM Item WHERE numItemCode=@numItemCode
			
			  select numItemCode,vcItemName,isnull(Dtl.numWareHouseItemId,0) as numWarehouseItmsID,txtItemDesc,DTL.numQtyItemsReq,0 as numOppChildItemID,
CASE WHEN isnull(DTL.numWareHouseItemId,0)>0 THEN ISNULL(WI.[monWListPrice],0) ELSE Item.monListPrice END monListPrice, 
convert(varchar(30),CASE WHEN isnull(DTL.numWareHouseItemId,0)>0 THEN ISNULL(WI.[monWListPrice],0) ELSE Item.monListPrice END)+'/'+isnull(UOM.vcUnitName,'Units') as UnitPrice,            
case when charItemType='P' then 'Inventory Item' when charItemType='S' then 'Service' when charItemType='A' then 'Accessory' when charItemType='N' then 'Non-Inventory Item' end as charItemType,charItemType as ItemType                            
,WI.numItemID,numDefaultSelect,(SELECT vcPathForImage FROM ItemImages WHERE numItemCode = @numItemCode AND numDomainId = @numDomainId AND bitDefault = 1 AND bitIsImage = 1) AS  vcPathForImage,vcWareHouse,ISNULL(Dtl.numListId,0) AS numListId
from item                                
join ItemGroupsDTL Dtl 
LEFT OUTER JOIN [WareHouseItems] WI ON WI.[numItemID] = DTL.[numOppAccAttrID] AND WI.[numWareHouseItemID] = Dtl.[numWareHouseItemId]                  
LEFT OUTER JOIN Warehouses W ON W.numWareHouseID=WI.numWareHouseID
on numOppAccAttrID=numItemCode  
LEFT JOIN UOM ON UOM.numUOMId=item.numBaseUnit
                  
where  Dtl.numItemGroupID=@numItemGroupID and tintType=1 
			
		END
	ELSE
	BEGIN
		select numItemGroupID, vcItemGroup, numDomainID,bitCombineAssemblies,
		ISNULL(CAST((SELECT TOP 1 Fld_id FROM CFW_Fld_Master WHERE numlistid=numMapToDropdownFld) AS VARCHAR),0)+'-'+CAST(numMapToDropdownFld AS VARCHAR) AS numMapToDropdownFld
		,numProfileItem from ItemGroups     
		where numDomainID=@numDomainID    		
	END
end    
else if @numItemGroupID>0     
begin    
 select numItemGroupID, vcItemGroup, numDomainID,bitCombineAssemblies,
 ISNULL(CAST((SELECT TOP 1 Fld_id FROM CFW_Fld_Master WHERE numlistid=numMapToDropdownFld) AS VARCHAR),0)+'-'+CAST(numMapToDropdownFld AS VARCHAR) AS numMapToDropdownFld
 ,numProfileItem from ItemGroups     
 where numDomainID=@numDomainID and numItemGroupID=@numItemGroupID    
  
--select numItemCode,vcItemName,txtItemDesc,monListPrice from Item     
--  join ItemGroupsDTL    
--  on numOppAccAttrID=numItemCode    
--  where numItemGroupID=@numItemGroupID and tintType=1    
  
  select numItemCode,vcItemName,isnull(Dtl.numWareHouseItemId,0) as numWarehouseItmsID,txtItemDesc,DTL.numQtyItemsReq,0 as numOppChildItemID,
CASE WHEN isnull(DTL.numWareHouseItemId,0)>0 THEN ISNULL(WI.[monWListPrice],0) ELSE Item.monListPrice END monListPrice, 
convert(varchar(30),CASE WHEN isnull(DTL.numWareHouseItemId,0)>0 THEN ISNULL(WI.[monWListPrice],0) ELSE Item.monListPrice END)+'/'+isnull(UOM.vcUnitName,'Units') as UnitPrice,            
case when charItemType='P' then 'Inventory Item' when charItemType='S' then 'Service' when charItemType='A' then 'Accessory' when charItemType='N' then 'Non-Inventory Item' end as charItemType,charItemType as ItemType                            
,WI.numItemID,numDefaultSelect,(SELECT vcPathForImage FROM ItemImages WHERE numItemCode = @numItemCode AND numDomainId = @numDomainId AND bitDefault = 1 AND bitIsImage = 1) AS  vcPathForImage,vcWareHouse,ISNULL(Dtl.numListId,0) AS numListId
from item                                
join ItemGroupsDTL Dtl 
LEFT OUTER JOIN [WareHouseItems] WI ON WI.[numItemID] = DTL.[numOppAccAttrID] AND WI.[numWareHouseItemID] = Dtl.[numWareHouseItemId]                  
on numOppAccAttrID=numItemCode     
LEFT OUTER JOIN Warehouses W ON W.numWareHouseID=WI.numWareHouseID
LEFT JOIN UOM ON UOM.numUOMId=item.numBaseUnit

where  numItemGroupID=@numItemGroupID and tintType=1 


SELECT CFW.Fld_id,Fld_label, CFW.numlistid
		
FROM CFW_Fld_Master CFW  
JOIN ItemGroupsDTL    
	ON numOppAccAttrID=Fld_id    
WHERE numItemGroupID=@numItemGroupID and tintType=2 

--SELECT CFW.Fld_id,Fld_label,
--		CFW.numlistid,IA.FLD_Value,
--		CASE WHEN CFW.fld_type='SelectBox' THEN dbo.GetListIemName(Fld_Value)
--			ELSE CAST(Fld_Value AS VARCHAR)
--		END AS FLD_ValueName
--FROM CFW_Fld_Master CFW  
--JOIN ItemGroupsDTL    
--	ON numOppAccAttrID=Fld_id    
--LEFT JOIN ItemAttributes IA
--	ON CFW.Fld_id = IA.FLD_ID

--WHERE numItemGroupID=@numItemGroupID and tintType=2     
--GROUP BY CFW.Fld_id,Fld_label,CFW.numlistid,IA.FLD_Value,CFW.fld_type
    
end
GO
