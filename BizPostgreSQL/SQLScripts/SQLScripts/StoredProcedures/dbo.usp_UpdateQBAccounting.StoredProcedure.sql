/****** Object:  StoredProcedure [dbo].[usp_UpdateQBAccounting]    Script Date: 07/26/2008 16:21:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_updateqbaccounting')
DROP PROCEDURE usp_updateqbaccounting
GO
CREATE PROCEDURE [dbo].[usp_UpdateQBAccounting]
	@vcCompanyName varchar(255),
	@numLastUpdateDate numeric,
	@vcAccountNo varchar(20),
	@numOpenARBalance float,
	@numCreditLimit float,
	@vcTerms varchar(30)   
	--
as
update QBAccounting  set numLastUpdateDate=@numLastUpdateDate ,vcAccountNo=@vcAccountNo ,numOpenARBalance=@numOpenARBalance ,numCreditLimit=@numCreditLimit ,vcTerms=@vcTerms where vcCompanyName=@vcCompanyName
GO
