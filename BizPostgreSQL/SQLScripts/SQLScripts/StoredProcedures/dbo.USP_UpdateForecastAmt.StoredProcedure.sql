/****** Object:  StoredProcedure [dbo].[USP_UpdateForecastAmt]    Script Date: 07/26/2008 16:21:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_updateforecastamt')
DROP PROCEDURE usp_updateforecastamt
GO
CREATE PROCEDURE [dbo].[USP_UpdateForecastAmt]
@numForecastAmount as DECIMAL(20,5),
@numForecastID as numeric(9)

as 

update Forecast
	set monForecastAmount=@numForecastAmount
where numForecastID =@numForecastID
GO
