/****** Object:  StoredProcedure [dbo].[Recurrence_Upd]    Script Date: 07/26/2008 16:14:32 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
/*
**  Updates an existing Recurrence with new values for a recurring series
**  based on it's Recurrence key as found on it's associated Activities.
*/
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='recurrence_upd')
DROP PROCEDURE recurrence_upd
GO
CREATE PROCEDURE [dbo].[Recurrence_Upd]
	@EndDateUtc		datetime,
	@DayOfWeekMaskUtc	integer,
	@UtcOffset		integer,
	@DayOfMonth		integer,
	@MonthOfYear		integer,
	@PeriodMultiple		integer,
	@Period			nchar(1),
	@EditType		integer,
	@LastReminderDateTimeUtc	datetime,
	@RecurrenceID		integer
AS
BEGIN
	UPDATE
		[Recurrence] 
	SET 
		[Recurrence].[EndDateUtc] = @EndDateUtc,
		[Recurrence].[DayOfWeekMaskUtc] = @DayOfWeekMaskUtc, 
		[Recurrence].[UtcOffset] = @UtcOffset,
		[Recurrence].[DayOfMonth] = @DayOfMonth,
		[Recurrence].[MonthOfYear] = @MonthOfYear,
		[Recurrence].[PeriodMultiple] = @PeriodMultiple,
		[Recurrence].[Period] = @Period,
		[Recurrence].[EditType] = @EditType,
		[Recurrence].[LastReminderDateTimeUtc] = @LastReminderDateTimeUtc
	WHERE 
		( [Recurrence].[RecurrenceID] = @RecurrenceID );
END
GO
