/****** Object:  StoredProcedure [dbo].[USP_GetCheckDetails]    Script Date: 07/26/2008 16:16:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created By Siva      
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getcheckdetails')
DROP PROCEDURE usp_getcheckdetails
GO
CREATE PROCEDURE [dbo].[USP_GetCheckDetails]      
@numCheckId as numeric(9)=0,      
@numDomainId as numeric(9)=0      
As      
Begin      
 Select * From CheckDetails CD  
 Left outer join DivisionMaster DM on CD.numCustomerId=DM.numDivisionID     
 Left outer join CompanyInfo CI on DM.numCompanyid=CI.numCompanyid  
 Where CD.numCheckId=@numCheckId And CD.numDomainId=@numDomainId      
End
GO
