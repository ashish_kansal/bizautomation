/****** Object:  StoredProcedure [dbo].[usp_GetPhoneDet]    Script Date: 07/26/2008 16:18:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getphonedet')
DROP PROCEDURE usp_getphonedet
GO
CREATE PROCEDURE [dbo].[usp_GetPhoneDet]  
	@numContactId numeric  --   
AS  SELECT (numPhone + ', ' + numPhoneExtension) as vcPhonedet FROM additionalcontactsinformation where numcontactid=@numcontactid
GO
