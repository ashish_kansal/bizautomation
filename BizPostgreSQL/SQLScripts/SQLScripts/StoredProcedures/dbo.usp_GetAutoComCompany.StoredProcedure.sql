/****** Object:  StoredProcedure [dbo].[usp_GetAutoComCompany]    Script Date: 07/26/2008 16:16:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Modified By Anoop jayaraj                      
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getautocomcompany')
DROP PROCEDURE usp_getautocomcompany
GO
CREATE PROCEDURE [dbo].[usp_GetAutoComCompany]                            
 @numDomainId numeric=0,                                                                   
 @numUserCntID as numeric(9)=0                            
--                          
AS                               
-- checking the company rights                      
                      
DECLARE @ProspectsRights AS TINYINT
DECLARE @accountsRights AS TINYINT                     
DECLARE @leadsRights AS TINYINT

SET @ProspectsRights= (
						SELECT TOP 1 
							MAX(GA.intViewAllowed) As intViewAllowed                      
						FROM 
							UserMaster UM
						INNER JOIN
							GroupAuthorization GA
						ON
							UM.numGroupID = GA.numGroupID
						INNER JOIN
							PageMaster PM
						ON
							PM.numPageID=GA.numPageID                      
							AND PM.numModuleID=GA.numModuleID                      
							AND PM.numpageID=2 
							AND PM.numModuleID=3                     
						WHERE 
							UM.numUserID=(SELECT numUserID FROM userMaster WHERE numUserDetailId=@numUserCntID)                    
						GROUP BY 
							UM.numUserId
							,UM.vcUserName
							,PM.vcFileName
							,GA.numModuleID
							,GA.numPageID
						)                          
                      
SET @accountsRights = (
						SELECT TOP 1 
							MAX(GA.intViewAllowed) As intViewAllowed                      
						FROM 
							UserMaster UM
						INNER JOIN
							GroupAuthorization GA
						ON
							UM.numGroupID = GA.numGroupID
						INNER JOIN
							PageMaster PM
						ON
							PM.numPageID=GA.numPageID                      
							AND PM.numModuleID=GA.numModuleID                      
							AND PM.numpageID=2 
							AND PM.numModuleID=4                         
						WHERE 
							UM.numUserID=(SELECT numUserID FROM userMaster WHERE numUserDetailId=@numUserCntID)                       
						GROUP BY 
							UM.numUserId
							,UM.vcUserName
							,PM.vcFileName
							,GA.numModuleID
							,GA.numPageID
						)                  
                
SET @leadsRights = (
					SELECT TOP 1 
						MAX(GA.intViewAllowed) As intViewAllowed                      
					FROM 
						UserMaster UM
					INNER JOIN
						GroupAuthorization GA
					ON
						UM.numGroupID = GA.numGroupID
					INNER JOIN
						PageMaster PM
					ON
						PM.numPageID=GA.numPageID                      
						AND PM.numModuleID=GA.numModuleID                      
						AND PM.numpageID=2 
						AND PM.numModuleID=2                      
					WHERE 
						UM.numUserID=(select numUserID from userMaster where numUserDetailId=@numUserCntID)                       
					GROUP BY 
						UM.numUserId
						,UM.vcUserName
						,PM.vcFileName
						,GA.numModuleID
						,GA.numPageID
					)                     
                      
declare @strSQL as varchar(8000)                        
                          
set @strSQL='SELECT a.vcCompanyname,d.numDivisionID,a.numCompanyType                             
   FROM companyinfo a                        
   join divisionmaster d                        
   on  a.numCompanyid=d.numCompanyid                        
   WHERE a.numdomainid='+convert(varchar(15),@numdomainid)+' and d.tintCRMType<>0 and d.tintCRMType=1'
 if @ProspectsRights=0  set @strSQL=@strSQL+' and d.numRecOwner=0 '                      
 if @ProspectsRights=1  set @strSQL=@strSQL+' and d.numRecOwner='+convert(varchar(15),@numUserCntID)                       
 if @ProspectsRights=2  set @strSQL=@strSQL+' and (d.numTerID in (select numTerritoryID  from UserTerritory where numUserCntID='+convert(varchar(15),@numUserCntID)+') or d.numTerID=0)'                      
set @strSQL=@strSQL+' union SELECT a.vcCompanyname,d.numDivisionID,a.numCompanyType                             
   FROM companyinfo a                        
   join divisionmaster d                        
   on  a.numCompanyid=d.numCompanyid                        
   WHERE a.numdomainid='+convert(varchar(15),@numdomainid)+' and d.tintCRMType<>0 and d.tintCRMType=2'                   
 if @accountsRights=0  set @strSQL=@strSQL+' and d.numRecOwner=0 '                      
 if @accountsRights=1  set @strSQL=@strSQL+' and d.numRecOwner='+convert(varchar(15),@numUserCntID)                       
 if @accountsRights=2  set @strSQL=@strSQL+' and (d.numTerID in (select numTerritoryID  from UserTerritory where numUserCntID='+convert(varchar(15),@numUserCntID)+') or d.numTerID=0)'                   
set @strSQL=@strSQL+' union SELECT a.vcCompanyname,d.numDivisionID,a.numCompanyType                             
   FROM companyinfo a                        
   join divisionmaster d              
   on  a.numCompanyid=d.numCompanyid                        
   WHERE a.numdomainid='+convert(varchar(15),@numdomainid)+' and d.tintCRMType=0'                    
 if @leadsRights=0  set @strSQL=@strSQL+' and d.numRecOwner=0 '                      
 if @leadsRights=1  set @strSQL=@strSQL+' and d.numRecOwner='+convert(varchar(15),@numUserCntID)                       
 if @leadsRights=2  set @strSQL=@strSQL+' and (d.numTerID in (select numTerritoryID  from UserTerritory where numUserCntID='+convert(varchar(15),@numUserCntID)+') or d.numTerID=0)'                     
 set @strSQL=@strSQL+' ORDER BY a.vcCompanyname'                                       
    exec (@strSQL)
GO
