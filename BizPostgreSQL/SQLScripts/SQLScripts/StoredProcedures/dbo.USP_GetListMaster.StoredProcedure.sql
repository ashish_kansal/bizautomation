/****** Object:  StoredProcedure [dbo].[USP_GetListMaster]    Script Date: 07/26/2008 16:17:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
---created by anoop jayaraj  
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'usp_getlistmaster' ) 
    DROP PROCEDURE usp_getlistmaster
GO
CREATE PROCEDURE [dbo].[USP_GetListMaster]
    @numDomainID AS NUMERIC(9),
    @numModuleID AS NUMERIC(9) = 0,
    @bitCustom BIT = 0
AS 
BEGIN
	DECLARE @strSql VARCHAR(MAX)
    IF @bitCustom = 0 
    BEGIN
        SELECT  
			LM.numListID,
			(CASE WHEN (@numModuleID = 8 AND ISNULL(CLM.Loc_name,'') <> '') THEN CONCAT(LM.vcListName,' (',CLM.Loc_name,')') ELSE LM.vcListName END) AS vcListName 
		FROM    
			listmaster LM
		LEFT JOIN
			dbo.CFW_Fld_Master CFM
		ON
			CFM.numDomainID = LM.numDomainID 
			AND CFM.numlistid= LM.numListID 
		LEFT JOIN
			CFW_Loc_Master CLM    
		ON
			CFM.Grp_id = CLM.Loc_Id
        WHERE   
			(LM.numdomainID = @numDomainID OR LM.bitFlag = 1)
			AND ISNULL(LM.bitDeleted, 0) = 0 
			AND ( ISNULL(LM.numModuleID, 0) = @numModuleID OR @numModuleID = 0 )
        ORDER BY 
			vcListName  	
    END
    ELSE
	BEGIN
		SET @strSql = 'SELECT  L.numListID,L.vcListName FROM listmaster L'
		SET @strSql = @strSql +' WHERE (L.numdomainID = '+ CONVERT(VARCHAR,@numDomainID)+' OR L.bitFlag = 1) AND ISNULL(L.bitDeleted, 0) = 0 AND (ISNULL(L.numModuleID, 0) = '+ CONVERT(VARCHAR,@numModuleID)+''
		Set @strSql = @strSql +' OR  L.numModuleID = 0 )'
		SET @strSql = @strSql +   ' UNION '
		SET @strSql = @strSql +   '(SELECT 
										CFM.numListID,
										(CASE WHEN '+ CONVERT(VARCHAR,@numModuleID)+' = 8 AND ISNULL(CLM.Loc_name,'''') <> '''' THEN CONCAT(LM.vcListName,'' ('',CLM.Loc_name,'')'') ELSE LM.vcListName END) AS vcListName
									FROM 
										dbo.CFW_Fld_Master CFM 
									LEFT JOIN
										CFW_Loc_Master CLM    
									ON
										CFM.Grp_id = CLM.Loc_Id
									INNER JOIN 
										dbo.ListMaster LM 
									ON 
										CFM.numDomainID = LM.numDomainID 
										AND CFM.numlistid= LM.numListID 
										'
			IF @numModuleID = 1
			BEGIN
				SET @strSql = @strSql + 'AND Grp_id IN (1,12,13,14)'	
			END
			IF @numModuleID = 2
			BEGIN
				SET @strSql = @strSql + 'AND Grp_id IN (2,6)'	
			END
			IF @numModuleID = 3
			BEGIN
				SET @strSql = @strSql + 'AND Grp_id IN (5)'	
			END
			IF @numModuleID = 4
			BEGIN
				SET @strSql = @strSql + 'AND Grp_id IN (11)'	
			END
			IF @numModuleID = 5
			BEGIN
				SET @strSql = @strSql + 'AND Grp_id IN (3)'	
			END
			IF @numModuleID = 6
			BEGIN
				SET @strSql = @strSql + 'AND Grp_id IN (12,13,14)'	
			END
			IF @numModuleID = 7
			BEGIN
				SET @strSql = @strSql + 'AND Grp_id IN (0)'	
			END
			IF @numModuleID = 8
			BEGIN
				SET @strSql = @strSql + 'AND Grp_id IN (0)'	
			END
			IF @numModuleID = 9
			BEGIN
				SET @strSql = @strSql + 'AND Grp_id IN (4)'	
			END
			IF @numModuleID = 10
			BEGIN
				SET @strSql = @strSql + 'AND Grp_id IN (0)'	
			END
			IF @numModuleID = 11
			BEGIN
				SET @strSql = @strSql + 'AND Grp_id IN (15)'	
			END
			IF @numModuleID = 12
			BEGIN
				SET @strSql = @strSql + 'AND Grp_id IN (0)'	
			END
			SET @strSql = @strSql + ' WHERE CFM.numDomainID='+ CONVERT(VARCHAR,@numDomainID)+') '
				
			SET @strSql = @strSql + ' order by vcListName '
			PRINT @strSql
			EXEC (@strSql)
	END
END
GO
