/****** Object:  StoredProcedure [dbo].[USP_Journal_CompanyDetForDivisionId]    Script Date: 07/26/2008 16:19:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created By Siva    
 GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_journal_companydetfordivisionid')
DROP PROCEDURE usp_journal_companydetfordivisionid
GO
CREATE PROCEDURE [dbo].[USP_Journal_CompanyDetForDivisionId]          
@numDomainID numeric(9)=0,      
@numDivisionId numeric(9)=0      
As           
Begin        
Select DM.numDivisionID,CI.vcCompanyName From CompanyInfo CI          
join Divisionmaster DM on  CI.numCompanyid=DM.numCompanyid Where CI.numDomainID= @numDomainID  And DM.numDivisionID = @numDivisionId   
End
GO
