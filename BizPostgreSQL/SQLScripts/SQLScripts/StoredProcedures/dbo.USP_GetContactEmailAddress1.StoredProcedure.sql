/****** Object:  StoredProcedure [dbo].[USP_GetContactEmailAddress1]    Script Date: 07/26/2008 16:16:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created By Anoop jayaraj                
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getcontactemailaddress1')
DROP PROCEDURE usp_getcontactemailaddress1
GO
CREATE PROCEDURE [dbo].[USP_GetContactEmailAddress1]                     
 @numUserCntID numeric(9)=0,                      
 @numDomainID numeric(9)=0,              
 @keyWord varchar(100)=''                  
                    
as                  
                    
Select  ADC.vcEmail        
  FROM AdditionalContactsInformation ADC INNER JOIN                      
                      DivisionMaster DM ON ADC.numDivisionId = DM.numDivisionID INNER JOIN                      
                      CompanyInfo C ON DM.numCompanyID = C.numCompanyId                    
        where vcEmail<>'' 
         AND ADC.numDomainID= convert(varchar(10),@numDomainID)                  
        and (ADC.vcFirstName  like ''+@keyWord+'%'                     
    or ADC.vcLastName like ''+@keyWord+'%'                    
    or c.vcCompanyName like ''+@keyWord+'%')
GO
