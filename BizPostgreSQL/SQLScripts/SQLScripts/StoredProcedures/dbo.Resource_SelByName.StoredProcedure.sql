/****** Object:  StoredProcedure [dbo].[Resource_SelByName]    Script Date: 07/26/2008 16:14:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*  
**  Selects a single Resource by their Name.  
**  
**  This stored procedure can be used to retrieve the ID key for a Resource from their user-friendly Name.  
*/  
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='resource_selbyname')
DROP PROCEDURE resource_selbyname
GO
CREATE PROCEDURE [dbo].[Resource_SelByName]   
 @ResourceName nvarchar(64) -- name of the resource.  
AS  
BEGIN  
 SELECT  
  [Resource].[ResourceID],   
  [Resource].[ResourceName],   
  [Resource].[ResourceDescription],  
  [Resource].[EmailAddress],  
  ISNULL([ResourcePreference].[EnableEmailReminders],0) AS EnableEmailReminders,  
  [Resource].[_ts]  
 FROM   
  [Resource] LEFT JOIN [ResourcePreference] ON [Resource].[ResourceID] = [ResourcePreference].[ResourceID]  
 WHERE  
  ( [Resource].[ResourceID] = convert(numeric(9),@ResourceName) );  
END
GO
