/****** Object:  StoredProcedure [dbo].[USP_ManageFavorites]    Script Date: 07/26/2008 16:19:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_managefavorites')
DROP PROCEDURE usp_managefavorites
GO
CREATE PROCEDURE [dbo].[USP_ManageFavorites]    
@byteMode as tinyint=0,    
@numContactID as numeric(9)=0,    
@numUserCntID as numeric(9)=0,  
@cType as char(1)    
as    
    
if @byteMode=0    
begin    
delete from Favorites where numUserCntID=@numUserCntID and  numContactID=@numContactID    
insert  into Favorites (numUserCntID,numContactID,cType) values (@numUserCntID,@numContactID,@cType)    
end    
    
if @byteMode=1    
begin    
delete from Favorites where numUserCntID=@numUserCntID and  numContactID=@numContactID    
end
GO
