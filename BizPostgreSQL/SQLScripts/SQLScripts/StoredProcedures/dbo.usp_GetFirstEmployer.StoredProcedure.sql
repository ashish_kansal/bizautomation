/****** Object:  StoredProcedure [dbo].[usp_GetFirstEmployer]    Script Date: 07/26/2008 16:17:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
--This will return the first employer record for the company
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getfirstemployer')
DROP PROCEDURE usp_getfirstemployer
GO
CREATE PROCEDURE [dbo].[usp_GetFirstEmployer]
	@numDivId numeric=0   
--
AS
	SELECT vcFirstname,vcLastName,numcontactId FROM additionalcontactsinformation WHERE numContactType=92 and numdivisionId=@numDivId
GO
