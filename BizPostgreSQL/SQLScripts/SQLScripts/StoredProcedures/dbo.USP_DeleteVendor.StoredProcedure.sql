/****** Object:  StoredProcedure [dbo].[USP_DeleteVendor]    Script Date: 07/26/2008 16:15:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_deletevendor')
DROP PROCEDURE usp_deletevendor
GO
CREATE PROCEDURE [dbo].[USP_DeleteVendor]
@numVendorID as numeric(9),
@numItemCode AS NUMERIC(9),
@numDomainID as numeric(9)
as
delete from Vendor 
where numVendorID=@numVendorID 
AND numItemCode =@numItemCode
and numDomainID=@numDomainID;

IF exists(SELECT * FROM item WHERE numItemCode=@numItemCode AND numVendorID=@numVendorID AND numDomainID=@numDomainID)
BEGIN
	UPDATE item SET numVendorID = NULL WHERE numItemCode=@numItemCode AND numDomainID=@numDomainID	
END

GO
