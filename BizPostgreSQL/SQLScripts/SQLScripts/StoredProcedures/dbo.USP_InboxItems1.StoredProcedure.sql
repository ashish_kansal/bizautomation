/****** Object:  StoredProcedure [dbo].[USP_InboxItems1]    Script Date: 07/26/2008 16:18:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_inboxitems1')
DROP PROCEDURE usp_inboxitems1
GO
CREATE PROCEDURE [dbo].[USP_InboxItems1]              
    @PageSize [int] = -1,    
    @CurrentPage [int] = -1,    
@srchSubjectBody as varchar(100) = '',    
@srchEmail as varchar(100) ='',    
@srchAttachmenttype as varchar(100) ='',   
@columnName as Varchar(50),                                                                      
@columnSortOrder as Varchar(10)='desc' ,  
@TotRecs int output    
 
         
as

 declare @firstRec as integer                                                    
  declare @lastRec as integer                                                    
 set @firstRec= (@CurrentPage-1) * @PageSize                                                    
 set @lastRec= (@CurrentPage*@PageSize+1)                                                     


       
declare @strSql as varchar(8000)              
set  @strSql= ' select  FromName,FromEmail , ToEmail ,  CCEmail ,BCCEmail , numEmailHstrID  ,[count],Subject,Body,created,ChangeKey,IsRead,Size,HasAttachments
,AttachmentType,sent,type,Source,Category from (
select  isnull(vcMessageFrom,'''') as FromName,      
isnull(vcFromEmail,'''') as FromEmail,              
dbo.GetEmaillAdd(emailHistory.numEmailHstrID,1) as ToEmail,         
dbo.GetEmaillAdd(emailHistory.numEmailHstrID,2) as CCEmail,        
dbo.GetEmaillAdd(emailHistory.numEmailHstrID,3) as BCCEmail,             
emailHistory.numEmailHstrID,      
ROW_NUMBER() OVER (ORDER BY emailHistory.numEmailHstrID ASC) AS  [count],      
isnull(vcSubject,'''') as Subject,              
isnull(vcBody,'''') as Body,      
isnull(bintCreatedOn,getutcdate()) as created,      
isnull(vcItemId,'''') as ItemId,      
isnull(vcChangeKey,'''') as ChangeKey,      
isnull(bitIsRead,''False'') as IsRead,      
isnull(vcSize,0) as Size,      
isnull(bitHasAttachments,''False'') as HasAttachments,      
isnull(EmailHstrAttchDtls.vcFileName,'''') as AttachmentPath,    
isnull(EmailHstrAttchDtls.vcAttachmentType,'''') as AttachmentType,    
isnull(dtReceivedOn,getutcdate()) as sent,      
isnull(tintType,1) as type,      
isnull(chrSource,''B'') as Source,  
isnull(vcCategory,''white'') as Category    
      

    
from emailHistory        
left join EmailHstrAttchDtls on emailHistory.numEmailHstrID=EmailHstrAttchDtls.numEmailHstrID    
where tinttype=1 
 )   as i
 WHERE [count] > '+convert(varchar(200),@firstRec)+' and [count] <'+convert(varchar(200), @lastRec)+
' order by '+ convert(varchar(100),@columnName) +' '+@columnSortOrder

print(@strSql)
   exec(@strSql)
GO
