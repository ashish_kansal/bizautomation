/****** Object:  StoredProcedure [dbo].[USP_CFWDtlFieldDelete]    Script Date: 07/26/2008 16:15:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_cfwdtlfielddelete')
DROP PROCEDURE usp_cfwdtlfielddelete
GO
CREATE PROCEDURE [dbo].[USP_CFWDtlFieldDelete]
@numFieldDtlID as numeric(9)=null
as

delete from CFW_Fld_Dtl where numFieldDtlID=@numFieldDtlID
GO
