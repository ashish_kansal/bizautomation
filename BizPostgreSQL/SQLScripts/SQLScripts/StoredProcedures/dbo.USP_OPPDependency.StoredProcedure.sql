/****** Object:  StoredProcedure [dbo].[USP_OPPDependency]    Script Date: 07/26/2008 16:20:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_oppdependency')
DROP PROCEDURE usp_oppdependency
GO
CREATE PROCEDURE [dbo].[USP_OPPDependency]
(
@byteMode as tinyint=null,
@ProcessId as numeric(9)=null,
@Percentage as numeric(9)=null
)
as

if @byteMode= 0 
begin

	select numOppId,vcPOppName from OpportunityMaster
end
----OppMilestone
if @byteMode= 1 
begin

	
	select dtl.numstagepercentage,'Milestone - ' + convert(varchar(4),dtl.numstagepercentage) +'%' as numstagepercentageText from OpportunityStageDetails dtl
	where  dtl.numstagepercentage!=100 and dtl.numstagepercentage!=0 and dtl.numOppId=@ProcessId
	group by dtl.numstagepercentage



end

--OppStageDetails
if @byteMode= 2 
begin

select numOppStageId,vcstagedetail from OpportunityStageDetails where numoppid=@ProcessId and numstagepercentage=@Percentage

end

---ProjectMilestone
if @byteMode= 3 
begin

	
	select dtl.numstagepercentage,'Milestone - ' + convert(varchar(4),dtl.numstagepercentage) +'%' as numstagepercentageText from ProjectsStageDetails dtl
	where  dtl.numstagepercentage!=100 and dtl.numstagepercentage!=0 and dtl.numProId=@ProcessId
	group by dtl.numstagepercentage



end

--ProStageDetails
if @byteMode= 4 
begin

select numProStageId as numOppStageId,vcstagedetail from ProjectsStageDetails where numProid=@ProcessId and numstagepercentage=@Percentage

end
GO
