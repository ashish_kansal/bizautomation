/****** Object:  StoredProcedure [dbo].[USP_GetCheckNumber]    Script Date: 07/26/2008 16:16:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created By Siva    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getchecknumber')
DROP PROCEDURE usp_getchecknumber
GO
CREATE PROCEDURE  [dbo].[USP_GetCheckNumber]    
@numDomainId as numeric(9)=0    
As    
Begin    
 Select isnull(numCheckNo,0) +1 as numCheckNo,numBankRoutingNo as numBankRoutingNo,numBankAccountNo as numBankAccountNo from CheckDetails Where numDomainId=@numDomainId And numCheckId=(Select max(isnull(numCheckId,0)) from  CheckDetails Where numDomainId=@numDomainId)  


  
End
GO
