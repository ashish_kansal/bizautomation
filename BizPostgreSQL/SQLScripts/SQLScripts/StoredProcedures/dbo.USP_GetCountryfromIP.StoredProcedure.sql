/****** Object:  StoredProcedure [dbo].[USP_GetCountryfromIP]    Script Date: 07/26/2008 16:17:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getcountryfromip')
DROP PROCEDURE usp_getcountryfromip
GO
CREATE PROCEDURE [dbo].[USP_GetCountryfromIP]
@IPNo as bigint=0
as

select ConName from IPToCountry where IPFrom<=@IPNo and IPTo>=@IPNo
GO
