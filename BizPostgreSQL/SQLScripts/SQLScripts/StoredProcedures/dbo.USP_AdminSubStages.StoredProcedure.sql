/****** Object:  StoredProcedure [dbo].[USP_AdminSubStages]    Script Date: 07/26/2008 16:14:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_adminsubstages')
DROP PROCEDURE usp_adminsubstages
GO
CREATE PROCEDURE [dbo].[USP_AdminSubStages]
(
@byteMode as tinyint=null,
@ProcessID as numeric(9)=null,
@strSubStages as text='',
@strSubStageName as varchar(100),
@SubStageID as numeric(9) =null output
)
as
if @byteMode=0
begin

	select 	numSubstageDtlid,
		numProcessListID,
		numSubStageHDrId,
		vcSubStageElemt  from SubStageDetails
	where  	numSubStageHDrId=@SubStageID
end

if @byteMode=1
begin
	declare @hDoc  int

	declare @StageID as numeric(9)
if @SubStageID= 0
begin
	insert into SubStageHDR 
		(
			numProcessId,
			vcSubStageName
		)
		values (@ProcessID,@strSubStageName)
		set @StageID=@@identity
		set @SubStageID=@@identity
end
else
begin
		update SubStageHDR
			set vcSubStageName=@strSubStageName
		where numSubStageHdrID=@SubStageID
		set @StageID=@SubStageID
			
end
	

	EXEC sp_xml_preparedocument @hDoc OUTPUT, @strSubStages
	
	delete from SubStageDetails where numProcessListID=@ProcessID and numSubStageHdrID=@StageID
	insert into SubStageDetails
		(numSubStageHdrID,numProcessListID,vcSubStageElemt)

	select @StageID,@ProcessID,X.*  from(SELECT *FROM OPENXML (@hDoc,'/NewDataSet/Table1',2)
	WITH 	(
		vcSubStageElemt varchar(100)
		))X


	insert into SubStageDetails
		(numSubStageHdrID,numProcessListID,vcSubStageElemt)

	select @StageID,@ProcessID,X.*  from(SELECT *FROM OPENXML (@hDoc,'/NewDataSet/Table',2)
	WITH 	(
		vcSubStageElemt varchar(100)
		))X

	EXEC sp_xml_removedocument @hDoc
end
GO
