/****** Object:  StoredProcedure [dbo].[Usp_SaveProjectOpportunities]    Script Date: 07/26/2008 16:21:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- exec Usp_SaveProjectOpportunities @numProjectId=197,@numDomainId=72,@numOppBizDocsID=0,@numBillID=1902
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_saveprojectopportunities')
DROP PROCEDURE usp_saveprojectopportunities
GO
CREATE PROCEDURE [dbo].[Usp_SaveProjectOpportunities]
    @numProjectId NUMERIC,
    @numDomainId NUMERIC,
    @numOppID NUMERIC(9),
    @numBillID NUMERIC(9) = 0,
    @numStageID NUMERIC(9) = 0
AS 
    BEGIN
		IF @numStageID = 0 
		SET @numStageID = NULL 
    
        IF @numBillID > 0 
            BEGIN
                INSERT  INTO [ProjectsOpportunities]
                        (
                          [numProId],
                          [numOppId],
                          [numDomainId],
                          [numOppBizDocID],
                          numBillID,
                          numStageID
                                
                        )
                VALUES  (
                          @numProjectId,
                          NULL,
                          @numDomainId,
                          NULL,
                          @numBillID,
                          @numStageID
                        ) 
            END
    else if @numOppId>0
		BEGIN
			INSERT  INTO [ProjectsOpportunities]
                                        (
                                          [numProId],
                                          [numOppId],
                                          [numDomainId],
                                          [numOppBizDocID],
                                          numBillID,
                                          numStageID
                                        )
                                VALUES  (
                                          @numProjectId,
                                          @numOppID,
                                          @numDomainId,
                                          NULL,
                                          NULL,
                                          @numStageID
                                        ) 
        END
  END

--        IF @numOppBizDocsID > 0 
--            BEGIN
--                DECLARE @numOppID NUMERIC(9)
--                SELECT  @numOppID = numOppID
--                FROM    [OpportunityBizDocs]
--                WHERE   [numOppBizDocsId] = @numOppBizDocsID
--        
--        
--                IF @numOppID > 0 
--                    BEGIN
--                        IF ( SELECT COUNT(*)
--                             FROM   [ProjectsOpportunities]
--                             WHERE  [numOppBizDocID] = @numOppBizDocsID
--                                    AND [numProId] = @numProjectId AND (numStageID = @numStageID OR @numStageID IS NULL)
--                           ) = 0 
--                            BEGIN
--                                INSERT  INTO [ProjectsOpportunities]
--                                        (
--                                          [numProId],
--                                          [numOppId],
--                                          [numDomainId],
--                                          [numOppBizDocID],
--                                          numBillID,
--                                          numStageID
--                                        )
--                                VALUES  (
--                                          @numProjectId,
--                                          @numOppID,
--                                          @numDomainId,
--                                          @numOppBizDocsID,
--                                          NULL,
--                                          @numStageID
--                                        ) 
--                            END
--                
-- 	
--                    END
--            END
    
          

--    END
