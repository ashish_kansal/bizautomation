/****** Object:  StoredProcedure [dbo].[USP_GetAuthorizativeBizDocsFromOpportunity]    Script Date: 07/26/2008 16:16:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created By Siva      
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getauthorizativebizdocsfromopportunity')
DROP PROCEDURE usp_getauthorizativebizdocsfromopportunity
GO
CREATE PROCEDURE [dbo].[USP_GetAuthorizativeBizDocsFromOpportunity]    
(@numAuthorizativeSalesId as numeric(9)=0,    
@numAuthorizativePurchaseId as numeric(9)=0,     
@numDomainId as numeric(9)=0,    
@numCountSalesBizDocs as numeric(9)=0 Output,    
@numCountPurchaseBizDocs as numeric(9)=0 Output 
)    
As    
Begin    
 Select @numCountSalesBizDocs=Count(*) From OpportunityMaster OM    
 Inner Join OpportunityBizdocs OppBizDocs On OM.numOppId=OppBizDocs.numOppId     
 Where OppBizDocs.numBizDocId=@numAuthorizativeSalesId And OM.numDomainId=@numDomainId And  isnull(OppBizDocs.bitAuthoritativeBizDocs,0)=1    
    
 Select @numCountPurchaseBizDocs=Count(*) From OpportunityMaster OM    
 Inner Join OpportunityBizdocs OppBizDocs On OM.numOppId=OppBizDocs.numOppId     
 Where OppBizDocs.numBizDocId=@numAuthorizativePurchaseId And OM.numDomainId=@numDomainId And  isnull(OppBizDocs.bitAuthoritativeBizDocs,0)=1    
  
End
GO
