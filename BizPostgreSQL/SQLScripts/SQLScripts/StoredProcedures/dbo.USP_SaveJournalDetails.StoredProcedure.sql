/****** Object:  StoredProcedure [dbo].[USP_SaveJournalDetails]    Script Date: 07/26/2008 16:21:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Create by Siva                                                                                        
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_SaveJournalDetails' ) 
    DROP PROCEDURE USP_SaveJournalDetails
GO
CREATE PROCEDURE [dbo].[USP_SaveJournalDetails]
    @numJournalId AS NUMERIC(9) = 0 ,
    @strRow AS TEXT = '' ,
    @Mode AS TINYINT = 0 ,
    @numDomainId AS NUMERIC(9) = 0   
AS 
BEGIN                   
BEGIN TRY 
	-- INCLUDE ENCODING OTHER WISE IT WILL THROW ERROR WHEN SPECIAL CHARACTERS ARE AVAILABLE IN DESCRIPT FIELDS
	IF ISNULL(CAST(@strRow AS VARCHAR),'') <> '' AND CHARINDEX('<?xml',@strRow) = 0
	BEGIN
		SET @strRow = CONCAT('<?xml version="1.0" encoding="ISO-8859-1"?>',@strRow)
	END

    BEGIN TRAN  
        
    ---Always set default Currency ID as base currency ID of domain, and Rate =1
    DECLARE @numBaseCurrencyID NUMERIC
    SELECT @numBaseCurrencyID = ISNULL(numCurrencyID,0) FROM dbo.Domain WHERE numDomainId=@numDomainId
        
    IF CONVERT(VARCHAR(100), @strRow) <> '' 
    BEGIN
        DECLARE @hDoc3 INT                                                                                                                                                                
        EXEC sp_xml_preparedocument @hDoc3 OUTPUT, @strRow

		DECLARE @TEMP TABLE
		(
			numTransactionId numeric(10,0)
			,numDebitAmt FLOAT
			,numCreditAmt FLOAT
			,numChartAcntId numeric(18, 0)
			,varDescription varchar(1000)
			,numCustomerId numeric(18, 0)
			,bitMainDeposit bit
			,bitMainCheck bit
			,bitMainCashCredit bit
			,numoppitemtCode numeric(18, 0)
			,chBizDocItems nchar(10)
			,vcReference varchar(500)
			,numPaymentMethod numeric(18, 0)
			,bitReconcile bit
			,numCurrencyID numeric(18, 0)
			,fltExchangeRate float
			,numTaxItemID numeric(18, 0)
			,numBizDocsPaymentDetailsId numeric(18, 0)
			,numcontactid numeric(9, 0)
			,numItemID numeric(18, 0)
			,numProjectID numeric(18, 0)
			,numClassID numeric(18, 0)
			,numCommissionID numeric(9, 0)
			,numReconcileID numeric(18, 0)
			,bitCleared bit
			,tintReferenceType tinyint
			,numReferenceID numeric(18, 0)
			,numCampaignID NUMERIC(18,0)
		)

		INSERT INTO @TEMP
		(
			numTransactionId
			,numDebitAmt
			,numCreditAmt
			,numChartAcntId
			,varDescription
			,numCustomerId
			,bitMainDeposit
			,bitMainCheck
			,bitMainCashCredit
			,numoppitemtCode
			,chBizDocItems
			,vcReference
			,numPaymentMethod
			,bitReconcile
			,numCurrencyID
			,fltExchangeRate
			,numTaxItemID
			,numBizDocsPaymentDetailsId
			,numcontactid
			,numItemID
			,numProjectID
			,numClassID
			,numCommissionID
			,numReconcileID
			,bitCleared
			,tintReferenceType
			,numReferenceID
			,numCampaignID 
		)
        SELECT 
			numTransactionId
			,numDebitAmt
			,numCreditAmt
			,numChartAcntId
			,varDescription
			,numCustomerId
			,bitMainDeposit
			,bitMainCheck
			,bitMainCashCredit
			,numoppitemtCode
			,chBizDocItems
			,vcReference
			,numPaymentMethod
			,bitReconcile
			,ISNULL(NULLIF(numCurrencyID,0),@numBaseCurrencyID) AS numCurrencyID
			,ISNULL(NULLIF(fltExchangeRate,0.0000),1) AS fltExchangeRate
			,numTaxItemID
			,numBizDocsPaymentDetailsId
			,numcontactid
			,numItemID
			,numProjectID
			,numClassID
			,numCommissionID
			,numReconcileID
			,bitCleared
			,tintReferenceType
			,numReferenceID
			,numCampaignID 
		FROM 
			OPENXML (@hdoc3,'/ArrayOfJournalEntryNew/JournalEntryNew',2)
		WITH 
		(
			numTransactionId numeric(10,0)
			,numDebitAmt FLOAT
			,numCreditAmt FLOAT
			,numChartAcntId numeric(18, 0)
			,varDescription varchar(1000)
			,numCustomerId numeric(18, 0)
			,bitMainDeposit bit
			,bitMainCheck bit
			,bitMainCashCredit bit
			,numoppitemtCode numeric(18, 0)
			,chBizDocItems nchar(10)
			,vcReference varchar(500)
			,numPaymentMethod numeric(18, 0)
			,bitReconcile bit
			,numCurrencyID numeric(18, 0)
			,fltExchangeRate float
			,numTaxItemID numeric(18, 0)
			,numBizDocsPaymentDetailsId numeric(18, 0)
			,numcontactid numeric(9, 0)
			,numItemID numeric(18, 0)
			,numProjectID numeric(18, 0)
			,numClassID numeric(18, 0)
			,numCommissionID numeric(9, 0)
			,numReconcileID numeric(18, 0)
			,bitCleared bit
			,tintReferenceType tinyint
			,numReferenceID numeric(18, 0)
			,numCampaignID NUMERIC(18,0)
		)

        EXEC sp_xml_removedocument @hDoc3 
                                    

/*-----------------------Validation of balancing entry*/

		IF EXISTS(SELECT * FROM @TEMP)
		BEGIN
			DECLARE @SumTotal DECIMAL(20,5) 
			SELECT @SumTotal = CAST(SUM(numDebitAmt) AS DECIMAL(20,5))-CAST(SUM(numCreditAmt) AS DECIMAL(20,5)) FROM @TEMP

			IF cast( @SumTotal AS DECIMAL(10,4)) <> 0.0000
			BEGIN
				 INSERT INTO [dbo].[GenealEntryAudit]
				   ([numDomainID]
				   ,[numJournalID]
				   ,[numTransactionID]
				   ,[dtCreatedDate]
				   ,[varDescription])
				 SELECT @numDomainID,
						@numJournalId,
						0,
						GETUTCDATE(),
						'IM_BALANCE'

				 RAISERROR ( 'IM_BALANCE', 16, 1 ) ;
				 RETURN;
			END
		END



		DECLARE @numEntryDateSortOrder AS NUMERIC
		DECLARE @datEntry_Date AS DATETIME
		--Combine Date from datentryDate with time part of current time
		SELECT @datEntry_Date=( CAST( CONVERT(VARCHAR,DATEPART(year, datEntry_Date)) + '-' + RIGHT('00000'+ CONVERT(VARCHAR,DATEPART(month, datEntry_Date)),2)+ 
		+ '-' + CONVERT(VARCHAR,DATEPART(day, datEntry_Date))
		+ ' ' + CONVERT(VARCHAR,DATEPART(hour, GETDATE()))
		+ ':' + CONVERT(VARCHAR,DATEPART(minute, GETDATE()))
		+ ':' + CONVERT(VARCHAR,DATEPART(second, GETDATE())) AS DATETIME)  
		) 
		FROM 
			dbo.General_Journal_Header
		WHERE 
			numJournal_Id=@numJournalId 
			AND numDomainId=@numDomainId

		SET @numEntryDateSortOrder = convert(numeric,
		RIGHT('00000'+ CONVERT(VARCHAR,DATEPART(year, @datEntry_Date)),4)+
		RIGHT('00000'+ CONVERT(VARCHAR,DATEPART(month, @datEntry_Date)),2)+
		RIGHT('00000'+ CONVERT(VARCHAR,DATEPART(day, @datEntry_Date)),2)+
		RIGHT('00000'+ CONVERT(VARCHAR,DATEPART(hour, @datEntry_Date)),2)+
		RIGHT('00000'+ CONVERT(VARCHAR,DATEPART(minute, @datEntry_Date)),2)+
		RIGHT('00000'+ CONVERT(VARCHAR,DATEPART(second, @datEntry_Date)),2)+
		RIGHT('00000'+ CONVERT(VARCHAR,DATEPART(millisecond, @datEntry_Date)),3))

		/*-----------------------Validation of balancing entry*/

			

        IF @Mode = 1  /*Edit Journal entries*/
        BEGIN
			PRINT '---delete removed transactions ---'
            DELETE GJD FROM General_Journal_Details GJD LEFT JOIN @TEMP T1 ON GJD.numTransactionId=T1.numTransactionId WHERE numJournalId = @numJournalId AND T1.numTransactionId IS NULL
			                                                           
			--Update transactions
			PRINT '---updated existing transactions ---'
            UPDATE  dbo.General_Journal_Details 
            SET    		[numDebitAmt] = X.numDebitAmt,
						[numCreditAmt] = X.numCreditAmt,
						[numChartAcntId] = X.numChartAcntId,
						[varDescription] = X.varDescription,
						[numCustomerId] = NULLIF(X.numCustomerId,0),
						[bitMainDeposit] = X.bitMainDeposit,
						[bitMainCheck] = X.bitMainCheck,
						[bitMainCashCredit] = X.bitMainCashCredit,
						[numoppitemtCode] = NULLIF(X.numoppitemtCode,0),
						[chBizDocItems] = X.chBizDocItems,
						[vcReference] = X.vcReference,
						[numPaymentMethod] = X.numPaymentMethod,
						[numCurrencyID] = NULLIF(X.numCurrencyID,0),
						[fltExchangeRate] = X.fltExchangeRate,
						[numTaxItemID] = NULLIF(X.numTaxItemID,0),
						[numBizDocsPaymentDetailsId] = NULLIF(X.numBizDocsPaymentDetailsId,0),
						[numcontactid] = NULLIF(X.numcontactid,0),
						[numItemID] = NULLIF(X.numItemID,0),
						[numProjectID] =NULLIF( X.numProjectID,0),
						[numClassID] = NULLIF(X.numClassID,0),
						[numCommissionID] = NULLIF(X.numCommissionID,0),
						numCampaignID=NULLIF(X.numCampaignID,0),
						numEntryDateSortOrder1=@numEntryDateSortOrder
--									[tintReferenceType] = X.tintReferenceType,
--									[numReferenceID] = X.numReferenceID
                FROM    @TEMP as X 
				WHERE   X.numTransactionId = General_Journal_Details.numTransactionId
			               
		               
			INSERT INTO [dbo].[GenealEntryAudit]
						([numDomainID]
						,[numJournalID]
						,[numTransactionID]
						,[dtCreatedDate]
						,[varDescription])
			SELECT GJD.numDomainID, GJD.[numJournalId], GJD.[numTransactionId], GETUTCDATE(),[GJD].[varDescription]
			FROM General_Journal_Details AS GJD                                             
			JOIN @TEMP AS X ON  X.numTransactionId = GJD.numTransactionId
						
        END 
        IF @Mode = 0 OR @Mode = 1 /*-- Insert journal only*/
        BEGIN
			PRINT '---insert existing transactions ---'                                                                                               
            INSERT  INTO [dbo].[General_Journal_Details]
            ( 
				[numJournalId] ,
                [numDebitAmt] ,
                [numCreditAmt] ,
                [numChartAcntId] ,
                [varDescription] ,
                [numCustomerId] ,
                [numDomainId] ,
                [bitMainDeposit] ,
                [bitMainCheck] ,
                [bitMainCashCredit] ,
                [numoppitemtCode] ,
                [chBizDocItems] ,
                [vcReference] ,
                [numPaymentMethod] ,
                [bitReconcile] ,
                [numCurrencyID] ,
                [fltExchangeRate] ,
                [numTaxItemID] ,
                [numBizDocsPaymentDetailsId] ,
                [numcontactid] ,
                [numItemID] ,
                [numProjectID] ,
                [numClassID] ,
                [numCommissionID] ,
                [numReconcileID] ,
                [bitCleared],
                [tintReferenceType],
				[numReferenceID],[numCampaignID],numEntryDateSortOrder1
	                                
            )
            SELECT  
				@numJournalId ,
                [numDebitAmt] ,
                [numCreditAmt] ,
                CASE WHEN [numChartAcntId]=0 THEN NULL ELSE numChartAcntId END  ,
                [varDescription] ,
                NULLIF(numCustomerId, 0) ,
                @numDomainId ,
                [bitMainDeposit] ,
                [bitMainCheck] ,
                [bitMainCashCredit] ,
                NULLIF([numoppitemtCode], 0) ,
                [chBizDocItems] ,
                [vcReference] ,
                [numPaymentMethod] ,
                [bitReconcile] ,
                NULLIF([numCurrencyID], 0) ,
                [fltExchangeRate] ,
                NULLIF([numTaxItemID], 0) ,
                NULLIF([numBizDocsPaymentDetailsId], 0) ,
                NULLIF([numcontactid], 0) ,
                NULLIF([numItemID], 0) ,
                NULLIF([numProjectID], 0) ,
                NULLIF([numClassID], 0) ,
                NULLIF([numCommissionID], 0) ,
                NULLIF([numReconcileID], 0) ,
                [bitCleared],
                tintReferenceType,
				NULLIF(numReferenceID,0),NULLIF(numCampaignID,0),@numEntryDateSortOrder                  
            FROM 
				@TEMP as X    
            WHERE 
				X.numTransactionId =0

			--Set Default Class If enable User Level Class Accountng 
			DECLARE @numAccountClass AS NUMERIC(18)=0                             
            SELECT @numAccountClass=ISNULL(numAccountClass,0) FROM General_Journal_Header WHERE numDomainId=@numDomainId AND numJournal_Id=@numJournalId

			If ISNULL((SELECT TOP 1 numClassID FROM General_Journal_Details WHERE numJournalId=@numJournalId AND numDomainId=@numDomainId AND ISNULL(numClassID,0) > 0),0) > 0 
				AND ISNULL(@numAccountClass,0) <> ISNULL((SELECT TOP 1 numClassID FROM General_Journal_Details WHERE numJournalId=@numJournalId AND numDomainId=@numDomainId AND ISNULL(numClassID,0) > 0),0)
			BEGIN
				SET @numAccountClass = ISNULL((SELECT TOP 1 numClassID FROM General_Journal_Details WHERE numJournalId=@numJournalId AND numDomainId=@numDomainId AND ISNULL(numClassID,0) > 0),0)
			END

            IF @numAccountClass > 0
            BEGIN
				UPDATE General_Journal_Details SET numClassID=@numAccountClass WHERE numJournalId=@numJournalId AND numDomainId=@numDomainId AND ISNULL(numClassID,0) = 0
			END

			INSERT INTO [dbo].[GenealEntryAudit]
						([numDomainID]
						,[numJournalID]
						,[numTransactionID]
						,[dtCreatedDate]
						,[varDescription])
			SELECT @numDomainID, @numJournalId, X.numTransactionId, GETUTCDATE(),X.varDescription
			FROM @TEMP AS X WHERE X.numTransactionId = 0

        END 
                    
                    
        UPDATE 
			dbo.General_Journal_Header 
		SET 
			numAmount=(SELECT SUM(numDebitAmt) FROM dbo.General_Journal_Details WHERE numJournalID=@numJournalID)
		WHERE 
			numJournal_Id=@numJournalId AND numDomainId=@numDomainId
						
		SELECT 
			@SumTotal = CAST(SUM(numDebitAmt) AS DECIMAL(20,5))-CAST(SUM(numCreditAmt) AS DECIMAL(20,5))
		FROM 
			General_Journal_Details 
		WHERE 
			numJournalId=@numJournalId 
			AND numDomainId=@numDomainId

		IF cast( @SumTotal AS DECIMAL(10,4)) > 0.0001
		BEGIN
			INSERT INTO [dbo].[GenealEntryAudit]
			(
				[numDomainID]
				,[numJournalID]
				,[numTransactionID]
				,[dtCreatedDate]
				,[varDescription]
			)
			SELECT 
				@numDomainID,
				@numJournalId,
				0,
				GETUTCDATE(),
				'IM_BALANCE'

			RAISERROR ( 'IM_BALANCE', 16, 1 ) ;
			RETURN;
		END
                    
        -- Update leads,prospects to Accounts
		DECLARE @numDivisionID AS NUMERIC(18)
		DECLARE @numUserCntID AS NUMERIC(18)
					
		SELECT TOP 1 @numDivisionID = ISNULL(numCustomerId,0), @numUserCntID = ISNULL(numCreatedBy,0)
		FROM dbo.General_Journal_Details GJD
		JOIN dbo.General_Journal_Header GJH ON GJD.numJournalId = GJH.numJournal_Id AND GJD.numDomainId = GJH.numDomainId
		WHERE numJournalId = @numJournalId 
		AND GJH.numDomainId = @numDomainID
					
		IF @numDivisionID > 0 AND @numUserCntID > 0
		BEGIN
			EXEC dbo.USP_UpdateCRMTypeToAccounts @numDivisionID ,@numDomainID ,@numUserCntID
		END
    END
    ELSE IF @Mode = 1  /*Edit Journal entries*/    
	BEGIN
		DELETE FROM 
			General_Journal_Details
        WHERE  
			numJournalId = @numJournalId

		UPDATE 
			dbo.General_Journal_Header 
		SET 
			numAmount=ISNULL((SELECT SUM(numDebitAmt) FROM dbo.General_Journal_Details WHERE numJournalID=@numJournalID),0)
		WHERE 
			numJournal_Id=@numJournalId AND numDomainId=@numDomainId
						
		SELECT 
			@SumTotal = CAST(SUM(numDebitAmt) AS DECIMAL(20,5))-CAST(SUM(numCreditAmt) AS DECIMAL(20,5))
		FROM 
			General_Journal_Details 
		WHERE 
			numJournalId=@numJournalId 
			AND numDomainId=@numDomainId

		IF cast( @SumTotal AS DECIMAL(10,4)) > 0.0001
		BEGIN
			INSERT INTO [dbo].[GenealEntryAudit]
			(
				[numDomainID]
				,[numJournalID]
				,[numTransactionID]
				,[dtCreatedDate]
				,[varDescription]
			)
			SELECT 
				@numDomainID,
				@numJournalId,
				0,
				GETUTCDATE(),
				'IM_BALANCE'

			RAISERROR ( 'IM_BALANCE', 16, 1 ) ;
			RETURN;
		END
	END
	   
	DECLARE @TEMP1 TABLE
	(
		numTransactionId NUMERIC(18,0)
		,numEntryDateSortOrder1 NUMERIC(18,0)
		,NewSortId NUMERIC(18,0)
	)
	INSERT INTO @TEMP1
	(
		numTransactionId
		,numEntryDateSortOrder1
		,NewSortId
	)
	SELECT 
		GJD.numTransactionId
		,GJD.numEntryDateSortOrder1
		,Row_number() OVER(ORDER BY GJD.numTransactionId) AS NewSortId
	FROM 
		General_Journal_Details GJD 
	WHERE 
		GJD.numDomainId = @numDomainID 
		AND GJD.numEntryDateSortOrder1  LIKE SUBSTRING(CAST(@numEntryDateSortOrder AS VARCHAR(25)),1,12) + '%'

	UPDATE 
		GJD 
	SET 
		GJD.numEntryDateSortOrder1 = (Temp1.numEntryDateSortOrder1 + Temp1.NewSortId) 
	FROM 
		General_Journal_Details GJD 
	INNER JOIN 
		@TEMP1 AS Temp1 
	ON 
		GJD.numTransactionId = Temp1.numTransactionId

    COMMIT TRAN 
END TRY 
BEGIN CATCH		
    DECLARE @strMsg VARCHAR(200)
    SET @strMsg = ERROR_MESSAGE()
    IF ( @@TRANCOUNT > 0 ) 
        BEGIN
			INSERT INTO [dbo].[GenealEntryAudit]
								([numDomainID]
								,[numJournalID]
								,[numTransactionID]
								,[dtCreatedDate]
								,[varDescription])
					SELECT @numDomainID, @numJournalId,0, GETUTCDATE(),@strMsg

            RAISERROR ( @strMsg, 16, 1 ) ;
            ROLLBACK TRAN
            RETURN 1
        END
END CATCH	

            
--End                                              
---For Recurring Transaction                                              
                                              
--if @RecurringMode=1                                              
--Begin    
--  print 'SIVA--Recurring'    
--  print  '@numDomainId=============='+Convert(varchar(10),@numDomainId)                                     
--  Declare @numMaxJournalId as numeric(9)                                              
--  Select @numMaxJournalId=max(numJournal_Id) From General_Journal_Header Where numDomainId=@numDomainId                                              
--  insert into General_Journal_Details(numJournalId,numDebitAmt,numCreditAmt,numChartAcntId,varDescription,numCustomerId,numDomainId,bitMainCheck,bitMainCashCredit)                                              
--  Select @numMaxJournalId,numDebitAmt,numCreditAmt,numChartAcntId,varDescription,NULLIF(numCustomerId,0),numDomainId,bitMainCheck,bitMainCashCredit from General_Journal_Details Where numJournalId=@numJournalId  And numDomainId=@numDomainId                         
--   
--                   
--/*Commented by chintan Opening balance Will be updated by Trigger*/
----Exec USP_UpdateChartAcntOpnBalanceForJournalEntry @JournalId=@numMaxJournalId,@numDomainId=@numDomainId                                              
--End           
END
GO
