/****** Object:  StoredProcedure [dbo].[USP_AddContactItemId]    Script Date: 07/26/2008 16:14:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by gangadhar

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_addcontactitemid')
DROP PROCEDURE usp_addcontactitemid
GO
CREATE PROCEDURE [dbo].[USP_AddContactItemId]
@ItemId as varchar(250),
@changeKey as varchar(250),
@ContactId as numeric

as 

update AdditionalContactsInformation set vcitemid = @itemId ,vcChangeKey = @ChangeKey where numContactId = @ContactId
GO
