
Go
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*  
**  Selects the set of Recurrences applicable over a given date range for  
**  recurring activities with the given primary Resource.  
**  
**  Use this stored procedure to access the set of Recurrences belonging  
**  to a set of Activities selected with Activity_SelByDate and the same  
**  arguments.  
*/  
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='recurrence_sel')
DROP PROCEDURE recurrence_sel
GO
CREATE PROCEDURE [dbo].[Recurrence_Sel]  
 @StartDateTimeUtc datetime, -- the start date before which no activities are retrieved  
 @EndDateTimeUtc datetime, -- the end date, any activities starting after this date are excluded  
 @OrganizerName nvarchar(64) -- resource name (will be used to lookup ID)  
AS  
BEGIN  
 --  
 -- First step is to get the ResourceID for the primary resource name supplied.  
 --  
 DECLARE @ResourceID AS integer;
if isnumeric(@OrganizerName) =1
begin
   set @ResourceID=convert(numeric(9), @OrganizerName)
end
else
begin 
	set @ResourceID =-999	
end  

-- SELECT  
--  @ResourceID = [Resource].[ResourceID]  
-- FROM  
--  Resource  
-- WHERE  
--  ( [Resource].[ResourceName] = @OrganizerName );  
 --  
 -- Second step is to retrieve the Recurrences for root activities organized  
 -- (or owned by) this resource.  
 --  
 SELECT   
  [Recurrence].[RecurrenceID],  
  [Recurrence].[EndDateUtc],  
  [Recurrence].[DayOfWeekMaskUtc],  
  [Recurrence].[UtcOffset],  
  [Recurrence].[DayOfMonth],  
  [Recurrence].[MonthOfYear],  
  [Recurrence].[PeriodMultiple],  
  [Recurrence].[Period],  
  [Recurrence].[EditType],  
  ISNULL([Recurrence].[LastReminderDateTimeUtc],'1900-01-01 00:00:00') AS LastReminderDateTimeUtc,  
  [Recurrence].[_ts]  
 FROM   
  [Recurrence] INNER JOIN ( [Activity] INNER JOIN ( [Resource] INNER JOIN [ActivityResource]
 ON [Resource].[ResourceID] = [ActivityResource].[ResourceID] ) ON [Activity].[ActivityID] = [ActivityResource].[ActivityID] )
 ON [Activity].[RecurrenceID] = [Recurrence].[RecurrenceID]  
 WHERE   
  ( ( [Resource].[ResourceID] = @ResourceID )  
   AND ( [Activity].[StartDateTimeUtc] <= @EndDateTimeUtc )  
   AND ( [Recurrence].[EndDateUtc] > @StartDateTimeUtc )  
   AND ( [Activity].[OriginalStartDateTimeUtc] IS NULL ) ) and
[Recurrence].[RecurrenceID]>0;  
END
GO
