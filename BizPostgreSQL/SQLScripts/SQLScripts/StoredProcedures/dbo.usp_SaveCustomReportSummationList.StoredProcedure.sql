/****** Object:  StoredProcedure [dbo].[usp_SaveCustomReportSummationList]    Script Date: 07/26/2008 16:21:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_savecustomreportsummationlist')
DROP PROCEDURE usp_savecustomreportsummationlist
GO
CREATE PROCEDURE [dbo].[usp_SaveCustomReportSummationList]    
@ReportId as numeric(9)=0,    
@strSummationList as text=''    
as    
 delete CustReportSummationlist where numCustomReportId =@ReportId    
 declare @hDoc  int                  
    
   EXEC sp_xml_preparedocument @hDoc OUTPUT, @strSummationList        
                     
    insert into CustReportSummationlist                  
     (numCustomReportId,vcFieldName,bitSum,bitAvg,bitMax,bitMin)               
                      
    select @ReportId,    
     X.*   from(SELECT * FROM OPENXML (@hDoc,'/NewDataSet/Table',2)                  
    WITH  (                  
	vcField varchar(200),      
    bitSum bit,  
	bitAvg bit,  
	bitMax bit,  
	bitMin bit  
 ))X     
    EXEC sp_xml_removedocument @hDoc
GO
