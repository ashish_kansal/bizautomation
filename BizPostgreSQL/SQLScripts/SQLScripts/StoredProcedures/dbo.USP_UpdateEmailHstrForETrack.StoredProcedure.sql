/****** Object:  StoredProcedure [dbo].[USP_UpdateEmailHstrForETrack]    Script Date: 07/26/2008 16:21:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_updateemailhstrforetrack')
DROP PROCEDURE usp_updateemailhstrforetrack
GO
CREATE PROCEDURE [dbo].[USP_UpdateEmailHstrForETrack]
@numEmailHstrID as numeric(9)=0
as

update EmailHistory set numNoofTimes=(isnull(numNoofTimes,0)+1) where numEmailHstrID=@numEmailHstrID
GO
