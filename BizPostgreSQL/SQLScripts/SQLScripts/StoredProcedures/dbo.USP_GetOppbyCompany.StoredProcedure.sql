/****** Object:  StoredProcedure [dbo].[USP_GetOppbyCompany]    Script Date: 07/26/2008 16:17:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getoppbycompany')
DROP PROCEDURE usp_getoppbycompany
GO
CREATE PROCEDURE [dbo].[USP_GetOppbyCompany]
@numDivisionID as numeric(9)=0
as

select numOppID,vcPoppName from OpportunityMaster where numDivisionID=@numDivisionID and tintOppStatus<>2
GO
