/****** Object:  StoredProcedure [dbo].[usp_GetCampaignForReport]    Script Date: 07/26/2008 16:16:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
-- created By Anoop  Jayaraj  
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getcampaignforreport')
DROP PROCEDURE usp_getcampaignforreport
GO
CREATE PROCEDURE [dbo].[usp_GetCampaignForReport]  
@numDomainId as numeric,  
@numUserCntID as numeric,  
@tintType as tinyint,  
@tintRights as tinyint  
as  
  
  
--if @tintRights=1  
--select Cam.numCampaignId,  
--intLaunchDate,  
--intEndDate,  
--vcdata as CampaignName,   
--dbo.fn_GetListItemName(numCampaignType)as CampaignType,  
--dbo.fn_GetListItemName(numCampaignStatus)as CampaignStatus,  
--dbo.fn_GetListItemName(numRegion)as CampaignRegion,  
--monCampaignCost,  
--dbo.GetIncomeforCampaign(Cam.numCampaignId,2) as income,  
--dbo.GetIncomeforCampaign(Cam.numCampaignId,4) as ROI,  
--0 as DealCost,  
--0 as NoOfDeals  
--from CampaignMaster Cam  
--join listdetails  
--on Cam.numCampaign=numListItemID  
--join ForReportsByCampaign ForRep  
--on ForRep.numCampaignID=Cam.numCampaignID  
--where Cam.numCreatedBy=@numUserCntID  
--  
--  
--if @tintRights=2  
--select Cam.numCampaignId,  
--intLaunchDate,  
--intEndDate,  
--vcdata as CampaignName,   
--dbo.fn_GetListItemName(numCampaignType)as CampaignType,  
--dbo.fn_GetListItemName(numCampaignStatus)as CampaignStatus,  
--dbo.fn_GetListItemName(numRegion)as CampaignRegion,  
--monCampaignCost,  
--dbo.GetIncomeforCampaign(Cam.numCampaignId,2) as income,  
--dbo.GetIncomeforCampaign(Cam.numCampaignId,4) as ROI,  
--0 as DealCost,  
--0 as NoOfDeals  
--from CampaignMaster Cam  
--join listdetails  
--on Cam.numCampaign=numListItemID  
--join ForReportsByCampaign ForRep  
--on ForRep.numCampaignID=Cam.numCampaignID  
--where Cam.numCreatedBy in(select distinct(numUserCntID) from userTeams where numTeam in(select F.numTeam from ForReportsByTeam F           
--where F.numUserCntID=@numUserCntID and F.numDomainid=@numDomainId and F.tintType=@tintType))  
--  
--  
--if @tintRights=3  
--select Cam.numCampaignId,  
--intLaunchDate,  
--intEndDate,  
--vcdata as CampaignName,   
--dbo.fn_GetListItemName(numCampaignType)as CampaignType,  
--dbo.fn_GetListItemName(numCampaignStatus)as CampaignStatus,  
--dbo.fn_GetListItemName(numRegion)as CampaignRegion,  
--monCampaignCost,  
--dbo.GetIncomeforCampaign(Cam.numCampaignId,2) as income,  
--dbo.GetIncomeforCampaign(Cam.numCampaignId,4) as ROI,  
--0 as DealCost,  
--0 as NoOfDeals  
--from CampaignMaster Cam  
--join listdetails  
--on Cam.numCampaign=numListItemID  
--join ForReportsByCampaign ForRep  
--on ForRep.numCampaignID=Cam.numCampaignID  
--where Cam.numCreatedBy in(select distinct(numUserCntID) from userterritory where numTerritoryID in(select F.numTerritory from ForReportsByTerritory F           
--     where F.numUserCntID=@numUserCntID and F.numDomainid=@numDomainId and F.tintType=@tintType))

GO
