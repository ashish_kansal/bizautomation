/****** Object:  StoredProcedure [dbo].[USP_CaseDetailsdtlPL]    Script Date: 07/26/2008 16:15:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--exec USP_CaseDetails @numCaseId = 34                  
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_casedetailsdtlpl')
DROP PROCEDURE usp_casedetailsdtlpl
GO
CREATE PROCEDURE [dbo].[USP_CaseDetailsdtlPL]                                      
@numCaseId as numeric(9),                
@numDomainID varchar ,      
@ClientTimeZoneOffset as int                                     
                                      
as                                      
declare @strOppName as varchar(200)            
            
set @strOppName = ''            
select @strOppName= @strOppName +' <br> '+ vcpoppname from CaseOpportunities CO            
 join opportunitymaster OM on om.numoppid=co.numoppid and numCaseId=@numCaseId            
and co.numDomainid = @numdomainid            
            
                                  
select @strOppName as OppName,C.numContactID,vcCaseNumber,
 intTargetResolveDate,textSubject,Div.numDivisionID,Div.tintCRMType,   C.numContractID,                                  
 (select vcData from listdetails where numlistitemid =numStatus) as numStatusName, 
 numStatus,               
 ( select vcData from listdetails where numlistitemid =numPriority) as  numPriorityName,
 numPriority,
 textDesc,
 textInternalComments,
 ( select vcData from listdetails where numlistitemid =numReason) as numReasonName,
 numReason,
 ( select vcData from listdetails where numlistitemid =numOrigin) as numOriginName,
  numOrigin,                
 ( select vcData from listdetails where numlistitemid =numType) as numTypeName,
 numType,
 dbo.fn_GetContactName(C.numCreatedby)+' ' + convert(varchar(20),dateadd(minute,-@ClientTimeZoneOffset,C.bintCreatedDate)) as CreatedBy,                                   
 dbo.fn_GetContactName(C.numModifiedBy)+' ' + convert(varchar(20),dateadd(minute,-@ClientTimeZoneOffset,C.bintModifiedDate)) as ModifiedBy,
 dbo.fn_GetContactName(C.numRecOwner) as RecOwner,
 tintSupportKeyType,
 dbo.fn_GetContactName(c.numAssignedTo)as numAssignedToName,
 dbo.fn_GetContactName(c.numContactId)as numContactIdName,
 C.numAssignedTo,
 C.numAssignedBy,isnull(Addc.vcFirstName,'') + ' ' + isnull(Addc.vcLastName,'') as Name,
 isnull(vcEmail,'') as vcEmail,isnull(numPhone,'') + case when numPhoneExtension is null then '' when numPhoneExtension ='' then '' else '/ '+ numPhoneExtension end  as Phone,
 Com.numCompanyId,Com.vcCompanyName,(select count(*) from Comments where numCaseID =@numCaseId)  as NoofCases,
 ISNULL((SELECT COUNT(*) FROM [CaseOpportunities] WHERE [numCaseId]=@numCaseId),0) LinkedItemCount,
 ISNULL((SELECT CM.vcContractName FROM ContractManagement CM WHERE CM.numContractId = C.numContractId),'') ContractName,
 (SELECT SUBSTRING(
	(SELECT ','+  A.vcFirstName+' ' +A.vcLastName + CASE WHEN ISNULL(SR.numContactType,0)>0 THEN '(' + dbo.fn_GetListItemName(SR.numContactType) + ')' ELSE '' END
	FROM ShareRecord SR JOIN UserMaster UM ON UM.numUserDetailId=SR.numAssignedTo 
							 JOIN AdditionalContactsInformation A ON UM.numUserDetailId=A.numContactID 
		 WHERE SR.numDomainID=C.numDomainID AND SR.numModuleID=7 AND SR.numRecordID=C.numCaseId
				AND UM.numDomainID=C.numDomainID and UM.numDomainID=A.numDomainID FOR XML PATH('')),2,200000)) AS numShareWith,
 (SELECT COUNT(*) FROM   dbo.GenericDocuments WHERE  numRecID = @numCaseId AND vcDocumentSection = 'CS') AS DocumentCount	,
 ISNULL(C.vcSolutionShortMessage,'')  AS SoultionName,
 (SELECT COUNT(*) FROM   dbo.CaseSolutions where numCaseId=@numCaseId) AS SolutionCount,
 (SELECT (ISNULL(C.numIncidents,0)  - ISNULL(C.numIncidentsUsed,0)) FROM Contracts AS C WHERE C.numDivisonId=Div.numDivisionID AND numDomainId=@numDomainID AND intType=3) AS numIncidentLeft
 from Cases C                                      
 left join AdditionalContactsInformation Addc                                      
 on Addc.numContactId=C.numContactID                                       
 join DivisionMaster Div                                      
 on Div.numDivisionId=Addc.numDivisionId                                      
 join CompanyInfo Com                                       
 on Com.numCompanyID=div.numCompanyID                                      
                                    
 where numCaseId=@numCaseId
GO
