/****** Object:  StoredProcedure [dbo].[Variance_Sel]    Script Date: 07/26/2008 16:22:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*  
**  Selects the set of Activities representing Variances to an  
**  established recurring series.  Each Variance is an Activity  
**  with one or more field values different from that of the  
**  recurring series' Root Activity.  They override their  
**  modified qualities on the occurrence that would have been  
**  generated on OriginalStartDateTimeUtc (I say "would have  
**  been" because one of the qualities that can be varied is  
**  the StartDateTimeUtc.)  
*/  
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='variance_sel')
DROP PROCEDURE variance_sel
GO
CREATE PROCEDURE [dbo].[Variance_Sel]  
 @StartDateTimeUtc datetime, -- the start date before which no variances are retrieved  
 @EndDateTimeUtc  datetime, -- the end date, any variances starting after this date are excluded  
 @OrganizerName  nvarchar(64), -- resource name (will be used to lookup ID)  
 @RecurrenceKey  integer, -- required to identify the recurring series  
 @VarianceKey  uniqueidentifier   -- required to identify correlated Activity rows that make up a recurring series' variances  
AS  
BEGIN  
 --  
 -- First step is to get the ResourceID for the primary resource name supplied.  
 --  
 
 DECLARE @ResourceID AS integer;
if isnumeric(@OrganizerName) =1
begin
   set @ResourceID=convert(numeric(9), @OrganizerName)
end
else
begin 
	set @ResourceID =-999	
end  
-- SELECT  
--  @ResourceID = [Resource].[ResourceID]  
-- FROM  
--  [Resource]  
-- WHERE  
--  ( [Resource].[ResourceName] = @OrganizerName );  
 --  
 -- Second step is to retrieve the Activities organized (or owned by) this resource.  
 -- They will be the ones in the ActivityResource table where ResourceID is the  
 -- key number I've just identified.  
 --  
 SELECT   
  [Activity].[AllDayEvent],   
  [Activity].[ActivityDescription],   
  [Activity].[Duration],   
  [Activity].[Location],   
  [Activity].[ActivityID],   
  [Activity].[StartDateTimeUtc],   
  [Activity].[Subject],   
  [Activity].[EnableReminder],   
  [Activity].[ReminderInterval],  
  [Activity].[ShowTimeAs],  
  [Activity].[Importance],  
  [Activity].[Status],  
  [Activity].[RecurrenceID],  
  [Activity].[VarianceID],  
  [Activity].[OriginalStartDateTimeUtc],  
  [Activity].[_ts],  
  [Activity].[ItemId],  
  [Activity].[ChangeKey],isnull(Activity.Color,0) as Color,
 ISNULL(LastSnoozDateTimeUtc,'1900-01-01 00:00:00') AS LastSnoozDateTimeUtc    
 FROM   
  [Activity] INNER JOIN [ActivityResource] ON [Activity].[ActivityID] = [ActivityResource].[ActivityID]  
 WHERE   
  (([Activity].[ActivityID] = [ActivityResource].[ActivityID]) AND  
   ([ActivityResource].[ResourceID] = @ResourceID)) AND   
   (( [Activity].[StartDateTimeUtc] >= @StartDateTimeUtc ) AND  
    ( [Activity].[StartDateTimeUtc] < @EndDateTimeUtc ) AND   
    ( [Activity].[RecurrenceID] = @RecurrenceKey ) AND  
    ( [Activity].[VarianceID] = @VarianceKey ) AND  
    ( NOT ( [Activity].[OriginalStartDateTimeUtc] IS NULL ) ) );  
END
GO
