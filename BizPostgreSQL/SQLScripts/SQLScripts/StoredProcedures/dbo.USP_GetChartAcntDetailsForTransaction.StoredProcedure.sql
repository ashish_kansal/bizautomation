/****** Object:  StoredProcedure [dbo].[USP_GetChartAcntDetailsForTransaction]    Script Date: 07/26/2008 16:16:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- Created by Siva                                      
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getchartacntdetailsfortransaction')
DROP PROCEDURE usp_getchartacntdetailsfortransaction
GO
CREATE PROCEDURE [dbo].[USP_GetChartAcntDetailsForTransaction]             
@numAccountId as numeric(9),                                  
@numDomainId as numeric(9),                          
@dtFromDate as datetime,                        
@dtToDate as datetime                                     
As                                        
Begin                                        
 Declare @strSQL as varchar(2000)                        
 Declare @i as integer                    
 Set @strSQL=''      
   
  Declare @numParentAccountId as numeric(9)      
  Set @numParentAccountId=(Select numAccountId From Chart_Of_Accounts Where numParntAcntTypeId is null and numAcntTypeID is null and numDomainId = @numDomainId) --and numAccountId = 1       
       
                        
 --  Select @i=count(*) From General_Journal_Header GJH Where GJH.datEntry_Date>='' + convert(varchar(300),@dtFromDate) +''                
 --  And  GJH.datEntry_Date<='' + convert(varchar(300),@dtToDate) +''    
 Select @i=count(*) From General_Journal_Header GJH Where --GJH.datEntry_Date>='' + convert(varchar(300),@dtFromDate) +''  And                               
 GJH.numDomainId=@numDomainId And GJH.datEntry_Date<='' + convert(varchar(300),@dtToDate) +''               
 print @i        
 if @i=0                  
  Begin                  
     Set @strSQL = ' Select distinct numAccountId as numAccountId,vcCatgyName as CategoryName,numOpeningBal as OpeningBalance                            
   From Chart_Of_Accounts COA                            
   Left outer join General_Journal_Details GJD on COA.numAccountId=GJD.numChartAcntId                            
   Where  COA.numDomainId=' + Convert(varchar(5),@numDomainId) + 'And COA.numAccountId <>'+convert(varchar(10),@numParentAccountId) +' And COA.numOpeningBal is not null   And COA.numAccountId=' + Convert(Varchar(5),@numAccountId)                      
     Set @strSQL=@strSQL+ '  And COA.dtOpeningDate>=''' + convert(varchar(300),@dtFromDate) +''' And  COA.dtOpeningDate<=''' + convert(varchar(300),@dtToDate) +''''                           
  End                   
 Else                  
  Begin                  
     Set @strSQL = ' Select distinct numAccountId as numAccountId,vcCatgyName as CategoryName,numOpeningBal as OpeningBalance                            
   From Chart_Of_Accounts COA                            
   Left outer join General_Journal_Details GJD on COA.numAccountId=GJD.numChartAcntId                            
   Where  COA.numDomainId=' + Convert(varchar(5),@numDomainId) + 'And COA.numAccountId <>'+Convert(varchar(10),@numParentAccountId)+' And COA.numOpeningBal is not null  And COA.numAccountId=' + Convert(Varchar(5),@numAccountId)                    
  End                  
 print @strSQL                        
 exec (@strSQL)                        
End
GO
