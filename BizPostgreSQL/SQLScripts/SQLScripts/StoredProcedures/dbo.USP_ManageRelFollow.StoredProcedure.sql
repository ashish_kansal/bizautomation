/****** Object:  StoredProcedure [dbo].[USP_ManageRelFollow]    Script Date: 07/26/2008 16:19:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj  
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_managerelfollow')
DROP PROCEDURE usp_managerelfollow
GO
CREATE PROCEDURE [dbo].[USP_ManageRelFollow] 
@numRelationship as numeric(9),  
@numFollow as numeric(9),  
@numRelFolID as numeric(9)  
as  
if @numRelFolID>0   
begin  
update  RelFollowupStatus set numRelationshipID=@numRelationship,numFollowID=@numFollow where numRelFolID=@numRelFolID  
end  
else  
begin  
insert into RelFollowupStatus(numRelationshipID,numFollowID) values(@numRelationship,@numFollow)  
end
GO
