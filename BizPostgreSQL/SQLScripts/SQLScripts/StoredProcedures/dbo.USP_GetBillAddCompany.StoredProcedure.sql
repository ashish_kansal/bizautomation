
GO
/****** Object:  StoredProcedure [dbo].[USP_GetBillAddCompany]    Script Date: 08/08/2009 16:38:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created By Anoop Jayaraj                
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getbilladdcompany')
DROP PROCEDURE usp_getbilladdcompany
GO
CREATE PROCEDURE [dbo].[USP_GetBillAddCompany]                
@numDivisionId as numeric(9),        
@numContactID as numeric(9),    
@numDomainID as numeric(9)=0                
as          
if   @numDivisionId>0        
begin        
 select
   vcCompanyName,                  
   AD1.vcstreet vcBillstreet,                
   AD1.vcCity vcBillCity,                
   AD1.numState vcBilState,                  
   AD1.vcPostalCode vcBillPostCode,                
   AD1.numCountry vcBillCountry,                
   AD2.vcstreet vcShipStreet,                
   AD2.vcCity vcShipCity,                
   AD2.numState vcShipState,                
   AD2.vcPostalCode vcShipPostCode,                
   AD2.numCountry vcShipCountry,          
   tintBillingTerms,            
   numBillingDays,            
   tintInterestType,            
   fltInterest,        
   vcWebSite,      
   isnull(dbo.fn_GetState(AD1.numState),'-') as BillState,      
   dbo.fn_GetListItemName(AD1.numCountry) as BillCountry,
   D.[vcComPhone]
  from divisionmaster D        
   Join CompanyInfo C        
   on C.numCompanyID=D.numCompanyID                 
   LEFT JOIN dbo.AddressDetails AD1 ON AD1.numDomainID=D.numDomainID 
   AND AD1.numRecordID= D.numDivisionID AND AD1.tintAddressOf=2 AND AD1.tintAddressType=1 AND AD1.bitIsPrimary=1
   LEFT JOIN dbo.AddressDetails AD2 ON AD2.numDomainID=D.numDomainID 
   AND AD2.numRecordID= D.numDivisionID AND AD2.tintAddressOf=2 AND AD2.tintAddressType=2 AND AD2.bitIsPrimary=1
  where                 
   D.numDivisionID=@numDivisionId   and D.numDomainID= @numDomainID
end
else if   @numContactID>0
begin
 select
   vcFirstname,vcLastname,vcEmail,numPhone as vcPhone, vcCompanyName,                
   AD1.vcstreet vcBillstreet,                
   AD1.vcCity vcBillCity,                
   AD1.numState vcBilState,                  
   AD1.vcPostalCode vcBillPostCode,                
   AD1.numCountry vcBillCountry,                
   AD2.vcstreet vcShipStreet,                
   AD2.vcCity vcShipCity,                
   AD2.numState vcShipState,                
   AD2.vcPostalCode vcShipPostCode,                
   AD2.numCountry vcShipCountry,                  
   tintBillingTerms,            
   numBillingDays,            
   tintInterestType,            
   fltInterest,        
   isnull(vcWebSite,'') as vcWebSite,isnull(AD.vcStreet,'') as vcStreet ,      
   isnull(AD.vcCity,'') as vcCity ,isnull(AD.vcPostalCode,'') as vcPPostalCode ,      
   isnull(AD.numState,0) as numState,isnull(AD.numCountry,0) as numCountry,      
   isnull(dbo.fn_GetState(AD1.numState),'-') as BillState,      
   dbo.fn_GetListItemName(AD1.numCountry) as BillCountry,     
   isnull(dbo.fn_GetState(AD.numState),'-') as PState,      
   dbo.fn_GetListItemName(AD.numCountry) as PCountry,
   D.[vcComPhone]
  from                  
   divisionmaster D        
   Join CompanyInfo C        
   on C.numCompanyID=D.numCompanyID         
   join AdditionalContactsInformation A        
   on A.numDivisionId=D.numDivisionId      
   LEFT JOIN dbo.AddressDetails AD ON AD.numDomainID=D.numDomainID 
   AND AD.numRecordID= A.numContactId AND AD.tintAddressOf=1 AND AD.tintAddressType=0 AND AD.bitIsPrimary=1
   LEFT JOIN dbo.AddressDetails AD1 ON AD1.numDomainID=D.numDomainID 
   AND AD1.numRecordID= D.numDivisionID AND AD1.tintAddressOf=2 AND AD1.tintAddressType=1 AND AD1.bitIsPrimary=1
   LEFT JOIN dbo.AddressDetails AD2 ON AD2.numDomainID=D.numDomainID 
   AND AD2.numRecordID= D.numDivisionID AND AD2.tintAddressOf=2 AND AD2.tintAddressType=2 AND AD2.bitIsPrimary=1               
  where                 
   A.numContactId=@numContactID   and A.numDomainID= @numDomainID     
end
