/****** Object:  StoredProcedure [dbo].[usp_getDocumentsContCmpOpp]    Script Date: 07/26/2008 16:17:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getdocumentscontcmpopp')
DROP PROCEDURE usp_getdocumentscontcmpopp
GO
CREATE PROCEDURE [dbo].[usp_getDocumentsContCmpOpp]
@numContactId numeric=0,
@numDivisionId numeric=0,
@numDocCategory numeric=0,
@byte numeric =0
as 
if @byte=0
begin
 select distinct numGenericDocID,            
  VcFileName ,            
  vcDocName 
           
  from GenericDocuments          
  where numRecID=@numContactId or numRecID=@numDivisionId
end
if @byte =1 
begin
 select distinct numGenericDocID,            
  VcFileName ,            
  vcDocName            
from GenericDocuments          
  where numRecID in (select numoppid from opportunitymaster where numcontactid = @numContactId)
and numDocCategory = @numDocCategory
end
GO
