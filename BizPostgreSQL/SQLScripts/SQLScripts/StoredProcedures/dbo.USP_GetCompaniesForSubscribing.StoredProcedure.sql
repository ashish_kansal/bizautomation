/****** Object:  StoredProcedure [dbo].[USP_GetCompaniesForSubscribing]    Script Date: 07/26/2008 16:16:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj    
    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getcompaniesforsubscribing')
DROP PROCEDURE usp_getcompaniesforsubscribing
GO
CREATE PROCEDURE [dbo].[USP_GetCompaniesForSubscribing]    
@vcSearch as varchar(100)='',    
@numDomainID as numeric(9)=0    
as    
    
 select D.numDivisionID,A.numContactID,vcCompanyName,isnull(vcData,'-') as Employees,  
 vcWebsite,isnull(vcFirstName,'')+' ' + isnull(vcLastName,'') as [Name],vcEmail from CompanyInfo C    
 Join DivisionMaster D    
 on C.numCompanyID = D.numCompanyID    
 Join AdditionalContactsInformation A    
 on A.numDivisionID=D.numDivisionID    
 left join ListDetails     
 on numListItemID=numNoOfEmployeesID    
 where ISNULL(A.bitPrimaryContact,0)=1 and D.numDomainID=@numDomainID and D.numDivisionID not in (select numDivisionID from Subscribers where numDomainID=@numDomainID and bitDeleted=0)     
 and vcCompanyName like '%'+@vcSearch+'%'
GO
