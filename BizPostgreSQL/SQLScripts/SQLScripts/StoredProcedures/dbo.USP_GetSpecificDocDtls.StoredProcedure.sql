/****** Object:  StoredProcedure [dbo].[USP_GetSpecificDocDtls]    Script Date: 07/26/2008 16:18:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj        
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getspecificdocdtls')
DROP PROCEDURE usp_getspecificdocdtls
GO
CREATE PROCEDURE [dbo].[USP_GetSpecificDocDtls]        
@numGenericDocID as numeric(9),      
@numUserCntID as numeric(9)=0 ,
@numDomainID as numeric(9)=0       
as        
select A.bintCreatedDate,A.bintModifiedDate,A.cUrlType,A.numDocStatus,
A.numCreatedBy,A.numDocCategory,A.numDomainID,A.numModifiedBy,A.numRecID,
A.numGenericDocID,A.vcDocDesc,A.vcDocName,A.vcDocumentSection,
ISNULL(A.VcFileName,'') AS VcFileName ,A.vcFileType
 , (select count(*) from DocumentWorkflow     
where numDocID=@numGenericDocID and numContactID=@numUserCntID and cDocType=vcDocumentSection and tintApprove=0) as AppReq,    
 (select count(*) from DocumentWorkflow             
 where numDocID=@numGenericDocID and cDocType=vcDocumentSection and tintApprove=1) as Approved,    
 (select count(*) from DocumentWorkflow             
 where numDocID=@numGenericDocID and cDocType=vcDocumentSection and tintApprove=2) as Declined,    
 (select count(*) from DocumentWorkflow             
 where numDocID=@numGenericDocID and cDocType=vcDocumentSection and tintApprove=0) as Pending       
from GenericDocuments A where numGenericDocID=@numGenericDocID and numDomainID=@numDomainID
GO
