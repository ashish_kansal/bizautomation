/****** Object:  StoredProcedure [dbo].[Resource_Rem]    Script Date: 07/26/2008 16:14:34 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
/*
**  Removes the Resource identified by the given ID number, and
**  re-sets any Activities that reference this Resource in the 
**  ActivityResource table to reference the UnassignedResource
**  (-999), instead.
**
**  This stored procedure is not ordinarily required by interactive users.
**  It supports an "administrative" mode application that needs to remove
**  Resources from the Microsoft SQL Server database.
*/
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='resource_rem')
DROP PROCEDURE resource_rem
GO
CREATE PROCEDURE [dbo].[Resource_Rem]
	@ResourceID		integer		-- primary key number of the resource to delete
AS
BEGIN
	-- Start a transaction to commit both deletions on the Resource
	-- and ActivityResource tables simultaneously.
	BEGIN TRANSACTION

	UPDATE
		[ActivityResource]
	SET
		[ActivityResource].[ResourceID] = (-999)
	WHERE 
		( [ActivityResource].[ResourceID] = @ResourceID );

	DELETE FROM [ResourcePreference]
	WHERE ( [ResourcePreference].[ResourceID] = @ResourceID );

	DELETE FROM [Resource]
	WHERE ( [Resource].[ResourceID] = @ResourceID );

	COMMIT
END
GO
