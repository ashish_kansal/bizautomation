/****** Object:  StoredProcedure [dbo].[USP_ForecastDelete]    Script Date: 07/26/2008 16:15:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_forecastdelete')
DROP PROCEDURE usp_forecastdelete
GO
CREATE PROCEDURE [dbo].[USP_ForecastDelete]  
@tintForecastType as tinyint=null,  
@numUserCntID as numeric(9)=null,  
@numItemCode as numeric(9)=null,  
@sintYear as smallint=null,  
@tintquarter as tinyint =null,  
@numDomainId as numeric(9)=null  
as  
if @tintForecastType=1   
begin  
delete from Forecast where numCreatedBy=@numUserCntID   
and sintYear=@sintYear and tintquarter=@tintquarter  
and tintForecastType=@tintForecastType  
and numDomainId=@numDomainId  
end  
else  
begin  
delete from Forecast where numCreatedBy=@numUserCntID   
and sintYear=@sintYear and tintquarter=@tintquarter  
and tintForecastType=@tintForecastType  
and numItemCode=@numItemCode and numDomainId=@numDomainId  
end
GO
