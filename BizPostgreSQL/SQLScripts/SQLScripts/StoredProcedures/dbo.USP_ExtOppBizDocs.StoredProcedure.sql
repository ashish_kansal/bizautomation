/****** Object:  StoredProcedure [dbo].[USP_ExtOppBizDocs]    Script Date: 07/26/2008 16:15:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj            
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_extoppbizdocs')
DROP PROCEDURE usp_extoppbizdocs
GO
--Obsolete procedure
CREATE PROCEDURE [dbo].[USP_ExtOppBizDocs]                
(                       
 @numOppId as numeric(9)=null                 
)                
as                
                
                
select  opp.numOppBizDocsId,                
 opp.numOppId,                
 opp.numBizDocId,                
 oppmst.vcPOppName,                
 mst.vcData as BizDoc,                
 opp.numCreatedBy,                
opp.dtCreatedDate CreatedDate,                
 vcBizDocID               
from                 
 OpportunityBizDocs  opp                
 left join ListDetails mst                
 on mst.numListItemID=opp.numBizDocId                
 join OpportunityMaster oppmst                
 on oppmst.numOppId= opp.numOppId                               
 left join PortalBizDocs P        
 on P.numBizDocID=opp.numBizDocId        
where opp.numOppId = @numOppId
GO
