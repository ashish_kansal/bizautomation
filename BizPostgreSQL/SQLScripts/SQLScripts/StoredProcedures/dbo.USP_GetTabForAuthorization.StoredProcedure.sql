/****** Object:  StoredProcedure [dbo].[USP_GetTabForAuthorization]    Script Date: 07/28/2009 22:53:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj    
-- exec USP_GetTabForAuthorization @numGroupID=103,@numRelationShip=46,@numProfileID=0,@numDomainID=72
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_gettabforauthorization')
DROP PROCEDURE usp_gettabforauthorization
GO
CREATE PROCEDURE [dbo].[USP_GetTabForAuthorization]          
@numGroupID as numeric(9)=0  ,    
@numRelationShip as numeric(9)=0,
@numProfileID as numeric(9)=0,
@numDomainID as numeric(9)=0
--@numLocationID as numeric(9)=0       
as          
    
--if @numLocationID=0
--  begin    
   SELECT   numTabID,
            CASE WHEN bitFixed = 1
                 THEN CASE WHEN EXISTS ( SELECT numTabName
                                         FROM   TabDefault
                                         WHERE  numDomainid = @numDomainID
                                                AND numTabId = TM.numTabId
                                                AND tintTabType = TM.tintTabType )
                           THEN ( SELECT TOP 1
                                            numTabName
                                  FROM      TabDefault
                                  WHERE     numDomainid = @numDomainID
                                            AND numTabId = TM.numTabId
                                            AND tintTabType = TM.tintTabType
                                )
                           ELSE Tm.numTabName
                      END
                 ELSE Tm.numTabName
            END numTabname,
            tintTabType
   FROM     TabMaster TM
   WHERE    ( numDomainID = @numDomainID
              OR bitFixed = 1
            ) 
            AND tintTabType = ( SELECT  tintGroupType
                                FROM    AuthenticationGroupMaster
                                WHERE   numGroupID = @numGroupID
                              ) 
            AND numTabName NOT IN ('POS','OmniBiz')    
			  
   SELECT   T.numTabID,
            CASE WHEN bitFixed = 1
                 THEN CASE WHEN EXISTS ( SELECT numTabName
                                         FROM   TabDefault
                                         WHERE  numDomainid = @numDomainID
                                                AND numTabId = T.numTabId
                                                AND tintTabType = T.tintTabType )
                           THEN ( SELECT TOP 1
                                            numTabName
                                  FROM      TabDefault
                                  WHERE     numDomainid = @numDomainID
                                            AND numTabId = T.numTabId
                                            AND tintTabType = T.tintTabType
                                )
                           ELSE T.numTabName
                      END
                 ELSE T.numTabName
            END numTabname,
            tintTabType,*
   FROM     TabMaster T
            JOIN GroupTabDetails G ON G.numTabId = T.numTabId
   WHERE    ( numDomainID = @numDomainID
              OR bitFixed = 1
            ) AND ISNULL([tintType],0)=0
            AND numGroupID = @numGroupID
            AND ISNULL(numRelationShip,0) = @numRelationShip 
            AND ISNULL(numProfileID,0) = @numProfileID 
            AND numTabName NOT IN ('POS','OmniBiz')
   ORDER BY numOrder
--   end
--else
--     begin
--      SELECT numTabId,
--             numTabName,
--             tintTabType
--      FROM   TabMaster
--      WHERE  tintTabType = @numGroupID
--             AND numModuleId = @numLocationID
--             AND numDomainId = @numDomainId
--      SELECT tm.numTabId,
--             tm.numTabName numTabName,
--             tm.tintTabType tintTabType,
--             tm.numModuleId
--      FROM   TabMaster tm
--             INNER JOIN GroupTabDetails G
--               ON tm.numTabId = G.numTabId
--      WHERE  tm.numDomainId = @numDomainID
--             AND G.[numTabId] = @numLocationID -- not sure about this change pls verify once
--             AND tm.tintTabType = @numGroupID
--      
--     end