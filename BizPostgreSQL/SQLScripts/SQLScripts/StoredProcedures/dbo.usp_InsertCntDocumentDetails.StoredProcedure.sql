/****** Object:  StoredProcedure [dbo].[usp_InsertCntDocumentDetails]    Script Date: 07/26/2008 16:19:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_insertcntdocumentdetails')
DROP PROCEDURE usp_insertcntdocumentdetails
GO
CREATE PROCEDURE [dbo].[usp_InsertCntDocumentDetails]
	@numUserID numeric(9)=0,
	@vcFileName varchar(100)=0,
	@vcDisplayName varchar(100)=0,
	@bintDateOfUpload bigint=0,
	@numCntID numeric(9)=0,
	@numSize numeric(9)=0,
	@numDomainID numeric(9)=0   
--

AS
	--Insertion into Table  
	INSERT INTO CntDocuments(numUserID,vcFileName,vcDisplayName,bintDateOfUpload,numCntID,numSize,numDomainID)
	VALUES(@numUserID,@vcFileName,@vcDisplayName,@bintDateOfUpload,@numCntID,@numSize,@numDomainID)
GO
