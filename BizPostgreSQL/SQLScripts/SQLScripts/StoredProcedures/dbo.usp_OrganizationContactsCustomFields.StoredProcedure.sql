/****** Object:  StoredProcedure [dbo].[usp_OrganizationContactsCustomFields]    Script Date: 07/26/2008 16:20:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--DROP TABLE ##OrgContactCustomField                
--SELECT * FROM ##OrgContactCustomField                
                
--Created By: Debasish Nag                          
--Created On: 12th Nov 2005                          
--Purpose: To Get the Custom Fields for Organizations and Contact and return the same as a pivoted table in a view                          
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_organizationcontactscustomfields')
DROP PROCEDURE usp_organizationcontactscustomfields
GO
CREATE PROCEDURE [dbo].[usp_OrganizationContactsCustomFields]        
 @numRefreshTimeInterval Numeric                                    
AS                        
BEGIN                        
 DECLARE @tempOrgContactTable NVARCHAR(100)                        
 SET @tempOrgContactTable = '##OrgContactCustomField'                        
                  
 DECLARE @vcColumnListNew NVarchar(4000)                        
 DECLARE @vcColumnListNewOrg NVarchar(4000)                        
 DECLARE @vcColumnListNewCont NVarchar(4000)                        
                        
 SELECT @vcColumnListNewOrg = dbo.fn_OrgAndContCustomFieldsColumns(1,1)                        
 --PRINT 'ColumnListNewOrg: ' + @vcColumnListNewOrg                        
 SELECT @vcColumnListNewCont = dbo.fn_OrgAndContCustomFieldsColumns(4,1)                        
 --PRINT 'ColumnListNewCont: ' + @vcColumnListNewCont                        
 IF @vcColumnListNewOrg Is Not NULL AND @vcColumnListNewCont Is Not NULL                        
  SELECT @vcColumnListNew = @vcColumnListNewOrg +  ', ' + @vcColumnListNewCont                        
 ELSE IF  @vcColumnListNewOrg Is NULL AND @vcColumnListNewCont Is Not NULL                    
  SELECT @vcColumnListNew = @vcColumnListNewCont                        
 ELSE IF  @vcColumnListNewCont Is NULL AND @vcColumnListNewOrg Is Not NULL                            
  SELECT @vcColumnListNew = @vcColumnListNewOrg          
                       
                        
 --PRINT @vcColumnListNew                        
                        
 IF NOT EXISTS (SELECT 'x' FROM tempdb..sysobjects WHERE type = 'U' and NAME = @tempOrgContactTable)                        
  BEGIN                        
   --Print 'Table To Be Created'                        
   DECLARE @OrgAndContTempCustomTableColumns NVarchar(4000)                        
   DECLARE @OrgTempCustomTableQuery NVarchar(4000)                        
   DECLARE @ContTempCustomTableQuery NVarchar(4000)                        
   SELECT @OrgTempCustomTableQuery = dbo.fn_OrgAndContCustomFieldsColumns(1,0)                        
   SELECT @ContTempCustomTableQuery = dbo.fn_OrgAndContCustomFieldsColumns(4,0)                        
                        
   IF @OrgTempCustomTableQuery Is Not NULL AND @ContTempCustomTableQuery Is Not NULL                        
    SELECT @OrgAndContTempCustomTableColumns = @OrgTempCustomTableQuery +  ', ' + @ContTempCustomTableQuery                        
   ELSE IF  @OrgTempCustomTableQuery Is NULL AND @ContTempCustomTableQuery Is Not NULL                        
    SELECT @OrgAndContTempCustomTableColumns = @ContTempCustomTableQuery                        
   ELSE IF  @ContTempCustomTableQuery Is NULL AND @OrgTempCustomTableQuery Is Not NULL                        
    SELECT @OrgAndContTempCustomTableColumns = @OrgTempCustomTableQuery                  
                
   DECLARE @vwOrgContactTableColumns Nvarchar(4000)                   
                
   SELECT @vwOrgContactTableColumns = Coalesce(@vwOrgContactTableColumns + ', ', '') + name                 
   FROM dbo.syscolumns                 
    WHERE ID = (                
    SELECT ID FROM dbo.sysobjects                
    WHERE name  = 'vw_OrgContactCustomReport'                
    AND xType = 'V'                
    )                  
   AND colid > 3                
   ORDER BY colid                 
   --PRINT @vwOrgContactTableColumns                
                
   DECLARE @vwOrgContactTableColumnsAndDataType Nvarchar(4000)                   
                
   SELECT @vwOrgContactTableColumnsAndDataType = Coalesce(@vwOrgContactTableColumnsAndDataType + ', ', '') + c.name + ' ' + t.name +  + CASE WHEN t.name <> 'tinyInt' Then                 
                  CASE WHEN (c.prec IS NULL OR t.name = 'bit') THEN ''                 
                  ELSE                 
                   '(' + Cast(c.prec As NVarchar) + CASE WHEN c.scale IS NULL THEN ')' ELSE ',' + Cast(c.scale AS Nvarchar) + ')' END                 
                  END                
              ELSE                
                  ''                
                 END                
    FROM dbo.syscolumns c, (SELECT name, type, xtype, xusertype, usertype FROM dbo.systypes) t            
    WHERE c.ID = (                
    SELECT ID FROM dbo.sysobjects                
    WHERE name  = 'vw_OrgContactCustomReport'                
    AND xType = 'V'                
    )                 
   --AND t.type = c.type                
   AND t.xusertype = c.xusertype                
   AND t.usertype = c.usertype                
   AND colid > 3                
   ORDER BY colid                 
   --PRINT @vwOrgContactTableColumnsAndDataType          
          
   DECLARE @OrgContCreateQuery1 NVarchar(4000)          
   DECLARE @OrgContCreateQuery2 NVarchar(4000)          
   SELECT @OrgContCreateQuery1 = 'CREATE TABLE ' + @tempOrgContactTable + '(numCompanyID Numeric, numDivisionId Numeric, numContactId Numeric, vcColumnList NVarchar(2000), CreatedDateTime DateTime, ' + @vwOrgContactTableColumnsAndDataType                 
  
    
      
       
   SELECT @OrgContCreateQuery2 =  CASE WHEN @OrgAndContTempCustomTableColumns IS Not NULL THEN ', ' + @OrgAndContTempCustomTableColumns ELSE '' END + ')'                        
   PRINT @OrgContCreateQuery1                        
   PRINT @OrgContCreateQuery2                        
   EXEC (@OrgContCreateQuery1 + @OrgContCreateQuery2)          
                        
    DECLARE @OrgContTempCustomTableDataEntryQuery1st NVarchar(4000)                 
    DECLARE @OrgContTempCustomTableDataEntryQuery2nd NVarchar(4000)                  
    DECLARE @OrgContTempCustomTableDataEntryQuery3rd NVarchar(4000)                   
    DECLARE @OrgContTempCustomTableDataEntryQuery4th NVarchar(4000)                         
    SELECT @OrgContTempCustomTableDataEntryQuery1st = 'INSERT INTO ' + @tempOrgContactTable + ' (numCompanyID, numDivisionId, numContactId, vcColumnList, CreatedDateTime, ' + @vwOrgContactTableColumns +          
    CASE WHEN (@OrgTempCustomTableQuery Is Not NULL) AND (@ContTempCustomTableQuery Is Not NULL) THEN                
      ', ' + dbo.fn_OrgAndContCustomFieldsColumns(1,1) + ', ' + dbo.fn_OrgAndContCustomFieldsColumns(4,1) + ') '                
    WHEN (@OrgTempCustomTableQuery Is NULL) AND (@ContTempCustomTableQuery Is Not NULL) THEN                
      ', ' + dbo.fn_OrgAndContCustomFieldsColumns(4,1) + ') '                
    WHEN (@ContTempCustomTableQuery Is NULL) AND (@OrgTempCustomTableQuery Is Not NULL) THEN                
      ', ' + dbo.fn_OrgAndContCustomFieldsColumns(1,1) + ') '                
    ELSE                
      ') '                
    END          
          
    SELECT @OrgContTempCustomTableDataEntryQuery2nd = ' SELECT numCompanyId, numDivisionId, numContactId, ''' + CASE WHEN @vcColumnListNew IS NOT NULL THEN @vcColumnListNew ELSE '' END + ''', getutcdate(), ' + @vwOrgContactTableColumns                
                
    IF (@OrgTempCustomTableQuery Is Not NULL)  AND (@ContTempCustomTableQuery Is Not NULL)                
     SELECT @OrgContTempCustomTableDataEntryQuery3rd = ', ' + dbo.fn_OrgAndContCustomFieldsColumns(1,2)  + ', ' + dbo.fn_OrgAndContCustomFieldsColumns(4,2)                
    ELSE IF(@OrgTempCustomTableQuery Is NULL) AND (@ContTempCustomTableQuery Is Not NULL)                
     SELECT @OrgContTempCustomTableDataEntryQuery3rd = ', ' + dbo.fn_OrgAndContCustomFieldsColumns(4,2)                
    ELSE IF(@ContTempCustomTableQuery Is NULL) AND (@OrgTempCustomTableQuery Is Not NULL)                
     SELECT @OrgContTempCustomTableDataEntryQuery3rd = ', ' + dbo.fn_OrgAndContCustomFieldsColumns(1,2)                
    ELSE                
     SELECT @OrgContTempCustomTableDataEntryQuery3rd = ''                
                    
    SELECT @OrgContTempCustomTableDataEntryQuery4th = ' FROM vw_OrgContactCustomReport ORDER BY numCompanyId, numDivisionId, numContactId'                
                
    PRINT @OrgContTempCustomTableDataEntryQuery1st                
    PRINT @OrgContTempCustomTableDataEntryQuery2nd                
    PRINT @OrgContTempCustomTableDataEntryQuery3rd           
    PRINT @OrgContTempCustomTableDataEntryQuery4th                
    EXEC (@OrgContTempCustomTableDataEntryQuery1st + @OrgContTempCustomTableDataEntryQuery2nd + @OrgContTempCustomTableDataEntryQuery3rd + @OrgContTempCustomTableDataEntryQuery4th)                
    --EXEC ('SELECT * FROM ' + @tempOrgContactTable)                        
  END                        
 ELSE                        
  BEGIN          
   IF (NOT EXISTS (SELECT TOP 1 vcColumnList FROM ##OrgContactCustomField WHERE vcColumnList =  @vcColumnListNew))  OR ((SELECT TOP 1 numCompanyID FROM ##OrgContactCustomField WHERE DATEDIFF(mi,  CreatedDateTime, getutcdate()) > @numRefreshTimeInterval) > 0)
   BEGIN                        
        EXEC('DROP TABLE ' +  @tempOrgContactTable)                
        EXEC ('usp_OrganizationContactsCustomFields ' + @numRefreshTimeInterval)                        
   END                 
  END                        
END
GO
