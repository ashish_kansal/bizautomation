/****** Object:  StoredProcedure [dbo].[USP_RepCampaignDetails]    Script Date: 07/26/2008 16:20:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_repcampaigndetails')
DROP PROCEDURE usp_repcampaigndetails
GO
CREATE PROCEDURE [dbo].[USP_RepCampaignDetails]  
@numCampaignId as numeric(9)=0  
as  
select numCampaignId,  
numCampaignStatus,  
numCampaignType,  
intLaunchDate,  
intEndDate,  
numRegion,  
numNoSent,  
monCampaignCost,  
numDomainID,  
dbo.fn_GetUserName(numCreatedBy) as CreatedBy,  
dbo.fn_GetDateTimeFromNumber(bintCreatedDate) as CreatedDate,  
dbo.fn_GetUserName(numModifiedBy) as ModifiedBy,  
dbo.fn_GetDateTimeFromNumber(bintModifiedDate) as ModifiedDate,  
dbo.GetIncomeforCampaign(numCampaignId,1) as TIP,  
dbo.GetIncomeforCampaign(numCampaignId,2) as TI,  
dbo.GetIncomeforCampaign(numCampaignId,3) as CostMFG,  
dbo.GetIncomeforCampaign(numCampaignId,4) as ROI,
dbo.fn_GetListItemName(numRegion) as Region,
dbo.fn_GetListItemName(numCampaignType) as Type,
dbo.fn_GetListItemName(numCampaignStatus) as Status,
dbo.fn_GetListItemName(numCampaign) as Campaign
from CampaignMaster  
where numCampaignId=@numCampaignId
GO
