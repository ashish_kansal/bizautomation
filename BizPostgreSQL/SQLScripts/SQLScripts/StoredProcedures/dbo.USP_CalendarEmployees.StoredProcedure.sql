/****** Object:  StoredProcedure [dbo].[USP_CalendarEmployees]    Script Date: 07/26/2008 16:14:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj                      
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_calendaremployees')
DROP PROCEDURE usp_calendaremployees
GO
CREATE PROCEDURE [dbo].[USP_CalendarEmployees]                          
@numUserCntId as numeric(9)=0,            
@numDomainID as numeric(9)=0,            
@GroupId as numeric(9)=0                     
as               
            
--select *,resourceName as [Name] from resource      
declare @UserID as numeric(9)            
select @UserID=numUserID from UserMaster where numUserDetailID=@numUserCntId            

--1000 is added so that Myself is always first even if resourceId is greater than its previous records
SELECT * FROM (
select 1000 AS intOrder,resourceId,vcFirstName+' '+ vcLastName as [Name] from AdditionalContactsInformation                          
join UserMaster on numContactID=numUserDetailID        
join Resource on numusercntid = numUserDetailID                      
where   
--numTeam in (select numTeam from ForReportsByTeam where tintType=@intType and numUserCntID=@numUserCntId)  and                        
  numUserId <> @UserID      
 and UserMaster.numDomainID=@numDomainID    and numGroupID = @GroupId                 
      
union      
      
select 1 AS intOrder,resourceId,'Myself' as [Name] from AdditionalContactsInformation                          
join UserMaster on numUserDetailID=numUserDetailID       
join Resource on numusercntid = numUserDetailID       
where numUserDetailID=@numUserCntId and UserMaster.numDomainID=@numDomainID and numGroupID = @GroupId) X ORDER BY intOrder
GO
