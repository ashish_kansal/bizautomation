/****** Object:  StoredProcedure [dbo].[USP_GetContactsLogged]    Script Date: 07/26/2008 16:16:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- created by anoop jayaraj
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getcontactslogged')
DROP PROCEDURE usp_getcontactslogged
GO
CREATE PROCEDURE [dbo].[USP_GetContactsLogged]
@numDivisionID as numeric(9)=0
as
select Dtl.numContactID,
vcFirstName+' '+vcLastName +', '+ vcEmail as [Name],
bintLastLoggedIn,
numNoofTimes from ExtarnetAccounts Hdr
join ExtranetAccountsDtl Dtl
on Hdr.numExtranetID=Dtl.numExtranetID
join AdditionalContactsInformation AddC
on Dtl.numContactID=AddC.numContactID
where Hdr.numDivisionID=@numDivisionID
GO
