/****** Object:  StoredProcedure [dbo].[usp_GetSurveyHistoryList]    Script Date: 07/26/2008 16:18:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created By: Debasish  
--Modified On: 03/27/2006     Change the comparision from the email of the contact to the contact id  
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getsurveyhistorylist')
DROP PROCEDURE usp_getsurveyhistorylist
GO
CREATE PROCEDURE [dbo].[usp_GetSurveyHistoryList]        
 @numDomainID numeric,               
 @numContactID numeric        
AS                
--This procedure will return all surveys for the selected Contact         
BEGIN                
 SELECT Distinct sm.numSurID, vcSurName,               
 bintCreatedOn as dateCreatedOn,        
 srm.numRespondantID, srm.numSurRating, Convert(NVarchar,sm.numSurID) + ',' + Convert(NVarchar,srm.numRespondantID) AS numContactSurResponseLink        
 FROM SurveyMaster sm INNER JOIN SurveyRespondentsMaster srm ON sm.numSurID = srm.numSurID 
 INNER JOIN AdditionalContactsInformation aci ON aci.numContactId = srm.numRegisteredRespondentContactId     
 WHERE sm.numDomainID = @numDomainID        
 AND aci.numContactId = @numContactID        
 AND aci.numDomainID = sm.numDomainID        
END
GO
