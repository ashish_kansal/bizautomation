/****** Object:  StoredProcedure [dbo].[Select_Action_Template_Data]    Script Date: 07/26/2008 16:14:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='select_action_template_data')
DROP PROCEDURE select_action_template_data
GO
CREATE PROCEDURE [dbo].[Select_Action_Template_Data]
( 
@rowID Int 
)
As
Select * From tblActionItemData Where RowID = @rowID
GO
