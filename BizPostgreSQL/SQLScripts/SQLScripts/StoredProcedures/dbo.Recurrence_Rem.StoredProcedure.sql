/****** Object:  StoredProcedure [dbo].[Recurrence_Rem]    Script Date: 07/26/2008 16:14:31 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
/*
**  Removes an existing Recurrence and any Variances to leave only the original
**  Root Activity as a non-recurring appointment.
*/
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='recurrence_rem')
DROP PROCEDURE recurrence_rem
GO
CREATE PROCEDURE [dbo].[Recurrence_Rem]
	@DataKey	integer,	-- identifies the root activity
	@RecurrenceKey	integer		-- identfieis the recurrence
AS
BEGIN
	BEGIN TRANSACTION

	DECLARE @VarianceKey	uniqueidentifier;

	-- Get the variance correlation ID, if any, off of the Root Activity
	-- before striking it out.
	--
	SELECT
		@VarianceKey = [Activity].[VarianceID]
	FROM
		[Activity]
	WHERE
		( [Activity].[ActivityID] = @DataKey );

	-- Detach the Root Activity from the recurring series and clear-out any
	-- variance correlation ID so it isn't deleted when deleting variances.
	--
	UPDATE 
		[Activity]
	SET
		[RecurrenceID] = -999,
		[VarianceID]   = NULL
	WHERE
		( [Activity].[ActivityID] = @DataKey );

	-- Delete all variance, if any, rows in the Activity table related to
	-- the Recurrence.
	--
	IF ( NOT @VarianceKey IS NULL )
	BEGIN
		DELETE FROM [Activity]
		WHERE ( ( [Activity].[VarianceID] = @VarianceKey ) AND 
			( [Activity].[RecurrenceID] = @RecurrenceKey ) );
	END

	-- Now that all child table references to the Recurrence have been
	-- updated or deleted, eliminate the Recurrence.
	--
	DELETE FROM [Recurrence]
	WHERE ( [Recurrence].[RecurrenceID] = @RecurrenceKey );

	COMMIT

END
GO
