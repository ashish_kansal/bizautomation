/****** Object:  StoredProcedure [dbo].[USP_ManageAuthorizationGroup]    Script Date: 07/26/2008 16:19:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_manageauthorizationgroup')
DROP PROCEDURE usp_manageauthorizationgroup
GO
CREATE PROCEDURE [dbo].[USP_ManageAuthorizationGroup]
@numDomainID as numeric(9)=0,
@numGroupID as numeric(9)=0,
@vcGroupName as varchar(100)='',
@tinTGroupType as tinyint=null
AS
IF @numGroupID =0
BEGIN
	INSERT INTO AuthenticationGroupMaster 
	(
		vcGroupName,
		numDomainID,
		tintGroupType
	)
	VALUES
	(
		@vcGroupName,
		@numDomainID,
		@tinTGroupType
	)

	SELECT @numGroupID = SCOPE_IDENTITY()

	--MainTab From TabMaster 
	INSERT INTO GroupTabDetails (numGroupId,numTabId,numRelationShip,bitallowed,numOrder,tintType) VALUES (@numGroupID,1,0,1,1,0)
	INSERT INTO GroupTabDetails (numGroupId,numTabId,numRelationShip,bitallowed,numOrder,tintType) VALUES (@numGroupID,36,0,1,2,0)
	INSERT INTO GroupTabDetails (numGroupId,numTabId,numRelationShip,bitallowed,numOrder,tintType) VALUES (@numGroupID,7,0,1,3,0)
	INSERT INTO GroupTabDetails (numGroupId,numTabId,numRelationShip,bitallowed,numOrder,tintType) VALUES (@numGroupID,80,0,1,4,0)
	INSERT INTO GroupTabDetails (numGroupId,numTabId,numRelationShip,bitallowed,numOrder,tintType) VALUES (@numGroupID,3,0,1,5,0)
	INSERT INTO GroupTabDetails (numGroupId,numTabId,numRelationShip,bitallowed,numOrder,tintType) VALUES (@numGroupID,45,0,1,6,0)
	INSERT INTO GroupTabDetails (numGroupId,numTabId,numRelationShip,bitallowed,numOrder,tintType) VALUES (@numGroupID,4,0,1,7,0)
	INSERT INTO GroupTabDetails (numGroupId,numTabId,numRelationShip,bitallowed,numOrder,tintType) VALUES (@numGroupID,44,0,1,8,0)
	INSERT INTO GroupTabDetails (numGroupId,numTabId,numRelationShip,bitallowed,numOrder,tintType) VALUES (@numGroupID,6,0,1,9,0)

	IF (SELECT COUNT(*) FROM TreeNavigationAuthorization WHERE numDomainID=@numDomainID AND numGroupID=@numGroupID) = 0
	BEGIN
		INSERT  INTO dbo.TreeNavigationAuthorization
		(
			numGroupID,
			numTabID,
			numPageNavID,
			bitVisible,
			numDomainID,
			tintType
		)
		SELECT  
			@numGroupID as numGroupID,
			PND.numTabID,
			numPageNavID,
			1,
			@numDomainID AS [numDomainID],
			@tinTGroupType AS [tintType]
		FROM    
			dbo.PageNavigationDTL PND
		WHERE
			PND.numTabID IS NOT NULL
		UNION ALL 
		SELECT  
			numGroupID AS [numGroupID],
			1 AS [numTabID],
			ISNULL(LD.[numListItemID], 0) AS [numPageNavID],
			1 AS [bitVisible],
			@numDomainID AS [numDomainID],
			@tinTGroupType AS [tintType]
		FROM    
			dbo.ListDetails LD
		CROSS JOIN 
			dbo.AuthenticationGroupMaster AG
		WHERE   
			numListID = 27
			AND ISNULL(constFlag, 0) = 1
			AND AG.numDomainID = @numDomainID
			AND AG.numGroupID=@numGroupID
			AND numListItemID NOT IN (293,294,295,297,298,299)
	END
END
ELSE
BEGIN
	UPDATE 
		AuthenticationGroupMaster 
	SET 
		vcGroupName=@vcGroupName,
		numDomainID=@numDomainID,
		tintGroupType=@tinTGroupType
	WHERE 
		numGroupID=@numGroupID
END
GO
