/****** Object:  StoredProcedure [dbo].[USP_OPPMilestone]    Script Date: 07/26/2008 16:20:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj       
-- exec USP_OPPMilestone @ProcessID=0,@numUserCntID=17,@DomianID=72,@OpportunityID=5719                       
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_oppmilestone')
DROP PROCEDURE usp_oppmilestone
GO
CREATE PROCEDURE [dbo].[USP_OPPMilestone]                                
(                                
 @ProcessID numeric(9)=null,                                
 @numUserCntID numeric(9)=null,                                
 @DomianID numeric(9)=null,                                
 @OpportunityID numeric(9)=0                                         
)                                
as                                
begin                                
--                                
--if @ProcessID != 0                                 
--begin   
--PRINT 'hi'                             
-- select  mst.numStagePercentage,                                
--  dtl.numstagedetailsID,                                
--  dtl.vcstageDetail,                                
--  0 AS StageID,                                
--  @DomianID AS numDomainId,                                
--  @numUserCntID AS numCreatedBy,                                
--  getutcdate() AS bintCreatedDate,                                
--  @numUserCntID AS numModifiedBy ,                                 
--  getutcdate() AS bintModifiedDate,                                
--  getutcdate() AS bintDueDate,                                
--  getutcdate() AS bintStageComDate,                                
--  '' AS vcComments,                                
-- dtl.numAssignTo,                                 
--  0 AS bitAlert ,                                
--  0 AS bitStageCompleted,                                
--  0 as Op_Flag,                                
--  dbo.fn_GetContactName(@numUserCntID) as numModifiedByName,                                
--  0 as numOppStageID,                                
--  0 as Depend,      
--0 as  numTCategoryHDRID,      
--0 as numECategoryHDRID,                                
--  0 as Expense,                                
--  0 as Time,                                
--  0 as SubStg,                          
--  0 as numStage,              
--vcStagePercentageDtl,              
--isnull(numTemplateId,0) numTemplateId,          
-- isnull(tintPercentage,0) tintPercentage  ,    
--isnull(numEvent,0 ) numEvent  ,    
--isnull(numReminder,0)numReminder  ,    
--isnull(numET,0)numET  ,    
--isnull(numActivity,0)numActivity  ,    
--isnull(numStartDate,0)numStartDate  ,    
--isnull(numStartTime,0)numStartTime  ,    
--isnull(numStartTimePeriod,15)numStartTimePeriod  ,    
--isnull(numEndTime,0)numEndTime  ,    
--isnull(numEndTimePeriod,15)numEndTimePeriod  ,    
--isnull(txtCom,'') txtCom,  
-- isnull(numChgStatus,0)numChgStatus,  
--isnull(bitChgStatus,0)bitChgStatus,  
--isnull(bitClose,0)bitClose,  
--isnull(numType,0) numType  ,
--0 as numCommActId              
-- from StagePercentageMaster mst                                
-- join StagePercentageDetails dtl                                 
-- on dtl.numstagepercentageid=mst.numstagepercentageid                                
-- where dtl.slp_id=@ProcessID                                
-- order by numStagePercentage,numOppstageid                                
--end                                
--else                                
--begin                                
--                                
-- select  numStagePercentage,                                
--  numOppStageID as numstagedetailsID,                                
--  vcstageDetail,                                
--  numOppStageID as StageID,                                
--  numDomainId,                                
--  numCreatedBy,                                
--  bintCreatedDate ,                                
--  numModifiedBy ,                                 
--  bintModifiedDate,                                
--  CASE bintDueDate
--  WHEN '1900-01-01 00:00:00.000' THEN NULL
--  ELSE bintDueDate END AS bintDueDate,                                
--  CASE bintStageComDate
--  WHEN '1900-01-01 00:00:00.000' THEN NULL
--  ELSE bintStageComDate End AS bintStageComDate,
--  vcComments,                                
--  isnull(numAssignTo,0) as numAssignTo,                                 
--  bitAlert ,                                
--  bitStageCompleted,                                
--  0 as Op_Flag,                                
--  dbo.fn_GetContactName(numModifiedBy) as numModifiedByName,                                
--  numOppStageID,             
--  (select isnull([numCategoryHDRID],0) from TimeAndExpense where numDomainId = @DomianID and      
--  numOppid=@OpportunityID and numStageId = numOppStageID and tintTEType=1 and numCategory = 1) as numTCategoryHDRID,      
--  (select isnull([numCategoryHDRID],0) from TimeAndExpense where numDomainId = @DomianID and      
--  numOppid=@OpportunityID and numStageId = numOppStageID and tintTEType=1 and numCategory = 2) as numECategoryHDRID,                         
--  dbo.fn_GetDepDtlsbySalesStgID(@OpportunityID,numOppStageID,0) as Depend,                                
--  dbo.fn_GetExpenseDtlsbySalesStgID(@OpportunityID,numOppStageID,0) as Expense,                                
--  dbo.fn_GeTimeDtlsbySalesStgID(@OpportunityID,numOppStageID,0) as [Time],                                
--  dbo.fn_GetSubStgDtlsbySalesStgID(@OpportunityID,numOppStageID,0) as SubStg,   
--                         
--  isnull(numStage,0) as numStage,            
-- vcStagePercentageDtl,            
--isnull(numTemplateId,0) numTemplateId,isnull(tintPercentage,0) tintPercentage ,    
--    
--isnull(numEvent,0 ) numEvent  ,    
--isnull(numReminder,0)numReminder  ,    
--isnull(numET,0)numET  ,    
--isnull(numActivity,0)numActivity  ,    
--isnull(numStartDate,0)numStartDate  ,    
--isnull(numStartTime,0)numStartTime  ,    
--isnull(numStartTimePeriod,15)numStartTimePeriod  ,    
--isnull(numEndTime,0)numEndTime  ,    
--isnull(numEndTimePeriod,15)numEndTimePeriod  ,    
--isnull(txtCom,'') txtCom  ,  
--  
-- isnull(numChgStatus,0)numChgStatus,  
--isnull(bitChgStatus,0)bitChgStatus,  
--isnull(bitClose,0)bitClose ,      
-- isnull(numType ,0)numType ,isnull(numCommActId,0 )numCommActId
-- from OpportunityStageDetails                                 
-- where numOppId=@OpportunityID  order by numStagePercentage,numOppstageid                                
--                                 
--                                
--                                
--                                
--end                                
        RETURN
end
GO
