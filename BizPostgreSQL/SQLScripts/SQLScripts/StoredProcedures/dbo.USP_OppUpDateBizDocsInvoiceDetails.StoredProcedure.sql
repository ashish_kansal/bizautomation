/****** Object:  StoredProcedure [dbo].[USP_OppUpDateBizDocsInvoiceDetails]    Script Date: 07/26/2008 16:20:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created By Siva
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_oppupdatebizdocsinvoicedetails')
DROP PROCEDURE usp_oppupdatebizdocsinvoicedetails
GO
CREATE PROCEDURE [dbo].[USP_OppUpDateBizDocsInvoiceDetails]                      
(                      
 @numOppBizDocsId as numeric(9)=null,                      
 @UserCntID as numeric(9) =null,                     
 @Comments as varchar(1000)=null,
 --@numBizDocStatus as numeric(9),
 @numShipCompany as numeric(9),
 @vcTrackingURL as varchar(1000)            
  
)                      
as                      
/* Modifed to update selective fields */
DECLARE @sql VARCHAR(4000)
SET @sql = 'update OpportunityBizDocs Set '


IF(@UserCntID>0)
BEGIN
	SET @sql =@sql + ' numModifiedBy='+ CONVERT(VARCHAR(10),@UserCntID) +', '
END
--if Comments removed then saving blank values
--IF(LEN(@Comments)>0)
--BEGIN
	SET @sql =@sql + ' vcComments='''+ @Comments +''', '
--END
--IF(@numBizDocStatus>0) --Commented, Reason:Can not reset bizdoc status to select one 
--BEGIN
	--SET @sql =@sql + ' numBizDocStatus='+ CONVERT(VARCHAR(10),@numBizDocStatus) +', '
--END
IF(@numShipCompany>0)
BEGIN
	SET @sql =@sql + ' numShipVia='+ CONVERT(VARCHAR(10),@numShipCompany) +', '
END
IF(LEN(@vcTrackingURL)>0)
BEGIN
	SET @sql =@sql + ' vcTrackingURL=''' + @vcTrackingURL + ''', ' 
END
SET @sql=SUBSTRING(@sql,0,LEN(@sql)) + ' '
SET @sql =@sql + 'where numOppBizDocsId= '+CONVERT(VARCHAR(10),@numOppBizDocsId)

PRINT @sql
EXEC(@sql)

--update OpportunityBizDocs
--	set  vcPurchaseOdrNo=@vcPurchaseOdrNo,                      
--	numShipVia=@numShipCompany,                      
--	numModifiedBy=@UserCntID,                      
--	vcComments=@Comments,                      
--	dtModifiedDate=getutcdate(),
--    numShipDoc= @numShipDoc,
--    numBizDocStatus=@numBizDocStatus,
--    vcTrackingURL=@vcTrackingURL                     
--	where numOppBizDocsId=@numOppBizDocsId
--GO
