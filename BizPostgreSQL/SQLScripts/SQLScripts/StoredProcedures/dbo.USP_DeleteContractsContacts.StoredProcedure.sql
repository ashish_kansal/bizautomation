/****** Object:  StoredProcedure [dbo].[USP_DeleteContractsContacts]    Script Date: 07/26/2008 16:15:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_deletecontractscontacts')
DROP PROCEDURE usp_deletecontractscontacts
GO
CREATE PROCEDURE [dbo].[USP_DeleteContractsContacts]
@numContactID as numeric(18),
@numContractID as numeric(18),
@numDomainID as numeric(18)
as
delete contractsContact where 
numcontactId = @numContactID
and numContractID=@numContractID and numDomainID=@numDomainID
GO
