/****** Object:  StoredProcedure [dbo].[USP_UpdateOppItems]    Script Date: 07/26/2008 16:21:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_updateoppitems')
DROP PROCEDURE usp_updateoppitems
GO
CREATE PROCEDURE [dbo].[USP_UpdateOppItems]
@numOppID as numeric(9)=0,
@numItemCode as numeric(9)=0,
@numUnitHour as FLOAT=0,
@monPrice as DECIMAL(20,5),
@vcItemName VARCHAR(300),
@vcModelId VARCHAR(200),
@vcPathForTImage VARCHAR(300),
@vcItemDesc as varchar(1000),
@numWarehouseItmsID as numeric(9),
@ItemType as Varchar(50),
@DropShip as bit=0,
@numUOM as numeric(9)=0,
@numClassID as numeric(9)=0,
@numProjectID as numeric(9)=0,
@vcNotes varchar(1000)=null,
@dtItemRequiredDate DATETIME = null,
@vcAttributes VARCHAR(MAX),
@vcAttributeIDs VARCHAR(MAX)
as                                                   
	SET @vcNotes = (CASE WHEN @vcNotes='&nbsp;' THEN '' ELSE @vcNotes END)
	SET @vcItemDesc = (CASE WHEN ISNULL(@vcItemDesc,'')='' THEN ISNULL((SELECT txtItemDesc FROM Item WHERE numItemCode=@numItemCode),'') ELSE @vcItemDesc END)
	
   if @numWarehouseItmsID=0 set @numWarehouseItmsID=null 
	
	DECLARE @vcManufacturer VARCHAR(250)
	SELECT @vcManufacturer = vcManufacturer FROM item WHERE [numItemCode]=@numItemCode
    insert into OpportunityItems                                                    
	(numOppId,numItemCode,vcNotes,numUnitHour,monPrice,numCost,monTotAmount,vcItemDesc,numWarehouseItmsID,vcType,bitDropShip,bitDiscountType,fltDiscount,monTotAmtBefDiscount,[vcItemName],[vcModelID],[vcPathForTImage],[vcManufacturer],numUOMId,numClassID,numProjectID,numSortOrder,ItemRequiredDate,vcAttributes,vcAttrValues)
	values
	(@numOppID,@numItemCode,@vcNotes,@numUnitHour,@monPrice,@monPrice,@numUnitHour*@monPrice,@vcItemDesc,@numWarehouseItmsID,@ItemType,@DropShip,0,0,@numUnitHour*@monPrice,@vcItemName,@vcModelId,@vcPathForTImage,@vcManufacturer,@numUOM,@numClassID,@numProjectID,ISNULL((SELECT MAX(numSortOrder) FROM OpportunityItems WHERE numOppID=@numOppID),0) + 1,@dtItemRequiredDate,@vcAttributes,@vcAttributeIDs)
	
	select SCOPE_IDENTITY()
	                                 
	update OpportunityMaster set  monPamount=(select sum(monTotAmount)from OpportunityItems where numOppId=@numOppID)                                                     
	where numOppId=@numOppID
GO
