/****** Object:  StoredProcedure [dbo].[GetAuthorativeBDocs]    Script Date: 07/26/2008 16:14:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='getauthorativebdocs')
DROP PROCEDURE getauthorativebdocs
GO
CREATE PROCEDURE [dbo].[GetAuthorativeBDocs]    
@numDivID as numeric,    
@numDomainId as numeric    
    
as    
    
Select vcBizDocId,numOppBizDocsId From OpportunityMaster  OM    
Inner join opportunityBizDocs OBD on OM.numOppId=OBD.numOppId Where   OM.numDivisionId=@numDivID and    
OBD.numBizDocId in (Select numAuthoritativeSales From AuthoritativeBizdocs where numDomainid = @numDomainId)
GO
