/****** Object:  StoredProcedure [dbo].[usp_CampaignOverlapChecking]    Script Date: 07/26/2008 16:15:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- created by anoop jayaraj  
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_campaignoverlapchecking')
DROP PROCEDURE usp_campaignoverlapchecking
GO
--Obsolete Procedure, reason - bug id 465 #2
CREATE PROCEDURE [dbo].[usp_CampaignOverlapChecking]        
@numDomainID as numeric(9)=0,
@intLaunchDate as datetime,        
@intEndDate as datetime,      
@byteMode as tinyint,      
@numCampaignID as numeric(9)=0       
as        
      
if @byteMode=0 ---When Adding a new record      
begin      
select count(*) from CampaignMaster where [numDomainID]=@numDomainID and ((@intLaunchDate between intLaunchDate and intEndDate )        
or (@intEndDate  between intLaunchDate and intEndDate)) AND [bitIsOnline]<>1     
end      
      
if @byteMode=1 -- Editing a Record      
begin      
select count(*) from CampaignMaster where [numDomainID]=@numDomainID and numCampaignID<>@numCampaignID and ((@intLaunchDate between intLaunchDate and intEndDate )        
or (@intEndDate  between intLaunchDate and intEndDate)) AND	[bitIsOnline]<>1
END

IF @byteMode=2 -- Online Campaign No need to validate date range
BEGIN
	SELECT 0
END
GO
