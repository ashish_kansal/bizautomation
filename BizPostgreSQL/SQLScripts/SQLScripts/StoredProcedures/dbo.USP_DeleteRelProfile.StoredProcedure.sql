/****** Object:  StoredProcedure [dbo].[USP_DeleteRelProfile]    Script Date: 07/26/2008 16:15:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj  
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_deleterelprofile')
DROP PROCEDURE usp_deleterelprofile
GO
CREATE PROCEDURE [dbo].[USP_DeleteRelProfile]  
@numRelProID as numeric(9)  
as  
delete  from RelProfile  where numRelProID=@numRelProID
GO
