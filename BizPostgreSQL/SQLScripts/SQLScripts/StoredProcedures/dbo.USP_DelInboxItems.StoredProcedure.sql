/****** Object:  StoredProcedure [dbo].[USP_DelInboxItems]    Script Date: 07/26/2008 16:15:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_delinboxitems')
DROP PROCEDURE usp_delinboxitems
GO
CREATE PROCEDURE [dbo].[USP_DelInboxItems]       
@numEmailHstrID as numeric(9)  ,           
@mode as bit,
@numDomainID AS NUMERIC(18,0),
@numUserCntID AS NUMERIC(18,0)            
as              
   
if @mode =1  
begin             
/*Bug reported by Audiogenx that they can not delete mails, cause: Email was added to correspondance of Project*/
 DELETE FROM dbo.Correspondence WHERE numEmailHistoryID = @numEmailHstrID 

 delete emailHistory               
 where numEmailHstrID=@numEmailHstrID         
       
 delete EmailHStrToBCCAndCC               
 where numEmailHstrID=@numEmailHstrID       
       
 delete EmailHstrAttchDtls               
 where numEmailHstrID=@numEmailHstrID      
 end  
else  
begin  
 --OLD NODE ID REQUIRED TO GET FOLDER NAME WHERE EMAIL WAS BEFORE MOVED TO DELETED FOLDER
 --BECAUSE TO DELETE EMAIL IN GAMIL FOLDER NAME IS REQUIRED
 UPDATE EmailHistory SET numOldNodeID=numNodeId WHERE numEmailHstrID=@numEmailHstrID 
 update emailHistory set tinttype=3,numNodeid=ISNULL((SELECT numNodeID FROM InboxTreeSort WHERE numDomainID = @numDomainID AND numUserCntID=@numUserCntID AND bitSystem=1 AND numFixID=2),2) where numEmailHstrID=@numEmailHstrID     
end
GO
