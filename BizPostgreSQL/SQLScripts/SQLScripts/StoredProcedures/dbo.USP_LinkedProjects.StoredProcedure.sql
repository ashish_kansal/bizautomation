/****** Object:  StoredProcedure [dbo].[USP_LinkedProjects]    Script Date: 07/26/2008 16:19:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- created by anoop jayaraj      
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_linkedprojects')
DROP PROCEDURE usp_linkedprojects
GO
CREATE PROCEDURE [dbo].[USP_LinkedProjects]      
@ByteMode as int = 0  ,
@numOppID as numeric(9)=0 

     
as      
    
if @ByteMode =0 ---For projects    
 begin     
	  select pm.numProId,vcProjectName,'Project' as [Type] from  ProjectsMaster Pm    
	  join ProjectsOpportunities Po on Pm.numProid = po.numproId    
	  where po.numOppId=@numOppID 

	  union all      

	  select c.numCaseId,vcCaseNumber,'Case' as [Type] from  Cases C    
	  join caseOpportunities co on C.numCaseId = Co.numCaseId    
	  where Co.numOppId=@numOppID  
   
 end
GO
