/****** Object:  StoredProcedure [dbo].[usp_GetOppItemDetails]    Script Date: 07/26/2008 16:17:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--------------------------------------
--usp_GetOppItemDetails








GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getoppitemdetails')
DROP PROCEDURE usp_getoppitemdetails
GO
CREATE PROCEDURE [dbo].[usp_GetOppItemDetails]
	@numOppId numeric(9),
	@numItemCode numeric=0,
	@numCountFlg numeric=0   
	--@numUserCreatedby numeric Output
--
AS

DECLARE 
	@numCount numeric(9),
	@numUserCreatedby  numeric(9),
	@numTerId numeric(9)
	
	--Implementing the Security rights
	SELECT @numusercreatedby=numcreatedby from opportunitymaster where numoppid=@numoppid
	--SELECT @numusercreatedby as UserId	

	SELECT @numterid=numterid from divisionmaster where numdivisionid in 
	(select numdivisionid from opportunitymaster where numoppid=@numOppid)
	--SELECT @numterid as TerritoryId

	IF @numCountFlg=0
		BEGIN
			IF @numItemCode =0 
				BEGIN
					SELECT @numterid as TerritoryId,@numusercreatedby as UserId, a.*,b.vcItemName,b.txtItemDesc,b.numitemcode,b.charitemType FROM opportunityitems a INNER JOIN item b ON a.numitemcode=b.numitemcode
					WHERE a.numoppid=@numoppid

				END
			ELSE
				BEGIN
					SELECT @numterid as TerritoryId,@numusercreatedby as UserId, a.*,b.vcItemName,b.txtItemDesc,b.numitemcode,b.charitemType FROM opportunityitems a INNER JOIN item b ON a.numitemcode=b.numitemcode
					WHERE a.numoppid=@numoppid
					AND a.numItemCode=@numItemCode
						
	END
		END
	ELSE
		BEGIN
			--SELECT @numcount=count(a.*),b.numitemcode FROM opportunityitems a,item b
			--			WHERE a.numitemcode=b.numitemcode and a.numoppid=@numoppid
			SELECT @numcount= COUNT(*) from opportunityItems
			WHERE numOppId=@numOppId
			SELECT @numCount
			
	END
GO
