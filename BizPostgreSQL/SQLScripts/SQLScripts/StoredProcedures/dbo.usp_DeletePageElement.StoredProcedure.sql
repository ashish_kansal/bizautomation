/****** Object:  StoredProcedure [dbo].[usp_DeletePageElement]    Script Date: 07/26/2008 16:15:38 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_deletepageelement')
DROP PROCEDURE usp_deletepageelement
GO
CREATE PROCEDURE [dbo].[usp_DeletePageElement]
	@numPageID INT,
	@numModuleID INT,
	@vcParentElementInitiatingAction VARCHAR(20) = '',
	@vcChildElementInitiatingAction VARCHAR(20) = ''   
--
AS
	BEGIN
		DELETE FROM 	PageActionElements
		WHERE numModuleID = @numModuleID
		AND numPageID = @numPageID
		AND vcElementInitiatingAction = @vcParentElementInitiatingAction
		AND vcChildElementInitiatingAction = @vcChildElementInitiatingAction
	END
GO
