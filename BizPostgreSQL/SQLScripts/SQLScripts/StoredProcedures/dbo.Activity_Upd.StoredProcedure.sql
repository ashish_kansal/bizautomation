/****** Object:  StoredProcedure [dbo].[Activity_Upd]    Script Date: 07/26/2008 16:14:24 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
/*
**  Updates an existing Activity by its primary key number.
*/
GO

--Alter Table Activity 
--	   add AttendeesEmail varchar(MAX)



--Alter Table Activity 
--	   add PrimaryAttendeeEmail varchar(50)

IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='activity_upd')
DROP PROCEDURE activity_upd
GO
CREATE PROCEDURE [dbo].[Activity_Upd]
	@DataKey		integer,		-- the key of the Activity to be updated
	@AllDayEvent		bit,		-- whether this is an all-day event
	@ActivityDescription	ntext,		-- additional information about the activity
	@Duration		integer,		-- length of activity in seconds
	@Location		nvarchar(64),	-- where the activity takes place
	@StartDateTimeUtc	datetime,	-- when the activity begins
	@Subject		nvarchar(255),	-- brief description of the activity
	@EnableReminder		bit,		-- whether this activity should notify the user
	@ReminderInterval	integer,		-- how long before start time (in seconds) to notify the user
	@ShowTimeAs		integer,		-- how to display the time use of this activity to the user in DayView
	@Importance		integer,		-- priority that this activity has
	@Status			integer,		-- reminder notification status (expired, pending, etc.)
	@RecurrenceKey		integer,		-- relates this activity to its recurrence pattern info, if any
	@VarianceKey		uniqueidentifier,	-- corelates all variances of a recurring activity, otherwise null
	@ItemIdOccur varchar(250),
	@tintMode tinyint=0,
	@Color tinyint=0,
	@HtmlLink varchar(500)
AS
BEGIN
	-- NOTE: This stored procedure does not support the changing
	-- of existing Recurrence ID's (splitting).  It does support
	-- adding a Recurrence to a formerly non-recurring Activity,
	-- since it had no pre-existing variances.
	--
	-- Data Provider-level logic must switch to a split-enhanced
	-- stored procedure when conditions require.

IF @tintMode=0
BEGIN
	UPDATE
		[Activity]
	SET 
		[Activity].[AllDayEvent] = @AllDayEvent, 
		[Activity].[ActivityDescription] = @ActivityDescription, 
		[Activity].[Duration] = @Duration, 
		[Activity].[Location] = @Location, 
		[Activity].[StartDateTimeUtc] = @StartDateTimeUtc, 
		[Activity].[Subject] = @Subject, 
		[Activity].[EnableReminder] = @EnableReminder, 
		[Activity].[ReminderInterval] = @ReminderInterval,
		[Activity].[ShowTimeAs] = @ShowTimeAs,
		[Activity].[Importance] = @Importance,
		[Activity].[Status] = @Status,
		[Activity].[RecurrenceID] = @RecurrenceKey,
		[Activity].[VarianceID] = @VarianceKey,
	    [Activity].Color=@Color,
		[Activity].HtmlLink=@HtmlLink
	WHERE
		( [Activity].[ActivityID] = @DataKey );
END
ELSE IF @tintMode=1
BEGIN
    UPDATE
		[Activity]
	SET 
		[Activity].[Duration] = @Duration, 
		[Activity].[StartDateTimeUtc] = @StartDateTimeUtc
	WHERE
		( [Activity].[ActivityID] = @DataKey );
END


IF EXISTS (SELECT numActivityID FROM dbo.Communication WHERE numActivityID=@DataKey)
BEGIN
	Declare @dtStartTime AS DATETIME;
	Declare @dtEndtime AS DATETIME;
	
	SET @dtStartTime=@StartDateTimeUtc
	SET @dtEndtime=DATEADD(second,@Duration,@dtStartTime)
	
	UPDATE dbo.Communication SET dtStartTime=@dtStartTime,dtEndTime=@dtEndtime WHERE numActivityID=@DataKey
END	

END


GO
