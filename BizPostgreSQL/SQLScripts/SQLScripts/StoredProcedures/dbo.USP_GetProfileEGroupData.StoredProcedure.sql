/****** Object:  StoredProcedure [dbo].[USP_GetProfileEGroupData]    Script Date: 07/26/2008 16:18:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getprofileegroupdata')
DROP PROCEDURE usp_getprofileegroupdata
GO
CREATE PROCEDURE [dbo].[USP_GetProfileEGroupData]   
  @strcontact as text=null,    
  @numDomainID as numeric,  
  @numUserCntID as numeric,  
  @numGroupID as numeric  
as  
  
  
if  @numGroupID <> 0    
begin  

  
  
if convert(varchar(10),@strcontact) <> ''  
begin                              
   DECLARE @hDoc4 int                                      
   EXEC sp_xml_preparedocument @hDoc4 OUTPUT, @strcontact                                      
                                      
select PGD.numContactId,ADC.VcFirstName,ADC.vcLastName,c.vcCompanyName,adc.vcEmail    
  FROM ProfileEGroupDTL PGD                                   
join AdditionalContactsInformation ADC on ADC.numContactID = PGD.numContactID
  JOIN DivisionMaster DM ON ADC.numDivisionId = DM.numDivisionID                                  
  JOIN CompanyInfo C ON DM.numCompanyID = C.numCompanyId              
join ProfileEmailGroup PEG on PGD.numEmailGroupID = PEG.numEmailGroupID  
  
where PGD.numContactID not in (
                                         
    select X.numContactId from  
(SELECT * FROM OPENXML (@hDoc4,'/NewDataSet/Table',2)                                      
 WITH  (                                  
  numContactId numeric(9)))X  ) and PGD.numEmailGroupID = @numGroupID
     
 
end   
                                      
                                        
   EXEC sp_xml_removedocument @hDoc4    
end
GO
