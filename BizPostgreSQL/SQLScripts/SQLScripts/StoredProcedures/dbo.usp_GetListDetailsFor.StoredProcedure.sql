/****** Object:  StoredProcedure [dbo].[usp_GetListDetailsFor]    Script Date: 07/26/2008 16:17:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getlistdetailsfor')
DROP PROCEDURE usp_getlistdetailsfor
GO
CREATE PROCEDURE [dbo].[usp_GetListDetailsFor]
	@strListMasterName varchar(50),
	@numDomainID numeric,
	@bitSortbyID bit=0   
--
AS
--This procedure will return the list details which will be filled in various combo.
BEGIN
	IF @bitSortbyID=0
	BEGIN
		SELECT LD.numListItemID, LD.vcData 
			FROM ListDetails LD INNER JOIN ListMaster LM ON LD.numListID = LM.numListID
			WHERE LM.vcListName = @strListMasterName
				AND LD.numDomainID = @numDomainID
			ORDER BY LD.vcData
	END
	ELSE
	BEGIN
		SELECT LD.numListItemID, LD.vcData 
			FROM ListDetails LD INNER JOIN ListMaster LM ON LD.numListID = LM.numListID
			WHERE LM.vcListName = @strListMasterName
				AND LD.numDomainID = @numDomainID
			ORDER BY LD.numListItemID
	END
END
GO
