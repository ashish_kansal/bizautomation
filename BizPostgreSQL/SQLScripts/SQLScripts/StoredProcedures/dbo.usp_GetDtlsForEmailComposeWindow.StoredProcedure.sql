/****** Object:  StoredProcedure [dbo].[usp_GetDtlsForEmailComposeWindow]    Script Date: 07/26/2008 16:17:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created By Anoop Jayaraj                        
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getdtlsforemailcomposewindow')
DROP PROCEDURE usp_getdtlsforemailcomposewindow
GO
CREATE PROCEDURE [dbo].[usp_GetDtlsForEmailComposeWindow]                                  
 @strContactID varchar(7000)=''                                                              
AS   
declare @strSQL as varchar(8000)
set @strSQL='select D.numDivisionID,A.numContactID,vcFirstName,vcLastName,vcEmail as Email,vcCompanyName,vcCompanyName +'' ,<br>''+                                                                                           
 VcShipStreet + '' <br>''+                                                                                           
 VcShipCity+ '' ,''+                                                                                           
 isnull(dbo.fn_GetState(vcShipState),'''') + '' ''+                                                                                          
 vcShipPostCode+'' <br>''+                                                   
 isnull(dbo.fn_GetListItemName(vcShipCountry),'''') as Ship,
 '''' as [Opt-Out]
 from CompanyInfo C
join DivisionMaster D
on D.numCompanyID=C.numCompanyID
join AdditionalContactsInformation A
on A.numDivisionID=D.numDivisionID
where numContactID in ('+@strContactID +')' 
exec   (@strSQL)
GO
