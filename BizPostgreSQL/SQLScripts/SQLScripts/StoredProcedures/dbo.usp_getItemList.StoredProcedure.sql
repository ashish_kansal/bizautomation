/****** Object:  StoredProcedure [dbo].[usp_getItemList]    Script Date: 07/26/2008 16:17:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--EXEC usp_getItemList 72,'S',2
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getitemlist')
DROP PROCEDURE usp_getitemlist
GO
CREATE PROCEDURE [dbo].[usp_getItemList]
@numDomainId as numeric(9),
@varFilter as varchar(30),
@type as tinyint =0
as 

if @type = 0
	select numItemCode,vcItemName from item where numDomainid=@numDomainId  and vcItemName like @varFilter

if @type = 1
	select numwareHouseId,vcWareHouse from Warehouses where numDomainid=1

if @type = 2
	select numItemCode,vcItemName,ISNULL(monListPrice,0) AS monListPrice from item where numDomainid=@numDomainId and charItemType = @varFilter ORDER BY [vcItemName]

if @type = 3
	select CONVERT(VARCHAR(10),numItemCode) + '~' + CONVERT(VARCHAR(10),ISNULL(monListPrice,0)) AS [numItemCode],vcItemName from item where numDomainid=@numDomainId and charItemType = @varFilter ORDER BY [vcItemName]

GO
