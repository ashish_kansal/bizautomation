/****** Object:  StoredProcedure [dbo].[usp_CheckCompany]    Script Date: 07/26/2008 16:15:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_checkcompany')
DROP PROCEDURE usp_checkcompany
GO
CREATE PROCEDURE [dbo].[usp_CheckCompany]
	@numListId numeric=0,
	@numDomainId numeric=0,
	@numCompId numeric=0
--   	
AS
	DECLARE @numcompanytype numeric
	IF @numCompId=0 
		BEGIN
			SELECT COUNT(*) FROM companyinfo WHERE numCompanyType=@numListId AND numDomainid=@numDomainid
		END
	ELSE
		BEGIN
			SELECT @numcompanytype= numCompanyType FROM companyinfo WHERE numcompanyid=@numCompId
			IF @numcompanytype=93
				BEGIN
					SELECT -1
				END
			ELSE
				BEGIN
					SELECT 0
				END
		END
GO
