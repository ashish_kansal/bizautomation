/****** Object:  StoredProcedure [dbo].[USP_UpdateOppSerializedItems]    Script Date: 07/26/2008 16:21:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj  
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_updateoppserializeditems')
DROP PROCEDURE usp_updateoppserializeditems
GO
CREATE PROCEDURE [dbo].[USP_UpdateOppSerializedItems]  
@numOppItemTcode as numeric(9)=0,  
@numOppID as numeric(9)=0,  
@strItems as text='',
@OppType as tinyint  
as  
--THIS PROCEDURE IS NOT USED AND ITS IMPLEMETATION IS OLD AND NOT RELEVENT TO CURRENT STRUCTURE SO DO NOT USE IT
--declare @hDoc as int                
--EXEC sp_xml_preparedocument @hDoc OUTPUT, @strItems    
--if   @OppType=2
--begin             
                
--    update WareHouseItmsDTL set                                             
--     numWareHouseItemID=X.numWareHouseItemID,                                           
--     vcSerialNo=X.vcSerialNo                                                               
--      From (SELECT numWareHouseItmsDTLID as WareHouseItmsDTLID,numWareHouseItemID,vcSerialNo,Op_Flag                                           
--     FROM OPENXML(@hDoc,'/NewDataSet/SerializedItems[Op_Flag=2]',2)                                             
--      with(numWareHouseItmsDTLID numeric(9),                                            
--      numWareHouseItemID numeric(9),                                            
--      vcSerialNo varchar(100),                
--      Op_Flag tinyint))X                                             
--    where  numWareHouseItmsDTLID=X.WareHouseItmsDTLID                
                
                
-- delete from  WareHouseItmsDTL where numWareHouseItmsDTLID in (SELECT numWareHouseItmsDTLID                                            
--     FROM OPENXML(@hDoc,'/NewDataSet/SerializedItems[Op_Flag=3]',2)                                             
--      with(numWareHouseItmsDTLID numeric(9),                                            
--      numWareHouseItemID numeric(9),                                            
--      vcSerialNo varchar(100),                
--      Op_Flag tinyint))  
  
       
--  delete from  OppWarehouseSerializedItem where numWareHouseItmsDTLID in (SELECT numWareHouseItmsDTLID                                            
--     FROM OPENXML(@hDoc,'/NewDataSet/SerializedItems[Op_Flag=2]',2)                                             
--      with(numWareHouseItmsDTLID numeric(9),                                            
--      numWareHouseItemID numeric(9),                                            
--      vcSerialNo varchar(100),                
--      Op_Flag tinyint))  
--  delete from  OppWarehouseSerializedItem where numWareHouseItmsDTLID in (SELECT numWareHouseItmsDTLID                                            
--     FROM OPENXML(@hDoc,'/NewDataSet/SerializedItems[Op_Flag=3]',2)                                             
--      with(numWareHouseItmsDTLID numeric(9),                                            
--      numWareHouseItemID numeric(9),                                            
--      vcSerialNo varchar(100),                
--      Op_Flag tinyint))  
    
-- Insert into OppWarehouseSerializedItem(numWarehouseItmsDTLID,numOppID,numOppItemID,numWarehouseItmsID)                
--  (SELECT numWareHouseItmsDTLID,@numOppID,@numOppItemTcode,numWareHouseItemID                                            
--      FROM OPENXML(@hDoc,'/NewDataSet/SerializedItems[Op_Flag=2]',2)                                             
--      with(numWareHouseItmsDTLID numeric(9),                                            
--      numWareHouseItemID numeric(9),                      
--      vcSerialNo varchar(100),                
--      Op_Flag tinyint))   
  
  
  
--declare  @rows as integer            
            
--SELECT @rows=count(*) FROM OPENXML (@hDoc,'/NewDataSet/CusFlds',2)                      
-- WITH (Fld_ID numeric(9),Fld_Value varchar(100),RecId numeric(9))            
                                              
             
--if @rows>0            
--begin            
-- Create table #tempTable ( ID INT IDENTITY PRIMARY KEY,                                                          
-- Fld_ID numeric(9),            
-- Fld_Value varchar(100),            
-- RecId numeric(9),        
-- bitSItems bit                                             
-- )             
-- insert into #tempTable (Fld_ID,Fld_Value,RecId,bitSItems)            
-- SELECT * FROM OPENXML (@hDoc,'/NewDataSet/CusFlds',2)                      
-- WITH (Fld_ID numeric(9),Fld_Value varchar(100),RecId numeric(9),bitSItems bit)               
            
-- delete CFW_Fld_Values_Serialized_Items from CFW_Fld_Values_Serialized_Items C            
-- inner join #tempTable T            
-- on   C.Fld_ID=T.Fld_ID and C.RecId=T.RecId and bitSerialized=bitSItems             
            
-- drop table #tempTable            
                               
-- insert into CFW_Fld_Values_Serialized_Items(Fld_ID,Fld_Value,RecId,bitSerialized)                      
-- select Fld_ID,isnull(Fld_Value,'') as Fld_Value,RecId,bitSItems from(SELECT * FROM OPENXML (@hDoc,'/NewDataSet/CusFlds',2)                      
-- WITH (Fld_ID numeric(9),Fld_Value varchar(100),RecId numeric(9),bitSItems bit)) X             
--end   
--end
--else if @OppType=1
--begin

--update WareHouseItmsDTL set tintStatus=0 where numWareHouseItmsDTLID in (select numWarehouseItmsDTLID from OppWarehouseSerializedItem where numOppItemID=@numOppItemTcode)

--delete from OppWarehouseSerializedItem where numOppItemID=@numOppItemTcode

-- Insert into OppWarehouseSerializedItem(numWarehouseItmsDTLID,numOppID,numOppItemID,numWarehouseItmsID,numQty)                
--  (SELECT numWareHouseItmsDTLID,@numOppID,@numOppItemTcode,numWareHouseItemID,UsedQty                                            
--      FROM OPENXML(@hDoc,'/NewDataSet/SerializedItems',2)                                             
--      with(numWareHouseItmsDTLID numeric(9),                                            
--      numWareHouseItemID numeric(9),UsedQty NUMERIC(9)))
      
--update WareHouseItmsDTL set tintStatus=1 where numWareHouseItmsDTLID in 
--(SELECT numWareHouseItmsDTLID                                            
--      FROM OPENXML(@hDoc,'/NewDataSet/SerializedItems',2)                                             
--      with(numWareHouseItmsDTLID numeric(9),UsedQty NUMERIC(9),TotalQty NUMERIC(9)) WHERE (TotalQty-UsedQty)=0)

--end  
  
--EXEC sp_xml_removedocument @hDoc
GO
