/****** Object:  StoredProcedure [dbo].[usp_ManageDivisionsPopUps]    Script Date: 07/26/2008 16:19:46 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_managedivisionspopups')
DROP PROCEDURE usp_managedivisionspopups
GO
CREATE PROCEDURE [dbo].[usp_ManageDivisionsPopUps]      
 -- This Procedure will INSERT a new Company OR UPDATE existing Company into the CompanyInfo Table.
-- If inserted, it will return the auto-generated ID for that company.
-- The CompanyID, if passed will ensure that the UPDATE Query is Executed.
-- If the Company ID is not passed, it will execute INSERT Query.
-- Declare the Input Variables.
	@numCompanyId numeric=0,
	@vcCompanyName varchar (100)='',
	@numDivisionID numeric=0
--
AS
BEGIN
	DECLARE @strSQL varchar (1000)	--Declared to use as String for SQL Query.
	DECLARE @numCompanyIDInternal numeric
	DECLARE @extractedNumCompanyid numeric 
	Declare @numGetDivision numeric
	Set @numGetDivision = 0
	
	select @extractedNumCompanyid = max(numcompanyid) from companyinfo where  vccompanyname  = @vcCompanyName
	update divisionmaster set numcompanyid =  @extractedNumCompanyid where numcompanyid = @numCompanyId and numDivisionID = @numDivisionID
	
	Select @numGetDivision = count(numDivisionID) from DivisionMaster where numcompanyid = @numCompanyId
	If @numGetDivision = 0
	Begin
		Delete from companyinfo where numCompanyID = @numCompanyId
	End
	
	--delete from companyinfo where numcompanyid = @extractedNumCompanyid
	
END
GO
