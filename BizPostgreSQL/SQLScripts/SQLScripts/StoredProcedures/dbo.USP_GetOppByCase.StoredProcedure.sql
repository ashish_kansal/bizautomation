/****** Object:  StoredProcedure [dbo].[USP_GetOppByCase]    Script Date: 07/26/2008 16:17:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getoppbycase')
DROP PROCEDURE usp_getoppbycase
GO
CREATE PROCEDURE [dbo].[USP_GetOppByCase]  
@numCaseId numeric,  
@numDomainId numeric  
as   
select c.numoppid ,M.vcPOppName  
from caseopportunities C  
join opportunitymaster M  
on c.numOppid= M.numoppid  
where c.numCaseId=@numCaseId and c.numDomainid=@numDomainId  and tintopptype =1
GO
