/****** Object:  StoredProcedure [dbo].[USP_GetRecOwnerUsingRules]    Script Date: 07/26/2008 16:18:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--craeted by anoop jayaraj        
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getrecownerusingrules')
DROP PROCEDURE usp_getrecownerusingrules
GO
CREATE PROCEDURE [dbo].[USP_GetRecOwnerUsingRules]        
@numDomainID as numeric(9)=0,        
@strData as text        
        
as        
declare @numRecOwner as numeric(9)        
set @numRecOwner=0        
        
        
        
        
        
Create table #tempTable ( ID INT IDENTITY PRIMARY KEY,                                                    
 vcDbColumnName varchar(100) Collate Database_default NULL,        
 vcDbColumnText varchar(100) Collate Database_default NULL)          
if convert(varchar(10),@strData) <>''                                          
begin                                          
  DECLARE @hDoc2 int                                          
  EXEC sp_xml_preparedocument @hDoc2 OUTPUT, @strData                                          
                                          
  insert into #tempTable                                          
  (vcDbColumnName,vcDbColumnText)                                          
                                           
  SELECT *FROM OPENXML (@hDoc2,'/NewDataSet/Table',2)                                          
  WITH  (                                          
  vcDbColumnName varchar(100),                                          
  vcDbColumnText varchar(100))                                         
                                          
                                          
 EXEC sp_xml_removedocument @hDoc2              
    
 declare @numRoutID as varchar(15)        
 declare @tintPriority as varchar(4)        
 declare @vcDBColumnName as varchar(100)  
 declare @rowcount AS tinyint        
 declare @tintEqualTo as varchar(4)        
 set @numRoutID=0        
 select top 1 @numRoutID=numRoutID,@tintPriority=tintPriority,@vcDBColumnName=vcDBColumnName,@tintEqualTo=tintEqualTo         
 from RoutingLeads where numDomainID=@numDomainID and bitDefault=0 order by tintPriority         
         
 while @numRoutID>0        
 begin             
  if @tintEqualTo=1  and   (@vcDBColumnName like '%State%')    
  begin     
 select top 1 @numRecOwner=numEmpID from RoutingLeads R        
    Join RoutingLeadDetails DTL        
    on DTL.numRoutID=R.numRoutID        
    join RoutinLeadsValues V        
    on V.numRoutID=R.numRoutID        
    join #tempTable T        
    on T.vcDbColumnName=R.vcDbColumnName and T.vcDbColumnText=V.vcValue        
    where R.numRoutID=@numRoutID and intAssignOrder=1          
  end     
  else if @tintEqualTo=1    
   begin        
       select top 1 @numRecOwner=numEmpID from RoutingLeads R        
    Join RoutingLeadDetails DTL        
    on DTL.numRoutID=R.numRoutID        
    join #tempTable T        
    on T.vcDbColumnName=R.vcDbColumnName and T.vcDbColumnText=R.numValue        
    where R.numRoutID=@numRoutID  and intAssignOrder=1     
   end      
  else if @tintEqualTo=2        
  begin     
   select top 1 @numRecOwner=numEmpID from RoutingLeads R        
   Join RoutingLeadDetails DTL        
   on DTL.numRoutID=R.numRoutID        
   join RoutinLeadsValues V        
   on V.numRoutID=R.numRoutID        
   join #tempTable T        
   on T.vcDbColumnName=R.vcDbColumnName         
   where R.numRoutID=@numRoutID  and T.vcDbColumnText like '%'+ V.vcValue+'%' and intAssignOrder=1         
          
  end        
  else if @tintEqualTo=3        
  begin        
           
   select top 1 @numRecOwner=numEmpID from RoutingLeads R        
   Join RoutingLeadDetails DTL        
   on DTL.numRoutID=R.numRoutID        
   join RoutinLeadsValues V        
   on V.numRoutID=R.numRoutID        
   join #tempTable T        
   on T.vcDbColumnName=R.vcDbColumnName and T.vcDbColumnText=V.vcValue        
   where R.numRoutID=@numRoutID and intAssignOrder=1        
  end        
           
  select top 1 @numRoutID=numRoutID,@tintPriority=tintPriority,@vcDBColumnName=vcDBColumnName,@tintEqualTo=tintEqualTo         
  from RoutingLeads where numDomainID=@numDomainID and bitDefault=0  and tintPriority>@tintPriority  order by tintPriority   
  set @rowcount =@@rowcount       
  if @numRecOwner>0         
  begin        
 update RoutingLeadDetails set intAssignOrder=intAssignOrder-1 where numRoutID=@numRoutID      
 update RoutingLeadDetails set intAssignOrder=((select max(intAssignOrder) from RoutingLeadDetails where numRoutID=@numRoutID)+1) where intAssignOrder=0      
   set @numRoutID=0        
  end        
  if @rowcount=0 set @numRoutID=0        
 end        
        
        
 if @numRecOwner=0        
 begin        
  select @numRecOwner=numEmpId from RoutingLeadDetails DTL        
  join RoutingLeads R        
  on R.numRoutID=DTL.numRoutID        
  where numDomainID=@numDomainID and bitDefault=1        
 end        
 drop table  #tempTable        
 select @numRecOwner        
                                                               
end
ELSE
BEGIN
	select @numRecOwner=numEmpId from RoutingLeadDetails DTL        
	join RoutingLeads R        
	on R.numRoutID=DTL.numRoutID        
	where numDomainID=@numDomainID and bitDefault=1 
END
GO
