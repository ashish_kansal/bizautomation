/****** Object:  StoredProcedure [dbo].[USP_InsertImapUserDtls]    Script Date: 07/26/2008 16:19:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_insertimapuserdtls')
DROP PROCEDURE usp_insertimapuserdtls
GO
CREATE PROCEDURE [dbo].[USP_InsertImapUserDtls]
    @vcImapPassword AS VARCHAR(100),
    @vcImapServerUrl AS VARCHAR(200),
    @numImapSSLPort AS NUMERIC,
    @bitImapSsl AS BIT,
    @bitImap AS BIT,
    @numUserCntId AS NUMERIC(9),--tintMode=0 then supply UserID instead of UserContactID
    @numDomainID AS NUMERIC(9),
    @bitUseUserName AS BIT,
    @numLastUID AS NUMERIC(9),
    @tintMode TINYINT,
    @vcImapUserName AS VARCHAR(50)--For Support Email Address
AS 
DECLARE @numUserDetailId NUMERIC(9);SET @numUserDetailId=@numUserCntId

--IF @numUserCntId=-1 : For Support Email 
IF @numUserCntId>0
BEGIN
	SELECT  @numUserDetailId = numUserDetailId
	FROM    UserMaster
	WHERE   numUserId = @numUserCntId

	UPDATE UserMaster SET tintMailProvider=3 WHERE numUserId=@numUserCntId
END

--delete [ImapUserDetails] where numdomainId = @numDomainID and numUserCntId = @numUserDetailId
IF @tintMode = 0 
    BEGIN
        IF NOT EXISTS ( SELECT  *
                        FROM    [ImapUserDetails]
                        WHERE   numdomainId = @numDomainID
                                AND numUserCntId = @numUserDetailId ) 
            BEGIN
                INSERT  INTO [ImapUserDetails] ( bitImap,
                                                 [vcImapServerUrl],
                                                 [vcImapPassword],
                                                 [bitSSl],
                                                 [numPort],
                                                 [numDomainId],
                                                 [numUserCntId],
                                                 bitUseUserName,
                                                 tintRetryLeft,
                                                 numLastUID,
                                                 bitNotified,vcImapUserName)
                VALUES  (
                          @bitImap,
                          @vcImapServerUrl,
                          @vcImapPassword,
                          @bitImapSsl,
                          @numImapSSLPort,
                          @numDomainID,
                          @numUserDetailId,
                          @bitUseUserName,
                          10,
                          0,
                          1,@vcImapUserName
                        )
            END
        ELSE 
            BEGIN
                UPDATE  ImapUserDetails
                SET     bitImap = @bitImap,
                        [vcImapServerUrl] = @vcImapServerUrl,
                        [vcImapPassword] = @vcImapPassword,
                        [bitSSl] = @bitImapSsl,
                        [numPort] = @numImapSSLPort,
                        bitUseUserName = @bitUseUserName,
                        bitNotified=1,
                        tintRetryLeft=10,vcImapUserName=@vcImapUserName
                WHERE   numdomainId = @numDomainID
                        AND numUserCntId = @numUserDetailId 
            END
            
    END 
ELSE 
    IF @tintMode = 1 
        BEGIN
		
            UPDATE  ImapUserDetails
            SET     numLastUID = @numLastUID
            WHERE   numdomainId = @numDomainID
                    AND numUserCntId = @numUserCntId
        END
        
ELSE IF @tintMode = 2 
        BEGIN
		
            UPDATE  ImapUserDetails
            SET     bitNotified=1
            WHERE   numdomainId = @numDomainID
                    AND numUserCntId = @numUserCntId
        END