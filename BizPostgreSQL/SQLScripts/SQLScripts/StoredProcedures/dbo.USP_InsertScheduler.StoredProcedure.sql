/****** Object:  StoredProcedure [dbo].[USP_InsertScheduler]    Script Date: 07/26/2008 16:19:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created by Siva          
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_insertscheduler')
DROP PROCEDURE usp_insertscheduler
GO
CREATE PROCEDURE [dbo].[USP_InsertScheduler]         
@numScheduleId as numeric(9)=0,        
@varScheduleName as varchar(50),          
@chrIntervalType as char(1),          
@tintIntervalDays as tinyInt,          
@tintMonthlyType as tinyint,          
@tintFirstDet as tinyint,          
@tintWeekDays as tinyint,          
@tintMonths as tinyint,          
@dtStartDate as datetime,          
@dtEndDate as datetime,          
@bitEndType as bit,      
@bitEndTransactionType as bit,      
@numNoTransaction as numeric(9),    
@numDomainId as numeric(9) ,  
@numReportId as numeric      
As          
Begin          
if @dtEndDate ='Jan  1 1753 12:00AM' set  @dtEndDate=null         
        
--Insert        
if @numScheduleId=0         
Begin        
Insert into CustRptScheduler(varScheduleName,chrIntervalType,tintIntervalDays,tintMonthlyType,tintFirstDet,tintWeekDays,tintMonths,dtStartDate,dtEndDate,bitEndType,bitEndTransactionType,numNoTransaction,numDomainId,numReportId)          
Values(@varScheduleName,@chrIntervalType,@tintIntervalDays,@tintMonthlyType,@tintFirstDet,@tintWeekDays,@tintMonths,@dtStartDate,@dtEndDate,@bitEndType,@bitEndTransactionType,@numNoTransaction,@numDomainId,@numReportId)          
  SELECT  
   @numScheduleId = SCOPE_IDENTITY();  
End        
        
---Update        
if @numScheduleId<>0         
Begin        
Update CustRptScheduler Set varScheduleName=@varScheduleName,chrIntervalType=@chrIntervalType,tintIntervalDays=@tintIntervalDays,tintMonthlyType=@tintMonthlyType,        
tintFirstDet=@tintFirstDet,tintWeekDays=@tintWeekDays,tintMonths=@tintMonths,dtStartDate=@dtStartDate,dtEndDate=@dtEndDate,bitEndType=@bitEndType,bitEndTransactionType=@bitEndTransactionType,      
numNoTransaction=@numNoTransaction,numDomainId=@numDomainId,numReportId=@numReportId Where numScheduleId=@numScheduleId        
End        
    SELECT  
   @numScheduleId;          
End
GO
