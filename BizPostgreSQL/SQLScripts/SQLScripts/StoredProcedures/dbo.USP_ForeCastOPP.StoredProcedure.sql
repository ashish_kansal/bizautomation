/****** Object:  StoredProcedure [dbo].[USP_ForeCastOPP]    Script Date: 07/26/2008 16:16:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--exec usp_GetOppDetailsForTest @numUserCntID=1,@numDomainID=1,@tintType=1,@numDivisionID=0,@tintFirstMonth=4,@tintSecondMonth=5,@tintThirdMonth=6,@intYear=2008      
--created by anoop jayaraj      
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_forecastopp')
DROP PROCEDURE usp_forecastopp
GO
CREATE PROCEDURE [dbo].[USP_ForeCastOPP]                           
 @numUserCntID numeric(9)=0,                          
 @numDomainID numeric(9)=0,                        
 @tintType tinyint=1 ,                  
 @numDivisionID numeric(9)=0,          
 @tintFirstMonth tinyint=0,          
 @tintSecondMonth tinyint=0,          
 @tintThirdMonth tinyint=0,          
 @intYear as int=0 ,         
 @byteMode as tinyint                       
AS                      
                       
      
if @byteMode=0 or @byteMode=2      
begin      

SELECT X.Mon,
	   X.ForeCastAmount,
	   X.QuotaAmt,
	   X.sold,
	   X.Pipeline,
	   X.Probability,
	   ((X.Pipeline * X.Probability)/100) AS ProbableAmt 
FROM
( 
 Select M.Mon,isnull(monForecastAmount,0) as ForeCastAmount,isnull(monQuota,0) as QuotaAmt,      
 --SOLD
 isnull((SELECT sum(monPAmount) FROM  OpportunityMaster WHERE tintOppType=1 and  tintOppStatus=1  
 and 1=(Case WHEN @byteMode=0 then 1 
			 else Case when numRecOwner in (case when @numUserCntID> 0 then (case when @tintType=1 then (select @numUserCntID) else (select numContactId from additionalcontactsinformation where numManagerID =@numUserCntID) end) 
												 when @numDivisionID>0 then (select numContactId from additionalcontactsinformation where numDivisionID=@numDivisionID) else (select 0) end) then 1 else 0 end end)                   
 and numDomainID=@numDomainID And month(bintCreatedDate)=M.Mon And year(bintCreatedDate)=@intYear),0) AS sold,
 
 --in Pipeline
 isnull((SELECT sum(monPAmount) FROM OpportunityMaster WHERE tintOppType=1 And  tintOppStatus=0  
 and 1=(Case WHEN @byteMode=0 then 1  
			else case when numRecOwner in (case when @numUserCntID> 0 then (case when @tintType=1 then (select @numUserCntID) else (select numContactId from additionalcontactsinformation where numManagerID =@numUserCntID) end)     
											when @numDivisionID>0 then (select numContactId from additionalcontactsinformation where numDivisionID=@numDivisionID) else (select 0) end) then 1 else 0 end end)                           
 and numDomainID=@numDomainID And  month(bintCreatedDate)=M.Mon And year(bintCreatedDate)=@intYear),0) As Pipeline,
 
  -- Probability
 isnull((Select Avg(isnull(intTotalProgress,0)) FROM OpportunityMaster Opp inner join dbo.ProjectProgress PP on Opp.numOppId=PP.numOppId          
 Where Opp.tintOppType=1 And   Opp.tintOppStatus=0  
	and 1=(Case WHEN @byteMode=0 then 1
			    else case when numRecOwner in (case when @numUserCntID> 0 then (case when @tintType=1 then (select @numUserCntID) else (select numContactId from additionalcontactsinformation where numManagerID =@numUserCntID) end)     
													when @numDivisionID>0 then (select numContactId from additionalcontactsinformation where numDivisionID=@numDivisionID) else (select 0) end) then 1 else 0 end end)           
	and Opp.numDomainID=@numDomainID And  month(bintCreatedDate)=M.Mon And year(bintCreatedDate)=@intYear),0) As Probability,       
--ProbableAmt
-- isnull((Select Sum(X.Amount)   From (Select   ( monPAmount*(Select AVG(isnull(intTotalProgress,0))  from dbo.ProjectProgress PP
-- Where OpportunityMaster.numOppId=PP.numOppId ))/100 as Amount  From OpportunityMaster                         
-- Where tintOppType=1 And month(intPEstimatedCloseDate)=M.Mon and year(intPEstimatedCloseDate)=@intYear       
-- and  numRecOwner in (case when @numUserCntID> 0 then (case when @tintType=1 then (select @numUserCntID) else (select numContactId from additionalcontactsinformation where numManagerID =@numUserCntID) end) when @numDivisionID>0 then     
--	(select numContactId from additionalcontactsinformation where numDivisionID=@numDivisionID) else (select 0) end)  And tintoppStatus=0 and tintshipped=0 and numDomainID=@numDomainID)X ),0) 
0 As ProbableAmt     
	  

 From Forecast F      
 right join (select @tintFirstMonth as Mon union select @tintSecondMonth union select @tintThirdMonth) M      
 on F.tintMonth=M.Mon and sintYear=@intYear and numDomainID=@numDomainID and tintForecastType=1    and numcreatedby=   @numUserCntID  
 ) X
 
 
end      
else if @byteMode=1      
begin  
    
 Select M.Mon,isnull(monForecastAmount,0) as ForeCastAmount,isnull(monQuota,0) as QuotaAmt      
 From Forecast F      
 right join (select @tintFirstMonth as Mon union select @tintSecondMonth union select @tintThirdMonth) M      
 on F.tintMonth=M.Mon and sintYear=@intYear and numDomainID=@numDomainID and tintForecastType=1   
  
end
GO
