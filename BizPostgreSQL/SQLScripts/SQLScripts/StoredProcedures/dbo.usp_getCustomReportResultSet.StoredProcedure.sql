/****** Object:  StoredProcedure [dbo].[usp_getCustomReportResultSet]    Script Date: 07/26/2008 16:17:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getcustomreportresultset')
DROP PROCEDURE usp_getcustomreportresultset
GO
CREATE PROCEDURE [dbo].[usp_getCustomReportResultSet]                                                        
 @vcCustomReportConfig Ntext,                                                          
 @numUserCntID Numeric,                      
 @numRefreshTimeInterval Numeric,                      
 @numDomainId Numeric,
 @tintType Bigint          
AS                                                        
BEGIN                                                        
 DECLARE @ReportType Numeric                                                     
 --For Org and Contacts                                                       
 DECLARE @RelationShip Numeric                                                        
 DECLARE @CRMType Numeric                                                          
 DECLARE @IncludeContacts Bit                                                     
 DECLARE @ContactType Numeric                                                    
 DECLARE @CustomFields Bit                                    
 --For Opportunity and Items                                                    
 DECLARE @OppType Numeric                                          
                                               
                                                        
 DECLARE @vcReportLayout Char(3)                                                        
 DECLARE @bitStandardFilters Bit                                                        
 DECLARE @vcStandardFilterCriteria Nvarchar(50)                                                        
 DECLARE @vcStandardFilterStartDate Nvarchar(10)                                                        
 DECLARE @vcStandardFilterEndDate Nvarchar(10)                                                        
 DECLARE @vcTeams Nvarchar(250)                                                        
 DECLARE @vcTerritories Nvarchar(250)                                                        
 DECLARE @vcFilterOn Int                                                        
 DECLARE @vcAdvFilterJoining Char(1)                                                        
 DECLARE @bitRecordCountSummation Bit                                                        
                                                        
 DECLARE @vcReportColumnNames Nvarchar(4000)                                              
 DECLARE @vcAggregateReportColumns Nvarchar(1000)                                          
 DECLARE @vcReportAdvFilterWhereClause Nvarchar(1000)                                                        
 DECLARE @vcReportStdFilterWhereClause Nvarchar(1000)                                                         
 DECLARE @vcReportDataTypeWhereClause Nvarchar(500)                                                        
 DECLARE @vcReportWhereClause Nvarchar(2000)                   
                
 DECLARE @numDefaultOwner Numeric                        
	SELECT 
		@numDefaultOwner = RD.numEmpId 
	FROM 
		RoutingLeadDetails RD
	INNER JOIN
		RoutingLeads RL                        
	ON
		RD.numRoutID = RL.numRoutID
	WHERE                         
		RL.bitDefault = 1 AND RL.numDomainId = @numDomainId                           
                                                        
 DECLARE @iDoc int                                                        
 EXECUTE sp_xml_preparedocument @iDoc OUTPUT, @vcCustomReportConfig                                                        
                                                           
 SELECT @ReportType = ReportType FROM OpenXML(@iDoc, '/CustRptConfig/ReportTypeConfig', 2)                                                                          
 WITH                                
 (                                                                  
 ReportType Numeric                                                        
 )                              
 PRINT 'ReportType: ' + Convert(NVarchar,@ReportType)                              
 DECLARE @vcViewName Nvarchar(70)                                                      
 SELECT @vcViewName =   CASE @ReportType                                                      
    WHEN '1' THEN '##OrgContactCustomField'                 
    WHEN '2' THEN '##OppItemCustomField'                                                      
    WHEN '3' THEN 'dbo.vw_LeadsCustomReport'                                                      
   END                                                      
                                              
 IF @ReportType = 1 --Organization and Contacts                                                    
 BEGIN                                                    
 SELECT @RelationShip = RelationShip, @CRMType = CRMType, @IncludeContacts = IncludeContacts, @ContactType = ContactType, @CustomFields = CustomFields                               
   FROM OpenXML(@iDoc, '/CustRptConfig/ReportTypeConfig', 2)                                   
 WITH                                                                          
 (                            
  RelationShip Numeric,                                                    
  CRMType Numeric,                                                    
  IncludeContacts Bit,                                                    
  ContactType Numeric,                                    
  CustomFields Bit                                            
 )                                          
 END                                                    
 ELSE IF @ReportType = 2 --Opprotunity and Items                                                    
 BEGIN                                                  
 SELECT @OppType = OppType, @CustomFields = CustomFields FROM OpenXML(@iDoc, '/CustRptConfig/ReportTypeConfig', 2)                                                                          
 WITH                                                                          
 (                                                                  
  OppType Numeric,                                    
  CustomFields Bit                                                      
 )                                                    
 END                                                   
 PRINT 'RelationShip: ' + Cast(@RelationShip As NVArchar)                                                    
 PRINT 'CRMType: ' + Cast(@CRMType As NVArchar)                                                    
 PRINT 'IncludeContacts: ' + Cast(@IncludeContacts As NVArchar)                                                    
 PRINT 'ContactType: ' + Cast(@ContactType As NVArchar)                                                       
 PRINT 'CustomFields: ' + Cast(@CustomFields As NVArchar)                                                         
                                                    
 PRINT 'OppType: ' + Cast(@OppType As NVArchar)                                                    
                                                    
 SELECT @vcReportDataTypeWhereClause = CASE @ReportType                              
  WHEN 1 THEN                              
  ' AND tintCRMType = ' + Cast(@CRMType As NVArchar) +                              
 CASE WHEN @RelationShip > 0 THEN                              
    ' AND numOrgCompanyType = ' + Cast(@RelationShip As NVArchar)                              
 ELSE                              
    ''                              
 END                              
 +                              
   CASE WHEN @IncludeContacts = 1 THEN                              
  CASE WHEN @ContactType > 0 THEN                              
        ' AND numContContactType = ' + Cast(@ContactType As NVArchar)                           
  ELSE                              
     ''                              
  END                                                     
 ELSE      
    ''                                                    
 END                                                    
  WHEN 2 THEN                                                    
   ' AND tintOppType = ' + Cast(@OppType As NVArchar)                                                    
  ELSE                                                 
   ''                                                     
  END                          
                                                    
 PRINT 'vcReportDataTypeWhereClause: ' + @vcReportDataTypeWhereClause                                                     
                                                    
 SELECT @vcReportLayout = vcReportLayout, @bitStandardFilters = bitStandardFilters, @vcStandardFilterCriteria = vcStandardFilterCriteria,                                                        
 @vcStandardFilterStartDate = vcStandardFilterStartDate, @vcStandardFilterEndDate = vcStandardFilterEndDate,                                                        
 @vcTeams = vcTeams, @vcTerritories = vcTerritories, @vcFilterOn = vcFilterOn, @vcAdvFilterJoining = vcAdvFilterJoining,                                                
 @bitRecordCountSummation = bitRecordCountSummation                                                        
 FROM OpenXML(@iDoc, '/CustRptConfig/LayoutAndStdFilter', 2)                                        
 WITH                                                                
 (                                                                  
  vcReportLayout Char(3),                                                        
  bitStandardFilters Bit,                                                        
  vcStandardFilterCriteria Nvarchar(50),                                                        
  vcStandardFilterStartDate Nvarchar(10),                                                        
  vcStandardFilterEndDate Nvarchar(10),                                                        
  vcTeams Nvarchar(250),                                                        
  vcTerritories Nvarchar(250),                                                        
  vcFilterOn Int,                                      
  vcAdvFilterJoining Char(1),                                                        
  bitRecordCountSummation Bit                                                        
 )                                                        
 PRINT '@ReportType' + Convert(NVarchar,@ReportType)                    
 PRINT 'vcReportLayout: ' + Convert(NVarchar,@vcReportLayout)                                                        
 PRINT 'bitStandardFilters: ' + Convert(NVarchar,@bitStandardFilters)                                                        
 PRINT 'vcStandardFilterCriteria: ' + Convert(NVarchar,@vcStandardFilterCriteria)                                                        
 PRINT 'vcStandardFilterStartDate: ' + Convert(NVarchar,@vcStandardFilterStartDate)                                                        
 PRINT 'vcStandardFilterEndDate: ' + Convert(NVarchar,@vcStandardFilterEndDate)                                                        
 PRINT 'vcTeams: ' + Convert(NVarchar,@vcTeams)                                                        
 PRINT 'vcTerritories: ' + Convert(NVarchar,@vcTerritories)                                                        
 PRINT 'vcFilterOn: ' + Convert(NVarchar,@vcFilterOn)                                                 
 PRINT 'vcAdvFilterJoining: ' + Convert(NVarchar,@vcAdvFilterJoining)                                                        
 PRINT 'bitRecordCountSummation: ' + Convert(NVarchar,@bitRecordCountSummation)                                                       
                                                      
 SELECT @vcReportStdFilterWhereClause = CASE  @vcFilterOn                                                      
      WHEN '0' THEN              
 CASE WHEN @ReportType = 2 THEN    --Opportunities              
  ' numOppRecOwner = ' + Convert(NVarchar,@numUserCntID) + ' '              
 ELSE    --Organizations and Contacts              
  ' (numContRecOwner = ' + Convert(NVarchar,@numUserCntID) + ' OR numDivRecOwner = ' + Convert(NVarchar,@numUserCntID) + ') '               
 END              
      WHEN '1' THEN               
 CASE WHEN @ReportType = 2 THEN    --Opportunities              
 ' numOppRecOwner = ' + Convert(NVarchar,@numUserCntID) +          
                    ' AND numOppCreatedBy IN (SELECT numContactId FROM AdditionalContactsInformation WHERE numTeam IN (select numTeam From ForReportsByTeam Where numuserCntid ='+ Convert(NVarchar,@numUserCntID)+ 'numDomainid='+convert(varchar(15),
                      @numDomainId)+ 'tintType=32)) AND ((bitPublicFlag = 0) OR (bitPublicFlag=1 and numOppRecOwner = ' + Convert(NVarchar,@numUserCntID) + ')) '              
 ELSE    --Organizations and Contacts              
 ' (numContRecOwner = ' + Convert(NVarchar,@numUserCntID) + ' OR numDivRecOwner = ' + Convert(NVarchar,@numUserCntID) + ') ' +              
                    ' AND numContCreatedBy IN (SELECT numContactId FROM AdditionalContactsInformation WHERE numTeam IN (select numTeam From ForReportsByTeam Where numuserCntid ='+ Convert(NVarchar,@numUserCntID)+ 'numDomainid='+convert(varchar(15),
                      @numDomainId)+ 'tintType=32)) AND ((bitPublicFlag = 0) OR (bitPublicFlag=1 and numDivRecOwner = ' + Convert(NVarchar,@numUserCntID) + ')) '              
 END              
      ELSE              
 CASE WHEN @ReportType = 2 THEN  --Opportunities              
 ' numTerritoryId IN (select F.numTerritory from ForReportsByTerritory F where F.numuserCntid='+convert(varchar(15),@numUserCntID)+' and F.numDomainid='+convert(varchar(15),@numDomainID)+' and F.tintType=' +convert(varchar(15),@tintType)+ ')  AND ((bitPublicFlag = 0) OR (bitPublicFlag = 1 an
d numOppRecOwner = ' + Convert(NVarchar,@numUserCntID) + ')) '              
 ELSE   --Organizations and Contacts              
 ' numTerritoryId IN (select F.numTerritory from ForReportsByTerritory F where F.numuserCntid='+convert(varchar(15),@numUserCntID)+' and F.numDomainid='+convert(varchar(15),@numDomainID)+' and F.tintType=' +convert(varchar(15),@tintType)+ ')  AND ((bitPublicFlag = 0) OR (bitPublicFlag = 1 an
d numDivRecOwner = ' + Convert(NVarchar,@numUserCntID) + ')) '              
 END              
     END                                                      
     +                                                      
     CASE @bitStandardFilters                                       
      WHEN '1' THEN ' AND ' + CASE @vcStandardFilterCriteria                                                      
         WHEN 'All' THEN                        
           CASE    WHEN @vcStandardFilterStartDate <> '' AND @vcStandardFilterEndDate <> ''  THEN                        
             ' dtRecordCreatedOn >= ' + '''' + @vcStandardFilterStartDate + '''' + ' AND dtRecordCreatedOn <= ' + '''' + @vcStandardFilterEndDate + '''' + ' OR dtRecordLastModifiedOn >= ' + '''' + @vcStandardFilterStartDate + '''' + '          
               AND dtRecordLastModifiedOn <= ' + '''' + @vcStandardFilterEndDate + ''''                        
            WHEN @vcStandardFilterEndDate <> '' THEN                        
             ' dtRecordCreatedOn <= ' + '''' + @vcStandardFilterEndDate + '''' + ' AND dtRecordLastModifiedOn <= ' + '''' + @vcStandardFilterEndDate + ''''                        
            ELSE                        
             ' dtRecordCreatedOn >= ' + '''' + @vcStandardFilterStartDate + '''' + ' AND dtRecordLastModifiedOn >= ' + '''' + @vcStandardFilterStartDate + ''''                        
           END                         
          ELSE                         
           CASE    WHEN @vcStandardFilterStartDate <> '' AND @vcStandardFilterEndDate <> ''  THEN                        
             @vcStandardFilterCriteria + ' >= ' + '''' + @vcStandardFilterStartDate + '''' + ' AND ' + @vcStandardFilterCriteria + ' <= ' + '''' + @vcStandardFilterEndDate + ''''                        
            WHEN @vcStandardFilterEndDate <> '' THEN                        
             @vcStandardFilterCriteria + ' <= ' + '''' + @vcStandardFilterEndDate + ''''                        
            ELSE                        
             @vcStandardFilterCriteria + ' >= ' + '''' + @vcStandardFilterStartDate + ''''                        
           END                        
         END                                                      
      ELSE ''                                                      
     END                                                      
                                                      
 PRINT 'vcReportStdFilterWhereClause: ' + @vcReportStdFilterWhereClause                                                      
                                                 
 SELECT  TOP 100 @vcReportColumnNames = COALESCE(@vcReportColumnNames + ', ', '') + vcDbFieldName + ' AS [' + vcScrFieldName + ']'                                                    
 FROM OpenXML(@iDoc, '/CustRptConfig/ColAndAdvFilter/vcDbRow', 3)                                                        
 WITH                                                                          
 (                                                             
    vcDbFieldName Nvarchar(50),                                                 
    vcScrFieldName Nvarchar(50),                                                       
    numOrderOfDisplay Int                                                      
 )                                                        
 ORDER BY numOrderOfDisplay Asc                                                        
                    
                                           
 SELECT  TOP 100 @vcAggregateReportColumns = COALESCE(@vcAggregateReportColumns + '|', '') +                                             
   CAST(bitSumAggregation As Char(1)) + ',' +CAST(bitAvgAggregation As Char(1)) + ',' + CAST(bitLValueAggregation As Char(1)) + ',' + CAST(bitSValueAggregation As Char(1))                                        
 FROM OpenXML(@iDoc, '/CustRptConfig/ColAndAdvFilter/vcDbRow', 3)                                                        
 WITH                                                                          
 (                                            
    vcDbFieldName Nvarchar(50),                                                       
    vcScrFieldName Nvarchar(50),                                                       
    bitSumAggregation Bit,                                                      
    bitAvgAggregation Bit,                                                       
    bitLValueAggregation Bit,                                          
    bitSValueAggregation Bit,                                          
    numOrderOfDisplay Int                                          
 )                                                
 ORDER BY numOrderOfDisplay Asc                                             
                                                       
 PRINT '@vcAggregateReportColumns: ' + @vcAggregateReportColumns                                          
                                          
 SELECT  @vcReportAdvFilterWhereClause = COALESCE(@vcReportAdvFilterWhereClause + CASE WHEN @vcAdvFilterJoining = 'A' THEN ' AND ' ELSE ' OR ' END, '') +                                                        
    vcDbFieldName  +                                        
  CASE vcAdvFilterOperator                                        
   WHEN 'EQ' THEN ' = ' + '''' + vcAdvFilterValue + ''''                                                      
   WHEN 'LT' THEN ' < ' + '''' + vcAdvFilterValue + ''''                                                      
   WHEN 'GT' THEN ' > ' + '''' + vcAdvFilterValue + ''''                                                      
   WHEN 'LEQ' THEN ' <= ' + '''' + vcAdvFilterValue + ''''                                                 
   WHEN 'GEQ' THEN ' >= ' + '''' + vcAdvFilterValue + ''''                                                      
   WHEN 'NEQ' THEN ' <> ' + '''' + vcAdvFilterValue + ''''                                                      
   WHEN 'STW' THEN ' LIKE ' + '''%' + vcAdvFilterValue + ''''                                        
   WHEN 'LIKE' THEN ' LIKE ' + '''%' + vcAdvFilterValue + '%'''                                                      
   WHEN 'NOT LIKE' THEN ' NOT LIKE ' + '''%' + vcAdvFilterValue + '%'''                                                      
  END                                                        
 FROM OpenXML(@iDoc, '/CustRptConfig/ColAndAdvFilter/vcDbRow', 3)                                                        
 WITH                                                                          
 (                                                             
 vcDbFieldName Nvarchar(50),                                               
 vcAdvFilterOperator Nvarchar(10),                       
 vcAdvFilterValue  Nvarchar(50)                                                      
 )                                                        
 WHERE vcAdvFilterOperator <> '0'                                        
                                                  
 PRINT 'vcReportColumnNames: ' + @vcReportColumnNames                           
 SELECT @vcReportAdvFilterWhereClause = '(' + @vcReportAdvFilterWhereClause +  ')'                                 
 PRINT 'vcReportAdvFilterWhereClause: ' + @vcReportAdvFilterWhereClause                                                      
                                                      
 SELECT @vcReportWhereClause = ' WHERE ' + @vcReportStdFilterWhereClause + CASE WHEN @vcReportAdvFilterWhereClause <> '' THEN '          
                                AND ' + @vcReportAdvFilterWhereClause ELSE '' END + CASE WHEN @vcReportDataTypeWhereClause <> '' THEN @vcReportDataTypeWhereClause ELSE '' END + '        
                                AND numDomainId = ' + Cast(@numDomainId AS Varchar)        
                                        
 EXECUTE sp_xml_removedocument @iDoc                                       
                                    
 IF @ReportType  = 2                                    
 BEGIN                                                   
  PRINT('usp_OppItemCustomFields ' + Cast(@numRefreshTimeInterval AS Varchar))                     
  EXEC ('usp_OppItemCustomFields ' + @numRefreshTimeInterval)                                    
 END                                    
 ELSE IF  @ReportType  = 1                                    
 BEGIN                                    
  PRINT('usp_OrganizationContactsCustomFields ' + Cast(@numRefreshTimeInterval AS Varchar))                     
  EXEC ('usp_OrganizationContactsCustomFields ' + @numRefreshTimeInterval)                                       
 END                                    
 PRINT 'vcReportWhereClause: ' + @vcReportWhereClause                                                         
                                                       
 DECLARE @vcCustomReportQuery NVarchar(4000)                       
 DECLARE @vcCustomReportQueryMiddle NVarchar(4000)                                             
 DECLARE @vcCustomReportQueryTrailer NVarchar(4000)               
                          
 SELECT @vcCustomReportQuery = Left('SELECT ' + @vcReportColumnNames, 3900)                           
 SELECT @vcCustomReportQueryMiddle = Substring('SELECT ' + @vcReportColumnNames, 3901, 4000)                           
 SELECT @vcCustomReportQueryTrailer = ' FROM ' + @vcViewName + ' ' + @vcReportWhereClause                          
                                                      
 PRINT 'vcCustomReportQuery: ' + @vcCustomReportQuery + @vcCustomReportQueryMiddle + @vcCustomReportQueryTrailer                                                   
                                     
                                    
EXEC(@vcCustomReportQuery + @vcCustomReportQueryMiddle + @vcCustomReportQueryTrailer)                                          
                                          
    
 EXEC('SELECT ''' +  @vcAggregateReportColumns + ''' AS AggregateReportColumns')       
 SELECT @bitRecordCountSummation as 'RecordCountSummation'                                      
END
GO
