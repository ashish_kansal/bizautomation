/****** Object:  StoredProcedure [dbo].[USP_LoggedOutDetails]    Script Date: 07/26/2008 16:19:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created by anoop jayaraj                                          
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_loggedoutdetails')
DROP PROCEDURE usp_loggedoutdetails
GO
CREATE PROCEDURE [dbo].[USP_LoggedOutDetails]                                              
(                                                                    
@numAccessID as numeric(9)
)
as

   update UserAccessedDTL set dtLoggedOutTime=getutcdate() where  numAppAccessID=@numAccessID
GO
