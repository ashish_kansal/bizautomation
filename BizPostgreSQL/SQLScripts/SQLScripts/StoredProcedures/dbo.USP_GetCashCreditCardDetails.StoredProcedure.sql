/****** Object:  StoredProcedure [dbo].[USP_GetCashCreditCardDetails]    Script Date: 07/26/2008 16:16:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created By Siva    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getcashcreditcarddetails')
DROP PROCEDURE usp_getcashcreditcarddetails
GO
CREATE PROCEDURE [dbo].[USP_GetCashCreditCardDetails]  
@numCashCreditId as numeric(9)=0,    
@numDomainId as numeric(9)=0    
As    
Begin    
	Select * From CashCreditCardDetails Where numCashCreditId=@numCashCreditId And numDomainId=@numDomainId    
End
GO
