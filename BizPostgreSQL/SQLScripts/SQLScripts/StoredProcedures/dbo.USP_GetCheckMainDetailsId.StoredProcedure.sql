/****** Object:  StoredProcedure [dbo].[USP_GetCheckMainDetailsId]    Script Date: 07/26/2008 16:16:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created By Siva    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getcheckmaindetailsid')
DROP PROCEDURE usp_getcheckmaindetailsid
GO
CREATE PROCEDURE [dbo].[USP_GetCheckMainDetailsId]    
@numCheckId as numeric(9)=0,    
@numDomainId as numeric(9)=0    
As    
Begin     
	Declare @numJournalId as numeric(9)  
	Set @numJournalId=0  
	  
	Select @numJournalId=numJournal_Id From General_Journal_Header Where numCheckId=@numCheckId  
	  
	Select numTransactionId From General_Journal_Details   
	Where numJournalId=@numJournalId And bitMainCheck=1 And numDomainId=@numDomainId    
End
GO
