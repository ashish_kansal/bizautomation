GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getallsystemmodules')
DROP PROCEDURE usp_getallsystemmodules
GO
CREATE PROCEDURE [dbo].[usp_GetAllSystemModules]  
@tintGroupType as tinyint=0 
AS  
BEGIN
	IF EXISTS (SELECT ID FROM ModuleMasterGroupType WHERE tintGroupType=@tintGroupType)
	BEGIN
		SELECT 
			ModuleMaster.numModuleID
			,ModuleMaster.vcModuleName
			,ModuleMasterGroupType.tintGroupType
		FROM 
			ModuleMaster 
		INNER JOIN
			ModuleMasterGroupType
		ON
			ModuleMaster.numModuleID = ModuleMasterGroupType.numModuleID
		WHERE 
			ModuleMasterGroupType.tintGroupType=@tintGroupType 
		ORDER BY 
			vcModuleName ASC
	END
	ELSE
	BEGIN
		SELECT 
			ModuleMaster.numModuleID
			,ModuleMaster.vcModuleName
			,tintGroupType
		FROM 
			ModuleMaster 
		WHERE 
			tintGroupType=@tintGroupType 
		ORDER BY 
			vcModuleName ASC 
	END
	
END
GO
