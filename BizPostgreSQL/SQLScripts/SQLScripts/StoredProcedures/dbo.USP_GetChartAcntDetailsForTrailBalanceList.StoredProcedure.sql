/****** Object:  StoredProcedure [dbo].[USP_GetChartAcntDetailsForTrailBalanceList]    Script Date: 07/26/2008 16:16:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- Created by Siva                      
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getchartacntdetailsfortrailbalancelist')
DROP PROCEDURE usp_getchartacntdetailsfortrailbalancelist
GO
CREATE PROCEDURE [dbo].[USP_GetChartAcntDetailsForTrailBalanceList]                    
@numDomainId as numeric(9),          
@dtFromDate as datetime,        
@dtToDate as datetime                     
As                        
Begin                        
	Declare @strSQL as varchar(2000)        
	Declare @i as integer    
	Set @strSQL=''       
	Select @i=count(*) From General_Journal_Header Where datEntry_Date>='' + convert(varchar(300),@dtFromDate) +'' And   datEntry_Date<='' + convert(varchar(300),@dtToDate) +''  
	if @i=0  
		Begin  
		   Set @strSQL = ' Select distinct numAccountId as numAccountId,vcCatgyName as CategoryName,numOpeningBal as OpeningBalance            
			From Chart_Of_Accounts COA            
			Left outer join General_Journal_Details GJD on COA.numAccountId=GJD.numChartAcntId            
			Where  COA.numDomainId=' + Convert(varchar(5),@numDomainId) + 'And COA.numAccountId <>1 And COA.numOpeningBal is not null And CoA.numParntAcntId =1'        
		   Set @strSQL=@strSQL+ '  And COA.dtOpeningDate>=''' + convert(varchar(300),@dtFromDate) +''' And  COA.dtOpeningDate<=''' + convert(varchar(300),@dtToDate) +''''           
		End   
		Else  
			Begin  
			   Set @strSQL = ' Select distinct numAccountId as numAccountId,vcCatgyName as CategoryName,numOpeningBal as OpeningBalance            
				From Chart_Of_Accounts COA            
				Left outer join General_Journal_Details GJD on COA.numAccountId=GJD.numChartAcntId            
				Where  COA.numDomainId=' + Convert(varchar(5),@numDomainId) + 'And COA.numAccountId <>1 And COA.numOpeningBal is not null And CoA.numParntAcntId =1'        
			End  
		print @strSQL        
		exec (@strSQL)        
End
GO
