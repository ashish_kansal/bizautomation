/****** Object:  StoredProcedure [dbo].[USP_EmailHistoryDelete]    Script Date: 07/26/2008 16:15:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_emailhistorydelete')
DROP PROCEDURE usp_emailhistorydelete
GO
CREATE PROCEDURE  [dbo].[USP_EmailHistoryDelete]
@numEmailHstrID as numeric(9)=0

as

delete from EmailHistory where numEmailHstrID=@numEmailHstrID
GO
