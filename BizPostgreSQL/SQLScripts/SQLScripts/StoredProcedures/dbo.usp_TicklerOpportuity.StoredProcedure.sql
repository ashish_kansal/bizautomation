/****** Object:  StoredProcedure [dbo].[usp_TicklerOpportuity]    Script Date: 07/26/2008 16:21:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj                              
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_tickleropportuity')
DROP PROCEDURE usp_tickleropportuity
GO
CREATE PROCEDURE [dbo].[usp_TicklerOpportuity]                                      
@numUserCntID as numeric(9)=null,                                      
@numDomainID as numeric(9)=null,                                      
@startDate as datetime,                                      
@endDate as datetime                                     
as                                       
  
DECLARE @vcSelectedEmployess VARCHAR(500)
DECLARE @vcAssignedStages VARCHAR(500)
DECLARE @vcActionTypes VARCHAR(100)
DECLARE @strSql VARCHAR(8000) = ''

SELECT 
	@vcSelectedEmployess = vcSelectedEmployeeOpp,
	@vcAssignedStages = vcAssignedStages
FROM 
	TicklerListFilterConfiguration
WHERE
	numDomainID = @numDomainID AND
	numUserCntID = @numUserCntID


SELECT   
		 Split.a.value('.', 'VARCHAR(100)') AS Data  
	INTO 
		#TEMP
	FROM  
	(
		SELECT CAST ('<M>' + REPLACE(ISNULL(@vcAssignedStages,''),',','</M><M>') + '</M>' AS XML) AS Data  
	) AS A CROSS APPLY Data.nodes ('/M') AS Split(a); 

IF ISNULL(LEN(@vcAssignedStages),0) = 0 OR (SELECT COUNT(*) FROM #TEMP WHERE Data = 1) > 0 --Sales Opportunity
BEGIN
	SET @strSql = 'select 
						SPD.numStageDetailsId,
						SPD.vcStageName,
						SPD.tinProgressPercentage as Tprogress,
						SPD.dtEndDate as CloseDate,
						opp.numOppId as ID,vcPOppName,
						''Sales Opportunity'' AS Type,
						Div.numTerId                                      
					FROM 
						OpportunityMaster opp 
					JOIN 
						StagePercentageDetails SPD 
					ON 
						opp.numOppID=SPD.numOppID 
					JOIN 
						DivisionMaster div 
					ON 
						div.numDivisionID=opp.numDivisionID  
					WHERE 
						opp.tintOppType= 1 AND 
						opp.tintOppStatus = 0 AND
						opp.numDomainID=' + CAST(@numDomainID AS VARCHAR(10))
						 + ' AND (SPD.numAssignTo IN (' + CASE WHEN LEN(@vcSelectedEmployess) > 0 THEN @vcSelectedEmployess ELSE Cast(@numUserCntID as varchar(10)) END +')  or SPD.numCreatedBy IN (' + CASE WHEN LEN(@vcSelectedEmployess) > 0 THEN @vcSelectedEmployess ELSE Cast(@numUserCntID as varchar(10)) END +' ))'
						 + ' AND (SPD.dtEndDate  >= '''+ Cast(@startDate as varchar(30)) +''' AND SPD.dtEndDate  <= ''' + CAST(@endDate AS VARCHAR(30)) + ''')'
END

IF ISNULL(LEN(@vcAssignedStages),0) = 0 OR (SELECT COUNT(*) FROM #TEMP WHERE Data = 2) > 0 --Purchase Opportunity
BEGIN
	SET @strSql =  @strSql + CASE WHEN LEN(@strSql) > 0 THEN ' UNION ' ELSE '' END + 
					'select 
						SPD.numStageDetailsId,
						SPD.vcStageName,
						SPD.tinProgressPercentage as Tprogress,
						SPD.dtEndDate as CloseDate,
						opp.numOppId as ID,vcPOppName,
						''Purchase Opportunity'' AS Type,
						Div.numTerId                                      
					FROM 
						OpportunityMaster opp 
					JOIN 
						StagePercentageDetails SPD 
					ON 
						opp.numOppID=SPD.numOppID 
					JOIN 
						DivisionMaster div 
					ON 
						div.numDivisionID=opp.numDivisionID  
					WHERE 
						opp.tintOppType= 2 AND 
						opp.tintOppStatus = 0 AND
						opp.numDomainID=' + CAST(@numDomainID AS VARCHAR(10))
						 + ' AND (SPD.numAssignTo IN (' + CASE WHEN LEN(@vcSelectedEmployess) > 0 THEN @vcSelectedEmployess ELSE Cast(@numUserCntID as varchar(10)) END +')  or SPD.numCreatedBy IN (' + CASE WHEN LEN(@vcSelectedEmployess) > 0 THEN @vcSelectedEmployess ELSE Cast(@numUserCntID as varchar(10)) END +' ))'
						 + ' AND (SPD.dtEndDate  >= '''+ Cast(@startDate as varchar(30)) +''' AND SPD.dtEndDate  <= ''' + CAST(@endDate AS VARCHAR(30)) + ''')'
END
	
IF ISNULL(LEN(@vcAssignedStages),0) = 0 OR (SELECT COUNT(*) FROM #TEMP WHERE Data = 3) > 0 --Sales Order
BEGIN
	SET @strSql = @strSql + CASE WHEN LEN(@strSql) > 0 THEN ' UNION ' ELSE '' END +
					'select 
						SPD.numStageDetailsId,
						SPD.vcStageName,
						SPD.tinProgressPercentage as Tprogress,
						SPD.dtEndDate as CloseDate,
						opp.numOppId as ID,vcPOppName,
						''Sales Order'' AS Type,
						Div.numTerId                                      
					FROM 
						OpportunityMaster opp 
					JOIN 
						StagePercentageDetails SPD 
					ON 
						opp.numOppID=SPD.numOppID 
					JOIN 
						DivisionMaster div 
					ON 
						div.numDivisionID=opp.numDivisionID  
					WHERE 
						opp.tintOppType= 1 AND 
						opp.tintOppStatus = 1 AND
						opp.numDomainID=' + CAST(@numDomainID AS VARCHAR(10))
						 + ' AND (SPD.numAssignTo IN (' + CASE WHEN LEN(@vcSelectedEmployess) > 0 THEN @vcSelectedEmployess ELSE Cast(@numUserCntID as varchar(10)) END +')  or SPD.numCreatedBy IN (' + CASE WHEN LEN(@vcSelectedEmployess) > 0 THEN @vcSelectedEmployess ELSE Cast(@numUserCntID as varchar(10)) END +' ))'
						 + ' AND (SPD.dtEndDate  >= '''+ Cast(@startDate as varchar(30)) +''' AND SPD.dtEndDate  <= ''' + CAST(@endDate AS VARCHAR(30)) + ''')'
END

IF ISNULL(LEN(@vcAssignedStages),0) = 0 OR (SELECT COUNT(*) FROM #TEMP WHERE Data = 4) > 0 --Purchase Order
BEGIN
	SET @strSql = @strSql + CASE WHEN LEN(@strSql) > 0 THEN ' UNION ' ELSE '' END +
					'select 
						SPD.numStageDetailsId,
						SPD.vcStageName,
						SPD.tinProgressPercentage as Tprogress,
						SPD.dtEndDate as CloseDate,
						opp.numOppId as ID,vcPOppName,
						''Purchase Order'' AS Type,
						Div.numTerId                                      
					FROM 
						OpportunityMaster opp 
					JOIN 
						StagePercentageDetails SPD 
					ON 
						opp.numOppID=SPD.numOppID 
					JOIN 
						DivisionMaster div 
					ON 
						div.numDivisionID=opp.numDivisionID  
					WHERE 
						opp.tintOppType= 2 AND 
						opp.tintOppStatus = 1 AND
						opp.numDomainID=' + CAST(@numDomainID AS VARCHAR(10))
						 + ' AND (SPD.numAssignTo IN (' + CASE WHEN LEN(@vcSelectedEmployess) > 0 THEN @vcSelectedEmployess ELSE Cast(@numUserCntID as varchar(10)) END +')  or SPD.numCreatedBy IN (' + CASE WHEN LEN(@vcSelectedEmployess) > 0 THEN @vcSelectedEmployess ELSE Cast(@numUserCntID as varchar(10)) END +' ))'
						 + ' AND (SPD.dtEndDate  >= '''+ Cast(@startDate as varchar(30)) +''' AND SPD.dtEndDate  <= ''' + CAST(@endDate AS VARCHAR(30)) + ''')'
END

IF ISNULL(LEN(@vcAssignedStages),0) = 0 OR (SELECT COUNT(*) FROM #TEMP WHERE Data = 5) > 0 --Project
BEGIN
	SET @strSql = @strSql + CASE WHEN LEN(@strSql) > 0 THEN ' UNION ' ELSE '' END +                    
							'SELECT 
								SPD.numStageDetailsId,
								SPD.vcStageName,
								SPD.tinProgressPercentage as Tprogress,
								SPD.dtEndDate as CloseDate,
								Pro.numProId as ID,
								vcProjectName as vcPOppName,
								''Project'' as Type,Div.numTerId  
							FROM 
								ProjectsMaster Pro 
							JOIN 
								StagePercentageDetails SPD 
							ON 
								Pro.numProId=SPD.numProjectID
							JOIN 
								DivisionMaster div 
							ON 
								div.numDivisionID=Pro.numDivisionID   
							WHERE 
								Pro.numDomainID=' + CAST(@numDomainID AS VARCHAR(10))
							+ ' AND (SPD.numAssignTo IN (' + CASE WHEN LEN(@vcSelectedEmployess) > 0 THEN @vcSelectedEmployess ELSE Cast(@numUserCntID as varchar(10)) END +')  or SPD.numCreatedBy IN (' + CASE WHEN LEN(@vcSelectedEmployess) > 0 THEN @vcSelectedEmployess ELSE Cast(@numUserCntID as varchar(10)) END +' ))'
							+ ' AND (SPD.dtEndDate  >= '''+ Cast(@startDate as varchar(30)) +''' and SPD.dtEndDate  <= ''' + CAST(@endDate AS VARCHAR(30)) + ''') ORDER BY dtEndDate Desc'                   
END


Print @strSql
EXEC(@strSql)   

DROP TABLE #TEMP                           
GO
