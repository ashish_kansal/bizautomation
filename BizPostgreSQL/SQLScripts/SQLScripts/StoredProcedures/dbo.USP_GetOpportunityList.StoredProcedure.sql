/****** Object:  StoredProcedure [dbo].[USP_GetOpportunityList]    Script Date: 07/26/2008 16:18:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- created by anoop jayaraj                                               
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getopportunitylist')
DROP PROCEDURE usp_getopportunitylist
GO
CREATE PROCEDURE [dbo].[USP_GetOpportunityList]                                                    
 @numUserCntID numeric(9)=0,                                                    
 @numDomainID numeric(9)=0,                                                    
 @tintUserRightType tinyint=0,                                                    
 @tintSortOrder tinyint=4,                                                    
 @dtLastDate datetime,                                                    
 @SortChar char(1)='0',                                                    
 @FirstName varChar(100)= '',                                                    
 @LastName varchar(100)='',                                                    
 @CustName varchar(100)='' ,                                                    
 @OppType as tinyint,                                                    
 @CurrentPage int,                                                    
 @PageSize int,                                                    
 @TotRecs int output,                                                    
 @columnName as Varchar(50),                                                    
 @columnSortOrder as Varchar(10),                                                  
 @numDivisionID as numeric(9)=0,                      
 @bitPartner as bit=0 ,        
 @ClientTimeZoneOffset as int                                                    
as                                                    
                                                    
--Create a Temporary table to hold data                                                    
Create table #tempTable ( ID INT IDENTITY PRIMARY KEY,                                                     
      numOppId numeric(9)                                                    
 )                                                    
declare @strSql as varchar(8000)                          
set @strSql=''                                                                     
set @strSql=@strSql+'SELECT   Opp.numOppId                                     
  FROM OpportunityMaster Opp                                                     
  INNER JOIN AdditionalContactsInformation ADC                                                     
  ON Opp.numContactId = ADC.numContactId                                                     
  INNER JOIN DivisionMaster Div                                                     
  ON Opp.numDivisionId = Div.numDivisionID AND ADC.numDivisionId = Div.numDivisionID                                                     
  INNER JOIN CompanyInfo C                                                     
  ON Div.numCompanyID = C.numCompanyId                                                          
  left join AdditionalContactsInformation ADC1 on ADC1.numContactId=Opp.numRecOwner '                        
                     
                        
  if @bitPartner=1 set @strSql=@strSql+' left join OpportunityContact OppCont on OppCont.numOppId=Opp.numOppId'                                                               
  set @strSql=@strSql+' WHERE Opp.tintOppstatus=0                                                   
  and Div.numDomainID=' + convert(varchar(15),@numDomainID)+'                                                    
  and Opp.tintOppType= ' + convert(varchar(1),@OppType)+'                                                    
  and ADC.vcFirstName  like '''+@FirstName+'%''                                                     
  and ADC.vcLastName like '''+@LastName+'%''                                                    
  and C.vcCompanyName like '''+@CustName+'%'''                                                    
  if @numDivisionID<>0 set @strSql=@strSql + ' And Div.numDivisionID =' + convert(varchar(15),@numDivisionID)                                                   
if @SortChar<>'0' set @strSql=@strSql + ' And Opp.vcPOppName like '''+@SortChar+'%'''                                                     
if @tintUserRightType=1 set @strSql=@strSql + ' AND (Opp.numRecOwner = '+convert(varchar(15),@numUserCntID) +' or Opp.numAssignedTo= '+ convert(varchar(15),@numUserCntID)  +' or  Opp.numOppId in (select distinct(numOppId)             
from OpportunityStageDetails where numAssignTo ='+ convert(varchar(15),@numUserCntID)+')'+ case when @bitPartner=1 then ' or ( OppCont.bitPartner=1 and OppCont.numContactId='+convert(varchar(15),@numUserCntID)+ ')' else '' end +')'                        
  
    
      
        
          
                                         
else if @tintUserRightType=2 set @strSql=@strSql + ' AND (Div.numTerID in (select numTerritoryID from  UserTerritory where numUserCntID= '+convert(varchar(15),@numUserCntID)+' ) or div.numTerID=0 or             
Opp.numAssignedTo= '+ convert(varchar(15),@numUserCntID)  +' or  Opp.numOppId in (select distinct(numOppId)             
from OpportunityStageDetails where numAssignTo ='+ convert(varchar(15),@numUserCntID)+'))'                                                        
    --                                                
if @tintSortOrder=1  set @strSql=@strSql + ' AND (Opp.numRecOwner = '+convert(varchar(15),@numUserCntID) +' or Opp.numAssignedTo= '+ convert(varchar(15),@numUserCntID)  +' or  Opp.numOppId in (select distinct(numOppId)             
from OpportunityStageDetails where numAssignTo ='+ convert(varchar(15),@numUserCntID)+')'+ case when @bitPartner=1 then ' or ( OppCont.bitPartner=1 and OppCont.numContactId='+convert(varchar(15),@numUserCntID)+ ')' else '' end +')'                 
else if @tintSortOrder=2  set @strSql=@strSql + 'AND (Opp.numRecOwner in (select A.numContactID from AdditionalContactsInformation A                                                                             
where A.numManagerID='+ convert(varchar(15),@numUserCntID) +') or Opp.numAssignedTo in (select A.numContactID from AdditionalContactsInformation A                                                                             
where A.numManagerID='+ convert(varchar(15),@numUserCntID)  +') or  Opp.numOppId in (select distinct(numOppId)             
from OpportunityStageDetails where numAssignTo in (select A.numContactID from AdditionalContactsInformation A                                                                             
where A.numManagerID='+ convert(varchar(15),@numUserCntID)+'))'+ case when @bitPartner=1 then ' or ( OppCont.bitPartner=1 and OppCont.numContactId in (select A.numContactID from AdditionalContactsInformation A                                              
  
    
      
        
          
                               
where A.numManagerID='+ convert(varchar(15),@numUserCntID)+ '))' else '' end +')'                                                     
else if @tintSortOrder=4  set @strSql=@strSql + ' AND Opp.bintCreatedDate> '''+convert(varchar(20),dateadd(day,-8,@dtLastDate))+''''                                                    
else if @tintSortOrder=6  set @strSql=@strSql + ' ORDER BY Opp.bintModifiedDate desc '                                                    
                                                    
if @tintSortOrder=1  set @strSql=@strSql + ' ORDER BY ' + @columnName +' '+ @columnSortOrder                                               
else if @tintSortOrder=2  set @strSql=@strSql + ' ORDER BY ' + @columnName +' '+ @columnSortOrder                                                            
else if @tintSortOrder=3  set @strSql=@strSql + ' ORDER BY ' + @columnName +' '+ @columnSortOrder                                                        
else if @tintSortOrder=4  set @strSql=@strSql + ' ORDER BY ' + @columnName +' '+ @columnSortOrder                                                    
else if @tintSortOrder=5  set @strSql=@strSql + ' ORDER BY ' + @columnName +' '+ @columnSortOrder                                                    
else if @tintSortOrder=6  set @strSql=@strSql +' , ' + @columnName +' '+ @columnSortOrder                                                   
if @numDivisionID<>0 set @strSql=@strSql +' ORDER BY ' + @columnName +' '+ @columnSortOrder                                   
                     
print   @strSql                                                  
insert into #tempTable(numOppId)                                        
exec (@strSql)                    
                                                     
                                                    
  declare @firstRec as integer                                              
  declare @lastRec as integer                                     
 set @firstRec= (@CurrentPage-1) * @PageSize             
     set @lastRec= (@CurrentPage*@PageSize+1)                                                    
                                                  
set @TotRecs=(select count(*) from #tempTable)                                         
                                        
SELECT  Opp.numOppId,                                                     
  Opp.vcPOppName AS Name,                                                     
  dbo.GetOppStatus(Opp.numOppId) as STAGE,                                                     
  Opp.intPEstimatedCloseDate AS CloseDate,                                                     
  Opp.numRecOwner,                                                    
  ADC.vcFirstName + ' ' + ADC.vcLastName AS Contact,                                                     
  C.vcCompanyName AS Company,                                                    
                C.numCompanyID,                                                    
  Div.tintCRMType,                                                    
  ADC.numContactID,                                                    
  Div.numDivisionID,                                                    
  Div.numTerID,                                                    
  Opp.monPAmount,                                                    
  ADC1.vcFirstName+' '+ADC1.vcLastName as [UserName],                        
  dbo.fn_GetContactName(Opp.numAssignedTo)+'/ '+dbo.fn_GetContactName(Opp.numAssignedBy) as AssignedToBy                                                  
  FROM OpportunityMaster Opp                                                     
  INNER JOIN AdditionalContactsInformation ADC                                                     
  ON Opp.numContactId = ADC.numContactId                                                     
  INNER JOIN DivisionMaster Div                                      
  ON Opp.numDivisionId = Div.numDivisionID AND ADC.numDivisionId = Div.numDivisionID                                                     
  INNER JOIN CompanyInfo C                           
  ON Div.numCompanyID = C.numCompanyId                                                       
  left join AdditionalContactsInformation ADC1 on ADC1.numContactId=Opp.numRecOwner                                         
 join #tempTable T on T.numOppId=Opp.numOppId                                        
  WHERE ID > @firstRec and ID < @lastRec order by ID                                        
                                        
                                                   
drop table #tempTable
GO
