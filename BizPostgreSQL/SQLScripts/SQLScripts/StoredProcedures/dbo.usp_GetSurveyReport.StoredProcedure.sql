/****** Object:  StoredProcedure [dbo].[usp_GetSurveyReport]    Script Date: 07/26/2008 16:18:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getsurveyreport')
DROP PROCEDURE usp_getsurveyreport
GO
CREATE PROCEDURE [dbo].[usp_GetSurveyReport]
	@numSurveyID numeric   
--
AS
--This SP will get the report for a particular Survey.
BEGIN
	--Declare variables for the SurveyQuestions.
	DECLARE @vcSurName varchar(50), @numQID numeric, @vcQuestion varchar(100)
	DECLARE @tintAnsType tinyint, @numAnsID numeric, @vcAnsLabel varchar(100)
	DECLARE @intCount int
	DECLARE @numRespondantID numeric, @vcAnsText varchar(500)

	--Create A Temp Table for the Resultset.
	CREATE TABLE #tmpSrQ (
		numSurveyID numeric,
		vcSurveyName varchar(50), 
		numQID numeric,
		vcQuestion varchar(100), 
		numAnsID numeric,
		vcAnsLabel varchar(100),
		tintAnsType tinyint, 
		intCount int,
		numRespondantID numeric,
		vcAnsText varchar(500))
	
	--Create Cursor for the Questions.
	DECLARE SurveyMaster_cursor CURSOR FOR 
		SELECT vcSurName, numQID, vcQuestion, tintAnstype, 
			numAnsID, vcAnsLabel
			FROM vw_SurveyQuestions
			WHERE numSurID = @numSurveyID
			ORDER BY numQID

	--Open The Cursor.
	OPEN SurveyMaster_cursor

	--Loop thru the Cursor and get the data from answers.
	FETCH NEXT FROM SurveyMaster_cursor 
		INTO @vcSurName, @numQID, @vcQuestion, @tintAnsType, @numAnsID, @vcAnsLabel

	WHILE @@FETCH_STATUS = 0    --No Error
	BEGIN
		--Set the Default value of the Count to 0.
		SET @intCount=0
		IF @tintAnsType=0 OR @tintAnsType=1 -- CheckBox OR Radio Buttons.
		BEGIN
			--Get the Count For the Answer.
			SELECT @intCount=COUNT(numAnsID)
				FROM vw_SurveyResponses 
				WHERE numAnsID=@numAnsID

			--Insert the data into the Temp Table.
			INSERT INTO #tmpSrQ values(
				@numSurveyID, @vcSurName, @numQID, @vcQuestion, @numAnsID,
				@vcAnsLabel, @tintAnsType, @intCount, 0, '')
			
		END
		ELSE -- Text Box
		BEGIN 
			--Create a new Cursor for the Text Box Values.
			DECLARE TextAns_cursor CURSOR
			   FOR SELECT numRespondantID, vcAnsText 
				FROM vw_SurveyResponses 
				WHERE numAnsID=@numAnsID

			--Open the Cursor.
			OPEN TextAns_cursor
	
			--Get the First Record.
			FETCH NEXT FROM TextAns_cursor
				INTO @numRespondantID, @vcAnsText

			--Loop thru all the records and insert into Temp Table.
			WHILE @@FETCH_STATUS = 0    --No Error
			BEGIN
				--Insert the data into the Temp Table.
				INSERT INTO #tmpSrQ values(
					@numSurveyID, @vcSurName, @numQID, @vcQuestion, @numAnsID,
					@vcAnsLabel, @tintAnsType, 0, @numRespondantID, @vcAnsText)
				--Get the Next Record.	
				FETCH NEXT FROM TextAns_cursor
					INTO @numRespondantID, @vcAnsText
			END
			
			--Close the Cursor and dispose the cursor.
			CLOSE TextAns_cursor
			DEALLOCATE TextAns_cursor
		END
			
		--Get the Next Record
		FETCH NEXT FROM SurveyMaster_cursor 
			INTO @vcSurName, @numQID, @vcQuestion, @tintAnsType, @numAnsID, @vcAnsLabel
	
	END
	--Close the Cursor and dispose the cursor.
	CLOSE SurveyMaster_cursor
	DEALLOCATE SurveyMaster_cursor

	--Return the Resulting Recordset frm teh Temp Table.
	SELECT * FROM #tmpSrQ
END
GO
