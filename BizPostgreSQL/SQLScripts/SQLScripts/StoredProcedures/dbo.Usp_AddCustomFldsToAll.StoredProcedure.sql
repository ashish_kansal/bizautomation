/****** Object:  StoredProcedure [dbo].[Usp_AddCustomFldsToAll]    Script Date: 07/26/2008 16:14:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_addcustomfldstoall')
DROP PROCEDURE usp_addcustomfldstoall
GO
CREATE PROCEDURE  [dbo].[Usp_AddCustomFldsToAll]    
@FieldId as numeric(9)=0,      
@RelaionId as numeric(9)=0  
as        
declare @order as numeric  
if not exists(select * from CFW_Fld_Dtl where numFieldId=@FieldId and numRelation=@RelaionId)  
begin  
select @order=max(numOrder) from CFW_Fld_Dtl where numFieldId=@FieldId and numRelation=@RelaionId  
if @order is null set  @order=0  
set @order=@order+1  
insert into CFW_Fld_Dtl(numFieldId,numRelation,numOrder)values(@FieldId,@RelaionId,@order)  
end
GO
