/****** Object:  StoredProcedure [dbo].[USP_LoadWarehouseAttributesEcomm]    Script Date: 07/26/2008 16:19:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--exec USP_LoadWarehouseAttributes 14,''        
-- exec USP_LoadWarehouseAttributesEcomm @numItemCode='173033',@strAtrr='7270,9708,',@numWareHouseID='63'
--created by anoop jayaraj        
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_loadwarehouseattributesecomm')
DROP PROCEDURE usp_loadwarehouseattributesecomm
GO
CREATE PROCEDURE [dbo].[USP_LoadWarehouseAttributesEcomm]
@numItemCode as varchar(20)='',        
@strAtrr as varchar(1000)='',    
@numWareHouseID as varchar(20)        
AS  
BEGIN
 
	DECLARE @numDomainID AS NUMERIC(18,0)   
       
	DECLARE @strSQL AS VARCHAR(MAX)    
	DECLARE @strAtrrTemp AS VARCHAR(MAX) 
	DECLARE @CharType AS CHAR(1)
	DECLARE @bitMatrix AS BIT
	DECLARE @numItemGroup NUMERIC(18,0)
	DECLARE @vcItemName VARCHAR(500)

    
	SELECT 
		@numDomainID=numDomainID
		,@CharType=charItemType
		,@bitMatrix = ISNULL(bitMatrix,0)
		,@numItemGroup = ISNULL(numItemGroup,0)
		,@vcItemName = vcItemName
	FROM 
		Item 
	WHERE 
		numItemCode=@numItemCode  
  
	SET @strAtrrTemp= @strAtrr       

	IF @strAtrr!=''        
	BEGIN        
		DECLARE @Cnt INT
		DECLARE @vcAttribute AS VARCHAR(100)
	 
		SET @Cnt = 1        
		DECLARE @SplitOn CHAR(1)=','        
	
		IF @bitMatrix = 1
		BEGIN
			SET @strSQL = 'SELECT Item.numItemCode FROM ItemAttributes INNER JOIN Item ON ItemAttributes.numItemCode = Item.numItemCode  WHERE'
		END
		ELSE
		BEGIN  
			SET @strSQL = 'SELECT recid FROM CFW_Fld_Values_Serialized_Items WHERE'  
		END   
	 
		WHILE (Charindex(@SplitOn,@strAtrr)>0)        
		BEGIN
			IF @bitMatrix = 1
			BEGIN
				SET @strSQL=@strSQL + (CASE WHEN @Cnt=1 THEN '' ELSE ' OR ' END) + ' fld_value=CAST(''' + (ltrim(rtrim(Substring(@strAtrr,1,Charindex(@SplitOn,@strAtrr)-1))))+''' AS NUMERIC)'
			END
			ELSE
			BEGIN
				SET @strSQL=@strSQL + (CASE WHEN @Cnt=1 THEN '' ELSE ' OR ' END) + ' fld_value=''' + (ltrim(rtrim(Substring(@strAtrr,1,Charindex(@SplitOn,@strAtrr)-1))))+''''         
			END
			
			SET @strAtrr = SUBSTRING(@strAtrr,CHARINDEX(@SplitOn,@strAtrr)+1,LEN(@strAtrr))
			       
			SET @Cnt = @Cnt + 1        
		End

		IF @bitMatrix = 1
		BEGIN
			SET @strSQL= CONCAT(@strSQL,(CASE WHEN @Cnt=1 THEN '' ELSE ' OR ' END),' fld_value=CAST(''',@strAtrr,''' AS NUMERIC) AND ISNULL(bitMatrix,0) = 1 AND ISNULL(Item.numItemGroup,0) = ',@numItemGroup,' AND Item.vcItemName=''',@vcItemName,'''', ' GROUP BY Item.numItemCode HAVING count(*) > ',@Cnt-1)
		END
		ELSE
		BEGIN
			SET @strSQL=@strSQL + (CASE WHEN @Cnt=1 THEN '' ELSE ' OR ' END) +' fld_value=''' + @strAtrr + ''' GROUP BY recid HAVING count(*) > '+convert(varchar(10), @Cnt-1)  
		END  		    		     
	END 


	IF @strAtrrTemp <> ''
	BEGIN      
		IF @bitMatrix = 1
		BEGIN	
			set @strSQL = 'DECLARE @bitShowInStock AS BIT = 0
						   DECLARE @bitShowQuantity AS BIT = 0   
						   DECLARE @bitAllowBackOrder AS BIT
							
							SELECT 
								@bitShowInStock=isnull(bitShowInStock,0)
								,@bitShowQuantity=isnull(bitShowQOnHand,0) 
							FROM 
								eCommerceDTL 
							WHERE 
								numDomainID='+ CAST(@numDomainID AS VARCHAR) +'
						   
							SELECT @bitAllowBackOrder=bitAllowBackOrder FROM Item Where numItemCode IN (' + @strSQL + ')
						   
							SELECT TOP 1
								numItemID
								,WareHouseItems.numWareHouseItemId
								,ISNULL(sum(numOnHand),0) as numOnHand
								,('+CASE 
									WHEN @CharType<>'S' 
									THEN '(Case 
											when @bitShowInStock=1 then (Case 
																			when @bitAllowBackOrder=1 then ''In Stock'' 
																			else (Case 
																					when isnull(sum(numOnHand),0) > 0 then ''In Stock'' + (Case when @bitShowQuantity=1 then ''(''+convert(varchar(20),isnull(sum(numOnHand),0))+'')'' else '''' end)     
																					else ''<font color=red>Out Of Stock</font>'' 
																					end 
																				) 
																			end
																		)    
											else ''''
											end
										)' 
									ELSE '''' END + ') as InStock
								,monWListPrice  
							FROM 
								WareHouseItems        
							JOIN 
								Warehouses 
							ON 
								Warehouses.numWareHouseID=WareHouseItems.numWareHouseID        
							WHERE 
								numItemID IN ('+ @strSQL +' )
								AND WareHouseItems.numWareHouseID='+@numWareHouseID+' GROUP BY numItemID,numWareHouseItemId,numOnHand,monWListPrice'
		END
		ELSE
		BEGIN
			set @strSQL = '
						   declare @bitShowInStock as bit    
						   declare @bitShowQuantity as bit   
						   declare @bitAllowBackOrder as bit    
							set @bitShowInStock=0    
							set @bitShowQuantity=0  
						   select @bitShowInStock=isnull(bitShowInStock,0),@bitShowQuantity=isnull(bitShowQOnHand,0) from eCommerceDTL where numDomainID='+ CAST(@numDomainID AS VARCHAR) +'
						   select @bitAllowBackOrder=bitAllowBackOrder from item where numItemCode='+@numItemCode+'
						   select numItemID, WareHouseItems.numWareHouseItemId,isnull(sum(numOnHand),0) as numOnHand,('+case when @CharType<>'S' then     
						 '(Case when @bitShowInStock=1 then ( Case when @bitAllowBackOrder=1 then ''In Stock'' else     
						 (Case when isnull(sum(numOnHand),0)>0 then ''In Stock''+(Case when @bitShowQuantity=1 then ''(''+convert(varchar(20),isnull(sum(numOnHand),0))+'')'' else '''' end)     
						 else ''<font color=red>Out Of Stock</font>'' end ) end)    else '''' end)' else '''' end +') as InStock,monWListPrice  from WareHouseItems        
						   join Warehouses on Warehouses.numWareHouseID=WareHouseItems.numWareHouseID        
						   where numItemID='+@numItemCode+' and WareHouseItems.numWareHouseID='+@numWareHouseID+' and numWareHouseItemId in ('+@strSQL+') group by numItemID,numWareHouseItemId,numOnHand,monWListPrice'
		END
	END
	ELSE IF @strAtrrTemp=''        
	BEGIN
		IF @bitMatrix = 1
		BEGIN	
			set @strSQL='
						declare @bitShowInStock as bit    
						declare @bitShowQuantity as bit   
						declare @bitAllowBackOrder as bit    
						set @bitShowInStock=0    
						set @bitShowQuantity=0 
						select @bitShowInStock=isnull(bitShowInStock,0),@bitShowQuantity=isnull(bitShowQOnHand,0) from eCommerceDTL where numDomainID='+ CAST(@numDomainID AS VARCHAR) +'
						select @bitAllowBackOrder=bitAllowBackOrder from item where numItemCode IN (' + @strSQL + ')

						select TOP 1 numItemID, WareHouseItems.numWareHouseItemId,isnull(sum(numOnHand),0) as numOnHand,('+case when @CharType<>'S' then     
						'(Case when @bitShowInStock=1 then ( Case when @bitAllowBackOrder=1 then ''In Stock'' else     
						(Case when isnull(sum(numOnHand),0)>0 then ''In Stock''+(Case when @bitShowQuantity=1 then ''(''+convert(varchar(20),isnull(sum(numOnHand),0))+'')'' else '''' end)     
						else ''<font color=red>Out Of Stock</font>'' end ) end)    else '''' end)' else '''' end +') as InStock,monWListPrice from WareHouseItems        
						join Warehouses on Warehouses.numWareHouseID=WareHouseItems.numWareHouseID        
						where numItemID IN ('+ @strSQL +' )  and WareHouseItems.numWareHouseID='+@numWareHouseID+' group by numItemID,numWareHouseItemId,numOnHand,monWListPrice'
		END
		ELSE 
		BEGIN
			set @strSQL='
						declare @bitShowInStock as bit    
						declare @bitShowQuantity as bit   
						declare @bitAllowBackOrder as bit    
						set @bitShowInStock=0    
						set @bitShowQuantity=0 
						select @bitShowInStock=isnull(bitShowInStock,0),@bitShowQuantity=isnull(bitShowQOnHand,0) from eCommerceDTL where numDomainID='+ CAST(@numDomainID AS VARCHAR) +'
						select @bitAllowBackOrder=bitAllowBackOrder from item where numItemCode='+@numItemCode+'

						select numItemID, WareHouseItems.numWareHouseItemId,isnull(sum(numOnHand),0) as numOnHand,('+case when @CharType<>'S' then     
						'(Case when @bitShowInStock=1 then ( Case when @bitAllowBackOrder=1 then ''In Stock'' else     
						(Case when isnull(sum(numOnHand),0)>0 then ''In Stock''+(Case when @bitShowQuantity=1 then ''(''+convert(varchar(20),isnull(sum(numOnHand),0))+'')'' else '''' end)     
						else ''<font color=red>Out Of Stock</font>'' end ) end)    else '''' end)' else '''' end +') as InStock,monWListPrice from WareHouseItems        
						join Warehouses on Warehouses.numWareHouseID=WareHouseItems.numWareHouseID        
						where numItemID='+@numItemCode +'  and WareHouseItems.numWareHouseID='+@numWareHouseID+' group by numItemID,numWareHouseItemId,numOnHand,monWListPrice'

		END
		   
	END 
	print @strSQL
 exec(@strSQL)              
       
        
	

END
GO
