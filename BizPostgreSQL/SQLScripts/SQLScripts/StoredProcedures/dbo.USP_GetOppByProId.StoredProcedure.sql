/****** Object:  StoredProcedure [dbo].[USP_GetOppByProId]    Script Date: 07/26/2008 16:17:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getoppbyproid')
DROP PROCEDURE usp_getoppbyproid
GO
CREATE PROCEDURE [dbo].[USP_GetOppByProId]  
@numProId numeric,  
@numDomainId numeric  
as   
select P.numoppid ,M.vcPOppName  
from Projectsopportunities P  
join opportunitymaster M  
on P.numOppid= M.numoppid  
where P.numProId=@numProId and P.numDomainid=@numDomainId  and tintOpptype = 1
GO
