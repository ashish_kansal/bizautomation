/****** Object:  StoredProcedure [dbo].[USP_InsertJournalEntryHeaderForChecks]    Script Date: 07/26/2008 16:19:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created by Siva                    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_insertjournalentryheaderforchecks')
DROP PROCEDURE usp_insertjournalentryheaderforchecks
GO
CREATE PROCEDURE [dbo].[USP_InsertJournalEntryHeaderForChecks]                                
@datEntry_Date as datetime,                                
@numAmount as DECIMAL(20,5),                              
@numJournal_Id as numeric(9)=0,                          
@numDomainId as numeric(9)=0,                  
@numCheckId as numeric(9)=0,                
@numRecurringId as numeric(9)=0,              
@numUserCntID as numeric(9)=0   
As                                
Begin 
 if @numRecurringId=0 Set @numRecurringId=null                                
 If @numJournal_Id=0                        
  Begin                        
   Insert into General_Journal_Header(datEntry_Date,numAmount,numDomainId,numCheckId,numRecurringId,numCreatedBy,datCreatedDate)        
   Values(@datEntry_Date,@numAmount,@numDomainId,@numCheckId,@numRecurringId,@numUserCntID,getutcdate())                              
   Set @numJournal_Id = SCOPE_IDENTITY()                              
   Select @numJournal_Id                          
  End                        
End
GO
