/****** Object:  StoredProcedure [dbo].[USP_SaveShrtCutBarCustomFields]    Script Date: 07/26/2008 16:21:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_saveshrtcutbarcustomfields')
DROP PROCEDURE usp_saveshrtcutbarcustomfields
GO
CREATE PROCEDURE [dbo].[USP_SaveShrtCutBarCustomFields]  
@numGroupID numeric,  
@numDomainId numeric,  
@strName as varchar(150),  
@strUrl as varchar(200)  ,
@numContactId numeric =0,
@numTabId numeric(9)=0  
as  
  
insert into ShortCutBar   
(  
vclinkName,  
Link,  
bitdefault,  
numGroupId,  
numDomainId  ,
numContactId,numTabId
)  
values  
(  
@strName,  
@strUrl,  
0,  
@numGroupID,  
@numDomainId  ,
@numContactId,@numTabId
)
GO
