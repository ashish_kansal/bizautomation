/****** Object:  StoredProcedure [dbo].[USP_ProfitLossList]    Script Date: 07/26/2008 16:20:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created By Siva                                                                                                                              
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_profitlosslist')
DROP PROCEDURE usp_profitlosslist
GO
CREATE PROCEDURE [dbo].[USP_ProfitLossList]                         
 @numChartAcntId as numeric(9)=0,                      
 @numDomainId as numeric(9)=0,                                                                                                                                        
 @dtFromDate as Datetime,                                                
 @dtToDate as Datetime                                                                                                                               
As                                                                                                                                          
--Begin                                                                                                                                     
--  Select distinct CA.numAccountId as numAccountId,CA.vcCatgyName as AcntTypeDescription,                      
--  dbo.fn_GetCurrentOpeningBalanceForProfitLoss(@numChartAcntId,@dtFromDate, @dtToDate,@numDomainId) As Amount                                    
--  from Chart_Of_Accounts CA            
--  Left outer join ListDetails LD on CA.numAcntType=LD.numListItemID             
--  --inner join General_Journal_Details GJD on CA.numAccountId=GJD.numChartAcntId                      
--  Where  CA.numAccountId=@numChartAcntId And CA.numDomainId=@numDomainId      
--End
GO
