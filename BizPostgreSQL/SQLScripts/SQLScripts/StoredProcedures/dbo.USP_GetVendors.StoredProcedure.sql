/****** Object:  StoredProcedure [dbo].[USP_GetVendors]    Script Date: 07/26/2008 16:18:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getvendors')
DROP PROCEDURE usp_getvendors
GO
CREATE PROCEDURE [dbo].[USP_GetVendors]         
@numDomainID as numeric(9)=0,    
@numItemCode as numeric(9)=0       
as        
select div.numCompanyID,Vendor.numItemCode,Vendor.numVendorID,vcPartNo,vcCompanyName as Vendor,        
vcFirstName+ ' '+vcLastName as ConName,        
convert(varchar(15),numPhone)+ ', '+ convert(varchar(15),numPhoneExtension) as Phone,ISNULL(monCost,0) monCost,vcItemName,ISNULL([intMinQty],0) intMinQty
,numContactId, vcNotes,
(CASE WHEN (SELECT COUNT(*) FROM ItemDetails INNER JOIN Item ON numItemKitID=Item.numItemCode WHERE Item.bitAssembly=1 AND numChildItemID = I.numItemCode) > 0 THEN 1 ELSE 0 END) AS bitUsedInAssembly,
ISNULL(monCost,0) * dbo.fn_UOMConversion(I.numBaseUnit,I.numItemCode,I.numDomainID,I.numPurchaseUnit) AS monCostEach
from Vendor          
join divisionMaster div        
on div.numdivisionid=numVendorid        
join companyInfo com        
on com.numCompanyID=div.numCompanyID        
left join AdditionalContactsInformation ADC        
on ADC.numdivisionID=Div.numdivisionID   
join Item I  
on I.numItemCode= Vendor.numItemCode
WHERE ISNULL(ADC.bitPrimaryContact,0)=1 and Vendor.numDomainID=@numDomainID and Vendor.numItemCode= @numItemCode
GO
