/****** Object:  StoredProcedure [dbo].[usp_GetAllAuthenticationGroups]    Script Date: 07/26/2008 16:16:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--modified by anoop jayaraj
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getallauthenticationgroups')
DROP PROCEDURE usp_getallauthenticationgroups
GO
CREATE PROCEDURE [dbo].[usp_GetAllAuthenticationGroups]    
 @numGroupID NUMERIC(18,0)= 0,  
 @vcGroupTypes AS VARCHAR(MAX)='1',
 @numDomainID AS NUMERIC(18,0)=0       
AS
BEGIN
	IF @numGroupID = 0    
	BEGIN    
		SELECT 
			numGroupID
			,vcGroupName 
		FROM 
			AuthenticationGroupMaster   
		WHERE 
			numDomainID=@numDomainID
			AND 1 = (CASE 
						WHEN LEN(ISNULL(@vcGroupTypes,'')) > 0
						THEN (CASE WHEN tintGroupType IN (SELECT Id FROM dbo.SplitIDs(@vcGroupTypes,',')) THEN 1 ELSE 0 END)
						ELSE 1
					END)
		ORDER BY 
			numGroupID    
	END    
	ELSE    
	BEGIN    
		SELECT 
			numGroupID
			,vcGroupName 
		FROM 
			AuthenticationGroupMaster    
		WHERE 
			numGroupID=@numGroupID    
	END
END
GO
