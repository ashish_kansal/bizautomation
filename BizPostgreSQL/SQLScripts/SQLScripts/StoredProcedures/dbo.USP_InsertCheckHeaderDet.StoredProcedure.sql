/****** Object:  StoredProcedure [dbo].[USP_InsertCheckHeaderDet]    Script Date: 07/26/2008 16:19:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created by Siva                              
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_insertcheckheaderdet')
DROP PROCEDURE usp_insertcheckheaderdet
GO
CREATE PROCEDURE [dbo].[USP_InsertCheckHeaderDet]                              
@numCheckId as numeric(9)=0 ,                            
@datEntry_Date as datetime,                                          
@numAmount as DECIMAL(20,5),                                  
@varMemo as varchar(200)='',                            
@numCustomerId as numeric(18)=0,                          
@numAcntTypeId as numeric(18)=0,          
@numCheckNo as numeric(18)=0,        
@numBankRoutingNo as numeric(18)=0,        
@numBankAccountNo as numeric(18)=0,                            
@numCheckCompanyId as numeric(18)=0,      
@numRecurringId as numeric(18)=0,                
@numDomainId as numeric(9)=0,              
@numUserCntID as numeric(9)=0              
as                                           
Begin      

set @numCheckCompanyId=(case @numCheckCompanyId when 0 then null else @numCheckCompanyId end);

 if @numRecurringId=0 Set @numRecurringId=null                   
 If @numCheckId=0                                  
 Begin              
  Insert into CheckDetails(datCheckDate,monCheckAmt,varMemo,numCustomerId,numAcntTypeId,numCheckNo,numBankRoutingNo,numBankAccountNo,numCheckCompanyId,numDomainId,numRecurringId,numCreatedBy,dtCreatedDate,[bitIsPrint]) Values            
  (@datEntry_Date,@numAmount,@varMemo,@numCustomerId,@numAcntTypeId,@numCheckNo,@numBankRoutingNo,@numBankAccountNo,@numCheckCompanyId,@numDomainId,@numRecurringId,@numUserCntID,getutcdate(),1) --inserting IsPrinted to true because on front end we have single button called "Print Checks & Pay Amt Selected" -- by chintan
  Set @numCheckId = SCOPE_IDENTITY()                                        
  Select @numCheckId        
  -- To Update Money in CheckCompanyDetails    
    Update CheckCompanyDetails Set monAmount=@numAmount,vcMemo=@varMemo Where numCheckCompanyId=@numCheckCompanyId And numDomainId=@numDomainId  
 End                            
                        
 If @numCheckId<>0                        
 Begin                        
  Update CheckDetails Set datCheckDate=@datEntry_Date,monCheckAmt=@numAmount,varMemo=@varMemo,numCustomerId=@numCustomerId,numAcntTypeId=@numAcntTypeId,        
 numCheckNo=@numCheckNo,numBankRoutingNo=@numBankRoutingNo,numBankAccountNo=@numBankAccountNo,numCheckCompanyId=@numCheckCompanyId, numRecurringId=@numRecurringId,numDomainId=@numDomainId,          
 numModifiedBy=@numUserCntID, dtModifiedDate=getutcdate()  Where numCheckId=@numCheckId And numDomainId=@numDomainId        
  -- To Update Money in CheckCompanyDetails    
    Update CheckCompanyDetails Set monAmount=@numAmount,vcMemo=@varMemo Where numCheckCompanyId=@numCheckCompanyId And numDomainId=@numDomainId  
 End                        
End
