/****** Object:  StoredProcedure [dbo].[USP_GetFixedAssetDetails]    Script Date: 07/26/2008 16:17:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created By Siva

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getfixedassetdetails')
DROP PROCEDURE usp_getfixedassetdetails
GO
CREATE PROCEDURE [dbo].[USP_GetFixedAssetDetails]
@numFixedAssetId as numeric(9)=0
As
Begin
 Select * From FixedAssetDetails Where numFixedAssetId=@numFixedAssetId
End
GO
