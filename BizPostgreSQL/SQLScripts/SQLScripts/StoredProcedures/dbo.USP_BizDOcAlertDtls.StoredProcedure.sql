/****** Object:  StoredProcedure [dbo].[USP_BizDOcAlertDtls]    Script Date: 07/26/2008 16:14:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- exec USP_BizDOcAlertDtls @numBizDocID=287,@bitCreated=0,@bitModified=1,@bitApproved=0,@numDomainID=1
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_bizdocalertdtls')
DROP PROCEDURE usp_bizdocalertdtls
GO
CREATE PROCEDURE [dbo].[USP_BizDOcAlertDtls]      
@numBizDocID as numeric=0,
@bitCreated AS BIT=0,
@bitModified AS BIT=0,
@bitApproved AS BIT=0,
@numDomainID AS numeric
as      
if @numBizDocID=0   
	select numBizDocAlertID,dbo.fn_GetListItemName(numBizDocID) as BizDoc,      
	convert(varchar,bitCreated)+'~'+      
	convert(varchar,bitModified)+'~'+      
	convert(varchar,bitApproved) as [Is],      
	numEmailTemplate,      
	vcDocName,      
	convert(varchar,bitOppOwner)+'~'+      
	convert(varchar,bitCCManager)+'~'+      
	convert(varchar,bitPriConatct) as [To]      
	from BizDocAlerts Biz      
	left join GenericDocuments as Doc          
	on Doc.numGenericDocID=Biz.numEmailTemplate
	WHERE Biz.[numDomainID]=@numDomainID
      
if @numBizDocID >0
BEGIN
	IF	@bitCreated =1 
	BEGIN
		select * from BizDocAlerts where numBizDocID=@numBizDocID AND numDomainID = @numDomainID
		AND [bitCreated] = @bitCreated 
	END
	ELSE IF @bitModified =1 
	BEGIN
		select * from BizDocAlerts where numBizDocID=@numBizDocID AND numDomainID = @numDomainID
		AND [bitModified] = @bitModified
	END
	ELSE IF @bitApproved =1 
	BEGIN
		select * from BizDocAlerts where numBizDocID=@numBizDocID AND numDomainID = @numDomainID
		AND [bitApproved] = @bitApproved
	END
	ELSE 
		select * from BizDocAlerts where numBizDocID=@numBizDocID AND numDomainID = @numDomainID
	
END
	
GO
