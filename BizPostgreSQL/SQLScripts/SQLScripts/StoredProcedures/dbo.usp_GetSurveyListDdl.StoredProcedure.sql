/****** Object:  StoredProcedure [dbo].[usp_GetSurveyListDdl]    Script Date: 07/26/2008 16:18:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created By: Gangadhar Rao                                     
--Purpose: list of surveys                  
--Created Date: 03/12/2008  
--Modified By: Gangadhar Rao  
  
                     
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getsurveylistddl')
DROP PROCEDURE usp_getsurveylistddl
GO
CREATE PROCEDURE [dbo].[usp_GetSurveyListDdl]      
 @numDomainID numeric                   
AS                             
BEGIN                    
 SELECT sm.numSurID, vcSurName                   
                
  FROM SurveyMaster sm                
                 
  WHERE sm.numDomainID = @numDomainID                
                
END
GO
