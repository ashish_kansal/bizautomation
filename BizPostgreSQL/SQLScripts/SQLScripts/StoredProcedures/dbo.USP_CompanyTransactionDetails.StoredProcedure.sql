/****** Object:  StoredProcedure [dbo].[USP_CompanyTransactionDetails]    Script Date: 07/26/2008 16:15:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created By Siva            
/*the transaction list will include ALL authoritative BizDocs over the past 12 months, regardless of whether they've been paid or not. The sum total for those will be reflected in the chart accordingly.*/
-- exec USP_CompanyTransactionDetails @numDomainId=72,@numDivisionId=16150,@ClientTimeZoneOffset=-330
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_companytransactiondetails')
DROP PROCEDURE usp_companytransactiondetails
GO
CREATE PROCEDURE [dbo].[USP_CompanyTransactionDetails]
    (
      @numDomainId AS NUMERIC(9) = 0,
      @numDivisionId AS NUMERIC(9) = 0,
      @ClientTimeZoneOffset AS INT ,
      @CurrentPage INT,
	  @PageSize INT,
      @TotRecs INT   OUTPUT  
    )
AS 
    BEGIN

-- show transaction details of last 12 months 
DECLARE @numAuthoritativePurchase AS NUMERIC(18);SET @numAuthoritativePurchase=0
DECLARE @numAuthoritativeSales AS NUMERIC(18);SET @numAuthoritativeSales=0
SELECT    @numAuthoritativePurchase = isnull(numAuthoritativePurchase,0) ,@numAuthoritativeSales=  isnull(numAuthoritativeSales,0) FROM AuthoritativeBizDocs WHERE numDomainId = @numDomainId 

		DECLARE @firstRec AS INTEGER
		DECLARE @lastRec AS INTEGER
        DECLARE @StartDate DATETIME
        DECLARE @EndDate DATETIME
        DECLARE @year AS VARCHAR(5)    
		DECLARE @Month AS VARCHAR(5)   
		DECLARE @day AS VARCHAR(5)  
   
		SET @firstRec = (@CurrentPage - 1)* @PageSize
		SET @lastRec = (@CurrentPage * @PageSize + 1)
		SET @year=YEAR(GETUTCDATE())     
		SET @Month=MONTH(GETUTCDATE())    
		IF @Month<10 SET @Month='0'+@Month   
		
		set @day=(SELECT CASE WHEN @Month = '01'  
					THEN 31  
					WHEN @Month = '02'  
					THEN CASE WHEN (@year % 4 = 0 AND @year % 100 <> 0) OR  
								   @year % 400 = 0  
							  THEN 29  
							  ELSE 28  
					END  
					WHEN @Month = '03'  
					THEN 31  
					WHEN @Month = '04'  
					THEN 30  
					WHEN @Month = '05'  
					THEN 31  
					WHEN @Month = '06'  
					THEN 30  
					WHEN @Month = '07'  
					THEN 31  
					WHEN @Month = '08'  
					THEN 31  
					WHEN @Month = '09'  
					THEN 30  
					WHEN @Month = '10'  
					THEN 31  
					WHEN @Month = '11'  
					THEN 30  
					WHEN @Month = '12'  
					THEN 31 END )  
		  
		set @EndDate=@Month+'/'+ @day +'/'+@year 
		set @year=year(getutcdate()-365)     
		set @Month=month(getutcdate()-365)  
		set @startDate=@Month+'/01/'+@year
	
				CREATE TABLE #tempTable (
				ID INT   IDENTITY   PRIMARY KEY,
				numOppId NUMERIC(9),
				vcData VARCHAR(1000),
				[Name] VARCHAR(1000),
				TotalAmt DECIMAL(20,5) ,
				TotalBizDocAmt DECIMAL(20,5),AmountPaid DECIMAL(20,5),OppType VARCHAR(50)  ,BalanceAmt DECIMAL(20,5),DueDate VARCHAR(50),[Status]  BIT ,numOppBizDocsId INT ,dtCreatedDate VARCHAR(50),
				vcBizDocIdName VARCHAR(500) )
				INSERT INTO #tempTable 
				SELECT  Opp.numOppId,
                LD.vcData AS vcData,
                Opp.vcPOppName AS Name,
                Opp.monDealAmount AS TotalAmt,
                OppBizDocs.[monDealAmount] AS TotalBizDocAmt,
                ISNULL(OppBizDocs.monAmountPaid,0) AS AmountPaid,
                CASE WHEN tintOppType = 1 THEN 'S.O'
                     ELSE CASE WHEN tintOppType = 2 THEN 'P.O'
                          END
                END AS OppType,
--   dateadd(minute,-@ClientTimeZoneOffset,OppBizDocsDet.dtCreationDate) as datePaid,    
                --isnull(Opp.monPAmount,0) 
                isnull(OppBizDocs.[monDealAmount],0)-ISNULL(OppBizDocs.monAmountPaid,0)  AS BalanceAmt,
--                dbo.FormatedDateFromDate(DATEADD(day,CASE WHEN Opp.bitBillingTerms = 1 
--														  THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(Opp.intBillingDays,0)),0))  
--														  ELSE 0 
--													 END,OppBizDocs.[dtFromDate]),@numDomainId) AS DueDate,
				dbo.FormatedDateFromDate(DATEADD(day,CASE WHEN Opp.bitBillingTerms = 1 
														  THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(Opp.intBillingDays,0)), 0))         
														  ELSE 0 
													 END,OppBizDocs.[dtFromDate]),@numDomainId) AS DueDate,
                CASE when  isnull(OppBizDocs.[monDealAmount],0)-ISNULL(OppBizDocs.monAmountPaid,0) = 0 THEN 0
--					WHEN DATEDIFF(Day,
--                                   DATEADD(day,
--                                           CASE WHEN Opp.bitBillingTerms = 1
--                                                THEN  convert(int,ISNULL(dbo.fn_GetListItemName(isnull(Opp.intBillingDays,0)),0)) 
--                                                ELSE 0
--                                           END, OppBizDocs.[dtFromDate]),
--                                   GETDATE()) > 0 THEN 1
					WHEN DATEDIFF(Day,
                                   DATEADD(day,
                                           CASE WHEN Opp.bitBillingTerms = 1
                                                THEN  convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(Opp.intBillingDays,0)), 0))         
                                                ELSE 0
                                           END, OppBizDocs.[dtFromDate]),
                                   GETDATE()) > 0 THEN 1
                     ELSE 0
                END AS Status,
                OppBizDocs.[numOppBizDocsId],
                dbo.FormatedDateFromDate(OppBizDocs.dtCreatedDate,@numDomainId) dtCreatedDate,'BD-' + SUBSTRING(OppBizDocs.vcBizDocId,LEN(OppBizDocs.vcBizDocId) - CHARINDEX('-', REVERSE(OppBizDocs.vcBizDocId)) + 2,CHARINDEX('-', REVERSE(vcBizDocId))) AS vcBizDocIdName
        FROM    OpportunityMaster Opp
                INNER JOIN OpportunityBizDocs OppBizDocs ON OppBizDocs.numOppId = Opp.numOppId
                INNER JOIN AdditionalContactsInformation ADC ON Opp.numContactId = ADC.numContactId
                INNER JOIN DivisionMaster Div ON Opp.numDivisionId = Div.numDivisionID
                                                 AND ADC.numDivisionId = Div.numDivisionID
                INNER JOIN CompanyInfo C ON Div.numCompanyID = C.numCompanyId
                INNER JOIN ListDetails LD ON OppBizDocs.numBizDocId = LD.numListItemID
        WHERE   OPP.numDomainId = @numDomainId
--                AND Opp.tintOppStatus = 1
                AND Opp.numDivisionId = @numDivisionId
                AND Div.numDomainID = @numDomainId 
                AND OppBizDocs.bitAuthoritativeBizDocs=1
                AND CONVERT(DATETIME, CONVERT(CHAR, OppBizDocs.dtCreatedDate, 106)) >= @StartDate
                AND CONVERT(DATETIME, CONVERT(CHAR, OppBizDocs.dtCreatedDate, 106)) <= @EndDate
				AND (OppBizDocs.numBizDocId IN(@numAuthoritativePurchase,@numAuthoritativeSales))
--                AND ( OppBizDocs.numBizDocId IN (
--                      SELECT    numAuthoritativePurchase
--                      FROM      AuthoritativeBizDocs
--                      WHERE     numDomainId = @numDomainId )
--                      OR OppBizDocs.numBizDocId IN (
--                      SELECT    numAuthoritativeSales
--                      FROM      AuthoritativeBizDocs
--                      WHERE     numDomainId = @numDomainId )
--                    )


--        GROUP BY Opp.[numOppId],
--                [vcData],
--                Opp.vcPOppName,
--                [monPAmount],
--                Opp.monDealAmount,
--                Opp.[tintOppType],
--                Opp.bitBillingTerms,
--                Opp.[intBillingDays],
--                OppBizDocs.[dtCreatedDate],
--                OppBizDocs.[numOppBizDocsId],
--                OppBizDocs.[monDealAmount],
--                OppBizDocs.dtFromDate,OppBizDocs.vcBizDocId --OppBizDocsDet.monAmount
                
   SET @TotRecs = (SELECT COUNT(*) FROM #temptable)
   SELECT * FROM #tempTable WHERE Id > @firstRec AND Id < @lastRec

   DROP TABLE #tempTable
    END
