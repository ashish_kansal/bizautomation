/****** Object:  StoredProcedure [dbo].[usp_UpdateExistingRoutingRule]    Script Date: 07/26/2008 16:21:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Modified by anoop jayaraj               
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_updateexistingroutingrule')
DROP PROCEDURE usp_updateexistingroutingrule
GO
CREATE PROCEDURE [dbo].[usp_UpdateExistingRoutingRule]                  
 @numRoutID numeric(9)=0,                  
 @numDomainID numeric(9)=0,                  
 @vcRoutName varchar(50)='',  
 @tintEqualTo as tinyint=0,                                              
 @numValue numeric(9)=0,                                
 @numUserCntID numeric=0,                       
 @tintPriority tinyint=0,  
 @strValues as text,  
 @strDistribitionList as text,
 @vcDBCoulmnName as varchar(100)='',
 @vcSelectedColumn as varchar(200)=''                        
AS       
  
  
  update RoutingLeads   
 set numDomainID=@numDomainID,  
 vcRoutName=@vcRoutName,  
 tintEqualTo=@tintEqualTo,  
 numValue=@numValue,  
 numModifiedBy=@numUserCntID,  
 dtModifiedDate=getutcdate(),
 vcDBColumnName=@vcDBCoulmnName,
 vcSelectedCoulmn=@vcSelectedColumn,  
 tintPriority=@tintPriority  
 where numRoutID=@numRoutID  
        delete from RoutinLeadsValues where numRoutID=@numRoutID  
 if convert(varchar(2),@strValues)<>''  
 begin  
    
  DECLARE @hDoc1 int                                    
   EXEC sp_xml_preparedocument @hDoc1 OUTPUT, @strValues                                    
                                      
   insert into RoutinLeadsValues                                    
    (numRoutID,vcValue)                                    
                                       
   select @numRoutID,X.vcValue   
   from(SELECT *FROM OPENXML (@hDoc1,'/NewDataSet/Table',2)                                    
   WITH  (                                    
    vcValue varchar(100)              
    ))X                                                     
   EXEC sp_xml_removedocument @hDoc1     
  
 end  
 delete from RoutingLeadDetails where numRoutID=@numRoutID  
 if convert(varchar(2),@strDistribitionList)<>''  
 begin  
    
  DECLARE @hDoc2 int                                    
   EXEC sp_xml_preparedocument @hDoc2 OUTPUT, @strDistribitionList                                    
                                      
   insert into RoutingLeadDetails                                    
    (numRoutID,numEmpId,intAssignOrder)                                    
                                       
   select @numRoutID,X.numEmpId,X.intAssignOrder   
   from(SELECT *FROM OPENXML (@hDoc2,'/NewDataSet/Table',2)                                    
   WITH  (                                    
    numEmpId numeric(9),  
    intAssignOrder int              
    ))X                                                     
   EXEC sp_xml_removedocument @hDoc2     
  
 end
GO
