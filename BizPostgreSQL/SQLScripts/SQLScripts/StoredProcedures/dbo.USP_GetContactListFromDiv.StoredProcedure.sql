/****** Object:  StoredProcedure [dbo].[USP_GetContactListFromDiv]    Script Date: 07/26/2008 16:16:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getcontactlistfromdiv')
DROP PROCEDURE usp_getcontactlistfromdiv
GO
CREATE PROCEDURE [dbo].[USP_GetContactListFromDiv]  
@numDivisionId as numeric,  
@numDomainId as numeric ,  
@ClientTimeZoneOffset as numeric   
  
as  
select                                       
                                 
c.numcontractId,                                      
c.numDivisionId,                                      
vcContractName,                          
case when bitdays =1 then                            
isnull(convert(varchar(100),                          
case when datediff(day,bintStartdate,getutcdate())>=0 then datediff(day,bintStartdate,getutcdate()) else 0 end                          
                          
),0) +' of '+ isnull(convert(varchar(100),datediff(day,bintStartDate,bintExpDate)),0)                           
else '-' end as Days,                                      
                          
case when bitincidents =1 then                            
isnull(convert(varchar(100),dbo.GetIncidents(c.numcontractId,@numDomainId ,c.numDivisionId ),0),0) +' of '+ isnull(convert(varchar(100),numincidents),0)                           
else '-' end as Incidents,                              
                            
case when bitamount =1 then                            
isnull(convert(varchar(100),dbo.GetContractRemainingAmount(c.numcontractId,c.numDivisionId,@numDomainId)),0) + ' of ' + convert(varchar(100),convert(decimal(10,2),c.numAmount+( Select isnull(sum(monAmount),0) From OpportunityBizDocsDetails Where      
  numContractId= c.numContractId )))                           
else '-' end  as Amount,                                    
                          
case when bithour =1 then                            
isnull(convert(varchar(100),dbo.GetContractRemainingHours(c.numcontractId,c.numDivisionId,@numDomainId)),0) +' of ' + convert(varchar(100),c.numhours )                          
else '-' end as Hours,                                     
                                    
dbo.FormatedDateFromDate(dateadd(minute,-@ClientTimeZoneOffset,c.bintCreatedOn),@numDomainId) as bintCreatedOn ,                                      
com.vcCompanyName                                       
                                     
from ContractManagement c                                                        
join divisionmaster d on c.numDivisionId= d.numDivisionId                                      
join companyinfo com on d.numcompanyId = com.numcompanyId     
  WHERE c.numdivisionid = @numDivisionId and c.numdomainId = @numDomainId
GO
