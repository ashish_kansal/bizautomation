/****** Object:  StoredProcedure [dbo].[USP_GetAllStates]    Script Date: 07/26/2008 16:16:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- created By Anoop Jayaraj    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getallstates')
DROP PROCEDURE usp_getallstates
GO
CREATE PROCEDURE [dbo].[USP_GetAllStates]        
@numDomainID as numeric(9)=0      
as          
select numStateID,vcState,numCountryID,vcData,vcAbbreviations from State    
join ListDetails on numListItemID=  numCountryID       
where State.numDomainID=@numDomainID  
order by  vcState
GO
