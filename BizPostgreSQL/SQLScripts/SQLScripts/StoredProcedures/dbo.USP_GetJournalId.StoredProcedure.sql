/****** Object:  StoredProcedure [dbo].[USP_GetJournalId]    Script Date: 07/26/2008 16:17:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created By Siva            
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getjournalid')
DROP PROCEDURE usp_getjournalid
GO
CREATE PROCEDURE [dbo].[USP_GetJournalId]           
@numCashCreditCardId as numeric(9)=0,            
@numDomainId as numeric(9)=0            
As            
Begin             
Declare @numJournalId as numeric(9)          
      
Select numJournal_Id From General_Journal_Header Where numCashCreditCardId=@numCashCreditCardId and numDomainId=@numDomainId       
End
GO
