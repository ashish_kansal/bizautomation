/****** Object:  StoredProcedure [dbo].[USP_FixedAsset]    Script Date: 07/26/2008 16:15:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- Created by Siva    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_fixedasset')
DROP PROCEDURE usp_fixedasset
GO
CREATE PROCEDURE [dbo].[USP_FixedAsset]      
As      
Begin      
   Select LD.numListItemID, LD.vcData as AssetGroup,isnull(FAD.monCost,0) as  Cost,isnull(FAD.monCurrentValue,0) as CurrentValue,isnull(FAD.monFiscalDepOrAppAmt,0) as [Fiscal YTD Depreciation or Appreciation (Total Amount)]  
 from ListDetails LD  
   Left outer join  FixedAssetDetails FAD on LD.numListItemID=FAD.numAssetClass
   Where numListId=64   
End
GO
