/****** Object:  StoredProcedure [dbo].[USP_BestAccountClosedReport]    Script Date: 07/26/2008 16:14:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Create By Siva    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_bestaccountclosedreport')
DROP PROCEDURE usp_bestaccountclosedreport
GO
CREATE PROCEDURE [dbo].[USP_BestAccountClosedReport]      
 @numDivisionId integer,      
 @numDomainId integer,    
 @numStatus tinyint,  
 @dtDateFrom datetime,                      
 @dtDateTo datetime                     
                               
as      
Begin        
  SELECT  Opp.numOppId,                                         
  Opp.vcPOppName AS Name,                                         
  dbo.GetOppStatus(Opp.numOppId) as STAGE,                                         
  Opp.intPEstimatedCloseDate AS CloseDate,                                         
  Opp.numRecOwner,                                        
  ADC.vcFirstName + ' ' + ADC.vcLastName AS Contact,                                         
  C.vcCompanyName AS Company,                                        
                C.numCompanyID,                                        
  Div.tintCRMType,                                        
  ADC.numContactID,                                        
  Div.numDivisionID,                                        
  Div.numTerID,                                        
  Opp.monPAmount,                                        
  ADC1.vcFirstName+' '+ADC1.vcLastName as [UserName],            
  dbo.fn_GetContactName(Opp.numAssignedTo)+'/ '+dbo.fn_GetContactName(Opp.numAssignedBy) as AssignedToBy                                      
  FROM OpportunityMaster Opp                                         
  INNER JOIN AdditionalContactsInformation ADC                                         
  ON Opp.numContactId = ADC.numContactId                                         
  INNER JOIN DivisionMaster Div                                         
  ON Opp.numDivisionId = Div.numDivisionID AND ADC.numDivisionId = Div.numDivisionID                                         
  INNER JOIN CompanyInfo C                                         
  ON Div.numCompanyID = C.numCompanyId                                           
  left join AdditionalContactsInformation ADC1 on ADC1.numContactId=Opp.numRecOwner Where Opp.tintOppType=1
  And Opp.tintOppStatus = @numStatus AND Div.numDivisionID = @numDivisionId AND Opp.numDomainId=@numDomainId   
  And Opp.bintCreatedDate Between @dtDateFrom And @dtDateTo  
  
     
      
End
GO
