/****** Object:  StoredProcedure [dbo].[usp_GetAuthorizationGroupsForSelectedUser]    Script Date: 07/26/2008 16:16:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getauthorizationgroupsforselecteduser')
DROP PROCEDURE usp_getauthorizationgroupsforselecteduser
GO
CREATE PROCEDURE [dbo].[usp_GetAuthorizationGroupsForSelectedUser]  
 @numUserID NUMERIC(9)   
--    
AS  
  SELECT numGroupID FROM Usermaster  
  WHERE numUserID=@numUserID  
  ORDER BY numGroupID
GO
