/****** Object:  StoredProcedure [dbo].[usp_GetUserNameFromContacts]    Script Date: 07/26/2008 16:18:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created By: Debasish Nag      
--Purpose: To return the user who has checked out the survey
--Created On: 9th Mar 2006
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getusernamefromcontacts')
DROP PROCEDURE usp_getusernamefromcontacts
GO
CREATE PROCEDURE [dbo].[usp_GetUserNameFromContacts]
 @numDomainID numeric,      
 @numUserID numeric      
AS                  
BEGIN                  
 SELECT aci.vcFirstName + ' '+ aci.vcLastname AS vcUserName     
 FROM UserMaster UM INNER JOIN AdditionalContactsInformation ACI ON ACI.numContactId = UM.numUserDetailId
 WHERE um.numDomainID=@numDomainID         
 AND um.numUserId  = @numUserID 
END
GO
