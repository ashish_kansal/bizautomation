/****** Object:  StoredProcedure [dbo].[usp_GetPageLevelUserRights]    Script Date: 07/26/2008 16:18:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Modified by anoop jayaraj  
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getpageleveluserrights')
DROP PROCEDURE usp_getpageleveluserrights
GO
CREATE PROCEDURE [dbo].[usp_GetPageLevelUserRights]          
 @numUserID numeric,          
 --@vcFileName varchar (100),          
 @numModuleID numeric=0,          
 @numPageID numeric=0             
--          
As           
declare @numDomainID as numeric(9)  
declare @numAdminID as numeric(9)  
declare @numUserDetailId as numeric(9)

select  @numDomainID=numDomainID,@numUserDetailId=numUserDetailId from UserMaster where numUserID=@numUserID  
select @numAdminID=numAdminID from domain where numDomainID=@numDomainID  

if @numAdminID=@numUserDetailId  
 SELECT top 1 UM.numUserId, UM.vcUserName,
-- @vcFileName as vcFileName,        
   @numModuleID as numModuleID, @numPageID as numPageID, 3 As intExportAllowed,         
   3 As intPrintAllowed, 3 As intViewAllowed,         
  3 As intAddAllowed, 3 As intUpdateAllowed,         
   3 As intDeleteAllowed        
   FROM UserMaster UM     
   WHERE UM.numUserId= @numUserID      
     
else  
       
        
 SELECT top 1 UM.numUserId, UM.vcUserName, PM.vcFileName,        
   GA.numModuleID, GA.numPageID, MAX(GA.intExportAllowed) As intExportAllowed,         
   MAX(GA.intPrintAllowed) As intPrintAllowed, MAX(GA.intViewAllowed) As intViewAllowed,         
   MAX(GA.intAddAllowed) As intAddAllowed, MAX(GA.intUpdateAllowed) As intUpdateAllowed,         
   MAX(GA.intDeleteAllowed) As intDeleteAllowed        
   FROM UserMaster UM INNER JOIN GroupAuthorization GA ON GA.numGroupID=UM.numGroupID LEFT JOIN PageMaster PM ON PM.numPageID=GA.numPageID AND PM.numModuleID=GA.numModuleID
   WHERE         
	GA.numModuleID=@numModuleID          
    AND GA.numPageID=@numPageID      
	AND UM.numUserId= @numUserID      
   GROUP BY UM.numUserId, UM.vcUserName,        
    PM.vcFileName, GA.numModuleID, GA.numPageID
GO
