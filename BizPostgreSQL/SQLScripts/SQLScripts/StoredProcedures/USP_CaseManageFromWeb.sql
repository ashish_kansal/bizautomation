--created by Prasanta      
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_CaseManageFromWeb')
DROP PROCEDURE USP_CaseManageFromWeb
GO
CREATE PROCEDURE [dbo].[USP_CaseManageFromWeb]                      
@numCaseId as numeric(9)=0 output,                      
@numDivisionID as numeric(9)=null,                      
@numContactId as numeric(9)=null,                      
@vcCaseNumber as varchar(50)='',                      
@textSubject as varchar(2000)='',                      
@textDesc as text='',                      
@numUserCntID as numeric(9)=null,                                  
@numDomainID as numeric(9)=null,                      
@tintSupportKeyType as tinyint,                      
@intTargetResolveDate as datetime,                      
@numStatus as numeric(9)=null,                      
@numPriority as numeric(9)=null,                      
@textInternalComments as text='',                      
@numReason as numeric(9)=null,                      
@numOrigin  as numeric(9)=null,                      
@numType as numeric(9)=null,                      
@numAssignedTo as numeric(9)=0,              
@numContractId as numeric(9)=0,
@vcEmail varchar(50)=null              
AS
BEGIN 
	IF @intTargetResolveDate='Jan  1 1753 12:00:00:000AM' 
	BEGIN
		SET @intTargetResolveDate = NULL                        
	END

	IF @numCaseId = 0                      
	BEGIN              
		IF EXISTS (SELECT numContactId FROM AdditionalContactsInformation WHERE numDomainID=@numDomainID AND vcEmail=@vcEmail)
		BEGIN
			SELECT TOP 1 
				@numContactId=numContactId
				,@numDivisionID=numDivisionId
			FROM 
				AdditionalContactsInformation 
			WHERE 
				numDomainID=@numDomainID 
				AND vcEmail=@vcEmail
		END
		    
		INSERT INTO Cases
		(                      
			numDivisionID,                      
			numContactId,                      
			vcCaseNumber,                      
			textSubject,                      
			textDesc,                      
			numCreatedBy,                      
			bintCreatedDate,                      
			numModifiedBy,                      
			bintModifiedDate,                      
			numDomainID,                      
			tintSupportKeyType,                      
			intTargetResolveDate,          
			numRecOwner ,    
			numContractId,
			numStatus,numPriority,numOrigin,numType,numReason,textInternalComments                 
		 )                      
		 VALUES                      
		 (                      
			 @numDivisionID,                      
			 @numContactId,                      
			 @vcCaseNumber,                      
			 @textSubject,                      
			 @textDesc,                      
			 @numUserCntID,                      
			 getutcdate(),                      
			 @numUserCntID,                      
			 getutcdate(),                      
			 @numDomainID,                      
			 @tintSupportKeyType,                      
			 @intTargetResolveDate,          
			 -1 ,    
			 @numContractId,
			 @numStatus,@numPriority,@numOrigin,@numType,@numReason,@textInternalComments
		 )       
                
		SET @numCaseId=@@identity               
              
      
		--Map Custom Field	
		DECLARE @tintPageID AS TINYINT;SET @tintPageID=3
  	
		EXEC dbo.USP_AddParentChildCustomFieldMap
		@numDomainID = @numDomainID, --  numeric(18, 0)
		@numRecordID = @numCaseId, --  numeric(18, 0)
		@numParentRecId = @numDivisionId, --  numeric(18, 0)
		@tintPageID = @tintPageID --  tinyint               
    END                 
	ELSE    
	BEGIN                  
		DECLARE @tempAssignedTo NUMERIC(18,0) = NULL  
		SELECT 
			@tempAssignedTo=ISNULL(numAssignedTo,0) 
		FROM 
			Cases 
		WHERE 
			numCaseId=@numCaseId              
          
		IF (@tempAssignedTo<>@numAssignedTo and  @numAssignedTo<>'0')            
		BEGIN
			UPDATE 
				Cases 
			SET 
				numAssignedTo=@numAssignedTo
				,numAssignedBy=@numUserCntID 
			WHERE 
				numCaseId=@numCaseId             
		END    
		ELSE IF (@numAssignedTo =0)            
		BEGIN
			UPDATE 
				Cases 
			SET 
				numAssignedTo=0
				,numAssignedBy=0 
			WHERE 
				numCaseId=@numCaseId          
		END
	END
END
GO
