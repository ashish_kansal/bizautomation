/****** Object:  StoredProcedure [dbo].[USP_GetContactList]    Script Date: 07/26/2008 16:16:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created By Anoop jayaraj                                                  
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getcontactlist')
DROP PROCEDURE usp_getcontactlist
GO
CREATE PROCEDURE [dbo].[USP_GetContactList]                                                       
 @numUserCntID numeric(9)=0,                                                        
 @numDomainID numeric(9)=0,                                                        
 @tintUserRightType tinyint=0,                                                        
 @tintSortOrder tinyint=4,                                                        
 @SortChar char(1)='0',                                                       
 @FirstName varChar(100)= '',                                                      
 @LastName varchar(100)='',                                                      
 @CustName varchar(100)='',                                                      
 @CurrentPage int,                                                      
@PageSize int,                                                      
@TotRecs int output,                                                      
@columnName as Varchar(50),                                                      
@columnSortOrder as Varchar(10),                                                      
@numDivisionID as numeric(9)=0,                  
@bitPartner as bit ,                       
@inttype as numeric(9)=0                                                 
--                                                        
as                                                        
                              
                                                  
Create table #tempTable ( ID INT IDENTITY PRIMARY KEY,                                                       
      numContactID varchar(15)                                                      
 )                                                      
                                                      
                                                      
                                                      
                                                      
declare @strSql as varchar(8000)                                                
 set  @strSql='Select '                                              
      if (@tintSortOrder=5 or @tintSortOrder=6) set  @strSql= @strSql+' top 20 '                                               
                                                        
   set  @strSql= @strSql+ ' ADC.numContactId                                                  
  FROM AdditionalContactsInformation ADC                                     
  JOIN DivisionMaster DM ON ADC.numDivisionId = DM.numDivisionID                                    
  JOIN CompanyInfo C ON DM.numCompanyID = C.numCompanyId                                     
  left join ListDetails LD on LD.numListItemID=C.numCompanyRating                                               
  left join ListDetails LD1 on LD1.numListItemID= ADC.numEmpStatus'                   
  if @bitPartner=1 set @strSql=@strSql + ' left join CompanyAssociations CA on CA.numAssociateFromDivisionID=DM.numDivisionID and bitShareportal=1 and CA.bitDeleted=0                     
and CA.numDivisionID=(select numDivisionID from AdditionalContactsInformation where numContactID='+convert(varchar(15),@numUserCntID)+ ')'                                       
                                        
 if @tintSortOrder= 7 set @strSql=@strSql+' join Favorites F on F.numContactid=ADC.numContactID '                                         
                                                        
  set  @strSql= @strSql +'                                                        
        where DM.tintCRMType<>0                         
    and DM.numDomainID  = '+convert(varchar(15),@numDomainID)+'' 
	if @inttype<>0  set @strSql=@strSql+' and ADC.numContactType  = '+convert(varchar(10),@inttype)
    if @FirstName<>'' set @strSql=@strSql+' and ADC.vcFirstName  like '''+@FirstName+'%'''                                                      
    if @LastName<>'' set @strSql=@strSql+' and ADC.vcLastName like '''+@LastName+'%'''                  
    if @CustName<>'' set @strSql=@strSql+' and c.vcCompanyName like '''+@CustName+'%'''                                                      
if @SortChar<>'0' set @strSql=@strSql + ' And ADC.vcFirstName like '''+@SortChar+'%'''                                                               
if @tintUserRightType=1 set @strSql=@strSql + ' AND (ADC.numRecOwner = '+convert(varchar(15),@numUserCntID)+ ')' + case when @bitPartner=1 then ' or (CA.bitShareportal=1 and  CA.bitDeleted=0)' else '' end                                                  
   
    
else if @tintUserRightType=2 set @strSql=@strSql + ' AND (DM.numTerID in (select numTerritoryID from  UserTerritory where numUserCntID= '+convert(varchar(15),@numUserCntID)+' ) or DM.numTerID=0) '                                                       
if @numDivisionID <> 0  set @strSql=@strSql + ' And DM.numDivisionID ='+ convert(varchar(10),@numDivisionID)+'   ORDER BY ' + @columnName +' '+ @columnSortOrder                                                   
                                                                
if @tintSortOrder=2  set @strSql=@strSql + ' AND (ADC.numRecOwner = '+convert(varchar(15),@numUserCntID)+ ')'                                                          
else if @tintSortOrder=3  set @strSql=@strSql + '  AND ADC.numContactType=92  '                                              
else if @tintSortOrder=4  set @strSql=@strSql + ' AND ADC.bintCreatedDate > '''+convert(varchar(20),dateadd(day,-7,getutcdate()))+''''                                            
else if @tintSortOrder=5  set @strSql=@strSql + ' and ADC.numCreatedby='+convert(varchar(15),@numUserCntID)+ ' ORDER BY ADC.bintCreateddate desc '                                              
else if @tintSortOrder=6  set @strSql=@strSql + ' and ADC.numModifiedby='+convert(varchar(15),@numUserCntID)+ ' ORDER BY ADC.bintmodifieddate desc '                                                      
else if @tintSortOrder=7  set @strSql=@strSql + ' and F.numUserCntID='+convert(varchar(15),@numUserCntID)                                                 
if @tintSortOrder=1  set @strSql=@strSql + ' ORDER BY ' + @columnName +' '+ @columnSortOrder                                                      
else if @tintSortOrder=2  set @strSql=@strSql + ' ORDER BY ' + @columnName +' '+ @columnSortOrder                                                              
else if @tintSortOrder=3  set @strSql=@strSql + ' ORDER BY ' + @columnName +' '+ @columnSortOrder                                                          
else if @tintSortOrder=4  set @strSql=@strSql + ' ORDER BY ' + @columnName +' '+ @columnSortOrder                                                      
else if (@tintSortOrder=5 and @columnName!='ADC.bintcreateddate')  set @strSql='select * from ('+@strSql+')X order by '+ @columnName +' '+ @columnSortOrder                                                   
else if (@tintSortOrder=6 and @columnName!='ADC.bintcreateddate')  set @strSql='select * from ('+@strSql+')X order by '+ @columnName +' '+ @columnSortOrder                                         
else if @tintSortOrder=7  set @strSql=@strSql + ' and cType=''C'' ORDER BY ' + @columnName +' '+ @columnSortOrder                                                     
                               
Print @strSql          
insert into #tempTable(numContactID)                                                      
exec(@strSql)                                                       
                                                      
  declare @firstRec as integer                                                      
  declare @lastRec as integer                                                      
 set @firstRec= (@CurrentPage-1) * @PageSize                                                      
 set @lastRec= (@CurrentPage*@PageSize+1)                                                       
set @TotRecs=(select count(*) from #tempTable)                                                                         
Select ADC.numContactId AS numContactID,                                                         
              ADC.vcFirstName + ' ' + ADC.vcLastName AS Contact,                  
              C.vcCompanyName+', '+isnull(dbo.fn_GetListItemName(numCompanyType),'-') AS Company,                                                         
              case when ADC.numPhone<>'' then + ADC.numPhone +case when ADC.numPhoneExtension<>'' then ' - ' + ADC.numPhoneExtension else '' end  else '' end as [Telephone],                                                                                  
 
    
       
        
          
                                                                            
              ADC.vcEmail,                                                         
              LD.vcData  AS Rating,                                                         
              LD1.vcData AS Status,                               
     C.numCompanyID,                                                        
     DM.tintCRMType,                                                        
     DM.numTerID,                                                        
     DM.numDivisionID,                                                      
     ADC.numRecowner                                                      
  FROM AdditionalContactsInformation ADC                                     
  JOIN DivisionMaster DM ON ADC.numDivisionId = DM.numDivisionID                                     
  join CompanyInfo C ON DM.numCompanyID = C.numCompanyId                            
  left join ListDetails LD on LD.numListItemID=C.numCompanyRating                                               
  left join ListDetails LD1 on LD1.numListItemID= ADC.numEmpStatus                           
  join #tempTable T on T.numContactId=ADC.numContactId                                                 
   WHERE ID > @firstRec and ID < @lastRec order by ID                                     
                                                    
drop table #tempTable
GO
