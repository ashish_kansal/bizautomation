/****** Object:  StoredProcedure [dbo].[USP_AccountList]    Script Date: 07/26/2008 16:14:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--- Created By Anoop Jayaraj                                            
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_accountlist')
DROP PROCEDURE usp_accountlist
GO
CREATE PROCEDURE [dbo].[USP_AccountList]                                              
@CRMType numeric,                                              
@numUserCntID numeric,                                              
@tintUserRightType tinyint,                                              
@tintSortOrder numeric=4,                                                                  
@SortChar char(1)='0',                        
@numDomainID as numeric(9)=0,                                              
@FirstName varChar(100)= '',                                              
@LastName varChar(100)= '',                                              
@CustName varChar(100)= '',                                            
@CurrentPage int,                                            
@PageSize int,                                            
@TotRecs int output,                                            
@columnName as Varchar(50),                                            
@columnSortOrder as Varchar(10)    ,    
@numProfile as numeric   ,  
@bitPartner as bit                                
as                                            
                                            
----Create a Temporary table to hold data                                            
--Create table #tempTable ( ID INT IDENTITY PRIMARY KEY,                                                      
-- numDivisionID numeric(9)                                         
-- )                                            
--                                            
	declare @firstRec as integer                                            
	declare @lastRec as integer                                            
	set @firstRec= (@CurrentPage-1) * @PageSize                                            
	set @lastRec= (@CurrentPage*@PageSize+1)  
                                         
declare @strSql as varchar(5000)                                            
set @strSql='With tblAccount AS (SELECT '                                            
if @tintSortOrder=7 or @tintSortOrder=8  set @strSql=@strSql + ' top 20 '                                            
set @strSql=@strSql+'  ROW_NUMBER() OVER (ORDER BY '+@columnName+' '+ @columnSortOrder+') AS RowNumber,                                             
     DM.numDivisionID                                              
    FROM  CompanyInfo CMP                                            
    join DivisionMaster DM on DM.numCompanyID=CMP.numCompanyID'                                    
                                    
if @tintSortOrder= 9 set @strSql=@strSql+' join Favorites F on F.numContactid=DM.numDivisionID '                                    
                                    
set @strSql=@strSql+ '                                                
    join AdditionalContactsInformation ADC on ADC.numDivisionID=DM.numDivisionID                                            
    left join ListDetails LD on LD.numListItemID=CMP.numCompanyRating                                             
    left join ListDetails LD1 on LD1.numListItemID= DM.numFollowUpStatus            
    left join  AdditionalContactsInformation AD1 on AD1.numContactID=DM.numAssignedTo                                            
  WHERE  ISNULL(ADC.bitPrimaryContact,0)=1                                           
    AND (DM.bitPublicFlag=0 OR(DM.bitPublicFlag=1 and DM.numRecOwner='+convert(varchar(15),@numUserCntID)+'))                                            
    AND DM.tintCRMType= '+ convert(varchar(2),@CRMType)+'                            
    AND DM.numDomainID= '+ convert(varchar(15),@numDomainID)+''                      
    if @FirstName<>'' set  @strSql=@strSql+'and ADC.vcFirstName  like '''+@FirstName+'%'''                                             
    if @LastName<>'' set  @strSql=@strSql+'and ADC.vcLastName like '''+@LastName+'%'''                                            
    if @CustName<>'' set  @strSql=@strSql+'and CMP.vcCompanyName like '''+@CustName+'%'''                                            
if @SortChar<>'0' set @strSql=@strSql + ' And CMP.vcCompanyName like '''+@SortChar+'%'''    
  
if @bitPartner = 1 set @strSql=@strSql+'and (DM.numAssignedTo='+convert(varchar(15),@numUserCntID)+   
           ' or DM.numCreatedBy='+convert(varchar(15),@numUserCntID)+ ') '  
                                       
if @tintUserRightType=1 set @strSql=@strSql + ' AND (DM.numRecOwner = '+convert(varchar(15),@numUserCntID)+ ' or DM.numAssignedTo='+convert(varchar(15),@numUserCntID)+ ')'                                                                
else if @tintUserRightType=2 set @strSql=@strSql + ' AND (DM.numTerID in (select numTerritoryID from  UserTerritory where numUserCntID= '+convert(varchar(15),@numUserCntID)+' ) or DM.numTerID=0 or DM.numAssignedTo='+convert(varchar(15),@numUserCntID)+ ')'
  
    
if @numProfile  <> 0 set @strSQl=@strSql+' and cmp.vcProfile = '+convert(varchar(15),@numProfile)     
                                              
                                             
if @tintSortOrder=1  set @strSql=@strSql + ' AND DM.numStatusID=2 '                                            
else if @tintSortOrder=2  set @strSql=@strSql + ' AND DM.numStatusID=3 '                                                      
else if @tintSortOrder=3  set @strSql=@strSql + ' AND (DM.numRecOwner = '+convert(varchar(15),@numUserCntID)+ ' or DM.numAssignedTo='+convert(varchar(15),@numUserCntID)+ ')'                                        
--else if @tintSortOrder=5  set @strSql=@strSql + ' order by numCompanyRating desc '                  
else if @tintSortOrder=6  set @strSql=@strSql + ' AND ADC.bintCreatedDate > '''+ convert(varchar(30),dateadd(day,-7,getutcdate())) +''''                                            
else if @tintSortOrder=7  set @strSql=@strSql + ' and DM.numCreatedby='+convert(varchar(15),@numUserCntID)
--+ ' ORDER BY DM.bintCreateddate desc '                                      
else if @tintSortOrder=8  set @strSql=@strSql + ' and DM.numModifiedby='+convert(varchar(15),@numUserCntID)
--+ ' ORDER BY DM.bintmodifieddate desc '                                                
                                            
--if @tintSortOrder=1  set @strSql=@strSql + ' ORDER BY ' + @columnName +' '+ @columnSortOrder                                 
--else if @tintSortOrder=2  set @strSql=@strSql + ' ORDER BY ' + @columnName +' '+ @columnSortOrder                                                    
--else if @tintSortOrder=3  set @strSql=@strSql + ' ORDER BY ' + @columnName +' '+ @columnSortOrder                                                
--else if @tintSortOrder=4  set @strSql=@strSql + ' ORDER BY ' + @columnName +' '+ @columnSortOrder                                            
--else if @tintSortOrder=5  set @strSql=@strSql + ' , ' + @columnName +' '+ @columnSortOrder                                            
--else if @tintSortOrder=6  set @strSql=@strSql + ' ORDER BY ' + @columnName +' '+ @columnSortOrder                                            
--else 
if (@tintSortOrder=7 and @columnName!='DM.bintcreateddate')  
	set @strSql='select * from ('+@strSql+')X order by '+ @columnName +' '+ @columnSortOrder                                           
else if (@tintSortOrder=8 and @columnName!='DM.bintcreateddate')  
	set @strSql='select * from ('+@strSql+')X order by '+ @columnName +' '+ @columnSortOrder                                     
else if @tintSortOrder=9  set @strSql=@strSql + 
	' and F.numUserCntID='+convert(varchar(15),@numUserCntID)+ ' and cType=''O'' ORDER BY ' + @columnName +' '+ @columnSortOrder                                             
                                   
--insert into #tempTable (                                            
--   numDivisionID                                         
--  )                                            
                                      
set @strSql=@strSql +')         
                                  
                                  
SELECT RowNumber,CMP.vcCompanyName + '' - <I>'' + DM.vcDivisionName + ''</I>'' as CompanyName,                                               
     DM.numTerID,                                              
     ADC.vcFirstName + '' '' + ADC.vcLastName as PrimaryContact,                                           
     dbo.fn_GetListItemName(CMP.numNoOfEmployeesId) as Employees,                                              
        case when ADC.numPhone<>'''' then + ADC.numPhone +case when ADC.numPhoneExtension<>'''' then ''-'' + ADC.numPhoneExtension else '''' end           
     else '''' end as [Phone],
     ADC.vcEmail As vcEmail,                                              
     CMP.numCompanyID AS numCompanyID,                                              
     DM.numDivisionID As numDivisionID,                                              
     ADC.numContactID AS numContactID,                                                    
     LD.vcData as vcRating,                                
     LD1.vcData as Follow,                               
   DM.numCreatedby AS numCreatedby,DM.numRecOwner,                    
     dbo.fn_GetContactName(numAssignedTo)+''/''+dbo.fn_GetContactName(numAssignedBy) AssignedToBy                                            
    FROM  CompanyInfo CMP                                            
    join DivisionMaster DM on DM.numCompanyID=CMP.numCompanyID                                  
    join AdditionalContactsInformation ADC on ADC.numDivisionID=DM.numDivisionID                                            
    left join ListDetails LD on LD.numListItemID=CMP.numCompanyRating                             
    left join ListDetails LD1 on LD1.numListItemID=DM.numFollowUpStatus                                                  
    join tblAccount T on T.numDivisionID=DM.numDivisionID                                             
  WHERE ISNULL(ADC.bitPrimaryContact,0)=1 and RowNumber > '+convert(varchar(10),@firstRec)+' and RowNumber < '+convert(varchar(10),@lastRec)+'                   
 union select 0,null,count(*),null,null,null,null,null,null,null,null,null,null,null,null from tblAccount order by RowNumber' 


print( @strSql)
exec( @strSql)                                  
                                           
--drop table #tempTable
GO
