/****** Object:  StoredProcedure [dbo].[USP_HowOftenPagesVisited]    Script Date: 07/26/2008 16:18:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj                  
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_howoftenpagesvisited')
DROP PROCEDURE usp_howoftenpagesvisited
GO
CREATE PROCEDURE [dbo].[USP_HowOftenPagesVisited]                  
@From datetime,                  
@To datetime,                  
@CurrentPage int,                          
@PageSize int,                          
@TotRecs int output,                          
@columnName as Varchar(50),                          
@columnSortOrder as Varchar(10) ,             
@numDomainID as numeric(9),  
@ClientOffsetTime as integer                  
as                          
                          
--Create a Temporary table to hold data                          
Create table #tempTable                   
    ( ID INT IDENTITY PRIMARY KEY,                           
      vcPageName varchar(200),                  
      Times VARCHAR(10),                       
 numPageId varchar(10)          
      )                          
                  
                     
declare @strSql as varchar(5000)                          
set @strSql='select     
isnull(vcPageName,''-'')    
,count(*)  as [Times], 0 as numPageId  from TrackingVisitorsHDR                    
join  TrackingVisitorsDTL DTL                  
on numTrackingID=numTracVisitorsHDRID                
--join PageMasterWebAnlys P                
--on P.numPageID=DTL.numPageID                
where  numDomainId='+convert(varchar(10),@numDomainID)+' and (dtCreated between '''+convert(varchar(50),dateadd(minute,@ClientOffsetTime,@From))+''' and '''
+convert(varchar(50),dateadd(minute,@ClientOffsetTime,@To))+''')    
group by isnull(vcPageName,''-'') '                    
--set  @strSql=@strSql +' ORDER BY ' + @columnName +' '+ @columnSortOrder                        
print @strSql                    
insert into #tempTable (vcPageName,Times,numPageId )                          
exec( @strSql)                          
  declare @firstRec as integer                          
  declare @lastRec as integer                          
 set @firstRec= (@CurrentPage-1) * @PageSize                          
     set @lastRec= (@CurrentPage*@PageSize+1)                          
select * from #tempTable where ID > @firstRec and ID < @lastRec                          
set @TotRecs=(select count(*) from #tempTable)  


                        
drop table #tempTable
GO
