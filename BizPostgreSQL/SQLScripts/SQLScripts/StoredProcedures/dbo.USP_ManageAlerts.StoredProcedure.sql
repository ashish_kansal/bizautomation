/****** Object:  StoredProcedure [dbo].[USP_ManageAlerts]    Script Date: 07/26/2008 16:19:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created By Anoop Jayarj        
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_managealerts')
DROP PROCEDURE usp_managealerts
GO
CREATE PROCEDURE [dbo].[USP_ManageAlerts]          
@numAlertID as numeric(9)=0,          
@numAlertDTLid as numeric(9)=0,          
@numEmailTemplate as numeric(9)=0,          
@tintCCManager as tinyint,          
@numDaysAfterDue as numeric(9)=0,          
@monAmount DECIMAL(20,5),          
@numBizDocs as numeric(9)=0,          
@tintAlertOn as tinyint,      
@numDepartMentID as numeric(9)=0,      
@numNoOfItems as numeric(9)=0,    
@numAge as numeric(9)=0  ,
@numDomainId as numeric(9)     =0,
@numEmailCampaignId as numeric(9) = 0
as  
  
if @numAlertDTLid <>0  
begin  
	if ((select count(*) from AlertDomainDtl where numAlertDTLid=@numAlertDTLid and numDomainId=@numDomainId) >0)
		begin
			update AlertDomainDtl set           
			 numEmailTemplate=@numEmailTemplate,       
			      
			 numDaysAfterDue=@numDaysAfterDue,          
			 monAmount=@monAmount,          
			 numBizDocs=@numBizDocs,          
			 tintAlertOn=@tintAlertOn ,      
			 numDepartment=@numDepartMentID,      
			 numNoOfItems=@numNoOfItems,    
			 numAge=@numAge,     
			 numEmailCampaignId=@numEmailCampaignId
			 where numAlertDTLid=@numAlertDTLid and numDomainId=@numDomainId
		end
	else
		begin
			 insert into AlertDomainDtl          
				 (numEmailTemplate,
				 numDaysAfterDue,          
				 monAmount,          
				 numBizDocs,          
				 tintAlertOn,      
				 numDepartment,      
				 numNoOfItems,    
				 numAge,    
				 numAlertDTLid,
				 numDomainId,
				 numEmailCampaignId)  
				 values
				(@numEmailTemplate,  
				@numDaysAfterDue,
				@monAmount,
				@numBizDocs,  
				@tintAlertOn,
				@numDepartMentID,
				@numNoOfItems,
				@numAge,
				@numAlertDTLid,
				@numDomainId,
				@numEmailCampaignId) 
		end
end  
  
  
if @numAlertDTLid =0  
begin  
    
 insert into AlertDTL ( numAlertID) values(@numAlertID)
 SELECT  
   @numAlertDTLid = SCOPE_IDENTITY();   
		 insert into AlertDomainDtl          
				 (numEmailTemplate,
				 numDaysAfterDue,          
				 monAmount,          
				 numBizDocs,          
				 tintAlertOn,      
				 numDepartment,      
				 numNoOfItems,    
				 numAge,    
				 numAlertDTLid,
				 numDomainId)  
				 values
				(@numEmailTemplate,  
				@numDaysAfterDue,
				@monAmount,
				@numBizDocs,  
				@tintAlertOn,
				@numDepartMentID,
				@numNoOfItems,
				@numAge,
				@numAlertDTLid,
				@numDomainId) 
  
end
GO
