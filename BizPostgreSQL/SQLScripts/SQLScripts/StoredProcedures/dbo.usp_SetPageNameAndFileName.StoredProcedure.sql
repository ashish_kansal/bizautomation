/****** Object:  StoredProcedure [dbo].[usp_SetPageNameAndFileName]    Script Date: 07/26/2008 16:21:17 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_setpagenameandfilename')
DROP PROCEDURE usp_setpagenameandfilename
GO
CREATE PROCEDURE [dbo].[usp_SetPageNameAndFileName]
	@vcFileName VARCHAR(50),
	@vcPageDesc VARCHAR(100),
	@numModuleID NUMERIC(9),
	@numPageID NUMERIC(9) = 0,
	@bitIsViewApplicable BIT,
	@bitIsAddApplicable BIT,
	@bitIsUpdateApplicable BIT,
	@bitIsDeleteApplicable BIT,
	@bitIsExportApplicable BIT   
--
AS
	BEGIN
		IF @numPageID > 0 
			BEGIN
				UPDATE PageMaster SET vcFileName = @vcFileName, 
						vcPageDesc = @vcPageDesc, 
						bitIsViewApplicable = @bitIsViewApplicable,
						bitIsAddApplicable = @bitIsAddApplicable,
						bitIsUpdateApplicable = @bitIsUpdateApplicable,
						bitIsDeleteApplicable = @bitIsDeleteApplicable,
						bitIsExportApplicable = @bitIsExportApplicable					
				WHERE numModuleID = @numModuleID
				AND numPageID = @numPageID
			END
		ELSE
			BEGIN
				SELECT @numPageID = ISNULL(MAX(numPageID), 0) + 1 FROM PageMaster WHERE numModuleID = @numModuleID
				INSERT INTO PageMaster (numPageID, numModuleID, vcFileName, vcPageDesc,
								bitIsViewApplicable, bitIsAddApplicable, bitIsUpdateApplicable, bitIsDeleteApplicable,
								bitIsExportApplicable)
				VALUES(@numPageID, @numModuleID, @vcFileName, @vcPageDesc,
								@bitIsViewApplicable, @bitIsAddApplicable, @bitIsUpdateApplicable, @bitIsDeleteApplicable,
								@bitIsExportApplicable)
			END
	END
GO
