/****** Object:  StoredProcedure [dbo].[USP_GetAOI]    Script Date: 07/26/2008 16:16:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj  
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getaoi')
DROP PROCEDURE usp_getaoi
GO
CREATE PROCEDURE [dbo].[USP_GetAOI]    
@numContactID as numeric(9),     
@numDomainID as numeric(9)    
as    
    
    
select numListItemID as numAOIId,vcData as vcAOIName,dbo.GetAOIStatus(numListItemID,@numContactID) as [Status] from ListDetails    
where  numDomainID=@numDomainID  and numListID=74
GO
