/****** Object:  StoredProcedure [dbo].[USP_GetChartOfAcntDetails]    Script Date: 07/26/2008 16:16:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created By Siva    
    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getchartofacntdetails')
DROP PROCEDURE usp_getchartofacntdetails
GO
CREATE PROCEDURE [dbo].[USP_GetChartOfAcntDetails]    
(@numAcntTypeId as numeric(9)=0,    
 @numDomainId as numeric(9)=0)    
As    
Begin    
     Declare @numParntAcntId as numeric(9)
               Set @numParntAcntId=(Select numAccountId From Chart_Of_Accounts Where numParntAcntTypeId is null and numAcntTypeID is null and numDomainId = @numDomainId) --and numAccountId = 1 
        
  Select numAccountId as numAccountId From Chart_Of_Accounts Where numAcntTypeID=@numAcntTypeId And numParntAcntTypeId=@numParntAcntId   
End
GO
