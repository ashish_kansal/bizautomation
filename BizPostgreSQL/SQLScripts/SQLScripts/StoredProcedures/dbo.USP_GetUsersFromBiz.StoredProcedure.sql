/****** Object:  StoredProcedure [dbo].[USP_GetUsersFromBiz]    Script Date: 07/26/2008 16:18:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getusersfrombiz')
DROP PROCEDURE usp_getusersfrombiz
GO
CREATE PROCEDURE [dbo].[USP_GetUsersFromBiz]    
@bitActivateFlag as tinyint    
as    
select vcUserName,vcMailNickName from usermaster where bitActivateFlag=@bitActivateFlag
GO
