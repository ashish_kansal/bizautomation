/****** Object:  StoredProcedure [dbo].[usp_GetTemplateDetails]    Script Date: 07/26/2008 16:18:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_gettemplatedetails')
DROP PROCEDURE usp_gettemplatedetails
GO
CREATE PROCEDURE [dbo].[usp_GetTemplateDetails]  
@numDomainID numeric,  
@numUserID numeric,  
@numPageID numeric  
--
as  
select numTemplateID, vcTemplateName, bintDateTimeUpload, vcFileName, numPageID from TemplateDocuments where numDomainid = @numDomainID and numPageID = @numPageID and bitStatus = 0
GO
