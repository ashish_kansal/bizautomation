/****** Object:  StoredProcedure [dbo].[USP_GetCashCreditCardMainDetailsId]    Script Date: 07/26/2008 16:16:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created By Siva          
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getcashcreditcardmaindetailsid')
DROP PROCEDURE usp_getcashcreditcardmaindetailsid
GO
CREATE PROCEDURE [dbo].[USP_GetCashCreditCardMainDetailsId]          
@numCashCreditCardId as numeric(9)=0,          
@numDomainId as numeric(9)=0          
As          
Begin           
 Declare @numJournalId as numeric(9)        
 Set @numJournalId=0        
         
 Select @numJournalId=numJournal_Id From General_Journal_Header Where numCashCreditCardId=@numCashCreditCardId And numDomainId=@numDomainId           
         
 Select numTransactionId From General_Journal_Details         
 Where numJournalId=@numJournalId And bitMainCashCredit=1 And numDomainId=@numDomainId          
End
GO
