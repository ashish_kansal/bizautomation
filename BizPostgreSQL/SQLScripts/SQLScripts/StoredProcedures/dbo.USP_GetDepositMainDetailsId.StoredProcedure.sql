/****** Object:  StoredProcedure [dbo].[USP_GetDepositMainDetailsId]    Script Date: 07/26/2008 16:17:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created By Siva          
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getdepositmaindetailsid')
DROP PROCEDURE usp_getdepositmaindetailsid
GO
CREATE PROCEDURE [dbo].[USP_GetDepositMainDetailsId]          
@numDepositId as numeric(9)=0,          
@numDomainId as numeric(9)=0          
As          
Begin           
Declare @numJournalId as numeric(9)        
Set @numJournalId=0        
        
Select @numJournalId=numJournal_Id From General_Journal_Header Where numDepositId=@numDepositId      
      
        
Select numTransactionId From General_Journal_Details         
Where numJournalId=@numJournalId And bitMainDeposit=1 And numDomainId=@numDomainId          
End
GO
