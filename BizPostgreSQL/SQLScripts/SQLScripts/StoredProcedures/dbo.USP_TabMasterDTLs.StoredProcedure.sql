/****** Object:  StoredProcedure [dbo].[USP_TabMasterDTLs]    Script Date: 07/28/2009 22:57:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj  
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_tabmasterdtls')
DROP PROCEDURE usp_tabmasterdtls
GO
CREATE PROCEDURE [dbo].[USP_TabMasterDTLs]
    @numDomainID AS NUMERIC(9) = 0,
    @tintTabType AS INT
AS 
    SELECT  numTabId,
            CASE WHEN bitFixed = 1
                 THEN CASE WHEN EXISTS ( SELECT numTabName
                                         FROM   TabDefault
                                         WHERE  numDomainid = @numDomainID
                                                AND numTabId = TM.numTabId
                                                AND tintTabType = @tintTabType )
                           THEN ( SELECT    numTabName
                                  FROM      TabDefault
                                  WHERE     numDomainid = @numDomainID
                                            AND numTabId = TM.numTabId
                                            AND tintTabType = @tintTabType
                                )
                           ELSE numTabName
                      END
                 ELSE numTabName
            END numTabname,
            Remarks,
            tintTabType,
            vcURL,
            bitFixed,
            numDomainID
    FROM    TabMaster TM
    WHERE   ( numDomainID = @numDomainID
              OR bitFixed = 1
            )
            AND tintTabType = @tintTabType