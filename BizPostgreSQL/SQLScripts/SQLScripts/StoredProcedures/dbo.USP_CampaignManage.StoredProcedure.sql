/****** Object:  StoredProcedure [dbo].[USP_CampaignManage]    Script Date: 08/14/2009 00:18:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created By Anoop Jayaraj      
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_campaignmanage')
DROP PROCEDURE usp_campaignmanage
GO
CREATE PROCEDURE [dbo].[USP_CampaignManage]        
@numCampaignId as numeric(9)=0 output,        
@vcCampaignName as VARCHAR(200)='',
@numCampaignStatus as numeric(9)=0,        
@numCampaignType as numeric(9)=0,        
@intLaunchDate as datetime,
@intEndDate as datetime,        
@numRegion as numeric(9)=0,        
@numNoSent as numeric(9)=0,        
@monCampaignCost as DECIMAL(20,5)=0,
@bitIsOnline AS BIT,
@bitIsMonthly AS BIT,
@vcCampaignCode	varchar(500),
@numDomainID as numeric(9)=0,
@numUserCntID as numeric(9)=0,
@IsDuplicate BIT=0 OUTPUT,
@bitActive BIT =1 
as        

/*Landing page duplication validation*/
IF @bitIsOnline =1 AND (SELECT COUNT(*) FROM [CampaignMaster] WHERE numCampaignID <> @numCampaignId AND [numDomainID]=@numDomainID AND [bitIsOnline]=1 AND [vcCampaignCode]=@vcCampaignCode)>0
BEGIN
	SET @IsDuplicate = 1
	RETURN
END

if @numCampaignId=0        
begin        
         
insert into CampaignMaster        
(vcCampaignName,        
numCampaignStatus,        
numCampaignType,        
intLaunchDate,        
intEndDate,        
numRegion,        
numNoSent,        
monCampaignCost,
bitIsOnline,
bitIsMonthly,
vcCampaignCode,        
numDomainID,        
numCreatedBy,        
bintCreatedDate,        
numModifiedBy,        
bintModifiedDate,
bitActive        
)        
values        
(        
@vcCampaignName,        
@numCampaignStatus,        
@numCampaignType,        
@intLaunchDate,        
@intEndDate,        
@numRegion,        
@numNoSent,        
@monCampaignCost,  
@bitIsOnline,
@bitIsMonthly,
@vcCampaignCode,      
@numDomainID,        
@numUserCntID,        
getutcdate(),        
@numUserCntID,        
getutcdate(),
@bitActive
)        
      
select @numCampaignId=@@identity      
end        
if @numCampaignId>0        
begin        
update CampaignMaster set        
vcCampaignName=@vcCampaignName,        
numCampaignStatus=@numCampaignStatus,        
numCampaignType=@numCampaignType,        
intLaunchDate=@intLaunchDate,        
intEndDate=@intEndDate,        
numRegion=@numRegion,        
numNoSent=@numNoSent,        
monCampaignCost=@monCampaignCost,        
bitIsOnline=@bitIsOnline,
bitIsMonthly=@bitIsMonthly,
vcCampaignCode=@vcCampaignCode,
numDomainID=@numDomainID,        
numModifiedBy=@numUserCntID,        
bintModifiedDate=getutcdate(),
bitActive=@bitActive
where @numCampaignId=numCampaignId        

/*Droped this idea for time being*/
/*In order to get accurate details of Leads created using perticular Online Camaign 
We are going to auto update Campaign dropdown in account detail based on Landing page mapped-by chintan*/
--IF @bitIsOnline = 1  AND LEN(@vcCampaignCode)>2
--BEGIN
--	UPDATE [DivisionMaster] SET [numCampaignID] = @numCampaign --Pk of ListDetails table not CampaignMaster
--	WHERE [numDivisionID] IN (SELECT DISTINCT numDivisionID FROM [TrackingVisitorsHDR] WHERE LOWER([vcOrginalURL])= LOWER(@vcCampaignCode))
--END

end
