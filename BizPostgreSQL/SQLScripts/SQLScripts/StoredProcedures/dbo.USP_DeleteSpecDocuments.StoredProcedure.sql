/****** Object:  StoredProcedure [dbo].[USP_DeleteSpecDocuments]    Script Date: 07/26/2008 16:15:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_deletespecdocuments')
DROP PROCEDURE usp_deletespecdocuments
GO
CREATE PROCEDURE [dbo].[USP_DeleteSpecDocuments]
@numSpecificDocID as numeric(9)=0
as

DELETE FROM dbo.DocumentWorkflow WHERE numDocID = @numSpecificDocID

delete from SpecificDocuments where numSpecificDocID=@numSpecificDocID
GO
