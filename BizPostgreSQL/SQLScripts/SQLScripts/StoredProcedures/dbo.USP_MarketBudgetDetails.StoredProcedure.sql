/****** Object:  StoredProcedure [dbo].[USP_MarketBudgetDetails]    Script Date: 07/26/2008 16:20:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created By siva                                
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_marketbudgetdetails')
DROP PROCEDURE usp_marketbudgetdetails
GO
CREATE PROCEDURE [dbo].[USP_MarketBudgetDetails]                                
(@numDomainId as numeric(9)=0,            
 @numMarketId as numeric(9)=0,            
 @intType as int=0                             
 )                                
As                                
Begin                                
 Declare @numItemGroupId as numeric(9)                                
 Declare @vcItemGroup as varchar(150)                                
 Declare @Count as numeric(9)                   
 Declare @lstrSQL as varchar(8000)               
 Declare @lstr as varchar(8000)                   
 Declare @numMonth as tinyint                 
 Set @numMonth=1                  
 Set @lstrSQL=''               
 Set @lstr=''               
 Declare @Date as datetime   
 Declare @dtFiscalStDate as datetime                          
 Declare @dtFiscalEndDate as datetime                
 Set @Date = dateadd(year,@intType,getutcdate())                 
                
 While @numMonth <=12                          
  Begin                          
                     
     Set @dtFiscalStDate =dbo.GetFiscalStartDate(dbo.GetFiscalyear(@Date,@numDomainId),@numDomainId)                       
     Set @dtFiscalStDate=dateadd(month,@numMonth-1,@dtFiscalStDate)                    
     Set @dtFiscalEndDate =  dateadd(day,-1,dateadd(year,1,dbo.GetFiscalStartDate(dbo.GetFiscalyear(@Date,@numDomainId),@numDomainId)))                 
     print @dtFiscalStDate                
     Set @lstrSQL=@lstrSQL+' '+  'dbo.fn_GetMarketBudgetMonthDetail('+convert(varchar(10),@numMarketId)+','+Convert(varchar(2),month(@dtFiscalStDate))+','+Convert(varchar(4),year(@dtFiscalStDate))+','+Convert(varchar(10),@numDomainId)+',LD.numListItemID)'
     
       + ' as ''' +Convert(varchar(2),month(@dtFiscalStDate))+ '~'+Convert(varchar(4),year(@dtFiscalStDate))+''','                
     Set @numMonth=@numMonth+1                          
  End     
 Set @dtFiscalStDate =dbo.GetFiscalStartDate(dbo.GetFiscalyear(@Date,@numDomainId),@numDomainId)                       
              
    Set @lstrSQL = @lstrSQL +  
 --  ' dbo.fn_GetMarketBudgetMonthTotalAmt('+Convert(varchar(10),@numMarketId)+','''+convert(varchar(500),@Date)+''',LD.numListItemID,'+convert(varchar(10),@numDomainId)+') as Total' +           
 -- ',dbo.fn_GetMarketBudgetComments('+Convert(varchar(10),@numMarketId)+','+Convert(varchar(10),@numDomainId)+',LD.numListItemID) as  Comments'           
 ' (Select Sum(MBD.monAmount)  From MarketBudgetMaster MBM    
 inner join MarketBudgetDetails MBD on MBM.numMarketBudgetId=MBD.numMarketId    
 Where MBM.numDomainId='+Convert(varchar(10),@numDomainId)+' And MBD.numMarketId='+Convert(varchar(10),@numMarketId)+' And MBD.numListItemID=LD.numListItemID  And (((MBD.tintMonth between Month('''+Convert(varchar(100),@dtFiscalStDate)+''') and 12) and MBD
.intYear=Year('''+Convert(varchar(100),@dtFiscalStDate)+  
 ''')) Or ((MBD.tintMonth between 1 and month('''+Convert(varchar(100),@dtFiscalEndDate)+''')) and MBD.intYear=Year('''+Convert(varchar(100),@dtFiscalEndDate)+'''))))  as Total  
 , ( Select top 1 isnull(MBD.vcComments,'''') From MarketBudgetMaster MBM      
 inner join MarketBudgetDetails MBD on MBM.numMarketBudgetId=MBD.numMarketId          
 Where MBM.numDomainId='+Convert(varchar(10),@numDomainId)+' And MBD.numMarketId='+Convert(varchar(10),@numMarketId) +' And MBD.numListItemID=LD.numListItemID    
 And MBD.tintMonth=Month('''+Convert(varchar(100),@dtFiscalEndDate)+''') And MBD.intYear=Year('''+Convert(varchar(100),@dtFiscalEndDate)+'''))as  Comments'    
 print @lstrSQL                        
 Set @lstr  ='Select LD.numListItemID as numListItemID,LD.vcData as vcData,LD.numDomainID as numDomainID,' +Convert(varchar(8000),@lstrSQL)          
            +' From ListDetails LD Where LD.numListId=22 And LD.numDomainID='+Convert(varchar(10),@numDomainId)                  
                 
 Print(@lstr)              
 Exec (@lstr)              
End
GO
