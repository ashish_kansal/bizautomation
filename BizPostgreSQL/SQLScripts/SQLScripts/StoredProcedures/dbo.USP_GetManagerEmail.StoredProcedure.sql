/****** Object:  StoredProcedure [dbo].[USP_GetManagerEmail]    Script Date: 07/26/2008 16:17:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--- Created By Anoop Jayaraj
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getmanageremail')
DROP PROCEDURE usp_getmanageremail
GO
CREATE PROCEDURE [dbo].[USP_GetManagerEmail]
@byteMode as tinyint,
@numUserID as numeric(9)=0,
@numContactID as numeric(9)=0
as
if @byteMode=0 --- If Userid Is Available
begin
	select isnull(vcEmail,'') from AdditionalContactsInformation where
	numContactID=(select numManagerID from AdditionalContactsInformation Addc
	join UserMaster U
	on U.numUserDetailId=Addc.numContactId
	where numUserID=@numUserID)
end
if @byteMode=1  --- If Conatct ID is Available
begin
	select isnull(vcEmail,'') from AdditionalContactsInformation where
	numContactID=(select numManagerID from AdditionalContactsInformation where numContactID=@numContactID)
end
GO
