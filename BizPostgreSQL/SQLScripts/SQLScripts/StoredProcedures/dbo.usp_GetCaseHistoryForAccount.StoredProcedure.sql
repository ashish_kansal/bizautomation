/****** Object:  StoredProcedure [dbo].[usp_GetCaseHistoryForAccount]    Script Date: 07/26/2008 16:16:27 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getcasehistoryforaccount')
DROP PROCEDURE usp_getcasehistoryforaccount
GO
CREATE PROCEDURE [dbo].[usp_GetCaseHistoryForAccount]
	--@numCreatedBy numeric(9)=0,
	@numCompanyId numeric(9),
	--@numOrderby numeric=0,
	@numDomainID numeric(9)=0
	--@numUserId numeric(9)=0,
	--@tintUserRightType tinyint    --For Security
--
AS
	--Get teh Territory ID for the User.
	--DECLARE @numTerritoryID numeric
	--SELECT @numTerritoryID=numTerritoryID FROM UserMaster WHERE numUserId=@numUserId

--IF @numCreatedby=0 and @tintUserRightType=1 
-- Means that sorting is not passed, but has got rights to view only his records
	
	BEGIN
		SELECT 
			AdditionalContactsInformation.vcFirstName,
			AdditionalContactsInformation.vcLastName,
			CompanyInfo.vcCompanyName,
			Cases.numCaseID,
			Cases.vcCaseNumber,
			Cases.intTargetResolveDate,
			Cases.numCreatedBy,
			Cases.textSubject,
			Cases.numPriority,
			Cases.numCreatedBy,
			Cases.textDesc,
			(Select vcData FROM ListDetails WHERE numListItemID=Cases.numStatus)as vcStatus,
			CompanyInfo.numCompanyID,
			DivisionMaster.numDivisionID,
			Divisionmaster.numTerId,
			AdditionalContactsInformation.numContactID 

	FROM 
	AdditionalContactsInformation INNER JOIN Cases			
	ON AdditionalContactsInformation.numContactId = 
	Cases.numContactId INNER JOIN DivisionMaster 
	ON AdditionalContactsInformation.numDivisionId = DivisionMaster.numDivisionID 
	INNER JOIN CompanyInfo ON DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
	Where 
		Cases.numStatus=136 AND --to avoid the closed cases
		AdditionalContactsInformation.numDomainID=@numDomainID
	--AND Cases.numCreatedBy = @numUserId 
	and CompanyInfo.numCompanyId = @numCompanyId
	--COALESCE(@vccompanyname,CompanyInfo.vcCompanyName) 
	--ORDER BY
	--CASE
		--WHEN @numOrderby=3 THEN AdditionalContactsInformation.numContactId
		--WHEN @numOrderby=1 THEN Cases.vcPriority 
		--WHEN @numOrderby=2 THEN intTargetResolveDate
		--WHEN @numOrderby=0 THEN intTargetResolveDate
	END  --End of the Case
GO
