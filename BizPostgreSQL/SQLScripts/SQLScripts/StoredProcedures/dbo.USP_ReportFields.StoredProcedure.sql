/****** Object:  StoredProcedure [dbo].[USP_ReportFields]    Script Date: 07/26/2008 16:20:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created bY Anoop jayaraj
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_reportfields')
DROP PROCEDURE usp_reportfields
GO
CREATE PROCEDURE [dbo].[USP_ReportFields]
@byteMode as tinyint
as
if @byteMode=0
select vcFieldName,numRepFieldID from ReportFields

if @byteMode=1
select vcFieldName,numRepFieldID from ReportFields where tintSelected=1
GO
