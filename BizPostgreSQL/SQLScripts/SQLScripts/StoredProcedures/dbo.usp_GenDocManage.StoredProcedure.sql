/****** Object:  StoredProcedure [dbo].[usp_GenDocManage]    Script Date: 07/26/2008 16:16:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created By Anoop Jayaraj        
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_gendocmanage')
DROP PROCEDURE usp_gendocmanage
GO
CREATE PROCEDURE [dbo].[usp_GenDocManage]          
(          
 @numDomainID as numeric(9)=null,          
 @vcDocDesc as text='',          
 @VcFileName as varchar(100)='',          
 @vcDocName as varchar(100)='',          
 @numDocCategory as numeric(9)=null,          
 @vcfiletype as varchar(10)='',          
 @numDocStatus as numeric(9)=null,          
 @numUserCntID as numeric(9)=null,          
 @numGenDocId as numeric(9)=0 output,          
 @cUrlType as varchar(1)='',        
 @vcSubject as varchar(500)='' ,  
 @vcDocumentSection VARCHAR(10),
 @numRecID as numeric(9)=0,
 @tintDocumentType TINYINT=1,-- 1 = generic, 2= specific document
 @numModuleID NUMERIC =0,
 @vcContactPosition VARCHAR(200)='',
 @numFormFieldGroupId NUMERIC(18,0) = 0
)          
as          

          
if @numGenDocId=0           
begin          

IF @numModuleID=0 SET @numModuleID=null

insert into GenericDocuments          
          
 (          
 numDomainID,          
 vcDocDesc,          
 VcFileName,          
 vcDocName,          
 numDocCategory,          
 vcfiletype,          
 numDocStatus,          
 numCreatedBy,          
 bintCreatedDate,          
 numModifiedBy,          
 bintModifiedDate,          
 cUrlType,        
 vcSubject,
 vcDocumentSection,
 numRecID,
 tintDocumentType,
 numModuleID,vcContactPosition,numFormFieldGroupId
 )          
values          
 (          
 @numDomainID,          
 @vcDocDesc,          
 @VcFileName,          
 @vcDocName,          
 @numDocCategory,          
 @vcfiletype,          
 @numDocStatus,          
 @numUserCntID,           
 getutcdate(),          
 @numUserCntID,          
 getutcdate(),          
 @cUrlType,        
 @vcSubject,
 @vcDocumentSection,
 @numRecID,
 @tintDocumentType,
 @numModuleID,@vcContactPosition,@numFormFieldGroupId
 )          
          
set @numGenDocId=@@identity           
          
end          
          
else          
          
begin          
          
 IF @numModuleID = 0  SET @numModuleID = NULL;
 
 update  GenericDocuments          
           
 set           
  vcDocDesc=@vcDocDesc,          
  VcFileName=@VcFileName,          
  vcDocName=@vcDocName,          
  numDocCategory=@numDocCategory,          
  vcfiletype=@vcfiletype,
  numDocStatus=@numDocStatus,          
  numModifiedBy=@numUserCntID,          
  bintModifiedDate=getutcdate(),          
  cUrlType=@cUrlType,        
  vcSubject=@vcSubject,
  vcDocumentSection = @vcDocumentSection,--Bug ID 1149
  numModuleID= @numModuleID,vcContactPosition=@vcContactPosition
           
 where numGenericDocID = @numGenDocId          
          
end
GO
