/****** Object:  StoredProcedure [dbo].[USP_OppDeleteItem]    Script Date: 07/26/2008 16:20:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_oppdeleteitem')
DROP PROCEDURE usp_oppdeleteitem
GO
CREATE PROCEDURE [dbo].[USP_OppDeleteItem]   
@OppItemCode as numeric(9)  
as  
 declare @OppId as numeric(9)
 declare @TotalAmount as DECIMAL(20,5)
select @OppId=numOppId from OpportunityItems where numoppitemtCode=@OppItemCode
delete from OpportunityItems where numoppitemtCode=@OppItemCode 

select @TotalAmount=sum(monTotAmount)from OpportunityItems where numOppId=@OppId
	update OpportunityMaster set  monPamount=@TotalAmount
	where numOppId=@OppId
GO
