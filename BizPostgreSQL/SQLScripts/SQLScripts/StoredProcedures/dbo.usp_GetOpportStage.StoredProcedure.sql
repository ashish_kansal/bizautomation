/****** Object:  StoredProcedure [dbo].[usp_GetOpportStage]    Script Date: 07/26/2008 16:17:59 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getopportstage')
DROP PROCEDURE usp_getopportstage
GO
CREATE PROCEDURE [dbo].[usp_GetOpportStage]
	@numOppID  numeric(9)=0

AS
BEGIN
	SELECT *  FROM StageOpportunity
	WHERE numOppId=@numOppID
END
GO
