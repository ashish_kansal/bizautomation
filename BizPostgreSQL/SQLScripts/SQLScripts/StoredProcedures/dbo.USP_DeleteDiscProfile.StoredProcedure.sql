/****** Object:  StoredProcedure [dbo].[USP_DeleteDiscProfile]    Script Date: 07/26/2008 16:15:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_deletediscprofile')
DROP PROCEDURE usp_deletediscprofile
GO
CREATE PROCEDURE [dbo].[USP_DeleteDiscProfile]
@numDiscProfileID as numeric(9)=0
as


delete from ItemDiscountProfile where numDiscProfileID=@numDiscProfileID
GO
