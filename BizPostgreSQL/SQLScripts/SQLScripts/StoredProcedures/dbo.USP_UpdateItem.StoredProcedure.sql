/****** Object:  StoredProcedure [dbo].[USP_UpdateItem]    Script Date: 07/26/2008 16:21:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_updateitem')
DROP PROCEDURE usp_updateitem
GO
CREATE PROCEDURE [dbo].[USP_UpdateItem]
@OppItemCode as numeric(9),
@ItemCode as numeric(9),
@Units as numeric(9)
as
declare @monPrice as DECIMAL(20,5)
declare @TotalmonPrice as DECIMAL(20,5)     
select @TotalmonPrice=convert(DECIMAL(20,5),dbo.GetPrice(@ItemCode,@Units)) 
select @monPrice=convert(DECIMAL(20,5),dbo.GetPrice(@ItemCode,1))

 update OpportunityItems set numItemCode=@ItemCode,
                             numUnitHour=@Units,
			     monPrice=isnull(@monPrice,0),
			     monTotAmount=isnull(@TotalmonPrice,0)
where numoppitemtCode=@OppItemCode

declare @OppId as numeric(9)
declare @TotalAmount as DECIMAL(20,5)
select @OppId=numOppId  from OpportunityItems where numoppitemtCode=@OppItemCode
select @TotalAmount=sum(monTotAmount)from OpportunityItems where numOppId=@OppId
	update OpportunityMaster set  monPamount=@TotalAmount
	where numOppId=@OppId
GO
