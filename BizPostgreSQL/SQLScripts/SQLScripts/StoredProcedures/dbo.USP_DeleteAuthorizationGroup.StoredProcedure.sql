/****** Object:  StoredProcedure [dbo].[USP_DeleteAuthorizationGroup]    Script Date: 07/26/2008 16:15:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_deleteauthorizationgroup')
DROP PROCEDURE usp_deleteauthorizationgroup
GO
CREATE PROCEDURE [dbo].[USP_DeleteAuthorizationGroup]
@numDomainID as numeric(9)=0,
@numGroupID as numeric(9)=0
as
delete from AuthenticationGroupMaster where numGroupID=@numGroupID and numDomainID=@numDomainID
GO
