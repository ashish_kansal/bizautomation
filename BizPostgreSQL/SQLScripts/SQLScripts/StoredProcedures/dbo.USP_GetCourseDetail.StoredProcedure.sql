/****** Object:  StoredProcedure [dbo].[USP_GetCourseDetail]    Script Date: 07/26/2008 16:17:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj  
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getcoursedetail')
DROP PROCEDURE usp_getcoursedetail
GO
CREATE PROCEDURE [dbo].[USP_GetCourseDetail]  
@numWareHouseItemID as numeric(9)=0,  
@ClassNum as varchar(10)=''  
as  
select top 1 numItemCode,WI.numWareHouseItemID,vcClassNum,vcModelID as CourseCode,vcItemName as Title,vcWarehouse,    
convert(varchar(11),cast(dbo.GetCustFldValue(150,5,I.numItemCode) as datetime),100)+' - '+ convert(varchar(11),cast(dbo.GetCustFldValue(151,5,I.numItemCode) as datetime),100) as CourseDate,    
CLASSAT,CITY,STATE,monWListPrice,txtItemDesc,facname,PREREQUISITS,convert(decimal,length) as length,OBJECTIVE,AUDIENCE_TEXT,Chapter,SubChapter,
Address1 +' '+Address2 + ' '+City+' '+ [State] as [Address] , Topic
from Item I      
left join RemarketDTL DTL       
on DTL.numItemID=I.numItemCode      
left Join Remarket R      
on R.CLASSNUM=DTL.vcClassNum      
Left Join WareHouseItems WI      
on WI.numItemID=I.numItemCode      
Left Join  Warehouses W      
on W.numWareHouseID=WI.numWareHouseID  
where WI.numWareHouseItemID=@numWareHouseItemID
GO

