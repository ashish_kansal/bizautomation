/****** Object:  StoredProcedure [dbo].[usp_GetDefaultRuleId]    Script Date: 07/26/2008 16:17:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created By: Debasish Tapan Nag                        
--Purpose: Get a Rule Id  of Default Auto Routing rule for Leads    
--Created Date: 08/03/2005                           
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getdefaultruleid')
DROP PROCEDURE usp_getdefaultruleid
GO
CREATE PROCEDURE [dbo].[usp_GetDefaultRuleId]    
 @numDomainId Numeric(9)  
AS                
--This procedure will return all the Default Routing Rules details      
BEGIN          
   DECLARE @numRoutId Numeric(9)
   SET @numRoutId = 0
   SELECT TOP 1 @numRoutId = numRoutID      
   FROM RoutingLeads    
   where bitDefault = 1  
   AND numDomainId = @numDomainId  

   If @@ROWCOUNT = 0
     SELECT numRoutId = 0
   ELSE 
     SELECT numRoutId = @numRoutId
END
GO
