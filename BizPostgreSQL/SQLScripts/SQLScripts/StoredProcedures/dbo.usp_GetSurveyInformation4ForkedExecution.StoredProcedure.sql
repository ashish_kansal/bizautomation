/****** Object:  StoredProcedure [dbo].[usp_GetSurveyInformation4ForkedExecution]    Script Date: 07/26/2008 16:18:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created By: Debasish Nag          
--Created On: 09/21/2005          
--Purpose: Returns the Question and Answers from the Other Survey as a part of WorkFlow Rules
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getsurveyinformation4forkedexecution')
DROP PROCEDURE usp_getsurveyinformation4forkedexecution
GO
CREATE PROCEDURE [dbo].[usp_GetSurveyInformation4ForkedExecution]  
 @numSurId Numeric,          
 @vcQuestionIDs NVarchar(50)
AS            
--This procedure will return the selected survey Questions and Answer Information for the selected survey          
BEGIN           
 PRINT @numSurId
 PRINT @vcQuestionIDs
 DECLARE @SqlSelect NVarchar(4000)

 SET @SqlSelect = 'SELECT  numSurID, numQID, vcQuestion, tIntAnsType, intNumOfAns        
  FROM SurveyQuestionMaster            
  WHERE numSurId = ' + Convert(NVarchar, @numSurId) + '
  AND Convert(NVarchar(3), numQID) IN ('+@vcQuestionIDs+')  
  ORDER BY numQID'
 EXEC (@SqlSelect)
        
 SET @SqlSelect = 'SELECT numSurID, numQID, numAnsID, vcAnsLabel, 0 as boolSurveyRuleAttached, 0 as boolDeleted
  FROM SurveyAnsMaster            
  WHERE  numSurId = ' + Convert(NVarchar, @numSurId) + '
  AND Convert(NVarchar(3), numQID) IN ('+@vcQuestionIDs+')  
  ORDER BY numQID, numAnsID'     
 EXEC (@SqlSelect)
    
END
GO
