/****** Object:  StoredProcedure [dbo].[USP_GetItems]    Script Date: 07/26/2008 16:17:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
---created by anooop jayaraj    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getitems')
DROP PROCEDURE usp_getitems
GO
CREATE PROCEDURE [dbo].[USP_GetItems]    
@numDomainID as numeric(9)    
as    
    
select numItemCode,vcItemName,monListPrice as Price,vcSKU as SKU,vcModelID as [Model ID] from Item where numDomainID=@numDomainID order by  vcItemName
GO
