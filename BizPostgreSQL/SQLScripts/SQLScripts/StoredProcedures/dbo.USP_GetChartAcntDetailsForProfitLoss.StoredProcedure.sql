set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go


-- Created by Siva                                                      
-- [USP_GetChartAcntDetailsForProfitLoss] 72,'2008-09-28 14:22:53.937','2009-09-28 14:22:53.937'
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getchartacntdetailsforprofitloss')
DROP PROCEDURE usp_getchartacntdetailsforprofitloss
GO
CREATE PROCEDURE [dbo].[USP_GetChartAcntDetailsForProfitLoss]
@numDomainId as numeric(9),                                          
@dtFromDate as datetime,                                        
@dtToDate as DATETIME,
@ClientTimeZoneOffset INT, --Added by Chintan to enable calculation of date according to client machine                                                   
@numAccountClass AS NUMERIC(9)=0
As                                                        
Begin 
DECLARE @CURRENTPL DECIMAL(20,5) ;
DECLARE @PLOPENING DECIMAL(20,5);
DECLARE @PLCHARTID NUMERIC(8)
--DECLARE @numFinYear INT;
DECLARE @TotalIncome DECIMAL(20,5);
DECLARE @TotalExpense DECIMAL(20,5);
DECLARE @TotalCOGS DECIMAL(20,5);
DECLARE @dtFinYearFrom datetime;


--set @numFinYear= (SELECT numFinYearId FROM FINANCIALYEAR WHERE dtPeriodFrom <= @dtFromDate AND  
--dtPeriodTo >=@dtFromDate AND numDomainId= @numDomainId);

--set @dtFinYearFrom = (SELECT dtPeriodFrom FROM FINANCIALYEAR WHERE dtPeriodFrom <= @dtFromDate AND  
--dtPeriodTo >=@dtFromDate AND numDomainId= @numDomainId);
SELECT * INTO #view_journal FROM view_journal WHERE numDomainID=@numDomainId AND (numAccountClass=@numAccountClass OR @numAccountClass=0);

SELECT @dtFinYearFrom=MIN(datEntry_Date) FROM #view_journal WHERE numDomainID=@numDomainId


SET @dtFromDate = DateAdd(minute, @ClientTimeZoneOffset, @dtFromDate);
SET @dtToDate = DateAdd(minute, @ClientTimeZoneOffset, @dtToDate);


--select * from view_journal where numDomainid=72

CREATE TABLE #PLSummary (numAccountId numeric(9),vcAccountName varchar(250),
numParntAcntTypeID numeric(9),vcAccountDescription varchar(250),
vcAccountCode varchar(50) COLLATE Database_Default,Opening DECIMAL(20,5),Debit DECIMAL(20,5),Credit DECIMAL(20,5),bitIsSubAccount Bit);

INSERT INTO  #PLSummary
SELECT COA.numAccountId,vcAccountName,numParntAcntTypeID,vcAccountDescription,vcAccountCode,

/*isnull((SELECT SUM(isnull(monOpening,0)) from CHARTACCOUNTOPENING CAO WHERE
numFinYearId=@numFinYear and numDomainID=@numDomainId 
AND CAO.numAccountId IN (SELECT numAccountId FROM dbo.Chart_Of_Accounts WHERE numDomainId = @numDomainId AND vcAccountCode like COA.vcAccountCode + '%' ) ),0) 

+*/

/*ISNULL((SELECT sum(Debit-Credit) FROM #view_journal VJ
WHERE VJ.numDomainId=@numDomainId AND
	VJ.COAvcAccountCode like COA.vcAccountCode + '%' AND
	datEntry_Date BETWEEN @dtFinYearFrom AND  DATEADD(Minute,-1,@dtFromDate) ),0)*/ 0 AS OPENING,

ISNULL((SELECT sum(Debit) FROM #view_journal VJ
WHERE VJ.numDomainId=@numDomainId AND
	VJ.COAvcAccountCode like COA.vcAccountCode + '%' /*VJ.numAccountId=COA.numAccountId*/ AND
	datEntry_Date BETWEEN  @dtFromDate AND @dtToDate ),0) as DEBIT,

ISNULL((SELECT sum(Credit) FROM #view_journal VJ
WHERE VJ.numDomainId=@numDomainId AND
	VJ.COAvcAccountCode like COA.vcAccountCode + '%' /*VJ.numAccountId=COA.numAccountId*/ AND
	datEntry_Date BETWEEN  @dtFromDate AND @dtToDate ),0) as CREDIT,
	ISNULL(COA.bitIsSubAccount,0)
FROM Chart_of_Accounts COA
WHERE COA.numDomainId=@numDomainId AND COA.bitActive = 1 AND 
      (COA.vcAccountCode LIKE '0103%' OR
       COA.vcAccountCode LIKE '0104%' OR
       COA.vcAccountCode LIKE '0106%')  ;

CREATE TABLE #PLOutPut (numAccountId numeric(9),vcAccountName varchar(250),
numParntAcntTypeID numeric(9),vcAccountDescription varchar(250),
vcAccountCode varchar(50) COLLATE Database_Default,Opening DECIMAL(20,5),Debit DECIMAL(20,5),Credit DECIMAL(20,5));

INSERT INTO #PLOutPut
SELECT ATD.numAccountTypeID,ATD.vcAccountType,ATD.numParentID, '',ATD.vcAccountCode,
 ISNULL(SUM(Opening),0) as Opening,
ISNUlL(Sum(Debit),0) as Debit,ISNULL(Sum(Credit),0) as Credit
FROM 
 AccountTypeDetail ATD RIGHT OUTER JOIN 
#PLSummary PL ON
PL.vcAccountCode LIKE ATD.vcAccountCode + '%'
AND ATD.numDomainId=@numDomainId AND
(ATD.vcAccountCode LIKE '0103%' OR
       ATD.vcAccountCode LIKE '0104%' OR
       ATD.vcAccountCode LIKE '0106%')
WHERE 
PL.bitIsSubAccount=0
GROUP BY 
ATD.numAccountTypeID,ATD.vcAccountCode,ATD.vcAccountType,ATD.numParentID;



ALTER TABLE #PLSummary
DROP COLUMN bitIsSubAccount

--SELECT * FROM #PLOutPut
--------------------------------------------------------
-- GETTING P&L VALUE
DECLARE  @CurrentPL_COA DECIMAL(20,5)
SET @CURRENTPL =0;	
SET @PLOPENING=0;


SELECT @PLCHARTID=COA.numAccountId FROM Chart_of_Accounts COA WHERE numDomainID=@numDomainId and
bitProfitLoss=1;

SELECT @CurrentPL_COA =  ISNULL(sum(Credit),0)-ISNULL(sum(Debit),0) FROM #view_journal VJ WHERE numDomainID=@numDomainID
AND numAccountId=@PLCHARTID AND datEntry_Date between  @dtFromDate AND @dtToDate;

SELECT  @TotalIncome= ISNULL(sum(Opening),0) + ISNULL(sum(Credit),0) -ISNULL(sum(Debit),0) FROM
#PLOutPut P WHERE 
vcAccountCode IN ('0103')

SELECT  @TotalExpense=ISNULL(sum(Opening),0)+ ISNULL(sum(Credit),0) -ISNULL(sum(Debit),0) FROM
#PLOutPut P WHERE 
vcAccountCode IN ('0104')

SELECT  @TotalCOGS= ISNULL(sum(Opening),0) + ISNULL(sum(Credit),0) -ISNULL(sum(Debit),0) FROM
#PLOutPut P WHERE 
vcAccountCode IN ('0106')

PRINT 'TotalIncome=									' + CAST(@TotalIncome AS VARCHAR(20))
PRINT 'TotalExpense=									'+ CAST(@TotalExpense AS VARCHAR(20))
PRINT 'TotalCOGS=										'+ CAST(@TotalCOGS AS VARCHAR(20))

SELECT @CURRENTPL = @CurrentPL_COA + @TotalIncome + @TotalExpense + @TotalCOGS 
PRINT 'Current Profit/Loss = (Income - expense - cogs)= ' + CAST( @CURRENTPL AS VARCHAR(20))
--PRINT @CURRENTPL


--SELECT @CURRENTPL = @CURRENTPL - ISNULL(SUM(Opening),0)+ISNULL(sum(Debit),0)-ISNULL(sum(Credit),0) FROM
--#PLOutPut P WHERE 
--vcAccountCode IN ('0104')

set @CURRENTPL=@CURRENTPL * (-1)



SELECT @PLOPENING = ISNULL(sum(Credit),0)-ISNULL(sum(Debit),0) FROM #view_journal VJ WHERE numDomainID=@numDomainID 
AND ( vcAccountCode  LIKE '0103%' or  vcAccountCode  LIKE '0104%' or  vcAccountCode  LIKE '0106%')
AND datEntry_Date <=  DATEADD(Minute,-1,@dtFromDate);

SELECT @PLOPENING = ISNULL(@PLOPENING,0) + ISNULL(sum(Credit),0)-ISNULL(sum(Debit),0) FROM #view_journal VJ WHERE numDomainID=@numDomainID
AND numAccountId=@PLCHARTID AND datEntry_Date <=  DATEADD(Minute,-1,@dtFromDate);
--PRINT @PLCHARTID
--SELECT sum(Credit-Debit) FROM #view_journal VJ
--WHERE VJ.numDomainId=@numDomainId AND
--	VJ.numAccountId=@PLCHARTID
--	AND datEntry_Date <= @dtFromDate-1
--	
--	PRINT @dtFromDate
	
/*SELECT   @PLOPENING  = 
ISNULL((SELECT sum(Credit-Debit) FROM #view_journal VJ
WHERE VJ.numDomainId=@numDomainId AND
	VJ.numAccountId=COA.numAccountId 
	AND datEntry_Date <= @dtFromDate-1 ),0)
 FROM Chart_of_Accounts COA WHERE numDomainID=@numDomainId and
bitProfitLoss=1;*/
/*SELECT @PLOPENING = ISNULL(sum(Credit),0)-ISNULL(sum(Debit),0) FROM #PLOutPut P WHERE numDomainID=@numDomainID 

AND  vcAccountCode  IN( '0103' ,'0104' ,'0106')
AND datEntry_Date <=  DATEADD(Minute,-1,@dtFromDate);

SELECT @PLOPENING = ISNULL(@PLOPENING,0) + ISNULL(sum(Credit),0)-ISNULL(sum(Debit),0) FROM #PLOutPut P WHERE numDomainID=@numDomainID
AND numAccountId=@PLCHARTID
*/
--SELECT  @CURRENTPL=@CURRENTPL + 		   
--ISNULL((SELECT sum(Credit-Debit) FROM #view_journal VJ
--WHERE VJ.numDomainId=@numDomainId AND
--	VJ.numAccountId=@PLCHARTID AND
--	datEntry_Date BETWEEN @dtFromDate AND @dtToDate),0) ;

SET @CURRENTPL=@CURRENTPL * (-1)
--SELECT @PLCHARTID,@CURRENTPL,@PLOPENING

-----------------------------------------------------------------

 CREATE TABLE #PLShow (numAccountId numeric(9),vcAccountName varchar(250),
numParntAcntTypeID numeric(9),vcAccountDescription varchar(250),
vcAccountCode varchar(50),Opening DECIMAL(20,5),Debit DECIMAL(20,5),Credit DECIMAL(20,5),
Balance varchar(50),AccountCode1  varchar(100),vcAccountName1 varchar(250),[Type] INT);

INSERT INTO #PLShow
 
SELECT *,CAST( (/*Opening +*/ Credit - Debit ) AS VARCHAR(50)) as Balance,
CASE 
 WHEN LEN(P.[vcAccountCode])>4 THEN REPLICATE('&nbsp;', LEN(P.[vcAccountCode])-4) + P.[vcAccountCode] 
 ELSE P.[vcAccountCode]
 END AS AccountCode1,
 CASE 
 WHEN LEN(P.[vcAccountCode])>4 THEN REPLICATE('&nbsp;', LEN(P.[vcAccountCode])-4) + P.[vcAccountName]
 ELSE P.[vcAccountName]
 END AS vcAccountName1, 1 AS Type
 FROM #PLSummary P
 
 UNION
 select  0,'',0,'','0103A',0,0,0, '________________' ,'','',2 
 UNION
 Select 0,'Total Income :',0,'','0103A1',0,0,0,CAST( @TotalIncome AS VARCHAR(50)) ,'','Total Income :',2
 UNION
 select  0,'',0,'','0103A2',0,0,0, '' ,'','',2 
 UNION
SELECT *,CAST( (Opening* case substring(O.[vcAccountCode],1,4) when '0103' then (-1) else 1 end)+
(Debit* case substring(O.[vcAccountCode],1,4) when '0103' then (-1) else 1 end)-
(Credit* case substring(O.[vcAccountCode],1,4) when '0103' then (-1) else 1 end)  AS VARCHAR(50)) as Balance,

CASE 
 WHEN LEN(O.[vcAccountCode])>4 THEN REPLICATE('&nbsp;', LEN(O.[vcAccountCode])-4) + O.[vcAccountCode] 
 ELSE O.[vcAccountCode]
 END AS AccountCode1,
 CASE 
 WHEN LEN(O.[vcAccountCode])>4 THEN REPLICATE('&nbsp;', LEN(O.[vcAccountCode])-4) + O.[vcAccountName]
 ELSE O.[vcAccountName]
 END AS vcAccountName1, 2 as Type
 FROM #PLOutPut O
 
 UNION
 select  0,'',0,'','0104A',0,0,0, '________________' ,'','',2 
 UNION
 Select 0,'Total Expense :',0,'','0104A1',0,0,0,CAST( @TotalExpense AS VARCHAR(50)) ,'','Total Expense :',2
 UNION
 select  0,'',0,'','0104A2',0,0,0, '' ,'','',2 
 -----------Added by chintan for COGS change
 UNION
 select  0,'',0,'','0106A',0,0,0, '________________' ,'','',2 
 UNION
 Select 0,'Total COGS :',0,'','0106A1',0,0,0,CAST( @TotalCOGS AS VARCHAR(50)) ,'','Total COGS :',2
 UNION
 select  0,'',0,'','0106A2',0,0,0, '' ,'','',2 
 -----------End of block, Added by chintan for COGS change

--COA.numAccountId,vcAccountName,numParntAcntTypeID,vcAccountDescription,vcAccountCode
UNION
Select @PLCHARTID,'Profit / (Loss) Current :',0,'','051',0,0,0, CASE WHEN @CURRENTPL <0 THEN '(' + CAST( @CURRENTPL  AS VARCHAR(50)) + ')' ELSE CAST(@CURRENTPL AS VARCHAR(50)) END  ,'','Profit / (Loss) Current :',3
UNION
select @PLCHARTID,'Profit / (Loss) Opening :',0,'','052',0,0,0,CASE WHEN @PLOPENING <0 THEN '(' + CAST(@PLOPENING AS VARCHAR(50)) + ')' ELSE CAST(@PLOPENING AS VARCHAR(50)) END  ,'','Profit / (Loss) Opening :',4
UNION
Select  @PLCHARTID, 'Net Profit/(Loss) :',0,'','053',0,0,0, CASE WHEN (@PLOPENING + @CURRENTPL) <0 THEN '(' + CAST(@PLOPENING + @CURRENTPL AS VARCHAR(50)) + ')' ELSE CAST(@PLOPENING + @CURRENTPL AS VARCHAR(50)) END  ,'','Net Profit/(Loss) :',5


select  A.numAccountId ,
        A.vcAccountName ,
        A.numParntAcntTypeID ,
        A.vcAccountDescription ,
        A.vcAccountCode ,
        A.Opening ,
        A.Debit ,
        A.Credit ,
        A.Balance ,
        A.AccountCode1 ,
        CASE WHEN LEN(A.[vcAccountCode]) > 4
                     THEN REPLICATE('&nbsp;',LEN(A.[vcAccountCode]) - 4 )+ A.[vcAccountName]
                     ELSE A.[vcAccountName]
                END AS vcAccountName1, A.vcAccountCode ,
        A.Type 
        from #PLShow A ORDER BY A.vcAccountCode ASC ;

DROP TABLE #PLOutPut;
DROP TABLE #PLSummary;
DROP TABLE #PLShow



End
-- exec USP_GetChartAcntDetailsForProfitLoss @numDomainId=171,@dtFromDate='2013-02-14 00:00:00:000',@dtToDate='2013-02-28 23:59:59:000',@ClientTimeZoneOffset=-330
