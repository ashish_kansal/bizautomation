/****** Object:  StoredProcedure [dbo].[USP_GetManager]    Script Date: 07/26/2008 16:17:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getmanager')
DROP PROCEDURE usp_getmanager
GO
CREATE PROCEDURE [dbo].[USP_GetManager]
@numManagerID as numeric(9)=0
as
select vcFirstName + ' '+ VcLastName from AdditionalContactsInformation where numContactID=@numManagerID
GO
