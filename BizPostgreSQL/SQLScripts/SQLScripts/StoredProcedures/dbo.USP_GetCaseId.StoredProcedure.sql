/****** Object:  StoredProcedure [dbo].[USP_GetCaseId]    Script Date: 07/26/2008 16:16:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getcaseid')
DROP PROCEDURE usp_getcaseid
GO
CREATE PROCEDURE [dbo].[USP_GetCaseId]

as
if exists(select  top 1 * from Cases)
select max(numcaseId)+1 from Cases
else
select 1
GO
