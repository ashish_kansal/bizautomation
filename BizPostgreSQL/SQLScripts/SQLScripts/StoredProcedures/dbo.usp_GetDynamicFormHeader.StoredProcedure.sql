/****** Object:  StoredProcedure [dbo].[usp_GetDynamicFormHeader]    Script Date: 07/26/2008 16:17:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created By: Debasish Tapan Nag                        
--Created By: 07/31/2005                        
--Purpose:    Get Dynamic Form Header Details      
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getdynamicformheader')
DROP PROCEDURE usp_getdynamicformheader
GO
CREATE PROCEDURE [dbo].[usp_GetDynamicFormHeader]         
 @numFormId numeric(9),    
 @numDomainID as numeric(9)     
AS      
BEGIN      
 SELECT vcFormName, isnull(vcAdditionalParam,'') vcAdditionalParam, isnull(numGrpId,0) as  numGrpId,isnull(ntxtHeader,'') as ntxtHeader,isnull(ntxtFooter,'') as ntxtFooter,isnull(ntxtLeft,'') as ntxtLeft,isnull(ntxtRight,'') as ntxtRight,isnull(ntxtStyle ,'') ntxtStyle 
 FROM DynamicFormMaster DM      
 left join DynamicFormMasterParam DMP on    
 DMP.numFormID=DM.numFormID  and numDomainID=@numDomainID    
 WHERE DM.numFormId = @numFormId    
END
GO
