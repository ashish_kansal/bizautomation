/****** Object:  StoredProcedure [dbo].[usp_GetContactInfo]    Script Date: 07/26/2008 16:16:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getcontactinfo')
DROP PROCEDURE usp_getcontactinfo
GO
CREATE PROCEDURE [dbo].[usp_GetContactInfo]        
 @numContactID numeric=0,        
 @numDivID numeric = 0,                
 @tintUserRightType tinyint=0,        
 @bitShowAll bit=0           
--        
        
AS        
BEGIN        
       
        
       
  IF @numDivID = 0         
  BEGIN        
   SELECT vcLastName, vcFirstName,vcFirstName+' '+vcLastName as [Name],vcGivenName, vcPosition, vcEmail, vcDepartment,        
    numPhone, numPhoneExtension, vcFax, numContacttype,   vcData ,      
    AC.numDivisionId, bintDOB, txtNotes, AC.numCreatedBy,        
   DM.numTerID, --New        
   DM.numCreatedBy, --New
   vcFirstName+' '+vcLastName + CASE WHEN lst.vcData IS NOT NULL THEN ' (' + lst.vcData + ')' ELSE '' end as ContactType,
    AC.bitPrimaryContact,
   dbo.GetListIemName(ISNULL(vcDepartment,0)) AS vcDepartmentName, 
   dbo.GetListIemName(ISNULL(vcPosition,0)) AS vcPositionName,
   dbo.GetListIemName(ISNULL(numContactType,0)) AS vcContactTypeName 
   FROM AdditionalContactsInformation AC join         
   divisionmaster DM on DM.numDivisionID=AC.numDivisionID       
 left join ListDetails lst on ac.numContacttype=lst.numListItemID      
   WHERE numContactID=@numContactID        
       
  END        
  ELSE        
  BEGIN        
   SELECT numContactId,vcFirstName+' '+vcLastName as [Name],vcLastName, vcFirstName,vcGivenName, vcPosition, vcEmail, vcDepartment,        
    numPhone, numPhoneExtension, vcFax, numContacttype,   vcData ,       
     bintDOB, txtNotes, AC.numCreatedBy,        
   DM.numTerID, --New        
   DM.numCreatedBy, --New  
   vcFirstName+' '+vcLastName + CASE WHEN lst.vcData IS NOT NULL THEN ' (' + lst.vcData + ')' ELSE '' end as ContactType,
    AC.bitPrimaryContact,
   dbo.GetListIemName(ISNULL(vcDepartment,0)) AS vcDepartmentName, 
   dbo.GetListIemName(ISNULL(vcPosition,0)) AS vcPositionName,
   dbo.GetListIemName(ISNULL(numContactType,0)) AS vcContactTypeName     
   FROM AdditionalContactsInformation AC join         
   divisionmaster DM on DM.numDivisionID=AC.numDivisionID       
 left join ListDetails lst on ac.numContacttype=lst.numListItemID      
   WHERE AC.numDivisionID = @numDivID         
  END        
 END
GO