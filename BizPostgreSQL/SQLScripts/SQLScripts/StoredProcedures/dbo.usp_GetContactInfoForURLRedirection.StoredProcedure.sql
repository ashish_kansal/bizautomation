/****** Object:  StoredProcedure [dbo].[usp_GetContactInfoForURLRedirection]    Script Date: 07/26/2008 16:16:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created By: Debasish Nag          
--Purpose: To show the Survey Respondents          
  
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getcontactinfoforurlredirection')
DROP PROCEDURE usp_getcontactinfoforurlredirection
GO
CREATE PROCEDURE [dbo].[usp_GetContactInfoForURLRedirection]      
 @numEntityId Numeric,    
 @vcEntityType Nvarchar(4)    
AS                            
BEGIN     
  IF @vcEntityType = 'Cont'     
   SELECT c.numContactId, d.numDivisionId, d.tintCRMType, d.numCompanyId, d.numGrpId      
   FROM AdditionalContactsInformation c INNER JOIN DivisionMaster d ON c.numDivisionId = d.numDivisionId
   WHERE c.numContactId = @numEntityId 
  ELSE IF  @vcEntityType = 'Div'     
   SELECT TOP 1 c.numContactId, d.numDivisionId, d.tintCRMType, d.numCompanyId, d.numGrpId      
   FROM AdditionalContactsInformation c INNER JOIN DivisionMaster d ON c.numDivisionId = d.numDivisionId  
   WHERE     
	c.numDivisionId = @numEntityId     
END
GO
