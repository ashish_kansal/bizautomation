/****** Object:  StoredProcedure [dbo].[USP_GetChartAcntDetailsForGL]    Script Date: 07/26/2008 16:16:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- Created by Siva                                        
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getchartacntdetailsforgl')
DROP PROCEDURE usp_getchartacntdetailsforgl
GO
CREATE PROCEDURE [dbo].[USP_GetChartAcntDetailsForGL]                                       
@numDomainId as numeric(9),                            
@dtFromDate as datetime,                          
@dtToDate as datetime                                       
As                                          
Begin                                          
 Declare @strSQL as varchar(2000)                          
 Declare @i as integer                      
 Set @strSQL=''                         
 ----  Select @i=count(*) From General_Journal_Header GJH Where GJH.numDomainId= @numDomainId And GJH.datEntry_Date>='' + convert(varchar(300),@dtFromDate) +''                  
 ----    And  GJH.datEntry_Date<='' + convert(varchar(300),@dtToDate) +''           
        
  Declare @numParntAcntId as numeric(9)      
  Set @numParntAcntId=(Select numAccountId From Chart_Of_Accounts Where numParntAcntTypeId is null and numAcntTypeID is null and numDomainId = @numDomainId) --and numAccountId = 1       
                      
  Select @i=count(*) From General_Journal_Header GJH Where --GJH.datEntry_Date>='' + convert(varchar(300),@dtFromDate) +''  And                                 
  GJH.numDomainId=@numDomainId And GJH.datEntry_Date<='' + convert(varchar(300),@dtToDate) +''               
  If @i=0                    
  Begin                    
   Set @strSQL = ' Select distinct COA.numAccountId as numAccountId,COA.numParntAcntId as numParntAcntId,vcCatgyName as CategoryName,COA.numOpeningBal as OpeningBalance                              
       From Chart_Of_Accounts COA                              
       Left outer join General_Journal_Details GJD on COA.numAccountId=GJD.numChartAcntId                              
       Where  COA.numDomainId=' + Convert(varchar(5),@numDomainId) + 'And COA.numAccountId <>'+convert(varchar(10),@numParntAcntId)+' And COA.numOpeningBal is not null '--And CoA.numParntAcntId ='+convert(varchar(10),@numParntAcntId)                         
 
   Set @strSQL=@strSQL+ '  And COA.dtOpeningDate>=''' + convert(varchar(300),@dtFromDate) +''' And  COA.dtOpeningDate<=''' + convert(varchar(300),@dtToDate) +''''                             
  End                     
  Else                    
   Begin                    
    Set @strSQL = ' Select distinct COA.numAccountId as numAccountId,COA.numParntAcntId as numParntAcntId,COA.vcCatgyName as CategoryName,COA.numOpeningBal as OpeningBalance                              
        From Chart_Of_Accounts COA                              
        Left outer join General_Journal_Details GJD on COA.numAccountId=GJD.numChartAcntId                              
        Where COA.numDomainId=' + Convert(varchar(5),@numDomainId) + 'And COA.numAccountId <>'+convert(varchar(10),@numParntAcntId)+' And COA.numOpeningBal is not null '--And CoA.numParntAcntId ='+convert(varchar(10),@numParntAcntId)                         
   End                    
  print @strSQL                          
  exec (@strSQL)                          
 End
GO
