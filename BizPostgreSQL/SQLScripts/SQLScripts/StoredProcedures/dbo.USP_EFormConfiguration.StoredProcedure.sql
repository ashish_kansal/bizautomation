/****** Object:  StoredProcedure [dbo].[USP_EFormConfiguration]    Script Date: 07/26/2008 16:15:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_eformconfiguration')
DROP PROCEDURE usp_eformconfiguration
GO
CREATE PROCEDURE [dbo].[USP_EFormConfiguration]
@numSpecDocID as numeric(9)=0,
@strSpecDocDetails as text 
as
 DECLARE @hDoc1 int  
 EXEC sp_xml_preparedocument @hDoc1 OUTPUT, @strSpecDocDetails  
  delete from EformConfiguration where numSpecDocID=@numSpecDocID
 insert into EformConfiguration  
  (numSpecDocID,vcColumnName,vcEFormFld,VcFldType)  
   
 select @numSpecDocID,X.* from(SELECT *FROM OPENXML (@hDoc1,'/NewDataSet/Table',2)  
 WITH (ColumnName varchar(100),  
  EformField varchar(100),
  FieldType varchar(1)))X  
  
  
 EXEC sp_xml_removedocument @hDoc1
GO
