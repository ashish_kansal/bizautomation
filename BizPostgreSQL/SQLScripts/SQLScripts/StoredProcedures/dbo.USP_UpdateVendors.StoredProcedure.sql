/****** Object:  StoredProcedure [dbo].[USP_UpdateVendors]    Script Date: 07/26/2008 16:21:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj      
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_updatevendors')
DROP PROCEDURE usp_updatevendors
GO
CREATE PROCEDURE [dbo].[USP_UpdateVendors]          
(          
@strFieldList as text='',    
@Itemcode as numeric(9)=0     
)          
as          
          

IF convert(varchar(10),@strFieldList) <>''
BEGIN
	 declare @hDoc  int          
	 EXEC sp_xml_preparedocument @hDoc OUTPUT, @strFieldList          
	 update Vendor          
	  set vcPartNo= X.PartNo,monCost=X.monCost,[intMinQty]=X.intMinQty,vcNotes=X.vcNotes           
	                
	 from(SELECT  * FROM OPENXML (@hDoc,'/NewDataSet/Table',2)          
	 WITH ( VendorID numeric(9),          
	  PartNo varchar(100),      
	  monCost DECIMAL(20,5),          
	  intMinQty int,
	  vcNotes VARCHAR(300)
	  ))X          
	 where numVendorID=X.VendorID  and  numItemCode=  @Itemcode      
	              
	 EXEC sp_xml_removedocument @hDoc
 
 END
GO
