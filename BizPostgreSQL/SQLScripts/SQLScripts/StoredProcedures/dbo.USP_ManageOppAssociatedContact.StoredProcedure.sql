/****** Object:  StoredProcedure [dbo].[USP_ManageOppAssociatedContact]    Script Date: 07/26/2008 16:19:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_manageoppassociatedcontact')
DROP PROCEDURE usp_manageoppassociatedcontact
GO
CREATE PROCEDURE [dbo].[USP_ManageOppAssociatedContact]
@numOppId as numeric(9)=0,
@numDomainID as numeric(9)=0,
@numContactID as numeric(9)=0,
@numRole as numeric(9)=0,
@bitPartner as bit,
@byteMode as tinyint,
@bitSubscribedEmailAlert AS bit
as
if  @byteMode=0
begin
 insert into OpportunityContact(numOppId, numContactId, numRole, bitPartner,bitSubscribedEmailAlert)
 values (@numOppId,@numContactID,@numRole,@bitPartner,@bitSubscribedEmailAlert)
end
else if @byteMode=1
begin 
	delete from OpportunityContact where numOppID=@numOppId and numContactId=@numContactID
end






 SELECT  a.numContactId,vcCompanyName+', '+isnull(Lst.vcData,'-') as Company,           
  a.vcFirstname +' '+ a.vcLastName as [Name],            
  case when a.numPhone<>'' then + a.numPhone +case when a.numPhoneExtension<>'' then ' - ' + a.numPhoneExtension else '' end  else '' end as [Phone],
 a.vcEmail as Email,            
  b.vcData as ContactRole,            
  opp.numRole as ContRoleId,           
  convert(integer,isnull(bitPartner,0)) as bitPartner,           
  a.numcontacttype,CASE WHEN opp.bitSubscribedEmailAlert=1 THEN 'Yes' ELSE 'No' END AS SubscribedEmailAlert,D.numDivisionID AS numDivisionID FROM OpportunityContact opp            
  join additionalContactsinformation a on            
  a.numContactId=opp.numContactId          
  join DivisionMaster D          
  on a.numDivisionID =D.numDivisionID          
  join CompanyInfo C          
  on D.numCompanyID=C.numCompanyID           
  left join listdetails b             
  on b.numlistitemid=opp.numRole          
  left join listdetails Lst          
  on Lst.numlistitemid=C.numCompanyType          
  WHERE opp.numOppId=@numOppId and A.numDomainID=@numDomainId
GO
