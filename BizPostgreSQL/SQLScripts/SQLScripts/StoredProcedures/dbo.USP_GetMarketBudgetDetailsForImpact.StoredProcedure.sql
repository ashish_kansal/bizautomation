/****** Object:  StoredProcedure [dbo].[USP_GetMarketBudgetDetailsForImpact]    Script Date: 07/26/2008 16:17:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created By Siva                  
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getmarketbudgetdetailsforimpact')
DROP PROCEDURE usp_getmarketbudgetdetailsforimpact
GO
CREATE PROCEDURE [dbo].[USP_GetMarketBudgetDetailsForImpact]                
(@numDomainId as numeric(9)=0,          
@intFiscalYear as int=0)                
As                
Begin                
 Select distinct Convert(varchar(10),LD.numListItemID)+'~'+Convert(varchar(10),numMarketBudgetId) as numListItemID,LD.vcData as  vcData From MarketBudgetMaster MBM              
inner join MarketBudgetDetails MBD on MBM.numMarketBudgetId=MBD.numMarketId           
inner join  ListDetails LD on MBD.numListItemID=LD.numListItemID And LD.numListID=22       
 Where MBM.numDomainId=@numDomainId    --  MBM.intFiscalYear=@intFiscalYear And         
End
GO
