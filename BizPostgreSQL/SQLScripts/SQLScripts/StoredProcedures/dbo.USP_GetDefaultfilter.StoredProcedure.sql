/****** Object:  StoredProcedure [dbo].[USP_GetDefaultfilter]    Script Date: 07/26/2008 16:17:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getdefaultfilter')
DROP PROCEDURE usp_getdefaultfilter
GO
CREATE PROCEDURE [dbo].[USP_GetDefaultfilter]
@numDomainId as numeric(9),
@numUserCntId as numeric(9),
@numRelation as numeric(9),
@FormId as numeric(9),
@GroupId AS NUMERIC(9)
as

select * from RelationsDefaultFilter 
	WHERE numUserCntId=@numUserCntId and numDomainId= @numDomainId
	  and numRelationId=@numRelation
		  and numformId=@FormId AND tintGroupId=@GroupId
GO
