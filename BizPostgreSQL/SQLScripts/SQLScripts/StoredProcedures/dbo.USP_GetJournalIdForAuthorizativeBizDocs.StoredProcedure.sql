/****** Object:  StoredProcedure [dbo].[USP_GetJournalIdForAuthorizativeBizDocs]    Script Date: 07/26/2008 16:17:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created By Siva  
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getjournalidforauthorizativebizdocs')
DROP PROCEDURE usp_getjournalidforauthorizativebizdocs
GO
CREATE PROCEDURE [dbo].[USP_GetJournalIdForAuthorizativeBizDocs]  
(  
  @numOppId as numeric(9)=0,  
  @numOppBizDocsId as numeric(9)=0  
)  
As  
Begin  
	Select GJH.numJournal_Id as numJournalId from General_Journal_Header GJH  
	Where GJH.numOppId=@numOppId And GJH.numOppBizDocsId=@numOppBizDocsId
--	and numBizDocsPaymentDetId is null
-- Commented by chintan 
End
GO
