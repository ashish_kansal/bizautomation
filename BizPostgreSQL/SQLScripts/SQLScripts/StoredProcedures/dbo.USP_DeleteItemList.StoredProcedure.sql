/****** Object:  StoredProcedure [dbo].[USP_DeleteItemList]    Script Date: 07/26/2008 16:15:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj        
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_deleteitemlist')
DROP PROCEDURE usp_deleteitemlist
GO
CREATE PROCEDURE [dbo].[USP_DeleteItemList]          
@numListItemID as numeric(9)=0,            
@numListId as numeric(9)=0  
as              
Begin    

	IF ISNULL((SELECT constFlag FROM ListDetails where numListItemID=@numListItemID),0) = 0
	BEGIN
		IF @numListId = 27 AND EXISTS (SELECT numDomainId FROM AuthoritativeBizDocs WHERE numAuthoritativeSales=@numListItemID OR numAuthoritativePurchase=@numListItemID)
		BEGIN
			RAISERROR('USED_AS_AUTHORITATIVE_BIZDOC',16,1)
		END
		ELSE
		BEGIN
			DELETE FROM ListDetails WHERE numListItemID=@numListItemID   
   
			/* delete dependant dropdown relations if anyone in use */
			DELETE FROM [FieldRelationshipDTL] WHERE [numPrimaryListItemID] = @numListItemID
			DELETE FROM [FieldRelationshipDTL] WHERE [numSecondaryListItemID] = @numListItemID
		END
	END

End
GO
