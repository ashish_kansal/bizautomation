/****** Object:  StoredProcedure [dbo].[usp_GetSolutionForEdit]    Script Date: 07/26/2008 16:18:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getsolutionforedit')
DROP PROCEDURE usp_getsolutionforedit
GO
CREATE PROCEDURE [dbo].[usp_GetSolutionForEdit]  
@numSolnID numeric  
--  
AS  
  
BEGIN  
 SELECT numSolnID,numCategory,vcSolnTitle,txtSolution,vcLink  
FROM SolutionMaster WHERE numSolnID=@numSolnID  
  
END
GO
