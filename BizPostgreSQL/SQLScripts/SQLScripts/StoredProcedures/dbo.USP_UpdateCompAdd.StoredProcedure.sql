/****** Object:  StoredProcedure [dbo].[USP_UpdateCompAdd]    Script Date: 07/26/2008 16:21:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_updatecompadd')
DROP PROCEDURE usp_updatecompadd
GO
CREATE PROCEDURE [dbo].[USP_UpdateCompAdd]            
@vcBillStreet as varchar(100),            
@vcBillCity as varchar(50),            
@vcBilState as numeric(9),            
@vcBillPostCode as varchar(15),            
@vcBillCountry as numeric(9),            
@bitSameAddr as tinyint,            
@vcShipStreet as varchar(100),            
@vcShipCity as varchar(50),            
@vcShipState as numeric(9),            
@vcShipPostCode as varchar(15),         
@vcShipCountry as numeric(9),            
@numDivisionID as numeric(9),  
@numContactId as numeric(9),  
@vcFirstname as varchar(50),  
@vcLastName as varchar(50),  
@vcEmail as varchar(50),  
@vcPhone as varchar(15)       
as            
update dbo.AddressDetails            
   set              
  vcStreet=@vcBillStreet,            
  vcCity=@vcBillCity,            
  numState=@vcBilState,            
  vcPostalCode=@vcBillPostCode,            
  numCountry=@vcBillCountry        
where  numRecordID= @numDivisionID AND tintAddressOf=2 AND tintAddressType=1 AND bitIsPrimary=1
update dbo.AddressDetails            
   set              
  vcStreet=@vcShipStreet,            
  vcCity=@vcShipCity,            
  numState=@vcShipState,            
  vcPostalCode=@vcShipPostCode,            
  numCountry=@vcShipCountry
where  numRecordID= @numDivisionID AND tintAddressOf=2 AND tintAddressType=2 AND bitIsPrimary=1
if  @numContactId>0  
begin  
 update AdditionalContactsInformation   
  set vcFirstName=@vcFirstname,  
   vcLastName=@vcLastName,  
   numPhone=@vcPhone,  
   vcEmail=@vcEmail  
 where numContactID=@numContactId  
END


declare @bitAutoPopulateAddress bit 
declare @tintPoulateAddressTo tinyint 
-- Auto Populate Primary Address if address not specified
select @bitAutoPopulateAddress= isnull(bitAutoPopulateAddress,'0'),@tintPoulateAddressTo=isnull(tintPoulateAddressTo,'0') from Domain where numDomainId IN (SELECT numDomainId FROM dbo.DivisionMaster WHERE numDivisionID=@numDivisionID);
if (@bitAutoPopulateAddress = 1)
BEGIN
	DECLARE @PContact NUMERIC
	DECLARE @vcPStreet varchar(100)                      
	DECLARE @vcPCity varchar(50)
	DECLARE @vcPPostalCode varchar(15)
	DECLARE @vcPState numeric(9)
	DECLARE @vcPCountry numeric(9)
	
	SELECT @PContact = numContactId FROM AdditionalContactsInformation WHERE numDivisionId=@numDivisionID AND ISNULL(bitPrimaryContact,0)=1
	if(@tintPoulateAddressTo = 1) --if primary address is not specified then getting billing address    
	begin
		 
		  select    
			@vcPStreet = AD1.vcStreet,
			@vcPCity= AD1.vcCity,
			@vcPState= AD1.numState,
			@vcPPostalCode= AD1.vcPostalCode,
			@vcPCountry= AD1.numCountry
		  from dbo.AddressDetails AD1
		  where  numRecordID= @numDivisionID AND tintAddressOf=2 AND tintAddressType=1 AND bitIsPrimary=1
	end  
	else if (@tintPoulateAddressTo = 2)-- Primary Address is Shipping Address
	begin
		  select                   
			@vcPStreet= AD2.vcStreet,
			@vcPCity=AD2.vcCity,
			@vcPState=AD2.numState,
			@vcPPostalCode=AD2.vcPostalCode,
			@vcPCountry=AD2.numCountry                   
		   from dbo.AddressDetails AD2
		  where  numRecordID= @numDivisionID AND tintAddressOf=2 AND tintAddressType=2 AND bitIsPrimary=1

	
	end  
	
-- ONLY UPDATE WHEN address lenght is 0 ,
DECLARE @PrimaryAddress VARCHAR(100);
SELECT @PrimaryAddress = ISNULL(vcStreet,'') + ISNULL(vcCity,'') FROM dbo.AddressDetails WHERE numRecordID=@PContact AND bitIsPrimary=1 AND tintAddressOf=1 AND tintAddressType=0
IF LEN(@PrimaryAddress)=0
BEGIN
	UPDATE dbo.AddressDetails 
	SET vcStreet = @vcPStreet,
			vcCity = @vcPCity,
			numCountry = @vcPCountry,
			vcPostalCode = @vcPPostalCode,
			numState = @vcPState
	 WHERE  numRecordID = @PContact	AND tintAddressOf=1 AND tintAddressType=0 AND bitIsPrimary=1
	 
END
 
END
GO
