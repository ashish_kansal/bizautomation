/****** Object:  StoredProcedure [dbo].[usp_GetPageList]    Script Date: 07/26/2008 16:18:09 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getpagelist')
DROP PROCEDURE usp_getpagelist
GO
CREATE PROCEDURE [dbo].[usp_GetPageList]
	@numModuleID NUMERIC(9) = 0,
	@numPageID NUMERIC(9) = 0   
--
AS
	IF @numPageID <> 0
		BEGIN
			SELECT PM.numPageID, PM.vcPageDesc, PM.vcFileName, PM.bitIsViewApplicable, PM.bitIsAddApplicable, PM.bitIsUpdateApplicable, PM.bitIsDeleteApplicable, PM.bitIsExportApplicable
			FROM PageMaster PM
			WHERE 
			PM.numPageID = @numPageID
			AND PM.numModuleID = @numModuleID
			ORDER BY PM.numPageID
		END
	ELSE
		BEGIN
			SELECT PM.numPageID, PM.vcPageDesc, PM.vcFileName, PM.bitIsViewApplicable, PM.bitIsAddApplicable, PM.bitIsUpdateApplicable, PM.bitIsDeleteApplicable, PM.bitIsExportApplicable
			FROM PageMaster PM
			WHERE PM.numModuleID = @numModuleID
			ORDER BY PM.numPageID
		END
GO
