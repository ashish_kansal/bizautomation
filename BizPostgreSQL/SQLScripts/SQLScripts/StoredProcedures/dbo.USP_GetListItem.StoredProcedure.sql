/****** Object:  StoredProcedure [dbo].[USP_GetListItem]    Script Date: 07/26/2008 16:17:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getlistitem')
DROP PROCEDURE usp_getlistitem
GO
CREATE PROCEDURE [dbo].[USP_GetListItem]
@numListItemID as numeric(9)=0
as
select isnull(vcData,'') from listdetails where numListItemID=@numListItemID
GO
