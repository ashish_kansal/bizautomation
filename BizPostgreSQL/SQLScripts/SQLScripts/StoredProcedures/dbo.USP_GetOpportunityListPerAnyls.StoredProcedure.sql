/****** Object:  StoredProcedure [dbo].[USP_GetOpportunityListPerAnyls]    Script Date: 07/26/2008 16:18:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- Created By Anoop Jayaraj                            
 GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getopportunitylistperanyls')
DROP PROCEDURE usp_getopportunitylistperanyls
GO
CREATE PROCEDURE [dbo].[USP_GetOpportunityListPerAnyls]                                  
 @numUserCntID numeric(9)=0,                                  
 @numDomainID numeric(9)=0,                                             
 @OppStatus as tinyint,                            
 @Type as tinyint,                                  
 @CurrentPage int,                                  
 @PageSize int,                                  
 @TotRecs int output,                                  
 @columnName as Varchar(50),                                  
 @columnSortOrder as Varchar(10),                            
 @numStartDate datetime,                                
 @numEndDate datetime,                    
 @strUserIDs as varchar(1000),      
 @TeamType as tinyint        
                              
as                               
                                        
if @strUserIDs='' set @strUserIDs='0'                                 
--Create a Temporary table to hold data                       
if  @Type<>3                    
begin                              
Create table #tempTable ( ID INT IDENTITY PRIMARY KEY,                                   
      numOppId VARCHAR(15),                                  
      Name varchar(100),                                  
      STAGE VARCHAR(100),                                  
      CloseDate varchar(20),                                  
 numcreatedby varchar(15),                                  
 Contact varchar(110),                                  
 Company varchar(100),                                  
 numCompanyID varchar(15),                            
 OppType  varchar(20),                                  
 tintCRMType varchar(10),                                  
 numContactID varchar(15),                                  
 numDivisionID  varchar(15),                                  
 numTerID varchar(15),                                  
 monPAmount DECIMAL(20,5),                                  
 vcusername varchar(50)                                  
 )                      
end                                
declare @strSql as varchar(8000)                             
if   @Type= 1                            
begin                               
set @strSql='SELECT  Opp.numOppId,                                   
  Opp.vcPOppName AS Name,                                   
  dbo.GetOppStatus(Opp.numOppId) as STAGE,                                   
  Opp.intPEstimatedCloseDate AS CloseDate,                                   
  isnull(Opp.numcreatedby,0) ,                                  
  ADC.vcFirstName + '' '' + ADC.vcLastName AS Contact,                                   
  C.vcCompanyName AS Company,                                  
                C.numCompanyID,                             
  case when tintOppType=1 then ''Sales opportunity'' else ''Purchase opportunity''  end as OppType,                                  
  Div.tintCRMType,                                  
  ADC.numContactID,                                  
  Div.numDivisionID,                                  
  Div.numTerID,                                  
  Opp.monPAmount,                                  
  dbo.fn_GetContactName(Opp.numRecOwner) as vcUserName                                  
  FROM OpportunityMaster Opp                                   
  INNER JOIN AdditionalContactsInformation ADC                                   
  ON Opp.numContactId = ADC.numContactId                                   
  INNER JOIN DivisionMaster Div                                   
  ON Opp.numDivisionId = Div.numDivisionID AND ADC.numDivisionId = Div.numDivisionID                                   
  INNER JOIN CompanyInfo C                                   
  ON Div.numCompanyID = C.numCompanyId                                                        
 WHERE  tintopptype=1 and Opp.bintCreatedDate  between ''' + convert(varchar(20),@numStartDate)+'''                              
  and ''' + convert(varchar(20),@numEndDate)+'''  and Opp.numRecOwner in (select numUserCntID from UserTeams                                             
  where numTeam in(select numTeam from ForReportsByTeam F where F.numUserCntID=' + convert(varchar(15),@numUserCntID)+'                             
  and F.numDomainID=' + convert(varchar(15),@numDomainId)+' and F.tintType=' + convert(varchar(15), @TeamType)+'))                               
  and Opp.numDomainID=' + convert(varchar(15),@numDomainID)                        
  if @OppStatus> 0 set  @strSql=@strSql+ '  and Opp.tintOppStatus= ' + convert(varchar(1),@OppStatus)                               
   set  @strSql=@strSql+' ORDER BY ' + @columnName +' '+ @columnSortOrder                      
 end                             
if   @Type= 2                            
begin            set @strSql='SELECT  Opp.numOppId,                                   
  Opp.vcPOppName AS Name,                            
  dbo.GetOppStatus(Opp.numOppId) as STAGE,                                   
  Opp.intPEstimatedCloseDate AS CloseDate,                                   
  Opp.numRecOwner,                                  
  ADC.vcFirstName + '' '' + ADC.vcLastName AS Contact,                                   
  C.vcCompanyName AS Company,                                  
  C.numCompanyID,                            
  case when tintOppType=1 then ''Sales opportunity'' else ''Purchase opportunity''  end as OppType,                              
  Div.tintCRMType,                                  
  ADC.numContactID,                                  
  Div.numDivisionID,                                  
  Div.numTerID,                                  
  Opp.monPAmount,                                  
  dbo.fn_GetContactName(Opp.numRecOwner) vcUserName                                  
  FROM OpportunityMaster Opp                                   
  INNER JOIN AdditionalContactsInformation ADC                         
   ON Opp.numContactId = ADC.numContactId                                   
  INNER JOIN DivisionMaster Div                                   
  ON Opp.numDivisionId = Div.numDivisionID AND ADC.numDivisionId = Div.numDivisionID                                   
  INNER JOIN CompanyInfo C                                   
  ON Div.numCompanyID = C.numCompanyId                                                        
  WHERE Opp.bintCreatedDate  between ''' + convert(varchar(20),@numStartDate)+'''                              
  and ''' + convert(varchar(20),@numEndDate)+'''  and tintopptype=1 and Opp.numRecOwner=' + convert(varchar(15),@numUserCntID)+'                               
  and ADC.numDomainID=' + convert(varchar(15),@numDomainID)                                  
  if @OppStatus> 0 set  @strSql=@strSql+ 'and Opp.tintOppStatus= ' + convert(varchar(1),@OppStatus)                               
  set  @strSql=@strSql+' ORDER BY ' + @columnName +' '+ @columnSortOrder                        
                                
 end                 
print @strSql                  
if  @Type<>3                    
begin                                 
insert into #tempTable(numOppId,                                  
      Name,                                  
      STAGE,                                  
      CloseDate,                                  
 numcreatedby ,                                  
 Contact,                                  
 Company,                                  
 numCompanyID,                             
 OppType,                                 
 tintCRMType,                                  
 numContactID,                                  
 numDivisionID,                                  
 numTerID,                                  
 monPAmount,                                  
 vcusername)                                  
exec (@strSql)                                   
                                  
  declare @firstRec as integer                                   
  declare @lastRec as integer                         
 set @firstRec= (@CurrentPage-1) * @PageSize                                  
     set @lastRec= (@CurrentPage*@PageSize+1)                                  
select * from #tempTable where ID > @firstRec and ID < @lastRec                                  
set @TotRecs=(select count(*) from #tempTable)                                  
drop table #tempTable                      
end                    
                    
 if  @Type=3                    
begin                    
                    
 set  @strSql= 'SELECT ADC1.numContactId,isnull(vcFirstName,''-'') as vcFirstName,isnull(vcLastName,''-'') as vcLastName,(select count(*) from OPPORTUNITYMASTER OM  WHERE                               
     (OM.bintCreatedDate  between '''+convert(varchar(20),@numStartDate)+''' and '''+convert(varchar(20),@numEndDate)+''') and tintopptype=1 and OM.numRecOwner = ADC1.numContactId'                    
                    
if @OppStatus> 0 set  @strSql=@strSql+ ' and OM.tintOppStatus= ' + convert(varchar(1),@OppStatus)                     
set  @strSql=@strSql+') as NoofOpp FROM  AdditionalContactsInformation ADC1                   
                       
  WHERE                  
   ADC1.numContactId in ('+@strUserIDs+')'                     
                  
exec (@strSql)                    
                            
end
GO
