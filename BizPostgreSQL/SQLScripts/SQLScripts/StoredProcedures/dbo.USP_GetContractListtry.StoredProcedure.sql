/****** Object:  StoredProcedure [dbo].[USP_GetContractListtry]    Script Date: 07/26/2008 16:16:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getcontractlisttry')
DROP PROCEDURE usp_getcontractlisttry
GO
CREATE PROCEDURE [dbo].[USP_GetContractListtry]                                                                     
 @numUserCntID numeric(9)=0,                                                                      
 @numDomainID numeric(9)=0,                                                                      
 @tintSortOrder tinyint=4,                                                                      
 @SortChar char(1)='0',                                                                                                                        
 @CurrentPage int,                                                                    
@PageSize int,                                                                    
@TotRecs int output,                                                                    
@columnName as Varchar(50),                                                                    
@columnSortOrder as Varchar(10)='desc'                                                                 
                                                                  
as   
Create table #tempTable ( ID INT IDENTITY PRIMARY KEY,                                                   
      numContractID varchar(15)                                                  
 )  
                                                  
declare @strSql as varchar(8000)                                            
 set  @strSql='select                               
c.numcontractId             
from ContractManagement c                                  
join divisionmaster d on c.numDivisionId= d.numDivisionId                
join companyinfo com on d.numcompanyId = com.numcompanyId                
where numUserCntID = '+convert(varchar(100),@numUserCntId)+' and c.numDomainId ='+convert(varchar(100),@numDomainId)+'
'
if @SortChar<>'0' set @strSql=@strSql + ' And vcContractName like '''+@SortChar+'%'''                                                           
set @strSql=@strSql + 'ORDER BY ' + @columnName +' '+ @columnSortOrder   

Print @strSql      
insert into #tempTable(numContractID)                                                  
exec(@strSql)

 declare @firstRec as integer                                                  
  declare @lastRec as integer                                                  
 set @firstRec= (@CurrentPage-1) * @PageSize                                                  
 set @lastRec= (@CurrentPage*@PageSize+1)                                                   
set @TotRecs=(select count(*) from #tempTable) 

select                 
                 
c.numcontractId,                
c.numDivisionId,                
vcContractName,    
case when bitdays =1 then      
isnull(convert(varchar(100),    
case when datediff(day,bintStartdate,getutcdate())>=0 then datediff(day,bintStartdate,getutcdate()) else 0 end    
    
),0) +' of '+ isnull(convert(varchar(100),datediff(day,bintStartDate,bintExpDate)),0)     
else '-' end as Days,                
    
case when bitincidents =1 then      
isnull(convert(varchar(100),dbo.GetIncidents(c.numcontractId,c.numDivisionId,@numDomainId  ),0),0) +' of '+ isnull(convert(varchar(100),numincidents),0)     
else '-' end as Incidents,        
      
case when bitamount =1 then      
isnull(convert(varchar(100),dbo.GetContractRemainingAmount(c.numcontractId,c.numDivisionId,@numDomainId)),0) + ' of ' + convert(varchar(100),convert(decimal(10,2),c.numAmount))     
else '-' end  as Amount,              
    
case when bithour =1 then      
isnull(convert(varchar(100),convert(decimal(5,2),dbo.GetContractRemainingHours(c.numcontractId,c.numDivisionId,@numDomainId))),0) +' of ' + convert(varchar(100),c.numhours )    
else '-' end as Hours,               
              
c.bintCreatedOn,                
com.vcCompanyName                 
               
from ContractManagement c                                  
join divisionmaster d on c.numDivisionId= d.numDivisionId                
join companyinfo com on d.numcompanyId = com.numcompanyId   
 join #tempTable T on T.numContractId=C.numContractId                                             
   WHERE ID > @firstRec and ID < @lastRec            
order by ID                                 
                                                
drop table #tempTable
GO
