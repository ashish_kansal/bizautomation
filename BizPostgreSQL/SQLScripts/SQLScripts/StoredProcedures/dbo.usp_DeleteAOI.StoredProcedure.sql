/****** Object:  StoredProcedure [dbo].[usp_DeleteAOI]    Script Date: 07/26/2008 16:15:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_deleteaoi')
DROP PROCEDURE usp_deleteaoi
GO
CREATE PROCEDURE [dbo].[usp_DeleteAOI]
	@numAOIID numeric,
	@numUserID numeric,
	@bintTimeStamp bigint 
--  
AS
	--This SP will update the Name changes of the AOIs.
BEGIN
	UPDATE AOIMaster 
		SET bitDeleted=1,
			numModifiedBy=@numUserID,
			bintmodifiedDate=@bintTimeStamp
		WHERE numAOIID=@numAOIID

END
GO
