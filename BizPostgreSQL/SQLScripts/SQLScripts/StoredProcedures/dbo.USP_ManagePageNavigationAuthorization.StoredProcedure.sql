GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_ManagePageNavigationAuthorization' ) 
    DROP PROCEDURE USP_ManagePageNavigationAuthorization
GO

CREATE PROCEDURE USP_ManagePageNavigationAuthorization
	@numGroupID		NUMERIC(18,0),
	@numTabID		NUMERIC(18,0),
    @numDomainID	NUMERIC(18,0),
    @strItems		VARCHAR(MAX)
AS 
BEGIN
    SET NOCOUNT ON  
    BEGIN TRY 
        BEGIN TRAN  
           
        DELETE  FROM dbo.TreeNavigationAuthorization WHERE numGroupID = @numGroupID 
													 AND numDomainID = @numDomainID
													 AND numTabID = @numTabID 
													 AND numPageNavID NOT IN (SELECT ISNULL(numPageNavID,0) FROM dbo.PageNavigationDTL WHERE bitVisible=0)
				
        DECLARE @hDocItem INT
        IF CONVERT(VARCHAR(MAX), @strItems) <> '' 
            BEGIN
                EXEC sp_xml_preparedocument @hDocItem OUTPUT, @strItems
                
				IF @numTabID = 7 --Relationships
				BEGIN

					DELETE FROM TreeNodeOrder WHERE numGroupID = @numGroupID AND numDomainID = @numDomainID AND numTabID = @numTabID	

					SELECT  
						@numGroupID AS numGroupID,
						@numTabID AS numTabID,
						X.[numPageNavID] AS numPageNavID,
						X.[numListItemID] As numListItemID,
						X.[bitVisible] AS bitVisible,
						@numDomainID AS numDomainID,
						X.[numParentID] AS numParentID,
						X.tintType AS tintType,
						X.[numOrder] AS numOrder
					INTO
						#TMEP
					FROM    
						( 
							SELECT    
								*
							FROM      
								OPENXML (@hDocItem, '/NewDataSet/Table1', 2)
								WITH ( [numPageNavID] NUMERIC(18, 0), bitVisible BIT, tintType TINYINT, numListItemID NUMERIC(18,0),numParentID NUMERIC(18,0), numOrder INT)
						) X


					INSERT  INTO [dbo].[TreeNavigationAuthorization]
							(
							  [numGroupID]
							  ,[numTabID]
							  ,[numPageNavID]
							  ,[bitVisible]
							  ,[numDomainID]
							  ,tintType
							)
					SELECT
						numGroupID,
						numTabID,
						numPageNavID,
						bitVisible,
						numDomainID,
						tintType
					FROM
						#TMEP
					WHERE
						ISNULL(numPageNavID,0) <> 0

					INSERT  INTO [dbo].[TreeNodeOrder]
					(
						numTabID,
						numDomainID,
						numGroupID,
						numPageNavID,
						numListItemID,
						numParentID,
						bitVisible,
						tintType,
						numOrder
					)
					SELECT
						numTabID,
						numDomainID,
						numGroupID,
						numPageNavID,
						numListItemID,
						numParentID,
						bitVisible,
						tintType,
						numOrder
					FROM
						#TMEP
				END
				ELSE
				BEGIN
					INSERT  INTO [dbo].[TreeNavigationAuthorization]
                        (
                          [numGroupID]
						  ,[numTabID]
						  ,[numPageNavID]
						  ,[bitVisible]
						  ,[numDomainID]
						  ,tintType
                        )
                        SELECT  @numGroupID,
							    @numTabID,
							    X.[numPageNavID],
							    X.[bitVisible],
							    @numDomainID,
							    X.tintType
                        FROM    ( SELECT    *
                                  FROM      OPENXML (@hDocItem, '/NewDataSet/Table1', 2)
                                            WITH ( [numPageNavID] NUMERIC(18, 0), bitVisible BIT, tintType TINYINT)
                                ) X

					-- SHOW PARENT RECURSIVELY
					;WITH CTE (numPageNavID,numParentID) AS
					(
						SELECT
							PND.numPageNavID
							,PND.numParentID
						FROM
							TreeNavigationAuthorization TNA
						INNER JOIN
							PageNavigationDTL PND
						ON
							TNA.numPageNavID = PND.numPageNavID
						WHERE
							TNA.bitVisible = 1
							AND TNA.numDomainID = @numDomainID
							AND TNA.numGroupID = @numGroupID
							AND TNA.numPageNavID NOT IN (26)
							AND TNA.numTabID = @numTabID
						UNION ALL
						SELECT 
							PND.numPageNavID
							,PND.numParentID
						FROM
							TreeNavigationAuthorization TNA
						INNER JOIN
							PageNavigationDTL PND
						ON
							TNA.numPageNavID = PND.numPageNavID
						JOIN
							CTE c
						ON
							TNA.numPageNavID= c.numParentID
						WHERE
							TNA.numDomainID = @numDomainID
							AND TNA.numGroupID = @numGroupID
							AND TNA.numTabID = @numTabID
					)

					UPDATE 
						TreeNavigationAuthorization 
					SET 
						bitVisible = 1 
					WHERE 
						numDomainID = @numDomainID
						AND numGroupID = @numGroupID
						AND numTabID = @numTabID 
						AND bitVisible=0 
						AND numPageNavID IN (SELECT numPageNavID FROM CTE)


					--HIDE CHILD RECURSIVELY
					;WITH CTE (numPageAuthID,numDomainID,numGroupID,numTabID,numPageNavID) AS
					(
						SELECT
							numPageAuthID,
							numDomainID,
							numGroupID,
							numTabID,
							numPageNavID
						FROM
							TreeNavigationAuthorization TNA
						WHERE
							TNA.bitVisible = 0
							AND TNA.numDomainID = @numDomainID
							AND TNA.numGroupID = @numGroupID
							AND TNA.numPageNavID NOT IN (26)
							AND TNA.numTabID = @numTabID
						UNION ALL
						SELECT 
							TNA.numPageAuthID,
							TNA.numDomainID,
							TNA.numGroupID,
							TNA.numTabID,
							TNA.numPageNavID
						FROM
							TreeNavigationAuthorization TNA
						JOIN
							PageNavigationDTL PND
						ON
							TNA.numPageNavID = PND.numPageNavID
						JOIN
							CTE c
						ON
							PND.numParentID= c.numPageNavID
						WHERE
							TNA.numDomainID = @numDomainID
							AND TNA.numGroupID = @numGroupID
							AND TNA.numTabID = @numTabID
					)

					UPDATE TreeNavigationAuthorization SET bitVisible = 0 WHERE bitVisible=1 AND numPageAuthID IN (SELECT numPageAuthID FROM CTE) 
				END
                EXEC sp_xml_removedocument @hDocItem
            END


        COMMIT TRAN 
    END TRY 
    BEGIN CATCH		
        DECLARE @strMsg VARCHAR(200)
        SET @strMsg = ERROR_MESSAGE()
        IF ( @@TRANCOUNT > 0 ) 
            BEGIN
                RAISERROR ( @strMsg, 16, 1 ) ;
                ROLLBACK TRAN
                RETURN 1
            END
    END CATCH
END