/****** Object:  StoredProcedure [dbo].[USP_GetItemsForCreatingOpp]    Script Date: 07/26/2008 16:17:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getitemsforcreatingopp')
DROP PROCEDURE usp_getitemsforcreatingopp
GO
CREATE PROCEDURE [dbo].[USP_GetItemsForCreatingOpp]    
@numOppID as numeric(9)=0    
as    
select      
 OppOld.numoppitemtCode,    
 OppOld.numItemCode,    
 i.vcItemName,    
 OppOld.vcItemDesc as [Desc],    
 OppOld.vcType ItemType,    
 OppOld.numUnitHour,    
 OppOld.monPrice,    
 OppOld.monTotAmount,    
 (SELECT TOP 1  vcPathForImage FROM dbo.ItemImages 
 WHERE  numItemCode  = I.numItemCode
 AND BitDefault = 1 AND bitIsImage = 1
 )   
  AS vcPathForImage,    
 OppOld.vcItemDesc,    
 WItems.numWarehouseID,    
 OppOld.numWarehouseItmsID ,    
 0 as Op_Flag,    
 vcWarehouse as Warehouse,    
 I.bitSerialized,    
 dbo.fn_GetAttributes(OppOld.numWarehouseItmsID,I.bitSerialized) as Attributes,    
    vcPOppName,    
    vcCompanyName,    
 isnull(OppNewMst.numOppID,0) as numoppIDNew,    
    OppNew.monPrice as NewPrice    
from OpportunityItems OppOld    
join item I     
on OppOld.numItemCode=i.numItemcode    
left join  WareHouseItems  WItems    
on   WItems.numWareHouseItemID= OppOld.numWarehouseItmsID    
left join  Warehouses  W     
on   W.numWarehouseID= WItems.numWarehouseID     
left join OpportunityItemLinking OppLink    
on numOppOldItemID=OppOld.numoppitemtCode    
left join OpportunityItems OppNew    
on numOppNewItemID=OppNew.numoppitemtCode    
left join  OpportunityMaster OppNewMst    
on OppNewMst.numOppID=OppNew.numOppID    
left join DivisionMaster Div    
on Div.numDivisionID=OppNewMst.numDivisionID    
left join CompanyInfo Com    
on Com.numCompanyID=Div.numCompanyID    
where OppOld.numOppId=@numOppID
GO
