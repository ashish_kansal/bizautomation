/****** Object:  StoredProcedure [dbo].[USP_UpdateEcommHeaderFooter]    Script Date: 07/26/2008 16:21:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_updateecommheaderfooter')
DROP PROCEDURE usp_updateecommheaderfooter
GO
CREATE PROCEDURE [dbo].[USP_UpdateEcommHeaderFooter]    
@numDomainId as numeric(9)=0,    
@strText as ntext,    
@byteMode as tinyint    
as    
    
    
    
-- if @byteMode=0    
--  update eCommerceDTL set txtHeader=@strText where numDomainID=@numDomainId    
-- if @byteMode=1    
--  update eCommerceDTL set txtFooter=@strText where numDomainID=@numDomainId    
 if @byteMode=2   
  update DynamicFormMasterParam set ntxtHeader=@strText where numDomainID=@numDomainId and   numFormID=3  
 if @byteMode=3    
  update DynamicFormMasterParam set ntxtFooter=@strText where numDomainID=@numDomainId   and   numFormID=3  
 if @byteMode=4   
  update DynamicFormMasterParam set ntxtLeft=@strText where numDomainID=@numDomainId   and   numFormID=3  
 if @byteMode=5    
  update DynamicFormMasterParam set ntxtRight=@strText where numDomainID=@numDomainId   and   numFormID=3
 if @byteMode=6    
  update DynamicFormMasterParam set ntxtStyle=@strText where numDomainID=@numDomainId   and   numFormID=3
GO
