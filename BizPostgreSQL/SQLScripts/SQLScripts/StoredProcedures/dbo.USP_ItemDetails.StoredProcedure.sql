GO
/****** Object:  StoredProcedure [dbo].[USP_ItemDetails]    Script Date: 02/07/2010 15:31:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created By Anoop Jayaraj                                          
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_itemdetails')
DROP PROCEDURE usp_itemdetails
GO
CREATE PROCEDURE [dbo].[USP_ItemDetails]                                                  
@numItemCode as numeric(9) ,          
@ClientTimeZoneOffset as int,
@numDivisionId as numeric(18)                                               
AS
BEGIN 
   DECLARE @bitItemIsUsedInOrder AS BIT=0
   DECLARE @bitOppOrderExists AS BIT = 0
   
   DECLARE @numCompanyID NUMERIC(18,0)=0
   IF ISNULL(@numDivisionId,0) > 0
   BEGIN
		SELECT @numCompanyID=numCompanyID FROM DivisionMaster WHERE numDivisionID=@numDivisionId
   END

   DECLARE @numDomainId AS NUMERIC
   DECLARE @numItemGroup AS NUMERIC(18,0)
   SELECT @numDomainId=numDomainId,@numItemGroup=numItemGroup FROM Item WHERE numItemCode=@numItemCode
   
	
	IF (SELECT COUNT(OI.numOppId) FROM dbo.OpportunityItems OI WHERE OI.numItemCode =@numItemCode) > 0
	BEGIN
		SET @bitItemIsUsedInOrder=1
		SET @bitOppOrderExists = 1
	END
	
	IF (SELECT COUNT(OI.numOppId) FROM dbo.OpportunityKitItems OI WHERE OI.numChildItemID =@numItemCode) > 0
	BEGIN
		SET @bitItemIsUsedInOrder=1
		SET @bitOppOrderExists = 1
	END
	
	IF (SELECT COUNT(OI.numOppId) FROM dbo.OpportunityKitChildItems OI WHERE OI.numItemID =@numItemCode) > 0
	BEGIN
		SET @bitItemIsUsedInOrder=1
		SET @bitOppOrderExists = 1
	END
	
	IF EXISTS(SELECT numItemID FROM dbo.General_Journal_Details WHERE numItemID= @numItemCode and numDomainId=@numDomainId AND chBizDocItems <> 'IA1' AND chBizDocItems <> 'OE1')
	BEGIN
		SET @bitItemIsUsedInOrder =1
    END
	                                      
	SELECT 
		I.numItemCode, vcItemName, ISNULL(txtItemDesc,'') txtItemDesc,CAST(ISNULL(IED.txtDesc,'')  AS VARCHAR(MAX)) vcExtendedDescToAPI, charItemType, 
		CASE 
			WHEN charItemType='P' AND ISNULL(bitAssembly,0) = 1 then 'Assembly'
			WHEN charItemType='P' AND ISNULL(bitKitParent,0) = 1 then 'Kit'
			WHEN charItemType='P' AND ISNULL(bitAsset,0) = 1 AND ISNULL(bitRental,0)=0 then 'Asset'
			WHEN charItemType='P' AND ISNULL(bitAsset,0) = 1 AND ISNULL(bitRental,0)=1 then 'Rental Asset'
			WHEN charItemType='P' AND ISNULL(bitSerialized,0) = 1 AND ISNULL(bitAsset,0) = 0  then 'Serialized'
			WHEN charItemType='P' AND ISNULL(bitSerialized,0) = 1 AND ISNULL(bitAsset,0) = 1 AND ISNULL(bitRental,0)=0 then 'Serialized Asset'
			WHEN charItemType='P' AND ISNULL(bitSerialized,0) = 1 AND ISNULL(bitAsset,0) = 1 AND ISNULL(bitRental,0)=1 then 'Serialized Rental Asset'
			WHEN charItemType='P' AND ISNULL(bitLotNo,0)=1 THEN 'Lot #'
			ELSE CASE WHEN charItemType='P' THEN 'Inventory' ELSE '' END
		END AS InventoryItemType
		,dbo.fn_GetItemChildMembershipCount(@numDomainId,I.numItemCode) AS numChildMembershipCount
		,CASE WHEN ISNULL(bitAssembly,0) = 1 THEN dbo.fn_GetAssemblyPossibleWOQty(I.numItemCode,ISNULL(W.numWareHouseID,0)) ELSE 0 END AS numWOQty
		,CASE WHEN charItemType='P' THEN ISNULL((SELECT TOP 1 monWListPrice FROM WarehouseItems WHERE numDomainID=@numDomainID AND numItemID=@numItemCode AND ISNULL(monWListPrice,0) > 0),0) ELSE monListPrice END monListPrice,                   
		numItemClassification, isnull(bitTaxable,0) as bitTaxable, ISNULL(vcSKU,'') AS vcItemSKU, 
		(CASE WHEN I.numItemGroup > 0 THEN ISNULL(W.[vcWHSKU],'') ELSE ISNULL(vcSKU,'') END) AS vcSKU, 
		ISNULL(bitKitParent,0) as bitKitParent, V.numVendorID, V.monCost, I.numDomainID, I.numCreatedBy, I.bintCreatedDate, I.bintModifiedDate,I.numModifiedBy,
		(SELECT TOP 1 vcPathForImage FROM ItemImages WHERE numItemCode = @numItemCode AND bitDefault = 1 AND bitIsImage = 1 ) AS vcPathForImage ,
		(SELECT TOP 1 vcPathForTImage FROM ItemImages WHERE numItemCode = @numItemCode AND bitDefault = 1 AND bitIsImage = 1) AS  vcPathForTImage
		,ISNULL(bitSerialized,0) as bitSerialized, vcModelID,                   
		(SELECT 
			numItemImageId
			,vcPathForImage
			,vcPathForTImage
			,bitDefault
			,CASE WHEN bitdefault=1 THEN -1 ELSE ISNULL(intDisplayOrder,0) END AS intDisplayOrder 
		FROM 
			ItemImages  
		WHERE 
			numItemCode=@numItemCode 
		ORDER BY 
			CASE WHEN bitdefault=1 THEN -1 ELSE isnull(intDisplayOrder,0) END ASC 
		FOR XML AUTO,ROOT('Images')) AS xmlItemImages,
		(SELECT STUFF((SELECT ',' + CONVERT(VARCHAR(50) , numCategoryID)  FROM dbo.ItemCategory WHERE numItemID = I.numItemCode FOR XML PATH('')), 1, 1, '')) AS vcItemCategories ,
		numItemGroup, 
		(CASE WHEN ISNULL(numItemGroup,0) > 0 AND (SELECT COUNT(*) FROM CFW_Fld_Master CFW JOIN ItemGroupsDTL ON numOppAccAttrID=Fld_id WHERE numItemGroupID=numItemGroup AND tintType=2) > 0 THEN 1 ELSE 0 END) bitItemGroupHasAttributes,
		numCOGsChartAcntId, numAssetChartAcntId, numIncomeChartAcntId, ISNULL(bitExpenseItem,0) bitExpenseItem, ISNULL(numExpenseChartAcntId,0) numExpenseChartAcntId, (CASE WHEN ISNULL(I.bitVirtualInventory,0) = 1 THEN 0 ELSE monAverageCost END) AS monAverageCost,                   
		monCampaignLabourCost,dbo.fn_GetContactName(I.numCreatedBy)+' ,'+convert(varchar(20),DateAdd(minute, -@ClientTimeZoneOffset,I.bintCreatedDate )) as CreatedBy ,                                      
		dbo.fn_GetContactName(I.numModifiedBy)+' ,'+convert(varchar(20),DateAdd(minute, -@ClientTimeZoneOffset,I.bintModifiedDate )) as ModifiedBy,                      
		sum(numOnHand) as numOnHand,                      
		sum(numOnOrder) as numOnOrder,                      
		MAX(numReorder)  as numReOrder,                      
		sum(numAllocation)  as numAllocation,                      
		sum(numBackOrder)  as numBackOrder,                   
		isnull(fltWeight,0) as fltWeight,                
		isnull(fltHeight,0) as fltHeight,                
		isnull(fltWidth,0) as fltWidth,                
		isnull(fltLength,0) as fltLength,                
		isnull(bitFreeShipping,0) as bitFreeShipping,              
		isnull(bitAllowBackOrder,0) as bitAllowBackOrder,              
		isnull(vcUnitofMeasure,'Units') as vcUnitofMeasure,      
		isnull(bitShowDeptItem,0) bitShowDeptItem,      
		isnull(bitShowDeptItemDesc,0) bitShowDeptItemDesc,  
		isnull(bitCalAmtBasedonDepItems ,0) bitCalAmtBasedonDepItems,
		isnull(bitAssembly ,0) bitAssembly ,
		isnull(cast(numBarCodeId as varchar(50)),'') as  numBarCodeId ,
		(CASE WHEN ISNULL(I.numManufacturer,0) > 0 THEN ISNULL((SELECT vcCompanyName FROM DivisionMaster INNER JOIN CompanyInfo ON DivisionMaster.numCompanyID=CompanyInfo.numCompanyId WHERE numDivisionID=I.numManufacturer),'')  ELSE ISNULL(I.vcManufacturer,'') END) AS vcManufacturer,
		ISNULL(I.numManufacturer,0) numManufacturer,
		ISNULL(I.numBaseUnit,0) AS numBaseUnit,ISNULL(I.numPurchaseUnit,0) AS numPurchaseUnit,ISNULL(I.numSaleUnit,0) AS numSaleUnit,
		dbo.fn_GetUOMName(ISNULL(I.numBaseUnit,0)) AS vcBaseUnit,dbo.fn_GetUOMName(ISNULL(I.numPurchaseUnit,0)) AS vcPurchaseUnit,dbo.fn_GetUOMName(ISNULL(I.numSaleUnit,0)) AS vcSaleUnit,
		isnull(bitLotNo,0) as bitLotNo,
		ISNULL(IsArchieve,0) AS IsArchieve,
		case when charItemType='P' then 'Inventory Item' when charItemType='S' then 'Service' when charItemType='A' then 'Accessory' when charItemType='N' then 'Non-Inventory Item' end as ItemType,
		isnull(I.numItemClass,0) as numItemClass,
		ISNULL(tintStandardProductIDType,0) tintStandardProductIDType,
		ISNULL(vcExportToAPI,'') vcExportToAPI,
		ISNULL(numShipClass,0) numShipClass,ISNULL(I.bitAllowDropShip,0) AS bitAllowDropShip,
		ISNULL(@bitItemIsUsedInOrder,0) AS bitItemIsUsedInOrder,
		ISNULL((SELECT COUNT(*) FROM ItemDetails WHERE numChildItemID = I.numItemCode),0) AS intParentKitItemCount,
		ISNULL(I.bitArchiveItem,0) AS [bitArchiveItem],
		ISNULL(W.numWareHouseID,0) AS [numWareHouseID],
		ISNULL(W.numWareHouseItemID,0) AS [numWareHouseItemID],
		ISNULL((SELECT COUNT(WI.[numWareHouseItemID]) FROM [dbo].[WareHouseItems] WI WHERE WI.[numItemID] = I.numItemCode AND WI.[numItemID] = @numItemCode),0) AS [intTransCount],
		ISNULL(I.bitAsset,0) AS [bitAsset],
		ISNULL(I.bitRental,0) AS [bitRental],
		ISNULL(W.vcBarCode,ISNULL(I.[numBarCodeId],'')) AS [vcBarCode],
		ISNULL((SELECT COUNT(*) FROM ItemUOMConversion WHERE numItemCode=I.numItemCode),0) AS tintUOMConvCount,
		ISNULL(I.bitVirtualInventory,0) bitVirtualInventory ,
		ISNULL(I.bitContainer,0) bitContainer
		,ISNULL(I.numContainer,0) numContainer
		,ISNULL(I.numNoItemIntoContainer,0) numNoItemIntoContainer
		,ISNULL(I.bitMatrix,0) AS bitMatrix
		,ISNULL(@bitOppOrderExists,0) AS bitOppOrderExists
		,CPN.CustomerPartNo
		,ISNULL(I.vcASIN,'') AS vcASIN
		,ISNULL(I.tintKitAssemblyPriceBasedOn,1) AS tintKitAssemblyPriceBasedOn
		,ISNULL(I.fltReorderQty,0) AS fltReorderQty
		,ISNULL(bitSOWorkOrder,0) bitSOWorkOrder
		,ISNULL(bitKitSingleSelect,0) bitKitSingleSelect
		,ISNULL(I.numBusinessProcessId,0) numBusinessProcessId,
		ISNULL(I.bitTimeContractFromSalesOrder,0) AS bitTimeContractFromSalesOrder
		,ISNULL(I.bitAutomateReorderPoint,0) bitAutomateReorderPoint
	FROM 
		Item I       
	left join  
		WareHouseItems W                  
	on 
		W.numItemID=I.numItemCode                
	LEFT JOIN 
		ItemExtendedDetails IED 
	ON 
		I.numItemCode = IED.numItemCode
	LEFT JOIN 
		Vendor V 
	ON 
		I.numVendorID = V.numVendorID AND I.numItemCode = V.numItemCode  
	LEFT JOIN 
		CustomerPartNumber CPN 
	ON 
		I.numItemCode=CPN.numItemCode 
		AND CPN.numCompanyID=@numCompanyID
	WHERE 
		I.numItemCode=@numItemCode  
	GROUP BY 
		I.numItemCode, vcItemName, txtItemDesc, charItemType, monListPrice,                   
		numItemClassification, bitTaxable,vcSKU,[W].[vcWHSKU] , bitKitParent,V.numVendorID, V.monCost, I.numDomainID,               
		I.numCreatedBy, I.bintCreatedDate, I.bintModifiedDate,numContainer,   numNoItemIntoContainer,                
		I.numModifiedBy,bitAllowBackOrder, bitSerialized, vcModelID,                   
		numItemGroup, numCOGsChartAcntId, numAssetChartAcntId, numIncomeChartAcntId, monAverageCost, I.bitVirtualInventory, bitExpenseItem, numExpenseChartAcntId,                  
		monCampaignLabourCost,fltWeight,fltHeight,fltWidth,fltLength,bitFreeShipping,vcUnitofMeasure,bitShowDeptItemDesc,bitShowDeptItem,bitCalAmtBasedonDepItems,bitAssembly,numBarCodeId,I.vcManufacturer
		,I.numBaseUnit,I.numPurchaseUnit,I.numSaleUnit,I.bitContainer,I.bitLotNo,I.IsArchieve,I.numItemClass,I.tintStandardProductIDType,I.vcExportToAPI,I.numShipClass,
		CAST( ISNULL(IED.txtDesc,'')  AS VARCHAR(MAX) ),I.bitAllowDropShip,I.bitArchiveItem,I.bitAsset,I.bitRental,W.[numWarehouseID],W.numWareHouseItemID,W.[vcBarCode],I.bitMatrix
		,I.numManufacturer,CPN.CustomerPartNo,I.vcASIN,I.tintKitAssemblyPriceBasedOn,I.fltReorderQty,bitSOWorkOrder,bitKitSingleSelect
		,I.numBusinessProcessId,I.bitTimeContractFromSalesOrder,I.bitAutomateReorderPoint
END