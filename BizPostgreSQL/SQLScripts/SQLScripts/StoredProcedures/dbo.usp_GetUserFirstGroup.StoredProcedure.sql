/****** Object:  StoredProcedure [dbo].[usp_GetUserFirstGroup]    Script Date: 07/26/2008 16:18:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getuserfirstgroup')
DROP PROCEDURE usp_getuserfirstgroup
GO
CREATE PROCEDURE [dbo].[usp_GetUserFirstGroup]  
 @numUserID numeric,  
 @vcGrpName varchar(50)   --
AS  
--This procedure will return the first GroupID for a user.  
BEGIN  
 SELECT numGrpID FROM Groups   
  WHERE vcGrpName=@vcGrpName  
  order by vcGrpName DESC  
END
GO
