/****** Object:  StoredProcedure [dbo].[USP_CheckEmailWeb]    Script Date: 07/26/2008 16:15:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_checkemailweb')
DROP PROCEDURE usp_checkemailweb
GO
CREATE PROCEDURE [dbo].[USP_CheckEmailWeb]          
 @vcEmail as varchar(50)='',      
@vcWebsite as varchar(100)  ='',      
 @numDomainId As Numeric(9)        
as          
          
          
select top 1 vcemail from AdditionalContactsInformation   where vcemail = @vcEmail          
and numDomainId = case when @vcEmail='' then 0 else @numDomainId      end  
      
      
      
select top 1 vcWebsite from companyInfo where vcWebsite like'%'+ @vcWebsite+'%'   
and numDomainId = case when @vcWebsite='' then -1 else @numDomainId      end
GO
