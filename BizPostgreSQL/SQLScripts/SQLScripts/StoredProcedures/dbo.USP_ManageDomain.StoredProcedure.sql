/****** Object:  StoredProcedure [dbo].[USP_ManageDomain]    Script Date: 07/26/2008 16:19:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--create by anoop jayaraj  
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_managedomain')
DROP PROCEDURE usp_managedomain
GO
CREATE PROCEDURE [dbo].[USP_ManageDomain]  
@byteMode as tinyint=0,  
@vcDomainName as varchar(50)='',  
@numDomainId as numeric(9)=0  
as  
  
if @byteMode=0  
begin  
declare @numDivisionID as numeric(9)

insert into Domain (vcDomainName)  
values (@vcDomainName)  
set @numDomainId=@@identity 
insert into CompanyInfo(vcCompanyName,bintCreatedDate,numDomainID)
values(@vcDomainName,getutcdate(),@numDomainId)
insert into DivisionMaster(numCompanyID,vcDivisionName,bintCreatedDate,tintCRMType,numDomainID,numStatusID,bitLeadBoxFlg,numGrpId)
values(@@identity,'-',getutcdate(),2,@numDomainId,2,0,0)
select @numDivisionID=@@identity
insert into AdditionalContactsInformation(vcGivenName,vcFirstName,vcLastName,numDivisionId,numContactType,bintCreatedDate,numEmpStatus,numDomainID,bitPrimaryContact)
values('Administrator','Administrator','-',@numDivisionID,0,getutcdate(),658,@numDomainId,1)
update Domain set numDivisionID=@numDivisionID where numdomainID=@numDomainId
select @numDomainId

 
end  
  
if @byteMode=1  
begin  
update Domain set vcDomainName=@vcDomainName  
where numDomainId=@numDomainId  
select @numDomainId  
end  
  
if @byteMode=2  
begin  
select numDomainId,vcDomainName from domain  
end
GO
