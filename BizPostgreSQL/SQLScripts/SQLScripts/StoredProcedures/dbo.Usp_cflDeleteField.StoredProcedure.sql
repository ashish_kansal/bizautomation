/****** Object:  StoredProcedure [dbo].[Usp_cflDeleteField]    Script Date: 07/26/2008 16:15:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--exec Usp_cflDeleteField 2  
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_cfldeletefield')
DROP PROCEDURE usp_cfldeletefield
GO
CREATE PROCEDURE [dbo].[Usp_cflDeleteField]      
@fld_id as numeric(9)=NULL,
@numDomainID AS NUMERIC
as      
      
IF @numDomainID >0
BEGIN

	IF(EXISTS(SELECT 1 FROM eCommerceDTL WHERE numDomainId = @numDomainID AND bitElasticSearch = 1))
	BEGIN
		
		IF (EXISTS (SELECT 1 FROM CFW_Fld_Master where fld_id=@fld_id AND Grp_id IN (5,9)))
		BEGIN
			INSERT INTO ElasticSearchBizCart (numDomainID,vcValue,vcAction,vcModule)
			SELECT @numDomainID,
			CASE 
				WHEN Grp_id=5 THEN REPLACE(Fld_label,' ','') + '_C'
				WHEN Grp_id=9 THEN REPLACE(Fld_label,' ','') + '_' + CAST(Fld_Id AS VARCHAR)
			END
			,'Delete'
			,'CustomField'
			FROM CFW_Fld_Master where fld_id=@fld_id AND Grp_id IN (5,9)
		END
	END


	DELETE FROM DycFormConfigurationDetails WHERE bitCustom =1 AND numFieldID = @fld_id AND numDomainId=@numDomainID -- Bug id 278 permenant fix 

	delete from listdetails where numlistID=(select numlistID from  CFW_Fld_Master where fld_id=@fld_id)    
	delete from listmaster where numListID=(select numlistID from  CFW_Fld_Master where fld_id=@fld_id)
	delete from CFW_Fld_Values_Product where Fld_ID=@fld_id   
	delete from CFW_FLD_Values where Fld_ID=@fld_id    
	delete from CFW_FLD_Values_Case where fld_id=@fld_id    
	delete from CFW_FLD_Values_Cont where fld_id=@fld_id    
	delete from CFW_FLD_Values_Item where fld_id=@fld_id      
	delete from CFW_Fld_Values_Opp where fld_id=@fld_id 
	DELETE FROM [dbo].[CFW_Fld_Values_OppItems] where fld_id=@fld_id
	DELETE FROM ItemAttributes WHERE FLD_ID=@fld_id
	
	DELETE FROM dbo.CFW_Validation WHERE numFieldID = @fld_id 
	delete from CFW_Fld_Dtl where numFieldId=@fld_id     
	delete from CFW_Fld_Master where fld_id=@fld_id
	
				

END       
GO
