/****** Object:  StoredProcedure [dbo].[USP_ManageTeamForRept]    Script Date: 07/26/2008 16:20:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
---created by anoop jayaraj
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_manageteamforrept')
DROP PROCEDURE usp_manageteamforrept
GO
CREATE PROCEDURE [dbo].[USP_ManageTeamForRept]  
@numUserCntId as numeric(9),  
@numDomainId as numeric(9),  
@tintType as tinyint,  
@strTeam as varchar(4000)  
as  
  
  
declare @separator_position as integer  
declare @strPosition as varchar(1000)  
  
delete from ForReportsByTeam where numUserCntID = @numUserCntId  
and numDomainId=@numDomainID and tintType=@tintType  
   
  
   while patindex('%,%' , @strTeam) <> 0  
    begin -- Begin for While Loop  
      select @separator_position =  patindex('%,%' , @strTeam)  
    select @strPosition = left(@strTeam, @separator_position - 1)  
       select @strTeam = stuff(@strTeam, 1, @separator_position,'')  
     insert into ForReportsByTeam (numUserCntID,numTeam,numDomainID,tintType)  
     values (@numUserCntId,@strPosition,@numDomainID,@tintType)  
   
    
   end
GO
