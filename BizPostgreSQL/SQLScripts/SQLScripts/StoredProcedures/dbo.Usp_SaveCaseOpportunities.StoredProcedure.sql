/****** Object:  StoredProcedure [dbo].[Usp_SaveCaseOpportunities]    Script Date: 07/26/2008 16:20:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_savecaseopportunities')
DROP PROCEDURE usp_savecaseopportunities
GO
CREATE PROCEDURE [dbo].[Usp_SaveCaseOpportunities]
    @numCaseId NUMERIC,
    @numDomainId NUMERIC,
    @strOppid VARCHAR(4000),
    @tintMode TINYINT = 0
AS 
    DECLARE @hDocItem INT
    
    --delete and insert
    IF @tintMode = 0 
        BEGIN
            DELETE  CaseOpportunities
            WHERE   numCaseId = @numCaseId
                    AND numDomainid = @numdomainid    
    
    
            IF @strOppid <> '' 
                BEGIN 
    
                    
                    IF CONVERT(VARCHAR(10), @strOppid) <> '' 
                        BEGIN
                            EXEC sp_xml_preparedocument @hDocItem OUTPUT,
                                @strOppid
                            INSERT  INTO [CaseOpportunities]
                                    (
                                      [numCaseId],
                                      [numOppId],
                                      [numDomainId],
                                      [numOppItemID]
		                        )
                                    SELECT  @numCaseId,
                                            X.numOppId,
                                            @numDomainId,
                                            X.numOppItemID
                                    FROM    ( SELECT    *
                                              FROM      OPENXML (@hDocItem, '/NewDataSet/Table1', 2)
                                                        WITH ( numOppId NUMERIC(9), numOppItemID NUMERIC(9) )
                                            ) X
                            EXEC sp_xml_removedocument @hDocItem
                        END

                END
	
        END
        --Delete selected 
    IF @tintMode = 1 
        BEGIN
            DELETE  FROM [CaseOpportunities]
            WHERE   [numCaseId] = @numCaseId
                    AND [numDomainId] = @numDomainId
                    AND [numOppItemID] IN (
                    SELECT  Id
                    FROM    dbo.[SplitIDs](@strOppId, ',') )
        END
  
		--Add selected , only insert
    IF @tintMode = 2 
        BEGIN
            IF @strOppid <> '' 
                BEGIN 
                    IF CONVERT(VARCHAR(10), @strOppid) <> '' 
                        BEGIN
                            EXEC sp_xml_preparedocument @hDocItem OUTPUT,
                                @strOppid
                            INSERT  INTO [CaseOpportunities]
                                    (
                                      [numCaseId],
                                      [numOppId],
                                      [numDomainId],
                                      [numOppItemID]
		                        )
                                    SELECT  @numCaseId,
                                            X.numOppId,
                                            @numDomainId,
                                            X.numOppItemID
                                    FROM    ( SELECT    *
                                              FROM      OPENXML (@hDocItem, '/NewDataSet/Table1', 2)
                                                        WITH ( numOppId NUMERIC(9), numOppItemID NUMERIC(9) )
                                            ) X
                            EXEC sp_xml_removedocument @hDocItem
                        END

                END
			
        END
  