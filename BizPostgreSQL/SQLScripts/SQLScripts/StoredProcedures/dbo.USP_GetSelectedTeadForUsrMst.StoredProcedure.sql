/****** Object:  StoredProcedure [dbo].[USP_GetSelectedTeadForUsrMst]    Script Date: 07/26/2008 16:18:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getselectedteadforusrmst')
DROP PROCEDURE usp_getselectedteadforusrmst
GO
CREATE PROCEDURE [dbo].[USP_GetSelectedTeadForUsrMst]
@numUserCntId as numeric(9),
@numDomainID as numeric(9)
as
select numlistItemId,vcdata from UserTeams
join listdetails
on numListItemID =numTeam
where numuserCntid=@numUserCntId
and UserTeams.numDomainID=@numDomainID
GO
