
/****** Object:  StoredProcedure [dbo].[USP_GetSalesFulfillment]    Script Date: 05/15/2009 08:05:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
declare @p8 int
set @p8=63
exec USP_GetSalesFulfillment @numUserCntID=17,@numDomainID=72,@tintSortOrder=1,@SortChar='0',@CustName='',@CurrentPage=1,@PageSize=30,@TotRecs=@p8 
output,@columnName='opp.bintCreatedDate',@columnSortOrder='Desc',@ClientTimeZoneOffset=-330
select @p8 
*/
--created by anoop jayaraj                                              
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getsalesfulfillment')
DROP PROCEDURE usp_getsalesfulfillment
GO
CREATE PROCEDURE [dbo].[USP_GetSalesFulfillment] 
 @numUserCntID numeric(9)=0,                                                              
 @numDomainID numeric(9)=0,                                                              
 @tintSortOrder tinyint=0,                                                            
 @SortChar char(1)='0',                                                                                                                           
 @CustName varchar(100)='' ,                                                                    
 @CurrentPage int,                                                              
 @PageSize int,                                                              
 @TotRecs int output,                                                              
 @columnName as Varchar(50),                                                              
 @columnSortOrder as Varchar(10),                                                                                     
 @ClientTimeZoneOffset as INT,
 @numOrderStatus NUMERIC(9)                 
as                                              
                                                            
Create table #tempTable ( ID INT IDENTITY PRIMARY KEY, 
    numOppID numeric(9),
    numoppitemtCode numeric(9),
    Fulfilled numeric(9),
    QtyToFulfill numeric(9),
    bintCreatedDate datetime,
    Allocation FLOAT,
    OnHand FLOAT,
    numWareHouseItemID numeric(9),
    AmtPaid DECIMAL(20,5),bitSerialized BIT,bitLotNo BIT,numShipVia NUMERIC(9),QtyShipped numeric(9),bitKitParent bit
--    numOppBizDocsId numeric(9)
    )                                              
                                              
            
      
                                                             
declare @strSql as varchar(8000)                                                              
set @strSql='Select numOppID,numoppitemtCode,Fulfilled,numUnitHour- Fulfilled as QtyToFulfill,bintCreatedDate,numAllocation as Allocation,numOnHand,numWareHouseItemID,AmtPaid,bitSerialized,bitLotNo,numShipVia,QtyShipped,bitKitParent from (
   SELECT  Opp.numOppID,OppItems.numoppitemtCode,Opp.bintCreatedDate,isnull(sum(OppBizItems.numUnitHour),0) as Fulfilled,isnull(SUM(OppBiz.monAmountPaid),0) AS AmtPaid,
  OppItems.numUnitHour  ,numAllocation ,numOnHand,numWareHouseItemID,isnull(I.bitSerialized,0) as bitSerialized,isnull(I.bitLotNo,0) as bitLotNo,OppBiz.numShipVia,
  Case when Opp.tintShipped=1 then isnull(sum(OppItems.numUnitHour),0) else isnull(sum(OppItems.numQtyShipped),0) end as QtyShipped,
  CASE WHEN ISNULL(bitKitParent,0) = 1
                               AND ISNULL(bitAssembly,0) = 1 THEN 0
                          WHEN ISNULL(bitKitParent,0) = 1 THEN 1
                          ELSE 0
                     END as bitKitParent
  FROM OpportunityMaster Opp 
  INNER JOIN AdditionalContactsInformation ADC                                                               
  ON Opp.numContactId = ADC.numContactId                                                               
  INNER JOIN DivisionMaster Div                                                               
  ON Opp.numDivisionId = Div.numDivisionID AND ADC.numDivisionId = Div.numDivisionID                                                               
  INNER JOIN CompanyInfo C                                                               
  ON Div.numCompanyID = C.numCompanyId                                  
  left join AdditionalContactsInformation ADC1 
  on ADC1.numContactId=Opp.numRecOwner
   left join OpportunityBizDocs OppBiz 
  on Opp.numOppID=OppBiz.numOppID and OppBiz.numBizDocId=(select numAuthoritativeSales from AuthoritativeBizDocs where numDomainID=Div.numDomainID ) 
  Join  OpportunityItems OppItems
  on   OppItems.numOppID= Opp.numOppID
  left Join  OpportunityBizDocItems OppBizItems
  on  OppBizItems.numOppItemID= OppItems.numoppitemtCode and OppBiz.numOppBizDocsId= OppBizItems.numOppBizDocID                                                   
  left join WareHouseItems WI
  on WI.numWareHouseItemID=OppItems.numWarehouseItmsID 
  LEFT JOIN [Item] I ON I.[numItemCode] = OppItems.[numItemCode]
 WHERE Opp.tintOppstatus=1 and  Opp.tintOppType=1 and (WI.numWareHouseID in (select numWarehouseID from WareHouseForSalesFulfillment where numDomainID='+ convert(varchar(15),@numDomainID) +') or WI.numWareHouseID is null) 
  AND I.[charItemType]=''P'' and (OppItems.bitDropShip=0 or OppItems.bitDropShip is null)
  and Div.numDomainID=' + convert(varchar(15),@numDomainID)+''                       
  
if @tintSortOrder != 4  
	SET @strSql =@strSql + ' and Opp.tintShipped=0 '

                                  
if @CustName<>'' set @strSql=@strSql+'and C.vcCompanyName like '''+@CustName+'%'''
if @numOrderStatus<>0 set @strSql=@strSql+'and Opp.numStatus ='+ CONVERT(VARCHAR(10),@numOrderStatus)
                                                             
if @SortChar<>'0' set @strSql=@strSql + ' And Opp.vcPOppName like '''+@SortChar+'%'''                                                      


    
--if @columnName = 'OppBiz.dtCreatedDate'                                                                               
-- set @strSql=@strSql + ' ORDER BY  ' + @columnName +' '+ @columnSortOrder                                                                             
 set @strSql=@strSql + ' group by  numWareHouseItemID,numAllocation,numOnHand,Opp.bintCreatedDate,OppItems.numUnitHour,Opp.numOppID,OppItems.numoppitemtCode,I.bitSerialized,I.bitLotNo,OppBiz.numShipVia,OppItems.numQtyShipped,Opp.tintShipped,I.bitKitParent,I.bitAssembly)X where '

if @tintSortOrder=3 
BEGIN
   SET @strSql=@strSql + '  X.QtyShipped = 0'
END
ELSE if @tintSortOrder=4 
BEGIN
   SET @strSql=@strSql + '  X.QtyShipped > 0'
END
else 
BEGIN
DECLARE @strOperator VARCHAR(5)
 if @tintSortOrder=1  
	set @strOperator = '='
 ELSE 
	set @strOperator = '>' 

set @strSql=@strSql + ' X.numUnitHour-X.Fulfilled'+ @strOperator +'0' 
END

-- OLD filters
-- if @tintSortOrder=1  set @strSql=@strSql + '  AND isnull(numAllocation,0)>=numUnitHour- Fulfilled'        
-- if @tintSortOrder=2  set @strSql=@strSql + '  AND numWareHouseItemID>0' 
-- if @tintSortOrder=3  set @strSql=@strSql + '  AND isnull(numAllocation,0)<numUnitHour- Fulfilled'        
-- if @tintSortOrder=4  set @strSql=@strSql + '  AND ( (isnull(numUnitHour,0)< isnull(numAllocation,0)) or (isnull(numUnitHour,0)< isnull(numOnHand,0)) ) '

 if @tintSortOrder=2  set @strSql=@strSql + '  AND X.Fulfilled >0' 
 if @tintSortOrder=4 or @tintSortOrder=3  set @strSql=@strSql + '  AND (SELECT COUNT(*) FROM [OpportunityBizDocItems] WHERE [numOppItemID]=X.numoppitemtCode)=0'
--Sort Newest to oldest
	--set @strSql=@strSql + ' Order by bintCreatedDate desc'
       
print @strSql   
                                                         
insert into #tempTable(numOppId,numoppitemtCode,Fulfilled ,QtyToFulfill,bintCreatedDate,Allocation,OnHand,numWareHouseItemID,AmtPaid,bitSerialized,bitLotNo/*,numOppBizDocsId*/,numShipVia,QtyShipped,bitKitParent)
exec (@strSql)                                                               
                    
                  
                  
                                                            
declare @firstRec as integer                                                
declare @lastRec as integer                                                              
set @firstRec= (@CurrentPage-1) * @PageSize                                                              
set @lastRec= (@CurrentPage*@PageSize+1)                                           
set @TotRecs=(select count(*) from #tempTable)                                                
                    
declare @tintOrder as tinyint                                                          
declare @vcFieldName as varchar(50)             
declare @vcListItemType as varchar(3)                                                     
declare @vcListItemType1 as varchar(1)                                                         
declare @vcAssociatedControlType varchar(10)                                                          
declare @numListID AS numeric(9)                                                          
declare @vcDbColumnName varchar(40)                              
declare @WhereCondition varchar(2000)                               
declare @vcLookBackTableName varchar(2000)                        
 Declare @bitCustom as bit                
Declare @numFieldId as numeric  
DECLARE @bitAllowSorting AS CHAR(1)   
Declare @bitAllowEdit as bit
 Declare @ListRelID as numeric(9) 

set @tintOrder=0                                                          
set @WhereCondition =''                         
                           
declare @Nocolumns as tinyint                        
set @Nocolumns=0               

select @Nocolumns=isnull(sum(TotalRow),0)  from(            
Select count(*) TotalRow from View_DynamicColumns where numFormId=23 and numUserCntID=@numUserCntID and numDomainID=@numDomainID AND tintPageType=1
Union 
Select count(*) TotalRow from View_DynamicCustomColumns where numFormId=23 and numUserCntID=@numUserCntID and numDomainID=@numDomainID AND tintPageType=1) TotalRows

declare @DefaultNocolumns as tinyint                     
set @DefaultNocolumns=  @Nocolumns                       
declare @strColumns as varchar(2000)                        
set @strColumns=''    

CREATE TABLE #tempForm (tintOrder TINYINT,vcDbColumnName nvarchar(50),vcFieldName nvarchar(50),vcAssociatedControlType nvarchar(50),
  vcListItemType char(3),numListID numeric(9),vcLookBackTableName varchar(50),bitCustomField BIT,
  numFieldId NUMERIC,bitAllowSorting BIT,bitAllowEdit BIT,bitIsRequired BIT,bitIsEmail BIT,bitIsAlphaNumeric BIT,bitIsNumeric BIT,
  bitIsLengthValidation BIT,intMaxLength int,intMinLength int,bitFieldMessage BIT,vcFieldMessage VARCHAR(500),ListRelID numeric(9))
                     
while @DefaultNocolumns>0                        
begin                        
                        
 set @strColumns=@strColumns+',null'                        
 set @DefaultNocolumns=@DefaultNocolumns-1                        
end                  
 set @strSql = ''                    
set @strSql=' select ADC.numContactId,DM.numDivisionID,isnull(DM.numTerID,0) numTerID,opp.numRecOwner,DM.tintCRMType,opp.numOppId,OppItems.numoppitemtCode,Case when Opp.tintShipped=1 then isnull(OppItems.numUnitHour,0) else isnull(OppItems.numQtyShipped,0) end as numQtyShipped,Fulfilled,isnull(WI.numWareHouseItemID,0) numWareHouseItemID,T.bitSerialized,T.bitLotNo,Item.numItemCode,cast(OppItems.numUnitHour as numeric(9)) as QtyOrdered,T.bitKitParent'
     
if @Nocolumns > 0                    
begin  
INSERT INTO #tempForm
select tintRow+1 as tintOrder,vcDbColumnName,ISNULL(vcCultureFieldName,vcFieldName),                  
 vcAssociatedControlType,vcListItemType,numListID,vcLookBackTableName                                                 
,bitCustom,numFieldId,bitAllowSorting,bitAllowEdit,
bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage vcFieldMessage,ListRelID 
 FROM View_DynamicColumns 
 where numFormId=23 and numUserCntID=@numUserCntID and numDomainID=@numDomainID AND tintPageType=1
 
 
       UNION
    
     select tintRow+1 as tintOrder,vcDbColumnName,vcFieldName,                  
 vcAssociatedControlType,'' as vcListItemType,numListID,''                                                 
,bitCustom,numFieldId,bitAllowSorting,bitAllowEdit,
bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage,ListRelID
 from View_DynamicCustomColumns
 where numFormId=23 and numUserCntID=@numUserCntID and numDomainID=@numDomainID AND tintPageType=1
 AND ISNULL(bitCustom,0)=1
 
  order by tintOrder asc     
                         
end                    
else                    
begin                    
 
INSERT INTO #tempForm
select tintOrder,vcDbColumnName,ISNULL(vcCultureFieldName,vcFieldName),                  
 vcAssociatedControlType,vcListItemType,numListID,vcLookBackTableName                                                 
,bitCustom,numFieldId,bitAllowSorting,bitAllowEdit,
bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage vcFieldMessage,ListRelID 
 FROM View_DynamicDefaultColumns
 where numFormId=23 and bitDefault=1 AND ISNULL(bitSettingField,0)=1 and numDomainID=@numDomainID
order by tintOrder asc   
end                                                    
                    set @DefaultNocolumns=  @Nocolumns                      
        

    select top 1 @tintOrder=tintOrder+1,@vcDbColumnName=vcDbColumnName,@vcFieldName=vcFieldName,                
  @vcAssociatedControlType=vcAssociatedControlType,@vcListItemType=vcListItemType,@numListID=numListID,@vcLookBackTableName=vcLookBackTableName                                               
,@bitCustom=bitCustomField,@numFieldId=numFieldId,@bitAllowSorting=bitAllowSorting,@bitAllowEdit=bitAllowEdit,@ListRelID=ListRelID
  from  #tempForm order by tintOrder asc            
                                              
while @tintOrder>0                                                          
begin     

  print @tintOrder                                                      
     if @bitCustom = 0              
 begin                                                     
                declare @Prefix as varchar(5)                        
       if @vcLookBackTableName = 'AdditionalContactsInformation'  set @Prefix = 'ADC.'                        
       if @vcLookBackTableName = 'DivisionMaster'  set @Prefix = 'DM.'                        
       if @vcLookBackTableName = 'OpportunityMaster' set @PreFix ='Opp.'                                               
	   if @vcLookBackTableName = 'Warehouses' set @PreFix ='W.'                                               

 if @vcAssociatedControlType='SelectBox'                                                          
        begin                                                          
                                                                          
     if @vcListItemType='LI'              
     begin                                                          
      set @strSql=@strSql+',L'+ convert(varchar(3),@tintOrder)+'.vcData'+' ['+ @vcFieldName+'~'+ @vcDbColumnName+'~'+ @bitAllowSorting+']'                                                          
      set @WhereCondition= @WhereCondition +' left Join ListDetails L'+ convert(varchar(3),@tintOrder)+ ' on L'+convert(varchar(3),@tintOrder)+ '.numListItemID='+@vcDbColumnName                                                          
     end                                                          
     else if @vcListItemType='S'                                                           
     begin                                                          
      set @strSql=@strSql+',S'+ convert(varchar(3),@tintOrder)+'.vcState'+' ['+ @vcFieldName+'~'+ @vcDbColumnName+'~'+ @bitAllowSorting+']'                                                          
      set @WhereCondition= @WhereCondition +' left join State S'+ convert(varchar(3),@tintOrder)+ ' on S'+convert(varchar(3),@tintOrder)+ '.numStateID='+@vcDbColumnName                                                          
     end                                                          
     else if @vcListItemType='T'                                                           
     begin                                                          
      set @strSql=@strSql+',L'+ convert(varchar(3),@tintOrder)+'.vcData'+' ['+ @vcFieldName+'~'+ @vcDbColumnName+'~'+ @bitAllowSorting+']'                                                          
      set @WhereCondition= @WhereCondition +' left Join ListDetails L'+ convert(varchar(3),@tintOrder)+ ' on L'+convert(varchar(3),@tintOrder)+ '.numListItemID='+@vcDbColumnName                                                          
     end                         
     else if   @vcListItemType='U'                                                       
    begin                         
                        
                         
    set @strSql=@strSql+',dbo.fn_GetContactName('+@Prefix+@vcDbColumnName+') ['+ @vcFieldName+'~'+ @vcDbColumnName+'~'+ @bitAllowSorting+']'                                                          
    end                        
    end                  
    else if @vcAssociatedControlType='DateField'                                                        
   begin                  
     set @strSql=@strSql+',case when convert(varchar(11),'+@Prefix+@vcDbColumnName+')= convert(varchar(11),getdate()) then ''<b><font color=red>Today</font></b>'''          
     set @strSql=@strSql+'when convert(varchar(11),'+@Prefix+@vcDbColumnName+')= convert(varchar(11),dateadd(day,-1,getdate())) then ''<b><font color=purple>YesterDay</font></b>'''          
     set @strSql=@strSql+'when convert(varchar(11),'+@Prefix+@vcDbColumnName+')= convert(varchar(11),dateadd(day,1,getdate())) then''<b><font color=orange>Tommorow</font></b>'' '          
     set @strSql=@strSql+'else dbo.FormatedDateFromDate('+@Prefix+@vcDbColumnName+','+convert(varchar(10),@numDomainId)+') end  ['+ @vcFieldName+'~'+ @vcDbColumnName+'~'+ @bitAllowSorting+']'                   
         
   end                
    else if @vcAssociatedControlType='TextBox'                                                        
   begin                 
    set @strSql=@strSql+','+ case                      
    when @vcDbColumnName='numAge' then 'year(getutcdate())-year(bintDOB)'               
    when @vcDbColumnName='monPAmount' then 'isnull((SELECT varCurrSymbol FROM [Currency] WHERE numCurrencyID = Opp.[numCurrencyID]),'''') + '' ''+ CONVERT(VARCHAR,CONVERT(DECIMAL(20,5),monPAmount),1)'
    when @vcDbColumnName='numDivisionID' then 'DM.numDivisionID'
    WHEN @vcDbColumnName='OrderAmt' then 'monPAmount'
    WHEN @vcDbColumnName='AmtPaid' then 'AmtPaid'
    WHEN @vcDbColumnName='vcItemName' then 'OppItems.vcItemName'
    WHEN @vcDbColumnName='vcModelID' then 'OppItems.vcModelID'
    WHEN @vcDbColumnName='vcItemDesc' then 'OppItems.vcItemDesc'
	WHEN @vcDbColumnName='vcPOppName' then 'Opp.vcPOppName + '' ('' + dbo.fn_GetListItemName(isnull(Opp.numStatus,0)) + '')'' '
	WHEN @vcDbColumnName='numInTransit' then 'dbo.fn_GetItemTransitCount(Item.numItemCode,Opp.numDomainID)'
    WHEN @vcDbColumnName='numQtyShipped' then 'Case when Opp.tintShipped=1 then isnull(OppItems.numUnitHour,0) else isnull(OppItems.numQtyShipped,0) end'
    
    WHEN @vcDbColumnName='numOnHand' then 'Case when T.bitKitParent=1 then dbo.fn_GetKitInventory(OppItems.numItemCode,WI.numWarehouseID) else isnull(WI.numOnHand,0) END '
    WHEN @vcDbColumnName='numOnOrder' then 'Case when T.bitKitParent=1 then 0 else isnull(WI.numOnOrder,0) END '
    WHEN @vcDbColumnName='numAllocation' then 'Case when T.bitKitParent=1 then 0 else isnull(WI.numAllocation,0) END '
    WHEN @vcDbColumnName='numBackOrder' then 'Case when T.bitKitParent=1 then 0 else isnull(WI.numBackOrder,0) END '
    
    else @vcDbColumnName end+' ['+ @vcFieldName+'~'+ @vcDbColumnName+'~'+ @bitAllowSorting+']'                  
   end  
   else if  @vcAssociatedControlType='TextArea'                                              
   begin  
		set @strSql=@strSql+', '''' ' +' ['+ @vcFieldName+'~'+ @vcDbColumnName+'~'+ @bitAllowSorting+']'            
   end        
      

                                                      
  end              
Else                                                          
 Begin              
                 
   select @vcFieldName=FLd_label,@vcAssociatedControlType=fld_type,@vcDbColumnName='Cust'+Convert(varchar(10),Fld_Id)  from CFW_Fld_Master                                               
   where  CFW_Fld_Master.Fld_Id = @numFieldId                                          
                            
   if @vcAssociatedControlType = 'TextBox' or @vcAssociatedControlType = 'TextArea'            
   begin          
             
    set @strSql= @strSql+',CFW'+ convert(varchar(3),@tintOrder)+'.Fld_Value  ['+ @vcFieldName+'~'+ @vcDbColumnName+'~0]'                 
    set @WhereCondition= @WhereCondition +' left Join CFW_FLD_Values_Opp CFW'+ convert(varchar(3),@tintOrder)+ '               
    on CFW'+ convert(varchar(3),@tintOrder)+ '.Fld_Id='+convert(varchar(10),@numFieldId) +'and CFW'+ convert(varchar(3),@tintOrder)+ '.RecId=Opp.numOppid   '                                                       
   end              
                 
    else if @vcAssociatedControlType = 'CheckBox'             
   begin          
             
    set @strSql= @strSql+',case when isnull(CFW'+ convert(varchar(3),@tintOrder)+'.Fld_Value,0)=0 then ''No'' when isnull(CFW'+ convert(varchar(3),@tintOrder)+'.Fld_Value,0)=1 then ''Yes'' end   ['+ @vcFieldName+'~'+ @vcDbColumnName+'~0]'             
    set @WhereCondition= @WhereCondition +' left Join CFW_FLD_Values_Opp CFW'+ convert(varchar(3),@tintOrder)+ '           
    on CFW'+ convert(varchar(3),@tintOrder)+ '.Fld_Id='+convert(varchar(10),@numFieldId) +'and CFW'+ convert(varchar(3),@tintOrder)+ '.RecId=Opp.numOppid   '                                                   
   end          
   else if @vcAssociatedControlType = 'DateField'          
   begin               
                 
    set @strSql= @strSql+',dbo.FormatedDateFromDate(CFW'+ convert(varchar(3),@tintOrder)+'.Fld_Value,'+convert(varchar(10),@numDomainId)+')  ['+ @vcFieldName+'~'+ @vcDbColumnName+'~0]'                 
    set @WhereCondition= @WhereCondition +' left Join CFW_FLD_Values_Opp CFW'+ convert(varchar(3),@tintOrder)+ '               
    on CFW'+ convert(varchar(3),@tintOrder)+ '.Fld_Id='+convert(varchar(10),@numFieldId) +'and CFW'+ convert(varchar(3),@tintOrder)+ '.RecId=Opp.numOppid    '                                                       
   end              
    else if @vcAssociatedControlType = 'SelectBox'                
   begin            
    set @vcDbColumnName = 'DCust'+Convert(varchar(10),@numFieldId)              
    set @strSql=@strSql+',L'+ convert(varchar(3),@tintOrder)+'.vcData'+' ['+ @vcFieldName+'~'+ @vcDbColumnName+'~0]'                                             
    set @WhereCondition= @WhereCondition +' left Join CFW_FLD_Values_Opp CFW'+ convert(varchar(3),@tintOrder)+ '               
     on CFW'+ convert(varchar(3),@tintOrder)+ '.Fld_Id='+convert(varchar(10),@numFieldId) +'and CFW'+ convert(varchar(3),@tintOrder)+ '.RecId=Opp.numOppid     '                                                       
    set @WhereCondition= @WhereCondition +' left Join ListDetails L'+ convert(varchar(3),@tintOrder)+ ' on L'+convert(varchar(3),@tintOrder)+ '.numListItemID=CFW'+ convert(varchar(3),@tintOrder)+'.Fld_Value'              
   end                 
 End                                        
                           
     select top 1 @tintOrder=tintOrder+1,@vcDbColumnName=vcDbColumnName,@vcFieldName=vcFieldName,                
  @vcAssociatedControlType=vcAssociatedControlType,@vcListItemType=vcListItemType,@numListID=numListID,@vcLookBackTableName=vcLookBackTableName                                               
,@bitCustom=bitCustomField,@numFieldId=numFieldId,@bitAllowSorting=bitAllowSorting,@bitAllowEdit=bitAllowEdit,@ListRelID=ListRelID
  from #tempForm WHERE tintOrder > @tintOrder-1 order by tintOrder asc            
 
  if @@rowcount=0 set @tintOrder=0 
                
end                               
                          
   IF @columnName='bintCreatedDate'
	SET @columnName='opp.bintCreatedDate'                        
                  
                                            
 set @strSql=@strSql+'                                                            
  FROM OpportunityMaster Opp     
  INNER JOIN AdditionalContactsInformation ADC  ON Opp.numContactId = ADC.numContactId                                                               
  INNER JOIN DivisionMaster DM  ON Opp.numDivisionId = DM.numDivisionID AND ADC.numDivisionId = DM.numDivisionID                                                               
   join #tempTable T on T.numOppId=Opp.numOppId                                                          
  INNER JOIN CompanyInfo C                                                               
  ON DM.numCompanyID = C.numCompanyId '+@WhereCondition+  '                       
  Join OpportunityItems OppItems
  on   OppItems.numoppitemtCode=T.numoppitemtCode
  left join WareHouseItems WI
  on WI.numWareHouseItemID=OppItems.numWarehouseItmsID 
  left join Warehouses W on W.numWareHouseID=WI.numWareHouseID
  Join Item
  on Item.numItemCode= OppItems.numItemCode                                       
  WHERE ID > '+convert(varchar(10),@firstRec)+ ' and ID <'+ convert(varchar(10),@lastRec)+' and opp.numOppId not in (SELECT numOppId FROM WorkOrder WHERE numDomainID='+ convert(varchar(15),@numDomainID) +' AND numWOStatus!=23184 AND numOppId IS NOT null) '
set @strSql=@strSql + ' ORDER BY  ' + @columnName +' '+ @columnSortOrder                   
                    
print @strSql                  
exec (@strSql)                                            
                          
drop table #tempTable
