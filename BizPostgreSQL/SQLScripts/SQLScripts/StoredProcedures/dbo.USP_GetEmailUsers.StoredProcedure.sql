/****** Object:  StoredProcedure [dbo].[USP_GetEmailUsers]    Script Date: 07/26/2008 16:17:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj        
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getemailusers')
DROP PROCEDURE usp_getemailusers
GO
CREATE PROCEDURE [dbo].[USP_GetEmailUsers]            
@numDomainID as numeric(9)=0,            
@numContactID as numeric(9)=0,            
@byteMode as tinyint            
as            
            
if @byteMode=0             
select distinct(numContactID),vcFirstName+' '+vcLastName as vcUserName from usermaster U        
join AdditionalContactsInformation        
on numContactID=numUserDetailId             
where bitActivateFlag=1 and U.numDomainID=@numDomainID            
            
            
if @byteMode=1            
select distinct(A.numContactID),vcFirstName+' '+vcLastName as vcUserName from usermaster  U            
join EmailUsers E            
on E.numContactID=U.numUserDetailId         
join AdditionalContactsInformation A        
on A.numContactID=numUserDetailId            
where bitActivateFlag=1 and E.numUserCntID=@numContactID
GO
