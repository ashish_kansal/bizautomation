/****** Object:  StoredProcedure [dbo].[usp_GetListID]    Script Date: 07/26/2008 16:17:45 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getlistid')
DROP PROCEDURE usp_getlistid
GO
CREATE PROCEDURE [dbo].[usp_GetListID]
	@vcData varchar(50)='',
	@numListID numeric(9)=0,
	@numCreatedBy numeric(9)=0,
	@bintCreatedDate bigint=0,
	@numDomainId numeric(9)=0  
-- 
AS	
	DECLARE @numListIntId numeric
	
	IF @vcData <> ''
		BEGIN	
			SELECT @numListIntId=  ListDetails.numListItemID FROM  ListDetails INNER JOIN
			 ListMaster ON ListDetails.numListID = ListMaster.numListID
			 WHERE  upper(ListDetails.vcData) = upper(@vcData)
		IF @numListIntID is null  
			BEGIN
				INSERT INTO listdetails(numListId,vcData,numCreatedBy,bintCreatedDate,numDomainid)
				VALUES(@numListId,@vcData,@numCreatedBy,@bintCreatedDate,@numDomainid)
				SELECT SCOPE_IDENTITY()
			END
		ELSE
			BEGIN
				SELECT @numListIntId
			END
		END
	ELSE
		BEGIN  -- Means that there is no entry made
			SELECT 0
		END
GO
