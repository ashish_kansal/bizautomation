/****** Object:  StoredProcedure [dbo].[USP_OppSaveMilestone]    Script Date: 07/26/2008 16:20:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj                                            
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_oppsavemilestone')
DROP PROCEDURE usp_oppsavemilestone
GO
CREATE PROCEDURE [dbo].[USP_OppSaveMilestone]                                              
(                                              
 @numOppID as numeric(9)=null,                                              
 @strMilestone as text='' ,                        
 @numUserCntID as numeric(9)=0                                           
)                                              
as 
BEGIN                                             
declare @status as tinyint                                              
declare @hDoc3  int                                              
declare @divId as numeric(9)                                              
declare @ConId as numeric(9)                                              
declare @tintOpptype as tinyint                                              
declare @numItemCode as numeric(9)                                              
declare @numOppItemCode as numeric(9)                                              
declare @numUnit as numeric(9)                                              
declare @numQtyOnHand as numeric(9)                                          
                                          
                                            
                                            
if convert(varchar(10),@strMilestone) <>''                                           
 begin                                              
  EXEC sp_xml_preparedocument @hDoc3 OUTPUT, @strMilestone  


   delete from OpportunityStageDetails                                               
   where numOppStageID not in (SELECT StageID FROM OPENXML(@hDoc3,'/NewDataSet/Table',2) with(StageID numeric,Op_Flag numeric))                                             
      and numOppID=  @numOppID                                             
                                                
    update OpportunityStageDetails set                                               
     vcstageDetail=X.vcstageDetail,                                              
     numModifiedBy=X.numModifiedBy,                                    
     bintModifiedDate=GETUTCDATE(),
     bintDueDate=CONVERT(DATETIME,replace(X.bintDueDate,'T',' ')),
     bintStageComDate= CONVERT(DATETIME,REPLACE(X.bintStageComDate,'T',' ')),
     vcComments=X.vcComments,                                              
     numAssignTo=X.numAssignTo,                                              
     bitAlert=X.bitAlert,                                               
     bitStageCompleted=X.bitStageCompleted,numStage=X.numStage ,          
  vcStagePercentageDtl =x.vcStagePercentageDtl, numTemplateId =  x.numTemplateId,          
  tintPercentage=X.tintPercentage  ,      
 numEvent=X.numEvent,      
 numReminder=X.numReminder,      
 numET=X.numET,      
 numActivity =X.numActivity ,      
 numStartDate=X.numStartDate,      
 numStartTime=X.numStartTime,      
 numStartTimePeriod=X.numStartTimePeriod,      
 numEndTime=X.numEndTime,      
 numEndTimePeriod=X.numEndTimePeriod,      
 txtCom=X.txtCom     
 ,numChgStatus =x.numChgStatus ,bitChgStatus=x.bitChgStatus    
,bitClose =x.bitClose,numType =x.numType  ,numCommActId= x.numCommActId  
      From (SELECT *                                              
     FROM OPENXML(@hDoc3,'/NewDataSet/Table[StageID>0][Op_Flag=0]',2)                                  
      with(StageID numeric,                                              
      vcstageDetail varchar(100),                                              
      numModifiedBy numeric,                                   
      bintModifiedDate VARCHAR(23),                                              
      bintDueDate VARCHAR(23),
      bintStageComDate VARCHAR(23),
      vcComments varchar(1000),                                    
      numAssignTo numeric,                                              
      bitAlert bit,                                              
      bitStageCompleted bit,                                              
      Op_Flag numeric,                     
      numStage numeric,            
    vcStagePercentageDtl varchar(500),            
    numTemplateId numeric,          
  tintPercentage tinyint,      
  numEvent tinyint,      
  numReminder numeric,      
  numET numeric,      
  numActivity numeric ,      
  numStartDate tinyint,      
  numStartTime tinyint,      
  numStartTimePeriod tinyint ,      
  numEndTime tinyint ,      
  numEndTimePeriod tinyint,       
  txtCom varchar(1000) ,numChgStatus numeric(9),bitChgStatus bit ,bitClose bit,numType numeric(9),numCommActId numeric(9) ))X                                               
    where  numOppStageID=X.StageID                                              
                                                
                                                
   insert into OpportunityStageDetails                                              
    (numOppId,vcStageDetail,numStagePercentage,numDomainId,                                        
    numCreatedBy,bintCreatedDate,numModifiedBy,bintModifiedDate,                                              
    bintDueDate,bintStageComDate,vcComments,numAssignTo,bitAlert,                                              
    bitStageCompleted,numStage,vcStagePercentageDtl,numTemplateId,numEvent,numReminder,numET,numActivity ,numStartDate,numStartTime,numStartTimePeriod ,numEndTime ,      
numEndTimePeriod, txtCom ,numChgStatus ,bitChgStatus  ,bitClose,numType,numCommActId,tintPercentage )                                              
                                                 
   select @numOppID,                                              
    X.vcstageDetail ,                                              
    X.numStagePercentage ,                              
    X.numDomainId,                                              
    X.numCreatedBy ,                                              
    GETUTCDATE(),--replace(X.bintCreatedDate,'T',' ') ,                                              
	X.numModifiedBy ,                                              
    GETUTCDATE(),--replace(X.bintModifiedDate,'T',' ') ,                                              
    replace(X.bintDueDate,'T',' ') ,
    replace(X.bintStageComDate,'T',' ') ,
    X.vcComments ,                                              
    X.numAssignTo,                                    
    X.bitAlert ,                                              
    X.bitStageCompleted,                                        
    X.numStage,x.vcStagePercentageDtl,x.numTemplateId,X.numEvent,      
 X.numReminder,      
 X.numET,      
 X.numActivity ,      
 X.numStartDate,      
 X.numStartTime,      
 X.numStartTimePeriod ,      
 X.numEndTime ,      
 X.numEndTimePeriod,       
 X.txtCom  ,x.numChgStatus ,x.bitChgStatus  ,x.bitClose ,x.numType,x.numCommActId,x.tintPercentage from(SELECT *FROM OPENXML (@hDoc3,'/NewDataSet/Table[StageID=0][Op_Flag=0]',2)                                              
   WITH  (StageID numeric,                                              
    vcstageDetail varchar(100),                                              
    numStagePercentage numeric,                                              
    numDomainId numeric,                                              
    numCreatedBy numeric,                                              
    bintCreatedDate VARCHAR(23),                                              
    numModifiedBy numeric,                                              
    bintModifiedDate VARCHAR(23),                                              
    bintDueDate VARCHAR(23),
    bintStageComDate VARCHAR(23),                                              
    vcComments varchar(1000),                                              
    numAssignTo numeric,                                              
    bitAlert bit,                                              
    bitStageCompleted bit,                                              
    Op_Flag numeric,                                        
    numStage numeric ,            
 vcStagePercentageDtl varchar(500),            
    numTemplateId numeric ,      
 numEvent tinyint,      
 numReminder numeric,      
 numET numeric,      
 numActivity numeric ,      
 numStartDate tinyint,      
 numStartTime tinyint,      
 numStartTimePeriod tinyint ,      
 numEndTime tinyint ,      
 numEndTimePeriod tinyint,       
 txtCom varchar(1000)  ,numChgStatus numeric(9),bitChgStatus bit ,bitClose bit ,numType numeric(9),numCommActId numeric(9),tintPercentage tinyint
    ))X 

         
                                        
                                                
                                                
                                                
                                                
   EXEC sp_xml_removedocument @hDoc3                                              
                                              
   end                                          
         
                                            
declare @numDivisionID as numeric(9)        
declare @tintCRMType as numeric(9)        
declare @AccountClosingDate as datetime        
                                        
if exists(select * from OpportunityStageDetails where numOppId=@numOppID and   numStagePercentage=100 and bitStageCompleted=1)              
begin           
 select @numDivisionID=numDivisionID,@AccountClosingDate=bintAccountClosingDate from  OpportunityMaster where numOppID=@numOppID         
 select @tintCRMType=tintCRMType from divisionmaster where numDivisionID =@numDivisionID         
           
 if @AccountClosingDate is null                   
  update OpportunityMaster set tintOppStatus=1,bintAccountClosingDate=getutcdate() where numOppID=@numOppID              
    else         
  update OpportunityMaster set tintOppStatus=1 where numOppID=@numOppID         
 if @tintCRMType=0        
 begin        
  update divisionmaster set tintCRMType=2,bintLeadProm=getutcdate(),bintLeadPromBy=@numUserCntID,bintProsProm=getutcdate(),bintProsPromBy=@numUserCntID        
  where numDivisionID=@numDivisionID        
 end        
 else if @tintCRMType=1        
 begin        
  update divisionmaster set tintCRMType=2,bintProsProm=getutcdate(),bintProsPromBy=@numUserCntID        
  where numDivisionID=@numDivisionID        
 end        
        
end         
else if exists(select * from OpportunityStageDetails where numOppId=@numOppID and   numStagePercentage=0 and bitStageCompleted=1)              
begin         
 select @numDivisionID=numDivisionID,@AccountClosingDate=bintAccountClosingDate from  OpportunityMaster where numOppID=@numOppID         
 select @tintCRMType=tintCRMType from divisionmaster where numDivisionID =@numDivisionID         
 if @AccountClosingDate is null                   
  update OpportunityMaster set tintOppStatus=2,bintAccountClosingDate=getutcdate() where numOppID=@numOppID              
    else         
  update OpportunityMaster set tintOppStatus=2 where numOppID=@numOppID         
end                    
                            
 select numoppitemtCode from OpportunityItems where numOppid= @numOppID                      
              
              
--Updating the warehouse items              
	declare @tintOppStatus as tinyint               
	declare @tintShipped as tinyint   
	DECLARE @tintCommitAllocation TINYINT      
	
	SELECT 
		@tintOppStatus=tintOppStatus
		,@tintOppType=tintOppType
		,@tintShipped=tintShipped
		,@tintCommitAllocation=ISNULL(tintCommitAllocation,1)
	FROM 
		OpportunityMaster 
	INNER JOIN
		Domain
	ON
		OpportunityMaster.numDomainId = Domain.numDomainId
	WHERE 
		numOppID=@numOppID              
	
	IF @tintOppStatus=1 AND @tintCommitAllocation=1        
	BEGIN
		exec USP_UpdatingInventoryonCloseDeal @numOppID,0,@numUserCntID        
	END
END
GO
