/****** Object:  StoredProcedure [dbo].[usp_GetMonthToMonthComparison]    Script Date: 07/26/2008 16:17:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- modified by anoop jayaraj      
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getmonthtomonthcomparison')
DROP PROCEDURE usp_getmonthtomonthcomparison
GO
CREATE PROCEDURE [dbo].[usp_GetMonthToMonthComparison]          
        @numDomainID numeric,        
  @numUserCntID numeric,          
        @FirstStartDate datetime,          
        @FirstEndDate datetime,          
        @SecondStartDate datetime,        
        @SecondEndDate datetime,        
  @numCompareFirst numeric,        
  @numCompareSecond numeric,        
  @tintTypeCompare tinyint      
        
AS          
        
if @tintTypeCompare=0       
begin      
 SELECT isnull(SUM(OM.monPAmount),0) as Amount         
  FROM        
  OpportunityMaster OM      
  join AdditionalContactsInformation A      
  on OM.numRecOwner =A.numContactID      
  where A.numTeam=@numCompareFirst       
  and ( OM.bintAccountClosingDate >= @FirstStartDate AND OM.bintAccountClosingDate <= @SecondEndDate)      
      
      
      
  SELECT isnull(SUM(OM.monPAmount),0) as Amount         
  FROM        
  OpportunityMaster OM            
  join AdditionalContactsInformation A      
  on OM.numRecOwner =A.numContactID      
  where numTeam=@numCompareSecond      
  and ( OM.bintAccountClosingDate >= @FirstStartDate AND OM.bintAccountClosingDate <= @SecondEndDate)      
      
end        
if @tintTypeCompare=1      
begin      
SELECT isnull(SUM(OM.monPAmount),0) as Amount         
  FROM        
  OpportunityMaster OM      
  join DivisionMaster D      
  on D.numDivisionID=OM.numDivisionID      
  where numTerID=@numCompareFirst      
  and ( OM.bintAccountClosingDate >= @FirstStartDate AND OM.bintAccountClosingDate <= @SecondEndDate)      
      
      
SELECT isnull(SUM(OM.monPAmount),0) as Amount         
  FROM        
  OpportunityMaster OM      
  join DivisionMaster D      
  on D.numDivisionID=OM.numDivisionID      
  where numTerID=@numCompareSecond      
  and ( OM.bintAccountClosingDate >= @FirstStartDate AND OM.bintAccountClosingDate <= @SecondEndDate)      
      
end
GO
