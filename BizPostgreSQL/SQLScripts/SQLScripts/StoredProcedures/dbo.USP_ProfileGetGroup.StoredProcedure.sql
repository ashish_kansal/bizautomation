/****** Object:  StoredProcedure [dbo].[USP_ProfileGetGroup]    Script Date: 07/26/2008 16:20:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_profilegetgroup')
DROP PROCEDURE usp_profilegetgroup
GO
CREATE PROCEDURE [dbo].[USP_ProfileGetGroup]  
@numDomainID as integer ,
@numUserCntID as numeric 
  
as  
  
select vcEmailGroupName as Name,numEmailGroupID as GroupId from ProfileEmailGroup where numDomainID = @numDomainID  and numCreatedBy = @numUserCntID
GO
