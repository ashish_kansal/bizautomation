/****** Object:  StoredProcedure [dbo].[usp_GetPageElementsAndAvailableActions]    Script Date: 07/26/2008 16:18:08 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getpageelementsandavailableactions')
DROP PROCEDURE usp_getpageelementsandavailableactions
GO
CREATE PROCEDURE [dbo].[usp_GetPageElementsAndAvailableActions]
	@numModuleID INT,
	@numPageID INT   
--
AS
	SELECT     dbo.ModuleMaster.vcModuleName, dbo.ModuleMaster.numModuleID, dbo.PageActionElements.numPageID, 
                      dbo.PageActionElements.vcElementInitiatingAction, dbo.PageActionElements.vcElementInitiatingActionDesc, 
                      dbo.PageActionElements.vcElementInitiatingActionType, dbo.PageActionElements.vcChildElementInitiatingAction, 
                      dbo.PageActionElements.vcChildElementInitiatingActionType, dbo.PageActionElements.vcActionName, dbo.PageMaster.vcPageDesc
	FROM         dbo.ModuleMaster INNER JOIN
                      dbo.PageActionElements ON dbo.ModuleMaster.numModuleID = dbo.PageActionElements.numModuleID INNER JOIN
                      dbo.PageMaster ON dbo.ModuleMaster.numModuleID = dbo.PageMaster.numModuleID AND 
                      dbo.PageActionElements.numPageID = dbo.PageMaster.numPageID AND 
                      dbo.PageActionElements.numModuleID = dbo.PageMaster.numModuleID
	WHERE     (dbo.ModuleMaster.numModuleID = @numModuleID) AND (dbo.PageActionElements.numPageID = @numPageID)  AND (dbo.PageActionElements.bitStatus = 1)
GO
