/****** Object:  StoredProcedure [dbo].[USP_SaveMarketBudgetDetails]    Script Date: 07/26/2008 16:21:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
----Select * From MarketBudgetMaster                    
----Select * From MarketBudgetDetails            
--Created By Siva                                      
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_savemarketbudgetdetails')
DROP PROCEDURE usp_savemarketbudgetdetails
GO
CREATE PROCEDURE [dbo].[USP_SaveMarketBudgetDetails]                                      
@numDomainId as numeric(9)=0,                           
@numMarketId as numeric(9)=0,                          
@strRow as text='',                  
@intYear as int=0,              
@sintByte as tinyint=0,            
@intType as int=0,      
@numListItemID as numeric(9)=0      
As                                      
Begin                                      
                                
  Declare @hDoc3 int                                                                                                                                                                                                      
  EXEC sp_xml_preparedocument @hDoc3 OUTPUT, @strRow              
  Declare @Date as datetime                      
  Set @Date = dateadd(year,@intType,getutcdate())                 
  Declare @dtFiscalStDate as datetime                                
  Declare @dtFiscalEndDate as datetime                           
  Set @dtFiscalStDate =dbo.GetFiscalStartDate(dbo.GetFiscalyear(@Date,@numDomainId),@numDomainId)                             
  Set @dtFiscalEndDate =  dateadd(day,-1,dateadd(year,1,dbo.GetFiscalStartDate(dbo.GetFiscalyear(@Date,@numDomainId),@numDomainId)))                       
  print @dtFiscalStDate             
  print @dtFiscalEndDate                   
 if @sintByte=0             
  Begin            
    --To Delete the Records                    
    Delete MBD from MarketBudgetMaster MBM                    
    inner join MarketBudgetDetails MBD on MBM.numMarketBudgetId=MBD.numMarketId                    
    Where  MBD.numMarketId=@numMarketId And MBM.numDomainId=@numDomainId And ((MBD.tintMonth between Month(@dtFiscalStDate) and 12) and MBD.intYear=Year(@dtFiscalStDate)) Or ((MBD.tintMonth between 1 and month(@dtFiscalEndDate) ) and MBD.intYear=Year(@dtFiscalEndDate))              
                                        
     Insert Into MarketBudgetDetails(numMarketId,numListItemID,tintMonth,intYear,monAmount,vcComments)                                                                                                                         
    Select * from (                                                                                                                                                   
    Select * from OPenXml (@hdoc3,'/NewDataSet/Table1',2)                                                                                                                                                    
    With(                                               
    numMarketId numeric(9),                                    
    numListItemID numeric(9),                                 
    tintMonth tinyint,                                      
    intYear int,                                      
    monAmount DECIMAL(20,5),                                      
    vcComments varchar(100)                                       
    ))X                                    
  End            
 Else            
  Begin            
          
   --To Delete Record            
   Delete MBD from MarketBudgetMaster MBM                    
   inner join MarketBudgetDetails MBD on MBM.numMarketBudgetId=MBD.numMarketId 
   Where MBD.numMarketId=@numMarketId And MBD.numListItemID=@numListItemID     
   -- And (tintMonth>=month(getutcdate()) And intYear=year(@Date)) Or (intYear=year(@Date)+1)            
      And (((MBD.tintMonth between month(getutcdate()) and 12) and MBD.intYear=year(@Date)) Or ((MBD.tintMonth between 1 and 12) and MBD.intYear=Year(@date)+1))         
         
                                                 
   Insert Into MarketBudgetDetails(numMarketId,numListItemID,tintMonth,intYear,monAmount,vcComments)               
   Select * from (                                                                                                                                                   
   Select * from OPenXml (@hdoc3,'/NewDataSet/Table1',2)     
   With(                                               
   numMarketId numeric(9),        
   numListItemID numeric(9),                                 
   tintMonth tinyint,                                      
   intYear int,                                      
   monAmount DECIMAL(20,5),                                      
   vcComments varchar(100)                  
   ))X                            
  End                              
             
End
GO
