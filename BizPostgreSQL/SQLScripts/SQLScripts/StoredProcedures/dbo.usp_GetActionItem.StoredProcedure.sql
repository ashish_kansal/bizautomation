/****** Object:  StoredProcedure [dbo].[usp_GetActionItem]    Script Date: 07/26/2008 16:16:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getactionitem')
DROP PROCEDURE usp_getactionitem
GO
CREATE PROCEDURE [dbo].[usp_GetActionItem]
    	@numUserID Numeric(9)   
--
AS
select * from QBActionItem where numUserID=@numUserID and bitStatus=0
GO
