/****** Object:  StoredProcedure [dbo].[Reminder_UpdByRecurrence]    Script Date: 07/26/2008 16:14:33 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
/*
**  Updates the Reminder notification information on a recurring series,
**  such that the most recent unreminded occurrence's StartDateTimeUtc
**  is stored in the series' LastReminderDateTimeUtc.
*/
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='reminder_updbyrecurrence')
DROP PROCEDURE reminder_updbyrecurrence
GO
CREATE PROCEDURE [dbo].[Reminder_UpdByRecurrence]
	@LastReminderDateTimeUtc datetime, -- start date/time of the last occurrence
					   -- to have a reminder dismissed
	@RecurrenceID		 integer   -- the key of the Recurring series to be
					   -- updated.
AS
BEGIN
	UPDATE
		[Recurrence]
	SET 
		[LastReminderDateTimeUtc] = @LastReminderDateTimeUtc
	WHERE
		( [Recurrence].[RecurrenceID] = @RecurrenceID );
END
GO
