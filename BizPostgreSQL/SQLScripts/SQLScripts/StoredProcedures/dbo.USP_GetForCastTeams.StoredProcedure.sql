/****** Object:  StoredProcedure [dbo].[USP_GetForCastTeams]    Script Date: 07/26/2008 16:17:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getforcastteams')
DROP PROCEDURE usp_getforcastteams
GO
CREATE PROCEDURE [dbo].[USP_GetForCastTeams]  
@numUserCntid as numeric(9),  
@numDomainId AS NUMERIc(9),  
@tintType as tinyint  
as  
select numListitemid,vcdata from ForReportsByTeam  
join listdetails   
on numlistitemid=numteam  
where ForReportsByTeam.numUserCntID=@numUserCntid  
and ForReportsByTeam.numDomainId=@numDomainId  
and tintType=@tintType
GO
