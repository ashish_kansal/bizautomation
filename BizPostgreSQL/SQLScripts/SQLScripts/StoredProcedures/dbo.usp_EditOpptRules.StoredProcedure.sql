/****** Object:  StoredProcedure [dbo].[usp_EditOpptRules]    Script Date: 07/26/2008 16:15:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
--This SP is used for two purposes. One is for getting the alert records and the other
--is for getting the stagepercentageid.
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_editopptrules')
DROP PROCEDURE usp_editopptrules
GO
CREATE PROCEDURE [dbo].[usp_EditOpptRules]
--Means it is for fetching the details of the rules	  
@numRuleId numeric=0,
@numEmpDets numeric=0,
@numDomainID numeric=0   
--
AS

IF @numRuleId = 0 
	BEGIN

		SELECT OM.numRuleID, OM.numStagePercentageID, OM.monAmt, OM.vcCondn, OM.bintCreatedDate,
			OM.numNoOfDays, OM.vcRuleName, MIN(ACI.vcFirstName + ' ' + ACI.vcLastName) As vcUserNames
			FROM OpptRuleMaster OM INNER JOIN OpptRuleDetails OD ON OD.numRuleID=OM.numRuleID INNER JOIN AdditionalContactsInformation ACI ON ACI.numCOntactID=OD.numEmpID
			WHERE OM.numDomainID=@numDomainID
			GROUP BY OM.numRuleID, OM.numStagePercentageID, OM.monAmt, OM.vcCondn, OM.numNoOfDays, OM.vcRuleName, OM.bintCreatedDate

	END
ELSE
	BEGIN
		--SELECT numstagepercentage FROM stagepercentagemaster where 
		--numstagepercentageid=@numstagepercentageId
		IF @numEmpDets=0
			BEGIN
				SELECT * FROM opptrulemaster WHERE numruleid=@numruleid
			END
		ELSE
			BEGIN
				SELECT * FROM opptruledetails WHERE numruleid=@numruleid			
			END
END
GO
