/****** Object:  StoredProcedure [dbo].[USP_GetPriceOfItem]    Script Date: 07/26/2008 16:18:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--exec USP_GetPriceOfItem @numItemID=173028,@intQuantity=1,@numDomainID=72,@numDivisionID=7056

--created by anoop jayaraj
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getpriceofitem')
DROP PROCEDURE usp_getpriceofitem
GO
CREATE PROCEDURE [dbo].[USP_GetPriceOfItem]
@numItemID AS NUMERIC(18,0)=0,
@intQuantity AS INTEGER=0,
@numDomainID AS NUMERIC(18,0)=0,
@numDivisionID as numeric(18,0)=0,
@numWarehouseID AS NUMERIC(18,0)=0,
@numWarehouseItemID AS NUMERIC(18,0) = 0,
@vcSelectedKitChildItems AS VARCHAR(MAX) = ''
AS
BEGIN
	IF ISNULL(@numWarehouseItemID,0) = 0
	BEGIN
		SELECT TOP 1 @numWareHouseItemID=[numWareHouseItemID] FROM [WareHouseItems] WHERE [numItemID]=@numItemID AND [numWareHouseID]=@numWarehouseID
	END

	EXEC USP_ItemPricingRecomm 
		@numItemCode=@numItemID
		,@units=@intQuantity
		,@numOppID=0
		,@numDivisionID=@numDivisionID
		,@numDomainID=@numDomainID
		,@tintOppType=1
		,@CalPrice=0.0
		,@numWareHouseItemID=@numWareHouseItemID
		,@bitMode=2
		,@vcSelectedKitChildItems=@vcSelectedKitChildItems
		,@numOppItemID=0
END
GO
