/****** Object:  StoredProcedure [dbo].[USP_CompanyList2]    Script Date: 07/26/2008 16:15:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--- Created By Anoop Jayaraj                                        
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_companylist2')
DROP PROCEDURE usp_companylist2
GO
CREATE PROCEDURE [dbo].[USP_CompanyList2]                                          
@numRelationType as numeric(9),                                       
@numUserCntID numeric(9),                                          
@tintUserRightType tinyint,                                              
@SortChar char(1)='0',                                         
@FirstName varChar(100)= '',                                          
@LastName varChar(100)= '',                                          
@CustName varChar(100)= '',                                        
@CurrentPage int,                                        
@PageSize int,                                        
@TotRecs int output,                                        
@columnName as Varchar(50),                                        
@columnSortOrder as Varchar(10),                                
@tintSortOrder numeric=0,                                 
@numProfile as numeric(9)=0,                        
@numDomainID as numeric(9)=0,                
@tintCRMType as tinyint,                
@bitPartner as bit                                       
as                                        
  declare @join as varchar(400)  
set @join = '' 
    declare @firstRec as integer                                                    
  declare @lastRec as integer                                                    
 set @firstRec= (@CurrentPage-1) * @PageSize                                                    
     set @lastRec= (@CurrentPage*@PageSize+1)                                                              
  declare @column as varchar(50)      
set @column = @columnName      
declare @lookbckTable as varchar(50)      
set @lookbckTable = ''  
      
 if @column like 'Cust%' 
begin

	
	set @join= ' left Join CFW_FLD_Values CFW on CFW.RecId=Dm.numDivisionId  and CFW.fld_id= '+replace(@column,'Cust','') +' '                                         
	set @column='CFW.Fld_Value'
		
end                         
if @columnName like 'DCust%'
begin
                                       
	set @join= ' left Join CFW_FLD_Values_Cont CFW on CFW.RecId=Dm.numDivisionId   and CFW.fld_id= '+replace(@column,'DCust','')                                                                                   
	set @join= @join +' left Join ListDetails LstCF on LstCF.numListItemID=CFW.Fld_Value  '
	set @column='LstCF.vcData'  

end                                       
if @column not like 'Cust%' or  @column not like 'Cust%'                                            
begin
	 select  @lookbckTable=vcLookBackTableName from DynamicFormFieldMaster D where D.numFormId=11 and vcDbColumnName = @columnName      
	      
	if @lookbckTable <>''       
	 begin      
	  if @lookbckTable = 'Contacts'      
	   set @column = 'ADC.'+@column      
	  if @lookbckTable = 'Division'      
	   set @column = 'DM.'+@column     
	     
	 end                                       
end                                       
                                        
  declare @strSql as varchar(5000)                                      
set @strSql='with tblCompany as (SELECT   ROW_NUMBER() OVER (ORDER BY '+@column+' '+ @columnSortOrder+') AS RowNumber,                                           
     DM.numDivisionID                              
    FROM  CompanyInfo CMP                                        
    join DivisionMaster DM on DM.numCompanyID=CMP.numCompanyID'                                
                                
if @tintSortOrder= 1 set @strSql=@strSql+' join Favorites F on F.numContactid=DM.numDivisionID '                                
if @bitPartner=1 set @strSql=@strSql + ' left join CompanyAssociations CA on CA.numAssociateFromDivisionID=DM.numDivisionID and bitShareportal=1 and bitDeleted=0               
and CA.numDivisionID=(select numDivisionID from AdditionalContactsInformation where numContactID='+convert(varchar(15),@numUserCntID)+ ')'              
set @strSql=@strSql+ ' join AdditionalContactsInformation ADC on ADC.numDivisionID=DM.numDivisionID                                        
    left join ListDetails LD on LD.numListItemID=CMP.numCompanyRating                                         
    left join ListDetails LD1 on LD1.numListItemID= DM.numFollowUpStatus                                         
  WHERE ISNULL(ADC.bitPrimaryContact,0)=1                                         
    AND (DM.bitPublicFlag=0 OR DM.numRecOwner='+convert(varchar(15),@numUserCntID)+')                                              
    AND DM.numDomainID = '+convert(varchar(15),@numDomainID)+ ''                                        
   if @FirstName<>'' set    @strSql=@strSql+' and ADC.vcFirstName  like '''+@FirstName+'%'''                                         
    if @LastName<>'' set    @strSql=@strSql+' and ADC.vcLastName like '''+@LastName+'%'''                                        
    if @CustName<>'' set    @strSql=@strSql+' and CMP.vcCompanyName like '''+@CustName+'%'''                   
if @numRelationType<>'0' set    @strSql=@strSql+' AND CMP.numCompanyType = '+convert(varchar(15),@numRelationType)+ ''                                        
if @SortChar<>'0' set @strSql=@strSql + ' And CMP.vcCompanyName like '''+@SortChar+'%'''                                         
if @tintUserRightType=1 set @strSql=@strSql + ' AND ((DM.numRecOwner = '+convert(varchar(15),@numUserCntID)+ ')' + case when @bitPartner=1 then ' or ( CA.bitShareportal=1 and CA.bitDeleted=0))' else ')' end                                              
else if @tintUserRightType=2 set @strSql=@strSql + ' AND (DM.numTerID in (select numTerritoryID from  UserTerritory where numUserCntID= '+convert(varchar(15),@numUserCntID)+' ) or DM.numTerID=0) '         
if @tintSortOrder=1 set @strSql=@strSql + ' and F.numUserCntID='+convert(varchar(15),@numUserCntID)+ ' and cType=''O'''                                
if @numProfile> 0 set @strSql=@strSql + ' and  vcProfile='+convert(varchar(15),@numProfile)                                
if @tintCRMType>0 set @strSql=@strSql + ' and  DM.tintCRMType='+convert(varchar(1),@tintCRMType)                                  
                                      
                                      
                                        
  set @strSql=@strSql + ')'          
                                                                     
declare @tintOrder as tinyint                                            
declare @vcFormFieldName as varchar(50)                                            
declare @vcListItemType as varchar(1)                                       
declare @vcListItemType1 as varchar(1)                                           
declare @vcAssociatedControlType varchar(10)                                            
declare @numListID AS numeric(9)                                            
declare @vcDbColumnName varchar(20)                
declare @WhereCondition varchar(2000)                 
declare @Table varchar(2000)          
Declare @bitCustom as bit  
Declare @numFormFieldId as numeric  
            
set @tintOrder=0                                            
set @WhereCondition =''           
             
declare @Nocolumns as tinyint     
declare @DefaultNocolumns as tinyint          
set @Nocolumns=0          
select @Nocolumns=isnull(count(*),0)          
from [InitialListColumnConf] A             
where A.numFormId=11 and numUserCntID=@numUserCntID and numDomainID=@numDomainID  and numtype = @numRelationType              
   set @DefaultNocolumns=  @Nocolumns           
declare @strColumns as varchar(2000)          
set @strColumns=''          
while @DefaultNocolumns>0          
begin         
 set @strColumns=@strColumns+',null'          
 set @DefaultNocolumns=@DefaultNocolumns-1          
end      
set @strSql=@strSql+' select ADC.numContactId,DM.numDivisionID,DM.numTerID,ADC.numRecOwner,DM.tintCRMType,RowNumber'           
if @Nocolumns>0      
begin          
                                          
 select top 1 @tintOrder=tintOrder+1,@vcDbColumnName=vcDbColumnName,@vcFormFieldName=vcFormFieldName,              
 @vcAssociatedControlType=vcAssociatedControlType,@vcListItemType=vcListItemType,@numListID=numListID,@Table=vcLookBackTableName                                             
 	,@bitCustom=a.bitCustom,@numFormFieldId=A.numFormFieldId  from [InitialListColumnConf] A                                            
 join DynamicFormFieldMaster D                                             
 on D.numFormFieldId=A.numFormFieldId                                            
 where A.numFormId=11 and numUserCntID=@numUserCntID and A.numDomainID=@numDomainID  and numtype = @numRelationType              
 order by tintOrder asc      
end    
Else    
Begin    
  select @DefaultNocolumns=isnull(Count(*),0)    
  from  DynamicFormFieldMaster D      
  where D.numFormId=11   and bitDefault = 1     
    
  while @DefaultNocolumns>0        
  begin             
  set @strColumns=@strColumns+',null'        
  set @DefaultNocolumns=@DefaultNocolumns-1        
  end     
    
  select top 1 @tintOrder=[Order]+1,@vcDbColumnName=vcDbColumnName,@vcFormFieldName=vcFormFieldName,            
  @vcAssociatedControlType=vcAssociatedControlType,@vcListItemType=vcListItemType,@numListID=numListID,@Table=vcLookBackTableName                                           
 	,@bitCustom=0,@numFormFieldId=numFormFieldId   from  DynamicFormFieldMaster D      
  where D.numFormId=11   and bitDefault = 1          
  order by [Order] asc     
End    
    set @DefaultNocolumns=  @Nocolumns                                          
while @tintOrder>0                                            
begin                                            
                                       
    if @bitCustom = 0
	begin                                              
		 if @vcAssociatedControlType='SelectBox'                                            
				begin                                            
		                                                            
		  if @vcListItemType='L'                                             
		  begin                                            
		   set @strSql=@strSql+',L'+ convert(varchar(3),@tintOrder)+'.vcData'+' ['+ @vcFormFieldName+'~'+ @vcDbColumnName+']'                                            
		   set @WhereCondition= @WhereCondition +' left Join ListDetails L'+ convert(varchar(3),@tintOrder)+ ' on L'+convert(varchar(3),@tintOrder)+ '.numListItemID='+@vcDbColumnName                                            
		  end                                            
		  else if @vcListItemType='S'                                             
		  begin                                            
		   set @strSql=@strSql+',S'+ convert(varchar(3),@tintOrder)+'.vcState'+' ['+ @vcFormFieldName+'~'+ @vcDbColumnName+']'                                            
		   set @WhereCondition= @WhereCondition +' left join State S'+ convert(varchar(3),@tintOrder)+ ' on S'+convert(varchar(3),@tintOrder)+ '.numStateID='+@vcDbColumnName                                            
		  end                                            
		  else if @vcListItemType='T'                                             
		  begin                                            
			  set @strSql=@strSql+',L'+ convert(varchar(3),@tintOrder)+'.vcData'+' ['+ @vcFormFieldName+'~'+ @vcDbColumnName+']'                                            
		   set @WhereCondition= @WhereCondition +' left Join ListDetails L'+ convert(varchar(3),@tintOrder)+ ' on L'+convert(varchar(3),@tintOrder)+ '.numListItemID='+@vcDbColumnName                          
		  end           
		  else if   @vcListItemType='U'                                         
		 begin           
		   declare @Prefix as varchar(5)          
		   if @Table = 'Contacts'          
			set @Prefix = 'ADC.'          
		   if @Table = 'Division'          
			set @Prefix = 'DM.'          
		             
			set @strSql=@strSql+',dbo.fn_GetContactName('+@Prefix+@vcDbColumnName+') ['+ @vcFormFieldName+'~'+ @vcDbColumnName+']'                                            
		 end          
		 end                                            
		  else set @strSql=@strSql+','+ case  when @vcDbColumnName='numAge' then 'year(getutcdate())-year(bintDOB)' when @vcDbColumnName='numDivisionID' then 'DM.numDivisionID' else @vcDbColumnName end+' ['+ @vcFormFieldName+'~'+ @vcDbColumnName+']'              
		 
	end
if @bitCustom = 1
begin
			
			select @vcFormFieldName=FLd_label,@vcAssociatedControlType=fld_type,@vcDbColumnName='Cust'+Convert(varchar(10),Fld_Id)               
			 from CFW_Fld_Master join CFW_Fld_Dtl                                
			on Fld_id=numFieldId                                
			left join CFw_Grp_Master                                 
			on subgrp=CFw_Grp_Master.Grp_id                                
			where CFW_Fld_Master.grp_id=1 and numRelation=@numRelationType and CFW_Fld_Master.numDomainID=@numDomainID  and CFW_Fld_Master.Fld_Id = @numFormFieldId 


			print @vcAssociatedControlType
			if @vcAssociatedControlType <> 'SelectBox'
			begin
			
				set @strSql= @strSql+',CFW'+ convert(varchar(3),@tintOrder)+'.Fld_Value  ['+ @vcFormFieldName+'~'+ @vcDbColumnName+']'   
				set @WhereCondition= @WhereCondition +' left Join CFW_FLD_Values CFW'+ convert(varchar(3),@tintOrder)+ ' 
				on CFW'+ convert(varchar(3),@tintOrder)+ '.Fld_Id='+convert(varchar(10),@numFormFieldId) +'and CFW'+ convert(varchar(3),@tintOrder)+ '.RecId=Dm.numDivisionId   '                                         
			end
			if @vcAssociatedControlType = 'SelectBox'
			begin
				set @vcDbColumnName = 'DCust'+Convert(varchar(10),@numFormFieldId)
				set @strSql=@strSql+',L'+ convert(varchar(3),@tintOrder)+'.vcData'+' ['+ @vcFormFieldName+'~'+ @vcDbColumnName+']'                                          
				set @WhereCondition= @WhereCondition +' left Join CFW_FLD_Values CFW'+ convert(varchar(3),@tintOrder)+ ' 
					on CFW'+ convert(varchar(3),@tintOrder)+ '.Fld_Id='+convert(varchar(10),@numFormFieldId) +'and CFW'+ convert(varchar(3),@tintOrder)+ '.RecId=Dm.numDivisionId   '                                         
				set @WhereCondition= @WhereCondition +' left Join ListDetails L'+ convert(varchar(3),@tintOrder)+ ' on L'+convert(varchar(3),@tintOrder)+ '.numListItemID=CFW'+ convert(varchar(3),@tintOrder)+'.Fld_Value'
			end	
end		  
    	    
           
 if @DefaultNocolumns>0      
 begin    
    select top 1 @tintOrder=tintOrder+1,@vcDbColumnName=vcDbColumnName,@vcFormFieldName=vcFormFieldName,          
    @vcAssociatedControlType=vcAssociatedControlType,@vcListItemType=vcListItemType,@numListID=numListID  ,@Table=vcLookBackTableName           
   	,@bitCustom=a.bitCustom,@numFormFieldId=A.numFormFieldId   from [InitialListColumnConf] A                                            
    join DynamicFormFieldMaster D                                             
    on D.numFormFieldId=A.numFormFieldId     
    where a.numFormId=11 and numUserCntID=@numUserCntID and A.numDomainID=@numDomainID  and numtype = @numRelationType and               
    tintOrder > @tintOrder-1 order by tintOrder asc                                            
    if @@rowcount=0 set @tintOrder=0     
 end    
 else    
 Begin       
     select top 1 @tintOrder=[Order]+1,@vcDbColumnName=vcDbColumnName,@vcFormFieldName=vcFormFieldName,        
     @vcAssociatedControlType=vcAssociatedControlType,@vcListItemType=vcListItemType,@numListID=numListID  ,@Table=vcLookBackTableName         
   	,@bitCustom=0,@numFormFieldId=numFormFieldId    from DynamicFormFieldMaster D                                           
     where D.numFormId=11 and bitDefault=1  and           
     [Order] > @tintOrder-1 order by [Order] asc                                          
     if @@rowcount=0 set @tintOrder=0                    
 End    
                                 
                                           
end                 
              
                        
set @strSql=@strSql+' From CompanyInfo CMP                                                    
    join DivisionMaster DM on DM.numCompanyID=CMP.numCompanyID                                          
    join AdditionalContactsInformation ADC on ADC.numDivisionID=DM.numDivisionID                                             
   '+@WhereCondition+          
' join tblCompany T on T.numDivisionID=DM.numDivisionID                                                                              
  WHERE RowNumber > '+convert(varchar(10),@firstRec)+ ' and RowNumber <'+ convert(varchar(10),@lastRec)+          
' union select count(*),null,null,null,null,null '+@strColumns+' from tblCompany order by RowNumber'                                     
                                         
print @strSql                  
          
exec (@strSql)                                                  
                                                     
                              
                              
                              
--SELECT  CMP.vcCompanyName + ' - <I>' + DM.vcDivisionName + '</I>' as CompanyName,                                           
--     DM.numTerID,                                          
--     ADC.vcFirstName  + ' ' +ADC.vcLastName as PrimaryContact,                                          
--     case when ADC.numPhone<>'' then + ADC.numPhone +case when ADC.numPhoneExtension<>'' then ' - ' + ADC.numPhoneExtension else '' end  else '' end as [Phone],                                       
--     ADC.vcEmail As vcEmail,                                          
--     CMP.numCompanyID AS numCompanyID,                                          
--     DM.numDivisionID As numDivisionID,                                
--     ADC.numContactID AS numContactID,                                           
--     CMP.numCompanyRating AS numCompanyRating,                                           
--     LD.vcData as vcRating,                                            
--     DM.numStatusID AS numStatusID,                                           
--     LD1.vcData as Follow,                                          
--     DM.numCreatedby AS numCreatedby, DM.numRecOwner , DM.tintCRMType,                                     
--     case when DM.tintCRMType=1 then 'Prospects' when DM.tintCRMType=2 then 'Accounts' end as AccountType                                      
--    FROM  CompanyInfo CMP                                        
--    join DivisionMaster DM on DM.numCompanyID=CMP.numCompanyID                              
--    join AdditionalContactsInformation ADC on ADC.numDivisionID=DM.numDivisionID                                        
--    left join ListDetails LD on LD.numListItemID=CMP.numCompanyRating                                         
--    left join ListDetails LD1 on LD1.numListItemID= DM.numFollowUpStatus                                         
--    join #tempTable T on T.numDivisionID=DM.numDivisionID                                           
--    WHERE ADC.numContacttype=70 and ID > @firstRec and ID < @lastRec order by ID                       
--drop table #tempTable
GO
