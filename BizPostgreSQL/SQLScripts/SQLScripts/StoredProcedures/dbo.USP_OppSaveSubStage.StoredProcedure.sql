/****** Object:  StoredProcedure [dbo].[USP_OppSaveSubStage]    Script Date: 07/26/2008 16:20:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_oppsavesubstage')
DROP PROCEDURE usp_oppsavesubstage
GO
CREATE PROCEDURE [dbo].[USP_OppSaveSubStage]  
(  
 @byteMode as tinyint=null,  
 @OppID as numeric(9)=null,  
 @stageDetailsId as numeric(9)=null,  
 @strSubStage as text='',  
 @numSubStageHdrID as numeric(9),  
 @numSubStageID as numeric(9)=null  
)  
  
as  
  
begin  
if @byteMode= 0   
begin  
declare @hDoc  int  
EXEC sp_xml_preparedocument @hDoc OUTPUT, @strSubStage  
 delete from OpportunitySubStageDetails where numStageDetailsId=@stageDetailsId and numOppId=@OppID  
 insert into OpportunitySubStageDetails  
   (  
    numOppId,  
    numStageDetailsId,  
    numSubStageID,  
    numProcessListId,  
    vcSubStageDetail,  
    vcComments,  
    bitStageCompleted  
   )  
 select @OppID,@stageDetailsId,@numSubStageID,X.* from(SELECT *FROM OPENXML (@hDoc,'/NewDataSet/Table',2)  
 WITH ( numSalesProsessList numeric(9),  
  vcSubStageDetail varchar(1000),  
  vcComments varchar(250),  
  bitStageCompleted bit  
  ))X  
   
  
  
  
EXEC sp_xml_removedocument @hDoc    
  
end  
if @byteMode= 1  
   
begin  
 select numSubStageElmtID,numOppId,numStageDetailsId,  
    numSubStageID ,  
    numProcessListId as numSalesProsessList,  
    vcSubStageDetail,  
    vcComments,  
    bitStageCompleted  
 from OpportunitySubStageDetails   
 where numOppId=@OppID and numStageDetailsId=@stageDetailsId  
  
end  
  
if @byteMode= 2  
   
begin  
 select  numSubStageDtlID,  
  0 as numOppId,  
  0 as numStageDetailsId,  
  0 as numSubStageID,  
  numProcessListId as numSalesProsessList,  
  vcSubStageElemt as vcSubStageDetail,  
  '' vcComments,  
  0 as bitStageCompleted  
 from SubStageDetails  
 where  numSubStageHdrID=@numSubStageHdrID   
  
  
  
end  
  
  
end
GO
