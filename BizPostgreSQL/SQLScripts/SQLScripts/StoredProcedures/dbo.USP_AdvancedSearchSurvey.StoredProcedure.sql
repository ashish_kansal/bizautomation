GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_AdvancedSearchSurvey')
DROP PROCEDURE USP_AdvancedSearchSurvey
GO
CREATE  PROCEDURE [dbo].[USP_AdvancedSearchSurvey]  
@WhereCondition as varchar(1000)='',                   
@numDomainID as numeric(9)=0,            
@numUserCntID as numeric(9)=0,                                                 
@CurrentPage int,                                                                              
@PageSize int,                                                                              
@TotRecs int output,                                                                                                                       
@columnSortOrder as Varchar(10),          
@ColumnName as varchar(20)='',          
@SortCharacter as char(1),                                  
@SortColumnName as varchar(20)='',          
@LookTable as varchar(10)='',
@GetAll as bit,
@vcDisplayColumns VARCHAR(MAX)
as  
declare @tintOrder as tinyint                                        
declare @vcFormFieldName as varchar(50)                                        
declare @vcListItemType as varchar(3)                                   
declare @vcListItemType1 as varchar(3)                                       
declare @vcAssociatedControlType varchar(10)                                        
declare @numListID AS numeric(9)                                        
declare @vcDbColumnName varchar(30)                                         
declare @ColumnSearch as varchar(10)   
DECLARE @bitContactAddressJoinAdded AS BIT;
DECLARE @bitBillAddressJoinAdded AS BIT;
DECLARE @bitShipAddressJoinAdded AS BIT;

                             
if (@SortCharacter<>'0' and @SortCharacter<>'') set @ColumnSearch=@SortCharacter         
Create table #tempTable ( ID INT IDENTITY PRIMARY KEY,                                                                               
      numContactId varchar(15)                                                                              
 )   
                                                                       
declare @strSql as varchar(8000)                                                                  
 set  @strSql='Select ADC.numContactId                        
  FROM AdditionalContactsInformation ADC                                                       
  JOIN DivisionMaster DM ON ADC.numDivisionId = DM.numDivisionID                                                      
  JOIN CompanyInfo C ON DM.numCompanyID = C.numCompanyId                                  
   '                              
  if (@ColumnName<>'' and  @ColumnSearch<>'')                            
  begin                              
 select @vcListItemType=vcListItemType from View_DynamicDefaultColumns where vcDbColumnName=@ColumnName and numFormID=19 and numDomainId=@numDomainId                              
    if @vcListItemType='LI'                                   
    begin                                    
      set @strSql= @strSql +' left Join ListDetails L1 on L1.numListItemID='+@ColumnName                                  
    end                                  
    else if @vcListItemType='S'                                   
    begin                                      
      set @strSql= @strSql +' left join State S1 on S1.numStateID='+@ColumnName                                  
    end                                  
    else if @vcListItemType='T'                                   
    begin                                  
      set @strSql= @strSql +' left Join ListDetails L1 on L1.numListItemID='+@ColumnName                                 
    end                               
   end                              
   if (@SortColumnName<>'')                        
  begin                              
 select @vcListItemType1=vcListItemType from View_DynamicDefaultColumns where vcDbColumnName=@SortColumnName and numFormID=19  and numDomainId=@numDomainId                            
    if @vcListItemType1='LI'                       
    begin                                    
      set @strSql= @strSql +' left Join ListDetails L2 on L2.numListItemID='+@SortColumnName                           
    end                                  
    else if @vcListItemType1='S'               
    begin                                      
      set @strSql= @strSql +' left join State S2 on S2.numStateID='+@SortColumnName                                  
    end                                  
    else if @vcListItemType1='T'                      
    begin                                  
      set @strSql= @strSql +' left Join ListDetails L2 on L2.numListItemID='+@SortColumnName                                   
    end                               
  end                              
  set @strSql=@strSql+' where DM.numDomainID  = '+convert(varchar(15),@numDomainID)+    
' and ADC.numContactId in   
(select numRegisteredRespondentContactId from SurveyRespondentsMaster SRM   
join SurveyResponse  SR on SRM.numRespondantID=SR.numRespondantID  
where SRM.numDomainId = '+convert(varchar(15),@numDomainID)+  ' and bitRegisteredRespondant =1  '  
if @WhereCondition <> ''  
  set @strSql=@strSql+'and(  ' +@WhereCondition+')'  
set @strSql=@strSql+')'  
                                       
                        
                                
                                                
                             
   if (@SortColumnName<>'')                            
  begin                               
    if @vcListItemType1='LI'                                   
    begin                                    
      set @strSql= @strSql +' order by L2.vcData '+@columnSortOrder                                  
    end                                  
    else if @vcListItemType1='S'                                   
    begin                                      
      set @strSql= @strSql +' order by S2.vcState '+@columnSortOrder                                  
    end                                  
    else if @vcListItemType1='T'                                   
    begin                                  
      set @strSql= @strSql +' order by T2.vcTerName '+@columnSortOrder                                  
    end                              
           else  set @strSql= @strSql +' order by '+ @SortColumnName +' ' +@columnSortOrder                            
  end                            
                                                               
insert into #tempTable(numContactID)                   
exec(@strSql)    
print @strSql                                 
set @strSql=''                                  
                                  
SET @bitBillAddressJoinAdded=0;
SET @bitShipAddressJoinAdded=0;
SET @bitContactAddressJoinAdded=0;                            
set @tintOrder=0                                  
set @WhereCondition =''                               
set @strSql='select ADC.numContactId,DM.numDivisionID,DM.numTerID,ADC.numRecOwner,DM.tintCRMType '

	DECLARE @TEMPSelectedColumns TABLE
	(
		numFieldID NUMERIC(18,0)
		,bitCustomField BIT
		,tintOrder INT
		,intColumnWidth FLOAT
	)
	DECLARE @vcLocationID VARCHAR(100) = '0'
	SELECT @vcLocationID =isnull(vcLocationID,'0') from dynamicFormMaster where numFormID=19    

	-- IF USER HAS SELECTED ITS OWN DISPLAY COLUMNS THEN USE IT
	IF LEN(ISNULL(@vcDisplayColumns,'')) > 0
	BEGIN
		DECLARE @TempIDs TABLE
		(
			ID INT IDENTITY(1,1),
			vcFieldID VARCHAR(300)
		)

		INSERT INTO @TempIDs
		(
			vcFieldID
		)
		SELECT 
			OutParam 
		FROM 
			SplitString(@vcDisplayColumns,',')

		INSERT INTO @TEMPSelectedColumns
		(
			numFieldID,
			bitCustomField,
			tintOrder,
			intColumnWidth
		)
		SELECT
			numFormFieldID
			,bitCustomField
			,(SELECT (ID-1) FROM @TempIDs T3 WHERE T3.vcFieldID = TEMP.vcFieldID)
			,0
		FROM
		(
			SELECT  
				numFieldID as numFormFieldID,
				0 bitCustomField,
				CONCAT(numFieldID,'~0') vcFieldID
			FROM    
				View_DynamicDefaultColumns
			WHERE   
				numFormID = 19
				AND bitInResults = 1
				AND bitDeleted = 0 
				AND numDomainID = @numDomainID
			UNION 
			SELECT  
				c.Fld_id AS numFormFieldID
				,1 bitCustomField
				,CONCAT(Fld_id,'~1') vcFieldID
			FROM    
				CFW_Fld_Master C 
			LEFT JOIN 
				CFW_Validation V ON V.numFieldID = C.Fld_id
			JOIN 
				CFW_Loc_Master L on C.GRP_ID=L.Loc_Id
			WHERE   
				C.numDomainID = @numDomainID
				AND GRP_ID IN (SELECT ID FROM dbo.SplitIDs(@vcLocationID,','))
		) TEMP
		WHERE
			vcFieldID IN (SELECT vcFieldID FROM @TempIDs)
	END

	-- IF USER SELECTED DISPLAY COLUMNS IS NOT AVAILABLE THAN GET COLUMNS CONFIGURED FROM SEARCH RESULT GRID
	IF (SELECT COUNT(*) FROM @TEMPSelectedColumns) = 0
	BEGIN
		INSERT INTO @TEMPSelectedColumns
		(
			numFieldID,
			bitCustomField,
			tintOrder,
			intColumnWidth
		)
		SELECT
			numFormFieldID
			,bitCustomField
			,tintOrder
			,intColumnWidth
		FROM
		(
			SELECT  
				A.numFormFieldID,
				0 bitCustomField,
				A.tintOrder,
				A.intColumnWidth
			FROM    AdvSerViewConf A
					JOIN View_DynamicDefaultColumns D ON D.numFieldID = A.numFormFieldID
			WHERE   A.numFormID = 19
					AND D.bitInResults = 1
					AND D.bitDeleted = 0
					AND D.numDomainID = @numDomainID 
					AND D.numFormID = 19
					AND A.numDomainID = @numDomainID
					AND A.numUserCntID=@numUserCntID
					AND ISNULL(A.bitCustom,0) = 0
			UNION
			SELECT  
				A.numFormFieldID,
				1 bitCustomField,
				A.tintOrder,
				A.intColumnWidth
			FROM    AdvSerViewConf A
					JOIN CFW_Fld_Master C ON C.Fld_id = A.numFormFieldID
			WHERE   A.numFormID = 19
					AND C.numDomainID = @numDomainID 
					AND A.numDomainID = @numDomainID
					AND A.numUserCntID=@numUserCntID
					AND ISNULL(A.bitCustom,0) = 1
		) T1
		ORDER BY
			 tintOrder
	END

	-- IF USER HASN'T CONFIGURED COLUMNS FROM SEARCH RESULT GRID THEN RETURN DEFAULT COLUMNS
	IF (SELECT COUNT(*) FROM @TEMPSelectedColumns) = 0
	BEGIN
		INSERT INTO @TEMPSelectedColumns
		(
			numFieldID,
			bitCustomField,
			tintOrder,
			intColumnWidth
		)
		SELECT  
			numFieldID,
			0,
			1,
			0
		FROM    
			View_DynamicDefaultColumns
		WHERE   
			numFormID = 19
			AND bitInResults = 1
			AND bitDeleted = 0 
			AND numDomainID = @numDomainID
			AND numFieldID = 3
	END
                                
	SELECT TOP 1 
		@tintOrder=T1.tintOrder+1
		,@vcDbColumnName=vcDbColumnName
		,@vcFormFieldName=vcFieldName
		,@vcAssociatedControlType=vcAssociatedControlType
		,@vcListItemType=vcListItemType
		,@numListID=numListID                                   
	FROM 
		View_DynamicDefaultColumns D                                   
	INNER JOIN
		@TEMPSelectedColumns T1
	ON 
		D.numFieldID=T1.numFieldID                             
	WHERE 
		D.numFormID = 19 
	ORDER BY 
		T1.tintOrder ASC           
                       
	WHILE @tintOrder>0                                  
	BEGIN                                                           
		IF @vcAssociatedControlType='SelectBox'                                  
		BEGIN                                  
			  if @vcListItemType='LI'                                   
			  begin                  
				 IF @numListID=40--Country
				  BEGIN
	  				set @strSql=@strSql+',L'+ convert(varchar(3),@tintOrder)+'.vcData'+' ['+ @vcFormFieldName+'~'+ @vcDbColumnName+']'                              
					IF @vcDbColumnName ='numCountry'
					BEGIN
						IF @bitContactAddressJoinAdded=0 
					  BEGIN
							set @WhereCondition= @WhereCondition +' left Join AddressDetails AD on AD.numRecordId= ADC.numContactID and AD.tintAddressOf=1 and AD.tintAddressType=0 AND AD.bitIsPrimary=1 and AD.numDomainID= ADC.numDomainID'	
							SET @bitContactAddressJoinAdded=1;
					  END
					  set @WhereCondition= @WhereCondition +' left Join ListDetails L'+ convert(varchar(3),@tintOrder)+ ' on L'+convert(varchar(3),@tintOrder)+ '.numListItemID=AD.numCountry'	
					END
					ELSE IF @vcDbColumnName ='numBillCountry'
					BEGIN
						IF @bitBillAddressJoinAdded=0 
						BEGIN
							set @WhereCondition= @WhereCondition +' left Join AddressDetails AD1 on AD1.numRecordId= DM.numDivisionID and AD1.tintAddressOf=2 and AD1.tintAddressType=1 AND AD1.bitIsPrimary=1 and AD1.numDomainID= ADC.numDomainID '
					
							SET @bitBillAddressJoinAdded=1;
						END
						set @WhereCondition= @WhereCondition +' left Join ListDetails L'+ convert(varchar(3),@tintOrder)+ ' on L'+convert(varchar(3),@tintOrder)+ '.numListItemID=AD1.numCountry'
					END
					ELSE IF @vcDbColumnName ='numShipCountry'
					BEGIN
						IF @bitShipAddressJoinAdded=0 
						BEGIN
							set @WhereCondition= @WhereCondition +' left Join AddressDetails AD2 on AD2.numRecordId= DM.numDivisionID and AD2.tintAddressOf=2 and AD2.tintAddressType=2 AND AD2.bitIsPrimary=1 and AD2.numDomainID= ADC.numDomainID '
					
							SET @bitShipAddressJoinAdded=1;
						END
						set @WhereCondition= @WhereCondition +' left Join ListDetails L'+ convert(varchar(3),@tintOrder)+ ' on L'+convert(varchar(3),@tintOrder)+ '.numListItemID=AD2.numCountry'
					END
				  END                        
				  ELSE
				  BEGIN                
					   set @strSql=@strSql+',L'+ convert(varchar(3),@tintOrder)+'.vcData'+' ['+ @vcFormFieldName+'~'+ @vcDbColumnName+']'                                  
					   set @WhereCondition= @WhereCondition +' left Join ListDetails L'+ convert(varchar(3),@tintOrder)+ ' on L'+convert(varchar(3),@tintOrder)+ '.numListItemID='+@vcDbColumnName                                  
				  END			   
			  end                                  
			  else if @vcListItemType='S'                                   
			  begin     
				set @strSql=@strSql+',S'+ convert(varchar(3),@tintOrder)+'.vcState'+' ['+ @vcFormFieldName+'~'+ @vcDbColumnName+']'
				IF @vcDbColumnName ='numState'
				BEGIN
					IF @bitContactAddressJoinAdded=0 
					   BEGIN
							set @WhereCondition= @WhereCondition +' left Join AddressDetails AD on AD.numRecordId= ADC.numContactID and AD.tintAddressOf=1 and AD.tintAddressType=0 AND AD.bitIsPrimary=1 and AD.numDomainID= ADC.numDomainID'	
							SET @bitContactAddressJoinAdded=1;
					  END	
					  set @WhereCondition= @WhereCondition +' left join State S'+ convert(varchar(3),@tintOrder)+ ' on S'+convert(varchar(3),@tintOrder)+ '.numStateID=AD.numState'
				END
				ELSE IF @vcDbColumnName ='numBillState'
				BEGIN
					IF @bitBillAddressJoinAdded=0 
					BEGIN
						set @WhereCondition= @WhereCondition +' left Join AddressDetails AD1 on AD1.numRecordId= DM.numDivisionID and AD1.tintAddressOf=2 and AD1.tintAddressType=1 AND AD1.bitIsPrimary=1 and AD1.numDomainID= ADC.numDomainID '
						SET @bitBillAddressJoinAdded=1;
					END
					set @WhereCondition= @WhereCondition +' left join State S'+ convert(varchar(3),@tintOrder)+ ' on S'+convert(varchar(3),@tintOrder)+ '.numStateID=AD1.numState'
				END
				ELSE IF @vcDbColumnName ='numShipState'
				BEGIN
					IF @bitShipAddressJoinAdded=0 
					BEGIN
						set @WhereCondition= @WhereCondition +' left Join AddressDetails AD2 on AD2.numRecordId= DM.numDivisionID and AD2.tintAddressOf=2 and AD2.tintAddressType=2 AND AD2.bitIsPrimary=1 and AD2.numDomainID= ADC.numDomainID '
						SET @bitShipAddressJoinAdded=1;
					END
					set @WhereCondition= @WhereCondition +' left join State S'+ convert(varchar(3),@tintOrder)+ ' on S'+convert(varchar(3),@tintOrder)+ '.numStateID=AD2.numState'
				END
			  end                                  
			  else if @vcListItemType='T'                                   
			  begin                                  
				  set @strSql=@strSql+',L'+ convert(varchar(3),@tintOrder)+'.vcData'+' ['+ @vcFormFieldName+'~'+ @vcDbColumnName+']'                                  
			   set @WhereCondition= @WhereCondition +' left Join ListDetails L'+ convert(varchar(3),@tintOrder)+ ' on L'+convert(varchar(3),@tintOrder)+ '.numListItemID='+@vcDbColumnName                                  
			  end                                  
		end
		ELSE IF @vcAssociatedControlType='TextBox'   
		BEGIN
 			IF @vcDbColumnName='vcStreet' OR @vcDbColumnName='vcCity' OR @vcDbColumnName='vcPostalCode'
			begin      
			PRINT 'hi'                        
			   set @strSql=@strSql+',AD.'+ @vcDbColumnName+' ['+ @vcFormFieldName+'~'+ @vcDbColumnName+']'                              
			   IF @bitContactAddressJoinAdded=0 
			   BEGIN
					set @WhereCondition= @WhereCondition +' left Join AddressDetails AD on AD.numRecordId= ADC.numContactID and AD.tintAddressOf=1 and AD.tintAddressType=0 AND AD.bitIsPrimary=1 and AD.numDomainID= ADC.numDomainID'	
					SET @bitContactAddressJoinAdded=1;
			   END
			   PRINT  @WhereCondition
			end 
			ELSE IF @vcDbColumnName='vcBillStreet' OR @vcDbColumnName='vcBillCity' OR @vcDbColumnName='vcBillPostCode'
			begin                              
			   set @strSql=@strSql+',AD1.'+ REPLACE(REPLACE(@vcDbColumnName,'Bill',''),'PostCode','PostalCode') +' ['+ @vcFormFieldName+'~'+ @vcDbColumnName+']'                              
			   IF @bitBillAddressJoinAdded=0 
			   BEGIN
	   				set @WhereCondition= @WhereCondition +' left Join AddressDetails AD1 on AD1.numRecordId= DM.numDivisionID and AD1.tintAddressOf=2 and AD1.tintAddressType=1 AND AD1.bitIsPrimary=1 and AD1.numDomainID= ADC.numDomainID '
	   				SET @bitBillAddressJoinAdded=1;
			   END
			end 
			ELSE IF @vcDbColumnName='vcShipStreet' OR @vcDbColumnName='vcShipCity' OR @vcDbColumnName='vcShipPostCode'
			begin                              
			   set @strSql=@strSql+',AD2.'+ REPLACE(REPLACE(@vcDbColumnName,'Ship',''),'PostCode','PostalCode') +' ['+ @vcFormFieldName+'~'+ @vcDbColumnName+']'                              
			   IF @bitShipAddressJoinAdded=0 
			   BEGIN
	   				set @WhereCondition= @WhereCondition +' left Join AddressDetails AD2 on AD2.numRecordId= DM.numDivisionID and AD2.tintAddressOf=2 and AD2.tintAddressType=2  AND AD2.bitIsPrimary=1 and AD2.numDomainID= ADC.numDomainID '
	   				SET @bitShipAddressJoinAdded=1;
			   END
			end 
			ELSE
			BEGIN
				set @strSql=@strSql+','+ case  when @vcDbColumnName='numAge' then 'year(getutcdate())-year(bintDOB)' when @vcDbColumnName='numDivisionID' then 'DM.numDivisionID' else @vcDbColumnName end+' ['+ @vcFormFieldName+'~'+ @vcDbColumnName+']'             
			END
		END
		ELSE 
		BEGIN
			SET @strSql=@strSql+','+ case  when @vcDbColumnName='numAge' then 'year(getutcdate())-year(bintDOB)' when @vcDbColumnName='numDivisionID' then 'DM.numDivisionID' else @vcDbColumnName end+' ['+ @vcFormFieldName+'~'+ @vcDbColumnName+']'             
		END
  
		SELECT TOP 1 
			@tintOrder=T1.tintOrder+1
			,@vcDbColumnName=vcDbColumnName
			,@vcFormFieldName=vcFieldName
			,@vcAssociatedControlType=vcAssociatedControlType
			,@vcListItemType=vcListItemType
			,@numListID=numListID                                   
		FROM 
			View_DynamicDefaultColumns D                                   
		INNER JOIN
			@TEMPSelectedColumns T1
		ON 
			D.numFieldID=T1.numFieldID                             
		WHERE 
			D.numFormID = 19
			AND T1.tintOrder > @tintOrder-1 
		ORDER BY 
			T1.tintOrder ASC
                                       
		IF @@rowcount=0 
			SET @tintOrder=0                                  
	END                                  
                                  
                                  
                                  
                                  
                             
                                                                        
  declare @firstRec as integer                                                                        
  declare @lastRec as integer                                                                        
 set @firstRec= (@CurrentPage-1) * @PageSize                          
 set @lastRec= (@CurrentPage*@PageSize+1)                                                                         
set @TotRecs=(select count(*) from #tempTable)                                                       
                                              
if @GetAll=0
begin           
            
set @strSql=@strSql+'FROM AdditionalContactsInformation ADC                                                       
  JOIN DivisionMaster DM ON ADC.numDivisionId = DM.numDivisionID                                                      
  JOIN CompanyInfo C ON DM.numCompanyID = C.numCompanyId                                  
   '+@WhereCondition+' join #tempTable T on T.numContactId=ADC.numContactId                                                                   
  WHERE ID > '+convert(varchar(10),@firstRec)+ ' and ID <'+ convert(varchar(10),@lastRec)                           
  
end
else
begin
  set @strSql=@strSql+'FROM AdditionalContactsInformation ADC                                                       
  JOIN DivisionMaster DM ON ADC.numDivisionId = DM.numDivisionID                                                      
  JOIN CompanyInfo C ON DM.numCompanyID = C.numCompanyId                                  
   '+@WhereCondition+' join #tempTable T on T.numContactId=ADC.numContactId'

end
 print @strSql                                                          
exec(@strSql)                                                                     
drop table #tempTable
