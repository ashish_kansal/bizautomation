/****** Object:  StoredProcedure [dbo].[USP_ManageItemList]    Script Date: 07/26/2008 16:19:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj      
--Author:Anoop Jayaraj
--Modified By:Sachin Sadhu
--History: Added One Field numListType to save  bizdoc type wise BizDoc status     (added on 12thJun)    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_manageitemlist')
DROP PROCEDURE usp_manageitemlist
GO
CREATE PROCEDURE [dbo].[USP_ManageItemList]                
@numListItemID as numeric(9) OUTPUT,                  
@numDomainID as numeric(9)=0,                     
@vcData as varchar(100)='',             
@numListID as numeric(9)=0,                      
@numUserCntID as numeric(9),
@numListType numeric(9,0),
@tintOppOrOrder TINYINT,
@bitEnforceMinOrderAmount bit = 0,
@fltMinOrderAmount FLOAT = 0,
@numListItemGroupId NUMERIC=0,
@vcColorScheme VARCHAR(500)=''            
as                    
if @numListItemID =0                    
                    
begin         
	declare @sintOrder as smallint     
	Declare @numListItemIDPK as numeric(9)   

	update ListDetails set  sintOrder=0 where sintOrder is null   
	   
	select @sintOrder=(select max(sintOrder)+1 from ListDetails where numListID=@numListID)      
      
                 
	insert into ListDetails(numListID,vcData,numCreatedBY,bintCreatedDate,numModifiedBy,bintModifiedDate,bitDelete,numDomainID,constFlag,sintOrder,numListType,tintOppOrOrder)                    
	values(@numListID,@vcData,@numUserCntID,getutcdate(),@numUserCntID,getutcdate(),0,@numDomainID,0,@sintOrder,@numListType,@tintOppOrOrder)     
  
	Set @numListItemIDPK = SCOPE_IDENTITY()
	SET @numListItemID = SCOPE_IDENTITY()

	Declare @CurrentDate as datetime      
    
	Set @CurrentDate=getutcdate()    
	
	IF @bitEnforceMinOrderAmount = 1 AND @numListID = 21
	BEGIN
		INSERT INTO SalesOrderRule
			(numDomainId, numListItemID, numListID, bitEnforceMinOrderAmount, fltMinOrderAmount)
		VALUES
			(@numDomainID, @numListItemID, @numListID, @bitEnforceMinOrderAmount, @fltMinOrderAmount)
	END
	--If @numListID=64    
	--Begin    
	--Exec usp_InsertNewChartAccountDetails @numParntAcntId=10,@numAcntType=818,@vcCatgyName=@vcData,@vcCatgyDescription=@vcData,@numOriginalOpeningBal=null,    
	--     @numOpeningBal=null,@dtOpeningDate=@CurrentDate,@bitActive=1,@numAccountId=0,@bitFixed=0,@numDomainId=@numDomainID,@numListItemID=@numListItemIDPK    
	--End    
end                    
else                    
begin                    
	update ListDetails set vcData=@vcData,                   
	numModifiedBy=@numUserCntID,                  
	bintModifiedDate=getutcdate(),
	numListType=@numListType,
	tintOppOrOrder=@tintOppOrOrder,
	numListItemGroupId=@numListItemGroupId           
	where numListItemID=@numListItemID     
	--To update Category Description in Chart_of_accounts table  
	--update Chart_Of_Accounts Set vcCatgyName=@vcData,vcCatgyDescription=@vcData Where numListItemID=@numListItemID  
	IF(@numListId=30)
	BEGIN
	    IF((SELECT COUNT(*) FROM DycFieldColorScheme WHERE numFieldID=18 AND numFormID=34 AND numDomainID=@numDomainID AND vcFieldValue=@numListItemID)=0)
		BEGIN
			IF(@vcColorScheme<>'')
			BEGIN
				INSERT INTO DycFieldColorScheme(numFieldID,numFormID,numDomainID,vcFieldValue,vcFieldValue1,vcColorScheme)
				VALUES (18,34,@numDomainID,@numListItemID,'0',@vcColorScheme)

				INSERT INTO DycFieldColorScheme(numFieldID,numFormID,numDomainID,vcFieldValue,vcFieldValue1,vcColorScheme)
				VALUES (18,35,@numDomainID,@numListItemID,'0',@vcColorScheme)

				INSERT INTO DycFieldColorScheme(numFieldID,numFormID,numDomainID,vcFieldValue,vcFieldValue1,vcColorScheme)
				VALUES (18,36,@numDomainID,@numListItemID,'0',@vcColorScheme)

				INSERT INTO DycFieldColorScheme(numFieldID,numFormID,numDomainID,vcFieldValue,vcFieldValue1,vcColorScheme)
				VALUES (18,38,@numDomainID,@numListItemID,'0',@vcColorScheme)

				INSERT INTO DycFieldColorScheme(numFieldID,numFormID,numDomainID,vcFieldValue,vcFieldValue1,vcColorScheme)
				VALUES (18,39,@numDomainID,@numListItemID,'0',@vcColorScheme)

				INSERT INTO DycFieldColorScheme(numFieldID,numFormID,numDomainID,vcFieldValue,vcFieldValue1,vcColorScheme)
				VALUES (18,40,@numDomainID,@numListItemID,'0',@vcColorScheme)

				INSERT INTO DycFieldColorScheme(numFieldID,numFormID,numDomainID,vcFieldValue,vcFieldValue1,vcColorScheme)
				VALUES (18,41,@numDomainID,@numListItemID,'0',@vcColorScheme)
			END
			ELSE
			BEGIN
				DELETE FROM DycFieldColorScheme WHERE numFieldID=18 AND numFormID IN(34,35,36,38,39,40,41) AND numDomainID=@numDomainID AND vcFieldValue=@numListItemID
			END
		END
		ELSE
		BEGIN
			IF(@vcColorScheme<>'')
			BEGIN
				UPDATE 
					DycFieldColorScheme
				SET
					vcColorScheme=@vcColorScheme
				WHERE
					numDomainID=@numDomainID AND
					vcFieldValue=@numListItemID AND
					numFieldID=18 AND
					numFormID IN(34,35,36,38,39,40,41)
			END
			ELSE
			BEGIN
				DELETE FROM DycFieldColorScheme WHERE numFieldID=18 AND numFormID IN(34,35,36,38,39,40,41) AND numDomainID=@numDomainID AND vcFieldValue=@numListItemID
			END
		END
	END     
	IF @bitEnforceMinOrderAmount = 1 AND @numListID = 21
	BEGIN
		IF EXISTS(SELECT * FROM SalesOrderRule WHERE numListItemID=@numListItemID)
		BEGIN
			UPDATE SalesOrderRule SET 
				bitEnforceMinOrderAmount = @bitEnforceMinOrderAmount, fltMinOrderAmount = @fltMinOrderAmount 
			WHERE numListItemID=@numListItemID    
		END
		ELSE
		BEGIN
			INSERT INTO SalesOrderRule
				(numDomainId, numListItemID, numListID, bitEnforceMinOrderAmount, fltMinOrderAmount)
			VALUES
				(@numDomainID, @numListItemID, @numListID, @bitEnforceMinOrderAmount, @fltMinOrderAmount)
		END
	END    
	ELSE IF @bitEnforceMinOrderAmount = 0 AND @numListID = 21
	BEGIN
		IF EXISTS(SELECT * FROM SalesOrderRule WHERE numListItemID=@numListItemID)
		BEGIN
			UPDATE SalesOrderRule SET 
				bitEnforceMinOrderAmount = @bitEnforceMinOrderAmount, fltMinOrderAmount = 0 
			WHERE numListItemID=@numListItemID    
		END
	END             
end
GO
