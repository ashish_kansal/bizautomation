/****** Object:  StoredProcedure [dbo].[usp_GetContactForCaseWhenKeyChange]    Script Date: 07/26/2008 16:16:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getcontactforcasewhenkeychange')
DROP PROCEDURE usp_getcontactforcasewhenkeychange
GO
CREATE PROCEDURE [dbo].[usp_GetContactForCaseWhenKeyChange]
	@numDomainID numeric(9)=0,
	@numContactID numeric(9)=0  
--
AS
SELECT     AdditionalContactsInformation.vcFirstName, AdditionalContactsInformation.vcLastName,AdditionalContactsInformation.numContactID
FROM AdditionalContactsInformation INNER JOIN OpportunityMaster 
ON AdditionalContactsInformation.numContactId = OpportunityMaster.numContactId
WHERE AdditionalContactsInformation.numDomainID=@numDomainID 
AND AdditionalContactsInformation.numContactID=@numContactID
GO
