/****** Object:  StoredProcedure [dbo].[USP_GetTeamMembers]    Script Date: 07/26/2008 16:18:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getteammembers')
DROP PROCEDURE usp_getteammembers
GO
CREATE PROCEDURE [dbo].[USP_GetTeamMembers]    
@numUserCntId as numeric(9)=0,    
@numDomainId as numeric(9)=0,    
@intType as tinyint=0    
as    
    
select  numContactID as  numUserID, vcFirstName+' '+ vcLastName as Name from AdditionalContactsInformation     
join Usermaster on numContactID=numUserDetailId    
where bitActivateFlag=1 and numTeam in (select F.numTeam from ForReportsByTeam F           
 where F.numUserCntID=@numUserCntId and F.numDomainid=@numDomainId and F.tintType=@intType)
GO
