GO
/****** Object:  StoredProcedure [dbo].[usp_GetParentOrgForCurrentOrg]    Script Date: 03/05/2010 20:22:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--Created By: Debasish Nag                              
--Created On: 18th Jan 2006                    
--Purpose: Return the Parent Organization as indicated in Association To          
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getparentorgforcurrentorg')
DROP PROCEDURE usp_getparentorgforcurrentorg
GO
CREATE PROCEDURE [dbo].[usp_GetParentOrgForCurrentOrg]
    @numDomainID NUMERIC,
    @numAssociateFromDivisionID NUMERIC
AS 
    BEGIN

            SELECT  Ld.vcData,
                    CI.vcCompanyName,
                    DM.vcDivisionName,
                    DM.numDivisionId,
                    DM.[tintCRMType]
            FROM    
				CompanyAssociations CA
			INNER JOIN
				DivisionMaster DM
			ON
				CA.[numDivisionID] = DM.numDivisionId
			INNER JOIN
                CompanyInfo CI
			ON
				DM.numCompanyID = CI.numCompanyId
            INNER JOIN 
				ListDetails Ld
            ON
				CA.[numAssoTypeLabel] = Ld.numListItemID    
            WHERE   CA.[numAssociateFromDivisionID] = @numAssociateFromDivisionID
                    AND CA.numDomainID = @numDomainID            
                    AND [bitTypeLabel]=1 
END

