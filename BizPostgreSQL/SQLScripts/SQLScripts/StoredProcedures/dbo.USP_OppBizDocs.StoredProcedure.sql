/****** Object:  StoredProcedure [dbo].[USP_OppBizDocs]    Script Date: 03/25/2009 15:23:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj                    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_oppbizdocs')
DROP PROCEDURE usp_oppbizdocs
GO
CREATE PROCEDURE [dbo].[USP_OppBizDocs]
(                        
	@byteMode as tinyint=null,                        
	@numOppId as numeric(9)=null,                        
	@numOppBizDocsId as numeric(9)=null,
	@ClientTimeZoneOffset AS INT=0,
	@numDomainID AS NUMERIC(18,0) = NULL,
	@numUserCntID AS NUMERIC(18,0) = NULL
)                        
AS      
BEGIN
	DECLARE @ParentBizDoc AS NUMERIC(9)=0
	DECLARE @lngJournalId AS NUMERIC(18,0)
	DECLARE @numDivisionID NUMERIC(18,0)
	
	SELECT @numDivisionID=numDivisionId FROM OpportunityMaster WHERE numOppId=@numOppId
    
	SELECT 
		ISNULL(CompanyInfo.numCompanyType,0) AS numCompanyType,
		ISNULL(CompanyInfo.vcProfile,0) AS vcProfile,
		ISNULL(OpportunityMaster.numAccountClass,0) AS numAccountClass
	FROM 
		OpportunityMaster 
	JOIN 
		DivisionMaster 
	ON 
		DivisionMaster.numDivisionId = OpportunityMaster.numDivisionId
	JOIN 
		CompanyInfo
	ON 
		CompanyInfo.numCompanyId = DivisionMaster.numCompanyID
	WHERE 
		numOppId = @numOppId
		                                  
	IF @byteMode= 1                        
	BEGIN 
		DECLARE @numBizDocID AS INT
		DECLARE @bitFulfilled AS BIT
		Declare @tintOppType as tinyint
		DECLARE @tintShipped AS BIT
		DECLARE @bitAuthoritativeBizDoc AS BIT

		SELECT @tintOppType=tintOppType,@tintShipped=ISNULL(tintshipped,0) FROM OpportunityMaster WHERE  numOppId=@numOppId  and numDomainID= @numDomainID 
		SELECT @numBizDocID = numBizDocId, @bitFulfilled=bitFulfilled, @bitAuthoritativeBizDoc=ISNULL(bitAuthoritativeBizDocs,0) FROM OpportunityBizDocs WHERE numOppBizDocsId = @numOppBizDocsId

		IF ISNULL(@tintShipped,0) = 1 AND (@numBizDocID=296 OR ISNULL(@bitAuthoritativeBizDoc,0)=1)
		BEGIN
			RAISERROR('NOT_ALLOWED_TO_DELETE_AUTHORITATIVE_BIZDOC_AFTER_ORDER_IS_CLOSED',16,1)
			RETURN
		END

		IF EXISTS (SELECT numDepositeDetailID FROM dbo.DepositeDetails WHERE numOppBizDocsID = @numOppBizDocsId)
		BEGIN
			RAISERROR('PAID',16,1);
			RETURN
		END

		IF EXISTS (SELECT OBPD.* FROM OpportunityBizDocsPaymentDetails OBPD INNER JOIN EmbeddedCost EC ON OBPD.numBizDocsPaymentDetId=EC.numBizDocsPaymentDetId WHERE EC.numOppBizDocID=@numOppBizDocsId)
		BEGIN
			RAISERROR('PAID',16,1);
			RETURN
		END

		DECLARE @tintCommitAllocation AS TINYINT
		DECLARE @bitAllocateInventoryOnPickList AS BIT

		SELECT @tintCommitAllocation=ISNULL(tintCommitAllocation,1) FROM Domain WHERE numDomainId=@numDomainID
		SELECT @bitAllocateInventoryOnPickList=ISNULL(bitAllocateInventoryOnPickList,0) FROM DivisionMaster WHERE numDivisionID=@numDivisionID

		IF ISNULL(@tintCommitAllocation,0)=2 AND ISNULL(@bitAllocateInventoryOnPickList,0) = 1 AND @numBizDocID=29397 AND (SELECT COUNT(*) FROM OpportunityBizDocs WHERE numOppId=@numOppId AND numBizDocId=296 AND numSourceBizDocId=@numOppBizDocsId) > 0
		BEGIN
			RAISERROR('NOT_ALLOWED_TO_DELETE_PICK_LIST_AFTER_FULFILLMENT_BIZDOC_IS_GENERATED_AGAINST_IT',16,1)
			RETURN
		END

		IF ISNULL(@tintCommitAllocation,0)=2 AND ISNULL(@bitAllocateInventoryOnPickList,0) = 1 AND @numBizDocID=29397
		BEGIN
			-- Revert Allocation
			EXEC USP_RevertAllocationPickList @numDomainID,@numOppID,@numOppBizDocsId,@numUserCntID
		END

		DECLARE @TEMP TABLE
		(
			ID INT IDENTITY(1,1)
			,numOppBizDocsId NUMERIC(18,0)
		)

		INSERT INTO @TEMP
		(
			numOppBizDocsId
		)
		SELECT
			numOppBizDocsId
		FROM
			OpportunityBizDocs 
		WHERE 
			numSourceBizDocId= @numOppBizDocsId

		BEGIN TRY
		BEGIN TRANSACTION

			DECLARE @i AS INT = 1
			DECLARE @iCount AS INT

			SELECT @iCount = COUNT(*) FROM @TEMP

			WHILE @i <= @iCount
			BEGIN
				SELECT @ParentBizDoc=numOppBizDocsId FROM @TEMP WHERE ID = @i

				IF NOT EXISTS (SELECT numDepositeDetailID FROM dbo.DepositeDetails WHERE numOppBizDocsID = @ParentBizDoc)
				BEGIN
					EXEC USP_OppBizDocs @byteMode=@byteMode,@numOppId=@numOppId,@numOppBizDocsId=@ParentBizDoc,@ClientTimeZoneOffset=@ClientTimeZoneOffset,@numDomainID=@numDomainID,@numUserCntID=@numUserCntID
				END

				SET @i = @i + 1
			END

			--IF it's as sales order and BizDoc is of type of fulfillment and already fulfilled revert items to allocations
			IF @tintOppType = 1 AND @numBizDocID = 296 AND @bitFulfilled = 1
			BEGIN
				EXEC USP_OpportunityBizDocs_RevertFulFillment @numDomainID,@numUserCntID,@numOppId,@numOppBizDocsId
			END

			IF @numBizDocID = 29397
			BEGIN
				UPDATE
					OI
				SET
					OI.numQtyPicked = (CASE 
											WHEN ISNULL(OI.numQtyPicked,0) > ISNULL(OBDI.numUnitHour,0) 
											THEN (ISNULL(OI.numQtyPicked,0) - ISNULL(OBDI.numUnitHour,0)) 
											ELSE 0 
										END)
				FROM	
					OpportunityBizDocItems OBDI
				INNER JOIN
					OpportunityItems OI
				ON
					OBDI.numOppItemID = OI.numoppitemtCode
				WHERE
					OBDI.numOppBizDocID=@numOppBizDocsId
			END

			DELETE FROM [ProjectsOpportunities] WHERE numOppBizDocID=@numOppBizDocsId
			DELETE FROM [ShippingReportItems] WHERE [numShippingReportId] IN  (SELECT numShippingReportId from [ShippingReport] WHERE [numOppBizDocId] = @numOppBizDocsId)
			DELETE FROM [ShippingBox] WHERE [numShippingReportId] IN  (SELECT numShippingReportId from [ShippingReport] WHERE [numOppBizDocId] = @numOppBizDocsId)
			DELETE FROM [ShippingReport] WHERE [numOppBizDocId] = @numOppBizDocsId
			DELETE  FROM OpportunityBizDocItems WHERE numOppBizDocID=@numOppBizDocsId              
			DELETE FROM OpportunityRecurring WHERE numOppBizDocID=@numOppBizDocsId and tintRecurringType=4
			UPDATE dbo.OpportunityRecurring SET numOppBizDocID=NULL WHERE numOppBizDocID=@numOppBizDocsId
			DELETE FROM [RecurringTransactionReport] WHERE [numRecTranBizDocID] = @numOppBizDocsId
				--Delete All entries for current bizdoc
			--EXEC [USP_DeleteEmbeddedCost] @numOppBizDocsId, 0, @numDomainID 
			DELETE OBPD FROM OpportunityBizDocsPaymentDetails OBPD INNER JOIN EmbeddedCost EC ON OBPD.numBizDocsPaymentDetId=EC.numBizDocsPaymentDetId WHERE EC.numOppBizDocID=@numOppBizDocsId
			DELETE ECI FROM [EmbeddedCostItems] ECI INNER JOIN EmbeddedCost EC ON ECI.numEmbeddedCostID=EC.numEmbeddedCostID WHERE EC.[numOppBizDocID]=@numOppBizDocsId AND EC.[numDomainID] = @numDomainID
			DELETE FROM [EmbeddedCost] WHERE [numOppBizDocID] = @numOppBizDocsId	AND [numDomainID] = @numDomainID
			DELETE OBDD	FROM OpportunityBizDocsDetails OBDD INNER JOIN EmbeddedCost EC ON OBDD.numBizDocsPaymentDetId = EC.numBizDocsPaymentDetId WHERE	numOppBizDocID = @numOppBizDocsId

		

			--Credit Balance
			DECLARE @monCreditAmount AS DECIMAL(20,5);
			SET @monCreditAmount=0
			SET @numDivisionID=0

			SELECT 
				@monCreditAmount=ISNULL(oppBD.monCreditAmount,0)
				,@numDivisionID=Om.numDivisionID
				,@numOppId=OM.numOppId 
			FROM 
				OpportunityBizDocs oppBD 
			JOIN 
				OpportunityMaster Om 
			ON 
				OM.numOppId=oppBD.numOppId 
			WHERE
				numOppBizDocsId = @numOppBizDocsId

			DELETE FROM [CreditBalanceHistory] where numOppBizDocsId=@numOppBizDocsId

 
			IF @tintOppType=1 --Sales
				UPDATE DivisionMaster SET monSCreditBalance=isnull(monSCreditBalance,0) + isnull(@monCreditAmount,0) where numDivisionID=@numDivisionID and numDomainID=@numDomainID
			ELSE IF @tintOppType=2 --Purchase
				UPDATE DivisionMaster SET monPCreditBalance=isnull(monPCreditBalance,0) + isnull(@monCreditAmount,0) where numDivisionID=@numDivisionID and numDomainID=@numDomainID


			DELETE FROM DocumentWorkflow WHERE numDocID=@numOppBizDocsId AND cDocType='B'      
			DELETE BA FROM BizDocAction BA INNER JOIN BizActionDetails BAD ON BA.numBizActionId=BAD.numBizActionId WHERE BAD.numOppBizDocsId =@numOppBizDocsId and btDocType=1
			DELETE FROM BizActionDetails where numOppBizDocsId=@numOppBizDocsId and btDocType=1


			DELETE GJD FROM General_Journal_Details GJD INNER JOIN General_Journal_Header GJH ON GJD.numJournalID=GJH.numJournal_Id WHERE GJH.numOppId=@numOppId AND GJH.numOppBizDocsId=@numOppBizDocsId AND GJH.numDomainId=@numDomainID
			DELETE FROM dbo.General_Journal_Header WHERE numOppId=@numOppId AND numOppBizDocsId=@numOppBizDocsId AND numDomainId= @numDomainID

			DELETE FROM OpportunityBizDocs WHERE numOppBizDocsId=@numOppBizDocsId                        

		COMMIT TRANSACTION
		END TRY
		BEGIN CATCH
			DECLARE @ErrorMessage NVARCHAR(4000)
			DECLARE @ErrorNumber INT
			DECLARE @ErrorSeverity INT
			DECLARE @ErrorState INT
			DECLARE @ErrorLine INT
			DECLARE @ErrorProcedure NVARCHAR(200)

			IF @@TRANCOUNT > 0
				ROLLBACK TRANSACTION

			SELECT 
				@ErrorMessage = ERROR_MESSAGE(),
				@ErrorNumber = ERROR_NUMBER(),
				@ErrorSeverity = ERROR_SEVERITY(),
				@ErrorState = ERROR_STATE(),
				@ErrorLine = ERROR_LINE(),
				@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

			RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);
		END CATCH             
	END                        
                        
if @byteMode= 2                        
begin  

DECLARE @BaseCurrencySymbol nVARCHAR(3);SET @BaseCurrencySymbol='$'
DECLARE @numCurrencyID NUMERIC(18,0)
DECLARE @numARContactPosition NUMERIC(18,0)

SELECT @numCurrencyID=ISNULL(numCurrencyID,0),@numARContactPosition=ISNULL(numARContactPosition,0) FROM Domain WHERE numDomainId=@numDomainId
SELECT @BaseCurrencySymbol = ISNULL(varCurrSymbol,'$') FROM dbo.Currency WHERE numCurrencyID = @numCurrencyID
																					                      
select *, isnull(X.monPAmount,0)-X.monAmountPaid as BalDue from (select  opp.numOppBizDocsId,                        
 opp.numOppId,                        
 opp.numBizDocId,                        
 ISNULL((SELECT TOP 1 numOrientation FROM BizDocTemplate WHERE numBizDocTempID = Opp.numBizDocTempID),0) AS numOrientation, 
 ISNULL((SELECT TOP 1 vcTemplateName FROM BizDocTemplate WHERE numBizDocTempID = Opp.numBizDocTempID),0) AS vcTemplateName, 
 ISNULL((SELECT TOP 1 bitKeepFooterBottom FROM BizDocTemplate WHERE numBizDocTempID = Opp.numBizDocTempID),0) AS bitKeepFooterBottom,                  
 oppmst.vcPOppName,                        
 ISNULL(ListDetailsName.vcName,mst.vcData) as BizDoc,                        
 opp.numCreatedBy,                        
DATEADD(minute,-@ClientTimeZoneOffset,opp.dtCreatedDate) CreatedDate,
 opp.vcRefOrderNo,                       
 vcBizDocID,
 CASE WHEN LEN(ISNULL(vcBizDocName,''))=0 THEN vcBizDocID ELSE vcBizDocName END vcBizDocName,      
  [dbo].[GetDealAmount](@numOppId ,getutcdate(),opp.numOppBizDocsId ) as monPAmount,
  CASE 
	WHEN tintOppType=1  
	THEN (CASE 
			WHEN (ISNULL(numAuthoritativeSales,0)=numBizDocId OR numBizDocId=296) 
			THEN 'Authoritative' + CASE WHEN ISNULL(Opp.tintDeferred,0)=1 THEN ' (Deferred)' ELSE '' END 
			WHEN opp.numBizDocId =  304 THEN 'Deferred-Authoritative'
			ELSE 'Non-Authoritative' END ) 
  when tintOppType=2  then (case when isnull(numAuthoritativePurchase,0)=numBizDocId then 'Authoritative' + Case when isnull(Opp.tintDeferred,0)=1 then ' (Deferred)' else '' end else 'Non-Authoritative' end )  end as BizDocType,
  isnull(monAmountPaid,0) monAmountPaid, 
  ISNULL(C.varCurrSymbol,'') varCurrSymbol ,
  ISNULL(Opp.[bitAuthoritativeBizDocs],0) bitAuthoritativeBizDocs,
  ISNULL(oppmst.[tintshipped] ,0) tintshipped,isnull(opp.numShipVia,0) as numShipVia,oppmst.numDivisionId,isnull(Opp.tintDeferred,0) as tintDeferred,
  ISNULL(Opp.numBizDocTempID,0) AS numBizDocTempID
  ,con.numContactid
  ,isnull(con.vcEmail,'') AS vcEmail
  ,ISNULL(Opp.numBizDocStatus,0) AS numBizDocStatus,
  CASE WHEN ISNUMERIC((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(oppmst.intBillingDays,0))) = 1
 	   THEN (SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(oppmst.intBillingDays,0))
	   ELSE 0
  END AS numBillingDaysName,
  DATEADD(MINUTE,-1 * @ClientTimeZoneOffset,opp.dtFromDate) dtFromDate,
  DATEADD(MINUTE,-1 * @ClientTimeZoneOffset,opp.dtFromDate) AS BillingDate,
  ISNULL(C.fltExchangeRate,1) fltExchangeRateCurrent,
  ISNULL(oppmst.fltExchangeRate,1) fltExchangeRateOfOrder,
  @BaseCurrencySymbol BaseCurrencySymbol,0 AS numReturnHeaderID,0 AS tintReturnType,ISNULL(Opp.fltExchangeRateBizDoc,ISNULL(oppmst.fltExchangeRate,1)) AS fltExchangeRateBizDoc,
  CASE WHEN (SELECT COUNT(*) FROM dbo.ShippingReport WHERE numOppBizDocId = opp.numOppBizDocsId AND numDomainID = @numDomainID) > 0 THEN 1 ELSE 0 END AS [IsShippingReportGenerated],
  --CASE WHEN (SELECT ISNULL(vcTrackingNo,'') FROM dbo.OpportunityBizDocs WHERE numOppBizDocsId = opp.numOppBizDocsId) <> '' THEN 1 ELSE 0 END AS [ISTrackingNumGenerated]
  CASE WHEN (SELECT COUNT(*) FROM dbo.ShippingBox 
			 WHERE numShippingReportId IN (SELECT numShippingReportId FROM dbo.ShippingReport WHERE numOppBizDocId = opp.numOppBizDocsId AND numDomainID = @numDomainID)
			 AND LEN(ISNULL(vcTrackingNumber,'')) > 0) > 0 THEN 1 ELSE 0 END AS [ISTrackingNumGenerated],
  ISNULL((SELECT MAX(numShippingReportId) from ShippingReport SR 
		  WHERE SR.numOppBizDocId = opp.numoppbizdocsid 
		  AND SR.numDomainID = oppmst.numDomainID),0) AS numShippingReportId,
  ISNULL((SELECT MAX(SB.numShippingReportId) FROM [ShippingBox] SB 
		  JOIN dbo.ShippingReport SR ON SB.numShippingReportId = SR.numShippingReportId 
		  where SR.numOppBizDocId = opp.numoppbizdocsid AND SR.numDomainID = oppmst.numDomainID 
		  AND LEN(ISNULL(SB.vcTrackingNumber,''))>0),0) AS numShippingLabelReportId			 
 ,ISNULL(STUFF(( SELECT  ',' +  CASE WHEN LEN(ISNULL(W.[vcWareHouse],'')  + ':' + ISNULL([WL].[vcLocation],'')) > 0 THEN ISNULL(W.[vcWareHouse],'')  + ':' + ISNULL([WL].[vcLocation],'') ELSE '' END
                FROM    [dbo].[WarehouseLocation] AS WL
				JOIN [dbo].[Warehouses] W ON [WL].[numWarehouseID] = [W].[numWareHouseID]
                WHERE  [WL].[numWLocationID] IN (
                        SELECT  WHI.[numWLocationID]
                        FROM    [dbo].[WareHouseItems] AS WHI
                                JOIN [dbo].[OpportunityItems] AS OI ON [WHI].[numWareHouseItemID] = [OI].[numWarehouseItmsID]
                        WHERE   [OI].[numOppId] = oppmst.[numOppId])
              FOR
                XML PATH('')
              ), 1, 1, ''),'') AS [vcLocation],
		(CASE WHEN ISNULL(RecurrenceConfiguration.numRecConfigID,0) = 0 THEN 0 ELSE 1 END) AS bitRecur,
		ISNULL(Opp.bitRecurred,0) AS bitRecurred,
		ISNULL((SELECT COUNT(*) FROM OpportunityBizDocs WHERE numOppID=@numOppID AND ISNULL(numDeferredBizDocID,0) = ISNULL(opp.numOppBizDocsId,0)),0) AS intInvoiceForDiferredBizDoc,
		ISNULL(Opp.bitShippingGenerated,0) AS bitShippingGenerated,
		ISNULL(oppmst.intUsedShippingCompany,0) as intUsedShippingCompany,
		numSourceBizDocId,
		ISNULL((SELECT TOP 1 vcBizDocID FROM OpportunityBizDocs WHERE numOppBizDOcsId=opp.numSourceBizDocId),'NA') AS vcSourceBizDocID,
		ISNULL(oppmst.numShipmentMethod,0) numShipmentMethod,
		ISNULL(oppmst.numShippingService,0) numShippingService,
		(CASE WHEN (SELECT COUNT(*) FROM OpportunityBizDocs OBInner WHERE OBInner.numOppID=@numOppID AND OBInner.numBizDocId=296 AND OBInner.numSourceBizDocId=opp.numOppBizDocsId AND opp.numBizDocId=29397) > 0 THEN 1 ELSE 0 END) AS IsFulfillmentGeneratedOnPickList
 FROM                         
 OpportunityBizDocs  opp                        
 left join ListDetails mst                        
 on mst.numListItemID=opp.numBizDocId  
 LEFT JOIN ListDetailsName
 ON ListDetailsName.numListItemID=opp.numBizDocId 
 AND ListDetailsName.numDomainID=@numDomainID
 join OpportunityMaster oppmst                        
 on oppmst.numOppId= opp.numOppId                                
 left join  AuthoritativeBizDocs  AB
 on AB.numDomainId=  oppmst.numDomainID             
 LEFT OUTER JOIN [Currency] C
  ON C.numCurrencyID = oppmst.numCurrencyID
  LEFT JOIN AdditionalContactsInformation con ON con.numContactid = oppmst.numContactId
  LEFT JOIN
	RecurrenceConfiguration
  ON
	RecurrenceConfiguration.numOppBizDocID = opp.numOppBizDocsId
where opp.numOppId=@numOppId

UNION ALL

select  0 AS numOppBizDocsId,                        
 RH.numOppId,                        
 0 AS numBizDocId,                        
 1 AS numOrientation,  
 '' AS vcTemplateName,
 0 AS bitKeepFooterBottom,                   
 '' AS vcPOppName,                        
 'RMA' as BizDoc,                        
 RH.numCreatedBy,                        
DATEADD(minute,-@ClientTimeZoneOffset,RH.dtCreatedDate) CreatedDate,
 '' vcRefOrderNo,                       
 RH.vcRMA AS vcBizDocID,
 RH.vcRMA AS vcBizDocName,      
  ISNULL(monAmount,0) as monPAmount,
  'RMA' as BizDocType,
  0 AS monAmountPaid, 
  '' varCurrSymbol ,
  0 bitAuthoritativeBizDocs,
  0 tintshipped,0 as numShipVia,RH.numDivisionId,0 as tintDeferred,
  0 AS numBizDocTempID,con.numContactid,isnull(con.vcEmail,'') AS vcEmail,0 AS numBizDocStatus,
  0 AS numBillingDaysName,RH.dtCreatedDate AS dtFromDate,
  RH.dtCreatedDate AS BillingDate,
  1 fltExchangeRateCurrent,
  1 fltExchangeRateOfOrder,
  @BaseCurrencySymbol BaseCurrencySymbol,RH.numReturnHeaderID,RH.tintReturnType,1 AS fltExchangeRateBizDoc
  ,0 [IsShippingReportGenerated],0 [ISTrackingNumGenerated],
  0 AS numShippingReportId,
  0 AS numShippingLabelReportId			 
  ,ISNULL(STUFF(( SELECT  ',' +  CASE WHEN LEN(ISNULL(W.[vcWareHouse],'')  + ':' + ISNULL([WL].[vcLocation],'')) > 0 THEN ISNULL(W.[vcWareHouse],'')  + ':' + ISNULL([WL].[vcLocation],'') ELSE '' END
                FROM    [dbo].[WarehouseLocation] AS WL
				JOIN [dbo].[Warehouses] W ON [WL].[numWarehouseID] = [W].[numWareHouseID]
                WHERE  [WL].[numWLocationID] IN (
                        SELECT  WHI.[numWLocationID]
                        FROM    [dbo].[WareHouseItems] AS WHI
                                JOIN [dbo].[OpportunityItems] AS OI ON [WHI].[numWareHouseItemID] = [OI].[numWarehouseItmsID]
                        WHERE   [OI].[numOppId] = RH.[numOppId])
              FOR
                XML PATH('')
              ), 1, 1, ''),'') AS [vcLocation],
			  0 AS bitRecur,
			  0 AS bitRecurred,
		0 AS intInvoiceForDiferredBizDoc ,
		0 AS bitShippingGenerated,
		0 as intUsedShippingCompany,
		0 AS numSourceBizDocId,
		'NA' AS vcSourceBizDocID,
		0 AS numShipmentMethod,
		0 AS numShippingService
		,0 AS IsFulfillmentGeneratedOnPickList
from                         
 ReturnHeader RH                        
 LEFT JOIN AdditionalContactsInformation con ON con.numContactid = RH.numContactId
  
where RH.numOppId=@numOppId
)X ORDER BY CreatedDate ASC       
                        
END
END
GO