/****** Object:  StoredProcedure [dbo].[usp_UpdateCaseDetails]    Script Date: 07/26/2008 16:21:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_updatecasedetails')
DROP PROCEDURE usp_updatecasedetails
GO
CREATE PROCEDURE [dbo].[usp_UpdateCaseDetails]  
 @intTargetResolveDate int=0,  
 @textSubject text='',  
 @vcStatus numeric,  
 @vcPriority varchar(25)='',  
 @textDesc text='',  
 @textInternalComments text='',  
 @vcReason varchar(250)='',  
 @vcOrigin varchar(50)='',  
 @vcType varchar(50)='',  
 @vcAssignTo varchar(50)='',  
 @numCaseID numeric(9)=0,  
 @numModifiedBy numeric(9)=0,  
 @bintModifiedDate bigint=0,  
 @vcClosedDate varchar(10)='',  
 @numSolnID numeric  
 --@txtSolution varchar(150)=''    
--
AS  
--Insertion into Table    
	
	if (@vcClosedDate='' )  
		Update Cases set intTargetResolveDate=@intTargetResolveDate,textSubject=@textSubject,  
		numStatus=@vcStatus, numPriority=@vcPriority,textDesc=@textDesc,textInternalComments=@textInternalComments,  
		numReason=@vcReason,numOrigin=@vcOrigin,numType=@vcType,numAssignedTo=@vcAssignTo,  
		numModifiedBy=@numModifiedBy,bintModifiedDate=@bintModifiedDate  
		WHERE numCaseID=@numCaseID  
		
	else  
		Update Cases set intTargetResolveDate=@intTargetResolveDate,textSubject=@textSubject,  
		numStatus=@vcStatus, numPriority=@vcPriority,textDesc=@textDesc,textInternalComments=@textInternalComments,  
		numReason=@vcReason,numOrigin=@vcOrigin,numType=@vcType,numAssignedTo=@vcAssignTo,  
		numModifiedBy=@numModifiedBy,bintModifiedDate=@bintModifiedDate  
		WHERE numCaseID=@numCaseID  
		
	if @numSolnID=0  
		Delete from CaseSolutions where numCaseID=@numCaseID  
	else  
		Declare @numID as numeric  
		set @numID=0  
		select @numID=numSolnID from CaseSolutions where numCaseID=@numCaseID  
		Print @numID  
	If @numID=0  
		Insert into CaseSolutions(numCaseID,numSolnID) values (@numCaseID,@numSolnID)  
	Else  
		Update CaseSolutions set numSolnID=@numSolnID where numCaseID=@numCaseID
GO
