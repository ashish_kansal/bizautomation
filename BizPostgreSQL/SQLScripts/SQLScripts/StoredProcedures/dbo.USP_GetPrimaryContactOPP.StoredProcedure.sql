/****** Object:  StoredProcedure [dbo].[USP_GetPrimaryContactOPP]    Script Date: 07/26/2008 16:18:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getprimarycontactopp')
DROP PROCEDURE usp_getprimarycontactopp
GO
CREATE PROCEDURE [dbo].[USP_GetPrimaryContactOPP]
@OpportunityId numeric
as
select
A.vcFirstName,     
A.vcLastName,  
(A.numPhone+','+ A.numPhoneExtension)as numPhone,       
 A.vcEmail,  
 C.vcCompanyName,    
 A.vcFax,  
A.charSex,       
 A.bintDOB,      
 dbo.GetAge(A.bintDOB, getutcdate()) as Age,  
 A.txtNotes,  
 A.NumHomePhone,      
 (A.vcAsstFirstName+' '+A.vcAsstLastName)as vcAsstName,      
 (A.numAsstPhone+','+A.numAsstExtn)as numAsstPhone,                                                        
    A.vcAsstEmail,      
 A.charSex,  
 isnull(vcStreet,'') + isnull(vcCity,'') + ' ,'
			+ isnull(dbo.fn_GetState(numState),'') + ' '
			+ isnull(vcPostalCode,'') + ' <br>'
			+ isnull(dbo.fn_GetListItemName(numCountry),'') [Address],
  a.numcontacttype 
FROM opportunitymaster opp        
  join additionalContactsinformation a on        
  a.numContactId=opp.numContactId      
  join DivisionMaster D      
  on a.numDivisionID =D.numDivisionID      
  join CompanyInfo C      
  on D.numCompanyID=C.numCompanyID     
  LEFT JOIN dbo.AddressDetails AD ON AD.numRecordID = A.numContactId
                                               AND AD.tintAddressOf = 1
                                               AND AD.tintAddressType = 0
                                               AND AD.bitIsPrimary = 1
                                               AND AD.numDomainID = A.numDomainID  
  WHERE opp.numOppId=@OpportunityId
GO
