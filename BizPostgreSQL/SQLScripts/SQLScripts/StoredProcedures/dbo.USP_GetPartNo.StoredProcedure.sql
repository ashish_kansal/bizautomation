/****** Object:  StoredProcedure [dbo].[USP_GetPartNo]    Script Date: 07/26/2008 16:18:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getpartno')
DROP PROCEDURE usp_getpartno
GO
CREATE PROCEDURE [dbo].[USP_GetPartNo]     
@numItemCode as numeric(9),    
@numDivisionID as numeric(9),  
@vcPartNo as varchar(100) output,  
@monCost as DECIMAL(20,5) output    
as    
 set @vcPartNo=''
 set @monCost=0    
    
 select @vcPartNo=isnull(vcPartNo,''),@monCost=isnull(monCost,0) from Vendor where numVendorID=@numDivisionID and numItemCode=@numItemCode
GO
