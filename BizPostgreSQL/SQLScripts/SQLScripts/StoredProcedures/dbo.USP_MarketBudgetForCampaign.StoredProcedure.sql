/****** Object:  StoredProcedure [dbo].[USP_MarketBudgetForCampaign]    Script Date: 07/26/2008 16:20:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created By Siva      
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_marketbudgetforcampaign')
DROP PROCEDURE usp_marketbudgetforcampaign
GO
CREATE PROCEDURE [dbo].[USP_MarketBudgetForCampaign]  
(@numCampaignId as numeric(9)=0,  
 @numDomainId as numeric(9)=0,    
 @numListItemID as numeric(9)=0,    
 @bitMarketCampaign as bit output,      
 @monmonthlyAmt as float=0 Output,      
 @monTotalYearlyAmt as float=0 Output      
 )      
As      
Begin      
       
 Declare @numMarketBudgetId as numeric(9)      
 Declare @OppMonthAmount as DECIMAL(20,5)      
 Declare @CampaignCost as DECIMAL(20,5)      
 Declare @BudgetAmt as DECIMAL(20,5)      
 Declare @TotalBudgetAmt as DECIMAL(20,5)      
     
 --Select @numMarketBudgetId=numMarketBudgetId,@bitMarketCampaign=bitMarketCampaign From MarketBudgetMaster Where numDomainId=@numDomainId  --   intFiscalYear=year(getutcdate()) And 
Select @bitMarketCampaign=bitMarketCampaign From MarketBudgetMaster Where numDomainId=@numDomainId 
 If  @bitMarketCampaign=1      --@numMarketBudgetId is not null And
 Begin      
  
 Select @CampaignCost=monCampaignCost From CampaignMaster Where numCampaignId=@numCampaignId   
 Select @BudgetAmt=(Select isnull(monAmount,0) From MarketbudgetMaster MBM      
  inner join MarketBudgetDetails MBD on MBM.numMarketBudgetId=MBD.numMarketId       
  Where MBD.intYear=year(getutcdate()) And MBD.tintMonth=month(getutcdate())  And MBD.numListItemID= @numListItemID And MBM.numDomainId=@numDomainId) 
 Select @TotalBudgetAmt=(Select sum(monAmount) From MarketbudgetMaster MBM      
  inner join MarketBudgetDetails MBD on MBM.numMarketBudgetId=MBD.numMarketId       
  Where MBD.intYear=year(getutcdate())  And MBD.numListItemID= @numListItemID And MBM.numDomainId=@numDomainId) 


----  Select @BudgetAmt=(case when  month(getutcdate())=1 then monJan         
----  when month(getutcdate())=2 then monFeb      
----  when month(getutcdate())=3 then monMar      
----  when month(getutcdate())=4 then monApr      
----  when month(getutcdate())=5 then monMay      
----  when month(getutcdate())=6 then monJune      
----  when month(getutcdate())=7 then monJuly      
----  when month(getutcdate())=8 then monAug      
----  when month(getutcdate())=9 then monSep      
----  when month(getutcdate())=10 then monOct      
----  when month(getutcdate())=11 then monNov      
----  when month(getutcdate())=12 then monDec      
----  End) ,@TotalBudgetAmt=monTotal From MarketbudgetMaster MBM      
----  inner join MarketBudgetDetails MBD on MBM.numMarketBudgetId=MBD.numMarketId      
     
        
  Set @monmonthlyAmt = isnull(@BudgetAmt,0)-isnull(@CampaignCost,0)    
  print '@monmonthlyAmt=='+ Convert(varchar(10),@monmonthlyAmt)      
--  print  @TotalBudgetAmt      
  Set @monTotalYearlyAmt=isnull(@TotalBudgetAmt,0)-isnull(@CampaignCost,0)    
   Print '@monTotalYearlyAmt=='+ Convert(varchar(10),@monTotalYearlyAmt)      
  Set @bitMarketCampaign=@bitMarketCampaign      
    END      
         
End
GO
