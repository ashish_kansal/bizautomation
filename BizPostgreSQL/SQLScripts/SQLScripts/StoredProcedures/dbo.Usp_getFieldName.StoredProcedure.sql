/****** Object:  StoredProcedure [dbo].[Usp_getFieldName]    Script Date: 07/26/2008 16:17:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getfieldname')
DROP PROCEDURE usp_getfieldname
GO
CREATE PROCEDURE [dbo].[Usp_getFieldName]  
@strGrpfld as varchar(200),  
@GroupId as numeric  
  
as   
  

  

if @strGrpfld not like 'CFLD%'
begin

select * from CustRptDefaultFields where numFieldGroupId = @GroupId and vcDbFieldName = @strGrpfld
end
else 
begin

select Fld_label as vcscrFieldName  from CFW_Fld_Master where fld_id in (select case when items = 'cfl' then 0 else items end from  dbo.split(@strGrpfld,'d'))
end
GO
