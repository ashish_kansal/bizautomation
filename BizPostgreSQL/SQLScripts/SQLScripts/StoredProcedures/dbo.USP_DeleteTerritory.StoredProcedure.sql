/****** Object:  StoredProcedure [dbo].[USP_DeleteTerritory]    Script Date: 07/26/2008 16:15:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_deleteterritory')
DROP PROCEDURE usp_deleteterritory
GO
CREATE PROCEDURE  [dbo].[USP_DeleteTerritory]
@numTerID as numeric(9)=0,
@numDomainID as numeric(9)=0

as


delete from TerritoryMaster where numTerID=@numTerID and numDomainID=@numDomainID
GO
