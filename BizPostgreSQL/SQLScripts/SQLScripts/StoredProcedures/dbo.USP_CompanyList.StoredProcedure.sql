/****** Object:  StoredProcedure [dbo].[USP_CompanyList]    Script Date: 07/26/2008 16:15:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--- Created By Anoop Jayaraj                                    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_companylist')
DROP PROCEDURE usp_companylist
GO
CREATE PROCEDURE [dbo].[USP_CompanyList]                                      
@numRelationType as numeric(9),                                   
@numUserCntID numeric(9),                                      
@tintUserRightType tinyint,                                          
@SortChar char(1)='0',                                     
@FirstName varChar(100)= '',                                      
@LastName varChar(100)= '',                                      
@CustName varChar(100)= '',                                    
@CurrentPage int,                                    
@PageSize int,                                    
@TotRecs int output,                                    
@columnName as Varchar(50),                                    
@columnSortOrder as Varchar(10),                            
@tintSortOrder numeric=0,                             
@numProfile as numeric(9)=0,                    
@numDomainID as numeric(9)=0,            
@tintCRMType as tinyint,            
@bitPartner as bit                                   
as                                    
                                    
--Create a Temporary table to hold data                                    
Create table #tempTable ( ID INT IDENTITY PRIMARY KEY,                                    
 numDivisionID varchar(15)                                  
 )                                    
                                    
                                    
declare @strSql as varchar(5000)                                    
set @strSql='SELECT                                        
     DM.numDivisionID                          
    FROM  CompanyInfo CMP                                    
    join DivisionMaster DM on DM.numCompanyID=CMP.numCompanyID'                            
                            
if @tintSortOrder= 1 set @strSql=@strSql+' join Favorites F on F.numContactid=DM.numDivisionID '                            
if @bitPartner=1 set @strSql=@strSql + ' left join CompanyAssociations CA on CA.numAssociateFromDivisionID=DM.numDivisionID and bitShareportal=1 and bitDeleted=0           
and CA.numDivisionID=(select numDivisionID from AdditionalContactsInformation where numContactID='+convert(varchar(15),@numUserCntID)+ ')'          
set @strSql=@strSql+ ' join AdditionalContactsInformation ADC on ADC.numDivisionID=DM.numDivisionID                                    
    left join ListDetails LD on LD.numListItemID=CMP.numCompanyRating                                     
    left join ListDetails LD1 on LD1.numListItemID= DM.numFollowUpStatus                                     
  WHERE ISNULL(ADC.bitPrimaryContact,0)=1                                     
    AND (DM.bitPublicFlag=0 OR DM.numRecOwner='+convert(varchar(15),@numUserCntID)+')                                          
    AND DM.numDomainID = '+convert(varchar(15),@numDomainID)+ ''                                    
   if @FirstName<>'' set    @strSql=@strSql+' and ADC.vcFirstName  like '''+@FirstName+'%'''                                     
    if @LastName<>'' set    @strSql=@strSql+' and ADC.vcLastName like '''+@LastName+'%'''                                    
    if @CustName<>'' set    @strSql=@strSql+' and CMP.vcCompanyName like '''+@CustName+'%'''               
if @numRelationType<>'0' set    @strSql=@strSql+' AND CMP.numCompanyType = '+convert(varchar(15),@numRelationType)+ ''                                    
if @SortChar<>'0' set @strSql=@strSql + ' And CMP.vcCompanyName like '''+@SortChar+'%'''                                     
if @tintUserRightType=1 set @strSql=@strSql + ' AND ((DM.numRecOwner = '+convert(varchar(15),@numUserCntID)+ ')' + case when @bitPartner=1 then ' or ( CA.bitShareportal=1 and CA.bitDeleted=0))' else ')' end                                          
else if @tintUserRightType=2 set @strSql=@strSql + ' AND (DM.numTerID in (select numTerritoryID from  UserTerritory where numUserCntID= '+convert(varchar(15),@numUserCntID)+' ) or DM.numTerID=0) '     
if @tintSortOrder=1 set @strSql=@strSql + ' and F.numUserCntID='+convert(varchar(15),@numUserCntID)+ ' and cType=''O'''                            
if @numProfile> 0 set @strSql=@strSql + ' and  vcProfile='+convert(varchar(15),@numProfile)                            
if @tintCRMType>0 set @strSql=@strSql + ' and  DM.tintCRMType='+convert(varchar(1),@tintCRMType)                              
set @strSql=@strSql + ' ORDER BY ' + @columnName +' '+ @columnSortOrder                                    
   print @strSql                                        
insert into #tempTable (                                   
   numDivisionID)                                    
exec( @strSql)                                    
  declare @firstRec as integer                                    
  declare @lastRec as integer                                    
 set @firstRec= (@CurrentPage-1) * @PageSize                                    
     set @lastRec= (@CurrentPage*@PageSize+1)                              
                                   
set @TotRecs=(select count(*) from #tempTable)                            
                          
                          
                          
SELECT  CMP.vcCompanyName + ' - <I>' + DM.vcDivisionName + '</I>' as CompanyName,                                       
     DM.numTerID,                                      
     ADC.vcFirstName  + ' ' +ADC.vcLastName as PrimaryContact,                                      
     case when ADC.numPhone<>'' then + ADC.numPhone +case when ADC.numPhoneExtension<>'' then ' - ' + ADC.numPhoneExtension else '' end  else '' end as [Phone],                                   
     ADC.vcEmail As vcEmail,                                      
     CMP.numCompanyID AS numCompanyID,                                      
     DM.numDivisionID As numDivisionID,                            
     ADC.numContactID AS numContactID,                                       
     CMP.numCompanyRating AS numCompanyRating,                                       
     LD.vcData as vcRating,                                        
     DM.numStatusID AS numStatusID,                                       
     LD1.vcData as Follow,                                      
     DM.numCreatedby AS numCreatedby, DM.numRecOwner , DM.tintCRMType,                                 
     case when DM.tintCRMType=1 then 'Prospects' when DM.tintCRMType=2 then 'Accounts' end as AccountType                                  
    FROM  CompanyInfo CMP                                    
    join DivisionMaster DM on DM.numCompanyID=CMP.numCompanyID                          
    join AdditionalContactsInformation ADC on ADC.numDivisionID=DM.numDivisionID                                    
    left join ListDetails LD on LD.numListItemID=CMP.numCompanyRating                                     
    left join ListDetails LD1 on LD1.numListItemID= DM.numFollowUpStatus                                     
    join #tempTable T on T.numDivisionID=DM.numDivisionID                                       
    WHERE ISNULL(ADC.bitPrimaryContact,0)=1 and ID > @firstRec and ID < @lastRec order by ID                                 
drop table #tempTable
GO
