/****** Object:  StoredProcedure [dbo].[USP_TimeAndExpensesDetailsForPay]    Script Date: 07/26/2008 16:21:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created By Siva            
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_timeandexpensesdetailsforpay')
DROP PROCEDURE usp_timeandexpensesdetailsforpay
GO
CREATE PROCEDURE [dbo].[USP_TimeAndExpensesDetailsForPay]              
(
	@numUserCntID NUMERIC(18,0),
	@numDomainID NUMERIC(18,0),    
	@numComPayPeriodID NUMERIC(18,0),
	@ClientTimeZoneOffset INT
)              
As              
BEGIN          
	SELECT    
		numCategoryHDRID,      
		DATEADD(MINUTE,-@ClientTimeZoneOffset,TE.dtFromDate) AS dtFromDate,                                    
		DATEADD(MINUTE,-@ClientTimeZoneOffset,TE.dtToDate) AS dtToDate,
		CASE 
			WHEN TE.numCategory=1 THEN convert(varchar,convert(float,DATEDIFF(mi, TE.dtFromDate, TE.dtToDate))/60)                                    
			WHEN TE.numCategory=2 THEN +convert(varchar,monAmount)                                  
			WHEN TE.numCategory=3 THEN (Case when bitFromFullDay=0  then  'HDL'                                   
			WHEN bitFromFullDay=1 THEN  'FDL' end)                                   
		END AS Detail,
		Case 
			WHEN TE.numCategory = 1 AND TE.numType=1 AND OM.numOppId > 0 
			THEN ((ISNULL(CAST(DATEDIFF(MINUTE,TE.dtfromdate,TE.dttodate) AS FLOAT)/CAST(60 AS FLOAT),0)) * TE.monAmount)
			WHEN  OM.numOppId > 0 THEN TE.monAmount 
			ELSE 0
		END [ClientCharge],
		CASE 
			WHEN TE.numCategory=1 THEN 'Time' 
			WHEN TE.numCategory=2 THEN (CASE WHEN TE.bitReimburse=1 THEN 'Expense(Reimb)' ELSE 'Expense' END)
			ELSE 'Leave' 
		END AS RecType,
		CASE WHEN TE.numType=1 THEN 'Billable' WHEN TE.numType=2 THEN 'Non-Billable' WHEN TE.numType=3 then 'Paid Leave' WHEN TE.numType=4 then 'Non-Paid' END as Type,
		CASE 
			WHEN tintTEType = 0 THEN 'T & E'            
			WHEN tintTEType = 1 THEN (CASE WHEN TE.numCategory = 1 THEN 'Sales Time' ELSE 'Sales Exp' END)
			WHEN tintTEType = 2 THEN (CASE WHEN TE.numCategory = 1 THEN 'Project Time' ELSE 'Project Exp' END)             
			WHEN tintTEType = 3 THEN (CASE WHEN TE.numCategory = 1 then 'Case Time' ELSE 'Case Exp' END)
		end as chrFrom,
		ISNULL(txtDesc,'') Description,
		U.vcUserName,
		(SELECT CAST(U.vcUserName AS varchar)+'('+CAST(L.dtmApprovedOn AS varchar)+')' + ', ' AS 'data()' 
		FROM Approval_transaction_log as L  
		LEFT JOIN UserMaster as U ON L.numApprovedBy=U.numUserDetailId
		WHERE numRecordId=TE.numCategoryHDRID order by int_transaction_id desc
		FOR XML PATH('')) as ApprovedBy
	FROM
		TimeAndExpenseCommission TEC
	INNER JOIN
		TimeAndExpense TE
	ON
		TEC.numTimeAndExpenseID = TE.numCategoryHDRID
	LEFT JOIN 
		UserMaster as U 
	ON 
		TE.numTCreatedBy=U.numUserDetailId  
	LEFT JOIN 
		dbo.OpportunityMaster OM 
	ON
		TE.numOppId = OM.numoppId 	
	WHERE           
		TEC.numComPayPeriodID=@numComPayPeriodID                          
		AND TE.numUserCntID=@numUserCntID       
		
	SELECT 
		vcPOppName
		,vcProjectName
		,dbo.fn_GetContactName(SPDTC.numUserCntID) vcUserName
		,vcMileStoneName
		,vcStageName
		,vcTaskName
		,dbo.FormatedDateFromDate(CAST(DATEADD(MINUTE,-@ClientTimeZoneOffset,DATEADD(ss,-1 *DATEDIFF(ss,GETUTCDATE(),GETDATE()),dtmCreatedOn)) AS DATE),@numDomainID) dtCreatedDate
		,SPDTC.numHours
	FROM 
		StagePercentageDetailsTaskCommission SPDTC
	INNER JOIN
		StagePercentageDetailsTask SPDT 
	ON
		SPDTC.numTaskID = SPDT.numTaskId
	INNER JOIN 
		StagePercentageDetails SPD 
	ON 
		SPDT.numStageDetailsId=SPD.numStageDetailsId 
	LEFT JOIN
		OpportunityMaster OM
	ON
		SPDT.numOppId = OM.numOppID
	LEFT JOIN
		ProjectsMaster PM
	ON	
		SPDT.numProjectId = PM.numProId
	WHERE 
		SPDTC.numComPayPeriodID = @numComPayPeriodID
		AND SPDTC.numUserCntID = @numUserCntID
End
GO
