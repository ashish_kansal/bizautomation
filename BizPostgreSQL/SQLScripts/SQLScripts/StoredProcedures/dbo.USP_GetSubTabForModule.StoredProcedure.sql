/****** Object:  StoredProcedure [dbo].[USP_GetSubTabForModule]    Script Date: 07/28/2009 23:22:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by Mohan Joshi  
Create proc [dbo].[USP_GetSubTabForModule]  
@numDomainID as numeric(9)=0,  
@tintTabType as tinyint, 
@numModuleId as numeric(9)
as  
 select
numTabId,
numTabname,
Remarks,
tintTabType,
vcURL,
bitFixed,
numDomainID,
dbo.fn_GetModuleName(numModuleId) as ModuleName
from TabMaster   TM
where numDomainID=@numDomainID   
 and tintTabType=@tintTabType 
 and numModuleId=@numModuleId
