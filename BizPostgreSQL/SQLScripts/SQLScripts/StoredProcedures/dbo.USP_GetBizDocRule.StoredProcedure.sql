GO
/****** Object:  StoredProcedure [dbo].[USP_GetBizDocRule]    Script Date: 03/03/2010 18:42:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getbizdocrule')
DROP PROCEDURE usp_getbizdocrule
GO
CREATE PROCEDURE [dbo].[USP_GetBizDocRule]
@numDomainID int

as

SELECT BZT.vcData as BizDocType,numRangeFrom,numRangeTo,
		cast ( numRangeFrom  as varchar(50)) + ' to '  +cast ( numRangeTo  as varchar(50)) as [Range],
		case btBizDocCreated when 1 then 'The moment BizDoc is created ' else '' end  + 
		case btBizDocPaidFull when 1 then 'When the BizDoc has been paid in full ' else '' end + 
		' AND create an action item with the following "type" value' + ACT.vcData + 
		'for each approver (requesting approval or declination).' +
		case numApproveDocStatusID when 0 then '' else 
		' And  WHEN ALL the approvers have approved the BizDoc (and none have declined) change the BiizDoc Status value to "' +
		ADS.vcData + '".' end + 
		case numDeclainDocStatusID when 0 then '' else ' If any approver decline the BizDoc change the status value to "'
		 + DDS.vcData  + '".' end		as Condition,

		dbo.fn_GetEmployeeName(numBizDocAppID,@numDomainID) AS Employee,
		btBizDocCreated,btBizDocPaidFull,  
		ACT.vcData As [Action Type],
		ADS.vcData AS [Approve Status],
		DDS.vcData as [Decline Status],
		numBizDocTypeID,
		numActionTypeID,		
		numApproveDocStatusID,
		numDeclainDocStatusID,
		dbo.fn_GetEmployeeID(numBizDocAppID,@numDomainID) as EmployeeID,
		numBizDocAppID
		
		FROM (SELECT * FROM BizDocApprovalRule where numDomainID=@numDomainID) BAR 
		INNER JOIN (SELECT * FROM View_GetMasterListItems WHERE (constFlag=1 or  numDomainID=@numDomainID)) BZT
		ON 
		BZT.numListItemID=BAR.numBizDocTypeID 
		INNER JOIN (SELECT * FROM View_GetMasterListItems WHERE (constFlag=1 or  numDomainID=@numDomainID)) ACT 
		ON
		ACT.numListItemID=BAR.numActionTypeID 
		INNER JOIN (SELECT * FROM View_GetMasterListItems WHERE (constFlag=1 or  numDomainID=@numDomainID)) ADS
		ON
		ADS.numListItemID=BAR.numApproveDocStatusID 
		INNER JOIN (SELECT * FROM View_GetMasterListItems WHERE (constFlag=1 or  numDomainID=@numDomainID)) DDS
		ON DDS.numListItemID=numDeclainDocStatusID
		


