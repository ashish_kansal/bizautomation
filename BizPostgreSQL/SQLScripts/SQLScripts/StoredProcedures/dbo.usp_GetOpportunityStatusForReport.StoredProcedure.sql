/****** Object:  StoredProcedure [dbo].[usp_GetOpportunityStatusForReport]    Script Date: 07/26/2008 16:18:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
---Created By Maha            
-- Modified By Anoop jayaraj            
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getopportunitystatusforreport')
DROP PROCEDURE usp_getopportunitystatusforreport
GO
CREATE PROCEDURE [dbo].[usp_GetOpportunityStatusForReport]                 
 @numDomainID numeric,                  
 @dtDateFrom datetime,                  
 @intSalesPurchase numeric=0,                
 @numUserCntID numeric=0,                            
 @intType numeric=0,                
 @tintRights TINYINT=1                
  --                
AS                  
BEGIN       
Declare @SQL as varchar(8000)           
 If @tintRights=1                
 --All Records Rights                  
 BEGIN                  
 IF @dtDateFrom = 'Jan  1 1753 12:00:00:000AM'              
 BEGIN                
  SELECT OM.numOppid,OM.intPEstimatedCloseDate as DueDate,                
  OM.vcPOppName as OpportunityName,OM.monPAmount as Amount,                
  isnull(dbo.GetOppLstMileStoneCompltd(numOppId),'') as LastMileStoneCompleted,                
  isnull(dbo.GetOppLstStageCompltd(numOppId),'') as LastStageCompleted                
  FROM OpportunityMaster OM                  
  WHERE  OM.numDomainID=@numDomainID             
  and OM.tintOppStatus=0               
  AND OM.tintOppType=@intSalesPurchase              
 END                
 ELSE                
 BEGIN                
 SELECT OM.numOppid,OM.intPEstimatedCloseDate as DueDate,                
  OM.vcPOppName as OpportunityName,OM.monPAmount as Amount,                
  isnull(dbo.GetOppLstMileStoneCompltd(numOppId),'') as LastMileStoneCompleted,                
  isnull(dbo.GetOppLstStageCompltd(numOppId),'') as LastStageCompleted                
  FROM OpportunityMaster OM                  
  WHERE OM.numDomainID=@numDomainID             
  and OM.tintOppStatus=0               
  AND OM.tintOppType=@intSalesPurchase                
  AND OM.intPEstimatedCloseDate<= @dtDateFrom   
 END                
                
 END                  
                  
 If @tintRights=2                
 BEGIN                  
 IF @dtDateFrom = 'Jan  1 1753 12:00:00:000AM'        
 BEGIN                
	SELECT 
		OM.numOppid,OM.intPEstimatedCloseDate as DueDate,                
		OM.vcPOppName as OpportunityName,OM.monPAmount as Amount,                
		ISNULL(dbo.GetOppLstMileStoneCompltd(numOppId),'') as LastMileStoneCompleted,                
		ISNULL(dbo.GetOppLstStageCompltd(numOppId),'') as LastStageCompleted                
	FROM 
		OpportunityMaster OM
	LEFT JOIN 
		AdditionalContactsInformation ADC                         
    ON
		OM.numContactId = ADC.numContactId             
	WHERE  
		OM.numDomainID=@numDomainID            
		AND OM.tintOppStatus=0               
		AND OM.tintOppType=@intSalesPurchase                
		AND ADC.numTeam IN (select F.numTeam from ForReportsByTeam F WHERE F.numUserCntID=@numUserCntID and F.numDomainid=@numDomainId and F.tintType=@intType)                   
 END                
 ELSE                
 BEGIN                
	SELECT 
		OM.numOppid,OM.intPEstimatedCloseDate as DueDate,                
		OM.vcPOppName as OpportunityName,OM.monPAmount as Amount,                
		isnull(dbo.GetOppLstMileStoneCompltd(numOppId),'') as LastMileStoneCompleted,                
		isnull(dbo.GetOppLstStageCompltd(numOppId),'') as LastStageCompleted                
	FROM 
		OpportunityMaster OM 
	LEFT JOIN
		AdditionalContactsInformation ADC                 
	ON
		OM.numContactId = ADC.numContactId
	WHERE  
		OM.numDomainID=@numDomainID             
		AND OM.tintOppStatus=0               
		AND OM.tintOppType=@intSalesPurchase             
		AND OM.intPEstimatedCloseDate <= @dtDateFrom                
		AND ADC.numTeam IN (SELECT F.numTeam FROM ForReportsByTeam F WHERE F.numUserCntID=@numUserCntID AND F.numDomainid=@numDomainId AND F.tintType=@intType)
 END                
                
 END                  
                  

 If @tintRights=3                
 BEGIN                  
 IF @dtDateFrom = 'Jan  1 1753 12:00:00:000AM'              
 BEGIN                
 SELECT OM.numOppid,OM.intPEstimatedCloseDate as DueDate,                
  OM.vcPOppName as OpportunityName,OM.monPAmount as Amount,                
  isnull(dbo.GetOppLstMileStoneCompltd(numOppId),'') as LastMileStoneCompleted,                
  isnull(dbo.GetOppLstStageCompltd(numOppId),'') as LastStageCompleted                
  FROM 
	OpportunityMaster OM
	INNER JOIN
		DivisionMaster DM                          
	ON
		OM.numDivisionId = DM.numDivisionID
  WHERE  OM.numDomainID=@numDomainID                
  AND OM.tintOppStatus=0               
  AND OM.tintOppType=@intSalesPurchase                     
  AND DM.numTerId  in(select F.numTerritory from ForReportsByTerritory F                   
  where F.numUserCntID=@numUserCntID and F.numDomainid=@numDomainID and F.tintType=@intType)                  
               
                
 END                
 ELSE                
 BEGIN                
 SELECT OM.numOppid,OM.intPEstimatedCloseDate as DueDate,                
  OM.vcPOppName as OpportunityName,OM.monPAmount as Amount,                
  isnull(dbo.GetOppLstMileStoneCompltd(numOppId),'') as LastMileStoneCompleted,                
  isnull(dbo.GetOppLstStageCompltd(numOppId),'') as LastStageCompleted                
  FROM OpportunityMaster OM
  INNER JOIN
		DivisionMaster DM                          
	ON
		OM.numDivisionId = DM.numDivisionID                    
  WHERE  OM.numDomainID=@numDomainID                 
  AND OM.tintOppStatus=0               
  AND OM.tintOppType=@intSalesPurchase            
  AND OM.intPEstimatedCloseDate <= @dtDateFrom                      
  AND DM.numTerId in(select F.numTerritory from ForReportsByTerritory F                   
  where F.numUserCntID=@numUserCntID and F.numDomainid=@numDomainID and F.tintType=@intType)                 
                
 END                
                
 END                  
 END
GO
