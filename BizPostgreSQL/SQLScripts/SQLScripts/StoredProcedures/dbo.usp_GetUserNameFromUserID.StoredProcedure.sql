/****** Object:  StoredProcedure [dbo].[usp_GetUserNameFromUserID]    Script Date: 07/26/2008 16:18:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getusernamefromuserid')
DROP PROCEDURE usp_getusernamefromuserid
GO
CREATE PROCEDURE [dbo].[usp_GetUserNameFromUserID]
	@numUserID numeric(9)=0   
--
AS
	SELECT vcUserName,vcUserDesc FROM UserMaster WHERE numUserID=@numUserID
GO
