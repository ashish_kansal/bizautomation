/****** Object:  StoredProcedure [dbo].[usp_GetDealConclusion]    Script Date: 07/26/2008 16:17:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--sp_helptext usp_GetDealConclusion   
  -- Created By Maha      
-- Last Modified By Anoop Jayaraj      
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getdealconclusion')
DROP PROCEDURE usp_getdealconclusion
GO
CREATE PROCEDURE [dbo].[usp_GetDealConclusion]          
        @numDomainID numeric,          
        @dtFromDate datetime,          
        @dtToDate datetime,          
        @numUserCntID numeric=0,                 
        @tintType numeric,          
        @tintRights numeric=0,          
        @intDealStatus numeric=1             
--          
AS          
BEGIN          
         IF @tintRights=0          
        -- ALL RECORDS RIGHTS          
        BEGIN          
                SELECT  LD.vcData as ConclusionBasis,COUNT(OM.numOppID) Total,          
                        (COUNT(OM.numOppID) * 100.00) /          
                                (select Count(*) from Opportunitymaster OM INNER JOIN ListDetails LD ON LD.numListItemID =  OM.lngpconclanalysis AND LD.numListID = 12 
    WHERE           
		OM.numDomainID = @numDomainID          
                                AND OM.tintOppStatus = @intDealStatus          
                                AND OM.bintCreatedDate >= @dtFromDate AND OM.bintCreatedDate <= @dtToDate      
    and OM.numModifiedBy=@numUserCntID -- Added By Anoop           
                                ) as Percentage          
                        FROM Opportunitymaster OM INNER JOIN ListDetails LD ON LD.numListItemID = OM.lngpconclanalysis AND LD.numListID = 12 
                        WHERE            
							OM.numRecOwner=@numUserCntID -- Added By Anoop        
                    
                                AND OM.numDomainID = @numDomainID          
                                AND OM.tintOppStatus = @intDealStatus          
                                AND OM.bintCreatedDate >= @dtFromDate AND OM.bintCreatedDate <= @dtToDate          
                        GROUP BY OM.lngpconclanalysis, LD.vcData          
          
        END          
          
         IF @tintRights=1          
        -- OWNER RECORDS RIGHTS          
        BEGIN          
                SELECT  LD.vcData as ConclusionBasis,COUNT(OM.numOppID) Total,          
                        (COUNT(OM.numOppID) * 100.00) /          
                                (select Count(*) from Opportunitymaster OM
								INNER JOIN ListDetails LD ON LD.numListItemID = OM.lngpconclanalysis AND LD.numListID = 12 
								INNER JOIN AdditionalContactsInformation  ADC ON ADC.numContactId= OM.numRecOwner         
    WHERE OM.numDomainID = @numDomainID          
                                AND OM.tintOppStatus = @intDealStatus          
                                AND OM.bintCreatedDate >= @dtFromDate AND OM.bintCreatedDate <= @dtToDate                 
    and ADC.numTeam in(select F.numTeam from ForReportsByTeam F       -- Added By Anoop        
    where F.numUserCntID=@numUserCntID and F.numDomainid=@numDomainId and F.tintType=@tintType)            
          
                                ) as Percentage          
                        FROM Opportunitymaster OM
						INNER JOIN ListDetails LD ON LD.numListItemID = OM.lngpconclanalysis AND LD.numListID = 12 
						INNER JOIN AdditionalContactsInformation  ADC ON ADC.numContactId= OM.numRecOwner          
                        WHERE   OM.numDomainID = @numDomainID          
                                AND OM.tintOppStatus = @intDealStatus          
                                AND OM.bintCreatedDate >= @dtFromDate AND OM.bintCreatedDate <= @dtToDate          
    and  ADC.numTeam in(select F.numTeam from ForReportsByTeam F             
    where F.numUserCntID=@numUserCntID and F.numDomainid=@numDomainId and F.tintType=@tintType)            
                        GROUP BY OM.lngpconclanalysis, LD.vcData          
        END          
          
         IF @tintRights=2          
        -- TERRITORY RECORDS RIGHTS          
        BEGIN          
   SELECT  LD.vcData as ConclusionBasis,COUNT(OM.numOppID) Total,          
                        (COUNT(OM.numOppID) * 100.00) /          
                           (select Count(*) from Opportunitymaster OM
						   INNER JOIN ListDetails LD ON LD.numListItemID = OM.lngpconclanalysis AND LD.numListID = 12  
						   INNER JOIN DivisionMaster Div ON OM.numDivisionId = Div.numDivisionID   
    WHERE OM.numDomainID = @numDomainID          
                                AND OM.tintOppStatus = @intDealStatus          
                                AND OM.bintCreatedDate >= @dtFromDate AND OM.bintCreatedDate <= @dtToDate          
    and Div.numTerId  in(select F.numTerritory from ForReportsByTerritory F             
    where F.numUserCntID=@numUserCntID and F.numDomainid=@numDomainID and F.tintType=@tintType)            
                                ) as Percentage          
                        FROM Opportunitymaster OM INNER JOIN ListDetails LD ON LD.numListItemID = OM.lngpconclanalysis AND LD.numListID = 12  
						   INNER JOIN DivisionMaster Div ON OM.numDivisionId = Div.numDivisionID         
                        WHERE   OM.numDomainID = @numDomainID          
                                AND OM.tintOppStatus = @intDealStatus          
                                AND OM.bintCreatedDate >= @dtFromDate AND OM.bintCreatedDate <= @dtToDate          
    and Div.numTerId  in(select F.numTerritory from ForReportsByTerritory F     -- Added By Anoop         
    where F.numUserCntID=@numUserCntID and F.numDomainid=@numDomainID and F.tintType=@tintType)            
                        GROUP BY OM.lngpconclanalysis, LD.vcData          
        END          
          
END
GO
