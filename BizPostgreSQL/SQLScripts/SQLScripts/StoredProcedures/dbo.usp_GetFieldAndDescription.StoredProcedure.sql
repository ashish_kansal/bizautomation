/****** Object:  StoredProcedure [dbo].[usp_GetFieldAndDescription]    Script Date: 07/26/2008 16:17:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getfieldanddescription')
DROP PROCEDURE usp_getfieldanddescription
GO
CREATE PROCEDURE [dbo].[usp_GetFieldAndDescription]
	@vcTableName varchar(100)='',
	@vcOrderBy varchar(10)=''   
--
AS
	/*select  syscolumns.NAme ,sysproperties.Value,systypes.name as DataType   from syscolumns , sysobjects  ,sysproperties,systypes where
 syscolumns.id  = sysobjects.id 
and sysproperties.id =  sysobjects.id 
and sysproperties.SmallId = syscolumns.Colid
and systypes.xtype=syscolumns.xtype
and sysobjects.Name  =  @vcTableName*/

-- the below query will return the Views Field Name and Data type
If @vcOrderBy='yes' 
begin
select  syscolumns.NAme,systypes.name as DataType   from syscolumns INNER JOIN sysobjects ON syscolumns.id  = sysobjects.id INNER JOIN systypes ON systypes.xtype=syscolumns.xtype where
sysobjects.Name  =@vcTableName and systypes.xtype NOT IN (35,99)
--Order By DataType DESC
end
else
begin
select  syscolumns.NAme,systypes.name as DataType from syscolumns INNER JOIN sysobjects ON syscolumns.id  = sysobjects.id INNER JOIN systypes ON systypes.xtype=syscolumns.xtype where
sysobjects.Name  =@vcTableName
Order By syscolumns.NAme DESC

end
GO
