/****** Object:  StoredProcedure [dbo].[USP_AddImportantItems]    Script Date: 07/26/2008 16:14:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_addimportantitems')
DROP PROCEDURE usp_addimportantitems
GO
CREATE PROCEDURE [dbo].[USP_AddImportantItems]                    
@numEmailHstrID as numeric(9)                 
as                
 DECLARE @NewEmailHstrID integer;                    
insert into    emailHistory (vcSubject,vcBody,bintCreatedOn,numNoofTimes,vcItemId,vcChangeKey,bitIsRead,vcSize,bitHasAttachments,
	dtReceivedOn,tintType,chrSource,vcCategory,vcBodyText,numDomainID,numUserCntId,numUid)            
            
select vcSubject,vcBody,bintCreatedOn,numNoofTimes,vcItemId,vcChangeKey,bitIsRead,vcSize,bitHasAttachments,dtReceivedOn,'4',
	chrSource,vcCategory,vcBodyText,numDomainID,numUserCntId,numUid           
from emailHistory where numEmailHstrID=@numEmailHstrID            
                
  SELECT            
   @NewEmailHstrID = SCOPE_IDENTITY();            
            
insert into    EmailHStrToBCCAndCC (numEmailHstrID,vcName,tintType,numDivisionId,numEmailId)            
select @NewEmailHstrID,vcName,tintType,numDivisionId,numEmailId from  EmailHStrToBCCAndCC where numEmailHstrID=@numEmailHstrID            
                  
insert into EmailHstrAttchDtls ( numEmailHstrID,vcFileName,vcAttachmentItemId,vcAttachmentType)            
select  @NewEmailHstrID,vcFileName,vcAttachmentItemId,vcAttachmentType  from EmailHstrAttchDtls where numEmailHstrID=@numEmailHstrID
GO
