/****** Object:  StoredProcedure [dbo].[USP_GetFiscalYearStartDate]    Script Date: 07/26/2008 16:17:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created By Siva
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getfiscalyearstartdate')
DROP PROCEDURE usp_getfiscalyearstartdate
GO
CREATE PROCEDURE [dbo].[USP_GetFiscalYearStartDate]
(@numYear as numeric(9)=0,
@numDomainId as numeric(9)=0)
As
Begin
  Declare @FiscalStartDate as datetime
  declare @FiscalDate as datetime
  Declare @month as int
  Declare @currentMonth as int
  Set @FiscalDate= (Select dbo.GetFiscalStartDate(@numYear,@numDomainId))
  Print  @FiscalDate
  print 'getutcdate()==='+convert(varchar(100),getutcdate())
  Set @month= month(@FiscalDate)
  Set @currentMonth=month(getutcdate())
  print @month
  Print @currentMonth
  if @month>@currentMonth
    Set @FiscalStartDate=dateadd(year,-1,@FiscalDate)
  Else
	Set @FiscalStartDate=@FiscalDate
 
Select @FiscalStartDate
  
End
GO
