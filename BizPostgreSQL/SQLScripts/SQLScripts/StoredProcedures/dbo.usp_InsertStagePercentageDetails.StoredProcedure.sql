
GO
/****** Object:  StoredProcedure [dbo].[usp_InsertStagePercentageDetails]    Script Date: 09/17/2010 17:41:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_insertstagepercentagedetails')
DROP PROCEDURE usp_insertstagepercentagedetails
GO
CREATE PROCEDURE [dbo].[usp_InsertStagePercentageDetails]
    @numStagePercentageId NUMERIC,
    @tintConfiguration TINYINT,
    @vcStageName VARCHAR(1000),
    @slpid NUMERIC,
    @numDomainId NUMERIC,
    @numUserCntID NUMERIC,
    @numAssignTo NUMERIC = 0,
    @vcMileStoneName VARCHAR(1000),
    @tintPercentage AS TINYINT,
    @vcDescription VARCHAR(2000),
	@dtStartDate as DATETIME,
	@dtEndDate as DATETIME, 
    @numParentStageID NUMERIC,
	@numProjectID NUMERIC,
	@numOppID NUMERIC
AS 
DECLARE @numStageDetailId NUMERIC               

DECLARE @sortOrder AS NUMERIC(18,0)=0
SET @sortOrder=(SELECT ISNULL(MAX(numStageOrder),0) FROM StagePercentageDetails WHERE numStagePercentageId=@numStagePercentageId)
SET @sortOrder=@sortOrder+1;
INSERT  INTO [StagePercentageDetails] ( [numStagePercentageId],
                                        [tintConfiguration],
                                        [vcStageName],
                                        [numDomainId],
                                        [numCreatedBy],
                                        [bintCreatedDate],
                                        [numModifiedBy],
                                        [bintModifiedDate],
                                        [slp_id],
                                        [numAssignTo],
                                        [vcMileStoneName],
                                        [tintPercentage],
                                        [vcDescription],dtStartDate,dtEndDate,numParentStageID,numProjectID,numOppID,bitclose,numStageOrder
 )
VALUES  (
          @numStagePercentageId,
          @tintConfiguration,
          @vcStageName,
          @numDomainId,
          @numUserCntID,
          GETUTCDATE(),
          @numUserCntID,
          GETUTCDATE(),
          @slpid,
          @numAssignTo,
          @vcMileStoneName,
          @tintPercentage,
          @vcDescription,@dtStartDate,@dtEndDate,@numParentStageID,@numProjectID,@numOppID,0,@sortOrder
  )

  DECLARE @numTempStagePercentageId AS NUMERIC(18,0) 
  SET @numTempStagePercentageId = SCOPE_IDENTITY()

if((select bitClose from StagePercentageDetails where numStageDetailsId=@numParentStageID)=1)
	update StagePercentageDetails set bitclose=1,tinProgressPercentage=100 where @numParentStageID=numParentStageID and numStageDetailsId=@numTempStagePercentageId

	EXEC usp_ManageStageAccessDetail @numProjectID,@numOppID,@numTempStagePercentageId,@numUserCntID,0

	IF ISNULL((SELECT COUNT(*) FROM StagePercentageDetailsTask WHERE numStageDetailsId=@numTempStagePercentageId),0) = 0
	BEGIN
		EXEC USP_ManageStageTask
				@numDomainID,    
				@numTempStagePercentageId,           
				'Place Holder',           
				0,           
				0,           
				@numUserCntID,           
				@numUserCntID,
				@numOppId,   
				@numProjectId,   
				0,
				0,
				0,
				0,
				0,
				0,
				0
	END

--Update total progress
IF @numProjectID>0
	EXEC dbo.USP_GetProjectTotalProgress
	@numDomainID = @numDomainID, --  numeric(9, 0)
	@numProId = @numProjectID, --  numeric(9, 0)
	@tintMode = 1 --  tinyint

IF @numOppID>0
	EXEC dbo.USP_GetProjectTotalProgress
	@numDomainID = @numDomainID, --  numeric(9, 0)
	@numProId = @numOppID, --  numeric(9, 0)
	@tintMode = 0 --  tinyint

  
SELECT  @numTempStagePercentageId
