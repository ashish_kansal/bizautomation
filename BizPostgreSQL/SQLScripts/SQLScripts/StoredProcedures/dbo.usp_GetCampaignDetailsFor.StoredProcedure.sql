/****** Object:  StoredProcedure [dbo].[usp_GetCampaignDetailsFor]    Script Date: 07/26/2008 16:16:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getcampaigndetailsfor')
DROP PROCEDURE usp_getcampaigndetailsfor
GO
CREATE PROCEDURE [dbo].[usp_GetCampaignDetailsFor] 
	@numCreatedBy numeric(9)=0,
	@numDomainID numeric(9)=0,
	@numCampaignId numeric(9)=0   
--
AS
BEGIN
	SELECT 
		vcCampaignNumber,
		textSubject,
		intNoOfContacts,
		textMsgDocument,
		vcEmailId
		
	FROM 	
		Campaign
	WHERE numCreatedBy=@numCreatedBy
	and numDomainID=@numDomainID
	and numCampaignID=@numCampaignID
		 
	
END
GO
