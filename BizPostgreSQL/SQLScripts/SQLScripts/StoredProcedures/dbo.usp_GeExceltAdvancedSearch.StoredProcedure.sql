/****** Object:  StoredProcedure [dbo].[usp_GeExceltAdvancedSearch]    Script Date: 07/26/2008 16:16:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_geexceltadvancedsearch')
DROP PROCEDURE usp_geexceltadvancedsearch
GO
CREATE PROCEDURE [dbo].[usp_GeExceltAdvancedSearch]  
 @numDomainID numeric,  
 @bitPublicPrivate tinyint=2,  
 @vcFirstName varchar(50)='',  
 @vcLastName varchar(50)='',  
 @vcCompanyName varchar(50)='',  
 @vcDivisionName varchar(50)='',  
 @vcCountry varchar(50)='',  
 @numContactType numeric=0,  
 @numCredit numeric=0,  
 @numCompanyType numeric=0,  
 @numRating numeric=0,  
 @numIndustry numeric=0,  
 @numCloseProb numeric=0,  
 @monOpptAmt DECIMAL(20,5)=0,  
 @tintStage tinyint=0,  
 @numGroup numeric=0,  
 @vcProfile varchar(50)='',  
 @numTeriD numeric=0,  
 @vcAOIList varchar(250)='',  
 @SortChar char(1) ='',  
 @numUserID numeric=0,  
 @numUserTerID numeric=0,  
 @tintRights tinyint=0,  
 @numCustomerID numeric=0,  
 @vcState varchar(50)='',  
 @vcCity varchar(50)=''   --
  
AS  
BEGIN  
 IF @bitPublicPrivate=1  --ONLY PRIVATE RECORDS  
 BEGIN  
  SELECT DISTINCT(VCD.numContactID), VCD.numCompanyID, VCD.numDivisionID,   
   --VCD.vcFirstName, VCD.vcLastName, VCD.vcCompanyName, VCD.vcDivisionName,   
   '<A href="prospects-display.aspx?frm=search&CmpID=' + cast(VCD.numCompanyID as varchar) + '&DivID=' + cast(VCD.numDivisionID as varchar) + '&CRMType=' + cast(VCD.tintCRMType AS varchar) + '&CntID=' + cast(VCD.numContactID AS varchar) + '">' + VCD.vcCompanyName + ' - (<I>' +  VCD.vcDivisionName + '</I>)</A>' AS vcCompName,  
   '<A href="contactdetails.aspx?frm=search&CntID=' + cast(VCD.numContactID AS varchar) + '">' + VCD.vcFirstname + ' ' + VCD.vcLastName + '</A>' AS vcContactName,   
   '<a Onclick="window.open(''callemail.aspx?LsEmail=' + VCD.vcEmail + ''',''NewAction'',''height=500,width=700,scrollbars=Auto'');return false;" href="callemail.aspx?Lsemail=nileshl@kaba.com&fromwhere=searchdisplay">' +  VCD.vcEmail + '</A>' AS vcEMail, 
 
   VCD.vcCompanyName as vcCompanyName,  
   VCD.vcDivisionName as vcDivName,  
   VCD.vcEmail  AS vcMail,   
   VCD.vcBillStreet + ',' + ' ' + VCD.vcBillCity + ',' + ' ' + VCD.vcBilState + ',' + ' ' + VCD.vcBillPostCode + ',' + ' ' + VCD.vcBillCountry as BillingAddress,  
   VCD.vcShipStreet + ',' + ' ' + VCD.vcShipCity + ',' + ' ' + VCD.vcShipState + ',' + ' ' + VCD.vcShipPostCode + ',' + ' ' + VCD.vcShipCountry as ShippingAddress,       
   VCD.vcFirstname + ' ' + VCD.vcLastName  AS vcContactName,  
  
   CASE   
    WHEN VCD.numPhone<>'' THEN VCD.numPhone +   
     CASE  
      WHEN VCD.numPhoneExtension<>'' THEN ' (' +  VCD.numPhoneExtension + ')'   
      ELSE ''  
     END  
    ELSE ''  
   END as vcPhone,   
   VCD.tintCRMType, --, OM.numPClosingPercent, OM.monPAmount, OM.tintPStage  
   VCD.numContCreatedBy, VCD.numTerID  
  FROM vwContactDetails VCD, OpportunityMaster OM  
  WHERE VCD.vcCompanyName LIKE  
    CASE   
     WHEN @vcCompanyName<>'' THEN @vcCompanyName + '%'  
     ELSE '%'  
    END   
   AND VCD.vcDivisionName LIKE  
    CASE   
     WHEN @vcDivisionName<>'' THEN @vcDivisionName + '%'  
     ELSE '%'  
    END  
   AND VCD.vcFirstName LIKE   
    CASE   
     WHEN @vcFirstName<>'' THEN @vcFirstName + '%'  
     ELSE '%'  
    END  
   AND VCD.vcLastName LIKE   
    CASE   
     WHEN @vcLastName<>'' THEN @vcLastName + '%'  
     ELSE '%'  
    END  
   AND VCD.vcProfile LIKE   
    CASE   
     WHEN @vcProfile<>'' THEN @vcProfile + '%'  
     ELSE '%'  
    END  
   AND VCD.vcBillCountry LIKE   
    CASE   
     WHEN @vcCountry<>'' THEN @vcCountry + '%'  
     ELSE '%'  
    END  
   AND (OM.numPClosingPercent =   
    CASE   
     WHEN @numCloseProb>0 THEN @numCloseProb  
    END  
    OR  
    OM.numPClosingPercent >=   
     CASE   
      WHEN @numCloseProb=0 THEN 0  
     END  
    )  
   AND (OM.monPAmount =   
    CASE   
     WHEN @monOpptAmt>0 THEN @monOpptAmt  
    END  
    OR  
    OM.monPAmount >=   
     CASE   
      WHEN @monOpptAmt=0 THEN 0  
     END  
    )  
   AND (OM.tintPStage =   
    CASE   
     WHEN @tintStage>0 THEN @tintStage  

    END  

    OR  
    OM.tintPStage >=   
     CASE   
      WHEN @tintStage=0 THEN 0  
     END  
    )  
   AND (VCD.numGrpID =   
    CASE   
     WHEN @numGroup>0 THEN @numGroup  
    END  
    OR  
    VCD.numGrpID >=   
     CASE   
      WHEN @numGroup=0 THEN 0  
     END  
    )  
   AND (VCD.numCompanyRating =   
    CASE   
     WHEN @numRating>0 THEN @numRating  
    END  
    OR  
    VCD.numCompanyRating >=   
     CASE   
      WHEN @numRating=0 THEN 0  
     END  
    )  
   AND (VCD.numCompanyType =   
    CASE   
     WHEN @numCompanyType>0 THEN @numCompanyType  
    END  
    OR  
    VCD.numCompanyType >=   
     CASE   
      WHEN @numCompanyType=0 THEN 0  
     END  
    )  
   AND (VCD.numCompanyIndustry =   
    CASE   
     WHEN @numIndustry>0 THEN @numIndustry  
    END  
    OR  
    VCD.numCompanyIndustry >=   
     CASE   
      WHEN @numIndustry=0 THEN 0  
     END  
    )  
   AND (VCD.numCompanyCredit =   
    CASE   
     WHEN @numCredit>0 THEN @numCredit  
    END  
    OR  
    VCD.numCompanyCredit >=   
     CASE   
      WHEN @numCredit=0 THEN 0  
     END  
    )  
   AND VCD.vcFirstName LIKE   
    CASE  
     WHEN @SortChar<>'' THEN @SortChar + '%'  
     ELSE '%'  
    END  
   AND (VCD.numContactType =   
    CASE   
     WHEN @numContactType>0 THEN @numContactType  
    END  
    OR  
    VCD.numContactType >=   
     CASE   
      WHEN @numContactType=0 THEN 0  
     END  
    )  
   AND VCD.bitPublicFlag = 1  
   AND (VCD.numTerID =   
    CASE   
     WHEN @numTerID>0 THEN @numTerID  
    END  
    OR  
    VCD.numTerID >=   
     CASE   
      WHEN @numTerID=0 THEN 0  
     END  
    )  
   AND (VCD.numTerID=CASE  
     WHEN @tintRights=0 THEN 0  
     WHEN @tintRights=2 THEN @numUserTerID  
          END  
        OR VCD.numTerID>=CASE  
     WHEN @tintRights=3 THEN 0  
     WHEN @tintRights=1 THEN 0  
          END  
    )  
   --Sridhar -- Start  
   AND (VCD.numDivisionID =   
    CASE   
     WHEN @numCustomerID>0 THEN @numCustomerID  
    END  
    OR  
    VCD.numDivisionID >=   
     CASE   
      WHEN @numCustomerID=0 THEN 0  
     END  
    )  
   AND (VCD.vcBillCity LIKE CASE  
     WHEN @vcCity <> '' THEN @vcCity + '%'  
     ELSE '%'  
          END  
        OR VCD.vcShipCity LIKE CASE  
     WHEN @vcCity <> '' THEN @vcCity + '%'  
     ELSE '%'  
          END  
       )  
  
   AND (VCD.vcBilState LIKE CASE  
     WHEN @vcState <> '' THEN @vcState + '%'  
     ELSE '%'  
          END  
        OR VCD.vcShipState LIKE CASE  
     WHEN @vcState <> '' THEN @vcState + '%'  
     ELSE '%'  
          END  
       )  
   --Sridhar -- End  
   AND VCD.numContCreatedBy =@numUserID  
   --AND dbo.fn_IsAOIExists(VCD.vcAOIList, @vcAOIList)=1  
   AND VCD.numDivDomainID=@numDomainID  
   --AND OM.numContactID=VCD.numContactID --By Madhan  
   AND (OM.numContactID =   
    CASE   
     WHEN @monOpptAmt>0 THEN VCD.numContactID  
     WHEN @tintStage>0 THEN VCD.numContactID  
    END  
    OR  
    OM.numContactID >=   
     CASE   
      WHEN @tintStage=0 AND @monOpptAmt=0 THEN 0  
     END  
    )  
   AND (VCD.tintCRMType=1 OR VCD.tintCRMType=2)  
  --ORDER BY VCD.vcCompanyName, VCD.vcDivisionName, VCD.vcFirstName, VCD.vcLastName  
  ORDER BY vcCompName, vcContactName  
 END  
 ELSE  --Public & Private records.  
 BEGIN  
  SELECT DISTINCT(VCD.numContactID), VCD.numCompanyID, VCD.numDivisionID,   
   --VCD.vcFirstName, VCD.vcLastName, VCD.vcCompanyName, VCD.vcDivisionName,   
   '<A href="prospects-display.aspx?frm=search&CmpID=' + cast(VCD.numCompanyID as varchar) + '&DivID=' + cast(VCD.numDivisionID as varchar) + '&CRMType=' + cast(VCD.tintCRMType AS varchar) + '&CntID=' + cast(VCD.numContactID AS varchar) + '">' + VCD.vcCompanyName + ' - (<I>' +  VCD.vcDivisionName + '</I>)</A>' AS vcCompName,  
   '<A href="contactdetails.aspx?frm=search&CntID=' + cast(VCD.numContactID AS varchar) + '">' + VCD.vcFirstname + ' ' + VCD.vcLastName + '</A>' AS vcContactName,   
   '<a Onclick="window.open(''callemail.aspx?LsEmail=' + VCD.vcEmail + ''',''NewAction'',''height=500,width=700,scrollbars=Auto'');return false;" href="callemail.aspx?Lsemail=nileshl@kaba.com&fromwhere=searchdisplay">' +  VCD.vcEmail + '</A>' AS vcEMail, 
 
   VCD.vcCompanyName as vcCompanyName,  
   VCD.vcDivisionName as vcDivName,  
   VCD.vcEmail  AS vcMail,   
   VCD.vcBillStreet + ',' + ' ' + VCD.vcBillCity + ',' + ' ' + VCD.vcBilState + ',' + ' ' + VCD.vcBillPostCode + ',' + ' ' + VCD.vcBillCountry as BillingAddress,  
   VCD.vcShipStreet + ',' + ' ' + VCD.vcShipCity + ',' + ' ' + VCD.vcShipState + ',' + ' ' + VCD.vcShipPostCode + ',' + ' ' + VCD.vcShipCountry as ShippingAddress,       
   VCD.vcFirstname + ' ' + VCD.vcLastName  AS vcContactName,  
  
  
   CASE   
    WHEN VCD.numPhone<>'' THEN VCD.numPhone +   
     CASE  
      WHEN VCD.numPhoneExtension<>'' THEN ' (' +  VCD.numPhoneExtension + ')'   
      ELSE ''  
     END  
    ELSE ''  
   END as vcPhone,   
   VCD.tintCRMType, --, OM.numPClosingPercent, OM.monPAmount, OM.tintPStage  
   VCD.numContCreatedBy, VCD.numTerID  
  FROM vwContactDetails VCD, OpportunityMaster OM  
  WHERE VCD.vcCompanyName LIKE  
    CASE   
     WHEN @vcCompanyName<>'' THEN @vcCompanyName + '%'  
     ELSE '%'  
    END   
   AND VCD.vcDivisionName LIKE  
    CASE   
     WHEN @vcDivisionName<>'' THEN @vcDivisionName + '%'  
     ELSE '%'  
    END  
   AND VCD.vcFirstName LIKE   
    CASE   
     WHEN @vcFirstName<>'' THEN @vcFirstName + '%'  
     ELSE '%'  
    END  
   AND VCD.vcLastName LIKE   
    CASE   
     WHEN @vcLastName<>'' THEN @vcLastName + '%'  
     ELSE '%'  
    END  
   AND VCD.vcProfile LIKE   
    CASE   
     WHEN @vcProfile<>'' THEN @vcProfile + '%'  
     ELSE '%'  
    END  
   AND VCD.vcBillCountry LIKE   
    CASE   
     WHEN @vcCountry<>'' THEN @vcCountry + '%'  
     ELSE '%'  
    END  
   AND (OM.numPClosingPercent =   
    CASE   
     WHEN @numCloseProb>0 THEN @numCloseProb  
    END  
    OR  
    OM.numPClosingPercent >=   
     CASE   
      WHEN @numCloseProb=0 THEN 0  
     END  
    )  
   AND (OM.monPAmount =   
    CASE   
     WHEN @monOpptAmt>0 THEN @monOpptAmt  
    END  
    OR  
    OM.monPAmount >=   
     CASE   
      WHEN @monOpptAmt=0 THEN 0  
     END  
    )  
   AND (OM.tintPStage =   
    CASE   
     WHEN @tintStage>0 THEN @tintStage  
    END  
    OR  
    OM.tintPStage >=   
     CASE   
      WHEN @tintStage=0 THEN 0  
     END  
    )  
   AND (VCD.numGrpID =   
    CASE   
     WHEN @numGroup>0 THEN @numGroup  
    END  
    OR  
    VCD.numGrpID >=   
     CASE   
      WHEN @numGroup=0 THEN 0  
     END  
    )  
   AND (VCD.numCompanyRating =   
    CASE   
     WHEN @numRating>0 THEN @numRating  
    END  
    OR  
    VCD.numCompanyRating >=   
     CASE   
      WHEN @numRating=0 THEN 0  
     END  
    )  
   AND (VCD.numCompanyType =   
    CASE   
     WHEN @numCompanyType>0 THEN @numCompanyType  
    END  
    OR  
    VCD.numCompanyType >=   
     CASE   
      WHEN @numCompanyType=0 THEN 0  
     END  
    )  
   AND (VCD.numCompanyIndustry =   
    CASE   
     WHEN @numIndustry>0 THEN @numIndustry  
    END  
    OR  
    VCD.numCompanyIndustry >=   
     CASE   
      WHEN @numIndustry=0 THEN 0  
     END  
    )  
   AND (VCD.numCompanyCredit =   
    CASE   
     WHEN @numCredit>0 THEN @numCredit  
    END  
    OR  
    VCD.numCompanyCredit >=   
     CASE   
      WHEN @numCredit=0 THEN 0  
     END  
    )  
   AND VCD.vcFirstName LIKE   
    CASE  
     WHEN @SortChar<>'' THEN @SortChar + '%'  
     ELSE '%'  
    END  
   AND (VCD.numContactType =   
    CASE   
     WHEN @numContactType>0 THEN @numContactType  
    END  
    OR  

    VCD.numContactType >=   
     CASE   
      WHEN @numContactType=0 THEN 0  
     END  
    )  
   AND (VCD.bitPublicFlag = 0  
    OR  
    (VCD.bitPublicFlag = 1  
     AND  
     VCD.numContCreatedBy =@numUserID  
    )  
       )  
   AND (VCD.numTerID =   
    CASE   
     WHEN @numTerID>0 THEN @numTerID  
    END  
    OR  
    VCD.numTerID >=   
     CASE   
      WHEN @numTerID=0 THEN 0  
     END  
    )  
   AND (VCD.numTerID=CASE  
     WHEN @tintRights=0 THEN 0  
     WHEN @tintRights=2 THEN @numUserTerID  
          END  
        OR VCD.numTerID>=CASE  
     WHEN @tintRights=3 THEN 0  
     WHEN @tintRights=1 THEN 0  
          END  
    )  
  
   AND (VCD.numContCreatedBy =  
    CASE  
     WHEN @tintRights=1 THEN @numUserID  
    END  
    OR  
    VCD.numContCreatedBy >  
     CASE  
      WHEN @tintRights<>1 THEN 0  
     END  
    )  
  
   --Sridhar -- Start  
   AND (VCD.numDivisionID =   
    CASE   
     WHEN @numCustomerID>0 THEN @numCustomerID  
    END  
    OR  
    VCD.numDivisionID >=   
     CASE   
      WHEN @numCustomerID=0 THEN 0  
     END  
    )  
   AND (VCD.vcBillCity LIKE CASE  
     WHEN @vcCity<>'' THEN @vcCity + '%'  
     ELSE '%'  
          END  
        OR VCD.vcShipCity LIKE CASE  
     WHEN @vcCity <>'' THEN @vcCity + '%'  
     ELSE '%'  
          END  
       )  
  
   AND (VCD.vcBilState LIKE CASE  
     WHEN @vcState<>'' THEN @vcState + '%'  
     ELSE '%'  
          END  
        OR VCD.vcShipState LIKE CASE  
     WHEN @vcState <>'' THEN @vcState + '%'  
     ELSE '%'  
          END  
       )  
   --Sridhar -- End  
  
 --  AND dbo.fn_IsAOIExists(VCD.vcAOIList, @vcAOIList)=1  
   AND VCD.numDivDomainID=@numDomainID  
   --AND OM.numContactID=VCD.numContactID --By Madhan  
   AND (OM.numContactID =   
    CASE   
     WHEN @monOpptAmt>0 THEN VCD.numContactID  
     WHEN @tintStage>0 THEN VCD.numContactID  
    END  
    OR  
    OM.numContactID >=   
     CASE   
      WHEN @tintStage=0 AND @monOpptAmt=0 THEN 0  
     END  
    )  
   AND (VCD.tintCRMType=1 OR VCD.tintCRMType=2)  
  --ORDER BY VCD.vcCompanyName, VCD.vcDivisionName, VCD.vcFirstName, VCD.vcLastName  
  ORDER BY vcCompName, vcContactName  
 END  
  
END
GO
