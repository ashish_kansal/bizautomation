/****** Object:  StoredProcedure [dbo].[USP_GetMaxRows]    Script Date: 07/26/2008 16:17:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--- Created By Anoop Jayaraj      
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getmaxrows')
DROP PROCEDURE usp_getmaxrows
GO
CREATE PROCEDURE [dbo].[USP_GetMaxRows]      
    
@numUserCntID as numeric(9)=0,      
@numDomainId as numeric(9)=0 ,    
@Ctype as varchar     
as      
if((select count(*) from pagelayoutdtl DTL  where DTL.numUserCntID=@numUserCntID and DTL.numDomainId=@numDomainId and DTL.Ctype=@Ctype ) <> 0)    
begin    
select max(DTL.intcoulmn) from pagelayoutdtl DTL        
where DTL.numUserCntID=@numUserCntID and DTL.numDomainId=@numDomainId and DTL.Ctype=@Ctype    
    
end      
else    
begin    
select max(intcolumn) from pagelayout  where Ctype=@Ctype    
end
GO
