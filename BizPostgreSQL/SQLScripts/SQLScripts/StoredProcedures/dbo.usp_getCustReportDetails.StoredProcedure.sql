/****** Object:  StoredProcedure [dbo].[usp_getCustReportDetails]    Script Date: 07/26/2008 16:17:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getcustreportdetails')
DROP PROCEDURE usp_getcustreportdetails
GO
CREATE PROCEDURE [dbo].[usp_getCustReportDetails]              
@ReportId as numeric = 0,              
@numDomainId as numeric =0              
as               
select numCustomReportID,vcReportName,vcReportDescription,textQuery,numCreatedBy,numDomainID,numModifiedBy,numModuleId,            
numGroupId , bitFilterRelAnd , varstdFieldId,custFilter,FromDate,            
ToDate,bintRows  ,bitGridType,varGrpfld,varGrpOrd,varGrpflt,textQueryGrp ,FilterType,AdvFilterStr,  
OrderbyFld,Orderf,MttId,MttValue,CompFilter1,CompFilter2,CompFilterOpr,isnull(bitDrillDown,0) as bitDrillDown,isnull(bitSortRecCount,0) as bitSortRecCount    
from CustomReport where numCustomReportId = @ReportId
GO
