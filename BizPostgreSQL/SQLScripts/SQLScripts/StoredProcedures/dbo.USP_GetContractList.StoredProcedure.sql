
GO
/****** Object:  StoredProcedure [dbo].[USP_GetContractList]    Script Date: 01/09/2009 23:12:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getcontractlist')
DROP PROCEDURE usp_getcontractlist
GO
CREATE PROCEDURE [dbo].[USP_GetContractList]                                                                                         
 @numUserCntID numeric(9)=0,                                                                                          
 @numDomainID numeric(9)=0,                                                                                          
 @tintSortOrder tinyint=4,                                                                                          
 @SortChar char(1)='0',                                                                                                                                            
 @CurrentPage int,                                                                                        
@PageSize int,                                                                                        
@TotRecs int output,                                                                                        
@columnName as Varchar(50),                                                                                        
@columnSortOrder as Varchar(10)='desc'                                                                             ,        
 @ClientTimeZoneOffset int   ,      
@numDivisionID numeric(9)=0                                                                                  
as                       
Create table #tempTable ( ID INT IDENTITY PRIMARY KEY,                                                                       
      numContractID varchar(15)
	                                                               
 )                      
                                                                      
declare @strSql as varchar(8000)                                                                
 set  @strSql='select                                                   
c.numcontractId 
from ContractManagement c                                                      
join divisionmaster d on c.numDivisionId= d.numDivisionId                                    
join companyinfo com on d.numcompanyId = com.numcompanyId                                    
where          
c.numDomainId ='+convert(varchar(100),@numDomainId)+'                    
'                    
if @SortChar<>'0' set @strSql=@strSql + ' And vcContractName like '''+@SortChar+'%'''                 
if @tintsortOrder = 1 set @strSql =@strSql +'and c.numContractId in                
 (select distinct(numcontractId) from timeandexpense Te            
  where numcaseid <> 0 and numDomainId ='+convert(varchar(100),@numDomainId)+')'           
if @tintsortOrder = 2 set @strSql =@strSql +'and c.numContractId in              
(select distinct(numcontractId) from timeandexpense where numOppid<> 0 and   
 numDomainId ='+convert(varchar(100),@numDomainId)+')'           
if @tintsortOrder = 3 set @strSql =@strSql +'and c.numContractId in                
 (select distinct(numcontractId) from timeandexpense where numProid <> 0   
 and numDomainId ='+convert(varchar(100),@numDomainId)+')'
if @tintsortOrder = 4 set @strSql =@strSql +'and c.bitDays = 1 and c.bintExpDate > getdate()
and c.numDomainId ='+convert(varchar(100),@numDomainId) + ' '
  
if @numDivisionID <> 0 set @strSql =@strSql +'and c.numDivisionID='+convert(varchar(100),@numDivisionID)      
set @strSql=@strSql + 'ORDER BY ' + @columnName +' '+ @columnSortOrder                       
                    
Print @strSql                          
insert into #tempTable(numContractID)                                                    
exec(@strSql)                    
                    
 declare @firstRec as integer                                                          
  declare @lastRec as integer                                         
 set @firstRec= (@CurrentPage-1) * @PageSize                                                                      
 set @lastRec= (@CurrentPage*@PageSize+1)                                                                       
set @TotRecs=(select count(*) from #tempTable)                     
                    
select                                     
CONVERT(VARCHAR(10),c.[bintExpDate],105) bintExpDate,
c.numcontractId,                                    
c.numDivisionId,                                    
vcContractName,                        
case when bitdays =1 then                          
isnull(convert(varchar(100),                        
case when datediff(day,bintStartdate,getutcdate())>=0 then datediff(day,bintStartdate,getutcdate()) else 0 end                        
                        
),0) +' of '+ isnull(convert(varchar(100),datediff(day,bintStartDate,bintExpDate)),0)                         
else '-' end as Days,                                    
                        
case when bitincidents =1 then                          
isnull(convert(varchar(100),dbo.GetIncidents(c.numcontractId,@numDomainId ,c.numDivisionId ),0),0) +' of '+ isnull(convert(varchar(100),numincidents),0)                         
else '-' end as Incidents,                            
                          
case when bitamount =1 then                          
isnull(convert(varchar(100),dbo.GetContractRemainingAmount(c.numcontractId,c.numDivisionId,@numDomainId)),0) + ' of ' + convert(varchar(100),convert(decimal(10,2),c.numAmount+( Select isnull(sum(monAmount),0) From OpportunityBizDocsDetails Where    
  numContractId= c.numContractId )))                         
else '-' end  as Amount,                                  
                        
case when bithour =1 then                          
isnull(convert(varchar(100),dbo.GetContractRemainingHours(c.numcontractId,c.numDivisionId,@numDomainId)),0) +' of ' + convert(varchar(100),c.numhours )                        
else '-' end as Hours,                                   
                                  
dbo.FormatedDateFromDate(dateadd(minute,-@ClientTimeZoneOffset,c.bintCreatedOn),@numDomainId)

 as bintCreatedOn ,                                    
com.vcCompanyName                                     
                                   
from ContractManagement c                                                      
join divisionmaster d on c.numDivisionId= d.numDivisionId                                    
join companyinfo com on d.numcompanyId = com.numcompanyId                       
 join #tempTable T on T.numContractId=C.numContractId                                                                 
   WHERE ID > @firstRec and ID < @lastRec                                
order by ID                                                     
                                                                    
drop table #tempTable
