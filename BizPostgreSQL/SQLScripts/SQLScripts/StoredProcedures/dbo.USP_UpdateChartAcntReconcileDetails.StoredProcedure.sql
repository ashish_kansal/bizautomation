/****** Object:  StoredProcedure [dbo].[USP_UpdateChartAcntReconcileDetails]    Script Date: 07/26/2008 16:21:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created By Siva

/* We are no longer using this procedure- by chintan*/
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_updatechartacntreconciledetails')
DROP PROCEDURE usp_updatechartacntreconciledetails
GO
CREATE PROCEDURE [dbo].[USP_UpdateChartAcntReconcileDetails]
(
@numDomainID as numeric(9)=0,
@numChartAcntId as numeric(9)=0,
@dtEndingBal as datetime,
@monEndingBal as DECIMAL(20,5),
@numServiceChargeChartAcntId as numeric(9)=0,
@dtServiceChargeDate as datetime,
@monServiceChargeAmt as DECIMAL(20,5),
@numInterestEarnedChartAcntId as numeric(9)=0,
@dtInterestEarnedDate as datetime,
@monInterestEarnedAmt as DECIMAL(20,5),
@numFinanceChargeChartAcntId as numeric(9)=0,
@dtFinanceChargeDate as datetime,
@monFinanceChargeAmt as DECIMAL(20,5))
As
--Begin
--if @dtServiceChargeDate ='Jan  1 1753 12:00AM' set  @dtServiceChargeDate=null                                                                                                 
--if @dtInterestEarnedDate ='Jan  1 1753 12:00AM' set  @dtInterestEarnedDate=null     
--if @dtFinanceChargeDate ='Jan  1 1753 12:00AM' set  @dtFinanceChargeDate=null   
--if @numServiceChargeChartAcntId=0 set @numServiceChargeChartAcntId=null
--if @numInterestEarnedChartAcntId=0 set @numInterestEarnedChartAcntId=null
--if @numFinanceChargeChartAcntId=0 set @numFinanceChargeChartAcntId=null  
--Update Chart_of_Accounts  Set monEndingBal=@monEndingBal,
--							dtEndStatementDate=@dtEndingBal,
--							numServiceAcntId=@numServiceChargeChartAcntId,
--							dtServiceCharge=@dtServiceChargeDate,
--							monserviceCharges=@monServiceChargeAmt,
--						    numInterestEarnedAcntId=@numInterestEarnedChartAcntId,
--							dtInternestEarned=@dtInterestEarnedDate,
--							monInterestEarned=@monInterestEarnedAmt,
--							numFinancialAntId=@numFinanceChargeChartAcntId,
--							dtFinancialCharges=@dtFinanceChargeDate,
--							monFinancialCharges=@monFinanceChargeAmt 
--Where numAccountId = @numChartAcntId And numDomainID=@numDomainID
--								 
--End
GO
