/****** Object:  StoredProcedure [dbo].[Get_Max_Template_ID]    Script Date: 07/26/2008 16:14:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='get_max_template_id')
DROP PROCEDURE get_max_template_id
GO
CREATE PROCEDURE [dbo].[Get_Max_Template_ID]
As
Declare @maxValue Int
--Insert Into tblActionItemData Values( '' , 0 , 356 , 'Communication' , 358 , '' )
Select @maxValue = Max(RowID) From tblActionItemData 
Return @maxValue + 1
GO
