/****** Object:  StoredProcedure [dbo].[usp_getDefaultValues]    Script Date: 07/26/2008 16:17:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getdefaultvalues')
DROP PROCEDURE usp_getdefaultvalues
GO
CREATE PROCEDURE [dbo].[usp_getDefaultValues]
@intOption as integer
as

select vcSQLValue,vcOptionName from CustomReptOptValues where intOptions = @intOption
GO
