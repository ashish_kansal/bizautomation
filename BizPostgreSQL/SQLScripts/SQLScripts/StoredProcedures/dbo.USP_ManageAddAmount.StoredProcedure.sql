/****** Object:  StoredProcedure [dbo].[USP_ManageAddAmount]    Script Date: 07/26/2008 16:19:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--craeted by anoop jayaraj    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_manageaddamount')
DROP PROCEDURE usp_manageaddamount
GO
CREATE PROCEDURE [dbo].[USP_ManageAddAmount]    
@byteMode as tinyint,    
@numAmtCategory as numeric(9)=0,    
@monAmount as DECIMAL(20,5),    
@dtAmtAdded as datetime,    
@numUserCntID as numeric(9)=0,    
@numAddHDRID as numeric(9)=0    
as    
    
    
if @byteMode=0    
begin    
 insert into TimeExpAddAmount(numAmtCategory,monAmount,dtAmtAdded,numUserCntID)    
        values(@numAmtCategory,@monAmount,@dtAmtAdded,@numUserCntID)    
        select 1    
end    
if @byteMode=1    
begin    
 select * from TimeExpAddAmount    
        join ListDetails on numListItemID=numAmtCategory    
        where numUserCntID=@numUserCntID    
end    
if @byteMode=2    
begin    
 delete from TimeExpAddAmount where numAddHDRID=@numAddHDRID    
         select 1    
end
GO
