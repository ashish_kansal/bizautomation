/****** Object:  StoredProcedure [dbo].[usp_GetDuplicateCompany_FirstSave]    Script Date: 07/26/2008 16:17:22 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getduplicatecompany_firstsave')
DROP PROCEDURE usp_getduplicatecompany_firstsave
GO
CREATE PROCEDURE [dbo].[usp_GetDuplicateCompany_FirstSave]

--@numCompanyID numeric,
@vcCompanyName varchar(100)
--@numDivisionID numeric
--
AS
BEGIN

select CI.numCompanyID, CI.vcCompanyName, DM.vcDivisionName
from dbo.CompanyInfo CI INNER JOIN DivisionMaster DM ON CI.numCompanyID=DM.numCompanyID
where  
CI.vcCompanyName = @vcCompanyName
--And DM.numDivisionID <> @numDivisionID and DM.bitPublicFlag=0 and DM.tintCRMType in (1,2)
Group by CI.numCompanyID, CI.vcCompanyName, DM.vcDivisionName

END
GO
