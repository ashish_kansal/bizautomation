
/****** Object:  StoredProcedure [dbo].[USP_GetReceivePaymentDetails]    Script Date: 03/06/2009 00:33:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created By Siva
-- exec [dbo].[USP_GetReceivePaymentDetails] 72,-333
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getreceivepaymentdetails')
DROP PROCEDURE usp_getreceivepaymentdetails
GO
CREATE PROCEDURE [dbo].[USP_GetReceivePaymentDetails]
(
               @numDomainId          AS NUMERIC(9)  = 0,
               @ClientTimeZoneOffset AS INT)
AS
  BEGIN
    SELECT OppBizDocsPayDet.dtDueDate,
           OppBizDocsPayDet.numBizDocsPaymentDetailsId AS numBizDocsPaymentDetailsId,
           OppBizDocsDet.numBizDocsPaymentDetId AS numBizDocsPaymentDetId,
           Opp.numOppId AS numOppId,
           Opp.numDivisionId AS numDivisionId,
           OppBizDocsDet.numBizDocsId AS numBizDocsId,
           Opp.vcPOppName AS Name,
           OppBizDocsPayDet.monAmount AS Amount,
           OppBizDocsDet.numPaymentMethod,
		   dbo.fn_GetListItemName(OppBizDocsDet.numPaymentMethod) AS PaymentMethod,
           dbo.fn_GetContactName(OppBizDocsDet.numCreatedBy)
             + ' - '
             + dbo.FormatedDateTimeFromDate(dateadd(MINUTE,-@ClientTimeZoneOffset,OppBizDocsDet.dtCreationDate),
                                            @numDomainId) AS CreatedBy,
           isnull(OppBizDocsDet.vcMemo,'') AS Memo,
           isnull(OppBizDocsDet.vcReference,'') AS Reference,
           
           ISNULL(OppBizDocsDet.numCurrencyID,ISNULL(opp.[numCurrencyID],0)) numCurrencyID,
           [dbo].[GetExchangeRate](opp.numdomainID,ISNULL(OppBizDocsDet.fltExchangeRate,opp.fltExchangeRate)) fltExchangeRate,
           isnull(OppBizDocsPayDet.monAmount
                    * OppBizDocsDet.fltExchangeRate,0) AS ConAmt,
           ISNULL(OppBizDocsDet.[numCardTypeID],0) numCardTypeID,OppBizDocs.vcBizDocID
--           (SELECT fltTransactionCharge FROM [COACreditCardCharge] numDomainID=@numDomainId AND numCreditCardTypeId =OppBizDocsDet.[numCardTypeID]) AS 
    FROM   OpportunityBizDocsDetails OppBizDocsDet
           INNER JOIN OpportunityBizDocsPaymentDetails OppBizDocsPayDet
             ON OppBizDocsDet.numBizDocsPaymentDetId = OppBizDocsPayDet.numBizDocsPaymentDetId
           INNER JOIN OpportunityBizDocs OppBizDocs
             ON OppBizDocsDet.numBizDocsId = OppBizDocs.numOppBizDocsId
           INNER JOIN OpportunityMaster Opp
             ON OppBizDocs.numOppId = Opp.numOppId
           INNER JOIN AdditionalContactsInformation ADC
             ON Opp.numContactId = ADC.numContactId
           INNER JOIN DivisionMaster Div
             ON Opp.numDivisionId = Div.numDivisionID
                AND ADC.numDivisionId = Div.numDivisionID
           INNER JOIN CompanyInfo C
             ON Div.numCompanyID = C.numCompanyId
    WHERE  OppBizDocsDet.numDomainId = @numDomainId
           AND OppBizDocsDet.bitAuthoritativeBizDocs = 1
           AND OppBizDocsPayDet.bitIntegrated = 0
           AND Opp.tintOppType = 1
           AND OppBizDocsDet.tintPaymentType<>5 --Do not show commission entries
--           AND dbo.fn_GetListItemName(OppBizDocsDet.numPaymentMethod) <> 'Credit Card'
  --And OppBizDocsPayDet.dtDueDate<=dateadd(minute,-@ClientTimeZoneOffset,getutcdate())
  END
