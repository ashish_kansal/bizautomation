/****** Object:  StoredProcedure [dbo].[USP_ProSaveSubStage]    Script Date: 07/26/2008 16:20:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_prosavesubstage')
DROP PROCEDURE usp_prosavesubstage
GO
CREATE PROCEDURE [dbo].[USP_ProSaveSubStage]
(
	@byteMode as tinyint=null,
	@numProID as numeric(9)=null,
	@stageDetailsId as numeric(9)=null,
	@strSubStage as text='',
	@numSubStageHdrID as numeric(9),
	@numSubStageID as numeric(9)=null
)

as

begin
if @byteMode= 0 
begin
declare @hDoc  int
EXEC sp_xml_preparedocument @hDoc OUTPUT, @strSubStage
	delete from ProjectsSubStageDetails where numStageDetailsId=@stageDetailsId and numProId=@numProID
	insert into ProjectsSubStageDetails
			(
				numProId,
				numStageDetailsId,
				numSubStageID,
				numProcessListId,
				vcSubStageDetail,
				bitStageCompleted
			)
	select @numProID,@stageDetailsId,@numSubStageID,X.* from(SELECT *FROM OPENXML (@hDoc,'/NewDataSet/Table',2)
	WITH (	numSalesProsessList numeric(9),
		vcSubStageDetail varchar(1000),
		bitStageCompleted bit
		))X
	



EXEC sp_xml_removedocument @hDoc

end
if @byteMode= 1
 
begin
	select numSubStageElmtID,numProId,numStageDetailsId,
				numSubStageID ,
				numProcessListId as numSalesProsessList,
				vcSubStageDetail,
				bitStageCompleted
	from ProjectsSubStageDetails 
	where numProId=@numProId and numStageDetailsId=@stageDetailsId

end

if @byteMode= 2
 
begin
	select 	numSubStageDtlID,
		0 as numProId,
		0 as numStageDetailsId,
		0 as numSubStageID,
		numProcessListId as numSalesProsessList,
		vcSubStageElemt as vcSubStageDetail,
		0 as bitStageCompleted
	from SubStageDetails
	where  numSubStageHdrID=@numSubStageHdrID 



end


end
GO
