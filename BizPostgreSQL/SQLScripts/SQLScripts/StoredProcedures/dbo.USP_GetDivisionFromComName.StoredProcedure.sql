/****** Object:  StoredProcedure [dbo].[USP_GetDivisionFromComName]    Script Date: 07/26/2008 16:17:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj  
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getdivisionfromcomname')
DROP PROCEDURE usp_getdivisionfromcomname
GO
CREATE PROCEDURE [dbo].[USP_GetDivisionFromComName]  
@vcCompanyName as varchar(100)=''  
as  
  
declare @numDivisionID numeric  
set @numDivisionID=0  
  
select top 1 @numDivisionID=numDivisionID from CompanyInfo C  
join DivisionMaster D on  
C.numCompanyID=D.numCompanyID  
where vcWebSite like '%'+@vcCompanyName+'%' 
  
select @numDivisionID
GO
