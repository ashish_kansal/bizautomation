
/****** Object:  StoredProcedure [dbo].[USP_CheckAndDepositAmountForBizdoc]    Script Date: 02/19/2010 17:44:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_CheckAndDepositAmountForBizdoc')
DROP PROCEDURE USP_CheckAndDepositAmountForBizdoc
GO
CREATE PROCEDURE [dbo].[USP_CheckAndDepositAmountForBizdoc]
(
	@numDivisionID			NUMERIC(18,0),
	@numOppBizDocId			NUMERIC(18,0),
	@numAmount				DECIMAL(20,5),
	@numDomainID			NUMERIC(18,0)
)
AS 
BEGIN
		
	SELECT CASE WHEN ISNULL([OBD].[monDealAmount],0) = @numAmount THEN 1 -- If paid amount + new paid amount is equal to total deal amount, Fully Paid
				WHEN ISNULL([OBD].[monDealAmount],0) > SUM(ISNULL(DD.[monAmountPaid],0)) + @numAmount THEN 2 -- If paid amount + new paid amount is greater than total deal amount, Due Amounts
				WHEN ISNULL([OBD].[monDealAmount],0) < SUM(ISNULL(DD.[monAmountPaid],0)) + @numAmount THEN 3 -- If paid amount + new paid amount is less than total deal   amount, Unapplied Amounts
				ELSE 0
		   END AS [IsPaidInFull],
		   [DM].[numDivisionID],
		   [DM].[numDomainId],
		   [DD].[numOppBizDocsID],
		   ISNULL([OBD].[monDealAmount],0)  [monDealAmount],
		   SUM(ISNULL(DD.[monAmountPaid],0)) [monAmountPaid],
		   SUM(ISNULL(DD.[monAmountPaid],0)) + @numAmount [NewAmountPaid],
		   (SUM(ISNULL(DD.[monAmountPaid],0)) + @numAmount) - ISNULL([OBD].[monDealAmount],0) [OverPaidAmount]
	FROM [dbo].[DepositMaster] AS DM 
	JOIN [dbo].[DepositeDetails] AS DD ON [DM].[numDepositId] = [DD].[numDepositID]
	JOIN [dbo].[OpportunityMaster] AS OM ON OM.[numOppId] = DD.[numOppID] AND OM.[numDomainId] = DM.[numDomainId]
	JOIN [dbo].[OpportunityBizDocs] AS OBD ON [OBD].[numOppBizDocsId] = DD.[numOppBizDocsID] AND [OM].[numOppId] = [OBD].[numOppId]
	WHERE [DM].[numDomainId] = @numDomainID
	AND [DM].[numDivisionID] = @numDivisionID
	AND DD.[numOppBizDocsID] = @numOppBizDocId
	GROUP BY [OBD].[monDealAmount],
			 [DM].[numDivisionID],
			 [DM].[numDomainId],
			 [DD].[numOppBizDocsID]

END

GO
