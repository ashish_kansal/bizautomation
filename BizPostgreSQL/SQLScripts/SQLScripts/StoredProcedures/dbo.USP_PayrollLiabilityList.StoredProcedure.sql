
GO
/****** Object:  StoredProcedure [dbo].[USP_PayrollLiabilityList]    Script Date: 02/28/2009 13:55:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created By Siva
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_payrollliabilitylist')
DROP PROCEDURE usp_payrollliabilitylist
GO
CREATE PROCEDURE [dbo].[USP_PayrollLiabilityList]
(
    @numDomainId NUMERIC(18,0),
    @numDepartmentId NUMERIC(18,0),
    @numComPayPeriodID NUMERIC(18,0),
    @ClientTimeZoneOffset INT,
	@numOrgUserCntID AS NUMERIC(18,0),
	@tintUserRightType INT = 3,
	@tintUserRightForUpdate INT = 3
)
AS
BEGIN
	DECLARE @dtStartDate DATETIME
	DECLARE @dtEndDate DATETIME

	SELECT
		@dtStartDate = DATEADD (MS , 000 , CAST(dtStart AS DATETIME))
		,@dtEndDate = DATEADD (MS , -3 , CAST(dtEnd AS DATETIME))  
	FROM
		CommissionPayPeriod
	WHERE
		numComPayPeriodID=@numComPayPeriodID
	
	-- TEMPORARY TABLE TO HOLD DATA
	CREATE TABLE #tempHrs 
	(
		ID INT IDENTITY(1,1),
		numUserId NUMERIC,
		vcteritorries VARCHAR(1000),
		vcTaxID VARCHAR(100),
		vcUserName VARCHAR(100),
		vcdepartment VARCHAR(100),
		bitLinkVisible BIT,
		bitpayroll BIT,
		numUserCntID NUMERIC,
		vcEmployeeId VARCHAR(50),
		monHourlyRate DECIMAL(20,5),
		decRegularHrs decimal(20,5),
		decPaidLeaveHrs decimal(20,5),
		monRegularHrsPaid DECIMAL(20,5),
		monAdditionalAmountPaid DECIMAL(20,5),
		monExpense DECIMAL(20,5),
		monReimburse DECIMAL(20,5),
		monReimbursePaid DECIMAL(20,5),
		monCommPaidInvoice DECIMAL(20,5),
		monCommPaidInvoiceDeposited DECIMAL(20,5),
		monCommUNPaidInvoice DECIMAL(20,5),
		monCommPaidCreditMemoOrRefund DECIMAL(20,5),
		monOverPayment DECIMAL(20,5),
		monCommissionEarned DECIMAL(20,5),
		monCommissionPaid DECIMAL(20,5),
		monInvoiceSubTotal DECIMAL(20,5),
		monOrderSubTotal  DECIMAL(20,5),
		monTotalAmountDue DECIMAL(20,5),
		monAmountPaid DECIMAL(20,5), 
		bitCommissionContact BIT,
		numDivisionID NUMERIC(18,0),
		bitPartner BIT,
		tintCRMType TINYINT
	)

	-- GET EMPLOYEE OF DOMAIN
	INSERT INTO #tempHrs
	(
		numUserId,
		vcteritorries,
		vcTaxID,
		vcUserName,
		vcdepartment,
		bitLinkVisible,
		bitpayroll,
		numUserCntID,
		vcEmployeeId,
		monHourlyRate,
		bitCommissionContact
	)
	SELECT 
		UM.numUserId,
		CONCAT(',',STUFF((SELECT CONCAT(',',numTerritoryID) FROM UserTerritory WHERE numUserCntID=UM.numUserDetailId AND numDomainID=@numDomainId FOR XML PATH('')),1,1,''),','),
		ISNULL(adc.vcTaxID,''),
		Isnull(ADC.vcfirstname + ' ' + adc.vclastname,'-') vcUserName,
		dbo.fn_GetListItemName(adc.vcdepartment),
		(CASE @tintUserRightForUpdate
			WHEN 0 THEN 0
			WHEN 1 THEN CASE WHEN UM.numUserDetailId = @numOrgUserCntID THEN 1 ELSE 0 END
			WHEN 2 THEN CASE WHEN UM.numUserDetailId IN (SELECT numUserCntID FROM UserTerritory WHERE numUserCntID <> @numOrgUserCntID AND numTerritoryID IN (SELECT numTerritoryID FROM UserTerritory WHERE numUserCntID = @numOrgUserCntID)) THEN 1 ELSE 0 END
			ELSE 1
		END),
		Isnull(um.bitpayroll,0) AS bitpayroll,
		adc.numcontactid AS numUserCntID,
		Isnull(um.vcEmployeeId,'') AS vcEmployeeId,
		ISNULL(UM.monHourlyRate,0),
		0
	FROM  
		dbo.UserMaster UM 
	JOIN 
		dbo.AdditionalContactsInformation adc 
	ON 
		adc.numDomainID=@numDomainId 
		AND adc.numcontactid = um.numuserdetailid
	WHERE 
		UM.numDomainId=@numDomainId AND (adc.vcdepartment=@numDepartmentId OR @numDepartmentId=0)
		AND ISNULL(UM.bitPayroll,0) = 1
		AND 1 = (CASE @tintUserRightType
					WHEN 1 
					THEN CASE WHEN UM.numUserDetailId = @numOrgUserCntID THEN 1 ELSE 0 END
					WHEN 2 THEN CASE WHEN UM.numUserDetailId IN (SELECT numUserCntID FROM UserTerritory WHERE numUserCntID <> @numOrgUserCntID AND numTerritoryID IN (SELECT numTerritoryID FROM UserTerritory WHERE numUserCntID = @numOrgUserCntID)) THEN 1 ELSE 0 END
					ELSE 1
				END)

	-- GET COMMISSION CONTACTS OF DOMAIN
	INSERT INTO #tempHrs 
	(
		numUserId,
		vcTaxID,
		vcUserName,
		numUserCntID,
		vcEmployeeId,
		vcdepartment,
		bitLinkVisible,
		bitpayroll,
		monHourlyRate,
		bitCommissionContact
	)
	SELECT  
		0,
		ISNULL(A.vcTaxID,''),
		ISNULL(A.vcFirstName +' '+ A.vcLastName + '(' + C.vcCompanyName +')','-'),
		A.numContactId,
		'',
		'-',
		(CASE WHEN @tintUserRightForUpdate = 0 OR @tintUserRightForUpdate = 1 OR @tintUserRightForUpdate = 2 THEN 0 ELSE 1 END),
		0,
		0,
		1
	FROM 
		CommissionContacts CC 
	JOIN DivisionMaster D ON CC.numDivisionID=D.numDivisionID
	JOIN AdditionalContactsInformation A ON D.numDivisionID=A.numDivisionID
	JOIN CompanyInfo C ON D.numCompanyID = C.numCompanyId
	WHERE 
		CC.numDomainID=@numDomainID
		AND 1 = (CASE 
					WHEN @tintUserRightType = 1 THEN (CASE WHEN EXISTS (SELECT EAD.numExtranetDtlID FROM ExtranetAccountsDtl EAD INNER JOIN ExtarnetAccounts EA ON EAD.numExtranetID=EA.numExtranetID WHERE EAD.numDomainID=@numDomainID AND EAD.numContactID=@numOrgUserCntID AND EA.numDivisionID=D.numDivisionID) THEN 1 ELSE 0 END)
					WHEN @tintUserRightType = 2 THEN 0 
					ELSE 1 
				END)


	-- GET PARTNERS OF DOMAIN
	INSERT INTO #tempHrs 
	(
		numUserId,
		vcTaxID,
		vcUserName,
		numUserCntID,
		vcEmployeeId,
		vcdepartment,
		bitLinkVisible,
		bitpayroll,
		monHourlyRate,
		numDivisionID,
		bitPartner,
		bitCommissionContact,
		tintCRMType
	)
	SELECT  
		0,
		ISNULL(A.vcTaxID,''),
		ISNULL(C.vcCompanyName,'-'),
		0,
		'',
		'-',
		(CASE WHEN @tintUserRightForUpdate = 0 OR @tintUserRightForUpdate = 1 OR @tintUserRightForUpdate = 2 THEN 0 ELSE 1 END),
		0,
		0,
		D.numDivisionID,
		1,
		1,
		tintCRMType
	FROM  
		DivisionMaster AS D 
	LEFT JOIN 
		CompanyInfo AS C
	ON 
		D.numCompanyID=C.numCompanyID
	LEFT JOIN
		AdditionalContactsInformation A
	ON
		A.numDivisionID = D.numDivisionID
		AND ISNULL(A.bitPrimaryContact,1) = 1
	WHERE 
		D.numDomainID=@numDomainID 
		AND D.vcPartnerCode <> ''
		AND 1 = (CASE WHEN @tintUserRightType = 1 OR @tintUserRightType = 2 THEN 0 ELSE 1 END)
	ORDER BY
		vcCompanyName


	Declare @decTotalHrsWorked as decimal(10,2)
	DECLARE @monReimburse DECIMAL(20,5),@monCommPaidInvoice DECIMAL(20,5),@monCommPaidInvoiceDepositedToBank DECIMAL(20,5),@monCommUNPaidInvoice DECIMAL(20,5),@monCommPaidCreditMemoOrRefund DECIMAL(20,5)

	DECLARE @i INT = 1
	DECLARE @COUNT INT = 0
	DECLARE @numUserCntID NUMERIC(18,0)
	DECLARE @numDivisionID NUMERIC(18,0)
	DECLARE @bitCommissionContact BIT
	DECLARE @bitPartner BIT
    
	SELECT @COUNT=COUNT(*) FROM #tempHrs

	-- LOOP OVER EACH EMPLOYEE
	WHILE @i <= @COUNT
	BEGIN
		SELECT @decTotalHrsWorked=0,@monReimburse=0,@monCommPaidInvoice=0,@monCommPaidInvoiceDepositedToBank=0,@monCommUNPaidInvoice=0, @numUserCntID=0,@bitCommissionContact=0,@monCommPaidCreditMemoOrRefund=0
		
		SELECT
			@numUserCntID = ISNULL(numUserCntID,0),
			@bitCommissionContact = ISNULL(bitCommissionContact,0),
			@numDivisionID=ISNULL(numDivisionID,0),
			@bitPartner=ISNULL(bitPartner,0)
		FROM
			#tempHrs
		WHERE
			ID = @i
		
		-- EMPLOYEE (ONLY COMMISSION AMOUNT IS NEEDED FOR COMMISSION CONTACTS)
		IF @bitCommissionContact = 0
		BEGIN
			-- CALCULATE REGULAR HRS
			SELECT  
				@decTotalHrsWorked=ISNULL(SUM(numHours),0)       
			FROM 
				TimeAndExpenseCommission 
			WHERE 
				numComPayPeriodID = @numComPayPeriodID
				AND numUserCntID=@numUserCntID
				AND (numType=1 OR numType=2) 
				AND numCategory=1

			SET @decTotalHrsWorked = ISNULL(@decTotalHrsWorked,0) + ISNULL((SELECT  
																			SUM(numHours)   
																		FROM 
																			StagePercentageDetailsTaskCommission 
																		WHERE 
																			numComPayPeriodID = @numComPayPeriodID
																			AND numUserCntID=@numUserCntID),0)

			-- CALCULATE REIMBURSABLE EXPENSES
			SELECT  
				@monReimburse=ISNULL(SUM(monTotalAmount),0)       
			FROM 
				TimeAndExpenseCommission 
			WHERE 
				numComPayPeriodID = @numComPayPeriodID
				AND numUserCntID=@numUserCntID
				AND numCategory=2
				AND bitReimburse = 1
		END

		-- CALCULATE COMMISSION PAID INVOICE
		SELECT 
			@monCommPaidInvoice = ISNULL(SUM(BC.numComissionAmount),0)
		FROM 
			BizDocComission BC
		INNER JOIN 
			OpportunityBizDocs oppBiz 
		ON 
			BC.numOppId =oppBiz.numOppId
		WHERE 
			BC.numComPayPeriodID = @numComPayPeriodID
			AND ((BC.numUserCntId=@numUserCntID AND ISNULL(BC.tintAssignTo,0) <> 3) OR (BC.numUserCntID=@numDivisionID AND BC.tintAssignTo=3))
			AND oppBiz.bitAuthoritativeBizDocs = 1 
			AND isnull(oppBiz.monAmountPaid,0) >=oppBiz.monDealAmount 

		-- CALCULATE COMMISSION PAID INVOICE (DEPOSITED TO BANK)
		SELECT 
			@monCommPaidInvoiceDepositedToBank = ISNULL(SUM(BC.numComissionAmount),0)
		FROM 
			BizDocComission BC
		INNER JOIN 
			OpportunityBizDocs oppBiz 
		ON 
			BC.numOppId =oppBiz.numOppId 
		CROSS APPLY
		(
			SELECT 
				MAX(DM.dtDepositDate) dtDepositDate
			FROM 
				DepositMaster DM 
			JOIN dbo.DepositeDetails DD	ON DM.numDepositId=DD.numDepositID 
			WHERE 
				DM.tintDepositePage = 2
				AND DM.numDomainId=@numDomainId
				AND DD.numOppID=oppBiz.numOppID 
				AND DD.numOppBizDocsID =oppBiz.numOppBizDocsID
				AND DM.tintDepositePage=2 
				AND 1 = (CASE WHEN DM.tintDepositeToType=2 THEN (CASE WHEN ISNULL(DM.bitDepositedToAcnt,0)=1 THEN 1 ELSE 0 END) ELSE 1 END)
		) TEMPDeposit
		WHERE 
			BC.numComPayPeriodID = @numComPayPeriodID
			AND ((BC.numUserCntId=@numUserCntID AND ISNULL(BC.tintAssignTo,0) <> 3) OR (BC.numUserCntID=@numDivisionID AND BC.tintAssignTo=3))
			AND oppBiz.bitAuthoritativeBizDocs = 1 
			AND isnull(oppBiz.monAmountPaid,0) >=oppBiz.monDealAmount
		

		-- CALCULATE COMMISSION UNPAID INVOICE 
		SELECT 
			@monCommUNPaidInvoice=ISNULL(SUM(BC.numComissionAmount),0) 
		FROM 
			BizDocComission BC
		INNER JOIN 
			OpportunityBizDocs oppBiz 
		ON 
			BC.numOppId =oppBiz.numOppId
		WHERE 
			BC.numComPayPeriodID = @numComPayPeriodID
			AND ((BC.numUserCntId=@numUserCntID AND ISNULL(BC.tintAssignTo,0) <> 3) OR (BC.numUserCntID=@numDivisionID AND BC.tintAssignTo=3))
			AND oppBiz.bitAuthoritativeBizDocs = 1 
			AND ISNULL(oppBiz.monAmountPaid,0) <oppBiz.monDealAmount

		-- CALCULATE COMMISSION FOR PAID CREDIT MEMO/REFUND
		SET @monCommPaidCreditMemoOrRefund = ISNULL((SELECT 
														SUM(monCommissionReversed) 
													FROM 
														SalesReturnCommission 
													WHERE 
														numComPayPeriodID=@numComPayPeriodID
														AND 1 = (CASE 
																WHEN ISNULL(@bitPartner,0) = 1 
																THEN (CASE WHEN numUserCntID=@numDivisionID AND tintAssignTo=3 THEN 1 ELSE 0 END)
																ELSE (CASE WHEN numUserCntId=@numUserCntID AND ISNULL(tintAssignTo,0) <> 3 THEN 1 ELSE 0 END)
															END)),0)	
	
		UPDATE 
			 TH
		SET    
			decRegularHrs=ISNULL(@decTotalHrsWorked,0)
			,monReimburse=ISNULL(@monReimburse,0)
			,monCommPaidInvoice=ISNULL(@monCommPaidInvoice,0)
			,monCommPaidInvoiceDeposited=ISNULL(@monCommPaidInvoiceDepositedToBank,0)
			,monCommUNPaidInvoice=ISNULL(@monCommUNPaidInvoice,0)
			,monCommPaidCreditMemoOrRefund=ISNULL(@monCommPaidCreditMemoOrRefund,0)
			,monCommissionEarned = (CASE 
										WHEN ISNULL(bitPartner,0) = 1 
										THEN ISNULL((SELECT SUM(numComissionAmount) FROM BizDocComission WHERE BizDocComission.numComPayPeriodID=@numComPayPeriodID AND BizDocComission.numUserCntID=@numDivisionID AND BizDocComission.tintAssignTo=3),0) 
										ELSE ISNULL((SELECT SUM(numComissionAmount) FROM BizDocComission WHERE BizDocComission.numComPayPeriodID=@numComPayPeriodID AND BizDocComission.numUserCntID=@numUserCntID AND BizDocComission.tintAssignTo <> 3),0)
									END)
			,monOverPayment=(CASE 
								WHEN ISNULL(bitPartner,0) = 1 
								THEN ISNULL((SELECT SUM(monDifference) FROM BizDocComissionPaymentDifference BCPD INNER JOIN BizDocComission BCInner ON BCPD.numComissionID=BCInner.numComissionID WHERE BCInner.numDomainId=@numDomainId AND (ISNULL(BCPD.bitDifferencePaid,0)=0 OR ISNULL(BCPD.numComPayPeriodID,0) = @numComPayPeriodID) AND BCInner.numUserCntID=@numDivisionID AND BCInner.tintAssignTo=3),0) 
								ELSE ISNULL((SELECT SUM(monDifference) FROM BizDocComissionPaymentDifference BCPD INNER JOIN BizDocComission BCInner ON BCPD.numComissionID=BCInner.numComissionID WHERE BCInner.numDomainId=@numDomainId AND (ISNULL(BCPD.bitDifferencePaid,0)=0 OR ISNULL(BCPD.numComPayPeriodID,0) = @numComPayPeriodID) AND BCInner.numUserCntID=@numUserCntID AND BCInner.tintAssignTo <> 3),0)
							END)
			,monInvoiceSubTotal = ISNULL(TMEPInvoice.monSubTotal,0)
			,monOrderSubTotal = ISNULL(TMEPOrder.monSubTotal,0)
		FROM
			#tempHrs TH
		OUTER APPLY
		(
			SELECT 
				SUM(monInvoiceSubTotal) monSubTotal
			FROM 
			(
				SELECT 
					numOppBizDocId,numOppBizDocItemID,monInvoiceSubTotal 
				FROM
					BizDocComission 
				WHERE 
					numComPayPeriodID=@numComPayPeriodID 
					AND BizDocComission.numUserCntID= (CASE WHEN ISNULL(TH.bitPartner,0) = 1 THEN @numDivisionID ELSE @numUserCntID END) 
					AND 1 = (CASE 
								WHEN ISNULL(TH.bitPartner,0) = 1 
								THEN (CASE WHEN BizDocComission.tintAssignTo=3 THEN 1 ELSE 0 END) 
								ELSE (CASE WHEN BizDocComission.tintAssignTo<>3 THEN 1 ELSE 0 END) 
							END) 
				GROUP BY 
					numOppBizDocId,numOppBizDocItemID,monInvoiceSubTotal
			) TEMP
		) TMEPInvoice
		OUTER APPLY
		(
			SELECT 
				SUM(monOrderSubTotal) monSubTotal
			FROM 
			(
				SELECT 
					numOppBizDocId,numOppBizDocItemID,monOrderSubTotal 
				FROM
					BizDocComission 
				WHERE 
					numComPayPeriodID=@numComPayPeriodID 
					AND BizDocComission.numUserCntID=(CASE WHEN ISNULL(TH.bitPartner,0) = 1 THEN @numDivisionID ELSE @numUserCntID END) 
					AND 1 = (CASE 
								WHEN ISNULL(TH.bitPartner,0) = 1 
								THEN (CASE WHEN BizDocComission.tintAssignTo=3 THEN 1 ELSE 0 END) 
								ELSE (CASE WHEN BizDocComission.tintAssignTo<>3 THEN 1 ELSE 0 END) 
							END) 
				GROUP BY 
					numOppBizDocId,numOppBizDocItemID,monOrderSubTotal
			) TEMP
		) TMEPOrder
        WHERE 
			(numUserCntID=@numUserCntID AND ISNULL(bitPartner,0) = 0) 
			OR (numDivisionID=@numDivisionID AND ISNULL(bitPartner,0) = 1)

		SET @i = @i + 1
	END

	-- CALCULATE PAID AMOUNT
	UPDATE 
		temp 
	SET 
		monAmountPaid=ISNULL(T1.monTotalAmt,0)
		,monRegularHrsPaid=ISNULL(T1.monHourlyAmt,0)
		,monAdditionalAmountPaid=ISNULL(T1.monAdditionalAmt,0)
		,monReimbursePaid=ISNULL(T1.monReimbursableExpenses,0)
		,monCommissionPaid=ISNULL(T1.monCommissionAmt,0)
		,monTotalAmountDue= (temp.decRegularHrs * monHourlyRate) + ISNULL(monReimburse,0) + ISNULL(monCommissionEarned,0) + ISNULL(monOverPayment,0) - ISNULL(monCommPaidCreditMemoOrRefund,0)
	FROM 
		#tempHrs temp  
	LEFT JOIN
	(
		SELECT
			numUserCntID
			,numDivisionID
			,monHourlyAmt
			,monAdditionalAmt
			,monReimbursableExpenses
			,monCommissionAmt
			,monTotalAmt
		FROM
			dbo.PayrollHeader PH
		INNER JOIN
			PayrollDetail PD 
		ON 
			PH.numPayrollHeaderID = PD.numPayrollHeaderID
		WHERE
			PH.numComPayPeriodID = @numComPayPeriodID
	) T1
	ON
		1 = (CASE 
				WHEN ISNULL(bitPartner,0) = 1  
				THEN (CASE WHEN temp.numDivisionID = T1.numDivisionID THEN 1 ELSE 0 END)
				ELSE (CASE WHEN temp.numUserCntID = T1.numUserCntID THEN 1 ELSE 0 END)
			END)
		
 
	SELECT * FROM #tempHrs
	DROP TABLE #tempHrs
END
