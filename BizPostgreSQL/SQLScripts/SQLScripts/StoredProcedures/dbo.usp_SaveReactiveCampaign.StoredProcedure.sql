/****** Object:  StoredProcedure [dbo].[usp_SaveReactiveCampaign]    Script Date: 07/26/2008 16:21:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_savereactivecampaign')
DROP PROCEDURE usp_savereactivecampaign
GO
CREATE PROCEDURE [dbo].[usp_SaveReactiveCampaign]
	(
		@numItemDetailId numeric,
		@sintYear numeric,
		@tintquarter numeric,
		@tintMonth numeric,
		@FirstCampaignAmount DECIMAL(20,5),
		@SecondCampaignAmount DECIMAL(20,5),
		@ThirdCampaignAmount DECIMAL(20,5),
		@numUserID numeric,
		@bintDate numeric,
		@numDomainID numeric
	)
AS
Declare @numReactiveCampaignId numeric
	Set @numReactiveCampaignId = 0
	Select @numReactiveCampaignID = numReactiveCampaignID from ReactiveCampaign 
	where 
		numItemDetailID = @numItemDetailID and
		sintYear = @sintYear and
		tintquarter = @tintquarter and
		tintMonth = @tintMonth
		If @numReactiveCampaignID < 1
		Begin
			Insert into ReactiveCampaign (numItemDetailID, sintYear, tintQuarter, tintMonth, FirstCampaignAmount, SecondCampaignAmount, ThirdCampaignAmount, numCreatedBy, bintCreatedDate, numDomainID)
			Values(@numItemDetailId, @sintYear, @tintquarter, @tintMonth, @FirstCampaignAmount, @SecondCampaignAmount, @ThirdCampaignAmount, @numUserID, @bintDate, @numDomainID)
		End
		ELSE
		Begin
			Update ReactiveCampaign set FirstCampaignAmount = @FirstCampaignAmount, SecondCampaignAmount = @SecondCampaignAmount, ThirdCampaignAmount = @ThirdCampaignAmount, numModifiedBy = @numUserID, bintModifiedDate = @bintDate
			Where numItemDetailID = @numItemDetailId and sintYear = @sintYear and tintQuarter = @tintquarter and tintMonth = @tintMonth
		END
GO
