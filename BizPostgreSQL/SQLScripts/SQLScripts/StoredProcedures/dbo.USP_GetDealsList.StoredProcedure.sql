/****** Object:  StoredProcedure [dbo].[USP_GetDealsList]    Script Date: 07/26/2008 16:17:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj                            
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getdealslist')
DROP PROCEDURE usp_getdealslist
GO
CREATE PROCEDURE [dbo].[USP_GetDealsList]                                            
 @numUserCntID numeric(9)=0,                                            
 @numDomainID numeric(9)=0,                                            
 @tintSalesUserRightType tinyint=0,                                   
 @tintPurchaseUserRightType tinyint=0,                                            
 @tintSortOrder tinyint=4,                                          
 @SortChar char(1)='0',                                            
 @FirstName varChar(100)= '',                                            
 @LastName varchar(100)='',                                            
 @CustName varchar(100)='' ,                                                  
 @CurrentPage int,                                            
 @PageSize int,                                            
 @TotRecs int output,                                            
 @columnName as Varchar(50),                                            
 @columnSortOrder as Varchar(10),                                          
 @numCompanyID as numeric(9)=0 ,                
 @bitPartner as bit=0  ,        
 @ClientTimeZoneOffset as int ,    
 @tintShipped as tinyint  ,  
@intOrder    as tinyint                                          
as                                            
                                            
--Create a Temporary table to hold data                                            
Create table #tempTable ( ID INT IDENTITY PRIMARY KEY,                            
    numOppId numeric(9),                            
    vcPOppName varchar(100),                            
    vcCompanyName varchar(50),                            
    vcFirstName varchar(50),                            
    vcusername varchar(50),                            
    tintOppType varchar(1),                            
    bintAccountClosingDate datetime,                            
    monPAmount DECIMAL(20,5),                            
    STAGE varchar(20),                            
    bintCreatedDate datetime,                        
                        
     )                            
                            
                                            
declare @strSql as varchar(8000)                                            
set @strSql='SELECT  Opp.numOppId,                            
  Opp.vcPOppName,                            
  C.vcCompanyName,                            
  ADC.vcFirstName,                        
  ADC1.vcFirstName,                                                                   
  Opp.tintOppType,                            
  Opp.bintAccountClosingDate,                            
  Opp.monPAmount,                               
  dbo.GetOppStatus(Opp.numOppId) as STAGE,                            
  opp.bintCreatedDate                                         
  FROM OpportunityMaster Opp                                             
  INNER JOIN AdditionalContactsInformation ADC                                             
  ON Opp.numContactId = ADC.numContactId                                             
  INNER JOIN DivisionMaster Div                                             
  ON Opp.numDivisionId = Div.numDivisionID AND ADC.numDivisionId = Div.numDivisionID                                             
  INNER JOIN CompanyInfo C                                             
  ON Div.numCompanyID = C.numCompanyId '                
  if @bitPartner=1 set @strSql=@strSql+'left join OpportunityContact OppCont on OppCont.numOppId=Opp.numOppId and OppCont.bitPartner=1 and OppCont.numContactId='+convert(varchar(15),@numUserCntID)+ ''                
                
                                                
  set @strSql=@strSql+'left join AdditionalContactsInformation ADC1 on ADC1.numContactId=Opp.numRecOwner                                             
  WHERE Opp.tintOppstatus=1 and Opp.tintShipped='+ convert(varchar(15),@tintShipped) +' and  Opp.tintOppType=1                                            
  and Div.numDomainID=' + convert(varchar(15),@numDomainID)+''     
                  
  if @FirstName<>'' set @strSql=@strSql+'and ADC.vcFirstName  like '''+@FirstName+'%'''                                             
  if @LastName<>'' set @strSql=@strSql+'and ADC.vcLastName like '''+@LastName+'%'''                  
  if @CustName<>'' set @strSql=@strSql+'and C.vcCompanyName like '''+@CustName+'%'''                 
   if @intOrder <> 0  
 begin  
  if @intOrder = 1  
   set @strSql=@strSql+' and Opp.bitOrder = 0 '   
  if @intOrder = 2  
   set    @strSql=@strSql+' and Opp.bitOrder = 1'                                       
 end     
                                           
  if @numCompanyID<>0 set @strSql=@strSql + ' And Div.numCompanyID =' + convert(varchar(15),@numCompanyID)                                            
if @SortChar<>'0' set @strSql=@strSql + ' And Opp.vcPOppName like '''+@SortChar+'%'''                                    
if @tintSalesUserRightType=0 set @strSql=@strSql + ' AND opp.tintOppType =0'                                            
if @tintSalesUserRightType=1 set @strSql=@strSql + ' AND (Opp.numRecOwner= '+ convert(varchar(15),@numUserCntID)  +' or Opp.numAssignedTo= '+ convert(varchar(15),@numUserCntID)  +' or  Opp.numOppId in             
(select distinct(numOppId) from OpportunityStageDetails where  numAssignTo ='+ convert(varchar(15),@numUserCntID)+')'+ case when @bitPartner=1 then ' or ( OppCont.bitPartner=1 and OppCont.numContactId='+convert(varchar(15),@numUserCntID)+ ')' else '' end 
 
    
      
        
           
+')'                                                 
else if @tintSalesUserRightType=2 set @strSql=@strSql + ' AND (Div.numTerID in (select numTerritoryID from  UserTerritory where numUserCntID= '+convert(varchar(15),@numUserCntID)+' ) or div.numTerID=0 or             
Opp.numAssignedTo= '+ convert(varchar(15),@numUserCntID)  +' or  Opp.numOppId in (select distinct(numOppId)             
from OpportunityStageDetails where numAssignTo ='+ convert(varchar(15),@numUserCntID)+'))'                                         
if @tintSortOrder<>'0'  set @strSql=@strSql + '  AND Opp.tintOppType= '+ convert(varchar(1),@tintSortOrder)                                   
                                 
                                  
set @strSql= @strSql+' union SELECT  Opp.numOppId,                            
  Opp.vcPOppName,                            
  C.vcCompanyName,                            
  ADC.vcFirstName,                        
  ADC1.vcFirstName ,                                                                   
  Opp.tintOppType,                            
  Opp.bintAccountClosingDate,                            
  Opp.monPAmount,                               
  dbo.GetOppStatus(Opp.numOppId) as STAGE,                            
  opp.bintCreatedDate                                              
  FROM OpportunityMaster Opp                                             
  INNER JOIN AdditionalContactsInformation ADC                                             
  ON Opp.numContactId = ADC.numContactId                                             
  INNER JOIN DivisionMaster Div                                             
  ON Opp.numDivisionId = Div.numDivisionID AND ADC.numDivisionId = Div.numDivisionID                                             
  INNER JOIN CompanyInfo C                                             
   ON Div.numCompanyID = C.numCompanyId '                
  if @bitPartner=1 set @strSql=@strSql+'left join OpportunityContact OppCont on OppCont.numOppId=Opp.numOppId and OppCont.bitPartner=1 and OppCont.numContactId='+convert(varchar(15),@numUserCntID)+ ''                
                
                                                
  set @strSql=@strSql+'left join AdditionalContactsInformation ADC1 on ADC1.numContactId=Opp.numRecOwner                                             
  WHERE Opp.tintOppstatus=1 and Opp.tintShipped='+ convert(varchar(15),@tintShipped) +'   and Div.numDomainID=' + convert(varchar(15),@numDomainID)+''                   
  if @FirstName<>'' set @strSql=@strSql+'and ADC.vcFirstName  like '''+@FirstName+'%'''                                             
  if @LastName<>'' set @strSql=@strSql+'and ADC.vcLastName like '''+@LastName+'%'''                  
  if @CustName<>'' set @strSql=@strSql+'and C.vcCompanyName like '''+@CustName+'%'''                                                                          
  if @numCompanyID<>0 set @strSql=@strSql + ' And Div.numCompanyID =' + convert(varchar(15),@numCompanyID)                                            
  if @SortChar<>'0' set @strSql=@strSql + ' And Opp.vcPOppName like '''+@SortChar+'%'''    
  
  
 if @intOrder <> 0  
 begin  
  if @intOrder = 1  
   set @strSql=@strSql+' and Opp.bitOrder = 0 '   
  if @intOrder = 2  
   set    @strSql=@strSql+' and Opp.bitOrder = 1'                                       
 end   
  
                                           
if @tintPurchaseUserRightType=0 set @strSql=@strSql + ' AND opp.tintOppType =0'                                      
if @tintPurchaseUserRightType=1 set @strSql=@strSql + ' AND (Opp.numRecOwner= '+ convert(varchar(15),@numUserCntID)  +' or Opp.numAssignedTo= '+ convert(varchar(15),@numUserCntID)  +' or  Opp.numOppId in             
(select distinct(numOppId) from OpportunityStageDetails where               
numAssignTo ='+ convert(varchar(15),@numUserCntID)+')'+ case when @bitPartner=1 then ' or ( OppCont.bitPartner=1 and OppCont.numContactId='+convert(varchar(15),@numUserCntID)+ ')' else '' end +')'                                                           
  
    
     
         
          
            
else if @tintPurchaseUserRightType=2 set @strSql=@strSql + ' AND (Div.numTerID in (select numTerritoryID from  UserTerritory where numUserCntID= '+convert(varchar(15),@numUserCntID)+' ) or div.numTerID=0 or             
Opp.numAssignedTo= '+ convert(varchar(15),@numUserCntID)  +' or  Opp.numOppId in (select distinct(numOppId)             
from OpportunityStageDetails where numAssignTo ='+ convert(varchar(15),@numUserCntID)+'))'                                                                        
            
            
if @tintSortOrder<>'0'  set @strSql=@strSql + ' AND Opp.tintOppType= '+ convert(varchar(1),@tintSortOrder)                                                              
set @strSql=@strSql + ' ORDER BY  ' + @columnName +' '+ @columnSortOrder                                                           
print @strSql                                          
insert into #tempTable(numOppId,                            
    vcPOppName,                            
    vcCompanyName,                            
    vcFirstName,                            
    vcusername,                            
    tintOppType,                            
    bintAccountClosingDate,                            
    monPAmount,                            
    STAGE,bintCreatedDate )                                            
exec (@strSql)                                             
                                            
  declare @firstRec as integer                                             
  declare @lastRec as integer                                            
 set @firstRec= (@CurrentPage-1) * @PageSize                                            
     set @lastRec= (@CurrentPage*@PageSize+1)                                                           
set @TotRecs=(select count(*) from #tempTable)                              
                            
SELECT  Opp.numOppId,                                             
  Opp.vcPOppName AS Name,                                             
  dbo.GetOppStatus(Opp.numOppId) as STAGE,                                             
  Opp.intPEstimatedCloseDate AS CloseDate,                                  
  Opp.numRecOwner,                                           
  dateadd(minute,-@ClientTimeZoneOffset,opp.bintCreatedDate)as bintCreatedDate ,                                            
  ADC.vcFirstName + ' ' + ADC.vcLastName AS Contact,                                             
  C.vcCompanyName AS Company,                                            
  C.numCompanyID,                                            
  Div.tintCRMType,                                            
  ADC.numContactID,                                            
  Div.numDivisionID,                                            
  Div.numTerID,                                            
  Opp.monPAmount,                                            
  ADC1.vcFirstName+' '+ADC1.vcLastName as [UserName],                                  
 Opp.tintOppType,      
  dateadd(minute,-@ClientTimeZoneOffset,Opp.bintAccountClosingDate) as bintAccountClosingDate,                  
  dbo.fn_GetContactName(Opp.numAssignedTo)+'/ '+dbo.fn_GetContactName(Opp.numAssignedBy) as AssignedToBy                                            
  FROM OpportunityMaster Opp                                             
  INNER JOIN AdditionalContactsInformation ADC                                             
  ON Opp.numContactId = ADC.numContactId                                             
  INNER JOIN DivisionMaster Div                                             
  ON Opp.numDivisionId = Div.numDivisionID AND ADC.numDivisionId = Div.numDivisionID                                             
  INNER JOIN CompanyInfo C                                             
  ON Div.numCompanyID = C.numCompanyId                                            
  left join AdditionalContactsInformation ADC1 on ADC1.numContactId=Opp.numRecOwner                            
  join #tempTable T on T.numOppId=Opp.numOppId                            
  WHERE ID > @firstRec and ID < @lastRec order by ID                            
                            
                                          
drop table #tempTable
GO
