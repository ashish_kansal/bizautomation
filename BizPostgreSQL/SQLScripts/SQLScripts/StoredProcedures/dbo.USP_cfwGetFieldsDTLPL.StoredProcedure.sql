/****** Object:  StoredProcedure [dbo].[USP_cfwGetFieldsDTLPL]    Script Date: 07/26/2008 16:15:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj                    
--Obsolete("Procedure contains old code needs updation")
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_cfwgetfieldsdtlpl')
DROP PROCEDURE usp_cfwgetfieldsdtlpl
GO
CREATE PROCEDURE [dbo].[USP_cfwGetFieldsDTLPL]                              
@PageId as tinyint,                              
@numRelation as numeric(9),                              
@numRecordId as numeric(9),                  
@numDomainID as numeric(9)                              
as                              
--if @PageId= 1                              
--begin                              
--select fld_id,fld_type,fld_label,numlistid,tintFldReq,numOrder,            
--case when fld_type = 'Drop Down list Box' then (select vcData from listdetails where numlistitemid=case when  isnumeric(dbo.GetCustFldValue(fld_id,@PageId,@numRecordId))=1 then dbo.GetCustFldValue(fld_id,@PageId,@numRecordId) else 0 end)  
--else dbo.GetCustFldValue(fld_id,@PageId,@numRecordId) end as value,              
--subgrp as TabId,Grp_Name as tabname,vcURL  from CFW_Fld_Master join CFW_Fld_Dtl                              
--on Fld_id=numFieldId                              
--left join CFw_Grp_Master                               
--on subgrp=CFw_Grp_Master.Grp_id                              
--where CFW_Fld_Master.grp_id=@PageId and numRelation=@numRelation and CFW_Fld_Master.numDomainID=@numDomainID and  CFw_Grp_Master.tintType=0                       
-- union        
--select 0 as fld_id,'Frame' as fld_type,'' as fld_label,0 as numlistid,0,0,'' as Value,Grp_id as TabId,Grp_Name as tabname,vcURLF from CFw_Grp_Master        
--where tintType=1 and loc_Id = @PageId  and numDomainID=@numDomainID        
--order by subgrp,numOrder                             
--end                              
--                              
--if @PageId= 4                              
--begin                        
--declare @ContactId as numeric(9)                              
--select @ContactId=numContactId from cases where numCaseid=@numRecordId                              
--select @numRelation=numCompanyType from AdditionalContactsInformation AddC                              
--join DivisionMaster Div on AddC.numDivisionId=Div.numDivisionId                              
--join CompanyInfo Com on  Com.numCompanyId=Div.numCompanyId                              
--where AddC.numContactID=@ContactId                              
--                              
--select fld_id,fld_type,fld_label,numlistid,tintFldReq,numOrder,              
--case when fld_type = 'Drop Down list Box' then (select vcData from listdetails where numlistitemid=case when  isnumeric(dbo.GetCustFldValue(fld_id,@PageId,@numRecordId))=1 then dbo.GetCustFldValue(fld_id,@PageId,@numRecordId) else 0 end)  
--else dbo.GetCustFldValue(fld_id,@PageId,@numRecordId) end as value,              
--subgrp as TabId,Grp_Name as tabname,vcURL from CFW_Fld_Master join CFW_Fld_Dtl                              
--on Fld_id=numFieldId                              
--left join CFw_Grp_Master                               
--on subgrp=CFw_Grp_Master.Grp_id                              
--where CFW_Fld_Master.grp_id=@PageId and numRelation=@numRelation and CFW_Fld_Master.numDomainID=@numDomainID and  CFw_Grp_Master.tintType=0                           
----select @numRelation=numContactType from AdditionalContactsInformation AddC                              
----join DivisionMaster Div on AddC.numDivisionId=Div.numDivisionId                              
----join CompanyInfo Com on  Com.numCompanyId=Div.numCompanyId                              
----where AddC.numContactID=@numRecordId                              
--                              
--union        
--select 0 as fld_id,'Frame' as fld_type,'' as fld_label,0 as numlistid,0,0,'' as Value,Grp_id as TabId,Grp_Name as tabname,vcURLF from CFw_Grp_Master        
--where tintType=1 and loc_Id = @PageId   and numDomainID=@numDomainID                            
--   order by subgrp,numOrder                              
--                              
--end                              
--                              
--if @PageId= 2 or  @PageId= 3 or @PageId= 5 or @PageId= 6 or @PageId= 7 or @PageId= 8       or @PageId= 11                
--begin                       
--select fld_id,fld_type,fld_label,numlistid,              
--case when fld_type = 'Drop Down list Box' then (select vcData from listdetails where numlistitemid=case when  isnumeric(dbo.GetCustFldValue(fld_id,@PageId,@numRecordId))=1 then dbo.GetCustFldValue(fld_id,@PageId,@numRecordId) else 0 end)  
--else dbo.GetCustFldValue(fld_id,@PageId,@numRecordId) end as value,              
--subgrp as TabId,Grp_Name as tabname,vcURL from CFW_Fld_Master                               
--left join CFw_Grp_Master                               
--on subgrp=CFw_Grp_Master.Grp_id                              
--where CFW_Fld_Master.grp_id=@PageId and CFW_Fld_Master.numDomainID=@numDomainID and  CFw_Grp_Master.tintType=0        
--union      
--select 0 as fld_id,'Frame' as fld_type,'' as fld_label,0 as numlistid,'' as Value,Grp_id as TabId,Grp_Name as tabname,vcURLF from CFw_Grp_Master        
--where tintType=1 and loc_Id = @PageId       and numDomainID=@numDomainID                   
--end
GO
