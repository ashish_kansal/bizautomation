/****** Object:  StoredProcedure [dbo].[usp_InsertQBActionItem]    Script Date: 07/26/2008 16:19:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_insertqbactionitem')
DROP PROCEDURE usp_insertqbactionitem
GO
CREATE PROCEDURE [dbo].[usp_InsertQBActionItem]
	@vcSubject varchar(500),
	@vcStartDate varchar(10),
	@vcCompanyName varchar(255),
	@numUserID numeric(8),
	@numDomainID numeric(8),
	@bitStatus bit   
--

as
insert into QBActionItem values(@vcSubject,@vcStartDate,@vcCompanyName,@numUserID,@numDomainID,@bitStatus )
GO
