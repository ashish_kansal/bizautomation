/****** Object:  StoredProcedure [dbo].[usp_cfwListFiels]    Script Date: 07/26/2008 16:15:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_cfwlistfiels')
DROP PROCEDURE usp_cfwlistfiels
GO
CREATE PROCEDURE [dbo].[usp_cfwListFiels]  
@TabId as integer=null,  
@loc_id as tinyint=null,
@numDomainID as numeric(9)  
as  
  
if @TabId = -1   
begin  
Select fld_id,fld_label from CFW_Fld_Master   
where Grp_id=@loc_id  and numDomainID=@numDomainID 
order by fld_tab_ord  
end  
  
if @TabId !=-1  
begin  
Select fld_id,fld_label from CFW_Fld_Master   
where Grp_id=@loc_id  and subgrp=@TabId  and numDomainID=@numDomainID 
order by fld_tab_ord  
end
GO
