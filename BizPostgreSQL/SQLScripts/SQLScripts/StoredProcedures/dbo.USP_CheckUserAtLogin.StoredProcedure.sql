--Created by anoop jayaraj                                                          
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_checkuseratlogin')
DROP PROCEDURE usp_checkuseratlogin
GO
CREATE PROCEDURE [dbo].[USP_CheckUserAtLogin]                                                              
(                                                                                    
	@UserName as varchar(100)='',                                                                        
	@vcPassword as varchar(100)='',
	@vcLinkedinId as varchar(300)=''                                                                           
)                                                              
AS             
BEGIN
	DECLARE @listIds VARCHAR(MAX)

	SELECT 
		@listIds = COALESCE(@listIds+',' ,'') + CAST(UDTL.numUserId AS VARCHAR(100)) 
	FROM 
		UnitPriceApprover U
	Join 
		Domain D                              
	on 
		D.numDomainID=U.numDomainID
	Join  
		Subscribers S                            
	on 
		S.numTargetDomainID=D.numDomainID
	LEFT JOIN 
		UserMaster As UDTL
	ON
		U.numUserID=UDTL.numUserDetailId
	WHERE 
		1 = (CASE 
			WHEN @vcLinkedinId='N' THEN (CASE WHEN UDTL.vcEmailID=@UserName and UDTL.vcPassword=@vcPassword THEN 1 ELSE 0 END)
			ELSE (CASE WHEN UDTL.vcLinkedinId=@vcLinkedinId THEN 1 ELSE 0 END)
			END)
       

	DECLARE @bitIsBusinessPortalUser AS BIT = 0

	DECLARE @numUserCount INT = ISNULL((SELECT 
			COUNT(*)
		FROM 
			UserMaster U                              
		JOIN 
			Domain D                              
		ON 
			D.numDomainID=U.numDomainID
		JOIN 
			Subscribers S                            
		ON 
			S.numTargetDomainID=D.numDomainID
		WHERE 
			1 = (CASE 
													WHEN @vcLinkedinId='N' 
													THEN (CASE WHEN vcEmailID=@UserName and vcPassword=@vcPassword THEN 1 ELSE 0 END)
					ELSE (CASE WHEN vcLinkedinId=@vcLinkedinId THEN 1 ELSE 0 END)
				END) 
											AND bitActive=1),0)

	IF @numUserCount > 1
	BEGIN
		RAISERROR('MULTIPLE_RECORDS_WITH_SAME_EMAIL',16,1)
		RETURN;
	END   
	ELSE IF @numUserCount=1 AND EXISTS (SELECT 
											A.numContactId 
										FROM 
											AdditionalContactsInformation A                                  
										INNER JOIN 
											ExtranetAccountsDtl E                                  
										ON 
											A.numContactID=E.numContactID
										INNER JOIN
											ExtarnetAccounts EA
										ON
											E.numExtranetID = EA.numExtranetID
										WHERE 
											(E.vcUserName=@UserName OR vcemail=@UserName) 
											AND (vcPassword=@vcPassword) 
											AND bitPartnerAccess=1)
	BEGIN
		RAISERROR('MULTIPLE_RECORDS_WITH_SAME_EMAIL',16,1)
		RETURN;
	END
	ELSE IF @numUserCount = 1
	BEGIN
		SET @bitIsBusinessPortalUser = 0
	END
	ELSE IF @numUserCount = 0 AND (SELECT 
										COUNT(A.numContactId)
									FROM 
										AdditionalContactsInformation A                                  
									INNER JOIN 
										ExtranetAccountsDtl E                                  
									ON 
										A.numContactID=E.numContactID
									INNER JOIN
										ExtarnetAccounts EA
									ON
										E.numExtranetID = EA.numExtranetID                              
									WHERE 
										(E.vcUserName=@UserName OR vcemail=@UserName) 
										AND (vcPassword=@vcPassword) 
										AND bitPartnerAccess=1) > 1
	BEGIN
		RAISERROR('MULTIPLE_RECORDS_WITH_SAME_EMAIL',16,1)
		RETURN;
	END   
	ELSE IF EXISTS (SELECT 
						A.numContactId 
					FROM 
						AdditionalContactsInformation A                                  
					INNER JOIN 
						ExtranetAccountsDtl E                                  
					ON 
						A.numContactID=E.numContactID
					INNER JOIN
						ExtarnetAccounts EA
					ON
						E.numExtranetID = EA.numExtranetID                             
					WHERE 
						(E.vcUserName=@UserName OR vcemail=@UserName) 
						AND (vcPassword=@vcPassword) 
						AND bitPartnerAccess=1)
	BEGIN
		SET @bitIsBusinessPortalUser = 1
	END
	                                          
	/*check subscription validity */
	IF EXISTS(SELECT 
					*
			FROM 
				UserMaster U                              
			JOIN 
				Domain D                              
			ON 
				D.numDomainID=U.numDomainID
			JOIN 
				Subscribers S                            
			ON 
				S.numTargetDomainID=D.numDomainID
			WHERE 
				1 = (CASE 
						WHEN @vcLinkedinId='N' THEN (CASE WHEN vcEmailID=@UserName and vcPassword=@vcPassword THEN 1 ELSE 0 END)
						ELSE (CASE WHEN vcLinkedinId=@vcLinkedinId THEN 1 ELSE 0 END)
					END) 
				AND bitActive=1 
				AND getutcdate() > dtSubEndDate)
	BEGIN
		RAISERROR('SUB_RENEW',16,1)
		RETURN;
	END

	IF EXISTS(SELECT 
					*
			FROM 
				AdditionalContactsInformation A                                  
			INNER JOIN 
				ExtranetAccountsDtl E                                  
			ON 
				A.numContactID=E.numContactID
			INNER JOIN
				ExtarnetAccounts EA
			ON
				E.numExtranetID = EA.numExtranetID                          
			JOIN 
				Domain D                              
			ON 
				D.numDomainID=E.numDomainID
			JOIN 
				Subscribers S                            
			ON 
				S.numTargetDomainID=D.numDomainID
			WHERE 
				(E.vcUserName=@UserName OR A.vcemail=@UserName) 
				AND (vcPassword=@vcPassword) 
				AND bitPartnerAccess=1
				AND getutcdate() > dtSubEndDate)
	BEGIN
		RAISERROR('SUB_RENEW',16,1)
		RETURN;
	END

	IF @bitIsBusinessPortalUser = 1
	BEGIN
		SELECT TOP 1 
			0 numUserID
			,EAD.numExtranetDtlID
			,A.numContactId AS numUserDetailId
			,D.numCost
			,isnull(A.vcEmail,'') vcEmailID
			,'' vcEmailAlias
			,'' vcEmailAliasPassword
			,isnull(numGroupID,0) numGroupID
			,1 AS bitActivateFlag
			,'' vcMailNickName
			,'' txtSignature
			,isnull(EAD.numDomainID,0) numDomainID
			,isnull(EA.numDivisionID,0) numDivisionID
			,isnull(vcCompanyName,'') vcCompanyName
			,0 bitExchangeIntegration
			,0 bitAccessExchange
			,'' vcExchPath
			,'' vcExchDomain
			,'' vcExchUserName
			,isnull(vcFirstname,'') + ' ' + isnull(vcLastName,'') as ContactName
			,'' as vcExchPassword
			,tintCustomPagingRows
			,vcDateFormat
			,numDefCountry
			,tintComposeWindow
			,dateadd(day,-sintStartDate,getutcdate()) as StartDate
			,dateadd(day,sintNoofDaysInterval,dateadd(day,-sintStartDate,getutcdate())) as EndDate
			,tintAssignToCriteria
			,bitIntmedPage
			,tintFiscalStartMonth
			,numAdminID
			,isnull(A.numTeam,0) numTeam
			,isnull(D.vcCurrency,'') as vcCurrency
			,isnull(D.numCurrencyID,0) as numCurrencyID, 
			isnull(D.bitMultiCurrency,0) as bitMultiCurrency,              
			bitTrial,isnull(tintChrForComSearch,0) as tintChrForComSearch,  
			isnull(tintChrForItemSearch,0) as tintChrForItemSearch,              
			'' as vcSMTPServer  ,          
			0 as numSMTPPort      
			,0 bitSMTPAuth    
			,'' vcSmtpPassword    
			,0 bitSMTPSSL
			,0 bitSMTPServer,       
			0 bitImapIntegration, isnull(charUnitSystem,'E')   UnitSystem, datediff(day,getutcdate(),dtSubEndDate) as NoOfDaysLeft,
			ISNULL(bitCreateInvoice,0) bitCreateInvoice,
			ISNULL([numDefaultSalesBizDocId],0) numDefaultSalesBizDocId,
			ISNULL([numDefaultPurchaseBizDocId],0) numDefaultPurchaseBizDocId,
			vcDomainName,
			S.numSubscriberID,
			D.[tintBaseTax],
			'' ProfilePic,
			ISNULL(D.[numShipCompany],0) numShipCompany,
			ISNULL(D.[bitEmbeddedCost],0) bitEmbeddedCost,
			(Select isnull(numAuthoritativeSales,0) From AuthoritativeBizDocs Where numDomainId=D.numDomainID) numAuthoritativeSales,
			(Select isnull(numAuthoritativePurchase,0) From AuthoritativeBizDocs Where numDomainId=D.numDomainID) numAuthoritativePurchase,
			ISNULL(D.[numDefaultSalesOppBizDocId],0) numDefaultSalesOppBizDocId,
			ISNULL(tintDecimalPoints,2) tintDecimalPoints,
			ISNULL(D.tintBillToForPO,0) tintBillToForPO, 
			ISNULL(D.tintShipToForPO,0) tintShipToForPO,
			ISNULL(D.tintOrganizationSearchCriteria,0) tintOrganizationSearchCriteria,
			ISNULL(D.tintLogin,0) tintLogin,
			ISNULL(S.intFullUserConcurrency,0) intFullUserConcurrency,
			ISNULL(bitDocumentRepositary,0) bitDocumentRepositary,
			ISNULL(D.vcPortalName,'') vcPortalName,
			(SELECT COUNT(*) FROM dbo.Subscribers WHERE getutcdate() between dtSubStartDate and dtSubEndDate and bitActive=1),
			ISNULL(D.bitGtoBContact,0) bitGtoBContact,
			ISNULL(D.bitBtoGContact,0) bitBtoGContact,
			ISNULL(D.bitGtoBCalendar,0) bitGtoBCalendar,
			ISNULL(D.bitBtoGCalendar,0) bitBtoGCalendar,
			ISNULL(bitExpenseNonInventoryItem,0) bitExpenseNonInventoryItem,
			ISNULL(D.bitInlineEdit,0) bitInlineEdit,
			0 numDefaultClass,
			ISNULL(D.bitRemoveVendorPOValidation,0) bitRemoveVendorPOValidation,
			ISNULL(D.bitTrakDirtyForm,1) bitTrakDirtyForm,
			isnull(D.tintBaseTaxOnArea,0) tintBaseTaxOnArea,
			ISNULL(D.bitAmountPastDue,0) bitAmountPastDue,
			CONVERT(DECIMAL(18,2),ISNULL(D.monAmountPastDue,0)) monAmountPastDue,
			ISNULL(D.bitSearchOrderCustomerHistory,0) bitSearchOrderCustomerHistory,
			0 as tintTabEvent,
			isnull(D.bitRentalItem,0) as bitRentalItem,
			isnull(D.numRentalItemClass,0) as numRentalItemClass,
			isnull(D.numRentalHourlyUOM,0) as numRentalHourlyUOM,
			isnull(D.numRentalDailyUOM,0) as numRentalDailyUOM,
			isnull(D.tintRentalPriceBasedOn,0) as tintRentalPriceBasedOn,
			isnull(D.tintCalendarTimeFormat,24) as tintCalendarTimeFormat,
			isnull(D.tintDefaultClassType,0) as tintDefaultClassType,
			0 numDefaultWarehouse,
			ISNULL(D.tintPriceBookDiscount,0) tintPriceBookDiscount,
			ISNULL(D.bitAutoSerialNoAssign,0) bitAutoSerialNoAssign,
			ISNULL(IsEnableProjectTracking,0) AS [IsEnableProjectTracking],
			ISNULL(IsEnableCampaignTracking,0) AS [IsEnableCampaignTracking],
			ISNULL(IsEnableResourceScheduling,0) AS [IsEnableResourceScheduling],
			ISNULL(D.numSOBizDocStatus,0) AS numSOBizDocStatus,
			ISNULL(D.bitAutolinkUnappliedPayment,0) AS bitAutolinkUnappliedPayment,
			ISNULL(numShippingServiceItemID,0) AS [numShippingServiceItemID],
			ISNULL(D.numDiscountServiceItemID,0) AS numDiscountServiceItemID,
			ISNULL(D.tintCommissionType,1) AS tintCommissionType,
			ISNULL(D.bitDiscountOnUnitPrice,0) AS bitDiscountOnUnitPrice,
			ISNULL(D.intShippingImageWidth,0) AS intShippingImageWidth ,
			ISNULL(D.intShippingImageHeight,0) AS intShippingImageHeight,
			ISNULL(D.numTotalInsuredValue,0) AS numTotalInsuredValue,
			ISNULL(D.numTotalCustomsValue,0) AS numTotalCustomsValue,
			ISNULL(D.bitUseBizdocAmount,1) AS [bitUseBizdocAmount],
			ISNULL(D.bitDefaultRateType,1) AS [bitDefaultRateType],
			ISNULL(D.bitIncludeTaxAndShippingInCommission,1) AS [bitIncludeTaxAndShippingInCommission],
			ISNULL(D.[bitLandedCost],0) AS bitLandedCost,
			ISNULL(D.vcLanedCostDefault,'') AS vcLanedCostDefault,
			ISNULL(D.bitMinUnitPriceRule,0) AS bitMinUnitPriceRule,
			ISNULL(@listIds,'') AS vcUnitPriceApprover,
			ISNULL(D.numPODropShipBizDoc,0) AS numPODropShipBizDoc,
			ISNULL(D.numPODropShipBizDocTemplate,0) AS numPODropShipBizDocTemplate,
			ISNULL(D.IsEnableDeferredIncome,0) AS IsEnableDeferredIncome,
			ISNULL(D.bitEnableItemLevelUOM,0) AS bitEnableItemLevelUOM,
			ISNULL(D.bitPurchaseTaxCredit,0) AS bitPurchaseTaxCredit,
			NULL AS bintCreatedDate,
			(CASE WHEN LEN(ISNULL(TempDefaultPage.vcDefaultNavURL,'')) = 0 THEN ISNULL(TempDefaultTab.vcDefaultNavURL,'Items/frmItemList.aspx?Page=All Items&ItemGroup=0') ELSE ISNULL(TempDefaultPage.vcDefaultNavURL,'') END) vcDefaultNavURL,
			ISNULL(D.bitApprovalforTImeExpense,0) AS bitApprovalforTImeExpense,
			ISNULL(D.intTimeExpApprovalProcess,0) AS intTimeExpApprovalProcess,ISNULL(D.bitApprovalforOpportunity,0) AS bitApprovalforOpportunity,
			ISNULL(D.intOpportunityApprovalProcess,0) AS intOpportunityApprovalProcess,
			ISNULL(D.numDefaultSalesShippingDoc,0) AS numDefaultSalesShippingDoc,
			ISNULL(D.numDefaultPurchaseShippingDoc,0) AS numDefaultPurchaseShippingDoc,
			ISNULL((SELECT TOP 1 bitMarginPriceViolated FROM ApprovalProcessItemsClassification  WHERE numDomainID=D.numDomainID AND ISNULL(bitMarginPriceViolated,0) = 1),0) AS bitMarginPriceViolated,
			ISNULL((SELECT TOP 1 intViewAllowed FROM GroupAuthorization WHERE numGroupID=EA.numGroupID AND numModuleID=2 AND numPageID=2),0) AS tintLeadRights,
			ISNULL((SELECT TOP 1 intViewAllowed FROM GroupAuthorization WHERE numGroupID=EA.numGroupID AND numModuleID=3 AND numPageID=2),0) AS tintProspectRights,
			ISNULL((SELECT TOP 1 intViewAllowed FROM GroupAuthorization WHERE numGroupID=EA.numGroupID AND numModuleID=4 AND numPageID=2),0) AS tintAccountRights,
			ISNULL((SELECT TOP 1 intViewAllowed FROM GroupAuthorization WHERE numGroupID=EA.numGroupID AND numModuleID=10 AND numPageID=10),0) AS tintSalesOrderListRights,
			ISNULL((SELECT TOP 1 intViewAllowed FROM GroupAuthorization WHERE numGroupID=EA.numGroupID AND numModuleID=10 AND numPageID=11),0) AS tintPurchaseOrderListRights,
			ISNULL((SELECT TOP 1 intViewAllowed FROM GroupAuthorization WHERE numGroupID=EA.numGroupID AND numModuleID=10 AND numPageID=2),0) AS tintSalesOpportunityListRights,
			ISNULL((SELECT TOP 1 intViewAllowed FROM GroupAuthorization WHERE numGroupID=EA.numGroupID AND numModuleID=10 AND numPageID=8),0) AS tintPurchaseOpportunityListRights,
			ISNULL((SELECT TOP 1 intViewAllowed FROM GroupAuthorization WHERE numGroupID=EA.numGroupID AND numModuleID=11 AND numPageID=2),0) AS tintContactListRights,
			ISNULL((SELECT TOP 1 intViewAllowed FROM GroupAuthorization WHERE numGroupID=EA.numGroupID AND numModuleID=7 AND numPageID=2),0) AS tintCaseListRights,
			ISNULL((SELECT TOP 1 intViewAllowed FROM GroupAuthorization WHERE numGroupID=EA.numGroupID AND numModuleID=12 AND numPageID=2),0) AS tintProjectListRights,
			ISNULL((SELECT TOP 1 numCriteria FROM TicklerListFilterConfiguration WHERE CHARINDEX(CONCAT(',',A.numContactId,','),CONCAT(',',vcSelectedEmployee,',')) > 0),0) AS tintActionItemRights,
			ISNULL(D.numAuthorizePercentage,0) AS numAuthorizePercentage,
			ISNULL(D.bitAllowDuplicateLineItems,0) bitAllowDuplicateLineItems,
			ISNULL(D.bitLogoAppliedToBizTheme,0) bitLogoAppliedToBizTheme,
			ISNULL(D.vcLogoForBizTheme,'') vcLogoForBizTheme,
			ISNULL(D.bitLogoAppliedToLoginBizTheme,0) bitLogoAppliedToLoginBizTheme,
			ISNULL(D.vcLogoForLoginBizTheme,'') vcLogoForLoginBizTheme,
			ISNULL(D.vcThemeClass,'1473b4') vcThemeClass,
			ISNULL(D.vcLoginURL,'') AS vcLoginURL,
			ISNULL(D.bitDisplayCustomField,0) AS bitDisplayCustomField,
			ISNULL(D.bitFollowupAnytime,0) AS bitFollowupAnytime,
			ISNULL(D.bitpartycalendarTitle,0) AS bitpartycalendarTitle,
			ISNULL(D.bitpartycalendarLocation,0) AS bitpartycalendarLocation,
			ISNULL(D.bitpartycalendarDescription,0) AS bitpartycalendarDescription,
			ISNULL(D.bitREQPOApproval,0) AS bitREQPOApproval,
			ISNULL(D.bitARInvoiceDue,0) AS bitARInvoiceDue,
			ISNULL(D.bitAPBillsDue,0) AS bitAPBillsDue,
			ISNULL(D.bitItemsToPickPackShip,0) AS bitItemsToPickPackShip,
			ISNULL(D.bitItemsToInvoice,0) AS bitItemsToInvoice,
			ISNULL(D.bitSalesOrderToClose,0) AS bitSalesOrderToClose,
			ISNULL(D.bitItemsToPutAway,0) AS bitItemsToPutAway,
			ISNULL(D.bitItemsToBill,0) AS bitItemsToBill,
			ISNULL(D.bitPosToClose,0) AS bitPosToClose,
			ISNULL(D.bitPOToClose,0) AS bitPOToClose,
			ISNULL(D.bitBOMSToPick,0) AS bitBOMSToPick,
			ISNULL(D.vchREQPOApprovalEmp,'') AS vchREQPOApprovalEmp,
			ISNULL(D.vchARInvoiceDue,'') AS vchARInvoiceDue,
			ISNULL(D.vchAPBillsDue,'') AS vchAPBillsDue,
			ISNULL(D.vchItemsToPickPackShip,'') AS vchItemsToPickPackShip,
			ISNULL(D.vchItemsToInvoice,'') AS vchItemsToInvoice,
			ISNULL(stuff((
			select ',' + CAST(UPC.numUserID AS VARCHAR(10))
			from UnitPriceApprover UPC
			where UPC.numDomainID = D.numDomainId
			order by UPC.numUserID
			for xml path('')),1,1,''),'') AS vchPriceMarginApproval,
			ISNULL(D.vchSalesOrderToClose,'') AS vchSalesOrderToClose,
			ISNULL(D.vchItemsToPutAway,'') AS vchItemsToPutAway,
			ISNULL(D.vchItemsToBill,'') AS vchItemsToBill,
			ISNULL(D.vchPosToClose,'') AS vchPosToClose,
			ISNULL(D.vchPOToClose,'') AS vchPOToClose,
			ISNULL(D.vchBOMSToPick,'') AS vchBOMSToPick,
			ISNULL(D.decReqPOMinValue,0) AS decReqPOMinValue,
			0 tintPayrollType,
			(CASE WHEN EXISTS (SELECT numCategoryHDRID FROM TimeAndExpense WHERE numUserCntID=A.numContactId AND numCategory=1 AND numType=7 AND dtToDate IS NULL) THEN 1 ELSE 0 END) bitUserClockedIn,
			ISNULL(D.bitUseOnlyActionItems,0) AS bitUseOnlyActionItems,
			ISNULL(D.vcElectiveItemFields,'') AS vcElectiveItemFields,
			ISNULL(D.bitDisplayContractElement,0) AS bitDisplayContractElement,
			ISNULL(D.vcEmployeeForContractTimeElement,'') AS vcEmployeeForContractTimeElement,
			ISNULL(D.bitEnableSmartyStreets,0) bitEnableSmartyStreets,
			ISNULL(D.vcSmartyStreetsAPIKeys,'') vcSmartyStreetsAPIKeys
		FROM 
			ExtranetAccountsDtl EAD
		INNER JOIN
			ExtarnetAccounts EA
		ON
			EAD.numExtranetID = EA.numExtranetID                                                
		INNER JOIN 
			Domain D                              
		ON 
			D.numDomainID=EAD.numDomainID
		INNER JOIN 
			AdditionalContactsInformation A                              
		ON 
			A.numContactID=EAD.numContactID
		INNER JOIN 
			Subscribers S            
		ON 
			S.numTargetDomainID=D.numDomainID 
		LEFT JOIN 
			DivisionMaster Div                                            
		ON 
			EA.numDivisionID=Div.numDivisionID                               
		LEFT JOIN 
			CompanyInfo C                              
		ON 
			C.numCompanyID=Div.numCompanyID    
		OUTER APPLY
		(
			SELECT  
				TOP 1 REPLACE(SCB.Link,'../','') vcDefaultNavURL
			FROM    
				TabMaster T
			JOIN 
				GroupTabDetails G ON G.numTabId = T.numTabId
			LEFT JOIN
				ShortCutGrpConf SCUC ON G.numTabId=SCUC.numTabId AND SCUC.numDomainId=EAD.numDomainID AND SCUC.numGroupId=EA.numGroupID AND bitDefaultTab=1
			LEFT JOIN
				ShortCutBar SCB
			ON
				SCB.Id=SCUC.numLinkId
			WHERE   
				(T.numDomainID = EAD.numDomainID OR bitFixed = 1)
				AND G.numGroupID = EA.numGroupID
				AND ISNULL(G.[tintType], 0) <> 1
				AND tintTabType =1
				AND T.numTabID NOT IN (2,68)
			ORDER BY 
				SCUC.bitInitialPage DESC
		)  TempDefaultPage 
		 OUTER APPLY
		 (
			SELECT  
				TOP 1 REPLACE(T.vcURL,'../','') vcDefaultNavURL
			FROM    
				TabMaster T
			JOIN 
				GroupTabDetails G 
			ON 
				G.numTabId = T.numTabId
			WHERE   
				(T.numDomainID = EAD.numDomainID OR bitFixed = 1)
				AND G.numGroupID = EA.numGroupID
				AND ISNULL(G.[tintType], 0) <> 1
				AND tintTabType =1
				AND T.numTabID NOT IN (2,68)
				AND ISNULL(G.bitInitialTab,0) = 1 
				AND ISNULL(bitallowed,0)=1
			ORDER BY 
				G.bitInitialTab DESC
		 ) TEMPDefaultTab                   
		WHERE 
			(EAD.vcUserName=@UserName OR A.vcemail=@UserName) 
			AND (vcPassword=@vcPassword) 
			AND bitPartnerAccess=1 
			AND GETUTCDATE() BETWEEN dtSubStartDate and dtSubEndDate
	END
	ELSE
	BEGIN
	SELECT TOP 1 
		U.numUserID
		,numUserDetailId
		,D.numCost
		,isnull(vcEmailID,'') vcEmailID
		,ISNULL(vcEmailAlias,'') vcEmailAlias
		,ISNULL(vcEmailAliasPassword,'') vcEmailAliasPassword
		,isnull(numGroupID,0) numGroupID
		,bitActivateFlag
		,isnull(vcMailNickName,'') vcMailNickName
		,isnull(txtSignature,'') txtSignature
		,isnull(U.numDomainID,0) numDomainID
		,isnull(Div.numDivisionID,0) numDivisionID
		,isnull(vcCompanyName,'') vcCompanyName
		,isnull(E.bitExchangeIntegration,0) bitExchangeIntegration
		,isnull(E.bitAccessExchange,0) bitAccessExchange
		,isnull(E.vcExchPath,'') vcExchPath
		,isnull(E.vcExchDomain,'') vcExchDomain
		,isnull(D.vcExchUserName,'') vcExchUserName
		,isnull(vcFirstname,'') + ' ' + isnull(vcLastName,'') as ContactName
		,Case when E.bitAccessExchange=0 then  E.vcExchPassword when E.bitAccessExchange=1 then D.vcExchPassword end as vcExchPassword
		,tintCustomPagingRows
		,vcDateFormat
		,numDefCountry
		,tintComposeWindow
		,dateadd(day,-sintStartDate,getutcdate()) as StartDate
		,dateadd(day,sintNoofDaysInterval,dateadd(day,-sintStartDate,getutcdate())) as EndDate
		,tintAssignToCriteria
		,bitIntmedPage
		,tintFiscalStartMonth
		,numAdminID
		,isnull(A.numTeam,0) numTeam
		,isnull(D.vcCurrency,'') as vcCurrency
		,isnull(D.numCurrencyID,0) as numCurrencyID, 
		isnull(D.bitMultiCurrency,0) as bitMultiCurrency,              
		bitTrial,isnull(tintChrForComSearch,0) as tintChrForComSearch,  
		isnull(tintChrForItemSearch,0) as tintChrForItemSearch,              
		case when bitSMTPServer= 1 then vcSMTPServer else '' end as vcSMTPServer  ,          
		case when bitSMTPServer= 1 then  isnull(numSMTPPort,0)  else 0 end as numSMTPPort      
		,isnull(bitSMTPAuth,0) bitSMTPAuth    
		,isnull([vcSmtpPassword],'') vcSmtpPassword    
		,isnull(bitSMTPSSL,0) bitSMTPSSL   ,isnull(bitSMTPServer,0) bitSMTPServer,       
		isnull(IM.bitImap,0) bitImapIntegration, isnull(charUnitSystem,'E')   UnitSystem, datediff(day,getutcdate(),dtSubEndDate) as NoOfDaysLeft,
		ISNULL(bitCreateInvoice,0) bitCreateInvoice,
		ISNULL([numDefaultSalesBizDocId],0) numDefaultSalesBizDocId,
		ISNULL([numDefaultPurchaseBizDocId],0) numDefaultPurchaseBizDocId,
		vcDomainName,
		S.numSubscriberID,
		D.[tintBaseTax],
		u.ProfilePic,
		ISNULL(D.[numShipCompany],0) numShipCompany,
		ISNULL(D.[bitEmbeddedCost],0) bitEmbeddedCost,
		(Select isnull(numAuthoritativeSales,0) From AuthoritativeBizDocs Where numDomainId=D.numDomainID) numAuthoritativeSales,
		(Select isnull(numAuthoritativePurchase,0) From AuthoritativeBizDocs Where numDomainId=D.numDomainID) numAuthoritativePurchase,
		ISNULL(D.[numDefaultSalesOppBizDocId],0) numDefaultSalesOppBizDocId,
		ISNULL(tintDecimalPoints,2) tintDecimalPoints,
		ISNULL(D.tintBillToForPO,0) tintBillToForPO, 
		ISNULL(D.tintShipToForPO,0) tintShipToForPO,
		ISNULL(D.tintOrganizationSearchCriteria,0) tintOrganizationSearchCriteria,
		ISNULL(D.tintLogin,0) tintLogin,
			ISNULL(S.intFullUserConcurrency,0) intFullUserConcurrency,
		ISNULL(bitDocumentRepositary,0) bitDocumentRepositary,
		ISNULL(D.vcPortalName,'') vcPortalName,
		(SELECT COUNT(*) FROM dbo.Subscribers WHERE getutcdate() between dtSubStartDate and dtSubEndDate and bitActive=1),
		ISNULL(D.bitGtoBContact,0) bitGtoBContact,
		ISNULL(D.bitBtoGContact,0) bitBtoGContact,
		ISNULL(D.bitGtoBCalendar,0) bitGtoBCalendar,
		ISNULL(D.bitBtoGCalendar,0) bitBtoGCalendar,
		ISNULL(bitExpenseNonInventoryItem,0) bitExpenseNonInventoryItem,
		ISNULL(D.bitInlineEdit,0) bitInlineEdit,
		ISNULL(U.numDefaultClass,0) numDefaultClass,
		ISNULL(D.bitRemoveVendorPOValidation,0) bitRemoveVendorPOValidation,
		ISNULL(D.bitTrakDirtyForm,1) bitTrakDirtyForm,
		isnull(D.tintBaseTaxOnArea,0) tintBaseTaxOnArea,
		ISNULL(D.bitAmountPastDue,0) bitAmountPastDue,
		CONVERT(DECIMAL(18,2),ISNULL(D.monAmountPastDue,0)) monAmountPastDue,
		ISNULL(D.bitSearchOrderCustomerHistory,0) bitSearchOrderCustomerHistory,
		isnull(U.tintTabEvent,0) as tintTabEvent,
		isnull(D.bitRentalItem,0) as bitRentalItem,
		isnull(D.numRentalItemClass,0) as numRentalItemClass,
		isnull(D.numRentalHourlyUOM,0) as numRentalHourlyUOM,
		isnull(D.numRentalDailyUOM,0) as numRentalDailyUOM,
		isnull(D.tintRentalPriceBasedOn,0) as tintRentalPriceBasedOn,
		isnull(D.tintCalendarTimeFormat,24) as tintCalendarTimeFormat,
		isnull(D.tintDefaultClassType,0) as tintDefaultClassType,
		ISNULL(U.numDefaultWarehouse,0) numDefaultWarehouse,
		ISNULL(D.tintPriceBookDiscount,0) tintPriceBookDiscount,
		ISNULL(D.bitAutoSerialNoAssign,0) bitAutoSerialNoAssign,
		ISNULL(IsEnableProjectTracking,0) AS [IsEnableProjectTracking],
		ISNULL(IsEnableCampaignTracking,0) AS [IsEnableCampaignTracking],
		ISNULL(IsEnableResourceScheduling,0) AS [IsEnableResourceScheduling],
		ISNULL(D.numSOBizDocStatus,0) AS numSOBizDocStatus,
		ISNULL(D.bitAutolinkUnappliedPayment,0) AS bitAutolinkUnappliedPayment,
		ISNULL(numShippingServiceItemID,0) AS [numShippingServiceItemID],
		ISNULL(D.numDiscountServiceItemID,0) AS numDiscountServiceItemID,
		ISNULL(D.tintCommissionType,1) AS tintCommissionType,
		ISNULL(D.bitDiscountOnUnitPrice,0) AS bitDiscountOnUnitPrice,
		ISNULL(D.intShippingImageWidth,0) AS intShippingImageWidth ,
		ISNULL(D.intShippingImageHeight,0) AS intShippingImageHeight,
		ISNULL(D.numTotalInsuredValue,0) AS numTotalInsuredValue,
		ISNULL(D.numTotalCustomsValue,0) AS numTotalCustomsValue,
		ISNULL(D.bitUseBizdocAmount,1) AS [bitUseBizdocAmount],
		ISNULL(D.bitDefaultRateType,1) AS [bitDefaultRateType],
		ISNULL(D.bitIncludeTaxAndShippingInCommission,1) AS [bitIncludeTaxAndShippingInCommission],
		ISNULL(D.[bitLandedCost],0) AS bitLandedCost,
		ISNULL(D.vcLanedCostDefault,'') AS vcLanedCostDefault,
		ISNULL(D.bitMinUnitPriceRule,0) AS bitMinUnitPriceRule,
		ISNULL(@listIds,'') AS vcUnitPriceApprover,
		ISNULL(D.numPODropShipBizDoc,0) AS numPODropShipBizDoc,
		ISNULL(D.numPODropShipBizDocTemplate,0) AS numPODropShipBizDocTemplate,
		ISNULL(D.IsEnableDeferredIncome,0) AS IsEnableDeferredIncome,
		ISNULL(D.bitEnableItemLevelUOM,0) AS bitEnableItemLevelUOM,
		ISNULL(D.bitPurchaseTaxCredit,0) AS bitPurchaseTaxCredit,
		ISNULL(U.bintCreatedDate,0) AS bintCreatedDate,
		(CASE WHEN LEN(ISNULL(TempDefaultPage.vcDefaultNavURL,'')) = 0 THEN ISNULL(TempDefaultTab.vcDefaultNavURL,'Items/frmItemList.aspx?Page=All Items&ItemGroup=0') ELSE ISNULL(TempDefaultPage.vcDefaultNavURL,'') END) vcDefaultNavURL,
		ISNULL(D.bitApprovalforTImeExpense,0) AS bitApprovalforTImeExpense,
		ISNULL(D.intTimeExpApprovalProcess,0) AS intTimeExpApprovalProcess,ISNULL(D.bitApprovalforOpportunity,0) AS bitApprovalforOpportunity,
		ISNULL(D.intOpportunityApprovalProcess,0) AS intOpportunityApprovalProcess,
		ISNULL(D.numDefaultSalesShippingDoc,0) AS numDefaultSalesShippingDoc,
		ISNULL(D.numDefaultPurchaseShippingDoc,0) AS numDefaultPurchaseShippingDoc,
		ISNULL((SELECT TOP 1 bitMarginPriceViolated FROM ApprovalProcessItemsClassification  WHERE numDomainID=D.numDomainID AND ISNULL(bitMarginPriceViolated,0) = 1),0) AS bitMarginPriceViolated,
		ISNULL((SELECT TOP 1 intViewAllowed FROM GroupAuthorization WHERE numGroupID=U.numGroupID AND numModuleID=2 AND numPageID=2),0) AS tintLeadRights,
		ISNULL((SELECT TOP 1 intViewAllowed FROM GroupAuthorization WHERE numGroupID=U.numGroupID AND numModuleID=3 AND numPageID=2),0) AS tintProspectRights,
		ISNULL((SELECT TOP 1 intViewAllowed FROM GroupAuthorization WHERE numGroupID=U.numGroupID AND numModuleID=4 AND numPageID=2),0) AS tintAccountRights,
		ISNULL((SELECT TOP 1 intViewAllowed FROM GroupAuthorization WHERE numGroupID=U.numGroupID AND numModuleID=10 AND numPageID=10),0) AS tintSalesOrderListRights,
		ISNULL((SELECT TOP 1 intViewAllowed FROM GroupAuthorization WHERE numGroupID=U.numGroupID AND numModuleID=10 AND numPageID=11),0) AS tintPurchaseOrderListRights,
		ISNULL((SELECT TOP 1 intViewAllowed FROM GroupAuthorization WHERE numGroupID=U.numGroupID AND numModuleID=10 AND numPageID=2),0) AS tintSalesOpportunityListRights,
		ISNULL((SELECT TOP 1 intViewAllowed FROM GroupAuthorization WHERE numGroupID=U.numGroupID AND numModuleID=10 AND numPageID=8),0) AS tintPurchaseOpportunityListRights,
		ISNULL((SELECT TOP 1 intViewAllowed FROM GroupAuthorization WHERE numGroupID=U.numGroupID AND numModuleID=11 AND numPageID=2),0) AS tintContactListRights,
		ISNULL((SELECT TOP 1 intViewAllowed FROM GroupAuthorization WHERE numGroupID=U.numGroupID AND numModuleID=7 AND numPageID=2),0) AS tintCaseListRights,
		ISNULL((SELECT TOP 1 intViewAllowed FROM GroupAuthorization WHERE numGroupID=U.numGroupID AND numModuleID=12 AND numPageID=2),0) AS tintProjectListRights,
		ISNULL((SELECT TOP 1 numCriteria FROM TicklerListFilterConfiguration WHERE CHARINDEX(CONCAT(',',U.numUserDetailID,','),CONCAT(',',vcSelectedEmployee,',')) > 0),0) AS tintActionItemRights,
		ISNULL(D.numAuthorizePercentage,0) AS numAuthorizePercentage,
		ISNULL(D.bitAllowDuplicateLineItems,0) bitAllowDuplicateLineItems,
		ISNULL(D.bitLogoAppliedToBizTheme,0) bitLogoAppliedToBizTheme,
		ISNULL(D.vcLogoForBizTheme,'') vcLogoForBizTheme,
		ISNULL(D.bitLogoAppliedToLoginBizTheme,0) bitLogoAppliedToLoginBizTheme,
		ISNULL(D.vcLogoForLoginBizTheme,'') vcLogoForLoginBizTheme,
		ISNULL(D.vcThemeClass,'1473b4') vcThemeClass,
		ISNULL(D.vcLoginURL,'') AS vcLoginURL,
		ISNULL(D.bitDisplayCustomField,0) AS bitDisplayCustomField,
		ISNULL(D.bitFollowupAnytime,0) AS bitFollowupAnytime,
		ISNULL(D.bitpartycalendarTitle,0) AS bitpartycalendarTitle,
		ISNULL(D.bitpartycalendarLocation,0) AS bitpartycalendarLocation,
		ISNULL(D.bitpartycalendarDescription,0) AS bitpartycalendarDescription,
		ISNULL(D.bitREQPOApproval,0) AS bitREQPOApproval,
		ISNULL(D.bitARInvoiceDue,0) AS bitARInvoiceDue,
		ISNULL(D.bitAPBillsDue,0) AS bitAPBillsDue,
		ISNULL(D.bitItemsToPickPackShip,0) AS bitItemsToPickPackShip,
		ISNULL(D.bitItemsToInvoice,0) AS bitItemsToInvoice,
		ISNULL(D.bitSalesOrderToClose,0) AS bitSalesOrderToClose,
		ISNULL(D.bitItemsToPutAway,0) AS bitItemsToPutAway,
		ISNULL(D.bitItemsToBill,0) AS bitItemsToBill,
		ISNULL(D.bitPosToClose,0) AS bitPosToClose,
		ISNULL(D.bitPOToClose,0) AS bitPOToClose,
		ISNULL(D.bitBOMSToPick,0) AS bitBOMSToPick,
		ISNULL(D.vchREQPOApprovalEmp,'') AS vchREQPOApprovalEmp,
		ISNULL(D.vchARInvoiceDue,'') AS vchARInvoiceDue,
		ISNULL(D.vchAPBillsDue,'') AS vchAPBillsDue,
		ISNULL(D.vchItemsToPickPackShip,'') AS vchItemsToPickPackShip,
		ISNULL(D.vchItemsToInvoice,'') AS vchItemsToInvoice,
		ISNULL(stuff((
        select ',' + CAST(UPC.numUserID AS VARCHAR(10))
        from UnitPriceApprover UPC
        where UPC.numDomainID = D.numDomainId
        order by UPC.numUserID
			for xml path('')),1,1,''),'') AS vchPriceMarginApproval,
		ISNULL(D.vchSalesOrderToClose,'') AS vchSalesOrderToClose,
		ISNULL(D.vchItemsToPutAway,'') AS vchItemsToPutAway,
		ISNULL(D.vchItemsToBill,'') AS vchItemsToBill,
		ISNULL(D.vchPosToClose,'') AS vchPosToClose,
		ISNULL(D.vchPOToClose,'') AS vchPOToClose,
		ISNULL(D.vchBOMSToPick,'') AS vchBOMSToPick,
		ISNULL(D.decReqPOMinValue,0) AS decReqPOMinValue,
		ISNULL(U.tintPayrollType,1) tintPayrollType,
		(CASE WHEN EXISTS (SELECT numCategoryHDRID FROM TimeAndExpense WHERE numUserCntID=U.numUserDetailId AND numCategory=1 AND numType=7 AND dtToDate IS NULL) THEN 1 ELSE 0 END) bitUserClockedIn,
		ISNULL(D.bitUseOnlyActionItems,0) AS bitUseOnlyActionItems,
ISNULL(D.vcElectiveItemFields,'') AS vcElectiveItemFields,
ISNULL(D.bitDisplayContractElement,0) AS bitDisplayContractElement,
ISNULL(D.vcEmployeeForContractTimeElement,'') AS vcEmployeeForContractTimeElement,
ISNULL(D.bitEnableSmartyStreets,0) bitEnableSmartyStreets,
ISNULL(D.vcSmartyStreetsAPIKeys,'') vcSmartyStreetsAPIKeys
	FROM 
		UserMaster U                              
	left join ExchangeUserDetails E                              
	on E.numUserID=U.numUserID                              
	left join ImapUserDetails IM                              
	on IM.numUserCntID=U.numUserDetailID          
	Join Domain D                              
	on D.numDomainID=U.numDomainID                                           
	left join DivisionMaster Div                                            
	on D.numDivisionID=Div.numDivisionID                               
	left join CompanyInfo C                              
	on C.numCompanyID=Div.numCompanyID                               
	left join AdditionalContactsInformation A                              
	on  A.numContactID=U.numUserDetailId                            
	Join  Subscribers S                            
	on S.numTargetDomainID=D.numDomainID    
	 OUTER APPLY
	 (
		SELECT  
			TOP 1 REPLACE(SCB.Link,'../','') vcDefaultNavURL
		FROM    
			TabMaster T
		JOIN 
			GroupTabDetails G ON G.numTabId = T.numTabId
		LEFT JOIN
			ShortCutGrpConf SCUC ON G.numTabId=SCUC.numTabId AND SCUC.numDomainId=U.numDomainID AND SCUC.numGroupId=U.numGroupID AND bitDefaultTab=1
		LEFT JOIN
			ShortCutBar SCB
		ON
			SCB.Id=SCUC.numLinkId
		WHERE   
			(T.numDomainID = U.numDomainID OR bitFixed = 1)
			AND G.numGroupID = U.numGroupID
			AND ISNULL(G.[tintType], 0) <> 1
			AND tintTabType =1
			AND T.numTabID NOT IN (2,68)
		ORDER BY 
			SCUC.bitInitialPage DESC
	 )  TempDefaultPage 
	 OUTER APPLY
	 (
		SELECT  
			TOP 1 REPLACE(T.vcURL,'../','') vcDefaultNavURL
		FROM    
			TabMaster T
		JOIN 
			GroupTabDetails G 
		ON 
			G.numTabId = T.numTabId
		WHERE   
			(T.numDomainID = U.numDomainID OR bitFixed = 1)
			AND G.numGroupID = U.numGroupID
			AND ISNULL(G.[tintType], 0) <> 1
			AND tintTabType =1
			AND T.numTabID NOT IN (2,68)
			AND ISNULL(G.bitInitialTab,0) = 1 
			AND ISNULL(bitallowed,0)=1
		ORDER BY 
			G.bitInitialTab DESC
	 ) TEMPDefaultTab                   
	WHERE 1 = (CASE 
		WHEN @vcLinkedinId='N' THEN (CASE WHEN vcEmailID=@UserName and vcPassword=@vcPassword THEN 1 ELSE 0 END)
		ELSE (CASE WHEN U.vcLinkedinId=@vcLinkedinId THEN 1 ELSE 0 END)
		END) and bitActive IN (1,2) AND getutcdate() between dtSubStartDate and dtSubEndDate
	END
             
	SELECT 
		numTerritoryId 
	FROM
		UserTerritory UT                              
	JOIN 
		UserMaster U                              
	ON 
		numUserDetailId =UT.numUserCntID                                         
	WHERE 
		1 = (CASE 
			WHEN @vcLinkedinId='N' THEN (CASE WHEN vcEmailID=@UserName and vcPassword=@vcPassword THEN 1 ELSE 0 END)
			ELSE (CASE WHEN vcLinkedinId=@vcLinkedinId THEN 1 ELSE 0 END)
			END)

END
GO