/****** Object:  StoredProcedure [dbo].[USP_ProMilestone]    Script Date: 07/26/2008 16:20:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj                      
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_promilestone')
DROP PROCEDURE usp_promilestone
GO
CREATE PROCEDURE [dbo].[USP_ProMilestone] 
( @ProcessID NUMERIC(9) = NULL,
                                           @ContactID NUMERIC(9) = NULL,
                                           @DomianID NUMERIC(9) = NULL,
                                           @numProId NUMERIC(9) = 0 )
AS 
IF @ProcessID != 0 
    BEGIN                        
        SELECT  mst.numStagePercentage,
                dtl.numstagedetailsID,
                dtl.vcstagename,
                0 AS StageID,
                @DomianID AS numDomainId,
                @ContactID AS numCreatedBy,
                GETUTCDATE() AS bintCreatedDate,
                @ContactID AS numModifiedBy,
                GETUTCDATE() AS bintModifiedDate,
                GETUTCDATE() AS bintDueDate,
                GETUTCDATE() AS bintStageComDate,
                '' AS vcComments,
                numAssignTo,
                ISNULL(dbo.fn_GetContactName(numAssignTo), '-') AS vcAssignTo,
                0 AS bitAlert,
                0 AS bitStageCompleted,
                0 AS Op_Flag,
                ISNULL(dbo.fn_GetContactName(@ContactID), '-') AS numModifiedByName,
                0 AS numOppStageID,
                0 AS Depend,
                0 AS numTCategoryHDRID,
                0 AS numECategoryHDRID,
                0 AS Expense,
                '0' AS Time,
                0 AS SubStg,
                0 AS numStage,
                '' AS vcStage,
                ISNULL(vcMileStoneName, '') AS vcMileStoneName,
                ISNULL(tintPercentage, 0) AS tintPercentage,
                ISNULL(bitClose, 0) bitClose,
                0 numCommActId
        FROM    StagePercentageMaster mst
                JOIN StagePercentageDetails dtl ON dtl.numstagepercentageid = mst.numstagepercentageid
        WHERE   dtl.slp_id = @ProcessID
        ORDER BY numStagePercentage,
                numOppstageid                          
    END                        
ELSE 
    BEGIN                        
                        
        SELECT  numStagePercentage,
                numProStageId AS numstagedetailsID,
                vcstageDetail,
                numProStageId AS StageID,
                @DomianID AS numDomainId,
                numCreatedBy,
                bintCreatedDate,
                numModifiedBy,
                bintModifiedDate,
                bintDueDate,
                bintStageComDate,
                vcComments,
                ISNULL(numAssignTo, 0) AS numAssignTo,
                ISNULL(dbo.fn_GetContactName(numAssignTo), '-') AS vcAssignTo,
                bitAlert,
                bitStageCompleted,
                0 AS Op_Flag,
                ISNULL(dbo.fn_GetContactName(numModifiedby), '-') AS numModifiedByName,
                numProStageId,
                dbo.fn_GetDepDtlsbyProStgID(@numProId, numProStageId, 0) AS Depend,
                0 AS numTCategoryHDRID,
                0 AS numECategoryHDRID,
                dbo.fn_GetExpenseDtlsbyProStgID(@numProId, numProStageId, 0) AS Expense,
                dbo.fn_GeTimeDtlsbyProStgID(@numProId, numProStageId, 0) AS [Time],
                dbo.fn_GetSubStgDtlsbyProStgID(@numProId, numProStageId, 0) AS SubStg,
                ISNULL(numStage, 0) AS numStage,
                ( SELECT    vcData
                  FROM      listdetails
                  WHERE     numlistitemid = numStage
                ) AS vcStage,
                ISNULL(vcStagePercentageDtl, '') AS vcStagePercentageDtl,
                ISNULL(numTemplateId, 0) numTemplateId,
                ISNULL(tintPercentage, 0) AS tintPercentage,
                ISNULL(numEvent, 0) numEvent,
                ISNULL(numReminder, 0) numReminder,
                ISNULL(numET, 0) numET,
                ISNULL(numActivity, 0) numActivity,
                ISNULL(numStartDate, 0) numStartDate,
                ISNULL(numStartTime, 0) numStartTime,
                ISNULL(numStartTimePeriod, 15) numStartTimePeriod,
                ISNULL(numEndTime, 0) numEndTime,
                ISNULL(numEndTimePeriod, 15) numEndTimePeriod,
                ISNULL(txtCom, '') txtCom,
                ISNULL(numChgStatus, 0) numChgStatus,
                ISNULL(bitChgStatus, 0) bitChgStatus,
                ISNULL(bitClose, 0) bitClose,
                ISNULL(numType, 0) numType,
                ISNULL(numCommActId, 0) numCommActId
        FROM    ProjectsStageDetails PSD
        WHERE   numProId = @numProId
        ORDER BY numStagePercentage,
                numProStageId                        
                        
                        
    END
GO
