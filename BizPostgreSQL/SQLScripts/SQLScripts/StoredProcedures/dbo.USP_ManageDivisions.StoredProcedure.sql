/****** Object:  StoredProcedure [dbo].[USP_ManageDivisions]    Script Date: 07/26/2008 16:19:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Modified by anoop jayaraj                                  
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_managedivisions')
DROP PROCEDURE usp_managedivisions
GO
CREATE PROCEDURE [dbo].[USP_ManageDivisions]                                                        
 @numDivisionID  numeric=0,                                                         
 @numCompanyID  numeric=0,                                                         
 @vcDivisionName  varchar (100)='',                                                        
 @vcBillStreet  varchar (50)='',                                                        
 @vcBillCity   varchar (50)='',                                                        
 @vcBillState  NUMERIC=0,                                                        
 @vcBillPostCode  varchar (15)='',                                                        
 @vcBillCountry  numeric=0,                                                        
 @bitSameAddr  bit=0,                                                        
 @vcShipStreet  varchar (50)='',                                                        
 @vcShipCity   varchar (50)='',                                                        
 @vcShipState  varchar (50)='',                                                        
 @vcShipPostCode  varchar (15)='',                                                        
 @vcShipCountry  varchar (50)='',                                                        
 @numGrpId   numeric=0,                                                                                          
 @numTerID   numeric=0,                                                        
 @bitPublicFlag  bit=0,                                                        
 @tintCRMType  tinyint=0,                                                        
 @numUserCntID  numeric=0,                                                                                                                                                              
 @numDomainID  numeric=0,                                                        
 @bitLeadBoxFlg  bit=0,  --Added this because when called form leadbox this value will be 1 else 0                                                        
 @numStatusID  numeric=0,                                                      
 @numCampaignID numeric=0,                                          
 @numFollowUpStatus numeric(9)=0,                                                                                  
 @tintBillingTerms as tinyint,                                                        
 @numBillingDays as numeric(18,0),                                                       
 @tintInterestType as tinyint,                                                        
 @fltInterest as float,                                        
 @vcComFax as varchar(50)=0,                          
 @vcComPhone as varchar(50)=0,                
 @numAssignedTo as numeric(9)=0,    
 @bitNoTax as bit,
 @UpdateDefaultTax as bit=1,
@numCompanyDiff as numeric(9)=0,
@vcCompanyDiff as varchar(100)='',
@numCurrencyID	AS NUMERIC(9,0),
@numAccountClass AS NUMERIC(18,0) = 0,
@numBillAddressID AS NUMERIC(18,0) = 0,
@numShipAddressID AS NUMERIC(18,0) = 0,
@vcPartenerSource AS VARCHAR(300)=0,              
@vcPartenerContact AS VARCHAR(300)=0,
@numPartner AS NUMERIC(18,0) = 0,
@vcBuiltWithJson NVARCHAR(MAX) = ''
AS   
BEGIN
	DECLARE @numPartenerSource NUMERIC(18,0)      
	DECLARE @numPartenerContact NUMERIC(18,0)     

	IF ISNULL(@numPartner,0) = 0
	BEGIN
		SET @numPartenerSource=(SELECT TOP 1 numDivisionID FROM DivisionMaster WHERE numDomainId=@numDomainID AND vcPartnerCode=@vcPartenerSource)
	END
	ELSE 
	BEGIN
		SET @numPartenerSource = @numPartner
	END

	SET @numPartenerContact=(SELECT TOP 1 numContactId FROM AdditionalContactsInformation WHERE numDomainId=@numDomainID AND numDivisionId=@numPartenerSource AND vcEmail=@vcPartenerContact)
                                 
	IF ISNULL(@numGrpId,0) = 0 AND ISNULL(@tintCRMType,0) = 0
	BEGIN
 		SET @numGrpId = 5
	END
                                                                                                               
	IF @numDivisionId is null OR @numDivisionId=0                                                 
	BEGIN
		IF @UpdateDefaultTax=1 
			SET @bitNoTax=0
                                                                                  
		INSERT INTO DivisionMaster                                    
		(                      
			numCompanyID,vcDivisionName,numGrpId,numFollowUpStatus,bitPublicFlag,numCreatedBy,bintCreatedDate,numModifiedBy,bintModifiedDate,tintCRMType,numDomainID,                                    
			bitLeadBoxFlg,numTerID,numStatusID,numRecOwner,tintBillingTerms,numBillingDays,tintInterestType,fltInterest,numCampaignID,vcComPhone,vcComFax,bitNoTax,
			numCompanyDiff,vcCompanyDiff,bitActiveInActive,numCurrencyID,numAccountClassID,numPartenerSource,numPartenerContact,vcBuiltWithJson     
		)                                    
		VALUES                      
		(                      
			@numCompanyID,@vcDivisionName,@numGrpId,@numFollowUpStatus,@bitPublicFlag,@numUserCntID,GETUTCDATE(),@numUserCntID,GETUTCDATE(),@tintCRMType,@numDomainID,                       
			@bitLeadBoxFlg,@numTerID,@numStatusID,@numUserCntID,(CASE WHEN ISNULL(@numBillingDays,0) > 0 THEN 1 ELSE 0 END),@numBillingDays,0,0,@numCampaignID,@vcComPhone,            
			@vcComFax,@bitNoTax,@numCompanyDiff,@vcCompanyDiff,1,@numCurrencyID,@numAccountClass,@numPartenerSource,@numPartenerContact,@vcBuiltWithJson                        
		)                                  
	
		SELECT @numDivisionID = SCOPE_IDENTITY()

		IF @numBillAddressID > 0 OR @numShipAddressID > 0
		BEGIN
			IF @numBillAddressID > 0
			BEGIN
				UPDATE AddressDetails SET numRecordID=@numDivisionID WHERE numAddressID=@numBillAddressID
			END

			IF @numShipAddressID > 0
			BEGIN
				UPDATE AddressDetails SET numRecordID=@numDivisionID WHERE numAddressID=@numShipAddressID
			END
		END
		ELSE
		BEGIN
		  --added By Sachin Sadhu||Issued Founded on:1stFeb14||By:JJ-AudioJenex
			IF EXISTS(SELECT 'col1' FROM dbo.AddressDetails WHERE  bitIsPrimary=1 AND numDomainID=@numDomainID AND numRecordID=@numDivisionID )
			BEGIN
				INSERT INTO dbo.AddressDetails 
				(
					vcAddressName,
					vcStreet,
					vcCity,
					vcPostalCode,
					numState,
					numCountry,
					bitIsPrimary,
					tintAddressOf,
					tintAddressType,
					numRecordID,
					numDomainID
				) 
				SELECT 'Address1',@vcBillStreet,@vcBillCity,@vcBillPostCode,@vcBillState,@vcBillCountry,0,2,1,@numDivisionID,@numDomainID
				union
				SELECT 'Address1',@vcShipStreet,@vcShipCity,@vcShipPostCode,@vcShipState,@vcShipCountry,0,2,2,@numDivisionID,@numDomainID
			END
			ELSE
			BEGIN
				INSERT INTO dbo.AddressDetails 
				(
					vcAddressName,
					vcStreet,
					vcCity,
					vcPostalCode,
					numState,
					numCountry,
					bitIsPrimary,
					tintAddressOf,
					tintAddressType,
					numRecordID,
					numDomainID
				) 
				SELECT 'Address1',@vcBillStreet,@vcBillCity,@vcBillPostCode,@vcBillState,@vcBillCountry,1,2,1,@numDivisionID,@numDomainID
				union
				SELECT 'Address1',@vcShipStreet,@vcShipCity,@vcShipPostCode,@vcShipState,@vcShipCountry,1,2,2,@numDivisionID,@numDomainID
			END
		END
       --End of Sachin Script                   

		IF @UpdateDefaultTax=1 
		BEGIN
			INSERT INTO DivisionTaxTypes
			SELECT 
				@numDivisionID,numTaxItemID,1 
			FROM 
				TaxItems
			WHERE 
				numDomainID=@numDomainID
			UNION
			SELECT 
				@numDivisionID,0,1 
		END                                                
		                  
		DECLARE @numGroupID NUMERIC
		SELECT TOP 1 
			@numGroupID=numGroupID 
		FROM 
			dbo.AuthenticationGroupMaster 
		WHERE 
			numDomainID=@numDomainID 
			AND tintGroupType=2
		
		IF @numGroupID IS NULL 
			SET @numGroupID = 0 
		
		INSERT INTO ExtarnetAccounts 
		(numCompanyID,numDivisionID,numGroupID,numDomainID)                                                
		VALUES
		(@numCompanyID,@numDivisionID,@numGroupID,@numDomainID)                                                                                                                 
	END                                                        
	ELSE if @numDivisionId>0                                                      
	BEGIN
		DECLARE @numFollow AS VARCHAR(10)                                        
		DECLARE @binAdded AS VARCHAR(20)                                        
		DECLARE @PreFollow AS VARCHAR(10)                                        
		DECLARE @RowCount AS VARCHAR(2)                                        
		SET @PreFollow=(SELECT COUNT(*) numFollowUpStatus FROM FollowUpHistory WHERE numDivisionID= @numDivisionID)                                        
    
		SET @RowCount=@@rowcount                                        
    
		SELECT 
			@numFollow=numFollowUpStatus,
			@binAdded=bintModifiedDate 
		FROM 
			divisionmaster
		WHERE 
			numDivisionID=@numDivisionID                                         
    
		IF @numFollow <>'0' AND @numFollow <> @numFollowUpStatus                                        
		BEGIN
			SELECT TOP 1 
				numFollowUpStatus 
			FROM 
				FollowUpHistory 
			WHERE 
				numDivisionID= @numDivisionID 
			ORDER BY 
				numFollowUpStatusID DESC                                        
                                         
			IF @PreFollow<>0                                        
			BEGIN
				IF @numFollow <> (select top 1  numFollowUpStatus from FollowUpHistory where numDivisionID= @numDivisionID order by numFollowUpStatusID desc)                                        
				BEGIN
					INSERT INTO FollowUpHistory
					(numFollowUpstatus,numDivisionID,bintAddedDate)                                        
					VALUES
					(@numFollow,@numDivisionID,@binAdded)                                        
				END                                        
			END                              
			ELSE
			BEGIN
				INSERT INTO FollowUpHistory
				(numFollowUpstatus,numDivisionID,bintAddedDate)                                        
				VALUES
				(@numFollow,@numDivisionID,@binAdded)                                   
			END                                                                 
		END                                                                                            
                                                        
		UPDATE 
			DivisionMaster                       
		SET                        
			numCompanyID = @numCompanyID ,                     
			vcDivisionName = @vcDivisionName,                                                        
			numGrpId = @numGrpId,                                                                       
			numFollowUpStatus =@numFollowUpStatus,                                                        
			numTerID = @numTerID,                                                   
			bitPublicFlag =@bitPublicFlag,                                                        
			numModifiedBy = @numUserCntID,                                                  
			bintModifiedDate = getutcdate(),                                                     
			tintCRMType = @tintCRMType,                                                      
			numStatusID = @numStatusID,                                                 
			tintBillingTerms = (CASE WHEN ISNULL(@numBillingDays,0) > 0 THEN 1 ELSE 0 END),                                                      
			numBillingDays = @numBillingDays,                                                     
			tintInterestType = @tintInterestType,                                                       
			fltInterest =@fltInterest,                          
			vcComPhone=@vcComPhone,                          
			vcComFax= @vcComFax,                      
			numCampaignID=@numCampaignID,    
			bitNoTax=@bitNoTax
			,numCompanyDiff=@numCompanyDiff
			,vcCompanyDiff=@vcCompanyDiff
			,numCurrencyID = @numCurrencyID
			,numPartenerSource=@numPartenerSource
			,numPartenerContact=@numPartenerContact                                                         
		WHERE 
			numDivisionID = @numDivisionID       

		UPDATE 
			dbo.AddressDetails 
		SET
			vcStreet=@vcBillStreet,                                    
			vcCity =@vcBillCity,                                    
			vcPostalCode=@vcBillPostCode,                                     
			numState=@vcBillState,                                    
			numCountry=@vcBillCountry
		WHERE 
			numDomainID=@numDomainID 
			AND numRecordID=@numDivisionID 
			AND bitIsPrimary=1 
			AND tintAddressOf=2 
			AND tintAddressType=1         
	   
		UPDATE 
			dbo.AddressDetails 
		SET
			vcStreet=@vcShipStreet,                                    
			vcCity =@vcShipCity,                                    
			vcPostalCode=@vcShipPostCode,                                     
			numState=@vcShipState,                                    
			numCountry=@vcShipCountry
		WHERE 
			numDomainID=@numDomainID 
			AND numRecordID=@numDivisionID 
			AND bitIsPrimary=1 
			AND tintAddressOf=2 
			AND tintAddressType=2
	END

    ---Updating if organization is assigned to someone                
	DECLARE @tempAssignedTo AS NUMERIC(9)              
              
	SELECT 
		@tempAssignedTo=isnull(numAssignedTo,0) 
	FROM 
		DivisionMaster 
	WHERE 
		numDivisionID = @numDivisionID                
        
	IF (@tempAssignedTo<>@numAssignedTo and  @numAssignedTo<>'0')              
	BEGIN                
		UPDATE 
			DivisionMaster 
		SET 
			numAssignedTo=@numAssignedTo
			,numAssignedBy=@numUserCntID 
		WHERE 
			numDivisionID = @numDivisionID                
	END               
  
	SELECT @numDivisionID
 END