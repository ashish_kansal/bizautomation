/****** Object:  StoredProcedure [dbo].[USP_TransferOwnerAssigneAdv]    Script Date: 07/26/2008 16:21:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_transferownerassigneadv')
DROP PROCEDURE usp_transferownerassigneadv
GO
CREATE PROCEDURE [dbo].[USP_TransferOwnerAssigneAdv]

@numUserCntID as numeric(9),
@xmlStr as varchar(500),
@byteMode as TinyInt,
@numDomainID as numeric(9)

as 

if @byteMode = 1
begin
		update DivisionMaster set numRecOwner =@numUserCntID where numDivisionId in (

		select distinct(numdivisionid) from additionalcontactsinformation where numDomainId = @numDomainID and 
		numcontactId in 
		(select Items from dbo.split(@xmlStr,',')
		)
		)

end
else
begin
		update DivisionMaster set numAssignedTo =@numUserCntID where numDivisionId in (

		select distinct(numdivisionid) from additionalcontactsinformation where numDomainId = @numDomainID and 
		numcontactId in 
		(select Items from dbo.split(@xmlStr,',')
		)
		)

end
GO
