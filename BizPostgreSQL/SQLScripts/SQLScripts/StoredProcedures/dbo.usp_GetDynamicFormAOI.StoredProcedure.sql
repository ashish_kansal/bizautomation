/****** Object:  StoredProcedure [dbo].[usp_GetDynamicFormAOI]    Script Date: 07/26/2008 16:17:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created By: Debasish Tapan Nag                                
--Created By: 07/11/2005                                
--Purpose:    Get Dynamic fields for the bizform wizard                                
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getdynamicformaoi')
DROP PROCEDURE usp_getdynamicformaoi
GO
CREATE PROCEDURE [dbo].[usp_GetDynamicFormAOI]                
 @numDomainID numeric(9)=0,                                     
 @numFormId Int ,        
 @numGroupID as numeric(9)=0        
as           
            
        
select numListItemID as numAOIID,vcData as vcAOIName from ListDetails where numDomainID=@numDomainID and numListID=74         
        
select distinct(numListItemID) as numAOIID,vcData as vcAOIName from DynamicFormAOIConf D        
join  ListDetails L on L.numListItemID=D.numAOIID           
where L.numDomainID= @numDomainID  and L.numListID=74    
and numFormID= @numFormId        
and numGroupID= @numGroupID
GO
