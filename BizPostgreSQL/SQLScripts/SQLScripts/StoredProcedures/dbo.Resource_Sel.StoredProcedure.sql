/****** Object:  StoredProcedure [dbo].[Resource_Sel]    Script Date: 07/26/2008 16:14:34 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
/*
**  Selects all Resources.
**
**  This stored procedure can be used to populate the Resources collection with all Resources.
**  It takes no parameters.
*/
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='resource_sel')
DROP PROCEDURE resource_sel
GO
CREATE PROCEDURE [dbo].[Resource_Sel] 
AS
BEGIN

	SELECT
		[Resource].[ResourceID], 
		[Resource].[ResourceName], 
		[Resource].[ResourceDescription],
		[Resource].[EmailAddress],
		ISNULL([ResourcePreference].[EnableEmailReminders],0) AS EnableEmailReminders,
		[Resource].[_ts]
	FROM 
		[Resource] LEFT JOIN [ResourcePreference] ON [Resource].[ResourceID] = [ResourcePreference].[ResourceID];

END
GO
