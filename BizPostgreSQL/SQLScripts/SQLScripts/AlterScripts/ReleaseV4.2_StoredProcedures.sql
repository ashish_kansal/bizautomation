
/******************************************************************
Project: Release 4.2 Date: 03.FEB.2015
Comments: STORED PROCEDURES
*******************************************************************/



-------------- FUNCTIONS


GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='CheckAssemblyBuildStatus')
DROP FUNCTION CheckAssemblyBuildStatus
GO
CREATE FUNCTION [dbo].[CheckAssemblyBuildStatus] (@numItemKitID NUMERIC(18,0),@numQty NUMERIC(18,0))
RETURNS BIT
AS
BEGIN
    DECLARE @IsReadyToBuild BIT = 1

	DECLARE @Temp TABLE
	(
		numItemCode NUMERIC(18,0),
		numQtyRequired INT,
		numOnAllocation NUMERIC(18,0),
		bitReadyToBuild BIT
	)

	INSERT INTO 
		@Temp
	SELECT
		numChildItemID,
		CAST(ISNULL(numQtyItemsReq * @numQty,0) AS INT),
		WareHouseItems.numAllocation,
		CASE WHEN  ISNULL(numQtyItemsReq * @numQty,0) > WareHouseItems.numAllocation THEN 0 ELSE 1 END AS bitReadyToBuild 
	FROM
		ItemDetails
	INNER JOIN
		WareHouseItems
	ON
		ItemDetails.numWareHouseItemId = WareHouseItems.numWareHouseItemID
	WHERE
		numItemKitID = @numItemKitID


	IF (SELECT COUNT(*) FROM @Temp WHERE  bitReadyToBuild = 0) > 0
	BEGIN
		SET @IsReadyToBuild = 0
	END

    RETURN @IsReadyToBuild
END
GO

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='fn_GetAssemblyPossibleWOQty')
DROP FUNCTION fn_GetAssemblyPossibleWOQty
GO
CREATE FUNCTION [dbo].[fn_GetAssemblyPossibleWOQty]
(
	@numItemCode as numeric(18,0)
)     
RETURNS INT 
AS      
BEGIN      
	DECLARE @numCount AS INT = 0;

	WITH CTE (numParentItemCode,numItemCode,vcItemName,numWarehouseItemID,numOnHand,numRequired) AS
	(
		SELECT 
			CAST(0 AS NUMERIC(18,0)),
			ItemDetails.numChildItemID,
			Item.vcItemName,
			WareHouseItems.numWareHouseItemID,
			WareHouseItems.numOnHand,
			ItemDetails.numQtyItemsReq
		FROM 
			ItemDetails 
		INNER JOIN
			Item
		ON
			ItemDetails.numChildItemID = Item.numItemCode
		INNER JOIN 
			WareHouseItems 
		ON 
			ItemDetails.numWareHouseItemId = WareHouseItems.numWareHouseItemID 
		WHERE 
			ItemDetails.numItemKitID = @numItemCode
		UNION ALL
		SELECT 
			CTE.numItemCode,
			ItemDetails.numChildItemID,
			Item.vcItemName,
			WareHouseItems.numWareHouseItemID,
			WareHouseItems.numOnHand,
			CAST((ItemDetails.numQtyItemsReq * CTE.numRequired) AS NUMERIC(18,0))
		FROM 
			CTE
		INNER JOIN
			ItemDetails 
		ON
			CTE.numItemCode = ItemDetails.numItemKitID
		INNER JOIN
			Item
		ON
			ItemDetails.numChildItemID = Item.numItemCode
		INNER JOIN 
			WareHouseItems 
		ON 
			ItemDetails.numWareHouseItemId = WareHouseItems.numWareHouseItemID 
	)

	SELECT @numCount = MIN(CAST((numOnHand/numRequired) AS INT)) FROM CTE WHERE numItemCode NOT IN (SELECT numParentItemCode FROM CTE)

	RETURN @numCount
END

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='fn_GetItemChildMembershipCount')
DROP FUNCTION fn_GetItemChildMembershipCount
GO
CREATE FUNCTION [dbo].[fn_GetItemChildMembershipCount]
(
	@numDomainID as numeric(18,0),
	@numItemCode as numeric(18,0)
)     
RETURNS INT 
AS      
BEGIN      
	DECLARE @numCount AS INT = 0

	SELECT
		@numCount = COUNT(vcItemName)
	FROM
	(
		SELECT 
			Item.vcItemName,
			CASE 
				WHEN ISNULL(Item.bitAssembly,0)=1 THEN 'Assembly' 
				WHEN ISNULL(Item.bitKitParent,0)=1 THEN 'Kit'
			END AS ParentItemType,
			CAST(ItemDetails.numQtyItemsReq AS VARCHAR(18)) AS numQtyItemsReq,
			Warehouses.vcWareHouse
		FROM 
			ItemDetails 
		INNER JOIN
			Item
		ON
			ItemDetails.numItemKitID = Item.numItemCode
		INNER JOIN
			WareHouseItems
		ON
			ItemDetails.numWareHouseItemId = WareHouseItems.numWareHouseItemID
		INNER JOIN
			Warehouses
		ON
			WareHouseItems.numWareHouseID = Warehouses.numWareHouseID
		WHERE 
			numChildItemID = @numItemCode
		UNION ALL
		SELECT
			ItemGroups.vcItemGroup AS vcItemName,
			'Item Group' AS ParentItemType,
			'' AS numQtyItemsReq,
			'' AS vcWareHouse
		FROM
			Item
		INNER JOIN
			ItemGroups
		ON
			Item.numItemGroup = ItemGroups.numItemGroupID
		WHERE
			Item.numDomainID = @numDomainID 
			AND numItemCode = @numItemCode
		UNION ALL
		SELECT 
			Item.vcItemName,
			'Related Items' AS ParentItemType,
			'' AS numQtyItemsReq,
			'' AS vcWareHouse
		FROM 
			SimilarItems 
		INNER JOIN
			Item
		ON
			SimilarItems.numParentItemCode = Item.numItemCode
		WHERE 
			SimilarItems.numDomainID = @numDomainID
			AND SimilarItems.numItemCode = @numItemCode
	) AS Temp

	RETURN @numCount
END

GO
/****** Object:  UserDefinedFunction [dbo].[fn_GetNewvcEmailString]    Script Date: 11/02/2011 12:21:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
------Function For Returning new string ----------------------
------Created By : Pinkal Patel Date : 06-Oct-2011
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='fn_GetNewvcEmailString')
DROP FUNCTION fn_GetNewvcEmailString
GO
CREATE FUNCTION [dbo].[fn_GetNewvcEmailString](
@vcEmailId VARCHAR(2000),
@numDomianID NUMERIC(18,0) = NULL
) RETURNS VARCHAR(2000)
As
BEGIN
DECLARE @vcNewEmailId AS VARCHAR(2000)
DECLARE @Email AS VARCHAR(200)
DECLARE @EmailName AS VARCHAR(200)
DECLARE @EmailAdd AS VARCHAR(200)
DECLARE @StartPos AS INTEGER
DECLARE @EndPos AS INTEGER
DECLARE @numEmailid AS NUMERIC(9)
---DECLARE @numDomainId AS NUMERIC(18, 0)

SET @vcNewEmailId =''
	IF @vcEmailId <> '' 
    BEGIN
        WHILE CHARINDEX('#^#', @vcEmailId) > 0	
            BEGIN
                SET @Email = LTRIM(RTRIM(SUBSTRING(@vcEmailId, 1,
                                                   CHARINDEX('#^#', @vcEmailId,
                                                             0)+2)))
                IF CHARINDEX('$^$', @Email) > 0 
                    BEGIN
                        SET @EmailName = LTRIM(RTRIM(SUBSTRING(@Email, 0, CHARINDEX('$^$', @Email))))
                        SET @EmailAdd = LTRIM(RTRIM(SUBSTRING(@Email, LEN(@EmailName) + 1, CHARINDEX('$^$', @Email) + LEN(@Email) - 1)))
                        SET @EmailAdd = REPLACE(@Emailadd, '$^$', '')
                        SET @EmailAdd = REPLACE(@Emailadd, '#^#', '')                
                    END
                   SET @vcNewEmailId =@vcNewEmailId + (SELECT TOP  1 CAST(ISNULL(numEmailid,0) AS VARCHAR) FROM dbo.EmailMaster WHERE dbo.EmailMaster.vcEmailID=@EmailAdd AND numDomainId=@numDomianID) + '$^$' + @EmailName + '$^$' + @EmailAdd   + '#^#'
				--PRINT @Email
				--SET @vcNewEmailId = @vcNewEmailId + ','
                SET @vcEmailId = LTRIM(RTRIM(SUBSTRING(@vcEmailId,
                                                       CHARINDEX('#^#', @vcEmailId)
                                                       + 3, LEN(@vcEmailId))))
	
		       END
            
			END
			IF LEN(@vcNewEmailId) > 0
			BEGIN
				 SET @vcNewEmailId = LEFT(@vcNewEmailId,LEN(@vcNewEmailId)-3)
			END                
                 RETURN @vcNewEmailId
                 
	END
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='GetAlertDetail')
DROP FUNCTION GetAlertDetail
GO
CREATE FUNCTION [dbo].[GetAlertDetail]
(
	@numEmailHstrID NUMERIC(18,0),
	@numECampaignID NUMERIC(18,0),
	@numContactID NUMERIC(18,0),
	@bitOpenActionItem BIT,
	@bitOpencases BIT,
	@bitOpenProject BIT,
	@bitOpenSalesOpp BIT,
	@bitBalancedue BIT,
	@bitUnreadEmail BIT,
	@bitCampaign BIT,
	@TotalBalanceDue BIGINT,
	@OpenSalesOppCount BIGINT,
	@OpenCaseCount BIGINT,
	@OpenProjectCount BIGINT,
	@UnreadEmailCount BIGINT,
	@OpenActionItemCount BIGINT,
	@CampaignDTLCount BIGINT
)
Returns VARCHAR(MAX) 
As
BEGIN
	DECLARE @vcAlert AS VARCHAR(MAX) = ''
	
	--CHECK AR BALANCE
	IF @bitBalancedue = 1 AND  @TotalBalanceDue > 0 
    BEGIN 
		SET @vcAlert = '<img alt="" src="../images/dollar.png" style="height: 16px; vertical-align: middle; padding-bottom: 2px;" />&nbsp;'
    END 

	--CHECK OPEN SALES ORDER COUNT
	IF @bitOpenSalesOpp = 1 AND @OpenSalesOppCount > 0 
    BEGIN 
		SET @vcAlert = @vcAlert + '<img alt="" src="../images/icons/cart.png" style="height: 16px; vertical-align: middle; padding-bottom: 2px;" />&nbsp;'
    END 

	--CHECK OPEN CASES COUNT
	IF @bitOpencases = 1 AND @OpenCaseCount > 0 
    BEGIN 
		SET @vcAlert = @vcAlert + '<img alt="" src="../images/icons/headphone_mic.png" style="height: 16px; vertical-align: middle; padding-bottom: 2px;" />&nbsp;'
    END 

	--CHECK OPEN PROJECT COUNT
	IF @bitOpenProject = 1 AND @OpenProjectCount > 0 
    BEGIN 
		SET @vcAlert = @vcAlert + '<img alt="" src="../images/Compass-16.gif" style="height: 16px; vertical-align: middle; padding-bottom: 2px;" />&nbsp;'  
    END 

	--CHECK UNREAD MAIL COUNT
	IF @bitUnreadEmail = 1 AND @UnreadEmailCount > 0 
    BEGIN 
       SET @vcAlert = @vcAlert + '<a title="Recent Correspondance" class="hyperlink" onclick="OpenCorresPondance('+ CAST(@numContactID AS VARCHAR(10)) + ');">(' + CAST(@UnreadEmailCount AS VARCHAR(10)) + ')</a>&nbsp;'
    END
	
	--CHECK ACTIVE CAMPAIGN COUNT
	IF @bitCampaign = 1 AND ISNULL(@numECampaignID,0) > 0
	BEGIN
		
		IF @CampaignDTLCount = 0
		BEGIN
			SET @vcAlert = @vcAlert + '<img alt="" src="../images/comflag.png" style="height: 16px; vertical-align: middle; padding-bottom: 2px;" />&nbsp;'  
		END
		ELSE
		BEGIN
			SET @vcAlert = @vcAlert + '<img alt="" src="../images/Circle_Green_16.png" style="height: 16px; vertical-align: middle; padding-bottom: 2px;" />&nbsp;'  
		END
	END 

	--CHECK COUNT OF ACTION ITEM
	IF @bitOpenActionItem = 1 AND @OpenActionItemCount > 0 
	BEGIN
		SET @vcAlert = @vcAlert + '<img alt="" src="../images/MasterList-16.gif" onclick="return NewActionItem('+ CAST(@numContactID AS VARCHAR(10)) +','+ CAST(@numEmailHstrID AS VARCHAR(10)) +')" style="height: 16px; vertical-align: middle; padding-bottom: 2px;" />&nbsp;'
	END

	RETURN @vcAlert
END
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='GetContactCampignAuditDetail')
DROP FUNCTION GetContactCampignAuditDetail
GO
CREATE FUNCTION [dbo].[GetContactCampignAuditDetail] (@numECampaignID NUMERIC(18,0), @numContactID NUMERIC(18,0))
RETURNS VARCHAR(500)
AS
BEGIN
	DECLARE @vcCampaignAudit AS VARCHAR(500) = ''
    DECLARE @CampaignName AS VARCHAR(200)
	DECLARE @NextDrip AS VARCHAR(200)
	DECLARE @CompletedStages AS VARCHAR(200)
    DECLARE @LastDrip AS VARCHAR(200)

	DECLARE @numDomainID NUMERIC(18,0)
	DECLARE @ConECampaignID AS VARCHAR(200)
	DECLARE @bitCompleted AS BIT = 0

	IF (SELECT COUNT(*) FROM ECampaign WHERE numECampaignID = @numECampaignID) > 0
	BEGIN
		SELECT @vcCampaignAudit = vcECampName, @numDomainID=numDomainID FROM ECampaign WHERE numECampaignID = @numECampaignID
		SELECT TOP 1 @ConECampaignID=numConEmailCampID  FROM ConECampaign WHERE numECampaignID = @numECampaignID AND numContactID = @numContactID ORDER BY numConEmailCampID DESC

		SELECT TOP 1 @NextDrip= CONCAT(MONTH(dtExecutionDate),'-',DAY(dtExecutionDate)) FROM ConECampaignDTL WHERE numConECampID = @ConECampaignID AND ISNULL(bitSend,0) = 0 ORDER BY numConECampDTLID ASC
		SELECT TOP 1 @LastDrip= CONCAT(MONTH(bintSentON),'-',DAY(bintSentON)) FROM ConECampaignDTL WHERE numConECampID = @ConECampaignID AND ISNULL(bitSend,0) = 1 ORDER BY numConECampDTLID DESC
		SELECT @CompletedStages=COUNT(*) FROM ConECampaignDTL WHERE numConECampID = @ConECampaignID AND ISNULL(bitSend,0) = 1

		IF (SELECT COUNT(*) FROM ConECampaignDTL WHERE numConECampID = @ConECampaignID AND ISNULL(bitSend,0) = 0) = 0
		BEGIN
			SET @bitCompleted = 1
		END

		SET @vcCampaignAudit = @vcCampaignAudit + ' ' + ISNULL(@NextDrip,'') + ' / Audit (' + ISNULL(@CompletedStages,0) + ') ' +  ISNULL(@LastDrip,'') + ' ' + (CASE WHEN @bitCompleted = 1 THEN '#Completed#' ELSE '#Running#' END)
	END

	RETURN @vcCampaignAudit
END
GO
/****** Object:  UserDefinedFunction [dbo].[GetCustFldItemsValue]    Script Date: 07/26/2008 18:12:58 ******/

GO

GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='GetCustFldItemsValue')
DROP FUNCTION GetCustFldItemsValue
GO
CREATE FUNCTION [dbo].[GetCustFldItemsValue](@numFldId numeric(9),@pageId as tinyint,@numRecordId as numeric(9),@bitSerialized as bit,@fld_type as varchar(100))  
 RETURNS varchar (100)   
AS  
BEGIN  
declare @vcValue as  varchar(100)  
if @pageId=9
begin
	select @vcValue=Case when @fld_type='SelectBox' then dbo.GetListIemName(Fld_Value)
						 when  @fld_type='CheckBox' then Case When isnull(Fld_Value,0)=1 then 'Yes' else 'No' end
						else Fld_Value end from CFW_Fld_Values_Serialized_Items where Fld_ID=@numFldId and RecId=@numRecordId and bitSerialized=@bitSerialized  
end 
if @vcValue is null set @vcValue='0'  
RETURN @vcValue  
END
GO

--Created By Anoop Jayaraj


GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='TF'AND NAME ='GetCustomFieldDTLIDAndValues')
DROP FUNCTION GetCustomFieldDTLIDAndValues
GO
CREATE FUNCTION [dbo].[GetCustomFieldDTLIDAndValues] (@PageID integer, @RecID numeric(9))

RETURNS @Results TABLE (FldDTLID numeric(9),Fld_ID numeric(9),Fld_Value varchar(1000)) 
AS


BEGIN


if @pageId=1   OR @PageId= 12 or  @PageId= 13 or  @PageId= 14
begin 
insert into  @Results (FldDTLID,Fld_ID,Fld_Value)    
 select FldDTLID,Fld_ID,Fld_Value from CFW_FLD_Values where  RecId=@RecID      
	  
end      
	  
if @pageId=4      
begin      
insert into  @Results (FldDTLID,Fld_ID,Fld_Value)   
 select FldDTLID,Fld_ID,Fld_Value from CFW_FLD_Values_Cont where  RecId=@RecID      
	  
end      
	  
if @pageId=3      
begin      
insert into  @Results (FldDTLID,Fld_ID,Fld_Value)    
 select FldDTLID,Fld_ID,Fld_Value from CFW_FLD_Values_Case where  RecId=@RecID      
	  
end      
	  
	  
if @pageId=5      
begin      
insert into  @Results (FldDTLID,Fld_ID,Fld_Value)   
 select FldDTLID,Fld_ID,Fld_Value from CFW_FLD_Values_Item where  RecId=@RecID      
	  
end      
	  
if @pageId=2 or @pageId=6      
begin      
insert into  @Results (FldDTLID,Fld_ID,Fld_Value)   
 select FldDTLID,Fld_ID,Fld_Value from CFW_Fld_Values_Opp where  RecId=@RecID      
	  
end      
	  
if @pageId=7 or @pageId=8      
begin      
insert into  @Results (FldDTLID,Fld_ID,Fld_Value)   
 select FldDTLID,Fld_ID,Fld_Value from CFW_Fld_Values_Product where  RecId=@RecID      
	  
end     
	
if @pageId=9    
begin 
   
insert into  @Results (FldDTLID,Fld_ID,Fld_Value) 
 select FldDTLID,Fld_ID,Fld_Value from CFW_Fld_Values_Serialized_Items where  RecId=@RecID      
	  
end    
if @pageId=11    
begin  
  
insert into  @Results (FldDTLID,Fld_ID,Fld_Value)    
 select FldDTLID,Fld_ID,Fld_Value from CFW_Fld_Values_Pro where  RecId=@RecID      
	  
end    
		   
	  
RETURN     


End
---------------------------

-------------------------- STORED PROCEDURES

/****** Object:  StoredProcedure [dbo].[USP_CalendarEmployees]    Script Date: 07/26/2008 16:14:58 ******/
GO 
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj                      
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_calendaremployees')
DROP PROCEDURE usp_calendaremployees
GO
CREATE PROCEDURE [dbo].[USP_CalendarEmployees]                          
@numUserCntId as numeric(9)=0,            
@numDomainID as numeric(9)=0,            
@GroupId as numeric(9)=0                     
as               
            
--select *,resourceName as [Name] from resource      
declare @UserID as numeric(9)            
select @UserID=numUserID from UserMaster where numUserDetailID=@numUserCntId            

--1000 is added so that Myself is always first even if resourceId is greater than its previous records
select 1000,resourceId,vcFirstName+' '+ vcLastName as [Name] from AdditionalContactsInformation                          
join UserMaster on numContactID=numUserDetailID        
join Resource on numusercntid = numUserDetailID                      
where   
--numTeam in (select numTeam from ForReportsByTeam where tintType=@intType and numUserCntID=@numUserCntId)  and                        
  numUserId <> @UserID      
 and UserMaster.numDomainID=@numDomainID    and numGroupID = @GroupId                 
      
union      
      
select 1,resourceId,'Myself' as [Name] from AdditionalContactsInformation                          
join UserMaster on numUserDetailID=numUserDetailID       
join Resource on numusercntid = numUserDetailID       
where numUserDetailID=@numUserCntId and UserMaster.numDomainID=@numDomainID and numGroupID = @GroupId
GO
/****** Object:  StoredProcedure [dbo].[USP_cfwGetFields]    Script Date: 07/26/2008 16:15:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj                    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_cfwgetfields')
DROP PROCEDURE usp_cfwgetfields
GO
CREATE PROCEDURE [dbo].[USP_cfwGetFields]                              
@PageId as tinyint,                              
@numRelation as numeric(9),                              
@numRecordId as numeric(9),                  
@numDomainID as numeric(9)                              
as            

                  
if @PageId= 1 
begin                              
select CFW_Fld_Master.fld_id,fld_type,fld_label,numlistid,numOrder,Rec.FldDTLID,isnull(Rec.Fld_Value,0) as Value,subgrp as TabId,Grp_Name as tabname,vcURL,CFW_Fld_Master.vcToolTip from CFW_Fld_Master join CFW_Fld_Dtl                              
on Fld_id=numFieldId                              
left join CFw_Grp_Master                               
on subgrp=CFw_Grp_Master.Grp_id 
left join dbo.GetCustomFieldDTLIDAndValues(@PageId,@numRecordId) Rec
on Rec.Fld_ID=CFW_Fld_Master.fld_id                             
where CFW_Fld_Master.grp_id=@PageId and numRelation=@numRelation and CFW_Fld_Master.numDomainID=@numDomainID                          
         
union        
select 0 as fld_id,'Frame' as fld_type,'' as fld_label,0 as numlistid,0,0,'' as Value,Grp_id as TabId,Grp_Name as tabname,vcURLF,Null as vcToolTip from CFw_Grp_Master        
where tintType=1 and loc_Id = @PageId   and numDomainID=@numDomainID           
order by subgrp,numOrder                             
end                              

---select Shared Custom fields and tab specific customfields -- where Pageid=1 stands for shared custom fields among accounts,leads,prospects
IF  @PageId= 12 or  @PageId= 13 or  @PageId= 14
BEGIN
	select CFW_Fld_Master.fld_id,fld_type,fld_label,numlistid,numOrder,Rec.FldDTLID,isnull(Rec.Fld_Value,0) as Value,subgrp as TabId,Grp_Name as tabname,vcURL,CFW_Fld_Master.vcToolTip from CFW_Fld_Master join CFW_Fld_Dtl                              
	on Fld_id=numFieldId                              
	left join CFw_Grp_Master                               
	on subgrp=CFw_Grp_Master.Grp_id 
	left join dbo.GetCustomFieldDTLIDAndValues(@PageId,@numRecordId) Rec
	on Rec.Fld_ID=CFW_Fld_Master.fld_id                             
	where CFW_Fld_Master.grp_id=@PageId and numRelation=@numRelation and CFW_Fld_Master.numDomainID=@numDomainID                          
	
	UNION 
	
	select CFW_Fld_Master.fld_id,fld_type,fld_label,numlistid,numOrder,Rec.FldDTLID,isnull(Rec.Fld_Value,0) as Value,subgrp as TabId,Grp_Name as tabname,vcURL,CFW_Fld_Master.vcToolTip from CFW_Fld_Master join CFW_Fld_Dtl                              
	on Fld_id=numFieldId                              
	left join CFw_Grp_Master                               
	on subgrp=CFw_Grp_Master.Grp_id 
	left join dbo.GetCustomFieldDTLIDAndValues(1,@numRecordId) Rec
	on Rec.Fld_ID=CFW_Fld_Master.fld_id                             
	where CFW_Fld_Master.grp_id=1 and numRelation=@numRelation and CFW_Fld_Master.numDomainID=@numDomainID
	         
	union        
	select 0 as fld_id,'Frame' as fld_type,'' as fld_label,0 as numlistid,0,0,'' as Value,Grp_id as TabId,Grp_Name as tabname,vcURLF,NUll as vcToolTip from CFw_Grp_Master        
	where tintType=1 and loc_Id = @PageId   and numDomainID=@numDomainID           
	order by subgrp,numOrder                             
	
END


                              
if @PageId= 4                              
begin       
---------------------Very Important----------------------    
-------Who wrote this part    
---------------------------------------------------------    
    
    
--declare @ContactId as numeric(9)                              
---select @ContactId=numContactId from cases where numCaseid=@numRecordId                              
--select @numRelation=numCompanyType from AdditionalContactsInformation AddC                              
---join DivisionMaster Div on AddC.numDivisionId=Div.numDivisionId                              
---join CompanyInfo Com on  Com.numCompanyId=Div.numCompanyId                              
---where AddC.numContactID=@ContactId                              
                              
select CFW_Fld_Master.fld_id,fld_type,fld_label,numlistid,numOrder,
Rec.FldDTLID,isnull(Rec.Fld_Value,0) as Value,
subgrp as TabId,Grp_Name as tabname,vcURL,CFW_Fld_Master.vcToolTip from CFW_Fld_Master join CFW_Fld_Dtl                              
on Fld_id=numFieldId                              
left join CFw_Grp_Master                               
on subgrp=CFw_Grp_Master.Grp_id  
left join dbo.GetCustomFieldDTLIDAndValues(@PageId,@numRecordId) Rec
on Rec.Fld_ID=CFW_Fld_Master.fld_id                             
where CFW_Fld_Master.grp_id=@PageId and numRelation=@numRelation and CFW_Fld_Master.numDomainID=@numDomainID                             
--select @numRelation=numContactType from AdditionalContactsInformation AddC                              
--join DivisionMaster Div on AddC.numDivisionId=Div.numDivisionId                              
--join CompanyInfo Com on  Com.numCompanyId=Div.numCompanyId                              
--where AddC.numContactID=@numRecordId                              
                              
union        
select 0 as fld_id,'Frame' as fld_type,'' as fld_label,0 as numlistid,0,0,'' as Value,
Grp_id as TabId,Grp_Name as tabname,vcURLF,NUll as vcToolTip from CFw_Grp_Master        
where tintType=1 and loc_Id = @PageId   and numDomainID=@numDomainID                              
   order by subgrp,numOrder                           
end                              
                              
if @PageId= 2 or  @PageId= 3 or @PageId= 5 or @PageId= 6 or @PageId= 7 or @PageId= 8     or @PageId= 11                   
begin                              
select CFW_Fld_Master.fld_id,fld_type,fld_label,numlistid,ISNULL(Rec.FldDTLID,0) FldDTLID,
CASE WHEN @PageId = 5 THEN (CASE WHEN ISNULL(Rec.Fld_Value, '') = '' AND fld_type = 'Checkbox' THEN '0' ELSE ISNULL(Rec.Fld_Value, 0) END) ELSE ISNULL(Rec.Fld_Value,0) END  AS Value,
subgrp as TabId,Grp_Name as tabname,vcURL,CFW_Fld_Master.vcToolTip  from CFW_Fld_Master                               
left join CFw_Grp_Master                               
on subgrp=CFw_Grp_Master.Grp_id  
left join dbo.GetCustomFieldDTLIDAndValues(@PageId,@numRecordId) Rec
on Rec.Fld_ID=CFW_Fld_Master.fld_id                             
where CFW_Fld_Master.grp_id=@PageId and CFW_Fld_Master.numDomainID=@numDomainID         
union        
select 0 as fld_id,'Frame' as fld_type,'' as fld_label,0 as numlistid,0 as FldDTLID,'0' as Value,Grp_id as TabId,Grp_Name as tabname,vcURLF,NULL as vcToolTip from CFw_Grp_Master        
where tintType=1 and loc_Id = @PageId  and numDomainID=@numDomainID      
end

select GRp_Id as TabID,Grp_Name AS TabName from CFw_Grp_Master where 
(Loc_ID=@PageId OR 1=(CASE WHEN @PageId IN (12,13,14) THEN CASE WHEN Loc_Id=1 THEN 1 ELSE 0 END ELSE 0 END))
 and numDomainID=@numDomainID
/*tintType stands for static tabs */ AND tintType<>2
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ConECampaignDTL_SetTrackingStatus')
DROP PROCEDURE USP_ConECampaignDTL_SetTrackingStatus
GO
CREATE PROCEDURE [dbo].[USP_ConECampaignDTL_SetTrackingStatus]                            
 @numConECampDTLID NUMERIC(9 )= 0
AS                            
BEGIN                            
	UPDATE ConECampaignDTL SET bitEmailRead = 1 WHERE numConECampDTLID = @numConECampDTLID
END
GO



GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ConECampaignDTLLinks_Save')
DROP PROCEDURE USP_ConECampaignDTLLinks_Save
GO
CREATE PROCEDURE [dbo].[USP_ConECampaignDTLLinks_Save]                            
 @numConECampDTLID NUMERIC(9 )= 0,
 @strBroadcastingLink AS TEXT = ''
AS                            
BEGIN                            
	DECLARE @hDoc4 INT                                                
    EXEC sp_xml_preparedocument @hDoc4 OUTPUT, @strBroadcastingLink

    INSERT INTO ConECampaignDTLLinks
		(
			numConECampaignDTLID,
			vcOriginalLink
		)
    SELECT  
			@numConECampDTLID,
            X.vcOriginalLink
    FROM    
		( 
			SELECT    
				*
            FROM      
				OPENXML (@hDoc4, '/LinkRecords/Table',2)
    WITH 
        ( 
            vcOriginalLink VARCHAR(2000)
        )
        ) X        

	SELECT * FROM ConECampaignDTLLinks WHERE numConECampaignDTLID = @numConECampDTLID
END
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ConECampaignDTLLinks_UpdateLinkClicked')
DROP PROCEDURE USP_ConECampaignDTLLinks_UpdateLinkClicked
GO
CREATE PROCEDURE [dbo].[USP_ConECampaignDTLLinks_UpdateLinkClicked]                            
 @numLinkID NUMERIC(9 )= 0
AS                            
BEGIN                            
	
	IF (SELECT ISNULL(bitClicked,0) FROM ConECampaignDTLLinks WHERE numLinkID = @numLinkID) = 0
	BEGIN
		UPDATE 
			ConECampaignDTLLinks
		SET
			bitClicked = 1
		WHERE
			numLinkID = @numLinkID
	END

	SELECT ISNULL(vcOriginalLink,'') FROM ConECampaignDTLLinks WHERE numLinkID = @numLinkID
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
                    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_DeleteAssemblyWOAndInventory')
DROP PROCEDURE USP_DeleteAssemblyWOAndInventory
GO
CREATE PROCEDURE [dbo].[USP_DeleteAssemblyWOAndInventory]
	@numDomainID AS NUMERIC(18,0),
	@numUserCntID AS NUMERIC(18,0),
	@numWOID AS NUMERIC(18,0),
	@numOppID AS NUMERIC(18,0)
AS
BEGIN
SET XACT_ABORT ON;

BEGIN TRY 
BEGIN TRAN	
	DECLARE @numChildWOID AS NUMERIC(18,0)
	DECLARE @WorkOrder TABLE
	(
		ID INT IDENTITY(1,1),
		numWOID NUMERIC(18,0)
	)

	--GET ALL WORK ORDER CREATED FOR ORDER ASSEMBLY ITEM IN BOTTOM TO TOP ORDER
	;WITH CTE (numLevel,numWOID) AS
	(
		SELECT 
			1,
			numWOId
		FROM
			WorkOrder
		WHERE
			numParentWOID = @numWOID
			AND numWOStatus <> 23184
		UNION ALL
		SELECT 
			c.numLevel + 1,
			WorkOrder.numWOId		
		FROM
			WorkOrder
		INNER JOIN
			CTE c
		ON
			WorkOrder.numParentWOID = c.numWOID AND
			WorkOrder.numWOStatus <> 23184
	)

	INSERT INTO @WorkOrder SELECT numWOID FROM CTE ORDER BY numLevel DESC

	DECLARE @i AS INT = 1
	DECLARE @COUNT AS INT
	SELECT @COUNT=COUNT(*) FROM @WorkOrder

	--DELETE EACH WORK ORDER CREATED RECURSIVELY
	WHILE @i <= @COUNT
	BEGIN
		SELECT @numChildWOID=numWOID FROM @WorkOrder WHERE ID=@i

		EXEC USP_DeleteWorkOrder @numWOId=@numChildWOID,@numUserCntID=@numUserCntID,@numDomainID=@numDomainID,@bitOrderDelete=1

		SET @i = @i + 1 
	END

	--REVERT INVENTORY OF WORK ORDER ITEMS
	DECLARE @TEMP TABLE
	(
		ID INT IDENTITY(1,1),
		numWarehouseItemID NUMERIC(18,0),
		numQtyItemsReq NUMERIC(18,0)
	)

	INSERT INTO @TEMP SELECT numWareHouseItemId,numQtyItemsReq FROM WorkOrderDetails WHERE numWOId=@numWOId AND ISNULL(numWareHouseItemId,0) > 0


	SET @i = 1
	SET @COUNT = 0
	SELECT @COUNT = COUNT(*) FROM @TEMP

	DECLARE @numWareHouseItemID NUMERIC(18,0)
	DECLARE @numQtyItemsReq NUMERIC(18,0)
	DECLARE @Description VARCHAR(2000)
	DECLARE @onHand NUMERIC(18,0)
	DECLARE @onOrder NUMERIC(18,0)
	DECLARE @onAllocation NUMERIC(18,0)
	DECLARE @onBackOrder NUMERIC(18,0)

	WHILE @i <= @COUNT
	BEGIN
		SELECT @numWareHouseItemID=ISNULL(numWarehouseItemID,0),@numQtyItemsReq=ISNULL(numQtyItemsReq,0) FROM @TEMP WHERE ID = @i

		SET @Description = 'Items Allocation Reverted SO-WO Deleted (Qty:' + CAST(@numQtyItemsReq AS VARCHAR(10)) + ')'
		
		--GET CURRENT INEVENTORY STATUS
		SELECT
			@onHand = ISNULL(numOnHand,0),
			@onOrder = ISNULL(numOnOrder,0),
			@onAllocation = ISNULL(numAllocation,0),
			@onBackOrder = ISNULL(numBackOrder,0)
		FROM
			WareHouseItems
		WHERE
			numWareHouseItemID = @numWareHouseItemID

		--CHANGE INVENTORY
		IF @numQtyItemsReq >= @onBackOrder 		BEGIN			SET @numQtyItemsReq = @numQtyItemsReq - @onBackOrder			SET @onBackOrder = 0                            			IF (@onAllocation - @numQtyItemsReq >= 0)					SET @onAllocation = @onAllocation - @numQtyItemsReq									SET @onHand = @onHand + @numQtyItemsReq                                                          		END                                            		ELSE IF @numQtyItemsReq < @onBackOrder 		BEGIN			SET @onBackOrder = @onBackOrder - @numQtyItemsReq		END

		--UPDATE INVENTORY AND WAREHOUSE TRACKING
		EXEC USP_UpdateInventoryAndTracking 
		@numOnHand=@onHand,
		@numOnAllocation=@onAllocation,
		@numOnBackOrder=@onBackOrder,
		@numOnOrder=@onOrder,
		@numWarehouseItemID=@numWareHouseItemID,
		@numReferenceID=@numOppID,
		@tintRefType=3,
		@numDomainID=@numDomainID,
		@numUserCntID=@numUserCntID,
		@Description=@Description 

		SET @i = @i + 1
	END


	DELETE FROM WorkOrderDetails WHERE numWOId=@numWOID 
	DELETE FROM WorkOrder WHERE numWOId=@numWOID AND [numDomainID] = @numDomainID
COMMIT TRAN 
END TRY 
BEGIN CATCH		
    IF ( @@TRANCOUNT > 0 ) 
    BEGIN
        ROLLBACK TRAN
    END

	DECLARE @ErrorMessage NVARCHAR(4000);
    DECLARE @ErrorSeverity INT;
    DECLARE @ErrorState INT;

    SELECT @ErrorMessage = ERROR_MESSAGE(), @ErrorSeverity = ERROR_SEVERITY(), @ErrorState = ERROR_STATE();

    RAISERROR (@ErrorMessage, @ErrorSeverity, @ErrorState);
END CATCH	

SET XACT_ABORT OFF;
END
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_DeleteFromCampaign')
DROP PROCEDURE USP_DeleteFromCampaign
GO
/****** Object:  StoredProcedure [dbo].[USP_DeleteFromCampaign]    Script Date: 02/18/2009 15:17:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create PROCEDURE [dbo].[USP_DeleteFromCampaign]
@numContactID NUMERIC(9),
@numECampaignID NUMERIC(9)
AS
  BEGIN
    DECLARE  @numConEmailCampID NUMERIC(9)
    
    SELECT @numConEmailCampID = [numConEmailCampID]
    FROM   [ConECampaign]
    WHERE  [numContactID] = @numContactID
           AND [numECampaignID] = @numECampaignID
        
	UPDATE [ConECampaign] SET [bitEngaged] = 0 WHERE [numConEmailCampID]=@numConEmailCampID
	UPDATE AdditionalContactsInformation SET numECampaignID = NULL WHERE numContactId = @numContactID
---- Delete child record                 
--    DELETE FROM [ConECampaignDTL]
--    WHERE       [numConECampID] = @numConEmailCampID
---- Delete parent Record           
--    DELETE FROM [ConECampaign]
--    WHERE       [numContactID] = @numContactID
--                AND [numECampaignID] = @numECampaignID
                

  END
--modified by anoop jayaraj              
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_deleteoppurtunity')
DROP PROCEDURE usp_deleteoppurtunity
GO
CREATE PROCEDURE [dbo].[USP_DeleteOppurtunity]
 @numOppId numeric(9) ,        
 @numDomainID as numeric(9),
 @numUserCntID AS NUMERIC(9)                       
AS                          
             
       
if exists(select * from OpportunityMaster where numOppID=@numOppId and numDomainID=@numDomainID)         
BEGIN 

SET XACT_ABORT ON;

BEGIN TRY 
BEGIN TRAN

	IF (SELECT COUNT(*) FROM RecurrenceConfiguration WHERE numOppID = @numOppId AND numDomainID=@numDomainID AND ISNULL(bitMarkForDelete,0) = 0) > 0
	BEGIN
		RAISERROR ( 'RECURRING ORDER OR BIZDOC', 16, 1 )
		RETURN
	END
	ELSE IF (SELECT COUNT(*) FROM RecurrenceTransaction INNER JOIN RecurrenceConfiguration ON RecurrenceTransaction.numRecConfigID = RecurrenceConfiguration.numRecConfigID WHERE RecurrenceTransaction.numRecurrOppID = @numOppId AND ISNULL(RecurrenceConfiguration.bitDisabled,0) = 0) > 0
	BEGIN	
		RAISERROR ( 'RECURRED ORDER', 16, 1 ) ;
		RETURN ;
	END

      IF EXISTS ( SELECT  *
                FROM    dbo.CaseOpportunities
                WHERE   numOppID = @numOppId
                        AND numDomainID = @numDomainID )         
        BEGIN
            RAISERROR ( 'CASE DEPENDANT', 16, 1 ) ;
            RETURN ;
        END	
	 IF EXISTS ( SELECT  *
			FROM    dbo.ReturnHeader
			WHERE   numOppId = @numOppId
					AND numDomainId= @numDomainID )         
	BEGIN
		RAISERROR ( 'RETURN_EXIST', 16, 1 ) ;
		RETURN ;
	END	

      
      
      

DECLARE @tintError TINYINT;SET @tintError=0

EXECUTE USP_CheckCanbeDeleteOppertunity @numOppId,@tintError OUTPUT
  
  IF @tintError=1
  BEGIN
  	 RAISERROR ( 'OppItems DEPENDANT', 16, 1 ) ;
     RETURN ;
  END
  
  EXEC dbo.USP_ValidateFinancialYearClosingDate
	@numDomainID = @numDomainID, --  numeric(18, 0)
	@tintRecordType = 2, --  tinyint
	@numRecordID = @numOppId --  numeric(18, 0)
	
  --Credit Balance
   Declare @monCreditAmount as money;Set @monCreditAmount=0
   Declare @monCreditBalance as money;Set @monCreditBalance=0
   Declare @numDivisionID as numeric(9);Set @numDivisionID=0
   Declare @tintOppType as tinyint

   Select @numDivisionID=numDivisionID,@tintOppType=tintOppType from OpportunityMaster WHERE  numOppId=@numOppId  and numDomainID= @numDomainID 
   
    IF @tintOppType=1 --Sales
      Select @monCreditBalance=isnull(monSCreditBalance,0) from DivisionMaster WHERE  numDivisionID=@numDivisionID  and numDomainID= @numDomainID 
    else IF @tintOppType=2 --Purchase
      Select @monCreditBalance=isnull(monPCreditBalance,0) from DivisionMaster WHERE  numDivisionID=@numDivisionID  and numDomainID= @numDomainID 
   
   Select @monCreditAmount=sum(monAmount) from CreditBalanceHistory where numReturnID in (select numReturnID from [Returns] WHERE [numOppId] = @numOppId)
	
	IF ( @monCreditAmount > @monCreditBalance )         
        BEGIN
            RAISERROR ( 'CreditBalance DEPENDANT', 16, 1 ) ;
            RETURN ;
        END	
        
  declare @tintOppStatus as tinyint    
 declare @tintShipped as tinyint                
  select @tintOppStatus=tintOppStatus,@tintShipped=tintShipped from OpportunityMaster where numOppID=@numOppId                
  
  DECLARE @isOrderItemsAvailable AS INT = 0	SELECT 		@isOrderItemsAvailable = COUNT(*) 	FROM    		OpportunityItems OI	JOIN 		dbo.OpportunityMaster OM ON OI.numOppId = OM.numOppId	JOIN 		Item I ON OI.numItemCode = I.numItemCode	WHERE   		(charitemtype='P' OR 1=(CASE WHEN tintOppType=1 THEN 								CASE WHEN ISNULL(I.bitKitParent,0) = 1 AND ISNULL(I.bitAssembly,0) = 1 THEN 0									 WHEN ISNULL(I.bitKitParent,0) = 1 THEN 1									 ELSE 0 END 								ELSE 0 END)) AND OI.numOppId = @numOppId							   AND ( bitDropShip = 0									 OR bitDropShip IS NULL								   )   if (@tintOppStatus=1 and @tintShipped=0) OR  @tintShipped=1                 begin        exec USP_RevertDetailsOpp @numOppId,1,@numUserCntID                  end                                 
                        
  DELETE FROM [OpportunitySalesTemplate] WHERE [numOppId] =@numOppId 
  delete OpportunityLinking where   numChildOppID=  @numOppId or   numParentOppID=    @numOppId          
  delete RECENTITEMS where numRecordID =  @numOppId and chrRecordType='O'             
                          
   if @tintShipped=1
    BEGIN
		update WD set tintStatus=0,numQty=WD.numQty + WS.numQty FROM WareHouseItmsDTL WD INNER JOIN OppWarehouseSerializedItem WS ON WD.numWareHouseItmsDTLID=WS.numWarehouseItmsDTLID where  WS.numOppID =  @numOppId
    END      
    ELSE
	BEGIN
		update WareHouseItmsDTL set tintStatus=0 where numWareHouseItmsDTLID in (select numWarehouseItmsDTLID from OppWarehouseSerializedItem where numOppID =  @numOppId)
	END
                       
  delete from OppWarehouseSerializedItem where numOppID=  @numOppId                  
                  
                        
  delete from OpportunityItemLinking where numNewOppID=@numOppId                      
                         
  delete from OpportunityAddress  where numOppID=@numOppId


  DELETE FROM OpportunityBizDocKitItems WHERE [numOppBizDocItemID] IN 
  (SELECT [numOppBizDocItemID] FROM [OpportunityBizDocItems] WHERE [numOppBizDocID] IN 
	(select numOppBizDocsId from OpportunityBizDocs where numOppId=@numOppId))
	
  delete from OpportunityBizDocItems where numOppBizDocID in                        
  (select numOppBizDocsId from OpportunityBizDocs where numOppId=@numOppId)

--  delete from OpportunityBizDocTaxItems where numOppBizDocID in                        
--  (select numOppBizDocsId from OpportunityBizDocs where numOppId=@numOppId)
--  

  delete from OpportunityItemsTaxItems WHERE numOppID=@numOppID
  delete from OpportunityMasterTaxItems WHERE numOppID=@numOppID
  
  delete from OpportunityBizDocDtl where numBizDocID in                        
  (select numOppBizDocsId from OpportunityBizDocs where numOppId=@numOppId)                        
           
   DELETE FROM OpportunityBizDocsPaymentDetails WHERE [numBizDocsPaymentDetId] IN 
	 (SELECT [numBizDocsPaymentDetId] FROM [OpportunityBizDocsDetails] where numBizDocsId in                        
		(select numOppBizDocsId from OpportunityBizDocs where numOppId=@numOppId)) 
               
  delete from OpportunityBizDocsDetails where numBizDocsId in                        
  (select numOppBizDocsId from OpportunityBizDocs where numOppId=@numOppId)  

DELETE FROM [ShippingReportItems] WHERE [numShippingReportId] IN  (SELECT numShippingReportId from [ShippingReport] WHERE [numOppBizDocId] IN (select numOppBizDocsId from OpportunityBizDocs where numOppId=@numOppId))
DELETE FROM [ShippingBox] WHERE [numShippingReportId] IN  (SELECT numShippingReportId from [ShippingReport] WHERE [numOppBizDocId] IN (select numOppBizDocsId from OpportunityBizDocs where numOppId=@numOppId))
DELETE FROM [ShippingReport] WHERE [numOppBizDocId] IN (select numOppBizDocsId from OpportunityBizDocs where numOppId=@numOppId)

delete from DocumentWorkflow where numDocID in 
(select numOppBizDocsId from OpportunityBizDocs where numOppId=@numOppId) and cDocType='B'      

delete from BizDocAction where numBizActionId in(select numBizActionId from BizActionDetails where numOppBizDocsId in
(select numOppBizDocsId from OpportunityBizDocs where numOppId=@numOppId) and btDocType=1)

delete from BizActionDetails where numOppBizDocsId in
(select numOppBizDocsId from OpportunityBizDocs where numOppId=@numOppId) and btDocType=1

  delete from OpportunityBizDocs where numOppId=@numOppId                        
                          
  delete from OpportunityContact where numOppId=@numOppId                        
                          
  delete from OpportunityDependency where numOpportunityId=@numOppId                        
                          
  delete from TimeAndExpense where numOppId=@numOppId   and numDomainID= @numDomainID                     
  
  --Credit Balance
  delete from CreditBalanceHistory where numReturnID in (select numReturnID from [Returns] WHERE [numOppId] = @numOppId)

    IF @tintOppType=1 --Sales
		update DivisionMaster set monSCreditBalance=isnull(monSCreditBalance,0) - isnull(@monCreditAmount,0) where numDivisionID=@numDivisionID and numDomainID=@numDomainID
    else IF @tintOppType=2 --Purchase
		update DivisionMaster set monPCreditBalance=isnull(monPCreditBalance,0) - isnull(@monCreditAmount,0) where numDivisionID=@numDivisionID and numDomainID=@numDomainID
  

  DELETE FROM [Returns] WHERE [numOppId] = @numOppId
                         
  delete from OpportunityKitItems where numOppId=@numOppId                        
                          
  delete from OpportunityItems where numOppId=@numOppId                        
                          
  DELETE FROM dbo.ProjectProgress WHERE numOppId =@numOppId AND numDomainId=@numDomainID
                          
  delete from OpportunityStageDetails where numOppId=@numOppId
                          
  delete from OpportunitySubStageDetails where numOppId=@numOppId
                                               
  DELETE FROM [OpportunityRecurring] WHERE [numOppId] = @numOppId
  
  DELETE FROM [OpportunityMasterAPI] WHERE [numOppId] = @numOppId
  
  DELETE FROM [WebApiOrderDetails] WHERE [numOppId] = @numOppId
  
  DELETE FROM [WebApiOppItemDetails] WHERE [numOppId] = @numOppId
  
  /*
  added by chintan: reason: when bizdoc is deleted and journal entries are not deleted in exceptional case, 
  it throws Foreign key reference error when deleting order.
  */
  DELETE FROM dbo.General_Journal_Details WHERE numJournalId IN (SELECT numJournal_Id FROM dbo.General_Journal_Header WHERE numOppId=@numOppId AND numDomainId=@numDomainID)
  DELETE FROM dbo.General_Journal_Header WHERE numOppId=@numOppId AND numDomainId= @numDomainID
 
  IF ISNULL(@tintOppStatus,0)=0 OR ISNULL(@isOrderItemsAvailable,0) = 0 OR (SELECT COUNT(*) FROM WareHouseItems_Tracking WHERE numDomainID=@numDomainID AND numReferenceID=@numOppId AND tintRefType IN (3,4) AND vcDescription LIKE '%Deleted%' AND CAST(dtCreatedDate AS DATE) = CAST(GETUTCDATE() AS DATE)) > 0
	delete from OpportunityMaster where numOppId=@numOppId and numDomainID= @numDomainID             
  ELSE
	RAISERROR ( 'INVENTORY IM-BALANCE', 16, 1 ) ;


COMMIT TRAN 

    END TRY 
    BEGIN CATCH		
        DECLARE @strMsg VARCHAR(200)
        SET @strMsg = ERROR_MESSAGE()
        IF ( @@TRANCOUNT > 0 ) 
            BEGIN
             RAISERROR ( @strMsg, 16, 1 ) ;
                ROLLBACK TRAN
                RETURN 1
            END
    END CATCH	

SET XACT_ABORT OFF;
END 

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_DeleteWorkOrder')
DROP PROCEDURE USP_DeleteWorkOrder
GO
CREATE PROCEDURE [dbo].[USP_DeleteWorkOrder]
@numWOId as numeric(9),
@numUserCntID AS NUMERIC(9),
@numDomainID AS NUMERIC(18,0),
@bitOrderDelete AS BIT
AS
BEGIN
BEGIN TRY
BEGIN TRANSACTION 

	DECLARE @numWareHouseItemID AS NUMERIC(9),@numQtyItemsReq AS NUMERIC(9),@numOppId AS NUMERIC(9),@numItemCode AS NUMERIC(9),@numWOStatus AS NUMERIC(18,0)
	DECLARE @onHand NUMERIC(18,0)
	DECLARE @onAllocation NUMERIC(18,0)
	DECLARE @onOrder NUMERIC(18,0)
	DECLARE @onBackOrder NUMERIC(18,0)
	DECLARE @Description VARCHAR(1000)

	DECLARE @numReferenceID AS NUMERIC(18,0)
	DECLARE @tintRefType TINYINT

	SELECT @numOppId=ISNULL(numOppId,0) FROM WorkOrder WHERE numWOId=@numWOId AND [WorkOrder].[numDomainID] = @numDomainID

	IF ISNULL(@numOppId,0) > 0
	BEGIN
		SET @numReferenceID = @numOppId
		SET @tintRefType = 3
	END
	ELSE
	BEGIN
		SET @numReferenceID = @numWOId
		SET @tintRefType = 2
	END

	--IF WORK ORDER IS CREATED FROM SALES ORDER AND WORK ORDER IS DELETED NOT WHOLE ORDER THEN EXECUTE FOLLOWING IF CODE
	IF @numOppId>0 AND ISNULL(@bitOrderDelete,0) = 0
	BEGIN
			UPDATE opp SET bitWorkOrder=0 FROM OpportunityItems opp JOIN WorkOrder wo 
			ON opp.numItemCode=wo.numItemCode AND ISNULL(wo.numOppId,0)=opp.numOppId WHERE wo.numWOId=@numWOId
			AND WO.[numDomainID] = @numDomainID
	END  


	SELECT 
		@numWareHouseItemID=numWareHouseItemID,
		@numQtyItemsReq=numQtyItemsReq,
		@numOppId=ISNULL(numOppId,0),
		@numItemCode=numItemCode,
		@numWOStatus=numWOStatus
	FROM 
		WorkOrder 
	WHERE 
		numWOId=@numWOId
		AND [WorkOrder].[numDomainID] = @numDomainID

	--IF NOT COMPLETED THEN DELETE
	IF @numWOStatus <> 23184
	BEGIN
		IF @numOppId>0
		BEGIN
			SET @Description = 'SO-WO Work Order Deleted (Qty:' + CAST(@numQtyItemsReq AS VARCHAR(10)) + ')'
		END
		ELSE
		BEGIN
			SET @Description = 'Work Order Deleted (Qty:' + CAST(@numQtyItemsReq AS VARCHAR(10)) + ')'
		END

		--GET CURRENT INEVENTORY STATUS
		SELECT
			@onHand = ISNULL(numOnHand,0),
			@onOrder = ISNULL(numOnOrder,0),
			@onAllocation = ISNULL(numAllocation,0),
			@onBackOrder = ISNULL(numBackOrder,0)
		FROM
			WareHouseItems
		WHERE
			numWareHouseItemID = @numWareHouseItemID

		--CHANGE INVENTORY
		IF @onOrder < @numQtyItemsReq
			SET @onOrder = 0
		ELSE
			SET @onOrder = @onOrder - @numQtyItemsReq

		EXEC USP_UpdateInventoryAndTracking 
		@numOnHand=@onHand,
		@numOnAllocation=@onAllocation,
		@numOnBackOrder=@onBackOrder,
		@numOnOrder=@onOrder,
		@numWarehouseItemID=@numWareHouseItemID,
		@numReferenceID=@numReferenceID,
		@tintRefType=@tintRefType,
		@numDomainID=@numDomainID,
		@numUserCntID=@numUserCntID,
		@Description=@Description 


		DECLARE @TEMP TABLE
		(
			ID INT IDENTITY(1,1),
			numWarehouseItemID NUMERIC(18,0),
			numQtyItemsReq NUMERIC(18,0)
		)

		INSERT INTO @TEMP SELECT numWareHouseItemId,numQtyItemsReq FROM WorkOrderDetails WHERE numWOId=@numWOId AND ISNULL(numWareHouseItemId,0) > 0

		DECLARE @i INT = 1
		DECLARE @COUNT INT

		SELECT @COUNT = COUNT(*) FROM @TEMP

		WHILE @i <= @COUNT
		BEGIN
			SELECT @numWareHouseItemID=ISNULL(numWarehouseItemID,0),@numQtyItemsReq=ISNULL(numQtyItemsReq,0) FROM @TEMP WHERE ID = @i

			IF @numOppId>0
			BEGIN
				SET @Description = 'Items Allocation Reverted SO-WO Work Order Deleted (Qty:' + CAST(@numQtyItemsReq AS VARCHAR(10)) + ')'
			END
			ELSE
			BEGIN
				SET @Description = 'Items Allocation Reverted Work Order Deleted (Qty:' + CAST(@numQtyItemsReq AS VARCHAR(10)) + ')'
			END

			--GET CURRENT INEVENTORY STATUS
			SELECT
				@onHand = ISNULL(numOnHand,0),
				@onOrder = ISNULL(numOnOrder,0),
				@onAllocation = ISNULL(numAllocation,0),
				@onBackOrder = ISNULL(numBackOrder,0)
			FROM
				WareHouseItems
			WHERE
				numWareHouseItemID = @numWareHouseItemID

			--CHANGE INVENTORY
			IF @numQtyItemsReq >= @onBackOrder 			BEGIN				SET @numQtyItemsReq = @numQtyItemsReq - @onBackOrder				SET @onBackOrder = 0                            				IF (@onAllocation - @numQtyItemsReq >= 0)						SET @onAllocation = @onAllocation - @numQtyItemsReq										SET @onHand = @onHand + @numQtyItemsReq                                                          			END                                            			ELSE IF @numQtyItemsReq < @onBackOrder 			BEGIN				SET @onBackOrder = @onBackOrder - @numQtyItemsReq			END

			--UPDATE INVENTORY AND WAREHOUSE TRACKING
			EXEC USP_UpdateInventoryAndTracking 
			@numOnHand=@onHand,
			@numOnAllocation=@onAllocation,
			@numOnBackOrder=@onBackOrder,
			@numOnOrder=@onOrder,
			@numWarehouseItemID=@numWareHouseItemID,
			@numReferenceID=@numReferenceID,
			@tintRefType=@tintRefType,
			@numDomainID=@numDomainID,
			@numUserCntID=@numUserCntID,
			@Description=@Description 

			SET @i = @i + 1
		END

		DELETE FROM WorkOrderDetails WHERE numWOId=@numWOId 

		DELETE FROM WorkOrder WHERE numWOId=@numWOId AND [numDomainID] = @numDomainID
	END
COMMIT
END TRY
BEGIN CATCH
  -- Whoops, there was an error
  IF @@TRANCOUNT > 0
     ROLLBACK

  -- Raise an error with the details of the exception
  DECLARE @ErrMsg nvarchar(4000), @ErrSeverity int
  SELECT @ErrMsg = ERROR_MESSAGE(),
         @ErrSeverity = ERROR_SEVERITY()

  RAISERROR(@ErrMsg, @ErrSeverity, 1)
END CATCH
END
/****** Object:  StoredProcedure [dbo].[USP_DelInboxItems]    Script Date: 07/26/2008 16:15:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_delinboxitems')
DROP PROCEDURE usp_delinboxitems
GO
CREATE PROCEDURE [dbo].[USP_DelInboxItems]       
@numEmailHstrID as numeric(9)  ,           
@mode as bit,
@numDomainID AS NUMERIC(18,0),
@numUserCntID AS NUMERIC(18,0)            
as              
   
if @mode =1  
begin             
/*Bug reported by Audiogenx that they can not delete mails, cause: Email was added to correspondance of Project*/
 DELETE FROM dbo.Correspondence WHERE numEmailHistoryID = @numEmailHstrID 

 delete emailHistory               
 where numEmailHstrID=@numEmailHstrID         
       
 delete EmailHStrToBCCAndCC               
 where numEmailHstrID=@numEmailHstrID       
       
 delete EmailHstrAttchDtls               
 where numEmailHstrID=@numEmailHstrID      
 end  
else  
begin  
 --OLD NODE ID REQUIRED TO GET FOLDER NAME WHERE EMAIL WAS BEFORE MOVED TO DELETED FOLDER
 --BECAUSE TO DELETE EMAIL IN GAMIL FOLDER NAME IS REQUIRED
 UPDATE EmailHistory SET numOldNodeID=numNodeId WHERE numEmailHstrID=@numEmailHstrID 
 update emailHistory set tinttype=3,numNodeid=ISNULL((SELECT numNodeID FROM InboxTreeSort WHERE numDomainID = @numDomainID AND numUserCntID=@numUserCntID AND bitSystem=1 AND numFixID=2),2) where numEmailHstrID=@numEmailHstrID     
end
GO
/****** Object:  StoredProcedure [dbo].[USP_EcampaginDTls]    Script Date: 06/04/2009 16:21:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj  
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_ecampagindtls')
DROP PROCEDURE usp_ecampagindtls
GO
CREATE PROCEDURE [dbo].[USP_EcampaginDTls]  
@numECampaignID as numeric(9)=0,  
@byteMode as tinyint=0  
as  
  
if @byteMode=0  
begin  
select vcECampName,txtDesc,ISNULL(dtStartTime,GETDATE()) dtStartTime,ISNULL(fltTimeZone,0) fltTimeZone,ISNULL(tintTimeZoneIndex,0) tintTimeZoneIndex, ISNULL([tintFromField],1) tintFromField,ISNULL([numFromContactID],0) numFromContactID from ECampaign   
where numECampaignID=@numECampaignID  
select numECampDTLId,numEmailTemplate,numActionItemTemplate, tintDays, tintWaitPeriod, ISNULL(numFollowUpID,0) numFollowUpID from ECampaignDTLs  
where numECampID=@numECampaignID  
end  
if @byteMode=1  
begin 
	UPDATE ECampaign SET bitDeleted=1 WHERE numECampaignID=@numECampaignID
end
/****** Object:  StoredProcedure [dbo].[USP_ECampaignHstr]    Script Date: 07/26/2008 16:15:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_ecampaignhstr')
DROP PROCEDURE usp_ecampaignhstr
GO
CREATE PROCEDURE [dbo].[USP_ECampaignHstr] 
    @numConatctID AS NUMERIC(9),
    @numConEmailCampID NUMERIC(9)
AS 
		SELECT
			ECampaign.vcECampName,
			(CASE 
				WHEN ISNULL(ECampaign.bitDeleted,0) = 0 
				THEN CASE WHEN ISNULL(ConECampaign.bitEngaged,0) = 0 THEN 'Disengaged' ELSE 'Engaged' END
				ELSE 'Campaign Deleted'
			END) AS [Status],
			(CASE 
				WHEN ISNULL(ECampaignDTLs.numEmailTemplate,0) > 0 THEN GenericDocuments.vcDocName
				WHEN ISNULL(ECampaignDTLs.numActionItemTemplate,0) > 0 THEN tblActionItemData.TemplateName
				ELSE ''
			END) AS Template,
			ISNULL(dbo.FormatedDateTimeFromDate(DATEADD(MINUTE,ISNULL(ECampaign.fltTimeZone,0) * 60,ConECampaignDTL.dtExecutionDate),ACI.[numDomainID]),'') vcExecutionDate,
			(CASE 
				WHEN ISNULL(ConECampaignDTL.bitSend,0)=1 AND ISNULL(ConECampaignDTL.tintDeliveryStatus,0)=0 THEN 'Failed'
				WHEN ISNULL(ConECampaignDTL.bitSend,0)=1 AND ISNULL(ConECampaignDTL.tintDeliveryStatus,0)=1 THEN 'Success'
				ELSE 'Pending'
			END) AS SendStatus,
			ISNULL(dbo.FormatedDateTimeFromDate(DATEADD(MINUTE,ISNULL(ECampaign.fltTimeZone,0)*60, ConECampaignDTL.[bintSentON]),ACI.[numDomainID]),'') vcSendDate,
			ISNULL(ConECampaignDTL.bitEmailRead,0) bitEmailRead,
			(SELECT COUNT(*) FROM ConECampaignDTLLinks WHERE numConECampaignDTLID = ConECampaignDTL.numConECampDTLID) AS NoOfLinks,
			(SELECT COUNT(*) FROM ConECampaignDTLLinks WHERE ISNULL(bitClicked,0) = 1 AND numConECampaignDTLID = ConECampaignDTL.numConECampDTLID) AS NoOfLinkClicked
		FROM ConECampaignDTL
		INNER JOIN ECampaignDTLs ON ConECampaignDTL.numECampDTLID = ECampaignDTLs.numECampDTLId
		INNER JOIN ConECampaign	ON ConECampaignDTL.numConECampID = ConECampaign.numConEmailCampID
		INNER JOIN ECampaign ON	ConECampaign.numECampaignID = ECampaign.numECampaignID
		INNER JOIN [AdditionalContactsInformation] ACI ON ACI.[numContactId] = ConECampaign.[numContactID]
		LEFT JOIN GenericDocuments ON GenericDocuments.numGenericDocID = ECampaignDTLs.numEmailTemplate
		LEFT JOIN tblActionItemData ON tblActionItemData.RowID = ECampaignDTLs.numActionItemTemplate
		WHERE 
			(ConECampaign.numConEmailCampID = @numConEmailCampID OR ISNULL(@numConEmailCampID,0)=0) AND
			(ACI.numContactID = @numConatctID OR ISNULL(@numConatctID,0)=0)
		ORDER BY [numConECampDTLID] ASC 
GO
/****** Object:  StoredProcedure [dbo].[USP_ECampaignList]    Script Date: 07/26/2008 16:15:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj        
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_ecampaignlist')
DROP PROCEDURE usp_ecampaignlist
GO
CREATE PROCEDURE [dbo].[USP_ECampaignList]            
 @numUserCntID numeric(9)=0,            
 @numDomainID numeric(9)=0,              
 @SortChar char(1)='0',           
 @CurrentPage int,            
 @PageSize int,            
 @TotRecs int output,            
 @columnName as Varchar(50),            
 @columnSortOrder as Varchar(10)              
as            
            
            
               
             
            
create table #tempTable ( ID INT IDENTITY PRIMARY KEY,             
 numECampaignID numeric(9),            
 vcECampName varchar(100),            
 txtDesc text,
 NoOfAssignedUser INT,
 NoOfEmailSent INT,
 NoOfEmailOpened INT,
 NoOfLinks INT,
 NoOfLinkClicked INT        
 )            
            
            
declare @strSql as varchar(8000)            
          
          
            
set @strSql='select 
numECampaignID,          
vcECampName,          
txtDesc, 
ISNULL((SELECT COUNT(*) FROM ConECampaign WHERE numECampaignID = ECampaign.numECampaignID),0) AS NoOfAssignedUser,
ISNULL((SELECT COUNT(*) FROM ConECampaignDTL INNER JOIN ECampaignDTLs ON ConECampaignDTL.numECampDTLID = ECampaignDTLs.numECampDTLId WHERE ISNULL(ECampaignDTLs.numEmailTemplate,0) > 0 AND numECampID = ECampaign.numECampaignID AND ISNULL(bitSend,0) = 1 AND ISNULL(tintDeliveryStatus,0) = 1),0) AS NoOfEmailSent,
ISNULL((SELECT COUNT(*) FROM ConECampaignDTL INNER JOIN ECampaignDTLs ON ConECampaignDTL.numECampDTLID = ECampaignDTLs.numECampDTLId WHERE ISNULL(ECampaignDTLs.numEmailTemplate,0) > 0 AND numECampID = ECampaign.numECampaignID AND ISNULL(bitSend,0) = 1 AND ISNULL(tintDeliveryStatus,0) = 1 AND ISNULL(bitEmailRead,0) = 1),0) AS NoOfEmailOpened,
(SELECT COUNT(*) FROM ConECampaignDTLLinks WHERE numConECampaignDTLID IN (SELECT ISNULL(CECDTL.numConECampDTLID,0) FROM ConECampaignDTL CECDTL WHERE numConECampID IN (SELECT ISNULL(CEC.numConEmailCampID,0) FROM ConECampaign CEC WHERE numECampaignID = ECampaign.numECampaignID))) AS NoOfLinks,
(SELECT COUNT(*) FROM ConECampaignDTLLinks WHERE ISNULL(bitClicked,0) = 1 AND numConECampaignDTLID IN (SELECT ISNULL(CECDTL.numConECampDTLID,0) FROM ConECampaignDTL CECDTL WHERE numConECampID IN (SELECT ISNULL(CEC.numConEmailCampID,0) FROM ConECampaign CEC WHERE numECampaignID = ECampaign.numECampaignID))) AS NoOfLinkClicked
from ECampaign          
where ISNULL(bitDeleted,0) = 0 AND numDomainID= '+ convert(varchar(15),@numDomainID)           
            
if @SortChar <>'0' set @strSql=@strSql + ' And vcECampName like '''+@SortChar+'%'''          
if @columnName<>''  set @strSql=@strSql + ' ORDER BY ' + @columnName +' '+ @columnSortOrder   
  
print @strSql         
insert into #tempTable
(            
	numECampaignID,            
	vcECampName,            
	txtDesc,
	NoOfAssignedUser,
	NoOfEmailSent,
	NoOfEmailOpened,
	NoOfLinks,
	NoOfLinkClicked 
)            
exec 
(
	@strSql
)             
            
  declare @firstRec as integer             
  declare @lastRec as integer            
 set @firstRec= (@CurrentPage-1) * @PageSize            
     set @lastRec= (@CurrentPage*@PageSize+1)            
--Don't change column sequence because its being used for Drip Campaign DropDown in Contact Details
select numECampaignID,vcECampName,txtDesc,NoOfAssignedUser,NoOfEmailSent,NoOfEmailOpened,NoOfLinks,NoOfLinkClicked from #tempTable where ID > @firstRec and ID < @lastRec            
set @TotRecs=(select count(*) from #tempTable)            
drop table #tempTable
GO
/****** Object:  StoredProcedure [dbo].[USP_EditCntInfo]    Script Date: 07/26/2008 16:15:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created By Anoop Jayaraj                                 
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_editcntinfo')
DROP PROCEDURE usp_editcntinfo
GO
CREATE PROCEDURE [dbo].[USP_EditCntInfo]                                                
@numContactID NUMERIC(9) ,    
@numDomainID as numeric(9)  
 ,  
@ClientTimeZoneOffset as int                     
                                              
AS                                                
BEGIN                                                
                                                
  SELECT                        
   A.numContactId, A.vcGivenName, A.vcFirstName,                                                 
                      A.vcLastName, D.numDivisionID, C.numCompanyId,                                                 
                      C.vcCompanyName, D.vcDivisionName, D.numDomainID,  
					  (select vcdata from listdetails where numListItemID = C.numCompanyType) as vcCompanyTypeName,                                               
                      A.numPhone, A.numPhoneExtension, A.vcEmail,  A.numTeam,                                               
                      A.vcFax, A.numContactType,  A.charSex, A.bintDOB,                                                 
                      A.vcPosition, A.txtNotes, A.numCreatedBy,                                                
                      A.numCell,A.NumHomePhone,A.vcAsstFirstName,                                                
                      A.vcAsstLastName,A.numAsstPhone,A.numAsstExtn,                                                
                      A.vcAsstEmail,A.charSex, A.vcDepartment,                                                 
                      AD.vcStreet AS vcpStreet,                                            
                      AD.vcCity AS vcPCity, 
                      dbo.fn_GetState(AD.numState) as State, dbo.fn_GetListName(AD.numCountry,0) as Country,
                      AD.vcPostalCode AS vcPPostalCode,A.bitOptOut,vcDepartment,                                                
        A.numManagerID, A.vcCategory  , dbo.fn_GetContactName(A.numCreatedBy)+' '+ convert(varchar(20),dateadd(minute,-@ClientTimeZoneOffset,A.bintCreatedDate)) CreatedBy,                                            
          dbo.fn_GetContactName(A.numModifiedBy)+' '+ convert(varchar(20),dateadd(minute,-@ClientTimeZoneOffset,A.bintModifiedDate)) ModifiedBy,vcTitle,vcAltEmail,                                            
  dbo.fn_GetContactName(A.numRecOwner) as RecordOwner,                                      
  C.vcCompanyName,C.vcWebSite, 
--  dbo.fn_GetState(AddC.vcPState) as State, dbo.fn_GetListName(AddC.vcPCountry,0) as Country,
   A.numEmpStatus,                          
  isnull((select top 1 case when bitEngaged=0 then 'Disengaged' when bitEngaged=1 then 'Engaged' end as ECampStatus  from ConECampaign                          
where  numContactID=@numContactID order by numConEmailCampID desc),'-') as CampStatus ,                        
dbo.GetECamLastAct(@numContactID) as Activity ,       
(select  count(*) from dbo.GenericDocuments   where numRecID=@numContactID and  vcDocumentSection='C') as DocumentCount,
(SELECT count(*)from CompanyAssociations where numDivisionID=@numContactID and bitDeleted=0 ) as AssociateCountFrom,              
(SELECT count(*)from CompanyAssociations where numAssociateFromDivisionID=@numContactID and bitDeleted=0 ) as AssociateCountTo    ,        
vcItemId ,        
vcChangeKey,              
vcCompanyName,      
tintCRMType ,isnull((select top 1 numUserId from  UserMaster where numUserDetailId=A.numContactId),0) as numUserID,
dbo.fn_GetListName(C.numNoOfEmployeesId,0) AS NoofEmp
FROM         AdditionalContactsInformation A     
INNER JOIN  DivisionMaster D ON A.numDivisionId = D.numDivisionID     
INNER JOIN  CompanyInfo C ON D.numCompanyID = C.numCompanyId                      
LEFT JOIN AddressDetails AD ON  A.numDomainID = AD.numDomainID AND AD.numRecordID= A.numContactId AND AD.tintAddressOf=1 AND AD.tintAddressType=0 AND AD.bitIsPrimary=1
WHERE     A.numContactId = @numContactID and A.numDomainID=@numDomainID                  
                                   
END
GO
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_EmailHistory_Archive' ) 
    DROP PROCEDURE USP_EmailHistory_Archive
GO
CREATE PROCEDURE USP_EmailHistory_Archive
	@numDomainID NUMERIC(18,0),
	@numUserCntID NUMERIC(18,0),
    @numEmailHstrID AS VARCHAR(MAX)
AS 
BEGIN
	IF @numEmailHstrID = 'ALL'
	BEGIN
		--Moves all emails which are more than 180 days old to Email Archive folder
		UPDATE t1
			SET t1.numNodeId = ISNULL((SELECT numNodeID FROM InboxTreeSort WHERE numDomainID = t1.numDomainID AND numUserCntID = t1.numUserCntID AND ISNULL(numFixID,0) = 2 AND ISNULL(bitSystem,0) = 1),0),bitArchived=1
		FROM
			EmailHistory t1
		WHERE
			CAST(t1.dtReceivedOn AS DATE) < CAST(DATEADD(D,-180,GETDATE()) AS DATE) AND
			numNodeId = (SELECT numNodeId FROM InboxTreeSort WHERE numDomainID=t1.numDomainID AND numUserCntID=t1.numUserCntID AND bitSystem = 1 AND numFixID = 1) AND
			ISNULL(t1.bitArchived,0) = 0
	END
	ELSE
	BEGIN
		DECLARE @numEmailArchiveNodeID NUMERIC(18,0)
		DECLARE @strSQL AS VARCHAR(MAX)

		SELECT @numEmailArchiveNodeID=ISNULL(numNodeID,0) FROM InboxTreeSort WHERE numDomainID = @numDomainID AND numUserCntID = @numUserCntID AND ISNULL(numFixID,0) = 2 AND ISNULL(bitSystem,0) = 1

		SET @strSQL = 'UPDATE EmailHistory SET bitArchived=1, numNodeId = ' + CAST(@numEmailArchiveNodeID AS VARCHAR(18)) + ' WHERE numEmailHstrID IN (' + @numEmailHstrID + ')'

		EXEC(@strSQL)
	END
END





GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_EmailHistory_DeleteArchived' ) 
    DROP PROCEDURE USP_EmailHistory_DeleteArchived
GO
CREATE PROCEDURE USP_EmailHistory_DeleteArchived
	@numDomainID NUMERIC(18,0),
	@numUserCntID NUMERIC(18,0),
    @dtFrom AS DATE,
	@dtTo AS DATE
AS 
BEGIN
	DECLARE @ErrorMessage NVARCHAR(4000);SET @ErrorMessage=''
	DECLARE @ErrorSeverity INT;
	DECLARE @ErrorState INT;
	DECLARE @numEmailArchiveNodeID NUMERIC(18,0)
	DECLARE @strSQL AS VARCHAR(MAX)

	BEGIN TRANSACTION
	BEGIN TRY
		--numFixID = 7 = Email Archive
		SELECT @numEmailArchiveNodeID=ISNULL(numNodeID,0) FROM InboxTreeSort WHERE numDomainID = @numDomainID AND numUserCntID = @numUserCntID AND numFixID = 2 AND ISNULL(bitSystem,0) = 1

		DELETE FROM dbo.Correspondence WHERE numEmailHistoryID IN (SELECT ISNULL(numEmailHstrID,0) FROM EmailHistory WHERE numDomainID = @numDomainID AND numUserCntId = @numUserCntID AND numNodeId = @numEmailArchiveNodeID AND CAST(dtReceivedOn AS DATE) >= @dtFrom AND CAST(dtReceivedOn AS DATE) <= @dtTo)

		DELETE FROM EmailHStrToBCCAndCC WHERE numEmailHstrID IN (SELECT ISNULL(numEmailHstrID,0) FROM EmailHistory WHERE numDomainID = @numDomainID AND numUserCntId = @numUserCntID AND numNodeId = @numEmailArchiveNodeID AND CAST(dtReceivedOn AS DATE) >= @dtFrom AND CAST(dtReceivedOn AS DATE) <= @dtTo)       
       
		DELETE FROM EmailHstrAttchDtls WHERE numEmailHstrID IN (SELECT ISNULL(numEmailHstrID,0) FROM EmailHistory WHERE numDomainID = @numDomainID AND numUserCntId = @numUserCntID AND numNodeId = @numEmailArchiveNodeID AND CAST(dtReceivedOn AS DATE) >= @dtFrom AND CAST(dtReceivedOn AS DATE) <= @dtTo)
	
		DELETE EmailHistory WHERE numEmailHstrID IN (SELECT ISNULL(numEmailHstrID,0) FROM EmailHistory WHERE numDomainID = @numDomainID AND numUserCntId = @numUserCntID AND numNodeId = @numEmailArchiveNodeID AND CAST(dtReceivedOn AS DATE) >= @dtFrom AND CAST(dtReceivedOn AS DATE) <= @dtTo)                   
	END TRY
	BEGIN CATCH
		SELECT @ErrorMessage = ERROR_MESSAGE(), @ErrorSeverity = ERROR_SEVERITY(), @ErrorState = ERROR_STATE();

		IF @@TRANCOUNT > 0
			ROLLBACK TRANSACTION;

		-- Use RAISERROR inside the CATCH block to return error
		-- information about the original error that caused
		-- execution to jump to the CATCH block.
		RAISERROR (@ErrorMessage,@ErrorSeverity,@ErrorState);
	END CATCH

	IF @@TRANCOUNT > 0
		COMMIT TRANSACTION
END



GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_EmailHistory_GetByIDsString' ) 
    DROP PROCEDURE USP_EmailHistory_GetByIDsString
GO
CREATE PROCEDURE USP_EmailHistory_GetByIDsString
    @vcEmailHstrID AS VARCHAR(MAX),
	@numDomainID AS NUMERIC(18,0),
	@numUserCntID AS NUMERIC(18,0)
AS 
BEGIN
	DECLARE @strSQL AS VARCHAR(MAX)

	SET @strSQL = 'SELECT numEmailHstrID,numUid,vcNodeName,chrSource FROM EmailHistory LEFT JOIN InboxTreeSort ON EmailHistory.numOldNodeID = InboxTreeSort.numNodeID WHERE EmailHistory.numDomainID=' + CAST(@numDomainID AS VARCHAR(9)) + ' AND EmailHistory.numUserCntId=' + CAST(@numUserCntID AS VARCHAR(9)) + ' AND EmailHistory.numEmailHstrID IN (' + @vcEmailHstrID + ')'
	
	EXEC(@strSQL)
END



GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_EmailHistory_MarkAsSpam' ) 
    DROP PROCEDURE USP_EmailHistory_MarkAsSpam
GO
CREATE PROCEDURE USP_EmailHistory_MarkAsSpam
    @vcEmailHstrID AS VARCHAR(MAX)
AS 
BEGIN
	DECLARE @strSQL AS VARCHAR(MAX)

	SELECT numUserCntId FROM EmailHistory

	SET @strSQL = 'DELETE FROM EmailHStrToBCCAndCC WHERE numEmailHstrID IN (' + @vcEmailHstrID + ')'
	EXEC(@strSQL)
    
	SET @strSQL = 'DELETE FROM EmailHstrAttchDtls WHERE numEmailHstrID IN (' + @vcEmailHstrID + ')'
	EXEC(@strSQL)   

	SET @strSQL = 'DELETE FROM EmailHistory WHERE numEmailHstrID IN (' + @vcEmailHstrID + ')'
	EXEC(@strSQL)
END
/****** Object:  StoredProcedure [dbo].[USP_MoveCopyItems]    Script Date: 07/26/2008 16:20:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_EmailHistory_MoveCopyIDs')
DROP PROCEDURE USP_EmailHistory_MoveCopyIDs
GO
CREATE PROCEDURE [dbo].[USP_EmailHistory_MoveCopyIDs]                      
@numEmailHstrID AS NUMERIC(18,0),
@numNodeId AS NUMERIC(9),
@ModeType AS BIT,
@numUID AS NUMERIC(18,0)
AS          
--COPY
IF @ModeType = 1
BEGIN
	DECLARE @NewEmailHstrID INTEGER;   

	--MAKE A COPY IN EmailHistory TABLE
		INSERT INTO EmailHistory
		(
			vcSubject,vcBody,bintCreatedOn,numNoofTimes,vcItemId,vcChangeKey,bitIsRead,vcSize,bitHasAttachments,dtReceivedOn,
			tintType,chrSource,vcCategory,vcBodyText,numDomainID,numUserCntId,numUid,numNodeId,vcFrom,vcTO,numEMailId
		)                        
		SELECT 
			vcSubject,vcBody,bintCreatedOn,numNoofTimes,vcItemId,vcChangeKey,bitIsRead,vcSize,bitHasAttachments,dtReceivedOn,
			tintType,chrSource,vcCategory,vcBodyText,numDomainID,numUserCntId,@numUID ,@numNodeId,vcFrom,vcTO,numEMailId            
		FROM 
			EmailHistory 
		WHERE 
			numEmailHstrID=@numEmailHstrID              
                  
		SELECT @NewEmailHstrID = SCOPE_IDENTITY();              
        
		--MAKE A COPY IN EmailHStrToBCCAndCC TABLE
		INSERT INTO EmailHStrToBCCAndCC 
		(
			numEmailHstrID,vcName,tintType,numDivisionId,numEmailId
		)              
		SELECT 
			@NewEmailHstrID,vcName,tintType,numDivisionId,numEmailId 
		FROM  
			EmailHStrToBCCAndCC 
		WHERE 
			numEmailHstrID=@numEmailHstrID              

		--MAKE A COPY IN EmailHstrAttchDtls TABLE                    
		INSERT INTO EmailHstrAttchDtls 
		( 
			numEmailHstrID,vcFileName,vcAttachmentItemId,vcAttachmentType
		)              
		SELECT  
			@NewEmailHstrID,vcFileName,vcAttachmentItemId,vcAttachmentType 
		FROM 
			EmailHstrAttchDtls 
		WHERE 
			numEmailHstrID=@numEmailHstrID 
	
END
--MOVE
ELSE
BEGIN
	UPDATE emailHistory SET numNodeId=@numNodeId, numUid = @numUID WHERE numEmailHstrID = @numEmailHstrID
END
GO
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_EmailHistory_Restore' ) 
    DROP PROCEDURE USP_EmailHistory_Restore
GO
CREATE PROCEDURE USP_EmailHistory_Restore
	@numDomainID NUMERIC(18,0),
	@numUserCntID NUMERIC(18,0),
    @numEmailHstrID AS VARCHAR(MAX)
AS 
BEGIN
	
	DECLARE @numInboxNodeID NUMERIC(18,0)
	DECLARE @strSQL AS VARCHAR(MAX)

	SELECT @numInboxNodeID=ISNULL(numNodeID,0) FROM InboxTreeSort WHERE numDomainID = @numDomainID AND numUserCntID = @numUserCntID AND numFixID = 1 AND ISNULL(bitSystem,0) = 1

	SET @strSQL = 'UPDATE EmailHistory SET bitArchived=0, numNodeId = ' + CAST(@numInboxNodeID AS VARCHAR(18)) + ' WHERE numEmailHstrID IN (' + @numEmailHstrID + ')'

	EXEC(@strSQL)
END



GO
IF EXISTS(SELECT * FROM sysobjects SYSOBJ WHERE SYSOBJ.xtype='p'AND SYSOBJ.NAME ='USP_GET_DROPDOWN_DATA_FOR_IMPORT')
DROP PROCEDURE USP_GET_DROPDOWN_DATA_FOR_IMPORT
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- USP_GET_DROPDOWN_DATA_FOR_IMPORT 75,1
CREATE PROCEDURE USP_GET_DROPDOWN_DATA_FOR_IMPORT
(
	@intImportFileID    BIGINT,
	@numDomainID		BIGINT
)
AS 
BEGIN
	
	DECLARE @numFormID AS NUMERIC
	
	SELECT @numFormID = numMasterID  FROM dbo.Import_File_Master  WHERE intImportFileID=@intImportFileID
	
	
	SELECT ROW_NUMBER() OVER (ORDER BY intImportFileID) AS [ID],* 
			INTO #tempDropDownData FROM(
					SELECT  PARENT.intImportFileID,
							vcImportFileName,
							PARENT.numDomainID,
							DFFM.numFieldId [numFormFieldId],
							DFFM.vcAssociatedControlType,
							DYNAMIC_FIELD.vcDbColumnName,
							DFFM.vcFieldName [vcFormFieldName],
							FIELD_MAP.intMapColumnNo,
							DYNAMIC_FIELD.numListID,
							0 AS [bitCustomField]					
					FROM Import_File_Master PARENT
					INNER JOIN Import_File_Field_Mapping FIELD_MAP ON PARENT.intImportFileID = FIELD_MAP.intImportFileID
					--INNER JOIN dbo.DynamicFormFieldMaster DYNAMIC_FIELD ON FIELD_MAP.numFormFieldID = DYNAMIC_FIELD.numFormFieldId
					INNER JOIN dbo.DycFormField_Mapping DFFM ON FIELD_MAP.numFormFieldID = DFFM.numFieldId
					INNER JOIN dbo.DycFieldMaster DYNAMIC_FIELD ON DYNAMIC_FIELD.numFieldId = DFFM.numFieldId
					WHERE DFFM.vcAssociatedControlType ='SelectBox'
					  AND PARENT.intImportFileID = @intImportFileID
					  AND PARENT.numDomainID = @numDomainID   
					  AND DFFM.numFormID = @numFormID
					
					UNION ALL
					
						  SELECT    FIELD_MAP.intImportFileID AS [intImportFileID],
									cfm.Fld_label  AS [vcImportFileName],
									numDomainID AS [numDomainID],
									CFm.Fld_id AS [numFormFieldId],
									CASE WHEN CFM.Fld_type = 'Drop Down List Box'
										 THEN 'SelectBox'
										 ELSE CFM.Fld_type 
									END	 AS [vcAssociatedControlType],
									cfm.Fld_label AS [vcDbColumnName],
									cfm.Fld_label AS [vcFormFieldName],
									FIELD_MAP.intMapColumnNo  AS [intMapColumnNo],
									CFM.numlistid AS [numListID],
									1 AS [bitCustomField]
						  FROM      dbo.CFW_Fld_Master CFM
						  INNER JOIN Import_File_Field_Mapping FIELD_MAP ON CFM.Fld_id = FIELD_MAP.numFormFieldID		
						  WHERE FIELD_MAP.intImportFileID = @intImportFileID     
						    AND numDomainID = @numDomainID) TABLE1 WHERE  vcAssociatedControlType = 'SelectBox' 
                    
	SELECT * FROM #tempDropDownData   
	
	DECLARE @intRow AS INT
	SELECT @intRow = COUNT(*) FROM #tempDropDownData
	PRINT @intRow
	
	DECLARE @intCnt AS INT
	DECLARE @numListIDValue AS BIGINT
	DECLARE @numFormFieldIDValue AS BIGINT
	DECLARE @bitCustomField AS BIT
		
	DECLARE @strSQL AS NVARCHAR(MAX)
	DECLARE @vcTableNames AS VARCHAR(MAX) 
	DECLARE @strDBColumnName AS NVARCHAR(1000)
	
	SET @intCnt = 0
	SET @strSQL = '' 
	SET @vcTableNames = ''

	CREATE TABLE #tempGroups
	(
		[Key] NUMERIC(9),
		[Value] VARCHAR(100)
	) 	
	INSERT INTO #tempGroups([Key],[Value]) 
	SELECT 0 AS [Key],'Groups' AS [Value]
	UNION ALL
	SELECT 1 AS [Key],'Prospects' AS [Value]
	UNION ALL
	SELECT 1 AS [Key],'Prospect' AS [Value]
	UNION ALL
	SELECT 1 AS [Key],'Pros' AS [Value]
	UNION ALL
	SELECT 2 AS [Key],'Accounts' AS [Value]
	UNION ALL
	SELECT 2 AS [Key],'Account' AS [Value]
	UNION ALL
	SELECT 2 AS [Key],'Acc' AS [Value]

	--SELECT * FROM #tempGroups
	
	WHILE(@intCnt < @intRow   )
		BEGIN
			SET @intCnt = @intCnt + 1
				
			SELECT  
					@numListIDValue = numListID,
					@strDBColumnName = vcDbColumnName, 
					@numFormFieldIDValue = numFormFieldID,
					@bitCustomField = bitCustomField 	 
			FROM #tempDropDownData 
			WHERE ID = @intCnt
			  AND intImportFileID = @intImportFileID
			
			PRINT 'BIT'
			PRINT @bitCustomField
			PRINT @strDBColumnName
			SELECT @strSQL = CASE  
								WHEN @strDBColumnName = 'numItemGroup'
								THEN @strSQL + 'SELECT ITEM_GROUP.numItemGroupID AS [Key],ITEM_GROUP.vcItemGroup AS [Value] FROM dbo.ItemGroups ITEM_GROUP WHERE ITEM_GROUP.numDomainID = ' + CONVERT(VARCHAR,@numDomainID)
								
								WHEN @strDBColumnName = 'tintCRMType'
								THEN @strSQL + 'SELECT DISTINCT [Key],[Value] FROM #tempGroups '
								
								WHEN @strDBColumnName = 'vcLocation'
								THEN @strSQL + 'SELECT numWLocationID AS [Key],vcLocation AS [Value] FROM dbo.WarehouseLocation WHERE numDomainID = ' + CONVERT(VARCHAR,@numDomainID)
								
								WHEN @strDBColumnName = 'vcExportToAPI'
								THEN @strSQL + 'SELECT DISTINCT WebApiId AS [Key],vcProviderName AS [Value] FROM dbo.WebAPI '
								
								WHEN @strDBColumnName = 'numCategoryID'
								THEN @strSQL + 'SELECT numCategoryID AS [Key], vcCategoryName AS [Value] FROM dbo.Category WHERE numDomainID =' + CONVERT(VARCHAR,@numDomainID)
								
								WHEN @strDBColumnName = 'numGrpID'
								THEN @strSQL + 'SELECT GRP.numGrpID AS [Key],GRP.vcGrpName AS [Value] FROM dbo.Groups GRP ' 
											    			    
								WHEN @strDBColumnName = 'numWareHouseID' --OR @strDBColumnName = 'numWareHouseItemID'															  
								THEN @strSQL + 'SELECT WAREHOUSE.numWareHouseID AS [Key],WAREHOUSE.vcWareHouse AS [Value] FROM dbo.Warehouses WAREHOUSE WHERE WAREHOUSE.numDomainID = '+ CONVERT(VARCHAR,@numDomainID) 
								
								WHEN @strDBColumnName = 'numWareHouseItemID'
								THEN @strSQL + 'SELECT WI.numWareHouseItemID AS [Key],W.vcWareHouse AS [Value] FROM dbo.WareHouseItems WI INNER JOIN dbo.Warehouses W ON WI.numWareHouseID = W.numWareHouseID WHERE W.numDomainID = '+ CONVERT(VARCHAR,@numDomainID)
												
								WHEN @strDBColumnName = 'numChildItemID' OR @strDBColumnName = 'numItemKitID'
								THEN @strSQL + ' SELECT ITEM.numItemCode AS [Key],ITEM.vcItemName AS [Value] FROM dbo.Item ITEM WHERE ITEM.numDomainID = '+ CONVERT(VARCHAR,@numDomainID)
								
								WHEN (@strDBColumnName = 'numUOMId' OR @strDBColumnName = 'numPurchaseUnit' OR @strDBColumnName = 'numSaleUnit' OR @strDBColumnName = 'numBaseUnit') 
								THEN @strSQL + '  SELECT numUOMId AS [Key], vcUnitName AS [Value] FROM dbo.UOM
												  JOIN Domain d ON dbo.UOM.numDomainID = d.numDomainID 
												  WHERE dbo.UOM.numDomainId = ' + CONVERT(VARCHAR(10),@numDomainID) +' 
												  AND bitEnabled=1 
												  AND dbo.UOM.tintUnitType=(CASE WHEN ISNULL(d.charUnitSystem,''E'')=''E'' THEN 1 WHEN d.charUnitSystem=''M'' THEN 2 END) '	
								
								WHEN (@strDBColumnName = 'numIncomeChartAcntId' OR @strDBColumnName = 'numAssetChartAcntId' OR @strDBColumnName = 'numCOGsChartAcntId' OR @strDBColumnName = 'numDefaultExpenseAccountID')
								THEN @strSQL + '  SELECT numAccountId AS [Key], LTRIM(RTRIM(REPLACE(vcAccountName,vcNumber,''''))) AS [Value] FROM dbo.Chart_Of_Accounts WHERE numDomainID= ' + CONVERT(VARCHAR(10),@numDomainID)

								WHEN @strDBColumnName = 'numCampaignID'
								THEN @strSQL + '  SELECT numCampaignID AS [Key], vcCampaignName AS [Value] FROM  CampaignMaster WHERE CampaignMaster.numDomainID = ' + CONVERT(VARCHAR(10),@numDomainID)
								
								WHEN @strDBColumnName = 'numVendorID'
								THEN @strSQL + '  SELECT DM.numDivisionID AS [Key], CI.vcCompanyName AS [Value] FROM dbo.CompanyInfo CI INNER JOIN dbo.DivisionMaster DM ON DM.numCompanyID = CI.numCompanyId 
												  WHERE DM.numDomainID = ' + CONVERT(VARCHAR(10),@numDomainID)
								
								WHEN @strDBColumnName = 'numAssignedTo' OR @strDBColumnName = 'numRecOwner' 
								THEN @strSQL + '  SELECT A.numContactID AS [Key], A.vcFirstName+'' ''+A.vcLastName AS [Value] from UserMaster UM join AdditionalContactsInformation A  on UM.numUserDetailId=A.numContactID  where UM.numDomainID = A.numDomainID AND UM.numDomainID = ' + CONVERT(VARCHAR,@numDomainID)	
								
------								 SELECT A.numContactID,A.vcFirstName+' '+A.vcLastName as vcUserName from UserMaster UM         
------								 join AdditionalContactsInformation A        
------								 on UM.numUserDetailId=A.numContactID          
------								 where UM.numDomainID=@numDomainID and UM.numDomainID=A.numDomainID   
							
								WHEN @strDBColumnName = 'numTermsID'
								THEN @strSQL + 'SELECT DISTINCT numTermsID AS [Key],vcTerms AS [Value] FROM BillingTerms 
												WHERE numDomainID = ' + CONVERT(VARCHAR,@numDomainID) 
												
								WHEN @strDBColumnName = 'numShipState' OR @strDBColumnName = 'numBillState' OR @strDBColumnName = 'numState' OR @strDBColumnName = 'State'
								THEN @strSQL + '  SELECT numStateID AS [Key], vcState AS [Value] FROM State where numDomainID=' + CONVERT(VARCHAR,@numDomainID)
								
								WHEN @strDBColumnName = 'charItemType'
								THEN @strSQL + ' SELECT ''A'' AS [Key], ''asset'' AS [Value]
											     UNION 				
											     SELECT ''P'' AS [Key], ''inventory item'' AS [Value]
											     UNION 				
											     SELECT ''S'' AS [Key], ''service'' AS [Value]
											     UNION 				
											     SELECT ''N'' AS [Key], ''non-inventory item'' AS [Value]'	
								
								WHEN @strDBColumnName = 'tintRuleType'
								THEN @strSQL + ' SELECT 1 AS [Key], ''Deduct from List price'' AS [Value]
											     UNION 				
											     SELECT 2 AS [Key], ''Add to Primary Vendor Cost'' AS [Value]
											     UNION 				
											     SELECT 3 AS [Key], ''Named Price'' AS [Value]'	
											     
								WHEN @strDBColumnName = 'tintDiscountType'
								THEN @strSQL + ' SELECT 1 AS [Key], ''Percentage'' AS [Value]
											     UNION 				
											     SELECT 2 AS [Key], ''Flat Amount'' AS [Value]'												     
											     																		    
								ELSE 
									 @strSQL + ' SELECT LIST.numListItemID AS [Key],vcData AS [Value] FROM ListDetails LIST LEFT JOIN listorder LIST_ORDER ON LIST.numListItemID = LIST_ORDER.numListItemID AND LIST_ORDER.numDomainId = ' + CONVERT(VARCHAR,@numDomainID) + ' WHERE ( LIST.numDomainID = ' + CONVERT(VARCHAR,@numDomainID) + ' OR LIST.constFlag = 1) AND LIST.numListID = ' + CONVERT(VARCHAR,@numListIDValue) + ' AND  (LIST.numDomainID = ' + CONVERT(VARCHAR,@numDomainID) + ' OR constFlag = 1 )  ORDER BY intSortOrder '
						     END						
						     
			SET @vcTableNames = @vcTableNames + CONVERT(VARCHAR,@numFormFieldIDValue) + '_' + @strDBColumnName + ',' 
		END
	
	PRINT @vcTableNames
	PRINT @strSQL	
	
	SELECT @vcTableNames AS TableNames
	EXEC SP_EXECUTESQL @strSQL		
	
	DROP TABLE #tempDropDownData
	DROP TABLE #tempGroups
END
/*
-- Added Support for Bills in AP Aging summary,by chintan
*/

-- [dbo].[USP_GetAccountPayableAging] 1,null 6719
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getaccountpayableaging')
DROP PROCEDURE usp_getaccountpayableaging
GO
set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go

CREATE PROCEDURE [dbo].[USP_GetAccountPayableAging]
    (
      @numDomainId AS NUMERIC(9) = 0,
      @numDivisionId AS NUMERIC(9) = NULL,
      @numAccountClass AS NUMERIC(9)=0,
	  @dtFromDate AS DATETIME,
	  @dtToDate AS DATETIME
    )
AS 
    BEGIN 
  
  		SET @dtFromDate = DATEADD(SECOND, DATEDIFF(SECOND, GETDATE(), GETUTCDATE()), @dtFromDate);
		SET @dtToDate =  DATEADD(SECOND, DATEDIFF(SECOND, GETDATE(), GETUTCDATE()), @dtToDate);

		PRINT @dtFromDate
		PRINT @dtToDate

        CREATE TABLE #TempAPRecord
            (
              [ID] [int] IDENTITY(1, 1)
                         NOT NULL,
              [numUserCntID] [numeric](18, 0) NOT NULL,
              [numOppId] [numeric](18, 0) NULL,
              [numDivisionId] [numeric](18, 0) NULL,
              [numOppBizDocsId] [numeric](18, 0) NULL,
              [DealAmount] [float] NULL,
              [numBizDocId] [numeric](18, 0) NULL,
              dtDueDate DATETIME NULL,
              [numCurrencyID] [numeric](18, 0) NULL,AmountPaid FLOAT NULL,monUnAppliedAmount MONEY NULL 
            ) ;
  
  
        CREATE TABLE #TempAPRecord1
            (
              [numID] [numeric](18, 0) IDENTITY(1, 1)
                                       NOT NULL,
              [numUserCntID] [numeric](18, 0) NOT NULL,
              [numDivisionId] [numeric](18, 0) NULL,
              [tintCRMType] [tinyint] NULL,
              [vcCompanyName] [varchar](200) NULL,
              [vcCustPhone] [varchar](50) NULL,
              [numContactId] [numeric](18, 0) NULL,
              [vcEmail] [varchar](100) NULL,
              [numPhone] [varchar](50) NULL,
              [vcContactName] [varchar](100) NULL,
              [numThirtyDays] [numeric](18, 2) NULL,
              [numSixtyDays] [numeric](18, 2) NULL,
              [numNinetyDays] [numeric](18, 2) NULL,
              [numOverNinetyDays] [numeric](18, 2) NULL,
              [numThirtyDaysOverDue] [numeric](18, 2) NULL,
              [numSixtyDaysOverDue] [numeric](18, 2) NULL,
              [numNinetyDaysOverDue] [numeric](18, 2) NULL,
              [numOverNinetyDaysOverDue] [numeric](18, 2) NULL,
              [numCompanyID] [numeric](18, 2) NULL,
              [numDomainID] [numeric](18, 2) NULL,
              [intThirtyDaysCount] [int] NULL,
              [intThirtyDaysOverDueCount] [int] NULL,
              [intSixtyDaysCount] [int] NULL,
              [intSixtyDaysOverDueCount] [int] NULL,
              [intNinetyDaysCount] [int] NULL,
              [intOverNinetyDaysCount] [int] NULL,
              [intNinetyDaysOverDueCount] [int] NULL,
              [intOverNinetyDaysOverDueCount] [int] NULL,
              [numThirtyDaysPaid] [numeric](18, 2) NULL,
              [numSixtyDaysPaid] [numeric](18, 2) NULL,
              [numNinetyDaysPaid] [numeric](18, 2) NULL,
              [numOverNinetyDaysPaid] [numeric](18, 2) NULL,
              [numThirtyDaysOverDuePaid] [numeric](18, 2) NULL,
              [numSixtyDaysOverDuePaid] [numeric](18, 2) NULL,
              [numNinetyDaysOverDuePaid] [numeric](18, 2) NULL,
              [numOverNinetyDaysOverDuePaid] [numeric](18, 2) NULL
              ,monUnAppliedAmount MONEY NULL,numTotal MONEY NULL 
            ) ;
  
  
  
        DECLARE @AuthoritativePurchaseBizDocId AS INTEGER
        SELECT  @AuthoritativePurchaseBizDocId = isnull(numAuthoritativePurchase,
                                                        0)
        FROM    AuthoritativeBizDocs
        WHERE   numDomainId = @numDomainId ;
        
     
        INSERT  INTO [#TempAPRecord]
                (
                  numUserCntID,
                  [numOppId],
                  [numDivisionId],
                  [numOppBizDocsId],
                  [DealAmount],
                  [numBizDocId],
                  dtDueDate,
                  [numCurrencyID],AmountPaid
                )
                SELECT  @numDomainId,
                        OB.numOppId,
                        Opp.numDivisionId,
                        OB.numOppBizDocsId,
                        ISNULL(OB.monDealAmount --dbo.GetDealAmount(OB.numOppID,getutcdate(),OB.numOppBizDocsId)
                               * ISNULL(Opp.fltExchangeRate, 1), 0)
                        - ISNULL(OB.monAmountPaid
                                 * ISNULL(Opp.fltExchangeRate, 1), 0) DealAmount,
                        OB.numBizDocId,
                        DATEADD(DAY,
                                CASE WHEN Opp.bitBillingTerms = 1
                                     --THEN convert(int, ISNULL(dbo.fn_GetListItemName(isnull(Opp.intBillingDays, 0)), 0))
                                     THEN convert(int, ISNULL((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(Opp.intBillingDays,0)), 0))
                                     ELSE 0
                                END, dtFromDate) AS dtDueDate,
                        ISNULL(Opp.numCurrencyID, 0) numCurrencyID,
                        ISNULL(OB.monAmountPaid * ISNULL(Opp.fltExchangeRate, 1), 0) AmountPaid
                FROM    OpportunityMaster Opp
                        JOIN OpportunityBizDocs OB ON OB.numOppId = Opp.numOppID
                        LEFT OUTER JOIN [Currency] C ON Opp.[numCurrencyID] = C.[numCurrencyID]
                WHERE   tintOpptype = 2
                        AND tintoppStatus = 1
                --AND dtShippedDate IS NULL
                        AND opp.numDomainID = @numDomainId
                        AND OB.bitAuthoritativeBizDocs = 1
                        AND ISNULL(OB.tintDeferred, 0) <> 1
                        AND numBizDocId = @AuthoritativePurchaseBizDocId
                        AND ( Opp.numDivisionId = @numDivisionId
                              OR @numDivisionId IS NULL
                            )
                        AND (Opp.numAccountClass=@numAccountClass OR @numAccountClass=0)
						AND OB.[dtCreatedDate] BETWEEN @dtFromDate AND @dtToDate
                GROUP BY OB.numOppId,
                        Opp.numDivisionId,
                        OB.numOppBizDocsId,
                        Opp.fltExchangeRate,
                        OB.numBizDocId,
                        OB.dtCreatedDate,
                        Opp.bitBillingTerms,
                        Opp.intBillingDays,
                        OB.monDealAmount,
                        Opp.numCurrencyID,
                        OB.[dtFromDate],OB.monAmountPaid
                HAVING  ( ISNULL(OB.monDealAmount * ISNULL(Opp.fltExchangeRate,
                                                           1), 0)
                          - ISNULL(OB.monAmountPaid
                                   * ISNULL(Opp.fltExchangeRate, 1), 0) ) > 0
                UNION ALL 
		--Add Bill Amount
                SELECT  @numDomainId,
                        0 numOppId,
                        BH.numDivisionId,
                        0 numBizDocsId,
                        ISNULL(SUM(BH.monAmountDue), 0) - ISNULL(SUM(BH.monAmtPaid), 0) DealAmount,
                        0 numBizDocId,
                        BH.dtDueDate AS dtDueDate,
                        0 numCurrencyID,ISNULL(SUM(BH.monAmtPaid), 0) AS AmountPaid
                FROM    BillHeader BH
                WHERE   BH.numDomainID = @numDomainId
                        AND ( BH.numDivisionId = @numDivisionId
                              OR @numDivisionId IS NULL
                            )
                         AND ISNULL(BH.monAmountDue, 0) - ISNULL(BH.monAmtPaid, 0) > 0   
                         AND (BH.numAccountClass=@numAccountClass OR @numAccountClass=0)
						 AND BH.[dtBillDate] BETWEEN @dtFromDate AND @dtToDate
                GROUP BY BH.numDivisionId,
                        BH.dtDueDate
                HAVING  ISNULL(SUM(BH.monAmountDue), 0) - ISNULL(SUM(BH.monAmtPaid), 0) > 0
                UNION ALL
		--Add Write Check entry (As Amount Paid)
                SELECT  @numDomainId,
                        0 numOppId,
                        CD.numCustomerId as numDivisionId,
                        0 numBizDocsId,
                        0 DealAmount,
                        0 numBizDocId,
                        CH.dtCheckDate AS dtDueDate,
                        0 numCurrencyID,ISNULL(SUM(CD.monAmount), 0) AS AmountPaid
                FROM    CheckHeader CH
						JOIN  dbo.CheckDetails CD ON CH.numCheckHeaderID=CD.numCheckHeaderID
                        INNER JOIN dbo.Chart_Of_Accounts COA ON COA.numAccountId = CD.numChartAcntId
                WHERE   CH.numDomainID = @numDomainId AND CH.tintReferenceType=1
						AND COA.vcAccountCode LIKE '01020102%'
                        AND ( CD.numCustomerId = @numDivisionId
                              OR @numDivisionId IS NULL
                            )
                        AND (CH.numAccountClass=@numAccountClass OR @numAccountClass=0)
						AND CH.[dtCheckDate] BETWEEN @dtFromDate AND @dtToDate
                GROUP BY CD.numCustomerId,
                        CH.dtCheckDate
		--Show Impact of AP journal entries in report as well 
                UNION ALL
                SELECT  @numDomainId,
                        0,
                        GJD.numCustomerId,
                        0,
                        CASE WHEN GJD.numDebitAmt > 0 THEN -1 * GJD.numDebitAmt
                             ELSE GJD.numCreditAmt
                        END,
                        0,
                        GJH.datEntry_Date,
                        GJD.[numCurrencyID],0 AS AmountPaid
                FROM    dbo.General_Journal_Header GJH
                        INNER JOIN dbo.General_Journal_Details GJD ON GJH.numJournal_Id = GJD.numJournalId
                                                                      AND GJH.numDomainId = GJD.numDomainId
                        INNER JOIN dbo.Chart_Of_Accounts COA ON COA.numAccountId = GJD.numChartAcntId
                WHERE   GJH.numDomainId = @numDomainId 
                        AND COA.vcAccountCode LIKE '01020102%'
                        AND ISNULL(numOppId, 0) = 0
                        AND ISNULL(numOppBizDocsId, 0) = 0
                        AND ISNULL(GJH.numBillID,0)=0 AND ISNULL(GJH.numBillPaymentID,0)=0 AND ISNULL(GJH.numCheckHeaderID,0)=0 AND ISNULL(GJH.numReturnID,0)=0
                        AND ISNULL(GJH.numPayrollDetailID,0)=0
                         AND ( GJD.numCustomerId = @numDivisionId
                              OR @numDivisionId IS NULL
                            )
                            AND (GJH.numAccountClass=@numAccountClass OR @numAccountClass=0)
						AND [GJH].[datEntry_Date]  BETWEEN @dtFromDate AND @dtToDate
                UNION ALL
	--Add Commission Amount
                SELECT  @numDomainId,
                        OBD.numOppId numOppId,
                        D.numDivisionId,
                        BDC.numOppBizDocId numBizDocsId,
                       isnull(sum(BDC.numComissionAmount - ISNULL(PT.monCommissionAmt,0)),0)  DealAmount,
                        0 numBizDocId,
                        OBD.dtCreatedDate AS dtDueDate,
                        ISNULL(OPP.numCurrencyID, 0) numCurrencyID,0 AS AmountPaid
                FROM    BizDocComission BDC
                        JOIN OpportunityBizDocs OBD ON BDC.numOppBizDocId = OBD.numOppBizDocsId
                        join OpportunityMaster Opp on OPP.numOppId = OBD.numOppId
                        JOIN Domain D on D.numDomainID = BDC.numDomainID
                        LEFT JOIN dbo.PayrollTracking PT ON BDC.numComissionID=PT.numComissionID
                WHERE   OPP.numDomainID = @numDomainId
                        and BDC.numDomainID = @numDomainId
                        AND ( D.numDivisionId = @numDivisionId
                              OR @numDivisionId IS NULL
                            )
                            AND (Opp.numAccountClass=@numAccountClass OR @numAccountClass=0)
						AND OBD.[dtCreatedDate]  BETWEEN @dtFromDate AND @dtToDate
				--AND BDC.bitCommisionPaid=0
                        AND OBD.bitAuthoritativeBizDocs = 1
                GROUP BY D.numDivisionId,
                        OBD.numOppId,
                        BDC.numOppBizDocId,
                        OPP.numCurrencyID,
                        BDC.numComissionAmount,
                        OBD.dtCreatedDate
                HAVING  ISNULL(BDC.numComissionAmount, 0) > 0
			UNION ALL --Standalone Refund against Account Receivable
			 SELECT @numDomainId,
					0 AS numOppID,
					RH.numDivisionId,
					0,
					CASE WHEN GJD.numDebitAmt > 0 THEN GJD.numDebitAmt * -1 ELSE GJD.numCreditAmt END DealAmount,
					0 numBizDocId,
					GJH.datEntry_Date,GJD.[numCurrencyID],0 AS AmountPaid
			 FROM   dbo.ReturnHeader RH JOIN dbo.General_Journal_Header GJH ON RH.numReturnHeaderID = GJH.numReturnID
			JOIN General_Journal_Details GJD ON GJH.numJournal_Id = GJD.numJournalId 
			INNER JOIN dbo.Chart_Of_Accounts COA ON COA.numAccountId = GJD.numChartAcntId
			WHERE RH.tintReturnType=4 AND RH.numDomainId=@numDomainId  AND COA.vcAccountCode LIKE '01020102%'
					AND (RH.numDivisionId = @numDivisionId OR @numDivisionId IS NULL)
					AND (monBizDocAmount > 0) AND ISNULL(RH.numBillPaymentIDRef,0)>0
					AND (RH.numAccountClass=@numAccountClass OR @numAccountClass=0)
					AND RH.[dtCreatedDate]  BETWEEN @dtFromDate AND @dtToDate
					--GROUP BY DM.numDivisionId,DM.dtDepositDate
		
		
---SELECT  SUM(monUnAppliedAmount) FROM    [#TempAPRecord]

   INSERT  INTO [#TempAPRecord]
        (
          numUserCntID,
          [numOppId],
          [numDivisionId],
          [numOppBizDocsId],
          [DealAmount],
          [numBizDocId],
          [numCurrencyID],
          [dtDueDate],AmountPaid,monUnAppliedAmount
        )
SELECT @numDomainId,0, BPH.numDivisionId,0,0,0,0,NULL,0,sum(ISNULL(monPaymentAmount,0) - ISNULL(monAppliedAmount,0))       
 FROM BillPaymentHeader BPH
		WHERE BPH.numDomainId=@numDomainId   
		AND (BPH.numDivisionId = @numDivisionId
                            OR @numDivisionId IS NULL)
		AND (ISNULL(monPaymentAmount,0) - ISNULL(monAppliedAmount,0)) > 0
		AND (BPH.numAccountClass=@numAccountClass OR @numAccountClass=0)
		AND [BPH].[dtPaymentDate]  BETWEEN @dtFromDate AND @dtToDate
		GROUP BY BPH.numDivisionId
		

--SELECT  GETDATE()
        INSERT  INTO [#TempAPRecord1]
                (
                  [numUserCntID],
                  [numDivisionId],
                  [tintCRMType],
                  [vcCompanyName],
                  [vcCustPhone],
                  [numContactId],
                  [vcEmail],
                  [numPhone],
                  [vcContactName],
                  [numThirtyDays],
                  [numSixtyDays],
                  [numNinetyDays],
                  [numOverNinetyDays],
                  [numThirtyDaysOverDue],
                  [numSixtyDaysOverDue],
                  [numNinetyDaysOverDue],
                  [numOverNinetyDaysOverDue],
                  numCompanyID,
                  numDomainID,
                  [intThirtyDaysCount],
                  [intThirtyDaysOverDueCount],
                  [intSixtyDaysCount],
                  [intSixtyDaysOverDueCount],
                  [intNinetyDaysCount],
                  [intOverNinetyDaysCount],
                  [intNinetyDaysOverDueCount],
                  [intOverNinetyDaysOverDueCount],
                  [numThirtyDaysPaid],
                  [numSixtyDaysPaid],
                  [numNinetyDaysPaid],
                  [numOverNinetyDaysPaid],
                  [numThirtyDaysOverDuePaid],
                  [numSixtyDaysOverDuePaid],
                  [numNinetyDaysOverDuePaid],
                  [numOverNinetyDaysOverDuePaid],monUnAppliedAmount
                )
                SELECT DISTINCT
                        @numDomainId,
                        Div.numDivisionID,
                        tintCRMType AS tintCRMType,
                        vcCompanyName AS vcCompanyName,
                        '',
                        NULL,
                        '',
                        '',
                        '',
                        0 numThirtyDays,
                        0 numSixtyDays,
                        0 numNinetyDays,
                        0 numOverNinetyDays,
                        0 numThirtyDaysOverDue,
                        0 numSixtyDaysOverDue,
                        0 numNinetyDaysOverDue,
                        0 numOverNinetyDaysOverDue,
                        C.numCompanyID,
                        Div.numDomainID,
                        0 [intThirtyDaysCount],
                        0 [intThirtyDaysOverDueCount],
                        0 [intSixtyDaysCount],
                        0 [intSixtyDaysOverDueCount],
                        0 [intNinetyDaysCount],
                        0 [intOverNinetyDaysCount],
                        0 [intNinetyDaysOverDueCount],
                        0 [intOverNinetyDaysOverDueCount],
                         0 numThirtyDaysPaid,
                        0 numSixtyDaysPaid,
                        0 numNinetyDaysPaid,
                        0 numOverNinetyDaysPaid,
                        0 numThirtyDaysOverDuePaid,
                        0 numSixtyDaysOverDuePaid,
                        0 numNinetyDaysOverDuePaid,
                        0 numOverNinetyDaysOverDuePaid,0 monUnAppliedAmount
                FROM    Divisionmaster Div
                        INNER JOIN CompanyInfo C ON C.numCompanyId = Div.numCompanyID
                    --JOIN [#TempAPRecord] t ON Div.[numDivisionID] = t.[numDivisionId]
                WHERE   Div.[numDomainID] = @numDomainId
                        AND Div.[numDivisionID] IN ( SELECT DISTINCT
                                                            [numDivisionID]
                                                     FROM   [#TempAPRecord] )



--Update Custome Phone 
        UPDATE  #TempAPRecord1
        SET     [vcCustPhone] = X.[vcCustPhone]
        FROM    ( SELECT    CASE WHEN numPhone IS NULL THEN ''
                                 WHEN numPhone = '' THEN ''
                                 ELSE ', ' + numPhone
                            END AS vcCustPhone,
                            numDivisionId
                  FROM      AdditionalContactsInformation
                  WHERE     bitPrimaryContact = 1
                            AND numDivisionId IN (
                            SELECT DISTINCT
                                    numDivisionID
                            FROM    [#TempAPRecord1]
                            WHERE   numUserCntID = @numDomainId )
                ) X
        WHERE   #TempAPRecord1.numDivisionID = X.numDivisionID

--Update Customer info 
        UPDATE  #TempAPRecord1
        SET     [numContactId] = X.[numContactID],
                [vcEmail] = X.[vcEmail],
                [vcContactName] = X.[vcContactName],
                numPhone = X.numPhone
        FROM    ( SELECT  numContactId AS numContactID,
                            ISNULL(vcEmail, '') AS vcEmail,
                            ISNULL(vcFirstname, '') + ' ' + ISNULL(vcLastName, '') AS vcContactName,
                            CAST(ISNULL(CASE WHEN numPhone IS NULL THEN ''
                                             WHEN numPhone = '' THEN ''
                                             ELSE ', ' + numPhone
                                        END, '') AS VARCHAR) AS numPhone,
                            [numDivisionId]
                  FROM      AdditionalContactsInformation
                  WHERE     (vcPosition = 843 or isnull(bitPrimaryContact,0)=1)
                            AND numDivisionId IN (
                            SELECT DISTINCT
                                    numDivisionID
                            FROM    [#TempAPRecord1]
                            WHERE   numUserCntID = @numDomainId )
                ) X
        WHERE   #TempAPRecord1.numDivisionID = X.numDivisionID

                    
--SELECT  GETDATE()
------------------------------------------      
        DECLARE @baseCurrency AS NUMERIC
        SELECT  @baseCurrency = numCurrencyID
        FROM    dbo.Domain
        WHERE   numDomainId = @numDomainId
/*when any Order is placed in Foreign currency (other than base currency)then show it in purple color */

------------------------------------------
        UPDATE  #TempAPRecord1
        SET     numThirtyDays = X.numThirtyDays
        FROM    ( SELECT    SUM(DealAmount) AS numThirtyDays,
                            [numDivisionId]
                  FROM      [#TempAPRecord]
                  WHERE     numUserCntID = @numDomainId
                            AND DATEDIFF(DAY, dbo.GetUTCDateWithoutTime(),
                                         [dtDueDate]) BETWEEN 0
                                                      AND     30
                  GROUP BY  numDivisionId
                ) X
        WHERE   #TempAPRecord1.numDivisionID = X.numDivisionID
        
         UPDATE  #TempAPRecord1
        SET     numThirtyDaysPaid = X.numThirtyDaysPaid
        FROM    ( SELECT    SUM(AmountPaid) AS numThirtyDaysPaid,
                            [numDivisionId]
                  FROM      [#TempAPRecord]
                  WHERE     numUserCntID = @numDomainId
                            AND DATEDIFF(DAY, dbo.GetUTCDateWithoutTime(),
                                         [dtDueDate]) BETWEEN 0
                                                      AND     30
                  GROUP BY  numDivisionId
                ) X
        WHERE   #TempAPRecord1.numDivisionID = X.numDivisionID
        
        UPDATE  #TempAPRecord1
        SET     intThirtyDaysCount = 2
        WHERE   numDivisionId IN (
                SELECT DISTINCT
                        [numDivisionId]
                FROM    [#TempAPRecord]
                WHERE   numUserCntID = @numDomainId
                        AND DATEDIFF(DAY, dbo.GetUTCDateWithoutTime(),
                                     [dtDueDate]) BETWEEN 0
                                                  AND     30
                        AND numCurrencyID <> @baseCurrency )
------------------------------------------
        UPDATE  #TempAPRecord1
        SET     numSixtyDays = X.numSixtyDays
        FROM    ( SELECT    SUM(DealAmount) AS numSixtyDays,
                            numDivisionId
                  FROM      [#TempAPRecord]
                  WHERE     numUserCntID = @numDomainId
                            AND DATEDIFF(DAY, dbo.GetUTCDateWithoutTime(),
                                         [dtDueDate]) BETWEEN 31
                                                      AND     60
                            AND dbo.GetUTCDateWithoutTime() <= [dtDueDate]
                  GROUP BY  numDivisionId
                ) X
        WHERE   #TempAPRecord1.numDivisionID = X.numDivisionID
        
        UPDATE  #TempAPRecord1
        SET     numSixtyDaysPaid = X.numSixtyDaysPaid
        FROM    ( SELECT    SUM(AmountPaid) AS numSixtyDaysPaid,
                            numDivisionId
                  FROM      [#TempAPRecord]
                  WHERE     numUserCntID = @numDomainId
                            AND DATEDIFF(DAY, dbo.GetUTCDateWithoutTime(),
                                         [dtDueDate]) BETWEEN 31
                                                      AND     60
                            AND dbo.GetUTCDateWithoutTime() <= [dtDueDate]
                  GROUP BY  numDivisionId
                ) X
        WHERE   #TempAPRecord1.numDivisionID = X.numDivisionID
        
        UPDATE  #TempAPRecord1
        SET     intSixtyDaysCount = 2
        WHERE   numDivisionId IN (
                SELECT DISTINCT
                        [numDivisionId]
                FROM    [#TempAPRecord]
                WHERE   numUserCntID = @numDomainId
                        AND DATEDIFF(DAY, dbo.GetUTCDateWithoutTime(),
                                     [dtDueDate]) BETWEEN 31
                                                  AND     60
                        AND dbo.GetUTCDateWithoutTime() <= [dtDueDate]
                        AND numCurrencyID <> @baseCurrency )

------------------------------------------
        UPDATE  #TempAPRecord1
        SET     numNinetyDays = X.numNinetyDays
        FROM    ( SELECT    SUM(DealAmount) AS numNinetyDays,
                            numDivisionId
                  FROM      #TempAPRecord
                  WHERE     numUserCntID = @numDomainId
                            AND DATEDIFF(DAY, dbo.GetUTCDateWithoutTime(),
                                         [dtDueDate]) BETWEEN 61
                                                      AND     90
                            AND dbo.GetUTCDateWithoutTime() <= [dtDueDate]
                  GROUP BY  numDivisionId
                ) X
        WHERE   #TempAPRecord1.numDivisionID = X.numDivisionID
        
         UPDATE  #TempAPRecord1
        SET     numNinetyDaysPaid = X.numNinetyDaysPaid
        FROM    ( SELECT    SUM(AmountPaid) AS numNinetyDaysPaid,
                            numDivisionId
                  FROM      #TempAPRecord
                  WHERE     numUserCntID = @numDomainId
                            AND DATEDIFF(DAY, dbo.GetUTCDateWithoutTime(),
                                         [dtDueDate]) BETWEEN 61
                                                      AND     90
                            AND dbo.GetUTCDateWithoutTime() <= [dtDueDate]
                  GROUP BY  numDivisionId
                ) X
        WHERE   #TempAPRecord1.numDivisionID = X.numDivisionID
        
        UPDATE  #TempAPRecord1
        SET     intNinetyDaysCount = 2
        WHERE   numDivisionId IN (
                SELECT DISTINCT
                        [numDivisionId]
                FROM    [#TempAPRecord]
                WHERE   numUserCntID = @numDomainId
                        AND DATEDIFF(DAY, dbo.GetUTCDateWithoutTime(),
                                     [dtDueDate]) BETWEEN 61
                                                  AND     90
                        AND dbo.GetUTCDateWithoutTime() <= [dtDueDate]
                        AND numCurrencyID <> @baseCurrency )

------------------------------------------
        UPDATE  #TempAPRecord1
        SET     numOverNinetyDays = X.numOverNinetyDays
        FROM    ( SELECT    SUM(DealAmount) AS numOverNinetyDays,
                            numDivisionId
                  FROM      #TempAPRecord
                  WHERE     numUserCntID = @numDomainId
                            AND [dtDueDate] >= DATEADD(DAY, 91,
                                                       dbo.GetUTCDateWithoutTime())
                            AND dbo.GetUTCDateWithoutTime() <= [dtDueDate]
                  GROUP BY  numDivisionId
                ) X
        WHERE   #TempAPRecord1.numDivisionID = X.numDivisionID
        
        UPDATE  #TempAPRecord1
        SET     numOverNinetyDaysPaid = X.numOverNinetyDaysPaid
        FROM    ( SELECT    SUM(AmountPaid) AS numOverNinetyDaysPaid,
                            numDivisionId
                  FROM      #TempAPRecord
                  WHERE     numUserCntID = @numDomainId
                            AND [dtDueDate] >= DATEADD(DAY, 91,
                                                       dbo.GetUTCDateWithoutTime())
                            AND dbo.GetUTCDateWithoutTime() <= [dtDueDate]
                  GROUP BY  numDivisionId
                ) X
        WHERE   #TempAPRecord1.numDivisionID = X.numDivisionID
        
        UPDATE  #TempAPRecord1
        SET     intOverNinetyDaysCount = 2
        WHERE   numDivisionId IN (
                SELECT DISTINCT
                        [numDivisionId]
                FROM    [#TempAPRecord]
                WHERE   numUserCntID = @numDomainId
                        AND [dtDueDate] >= DATEADD(DAY, 91,
                                                   dbo.GetUTCDateWithoutTime())
                        AND dbo.GetUTCDateWithoutTime() <= [dtDueDate]
                        AND numCurrencyID <> @baseCurrency )

------------------------------------------
        UPDATE  #TempAPRecord1
        SET     numThirtyDaysOverDue = X.numThirtyDaysOverDue
        FROM    ( SELECT    SUM(DealAmount) AS numThirtyDaysOverDue,
                            numDivisionId
                  FROM      #TempAPRecord
                  WHERE     numUserCntID = @numDomainId
                            AND dbo.GetUTCDateWithoutTime() > [dtDueDate]
                            AND DATEDIFF(DAY, [dtDueDate],
                                         dbo.GetUTCDateWithoutTime()) BETWEEN 0
                                                                      AND     30
                  GROUP BY  numDivisionId
                ) X
        WHERE   #TempAPRecord1.numDivisionID = X.numDivisionID
        
         UPDATE  #TempAPRecord1
        SET     numThirtyDaysOverDuePaid = X.numThirtyDaysOverDuePaid
        FROM    ( SELECT    SUM(AmountPaid) AS numThirtyDaysOverDuePaid,
                            numDivisionId
                  FROM      #TempAPRecord
                  WHERE     numUserCntID = @numDomainId
                            AND dbo.GetUTCDateWithoutTime() > [dtDueDate]
                            AND DATEDIFF(DAY, [dtDueDate],
                                         dbo.GetUTCDateWithoutTime()) BETWEEN 0
                                                                      AND     30
                  GROUP BY  numDivisionId
                ) X
        WHERE   #TempAPRecord1.numDivisionID = X.numDivisionID
        
        UPDATE  #TempAPRecord1
        SET     intThirtyDaysOverDueCount = 2
        WHERE   numDivisionId IN (
                SELECT DISTINCT
                        [numDivisionId]
                FROM    [#TempAPRecord]
                WHERE   numUserCntID = @numDomainId
                        AND dbo.GetUTCDateWithoutTime() > [dtDueDate]
                        AND DATEDIFF(DAY, [dtDueDate],
                                     dbo.GetUTCDateWithoutTime()) BETWEEN 0
                                                                  AND     30
                        AND numCurrencyID <> @baseCurrency )

----------------------------------
        UPDATE  #TempAPRecord1
        SET     numSixtyDaysOverDue = X.numSixtyDaysOverDue
        FROM    ( SELECT    SUM(DealAmount) AS numSixtyDaysOverDue,
                            numDivisionId
                  FROM      #TempAPRecord
                  WHERE     numUserCntID = @numDomainId
                            AND dbo.GetUTCDateWithoutTime() > [dtDueDate]
                            AND DATEDIFF(DAY, [dtDueDate],
                                         dbo.GetUTCDateWithoutTime()) BETWEEN 31
                                                                      AND     60
                  GROUP BY  numDivisionId
                ) X
        WHERE   #TempAPRecord1.numDivisionID = X.numDivisionID
        
          UPDATE  #TempAPRecord1
        SET     numSixtyDaysOverDuePaid = X.numSixtyDaysOverDuePaid
        FROM    ( SELECT    SUM(AmountPaid) AS numSixtyDaysOverDuePaid,
                            numDivisionId
                  FROM      #TempAPRecord
                  WHERE     numUserCntID = @numDomainId
                            AND dbo.GetUTCDateWithoutTime() > [dtDueDate]
                            AND DATEDIFF(DAY, [dtDueDate],
                                         dbo.GetUTCDateWithoutTime()) BETWEEN 31
                                                                      AND     60
                  GROUP BY  numDivisionId
                ) X
        WHERE   #TempAPRecord1.numDivisionID = X.numDivisionID
        
        UPDATE  #TempAPRecord1
        SET     intSixtyDaysOverDueCount = 2
        WHERE   numDivisionId IN (
                SELECT DISTINCT
                        [numDivisionId]
                FROM    [#TempAPRecord]
                WHERE   numUserCntID = @numDomainId
                        AND dbo.GetUTCDateWithoutTime() > [dtDueDate]
                        AND DATEDIFF(DAY, [dtDueDate],
                                     dbo.GetUTCDateWithoutTime()) BETWEEN 31
                                                                  AND     60
                        AND numCurrencyID <> @baseCurrency )
------------------------------------------
        UPDATE  #TempAPRecord1
        SET     numNinetyDaysOverDue = X.numNinetyDaysOverDue
        FROM    ( SELECT    SUM(DealAmount) AS numNinetyDaysOverDue,
                            numDivisionId
                  FROM      #TempAPRecord
                  WHERE     numUserCntID = @numDomainId
                            AND dbo.GetUTCDateWithoutTime() > [dtDueDate]
                            AND DATEDIFF(DAY, [dtDueDate],
                                         dbo.GetUTCDateWithoutTime()) BETWEEN 61
                                                                      AND     90
                  GROUP BY  numDivisionId
                ) X
        WHERE   #TempAPRecord1.numDivisionID = X.numDivisionID
        
         UPDATE  #TempAPRecord1
        SET     numNinetyDaysOverDuePaid = X.numNinetyDaysOverDuePaid
        FROM    ( SELECT    SUM(AmountPaid) AS numNinetyDaysOverDuePaid,
                            numDivisionId
                  FROM      #TempAPRecord
                  WHERE     numUserCntID = @numDomainId
                            AND dbo.GetUTCDateWithoutTime() > [dtDueDate]
                            AND DATEDIFF(DAY, [dtDueDate],
                                         dbo.GetUTCDateWithoutTime()) BETWEEN 61
                                                                      AND     90
                  GROUP BY  numDivisionId
                ) X
        WHERE   #TempAPRecord1.numDivisionID = X.numDivisionID
        
        UPDATE  #TempAPRecord1
        SET     intNinetyDaysOverDueCount = 2
        WHERE   numDivisionId IN (
                SELECT DISTINCT
                        [numDivisionId]
                FROM    [#TempAPRecord]
                WHERE   numUserCntID = @numDomainId
                        AND dbo.GetUTCDateWithoutTime() > [dtDueDate]
                        AND DATEDIFF(DAY, [dtDueDate],
                                     dbo.GetUTCDateWithoutTime()) BETWEEN 61
                                                                  AND     90
                        AND numCurrencyID <> @baseCurrency )
------------------------------------------
        UPDATE  #TempAPRecord1
        SET     numOverNinetyDaysOverDue = X.numOverNinetyDaysOverDue
        FROM    ( SELECT    SUM(DealAmount) AS numOverNinetyDaysOverDue,
                            numDivisionId
                  FROM      #TempAPRecord
                  WHERE     numUserCntID = @numDomainId
                            AND dbo.GetUTCDateWithoutTime() > [dtDueDate]
                            AND DATEDIFF(DAY, [dtDueDate],
                                         dbo.GetUTCDateWithoutTime()) > 90
                  GROUP BY  numDivisionId
                ) X
        WHERE   #TempAPRecord1.numDivisionID = X.numDivisionID
        
           UPDATE  #TempAPRecord1
        SET     numOverNinetyDaysOverDuePaid = X.numOverNinetyDaysOverDuePaid
        FROM    ( SELECT    SUM(AmountPaid) AS numOverNinetyDaysOverDuePaid,
                            numDivisionId
                  FROM      #TempAPRecord
                  WHERE     numUserCntID = @numDomainId
                            AND dbo.GetUTCDateWithoutTime() > [dtDueDate]
                            AND DATEDIFF(DAY, [dtDueDate],
                                         dbo.GetUTCDateWithoutTime()) > 90
                  GROUP BY  numDivisionId
                ) X
        WHERE   #TempAPRecord1.numDivisionID = X.numDivisionID
        
        UPDATE  #TempAPRecord1
        SET     intOverNinetyDaysOverDueCount = 2
        WHERE   numDivisionId IN (
                SELECT DISTINCT
                        [numDivisionId]
                FROM    [#TempAPRecord]
                WHERE   numUserCntID = @numDomainId
                        AND dbo.GetUTCDateWithoutTime() > [dtDueDate]
                        AND DATEDIFF(DAY, [dtDueDate],
                                     dbo.GetUTCDateWithoutTime()) > 90
                        AND numCurrencyID <> @baseCurrency )
             


UPDATE  #TempAPRecord1
SET     monUnAppliedAmount = ISNULL(X.monUnAppliedAmount,0)
FROM    (SELECT  SUM(ISNULL(monUnAppliedAmount,0)) AS monUnAppliedAmount,numDivisionId
FROM    #TempAPRecord
WHERE   numUserCntID =@numDomainId 
GROUP BY numDivisionId
) X WHERE   #TempAPRecord1.numDivisionID = X.numDivisionID

UPDATE  #TempAPRecord1 SET numTotal=  isnull(numThirtyDays, 0) + isnull(numThirtyDaysOverDue, 0)
                + isnull(numSixtyDays, 0) + isnull(numSixtyDaysOverDue, 0)
                + isnull(numNinetyDays, 0) + isnull(numNinetyDaysOverDue, 0)
                + isnull(numOverNinetyDays, 0)
                + isnull(numOverNinetyDaysOverDue, 0) 			
             
        SELECT  [numDivisionId],
                [numContactId],
                [tintCRMType],
                [vcCompanyName],
                [vcCustPhone],
                [vcContactName] as vcApName,
                [vcEmail],
                [numPhone] as vcPhone,
                [numThirtyDays],
                [numSixtyDays],
                [numNinetyDays],
                [numOverNinetyDays],
                [numThirtyDaysOverDue],
                [numSixtyDaysOverDue],
                [numNinetyDaysOverDue],
                [numOverNinetyDaysOverDue],
             ISNULL(numTotal,0) - ISNULL(monUnAppliedAmount,0) AS numTotal,
                numCompanyID,
                numDomainID,
                [intThirtyDaysCount],
                [intThirtyDaysOverDueCount],
                [intSixtyDaysCount],
                [intSixtyDaysOverDueCount],
                [intNinetyDaysCount],
                [intOverNinetyDaysCount],
                [intNinetyDaysOverDueCount],
                [intOverNinetyDaysOverDueCount],
                [numThirtyDaysPaid],
                [numSixtyDaysPaid],
                [numNinetyDaysPaid],
                [numOverNinetyDaysPaid],
                [numThirtyDaysOverDuePaid],
                [numSixtyDaysOverDuePaid],
                [numNinetyDaysOverDuePaid],
                [numOverNinetyDaysOverDuePaid],ISNULL(monUnAppliedAmount,0) AS monUnAppliedAmount
        FROM    #TempAPRecord1
        WHERE   numUserCntID = @numDomainId AND (numDivisionId = @numDivisionId OR @numDivisionId IS NULL)
        ORDER BY [vcCompanyName]

        drop table #TempAPRecord1
        drop table #TempAPRecord 

        SET NOCOUNT OFF   
    
    END
/****** Object:  StoredProcedure [dbo].[USP_GetAccountPayableAging_Details]    Script Date: 03/06/2009 00:30:26 ******/
    SET ANSI_NULLS ON


GO
/****** Object:  StoredProcedure [dbo].[USP_GetAccountReceivableAging]    Script Date: 09/01/2009 00:40:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--SELECT * FROM domain
--Created By Siva
-- exec [dbo].[USP_GetAccountReceivableAging] 72,null,1
/*Note By-Chintan 
Some places we have used 
DATEADD(DAY, 0, DATEDIFF(DAY,0,GETUTCDATE()))
insted of GETUTCDATE()
Reson is the difference in result repectively
"2009-03-05 00:00:00.000"	"2009-03-05 09:38:02.560"
There WE are comparing UTCDate without Time.
Created function for same thing dbo.GetUTCDateWithoutTime()
*/
-- EXEC usp_getaccountreceivableaging 169,0,1,0,'2001-01-01 00:00:00.000', '2014-12-31 23:59:59:999'
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[USP_GetAccountReceivableAging]
(
               @numDomainId   AS NUMERIC(9)  = 0,
               @numDivisionId AS NUMERIC(9)  = NULL,
               @numUserCntID AS NUMERIC(9),
               @numAccountClass AS NUMERIC(9)=0,
			   @dtFromDate AS DATETIME = NULL,
			   @dtToDate AS DATETIME = NULL
)
AS
  BEGIN
	SET NOCOUNT ON

	IF @dtFromDate IS NULL
		SET @dtFromDate = '1990-01-01 00:00:00.000'
	IF @dtToDate IS NULL
		SET @dtToDate = GETUTCDATE()
	
	--DECLARE @From DATETIME
	--DECLARE @To DATETIME

	SET @dtFromDate = DATEADD(SECOND, DATEDIFF(SECOND, GETDATE(), GETUTCDATE()), @dtFromDate);
	SET @dtToDate =  DATEADD(SECOND, DATEDIFF(SECOND, GETDATE(), GETUTCDATE()), @dtToDate);

	PRINT @dtFromDate
	PRINT @dtToDate

	DECLARE  @AuthoritativeSalesBizDocId  AS INTEGER
    SELECT @AuthoritativeSalesBizDocId = isnull(numAuthoritativeSales,0)
    FROM   AuthoritativeBizDocs
    WHERE  numDomainId = @numDomainId;
  
--SELECT  GETDATE()
INSERT  INTO [TempARRecord]
        (
          numUserCntID,
          [numOppId],
          [numDivisionId],
          [numOppBizDocsId],
          [DealAmount],
          [numBizDocId],
          [numCurrencyID],
          [dtDueDate],AmountPaid
        )
        SELECT  @numUserCntID,
                OB.numOppId,
                Opp.numDivisionId,
                OB.numOppBizDocsId,
                ISNULL(OB.monDealAmount --dbo.GetDealAmount(OB.numOppID,getutcdate(),OB.numOppBizDocsId)
                       * ISNULL(Opp.fltExchangeRate, 1), 0)
                - ISNULL(OB.monAmountPaid * ISNULL(Opp.fltExchangeRate, 1),
                         0) DealAmount,
                OB.numBizDocId,
                ISNULL(Opp.numCurrencyID, 0) numCurrencyID,
--                DATEADD(DAY,CASE WHEN Opp.bitBillingTerms = 1 
--								 THEN convert(int,ISNULL(dbo.fn_GetListItemName(isnull(Opp.intBillingDays,0)),0)) 
--								 ELSE 0 
--							END, dtFromDate) AS dtDueDate,
				DATEADD(DAY,CASE WHEN Opp.bitBillingTerms = 1 
								 THEN convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(Opp.intBillingDays,0)), 0))
								 ELSE 0 
							END, dtFromDate) AS dtDueDate,
                ISNULL(OB.monAmountPaid * ISNULL(Opp.fltExchangeRate, 1),0) as AmountPaid
        FROM    OpportunityMaster Opp
                JOIN OpportunityBizDocs OB ON OB.numOppId = Opp.numOppID
                LEFT OUTER JOIN [Currency] C ON Opp.[numCurrencyID] = C.[numCurrencyID]
				JOIN [dbo].[DivisionMaster] AS DIV ON DIV.[numDivisionID] = Opp.[numDivisionId]
        WHERE   tintOpptype = 1
                AND tintoppStatus = 1
                --AND dtShippedDate IS NULL --Commented by chintan reason: Account Transaction and AR values was not matching
                AND opp.numDomainID = @numDomainId
                AND numBizDocId = @AuthoritativeSalesBizDocId
                AND OB.bitAuthoritativeBizDocs=1
                AND ISNULL(OB.tintDeferred,0) <> 1
                AND (Opp.numDivisionId = @numDivisionId OR @numDivisionId IS NULL)
                            AND (Opp.numAccountClass=@numAccountClass OR @numAccountClass=0)
				AND CONVERT(DATE,OB.[dtCreatedDate]) BETWEEN @dtFromDate AND @dtToDate
        GROUP BY OB.numOppId,
                Opp.numDivisionId,
                OB.numOppBizDocsId,
                Opp.fltExchangeRate,
                OB.numBizDocId,
                OB.dtCreatedDate,
                Opp.bitBillingTerms,
                Opp.intBillingDays,
                OB.monDealAmount,
                Opp.numCurrencyID,
                OB.dtFromDate,OB.monAmountPaid
        HAVING  ( ISNULL(OB.monDealAmount * ISNULL(Opp.fltExchangeRate, 1), 0)
                  - ISNULL(OB.monAmountPaid * ISNULL(Opp.fltExchangeRate, 1),
                           0) ) != 0
                           
	--Show Impact of AR journal entries in report as well 
UNION 
 SELECT @numUserCntID,
        0,
        GJD.numCustomerId,
        0,
        CASE WHEN GJD.numDebitAmt > 0 THEN GJD.numDebitAmt ELSE -1*GJD.numCreditAmt END,
        0,
        GJD.[numCurrencyID],
        GJH.datEntry_Date ,0 AS AmountPaid
 FROM   dbo.General_Journal_Header GJH
        INNER JOIN dbo.General_Journal_Details GJD ON GJH.numJournal_Id = GJD.numJournalId
                                                      AND GJH.numDomainId = GJD.numDomainId
        INNER JOIN dbo.Chart_Of_Accounts COA ON COA.numAccountId = GJD.numChartAcntId
		JOIN [dbo].[DivisionMaster] AS DIV ON DIV.[numDivisionID] = [GJD].[numCustomerId]
 WHERE  GJH.numDomainId = @numDomainId
        AND COA.vcAccountCode LIKE '0101010501'
        AND ISNULL(numOppId,0)=0 AND ISNULL(numOppBizDocsId,0)=0
        AND (GJD.numCustomerId = @numDivisionId OR @numDivisionId IS NULL)
		AND ISNULL(GJH.numDepositID,0)=0 AND ISNULL(GJH.numReturnID,0)=0
		AND (GJH.numAccountClass=@numAccountClass OR @numAccountClass=0)                            
		AND CONVERT(DATE,GJH.[datEntry_Date]) BETWEEN @dtFromDate AND @dtToDate
UNION ALL
 SELECT @numUserCntID,
        0,
        DM.numDivisionId,
        0,
        (ISNULL(monDepositAmount,0) - ISNULL(monAppliedAmount,0) /*- ISNULL(monRefundAmount,0)*/) * -1,
        0,
        DM.numCurrencyID,
        DM.dtDepositDate ,0 AS AmountPaid
 FROM   DepositMaster DM 
 JOIN [dbo].[DivisionMaster] AS DIV ON [DM].[numDivisionID] = DIV.[numDivisionID]
 WHERE DM.numDomainId=@numDomainId AND tintDepositePage IN(2,3)  
		AND (DM.numDivisionId = @numDivisionId OR @numDivisionId IS NULL)
		AND ((ISNULL(monDepositAmount,0) - ISNULL(monAppliedAmount,0) /*- ISNULL(monRefundAmount,0)*/) > 0)
		AND (DM.numAccountClass=@numAccountClass OR @numAccountClass=0)
		--GROUP BY DM.numDivisionId,DM.dtDepositDate
		AND CONVERT(DATE,DM.[dtDepositDate]) BETWEEN @dtFromDate AND @dtToDate
UNION ALL --Standalone Refund against Account Receivable
SELECT @numUserCntID,
        0,
        RH.numDivisionId,
        0,
        CASE WHEN GJD.numDebitAmt > 0 THEN GJD.numDebitAmt ELSE -1*GJD.numCreditAmt END,
		--(ISNULL(RH.[monBizDocAmount],0) - (CASE WHEN GJD.numDebitAmt > 0 THEN GJD.numDebitAmt ELSE -1*GJD.numCreditAmt END)),
        0,
        GJD.[numCurrencyID],
        GJH.datEntry_Date ,0 AS AmountPaid
 FROM   dbo.ReturnHeader RH JOIN dbo.General_Journal_Header GJH ON RH.numReturnHeaderID = GJH.numReturnID
 JOIN [dbo].[DivisionMaster] AS DIV ON DIV.[numDivisionID] = RH.[numDivisionId]
JOIN General_Journal_Details GJD ON GJH.numJournal_Id = GJD.numJournalId 
JOIN dbo.Chart_Of_Accounts COA ON COA.numAccountId = GJD.numChartAcntId AND COA.vcAccountCode LIKE '0101010501'
WHERE RH.tintReturnType=4 AND RH.numDomainId=@numDomainId  
AND ISNULL(RH.numParentID,0) = 0
AND ISNULL(RH.IsUnappliedPayment,0) = 0
		AND (RH.numDivisionId = @numDivisionId OR @numDivisionId IS NULL)
		AND (monBizDocAmount > 0) AND ISNULL(RH.numBillPaymentIDRef,0)=0 --AND ISNULL(RH.numDepositIDRef,0) > 0
		AND (RH.numAccountClass=@numAccountClass OR @numAccountClass=0)
		AND CONVERT(DATE,GJH.[datEntry_Date])  BETWEEN @dtFromDate AND @dtToDate
		--AND RH.numDivisionId <> 235216
		--AND (ISNULL(RH.[monBizDocAmount],0) - (CASE WHEN GJD.numDebitAmt > 0 THEN GJD.numDebitAmt ELSE -1*GJD.numCreditAmt END)) != 0
		--GROUP BY DM.numDivisionId,DM.dtDepositDate

	
INSERT  INTO [TempARRecord]
        (
          numUserCntID,
          [numOppId],
          [numDivisionId],
          [numOppBizDocsId],
          [DealAmount],
          [numBizDocId],
          [numCurrencyID],
          [dtDueDate],AmountPaid,monUnAppliedAmount
        )
SELECT @numUserCntID,0, DM.numDivisionId,0,0,0,0,NULL,0,sum(ISNULL(monDepositAmount,0) - ISNULL(monAppliedAmount,0))       
 FROM DepositMaster DM 
 JOIN [dbo].[DivisionMaster] AS DIV ON DIV.[numDivisionID] = DM.[numDivisionID]
 		WHERE DM.numDomainId=@numDomainId AND tintDepositePage IN(2,3)  
		AND (DM.numDivisionId = @numDivisionId OR @numDivisionId IS NULL)
		AND ((ISNULL(monDepositAmount,0) - ISNULL(monAppliedAmount,0)) > 0)
		AND (DM.numAccountClass=@numAccountClass OR @numAccountClass=0)
		AND CONVERT(DATE,DM.[dtDepositDate]) BETWEEN @dtFromDate AND @dtToDate
		GROUP BY DM.numDivisionId


--SELECT  * FROM    [TempARRecord]


--SELECT  GETDATE()
INSERT  INTO [TempARRecord1] (
	[numUserCntID],
	[numDivisionId],
	[tintCRMType],
	[vcCompanyName],
	[vcCustPhone],
	[numContactId],
	[vcEmail],
	[numPhone],
	[vcContactName],
	[numThirtyDays],
	[numSixtyDays],
	[numNinetyDays],
	[numOverNinetyDays],
	[numThirtyDaysOverDue],
	[numSixtyDaysOverDue],
	[numNinetyDaysOverDue],
	[numOverNinetyDaysOverDue],
	numCompanyID,
	numDomainID,
	[intThirtyDaysCount],
	[intThirtyDaysOverDueCount],
	[intSixtyDaysCount],
	[intSixtyDaysOverDueCount],
	[intNinetyDaysCount],
	[intOverNinetyDaysCount],
	[intNinetyDaysOverDueCount],
	[intOverNinetyDaysOverDueCount],
	[numThirtyDaysPaid],
	[numSixtyDaysPaid],
	[numNinetyDaysPaid],
	[numOverNinetyDaysPaid],
	[numThirtyDaysOverDuePaid],
	[numSixtyDaysOverDuePaid],
	[numNinetyDaysOverDuePaid],
	[numOverNinetyDaysOverDuePaid],monUnAppliedAmount
) 
        SELECT DISTINCT
                @numUserCntID,
                Div.numDivisionID,
                tintCRMType AS tintCRMType,
                vcCompanyName AS vcCompanyName,
                '',
                NULL,
                '',
                '',
                '',
                0 numThirtyDays,
                0 numSixtyDays,
                0 numNinetyDays,
                0 numOverNinetyDays,
                0 numThirtyDaysOverDue,
                0 numSixtyDaysOverDue,
                0 numNinetyDaysOverDue,
                0 numOverNinetyDaysOverDue,
                C.numCompanyID,
				Div.numDomainID,
                0 [intThirtyDaysCount],
				0 [intThirtyDaysOverDueCount],
				0 [intSixtyDaysCount],
				0 [intSixtyDaysOverDueCount],
				0 [intNinetyDaysCount],
				0 [intOverNinetyDaysCount],
				0 [intNinetyDaysOverDueCount],
				0 [intOverNinetyDaysOverDueCount],
				0 numThirtyDaysPaid,
                0 numSixtyDaysPaid,
                0 numNinetyDaysPaid,
                0 numOverNinetyDaysPaid,
                0 numThirtyDaysOverDuePaid,
                0 numSixtyDaysOverDuePaid,
                0 numNinetyDaysOverDuePaid,
                0 numOverNinetyDaysOverDuePaid,0 monUnAppliedAmount
        FROM    Divisionmaster Div
                INNER JOIN CompanyInfo C ON C.numCompanyId = Div.numCompanyID
                    --JOIN [TempARRecord] t ON Div.[numDivisionID] = t.[numDivisionId]
        WHERE   Div.[numDomainID] = @numDomainId
                AND Div.[numDivisionID] IN ( SELECT DISTINCT
                                                    [numDivisionID]
                                             FROM   [TempARRecord] )



--Update Custome Phone 
UPDATE  TempARRecord1
SET     [vcCustPhone] = X.[vcCustPhone]
FROM    ( SELECT 
                            CASE WHEN numPhone IS NULL THEN ''
                                 WHEN numPhone = '' THEN ''
                                 ELSE ', ' + numPhone
                            END AS vcCustPhone,
                            numDivisionId
                  FROM      AdditionalContactsInformation
                  WHERE     bitPrimaryContact = 1
                            AND numDivisionId  IN (SELECT DISTINCT numDivisionID FROM [TempARRecord1] WHERE numUserCntID =@numUserCntID)

        ) X
WHERE   TempARRecord1.numDivisionID = X.numDivisionID

--Update Customer info 
UPDATE  TempARRecord1
SET     [numContactId] = X.[numContactID],[vcEmail] = X.[vcEmail],[vcContactName] = X.[vcContactName],numPhone=X.numPhone
FROM    ( SELECT numContactId AS numContactID,
                     ISNULL(vcEmail,'') AS vcEmail,
                     ISNULL(vcFirstname,'') + ' ' + ISNULL(vcLastName,'') AS vcContactName,
                     CAST(ISNULL( CASE WHEN numPhone IS NULL THEN '' WHEN numPhone = '' THEN '' ELSE ', ' + numPhone END,'') AS VARCHAR) AS numPhone,
                     [numDivisionId]
                  FROM      AdditionalContactsInformation
                  WHERE     (vcPosition = 844 or isnull(bitPrimaryContact,0)=1)
                            AND numDivisionId  IN (SELECT DISTINCT numDivisionID FROM [TempARRecord1] WHERE numUserCntID =@numUserCntID)
    ) X
WHERE   TempARRecord1.numDivisionID = X.numDivisionID

                    
--SELECT  GETDATE()
------------------------------------------      
DECLARE @baseCurrency AS NUMERIC
SELECT @baseCurrency=numCurrencyID FROM dbo.Domain WHERE numDomainId=@numDomainId
/*when any Order is placed in Foreign currency (other than base currency)then show it in purple color */
------------------------------------------
UPDATE  TempARRecord1
SET     numThirtyDays = X.numThirtyDays
FROM    ( SELECT    SUM(DealAmount) AS numThirtyDays,
                    [numDivisionId]
          FROM      [TempARRecord]
          WHERE     numUserCntID =@numUserCntID AND
		DATEDIFF(DAY,dbo.GetUTCDateWithoutTime(),[dtDueDate]) BETWEEN 0 AND 30	
          GROUP BY  numDivisionId
        ) X WHERE   TempARRecord1.numDivisionID = X.numDivisionID


UPDATE  TempARRecord1
SET     numThirtyDaysPaid = X.numThirtyDaysPaid
FROM    ( SELECT    SUM(AmountPaid) AS numThirtyDaysPaid,
                    [numDivisionId]
          FROM      [TempARRecord]
          WHERE     numUserCntID =@numUserCntID AND
		DATEDIFF(DAY,dbo.GetUTCDateWithoutTime(),[dtDueDate]) BETWEEN 0 AND 30	
          GROUP BY  numDivisionId
        ) X WHERE   TempARRecord1.numDivisionID = X.numDivisionID

                
UPDATE  TempARRecord1 SET intThirtyDaysCount = 2 WHERE numDivisionId IN ( SELECT DISTINCT [numDivisionId]
          FROM      [TempARRecord]
          WHERE     numUserCntID =@numUserCntID AND DATEDIFF(DAY,dbo.GetUTCDateWithoutTime(),[dtDueDate]) BETWEEN 0 AND 30	
                                                AND numCurrencyID <>@baseCurrency )
------------------------------------------
UPDATE  TempARRecord1
SET     numSixtyDays = X.numSixtyDays
FROM    ( SELECT  SUM(DealAmount) AS numSixtyDays,
        numDivisionId
FROM    [TempARRecord]
WHERE   numUserCntID =@numUserCntID AND 
	DATEDIFF(DAY,dbo.GetUTCDateWithoutTime(),[dtDueDate]) BETWEEN 31 AND 60	
        AND dbo.GetUTCDateWithoutTime() <= [dtDueDate]
GROUP BY numDivisionId
) X
WHERE   TempARRecord1.numDivisionID = X.numDivisionID


UPDATE  TempARRecord1
SET     numSixtyDaysPaid = X.numSixtyDaysPaid
FROM    ( SELECT  SUM(AmountPaid) AS numSixtyDaysPaid,
        numDivisionId
FROM    [TempARRecord]
WHERE   numUserCntID =@numUserCntID AND 
	DATEDIFF(DAY,dbo.GetUTCDateWithoutTime(),[dtDueDate]) BETWEEN 31 AND 60	
        AND dbo.GetUTCDateWithoutTime() <= [dtDueDate]
GROUP BY numDivisionId
) X
WHERE   TempARRecord1.numDivisionID = X.numDivisionID


UPDATE  TempARRecord1 SET intSixtyDaysCount = 2 WHERE numDivisionId IN ( SELECT DISTINCT [numDivisionId]
          FROM      [TempARRecord]
          WHERE     numUserCntID =@numUserCntID AND DATEDIFF(DAY,dbo.GetUTCDateWithoutTime(),[dtDueDate]) BETWEEN 31 AND 60
		AND dbo.GetUTCDateWithoutTime() <= [dtDueDate]
                                                AND numCurrencyID <>@baseCurrency )
------------------------------------------
UPDATE  TempARRecord1
SET     numNinetyDays = X.numNinetyDays
FROM    (SELECT  SUM(DealAmount) AS numNinetyDays,
        numDivisionId
FROM    TempARRecord
WHERE   numUserCntID =@numUserCntID AND DATEDIFF(DAY,dbo.GetUTCDateWithoutTime(),[dtDueDate]) BETWEEN 61 AND 90
        AND dbo.GetUTCDateWithoutTime() <= [dtDueDate]
GROUP BY numDivisionId
) X
WHERE   TempARRecord1.numDivisionID = X.numDivisionID


UPDATE  TempARRecord1
SET     numNinetyDaysPaid = X.numNinetyDaysPaid
FROM    (SELECT  SUM(AmountPaid) AS numNinetyDaysPaid,
        numDivisionId
FROM    TempARRecord
WHERE   numUserCntID =@numUserCntID AND DATEDIFF(DAY,dbo.GetUTCDateWithoutTime(),[dtDueDate]) BETWEEN 61 AND 90
        AND dbo.GetUTCDateWithoutTime() <= [dtDueDate]
GROUP BY numDivisionId
) X
WHERE   TempARRecord1.numDivisionID = X.numDivisionID


UPDATE  TempARRecord1 SET intNinetyDaysCount = 2 WHERE numDivisionId IN ( SELECT DISTINCT [numDivisionId]
          FROM      [TempARRecord]
          WHERE     numUserCntID =@numUserCntID AND DATEDIFF(DAY,dbo.GetUTCDateWithoutTime(),[dtDueDate]) BETWEEN 61 AND 90
        AND dbo.GetUTCDateWithoutTime() <= [dtDueDate]
                                                AND numCurrencyID <>@baseCurrency )
------------------------------------------
UPDATE  TempARRecord1
SET     numOverNinetyDays = X.numOverNinetyDays
FROM    (SELECT  SUM(DealAmount) AS numOverNinetyDays,
        numDivisionId
FROM    TempARRecord
WHERE   numUserCntID =@numUserCntID AND [dtDueDate] >= DATEADD(DAY, 91,
                                                    dbo.GetUTCDateWithoutTime())
        AND dbo.GetUTCDateWithoutTime() <= [dtDueDate]
GROUP BY numDivisionId
) X
WHERE   TempARRecord1.numDivisionID = X.numDivisionID


UPDATE  TempARRecord1
SET     numOverNinetyDaysPaid = X.numOverNinetyDaysPaid
FROM    (SELECT  SUM(AmountPaid) AS numOverNinetyDaysPaid,
        numDivisionId
FROM    TempARRecord
WHERE   numUserCntID =@numUserCntID AND [dtDueDate] >= DATEADD(DAY, 91,
                                                    dbo.GetUTCDateWithoutTime())
        AND dbo.GetUTCDateWithoutTime() <= [dtDueDate]
GROUP BY numDivisionId
) X
WHERE   TempARRecord1.numDivisionID = X.numDivisionID


UPDATE  TempARRecord1 SET intOverNinetyDaysCount = 2 WHERE numDivisionId IN ( SELECT DISTINCT [numDivisionId]
          FROM      [TempARRecord]
          WHERE     numUserCntID =@numUserCntID AND [dtDueDate] >= DATEADD(DAY, 91,
                                                    dbo.GetUTCDateWithoutTime())
        AND dbo.GetUTCDateWithoutTime() <= [dtDueDate]
                                                AND numCurrencyID <>@baseCurrency )

------------------------------------------
UPDATE  TempARRecord1
SET     numThirtyDaysOverDue = X.numThirtyDaysOverDue
FROM    (SELECT  SUM(DealAmount) AS numThirtyDaysOverDue,
        numDivisionId
FROM    TempARRecord
WHERE   numUserCntID =@numUserCntID AND dbo.GetUTCDateWithoutTime() > [dtDueDate]
        AND DATEDIFF(DAY,[dtDueDate], dbo.GetUTCDateWithoutTime()) BETWEEN 0 AND 30
GROUP BY numDivisionId
) X
WHERE   TempARRecord1.numDivisionID = X.numDivisionID


UPDATE  TempARRecord1
SET     numThirtyDaysOverDuePaid = X.numThirtyDaysOverDuePaid
FROM    (SELECT  SUM(AmountPaid) AS numThirtyDaysOverDuePaid,
        numDivisionId
FROM    TempARRecord
WHERE   numUserCntID =@numUserCntID AND dbo.GetUTCDateWithoutTime() > [dtDueDate]
        AND DATEDIFF(DAY,[dtDueDate], dbo.GetUTCDateWithoutTime()) BETWEEN 0 AND 30
GROUP BY numDivisionId
) X
WHERE   TempARRecord1.numDivisionID = X.numDivisionID


UPDATE  TempARRecord1 SET intThirtyDaysOverDueCount = 2 WHERE numDivisionId IN ( SELECT DISTINCT [numDivisionId]
          FROM      [TempARRecord]
          WHERE     numUserCntID =@numUserCntID AND dbo.GetUTCDateWithoutTime() > [dtDueDate]
        AND DATEDIFF(DAY,[dtDueDate], dbo.GetUTCDateWithoutTime()) BETWEEN 0 AND 30
                                                AND numCurrencyID <>@baseCurrency )

----------------------------------
UPDATE  TempARRecord1
SET     numSixtyDaysOverDue = X.numSixtyDaysOverDue
FROM    ( SELECT    SUM(DealAmount) AS numSixtyDaysOverDue,
                    numDivisionId
          FROM      TempARRecord
          WHERE     numUserCntID =@numUserCntID AND dbo.GetUTCDateWithoutTime() > [dtDueDate]
                    AND DATEDIFF(DAY,
                                 [dtDueDate],
                                 dbo.GetUTCDateWithoutTime()) BETWEEN 31
                                                              AND     60
          GROUP BY  numDivisionId
        ) X
WHERE   TempARRecord1.numDivisionID = X.numDivisionID

UPDATE  TempARRecord1
SET     numSixtyDaysOverDuePaid = X.numSixtyDaysOverDuePaid
FROM    ( SELECT    SUM(AmountPaid) AS numSixtyDaysOverDuePaid,
                    numDivisionId
          FROM      TempARRecord
          WHERE     numUserCntID =@numUserCntID AND dbo.GetUTCDateWithoutTime() > [dtDueDate]
                    AND DATEDIFF(DAY,
                                 [dtDueDate],
                                 dbo.GetUTCDateWithoutTime()) BETWEEN 31
                                                              AND     60
          GROUP BY  numDivisionId
        ) X
WHERE   TempARRecord1.numDivisionID = X.numDivisionID


UPDATE  TempARRecord1 SET intSixtyDaysOverDueCount = 2 WHERE numDivisionId IN ( SELECT DISTINCT [numDivisionId]
          FROM      [TempARRecord]
          WHERE     numUserCntID =@numUserCntID AND dbo.GetUTCDateWithoutTime() > [dtDueDate]
                    AND DATEDIFF(DAY,
                                 [dtDueDate],
                                 dbo.GetUTCDateWithoutTime()) BETWEEN 31
                                                              AND     60
                                                AND numCurrencyID <>@baseCurrency )

------------------------------------------
UPDATE  TempARRecord1
SET     numNinetyDaysOverDue = X.numNinetyDaysOverDue
FROM    (SELECT  SUM(DealAmount) AS numNinetyDaysOverDue,
        numDivisionId
FROM    TempARRecord
WHERE   numUserCntID =@numUserCntID AND dbo.GetUTCDateWithoutTime() > [dtDueDate]
        AND DATEDIFF(DAY,[dtDueDate], dbo.GetUTCDateWithoutTime()) BETWEEN 61 AND 90
GROUP BY numDivisionId
) X
WHERE   TempARRecord1.numDivisionID = X.numDivisionID


UPDATE  TempARRecord1
SET     numNinetyDaysOverDuePaid = X.numNinetyDaysOverDuePaid
FROM    (SELECT  SUM(AmountPaid) AS numNinetyDaysOverDuePaid,
        numDivisionId
FROM    TempARRecord
WHERE   numUserCntID =@numUserCntID AND dbo.GetUTCDateWithoutTime() > [dtDueDate]
        AND DATEDIFF(DAY,[dtDueDate], dbo.GetUTCDateWithoutTime()) BETWEEN 61 AND 90
GROUP BY numDivisionId
) X
WHERE   TempARRecord1.numDivisionID = X.numDivisionID


UPDATE  TempARRecord1 SET intNinetyDaysOverDueCount = 2 WHERE numDivisionId IN ( SELECT DISTINCT [numDivisionId]
          FROM      [TempARRecord]
          WHERE     numUserCntID =@numUserCntID AND dbo.GetUTCDateWithoutTime() > [dtDueDate]
        AND DATEDIFF(DAY,[dtDueDate], dbo.GetUTCDateWithoutTime()) BETWEEN 61 AND 90
                                                AND numCurrencyID <>@baseCurrency )
------------------------------------------
UPDATE  TempARRecord1
SET     numOverNinetyDaysOverDue = X.numOverNinetyDaysOverDue
FROM    (SELECT  SUM(DealAmount) AS numOverNinetyDaysOverDue,
        numDivisionId
FROM    TempARRecord
WHERE   numUserCntID =@numUserCntID AND dbo.GetUTCDateWithoutTime() > [dtDueDate]
        AND DATEDIFF(DAY, [dtDueDate], dbo.GetUTCDateWithoutTime()) > 90
GROUP BY numDivisionId
) X
WHERE   TempARRecord1.numDivisionID = X.numDivisionID


UPDATE  TempARRecord1
SET     numOverNinetyDaysOverDuePaid = X.numOverNinetyDaysOverDuePaid
FROM    (SELECT  SUM(AmountPaid) AS numOverNinetyDaysOverDuePaid,
        numDivisionId
FROM    TempARRecord
WHERE   numUserCntID =@numUserCntID AND dbo.GetUTCDateWithoutTime() > [dtDueDate]
        AND DATEDIFF(DAY, [dtDueDate], dbo.GetUTCDateWithoutTime()) > 90
GROUP BY numDivisionId
) X
WHERE   TempARRecord1.numDivisionID = X.numDivisionID


UPDATE  TempARRecord1 SET intOverNinetyDaysOverDueCount = 2 WHERE numDivisionId IN ( SELECT DISTINCT [numDivisionId]
          FROM      [TempARRecord]
          WHERE     numUserCntID =@numUserCntID AND dbo.GetUTCDateWithoutTime() > [dtDueDate]
        AND DATEDIFF(DAY, [dtDueDate], dbo.GetUTCDateWithoutTime()) > 90
                                                AND numCurrencyID <>@baseCurrency )


UPDATE  TempARRecord1
SET     monUnAppliedAmount = ISNULL(X.monUnAppliedAmount,0)
FROM    (SELECT  SUM(ISNULL(monUnAppliedAmount,0)) AS monUnAppliedAmount,numDivisionId
FROM    TempARRecord
WHERE   numUserCntID =@numUserCntID 
GROUP BY numDivisionId
) X WHERE   TempARRecord1.numDivisionID = X.numDivisionID


UPDATE  TempARRecord1 SET numTotal=isnull(numThirtyDays,0)
      + isnull(numThirtyDaysOverDue,0)
      + isnull(numSixtyDays,0)
      + isnull(numSixtyDaysOverDue,0)
      + isnull(numNinetyDays,0)
      + isnull(numNinetyDaysOverDue,0)
      + isnull(numOverNinetyDays,0)
      + isnull(numOverNinetyDaysOverDue,0) /*- ISNULL(monUnAppliedAmount,0) */
           
SELECT  
		[numDivisionId],
		[numContactId],
		[tintCRMType],
		[vcCompanyName],
		[vcCustPhone],
		[vcContactName],
		[vcEmail],
		[numPhone],
		[numThirtyDays],
		[numSixtyDays],
		[numNinetyDays],
		[numOverNinetyDays],
		[numThirtyDaysOverDue],
		[numSixtyDaysOverDue],
		[numNinetyDaysOverDue],
		[numOverNinetyDaysOverDue],
--		CASE WHEN (ISNULL(numTotal,0) - ISNULL(monUnAppliedAmount,0))>=0
--		 THEN ISNULL(numTotal,0) - ISNULL(monUnAppliedAmount,0) ELSE ISNULL(numTotal,0) END AS numTotal ,
		ISNULL(numTotal,0) AS numTotal,
		numCompanyID,
		numDomainID,
		[intThirtyDaysCount],
		[intThirtyDaysOverDueCount],
		[intSixtyDaysCount],
		[intSixtyDaysOverDueCount],
		[intNinetyDaysCount],
		[intOverNinetyDaysCount],
		[intNinetyDaysOverDueCount],
		[intOverNinetyDaysOverDueCount],
		[numThirtyDaysPaid],
		[numSixtyDaysPaid],
		[numNinetyDaysPaid],
		[numOverNinetyDaysPaid],
		[numThirtyDaysOverDuePaid],
		[numSixtyDaysOverDuePaid],
		[numNinetyDaysOverDuePaid],
		[numOverNinetyDaysOverDuePaid],ISNULL(monUnAppliedAmount,0) AS monUnAppliedAmount
FROM    TempARRecord1 WHERE numUserCntID =@numUserCntID AND ISNULL(numTotal,0)!=0
ORDER BY [vcCompanyName] 
DELETE  FROM TempARRecord1 WHERE   numUserCntID = @numUserCntID ;
DELETE  FROM TempARRecord WHERE   numUserCntID = @numUserCntID ;

SET NOCOUNT OFF   
    
  END
  



/****** Object:  StoredProcedure [dbo].[usp_GetAllSelectedSystemModulesPagesAndAccesses]    Script Date: 07/26/2008 16:16:13 ******/
SET ANSI_NULLS ON
/****** Object:  StoredProcedure [dbo].[usp_GetAvailableFieldsUser]    Script Date: 07/26/2008 16:16:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'usp_getavailablefieldsuser' ) 
    DROP PROCEDURE usp_getavailablefieldsuser
GO
CREATE PROCEDURE [dbo].[usp_GetAvailableFieldsUser]
    @numGroupType NUMERIC,
    @numDomainId NUMERIC,
    @numContactId NUMERIC,
    @numTabId NUMERIC
AS 
    IF ( SELECT COUNT(*)
         FROM   ShortCutGrpConf
         WHERE  numDomainId = @numDomainId
                AND numGroupID = @numGroupType
       ) = 0 
        BEGIN
            INSERT  INTO ShortCutGrpConf
                    (
                      numGroupId,
                      numLinkId,
                      numOrder,
                      bitType,
                      numDomainId,
                      numTabId,
                      bitInitialPage,
                      tintLinkType
                    )
                    SELECT  @numGroupType,
                            x.id,
                            x.[order],
                            x.bittype,
                            @numDomainId,
                            x.numTabId,
                            X.bitInitialPage,
                            0
                    FROM    ( ( SELECT  *
                                FROM    ShortCutBar
                                WHERE   bitdefault = 1
                                        AND numContactId = 0
                              )
                            )x
	               
        END


        
    IF ( SELECT COUNT(*)
         FROM   ShortCutGrpConf
         WHERE  numDomainId = @numDomainId
                AND numGroupID = @numGroupType
                AND numTabId = @numTabId
       ) > 0 
        BEGIN           
            IF ( SELECT COUNT(*)
                 FROM   ShortCutUsrcnf
                 WHERE  numDomainId = @numDomainId
                        AND numGroupID = @numGroupType
                        AND numContactId = @numContactId
                        AND numTabId = @numTabId
               ) > 0 
                BEGIN          
					IF @numTabId = 1
					BEGIN
						( SELECT    CONVERT(VARCHAR(10), Hdr.id) + '~0' AS id,
									Hdr.vclinkname,
									sconf.numOrder AS [Order]
						  FROM      ShortCutGrpconf Sconf
									JOIN ShortCutBar Hdr ON Hdr.id = Sconf.numLinkId
						  WHERE     Hdr.numTabId = @numTabId
									AND Sconf.numTabId = Hdr.numTabId
									AND sconf.numDomainId = @numDomainId
									AND sconf.numGroupID = @numGroupType
									AND numContactId = 0
									AND ISNULL(Sconf.tintLinkType, 0) = 0
									AND Sconf.numLinkId NOT IN (
									SELECT  numLinkId
									FROM    ShortCutUsrcnf
									WHERE   numDomainId = @numDomainId
											AND numGroupID = @numGroupType
											AND numContactId = @numContactId
											AND numTabId = @numTabId
											AND ISNULL(tintLinkType, 0) = 0 )
						  UNION
						  SELECT    CONVERT(VARCHAR(10), LD.numListItemID) + '~5' AS id,
									LD.vcData AS vclinkname,
									sconf.numOrder AS [Order]
						  FROM      ShortCutGrpconf Sconf
									JOIN ListDetails LD ON LD.numListItemID = Sconf.numLinkId
						  WHERE     LD.numListId = 5
									AND LD.numListItemID <> 46
									AND ( LD.numDomainID = @numDomainID
										  OR constflag = 1
										)
									AND Sconf.numTabId = @numTabId
									AND sconf.numDomainId = @numDomainId
									AND sconf.numGroupID = @numGroupType
									AND ISNULL(Sconf.tintLinkType, 0) = 5
									AND @numTabId = 7
									AND Sconf.numLinkId NOT IN (
									SELECT  numLinkId
									FROM    ShortCutUsrcnf
									WHERE   numDomainId = @numDomainId
											AND numGroupID = @numGroupType
											AND numContactId = @numContactId
											AND numTabId = @numTabId
											AND ISNULL(tintLinkType, 0) = 5 )
						  UNION
						  SELECT    CONVERT(VARCHAR(10), Hdr.id) + '~0' AS id,
									Hdr.vclinkname,
									[order]
						  FROM      ShortCutBar Hdr
						  WHERE     Hdr.numTabId = @numTabId
									AND hdr.numDomainId = @numDomainId
									AND hdr.numGroupID = @numGroupType
									AND numContactId = @numContactId
									AND id NOT IN (
									SELECT  numLinkId
									FROM    ShortCutUsrcnf
									WHERE   numDomainId = @numDomainId
											AND numGroupID = @numGroupType
											AND numContactId = @numContactId
											AND numTabId = @numTabId )
						  UNION ALL
						  SELECT    CONVERT(VARCHAR(10), LD.numListItemID) + '~0' AS id,
									LD.vcData AS [vclinkname],
									sintOrder AS [order]
						  FROM      dbo.ListDetails LD
						  WHERE     numListID = 27
									AND LD.numDomainID = @numDomainID
									AND ISNULL(constFlag, 0) = 0
									AND LD.numListItemID NOT IN (
									SELECT  numLinkId
									FROM    ShortCutUsrcnf
									WHERE   numDomainId = @numDomainId
											AND numGroupID = @numGroupType
											AND numContactId = @numContactId
											AND numTabId = @numTabId )
						  UNION ALL
						  SELECT    CONVERT(VARCHAR(10), LD.numListItemID) + '~0' AS id,
									LD.vcData AS [vclinkname],
									sintOrder AS [order]
						  FROM      dbo.ListDetails LD
						  WHERE     numListID = 27
									AND LD.numDomainID = @numDomainID
									AND ISNULL(constFlag, 0) = 1
									AND LD.numListItemID NOT IN (
									SELECT  numLinkId
									FROM    ShortCutUsrcnf
									WHERE   numDomainId = @numDomainId
											AND numGroupID = @numGroupType
											AND numContactId = @numContactId
											AND numTabId = @numTabId )
						 --ORDER BY  sconf.numOrder
	                      
						)
						ORDER BY [order]    
    
						SELECT  CONVERT(VARCHAR(10), Hdr.id) + '~0' AS id,
								Hdr.vclinkname,
								ISNULL(Sconf.bitInitialPage, 0) bitInitialPage,
								sconf.numOrder,
								ISNULL(Sconf.bitFavourite, 0) bitFavourite
						FROM    ShortCutUsrcnf Sconf
								JOIN ShortCutBar Hdr ON Hdr.id = Sconf.numLinkId
						WHERE   Hdr.numTabId = @numTabId
								AND sconf.numTabId = Hdr.numTabId
								AND sconf.numDomainId = @numDomainId
								AND sconf.numGroupID = @numGroupType
								AND sconf.numcontactid = @numContactId
								AND ISNULL(Sconf.tintLinkType, 0) = 0
						UNION
						SELECT  CONVERT(VARCHAR(10), LD.numListItemID) + '~5' AS id,
								LD.vcData AS vclinkname,
								ISNULL(Sconf.bitInitialPage, 0) bitInitialPage,
								sconf.numOrder,
								ISNULL(Sconf.bitFavourite, 0) bitFavourite
						FROM    ShortCutUsrcnf Sconf
								JOIN ListDetails LD ON LD.numListItemID = Sconf.numLinkId
						WHERE   LD.numListId = 5
								AND LD.numListItemID <> 46
								AND ( LD.numDomainID = @numDomainID
									  OR constflag = 1
									)
								AND sconf.numTabId = @numTabId
								AND sconf.numDomainId = @numDomainId
								AND sconf.numGroupID = @numGroupType
								AND sconf.numcontactid = @numContactId
								AND ISNULL(Sconf.tintLinkType, 0) = 5
								AND @numTabId = 7
						--ORDER BY sconf.numOrder
						UNION ALL
						SELECT  CONVERT(VARCHAR(10), LD.numListItemID) + '~0' AS id,
								LD.vcData AS [vclinkname],
								0 bitInitialPage,
								sintOrder AS [order],
								0 bitFavourite
						FROM    dbo.ListDetails LD
								JOIN ShortCutUsrcnf Sconf ON LD.numListItemID = Sconf.numLinkId
						WHERE   numListID = 27
								AND LD.numDomainID = @numDomainID
								AND Sconf.numContactId = @numContactId
								AND ISNULL(constFlag, 0) = 0
						UNION ALL
						SELECT  CONVERT(VARCHAR(10), LD.numListItemID) + '~0' AS id,
								LD.vcData AS [vclinkname],
								0 bitInitialPage,
								sintOrder AS [order],
								0 bitFavourite
						FROM    dbo.ListDetails LD
								JOIN ShortCutUsrcnf Sconf ON LD.numListItemID = Sconf.numLinkId
						WHERE   numListID = 27
								AND LD.numDomainID = @numDomainID
								AND ISNULL(constFlag, 0) = 1
						ORDER BY sconf.numOrder 
					END
					ELSE
					BEGIN
							( SELECT    CONVERT(VARCHAR(10), Hdr.id) + '~0' AS id,
										Hdr.vclinkname,
										sconf.numOrder AS [Order]
							  FROM      ShortCutGrpconf Sconf
										JOIN ShortCutBar Hdr ON Hdr.id = Sconf.numLinkId
							  WHERE     Hdr.numTabId = @numTabId
										AND Sconf.numTabId = Hdr.numTabId
										AND sconf.numDomainId = @numDomainId
										AND sconf.numGroupID = @numGroupType
										AND numContactId = 0
										AND ISNULL(Sconf.tintLinkType, 0) = 0
										AND Sconf.numLinkId NOT IN (
										SELECT  numLinkId
										FROM    ShortCutUsrcnf
										WHERE   numDomainId = @numDomainId
												AND numGroupID = @numGroupType
												AND numContactId = @numContactId
												AND numTabId = @numTabId
												AND ISNULL(tintLinkType, 0) = 0 )
							  UNION
							  SELECT    CONVERT(VARCHAR(10), LD.numListItemID) + '~5' AS id,
										LD.vcData AS vclinkname,
										sconf.numOrder AS [Order]
							  FROM      ShortCutGrpconf Sconf
										JOIN ListDetails LD ON LD.numListItemID = Sconf.numLinkId
							  WHERE     LD.numListId = 5
										AND LD.numListItemID <> 46
										AND ( LD.numDomainID = @numDomainID
											  OR constflag = 1
											)
										AND Sconf.numTabId = @numTabId
										AND sconf.numDomainId = @numDomainId
										AND sconf.numGroupID = @numGroupType
										AND ISNULL(Sconf.tintLinkType, 0) = 5
										AND @numTabId = 7
										AND Sconf.numLinkId NOT IN (
										SELECT  numLinkId
										FROM    ShortCutUsrcnf
										WHERE   numDomainId = @numDomainId
												AND numGroupID = @numGroupType
												AND numContactId = @numContactId
												AND numTabId = @numTabId
												AND ISNULL(tintLinkType, 0) = 5 )
							  UNION
							  SELECT    CONVERT(VARCHAR(10), Hdr.id) + '~0' AS id,
										Hdr.vclinkname,
										[order]
							  FROM      ShortCutBar Hdr
							  WHERE     Hdr.numTabId = @numTabId
										AND hdr.numDomainId = @numDomainId
										AND hdr.numGroupID = @numGroupType
										AND numContactId = @numContactId
										AND id NOT IN (
										SELECT  numLinkId
										FROM    ShortCutUsrcnf
										WHERE   numDomainId = @numDomainId
												AND numGroupID = @numGroupType
												AND numContactId = @numContactId
												AND numTabId = @numTabId )
							  
							)
							ORDER BY [order]    
	    
							SELECT  CONVERT(VARCHAR(10), Hdr.id) + '~0' AS id,
									Hdr.vclinkname,
									ISNULL(Sconf.bitInitialPage, 0) bitInitialPage,
									sconf.numOrder,
									ISNULL(Sconf.bitFavourite, 0) bitFavourite
							FROM    ShortCutUsrcnf Sconf
									JOIN ShortCutBar Hdr ON Hdr.id = Sconf.numLinkId
							WHERE   Hdr.numTabId = @numTabId
									AND sconf.numTabId = Hdr.numTabId
									AND sconf.numDomainId = @numDomainId
									AND sconf.numGroupID = @numGroupType
									AND sconf.numcontactid = @numContactId
									AND ISNULL(Sconf.tintLinkType, 0) = 0
							UNION
							SELECT  CONVERT(VARCHAR(10), LD.numListItemID) + '~5' AS id,
									LD.vcData AS vclinkname,
									ISNULL(Sconf.bitInitialPage, 0) bitInitialPage,
									sconf.numOrder,
									ISNULL(Sconf.bitFavourite, 0) bitFavourite
							FROM    ShortCutUsrcnf Sconf
									JOIN ListDetails LD ON LD.numListItemID = Sconf.numLinkId
							WHERE   LD.numListId = 5
									AND LD.numListItemID <> 46
									AND ( LD.numDomainID = @numDomainID
										  OR constflag = 1
										)
									AND sconf.numTabId = @numTabId
									AND sconf.numDomainId = @numDomainId
									AND sconf.numGroupID = @numGroupType
									AND sconf.numcontactid = @numContactId
									AND ISNULL(Sconf.tintLinkType, 0) = 5
									AND @numTabId = 7
							ORDER BY sconf.numOrder
					END
                    
                END          
            ELSE 
                BEGIN  
                    SELECT  CONVERT(VARCHAR(10), Hdr.id) + '~0' AS id,
                            Hdr.vclinkname
                    FROM    ShortCutGrpConf Sconf
                            JOIN ShortCutBar Hdr ON Hdr.id = Sconf.numLinkId
                                                    AND Sconf.numTabId = Hdr.numTabId
                    WHERE   Sconf.numGroupID = 0
                            AND Hdr.numTabId = @numTabId
                            AND ISNULL(Sconf.tintLinkType, 0) = 0        
                    UNION ALL
                    SELECT  CONVERT(VARCHAR(10), LD.numListItemID) + '~0' AS id,
                            LD.vcData AS [vclinkname]
                    FROM    dbo.ListDetails LD
                            --JOIN ShortCutUsrcnf Sconf ON LD.numListItemID = Sconf.numLinkId
                    WHERE   numListID = 27
                            AND LD.numDomainID = @numDomainID
                            AND ISNULL(constFlag, 0) = 0
                    UNION ALL
                    SELECT  CONVERT(VARCHAR(10), LD.numListItemID) + '~0' AS id,
                            LD.vcData AS [vclinkname]
                    FROM    dbo.ListDetails LD
                            --JOIN ShortCutUsrcnf Sconf ON LD.numListItemID = Sconf.numLinkId
                    WHERE   numListID = 27
                            AND LD.numDomainID = @numDomainID
                            AND ISNULL(constFlag, 0) = 1
                            
		     
                    ( SELECT    CONVERT(VARCHAR(10), Hdr.id) + '~0' AS id,
                                Hdr.vclinkname,
                                Sconf.numorder AS [order],
                                ISNULL(Sconf.bitInitialPage, 0) bitInitialPage,
                                0 AS bitFavourite
                      FROM      ShortCutGrpConf Sconf
                                JOIN ShortCutBar Hdr ON Hdr.id = Sconf.numLinkId
                      WHERE     Hdr.numTabId = @numTabId
                                AND Sconf.numTabId = Hdr.numTabId
                                AND Sconf.numGroupID = @numGroupType
                                AND Sconf.numDomainId = @numDomainId
                                AND numContactId = 0
                                AND ISNULL(Sconf.tintLinkType, 0) = 0
                      UNION
                      SELECT    CONVERT(VARCHAR(10), LD.numListItemID) + '~5' AS id,
                                LD.vcData AS vclinkname,
                                sconf.numOrder AS [order],
                                ISNULL(Sconf.bitInitialPage, 0) bitInitialPage,
                                0 AS bitFavourite
                      FROM      ShortCutGrpconf Sconf
                                JOIN ListDetails LD ON LD.numListItemID = Sconf.numLinkId
                      WHERE     LD.numListId = 5
                                AND LD.numListItemID <> 46
                                AND ( LD.numDomainID = @numDomainID
                                      OR constflag = 1
                                    )
                                AND sconf.numTabId = @numTabId
                                AND sconf.numDomainId = @numDomainId
                                AND sconf.numGroupID = @numGroupType
                                AND ISNULL(Sconf.tintLinkType, 0) = 5
                                AND @numTabId = 7
                      UNION
                      SELECT    CONVERT(VARCHAR(10), id) + '~0' AS id,
                                vclinkname,
                                [order],
                                0 AS bitInitialPage,
                                0 AS bitFavourite
                      FROM      ShortCutBar
                      WHERE     numTabId = @numTabId
                                AND bitdefault = 0
                                AND numGroupID = @numGroupType
                                AND numDomainId = @numDomainId
                                AND numContactId = @numcontactId
                    )
                    ORDER BY [order]    
            
                
        
                END          
        END         
    ELSE 
        BEGIN        
			IF @numTabId = 1
			BEGIN
				SELECT  CONVERT(VARCHAR(10), id) + '~0' AS id,
                    vclinkname
            FROM    ShortCutBar
            WHERE   numTabId = @numTabId
                    AND bitdefault = 0
                    AND numcontactid = 0
    
            SELECT  CONVERT(VARCHAR(10), id) + '~0' AS id,
                    vclinkname,
                    ISNULL(bitInitialPage, 0) AS bitInitialPage,
                    0 AS bitFavourite
            FROM    ShortCutBar
            WHERE   numTabId = @numTabId
                    AND bitdefault = 1
                    AND numContactId = 0
            UNION
            SELECT  CONVERT(VARCHAR(10), numListItemID) + '~5' AS id,
                    vcData AS vclinkname,
                    0 AS bitInitialPage,
                    0 AS bitFavourite
            FROM    ListDetails
            WHERE   numListId = 5
                    AND numListItemID <> 46
                    AND ( numDomainID = @numDomainID
                          OR constflag = 1
                        )
                    AND @numTabId = 7
            UNION ALL
            SELECT  CONVERT(VARCHAR(10), LD.numListItemID) + '~0' AS id,
                    LD.vcData AS [vclinkname],
                    0 AS bitInitialPage,
                    0 AS bitFavourite
            FROM    dbo.ListDetails LD
                    JOIN ShortCutUsrcnf Sconf ON LD.numListItemID = Sconf.numLinkId
            WHERE   numListID = 27
                    AND LD.numDomainID = @numDomainID
                    AND ISNULL(constFlag, 0) = 0
            UNION ALL
            SELECT  CONVERT(VARCHAR(10), LD.numListItemID) + '~0' AS id,
                    LD.vcData AS [vclinkname],
                    0 AS bitInitialPage,
                    0 AS bitFavourite
            FROM    dbo.ListDetails LD
                    JOIN ShortCutUsrcnf Sconf ON LD.numListItemID = Sconf.numLinkId
            WHERE   numListID = 27
                    AND LD.numDomainID = @numDomainID
                    AND ISNULL(constFlag, 0) = 1
			END
			ELSE
			BEGIN
				SELECT  CONVERT(VARCHAR(10), id) + '~0' AS id,
						vclinkname
				FROM    ShortCutBar
				WHERE   numTabId = @numTabId
						AND bitdefault = 0
						AND numcontactid = 0
	    
				SELECT  CONVERT(VARCHAR(10), id) + '~0' AS id,
						vclinkname,
						ISNULL(bitInitialPage, 0) AS bitInitialPage,
						0 AS bitFavourite
				FROM    ShortCutBar
				WHERE   numTabId = @numTabId
						AND bitdefault = 1
						AND numContactId = 0
				UNION
				SELECT  CONVERT(VARCHAR(10), numListItemID) + '~5' AS id,
						vcData AS vclinkname,
						0 AS bitInitialPage,
						0 AS bitFavourite
				FROM    ListDetails
				WHERE   numListId = 5
						AND numListItemID <> 46
						AND ( numDomainID = @numDomainID
							  OR constflag = 1
							)
						AND @numTabId = 7
			END
            
        END        
        
   
GO

/****** Object:  StoredProcedure [dbo].[USP_GetCartItem]    Script Date: 11/08/2011 18:00:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_GetCartItem' ) 
                    DROP PROCEDURE USP_GetCartItem
                    Go
CREATE PROCEDURE [dbo].[USP_GetCartItem]
(
		 @numUserCntId numeric(18,0),
		 @numDomainId numeric(18,0),
		 @vcCookieId varchar(100),
		 @bitUserType BIT =0--if 0 then anonomyous user 1 for logi user
)
AS
	IF @numUserCntId <> 0
	BEGIN
		SELECT numCartId,
			   numUserCntId,
			   [dbo].[CartItems].numDomainId,
			   vcCookieId,
			   numOppItemCode,
			   [dbo].[CartItems].numItemCode,
			   (SELECT TOP 1 numCategoryId  FROM dbo.ItemCategory WHERE numItemID =  CartItems.numItemCode AND [dbo].[CartItems].numDomainId = @numDomainId ) AS numCategoryId,
			   (SELECT vcCategoryName FROM [dbo].[Category] WHERE [Category].[numCategoryID]  = (SELECT TOP 1 numCategoryId  FROM dbo.ItemCategory WHERE numItemID =  CartItems.numItemCode AND [dbo].[CartItems].numDomainId = @numDomainId )) AS vcCategoryName,
			   numUnitHour,
			   monPrice,
			   monPrice AS [monListPrice],
			   numSourceId,
			   vcItemDesc,
			   vcItemDesc AS txtItemDesc,
			   [dbo].[CartItems].numWarehouseId,
			   [dbo].[CartItems].vcItemName,
			   vcWarehouse,
			   numWarehouseItmsID,
			   vcItemType,
			   vcAttributes,
			   vcAttrValues,
			   [dbo].[CartItems].bitFreeShipping,
			   numWeight,
			   tintOpFlag,
			   bitDiscountType,
			   fltDiscount,
			   monTotAmtBefDiscount,
			   ItemURL,
			   numUOM,
			   vcUOMName,
			   decUOMConversionFactor,
			   numHeight,
			   numLength,
			   numWidth,
			   vcShippingMethod,
			   numServiceTypeId,
			   decShippingCharge,
			   numShippingCompany,
			   tintServicetype,CAST( dtDeliveryDate AS VARCHAR(30)) dtDeliveryDate,monTotAmount,
			   (SELECT vcModelID FROM dbo.Item WHERE Item.numItemCode =  CartItems.numItemCode) AS [vcModelID]
			   ,(SELECT TOP 1 vcPathForTImage FROM dbo.ItemImages II WHERE II.numItemCode = CartItems.numItemCode AND II.numDomainId= CartItems.numDomainId AND II.bitDefault=1) As vcPathForTImage
			   , ISNULL(vcUpSellDesc,'') AS [txtUpSellItemDesc],ISNULL(bitPreUpSell,0) [bitPreUpSell], ISNULL(bitPostUpSell,0) [bitPostUpSell], ISNULL(vcUpSellDesc,'') [vcUpSellDesc]
			   ,ISNULL([I].[vcSKU],'') AS [vcSKU]
			   ,I.vcManufacturer
			   FROM CartItems 
			   LEFT JOIN [dbo].[SimilarItems] AS SI ON SI.[numItemCode] = [dbo].[CartItems].[numItemCode]
			   LEFT JOIN [dbo].[Item] AS I ON I.[numItemCode] = [dbo].[CartItems].[numItemCode]
			   --LEFT JOIN [dbo].[WareHouseItems] AS WHI ON WHI.[numItemID] = I.[numItemCode]
			   WHERE [dbo].[CartItems].numDomainId = @numDomainId AND numUserCntId = @numUserCntId 	
	END
	ELSE
	BEGIN
		SELECT numCartId,
			   numUserCntId,
			   [dbo].[CartItems].numDomainId,
			   vcCookieId,
			   numOppItemCode,
			   [dbo].[CartItems].numItemCode,
			   (SELECT TOP 1 numCategoryId  FROM dbo.ItemCategory WHERE numItemID =  CartItems.numItemCode AND CartItems.numDomainId =@numDomainId ) AS numCategoryId,
			   (SELECT vcCategoryName FROM [dbo].[Category] WHERE [Category].[numCategoryID]  = (SELECT TOP 1 numCategoryId  FROM dbo.ItemCategory WHERE numItemID =  CartItems.numItemCode AND [dbo].[CartItems].numDomainId = @numDomainId )) AS vcCategoryName,
			   numUnitHour,
			   monPrice,
			   monPrice AS [monListPrice],
			   numSourceId,
			   vcItemDesc,
			   vcItemDesc AS txtItemDesc,
			   [dbo].[CartItems].numWarehouseId,
			   [dbo].[CartItems].vcItemName,
			   vcWarehouse,
			   numWarehouseItmsID,
			   vcItemType,
			   vcAttributes,
			   vcAttrValues,
			   [dbo].[CartItems].bitFreeShipping,
			   numWeight,
			   tintOpFlag,
			   bitDiscountType,
			   fltDiscount,
			   monTotAmtBefDiscount,
			   ItemURL,
			   numUOM,
			   vcUOMName,
			   decUOMConversionFactor,
			   numHeight,
			   numLength,
			   numWidth,
			   vcShippingMethod,
			   numServiceTypeId,
			   decShippingCharge,
			   numShippingCompany,
			   tintServicetype,
			   CAST( dtDeliveryDate AS VARCHAR(30)) dtDeliveryDate,
			   monTotAmount,
			   (SELECT vcModelID FROM dbo.Item WHERE Item.numItemCode =  CartItems.numItemCode) AS [vcModelID]
			   ,(SELECT TOP 1 vcPathForTImage FROM dbo.ItemImages II WHERE II.numItemCode = CartItems.numItemCode AND II.numDomainId= CartItems.numDomainId AND II.bitDefault=1) As vcPathForTImage
			   , ISNULL(vcUpSellDesc,'') AS [txtUpSellItemDesc],ISNULL(bitPreUpSell,0) [bitPreUpSell], ISNULL(bitPostUpSell,0) [bitPostUpSell], ISNULL(vcUpSellDesc,'') [vcUpSellDesc]
			   , ISNULL(vcUpSellDesc,'') AS [txtUpSellItemDesc],ISNULL(bitPreUpSell,0) [bitPreUpSell], ISNULL(bitPostUpSell,0) [bitPostUpSell], ISNULL(vcUpSellDesc,'') [vcUpSellDesc]
			   ,ISNULL([I].[vcSKU],'') AS [vcSKU]
			   ,I.vcManufacturer
			   FROM CartItems
			   LEFT JOIN [dbo].[SimilarItems] AS SI ON SI.[numItemCode] = [dbo].[CartItems].[numItemCode]
			   LEFT JOIN [dbo].[Item] AS I ON I.[numItemCode] = [dbo].[CartItems].[numItemCode]
			   --LEFT JOIN [dbo].[WareHouseItems] AS WHI ON WHI.[numItemID] = I.[numItemCode]
			   WHERE [dbo].[CartItems].numDomainId = @numDomainId AND numUserCntId = @numUserCntId AND vcCookieId =@vcCookieId	
	END
	
	

--exec USP_GetCartItem @numUserCntId=1,@numDomainId=1,@vcCookieId='fc3d604b-25fa-4c25-960c-1e317768113e',@bitUserType=NULL


/****** Object:  StoredProcedure [dbo].[usp_GetContactDTlPL]    Script Date: 07/26/2008 16:16:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created By                                          
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getcontactdtlpl')
DROP PROCEDURE usp_getcontactdtlpl
GO
CREATE PROCEDURE [dbo].[usp_GetContactDTlPL]                                                              
@numContactID as numeric(9)=0  ,    
@numDomainID as numeric(9)=0   ,  
@ClientTimeZoneOffset as int                                    
--                                                            
AS                                                              
BEGIN                                                              
                                                              
  SELECT                                      
 A.numContactId,            
A.vcFirstName,           
A.vcLastName,                                                                   
 D.numDivisionID,             
 C.numCompanyId,            
 C.vcCompanyName,             
 D.vcDivisionName,             
 D.numDomainID,        
 tintCRMType,            
 A.numPhone,
 A.numPhoneExtension,
 A.vcEmail,  
 A.numTeam, 
 A.numTeam as vcTeam,         
[dbo].[GetListIemName](A.numTeam) as vcTeamName,                                                             
 A.vcFax,   
 A.numContactType as vcContactType,
 A.numContactType,         
[dbo].[GetListIemName](A.numContactType) as vcContactTypeName,                         
 A.charSex,             
 A.bintDOB,            
 dbo.GetAge(A.bintDOB, getutcdate()) as Age,                                                               
 [dbo].[GetListIemName]( A.vcPosition) as vcPositionName, 
 A.vcPosition,            
 A.txtNotes,             
 A.numCreatedBy,                                                              
 A.numCell,            
 A.NumHomePhone,            
 A.vcAsstFirstName,
 A.vcAsstLastName,            
 A.numAsstPhone,
 A.numAsstExtn,                                                              
 A.vcAsstEmail,            
 A.charSex, 
 case when A.charSex='M' then 'Male' when A.charSex='F' then 'Female' else  '-' end as charSexName,            
[dbo].[GetListIemName]( A.vcDepartment) as vcDepartmentName,  
vcDepartment,                                                             
--    case when AddC.vcPStreet is null then '' when AddC.vcPStreet='' then '' else AddC.vcPStreet + ',' end +             
-- case when AddC.vcPCity is null then '' when AddC.vcPCity='' then ''  else AddC.vcPCity end +             
-- case when  AddC.vcPPostalCode is null then '' when  AddC.vcPPostalCode='' then '' else ','+ AddC.vcPPostalCode end +              
-- case when dbo.fn_GetState(AddC.vcPState) is null then '' else  ','+ dbo.fn_GetState(AddC.vcPState) end +              
-- case when dbo.fn_GetListName(AddC.vcPCountry,0) ='' then '' else ',' + dbo.fn_GetListName(AddC.vcPCountry,0) end      
 dbo.getContactAddress(A.numContactId) as [Address],            
-- AddC.vcPState,            
-- AddC.vcPCountry,            
-- AddC.vcContactLocation,                                    
--  AddC.vcStreet,                                                               
--    AddC.vcCity,             
-- AddC.vcState,             
-- AddC.intPostalCode,                                                               
--  AddC.vcCountry,                                                              
    A.bitOptOut,                                                                         
 dbo.fn_GetContactName(a.numManagerId) as ManagerName,
 a.numManagerId   as   Manager,
[dbo].[GetListIemName](A.vcCategory) as vcCategoryName  ,     
 A.vcCategory,        
 dbo.fn_GetContactName(A.numCreatedBy)+' '+ convert(varchar(20),dateadd(minute,-@ClientTimeZoneOffset,A.bintCreatedDate)) CreatedBy,                                      
 dbo.fn_GetContactName(A.numModifiedBy)+' '+ convert(varchar(20),dateadd(minute,-@ClientTimeZoneOffset,A.bintModifiedDate)) ModifiedBy,            
 vcTitle,            
 vcAltEmail,                                                          
 dbo.fn_GetContactName(A.numRecOwner) as RecordOwner,
 A.numEmpStatus,                  
[dbo].[GetListIemName]( A.numEmpStatus) as numEmpStatusName,                                                          
(select  count(*) from GenericDocuments   where numRecID=A.numContactId and  vcDocumentSection='C') as DocumentCount,                          
(SELECT count(*)from CompanyAssociations where numDivisionID=A.numContactId and bitDeleted=0 ) as AssociateCountFrom,                            
(SELECT count(*)from CompanyAssociations where numAssociateFromDivisionID=A.numContactId and bitDeleted=0 ) as AssociateCountTo,
 ISNULL((SELECT vcECampName FROM [ECampaign] WHERE numECampaignID = A.numECampaignID),'') vcECampName,
 ISNULL(A.numECampaignID,0) numECampaignID,
 ISNULL( (SELECT MAX([numConEmailCampID]) FROM [ConECampaign] WHERE [numContactID] = A.numContactID AND [numECampaignID]= A.numECampaignID AND ISNULL(bitEngaged,0)=1),0) numConEmailCampID,
 ISNULL(A.bitPrimaryContact,0) AS bitPrimaryContact
 FROM AdditionalContactsInformation A INNER JOIN                                                              
 DivisionMaster D ON A.numDivisionId = D.numDivisionID INNER JOIN                                                              
 CompanyInfo C ON D.numCompanyID = C.numCompanyId                                    
-- left join ContactAddress AddC on A.numContactId=AddC.numContactId                                  
 left join UserMaster U on U.numUserDetailId=A.numContactId                                                             
 WHERE A.numContactId = @numContactID and A.numDomainID=@numDomainID                                                     
END
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getcontactlist1')
DROP PROCEDURE usp_getcontactlist1
GO
CREATE PROCEDURE [dbo].[USP_GetContactList1]                                                                                   
@numUserCntID numeric(9)=0,                                                                                    
@numDomainID numeric(9)=0,                                                                                    
@tintUserRightType tinyint=0,                                                                                    
@tintSortOrder tinyint=4,                                                                                    
@SortChar char(1)='0',                                                                                   
@FirstName varChar(100)= '',                                                                                  
@LastName varchar(100)='',                                                                                  
@CustName varchar(100)='',                                                                                  
@CurrentPage int,                                                                                  
@PageSize int,                                                                                  
@columnName as Varchar(50),                                                                                  
@columnSortOrder as Varchar(10),                                                                                  
@numDivisionID as numeric(9)=0,                                              
@bitPartner as bit ,                                                   
@inttype as numeric(9)=0,
@ClientTimeZoneOffset as int,
 @vcRegularSearchCriteria varchar(1000)='',
 @vcCustomSearchCriteria varchar(1000)=''                                                                             
--                                                                                    
as                  
                       
declare @join as varchar(400)                  
set @join = ''                 
                        
 if @columnName like 'CFW.Cust%'                 
begin                
                
                 
 set @join= ' left Join CFW_FLD_Values_Cont CFW on CFW.RecId=ADC.numcontactId  and CFW.fld_id= '+replace(@columnName,'CFW.Cust','') +' '                                                         
 set @columnName='CFW.Fld_Value'                
                  
end                                         
if @columnName like 'DCust%'                
begin                
                                                       
 set @join= ' left Join CFW_FLD_Values_Cont CFW on CFW.RecId=ADC.numcontactId  and CFW.fld_id= '+replace(@columnName,'DCust','')                                                                                                   
 set @join= @join +' left Join ListDetails LstCF on LstCF.numListItemID=CFW.Fld_Value  '                
 set @columnName='LstCF.vcData'                  
                
end                                                                              
                                                                                  
declare @firstRec as integer                                                                                  
declare @lastRec as integer                                                                                  
set @firstRec= (@CurrentPage-1) * @PageSize                              
set @lastRec= (@CurrentPage*@PageSize+1)                                                                                   
                                                                                  
                                                                                  
declare @strSql as varchar(8000)                                                                            
 set  @strSql='with tblcontacts as (Select '                                                                          
      if (@tintSortOrder=5 or @tintSortOrder=6) set  @strSql= @strSql+' top 20 '                                                
                                     
   set  @strSql= @strSql+ ' ROW_NUMBER() OVER (ORDER BY '+ CASE when @columnName='ADC.numAge' then 'ADC.bintDOB' ELSE @columnName end +' '+ @columnSortOrder+') AS RowNumber,
   COUNT(*) OVER(PARTITION BY NULL) AS TotalRowCount,
   ADC.numContactId                                   
  FROM AdditionalContactsInformation ADC                                
  JOIN DivisionMaster DM ON ADC.numDivisionId = DM.numDivisionID                                                
  JOIN CompanyInfo cmp ON DM.numCompanyID = cmp.numCompanyId'  +@join+  '                                                                  
  left join ListDetails LD on LD.numListItemID=cmp.numCompanyRating                                                                           
  left join ListDetails LD1 on LD1.numListItemID= ADC.numEmpStatus 
  LEFT JOIN AddressDetails AD ON AD.numRecordID = ADC.numContactID AND AD.tintAddressOf=1 AND AD.tintAddressType=0 AND AD.bitIsPrimary=1 '                                           
  if @bitPartner=1 set @strSql=@strSql + ' left join CompanyAssociations CA on CA.numAssociateFromDivisionID=DM.numDivisionID and bitShareportal=1 and CA.bitDeleted=0                                                 
and CA.numDivisionID=(select numDivisionID from AdditionalContactsInformation where numContactID='+convert(varchar(15),@numUserCntID)+ ')'                                                                   
                                                                    
 if @tintSortOrder= 7 set @strSql=@strSql+' join Favorites F on F.numContactid=ADC.numContactID '                                                                     
                                                       
  set  @strSql= @strSql +'                                                                
        where 
 --DM.tintCRMType<>0     AND 
 cmp.numDomainID=DM.numDomainID   
 and DM.numDomainID=ADC.numDomainID                                             
    and DM.numDomainID  = '+convert(varchar(15),@numDomainID)+''                             
 if @inttype<>0 and @inttype<>101  set @strSql=@strSql+' and ADC.numContactType  = '+convert(varchar(10),@inttype)                            
 if @inttype=101  set @strSql=@strSql+' and ADC.bitPrimaryContact  =1 '                            
    if @FirstName<>'' set @strSql=@strSql+' and ADC.vcFirstName  like '''+@FirstName+'%'''                                                             
    if @LastName<>'' set @strSql=@strSql+' and ADC.vcLastName like '''+@LastName+'%'''                                              
    if @CustName<>'' set @strSql=@strSql+' and cmp.vcCompanyName like '''+@CustName+'%'''                                                                                  
if @SortChar<>'0' set @strSql=@strSql + ' And ADC.vcFirstName like '''+@SortChar+'%'''                                                                                           
if @tintUserRightType=1 set @strSql=@strSql + ' AND (ADC.numRecOwner = '+convert(varchar(15),@numUserCntID)+ ')' + case when @bitPartner=1 then ' or (CA.bitShareportal=1 and  CA.bitDeleted=0)' else '' end                                                  
   
else if @tintUserRightType=2 set @strSql=@strSql + ' AND (DM.numTerID in (select numTerritoryID from  UserTerritory where numUserCntID= '+convert(varchar(15),@numUserCntID)+' ) or DM.numTerID=0) '                                                           
                
if @numDivisionID <> 0  set @strSql=@strSql + ' And DM.numDivisionID ='+ convert(varchar(10),@numDivisionID)                      
                                                                                         
if @tintSortOrder=2  set @strSql=@strSql + ' AND (ADC.numRecOwner = '+convert(varchar(15),@numUserCntID)+ ')'                                                                                      
else if @tintSortOrder=3  set @strSql=@strSql + '  AND ADC.numContactType=92  '                                                                          
else if @tintSortOrder=4  set @strSql=@strSql + ' AND ADC.bintCreatedDate > '''+convert(varchar(20),dateadd(day,-7,getutcdate()))+''''                                    
else if @tintSortOrder=5  set @strSql=@strSql + ' and ADC.numCreatedby='+convert(varchar(15),@numUserCntID)                      
                              
else if @tintSortOrder=6  set @strSql=@strSql + ' and ADC.numModifiedby='+convert(varchar(15),@numUserCntID)                      
                                                     
else if @tintSortOrder=7  set @strSql=@strSql + ' and F.numUserCntID='+convert(varchar(15),@numUserCntID)                                                                             
                    
 if (@tintSortOrder=5 and @columnName!='ADC.bintcreateddate')  set @strSql='select * from ('+@strSql+')X order by '+ @columnName +' '+ @columnSortOrder                        
else if (@tintSortOrder=6 and @columnName!='ADC.bintcreateddate')  set @strSql='select * from ('+@strSql+')X order by '+ @columnName +' '+ @columnSortOrder                                                                
else if @tintSortOrder=7  set @strSql=@strSql + ' and cType=''C'' '                      
    
IF @vcRegularSearchCriteria<>'' set @strSql=@strSql+' and ' + @vcRegularSearchCriteria 
IF @vcCustomSearchCriteria<>'' set @strSql=@strSql+' and ADC.numContactId in (select distinct CFW.RecId from CFW_FLD_Values_Cont CFW where ' + @vcCustomSearchCriteria + ')'

set @strSql=@strSql + ')'                      
                                                                                 
declare @tintOrder as tinyint                                                        
declare @vcFieldName as varchar(50)                                                        
declare @vcListItemType as varchar(3)                                                   
declare @vcListItemType1 as varchar(1)                                                       
declare @vcAssociatedControlType varchar(20)                                                        
declare @numListID AS numeric(9)                                                        
declare @vcDbColumnName varchar(20)                            
declare @WhereCondition varchar(2000)                             
declare @vcLookBackTableName varchar(2000)                      
Declare @bitCustom as bit  
DECLARE @bitAllowEdit AS CHAR(1)                 
Declare @numFieldId as numeric  
DECLARE @bitAllowSorting AS CHAR(1)                   
declare @vcColumnName AS VARCHAR(500)                            
set @tintOrder=0                                                        
set @WhereCondition =''                       
                         
declare @Nocolumns as tinyint                      
set @Nocolumns=0                      
  
select @Nocolumns=isnull(sum(TotalRow),0)  from(            
Select count(*) TotalRow from View_DynamicColumns where numFormId=10 and numUserCntID=@numUserCntID and numDomainID=@numDomainID AND tintPageType=1 AND numRelCntType=@inttype 
Union 
Select count(*) TotalRow from View_DynamicCustomColumns where numFormId=10 and numUserCntID=@numUserCntID and numDomainID=@numDomainID AND tintPageType=1 AND numRelCntType=@inttype) TotalRows

                   
--declare @strColumns as varchar(2000)                      
--set @strColumns=''                      
--while @DefaultNocolumns>0                      
--begin                      
--                  
-- set @strColumns=@strColumns+',null'                      
-- set @DefaultNocolumns=@DefaultNocolumns-1                      
--end                   
                     
   CREATE TABLE #tempForm (tintOrder TINYINT,vcDbColumnName nvarchar(50),vcOrigDbColumnName nvarchar(50),vcFieldName nvarchar(50),vcAssociatedControlType nvarchar(50),
  vcListItemType varchar(3),numListID numeric(9),vcLookBackTableName varchar(50),bitCustomField BIT,
  numFieldId NUMERIC,bitAllowSorting BIT,bitAllowEdit BIT,bitIsRequired BIT,bitIsEmail BIT,bitIsAlphaNumeric BIT,bitIsNumeric BIT,
  bitIsLengthValidation BIT,intMaxLength int,intMinLength int,bitFieldMessage BIT,vcFieldMessage VARCHAR(500),ListRelID numeric(9),intColumnWidth int,bitAllowFiltering BIT,vcFieldDataType CHAR(1))

--Check if Master Form Configuration is created by administrator if NoColumns=0
DECLARE @IsMasterConfAvailable BIT = 0
DECLARE @numUserGroup NUMERIC(18,0)

IF @Nocolumns=0
BEGIN
	SELECT @numUserGroup = numGroupID FROM UserMaster WHERE numUserDetailId = @numUserCntID 

	IF (SELECT COUNT(*) FROM BizFormWizardMasterConfiguration WHERE numDomainID = @numDomainID AND numFormID = 10 AND numRelCntType=@inttype AND bitGridConfiguration = 1 AND numGroupID = @numUserGroup) > 0
	BEGIN
		SET @IsMasterConfAvailable = 1
	END
END


--If MasterConfiguration is available then load it otherwise load default columns
IF @IsMasterConfAvailable = 1
BEGIN
	INSERT INTO #tempForm
	SELECT 
		(intRowNum + 1) as tintOrder, vcDbColumnName,	vcOrigDbColumnName,	ISNULL(vcCultureFieldName,vcFieldName), vcAssociatedControlType,
		vcListItemType, numListID, vcLookBackTableName,	bitCustom, numFieldId, bitAllowSorting,	bitInlineEdit, bitIsRequired,
		bitIsEmail,	bitIsAlphaNumeric, bitIsNumeric, bitIsLengthValidation,	intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage vcFieldMessage,
		ListRelID,intColumnWidth,bitAllowFiltering,vcFieldDataType 
	FROM 
		View_DynamicColumnsMasterConfig
	WHERE
		View_DynamicColumnsMasterConfig.numFormId=10 AND 
		View_DynamicColumnsMasterConfig.numDomainID=@numDomainID AND 
		View_DynamicColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
		View_DynamicColumnsMasterConfig.numRelCntType=@inttype AND 
		View_DynamicColumnsMasterConfig.bitGridConfiguration = 1 AND
		ISNULL(View_DynamicColumnsMasterConfig.bitSettingField,0)=1 AND 
		ISNULL(View_DynamicColumnsMasterConfig.bitCustom,0)=0
	UNION
    SELECT 
		tintRow+1 as tintOrder,vcDbColumnName,vcDbColumnName,vcFieldName, vcAssociatedControlType,'' as vcListItemType,numListID,'',
		bitCustom,numFieldId,bitAllowSorting,bitAllowEdit,bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
		intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage,ListRelID,intColumnWidth,0,''
	FROM 
		View_DynamicCustomColumnsMasterConfig
	WHERE 
		View_DynamicCustomColumnsMasterConfig.numFormId=10 AND 
		View_DynamicCustomColumnsMasterConfig.numDomainID=@numDomainID AND 
		View_DynamicCustomColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
		View_DynamicCustomColumnsMasterConfig.numRelCntType=@inttype AND 
		View_DynamicCustomColumnsMasterConfig.bitGridConfiguration = 1 AND
		ISNULL(View_DynamicCustomColumnsMasterConfig.bitCustom,0)=1
	ORDER BY 
		tintOrder ASC  
END
ELSE                                        
BEGIN                 
     
	if @Nocolumns=0
	BEGIN
	INSERT INTO DycFormConfigurationDetails (numFormID,numFieldID,intColumnNum,intRowNum,numDomainID,numUserCntID,numRelCntType,tintPageType,bitCustom,intColumnWidth)
	select 10,numFieldId,0,Row_number() over(order by tintRow desc),@numDomainID,@numUserCntID,@inttype,1,0,intColumnWidth
	 FROM View_DynamicDefaultColumns
	 where numFormId=10 and bitDefault=1 AND ISNULL(bitSettingField,0)=1 and numDomainID=@numDomainID
	order by tintOrder asc 
	END
                                   
   INSERT INTO #tempForm
select tintRow+1 as tintOrder,vcDbColumnName,vcOrigDbColumnName,ISNULL(vcCultureFieldName,vcFieldName),                  
 vcAssociatedControlType,vcListItemType,numListID,vcLookBackTableName                                                 
,bitCustom,numFieldId,bitAllowSorting,bitInlineEdit,
bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage vcFieldMessage,ListRelID ,intColumnWidth,bitAllowFiltering,vcFieldDataType
 FROM View_DynamicColumns 
 where numFormId=10 and numUserCntID=@numUserCntID and numDomainID=@numDomainID AND tintPageType=1 AND numRelCntType=@inttype
  AND ISNULL(bitSettingField,0)=1 AND ISNULL(bitCustom,0)=0
       
       UNION
    
   select tintRow+1 as tintOrder,vcDbColumnName,vcDbColumnName,vcFieldName,                  
 vcAssociatedControlType,'' as vcListItemType,numListID,''                                                 
,bitCustom,numFieldId,bitAllowSorting,bitAllowEdit,
bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage,ListRelID,intColumnWidth,0,''
 from View_DynamicCustomColumns
 where numFormId=10 and numUserCntID=@numUserCntID and numDomainID=@numDomainID AND tintPageType=1 AND numRelCntType=@inttype
 AND ISNULL(bitCustom,0)=1
 
  order by tintOrder asc  

END
   
set @strSql=@strSql+' select convert(varchar(10),isnull(ADC.numContactId,0)) as numContactId            
       ,ISNULL(ADC.numECampaignID,0) as numECampaignID
	   ,ISNULL( (SELECT MAX([numConEmailCampID]) FROM [ConECampaign] WHERE [numContactID] = ADC.numContactId AND [numECampaignID]= ADC.numECampaignID AND ISNULL(bitEngaged,0)=1),0) numConEmailCampID
	   ,ISNULL((SELECT ISNULL(VIE.Total,0) FROM View_InboxEmail VIE  WITH (NOEXPAND) where VIE.numDomainID = ADC.numDomainID AND  VIE.numContactId = ADC.numContactId),0) as TotalEmail
	   ,ISNULL((SELECT ISNULL(VOA.OpenActionItemCount,0) AS TotalActionItem FROM VIEW_OPEN_ACTION_ITEMS VOA WITH (NOEXPAND)  where VOA.numDomainID = ADC.numDomainID AND  VOA.numDivisionId = DM.numDivisionID),0) as TotalActionItem
       ,ISNULL(DM.numDivisionID,0) numDivisionID, isnull(DM.numTerID,0)numTerID,isnull( ADC.numRecOwner,0) numRecOwner,isnull( DM.tintCRMType,0) tintCRMType ,RowNumber'                      
                                                    
Declare @ListRelID as numeric(9) 
  
    select top 1 @tintOrder=tintOrder+1,@vcDbColumnName=vcDbColumnName,@vcFieldName=vcFieldName,                
  @vcAssociatedControlType=vcAssociatedControlType,@vcListItemType=vcListItemType,@numListID=numListID,@vcLookBackTableName=vcLookBackTableName                                               
,@bitCustom=bitCustomField,@numFieldId=numFieldId,@bitAllowSorting=bitAllowSorting,@bitAllowEdit=bitAllowEdit,@ListRelID=ListRelID
  from  #tempForm order by tintOrder asc            

                   
while @tintOrder>0                                                        
begin                                                        
                                            
   if @bitCustom = 0                
 begin           
        
    declare @Prefix as varchar(5)                      
      if @vcLookBackTableName = 'AdditionalContactsInformation'                      
    set @Prefix = 'ADC.'                      
      if @vcLookBackTableName = 'DivisionMaster'                      
    set @Prefix = 'DM.'         
        
                  SET @vcColumnName= @vcDbColumnName+'~'+ convert(varchar(10),@numFieldId) +'~0'
                          
 if @vcAssociatedControlType='SelectBox'                                                        
        begin                                                        
                                                        
     if @vcListItemType='LI'                                                         
     begin                                                        
      set @strSql=@strSql+',L'+ convert(varchar(3),@tintOrder)+'.vcData'+' ['+ @vcColumnName+']'                                                       
      set @WhereCondition= @WhereCondition +' left Join ListDetails L'+ convert(varchar(3),@tintOrder)+ ' on L'+convert(varchar(3),@tintOrder)+ '.numListItemID='+@vcDbColumnName                                                        
     end                                                        
     else if @vcListItemType='S'                                                         
     begin                                                        
      set @strSql=@strSql+',S'+ convert(varchar(3),@tintOrder)+'.vcState'+' ['+ @vcColumnName+']'                                                        
      set @WhereCondition= @WhereCondition +' left join State S'+ convert(varchar(3),@tintOrder)+ ' on S'+convert(varchar(3),@tintOrder)+ '.numStateID='+@vcDbColumnName                                                        
     end                                                        
     else if @vcListItemType='T'                                                         
     begin                                                        
      set @strSql=@strSql+',L'+ convert(varchar(3),@tintOrder)+'.vcData'+' ['+ @vcColumnName+']'                                                        
      set @WhereCondition= @WhereCondition +' left Join ListDetails L'+ convert(varchar(3),@tintOrder)+ ' on L'+convert(varchar(3),@tintOrder)+ '.numListItemID='+@vcDbColumnName                                                        
     end                       
     else if   @vcListItemType='U'                                                     
    begin                       
    set @strSql=@strSql+',dbo.fn_GetContactName('+@Prefix+@vcDbColumnName+') ['+ @vcColumnName+']'                                                        
    end                      
    end         
 else if @vcAssociatedControlType='DateField'                                                      
 begin                
           
     set @strSql=@strSql+',case when convert(varchar(11),DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+','+@Prefix+@vcDbColumnName+') )= convert(varchar(11),getdate()) then ''<b><font color=red>Today</font></b>'''          
     set @strSql=@strSql+'when convert(varchar(11),DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+','+@Prefix+@vcDbColumnName+') )= convert(varchar(11),dateadd(day,-1,getdate())) then ''<b><font color=purple>YesterDay</font></b>'''          
     set @strSql=@strSql+'when convert(varchar(11),DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+','+@Prefix+@vcDbColumnName+') )= convert(varchar(11),dateadd(day,1,getdate())) then''<b><font color=orange>Tommorow</font></b>'' '          
     set @strSql=@strSql+'else dbo.FormatedDateFromDate(DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+','+@Prefix+@vcDbColumnName+'),'+convert(varchar(10),@numDomainId)+') end  ['+ @vcColumnName+']'                    
   end        
              
else if @vcAssociatedControlType='TextBox' OR @vcAssociatedControlType='Label'                                                     
begin        
         
     set @strSql=@strSql+','+ case  when @vcDbColumnName='numAge' then 'year(getutcdate())-year(bintDOB)'         
 when @vcDbColumnName='numDivisionID' then 'DM.numDivisionID' else @vcDbColumnName end+' ['+ @vcColumnName+']'                   
 end      
else                                                
begin    
	if @vcDbColumnName='vcCampaignAudit'
		set @strSql=@strSql+', dbo.GetContactCampignAuditDetail(ADC.numECampaignID,ADC.numContactId) ['+ @vcColumnName+']'  
	else
		set @strSql=@strSql+', '+ @vcDbColumnName +' ['+ @vcColumnName+']'                 
 end                                               
            
              
                    
end                
else if @bitCustom = 1                
begin                
            
	SET @vcColumnName= @vcDbColumnName +'~'+ convert(varchar(10),@numFieldId)+'~1'
       
   select @vcFieldName=FLd_label,@vcAssociatedControlType=fld_type,@vcDbColumnName='Cust'+Convert(varchar(10),Fld_Id)                               
    from CFW_Fld_Master                                                
   where    CFW_Fld_Master.Fld_Id = @numFieldId                 
                
                 
    if @vcAssociatedControlType = 'TextBox' or @vcAssociatedControlType = 'TextArea'             
   begin                
                   
    set @strSql= @strSql+',CFW'+ convert(varchar(3),@tintOrder)+'.Fld_Value  ['+ @vcColumnName +']'                   
    set @WhereCondition= @WhereCondition +' left Join CFW_FLD_Values_Cont CFW'+ convert(varchar(3),@tintOrder)+ '                 
    on CFW'+ convert(varchar(3),@tintOrder)+ '.Fld_Id='+convert(varchar(10),@numFieldId) +'and CFW'+ convert(varchar(3),@tintOrder)+ '.RecId=ADC.numContactId   '                                                         
   end      
    else if @vcAssociatedControlType = 'CheckBox'             
   begin              
                 
    set @strSql= @strSql+',case when isnull(CFW'+ convert(varchar(3),@tintOrder)+'.Fld_Value,0)=0 then ''No'' when isnull(CFW'+ convert(varchar(3),@tintOrder)+'.Fld_Value,0)=1 then ''Yes'' end   ['+ @vcColumnName +']'              
   
    set @WhereCondition= @WhereCondition +' left Join CFW_FLD_Values_Cont CFW'+ convert(varchar(3),@tintOrder)+ '               
    on CFW'+ convert(varchar(3),@tintOrder)+ '.Fld_Id='+convert(varchar(10),@numFieldId) +'and CFW'+ convert(varchar(3),@tintOrder)+ '.RecId=ADC.numContactId   '                                                       
   end              
  else if @vcAssociatedControlType = 'DateField'            
   begin                
                   
    set @strSql= @strSql+',dbo.FormatedDateFromDate(CFW'+ convert(varchar(3),@tintOrder)+'.Fld_Value,'+convert(varchar(10),@numDomainId)+')  ['+ @vcColumnName +']'                   
    set @WhereCondition= @WhereCondition +' left Join CFW_FLD_Values_Cont CFW'+ convert(varchar(3),@tintOrder)+ '                 
    on CFW'+ convert(varchar(3),@tintOrder)+ '.Fld_Id='+convert(varchar(10),@numFieldId) +'and CFW'+ convert(varchar(3),@tintOrder)+ '.RecId=ADC.numContactId   '                                                         
   end                
   else if @vcAssociatedControlType = 'SelectBox'            
   begin                
    set @vcDbColumnName = 'DCust'+Convert(varchar(10),@numFieldId)                
    set @strSql=@strSql+',L'+ convert(varchar(3),@tintOrder)+'.vcData'+' ['+ @vcColumnName +']'                                                          
    set @WhereCondition= @WhereCondition +' left Join CFW_FLD_Values_Cont CFW'+ convert(varchar(3),@tintOrder)+ '                 
     on CFW'+ convert(varchar(3),@tintOrder)+ '.Fld_Id='+convert(varchar(10),@numFieldId) +'and CFW'+ convert(varchar(3),@tintOrder)+ '.RecId=ADC.numContactId   '                                                         
    set @WhereCondition= @WhereCondition +' left Join ListDetails L'+ convert(varchar(3),@tintOrder)+ ' on L'+convert(varchar(3),@tintOrder)+ '.numListItemID=CFW'+ convert(varchar(3),@tintOrder)+'.Fld_Value'                
   end                 
end                    
                    
   select top 1 @tintOrder=tintOrder+1,@vcDbColumnName=vcDbColumnName,@vcFieldName=vcFieldName,                
  @vcAssociatedControlType=vcAssociatedControlType,@vcListItemType=vcListItemType,@numListID=numListID,@vcLookBackTableName=vcLookBackTableName                                               
,@bitCustom=bitCustomField,@numFieldId=numFieldId,@bitAllowSorting=bitAllowSorting,@bitAllowEdit=bitAllowEdit,@ListRelID=ListRelID
  from #tempForm WHERE tintOrder > @tintOrder-1 order by tintOrder asc            
 
  if @@rowcount=0 set @tintOrder=0 

                                  
end                             
                          
       
-------Change Row Color-------
Declare @vcCSOrigDbCOlumnName as varchar(50),@vcCSLookBackTableName as varchar(50),@vcCSAssociatedControlType as varchar(50)
SET @vcCSOrigDbCOlumnName=''
SET @vcCSLookBackTableName=''

Create table #tempColorScheme(vcOrigDbCOlumnName varchar(50),vcLookBackTableName varchar(50),vcFieldValue varchar(50),vcFieldValue1 varchar(50),vcColorScheme varchar(50),vcAssociatedControlType varchar(50))

insert into #tempColorScheme select DFM.vcOrigDbCOlumnName,DFM.vcLookBackTableName,DFCS.vcFieldValue,DFCS.vcFieldValue1,DFCS.vcColorScheme,DFFM.vcAssociatedControlType 
from DycFieldColorScheme DFCS join DycFormField_Mapping DFFM on DFCS.numFieldID=DFFM.numFieldID AND DFCS.numFormID=DFFM.numFormID
join DycFieldMaster DFM on DFM.numModuleID=DFFM.numModuleID and DFM.numFieldID=DFFM.numFieldID
where DFCS.numDomainID=@numDomainID and DFFM.numFormID=10 AND DFCS.numFormID=10 and isnull(DFFM.bitAllowGridColor,0)=1

IF(SELECT COUNT(*) FROM #tempColorScheme)>0
BEGIN
   select top 1 @vcCSOrigDbCOlumnName=vcOrigDbCOlumnName,@vcCSLookBackTableName=vcLookBackTableName,@vcCSAssociatedControlType=vcAssociatedControlType from #tempColorScheme
END   
---------------------------- 
 
IF len(@vcCSOrigDbCOlumnName)>0 and len(@vcCSLookBackTableName)>0
BEGIN
set @strSql=@strSql+',tCS.vcColorScheme'
END                         
                                  
set @strSql=@strSql+' ,TotalRowCount FROM AdditionalContactsInformation ADC                                                                             
  JOIN DivisionMaster DM ON ADC.numDivisionId = DM.numDivisionID                                                                            
  JOIN CompanyInfo CI ON DM.numCompanyID = CI.numCompanyId
  LEFT JOIN AddressDetails AD ON AD.numRecordID = ADC.numContactID AND AD.tintAddressOf=1 AND AD.tintAddressType=0 AND AD.bitIsPrimary=1
   '+@WhereCondition+                      
' join tblcontacts T on T.numContactId=ADC.numContactId'                     
  
--' union select convert(varchar(10),count(*)),null,null,null,null,null '+@strColumns+' from tblcontacts order by RowNumber'                                                 

IF len(@vcCSOrigDbCOlumnName)>0 and len(@vcCSLookBackTableName)>0
BEGIN
	 if @vcCSLookBackTableName = 'AdditionalContactsInformation'                  
		set @Prefix = 'ADC.'                  
     if @vcCSLookBackTableName = 'DivisionMaster'                  
		set @Prefix = 'DM.'
	 if @vcCSLookBackTableName = 'CompanyInfo'                  
		set @Prefix = 'CI.' 
	 if @vcCSLookBackTableName = 'AddressDetails'                  
		set @Prefix = 'AD.'   
 
	IF @vcCSAssociatedControlType='DateField'
	BEGIN
--		set @strSql=@strSql+' left join #tempColorScheme tCS on ' + @Prefix + @vcCSOrigDbCOlumnName + '> DateAdd(day,CAST(SUBSTRING(tCS.vcFieldValue,1,CHARINDEX(''~'',tCS.vcFieldValue)-1) AS INT),GETDATE())
--			 and ' + @Prefix + @vcCSOrigDbCOlumnName + '< DateAdd(day,CAST(SUBSTRING(tCS.vcFieldValue,CHARINDEX(''~'',tCS.vcFieldValue)+1,len(tCS.vcFieldValue) - PATINDEX(''~'',tCS.vcFieldValue)) AS INT),GETDATE())'
		
		
			set @strSql=@strSql+' left join #tempColorScheme tCS on CONVERT(VARCHAR(10),' + @Prefix + @vcCSOrigDbCOlumnName + ',111) >= CONVERT(VARCHAR(10),DateAdd(day,cast(tCS.vcFieldValue as int),GETUTCDATE()),111)
			 and CONVERT(VARCHAR(10),' + @Prefix + @vcCSOrigDbCOlumnName + ',111) <= CONVERT(VARCHAR(10),DateAdd(day,cast(tCS.vcFieldValue1 as int),GETUTCDATE()),111)'

	END
	ELSE IF @vcCSAssociatedControlType='SelectBox' OR @vcCSAssociatedControlType='ListBox' OR @vcCSAssociatedControlType='CheckBox'
	BEGIN
		set @strSql=@strSql+' left join #tempColorScheme tCS on tCS.vcFieldValue=' + @Prefix + @vcCSOrigDbCOlumnName
	END
END
    

set @strSql=@strSql+' WHERE CI.numDomainID=DM.numDomainID   
  and DM.numDomainID=ADC.numDomainID   
  and RowNumber > '+convert(varchar(10),@firstRec)+ ' and RowNumber <'+ convert(varchar(10),@lastRec)+ ' order by RowNumber'

                                                     
print @strSql                              
                      
exec (@strSql)


SELECT * FROM #tempForm

DROP TABLE #tempForm  

drop table #tempColorScheme
/****** Object:  StoredProcedure [dbo].[Usp_getCorrespondance]    Script Date: 07/26/2008 16:17:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getcorrespondance')
DROP PROCEDURE usp_getcorrespondance
GO
CREATE PROCEDURE [dbo].[Usp_getCorrespondance]                          
@FromDate as datetime,                          
@toDate as datetime,                          
@numContactId as numeric,                          
@vcMessageFrom as varchar(200)='' ,                        
@tintSortOrder as numeric,                        
@SeachKeyword as varchar(100),                        
@CurrentPage as numeric,                        
@PageSize as numeric,                        
@TotRecs as numeric output,                        
@columnName as varchar(100),                        
@columnSortOrder as varchar(50),                        
@filter as numeric,                        
@numdivisionId as numeric =0,              
@numDomainID as numeric(9)=0,              
@numCaseID as numeric(9)=0,              
@ClientTimeZoneOffset INT,
@numOpenRecordID NUMERIC=0,
@tintMode TINYINT=0,
@bitOpenCommu BIT=0
as 

set @vcMessageFrom=replace(@vcMessageFrom,'''','|')
                         
if @filter>0 set @tintSortOrder=  @filter
               
if ((@filter <=2 and @tintSortOrder<=2) or (@tintSortOrder>2 and @filter>2))                 
begin  
                                                                      
 declare @strSql as varchar(8000)       
                                  
  declare @firstRec as integer                                                        
  declare @lastRec as integer                                                        
  set @firstRec= (@CurrentPage-1) * @PageSize                                                        
  set @lastRec= (@CurrentPage*@PageSize+1)                 
  set @strSql=''                
 if (@filter <=2 and @tintSortOrder<=2 and @bitOpenCommu=0)                 
 begin     
 declare @strCondition as varchar(4500);
 declare @vcContactEmail as varchar(100);
              
  if @numdivisionId =0 and  @vcMessageFrom <> '' 
 BEGIN 
	If @filter = 1 --ReceivedMessages
		set  @strCondition=' and (vcFrom like ''%'+ @vcMessageFrom+'%'')  ' 
	ELSE IF @filter=2 --SentMessages
		set  @strCondition=' and (vcTo like ''%'+ @vcMessageFrom+'%'')  ' 
	ELSE
		set  @strCondition=' and (vcFrom like ''%'+ @vcMessageFrom+'%'' or vcTo like ''%'+ @vcMessageFrom+'%'')  ' 
 END
  else if @numdivisionId <>0 
 BEGIN

 /* This approach does not work as numEmailId stores who sent email, id of sender
  set @strCondition=' and HDR.numEmailID in (
  SELECT numEmailID FROM dbo.EmailMaster WHERE numDomainID= ' + CONVERT(VARCHAR(15),@numDomainID) +' AND numContactID 
  IN (SELECT numcontactid FROM dbo.AdditionalContactsInformation WHERE numDivisionID = ' + convert(varchar(15),@numDivisionID) + ' AND numDomainID = ' + CONVERT(VARCHAR(15),@numDomainID) +' ))';
*/
-- bug fix id 1591,1628 
  DECLARE @TnumContactID NUMERIC(9);set @strCondition=''

  select TOP 1 @TnumContactID=numContactID,@vcContactEmail=replace(vcEmail,'''','''''') from AdditionalContactsInformation where numdivisionid=@numdivisionId and len(vcEmail)>2
  ORDER BY numContactID ASC
  
  WHILE @TnumContactID>0
  BEGIN
   if len(@strCondition)>0
    SET @strCondition=@strCondition + ' or '

	If @filter = 1 --ReceivedMessages
		set  @strCondition=' and (vcFrom like ''%'+ @vcContactEmail+'%'')  ' 
	ELSE IF @filter=2 --SentMessages
		set  @strCondition=' and (vcTo like ''%'+ @vcContactEmail+'%'')  ' 
	ELSE
		set  @strCondition=' and (vcFrom like ''%'+ @vcContactEmail+'%'' or vcTo like ''%'+ @vcContactEmail+'%'')  '

   select TOP 1 @TnumContactID=numContactID,@vcContactEmail=vcEmail from AdditionalContactsInformation where numdivisionid=@numdivisionId and len(vcEmail)>2 and numContactID>@TnumContactID
   ORDER BY numContactID ASC

   IF @@rowcount = 0 SET @TnumContactID = 0 
  END
 
  if len(@strCondition)>0
   SET @strCondition= @strCondition
  else
   SET @strCondition=' and (1=0)' 
  --set @strCondition=' and DTL.numEmailId  in (select numEmailId from EmailMaster where vcEmailID in (select vcEmail  from AdditionalContactsInformation where numdivisionid='''+ convert(varchar(15),@numdivisionId)+''' ))    '          
  END
  else if @numContactId>0 
 BEGIN
  select @vcContactEmail=isnull(replace(vcEmail,'''',''''''),'') from AdditionalContactsInformation where numContactID=@numContactId and len(vcEmail)>2

  if len(@vcContactEmail)>0
	If @filter = 1 --ReceivedMessages
		set  @strCondition=' and (vcFrom like ''%'+ @vcContactEmail +'%'')  ' 
	ELSE IF @filter=2 --SentMessages
		set  @strCondition=' and (vcTo like ''%'+ @vcContactEmail+'%'')  ' 
	ELSE
		set  @strCondition=' and (vcFrom like ''%'+ @vcContactEmail+'%'' or vcTo like ''%'+ @vcContactEmail+'%'')' 
  else
   SET @strCondition=' and (1=0)'
 END
 else 
  SET @strCondition=''
    
  set @strSql= '                        
  SELECT distinct(HDR.numEmailHstrID),convert(varchar(15),HDR.numEmailHstrID)+''~1~0''+convert(varchar(15),HDR.numUserCntId) as DelData,HDR.bintCreatedOn,
  dbo.FormatedDateTimeFromDate(DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+', bintCreatedOn),'+convert(varchar(15),@numDomainID)+') as [date],                
  vcSubject as [Subject],                          
  ''Email'' as [type] ,                          
  '''' as phone,'''' as assignedto ,                          
  ''0'' as caseid,                             
  null as vcCasenumber,                                 
  ''0''as caseTimeid,                                
  ''0''as caseExpid ,                          
  hdr.tinttype ,HDR.bintCreatedOn as dtCreatedDate,vcFrom + '','' + vcTo as [From] ,0 as bitClosedflag,ISNULL(HDR.bitHasAttachments,0) bitHasAttachments,
 CASE WHEN LEN(Cast(vcBodyText AS VARCHAR(1000)))>150 THEN SUBSTRING(vcBodyText,0,150) + ''...'' ELSE
   Cast(vcBodyText AS VARCHAR(1000)) END AS vcBody                     
  from EmailHistory HDR                                 
  LEFT JOIN [Correspondence] CR ON CR.numEmailHistoryID = HDR.numEmailHstrID '                    
   set  @strSql=@strSql +' where ISNULL(HDR.bitInvisible,0) = 0 AND HDR.numDomainID='+convert(varchar(15),@numDomainID)+' and  (bintCreatedOn between '''+ convert(varchar(30),@FromDate)+''' and '''+ convert(varchar(30),@ToDate)+''') '
   + ' AND (CR.numOpenRecordID = '+ convert(varchar(15),@numOpenRecordID) +' OR '+ convert(varchar(15),@numOpenRecordID) +'=0)'
  
 set  @strSql=@strSql+ @strCondition
--  if @numdivisionId =0 and  @vcMessageFrom <> '' set  @strSql=@strSql+ 'and (vcFrom like ''%'+ @vcMessageFrom+'%'' or vcTo like ''%'+ @vcMessageFrom+'%'')  '                      
--  else if @numdivisionId <>0 
-- BEGIN
--  set @strSql=@strSql+ 'and DTL.numEmailId  in (select numEmailId from EmailMaster where vcEmailID in (select vcEmail  from AdditionalContactsInformation where numdivisionid='''+ convert(varchar(15),@numdivisionId)+''' ))    '          
--  END
--  else if @numContactId>0 
-- BEGIN
--  declare @vcContactEmail as varchar(100);
--  select @vcContactEmail=vcEmail from AdditionalContactsInformation where numContactID=@numContactId
--
--  set  @strSql=@strSql+ 'and (vcFrom like ''%'+ @vcContactEmail+'%'' or vcTo like ''%'+ @vcContactEmail+'%'')    ' 
-- END

  IF @tintMode=1 SET @strSql=@strSql+ ' and  (CR.tintCorrType IN(1,3) OR '+ convert(varchar(15),@numOpenRecordID) +'=0)'
  IF @tintMode=2 SET @strSql=@strSql+ ' and  (CR.tintCorrType IN(2,4) OR '+ convert(varchar(15),@numOpenRecordID) +'=0)'
  IF @tintMode=5 SET @strSql=@strSql+ ' and  (CR.tintCorrType IN(5) OR '+ convert(varchar(15),@numOpenRecordID) +'=0)'
  IF @tintMode=6 SET @strSql=@strSql+ ' and  (CR.tintCorrType IN(6) OR '+ convert(varchar(15),@numOpenRecordID) +'=0)'
  
  if @filter =0 set  @strSql=@strSql+ ' and (hdr.tinttype=1 or hdr.tinttype=2 or  hdr.tinttype=5)'                  
  else if @filter =1 OR @filter =2 set  @strSql=@strSql+ ' and (hdr.tinttype=1 OR hdr.tinttype=2)'                                             
  if @filter > 2 set  @strSql=@strSql+ ' and hdr.tinttype=0'                 
                        
  if @tintSortOrder =0 and @SeachKeyword <> ''                 
  begin                
   set  @strSql=@strSql+ ' and (vcFrom like ''%'+ @SeachKeyword+'%'' or vcTo like ''%'+ @SeachKeyword+'%'' or vcsubject like ''%'+@SeachKeyword+'%'' or vcBody like ''%'+@SeachKeyword+'%'')'               
  end                
  if @tintSortOrder =1 and @SeachKeyword <> ''                 
  begin                
     set  @strSql=@strSql+ ' and (vcFrom like ''%'+ @SeachKeyword+'%'' or vcsubject like ''%'+@SeachKeyword+'%'' or  vcBody like ''%'+@SeachKeyword+'%'')'                 
  end                
  if @tintSortOrder =2 and @SeachKeyword <> ''                 
  begin                
     set  @strSql=@strSql+ ' and (vcTo like ''%'+ @SeachKeyword+'%'' or vcsubject like ''%'+@SeachKeyword+'%'' or  vcBody like ''%'+@SeachKeyword+'%'')'                 
  end                            
 end                
 if (@filter =0 and @tintSortOrder=0 and @bitOpenCommu=0) set  @strSql=@strSql+ ' union '                
                
 if ((@filter=0 or @filter>2) and (@tintSortOrder=0 or @tintSortOrder>2))                
 begin                
  set  @strSql=@strSql+ '                     
  SELECT c.numCommId as numEmailHstrID,convert(varchar(15),C.numCommId)+''~2~''+convert(varchar(15),C.numCreatedBy) as DelData,CASE WHEN c.bitTask = 973 THEN dtCreatedDate WHEN c.bitTask = 974 THEN dtCreatedDate WHEN c.bitTask = 971 THEN dtStartTime ELSE dtEndTime END AS bintCreatedOn,
  case when c.bitTask=973 then dbo.FormatedDateTimeFromDate(DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+', dtCreatedDate),'+convert(varchar(15),@numDomainID)+') 
  when c.bitTask=974 then dbo.FormatedDateTimeFromDate(DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+', dtEndTime),'+convert(varchar(15),@numDomainID)+') 
  when c.bitTask=971 THEN dbo.FormatedDateFromDate(DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+', dtEndTime),'+convert(varchar(15),@numDomainID)+') + '' '' + RIGHT(CONVERT(VARCHAR,DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+', [dtStartTime]),100),7)  +'' - '' + RIGHT(CONVERT(VARCHAR,DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+', [dtEndTime]),100),7)
  else  dbo.FormatedDateFromDate(DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+', dtEndTime),'+convert(varchar(15),@numDomainID)+') + '' '' + RIGHT(CONVERT(VARCHAR,DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+', [dtStartTime]),100),7)  +'' - '' + RIGHT(CONVERT(VARCHAR,DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+', [dtEndTime]),100),7)  END AS [date],
  convert(varchar(8000),textdetails) as [Subject],                                                                         
 dbo.fn_GetListItemName(c.bitTask)  AS [Type],                                                                 
  A1.vcFirstName +'' ''+ A1.vcLastName +'' ''+                                      
  case when A1.numPhone<>'''' then + A1.numPhone +case when A1.numPhoneExtension<>'''' then '' - '' + A1.numPhoneExtension else '''' end  else '''' end as [Phone],                        
  A2.vcFirstName+ '' ''+A2.vcLastName  as assignedto,                         
  c.caseid,                           
  (select top 1 vcCaseNumber from cases where cases.numcaseid= c.caseid )as vcCasenumber,                               
  isnull(caseTimeid,0)as caseTimeid,                              
  isnull(caseExpid,0)as caseExpid  ,                        
  1 as tinttype ,dtCreatedDate,'''' as [FROM]     ,isnull(C.bitClosedflag,0) as bitClosedflag,0 as bitHasAttachments,
'''' as vcBody
  from  Communication C                                              
  JOIN AdditionalContactsInformation  A1                                           
  ON c.numContactId = A1.numContactId                                                                             
  left join AdditionalContactsInformation A2 on A2.numContactID=c.numAssign
  left join UserMaster UM on A2.numContactid=UM.numUserDetailId and isnull(UM.bitActivateFlag,0)=1                                                                           
  left join listdetails on numActivity=numListID                           
  LEFT JOIN [Correspondence] CR ON CR.numCommID = C.numCommID
  where  C.numDomainID=' +convert(varchar(15),@numDomainID)                       
  + 'AND (numOpenRecordID = '+ convert(varchar(15),@numOpenRecordID) +' OR '+ convert(varchar(15),@numOpenRecordID) +'=0)'
  if (@numdivisionId =0 and @numContactId<>0) set  @strSql=@strSql+' and C.numContactId='+ convert(varchar(15),@numContactId)                        
  if @numdivisionId <> 0 set  @strSql=@strSql+' and  C.numDivisionId='+ convert(varchar(15),@numdivisionId)              
  if @numCaseID>0  set  @strSql=@strSql+' and  C.CaseId='+ convert(varchar(15),@numCaseID)                       
  if @filter =0  
	SET @strSql=@strSql+ ' and dtStartTime  >= '''+ convert(varchar(30),@FromDate)+''' and dtStartTime  <= '''+ convert(varchar(30),@ToDate)  +''''
  else
    SET @strSql=@strSql+ ' and c.bitTask=' + CONVERT(varchar(12),@filter) + ' and ((dtStartTime  >= '''+ convert(varchar(30),@FromDate)+''' and dtStartTime  <= '''+ convert(varchar(30),@ToDate)  +''') OR (dtEndTime  >= '''+ convert(varchar(30),@FromDate)+''' and dtEndTime  <= '''+ convert(varchar(30),@ToDate)  +'''))'
  
  IF @tintMode=1 SET @strSql=@strSql+ ' and  (CR.tintCorrType IN(1,3) OR '+ convert(varchar(15),@numOpenRecordID) +'=0)'
  IF @tintMode=2 SET @strSql=@strSql+ ' and  (CR.tintCorrType IN(2,4) OR '+ convert(varchar(15),@numOpenRecordID) +'=0)'
  IF @tintMode=5 SET @strSql=@strSql+ ' and  (CR.tintCorrType IN(5) OR '+ convert(varchar(15),@numOpenRecordID) +'=0)'
  IF @tintMode=6 SET @strSql=@strSql+ ' and  (CR.tintCorrType IN(6) OR '+ convert(varchar(15),@numOpenRecordID) +'=0)'
                          
  IF @tintSortOrder =0 and @SeachKeyword <>'' 
	SET @strSql=@strSql+ ' and textdetails like ''%'+@SeachKeyword+'%'''
  ELSE
	IF ISNULL(@filter,0) = 0 
	BEGIN
		SET @strSql=@strSql+ ' and textdetails like ''%'+@SeachKeyword+'%'''
	END
	ELSE
	BEGIN
		SET @strSql=@strSql+ ' and c.bitTask=' + CONVERT(varchar(12),@filter) + ' and textdetails like ''%'+@SeachKeyword+'%'''                     
	END
  IF @bitOpenCommu=1  SET @strSql=@strSql+ ' and isnull(C.bitClosedflag,0)=0'
 end                
 set @strSql='With tblCorr as (SELECT ROW_NUMBER() OVER (ORDER BY '+@columnName+' '+ @columnSortOrder+') AS RowNumber,X.* from('+@strSql+')X)'                

 set @strSql=@strSql +'                       
  select  *  from tblCorr
  Where RowNumber > '+convert(varchar(10),@firstRec)+' and RowNumber < '+convert(varchar(10),@lastRec)+'                     
  union select 0,count(*),null,null,null,'','',null,null,null,null,null,null,null,null,null,null,0,0,null from tblCorr order by RowNumber'                 
                
-- set @strSql=@strSql +'                       
--  select  *,CASE WHEN [Type]=''Email'' THEN dbo.GetEmaillAdd(numEmailHstrID,4)+'', '' +dbo.GetEmaillAdd(numEmailHstrID,1) ELSE '''' END as [From] from tblCorr
--  Where RowNumber > '+convert(varchar(10),@firstRec)+' and RowNumber < '+convert(varchar(10),@lastRec)+'                     
--  union select 0,count(*),null,null,null,'','',null,null,null,null,null,null,null,null,null,null from tblCorr order by RowNumber'                 
                
 print @strSql                
 exec (@strSql)                
end                
else                
select 0,0

/****** Object:  StoredProcedure [dbo].[USP_GetECampaignActionItems]    Script Date: 06/04/2009 15:15:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetECampaignActionItems')
DROP PROCEDURE USP_GetECampaignActionItems
GO
CREATE PROCEDURE [dbo].[USP_GetECampaignActionItems]
AS
  BEGIN
  
  	DECLARE @LastDateTime DATETIME
	DECLARE @CurrentDateTime DATETIME
	
	SELECT @LastDateTime = [dtLastDateTimeECampaign] FROM [WindowsServiceHistory]
	SET @CurrentDateTime = DATEADD(minute, DATEpart(minute,GETUTCDATE()) ,DATEADD(hour,DATEpart(hour,GETUTCDATE()),dbo.[GetUTCDateWithoutTime]()))
	PRINT @LastDateTime
	PRINT @CurrentDateTime


    SELECT   C.numContactID,
             C.numRecOwner,
             intStartDate,
             E.numActionItemTemplate,
             A.numDomainID,
             A.numDivisionId,
             DTL.[numConECampDTLID]
    FROM     ConECampaign C
             JOIN ConECampaignDTL DTL
               ON numConEmailCampID = numConECampID
             JOIN ECampaignDTLs E
               ON DTL.numECampDTLId = E.numECampDTLID
             JOIN [ECampaign] EC ON E.[numECampID] = EC.[numECampaignID]
             JOIN AdditionalContactsInformation A
               ON A.numContactID = C.[numContactID]
    WHERE    bitSend IS NULL 
            AND [bitEngaged]= 1
            AND [numActionItemTemplate] > 0
		    AND ((DTL.dtExecutionDate BETWEEN DATEADD(HOUR,-2,@LastDateTime) AND @CurrentDateTime) OR
			(SELECT COUNT(*) FROM ConECampaignDTL WHERE numConECampID = C.numConEmailCampID AND ISNULL(bitSend,0) = 1) = 0)
              

  END
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetECampaignContactFollowUpList')
DROP PROCEDURE USP_GetECampaignContactFollowUpList
GO
CREATE PROCEDURE USP_GetECampaignContactFollowUpList
AS 
    BEGIN
	DECLARE @LastDateTime DATETIME
	DECLARE @CurrentDateTime DATETIME
	
	SELECT @LastDateTime = [dtLastDateTimeECampaign] FROM [WindowsServiceHistory]
	SET @CurrentDateTime = DATEADD(minute, DATEpart(minute,GETUTCDATE()) ,DATEADD(hour,DATEpart(hour,GETUTCDATE()),dbo.[GetUTCDateWithoutTime]()))

    SELECT  
			E.[numECampaignID],
			ED.[numECampDTLId],
            C.numContactID,
            ED.[numFollowUpID],
            DTL.[numConECampDTLID],
            C.[numConEmailCampID],
                DATEADD(MINUTE, ISNULL(E.fltTimeZone,0) * 60, DTL.dtExecutionDate) AS StartDate,
                DATEADD(MINUTE,(ISNULL(E.fltTimeZone,0)-2)*60,@LastDateTime) Between1,
                DATEADD(MINUTE,ISNULL(E.fltTimeZone,0) * 60,@CurrentDateTime) Between2,
                E.fltTimeZone
    FROM    ConECampaign C
            JOIN ConECampaignDTL DTL ON numConEmailCampID = numConECampID
            JOIN ECampaignDTLs ED ON ED.numECampDTLId = DTL.numECampDTLID
            JOIN [ECampaign] E ON ED.[numECampID] = E.[numECampaignID]
            JOIN AdditionalContactsInformation A ON A.numContactID = C.[numContactID]
    WHERE   ISNULL(bitFollowUpStatus,0) = 0
			AND 
            [bitEngaged] = 1
            AND [numFollowUpID] > 0
           AND DATEADD(MINUTE, ISNULL(E.fltTimeZone,0) * 60, DTL.dtExecutionDate)
            BETWEEN DATEADD(MINUTE,(ISNULL(E.fltTimeZone,0)-2)*60,@LastDateTime) AND DATEADD(MINUTE,ISNULL(E.fltTimeZone,0) * 60,@CurrentDateTime)
END
GO
/****** Object:  StoredProcedure [dbo].[USP_GetECampaignDetailsReport]    Script Date: 06/04/2009 16:23:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Pratik  Vasani
-- Create date: 13/DEC/2008
-- Description:	This Stored proc gets the table for display in Ecampaign report
-- =============================================

--declare @p4 int
--set @p4=0
--exec USP_GetECampaignDetailsReport @numDomainId=1,@CurrentPage=1,@PageSize=20,@TotRecs=@p4 output
--select @p4
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getecampaigndetailsreport')
DROP PROCEDURE usp_getecampaigndetailsreport
GO
CREATE PROCEDURE [dbo].[USP_GetECampaignDetailsReport]
               @numDomainId NUMERIC(9,0)  = 0,
               @CurrentPage INT,
               @PageSize    INT,
               @TotRecs     INT  OUTPUT,
			   @numECampaingID NUMERIC(18,0) = 0
AS
  BEGIN
    CREATE TABLE #tempTable (
      ID INT IDENTITY PRIMARY KEY,
	  numECampaignID NUMERIC(9,0),
	  vcECampName VARCHAR(200),
	  numConEmailCampID NUMERIC(9,0),
      numContactID NUMERIC(9,0),
      CompanyName VARCHAR(100),
      ContactName VARCHAR(100),
      [Status] VARCHAR(20),
	  Template VARCHAR(500),
	  dtSentDate VARCHAR(80),
	  SendStatus VARCHAR(20),
	  dtNextStage VARCHAR(80),
	  Stage INT,
	  NoOfStagesCompleted INT,
	  NoOfEmailSent INT,
	  NoOfEmailOpened INT
      )
    INSERT INTO #tempTable
    
   SELECT
		ECampaign.numECampaignID,
		ECampaign.vcECampName,
		ConECampaign.numConEmailCampID,
		ConECampaign.numContactID,
		dbo.GetCompanyNameFromContactID(numContactID,ECampaign.numDomainId) AS CompanyName,
		dbo.fn_GetContactName(numContactID) AS ContactName,
		(CASE 
			WHEN ISNULL(ECampaign.bitDeleted,0) = 0 
			THEN CASE WHEN ISNULL(ConECampaign.bitEngaged,0) = 0 THEN 'Disengaged' ELSE 'Engaged' END
			ELSE 'Campaign Deleted'
		END) AS Status,
		ISNULL(CampaignExecutedDetail.Template,''),
		ISNULL(dbo.FormatedDateFromDate(CampaignExecutedDetail.bintSentON,@numDomainID),''),
		(CASE 
			WHEN ISNULL(CampaignExecutedDetail.bitSend,0)=1 AND ISNULL(CampaignExecutedDetail.tintDeliveryStatus,0)=0 THEN 'Failed'
			WHEN ISNULL(CampaignExecutedDetail.bitSend,0)=1 AND ISNULL(CampaignExecutedDetail.tintDeliveryStatus,0)=1 THEN 'Success'
			ELSE 'Pending'
		END) AS SendStatus
		,
		 dbo.FormatedDateFromDate(DATEADD(MINUTE,ISNULL(ECampaign.fltTimeZone,0) * 60,CampaignNextExecutationDetail.dtExecutionDate),@numDomainID),
		 ISNULL((SELECT COUNT(*) FROM ConECampaignDTL WHERE numConECampID = ConECampaign.numConEmailCampID),0) AS NoOfStages,
		 ISNULL((SELECT COUNT(*) FROM ConECampaignDTL WHERE numConECampID = ConECampaign.numConEmailCampID AND ISNULL(bitSend,0)=1),0) AS NoOfStagesCompleted,
		 (SELECT COUNT(*) FROM ConECampaignDTL INNER JOIN ECampaignDTLs ON ConECampaignDTL.numECampDTLID = ECampaignDTLs.numECampDTLId WHERE ECampaignDTLs.numEmailTemplate IS NOT NULL AND numConECampID = ConECampaign.numECampaignID AND bitSend = 1 AND ISNULL(tintDeliveryStatus,0) = 1) AS NoOfEmailSent,
		 (SELECT COUNT(*) FROM ConECampaignDTL INNER JOIN ECampaignDTLs ON ConECampaignDTL.numECampDTLID = ECampaignDTLs.numECampDTLId WHERE ECampaignDTLs.numEmailTemplate IS NOT NULL AND numConECampID = ConECampaign.numECampaignID AND bitSend = 1 AND ISNULL(tintDeliveryStatus,0) = 1 AND ISNULL(bitEmailRead,0) = 1) AS NoOfEmailOpened
	FROM
		ConECampaign
	INNER JOIN
		ECampaign
	ON
		ConECampaign.numECampaignID = ECampaign.numECampaignID
	OUTER APPLY
		(
			SELECT 
				TOP 1
				(CASE 
					WHEN ECampaignDTLs.numEmailTemplate IS NOT NULL THEN GenericDocuments.vcDocName
					WHEN ECampaignDTLs.numActionItemTemplate IS NOT NULL THEN tblActionItemData.TemplateName
					ELSE ''
				END) AS Template,
				ConECampaignDTL.bitSend,
				ConECampaignDTL.bintSentON,
				ConECampaignDTL.tintDeliveryStatus
			FROM 
				ConECampaignDTL 
			INNER JOIN 
				ECampaignDTLs 
			ON 
				ConECampaignDTL.numECampDTLID = ECampaignDTLs.numECampDTLId
			LEFT JOIN 
				GenericDocuments 
			ON 
				GenericDocuments.numGenericDocID = ECampaignDTLs.numEmailTemplate
			LEFT JOIN 
				tblActionItemData
			ON
				tblActionItemData.RowID = ECampaignDTLs.numActionItemTemplate
			WHERE
				ECampaignDTLs.numECampID = ECampaign.numECampaignID AND
				ConECampaignDTL.numConECampID = ConECampaign.numConEmailCampID AND
				ConECampaignDTL.bitSend IS NOT NULL
			ORDER BY
				numConECampDTLID DESC
		) CampaignExecutedDetail
	OUTER APPLY
		(
			SELECT 
				TOP 1
				ConECampaignDTL.dtExecutionDate
			FROM 
				ConECampaignDTL 
			INNER JOIN 
				ECampaignDTLs 
			ON 
				ConECampaignDTL.numECampDTLID = ECampaignDTLs.numECampDTLId
			LEFT JOIN 
				GenericDocuments 
			ON 
				ECampaignDTLs.numEmailTemplate = GenericDocuments.numGenericDocID
			LEFT JOIN 
				tblActionItemData
			ON
				ECampaignDTLs.numActionItemTemplate = tblActionItemData.RowID
			WHERE
				ECampaignDTLs.numECampID = ECampaign.numECampaignID AND
				ConECampaignDTL.numConECampID = ConECampaign.numConEmailCampID AND
				ConECampaignDTL.bitSend IS NULL
			ORDER BY
				numConECampDTLID
		) CampaignNextExecutationDetail
	WHERE
		ECampaign.numDomainID = @numDomainId AND
		(ECampaign.numECampaignID = @numECampaingID OR ISNULL(@numECampaingID,0) = 0)
    
   
            
    DECLARE  @firstRec  AS INTEGER
    DECLARE  @lastRec  AS INTEGER
    SET @firstRec = (@CurrentPage
                       - 1)
                      * @PageSize
    SET @lastRec = (@CurrentPage
                      * @PageSize
                      + 1)
    SET @TotRecs = (SELECT COUNT(* )
                    FROM   #tempTable)
    SELECT   *
    FROM     #tempTable
    WHERE    ID > @firstRec
             AND ID < @lastRec
    ORDER BY id

    DROP TABLE #tempTable
    SELECT @TotRecs AS TotRecs
  END
/****** Object:  StoredProcedure [dbo].[USP_GetEmailCampaignMails]    Script Date: 06/04/2009 15:10:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetEmailCampaignMails')
DROP PROCEDURE USP_GetEmailCampaignMails
GO
CREATE PROCEDURE [dbo].[USP_GetEmailCampaignMails]
AS 
BEGIN
-- Organization Fields Which Needs To Be Replaced
DECLARE  @vcCompanyName  AS VARCHAR(200)
DECLARE  @OrgNoOfEmployee  AS VARCHAR(12)
DECLARE  @OrgShippingAddress  AS VARCHAR(1000)
DECLARE  @Signature  AS VARCHAR(1000)


-- Contact Fields Which Needs To Be Replaced.
DECLARE @ContactAge AS VARCHAR(3)
DECLARE @ContactAssistantEmail AS VARCHAR(100)
DECLARE @AssistantFirstName AS VARCHAR(100)
DECLARE @AssistantLastName AS VARCHAR(100)
DECLARE @ContactAssistantPhone AS VARCHAR(20)
DECLARE @ContactAssistantPhoneExt AS VARCHAR(10)
DECLARE @ContactCategory AS VARCHAR(100)
DECLARE @ContactCell AS VARCHAR(20)
DECLARE @ContactDepartment AS VARCHAR(100)
DECLARE @ContactDripCampaign AS VARCHAR(500)
DECLARE @ContactEmail AS VARCHAR(100)
DECLARE @ContactFax AS VARCHAR(100)
DECLARE @ContactFirstName AS VARCHAR(100)
DECLARE @ContactGender AS VARCHAR(10)
DECLARE @ContactHomePhone AS VARCHAR(20)
DECLARE @ContactLastName AS VARCHAR(100)
DECLARE @ContactManager AS VARCHAR(100)
DECLARE @ContactPhone AS VARCHAR(20)	
DECLARE @ContactPhoneExt AS VARCHAR(10)
DECLARE @ContactPosition AS VARCHAR(100)
DECLARE @ContactPrimaryAddress AS VARCHAR(1000)
DECLARE @ContactStatus AS VARCHAR(100)
DECLARE @ContactTeam AS VARCHAR(100)
DECLARE @ContactTitle AS VARCHAR(100)
DECLARE @ContactType AS VARCHAR(100)
      
DECLARE  @numConEmailCampID  AS NUMERIC(9)
DECLARE  @intStartDate  AS DATETIME


DECLARE  @numConECampDTLID  AS NUMERIC(9)
declare @numContactID as numeric(9)   
declare @numFromContactID as numeric(9)   
declare @numEmailTemplate as varchar(15)
declare @numDomainId as numeric(9)
DECLARE  @tintFromField  AS TINYINT

declare @vcSubject as varchar(1000)
declare @vcBody as varchar(8000)              
declare @vcFormattedBody as varchar(8000)

DECLARE @LastDateTime DATETIME
DECLARE @CurrentDateTime DATETIME

SELECT @LastDateTime = [dtLastDateTimeECampaign] FROM [WindowsServiceHistory]
SET @CurrentDateTime = DATEADD(minute, DATEpart(minute,GETUTCDATE()) ,DATEADD(hour,DATEpart(hour,GETUTCDATE()),dbo.[GetUTCDateWithoutTime]()))
PRINT @LastDateTime
PRINT @CurrentDateTime


Create table #tempTableSendApplicationMail
(  vcTo varchar(2000),              
   vcSubject varchar(2000),              
   vcBody varchar(Max),              
   vcCC varchar(2000),      
   numDomainID numeric(9),
   numConECampDTLID NUMERIC(9) NULL,
   tintFromField TINYINT,
   numContactID NUMERIC,
   numFromContactID NUMERIC
 )                

-- EC.fltTimeZone-2 is used to fetch all email which are not sent from past 2 housrs of last execution date 
-- in case of amazon ses service is down and not able to send email in first try
SELECT   TOP 1 @numContactID = C.numContactID,
               @numConECampDTLID = numConECampDTLID,
               @numConEmailCampID = numConEmailCampID,
               @intStartDate = intStartDate,
               @numEmailTemplate = numEmailTemplate,
               @numDomainId = EC.numDomainID,
               @tintFromField = EC.[tintFromField],
			   @numFromContactID = numFromContactID
FROM     ConECampaign C
         JOIN ConECampaignDTL DTL
           ON numConEmailCampID = numConECampID
         JOIN ECampaignDTLs E
           ON DTL.numECampDTLId = E.numECampDTLID
         JOIN [ECampaign] EC ON E.[numECampID] = EC.[numECampaignID] 
WHERE    bitSend IS NULL 
         AND bitengaged = 1
         AND numEmailTemplate > 0 
		 AND ISNULL(EC.bitDeleted,0) = 0
		 -- When campaign is first time assigned to contact it may happen that it is assigned after service already executed 
		 -- so first email will be send immediately and after that service will compare execution datetime and current datetime
         AND ((DTL.dtExecutionDate BETWEEN DATEADD(HOUR,-2,@LastDateTime) AND @CurrentDateTime) OR
			(SELECT COUNT(*) FROM ConECampaignDTL WHERE numConECampID = C.numConEmailCampID AND ISNULL(bitSend,0) = 1) = 0)
          
ORDER BY numConEmailCampID
PRINT '@numConEmailCampID='
PRINT @numConEmailCampID



WHILE @numConEmailCampID > 0
  BEGIN
	PRINT 'inside @numConEmailCampID='
	PRINT @numConEmailCampID

    SELECT 
		   @ContactFirstName = ISNULL(A.vcFirstName,''),
           @ContactLastName = ISNULL(A.vcLastName,''),
           @vcCompanyName = ISNULL(C.vcCompanyName,''),
		   @OrgNoOfEmployee = ISNULL(dbo.GetListIemName(C.numNoOfEmployeesId),''),
		   @OrgShippingAddress = vcCompanyName + ' ,<br>' + ISNULL(AD2.vcStreet,'') + ' <br>' + ISNULL(AD2.VcCity,'') + ' ,' 
								+ ISNULL(dbo.fn_GetState(AD2.numState),'') + ' ' + ISNULL(AD2.vcPostalCode,'') + ' <br>' + 
								ISNULL(dbo.fn_GetListItemName(AD2.numCountry),''),								   
		   @Signature = ISNULL((select U.txtSignature from UserMaster U where U.numUserDetailId=A.numRecOwner),''),
           @ContactPhone = ISNULL(A.numPhone,''),
           @ContactEmail = ISNULL(A.vcEmail,''),
		   @ContactAge = ISNULL(dbo.GetAge(A.bintDOB, GETUTCDATE()),''),
		   @ContactAssistantEmail = ISNULL(A.vcAsstEmail,''),
		   @AssistantFirstName = ISNULL(A.VcAsstFirstName,''),
		   @AssistantLastName = ISNULL(A.vcAsstLastName,''),
		   @ContactAssistantPhone = ISNULL(A.numAsstPhone,''),
		   @ContactAssistantPhoneExt= ISNULL(A.numAsstExtn,''),
		   @ContactCategory=ISNULL([dbo].[GetListIemName](A.vcCategory),''),
		   @ContactCell=ISNULL(A.numCell,''),
		   @ContactDepartment=ISNULL([dbo].[GetListIemName](A.vcDepartment),''),
		   @ContactDripCampaign=ISNULL((SELECT vcECampName FROM [ECampaign] WHERE numECampaignID = A.numECampaignID), ''),
		   @ContactFax=ISNULL(A.vcFax,''),
		   @ContactGender=(CASE WHEN A.charSex = 'M' THEN 'Male' WHEN A.charSex = 'F' THEN 'Female' ELSE '-' END),
		   @ContactHomePhone=ISNULL(A.numHomePhone,''),
		   @ContactManager=ISNULL(dbo.fn_GetContactName(A.numManagerId),''),
		   @ContactPhoneExt=ISNULL(A.numPhoneExtension,''),
		   @ContactPosition= ISNULL([dbo].[GetListIemName](A.vcPosition),''),
		   @ContactPrimaryAddress=ISNULL(dbo.getContactAddress(A.numContactID),''),
		   @ContactStatus=ISNULL([dbo].[GetListIemName](A.numEmpStatus),''),
		   @ContactTeam=ISNULL([dbo].[GetListIemName](A.numTeam),''),
		   @ContactTitle=ISNULL(A.vcTitle,''),
		   @ContactType=ISNULL(dbo.GetListIemName(A.numContactType),'')
    FROM   AdditionalContactsInformation A
           JOIN DivisionMaster D
             ON A.numDivisionId = D.numDivisionID
           JOIN CompanyInfo C
             ON D.numCompanyID = C.numCompanyId
		   LEFT JOIN 
				dbo.AddressDetails AD2 
			ON 
				AD2.numDomainID=D.numDomainID 
				AND AD2.numRecordID= D.numDivisionID 
				AND AD2.tintAddressOf=2 
				AND AD2.tintAddressType=2 
				AND AD2.bitIsPrimary=1
    WHERE  A.numContactId = @numContactID
    
    SELECT @vcSubject = vcSubject,
           @vcBody = vcDocDesc
    FROM   genericdocuments
    WHERE  numGenericDocID = @numEmailTemplate
    
	--Replace organization fields from body
    SET @vcFormattedBody = REPLACE(@vcBody,'##OrganizationName##',@vcCompanyName)
	SET @vcFormattedBody = REPLACE(@vcFormattedBody,'##OrgNoOfEmployee##',@OrgNoOfEmployee)
	SET @vcFormattedBody = REPLACE(@vcFormattedBody,'##OrgShippingAddress##',@OrgShippingAddress)
	SET @vcFormattedBody = REPLACE(@vcFormattedBody,'##Signature##',@Signature)

	--Replace contact fields from body
    SET @vcFormattedBody = REPLACE(@vcFormattedBody,'##ContactFirstName##',@ContactFirstName)
    SET @vcFormattedBody = REPLACE(@vcFormattedBody,'##ContactLastName##',@ContactLastName)
    SET @vcFormattedBody = REPLACE(@vcFormattedBody,'##ContactPhone##',@ContactPhone)
    SET @vcFormattedBody = REPLACE(@vcFormattedBody,'##ContactEmail##',@ContactEmail)
	SET @vcFormattedBody = REPLACE(@vcFormattedBody,'##ContactAge##',@ContactAge)
	SET @vcFormattedBody = REPLACE(@vcFormattedBody,'##ContactAssistantEmail##',@ContactAssistantEmail)
	SET @vcFormattedBody = REPLACE(@vcFormattedBody,'##AssistantFirstName##',@AssistantFirstName)
	SET @vcFormattedBody = REPLACE(@vcFormattedBody,'##AssistantLastName##',@AssistantLastName)
	SET @vcFormattedBody = REPLACE(@vcFormattedBody,'##ContactAssistantPhone##',@ContactAssistantPhone)
	SET @vcFormattedBody = REPLACE(@vcFormattedBody,'##ContactAssistantPhoneExt##',@ContactAssistantPhoneExt)
	SET @vcFormattedBody = REPLACE(@vcFormattedBody,'##ContactCategory##',@ContactCategory)
	SET @vcFormattedBody = REPLACE(@vcFormattedBody,'##ContactCell##',@ContactCell)
	SET @vcFormattedBody = REPLACE(@vcFormattedBody,'##ContactDepartment##',@ContactDepartment)
	SET @vcFormattedBody = REPLACE(@vcFormattedBody,'##ContactDripCampaign##',@ContactDripCampaign)
	SET @vcFormattedBody = REPLACE(@vcFormattedBody,'##ContactFax##',@ContactFax)
	SET @vcFormattedBody = REPLACE(@vcFormattedBody,'##ContactGender##',@ContactGender)
	SET @vcFormattedBody = REPLACE(@vcFormattedBody,'##ContactHomePhone##',@ContactHomePhone)
	SET @vcFormattedBody = REPLACE(@vcFormattedBody,'##ContactManager##',@ContactManager)
	SET @vcFormattedBody = REPLACE(@vcFormattedBody,'##ContactPhoneExt##',@ContactPhoneExt)
	SET @vcFormattedBody = REPLACE(@vcFormattedBody,'##ContactPosition##',@ContactPosition)
	SET @vcFormattedBody = REPLACE(@vcFormattedBody,'##ContactPrimaryAddress##',@ContactPrimaryAddress)
	SET @vcFormattedBody = REPLACE(@vcFormattedBody,'##ContactStatus##',@ContactStatus)
	SET @vcFormattedBody = REPLACE(@vcFormattedBody,'##ContactTeam##',@ContactTeam)
	SET @vcFormattedBody = REPLACE(@vcFormattedBody,'##ContactTitle##',@ContactTitle)
	SET @vcFormattedBody = REPLACE(@vcFormattedBody,'##ContactType##',@ContactType)
    SET @vcFormattedBody = REPLACE(@vcFormattedBody,'##ContactOpt-OutLink##','')

    
   INSERT INTO #tempTableSendApplicationMail
   VALUES (     @ContactEmail,
                @vcSubject,
                @vcFormattedBody,
                '',
                @numDomainId,
                @numConECampDTLID,
                @tintFromField,
                @numContactID,
                @numFromContactID)
                

    
    SELECT   TOP 1 @numContactID = C.numContactID,
                   @numConECampDTLID = numConECampDTLID,
                   @numConEmailCampID = numConEmailCampID,
                   @intStartDate = intStartDate,
                   @numEmailTemplate = numEmailTemplate,
                   @numDomainId = EC.numDomainID,
                   @tintFromField = EC.[tintFromField],
                   @numFromContactID = numFromContactID
    FROM     ConECampaign C
             JOIN ConECampaignDTL DTL
               ON numConEmailCampID = numConECampID
             JOIN ECampaignDTLs E
               ON DTL.numECampDTLId = E.numECampDTLID
             JOIN [ECampaign] EC ON E.[numECampID] = EC.[numECampaignID] 
--             JOIN AdditionalContactsInformation A
--               ON A.numContactID = C.numRecOwner
    WHERE    bitSend IS NULL 
             AND bitengaged = 1
             AND numConEmailCampID > @numConEmailCampID
			 AND numEmailTemplate > 0 
			 AND ISNULL(EC.bitDeleted,0) = 0
			-- When campaign is first time assigned to contact it may happen that it is assigned after service already executed 
		 -- so first email will be send immediately and after that service will compare execution datetime and current datetime
         AND ((DTL.dtExecutionDate BETWEEN DATEADD(HOUR,-2,@LastDateTime) AND @CurrentDateTime) OR
			(SELECT COUNT(*) FROM ConECampaignDTL WHERE numConECampID = C.numConEmailCampID AND ISNULL(bitSend,0) = 1) = 0)
    ORDER BY numConEmailCampID
    
    IF @@ROWCOUNT = 0
      SET @numConEmailCampID = 0
  END
  
  
  SELECT    X.*,
			ISNULL(EmailBroadcastConfiguration.vcAWSDomain,'') vcAWSDomain,
            ISNULL(EmailBroadcastConfiguration.vcAWSAccessKey,'') vcAWSAccessKey,
            ISNULL(EmailBroadcastConfiguration.vcAWSSecretKey,'') vcAWSSecretKey,
            ISNULL(EmailBroadcastConfiguration.vcFrom,'') vcFrom
  FROM      ( SELECT    A.[vcTo],
                        A.[vcSubject],
                        A.[vcBody],
                        A.[vcCC],
                        A.[numDomainID],
                        A.[numConECampDTLID],
                        A.[tintFromField],
                        A.[numContactID],
                        CASE [tintFromField]
                          WHEN 1 THEN --Record Owner of Company
                               DM.[numRecOwner]
                          WHEN 2 THEN --Assignee of Company
                               DM.[numAssignedTo]
                          WHEN 3 THEN numFromContactID
                          ELSE DM.[numRecOwner]
                        END AS numFromContactID
              FROM      #tempTableSendApplicationMail A
                        INNER JOIN [AdditionalContactsInformation] ACI ON ACI.[numContactId] = A.numContactID
                        INNER JOIN [DivisionMaster] DM ON DM.[numDivisionID] = ACI.[numDivisionId]
            ) X
            LEFT JOIN 
				EmailBroadcastConfiguration
			ON
				X.numDomainID = EmailBroadcastConfiguration.numDomainID
  
  DROP TABLE #tempTableSendApplicationMail

  UPDATE [WindowsServiceHistory] SET [dtLastDateTimeECampaign] = GETUTCDATE()
END
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetIncomeExpenseStatement')
DROP PROCEDURE USP_GetIncomeExpenseStatement
GO
CREATE PROCEDURE [dbo].[USP_GetIncomeExpenseStatement]
@numDomainId AS NUMERIC(9),                                          
@dtFromDate AS DATETIME,                                        
@dtToDate AS DATETIME,
@ClientTimeZoneOffset INT, --Added by Chintan to enable calculation of date according to client machine                                                   
@numAccountClass AS NUMERIC(9)=0

AS                                                          
BEGIN   
DECLARE @CURRENTPL MONEY ;  
DECLARE @PLOPENING MONEY;  
DECLARE @PLCHARTID NUMERIC(8)  
DECLARE @numFinYear INT;  
  
DECLARE @dtFinYearFrom DATETIME;  
  
  
SET @numFinYear= (SELECT numFinYearId FROM FINANCIALYEAR WHERE dtPeriodFrom <= @dtFromDate AND    
dtPeriodTo >=@dtFromDate AND numDomainId= @numDomainId);  
  
SET @dtFinYearFrom = (SELECT dtPeriodFrom FROM FINANCIALYEAR WHERE dtPeriodFrom <= @dtFromDate AND    
dtPeriodTo >=@dtFromDate AND numDomainId= @numDomainId);  
  
SET @dtFromDate = DATEADD(minute, @ClientTimeZoneOffset, @dtFromDate);  
SET @dtToDate = DATEADD(minute, @ClientTimeZoneOffset, @dtToDate);  
  
  
--select * from view_journal where numDomainid=72  
  
CREATE TABLE #PLSummary (numAccountId NUMERIC(9),vcAccountName VARCHAR(250),  
numParntAcntTypeID NUMERIC(9),vcAccountDescription VARCHAR(250),  
vcAccountCode VARCHAR(50) COLLATE Database_Default,Opening MONEY,Debit MONEY,Credit MONEY,bitIsSubAccount Bit);  
 

select  numDomainID,Debit,Credit,COAvcAccountCode into #view_journal
from View_Journal where  datEntry_Date BETWEEN  @dtFromDate AND @dtToDate
AND numDomainID=@numDomainId AND (numAccountClass=@numAccountClass OR @numAccountClass=0) 

INSERT INTO  #PLSummary  
SELECT COA.numAccountId,vcAccountName,numParntAcntTypeID,vcAccountDescription,vcAccountCode,  
  
0 AS OPENING,  
  
ISNULL((SELECT SUM(Debit) FROM #view_journal VJ  
WHERE VJ.numDomainId=@numDomainId AND  
 VJ.COAvcAccountCode like COA.vcAccountCode + '%'),0) /*VJ.numAccountId=COA.numAccountId*/   
  AS DEBIT,  
  
ISNULL((SELECT SUM(Credit) FROM #view_journal VJ  
WHERE VJ.numDomainId=@numDomainId AND  
 VJ.COAvcAccountCode like COA.vcAccountCode + '%'),0) /*VJ.numAccountId=COA.numAccountId*/   
  AS CREDIT,ISNULL(COA.bitIsSubAccount,0)  
FROM Chart_of_Accounts COA   
WHERE   
--COA.numAccountId not in (select AC.numAccountID from AccountingCharges AC,AccountingChargeTypes AT where  
-- AC.numChargeTypeId=AT.numChargeTypeId and AT.chChargeCode='CG' and AC.numDomainID=@numDomainId) and  
      COA.numDomainId=@numDomainId AND COA.bitActive = 1 AND  
      (COA.vcAccountCode LIKE '0103%' OR  
       COA.vcAccountCode LIKE '0104%' OR   
       COA.vcAccountCode LIKE '0106%')  ;  
  
  
CREATE TABLE #PLOutPut (numAccountId NUMERIC(9),vcAccountName VARCHAR(250),  
numParntAcntTypeID NUMERIC(9),vcAccountDescription VARCHAR(250),  
vcAccountCode VARCHAR(50) COLLATE Database_Default,Opening MONEY,Debit MONEY,Credit MONEY);  
  
INSERT INTO #PLOutPut  
SELECT ATD.numAccountTypeID,ATD.vcAccountType,ATD.numParentID, '',ATD.vcAccountCode,  
 ISNULL(SUM(Opening),0) AS Opening,  
ISNULL(SUM(Debit),0) AS Debit,ISNULL(SUM(Credit),0) AS Credit  
FROM   
 AccountTypeDetail ATD RIGHT OUTER JOIN   
#PLSummary PL ON  
PL.vcAccountCode LIKE ATD.vcAccountCode + '%'  
AND ATD.numDomainId=@numDomainId AND  
(ATD.vcAccountCode LIKE '0103%' OR  
 ATD.vcAccountCode LIKE '0104%' OR  
 ATD.vcAccountCode LIKE '0106%')  
 WHERE PL.bitIsSubAccount=0  
GROUP BY   
ATD.numAccountTypeID,ATD.vcAccountCode,ATD.vcAccountType,ATD.numParentID;  
  
  
  
--------------------------------------------------------  
-- GETTING P&L VALUE  
  
SET @CURRENTPL =0;   
SET @PLOPENING=0;  
  
SELECT @CURRENTPL = ISNULL(SUM(Opening),0)+ISNULL(SUM(Debit),0)-ISNULL(SUM(Credit),0) FROM  
#PLOutPut P WHERE   
vcAccountCode IN ('0103','0104','0106')  
  
--SELECT @CURRENTPL = @CURRENTPL - ISNULL(SUM(Opening),0)+ISNULL(sum(Debit),0)-ISNULL(sum(Credit),0) FROM  
--#PLOutPut P WHERE   
--vcAccountCode IN ('0104')  
  
SET @CURRENTPL=@CURRENTPL * (-1)  
  
SELECT @PLCHARTID=COA.numAccountId FROM Chart_of_Accounts COA WHERE numDomainID=@numDomainId AND  
bitProfitLoss=1;  
  
--SELECT   @PLOPENING = isnull((SELECT isnull(monOpening,0) from CHARTACCOUNTOPENING CAO WHERE  
--numFinYearId=@numFinYear and numDomainID=@numDomainId and  
--CAO.numAccountId=COA.numAccountId),0)  +   
--ISNULL((SELECT sum(Debit-Credit) FROM view_journal VJ  
--WHERE VJ.numDomainId=@numDomainId AND  
-- VJ.numAccountId=COA.numAccountId AND  
-- datEntry_Date BETWEEN @dtFinYearFrom AND  @dtFromDate-1),0)  
-- FROM Chart_of_Accounts COA WHERE numDomainID=@numDomainId and  
--bitProfitLoss=1;  
  
--SELECT  @CURRENTPL=@CURRENTPL +        
--ISNULL((SELECT sum(Debit-Credit) FROM view_journal VJ  
--WHERE VJ.numDomainId=@numDomainId AND  
-- VJ.numAccountId=@PLCHARTID AND  
-- datEntry_Date BETWEEN @dtFromDate AND @dtToDate),0) ;  
  
SET @CURRENTPL=@CURRENTPL * (-1)  
--SELECT @PLCHARTID,@CURRENTPL,@PLOPENING  
  
-----------------------------------------------------------------  
  
 CREATE TABLE #PLShow (numAccountId NUMERIC(9),vcAccountName VARCHAR(250),  
numParntAcntTypeID NUMERIC(9),vcAccountDescription VARCHAR(250),  
vcAccountCode VARCHAR(50) COLLATE Database_Default,Opening MONEY,Debit MONEY,Credit MONEY,  
Balance  VARCHAR(50),AccountCode1  VARCHAR(100),vcAccountName1 VARCHAR(250),[Type] INT);  
  
 CREATE TABLE #PLShow1 (numAccountId NUMERIC(9),vcAccountName VARCHAR(250),  
numParntAcntTypeID NUMERIC(9),vcAccountDescription VARCHAR(250),  
vcAccountCode VARCHAR(50) COLLATE Database_Default,Opening MONEY,Debit MONEY,Credit MONEY,  
Balance  VARCHAR(50),AccountCode1  VARCHAR(100),vcAccountName1 VARCHAR(250),[Type] INT);  
  
CREATE TABLE #PLShowGrid1 (numAccountId NUMERIC(9),vcAccountName VARCHAR(250),  
numParntAcntTypeID NUMERIC(9),vcAccountDescription VARCHAR(250),  
vcAccountCode VARCHAR(50) COLLATE Database_Default,Opening MONEY,Debit MONEY,Credit MONEY,  
Balance VARCHAR(50),AccountCode1  VARCHAR(100),vcAccountName1 VARCHAR(250),[Type] INT);  
  
  
CREATE TABLE #PLShowGrid2 (numAccountId NUMERIC(9),vcAccountName VARCHAR(250),  
numParntAcntTypeID NUMERIC(9),vcAccountDescription VARCHAR(250),  
vcAccountCode VARCHAR(50) COLLATE Database_Default,Opening MONEY,Debit MONEY,Credit MONEY,  
Balance  VARCHAR(50),AccountCode1  VARCHAR(100),vcAccountName1 VARCHAR(250),[Type] INT);  
  
DECLARE @monIncome MONEY;  
DECLARE @monCOGS MONEY;  
DECLARE @monOtherIncome MONEY;  
DECLARE @monOtherExpense MONEY;  
DECLARE @monExpenses MONEY;  
  
  
SELECT @monIncome=ISNULL(SUM(Credit) - SUM(Debit) ,0)  
 FROM #PLSummary P  
WHERE vcAccountCode LIKE '010301%' AND P.bitIsSubAccount=0  
   
 SELECT @monOtherIncome=ISNULL( ISNULL(SUM(Debit),0)-ISNULL(SUM(Credit) ,0),0)  
 FROM #PLSummary P  
 WHERE vcAccountCode LIKE '010302%' AND P.bitIsSubAccount=0  
   
 SELECT @monExpenses=ISNULL( ISNULL(SUM(Debit),0)-ISNULL(SUM(Credit) ,0),0)  
 FROM #PLSummary P  
 WHERE vcAccountCode LIKE '010401%' AND P.bitIsSubAccount=0  
   
SELECT @monOtherExpense=ISNULL( ISNULL(SUM(Debit),0)-ISNULL(SUM(Credit) ,0),0)  
 FROM #PLSummary P  
 WHERE vcAccountCode LIKE '010402%' AND P.bitIsSubAccount=0  
   
  
 SET @monOtherIncome=@monOtherIncome * (-1)  
   
   
--SELECT  @monCOGS= dbo.GetCOGSValue(@numDomainId,@dtFromDate,@dtToDate )  
 SELECT @monCOGS=ISNULL( ISNULL(SUM(Debit),0)-ISNULL(SUM(Credit) ,0),0)  
 FROM #PLSummary P  
 --WHERE vcAccountCode LIKE '010403%'   
 WHERE vcAccountCode LIKE '0106%' AND P.bitIsSubAccount=0  
   
ALTER TABLE #PLSummary  
DROP COLUMN bitIsSubAccount  
  
  
   
INSERT INTO #PLShow  
  
SELECT 0,'Direct Income',0,'Direct Income','0101A',0,0,0,'' ,'','Direct Income',2    
  
UNION  
  
SELECT numAccountId ,vcAccountName ,  
numParntAcntTypeID ,vcAccountDescription ,  
'0101A1',Opening ,Debit ,Credit, CAST((Credit - Debit) AS VARCHAR(50)) AS Balance,  
vcAccountCode AS  AccountCode1,  
 CASE   
 WHEN LEN(P.vcAccountCode)>4 THEN REPLICATE('&nbsp;', LEN('0101A1')-4) + P.[vcAccountName]  
 ELSE P.[vcAccountName]  
 END AS vcAccountName1, 1 AS Type  
 FROM #PLSummary P  
 WHERE vcAccountCode LIKE '010301%'   
-- WHERE numAccountId IN (  
-- SELECT IT.numIncomeChartAcntId FROM Item IT where IT.numDomainID=@numDomainId )  
UNION  
  
SELECT 0,'',0,'','0101B',0,0,0,'________________' ,'','',2    
UNION  
SELECT 0,'',0,'','0101B1',0,0,0,CAST(@monIncome AS VARCHAR(50)),'','A) Total Direct Income',2    
UNION  
SELECT 0,'',0,'','0101B2',0,0,0,'' ,'&nbsp;','',2    
UNION  
SELECT 0,'',0,'','0101C',0,0,0,'' ,'','Cost of Goods Sold',2    
UNION  
SELECT numAccountId ,vcAccountName ,  
numParntAcntTypeID ,vcAccountDescription ,  
'0101C1',Opening ,Debit ,Credit, CAST((Debit-Credit) AS VARCHAR(50)) AS Balance,  
vcAccountCode AS  AccountCode1,  
 CASE   
 WHEN LEN('0101C1')>4 THEN REPLICATE('&nbsp;', LEN('0101C1')-4) + P.[vcAccountName]  
 ELSE P.[vcAccountName]  
 END AS vcAccountName1, 1 AS Type  
 FROM #PLSummary P  
 --WHERE vcAccountCode LIKE '010403%'   
 WHERE vcAccountCode LIKE '0106%'   
--select -1,'',0,'','0101C1',0,0,0,CAST(@monCOGS AS VARCHAR(50)),'','COGS',1  
UNION  
SELECT 0,'',0,'','0101C2',0,0,0,'________________' ,'','',2   
UNION  
SELECT 0,'',0,'','0101C3',0,0,0,CAST(@monCOGS AS VARCHAR(50)),'','B) Total COGS',2  
UNION  
SELECT 0,'',0,'','0101C4',0,0,0,'________________' ,'','',2   
UNION  
SELECT 0,'',0,'','0101C5',0,0,0,CAST(@monIncome-@monCOGS AS VARCHAR(50)),'','Gross Profit= (A- B)',2  
UNION  
SELECT 0,'',0,'','0101C6',0,0,0,'&nbsp;' ,'','',2  
UNION  
SELECT 0,'',0,'','0101D',0,0,0,'' ,'','Direct Expenses',2  
UNION  
SELECT numAccountId ,vcAccountName ,  
numParntAcntTypeID ,vcAccountDescription ,  
'0101D1',Opening ,Debit ,Credit,CAST((Debit - Credit) AS VARCHAR(50)) AS Balance,  
vcAccountCode AS  AccountCode1,  
 CASE   
 WHEN LEN('0101D1')>4 THEN REPLICATE('&nbsp;', LEN('0101D1')-4) + P.[vcAccountName]  
 ELSE P.[vcAccountName]  
 END AS vcAccountName1, 1 AS Type  
 FROM #PLSummary P  
 WHERE vcAccountCode LIKE '010401%'   
UNION  
SELECT 0,'',0,'','0101D2',0,0,0,'________________' ,'','',2  
UNION  
SELECT 0,'',0,'','0101D3',0,0,0,CAST (@monExpenses AS VARCHAR(50)),'','C) Total Direct Expenses',2  
UNION  
SELECT 0,'',0,'','0101E',0,0,0,'________________' ,'','',2  
UNION  
SELECT 0,'',0,'','0101E1',0,0,0,CAST(@monIncome-@monCOGS-@monExpenses AS VARCHAR(50)),'','Operating Income = (A -B - C)',2  
UNION  
SELECT 0,'',0,'','0101E2',0,0,0,'&nbsp;' ,'','',2  
UNION  
SELECT 0,'',0,'','0101F',0,0,0,'' ,'','Other Income',2  
UNION  
SELECT numAccountId ,vcAccountName ,  
numParntAcntTypeID ,vcAccountDescription ,  
'0101F1',Opening ,Debit ,Credit, CAST((Debit - Credit) * (-1) AS VARCHAR(50))AS Balance,  
vcAccountCode AS  AccountCode1,  
 CASE   
 WHEN LEN('0101F1')>4 THEN REPLICATE('&nbsp;', LEN('0101F1')-4) + P.[vcAccountName]  
 ELSE P.[vcAccountName]  
 END AS vcAccountName1, 1 AS Type  
 FROM #PLSummary P  
 WHERE vcAccountCode LIKE '010302%'   
-- and numAccountId NOT IN (  
-- SELECT IT.numIncomeChartAcntId FROM Item IT where IT.numDomainID=@numDomainId )  
UNION  
SELECT 0,'',0,'','0101F2',0,0,0,'________________' ,'','',2  
UNION  
SELECT 0,'',0,'','0101F3',0,0,0,CAST(@monOtherIncome AS VARCHAR(50)),'','D) Total Other Income',2  
UNION  
SELECT 0,'',0,'','0101F4',0,0,0,'&nbsp;' ,'','',2  
UNION  
SELECT 0,'',0,'','0101G',0,0,0,'' ,'','Other Expense',2  
UNION  
SELECT numAccountId ,vcAccountName ,  
numParntAcntTypeID ,vcAccountDescription ,  
'0101G1',Opening ,Debit ,Credit, CAST((Debit - Credit) AS VARCHAR(50))AS Balance,  
vcAccountCode AS  AccountCode1,  
 CASE   
 WHEN LEN('0101G1')>4 THEN REPLICATE('&nbsp;', LEN('0101G1')-4) + P.[vcAccountName]  
 ELSE P.[vcAccountName]  
 END AS vcAccountName1, 1 AS Type  
 FROM #PLSummary P  
 WHERE vcAccountCode LIKE '010402%'  
-- and numAccountId NOT IN (  
-- SELECT IT.numIncomeChartAcntId FROM Item IT where IT.numDomainID=@numDomainId )  
UNION  
SELECT 0,'',0,'','0101G2',0,0,0,'________________' ,'','',2  
UNION  
SELECT 0,'',0,'','0101G3',0,0,0,CAST(@monOtherExpense AS VARCHAR(50)),'','E) Total Other Expense',2  
UNION  
--select 0,'',0,'','0101G4',0,0,0,'________________' ,'','',2  
--UNION  
  
  
SELECT 0,'',0,'','0101H4',0,0,0,'________________' ,'','',2  
UNION  
SELECT 0,'',0,'','0101H5',0,0,0,CAST( (@monIncome+@monOtherIncome) - (@monCOGS + @monExpenses +@monOtherExpense) AS VARCHAR(50)) ,'','Net Income = (A + D) - (B + C + E)',2  
UNION  
SELECT 0,'',0,'','0101H6',0,0,0,'&nbsp;' ,'','',2  
/*  
insert into #PLShowGrid1  
select * from #PLShow A  ORDER BY A.vcAccountCode  
insert into #PLShowGrid1  
SELECT * FROM #PLShow1 a ORDER BY A.vcAccountCode*/  
INSERT INTO #PLShowGrid1  
SELECT  A.numAccountId ,A.vcAccountName ,A.numParntAcntTypeID ,A.vcAccountDescription ,A.vcAccountCode ,A.Opening ,A.Debit ,A.Credit ,A.Balance ,A.AccountCode1 ,  
        CASE WHEN LEN(COA.[vcAccountCode]) > 4 AND  A.[TYPE] <> 2  
             THEN REPLICATE('&nbsp;', LEN(COA.[vcAccountCode]) - 4) + A.[vcAccountName]  
             WHEN A.[TYPE] = 2 THEN A.[vcAccountName1]  
             ELSE A.[vcAccountName]  
        END AS vcAccountName1 ,A.Type  
FROM    #PLShow A LEFT JOIN dbo.Chart_Of_Accounts COA ON A.numAccountId = COA.numAccountId ORDER BY A.vcAccountCode,COA.vcAccountCode  
  
INSERT INTO #PLShowGrid1  
SELECT  A.numAccountId ,A.vcAccountName ,A.numParntAcntTypeID ,A.vcAccountDescription ,A.vcAccountCode ,A.Opening ,A.Debit ,A.Credit ,A.Balance ,A.AccountCode1 ,  
        CASE WHEN LEN(COA.[vcAccountCode]) > 4  
             THEN REPLICATE('&nbsp;', LEN(COA.[vcAccountCode]) - 4 ) + A.[vcAccountName]  
             ELSE A.[vcAccountName]  
        END AS vcAccountName1 ,A.Type  
FROM    #PLShow1 A LEFT JOIN dbo.Chart_Of_Accounts COA ON A.numAccountId = COA.numAccountId ORDER BY A.vcAccountCode,COA.vcAccountCode  
  
--insert into #PLShowGrid2  
--SELECT * FROM #PLShow1 a ORDER BY A.vcAccountCode  
  
  
SELECT * FROM #PLShowGrid1  
  
--union  
--select * from #PLShowGrid2  
  
DROP TABLE #PLShowGrid1;  
DROP TABLE #PLShowGrid2;  
DROP TABLE #PLOutPut;  
DROP TABLE #PLSummary;  
DROP TABLE #PLShow;  
DROP TABLE #PLShow1;  
DROP TABLE #view_journal;
END  
/****** Object:  StoredProcedure [dbo].[USP_GetInvoiceList]    Script Date: 07/26/2008 16:17:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj                  
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getinvoicelist')
DROP PROCEDURE usp_getinvoicelist
GO
CREATE PROCEDURE [dbo].[USP_GetInvoiceList]                            
 @numDivisionID as numeric(9)=0 ,
 @tintOppType AS TINYINT=1,
 @numDomainID AS NUMERIC(9)
as                        
 
 
 
IF (@tintOppType = 1 OR  @tintOppType =2 )
BEGIN
	
	SELECT OM.numOppID,OM.vcPOppName,OBD.numOppBizDocsId,OBD.vcBizDocID,
		isnull(OBD.monDealAmount * OM.fltExchangeRate,0) AS monDealAmount,ISNULL((OBD.[monAmountPaid] * OM.fltExchangeRate),0) AS monAmountPaid,
		isnull(OBD.monDealAmount * OM.fltExchangeRate - OBD.[monAmountPaid] * OM.fltExchangeRate,0) AS BalanceDue,
		[dbo].[FormatedDateFromDate](OBD.dtFromDate,OM.numDomainId) AS BillingDate,
		CASE ISNULL(OM.bitBillingTerms,0)  
                 WHEN 1 THEN [dbo].[FormatedDateFromDate](DATEADD(dd,convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(OM.intBillingDays,0)), 0)),OBD.dtFromDate),OM.numDomainId)
                 WHEN 0 THEN [dbo].[FormatedDateFromDate](OBD.[dtFromDate],OM.numDomainId)
               END AS DueDate,
        dbo.GetCreditTerms(OM.numOppId) as Credit,
		dbo.fn_GetContactName(OM.numCreatedBy)  as CreatedName,
		CONVERT(DATETIME, OM.bintCreatedDate) AS  dtCreatedDate,
		CONVERT(DATETIME, OM.bintCreatedDate) AS  numCreatedDate,
		isnull(OM.numPClosingPercent,0)as monAmountPaid,
		isnull(dbo.GetDealAmount(OM.numOppID,getdate(),0),0) as TotalAmt,
		OM.numContactId,
		ISNULL(OM.tintOppStatus,0) As [tintOppStatus]
		FROM dbo.OpportunityMaster OM 
		JOIN dbo.OpportunityBizDocs OBD ON OM.numoppID=OBD.numoppID
		JOIN dbo.DivisionMaster D ON D.numDivisionID = OM.numDivisionId
		JOIN dbo.CompanyInfo C ON C.numCompanyId = D.numCompanyID
		WHERE OM.numDomainId=@numDomainID AND OM.numDomainId = D.numDomainID AND OM.tintOppType= @tintOppType
		AND D.numDivisionID= @numDivisionID --AND ISNULL(bitAuthoritativeBizDocs,0)=1
		AND OM.tintOppStatus=1 
		--AND ISNULL(OBD.tintDeferred,0) <> 1 AND  isnull(OBD.monDealAmount * OM.fltExchangeRate,0) > 0 
		--AND isnull(OBD.monDealAmount * OM.fltExchangeRate - OBD.[monAmountPaid] * OM.fltExchangeRate,0) > 0

----Create a Temporary table to hold data                        
--Create table #tempTable ( ID INT IDENTITY PRIMARY KEY,                         
--      numOppID varchar(15),vcPOppName varchar(100),                  
--      vcBizDocID varchar(100),                
--   BizDocName varchar(100),              
--   CreatedName varchar(100),                       
--      Credit VARCHAR(100),                        
--      numCreatedDate datetime,                        
-- monAmountPaid money,    
-- TotalAmt MONEY,
-- numContactId NUMERIC(9),
-- tintOppStatus TINYINT)                        
--                  
--                  
--declare @strSql as varchar(8000)                        
--set @strSql=
--'SELECT 
--O.numOppID,
--O.vcPOppName,               
--CASE WHEN O.tintOppType = 1 AND O.tintOppStatus = 0 THEN ''Sales Opportunity''
--	 WHEN O.tintOppType = 1 AND O.tintOppStatus = 1 THEN ''Sales Order''
--END as BizDocName,              
--dbo.fn_GetContactName(O.numCreatedBy)  as CreatedName,     
--dbo.GetCreditTerms(O.numOppId) as Credit,                  
--CONVERT(DATETIME, O.bintCreatedDate) AS  dtCreatedDate,
--isnull(O.numPClosingPercent,0)as monAmountPaid,
--isnull(dbo.GetDealAmount(O.numOppID,getdate(),0),0) as TotalAmt,
--O.numContactId,ISNULL(O.tintOppStatus,0) As [tintOppStatus]
--FROM dbo.OpportunityMaster O 
--     JOIN dbo.DivisionMaster D 
--        ON D.numDivisionID = O.numDivisionId
--     JOIN dbo.CompanyInfo C 
--        ON C.numCompanyId = D.numCompanyID
--WHERE O.numDomainId = D.numDomainID '
--      
--set @strSql=@strSql + ' and O.tintOppType= ' + convert(varchar(3),@tintOppType)
--
--if @numDivisionID<>0 set @strSql=@strSql + ' and D.numDivisionID= ' + convert(varchar(15),@numDivisionID)                        
--if @SortChar<>'0' set @strSql=@strSql + ' And vcBizDocID like '''+@SortChar+'%'''   
--IF @bitflag =0
--BEGIN
--	SET @strSql = @strSql + ' And O.tintShipped  =1'                                            
--END 
--ELSE IF @bitflag=1
--BEGIN
--SET @strSql = @strSql + ' And (O.tintOppStatus = 1 OR O.tintOppStatus = 0)'
--	
--END
--
--set @strSql=@strSql +' ORDER BY ' + @columnName +' '+ @columnSortOrder                     
--print @strSql                      
--insert into #tempTable(                        
--      numOppID,                  
-- vcBizDocID,                
-- BizDocName,              
--  CreatedName,                        
--      Credit,                        
--      numCreatedDate,                        
-- monAmountPaid,
-- TotalAmt,
-- numContactId,tintOppStatus)                        
--exec (@strSql)                         
--                        
--  declare @firstRec as integer                         
--  declare @lastRec as integer                        
-- set @firstRec= (@CurrentPage-1) * @PageSize                        
--     set @lastRec= (@CurrentPage*@PageSize+1)                        
--select *,(TotalAmt-monAmountPaid) AS BalanceDue,
--CASE WHEN @tintOppType = 1 AND tintOppStatus = 0 THEN 'Sales Opportunity' 
--	 WHEN @tintOppType = 1 AND tintOppStatus = 1 THEN 'Sales Order' 
--     WHEN @tintOppType = 2 THEN 'Purchase Order' END AS [Type]
-- from #tempTable where ID > @firstRec and ID < @lastRec                        
--set @TotRecs=(select count(*) from #tempTable)                        
--drop table #tempTable
END 
--For bills
IF @tintOppType =  3 
BEGIN
-- SELECT 'Bill' AS [Type],OBD.vcMemo,OBD.vcReference,OBD.monAmount,dbo.FormatedDateFromDate(PD.dtDueDate,OBD.numDomainId) DueDate
-- FROM   dbo.OpportunityBizDocsDetails OBD
--        INNER JOIN dbo.OpportunityBizDocsPaymentDetails PD ON PD.numBizDocsPaymentDetId = OBD.numBizDocsPaymentDetId
--  WHERE OBD.numDivisionID = @numDivisionID AND PD.bitIntegrated=0

SELECT 0 AS numOppId,'Bill' vcPOppName,0 AS numOppBizDocsId,'Bill' + CASE WHEN len(BH.vcReference)=0 THEN '' ELSE '-' + BH.vcReference END AS [vcBizDocID],
						   BH.monAmountDue as monDealAmount,
						   ISNULL(BH.monAmtPaid, 0) as monAmountPaid,
					       ISNULL(BH.monAmountDue, 0) - ISNULL(BH.monAmtPaid, 0) as BalanceDue,
					       [dbo].[FormatedDateFromDate](BH.dtBillDate,BH.numDomainID) AS BillingDate,
						   [dbo].[FormatedDateFromDate](BH.dtDueDate,BH.numDomainID) AS DueDate
						   FROM    
							BillHeader BH 
							WHERE BH.numDomainId=@numDomainID AND BH.numDivisionId = @numDivisionID
							AND ISNULL(BH.monAmountDue, 0) - ISNULL(BH.monAmtPaid, 0) > 0
							
END



-- exec Usp_GetItemAttributesForAPI @numItemCode=822625,@numDomainID=170,@WebApiId = 5
/****** Subject:  StoredProcedure [dbo].[Usp_GetItemAttributesForAPI]    Script Date: 12th Jan,2015 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--created by Manish Anjara
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'Usp_GetItemAttributesForAPI' )
    DROP PROCEDURE Usp_GetItemAttributesForAPI
GO
CREATE PROCEDURE [dbo].[Usp_GetItemAttributesForAPI]
    @numItemCode AS NUMERIC(9) = 0 ,
    @numDomainID AS NUMERIC(9) ,
    @WebApiId AS INT
    --@byteMode AS TINYINT = 0
AS
    DECLARE @numDefaultWarehouseID AS NUMERIC(18)
    SELECT  @numDefaultWarehouseID = ISNULL(numwarehouseID, 0)
    FROM    [dbo].[WebAPIDetail] AS WAD
    WHERE   [WAD].[numDomainId] = @numDomainID
            AND [WAD].[WebApiId] = @WebApiId
    IF ISNULL(@numDefaultWarehouseID, 0) = 0
        BEGIN
            SELECT TOP 1
                    *
            FROM    [dbo].[WareHouseItems] AS WHI
            WHERE   [WHI].[numItemID] = @numItemCode
                    AND [WHI].[numDomainID] = @numDomainID
        END


    SELECT  DISTINCT
            FMST.fld_id ,
            Fld_label ,
            ISNULL(FMST.numlistid, 0) AS numlistid ,
            [LD].[numListItemID] ,
            [LD].[vcData]
    INTO    #tmpSpec
    FROM    CFW_Fld_Master FMST
            LEFT JOIN CFw_Grp_Master Tmst ON Tmst.Grp_id = FMST.subgrp
            JOIN CFW_Loc_Master Lmst ON Lmst.Loc_id = FMST.Grp_id
            JOIN [dbo].[ListMaster] AS LM ON [FMST].[numlistid] = LM.[numListID]
            JOIN [dbo].[ListDetails] AS LD ON LD.[numListID] = LM.[numListID]
                                              AND [FMST].[numlistid] = LD.[numListID]
            JOIN [dbo].[ItemGroupsDTL] AS IGD ON IGD.[numOppAccAttrID] = [FMST].[Fld_id]
            JOIN [dbo].[CFW_Fld_Values_Serialized_Items] AS CFVSI ON [CFVSI].[Fld_ID] = [FMST].[Fld_id]
                                                              AND CONVERT(NUMERIC, [CFVSI].[Fld_Value]) = [LD].[numListItemID]
                                                              AND [CFVSI].[RecId] IN (
                                                              SELECT
                                                              numWarehouseItemID
                                                              FROM
                                                              [dbo].[WareHouseItems]
                                                              WHERE
                                                              numwarehouseID = @numDefaultWarehouseID
                                                              AND numDomainID = @numDomainID
                                                              AND numItemID = @numItemCode )
    WHERE   FMST.numDomainID = @numDomainID
            AND Lmst.Loc_id = 9
    ORDER BY [fmst].[Fld_id]

	--SELECT * FROM [#tmpSpec] AS TS
    SELECT DISTINCT
            TS.fld_label [Name] ,
            STUFF(( SELECT  ',' + CAST([vcData] AS VARCHAR(10))
                    FROM    #tmpSpec
                    WHERE   [ts].[Fld_id] = #tmpSpec.[Fld_id]
                  FOR
                    XML PATH('')
                  ), 1, 1, '') [Value]
    FROM    [#tmpSpec] AS TS
    

    DECLARE @bitSerialize AS BIT                      
    DECLARE @str AS NVARCHAR(MAX)                 
    DECLARE @str1 AS NVARCHAR(MAX)               
    DECLARE @ColName AS VARCHAR(25)                      
    SET @str = ''                       
                        
    DECLARE @bitKitParent BIT
    DECLARE @numItemGroupID AS NUMERIC(9)                        
                        
    SELECT  @numItemGroupID = numItemGroup ,
            @bitSerialize = CASE WHEN bitSerialized = 0 THEN bitLotNo
                                 ELSE bitSerialized
                            END ,
            @bitKitParent = ( CASE WHEN ISNULL(bitKitParent, 0) = 1
                                        AND ISNULL(bitAssembly, 0) = 1 THEN 0
                                   WHEN ISNULL(bitKitParent, 0) = 1 THEN 1
                                   ELSE 0
                              END )
    FROM    Item
    WHERE   numItemCode = @numItemCode                        
    IF @bitSerialize = 1
        SET @ColName = 'numWareHouseItmsDTLID,1'              
    ELSE
        SET @ColName = 'numWareHouseItemID,0'              
              
--Create a Temporary table to hold data                                                            
    CREATE TABLE #tempTable
        (
          ID INT IDENTITY
                 PRIMARY KEY ,
          numCusFlDItemID NUMERIC(9)
        )                         
                        
    INSERT  INTO #tempTable
            ( numCusFlDItemID
            )
            SELECT DISTINCT
                    ( numOppAccAttrID )
            FROM    ItemGroupsDTL
            WHERE   numItemGroupID = @numItemGroupID
                    AND tintType = 2                       
                          
                 
    DECLARE @ID AS NUMERIC(9)                        
    DECLARE @numCusFlDItemID AS VARCHAR(20)                        
    DECLARE @fld_label AS VARCHAR(100) ,
        @fld_type AS VARCHAR(100)                        
    SET @ID = 0                        
    SELECT TOP 1
            @ID = ID ,
            @numCusFlDItemID = numCusFlDItemID ,
            @fld_label = fld_label ,
            @fld_type = fld_type
    FROM    #tempTable
            JOIN CFW_Fld_Master ON numCusFlDItemID = Fld_ID                        
                         
    WHILE @ID > 0
        BEGIN                        
                          
           -- SET @str = @str + ',  dbo.GetCustFldItems(' + @numCusFlDItemID + ',9,' + @ColName + ') as [' + @fld_label + ']'
	
            --IF @byteMode = 1
            SET @str = @str + ', dbo.GetCustFldItemsValue(' + @numCusFlDItemID + ',9,' + @ColName + ',''' + @fld_type + ''') as [' + @fld_label + '_c]'                                        
                          
            SELECT TOP 1
                    @ID = ID ,
                    @numCusFlDItemID = numCusFlDItemID ,
                    @fld_label = fld_label
            FROM    #tempTable
                    JOIN CFW_Fld_Master ON numCusFlDItemID = Fld_ID
                                           AND ID > @ID                        
            IF @@rowcount = 0
                SET @ID = 0                        
                          
        END                        
                       
--      

    DECLARE @KitOnHand AS NUMERIC(9);
    SET @KitOnHand = 0

    SET @str1 = 'select '

    SET @str1 = @str1 + 'I.numItemCode,'
	
    SET @str1 = @str1
        + 'numWareHouseItemID,isnull(vcWarehouse,'''') + '': '' + isnull(WL.vcLocation,'''') vcWarehouse,W.numWareHouseID,
Case when @bitKitParent=1 then ISNULL(dbo.fn_GetKitInventoryByWarehouse(I.numItemCode, W.numWareHouseID),0) else isnull(numOnHand,0) end as [OnHand],
CAST((dbo.fn_UOMConversion(ISNULL(I.numBaseUnit,0),I.numItemCode,I.numDomainId,ISNULL(I.numPurchaseUnit,0)) * Case when @bitKitParent=1 THEN ISNULL(dbo.fn_GetKitInventoryByWarehouse(I.numItemCode, W.numWareHouseID),0) ELSE isnull(numOnHand,0) end) as numeric(18,2)) as PurchaseOnHand,
CAST((dbo.fn_UOMConversion(ISNULL(I.numBaseUnit,0),I.numItemCode,I.numDomainId,ISNULL(I.numSaleUnit,0)) * Case when @bitKitParent=1 THEN ISNULL(dbo.fn_GetKitInventoryByWarehouse(I.numItemCode, W.numWareHouseID),0) ELSE isnull(numOnHand,0) end) as numeric(18,2)) as SalesOnHand,
Case when @bitKitParent=1 THEN 0 ELSE isnull(numReorder,0) END as [Reorder] 
,Case when @bitKitParent=1 THEN 0 ELSE isnull(numOnOrder,0) END as [OnOrder]
,Case when @bitKitParent=1 THEN 0 ELSE isnull(numAllocation,0) END as [Allocation] 
,Case when @bitKitParent=1 THEN 0 ELSE isnull(numBackOrder,0) END as [BackOrder]
,0 as Op_Flag , isnull(WL.vcLocation,'''') Location,isnull(WL.numWLocationID,0) as numWLocationID,
round(isnull(monWListPrice,0),2) Price,isnull(vcWHSKU,'''') as SKU,isnull(vcBarCode,'''') as BarCode '
        + CASE WHEN @bitSerialize = 1 THEN ' '
               ELSE @str
          END
        + '                   
,(SELECT CASE WHEN ISNULL(subI.bitKitParent,0) = 1 AND ISNULL((SELECT COUNT(*) FROM OpportunityItems 
															   WHERE OpportunityItems.numItemCode = WareHouseItems.numItemID
															   AND WareHouseItems.numWareHouseItemID = OpportunityItems.numWarehouseItmsID),0) = 0 THEN 1
			ELSE 0 
	    END 
FROM Item subI WHERE subI.numItemCode = WareHouseItems.numItemID) [IsDeleteKitWarehouse],@bitKitParent [bitKitParent]
from WareHouseItems                           
join Warehouses W                             
on W.numWareHouseID=WareHouseItems.numWareHouseID 
left join WarehouseLocation WL on WL.numWLocationID = WareHouseItems.numWLocationID
join Item I on I.numItemCode=WareHouseItems.numItemID and WareHouseItems.numDomainId=I.numDomainId
where numItemID=' + CONVERT(VARCHAR(15), @numItemCode)
        + ' AND WareHouseItems.numWarehouseItemID IN (
                                                              SELECT
                                                              numWarehouseItemID
                                                              FROM
                                                              [dbo].[WareHouseItems]
                                                              WHERE
                                                              numwarehouseID = '
        + CONVERT(VARCHAR(15), @numDefaultWarehouseID)
        + '
                                                              AND numDomainID = '
        + CONVERT(VARCHAR(15), @numDomainID)
        + '
                                                              AND numItemID = '
        + CONVERT(VARCHAR(15), @numItemCode) + ' ) '
   
    PRINT ( @str1 )           

    EXECUTE sp_executeSQL @str1, N'@bitKitParent bit', @bitKitParent
                       

GO

GO
/****** Object:  StoredProcedure [dbo].[USP_GetItems_API]    Script Date: 05/07/2009 17:44:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
---created by Chintan prajapati
--EXEC USP_GetItems_API 1,1,'',''
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetItems_API')
DROP PROCEDURE USP_GetItems_API
GO
CREATE PROCEDURE [dbo].[USP_GetItems_API]
          @numDomainID AS NUMERIC(9),
          @WebAPIId    AS int,
          @CreatedDate AS VARCHAR(50) ='',
          @ModifiedDate AS VARCHAR(50)='' 
AS
     
IF(@ModifiedDate='' and @CreatedDate='')
BEGIN
	IF @WebAPIId = 5
	BEGIN
		SELECT   numItemCode,
			   vcItemName,
				ISNULL((SELECT TOP 1([monWListPrice])
				 FROM   [WareHouseItems]
				 WHERE  numItemId = numItemCode
						AND [numWareHouseID] IN (SELECT ISNULL(numWareHouseId,0) AS numDefaultWareHouseID FROM dbo.WebAPIDetail 
												 WHERE  [numDomainId] = I.[numDomainID] AND WebApiId = @WebAPIId)),
				I.monListPrice) AS monListPrice,
			   vcModelID,
			   ISNULL(fltWeight,0) intWeight,
			   ISNULL((SELECT TOP 1([numOnHand])
				 FROM   [WareHouseItems]
				 WHERE  numItemId = numItemCode
						AND [numWareHouseID] IN (SELECT ISNULL(numWareHouseId,0) AS numDefaultWareHouseID FROM dbo.WebAPIDetail 
												 WHERE  [numDomainId] = I.[numDomainID] AND WebApiId = @WebAPIId)),
				0) AS QtyOnHand,
			   ISNULL(IA.vcAPIItemID,0) AS vcAPIItemID, ISNULL(IA.vcSKU, ISNULL(I.[vcSKU],'')) [vcAPISKU],I.numDomainID

	  FROM     Item I 
	  LEFT OUTER JOIN [ItemAPI] IA ON I.[numItemCode] = IA.[numItemID] AND IA.WebApiId = @WebAPIId 
	  WHERE    I.numDomainID = @numDomainID  
		AND  LEN(vcItemName) >0
		AND I.charItemType ='P'
		AND vcExportToAPI IS NOT NULL
		AND @WebAPIId IN (SELECT * FROM dbo.Split(vcExportToAPI,','))
	END
	ELSE
	BEGIN

		SELECT   numItemCode,
				   vcItemName,
					ISNULL((SELECT TOP 1([monWListPrice])
					 FROM   [WareHouseItems]
					 WHERE  numItemId = numItemCode
							AND [numWareHouseID] IN (SELECT ISNULL(numWareHouseId,0) AS numDefaultWareHouseID FROM dbo.WebAPIDetail 
													 WHERE  [numDomainId] = I.[numDomainID] AND WebApiId = @WebAPIId)),
					I.monListPrice) AS monListPrice,
				   vcModelID,
				   ISNULL(fltWeight,0) intWeight,
				   ISNULL((SELECT TOP 1([numOnHand])
					 FROM   [WareHouseItems]
					 WHERE  numItemId = numItemCode
							AND [numWareHouseID] IN (SELECT ISNULL(numWareHouseId,0) AS numDefaultWareHouseID FROM dbo.WebAPIDetail 
													 WHERE  [numDomainId] = I.[numDomainID] AND WebApiId = @WebAPIId)),
					0) AS QtyOnHand,
				   ISNULL(IA.vcAPIItemID,0) AS vcAPIItemID, ISNULL(WI.vcWHSKU, '') [vcAPISKU],I.numDomainID

		  FROM     Item I 
		  LEFT JOIN dbo.WareHouseItems WI ON I.numItemCode = WI.numItemID
		  LEFT OUTER JOIN [ItemAPI] IA ON I.[numItemCode] = IA.[numItemID] AND IA.WebApiId = @WebAPIId 
		  WHERE    I.numDomainID = @numDomainID  
			--AND LEN(vcModelID) >0
			AND  LEN(vcItemName) >0
			AND I.charItemType ='P'
			AND vcExportToAPI IS NOT NULL
			AND @WebAPIId IN (SELECT * FROM dbo.Split(vcExportToAPI,','))
		
	END
END
ELSE
IF(@ModifiedDate ='')
BEGIN
	IF @WebAPIId = 5
	BEGIN
		SELECT   numItemCode,
				   vcItemName,
					ISNULL((SELECT TOP 1([monWListPrice])
					 FROM   [WareHouseItems]
					 WHERE  numItemId = numItemCode
							AND [numWareHouseID] IN (SELECT ISNULL(numWareHouseId,0) AS numDefaultWareHouseID FROM dbo.WebAPIDetail 
													 WHERE  [numDomainId] = I.[numDomainID] AND WebApiId = @WebAPIId)),
					I.monListPrice) AS monListPrice,
				   vcModelID,
				   ISNULL(fltWeight,0) intWeight,
				   ISNULL((SELECT TOP 1([numOnHand])
					 FROM   [WareHouseItems]
					 WHERE  numItemId = numItemCode
							AND [numWareHouseID] IN (SELECT ISNULL(numWareHouseId,0) AS numDefaultWareHouseID FROM dbo.WebAPIDetail 
													 WHERE  [numDomainId] = I.[numDomainID] AND WebApiId = @WebAPIId)),
					0) AS QtyOnHand,
				   ISNULL(IA.vcAPIItemID,0) AS vcAPIItemID, ISNULL(IA.vcSKU, ISNULL(I.[vcSKU],'')) [vcAPISKU],I.numDomainID

		  FROM     Item I 
		  LEFT OUTER JOIN [ItemAPI] IA ON I.[numItemCode] = IA.[numItemID] AND IA.WebApiId = @WebAPIId 
		  WHERE    I.numDomainID = @numDomainID AND 
			bintCreatedDate > @CreatedDate
			AND  LEN(vcItemName) >0
			AND I.charItemType ='P'
			AND vcExportToAPI IS NOT NULL
			AND @WebAPIId IN (SELECT * FROM dbo.Split(vcExportToAPI,','))	
	END
	ELSE
	BEGIN
		SELECT   numItemCode,
				   vcItemName,
					ISNULL((SELECT TOP 1([monWListPrice])
					 FROM   [WareHouseItems]
					 WHERE  numItemId = numItemCode
							AND [numWareHouseID] IN (SELECT ISNULL(numWareHouseId,0) AS numDefaultWareHouseID FROM dbo.WebAPIDetail 
													 WHERE  [numDomainId] = I.[numDomainID] AND WebApiId = @WebAPIId)),
					I.monListPrice) AS monListPrice,
				   vcModelID,
				   ISNULL(fltWeight,0) intWeight,
				   ISNULL((SELECT TOP 1([numOnHand])
					 FROM   [WareHouseItems]
					 WHERE  numItemId = numItemCode
							AND [numWareHouseID] IN (SELECT ISNULL(numWareHouseId,0) AS numDefaultWareHouseID FROM dbo.WebAPIDetail 
													 WHERE  [numDomainId] = I.[numDomainID] AND WebApiId = @WebAPIId)),
					0) AS QtyOnHand,
				   ISNULL(IA.vcAPIItemID,0) AS vcAPIItemID, ISNULL(WI.vcWHSKU, '') [vcAPISKU],I.numDomainID

		  FROM     Item I 
		  LEFT JOIN dbo.WareHouseItems WI ON I.numItemCode = WI.numItemID
		  LEFT OUTER JOIN [ItemAPI] IA ON I.[numItemCode] = IA.[numItemID] AND IA.WebApiId = @WebAPIId 
		  WHERE    I.numDomainID = @numDomainID AND 
			bintCreatedDate > @CreatedDate
				--AND LEN(vcModelID) >0
			AND  LEN(vcItemName) >0
			AND I.charItemType ='P'
			AND vcExportToAPI IS NOT NULL
			AND @WebAPIId IN (SELECT * FROM dbo.Split(vcExportToAPI,','))
	END	
END
ELSE IF(@CreatedDate ='')
BEGIN
	IF @WebAPIId = 5
	BEGIN
		SELECT  numItemCode,
			    vcItemName,
			    ISNULL((SELECT TOP 1([monWListPrice])
                 FROM   [WareHouseItems]
                 WHERE  numItemId = numItemCode
                       AND [numWareHouseID] IN (SELECT ISNULL(numWareHouseId,0) AS numDefaultWareHouseID FROM dbo.WebAPIDetail 
                                                 WHERE  [numDomainId] = I.[numDomainID] AND WebApiId = @WebAPIId)),
                I.monListPrice) AS monListPrice,
			   vcModelID,
			   ISNULL(fltWeight,0) intWeight,
			   ISNULL((SELECT TOP 1([numOnHand])
                 FROM   [WareHouseItems]
                 WHERE  numItemId = numItemCode
                       AND [numWareHouseID] IN (SELECT ISNULL(numWareHouseId,0) AS numDefaultWareHouseID FROM dbo.WebAPIDetail 
                                                 WHERE  [numDomainId] = I.[numDomainID] AND WebApiId = @WebAPIId)),
                0) AS QtyOnHand,
			   ISNULL(IA.vcAPIItemID,0) AS vcAPIItemID, ISNULL(IA.vcSKU, ISNULL(I.[vcSKU],'')) [vcAPISKU],I.numDomainID
		  FROM     Item I 
		  LEFT OUTER JOIN [ItemAPI] IA ON I.[numItemCode] = IA.[numItemID] AND IA.WebApiId = @WebAPIId 
		  WHERE    I.numDomainID = @numDomainID AND 
			bintModifiedDate > @ModifiedDate
				--AND LEN(vcModelID) >0
			AND  LEN(vcItemName) >0
			AND I.charItemType ='P'
			AND vcExportToAPI IS NOT NULL
			AND @WebAPIId IN (SELECT * FROM dbo.Split(vcExportToAPI,','))	
	END
	ELSE
	BEGIN
		SELECT  numItemCode,
			    vcItemName,
			    ISNULL((SELECT TOP 1([monWListPrice])
                 FROM   [WareHouseItems]
                 WHERE  numItemId = numItemCode
                       AND [numWareHouseID] IN (SELECT ISNULL(numWareHouseId,0) AS numDefaultWareHouseID FROM dbo.WebAPIDetail 
                                                 WHERE  [numDomainId] = I.[numDomainID] AND WebApiId = @WebAPIId)),
                I.monListPrice) AS monListPrice,
			   vcModelID,
			   ISNULL(fltWeight,0) intWeight,
			   ISNULL((SELECT TOP 1([numOnHand])
                 FROM   [WareHouseItems]
                 WHERE  numItemId = numItemCode
                       AND [numWareHouseID] IN (SELECT ISNULL(numWareHouseId,0) AS numDefaultWareHouseID FROM dbo.WebAPIDetail 
                                                 WHERE  [numDomainId] = I.[numDomainID] AND WebApiId = @WebAPIId)),
                0) AS QtyOnHand,
			   ISNULL(IA.vcAPIItemID,0) AS vcAPIItemID, ISNULL(WI.vcWHSKU, '') [vcAPISKU],I.numDomainID
		  FROM     Item I 
		  LEFT JOIN dbo.WareHouseItems WI ON I.numItemCode = WI.numItemID
		  LEFT OUTER JOIN [ItemAPI] IA ON I.[numItemCode] = IA.[numItemID] AND IA.WebApiId = @WebAPIId 
		  WHERE    I.numDomainID = @numDomainID AND 
			bintModifiedDate > @ModifiedDate
				--AND LEN(vcModelID) >0
			AND  LEN(vcItemName) >0
			AND I.charItemType ='P'
			AND vcExportToAPI IS NOT NULL
			AND @WebAPIId IN (SELECT * FROM dbo.Split(vcExportToAPI,','))
	END		
END	
  
  

/****** Object:  StoredProcedure [dbo].[USP_GetItemsAndKits]    Script Date: 07/26/2008 16:17:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj  
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getitemsandkits')
DROP PROCEDURE usp_getitemsandkits
GO
CREATE PROCEDURE [dbo].[USP_GetItemsAndKits]        
@numDomainID as numeric(9)=0,
@strItem as varchar(10),
@type AS int,
@bitAssembly AS BIT = 0,
@numItemCode AS NUMERIC(18,0) = 0      
as   
IF @type=0 
BEGIN   
	select numItemCode,vcItemName from Item where numDomainID =@numDomainID  and vcItemName like @strItem+'%'
END

ELSE IF @type=1 
BEGIN   
	IF @bitAssembly = 1
	BEGIN
		--ONLY NON INVENTORY, SERVICE AND INVENTORY ITEMS WITH PRIMARY VENDOR CAN BE ADDED TO ASSEMBLY
		SELECT 
			numItemCode,vcItemName,bitKitParent,bitAssembly 
		FROM 
			Item 
		WHERE 
			Item.numDomainID =@numDomainID
			AND vcItemName like @strItem+'%' 
			AND ISNULL(bitLotNo,0)<>1 
			AND ISNULL(bitSerialized,0)<>1 
			AND ISNULL(bitKitParent,0)<>1 /*and ISNULL(bitAssembly,0)<>1 and  */
			AND ISNULL(bitRental,0) <> 1
			AND ISNULL(bitAsset,0) <> 1
			AND 1=(CASE 
				   WHEN charitemtype='P' THEN
						CASE 
							WHEN ((SELECT COUNT(numWareHouseItemID) FROM WareHouseItems WHERE numDomainID=Item.numDomainID AND numItemID=Item.numItemCode) > 0 AND 
							     (SELECT COUNT(numDivisionID) FROM DivisionMaster WHERE numDivisionID=Item.numVendorID) > 0) THEN 1 
							ELSE 
								0 
						END 
					ELSE 
						1 
					END) 
			AND numItemCode NOT IN (@numItemCode)
	END
	ELSE
	BEGIN
		select numItemCode,vcItemName,bitKitParent,bitAssembly from Item where numDomainID =@numDomainID
		AND vcItemName like @strItem+'%' 
		AND ISNULL(bitLotNo,0)<>1 and ISNULL(bitSerialized,0)<>1 
		AND ISNULL(bitKitParent,0)<>1 /*and ISNULL(bitAssembly,0)<>1 and  */
		AND 1=(CASE WHEN charitemtype='P' THEN
				CASE WHEN (SELECT COUNT(numWareHouseItemID) FROM WareHouseItems WHERE numDomainID=Item.numDomainID 
				AND numItemID=Item.numItemCode)>0 THEN 1 ELSE 0 END ELSE 1 END)
	END
END				
GO
GO
/****** Object:  StoredProcedure [dbo].[USP_GetLeadList1]    Script Date: 09/08/2010 14:11:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--- Created By Anoop Jayaraj                                                                
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getleadlist1')
DROP PROCEDURE usp_getleadlist1
GO
CREATE PROCEDURE [dbo].[USP_GetLeadList1]                                                                          
 @numGrpID numeric,                                                                              
 @numUserCntID numeric,
 @tintUserRightType tinyint,
 @CustName varchar(100)='',                                                                              
 @FirstName varChar(100)= '',                                                                              
 @LastName varchar(100)='',                                                                            
 @SortChar char(1)='0' ,                                                                            
 @numFollowUpStatus as numeric(9)=0 ,                                                                            
 @CurrentPage int,                                                                            
 @PageSize int,                                                                            
 @columnName as Varchar(50)='',                                                                            
 @columnSortOrder as Varchar(10)='',                                                    
 @ListType as tinyint=0,                                                    
 @TeamType as tinyint=0,                                                     
 @TeamMemID as numeric(9)=0,                                                
 @numDomainID as numeric(9)=0    ,                  
 @ClientTimeZoneOffset as int  ,              
 @bitPartner as bit,
 @bitActiveInActive as bit,
 @vcRegularSearchCriteria varchar(1000)='',
 @vcCustomSearchCriteria varchar(1000)=''                                                                                  
AS       

SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

select numAddressID,numCountry,numState,bitIsPrimary,tintAddressOf,tintAddressType,numRecordID,numDomainID into #tempAddressLead from AddressDetails  where tintAddressOf=2 AND bitIsPrimary=1 and numDomainID=@numDomainID

declare @join as varchar(400)              
set @join = ''       
 if @columnName like 'CFW.Cust%'             
begin            
             
 set @join= ' left Join CFW_FLD_Values CFW on CFW.RecId=DM.numDivisionID and CFW.fld_id= '+replace(@columnName,'CFW.Cust','') +' '                                                     
 set @columnName='CFW.Fld_Value'            
              
end                                     
if @columnName like 'DCust%'            
begin            
                                                   
 set @join= ' left Join CFW_FLD_Values CFW on CFW.RecId=DM.numDivisionID  and CFW.fld_id= '+replace(@columnName,'DCust','')                                                                                               
 set @join= @join +' left Join ListDetails LstCF on LstCF.numListItemID=CFW.Fld_Value  '            
 set @columnName='LstCF.vcData'              
            
end      
      
                                                                             
declare @strSql as varchar(8000)                                                                             


declare @firstRec as integer                                                                            
  declare @lastRec as integer                                                                            
 set @firstRec= (@CurrentPage-1) * @PageSize                                                                           
     set @lastRec= (@CurrentPage*@PageSize+1)                                                                            
 
  DECLARE @strShareRedordWith AS VARCHAR(300);
 SET @strShareRedordWith=' DM.numDivisionID in (select SR.numRecordID FROM ShareRecord SR WHERE SR.numDomainID='+ convert(varchar(15),@numDomainID )+' AND SR.numModuleID=1 AND SR.numAssignedTo='+convert(varchar(15),@numUserCntId)+') '
                                                                                                                           
                                                              
 set @strSql= '
DECLARE @numDomain NUMERIC
SET @numDomain='+ convert(varchar(15),@numDomainID)+';

 with tblLeads as ( SELECT     '         
 set  @strSql= @strSql+ ' ROW_NUMBER() OVER (ORDER BY '+ CASE WHEN PATINDEX('%txt%',@columnName)>0 THEN ' CONVERT(VARCHAR(Max),' + @columnName + ')'  WHEN @columnName='numRecOwner' THEN 'DM.numRecOwner' ELSE @columnName end +' '+ @columnSortOrder+') AS RowNumber,                                                                                            
	COUNT(*) OVER(PARTITION BY NULL) AS TotalRowCount,
   DM.numDivisionID 
  FROM                                                                                
   CompanyInfo cmp join divisionmaster DM on cmp.numCompanyID=DM.numCompanyID                                                 
 left join ListDetails lst on lst.numListItemID=DM.numFollowUpStatus           '  +@join+  '                                            
 join AdditionalContactsInformation ADC on ADC.numDivisionID=DM.numDivisionID  
 WHERE                                                                               
   DM.tintCRMType=0  and  ISNULL(ADC.bitPrimaryContact,0)=1   
  AND CMP.numDomainID=DM.numDomainID   
 and DM.numDomainID=ADC.numDomainID                                                        
 AND DM.numGrpID='+convert(varchar(15),@numGrpID)+ '                                                 
 AND DM.numDomainID=@numDomain'               
      
SET @strSql=@strSql + ' AND DM.bitActiveInActive=' + CONVERT(VARCHAR(15), @bitActiveInActive) 
        
if @bitPartner = 1 set @strSql=@strSql+' and (DM.numAssignedTo='+convert(varchar(15),@numUserCntID)+               
           ' or DM.numCreatedBy='+convert(varchar(15),@numUserCntID)+ ') '              
                                                                                    
 if @FirstName<>'' set @strSql=@strSql+' and ADC.vcFirstName  like '''+@FirstName+'%'''                                            
 if @LastName<>'' set @strSql=@strSql+' and ADC.vcLastName like '''+@LastName+'%'''                                            
 if @CustName<>'' set @strSql=@strSql+' and CMP.vcCompanyName like '''+@CustName+'%'''                                                                           
                                                                                   
if @numFollowUpStatus<>0 set @strSql=@strSql + '  AND DM.numFollowUpStatus='+convert(varchar(15),@numFollowUpStatus)                                                                             
if @SortChar<>'0' set @strSql=@strSql + ' And CMP.vcCompanyName like '''+@SortChar+'%'''                            
if @tintUserRightType=1 AND @numGrpID<>2 set @strSql=@strSql + ' AND (DM.numRecOwner = '+convert(varchar(15),@numUserCntID)+ ' or DM.numAssignedTo='+convert(varchar(15),@numUserCntID)+ ' or ' + @strShareRedordWith +')'                                                                             
else if @tintUserRightType=2 set @strSql=@strSql + ' AND (DM.numTerID in (select numTerritoryID from  UserTerritory where numUserCntID= '+convert(varchar(15),@numUserCntID)+' ) or DM.numTerID=0 or DM.numAssignedTo='+convert(varchar(15),@numUserCntID)+ ' or ' + @strShareRedordWith +')' 


if @ListType=0 -- when MySelf radiobutton is checked
begin                                                    
IF @tintUserRightType=1 AND @numGrpID=1 set @strSql=@strSql + ' AND (DM.numRecOwner = '+convert(varchar(15),@numUserCntID)+ ' or DM.numAssignedTo='+convert(varchar(15),@numUserCntID)+ ' or ' + @strShareRedordWith +')'                                                                
if @tintUserRightType=1 AND @numGrpID=5 set @strSql=@strSql + ' AND (DM.numRecOwner = '+convert(varchar(15),@numUserCntID)+ ' or DM.numAssignedTo='+convert(varchar(15),@numUserCntID)+ ' or ' + @strShareRedordWith +')'                                                     
end                                          
                                      
else if @ListType=1 -- when Team Selected radiobutton is checked
begin                                 
if @TeamMemID=0 -- When All team members is selected in dropdown
 begin                                                  
 if @numGrpID<>2 set @strSql=@strSql + '  AND (DM.numRecOwner  in(select numContactID from  AdditionalContactsInformation                                      
		where numTeam in(select numTeam from ForReportsByTeam where numUserCntID='+convert(varchar(15),@numUserCntID)+' and tintType='+convert(varchar(15),@TeamType)+') ) or DM.numAssignedTo in(select numContactID from  AdditionalContactsInformation             
		where numTeam in(select numTeam from ForReportsByTeam where numUserCntID='+convert(varchar(15),@numUserCntID)+' and tintType='+convert(varchar(15),@TeamType)+'))' + ' or ' + @strShareRedordWith +')'                                                                       
                                                   
 end                                                  
else-- When All team member name is selected in dropdown
 begin                                                  
 if @numGrpID<>2 set @strSql=@strSql + '  AND ( DM.numRecOwner='+convert(varchar(15),@TeamMemID) +'  or  DM.numAssignedTo='+convert(varchar(15),@TeamMemID) + ' or ' + @strShareRedordWith +')'                                                         
                                                   
 end                                                   
end

IF @vcRegularSearchCriteria<>'' 
BEGIN
	IF PATINDEX('%numShipCountry%', @vcRegularSearchCriteria)>0
	BEGIN
		set @strSql=@strSql+' and DM.numDivisionID in (select distinct numRecordID from #tempAddressLead AD where tintAddressOf=2 and tintAddressType=2 AND bitIsPrimary=1 AND  ' + replace(@vcRegularSearchCriteria,'numShipCountry','numCountry') + ')'
	END
	ELSE IF PATINDEX('%numBillCountry%', @vcRegularSearchCriteria)>0
	BEGIN
		set @strSql=@strSql+' and DM.numDivisionID in (select distinct numRecordID from #tempAddressLead AD where tintAddressOf=2 and tintAddressType=1 AND bitIsPrimary=1 AND ' + replace(@vcRegularSearchCriteria,'numBillCountry','numCountry') + ')'
	END
	ELSE IF PATINDEX('%numShipState%', @vcRegularSearchCriteria)>0
	BEGIN
		set @strSql=@strSql+' and DM.numDivisionID in (select distinct numRecordID from #tempAddressLead AD where tintAddressOf=2 and tintAddressType=2 AND bitIsPrimary=1 AND ' + replace(@vcRegularSearchCriteria,'numShipState','numState') + ')'
	END
	ELSE IF PATINDEX('%numBillState%', @vcRegularSearchCriteria)>0
	BEGIN
		set @strSql=@strSql+' and DM.numDivisionID in (select distinct numRecordID from #tempAddressLead AD where tintAddressOf=2 and tintAddressType=1 AND bitIsPrimary=1 AND ' + replace(@vcRegularSearchCriteria,'numBillState','numState') + ')'
	END
	ELSE
		set @strSql=@strSql+' and ' + @vcRegularSearchCriteria 
END
 
IF @vcCustomSearchCriteria<>'' set @strSql=@strSql+' and DM.numDivisionID in (select distinct CFW.RecId from CFW_FLD_Values CFW where ' + @vcCustomSearchCriteria + ')'

  set @strSql=@strSql+')'
--set @strSql=@strSql+'), AddUnreadMailCount AS 
--             ( SELECT ISNULL(VIE.Total,0) AS TotalEmail,t.* FROM tblLeads t left JOIN View_InboxEmail VIE  WITH (NOEXPAND) ON VIE.numDomainID = @numDomain AND  VIE.numContactId = t.numContactId
--                            )
--             , AddActionCount AS
--             ( SELECT    ISNULL(VOA.OpenActionItemCount,0) AS TotalActionItem,U.* FROM AddUnreadMailCount U LEFT JOIN  VIEW_OPEN_ACTION_ITEMS VOA WITH (NOEXPAND)  ON VOA.numDomainID = @numDomain AND  VOA.numDivisionId = U.numDivisionID 
--                            '                                  
                                                                           
declare @tintOrder as tinyint                                                
declare @vcFieldName as varchar(50)                                                
declare @vcListItemType as varchar(3)                                           
declare @vcAssociatedControlType varchar(20)                                                
declare @numListID AS numeric(9)                                                
declare @vcDbColumnName varchar(20)                    
declare @WhereCondition varchar(2000)                     
declare @vcLookBackTableName varchar(2000)              
Declare @bitCustomField as BIT
DECLARE @bitAllowEdit AS CHAR(1)                 
Declare @numFieldId as numeric  
DECLARE @bitAllowSorting AS CHAR(1)   
declare @vcColumnName AS VARCHAR(500)                  
set @tintOrder=0                                                
set @WhereCondition =''               
                 
declare @Nocolumns as tinyint              
--declare @DefaultNocolumns as tinyint         
set @Nocolumns=0              
 
select @Nocolumns=isnull(sum(TotalRow),0)  from(            
Select count(*) TotalRow from View_DynamicColumns where numFormId=34 and numUserCntID=@numUserCntID and numDomainID=@numDomainID AND tintPageType=1 AND numRelCntType=1 
Union 
Select count(*) TotalRow from View_DynamicCustomColumns where numFormId=34 and numUserCntID=@numUserCntID and numDomainID=@numDomainID AND tintPageType=1 AND numRelCntType=1) TotalRows
 

--Set @DefaultNocolumns=  @Nocolumns
  

CREATE TABLE #tempForm (tintOrder TINYINT,vcDbColumnName nvarchar(50),vcOrigDbColumnName nvarchar(50),vcFieldName nvarchar(50),vcAssociatedControlType nvarchar(50),
  vcListItemType varchar(3),numListID numeric(9),vcLookBackTableName varchar(50),bitCustomField BIT,
  numFieldId NUMERIC,bitAllowSorting BIT,bitAllowEdit BIT,bitIsRequired BIT,bitIsEmail BIT,bitIsAlphaNumeric BIT,bitIsNumeric BIT,
  bitIsLengthValidation BIT,intMaxLength int,intMinLength int,bitFieldMessage BIT,vcFieldMessage VARCHAR(500),ListRelID numeric(9),intColumnWidth int,bitAllowFiltering BIT,vcFieldDataType CHAR(1)
)

set @strSql=@strSql+' select ADC.numContactId,DM.numDivisionID,DM.numTerID,ADC.numRecOwner,DM.tintCRMType,RowNumber'      


--Check if Master Form Configuration is created by administrator if NoColumns=0
DECLARE @IsMasterConfAvailable BIT = 0
DECLARE @numUserGroup NUMERIC(18,0)

IF @Nocolumns=0
BEGIN
	SELECT @numUserGroup = numGroupID FROM UserMaster WHERE numUserDetailId = @numUserCntID 

	IF (SELECT COUNT(*) FROM BizFormWizardMasterConfiguration WHERE numDomainID = @numDomainID AND numFormID = 34 AND numRelCntType=1 AND bitGridConfiguration = 1 AND numGroupID = @numUserGroup) > 0
	BEGIN
		SET @IsMasterConfAvailable = 1
	END
END


--If MasterConfiguration is available then load it otherwise load default columns
IF @IsMasterConfAvailable = 1
BEGIN
	INSERT INTO #tempForm
	SELECT 
		(intRowNum + 1) as tintOrder, vcDbColumnName,	vcOrigDbColumnName,	ISNULL(vcCultureFieldName,vcFieldName), vcAssociatedControlType,
		vcListItemType, numListID, vcLookBackTableName,	bitCustom, numFieldId, bitAllowSorting,	bitInlineEdit, bitIsRequired,
		bitIsEmail,	bitIsAlphaNumeric, bitIsNumeric, bitIsLengthValidation,	intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage vcFieldMessage,
		ListRelID,intColumnWidth,bitAllowFiltering,vcFieldDataType 
	FROM 
		View_DynamicColumnsMasterConfig
	WHERE
		View_DynamicColumnsMasterConfig.numFormId=34 AND 
		View_DynamicColumnsMasterConfig.numDomainID=@numDomainID AND 
		View_DynamicColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
		View_DynamicColumnsMasterConfig.numRelCntType=1 AND 
		View_DynamicColumnsMasterConfig.bitGridConfiguration = 1 AND
		ISNULL(View_DynamicColumnsMasterConfig.bitSettingField,0)=1 AND 
		ISNULL(View_DynamicColumnsMasterConfig.bitCustom,0)=0
	UNION
    SELECT 
		tintRow+1 as tintOrder,vcDbColumnName,vcDbColumnName,vcFieldName, vcAssociatedControlType,'' as vcListItemType,numListID,'',
		bitCustom,numFieldId,bitAllowSorting,bitAllowEdit,bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
		intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage,ListRelID,intColumnWidth,0,''
	FROM 
		View_DynamicCustomColumnsMasterConfig
	WHERE 
		View_DynamicCustomColumnsMasterConfig.numFormId=34 AND 
		View_DynamicCustomColumnsMasterConfig.numDomainID=@numDomainID AND 
		View_DynamicCustomColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
		View_DynamicCustomColumnsMasterConfig.numRelCntType=1 AND 
		View_DynamicCustomColumnsMasterConfig.bitGridConfiguration = 1 AND
		ISNULL(View_DynamicCustomColumnsMasterConfig.bitCustom,0)=1
	ORDER BY 
		tintOrder ASC  
END
ELSE                                        
BEGIN
	if @Nocolumns=0
	BEGIN
					INSERT INTO DycFormConfigurationDetails (numFormID,numFieldID,intColumnNum,intRowNum,numDomainID,numUserCntID,numRelCntType,tintPageType,bitCustom,intColumnWidth)
select 34,numFieldId,0,Row_number() over(order by tintRow desc),@numDomainID,@numUserCntID,1,1,0,intColumnWidth
 FROM View_DynamicDefaultColumns
 where numFormId=34 and bitDefault=1 AND ISNULL(bitSettingField,0)=1 and numDomainID=@numDomainID
order by tintOrder asc   

	END

	INSERT INTO #tempForm
select tintRow+1 as tintOrder,vcDbColumnName,vcOrigDbColumnName,ISNULL(vcCultureFieldName,vcFieldName),                  
 vcAssociatedControlType,vcListItemType,numListID,vcLookBackTableName                                                 
,bitCustom,numFieldId,bitAllowSorting,bitInlineEdit,
bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage vcFieldMessage,ListRelID,intColumnWidth,bitAllowFiltering,vcFieldDataType 
 FROM View_DynamicColumns 
 where numFormId=34 and numUserCntID=@numUserCntID and numDomainID=@numDomainID AND tintPageType=1 AND numRelCntType=1
  AND ISNULL(bitSettingField,0)=1 AND ISNULL(bitCustom,0)=0
       
 UNION
    
    select tintRow+1 as tintOrder,vcDbColumnName,vcDbColumnName,vcFieldName,                  
 vcAssociatedControlType,'' as vcListItemType,numListID,''                                                 
,bitCustom,numFieldId,bitAllowSorting,bitAllowEdit,
bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage,ListRelID,intColumnWidth,0,''
 from View_DynamicCustomColumns
 where numFormId=34 and numUserCntID=@numUserCntID and numDomainID=@numDomainID AND tintPageType=1 AND numRelCntType=1
 AND ISNULL(bitCustom,0)=1
 
  order by tintOrder asc  
END        
Declare @ListRelID as numeric(9)  
  select top 1 @tintOrder=tintOrder+1,@vcDbColumnName=vcDbColumnName,@vcFieldName=vcFieldName,                
  @vcAssociatedControlType=vcAssociatedControlType,@vcListItemType=vcListItemType,@numListID=numListID,@vcLookBackTableName=vcLookBackTableName                                               
,@bitCustomField=bitCustomField,@numFieldId=numFieldId,@bitAllowSorting=bitAllowSorting,@bitAllowEdit=bitAllowEdit,
@ListRelID=ListRelID
  from  #tempForm order by tintOrder asc            
 


while @tintOrder>0                                                
begin                                                
     
     if @bitCustomField = 0            
 begin             
     declare @Prefix as varchar(5)                  
     if @vcLookBackTableName = 'AdditionalContactsInformation'                  
   set @Prefix = 'ADC.'                  
     if @vcLookBackTableName = 'DivisionMaster'                  
   set @Prefix = 'DM.'       
   
          SET @vcColumnName= @vcDbColumnName+'~'+ convert(varchar(10),@numFieldId) +'~0'
                                         
 if @vcAssociatedControlType='SelectBox' or @vcAssociatedControlType='ListBox'                                                    
        begin                                                    
          
      
     if @vcListItemType='LI'                                                     
     begin                                                    
		 IF @numListID=40 
		 BEGIN
			
				SET @strSql = @strSql + ',L' + CONVERT(VARCHAR(3), @tintOrder) + '.vcData' + ' [' + @vcColumnName + ']'                                                   
                                            
				IF @vcDbColumnName='numShipCountry'
				BEGIN
					SET @WhereCondition = @WhereCondition
										+ ' left Join #tempAddressLead AD2 on AD2.numRecordId= DM.numDivisionID and AD2.tintAddressOf=2 and AD2.tintAddressType=2 AND AD2.bitIsPrimary=1 and AD2.numDomainID= DM.numDomainID '
										+ ' left Join ListDetails L' + CONVERT(VARCHAR(3), @tintOrder) + ' on L' + CONVERT(VARCHAR(3), @tintOrder) + '.numListItemID=AD2.numCountry '
				END
				ELSE
				BEGIN
					SET @WhereCondition = @WhereCondition
									+ ' left Join #tempAddressLead AD1 on AD1.numRecordId= DM.numDivisionID and AD1.tintAddressOf=2 and AD1.tintAddressType=1 AND AD1.bitIsPrimary=1 and AD1.numDomainID= DM.numDomainID '
									+ ' left Join ListDetails L' + CONVERT(VARCHAR(3), @tintOrder) + ' on L' + CONVERT(VARCHAR(3), @tintOrder) + '.numListItemID=AD1.numCountry '
				END


		 END
		 ELSE
		 BEGIN
			set @strSql=@strSql+',L'+ convert(varchar(3),@tintOrder)+'.vcData'+' ['+ @vcColumnName+']'                                                   
			set @WhereCondition= @WhereCondition +' left Join ListDetails L'+ convert(varchar(3),@tintOrder)+ ' on L'+convert(varchar(3),@tintOrder)+ '.numListItemID='+@vcDbColumnName		 	
		 END
     end                                                    
     else if @vcListItemType='S'                                                     
     begin        
			SET @strSql = @strSql + ',S' + CONVERT(VARCHAR(3), @tintOrder) + '.vcState' + ' [' + @vcColumnName + ']'                                                            
											
											IF @vcDbColumnName='numShipState'
											BEGIN
												SET @WhereCondition = @WhereCondition
													+ ' left Join #tempAddressLead AD3 on AD3.numRecordId= DM.numDivisionID and AD3.tintAddressOf=2 and AD3.tintAddressType=2 AND AD3.bitIsPrimary=1 and AD3.numDomainID= DM.numDomainID '
													+ ' left Join State S' + CONVERT(VARCHAR(3), @tintOrder) + ' on S' + CONVERT(VARCHAR(3), @tintOrder) + '.numStateID=AD3.numState '
											END
											ELSE IF @vcDbColumnName='numBillState'
											BEGIN
												SET @WhereCondition = @WhereCondition
													+ ' left Join #tempAddressLead AD4 on AD4.numRecordId= DM.numDivisionID and AD4.tintAddressOf=2 and AD4.tintAddressType=1 AND AD4.bitIsPrimary=1 and AD4.numDomainID= DM.numDomainID '
													+ ' left Join State S' + CONVERT(VARCHAR(3), @tintOrder) + ' on S' + CONVERT(VARCHAR(3), @tintOrder) + '.numStateID=AD4.numState '
											END
											ELSE  
											BEGIN
												SET @WhereCondition = @WhereCondition + ' left join State S' + CONVERT(VARCHAR(3), @tintOrder) + ' on S' + CONVERT(VARCHAR(3), @tintOrder) + '.numStateID=' + @vcDbColumnName        
											END		                                            
     end                                                    
     else if @vcListItemType='T'                                                     
     begin                                                    
      set @strSql=@strSql+',L'+ convert(varchar(3),@tintOrder)+'.vcData'+' ['+ @vcColumnName+']'                                                    
      set @WhereCondition= @WhereCondition +' left Join ListDetails L'+ convert(varchar(3),@tintOrder)+ ' on L'+convert(varchar(3),@tintOrder)+ '.numListItemID='+@vcDbColumnName                                                    
     end   
      else if @vcListItemType='C'                                                     
     begin                                                    
      set @strSql=@strSql+',C'+ convert(varchar(3),@tintOrder)+'.vcCampaignName'+' ['+ @vcColumnName+']'                                                    
      set @WhereCondition= @WhereCondition +' left Join CampaignMaster C'+ convert(varchar(3),@tintOrder)+ ' on C'+convert(varchar(3),@tintOrder)+ '.numCampaignId=DM.'+@vcDbColumnName                                                    
     end  
      else if @vcListItemType='AG'                                                     
     begin                                                    
      set @strSql=@strSql+',AG'+ convert(varchar(3),@tintOrder)+'.vcGrpName'+' ['+ @vcColumnName+']'                                                    
      set @WhereCondition= @WhereCondition +' left Join Groups AG'+ convert(varchar(3),@tintOrder)+ ' on AG'+convert(varchar(3),@tintOrder)+ '.numGrpId=DM.'+@vcDbColumnName                                                    
     end   
      else if @vcListItemType='DC'                                                     
     begin                                                    
      set @strSql=@strSql+',DC'+ convert(varchar(3),@tintOrder)+'.vcECampName'+' ['+ @vcColumnName+']'                                                    
      set @WhereCondition= @WhereCondition +' left Join ECampaign DC'+ convert(varchar(3),@tintOrder)+ ' on DC'+convert(varchar(3),@tintOrder)+ '.numECampaignID=ADC.'+@vcDbColumnName                                                    
     end                   
     else if   @vcListItemType='U'                                                 
    begin                   
    set @strSql=@strSql+',dbo.fn_GetContactName('+@Prefix+@vcDbColumnName+') ['+ @vcColumnName+']'                                                    
    end                  
    end                                                    
      else if @vcAssociatedControlType='DateField'                                                  
 begin            
       
     set @strSql=@strSql+',case when convert(varchar(11),DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+',' +@Prefix+@vcDbColumnName+') )= convert(varchar(11),getdate()) then ''<b><font color=red>Today</font></b>'''      
     set @strSql=@strSql+'when convert(varchar(11),DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+',' +@Prefix+@vcDbColumnName+') )= convert(varchar(11),dateadd(day,-1,getdate())) then ''<b><font color=purple>YesterDay</font></b>'''      
     set @strSql=@strSql+'when convert(varchar(11),DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+',' +@Prefix+@vcDbColumnName+') )= convert(varchar(11),dateadd(day,1,getdate())) then''<b><font color=orange>Tommorow</font></b>'' ' 
     set @strSql=@strSql+'else dbo.FormatedDateFromDate(DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+',' +@Prefix+@vcDbColumnName+'),'+convert(varchar(10),@numDomainId)+') end  ['+ @vcColumnName+']'
   end    
          
else if @vcAssociatedControlType='TextBox' OR @vcAssociatedControlType='Label'                                                 
begin    
 set @strSql=@strSql+','+ case  when @vcDbColumnName='numAge' then 'year(getutcdate())-year(bintDOB)' when    
  @vcDbColumnName='numDivisionID' then 'DM.numDivisionID' else @vcDbColumnName end+' ['+ @vcColumnName+']'              
       
 end 
 else if @vcAssociatedControlType='Popup' and @vcDbColumnName='numShareWith'                                                   
		begin      
			 set @strSql=@strSql+',(SELECT SUBSTRING(
	(SELECT '','' + A.vcFirstName+'' ''+A.vcLastName + CASE WHEN ISNULL(SR.numContactType,0)>0 THEN ''('' + dbo.fn_GetListItemName(SR.numContactType) + '')'' ELSE '''' END
	FROM ShareRecord SR JOIN UserMaster UM ON UM.numUserDetailId=SR.numAssignedTo 
							 JOIN AdditionalContactsInformation A ON UM.numUserDetailId=A.numContactID 
		 WHERE SR.numDomainID=DM.numDomainID AND SR.numModuleID=1 AND SR.numRecordID=DM.numDivisionID
				AND UM.numDomainID=DM.numDomainID and UM.numDomainID=A.numDomainID FOR XML PATH('''')),2,200000)) ['+ @vcColumnName+']'                
		 end       
 else                                               
begin    
	if @vcDbColumnName='vcCampaignAudit'
		set @strSql=@strSql+', dbo.GetContactCampignAuditDetail(ADC.numECampaignID,ADC.numContactId) ['+ @vcColumnName+']'  
	else
		set @strSql=@strSql+','+ @vcDbColumnName +' ['+ @vcColumnName+']'
 end          
                
end            
else if @bitCustomField = 1            
begin            
               
     

   SET @vcColumnName= @vcDbColumnName +'~'+ convert(varchar(10),@numFieldId)+'~1'
            
   print @vcAssociatedControlType   
   print @numFieldId           
   if @vcAssociatedControlType = 'TextBox' or @vcAssociatedControlType = 'TextArea'            
   begin            
               
    set @strSql= @strSql+',CFW'+ convert(varchar(3),@tintOrder)+'.Fld_Value  ['+ @vcColumnName +']'               
    set @WhereCondition= @WhereCondition +' left Join CFW_FLD_Values CFW'+ convert(varchar(3),@tintOrder)+ '             
    on CFW'+ convert(varchar(3),@tintOrder)+ '.Fld_Id='+convert(varchar(10),@numFieldId) +'and CFW'+ convert(varchar(3),@tintOrder)+ '.RecId=DM.numDivisionId   '                                                     
   end  
   else if @vcAssociatedControlType = 'CheckBox'         
   begin            
               
    --set @strSql= @strSql+',case when isnull(CFW'+ convert(varchar(3),@tintOrder)+'.Fld_Value,0)=0 then ''No'' when isnull(CFW'+ convert(varchar(3),@tintOrder)+'.Fld_Value,0)=1 then ''Yes'' end   ['+ @vcColumnName +']'              
    set @strSql= @strSql+',case when isnull(CFW' + CONVERT(VARCHAR(3), @tintOrder)+ '.Fld_Value,0)='''' then 0 else  isnull(CFW'
                                        + CONVERT(VARCHAR(3), @tintOrder)+ '.Fld_Value,0) end ['+ @vcColumnName + ']'
 
    set @WhereCondition= @WhereCondition +' left Join CFW_FLD_Values CFW'+ convert(varchar(3),@tintOrder)+ '             
    on CFW'+ convert(varchar(3),@tintOrder)+ '.Fld_Id='+convert(varchar(10),@numFieldId) +'and CFW'+ convert(varchar(3),@tintOrder)+ '.RecId=DM.numDivisionId   '                                                     
   end            
   else if @vcAssociatedControlType = 'DateField'        
   begin            
               
    set @strSql= @strSql+',dbo.FormatedDateFromDate(CFW'+ convert(varchar(3),@tintOrder)+'.Fld_Value,'+convert(varchar(10),@numDomainId)+')  ['+ @vcColumnName +']'              
    set @WhereCondition= @WhereCondition +' left Join CFW_FLD_Values CFW'+ convert(varchar(3),@tintOrder)+ '             
    on CFW'+ convert(varchar(3),@tintOrder)+ '.Fld_Id='+convert(varchar(10),@numFieldId) +'and CFW'+ convert(varchar(3),@tintOrder)+ '.RecId=DM.numDivisionId   '                                                     
   end            
   else if @vcAssociatedControlType = 'SelectBox'           
   begin            
    set @vcDbColumnName = 'DCust'+Convert(varchar(10),@numFieldId)            
    set @strSql=@strSql+',L'+ convert(varchar(3),@tintOrder)+'.vcData'+' ['+ @vcColumnName +']'                                                     
    set @WhereCondition= @WhereCondition +' left Join CFW_FLD_Values CFW'+ convert(varchar(3),@tintOrder)+ '             
     on CFW'+ convert(varchar(3),@tintOrder)+ '.Fld_Id='+convert(varchar(10),@numFieldId) +'and CFW'+ convert(varchar(3),@tintOrder)+ '.RecId=DM.numDivisionId    '                                                     
    set @WhereCondition= @WhereCondition +' left Join ListDetails L'+ convert(varchar(3),@tintOrder)+ ' on L'+convert(varchar(3),@tintOrder)+ '.numListItemID=CFW'+ convert(varchar(3),@tintOrder)+'.Fld_Value'            
   end   
               
end      
            
    
  select top 1 @tintOrder=tintOrder+1,@vcDbColumnName=vcDbColumnName,@vcFieldName=vcFieldName,                
  @vcAssociatedControlType=vcAssociatedControlType,@vcListItemType=vcListItemType,@numListID=numListID,@vcLookBackTableName=vcLookBackTableName                                               
,@bitCustomField=bitCustomField,@numFieldId=numFieldId,@bitAllowSorting=bitAllowSorting,@bitAllowEdit=bitAllowEdit,@ListRelID=ListRelID
  from #tempForm WHERE tintOrder > @tintOrder-1 order by tintOrder asc            
 
  if @@rowcount=0 set @tintOrder=0 
        
    
          
end                     
 
-------Change Row Color-------
Declare @vcCSOrigDbCOlumnName as varchar(50),@vcCSLookBackTableName as varchar(50),@vcCSAssociatedControlType as varchar(50)
SET @vcCSOrigDbCOlumnName=''
SET @vcCSLookBackTableName=''

--select top 1 @vcCSOrigDbCOlumnName=DFM.vcOrigDbCOlumnName,@vcCSLookBackTableName=DFM.vcLookBackTableName from DycFieldColorScheme DFCS join DycFieldMaster DFM on 
--DFCS.numModuleID=DFM.numModuleID and DFCS.numFieldID=DFM.numFieldID
--where DFCS.numDomainID=@numDomainID and DFM.numModuleID=1

Create table #tempColorScheme(vcOrigDbCOlumnName varchar(50),vcLookBackTableName varchar(50),vcFieldValue varchar(50),vcFieldValue1 varchar(50),vcColorScheme varchar(50),vcAssociatedControlType varchar(50))

insert into #tempColorScheme select DFM.vcOrigDbCOlumnName,DFM.vcLookBackTableName,DFCS.vcFieldValue,DFCS.vcFieldValue1,DFCS.vcColorScheme,DFFM.vcAssociatedControlType 
from DycFieldColorScheme DFCS join DycFormField_Mapping DFFM on DFCS.numFieldID=DFFM.numFieldID
join DycFieldMaster DFM on DFM.numModuleID=DFFM.numModuleID and DFM.numFieldID=DFFM.numFieldID
where DFCS.numDomainID=@numDomainID and DFFM.numFormID=34 AND DFCS.numFormID=34 and isnull(DFFM.bitAllowGridColor,0)=1

IF(SELECT COUNT(*) FROM #tempColorScheme)>0
BEGIN
   select top 1 @vcCSOrigDbCOlumnName=vcOrigDbCOlumnName,@vcCSLookBackTableName=vcLookBackTableName,@vcCSAssociatedControlType=vcAssociatedControlType from #tempColorScheme
END   
----------------------------                       

IF len(@vcCSOrigDbCOlumnName)>0 and len(@vcCSLookBackTableName)>0
BEGIN
set @strSql=@strSql+',tCS.vcColorScheme'
END

  set @strSql=@strSql+' ,TotalRowCount
 ,ISNULL(ADC.numECampaignID,0) as numECampaignID
 ,ISNULL( (SELECT MAX([numConEmailCampID]) FROM [ConECampaign] WHERE [numContactID] = ADC.numContactId AND [numECampaignID]= ADC.numECampaignID AND ISNULL(bitEngaged,0)=1),0) numConEmailCampID
 ,isnull((SELECT ISNULL(VIE.Total,0) FROM View_InboxEmail VIE  WITH (NOEXPAND) where VIE.numDomainID = @numDomain AND  VIE.numContactId = ADC.numContactId),0) as TotalEmail,
 isnull((SELECT ISNULL(VOA.OpenActionItemCount,0) AS TotalActionItem FROM VIEW_OPEN_ACTION_ITEMS VOA WITH (NOEXPAND)  where VOA.numDomainID = @numDomain AND  VOA.numDivisionId = T.numDivisionID ),0) as TotalActionItem
 FROM CompanyInfo cmp join divisionmaster DM on cmp.numCompanyID=DM.numCompanyID                                                        
   left join ListDetails lst on lst.numListItemID=DM.numFollowUpStatus                                                                               
   join AdditionalContactsInformation ADC on ADC.numDivisionID=DM.numDivisionID                                                
   join tblLeads T on T.numDivisionID=DM.numDivisionID   '+@WhereCondition

IF len(@vcCSOrigDbCOlumnName)>0 and len(@vcCSLookBackTableName)>0
BEGIN
	 if @vcCSLookBackTableName = 'AdditionalContactsInformation'                  
		set @Prefix = 'ADC.'                  
     if @vcCSLookBackTableName = 'DivisionMaster'                  
		set @Prefix = 'DM.'
	 if @vcCSLookBackTableName = 'CompanyInfo'                  
		set @Prefix = 'CMP.'   
 
	IF @vcCSAssociatedControlType='DateField'
	BEGIN
		--set @strSql=@strSql+' left join #tempColorScheme tCS on convert(varchar(11),DateAdd(day,tCS.vcFieldValue,GETUTCDATE()))=' + @Prefix + 'convert(varchar(11),DateAdd(day,tCS.vcFieldValue,' + @vcCSOrigDbCOlumnName + '))'
					set @strSql=@strSql+' left join #tempColorScheme tCS on CONVERT(VARCHAR(10),' + @Prefix + @vcCSOrigDbCOlumnName + ',111) >= CONVERT(VARCHAR(10),DateAdd(day,cast(tCS.vcFieldValue as int),GETUTCDATE()),111) 
			 and CONVERT(VARCHAR(10),' + @Prefix + @vcCSOrigDbCOlumnName + ',111) <= CONVERT(VARCHAR(10),DateAdd(day,cast(tCS.vcFieldValue1 as int),GETUTCDATE()),111)'

	END
	ELSE IF @vcCSAssociatedControlType='SelectBox' OR @vcCSAssociatedControlType='ListBox' OR @vcCSAssociatedControlType='CheckBox'
	BEGIN
		set @strSql=@strSql+' left join #tempColorScheme tCS on tCS.vcFieldValue=' + @Prefix + @vcCSOrigDbCOlumnName
	END
END

 set @strSql=@strSql+' WHERE                                               
   DM.tintCRMType=0 and ISNULL(ADC.bitPrimaryContact,0)=1 AND CMP.numDomainID=DM.numDomainID  and DM.numDomainID=ADC.numDomainID                                                  
   and RowNumber > '+convert(varchar(10),@firstRec)+ ' and RowNumber <'+ convert(varchar(10),@lastRec)+ ' order by RowNumber'
                                             
print @strSql                      
              
exec (@strSql)  

SELECT * FROM #tempForm

DROP TABLE #tempForm

drop table #tempColorScheme

IF OBJECT_ID('tempdb..#tempAddressLead') IS NOT NULL
BEGIN
    DROP TABLE #tempAddressLead
END
/****** Object:  StoredProcedure [dbo].[USP_GetMenuShortCut]    Script Date: 07/26/2008 16:17:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'usp_getmenushortcut' ) 
    DROP PROCEDURE usp_getmenushortcut
GO
CREATE PROCEDURE [dbo].[USP_GetMenuShortCut]
    @numGroupId AS NUMERIC,
    @numDomainId AS NUMERIC,
    @numcontactId AS NUMERIC
AS 
    IF ( SELECT COUNT(*)
         FROM   ShortCutUsrCnf
         WHERE  numGroupId = @numGroupId
                AND numDomainId = @numDomainId
                AND numContactID = @numContactID
                AND ISNULL(numTabId, 0) > 0
       ) > 0 
        BEGIN    
            SELECT  Hdr.id,
                    Hdr.vclinkname AS [Name],
                    Hdr.link,
                    Hdr.numTabId,
                    Hdr.vcImage,
                    ISNULL(Hdr.bitNew, 0) AS bitNew,
                    ISNULL(Hdr.bitNewPopup, 0) AS bitNewPopup,
                    Hdr.NewLink,
                    ISNULL(Hdr.bitPopup, 0) AS bitPopup,
                    sconf.numOrder,
                    ISNULL(sconf.bitFavourite, 0) AS bitFavourite
            FROM    ShortCutUsrcnf Sconf
                    JOIN ShortCutBar Hdr ON Hdr.id = Sconf.numLinkId
                                            AND Sconf.numTabId = Hdr.numTabId
            WHERE   sconf.numDomainId = @numDomainId
                    AND sconf.numGroupID = @numGroupId
                    AND sconf.numContactId = @numContactId
                    AND ISNULL(Sconf.tintLinkType, 0) = 0
            UNION
            SELECT  LD.numListItemID,
                    LD.vcData AS [Name],
                    '../prospects/frmCompanyList.aspx?RelId='
                    + CONVERT(VARCHAR(10), LD.numListItemID) AS link,
                    Sconf.numTabId,
                    NULL AS vcImage,
                    1 AS bitNew,
                    1 AS bitNewPopup,
                    '../include/frmAddOrganization.aspx?RelID='
                    + CONVERT(VARCHAR(10), LD.numListItemID) + '&FormID=36' AS NewLink,
                    0 AS bitPopup,
                    sconf.numOrder,
                    ISNULL(sconf.bitFavourite, 0) AS bitFavourite
            FROM    ShortCutUsrcnf Sconf
                    JOIN ListDetails LD ON LD.numListItemID = Sconf.numLinkId
            WHERE   LD.numListId = 5
                    AND LD.numListItemID <> 46
                    AND ( LD.numDomainID = @numDomainID
                          OR constflag = 1
                        )
                    AND sconf.numDomainId = @numDomainId
                    AND sconf.numGroupID = @numGroupID
                    AND sconf.numContactId = @numContactId
                    AND ISNULL(Sconf.tintLinkType, 0) = 5
            UNION ALL
            SELECT  LD.numListItemID,
                    LD.vcData AS [Name],
                    '../Opportunity/frmOpenBizDocs.aspx?BizDocID='
                    + CONVERT(VARCHAR(10), LD.numListItemID) AS link,
                    Sconf.numTabId,
                    NULL AS vcImage,
                    0 AS bitNew,
                    0 AS bitNewPopup,
                    '' AS NewLink,
                    0 AS bitPopup,
                    sconf.numOrder,
                    ISNULL(sconf.bitFavourite, 0) AS bitFavourite
            FROM    dbo.ListDetails LD
                    JOIN ShortCutUsrcnf Sconf ON LD.numListItemID = Sconf.numLinkId
            WHERE   numListID = 27
                    AND LD.numDomainID = @numDomainID
                    AND ISNULL(constFlag, 0) = 0
					AND Sconf.numContactID = @numContactID
--                    AND LD.numListItemID NOT IN (
--                    SELECT  numLinkId
--                    FROM    ShortCutUsrcnf
--                    WHERE   numDomainId = @numDomainId
--                            AND numContactId = @numContactId )
            UNION ALL
            SELECT  LD.numListItemID,
                    LD.vcData AS [Name],
                    '../Opportunity/frmOpenBizDocs.aspx?BizDocID='
                    + CONVERT(VARCHAR(10), LD.numListItemID) AS link,
                    Sconf.numTabId,
                    NULL AS vcImage,
                    0 AS bitNew,
                    0 AS bitNewPopup,
                    '' AS NewLink,
                    0 AS bitPopup,
                    sconf.numOrder,
                    ISNULL(sconf.bitFavourite, 0) AS bitFavourite
            FROM    dbo.ListDetails LD
                    JOIN ShortCutUsrcnf Sconf ON LD.numListItemID = Sconf.numLinkId
            WHERE   numListID = 27
                    AND LD.numDomainID = @numDomainID
                    AND ISNULL(constFlag, 0) = 1
--                    AND LD.numListItemID NOT IN (
--                    SELECT  numLinkId
--                    FROM    ShortCutUsrcnf
--                    WHERE   numDomainId = @numDomainId
--                            AND numContactId = @numContactId )
            ORDER BY sconf.numOrder       
        END    
    ELSE 
        BEGIN    
            IF ( SELECT COUNT(*)
                 FROM   ShortCutGrpConf
                 WHERE  numDomainId = @numDomainId
                        AND numGroupID = @numGroupId
                        AND ISNULL(numTabId, 0) > 0
               ) > 0 
                BEGIN    
                    SELECT  Hdr.id,
                            Hdr.vclinkname AS [Name],
                            Hdr.link,
                            Hdr.numTabId,
                            Hdr.vcImage,
                            ISNULL(Hdr.bitNew, 0) AS bitNew,
                            ISNULL(Hdr.bitNewPopup, 0) AS bitNewPopup,
                            Hdr.NewLink,
                            ISNULL(Hdr.bitPopup, 0) AS bitPopup,
                            sconf.numOrder,
                            0 AS bitFavourite
                    FROM    ShortCutGrpConf Sconf
                            JOIN ShortCutBar Hdr ON Hdr.id = Sconf.numLinkId
                                                    AND Sconf.numTabId = Hdr.numTabId
                    WHERE   Sconf.numGroupID = @numGroupId
                            AND Sconf.numDomainId = @numDomainId
                            AND Hdr.numContactId = 0
                            AND ISNULL(Sconf.tintLinkType, 0) = 0
                    UNION
                    SELECT  LD.numListItemID,
                            LD.vcData AS [Name],
                            '../Opportunity/frmOpenBizDocs.aspx?BizDocID=' AS link,
                            Sconf.numTabId,
                            NULL AS vcImage,
                            0 AS bitNew,
                            0 AS bitNewPopup,
                            '' AS NewLink,
                            0 AS bitPopup,
                            sconf.numOrder,
                            0 AS bitFavourite
                    FROM    ShortCutGrpConf Sconf
                            JOIN ListDetails LD ON LD.numListItemID = Sconf.numLinkId
                    WHERE   LD.numListId = 5
                            AND LD.numListItemID <> 46
                            AND ( LD.numDomainID = @numDomainID
                                  OR constflag = 1
                                )
                            AND Sconf.numGroupID = @numGroupId
                            AND sconf.numDomainId = @numDomainId
                            AND ISNULL(Sconf.tintLinkType, 0) = 5
                    UNION ALL
                    SELECT  LD.numListItemID,
                            LD.vcData AS [Name],
                            '../Opportunity/frmOpenBizDocs.aspx?BizDocID=' AS link,
                            Sconf.numTabId,
                            NULL AS vcImage,
                            0 AS bitNew,
                            0 AS bitNewPopup,
                            '' AS NewLink,
                            0 AS bitPopup,
                            sconf.numOrder,
                            ISNULL(sconf.bitFavourite, 0) AS bitFavourite
                    FROM    dbo.ListDetails LD
                            JOIN ShortCutUsrcnf Sconf ON LD.numListItemID = Sconf.numLinkId
                    WHERE   numListID = 27
                            AND LD.numDomainID = @numDomainID
                            AND ISNULL(constFlag, 0) = 0
--                    AND LD.numListItemID NOT IN (
--                    SELECT  numLinkId
--                    FROM    ShortCutUsrcnf
--                    WHERE   numDomainId = @numDomainId
--                            AND numContactId = @numContactId )
                    UNION ALL
                    SELECT  LD.numListItemID,
                            LD.vcData AS [Name],
                            '../Opportunity/frmOpenBizDocs.aspx?BizDocID='
                            + CONVERT(VARCHAR(10), LD.numListItemID) AS link,
                            Sconf.numTabId,
                            NULL AS vcImage,
                            0 AS bitNew,
                            0 AS bitNewPopup,
                            '' AS NewLink,
                            0 AS bitPopup,
                            sconf.numOrder,
                            ISNULL(sconf.bitFavourite, 0) AS bitFavourite
                    FROM    dbo.ListDetails LD
                            JOIN ShortCutUsrcnf Sconf ON LD.numListItemID = Sconf.numLinkId
                    WHERE   numListID = 27
                            AND LD.numDomainID = @numDomainID
                            AND ISNULL(constFlag, 0) = 1
--                    AND LD.numListItemID NOT IN (
--                    SELECT  numLinkId
--                    FROM    ShortCutUsrcnf
--                    WHERE   numDomainId = @numDomainId
--                            AND numContactId = @numContactId )
                    ORDER BY sconf.numOrder 
 
                END    
            ELSE 
                BEGIN    
                    SELECT  id,
                            vclinkname AS [Name],
                            link,
                            numTabId,
                            vcImage,
                            ISNULL(bitNew, 0) AS bitNew,
                            ISNULL(bitNewPopup, 0) AS bitNewPopup,
                            NewLink,
                            ISNULL(bitPopup, 0) AS bitPopup,
                            0 AS bitFavourite
                    FROM    ShortCutBar
                    WHERE   bitdefault = 1
                            AND numContactId = 0
                            AND ISNULL(numTabId, 0) > 0
                    UNION
                    SELECT  numListItemID AS id,
                            vcData AS [Name],
                            '../prospects/frmCompanyList.aspx?RelId='
                            + CONVERT(VARCHAR(10), numListItemID) AS link,
                            7 AS numTabId,
                            NULL AS vcImage,
                            0 AS bitNew,
                            1 AS bitNewPopup,
                            '../include/frmAddOrganization.aspx?RelID='
                            + CONVERT(VARCHAR(10), numListItemID)
                            + '&FormID=36' AS NewLink,
                            0 AS bitPopup,
                            0 AS bitFavourite
                    FROM    ListDetails
                    WHERE   numListId = 5
                            AND numListItemID <> 46
                            AND ( numDomainID = @numDomainID
                                  OR constflag = 1
                                )
                    SELECT  numListItemID AS id,
                            vcData AS [Name],
                            '../Opportunity/frmOpenBizDocs.aspx?BizDocID='
                            + CONVERT(VARCHAR(10), numListItemID) AS link,
                            7 AS numTabId,
                            NULL AS vcImage,
                            0 AS bitNew,
                            0 AS bitNewPopup,
                            '' AS NewLink,
                            0 AS bitPopup,
                            0 AS bitFavourite
                    FROM    dbo.ListDetails LD
                            JOIN ShortCutUsrcnf Sconf ON LD.numListItemID = Sconf.numLinkId
                    WHERE   numListID = 27
                            AND LD.numDomainID = @numDomainID
                            AND ISNULL(constFlag, 0) = 0
--                            AND LD.numListItemID NOT IN (
--                            SELECT  numLinkId
--                            FROM    ShortCutUsrcnf
--                            WHERE   numDomainId = @numDomainId
--                                    AND numContactId = @numContactId )
                    UNION ALL
                    SELECT  numListItemID AS id,
                            vcData AS [Name],
                            '../Opportunity/frmOpenBizDocs.aspx?BizDocID=' AS link,
                            7 AS numTabId,
                            NULL AS vcImage,
                            0 AS bitNew,
                            0 AS bitNewPopup,
                            '' AS NewLink,
                            0 AS bitPopup,
                            0 AS bitFavourite
                    FROM    dbo.ListDetails LD
                            JOIN ShortCutUsrcnf Sconf ON LD.numListItemID = Sconf.numLinkId
                    WHERE   numListID = 27
                            AND LD.numDomainID = @numDomainID
                            AND ISNULL(constFlag, 0) = 1
--                            AND LD.numListItemID NOT IN (
--                            SELECT  numLinkId
--                            FROM    ShortCutUsrcnf
--                            WHERE   numDomainId = @numDomainId
--                                    AND numContactId = @numContactId )                                        
                END    
    
        END    
 
GO

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetOppSerialLotNumber')
DROP PROCEDURE USP_GetOppSerialLotNumber
GO
CREATE PROCEDURE [dbo].[USP_GetOppSerialLotNumber]
	@numOppID AS NUMERIC(18,0),
	@numOppItemID AS NUMERIC(18,0),
	@numWarehouseItemID AS NUMERIC(18,0)
AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @numDomainID AS NUMERIC(18,0)
	SELECT @numDomainID= numDomainId FROM OpportunityMaster WHERE numOppId = @numOppID


	-- Available Serail Lot Numbers
	SELECT
		ROW_NUMBER() OVER(ORDER BY dExpirationDate DESC) AS OrderNo,
		numWareHouseItmsDTLID,
		vcSerialNo,
		numQty,
		dbo.FormatedDateFromDate(dExpirationDate,@numDomainID) AS dExpirationDate
	FROM
	(
		SELECT
			
			numWareHouseItmsDTLID,
			vcSerialNo,
			numQty - isnull((select sum(w.numQty) from OppWarehouseSerializedItem w INNER JOIN OpportunityMaster opp ON w.numOppId=opp.numOppId where ISNULL(opp.tintOppType,0) <> 2 AND ISNULL(w.bitTransferComplete,0) = 0 AND w.numWarehouseItmsDTLID=WareHouseItmsDTL.numWarehouseItmsDTLID),0) AS numQty,
			dExpirationDate
		FROM
			WareHouseItmsDTL
		WHERE
			numWareHouseItemID = @numWarehouseItemID AND
			ISNULL(numQty,0) > 0 
		
	) Temp
	WHERE
		Temp.numQty > 0
	ORDER BY
		dExpirationDate DESC

    -- Selected Serail Lot Numbers
	SELECT
		ROW_NUMBER() OVER(ORDER BY WareHouseItmsDTL.dExpirationDate DESC) AS OrderNo,
		WareHouseItmsDTL.numWareHouseItmsDTLID,
		WareHouseItmsDTL.vcSerialNo,
		dbo.FormatedDateFromDate(WareHouseItmsDTL.dExpirationDate,@numDomainID) AS dExpirationDate,
		OppWarehouseSerializedItem.numQty
	FROM
		OppWarehouseSerializedItem
	JOIN
		WareHouseItmsDTL
	ON
		OppWarehouseSerializedItem.numWarehouseItmsDTLID = WareHouseItmsDTL.numWareHouseItmsDTLID
	WHERE
		numOppID = @numOppID AND
		numOppItemID = @numOppItemID AND
		numWareHouseItemID = @numWarehouseItemID
	ORDER BY
		WareHouseItmsDTL.dExpirationDate DESC

END



GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_GetOrderItems' ) 
    DROP PROCEDURE USP_GetOrderItems
GO
CREATE PROCEDURE USP_GetOrderItems ( @tintMode TINYINT = 0,
                                     @numOppItemID NUMERIC(9) = 0,
                                     @numOppID NUMERIC(9) = 0,
                                     @numDomainId NUMERIC(9),
                                     @ClientTimeZoneOffset INT,
									 @vcOppItemIDs VARCHAR(2000)='' )
AS 
BEGIN
	
    IF @tintMode = 1 --Add Return grid data & For Recurring Get Original Units to split
        BEGIN
            SELECT  OI.numoppitemtCode,
                    OI.numItemCode,
                    OI.vcItemName,
                    OI.vcItemDesc,
                    OI.numUnitHour numOriginalUnitHour,
					CAST(( dbo.fn_UOMConversion(ISNULL(I.numBaseUnit, 0), OI.numItemCode,
                                    OM.numDomainId, ISNULL(OI.numUOMId, 0))
						* OI.numUnitHour ) AS NUMERIC(18, 2)) AS numUnitHour,
					OI.monPrice
            FROM    dbo.OpportunityMaster OM
                    INNER JOIN dbo.OpportunityItems OI ON OM.numOppId = OI.numOppId
                    INNER JOIN dbo.Item I ON I.numItemCode = OI.numItemCode
            WHERE   OM.numDomainId = @numDomainId
                    AND OM.numOppId = @numOppID
                    
        END
 
    IF @tintMode = 2 -- Get single item based on PK, or all items
		-- Used by Sales/Purchase Template Load items in New Sales Order
        BEGIN 
            SELECT  OI.numoppitemtCode,
                    OI.numOppId,
                    OI.numItemCode,
					OI.bitItemPriceApprovalRequired,
                    OI.numUnitHour numOriginalUnitHour,
                    OI.monPrice,
                    OI.monTotAmount,
                    OI.numSourceID,
                    OI.vcItemDesc,
                    OI.numWarehouseItmsID,
                    OI.vcType,
                    OI.vcAttributes,
                    OI.bitDropShip,
                    ISNULL(OI.numUnitHourReceived,0) AS numUnitHourReceived,
                    OI.bitDiscountType,
                    OI.fltDiscount,
                    OI.monTotAmtBefDiscount,
                    ISNULL(OI.numQtyShipped,0) AS numQtyShipped,
                    OI.vcItemName,
                    OI.vcModelID,
                    OI.vcPathForTImage,
                    OI.vcManufacturer,
                    OI.monVendorCost,
                    ISNULL(OI.numUOMId, 0) AS numUOMId,
                    ISNULL(OI.numUOMId, 0) numUOM, -- used from add edit order
                    ISNULL(U.vcUnitName, '') vcUOMName,
                    dbo.fn_UOMConversion(OI.numUOMId, OI.numItemCode,
                                         OM.numDomainId, NULL) AS UOMConversionFactor,
                    OI.bitWorkOrder,
                    isnull(OI.numVendorWareHouse,0) as numVendorWareHouse,
                    OI.numShipmentMethod,
                    OI.numSOVendorId,
                    OI.numProjectID,
                    OI.numClassID,
                    OI.numProjectStageID,
                    OI.numToWarehouseItemID,
                    WItems.numWarehouseID,
                    W.vcWareHouse AS Warehouse,
					CAST(( dbo.fn_UOMConversion(ISNULL(I.numBaseUnit, 0), OI.numItemCode,
                                    OM.numDomainId, ISNULL(OI.numUOMId, 0))
						* OI.numUnitHour ) AS NUMERIC(18, 2)) AS numUnitHour,
					isnull(OI.numClassID,0) as numClassID,
					isnull(OI.numProjectID,0) as numProjectID,
					dbo.fn_GetUOMName(I.numBaseUnit) AS vcBaseUOMName,
					OI.bitDropShip AS bitAllowDropShip,
					I.numVendorID,
					ISNULL(I.bitFreeShipping,0) AS [bitFreeShipping],
					ISNULL(I.txtItemDesc,'') txtItemDesc,
					ISNULL(I.vcModelID,'') vcModelID,
					ISNULL(I.vcManufacturer,'') vcManufacturer,
					ISNULL(I.numBarCodeId,'') numBarCodeId,
					ISNULL(I.bitSerialized,0) bitSerialized,
					ISNULL(I.bitKitParent,0) bitKitParent,
					ISNULL(I.bitAssembly,0) bitAssembly,
					ISNULL(I.IsArchieve,0) IsArchieve,
					ISNULL(I.bitLotNo,0) bitLotNo,
					ISNULL(WItems.monWListPrice,0) monWListPrice,
					ISNULL(I.bitTaxable, 0) bitTaxable,
                    I.charItemType,
                    I.bitKitParent,
					I.bitAssembly,
					I.bitSerialized,
					ISNULL(I.fltWeight,0) AS [fltWeight],
					ISNULL(I.fltLength,0) AS [fltLength],
					ISNULL(I.fltWidth,0) AS [fltWidth],
					ISNULL(I.fltHeight,0) AS [fltHeight],
					ISNULL(WItems.vcWHSKU,ISNULL(I.vcSKU,'')) AS [vcSKU],
					ISNULL(I.monAverageCost,0) AS monAverageCost,
					CASE WHEN I.charItemType = 'P' THEN (SELECT CAST(ISNULL(SUM(numOnHand),0) AS VARCHAR(10)) FROM WareHouseItems WHERE WareHouseItems.numItemID = OI.numItemCode) ELSE '' END AS numOnHand,
					CASE WHEN I.charItemType = 'P' THEN (SELECT CAST(ISNULL(SUM(numOnOrder),0) AS VARCHAR(10)) FROM WareHouseItems WHERE WareHouseItems.numItemID = OI.numItemCode) ELSE '' END AS numOnOrder,
					CASE WHEN I.charItemType = 'P' THEN (SELECT CAST(ISNULL(SUM(numAllocation),0) AS VARCHAR(10)) FROM WareHouseItems WHERE WareHouseItems.numItemID = OI.numItemCode) ELSE '' END AS numAllocation,
					CASE WHEN I.charItemType = 'P' THEN (SELECT CAST(ISNULL(SUM(numBackOrder),0) AS VARCHAR(10)) FROM WareHouseItems WHERE WareHouseItems.numItemID = OI.numItemCode) ELSE '' END AS numBackOrder
            FROM    dbo.OpportunityItems OI
                    INNER JOIN dbo.OpportunityMaster OM ON OM.numOppId = OI.numOppId
                    INNER JOIN dbo.Item I ON I.numItemCode = OI.numItemCode
                    LEFT JOIN UOM U ON U.numUOMId = OI.numUOMId
                    LEFT JOIN WareHouseItems WItems ON WItems.numWareHouseItemID = OI.numWarehouseItmsID
                    LEFT JOIN Warehouses W ON W.numWarehouseID = WItems.numWarehouseID
            WHERE   OI.numOppId = @numOppID
                    AND ( OI.numoppitemtCode = @numOppItemID
                          OR @numOppItemID = 0
                        )
                    AND OM.numDomainId = @numDomainId
                    
        END
 
    IF @tintMode = 3 -- Get item for SO to PO and vice versa creation
        BEGIN
            
            SELECT  OI.numoppitemtCode,
                    OI.numOppId,
					OI.bitItemPriceApprovalRequired,
                    OI.vcItemDesc,
                    ISNULL(OI.vcPathForTImage, '') vcPathForTImage,
                    ISNULL(OI.numWarehouseItmsID, 0) numWarehouseItmsID,
                    OI.vcItemName,
                    OI.vcModelID,
                    ISNULL(OI.vcAttributes, '') vcAttributes,
                    OI.vcType,
                    ISNULL(OI.bitDropShip, 0) bitDropShip,
                    OI.numItemCode,
                    OI.numUnitHour numOriginalUnitHour,
                    ISNULL(OI.numUOMId, 0) AS numUOM,
                    ISNULL(U.vcUnitName, '') vcUOMName,
                    dbo.fn_UOMConversion(OI.numUOMId, OI.numItemCode,
                                         OM.numDomainId, NULL) AS UOMConversionFactor,
                    ISNULL(I.numVendorID, 0) numVendorID,
                    ISNULL(V.monCost, 0) monVendorCost,
                    ISNULL(V.vcPartNo, '') vcPartNo,
                    OI.monPrice,
                    ISNULL(OI.numSOVendorId, 0) numSOVendorId,
                    ISNULL(I.charItemType, 'N') charItemType,
					CAST(( dbo.fn_UOMConversion(ISNULL(I.numBaseUnit, 0), OI.numItemCode,
                                    OM.numDomainId, ISNULL(OI.numUOMId, 0))
						* OI.numUnitHour ) AS NUMERIC(18, 2)) AS numUnitHour,
					isnull(OI.numClassID,0) as numClassID,isnull(OI.numProjectID,0) as numProjectID,ISNULL(I.fltWeight,0) AS [fltWeight],ISNULL(I.bitFreeShipping,0) AS [bitFreeShipping],
					ISNULL(I.fltLength,0) AS [fltLength],
					ISNULL(I.fltWidth,0) AS [fltWidth],
					ISNULL(I.fltHeight,0) AS [fltHeight],
					ISNULL((SELECT vcWHSKU FROM dbo.WareHouseItems WHERE numWarehouseItemID = OI.numWarehouseItmsID ),ISNULL(I.vcSKU,'')) AS [vcSKU]
            FROM    dbo.OpportunityItems OI
                    INNER JOIN dbo.OpportunityMaster OM ON OM.numOppId = OI.numOppId
                    INNER JOIN dbo.Item I ON I.numItemCode = OI.numItemCode
                    LEFT JOIN UOM U ON U.numUOMId = OI.numUOMId
                    LEFT JOIN Vendor V ON V.numVendorID = I.numVendorID
                                          AND I.numItemCode = V.numItemCode
            WHERE   OI.numOppId = @numOppID
                    AND OM.numDomainId = @numDomainId
            
        END
        IF @tintMode = 4 -- Add Edit Order items
        BEGIN 
            SELECT  OI.numoppitemtCode,
                    OI.numOppId,
                    OI.numItemCode,
					OI.bitItemPriceApprovalRequired,
                    OI.numUnitHour,
                    OI.monPrice,
                    OI.monTotAmount,
                    OI.numSourceID,
                    OI.vcItemDesc,
                    OI.numWarehouseItmsID,
                    OI.vcType ItemType,
                    OI.vcAttributes Attributes,
                    OI.bitDropShip DropShip,
                    OI.numUnitHourReceived,
                    OI.bitDiscountType,
                    OI.fltDiscount,
                    OI.monTotAmtBefDiscount,
                    OI.numQtyShipped,
                    OI.vcItemName,
                    OI.vcModelID,
                    OI.vcPathForTImage,
                    OI.vcManufacturer,
                    OI.monVendorCost,
                    ISNULL(OI.numUOMId, 0) AS numUOM,
                    ISNULL(U.vcUnitName, '') vcUOMName,
                    dbo.fn_UOMConversion(OI.numUOMId, OI.numItemCode,
                                         OM.numDomainId, NULL) AS UOMConversionFactor,
                    OI.bitWorkOrder,
                    isnull(OI.numVendorWareHouse,0) as numVendorWareHouse,
                    OI.numShipmentMethod,
                    OI.numSOVendorId,
                    OI.numProjectID,
                    OI.numClassID,
                    OI.numProjectStageID,
                    OI.numToWarehouseItemID,
                    ISNULL(I.bitTaxable, 0) bitTaxable,
                   UPPER(I.charItemType) charItemType,
                    WItems.numWarehouseID,
                    W.vcWareHouse AS Warehouse,
                    0 as Op_Flag,
					CAST(( dbo.fn_UOMConversion(ISNULL(I.numBaseUnit, 0), OI.numItemCode,
                                    OM.numDomainId, ISNULL(OI.numUOMId, 0))
						* OI.numUnitHour ) AS NUMERIC(18, 2)) AS numUnitHour,
					isnull(OI.numClassID,0) as numClassID,isnull(OI.numProjectID,0) as numProjectID,
					CASE WHEN (SELECT COUNT(*) FROM OpportunityBizDocs OB JOIN dbo.OpportunityBizDocItems OBI ON ob.numOppBizDocsId=obi.numOppBizDocID
					AND ob.bitAuthoritativeBizDocs=1 AND obi.numOppItemID=oi.numoppitemtCode AND ob.numOppId=OM.numOppId)>0 THEN 1 ELSE 0 END  AS bitIsAuthBizDoc,
					ISNULL(OI.numUnitHourReceived,0) AS numUnitHourReceived,ISNULL(numQtyShipped,0) AS numQtyShipped,dbo.fn_GetUOMName(I.numBaseUnit) AS vcBaseUOMName,ISNULL(I.fltWeight,0) AS [fltWeight],ISNULL(I.bitFreeShipping,0) AS [bitFreeShipping],
					ISNULL(I.fltLength,0) AS [fltLength],
					ISNULL(I.fltWidth,0) AS [fltWidth],
					ISNULL(I.fltHeight,0) AS [fltHeight],
					ISNULL(WItems.vcWHSKU,ISNULL(I.vcSKU,'')) AS [vcSKU],
					ISNULL(I.monAverageCost,0) AS monAverageCost,
					ISNULL(I.bitFreeShipping,0) AS [bitFreeShipping],
					ISNULL(I.txtItemDesc,'') txtItemDesc,
					ISNULL(I.vcModelID,'') vcModelID,
					ISNULL(I.vcManufacturer,'') vcManufacturer,
					ISNULL(I.numBarCodeId,'') numBarCodeId,
					ISNULL(I.bitSerialized,0) bitSerialized,
					ISNULL(I.bitKitParent,0) bitKitParent,
					ISNULL(I.bitAssembly,0) bitAssembly,
					ISNULL(I.IsArchieve,0) IsArchieve,
					ISNULL(I.bitLotNo,0) bitLotNo,
					ISNULL(WItems.monWListPrice,0) monWListPrice,
					ISNULL(I.bitTaxable, 0) bitTaxable,
					CASE WHEN I.charItemType = 'P' THEN (SELECT CAST(ISNULL(SUM(numOnHand),0) AS VARCHAR(10)) FROM WareHouseItems WHERE WareHouseItems.numItemID = OI.numItemCode) ELSE '' END AS numOnHand,
					CASE WHEN I.charItemType = 'P' THEN (SELECT CAST(ISNULL(SUM(numOnOrder),0) AS VARCHAR(10)) FROM WareHouseItems WHERE WareHouseItems.numItemID = OI.numItemCode) ELSE '' END AS numOnOrder,
					CASE WHEN I.charItemType = 'P' THEN (SELECT CAST(ISNULL(SUM(numAllocation),0) AS VARCHAR(10)) FROM WareHouseItems WHERE WareHouseItems.numItemID = OI.numItemCode) ELSE '' END AS numAllocation,
					CASE WHEN I.charItemType = 'P' THEN (SELECT CAST(ISNULL(SUM(numBackOrder),0) AS VARCHAR(10)) FROM WareHouseItems WHERE WareHouseItems.numItemID = OI.numItemCode) ELSE '' END AS numBackOrder,
					case when ISNULL(I.bitAssembly,0)=1 then dbo.fn_GetAssemblyPossibleWOQty(I.numItemCode) else 0 end AS numMaxWorkOrderQty
--					0 AS [Tax0],0 AS bitTaxable0
            FROM    dbo.OpportunityItems OI
                    INNER JOIN dbo.OpportunityMaster OM ON OM.numOppId = OI.numOppId
                    INNER JOIN dbo.Item I ON I.numItemCode = OI.numItemCode
                    LEFT JOIN UOM U ON U.numUOMId = OI.numUOMId
                    LEFT JOIN WareHouseItems WItems ON WItems.numWareHouseItemID = OI.numWarehouseItmsID
                    LEFT JOIN Warehouses W ON W.numWarehouseID = WItems.numWarehouseID
            WHERE   OI.numOppId = @numOppID
                    AND OM.numDomainId = @numDomainId
                    
		SELECT  vcSerialNo,
				vcComments AS Comments,
				numOppItemID AS numoppitemtCode,
				W.numWarehouseItmsDTLID,
				numWarehouseItmsID AS numWItmsID,
				dbo.Fn_getattributes(W.numWarehouseItmsDTLID, 1) Attributes
		FROM    WareHouseItmsDTL W
				JOIN OppWarehouseSerializedItem O ON O.numWarehouseItmsDTLID = W.numWareHouseItmsDTLID
		WHERE   numOppID = @numOppID                
                   
        END
        
        
		IF @tintMode = 5 -- Get WebApi Order Items to enter WebApi Order line Item Details 
		BEGIN 
			SELECT OI.numOppItemtCode,OI.numOppId,OI.numItemCode,
			ISNULL((SELECT vcWHSKU FROM dbo.WareHouseItems WHERE WareHouseItems.numWarehouseItemID = OI.numWarehouseItmsID),ISNULL(IT.vcSKU,'')) AS [vcSKU],
			OI.vcItemDesc,OMAPI.vcAPIOppId
			FROM OpportunityItems OI 
			INNER JOIN dbo.OpportunityMaster OM ON OI.numOppId = OM.numOppId 
			INNER JOIN dbo.OpportunityMasterAPI OMAPI ON OM.numOppId = OMAPI.numOppId
			INNER JOIN dbo.Item IT ON OI.numItemCode = IT.numItemCode
			WHERE OI.numOppId = @numOppID and OM.numDomainID = @numDomainId 
        END

END
/****** Object:  StoredProcedure [dbo].[USP_GetPageElementAttributes]    Script Date: 08/08/2009 16:15:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--EXEC [USP_GetPageElementAttributes] 3,35,10
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetPageElementAttributes')
DROP PROCEDURE USP_GetPageElementAttributes
GO
CREATE PROCEDURE [dbo].[USP_GetPageElementAttributes]
               @numSiteID    NUMERIC(9),
               @numElementID NUMERIC(9),
			   @numPageID NUMERIC(9)
AS
  BEGIN
  
  --SELECT PA.[numAttributeID],
  --             PA.numElementID,
  --             PA.[vcAttributeName],
		--	   CASE	WHEN  @numElementID = (SELECT numElementID 
		--				  FROM [dbo].[PageElementMaster] 
		--				  WHERE [vcTagName] = '{#BreadCrumb#}')
		--			THEN (SELECT [vcPageName] 
		--				  FROM [dbo].[SitePages] 
		--				  WHERE [numPageID] = @numPageID)
		--	   WHEN ISNULL(PA.bitEditor,0)=1 THEN [vcHtml] 
		--	   ELSE ISNULL(vcAttributeValue,'')  
		--	   END vcAttributeValue,
		--	   PA.[vcControlType],
		--	   PA.[vcControlValues],ISNULL(PA.bitEditor,0) bitEditor
  --      FROM   [PageElementAttributes] PA
		--LEFT OUTER JOIN PageElementDetail PED ON PED.numAttributeID = PA.numAttributeID
  --      WHERE PA.[numElementID] = @numElementID
  --      AND numSiteID = @numSiteID 
        
        SELECT PA.[numAttributeID],
               PA.numElementID,
               PA.[vcAttributeName],
			   CASE	WHEN  @numElementID = (SELECT numElementID 
						  FROM [dbo].[PageElementMaster] 
						  WHERE [vcTagName] = '{#BreadCrumb#}')
					THEN (SELECT [vcPageName] 
						  FROM [dbo].[SitePages] 
						  WHERE [numPageID] = @numPageID)
			   WHEN ISNULL(PA.bitEditor,0)=1 THEN (SELECT [vcHtml]
                       FROM   [PageElementDetail]
                       WHERE  numSiteID = @numSiteID
                              AND numElementID = @numElementID
                              AND numAttributeID = PA.numAttributeID)
			   ELSE ISNULL((SELECT [vcAttributeValue]
                       FROM   [PageElementDetail]
                       WHERE  numSiteID = @numSiteID
                              AND numElementID = @numElementID
                              AND numAttributeID = PA.numAttributeID),'') 
			   END vcAttributeValue,
			   PA.[vcControlType],
			   PA.[vcControlValues],ISNULL(PA.bitEditor,0) bitEditor
        FROM   [PageElementAttributes] PA
        --           LEFT OUTER JOIN [PageElementDetail] PD
        --             ON PA.[numAttributeID] = PD.[numAttributeID]
        WHERE PA.[numElementID] = @numElementID
      END


/****** Object:  StoredProcedure [dbo].[usp_GetRecentItems]    Script Date: 07/26/2008 16:18:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj  
--EXEC usp_GetRecentItems 1 
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getrecentitems')
DROP PROCEDURE usp_getrecentitems
GO
CREATE PROCEDURE [dbo].[usp_GetRecentItems]  
--declare    
 @numUserCntID as numeric(9)    
--set @numUserCntID=1     
as
IF @numUserCntID = 0
BEGIN
	--get blank table to avoid error
	select 0 numRecId 
	from communication    
	where  1=0
	RETURN
END
	

declare @v1 int
if exists(select  ISNULL([intLastViewedRecord],10) FROM [Domain] WHERE [numDomainId] IN (SELECT numDomainID FROM [AdditionalContactsInformation] WHERE [numContactId]=@numUserCntID))
	begin
		select @v1 = ISNULL([intLastViewedRecord],10) FROM [Domain] WHERE [numDomainId] IN (SELECT numDomainID FROM [AdditionalContactsInformation] WHERE [numContactId]=@numUserCntID)
	end
else
	begin
		set @v1=10
	end


set rowcount @v1;

DECLARE @TEMPTABLE TABLE
(
	numRecordId numeric(18,0),
	bintvisiteddate DATETIME,
	numRecentItemsID numeric(18,0),
	chrRecordType char(10)
)


INSERT INTO 
	@TEMPTABLE
SELECT DISTINCT
	numrecordId,
	bintvisiteddate,
	numRecentItemsID,
	chrRecordType	
FROM    
	RECENTITEMS
WHERE   
	numUserCntId = @numUserCntID
	AND bitdeleted = 0
ORDER BY 
	bintvisiteddate DESC
set rowcount 0

SELECT  DM.numDivisionID AS numRecID ,isnull(case when len(CMP.vcCompanyName)>20 then convert(varchar(20),CMP.vcCompanyName)+'..' when len(CMP.vcCompanyName)<=20 then CMP.vcCompanyName end,'' ) as RecName,                           
     DM.tintCRMType,                             
     CMP.numCompanyID AS numCompanyID,                                           
     ADC.numContactID AS numContactID,          
     DM.numDivisionID,'C'   as Type,bintvisiteddate  ,0 as caseId,0 as caseTimeId,0 as caseExpId,
	 0 as numOppID   
    FROM  CompanyInfo CMP                            
    join DivisionMaster DM on DM.numCompanyID=CMP.numCompanyID                  
    join AdditionalContactsInformation ADC on ADC.numDivisionID=DM.numDivisionID          
    join @TEMPTABLE R on R.numRecordID=DM.numDivisionID                                             
    WHERE ISNULL(ADC.bitPrimaryContact,0)=1 and chrRecordType='C'    
union     
         
SELECT  DM.numDivisionID AS numRecID,    
isnull(case when len(vcFirstName + ' '+vcLastName)>20 then convert(varchar(20),ISNULL(vcFirstName,'-') + ' '+ISNULL(vcLastName,'-'))+'..'    
 when len(ISNULL(vcFirstName,'-') + ' '+ISNULL(vcLastName,'-'))<=20 then ISNULL(vcFirstName,'-') + ' '+ISNULL(vcLastName,'-') end,'') as RecName,                     
     DM.tintCRMType,                             
     CMP.numCompanyID AS numCompanyID,                                           
     ADC.numContactID AS numContactID,          
     DM.numDivisionID,'U',bintvisiteddate     ,0 as caseId,0 as caseTimeId,0 as caseExpId,
	 0 as numOppID                     
    FROM  CompanyInfo CMP                            
    join DivisionMaster DM on DM.numCompanyID=CMP.numCompanyID                  
    join AdditionalContactsInformation ADC on ADC.numDivisionID=DM.numDivisionID          
    join @TEMPTABLE R on numRecordID=ADC.numContactID   where chrRecordType='U'                                         
    
    
union           
    
select  numOppId as numRecID,isnull(case when len(vcPOppName)>20 then convert(varchar(20),vcPOppName)+'..' when len(vcPOppName)<=20 then vcPOppName end,'') as RecName,          
0,0,0,numDivisionID,'O',bintvisiteddate  ,0 as caseId,0 as caseTimeId,0 as caseExpId,
0 as numOppID from OpportunityMaster          
join @TEMPTABLE  on numRecordID=numOppId    where  chrRecordType='O'      
    
          
union       
        
select  numProId as numRecID,isnull(case when len(vcProjectName)>20 then convert(varchar(20),vcProjectName)+'..' when len(vcProjectName)<=20 then vcProjectName end,'') as RecName,          
0,0,0,numDivisionID,'P',bintvisiteddate  ,0 as caseId,0 as caseTimeId,0 as caseExpId,0 as numOppID from ProjectsMaster          
join @TEMPTABLE  on numRecordID=numProId  where  chrRecordType='P'    
       
    
union    
           
select  numCaseId as numRecID,    
isnull(case when len(vcCaseNumber)>20 then convert(varchar(20),vcCaseNumber)+'..' when len(vcCaseNumber)<=20 then vcCaseNumber end,'')  as RecName,          
0,0,0,numDivisionID,'S',bintvisiteddate ,0 as caseId,0 as caseTimeId,0 as caseExpId,0 as numOppID  from Cases          
join @TEMPTABLE  on numRecordID=numCaseId   where  chrRecordType='S'      
    
union    
    
select numCommId as numRecId,    
isnull(convert(varchar(20),    
case when bitTask =971 then 'C - '    
  when bitTask =972 then 'T - '    
  when bitTask =973 then 'N - '    
  when bitTask =974 then 'F - '   
 else convert(varchar(2),dbo.fn_GetListItemName(bitTask))  
end + convert(varchar(20),textDetails)),'')    
as RecName,0,0,0,numDivisionID,'A',bintvisiteddate     
 , caseId, caseTimeId, caseExpId,0 as numOppID    
from communication    
join @TEMPTABLE  on numRecordID=numCommId   where  chrRecordType='A'    
    
    
    union    
           
--select  numItemCode as numRecID,    
--vcItemName + '~' + case when charItemType='P' then 'Inventory Item' 
--when charItemType='N' then 'Non Inventory Item' 
--when charItemType='S' then 'Service' 
--when charItemType='A' then 'Accessory' end as RecName,          
--0,0,0,0,'I',bintvisiteddate ,0 as caseId,0 as caseTimeId,0 as caseExpId  from Item          
--join #tblVisitors  on numRecordID=numItemCode   where  chrRecordType='I'  
--
--  union    
--           
--select  numItemCode as numRecID,    
--vcItemName + '~' + case when charItemType='P' then 'Inventory Item' 
--when charItemType='N' then 'Non Inventory Item' 
--when charItemType='S' then 'Service' 
--when charItemType='A' then 'Accessory' end as RecName,          
--0,0,0,0,'AI',bintvisiteddate ,0 as caseId,0 as caseTimeId,0 as caseExpId  from Item          
--join #tblVisitors  on numRecordID=numItemCode   where  chrRecordType='AI'  

select  numItemCode as numRecID,    
isnull(case when len(vcItemName)>20 then convert(varchar(20),vcItemName)+'..'    
 when len(vcItemName)<=20 then vcItemName end,'') as RecName, 
0,0,0,0,'I',bintvisiteddate ,0 as caseId,0 as caseTimeId,0 as caseExpId,0 as numOppID  from Item          
join @TEMPTABLE  on numRecordID=numItemCode   where  chrRecordType='I'  

  union    
           
select  numItemCode as numRecID,    
isnull(case when len(vcItemName)>20 then convert(varchar(20),vcItemName)+'..'    
 when len(vcItemName)<=20 then vcItemName end,'') as RecName, 
0,0,0,0,'AI',bintvisiteddate ,0 as caseId,0 as caseTimeId,0 as caseExpId,0 as numOppID  from Item          
join @TEMPTABLE  on numRecordID=numItemCode   where  chrRecordType='AI'  
                                    
union    
           
select  numGenericDocId as numRecID,    
isnull(case when len(vcDocName)>20 then convert(varchar(20),vcDocName)+'..' when len(vcDocName)<=20 then vcDocName end,'')  as RecName,          
0,0,0,0,'D',bintvisiteddate ,0 as caseId,0 as caseTimeId,0 as caseExpId,0 as numOppID  from GenericDocuments          
join @TEMPTABLE  on numRecordID=numGenericDocId   where  chrRecordType='D'      

union 

select  numCampaignId as numRecID,isnull(case when len(vcCampaignName)>20 then convert(varchar(20),vcCampaignName)+'..' when len(vcCampaignName)<=20 then vcCampaignName end,'') as RecName,          
0,0,0,0 AS numDivisionID,'M',bintvisiteddate  ,0 as caseId,0 as caseTimeId,0 as caseExpId,0 as numOppID from dbo.CampaignMaster        
join @TEMPTABLE  on numRecordID=numCampaignId    where  chrRecordType='M'      
    
union 

select  numReturnHeaderID as numRecID,isnull(case when len(vcRMA)>20 then convert(varchar(20),vcRMA)+'..' when len(vcRMA)<=20 then vcRMA end,'') as RecName,          
0,0,0,numDivisionID,'R',bintvisiteddate  ,0 as caseId,0 as caseTimeId,0 as caseExpId,0 as numOppID from ReturnHeader          
join @TEMPTABLE  on numRecordID=numReturnHeaderID  where  chrRecordType='R'   

UNION

SELECT
	numOppBizDocsId as numRecID,
	isnull(case when len(vcBizDocID)>20 then convert(varchar(20),vcBizDocID)+'..' when len(vcBizDocID)<=20 then vcBizDocID end,'') as RecName,
	0,0,0,0 AS numDivisionID,'B',bintvisiteddate,0 as caseId,0 as caseTimeId,0 as caseExpId,numOppId as numOppID
FROM
	OpportunityBizDocs
JOIN
	@TEMPTABLE
ON
	numOppBizDocsId = numRecordId
WHERE
	chrRecordType = 'B'

order by bintVisitedDate desc     


--,Y.numRecentItemsID ORDER BY Y.numRecentItemsID DESC     
--,Y.bintVisitedDate    
--

GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_GetTreeNodesForRelationship' ) 
    DROP PROCEDURE USP_GetTreeNodesForRelationship
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- EXEC USP_GetPageNavigationAuthorizationDetails 1,1
CREATE PROCEDURE USP_GetTreeNodesForRelationship
    (
	  @numGroupID NUMERIC(18,0),
	  @numTabID NUMERIC(18,0),
      @numDomainID NUMERIC(18, 0),
	  @bitVisibleOnly bit
    )
AS 
    SET NOCOUNT ON
    SET TRANSACTION ISOLATION LEVEL READ COMMITTED
  
    BEGIN		
        DECLARE @numModuleID AS NUMERIC(18, 0)

        SELECT TOP 1
                @numModuleID = numModuleID
        FROM    dbo.PageNavigationDTL
        WHERE   numTabID = @numTabID 
 

SELECT
	ROW_NUMBER() OVER (ORDER BY numOrder) AS ID,
	*
INTO
	#RelationshipTree 
FROM
	(
	SELECT    
		PND.numPageNavID AS numPageNavID,
		NULL AS numListItemID,
		ISNULL(TNA.bitVisible,1) AS bitVisible,
		ISNULL(PND.vcPageNavName, '') AS [vcNodeName],
		ISNULL(vcNavURL, '') AS vcNavURL,
        vcImageURL,
		ISNULL(TNA.tintType,1) AS tintType,
		PND.numParentID AS numParentID,
		PND.numParentID AS numOriginalParentID,
		ISNULL(TreeNodeOrder.numOrder,1000) AS numOrder,
		0 AS isUpdated
	FROM      
		PageNavigationDTL PND
	LEFT JOIN 
		dbo.TreeNavigationAuthorization TNA 
	ON 
		TNA.numPageNavID = PND.numPageNavID AND
		TNA.numDomainID = @numDomainID AND 
		TNA.numGroupID = @numGroupID
	LEFT JOIN
		dbo.TreeNodeOrder 
	ON
		dbo.TreeNodeOrder.numTabID = @numTabID AND
		dbo.TreeNodeOrder.numDomainID = @numDomainID AND
		dbo.TreeNodeOrder.numGroupID = @numGroupID AND
		dbo.TreeNodeOrder.numPageNavID = PND.numPageNavID
	WHERE     
		1 = 1
		AND ISNULL(PND.bitVisible, 0) = 1
		AND numModuleID = @numModuleID
	UNION
	SELECT
		NULL AS numPageNavID,
		ListDetails.numListItemID,
		ISNULL(dbo.TreeNodeOrder.bitVisible,1) AS bitVisible,
		ListDetails.vcData AS [vcNodeName],
		'../prospects/frmCompanyList.aspx?RelId='+ convert(varchar(10),ListDetails.numListItemID) vcNavURL,
		NULL as vcImageURL,
		1 AS tintType,
		6 AS numParentID,
		6 AS numOriginalParentID,
		ISNULL(TreeNodeOrder.numOrder,2000) AS numOrder,
		0 AS isUpdated
	FROM
		ListDetails
	LEFT JOIN 
		dbo.TreeNodeOrder 
	ON
		dbo.TreeNodeOrder.numListItemID = ListDetails.numListItemID AND
		dbo.TreeNodeOrder.numParentID = 6 AND
		dbo.TreeNodeOrder.numTabID = @numTabID AND
		dbo.TreeNodeOrder.numDomainID = @numDomainID AND
		dbo.TreeNodeOrder.numGroupID = @numGroupID AND
		dbo.TreeNodeOrder.numPageNavID IS NULL
	WHERE
		numListID = 5 AND
		ListDetails.numDomainID = @numDomainID AND
		(ISNULL(bitDelete,0) = 0 OR constFlag = 1) AND
		ListDetails.numListItemID <> 46 
	UNION       
	SELECT 
		NULL AS numPageNavID,
		L2.numListItemID,   
		ISNULL(dbo.TreeNodeOrder.bitVisible,1) AS bitVisible,
		L2.vcData AS [vcNodeName],
		'../prospects/frmCompanyList.aspx?RelId=' 
		+ convert(varchar(10), FRDTL.numPrimaryListItemID) + '&profileid='
        + convert(varchar(10), numSecondaryListItemID) as vcNavURL,
        NULL as vcImageURL,
		1 AS tintType,
		FRDTL.numPrimaryListItemID AS numParentID,
		FRDTL.numPrimaryListItemID AS numOriginalParentID,
		ISNULL(TreeNodeOrder.numOrder,3000) AS numOrder,
		0 AS isUpdated
	FROM      
		FieldRelationship FR
    join 
		FieldRelationshipDTL FRDTL on FRDTL.numFieldRelID = FR.numFieldRelID
    join 
		ListDetails L1 on numPrimaryListItemID = L1.numListItemID
    join 
		ListDetails L2 on numSecondaryListItemID = L2.numListItemID
	LEFT JOIN 
		dbo.TreeNodeOrder 
	ON
		dbo.TreeNodeOrder.numListItemID = L2.numListItemID AND
		dbo.TreeNodeOrder.numParentID = FRDTL.numPrimaryListItemID AND
		dbo.TreeNodeOrder.numTabID = @numTabID AND
		dbo.TreeNodeOrder.numDomainID = @numDomainID AND
		dbo.TreeNodeOrder.numGroupID = @numGroupID AND
		dbo.TreeNodeOrder.numPageNavID IS NULL
	WHERE     
		numPrimaryListItemID <> 46
        and FR.numPrimaryListID = 5
		and FR.numSecondaryListID = 21
		and FR.numDomainID = @numDomainID
        and L2.numDomainID = @numDomainID
	UNION
	SELECT 
		NULL AS numPageNavID,
		L2.numListItemID,
		ISNULL(dbo.TreeNodeOrder.bitVisible,1) AS bitVisible,
		L2.vcData AS [vcNodeName],
		'../prospects/frmProspectList.aspx?numProfile=' + CONVERT(VARCHAR(10), L2.numListItemID) AS vcNavURL,
        NULL AS vcImageURL,
		1 AS tintType,
		11 AS numParentID,
		11 AS numOriginalParentID,
		ISNULL(TreeNodeOrder.numOrder,4000) AS numOrder,
		0 AS isUpdated
	FROM    
		FieldRelationship AS FR
	INNER JOIN 
		FieldRelationshipDTL AS FRD ON FRD.numFieldRelID = FR.numFieldRelID
	INNER JOIN 
		ListDetails AS L1 ON L1.numListItemID = FRD.numPrimaryListItemID
	INNER JOIN 
		ListDetails AS L2 ON L2.numListItemID = FRD.numSecondaryListItemID
	LEFT JOIN 
		dbo.TreeNodeOrder 
	ON
		dbo.TreeNodeOrder.numListItemID = L2.numListItemID AND
		dbo.TreeNodeOrder.numParentID = 11 AND
		dbo.TreeNodeOrder.numTabID = @numTabID AND
		dbo.TreeNodeOrder.numDomainID = @numDomainID AND
		dbo.TreeNodeOrder.numGroupID = @numGroupID AND
		dbo.TreeNodeOrder.numPageNavID IS NULL
	WHERE   ( FRD.numPrimaryListItemID = 46 )
			AND ( FR.numDomainID = @numDomainID )
	UNION
	SELECT 
		NULL AS numPageNavID,
		L2.numListItemID,
		ISNULL(dbo.TreeNodeOrder.bitVisible,1) AS bitVisible,
		L2.vcData AS [vcNodeName],
		'../account/frmAccountList.aspx?numProfile=' + CONVERT(VARCHAR(10), L2.numListItemID) AS vcNavURL,
        NULL AS vcImageURL,
		1 AS tintType,
		12 AS numParentID,
		12 AS numOriginalParentID,
		ISNULL(TreeNodeOrder.numOrder,5000) AS numOrder,
		0 AS isUpdated
	FROM    
		FieldRelationship AS FR
	INNER JOIN 
		FieldRelationshipDTL AS FRD ON FRD.numFieldRelID = FR.numFieldRelID
	INNER JOIN 
		ListDetails AS L1 ON L1.numListItemID = FRD.numPrimaryListItemID
	INNER JOIN 
		ListDetails AS L2 ON L2.numListItemID = FRD.numSecondaryListItemID
	LEFT JOIN 
		dbo.TreeNodeOrder 
	ON
		dbo.TreeNodeOrder.numListItemID = L2.numListItemID AND
		dbo.TreeNodeOrder.numParentID = 12 AND
		dbo.TreeNodeOrder.numTabID = @numTabID AND
		dbo.TreeNodeOrder.numDomainID = @numDomainID AND
		dbo.TreeNodeOrder.numGroupID = @numGroupID AND
		dbo.TreeNodeOrder.numPageNavID IS NULL
	WHERE   ( FRD.numPrimaryListItemID = 46 )
			AND ( FR.numDomainID = @numDomainID )
	UNION    
	--Select Contact
	SELECT 
		NULL AS numPageNavID,
		listdetails.numListItemID,
		ISNULL(dbo.TreeNodeOrder.bitVisible,1) AS bitVisible,
		listdetails.vcData AS [vcNodeName],
		'../contact/frmContactList.aspx?ContactType=' + CONVERT(VARCHAR(10), listdetails.numListItemID) AS vcNavURL,
        NULL AS vcImageURL,
		1 AS tintType,
		13 AS numParentID,
		13 AS numOriginalParentID,
		ISNULL(TreeNodeOrder.numOrder,6000) AS numOrder,
		0 AS isUpdated
	FROM    
		listdetails
	LEFT JOIN 
		dbo.TreeNodeOrder 
	ON
		dbo.TreeNodeOrder.numListItemID = listdetails.numListItemID AND
		dbo.TreeNodeOrder.numParentID = 13 AND
		dbo.TreeNodeOrder.numTabID = @numTabID AND
		dbo.TreeNodeOrder.numDomainID = @numDomainID AND
		dbo.TreeNodeOrder.numGroupID = @numGroupID AND
		dbo.TreeNodeOrder.numPageNavID IS NULL
	WHERE   numListID = 8
			AND ( constFlag = 1
				  OR listdetails.numDomainID = @numDomainID
				)
	UNION
	SELECT 
		NULL AS numPageNavID,
		101 AS numListItemID,
		ISNULL((
					SELECT 
						bitVisible 
					FROM 
						dbo.TreeNodeOrder 
					WHERE
						dbo.TreeNodeOrder.numListItemID = 101 AND
						dbo.TreeNodeOrder.numParentID = 13 AND
						dbo.TreeNodeOrder.numTabID = @numTabID AND
						dbo.TreeNodeOrder.numDomainID = @numDomainID AND
						dbo.TreeNodeOrder.numGroupID = @numGroupID AND
						dbo.TreeNodeOrder.numPageNavID IS NULL
				),1) AS bitVisible,
		'Primary Contact' AS [vcNodeName],
		'../Contact/frmcontactList.aspx?ContactType=101',
        NULL AS vcImageURL,
		1 AS tintType,
		13 AS numParentID,
		13 AS numOriginalParentID,
		ISNULL((
					SELECT 
						numOrder 
					FROM 
						dbo.TreeNodeOrder 
					WHERE
						dbo.TreeNodeOrder.numListItemID = 101 AND
						dbo.TreeNodeOrder.numParentID = 13 AND
						dbo.TreeNodeOrder.numTabID = @numTabID AND
						dbo.TreeNodeOrder.numDomainID = @numDomainID AND
						dbo.TreeNodeOrder.numGroupID = @numGroupID AND
						dbo.TreeNodeOrder.numPageNavID IS NULL
				),7000) AS numOrder,
		 0 AS isUpdated
	) AS TEMP
	ORDER BY
		TEMP.numOrder

	DECLARE @i AS INT = 1
	DECLARE @Count AS INT
	DECLARE @oldParentID AS INT
	SELECT @COUNT=COUNT(ID) FROM #RelationshipTree

	WHILE @i <= @COUNT
	BEGIN
		IF (SELECT ISNULL(numPageNavID,0) FROM #RelationshipTree WHERE ID = @i) <> 0
			SELECT @oldParentID = numPageNavID FROM #RelationshipTree WHERE ID = @i
		ELSE
			SELECT @oldParentID = numListItemID FROM #RelationshipTree WHERE ID = @i


		UPDATE #RelationshipTree SET numParentID = @i, isUpdated=1 WHERE numParentID = @oldParentID AND isUpdated = 0

		SET @i = @i + 1
	END


	IF @bitVisibleOnly = 1
		SELECT 
			CAST(ID AS INT) As ID,
			numPageNavID,
			numListItemID,
			bitVisible,
			vcNodeName, 
			vcNavURL, 
			vcImageURL,
			tintType,
			(CASE numParentID WHEN 0 THEN NULL ELSE CAST(numParentID AS INT) END) AS numParentID,
			numOriginalParentID, 
			numOrder 
		FROM
			#RelationshipTree 
		WHERE 
			bitVisible = 1
			AND (numParentID IN (SELECT ID FROM #RelationshipTree WHERE bitVisible = 1) OR numParentID = 0)
	ELSE
		SELECT CAST(ID AS INT) As ID,numPageNavID,numListItemID,bitVisible,vcNodeName,tintType,(CASE numParentID WHEN 0 THEN NULL ELSE CAST(numParentID AS INT) END) AS numParentID,numOriginalParentID, numOrder FROM #RelationshipTree WHERE numParentID IN (SELECT ID FROM #RelationshipTree) OR ISNULL(numParentID,0) = 0
	
	DROP TABLE #RelationshipTree
      
    END
/****** Object:  StoredProcedure [dbo].[USP_GetVendors]    Script Date: 07/26/2008 16:18:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getvendors')
DROP PROCEDURE usp_getvendors
GO
CREATE PROCEDURE [dbo].[USP_GetVendors]         
@numDomainID as numeric(9)=0,    
@numItemCode as numeric(9)=0       
as        
select div.numCompanyID,Vendor.numItemCode,Vendor.numVendorID,vcPartNo,vcCompanyName as Vendor,        
vcFirstName+ ' '+vcLastName as ConName,        
convert(varchar(15),numPhone)+ ', '+ convert(varchar(15),numPhoneExtension) as Phone,ISNULL(monCost,0) monCost,vcItemName,ISNULL([intMinQty],0) intMinQty
,numContactId, intLeadTimeDays,
(CASE WHEN (SELECT COUNT(*) FROM ItemDetails INNER JOIN Item ON numItemKitID=Item.numItemCode WHERE Item.bitAssembly=1 AND numChildItemID = I.numItemCode) > 0 THEN 1 ELSE 0 END) AS bitUsedInAssembly
from Vendor          
join divisionMaster div        
on div.numdivisionid=numVendorid        
join companyInfo com        
on com.numCompanyID=div.numCompanyID        
left join AdditionalContactsInformation ADC        
on ADC.numdivisionID=Div.numdivisionID   
join Item I  
on I.numItemCode= Vendor.numItemCode
WHERE ISNULL(ADC.bitPrimaryContact,0)=1 and Vendor.numDomainID=@numDomainID and Vendor.numItemCode= @numItemCode
GO
/****** Object:  StoredProcedure [dbo].[USP_GetWorkOrder]    Script Date: 07/26/2008 16:16:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
                    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetWorkOrder')
DROP PROCEDURE USP_GetWorkOrder
GO
CREATE PROCEDURE [dbo].[USP_GetWorkOrder]                             
@numDomainID as numeric(9),
@ClientTimeZoneOffset  INT,
@numWOStatus as numeric(9),
@SortExpression AS VARCHAR(50),
@CurrentPage int,                                                                            
@PageSize INT,
@numUserCntID AS NUMERIC(9),
@vcWOId  AS VARCHAR(4000)                                                                       
as                            

SET @vcWOId=ISNULL(@vcWOId,'')

DECLARE @firstRec AS INTEGER
DECLARE @lastRec AS INTEGER
SET @firstRec= ((@CurrentPage-1) * @PageSize) + 1                                                                           
SET @lastRec= (@CurrentPage * @PageSize)    
    
DECLARE @TEMP TABLE
(
	ItemLevel INT, numParentWOID NUMERIC(18,0), numWOID NUMERIC(18,0), ID VARCHAR(1000), numItemCode NUMERIC(18,0),
	vcItemName VARCHAR(500), numQty INT, numWarehouseItemID NUMERIC(18,0), vcWarehouse VARCHAR(200), numOnHand NUMERIC(18,0),
	numOnOrder NUMERIC(18,0), numAllocation NUMERIC(18,0), numBackOrder NUMERIC(18,0), vcInstruction VARCHAR(2000),
	numAssignedTo NUMERIC(18,0), vcAssignedTo VARCHAR(500), numCreatedBy NUMERIC(18,0), vcCreated VARCHAR(500),
	bintCompliationDate VARCHAR(50), numWOStatus NUMERIC(18,0), numOppID NUMERIC(18,0), vcOppName VARCHAR(1000),
	numPOID NUMERIC(18,0), vcPOName VARCHAR(1000), bitWorkOrder BIT, bitReadyToBuild BIT
)

DECLARE @TEMPWORKORDER TABLE
(
	ItemLevel INT, RowNumber INT, numParentWOID NUMERIC(18,0), numWOID NUMERIC(18,0), ID VARCHAR(1000), numItemCode NUMERIC(18,0),
	numOppID NUMERIC(18,0), numWarehouseItemID NUMERIC(18,0), numQty NUMERIC(18,0), numWOStatus NUMERIC(18,0), vcInstruction VARCHAR(2000),
	numAssignedTo NUMERIC(18,0), bintCreatedDate DATETIME, bintCompliationDate DATETIME, numCreatedBy NUMERIC(18,0),  
	bitWorkOrder BIT, bitReadyToBuild BIT
) 

INSERT INTO 
	@TEMPWORKORDER
SELECT 
	1 AS ItemLevel,
	ROW_NUMBER() OVER(ORDER BY numWOId) AS RowNumber,
	CAST(0 AS NUMERIC(18,0)) AS numParentId,
	numWOId,
	CAST(CONCAT('#',ISNULL(numWOId,'0'),'#') AS VARCHAR(1000)) AS ID,
	numItemCode,
	numOppId,
	numWareHouseItemId,
	numQtyItemsReq,
	numWOStatus,
	vcInstruction,
	numAssignedTo,
	bintCreatedDate,
	bintCompliationDate,
	numCreatedBy,
	1 AS bitWorkOrder,
	dbo.CheckAssemblyBuildStatus(numItemCode,numQtyItemsReq) AS bitReadyToBuild
FROM 
	WorkOrder
WHERE  
	numDomainID=@numDomainID 
	AND 1=(CASE WHEN ISNULL(@numWOStatus,0) = 0 THEN CASE WHEN WorkOrder.numWOStatus <> 23184 THEN 1 ELSE 0 END ELSE CASE WHEN WorkOrder.numWOStatus = @numWOStatus THEN 1 ELSE 0 END END)
	AND 1=(Case when @numUserCntID>0 then case when isnull(numAssignedTo,0)=@numUserCntID then 1 else 0 end else 1 end)
	AND 1=(Case when len(@vcWOId)>0 then Case when numWOId in (SELECT * from dbo.Split(@vcWOId,',')) then 1 else 0 end else 1 end)
	AND (ISNULL(numParentWOID,0) = 0 OR numParentWOID NOT IN (SELECT numWOId FROM WorkOrder WHERE numDomainID=@numDomainID))
ORDER BY
	bintCreatedDate DESC,
	numWOId ASC

DECLARE @TOTALROWCOUNT AS NUMERIC(18,0)

SELECT @TOTALROWCOUNT=COUNT(*) FROM @TEMPWORKORDER


;WITH CTEWorkOrder (ItemLevel,numParentWOID,numWOID,ID,numItemKitID,numQtyItemsReq,numWarehouseItemID,vcInstruction,numAssignedTo,
					bintCreatedDate,numCreatedBy,bintCompliationDate,numWOStatus,numOppID,bitWorkOrder,bitReadyToBuild) AS
(
	SELECT
		ItemLevel,
		numParentWOID,
		numWOId,
		ID,
		numItemCode,
		numQty,
		numWareHouseItemId,
		vcInstruction,
		numAssignedTo,
		bintCreatedDate,
		numCreatedBy,
		bintCompliationDate,
		numWOStatus,
		numOppId,
		1 AS bitWorkOrder,
		bitReadyToBuild
	FROM
		@TEMPWORKORDER t
	WHERE
		t.RowNumber BETWEEN @firstRec AND @lastRec
	UNION ALL
	SELECT 
		c.ItemLevel+2,
		c.numWOID,
		WorkOrder.numWOId,
		CAST(CONCAT(c.ID,'-','#',ISNULL(WorkOrder.numWOId,'0'),'#') AS VARCHAR(1000)),
		WorkOrder.numItemCode,
		WorkOrder.numQtyItemsReq,
		WorkOrder.numWareHouseItemId,
		CAST(WorkOrder.vcInstruction AS VARCHAR(2000)),
		WorkOrder.numAssignedTo,
		WorkOrder.bintCreatedDate,
		WorkOrder.numCreatedBy,
		WorkOrder.bintCompliationDate,
		WorkOrder.numWOStatus,
		WorkOrder.numOppId,
		1 AS bitWorkOrder,
		dbo.CheckAssemblyBuildStatus(numItemCode,WorkOrder.numQtyItemsReq) AS bitReadyToBuild
	FROM 
		WorkOrder
	INNER JOIN 
		CTEWorkOrder c 
	ON 
		WorkOrder.numParentWOID = c.numWOID
)

INSERT 
	@TEMP
SELECT
	ItemLevel,
	numParentWOID,
	numWOID,
	ID,
	numItemCode,
	vcItemName,
	CTEWorkOrder.numQtyItemsReq,
	CTEWorkOrder.numWarehouseItemID,
	Warehouses.vcWareHouse,
	WareHouseItems.numOnHand,
	WareHouseItems.numOnOrder,
	WareHouseItems.numAllocation,
	WareHouseItems.numBackOrder,
	vcInstruction,
	CTEWorkOrder.numAssignedTo,
	dbo.fn_GetContactName(isnull(CTEWorkOrder.numAssignedTo,0)) AS vcAssignedTo,
	CTEWorkOrder.numCreatedBy,
	CAST(ISNULL(dbo.fn_GetContactName(CTEWorkOrder.numCreatedBy), '&nbsp;&nbsp;-') + ',' + convert(varchar(20),dateadd(minute,-@ClientTimeZoneOffset,CTEWorkOrder.bintCreatedDate)) AS VARCHAR(500)) ,
	convert(varchar(20),dateadd(minute,-@ClientTimeZoneOffset,bintCompliationDate)) AS bintCompliationDate,
	numWOStatus,
	CTEWorkOrder.numOppID,
	OpportunityMaster.vcPOppName,
	NULL,
	NULL,
	bitWorkOrder,
	bitReadyToBuild
FROM 
	CTEWorkOrder
INNER JOIN 
	Item
ON
	CTEWorkOrder.numItemKitID = Item.numItemCode
LEFT JOIN
	WareHouseItems
ON
	CTEWorkOrder.numWarehouseItemID = WareHouseItems.numWareHouseItemID
LEFT JOIN
	Warehouses
ON
	WareHouseItems.numWareHouseID = Warehouses.numWareHouseID
LEFT JOIN 
	OpportunityMaster
ON
	CTEWorkOrder.numOppID = OpportunityMaster.numOppId

INSERT INTO 
	@TEMP
SELECT
	t1.ItemLevel + 1,
	t1.numWOID,
	NULL,
	CONCAT(t1.ID,'-#0#'),
	WorkOrderDetails.numChildItemID,
	Item.vcItemName,
	WorkOrderDetails.numQtyItemsReq,
	WorkOrderDetails.numWarehouseItemID,
	Warehouses.vcWareHouse,
	WareHouseItems.numOnHand,
	WareHouseItems.numOnOrder,
	WareHouseItems.numAllocation,
	WareHouseItems.numBackOrder,
	vcInstruction,
	NULL,
	NULL,
	NULL,
	NULL,
	convert(varchar(20),dateadd(minute,-@ClientTimeZoneOffset,bintCompliationDate)) AS bintCompliationDate,
	numWOStatus,
	NULL,
	NULL,
	OpportunityMaster.numOppId,
	OpportunityMaster.vcPOppName,
	0 AS bitWorkOrder,
	0 AS bitReadyToBuild
FROM
	WorkOrderDetails
INNER JOIN
	@TEMP t1
ON
	WorkOrderDetails.numWOId = t1.numWOID
INNER JOIN 
	Item
ON
	WorkOrderDetails.numChildItemID = Item.numItemCode
LEFT JOIN
	WareHouseItems
ON
	WorkOrderDetails.numWarehouseItemID = WareHouseItems.numWareHouseItemID
LEFT JOIN
	Warehouses
ON
	WareHouseItems.numWareHouseID = Warehouses.numWareHouseID
LEFT JOIN
	OpportunityMaster 
ON
	WorkOrderDetails.numPOID = OpportunityMaster.numOppId

SELECT * FROM @TEMP ORDER BY numParentWOID,ItemLevel

SELECT @TOTALROWCOUNT AS TotalRowCount 

GO
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_InboxItems' ) 
    DROP PROCEDURE USP_InboxItems
GO
Create PROCEDURE [dbo].[USP_InboxItems]                                                                                              
@PageSize [int] ,                            
@CurrentPage [int],                            
@srch as varchar(100) = '',                            
--@vcSubjectFromTo as varchar(100) ='',
--@srchAttachmenttype as varchar(100) ='',                                   
@ToEmail as varchar (100)  ,        
@columnName as varchar(50) ,        
@columnSortOrder as varchar(4)  ,  
@numDomainId as numeric(9),  
@numUserCntId as numeric(9)  ,    
@numNodeId as numeric(9),  
--@chrSource as char(1),
@ClientTimeZoneOffset AS INT=0,
@tintUserRightType AS INT,
@EmailStatus AS NUMERIC(9)=0,
@srchFrom as varchar(100) = '', 
@srchTo as varchar(100) = '', 
@srchSubject as varchar(100) = '', 
@srchHasWords as varchar(100) = '', 
@srchHasAttachment as bit = 0, 
@srchIsAdvancedsrch as bit = 0,
@srchInNode as numeric(9)=0,
@FromDate AS DATE = NULL,
@ToDate AS DATE = NULL
as                                                      

--GET ENTERIES FROM PERTICULAR DOMAIN FROM VIEW_Email_Alert_Config
SELECT numDivisionID,numContactId,TotalBalanceDue,OpenSalesOppCount, OpenCaseCount, OpenProjectCount, UnreadEmailCount,OpenActionItemCount INTO #TEMP FROM VIEW_Email_Alert_Config WHERE numDomainId=@numDomainId

/************* START - GET ALERT CONFIGURATION **************/
DECLARE @bitOpenActionItem AS BIT = 1
DECLARE @bitOpencases AS BIT = 1
DECLARE @bitOpenProject AS BIT = 1
DECLARE @bitOpenSalesOpp AS BIT = 1
DECLARE @bitBalancedue AS BIT = 1
DECLARE @bitUnreadEmail AS BIT = 1
DECLARE @bitCampaign AS BIT = 1

SELECT  
	@bitOpenActionItem = ISNULL([bitOpenActionItem],1),
    @bitOpencases = ISNULL([bitOpenCases],1),
    @bitOpenProject = ISNULL([bitOpenProject],1),
    @bitOpenSalesOpp = ISNULL([bitOpenSalesOpp],1),
    @bitBalancedue = ISNULL([bitBalancedue],1),
    @bitUnreadEmail = ISNULL([bitUnreadEmail],1),
	@bitCampaign = ISNULL([bitCampaign],1)
FROM    
	AlertConfig
WHERE   
	AlertConfig.numDomainId = @numDomainId AND 
	AlertConfig.numContactId = @numUserCntId

/************* END - GET ALERT CONFIGURATION **************/
                                                      
 ---DECLARE @CRMType NUMERIC 
DECLARE @tintOrder AS TINYINT                                                      
DECLARE @vcFieldName AS VARCHAR(50)                                                      
DECLARE @vcListItemType AS VARCHAR(1)                                                 
DECLARE @vcAssociatedControlType VARCHAR(20)                                                      
DECLARE @numListID AS NUMERIC(9)                                                      
DECLARE @vcDbColumnName VARCHAR(20)                          
DECLARE @WhereCondition VARCHAR(2000)                           
DECLARE @vcLookBackTableName VARCHAR(2000)                    
DECLARE @bitCustom AS BIT
DECLARE @bitAllowEdit AS CHAR(1)                 
DECLARE @numFieldId AS NUMERIC  
DECLARE @bitAllowSorting AS CHAR(1)              
DECLARE @vcColumnName AS VARCHAR(500)  

DECLARE @strSql AS VARCHAR(5000)
DECLARE @column AS VARCHAR(50)                             
DECLARE @join AS VARCHAR(400) 
DECLARE @Nocolumns AS TINYINT               
DECLARE @lastRec AS INTEGER 
DECLARE @firstRec AS INTEGER                   
SET @join = ''           
SET @strSql = ''
--DECLARE @ClientTimeZoneOffset AS INT 
--DECLARE @numDomainId AS NUMERIC(18, 0)
SET @tintOrder = 0  
SET @WhereCondition = ''

SET @firstRec = ( @CurrentPage - 1 ) * @PageSize                                                              
SET @lastRec = ( @CurrentPage * @PageSize + 1 )   
SET @column = @columnName                
--DECLARE @join AS VARCHAR(400)                    
SET @join = ''             
IF @columnName LIKE 'Cust%' 
    BEGIN                  
        SET @join = ' left Join CFW_FLD_Values_Email CFW on CFW.RecId=DM.numEmailHstrID and CFW.fld_id= '
            + REPLACE(@columnName, 'Cust', '') + ' '                                                           
        SET @column = 'CFW.Fld_Value'
    END                                           
ELSE 
    IF @columnName LIKE 'DCust%' 
        BEGIN                  
            SET @join = ' left Join CFW_FLD_Values_Email CFW on CFW.RecId=DM.numEmailHstrID  and CFW.fld_id= '
                + REPLACE(@columnName, 'DCust', '')                                                                                                     
            SET @join = @join
                + ' left Join ListDetails LstCF on LstCF.numListItemID=CFW.Fld_Value  '                  
            SET @column = 'LstCF.vcData'                    
                  
        END            
    ELSE 
        BEGIN            
            DECLARE @lookbckTable AS VARCHAR(50)                
            SET @lookbckTable = ''                                                         
            SELECT  @lookbckTable = vcLookBackTableName
            FROM    View_DynamicDefaultColumns
            WHERE   numFormId = 44
                    AND vcDbColumnName = @columnName  and numDomainID=@numDOmainID               
            IF @lookbckTable <> '' 
                BEGIN                
                    IF @lookbckTable = 'EmailHistory' 
                        SET @column = 'ADC.' + @column                
                END                                                              
        END              
--DECLARE @strSql AS VARCHAR(5000)   
--SET @numNodeID = 0  
--SET @srchBody = ''
--SET @vcSubjectFromTo =''
--PRINT 'numnodeid' + CONVERT(VARCHAR, @numNodeId)                                 
SET @strSql = '
with FilterRows as (SELECT TOP('+ CONVERT(varchar,@CurrentPage) + ' * '+ CONVERT(varchar,@PageSize) + ') '                
SET @strSql = @strSql + 'ROW_NUMBER() OVER(ORDER BY EH.bintCreatedOn DESC ) AS RowNumber,COUNT(*) OVER(PARTITION BY NULL) AS TotalRowCount,EH.numEmailHstrID,isnull(EM.numContactId,0) as numContactId'
SET @strSql = @strSql + ' 
FROM [EmailHistory] EH JOIN EmailMaster EM on EH.numEMailId=EM.numEMailId'
SET @strSql = @strSql + ' 
WHERE EH.numDomainID ='+ CONVERT(VARCHAR,@numDomainId) +' And [numUserCntId] = '+ CONVERT(VARCHAR,@numUserCntId)+' '
--SET @strSql = @strSql + 'Where [numDomainID] ='+ CONVERT(VARCHAR,@numDomainId) +'  And [numUserCntId] = '+ CONVERT(VARCHAR,@numUserCntId)+' AND numNodeID ='+ CONVERT(VARCHAR,@numNodeId) +''
SET @strSql = @strSql + '
And chrSource IN(''B'',''I'') '

--Simple Search for All Node
IF @srchIsAdvancedsrch=0 
BEGIN
if len(@srch)>0
	BEGIN
		SET @strSql = @strSql + '
			AND (EH.vcSubject LIKE ''%'' + '''+ @srch +''' + ''%'' or EH.vcBodyText LIKE ''%'' + '''+ @srch + ''' + ''%''
			or EH.vcFrom LIKE ''%'' + '''+ @srch + ''' + ''%'' or EH.vcTo LIKE ''%'' + '''+ @srch + ''' + ''%'')'
	END
ELSE
	BEGIN --Particular Node
		SET @strSql = @strSql + ' AND [numNodeID] = '+ CONVERT(VARCHAR,@numNodeID) +' '
	END
END
--Advanced Search for All Node or selected Node
Else IF @srchIsAdvancedsrch=1
BEGIN
declare @strCondition as varchar(2000);set @strCondition=''

if len(@srchSubject)>0
	SET @strCondition='EH.vcSubject LIKE ''%'' + '''+ @srchSubject +''' + ''%'''

if len(@srchFrom)>0
	begin
		if len(@strCondition)>0
			SET @strCondition=@strCondition + ' and '

		SET @strCondition = @strCondition + ' EH.vcFrom LIKE ''%'' + '''+ @srchFrom + ''' + ''%'''
	end

if len(@srchTo)>0
	begin
		if len(@strCondition)>0
			SET @strCondition=@strCondition + ' and '

		SET @strCondition = @strCondition + ' EH.vcTo LIKE ''%'' + '''+ @srchTo + ''' + ''%'''
	end

if len(@srchHasWords)>0
	begin
		if len(@strCondition)>0
			SET @strCondition=@strCondition + ' and '

		SET @strCondition = @strCondition + ' EH.vcBodyText LIKE ''%'' + '''+ @srchHasWords + ''' + ''%'''
	end

IF @srchHasAttachment=1
BEGIN
	if len(@strCondition)>0
			SET @strCondition=@strCondition + ' and '
	
	SET @strCondition = @strCondition + ' EH.bitHasAttachments =  '''+ CONVERT(VARCHAR,@srchHasAttachment) + ''''
END

IF LEN(@FromDate) > 0 AND LEN(@ToDate) > 0
BEGIN
	IF LEN(@strCondition)>0
		SET @strCondition=@strCondition + ' AND '

	SET @strCondition = @strCondition + ' EH.dtReceivedOn >=  '''+ CONVERT(VARCHAR,@FromDate) + ''' AND EH.dtReceivedOn <= ''' + CONVERT(VARCHAR,@ToDate) + ''''
END

if len(@strCondition)>0
	BEGIN
		SET @strSql = @strSql +' and (' + @strCondition + ')'
		
		if @numNodeId>-1
			SET @strSql = @strSql + ' AND [numNodeID] = '+ CONVERT(VARCHAR,@numNodeId) +' '
	END
ELSE
	BEGIN --Particular Node
		SET @strSql = @strSql + ' AND [numNodeID] = '+ CONVERT(VARCHAR,@numNodeID) +' '
	END



--SET @strSql = @strSql + '
--AND (EH.vcSubject LIKE ''%'' + '''+ @srchSubject +''' + ''%'' and EH.vcFrom LIKE ''%'' + '''+ @srchFrom + ''' + ''%'' 
--and EH.vcTo LIKE ''%'' + '''+ @srchTo + ''' + ''%'' and EH.vcBodyText LIKE ''%'' + '''+ @srchHasWords + ''' + ''%'''
--
--IF @srchHasAttachment=1
--BEGIN
--SET @strSql = @strSql + ' and EH.bitHasAttachments =  '''+ CONVERT(VARCHAR,@srchHasAttachment) + ''''
--END

END


SET @strSql = @strSql + ' )'
--,FinalResult As
--( SELECT  TOP ('+CONVERT(VARCHAR,@PageSize)+ ') numEmailHstrID'
--SET @strSql = @strSql + ' 
--From FilterRows'
--SET @strSql = @strSql + '
--WHERE RowNumber > (('+ CONVERT(VARCHAR,@CurrentPage) + ' - 1) * '+ CONVERT(VARCHAR,@PageSize) +' ))'
--PRINT @strSql
--EXEC(@strSql)

select @Nocolumns=isnull(sum(TotalRow),0)  from(            
Select count(*) TotalRow from View_DynamicColumns where numFormId=44 and numUserCntID=@numUserCntID and numDomainID=@numDomainID AND tintPageType=1 AND numRelCntType=0 
Union 
Select count(*) TotalRow from View_DynamicCustomColumns where numFormId=44 and numUserCntID=@numUserCntID and numDomainID=@numDomainID AND tintPageType=1 AND numRelCntType=0) TotalRows

  CREATE TABLE #tempForm (tintOrder TINYINT,vcDbColumnName nvarchar(50),vcFieldName nvarchar(50),vcAssociatedControlType nvarchar(50),
  vcListItemType char(3),numListID numeric(9),vcLookBackTableName varchar(50),bitCustomField BIT,
  numFieldId NUMERIC,bitAllowSorting BIT,bitAllowEdit BIT,bitIsRequired BIT,bitIsEmail BIT,bitIsAlphaNumeric BIT,bitIsNumeric BIT,
  bitIsLengthValidation BIT,intMaxLength int,intMinLength int,bitFieldMessage BIT,vcFieldMessage VARCHAR(500),ListRelID numeric(9))


---Insert number of rows into temp table.
PRINT 'number OF columns :' + CONVERT(VARCHAR, @Nocolumns)
IF  @Nocolumns > 0 
    BEGIN    

INSERT INTO #tempForm
select tintRow+1 as tintOrder,vcDbColumnName,ISNULL(vcCultureFieldName,vcFieldName),                  
 vcAssociatedControlType,vcListItemType,numListID,vcLookBackTableName                                                 
,bitCustom,numFieldId,bitAllowSorting,bitAllowEdit,
bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage vcFieldMessage,ListRelID 
 FROM View_DynamicColumns 
 where numFormId=44 and numUserCntID=@numUserCntID and numDomainID=@numDomainID AND tintPageType=1 AND numRelCntType=0
  AND ISNULL(bitSettingField,0)=1 AND ISNULL(bitCustom,0)=0
                  
    UNION

     select tintRow+1 as tintOrder,vcDbColumnName,vcFieldName,                  
 vcAssociatedControlType,'' as vcListItemType,numListID,''                                                 
,bitCustom,numFieldId,bitAllowSorting,bitAllowEdit,
bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage,ListRelID
 from View_DynamicCustomColumns
 where numFormId=44 and numUserCntID=@numUserCntID and numDomainID=@numDomainID AND tintPageType=1 AND numRelCntType=0
 AND ISNULL(bitCustom,0)=1
 
 ORDER BY tintOrder ASC  
   
 END 
ELSE 
    BEGIN

 INSERT INTO #tempForm
select tintOrder,vcDbColumnName,ISNULL(vcCultureFieldName,vcFieldName),                  
 vcAssociatedControlType,vcListItemType,numListID,vcLookBackTableName                                                 
,bitCustom,numFieldId,bitAllowSorting,bitAllowEdit,
bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage vcFieldMessage,ListRelID 
 FROM View_DynamicDefaultColumns
 where numFormId=44 and bitDefault=1 AND ISNULL(bitSettingField,0)=1 AND numDomainID=@numDomainID
order by tintOrder asc   

    END
      SET @strSql =@strSql + ' Select TotalRowCount, EH.numEmailHstrID ,EH.numListItemId,bitIsRead,CASE WHEN LEN(Cast(EH.vcBodyText AS VARCHAR(1000)))>150 THEN SUBSTRING(EH.vcBodyText,0,150) + ''...'' ELSE
		 EH.vcBodyText END as vcBodyText,EH.numEmailHstrID As [KeyId~numEmailHstrID~0~0~0~0~HiddenField],FR.numContactId AS [ContactId~numContactID~0~0~0~0~HiddenField] ,EH.bitIsRead As [IsRead~bitIsRead~0~0~0~0~Image],isnull(EH.IsReplied,0) As [~IsReplied~0~0~0~0~Image] '
 
Declare @ListRelID as numeric(9) 

   select top 1 @tintOrder=tintOrder+1,@vcDbColumnName=vcDbColumnName,@vcFieldName=vcFieldName,                
  @vcAssociatedControlType=vcAssociatedControlType,@vcListItemType=vcListItemType,@numListID=numListID,@vcLookBackTableName=vcLookBackTableName                                               
,@bitCustom=bitCustomField,@numFieldId=numFieldId,@bitAllowSorting=bitAllowSorting,@bitAllowEdit=bitAllowEdit,@ListRelID=ListRelID
  from  #tempForm order by tintOrder asc            

WHILE @tintOrder > 0                                                      
    BEGIN                                                      
        IF @bitCustom = 0  
            BEGIN            
                DECLARE @Prefix AS VARCHAR(5)                        
                IF @vcLookBackTableName = 'EmailHistory' 
                    SET @Prefix = 'EH.'                        
                SET @vcColumnName = @vcFieldName + '~' + @vcDbColumnName
                    + '~' + @bitAllowSorting + '~'
                    + CONVERT(VARCHAR(10), @numFieldId) + '~'
                    + @bitAllowEdit + '~' + CONVERT(VARCHAR(1), @bitCustom)
                    + '~' + @vcAssociatedControlType
                PRINT @vcColumnName
                
                IF @vcAssociatedControlType = 'SelectBox' 
                    BEGIN      
                        PRINT @vcListItemType                                                    
                        IF @vcListItemType = 'L' 
                            BEGIN    
                            ---PRINT 'columnName' + @vcColumnName
                                SET @strSql = @strSql + ',L'
                                    + CONVERT(VARCHAR(3), @tintOrder)
                                    + '.vcData' + ' [' + @vcColumnName + ']'                                                         
                                SET @WhereCondition = @WhereCondition
                                    + ' left Join ListDetails L'
                                    + CONVERT(VARCHAR(3), @tintOrder)
                                    + ' on L' + CONVERT(VARCHAR(3), @tintOrder)
                                    + '.numListItemID=EH.numListItemID'                                                           
                            END 
                           
                    END 
                  ELSE IF (@vcAssociatedControlType = 'TextBox'
                        OR @vcAssociatedControlType = 'TextArea'
                        OR @vcAssociatedControlType = 'Image')  AND @vcDbColumnName<>'AlertPanel' AND @vcDbColumnName<>'RecentCorrespondance'
                        BEGIN
							IF @numNodeId=4 and @vcDbColumnName='vcFrom'  
							BEGIN
								  SET @vcColumnName = 'To' + '~' + @vcDbColumnName
                    + '~' + @bitAllowSorting + '~'
                    + CONVERT(VARCHAR(10), @numFieldId) + '~'
                    + @bitAllowEdit + '~' + CONVERT(VARCHAR(1), @bitCustom)
                    + '~' + @vcAssociatedControlType

								 SET @strSql = @strSql + ',' + @Prefix  + 'vcTo' + ' [' + @vcColumnName + ']'   
							END 
							ELSE
							BEGIN
								SET @strSql = @strSql + ',' + @Prefix  + @vcDbColumnName + ' [' + @vcColumnName + ']'   
							END                        
                             PRINT @Prefix   
                        END 
				Else IF  @vcDbColumnName='AlertPanel'   
							BEGIN 
								SET @strSql = @strSql +  ', dbo.GetAlertDetail(EH.numEmailHstrID, ACI.numECampaignID, FR.numContactId,' 
													  + CAST(@bitOpenActionItem AS VARCHAR(10)) + ',' 
													  + CAST(@bitOpencases AS VARCHAR(10)) + ',' 
													  + CAST(@bitOpenProject AS VARCHAR(10)) + ',' 
													  + CAST(@bitOpenSalesOpp AS VARCHAR(10)) + ',' 
													  + CAST(@bitBalancedue AS VARCHAR(10)) + ',' 
													  + CAST(@bitUnreadEmail AS VARCHAR(10)) + ',' 
													  + CAST(@bitCampaign AS VARCHAR(10)) + ',' +
													  'V1.TotalBalanceDue,V1.OpenSalesOppCount,V1.OpenCaseCount,V1.OpenProjectCount,V1.UnreadEmailCount,V2.OpenActionItemCount,V3.CampaignDTLCount) AS  [' + '' + @vcColumnName + '' + ']' 
							 END 
                 ELSE IF  @vcDbColumnName='RecentCorrespondance'
							BEGIN 
					            --SET @strSql = @strSql + ','+ '(SELECT COUNT(*) FROM dbo.Communication WHERE numContactId = FR.numContactId)' + ' AS  [' + '' + @vcColumnName + '' + ']'                                                       
					            SET @strSql = @strSql + ',0 AS  [' + '' + @vcColumnName + '' + ']'
                             END 
--				else if @vcAssociatedControlType='DateField'                                                  
--					begin            
--							set @strSql=@strSql+',case when convert(varchar(11),DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+',' +@Prefix+@vcDbColumnName+') )= convert(varchar(11),getdate()) then ''<b><font color=red>Today</font></b>'''      
--							set @strSql=@strSql+'when convert(varchar(11),DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+',' +@Prefix+@vcDbColumnName+') )= convert(varchar(11),dateadd(day,-1,getdate())) then ''<b><font color=purple>YesterDay</font></b>'''      
--							set @strSql=@strSql+'when convert(varchar(11),DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+',' +@Prefix+@vcDbColumnName+') )= convert(varchar(11),dateadd(day,1,getdate())) then''<b><font color=orange>Tommorow</font></b>'' ' 
--							set @strSql=@strSql+'else dbo.FormatedDateFromDate(DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+',' +@Prefix+@vcDbColumnName+'),'+convert(varchar(10),@numDomainId)+') end  ['+ @vcColumnName+']'
--				    end    
				else if @vcAssociatedControlType='DateField' AND @vcDbColumnName <> 'dtReceivedOn'                                     
					begin           
							set @strSql=@strSql+',case when convert(varchar(11),DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+',' +@Prefix+@vcDbColumnName+') )= convert(varchar(11),getdate()) then ''<b><font color=red>Today</font></b>'''      
							set @strSql=@strSql+'when convert(varchar(11),DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+',' +@Prefix+@vcDbColumnName+') )= convert(varchar(11),dateadd(day,-1,getdate())) then ''<b><font color=purple>YesterDay</font></b>'''      
							set @strSql=@strSql+'when convert(varchar(11),DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+',' +@Prefix+@vcDbColumnName+') )= convert(varchar(11),dateadd(day,1,getdate())) then''<b><font color=orange>Tommorow</font></b>'' ' 
							set @strSql=@strSql+'else dbo.FormatedDateFromDate(DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+',' +@Prefix+@vcDbColumnName+'),'+convert(varchar(10),@numDomainId)+') end  ['+ @vcColumnName+']'
				    end    
				else if @vcAssociatedControlType='DateField' AND @vcDbColumnName = 'dtReceivedOn'                                                 
					begin    
							set @strSql=@strSql+',case when convert(varchar(11),DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+',' +@Prefix+@vcDbColumnName+') )= convert(varchar(11),getdate()) then ''<b><font color=red>Today</font></b>'''      
							set @strSql=@strSql+'when convert(varchar(11),DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+',' +@Prefix+@vcDbColumnName+') )= convert(varchar(11),dateadd(day,-1,getdate())) then ''<b><font color=purple>YesterDay</font></b>'''      
							set @strSql=@strSql+'when convert(varchar(11),DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+',' +@Prefix+@vcDbColumnName+') )= convert(varchar(11),dateadd(day,1,getdate())) then''<b><font color=orange>Tommorow</font></b>'' ' 
							set @strSql=@strSql+'else CONVERT(VARCHAR(20),DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+',' +@Prefix+@vcDbColumnName+'),100) end  ['+ @vcColumnName+']'
				    end   
			END 
else if @bitCustom = 1                
begin                
            
               SET @vcColumnName= @vcFieldName+'~'+ @vcDbColumnName+'~0~'+ convert(varchar(10),@numFieldId)+'~1~'+ convert(varchar(1),@bitCustom) +'~'+ @vcAssociatedControlType
       
   select @vcFieldName=FLd_label,@vcAssociatedControlType=fld_type,@vcDbColumnName='Cust'+Convert(varchar(10),Fld_Id)                               
    from CFW_Fld_Master                                                
   where    CFW_Fld_Master.Fld_Id = @numFieldId                 
                
                 
    if @vcAssociatedControlType = 'TextBox' or @vcAssociatedControlType = 'TextArea'               
   begin                
                   
    set @strSql= @strSql+',CFW'+ convert(varchar(3),@tintOrder)+'.Fld_Value  ['+ @vcColumnName +']'                   
    set @WhereCondition= @WhereCondition +' left Join CFW_FLD_Values_Cont CFW'+ convert(varchar(3),@tintOrder)+ '                 
    on CFW'+ convert(varchar(3),@tintOrder)+ '.Fld_Id='+convert(varchar(10),@numFieldId) +'and CFW'+ convert(varchar(3),@tintOrder)+ '.RecId=FR.numContactId   '                                                         
   end      
    else if @vcAssociatedControlType = 'CheckBox'                 
   begin              
                 
    set @strSql= @strSql+',case when isnull(CFW'+ convert(varchar(3),@tintOrder)+'.Fld_Value,0)=0 then ''No'' when isnull(CFW'+ convert(varchar(3),@tintOrder)+'.Fld_Value,0)=1 then ''Yes'' end   ['+ @vcColumnName +']'              
   
    set @WhereCondition= @WhereCondition +' left Join CFW_FLD_Values_Cont CFW'+ convert(varchar(3),@tintOrder)+ '               
    on CFW'+ convert(varchar(3),@tintOrder)+ '.Fld_Id='+convert(varchar(10),@numFieldId) +'and CFW'+ convert(varchar(3),@tintOrder)+ '.RecId=FR.numContactId   '                                                       
   end              
  else if @vcAssociatedControlType = 'DateField'              
   begin                
                   
    set @strSql= @strSql+',dbo.FormatedDateFromDate(CFW'+ convert(varchar(3),@tintOrder)+'.Fld_Value,'+convert(varchar(10),@numDomainId)+')  ['+ @vcColumnName +']'                   
    set @WhereCondition= @WhereCondition +' left Join CFW_FLD_Values_Cont CFW'+ convert(varchar(3),@tintOrder)+ '                 
    on CFW'+ convert(varchar(3),@tintOrder)+ '.Fld_Id='+convert(varchar(10),@numFieldId) +'and CFW'+ convert(varchar(3),@tintOrder)+ '.RecId=FR.numContactId   '                                                         
   end                
   else if @vcAssociatedControlType = 'SelectBox'                    
   begin                
    set @vcDbColumnName = 'DCust'+Convert(varchar(10),@numFieldId)                
    set @strSql=@strSql+',L'+ convert(varchar(3),@tintOrder)+'.vcData'+' ['+ @vcColumnName +']'                                                          
    set @WhereCondition= @WhereCondition +' left Join CFW_FLD_Values_Cont CFW'+ convert(varchar(3),@tintOrder)+ '                 
     on CFW'+ convert(varchar(3),@tintOrder)+ '.Fld_Id='+convert(varchar(10),@numFieldId) +'and CFW'+ convert(varchar(3),@tintOrder)+ '.RecId=FR.numContactId   '                                                         
    set @WhereCondition= @WhereCondition +' left Join ListDetails L'+ convert(varchar(3),@tintOrder)+ ' on L'+convert(varchar(3),@tintOrder)+ '.numListItemID=CFW'+ convert(varchar(3),@tintOrder)+'.Fld_Value'                
   end                 
end     
                   --    SET @vcColumnName= @vcFieldName+'~'+ @vcDbColumnName+'~0~'+ convert(varchar(10),@numFieldId)+'~1~'+ convert(varchar(1),@bitCustom) +'~'+ @vcAssociatedControlType   
  select top 1 @tintOrder=tintOrder+1,@vcDbColumnName=vcDbColumnName,@vcFieldName=vcFieldName,                
  @vcAssociatedControlType=vcAssociatedControlType,@vcListItemType=vcListItemType,@numListID=numListID,@vcLookBackTableName=vcLookBackTableName                                               
,@bitCustom=bitCustomField,@numFieldId=numFieldId,@bitAllowSorting=bitAllowSorting,@bitAllowEdit=bitAllowEdit,@ListRelID=ListRelID
  from #tempForm WHERE tintOrder > @tintOrder-1 order by tintOrder asc            
 
           IF @@rowcount = 0 SET @tintOrder = 0 
		   PRINT @tintOrder 
    END  
                     
SELECT  * FROM    #tempForm

-------Change Row Color-------
Declare @vcCSOrigDbCOlumnName as varchar(50),@vcCSLookBackTableName as varchar(50),@vcCSAssociatedControlType as varchar(50)
SET @vcCSOrigDbCOlumnName=''
SET @vcCSLookBackTableName=''

Create table #tempColorScheme(vcOrigDbCOlumnName varchar(50),vcLookBackTableName varchar(50),vcFieldValue varchar(50),vcFieldValue1 varchar(50),vcColorScheme varchar(50),vcAssociatedControlType varchar(50))

insert into #tempColorScheme select DFM.vcOrigDbCOlumnName,DFM.vcLookBackTableName,DFCS.vcFieldValue,DFCS.vcFieldValue1,DFCS.vcColorScheme,DFFM.vcAssociatedControlType 
from DycFieldColorScheme DFCS join DycFormField_Mapping DFFM on DFCS.numFieldID=DFFM.numFieldID AND DFCS.numFormID=DFFM.numFormID
join DycFieldMaster DFM on DFM.numModuleID=DFFM.numModuleID and DFM.numFieldID=DFFM.numFieldID
where DFCS.numDomainID=@numDomainID and DFFM.numFormID=10 AND DFCS.numFormID=10 and isnull(DFFM.bitAllowGridColor,0)=1

IF(SELECT COUNT(*) FROM #tempColorScheme)>0
BEGIN
   select top 1 @vcCSOrigDbCOlumnName=vcOrigDbCOlumnName,@vcCSLookBackTableName=vcLookBackTableName,@vcCSAssociatedControlType=vcAssociatedControlType from #tempColorScheme
END   
---------------------------- 

IF len(@vcCSOrigDbCOlumnName)>0 and len(@vcCSLookBackTableName)>0
BEGIN
set @strSql=@strSql+',tCS.vcColorScheme'
END  

DECLARE @fltTotalSize FLOAT
SELECT @fltTotalSize=isnull(SUM(ISNULL(SpaceOccupied,0)),0) FROM dbo.View_InboxCount where numDomainID=@numDomainID and numUserCntID=@numUserCntID;

SET @strSql = @strSql + ', '+ CONVERT(VARCHAR(18),@fltTotalSize) +' AS TotalSize From EmailHistory EH Inner Join FilterRows FR on  FR.numEmailHstrID = EH.numEmailHstrID '
SET @strSql=@strSql + ' LEFT JOIN
	AdditionalContactsInformation ACI
ON 
	ACI.numContactId = FR.numContactId
CROSS APPLY
(
	SELECT
		SUM(TotalBalanceDue) TotalBalanceDue,
		SUM(OpenSalesOppCount) OpenSalesOppCount,
		SUM(OpenCaseCount) OpenCaseCount,
		SUM(OpenProjectCount) OpenProjectCount,
		SUM(UnreadEmailCount) UnreadEmailCount
	FROM
		#TEMP
	WHERE
		#TEMP.numDivisionId = ACI.numDivisionId
) AS V1
CROSS APPLY
(
	SELECT
		SUM(OpenActionItemCount) OpenActionItemCount
	FROM
		#TEMP
	WHERE
		#TEMP.numDivisionId = ACI.numDivisionId AND
		#TEMP.numContactId = FR.numContactId
) AS V2
CROSS APPLY
(
	SELECT 
		COUNT(*) AS CampaignDTLCount
	FROM 
		ConECampaignDTL 
	WHERE 
		numConECampID = (SELECT TOP 1 ISNULL(numConEmailCampID,0)  FROM ConECampaign WHERE numECampaignID = ACI.numECampaignID AND numContactID = ACI.numContactId ORDER BY numConEmailCampID DESC) AND 
		ISNULL(bitSend,0) = 0
) AS V3 '
SET @strSql = @strSql + @WhereCondition

IF len(@vcCSOrigDbCOlumnName)>0 and len(@vcCSLookBackTableName)>0
BEGIN
	 if @vcCSLookBackTableName = 'AdditionalContactsInformation'                  
		set @Prefix = 'ADC.'                  
     if @vcCSLookBackTableName = 'DivisionMaster'                  
		set @Prefix = 'DM.'
	 if @vcCSLookBackTableName = 'CompanyInfo'                  
		set @Prefix = 'CI.' 
	 if @vcCSLookBackTableName = 'AddressDetails'                  
		set @Prefix = 'AD.'   

set @strSql=@strSql+' left JOIN AdditionalContactsInformation ADC ON ADC.numcontactId=FR.numContactID                                                                             
 left JOIN DivisionMaster DM ON ADC.numDivisionId = DM.numDivisionID                                                                            
 left JOIN CompanyInfo CI ON DM.numCompanyID = CI.numCompanyId '
   
	IF @vcCSAssociatedControlType='DateField'
	BEGIN
			set @strSql=@strSql+' left join #tempColorScheme tCS on CONVERT(VARCHAR(10),' + @Prefix + @vcCSOrigDbCOlumnName + ',111) >= CONVERT(VARCHAR(10),DateAdd(day,cast(tCS.vcFieldValue as int),GETUTCDATE()),111)
			 and CONVERT(VARCHAR(10),' + @Prefix + @vcCSOrigDbCOlumnName + ',111) <= CONVERT(VARCHAR(10),DateAdd(day,cast(tCS.vcFieldValue1 as int),GETUTCDATE()),111)'
	END
	ELSE IF @vcCSAssociatedControlType='SelectBox' OR @vcCSAssociatedControlType='ListBox' OR @vcCSAssociatedControlType='CheckBox'
	BEGIN
		set @strSql=@strSql+' left join #tempColorScheme tCS on tCS.vcFieldValue=' + @Prefix + @vcCSOrigDbCOlumnName
	END
END

SET @strSql = @strSql + ' Where RowNumber >'  + CONVERT(VARCHAR(10), @firstRec) + ' and RowNumber <'
        + CONVERT(VARCHAR(10), @lastRec) 
IF @EmailStatus <> 0 
BEGIN
	SET @strSql = @strSql + ' AND EH.numListItemId = ' + CONVERT(VARCHAR(10), @EmailStatus)
END       
SET @strSql = @strSql + ' order by RowNumber'

PRINT @strSql 
EXEC(@strSql)   
DROP TABLE #tempForm          
DROP TABLE #TEMP
 
 /*
 declare @firstRec as integer                                                      
 declare @lastRec as integer                                                      
 set @firstRec= (@CurrentPage-1) * @PageSize                                                      
 set @lastRec= (@CurrentPage*@PageSize+1)   ;    
 
DECLARE @totalRows  INT
DECLARE @fltTotalSize FLOAT
SELECT @totalRows = RecordCount FROM dbo.View_InboxCount where numDomainID=@numDomainID and numUserCntID=@numUserCntID and numNodeId = @numNodeID ;
SELECT @fltTotalSize=SUM(ISNULL(SpaceOccupied,0)) FROM dbo.View_InboxCount where numDomainID=@numDomainID and numUserCntID=@numUserCntID;



-- Step 1: Get Only rows specific to user and domain 

WITH FilterRows
AS 
(
   SELECT 
   TOP (@CurrentPage * @PageSize)
        ROW_NUMBER() OVER(ORDER BY EH.bintCreatedOn DESC ) AS totrows,
        EH.numEmailHstrID
   FROM [EmailHistory] EH
		--LEFT JOIN EmailHstrAttchDtls A ON EH.numEmailHstrID = A.numEmailHstrID
   WHERE 
	   [numDomainID] = @numDomainID
	AND [numUserCntId] = @numUserCntId
	AND numNodeID = @numNodeId
	AND chrSource IN('B','I')
    AND (EH.vcBodyText LIKE '%' + @srchBody + '%' OR @srchBody = '')
	AND (EH.vcSubject LIKE '%' + @vcSubjectFromTo + '%' OR @vcSubjectFromTo ='')
    --AND (A.[vcAttachmentType] LIKE '%'+ @srchAttachmenttype + '%' OR @srchAttachmenttype='')
   
)
 --select seq1 + totrows1 -1 as TotRows2,* FROM [FilterRows]
,FinalResult
AS 
(
	SELECT  TOP (@PageSize) numEmailHstrID --,seq + totrows -1 as TotRows
	FROM FilterRows
	WHERE totrows > ((@CurrentPage - 1) * @PageSize)
)



-- select * FROM [UserEmail]
select  
		@totalRows TotRows,
		@fltTotalSize AS TotalSize,
		B.[numEmailHstrID],
        B.[numDomainID],
--        B.bintCreatedOn,
--        B.dtReceivedOn,
        B.numEmailHstrID,
        ISNULL(B.numListItemId,-1) AS numListItemId,
        ISNULL(vcSubject, '') AS vcSubject,
        CONVERT(VARCHAR(MAX), vcBody) AS vcBody,
        ISNULL(vcItemId, '') AS ItemId,
        ISNULL(vcChangeKey, '') AS ChangeKey,
        ISNULL(bitIsRead, 'False') AS IsRead,
        ISNULL(vcSize, 0) AS vcSize,
        ISNULL(bitHasAttachments, 'False') AS HasAttachments,
        ISNULL(B.tintType, 1) AS type,
        ISNULL(B.chrSource, 'B') AS chrSource,
        ISNULL(vcCategory, 'white') AS vcCategory,
        CASE B.[numNodeId] WHEN 4 THEN REPLACE(REPLACE(ISNULL(B.[vcTo],''),'<','') , '>','') ELSE B.vcFrom END FromName,
--        dbo.GetEmaillName(B.numEmailHstrID, 4) AS FromName,
--        dbo.GetEmaillName(B.numEmailHstrID, 1) AS ToName,
		CASE B.[numNodeId] WHEN 4 THEN 
			dbo.FormatedDateTimeFromDate(DATEADD(minute, -@ClientTimeZoneOffset,B.bintCreatedOn),B.numDomainId) 
		ELSE 
			dbo.FormatedDateTimeFromDate(B.dtReceivedOn,B.numDomainId)
		END dtReceivedOn,
        ISNULL(CONVERT(VARCHAR(5),B.[numNoofTimes]),'') NoOfTimeOpened
FROM [FinalResult] A INNER JOIN emailHistory B ON B.numEmailHstrID = A.numEmailHstrID;



--WITH UserEmail
--AS
--(
--			   SELECT
--						ROW_NUMBER() OVER ( ORDER BY X.bintCreatedOn DESC ) AS RowNumber,
--                        X.[numEmailHstrID]
--               FROM		[EmailHistory] X
--               WHERE    [numDomainID] = @numDomainID
--						AND [numUserCntId] = @numUserCntId
--						AND numNodeID = @numNodeId
--						AND X.chrSource IN('B','I')
--						
--)
--	SELECT 
--		COUNT(*) 
--   FROM UserEmail UM INNER JOIN [EmailHistory] EH  ON UM.numEmailHstrID =EH.numEmailHstrID
--		LEFT JOIN EmailHstrAttchDtls A ON EH.numEmailHstrID = A.numEmailHstrID
--   WHERE 
--		 (EH.vcBodyText LIKE '%' + @srchBody + '%' OR @srchBody = '')
--	AND (EH.vcSubject LIKE '%' + @vcSubjectFromTo + '%' OR @vcSubjectFromTo ='')
--    AND (A.[vcAttachmentType] LIKE '%'+ @srchAttachmenttype + '%' OR @srchAttachmenttype='')



--SELECT COUNT(*)
--FROM      View_Inbox I INNER JOIN [EmailHistory] EH ON EH.numEmailHstrID = I.numEmailHstrID
--			LEFT JOIN EmailHstrAttchDtls A ON EH.numEmailHstrID = A.numEmailHstrID
-- WHERE     I.numDomainId = @numDomainId
--                                AND I.numUserCntId = @numUserCntId
--                                AND I.numNodeId = @numNodeId
--                                AND I.chrSource IN('B','I')
--                                AND (EH.vcBodyText LIKE '%' + @srchBody + '%' OR @srchBody = '')
--								AND (I.vcSubjectFromTo LIKE '%' + @vcSubjectFromTo + '%' OR @vcSubjectFromTo ='')
--                                AND (A.[vcAttachmentType] LIKE '%'+ @srchAttachmenttype + '%' OR @srchAttachmenttype='')

RETURN ;
*/                                           
/* SELECT Y.[ID],
        Y.[numEmailHstrID],
        emailHistory.[numDomainID],
        emailHistory.bintCreatedOn,
        emailHistory.dtReceivedOn,
        emailHistory.numEmailHstrID,
        ISNULL(vcSubject, '') AS vcSubject,
        CONVERT(VARCHAR(MAX), vcBody) AS vcBody,
        ISNULL(vcItemId, '') AS ItemId,
        ISNULL(vcChangeKey, '') AS ChangeKey,
        ISNULL(bitIsRead, 'False') AS IsRead,
        ISNULL(vcSize, 0) AS vcSize,
        ISNULL(bitHasAttachments, 'False') AS HasAttachments,
        ISNULL(emailHistory.tintType, 1) AS type,
        ISNULL(emailHistory.chrSource, 'B') AS chrSource,
        ISNULL(vcCategory, 'white') AS vcCategory,
        dbo.GetEmaillName(Y.numEmailHstrID, 4) AS FromName,
        dbo.GetEmaillName(Y.numEmailHstrID, 1) AS ToName,
        dbo.FormatedDateFromDate(DATEADD(minute, -@ClientTimeZoneOffset,
                                         emailHistory.bintCreatedOn),
                                 emailHistory.numDomainId) AS bintCreatedOn,
        dbo.FormatedDateFromDate(emailHistory.dtReceivedOn,
                                 emailHistory.numDomainId) AS dtReceivedOn,
        ISNULL(CONVERT(VARCHAR(5),EmailHistory.[numNoofTimes]),'') NoOfTimeOpened
 FROM   ( SELECT   
				ROW_NUMBER() OVER ( ORDER BY X.bintCreatedOn DESC ) AS ID,
				X.[numEmailHstrID]
          FROM      ( SELECT    DISTINCT
                                EH.numEmailHstrID,
                                EH.[bintCreatedOn]
                      FROM      --View_Inbox I INNER JOIN 
                      [EmailHistory] EH --ON EH.numEmailHstrID = I.numEmailHstrID
								LEFT JOIN EmailHstrAttchDtls A ON EH.numEmailHstrID = A.numEmailHstrID
                      WHERE     EH.numDomainId = @numDomainId
                                AND EH.numUserCntId = @numUserCntId
                                AND EH.numNodeId = @numNodeId
                                AND EH.chrSource IN('B','I')
                                AND (EH.vcBodyText LIKE '%' + @srchBody + '%' OR @srchBody = '')
								AND (EH.vcSubject LIKE '%' + @vcSubjectFromTo + '%' OR @vcSubjectFromTo ='')
                                AND (A.[vcAttachmentType] LIKE '%'+ @srchAttachmenttype + '%' OR @srchAttachmenttype='')
                    ) X
          
        ) Y
        INNER JOIN emailHistory ON emailHistory.numEmailHstrID = Y.numEmailHstrID
--        LEFT JOIN EmailHstrAttchDtls ON emailHistory.numEmailHstrID = EmailHstrAttchDtls.numEmailHstrID
        AND Y.ID > @firstRec AND Y.ID < @lastRec
--UNION
--   SELECT   0 AS ID,
--			COUNT(*),
--            NULL,
--            NULL,
--            NULL,
--            NULL,
--            NULL,
--            NULL,
--            NULL,
--            NULL,
--            NULL,
--            NULL,
--            NULL,
----            NULL,
----            NULL,
----            NULL,
--            1,
--            NULL,
--            NULL,
--            NULL,
--            NULL,
--            NULL,
--            NULL,
--            ''
--FROM      View_Inbox I INNER JOIN [EmailHistory] EH ON EH.numEmailHstrID = I.numEmailHstrID
--			LEFT JOIN EmailHstrAttchDtls A ON EH.numEmailHstrID = A.numEmailHstrID
-- WHERE     I.numDomainId = @numDomainId
--                                AND I.numUserCntId = @numUserCntId
--                                AND I.numNodeId = @numNodeId
--                                AND I.chrSource IN('B','I')
--                                AND (EH.vcBodyText LIKE '%' + @srchBody + '%' OR @srchBody = '')
--								AND (I.vcSubjectFromTo LIKE '%' + @vcSubjectFromTo + '%' OR @vcSubjectFromTo ='')
--                                AND (A.[vcAttachmentType] LIKE '%'+ @srchAttachmenttype + '%' OR @srchAttachmenttype='')
--   ORDER BY ID

RETURN 
*/
/*
declare @strSql as varchar(8000)                    
   if @columnName = 'FromName'               
 begin        
 set @columnName =  'dbo.GetEmaillName(emailHistory.numEmailHstrID,4)'        
 end        
 if @columnName = 'FromEmail'        
 begin        
 set @columnName = 'dbo.GetEmaillAdd(emailHistory.numEmailHstrID,4)'        
 end        
  if @columnName = 'ToName'               
 begin        
 set @columnName =  'dbo.GetEmaillName(emailHistory.numEmailHstrID,1)'        
 end        
 if @columnName = 'ToEmail'        
 begin        
 set @columnName = 'dbo.GetEmaillAdd(emailHistory.numEmailHstrID,1)'        
 end 
                  
set @strSql='With tblSubscriber AS (SELECT  distinct(emailHistory.numEmailHstrID) ,            
ROW_NUMBER() OVER (ORDER BY '+@columnName+' '+@columnSortOrder+') AS  RowNumber             
from emailHistory                                
--left join EmailHstrAttchDtls on emailHistory.numEmailHstrID=EmailHstrAttchDtls.numEmailHstrID                     
--join EmailHStrToBCCAndCC on emailHistory.numEmailHstrID=EmailHStrToBCCAndCC.numEmailHstrID                     
--join EmailMaster on        EmailMaster.numEmailId=EmailHStrToBCCAndCC.numEmailId                     
where numDomainId='+convert(varchar(15),@numDomainId)+' and numUserCntId='+convert(varchar(15),@numuserCntId)+  
' and numNodeId='+convert(varchar(15),@numNodeId)  
    
 if @chrSource<> '0'   set @strSql=@strSql +' and (chrSource  = ''B'' or chrSource like ''%'+@chrSource+'%'')'   
           
 if @srchBody <> '' set @strSql=@strSql +' and (vcSubject  like ''%'+@srchBody+'%'' or  
  vcBodyText like ''%'+@srchBody+'%''            
  or emailHistory.numEmailHstrID in (select numEmailHstrId from EmailHStrToBCCAndCC where             
 (numemailid in (select numEmailid from emailmaster where vcemailid like ''%'+@srchBody+'%'')) or   
  vcName like ''%'+@srchBody+'%'') )'                
--if @ToEmail <> '' set @strSql=@strSql +'               
--      and (EmailMaster.vcEmailId like '''+@ToEmail+''' and EmailHStrToBCCAndCC.tintType<>4 )  '            
if @srchAttachmenttype <>'' set @strSql=@strSql +'              
  and  emailHistory.numEmailHstrID in  (select numEmailHstrId from EmailHstrAttchDtls                
  where EmailHstrAttchDtls.vcAttachmentType like ''%'+@srchAttachmenttype+'%'')'            */
            
            
/*Please Do not use GetEmaillAdd function as it recursively fetched email address for single mail which create too bad performance*/                 

/*set @strSql=@strSql +')                     
 select RowNumber,            
--dbo.GetEmaillAdd(emailHistory.numEmailHstrID,4) as FromEmail,                  
dbo.GetEmaillName(emailHistory.numEmailHstrID,4) as FromName,                             
--isnull(vcFromEmail,'''') as FromEmail,                                      
--dbo.GetEmaillAdd(emailHistory.numEmailHstrID,1) as ToEmail,                                 
  
--dbo.GetEmaillAdd(emailHistory.numEmailHstrID,2) as CCEmail,                                
--dbo.GetEmaillAdd(emailHistory.numEmailHstrID,3) as BCCEmail,                                     
emailHistory.numEmailHstrID,                            
isnull(vcSubject,'''') as vcSubject,                                      
convert(varchar(max),vcBody) as vcBody,                              
dbo.FormatedDateFromDate( DATEADD(minute,'+ CONVERT(VARCHAR(15),-@ClientTimeZoneOffset)+',bintCreatedOn),numDomainId) as bintCreatedOn,                              
isnull(vcItemId,'''') as ItemId,                              
isnull(vcChangeKey,'''') as ChangeKey,                              
isnull(bitIsRead,''False'') as IsRead,                  
isnull(vcSize,0) as vcSize,                              
isnull(bitHasAttachments,''False'') as HasAttachments,                             
  isnull(EmailHstrAttchDtls.vcFileName,'''') as AttachmentPath,                            
isnull(EmailHstrAttchDtls.vcAttachmentType,'''') as AttachmentType,                      
isnull(EmailHstrAttchDtls.vcAttachmentItemId,'''') as AttachmentItemId,                             
dbo.FormatedDateFromDate(dtReceivedOn,numDomainId) as dtReceivedOn,    
dbo.GetEmaillName(emailHistory.numEmailHstrID,1) as ToName,                          
isnull(emailHistory.tintType,1) as type,                              
isnull(chrSource,''B'') as chrSource,                          
isnull(vcCategory,''white'') as vcCategory                   
 from tblSubscriber T                  
 join emailHistory                   
 on emailHistory.numEmailHstrID=T.numEmailHstrID             
left join EmailHstrAttchDtls on emailHistory.numEmailHstrID=EmailHstrAttchDtls.numEmailHstrID             
where numDomainId='+convert(varchar(15),@numDomainId)+' and numUserCntId='+convert(varchar(15),@numuserCntId)+  
' and numNodeId='+convert(varchar(15),@numNodeId)+' and                
  RowNumber > '+convert(varchar(10),@firstRec)+' and RowNumber < '+convert(varchar(10),@lastRec)+'              
union                 
 select 0 as RowNumber,null,count(*),null,null,            
null,null,null,null,null,null,null,null,null,NULL,           
null,1,null,null from tblSubscriber  order by RowNumber'            
--print    @strSql             
exec (@strSql)
GO
*/               

-- =============================================
-- Author:		<Author,,Sachin Sadhu>
-- Create date: <Create Date,,07-Dec-13>
-- Description:	<Description,,Fetch New MailBox>
-- =============================================

GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_InboxTreeSort_NewSortOrder' ) 
    DROP PROCEDURE USP_InboxTreeSort_NewSortOrder
GO
CREATE PROCEDURE USP_InboxTreeSort_NewSortOrder
   	-- Add the parameters for the stored procedure here
	@numDomainID int,
	@numUserCntID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-- Insert statements for procedure here
	IF Not Exists (Select 'col1' from  InboxTreeSort where numdomainid=@numDomainID AND numUserCntId=@numUserCntID)
	BEGIN

		INSERT INTO InboxTreeSort
		(numParentID, vcNodeName, numDomainID, numUserCntID, numSortOrder, bitSystem)
		VALUES     (NULL,NULL,@numDomainID,@numUserCntID,1,1)
				
		DECLARE @lastParentid INT
		SELECT @lastParentid=@@identity FROM InboxTreeSort

		INSERT INTO InboxTreeSort
		(numParentID, vcNodeName, numDomainID, numUserCntID, numSortOrder, bitSystem, numFixID)
		VALUES     (@lastParentid,'Inbox',@numDomainID,@numUserCntID,2,1,1)
		INSERT INTO InboxTreeSort
		(numParentID, vcNodeName, numDomainID, numUserCntID, numSortOrder, bitSystem, numFixID)
		VALUES     (@lastParentid,'Email Archive',@numDomainID,@numUserCntID,3,1,2)
		INSERT INTO InboxTreeSort
		(numParentID, vcNodeName, numDomainID, numUserCntID, numSortOrder, bitSystem, numFixID)
		VALUES     (@lastParentid,'Sent Messages',@numDomainID,@numUserCntID,4,1,4)
		INSERT INTO InboxTreeSort
		(numParentID, vcNodeName, numDomainID, numUserCntID, numSortOrder, bitSystem, numFixID)
		VALUES     (@lastParentid,'Calendar',@numDomainID,@numUserCntID,5,1,5)
		INSERT INTO InboxTreeSort
		(numParentID, vcNodeName, numDomainID, numUserCntID, numSortOrder, bitSystem, numFixID)
		VALUES     (@lastParentid,'Custom Folders',@numDomainID,@numUserCntID,6,1,6)

		--ALL RECORDS ARE ALREADY REPLACED WITH MASTER SCRIPT TO APPROPROATE ID FOR USERCNTID BUT
		--THERE ARE SOME RECORDS IN EMAIL HISTORY TABLE FOR SOME USERS WHOSE CORRESPONDING TREE STRUCTURE IS NOT AVILABLE IN INBOXTREESORT TABLE
		--FOLLOWING SCRIPTS UPDATED NODEID IN EMAIL HISTORY TABLE FOR INBOX=0, SENT EMAILS=4 AND DELETED MAILS=2 INCASE EXISTS BEFORE TREE STRUCTURE CREATED
		UPDATE EmailHistory SET numNodeId = ISNULL((SELECT numNodeId FROM InboxTreeSort WHERE numDomainID = @numDomainID AND numUserCntID = @numUserCntID AND ISNULL(numFixID,0)=4),4) WHERE numDomainID = @numDomainID AND numUserCntId = @numUserCntID AND numNodeId = 4 AND (numNodeId <> (SELECT numNodeId FROM InboxTreeSort WHERE numDomainID = @numDomainID AND numUserCntID = @numUserCntID AND ISNULL(numFixID,0)=4))
		UPDATE EmailHistory SET numNodeId = ISNULL((SELECT numNodeId FROM InboxTreeSort WHERE numDomainID = @numDomainID AND numUserCntID = @numUserCntID AND ISNULL(numFixID,0)=2),2) WHERE numDomainID = @numDomainID AND numUserCntId = @numUserCntID AND numNodeId = 2 AND (numNodeId <> (SELECT numNodeId FROM InboxTreeSort WHERE numDomainID = @numDomainID AND numUserCntID = @numUserCntID AND ISNULL(numFixID,0)=2))
		UPDATE EmailHistory SET numNodeId = ISNULL((SELECT numNodeId FROM InboxTreeSort WHERE numDomainID = @numDomainID AND numUserCntID = @numUserCntID AND ISNULL(numFixID,0)=1),0) WHERE numDomainID = @numDomainID AND numUserCntId = @numUserCntID AND numNodeId = 0 AND (numNodeId <> (SELECT numNodeId FROM InboxTreeSort WHERE numDomainID = @numDomainID AND numUserCntID = @numUserCntID AND ISNULL(numFixID,0)=1))
	END
	
	SELECT 
		numNodeID,
		numParentId,
		vcNodeName as vcNodeName,
		numSortOrder, 
		bitSystem,
		numFixID
	FROM 
		InboxTreeSort 
	WHERE 
		(numdomainid=@numDomainID) AND (numUserCntId=@numUserCntID )   
	ORDER BY 
		numSortOrder ASC
END
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_InLineEditDataSave')
DROP PROCEDURE USP_InLineEditDataSave
GO
CREATE PROCEDURE [dbo].[USP_InLineEditDataSave] 
( 
 @numDomainID NUMERIC(9),
 @numUserCntID NUMERIC(9),
 @numFormFieldId NUMERIC(9),
 @bitCustom AS BIT = 0,
 @InlineEditValue VARCHAR(8000),
 @numContactID NUMERIC(9),
 @numDivisionID NUMERIC(9),
 @numWOId NUMERIC(9),
 @numProId NUMERIC(9),
 @numOppId NUMERIC(9),
 @numRecId NUMERIC(9),
 @vcPageName NVARCHAR(100),
 @numCaseId NUMERIC(9),
 @numWODetailId NUMERIC(9),
 @numCommID NUMERIC(9)
 )
AS 

declare @strSql as nvarchar(4000)
DECLARE @vcDbColumnName AS VARCHAR(100),@vcOrigDbColumnName AS VARCHAR(100),@vcLookBackTableName AS varchar(100)
DECLARE @vcListItemType as VARCHAR(3),@numListID AS numeric(9),@vcAssociatedControlType varchar(20)
declare @tempAssignedTo as numeric(9);set @tempAssignedTo=null           
DECLARE @Value1 AS VARCHAR(18),@Value2 AS VARCHAR(18)

IF  @bitCustom =0
BEGIN
	SELECT @vcDbColumnName=vcDbColumnName,@vcLookBackTableName=vcLookBackTableName,
		   @vcListItemType=vcListItemType,@numListID=numListID,@vcAssociatedControlType=vcAssociatedControlType,
		   @vcOrigDbColumnName=vcOrigDbColumnName
	 FROM DycFieldMaster WHERE numFieldId=@numFormFieldId
	 
	 
	if @vcLookBackTableName = 'DivisionMaster' 
	 BEGIN 
	 
	   IF @vcDbColumnName='numFollowUpStatus' --Add to FollowUpHistory
	    Begin
			declare @numFollow as varchar(10),@binAdded as varchar(20),@PreFollow as varchar(10),@RowCount as varchar(2)                            
			                      
			set @PreFollow=(select count(*)  numFollowUpStatus from FollowUpHistory where numDivisionID= @numDivisionID and numDomainId= @numDomainID)                          
			set @RowCount=@@rowcount  
			                        
			select   @numFollow=numFollowUpStatus, @binAdded=bintModifiedDate from divisionmaster where numDivisionID=@numDivisionID and numDomainId= @numDomainID                    
			
			if @numFollow <>'0' and @numFollow <> @InlineEditValue                          
			begin                          
				if @PreFollow<>0                          
					begin                          
			                   
						if @numFollow <> (select top 1  numFollowUpStatus from FollowUpHistory where numDivisionID= @numDivisionID and numDomainId= @numDomainID order by numFollowUpStatusID desc)                          
							begin                          
			                      	insert into FollowUpHistory(numFollowUpstatus,numDivisionID,bintAddedDate,numDomainId)                          
									values(@numFollow,@numDivisionID,@binAdded,@numDomainID)                          
							end                          
					end                          
				else                          
					begin                          
						insert into FollowUpHistory(numFollowUpstatus,numDivisionID,bintAddedDate,numDomainId)                          
						 values(@numFollow,@numDivisionID,@binAdded,@numDomainID)                          
					end                          
			   end 
			END
		
		 IF @vcDbColumnName='numAssignedTo' ---Updating if organization is assigned to someone
			BEGIN
				select @tempAssignedTo=isnull(numAssignedTo,0) from DivisionMaster where numDivisionID = @numDivisionID and numDomainId= @numDomainID           
			
				if (@tempAssignedTo<>@InlineEditValue and  @InlineEditValue<>'0')          
					begin            
						update DivisionMaster set numAssignedTo=@InlineEditValue ,numAssignedBy=@numUserCntID where numDivisionID = @numDivisionID  and numDomainId= @numDomainID           
					end           
				else if  (@InlineEditValue ='0')          
					begin          
						update DivisionMaster set numAssignedTo=0 ,numAssignedBy=0 where numDivisionID = @numDivisionID  and numDomainId= @numDomainID        
					end    
			END
	    	
		SET @strSql='update DivisionMaster set ' + @vcDbColumnName + '=@InlineEditValue,numModifiedBy=@numUserCntID,bintModifiedDate=getutcdate() where numDivisionID=@numDivisionID and numDomainID=@numDomainID'
	 END 
	 ELSE if @vcLookBackTableName = 'AdditionalContactsInformation' 
	 BEGIN 
			IF @vcDbColumnName='bitPrimaryContact' AND @InlineEditValue='True'
			BEGIN
				DECLARE @bitPrimaryContactCheck AS BIT             
				DECLARE @numID AS NUMERIC(9) 
				
				SELECT TOP 1 @numID = numContactID,@bitPrimaryContactCheck = ISNULL(bitPrimaryContact,0)
					FROM AdditionalContactsInformation WHERE numDivisionID = @numDivisionID 
                   
					IF @@rowcount = 0 SET @numID = 0             
				
					WHILE @numID > 0            
					BEGIN            
						IF @bitPrimaryContactCheck = 1 
							UPDATE  AdditionalContactsInformation SET bitPrimaryContact = 0 WHERE numContactID = @numID  
                              
						SELECT TOP 1 @numID = numContactID,@bitPrimaryContactCheck = ISNULL(bitPrimaryContact,0)
						FROM AdditionalContactsInformation WHERE numDivisionID = @numDivisionID AND numContactID > @numID    
                                
						IF @@rowcount = 0 SET @numID = 0             
					END 
				END
				ELSE IF @vcDbColumnName='bitPrimaryContact' AND @InlineEditValue='False'
				BEGIN
					IF (SELECT COUNT(*) FROM AdditionalContactsInformation WHERE numDivisionID = @numDivisionID AND numContactId!=@numContactId AND ISNULL(bitPrimaryContact,0)=1) = 0
						SET @InlineEditValue='True'
				END 
				
		SET @strSql='update AdditionalContactsInformation set ' + @vcDbColumnName + '=@InlineEditValue,numModifiedBy=@numUserCntID,bintModifiedDate=getutcdate() where numDivisionID=@numDivisionID and numDomainID=@numDomainID and numContactId=@numContactId'
	 END 
	 ELSE if @vcLookBackTableName = 'CompanyInfo' 
	 BEGIN 
		SET @strSql='update CompanyInfo set ' + @vcDbColumnName + '=@InlineEditValue,numModifiedBy=@numUserCntID,bintModifiedDate=getutcdate() where numCompanyId=(select numCompanyId from divisionmaster where numDivisionID=@numDivisionID and numDomainID=@numDomainID) and numDomainID=@numDomainID'
	 END 
	 ELSE if @vcLookBackTableName = 'WorkOrder' 
	 BEGIN 
		DECLARE @numItemCode AS NUMERIC(9)
	 
		IF @numWODetailId>0
		BEGIN
			SET @strSql='update WorkOrderDetails set ' + @vcDbColumnName + '=@InlineEditValue where numWOId=@numWOId and numWODetailId=@numWODetailId'
			
			IF @vcDbColumnName='numQtyItemsReq'
			BEGIN 
				SELECT @numItemCode=numChildItemID FROM WorkOrderDetails WHERE numWOId=@numWOId and numWODetailId=@numWODetailId
				
				EXEC USP_UpdateWorkOrderDetailQuantity @numItemCode,@InlineEditValue,@numWOId
				--UPDATE WorkOrderDetails SET numQtyItemsReq=CONVERT(NUMERIC(9),numQtyItemsReq_Orig * CONVERT(NUMERIC(9,0),@InlineEditValue)) WHERE numWOId=@numWOId
			END 
		END 	
		ELSE
		BEGIN
			SET @strSql='update WorkOrder set ' + @vcDbColumnName + '=@InlineEditValue where numWOId=@numWOId and numDomainID=@numDomainID'
			
			IF @vcDbColumnName='numQtyItemsReq'
			BEGIN 
				SELECT @numItemCode=numItemCode FROM WorkOrder WHERE numWOId=@numWOId and numDomainID=@numDomainID
			
				EXEC USP_UpdateWorkOrderDetailQuantity @numItemCode,@InlineEditValue,@numWOId
				--UPDATE WorkOrderDetails SET numQtyItemsReq=CONVERT(NUMERIC(9),numQtyItemsReq_Orig * CONVERT(NUMERIC(9,0),@InlineEditValue)) WHERE numWOId=@numWOId
			END 
		END	
	 END
	 ELSE if @vcLookBackTableName = 'ProjectsMaster' 
	 BEGIN 
		IF @vcDbColumnName='numAssignedTo' ---Updating if Project is assigned to someone 
			BEGIN
				select @tempAssignedTo=isnull(numAssignedTo,0) from ProjectsMaster where numProId=@numProId and numDomainId= @numDomainID           
			
				if (@tempAssignedTo<>@InlineEditValue and  @InlineEditValue<>'0')          
					begin            
						update ProjectsMaster set numAssignedTo=@InlineEditValue ,numAssignedBy=@numUserCntID where numProId = @numProId  and numDomainId= @numDomainID           
					end           
				else if  (@InlineEditValue ='0')          
					begin          
						update ProjectsMaster set numAssignedTo=0 ,numAssignedBy=0 where numProId = @numProId  and numDomainId= @numDomainID        
					end    
			END
		else IF @vcDbColumnName='numProjectStatus'  ---Updating All Statge to 100% if Completed
			BEGIN
				IF @InlineEditValue='27492'
				BEGIN
					UPDATE PM SET dtCompletionDate=GETUTCDATE(),monTotalExpense=PIE.monExpense,
					monTotalIncome=PIE.monIncome,monTotalGrossProfit=PIE.monBalance,numProjectStatus=27492
					FROM ProjectsMaster PM,dbo.GetProjectIncomeExpense(@numDomainId,@numProId) PIE
					WHERE PM.numProId=@numProId
					
					EXEC USP_ManageProjectCommission @numDomainId,@numProId
						
					Update StagePercentageDetails set tinProgressPercentage=100,bitclose=1 where numProjectID=@numProId

					EXEC dbo.USP_GetProjectTotalProgress
							@numDomainID = @numDomainID, --  numeric(9, 0)
							@numProId = @numProId, --  numeric(9, 0)
							@tintMode = 1 --  tinyint
				END			
				ELSE
				BEGIN
					DELETE from BizDocComission WHERE numDomainID = @numDomainID AND numProId=@numProId AND numComissionID 
						NOT IN (SELECT BC.numComissionID FROM dbo.BizDocComission BC JOIN dbo.PayrollTracking PT 
							ON BC.numComissionID = PT.numComissionID AND BC.numDomainId = PT.numDomainId WHERE BC.numDomainID = @numDomainID AND ISNULL(BC.numProId,0) = @numProId)
				END
			END
			

		SET @strSql='update ProjectsMaster set ' + @vcDbColumnName + '=@InlineEditValue,numModifiedBy=@numUserCntID,bintModifiedDate=getutcdate() where numProId=@numProId and numDomainID=@numDomainID'
	 END 
	 ELSE if @vcLookBackTableName = 'OpportunityMaster' 
	 BEGIN 
		IF @vcDbColumnName='numAssignedTo' ---Updating if organization is assigned to someone  
			BEGIN
				select @tempAssignedTo=isnull(numAssignedTo,0) from OpportunityMaster where numOppId=@numOppId and numDomainId= @numDomainID           
			
				if (@tempAssignedTo<>@InlineEditValue and  @InlineEditValue<>'0')          
					begin            
						update OpportunityMaster set numAssignedTo=@InlineEditValue ,numAssignedBy=@numUserCntID where numOppId = @numOppId  and numDomainId= @numDomainID           
					end           
				else if  (@InlineEditValue ='0')          
					begin          
						update OpportunityMaster set numAssignedTo=0 ,numAssignedBy=0 where numOppId = @numOppId  and numDomainId= @numDomainID        
					end    
			END
		ELSE IF @vcDbColumnName='tintActive'
		 BEGIN
		 	SET @InlineEditValue= CASE @InlineEditValue WHEN 'True' THEN 1 ELSE 0 END 
		 END
		 ELSE IF @vcDbColumnName='tintSource'
		 BEGIN
			IF CHARINDEX('~', @InlineEditValue)>0
			BEGIN
				SET @Value1=SUBSTRING(@InlineEditValue, 1,CHARINDEX('~', @InlineEditValue)-1) 
				SET @Value2=SUBSTRING(@InlineEditValue, CHARINDEX('~', @InlineEditValue)+1,LEN(@InlineEditValue)) 
			END	
			ELSE
			BEGIN
				SET @Value1=@InlineEditValue
				SET @Value2=0
			END
			
		 	update OpportunityMaster set tintSourceType=@Value2 where numOppId = @numOppId  and numDomainId= @numDomainID        
		 	
		 	SET @InlineEditValue= @Value1
		 END
		ELSE IF @vcDbColumnName='tintOppStatus' --Update Inventory  0=Open,1=Won,2=Lost
		 BEGIN
			declare @tintOppStatus AS TINYINT
			select @tintOppStatus=tintOppStatus from OpportunityMaster where numOppID=@numOppID              

			if @tintOppStatus=0 and @InlineEditValue='1' --Open to Won
				BEGIN
					select @numDivisionID=numDivisionID from  OpportunityMaster where numOppID=@numOppID   
					DECLARE @tintCRMType AS TINYINT      
					select @tintCRMType=tintCRMType from divisionmaster where numDivisionID =@numDivisionID 

					-- Promote Lead to Account when Sales/Purchase Order is created against it
					if @tintCRMType=0 --Lead & Order
					begin        
						update divisionmaster set tintCRMType=2,bintLeadProm=getutcdate(),bintLeadPromBy=@numUserCntID,bintProsProm=getutcdate(),bintProsPromBy=@numUserCntID        
						where numDivisionID=@numDivisionID        
					end
					--Promote Prospect to Account
					else if @tintCRMType=1 
					begin        
						update divisionmaster set tintCRMType=2,bintProsProm=getutcdate(),bintProsPromBy=@numUserCntID        
						where numDivisionID=@numDivisionID        
					end    

		 			EXEC USP_UpdatingInventoryonCloseDeal @numOppID,@numUserCntID

					update OpportunityMaster set bintOppToOrder=GETUTCDATE() where numOppId=@numOppId and numDomainID=@numDomainID
				END
			if (@tintOppStatus=1 and @InlineEditValue='2') or (@tintOppStatus=1 and @InlineEditValue='0') --Won to Lost or Won to Open
				EXEC USP_RevertDetailsOpp @numOppID,0,@numUserCntID 
		 END
		 ELSE IF @vcDbColumnName='vcOppRefOrderNo' --Update vcRefOrderNo OpportunityBizDocs
		 BEGIN
			 IF LEN(ISNULL(@InlineEditValue,''))>0
			 BEGIN
	   			UPDATE dbo.OpportunityBizDocs SET vcRefOrderNo=@InlineEditValue WHERE numOppId=@numOppID
			 END
		 END
		 ELSE IF @vcDbColumnName='numStatus' 
		 BEGIN
		 	 IF EXISTS (SELECT 1 FROM dbo.OpportunityMaster WHERE numOppId = @numOppID AND tintOppType=1 AND isnull(tintOppStatus,0)=1 AND ISNULL(numStatus,0)!= @InlineEditValue)
			 BEGIN
	   				EXEC USP_ManageOpportunityAutomationQueue
							@numOppQueueID = 0, -- numeric(18, 0)
							@numDomainID = @numDomainID, -- numeric(18, 0)
							@numOppId = @numOppID, -- numeric(18, 0)
							@numOppBizDocsId = 0, -- numeric(18, 0)
							@numOrderStatus = @InlineEditValue, -- numeric(18, 0)
							@numUserCntID = @numUserCntID, -- numeric(18, 0)
							@tintProcessStatus = 1, -- tinyint
							@tintMode = 1 -- TINYINT
			END 
		 END	
		
		SET @strSql='update OpportunityMaster set ' + @vcDbColumnName + '=@InlineEditValue,numModifiedBy=@numUserCntID,bintModifiedDate=getutcdate() where numOppId=@numOppId and numDomainID=@numDomainID'
	 END
	 ELSE if @vcLookBackTableName = 'Cases' 
	 BEGIN 
	 		IF @vcDbColumnName='numAssignedTo' ---Updating if Case is assigned to someone  
			BEGIN
				select @tempAssignedTo=isnull(numAssignedTo,0) from Cases where numCaseId=@numCaseId and numDomainId= @numDomainID           
			
				if (@tempAssignedTo<>@InlineEditValue and  @InlineEditValue<>'0')          
					begin            
						update Cases set numAssignedTo=@InlineEditValue ,numAssignedBy=@numUserCntID where numCaseId = @numCaseId  and numDomainId= @numDomainID           
					end           
				else if  (@InlineEditValue ='0')          
					begin          
						update Cases set numAssignedTo=0 ,numAssignedBy=0 where numCaseId = @numCaseId  and numDomainId= @numDomainID        
					end    
			END
			
		SET @strSql='update Cases set ' + @vcDbColumnName + '=@InlineEditValue,numModifiedBy=@numUserCntID,bintModifiedDate=getutcdate() where numCaseId=@numCaseId and numDomainID=@numDomainID'
	 END 
	 ELSE if @vcLookBackTableName = 'Tickler' 
	 BEGIN 	
		SET @strSql='update Communication set ' + @vcOrigDbColumnName + '=@InlineEditValue,numModifiedBy=@numUserCntID,dtModifiedDate=getutcdate() where numCommId=@numCommId and numDomainID=@numDomainID'
	 END
	 
	 
	EXECUTE sp_executeSQL @strSql, N'@numDomainID NUMERIC(9),@numUserCntID NUMERIC(9),@numDivisionID NUMERIC(9),@numContactID NUMERIC(9),@InlineEditValue VARCHAR(8000),@numWOId NUMERIC(9),@numProId NUMERIC(9),@numOppId NUMERIC(9),@numCaseId NUMERIC(9),@numWODetailId NUMERIC(9),@numCommID Numeric(9)',
		     @numDomainID,@numUserCntID,@numDivisionID,@numContactID,@InlineEditValue,@numWOId,@numProId,@numOppId,@numCaseId,@numWODetailId,@numCommID;
		     
	IF @vcAssociatedControlType='SelectBox'                                                    
       BEGIN       
			IF @vcDbColumnName='tintSource' AND @vcLookBackTableName = 'OpportunityMaster'
			BEGIN
				SELECT dbo.fn_GetOpportunitySourceValue(@Value1,@Value2,@numDomainID) AS vcData                                             
			END
			else
				SELECT dbo.fn_getSelectBoxValue(@vcListItemType,@InlineEditValue,@vcDbColumnName) AS vcData                                             
		end 
		ELSE
		BEGIN
			 IF @vcDbColumnName='tintActive'
		  	 BEGIN
			 	SET @InlineEditValue= CASE @InlineEditValue WHEN 1 THEN 'True' ELSE 'False' END 
			 END

			IF @vcAssociatedControlType = 'Check box' or @vcAssociatedControlType = 'Checkbox'
			BEGIN
	 			SET @InlineEditValue= Case @InlineEditValue WHEN 'True' THEN 'Yes' ELSE 'No' END
			END
			
		    SELECT @InlineEditValue AS vcData
		END  
		
	IF @vcLookBackTableName = 'AdditionalContactsInformation' AND @vcDbColumnName = 'numECampaignID'
	BEGIN
		--Manage ECampiagn When Campaign is changed from inline edit
		DECLARE @Date AS DATETIME 
		SELECT @Date = dbo.[GetUTCDateWithoutTime]()
		DECLARE @numECampaignID AS NUMERIC(18,0)
		SET @numECampaignID = @InlineEditValue

		IF @numECampaignID = -1 
		SET @numECampaignID=0
	 
 		EXEC [USP_ManageConEmailCampaign]
		@numConEmailCampID = 0, --  numeric(9, 0)
		@numContactID = @numContactID, --  numeric(9, 0)
		@numECampaignID = @numECampaignID, --  numeric(9, 0)
		@intStartDate = @Date, --  datetime
		@bitEngaged = 1, --  bit
		@numUserCntID = @numUserCntID --  numeric(9, 0)
	END  
END 
ELSE
BEGIN	 

	SET @InlineEditValue=Cast(@InlineEditValue as varchar(1000))
	SELECT @vcAssociatedControlType=Fld_type
	 FROM CFW_Fld_Master WHERE Fld_Id=@numFormFieldId 

	IF @vcAssociatedControlType = 'CheckBox'
	BEGIN
	    SET @InlineEditValue=(case when @InlineEditValue='False' then 0 ELSE 1 END)
	END

	IF @vcPageName='organization'
	BEGIN 
		IF (SELECT COUNT(*) FROM CFW_Fld_Master CFM JOIN CFW_FLD_Values CFW ON CFM.Fld_id=CFW.Fld_id WHERE CFM.Fld_Id=@numFormFieldId AND CFW.RecId=@numRecId)>0
			BEGIN
				UPDATE CFW_FLD_Values SET Fld_Value=@InlineEditValue WHERE FldDTLID=(SELECT CFW.FldDTLID FROM CFW_Fld_Master CFM JOIN CFW_FLD_Values CFW ON CFM.Fld_id=CFW.Fld_id WHERE CFM.Fld_Id=@numFormFieldId AND CFW.RecId=@numRecId)
			END	
		ELSE
			BEGIN
				insert into CFW_FLD_Values (Fld_ID,Fld_Value,RecId) VALUES (@numFormFieldId,@InlineEditValue,@numRecId)         
			END

			--Added By :Sachin Sadhu ||Date:13thOct2014
			--Purpose  :To manage CustomFields in Queue
			EXEC USP_CFW_FLD_Values_CT
					@numDomainID =@numDomainID,
					@numUserCntID =@numUserCntID,
					@numRecordID =@numRecId ,
					@numFormFieldId  =@numFormFieldId
  
	  END
	ELSE IF @vcPageName='contact'
	BEGIN 
		IF (SELECT COUNT(*) FROM CFW_Fld_Master CFM JOIN CFW_FLD_Values_Cont CFW ON CFM.Fld_id=CFW.Fld_id WHERE CFM.Fld_Id=@numFormFieldId AND CFW.RecId=@numRecId)>0
			BEGIN
				UPDATE CFW_FLD_Values_Cont SET Fld_Value=@InlineEditValue WHERE FldDTLID=(SELECT CFW.FldDTLID FROM CFW_Fld_Master CFM JOIN CFW_FLD_Values_Cont CFW ON CFM.Fld_id=CFW.Fld_id WHERE CFM.Fld_Id=@numFormFieldId AND CFW.RecId=@numRecId)
			END	
		ELSE
			BEGIN
				insert into CFW_FLD_Values_Cont (Fld_ID,Fld_Value,RecId) VALUES (@numFormFieldId,@InlineEditValue,@numRecId)         
			END
			
			--Added By :Sachin Sadhu ||Date:13thOct2014
			--Purpose  :To manage CustomFields in Queue
			EXEC USP_CFW_FLD_Values_Cont_CT
					@numDomainID =@numDomainID,
					@numUserCntID =@numUserCntID,
					@numRecordID =@numRecId ,
					@numFormFieldId  =@numFormFieldId

	END
	ELSE IF @vcPageName='project'
	BEGIN 
		IF (SELECT COUNT(*) FROM CFW_Fld_Master CFM JOIN CFW_FLD_Values_Pro CFW ON CFM.Fld_id=CFW.Fld_id WHERE CFM.Fld_Id=@numFormFieldId AND CFW.RecId=@numRecId)>0
			BEGIN
				UPDATE CFW_FLD_Values_Pro SET Fld_Value=@InlineEditValue WHERE FldDTLID=(SELECT CFW.FldDTLID FROM CFW_Fld_Master CFM JOIN CFW_FLD_Values_Pro CFW ON CFM.Fld_id=CFW.Fld_id WHERE CFM.Fld_Id=@numFormFieldId AND CFW.RecId=@numRecId)
			END	
		ELSE
			BEGIN
				insert into CFW_FLD_Values_Pro (Fld_ID,Fld_Value,RecId) VALUES (@numFormFieldId,@InlineEditValue,@numRecId)         
			END

			
			--Added By :Sachin Sadhu ||Date:14thOct2014
			--Purpose  :To manage CustomFields in Queue
			EXEC USP_CFW_FLD_Values_Pro_CT
					@numDomainID =@numDomainID,
					@numUserCntID =@numUserCntID,
					@numRecordID =@numRecId ,
					@numFormFieldId  =@numFormFieldId
	END
	ELSE IF @vcPageName='opp'
	BEGIN 
		IF (SELECT COUNT(*) FROM CFW_Fld_Master CFM JOIN CFW_Fld_Values_Opp CFW ON CFM.Fld_id=CFW.Fld_id WHERE CFM.Fld_Id=@numFormFieldId AND CFW.RecId=@numRecId)>0
			BEGIN
				UPDATE CFW_Fld_Values_Opp SET Fld_Value=@InlineEditValue WHERE FldDTLID=(SELECT CFW.FldDTLID FROM CFW_Fld_Master CFM JOIN CFW_Fld_Values_Opp CFW ON CFM.Fld_id=CFW.Fld_id WHERE CFM.Fld_Id=@numFormFieldId AND CFW.RecId=@numRecId)
			END	
		ELSE
			BEGIN
				insert into CFW_Fld_Values_Opp (Fld_ID,Fld_Value,RecId) VALUES (@numFormFieldId,@InlineEditValue,@numRecId)         
			END

			--Added By :Sachin Sadhu ||Date:14thOct2014
			--Purpose  :To manage CustomFields in Queue
			EXEC USP_CFW_Fld_Values_Opp_CT
					@numDomainID =@numDomainID,
					@numUserCntID =@numUserCntID,
					@numRecordID =@numRecId ,
					@numFormFieldId  =@numFormFieldId

	END
	ELSE IF @vcPageName='case'
	BEGIN 
		IF (SELECT COUNT(*) FROM CFW_Fld_Master CFM JOIN CFW_FLD_Values_Case CFW ON CFM.Fld_id=CFW.Fld_id WHERE CFM.Fld_Id=@numFormFieldId AND CFW.RecId=@numRecId)>0
			BEGIN
				UPDATE CFW_FLD_Values_Case SET Fld_Value=@InlineEditValue WHERE FldDTLID=(SELECT CFW.FldDTLID FROM CFW_Fld_Master CFM JOIN CFW_FLD_Values_Case CFW ON CFM.Fld_id=CFW.Fld_id WHERE CFM.Fld_Id=@numFormFieldId AND CFW.RecId=@numRecId)
			END	
		ELSE
			BEGIN
				insert into CFW_FLD_Values_Case (Fld_ID,Fld_Value,RecId) VALUES (@numFormFieldId,@InlineEditValue,@numRecId)         
			END

			
			--Added By :Sachin Sadhu ||Date:14thOct2014
			--Purpose  :To manage CustomFields in Queue
			EXEC USP_CFW_FLD_Values_Case_CT
					@numDomainID =@numDomainID,
					@numUserCntID =@numUserCntID,
					@numRecordID =@numRecId ,
					@numFormFieldId  =@numFormFieldId
	END

	if @vcAssociatedControlType = 'SelectBox'            
	 begin            
		 select vcData from ListDetails WHERE numListItemID=@InlineEditValue            
	END
	ELSE IF @vcAssociatedControlType = 'CheckBox'
	BEGIN
	 	select CASE @InlineEditValue WHEN 1 THEN 'Yes' ELSE 'No' END AS vcData
	END
	ELSE
		BEGIN
			SELECT @InlineEditValue AS vcData
		END 
END

/****** Object:  StoredProcedure [dbo].[USP_InsertImapNewMails]    Script Date: 07/26/2008 16:19:07 ******/

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_insertimapnewmails')
DROP PROCEDURE usp_insertimapnewmails
GO
Create PROCEDURE [dbo].[USP_InsertImapNewMails]            
@numUserCntId as numeric(9),            
@numDomainID as numeric(9),            
@numUid NUMERIC(9),
@vcSubject VARCHAR(500),
@vcBody TEXT,
@vcBodyText text,
@dtReceivedOn datetime,
@vcSize varchar(50),
@bitIsRead bit,
@bitHasAttachments bit,
@vcFromName VARCHAR(5000),
@vcToName VARCHAR(5000),
@vcCCName VARCHAR(5000),
@vcBCCName VARCHAR(5000),
@vcAttachmentName VARCHAR(500),
@vcAttachmenttype VARCHAR(500),
@vcNewAttachmentName VARCHAR(500),
@vcAttachmentSize VARCHAR(500)
as             
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
SET CONCAT_NULL_YIELDS_NULL ON
SET NUMERIC_ROUNDABORT OFF
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON

                                        
DECLARE @Identity AS NUMERIC(9)              
            
if (select count(*) from emailhistory where numUserCntId = @numUserCntId             
   and numDomainid=@numDomainID  and numUid=@numUid)=0            
  begin      
             --Inser into Email Master
             SET @vcToName = @vcToName + '#^#'
             SET @vcFromName = @vcFromName + '#^#'
             SET @vcCCName = @vcCCName + '#^#'
             SET @vcBCCName = @vcBCCName + '#^#'

            -- PRINT 'ToName' + @ToName 
             EXECUTE USP_InsertEmailMaster @vcToName ,@numDomainID
             EXECUTE USP_InsertEmailMaster @vcFromName ,@numDomainID
             EXECUTE USP_InsertEmailMaster @vcCCName ,@numDomainID
             EXECUTE USP_InsertEmailMaster @vcBCCName ,@numDomainID 

             -- Insert Into Email History
                SET ARITHABORT ON
                INSERT  INTO EmailHistory
                        (
                          vcSubject,
                          vcBody,
                          vcBodyText,
                          bintCreatedOn,
                          bitHasAttachments,
                          vcItemId,
                          vcChangeKey,
                          bitIsRead,
                          vcSize,
                          chrSource,
                          tinttype,
                          vcCategory,
                          dtReceivedOn,
                          numDomainid,
                          numUserCntId,
                          numUid,IsReplied,
						  numNodeId
                        )
                VALUES  (
                          @vcSubject,
                          @vcBody,
                          dbo.fn_StripHTML(@vcBodyText),
                          GETUTCDATE(),
                          @bitHasAttachments,
                          '',
                          '',
                          @bitIsRead,
                          CONVERT(VARCHAR(200), @vcSize),
                          'I',
                          1,
                          '',
                          @dtReceivedOn,
                          @numDomainid,
                          @numUserCntId,
                          @numUid,0,
						  ISNULL((SELECT numNodeID FROM InboxTreeSort WHERE numDomainID = @numDomainID AND numUserCntID = @numUserCntId AND numFixID=1 AND ISNULL(bitSystem,0) = 1),0)
                        )                        
                SET @Identity = @@identity        
                

--				To increase performance of Select
                UPDATE  [EmailHistory]
                SET     vcFrom = dbo.fn_GetNewvcEmailString(@vcFromName,@numDomainID),	 --dbo.GetEmaillName(numEmailHstrID, 4),
                        [vcTo] = dbo.fn_GetNewvcEmailString(@vcToName,@numDomainID),    --dbo.GetEmaillName(numEmailHstrID, 1),
                        [vcCC] = dbo.fn_GetNewvcEmailString(@vcCCName,@numDomainID),	 --dbo.GetEmaillName(numEmailHstrID, 3)
                        [vcBCC]= dbo.fn_GetNewvcEmailString(@vcBCCName,@numDomainID)
                WHERE   [numEmailHstrID] = @Identity

				IF(SELECT vcFrom FROM EmailHistory WHERE [numEmailHstrID]=@Identity) IS NOT NULL 
				BEGIN
					update EmailHistory set numEmailId=(SELECT TOP 1 Item FROM dbo.DelimitedSplit8K(vcFrom,'$^$')) 
					where [numEmailHstrID]=@Identity	
				END

                IF @bitHasAttachments = 1 
                    BEGIN                    
                        EXEC USP_InsertAttachment @vcAttachmentName, '',
                            @vcAttachmentType, @Identity, @vcNewAttachmentName,@vcAttachmentSize                       
                    END 
    END            
GO
    
/****** Object:  StoredProcedure [dbo].[USP_InsertIntoEmailHistory]    Script Date: 07/26/2008 16:19:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created BY ANoop Jayaraj                          
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_insertintoemailhistory')
DROP PROCEDURE usp_insertintoemailhistory
GO
CREATE PROCEDURE [dbo].[USP_InsertIntoEmailHistory]                          
@vcMessageTo as varchar(5000),                     
@vcMessageToName as varchar(1000)='',              
@vcMessageFrom as varchar(5000),               
@vcMessageFromName as varchar(100)='',              
@vcSubject as varchar(500),                          
@vcBody as text,                       
@vcCC as varchar(5000),                        
@vcCCName as varchar(2000)='',                        
@vcBCC as varchar(5000)='',              
@bitHasAttachment as bit = 0,              
@vcItemId as varchar(1000)='',              
@vcChangeKey as varchar(1000)='',              
@bitIsRead as bit=1,              
@vcSize as numeric=0,              
@chrSource as char(1)= 'O',              
@tinttype as numeric =1,              
@vcCategory as varchar(100)='',          
@vcAttachmentType as varchar(500)= '',          
@vcFileName as varchar (500)= '',              
@vcAttachmentItemId as varchar(1000)=''  ,        
@dtReceived  as datetime  ,      
@numDomainId as numeric  ,    
@vcBodyText as text  ,  
@numUserCntId as numeric(9),
@IsReplied	BIT = FALSE,
@numRepliedMailID	NUMERIC(18) = 0,
@bitInvisible BIT = 0
as                    
declare @Identity as numeric(9)                
declare @FromName as varchar(1000)                
declare @FromEmailAdd as varchar(1000)                
declare @startPos as integer                
declare @EndPos as integer                
declare @EndPos1 as integer                
                
  set @startPos=CHARINDEX( '<', @vcMessageFrom)                
  set @EndPos=CHARINDEX( '>', @vcMessageFrom)                
  if @startPos>0                 
  begin                
   set @FromEmailAdd=  substring(@vcMessageFrom,@startPos+1,@EndPos-@startPos-1)                
   set @startPos=CHARINDEX( '"', @vcMessageFrom)                
   set @EndPos1=CHARINDEX( '"', substring(@vcMessageFrom,@startPos+1,len(@vcMessageFrom)))                
   set @FromName= substring(@vcMessageFrom,@startPos+1,@EndPos1-1)                
                   
                   
  end                 
  else                 
  begin                
   set @FromEmailAdd=@vcMessageFrom                
   set @FromName=@vcMessageFromName                
  end                
declare @updateinsert as bit     
    
if @vcItemId = ''    
begin    
 set @updateinsert  = 1    
end    
else    
begin    
 if (select count(*) from emailhistory  where vcItemId =@vcItemId) > 0    
  begin    
   set @updateinsert  = 0    
  end    
 else    
  begin    
   set @updateinsert  = 1    
  end    
end    
                  
if @updateinsert =0    
begin              
update EmailHistory            
set            
vcSubject=@vcSubject,            
vcBody=@vcBody,      
vcBodyText=dbo.fn_StripHTML(@vcBodyText),          
bintCreatedOn=getutcdate(),            
bitHasAttachments=@bitHasAttachment,            
vcItemId=@vcItemId,            
vcChangeKey=@vcChangeKey,            
bitIsRead=@bitIsRead,            
vcSize=convert(varchar(200),@vcSize),            
chrSource=@chrSource,            
tinttype=@tinttype,            
vcCategory =@vcCategory ,        
dtReceivedOn=@dtReceived --,IsReplied = @IsReplied           
where vcItemId = @vcItemId   and numDomainid= @numDomainId      
              
end                
else             
begin            
declare @node as NUMERIC(18,0)
set @node = case when @vcCategory = 'B' then ISNULL((SELECT numNodeID FROM InboxTreeSort WHERE numDomainID=@numDomainId AND numUserCntID=@numUserCntId AND bitSystem = 1 AND numFixID = 4),0) else 0 end 

SET @vcMessageTo = @vcMessageTo + '#^#'
SET @vcMessageFrom = @vcMessageFrom + '#^#'
SET @vcCC = @vcCC + '#^#'
SET @vcBCC = @vcBCC + '#^#'

EXECUTE dbo.USP_InsertEmailMaster @vcMessageTo ,@numDomainID
EXECUTE dbo.USP_InsertEmailMaster @vcMessageFrom ,@numDomainID
EXECUTE dbo.USP_InsertEmailMaster @vcCC ,@numDomainID
EXECUTE dbo.USP_InsertEmailMaster @vcBCC ,@numDomainID 

--IF @vcCategory = 'B'
--	SELECT @vcSize =DATALENGTH(@vcBody)
            
Insert into EmailHistory(vcSubject,vcBody,vcBodyText,bintCreatedOn,bitHasAttachments,vcItemId,vcChangeKey,bitIsRead,vcSize,chrSource,tinttype,vcCategory,dtReceivedOn,numDomainid,numUserCntId,numNodeId,vcFrom,vcTo,vcCC,vcBCC,IsReplied,bitInvisible)                          
values                          
(@vcSubject,@vcBody,dbo.fn_StripHTML(@vcBodyText),getutcdate(),@bitHasAttachment,@vcItemId,@vcChangeKey,@bitIsRead,convert(varchar(200),@vcSize),@chrSource,@tinttype,@vcCategory,@dtReceived,@numDomainid,@numUserCntId,@node,dbo.fn_GetNewvcEmailString(@vcMessageFrom,@numDomainId),dbo.fn_GetNewvcEmailString(@vcMessageTo,@numDomainId),dbo.fn_GetNewvcEmailString(@vcCC,@numDomainId),dbo.fn_GetNewvcEmailString(@vcBCC,@numDomainId),@IsReplied,@bitInvisible)              
set @Identity=@@identity                 

update EmailHistory set numEmailId=(SELECT TOP 1 Item FROM dbo.DelimitedSplit8K(vcFrom,'$^$'))
				where [numEmailHstrID]=@Identity                

update EmailHistory SET IsReplied = @IsReplied WHERE [numEmailHstrID] = @numRepliedMailID
                
--exec USP_InsertEmailAddCCAndBCC  @vcMessageTo,1,@Identity , @vcMessageToName               
--exec USP_InsertEmailAddCCAndBCC  @vcBCC,2,@Identity ,''               
--exec USP_InsertEmailAddCCAndBCC  @vcCC,3,@Identity  ,@vcCCName          
--exec USP_InsertEmailAddCCAndBCC  @vcMessageFrom,4,@Identity,  @vcMessageFromName      
  
  --No longer being used .. We are calling same function from front end to add attachment to email.. -- by chintan       
--if @bitHasAttachment  = 1          
--begin          
--exec USP_InsertAttachment   @vcFileName,@vcAttachmentItemId,@vcAttachmentType,@Identity           
--end       

SELECT ISNULL(@Identity,0)
end
GO
--exec USP_INVENTORYREPORT @numDomainID=72,@numUserCntID=1,@numDateRange=19,@numBasedOn=11,@numItemType=21,@numItemClass=22,@numItemClassId=0,@DateCondition=' OppDate between ''01/01/2008'' and ''11/23/2008'''
-- select * from financialyear
-- exec USP_InventoryReport @numDomainID=72,@numUserCntID=17,@numDateRange=19,@numBasedOn=9,@numItemType=21,@numItemClass=22,@numItemClassId=0,@DateCondition=' oppDate between ''01/01/2009 '' And '' 01/18/2010''',@numWareHouseId=0
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_INVENTORYREPORT')
DROP PROCEDURE USP_INVENTORYREPORT
GO
CREATE PROCEDURE [dbo].[USP_INVENTORYREPORT]
(@numDomainID as NUMERIC(9),
@numUserCntID as numeric(9),
@numDateRange as numeric(9),
@numBasedOn as numeric(9),
@numItemType as numeric(9),
@numItemClass as numeric(9),
@numItemClassId as numeric(9),
@DateCondition as varchar(100),
@numWareHouseId as NUMERIC,
@dtFromDate AS DATE,
@dtToDate AS DATE)
as
begin

DELETE FROM InventroyReportDTL WHERE numUserID=@numUserCntID;

INSERT INTO InventroyReportDTL SELECT @numDomainID,@numUserCntID,@numDateRange,0;
INSERT INTO InventroyReportDTL SELECT @numDomainID,@numUserCntID,@numBasedOn,0; 
INSERT INTO InventroyReportDTL SELECT @numDomainID,@numUserCntID,@numItemType,0;
INSERT INTO InventroyReportDTL SELECT @numDomainID,@numUserCntID,@numItemClass,@numItemClassId;



declare @FilterItemType varchar(MAX);
declare @FilterBasedOn varchar(MAX);
declare @FilterDateRange varchar(MAX);

declare @strSql varchar(MAX);
declare @strGroup varchar(MAX);
declare @ItemClassID varchar(100);
declare @strSort varchar(MAX);



select @FilterItemType= vcCondition from InventoryReportMaster a inner join InventroyReportDTL b on 
a.numReportID=b.numReportID and b.numUserID=@numUserCntID and a.numTypeID=1;

select @FilterBasedOn= vcCondition from InventoryReportMaster a inner join InventroyReportDTL b on 
a.numReportID=b.numReportID and b.numUserID=@numUserCntID and a.numTypeID=2;

select @FilterDateRange = vcDesc from InventoryReportMaster a inner join InventroyReportDTL b on 
a.numReportID=b.numReportID and b.numUserID=@numUserCntID and a.numTypeID=3;

select @ItemClassID = (case numItemClassification when 0 then '' else ' and a.numItemClassification=' + cast(numItemClassification as varchar(50))  end) from   InventroyReportDTL where numUserID=@numUserCntID 

--select @FinStartFrom= dtPeriodFrom  from financialyear where btCurrentyear=1 and numDomainID=@numDomainID

SELECT [WHIT].[numWareHouseItemID], SUM(ISNULL([WHIT].[numOnHand],0)) [numOnHand],SUM(ISNULL([WHIT].[monAverageCost],0)) AS [monAverageCost], (SUM(ISNULL([WHIT].[numOnHand],0)) * SUM(ISNULL([WHIT].[monAverageCost],0))) AS [IValue] 
INTO #ItemWareHouse 
FROM [dbo].[WareHouseItems_Tracking] AS WHIT  
JOIN [dbo].[WareHouseItems] AS WHI ON WHI.[numWareHouseItemID] = [WHIT].[numWareHouseItemID]
JOIN [dbo].[Item] AS I ON I.[numItemCode] = [WHI].[numItemID]
WHERE 1=1
AND [I].[numDomainID] = @numDomainID
--AND ISNULL([WHIT].[numWareHouseItemID],0) = 163841
AND [WHI].[numWareHouseID] = (CASE WHEN @numWareHouseId = 0 THEN [WHI].[numWareHouseID] ELSE @numWareHouseId END)
AND I.[charItemType] = (CASE WHEN @numItemType = 21 THEN I.[charItemType] 
							 WHEN @numItemType = 1 THEN 'S' 
							 WHEN @numItemType = 2 THEN 'P'
							 WHEN @numItemType = 3 THEN 'N'
							 WHEN @numItemType = 4 THEN 'A'
							 ELSE I.[charItemType] 
					    END)
AND ISNULL([WHI].[numOnHand],0) > 0
AND [WHIT].[dtCreatedDate] BETWEEN CONVERT(DATE,@dtFromDate) And CONVERT(DATE,@dtToDate)
GROUP BY [WHIT].[numWareHouseItemID]

--SELECT * FROM #ItemWareHouse 

create table #TempInventoryReport
(numItemCode numeric(18),
vcItemName varchar(2000),
vcModelID varchar(150),
ItemClass varchar (150),
ItemDesc varchar(MAX),
numOnHand numeric(18,2),
AVGCOST money,
InventoryValue MONEY,
numOnOrder numeric(18,2),
numAllocation numeric(18,2),
numBackOrder numeric(18,2),
Amount money,
SalesUnit numeric(18,2),
ReturnQty numeric(18,2),
ReturnPer numeric(18,2),
COGS money,
PROFIT money,
SALESRETURN MONEY);




set @strSql='select b.numItemCode,b.vcItemName,a.vcModelID,b.ItemClass,isnull(txtItemDesc,'''') as ItemDesc,
sum(isnull(IW.numOnHand,0)) as numOnHand, 
IW.monAverageCost AS AVGCOST,
(sum(isnull(IW.numOnHand,0)) * (IW.monAverageCost)) [InventoryValue],
sum(isnull(numOnOrder,0)) as numOnOrder,
sum(isnull(numAllocation,0)) as numAllocation, 
sum(isnull(numBackOrder,0))  as numBackOrder,
SUM(COGS) as Amount,
sum(SalesUnit) as SalesUnit,
sum(ReturnQty) as ReturnQty,
 isnull((sum(ReturnQty)/NULLIF(sum(SalesUnit),0)*100),0) as ReturnPer,
SUM(COGS) AS COGS,
SUM(PROFIT) AS PROFIT,
SUM(SALESRETURN) AS SALESRETURN
from  item A INNER JOIN WareHouseItems C ON C.numItemID=A.numItemCode  
JOIN #ItemWareHouse IW ON IW.[numWareHouseItemID] = C.[numWareHouseItemID]
and numWareHouseID = (case ' + cast(@numWareHouseId as varchar(10)) + '  when 0 then  numWareHouseID else '  + cast(@numWareHouseId as varchar(10)) + ' end) ' + @FilterItemType  +  @ItemClassID + '
inner JOIN VIEW_INVENTORYOPPSALES B ON  b.numItemCode = A.numItemCode
WHERE A.[numDomainID] = ' + CONVERT(VARCHAR(10),@numDomainID)

set @strGroup='group by b.numItemCode,b.vcItemName,a.vcModelID,b.ItemClass,isnull(txtItemDesc,''''),IW.monAverageCost'


--
--if @FilterDateRange='Last 30 days' 
--	begin
--		set @DateCondition ='OppDate between getdate() - 30 and getdate()'
--	end
--else if @FilterDateRange='Last 60 days' 
--	begin
--		set @DateCondition ='OppDate between getdate() - 60 and getdate()'
--	end
--else if @FilterDateRange='Last 90 days' 
--	begin
--		set @DateCondition ='OppDate between getdate() - 90 and getdate()'
--	end 
--else if @FilterDateRange='Month to date' 
--	begin
--		set @DateCondition ='OppDate between  dateadd(day,  -day(getdate())+1,getdate() )'
--	end
--else if @FilterDateRange='Quarter to date' 
--
--	begin
--		 if month(getdate()) <= 3 
--			begin
--				set @DateCondition = 'OppDate between ''' + @FinStartFrom + ''' and getdate()'
--			end
--		else if month(getdate()) >3 and month(getdate()) <=6
--			begin
--				set @DateCondition = 'OppDate between dateadd(month,3,''' + @FinStartFrom  + ''') and getdate()'
--			end
--		else if month(getdate()) >6 and month(getdate()) <=9
--			begin
--				set @DateCondition = 'OppDate between dateadd(month,6,''' + @FinStartFrom + ''') and getdate()'
--			end
--		else if month(getdate()) >9 and month(getdate()) <=12
--			begin
--				set @DateCondition = 'OppDate between dateadd(month,9,''' + @FinStartFrom + ''') and getdate()'
--			end
--	end
--else if @FilterDateRange='Year to date' 
--	begin
--		set @DateCondition = 'OppDate between ' + @FinStartFrom + ' and getdate()'
--	end
PRINT @DateCondition
if len(@DateCondition)>0
	set @strsql =@strsql +' AND '+ @DateCondition + ' '+ @strGroup
else
	set @strsql =@strsql + ' '+ @strGroup

insert into #TempInventoryReport
exec (@strsql)
PRINT @strSql
--print(@FilterBasedOn);

set @strSort='select * from #TempInventoryReport order by ' + @FilterBasedOn + ' desc'

exec (@strSort);


DROP TABLE #TempInventoryReport;
DROP TABLE #ItemWareHouse 

end
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_Item_GetChildMembership')
DROP PROCEDURE dbo.USP_Item_GetChildMembership
GO
CREATE PROCEDURE [dbo].[USP_Item_GetChildMembership]
    (
	  @numDomainID NUMERIC(18,0),
      @numItemCode NUMERIC(18,0)
    )
AS 
BEGIN
	SELECT 
		Item.vcItemName,
		CASE 
			WHEN ISNULL(Item.bitAssembly,0)=1 THEN 'Assembly' 
			WHEN ISNULL(Item.bitKitParent,0)=1 THEN 'Kit'
		END AS ParentItemType,
		CAST(ItemDetails.numQtyItemsReq AS VARCHAR(18)) AS numQtyItemsReq,
		Warehouses.vcWareHouse
	FROM 
		ItemDetails 
	INNER JOIN
		Item
	ON
		ItemDetails.numItemKitID = Item.numItemCode
	INNER JOIN
		WareHouseItems
	ON
		ItemDetails.numWareHouseItemId = WareHouseItems.numWareHouseItemID
	INNER JOIN
		Warehouses
	ON
		WareHouseItems.numWareHouseID = Warehouses.numWareHouseID
	WHERE 
		numChildItemID = @numItemCode
	UNION ALL
	SELECT
		ItemGroups.vcItemGroup AS vcItemName,
		'Item Group' AS ParentItemType,
		'' AS numQtyItemsReq,
		'' AS vcWareHouse
	FROM
		Item
	INNER JOIN
		ItemGroups
	ON
		Item.numItemGroup = ItemGroups.numItemGroupID
	WHERE
		Item.numDomainID = @numDomainID 
		AND numItemCode = @numItemCode
	UNION ALL
	SELECT 
		Item.vcItemName,
		'Related Items' AS ParentItemType,
		'' AS numQtyItemsReq,
		'' AS vcWareHouse
	FROM 
		SimilarItems 
	INNER JOIN
		Item
	ON
		SimilarItems.numParentItemCode = Item.numItemCode
	WHERE 
		SimilarItems.numDomainID = @numDomainID
		AND SimilarItems.numItemCode = @numItemCode
END
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_Item_GetDetails')
DROP PROCEDURE dbo.USP_Item_GetDetails
GO
CREATE PROCEDURE [dbo].[USP_Item_GetDetails]                                        
@numItemCode as numeric(9),
@numWarehouseItemID as NUMERIC(9),
@ClientTimeZoneOffset as int                                               
as                                                 
   
   DECLARE @bitItemIsUsedInOrder AS BIT;SET @bitItemIsUsedInOrder=0
   
   DECLARE @numDomainId AS NUMERIC
   SELECT @numDomainId=numDomainId FROM Item WHERE numItemCode=@numItemCode
   
   IF GETUTCDATE()<'2013-03-15 23:59:39'
		SET @bitItemIsUsedInOrder=0
   ELSE IF (SELECT COUNT(OI.numOppId) FROM dbo.OpportunityItems OI WHERE OI.numItemCode =@numItemCode) >0
		SET @bitItemIsUsedInOrder=1
   ELSE IF EXISTS(SELECT numItemID FROM dbo.General_Journal_Details WHERE numItemID= @numItemCode and numDomainId=@numDomainId AND chBizDocItems='IA1'   )
		set @bitItemIsUsedInOrder =1
                                               
	SELECT 
		I.numItemCode, 
		vcItemName, 
		ISNULL(txtItemDesc,'') txtItemDesc,
		CAST( ISNULL(IED.txtDesc,'')  AS VARCHAR(MAX) ) vcExtendedDescToAPI, 
		charItemType, 
		monListPrice,                   
		numItemClassification, 
		isnull(bitTaxable,0) as bitTaxable, 
		vcSKU AS vcSKU, 
		isnull(bitKitParent,0) as bitKitParent,--, dtDateEntered,                  
		numVendorID, I.numDomainID, numCreatedBy, bintCreatedDate, bintModifiedDate,                   
		numModifiedBy,
		(SELECT TOP 1 vcPathForImage FROM ItemImages WHERE numItemCode = @numItemCode AND bitDefault = 1 AND bitIsImage = 1 ) AS vcPathForImage ,
		(SELECT TOP 1 vcPathForTImage FROM ItemImages WHERE numItemCode = @numItemCode AND bitDefault = 1 AND bitIsImage = 1) AS  vcPathForTImage, isnull(bitSerialized,0) as bitSerialized, vcModelID,                   
		(SELECT numItemImageId,vcPathForImage,vcPathForTImage,bitDefault,case when bitdefault=1 then -1 
		else isnull(intDisplayOrder,0) end as intDisplayOrder 
		FROM ItemImages  
		WHERE numItemCode=@numItemCode order by case when bitdefault=1 then -1 else isnull(intDisplayOrder,0) end  asc 
		FOR XML AUTO,ROOT('Images')) AS xmlItemImages,
		(SELECT STUFF((SELECT ',' + CONVERT(VARCHAR(50) , numCategoryID)  FROM dbo.ItemCategory WHERE numItemID = I.numItemCode FOR XML PATH('')), 1, 1, '')) AS vcItemCategories ,
		numItemGroup, numCOGsChartAcntId, numAssetChartAcntId, numIncomeChartAcntId, monAverageCost,                   
		monCampaignLabourCost,dbo.fn_GetContactName(numCreatedBy)+' ,'+convert(varchar(20),DateAdd(minute, -@ClientTimeZoneOffset,bintCreatedDate )) as CreatedBy ,                                      
		dbo.fn_GetContactName(numModifiedBy)+' ,'+convert(varchar(20),DateAdd(minute, -@ClientTimeZoneOffset,bintModifiedDate )) as ModifiedBy,                      
		numOnHand as numOnHand,                      
		numOnOrder as numOnOrder,                      
		numReorder as numReorder,                      
		numAllocation as numAllocation,                      
		numBackOrder as numBackOrder,                   
		isnull(fltWeight,0) as fltWeight,                
		isnull(fltHeight,0) as fltHeight,                
		isnull(fltWidth,0) as fltWidth,                
		isnull(fltLength,0) as fltLength,                
		isnull(bitFreeShipping,0) as bitFreeShipping,              
		isnull(bitAllowBackOrder,0) as bitAllowBackOrder,              
		isnull(vcUnitofMeasure,'Units') as vcUnitofMeasure,      
		isnull(bitShowDeptItem,0) bitShowDeptItem,      
		isnull(bitShowDeptItemDesc,0) bitShowDeptItemDesc,  
		isnull(bitCalAmtBasedonDepItems ,0) bitCalAmtBasedonDepItems,
		isnull(bitAssembly ,0) bitAssembly ,
		isnull(cast(numBarCodeId as varchar(50)),'') as  numBarCodeId ,
		isnull(I.vcManufacturer,'') as vcManufacturer,
		ISNULL(I.numBaseUnit,0) AS numBaseUnit,ISNULL(I.numPurchaseUnit,0) AS numPurchaseUnit,ISNULL(I.numSaleUnit,0) AS numSaleUnit,
		dbo.fn_GetUOMName(ISNULL(I.numBaseUnit,0)) AS vcBaseUnit,dbo.fn_GetUOMName(ISNULL(I.numPurchaseUnit,0)) AS vcPurchaseUnit,dbo.fn_GetUOMName(ISNULL(I.numSaleUnit,0)) AS vcSaleUnit,
		isnull(bitLotNo,0) as bitLotNo,
		ISNULL(IsArchieve,0) AS IsArchieve,
		case when charItemType='P' then 'Inventory Item' when charItemType='S' then 'Service' when charItemType='A' then 'Accessory' when charItemType='N' then 'Non-Inventory Item' end as ItemType,
		case when ISNULL(bitAssembly,0)=1 then dbo.fn_GetAssemblyPossibleWOQty(I.numItemCode) else 0 end AS numWOQty,
		isnull(I.numItemClass,0) as numItemClass,
		ISNULL(tintStandardProductIDType,0) tintStandardProductIDType,
		ISNULL(vcExportToAPI,'') vcExportToAPI,
		ISNULL(numShipClass,0) numShipClass,ISNULL(I.bitAllowDropShip,0) AS bitAllowDropShip,
		ISNULL(@bitItemIsUsedInOrder,0) AS bitItemIsUsedInOrder,
		ISNULL((SELECT COUNT(*) FROM ItemDetails WHERE numChildItemID = I.numItemCode),0) AS intParentKitItemCount,
		ISNULL(I.bitArchiveItem,0) AS [bitArchiveItem],
		ISNULL(W.numWareHouseID,0) AS [numWareHouseID],
		ISNULL(W.numWareHouseItemID,0) AS [numWareHouseItemID],
		ISNULL((SELECT COUNT(WI.[numWareHouseItemID]) FROM [dbo].[WareHouseItems] WI WHERE WI.[numItemID] = I.numItemCode AND WI.[numItemID] = @numItemCode),0) AS [intTransCount],
		ISNULL(I.bitAsset,0) AS [bitAsset],
		ISNULL(I.bitRental,0) AS [bitRental]
	FROM 
		Item I       
	left join  
		WareHouseItems W                  
	on 
		W.numItemID=I.numItemCode AND
		W.numWareHouseItemID = @numWarehouseItemID           
	LEFT JOIN 
		ItemExtendedDetails IED   
	ON 
		I.numItemCode = IED.numItemCode               
	WHERE 
		I.numItemCode=@numItemCode 

GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_Item_SearchForSalesOrder' )
    DROP PROCEDURE USP_Item_SearchForSalesOrder
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Sandeep Patel
-- Create date: 3 March 2014
-- Description:	Search items for sales order
-- =============================================
CREATE PROCEDURE USP_Item_SearchForSalesOrder
    @tintOppType AS TINYINT ,
    @numDomainID AS NUMERIC(9) ,
    @numDivisionID AS NUMERIC(9) = 0 ,
    @str AS VARCHAR(1000) ,
    @numUserCntID AS NUMERIC(9) ,
    @tintSearchOrderCustomerHistory AS TINYINT = 0 ,
    @numPageIndex AS INT ,
    @numPageSize AS INT ,
    @WarehouseID AS NUMERIC(18, 0) = NULL ,
    @TotalCount AS INT OUTPUT
AS
    BEGIN
  SET NOCOUNT ON;

DECLARE @strNewQuery NVARCHAR(MAX)
DECLARE @strSQL AS NVARCHAR(MAX)
SELECT  @str = REPLACE(@str, '''', '''''')

DECLARE @TableRowCount TABLE ( Value INT );
	
SELECT  *
INTO    #Temp1
FROM    ( SELECT    numFieldId ,
                    vcDbColumnName ,
                    ISNULL(vcCultureFieldName, vcFieldName) AS vcFieldName ,
                    tintRow AS tintOrder ,
                    0 AS Custom
          FROM      View_DynamicColumns
          WHERE     numFormId = 22
                    AND numUserCntID = @numUserCntID
                    AND numDomainID = @numDomainID
                    AND tintPageType = 1
                    AND bitCustom = 0
                    AND ISNULL(bitSettingField, 0) = 1
                    AND numRelCntType = 0
          UNION
          SELECT    numFieldId ,
                    vcFieldName ,
                    vcFieldName ,
                    tintRow AS tintOrder ,
                    1 AS Custom
          FROM      View_DynamicCustomColumns
          WHERE     Grp_id = 5
                    AND numFormId = 22
                    AND numUserCntID = @numUserCntID
                    AND numDomainID = @numDomainID
                    AND tintPageType = 1
                    AND bitCustom = 1
                    AND numRelCntType = 0
        ) X 
  
IF NOT EXISTS ( SELECT  *
                FROM    #Temp1 )
    BEGIN
        INSERT  INTO #Temp1
                SELECT  numFieldId ,
                        vcDbColumnName ,
                        ISNULL(vcCultureFieldName, vcFieldName) AS vcFieldName ,
                        tintorder ,
                        0
                FROM    View_DynamicDefaultColumns
                WHERE   numFormId = 22
                        AND bitDefault = 1
                        AND ISNULL(bitSettingField, 0) = 1
                        AND numDomainID = @numDomainID 
    END

CREATE TABLE #tempItemCode ( numItemCode NUMERIC(9) )
DECLARE @bitRemoveVendorPOValidation AS BIT
SELECT  
	@bitRemoveVendorPOValidation = ISNULL(bitRemoveVendorPOValidation,0)
FROM    
	domain
WHERE  
	numDomainID = @numDomainID

IF @tintOppType > 0
    BEGIN 
        IF CHARINDEX(',', @str) > 0
        BEGIN
			DECLARE @itemSearchText VARCHAR(100)
            DECLARE @vcAttribureSearch VARCHAR(MAX)
			DECLARE @vcAttribureSearchCondition VARCHAR(MAX)

			SET @itemSearchText = LTRIM(RTRIM(SUBSTRING(@str,0,CHARINDEX(',',@str))))

			--GENERATES ATTRIBUTE SEARCH CONDITION
			SET @vcAttribureSearch = SUBSTRING(@str,CHARINDEX(',',@str) + 1,LEN(@str))
	
			DECLARE @attributeSearchText VARCHAR(MAX)
			DECLARE @TempTable TABLE (vcValue VARCHAR(MAX))

			WHILE LEN(@vcAttribureSearch) > 0
			BEGIN
				SET @attributeSearchText = LEFT(@vcAttribureSearch, 
										ISNULL(NULLIF(CHARINDEX(',', @vcAttribureSearch) - 1, -1),
										LEN(@vcAttribureSearch)))
				SET @vcAttribureSearch = SUBSTRING(@vcAttribureSearch,
												ISNULL(NULLIF(CHARINDEX(',', @vcAttribureSearch), 0),
												LEN(@vcAttribureSearch)) + 1, LEN(@vcAttribureSearch))

				INSERT INTO @TempTable (vcValue) VALUES ( @attributeSearchText )
			END
		
			--REMOVES WHITE SPACES
			UPDATE
				@TempTable
			SET
				vcValue = LTRIM(RTRIM(vcValue))
	
			--CONVERTING TABLE TO COMMA SEPERATED STRING AFTER REMOVEING WHITE SPACES
			SELECT @vcAttribureSearch = COALESCE(@vcAttribureSearch,'') +  CONVERT(VARCHAR(MAX),vcValue) + ',' FROM @TempTable
	
			--REMOVE LAST COMMA FROM FINAL SEARCH STRING
			SET @vcAttribureSearch = LTRIM(RTRIM(@vcAttribureSearch))
			IF DATALENGTH(@vcAttribureSearch) > 0
				SET @vcAttribureSearch = LEFT(@vcAttribureSearch, LEN(@vcAttribureSearch) - 1)
	
			--START: GENERATES ATTRIBUTE SEARCH CONDITION
			SELECT 	@vcAttribureSearchCondition = '(TEMPMATRIX.Attributes LIKE ''%' + REPLACE(@vcAttribureSearch, ',', '%'' AND TEMPMATRIX.Attributes LIKE ''%') + '%'')' 

			--LOGIC FOR GENERATING FINAL SQL STRING
			IF @tintOppType = 1 OR ( @bitRemoveVendorPOValidation = 1 AND @tintOppType = 2)
                BEGIN      

				SET @strSQL = 'SELECT
									I.numItemCode,
									I.numVendorID,
									MIN(WHT.numWareHouseItemID) as numWareHouseItemID,
									dbo.fn_GetAttributes(WHT.numWareHouseItemId,I.bitSerialized) AS [Attributes]
								FROM
									dbo.Item I
								LEFT JOIN
									dbo.WareHouseItems WHT
								ON
									I.numItemCode = WHT.numItemID
								WHERE 
									((ISNULL(I.numItemGroup,0) > 0 AND I.vcItemName LIKE ''%' + @itemSearchText + '%'') OR (ISNULL(I.numItemGroup,0) = 0 AND I.vcItemName LIKE ''%' + @str + '%''))
									AND ISNULL(I.Isarchieve, 0) <> 1
									AND I.charItemType NOT IN ( ''A'' )
									AND I.numDomainID = ' + CAST(@numDomainID AS VARCHAR(20)) + '
									AND (WHT.numWareHouseID = ' + CAST(ISNULL(@WarehouseID,0) AS VARCHAR(20)) + ' OR ' + CAST(ISNULL(@WarehouseID,0) AS VARCHAR(20)) + ' = 0)'

				--- added Asset validation  by sojan
                IF @bitRemoveVendorPOValidation = 1
                    AND @tintOppType = 2
                    SET @strSQL = @strSQL
                        + ' and ISNULL(I.bitKitParent,0) = 0 '

                IF @tintSearchOrderCustomerHistory = 1 AND @numDivisionID > 0
                BEGIN
                    SET @strSQL = @strSQL
                        + ' and I.numItemCode in (select distinct oppI.numItemCode from 
						OpportunityMaster oppM join OpportunityItems oppI on oppM.numOppId=oppI.numOppId where oppM.numDomainID='
                        + CONVERT(VARCHAR(20), @numDomainID)
                        + ' and oppM.numDivisionId='
                        + CONVERT(VARCHAR(20), @numDivisionId)
                        + ')'
                END

				SET @strSQL = @strSQL + ' GROUP BY
										I.numItemCode,
										I.numVendorID,
										dbo.fn_GetAttributes(numWareHouseItemId,I.bitSerialized)'

				SET @strSQL = 
				'
				SELECT    
					SRNO=ROW_NUMBER() OVER( ORDER BY  I.vcItemName),
					I.numItemCode,
					(SELECT TOP 1 vcPathForImage FROM ItemImages WHERE numItemCode = I.numItemCode) AS vcPathForImage ,
					(SELECT TOP 1 vcPathForTImage FROM ItemImages WHERE numItemCode = I.numItemCode) AS vcPathForTImage ,
					ISNULL(vcItemName, '''') vcItemName ,
					CASE WHEN I.[charItemType] = ''P''
					THEN CONVERT(VARCHAR(100), ISNULL((SELECT TOP 1 ROUND(monWListPrice,2) FROM [WareHouseItems] WHERE [numItemID] = I.numItemCode), 0))
					ELSE CONVERT(VARCHAR(100), ROUND(monListPrice, 2))
					END AS monListPrice,
					ISNULL(vcSKU, '''') vcSKU ,
					ISNULL(numBarCodeId, 0) numBarCodeId ,
					ISNULL(vcModelID, '''') vcModelID ,
					ISNULL(txtItemDesc, '''') AS txtItemDesc ,
					ISNULL(C.vcCompanyName, '''') AS vcCompanyName,
					TEMPMATRIX.numWareHouseItemID,
					TEMPMATRIX.Attributes AS [vcAttributes]
				FROM  
					(' + @strSQL +') AS TEMPMATRIX
				INNER JOIN    
					Item I
				ON
					TEMPMATRIX.numItemCode = I.numItemCode
				LEFT JOIN 
					DivisionMaster D 
				ON 
					I.numVendorID = D.numDivisionID
				LEFT JOIN 
					CompanyInfo C 
				ON 
					C.numCompanyID = D.numCompanyID
				WHERE  
				  ((ISNULL(I.numItemGroup,0) = 0 AND I.vcItemName LIKE ''%' + @str + '%'') OR ' + @vcAttribureSearchCondition + ')'

				

                INSERT  INTO @TableRowCount
                        EXEC
                            ( 'SELECT COUNT(*) FROM ( ' + @strSQL
                                + ') as t2'
                                    );

                SELECT  @TotalCount = Value
                FROM    @TableRowCount;

                SET @strNewQuery = 'select * from (' + @strSQL
                    + ') as t where SRNO> '
                    + CAST(( ( @numPageIndex - 1 ) * @numPageSize ) AS VARCHAR(10))
                    + ' and SRNO < '
                    + CAST(( ( @numPageIndex * @numPageSize ) + 1 ) AS VARCHAR(10))
                    + ' order by  vcItemName'

				--PRINT @strNewQuery
                EXEC(@strNewQuery)
				END
			ELSE
				BEGIN

				SET @strSQL = 'SELECT
									I.numItemCode,
									I.numVendorID,
									MIN(WHT.numWareHouseItemID) as numWareHouseItemID,
									dbo.fn_GetAttributes(WHT.numWareHouseItemId,I.bitSerialized) AS [Attributes]
								FROM
									dbo.Item I
								INNER JOIN
									dbo.Vendor V
								ON
									V.numItemCode = I.numItemCode
								LEFT JOIN
									dbo.WareHouseItems WHT
								ON
									I.numItemCode = WHT.numItemID
								WHERE 
									((ISNULL(I.numItemGroup,0) > 0 AND I.vcItemName LIKE ''%' + @itemSearchText + '%'') OR (ISNULL(I.numItemGroup,0) = 0 AND I.vcItemName LIKE ''%' + @str + '%''))
									AND ISNULL(I.Isarchieve, 0) <> 1
									AND I.charItemType NOT IN ( ''A'' )
									AND I.numDomainID = ' + CAST(@numDomainID AS VARCHAR(20)) + '
									AND (WHT.numWareHouseID = ' + CAST(ISNULL(@WarehouseID,0) AS VARCHAR(20)) + ' OR ' + CAST(ISNULL(@WarehouseID,0) AS VARCHAR(20)) + ' = 0)'

				--- added Asset validation  by sojan
                IF @tintSearchOrderCustomerHistory = 1 AND @numDivisionID > 0
                BEGIN
                    SET @strSQL = @strSQL
                        + ' and I.numItemCode in (select distinct oppI.numItemCode from 
						OpportunityMaster oppM join OpportunityItems oppI on oppM.numOppId=oppI.numOppId where oppM.numDomainID='
                        + CONVERT(VARCHAR(20), @numDomainID)
                        + ' and oppM.numDivisionId='
                        + CONVERT(VARCHAR(20), @numDivisionId)
                        + ')'
                END

				SET @strSQL = @strSQL + ' GROUP BY
										I.numItemCode,
										I.numVendorID,
										dbo.fn_GetAttributes(numWareHouseItemId,I.bitSerialized)'

				SET @strSQL = 
				'
				SELECT    
					SRNO=ROW_NUMBER() OVER( ORDER BY  I.vcItemName),
					I.numItemCode,
					(SELECT TOP 1 vcPathForImage FROM ItemImages WHERE numItemCode = I.numItemCode) AS vcPathForImage ,
					(SELECT TOP 1 vcPathForTImage FROM ItemImages WHERE numItemCode = I.numItemCode) AS vcPathForTImage ,
					ISNULL(vcItemName, '''') vcItemName ,
					CASE WHEN I.[charItemType] = ''P''
					THEN CONVERT(VARCHAR(100), ISNULL((SELECT TOP 1 ROUND(monWListPrice,2) FROM [WareHouseItems] WHERE [numItemID] = I.numItemCode), 0))
					ELSE CONVERT(VARCHAR(100), ROUND(monListPrice, 2))
					END AS monListPrice,
					ISNULL(vcSKU, '''') vcSKU ,
					ISNULL(numBarCodeId, 0) numBarCodeId ,
					ISNULL(vcModelID, '''') vcModelID ,
					ISNULL(txtItemDesc, '''') AS txtItemDesc ,
					ISNULL(C.vcCompanyName, '''') AS vcCompanyName,
					TEMPMATRIX.numWareHouseItemID,
					TEMPMATRIX.Attributes AS [vcAttributes]
				FROM  
					(' + @strSQL +') AS TEMPMATRIX
				INNER JOIN    
					Item I
				ON
					TEMPMATRIX.numItemCode = I.numItemCode
				LEFT JOIN 
					DivisionMaster D 
				ON 
					I.numVendorID = D.numDivisionID
				LEFT JOIN 
					CompanyInfo C 
				ON 
					C.numCompanyID = D.numCompanyID
				WHERE  
				 ((ISNULL(I.numItemGroup,0) = 0 AND I.vcItemName LIKE ''%' + @str + '%'') OR ' + @vcAttribureSearchCondition + ')'

                INSERT  INTO @TableRowCount
                        EXEC
                            ( 'SELECT COUNT(*) FROM ( ' + @strSQL
                                + ') as t2'
                                    );

                SELECT  @TotalCount = Value
                FROM    @TableRowCount;

                SET @strNewQuery = 'select * from (' + @strSQL
                    + ') as t where SRNO> '
                    + CAST(( ( @numPageIndex - 1 ) * @numPageSize ) AS VARCHAR(10))
                    + ' and SRNO < '
                    + CAST(( ( @numPageIndex * @numPageSize ) + 1 ) AS VARCHAR(10))
                    + ' order by  vcItemName'

				--PRINT @strNewQuery
                EXEC(@strNewQuery)
			END	
        END
        ELSE
        BEGIN
				DECLARE @vcWareHouseSearch VARCHAR(1000)
				DECLARE @vcVendorSearch VARCHAR(1000)
               

				SET @vcWareHouseSearch = ''
				SET @vcVendorSearch = ''

                DECLARE @tintOrder AS INT
                DECLARE @Fld_id AS NVARCHAR(20)
                DECLARE @Fld_Name AS VARCHAR(20)
                SET @strSQL = ''
                SELECT TOP 1
                        @tintOrder = tintOrder + 1 ,
                        @Fld_id = numFieldId ,
                        @Fld_Name = vcFieldName
                FROM    #Temp1
                WHERE   Custom = 1
                ORDER BY tintOrder
                WHILE @tintOrder > 0
                    BEGIN
                        SET @strSQL = @strSQL + ', dbo.GetCustFldValueItem('
                            + @Fld_id + ', I.numItemCode) as [' + @Fld_Name
                            + ']'

                        SELECT TOP 1
                                @tintOrder = tintOrder + 1 ,
                                @Fld_id = numFieldId ,
                                @Fld_Name = vcFieldName
                        FROM    #Temp1
                        WHERE   Custom = 1
                                AND tintOrder >= @tintOrder
                        ORDER BY tintOrder
                        IF @@rowcount = 0
                            SET @tintOrder = 0
                    END


				--Temp table for Item Search Configuration

                SELECT  *
                INTO    #tempSearch
                FROM    ( SELECT    numFieldId ,
                                    vcDbColumnName ,
                                    ISNULL(vcCultureFieldName, vcFieldName) AS vcFieldName ,
                                    tintRow AS tintOrder ,
                                    0 AS Custom
                          FROM      View_DynamicColumns
                          WHERE     numFormId = 22
                                    AND numUserCntID = @numUserCntID
                                    AND numDomainID = @numDomainID
                                    AND tintPageType = 1
                                    AND bitCustom = 0
                                    AND ISNULL(bitSettingField, 0) = 1
                                    AND numRelCntType = 1
                          UNION
                          SELECT    numFieldId ,
                                    vcFieldName ,
                                    vcFieldName ,
                                    tintRow AS tintOrder ,
                                    1 AS Custom
                          FROM      View_DynamicCustomColumns
                          WHERE     Grp_id = 5
                                    AND numFormId = 22
                                    AND numUserCntID = @numUserCntID
                                    AND numDomainID = @numDomainID
                                    AND tintPageType = 1
                                    AND bitCustom = 1
                                    AND numRelCntType = 1
                        ) X 
  
                IF NOT EXISTS ( SELECT  *
                                FROM    #tempSearch )
                    BEGIN
                        INSERT  INTO #tempSearch
                                SELECT  numFieldId ,
                                        vcDbColumnName ,
                                        ISNULL(vcCultureFieldName, vcFieldName) AS vcFieldName ,
                                        tintorder ,
                                        0
                                FROM    View_DynamicDefaultColumns
                                WHERE   numFormId = 22
                                        AND bitDefault = 1
                                        AND ISNULL(bitSettingField, 0) = 1
                                        AND numDomainID = @numDomainID 
                    END

				--Regular Search
                DECLARE @strSearch AS VARCHAR(8000) ,
                    @CustomSearch AS VARCHAR(4000) ,
                    @numFieldId AS NUMERIC(9)
                SET @strSearch = ''
                SELECT TOP 1
                        @tintOrder = tintOrder + 1 ,
                        @Fld_Name = vcDbColumnName ,
                        @numFieldId = numFieldId
                FROM    #tempSearch
                WHERE   Custom = 0
                ORDER BY tintOrder
                WHILE @tintOrder > -1
                    BEGIN
                        IF @Fld_Name = 'vcPartNo'
						BEGIN
                            INSERT  INTO #tempItemCode
                                    SELECT DISTINCT
                                            Item.numItemCode
                                    FROM    Vendor
                                            JOIN Item ON Item.numItemCode = Vendor.numItemCode
                                    WHERE   Item.numDomainID = @numDomainID
                                            AND Vendor.numDomainID = @numDomainID
                                            AND Vendor.vcPartNo IS NOT NULL
                                            AND LEN(Vendor.vcPartNo) > 0
                                            AND vcPartNo LIKE '%' + @str + '%'
							
							SET @vcVendorSearch = 'dbo.Vendor.vcPartNo LIKE ''%' + @str + '%'''
							
						END
                        ELSE IF @Fld_Name = 'vcBarCode'
						BEGIN
                            INSERT  INTO #tempItemCode
                                    SELECT DISTINCT
                                            Item.numItemCode
                                    FROM    Item
                                            JOIN WareHouseItems WI ON WI.numDomainID = @numDomainID
                                                            AND Item.numItemCode = WI.numItemID
                                            JOIN Warehouses W ON W.numDomainID = @numDomainID
                                                            AND W.numWareHouseID = WI.numWareHouseID
                                    WHERE   Item.numDomainID = @numDomainID
                                            AND ISNULL(Item.numItemGroup,
                                                        0) > 0
                                            AND WI.vcBarCode IS NOT NULL
                                            AND LEN(WI.vcBarCode) > 0
                                            AND WI.vcBarCode LIKE '%' + @str + '%'

							SET @vcWareHouseSearch = 'dbo.WareHouseItems.vcBarCode LIKE ''%' + @str + '%'''
						END
                        ELSE IF @Fld_Name = 'vcWHSKU'
						BEGIN
                            INSERT  INTO #tempItemCode
                                    SELECT DISTINCT
                                            Item.numItemCode
                                    FROM    Item
                                            JOIN WareHouseItems WI ON WI.numDomainID = @numDomainID
                                                        AND Item.numItemCode = WI.numItemID
                                            JOIN Warehouses W ON W.numDomainID = @numDomainID
                                                        AND W.numWareHouseID = WI.numWareHouseID
                                    WHERE   Item.numDomainID = @numDomainID
                                            AND ISNULL(Item.numItemGroup,
                                                        0) > 0
                                            AND WI.vcWHSKU IS NOT NULL
                                            AND LEN(WI.vcWHSKU) > 0
                                            AND WI.vcWHSKU LIKE '%'
                                            + @str + '%'

							IF LEN(@vcWareHouseSearch) > 0
								SET @vcWareHouseSearch = @vcWareHouseSearch + ' OR dbo.WareHouseItems.vcWHSKU LIKE ''%' + @str + '%'''
							ELSE
								SET @vcWareHouseSearch = 'dbo.WareHouseItems.vcWHSKU LIKE ''%' + @str + '%'''
						END
                        ELSE
                            SET @strSearch = @strSearch
                                + (CASE @Fld_Name WHEN 'numItemCode' THEN ' I.[' + @Fld_Name ELSE ' [' + @Fld_Name END)  + '] LIKE ''%' + @str
                                + '%'''

                        SELECT TOP 1
                                @tintOrder = tintOrder + 1 ,
                                @Fld_Name = vcDbColumnName ,
                                @numFieldId = numFieldId
                        FROM    #tempSearch
                        WHERE   tintOrder >= @tintOrder
                                AND Custom = 0
                        ORDER BY tintOrder
                        IF @@rowcount = 0
                            BEGIN
                                SET @tintOrder = -1
                            END
                        ELSE
                            IF @Fld_Name != 'vcPartNo'
                                AND @Fld_Name != 'vcBarCode'
                                AND @Fld_Name != 'vcWHSKU'
                                BEGIN
                                    SET @strSearch = @strSearch + ' or '
                                END
                    END

                IF ( SELECT COUNT(*)
                     FROM   #tempItemCode
                   ) > 0
                    SET @strSearch = @strSearch
                        + ' or  I.numItemCode in (select numItemCode from #tempItemCode)' 

				--Custom Search
                SET @CustomSearch = ''
                SELECT TOP 1
                        @tintOrder = tintOrder + 1 ,
                        @Fld_Name = vcDbColumnName ,
                        @numFieldId = numFieldId
                FROM    #tempSearch
                WHERE   Custom = 1
                ORDER BY tintOrder
                WHILE @tintOrder > -1
                    BEGIN
    
                        SET @CustomSearch = @CustomSearch
                            + CAST(@numFieldId AS VARCHAR(10)) 

                        SELECT TOP 1
                                @tintOrder = tintOrder + 1 ,
                                @Fld_Name = vcDbColumnName ,
                                @numFieldId = numFieldId
                        FROM    #tempSearch
                        WHERE   tintOrder >= @tintOrder
                                AND Custom = 1
                        ORDER BY tintOrder
                        IF @@rowcount = 0
                            BEGIN
                                SET @tintOrder = -1
                            END
                        ELSE
                            BEGIN
                                SET @CustomSearch = @CustomSearch + ' , '
                            END
                    END
	
                IF LEN(@CustomSearch) > 0
                    BEGIN
                        SET @CustomSearch = ' I.numItemCode in (SELECT RecId FROM dbo.CFW_FLD_Values_Item WHERE Fld_ID IN ('
                            + @CustomSearch + ') and Fld_Value like ''%'
                            + @str + '%'')'

                        IF LEN(@strSearch) > 0
                            SET @strSearch = @strSearch + ' OR '
                                + @CustomSearch
                        ELSE
                            SET @strSearch = @CustomSearch  
                    END


                IF @tintOppType = 1
                    OR ( @bitRemoveVendorPOValidation = 1
                         AND @tintOppType = 2
                       )
                    BEGIN      
                        SET @strSQL = 'select SRNO=ROW_NUMBER() OVER( ORDER BY  I.vcItemName), I.numItemCode,
									  (SELECT TOP 1 vcPathForImage from ItemImages WHERE numItemCode = I.numItemCode) as vcPathForImage,
									  (SELECT TOP 1 vcPathForTImage from ItemImages WHERE numItemCode = I.numItemCode) as vcPathForTImage,
									  isnull(vcItemName,'''') vcItemName,
									  CASE 
									  WHEN I.[charItemType]=''P''
									  THEN convert(varchar(100),ISNULL((SELECT TOP 1 ROUND(monWListPrice,2) FROM [WareHouseItems] WHERE [numItemID]=I.numItemCode),0)) 
									  ELSE 
										CONVERT(VARCHAR(100), ROUND(monListPrice, 2)) 
									  END AS monListPrice,
									  isnull(vcSKU,'''') vcSKU,
									  isnull(numBarCodeId,0) numBarCodeId,
									  isnull(vcModelID,'''') vcModelID,
									  isnull(txtItemDesc,'''') as txtItemDesc,
									  isnull(C.vcCompanyName,'''') as vcCompanyName,
									  WHT.numWareHouseItemID,
									  dbo.fn_GetAttributes(WHT.numWareHouseItemId,I.bitSerialized) AS [vcAttributes]'
									  + @strSQL
									  + ' from Item  I 
									  Left join DivisionMaster D              
									  on I.numVendorID=D.numDivisionID  
									  left join CompanyInfo C  
									  on C.numCompanyID=D.numCompanyID 
									  LEFT JOIN
											dbo.WareHouseItems WHT
									  ON
											WHT.numItemID = I.numItemCode AND
											WHT.numWareHouseItemID = (
																		SELECT 
																			TOP 1 numWareHouseItemID 
																		FROM 
																			dbo.WareHouseItems 
																		WHERE 
																			dbo.WareHouseItems.numDomainID = ' + CAST(@numDomainID AS VARCHAR(20)) + ' 
																			AND dbo.WareHouseItems.numItemID = I.numItemCode 
																			AND (dbo.WareHouseItems.numWareHouseID = ' +  CAST(ISNULL(@WarehouseID,0) AS VARCHAR(20))  + ' OR ' + CAST(ISNULL(@WarehouseID,0) AS VARCHAR(20)) + ' = 0)' +
																			CASE LEN(@vcWareHouseSearch)
																			WHEN 0 THEN ''
																			ELSE ' AND (' + @vcWareHouseSearch + ' OR 1=1)'
																			END
																			+
																	') 
										where ((I.charItemType <> ''P'') OR ((SELECT COUNT(*) FROM dbo.WareHouseItems WHERE numItemID = I.numItemCode AND numDomainID = '
									  + CONVERT(VARCHAR(20), @numDomainID)
									  + ' AND numWareHouseID =  ' +  CAST(ISNULL(@WarehouseID,0) AS VARCHAR(20))  + ' OR ' + CAST(ISNULL(@WarehouseID,0) AS VARCHAR(20)) + ' = 0) > 0)) AND ISNULL(I.Isarchieve,0) <> 1 And I.charItemType NOT IN(''A'') AND  I.numDomainID='
									  + CONVERT(VARCHAR(20), @numDomainID) + ' and ('
									  + @strSearch + ') '   
							      
						--- added Asset validation  by sojan
                        IF @bitRemoveVendorPOValidation = 1
                            AND @tintOppType = 2
                            SET @strSQL = @strSQL
                                + ' and ISNULL(I.bitKitParent,0) = 0 '

                        IF @tintSearchOrderCustomerHistory = 1
                            AND @numDivisionID > 0
                            BEGIN
                                SET @strSQL = @strSQL
                                    + ' and I.numItemCode in (select distinct oppI.numItemCode from 
									OpportunityMaster oppM join OpportunityItems oppI on oppM.numOppId=oppI.numOppId where oppM.numDomainID='
                                    + CONVERT(VARCHAR(20), @numDomainID)
                                    + ' and oppM.numDivisionId='
                                    + CONVERT(VARCHAR(20), @numDivisionId)
                                    + ')'
                            END

                        INSERT  INTO @TableRowCount
                                EXEC
                                    ( 'SELECT COUNT(*) FROM ( ' + @strSQL
                                      + ') as t2'
                                    );
                        SELECT  @TotalCount = Value
                        FROM    @TableRowCount;

                        SET @strNewQuery = 'select * from (' + @strSQL
                            + ') as t where SRNO> '
                            + CAST(( ( @numPageIndex - 1 ) * @numPageSize ) AS VARCHAR(10))
                            + ' and SRNO < '
                            + CAST(( ( @numPageIndex * @numPageSize ) + 1 ) AS VARCHAR(10))
                            + ' order by  vcItemName'
                        
						--PRINT @strNewQuery
						EXEC(@strNewQuery)
                    END      
                ELSE
                    IF @tintOppType = 2
                        BEGIN      
                            SET @strSQL = 'SELECT SRNO=ROW_NUMBER() OVER( ORDER BY  I.vcItemName), I.numItemCode,
										  (SELECT TOP 1 vcPathForImage from ItemImages WHERE numItemCode = I.numItemCode) as vcPathForImage,
										  (SELECT TOP 1 vcPathForTImage from ItemImages WHERE numItemCode = I.numItemCode) as vcPathForTImage,
										  ISNULL(vcItemName,'''') vcItemName,
										  convert(varchar(200),round(monListPrice,2)) as monListPrice,
										  isnull(vcSKU,'''') vcSKU,
										  isnull(numBarCodeId,0) numBarCodeId,
										  isnull(vcModelID,'''') vcModelID,
										  isnull(txtItemDesc,'''') as txtItemDesc,
										  isnull(C.vcCompanyName,'''') as vcCompanyName,
										  WHT.numWareHouseItemID,
										  dbo.fn_GetAttributes(WHT.numWareHouseItemId,I.bitSerialized) AS [vcAttributes]'
										  + @strSQL
										  + '  from item I 
										  INNER JOIN
											dbo.Vendor V
										  ON
											V.numItemCode = I.numItemCode
										  Left join 
											DivisionMaster D              
										  ON 
											V.numVendorID=D.numDivisionID  
										  LEFT join 
											CompanyInfo C  
										  ON 
											C.numCompanyID=D.numCompanyID    
										  LEFT JOIN
											dbo.WareHouseItems WHT
										  ON
											WHT.numItemID = I.numItemCode AND
											WHT.numWareHouseItemID = (
																		SELECT 
																			TOP 1 numWareHouseItemID 
																		FROM 
																			dbo.WareHouseItems 
																		WHERE 
																			dbo.WareHouseItems.numDomainID = ' + CAST(@numDomainID AS VARCHAR(20)) + ' 
																			AND dbo.WareHouseItems.numItemID = I.numItemCode 
																			AND (dbo.WareHouseItems.numWareHouseID = ' +  CAST(ISNULL(@WarehouseID,0) AS VARCHAR(20))  + ' OR ' + CAST(ISNULL(@WarehouseID,0) AS VARCHAR(20)) + ' = 0)' +
																			CASE LEN(@vcWareHouseSearch)
																			WHEN 0 THEN ''
																			ELSE ' AND (' + @vcWareHouseSearch  + ' OR 1=1)' 
																			END
																			+
																	')
										       
										  WHERE (
													(I.charItemType <> ''P'') OR 
													((SELECT COUNT(*) FROM dbo.WareHouseItems WHERE numItemID = I.numItemCode AND numDomainID = '
										 + CONVERT(VARCHAR(20), @numDomainID)
										 + ' AND numWareHouseID =  ' +  CAST(ISNULL(@WarehouseID,0) AS VARCHAR(20))  + ' OR ' + CAST(ISNULL(@WarehouseID,0) AS VARCHAR(20)) + ' = 0) > 0)) AND ISNULL(I.Isarchieve,0) <> 1 and ISNULL(I.bitKitParent,0) = 0 And I.charItemType NOT IN(''A'') AND V.numVendorID='
										 + CONVERT(VARCHAR(20), @numDivisionID)
										 + ' and ('+ CASE LEN(@vcVendorSearch)
																			WHEN 0 THEN ''
																			ELSE REPLACE(@vcVendorSearch,'dbo.Vendor','V') + ' OR '
																			END + @strSearch + ') ' 

							--- added Asset validation  by sojan
                            IF @tintSearchOrderCustomerHistory = 1
                                AND @numDivisionID > 0
                                BEGIN
                                    SET @strSQL = @strSQL
                                        + ' and I.numItemCode in (select distinct oppI.numItemCode from 
									OpportunityMaster oppM join OpportunityItems oppI on oppM.numOppId=oppI.numOppId where oppM.numDomainID='
                                        + CONVERT(VARCHAR(20), @numDomainID)
                                        + ' and oppM.numDivisionId='
                                        + CONVERT(VARCHAR(20), @numDivisionId)
                                        + ')'
                                END


                            INSERT  INTO @TableRowCount
                                    EXEC
                                        ( 'SELECT COUNT(*) FROM ( ' + @strSQL
                                          + ') as t2'
                                        );
                            SELECT  @TotalCount = Value
                            FROM    @TableRowCount;

                            SET @strNewQuery = 'select * from (' + @strSQL
                                + ') as t where SRNO> '
                                + CAST(( ( @numPageIndex - 1 ) * @numPageSize ) AS VARCHAR(10))
                                + ' and SRNO < '
                                + CAST(( ( @numPageIndex * @numPageSize ) + 1 ) AS VARCHAR(10))
                                + ' order by  vcItemName'
                            
							--PRINT @strNewQuery
							EXEC(@strNewQuery)
                        END
                    ELSE
                        SELECT  0  
            END
    END
ELSE
    SELECT  0  

SELECT  *
FROM    #Temp1
WHERE   vcDbColumnName NOT IN ( 'vcPartNo', 'vcBarCode', 'vcWHSKU' )ORDER BY tintOrder 
   


IF OBJECT_ID('dbo.Scores', 'U') IS NOT NULL
  DROP TABLE dbo.Scores

IF OBJECT_ID('tempdb..#Temp1') IS NOT NULL
BEGIN
    DROP TABLE #Temp1
END

IF OBJECT_ID('tempdb..#tempSearch') IS NOT NULL
BEGIN
    DROP TABLE #tempSearch
END

IF OBJECT_ID('tempdb..#tempItemCode') IS NOT NULL
BEGIN
    DROP TABLE #tempItemCode
END


    END
/*
declare @p7 int
set @p7=0
exec USP_ItemList1 @ItemClassification=0,@KeyWord='',@IsKit=0,@SortChar='0',@CurrentPage=1,@PageSize=10,@TotRecs=@p7 
output,@columnName='vcItemName',@columnSortOrder='Asc',@numDomainID=1,@ItemType=' ',@bitSerialized=0,@numItemGroup=0,@bitAssembly=0,@numUserCntID=1,@Where='',@IsArchive=0
select @p7
*/

GO
/****** Object:  StoredProcedure [dbo].[USP_ItemList1]    Script Date: 05/07/2009 18:14:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--- Created By Anoop Jayaraj                                        
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'usp_itemlist1' ) 
    DROP PROCEDURE usp_itemlist1
GO
CREATE PROCEDURE [dbo].[USP_ItemList1]
    @ItemClassification AS NUMERIC(9) = 0,
    @KeyWord AS VARCHAR(1000) = '',
    @IsKit AS TINYINT,
    @SortChar CHAR(1) = '0',
    @CurrentPage INT,
    @PageSize INT,
    @TotRecs INT OUTPUT, 
    @columnName AS VARCHAR(50),
    @columnSortOrder AS VARCHAR(10),
    @numDomainID AS NUMERIC(9) = 0,
    @ItemType AS CHAR(1),
    @bitSerialized AS BIT,
    @numItemGroup AS NUMERIC(9),
    @bitAssembly AS BIT,
    @numUserCntID AS NUMERIC(9),
    @Where	AS VARCHAR(MAX),
    @IsArchive AS BIT = 0,
    @byteMode AS TINYINT,
	@bitAsset as BIT=0,
	@bitRental as BIT=0
AS 
	
	 DECLARE @Nocolumns AS TINYINT               
    SET @Nocolumns = 0                
 
    SELECT  @Nocolumns = ISNULL(SUM(TotalRow), 0)
    FROM    ( SELECT    COUNT(*) TotalRow
              FROM      View_DynamicColumns
              WHERE     numFormId = 21
                        AND numUserCntID = @numUserCntID
                        AND numDomainID = @numDomainID
                        AND tintPageType = 1 AND ISNULL(numRelCntType,0)=0
              UNION
              SELECT    COUNT(*) TotalRow
              FROM      View_DynamicCustomColumns
              WHERE     numFormId = 21
                        AND numUserCntID = @numUserCntID
                        AND numDomainID = @numDomainID
                        AND tintPageType = 1 AND ISNULL(numRelCntType,0)=0
            ) TotalRows
               
 
 if @Nocolumns=0
BEGIN
INSERT INTO DycFormConfigurationDetails (numFormID,numFieldID,intColumnNum,intRowNum,numDomainID,numUserCntID,numRelCntType,tintPageType,bitCustom,intColumnWidth,numViewID)
select 21,numFieldId,0,Row_number() over(order by tintRow desc),@numDomainID,@numUserCntID,0,1,0,intColumnWidth,0
 FROM    View_DynamicDefaultColumns
                    WHERE   numFormId = 21
                            AND bitDefault = 1
                            AND ISNULL(bitSettingField, 0) = 1
                            AND numDomainID = @numDomainID
                    ORDER BY tintOrder ASC 
END

    CREATE TABLE #tempForm
        (
          tintOrder TINYINT,
          vcDbColumnName NVARCHAR(50),
          vcFieldName NVARCHAR(50),
          vcAssociatedControlType NVARCHAR(50),
          vcListItemType CHAR(3),
          numListID NUMERIC(9),
          vcLookBackTableName VARCHAR(50),
          bitCustomField BIT,
          numFieldId NUMERIC,
          bitAllowSorting BIT,
          bitAllowEdit BIT,
          bitIsRequired BIT,
          bitIsEmail BIT,
          bitIsAlphaNumeric BIT,
          bitIsNumeric BIT,
          bitIsLengthValidation BIT,
          intMaxLength INT,
          intMinLength INT,
          bitFieldMessage BIT,
          vcFieldMessage VARCHAR(500),
          ListRelID NUMERIC(9),
		  intColumnWidth INT
        )


          

            INSERT  INTO #tempForm
                    SELECT  tintRow + 1 AS tintOrder,
                            vcDbColumnName,
                            ISNULL(vcCultureFieldName, vcFieldName),
                            vcAssociatedControlType,
                            vcListItemType,
                            numListID,
                            vcLookBackTableName,
                            bitCustom,
                            numFieldId,
                            bitAllowSorting,
                            bitAllowEdit,
                            bitIsRequired,
                            bitIsEmail,
                            bitIsAlphaNumeric,
                            bitIsNumeric,
                            bitIsLengthValidation,
                            intMaxLength,
                            intMinLength,
                            bitFieldMessage,
                            vcFieldMessage vcFieldMessage,
                            ListRelID,
							intColumnWidth
                    FROM    View_DynamicColumns
                    WHERE   numFormId = 21
                            AND numUserCntID = @numUserCntID
                            AND numDomainID = @numDomainID
                            AND tintPageType = 1
                            AND ISNULL(bitSettingField, 0) = 1
                            AND ISNULL(bitCustom, 0) = 0
                            AND ISNULL(numRelCntType,0)=0
                    UNION
                    SELECT  tintRow + 1 AS tintOrder,
                            vcDbColumnName,
                            vcFieldName,
                            vcAssociatedControlType,
                            '' AS vcListItemType,
                            numListID,
                            '',
                            bitCustom,
                            numFieldId,
                            bitAllowSorting,
                            bitAllowEdit,
                            bitIsRequired,
                            bitIsEmail,
                            bitIsAlphaNumeric,
                            bitIsNumeric,
                            bitIsLengthValidation,
                            intMaxLength,
                            intMinLength,
                            bitFieldMessage,
                            vcFieldMessage,
                            ListRelID,
							intColumnWidth
                    FROM    View_DynamicCustomColumns
                    WHERE   numFormId = 21
                            AND numUserCntID = @numUserCntID
                            AND numDomainID = @numDomainID
                            AND tintPageType = 1
                            AND ISNULL(bitCustom, 0) = 1
                            AND ISNULL(numRelCntType,0)=0
                    ORDER BY tintOrder ASC      
            
    
            
	IF @byteMode=0
	BEGIN
		
	IF @columnName = 'OnHand' 
        SET @columnName = 'numOnHand'
    ELSE IF @columnName = 'Backorder' 
        SET @columnName = 'numBackOrder'
    ELSE IF @columnName = 'OnOrder' 
        SET @columnName = 'numOnOrder'
    ELSE IF @columnName = 'OnAllocation' 
        SET @columnName = 'numAllocation'
    ELSE IF @columnName = 'Reorder' 
        SET @columnName = 'numReorder'
    ELSE IF @columnName = 'monStockValue' 
        SET @columnName = 'sum(numOnHand) * Isnull(monAverageCost,0)'
    ELSE IF @columnName = 'ItemType' 
        SET @columnName = 'charItemType'

  
    DECLARE @column AS VARCHAR(50)              
    SET @column = @columnName              
    DECLARE @join AS VARCHAR(400)                  
    SET @join = ''           
    IF @columnName LIKE '%Cust%' 
        BEGIN                
			DECLARE @CustomFieldType AS VARCHAR(10)
			DECLARE @fldId AS VARCHAR(10)
			
			SET @fldId = REPLACE(@columnName, 'Cust', '')
			SELECT TOP 1 @CustomFieldType = ISNULL(vcAssociatedControlType,'') FROM View_DynamicCustomColumns WHERE numFieldID = @fldId
            
            IF ISNULL(@CustomFieldType,'') = 'SelectBox' OR ISNULL(@CustomFieldType,'') = 'ListBox'
				BEGIN
					SET @join = @join + ' left Join CFW_FLD_Values_Item CFW on CFW.RecId=item.numItemCode and CFW.fld_id= ' + @fldId + ' ' 
					SET @join = @join + ' left Join ListDetails LstCF on LstCF.numListItemID = CFW.Fld_Value  '            
					SET @columnName = ' LstCF.vcData ' 	
				END
			ELSE
				BEGIN
					SET @join = ' left Join CFW_FLD_Values_Item CFW on CFW.RecId=item.numItemCode and CFW.fld_id= ' + @fldId + ' '                                                         
					SET @columnName = 'CFW.Fld_Value'                	
				END
        END                                                
           
                                      
    DECLARE @firstRec AS INTEGER                                        
    DECLARE @lastRec AS INTEGER                                        
    SET @firstRec = ( @CurrentPage - 1 ) * @PageSize                                        
    SET @lastRec = ( @CurrentPage * @PageSize + 1 )   

    DECLARE @bitLocation AS BIT  
    SELECT  @bitLocation = ( CASE WHEN COUNT(*) > 0 THEN 1
                                  ELSE 0
                             END )
    FROM    View_DynamicColumns
    WHERE   numFormId = 21
            AND numUserCntID = @numUserCntID
            AND numDomainID = @numDomainID
            AND tintPageType = 1
            AND bitCustom = 0
            AND ISNULL(bitSettingField, 0) = 1
            AND vcDbColumnName = 'vcWareHouse'
                                                                   
                                        
    DECLARE @strSql AS VARCHAR(8000)
    DECLARE @strWhere AS VARCHAR(8000)
    SET @strWhere = ' AND 1=1'
    
      SET @strSql = '
declare @numDomain numeric
set @numDomain = ' + CONVERT(VARCHAR(15), @numDomainID) + ';
    INSERT INTO #tempItemList select numItemCode'
  
--    SET @strSql = @strSql + ' from item                     
        
        
    IF @bitAssembly = '1' 
        SET @strWhere = @strWhere + ' and bitAssembly= ' + CONVERT(VARCHAR(2), @bitAssembly) + ''  
	ELSE                                   
        SET @strWhere = @strWhere + ' and ISNULL(bitAssembly,0)=0 ' 

    IF @IsKit = '1' 
        SET @strWhere = @strWhere + ' and (bitAssembly=0 or  bitAssembly is null) and bitKitParent= ' + CONVERT(VARCHAR(2), @IsKit) + ''  
	ElSE
		 SET @strWhere = @strWhere + ' and ISNULL(bitKitParent,0)=0 '	             

    IF @ItemType = 'R' 
        SET @strWhere = @strWhere + ' and numItemClassification=(select numRentalItemClass from domain where numDomainId=' + CONVERT(VARCHAR(20), @numDomainID) + ')'            
    ELSE 
        IF @ItemType <> ''  
            SET @strWhere = @strWhere + ' and charItemType= ''' + @ItemType + ''''            

    IF @bitSerialized = 1 
        SET @strWhere = @strWhere + ' and (bitSerialized=1 or bitLotNo=1)'
	ElSE                                                   
        SET @strWhere = @strWhere + ' and ISNULL(bitSerialized,0)=0 and ISNULL(bitLotNo,0)=0 '

    IF @SortChar <> '0' 
        SET @strWhere = @strWhere + ' and vcItemName like ''' + @SortChar + '%'''                                       
        
    IF @ItemClassification <> '0' 
        SET @strWhere = @strWhere + ' and numItemClassification=' + CONVERT(VARCHAR(15), @ItemClassification)    
    
    IF @IsArchive = 0 
        SET @strWhere = @strWhere + ' AND ISNULL(Item.IsArchieve,0) = 0'       
		
	IF @bitAsset = 1 
		BEGIN
			SET @strWhere = @strWhere + ' AND ISNULL(Item.bitAsset,0) = 1'
		END
    ELSE
		BEGIN
			SET @strWhere = @strWhere + ' AND ISNULL(Item.bitAsset,0) = 0'
		END

	IF @bitRental = 1
	BEGIN
		SET @strWhere = @strWhere + ' AND ISNULL(Item.bitRental,0) = 1 AND ISNULL(Item.bitAsset,0) = 1 '
	END	
	ELSE
	BEGIN
		SET @strWhere = @strWhere + ' AND ISNULL(Item.bitRental,0) = 0'
	END
			  
		  
		

    IF @KeyWord <> '' 
        BEGIN 
            IF CHARINDEX('vcCompanyName', @KeyWord) > 0 
                BEGIN
                    SET @strWhere = @strWhere + ' and item.numItemCode in (select Vendor.numItemCode from Vendor join 
											  divisionMaster div on div.numdivisionid=Vendor.numVendorid 
											  join companyInfo com  on com.numCompanyID=div.numCompanyID  
											  WHERE Vendor.numDomainID=' + CONVERT(VARCHAR(15), @numDomainID) + '  
											  and Vendor.numItemCode= item.numItemCode and com.' + @KeyWord + ')' 
											  
					SET @join = @join + ' LEFT JOIN divisionmaster div on numVendorid=div.numDivisionId '   		
                END
            ELSE 
                SET @strWhere = @strWhere + ' and ' + @KeyWord 
        END
        
    IF @numItemGroup > 0 
        SET @strWhere = @strWhere + ' and numItemGroup = ' + CONVERT(VARCHAR(20), @numItemGroup)    
               
    IF @numItemGroup = -1 
        SET @strWhere = @strWhere + ' and numItemGroup in (select numItemGroupID from ItemGroups where numDomainID=' + CONVERT(VARCHAR(20), @numDomainID) + ')'                                      
        
    SET @strWhere = @strWhere + ISNULL(@Where,'') + ' group by numItemCode '  
  
    IF @bitLocation = 1 
        SET @strWhere = @strWhere + '  ,vcWarehouse'
        
        SET @join = @join + ' LEFT JOIN WareHouseItems ON numItemID = numItemCode '
        SET @join = @join + ' LEFT JOIN Warehouses W ON W.numWareHouseID = WareHouseItems.numWareHouseID '
        
    IF @columnName <> 'sum(numOnHand) * Isnull(monAverageCost,0)' 
        BEGIN
            SET @strWhere = @strWhere + ',' + @columnName   
            
            IF CHARINDEX('LEFT JOIN WareHouseItems',@join) = 0
            BEGIN
				SET @join = @join + ' LEFT JOIN WareHouseItems ON numItemID = numItemCode '
			END	
        END
    
  
    SET @strSql = @strSql + ' FROM Item  ' + @join + '   
	LEFT JOIN ListDetails LD ON LD.numListItemID = Item.numShipClass
	LEFT JOIN ItemCategory IC ON IC.numItemID = Item.numItemCode
	WHERE Item.numDomainID= @numDomain
	AND ( WareHouseItems.numDomainID = ' + CONVERT(VARCHAR(15), @numDomainID) + ' OR WareHouseItems.numWareHouseItemID IS NULL) ' + @strWhere
    
    SET @strSql = @strSql + ' ORDER BY ' + @columnName + ' ' + @columnSortOrder 
 
	PRINT @strSql

	CREATE TABLE #tempItemList (RowNo INT IDENTITY(1,1) NOT NULL,numItemCode NUMERIC(18,0))
	 EXEC (@strSql)
	DELETE FROM #tempItemList WHERE RowNo NOT IN (SELECT MIN(RowNo) FROM #tempItemList GROUP BY numItemCode)

	CREATE TABLE #tempItemList1 (RunningCount INT IDENTITY(1,1) NOT NULL,numItemCode NUMERIC(18,0),TotalRowCount INT)
	INSERT INTO #tempItemList1 (numItemCode) SELECT numItemCode FROM #tempItemList
	UPDATE #tempItemList1 SET TotalRowCount=(SELECT COUNT(*) FROM #tempItemList1)
	
	SET @strSql = ' select                    
 min(RunningCount) as RunningCount,
 min(TotalRowCount) as TotalRowCount,I.numItemCode,I.vcItemName,I.txtItemDesc,I.charItemType,                                     
case when charItemType=''P'' then 
case 
when ISNULL(bitAssembly,0) = 1 then ''Assembly''
when ISNULL(bitKitParent,0) = 1 then ''Kit''
when ISNULL(bitAsset,0) = 1 AND ISNULL(bitRental,0)=0 then ''Asset''
when ISNULL(bitAsset,0) = 1 AND ISNULL(bitRental,0)=1 then ''Rental Asset''
when ISNULL(bitSerialized,0) = 1 AND ISNULL(bitAsset,0) = 0  then ''Serialized''
when ISNULL(bitSerialized,0) = 1 AND ISNULL(bitAsset,0) = 1 AND ISNULL(bitRental,0)=0 then ''Serialized Asset''
when ISNULL(bitSerialized,0) = 1 AND ISNULL(bitAsset,0) = 1 AND ISNULL(bitRental,0)=1 then ''Serialized Rental Asset''
when ISNULL(bitLotNo,0)=1 THEN ''Lot #''
else ''Inventory Item'' end
when charItemType=''N'' then ''Non Inventory Item'' when charItemType=''S'' then ''Service'' when charItemType=''A'' then ''Accessory'' end as ItemType,                                      
CASE WHEN bitKitParent = 1 THEN ISNULL(dbo.fn_GetKitInventory(I.numItemCode),0) ELSE isnull(sum(numOnHand),0) END  as numOnHand,                  
isnull(sum(numOnOrder),0) as numOnOrder,                                      
isnull(sum(numReorder),0) as numReorder,                  
isnull(sum(numBackOrder),0) as numBackOrder,                  
isnull(sum(numAllocation),0) as numAllocation,                  
vcCompanyName,  
CAST(ISNULL(monAverageCost,0) AS DECIMAL(10,2)) AS monAverageCost ,
CAST(ISNULL(monAverageCost,0) * SUM(numOnHand) AS DECIMAL(10,2)) AS monStockValue,
ISNULL((select vcItemGroup from ItemGroups where numItemGroupID = I.numItemGroup),'''') AS numItemGroup,
I.vcModelID,I.monListPrice,I.vcManufacturer,I.numBarCodeId,I.fltLength,I.fltWidth,I.fltHeight,I.fltWeight,I.vcSKU,LD.vcData AS numShipClass ' 

   IF @bitLocation = 1 
        SET @strSql = @strSql + '  ,vcWarehouse'  

SET @strSql = @strSql + ' INTO #tblItem FROM #tempItemList1 as tblItem JOIN ITEM I ON tblItem.numItemCode=I.numItemCode 
left join WareHouseItems on numItemID=I.numItemCode                                  
left join divisionmaster div  on I.numVendorid=div.numDivisionId                                    
left join companyInfo com on com.numCompanyid=div.numcompanyID   
left join Warehouses W  on W.numWareHouseID=WareHouseItems.numWareHouseID 
LEFT JOIN ListDetails LD ON LD.numListItemID = i.numShipClass
    where  RunningCount >' + CONVERT(VARCHAR(15), @firstRec)
        + ' and RunningCount < ' + CONVERT(VARCHAR(15), @lastRec)
          + ' group by I.numItemCode,vcItemName,txtItemDesc,charItemType,vcCompanyName,vcModelID,monListPrice,vcManufacturer,numBarCodeId,fltLength,fltWidth,fltHeight,fltWeight,monAverageCost,I.vcSKU,LD.vcData,bitKitParent,bitAssembly,bitAsset,bitRental,bitSerialized,bitLotNo,numItemGroup '  
          
              IF @bitLocation = 1 
        SET @strSql = @strSql + '  ,vcWarehouse'  
        
        SET @strSql = @strSql + ' order by RunningCount '   
          
    DECLARE @tintOrder AS TINYINT                                                  
    DECLARE @vcFieldName AS VARCHAR(50)                                                  
    DECLARE @vcListItemType AS VARCHAR(3)                                             
    DECLARE @vcListItemType1 AS VARCHAR(1)                                                 
    DECLARE @vcAssociatedControlType VARCHAR(10)                                                  
    DECLARE @numListID AS NUMERIC(9)                                                  
    DECLARE @vcDbColumnName VARCHAR(20)                      
    DECLARE @WhereCondition VARCHAR(2000)                       
    DECLARE @vcLookBackTableName VARCHAR(2000)                
    DECLARE @bitCustom AS BIT                  
    DECLARE @numFieldId AS NUMERIC  
    DECLARE @bitAllowSorting AS CHAR(1)   
    DECLARE @bitAllowEdit AS CHAR(1)                   
                 
    SET @tintOrder = 0                                                  
    SET @WhereCondition = ''                 
                   
   
   
    SET @strSql = @strSql
        + ' select TotalRowCount,RunningCount,temp.numItemCode,vcItemName,txtItemDesc,charItemType,ItemType,numOnHand, LD.vcData AS numShipClass,                  
				   numOnOrder,numReorder,numBackOrder,numAllocation,vcCompanyName,vcModelID,monListPrice,vcManufacturer,numBarCodeId,fltLength,fltWidth,fltHeight,
				   fltWeight,(Select SUM(WO.numQtyItemsReq) from WorkOrder WO where WO.numItemCode=temp.numItemCode and WO.numWOStatus=0) as WorkOrder,
				   monAverageCost, monStockValue,vcSKU, numItemGroup'     
  
    IF @bitLocation = 1 
        SET @strSql = @strSql + '  ,vcWarehouse'                                               

    DECLARE @ListRelID AS NUMERIC(9) 
  
    SELECT TOP 1
            @tintOrder = tintOrder + 1,
            @vcDbColumnName = vcDbColumnName,
            @vcFieldName = vcFieldName,
            @vcAssociatedControlType = vcAssociatedControlType,
            @vcListItemType = vcListItemType,
            @numListID = numListID,
            @vcLookBackTableName = vcLookBackTableName,
            @bitCustom = bitCustomField,
            @numFieldId = numFieldId,
            @bitAllowSorting = bitAllowSorting,
            @bitAllowEdit = bitAllowEdit,
            @ListRelID = ListRelID
    FROM    #tempForm --WHERE bitCustomField=1
    ORDER BY tintOrder ASC            

    WHILE @tintOrder > 0                                                  
        BEGIN                                                  
            IF @bitCustom = 0
               BEGIN
               PRINT @vcDbColumnName
			   		IF @vcDbColumnName = 'vcPathForTImage'
		   			BEGIN
					   	SET @strSql = @strSql + ',ISNULL(II.vcPathForTImage,'''') AS [vcPathForTImage]'                   
                        SET @WhereCondition = @WhereCondition + ' LEFT JOIN ItemImages II ON II.numItemCode = temp.numItemCode AND bitDefault = 1'
					END
			   END                              
			   
            ELSE IF @bitCustom = 1 
                BEGIN      
                  
                    SELECT  @vcFieldName = FLd_label,
                            @vcAssociatedControlType = fld_type,
                            @vcDbColumnName = 'Cust'
                            + CONVERT(VARCHAR(10), Fld_Id)
                    FROM    CFW_Fld_Master
                    WHERE   CFW_Fld_Master.Fld_Id = @numFieldId                 
     
              
--    print @vcAssociatedControlType                
                    IF @vcAssociatedControlType = 'TextBox'
                        OR @vcAssociatedControlType = 'TextArea' 
                        BEGIN                
                   
                            SET @strSql = @strSql + ',CFW'  + CONVERT(VARCHAR(3), @tintOrder)  + '.Fld_Value  [' + @vcFieldName + '~' + @vcDbColumnName + ']'                   
                            SET @WhereCondition = @WhereCondition + ' left Join CFW_FLD_Values_Item CFW' + CONVERT(VARCHAR(3), @tintOrder)
                                + '                 
    on CFW' + CONVERT(VARCHAR(3), @tintOrder) + '.Fld_Id='
                                + CONVERT(VARCHAR(10), @numFieldId)
                                + 'and CFW' + CONVERT(VARCHAR(3), @tintOrder)
                                + '.RecId=temp.numItemCode   '                                                         
                        END   
                    ELSE 
                        IF @vcAssociatedControlType = 'CheckBox' 
                            BEGIN            
               
                                SET @strSql = @strSql
                                    + ',case when isnull(CFW'
                                    + CONVERT(VARCHAR(3), @tintOrder)
                                    + '.Fld_Value,0)=0 then ''No'' when isnull(CFW'
                                    + CONVERT(VARCHAR(3), @tintOrder)
                                    + '.Fld_Value,0)=1 then ''Yes'' end   ['
                                    + @vcFieldName + '~' + @vcDbColumnName
                                    + ']'              
 
                                SET @WhereCondition = @WhereCondition
                                    + ' left Join CFW_FLD_Values_Item CFW'
                                    + CONVERT(VARCHAR(3), @tintOrder)
                                    + '             
    on CFW' + CONVERT(VARCHAR(3), @tintOrder) + '.Fld_Id='
                                    + CONVERT(VARCHAR(10), @numFieldId)
                                    + 'and CFW'
                                    + CONVERT(VARCHAR(3), @tintOrder)
                                    + '.RecId=temp.numItemCode   '                                                     
                            END                
                        ELSE 
                            IF @vcAssociatedControlType = 'DateField' 
                                BEGIN              
                   
                                    SET @strSql = @strSql
                                        + ',dbo.FormatedDateFromDate(CFW'
                                        + CONVERT(VARCHAR(3), @tintOrder)
                                        + '.Fld_Value,'
                                        + CONVERT(VARCHAR(10), @numDomainId)
                                        + ')  [' + @vcFieldName + '~'
                                        + @vcDbColumnName + ']'                   
                                    SET @WhereCondition = @WhereCondition
                                        + ' left Join CFW_FLD_Values_Item CFW'
                                        + CONVERT(VARCHAR(3), @tintOrder)
                                        + '                 
    on CFW' + CONVERT(VARCHAR(3), @tintOrder) + '.Fld_Id='
                                        + CONVERT(VARCHAR(10), @numFieldId)
                                        + 'and CFW'
                                        + CONVERT(VARCHAR(3), @tintOrder)
                                        + '.RecId=temp.numItemCode   '                                                         
                                END                
                            ELSE 
                                IF @vcAssociatedControlType = 'SelectBox' 
                                    BEGIN                
                                        SET @vcDbColumnName = 'Cust'
                                            + CONVERT(VARCHAR(10), @numFieldId)                
                                        SET @strSql = @strSql + ',L'
                                            + CONVERT(VARCHAR(3), @tintOrder)
                                            + '.vcData' + ' [' + @vcFieldName
                                            + '~' + @vcDbColumnName + ']'                                                          
                                        SET @WhereCondition = @WhereCondition
                                            + ' left Join CFW_FLD_Values_Item CFW'
                                            + CONVERT(VARCHAR(3), @tintOrder)
                                            + '                 
     on CFW' + CONVERT(VARCHAR(3), @tintOrder) + '.Fld_Id='
                                            + CONVERT(VARCHAR(10), @numFieldId)
                                            + 'and CFW'
                                            + CONVERT(VARCHAR(3), @tintOrder)
                                            + '.RecId=temp.numItemCode    '                                                         
                                        SET @WhereCondition = @WhereCondition
                                            + ' left Join ListDetails L'
                                            + CONVERT(VARCHAR(3), @tintOrder)
                                            + ' on L'
                                            + CONVERT(VARCHAR(3), @tintOrder)
                                            + '.numListItemID=CFW'
                                            + CONVERT(VARCHAR(3), @tintOrder)
                                            + '.Fld_Value'                
                                    END                 
                END          
  
            SELECT TOP 1
                    @tintOrder = tintOrder + 1,
                    @vcDbColumnName = vcDbColumnName,
                    @vcFieldName = vcFieldName,
                    @vcAssociatedControlType = vcAssociatedControlType,
                    @vcListItemType = vcListItemType,
                    @numListID = numListID,
                    @vcLookBackTableName = vcLookBackTableName,
                    @bitCustom = bitCustomField,
                    @numFieldId = numFieldId,
                    @bitAllowSorting = bitAllowSorting,
                    @bitAllowEdit = bitAllowEdit,
                    @ListRelID = ListRelID
            FROM    #tempForm
            WHERE   tintOrder > @tintOrder - 1 --AND bitCustomField=1
            ORDER BY tintOrder ASC            
 
            IF @@rowcount = 0 
                SET @tintOrder = 0 
            
        END                       
      
    PRINT @bitLocation
    SET @strSql = @strSql + ' from #tblItem temp
			LEFT JOIN ListDetails LD ON LD.vcData = temp.numShipClass AND LD.numListID=461 and LD.numDomainID = '  + CONVERT(VARCHAR(20), @numDomainID) + ' '
		+ @WhereCondition
        + ' where  RunningCount >' + CONVERT(VARCHAR(15), @firstRec)
        + ' and RunningCount < ' + CONVERT(VARCHAR(15), @lastRec)
        + ' order by RunningCount'   

    SET @strSql = REPLACE(@strSql, '|', ',') 
    PRINT @strSql
    EXEC ( @strSql)  

    
                              
 
DROP TABLE #tempItemList
DROP TABLE #tempItemList1

END

UPDATE  #tempForm
    SET     vcDbColumnName = CASE WHEN bitCustomField = 1
                                  THEN vcFieldName + '~' + vcDbColumnName
                                  ELSE vcDbColumnName END
                                  
   SELECT  * FROM #tempForm

    DROP TABLE #tempForm
/****** Object:  StoredProcedure [dbo].[USP_ItemPricingRecomm]    Script Date: 02/19/2009 01:33:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- exec USP_ItemPricingRecomm @numItemCode=173028,@units=1,@numOppID=0,@numDivisionID=7052,@numDomainID=72,@tintOppType=1,@CalPrice=$50,@numWareHouseItemID=130634,@bitMode=0
--created by anoop jayaraj                          
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_itempricingrecomm')
DROP PROCEDURE usp_itempricingrecomm
GO
CREATE PROCEDURE [dbo].[USP_ItemPricingRecomm]                                        
@numItemCode as numeric(9),                                        
@units integer,                                  
@numOppID as numeric(9)=0,                                
@numDivisionID as numeric(9)=0,                  
@numDomainID as numeric(9)=0,            
@tintOppType as tinyint,          
@CalPrice as money,      
@numWareHouseItemID as numeric(9),
@bitMode tinyint=0
as                 
BEGIN TRY

--      SELECT 1/0 ; --Divide by zero error encountered.
      
declare @numRelationship as numeric(9)                  
declare @numProfile as numeric(9)                  
declare @monListPrice MONEY
DECLARE @bitCalAmtBasedonDepItems bit
DECLARE @numDefaultSalesPricing TINYINT

/*Profile and relationship id */
select @numRelationship=numCompanyType,@numProfile=vcProfile from DivisionMaster D                  
join CompanyInfo C on C.numCompanyId=D.numCompanyID                  
where numDivisionID =@numDivisionID       

          
            
if @tintOppType=1            
begin            

SELECT @bitCalAmtBasedonDepItems=ISNULL(bitCalAmtBasedonDepItems,0) FROM item WHERE numItemCode = @numItemCode
      


/*Get List Price for item*/      
if ((@numWareHouseItemID>0) and exists(select * from item where numItemCode=@numItemCode and  charItemType='P'))      
begin      
	select @monListPrice=isnull(monWListPrice,0) from WareHouseItems where numWareHouseItemID=@numWareHouseItemID      
    if @monListPrice=0 select @monListPrice=monListPrice from Item where numItemCode=@numItemCode       
end      
else      
begin      
	 select @monListPrice=monListPrice from Item where numItemCode=@numItemCode      
end      
print @monListPrice   
/*Use Calculated Price When item is Assembly/Kit,Calculate price based on sum of dependent items.*/
IF @bitCalAmtBasedonDepItems = 1 
BEGIN
	
	SELECT  
			@CalPrice = ISNULL(  SUM(ISNULL(CASE WHEN I.[charItemType]='P' 
			THEN CASE WHEN I.bitSerialized = 1 THEN I.monListPrice ELSE WI.[monWListPrice] END 
			ELSE monListPrice END,0) * ID.[numQtyItemsReq] )  ,0)
	FROM    [ItemDetails] ID
			INNER JOIN [Item] I ON ID.[numChildItemID] = I.[numItemCode]
			left join  WareHouseItems WI
			on WI.numItemID=I.numItemCode and WI.[numWareHouseItemID]=ID.[numWareHouseItemId]
	WHERE   [numItemKitID] = @numItemCode
	
END


SELECT @numDefaultSalesPricing = ISNULL(numDefaultSalesPricing,2) FROM Domain WHERE numDomainID = @numDomainID

IF @numDefaultSalesPricing = 1 -- Use Price Level
BEGIN
	DECLARE @newPrice MONEY
	DECLARE @finalUnitPrice FLOAT = 0
	DECLARE @tintRuleType INT
	DECLARE @tintDiscountType INT
	DECLARE @decDiscount FLOAT
	DECLARE @ItemPrice FLOAT

	SET @tintRuleType = 0
	SET @tintDiscountType = 0
	SET @decDiscount  = 0
	SET @ItemPrice = 0

	SELECT TOP 1
		@tintRuleType = tintRuleType,
		@tintDiscountType = tintDiscountType,
		@decDiscount = decDiscount
	FROM 
		PricingTable 
	WHERE 
		numItemCode = @numItemCode AND
		(@units BETWEEN intFromQty AND intToQty)

	IF @tintRuleType > 0 AND @tintDiscountType > 0
	BEGIN	
		IF @tintRuleType = 1 -- Deduct from List price
		BEGIN
		IF (SELECT ISNULL(charItemType,'') FROM Item WHERE numDomainID = @numDomainID AND numItemCode = @numItemCode) = 'P'
			If (SELECT COUNT(*) FROM Item WHERE numDomainID = @numDomainID AND numItemCode = @numItemCode AND (bitKitParent = 1 OR bitAssembly = 1)) > 0 
				SELECT @ItemPrice = dbo.GetUnitPriceForKitOrAssembly(@numDomainID,@numItemCode,@numWarehouseItemID,@units)
			ELSE
				SELECT @ItemPrice = ISNULL(monWListPrice,0) FROM WareHouseItems WHERE numDomainID = @numDomainID AND numItemID = @numItemCode AND numWareHouseItemID = @numWarehouseItemID
		ELSE
			SELECT @ItemPrice = ISNULL(monListPrice,0) FROM Item WHERE numDomainID = @numDomainID AND numItemCode = @numItemCode

		IF @tintDiscountType = 1 -- Percentage
		BEGIN
			IF @decDiscount > 0
				SELECT @finalUnitPrice = @ItemPrice - (@ItemPrice * (@decDiscount/100))
		END
		ELSE IF @tintDiscountType = 2 -- Flat Amount
		BEGIN
				SELECT @finalUnitPrice = @ItemPrice - @decDiscount
		END
	END
		ELSE IF @tintRuleType = 2 -- Add to Primary Vendor Cost
		BEGIN
			If (SELECT COUNT(*) FROM Item WHERE numDomainID = @numDomainID AND numItemCode = @numItemCode AND (bitKitParent = 1 OR bitAssembly = 1)) > 0 
				SELECT @ItemPrice = dbo.GetUnitPriceForKitOrAssembly(@numDomainID,@numItemCode,@numWarehouseItemID,@units)
			ELSE
				SELECT @ItemPrice = ISNULL(monCost,0) FROM Vendor WHERE numItemCode = @numItemCode AND numVendorID = (SELECT numVendorID FROM Item WHERE numItemCode = @numItemCode)

			If @ItemPrice > 0
			BEGIN
				IF @tintDiscountType = 1 -- Percentage
				BEGIN
					IF @decDiscount > 0
						SELECT @finalUnitPrice = @ItemPrice + (@ItemPrice * (@decDiscount/100))
				END
				ELSE IF @tintDiscountType = 2 -- Flat Amount
				BEGIN
					SELECT @finalUnitPrice = @ItemPrice + @decDiscount
				END
			END
		END
		ELSE IF @tintRuleType = 3 -- Named Price
		BEGIN
			SELECT @finalUnitPrice = @decDiscount
			SET @tintDiscountType = 2

			If @monListPrice > 0 AND @monListPrice >= @finalUnitPrice
			BEGIN
				SET @decDiscount = @monListPrice - @finalUnitPrice
			END
		END
	END

	Print @finalUnitPrice

	IF @finalUnitPrice = 0
		SET @newPrice = @monListPrice
	ELSE
		SET @newPrice = @finalUnitPrice

	If @tintRuleType = 2
	BEGIN
		IF @finalUnitPrice > 0
		BEGIN
			IF @tintDiscountType = 1 -- Percentage
			BEGIN
				IF @newPrice > @monListPrice
					SET @decDiscount = 0
				ELSE
					If @monListPrice > 0
						SET @decDiscount = ((@monListPrice - @newPrice) * 100) / CAST(@monListPrice AS FLOAT)
					ELSE
						SET @decDiscount = 0
			END
			ELSE IF @tintDiscountType = 2 -- Flat Amount
			BEGIN
				If @newPrice > @monListPrice
					SET @decDiscount = 0
				ELSE
					SET @decDiscount = @monListPrice - @newPrice
			END
		END
		ELSE
		BEGIN
			SET @decDiscount = 0
			SET @tintDiscountType = 0
		END
		
	END

	SELECT 
		1 as numUintHour, 
		@newPrice AS ListPrice,
		'' AS vcPOppName,
		'' AS bintCreatedDate,
		CASE 
			WHEN @tintRuleType = 0 THEN 'Price - List price'
            ELSE 
               CASE @tintRuleType
               WHEN 1 
					THEN 
						'Deduct from List price ' +  CASE 
													   WHEN @tintDiscountType = 1 THEN CONVERT(VARCHAR(20), @decDiscount) + '%'
													   ELSE 'flat ' + CONVERT(VARCHAR(20), CAST(@decDiscount AS int))
													   END 
               WHEN 2 THEN 'Add to primary vendor cost '  +  CASE 
													   WHEN @tintDiscountType = 1 THEN CONVERT(VARCHAR(20), @decDiscount) + '%'
													   ELSE 'flat ' + CONVERT(VARCHAR(20), CAST(@decDiscount AS int))
													   END 
               ELSE 'Flat ' + CONVERT(VARCHAR(20), @decDiscount)
               END
        END AS OppStatus,
		CAST(0 AS NUMERIC) AS numPricRuleID,
		CAST(1 AS TINYINT) AS tintPricingMethod,
		CAST(@tintDiscountType AS TINYINT) AS tintDiscountTypeOriginal,
		CAST(@decDiscount AS FLOAT) AS decDiscountOriginal,
		CAST(@tintRuleType AS TINYINT) AS tintRuleType
END
ELSE -- Use Price Rule
BEGIN
/* Checks Pricebook if exist any.. */      
SELECT TOP 1 1 AS numUnitHour,
		dbo.GetPriceBasedOnPriceBook(P.[numPricRuleID],@monListPrice,@units,I.numItemCode) 
		* (CASE WHEN P.tintPricingMethod =2 AND P.tintRuleType=2 THEN dbo.fn_UOMConversion(numBaseUnit,@numItemCode,@numDomainId,CASE WHEN ISNULL(numPurchaseUnit,0)>0 THEN numPurchaseUnit ELSE numBaseUnit END) ELSE 1 END)  AS ListPrice,
        vcRuleName AS vcPOppName,
        CONVERT(VARCHAR(20), NULL) AS bintCreatedDate,
        CASE WHEN numPricRuleID IS NULL THEN 'Price - List price'
             ELSE 
              CASE WHEN P.tintPricingMethod = 1
                                 THEN 'Price Book Rule'
                                 ELSE CASE PBT.tintRuleType
                                        WHEN 1 THEN 'Deduct from List price '
                                        WHEN 2 THEN 'Add to primary vendor cost '
                                        ELSE ''
                                      END
                                      + CASE WHEN tintDiscountType = 1
                                             THEN CONVERT(VARCHAR(20), decDiscount)
                                                  + '%'
                                             ELSE 'flat ' + CONVERT(VARCHAR(20), CAST(decDiscount AS int))
                                        END + ' for every '
                                      + CONVERT(VARCHAR(20), CASE WHEN [intQntyItems] = 0 THEN 1
                                                                  ELSE intQntyItems
                                                             END)
                                      + ' units, until maximum of '
                                      + CONVERT(VARCHAR(20), decMaxDedPerAmt)
                                      + CASE WHEN tintDiscountType = 1 THEN ' %' ELSE ' Flat' END END
        END AS OppStatus,P.numPricRuleID,P.tintPricingMethod,PBT.tintDiscountTypeOriginal,CAST(PBT.decDiscountOriginal AS FLOAT) AS decDiscountOriginal,PBT.tintRuleType
FROM    Item I
        LEFT JOIN PriceBookRules P ON I.numDomainID = P.numDomainID
        LEFT JOIN PriceBookRuleDTL PDTL ON P.numPricRuleID = PDTL.numRuleID
        LEFT JOIN PriceBookRuleItems PBI ON P.numPricRuleID = PBI.numRuleID
        LEFT JOIN [PriceBookPriorities] PP ON PP.[Step2Value] = P.[tintStep2] AND PP.[Step3Value] = P.[tintStep3]
		CROSS APPLY dbo.GetPriceBasedOnPriceBookTable(P.[numPricRuleID],CASE WHEN bitCalAmtBasedonDepItems=1 then @CalPrice ELSE @monListPrice END ,@units,I.numItemCode) as PBT
WHERE   numItemCode = @numItemCode AND tintRuleFor=@tintOppType
AND 
(
((tintStep2 = 1 AND PBI.numValue = @numItemCode) AND (tintStep3 = 1 AND PDTL.numValue = @numDivisionID)) -- Priority 1
OR ((tintStep2 = 2 AND PBI.numValue = I.numItemClassification) AND (tintStep3= 1 AND PDTL.numValue = @numDivisionID)) -- Priority 2
OR ((tintStep2 = 3) AND (tintStep3 = 1 AND PDTL.numValue = @numDivisionID)) -- Priority 3

OR ((tintStep2 = 1 AND PBI.numValue = @numItemCode) AND (tintStep3 = 2 AND PDTL.numValue = @numRelationship AND numProfile = @numProfile)) -- Priority 4
OR ((tintStep2 = 2 AND PBI.numValue = I.numItemClassification) AND (tintStep3 = 2 AND PDTL.numValue = @numRelationship AND numProfile = @numProfile)) -- Priority 5
OR ((tintStep2 = 3) AND (tintStep3 = 2 AND PDTL.numValue = @numRelationship AND numProfile = @numProfile)) -- Priority 6

OR ((tintStep2 = 1 AND PBI.numValue = @numItemCode) AND (tintStep3 = 3)) -- Priority 7
OR ((tintStep2 = 2 AND PBI.numValue = I.numItemClassification) AND (tintStep3 = 3)) -- Priority 8
OR ((tintStep2 = 3) and (tintStep3 = 3)) -- Priority 9
)
ORDER BY PP.Priority ASC
END

IF @bitMode=2  -- When this procedure is being called from USP_GetPriceOfItem for e-com
	RETURN 
-- select 1 as numUnitHour,convert(money,(case when tintRuleType is null then (case when bitCalAmtBasedonDepItems=1 then @CalPrice else @monListPrice end)*@units when tintRuleType=0 then (case when bitCalAmtBasedonDepItems=1 then @CalPrice else @monListPrice end)*@units-
--(case when bitCalAmtBasedonDepItems=1 then @CalPrice else @monListPrice end)*(case when bitVolDisc=1 then           
--(case when (@units/intQntyItems)*fltDedPercentage >decMaxDedPerAmt then decMaxDedPerAmt else (@units/intQntyItems)*fltDedPercentage end)           
-- else fltDedPercentage end )*@units/100                  
--  when tintRuleType=1 then (case when bitCalAmtBasedonDepItems=1 then @CalPrice else @monListPrice end)*@units-          
--(case when bitVolDisc=1 then (case when (@units/intQntyItems)*decflatAmount>decMaxDedPerAmt then decMaxDedPerAmt else           
--(@units/intQntyItems)*decflatAmount end) else decflatAmount end )  end)/@units)           
--as ListPrice,vcRuleName as vcPOppName,convert(varchar(20),null) as bintCreatedDate,case when numPricRuleID is null then 'Price - List price' else 'Price Book Rule' end as OppStatus  from Item I      
-- left join  PriceBookRules P      
-- on I.numDomainID=P.numDomainID                   
-- left join PriceBookRuleDTL  PDTL                  
-- on P.numPricRuleID=PDTL.numRuleID                   
--where numItemCode=@numItemCode and    (((tintRuleAppType=1 and numValue=@numItemCode) or                  
-- (tintRuleAppType=2 and numValue=numItemGroup) or                  
-- (tintRuleAppType=4 and numValue=@numDivisionID) or                  
-- (tintRuleAppType=5 and numValue=@numRelationship) or                  
-- (tintRuleAppType=6 and numValue=@numRelationship and numProfile=@numProfile) or                  
-- (tintRuleAppType=7) or (tintRuleAppType= case when numItemGroup>0 then 3 else 10 end)) and (case when bitTillDate=1 then dtTillDate  else getutcdate() end >=getutcdate()) or (tintRuleAppType is null) )                               
                                   
/*Checks Old Order for price*/
 select 1 as numUnitHour,convert(money,(monTotAmount/(case when numUnitHour< 1 then 1 else numUnitHour end))) as ListPrice,vcPOppName,convert(varchar(20),bintCreatedDate) as bintCreatedDate,case when tintOppStatus=0 then 'Opportunity' when tintOppStatus=1 then 'Deal won'   
 when tintOppStatus=2 then 'Deal Lost' end as OppStatus,CAST(0 AS NUMERIC) AS numPricRuleID,CAST(0 AS TINYINT) AS tintPricingMethod,CAST(0 AS TINYINT) as tintDiscountTypeOriginal,CAST(0 AS FLOAT) as decDiscountOriginal,CAST(0 AS TINYINT) as tintRuleType                         
 from OpportunityItems itm                                        
 join OpportunityMaster mst                                        
 on mst.numOppId=itm.numOppId              
 where  tintOppType=1 and numItemCode=@numItemCode and mst.numDivisionID=@numDivisionID order by OppStatus desc  
  
  
  /* */
 select 1 as numUnitHour,CASE WHEN @bitCalAmtBasedonDepItems=1 THEN convert(money,@CalPrice) ELSE convert(money,@monListPrice) End as ListPrice,'' as vcPOppName,convert(varchar(20),null) as bintCreatedDate,CASE WHEN @bitCalAmtBasedonDepItems=1 THEN 'Sum of dependent items' ELSE '' END as OppStatus
 ,CAST(0 AS NUMERIC) AS numPricRuleID,CAST(0 AS TINYINT) AS tintPricingMethod ,CAST(0 AS TINYINT) as tintDiscountTypeOriginal,CAST(0 AS FLOAT) as decDiscountOriginal,CAST(0 AS TINYINT) as tintRuleType                    
  
            
end             
else            
begin                                               

If @numDivisionID=0
BEGIN
 SELECT @numDivisionID=V.numVendorID FROM [Vendor] V INNER JOIN Item I 
		on V.numVendorID=I.numVendorID and V.numItemCode=I.numItemCode 
		WHERE V.[numItemCode]=@numItemCode AND V.[numDomainID]=@numDomainID 
END

IF EXISTS(SELECT ISNULL([monCost],0) ListPrice FROM [Vendor] V INNER JOIN dbo.Item I 
		ON V.numItemCode = I.numItemCode
		WHERE V.[numItemCode]=@numItemCode AND V.[numDomainID]=@numDomainID AND V.numVendorID=@numDivisionID)       
begin      
       SELECT @monListPrice=ISNULL([monCost],0) FROM [Vendor] V INNER JOIN dbo.Item I 
				ON V.numItemCode = I.numItemCode
				WHERE V.[numItemCode]=@numItemCode AND V.[numDomainID]=@numDomainID 
				AND V.numVendorID=@numDivisionID
end      
else      
begin      
	 SELECT @monListPrice=ISNULL([monCost],0) FROM [Vendor] WHERE [numItemCode]=@numItemCode AND [numDomainID]=@numDomainID
end      

print @monListPrice   

SELECT TOP 1 1 AS numUnitHour,
			dbo.GetPriceBasedOnPriceBook(P.[numPricRuleID],@monListPrice,@units,I.numItemCode) AS ListPrice,
			vcRuleName AS vcPOppName,
			CONVERT(VARCHAR(20), NULL) AS bintCreatedDate,
			CASE WHEN numPricRuleID IS NULL THEN 'Price - List price'
				 ELSE  CASE WHEN P.tintPricingMethod = 1
                                 THEN 'Price Book Rule'
                                 ELSE CASE tintRuleType
                                        WHEN 1 THEN 'Deduct from List price '
                                        WHEN 2 THEN 'Add to primary vendor cost '
                                        ELSE ''
                                      END
                                      + CASE WHEN tintDiscountType = 1
                                             THEN CONVERT(VARCHAR(20), decDiscount)
                                                  + '%'
                                             ELSE 'flat ' + CONVERT(VARCHAR(20), CAST(decDiscount AS int))
                                        END + ' for every '
                                      + CONVERT(VARCHAR(20), CASE WHEN [intQntyItems] = 0 THEN 1
                                                                  ELSE intQntyItems
                                                             END)
                                      + ' units, until maximum of '
                                      + CONVERT(VARCHAR(20), decMaxDedPerAmt)
                                      + CASE WHEN tintDiscountType = 1 THEN ' %' ELSE ' Flat' END END
			END AS OppStatus,P.numPricRuleID,P.tintPricingMethod,@numDivisionID as numDivisionID
		FROM    Item I 
        LEFT JOIN PriceBookRules P ON I.numDomainID = P.numDomainID
        LEFT JOIN PriceBookRuleDTL PDTL ON P.numPricRuleID = PDTL.numRuleID
        LEFT JOIN PriceBookRuleItems PBI ON P.numPricRuleID = PBI.numRuleID
        LEFT JOIN [PriceBookPriorities] PP ON PP.[Step2Value] = P.[tintStep2]
                                              AND PP.[Step3Value] = P.[tintStep3]
WHERE   numItemCode = @numItemCode AND tintRuleFor=@tintOppType
AND 
(
((tintStep2 = 1 AND PBI.numValue = @numItemCode) AND (tintStep3 = 1 AND PDTL.numValue = @numDivisionID)) -- Priority 1
OR ((tintStep2 = 2 AND PBI.numValue = I.numItemClassification) AND (tintStep3= 1 AND PDTL.numValue = @numDivisionID)) -- Priority 2
OR ((tintStep2 = 3) AND (tintStep3 = 1 AND PDTL.numValue = @numDivisionID)) -- Priority 3

OR ((tintStep2 = 1 AND PBI.numValue = @numItemCode) AND (tintStep3 = 2 AND PDTL.numValue = @numRelationship AND numProfile = @numProfile)) -- Priority 4
OR ((tintStep2 = 2 AND PBI.numValue = I.numItemClassification) AND (tintStep3 = 2 AND PDTL.numValue = @numRelationship AND numProfile = @numProfile)) -- Priority 5
OR ((tintStep2 = 3) AND (tintStep3 = 2 AND PDTL.numValue = @numRelationship AND numProfile = @numProfile)) -- Priority 6

OR ((tintStep2 = 1 AND PBI.numValue = @numItemCode) AND (tintStep3 = 3)) -- Priority 7
OR ((tintStep2 = 2 AND PBI.numValue = I.numItemClassification) AND (tintStep3 = 3)) -- Priority 8
OR ((tintStep2 = 3) and (tintStep3 = 3)) -- Priority 9
)
ORDER BY PP.Priority ASC

--	IF @bitMode = 1 /* from Popup for Grid*/
--	BEGIN
		 select 1 as numUnitHour,(monTotAmount/(case when numUnitHour< 1 then 1 else numUnitHour end)) as ListPrice,vcPOppName,convert(varchar(20),bintCreatedDate) as bintCreatedDate,case when tintOppStatus=0 then 'Opportunity' 
		 when tintOppStatus=1 then 'Deal won'  
		 when tintOppStatus=2 then 'Deal Lost' end as OppStatus
		 ,CAST(0 AS NUMERIC) AS numPricRuleID,CAST(0 AS TINYINT) AS tintPricingMethod,@numDivisionID as numDivisionID                         
		 from OpportunityItems itm                                        
		 join OpportunityMaster mst                                        
		 on mst.numOppId=itm.numOppId                                        
		 where  tintOppType=2 and numItemCode=@numItemCode and mst.numDivisionID=@numDivisionID order by OppStatus DESC
--	END
--	ELSE
--	BEGIN
		/* Checks Pricebook if exist any.. */      
		
--	END
 
--	 IF @@ROWCOUNT=0
--	 BEGIN
 		select 1 as numUnitHour,convert(money,@monListPrice) as ListPrice,'' as vcPOppName,convert(varchar(20),null) as bintCreatedDate,''  as OppStatus,@numDivisionID as numDivisionID
 		,CAST(0 AS NUMERIC) AS numPricRuleID,CAST(0 AS TINYINT) AS tintPricingMethod                         
--	 END
 
	
end

END TRY
BEGIN CATCH
    SELECT 0 AS ListPrice
    SELECT 0 AS ListPrice
    SELECT 0 AS ListPrice
END CATCH

GO
/****** Object:  StoredProcedure [dbo].[USP_ManageAlertConfig]    Script Date: 11/02/2011 12:44:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ManageAlertConfig')
DROP PROCEDURE USP_ManageAlertConfig
GO
CREATE PROC [dbo].[USP_ManageAlertConfig] 
    @numAlertConfigId numeric(18, 0) OUTPUT ,
    @numDomainId numeric(18, 0),
    @numContactId numeric(9, 0),
    @bitOpenActionItem bit,
    @vcOpenActionMsg varchar(15),
    @bitOpenCases bit,
    @vcOpenCasesMsg varchar(15),
    @bitOpenProject bit,
    @vcOpenProjectMsg varchar(15),
    @bitOpenSalesOpp bit,
    @vcOpenSalesOppMsg varchar(15),
    @bitOpenPurchaseOpp bit,
    @vcOpenPurchaseOppMsg varchar(15),
    @bitBalancedue bit,
    @monBalancedue money,
    @vcBalancedueMsg varchar(15),
    @bitUnreadEmail bit,
    @intUnreadEmail int,
    @vcUnreadEmailMsg varchar(15),
    @bitPurchasedPast bit,
    @monPurchasedPast money,
    @vcPurchasedPastMsg varchar(15),
    @bitSoldPast bit,
    @monSoldPast money,
    @vcSoldPastMsg varchar(15),
    @bitCustomField bit,
    @numCustomFieldId numeric(9, 0),
    @vcCustomFieldValue varchar(50),
    @vcCustomFieldMsg varchar(15),
	@bitCampaign BIT
AS 
DECLARE @numCount AS NUMERIC
SET @numCount = 0
SELECT @numCount=COUNT(*) FROM dbo.AlertConfig WHERE numDomainId =@numDomainId AND numContactId=@numContactId
PRINT @numCount 
IF @numCount > 0 
    BEGIN
 UPDATE [dbo].[AlertConfig]
 SET    [numDomainId] = @numDomainId, [numContactId] = @numContactId, [bitOpenActionItem] = @bitOpenActionItem, [vcOpenActionMsg] = @vcOpenActionMsg, 
 [bitOpenCases] = @bitOpenCases, [vcOpenCasesMsg] = @vcOpenCasesMsg, [bitOpenProject] = @bitOpenProject, [vcOpenProjectMsg] = @vcOpenProjectMsg, 
 [bitOpenSalesOpp] = @bitOpenSalesOpp, [vcOpenSalesOppMsg] = @vcOpenSalesOppMsg, [bitOpenPurchaseOpp] = @bitOpenPurchaseOpp,
 [vcOpenPurchaseOppMsg] = @vcOpenPurchaseOppMsg, [bitBalancedue] = @bitBalancedue, [monBalancedue] = @monBalancedue, 
 [vcBalancedueMsg] = @vcBalancedueMsg,[bitUnreadEmail] = @bitUnreadEmail, [intUnreadEmail] = @intUnreadEmail, [vcUnreadEmailMsg] = @vcUnreadEmailMsg, [bitPurchasedPast] = @bitPurchasedPast, [monPurchasedPast] = @monPurchasedPast, [vcPurchasedPastMsg] = @vcPurchasedPastMsg, [bitSoldPast] = @bitSoldPast, [monSoldPast] = @monSoldPast, [vcSoldPastMsg] = @vcSoldPastMsg, [bitCustomField] = @bitCustomField, [numCustomFieldId] = @numCustomFieldId, [vcCustomFieldValue] = @vcCustomFieldValue, [vcCustomFieldMsg] = @vcCustomFieldMsg,
 bitCampaign = @bitCampaign
 WHERE numDomainId =@numDomainId AND numContactId=@numContactId  --[numAlertConfigId] = @numAlertConfigId
     END 
else 
 BEGIN
 INSERT INTO [dbo].[AlertConfig] ([numDomainId], [numContactId], [bitOpenActionItem], [vcOpenActionMsg], [bitOpenCases], [vcOpenCasesMsg], [bitOpenProject], [vcOpenProjectMsg], [bitOpenSalesOpp], [vcOpenSalesOppMsg], [bitOpenPurchaseOpp], [vcOpenPurchaseOppMsg], [bitBalancedue], [monBalancedue], [vcBalancedueMsg], [bitUnreadEmail], [intUnreadEmail], [vcUnreadEmailMsg], [bitPurchasedPast], [monPurchasedPast], [vcPurchasedPastMsg], [bitSoldPast], [monSoldPast], [vcSoldPastMsg], [bitCustomField], [numCustomFieldId], [vcCustomFieldValue], [vcCustomFieldMsg], [bitCampaign])
  SELECT @numDomainId, @numContactId, @bitOpenActionItem, @vcOpenActionMsg, @bitOpenCases, @vcOpenCasesMsg, @bitOpenProject, @vcOpenProjectMsg, @bitOpenSalesOpp, @vcOpenSalesOppMsg, @bitOpenPurchaseOpp, @vcOpenPurchaseOppMsg, @bitBalancedue, @monBalancedue, @vcBalancedueMsg,@bitUnreadEmail, @intUnreadEmail, @vcUnreadEmailMsg, @bitPurchasedPast, @monPurchasedPast, @vcPurchasedPastMsg, @bitSoldPast, @monSoldPast, @vcSoldPastMsg, @bitCustomField, @numCustomFieldId, @vcCustomFieldValue, @vcCustomFieldMsg, @bitCampaign
  
  SET @numAlertConfigId= SCOPE_IDENTITY()
 END 
 
/****** Object:  StoredProcedure [dbo].[USP_ManageConEmailCampaign]    Script Date: 07/26/2008 16:19:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--create by anoop jayaraj        
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_manageconemailcampaign')
DROP PROCEDURE usp_manageconemailcampaign
GO
CREATE PROCEDURE [dbo].[USP_ManageConEmailCampaign]        
@numConEmailCampID numeric(9)=0,        
@numContactID numeric(9)=0,        
@numECampaignID numeric(9)=0,        
@intStartDate datetime,        
@bitEngaged bit,    
@numUserCntID numeric(9)=0        
as        
    
--DisEngage Current contact from ECampaign
IF @numECampaignID = 0 
BEGIN
	UPDATE [ConECampaign] SET [bitEngaged]=0 WHERE [numContactID]=@numContactID
	RETURN ;
END
        
        
if @numConEmailCampID= 0         
begin        
	
	-- stop all other campaign for given contact 
	update ConECampaign set bitEngaged= 0 where numContactID=@numContactID AND [numECampaignID] <> @numECampaignID
	
	--Prevent Duplicate entry
	IF NOT EXISTS (SELECT * FROM [ConECampaign] WHERE [numContactID]=@numContactID AND [numECampaignID]=@numECampaignID AND [bitEngaged]=1)
	BEGIN
		
		/*Add Start Time to Given Start Date from ECampaign*/
		DECLARE @StartDateTime DATETIME 	   
		DECLARE @dtECampStartTime1 DATETIME 	   
		SELECT @dtECampStartTime1 = ISNULL(dtStartTime,0) FROM [ECampaign] WHERE [numECampaignID]=@numECampaignID
		--	SELECT @StartDateTime;
		SELECT @StartDateTime = DATEADD(hour,DATEPART(hour,@dtECampStartTime1) ,@intStartDate)
		SELECT @StartDateTime = DATEADD(minute,DATEPART(minute,@dtECampStartTime1) ,@StartDateTime)
			
	
		--Insert To Header
		insert into ConECampaign(numContactID,numECampaignID,intStartDate,bitEngaged,numRecOwner)        
		values (@numContactID,@numECampaignID,@StartDateTime,@bitEngaged,@numUserCntID)
		set @numConEmailCampID=@@identity       
		--Insert To Detail
		DECLARE @TEMP TABLE
		(
			RowNo INT,
			numECampDTLId NUMERIC(18,0),
			tintDays INT,
			tintWaitPeriod TINYINT
		)

		INSERT INTO 
			@TEMP 
		SELECT 
			ROW_NUMBER() OVER(ORDER BY numECampDTLId),
			numECampDTLId,
			tintDays,
			tintWaitPeriod 
		FROM 
			ECampaignDTLs      
		WHERE 
			numECampID=@numECampaignID  AND
			(numEmailTemplate > 0 OR numActionItemTemplate > 0)
		ORDER BY
			numECampDTLId


		DECLARE @i AS INT = 0
		DECLARE @Count AS INT
		SELECT @Count=COUNT(*) FROM @TEMP


		DECLARE @TempDate AS DATETIME
		DECLARE @numECampDTLId AS NUMERIC(18,0)
		DECLARE @TempDays AS INT
		DECLARE @TempWaitPeriod AS INT

		SET @TempDate = @StartDateTime

		WHILE @i <= @Count
		BEGIN

			SELECT @numECampDTLId=numECampDTLId, @TempDays=tintDays, @TempWaitPeriod=tintWaitPeriod FROM @TEMP WHERE RowNo=@i

			INSERT INTO ConECampaignDTL
			(
				numConECampID,
				numECampDTLID,
				dtExecutionDate
			)       
			SELECT 
				@numConEmailCampID,
				@numECampDTLId,
				@TempDate 
			FROM 
				@TEMP      
			WHERE 
				RowNo=@i

			IF (@TempWaitPeriod = 1) --Day
				SELECT @TempDate = DATEADD(D,@TempDays,@TempDate)
			ELSE IF (@TempWaitPeriod = 2) --Week
				SELECT @TempDate = DATEADD(D,(@TempDays * 7),@TempDate)
			
			SET @i = @i + 1
		END
	END
end        
else         
begin        
	update ConECampaign set numContactID=@numContactID,        
	numECampaignID=@numECampaignID,        
	intStartDate=@intStartDate,        
	bitEngaged=@bitEngaged        
	where numConEmailCampID=@numConEmailCampID        
	end        
	        
	select @numConEmailCampID
GO
/****** Object:  StoredProcedure [dbo].[USP_ManageECampaign]    Script Date: 06/04/2009 16:20:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj  
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_manageecampaign')
DROP PROCEDURE usp_manageecampaign
GO
CREATE PROCEDURE [dbo].[USP_ManageECampaign]  
@numECampaignID numeric(9)=0,  
@vcCampaignName as varchar(100)='',  
@txtDesc text='',  
@strECampaignDTLs as text,
@numDomainID as numeric(9)=0 ,
@dtStartTime AS DATETIME,
@fltTimeZone AS FLOAT,
@tintTimeZoneIndex TINYINT,
@tintFromField TINYINT,
@numFromContactID NUMERIC,
@numUserContactID NUMERIC(9)
as  
DECLARE @hDoc int    
if @numECampaignID=0   
  
begin  
insert into ECampaign(vcECampName,txtDesc,numDomainID,dtStartTime,[tintFromField],[numFromContactID],fltTimeZone,tintTimeZoneIndex,numCreatedBy)  
values(@vcCampaignName,@txtDesc,@numDomainID,@dtStartTime,@tintFromField,@numFromContactID,@fltTimeZone,@tintTimeZoneIndex,@numUserContactID)  
set @numECampaignID=@@identity    
  
      
 EXEC sp_xml_preparedocument @hDoc OUTPUT, @strECampaignDTLs        
        
 insert into ECampaignDTLs        
  (numECampID,numEmailTemplate,numActionItemTemplate,tintDays,tintWaitPeriod,[numFollowUpID])        --,[numEmailGroupID],[dtStartDate]  ,@numEmailGroupID,@dtStartDate
         
 select @numECampaignID,X.numEmailTemplate,X.numActionItemTemplate,x.tintDays,X.tintWaitPeriod,X.numFollowUpID from(        
 SELECT *FROM OPENXML (@hDoc,'/NewDataSet/Table',2)        
 WITH  (        
  numEmailTemplate numeric(9),
  numActionItemTemplate NUMERIC(9),      
  tintDays TINYINT,
  tintWaitPeriod TINYINT,
  [numFollowUpID] numeric
  ))X        
        
        
 EXEC sp_xml_removedocument @hDoc  
  
end  
else  
begin  
  
update ECampaign set vcECampName=@vcCampaignName,  
txtDesc=@txtDesc ,
dtStartTime=@dtStartTime,
fltTimeZone = @fltTimeZone,
tintTimeZoneIndex=@tintTimeZoneIndex,
[tintFromField] = @tintFromField,
[numFromContactID]=@numFromContactID
where numECampaignID= @numECampaignID  
  
 EXEC sp_xml_preparedocument @hDoc OUTPUT, @strECampaignDTLs        
          
    update ECampaignDTLs set         
     numEmailTemplate=X.numEmailTemplate,        
     numActionItemTemplate=X.numActionItemTemplate,
     tintDays=X.tintDays,
	 tintWaitPeriod = X.tintWaitPeriod,
     [numFollowUpID] = X.numFollowUpID 
      From (SELECT *        
     FROM OPENXML(@hDoc,'/NewDataSet/Table1',2)         
      with(  
      numECampDTLId numeric(9),  
      numEmailTemplate numeric(9),
      numActionItemTemplate numeric(9),
      tintDays TINYINT,
	  tintWaitPeriod TINYINT,
      numFollowUpID NUMERIC ))X         
      where  ECampaignDTLs.numECampDTLId=X.numECampDTLId    
  
 EXEC sp_xml_removedocument @hDoc  
  
end  
  
select @numECampaignID
/****** Object:  StoredProcedure [dbo].[Usp_ManageInboxTree]    Script Date: 07/26/2008 16:19:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_manageinboxtree')
DROP PROCEDURE usp_manageinboxtree
GO
CREATE PROCEDURE [dbo].[Usp_ManageInboxTree]    
@numUserCntID as numeric(9),    
@numDomainID as numeric(9),    
@NodeID as numeric(9),    
@NodeName as varchar(30),    
@mode as tinyint,    
@ParentId as numeric(9)    
as      
    
if @mode = 0      
begin      
IF Not Exists (Select 'col1' from  InboxTreeSort where numdomainid=@numDomainID AND numUserCntId=@numUserCntID)
		 BEGIN
			 select numNodeID,numParentId,vcName as vcNodeName from InboxTree where (numdomainid=0 or numdomainid=@numDomainID )       
			   and (numUserCntId=@numUserCntID or numUserCntId=0)      
			 order by numNodeID,numParentId 
		 END
   ELSE 
		BEGIN
			SELECT numNodeID,numParentId,ISNULL(vcNodeName,'') as vcNodeName ,numSortOrder, bitSystem, numFixID 
				FROM InboxTreeSort 
				WHERE (numdomainid=@numDomainID)   
					AND (numUserCntId=@numUserCntID )   
                    
			    ORDER BY numSortOrder ASC    
		END     
end      
if @mode = 1      
begin   
	DECLARE @ErrorMessage NVARCHAR(4000)
	SET @ErrorMessage=''
	DECLARE @ErrorSeverity INT
	DECLARE @ErrorState INT
		
	BEGIN TRANSACTION

	BEGIN TRY
		--Changed by sachin sadhu|| Date:21stJan2014
    --Purpose:Bug on creating Folders||VirginDrinks
		DECLARE @numSortOrder NUMERIC
		SELECT @numSortOrder=MAX(numSortOrder)+1 FROM dbo.InboxTreeSort WHERE numDomainID=@numDomainID AND numUserCntID=@numUserCntID   

		IF(@ParentId=0)
		BEGIN
			SELECT @ParentId=numNodeID FROM dbo.InboxTreeSort WHERE numDomainID=@numDomainID AND numUserCntID=@numUserCntID AND numParentID IS NULL
		END

		IF EXISTS (SELECT numNodeID FROM InboxTreeSort WHERE numDomainID=@numDomainID AND numUserCntID=@numUserCntID AND numParentID = @ParentId AND LOWER(vcNodeName) = LOWER(@NodeName))
		BEGIN
			RAISERROR ('FOLDER_ALREADY_EXISTS',16,1)
			RETURN -1
		END 
		ELSE
		BEGIN
			INSERT INTO [InboxTree]      
				(vcName      
				,numParentId      
				,[numDomainId],numUserCntId)      
			VALUES      
				(@NodeName      
				,@ParentId      
				,@numDomainID,@numUserCntId)   
			--To Save values into InboxTree Sort
			INSERT INTO [InboxTreeSort]      
				(vcNodeName      
				,numParentId      
				,[numDomainId],numUserCntId,numSortOrder)      
			VALUES      
				(@NodeName      
				,@ParentId      
				,@numDomainID,@numUserCntId,@numSortOrder) 
		END
	END TRY
	BEGIN CATCH
		SELECT @ErrorMessage = ERROR_MESSAGE(), @ErrorSeverity = ERROR_SEVERITY(), @ErrorState = ERROR_STATE();

		IF @@TRANCOUNT > 0
			ROLLBACK TRANSACTION;

		RAISERROR (@ErrorMessage,@ErrorSeverity,@ErrorState);
	END CATCH

	IF @@TRANCOUNT > 0
			COMMIT TRANSACTION;
end      
      
if @mode = 2      
begin      
     
update  emailhistory set numNodeid=2 where numdomainid=@numDomainID and numUserCntId=@numUserCntID and  numNodeid in    
  ( select numNodeId from inboxTree where (numNodeID = @NodeID or numParentId = @NodeID)  and       
   numdomainid=@numDomainID and numUserCntId=@numUserCntID )     
   
 delete [InboxTree] where (numNodeID = @NodeID or numParentId = @NodeID)  and       
   numdomainid=@numDomainID and numUserCntId=@numUserCntID    
--Update Email History with [InboxTreeSort]
update  emailhistory set numNodeid=2 where numdomainid=@numDomainID and numUserCntId=@numUserCntID and  numNodeid in    
  ( select numNodeId from InboxTreeSort where (numNodeID = @NodeID or numParentId = @NodeID)  and       
   numdomainid=@numDomainID and numUserCntId=@numUserCntID )  
      
 delete [InboxTreeSort] where (numNodeID = @NodeID or numParentId = @NodeID)  and       
   numdomainid=@numDomainID and numUserCntId=@numUserCntID    
end  
GO
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'usp_ManageInventory' ) 
    DROP PROCEDURE usp_ManageInventory
GO
CREATE PROCEDURE [dbo].[usp_ManageInventory]
    @itemcode AS NUMERIC(9) = 0,
    @numWareHouseItemID AS NUMERIC(9) = 0,
    @monAmount AS MONEY,
    @tintOpptype AS TINYINT,
    @numUnits AS NUMERIC,
    @QtyShipped AS NUMERIC,
    @QtyReceived AS NUMERIC,
    @monPrice AS MONEY,
    @tintFlag AS TINYINT,  --1: SO/PO insert/edit 2:SO/PO Close 3:SO/PO Re-Open 4:Sales Fulfillment Release 5:Sales Fulfillment Revert
    @numOppID as numeric(9)=0,
    @numoppitemtCode AS NUMERIC(9)=0,
    @numUserCntID AS NUMERIC(9)=0,
	@bitWorkOrder AS BIT = 0   
AS 
    DECLARE @onHand AS NUMERIC                                    
    DECLARE @onOrder AS NUMERIC 
    DECLARE @onBackOrder AS NUMERIC
    DECLARE @onAllocation AS NUMERIC
    DECLARE @monAvgCost AS MONEY 
    DECLARE @bitKitParent BIT
	DECLARE @numOrigUnits AS NUMERIC		
	DECLARE @description AS VARCHAR(100)
	DECLARE @bitAsset as BIT
--    DECLARE @OldOnHand AS NUMERIC    
--    DECLARE @NewAverageCost AS MONEY    
--    DECLARE @OldCost AS MONEY    
--    DECLARE @NewCost AS MONEY    
      
	SELECT @bitAsset=ISNULL(bitAsset,0) from Item where numItemCode=@itemcode--Added By Sachin
    
	SELECT  @monAvgCost = ISNULL(monAverageCost, 0),
            @bitKitParent = ( CASE WHEN ISNULL(bitKitParent,0) = 1
                               AND ISNULL(bitAssembly,0) = 1 THEN 0
                          WHEN ISNULL(bitKitParent,0) = 1 THEN 1
                          ELSE 0
                     END )
    FROM    Item
    WHERE   numitemcode = @itemcode                                   
    
    IF @numWareHouseItemID>0
    BEGIN                     
		SELECT  @onHand = ISNULL(numOnHand, 0),
				@onAllocation = ISNULL(numAllocation, 0),
				@onOrder = ISNULL(numOnOrder, 0),
				@onBackOrder = ISNULL(numBackOrder, 0)
		FROM    WareHouseItems
		WHERE   numWareHouseItemID = @numWareHouseItemID 
    END
   
   DECLARE @dtItemReceivedDate AS DATETIME;SET @dtItemReceivedDate=NULL
   
   DECLARE @TotalOnHand AS NUMERIC;SET @TotalOnHand=0  
   SELECT @TotalOnHand=SUM(ISNULL(numOnHand, 0)) FROM dbo.WareHouseItems WHERE numItemID=@itemcode
					
   SET @numOrigUnits=@numUnits
            
    IF @tintFlag = 1  --1: SO/PO insert/edit 
        BEGIN                                   
            IF @tintOpptype = 1 
                BEGIN  
			--When change below code also change to USP_ManageWorkOrderStatus  

				/* When QtyShipped has been updated form sales fulfillment then it need to be deducted */
					SET @description='SO insert/edit (Qty:' + CAST(@numUnits AS VARCHAR(10)) + ' Shipped:' +  CAST(@QtyShipped AS VARCHAR(10)) + ')'
				
				IF @bitKitParent = 1 
                    BEGIN
                        EXEC USP_UpdatingInventoryForEmbeddedKits @numoppitemtCode,
                            @numOrigUnits, 0,@numOppID,0,@numUserCntID
                    END 
				ELSE IF @bitWorkOrder = 1
				BEGIN
					SET @description='SO-WO insert/edit (Qty:' + CAST(@numUnits AS VARCHAR(10)) + ' Shipped:' +  CAST(@QtyShipped AS VARCHAR(10)) + ')'
					-- MANAGE INVENTOY OF ASSEMBLY ITEMS
					EXEC USP_ManageInventoryAssemblyWO @numUserCntID,@numOppID,@itemcode,@numWareHouseItemID,@QtyShipped,1
				END
                ELSE
				  BEGIN       
					SET @numUnits = @numUnits - @QtyShipped
                                                        
                    IF @onHand >= @numUnits 
                        BEGIN                                    
                            SET @onHand = @onHand - @numUnits                            
                            SET @onAllocation = @onAllocation + @numUnits                                    
                        END                                    
                    ELSE IF @onHand < @numUnits 
                            BEGIN                                    
                                SET @onAllocation = @onAllocation + @onHand                                    
                                SET @onBackOrder = @onBackOrder + @numUnits
                                    - @onHand                                    
                                SET @onHand = 0                                    
                            END    
			                                 
                 
				 
				    --UPDATE  WareHouseItems
        --            SET     numOnHand = @onHand,
        --                    numAllocation = @onAllocation,
        --                    numBackOrder = @onBackOrder,dtModified = GETDATE() 
        --            WHERE   numWareHouseItemID = @numWareHouseItemID  

				IF @bitAsset=0--Not Asset
						BEGIN 
							UPDATE  WareHouseItems
							SET     numOnHand = @onHand,
									numAllocation = @onAllocation,
									numBackOrder = @onBackOrder,dtModified = GETDATE() 
							WHERE   numWareHouseItemID = @numWareHouseItemID  

						END
					--ELSE --For Asset
					--	BEGIN

					--	DECLARE @onHandShake AS NUMERIC 
						  
					--						SELECT  @onHandShake = ISNULL(numOnHand, 0)											
					--						FROM    WareHouseItems
					--						WHERE   numWareHouseItemID = @numWareHouseItemID

					--		UPDATE  WareHouseItems
					--		SET   
					--				 numOnHand = @onHandShake,
					--				--numAllocation = @onAllocation,
					--				--numBackOrder = @onBackOrder,
					--				dtModified = GETDATE() 
					--		WHERE   numWareHouseItemID = @numWareHouseItemID  

					--	END


				END
			 
				       
                END                       
            ELSE IF @tintOpptype = 2 
                    BEGIN  
			        
						SET @description='PO insert/edit (Qty:' + CAST(@numUnits AS VARCHAR(10)) + ' Received:' +  CAST(@QtyReceived AS VARCHAR(10)) + ')'
		            
						/* When QtyReceived has been updated form purchase fulfillment then it need to be deducted */
                        SET @numUnits = @numUnits - @QtyReceived   
                                             
                        SET @onOrder = @onOrder + @numUnits 
		    
                        UPDATE  WareHouseItems
                        SET     numOnHand = @onHand,
                                numAllocation = @onAllocation,
                                numBackOrder = @onBackOrder,
                                numOnOrder = @onOrder,dtModified = GETDATE() 
                        WHERE   numWareHouseItemID = @numWareHouseItemID 
                        
                    END                                                                                                                                         
        END
	 
    ELSE IF @tintFlag = 2 --2:SO/PO Close
            BEGIN
	 	
                PRINT '@OppType=' + CONVERT(VARCHAR(5), @tintOpptype)           
                IF @tintOpptype = 1 
                    BEGIN   
					SET @description='SO Close (Qty:' + CAST(@numUnits AS VARCHAR(10)) + ' Shipped:' +  CAST(@QtyShipped AS VARCHAR(10)) + ')'
                    
                    IF @bitKitParent = 1 
                    BEGIN
                        EXEC USP_UpdatingInventoryForEmbeddedKits @numoppitemtCode,
                            @numOrigUnits, 2,@numOppID,0,@numUserCntID
                    END  
					ELSE IF @bitWorkOrder = 1
					BEGIN
						SET @description='SO-WO Close (Qty:' + CAST(@numUnits AS VARCHAR(10)) + ' Shipped:' +  CAST(@QtyShipped AS VARCHAR(10)) + ')'
						EXEC USP_ManageInventoryAssemblyWO @numUserCntID,@numOppID,@itemcode,@numWareHouseItemID,@QtyShipped,2
					END
                    ELSE
                    BEGIN       
                    
    /* When QtyShipped has been updated form sales fulfillment then it need to be deducted */
							SET @numUnits = @numUnits - @QtyShipped
					  IF @bitAsset=0--Not Asset
							  BEGIN
									SET @onAllocation = @onAllocation - @numUnits 
							  END
					  ELSE--Asset
							  BEGIN
									 SET @onAllocation = @onAllocation  
							  END
											           
		    
							UPDATE  WareHouseItems
							SET     numOnHand = @onHand,
									numAllocation = @onAllocation,
									numBackOrder = @onBackOrder,dtModified = GETDATE() 
							WHERE   numWareHouseItemID = @numWareHouseItemID   
					   END
	
                    END                
                ELSE IF @tintOpptype = 2 
                        BEGIN
					SET @description='PO Close (Qty:' + CAST(@numUnits AS VARCHAR(10)) + ' Received:' +  CAST(@QtyReceived AS VARCHAR(10)) + ')'
                        
        /* When QtyReceived has been updated form purchase fulfillment then it need to be deducted */
                            SET @numUnits = @numUnits - @QtyReceived
            
							IF EXISTS(SELECT 1 FROM dbo.OpportunityMaster WHERE numOppID=@numOppID AND ISNULL(bitStockTransfer,0)=0)
							BEGIN
								--Updating the Average Cost
								IF @TotalOnHand + @numUnits = 0
								BEGIN
									  SET @monAvgCost = @monAvgCost --( ( @TotalOnHand * @monAvgCost ) + (@numUnits * @monPrice))
								END
								ELSE
								BEGIN
									  SET @monAvgCost = ( ( @TotalOnHand * @monAvgCost )
												+ (@numUnits * @monPrice))
								/ ( @TotalOnHand + @numUnits )
								END    
		                            
								UPDATE  item
								SET     monAverageCost = @monAvgCost
								WHERE   numItemCode = @itemcode
							END
		    
--                            SELECT  @OldOnHand = SUM(numonhand)
--                            FROM    WareHouseItems WHI
--                            WHERE   WHI.numitemid = @itemcode     
--                            SET @OldCost = @OldOnHand * @monAvgCost     
      
                            IF @onOrder >= @numUnits 
                                BEGIN            
                                    PRINT 'in @onOrder >= @numUnits '
                                    SET @onOrder = @onOrder - @numUnits            
                                    IF @onBackOrder >= @numUnits 
                                        BEGIN            
                                            PRINT 'in @onBackOrder >= @numUnits '
                                            PRINT @numUnits
                                            SET @onBackOrder = @onBackOrder
                                                - @numUnits            
                                            SET @onAllocation = @onAllocation
                                                + @numUnits            
                                        END            
                                    ELSE 
                                        BEGIN            
                                            SET @onAllocation = @onAllocation
                                                + @onBackOrder            
                                            SET @numUnits = @numUnits
                                                - @onBackOrder            
                                            SET @onBackOrder = 0            
                                            SET @onHand = @onHand + @numUnits            
                                        END         
                                END            
                            ELSE IF @onOrder < @numUnits 
                                    BEGIN            
                                        PRINT 'in @onOrder < @numUnits '
                                        SET @onHand = @onHand + @onOrder
                                        SET @onOrder = @numUnits - @onOrder
                                    END         
   -- To Find New Stock Details    
--    Print '@numUnits='+convert(varchar(5),@numUnits)    
--    Print '@monPrice='+Convert(varchar(5),@monPrice)    
--                            SET @NewCost = @numUnits * @monPrice    
    ---Set @NewAverageCost=(@OldCost+@NewCost)/@onHand    
    --Print '@NewAverageCost='+Convert(varchar(5),@NewAverageCost)    
                            UPDATE  WareHouseItems
                            SET     numOnHand = @onHand,
                                    numAllocation = @onAllocation,
                                    numBackOrder = @onBackOrder,
                                    numOnOrder = @onOrder,dtModified = GETDATE() 
                            WHERE   numWareHouseItemID = @numWareHouseItemID  
			                
						SELECT @dtItemReceivedDate=dtItemReceivedDate FROM OpportunityMaster WHERE numOppId=@numOppId
			                
                --ASk Carl for more Info
--                            UPDATE  Item
--                            SET     monAverageCost = @NewAverageCost
--                            WHERE   numItemCode = @itemcode         
                        END
            END

ELSE IF @tintFlag = 3 --3:SO/PO Re-Open
            BEGIN
	 	
                PRINT '@OppType=' + CONVERT(VARCHAR(5), @tintOpptype)           
                IF @tintOpptype = 1 
                    BEGIN         
					SET @description='SO Re-Open (Qty:' + CAST(@numUnits AS VARCHAR(10)) + ' Shipped:' +  CAST(@QtyShipped AS VARCHAR(10)) + ')'
                    
                    IF @bitKitParent = 1 
                        BEGIN
                            EXEC USP_UpdatingInventoryForEmbeddedKits @numoppitemtCode,
                                @numOrigUnits, 3,@numOppID,0,@numUserCntID
                        END 
					ELSE IF @bitWorkOrder = 1
					BEGIN
						SET @description='SO-WO Re-Open (Qty:' + CAST(@numUnits AS VARCHAR(10)) + ' Shipped:' +  CAST(@QtyShipped AS VARCHAR(10)) + ')'
						EXEC USP_ManageInventoryAssemblyWO @numUserCntID,@numOppID,@itemcode,@numWareHouseItemID,@QtyShipped,3
					END
                     ELSE
                     BEGIN
    /* When QtyShipped has been updated form sales fulfillment then it need to be deducted */
                        SET @numUnits = @numUnits - @QtyShipped
  
					 IF @bitAsset=0--Not Asset
							  BEGIN
									SET @onAllocation = @onAllocation + @numUnits
							  END
					  ELSE--Asset
							  BEGIN
									 SET @onAllocation = @onAllocation 
							  END
                                    
	    
                        UPDATE  WareHouseItems
                        SET     numOnHand = @onHand,
                                numAllocation = @onAllocation,
                                numBackOrder = @onBackOrder,dtModified = GETDATE() 
                        WHERE   numWareHouseItemID = @numWareHouseItemID
					  END
						               
                    END                
                ELSE IF @tintOpptype = 2 
                        BEGIN
					SET @description='PO Re-Open (Qty:' + CAST(@numUnits AS VARCHAR(10)) + ' Received:' +  CAST(@QtyReceived AS VARCHAR(10)) + ')'
                        
        /* When QtyReceived has been updated form purchase fulfillment then it need to be deducted */
                            SET @numUnits = @numUnits - @QtyReceived
            
            
							--Updating the Average Cost
							IF EXISTS(SELECT 1 FROM dbo.OpportunityMaster WHERE numOppID=@numOppID AND ISNULL(bitStockTransfer,0)=0)
							BEGIN
								--IF @onHand + @onOrder - @numUnits <> 0 
								--BEGIN
								IF @TotalOnHand - @numUnits = 0
								BEGIN
									SET @monAvgCost = 0
	--								SET @monAvgCost = ( ( @TotalOnHand * @monAvgCost )
	--                                                     - (@numUnits * @monPrice))
								END
								ELSE
								BEGIN
									SET @monAvgCost = ( ( @TotalOnHand * @monAvgCost )
														 - (@numUnits * @monPrice))
										/ ( @TotalOnHand - @numUnits )
								END        
								--END
							--ELSE 
								--SET @monAvgCost = 0
                            
                            
								 UPDATE  item
								 SET     monAverageCost = @monAvgCost
								 WHERE   numItemCode = @itemcode
							END

                            IF @onHand >= @numUnits 
                                BEGIN     
                                     SET @onHand = @onHand - @numUnits            
                                     SET @onOrder = @onOrder + @numUnits
                                END         
                          
						   UPDATE  WareHouseItems
                            SET     numOnHand = @onHand,
                                    numOnOrder = @onOrder,dtModified = GETDATE() 
                            WHERE   numWareHouseItemID = @numWareHouseItemID  
                            
                        END
            END
            
            
            
             IF @numWareHouseItemID>0
				 BEGIN
					DECLARE @tintRefType AS TINYINT
					
					IF @tintOpptype = 1 --SO
					  SET @tintRefType=3
					ELSE IF @tintOpptype = 2 --PO
					  SET @tintRefType=4
					  
						DECLARE @numDomain AS NUMERIC(18,0)
						SET @numDomain = (SELECT TOP 1 [numDomainID] FROM WareHouseItems WHERE [WareHouseItems].[numWareHouseItemID] = @numWareHouseItemID)

						EXEC dbo.USP_ManageWareHouseItems_Tracking
						@numWareHouseItemID = @numWareHouseItemID, --  numeric(9, 0)
						@numReferenceID = @numOppID, --  numeric(9, 0)
						@tintRefType = @tintRefType, --  tinyint
						@vcDescription = @description, --  varchar(100)
						@numModifiedBy = @numUserCntID,
						@dtRecordDate =  @dtItemReceivedDate,
						@numDomainID = @numDomain
                  END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
                    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ManageInventoryAssemblyWO')
DROP PROCEDURE USP_ManageInventoryAssemblyWO
GO
CREATE PROCEDURE [dbo].[USP_ManageInventoryAssemblyWO]
	@numUserCntID AS NUMERIC(18,0),
	@numOppID AS NUMERIC(9),
	@numItemCode as numeric(9),
	@numWarehouseItemID AS NUMERIC(18,0),
	@QtyShipped AS NUMERIC(18,0),
	@tintMode AS TINYINT=0 --1: Work Order insert/edit 2:Work Order Close 3:Work Order Re-Open 4: Work Order Delete 5: Work Order Edit(Revert Before New Insert)
AS
BEGIN
	/*  
		WHEN WORK ORDER IS CREATED FOR ASSEMBY THEN BOTH SO AND WO PROCESS ARE CARRIED OUT SEPERATELY.
		FOR EXAMPLE: LETS SAY WE HAVE "ASSEMBY ITEM" WHICH REQUIRED 2 QTY OF "ASSEMBLY SUB ITEM" 
		CURRENT INVENTORY STATUS FOR PARENT ITEM: ONHAND:1 ONORDER:0 ONALLOCATION:0 ONBACKORDER:0
		CURRENT INVENTORY STATUS FOR CHILD ITEM: ONHAND:4 ONORDER:0 ONALLOCATION:0 ONBACKORDER:0

		-- WHEN WORK ORDER IS CREATED FOR 1 QTY:
		CURRENT INVENTORY STATUS FOR PARENT ITEM: ONHAND:0 ONORDER:1 ONALLOCATION:1 ONBACKORDER:0
		CURRENT INVENTORY STATUS FOR CHILD ITEM: ONHAND:2 ONORDER:0 ONALLOCATION:2 ONBACKORDER:0

		HERE PARENT ITEM HAVE BOTH SO AND WO IMPACT. SO ONHAND DECREASE AND ONALLOCATION IS INCREASED
		BUT ITS WORK ORDER SO WE HAVE TO INCERASE ONORDER ALSO BY QTY. CHILD ITEM HAVE ONLY WO IMPACT MEANS 
		ONHAND IS DECREASE AND ONALLOCATION IS INCREASED

		-- WHEN ASSEMBLY IS COMPLETED
		CURRENT INVENTORY STATUS FOR PARENT ITEM: ONHAND:1 ONORDER:0 ONALLOCATION:1 ONBACKORDER:0
		CURRENT INVENTORY STATUS FOR CHILD ITEM: ONHAND:2 ONORDER:0 ONALLOCATION:0 ONBACKORDER:0

		-- WHEN WORK ORDER IS CLOSED:
		CURRENT INVENTORY STATUS FOR PARENT ITEM: ONHAND:1 ONORDER:0 ONALLOCATION:0 ONBACKORDER:0
		CURRENT INVENTORY STATUS FOR CHILD ITEM: ONHAND:2 ONORDER:0 ONALLOCATION:0 ONBACKORDER:0
	*/
DECLARE @numWOID AS NUMERIC(18,0)
DECLARE @numParentWOID AS NUMERIC(18,0)
DECLARE	@numDomainID AS NUMERIC(18,0)
DECLARE @vcInstruction AS VARCHAR(1000)
DECLARE @bintCompliationDate DATETIME
DECLARE @numAssemblyQty NUMERIC(18,0) 
DECLARE @numWOStatus AS NUMERIC(18,0)
DECLARE @numWOAssignedTo AS NUMERIC(18,0)

SET XACT_ABORT ON;

BEGIN TRY 
BEGIN TRAN
	SELECT 
		@numWOID=numWOId,@numAssemblyQty=numQtyItemsReq,@numDomainID=numDomainID,
		@numWOStatus=ISNULL(numWOStatus,0),@vcInstruction=vcInstruction,
		@bintCompliationDate= bintCompliationDate, @numWOAssignedTo=numAssignedTo,@numParentWOID=ISNULL(numParentWOID,0)
	FROM 
		WorkOrder 
	WHERE 
		numOppId=@numOppID AND numItemCode=@numItemCode AND numWareHouseItemId=@numWarehouseItemID

	--IF WORK ORDER IS FOUND FOR ITEM AND QTY IS GREATER THEN 0 THEN PROCESS INVENTORY
	IF ISNULL(@numWOID,0) > 0 AND ISNULL(@numAssemblyQty,0) > 0
	BEGIN
		
		DECLARE @TEMPITEM AS TABLE
		(
			RowNo INT NOT NULL identity(1,1),
			numItemCode INT,
			numQty INT,
			numOrgQtyRequired INT,
			numWarehouseItemID INT,
			bitAssembly BIT
		)

		INSERT INTO @TEMPITEM SELECT numChildItemID,numQtyItemsReq,numQtyItemsReq_Orig,numWareHouseItemId,Item.bitAssembly FROM WorkOrderDetails INNER JOIN Item ON WorkOrderDetails.numChildItemID=Item.numItemCode WHERE numWOId = @numWOID AND ISNULL(numWareHouseItemId,0) > 0

		DECLARE @i AS INT = 1
		DECLARE @Count AS INT

		SELECT @Count = COUNT(RowNo) FROM @TEMPITEM

		--IF ASSEMBLY SUB ITEMS ARE AVAILABLE THEN UPDATE INVENTORY OF SUB ITEMS
		IF ISNULL(@Count,0) > 0
		BEGIN
			DECLARE @Description AS VARCHAR(500)
			DECLARE @numChildItemCode AS INT
			DECLARE @numChildItemQty AS INT
			DECLARE @numChildOrgQtyRequired AS INT
			DECLARE @bitChildIsAssembly AS BIT
			DECLARE @numQuantityToBuild AS INT
			DECLARE @numChildItemWarehouseItemID AS INT
			DECLARE @numOnHand AS INT
			DECLARE @numOnOrder AS INT
			DECLARE @numOnAllocation AS INT
			DECLARE @numOnBackOrder AS INT

			--THIS BLOCKS UPDATE INVENTORY LEVEL OF TOP LEVEL PARENT ONLY AND NOT CALLED FOR ITS RECURSIVE CHILD
			IF @numParentWOID = 0
			BEGIN
				--GET ASSEMBY ITEM INVENTORY
				SELECT
					@numOnHand = ISNULL(numOnHand, 0),
					@numOnAllocation = ISNULL(numAllocation, 0),
					@numOnOrder = ISNULL(numOnOrder, 0),
					@numOnBackOrder = ISNULL(numBackOrder, 0)
				FROM
					WareHouseItems
				WHERE
					numWareHouseItemID = @numWarehouseItemID

				IF @tintMode = 1 --WO INSERT
				BEGIN
					-- IF HAVE ENOUGH ITEM QTY IN WAREHOUSE THEN 
					-- DECREASE ONHAND BY QTY AND 
					-- INCREASE ONALLOCATION BY QTY
					-- ONORDER GOES UP BY ASSEMBLY QTY
					IF @numOnHand >= @numAssemblyQty 
					BEGIN      
						SET @numOnHand = @numOnHand - @numAssemblyQty
						SET @numOnOrder = @numOnOrder + @numAssemblyQty
						SET @numOnAllocation = @numOnAllocation + @numAssemblyQty

						UPDATE 
							WareHouseItems
						SET    
							numOnHand = @numOnHand,
							numOnOrder= @numOnOrder,
							numAllocation = @numOnAllocation,
							dtModified = GETDATE() 
						WHERE   
							numWareHouseItemID = @numWareHouseItemID                                                              
					END    
					-- IF DO NOT HAVE ENOUGH ITEM QTY IN WAREHOUSE THEN 
					-- INCREASE ONALLOCATION BY QTY WE HAVE IN WAREHOUSE AND
					-- REMAINING QTY AFTER ALLLOCATING FROM ONHAND WILL GO TO BACKORDER
					-- SET ONHAND TO 0 BECAUSE NOTHING LEFT NOW IN WAREHOUSE FOR ITEM 
					-- ONORDER GOES UP BY ASSEMBLY QTY				 
					ELSE IF @numOnHand < @numAssemblyQty 
					BEGIN      
						SET @numOnAllocation = @numOnAllocation + @numOnHand
						SET @numOnBackOrder = @numOnBackOrder + (@numAssemblyQty - @numOnHand)
						SET @numOnOrder = @numOnOrder + @numAssemblyQty
						SET @numOnHand = 0
 
						UPDATE 
							WareHouseItems
						SET    
							numAllocation = @numOnAllocation,
							numBackOrder = @numOnBackOrder,
							numOnHand = @numOnHand,		
							numOnOrder= @numOnOrder,				
							dtModified = GETDATE() 
						WHERE   
							numWareHouseItemID = @numWareHouseItemID                                     
					END 
				--UPDATE ONORDER QTY OF ASSEMBLY ITEM BY QTY
				END
				ELSE IF @tintMode = 2 --WO CLOSE
				BEGIN
					--REMOVE SHIPPED QTY FROM ORIGIAL QUANTITY
					SET @numAssemblyQty = ISNULL(@numAssemblyQty,0) - ISNULL(@QtyShipped,0)

					--RELASE ALLOCATION
					SET @numOnAllocation = @numOnAllocation - @numAssemblyQty 
											           
					UPDATE  
						WareHouseItems
					SET     
						numAllocation = @numOnAllocation,
						dtModified = GETDATE() 
					WHERE   
						numWareHouseItemID = @numWareHouseItemID   
				END
				ELSE IF @tintMode = 3 --WO REOPEN
				BEGIN
					--REMOVE SHIPPED QTY FROM ORIGIAL QUANTITY
					SET @numAssemblyQty = @numAssemblyQty - @QtyShipped
  
					--ADD ITEM TO ALLOCATION
					SET @numOnAllocation = @numOnAllocation + @numAssemblyQty
                                    
					UPDATE  
						WareHouseItems
					SET     
						numAllocation = @numOnAllocation,
						dtModified = GETDATE() 
					WHERE   
						numWareHouseItemID = @numWareHouseItemID
				END
				ELSE IF (@tintMode = 4 OR  @tintMode=5) --WO DELETE OR EDIT
				BEGIN
					-- CHECK WHETHER ANY QTY IS SHIPPED OR NOT
					IF @QtyShipped > 0
					BEGIN
						IF @tintMode=4
								SET @numAssemblyQty = @numAssemblyQty - @QtyShipped
						ELSE IF @tintmode=5
								SET @numOnAllocation = @numOnAllocation + @QtyShipped 
					END 
	
					-- IF BACKORDER QTY IS GREATER THEN QTY TO REVERT THEN
					-- DECREASE BACKORDER QTY BY QTY TO REVERT
					IF @numAssemblyQty < @numOnBackOrder 
					BEGIN                  
						SET @numOnBackOrder = @numOnBackOrder - @numAssemblyQty
					END 
					-- IF BACKORDER QTY IS LESS THEN OR EQUAL TO QTY TO REVERT THEN
					-- DECREASE QTY TO REVERT BY QTY OF BACK ORDER AND
					-- SET BACKORDER QTY TO 0
					ELSE IF @numAssemblyQty >= @numOnBackOrder 
					BEGIN
						SET @numAssemblyQty = @numAssemblyQty - @numOnBackOrder
						SET @numOnBackOrder = 0
                        
						--REMOVE ITEM FROM ALLOCATION 
						IF (@numOnAllocation - @numAssemblyQty) >= 0
							SET @numOnAllocation = @numOnAllocation - @numAssemblyQty
						
						--ADD QTY TO ONHAND
						SET @numOnHand = @numOnHand + @numAssemblyQty
					END

					--THIS IF BLOCK SHOULD BE LAST OTHERWISE IT WILL CREATE PROBLEM IN INVENTORY LEVEL
					--IF WORK ORDER IS NOT COMPLETED THEN REVERT 
					--QTY FROM ONORDER BECAUSE WE HAVE TO REVERT
					IF @numWOStatus <> 23184
					BEGIN
						--DECREASE ONORDER BY ASSEMBLY QTY
						SET @numOnOrder = @numOnOrder - @numAssemblyQty
					END
					ELSE
					BEGIN
						--DECREASE ONHAND BY ASSEMBLY QTY
						--BECAUSE WHEN WORK ORDER IS COMPLETED ONHAND QTY OF PARENT IS INCREASED
						SET @numOnHand = @numOnHand - @numAssemblyQty
					END

					UPDATE 
						WareHouseItems
					SET    
						numOnHand = @numOnHand,
						numAllocation = @numOnAllocation,
						numBackOrder = @numOnBackOrder,
						numOnOrder = @numOnOrder,
						dtModified = GETDATE() 
					WHERE   
						numWareHouseItemID = @numWareHouseItemID				
				END
			END

			--CHILD ITEM INVENTORY IS CHANGED ONLY WHEN SALES ORDER WITH WORK ORDER IS INSERTED OR DELETED
			--THE INVENTORY MANGMENT FOR DELETE ACTION IS HANDLED FROM STORE PROCEDURE USP_DeleteAssemblyWOAndInventory
			IF @tintMode = 1
			BEGIN
				
				WHILE @i <= @Count
				BEGIN
					DECLARE @numNewOppID AS NUMERIC(18,0) = 0
					DECLARE @QtyToBuild AS NUMERIC(18,0)
					DECLARE @numQtyShipped AS NUMERIC(18,0)

					--GET CHILD ITEM DETAIL FROM TEMP TABLE
					SELECT @numChildItemCode=numItemCode,@numChildItemWarehouseItemID=numWarehouseItemID,@numChildItemQty=numQty,@numChildOrgQtyRequired=numOrgQtyRequired,@bitChildIsAssembly=bitAssembly FROM @TEMPITEM WHERE RowNo = @i
					
					--GET CHILD ITEM CURRENT INVENTORY DETAIL
					SELECT  
						@numOnHand = ISNULL(numOnHand, 0),
						@numOnAllocation = ISNULL(numAllocation, 0),
						@numOnOrder = ISNULL(numOnOrder, 0),
						@numOnBackOrder = ISNULL(numBackOrder, 0)
					FROM    
						WareHouseItems
					WHERE   
						numWareHouseItemID = @numChildItemWarehouseItemID
					
					IF ISNULL(@numParentWOID,0) = 0
					BEGIN
						SET @Description='Items Allocated For SO-WO (Qty:' + CAST(@numChildItemQty AS VARCHAR(10)) + ' Shipped:' +  CAST((@QtyShipped * @numChildOrgQtyRequired) AS VARCHAR(10)) + ')'
					END
					ELSE
					BEGIN
						SET @Description='Items Allocated For SO-WO Work Order (Qty:' + CAST(@numChildItemQty AS VARCHAR(10)) + ' Shipped:' +  CAST((@QtyShipped * @numChildOrgQtyRequired) AS VARCHAR(10)) + ')'
					END
					
					SET @numChildItemQty = @numChildItemQty - (@QtyShipped * @numChildOrgQtyRequired)
						
					--IF CHILD ITEM IS ASSEMBLY THEN SAME LOGIC LIKE PARENT IS APPLIED
					IF ISNULL(@bitChildIsAssembly,0) = 1
					BEGIN
						-- IF HAVE ENOUGH ITEM QTY IN WAREHOUSE THEN 
						-- DECREASE ONHAND BY QTY AND 
						-- INCREASE ONALLOCATION BY QTY
						-- ONORDER GOES UP BY ASSEMBLY QTY
						IF @numOnHand >= @numChildItemQty 
						BEGIN      
							SET @numOnHand = @numOnHand - @numChildItemQty
							SET @numOnAllocation = @numOnAllocation + @numChildItemQty  
							
							--DONOT PLACE THIS LOGIC AT BOTTOM BECAUSE WORK ORDER ARE CREATED RECURSIVELY WHICH CREATE PROBLEM IN WAREHOUSE TRAKING ENTRIES
							--UPDATE WAREHOUSE INVENTORY AND WAREHOUSE HISTORY
							EXEC USP_UpdateInventoryAndTracking 
							@numOnHand=@numOnHand,
							@numOnAllocation=@numOnAllocation,
							@numOnBackOrder=@numOnBackOrder,
							@numOnOrder=@numOnOrder,
							@numWarehouseItemID=@numChildItemWarehouseItemID,
							@numReferenceID=@numOppID,
							@tintRefType=3,
							@numDomainID=@numDomainID,
							@numUserCntID=@numUserCntID,
							@Description=@Description                                                         
						END    
						-- IF DO NOT HAVE ENOUGH ITEM QTY IN WAREHOUSE THEN 
						-- INCREASE ONALLOCATION BY QTY WE HAVE IN WAREHOUSE AND
						-- REMAINING QTY AFTER ALLLOCATING FROM ONHAND WILL GO TO BACKORDER
						-- SET ONHAND TO 0 BECAUSE NOTHING LEFT NOW IN WAREHOUSE FOR ITEM 
						-- ONORDER GOES UP BY ASSEMBLY QTY				 
						ELSE IF @numOnHand < @numChildItemQty 
						BEGIN      
							SET @numOnAllocation = @numOnAllocation + @numOnHand
							SET @numOnBackOrder = @numOnBackOrder + (@numChildItemQty - @numOnHand)
							SET @QtyToBuild = (@numChildItemQty - @numOnHand)
							SET @numQtyShipped = (@QtyShipped * @numChildOrgQtyRequired)
							SET @numOnHand = 0   

							--DONOT PLACE THIS LOGIC AT BOTTOM BECAUSE WORK ORDER ARE CREATED RECURSIVELY WHICH CREATE PROBLEM IN WAREHOUSE TRAKING ENTRIES
							--UPDATE WAREHOUSE INVENTORY AND WAREHOUSE HISTORY
							EXEC USP_UpdateInventoryAndTracking 
							@numOnHand=@numOnHand,
							@numOnAllocation=@numOnAllocation,
							@numOnBackOrder=@numOnBackOrder,
							@numOnOrder=@numOnOrder,
							@numWarehouseItemID=@numChildItemWarehouseItemID,
							@numReferenceID=@numOppID,
							@tintRefType=3,
							@numDomainID=@numDomainID,
							@numUserCntID=@numUserCntID,
							@Description=@Description
								
							EXEC USP_WorkOrder_InsertRecursive @numOppID,@numChildItemCode,@QtyToBuild,@numChildItemWarehouseItemID,@numDomainID,@numUserCntID,@numQtyShipped,@numWOID,0
						END 
					END
					ELSE
					BEGIN
						-- IF HAVE ENOUGH ITEM QTY IN WAREHOUSE THEN 
						-- DECREASE ONHAND BY QTY AND 
						-- INCREASE ONALLOCATION BY QTY
						IF @numOnHand >= @numChildItemQty 
						BEGIN                                    
							SET @numOnHand = @numOnHand - @numChildItemQty                            
							SET @numOnAllocation = @numOnAllocation + @numChildItemQty 
							
							--DONOT PLACE THIS LOGIC AT BOTTOM BECAUSE WORK ORDER ARE CREATED RECURSIVELY WHICH CREATE PROBLEM IN WAREHOUSE TRAKING ENTRIES
							--UPDATE WAREHOUSE INVENTORY AND WAREHOUSE HISTORY
							EXEC USP_UpdateInventoryAndTracking 
							@numOnHand=@numOnHand,
							@numOnAllocation=@numOnAllocation,
							@numOnBackOrder=@numOnBackOrder,
							@numOnOrder=@numOnOrder,
							@numWarehouseItemID=@numChildItemWarehouseItemID,
							@numReferenceID=@numOppID,
							@tintRefType=3,
							@numDomainID=@numDomainID,
							@numUserCntID=@numUserCntID,
							@Description=@Description                                   
						END    
						-- IF DO NOT HAVE ENOUGH ITEM QTY IN WAREHOUSE THEN 
						-- INCREASE ONALLOCATION BY QTY WE HAVE IN WAREHOUSE AND
						-- REMAINING QTY AFTER ALLLOCATING FROM ONHAND WILL GO TO BACKORDER
						-- SET ONHAND TO 0 BECAUSE NOTHING LEFT NOW IN WAREHOUSE FOR ITEM 				 
						ELSE IF @numOnHand < @numChildItemQty 
						BEGIN     
								
							SET @numOnAllocation = @numOnAllocation + @numOnHand  
							SET @numOnBackOrder = @numOnBackOrder + (@numChildItemQty - @numOnHand) 
							SET @QtyToBuild = (@numChildItemQty - @numOnHand) 
							SET @numOnHand = 0 

							--DONOT PLACE THIS LOGIC AT BOTTOM BECAUSE WORK ORDER ARE CREATED RECURSIVELY WHICH CREATE PROBLEM IN WAREHOUSE TRAKING ENTRIES
							--UPDATE WAREHOUSE INVENTORY AND WAREHOUSE HISTORY
							EXEC USP_UpdateInventoryAndTracking 
							@numOnHand=@numOnHand,
							@numOnAllocation=@numOnAllocation,
							@numOnBackOrder=@numOnBackOrder,
							@numOnOrder=@numOnOrder,
							@numWarehouseItemID=@numChildItemWarehouseItemID,
							@numReferenceID=@numOppID,
							@tintRefType=3,
							@numDomainID=@numDomainID,
							@numUserCntID=@numUserCntID,
							@Description=@Description

							--CREATE PURCHASE ORDER FOR QUANTITY WHICH GOES ON BACKORDER
							EXEC USP_OpportunityMaster_CreatePO @numNewOppID OUTPUT,@numDomainID,@numUserCntID,@numChildItemCode,@QtyToBuild,@numChildItemWarehouseItemID,1
								
							UPDATE WorkOrderDetails SET numPOID= @numNewOppID WHERE numWOId=@numWOID AND numChildItemID=@numChildItemCode AND numWareHouseItemId=@numChildItemWarehouseItemID                                 
						END 
					END
 
					
					SET @i = @i + 1
				END
			END
		END
	END
COMMIT TRAN 
END TRY 
BEGIN CATCH		
    IF ( @@TRANCOUNT > 0 ) 
    BEGIN
        ROLLBACK TRAN
    END

	DECLARE @ErrorMessage NVARCHAR(4000);
    DECLARE @ErrorSeverity INT;
    DECLARE @ErrorState INT;

    SELECT @ErrorMessage = ERROR_MESSAGE(), @ErrorSeverity = ERROR_SEVERITY(), @ErrorState = ERROR_STATE();

    RAISERROR (@ErrorMessage, @ErrorSeverity, @ErrorState);
END CATCH	

SET XACT_ABORT OFF;
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ManageInventoryWorkOrder')
DROP PROCEDURE USP_ManageInventoryWorkOrder
GO
CREATE PROCEDURE [dbo].[USP_ManageInventoryWorkOrder]
@numWOId AS NUMERIC(18,0),
@numDomainID AS NUMERIC(18,0),
@numUserCntID AS NUMERIC(18,0)
AS
BEGIN
BEGIN TRY 
BEGIN TRAN
		
	DECLARE @TEMPITEM AS TABLE
	(
		RowNo INT NOT NULL identity(1,1),
		numItemCode INT,
		numQty INT,
		numOrgQtyRequired INT,
		numWarehouseItemID INT,
		bitAssembly BIT
	)

	INSERT INTO @TEMPITEM SELECT numChildItemID,numQtyItemsReq,numQtyItemsReq_Orig,numWareHouseItemId,Item.bitAssembly FROM WorkOrderDetails INNER JOIN Item ON WorkOrderDetails.numChildItemID=Item.numItemCode WHERE numWOId = @numWOID AND ISNULL(numWareHouseItemId,0) > 0

	DECLARE @i AS INT = 1
	DECLARE @Count AS INT

	SELECT @Count = COUNT(RowNo) FROM @TEMPITEM

	--IF ASSEMBLY SUB ITEMS ARE AVAILABLE THEN UPDATE INVENTORY OF SUB ITEMS
	IF ISNULL(@Count,0) > 0
	BEGIN
		DECLARE @Description AS VARCHAR(1000)
		DECLARE @numChildItemCode AS INT
		DECLARE @numChildItemQty AS INT
		DECLARE @numChildOrgQtyRequired AS INT
		DECLARE @bitChildIsAssembly AS BIT
		DECLARE @numQuantityToBuild AS INT
		DECLARE @numChildItemWarehouseItemID AS INT
		DECLARE @numOnHand AS INT
		DECLARE @numOnOrder AS INT
		DECLARE @numOnAllocation AS INT
		DECLARE @numOnBackOrder AS INT

		WHILE @i <= @Count
		BEGIN
			DECLARE @numNewOppID AS NUMERIC(18,0) = 0
			DECLARE @QtyToBuild AS NUMERIC(18,0)
			DECLARE @numQtyShipped AS NUMERIC(18,0)

			--GET CHILD ITEM DETAIL FROM TEMP TABLE
			SELECT @numChildItemCode=numItemCode,@numChildItemWarehouseItemID=numWarehouseItemID,@numChildItemQty=numQty,@numChildOrgQtyRequired=numOrgQtyRequired,@bitChildIsAssembly=bitAssembly FROM @TEMPITEM WHERE RowNo = @i
					
			--GET CHILD ITEM CURRENT INVENTORY DETAIL
			SELECT  
				@numOnHand = ISNULL(numOnHand, 0),
				@numOnAllocation = ISNULL(numAllocation, 0),
				@numOnOrder = ISNULL(numOnOrder, 0),
				@numOnBackOrder = ISNULL(numBackOrder, 0)
			FROM    
				WareHouseItems
			WHERE   
				numWareHouseItemID = @numChildItemWarehouseItemID	

			SET @Description='Items Allocated For Work Order (Qty:' + CAST(@numChildItemQty AS VARCHAR(10)) + ')'
						
			--IF CHILD ITEM IS ASSEMBLY THEN SAME LOGIC LIKE PARENT IS APPLIED
			IF ISNULL(@bitChildIsAssembly,0) = 1
			BEGIN
				-- IF HAVE ENOUGH ITEM QTY IN WAREHOUSE THEN 
				-- DECREASE ONHAND BY QTY AND 
				-- INCREASE ONALLOCATION BY QTY
				-- ONORDER GOES UP BY ASSEMBLY QTY
				IF @numOnHand >= @numChildItemQty 
				BEGIN      
					SET @numOnHand = @numOnHand - @numChildItemQty
					SET @numOnAllocation = @numOnAllocation + @numChildItemQty  
					
					--DONOT PLACE THIS LOGIC AT BOTTOM BECAUSE WORK ORDER ARE CREATED RECURSIVELY WHICH CREATE PROBLEM IN WAREHOUSE TRAKING ENTRIES
					--UPDATE WAREHOUSE INVENTORY AND WAREHOUSE HISTORY
					EXEC USP_UpdateInventoryAndTracking 
					@numOnHand=@numOnHand,
					@numOnAllocation=@numOnAllocation,
					@numOnBackOrder=@numOnBackOrder,
					@numOnOrder=@numOnOrder,
					@numWarehouseItemID=@numChildItemWarehouseItemID,
					@numReferenceID=@numWOId,
					@tintRefType=2,
					@numDomainID=@numDomainID,
					@numUserCntID=@numUserCntID,
					@Description=@Description                                                         
				END    
				-- IF DO NOT HAVE ENOUGH ITEM QTY IN WAREHOUSE THEN 
				-- INCREASE ONALLOCATION BY QTY WE HAVE IN WAREHOUSE AND
				-- REMAINING QTY AFTER ALLLOCATING FROM ONHAND WILL GO TO BACKORDER
				-- SET ONHAND TO 0 BECAUSE NOTHING LEFT NOW IN WAREHOUSE FOR ITEM 
				-- ONORDER GOES UP BY ASSEMBLY QTY				 
				ELSE IF @numOnHand < @numChildItemQty 
				BEGIN      
					SET @numOnAllocation = @numOnAllocation + @numOnHand
					SET @numOnBackOrder = @numOnBackOrder + (@numChildItemQty - @numOnHand)
					SET @QtyToBuild = (@numChildItemQty - @numOnHand)
					SET @numQtyShipped = 0
					SET @numOnHand = 0   
					
					--DONOT PLACE THIS LOGIC AT BOTTOM BECAUSE WORK ORDER ARE CREATED RECURSIVELY WHICH CREATE PROBLEM IN WAREHOUSE TRAKING ENTRIES
					--UPDATE WAREHOUSE INVENTORY AND WAREHOUSE HISTORY	
					EXEC USP_UpdateInventoryAndTracking 
					@numOnHand=@numOnHand,
					@numOnAllocation=@numOnAllocation,
					@numOnBackOrder=@numOnBackOrder,
					@numOnOrder=@numOnOrder,
					@numWarehouseItemID=@numChildItemWarehouseItemID,
					@numReferenceID=@numWOId,
					@tintRefType=2,
					@numDomainID=@numDomainID,
					@numUserCntID=@numUserCntID,
					@Description=@Description 

					EXEC USP_WorkOrder_InsertRecursive 0,@numChildItemCode,@QtyToBuild,@numChildItemWarehouseItemID,@numDomainID,@numUserCntID,@numQtyShipped,@numWOID,1
				END 
			END
			ELSE
			BEGIN
				-- IF HAVE ENOUGH ITEM QTY IN WAREHOUSE THEN 
				-- DECREASE ONHAND BY QTY AND 
				-- INCREASE ONALLOCATION BY QTY
				IF @numOnHand >= @numChildItemQty 
				BEGIN                                    
					SET @numOnHand = @numOnHand - @numChildItemQty                            
					SET @numOnAllocation = @numOnAllocation + @numChildItemQty        
					
					--DONOT PLACE THIS LOGIC AT BOTTOM BECAUSE WORK ORDER ARE CREATED RECURSIVELY WHICH CREATE PROBLEM IN WAREHOUSE TRAKING ENTRIES
					--UPDATE WAREHOUSE INVENTORY AND WAREHOUSE HISTORY
					EXEC USP_UpdateInventoryAndTracking 
					@numOnHand=@numOnHand,
					@numOnAllocation=@numOnAllocation,
					@numOnBackOrder=@numOnBackOrder,
					@numOnOrder=@numOnOrder,
					@numWarehouseItemID=@numChildItemWarehouseItemID,
					@numReferenceID=@numWOId,
					@tintRefType=2,
					@numDomainID=@numDomainID,
					@numUserCntID=@numUserCntID,
					@Description=@Description                             
				END    
				-- IF DO NOT HAVE ENOUGH ITEM QTY IN WAREHOUSE THEN 
				-- INCREASE ONALLOCATION BY QTY WE HAVE IN WAREHOUSE AND
				-- REMAINING QTY AFTER ALLLOCATING FROM ONHAND WILL GO TO BACKORDER
				-- SET ONHAND TO 0 BECAUSE NOTHING LEFT NOW IN WAREHOUSE FOR ITEM 				 
				ELSE IF @numOnHand < @numChildItemQty 
				BEGIN     
								
					SET @numOnAllocation = @numOnAllocation + @numOnHand  
					SET @numOnBackOrder = @numOnBackOrder + (@numChildItemQty - @numOnHand) 
					SET @QtyToBuild = (@numChildItemQty - @numOnHand) 
					SET @numOnHand = 0 

					--DONOT PLACE THIS LOGIC AT BOTTOM BECAUSE WORK ORDER ARE CREATED RECURSIVELY WHICH CREATE PROBLEM IN WAREHOUSE TRAKING ENTRIES
					--UPDATE WAREHOUSE INVENTORY AND WAREHOUSE HISTORY
					EXEC USP_UpdateInventoryAndTracking 
					@numOnHand=@numOnHand,
					@numOnAllocation=@numOnAllocation,
					@numOnBackOrder=@numOnBackOrder,
					@numOnOrder=@numOnOrder,
					@numWarehouseItemID=@numChildItemWarehouseItemID,
					@numReferenceID=@numWOId,
					@tintRefType=2,
					@numDomainID=@numDomainID,
					@numUserCntID=@numUserCntID,
					@Description=@Description 

					--CREATE PURCHASE ORDER FOR QUANTITY WHICH GOES ON BACKORDER
					EXEC USP_OpportunityMaster_CreatePO @numNewOppID OUTPUT,@numDomainID,@numUserCntID,@numChildItemCode,@QtyToBuild,@numChildItemWarehouseItemID,1
								
					UPDATE WorkOrderDetails SET numPOID= @numNewOppID WHERE numWOId=@numWOID AND numChildItemID=@numChildItemCode AND numWareHouseItemId=@numChildItemWarehouseItemID                                 
				END 
			END
					
			SET @i = @i + 1
		END
	END
COMMIT TRAN 
END TRY 
BEGIN CATCH		
    IF ( @@TRANCOUNT > 0 ) 
    BEGIN
        ROLLBACK TRAN
    END

	DECLARE @ErrorMessage NVARCHAR(4000);
    DECLARE @ErrorSeverity INT;
    DECLARE @ErrorState INT;

    SELECT @ErrorMessage = ERROR_MESSAGE(), @ErrorSeverity = ERROR_SEVERITY(), @ErrorState = ERROR_STATE();

    RAISERROR (@ErrorMessage, @ErrorSeverity, @ErrorState);
END CATCH	
END
GO
/****** Object:  StoredProcedure [dbo].[USP_ManageItemsAndKits]    Script Date: 02/15/2010 21:44:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created By Anoop Jayaraj                                                          
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_manageitemsandkits')
DROP PROCEDURE usp_manageitemsandkits
GO
CREATE PROCEDURE [dbo].[USP_ManageItemsAndKits]                                                                
@numItemCode as numeric(9) output,                                                                
@vcItemName as varchar(300),                                                                
@txtItemDesc as varchar(1000),                                                                
@charItemType as char(1),                                                                
@monListPrice as money,                                                                
@numItemClassification as numeric(9),                                                                
@bitTaxable as bit,                                                                
@vcSKU as varchar(50),                                                                                                                                      
@bitKitParent as bit,                                                                                   
--@dtDateEntered as datetime,                                                                
@numDomainID as numeric(9),                                                                
@numUserCntID as numeric(9),                                                                                                  
@numVendorID as numeric(9),                                                            
@bitSerialized as bit,                                               
@strFieldList as text,                                            
@vcModelID as varchar(200)='' ,                                            
@numItemGroup as numeric(9)=0,                                      
@numCOGSChartAcntId as numeric(9)=0,                                      
@numAssetChartAcntId as numeric(9)=0,                                      
@numIncomeChartAcntId as numeric(9)=0,                            
@monAverageCost as money,                          
@monLabourCost as money,                  
@fltWeight as float,                  
@fltHeight as float,                  
@fltLength as float,                  
@fltWidth as float,                  
@bitFreeshipping as bit,                
@bitAllowBackOrder as bit,              
@UnitofMeasure as varchar(30)='',              
@strChildItems as text,            
@bitShowDeptItem as bit,              
@bitShowDeptItemDesc as bit,        
@bitCalAmtBasedonDepItems as bit,    
@bitAssembly as BIT,
@intWebApiId INT=null,
@vcApiItemId AS VARCHAR(50)=null,
@numBarCodeId as VARCHAR(25)=null,
@vcManufacturer as varchar(250)=NULL,
@numBaseUnit AS NUMERIC(18)=0,
@numPurchaseUnit AS NUMERIC(18)=0,
@numSaleUnit AS NUMERIC(18)=0,
@bitLotNo as BIT,
@IsArchieve AS BIT,
@numItemClass as numeric(9)=0,
@tintStandardProductIDType AS TINYINT=0,
@vcExportToAPI VARCHAR(50),
@numShipClass NUMERIC,
@ProcedureCallFlag SMALLINT =0,
@vcCategories AS VARCHAR(2000) = '',
@bitAllowDropShip AS BIT=0,
@bitArchiveItem AS BIT = 0,
@bitAsset AS BIT = 0,
@bitRental AS BIT = 0
as                                                                
declare @hDoc as INT

if @numCOGSChartAcntId=0 set @numCOGSChartAcntId=null                      
if  @numAssetChartAcntId=0 set @numAssetChartAcntId=null  
if  @numIncomeChartAcntId=0 set @numIncomeChartAcntId=null     

DECLARE @ParentSKU VARCHAR(50)                                        
DECLARE @ItemID AS NUMERIC(9)
DECLARE @cnt AS INT

--if @dtDateEntered = 'Jan  1 1753 12:00:00:000AM' set @dtDateEntered=null 

IF ISNULL(@charItemType,'') = '' OR ISNULL(@charItemType,'') = '0'  SET @charItemType = 'N'

IF @intWebApiId = 2 
    AND LEN(ISNULL(@vcApiItemId, '')) > 0
    AND ISNULL(@numItemGroup, 0) > 0 
    BEGIN      
    
        -- check wether this id already Mapped in ITemAPI 
        SELECT  @cnt = COUNT([numItemID])
        FROM    [ItemAPI]
        WHERE   [WebApiId] = @intWebApiId
                AND [numDomainId] = @numDomainID
                AND [vcAPIItemID] = @vcApiItemId
        IF @cnt > 0 
            BEGIN
                SELECT  @ItemID = [numItemID]
                FROM    [ItemAPI]
                WHERE   [WebApiId] = @intWebApiId
                        AND [numDomainId] = @numDomainID
                        AND [vcAPIItemID] = @vcApiItemId
        --Update Existing Product coming from API
                SET @numItemCode = @ItemID
            END
    
    END

ELSE 
    IF @intWebApiId > 1 --AND  @intWebApiId <> 2
        AND LEN(ISNULL(@vcSKU, '')) > 0 
        BEGIN
            SET @ParentSKU = @vcSKU
            --DECLARE @ItemID AS NUMERIC(9)
            --DECLARE @cnt AS INT
    
       -- check wether this id already exist in Domain
       
            SELECT  @cnt = COUNT([numItemCode])
            FROM    [Item]
            WHERE   [numDomainId] = @numDomainID
                    AND ( vcSKU = @vcSKU
                          OR @vcSKU IN ( SELECT vcWHSKU
                                         FROM   dbo.WareHouseItems
                                         WHERE  numItemID = Item.[numItemCode]
                                                AND vcWHSKU = @vcSKU )
                        )
            IF @cnt > 0 
                BEGIN
                    SELECT  @ItemID = [numItemCode],
                            @ParentSKU = vcSKU
                    FROM    [Item]
                    WHERE   [numDomainId] = @numDomainID
                            AND ( vcSKU = @vcSKU
                                  OR @vcSKU IN (
                                  SELECT    vcWHSKU
                                  FROM      dbo.WareHouseItems
                                  WHERE     numItemID = Item.[numItemCode]
                                            AND vcWHSKU = @vcSKU )
                                )
                    SET @numItemCode = @ItemID
                END
            ELSE 
                BEGIN
			
    -- check wether this id already Mapped in ITemAPI 
                    SELECT  @cnt = COUNT([numItemID])
                    FROM    [ItemAPI]
                    WHERE   [WebApiId] = @intWebApiId
                            AND [numDomainId] = @numDomainID
                            AND [vcAPIItemID] = @vcApiItemId
                    IF @cnt > 0 
                        BEGIN
                            SELECT  @ItemID = [numItemID]
                            FROM    [ItemAPI]
                            WHERE   [WebApiId] = @intWebApiId
                                    AND [numDomainId] = @numDomainID
                                    AND [vcAPIItemID] = @vcApiItemId
        --Update Existing Product coming from API
                            SET @numItemCode = @ItemID
                        END
     
                END
        END
                                                         
if @numItemCode=0 or  @numItemCode is null
begin                                                                 
 insert into Item (                                             
  vcItemName,                                             
  txtItemDesc,                                             
  charItemType,                                             
  monListPrice,                                             
  numItemClassification,                                             
  bitTaxable,                                             
  vcSKU,                                             
  bitKitParent,                                             
  --dtDateEntered,                                              
  numVendorID,                                             
  numDomainID,                                             
  numCreatedBy,                                             
  bintCreatedDate,                                             
  bintModifiedDate,                                             
 numModifiedBy,                                              
  bitSerialized,                                     
  vcModelID,                                            
  numItemGroup,                                          
  numCOGsChartAcntId,                        
  numAssetChartAcntId,                                      
  numIncomeChartAcntId,                            
  monAverageCost,                          
  monCampaignLabourCost,                  
  fltWeight,                  
  fltHeight,                  
  fltWidth,                  
  fltLength,                  
  bitFreeShipping,                
  bitAllowBackOrder,              
  vcUnitofMeasure,            
  bitShowDeptItem,            
  bitShowDeptItemDesc,        
  bitCalAmtBasedonDepItems,    
  bitAssembly,
  numBarCodeId,
  vcManufacturer,numBaseUnit,numPurchaseUnit,numSaleUnit,bitLotNo,IsArchieve,numItemClass,tintStandardProductIDType,vcExportToAPI,numShipClass,
  bitAllowDropShip,bitArchiveItem,
  bitAsset,bitRental
   )                                                              
   values                                                                
    (                                                                
  @vcItemName,                                             
  @txtItemDesc,                                  
  @charItemType,                                             
  @monListPrice,                                             
  @numItemClassification,                                             
  @bitTaxable,                                             
  @ParentSKU,                       
  @bitKitParent,                                             
  --@dtDateEntered,                                             
  @numVendorID,                                             
  @numDomainID,                                             
  @numUserCntID,                                           
  getutcdate(),                                             
  getutcdate(),                                             
  @numUserCntID,                                             
  @bitSerialized,                                       
  @vcModelID,                                            
  @numItemGroup,                                      
  @numCOGsChartAcntId,                                      
  @numAssetChartAcntId,                                      
  @numIncomeChartAcntId,                            
  @monAverageCost,                          
  @monLabourCost,                  
  @fltWeight,                  
  @fltHeight,                  
  @fltWidth,                  
  @fltLength,                  
  @bitFreeshipping,                
  @bitAllowBackOrder,              
  @UnitofMeasure,            
  @bitShowDeptItem,            
  @bitShowDeptItemDesc,        
  @bitCalAmtBasedonDepItems,    
  @bitAssembly,                                                          
   @numBarCodeId,
@vcManufacturer,@numBaseUnit,@numPurchaseUnit,@numSaleUnit,@bitLotNo,@IsArchieve,@numItemClass,@tintStandardProductIDType,@vcExportToAPI,@numShipClass,
@bitAllowDropShip,@bitArchiveItem ,@bitAsset,@bitRental)                                             
 
 set @numItemCode = SCOPE_IDENTITY()
 
 --insert warehouse for inventory item
-- EXEC sp_xml_preparedocument @hDoc OUTPUT, @strFieldList      
-- insert into WareHouseItems (numItemID, numWareHouseID,numOnHand,monWListPrice,vcLocation,vcWHSKU,vcBarCode,numDomainID,numReorder)  
--	SELECT @numItemCode,numWareHouseID,OnHand,Price,Location,SKU,BarCode,@numDomainID,Reorder
--	FROM OPENXML(@hDoc,'/NewDataSet/WareHouse[Op_Flag=1]',2)                                                    
--    with(numWareHouseItemID numeric(9),                                                                        
--    numWareHouseID numeric(9),                    
--    Reorder numeric(9),                                                                       
--    OnHand numeric(9),                                            
--    Op_Flag tinyint,      
--    Price money,
--    Location Varchar(100),
--    SKU varchar(100),
--    BarCode varchar(100))
--    
--update WareHouseItems  set                                                                         
--   numItemID=@numItemCode,                                                                       
--   numWareHouseID=X.numWareHouseID,                                                              
--   numOnHand=X.OnHand,                     
--   numReorder=X.Reorder,      
--   monWListPrice=X.Price,
--   vcLocation=X.Location,
--   vcWHSKU=X.SKU,
--	vcBarCode=X.BarCode                              
--    From (SELECT numWareHouseItemID as WareHouseItemID,numWareHouseID,Reorder,OnHand,Op_Flag,Price,Location,SKU,BarCode                                                                        
--   FROM OPENXML(@hDoc,'/NewDataSet/WareHouse[Op_Flag=2]',2)                                                                         
--    with(numWareHouseItemID numeric(9),                                                                        
--    numWareHouseID numeric(9),                    
--    Reorder numeric(9),                                                                       
--    OnHand numeric(9),                                            
--    Op_Flag tinyint,      
--    Price money,
--    Location Varchar(100),
--    SKU varchar(100),
--    BarCode varchar(100)))X                                                                         
--  where  numWareHouseItemID=X.WareHouseItemID 
--    insert into ItemUOM (numItemCode, numUOMId,numDomainId)  
--	SELECT @numItemCode,numUOMId,@numDomainID
--	FROM OPENXML(@hDoc,'/NewDataSet/TableUOM',2)                                                    
--    with(numUOMId numeric(9))
  
 IF  @intWebApiId > 1 
   BEGIN
     -- insert new product
     --insert this id into linking table
     INSERT INTO [ItemAPI] (
	 	[WebApiId],
	 	[numDomainId],
	 	[numItemID],
	 	[vcAPIItemID],
	 	[numCreatedby],
	 	[dtCreated],
	 	[numModifiedby],
	 	[dtModified],vcSKU
	 ) VALUES     (@intWebApiId,
                 @numDomainID,
                 @numItemCode,
                 @vcApiItemId,
	 			 @numUserCntID,
	 			 GETUTCDATE(),
	 			 @numUserCntID,
	 			 GETUTCDATE(),@vcSKU
	 			 ) 
   
	--update lastUpdated Date                  
	--UPDATE [WebAPIDetail] SET [vcThirteenthFldValue] = GETUTCDATE() WHERE [WebApiId] =@intWebApiId AND [numDomainId] = @numDomainID
   END
 
 
end         
else if    @numItemCode>0  AND @intWebApiId > 0 
BEGIN 
	IF @ProcedureCallFlag =1 
		BEGIN
		DECLARE @ExportToAPIList AS VARCHAR(30)
		SELECT @ExportToAPIList = vcExportToAPI FROM dbo.Item WHERE numItemCode = @numItemCode
		IF @ExportToAPIList != ''
			BEGIN
			IF NOT EXISTS(select * from (SELECT * FROM dbo.Split(@ExportToAPIList ,','))as A where items= @vcExportToAPI ) 
				BEGIN
					SET @vcExportToAPI = @ExportToAPIList + ',' + @vcExportToAPI
				End
			ELSE
				BEGIN
					SET @vcExportToAPI = @ExportToAPIList 
				End
			END
		--update ExportToAPI String value
		UPDATE dbo.Item SET vcExportToAPI=@vcExportToAPI where numItemCode=@numItemCode  AND numDomainID=@numDomainID
					
			SELECT @cnt = COUNT([numItemID]) FROM [ItemAPI] WHERE  [WebApiId] = @intWebApiId 
																AND [numDomainId] = @numDomainID
																AND [vcAPIItemID] = @vcApiItemId
																AND numItemID = @numItemCode
			IF @cnt > 0
			BEGIN
				--update lastUpdated Date 
				UPDATE [ItemAPI] SET [dtModified] =GETUTCDATE() WHERE [WebApiId] =@intWebApiId AND [numDomainId] = @numDomainID
			END
			ELSE
				--Insert ItemAPI mapping
				BEGIN
					INSERT INTO [ItemAPI] (
						[WebApiId],
						[numDomainId],
						[numItemID],
						[vcAPIItemID],
						[numCreatedby],
						[dtCreated],
						[numModifiedby],
						[dtModified],vcSKU
					) VALUES     (@intWebApiId,
						@numDomainID,
						@numItemCode,
						@vcApiItemId,
						@numUserCntID,
						GETUTCDATE(),
						@numUserCntID,
						GETUTCDATE(),@vcSKU
					)	
				END

		END   
END
                                                       
else if    @numItemCode>0  AND @intWebApiId <= 0                                                           
begin

	DECLARE @OldGroupID NUMERIC 
	SELECT @OldGroupID = ISNULL(numItemGroup,0) FROM [Item] WHERE numitemcode = @numItemCode AND [numDomainId] = @numDomainID
	
--	--validate average cost, do not update average cost if item has sales/purchase orders
--	IF (SELECT COUNT(OI.numOppId) FROM dbo.OpportunityItems OI WHERE OI.numItemCode =@numItemCode) >0
--	BEGIN
--		SELECT @monAverageCost = monAverageCost FROM dbo.Item WHERE numItemCode=@numItemCode
--	END

	DECLARE @monOldAverageCost AS DECIMAL(18,4);SET @monOldAverageCost=0
	SELECT @monOldAverageCost=ISNULL(monAverageCost,0) FROM Item WHERE numItemCode =@numItemCode AND [numDomainId] = @numDomainID
	
----	-- Remove below code block while you update production server
----	PRINT 'FROM ITEM UPDATE : Supplied VendorID =' + CONVERT(VARCHAR(10), @numVendorID)
----	IF ISNULL(@numVendorID,0) = 0
----	BEGIN
----		SELECT @numVendorID = ISNULL(numVendorID,0) FROM dbo.Item WHERE numItemCode = @numItemCode
----		--PRINT @numVendorID
----	END
----	PRINT 'FROM ITEM UPDATE : Modified VendorID =' + CONVERT(VARCHAR(10), @numVendorID)
----	--------------------------------------------------------              
 update item set vcItemName=@vcItemName,                           
  txtItemDesc=@txtItemDesc,                                             
  charItemType=@charItemType,                                             
  monListPrice= @monListPrice,
  numItemClassification=@numItemClassification,                                             
  bitTaxable=@bitTaxable,                                             
  vcSKU = (CASE WHEN ISNULL(@ParentSKU,'') = '' THEN @vcSKU ELSE @ParentSKU END),                                             
  bitKitParent=@bitKitParent,                                             
  --dtDateEntered=@dtDateEntered,                                             
  numVendorID=@numVendorID,                                             
  numDomainID=@numDomainID,                                             
  bintModifiedDate=getutcdate(),                                             
  numModifiedBy=@numUserCntID,                                   
  bitSerialized=@bitSerialized,                                                         
  vcModelID=@vcModelID,                                            
  numItemGroup=@numItemGroup,                                      
  numCOGsChartAcntId=@numCOGsChartAcntId,                                      
  numAssetChartAcntId=@numAssetChartAcntId,                                      
  numIncomeChartAcntId=@numIncomeChartAcntId,                            
  --monAverageCost=@monAverageCost,                          
  monCampaignLabourCost=@monLabourCost,                
  fltWeight=@fltWeight,                  
  fltHeight=@fltHeight,                  
  fltWidth=@fltWidth,                  
  fltLength=@fltLength,                  
  bitFreeShipping=@bitFreeshipping,                
  bitAllowBackOrder=@bitAllowBackOrder,              
  vcUnitofMeasure=@UnitofMeasure,            
  bitShowDeptItem=@bitShowDeptItem,            
  bitShowDeptItemDesc=@bitShowDeptItemDesc,        
  bitCalAmtBasedonDepItems=@bitCalAmtBasedonDepItems,    
  bitAssembly=@bitAssembly ,
numBarCodeId=@numBarCodeId,
vcManufacturer=@vcManufacturer,numBaseUnit=@numBaseUnit,numPurchaseUnit=@numPurchaseUnit,numSaleUnit=@numSaleUnit,bitLotNo=@bitLotNo,
IsArchieve = @IsArchieve,numItemClass=@numItemClass,tintStandardProductIDType=@tintStandardProductIDType,vcExportToAPI=@vcExportToAPI,
numShipClass=@numShipClass,bitAllowDropShip=@bitAllowDropShip, bitArchiveItem = @bitArchiveItem,bitAsset=@bitAsset,bitRental=@bitRental
where numItemCode=@numItemCode  AND numDomainID=@numDomainID

IF NOT EXISTS(SELECT numItemCode FROM dbo.OpportunityItems WHERE numItemCode = @numItemCode)
	BEGIN
		IF (((SELECT ISNULL(SUM(numOnHand),0) FROM WareHouseItems WHERE numItemID = @numItemCode AND numDomainId=@numDomainID) = 0) AND ((SELECT ISNULL(monAverageCost,0) FROM Item  WHERE numItemCode = @numItemCode AND numDomainId=@numDomainID ) = 0))
		BEGIN
			UPDATE Item SET monAverageCost=@monAverageCost  where numItemCode=@numItemCode  AND numDomainID=@numDomainID	
		END
		--UPDATE Item SET monAverageCost=@monAverageCost  where numItemCode=@numItemCode  AND numDomainID=@numDomainID	
	END
	
 --If Average Cost changed then add in Tracking
	IF ISNULL(@monAverageCost,0) != ISNULL(@monOldAverageCost,0)
	BEGIN
		DECLARE @numWareHouseItemID NUMERIC(9);SET @numWareHouseItemID=0
		DECLARE @numTotal int;SET @numTotal=0
		DECLARE @i int;SET @i=0
		DECLARE @vcDescription AS VARCHAR(100);SET @vcDescription=''
		
	    SELECT @numTotal=COUNT(*) FROM WareHouseItems where numItemID=@numItemCode AND numDomainId=@numDomainId
	     
		WHILE @i < @numTotal
		BEGIN
			SELECT @numWareHouseItemID = min(numWareHouseItemID) FROM WareHouseItems 
				WHERE numItemID=@numItemCode AND numDomainId=@numDomainId and numWareHouseItemID > @numWareHouseItemID 
				
			IF @numWareHouseItemID>0
			BEGIN
				SET @vcDescription='Average Cost Changed Manually OLD : ' + CAST(ISNULL(@monOldAverageCost,0) AS VARCHAR(20))
				
				DECLARE @numDomain AS NUMERIC(18,0)
				SET @numDomain = @numDomainID
				EXEC dbo.USP_ManageWareHouseItems_Tracking
					@numWareHouseItemID = @numWareHouseItemID, --  numeric(9, 0)		
					@numReferenceID = @numItemCode, --  numeric(9, 0)
					@tintRefType = 1, --  tinyint
					@vcDescription = @vcDescription, --  varchar(100)
					@tintMode = 0, --  tinyint
					@numModifiedBy = @numUserCntID, --  numeric(9, 0)
					@numDomainID = @numDomain
		    END
		    
		    SET @i = @i + 1
		END
	END
 
 DECLARE @bitCartFreeShipping as  bit 
 SELECT @bitCartFreeShipping = bitFreeShipping FROM dbo.CartItems where numItemCode = @numItemCode  AND numDomainID = @numDomainID 
 
 IF @bitCartFreeShipping <> @bitFreeshipping
 BEGIN
    UPDATE dbo.CartItems SET bitFreeShipping = @bitFreeshipping WHERE numItemCode = @numItemCode  AND numDomainID=@numDomainID
 END 
 
 
 IF  @intWebApiId > 1 
   BEGIN
      SELECT @cnt = COUNT([numItemID])
    FROM   [ItemAPI]
    WHERE  [WebApiId] = @intWebApiId
           AND [numDomainId] = @numDomainID
           AND [vcAPIItemID] = @vcApiItemId
    IF @cnt > 0
      BEGIN
      --update lastUpdated Date 
      UPDATE [ItemAPI] SET [dtModified] =GETUTCDATE() WHERE [WebApiId] =@intWebApiId AND [numDomainId] = @numDomainID
      END
      ELSE
      --Insert ItemAPI mapping
      BEGIN
        INSERT INTO [ItemAPI] (
	 	[WebApiId],
	 	[numDomainId],
	 	[numItemID],
	 	[vcAPIItemID],
	 	[numCreatedby],
	 	[dtCreated],
	 	[numModifiedby],
	 	[dtModified],vcSKU
	 ) VALUES     (@intWebApiId,
                 @numDomainID,
                 @numItemCode,
                 @vcApiItemId,
	 			 @numUserCntID,
	 			 GETUTCDATE(),
	 			 @numUserCntID,
	 			 GETUTCDATE(),@vcSKU
	 			 ) 
      END

--UPDATE [WebAPIDetail] SET [vcThirteenthFldValue] = GETUTCDATE() WHERE [WebApiId] =@intWebApiId AND [numDomainId] = @numDomainID
--UPDATE [ItemAPI] SET [dtModified] =GETUTCDATE() WHERE [WebApiId] =@intWebApiId AND [numDomainId] = @numDomainID
--  SELECT * FROM dbo.ItemCategory
--  Insert into ItemCategory(numItemID,numCategoryID)                                            
--  (SELECT numItemID,numCategoryID
--  FROM OPENXML(@hDoc,'/ItemCategories/',2)                                                                         
--    with(numWareHouseItmsDTLID numeric(9),                                                                        
--    numWareHouseItemID numeric(9),                                                  
--   ))
   
   
   END                                                                               
-- kishan It is necessary to add item into itemTax table . 
 IF	@bitTaxable = 1
	BEGIN
		IF NOT EXISTS(SELECT * FROM dbo.ItemTax WHERE numItemCode = @numItemCode AND numTaxItemID = 0)
			BEGIN
			    	INSERT INTO dbo.ItemTax (
						numItemCode,
						numTaxItemID,
						bitApplicable
					) VALUES ( 
						/* numItemCode - numeric(18, 0) */ @numItemCode,
						/* numTaxItemID - numeric(18, 0)*/ 0,
						/* bitApplicable - bit */ 1 ) 						
			END
	END


--  Insert into WareHouseItmsDTL(numWareHouseItemID,vcSerialNo,vcComments,numQty)                                            
--  (SELECT numWareHouseItemID,vcSerialNo,Comments,numQty                                                                        
--  FROM OPENXML(@hDoc,'/NewDataSet/SerializedItems[Op_Flag=1]',2)                                                                         
--    with(numWareHouseItmsDTLID numeric(9),                                                                        
--    numWareHouseItemID numeric(9),                                                  
--    vcSerialNo varchar(100),                                            
--    Op_Flag tinyint,          
--    Comments varchar(1000),numQty NUMERIC(9)))
--                                                                                
--  update WareHouseItmsDTL set                                                                         
--   numWareHouseItemID=X.numWareHouseItemID,                                                                       
--   vcSerialNo=X.vcSerialNo,          
--    vcComments=X.Comments,numQty=X.numQty                                                                                          
--    From (SELECT numWareHouseItmsDTLID as WareHouseItmsDTLID,numWareHouseItemID,vcSerialNo,Op_Flag,Comments,numQty                              
--   FROM OPENXML(@hDoc,'/NewDataSet/SerializedItems[Op_Flag=2]',2)                                                                         
--    with(numWareHouseItmsDTLID numeric(9),                                                          
--    numWareHouseItemID numeric(9),                                                                        
--    vcSerialNo varchar(100),                                            
--    Op_Flag tinyint,          
--    Comments varchar(1000),numQty NUMERIC(9)))X                                                                         
--  where  numWareHouseItmsDTLID=X.WareHouseItmsDTLID                                            
--                                             
--  delete from  WareHouseItmsDTL where numWareHouseItmsDTLID in (SELECT numWareHouseItmsDTLID                                                                        
--   FROM OPENXML(@hDoc,'/NewDataSet/SerializedItems[Op_Flag=3]',2)                                   
--    with(numWareHouseItmsDTLID numeric(9),                              
--    numWareHouseItemID numeric(9),                                                                        
--    vcSerialNo varchar(100),                                            
--  Op_Flag tinyint)) 


--  SELECT X.*,ROW_NUMBER() OVER( order by X.numQty) AS ROWNUMBER
--INTO #TempTableWareHouseItmsDTLID
--FROM ( SELECT  numWareHouseItmsDTLID,numWareHouseItemID,vcSerialNo,Op_Flag,Comments,numQty,OldQty
--                  FROM      OPENXML(@hDoc, '/NewDataSet/SerializedItems',2)
--                            WITH ( numWareHouseItmsDTLID NUMERIC(9), numWareHouseItemID NUMERIC(9), vcSerialNo VARCHAR(100), Op_Flag TINYINT, Comments VARCHAR(1000),OldQty NUMERIC(9),numQty NUMERIC(9) )
--                ) X


--DECLARE @minROWNUMBER INT
--DECLARE @maxROWNUMBER INT
--DECLARE @Diff INT,@Op_Flag AS int
--DECLARE @Diff1 INT
--DECLARE @numWareHouseItemID NUMERIC(9)
--DECLARE @numWareHouseItmsDTLID NUMERIC(9)
--
--SELECT  @minROWNUMBER = MIN(ROWNUMBER) , @maxROWNUMBER =MAX(ROWNUMBER) FROM #TempTableWareHouseItmsDTLID
--
--WHILE  @minROWNUMBER <= @maxROWNUMBER
--    BEGIN
--   	    SELECT @Diff = numQty - OldQty,@numWareHouseItemID=numWareHouseItemID,@Op_Flag=Op_Flag,@numWareHouseItmsDTLID=numWareHouseItmsDTLID FROM #TempTableWareHouseItmsDTLID WHERE ROWNUMBER=@minROWNUMBER
--   	    
--   	    IF @Op_Flag=2 
--   	    BEGIN
--		   	 UPDATE WareHouseItmsDTL SET  numWareHouseItemID = X.numWareHouseItemID,
--                vcSerialNo = X.vcSerialNo,vcComments = X.Comments,numQty=X.numQty 
--                FROM #TempTableWareHouseItmsDTLID X INNER JOIN WareHouseItmsDTL W
--                ON X.numWareHouseItmsDTLID=W.numWareHouseItmsDTLID WHERE X.ROWNUMBER=@minROWNUMBER
--		END 
--   	    
--   	    ELSE IF @Op_Flag=1 
--   	    BEGIN
--   	       	  INSERT INTO WareHouseItmsDTL(numWareHouseItemID,vcSerialNo,vcComments,numQty)  
--		   	   SELECT  X.numWareHouseItemID,X.vcSerialNo,X.Comments,X.numQty  FROM #TempTableWareHouseItmsDTLID X 
--		   	   WHERE X.ROWNUMBER=@minROWNUMBER
--		END
--   	    
--   	    ELSE IF @Op_Flag=3 
--   	    BEGIN
--   	       	  delete from  WareHouseItmsDTL where numWareHouseItmsDTLID=@numWareHouseItmsDTLID
--		END
--		
--   	    update WareHouseItems SET numOnHand=numOnHand + @Diff where numWareHouseItemID = @numWareHouseItemID AND (numOnHand + @Diff)>=0
--  
--        SELECT  @minROWNUMBER = MIN(ROWNUMBER) FROM  #TempTableWareHouseItmsDTLID WHERE  [ROWNUMBER] > @minROWNUMBER
--    END	 
      
IF @charItemType='S' or @charItemType='N'                                                                       
BEGIN
	delete from WareHouseItmsDTL where numWareHouseItemID in (select numWareHouseItemID from WareHouseItems where numItemID=@numItemCode AND [WareHouseItems].[numDomainID] = @numDomainID)
	delete from WareHouseItems where numItemID=@numItemCode AND [WareHouseItems].[numDomainID] = @numDomainID
END

--	insert into WareHouseItems (numItemID, numWareHouseID,numOnHand,monWListPrice,vcLocation,vcWHSKU,vcBarCode,numDomainID,numReorder)  
--	SELECT @numItemCode,numWareHouseID,OnHand,Price,Location,SKU,BarCode,@numDomainID,Reorder
--	FROM OPENXML(@hDoc,'/NewDataSet/WareHouse[Op_Flag=1]',2)                                                    
--    with(numWareHouseItemID numeric(9),                                                                        
--    numWareHouseID numeric(9),                    
--    Reorder numeric(9),                                                                       
--    OnHand numeric(9),                                            
--    Op_Flag tinyint,      
--    Price money,
--    Location Varchar(100),
--    SKU varchar(100),
--    BarCode varchar(100))
--SELECT @@IDENTITY	

--  update WareHouseItems  set                                                                         
--   numItemID=@numItemCode,                                                                       
--   numWareHouseID=X.numWareHouseID,                                                              
--   numOnHand=X.OnHand,                     
--   numReorder=X.Reorder,      
--   monWListPrice=X.Price,
--   vcLocation=X.Location,
--   vcWHSKU=X.SKU,
--	vcBarCode=X.BarCode                              
--    From (SELECT numWareHouseItemID as WareHouseItemID,numWareHouseID,Reorder,OnHand,Op_Flag,Price,Location,SKU,BarCode                                                                        
--   FROM OPENXML(@hDoc,'/NewDataSet/WareHouse[Op_Flag=2]',2)                                                                         
--    with(numWareHouseItemID numeric(9),                                                                        
--    numWareHouseID numeric(9),                    
--    Reorder numeric(9),                                                                       
--    OnHand numeric(9),                                            
--    Op_Flag tinyint,      
--    Price money,
--    Location Varchar(100),
--    SKU varchar(100),
--    BarCode varchar(100)))X                                                                         
--  where  numWareHouseItemID=X.WareHouseItemID                                   
                                   
                                   
--    delete from  WareHouseItmsDTL where numWareHouseItemID in (SELECT numWareHouseItemID                                                                 
--   FROM OPENXML(@hDoc,'/NewDataSet/WareHouse[Op_Flag=3]',2)                                                                         
--    with(numWareHouseItemID numeric(9),                                            
--    Op_Flag tinyint))                                             
	
	/*Enforce valiation for child records*/
--	IF exists (SELECT * FROM [OpportunityMaster] OM INNER JOIN [OpportunityItems] OI ON OM.[numOppId] = OI.[numOppId] WHERE OM.[numDomainId]=@numDomainID AND [numWarehouseItmsID] IN (SELECT numWareHouseItemID FROM OPENXML(@hDoc,'/NewDataSet/WareHouse[Op_Flag=3]',2)
--    with(numWareHouseItemID numeric(9),
--    Op_Flag TINYINT)) )
--	BEGIN
--		raiserror('CHILD_WAREHOUSE',16,1);
--		RETURN ;
--	END

--  delete from  WareHouseItems where numWareHouseItemID in (SELECT numWareHouseItemID FROM OPENXML(@hDoc,'/NewDataSet/WareHouse[Op_Flag=3]',2)
--    with(numWareHouseItemID numeric(9),                                                                 
--    numWareHouseID numeric(9),                                                                        
--    OnHand numeric(9),                                            
--    Op_Flag tinyint))                                  
    /*commented by chintan, Obsolete, Reason: While inserting new Row numWareHouseItemID will be 0,1,2 incrementally. */
--  delete from  WareHouseItems where numItemID=@numItemCode and  numWareHouseItemID not in (SELECT numWareHouseItemID                                                                    
--   FROM OPENXML(@hDoc,'/NewDataSet/WareHouse',2)                                                                         
--    with(numWareHouseItemID numeric(9),                                    
--    numWareHouseID numeric(9),                                                                        
--    OnHand numeric(9),                                            
--    Op_Flag tinyint))                                        

/*If Item is Matrix item and changed to non matrix item then remove matrix attributes associated with- by chintan*/
--IF @OldGroupID>0 AND @numItemGroup = 0 
--BEGIN
--	DELETE FROM [CFW_Fld_Values_Serialized_Items] WHERE [bitSerialized] =0 AND 
--	[RecId] IN ( SELECT numWareHouseItemID FROM  OPENXML(@hDoc,'/NewDataSet/WareHouse',2) with(numWareHouseItemID numeric(9)) )
--END
                                         
                                             
                                                       
                                             
                                             
-- delete from WareHouseItmsDTL where numWareHouseItemID not in (select numWareHouseItemID from WareHouseItems)                                            
 
-- if @bitSerialized=0 AND @bitLotNo=0 delete from WareHouseItmsDTL where  numWareHouseItemID  in (select numWareHouseItemID from WareHouseItems where numItemID=@numItemCode)                                             
    
--    DELETE FROM  ItemUOM WHERE numItemCode=@numItemCode AND numDomainId=@numDomainId
    
--    insert into ItemUOM (numItemCode, numUOMId,numDomainId)  
--	SELECT @numItemCode,numUOMId,@numDomainID
--	FROM OPENXML(@hDoc,'/NewDataSet/TableUOM',2)                                                    
--    with(numUOMId numeric(9))                                        
   
    if @bitKitParent=1  OR @bitAssembly = 1              
    begin              
  declare @hDoc1 as int                                            
  EXEC sp_xml_preparedocument @hDoc1 OUTPUT, @strChildItems                                                                        
                                                                                                 
                                                                
   update ItemDetails set                                                                         
    numQtyItemsReq=X.QtyItemsReq,numWareHouseItemId=X.numWarehouseItmsID,numUOMId=X.numUOMId,vcItemDesc=X.vcItemDesc,sintOrder=X.sintOrder                                                                                          
     From (SELECT QtyItemsReq,ItemKitID,ChildItemID,numWarehouseItmsID,numUOMId,vcItemDesc,sintOrder                             
    FROM OPENXML(@hDoc1,'/NewDataSet/Table1',2)                                                                         
     with(ItemKitID numeric(9),                                                          
     ChildItemID numeric(9),                                                                        
     QtyItemsReq numeric(9),  
     numWarehouseItmsID numeric(9),numUOMId NUMERIC(9),vcItemDesc VARCHAR(1000),sintOrder int))X                                                                         
   where  numItemKitID=X.ItemKitID and numChildItemID=ChildItemID                
              
 end   
 ELSE
 BEGIN
 	DELETE FROM ItemDetails WHERE numItemKitID=@numItemCode
 END           
                                                               
end

declare  @rows as integer                                        
 EXEC sp_xml_preparedocument @hDoc OUTPUT, @strFieldList         
 
 --THIS CODE IS COMMENTED BECAUSE THERE IS NO LIST PRICE TEXTBOX IN MANAGE INVENTORY SCREEN FOR SERIALIZED ITEM NOW
 --FROM WHERE USER CAN SET LIST PRICE
 --IF @bitSerialized = 1
 --BEGIN
	
	--UPDATE Item SET [monListPrice]  = (
	--SELECT WHI.[monWListPrice] FROM [dbo].[WareHouseItems] AS WHI WHERE [WHI].[numItemID] = @numItemCode
	--AND [WHI].[numDomainID] = @numDomainID
	--AND [WHI].[numWareHouseID] = (SELECT TOP 1 [WareHouseItems].[numWareHouseID] FROM [dbo].[WareHouseItems] WHERE [WareHouseItems].[numItemID] = [WHI].[numItemID])
	--)
	--WHERE [Item].[numItemCode] = @numItemCode AND [Item].[numDomainID] = @numDomainID

 --END                                                               
                                         
 SELECT @rows=count(*) FROM OPENXML (@hDoc,'/NewDataSet/CusFlds',2)                                                  
  WITH (Fld_ID numeric(9),Fld_Value varchar(100),RecId numeric(9))                                        
                                                                           
                                          
 if @rows>0                                        
 begin                                        
  Create table #tempTable ( ID INT IDENTITY PRIMARY KEY,                    
  Fld_ID numeric(9),                                        
  Fld_Value varchar(100),                                        
  RecId numeric(9),                                    
  bitSItems bit                                                                         
  )                                         
  insert into #tempTable (Fld_ID,Fld_Value,RecId,bitSItems)                                        
  SELECT * FROM OPENXML (@hDoc,'/NewDataSet/CusFlds',2)                                                  
  WITH (Fld_ID numeric(9),Fld_Value varchar(100),RecId numeric(9),bitSItems bit)                                           
                                         
  delete CFW_Fld_Values_Serialized_Items from CFW_Fld_Values_Serialized_Items C                                        
  inner join #tempTable T                          
  on   C.Fld_ID=T.Fld_ID and C.RecId=T.RecId and bitSerialized=bitSItems                                         
                                         
  drop table #tempTable                                        
                                                            
  insert into CFW_Fld_Values_Serialized_Items(Fld_ID,Fld_Value,RecId,bitSerialized)                                                  
  select Fld_ID,isnull(Fld_Value,'') as Fld_Value,RecId,bitSItems from(SELECT * FROM OPENXML (@hDoc,'/NewDataSet/CusFlds',2)                                                  
  WITH (Fld_ID numeric(9),Fld_Value varchar(100),RecId numeric(9),bitSItems bit)) X                                         
 end  

IF @numItemCode > 0
	BEGIN
			IF EXISTS(SELECT * FROM dbo.ItemCategory WHERE numItemID = @numItemCode)
			 BEGIN
					DELETE FROM dbo.ItemCategory WHERE numItemID = @numItemCode		
			 END 
	  IF @vcCategories <> ''	
		BEGIN
        INSERT INTO ItemCategory( numItemID , numCategoryID )  
        SELECT  @numItemCode AS numItemID,ID from dbo.SplitIDs(@vcCategories,',')	
		END
	END

 EXEC sp_xml_removedocument @hDoc              

--CASE WHEN @bitSerialized = 1 THEN (SELECT * FROM [dbo].[WareHouseItems] AS WI WHERE [WI].[numItemID] = @numItemCode AND [WI].[numWareHouseID] = )
--					 ELSE @monListPrice 
--				END,                                             
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_ManageOpportunityAutomationRules' ) 
    DROP PROCEDURE USP_ManageOpportunityAutomationRules
GO
CREATE PROCEDURE USP_ManageOpportunityAutomationRules
    @numDomainID AS NUMERIC,
    @strItems TEXT,
    @tintMode AS TINYINT,
    @tintOppType AS TINYINT 
AS 
BEGIN
    IF @tintMode = 0
        BEGIN
			SELECT * FROM OpportunityAutomationRules WHERE numDomainID=@numDomainID AND (tintOppType=@tintOppType OR @tintOppType=0)
        END
	ELSE IF @tintMode = 1 
    BEGIN
		DECLARE @ErrorMessage NVARCHAR(4000);SET @ErrorMessage=''
		DECLARE @ErrorSeverity INT;
		DECLARE @ErrorState INT;
		
		BEGIN TRANSACTION;

		BEGIN TRY
            DECLARE @hDocItem INT
            IF CONVERT(VARCHAR(10), @strItems) <> '' 
            BEGIN
                    EXEC sp_xml_preparedocument @hDocItem OUTPUT, @strItems

					DECLARE @TEMPTABLE TABLE
					(
						numRuleID NUMERIC(18,0),
						numBizDocStatus1 NUMERIC(18,0),
						numBizDocStatus2 NUMERIC(18,0),
						numOrderStatus NUMERIC(18,0)
					)

					INSERT INTO @TEMPTABLE
					SELECT  
						X.numRuleID,
						X.numBizDocStatus1,
						X.numBizDocStatus2,
						X.numOrderStatus
                    FROM    
						( 
							SELECT    
								*
                            FROM      
								OPENXML (@hDocItem, '/NewDataSet/Table', 2)
                            WITH ( numRuleID NUMERIC(18),numBizDocStatus1 NUMERIC(18),numBizDocStatus2 NUMERIC(18),numOrderStatus NUMERIC(18))
                        ) X
                    EXEC sp_xml_removedocument @hDocItem


					--TODO CHECK IF SALES FULLFILLMENT RULES CONTAINS CREATE INVOICE RULE

					IF (SELECT COUNT(*) FROM @TEMPTABLE WHERE numBizDocStatus1 IN (SELECT ISNULL(numListItemID,0) FROM ListDetails WHERE numListID = 11 AND numListType = 287 AND numDomainID = @numDomainID)) > 0
					BEGIN
						IF (SELECT COUNT(WorkFlowActionList.numWFActionID) FROM WorkFlowActionList INNER JOIN WorkFlowMaster ON WorkFlowActionList.numWFID = WorkFlowMaster.numWFID WHERE tintActionType=6 AND numBizDocTypeID=287 AND WorkFlowMaster.numDomainID = @numDomainID) > 0
						BEGIN
							RAISERROR ('WORKFLOW AUTOMATION RULE EXISTS',16,1);
							RETURN -1
						END
					END

					--TODO CHECK IF SALES FULLFILLMENT RULES CONTAINS CREATE PACKING SLIP RULE
					IF (SELECT COUNT(*) FROM @TEMPTABLE WHERE numBizDocStatus1 IN (SELECT ISNULL(numListItemID,0) FROM ListDetails WHERE numListID = 11 AND numListType = 29397 AND numDomainID = @numDomainID)) > 0
					BEGIN
						IF (SELECT COUNT(WorkFlowActionList.numWFActionID) FROM WorkFlowActionList INNER JOIN WorkFlowMaster ON WorkFlowActionList.numWFID = WorkFlowMaster.numWFID WHERE tintActionType=6 AND numBizDocTypeID=29397 AND WorkFlowMaster.numDomainID = @numDomainID) > 0
						BEGIN
							RAISERROR ('WORKFLOW AUTOMATION RULE EXISTS',16,1);
							RETURN -1
						END
					END

					DELETE FROM OpportunityAutomationRules WHERE numDomainID=@numDomainID AND tintOppType=@tintOppType 
            
                    INSERT  INTO dbo.OpportunityAutomationRules 
					(
						numDomainID,tintOppType,numRuleID,numBizDocStatus1,numBizDocStatus2,numOrderStatus
					)
                    SELECT  
						@numDomainID,@tintOppType,numRuleID,numBizDocStatus1,numBizDocStatus2,numOrderStatus
                    FROM  
						@TEMPTABLE
            END
            SELECT  ''

		END TRY
		BEGIN CATCH

			SELECT @ErrorMessage = ERROR_MESSAGE(), @ErrorSeverity = ERROR_SEVERITY(), @ErrorState = ERROR_STATE();

			 IF @@TRANCOUNT > 0
				ROLLBACK TRANSACTION;

			-- Use RAISERROR inside the CATCH block to return error
			-- information about the original error that caused
			-- execution to jump to the CATCH block.
			RAISERROR (@ErrorMessage,@ErrorSeverity,@ErrorState);
		END CATCH;

		IF @@TRANCOUNT > 0
			COMMIT TRANSACTION;
	END	
END



GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'usp_ManageRMAInventory' ) 
    DROP PROCEDURE usp_ManageRMAInventory
GO
CREATE PROCEDURE [dbo].[usp_ManageRMAInventory]
    @numReturnHeaderID AS NUMERIC(9) = 0,
    @numDomainId NUMERIC(9),
    @numUserCntID AS NUMERIC(9)=0,
    @tintFlag AS TINYINT  --1: Receive 2: Revert
AS 
  BEGIN
  	
  	DECLARE @tintReturnType AS TINYINT,@tintReceiveType AS TINYINT
  	DECLARE @numReturnItemID AS NUMERIC,@itemcode as numeric,@numUnits as NUMERIC,@numWareHouseItemID as numeric
	DECLARE @monAmount as money 
	DECLARE @onHand AS NUMERIC,@onOrder AS NUMERIC,@onBackOrder AS NUMERIC,@onAllocation AS NUMERIC
	DECLARE @description AS VARCHAR(100)
    
  	SELECT @tintReturnType=tintReturnType,@tintReceiveType=tintReceiveType FROM dbo.ReturnHeader WHERE numReturnHeaderID=@numReturnHeaderID
  	
  		select top 1 @numReturnItemID=numReturnItemID,@itemcode=RI.numItemCode,@numUnits=RI.numUnitHourReceived,
  		 @numWareHouseItemID=ISNULL(numWareHouseItemID,0),@monAmount=monTotAmount
		 from ReturnItems RI join Item I on RI.numItemCode=I.numItemCode                                          
		 where numReturnHeaderID=@numReturnHeaderID AND (charitemtype='P' OR 1=(CASE WHEN @tintReturnType=1 THEN 
							CASE WHEN ISNULL(I.bitKitParent,0) = 1 AND ISNULL(I.bitAssembly,0) = 1 THEN 0
								 WHEN ISNULL(I.bitKitParent,0) = 1 THEN 1
								 ELSE 0 END 
							ELSE 0 END)) order by RI.numReturnItemID                                       
		 while @numReturnItemID>0                                        
		  begin   
			 IF @numWareHouseItemID>0
			 BEGIN                     
				SELECT  @onHand = ISNULL(numOnHand, 0),
						@onAllocation = ISNULL(numAllocation, 0),
						@onOrder = ISNULL(numOnOrder, 0),
						@onBackOrder = ISNULL(numBackOrder, 0)
				FROM    WareHouseItems
				WHERE   numWareHouseItemID = @numWareHouseItemID 
			
				--Receive : SalesReturn 
				IF (@tintFlag=1 AND @tintReturnType=1)
				BEGIN
					SET @description='Sales Return Receive (Qty:' + CAST(@numUnits AS VARCHAR(10)) + ')'
										
                    IF @onBackOrder >= @numUnits 
                    BEGIN            
                         SET @onBackOrder = @onBackOrder - @numUnits            
                         SET @onAllocation = @onAllocation + @numUnits            
                    END            
                    ELSE 
                    BEGIN            
                         SET @onAllocation = @onAllocation + @onBackOrder            
                         SET @numUnits = @numUnits - @onBackOrder            
                         SET @onBackOrder = 0            
                         SET @onHand = @onHand + @numUnits            
                    END         
				END
				--Revert : SalesReturn
				ELSE IF (@tintFlag=2 AND @tintReturnType=1)
				BEGIN
					SET @description='Sales Return Delete (Qty:' + CAST(@numUnits AS VARCHAR(10)) + ')'
					
					IF @onHand  - @numUnits >= 0
						BEGIN						
							SET @onHand = @onHand - @numUnits
						END
					ELSE IF (@onHand + @onAllocation)  - @numUnits >= 0
						BEGIN						
							SET @numUnits = @numUnits - @onHand
                            SET @onHand = 0 
                            
                            SET @onBackOrder = @onBackOrder + @numUnits
                            SET @onAllocation = @onAllocation - @numUnits  
						END	
					ELSE IF  (@onHand + @onBackOrder + @onAllocation) - @numUnits >= 0
						BEGIN
							SET @numUnits = @numUnits - @onHand
                            SET @onHand = 0 
                            
                            SET @numUnits = @numUnits - @onAllocation  
                            SET @onAllocation = 0 
                            
							SET @onBackOrder = @onBackOrder + @numUnits
						END
				END
				--Revert : PurchaseReturn
				ELSE IF (@tintFlag=2 AND @tintReturnType=2) 
				BEGIN
					SET @description='Purchase Return Delete (Qty:' + CAST(@numUnits AS VARCHAR(10)) + ')'
					
					IF @onBackOrder >= @numUnits 
                    BEGIN            
                         SET @onBackOrder = @onBackOrder - @numUnits            
                         SET @onAllocation = @onAllocation + @numUnits            
                    END            
                    ELSE 
                    BEGIN            
                         SET @onAllocation = @onAllocation + @onBackOrder            
                         SET @numUnits = @numUnits - @onBackOrder            
                         SET @onBackOrder = 0            
                         SET @onHand = @onHand + @numUnits            
                    END      
				END
				--Receive : PurchaseReturn 
				ELSE IF (@tintFlag=1 AND @tintReturnType=2) 
				BEGIN
					SET @description='Purchase Return Receive (Qty:' + CAST(@numUnits AS VARCHAR(10)) + ')'
				
                    IF @onHand  - @numUnits >= 0
						BEGIN						
							SET @onHand = @onHand - @numUnits
						END
					ELSE
						BEGIN
						 RAISERROR ( 'PurchaseReturn_Qty', 16, 1 ) ;
						 RETURN ;
						END	
--					ELSE IF (@onHand + @onBackOrder)  - @numUnits >= 0
--						BEGIN						
--							SET @numUnits = @numUnits - @onHand
--                            SET @onHand = 0 
--                            
--                            SET @onBackOrder = @onBackOrder - @numUnits  
--						END	
--					ELSE IF  (@onHand + @onBackOrder + @onAllocation) - @numUnits >= 0
--						BEGIN
--							SET @numUnits = @numUnits - @onHand
--                            SET @onHand = 0 
--                            
--                            SET @numUnits = @numUnits - @onBackOrder  
--                            SET @onBackOrder = 0 
--                            
--							SET @onAllocation = @onAllocation - @numUnits
--						END
				END 
				
				     UPDATE  WareHouseItems
                            SET     numOnHand = @onHand,
                                    numAllocation = @onAllocation,
                                    numBackOrder = @onBackOrder,
                                    numOnOrder = @onOrder,dtModified = GETDATE() 
                            WHERE   numWareHouseItemID = @numWareHouseItemID 
                            
                  IF @numWareHouseItemID>0
				 BEGIN
					DECLARE @tintRefType AS TINYINT;SET @tintRefType=5
					  
						EXEC dbo.USP_ManageWareHouseItems_Tracking
						@numWareHouseItemID = @numWareHouseItemID, --  numeric(9, 0)
						@numReferenceID = @numReturnHeaderID, --  numeric(9, 0)
						@tintRefType = @tintRefType, --  tinyint
						@vcDescription = @description, --  varchar(100)
						@numModifiedBy = @numUserCntID,
						@numDomainId = @numDomainId
                  END           
			END
		  
		  select top 1 @numReturnItemID=numReturnItemID,@itemcode=RI.numItemCode,@numUnits=RI.numUnitHourReceived,
  		 @numWareHouseItemID=ISNULL(numWareHouseItemID,0),@monAmount=monTotAmount
		 from ReturnItems RI join Item I on RI.numItemCode=I.numItemCode                                          
		 where numReturnHeaderID=@numReturnHeaderID AND (charitemtype='P' OR 1=(CASE WHEN @tintReturnType=1 THEN 
							CASE WHEN ISNULL(I.bitKitParent,0) = 1 AND ISNULL(I.bitAssembly,0) = 1 THEN 0
								 WHEN ISNULL(I.bitKitParent,0) = 1 THEN 1
								 ELSE 0 END 
							ELSE 0 END)) and RI.numReturnItemID>@numReturnItemID order by RI.numReturnItemID
							
		   if @@rowcount=0 set @numReturnItemID=0         
		  END
		  
	
  END
GO

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ManageSOWorkOrder')
DROP PROCEDURE USP_ManageSOWorkOrder
GO
CREATE PROCEDURE [dbo].[USP_ManageSOWorkOrder]
@numDomainID AS numeric(9)=0,
@numOppID AS numeric(9)=0,
@strItems AS TEXT,
@numUserCntID AS numeric(9)=0
AS
BEGIN
 DECLARE @hDocItem int                                                                                                                            

IF (SELECT COUNT(*) FROM WorkOrder WHERE numOppId=@numOppId)=0
BEGIN
 EXEC sp_xml_preparedocument @hDocItem OUTPUT, @strItems  
  
  DECLARE @numItemCode AS numeric(9),@numUnitHour AS numeric(18,2),@numWarehouseItmsID AS numeric(9),@vcItemDesc AS VARCHAR(1000)
  
  DECLARE @numWOId AS NUMERIC(9),@vcInstruction AS VARCHAR(1000),@bintCompliationDate AS DATETIME,@numWOAssignedTo NUMERIC(9)
  
  DECLARE WOCursor CURSOR FOR 
  SELECT X.numItemCode,dbo.fn_UOMConversion(X.numUOM,numItemCode,@numDomainId,null) * x.numUnitHour AS numUnitHour,X.numWarehouseItmsID,X.vcItemDesc,X.vcInstruction,X.bintCompliationDate,X.numWOAssignedTo
	from(SELECT *FROM OPENXML (@hDocItem,'/NewDataSet/Item',2)                                                                          
		   WITH  (numItemCode numeric(9),numUnitHour numeric(9),numWarehouseItmsID numeric(9),numUOM NUMERIC(9),bitWorkOrder BIT,vcItemDesc VARCHAR(1000) ,
		   vcInstruction varchar(1000),bintCompliationDate DATETIME,numWOAssignedTo NUMERIC(9)
		  ))X  WHERE X.bitWorkOrder=1

OPEN WOCursor;

FETCH NEXT FROM WOCursor INTO @numItemCode, @numUnitHour,@numWarehouseItmsID,@vcItemDesc,@vcInstruction,@bintCompliationDate,@numWOAssignedTo

WHILE @@FETCH_STATUS = 0
BEGIN
	insert into WorkOrder(numItemCode,numQtyItemsReq,numWareHouseItemId,numCreatedBy,bintCreatedDate,numDomainID,numWOStatus,numOppId,vcItemDesc,vcInstruction,bintCompliationDate,numAssignedTo)
	values(@numItemCode,@numUnitHour,@numWarehouseItmsID,@numUserCntID,getutcdate(),@numDomainID,0,@numOppID,@vcItemDesc,@vcInstruction,@bintCompliationDate,@numWOAssignedTo)
	
	set @numWOId=@@IDENTITY
	

IF @numWOAssignedTo>0
BEGIN
	DECLARE @numDivisionId AS NUMERIC(9) ,@vcItemName AS VARCHAR(300)
	
	SELECT @vcItemName=ISNULL(vcItemName,'') FROM Item WHERE numItemCode=@numItemCode

      SET @vcItemName= 'Work Order for Item : ' + @vcItemName

      SELECT @numDivisionId=numDivisionId FROM AdditionalContactsInformation WHERE numContactId=@numWOAssignedTo

EXEC dbo.usp_InsertCommunication
	@numCommId = 0, --  numeric(18, 0)
	@bitTask = 972, --  numeric(18, 0)
	@numContactId = @numUserCntID, --  numeric(18, 0)
	@numDivisionId = @numDivisionId, --  numeric(18, 0)
	@txtDetails = @vcItemName, --  text
	@numOppId = 0, --  numeric(18, 0)
	@numAssigned = @numWOAssignedTo, --  numeric(18, 0)
	@numUserCntID = @numUserCntID, --  numeric(18, 0)
	@numDomainId = @numDomainID, --  numeric(18, 0)
	@bitClosed = 0, --  tinyint
	@vcCalendarName = '', --  varchar(100)
	@dtStartTime = @bintCompliationDate, --  datetime
	@dtEndtime = @bintCompliationDate, --  datetime
	@numActivity = 0, --  numeric(9, 0)
	@numStatus = 0, --  numeric(9, 0)
	@intSnoozeMins = 0, --  int
	@tintSnoozeStatus = 0, --  tinyint
	@intRemainderMins = 0, --  int
	@tintRemStaus = 0, --  tinyint
	@ClientTimeZoneOffset = 0, --  int
	@bitOutLook = 0, --  tinyint
	@bitSendEmailTemplate = 0, --  bit
	@bitAlert = 0, --  bit
	@numEmailTemplate = 0, --  numeric(9, 0)
	@tintHours = 0, --  tinyint
	@CaseID = 0, --  numeric(18, 0)
	@CaseTimeId = 0, --  numeric(18, 0)
	@CaseExpId = 0, --  numeric(18, 0)
	@ActivityId = 0, --  numeric(18, 0)
	@bitFollowUpAnyTime = 0, --  bit
	@strAttendee=''
END

INSERT INTO [WorkOrderDetails] 
(
	numWOId,numItemKitID,numChildItemID,numQtyItemsReq,numWareHouseItemId,vcItemDesc,sintOrder,numQtyItemsReq_Orig,numUOMId
)
SELECT 
	@numWOId,numItemKitID,numItemCode,
	CAST((DTL.numQtyItemsReq * @numUnitHour)AS NUMERIC(9,0)),
	isnull(Dtl.numWareHouseItemId,0),
	ISNULL(Dtl.vcItemDesc,txtItemDesc),
	ISNULL(sintOrder,0),
	DTL.numQtyItemsReq,
	Dtl.numUOMId 
FROM 
	item                                
INNER JOIN 
	ItemDetails Dtl 
ON 
	numChildItemID=numItemCode
WHERE 
	numItemKitID=@numItemCode
	
FETCH NEXT FROM WOCursor INTO @numItemCode, @numUnitHour,@numWarehouseItmsID,@vcItemDesc,@vcInstruction,@bintCompliationDate,@numWOAssignedTo
END

CLOSE WOCursor;
DEALLOCATE WOCursor;

EXEC sp_xml_removedocument @hDocItem    
 
END 
END
/****** Object:  StoredProcedure [dbo].[USP_ManageWareHouseItems_Tracking]    Script Date: 07/26/2008 16:14:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_ManageWareHouseItems_Tracking' ) 
    DROP PROCEDURE USP_ManageWareHouseItems_Tracking
GO
CREATE PROCEDURE [dbo].[USP_ManageWareHouseItems_Tracking]
    @numWareHouseItemID AS NUMERIC(9) = 0,
    @numReferenceID AS NUMERIC(9)=0,
    @tintRefType AS TINYINT=0,
    @vcDescription AS VARCHAR(100)='',
    @tintMode AS TINYINT=0,
    @CurrentPage INT=0,
    @PageSize INT=0,
    @TotRecs INT=0  OUTPUT,
    @numModifiedBy NUMERIC(9)=0,
    @ClientTimeZoneOffset INT=0,
    @dtRecordDate AS DATETIME=NULL,
	@numDomainID AS NUMERIC(18,0) = 0
AS 

  DECLARE  @firstRec  AS INTEGER
  DECLARE  @lastRec  AS INTEGER


IF @tintMode=1 --Select based on WareHouseItemID
BEGIN

SELECT X.*,Row_number() over(order by dtCreatedDate1 DESC,numWareHouseItemTrackingID DESC) AS RowNumber INTO #tempTable FROM (SELECT        WHIT.numWareHouseItemID,
              WHIT.numOnHand,
              WHIT.numOnOrder,
              WHIT.numReorder,
              WHIT.numAllocation,
              WHIT.numBackOrder,
              WHIT.numDomainID,
              WHIT.vcDescription,
              WHIT.tintRefType,
              WHIT.numReferenceID,
              '' as vcPOppName,
              0 as tintOppType ,
              dbo.FormatedDateTimeFromDate(DateAdd(minute,-@ClientTimeZoneOffset, WHIT.dtCreatedDate),WHIT.numDomainID) dtCreatedDate
			  ,dtCreatedDate AS dtCreatedDate1,WHIT.numModifiedBy,ISNULL(WHIT.monAverageCost,0) AS monAverageCost,ISNULL(WHIT.numTotalOnHand,0) AS numTotalOnHand,dbo.FormatedDateFromDate(WHIT.dtRecordDate,WHIT.numDomainID) AS dtRecordDate,WHIT.numWareHouseItemTrackingID
              FROM WareHouseItems_Tracking WHIT
              join Item I  on I.numItemCode = WHIT.numReferenceID
              WHERE numWareHouseItemID=@numWareHouseItemID  
			  AND [WHIT].[numDomainID] = @numDomainID
			  AND WHIT.tintRefType=1
UNION  
SELECT    WHIT.numWareHouseItemID,
              WHIT.numOnHand,
              WHIT.numOnOrder,
              WHIT.numReorder,
              WHIT.numAllocation,
              WHIT.numBackOrder,
              WHIT.numDomainID,
              WHIT.vcDescription,
              WHIT.tintRefType,
              ISNULL(WHIT.numReferenceID,0) AS numReferenceID,
              ISNULL(ISNULL(OM.vcPOppName,''),'Order Deleted - ' + CONVERT(VARCHAR(10),ISNULL(WHIT.numReferenceID,0))) as vcPOppName,
              ISNULL(OM.tintOppType,0) as tintOppType,
              dbo.FormatedDateTimeFromDate(DateAdd(minute,-@ClientTimeZoneOffset, WHIT.dtCreatedDate),WHIT.numDomainID) dtCreatedDate
			  ,dtCreatedDate AS dtCreatedDate1,WHIT.numModifiedBy,ISNULL(WHIT.monAverageCost,0) AS monAverageCost,ISNULL(WHIT.numTotalOnHand,0) AS numTotalOnHand,dbo.FormatedDateFromDate(WHIT.dtRecordDate,WHIT.numDomainID) AS dtRecordDate,WHIT.numWareHouseItemTrackingID
              FROM WareHouseItems_Tracking WHIT
              LEFT join OpportunityMaster OM  on WHIT.numReferenceID = OM.numOppId
    WHERE numWareHouseItemID=@numWareHouseItemID AND WHIT.tintRefType IN (3,4)
	AND [WHIT].[numDomainID] = @numDomainID
    UNION
	SELECT    WHIT.numWareHouseItemID,
              WHIT.numOnHand,
              WHIT.numOnOrder,
              WHIT.numReorder,
              WHIT.numAllocation,
              WHIT.numBackOrder, 
              WHIT.numDomainID,
              WHIT.vcDescription,
              WHIT.tintRefType,
              WHIT.numReferenceID,
              '' as vcPOppName,
              0 as tintOppType,
              dbo.FormatedDateTimeFromDate(DateAdd(minute,-@ClientTimeZoneOffset, WHIT.dtCreatedDate),WHIT.numDomainID) dtCreatedDate
			  ,dtCreatedDate AS dtCreatedDate1,WHIT.numModifiedBy,ISNULL(WHIT.monAverageCost,0) AS monAverageCost,ISNULL(WHIT.numTotalOnHand,0) AS numTotalOnHand,dbo.FormatedDateFromDate(WHIT.dtRecordDate,WHIT.numDomainID) AS dtRecordDate,WHIT.numWareHouseItemTrackingID
              FROM WareHouseItems_Tracking WHIT
              LEFT JOIN WorkOrder WO  on WHIT.numReferenceID = WO.numWOId
    WHERE  WHIT.numWareHouseItemID=@numWareHouseItemID AND  WHIT.tintRefType=2
	AND [WHIT].[numDomainID] = @numDomainID
    UNION
    SELECT    WHIT.numWareHouseItemID,
              WHIT.numOnHand,
              WHIT.numOnOrder,
              WHIT.numReorder,
              WHIT.numAllocation,
              WHIT.numBackOrder,
              WHIT.numDomainID,
              WHIT.vcDescription,
              WHIT.tintRefType,
              ISNULL(WHIT.numReferenceID,0) AS numReferenceID,
              ISNULL(ISNULL(RH.vcBizDocName,RH.vcRMA),'RMA Deleted - ' + CONVERT(VARCHAR(10),ISNULL(WHIT.numReferenceID,0))) as vcPOppName,
              5 as tintOppType,
              dbo.FormatedDateTimeFromDate(DateAdd(minute,-@ClientTimeZoneOffset, WHIT.dtCreatedDate),WHIT.numDomainID) dtCreatedDate
			  ,WHIT.dtCreatedDate AS dtCreatedDate1,WHIT.numModifiedBy,ISNULL(WHIT.monAverageCost,0) AS monAverageCost,ISNULL(WHIT.numTotalOnHand,0) AS numTotalOnHand,dbo.FormatedDateFromDate(WHIT.dtRecordDate,WHIT.numDomainID) AS dtRecordDate,WHIT.numWareHouseItemTrackingID
              FROM WareHouseItems_Tracking WHIT
              LEFT join ReturnHeader RH  on WHIT.numReferenceID = RH.numReturnHeaderID
    WHERE numWareHouseItemID=@numWareHouseItemID AND WHIT.tintRefType=5
	AND [WHIT].[numDomainID] = @numDomainID) X

  SET @firstRec = (@CurrentPage
                     - 1)
                    * @PageSize
  SET @lastRec = (@CurrentPage
                    * @PageSize
                    + 1)
  SET @TotRecs = (SELECT COUNT(* )
                  FROM   #tempTable)
                  
  SELECT *,dbo.fn_GetContactName(ISNULL(numModifiedBy,0)) AS vcUserName FROM #tempTable WHERE RowNumber > @firstRec and RowNumber < @lastRec order by RowNumber
  DROP TABLE #tempTable
END
ELSE IF @tintMode=2 --Only Biz Calculated Avg Cost
BEGIN

SELECT X.*,Row_number() over(order by dtCreatedDate1 DESC,numWareHouseItemTrackingID DESC) AS RowNumber INTO #tempTable1 FROM (
SELECT    WHIT.numWareHouseItemID,
              WHIT.numOnHand,
              WHIT.numOnOrder,
              WHIT.numReorder,
              WHIT.numAllocation,
              WHIT.numBackOrder,
              WHIT.numDomainID,
              WHIT.vcDescription,
              WHIT.tintRefType,
              ISNULL(WHIT.numReferenceID,0) AS numReferenceID,
              ISNULL(ISNULL(OM.vcPOppName,''),'Order Deleted - ' + CONVERT(VARCHAR(10),ISNULL(WHIT.numReferenceID,0))) as vcPOppName,
              ISNULL(OM.tintOppType,0) as tintOppType,
              dbo.FormatedDateTimeFromDate(DateAdd(minute,-@ClientTimeZoneOffset, WHIT.dtCreatedDate),WHIT.numDomainID) dtCreatedDate
			  ,dtCreatedDate AS dtCreatedDate1,WHIT.numModifiedBy,ISNULL(WHIT.monAverageCost,0) AS monAverageCost,ISNULL(WHIT.numTotalOnHand,0) AS numTotalOnHand,dbo.FormatedDateFromDate(WHIT.dtRecordDate,WHIT.numDomainID) AS dtRecordDate,WHIT.numWareHouseItemTrackingID
              FROM WareHouseItems_Tracking WHIT
              LEFT join OpportunityMaster OM  on WHIT.numReferenceID = OM.numOppId
    WHERE numWareHouseItemID=@numWareHouseItemID AND WHIT.tintRefType =6
	AND [WHIT].[numDomainID] = @numDomainID
    ) X

  SET @firstRec = (@CurrentPage
                     - 1)
                    * @PageSize
  SET @lastRec = (@CurrentPage
                    * @PageSize
                    + 1)
  SET @TotRecs = (SELECT COUNT(* )
                  FROM   #tempTable1)
                  
  SELECT *,dbo.fn_GetContactName(ISNULL(numModifiedBy,0)) AS vcUserName FROM #tempTable1 WHERE RowNumber > @firstRec and RowNumber < @lastRec order by RowNumber
  
  DROP TABLE #tempTable1
END
ELSE IF @tintMode=0 --Insert new record
BEGIN

    INSERT  INTO WareHouseItems_Tracking
            (
              numWareHouseItemID,
              numOnHand,
              numOnOrder,
              numReorder,
              numAllocation,
              numBackOrder,
              numDomainID,
              vcDescription,
              tintRefType,
              numReferenceID,
              dtCreatedDate,numModifiedBy,monAverageCost,numTotalOnHand,dtRecordDate
            )
            SELECT  WHI.numWareHouseItemID,
                    ISNULL(WHI.numOnHand,0),
                    ISNULL(WHI.numOnOrder,0),
                    ISNULL(WHI.numReorder,0),
                    ISNULL(WHI.numAllocation,0),
                    ISNULL(WHI.numBackOrder,0),
                    WHI.numDomainID,
                    @vcDescription,
                    @tintRefType,
                    @numReferenceID,
                    GETUTCDATE(),@numModifiedBy,ISNULL(I.monAverageCost,0),
                    (SELECT SUM(ISNULL(numOnHand, 0)) FROM dbo.WareHouseItems WHERE numItemID=I.numItemCode),@dtRecordDate
            FROM    Item I JOIN WareHouseItems WHI ON I.numItemCode=WHI.numItemID
			WHERE WHI.numWareHouseItemID = @numWareHouseItemID
			AND [WHI].[numDomainID] = @numDomainID
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ManageWorkFlowMaster')
DROP PROCEDURE USP_ManageWorkFlowMaster
GO
Create PROCEDURE [dbo].[USP_ManageWorkFlowMaster]                                        
	@numWFID numeric(18, 0) OUTPUT,
    @vcWFName varchar(200),
    @vcWFDescription varchar(500),   
    @numDomainID numeric(18, 0),
    @numUserCntID numeric(18, 0),
    @bitActive bit,
    @numFormID NUMERIC(18,0),
    @tintWFTriggerOn NUMERIC(18),
    @vcWFAction varchar(Max),
    @vcDateField VARCHAR(MAX),
    @intDays INT,
    @intActionOn INT,
	@bitCustom bit,
	@numFieldID numeric(18),
    @strText TEXT = ''
as                 
DECLARE @ErrorMessage NVARCHAR(4000);SET @ErrorMessage=''
DECLARE @ErrorSeverity INT;
DECLARE @ErrorState INT;
		
BEGIN TRANSACTION;

BEGIN TRY
	DECLARE @hDocItems INT                                                                                                                                                                
	EXEC sp_xml_preparedocument @hDocItems OUTPUT, @strText


	DECLARE @TEMP TABLE
	(
		tintActionType TINYINT,
		numBizDocTypeID NUMERIC(18,0)
	)
	INSERT INTO 
		@TEMP
	SELECT 
		tintActionType,
		numBizDocTypeID 
	FROM 
		OPENXML (@hDocItems, '/NewDataSet/WorkFlowActionList',2) 
	WITH 
		(
			tintActionType TINYINT, 
			numBizDocTypeID numeric(18,0)
		)

	IF (SELECT COUNT(*) FROM @TEMP WHERE tintActionType = 6 AND (numBizDocTypeID=287 OR numBizDocTypeID=29397)) > 0
		BEGIN
			--CHECK IF CREATE INVOICE OR CREATE PACKING SLIP RULE EXISTS IN MASS SALES FULLFILLMENT
			IF (
				SELECT 
					COUNT(*) 
				FROM 
					OpportunityAutomationRules 
				WHERE 
					numDomainID = @numDomainID AND 
					(numRuleID = 1 OR numRuleID = 2)
				) > 0
			BEGIN
				RAISERROR ('MASS SALES FULLFILLMENT RULE EXISTS',16,1);
				RETURN -1
			END
		END


	IF @numWFID=0
	BEGIN
	
		INSERT INTO [dbo].[WorkFlowMaster] ([numDomainID], [vcWFName], [vcWFDescription], [numCreatedBy], [numModifiedBy], [dtCreatedDate], [dtModifiedDate], [bitActive], [numFormID], [tintWFTriggerOn],[vcWFAction],[vcDateField],[intDays],[intActionOn],[bitCustom],[numFieldID])
		SELECT @numDomainID, @vcWFName, @vcWFDescription, @numUserCntID, @numUserCntID, GETUTCDATE(), GETUTCDATE(), @bitActive, @numFormID, @tintWFTriggerOn,@vcWFAction,@vcDateField,@intDays,@intActionOn,@bitCustom,@numFieldID

		SET @numWFID=@@IDENTITY
		--start of my
		IF DATALENGTH(@strText)>2
		BEGIN                                                                                                         
			--Delete records into WorkFlowTriggerFieldList and Insert
			DELETE FROM WorkFlowTriggerFieldList WHERE  [numWFID] = @numWFID

			INSERT INTO dbo.WorkFlowTriggerFieldList (numWFID,numFieldID,bitCustom) 
				SELECT @numWFID,numFieldID,bitCustom
				  FROM OPENXML (@hDocItems, '/NewDataSet/WorkFlowTriggerFieldList',2) WITH ( numFieldID NUMERIC, bitCustom BIT )
	
	
			--Delete records into WorkFlowConditionList and Insert
			DELETE FROM WorkFlowConditionList WHERE  [numWFID] = @numWFID

			INSERT INTO dbo.WorkFlowConditionList (numWFID,numFieldID,bitCustom,vcFilterValue,vcFilterOperator,vcFilterANDOR,vcFilterData,intCompare) 
				SELECT @numWFID,numFieldID,bitCustom,vcFilterValue,vcFilterOperator,vcFilterANDOR,vcFilterData,intCompare
				  FROM OPENXML (@hDocItems, '/NewDataSet/WorkFlowConditionList',2) WITH ( numFieldID NUMERIC, bitCustom BIT, vcFilterValue VARCHAR(500), vcFilterOperator VARCHAR(10),vcFilterANDOR VARCHAR(10),vcFilterData VARCHAR(MAX),intCompare int)


			--Delete records into WorkFlowActionUpdateFields and Insert
			DELETE FROM WorkFlowActionUpdateFields WHERE numWFActionID IN(SELECT numWFActionID FROM WorkFlowActionList WHERE numWFID=@numWFID)

			--Delete records into WorkFlowActionList and Insert
			DELETE FROM WorkFlowActionList WHERE  [numWFID] = @numWFID

			INSERT INTO dbo.WorkFlowActionList (numWFID,tintActionType,numTemplateID,vcEmailToType,tintTicklerActionAssignedTo,vcAlertMessage,tintActionOrder,tintIsAttachment,vcApproval,tintSendMail,numBizDocTypeID,numBizDocTemplateID,vcSMSText,vcEmailSendTo,vcMailBody,vcMailSubject) 
				SELECT @numWFID,tintActionType,numTemplateID,vcEmailToType,tintTicklerActionAssignedTo,vcAlertMessage,tintActionOrder,tintIsAttachment,vcApproval ,tintSendMail,numBizDocTypeID,numBizDocTemplateID,vcSMSText,vcEmailSendTo,vcMailBody ,vcMailSubject
				  FROM OPENXML (@hDocItems, '/NewDataSet/WorkFlowActionList',2) WITH ( tintActionType TINYINT, numTemplateID NUMERIC,vcEmailToType VARCHAR(50),tintTicklerActionAssignedTo TINYINT,vcAlertMessage NTEXT,tintActionOrder TINYINT,tintIsAttachment TINYINT,vcApproval VARCHAR(100),tintSendMail TINYINT,numBizDocTypeID numeric(18,0),numBizDocTemplateID numeric(18,0),vcSMSText NTEXT,vcEmailSendTo varchar(500),vcMailBody ntext,vcMailSubject varchar(500))

			INSERT INTO dbo.WorkFlowActionUpdateFields (numWFActionID,tintModuleType,numFieldID,bitCustom,vcValue,vcData) 
				SELECT WFA.numWFActionID,tintModuleType,numFieldID,bitCustom,vcValue,vcData
				  FROM OPENXML (@hDocItems, '/NewDataSet/WorkFlowActionUpdateFields',2) WITH ( numWFActionID NUMERIC, tintModuleType NUMERIC,numFieldID NUMERIC,bitCustom BIT,vcValue VARCHAR(500),vcData VARCHAR(500) ) WFAUF
				  JOIN WorkFlowActionList WFA ON WFA.tintActionOrder=WFAUF.numWFActionID WHERE WFA.numWFID=@numWFID

			EXEC sp_xml_removedocument @hDocItems
		END	
		--end of my
	END
	ELSE
	BEGIN
	
		UPDATE [dbo].[WorkFlowMaster]
		SET    [vcWFName] = @vcWFName, [vcWFDescription] = @vcWFDescription, [numModifiedBy] = @numUserCntID, [dtModifiedDate] = GETUTCDATE(), [bitActive] = @bitActive, [numFormID] = @numFormID, [tintWFTriggerOn] = @tintWFTriggerOn,[vcWFAction]=@vcWFAction,[vcDateField]=@vcDateField,[intDays]=@intDays,[intActionOn]=@intActionOn,[bitCustom]=@bitCustom,[numFieldID]=@numFieldID
		WHERE  [numDomainID] = @numDomainID AND [numWFID] = @numWFID
	
		IF DATALENGTH(@strText)>2
		BEGIN
			DECLARE @hDocItem INT                                                                                                                                                                
	
			EXEC sp_xml_preparedocument @hDocItem OUTPUT, @strText                                                                                                             
        
			--Delete records into WorkFlowTriggerFieldList and Insert
			DELETE FROM WorkFlowTriggerFieldList WHERE  [numWFID] = @numWFID

			INSERT INTO dbo.WorkFlowTriggerFieldList (numWFID,numFieldID,bitCustom) 
				SELECT @numWFID,numFieldID,bitCustom
				  FROM OPENXML (@hDocItem, '/NewDataSet/WorkFlowTriggerFieldList',2) WITH ( numFieldID NUMERIC, bitCustom BIT )
	
	
			--Delete records into WorkFlowConditionList and Insert
			DELETE FROM WorkFlowConditionList WHERE  [numWFID] = @numWFID

			INSERT INTO dbo.WorkFlowConditionList (numWFID,numFieldID,bitCustom,vcFilterValue,vcFilterOperator,vcFilterANDOR,vcFilterData,intCompare) 
				SELECT @numWFID,numFieldID,bitCustom,vcFilterValue,vcFilterOperator,vcFilterANDOR,vcFilterData,intCompare
				  FROM OPENXML (@hDocItem, '/NewDataSet/WorkFlowConditionList',2) WITH ( numFieldID NUMERIC, bitCustom BIT, vcFilterValue VARCHAR(500), vcFilterOperator VARCHAR(10),vcFilterANDOR VARCHAR(10),vcFilterData varchar(MAX),intCompare int)


			--Delete records into WorkFlowActionUpdateFields and Insert
			DELETE FROM WorkFlowActionUpdateFields WHERE numWFActionID IN(SELECT numWFActionID FROM WorkFlowActionList WHERE numWFID=@numWFID)

			--Delete records into WorkFlowActionList and Insert
			DELETE FROM WorkFlowActionList WHERE  [numWFID] = @numWFID

			INSERT INTO dbo.WorkFlowActionList (numWFID,tintActionType,numTemplateID,vcEmailToType,tintTicklerActionAssignedTo,vcAlertMessage,tintActionOrder,tintIsAttachment,vcApproval ,tintSendMail,numBizDocTypeID ,numBizDocTemplateID,vcSMSText,vcEmailSendTo,vcMailBody,vcMailSubject ) 
				SELECT @numWFID,tintActionType,numTemplateID,vcEmailToType,tintTicklerActionAssignedTo,vcAlertMessage,tintActionOrder,tintIsAttachment,vcApproval ,tintSendMail,numBizDocTypeID ,numBizDocTemplateID,vcSMSText ,vcEmailSendTo,vcMailBody,vcMailSubject
				  FROM OPENXML (@hDocItem, '/NewDataSet/WorkFlowActionList',2) WITH ( tintActionType TINYINT, numTemplateID NUMERIC,vcEmailToType VARCHAR(50),tintTicklerActionAssignedTo TINYINT,vcAlertMessage NTEXT,tintActionOrder TINYINT,tintIsAttachment TINYINT,vcApproval VARCHAR(100),tintSendMail TINYINT,numBizDocTypeID numeric(18,0),numBizDocTemplateID numeric(18,0),vcSMSText NTEXT ,vcEmailSendTo varchar(500),vcMailBody ntext,vcMailSubject varchar(500))

			INSERT INTO dbo.WorkFlowActionUpdateFields (numWFActionID,tintModuleType,numFieldID,bitCustom,vcValue,vcData) 
				SELECT WFA.numWFActionID,tintModuleType,numFieldID,bitCustom,vcValue,vcData
				  FROM OPENXML (@hDocItem, '/NewDataSet/WorkFlowActionUpdateFields',2) WITH ( numWFActionID NUMERIC, tintModuleType NUMERIC,numFieldID NUMERIC,bitCustom BIT,vcValue VARCHAR(500),vcData VARCHAR(500)) WFAUF
				  JOIN WorkFlowActionList WFA ON WFA.tintActionOrder=WFAUF.numWFActionID WHERE WFA.numWFID=@numWFID

			EXEC sp_xml_removedocument @hDocItem
		END	
	END
END TRY
BEGIN CATCH

	SELECT @ErrorMessage = ERROR_MESSAGE(), @ErrorSeverity = ERROR_SEVERITY(), @ErrorState = ERROR_STATE();

	IF @@TRANCOUNT > 0
	ROLLBACK TRANSACTION;

	-- Use RAISERROR inside the CATCH block to return error
	-- information about the original error that caused
	-- execution to jump to the CATCH block.
	RAISERROR (@ErrorMessage,@ErrorSeverity,@ErrorState);
END CATCH;

IF @@TRANCOUNT > 0
	COMMIT TRANSACTION;


	

	


	

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ManageWorkOrder')
DROP PROCEDURE USP_ManageWorkOrder
GO
CREATE PROCEDURE [dbo].[USP_ManageWorkOrder]
@numItemCode AS NUMERIC(18,0),
@numWareHouseItemID AS NUMERIC(18,0),
@Units AS INT,
@numDomainID AS NUMERIC(18,0),
@numUserCntID AS NUMERIC(18,0),
@vcInstruction AS VARCHAR(1000),
@bintCompliationDate AS DATETIME,
@numAssignedTo AS NUMERIC(9)
AS
BEGIN
BEGIN TRY
BEGIN TRAN
	DECLARE @numWOId AS NUMERIC(9),@numDivisionId AS NUMERIC(9) 
	DECLARE @txtItemDesc AS varchar(1000),@vcItemName AS VARCHAR(300)
	
	SELECT @txtItemDesc=ISNULL(txtItemDesc,''),@vcItemName=ISNULL(vcItemName,'') FROM Item WHERE numItemCode=@numItemCode

	INSERT INTO WorkOrder
	(
		numItemCode,numQtyItemsReq,numWareHouseItemId,numCreatedBy,
		bintCreatedDate,numDomainID,numWOStatus,vcItemDesc,vcInstruction,
		bintCompliationDate,numAssignedTo
	)
	VALUES
	(
		@numItemCode,@Units,@numWareHouseItemID,@numUserCntID,
		getutcdate(),@numDomainID,0,@txtItemDesc,@vcInstruction,
		@bintCompliationDate,@numAssignedTo
	)
	
	SET @numWOId=@@IDENTITY

	IF @numAssignedTo>0
	BEGIN

		SET @vcItemName= 'Work Order for Item : ' + @vcItemName
		SELECT @numDivisionId=numDivisionId FROM AdditionalContactsInformation WHERE numContactId=@numAssignedTo

		EXEC dbo.usp_InsertCommunication
		@numCommId = 0, --  numeric(18, 0)
		@bitTask = 972, --  numeric(18, 0)
		@numContactId = @numUserCntID, --  numeric(18, 0)
		@numDivisionId = @numDivisionId, --  numeric(18, 0)
		@txtDetails = @vcItemName, --  text
		@numOppId = 0, --  numeric(18, 0)
		@numAssigned = @numAssignedTo, --  numeric(18, 0)
		@numUserCntID = @numUserCntID, --  numeric(18, 0)
		@numDomainId = @numDomainID, --  numeric(18, 0)
		@bitClosed = 0, --  tinyint
		@vcCalendarName = '', --  varchar(100)
		@dtStartTime = @bintCompliationDate, --  datetime
		@dtEndtime = @bintCompliationDate, --  datetime
		@numActivity = 0, --  numeric(9, 0)
		@numStatus = 0, --  numeric(9, 0)
		@intSnoozeMins = 0, --  int
		@tintSnoozeStatus = 0, --  tinyint
		@intRemainderMins = 0, --  int
		@tintRemStaus = 0, --  tinyint
		@ClientTimeZoneOffset = 0, --  int
		@bitOutLook = 0, --  tinyint
		@bitSendEmailTemplate = 0, --  bit
		@bitAlert = 0, --  bit
		@numEmailTemplate = 0, --  numeric(9, 0)
		@tintHours = 0, --  tinyint
		@CaseID = 0, --  numeric(18, 0)
		@CaseTimeId = 0, --  numeric(18, 0)
		@CaseExpId = 0, --  numeric(18, 0)
		@ActivityId = 0, --  numeric(18, 0)
		@bitFollowUpAnyTime = 0, --  bit
		@strAttendee=''
END
	
	INSERT INTO [WorkOrderDetails] 
	(
		numWOId,numItemKitID,numChildItemID,numQtyItemsReq,numWareHouseItemId,vcItemDesc,sintOrder,numQtyItemsReq_Orig,numUOMId
	)
	SELECT 
		@numWOId,numItemKitID,numItemCode,
		CAST((DTL.numQtyItemsReq * @Units)AS NUMERIC(9,0)),
		isnull(Dtl.numWareHouseItemId,0),
		ISNULL(Dtl.vcItemDesc,txtItemDesc),
		ISNULL(sintOrder,0),
		DTL.numQtyItemsReq,
		Dtl.numUOMId 
	FROM 
		item                                
	INNER JOIN 
		ItemDetails Dtl 
	ON 
		numChildItemID=numItemCode
	WHERE 
		numItemKitID=@numItemCode

	--UPDATE ON ORDER OF ASSEMBLY
	UPDATE 
		WareHouseItems
	SET    
		numOnOrder= numOnOrder + @Units,
		dtModified = GETDATE() 
	WHERE   
		numWareHouseItemID = @numWareHouseItemID 
	
	--MAKE ENTRY IN WAREHOUSE TRACKING TABLE
	DECLARE @Description AS VARCHAR(1000)
	SET @Description='Work Order Created (Qty:' + CAST(@Units AS VARCHAR(10)) + ')'

	EXEC dbo.USP_ManageWareHouseItems_Tracking
	@numWareHouseItemID = @numWareHouseItemID, --  numeric(9, 0)
	@numReferenceID = @numWOId, --  numeric(9, 0)
	@tintRefType = 2, --  tinyint
	@vcDescription = @Description, --  varchar(100)
	@numModifiedBy = @numUserCntID,
	@numDomainID = @numDomainID

	EXEC USP_ManageInventoryWorkOrder @numWOId,@numDomainID,@numUserCntID
COMMIT TRAN 
END TRY 
BEGIN CATCH		
    IF ( @@TRANCOUNT > 0 ) 
    BEGIN
        ROLLBACK TRAN
    END

	DECLARE @ErrorMessage NVARCHAR(4000);
    DECLARE @ErrorSeverity INT;
    DECLARE @ErrorState INT;

    SELECT @ErrorMessage = ERROR_MESSAGE(), @ErrorSeverity = ERROR_SEVERITY(), @ErrorState = ERROR_STATE();

    RAISERROR (@ErrorMessage, @ErrorSeverity, @ErrorState);
END CATCH
END

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO

-- exec USP_ManageWorkOrderStatus @numWOId=10086,@numWOStatus=23184,@numUserCntID=1,@numDomainID=1
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ManageWorkOrderStatus')
DROP PROCEDURE USP_ManageWorkOrderStatus
GO
CREATE PROCEDURE [dbo].[USP_ManageWorkOrderStatus]
@numWOId as numeric(9)=0,
@numWOStatus as numeric(9),
@numUserCntID AS NUMERIC(9)=0,
@numDomainID AS NUMERIC(18,0)
AS
BEGIN
BEGIN TRY
BEGIN TRANSACTION
	DECLARE @numDomain AS NUMERIC(18,0)
	DECLARE @CurrentWorkOrderStatus AS NUMERIC(18,0)
	SELECT @numDomain = @numDomainID

	--GET CURRENT STATUS OF WORK ORDER
	SELECT @CurrentWorkOrderStatus=numWOStatus FROM WorkOrder WHERE numWOId=@numWOId AND [numDomainID] = @numDomainID

	--ASSIGN NEW STATUS TO WORK ORDER
	UPDATE WorkOrder SET numWOStatus=@numWOStatus WHERE numWOId=@numWOId AND [numDomainID] = @numDomainID

	--IF CURRENT WORK ORDER STATUS IS NOT COMPLETED AND NEW WORK ORDER STATUS IS COMPLETED THEN MAKE INVENTORY CHANGES
	IF ISNULL(@CurrentWorkOrderStatus,0) <> 23184 AND @numWOStatus=23184
	BEGIN
		DECLARE @PendingChildWO NUMERIC(18,0)

		;WITH CTE (numWOID) AS
		(
			SELECT
				numWOID 
			FROM 
				WorkOrder
			WHERE 
				numParentWOID = @numWOId 
				AND numWOStatus <> 23184
			UNION ALL
			SELECT 
				WorkOrder.numWOID 
			FROM 
				WorkOrder
			INNER JOIN
				CTE c
			ON
				 WorkOrder.numParentWOID = c.numWOID
			WHERE 
				WorkOrder.numWOStatus <> 23184
		)

		SELECT @PendingChildWO = COUNT(*) FROM CTE

		--WE HAVE IMPLEMENTED CODE IN SUCH WAY THAT FIRST CHILD WORK ORDER GET COMPLETED
		--SO IF SOME REASON CHILD WORK ORDER IS NOT COMPLETED THAN RAISERROR
		IF ISNULL(@PendingChildWO,0) > 0
		BEGIN
			RAISERROR('CHILD WORK ORDER STILL NOT COMPLETED',16,1);
		END

		DECLARE @numWareHouseItemID AS NUMERIC(9),@numQtyItemsReq AS NUMERIC(9),@numOppId AS NUMERIC(9)

		UPDATE WorkOrder SET bintCompletedDate=GETDATE() WHERE numWOId=@numWOId AND [numDomainID] = @numDomainID

		SELECT @numWareHouseItemID=numWareHouseItemID,@numQtyItemsReq=numQtyItemsReq,@numOppId=ISNULL(numOppId,0) FROM WorkOrder WHERE numWOId=@numWOId AND [numDomainID] = @numDomainID
	
		EXEC USP_UpdatingInventory 0,@numWareHouseItemID,@numQtyItemsReq,@numWOId,@numUserCntID,@numOppId
	
		--COMMENTED BECAUSE FOLLOWING INVENTORY CHANGES ARE DONE WHEN ORDER IS CREATED 
		--RATHER THEN WAITING FOR INVENTORY TO BE COMPLETED THROUGH STORE PROCEDURE 
		--CALLED USP_ManageInventoryAssemblyWO
		--IF @numOppId>0
		--  BEGIN
	
		--	declare @onHand as NUMERIC,@onOrder as NUMERIC,@onBackOrder as NUMERIC,@onAllocation as numeric                                    

		--	SELECT @onHand=isnull(numOnHand,0),@onAllocation=isnull(numAllocation,0),                                    
		--			@onOrder=isnull(numOnOrder,0),@onBackOrder=isnull(numBackOrder,0)                                     
		--			from WareHouseItems where numWareHouseItemID=@numWareHouseItemID  
		--			AND [numDomainID] = @numDomainID

		--	if @onHand>=@numQtyItemsReq                                    
		--		begin                                    
		--			set @onHand=@onHand-@numQtyItemsReq                            
		--			set @onAllocation=@onAllocation+@numQtyItemsReq                                    
		--		end                                    
		--	else if @onHand<@numQtyItemsReq                                    
		--		begin                                    
		--			set @onAllocation=@onAllocation+@onHand                                    
		--			set @onBackOrder=@onBackOrder+@numQtyItemsReq-@onHand                                    
		--			set @onHand=0                                    
		--		end   
			                                  
		--		 update WareHouseItems set numOnHand=@onHand,                                    
		--		 numAllocation=@onAllocation,                                    
		--		 numBackOrder=@onBackOrder,dtModified = GETDATE()                                     
		--		 where numWareHouseItemID=@numWareHouseItemID 
		--		 AND [numDomainID] = @numDomainID 
			 
		--		EXEC dbo.USP_ManageWareHouseItems_Tracking
		--			@numWareHouseItemID = @numWareHouseItemID, --  numeric(9, 0)
		--			@numReferenceID = @numWOId, --  numeric(9, 0)
		--			@tintRefType = 2, --  tinyint
		--			@vcDescription = 'WorkOrder', --  varchar(100)
		--			@numModifiedBy = @numUserCntID
		--			,@ClientTimeZoneOffset = 0
		--			,@dtRecordDate = NULL
		--			,@numDomainID = @numDomain 
		--	END	
		 
		DECLARE WO_Items CURSOR FOR 
		SELECT numWareHouseItemId,numQtyItemsReq FROM WorkOrderDetails WD JOIN item i ON WD.numChildItemID=i.numItemCode WHERE numWOId=@numWOId AND i.charItemType IN ('P')
							
		OPEN WO_Items

		FETCH NEXT FROM WO_Items into @numWareHouseItemId,@numQtyItemsReq
		WHILE @@FETCH_STATUS = 0
			BEGIN  
				EXEC USP_UpdatingInventory 1,@numWareHouseItemID,@numQtyItemsReq,@numWOId,@numUserCntID,@numOppId

				FETCH NEXT FROM WO_Items into @numWareHouseItemId,@numQtyItemsReq
			END

		CLOSE WO_Items
		DEALLOCATE WO_Items
	END
COMMIT
END TRY
BEGIN CATCH
	DECLARE @ErrorMessage NVARCHAR(4000)
	DECLARE @ErrorNumber INT
	DECLARE @ErrorSeverity INT
	DECLARE @ErrorState INT
	DECLARE @ErrorLine INT
	DECLARE @ErrorProcedure NVARCHAR(200)

	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;

	SET @numOppID = 0

	SELECT 
		@ErrorMessage = ERROR_MESSAGE(),
		@ErrorNumber = ERROR_NUMBER(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE(),
		@ErrorLine = ERROR_LINE(),
		@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

	RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);
END CATCH
END
/****** Object:  StoredProcedure [dbo].[USP_OPPItemDetails]    Script Date: 09/01/2009 18:59:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- exec USP_OPPItemDetails @numItemCode=31455,@OpportunityId=12387,@numDomainId=110
--created by anooop jayaraj
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_oppitemdetails')
DROP PROCEDURE usp_oppitemdetails
GO
CREATE PROCEDURE [dbo].[USP_OPPItemDetails]
    (
      @numUserCntID NUMERIC(9) = 0,
      @OpportunityId NUMERIC(9) = 0,
      @numDomainId NUMERIC(9),
      @ClientTimeZoneOffset  INT
    )
AS 
    BEGIN
        IF @OpportunityId >0
            BEGIN
                DECLARE @DivisionID AS NUMERIC(9)
                DECLARE @TaxPercentage AS FLOAT
                DECLARE @tintOppType AS TINYINT
                DECLARE @fld_Name AS VARCHAR(500)
                DECLARE @strSQL AS VARCHAR(8000)
                DECLARE @fld_ID AS NUMERIC(9)
                DECLARE @numBillCountry AS NUMERIC(9)
                DECLARE @numBillState AS NUMERIC(9)

                
                SELECT  @DivisionID = numDivisionID,
                        @tintOppType = tintOpptype
                FROM    OpportunityMaster
                WHERE   numOppId = @OpportunityId


               -- Calculate Tax based on Country and state
             /*  SELECT  @numBillState = ISNULL(numState, 0),
						@numBillCountry = ISNULL(numCountry, 0)
			   FROM     dbo.AddressDetails
			   WHERE    numDomainID = @numDomainID
						AND numRecordID = @DivisionID
						AND tintAddressOf = 2
						AND tintAddressType = 1
						AND bitIsPrimary=1
                SET @TaxPercentage = 0
                IF @numBillCountry > 0
                    AND @numBillState > 0 
                    BEGIN
                        IF @numBillState > 0 
                            BEGIN
                                IF EXISTS ( SELECT  COUNT(*)
                                            FROM    TaxDetails
                                            WHERE   numCountryID = @numBillCountry
                                                    AND numStateID = @numBillState
                                                    AND numDomainID = @numDomainID ) 
                                    SELECT  @TaxPercentage = decTaxPercentage
                                    FROM    TaxDetails
                                    WHERE   numCountryID = @numBillCountry
                                            AND numStateID = @numBillState
                                            AND numDomainID = @numDomainID
                                ELSE 
                                    SELECT  @TaxPercentage = decTaxPercentage
                                    FROM    TaxDetails
                                    WHERE   numCountryID = @numBillCountry
                                            AND numStateID = 0
                                            AND numDomainID = @numDomainID
                            END
                    END
                ELSE 
                    IF @numBillCountry > 0 
                        SELECT  @TaxPercentage = decTaxPercentage
                        FROM    TaxDetails
                        WHERE   numCountryID = @numBillCountry
                                AND numStateID = 0
                                AND numDomainID = @numDomainID
               
               */
               

SET @strSQL = ''

--------------Make Dynamic Query--------------
SET @strSQL = @strSQL + '
Select 
Opp.numoppitemtCode,
ISNULL(Opp.bitItemPriceApprovalRequired,0) AS bitItemPriceApprovalRequired,
OM.tintOppType,
Opp.numItemCode,
I.charItemType,
ISNULL(I.bitAsset,0) as bitAsset,
ISNULL(I.bitRental,0) as bitRental,
Opp.numOppId,
Opp.bitWorkOrder,
Opp.fltDiscount,
ISNULL(( SELECT numReturnID FROM   Returns WHERE  numOppItemCode = Opp.numoppitemtCode AND numOppId = Opp.numOppID), 0) AS numReturnID,
dbo.fn_GetReturnItemQty(Opp.numoppitemtCode) AS ReturrnedQty,
[dbo].fn_GetReturnStatus(Opp.numoppitemtCode) AS ReturnStatus,
ISNULL(opp.numUOMId, 0) AS numUOM,
(SELECT vcUnitName FROM dbo.UOM WHERE numUOMId = opp.numUOMId AND ISNULL(bitEnabled,0) = 1)  AS  [vcBaseUOMName],
Opp.numUnitHour AS numOriginalUnitHour, 
isnull(M.vcPOppName,''Internal'') as Source,
numSourceID,
  CASE WHEN ISNULL(bitKitParent,0) = 1
                               AND ISNULL(bitAssembly,0) = 1 THEN 0
                          WHEN ISNULL(bitKitParent,0) = 1 THEN 1
                          ELSE 0
                     END as bitKitParent,
(CASE WHEN (I.charItemType = ''p'' AND ISNULL(bitDropShip, 0) = 0 AND ISNULL(bitAsset,0)=0)THEN dbo.CheckOrderItemInventoryStatus(Opp.numItemCode,Opp.numUnitHour,Opp.numoppitemtCode,isnull(Opp.numWarehouseItmsID,0),(CASE WHEN ISNULL(bitKitParent,0) = 1
                               AND ISNULL(bitAssembly,0) = 1 THEN 0
                          WHEN ISNULL(bitKitParent,0) = 1 THEN 1
                          ELSE 0
                     END)) ELSE 0 END) AS bitBackOrder,
(SELECT vcData FROM dbo.ListDetails WHERE 1=1 AND numListItemID = numShipClass ) AS [numShipClass],ISNULL(Opp.numQtyShipped,0) numQtyShipped,ISNULL(Opp.numUnitHourReceived,0) AS numUnitHourReceived,ISNULL(OM.fltExchangeRate,0) AS fltExchangeRate,ISNULL(OM.numCurrencyID,0) AS numCurrencyID,ISNULL(OM.numDivisionID,0) AS numDivisionID,
isnull(i.numIncomeChartAcntId,0) as itemIncomeAccount,isnull(i.numAssetChartAcntId,0) as itemInventoryAsset,isnull(i.numCOGsChartAcntId,0) as itemCoGs,
ISNULL(Opp.numRentalIN,0) as numRentalIN,
ISNULL(Opp.numRentalLost,0) as numRentalLost,
ISNULL(Opp.numRentalOut,0) as numRentalOut,
(SELECT vcData FROM dbo.ListDetails WHERE 1=1 AND numListItemID = numShipClass ) AS [numShipClass],ISNULL(bitFreeShipping,0) AS [IsFreeShipping],Opp.bitDiscountType,isnull(Opp.monLandedCost,0) as monLandedCost, '

--	IF @fld_Name = 'vcPathForTImage' 
		SET @strSQL = @strSQL + 'Opp.vcPathForTImage,'
--	ELSE IF @fld_Name = 'vcItemName' 
		SET @strSQL = @strSQL + 'Opp.vcItemName,'
--	ELSE IF @fld_Name = 'vcModelID,' 
		SET @strSQL = @strSQL + 'Opp.vcModelID,'
--	ELSE IF @fld_Name = 'vcWareHouse' 
		SET @strSQL = @strSQL + 'vcWarehouse,'
		SET @strSQL = @strSQL + 'WItems.numOnHand,isnull(WItems.numAllocation,0) as numAllocation,'
		SET @strSQL = @strSQL + 'WL.vcLocation,'
--	ELSE IF @fld_Name = 'ItemType' 
		SET @strSQL = @strSQL + 'Opp.vcType ItemType,Opp.vcType,'
--	ELSE IF @fld_Name = 'vcItemDesc' 
		SET @strSQL = @strSQL + 'ISNULL(Opp.vcItemDesc, '''') vcItemDesc,'
--	ELSE IF @fld_Name = 'Attributes' 
		SET @strSQL = @strSQL + 'Opp.vcAttributes,' --dbo.fn_GetAttributes(Opp.numWarehouseItmsID, I.bitSerialized) AS Attributes,
--	ELSE IF @fld_Name = 'bitDropShip' 
		SET @strSQL = @strSQL + 'ISNULL(bitDropShip, 0) as DropShip,CASE ISNULL(bitDropShip, 0) WHEN 1 THEN ''Yes'' ELSE '''' END AS bitDropShip,'
--	ELSE IF @fld_Name = 'numUnitHour' 
		SET @strSQL = @strSQL + 'CAST(( dbo.fn_UOMConversion(ISNULL(I.numBaseUnit, 0), Opp.numItemCode,om.numDomainId, ISNULL(opp.numUOMId, 0))* Opp.numUnitHour ) AS NUMERIC(18, 2)) AS numUnitHour,'
--	ELSE IF @fld_Name = 'monPrice' 
		SET @strSQL = @strSQL + 'Opp.monPrice,'
--	ELSE IF @fld_Name = 'monTotAmount' 
		SET @strSQL = @strSQL + 'Opp.monTotAmount,'
--	ELSE IF @fld_Name = 'vcSKU' 
		SET @strSQL = @strSQL + 'ISNULL(WItems.vcWHSKU,ISNULL(I.vcSKU,'''')) [vcSKU],'
--	ELSE IF @fld_Name = 'vcManufacturer' 
		SET @strSQL = @strSQL + 'Opp.vcManufacturer,'
--	ELSE IF @fld_Name = 'vcItemClassification' 
		SET @strSQL = @strSQL + ' dbo.[fn_GetListItemName](I.[numItemClassification]) numItemClassification,'
--	ELSE IF @fld_Name = 'numItemGroup' 
		SET @strSQL = @strSQL + '( SELECT    [vcItemGroup] FROM      [ItemGroups] WHERE     [numItemGroupID] = I.numItemGroup ) numItemGroup,'
--	ELSE IF @fld_Name = 'vcUOMName' 
		SET @strSQL = @strSQL + 'ISNULL(u.vcUnitName, '''') numUOMId,'
--	ELSE IF @fld_Name = 'fltWeight' 
		SET @strSQL = @strSQL + 'I.fltWeight,'
--	ELSE IF @fld_Name = 'fltHeight' 
		SET @strSQL = @strSQL + 'I.fltHeight,'
--	ELSE IF @fld_Name = 'fltWidth' 
		SET @strSQL = @strSQL + 'I.fltWidth,'
--	ELSE IF @fld_Name = 'fltLength' 
		SET @strSQL = @strSQL + 'I.fltLength,'
--	ELSE IF @fld_Name = 'numBarCodeId' 
		SET @strSQL = @strSQL + 'I.numBarCodeId,'
		SET @strSQL = @strSQL + 'I.IsArchieve,'
--	ELSE IF @fld_Name = 'vcWorkOrderStatus' 
		SET @strSQL = @strSQL + 'CASE WHEN ISNULL(Opp.bitWorkOrder, 0) = 1
				 THEN ( SELECT  dbo.GetListIemName(numWOStatus)
								+ CASE WHEN ISNULL(numWOStatus, 0) = 23184
									   THEN '' - ''
											+ CAST(ISNULL(dbo.fn_GetContactName(numAssignedTo),
														  '''') + '',''
											+ CONVERT(VARCHAR(20), DATEADD(minute, '+ CONVERT(VARCHAR(15),-@ClientTimeZoneOffset)  +', bintCompletedDate)) AS VARCHAR(100))
									   ELSE ''''
								  END
						FROM    WorkOrder
						WHERE   ISNULL(numOppId, 0) = Opp.numOppId
								AND numItemCode = Opp.numItemCode AND ISNULL(numParentWOID,0)=0
					  )
				 ELSE ''''
			END AS vcWorkOrderStatus,'
--	ELSE IF @fld_Name = 'DiscAmt' 
		SET @strSQL = @strSQL + 'CASE WHEN Opp.bitDiscountType = 0
				 THEN CAST(ISNULL(Opp.monTotAmtBefDiscount, Opp.monTotAmount)
					  - Opp.monTotAmount AS VARCHAR(10)) + '' (''
					  + CAST(Opp.fltDiscount AS VARCHAR(10)) + ''%)''
				 ELSE CAST(ISNULL(Opp.monTotAmtBefDiscount, Opp.monTotAmount)
					  - Opp.monTotAmount AS VARCHAR(10))
			END AS DiscAmt,'	
--	ELSE IF @fld_Name = 'numProjectID' 
		SET @strSQL = @strSQL + ' ISNULL(Opp.numProjectID, 0) numProjectID,
		 CASE WHEN ( SELECT TOP 1
								ISNULL(OBI.numOppBizDocItemID, 0)
						FROM    dbo.OpportunityBizDocs OBD
								INNER JOIN dbo.OpportunityBizDocItems OBI ON OBD.numOppBizDocsID = OBI.numOppBizDocID
						WHERE   OBI.numOppItemID = Opp.numoppitemtCode
								AND ISNULL(OBD.bitAuthoritativeBizDocs, 0) = 1
								and isnull(OBD.numOppID,0)=OM.numOppID
					  ) > 0 THEN 1
				 ELSE 0
			END AS bitItemAddedToAuthBizDoc,'	
--	ELSE IF @fld_Name = 'numClassID' 
		SET @strSQL = @strSQL + 'ISNULL(Opp.numClassID, 0) numClassID,'	
--	ELSE IF @fld_Name = 'vcProjectStageName' 
		SET @strSQL = @strSQL + ' ISNULL(Opp.numProjectStageID, 0) numProjectStageID,
				CASE WHEN OPP.numProjectStageID > 0
				 THEN dbo.fn_GetProjectStageName(OPP.numProjectId,
												 OPP.numProjectStageID)
				 WHEN OPP.numProjectStageID = -1 THEN ''T&E''
				 WHEN OPP.numProjectId > 0
				 THEN CASE WHEN OM.tintOppType = 1 THEN ''S.O.''
						   ELSE ''P.O.''
					  END
				 ELSE ''-''
			END AS vcProjectStageName,'	
	        
	
	

--------------Add custom fields to query----------------
SELECT TOP 1 @fld_Name= vcFieldName,@fld_ID = numFieldId FROM View_DynamicCustomColumns
WHERE 
grp_id = 5 AND numDomainID=@numDomainId
AND numFormId=26 AND bitCustom=1 AND numUserCntID = @numUserCntID ORDER BY numFieldId
WHILE @fld_ID > 0
BEGIN ------------------

	 PRINT @fld_Name
	
	       SET @strSQL = @strSQL + 'dbo.GetCustFldValueItem('
                                    + CONVERT(VARCHAR(15), @fld_ID)
                                    + ',Opp.numItemCode) as ''' + @fld_Name + ''','
       

SELECT TOP 1 @fld_Name= vcFieldName,@fld_ID = numFieldId FROM View_DynamicCustomColumns
WHERE 
grp_id = 5 AND numDomainID=@numDomainId
AND numFormId=26 AND bitCustom=1  AND numUserCntID = @numUserCntID AND numFieldId > @fld_ID ORDER BY numFieldId
 IF @@ROWCOUNT=0
 SET @fld_ID = 0
END ------------------


                                
--Purchase Opportunity ?
IF @tintOppType = 2 
    BEGIN
        SET @strSQL =  @strSQL + 'ISNULL((SELECT sum(numQty) FROM OppWarehouseSerializedItem WHERE numOppID=OM.numOppID AND numOppItemID=Opp.numoppitemtCode),0) AS SerialLotNo,
		  CAST(Opp.numUnitHour AS NUMERIC(9, 0)) numQty,
		  (SELECT vcUnitName FROM dbo.UOM WHERE numUOMId = opp.numUOMId AND ISNULL(bitEnabled,0) = 1)  AS  [vcBaseUOMName],
		  I.bitSerialized,
		  ISNULL(I.bitLotNo, 0) AS bitLotNo,
		  isnull(Opp.numWarehouseItmsID,0) as numWarehouseItmsID,'
	END
ELSE
	BEGIN	
        SET @strSQL =  @strSQL + ' ISNULL((SELECT sum(numQty) FROM OppWarehouseSerializedItem WHERE numOppID=OM.numOppID AND numOppItemID=Opp.numoppitemtCode),0) AS SerialLotNo,
		   CAST(Opp.numUnitHour AS NUMERIC(9, 0)) numQty,
		  I.bitSerialized,
		  ISNULL(I.bitLotNo, 0) AS bitLotNo,
		  isnull(Opp.numWarehouseItmsID,0) as numWarehouseItmsID,'
	END


        
 SET @strSQL = left(@strSQL,len(@strSQL)-1) + '   
        FROM    OpportunityItems Opp
        LEFT JOIN OpportunityMaster OM ON Opp.numOppID = oM.numOppID
        JOIN item I ON Opp.numItemCode = i.numItemcode
        LEFT JOIN WareHouseItems WItems ON WItems.numWareHouseItemID = Opp.numWarehouseItmsID
        LEFT JOIN Warehouses W ON W.numWarehouseID = WItems.numWarehouseID
        LEFT JOIN WarehouseLocation WL ON WL.numWLocationID = WItems.numWLocationID
        LEFT JOIN OpportunityMaster M ON Opp.numSourceID = M.numOppID
        LEFT JOIN UOM u ON u.numUOMId = opp.numUOMId
WHERE   om.numDomainId = '+ CONVERT(VARCHAR(10),@numDomainId) + '
        AND Opp.numOppId = '+ CONVERT(VARCHAR(15),@OpportunityId) +'
ORDER BY numoppitemtCode ASC '
  
PRINT @strSQL;
EXEC (@strSQL);
SELECT  vcSerialNo,
       vcComments AS Comments,
        numOppItemID AS numoppitemtCode,
        W.numWarehouseItmsDTLID,
        numWarehouseItmsID AS numWItmsID,
        dbo.Fn_getattributes(W.numWarehouseItmsDTLID,
                             1) Attributes
FROM    WareHouseItmsDTL W
        JOIN OppWarehouseSerializedItem O ON O.numWarehouseItmsDTLID = W.numWareHouseItmsDTLID
WHERE   numOppID = @OpportunityId


CREATE TABLE #tempForm (tintOrder TINYINT,vcDbColumnName nvarchar(50),vcOrigDbColumnName nvarchar(50),vcFieldName nvarchar(50),vcAssociatedControlType nvarchar(50),
  vcListItemType varchar(3),numListID numeric(9),vcLookBackTableName varchar(50),bitCustomField BIT,
  numFieldId NUMERIC,bitAllowSorting BIT,bitAllowEdit BIT,bitIsRequired BIT,bitIsEmail BIT,bitIsAlphaNumeric BIT,bitIsNumeric BIT,
  bitIsLengthValidation BIT,intMaxLength int,intMinLength int,bitFieldMessage BIT,vcFieldMessage VARCHAR(500),ListRelID numeric(9),intColumnWidth int,intVisible int,vcFieldDataType CHAR(1))


IF
	(
		SELECT 
			COUNT(*)
		FROM 
			View_DynamicColumns DC 
		WHERE 
			DC.numFormId=26 and DC.numDomainID=@numDomainID AND numUserCntID = @numUserCntID AND
			ISNULL(DC.bitSettingField,0)=1 AND ISNULL(DC.bitDeleted,0)=0 AND ISNULL(DC.bitCustom,0)=0) > 0
	OR
	(
		SELECT 
			COUNT(*)
		FROM 
			View_DynamicCustomColumns
		WHERE 
			numFormId=26 AND numUserCntID=@numUserCntID AND numDomainID=@numDomainID AND tintPageType=1 AND numRelCntType=0 AND ISNULL(bitCustom,0)=1
	) > 0
BEGIN	

INSERT INTO #tempForm
select isnull(DC.tintRow,0)+1 as tintOrder,DDF.vcDbColumnName,DDF.vcOrigDbColumnName,ISNULL(DDF.vcCultureFieldName,DDF.vcFieldName),                  
 DDF.vcAssociatedControlType,DDF.vcListItemType,DDF.numListID,DDF.vcLookBackTableName                                                 
,DDF.bitCustom,DDF.numFieldId,DDF.bitAllowSorting,DDF.bitInlineEdit,
DDF.bitIsRequired,DDF.bitIsEmail,DDF.bitIsAlphaNumeric,DDF.bitIsNumeric,DDF.bitIsLengthValidation,
DDF.intMaxLength,DDF.intMinLength,DDF.bitFieldMessage,DDF.vcFieldMessage vcFieldMessage,DDF.ListRelID,DDF.intColumnWidth,Case when DC.numFieldID is null then 0 else 1 end,DDF.vcFieldDataType
 FROM View_DynamicDefaultColumns DDF left join View_DynamicColumns DC on DC.numFormId=26 and DDF.numFieldId=DC.numFieldId and DC.numUserCntID=@numUserCntID and DC.numDomainID=@numDomainID and numRelCntType=0 AND tintPageType=1 
 where DDF.numFormId=26 and DDF.numDomainID=@numDomainID AND ISNULL(DDF.bitSettingField,0)=1 AND ISNULL(DDF.bitDeleted,0)=0 AND ISNULL(DDF.bitCustom,0)=0
       
 UNION
    
    select tintRow+1 as tintOrder,vcDbColumnName,vcFieldName,vcFieldName,                  
 vcAssociatedControlType,'' as vcListItemType,numListID,''                                                 
,bitCustom,numFieldId,bitAllowSorting,0 as bitAllowEdit,
bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage,ListRelID,intColumnWidth,1,''
 from View_DynamicCustomColumns
 where numFormId=26 and numUserCntID=@numUserCntID and numDomainID=@numDomainID AND tintPageType=1 AND numRelCntType=0
 AND ISNULL(bitCustom,0)=1

END
ELSE
BEGIN
	--Check if Master Form Configuration is created by administrator if NoColumns=0
	DECLARE @IsMasterConfAvailable BIT = 0
	DECLARE @numUserGroup NUMERIC(18,0)

	SELECT @numUserGroup = numGroupID FROM UserMaster WHERE numUserDetailId = @numUserCntID 

	IF (SELECT COUNT(*) FROM BizFormWizardMasterConfiguration WHERE numDomainID = @numDomainID AND numFormID = 26 AND bitGridConfiguration = 1 AND numGroupID = @numUserGroup) > 0
	BEGIN
		SET @IsMasterConfAvailable = 1
	END

	--If MasterConfiguration is available then load it otherwise load default columns
	IF @IsMasterConfAvailable = 1
	BEGIN
		INSERT INTO #tempForm
		SELECT 
			(intRowNum + 1) as tintOrder, vcDbColumnName, vcOrigDbColumnName,ISNULL(vcCultureFieldName,vcFieldName),                  
			vcAssociatedControlType,vcListItemType,numListID,vcLookBackTableName,bitCustom,numFieldId,bitAllowSorting,bitInlineEdit,
			bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,intMaxLength,intMinLength,
			bitFieldMessage,vcFieldMessage vcFieldMessage,ListRelID,intColumnWidth,1,vcFieldDataType
		FROM 
			View_DynamicColumnsMasterConfig
		WHERE
			View_DynamicColumnsMasterConfig.numFormId=26 AND 
			View_DynamicColumnsMasterConfig.numDomainID=@numDomainID AND 
			View_DynamicColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
			View_DynamicColumnsMasterConfig.bitGridConfiguration = 1 AND
			ISNULL(View_DynamicColumnsMasterConfig.bitSettingField,0)=1 AND 
			ISNULL(View_DynamicColumnsMasterConfig.bitCustom,0)=0
		UNION
		SELECT 
			tintRow+1 as tintOrder,vcDbColumnName,vcFieldName,vcFieldName,                  
			 vcAssociatedControlType,'' as vcListItemType,numListID,''                                                 
			,bitCustom,numFieldId,bitAllowSorting,0 as bitAllowEdit,
			bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
			intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage,ListRelID,intColumnWidth,1,''
		FROM 
			View_DynamicCustomColumnsMasterConfig
		WHERE 
			View_DynamicCustomColumnsMasterConfig.numFormId=26 AND 
			View_DynamicCustomColumnsMasterConfig.numDomainID=@numDomainID AND 
			View_DynamicCustomColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
			View_DynamicCustomColumnsMasterConfig.bitGridConfiguration = 1 AND
			ISNULL(View_DynamicCustomColumnsMasterConfig.bitCustom,0)=1
		ORDER BY 
			tintOrder ASC  
	END
END


select * from #tempForm order by tintOrder

drop table #tempForm

/*
                        SET @strSQL = 'select  Opp.numoppitemtCode,Opp.numOppId,
                               Opp.numItemCode,                                    
                               Opp.vcItemName,                                                                    
                               ISNULL(Opp.vcItemDesc,'''') as [Desc],                                                                    
                               Opp.vcType ItemType,                                   
                               Opp.vcPathForTImage,
                               Opp.vcModelID,
							    CAST((dbo.fn_UOMConversion(ISNULL(I.numBaseUnit,0),Opp.numItemCode,om.numDomainId,ISNULL(opp.numUOMId,0)) * Opp.numUnitHour) as numeric(18,2)) as numUnitHour,   
                               Opp.monPrice,                                                                    
                               Opp.monTotAmount,                                                     
							   ISNULL(Opp.vcItemDesc,'''') vcItemDesc,
							   Opp.vcManufacturer,
							   I.charItemType,
							   I.bitKitParent,
							   I.bitAssembly,
							   I.vcSKU,                                   
							   I.numBarCodeId,
							   dbo.[fn_GetListItemName](I.[numItemClassification]) vcClassification,
							   (SELECT [vcItemGroup] FROM [ItemGroups] WHERE [numItemGroupID] =I.numItemGroup) vcItemGroup,
							   ISNULL(I.vcUnitofMeasure,'''') vcUnitofMeasure,
							   I.fltWeight,I.fltHeight,I.fltWidth,I.fltLength,
							   WItems.numWarehouseID,
                               Opp.numWarehouseItmsID,isnull(Opp.numToWarehouseItemID,0) numToWarehouseItemID,bitDropShip DropShip, CASE ISNULL(bitDropShip,0) when 1 then ''Yes'' else '''' end as vcDropShip,
                               0 as Op_Flag,isnull(Opp.bitDiscountType,0) as bitDiscountType,isnull(Opp.fltDiscount,0) as fltDiscount,isnull(monTotAmtBefDiscount,0) as monTotAmtBefDiscount,                                    
                               vcWarehouse as Warehouse,I.bitSerialized,isnull(I.bitLotNo,0) as bitLotNo,vcAttributes, 
                               dbo.fn_GetReturnItemQty(Opp.numoppitemtCode) as ReturrnedQty,
                               [dbo].fn_GetReturnStatus(Opp.numoppitemtCode) as ReturnStatus,
							   isnull((SELECT numReturnID FROM Returns WHERE numOppItemCode=Opp.numoppitemtCode and numOppId=Opp.numOppID),0) as numReturnID,
                               isnull(v.monCost,0) as VendorCost,
                               bitTaxable,
                               case when bitTaxable =0 then 0 when bitTaxable=1 then '
                            + CONVERT(VARCHAR(15), @TaxPercentage)
                            + ' end as Tax,                                                
                                   isnull(M.vcPOppName,''Internal'') as Source,dbo.fn_GetAttributes(Opp.numWarehouseItmsID,I.bitSerialized) as Attributes  ,numSourceID '
                            + CASE WHEN @strSQL = '' THEN ''
                                   ELSE ',' + @strSQL
                              END
                            + ',isnull(opp.numUOMId,0) AS numUOM,ISNULL(u.vcUnitName,'''') vcUOMName,dbo.fn_UOMConversion(opp.numUOMId,Opp.numItemCode,om.numDomainId,null) AS UOMConversionFactor
                          ,case when Opp.bitDiscountType=0 then  cast(isnull(Opp.monTotAmtBefDiscount,Opp.monTotAmount)-Opp.monTotAmount AS VARCHAR(10)) +  '' ('' + CAST(Opp.fltDiscount AS VARCHAR(10)) +  ''%)'' 
                          ELSE cast(isnull(Opp.monTotAmtBefDiscount,Opp.monTotAmount)-Opp.monTotAmount AS VARCHAR(10)) end as DiscAmt
                           ,isnull(I.numVendorID,0) numVendorID,OM.tintOppType,cast(Opp.numUnitHour as numeric(9,0)) as numQty,SUBSTRING(
(SELECT '','' + vcSerialNo + CASE WHEN isnull(I.bitLotNo,0)=1 THEN '' ('' + CONVERT(VARCHAR(15),whi.numQty) + '')'' ELSE '''' end 
FROM OppWarehouseSerializedItem oppI JOIN WareHouseItmsDTL whi ON oppI.numWareHouseItmsDTLID=whi.numWareHouseItmsDTLID where oppI.numOppID=Opp.numOppId and oppI.numOppItemID=Opp.numoppitemtCode 
ORDER BY vcSerialNo FOR XML PATH('''')),2,200000) AS SerialLotNo, ISNULL(Opp.bitWorkOrder,0) as bitWorkOrder,
CASE WHEN ISNULL(Opp.bitWorkOrder,0)=1 then (select dbo.GetListIemName(numWOStatus) + CASE WHEN isnull(numWOStatus,0)=23184 THEN
'' - '' + CAST(ISNULL(dbo.fn_GetContactName(numAssignedTo), '''')+'',''+convert(varchar(20),dateadd(minute,' + CONVERT(VARCHAR(15),-@ClientTimeZoneOffset) + ',bintCompletedDate)) AS VARCHAR(100)) ELSE '''' end
 from WorkOrder where isnull(numOppId,0)=Opp.numOppId and numItemCode=Opp.numItemCode) else '''' end as vcWorkOrderStatus,
 ISNULL(Opp.numVendorWareHouse,0) as numVendorWareHouse,ISNULL(Opp.numShipmentMethod,0) as numShipmentMethod,ISNULL(Opp.numSOVendorId,0) as numSOVendorId,Opp.numUnitHour as numOriginalUnitHour,
 isnull(Opp.numProjectID,0) numProjectID,isnull(Opp.numClassID,0) numClassID,isnull(Opp.numProjectStageID,0) numProjectStageID,Case When OPP.numProjectStageID>0 then dbo.fn_GetProjectStageName(OPP.numProjectId,OPP.numProjectStageID) when OPP.numProjectStageID=-1 then ''T&E'' When OPP.numProjectId>0 then case when OM.tintOppType=1 then ''S.O.'' else ''P.O.'' end else ''-'' end as vcProjectStageName,
 CASE WHEN (SELECT TOP 1 ISNULL(OBI.numOppBizDocItemID,0) FROM dbo.OpportunityBizDocs OBD INNER JOIN dbo.OpportunityBizDocItems OBI ON OBD.numOppBizDocsID = OBI.numOppBizDocID WHERE OBI.numOppItemID = Opp.numoppitemtCode AND ISNULL(OBD.bitAuthoritativeBizDocs,0)=1 ) > 0
      THEN 1 ELSE 0 END AS bitItemAddedToAuthBizDoc,isnull(I.numItemClass,0) as numItemClass
                              from OpportunityItems Opp                    
                               left join OpportunityMaster OM                                                            
                                  on Opp.numOppID=oM.numOppID                                                                     
                                 join item I                                                                    
                                  on Opp.numItemCode=i.numItemcode                                            
                                left join Vendor v 
									on v.numVendorID= I.numVendorID and I.numItemCode =v.numItemCode
                               left join  WareHouseItems  WItems                                        
                               on   WItems.numWareHouseItemID= Opp.numWarehouseItmsID                                            
                               left join  Warehouses  W                                            
                                  on   W.numWarehouseID= WItems.numWarehouseID                                         
                                  left join OpportunityMaster M                                                            
                                  on Opp.numSourceID=M.numOppID  
                                  LEFT JOIN  UOM u ON u.numUOMId = opp.numUOMId  
                                  where om.numDomainId ='
                            + CONVERT(VARCHAR(15), @numDomainId)
                            + ' and Opp.numOppId='+ CONVERT(VARCHAR(15), @OpportunityId) +  
                            + CASE WHEN @numItemCode > 0 THEN ' AND Opp.numOppItemTCode = ' + CONVERT(VARCHAR(15), @numItemCode)
                                   ELSE ''
                              END
                            +
                            ' order by numoppitemtCode ASC '
                        PRINT @strSQL
                        EXEC ( @strSQL
                            )
                        SELECT  vcSerialNo,
                                vcComments AS Comments,
                                numOppItemID AS numoppitemtCode,
                                W.numWarehouseItmsDTLID,
                                numWarehouseItmsID AS numWItmsID,
                                dbo.Fn_getattributes(W.numWarehouseItmsDTLID,
                                                     1) Attributes
                        FROM    WareHouseItmsDTL W
                                JOIN OppWarehouseSerializedItem O ON O.numWarehouseItmsDTLID = W.numWareHouseItmsDTLID
                        WHERE   numOppID = @OpportunityId
*/
      /*              END
                ELSE 
                    BEGIN
                        SELECT  @numDomainID = numDomainId
                        FROM    OpportunityMaster
                        WHERE   numOppID = @OpportunityId
                        SET @strSQL = ''
                        SELECT TOP 1
                                @fld_ID = Fld_ID,
                                @fld_Name = Fld_label 
                        FROM    CFW_Fld_Master
                        WHERE   grp_id = 5
                                AND numDomainID = @numDomainID
                        WHILE @fld_ID > 0
                            BEGIN
                                SET @strSQL =@strSQL + 'dbo.GetCustFldValueItem('
                                    + CONVERT(VARCHAR(15), @fld_ID)
                                    + ',Opp.numItemCode) as ''' + @fld_Name + ''''
                                SELECT TOP 1
                                        @fld_ID = Fld_ID,
                                        @fld_Name = Fld_label
                                FROM    CFW_Fld_Master
                                WHERE   grp_id = 5
                                        AND numDomainID = @numDomainID
                                        AND Fld_ID > @fld_ID
                                IF @@ROWCOUNT = 0 
                                    SET @fld_ID = 0
                                IF @fld_ID <> 0 
                                    SET @strSQL = @strSQL + ','
                            END
                        SET @strSQL = 'select  Opp.numoppitemtCode,Opp.numOppId,                                       
                                Opp.numItemCode,                                    
                               Opp.vcItemName,                                                                    
                               ISNULL(Opp.vcItemDesc,'''') as [Desc],                                                                    
                               Opp.vcType ItemType,                                   
                               Opp.vcPathForTImage,
                               Opp.vcModelID,
                              CAST((dbo.fn_UOMConversion(ISNULL(I.numBaseUnit,0),Opp.numItemCode,om.numDomainId,ISNULL(opp.numUOMId,0)) * Opp.numUnitHour) as numeric(18,2)) as numUnitHour,   
							   Opp.monPrice,                                                                    
                               Opp.monTotAmount,                                                     
							   ISNULL(Opp.vcItemDesc,'''') vcItemDesc,
							   Opp.vcManufacturer,
							   I.charItemType,
							   I.bitKitParent,
							   I.bitAssembly,
							   I.vcSKU,
							   I.numBarCodeId,
							   dbo.[fn_GetListItemName](I.[numItemClassification]) vcClassification,
							   (SELECT [vcItemGroup] FROM [ItemGroups] WHERE [numItemGroupID] =I.numItemGroup) vcItemGroup,
							   ISNULL(I.vcUnitofMeasure,'''') vcUnitofMeasure,
							   I.fltWeight,I.fltHeight,I.fltWidth,I.fltLength,
							   WItems.numWarehouseID,
                               Opp.numWarehouseItmsID,isnull(Opp.numToWarehouseItemID,0) numToWarehouseItemID,bitDropShip DropShip, CASE ISNULL(bitDropShip,0) when 1 then ''Yes'' else '''' end as vcDropShip,
                               0 as Op_Flag,isnull(Opp.bitDiscountType,0) bitDiscountType,isnull(Opp.fltDiscount,0) fltDiscount,isnull(monTotAmtBefDiscount,0) monTotAmtBefDiscount,                                           
                               vcWarehouse as Warehouse,I.bitSerialized,isnull(I.bitLotNo,0) as bitLotNo,vcAttributes,
                               dbo.fn_GetReturnItemQty(Opp.numoppitemtCode) as ReturrnedQty,
                               [dbo].fn_GetReturnStatus(Opp.numoppitemtCode) as ReturnStatus,
							   isnull((SELECT numReturnID FROM Returns WHERE numOppItemCode=Opp.numoppitemtCode and numOppId=Opp.numOppID),0) as numReturnID,
                               isnull(v.monCost,0) as VendorCost,
                               bitTaxable,
                               case when bitTaxable =0 then 0 when bitTaxable=1 then '
                            + CONVERT(VARCHAR(15), @TaxPercentage)
                            + ' end as Tax,                                                
                                   isnull(m.vcPOppName,''Internal'') as Source,dbo.fn_GetAttributes(Opp.numWarehouseItmsID,I.bitSerialized) as Attributes ,numSourceID '
                            + CASE WHEN @strSQL = '' THEN ''
                                   ELSE ',' + @strSQL
                              END
                            + ',isnull(opp.numUOMId,0) AS numUOM,ISNULL(u.vcUnitName,'''') vcUOMName,dbo.fn_UOMConversion(opp.numUOMId,Opp.numItemCode,om.numDomainId,null) AS UOMConversionFactor
                          ,case when Opp.bitDiscountType=0 then  cast(isnull(Opp.monTotAmtBefDiscount,Opp.monTotAmount)-Opp.monTotAmount AS VARCHAR(10)) +  '' ('' + CAST(Opp.fltDiscount AS VARCHAR(10)) +  ''%)'' 
                          ELSE cast(isnull(Opp.monTotAmtBefDiscount,Opp.monTotAmount)-Opp.monTotAmount AS VARCHAR(10)) end as DiscAmt
                         ,isnull(I.numVendorID,0) numVendorID,OM.tintOppType,cast(Opp.numUnitHour as numeric(9,0)) numQty,SUBSTRING(
(SELECT '','' + vcSerialNo + CASE WHEN isnull(I.bitLotNo,0)=1 THEN '' ('' + CONVERT(VARCHAR(15),whi.numQty) + '')'' ELSE '''' end 
FROM OppWarehouseSerializedItem oppI JOIN WareHouseItmsDTL whi ON oppI.numWareHouseItmsDTLID=whi.numWareHouseItmsDTLID where oppI.numOppID=Opp.numOppId and oppI.numOppItemID=Opp.numoppitemtCode 
ORDER BY vcSerialNo FOR XML PATH('''')),2,200000) AS SerialLotNo, ISNULL(Opp.bitWorkOrder,0) as bitWorkOrder,
CASE WHEN ISNULL(Opp.bitWorkOrder,0)=1 then (select dbo.GetListIemName(numWOStatus) + CASE WHEN isnull(numWOStatus,0)=23184 THEN
'' - '' + CAST(ISNULL(dbo.fn_GetContactName(numAssignedTo), '''')+'',''+convert(varchar(20),dateadd(minute,'+ CONVERT(VARCHAR(15),-@ClientTimeZoneOffset) +',bintCompletedDate)) AS VARCHAR(100)) ELSE '''' end
 from WorkOrder where isnull(numOppId,0)=Opp.numOppId and numItemCode=Opp.numItemCode) else '''' end as vcWorkOrderStatus,
 ISNULL(Opp.numVendorWareHouse,0) as numVendorWareHouse,ISNULL(Opp.numShipmentMethod,0) as numShipmentMethod,ISNULL(Opp.numSOVendorId,0) as numSOVendorId,Opp.numUnitHour as numOriginalUnitHour,
 isnull(Opp.numProjectID,0) numProjectID,isnull(Opp.numClassID,0) numClassID,isnull(Opp.numProjectStageID,0) numProjectStageID,Case When OPP.numProjectStageID>0 then dbo.fn_GetProjectStageName(OPP.numProjectId,OPP.numProjectStageID) when OPP.numProjectStageID=-1 then ''T&E'' When OPP.numProjectId>0 then case when OM.tintOppType=1 then ''S.O.'' else ''P.O.'' end else ''-'' end as vcProjectStageName,
 CASE WHEN (SELECT TOP 1 ISNULL(OBI.numOppBizDocItemID,0) FROM dbo.OpportunityBizDocs OBD INNER JOIN dbo.OpportunityBizDocItems OBI ON OBD.numOppBizDocsID = OBI.numOppBizDocID WHERE OBI.numOppItemID = Opp.numoppitemtCode AND ISNULL(OBD.bitAuthoritativeBizDocs,0)=1 ) > 0
 THEN 1 ELSE 0 END AS bitItemAddedToAuthBizDoc,isnull(I.numItemClass,0) as numItemClass
              from OpportunityItems Opp                    
                               left join OpportunityMaster OM                                                            
                                  on Opp.numOppID=oM.numOppID                                    
                                 join item I                                                                    
                                  on Opp.numItemCode=i.numItemcode   
                                  left join Vendor v 
									on v.numVendorID= I.numVendorID and I.numItemCode =v.numItemCode
                                left join  WareHouseItems  WItems                                        
                                on   WItems.numWareHouseItemID= Opp.numWarehouseItmsID                                            
                                left join  Warehouses  W                                            
                                  on   W.numWarehouseID= WItems.numWarehouseID                                         
                                  left join OpportunityMaster M                                                            
                                  on Opp.numSourceID=M.numOppID                      
                                        LEFT JOIN  UOM u ON u.numUOMId = opp.numUOMId
                                  where om.numDomainId ='
                            + CONVERT(VARCHAR(15), @numDomainId)
                            + ' and Opp.numOppId='+ CONVERT(VARCHAR(15), @OpportunityId) +  
                            + CASE WHEN @numItemCode > 0 THEN ' AND Opp.numOppItemTCode = ' + CONVERT(VARCHAR(15), @numItemCode)
                                   ELSE ''
                              END
                            +
                            ' order by numoppitemtCode ASC '
                        PRINT @strSQL
                        EXEC ( @strSQL
                            )
                        SELECT  vcSerialNo,
                                vcComments AS Comments,
                                numOppItemID AS numoppitemtCode,
                                W.numWarehouseItmsDTLID,
                                numWarehouseItmsID AS numWItmsID,
                                dbo.Fn_getattributes(W.numWarehouseItmsDTLID,
                                                     1) Attributes
                        FROM    OppWarehouseSerializedItem O
                                LEFT JOIN WareHouseItmsDTL W ON O.numWarehouseItmsDTLID = W.numWareHouseItmsDTLID
                        WHERE   numOppID = @OpportunityId
                    END
            */
            END
            
    END
/****** Object:  StoredProcedure [dbo].[USP_OppManage]    Script Date: 07/26/2008 16:20:19 ******/
--2 changes
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_OppManage')
DROP PROCEDURE usp_oppmanage
GO
CREATE PROCEDURE [dbo].[USP_OppManage] 
(                                                                          
  @numOppID as numeric=0 OUTPUT,                                                                          
  @numContactId numeric(9)=null,                                                                          
  @numDivisionId numeric(9)=null,                                                                          
  @tintSource numeric(9)=null,                                                                          
  @vcPOppName Varchar(100)='' OUTPUT,                                                                          
  @Comments varchar(1000)='',                                                                          
  @bitPublicFlag bit=0,                                                                          
  @numUserCntID numeric(9)=0,                                                                                 
  @monPAmount money =0,                                                 
  @numAssignedTo as numeric(9)=0,                                                                                                        
  @numDomainId numeric(9)=0,                                                                                                                                                                                   
  @strItems as text=null,                                                                          
  @strMilestone as text=null,                                                                          
  @dtEstimatedCloseDate datetime,                                                                          
  @CampaignID as numeric(9)=null,                                                                          
  @lngPConclAnalysis as numeric(9)=null,                                                                         
  @tintOppType as tinyint,                                                                                                                                             
  @tintActive as tinyint,                                                              
  @numSalesOrPurType as numeric(9),              
  @numRecurringId as numeric(9),
  @numCurrencyID as numeric(9)=0,
  @DealStatus as tinyint =0,-- 0=Open,1=Won,2=Lost
  @numStatus AS NUMERIC(9),
  @vcOppRefOrderNo VARCHAR(100),
  @numBillAddressId numeric(9)=0,
  @numShipAddressId numeric(9)=0,
  @bitStockTransfer BIT=0,
  @WebApiId INT = 0,
  @tintSourceType TINYINT=0,
  @bitDiscountType as bit,
  @fltDiscount  as float,
  @bitBillingTerms as bit,
  @intBillingDays as integer,
  @bitInterestType as bit,
  @fltInterest as float,
  @tintTaxOperator AS TINYINT,
  @numDiscountAcntType AS NUMERIC(9),
  @vcWebApiOrderNo VARCHAR(100)=NULL,
  @vcCouponCode		VARCHAR(100) = '',
  @vcMarketplaceOrderID VARCHAR(100)=NULL,
  @vcMarketplaceOrderDate  datetime=NULL,
  @vcMarketplaceOrderReportId VARCHAR(100)=NULL,
  @numPercentageComplete NUMERIC(9),
  @bitUseShippersAccountNo BIT = 0,
  @bitUseMarkupShippingRate BIT = 0,
  @numMarkupShippingRate NUMERIC(19,2) = 0,
  @intUsedShippingCompany INT = 0
)                                                                          
                                                                          
as  
BEGIN TRY
BEGIN TRANSACTION
 DECLARE @fltExchangeRate float                                 
 DECLARE @hDocItem int                                                                                                                            
 DECLARE @TotalAmount as money                                                                          
 DECLARE @tintOppStatus as tinyint               
 DECLARE @tintDefaultClassType NUMERIC
 DECLARE @numDefaultClassID NUMERIC;SET @numDefaultClassID=0
 
 --Get Default Item Class for particular user based on Domain settings  
 SELECT @tintDefaultClassType=isnull(tintDefaultClassType,0) FROM dbo.Domain  WHERE numDomainID = @numDomainID 

 IF @tintDefaultClassType=0
      SELECT @numDefaultClassID=NULLIF(numDefaultClass,0) FROM dbo.UserMaster  WHERE numUserDetailId =@numUserCntID AND numDomainID = @numDomainID 


--If new Oppertunity
if @numOppID = 0                                                                          
 begin     
  declare @intOppTcode as numeric(9)                           
  Select @intOppTcode=max(numOppId)from OpportunityMaster                
  set @intOppTcode =isnull(@intOppTcode,0) + 1                                                
  set @vcPOppName=@vcPOppName+ '-'+convert(varchar(4),year(getutcdate()))  +'-A'+ convert(varchar(10),@intOppTcode)                                  
  
  IF ISNULL(@numCurrencyID,0) = 0 
	BEGIN
	 SET @fltExchangeRate=1
	 SELECT @numCurrencyID= ISNULL(numCurrencyID,0) FROM dbo.Domain WHERE numDomainId=@numDomainId	
	END
  else set @fltExchangeRate=dbo.GetExchangeRate(@numDomainID,@numCurrencyID)
                                                                       
  --Set Default Class If enable User Level Class Accountng 
  DECLARE @numAccountClass AS NUMERIC(18);SET @numAccountClass=0
  SELECT @numAccountClass=ISNULL(numDefaultClass,0) FROM dbo.UserMaster UM JOIN dbo.Domain D ON UM.numDomainID=D.numDomainId
  WHERE D.numDomainId=@numDomainId AND UM.numUserDetailId=@numUserCntID AND ISNULL(D.IsEnableUserLevelClassTracking,0)=1
                                                                       
  insert into OpportunityMaster                                                                          
  (                                                                             
  numContactId,                                                                   
  numDivisionId,                                                                          
  txtComments,                                             
  numCampainID,                                                                          
  bitPublicFlag,                                                                          
  tintSource,
  tintSourceType,                                                             
  vcPOppName,                                                                          
  intPEstimatedCloseDate,                                                                          
  monPAmount,                                                                           
  numCreatedBy,                                                                          
  bintCreatedDate,                                                                           
  numModifiedBy,                                                                          
  bintModifiedDate,                                                                          
  numDomainId,                                                                                                             
  numRecOwner,                                                                          
  lngPConclAnalysis,                                                                         
  tintOppType,                                                              
  numSalesOrPurType,
  numCurrencyID,
  fltExchangeRate,
  numAssignedTo,
  numAssignedBy,
  [tintOppStatus],
  numStatus,
  vcOppRefOrderNo,
  --vcWebApiOrderNo,
  bitStockTransfer,bitBillingTerms,intBillingDays,bitInterestType,fltInterest,vcCouponCode,
  bitDiscountType,fltDiscount,bitPPVariance,vcMarketplaceOrderID,vcMarketplaceOrderReportId,
  numPercentageComplete,numAccountClass,tintTaxOperator,bitUseShippersAccountNo,
  bitUseMarkupShippingRate,
  numMarkupShippingRate,
  intUsedShippingCompany
  )                                                                          
  Values                                                                          
  (                                                                          
  @numContactId,                                                                          
  @numDivisionId,                                                                          
  @Comments,                           
  @CampaignID,                                                                          
  @bitPublicFlag,                     
  @tintSource,
  @tintSourceType,                         
  @vcPOppName,                                                                          
  @dtEstimatedCloseDate,                                                                          
  @monPAmount,                                                                                
  @numUserCntID,                                                                          
 CASE @tintSourceType WHEN 3 THEN @vcMarketplaceOrderDate ELSE GETUTCDATE() END,                                                                          
  @numUserCntID,                                                           
  getutcdate(),                                                                          
  @numDomainId,                                                                                   
  @numUserCntID,                                                                          
  @lngPConclAnalysis,                                                                                                                                                                      
  @tintOppType,                                                                                                                                      
  @numSalesOrPurType,              
  @numCurrencyID, 
  @fltExchangeRate,
  case when @numAssignedTo>0 then @numAssignedTo else null end,
  case when @numAssignedTo>0 then @numUserCntID else null   END,
  @DealStatus,
  @numStatus,
  @vcOppRefOrderNo,
 -- @vcWebApiOrderNo,
  @bitStockTransfer,@bitBillingTerms,@intBillingDays,@bitInterestType,@fltInterest,@vcCouponCode,
  @bitDiscountType,@fltDiscount,1,@vcMarketplaceOrderID,@vcMarketplaceOrderReportId,
  @numPercentageComplete,@numAccountClass,@tintTaxOperator,@bitUseShippersAccountNo,
  @bitUseMarkupShippingRate,
  @numMarkupShippingRate,
  @intUsedShippingCompany
    )                                                                                                                      
  set @numOppID=scope_identity()                                                
  
  --Update OppName as per Name Template
  EXEC dbo.USP_UpdateNameTemplateValue @tintOppType,@numDomainID,@numOppID

  --Map Custom Field	
  DECLARE @tintPageID AS TINYINT;
  SET @tintPageID=CASE WHEN @tintOppType=1 THEN 2 ELSE 6 END 
  	
  EXEC dbo.USP_AddParentChildCustomFieldMap
	@numDomainID = @numDomainID, --  numeric(18, 0)
	@numRecordID = @numOppID, --  numeric(18, 0)
	@numParentRecId = @numDivisionId, --  numeric(18, 0)
	@tintPageID = @tintPageID --  tinyint
 	
  
	IF ISNULL(@numStatus,0) > 0 AND @tintOppType=1 AND isnull(@DealStatus,0)=1
	   BEGIN
	   		EXEC USP_ManageOpportunityAutomationQueue
					@numOppQueueID = 0, -- numeric(18, 0)
					@numDomainID = @numDomainID, -- numeric(18, 0)
					@numOppId = @numOppID, -- numeric(18, 0)
					@numOppBizDocsId = 0, -- numeric(18, 0)
					@numOrderStatus = @numStatus, -- numeric(18, 0)
					@numUserCntID = @numUserCntID, -- numeric(18, 0)
					@tintProcessStatus = 1, -- tinyint
					@tintMode = 1 -- TINYINT
	   END
  ---- inserting Items                                                                          
   
                                                   
  if convert(varchar(10),@strItems) <>'' AND @numOppID>0
  begin                      
   EXEC sp_xml_preparedocument @hDocItem OUTPUT, @strItems       
	
  insert into                       
   OpportunityItems                                                                          
   (numOppId,numItemCode,numUnitHour,monPrice,monTotAmount,vcItemDesc,numWarehouseItmsID,vcType,bitDropShip,bitDiscountType,fltDiscount,monTotAmtBefDiscount,vcItemName,vcModelID,vcManufacturer,vcPathForTImage,monVendorCost,numUOMId,bitWorkOrder,
   numVendorWareHouse,numShipmentMethod,numSOVendorId,numProjectID,numProjectStageID,numClassID,numToWarehouseItemID,vcAttributes,monAvgCost,bitItemPriceApprovalRequired)
   select @numOppID,X.numItemCode,dbo.fn_UOMConversion(X.numUOM,numItemCode,@numDomainId,null) * x.numUnitHour,
   x.monPrice,x.monTotAmount,X.vcItemDesc,
   CASE X.numWarehouseItmsID WHEN -1 
							 THEN (SELECT [numWareHouseItemID] FROM [WareHouseItems] 
								   WHERE [numItemID] = X.numItemCode 
								   AND ISNULL(WareHouseItems.vcWHSKU,'') = ISNULL(X.vcSKU,'')
								   AND numWareHouseID IN (SELECT TOP 1 ISNULL(numWareHouseId,0) AS numDefaultWareHouseID FROM dbo.WebAPIDetail 
														  WHERE numDomainID =@numDomainId 
														  AND WebApiId = @WebApiId))  
							 WHEN 0 
							 THEN (SELECT TOP 1 [numWareHouseItemID] FROM [WareHouseItems] 
								   WHERE [numItemID] = X.numItemCode
								   AND ISNULL(WareHouseItems.vcWHSKU,'') = ISNULL(X.vcSKU,'')) 
							 ELSE  X.numWarehouseItmsID 
	END AS numWarehouseItmsID,
	X.ItemType,X.DropShip,X.bitDiscountType,
   X.fltDiscount,X.monTotAmtBefDiscount,X.vcItemName,
   (SELECT vcModelID FROM item WHERE numItemCode = X.numItemCode),
   (SELECT vcManufacturer FROM item WHERE numItemCode = X.numItemCode),
   (SELECT TOP 1 vcPathForTImage FROM ItemImages WHERE numItemCode = X.numItemCode AND BitDefault = 1 AND numdomainId = @numdomainId),
   (select isnull(VN.monCost,0) from Vendor VN, Item IT where IT.numItemCode=VN.numItemCode and  VN.numItemCode=X.numItemCode and  VN.numVendorID=IT.numVendorID),
   X.numUOM,X.bitWorkOrder,X.numVendorWareHouse,X.numShipmentMethod,X.numSOVendorId,X.numProjectID,X.numProjectStageID,Case When @tintDefaultClassType=0 then @numDefaultClassID else (SELECT numItemClass FROM item WHERE numItemCode = X.numItemCode) end,X.numToWarehouseItemID,X.Attributes,
   (SELECT ISNULL(monAverageCost,0) FROM Item I WHERE I.numDomainId=@numDomainId AND I.numItemCode=X.numItemCode),X.bitItemPriceApprovalRequired from(
   SELECT *FROM OPENXML (@hDocItem,'/NewDataSet/Item[Op_Flag=1]',2)                                                                          
   WITH                       
   (                                                                          
   numoppitemtCode numeric(9),                                     
   numItemCode numeric(9),                                                                          
   numUnitHour numeric(9,2),                                                                          
   monPrice money,                                                                       
   monTotAmount money,                                                                          
   vcItemDesc varchar(1000),                                    
   numWarehouseItmsID numeric(9),                          
   ItemType varchar(30),      
   DropShip bit,
   bitDiscountType bit,
   fltDiscount float,
   monTotAmtBefDiscount MONEY,
   vcItemName varchar(300),numUOM NUMERIC(9),bitWorkOrder BIT,numVendorWareHouse NUMERIC(9),numShipmentMethod NUMERIC(9) ,numSOVendorId numeric(9),numProjectID numeric(9),numProjectStageID numeric(9),numToWarehouseItemID NUMERIC(9),Attributes varchar(200),vcSKU VARCHAR(100),bitItemPriceApprovalRequired BIT
   ))X    
   ORDER BY 
   X.numoppitemtCode
    
    
   Update OpportunityItems                       
   set numItemCode=X.numItemCode,    
   numOppId=@numOppID,                     
   numUnitHour=dbo.fn_UOMConversion(X.numUOM,X.numItemCode,@numDomainId,null) * X.numUnitHour,                      
   monPrice=x.monPrice,                      
   monTotAmount=x.monTotAmount,                                  
   vcItemDesc=X.vcItemDesc,                      
   numWarehouseItmsID=X.numWarehouseItmsID,                 
   bitDropShip=X.DropShip,
   bitDiscountType=X.bitDiscountType,
   fltDiscount=X.fltDiscount,
   monTotAmtBefDiscount=X.monTotAmtBefDiscount,vcItemName=X.vcItemName,numUOMId=numUOM,bitWorkOrder=X.bitWorkOrder,
   numVendorWareHouse=X.numVendorWareHouse,numShipmentMethod=X.numShipmentMethod,numSOVendorId=X.numSOVendorId,numProjectId=X.numProjectID,numProjectStageId=X.numProjectStageID,numToWarehouseItemID = X.numToWarehouseItemID,
vcAttributes=X.Attributes,numClassID=(Case When @tintDefaultClassType=0 then @numDefaultClassID else (SELECT numItemClass FROM item WHERE numItemCode = X.numItemCode) END),bitItemPriceApprovalRequired = X.bitItemPriceApprovalRequired
--   ,vcModelID=(SELECT vcModelID FROM item WHERE numItemCode = X.numItemCode),vcManufacturer=(SELECT vcManufacturer FROM item WHERE numItemCode = X.numItemCode)
--  ,vcPathForTImage=(SELECT vcPathForTImage FROM item WHERE numItemCode = X.numItemCode),monVendorCost=(select isnull(VN.monCost,0) from Vendor VN, Item IT where IT.numItemCode=VN.numItemCode and  VN.numItemCode=X.numItemCode and  VN.numVendorID=IT.numVendorID)
   from (SELECT numoppitemtCode as numOppItemID,numItemCode,numUnitHour,monPrice,monTotAmount,vcItemDesc,numWarehouseItmsID,DropShip,bitDiscountType,fltDiscount,monTotAmtBefDiscount,vcItemName,numUOM,bitWorkOrder,
   numVendorWareHouse,numShipmentMethod,numSOVendorId,numProjectID,numProjectStageID,numToWarehouseItemID,Attributes,bitItemPriceApprovalRequired
   FROM OPENXML (@hDocItem,'/NewDataSet/Item[Op_Flag=2]',2)
   WITH  (                      
    numoppitemtCode numeric(9),                                     
    numItemCode numeric(9),                                                                          
    numUnitHour numeric(9,2),                                                                          
    monPrice money,                                         
    monTotAmount money,                                                                          
    vcItemDesc varchar(1000),                                    
    numWarehouseItmsID numeric(9),      
    DropShip bit,
	bitDiscountType bit,
    fltDiscount float,
    monTotAmtBefDiscount MONEY,vcItemName varchar(300),numUOM NUMERIC(9),bitWorkOrder BIT,numVendorWareHouse NUMERIC(9),numShipmentMethod NUMERIC(9),numSOVendorId numeric(9),numProjectID numeric(9),numProjectStageID numeric(9),numToWarehouseItemID NUMERIC(9),Attributes varchar(200),bitItemPriceApprovalRequired BIT
   ))X where numoppitemtCode=X.numOppItemID                                          
	
	
   -- Update UOM of opportunity items have not UOM set while exported from Marketplace (means tintSourceType = 3)
	IF ISNULL(@tintSourceType,0) = 3
	BEGIN
		--SELECT * FROM OpportunityMaster WHERE numOppId = 81654
		--SELECT * FROM OpportunityItems WHERE numOppId = 81654
		--SELECT * FROM Item WHERE numItemCode = 822078

		UPDATE OI SET numUOMID = ISNULL(I.numSaleUnit,0), 
					  numUnitHour = dbo.fn_UOMConversion(ISNULL(I.numSaleUnit,0),OI.numItemCode,@numDomainId,null) * OI.numUnitHour
		FROM OpportunityItems OI
		JOIN Item I ON OI.numItemCode = I.numItemCode
		WHERE numOppId = @numOppID
		AND I.numDomainID = @numDomainId

	END	
   --Update OpportunityItems
   --set 
   --FROM (SELECT numItemCode,vcItemName,[txtItemDesc],vcModelID FROM item WHERE numItemCode IN (SELECT [numItemCode] FROM OpportunityItems WHERE numOppID = @numOppID))X
   --WHERE OpportunityItems.[numItemCode] = X.numItemCode 
                                     
   insert into OppWarehouseSerializedItem                                                                          
   (numOppID,numWarehouseItmsDTLID,numOppItemID,numWarehouseItmsID)                      
   select @numOppID,X.numWarehouseItmsDTLID,X.numoppitemtCode,X.numWItmsID from(                                                                          
   SELECT * FROM OPENXML (@hDocItem,'/NewDataSet/SerialNo',2)                                                                          
   WITH                        
   (                                               
   numWarehouseItmsDTLID numeric(9),                      
   numoppitemtCode numeric(9),                      
   numWItmsID numeric(9)                                                     
   ))X                                    
               
                                 
   update OppWarehouseSerializedItem                       
   set numOppItemID=X.numoppitemtCode                                
   from                       
   (SELECT numWarehouseItmsDTLID as DTLID,O.numoppitemtCode FROM OPENXML (@hDocItem,'/NewDataSet/SerialNo',2)                                                                         
   WITH(numWarehouseItmsDTLID numeric(9),numoppitemtCode numeric(9),numWItmsID numeric(9))                                
   join OpportunityItems O on O.numWarehouseItmsID=numWItmsID and O.numOppID=@numOppID)X                 
   where numOppID=@numOppID and numWarehouseItmsDTLID=X.DTLID      
    
--   insert into OpportunityKitItems(numOppKitItem,numChildItem,intQuantity,numWareHouseItemId)    
--   SELECT * FROM OPENXML (@hDocItem,'/NewDataSet/ChildItems',2)                                                                          
--   WITH                        
--   (                                               
--   numoppitemtCode numeric(9),                      
--   numItemCode numeric(9),    
--   numQtyItemsReq integer,  
--   numWarehouseItmsID numeric(9)                                                       
--   )    
      
    
    
    
	EXEC USP_ManageSOWorkOrder @numDomainId,@numOppID,@strItems,@numUserCntID
    
                              
                          
   EXEC sp_xml_removedocument @hDocItem                                     
        end          
   set @TotalAmount=0                                                           
   select @TotalAmount=sum(monTotAmount)from OpportunityItems where numOppId=@numOppID                                                                          
  update OpportunityMaster set  monPamount=@TotalAmount                                                          
  where numOppId=@numOppID                                                 
                      
     
--Insert Tax for Division   

IF @tintOppType=1 OR (@tintOppType=2 and (select isnull(bitPurchaseTaxCredit,0) from Domain where numDomainId=@numDomainId)=1)
BEGIN                   
	INSERT dbo.OpportunityMasterTaxItems (
		numOppId,
		numTaxItemID,
		fltPercentage
	) SELECT @numOppID,TI.numTaxItemID,dbo.fn_CalItemTaxAmt(@numDomainID,@numDivisionID,0,TI.numTaxItemID,@numOppId,0,NULL) FROM TaxItems TI JOIN DivisionTaxTypes DTT ON TI.numTaxItemID = DTT.numTaxItemID
	 WHERE DTT.numDivisionID=@numDivisionId AND DTT.bitApplicable=1
	   union 
	  select @numOppID,0,dbo.fn_CalItemTaxAmt(@numDomainID,@numDivisionID,0,0,@numOppId,1,NULL) 
	  FROM dbo.DivisionMaster WHERE bitNoTax=0 AND numDivisionID=@numDivisionID
END
-- Archieve item based on Item's individual setting
--	UPDATE Item SET IsArchieve = CASE WHEN EXISTS(SELECT * FROM dbo.OpportunityItems WHERE OpportunityItems.numItemCode = I.numItemCode) AND ISNULL(I.bitArchiveItem,0) = 1 
--									  THEN 1 
--									  ELSE 0 
--									  END 
--	FROM Item I JOIN dbo.OpportunityItems OI ON I.numItemCode = OI.numItemCode
	
 END
 
 else                                                                                                                          
 BEGIN                  
	--Declaration
	 DECLARE @tempAssignedTo AS NUMERIC(9)                                                    
	 SET @tempAssignedTo = NULL 
	 
	 SELECT @tintOppStatus = tintOppStatus,@tempAssignedTo=isnull(numAssignedTo,0) FROM   OpportunityMaster WHERE  numOppID = @numOppID
 
	--Reverting back the warehouse items                  
	 IF @tintOppStatus = 1 
		BEGIN        
		PRINT 'inside revert'          
			EXEC USP_RevertDetailsOpp @numOppID,0,@numUserCntID                  
		END                  
	-- Update Master table
	
	   IF LEN(ISNULL(@vcOppRefOrderNo,''))>0
	   BEGIN
	   		UPDATE dbo.OpportunityBizDocs SET vcRefOrderNo=@vcOppRefOrderNo WHERE numOppId=@numOppID
	   END
		
	   IF EXISTS (SELECT 1 FROM dbo.OpportunityMaster WHERE numOppId = @numOppID AND tintOppType=1 AND isnull(tintOppStatus,0)=1 AND ISNULL(numStatus,0)!= @numStatus)
	   BEGIN
	   		EXEC USP_ManageOpportunityAutomationQueue
					@numOppQueueID = 0, -- numeric(18, 0)
					@numDomainID = @numDomainID, -- numeric(18, 0)
					@numOppId = @numOppID, -- numeric(18, 0)
					@numOppBizDocsId = 0, -- numeric(18, 0)
					@numOrderStatus = @numStatus, -- numeric(18, 0)
					@numUserCntID = @numUserCntID, -- numeric(18, 0)
					@tintProcessStatus = 1, -- tinyint
					@tintMode = 1 -- TINYINT
	   END 
						
	   UPDATE   OpportunityMaster
	   SET      vcPOppName = @vcPOppName,
				txtComments = @Comments,
				bitPublicFlag = @bitPublicFlag,
				numCampainID = @CampaignID,
				tintSource = @tintSource,
				tintSourceType=@tintSourceType,
				intPEstimatedCloseDate = @dtEstimatedCloseDate,
				numModifiedBy = @numUserCntID,
				bintModifiedDate = GETUTCDATE(),
				lngPConclAnalysis = @lngPConclAnalysis,
				monPAmount = @monPAmount,
				tintActive = @tintActive,
				numSalesOrPurType = @numSalesOrPurType,
				numStatus = @numStatus,
				tintOppStatus = @DealStatus,
				vcOppRefOrderNo=@vcOppRefOrderNo,
				--vcWebApiOrderNo = @vcWebApiOrderNo,
				bintOppToOrder=(Case when @tintOppStatus=0 and @DealStatus=1 then GETUTCDATE() else bintOppToOrder end),
				bitDiscountType=@bitDiscountType,fltDiscount=@fltDiscount,bitBillingTerms=@bitBillingTerms,
				intBillingDays=@intBillingDays,bitInterestType=@bitInterestType,fltInterest=@fltInterest,
				tintTaxOperator=@tintTaxOperator,numDiscountAcntType=@numDiscountAcntType,vcCouponCode = @vcCouponCode,
				numPercentageComplete =@numPercentageComplete,bitUseShippersAccountNo = @bitUseShippersAccountNo,
				bitUseMarkupShippingRate = @bitUseMarkupShippingRate,
				numMarkupShippingRate = @numMarkupShippingRate,
				intUsedShippingCompany = @intUsedShippingCompany
	   WHERE    numOppId = @numOppID   
	   
	---Updating if organization is assigned to someone                                                      
	   IF ( @tempAssignedTo <> @numAssignedTo AND @numAssignedTo <> '0' ) 
		BEGIN                                   
			UPDATE  OpportunityMaster SET     numAssignedTo = @numAssignedTo, numAssignedBy = @numUserCntID WHERE   numOppId = @numOppID                                                    
		END                                                     
	   ELSE 
	   IF ( @numAssignedTo = 0 ) 
		BEGIN                
			UPDATE  OpportunityMaster SET     numAssignedTo = 0, numAssignedBy = 0 WHERE   numOppId = @numOppID
		END                                                                      
		---- Updating Opp Items
		if convert(varchar(10),@strItems) <>'' AND @numOppID>0                                                                         
		begin
			   EXEC sp_xml_preparedocument @hDocItem OUTPUT, @strItems                                      
			   --Delete Items
			   delete from OpportunityKitItems where numOppID=@numOppID and numOppItemID not in                       
			   (SELECT numoppitemtCode FROM OPENXML (@hDocItem,'/NewDataSet/Item',2)                                                                          
			   WITH  (numoppitemtCode numeric(9)))                      
                                        
--               ---- ADDED BY Manish Anjara : Jun 26,2013 - Archive item based on Item's individual setting
--               UPDATE Item SET IsArchieve = CASE WHEN EXISTS(SELECT * FROM dbo.OpportunityItems WHERE OpportunityItems.numItemCode = I.numItemCode) AND ISNULL(I.bitArchiveItem,0) = 1 
--												 THEN 1 
--												 ELSE 0 
--												 END 
--			   FROM Item I JOIN dbo.OpportunityItems OI ON I.numItemCode = OI.numItemCode
--					 	
--			   UPDATE Item SET IsArchieve = CASE WHEN ISNULL(I.bitArchiveItem,0) = 1 THEN 0 ELSE 1 END
--			   FROM Item I
--			   JOIN OpportunityItems OI ON I.numItemCode = OI.numItemCode
--			   WHERE numOppID = @numOppID and OI.numItemCode NOT IN (SELECT numItemCode FROM OPENXML (@hDocItem,'/NewDataSet/Item',2) 
--																	 WITH (numItemCode numeric(9)))

			   delete from OpportunityItems where numOppID=@numOppID and numoppitemtCode not in                       
			   (SELECT numoppitemtCode FROM OPENXML (@hDocItem,'/NewDataSet/Item',2)                                                                          
			   WITH  (numoppitemtCode numeric(9)))   
	                                
			   insert into OpportunityItems                                                                          
			   (numOppId,numItemCode,numUnitHour,monPrice,monTotAmount,vcItemDesc,numWarehouseItmsID,vcType,bitDropShip,bitDiscountType,fltDiscount,monTotAmtBefDiscount,[vcItemName],[vcModelID],[vcManufacturer],vcPathForTImage,monVendorCost,numUOMId,bitWorkOrder,numVendorWareHouse,numToWarehouseItemID,numShipmentMethod,numSOVendorId,numProjectID,numProjectStageID,vcAttributes,numClassID,monAvgCost,bitItemPriceApprovalRequired)
			   select @numOppID,X.numItemCode,dbo.fn_UOMConversion(X.numUOM,numItemCode,@numDomainId,null) * x.numUnitHour,x.monPrice,x.monTotAmount,X.vcItemDesc,X.numWarehouseItmsID,X.ItemType,X.DropShip,X.bitDiscountType,X.fltDiscount,X.monTotAmtBefDiscount,X.vcItemName,(SELECT vcModelID FROM item WHERE numItemCode = X.numItemCode),(SELECT vcManufacturer FROM item WHERE numItemCode = X.numItemCode),(SELECT TOP 1 vcPathForTImage FROM dbo.ItemImages WHERE numItemCode = X.numItemCode AND BitDefault = 1 AND numDomainId = @numDomainId),(select isnull(VN.monCost,0) from Vendor VN, Item IT where IT.numItemCode=VN.numItemCode and  VN.numItemCode=X.numItemCode and  VN.numVendorID=IT.numVendorID),X.numUOM,X.bitWorkOrder,
			   X.numVendorWareHouse,X.numToWarehouseItemID,X.numShipmentMethod,numSOVendorId,numProjectID,numProjectStageID,Attributes,Case When @tintDefaultClassType=0 then @numDefaultClassID else (SELECT numItemClass FROM item WHERE numItemCode = X.numItemCode) END,
			   (SELECT ISNULL(monAverageCost,0) FROM Item I WHERE I.numDomainId=@numDomainId AND I.numItemCode=X.numItemCode), X.bitItemPriceApprovalRequired from(
			   SELECT *FROM OPENXML (@hDocItem,'/NewDataSet/Item[Op_Flag=1]',2)                                                                          
			   WITH  (                      
				numoppitemtCode numeric(9),                                     
				numItemCode numeric(9),                                                                          
				numUnitHour numeric(9,2),                                                                          
				monPrice money,                                                                       
				monTotAmount money,                                                                          
				vcItemDesc varchar(1000),                                    
				numWarehouseItmsID numeric(9),
				numToWarehouseItemID numeric(9),                   
				Op_Flag tinyint,                            
				ItemType varchar(30),      
				DropShip bit,
				bitDiscountType bit,
				fltDiscount float,
				monTotAmtBefDiscount MONEY,
				vcItemName VARCHAR(300),numUOM NUMERIC(9),bitWorkOrder BIT,numVendorWareHouse NUMERIC(9),numShipmentMethod NUMERIC(9),numSOVendorId numeric(9),numProjectID numeric(9),numProjectStageID numeric(9),Attributes varchar(200), bitItemPriceApprovalRequired BIT
				 ))X                                     
				--Update items                 
			   Update OpportunityItems                       
			   set numItemCode=X.numItemCode,    
			   numOppId=@numOppID,                       
			   numUnitHour=dbo.fn_UOMConversion(X.numUOM,X.numItemCode,@numDomainId,null) * x.numUnitHour,                      
			   monPrice=x.monPrice,                      
			   monTotAmount=x.monTotAmount,                                  
			   vcItemDesc=X.vcItemDesc,                      
			   numWarehouseItmsID=X.numWarehouseItmsID,
			   numToWarehouseItemID = X.numToWarehouseItemID,             
			   bitDropShip=X.DropShip,
			   bitDiscountType=X.bitDiscountType,
			   fltDiscount=X.fltDiscount,
			   monTotAmtBefDiscount=X.monTotAmtBefDiscount,vcItemName=X.vcItemName,numUOMId=numUOM,
			   numVendorWareHouse=X.numVendorWareHouse,numShipmentMethod=X.numShipmentMethod,numSOVendorId=X.numSOVendorId,numProjectId=X.numProjectID,numProjectStageId=X.numProjectStageID,vcAttributes=X.Attributes,bitItemPriceApprovalRequired=X.bitItemPriceApprovalRequired,bitWorkOrder=X.bitWorkOrder
			   from (SELECT numoppitemtCode as numOppItemID,numItemCode,numUnitHour,monPrice,monTotAmount,vcItemDesc,numWarehouseItmsID,numToWarehouseItemID,DropShip,bitDiscountType,fltDiscount,monTotAmtBefDiscount,vcItemName,numUOM,numVendorWareHouse,numShipmentMethod,numSOVendorId,numProjectID,numProjectStageID,Attributes,bitItemPriceApprovalRequired,bitWorkOrder  FROM OPENXML (@hDocItem,'/NewDataSet/Item[Op_Flag=2]',2)                                                                
			   WITH  (                      
				numoppitemtCode numeric(9),                                     
				numItemCode numeric(9),                                                                          
				numUnitHour numeric(9,2),                                                                          
				monPrice money,                                         
				monTotAmount money,                                                                          
				vcItemDesc varchar(1000),                                    
				numWarehouseItmsID numeric(9), 
				numToWarehouseItemID NUMERIC(18,0),     
				DropShip bit,
				bitDiscountType bit,
				fltDiscount float,
				monTotAmtBefDiscount MONEY,vcItemName varchar(300),numUOM NUMERIC(9),numVendorWareHouse NUMERIC(9),numShipmentMethod NUMERIC(9),numSOVendorId numeric(9),numProjectID numeric(9),numProjectStageID numeric(9),Attributes varchar(200),bitItemPriceApprovalRequired BIT, bitWorkOrder BIT                                              
			   ))X where numoppitemtCode=X.numOppItemID                           
			                                    
			   EXEC USP_ManageSOWorkOrder @numDomainId,@numOppID,@strItems,@numUserCntID
			   		                                                                      
			   EXEC sp_xml_removedocument @hDocItem                                               
		end     
	    --Delete previously added serial lot number added for opportunity
		DELETE FROM OppWarehouseSerializedItem WHERE numOppID = @numOppID                                                         
END

----------------generic section will be called in both insert and update ---------------------------------
-- Archieve item based on Item's individual setting
--	UPDATE Item SET IsArchieve = CASE WHEN EXISTS(SELECT * FROM dbo.OpportunityItems WHERE OpportunityItems.numItemCode = I.numItemCode) AND ISNULL(I.bitArchiveItem,0) = 1 
--									  THEN 1 
--									  ELSE 0 
--									  END 
--	FROM Item I JOIN dbo.OpportunityItems OI ON I.numItemCode = OI.numItemCode
	
if convert(varchar(10),@strItems) <>'' AND @numOppID>0                                                                        
  begin 
EXEC sp_xml_preparedocument @hDocItem OUTPUT, @strItems

--delete from OpportunityKitItems where numOppID=@numOppID and numOppItemID 
--	not in (SELECT numoppitemtCode FROM OpportunityItems WHERE numOppID=@numOppID)                      
		
--Update Kit Items                 
Update OKI set numQtyItemsReq=numQtyItemsReq_Orig * OI.numUnitHour
			   FROM OpportunityKitItems OKI JOIN OpportunityItems OI ON OKI.numOppItemID=OI.numoppitemtCode and OKI.numOppId=OI.numOppId  
			   WHERE OI.numOppId=@numOppId 

--Insert Kit Items                 
INSERT into OpportunityKitItems                                                                          
		(numOppId,numOppItemID,numChildItemID,numWareHouseItemId,numQtyItemsReq,numQtyItemsReq_Orig,numUOMId,numQtyShipped)
  SELECT @numOppId,OI.numoppitemtCode,ID.numChildItemID,ID.numWareHouseItemId,ID.numQtyItemsReq * OI.numUnitHour,ID.numQtyItemsReq,ID.numUOMId,0
	 FROM OpportunityItems OI JOIN ItemDetails ID ON OI.numItemCode=ID.numItemKitID WHERE 
	OI.numOppId=@numOppId AND OI.numoppitemtCode NOT IN (SELECT numOppItemID FROM OpportunityKitItems OKI WHERE OKI.numOppId=@numOppID)
--	OI.numoppitemtCode NOT IN (SELECT numoppitemtCode FROM OPENXML (@hDocItem,'/NewDataSet/Item[Op_Flag=2]',2)                                                                
--			   WITH  (                      
--				numoppitemtCode numeric(9))X)
			   
EXEC sp_xml_removedocument @hDocItem  
END

/*Added by chintan - While creating SO from PO it wasn't puting Item Qty to Allocation. this segment was being called from USP_OppSaveMilestone which No longer in use after changes anoop did. */     
--Updating the warehouse items              
declare @tintShipped as tinyint               
select @tintOppStatus=tintOppStatus,@tintOppType=tintOppType,@tintShipped=tintShipped from OpportunityMaster where numOppID=@numOppID              
if @tintOppStatus=1               
	begin         
	PRINT 'inside USP_UpdatingInventoryonCloseDeal'
		exec USP_UpdatingInventoryonCloseDeal @numOppID,@numUserCntID
	end              
            
if @tintOppType=1              
begin              
	if @tintOppStatus=1 Update WareHouseItmsDTL set tintStatus=1 where numWareHouseItmsDTLID in (select numWarehouseItmsDTLID from OppWarehouseSerializedItem where numOppID=@numOppID)
	if @tintShipped=1 Update WareHouseItmsDTL set tintStatus=2 where numWareHouseItmsDTLID in (select numWarehouseItmsDTLID from OppWarehouseSerializedItem where numOppID=@numOppID)              
END


declare @tintCRMType as numeric(9)        
declare @AccountClosingDate as datetime        

select @numDivisionID=numDivisionID,@AccountClosingDate=bintAccountClosingDate from  OpportunityMaster where numOppID=@numOppID         
select @tintCRMType=tintCRMType from divisionmaster where numDivisionID =@numDivisionID 

--When deal is won                                         
if @DealStatus = 1 
begin           
	 if @AccountClosingDate is null                   
	  update OpportunityMaster set bintAccountClosingDate=getutcdate() where numOppID=@numOppID              
end         
--When Deal is Lost
else if @DealStatus = 2
begin         
	 select @AccountClosingDate=bintAccountClosingDate from  OpportunityMaster where numOppID=@numOppID         
	 if @AccountClosingDate is null                   
	  update OpportunityMaster set bintAccountClosingDate=getutcdate() where numOppID=@numOppID
end                    

/*Added by chintan -  Biz usually automatically promotes the record from Lead/Prospect to Account When a Sales Opp/Order is made against it*/  
-- Promote Lead to Prospect when Sales/Purchase Opp is created against it
if @tintCRMType=0 AND @DealStatus = 0 --Lead & Open Opp
begin        
	update divisionmaster set tintCRMType=1,bintLeadProm=getutcdate(),bintLeadPromBy=@numUserCntID,bintProsProm=getutcdate(),bintProsPromBy=@numUserCntID        
	where numDivisionID=@numDivisionID        
end        
-- Promote Lead to Account when Sales/Purchase Order is created against it
ELSE if @tintCRMType=0 AND @DealStatus = 1 --Lead & Order
begin        
	update divisionmaster set tintCRMType=2,bintLeadProm=getutcdate(),bintLeadPromBy=@numUserCntID,bintProsProm=getutcdate(),bintProsPromBy=@numUserCntID        
	where numDivisionID=@numDivisionID        
end
--Promote Prospect to Account
else if @tintCRMType=1 AND @DealStatus = 1 
begin        
	update divisionmaster set tintCRMType=2,bintProsProm=getutcdate(),bintProsPromBy=@numUserCntID        
	where numDivisionID=@numDivisionID        
end

--Add/Update Address Details
DECLARE @vcStreet  varchar(100),@vcCity  varchar(50),@vcPostalCode  varchar(15),@vcCompanyName  varchar(100),@vcAddressName VARCHAR(50)      
DECLARE @numState  numeric(9),@numCountry  numeric(9), @numCompanyId  numeric(9) 
DECLARE @bitIsPrimary BIT;

select  @vcCompanyName=vcCompanyName,@numCompanyId=div.numCompanyID from CompanyInfo Com                            
join divisionMaster Div                            
on div.numCompanyID=com.numCompanyID                            
where div.numdivisionID=@numDivisionId

--Bill Address
IF @numBillAddressId>0
BEGIN
 SELECT  @vcStreet=ISNULL(vcStreet,''),@vcCity=ISNULL(vcCity,''),@vcPostalCode=ISNULL(vcPostalCode,''),
		@numState=ISNULL(numState,0),@numCountry=ISNULL(numCountry,0),@bitIsPrimary=bitIsPrimary,
		@vcAddressName=vcAddressName
            FROM  dbo.AddressDetails WHERE   numDomainID = @numDomainID AND numAddressID = @numBillAddressId
  	EXEC dbo.USP_UpdateOppAddress
	@numOppID = @numOppID, --  numeric(9, 0)
	@byteMode = 0, --  tinyint
	@vcStreet = @vcStreet, --  varchar(100)
	@vcCity = @vcCity, --  varchar(50)
	@vcPostalCode = @vcPostalCode, --  varchar(15)
	@numState = @numState, --  numeric(9, 0)
	@numCountry = @numCountry, --  numeric(9, 0)
	@vcCompanyName = @vcCompanyName, --  varchar(100)
	@numCompanyId = 0, --  numeric(9, 0)
	@vcAddressName = @vcAddressName
END
  
  
  --Ship Address
IF @numShipAddressId>0
BEGIN
	SELECT  @vcStreet=ISNULL(vcStreet,''),@vcCity=ISNULL(vcCity,''),@vcPostalCode=ISNULL(vcPostalCode,''),
		@numState=ISNULL(numState,0),@numCountry=ISNULL(numCountry,0),@bitIsPrimary=bitIsPrimary,
		@vcAddressName=vcAddressName
            FROM  dbo.AddressDetails WHERE   numDomainID = @numDomainID AND numAddressID = @numShipAddressId
 



  	EXEC dbo.USP_UpdateOppAddress
	@numOppID = @numOppID, --  numeric(9, 0)
	@byteMode = 1, --  tinyint
	@vcStreet = @vcStreet, --  varchar(100)
	@vcCity = @vcCity, --  varchar(50)
	@vcPostalCode = @vcPostalCode, --  varchar(15)
	@numState = @numState, --  numeric(9, 0)
	@numCountry = @numCountry, --  numeric(9, 0)
	@vcCompanyName = @vcCompanyName, --  varchar(100)
	@numCompanyId =@numCompanyId, --  numeric(9, 0)
	@vcAddressName = @vcAddressName
END

--Delete Tax for Opportunity Items if item deleted 
DELETE FROM OpportunityItemsTaxItems WHERE numOppId=@numOppId AND numOppItemID NOT IN (SELECT numoppitemtCode FROM OpportunityItems WHERE numOppId=@numOppID)

--Insert Tax for Opportunity Items
INSERT INTO dbo.OpportunityItemsTaxItems (
	numOppId,
	numOppItemID,
	numTaxItemID
) SELECT @numOppId,OI.numoppitemtCode,TI.numTaxItemID FROM dbo.OpportunityItems OI JOIN dbo.ItemTax IT ON OI.numItemCode=IT.numItemCode JOIN
TaxItems TI ON TI.numTaxItemID=IT.numTaxItemID WHERE OI.numOppId=@numOppID AND IT.bitApplicable=1 AND 
OI.numoppitemtCode NOT IN (SELECT numOppItemID FROM OpportunityItemsTaxItems WHERE numOppId=@numOppID)
UNION
  select @numOppId,OI.numoppitemtCode,0 FROM dbo.OpportunityItems OI JOIN dbo.Item I ON OI.numItemCode=I.numItemCode
   WHERE OI.numOppId=@numOppID  AND I.bitTaxable=1 AND
OI.numoppitemtCode NOT IN (SELECT numOppItemID FROM OpportunityItemsTaxItems WHERE numOppId=@numOppID)

  
 UPDATE OpportunityMaster SET monDealAmount=dbo.GetDealAmount(@numOppID,getdate(),0) WHERE numOppId=@numOppID
COMMIT
END TRY
BEGIN CATCH
	DECLARE @ErrorMessage NVARCHAR(4000)
	DECLARE @ErrorNumber INT
	DECLARE @ErrorSeverity INT
	DECLARE @ErrorState INT
	DECLARE @ErrorLine INT
	DECLARE @ErrorProcedure NVARCHAR(200)

	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;

	SET @numOppID = 0

	SELECT 
		@ErrorMessage = ERROR_MESSAGE(),
		@ErrorNumber = ERROR_NUMBER(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE(),
		@ErrorLine = ERROR_LINE(),
		@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

	RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);
END CATCH
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_OpportunityMaster_CreatePO')
DROP PROCEDURE dbo.USP_OpportunityMaster_CreatePO
GO
CREATE PROCEDURE [dbo].[USP_OpportunityMaster_CreatePO]
(
	@numOppID NUMERIC(18,0) OUTPUT,
	@numDomainID NUMERIC(18,0),
	@numUserCntID NUMERIC(18,0),
	@numItemCode NUMERIC(18,0),
	@numUnitHour NUMERIC(18,0),
	@numWarehouseItemID NUMERIC(18,0),
	@bitFromWorkOrder BIT
)
AS 
BEGIN
    DECLARE @ErrorMessage NVARCHAR(4000);SET @ErrorMessage=''
    DECLARE @ErrorSeverity INT;
    DECLARE @ErrorState INT;
    
	BEGIN TRY
	BEGIN TRAN
		DECLARE @numUnitPrice MONEY
		DECLARE @numContactID NUMERIC(18,0)
	    DECLARE @numDivisionID NUMERIC(18,0)
		DECLARE @numVendorID NUMERIC(18,0)
		DECLARE @fltExchangeRate float    
		DECLARE @numCurrencyID NUMERIC(18,0)
		DECLARE @vcPOppName VARCHAR(1000) = ''                         
		DECLARE @hDocItem int                                                                                                                            
		DECLARE @TotalAmount as money                                                                          
		DECLARE @tintOppStatus as tinyint               
		DECLARE @tintDefaultClassType NUMERIC
		DECLARE @numDefaultClassID NUMERIC;SET @numDefaultClassID=0


		SELECT @numDivisionID = numVendorID FROM Item WHERE numItemCode=@numItemCode

		IF ISNULL(@numDivisionID,0) > 0
		BEGIN
			SELECT @numContactID = numContactId FROM AdditionalContactsInformation WHERE numDivisionId = @numDivisionID AND bitPrimaryContact = 1

			IF @numContactID IS NULL
			BEGIN
				SELECT TOP 1 @numContactID = numContactId FROM AdditionalContactsInformation WHERE numDivisionId = @numDivisionID
			END

			SELECT @numUnitPrice = dbo.fn_FindVendorCost(@numItemCOde,@numDivisionID,@numDomainID,@numUnitHour)

			--Get Default Item Class for particular user based on Domain settings  
			SELECT @tintDefaultClassType=isnull(tintDefaultClassType,0) FROM dbo.Domain  WHERE numDomainID = @numDomainID 

			IF @tintDefaultClassType=0
				SELECT @numDefaultClassID=NULLIF(numDefaultClass,0) FROM dbo.UserMaster  WHERE numUserDetailId =@numUserCntID AND numDomainID = @numDomainID 
		
			DECLARE @intOppTcode AS NUMERIC(9)
			SELECT @intOppTcode=max(numOppId) FROM OpportunityMaster                
			SET @intOppTcode =isnull(@intOppTcode,0) + 1                                                
			SET @vcPOppName= @vcPOppName + '-' + convert(varchar(4),year(getutcdate()))  + '-A' + convert(varchar(10),@intOppTcode)                                  
  
			SET @fltExchangeRate=1
			SELECT @numCurrencyID= ISNULL(numCurrencyID,0) FROM dbo.Domain WHERE numDomainId=@numDomainId                                                                       
  
			--Set Default Class If enable User Level Class Accountng 
			DECLARE @numAccountClass AS NUMERIC(18);
			SET @numAccountClass=0
		
			SELECT 
				@numAccountClass=ISNULL(numDefaultClass,0) 
			FROM 
				dbo.UserMaster UM 
			JOIN 
				dbo.Domain D 
			ON 
				UM.numDomainID=D.numDomainId
			WHERE 
				D.numDomainId=@numDomainId 
				AND UM.numUserDetailId=@numUserCntID 
				AND ISNULL(D.IsEnableUserLevelClassTracking,0) = 1
                                                                       
			INSERT INTO OpportunityMaster                                                                          
			(                                                                             
				numContactId,numDivisionId,bitPublicFlag,tintSource,
				tintSourceType,vcPOppName,intPEstimatedCloseDate,numCreatedBy,bintCreatedDate,                                                                           
				numDomainId,numRecOwner,lngPConclAnalysis,tintOppType,                                                              
				numSalesOrPurType,numCurrencyID,fltExchangeRate,[tintOppStatus],numStatus,
				vcOppRefOrderNo,bitStockTransfer,bitBillingTerms,intBillingDays,bitInterestType,fltInterest,vcCouponCode,
				bitDiscountType,fltDiscount,bitPPVariance,numAccountClass,tintTaxOperator,bitUseShippersAccountNo,bitUseMarkupShippingRate,
				numMarkupShippingRate,intUsedShippingCompany,bitFromWorkOrder
			)                                                                          
			Values                                                                          
			(                                                                          
				@numContactId,@numDivisionId,0,0,1,@vcPOppName,DATEADD(D,1,GETUTCDATE()),@numUserCntID,GETUTCDATE(),
				@numDomainId,@numUserCntID,0,2,0,@numCurrencyID,@fltExchangeRate,1,
				0,NULL,0,0,0,0,0,'',0,0,1,@numAccountClass,0,0,0,0.00,0,@bitFromWorkOrder
			)                                                                                                                      
  
			set @numOppID=scope_identity()                                                
  
			--UPDATE OPPNAME AS PER NAME TEMPLATE
			EXEC dbo.USP_UpdateNameTemplateValue 2,@numDomainID,@numOppID

			EXEC dbo.USP_AddParentChildCustomFieldMap
			@numDomainID = @numDomainID,
			@numRecordID = @numOppID,
			@numParentRecId = @numDivisionId,
			@tintPageID = 6
 	
			--INSERTING ITEMS                                       
			IF @numOppID>0
			BEGIN                      

				INSERT INTO OpportunityItems                                                                          
				(
					numOppId,numItemCode,numUnitHour,monPrice,monTotAmount,vcItemDesc,numWarehouseItmsID,
					vcType,bitDropShip,bitDiscountType,fltDiscount,monTotAmtBefDiscount,vcItemName,vcModelID,
					vcManufacturer,vcPathForTImage,monVendorCost,numUOMId,bitWorkOrder,numVendorWareHouse,
					numShipmentMethod,numSOVendorId,numProjectID,numProjectStageID,numClassID,numToWarehouseItemID,
					vcAttributes,monAvgCost,bitItemPriceApprovalRequired
				)
				SELECT 
					@numOppID,@numItemCode,@numUnitHour,@numUnitPrice,(@numUnitPrice * @numUnitHour),Item.txtItemDesc,@numWarehouseItemID,
					'',0,0,0,(@numUnitPrice * @numUnitHour),Item.vcItemName,Item.vcModelID,Item.vcManufacturer,
					(SELECT TOP 1 vcPathForTImage FROM ItemImages WHERE numItemCode = Item.numItemCode AND BitDefault = 1 AND numdomainId = @numdomainId),
					(select isnull(VN.monCost,0) from Vendor VN, Item IT where IT.numItemCode=VN.numItemCode and  VN.numItemCode=Item.numItemCode and VN.numVendorID=IT.numVendorID),
					Item.numBaseUnit,0,NULL,NULL,NULL,NULL,NULL,
					Case When @tintDefaultClassType=0 then @numDefaultClassID else (SELECT numItemClass FROM item WHERE numItemCode = Item.numItemCode) end,
					NULL,'', (SELECT ISNULL(monAverageCost,0) FROM Item I WHERE I.numDomainId=@numDomainId AND I.numItemCode=Item.numItemCode),0
				FROM
					Item
				WHERE
					numItemCode = @numItemCode
			END          
	
			SET @TotalAmount=0                                                           
			SELECT @TotalAmount=sum(monTotAmount) FROM OpportunityItems WHERE numOppId=@numOppID                                                                          
			UPDATE OpportunityMaster SET monPamount=@TotalAmount WHERE numOppId=@numOppID                                                 
                      
			--Insert Tax for Division   
			IF (select isnull(bitPurchaseTaxCredit,0) from Domain where numDomainId=@numDomainId)=1
			BEGIN 
				INSERT dbo.OpportunityMasterTaxItems 
				(
					numOppId,
					numTaxItemID,
					fltPercentage
				) 
				SELECT 
					@numOppID,
					TI.numTaxItemID,
					dbo.fn_CalItemTaxAmt(@numDomainID,@numDivisionID,0,TI.numTaxItemID,@numOppId,0,NULL) 
				FROM 
					TaxItems TI 
				JOIN 
					DivisionTaxTypes DTT ON TI.numTaxItemID = DTT.numTaxItemID
				WHERE 
					DTT.numDivisionID=@numDivisionId AND DTT.bitApplicable=1
				UNION 
				SELECT
					@numOppID,
					0,
					dbo.fn_CalItemTaxAmt(@numDomainID,@numDivisionID,0,0,@numOppId,1,NULL) 
				FROM 
					dbo.DivisionMaster 
				WHERE 
					bitNoTax=0 AND 
					numDivisionID=@numDivisionID
			END

			/*Added by chintan - While creating SO from PO it wasn't puting Item Qty to Allocation. this segment was being called from USP_OppSaveMilestone which No longer in use after changes anoop did. */     
			--Updating the warehouse items              
			DECLARE @tintShipped AS TINYINT               
			DECLARE @tintOppType AS TINYINT
			SELECT 
				@tintOppStatus=tintOppStatus,
				@tintOppType=tintOppType,
				@tintShipped=tintShipped 
			FROM 
				OpportunityMaster 
			WHERE 
				numOppID=@numOppID              
		
			IF @tintOppStatus=1               
			BEGIN         
				EXEC USP_UpdatingInventoryonCloseDeal @numOppID,@numUserCntID
			END

			DECLARE @tintCRMType as numeric(9)        
			DECLARE @AccountClosingDate as datetime        

			select @numDivisionID=numDivisionID,@AccountClosingDate=bintAccountClosingDate from  OpportunityMaster where numOppID=@numOppID         
			select @tintCRMType=tintCRMType from divisionmaster where numDivisionID =@numDivisionID 

			IF @AccountClosingDate is null                   
			UPDATE OpportunityMaster set bintAccountClosingDate=getutcdate() where numOppID=@numOppID              
               
			/*Added by chintan -  Biz usually automatically promotes the record from Lead/Prospect to Account When a Sales Opp/Order is made against it*/  
			-- Promote Lead to Prospect when Sales/Purchase Opp is created against it
			IF @tintCRMType=0 AND 1 = 0 --Lead & Open Opp
			BEGIN
				UPDATE divisionmaster set tintCRMType=1,bintLeadProm=getutcdate(),bintLeadPromBy=@numUserCntID,bintProsProm=getutcdate(),bintProsPromBy=@numUserCntID        
				WHERE numDivisionID=@numDivisionID        
			END        
			-- Promote Lead to Account when Sales/Purchase Order is created against it
			ELSE if @tintCRMType=0 AND 1 = 1 --Lead & Order
			BEGIN 
				UPDATE divisionmaster set tintCRMType=2,bintLeadProm=getutcdate(),bintLeadPromBy=@numUserCntID,bintProsProm=getutcdate(),bintProsPromBy=@numUserCntID        
				WHERE numDivisionID=@numDivisionID        
			END
			--Promote Prospect to Account
			ELSE IF @tintCRMType=1 AND 1 = 1 
			BEGIN        
				update divisionmaster set tintCRMType=2,bintProsProm=getutcdate(),bintProsPromBy=@numUserCntID        
				where numDivisionID=@numDivisionID        
			END

			--Add/Update Address Details
			DECLARE @vcStreet  varchar(100),@vcCity  varchar(50),@vcPostalCode  varchar(15),@vcCompanyName  varchar(100),@vcAddressName VARCHAR(50)      
			DECLARE @numState  numeric(9),@numCountry  numeric(9), @numCompanyId  numeric(9) 
			DECLARE @bitIsPrimary BIT;

			SELECT  @vcCompanyName=vcCompanyName,@numCompanyId=div.numCompanyID FROM CompanyInfo Com                            
			JOIN divisionMaster Div                            
			ON div.numCompanyID=com.numCompanyID                            
			WHERE div.numdivisionID=@numDivisionId

			--INSERT TAX FOR OPPORTUNITY ITEMS
			INSERT INTO dbo.OpportunityItemsTaxItems 
			(
				numOppId,
				numOppItemID,
				numTaxItemID
			) 
			SELECT 
				@numOppId,OI.numoppitemtCode,TI.numTaxItemID 
			FROM 
				dbo.OpportunityItems OI 
			JOIN dbo.ItemTax IT ON OI.numItemCode=IT.numItemCode 
			JOIN TaxItems TI ON TI.numTaxItemID=IT.numTaxItemID 
			WHERE 
				OI.numOppId=@numOppID 
				AND IT.bitApplicable=1 
				AND OI.numoppitemtCode NOT IN (SELECT numOppItemID FROM OpportunityItemsTaxItems WHERE numOppId=@numOppID)
			UNION
			SELECT 
				@numOppId,OI.numoppitemtCode,0 
			FROM 
				dbo.OpportunityItems OI 
			JOIN dbo.Item I ON OI.numItemCode=I.numItemCode
			WHERE 
				OI.numOppId=@numOppID  
				AND I.bitTaxable=1 
				AND	OI.numoppitemtCode NOT IN (SELECT numOppItemID FROM OpportunityItemsTaxItems WHERE numOppId=@numOppID)

			UPDATE OpportunityMaster SET monDealAmount=dbo.GetDealAmount(@numOppID,getdate(),0) WHERE numOppId=@numOppID
		END
	COMMIT TRAN 
	END TRY
	BEGIN CATCH
		IF ( @@TRANCOUNT > 0 ) 
		BEGIN
			ROLLBACK TRAN
		END

		SELECT @ErrorMessage = ERROR_MESSAGE(),@ErrorSeverity = ERROR_SEVERITY(),@ErrorState = ERROR_STATE()
		RAISERROR (@ErrorMessage,@ErrorSeverity,@ErrorState);
	END CATCH;
END
/****** Object:  StoredProcedure [dbo].[USP_OppShippingorReceiving]    Script Date: 07/26/2008 16:20:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- created by anoop jayaraj            
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_oppshippingorreceiving')
DROP PROCEDURE usp_oppshippingorreceiving
GO
CREATE PROCEDURE [dbo].[USP_OppShippingorReceiving]            
@OppID as numeric(9),
@numUserCntID AS NUMERIC(9)          
as
BEGIN TRY
	
	DECLARE @numDomain AS NUMERIC(18,0)
	SELECT @numDomain = OM.numDomainID FROM [dbo].[OpportunityMaster] AS OM WHERE [OM].[numOppId] = @OppID
	PRINT @numDomain
	-- Added by Manish Anjara : 17th Sep,2014
	-- Check whether order is already closed or not. If closed then show a warning message
	IF (SELECT ISNULL(tintshipped,0) FROM [dbo].[OpportunityMaster] WHERE [OpportunityMaster].[numOppId] = @OppID AND [OpportunityMaster].[numDomainId] = @numDomain) = 1
	BEGIN
		RAISERROR('ORDER_CLOSED', 16, 1)
	END		

   BEGIN TRANSACTION 
			
			update OpportunityMaster set tintshipped=1,bintAccountClosingDate=GETUTCDATE(), bintClosedDate=GETUTCDATE() where  numOppId=@OppID 
			UPDATE [OpportunityBizDocs] SET [dtShippedDate] = GETUTCDATE() WHERE [numOppId]=@OppID
			
						
			declare @status as varchar(2)            
			declare @OppType as varchar(2)   
			declare @bitStockTransfer as BIT
			DECLARE @fltExchangeRate AS FLOAT 
         
			select @status=tintOppStatus,@OppType=tintOppType,@bitStockTransfer=ISNULL(bitStockTransfer,0),@fltExchangeRate=(CASE WHEN ISNULL(fltExchangeRate,0)=0 THEN 1 ELSE fltExchangeRate END) from OpportunityMaster
			where numOppId=@OppID
			
			-- Archive item based on Item's individual setting
			IF @OppType = 1
			BEGIN
				UPDATE Item SET IsArchieve = CASE WHEN EXISTS(SELECT OpportunityItems.numItemCode FROM dbo.OpportunityItems WHERE OpportunityItems.numItemCode = I.numItemCode) 
												  THEN 1
												  ELSE 0 
											 END 
				FROM Item I JOIN dbo.OpportunityItems OI ON I.numItemCode = OI.numItemCode  
				WHERE numOppId = @OppID 
				AND I.[numDomainID] = @numDomain

				AND ISNULL(I.bitArchiveItem,0) = 1 
			END
			----------------------------------------------------------------------------            
			--Updating Item Details            
			-----------------------------------------------------------------------            
            
			---Updating the inventory Items once the deal is won
			declare @numoppitemtCode as numeric
			declare @numUnits as numeric
			declare @numWarehouseItemID as numeric       
			declare @numToWarehouseItemID as numeric(9) --to be used with stock transfer
			declare @itemcode as numeric        
			declare @QtyShipped as NUMERIC
			declare @QtyReceived as numeric
			Declare @monPrice as money
			declare @Kit as bit
			declare @bitWorkOrder AS BIT
			declare @bitSerialized AS BIT
			declare @bitLotNo AS BIT
    
			 select top 1 @numoppitemtCode=numoppitemtCode,@itemcode=OI.numItemCode,@numUnits=ISNULL(numUnitHour,0),@qtyShipped=ISNULL([numQtyShipped],0),@qtyReceived=ISNULL([numUnitHourReceived],0),
			 @numWareHouseItemID=ISNULL(numWarehouseItmsID,0),@numToWarehouseItemID=numToWarehouseItemID,@monPrice=isnull(monPrice,0) * @fltExchangeRate,@Kit= (case when bitKitParent=1 and bitAssembly=1 then 0 when bitKitParent=1 then 1 else 0 end),
			 @bitWorkOrder = ISNULL(bitWorkOrder,0),@bitSerialized=ISNULL(bitSerialized,0),@bitLotNo=ISNULL(bitLotNo,0)
			 from OpportunityItems OI join Item I                                                
			 on OI.numItemCode=I.numItemCode and numOppId=@OppID and (bitDropShip=0 or bitDropShip is null)                                            
			 where (charitemtype='P' OR 1=(CASE WHEN @OppType=1 THEN 
							CASE WHEN ISNULL(I.bitKitParent,0) = 1 AND ISNULL(I.bitAssembly,0) = 1 THEN 0
								 WHEN ISNULL(I.bitKitParent,0) = 1 THEN 1
								 ELSE 0 END 
							ELSE 0 END))  
			 AND I.[numDomainID] = @numDomain	
							order by OI.numoppitemtCode      

			 while @numoppitemtCode>0                
			 begin   
 
--			  if @Kit = 1
--			  begin
--					exec USP_UpdateKitItems @numoppitemtCode,@numUnits,@OppType,2,@OppID
--			  end
             
             
				IF @bitStockTransfer = 1
				BEGIN
					--Make inventory changes as if sales order was closed , using OpportunityItems.numWarehouseItmsID
					EXEC usp_ManageInventory @itemcode,@numWareHouseItemID,0,1,@numUnits,@qtyShipped,@qtyReceived,@monPrice,2,@OppID,@numoppitemtCode,@numUserCntID,@bitWorkOrder
					--Make inventory changes as if purchase order was closed, using OpportunityItems.numToWarehouseItemID
					EXEC usp_ManageInventory @itemcode,@numToWarehouseItemID,0,2,@numUnits,@qtyShipped,@qtyReceived,@monPrice,2,@OppID,@numoppitemtCode,@numUserCntID,@bitWorkOrder
					If @bitSerialized = 1 OR @bitLotNo = 1
					BEGIN
						--Transfer Serial/Lot Numbers
						EXEC USP_WareHouseItmsDTL_TransferSerialLotNo @OppID,@numoppitemtCode,@numWareHouseItemID,@numToWarehouseItemID,@bitSerialized,@bitLotNo
					END
				END
				ELSE
				BEGIN
					EXEC usp_ManageInventory @itemcode,@numWareHouseItemID,0,@OppType,@numUnits,@qtyShipped,@qtyReceived,@monPrice,2,@OppID,@numoppitemtCode,@numUserCntID,@bitWorkOrder
				END
        
			  select top 1 @numoppitemtCode=numoppitemtCode,@itemcode=OI.numItemCode,@numUnits=ISNULL(numUnitHour,0),@qtyShipped=ISNULL([numQtyShipped],0),@qtyReceived=ISNULL([numUnitHourReceived],0),
			  @numWareHouseItemID=ISNULL(numWarehouseItmsID,0),@numToWarehouseItemID=numToWarehouseItemID,@monPrice=isnull(monPrice,0) * @fltExchangeRate,@Kit= (case when bitKitParent=1 and bitAssembly=1 then 0 when bitKitParent=1 then 1 else 0 end) 
			  from OpportunityItems OI
			  join Item I
			  on OI.numItemCode=I.numItemCode and numOppId=@OppID                                              
			  where (charitemtype='P' OR 1=(CASE WHEN @OppType=1 THEN 
							CASE WHEN ISNULL(I.bitKitParent,0) = 1 AND ISNULL(I.bitAssembly,0) = 1 THEN 0
								 WHEN ISNULL(I.bitKitParent,0) = 1 THEN 1
								 ELSE 0 END 
							ELSE 0 END)) and OI.numoppitemtCode>@numoppitemtCode and (bitDropShip=0 or bitDropShip is null) 
			  AND I.numDomainID = @numDomain
			  ORDER by OI.numoppitemtCode                                                
			  if @@rowcount=0 set @numoppitemtCode=0     
  
			 END
				--Update WareHouseItmsDTL : from tintStatus 2 to 0 (PO Serial/Lot No)

			IF @OppType=2
			BEGIn
			   update WareHouseItmsDTL SET tintStatus=0 FROM WareHouseItmsDTL owsi JOIN OppWarehouseSerializedItem opp 
			  ON owsi.numWareHouseItmsDTLID=opp.numWareHouseItmsDTLID where opp.numOppID=@OppID AND owsi.tintStatus=2
			END
			IF @OppType=1
			BEGIN
			 UPDATE  WareHouseItmsDTL SET WareHouseItmsDTL.numQty=WareHouseItmsDTL.numQty - isnull(OppWarehouseSerializedItem.numQty,0)
			 from   OppWarehouseSerializedItem join WareHouseItmsDTL
			 on WareHouseItmsDTL.numWareHouseItmsDTLID= OppWarehouseSerializedItem.numWarehouseItmsDTLID                           
			 where numOppID=@OppID      
			END

 COMMIT
END TRY
BEGIN CATCH
  -- Whoops, there was an error
  IF @@TRANCOUNT > 0
     ROLLBACK

  -- Raise an error with the details of the exception
  DECLARE @ErrMsg nvarchar(4000), @ErrSeverity int
  SELECT @ErrMsg = ERROR_MESSAGE(),
         @ErrSeverity = ERROR_SEVERITY()

  RAISERROR(@ErrMsg, @ErrSeverity, 1)
END CATCH

GO
/****** Object:  StoredProcedure [dbo].[USP_ReOpenOppertunity]    Script Date: 07/26/2008 16:20:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
          
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ReOpenOppertunity')
DROP PROCEDURE USP_ReOpenOppertunity
GO
CREATE PROCEDURE [dbo].[USP_ReOpenOppertunity]            
@OppID as numeric(9),
@numUserCntID AS NUMERIC(9),
@numDomainID AS NUMERIC(9)
          
as          
BEGIN TRY
		-- Added by Manish Anjara : 17th Sep,2014
	-- Check whether order is already opened or not. If already opened then show a warning message
	IF (SELECT ISNULL(tintshipped,0) FROM [dbo].[OpportunityMaster] WHERE [OpportunityMaster].[numOppId] = @OppID AND [OpportunityMaster].[numDomainId] = @numDomainID) = 0
	BEGIN
		RAISERROR('ORDER_OPENED', 16, 1)
	END

   BEGIN TRANSACTION       
			update OpportunityMaster set tintshipped=0,bintAccountClosingDate=NULL,bintClosedDate=NULL where  numOppId=@OppID  AND [numDomainId] = @numDomainID
			UPDATE [OpportunityBizDocs] SET [dtShippedDate] = NULL WHERE [numOppId]=@OppID
        			
			--SET Received/Shipped qty to 0	
			update OpportunityItems SET numUnitHourReceived=0,numQtyShipped=0 WHERE [numOppId]=@OppID
			
			
			update OpportunityKitItems SET numQtyShipped=0 WHERE [numOppId]=@OppID
			
			declare @status as varchar(2)            
			declare @OppType as varchar(2)  
			DECLARE @fltExchangeRate AS FLOAT 
			          
			select @status=tintOppStatus,@OppType=tintOppType,@fltExchangeRate=(CASE WHEN ISNULL(fltExchangeRate,0)=0 THEN 1 ELSE fltExchangeRate END) from OpportunityMaster            
			where numOppId=@OppID AND [numDomainId] = @numDomainID
			
			-- UnArchive item based on Item's individual setting
			IF @OppType = 1
			BEGIN
				UPDATE Item SET IsArchieve = CASE WHEN EXISTS(SELECT OpportunityItems.numItemCode FROM dbo.OpportunityItems 
															  WHERE OpportunityItems.numItemCode = I.numItemCode
															  AND OpportunityItems.numoppitemtCode = OI.numoppitemtCode) 
												  THEN 0
												  ELSE 1 
											 END 
				FROM Item I JOIN dbo.OpportunityItems OI ON I.numItemCode = OI.numItemCode  
				WHERE numOppId = @OppID 
				AND I.[numDomainId] = @numDomainID
				AND ISNULL(I.bitArchiveItem,0) = 1 
			END     
			----------------------------------------------------------------------------            
			--Updating Item Details            
			-----------------------------------------------------------------------            
            
			---Updating the inventory Items once the deal is won              
			declare @numoppitemtCode as numeric            
			declare @numUnits as numeric              
			declare @numWarehouseItemID as numeric       
			declare @itemcode as numeric        
			declare @QtyShipped as NUMERIC
			declare @QtyReceived as numeric
			Declare @monPrice as money  
			declare @Kit as bit  
			declare @bitWorkOrder AS BIT
    
			 select top 1 @numoppitemtCode=numoppitemtCode,@itemcode=OI.numItemCode,@numUnits=ISNULL(numUnitHour,0),@qtyShipped=ISNULL([numQtyShipped],0),@qtyReceived=ISNULL([numUnitHourReceived],0),
			 @numWareHouseItemID=ISNULL(numWarehouseItmsID,0),@monPrice=isnull(monPrice,0) * @fltExchangeRate,@Kit= (case when bitKitParent=1 and bitAssembly=1 then 0 when bitKitParent=1 then 1 else 0 end),
			 @bitWorkOrder = bitWorkOrder
			 from OpportunityItems OI join Item I                                                
			 on OI.numItemCode=I.numItemCode and numOppId=@OppID and (bitDropShip=0 or bitDropShip is null)                                            
			 where (charitemtype='P' OR 1=(CASE WHEN @OppType=1 THEN 
							CASE WHEN ISNULL(I.bitKitParent,0) = 1 AND ISNULL(I.bitAssembly,0) = 1 THEN 0
								 WHEN ISNULL(I.bitKitParent,0) = 1 THEN 1
								 ELSE 0 END 
							ELSE 0 END))  
			 AND I.[numDomainId] = @numDomainID
			order by OI.numoppitemtCode      
            
			 while @numoppitemtCode>0                
			 begin   
 
				EXEC usp_ManageInventory @itemcode,@numWareHouseItemID,0,@OppType,@numUnits,@qtyShipped,@qtyReceived,@monPrice,3,@OppID,@numoppitemtCode,@numUserCntID,@bitWorkOrder
        
			  select top 1 @numoppitemtCode=numoppitemtCode,@itemcode=OI.numItemCode,@numUnits=ISNULL(numUnitHour,0),@qtyShipped=ISNULL([numQtyShipped],0),@qtyReceived=ISNULL([numUnitHourReceived],0),
			  @numWareHouseItemID=ISNULL(numWarehouseItmsID,0),@monPrice=isnull(monPrice,0) * @fltExchangeRate,@Kit= (case when bitKitParent=1 and bitAssembly=1 then 0 when bitKitParent=1 then 1 else 0 end) 
			  from OpportunityItems OI                                                
			  join Item I                                                
			  on OI.numItemCode=I.numItemCode and numOppId=@OppID                                              
			  where (charitemtype='P' OR 1=(CASE WHEN @OppType=1 THEN 
							CASE WHEN ISNULL(I.bitKitParent,0) = 1 AND ISNULL(I.bitAssembly,0) = 1 THEN 0
								 WHEN ISNULL(I.bitKitParent,0) = 1 THEN 1
								 ELSE 0 END 
							ELSE 0 END))
					and OI.numoppitemtCode>@numoppitemtCode and (bitDropShip=0 or bitDropShip is null) 
					AND I.[numDomainId] = @numDomainID
					order by OI.numoppitemtCode                                                
			  if @@rowcount=0 set @numoppitemtCode=0     
  
			 END
				--Update WareHouseItmsDTL : from tintStatus 0 to 2 (PO Serial/Lot No)

			IF @OppType=2
			BEGIn
			  --   update WareHouseItmsDTL SET tintStatus=2 FROM WareHouseItmsDTL owsi JOIN OppWarehouseSerializedItem opp 
			  --  ON owsi.numWareHouseItmsDTLID=opp.numWareHouseItmsDTLID where opp.numOppID=@OppID AND owsi.tintStatus=0
				DELETE FROM WareHouseItmsDTL WHERE numWareHouseItmsDTLID in(SELECT numWareHouseItmsDTLID FROM OppWarehouseSerializedItem WHERE numOppID=@OppID)
				DELETE FROM OppWarehouseSerializedItem WHERE numOppID=@OppID
			END
			IF @OppType=1
			BEGIN
			 UPDATE  WareHouseItmsDTL SET WareHouseItmsDTL.numQty=WareHouseItmsDTL.numQty + isnull(OppWarehouseSerializedItem.numQty,0)
			 from   OppWarehouseSerializedItem join WareHouseItmsDTL
			 on WareHouseItmsDTL.numWareHouseItmsDTLID= OppWarehouseSerializedItem.numWarehouseItmsDTLID                           
			 where numOppID=@OppID      
			END
			
			
			IF @OppType=2
			BEGIn
			DELETE FROM dbo.General_Journal_Details WHERE numJournalId IN (SELECT numJournal_Id FROM dbo.General_Journal_Header WHERE numDomainID=@numDomainID AND numOppId=@OppID AND ISNULL(numOppBizDocsId,0)=0 
				AND isnull(numBillID,0)=0 AND isnull(numBillPaymentID,0)=0 AND isnull(numPayrollDetailID,0)=0
				AND isnull(numCheckHeaderID,0)=0 AND isnull(numReturnID,0)=0 
				AND isnull(numDepositId,0)=0)
			
			DELETE FROM General_Journal_Header WHERE numDomainID=@numDomainID AND numOppId=@OppID AND ISNULL(numOppBizDocsId,0)=0 
				AND isnull(numBillID,0)=0 AND isnull(numBillPaymentID,0)=0 AND isnull(numPayrollDetailID,0)=0
				AND isnull(numCheckHeaderID,0)=0 AND isnull(numReturnID,0)=0 
				AND isnull(numDepositId,0)=0
			END
COMMIT
END TRY
BEGIN CATCH
  -- Whoops, there was an error
  IF @@TRANCOUNT > 0
     ROLLBACK

  -- Raise an error with the details of the exception
  DECLARE @ErrMsg nvarchar(4000), @ErrSeverity int
  SELECT @ErrMsg = ERROR_MESSAGE(),
         @ErrSeverity = ERROR_SEVERITY()

  RAISERROR(@ErrMsg, @ErrSeverity, 1)
END CATCH


GO
/****** Object:  StoredProcedure [dbo].[USP_RevertDetailsOpp]    Script Date: 07/26/2008 16:20:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj      
/* deducts Qty from allocation and Adds back to onhand */
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_RevertDetailsOpp')
DROP PROCEDURE USP_RevertDetailsOpp
GO
CREATE PROCEDURE [dbo].[USP_RevertDetailsOpp] 
@numOppId NUMERIC,
@tintMode AS TINYINT=0, -- 0:Add/Edit 1:Delete
@numUserCntID AS NUMERIC(9)
AS ---reverting back to previous state if deal is being edited 	DECLARE @numDomain AS NUMERIC(18,0)	SELECT @numDomain = numDomainID FROM [dbo].[OpportunityMaster] AS OM WHERE [OM].[numOppId] = @numOppID    	DECLARE @OppType AS VARCHAR(2)                  DECLARE @itemcode AS NUMERIC           DECLARE @numWareHouseItemID AS NUMERIC                                       DECLARE @numToWarehouseItemID AS NUMERIC     DECLARE @numUnits AS NUMERIC                                                  DECLARE @onHand AS NUMERIC                                                DECLARE @onOrder AS NUMERIC                                                DECLARE @onBackOrder AS NUMERIC                                                  DECLARE @onAllocation AS NUMERIC    DECLARE @numQtyShipped AS NUMERIC    DECLARE @numUnitHourReceived AS NUMERIC    DECLARE @numoppitemtCode AS NUMERIC(9)     DECLARE @monAmount AS MONEY     DECLARE @monAvgCost AS MONEY       DECLARE @Kit AS BIT                                            DECLARE @bitKitParent BIT    DECLARE @bitStockTransfer BIT     DECLARE @numOrigUnits AS NUMERIC			    DECLARE @description AS VARCHAR(100)	DECLARE @bitWorkOrder AS BIT	--Added by :Sachin Sadhu||Date:18thSept2014	--For Rental/Asset Project	Declare @numRentalIN as Numeric	Declare @numRentalOut as Numeric	Declare @numRentalLost as Numeric	DECLARE @bitAsset as BIT	--end sachin    						    SELECT TOP 1            @numoppitemtCode = numoppitemtCode,            @itemcode = OI.numItemCode,            @numUnits = ISNULL(numUnitHour,0),            @numWareHouseItemID = ISNULL(numWarehouseItmsID,0),            @Kit = ( CASE WHEN bitKitParent = 1                               AND bitAssembly = 1 THEN 0                          WHEN bitKitParent = 1 THEN 1                          ELSE 0                     END ),            @monAmount = ISNULL(monTotAmount,0) * (CASE WHEN ISNULL(fltExchangeRate,0)=0 THEN 1 ELSE fltExchangeRate END),            @monAvgCost = ISNULL(monAverageCost,0),            @numQtyShipped = ISNULL(numQtyShipped,0),            @numUnitHourReceived = ISNULL(numUnitHourReceived,0),            @bitKitParent=ISNULL(bitKitParent,0),            @numToWarehouseItemID =OI.numToWarehouseItemID,            @bitStockTransfer = ISNULL(OM.bitStockTransfer,0),            @OppType = tintOppType,		    @numRentalIN=ISNULL(oi.numRentalIN,0),			@numRentalOut=Isnull(oi.numRentalOut,0),			@numRentalLost=Isnull(oi.numRentalLost,0),			@bitAsset =ISNULL(I.bitAsset,0),			@bitWorkOrder = ISNULL(OI.bitWorkOrder,0)    FROM    OpportunityItems OI			JOIN dbo.OpportunityMaster OM ON OI.numOppId = OM.numOppId            JOIN Item I ON OI.numItemCode = I.numItemCode    WHERE   (charitemtype='P' OR 1=(CASE WHEN tintOppType=1 THEN 							CASE WHEN ISNULL(I.bitKitParent,0) = 1 AND ISNULL(I.bitAssembly,0) = 1 THEN 0								 WHEN ISNULL(I.bitKitParent,0) = 1 THEN 1								 ELSE 0 END 							ELSE 0 END)) AND OI.numOppId = @numOppId                           AND ( bitDropShip = 0                                 OR bitDropShip IS NULL                               )     ORDER BY OI.numoppitemtCode	PRINT '@numoppitemtCode:' + CAST(@numoppitemtCode AS VARCHAR(10))	PRINT '@numUnits:' + CAST(@numUnits AS VARCHAR(10))	PRINT '@numWareHouseItemID:' + CAST(@numWareHouseItemID AS VARCHAR(10))	PRINT '@numQtyShipped:' + CAST(@numQtyShipped AS VARCHAR(10))	PRINT '@numUnitHourReceived:' + CAST(@numUnitHourReceived AS VARCHAR(10))	PRINT '@numToWarehouseItemID:' + CAST(@numToWarehouseItemID AS VARCHAR(10))	PRINT '@bitStockTransfer:' + CAST(@bitStockTransfer AS VARCHAR(10))	    WHILE @numoppitemtCode > 0                                          BEGIN            --Kamal : Item Group take as inline Item--            IF @Kit = 1 --                BEGIN  --                    EXEC USP_UpdateKitItems @numoppitemtCode, @numUnits,--                        @OppType, 1,@numOppID  --                END                                      SET @numOrigUnits=@numUnits                        IF @bitStockTransfer=1            BEGIN				SET @OppType = 1			END                        PRINT '@OppType:' + CAST(@OppType AS VARCHAR(10))                  IF @numWareHouseItemID>0            BEGIN     				PRINT 'FROM REVERT : CONDITION : @numWareHouseItemID>0'                             				SELECT  @onHand = ISNULL(numOnHand, 0),						@onAllocation = ISNULL(numAllocation, 0),						@onOrder = ISNULL(numOnOrder, 0),						@onBackOrder = ISNULL(numBackOrder, 0)				FROM    WareHouseItems				WHERE   numWareHouseItemID = @numWareHouseItemID       								PRINT '@onHand:' + CAST(@onHand AS VARCHAR(10))				PRINT '@onAllocation:' + CAST(@onAllocation AS VARCHAR(10))				PRINT '@onOrder:' + CAST(@onOrder AS VARCHAR(10))				PRINT '@onBackOrder:' + CAST(@onBackOrder AS VARCHAR(10))                                                     END                        IF @OppType = 1                 BEGIN					SET @description='SO Deleted (Qty:' + CAST(@numUnits AS VARCHAR(10)) + ' Shipped:' +  CAST(@numQtyShipped AS VARCHAR(10)) + ')'                                IF @Kit = 1				BEGIN					exec USP_UpdatingInventoryForEmbeddedKits @numoppitemtCode,@numOrigUnits,1,@numOppID,@tintMode,@numUserCntID				END				ELSE IF @bitWorkOrder = 1				BEGIN										IF @tintMode=0 -- EDIT					BEGIN						SET @description='SO-WO Edited (Qty:' + CAST(@numUnits AS VARCHAR(10)) + ' Shipped:' +  CAST(@numQtyShipped AS VARCHAR(10)) + ')'						-- UPDATE WORK ORDER - REVERT INVENTOY OF ASSEMBLY ITEMS						EXEC USP_ManageInventoryAssemblyWO @numUserCntID,@numOppID,@itemcode,@numWareHouseItemID,@numQtyShipped,5					END					ELSE IF @tintMode=1 -- DELETE					BEGIN						DECLARE @numWOID AS NUMERIC(18,0)						DECLARE @numWOStatus AS NUMERIC(18,0)						SELECT @numWOID=numWOId,@numWOStatus=numWOStatus FROM WorkOrder WHERE numOppId=@numOppId AND numItemCode=@itemcode AND numWareHouseItemId=@numWareHouseItemID						SET @description='SO-WO Deleted (Qty:' + CAST(@numUnits AS VARCHAR(10)) + ' Shipped:' +  CAST(@numQtyShipped AS VARCHAR(10)) + ')'						--IF WORK ORDER IS NOT COMPLETED THAN REMOVE ON ORDER QUANTITY						IF @numWOStatus <> 23184						BEGIN							IF @onOrder >= @numUnits								SET @onOrder = @onOrder - @numUnits							ELSE								SET @onOrder = 0						END						-- IF BACKORDER QTY IS GREATER THEN QTY TO REVERT THEN
						-- DECREASE BACKORDER QTY BY QTY TO REVERT
						IF @numUnits < @onBackOrder 
						BEGIN                  
							SET @onBackOrder = @onBackOrder - @numUnits
						END 
						-- IF BACKORDER QTY IS LESS THEN OR EQUAL TO QTY TO REVERT THEN
						-- DECREASE QTY TO REVERT BY QTY OF BACK ORDER AND
						-- SET BACKORDER QTY TO 0
						ELSE IF @numUnits >= @onBackOrder 
						BEGIN
							SET @numUnits = @numUnits - @onBackOrder
							SET @onBackOrder = 0
                        
							--REMOVE ITEM FROM ALLOCATION 
							IF (@onAllocation - @numUnits) >= 0
								SET @onAllocation = @onAllocation - @numUnits
						
							--ADD QTY TO ONHAND
							SET @onHand = @onHand + @numUnits
						END						UPDATE  WareHouseItems						SET     numOnHand = @onHand ,								numAllocation = @onAllocation,								numBackOrder = @onBackOrder,								numOnOrder = @onOrder,								dtModified = GETDATE()						WHERE   numWareHouseItemID = @numWareHouseItemID   						--IF WORK ORDER IS NOT COMPLETED THAN DELETE WORK ORDER AND ITS CHILDS WORK ORDER						IF @numWOStatus <> 23184						BEGIN							EXEC USP_DeleteAssemblyWOAndInventory  @numDomain,@numUserCntID,@numWOID,@numOppID						END					END				END					ELSE					BEGIN	                          IF @numQtyShipped>0								 BEGIN									IF @tintMode=0											SET @numUnits = @numUnits - @numQtyShipped									ELSE IF @tintmode=1											SET @onAllocation = @onAllocation + @numQtyShipped 								  END 								                                        IF @numUnits >= @onBackOrder                         BEGIN                            SET @numUnits = @numUnits - @onBackOrder                            SET @onBackOrder = 0                                                        IF (@onAllocation - @numUnits >= 0)								SET @onAllocation = @onAllocation - @numUnits								IF @bitAsset=1--Not Asset										BEGIN											  SET @onHand = @onHand +@numRentalIN+@numRentalLost+@numRentalOut     										END								ELSE										BEGIN											 SET @onHand = @onHand + @numUnits     										END                                                                                         END                                                                ELSE                         IF @numUnits < @onBackOrder                             BEGIN                  								IF (@onBackOrder - @numUnits >0)									SET @onBackOrder = @onBackOrder - @numUnits									--								IF @tintmode=1--									SET @onAllocation = @onAllocation + (@numUnits - @numQtyShipped)                            END                  							UPDATE  WareHouseItems						SET     numOnHand = @onHand ,								numAllocation = @onAllocation,								numBackOrder = @onBackOrder,								dtModified = GETDATE()						WHERE   numWareHouseItemID = @numWareHouseItemID       										                                                                                 				END								IF @numWareHouseItemID>0				BEGIN 						EXEC dbo.USP_ManageWareHouseItems_Tracking						@numWareHouseItemID = @numWareHouseItemID, --  numeric(9, 0)						@numReferenceID = @numOppId, --  numeric(9, 0)						@tintRefType = 3, --  tinyint						@vcDescription = @description, --  varchar(100)						@numModifiedBy = @numUserCntID,						@numDomainID = @numDomain 				END		          END                          IF @bitStockTransfer=1            BEGIN				SET @numWareHouseItemID = @numToWarehouseItemID;				SET @OppType = 2				SELECT  @onHand = ISNULL(numOnHand, 0),                    @onAllocation = ISNULL(numAllocation, 0),                    @onOrder = ISNULL(numOnOrder, 0),                    @onBackOrder = ISNULL(numBackOrder, 0)				FROM    WareHouseItems				WHERE   numWareHouseItemID = @numWareHouseItemID   			END                    IF @OppType = 2                     BEGIN 					SET @description='PO Deleted (Qty:' + CAST(@numUnits AS VARCHAR(10)) + ' Received:' +  CAST(@numUnitHourReceived AS VARCHAR(10)) + ')'						--Updating the Average Cost--                        IF @onHand + @onOrder - @numUnits <> 0 --                            BEGIN--                                SET @monAvgCost = ( ( @onHand + @onOrder )--                                                    * @monAvgCost - @monAmount )--                                    / ( @onHand + @onOrder - @numUnits )--                            END--                        ELSE --                            SET @monAvgCost = 0--                        UPDATE  item--                        SET     monAverageCost = @monAvgCost--                        WHERE   numItemCode = @itemcode										    --Partial Fulfillment						IF @tintmode=1 and  @onHand >= @numUnitHourReceived						BEGIN							SET @onHand= @onHand - @numUnitHourReceived							--SET @onOrder= @onOrder + @numUnitHourReceived						END											    SET @numUnits = @numUnits - @numUnitHourReceived 						IF (@onOrder - @numUnits)>=0						BEGIN							--Causing Negative Inventory Bug ID:494							SET @onOrder = @onOrder - @numUnits							END						ELSE IF (@onHand + @onOrder) - @numUnits >= 0						BEGIN													SET @onHand = @onHand - (@numUnits-@onOrder)							SET @onOrder = 0						END						ELSE IF  (@onHand + @onOrder + @onAllocation) - @numUnits >= 0						BEGIN							Declare @numDiff numeric								SET @numDiff = @numUnits - @onOrder							SET @onOrder = 0							SET @numDiff = @numDiff - @onHand							SET @onHand = 0							SET @onAllocation = @onAllocation - @numDiff							SET @onBackOrder = @onBackOrder + @numDiff						END					                            UPDATE  WareHouseItems                        SET     numOnHand = @onHand,                                numAllocation = @onAllocation,                                numBackOrder = @onBackOrder,                                numOnOrder = @onOrder,							dtModified = GETDATE()                        WHERE   numWareHouseItemID = @numWareHouseItemID                                                   EXEC dbo.USP_ManageWareHouseItems_Tracking						@numWareHouseItemID = @numWareHouseItemID, --  numeric(9, 0)						@numReferenceID = @numOppId, --  numeric(9, 0)						@tintRefType = 4, --  tinyint						@vcDescription = @description, --  varchar(100)						@numModifiedBy = @numUserCntID,						@numDomainID = @numDomain                                                              END                                                                               SELECT TOP 1                    @numoppitemtCode = numoppitemtCode,                    @itemcode = OI.numItemCode,                    @numUnits = numUnitHour,                    @numWareHouseItemID = ISNULL(numWarehouseItmsID,0),                    @Kit = ( CASE WHEN bitKitParent = 1                                       AND bitAssembly = 1 THEN 0                                  WHEN bitKitParent = 1 THEN 1                                  ELSE 0                             END ),                    @monAmount = ISNULL(monTotAmount,0)  * (CASE WHEN ISNULL(fltExchangeRate,0)=0 THEN 1 ELSE fltExchangeRate END),                    @monAvgCost = monAverageCost,                    @numQtyShipped = ISNULL(numQtyShipped,0),					@numUnitHourReceived = ISNULL(numUnitHourReceived,0),					@bitKitParent=ISNULL(bitKitParent,0),					@numToWarehouseItemID =OI.numToWarehouseItemID,					@bitStockTransfer = ISNULL(OM.bitStockTransfer,0),					@OppType = tintOppType            FROM    OpportunityItems OI					JOIN dbo.OpportunityMaster OM ON OI.numOppId = OM.numOppId                    JOIN Item I ON OI.numItemCode = I.numItemCode                                               WHERE   (charitemtype='P' OR 1=(CASE WHEN tintOppType=1 THEN 							CASE WHEN ISNULL(I.bitKitParent,0) = 1 AND ISNULL(I.bitAssembly,0) = 1 THEN 0								 WHEN ISNULL(I.bitKitParent,0) = 1 THEN 1								 ELSE 0 END 							ELSE 0 END))  							AND OI.numOppId = @numOppId                     AND OI.numoppitemtCode > @numoppitemtCode                    AND ( bitDropShip = 0                          OR bitDropShip IS NULL                        )            ORDER BY OI.numoppitemtCode                                                          IF @@rowcount = 0                 SET @numoppitemtCode = 0              END
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_SalesOrderConfiguration_Save' ) 
    DROP PROCEDURE USP_SalesOrderConfiguration_Save
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author: Sandeep Patel
-- Create date: 18 Feb 2014
-- Description:	Saves new sales order form configuration
-- =============================================
CREATE PROCEDURE USP_SalesOrderConfiguration_Save
	@numSOCID NUMERIC(18,0) = NULL,
	@numDomainID NUMERIC(18,0) = NULL,
	@bitAutoFocusCustomer BIT,
	@bitAutoFocusItem BIT,
	@bitDisplayRootLocation BIT,
	@bitAutoAssignOrder BIT,
	@bitDisplayLocation BIT,
	@bitDisplayFinancialStamp BIT,
	@bitDisplayItemDetails BIT,
	@bitDisplayUnitCost BIT,
	@bitDisplayProfitTotal BIT,
	@bitDisplayShippingRates BIT,
	@bitDisplayPaymentMethods BIT,
	@bitCreateOpenBizDoc BIT,
	@numListItemID NUMERIC(18,0),
	@vcPaymentMethodIDs VARCHAR(100),
	@bitDisplayCurrency BIT,
	@bitDisplayAssignTo BIT,
	@bitDisplayTemplate BIT,
	@bitDisplayCouponDiscount BIT,
	@bitDisplayShippingCharges BIT,
	@bitDisplayDiscount BIT,
	@bitDisplaySaveNew BIT,
	@bitDisplayAddToCart BIT,
	@bitDisplayCreateInvoice BIT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	IF (SELECT COUNT(*) FROM SalesOrderConfiguration WHERE numDomainID = @numDomainID) = 0
	BEGIN
		INSERT INTO dbo.SalesOrderConfiguration
		(
			numDomainID,
			bitAutoFocusCustomer,
			bitAutoFocusItem,
			bitDisplayRootLocation,
			bitAutoAssignOrder,
			bitDisplayLocation,
			bitDisplayFinancialStamp,
			bitDisplayItemDetails,
			bitDisplayUnitCost,
			bitDisplayProfitTotal,
			bitDisplayShippingRates,
			bitDisplayPaymentMethods,
			bitCreateOpenBizDoc,
			numListItemID,
			vcPaymentMethodIDs,
			bitDisplayCurrency,
			bitDisplayAssignTo,
			bitDisplayTemplate,
			bitDisplayCouponDiscount,
			bitDisplayShippingCharges,
			bitDisplayDiscount,
			bitDisplaySaveNew,
			bitDisplayAddToCart,
			bitDisplayCreateInvoice
		)
		VALUES
		(
			@numDomainID,
			@bitAutoFocusCustomer,
			@bitAutoFocusItem,
			@bitDisplayRootLocation,
			@bitAutoAssignOrder,
			@bitDisplayLocation,
			@bitDisplayFinancialStamp,
			@bitDisplayItemDetails,
			@bitDisplayUnitCost,
			@bitDisplayProfitTotal,
			@bitDisplayShippingRates,
			@bitDisplayPaymentMethods,
			@bitCreateOpenBizDoc,
			@numListItemID,
			@vcPaymentMethodIDs,
			@bitDisplayCurrency,
			@bitDisplayAssignTo,
			@bitDisplayTemplate,
			@bitDisplayCouponDiscount,
			@bitDisplayShippingCharges,
			@bitDisplayDiscount,
			@bitDisplaySaveNew,
			@bitDisplayAddToCart,
			@bitDisplayCreateInvoice
		)
	END
	ELSE
	BEGIN
		UPDATE
			dbo.SalesOrderConfiguration
		SET
			bitAutoFocusCustomer = @bitAutoFocusCustomer,
			bitAutoFocusItem = @bitAutoFocusItem,
			bitDisplayRootLocation = @bitDisplayRootLocation,
			bitAutoAssignOrder = @bitAutoAssignOrder,
			bitDisplayLocation = @bitDisplayLocation,
			bitDisplayFinancialStamp = @bitDisplayFinancialStamp,
			bitDisplayItemDetails = @bitDisplayItemDetails,
			bitDisplayUnitCost = @bitDisplayUnitCost,
			bitDisplayProfitTotal = @bitDisplayProfitTotal,
			bitDisplayShippingRates = @bitDisplayShippingRates,
			bitDisplayPaymentMethods = @bitDisplayPaymentMethods,
			bitCreateOpenBizDoc = @bitCreateOpenBizDoc,
			numListItemID = @numListItemID,
			vcPaymentMethodIDs  = @vcPaymentMethodIDs,
			bitDisplayCurrency = @bitDisplayCurrency,
			bitDisplayAssignTo = @bitDisplayAssignTo,
			bitDisplayTemplate = @bitDisplayTemplate,
			bitDisplayCouponDiscount = @bitDisplayCouponDiscount,
			bitDisplayShippingCharges = @bitDisplayShippingCharges,
			bitDisplayDiscount = @bitDisplayDiscount,
			bitDisplaySaveNew = @bitDisplaySaveNew,
			bitDisplayAddToCart = @bitDisplayAddToCart,
			bitDisplayCreateInvoice = @bitDisplayCreateInvoice
		WHERE
			numDomainID = @numDomainID
	END
    
END
GO


/****** Object:  StoredProcedure [dbo].[USP_GetWorkOrder]    Script Date: 07/26/2008 16:16:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
                    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_SitePages_GetByPageElementID')
DROP PROCEDURE USP_SitePages_GetByPageElementID
GO
CREATE PROCEDURE [dbo].[USP_SitePages_GetByPageElementID]                             
	@numDomainID NUMERIC (18,0),
	@numPageElementID NUMERIC (18,0),
	@numSiteID NUMERIC(18,0)                                                                     
AS                           
	DECLARE @vcPageElement VARCHAR(500) = ''
	SELECT @vcPageElement=ISNULL(vcTagName,'') FROM PageElementMaster WHERE numElementID =  @numPageElementID

	SELECT 
		* 
	FROM 
		SitePages 
	WHERE 
		numTemplateID IN (
							SELECT 
								numTemplateID 
							FROM 
								SiteTemplates 
							WHERE 
								numDomainID = @numDomainID AND
								numSiteID = @numSiteID AND 
								CONTAINS(txtTemplateHTML,@vcPageElement)
							)

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_SitePages_GetByTemplateID')
DROP PROCEDURE dbo.USP_SitePages_GetByTemplateID
GO
CREATE PROCEDURE [dbo].[USP_SitePages_GetByTemplateID]
	@numDomainID NUMERIC (18,0),
	@numTemplateID  NUMERIC (18,0),
	@numSiteID NUMERIC(18,0)
AS 
BEGIN

	DECLARE @vcTemplateCode VARCHAR(500)
	SELECT @vcTemplateCode='{template:' + REPLACE(LOWER([vcTemplateName]),' ','_') + '}' FROM SiteTemplates WHERE numTemplateID = @numTemplateID AND numDomainID = @numDomainID


	SELECT 
		* 
	FROM 
		SitePages 
	WHERE 
		numTemplateID IN (
							SELECT 
								numTemplateID 
							FROM 
								SiteTemplates 
							WHERE 
								numDomainID = @numDomainID AND
								numSiteID = @numSiteID AND 
								CONTAINS(txtTemplateHTML,@vcTemplateCode)
							)
	
END



/****** Object:  StoredProcedure [dbo].[USP_TicklerActItems]    Script Date: 07/26/2008 16:21:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- Created By Anoop jayaraj                                                        
                                                    
-- Modified by Tarun Juneja                                                    
-- date 26-08-2006                                                    
-- Reason:- Enhancement In Tickler                                                    
                                                                    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_tickleractitems')
DROP PROCEDURE usp_tickleractitems
GO
CREATE PROCEDURE [dbo].[USP_TicklerActItems]                                                                        
@numUserCntID as numeric(9)=null,                                                                        
@numDomainID as numeric(9)=null,                                                                        
@startDate as datetime,                                                                        
@endDate as datetime,                                                                    
@ClientTimeZoneOffset Int,  --Added by Debasish to enable calculation of date according to client machine                                                                      
@bitFilterRecord bit=0,
@columnName varchar(50),
@columnSortOrder varchar(50),
@vcBProcessValue varchar(500)
                                                  
As                                                                         
  
DECLARE @vcSelectedEmployess VARCHAR(100)
DECLARE @numCriteria INT
DECLARE @vcActionTypes VARCHAR(100)

SELECT 
	@vcSelectedEmployess = vcSelectedEmployee,
	@numCriteria = numCriteria,
	@vcActionTypes = vcActionTypes
FROM 
	TicklerListFilterConfiguration
WHERE
	numDomainID = @numDomainID AND
	numUserCntID = @numUserCntID


Create table #tempRecords (numContactID numeric(9),numDivisionID numeric(9),tintCRMType tinyint,
Id numeric(9),bitTask varchar(100),Startdate datetime,EndTime datetime,
itemDesc text,[Name] varchar(200),vcFirstName varchar(100),vcLastName varchar(100),numPhone varchar(15),numPhoneExtension varchar(7),
[Phone] varchar(30),vcCompanyName varchar(100),vcEmail varchar(50),Task varchar(100),Activity varchar(500),
Status varchar(100),numCreatedBy numeric(9),numTerId numeric(9),numAssignedTo varchar(1000),numAssignedBy varchar(50),
 caseid numeric(9),vcCasenumber varchar(50),casetimeId numeric(9),caseExpId numeric(9),type int,itemid varchar(50),
changekey varchar(50),DueDate varchar(50),bitFollowUpAnyTime bit,numNoOfEmployeesId varchar(50),vcComPhone varchar(50),vcColorScheme VARCHAR(50),
Slp_Name varchar(100),intTotalProgress INT,vcPoppName varchar(100), numOppId NUMERIC(18,0),numBusinessProcessID numeric(18,0) )                                                         
 
declare @strSql as varchar(8000)                                                            
declare @strSql1 as varchar(8000)                                                            
declare @strSql2 as varchar(8000)                                                            
 
   -------Change Row Color-------
Declare @vcCSOrigDbCOlumnName as varchar(50),@vcCSLookBackTableName as varchar(50),@vcCSAssociatedControlType as varchar(50)
SET @vcCSOrigDbCOlumnName=''
SET @vcCSLookBackTableName=''

--select top 1 @vcCSOrigDbCOlumnName=DFM.vcOrigDbCOlumnName,@vcCSLookBackTableName=DFM.vcLookBackTableName from DycFieldColorScheme DFCS join DycFieldMaster DFM on 
--DFCS.numModuleID=DFM.numModuleID and DFCS.numFieldID=DFM.numFieldID
--where DFCS.numDomainID=@numDomainID and DFM.numModuleID=1

Create table #tempColorScheme(vcOrigDbCOlumnName varchar(50),vcLookBackTableName varchar(50),vcFieldValue varchar(50),vcFieldValue1 varchar(50),vcColorScheme varchar(50),vcAssociatedControlType varchar(50))

insert into #tempColorScheme select DFM.vcOrigDbCOlumnName,DFM.vcLookBackTableName,DFCS.vcFieldValue,DFCS.vcFieldValue1,DFCS.vcColorScheme,DFFM.vcAssociatedControlType 
from DycFieldColorScheme DFCS join DycFormField_Mapping DFFM on DFCS.numFieldID=DFFM.numFieldID
join DycFieldMaster DFM on DFM.numModuleID=DFFM.numModuleID and DFM.numFieldID=DFFM.numFieldID
where DFCS.numDomainID=@numDomainID and DFFM.numFormID=43 AND DFCS.numFormID=43 and isnull(DFFM.bitAllowGridColor,0)=1

IF(SELECT COUNT(*) FROM #tempColorScheme)>0
BEGIN
   select top 1 @vcCSOrigDbCOlumnName=vcOrigDbCOlumnName,@vcCSLookBackTableName=vcLookBackTableName,@vcCSAssociatedControlType=vcAssociatedControlType from #tempColorScheme
END   
----------------------------                   


SET @strSql = 'insert into #tempRecords
Select * from (                                                    
 SELECT Addc.numContactID,Addc.numDivisionID,Div.tintCRMType,Comm.numCommId AS Id,convert(varchar(15),bitTask) as bitTask,                                               
 DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +', dtStartTime) as Startdate,                        
 DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +', dtEndTime) as EndTime ,                                                                                                                                                                  
 --DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +', dtStartTime) AS CloseDate,                                                                         
 Comm.textDetails AS itemDesc,                                                                                                               
 AddC.vcFirstName + '' '' + AddC.vcLastName  as [Name], 
 AddC.vcFirstName,AddC.vcLastName,Addc.numPhone,AddC.numPhoneExtension,                               
 case when Addc.numPhone<>'''' then + AddC.numPhone +case when AddC.numPhoneExtension<>'''' then '' - '' + AddC.numPhoneExtension else '''' end  else '''' end as [Phone],                                                                       
 Comp.vcCompanyName As vcCompanyName , --changed by bharat so that the complete name is displayed                                                                
 AddC.vcEmail As vcEmail ,                                                                         
-- case when Comm.bitTask=971 then  ''Communication'' when Comm.bitTask=2 then  ''Opportunity''       
--when Comm.bitTask = 972 then  ''Task'' when Comm.bitTask=973 then  ''Notes'' When Comm.bitTask = 974 then        
--''Follow-up'' else ''Unknown'' end         
dbo.GetListIemName(Comm.bitTask)AS Task,                
 listdetailsActivity.VcData As Activity ,                                                    
 listdetailsStatus.VcData As Status   ,  -- Priority column                                                                                             
 Comm.numCreatedBy,                                                                                                             
 Div.numTerId,                                           
-- case When Len( dbo.fn_GetContactName(numAssign) ) > 12                                           
--  Then Substring( dbo.fn_GetContactName(numAssign) , 0  ,  12 ) + ''..''                                           
--  Else dbo.fn_GetContactName(numAssign)                                           
-- end                                          
-- +                                          
-- '' / ''                                             
-- +                                              
-- case                                                
--  When Len( dbo.fn_GetContactName(Comm.numAssignedBy) ) > 12                                           
--  Then Substring( dbo.fn_GetContactName(Comm.numAssignedBy) , 0  ,  12 ) + ''..''                                        Else dbo.fn_GetContactName(Comm.numAssignedBy)                                          
-- end  

Substring( 
dbo.fn_GetContactName(numAssign) + 
ISNULL((SELECT SUBSTRING((SELECT '','' + dbo.fn_GetContactName(numContactId) FROM CommunicationAttendees WHERE numCommId=Comm.numCommId FOR XML PATH('''')),1,200000)),''''),0,50)
As numAssignedTo , 
dbo.fn_GetContactName(Comm.numAssignedBy) AS numAssignedBy,        
convert(varchar(50),comm.caseid) as caseid,                          
(select top 1 vcCaseNumber from cases where cases.numcaseid= comm.caseid )as vcCasenumber,                          
comm.casetimeId,                          
comm.caseExpId ,                        
0 as type   ,                    
'''' as itemid ,                    
'''' as changekey ,cast(Null as datetime) as DueDate ,bitFollowUpAnyTime,
dbo.GetListIemName(Comp.numNoOfEmployeesId) AS numNoOfEmployeesId,Div.vcComPhone'

IF len(@vcCSOrigDbCOlumnName)>0 and len(@vcCSLookBackTableName)>0
	set @strSql=@strSql+',tCS.vcColorScheme'
ELSE
	set @strSql=@strSql+','''' as vcColorScheme'

SET @strSql =@strSql + ' ,ISNULL(Slp_Name,'''') AS Slp_Name,ISNULL(intTotalProgress,0) AS [intTotalProgress],ISNULL(vcPoppName,'''') AS [vcPoppName], ISNULL(Opp.numOppId,0) AS [numOppId],ISNULL(Opp.numBusinessProcessID,0) AS numBusinessProcessID'

SET @strSql =@strSql + ' FROM  Communication Comm                              
 JOIN AdditionalContactsInformation AddC                                                  
 ON Comm.numContactId = AddC.numContactId                                  
 JOIN DivisionMaster Div                                                                         
 ON AddC.numDivisionId = Div.numDivisionID                                   
 JOIN CompanyInfo  Comp                                                                         
 ON Div.numCompanyID = Comp.numCompanyId                                                       
 Left Join listdetails listdetailsActivity                                                  
 On Comm.numActivity = listdetailsActivity.NumlistItemID                                               
 Left Join listdetails listdetailsStatus                                                 
 On Comm.numStatus = listdetailsStatus.NumlistItemID
 LEFT JOIN OpportunityMaster Opp ON Opp.numDivisionId = Div.numDivisionID AND Opp.numContactId = AddC.numContactId  AND Opp.numOppID = Comm.numOppID 
 LEFT JOIN Sales_process_List_Master splm ON splm.Slp_Id = Opp.numBusinessProcessID 
 LEFT JOIN ProjectProgress PP On PP.numOppId = Opp.numOppId '
 
 
DECLARE @Prefix AS VARCHAR(5)
 
 IF len(@vcCSOrigDbCOlumnName)>0 and len(@vcCSLookBackTableName)>0
BEGIN
	 if @vcCSLookBackTableName = 'AdditionalContactsInformation'                  
		set @Prefix = 'AddC.'                  
    else if @vcCSLookBackTableName = 'DivisionMaster'                  
		set @Prefix = 'Div.'
	else if @vcCSLookBackTableName = 'CompanyInfo'                  
		set @Prefix = 'Comp.'  
	ELSE IF @vcCSLookBackTableName='Tickler'	
		set @Prefix = 'Comm.'  
--	else if @vcCSLookBackTableName = 'ProjectProgress'                  
--		set @Prefix = 'pp.'  	
--	else if @vcCSLookBackTableName = 'OpportunityMaster'                  
--		set @Prefix = 'om.'  
--	else if @vcCSLookBackTableName = 'Sales_process_List_Master'                  
--		set @Prefix = 'splm.'  		
	else
		set @Prefix = ''   
	
	
	IF @vcCSAssociatedControlType='DateField'
	BEGIN
	IF @vcCSOrigDbCOlumnName='FormattedDate'
			SET @vcCSOrigDbCOlumnName='dtStartTime'
	
	
			set @strSql=@strSql+' left join #tempColorScheme tCS on CONVERT(VARCHAR(10),DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',' + @Prefix + @vcCSOrigDbCOlumnName + '),111) >= CONVERT(VARCHAR(10),DateAdd(day,cast(tCS.vcFieldValue as int),GETUTCDATE()),111)
			 and CONVERT(VARCHAR(10),DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',' + @Prefix + @vcCSOrigDbCOlumnName + '),111) <= CONVERT(VARCHAR(10),DateAdd(day,cast(tCS.vcFieldValue1 as int),GETUTCDATE()),111)'
	END
	ELSE IF @vcCSAssociatedControlType='SelectBox' OR @vcCSAssociatedControlType='ListBox' OR @vcCSAssociatedControlType='CheckBox'  OR @vcCSAssociatedControlType='TextBox'
	BEGIN
		PRINT 1
		set @strSql=@strSql+' left join #tempColorScheme tCS on tCS.vcFieldValue=' + @Prefix + @vcCSOrigDbCOlumnName
	END
END

set @strSql=@strSql+'  WHERE Comm.numDomainID = ' + Cast(@numDomainID as varchar(10)) 
 
 IF LEN(ISNULL(@vcBProcessValue,''))>0
BEGIN
	SET @strSql = @strSql + ' and ISNULL(Opp.numBusinessProcessID,0) in (SELECT Items FROM dbo.Split(''' +@vcBProcessValue + ''','',''))' 			
END

IF LEN(@vcActionTypes) > 0 
BEGIN
	SET @strSql = @strSql + ' AND Comm.bitTask IN (' + @vcActionTypes + ')'  
END
  
IF @numCriteria  = 1 --Action Items	Assigned
BEGIN
	set @strSql=@strSql+' AND 1=(Case when Comm.numAssign IN (' + CASE WHEN LEN(@vcSelectedEmployess) > 0 THEN @vcSelectedEmployess ELSE Cast(@numUserCntID as varchar(10)) END +') then 1 else 0 end)'
END
ELSE IF @numCriteria = 2 --Action Items Owned
BEGIN
	set @strSql=@strSql+' AND 1=(Case when Comm.numCreatedBy IN (' + CASE WHEN LEN(@vcSelectedEmployess) > 0 THEN @vcSelectedEmployess ELSE Cast(@numUserCntID as varchar(10)) END +' ) then 1 else 0 end)'
END
ELSE --Action Items Assigned Or Owned
BEGIN
	set @strSql=@strSql+' AND 1=(Case when (Comm.numAssign IN (' + CASE WHEN LEN(@vcSelectedEmployess) > 0 THEN @vcSelectedEmployess ELSE Cast(@numUserCntID as varchar(10)) END +')  or Comm.numCreatedBy IN (' + CASE WHEN LEN(@vcSelectedEmployess) > 0 THEN @vcSelectedEmployess ELSE Cast(@numUserCntID as varchar(10)) END +')) then 1 else 0 end)'
END
  
                                   
set @strSql=@strSql+' AND (DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',dtStartTime) >= '''+Cast(@startDate as varchar(30))+''' and DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +',dtStartTime) <= '''+Cast(@endDate as varchar(30))+''')                                                     
 AND Comm.bitclosedflag=0                                                                         
 and Comm.bitTask <> 973  ) As X '              

IF LEN(ISNULL(@vcBProcessValue,''))=0
BEGIN
	If LEN(ISNULL(@vcActionTypes,'')) = 0 OR CHARINDEX('98989898989898',@vcActionTypes) > 0
	BEGIN
		SET @strSql1 =  ' insert into #tempRecords 
select                          
 ACI.numContactID,ACI.numDivisionID,Div.tintCRMType,ac.activityid as id ,case when alldayevent = 1 then ''5'' else ''0'' end as bitTask,                        
DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +', startdatetimeutc) startdate,                        
DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +', dateadd(second,duration,startdatetimeutc)) as endtime,                        
activitydescription as itemdesc,                        
ACI.vcFirstName + '' '' + ACI.vcLastName name, 
 ACI.vcFirstName,ACI.vcLastName,ACI.numPhone,ACI.numPhoneExtension,                   
 case when ACI.numPhone<>'''' then + ACI.numPhone +case when ACI.numPhoneExtension<>'''' then '' - '' + ACI.numPhoneExtension else '''' end  else '''' end as [Phone],                        
 Substring ( Comp.vcCompanyName , 0 , 12 ) + ''..'' As vcCompanyName ,                      
vcEmail,                        
''Calendar'' as task,                        
[subject] as Activity,                        
case when importance =0 then ''Low'' when importance =1 then ''Normal'' when importance =2 then ''High'' end as status,                        
0 as numcreatedby,                        
0 as numterid,                        
''-/-'' as numAssignedTo, 
''-'' AS numAssignedBy,        
''0'' as caseid,                        
null as vccasenumber,                        
0 as casetimeid,                        
0 as caseexpid,                        
1 as type ,                    
isnull(itemid,'''') as itemid  ,                    
changekey  ,cast(Null as datetime) as DueDate,cast(0 as bit) as bitFollowUpAnyTime,
dbo.GetListIemName(Comp.numNoOfEmployeesId) AS numNoOfEmployeesId,Div.vcComPhone,'''' as vcColorScheme,'''',0,'''',0 AS [numOppId],0 AS numBusinessProcessID
from activity ac join             
activityresource ar on  ac.activityid=ar.activityid             
join resource res on ar.resourceid=res.resourceid                         
--join usermaster UM on um.numUserId = res.numUserCntId                
join AdditionalContactsInformation ACI on ACI.numContactId = res.numUserCntId                
 JOIN DivisionMaster Div                                                          
 ON ACI.numDivisionId = Div.numDivisionID                                   
 JOIN CompanyInfo  Comp                             
 ON Div.numCompanyID = Comp.numCompanyId                 
 where  res.numUserCntId = ' + Cast(@numUserCntID as varchar(10)) +'             
and ac.OriginalStartDateTimeUtc IS NULL            
and res.numDomainID = ' + Cast(@numDomainID as varchar(10)) +'  and 
(DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +', startdatetimeutc) >= '''+Cast(@startDate as varchar(30))+''' and DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +', startdatetimeutc) <= '''+Cast(@endDate as varchar(30))+''')
--(startdatetimeutc between dateadd(month,-1,getutcdate()) and dateadd(month,1,getutcdate())) 
and [status] <> -1                                                     
and ac.activityid not in 
(select distinct(numActivityId) from communication Comm where Comm.numDomainID = ' + Cast(@numDomainID as varchar(10)) +'  
AND 1=(Case ' + Cast(@bitFilterRecord as varchar(10)) +' when 1 then Case when Comm.numAssign =' + Cast(@numUserCntID as varchar(10)) +'  then 1 else 0 end 
		else  Case when (Comm.numAssign =' + Cast(@numUserCntID as varchar(10)) +'  or Comm.numCreatedBy=' + Cast(@numUserCntID as varchar(10)) +' ) then 1 else 0 end end)                                                                      
-- AND (dtStartTime >= '''+Cast(@startDate as varchar(30))+''' and dtStartTime <= '''+Cast(@endDate as varchar(30))+''') 
)
 Order by endtime'
	END

-- btDocType =2 for Document approval request, =1 for bizdoc
SET @strSql2 = ' insert into #tempRecords
select 
B.numContactID,B.numDivisionID,E.tintCRMType,CASE BA.btDocType WHEN 2 THEN BA.numOppBizDocsId WHEN 3 THEN BA.numOppBizDocsId ELSE A.numBizActionID END as ID,
convert(varchar(15),A.bitTask) as bitTask,
DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +', dtCreatedDate) as Startdate,                        
DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +', dtCreatedDate) as EndTime ,  
'''' as itemDesc,
vcFirstName + '' '' + vcLastName  as [Name],
B.vcFirstName , B.vcLastName ,B.numPhone,B.numPhoneExtension,  
case when B.numPhone<>'''' then + B.numPhone +case when B.numPhoneExtension<>'''' then '' - '' + B.numPhoneExtension else '''' end  else '''' end as [Phone],
vcCompanyName,
vcEmail,
CASE BA.btDocType WHEN 2  THEN  ''Document Approval Request'' ELSE ''BizDoc Approval Request'' END as Task,
dbo.GetListIemName(bitTask) as Activity,
'''' as Status,
A.numCreatedBy,
E.numTerId, 
case When Len( dbo.fn_GetContactName(numAssign) ) > 12                                           
  Then Substring( dbo.fn_GetContactName(numAssign) , 0  ,  12 ) + ''..''                                           
  Else dbo.fn_GetContactName(numAssign)                                           
 end                                          
 numAssignedTo ,  
 ''-'' AS numAssignedBy,        
''0'' as caseid,
''0'' as vcCasenumber,
0 as CaseTimeId,
0 as CaseExpId,
CASE BA.btDocType WHEN 2 THEN 2 ELSE 3 END as [type], 
'''' AS itemid  ,      
 '''' AS changekey ,   
dtCreatedDate as DueDate  ,cast(0 as bit) as bitFollowUpAnyTime,
dbo.GetListIemName(D.numNoOfEmployeesId) AS numNoOfEmployeesId,E.vcComPhone,'''' as vcColorScheme,'''',0,'''', 0 AS [numOppId],0 AS numBusinessProcessID
from BizDocAction A LEFT JOIN dbo.BizActionDetails BA ON BA.numBizActionId = A.numBizActionId, 
AdditionalContactsInformation B, 
CompanyInfo D,
DivisionMaster E
WHERE A.numContactId=b.numContactId and a.numDomainID=b.numDomainID  and 
D.numCompanyId=E.numCompanyId and 
D.numDomainID=a.numDomainID and 
E.numDivisionId=B.numDivisionId and 
a.numDomainID= ' + Cast(@numDomainID as varchar(10)) +' and a.numContactId = ' + Cast(@numUserCntID as varchar(10)) +'  and  
A.numStatus=0 
and (DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +', dtCreatedDate) >= '''+Cast(@startDate as varchar(30))+''' and DateAdd(minute, '+Cast(-@ClientTimeZoneOffset as varchar(10)) +', dtCreatedDate) <= '''+Cast(@endDate as varchar(30))+''')'

END


declare @Nocolumns as tinyint                
set @Nocolumns=0       
         
Select @Nocolumns=isnull(count(*),0) from View_DynamicColumns where numFormId=43 and numUserCntID=@numUserCntID and numDomainID=@numDomainID AND tintPageType=1 
  
  CREATE TABLE #tempForm (tintOrder TINYINT,vcDbColumnName nvarchar(50),vcFieldName nvarchar(50),vcAssociatedControlType nvarchar(50),
  vcListItemType char(3),numListID numeric(9),vcLookBackTableName varchar(50),bitCustomField BIT,
  numFieldId NUMERIC,bitAllowSorting BIT,bitAllowEdit BIT,bitIsRequired BIT,bitIsEmail BIT,bitIsAlphaNumeric BIT,bitIsNumeric BIT,
  bitIsLengthValidation BIT,intMaxLength int,intMinLength int,bitFieldMessage BIT,vcFieldMessage VARCHAR(500),ListRelID numeric(9),intColumnWidth NUMERIC(18,2))
                 
if @Nocolumns > 0            
begin      
INSERT INTO #tempForm
select tintRow+1 as tintOrder,vcDbColumnName,ISNULL(vcCultureFieldName,vcFieldName),                  
 vcAssociatedControlType,vcListItemType,numListID,vcLookBackTableName                                                 
,bitCustom,numFieldId,bitAllowSorting,bitAllowEdit,
bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage vcFieldMessage,ListRelID,intColumnWidth
 FROM View_DynamicColumns 
 where numFormId=43 and numUserCntID=@numUserCntID and numDomainID=@numDomainID AND tintPageType=1 
  AND ISNULL(bitSettingField,0)=1 AND ISNULL(bitCustom,0)=0
  order by tintOrder asc  
  
end            
else            
begin 

	--Check if Master Form Configuration is created by administrator if NoColumns=0
	DECLARE @IsMasterConfAvailable BIT = 0
	DECLARE @numUserGroup NUMERIC(18,0)

	SELECT @numUserGroup = numGroupID FROM UserMaster WHERE numUserDetailId = @numUserCntID 

	IF (SELECT COUNT(*) FROM BizFormWizardMasterConfiguration WHERE numDomainID = @numDomainID AND numFormID = 43 AND bitGridConfiguration = 1 AND numGroupID = @numUserGroup) > 0
	BEGIN
		SET @IsMasterConfAvailable = 1
	END

	--If MasterConfiguration is available then load it otherwise load default columns
	IF @IsMasterConfAvailable = 1
	BEGIN
		INSERT INTO #tempForm
		SELECT 
			(intRowNum + 1) as tintOrder, vcDbColumnName, ISNULL(vcCultureFieldName,vcFieldName), vcAssociatedControlType,
			vcListItemType,numListID,vcLookBackTableName, bitCustom,numFieldId,bitAllowSorting,bitAllowEdit,
			bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
			intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage vcFieldMessage,ListRelID,intColumnWidth
		FROM 
			View_DynamicColumnsMasterConfig
		WHERE
			View_DynamicColumnsMasterConfig.numFormId=43 AND 
			View_DynamicColumnsMasterConfig.numDomainID=@numDomainID AND 
			View_DynamicColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
			View_DynamicColumnsMasterConfig.bitGridConfiguration = 1 AND
			ISNULL(View_DynamicColumnsMasterConfig.bitSettingField,0)=1 AND 
			ISNULL(View_DynamicColumnsMasterConfig.bitCustom,0)=0
	ORDER BY 
		tintOrder ASC  
	END
	ELSE                                        
	BEGIN
		 INSERT INTO #tempForm
		select tintOrder,vcDbColumnName,ISNULL(vcCultureFieldName,vcFieldName),                  
		 vcAssociatedControlType,vcListItemType,numListID,vcLookBackTableName                                                 
		,bitCustom,numFieldId,bitAllowSorting,bitAllowEdit,
		bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
		intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage vcFieldMessage,ListRelID,intColumnWidth
		 FROM View_DynamicDefaultColumns
		 where numFormId=43 and bitDefault=1 AND ISNULL(bitSettingField,0)=1 AND numDomainID=@numDomainID
		order by tintOrder asc  
	END 
end                                            

DECLARE @strSql3 AS VARCHAR(8000)

SET @strSql3=   ' select * from #tempRecords order by ' + @columnName +' ' + @columnSortOrder  

print @strSql
exec (@strSql + @strSql1 + @strSql2 + @strSql3 )
drop table #tempRecords

SELECT * FROM #tempForm

DROP TABLE #tempForm
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_UpdateECampaignAlertStatus')
DROP PROCEDURE USP_UpdateECampaignAlertStatus
GO
CREATE PROCEDURE [dbo].[USP_UpdateECampaignAlertStatus]
    @numConECampDTLID NUMERIC(9),
    @tintDeliveryStatus TINYINT,
    @vcEmailLog VARCHAR(500) = NULL,
    @tintMode TINYINT = 0
AS 
    BEGIN
		DECLARE @Date AS DATETIME 
		SET @Date = GETUTCDATE()

        IF @tintMode = 0 
        BEGIN
            UPDATE  [ConECampaignDTL]
            SET     [vcEmailLog] = @vcEmailLog,
                    [tintDeliveryStatus] = @tintDeliveryStatus,
                    bitSend = 1,
                    bintSentON = @Date
            WHERE   [numConECampDTLID] = @numConECampDTLID
        END

        IF @tintMode = 1 
        BEGIN
            UPDATE  [ConECampaignDTL]
            SET     bitFollowUpStatus = 1
            WHERE   [numConECampDTLID] = @numConECampDTLID      
        END
			
        IF @tintMode = 2 --For Action Item
        BEGIN
            UPDATE  [ConECampaignDTL]
            SET     [vcEmailLog] = @vcEmailLog,
                    bitSend = 1,
					[tintDeliveryStatus] = @tintDeliveryStatus,
                    bintSentON = GETUTCDATE()
            WHERE   [numConECampDTLID] = @numConECampDTLID
        END
    END

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_UpdateInventoryAndTracking')
DROP PROCEDURE USP_UpdateInventoryAndTracking
GO
CREATE PROCEDURE [dbo].[USP_UpdateInventoryAndTracking]
@numOnHand AS NUMERIC(18,0),
@numOnAllocation AS NUMERIC(18,0),
@numOnBackOrder AS NUMERIC(18,0),
@numOnOrder AS NUMERIC(18,0),
@numWarehouseItemID AS NUMERIC(18,0),
@numReferenceID AS NUMERIC(18,0),
@tintRefType AS TINYINT,
@numDomainID AS NUMERIC(18,0),
@numUserCntID AS NUMERIC(18,0),
@Description AS VARCHAR(1000)
AS
BEGIN
BEGIN TRY 
BEGIN TRAN
	--UPDATE WAREHOUSE INVENTORY
	UPDATE  
		WareHouseItems
	SET     
		numOnHand = @numOnHand,
		numAllocation = @numOnAllocation,
		numBackOrder = @numOnBackOrder,
		numOnOrder = @numOnOrder,
		dtModified = GETDATE() 
	WHERE   
		numWareHouseItemID = @numWarehouseItemID 
			
	--UPDATE WAREHOUSE TRACKING
	EXEC dbo.USP_ManageWareHouseItems_Tracking
		@numWareHouseItemID = @numWarehouseItemID,
		@numReferenceID = @numReferenceID,
		@tintRefType = @tintRefType,
		@vcDescription = @Description, 
		@numModifiedBy = @numUserCntID,
		@numDomainID = @numDomainID           
COMMIT TRAN 
END TRY 
BEGIN CATCH		
    IF ( @@TRANCOUNT > 0 ) 
    BEGIN
        ROLLBACK TRAN
    END

	DECLARE @ErrorMessage NVARCHAR(4000);
    DECLARE @ErrorSeverity INT;
    DECLARE @ErrorState INT;

    SELECT @ErrorMessage = ERROR_MESSAGE(), @ErrorSeverity = ERROR_SEVERITY(), @ErrorState = ERROR_STATE();

    RAISERROR (@ErrorMessage, @ErrorSeverity, @ErrorState);
END CATCH	
END
--Created by Anoop Jayaraj

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_UpdatePOItemsForFulfillment')
DROP PROCEDURE USP_UpdatePOItemsForFulfillment
GO
CREATE PROCEDURE  USP_UpdatePOItemsForFulfillment
    @numQtyReceived NUMERIC(9),
    @numOppItemID NUMERIC(9),
    @vcError VARCHAR(200) = '' OUTPUT,
    @numUserCntID AS NUMERIC(9),
    @dtItemReceivedDate AS DATETIME 
AS 

--Transaction is invoked from ADO.net (Purchase Fulfillment) Do not add transaction code here.
-- BEGIN TRY
-- BEGIN TRANSACTION
			DECLARE @numDomain AS NUMERIC(18,0)
			DECLARE @onAllocation AS NUMERIC          
			DECLARE @numWarehouseItemID AS NUMERIC       
			DECLARE @numOldQtyReceived AS NUMERIC       
			DECLARE @numNewQtyReceived AS NUMERIC
			DECLARE @onHand AS NUMERIC         
			DECLARE @onOrder AS NUMERIC            
			DECLARE @onBackOrder AS NUMERIC              
			DECLARE @bitStockTransfer BIT
			DECLARE @numToWarehouseItemID NUMERIC
			DECLARE @numOppId NUMERIC
			DECLARE @numUnits NUMERIC
			DECLARE @monPrice AS MONEY 
			DECLARE @numItemCode NUMERIC 
		
			SELECT  @numWarehouseItemID = ISNULL([numWarehouseItmsID], 0),
					@numOldQtyReceived = ISNULL([numUnitHourReceived], 0),
					@bitStockTransfer = ISNULL(OM.bitStockTransfer,0),
					@numToWarehouseItemID= ISNULL(numToWarehouseItemID,0),
					@numOppId=OM.numOppId,
					@numUnits=OI.numUnitHour,
					@monPrice=isnull(monPrice,0) * (CASE WHEN ISNULL(OM.fltExchangeRate,0)=0 THEN 1 ELSE OM.fltExchangeRate END),
					@numItemCode=OI.numItemCode,
					@numDomain = OM.[numDomainId]
			FROM    [OpportunityItems] OI INNER JOIN dbo.OpportunityMaster OM
					ON OI.numOppId = OM.numOppId
			WHERE   [numoppitemtCode] = @numOppItemID
    
    
    
			IF @bitStockTransfer = 1 --added by chintan
			BEGIN
			--ship item from FROM warehouse
				declare @p3 varchar(500)
				set @p3=''
				exec USP_UpdateQtyShipped @numQtyReceived,@numOppItemID,@numUserCntID,@vcError=@p3 OUTPUT
				IF LEN(@p3)>0 
				BEGIN
					RAISERROR ( @p3,16, 1 )
					RETURN ;

				END
		
			-- Receive item from To Warehouse
			SET @numWarehouseItemID=@numToWarehouseItemID
    
		   END  
    
			DECLARE @numTotalQuantityReceived NUMERIC(18,0)
			SET @numTotalQuantityReceived = @numQtyReceived + @numOldQtyReceived ;
			SET @numNewQtyReceived = @numQtyReceived;
			DECLARE @description AS VARCHAR(100)
			SET @description='PO Qty Received (Qty:' + CAST(@numUnits AS VARCHAR(10)) + ' Received:' +  CAST(@numTotalQuantityReceived AS VARCHAR(10)) + ')'
			
		--    PRINT @numNewQtyReceived
			IF @numNewQtyReceived <= 0 
				RETURN 
  
					SELECT  @onHand = ISNULL(numOnHand, 0),
					@onAllocation = ISNULL(numAllocation, 0),
					@onOrder = ISNULL(numOnOrder, 0),
					@onBackOrder = ISNULL(numBackOrder, 0)
			FROM    WareHouseItems
			WHERE   numWareHouseItemID = @numWareHouseItemID
			
			           DECLARE @TotalOnHand AS NUMERIC;SET @TotalOnHand=0  
			           SELECT @TotalOnHand=SUM(ISNULL(numOnHand, 0)) FROM dbo.WareHouseItems WHERE numItemID=@numItemCode 
						
			           --Updating the Average Cost
			           DECLARE @monAvgCost AS MONEY 
			           SELECT  @monAvgCost = ISNULL(monAverageCost, 0) FROM    Item WHERE   numitemcode = @numItemCode  
    
  						SET @monAvgCost = ( ( @TotalOnHand * @monAvgCost )
                                             + (@numNewQtyReceived * @monPrice))
                            / ( @TotalOnHand + @numNewQtyReceived )
                            
                        UPDATE  item
                        SET     monAverageCost = @monAvgCost
                        WHERE   numItemCode = @numItemCode
    
			
  
		--	SELECT @onHand onHand,
		--            @onAllocation onAllocation,
		--            @onBackOrder onBackOrder,
		--            @onOrder onOrder
      
			IF @onOrder >= @numNewQtyReceived 
				BEGIN    
				PRINT '1 case'        
					SET @onOrder = @onOrder - @numNewQtyReceived             
					IF @onBackOrder >= @numNewQtyReceived 
						BEGIN            
							SET @onBackOrder = @onBackOrder - @numNewQtyReceived             
							SET @onAllocation = @onAllocation + @numNewQtyReceived             
						END            
					ELSE 
						BEGIN            
							SET @onAllocation = @onAllocation + @onBackOrder            
							SET @numNewQtyReceived = @numNewQtyReceived - @onBackOrder            
							SET @onBackOrder = 0            
							SET @onHand = @onHand + @numNewQtyReceived             
						END         
				END            
			ELSE IF @onOrder < @numNewQtyReceived 
				BEGIN            
				PRINT '2 case'        
					SET @onHand = @onHand + @onOrder
					SET @onOrder = @numNewQtyReceived - @onOrder
				END   

				SELECT @onHand onHand,
					@onAllocation onAllocation,
					@onBackOrder onBackOrder,
					@onOrder onOrder
                
			UPDATE  [OpportunityItems]
			SET     numUnitHourReceived = @numTotalQuantityReceived
			WHERE   [numoppitemtCode] = @numOppItemID

			UPDATE  WareHouseItems
			SET     numOnHand = @onHand,
					numAllocation = @onAllocation,
					numBackOrder = @onBackOrder,
					numOnOrder = @onOrder,dtModified = GETDATE() 
			WHERE   numWareHouseItemID = @numWareHouseItemID
    
  
	    UPDATE dbo.OpportunityMaster SET dtItemReceivedDate=@dtItemReceivedDate WHERE numOppId=@numOppId
		
		EXEC dbo.USP_ManageWareHouseItems_Tracking
			@numWareHouseItemID = @numWareHouseItemID, --  numeric(9, 0)
			@numReferenceID = @numOppId, --  numeric(9, 0)
			@tintRefType = 4, --  tinyint
			@vcDescription = @description, --  varchar(100)
			@numModifiedBy = @numUserCntID,
			@dtRecordDate =  @dtItemReceivedDate,
			@numDomainID = @numDomain
          
    
		--    BEGIN
		--        DECLARE @vcOppName VARCHAR(200)
		--        SELECT TOP 1
		--                @vcOppName = ISNULL([vcPOppName], '')
		--        FROM    [OpportunityMaster]
		--        WHERE   [numOppId] IN ( SELECT  [numOppId]
		--                                FROM    [OpportunityItems]
		--                                WHERE   [numoppitemtCode] = @numOppItemID )
		--        SET @vcError = 'There is not enough Qty on Allocation to save quantity Received for ' + @vcOppName
		--        RETURN
		--    END
		--     
		
--COMMIT
--END TRY
--BEGIN CATCH
--   --Whoops, there was an error
--  IF @@TRANCOUNT > 0
--     ROLLBACK

--  -- Raise an error with the details of the exception
--  DECLARE @ErrMsg nvarchar(4000), @ErrSeverity int
--  SELECT @ErrMsg = ERROR_MESSAGE(),
--         @ErrSeverity = ERROR_SEVERITY()

--  RAISERROR(@ErrMsg, @ErrSeverity, 1)
--END CATCH
GO

GO
/****** Object:  StoredProcedure [dbo].[USP_UpdatingInventory]    Script Date: 06/01/2009 23:48:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj       
--exec usp_updatinginventory 0,181193,1,0,1       
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_updatinginventory')
DROP PROCEDURE usp_updatinginventory
GO
CREATE PROCEDURE [dbo].[USP_UpdatingInventory]        
@byteMode as tinyint=0,  
@numWareHouseItemID as numeric(9)=0,  
@Units as INTEGER,
@numWOId AS NUMERIC(9)=0,
@numUserCntID AS NUMERIC(9)=0,
@numOppId AS NUMERIC(9)=0      
as      
  DECLARE @Description AS VARCHAR(200)
  DECLARE @numItemCode NUMERIC(18)
  DECLARE @numDomain AS NUMERIC(18,0)
  DECLARE @ParentWOID AS NUMERIC(18,0)

  SELECT @ParentWOID=numParentWOID FROM WorkOrder WHERE numWOId=@numWOId
  SELECT @numItemCode=numItemID,@numDomain = numDomainID from WareHouseItems where numWareHouseItemID = @numWareHouseItemID
  
if @byteMode=0  -- Aseeembly Item
begin  
	DECLARE @CurrentAverageCost MONEY
	DECLARE @TotalCurrentOnHand INT
	DECLARE @newAverageCost MONEY
	
	SELECT @CurrentAverageCost = monAverageCost FROM dbo.Item WHERE numItemCode =@numItemCode 
	SELECT @TotalCurrentOnHand = SUM(numOnHand) FROM dbo.WareHouseItems WHERE numItemID =@numItemCode 
	PRINT @CurrentAverageCost
	PRINT @TotalCurrentOnHand
	
	
	SELECT @newAverageCost = SUM((numQtyItemsReq * dbo.fn_UOMConversion(UOM.numUOMId,0,IDUOM.numDomainId,IDUOM.numUOMId)) * I.monAverageCost )
	FROM dbo.ItemDetails ID
	LEFT JOIN dbo.Item I ON I.numItemCode = ID.numChildItemID
	LEFT JOIN UOM ON UOM.numUOMId=i.numBaseUnit
	LEFT JOIN UOM IDUOM ON IDUOM.numUOMId=ID.numUOMId
	WHERE numItemKitID = @numItemCode
	PRINT @newAverageCost
	SET @newAverageCost = ((@CurrentAverageCost * @TotalCurrentOnHand)  + (@newAverageCost * @Units)) / (@TotalCurrentOnHand + @Units)
	
	PRINT @newAverageCost

	UPDATE item SET monAverageCost =@newAverageCost WHERE numItemCode = @numItemCode
	UPDATE WareHouseItems set numOnHand=ISNULL(numOnHand,0)+@Units,numOnOrder=ISNULL(numOnOrder,0)-@Units,dtModified = GETDATE()  where numWareHouseItemID=@numWareHouseItemID  
end  
else if @byteMode=1  --Aseembly Child item
begin  
	declare @onHand as numeric                                    
	declare @onOrder as numeric                                    
	declare @onBackOrder as numeric                                   
	declare @onAllocation as numeric    

	select                                     
		@onHand=isnull(numOnHand,0),                                    
		@onAllocation=isnull(numAllocation,0),                                    
		@onOrder=isnull(numOnOrder,0),                                    
		@onBackOrder=isnull(numBackOrder,0)                                     
	from 
		WareHouseItems 
	where 
		numWareHouseItemID=@numWareHouseItemID  

	--RELEASE QTY FROM ALLOCATION
	SET @onAllocation=@onAllocation-@Units

	 --UPDATE INVENTORY
	UPDATE 
		WareHouseItems 
	SET      
		numOnHand=@onHand,
		numOnOrder=@onOrder,                         
		numAllocation=@onAllocation,
		numBackOrder=@onBackOrder,
		dtModified = GETDATE()                                    
	WHERE 
		numWareHouseItemID=@numWareHouseItemID   
END

IF @numWOId>0
BEGIN
    
	DECLARE @numReferenceID NUMERIC(18,0)
	DECLARE @tintRefType NUMERIC(18,0)

	IF ISNULL(@numOppId,0)>0 --FROM SALES ORDER 
	BEGIN
		IF @byteMode = 0 --Aseeembly Item
			SET @Description = 'SO-WO Work Order Completed (Qty: ' + CONVERT(varchar(10),@Units) + ')'
		ELSE IF @byteMode =1 --Aseembly Child item
			IF ISNULL(@ParentWOID,0) = 0
			BEGIN
				SET @Description = 'Items Used In SO-WO (Qty: ' + CONVERT(varchar(10),@Units) + ')'
			END
			ELSE
			BEGIN
				SET @Description = 'Items Used In SO-WO Work Order (Qty: ' + CONVERT(varchar(10),@Units) + ')'
			END

		SET @numReferenceID = @numOppId
		SET @tintRefType = 3
	END
	ELSE --FROM CREATE ASSEMBLY SCREEN FROM ITEM
	BEGIN
		IF @byteMode = 0 --Aseeembly Item
			SET @Description = 'Work Order Completed (Qty: ' + CONVERT(varchar(10),@Units) + ')'
		ELSE IF @byteMode =1 --Aseembly Child item
			SET @Description = 'Items Used In Work Order (Qty: ' + CONVERT(varchar(10),@Units) + ')'

		SET @numReferenceID = @numWOId
		SET @tintRefType=2
	END

	EXEC dbo.USP_ManageWareHouseItems_Tracking
				@numWareHouseItemID = @numWareHouseItemID, --  numeric(9, 0)
				@numReferenceID = @numReferenceID, --  numeric(9, 0)
				@tintRefType = @tintRefType, --  tinyint
				@vcDescription = @Description, --  varchar(100)
				@numModifiedBy = @numUserCntID,
				@numDomainID = @numDomain
END
ELSE if @byteMode=0
BEGIN
DECLARE @desc AS VARCHAR(100)
SET @desc= 'Create Assembly:' + CONVERT(varchar(10),@Units) + ' Units';

  EXEC dbo.USP_ManageWareHouseItems_Tracking
				@numWareHouseItemID = @numWareHouseItemID, --  numeric(9, 0)
				@numReferenceID = @numItemCode, --  numeric(9, 0)
				@tintRefType = 1, --  tinyint
				@vcDescription = @desc, --  varchar(100)
				@numModifiedBy = @numUserCntID,
				@numDomainID = @numDomain
END
ELSE if @byteMode=1
BEGIN
SET @Description = 'Item Used in Assembly (Qty: ' + CONVERT(varchar(10),@Units) + ')' 

EXEC dbo.USP_ManageWareHouseItems_Tracking
				@numWareHouseItemID = @numWareHouseItemID, --  numeric(9, 0)
				@numReferenceID = @numItemCode, --  numeric(9, 0)
				@tintRefType = 1, --  tinyint
				@vcDescription = @Description, --  varchar(100)
				@numModifiedBy = @numUserCntID,
				@numDomainID = @numDomain


END 
/* FOR SO:Adds Qty on allocation and deducts from onhand*/
/* FOR PO:Adds Qty on Order and updates average cost */
--exec USP_UpdatingInventoryonCloseDeal 5833      
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_updatinginventoryonclosedeal')
DROP PROCEDURE usp_updatinginventoryonclosedeal
GO
CREATE PROCEDURE [dbo].[USP_UpdatingInventoryonCloseDeal]
@numOppID as numeric(9),
@numUserCntID AS NUMERIC(9)        
AS
BEGIN TRY
   BEGIN TRANSACTION      
		
		declare @tintOpptype as tinyint        
		declare @itemcode as numeric                                    
		declare @numUnits as numeric                                      
		declare @numoppitemtCode as numeric(9)         
		declare @numWareHouseItemID as numeric(9) 
		declare @numToWarehouseItemID as numeric(9) --to be used with stock transfer
		declare @monAmount as money 
		declare @bitStockTransfer as bit
		declare @QtyShipped as NUMERIC
		declare @QtyReceived as NUMERIC
		DECLARE @fltExchangeRate AS FLOAT 
		DECLARE @bitWorkOrder AS BIT

		DECLARE @numDomain AS NUMERIC(18,0)
		SELECT @numDomain = OM.numDomainID FROM [dbo].[OpportunityMaster] AS OM WHERE [OM].[numOppId] = @numOppID
		select @tintOpptype=tintOppType,@bitStockTransfer=ISNULL(bitStockTransfer,0),@fltExchangeRate=(CASE WHEN ISNULL(fltExchangeRate,0)=0 THEN 1 ELSE fltExchangeRate END) from OpportunityMaster where numOppId=@numOppID
               
		 select top 1 @numoppitemtCode=numoppitemtCode,@itemcode=OI.numItemCode,@numUnits=numUnitHour,@qtyShipped=ISNULL([numQtyShipped],0),@qtyReceived=ISNULL([numUnitHourReceived],0),        
		 @numWareHouseItemID=ISNULL(numWarehouseItmsID,0),@numToWarehouseItemID=numToWarehouseItemID,@monAmount=monTotAmount * @fltExchangeRate,@bitWorkOrder=OI.bitWorkOrder
		 FROM OpportunityItems OI join Item I                                                
		 on OI.numItemCode=I.numItemCode   and numOppId=@numOppId                                          
		 where (charitemtype='P' OR 1=(CASE WHEN @tintOpptype=1 THEN 
							CASE WHEN ISNULL(I.bitKitParent,0) = 1 AND ISNULL(I.bitAssembly,0) = 1 THEN 0
								 WHEN ISNULL(I.bitKitParent,0) = 1 THEN 1
								 ELSE 0 END 
							ELSE 0 END)) and (bitDropShip=0 or bitDropShip is null)
		 AND I.[numDomainID] = @numDomain
		 ORDER by OI.numoppitemtCode                                       
		 while @numoppitemtCode>0                                        
		  begin   
	
	
				IF @bitStockTransfer = 1
				BEGIN
					--Make inventory changes as if sales order was placed , using OpportunityItems.numWarehouseItmsID
					EXEC usp_ManageInventory @itemcode,@numWareHouseItemID,@monAmount,1,@numUnits,@QtyShipped,@qtyReceived,0,1,@numOppID,@numoppitemtCode,@numUserCntID,@bitWorkOrder
					--Make inventory changes as if purchase order was placed , using OpportunityItems.numToWarehouseItemID
					EXEC usp_ManageInventory @itemcode,@numToWarehouseItemID,@monAmount,2,@numUnits,@QtyShipped,@qtyReceived,0,1,@numOppID,@numoppitemtCode,@numUserCntID,@bitWorkOrder
				END
				ELSE
				BEGIN
					EXEC usp_ManageInventory @itemcode,@numWareHouseItemID,@monAmount,@tintOpptype,@numUnits,@QtyShipped,@qtyReceived,0,1,@numOppID,@numoppitemtCode,@numUserCntID,@bitWorkOrder
				END
                                                                                                                                      
		   select top 1 @numoppitemtCode=numoppitemtCode,@itemcode=OI.numItemCode,@numUnits=numUnitHour,@qtyShipped=ISNULL([numQtyShipped],0),@qtyReceived=ISNULL([numUnitHourReceived],0),        
		 @numWareHouseItemID=ISNULL(numWarehouseItmsID,0),@numToWarehouseItemID=numToWarehouseItemID,@monAmount=monTotAmount * @fltExchangeRate,@bitWorkOrder=OI.bitWorkOrder
			from OpportunityItems OI join Item I         
		   on OI.numItemCode=I.numItemCode and numOppId=@numOppId               
		   where (charitemtype='P' OR 1=(CASE WHEN @tintOpptype=1 THEN 
							CASE WHEN ISNULL(I.bitKitParent,0) = 1 AND ISNULL(I.bitAssembly,0) = 1 THEN 0
								 WHEN ISNULL(I.bitKitParent,0) = 1 THEN 1
								 ELSE 0 END 
							ELSE 0 END)) and OI.numoppitemtCode>@numoppitemtCode and (bitDropShip=0 or bitDropShip is null)  
			AND I.[numDomainID] = @numDomain
			order by OI.numoppitemtCode                                                
		   if @@rowcount=0 set @numoppitemtCode=0         
		  END

COMMIT
END TRY
BEGIN CATCH
  -- Whoops, there was an error
  IF @@TRANCOUNT > 0
     ROLLBACK

  -- Raise an error with the details of the exception
  DECLARE @ErrMsg nvarchar(4000), @ErrSeverity int
  SELECT @ErrMsg = ERROR_MESSAGE(),
         @ErrSeverity = ERROR_SEVERITY()

  RAISERROR(@ErrMsg, @ErrSeverity, 1)
END CATCH
        
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_ValidateeWorkOrderInventory')
DROP PROCEDURE usp_ValidateeWorkOrderInventory
GO
CREATE PROCEDURE [dbo].[usp_ValidateeWorkOrderInventory]
    (
      @numWOId as numeric(9)=0,
	  @numWOStatus as numeric(9)
    )
AS 
    BEGIN
   
IF @numWOStatus=23184 
BEGIN
	Select WD.numQtyItemsReq,i.vcItemName,isnull(WHI.numOnHand,0) numOnHand,isnull(WHI.numAllocation,0) numAllocation FROM WorkOrderDetails WD JOIN item i ON WD.numChildItemID=i.numItemCode
	join WareHouseItems WHI on WHI.numWareHouseItemID=WD.numWareHouseItemID  
	 WHERE numWOId=@numWOId AND i.charItemType IN ('P') and WD.numQtyItemsReq > isnull(WHI.numAllocation,0)
END

    END
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
         
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_WareHouseItmsDTL_TransferSerialLotNo')
DROP PROCEDURE USP_WareHouseItmsDTL_TransferSerialLotNo
GO
CREATE PROCEDURE [dbo].[USP_WareHouseItmsDTL_TransferSerialLotNo]            
@numOppID AS NUMERIC(18,0),
@numOppItemID AS numeric(18,0),
@numFormWarehouseID AS NUMERIC(18,0),
@numToWarehouseID AS NUMERIC(18,0),
@bitSerialized AS BIT,
@bitLotNo AS BIT     
AS
BEGIN TRY
BEGIN TRANSACTION 
	
	IF @bitSerialized = 1
	BEGIN
		--TRANSFER SERIAL NUMBERS TO SELECTED WAREHOUSE
		UPDATE 
			WareHouseItmsDTL 
		SET 
			numWareHouseItemID=@numToWarehouseID 
		WHERE 
			numWareHouseItmsDTLID IN 
			(
				SELECT 
					t1.numWarehouseItmsDTLID
				FROM 
					OppWarehouseSerializedItem t1
				WHERE 
					t1.numOppID=@numOppID AND
					t1.numOppItemID=@numOppItemID AND
					t1.numWarehouseItmsID = @numFormWarehouseID)
	END

	IF @bitLotNo = 1
	BEGIN
		DECLARE @TableLotNoSelected TABLE
		(
			ID INT IDENTITY(1,1),
			numWarehouseItmsDTLID NUMERIC(18,0),
			numOppID NUMERIC(18,0),
			numOppItemID NUMERIC(18,0),
			numWarehouseItmsID NUMERIC(18,0),
			numQty NUMERIC(18,0)
		)
		
		INSERT INTO 
			@TableLotNoSelected 
		SELECT 
			numWarehouseItmsDTLID,
			numOppID,
			numOppItemID,
			numWarehouseItmsID,
			numQty
		FROM 
			OppWarehouseSerializedItem 
		WHERE 
			numOppID=@numOppID 
			AND numOppItemID=@numOppItemID 
			AND numWarehouseItmsID = @numFormWarehouseID

		DECLARE @i INT = 1
		DECLARE @Count INT
		SELECT @Count=COUNT(*) FROM @TableLotNoSelected

		WHILE @i <= @Count
		BEGIN
			DECLARE @numWarehouseItmsDTLID AS NUMERIC(18,0)
			DECLARE @vcSerialNo AS VARCHAR(100)
			DECLARE @numQty AS INT

			SELECT @numQty=numQty,@numWarehouseItmsDTLID=numWarehouseItmsDTLID FROM @TableLotNoSelected WHERE ID= @i
			SELECT @vcSerialNo=vcSerialNo FROM WareHouseItmsDTL WHERE numWareHouseItmsDTLID=@numWarehouseItmsDTLID

			--IF SAME LOT NUMBER IS ALREADY AVAILABLE IN DESTINATION WAREHOUSE THEN UPDATE QUANITY ELSE CREATE NEW ENTRY
			IF EXISTS (SELECT * FROM WareHouseItmsDTL WHERE numWareHouseItemID = @numToWarehouseID AND vcSerialNo=@vcSerialNo)
			BEGIN
				UPDATE
					WareHouseItmsDTL
				SET
					numQty = ISNULL(numQty,0) + ISNULL(@numQty,0)
				WHERE
					numWareHouseItemID = @numToWarehouseID AND vcSerialNo=@vcSerialNo
			END
			ELSE
			BEGIN
				INSERT INTO WareHouseItmsDTL
				(
					numWareHouseItemID,
					vcSerialNo,
					numQty
				)
				VALUES
				(
					@numToWarehouseID,
					@vcSerialNo,
					@numQty
				)
			END

			--DECREASE QUANTITY IN FROM WAREHOUSE
			UPDATE WareHouseItmsDTL SET numQty=ISNULL(numQty,0)-ISNULL(@numQty,0) WHERE numWareHouseItmsDTLID=@numWarehouseItmsDTLID

			SET @i = @i + 1
		END 
	END


	--UPDATE TRANSFER COMPLETES SO USERS WILL BE ABLE TO USE SERIAL/LOT NUMBR OTHERWISE USER WILL NOT BE ABLE TO SELECT SERIAL/LOT NUMBR
	UPDATE OppWarehouseSerializedItem SET bitTransferComplete=1 WHERE numOppID=@numOppID AND numOppItemID=@numOppItemID AND numWarehouseItmsID = @numFormWarehouseID

COMMIT
END TRY
BEGIN CATCH
  -- Whoops, there was an error
  IF @@TRANCOUNT > 0
     ROLLBACK

  -- Raise an error with the details of the exception
  DECLARE @ErrMsg nvarchar(4000), @ErrSeverity int
  SELECT @ErrMsg = ERROR_MESSAGE(),
         @ErrSeverity = ERROR_SEVERITY()

  RAISERROR(@ErrMsg, @ErrSeverity, 1)
END CATCH

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
                    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_WorkOrder_InsertRecursive')
DROP PROCEDURE USP_WorkOrder_InsertRecursive
GO
CREATE PROCEDURE [dbo].[USP_WorkOrder_InsertRecursive]
	@numOppID AS NUMERIC(9)=0,
	@numItemCode AS NUMERIC(18,0),
	@numQty AS NUMERIC(18,0),
	@numWarehouseItemID AS NUMERIC(18,0),
	@numDomainID AS NUMERIC(18,0),
	@numUserCntID AS NUMERIC(18,0)=0,
	@numQtyShipped AS NUMERIC(18,0),
	@numParentWOID AS NUMERIC(18,0),
	@bitFromWorkOrderScreen AS BIT
AS
BEGIN
	DECLARE @numWOID NUMERIC(18,0)
	DECLARE @vcInstruction VARCHAR(2000)
	DECLARE @numAssignedTo NUMERIC(18,0)
	DECLARE @bintCompletionDate DATETIME

	SELECT @vcInstruction=vcInstruction,@numAssignedTo=numAssignedTo,@bintCompletionDate=bintCompliationDate FROM WorkOrder WHERE numWOId=@numParentWOID

	INSERT INTO WorkOrder
	(
		numItemCode,numQtyItemsReq,numWareHouseItemId,numCreatedBy,bintCreatedDate,numDomainID,numWOStatus,numOppId, numParentWOID,vcInstruction,numAssignedTo,bintCompliationDate
	)
	VALUES
	(
		@numItemCode,@numQty,@numWarehouseItemID,@numUserCntID,getutcdate(),@numDomainID,0,@numOppID,@numParentWOID,@vcInstruction,@numAssignedTo,@bintCompletionDate
	)

	SELECT @numWOID = SCOPE_IDENTITY()

	INSERT INTO [WorkOrderDetails] 
	(
		numWOId,numItemKitID,numChildItemID,numQtyItemsReq,numWareHouseItemId,vcItemDesc,sintOrder,numQtyItemsReq_Orig,numUOMId
	)
	SELECT 
		@numWOId,numItemKitID,numItemCode,
		CAST((DTL.numQtyItemsReq * @numQty)AS NUMERIC(9,0)),
		isnull(Dtl.numWareHouseItemId,0),
		ISNULL(Dtl.vcItemDesc,txtItemDesc),
		ISNULL(sintOrder,0),
		DTL.numQtyItemsReq,
		Dtl.numUOMId 
	FROM 
		item                                
	INNER JOIN 
		ItemDetails Dtl 
	ON 
		numChildItemID=numItemCode
	WHERE 
		numItemKitID=@numItemCode

	DECLARE @Description AS VARCHAR(1000)

	--UPDATE ON ORDER OF ASSEMBLY
	UPDATE 
		WareHouseItems
	SET    
		numOnOrder= numOnOrder + @numQty,
		dtModified = GETDATE() 
	WHERE   
		numWareHouseItemID = @numWareHouseItemID 

	IF ISNULL(@bitFromWorkOrderScreen,0) = 0
	BEGIN
		
		SET @Description='SO-WO Work Order Created (Qty:' + CAST(@numQty AS VARCHAR(10)) + ')'

		--MAKE ENTRY IN WAREHOUSE TRACKING TABLE
		EXEC dbo.USP_ManageWareHouseItems_Tracking
		@numWareHouseItemID = @numWareHouseItemID, --  numeric(9, 0)
		@numReferenceID = @numOppID, --  numeric(9, 0)
		@tintRefType = 3, --  tinyint
		@vcDescription = @Description, --  varchar(100)
		@numModifiedBy = @numUserCntID,
		@numDomainID = @numDomainID

		EXEC USP_ManageInventoryAssemblyWO @numUserCntID,@numOppID,@numItemCode,@numWarehouseItemID,@numQtyShipped,1
	END
	ELSE IF ISNULL(@bitFromWorkOrderScreen,0) = 1
	BEGIN
		SET @Description='Work Order Created (Qty:' + CAST(@numQty AS VARCHAR(10)) + ')'

		--MAKE ENTRY IN WAREHOUSE TRACKING TABLE
		EXEC dbo.USP_ManageWareHouseItems_Tracking
		@numWareHouseItemID = @numWareHouseItemID, --  numeric(9, 0)
		@numReferenceID = @numWOId, --  numeric(9, 0)
		@tintRefType = 2, --  tinyint
		@vcDescription = @Description, --  varchar(100)
		@numModifiedBy = @numUserCntID,
		@numDomainID = @numDomainID

		EXEC USP_ManageInventoryWorkOrder @numWOID,@numDomainID,@numUserCntID
	END
END


GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetOrganizationTransaction')
DROP PROCEDURE USP_GetOrganizationTransaction
GO
CREATE PROCEDURE [dbo].[USP_GetOrganizationTransaction]
    (
      @numDivisionID NUMERIC(9)=0,
      @numDomainID NUMERIC,
      @CurrentPage INT=0,
	  @PageSize INT=0,
      @TotRecs INT=0  OUTPUT,
      @vcTransactionType AS VARCHAR(500),
      @dtFromDate DATETIME,
      @dtToDate DATETIME,
      @ClientTimeZoneOffset INT,
	  @SortCreatedDate TINYINT
    )
AS 
    BEGIN
		
		SELECT Items INTO #temp FROM dbo.Split(@vcTransactionType,',')

		CREATE TABLE #tempTransaction(intTransactionType INT,vcTransactionType VARCHAR(30),dtCreatedDate DATETIME,numRecordID NUMERIC,
		vcRecordName VARCHAR(100),monTotalAmount MONEY,monPaidAmount MONEY,dtDueDate DATETIME,vcMemo VARCHAR(1000),vcDescription VARCHAR(500),
		vcStatus VARCHAR(100),numRecordID2 NUMERIC,numStatus NUMERIC, vcBizDocId VARCHAR(1000))
		 
		 --Sales Order
		 IF EXISTS (SELECT 1 FROM #temp WHERE Items=1)
		 BEGIN
		 	INSERT INTO #tempTransaction 
		 	SELECT 1,'Sales Order',dbo.FormatedDateFromDate(OM.bintCreatedDate,OM.numDomainId),OM.numOppId,
		 	OM.vcPOppName,
		 	[dbo].[getdealamount](OM.numOppId,Getutcdate(),0),
		 	ISNULL((SELECT SUM(ISNULL(monAmountPaid,0)) FROM   OpportunityBizDocs BD WHERE  BD.numOppId = OM.numOppId AND ISNULL(bitAuthoritativeBizDocs,0)=1),0),
		 	NULL,'','',dbo.GetListIemName(numStatus),0,numStatus,
		 	ISNULL(OM.vcOppRefOrderNo,'')
		 	 FROM OpportunityMaster OM where OM.numDomainID = @numDomainID AND OM.numDivisionId=@numDivisionId
                        AND OM.tintOppType = 1  AND OM.tintOppStatus =  1
                        AND ((dateadd(minute,-@ClientTimeZoneOffset,OM.bintCreatedDate) between @dtFromDate And @dtToDate))
		 END
		 
		 --Sales Order Invoices(BizDocs)
		 IF EXISTS (SELECT 1 FROM #temp WHERE Items=2)
		 BEGIN
		 	INSERT INTO #tempTransaction 
		 	SELECT 2,'SO Invoice',dbo.FormatedDateFromDate(OBD.dtCreatedDate,OM.numDomainId),OBD.numOppBizDocsId,
		 	OBD.vcBizDocID,[dbo].[getdealamount](OM.numOppId,Getutcdate(),OBD.numOppBizDocsId),
		 	ISNULL(OBD.monAmountPaid,0),
		 	NULL,'','',dbo.GetListIemName(OBD.numBizDocStatus),OM.numOppId,numBizDocStatus,
		 	ISNULL(vcRefOrderNo,'')
		 	 FROM OpportunityMaster OM INNER JOIN OpportunityBizDocs OBD ON OBD.numOppId = OM.numOppId 
		 	 where OM.numDomainID = @numDomainID AND OM.numDivisionId=@numDivisionId
                        AND OM.tintOppType = 1  AND OM.tintOppStatus =  1 AND OBD.[bitAuthoritativeBizDocs] =1
                        AND ((dateadd(minute,-@ClientTimeZoneOffset,OBD.dtCreatedDate) between @dtFromDate And @dtToDate))
        END
		 
		 --Sales Opportunity
		  IF EXISTS (SELECT 1 FROM #temp WHERE Items=3)
		 BEGIN
		 	INSERT INTO #tempTransaction 
		 	SELECT 3,'Sales Opportunity',dbo.FormatedDateFromDate(OM.bintCreatedDate,OM.numDomainId),OM.numOppId,
		 	OM.vcPOppName,[dbo].[getdealamount](OM.numOppId,Getutcdate(),0),
		 	ISNULL((SELECT SUM(ISNULL(monAmountPaid,0)) FROM   OpportunityBizDocs BD WHERE  BD.numOppId = OM.numOppId AND ISNULL(bitAuthoritativeBizDocs,0)=1),0),
		 	NULL,'','',dbo.GetListIemName(numStatus),0,numStatus,''
		 	 FROM OpportunityMaster OM where OM.numDomainID = @numDomainID AND OM.numDivisionId=@numDivisionId
                        AND OM.tintOppType = 1  AND OM.tintOppStatus =  0
                        AND ((dateadd(minute,-@ClientTimeZoneOffset,OM.bintCreatedDate) between @dtFromDate And @dtToDate))
		 END
		 
		 --Purchase Order
		  IF EXISTS (SELECT 1 FROM #temp WHERE Items=4)
		 BEGIN
		 	INSERT INTO #tempTransaction 
		 	SELECT 4,'Purchase Order',dbo.FormatedDateFromDate(OM.bintCreatedDate,OM.numDomainId),OM.numOppId,
		 	OM.vcPOppName,
		 	[dbo].[getdealamount](OM.numOppId,Getutcdate(),0),
		 	ISNULL((SELECT SUM(ISNULL(monAmountPaid,0)) FROM   OpportunityBizDocs BD WHERE  BD.numOppId = OM.numOppId AND ISNULL(bitAuthoritativeBizDocs,0)=1),0),
		 	NULL,'','',dbo.GetListIemName(numStatus),0,numStatus,
		 	ISNULL(OM.vcOppRefOrderNo,'')
		 	 FROM OpportunityMaster OM where OM.numDomainID = @numDomainID AND OM.numDivisionId=@numDivisionId
                        AND OM.tintOppType = 2  AND OM.tintOppStatus =  1
                        AND ((dateadd(minute,-@ClientTimeZoneOffset,OM.bintCreatedDate) between @dtFromDate And @dtToDate))
		 END
		 
		 --Purchase Order Bill(BizDocs)
		  IF EXISTS (SELECT 1 FROM #temp WHERE Items=5)
		 BEGIN
		 	INSERT INTO #tempTransaction 
		 	SELECT 5,'PO Bill',dbo.FormatedDateFromDate(OBD.dtCreatedDate,OM.numDomainId),OBD.numOppBizDocsId,
		 	OBD.vcBizDocID,[dbo].[getdealamount](OM.numOppId,Getutcdate(),OBD.numOppBizDocsId),
		 	ISNULL(OBD.monAmountPaid,0),
		 	NULL,'','',dbo.GetListIemName(OBD.numBizDocStatus),OM.numOppId,numBizDocStatus,
		 	ISNULL(vcRefOrderNo,'')
--		 	(SELECT SUBSTRING((SELECT vcBizDocID + ',' FROM OpportunityBizDocs 
--														   WHERE numOppId = OM.numOppId
--														   AND ISNULL(bitAuthoritativeBizDocs,0) = 1 
--										 FOR XML PATH('')), 0, 200000))
		 	 FROM OpportunityMaster OM INNER JOIN OpportunityBizDocs OBD ON OBD.numOppId = OM.numOppId 
		 	 where OM.numDomainID = @numDomainID AND OM.numDivisionId=@numDivisionId
                        AND OM.tintOppType = 2  AND OM.tintOppStatus =  1 AND OBD.[bitAuthoritativeBizDocs] =1
                        AND ((dateadd(minute,-@ClientTimeZoneOffset,OBD.dtCreatedDate) between @dtFromDate And @dtToDate))
		 END
		 
		 --Purchase Opportunity
		  IF EXISTS (SELECT 1 FROM #temp WHERE Items=6)
		 BEGIN
		 	INSERT INTO #tempTransaction 
		 	SELECT 6,'Purchase Opportunity',dbo.FormatedDateFromDate(OM.bintCreatedDate,OM.numDomainId),OM.numOppId,
		 	OM.vcPOppName,[dbo].[getdealamount](OM.numOppId,Getutcdate(),0),
		 	ISNULL((SELECT SUM(ISNULL(monAmountPaid,0)) FROM   OpportunityBizDocs BD WHERE  BD.numOppId = OM.numOppId AND ISNULL(bitAuthoritativeBizDocs,0)=1),0),
		 	NULL,'','',dbo.GetListIemName(numStatus),0,numStatus,''
		 	 FROM OpportunityMaster OM where OM.numDomainID = @numDomainID AND OM.numDivisionId=@numDivisionId
                        AND OM.tintOppType = 2  AND OM.tintOppStatus =  0
                        AND ((dateadd(minute,-@ClientTimeZoneOffset,OM.bintCreatedDate) between @dtFromDate And @dtToDate))
		 END
		 
		 --Sales Return
		  IF EXISTS (SELECT 1 FROM #temp WHERE Items=7)
		 BEGIN
		 	INSERT INTO #tempTransaction 
		 	SELECT 7,'Sales Return',dbo.FormatedDateFromDate(RH.dtCreatedDate,RH.numDomainId),RH.numReturnHeaderID,
		 	RH.vcRMA,ISNULL(RH.monAmount,0),
		 	CASE ISNULL(RH.tintReceiveType,0) WHEN 1 THEN ISNULL((SELECT ISNULL(SUM(CH.monAmount),0) FROM dbo.CheckHeader CH WHERE CH.numDomainID=1 AND CH.tintReferenceType=10 AND CH.numReferenceID=RH.numReturnHeaderID AND ISNULL(CH.bitIsPrint,0)=1),0)
		 		 	WHEN 2 THEN ISNULL((SELECT SUM(ISNULL(monAppliedAmount,0)) AS monAmtPaid FROM DepositMaster DM WHERE DM.numDomainID=RH.numDomainID AND DM.numReturnHeaderID=RH.numReturnHeaderID),0)
		 		 	ELSE 0 END,
		 	NULL,'','',dbo.GetListIemName(RH.numReturnStatus),0,RH.numReturnStatus,''
		 	 FROM ReturnHeader RH where RH.numDomainID = @numDomainID AND RH.numDivisionId=@numDivisionId
                        AND RH.tintReturnType = 1
                        AND ((dateadd(minute,-@ClientTimeZoneOffset,RH.dtCreatedDate) between @dtFromDate And @dtToDate))
		 END
		 
		 --Purchase Return
		  IF EXISTS (SELECT 1 FROM #temp WHERE Items=8)
		 BEGIN
		 	INSERT INTO #tempTransaction 
		 	SELECT 8,'Purchase Return',dbo.FormatedDateFromDate(RH.dtCreatedDate,RH.numDomainId),RH.numReturnHeaderID,
		 	RH.vcRMA,ISNULL(RH.monAmount,0),
		 	ISNULL((SELECT SUM(ISNULL(monAppliedAmount,0)) AS monAmtPaid FROM BillPaymentHeader BPH WHERE BPH.numDomainID=RH.numDomainID AND BPH.numReturnHeaderID=RH.numReturnHeaderID),0),
		 	NULL,'','',dbo.GetListIemName(RH.numReturnStatus),0,RH.numReturnStatus,''
		 	 FROM ReturnHeader RH where RH.numDomainID = @numDomainID AND RH.numDivisionId=@numDivisionId
                        AND RH.tintReturnType = 2
                        AND ((dateadd(minute,-@ClientTimeZoneOffset,RH.dtCreatedDate) between @dtFromDate And @dtToDate))

		 END
		 
		 --Credit Memo
		  IF EXISTS (SELECT 1 FROM #temp WHERE Items=9)
		 BEGIN
		 	INSERT INTO #tempTransaction 
		 	SELECT 9,'Credit Memo',dbo.FormatedDateFromDate(RH.dtCreatedDate,RH.numDomainId),RH.numReturnHeaderID,
		 	RH.vcRMA,ISNULL(RH.monAmount,0),
		 	ISNULL((SELECT SUM(ISNULL(monAppliedAmount,0)) AS monAmtPaid FROM DepositMaster DM WHERE DM.numDomainID=RH.numDomainID AND DM.numReturnHeaderID=RH.numReturnHeaderID),0),
		 	NULL,'','',dbo.GetListIemName(RH.numReturnStatus),0,RH.numReturnStatus,''
		 	 FROM ReturnHeader RH where RH.numDomainID = @numDomainID AND RH.numDivisionId=@numDivisionId
                        AND RH.tintReturnType = 3
                        AND ((dateadd(minute,-@ClientTimeZoneOffset,RH.dtCreatedDate) between @dtFromDate And @dtToDate))
		 END
		 
		 --Refund Receipt
		  IF EXISTS (SELECT 1 FROM #temp WHERE Items=10)
		 BEGIN
		 	INSERT INTO #tempTransaction 
		 	SELECT 10,'Refund Receipt',dbo.FormatedDateFromDate(RH.dtCreatedDate,RH.numDomainId),RH.numReturnHeaderID,
		 	RH.vcRMA,ISNULL(RH.monAmount,0),
		 	ISNULL((SELECT ISNULL(SUM(CH.monAmount),0) FROM dbo.CheckHeader CH WHERE CH.numDomainID=1 AND CH.tintReferenceType=10 AND CH.numReferenceID=RH.numReturnHeaderID AND ISNULL(CH.bitIsPrint,0)=1),0),
		 	NULL,'','',dbo.GetListIemName(RH.numReturnStatus),0,RH.numReturnStatus,''
		 	 FROM ReturnHeader RH where RH.numDomainID = @numDomainID AND RH.numDivisionId=@numDivisionId
                        AND RH.tintReturnType = 4
                        AND ((dateadd(minute,-@ClientTimeZoneOffset,RH.dtCreatedDate) between @dtFromDate And @dtToDate))
		 END
		 
		 --Bills
		  IF EXISTS (SELECT 1 FROM #temp WHERE Items=11)
		 BEGIN
		 	INSERT INTO #tempTransaction 
		 	SELECT 11,'Bills',dbo.FormatedDateFromDate(BH.dtCreatedDate,BH.numDomainId),BH.numBillID,
		 	'Bill-' + CAST(BH.numBillID AS VARCHAR(18)),ISNULL(BH.monAmountDue,0),
		 	ISNULL(BH.monAmtPaid,0),
		 	NULL,BH.vcMemo,BH.vcReference,'',0,0,''
		 	 FROM BillHeader BH where BH.numDomainID = @numDomainID AND BH.numDivisionId=@numDivisionId
                        AND ((dateadd(minute,-@ClientTimeZoneOffset,BH.dtCreatedDate) between @dtFromDate And @dtToDate))
						AND ISNULL(BH.[bitLandedCost],0) = 0

		 END
		 
		  --Bills Landed Cost
		  IF EXISTS (SELECT 1 FROM #temp WHERE Items=16)
		 BEGIN
		 	INSERT INTO #tempTransaction 
		 	SELECT 16,'Landed Cost',dbo.FormatedDateFromDate(BH.dtCreatedDate,BH.numDomainId),BH.[numOppId],
		 	'Landed Cost Bill-' + CAST(BH.numBillID AS VARCHAR(18)),ISNULL(BH.monAmountDue,0),
		 	ISNULL(BH.monAmtPaid,0),
		 	NULL,BH.vcMemo,BH.vcReference,'',0,0,''
		 	 FROM BillHeader BH where BH.numDomainID = @numDomainID AND BH.numDivisionId=@numDivisionId
                        AND ((dateadd(minute,-@ClientTimeZoneOffset,BH.dtCreatedDate) between @dtFromDate And @dtToDate))
						AND ISNULL(BH.[bitLandedCost],0) = 1
		 END

		 --UnApplied Bill Payments
		  IF EXISTS (SELECT 1 FROM #temp WHERE Items=12)
		 BEGIN
		 	INSERT INTO #tempTransaction 
		 	SELECT 12,'Unapplied Bill Payments',dbo.FormatedDateFromDate(BPH.dtCreateDate,BPH.numDomainId),BPH.numBillPaymentID,
		 	CASE WHEN ISNULL(numReturnHeaderID,0)>0 THEN 'Return Credit #' + CAST(numReturnHeaderID AS VARCHAR(20)) ELSE 'Unapplied Bill Payment' END
			,ISNULL(BPH.monPaymentAmount,0),
		 	ISNULL(BPH.monAppliedAmount,0),
		 	NULL,'','','',ISNULL(numReturnHeaderID,0),0,''
		 	 FROM BillPaymentHeader BPH where BPH.numDomainID = @numDomainID AND BPH.numDivisionId=@numDivisionId
                        AND ((dateadd(minute,-@ClientTimeZoneOffset,BPH.dtCreateDate) between @dtFromDate And @dtToDate))
                        AND (ISNULL(monPaymentAmount,0) - ISNULL(monAppliedAmount,0)) > 0
		 END
		 
		 --Checks
		  IF EXISTS (SELECT 1 FROM #temp WHERE Items=13)
		 BEGIN
		 	INSERT INTO #tempTransaction 
		 	SELECT 13,'Checks',dbo.FormatedDateFromDate(CH.dtCreatedDate,CH.numDomainId),CH.numCheckHeaderID,
		 	CASE CH.tintReferenceType WHEN 1 THEN 'Checks' 
				  WHEN 8 THEN 'Bill Payment'
				  WHEN 10 THEN 'RMA'
				  WHEN 11 THEN 'Payroll'
				  ELSE 'Check' END + CASE WHEN ISNULL(CH.numCheckNo,0) > 0 THEN ' - #' + CAST(CH.numCheckNo AS VARCHAR(18)) ELSE '' END 
			,ISNULL(CH.monAmount,0),
		 	CASE WHEN ISNULL(bitIsPrint,0)=1 THEN ISNULL(CH.monAmount,0) ELSE 0 END,
		 	NULL,CH.vcMemo,'','',0,0,''
		 	 FROM dbo.CheckHeader CH where CH.numDomainID = @numDomainID AND CH.numDivisionId=@numDivisionId
		 				--AND ch.tintReferenceType=1
                        AND ((dateadd(minute,-@ClientTimeZoneOffset,CH.dtCreatedDate) between @dtFromDate And @dtToDate))

		 END
		 
		 --Deposits
		  IF EXISTS (SELECT 1 FROM #temp WHERE Items=14)
		 BEGIN
		 	INSERT INTO #tempTransaction 
		 	SELECT 14,'Deposits',dbo.FormatedDateFromDate(DM.dtCreationDate,DM.numDomainId),DM.numDepositId,
		 	'Deposit',
		 	ISNULL(DM.monDepositAmount,0),
		 	ISNULL(DM.monAppliedAmount,0),
		 	NULL,DM.vcMemo,DM.vcReference,'',0,tintDepositePage,''
		 	 FROM DepositMaster DM where DM.numDomainID = @numDomainID AND DM.numDivisionId=@numDivisionId
                        AND ((dateadd(minute,-@ClientTimeZoneOffset,DM.dtCreationDate) between @dtFromDate And @dtToDate))
                        AND tintDepositePage IN (1,2)
		 END
		 
		 --Unapplied Payments
		  IF EXISTS (SELECT 1 FROM #temp WHERE Items=15)
		 BEGIN
			INSERT INTO #tempTransaction 
		 	SELECT 15,'Unapplied Payments',dbo.FormatedDateFromDate(DM.dtCreationDate,DM.numDomainId),DM.numDepositId,
		 	CASE WHEN tintDepositePage=3 THEN 'Credit Memo #' + CAST(numReturnHeaderID AS VARCHAR(20)) ELSE 'Unapplied Payment' END,
		 	ISNULL(DM.monDepositAmount,0),
		 	ISNULL(DM.monAppliedAmount,0),
		 	NULL,DM.vcMemo,DM.vcReference,'',ISNULL(numReturnHeaderID,0),tintDepositePage,''
		 	 FROM DepositMaster DM where DM.numDomainID = @numDomainID AND DM.numDivisionId=@numDivisionId
                        AND ((dateadd(minute,-@ClientTimeZoneOffset,DM.dtCreationDate) between @dtFromDate And @dtToDate))
                        AND (ISNULL(monDepositAmount,0) - ISNULL(monAppliedAmount,0)) > 0 AND tintDepositePage IN(2,3)
		 END
		 
		 DECLARE  @firstRec  AS INTEGER
		 DECLARE  @lastRec  AS INTEGER

         SET @firstRec = (@CurrentPage - 1) * @PageSize
		 SET @lastRec = (@CurrentPage * @PageSize + 1)
		 SET @TotRecs = (SELECT COUNT(*) FROM   #tempTransaction)
         
		 IF @SortCreatedDate = 1 -- Ascending 
		 BEGIN
			SELECT * FROM 
         (SELECT intTransactionType ,vcTransactionType ,dbo.FormatedDateFromDate(dtCreatedDate,@numDomainID) AS [dtCreatedDate],numRecordID ,
				vcRecordName,monTotalAmount,monPaidAmount,dtDueDate E,vcMemo,vcDescription,
				vcStatus ,numRecordID2 ,numStatus, vcBizDocId ,
				ROW_NUMBER() OVER(ORDER BY dtCreatedDate ASC) AS RowNumber FROM #tempTransaction) a
          WHERE RowNumber > @firstRec and RowNumber < @lastRec order BY  dtCreatedDate,intTransactionType
		 END
		 ELSE --By Default Descending
		 BEGIN
			 SELECT * FROM 
         (SELECT intTransactionType ,vcTransactionType ,dbo.FormatedDateFromDate(dtCreatedDate,@numDomainID) AS [dtCreatedDate],numRecordID ,
				vcRecordName,monTotalAmount,monPaidAmount,dtDueDate E,vcMemo,vcDescription,
				vcStatus ,numRecordID2 ,numStatus, vcBizDocId ,
				ROW_NUMBER() OVER(ORDER BY dtCreatedDate DESC) AS RowNumber FROM #tempTransaction) a
          WHERE RowNumber > @firstRec and RowNumber < @lastRec order BY  dtCreatedDate,intTransactionType
         END

		 DROP TABLE #tempTransaction
		 
		 DROP TABLE #temp
    END

GO
/****** Object:  StoredProcedure [dbo].[USP_ItemDetails]    Script Date: 02/07/2010 15:31:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created By Anoop Jayaraj                                          
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_itemdetails')
DROP PROCEDURE usp_itemdetails
GO
CREATE PROCEDURE [dbo].[USP_ItemDetails]                                                  
@numItemCode as numeric(9) ,          
@ClientTimeZoneOffset as int                                               
as                                                 
   
   DECLARE @bitItemIsUsedInOrder AS BIT;SET @bitItemIsUsedInOrder=0
   
   DECLARE @numDomainId AS NUMERIC
   SELECT @numDomainId=numDomainId FROM Item WHERE numItemCode=@numItemCode
   
   IF GETUTCDATE()<'2013-03-15 23:59:39'
		SET @bitItemIsUsedInOrder=0
   ELSE IF (SELECT COUNT(OI.numOppId) FROM dbo.OpportunityItems OI WHERE OI.numItemCode =@numItemCode) >0
		SET @bitItemIsUsedInOrder=1
   ELSE IF EXISTS(SELECT numItemID FROM dbo.General_Journal_Details WHERE numItemID= @numItemCode and numDomainId=@numDomainId AND chBizDocItems='IA1'   )
		set @bitItemIsUsedInOrder =1
                                               
select I.numItemCode, vcItemName, ISNULL(txtItemDesc,'') txtItemDesc,CAST( ISNULL(IED.txtDesc,'')  AS VARCHAR(MAX) ) vcExtendedDescToAPI, charItemType, 
case 
when charItemType='P' AND ISNULL(bitAssembly,0) = 1 then 'Assembly'
when charItemType='P' AND ISNULL(bitKitParent,0) = 1 then 'Kit'
when charItemType='P' AND ISNULL(bitAsset,0) = 1 AND ISNULL(bitRental,0)=0 then 'Asset'
when charItemType='P' AND ISNULL(bitAsset,0) = 1 AND ISNULL(bitRental,0)=1 then 'Rental Asset'
when charItemType='P' AND ISNULL(bitSerialized,0) = 1 AND ISNULL(bitAsset,0) = 0  then 'Serialized'
when charItemType='P' AND ISNULL(bitSerialized,0) = 1 AND ISNULL(bitAsset,0) = 1 AND ISNULL(bitRental,0)=0 then 'Serialized Asset'
when charItemType='P' AND ISNULL(bitSerialized,0) = 1 AND ISNULL(bitAsset,0) = 1 AND ISNULL(bitRental,0)=1 then 'Serialized Rental Asset'
when charItemType='P' AND ISNULL(bitLotNo,0)=1 THEN 'Lot #'
else case when charItemType='P' then 'Inventory' else '' end
end as InventoryItemType,
dbo.fn_GetItemChildMembershipCount(@numDomainId,I.numItemCode) AS numChildMembershipCount,
case when ISNULL(bitAssembly,0)=1 then dbo.fn_GetAssemblyPossibleWOQty(I.numItemCode) else 0 end AS numWOQty,
monListPrice,                   
numItemClassification, isnull(bitTaxable,0) as bitTaxable, 
ISNULL(W.[vcWHSKU], ISNULL(vcSKU,'')) AS vcSKU, 
ISNULL(bitKitParent,0) as bitKitParent,--, dtDateEntered,                  
 numVendorID, I.numDomainID, numCreatedBy, bintCreatedDate, bintModifiedDate,                   
numModifiedBy,
(SELECT TOP 1 vcPathForImage FROM ItemImages WHERE numItemCode = @numItemCode AND bitDefault = 1 AND bitIsImage = 1 ) AS vcPathForImage ,
(SELECT TOP 1 vcPathForTImage FROM ItemImages WHERE numItemCode = @numItemCode AND bitDefault = 1 AND bitIsImage = 1) AS  vcPathForTImage, isnull(bitSerialized,0) as bitSerialized, vcModelID,                   
(SELECT numItemImageId,vcPathForImage,vcPathForTImage,bitDefault,case when bitdefault=1 then -1 
else isnull(intDisplayOrder,0) end as intDisplayOrder 
FROM ItemImages  
WHERE numItemCode=@numItemCode order by case when bitdefault=1 then -1 else isnull(intDisplayOrder,0) end  asc 
FOR XML AUTO,ROOT('Images')) AS xmlItemImages,
(SELECT STUFF((SELECT ',' + CONVERT(VARCHAR(50) , numCategoryID)  FROM dbo.ItemCategory WHERE numItemID = I.numItemCode FOR XML PATH('')), 1, 1, '')) AS vcItemCategories ,
numItemGroup, numCOGsChartAcntId, numAssetChartAcntId, numIncomeChartAcntId, monAverageCost,                   
monCampaignLabourCost,dbo.fn_GetContactName(numCreatedBy)+' ,'+convert(varchar(20),DateAdd(minute, -@ClientTimeZoneOffset,bintCreatedDate )) as CreatedBy ,                                      
dbo.fn_GetContactName(numModifiedBy)+' ,'+convert(varchar(20),DateAdd(minute, -@ClientTimeZoneOffset,bintModifiedDate )) as ModifiedBy,                      
sum(numOnHand) as numOnHand,                      
sum(numOnOrder) as numOnOrder,                      
sum(numReorder)  as numReorder,                      
sum(numAllocation)  as numAllocation,                      
sum(numBackOrder)  as numBackOrder,                   
isnull(fltWeight,0) as fltWeight,                
isnull(fltHeight,0) as fltHeight,                
isnull(fltWidth,0) as fltWidth,                
isnull(fltLength,0) as fltLength,                
isnull(bitFreeShipping,0) as bitFreeShipping,              
isnull(bitAllowBackOrder,0) as bitAllowBackOrder,              
isnull(vcUnitofMeasure,'Units') as vcUnitofMeasure,      
isnull(bitShowDeptItem,0) bitShowDeptItem,      
isnull(bitShowDeptItemDesc,0) bitShowDeptItemDesc,  
isnull(bitCalAmtBasedonDepItems ,0) bitCalAmtBasedonDepItems,
isnull(bitAssembly ,0) bitAssembly ,
isnull(cast(numBarCodeId as varchar(50)),'') as  numBarCodeId ,
isnull(I.vcManufacturer,'') as vcManufacturer,
ISNULL(I.numBaseUnit,0) AS numBaseUnit,ISNULL(I.numPurchaseUnit,0) AS numPurchaseUnit,ISNULL(I.numSaleUnit,0) AS numSaleUnit,
dbo.fn_GetUOMName(ISNULL(I.numBaseUnit,0)) AS vcBaseUnit,dbo.fn_GetUOMName(ISNULL(I.numPurchaseUnit,0)) AS vcPurchaseUnit,dbo.fn_GetUOMName(ISNULL(I.numSaleUnit,0)) AS vcSaleUnit,
isnull(bitLotNo,0) as bitLotNo,
ISNULL(IsArchieve,0) AS IsArchieve,
case when charItemType='P' then 'Inventory Item' when charItemType='S' then 'Service' when charItemType='A' then 'Accessory' when charItemType='N' then 'Non-Inventory Item' end as ItemType,
isnull(I.numItemClass,0) as numItemClass,
ISNULL(tintStandardProductIDType,0) tintStandardProductIDType,
ISNULL(vcExportToAPI,'') vcExportToAPI,
ISNULL(numShipClass,0) numShipClass,ISNULL(I.bitAllowDropShip,0) AS bitAllowDropShip,
ISNULL(@bitItemIsUsedInOrder,0) AS bitItemIsUsedInOrder,
ISNULL((SELECT COUNT(*) FROM ItemDetails WHERE numChildItemID = I.numItemCode),0) AS intParentKitItemCount,
ISNULL(I.bitArchiveItem,0) AS [bitArchiveItem],
ISNULL(W.numWareHouseID,0) AS [numWareHouseID],
ISNULL(W.numWareHouseItemID,0) AS [numWareHouseItemID],
ISNULL((SELECT COUNT(WI.[numWareHouseItemID]) FROM [dbo].[WareHouseItems] WI WHERE WI.[numItemID] = I.numItemCode AND WI.[numItemID] = @numItemCode),0) AS [intTransCount],
ISNULL(I.bitAsset,0) AS [bitAsset],
ISNULL(I.bitRental,0) AS [bitRental],
ISNULL(W.vcBarCode,ISNULL(I.[numBarCodeId],'')) AS [vcBarCode]
FROM Item I       
left join  WareHouseItems W                  
on W.numItemID=I.numItemCode                
LEFT JOIN ItemExtendedDetails IED   ON I.numItemCode = IED.numItemCode               
WHERE I.numItemCode=@numItemCode  
GROUP BY I.numItemCode, vcItemName, txtItemDesc, charItemType, monListPrice,                   
numItemClassification, bitTaxable,vcSKU,[W].[vcWHSKU] , bitKitParent,numVendorID, I.numDomainID,               
numCreatedBy, bintCreatedDate, bintModifiedDate,                   
numModifiedBy,bitAllowBackOrder, bitSerialized, vcModelID,                   
numItemGroup, numCOGsChartAcntId, numAssetChartAcntId, numIncomeChartAcntId, monAverageCost,                   
monCampaignLabourCost,fltWeight,fltHeight,fltWidth,fltLength,bitFreeShipping,vcUnitofMeasure,bitShowDeptItemDesc,bitShowDeptItem,bitCalAmtBasedonDepItems,bitAssembly,numBarCodeId,I.vcManufacturer
,I.numBaseUnit,I.numPurchaseUnit,I.numSaleUnit,I.bitLotNo,I.IsArchieve,I.numItemClass,I.tintStandardProductIDType,I.vcExportToAPI,I.numShipClass,
CAST( ISNULL(IED.txtDesc,'')  AS VARCHAR(MAX) ),I.bitAllowDropShip,I.bitArchiveItem,I.bitAsset,I.bitRental,W.[numWarehouseID],W.numWareHouseItemID,W.[vcBarCode]


--exec USP_ItemDetails @numItemCode = 822625 ,@ClientTimeZoneOffset =330

