/******************************************************************
Project: Release 14.6 Date: 05.DEC.2020
Comments: ALTER SCRIPTS
*******************************************************************/

/******************************************** PRASHANT *********************************************/

INSERT INTO GenericDocuments
(
	VcFileName, vcDocName, numDocCategory, cUrlType, vcFileType, numDocStatus, vcDocDesc, numDomainID, numCreatedBy, 
	bintCreatedDate, numModifiedBy, bintModifiedDate, vcSubject, vcDocumentSection, numRecID, tintCheckOutStatus, intDocumentVersion, 
	numLastCheckedOutBy, dtCheckOutDate, dtCheckInDate, tintDocumentType, numOldSpecificDocID, numModuleID, vcContactPosition, BizDocOppType, 
	BizDocType, BizDocTemplate, vcGroupsPermission, numCategoryId, bitLastFollowupUpdate, numFollowUpStatusId, bitUpdateFollowupStatus, numFormFieldGroupId
)
SELECT 
	'#SYS#EMAIL_ALERT:PROJECT_TASK_SUMMARY', 'Email Alert: Project Summary Report', 369, '', '', 0, '<div class="modal-body" style="font-size:16px !important">
   <center>
      <h3> Project Work Completed</h3>
   </center>
   <label style="padding-left:10px;" id="lblProjectProgressName">##ProjectName##</label>:  &nbsp;<span id="lblProjectProgressDescription">##ProjectDescription##</span><br>
   <label style="padding-left:10px;">For: &nbsp;</label><span id="lblProjectProgressCompanyFor">##ProjectCompanyFor##</span><br>
   <label style="padding-left:10px;">By: &nbsp;</label><span id="lblProjectProgressCompanyBy">##ProjectCompanyBy##</span><br>
   {##ProjectLineTask## }
   <div id="divProjectProgressTaskDetails">
      <div style="margin-top:5px;padding:10px;background-color:##color##">
         <div class="pull-left">
            <div class="form-group" style="margin-bottom: 0px;">
               <label>Total Progress:&nbsp;&nbsp;<span class="badge bg-light-blue lblProjectProgressTotalProgress">##TotalProgress##%</span></label>
               <div class="progress progress-xs progress-striped active" style="margin-bottom: 0px;">
                  <div class="progress-bar progress-bar-primary divProjectProgressTotalProgress" style="width: ##TotalProgress##%;"></div>
               </div>
            </div>
         </div>
         <div class="pull-right"><label>On</label>:<span>##FinishDate##</span> <label>From</label>: <span id="">##DurationTakenForProject##</span>, <span id="">##StartTiime##</span><label> &nbsp;To:</label> <span id="">##FinishTime## </span></div>
         <div class="clearfix"></div>
         <label>Work Description: </label><span>##TaskName##</span> <br><label>By: </label><span>##AssignedTo##</span> <br><label>Balance Remaining: </label><span>##BalanceTime##</span> <br>
      </div>
   </div>
   {##/ProjectLineTask## }
</div>'
	, numDomainID, 0, GETUTCDATE(), 0, GETUTCDATE(), 'Project Summary Report For ##ProjectName##', NULL, NULL, NULL, NULL, 
	NULL, NULL, NULL, 1, NULL, 6, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL
FROM Domain

/******************************************** SANDEEP *********************************************/

INSERT INTO DynamicFormMaster
(
	numFormId,vcFormName,cCustomFieldsAssociated,cAOIAssociated,bitDeleted,tintFlag,numBizFormModuleID
)
VALUES
(
	146,'Vendor Details','Y','N',0,0,2
)

INSERT INTO DynamicFormMaster
(
	numFormId,vcFormName,cCustomFieldsAssociated,cAOIAssociated,bitDeleted,tintFlag,numBizFormModuleID
)
VALUES
(
	147,'Employer Details','Y','N',0,0,2
)

--------------------

UPDATE BizFormWizardMasterConfiguration SET numFormID=36 WHERE numRelCntType NOT IN (46,47,93) AND numRelCntType IN (SELECT numListItemID FROM ListDetails WHERE numListID=5 AND ISNULL(constFlag,0)=0) AND numFormID=35

-------------------------

UPDATE DynamicFormMaster SET vcFormName='Prospect Details' WHERE numFormId=100

--------------------------

ALTER TABLE Domain ADD tintReorderPointBasedOn TINYINT DEFAULT 1
ALTER TABLE Domain ADD intReorderPointDays INT DEFAULT 30
ALTER TABLE Domain ADD intReorderPointPercent INT DEFAULT 0
ALTER TABLE Domain ADD bitUsePreviousEmailBizDoc BIT DEFAULT 1
ALTER TABLE Item ADD bitAutomateReorderPoint BIT 
ALTER TABLE Item ADD dtLastAutomateReorderPoint DATE
--------------------------------

DECLARE @numFieldID INT

INSERT INTO DycFieldMaster
(
	numModuleID,vcFieldName,vcDbColumnName,vcOrigDbColumnName,vcPropertyName,vcLookBackTableName,vcFieldDataType,vcFieldType,vcAssociatedControlType,vcListItemType,numListID,bitInResults,bitDeleted,bitAllowEdit,bitDefault
)
VALUES
(
	4,'Automate Re-Order Point & Qty','bitAutomateReorderPoint','bitAutomateReorderPoint','IsAutomateReorderPoint','Item','Y','R','CheckBox','',0,1,0,1,0
)

SET @numFieldID = SCOPE_IDENTITY()

INSERT INTO DycFormField_Mapping
(
	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,bitInResults,bitDeleted,bitDefault,bitImport,bitAllowFiltering,intSectionID
)
VALUES
(
	4,@numFieldID,20,0,0,'Automate Re-Order Point & Qty','CheckBox',1,0,0,1,1,1
)

--------------------------------

ALTER TABLE MetaTags ADD vcPageTitle VARCHAR(4000)

--------------------------------

USE [Production.2014]
GO

/****** Object:  Table [dbo].[DivisionMasterBizDocEmail]    Script Date: 03-Dec-20 10:18:53 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[DivisionMasterBizDocEmail](
	[ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[numDomainID] [numeric](18, 0) NOT NULL,
	[numDivisionID] [numeric](18, 0) NOT NULL,
	[numBizDocID] [numeric](18, 0) NOT NULL,
	[vcTo] [varchar](5000) NULL,
	[vcCC] [varchar](5000) NULL,
	[vcBCC] [varchar](5000) NULL,
 CONSTRAINT [PK_DivisionMasterBizDocEmail] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING ON
GO

-----------------------------------------

DELETE FROM CFw_Grp_Master WHERE Loc_Id=2 AND tintType=2 AND Grp_Name='Sales Forecasts'
