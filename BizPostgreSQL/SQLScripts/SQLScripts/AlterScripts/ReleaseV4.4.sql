/******************************************************************
Project: Release 4.4 Date: 31.MARCH.2015
Comments: 
*******************************************************************/


---------------  Sandeep  ----------------------

/****** Object:  Table [dbo].[CommunicationLinkedOrganization]    Script Date: 02-Mar-15 5:13:26 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[CommunicationLinkedOrganization](
	[numCLOID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[numCommID] [numeric](18, 0) NOT NULL,
	[numDivisionID] [numeric](18, 0) NULL,
	[numContactID] [numeric](18, 0) NULL,
 CONSTRAINT [PK_CommunicationLinkedOrganization] PRIMARY KEY CLUSTERED 
(
	[numCLOID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[CommunicationLinkedOrganization]  WITH CHECK ADD  CONSTRAINT [FK_CommunicationLinkedOrganization_Communication] FOREIGN KEY([numCommID])
REFERENCES [dbo].[Communication] ([numCommId])
ON DELETE CASCADE
GO

ALTER TABLE [dbo].[CommunicationLinkedOrganization] CHECK CONSTRAINT [FK_CommunicationLinkedOrganization_Communication]
GO



---------------------------------------------------


ALTER TABLE CommissionRules ADD
tintComOrgType TINYINT

GO
UPDATE CommissionRules SET tintComOrgType=3 


ALTER TABLE BizDocComission ADD
numComPayPeriodID NUMERIC(18,0),
bitFullPaidBiz BIT

ALTER TABLE BizDocComission 
ALTER COLUMN numComissionAmount decimal(18,4)


---------------------------------------------------

GO

/****** Object:  Table [dbo].[CommissionRuleOrganization]    Script Date: 03-Mar-15 7:20:34 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[CommissionRuleOrganization](
	[numComRuleOrgID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[numComRuleID] [numeric](18, 0) NOT NULL,
	[numValue] [numeric](18, 0) NULL,
	[numProfile] [numeric](18, 0) NULL,
	[tintType] [tinyint] NOT NULL,
 CONSTRAINT [PK_CommissionRuleOrganization] PRIMARY KEY CLUSTERED 
(
	[numComRuleOrgID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[CommissionRuleOrganization]  WITH CHECK ADD  CONSTRAINT [FK_CommissionRuleOrganization_CommissionRules] FOREIGN KEY([numComRuleID])
REFERENCES [dbo].[CommissionRules] ([numComRuleID])
ON DELETE CASCADE
GO

ALTER TABLE [dbo].[CommissionRuleOrganization] CHECK CONSTRAINT [FK_CommissionRuleOrganization_CommissionRules]
GO




GO

/****** Object:  Table [dbo].[CommissionPayPeriod]    Script Date: 12-Mar-15 3:49:46 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[CommissionPayPeriod](
	[numComPayPeriodID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[numDomainID] [numeric](18, 0) NOT NULL,
	[dtStart] [date] NOT NULL,
	[dtEnd] [date] NOT NULL,
	[tintPayPeriod] [int] NOT NULL,
	[numCreatedBy] [numeric](18, 0) NOT NULL,
	[dtCreated] [datetime] NOT NULL,
	[numModifiedBy] [numeric](18, 0) NULL,
	[dtModified] [datetime] NULL,
 CONSTRAINT [PK_CommissionPayPeriod] PRIMARY KEY CLUSTERED 
(
	[numComPayPeriodID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[CommissionPayPeriod]  WITH CHECK ADD  CONSTRAINT [FK_CommissionPayPeriod_Domain] FOREIGN KEY([numDomainID])
REFERENCES [dbo].[Domain] ([numDomainId])
ON DELETE CASCADE
GO

ALTER TABLE [dbo].[CommissionPayPeriod] CHECK CONSTRAINT [FK_CommissionPayPeriod_Domain]
GO


-------------------------------------------------------------------------------------------------

INSERT INTO ReportFieldGroupMappingMaster 
(
	numReportFieldGroupID,
	numFieldID,
	vcFieldName,
	vcAssociatedControlType,
	vcFieldDataType,
	bitAllowSorting,
	bitAllowGrouping,
	bitAllowAggregate,
	bitAllowFiltering
)
VALUES
(
	7,
	320,
	'Serial No',
	'TextBox',
	'V',
	0,
	0,
	0,
	0
)


INSERT INTO ReportFieldGroupMappingMaster 
(
	numReportFieldGroupID,
	numFieldID,
	vcFieldName,
	vcAssociatedControlType,
	vcFieldDataType,
	bitAllowSorting,
	bitAllowGrouping,
	bitAllowAggregate,
	bitAllowFiltering
)
VALUES
(
	11,
	320,
	'Serial No',
	'TextBox',
	'V',
	0,
	0,
	0,
	0
)

ALTER TABLE AdvSerViewConf ADD
bitCustom BIT,
intColumnWidth INT