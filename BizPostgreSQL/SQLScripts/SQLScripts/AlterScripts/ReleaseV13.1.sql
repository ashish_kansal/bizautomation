/******************************************************************
Project: Release 13.1 Date: 17.FEB.2020
Comments: ALTER SCRIPTS
*******************************************************************/


/******************************************** SANDEEP *********************************************/


ALTER TABLE WorkOrder ADD numModifiedBy NUMERIC(18,0)
ALTER TABLE WorkOrder ADD bintModifiedDate DATETIME
ALTER TABLE WorkOrder ADD numCompletedBy NUMERIC(18,0)
ALTER TABLE WorkOrder ADD monLabourCost DECIMAL(20,5)
ALTER TABLE WorkOrder ADD monOverheadCost DECIMAL(20,5)
ALTER TABLE StagePercentageDetailsTask ADD monHourlyRate DECIMAL(20,5)
ALTER TABLE Domain ADD numOverheadServiceItemID NUMERIC(18,0)
ALTER TABLE ProjectProgress ADD numWorkOrderID NUMERIC(18,0)

ALTER TABLE StagePercentageDetailsTask ADD dtStartTime DATETIME
ALTER TABLE StagePercentageDetailsTask ADD dtEndTime DATETIME

--------------------------------------------

UPDATE UserMaster SET monOverTimeRate = monHourlyRate WHERE ISNULL(monOverTimeRate,0) = 0

--------------------------------------------

INSERT INTO ListModule
(
	numModuleID,vcModuleName,bitVisible
)
VALUES
(
	15,'Process time entry',1
)

----------------------------------------------------------------------

SET IDENTITY_INSERT ListMaster ON

INSERT INTO ListMaster
(
	numListID,vcListName,numCreatedBy,bitDeleted,bitFixed,numDomainID,bitFlag,vcDataType,numModuleID
)
VALUES
(
	52,'Reason for Pause',1,0,0,1,1,'string',15
)

SET IDENTITY_INSERT ListDetails OFF

-----------------------------------------------------------------------

UPDATE DynamicFOrmMaster SET vcFormName='Work Order Grid Column Settings' WHERE vcFormName LIKE '%work order%' AND numFormId=33

------------------------------------------------------------------------

INSERT INTO DynamicFormMaster
(
	numFormId,vcFormName,cCustomFieldsAssociated,cAOIAssociated,bitDeleted,tintFlag
)
VALUES
(
	144,'Work Order Detail Page Layout Setting','Y','N',0,0
)

-------------------------------------------------------------------------------

DECLARE @numFieldID NUMERIC(18,0)
INSERT INTO DycFieldMaster
(
	numModuleID,vcFieldName,vcDbColumnName,vcOrigDbColumnName,vcPropertyName,vcLookBackTableName,vcFieldDataType,vcFieldType,vcAssociatedControlType,vcListItemType,numListID,bitInResults,bitDeleted,bitAllowEdit,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitWorkFlowField,bitAllowFiltering
)
VALUES
(
	3,'Planned Start Date','dtmStartDate','dtmStartDate','PlannedStartDate','WorkOrder','V','R','DateField','',0,1,0,1,1,1,1,1,1,1,1
)

SET @numFieldID = SCOPE_IDENTITY()

INSERT INTO DycFormField_Mapping
(
	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,bitInResults,bitDeleted,bitDefault,bitSettingField,bitAddField,bitDetailField
)
VALUES
(
	3,@numFieldID,144,1,1,'Planned Start Date','DateField',1,0,1,1,1,1
)

------------------------------------------------------------------------

DECLARE @numFieldID NUMERIC(18,0)
INSERT INTO DycFieldMaster
(
	numModuleID,vcFieldName,vcDbColumnName,vcOrigDbColumnName,vcPropertyName,vcLookBackTableName,vcFieldDataType,vcFieldType,vcAssociatedControlType,vcListItemType,numListID,bitInResults,bitDeleted,bitAllowEdit,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitWorkFlowField,bitAllowFiltering
)
VALUES
(
	3,'Requested Finish','dtmEndDate','dtmEndDate','RequestedFinish','WorkOrder','V','R','DateField','',0,1,0,1,1,1,1,1,1,1,1
)

SET @numFieldID = SCOPE_IDENTITY()

INSERT INTO DycFormField_Mapping
(
	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,bitInResults,bitDeleted,bitDefault,bitSettingField,bitAddField,bitDetailField
)
VALUES
(
	3,@numFieldID,144,1,1,'Requested Finish','DateField',1,0,1,1,1,1
)

------------------------------------------------------------------------

DECLARE @numFieldID NUMERIC(18,0)
INSERT INTO DycFieldMaster
(
	numModuleID,vcFieldName,vcDbColumnName,vcOrigDbColumnName,vcPropertyName,vcLookBackTableName,vcFieldDataType,vcFieldType,vcAssociatedControlType,vcListItemType,numListID,bitInResults,bitDeleted,bitAllowEdit,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitWorkFlowField,bitAllowFiltering
)
VALUES
(
	3,'Actual Start','dtActualStartDate','dtActualStartDate','ActualStartDate','WorkOrder','V','R','DateField','',0,1,0,0,0,1,0,1,1,1,1
)

SET @numFieldID = SCOPE_IDENTITY()

INSERT INTO DycFormField_Mapping
(
	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,bitInResults,bitDeleted,bitDefault,bitSettingField,bitAddField,bitDetailField
)
VALUES
(
	3,@numFieldID,144,0,0,'Actual Start','DateField',1,0,0,1,0,1
)


------------------------------------------------------------------------

DECLARE @numFieldID NUMERIC(18,0)
INSERT INTO DycFieldMaster
(
	numModuleID,vcFieldName,vcDbColumnName,vcOrigDbColumnName,vcPropertyName,vcLookBackTableName,vcFieldDataType,vcFieldType,vcAssociatedControlType,vcListItemType,numListID,bitInResults,bitDeleted,bitAllowEdit,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitWorkFlowField,bitAllowFiltering
)
VALUES
(
	3,'Projected Finish','vcProjectedFinish','vcProjectedFinish','ProjectedFinish','WorkOrder','V','R','Label','',0,1,0,0,0,1,0,1,1,1,1
)

SET @numFieldID = SCOPE_IDENTITY()

INSERT INTO DycFormField_Mapping
(
	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,bitInResults,bitDeleted,bitDefault,bitSettingField,bitAddField,bitDetailField
)
VALUES
(
	3,@numFieldID,144,0,0,'Projected Finish','Label',1,0,0,1,0,1
)

------------------------------------------------------------------------

INSERT INTO DycFormField_Mapping
(
	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,bitInResults,bitDeleted,bitDefault,bitSettingField,bitAddField,bitDetailField
)
VALUES
(
	3,298,144,0,0,'Warehouse','Label',1,0,1,1,0,1
)

-------------------------------------------------------------------------

INSERT INTO DycFormField_Mapping
(
	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,bitInResults,bitDeleted,bitDefault,bitSettingField,bitAddField,bitDetailField
)
VALUES
(
	3,96,144,0,0,'Sales Order','Label',1,0,1,1,0,1
)

-------------------------------------------------------------------------

DECLARE @numFieldID NUMERIC(18,0)
INSERT INTO DycFieldMaster
(
	numModuleID,vcFieldName,vcDbColumnName,vcOrigDbColumnName,vcPropertyName,vcLookBackTableName,vcFieldDataType,vcFieldType,vcAssociatedControlType,vcListItemType,numListID,bitInResults,bitDeleted,bitAllowEdit,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitWorkFlowField,bitAllowFiltering
)
VALUES
(
	3,'Build Manager','numAssignedTo','numAssignedTo','AssignedTo','WorkOrder','N','R','SelectBox','U',0,1,0,1,1,1,1,1,1,1,1
)

SET @numFieldID = SCOPE_IDENTITY()

INSERT INTO DycFormField_Mapping
(
	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,bitInResults,bitDeleted,bitDefault,bitSettingField,bitAddField,bitDetailField
)
VALUES
(
	3,@numFieldID,144,1,1,'Build Manager','SelectBox',1,0,1,1,1,1
)

-------------------------------------------------------------------------


INSERT INTO DycFormField_Mapping
(
	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,bitInResults,bitDeleted,bitDefault,bitSettingField,bitAddField,bitDetailField
)
VALUES
(
	3,302,144,1,1,'Comments','TextArea',1,0,1,1,1,1
)

------------------------------------------------------------------------

UPDATE DycFieldMaster SET vcPropertyName='WarehouseName' WHERE numFieldId=298
UPDATE DycFieldMaster SET vcPropertyName='Comments' WHERE numFieldId=302

------------------------------------------------------------------------

SET IDENTITY_INSERT ModuleMaster ON

INSERT INTO ModuleMaster
(
	numModuleID,vcModuleName,tintGroupType
)
VALUES
(
	16,'Manufacturing',1
)

SET IDENTITY_INSERT ModuleMaster OFF

-----------------------------------------------------------------------------


INSERT INTO PageMaster
(
	numPageID,numModuleID,vcFileName,vcPageDesc,bitIsViewApplicable,bitIsAddApplicable,bitIsUpdateApplicable,bitIsDeleteApplicable,bitIsExportApplicable
)
VALUES
(152,16,'frmWorkOrderList.aspx','Work Orders List',1,1,1,1,0)
,(153,16,'frmWorkOrder.aspx','Work Order Detail',1,0,1,1,0)


-------------------------------------------------------------------------

USE [Production.2014]
GO

/****** Object:  Table [dbo].[WorkSchedule]    Script Date: 05-Dec-19 11:24:52 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[WorkSchedule](
	[ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[numDomainID] [numeric](18, 0) NOT NULL,
	[numUserCntID] [numeric](18, 0) NOT NULL,
	[numWorkHours] [tinyint] NOT NULL,
	[numWorkMinutes] [tinyint] NOT NULL,
	[numProductiveHours] [tinyint] NOT NULL,
	[numProductiveMinutes] [tinyint] NOT NULL,
	[tmStartOfDay] [time](7) NOT NULL,
	[vcWorkDays] [varchar](20) NOT NULL,
 CONSTRAINT [PK_WorkSchedule] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING ON
GO


-----------------------------------------------

USE [Production.2014]
GO

/****** Object:  Table [dbo].[WorkScheduleDaysOff]    Script Date: 05-Dec-19 11:25:16 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[WorkScheduleDaysOff](
	[ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[numWorkScheduleID] [numeric](18, 0) NOT NULL,
	[dtDayOffFrom] [date] NOT NULL,
	[dtDayOffTo] [date] NOT NULL,
 CONSTRAINT [PK_WorkScheduleDaysOff] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[WorkScheduleDaysOff]  WITH CHECK ADD  CONSTRAINT [FK_WorkScheduleDaysOff_WorkSchedule] FOREIGN KEY([numWorkScheduleID])
REFERENCES [dbo].[WorkSchedule] ([ID])
ON DELETE CASCADE
GO

ALTER TABLE [dbo].[WorkScheduleDaysOff] CHECK CONSTRAINT [FK_WorkScheduleDaysOff_WorkSchedule]
GO

-------------------------------------------------------------

ALTER TABLE UserMaster ADD tintPayrollType TINYINT
ALTER TABLE UserMaster ADD tintHourType TINYINT

--------------------------------------------------------------

USE [Production.2014]
GO

/****** Object:  Table [dbo].[StagePercentageDetailsTaskTimeLog]    Script Date: 28-Jan-20 11:17:36 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[StagePercentageDetailsTaskTimeLog](
	[ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[numDomainID] [numeric](18, 0) NOT NULL,
	[numUserCntID] [numeric](18, 0) NOT NULL,
	[numTaskID] [numeric](18, 0) NOT NULL,
	[tintAction] [tinyint] NOT NULL,
	[dtActionTime] [datetime] NOT NULL,
	[numProcessedQty] [float] NOT NULL,
	[numReasonForPause] [numeric](18, 0) NOT NULL,
	[vcNotes] [varchar](1000) NOT NULL,
	[bitManualEntry] [bit] NOT NULL,
	[numModifiedBy] [numeric](18, 0) NULL,
	[dtModifiedDate] [datetime] NULL,
 CONSTRAINT [PK_StagePercentageDetailsTaskTimeLog] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING ON
GO

ALTER TABLE [dbo].[StagePercentageDetailsTaskTimeLog]  WITH CHECK ADD  CONSTRAINT [FK_StagePercentageDetailsTaskTimeLog_StagePercentageDetailsTask] FOREIGN KEY([numTaskID])
REFERENCES [dbo].[StagePercentageDetailsTask] ([numTaskId])
GO

ALTER TABLE [dbo].[StagePercentageDetailsTaskTimeLog] CHECK CONSTRAINT [FK_StagePercentageDetailsTaskTimeLog_StagePercentageDetailsTask]
GO



/******************************************** PRASANT *********************************************/

INSERT INTO TabMaster(numTabName, Remarks, tintTabType, vcURL, bitFixed, numDomainID, vcImage, vcAddURL, bitAddIsPopUp)
VALUES('Human Resources','Human Resources',1,'',1,1,NULL,NULL,NULL)

INSERT INTO GroupTabDetails(numGroupId, numTabId, numRelationShip, bitallowed, numOrder, numProfileID, tintType, bitInitialTab)
SELECT  
	numGroupID,(SELECT TOP 1 numTabId FROM TabMaster WHERE numTabName='Human Resources' AND numDomainID=1),
	0,1,15,0,0,0
FROM    
	AuthenticationGroupMaster
WHERE
	vcGroupName='System Administrator'

INSERT INTO ModuleMaster(vcModuleName,tintGroupType)VALUES('Human Resources',1)

BEGIN TRY
BEGIN TRANSACTION

	DECLARE @numMAXPageNavID INT
	SELECT @numMAXPageNavID = MAX(numPageNavID) FROM PageNavigationDTL


	INSERT INTO PageNavigationDTL
	(
		numPageNavID,numModuleID,numParentID,vcPageNavName,vcNavURL,bitVisible,numTabID
	)
	VALUES
	(
		(@numMAXPageNavID + 1),(SELECT TOP 1 numModuleID FROM ModuleMaster WHERE vcModuleName='Human Resources'),0,'Human Resources','',1,(SELECT TOP 1 numTabId FROM TabMaster WHERE numTabName='Human Resources' AND numDomainID=1)
	)

	INSERT INTO dbo.TreeNavigationAuthorization
	(
		numGroupID,
		numTabID,
		numPageNavID,
		bitVisible,
		numDomainID,
		tintType
	)
	SELECT  
		numGroupID,
		(SELECT TOP 1 numTabId FROM TabMaster WHERE numTabName='Human Resources' AND numDomainID=1),
		(@numMAXPageNavID + 1),
		1,
		numDomainID,
		1
	FROM    
		AuthenticationGroupMaster
	WHERE
		vcGroupName='System Administrator'
COMMIT
END TRY
BEGIN CATCH
	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;

	SELECT 
		ERROR_MESSAGE(),
		ERROR_NUMBER(),
		ERROR_SEVERITY(),
		ERROR_STATE(),
		ERROR_LINE(),
		ISNULL(ERROR_PROCEDURE(), '-');
END CATCH
--Internal Users
UPDATE 
	PageNavigationDTL 
SET 
	numParentID=(SELECT TOP 1 numPageNavID FROM PageNavigationDTL WHERE vcPageNavName='Human Resources'),
	numModuleID=(SELECT TOP 1 numModuleID FROM ModuleMaster WHERE vcModuleName='Human Resources'),
	numTabID=(SELECT TOP 1 numTabId FROM TabMaster WHERE numTabName='Human Resources' AND numDomainID=1)
WHERE 
	vcPageNavName='Internal Users'

DELETE FROM  TreeNavigationAuthorization  WHERE numTabID=-1 AND numPageNavId=(SELECT TOP 1 numPageNavID FROM PageNavigationDTL WHERE vcPageNavName='Internal Users')
INSERT INTO dbo.TreeNavigationAuthorization
	(
		numGroupID,
		numTabID,
		numPageNavID,
		bitVisible,
		numDomainID,
		tintType
	)
	SELECT  
		numGroupID,
		(SELECT TOP 1 numTabId FROM TabMaster WHERE numTabName='Human Resources' AND numDomainID=1),
		(SELECT TOP 1 numPageNavID FROM PageNavigationDTL WHERE vcPageNavName='Internal Users'),
		1,
		numDomainID,
		1
	FROM    
		AuthenticationGroupMaster
	WHERE
		vcGroupName='System Administrator'
--External Users
UPDATE 
	PageNavigationDTL 
SET 
	numParentID=(SELECT TOP 1 numPageNavID FROM PageNavigationDTL WHERE vcPageNavName='Human Resources'),
	numModuleID=(SELECT TOP 1 numModuleID FROM ModuleMaster WHERE vcModuleName='Human Resources'),
	numTabID=(SELECT TOP 1 numTabId FROM TabMaster WHERE numTabName='Human Resources' AND numDomainID=1)
WHERE 
	vcPageNavName='External Users'

DELETE FROM  TreeNavigationAuthorization  WHERE numTabID=-1 AND numPageNavId=(SELECT TOP 1 numPageNavID FROM PageNavigationDTL WHERE vcPageNavName='External Users')
INSERT INTO dbo.TreeNavigationAuthorization
	(
		numGroupID,
		numTabID,
		numPageNavID,
		bitVisible,
		numDomainID,
		tintType
	)
	SELECT  
		numGroupID,
		(SELECT TOP 1 numTabId FROM TabMaster WHERE numTabName='Human Resources' AND numDomainID=1),
		(SELECT TOP 1 numPageNavID FROM PageNavigationDTL WHERE vcPageNavName='External Users'),
		1,
		numDomainID,
		1
	FROM    
		AuthenticationGroupMaster
	WHERE
		vcGroupName='System Administrator'

--Manage Authorization / Permissions & Roles
UPDATE 
	PageNavigationDTL 
SET 
	numParentID=(SELECT TOP 1 numPageNavID FROM PageNavigationDTL WHERE vcPageNavName='Human Resources'),
	numModuleID=(SELECT TOP 1 numModuleID FROM ModuleMaster WHERE vcModuleName='Human Resources'),
	numTabID=(SELECT TOP 1 numTabId FROM TabMaster WHERE numTabName='Human Resources' AND numDomainID=1),
	vcPageNavName='Permissions & Roles'
WHERE 
	vcPageNavName='Manage Authorization'

DELETE FROM  TreeNavigationAuthorization  WHERE numTabID=-1 AND numPageNavId=(SELECT TOP 1 numPageNavID FROM PageNavigationDTL WHERE vcPageNavName='Permissions & Roles')
INSERT INTO dbo.TreeNavigationAuthorization
	(
		numGroupID,
		numTabID,
		numPageNavID,
		bitVisible,
		numDomainID,
		tintType
	)
	SELECT  
		numGroupID,
		(SELECT TOP 1 numTabId FROM TabMaster WHERE numTabName='Human Resources' AND numDomainID=1),
		(SELECT TOP 1 numPageNavID FROM PageNavigationDTL WHERE vcPageNavName='Permissions & Roles'),
		1,
		numDomainID,
		1
	FROM    
		AuthenticationGroupMaster
	WHERE
		vcGroupName='System Administrator'

--Time & Expense
DELETE GroupTabDetails WHERE numTabId IN(SELECT numTabId FROM TabMaster WHERE numTabName='Time & Expense' )
DELETE TabMaster WHERE numTabId IN(SELECT numTabId FROM TabMaster WHERE numTabName='Time & Expense' )
BEGIN TRY
BEGIN TRANSACTION
	
	DECLARE @numMAXPageNavID INT
	SELECT @numMAXPageNavID = MAX(numPageNavID) FROM PageNavigationDTL


	INSERT INTO PageNavigationDTL
	(
		numPageNavID,numModuleID,numParentID,vcPageNavName,vcNavURL,bitVisible,numTabID
	)
	VALUES
	(
		(@numMAXPageNavID + 1),(SELECT TOP 1 numModuleID FROM ModuleMaster WHERE vcModuleName='Human Resources'),(SELECT TOP 1 numPageNavID FROM PageNavigationDTL WHERE vcPageNavName='Human Resources'),'Time & Expense','../TimeAndExpense/frmEmpCal.aspx?frm=contactlist&CntID=RecordID',1,(SELECT TOP 1 numTabId FROM TabMaster WHERE numTabName='Human Resources' AND numDomainID=1)
	)

	INSERT INTO dbo.TreeNavigationAuthorization
	(
		numGroupID,
		numTabID,
		numPageNavID,
		bitVisible,
		numDomainID,
		tintType
	)
	SELECT  
		numGroupID,
		(SELECT TOP 1 numTabId FROM TabMaster WHERE numTabName='Human Resources' AND numDomainID=1),
		(@numMAXPageNavID + 1),
		1,
		numDomainID,
		1
	FROM    
		AuthenticationGroupMaster
	WHERE
		vcGroupName='System Administrator'
COMMIT
END TRY
BEGIN CATCH
	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;

	SELECT 
		ERROR_MESSAGE(),
		ERROR_NUMBER(),
		ERROR_SEVERITY(),
		ERROR_STATE(),
		ERROR_LINE(),
		ISNULL(ERROR_PROCEDURE(), '-');
END CATCH

--Sales Commissions
UPDATE 
	PageNavigationDTL 
SET 
	numParentID=(SELECT TOP 1 numPageNavID FROM PageNavigationDTL WHERE vcPageNavName='Human Resources'),
	numModuleID=(SELECT TOP 1 numModuleID FROM ModuleMaster WHERE vcModuleName='Human Resources'),
	numTabID=(SELECT TOP 1 numTabId FROM TabMaster WHERE numTabName='Human Resources' AND numDomainID=1),
	vcPageNavName='Sales Commissions',
	vcImageURL=NULL
WHERE 
	vcPageNavName='Commission Rules'

DELETE FROM  TreeNavigationAuthorization  WHERE numTabID=-1 AND numPageNavId=(SELECT TOP 1 numPageNavID FROM PageNavigationDTL WHERE vcPageNavName='Sales Commissions')
INSERT INTO dbo.TreeNavigationAuthorization
	(
		numGroupID,
		numTabID,
		numPageNavID,
		bitVisible,
		numDomainID,
		tintType
	)
	SELECT  
		numGroupID,
		(SELECT TOP 1 numTabId FROM TabMaster WHERE numTabName='Human Resources' AND numDomainID=1),
		(SELECT TOP 1 numPageNavID FROM PageNavigationDTL WHERE vcPageNavName='Sales Commissions'),
		1,
		numDomainID,
		1
	FROM    
		AuthenticationGroupMaster
	WHERE
		vcGroupName='System Administrator'

--Payroll
UPDATE 
	PageNavigationDTL 
SET 
	numParentID=(SELECT TOP 1 numPageNavID FROM PageNavigationDTL WHERE vcPageNavName='Human Resources'),
	numModuleID=(SELECT TOP 1 numModuleID FROM ModuleMaster WHERE vcModuleName='Human Resources'),
	numTabID=(SELECT TOP 1 numTabId FROM TabMaster WHERE numTabName='Human Resources' AND numDomainID=1),
	vcPageNavName='Payroll',
	vcImageURL=NULL
WHERE 
	vcPageNavName='Payroll Expenses'

DELETE FROM  TreeNavigationAuthorization  WHERE numPageNavId=(SELECT TOP 1 numPageNavID FROM PageNavigationDTL WHERE vcPageNavName='Payroll')
INSERT INTO dbo.TreeNavigationAuthorization
	(
		numGroupID,
		numTabID,
		numPageNavID,
		bitVisible,
		numDomainID,
		tintType
	)
	SELECT  
		numGroupID,
		(SELECT TOP 1 numTabId FROM TabMaster WHERE numTabName='Human Resources' AND numDomainID=1),
		(SELECT TOP 1 numPageNavID FROM PageNavigationDTL WHERE vcPageNavName='Payroll'),
		1,
		numDomainID,
		1
	FROM    
		AuthenticationGroupMaster
	WHERE
		vcGroupName='System Administrator'

--Payroll History
UPDATE 
	PageNavigationDTL 
SET 
	numParentID=(SELECT TOP 1 numPageNavID FROM PageNavigationDTL WHERE vcPageNavName='Payroll'),
	numModuleID=(SELECT TOP 1 numModuleID FROM ModuleMaster WHERE vcModuleName='Human Resources'),
	numTabID=(SELECT TOP 1 numTabId FROM TabMaster WHERE numTabName='Human Resources' AND numDomainID=1),
	vcImageURL=NULL
WHERE 
	vcPageNavName='Payroll History'

DELETE FROM  TreeNavigationAuthorization  WHERE numPageNavId=(SELECT TOP 1 numPageNavID FROM PageNavigationDTL WHERE vcPageNavName='Payroll History')
INSERT INTO dbo.TreeNavigationAuthorization
	(
		numGroupID,
		numTabID,
		numPageNavID,
		bitVisible,
		numDomainID,
		tintType
	)
	SELECT  
		numGroupID,
		(SELECT TOP 1 numTabId FROM TabMaster WHERE numTabName='Human Resources' AND numDomainID=1),
		(SELECT TOP 1 numPageNavID FROM PageNavigationDTL WHERE vcPageNavName='Payroll History'),
		1,
		numDomainID,
		1
	FROM    
		AuthenticationGroupMaster
	WHERE
		vcGroupName='System Administrator'

--Resource Planning

BEGIN TRY
BEGIN TRANSACTION

	DECLARE @numMAXPageNavID INT
	SELECT @numMAXPageNavID = MAX(numPageNavID) FROM PageNavigationDTL


	INSERT INTO PageNavigationDTL
	(
		numPageNavID,numModuleID,numParentID,vcPageNavName,vcNavURL,bitVisible,numTabID
	)
	VALUES
	(
		(@numMAXPageNavID + 1),(SELECT TOP 1 numModuleID FROM ModuleMaster WHERE vcModuleName='Human Resources'),(SELECT TOP 1 numPageNavID FROM PageNavigationDTL WHERE vcPageNavName='Human Resources')
		,'Resource Planning','../reports/frmCapacityPlanningResources.aspx',1,(SELECT TOP 1 numTabId FROM TabMaster WHERE numTabName='Human Resources' AND numDomainID=1)
	)

	INSERT INTO dbo.TreeNavigationAuthorization
	(
		numGroupID,
		numTabID,
		numPageNavID,
		bitVisible,
		numDomainID,
		tintType
	)
	SELECT  
		numGroupID,
		(SELECT TOP 1 numTabId FROM TabMaster WHERE numTabName='Human Resources' AND numDomainID=1),
		(@numMAXPageNavID + 1),
		1,
		numDomainID,
		1
	FROM    
		AuthenticationGroupMaster
	WHERE
		vcGroupName='System Administrator'
COMMIT
END TRY
BEGIN CATCH
	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;

	SELECT 
		ERROR_MESSAGE(),
		ERROR_NUMBER(),
		ERROR_SEVERITY(),
		ERROR_STATE(),
		ERROR_LINE(),
		ISNULL(ERROR_PROCEDURE(), '-');
END CATCH

------------------------------------------------PROCUREMENT------------------------------------------------------------
INSERT INTO TabMaster(numTabName, Remarks, tintTabType, vcURL, bitFixed, numDomainID, vcImage, vcAddURL, bitAddIsPopUp)
VALUES('Procurement','Procurement',1,'',1,1,NULL,NULL,NULL)

INSERT INTO GroupTabDetails(numGroupId, numTabId, numRelationShip, bitallowed, numOrder, numProfileID, tintType, bitInitialTab)
SELECT  
	numGroupID,(SELECT TOP 1 numTabId FROM TabMaster WHERE numTabName='Procurement' AND numDomainID=1),
	0,1,16,0,0,0
FROM    
	AuthenticationGroupMaster
WHERE
	vcGroupName='System Administrator'

INSERT INTO ModuleMaster(vcModuleName,tintGroupType)VALUES('Procurement',1)

BEGIN TRY
BEGIN TRANSACTION

	DECLARE @numMAXPageNavID INT
	SELECT @numMAXPageNavID = MAX(numPageNavID) FROM PageNavigationDTL


	INSERT INTO PageNavigationDTL
	(
		numPageNavID,numModuleID,numParentID,vcPageNavName,vcNavURL,bitVisible,numTabID
	)
	VALUES
	(
		(@numMAXPageNavID + 1),(SELECT TOP 1 numModuleID FROM ModuleMaster WHERE vcModuleName='Procurement'),0,'Procurement','',1,(SELECT TOP 1 numTabId FROM TabMaster WHERE numTabName='Procurement' AND numDomainID=1)
	)

	INSERT INTO dbo.TreeNavigationAuthorization
	(
		numGroupID,
		numTabID,
		numPageNavID,
		bitVisible,
		numDomainID,
		tintType
	)
	SELECT  
		numGroupID,
		(SELECT TOP 1 numTabId FROM TabMaster WHERE numTabName='Procurement' AND numDomainID=1),
		(@numMAXPageNavID + 1),
		1,
		numDomainID,
		1
	FROM    
		AuthenticationGroupMaster
	WHERE
		vcGroupName='System Administrator'
COMMIT
END TRY
BEGIN CATCH
	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;

	SELECT 
		ERROR_MESSAGE(),
		ERROR_NUMBER(),
		ERROR_SEVERITY(),
		ERROR_STATE(),
		ERROR_LINE(),
		ISNULL(ERROR_PROCEDURE(), '-');
END CATCH

--Purchase Orders
UPDATE 
	PageNavigationDTL 
SET 
	numParentID=(SELECT TOP 1 numPageNavID FROM PageNavigationDTL WHERE vcPageNavName='Procurement'),
	numModuleID=(SELECT TOP 1 numModuleID FROM ModuleMaster WHERE vcModuleName='Procurement'),
	numTabID=(SELECT TOP 1 numTabId FROM TabMaster WHERE numTabName='Procurement' AND numDomainID=1)
WHERE 
	vcPageNavName='Purchase Orders'
UPDATE TreeNavigationAuthorization SET numTabID= (SELECT TOP 1 numTabId FROM TabMaster WHERE numTabName='Procurement' AND numDomainID=1) WHERE 
numPageNavID=(SELECT TOP 1 numPageNavID FROM PageNavigationDTL WHERE vcPageNavName='Purchase Orders')

--Purchase Fulfillment
UPDATE 
	PageNavigationDTL 
SET 
	numParentID=(SELECT TOP 1 numPageNavID FROM PageNavigationDTL WHERE vcPageNavName='Procurement'),
	numModuleID=(SELECT TOP 1 numModuleID FROM ModuleMaster WHERE vcModuleName='Procurement'),
	numTabID=(SELECT TOP 1 numTabId FROM TabMaster WHERE numTabName='Procurement' AND numDomainID=1)
WHERE 
	vcPageNavName='Purchase Fulfillment'
UPDATE TreeNavigationAuthorization SET numTabID= (SELECT TOP 1 numTabId FROM TabMaster WHERE numTabName='Procurement' AND numDomainID=1) WHERE 
numPageNavID=(SELECT TOP 1 numPageNavID FROM PageNavigationDTL WHERE vcPageNavName='Purchase Fulfillment')

--Requisitions / OLD Purchase Opportunities
UPDATE 
	PageNavigationDTL 
SET 
	numParentID=(SELECT TOP 1 numPageNavID FROM PageNavigationDTL WHERE vcPageNavName='Procurement'),
	numModuleID=(SELECT TOP 1 numModuleID FROM ModuleMaster WHERE vcModuleName='Procurement'),
	numTabID=(SELECT TOP 1 numTabId FROM TabMaster WHERE numTabName='Procurement' AND numDomainID=1),
	vcPageNavName='Requisitions'
WHERE 
	vcPageNavName='Purchase Opportunities'
UPDATE TreeNavigationAuthorization SET numTabID= (SELECT TOP 1 numTabId FROM TabMaster WHERE numTabName='Procurement' AND numDomainID=1) WHERE 
numPageNavID=(SELECT TOP 1 numPageNavID FROM PageNavigationDTL WHERE vcPageNavName='Requisitions')

--Demand Planning / OLD Planning & Procurement
UPDATE 
	PageNavigationDTL 
SET 
	numParentID=(SELECT TOP 1 numPageNavID FROM PageNavigationDTL WHERE vcPageNavName='Procurement'),
	numModuleID=(SELECT TOP 1 numModuleID FROM ModuleMaster WHERE vcModuleName='Procurement'),
	numTabID=(SELECT TOP 1 numTabId FROM TabMaster WHERE numTabName='Procurement' AND numDomainID=1),
	vcPageNavName='Demand Planning'
WHERE 
	vcPageNavName='Planning & Procurement'
UPDATE TreeNavigationAuthorization SET numTabID= (SELECT TOP 1 numTabId FROM TabMaster WHERE numTabName='Procurement' AND numDomainID=1) WHERE 
numPageNavID=(SELECT TOP 1 numPageNavID FROM PageNavigationDTL WHERE vcPageNavName='Demand Planning')


--Demand Planning / OLD Planning & Procurement
UPDATE 
	PageNavigationDTL 
SET 
	numParentID=(SELECT TOP 1 numPageNavID FROM PageNavigationDTL WHERE vcPageNavName='Procurement'),
	numModuleID=(SELECT TOP 1 numModuleID FROM ModuleMaster WHERE vcModuleName='Procurement'),
	numTabID=(SELECT TOP 1 numTabId FROM TabMaster WHERE numTabName='Procurement' AND numDomainID=1)
WHERE 
	vcPageNavName='Purchase Returns'
UPDATE TreeNavigationAuthorization SET numTabID= (SELECT TOP 1 numTabId FROM TabMaster WHERE numTabName='Procurement' AND numDomainID=1) WHERE 
numPageNavID=(SELECT TOP 1 numPageNavID FROM PageNavigationDTL WHERE vcPageNavName='Purchase Returns')

--Sales Order Opportunity/Order
UPDATE TabMaster SET numTabName='Orders & Quotes' where numTabName='Orders'

--Contacts
INSERT INTO TabMaster(numTabName, Remarks, tintTabType, vcURL, bitFixed, numDomainID, vcImage, vcAddURL, bitAddIsPopUp)
VALUES('Contacts','Contacts',1,'Contact/frmContactList.aspx',1,1,NULL,'Contact/newcontact.aspx',1)


INSERT INTO GroupTabDetails(numGroupId, numTabId, numRelationShip, bitallowed, numOrder, numProfileID, tintType, bitInitialTab)
SELECT  
	numGroupID,(SELECT TOP 1 numTabId FROM TabMaster WHERE numTabName='Contacts' AND numDomainID=1 AND tintTabType=1),
	0,1,15,0,0,0
FROM    
	AuthenticationGroupMaster
WHERE
	vcGroupName='System Administrator'

UPDATE 
	PageNavigationDTL 
SET 
	numParentID=0,
	numModuleID=(SELECT TOP 1 numModuleID FROM ModuleMaster WHERE vcModuleName='Contacts'),
	numTabID=(SELECT TOP 1 numTabId FROM TabMaster WHERE numTabName='Contacts' AND numDomainID=1 AND tintTabType=1)
WHERE 
	vcPageNavName='Contacts'

UPDATE TreeNodeOrder SET numTabID=(SELECT TOP 1 numTabId FROM TabMaster WHERE numTabName='Contacts' AND numDomainID=1 AND tintTabType=1)
WHERE numParentID=13

UPDATE TreeNavigationAuthorization SET numTabID= (SELECT TOP 1 numTabId FROM TabMaster WHERE numTabName='Contacts' AND numDomainID=1 AND tintTabType=1) WHERE 
numPageNavID=(SELECT TOP 1 numPageNavID FROM PageNavigationDTL WHERE vcPageNavName='Contacts')

DELETE FROM PageNavigationDTL WHERE vcPageNavName='User Administration'


------------------------------------------------Manufacturing------------------------------------------------------------
INSERT INTO TabMaster(numTabName, Remarks, tintTabType, vcURL, bitFixed, numDomainID, vcImage, vcAddURL, bitAddIsPopUp)
VALUES('Manufacturing','Manufacturing',1,'',1,1,NULL,NULL,NULL)

INSERT INTO GroupTabDetails(numGroupId, numTabId, numRelationShip, bitallowed, numOrder, numProfileID, tintType, bitInitialTab)
SELECT  
	numGroupID,(SELECT TOP 1 numTabId FROM TabMaster WHERE numTabName='Manufacturing' AND numDomainID=1),
	0,1,16,0,0,0
FROM    
	AuthenticationGroupMaster
WHERE
	vcGroupName='System Administrator'

INSERT INTO ModuleMaster(vcModuleName,tintGroupType)VALUES('Manufacturing',1)

BEGIN TRY
BEGIN TRANSACTION

	DECLARE @numMAXPageNavID INT
	SELECT @numMAXPageNavID = MAX(numPageNavID) FROM PageNavigationDTL


	INSERT INTO PageNavigationDTL
	(
		numPageNavID,numModuleID,numParentID,vcPageNavName,vcNavURL,bitVisible,numTabID
	)
	VALUES
	(
		(@numMAXPageNavID + 1),(SELECT TOP 1 numModuleID FROM ModuleMaster WHERE vcModuleName='Manufacturing'),0,'Manufacturing','',1,(SELECT TOP 1 numTabId FROM TabMaster WHERE numTabName='Manufacturing' AND numDomainID=1)
	)

	INSERT INTO dbo.TreeNavigationAuthorization
	(
		numGroupID,
		numTabID,
		numPageNavID,
		bitVisible,
		numDomainID,
		tintType
	)
	SELECT  
		numGroupID,
		(SELECT TOP 1 numTabId FROM TabMaster WHERE numTabName='Manufacturing' AND numDomainID=1),
		(@numMAXPageNavID + 1),
		1,
		numDomainID,
		1
	FROM    
		AuthenticationGroupMaster
	WHERE
		vcGroupName='System Administrator'
COMMIT
END TRY
BEGIN CATCH
	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;

	SELECT 
		ERROR_MESSAGE(),
		ERROR_NUMBER(),
		ERROR_SEVERITY(),
		ERROR_STATE(),
		ERROR_LINE(),
		ISNULL(ERROR_PROCEDURE(), '-');
END CATCH

------------------WORK ORDER--------------------
BEGIN TRY
BEGIN TRANSACTION

	DECLARE @numMAXPageNavID INT
	SELECT @numMAXPageNavID = MAX(numPageNavID) FROM PageNavigationDTL


	INSERT INTO PageNavigationDTL
	(
		numPageNavID,numModuleID,numParentID,vcPageNavName,vcNavURL,bitVisible,numTabID,vcAddURL,bitAddIsPopup
	)
	VALUES
	(
		(@numMAXPageNavID + 1),(SELECT TOP 1 numModuleID FROM ModuleMaster WHERE vcModuleName='Manufacturing'),(SELECT TOP 1 numPageNavID FROM PageNavigationDTL WHERE vcPageNavName='Manufacturing'),'Work Orders','../Opportunity/frmWorkOrderList.aspx',1,
		(SELECT TOP 1 numTabId FROM TabMaster WHERE numTabName='Manufacturing' AND numDomainID=1),'~/Items/frmManageAssembly.aspx',1
	)

	INSERT INTO dbo.TreeNavigationAuthorization
	(
		numGroupID,
		numTabID,
		numPageNavID,
		bitVisible,
		numDomainID,
		tintType
	)
	SELECT  
		numGroupID,
		(SELECT TOP 1 numTabId FROM TabMaster WHERE numTabName='Manufacturing' AND numDomainID=1),
		(@numMAXPageNavID + 1),
		1,
		numDomainID,
		1
	FROM    
		AuthenticationGroupMaster
	WHERE
		vcGroupName='System Administrator'

	UPDATE PageNavigationDTL SET vcAddURL='~/Items/frmManageAssembly.aspx' WHERE numPageNavId=(SELECT TOP 1 numPageNavID FROM PageNavigationDTL WHERE vcPageNavName='Work Orders')
COMMIT
END TRY
BEGIN CATCH
	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;

	SELECT 
		ERROR_MESSAGE(),
		ERROR_NUMBER(),
		ERROR_SEVERITY(),
		ERROR_STATE(),
		ERROR_LINE(),
		ISNULL(ERROR_PROCEDURE(), '-');
END CATCH

------------------Production Planning--------------------
BEGIN TRY
BEGIN TRANSACTION

	DECLARE @numMAXPageNavID INT
	SELECT @numMAXPageNavID = MAX(numPageNavID) FROM PageNavigationDTL


	INSERT INTO PageNavigationDTL
	(
		numPageNavID,numModuleID,numParentID,vcPageNavName,vcNavURL,bitVisible,numTabID
	)
	VALUES
	(
		(@numMAXPageNavID + 1),(SELECT TOP 1 numModuleID FROM ModuleMaster WHERE vcModuleName='Manufacturing'),(SELECT TOP 1 numPageNavID FROM PageNavigationDTL WHERE vcPageNavName='Manufacturing'),'Production Planning','../reports/frmProductionPlanning.aspx?type=3',1,
		(SELECT TOP 1 numTabId FROM TabMaster WHERE numTabName='Manufacturing' AND numDomainID=1)
	)

	INSERT INTO dbo.TreeNavigationAuthorization
	(
		numGroupID,
		numTabID,
		numPageNavID,
		bitVisible,
		numDomainID,
		tintType
	)
	SELECT  
		numGroupID,
		(SELECT TOP 1 numTabId FROM TabMaster WHERE numTabName='Manufacturing' AND numDomainID=1),
		(@numMAXPageNavID + 1),
		1,
		numDomainID,
		1
	FROM    
		AuthenticationGroupMaster
	WHERE
		vcGroupName='System Administrator'
COMMIT
END TRY
BEGIN CATCH
	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;

	SELECT 
		ERROR_MESSAGE(),
		ERROR_NUMBER(),
		ERROR_SEVERITY(),
		ERROR_STATE(),
		ERROR_LINE(),
		ISNULL(ERROR_PROCEDURE(), '-');
END CATCH

------------------Capacity Planning--------------------
BEGIN TRY
BEGIN TRANSACTION

	DECLARE @numMAXPageNavID INT
	SELECT @numMAXPageNavID = MAX(numPageNavID) FROM PageNavigationDTL


	INSERT INTO PageNavigationDTL
	(
		numPageNavID,numModuleID,numParentID,vcPageNavName,vcNavURL,bitVisible,numTabID
	)
	VALUES
	(
		(@numMAXPageNavID + 1),(SELECT TOP 1 numModuleID FROM ModuleMaster WHERE vcModuleName='Manufacturing'),(SELECT TOP 1 numPageNavID FROM PageNavigationDTL WHERE vcPageNavName='Manufacturing'),'Capacity Planning','../reports/frmCapacityPlanningTasks.aspx?type=3',1,
		(SELECT TOP 1 numTabId FROM TabMaster WHERE numTabName='Manufacturing' AND numDomainID=1)
	)

	INSERT INTO dbo.TreeNavigationAuthorization
	(
		numGroupID,
		numTabID,
		numPageNavID,
		bitVisible,
		numDomainID,
		tintType
	)
	SELECT  
		numGroupID,
		(SELECT TOP 1 numTabId FROM TabMaster WHERE numTabName='Manufacturing' AND numDomainID=1),
		(@numMAXPageNavID + 1),
		1,
		numDomainID,
		1
	FROM    
		AuthenticationGroupMaster
	WHERE
		vcGroupName='System Administrator'

COMMIT
END TRY
BEGIN CATCH
	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;

	SELECT 
		ERROR_MESSAGE(),
		ERROR_NUMBER(),
		ERROR_SEVERITY(),
		ERROR_STATE(),
		ERROR_LINE(),
		ISNULL(ERROR_PROCEDURE(), '-');
END CATCH

------------------Performance Evaluation--------------------
BEGIN TRY
BEGIN TRANSACTION

	DECLARE @numMAXPageNavID INT
	SELECT @numMAXPageNavID = MAX(numPageNavID) FROM PageNavigationDTL


	INSERT INTO PageNavigationDTL
	(
		numPageNavID,numModuleID,numParentID,vcPageNavName,vcNavURL,bitVisible,numTabID
	)
	VALUES
	(
		(@numMAXPageNavID + 1),(SELECT TOP 1 numModuleID FROM ModuleMaster WHERE vcModuleName='Manufacturing'),(SELECT TOP 1 numPageNavID FROM PageNavigationDTL WHERE vcPageNavName='Manufacturing'),'Performance Evaluation','../reports/frmPerformanceEvaluation.aspx?type=3',1,
		(SELECT TOP 1 numTabId FROM TabMaster WHERE numTabName='Manufacturing' AND numDomainID=1)
	)

	INSERT INTO dbo.TreeNavigationAuthorization
	(
		numGroupID,
		numTabID,
		numPageNavID,
		bitVisible,
		numDomainID,
		tintType
	)
	SELECT  
		numGroupID,
		(SELECT TOP 1 numTabId FROM TabMaster WHERE numTabName='Manufacturing' AND numDomainID=1),
		(@numMAXPageNavID + 1),
		1,
		numDomainID,
		1
	FROM    
		AuthenticationGroupMaster
	WHERE
		vcGroupName='System Administrator'

	UPDATE PageNavigationDTL SET vcAddURL='~/Items/frmManageAssembly.aspx' WHERE numPageNavId=(SELECT TOP 1 numPageNavID FROM PageNavigationDTL WHERE vcPageNavName='Work Orders')
COMMIT
END TRY
BEGIN CATCH
	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;

	SELECT 
		ERROR_MESSAGE(),
		ERROR_NUMBER(),
		ERROR_SEVERITY(),
		ERROR_STATE(),
		ERROR_LINE(),
		ISNULL(ERROR_PROCEDURE(), '-');
END CATCH

------------------------------------------------------PROJECTS-----------------------------------------------------------------------

------------------PROJECT--------------------
UPDATE TabMaster SET vcURL='',vcAddURL='',bitAddIsPopup=0 WHERE numTabName='Projects' AND tintTabType=1 
BEGIN TRY
BEGIN TRANSACTION

	DECLARE @numMAXPageNavID INT
	SELECT @numMAXPageNavID = MAX(numPageNavID) FROM PageNavigationDTL


	INSERT INTO PageNavigationDTL
	(
		numPageNavID,numModuleID,numParentID,vcPageNavName,vcNavURL,bitVisible,numTabID,vcAddURL,bitAddIsPopup
	)
	VALUES
	(
		(@numMAXPageNavID + 1),(SELECT TOP 1 numModuleID FROM ModuleMaster WHERE vcModuleName='Projects'),0,'Projects','',1,
		(SELECT TOP 1 numTabId FROM TabMaster WHERE numTabName='Projects' AND numDomainID=1),'',0
	)

	INSERT INTO dbo.TreeNavigationAuthorization
	(
		numGroupID,
		numTabID,
		numPageNavID,
		bitVisible,
		numDomainID,
		tintType
	)
	SELECT  
		numGroupID,
		(SELECT TOP 1 numTabId FROM TabMaster WHERE numTabName='Projects' AND numDomainID=1),
		(@numMAXPageNavID + 1),
		1,
		numDomainID,
		1
	FROM    
		AuthenticationGroupMaster
	WHERE
		vcGroupName='System Administrator'

COMMIT
END TRY
BEGIN CATCH
	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;

	SELECT 
		ERROR_MESSAGE(),
		ERROR_NUMBER(),
		ERROR_SEVERITY(),
		ERROR_STATE(),
		ERROR_LINE(),
		ISNULL(ERROR_PROCEDURE(), '-');
END CATCH

UPDATE  PageNavigationDTL SET vcNavURL='',vcAddURL='',bitAddIsPopup=0 WHERE vcPageNavName='Projects' AND numParentID=0

BEGIN TRY
BEGIN TRANSACTION

	DECLARE @numMAXPageNavID INT
	SELECT @numMAXPageNavID = MAX(numPageNavID) FROM PageNavigationDTL


	INSERT INTO PageNavigationDTL
	(
		numPageNavID,numModuleID,numParentID,vcPageNavName,vcNavURL,bitVisible,numTabID,vcAddURL,bitAddIsPopup
	)
	VALUES
	(
		(@numMAXPageNavID + 1),(SELECT TOP 1 numModuleID FROM ModuleMaster WHERE vcModuleName='Projects'),(SELECT TOP 1 numPageNavID FROM PageNavigationDTL WHERE vcPageNavName='Projects'),'Projects','../projects/frmProjectList.aspx',1,
		(SELECT TOP 1 numTabId FROM TabMaster WHERE numTabName='Projects' AND numDomainID=1),'~/Projects/frmProjectAdd.aspx',1
	)

	INSERT INTO dbo.TreeNavigationAuthorization
	(
		numGroupID,
		numTabID,
		numPageNavID,
		bitVisible,
		numDomainID,
		tintType
	)
	SELECT  
		numGroupID,
		(SELECT TOP 1 numTabId FROM TabMaster WHERE numTabName='Projects' AND numDomainID=1),
		(@numMAXPageNavID + 1),
		1,
		numDomainID,
		1
	FROM    
		AuthenticationGroupMaster
	WHERE
		vcGroupName='System Administrator'

COMMIT
END TRY
BEGIN CATCH
	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;

	SELECT 
		ERROR_MESSAGE(),
		ERROR_NUMBER(),
		ERROR_SEVERITY(),
		ERROR_STATE(),
		ERROR_LINE(),
		ISNULL(ERROR_PROCEDURE(), '-');
END CATCH

------------------Production Planning--------------------
BEGIN TRY
BEGIN TRANSACTION

	DECLARE @numMAXPageNavID INT
	SELECT @numMAXPageNavID = MAX(numPageNavID) FROM PageNavigationDTL


	INSERT INTO PageNavigationDTL
	(
		numPageNavID,numModuleID,numParentID,vcPageNavName,vcNavURL,bitVisible,numTabID
	)
	VALUES
	(
		(@numMAXPageNavID + 1),(SELECT TOP 1 numModuleID FROM ModuleMaster WHERE vcModuleName='Projects'),(SELECT TOP 1 numPageNavID FROM PageNavigationDTL WHERE vcPageNavName='Projects' AND numParentID=0),'Production Planning','../reports/frmProductionPlanning.aspx?type=1',1,
		(SELECT TOP 1 numTabId FROM TabMaster WHERE numTabName='Projects' AND numDomainID=1)
	)

	INSERT INTO dbo.TreeNavigationAuthorization
	(
		numGroupID,
		numTabID,
		numPageNavID,
		bitVisible,
		numDomainID,
		tintType
	)
	SELECT  
		numGroupID,
		(SELECT TOP 1 numTabId FROM TabMaster WHERE numTabName='Projects' AND numDomainID=1),
		(@numMAXPageNavID + 1),
		1,
		numDomainID,
		1
	FROM    
		AuthenticationGroupMaster
	WHERE
		vcGroupName='System Administrator'
COMMIT
END TRY
BEGIN CATCH
	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;

	SELECT 
		ERROR_MESSAGE(),
		ERROR_NUMBER(),
		ERROR_SEVERITY(),
		ERROR_STATE(),
		ERROR_LINE(),
		ISNULL(ERROR_PROCEDURE(), '-');
END CATCH

------------------Capacity Planning--------------------
BEGIN TRY
BEGIN TRANSACTION

	DECLARE @numMAXPageNavID INT
	SELECT @numMAXPageNavID = MAX(numPageNavID) FROM PageNavigationDTL


	INSERT INTO PageNavigationDTL
	(
		numPageNavID,numModuleID,numParentID,vcPageNavName,vcNavURL,bitVisible,numTabID
	)
	VALUES
	(
		(@numMAXPageNavID + 1),(SELECT TOP 1 numModuleID FROM ModuleMaster WHERE vcModuleName='Projects'),(SELECT TOP 1 numPageNavID FROM PageNavigationDTL WHERE vcPageNavName='Projects'  AND numParentID=0),'Capacity Planning','../reports/frmCapacityPlanningTasks.aspx?type=1',1,
		(SELECT TOP 1 numTabId FROM TabMaster WHERE numTabName='Projects' AND numDomainID=1)
	)

	INSERT INTO dbo.TreeNavigationAuthorization
	(
		numGroupID,
		numTabID,
		numPageNavID,
		bitVisible,
		numDomainID,
		tintType
	)
	SELECT  
		numGroupID,
		(SELECT TOP 1 numTabId FROM TabMaster WHERE numTabName='Projects' AND numDomainID=1),
		(@numMAXPageNavID + 1),
		1,
		numDomainID,
		1
	FROM    
		AuthenticationGroupMaster
	WHERE
		vcGroupName='System Administrator'

COMMIT
END TRY
BEGIN CATCH
	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;

	SELECT 
		ERROR_MESSAGE(),
		ERROR_NUMBER(),
		ERROR_SEVERITY(),
		ERROR_STATE(),
		ERROR_LINE(),
		ISNULL(ERROR_PROCEDURE(), '-');
END CATCH

------------------Performance Evaluation--------------------
BEGIN TRY
BEGIN TRANSACTION

	DECLARE @numMAXPageNavID INT
	SELECT @numMAXPageNavID = MAX(numPageNavID) FROM PageNavigationDTL


	INSERT INTO PageNavigationDTL
	(
		numPageNavID,numModuleID,numParentID,vcPageNavName,vcNavURL,bitVisible,numTabID
	)
	VALUES
	(
		(@numMAXPageNavID + 1),(SELECT TOP 1 numModuleID FROM ModuleMaster WHERE vcModuleName='Projects'),(SELECT TOP 1 numPageNavID FROM PageNavigationDTL WHERE vcPageNavName='Projects'  AND numParentID=0),'Performance Evaluation','../reports/frmPerformanceEvaluation.aspx?type=1',1,
		(SELECT TOP 1 numTabId FROM TabMaster WHERE numTabName='Projects' AND numDomainID=1)
	)

	INSERT INTO dbo.TreeNavigationAuthorization
	(
		numGroupID,
		numTabID,
		numPageNavID,
		bitVisible,
		numDomainID,
		tintType
	)
	SELECT  
		numGroupID,
		(SELECT TOP 1 numTabId FROM TabMaster WHERE numTabName='Projects' AND numDomainID=1),
		(@numMAXPageNavID + 1),
		1,
		numDomainID,
		1
	FROM    
		AuthenticationGroupMaster
	WHERE
		vcGroupName='System Administrator'

	UPDATE PageNavigationDTL SET vcAddURL='~/Items/frmManageAssembly.aspx' WHERE numPageNavId=(SELECT TOP 1 numPageNavID FROM PageNavigationDTL WHERE vcPageNavName='Work Orders')
COMMIT
END TRY
BEGIN CATCH
	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;

	SELECT 
		ERROR_MESSAGE(),
		ERROR_NUMBER(),
		ERROR_SEVERITY(),
		ERROR_STATE(),
		ERROR_LINE(),
		ISNULL(ERROR_PROCEDURE(), '-');
END CATCH
------------------------------------------------END MENU WORK------------------------------------------------------------------------

ALTER TABLE ProjectsMaster ADD numCompletedBy NUMERIC(18,0)
UPDATE DycFormField_Mapping SET vcFieldName = 'Project Manager' WHERE vcFieldName='Internal Project Manager' AND numFormID=13
UPDATE DycFormField_Mapping SET vcFieldName = 'Project Site' WHERE vcFieldName='Shipping Address' AND numFormID=13
ALTER TABLE ProjectsMaster Add numClientBudget NUMERIC(18,2) DEFAULT 0
DECLARE @numFieldID NUMERIC(18,0)
INSERT INTO DycFieldMaster
(
	numModuleID,vcFieldName,vcDbColumnName,vcOrigDbColumnName,vcPropertyName,vcLookBackTableName,vcFieldDataType,vcFieldType,vcAssociatedControlType,vcListItemType,numListID,bitInResults,bitDeleted,bitAllowEdit,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitWorkFlowField,bitAllowFiltering
)
VALUES
(
	5,'Billed (Balance)','ClientBudgetBalance','ClientBudgetBalance','ClientBudgetBalance','ProjectsMaster','V','R','Label','',0,1,0,0,0,1,0,1,1,1,1
)

SET @numFieldID = SCOPE_IDENTITY()

INSERT INTO DycFormField_Mapping
(
	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,bitInResults,bitDeleted,bitDefault,bitSettingField,bitAddField,bitDetailField
)
VALUES
(
	5,@numFieldID,13,0,0,'Billed (Balance)','Label',1,0,0,1,0,1
)
---------------------------------------------------------------------------
DECLARE @numFieldID NUMERIC(18,0)
INSERT INTO DycFieldMaster
(
	numModuleID,vcFieldName,vcDbColumnName,vcOrigDbColumnName,vcPropertyName,vcLookBackTableName,vcFieldDataType,vcFieldType,vcAssociatedControlType,vcListItemType,numListID,bitInResults,bitDeleted,bitAllowEdit,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitWorkFlowField,bitAllowFiltering
)
VALUES
(
	5,'Client Budget','ClientBudget','ClientBudget','ClientBudget','ProjectsMaster','V','R','Label','',0,1,0,0,0,1,0,1,1,1,1
)

SET @numFieldID = SCOPE_IDENTITY()

INSERT INTO DycFormField_Mapping
(
	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,bitInResults,bitDeleted,bitDefault,bitSettingField,bitAddField,bitDetailField
)
VALUES
(
	5,@numFieldID,13,0,0,'Client Budget','Label',1,0,0,1,0,1
)
----------------------------------------------------------------------
DECLARE @numFieldID NUMERIC(18,0)
INSERT INTO DycFieldMaster
(
	numModuleID,vcFieldName,vcDbColumnName,vcOrigDbColumnName,vcPropertyName,vcLookBackTableName,vcFieldDataType,vcFieldType,vcAssociatedControlType,vcListItemType,numListID,bitInResults,bitDeleted,bitAllowEdit,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitWorkFlowField,bitAllowFiltering
)
VALUES
(
	5,'Planned Start Date','dtmStartDate','dtmStartDate','PlannedStartDate','ProjectsMaster','V','R','DateField','',0,1,0,1,1,1,1,1,1,1,1
)

SET @numFieldID = SCOPE_IDENTITY()

INSERT INTO DycFormField_Mapping
(
	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,bitInResults,bitDeleted,bitDefault,bitSettingField,bitAddField,bitDetailField
)
VALUES
(
	5,@numFieldID,13,1,1,'Planned Start Date','DateField',1,0,1,1,1,1
)

------------------------------------------------------------------------

DECLARE @numFieldID NUMERIC(18,0)
INSERT INTO DycFieldMaster
(
	numModuleID,vcFieldName,vcDbColumnName,vcOrigDbColumnName,vcPropertyName,vcLookBackTableName,vcFieldDataType,vcFieldType,vcAssociatedControlType,vcListItemType,numListID,bitInResults,bitDeleted,bitAllowEdit,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitWorkFlowField,bitAllowFiltering
)
VALUES
(
	5,'Requested Finish','dtmEndDate','dtmEndDate','RequestedFinish','ProjectsMaster','V','R','DateField','',0,1,0,1,1,1,1,1,1,1,1
)

SET @numFieldID = SCOPE_IDENTITY()

INSERT INTO DycFormField_Mapping
(
	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,bitInResults,bitDeleted,bitDefault,bitSettingField,bitAddField,bitDetailField
)
VALUES
(
	5,@numFieldID,13,1,1,'Requested Finish','DateField',1,0,1,1,1,1
)

------------------------------------------------------------------------

DECLARE @numFieldID NUMERIC(18,0)
INSERT INTO DycFieldMaster
(
	numModuleID,vcFieldName,vcDbColumnName,vcOrigDbColumnName,vcPropertyName,vcLookBackTableName,vcFieldDataType,vcFieldType,vcAssociatedControlType,vcListItemType,numListID,bitInResults,bitDeleted,bitAllowEdit,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitWorkFlowField,bitAllowFiltering
)
VALUES
(
	5,'Actual Start','dtActualStartDate','dtActualStartDate','ActualStartDate','ProjectsMaster','V','R','DateField','',0,1,0,0,0,1,0,1,1,1,1
)

SET @numFieldID = SCOPE_IDENTITY()

INSERT INTO DycFormField_Mapping
(
	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,bitInResults,bitDeleted,bitDefault,bitSettingField,bitAddField,bitDetailField
)
VALUES
(
	5,@numFieldID,13,0,0,'Actual Start','DateField',1,0,0,1,0,1
)


------------------------------------------------------------------------

DECLARE @numFieldID NUMERIC(18,0)
INSERT INTO DycFieldMaster
(
	numModuleID,vcFieldName,vcDbColumnName,vcOrigDbColumnName,vcPropertyName,vcLookBackTableName,vcFieldDataType,vcFieldType,vcAssociatedControlType,vcListItemType,numListID,bitInResults,bitDeleted,bitAllowEdit,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitWorkFlowField,bitAllowFiltering
)
VALUES
(
	5,'Projected Finish','vcProjectedFinish','vcProjectedFinish','ProjectedFinish','ProjectsMaster','V','R','Label','',0,1,0,0,0,1,0,1,1,1,1
)

SET @numFieldID = SCOPE_IDENTITY()

INSERT INTO DycFormField_Mapping
(
	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,bitInResults,bitDeleted,bitDefault,bitSettingField,bitAddField,bitDetailField
)
VALUES
(
	5,@numFieldID,13,0,0,'Projected Finish','Label',1,0,0,1,0,1
)
ALTER TABLE ProjectsMaster Add dtActualStartDate DATETIME DEFAULT NULL
ALTER TABLE ProjectsMaster Add dtmEndDate DATETIME DEFAULT NULL
ALTER TABLE ProjectsMaster Add dtmStartDate DATETIME DEFAULT NULL
ALTER TABLE Domain ADD vcLoginURL VARCHAR(500) DEFAULT NULL
ALTER TABLE Domain ADD vcLogoForLoginBizTheme VARCHAR(500) DEFAULT NULL
ALTER TABLE Domain ADD bitLogoAppliedToLoginBizTheme BIT DEFAULT 0
UPDATE DynamicFormMaster SET vcFormName ='Prospect (and all Organization relationships that aren�t Leads or Accounts)'  where vcFormName='Prospect'
UPDATE DynamicFormMaster SET vcFormName ='Prospect (and all Organization relationships that aren�t Leads or Accounts)'  where vcFormName='Prospect'
ALTER TABLE DynamicFormMaster ALTER COLUMN vcFormName VARCHAR(500);
UPDATE CFW_Loc_Master SET Loc_Name='Project Details' WHERE Loc_id=11
ALTER TABLE FormFieldGroupConfigurarion ADD numOrder NUMERIC DEFAULT 0
DELETE FROM TreeNavigationAuthorization WHERE numPageNavID=
(SELECT TOP 1 numPageNavID FROM PageNavigationDTL WHERE numModuleID = 13  AND vcPageNavName='Map Custom Fields')--69

DELETE FROM PageNavigationDTL WHERE numModuleID = 13  AND vcPageNavName='Map Custom Fields'

UPDATE BizFormWizardModule SET bitActive=0 WHERE numBizFormModuleID =8
ALTER TABLE BizFormWizardModule ADD bitActive BIT DEFAULT 1 
ALTER TABLE Domain ADD vcLogoForBizTheme VARCHAR(500) DEFAULT NULL
ALTER TABLE Domain ADD bitLogoAppliedToBizTheme BIT DEFAULT 0
ALTER TABLE Domain ADD vcThemeClass VARCHAR(500) DEFAULT NULL

ALTER TABLE Sales_process_List_Master ADD numWorkOrderId NUMERIC(18,0) DEFAULT 0
ALTER TABLE StagePercentageDetails ADD numWorkOrderId NUMERIC(18,0) DEFAULT 0
ALTER TABLE StagePercentageDetailsTask ADD numWorkOrderId NUMERIC(18,0) DEFAULT 0

INSERT  INTO dbo.NameTemplate
(
	numDomainID,vcModuleName,vcNameTemplate,tintModuleID,numSequenceId,numMinLength,numRecordID
)
SELECT 
	numDomainId,'Work Order','WO-',1,1,4,3
FROM   
	Domain	
 
ALTER TABLE WorkOrder ADD vcWorkOrderName  VARCHAR(500) DEFAULT 0
ALTER TABLE WorkOrder ADD numBuildProcessId NUMERIC(18,0) DEFAULT 0
ALTER TABLE WorkOrder ADD dtmStartDate DATETIME DEFAULT NULL
ALTER TABLE WorkOrder ADD dtmEndDate DATETIME DEFAULT NULL
ALTER TABLE WorkOrder ADD dtActualStartDate DATETIME DEFAULT NULL


CREATE TABLE [dbo].[tblStageGradeDetails](
	[numStageDetailsGradeId] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[numStageDetailsId] [numeric](18, 0) NOT NULL,
	[numAssigneId] [numeric](18, 0) NOT NULL,
	[vcGradeId] [varchar](100) NULL,
	[numDomainId] [numeric](18, 0) NOT NULL,
	[numCreatedBy] [numeric](18, 0) NULL,
	[bintCreatedDate] [datetime] NULL,
	[numModifiedBy] [numeric](18, 0) NULL,
	[bintModifiedDate] [datetime] NULL,
	[numTaskId] [numeric](18, 0) NULL,
 CONSTRAINT [PK_tblStageGradeDetails] PRIMARY KEY CLUSTERED 
(
	[numStageDetailsGradeId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO


ALTER TABLE StagePercentageDetails ADD intTaskType INT DEFAULT 0
ALTER TABLE Item ADD numBusinessProcessId NUMERIC(18,0) DEFAULT 0