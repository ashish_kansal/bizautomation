/******************************************************************
Project: Release 7.0 Date: 27.FEB.2017
Comments: STORE PROCEDURES
*******************************************************************/


GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='TF'AND NAME ='fn_DemandForecastRelaseDatesCount')
DROP FUNCTION fn_DemandForecastRelaseDatesCount
GO
CREATE FUNCTION fn_DemandForecastRelaseDatesCount
(
    @numDomainID AS NUMERIC(18,0),
    @dtDate AS DATE,
	@numDFID AS NUMERIC(18,0)
)
RETURNS @TEMPReleaseDates TABLE
(
    numItemCode NUMERIC(18,0),
	numWarehouseID NUMERIC(18,0),
	numWarehouseItemID NUMERIC(18,0),
	numQty INT,
	numQtySalesOppProgress INT
)
AS 
BEGIN
	DECLARE @bitWarehouseFilter BIT = 0
	DECLARE @bitItemClassificationFilter BIT = 0
	DECLARE @bitItemGroupFilter BIT = 0

	IF (SELECT COUNT(*) FROM DemandForecastWarehouse WHERE numDFID = @numDFID) > 0
	BEGIN
		SET @bitWarehouseFilter = 1
	END

	IF (SELECT COUNT(*) FROM DemandForecastItemClassification WHERE numDFID = @numDFID) > 0
	BEGIN
		SET @bitItemClassificationFilter = 1
	END

	IF (SELECT COUNT(*) FROM DemandForecastItemGroup WHERE numDFID = @numDFID) > 0
	BEGIN
		SET @bitItemGroupFilter = 1
	END

	


	DECLARE @TEMPORDER TABLE
	(
		numOppID NUMERIC(18,0),
		numOppItemID NUMERIC(18,0),
		numItemCode NUMERIC(18,0),
		numWarehouseID NUMERIC(18,0),
		numWarehouseItemID NUMERIC(18,0),
		numQty FLOAT,
		numQtySalesOppProgress FLOAT,
		bitKitParent BIT,
		bitAssembly BIT
	)

	-- Based on order/opp release date
	INSERT INTO 
		@TEMPORDER
	SELECT
		OpportunityMaster.numOppID
		,OpportunityItems.numoppitemtcode
		,OpportunityItems.numItemCode
		,WI.numWareHouseID
		,(CASE
			WHEN ISNULL(I.numItemGroup,0) > 0 AND LEN(dbo.fn_GetAttributes(WI.numWareHouseItemID,0)) > 0
			THEN
				(CASE 
					WHEN EXISTS (SELECT numWareHouseItemID FROM WareHouseItems WIInner WHERE WIInner.numDomainID=@numDomainID AND  WIInner.numItemID=I.numItemCode AND WIInner.numWareHouseID=WI.numWareHouseID AND WIInner.numWareHouseID=-1 AND  dbo.fn_getattributes(WIInner.numWareHouseItemID,0) = dbo.fn_getattributes(WI.numWareHouseItemID,0)) 
					THEN
						(SELECT TOP 1 numWareHouseItemID FROM WareHouseItems WIInner WHERE WIInner.numDomainID=@numDomainID AND  WIInner.numItemID=I.numItemCode AND WIInner.numWareHouseID=WI.numWareHouseID AND WIInner.numWareHouseID=-1 AND  dbo.fn_getattributes(WIInner.numWareHouseItemID,0) = dbo.fn_getattributes(WI.numWareHouseItemID,0))
					ELSE
						WI.numWareHouseItemID
				END)
			ELSE
				(SELECT TOP 1 numWareHouseItemID FROM WareHouseItems WHERE numDomainID=@numDomainID AND numItemID=I.numItemCode AND numWareHouseID=WI.numWareHouseID AND numWLocationID = -1)
		END) numWarehouseItemID
		,OpportunityItems.numUnitHour - ISNULL(TEMPItemRelease.numQty,0) AS numQty
		,0 AS numQtySalesOppProgress
		,I.bitKitParent
		,I.bitAssembly
	FROM
		OpportunityMaster
	INNER JOIN
		OpportunityItems
	ON
		OpportunityMaster.numOppId = OpportunityItems.numOppId
	INNER JOIN
		Item I
	ON
		OpportunityItems.numItemCode = I.numItemCode
	INNER JOIN
		WareHouseItems WI
	ON
		OpportunityItems.numWarehouseItmsID=WI.numWareHouseItemID
	OUTER APPLY
	(
		SELECT
			SUM(numQty) AS numQty
		FROM
			OpportunityItemsReleaseDates OIRD
		WHERE
			OIRD.numOppID = OpportunityMaster.numOppId
			AND OIRD.numOppItemID = OpportunityItems.numoppitemtCode
			AND ((OIRD.dtReleaseDate <= @dtDate AND ISNULL(OIRD.tintStatus,0) = 1) OR ISNULL(OIRD.tintStatus,0) = 2)
	) TEMPItemRelease
	WHERE
		OpportunityMaster.numDomainId = @numDomainID
		AND ISNULL(tintOppType,0) = 1
		AND OpportunityMaster.dtReleaseDate IS NOT NULL AND CAST(OpportunityMaster.dtReleaseDate AS DATE) <= @dtDate
		AND ISNULL(numReleaseStatus,1) = 1
		AND
		(
			(SELECT COUNT(*) FROM DemandForecastWarehouse WHERE numDFID = @numDFID) = 0 OR
			WI.numWareHouseID IN (SELECT numWareHouseID FROM DemandForecastWarehouse WHERE numDFID = @numDFID)
		)
		AND
		(
			(SELECT COUNT(*) FROM DemandForecastItemClassification WHERE numDFID = @numDFID) = 0 OR
			I.numItemClassification IN (SELECT numItemClassificationID FROM DemandForecastItemClassification WHERE numDFID = @numDFID)
		)
		AND
		(
			(SELECT COUNT(*) FROM DemandForecastItemGroup WHERE numDFID = @numDFID) = 0 OR
			I.numItemGroup IN (SELECT numItemGroupID FROM DemandForecastItemGroup WHERE numDFID = @numDFID)
		)

	-- Based on item release date
	INSERT INTO 
		@TEMPORDER
	SELECT
		OI.numOppID
		,OI.numoppitemtcode
		,OI.numItemCode
		,WI.numWareHouseID
		,(CASE
			WHEN ISNULL(I.numItemGroup,0) > 0 AND LEN(dbo.fn_GetAttributes(WI.numWareHouseItemID,0)) > 0
			THEN
				(CASE 
					WHEN EXISTS (SELECT numWareHouseItemID FROM WareHouseItems WIInner WHERE WIInner.numDomainID=@numDomainID AND  WIInner.numItemID=I.numItemCode AND WIInner.numWareHouseID=WI.numWareHouseID AND WIInner.numWareHouseID=-1 AND  dbo.fn_getattributes(WIInner.numWareHouseItemID,0) = dbo.fn_getattributes(WI.numWareHouseItemID,0)) 
					THEN
						(SELECT TOP 1 numWareHouseItemID FROM WareHouseItems WIInner WHERE WIInner.numDomainID=@numDomainID AND  WIInner.numItemID=I.numItemCode AND WIInner.numWareHouseID=WI.numWareHouseID AND WIInner.numWareHouseID=-1 AND  dbo.fn_getattributes(WIInner.numWareHouseItemID,0) = dbo.fn_getattributes(WI.numWareHouseItemID,0))
					ELSE
						WI.numWareHouseItemID
				END)
			ELSE
				(SELECT TOP 1 numWareHouseItemID FROM WareHouseItems WHERE numDomainID=@numDomainID AND numItemID=I.numItemCode AND numWareHouseID=WI.numWareHouseID AND numWLocationID = -1)
		END) numWarehouseItemID
		,OI.numUnitHour
		,0
		,I.bitKitParent
		,I.bitAssembly
	FROM
		OpportunityItemsReleaseDates OIRD
	INNER JOIN
		OpportunityItems OI
	ON
		OIRD.numOppID=OI.numOppId
		AND OIRD.numOppItemID = OI.numoppitemtCode
	INNER JOIN
		Item I
	ON
		OI.numItemCode = I.numItemCode
	INNER JOIN
		WareHouseItems WI
	ON
		OI.numWarehouseItmsID=WI.numWareHouseItemID
	WHERE
		ISNULL(tintStatus,0)=1
		AND dtReleaseDate <= @dtDate
		AND
		(
			(SELECT COUNT(*) FROM DemandForecastWarehouse WHERE numDFID = @numDFID) = 0 OR
			WI.numWareHouseID IN (SELECT numWareHouseID FROM DemandForecastWarehouse WHERE numDFID = @numDFID)
		)
		AND
		(
			(SELECT COUNT(*) FROM DemandForecastItemClassification WHERE numDFID = @numDFID) = 0 OR
			I.numItemClassification IN (SELECT numItemClassificationID FROM DemandForecastItemClassification WHERE numDFID = @numDFID)
		)
		AND
		(
			(SELECT COUNT(*) FROM DemandForecastItemGroup WHERE numDFID = @numDFID) = 0 OR
			I.numItemGroup IN (SELECT numItemGroupID FROM DemandForecastItemGroup WHERE numDFID = @numDFID)
		)


	-- Based on sales opp total progress
	INSERT INTO 
		@TEMPORDER
	SELECT
		OI.numOppID
		,OI.numoppitemtcode
		,OI.numItemCode
		,WI.numWareHouseID
		,(CASE
			WHEN ISNULL(I.numItemGroup,0) > 0 AND LEN(dbo.fn_GetAttributes(WI.numWareHouseItemID,0)) > 0
			THEN
				(CASE 
					WHEN EXISTS (SELECT numWareHouseItemID FROM WareHouseItems WIInner WHERE WIInner.numDomainID=@numDomainID AND  WIInner.numItemID=I.numItemCode AND WIInner.numWareHouseID=WI.numWareHouseID AND WIInner.numWareHouseID=-1 AND  dbo.fn_getattributes(WIInner.numWareHouseItemID,0) = dbo.fn_getattributes(WI.numWareHouseItemID,0)) 
					THEN
						(SELECT TOP 1 numWareHouseItemID FROM WareHouseItems WIInner WHERE WIInner.numDomainID=@numDomainID AND  WIInner.numItemID=I.numItemCode AND WIInner.numWareHouseID=WI.numWareHouseID AND WIInner.numWareHouseID=-1 AND  dbo.fn_getattributes(WIInner.numWareHouseItemID,0) = dbo.fn_getattributes(WI.numWareHouseItemID,0))
					ELSE
						WI.numWareHouseItemID
				END)
			ELSE
				(SELECT TOP 1 numWareHouseItemID FROM WareHouseItems WHERE numDomainID=@numDomainID AND numItemID=I.numItemCode AND numWareHouseID=WI.numWareHouseID AND numWLocationID = -1)
		END) numWarehouseItemID
		,0
		,CEILING(CAST(OI.numUnitHour - ISNULL(TEMPItemRelease.numQty,0) AS FLOAT) * (PP.intTotalProgress/100)) AS numQty
		,I.bitKitParent
		,I.bitAssembly
	FROM
		OpportunityMaster OM
	INNER JOIN
		OpportunityItems OI
	ON
		OM.numOppID=OI.numOppId
	INNER JOIN
		Item I
	ON
		OI.numItemCode = I.numItemCode
	INNER JOIN
		WareHouseItems WI
	ON
		OI.numWarehouseItmsID = WI.numWareHouseItemID
	OUTER APPLY
	(
		SELECT
			SUM(numQty) AS numQty
		FROM
			OpportunityItemsReleaseDates OIRD
		WHERE
			OIRD.numOppID = OM.numOppId
			AND OIRD.numOppItemID = OI.numoppitemtCode
			AND ISNULL(OIRD.tintStatus,0) = 2
	) TEMPItemRelease
	LEFT JOIN
		ProjectProgress PP
	ON
		OM.numOppId = PP.numOppId
	WHERE
		tintOppType = 1
		AND ISNULL(tintOppStatus,0)=0
		AND intPEstimatedCloseDate IS NOT NULL AND CAST(OM.intPEstimatedCloseDate AS DATE) <= @dtDate
		AND ISNULL(OM.numReleaseStatus,1) = 1
		AND ISNULL(PP.intTotalProgress,0) > 0
		AND
		(
			@bitWarehouseFilter = 0 OR
			WI.numWareHouseID IN (SELECT numWareHouseID FROM DemandForecastWarehouse WHERE numDFID = @numDFID)
		)
		AND
		(
			@bitItemClassificationFilter = 0 OR
			I.numItemClassification IN (SELECT numItemClassificationID FROM DemandForecastItemClassification WHERE numDFID = @numDFID)
		)
		AND
		(
			@bitItemGroupFilter = 0 OR
			I.numItemGroup IN (SELECT numItemGroupID FROM DemandForecastItemGroup WHERE numDFID = @numDFID)
		)
	

	-- GET KIT ITEMS CHILD ITEMS
	;WITH CTE(numOppChildItemID,numOppID,numOppItemID,numItemCode,bitKitParent,numItemGroup,numWarehouseID,numWarehouseItemID,numQty,numQtySalesOppProgress) AS
	(
		SELECT 
			numOppChildItemID
			,t1.numOppId
			,t1.numOppItemID
			,numChildItemID
			,I.bitKitParent
			,I.numItemGroup
			,WI.numWarehouseID
			,WI.numWarehouseItemID
			,t1.numQty * numQtyItemsReq_Orig
			,t1.numQtySalesOppProgress * numQtyItemsReq_Orig
		FROM 
			@TEMPORDER AS t1
		INNER JOIN
			OpportunityKitItems OKI
		ON
			t1.numOppID = OKI.numOppId
			AND t1.numOppItemID = OKI.numOppItemID
		INNER JOIN
			Item I
		ON
			OKI.numChildItemID = I.numItemCode
		INNER JOIN
			WarehouseItems WI
		ON
			OKI.numWareHouseItemId = WI.numWarehouseItemID
		UNION ALL
		SELECT
			OKCI.numOppKitChildItemID
			,OKCI.numOppId
			,OKCI.numOppItemID
			,OKCI.numItemID
			,I.bitKitParent
			,I.numItemGroup
			,WI.numWarehouseID
			,WI.numWarehouseItemID
			,c.numQty * numQtyItemsReq_Orig
			,c.numQtySalesOppProgress * OKCI.numQtyItemsReq_Orig
		FROM
			OpportunityKitChildItems OKCI
		INNER JOIN	
			CTE c
		ON
			OKCI.numOppChildItemID = c.numOppChildItemID
		INNER JOIN
			Item I
		ON
			OKCI.numItemID = I.numItemCode
		INNER JOIN
			WarehouseItems WI
		ON
			OKCI.numWareHouseItemId = WI.numWarehouseItemID
	)

	INSERT INTO
		@TEMPORDER
	SELECT
		numOppID,
		numOppItemID,
		numItemCode,
		c.numWareHouseID,
		(CASE
			WHEN ISNULL(c.numItemGroup,0) > 0 AND LEN(dbo.fn_GetAttributes(numWareHouseItemID,0)) > 0
			THEN
				(CASE 
					WHEN EXISTS (SELECT numWareHouseItemID FROM WareHouseItems WIInner WHERE WIInner.numDomainID=@numDomainID AND  WIInner.numItemID=c.numItemCode AND WIInner.numWareHouseID=c.numWareHouseID AND WIInner.numWareHouseID=-1 AND  dbo.fn_getattributes(WIInner.numWareHouseItemID,0) = dbo.fn_getattributes(c.numWareHouseItemID,0)) 
					THEN
						(SELECT TOP 1 numWareHouseItemID FROM WareHouseItems WIInner WHERE WIInner.numDomainID=@numDomainID AND  WIInner.numItemID=c.numItemCode AND WIInner.numWareHouseID=c.numWareHouseID AND WIInner.numWareHouseID=-1 AND  dbo.fn_getattributes(WIInner.numWareHouseItemID,0) = dbo.fn_getattributes(c.numWareHouseItemID,0))
					ELSE
						c.numWareHouseItemID
				END)
			ELSE
				(SELECT TOP 1 numWareHouseItemID FROM WareHouseItems WHERE numDomainID=@numDomainID AND numItemID=c.numItemCode AND numWareHouseID=c.numWareHouseID AND numWLocationID = -1)
		END),
		numQty,
		numQtySalesOppProgress,
		bitKitParent,
		0
	FROM
		CTE c
	WHERE
		ISNULL(bitKitParent,0) = 0

	-- GET ASSEMBLY CHILD ITEMS
	;WITH CTEAssembly(numItemCode, bitAssembly, numItemGroup, numWareHouseID,numWarehouseItemID, numQty, numQtySalesOppProgress) AS 
	( 
		SELECT   
			numChildItemID,
			I.bitAssembly,
			I.numItemGroup,
			ISNULL(WI.numWareHouseID, 0),
			ISNULL(WI.numWareHouseItemID, 0),
			CAST(DTL.numQtyItemsReq * T1.numQty AS FLOAT),
			CAST(DTL.numQtyItemsReq * T1.numQtySalesOppProgress AS FLOAT)
		FROM
			@TEMPORDER T1
		INNER JOIN 
			ItemDetails Dtl 
		ON 
			numItemKitID = T1.numItemCode
		INNER JOIN
			Item I
		ON
			Dtl.numChildItemID = I.numItemCode
		INNER JOIN
			WareHouseItems WI
		ON
			Dtl.numWareHouseItemId = WI.numWareHouseItemID
		WHERE 
			ISNULL(T1.bitAssembly,0) = 1
		UNION ALL
		SELECT 
			I.numItemCode,
			I.bitAssembly,
			I.numItemGroup,
			ISNULL(WI.numWareHouseID, 0),
			ISNULL(WI.numWareHouseItemID, 0),
			CAST(DTL.numQtyItemsReq * c.numQty AS FLOAT),
			CAST(DTL.numQtyItemsReq * c.numQtySalesOppProgress AS FLOAT)
		FROM
			ItemDetails Dtl
		INNER JOIN 
			CTEAssembly c 
		ON 
			Dtl.numItemKitID = c.numItemCode
		INNER JOIN
			Item I
		ON
			Dtl.numChildItemID = I.numItemCode
		INNER JOIN
			WareHouseItems WI
		ON
			Dtl.numWareHouseItemId = WI.numWareHouseItemID
		WHERE    
			Dtl.numChildItemID != c.numItemCode			
	)

	INSERT INTO
		@TEMPORDER
	SELECT
		0,
		0,
		numItemCode,
		c.numWareHouseID,
		(CASE
			WHEN ISNULL(c.numItemGroup,0) > 0 AND LEN(dbo.fn_GetAttributes(numWareHouseItemID,0)) > 0
			THEN
				(CASE 
					WHEN EXISTS (SELECT numWareHouseItemID FROM WareHouseItems WIInner WHERE WIInner.numDomainID=@numDomainID AND  WIInner.numItemID=c.numItemCode AND WIInner.numWareHouseID=c.numWareHouseID AND WIInner.numWareHouseID=-1 AND  dbo.fn_getattributes(WIInner.numWareHouseItemID,0) = dbo.fn_getattributes(c.numWareHouseItemID,0)) 
					THEN
						(SELECT TOP 1 numWareHouseItemID FROM WareHouseItems WIInner WHERE WIInner.numDomainID=@numDomainID AND  WIInner.numItemID=c.numItemCode AND WIInner.numWareHouseID=c.numWareHouseID AND WIInner.numWareHouseID=-1 AND  dbo.fn_getattributes(WIInner.numWareHouseItemID,0) = dbo.fn_getattributes(c.numWareHouseItemID,0))
					ELSE
						c.numWareHouseItemID
				END)
			ELSE
				(SELECT TOP 1 numWareHouseItemID FROM WareHouseItems WHERE numDomainID=@numDomainID AND numItemID=c.numItemCode AND numWareHouseID=c.numWareHouseID AND numWLocationID = -1)
		END),
		numQty,
		numQtySalesOppProgress,
		0,
		0
	FROM
		CTEAssembly c
	WHERE
		ISNULL(bitAssembly,0) = 0


	INSERT INTO 
		@TEMPReleaseDates
	SELECT
		T1.numItemCode
		,T1.numWarehouseID
		,T1.numWarehouseItemID
		,SUM(numQty)
		,SUM(numQtySalesOppProgress)
	FROM
		@TEMPORDER T1
	WHERE
		ISNULL(T1.bitKitParent,0) = 0 AND ISNULL(T1.bitAssembly,0) = 0
		AND ISNULL(T1.numWarehouseItemID,0) > 0
	GROUP BY
		T1.numItemCode
		,T1.numWarehouseID
		,T1.numWarehouseItemID

	RETURN
END
/****** Object:  UserDefinedFunction [dbo].[fn_GetAttributes]    Script Date: 07/26/2008 18:12:28 ******/

GO

GO
--- Created By Anoop Jayaraj
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='fn_getattributes')
DROP FUNCTION fn_getattributes
GO
CREATE FUNCTION [dbo].[fn_GetAttributes] (@numRecID numeric,@bitSerialized  bit)
returns varchar(100)
as
begin
declare @strAttribute varchar(100)
declare @numCusFldID numeric(9)
declare @numCusFldValue varchar(100)
set @numCusFldID=0
set @strAttribute=''
select top 1 @numCusFldID=fld_id,@numCusFldValue=fld_value from CFW_Fld_Values_Serialized_Items where RecId=@numRecID and fld_value!='0' and fld_value!='' order by fld_id
while @numCusFldID>0
begin
	if isnumeric(@numCusFldValue)=1
	begin
		--set @strAttribute=@strAttribute+@numCusFldValue
		select @strAttribute=@strAttribute+Fld_label from  CFW_Fld_Master where Fld_id=@numCusFldID AND Grp_id = 9
                
        IF LEN(@strAttribute) > 0
        BEGIN
			select @strAttribute=@strAttribute+':'+ vcData +',' from ListDetails where numListItemID=@numCusFldValue
        END
		
	end
	else
	begin
		select @strAttribute=@strAttribute+Fld_label from  CFW_Fld_Master where Fld_id=@numCusFldID AND Grp_id = 9
		IF LEN(@strAttribute) > 0
        BEGIN
			select @strAttribute=@strAttribute+':-,' 
        END
	end
	select top 1 @numCusFldID=fld_id,@numCusFldValue=fld_value from CFW_Fld_Values_Serialized_Items where RecId=@numRecID and fld_value!='0' and fld_value!='' and fld_id>@numCusFldID order by fld_id
	if @@rowcount=0 set @numCusFldID=0
	
end

If LEN(@strAttribute) > 0
BEGIN
	SET @strAttribute = LEFT(@strAttribute, LEN(@strAttribute) - 1)
END

return @strAttribute
end
GO

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='fn_GetAttributesIds')
DROP FUNCTION fn_GetAttributesIds
GO
CREATE FUNCTION [dbo].[fn_GetAttributesIds] (@numRecID numeric)
returns varchar(100)
as
begin
declare @strAttribute varchar(100)
declare @numCusFldID numeric(9)
declare @numCusFldValue varchar(100)
set @numCusFldID=0
set @strAttribute=''
select top 1 @numCusFldID=fld_id,@numCusFldValue=fld_value from CFW_Fld_Values_Serialized_Items where RecId=@numRecID and fld_value!='0' and fld_value!='' order by fld_id
while @numCusFldID>0
begin
	IF ISNUMERIC(@numCusFldValue) = 1
	BEGIN
		--set @strAttribute=@strAttribute+@numCusFldValue
		select @strAttribute=CONCAT(@strAttribute,Fld_id) from  CFW_Fld_Master where Fld_id=@numCusFldID AND Grp_id = 9
                
        IF LEN(@strAttribute) > 0
        BEGIN
			select @strAttribute=CONCAT(@strAttribute,':',numListItemID,',') from ListDetails where numListItemID=@numCusFldValue
        END
	END
	
	SELECT TOP 1 @numCusFldID=fld_id,@numCusFldValue=fld_value from CFW_Fld_Values_Serialized_Items where RecId=@numRecID and fld_value!='0' and fld_value!='' and fld_id>@numCusFldID order by fld_id
	if @@rowcount=0 set @numCusFldID=0
	
end

If LEN(@strAttribute) > 0
BEGIN
	SET @strAttribute = LEFT(@strAttribute, LEN(@strAttribute) - 1)
END

return @strAttribute
end
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='fn_GetItemAttributes')
DROP FUNCTION fn_GetItemAttributes
GO
CREATE FUNCTION [dbo].[fn_GetItemAttributes] 
(
	@numDomainID NUMERIC(18,0),
	@numItemCode NUMERIC(18,0)
)
RETURNS VARCHAR(500)
AS
BEGIN
	DECLARE @strAttribute varchar(500) = ''
	DECLARE @numCusFldID NUMERIC(18,0) = 0
	DECLARE @numCusFldValue NUMERIC(18,0) = 0

	SELECT TOP 1
		@numCusFldID=fld_id
		,@numCusFldValue=fld_value 
	FROM 
		ItemAttributes 
	WHERE 
		numDomainID = @numDomainID
		AND numItemCode=@numItemCode 
		AND fld_value != 0
	ORDER BY 
		fld_id

	while @numCusFldID>0
	begin
		if isnumeric(@numCusFldValue)=1
		begin
			select @strAttribute=@strAttribute+Fld_label from  CFW_Fld_Master where Fld_id=@numCusFldID AND Grp_id = 9
                
			IF LEN(@strAttribute) > 0
			BEGIN
				select @strAttribute=@strAttribute+':'+ vcData +',' from ListDetails where numListItemID=@numCusFldValue
			END
		
		end
		else
		begin
			select @strAttribute=@strAttribute+Fld_label from  CFW_Fld_Master where Fld_id=@numCusFldID AND Grp_id = 9
			IF LEN(@strAttribute) > 0
			BEGIN
				select @strAttribute=@strAttribute+':-,' 
			END
		end


		SELECT TOP 1
			@numCusFldID=fld_id
			,@numCusFldValue=fld_value 
		FROM 
			ItemAttributes 
		WHERE 
			numDomainID = @numDomainID
			AND numItemCode=@numItemCode 
			AND fld_value != 0
			AND fld_id > @numCusFldID
		ORDER BY 
			fld_id

		IF @@rowcount=0 SET @numCusFldID=0
	END

	If LEN(@strAttribute) > 0
	BEGIN
		SET @strAttribute = LEFT(@strAttribute, LEN(@strAttribute) - 1)
	END

	RETURN @strAttribute
END
GO
/****** Object:  UserDefinedFunction [dbo].[GetCustFldItems]    Script Date: 07/26/2008 18:12:58 ******/

GO

GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='getcustflditems')
DROP FUNCTION getcustflditems
GO
CREATE FUNCTION [dbo].[GetCustFldItems]
(
	@numFldId NUMERIC(18,0)
	,@pageId AS TINYINT
	,@numRecordId AS NUMERIC(18,0)
	,@tintMode AS TINYINT  -- 0: Warehouse Level Attributes, 1: Item Level Attributes 
)  
RETURNS VARCHAR(300)   
AS  
BEGIN  
	DECLARE @vcValue AS VARCHAR(300)

	IF @pageId=9 AND ISNULL(@tintMode,0) = 0
	BEGIN
		SELECT 
			@vcValue=Fld_Value 
		FROM 
			CFW_Fld_Values_Serialized_Items 
		WHERE 
			Fld_ID=@numFldId 
			AND RecId=@numRecordId
	END 
	ELSE IF ISNULL(@tintMode,0) = 1
	BEGIN
		SELECT 
			@vcValue=Fld_Value 
		FROM 
			ItemAttributes 
		WHERE 
			Fld_ID=@numFldId 
			AND numItemCode=@numRecordId
	ENd

	IF @vcValue IS NULL 
		SET @vcValue='0'  
	
	RETURN @vcValue  
END
GO
/****** Object:  UserDefinedFunction [dbo].[GetCustFldItemsValue]    Script Date: 07/26/2008 18:12:58 ******/

GO

GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='GetCustFldItemsValue')
DROP FUNCTION GetCustFldItemsValue
GO
CREATE FUNCTION [dbo].[GetCustFldItemsValue]
(
	@numFldId numeric(9)
	,@pageId as tinyint
	,@numRecordId as numeric(9)
	,@tintMode as TINYINT -- 0: Warehouse Level Attributes, 1: Item Level Attributes 
	,@fld_type as varchar(100)
)  
RETURNS VARCHAR(300)   
AS  
BEGIN  
	DECLARE @vcValue AS VARCHAR(100)  

	IF @pageId=9 AND ISNULL(@tintMode,0) = 0
	BEGIN
		SELECT 
			@vcValue= CASE 
						WHEN @fld_type='SelectBox' THEN dbo.GetListIemName(Fld_Value)
						WHEN @fld_type='CheckBox' THEN CASE WHEN isnull(Fld_Value,0)=1 THEN 'Yes' ELSE 'No' END
						ELSE Fld_Value 
					 END 
		FROM 
			CFW_Fld_Values_Serialized_Items 
		WHERE 
			Fld_ID=@numFldId 
			AND RecId=@numRecordId  
	END 
	ELSE IF ISNULL(@tintMode,0) = 1
	BEGIN
		SELECT 
			@vcValue= CASE 
						WHEN @fld_type='SelectBox' THEN dbo.GetListIemName(Fld_Value)
						WHEN @fld_type='CheckBox' THEN CASE WHEN isnull(Fld_Value,0)=1 THEN 'Yes' ELSE 'No' END
						ELSE CAST(Fld_Value AS VARCHAR)
					 END 
		FROM 
			ItemAttributes 
		WHERE 
			Fld_ID=@numFldId 
			AND numItemCode=@numRecordId
	END

	IF @vcValue IS NULL 
		SET @vcValue='0'  

	RETURN @vcValue  
END
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='TF'AND NAME ='GetDFHistoricalSalesOrderData')
DROP FUNCTION GetDFHistoricalSalesOrderData
GO
CREATE FUNCTION [dbo].[GetDFHistoricalSalesOrderData] 
 (
	  @numDFID AS NUMERIC(18,0),
      @numDomainID AS NUMERIC(9),
	  @dtFromDate AS DATETIME,
	  @dtToDate AS DATETIME
)
RETURNS  @TEMP TABLE 
(
	numItemCode NUMERIC(18,0), 
	numWarehouseID NUMERIC(18,0),
	numWarehouseItemID NUMERIC(18,0),
	numQtySold INTEGER
)
AS BEGIN 
		DECLARE @bitWarehouseFilter BIT = 0
		DECLARE @bitItemClassificationFilter BIT = 0
		DECLARE @bitItemGroupFilter BIT = 0

		IF (SELECT COUNT(*) FROM DemandForecastWarehouse WHERE numDFID = @numDFID) > 0
		BEGIN
			SET @bitWarehouseFilter = 1
		END

		IF (SELECT COUNT(*) FROM DemandForecastItemClassification WHERE numDFID = @numDFID) > 0
		BEGIN
			SET @bitItemClassificationFilter = 1
		END

		IF (SELECT COUNT(*) FROM DemandForecastItemGroup WHERE numDFID = @numDFID) > 0
		BEGIN
			SET @bitItemGroupFilter = 1
		END


		DECLARE @TEMPORDER TABLE
		(
			numOppID NUMERIC(18,0),
			numOppItemID NUMERIC(18,0),
			numItemCode NUMERIC(18,0),
			numWarehouseID NUMERIC(18,0),
			numWarehouseItemID NUMERIC(18,0),
			numUnitHour FLOAT,
			bitKitParent BIT,
			bitAssembly BIT
		)

		INSERT INTO	
			@TEMPORDER
		SELECT
			OpportunityMaster.numOppId,
			OpportunityItems.numoppitemtCode,	
			Item.numItemCode,
			WareHouseItems.numWareHouseID,
			(CASE
				WHEN ISNULL(Item.numItemGroup,0) > 0 AND LEN(dbo.fn_GetAttributes(WareHouseItems.numWareHouseItemID,0)) > 0
				THEN
					(CASE 
						WHEN EXISTS (SELECT numWareHouseItemID FROM WareHouseItems WIInner WHERE WIInner.numDomainID=@numDomainID AND  WIInner.numItemID=Item.numItemCode AND WIInner.numWareHouseID=WareHouseItems.numWareHouseID AND WIInner.numWareHouseID=-1 AND  dbo.fn_getattributes(WIInner.numWareHouseItemID,0) = dbo.fn_getattributes(WareHouseItems.numWareHouseItemID,0)) 
						THEN
							(SELECT TOP 1 numWareHouseItemID FROM WareHouseItems WIInner WHERE WIInner.numDomainID=@numDomainID AND  WIInner.numItemID=Item.numItemCode AND WIInner.numWareHouseID=WareHouseItems.numWareHouseID AND WIInner.numWareHouseID=-1 AND  dbo.fn_getattributes(WIInner.numWareHouseItemID,0) = dbo.fn_getattributes(WareHouseItems.numWareHouseItemID,0))
						ELSE
							WareHouseItems.numWareHouseItemID
					END)
				ELSE
					(SELECT TOP 1 numWareHouseItemID FROM WareHouseItems WHERE numDomainID=@numDomainID AND numItemID=Item.numItemCode AND numWareHouseID=WareHouseItems.numWareHouseID AND numWLocationID = -1)
			END),
			OpportunityItems.numUnitHour,
			Item.bitKitParent,
			Item.bitAssembly
		FROM
			OpportunityMaster
		LEFT JOIN
			OpportunityItems
		ON
			OpportunityMaster.numOppId = OpportunityItems.numOppId
		INNER JOIN
			Item
		ON
			OpportunityItems.numItemCode = Item.numItemCode
		INNER JOIN
			WareHouseItems
		ON
			OpportunityItems.numWarehouseItmsID = WareHouseItems.numWareHouseItemID
		WHERE
			OpportunityMaster.numDomainId = @numDomainID AND
			CONVERT(DATE,OpportunityMaster.bintCreatedDate) BETWEEN CONVERT(DATE,@dtFromDate) AND CONVERT(DATE,@dtToDate) AND
			OpportunityMaster.tintOppType = 1 AND 
			OpportunityMaster.tintOppStatus = 1 
			AND
			(
				(SELECT COUNT(*) FROM DemandForecastWarehouse WHERE numDFID = @numDFID) = 0 OR
				WareHouseItems.numWareHouseID IN (SELECT numWareHouseID FROM DemandForecastWarehouse WHERE numDFID = @numDFID)
			)
			AND
			(
				(SELECT COUNT(*) FROM DemandForecastItemClassification WHERE numDFID = @numDFID) = 0 OR
				Item.numItemClassification IN (SELECT numItemClassification FROM DemandForecastItemClassification WHERE numDFID = @numDFID)
			)
			AND
			(
				(SELECT COUNT(*) FROM DemandForecastItemGroup WHERE numDFID = @numDFID) = 0 OR
				Item.numItemGroup IN (SELECT numItemGroup FROM DemandForecastItemGroup WHERE numDFID = @numDFID)
			)

		
		-- GET KIT ITEMS CHILD ITEMS
		;WITH CTE(numOppChildItemID,numOppID,numOppItemID,numItemCode,numItemGroup,bitKitParent,numWarehouseID,numWarehouseItemID,numQty) AS
		(
			SELECT 
				numOppChildItemID
				,OKI.numOppId
				,OKI.numOppItemID
				,numChildItemID
				,I.numItemGroup
				,I.bitKitParent
				,WI.numWarehouseID
				,WI.numWareHouseItemID
				,t1.numUnitHour * numQtyItemsReq_Orig
			FROM 
				@TEMPORDER AS t1
			INNER JOIN
				OpportunityKitItems OKI
			ON
				t1.numOppID = OKI.numOppId
				AND t1.numOppItemID = OKI.numOppItemID
			INNER JOIN
				Item I
			ON
				OKI.numChildItemID = I.numItemCode
			INNER JOIN
				WarehouseItems WI
			ON
				OKI.numWareHouseItemId = WI.numWarehouseItemID
				AND ISNULL(t1.bitKitParent,0) = 1 
				AND ISNULL(t1.bitAssembly,0) = 0 
			UNION ALL
			SELECT
				OKCI.numOppKitChildItemID
				,OKCI.numOppId
				,OKCI.numOppItemID
				,OKCI.numItemID
				,I.numItemGroup
				,I.bitKitParent
				,WI.numWarehouseID
				,WI.numWareHouseItemID
				,c.numQty * numQtyItemsReq_Orig
			FROM
				OpportunityKitChildItems OKCI
			INNER JOIN	
				CTE c
			ON
				OKCI.numOppChildItemID = c.numOppChildItemID
			INNER JOIN
				Item I
			ON
				OKCI.numItemID = I.numItemCode
			INNER JOIN
				WarehouseItems WI
			ON
				OKCI.numWareHouseItemId = WI.numWarehouseItemID
		)

		INSERT INTO
			@TEMPORDER
		SELECT
			numOppID,
			numOppItemID,
			numItemCode,
			numWarehouseID,
			(CASE
				WHEN ISNULL(c.numItemGroup,0) > 0 AND LEN(dbo.fn_GetAttributes(numWareHouseItemID,0)) > 0
				THEN
					(CASE 
						WHEN EXISTS (SELECT numWareHouseItemID FROM WareHouseItems WIInner WHERE WIInner.numDomainID=@numDomainID AND  WIInner.numItemID=c.numItemCode AND WIInner.numWareHouseID=c.numWareHouseID AND WIInner.numWareHouseID=-1 AND  dbo.fn_getattributes(WIInner.numWareHouseItemID,0) = dbo.fn_getattributes(c.numWareHouseItemID,0)) 
						THEN
							(SELECT TOP 1 numWareHouseItemID FROM WareHouseItems WIInner WHERE WIInner.numDomainID=@numDomainID AND  WIInner.numItemID=c.numItemCode AND WIInner.numWareHouseID=c.numWareHouseID AND WIInner.numWareHouseID=-1 AND  dbo.fn_getattributes(WIInner.numWareHouseItemID,0) = dbo.fn_getattributes(c.numWareHouseItemID,0))
						ELSE
							c.numWareHouseItemID
					END)
				ELSE
					(SELECT TOP 1 numWareHouseItemID FROM WareHouseItems WHERE numDomainID=@numDomainID AND numItemID=c.numItemCode AND numWareHouseID=c.numWareHouseID AND numWLocationID = -1)
			END),
			numQty,
			bitKitParent,
			0
		FROM
			CTE c
		WHERE
			ISNULL(bitKitParent,0) = 0

		-- GET ASSEMBLY CHILD ITEMS
		;WITH CTEAssembly(numItemCode, numItemGroup, bitAssembly, numWareHouseID, numWarehouseItemID, numQty) AS 
		( 
			SELECT   
				numChildItemID,
				I.numItemGroup,
				I.bitAssembly,
				ISNULL(WI.numWareHouseID, 0),
				ISNULL(WI.numWareHouseItemID, 0),
				CAST(DTL.numQtyItemsReq * T1.numUnitHour AS FLOAT)
			FROM
				@TEMPORDER T1
			INNER JOIN 
				ItemDetails Dtl 
			ON 
				numItemKitID = T1.numItemCode
			INNER JOIN
				Item I
			ON
				Dtl.numChildItemID = I.numItemCode
			INNER JOIN
				WareHouseItems WI
			ON
				Dtl.numWareHouseItemId = WI.numWareHouseItemID
			WHERE 
				ISNULL(T1.bitAssembly,0) = 1
			UNION ALL
			SELECT 
				I.numItemCode,
				I.numItemGroup,
				I.bitAssembly,
				ISNULL(WI.numWareHouseID, 0),
				ISNULL(WI.numWareHouseItemID, 0),
				CAST(DTL.numQtyItemsReq * c.numQty AS FLOAT)
			FROM
				ItemDetails Dtl
			INNER JOIN 
				CTEAssembly c 
			ON 
				Dtl.numItemKitID = c.numItemCode
			INNER JOIN
				Item I
			ON
				Dtl.numChildItemID = I.numItemCode
			INNER JOIN
				WareHouseItems WI
			ON
				Dtl.numWareHouseItemId = WI.numWareHouseItemID
			WHERE    
				Dtl.numChildItemID != c.numItemCode
		)

		INSERT INTO
			@TEMPORDER
		SELECT
			0,
			0,
			numItemCode,
			numWarehouseID,
			(CASE
				WHEN ISNULL(c.numItemGroup,0) > 0 AND LEN(dbo.fn_GetAttributes(numWareHouseItemID,0)) > 0
				THEN
					(CASE 
						WHEN EXISTS (SELECT numWareHouseItemID FROM WareHouseItems WIInner WHERE WIInner.numDomainID=@numDomainID AND  WIInner.numItemID=c.numItemCode AND WIInner.numWareHouseID=c.numWareHouseID AND WIInner.numWareHouseID=-1 AND  dbo.fn_getattributes(WIInner.numWareHouseItemID,0) = dbo.fn_getattributes(c.numWareHouseItemID,0)) 
						THEN
							(SELECT TOP 1 numWareHouseItemID FROM WareHouseItems WIInner WHERE WIInner.numDomainID=@numDomainID AND  WIInner.numItemID=c.numItemCode AND WIInner.numWareHouseID=c.numWareHouseID AND WIInner.numWareHouseID=-1 AND  dbo.fn_getattributes(WIInner.numWareHouseItemID,0) = dbo.fn_getattributes(c.numWareHouseItemID,0))
						ELSE
							c.numWareHouseItemID
					END)
				ELSE
					(SELECT TOP 1 numWareHouseItemID FROM WareHouseItems WHERE numDomainID=@numDomainID AND numItemID=c.numItemCode AND numWareHouseID=c.numWareHouseID AND numWLocationID = -1)
			END),
			numQty,
			0,
			0
		FROM
			CTEAssembly c
		WHERE
			ISNULL(bitAssembly,0) = 0


		-- Get total quantity of inventoty or assembly item in Sales Order(tintOppType = 1 AND tintOppStatus = 1)
		INSERT INTO
			@TEMP
		SELECT
			t1.numItemCode, 
			t1.numWarehouseID,
			t1.numWarehouseItemID,
			SUM(t1.numUnitHour) AS numQtySold
		FROM
			@TEMPORDER AS t1
		WHERE
			ISNULL(t1.bitKitParent,0) = 0 AND ISNULL(t1.bitAssembly,0) = 0
			AND ISNULL(t1.numWarehouseItemID,0) > 0
		GROUP BY
			t1.numItemCode,
			t1.numWarehouseID,
			t1.numWarehouseItemID

		RETURN
END

GO
/****** Object:  StoredProcedure [dbo].[USP_AddUpdateWareHouseForItems]    Script Date: 07/26/2008 16:14:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj  
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_AddUpdateWareHouseForItems')
DROP PROCEDURE USP_AddUpdateWareHouseForItems
GO
CREATE PROCEDURE [dbo].[USP_AddUpdateWareHouseForItems]  
@numItemCode as numeric(9)=0,  
@numWareHouseID as numeric(9)=0,
@numWareHouseItemID as numeric(9)=0 OUTPUT,
@vcLocation as varchar(250)='',
@monWListPrice as money =0,
@numOnHand as FLOAT=0,
@numReorder as FLOAT=0,
@vcWHSKU as varchar(100)='',
@vcBarCode as varchar(50)='',
@numDomainID AS NUMERIC(9),
@strFieldList as TEXT='',
@vcSerialNo as varchar(100)='',
@vcComments as varchar(1000)='',
@numQty as numeric(18)=0,
@byteMode as tinyint=0,
@numWareHouseItmsDTLID as numeric(18)=0,
@numUserCntID AS NUMERIC(9)=0,
@dtAdjustmentDate AS DATETIME=NULL,
@numWLocationID NUMERIC(9)=0
AS  
BEGIN
	DECLARE @numDomain AS NUMERIC(18,0)
	DECLARE @numGlobalWarehouseItemID NUMERIC(18,0)
	SET @numDomain = @numDomainID

	DECLARE @bitLotNo AS BIT = 0	
	DECLARE @bitSerialized AS BIT  = 0
	DECLARE @bitMatrix AS BIT = 0
	DECLARE @numItemGroup AS NUMERIC(18,0) =0
	DECLARE @vcSKU AS VARCHAR(200) = ''
	DECLARE @vcUPC AS VARCHAR(200) = ''
	DECLARE @monListPrice MONEY


	SELECT 
		@bitLotNo=ISNULL(Item.bitLotNo,0),
		@bitSerialized=ISNULL(Item.bitSerialized,0),
		@bitMatrix = ISNULL(bitMatrix,0),
		@numItemGroup = ISNULL(@numItemGroup,0),
		@vcSKU = ISNULL(vcSKU,0),
		@vcUPC = ISNULL(numBarCodeId,0),
		@monListPrice = ISNULL(monListPrice,0)
	FROM 
		Item 
	WHERE 
		numItemCode=@numItemCode 
		AND numDomainID=@numDomainID

	DECLARE @vcDescription AS VARCHAR(100)

	IF @byteMode=0 or @byteMode=1 or @byteMode=2 or @byteMode=5
	BEGIN
		--Insert/Update WareHouseItems
		IF @byteMode=0 or @byteMode=1
		BEGIN
			IF @numWareHouseItemID>0
			BEGIN
				UPDATE 
					WareHouseItems
				SET 
					numWareHouseID=@numWareHouseID,				
					numReorder=@numReorder,
					monWListPrice=@monWListPrice,
					vcLocation=@vcLocation,
					numWLocationID = @numWLocationID,
					vcWHSKU=@vcWHSKU,
					vcBarCode=@vcBarCode,
					dtModified=GETDATE()   
				WHERE 
					numItemID=@numItemCode 
					AND numDomainID=@numDomainID 
					AND numWareHouseItemID=@numWareHouseItemID
		
				IF NOT EXISTS(SELECT numItemCode FROM dbo.OpportunityItems WHERE numItemCode = @numItemCode)
				BEGIN
					IF ((SELECT ISNULL(numOnHand,0) FROM WareHouseItems where numItemID = @numItemCode AND numDomainId = @numDomainID and numWareHouseItemID = @numWareHouseItemID) = 0)
					BEGIN
						UPDATE 
							WareHouseItems 
						SET 
							numOnHand = @numOnHand 
						WHERE 
							numItemID = @numItemCode 
							AND numDomainID = @numDomainID 
							AND numWareHouseItemID = @numWareHouseItemID		
					END
				END
		
				SET @vcDescription='UPDATE WareHouse'
			END
			ELSE
			BEGIN
				INSERT INTO WareHouseItems 
				(
					numItemID, 
					numWareHouseID,
					numOnHand,
					numReorder,
					monWListPrice,
					vcLocation,
					vcWHSKU,
					vcBarCode,
					numDomainID,
					dtModified,
					numWLocationID
				)  
				VALUES
				(
					@numItemCode,
					@numWareHouseID,
					(Case When @bitLotNo=1 or @bitSerialized=1 then 0 else @numOnHand end),
					@numReorder,
					@monWListPrice,
					@vcLocation,
					@vcWHSKU,
					@vcBarCode,
					@numDomainID,
					GETDATE(),
					@numWLocationID
				)  

				SET @numWareHouseItemID = @@identity


				IF (SELECT COUNT(*) FROM WareHouseItems WHERE numWareHouseID=@numWareHouseID AND numItemID=@numItemCode AND numWLocationID=-1) = 0
				BEGIN
					INSERT INTO WareHouseItems 
					(
						numItemID, 
						numWareHouseID,
						numWLocationID,
						numOnHand,
						numReorder,
						monWListPrice,
						vcLocation,
						vcWHSKU,
						vcBarCode,
						numDomainID,
						dtModified
					)  
					VALUES
					(
						@numItemCode,
						@numWareHouseID,
						-1,
						0,
						0,
						@monWListPrice,
						'',
						@vcWHSKU,
						@vcBarCode,
						@numDomainID,
						GETDATE()
					)  

					SELECT @numGlobalWarehouseItemID = SCOPE_IDENTITY()
				END



				SET @vcDescription='INSERT WareHouse'
			END
		END

		--Insert/Update WareHouseItmsDTL

		DECLARE @OldQty FLOAT = 0

		IF @byteMode=0 or @byteMode=2 or @byteMode=5
		BEGIN
			IF @bitLotNo=1 or @bitSerialized=1
			BEGIN
				IF @bitSerialized=1
					SET @numQty=1
	
				IF @byteMode=0
				BEGIN
					SELECT TOP 1 
						@numWareHouseItmsDTLID=numWareHouseItmsDTLID,
						@OldQty=numQty 
					FROM 
						WareHouseItmsDTL 
					WHERE 
						numWareHouseItemID=@numWareHouseItemID 
						AND vcSerialNo=@vcSerialNo and numQty > 0
				END
				ELSE IF @numWareHouseItmsDTLID>0
				BEGIN
					SELECT TOP 1 
						@OldQty=numQty 
					FROM 
						WareHouseItmsDTL 
					WHERE 
						numWareHouseItmsDTLID=@numWareHouseItmsDTLID
				END

				IF @numWareHouseItmsDTLID>0
				BEGIN
					UPDATE 
						WareHouseItmsDTL 
					SET 
						vcComments = @vcComments,
						numQty=@numQty,
						vcSerialNo=@vcSerialNo
					WHERE 
						numWareHouseItmsDTLID=@numWareHouseItmsDTLID 
						AND numWareHouseItemID=@numWareHouseItemID
                
					SET @vcDescription=CASE WHEN @byteMode=5 THEN 'Inventory Adjustment (Update Lot/Serial#) : ' ELSE 'UPDATE Lot/Serial# : ' END + @vcSerialNo 
				END
				ELSE
				BEGIN
				   INSERT INTO WareHouseItmsDTL
				   (numWareHouseItemID,vcSerialNo,vcComments,numQty,bitAddedFromPO)  
				   VALUES 
				   (@numWareHouseItemID,@vcSerialNo,@vcComments,@numQty,0)
				
				   SET @numWareHouseItmsDTLID=@@identity

				   SET @vcDescription=CASE WHEN @byteMode=5 THEN 'Inventory Adjustment (Insert Lot/Serial#) : ' ELSE 'INSERT Lot/Serial# : ' END + @vcSerialNo 
				END

 				UPDATE 
					WareHouseItems 
				SET 
					numOnHand=numOnHand + (@numQty - @OldQty),
					dtModified=GETDATE() 
				WHERE 
					numWareHouseItemID = @numWareHouseItemID 
					AND [numDomainID] = @numDomainID
					AND (numOnHand + (@numQty - @OldQty))>=0
			END 
		END

		DECLARE @bitOppOrderExists AS BIT = 0

		IF (SELECT COUNT(OI.numOppId) FROM dbo.OpportunityItems OI WHERE OI.numItemCode =@numItemCode) > 0
		BEGIN
			SET @bitOppOrderExists = 1
		END
		ELSE IF (SELECT COUNT(OI.numOppId) FROM dbo.OpportunityKitItems OI WHERE OI.numChildItemID =@numItemCode) > 0
		BEGIN
			SET @bitOppOrderExists = 1
		END
		ELSE IF (SELECT COUNT(OI.numOppId) FROM dbo.OpportunityKitChildItems OI WHERE OI.numItemID =@numItemCode) > 0
		BEGIN
			SET @bitOppOrderExists = 1
		END

		IF @bitMatrix = 1 AND ISNULL(@bitOppOrderExists,0) <> 1 -- ITEM LEVEL ATTRIBUTES
		BEGIN
			DELETE FROM CFW_Fld_Values_Serialized_Items WHERE RecId IN (@numWareHouseItemID ,@numGlobalWarehouseItemID)
			
			INSERT INTO CFW_Fld_Values_Serialized_Items
			(
				Fld_ID,
				Fld_Value,
				RecId,
				bitSerialized
			)                                                  
			SELECT 
				Fld_ID,
				ISNULL(CAST(Fld_Value AS VARCHAR),'') AS Fld_Value,
				@numWareHouseItemID,
				0 
			FROM 
				ItemAttributes
			WHERE
				numDomainID = @numDomainID
				AND numItemCode = @numItemCode 
				
			INSERT INTO CFW_Fld_Values_Serialized_Items
			(
				Fld_ID,
				Fld_Value,
				RecId,
				bitSerialized
			)                                                  
			SELECT 
				Fld_ID,
				ISNULL(CAST(Fld_Value AS VARCHAR),'') AS Fld_Value,
				@numGlobalWarehouseItemID,
				0 
			FROM 
				ItemAttributes
			WHERE
				numDomainID = @numDomainID
				AND numItemCode = @numItemCode    
		END
		ELSE IF DATALENGTH(@strFieldList) > 2 AND ISNULL(@bitOppOrderExists,0) <> 1
		BEGIN
			--Insert Custom Fields base on WareHouseItems/WareHouseItmsDTL 
			declare @hDoc as int     
			EXEC sp_xml_preparedocument @hDoc OUTPUT, @strFieldList                                                                        

				declare  @rows as integer                                        
	                                         
			SELECT @rows=count(*) FROM OPENXML (@hDoc,'/NewDataSet/CusFlds',2)                                                  
							WITH (Fld_ID numeric(9),Fld_Value varchar(100))                                        
	                                                                           

			if @rows>0                                        
			begin  
				Create table #tempTable (ID INT IDENTITY PRIMARY KEY,Fld_ID numeric(9),Fld_Value varchar(100))   
	                                      
				insert into #tempTable (Fld_ID,Fld_Value)                                        
				SELECT * FROM OPENXML (@hDoc,'/NewDataSet/CusFlds',2)                                                  
				WITH (Fld_ID numeric(9),Fld_Value varchar(100))                                           
	                                         
				delete CFW_Fld_Values_Serialized_Items from CFW_Fld_Values_Serialized_Items C inner join #tempTable T on C.Fld_ID=T.Fld_ID where C.RecId=@numWareHouseItemID                              
	      
				INSERT INTO CFW_Fld_Values_Serialized_Items
				(
					Fld_ID,Fld_Value,RecId,bitSerialized
				) 

				select Fld_ID,isnull(Fld_Value,'') as Fld_Value,@numWareHouseItemID,0 from #tempTable 

				drop table #tempTable                                        
			 end  

			 EXEC sp_xml_removedocument @hDoc 
		END	  
 
		EXEC dbo.USP_ManageWareHouseItems_Tracking
		@numWareHouseItemID = @numWareHouseItemID, --  numeric(9, 0)
		@numReferenceID = @numItemCode, --  numeric(9, 0)
		@tintRefType = 1, --  tinyint
		@vcDescription = @vcDescription, --  varchar(100)
		@numModifiedBy = @numUserCntID,
		@ClientTimeZoneOffset = 0,
		@dtRecordDate = @dtAdjustmentDate,
		@numDomainID = @numDomain 

		IF ISNULL(@numGlobalWarehouseItemID,0) > 0
		BEGIN        
			EXEC dbo.USP_ManageWareHouseItems_Tracking
			@numWareHouseItemID = @numGlobalWarehouseItemID,
			@numReferenceID = @numItemCode,
			@tintRefType = 1,
			@vcDescription = @vcDescription,
			@numModifiedBy = @numUserCntID,
			@ClientTimeZoneOffset = 0,
			@dtRecordDate = @dtAdjustmentDate,
			@numDomainID = @numDomainID 
		END

		-- KEEP IT LAST IN SECTION
		IF ISNULL(@numItemGroup,0) > 0 OR ISNULL(@bitMatrix,0) = 1 -- IF IT's MATRIX ITEM THEN 
		BEGIN
			UPDATE WareHouseItems SET monWListPrice = (CASE WHEN @monListPrice > 0 THEN @monListPrice ELSE monWListPrice END), vcWHSKU=@vcSKU, vcBarCode=@vcUPC WHERE numDomainID = @numDomainID AND numItemID = @numItemCode
		END
	END
	ELSE IF @byteMode=3
	BEGIN
		IF EXISTS (SELECT * FROM [OpportunityMaster] OM INNER JOIN [OpportunityItems] OI ON OM.[numOppId] = OI.[numOppId] WHERE OM.[numDomainId]=@numDomainID AND [numWarehouseItmsID]=@numWareHouseItemID)
		BEGIN
			RAISERROR('OpportunityItems_Depend',16,1);
			RETURN
		END

		IF EXISTS (SELECT * FROM OpportunityItemsReceievedLocation WHERE [numDomainId]=@numDomainID AND numWarehouseItemID=@numWareHouseItemID)
		BEGIN
			RAISERROR('OpportunityItems_Depend',16,1);
			RETURN
		END

		IF (SELECT COUNT(*) FROM OpportunityKitItems WHERE numWareHouseItemId=@numWareHouseItemID) > 0
 		BEGIN
	  		RAISERROR('OpportunityKitItems_Depend',16,1);
			RETURN
		END
	
		IF (SELECT COUNT(*) FROM ItemDetails WHERE numWareHouseItemId=@numWareHouseItemID) > 0
 		BEGIN
	  		RAISERROR ('KitItems_Depend',16,1);
			RETURN
		END	
	    
		IF @bitLotNo=1 OR @bitSerialized=1
		BEGIN
			DELETE from CFW_Fld_Values_Serialized_Items where RecId in (select numWareHouseItmsDTLID from WareHouseItmsDTL where numWareHouseItemID=@numWareHouseItemID)
					and bitSerialized=(Case when @bitLotNo=1 or @bitSerialized=1 then 1 else 0 end) 
		END
		ELSE
		BEGIN
			DELETE from CFW_Fld_Values_Serialized_Items where RecId=@numWareHouseItemID
					and bitSerialized=(Case when @bitLotNo=1 or @bitSerialized=1 then 1 else 0 end) 
		END

		DELETE from WareHouseItmsDTL where numWareHouseItemID=@numWareHouseItemID 
		DELETE FROM WareHouseItems_Tracking WHERE numWareHouseItemID=@numWareHouseItemID and numDomainID=@numDomainID
		DELETE from WareHouseItems where numWareHouseItemID=@numWareHouseItemID and numDomainID=@numDomainID
	END
	ELSE IF @byteMode=4
	BEGIN
		IF @bitLotNo=1 or @bitSerialized=1
		BEGIN
			DELETE from CFW_Fld_Values_Serialized_Items where RecId=@numWareHouseItmsDTLID
			and bitSerialized=(Case when @bitLotNo=1 or @bitSerialized=1 then 1 else 0 end) 
		END

		update WHI SET numOnHand=WHI.numOnHand - WHID.numQty,dtModified=GETDATE()
		from WareHouseItems WHI join WareHouseItmsDTL WHID on WHI.numWareHouseItemID=WHID.numWareHouseItemID 
		where WHID.numWareHouseItmsDTLID=@numWareHouseItmsDTLID 
		AND WHI.numDomainID = @numDomainID
		AND (WHI.numOnHand - WHID.numQty)>=0

		SELECT @numWareHouseItemID=WHI.numWareHouseItemID,@numItemCode=numItemID 
		from WareHouseItems WHI join WareHouseItmsDTL WHID on WHI.numWareHouseItemID=WHID.numWareHouseItemID 
		where WHID.numWareHouseItmsDTLID=@numWareHouseItmsDTLID 
		AND WHI.numDomainID = @numDomainID


		EXEC dbo.USP_ManageWareHouseItems_Tracking
			@numWareHouseItemID = @numWareHouseItemID, --  numeric(9, 0)
			@numReferenceID = @numItemCode, --  numeric(9, 0)
			@tintRefType = 1, --  tinyint
			@vcDescription = 'DELETE Lot/Serial#', --  varchar(100)
			@numModifiedBy = @numUserCntID,
			@ClientTimeZoneOffset = 0,
			@dtRecordDate = NULL,
			@numDomainID = @numDomain

		DELETE from WareHouseItmsDTL where numWareHouseItmsDTLID=@numWareHouseItmsDTLID
	END
END
/****** Object:  StoredProcedure [dbo].[USP_CaseDetails]    Script Date: 07/26/2008 16:15:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--exec USP_CaseDetails @numCaseId = 34              
              
              
--sp_helptext USP_CaseDetails              
              
              
 --created by anoop jayaraj                            
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_casedetails')
DROP PROCEDURE usp_casedetails
GO
CREATE PROCEDURE [dbo].[USP_CaseDetails]                                  
@numCaseId as numeric(9) ,    
@numDomainID as numeric(9),    
@ClientTimeZoneOffset as int                                   
                                  
as                                  
                                  
select C.numContactID,vcCaseNumber,
 intTargetResolveDate,textSubject,Div.numDivisionID,                                  
 numStatus,dbo.GetListIemName(numStatus) AS vcCaseStatus, numPriority,textDesc,tintCRMType,                                  
 textInternalComments,numReason,C.numContractID,
 ISNULL((SELECT cm.vcContractName FROM dbo.ContractManagement cm WHERE cm.numContractId = C.numContractId),'') AS vcContractName,
 numOrigin,numType,                       
 dbo.fn_GetContactName(C.numCreatedby)+' ' + convert(varchar(20),dateadd(minute,-@ClientTimeZoneOffset,C.bintCreatedDate)) as CreatedBy,                                  
 dbo.fn_GetContactName(C.numModifiedBy)+' ' + convert(varchar(20),dateadd(minute,-@ClientTimeZoneOffset,C.bintModifiedDate)) as ModifiedBy,                      
 dbo.fn_GetContactName(C.numRecOwner) as RecOwner,                                
 C.numRecOwner,
 tintSupportKeyType,                                  
 C.numAssignedTo,C.numAssignedBy,isnull(Addc.vcFirstName,'') + ' ' + isnull(Addc.vcLastName,'') as vcName,                                  
 isnull(vcEmail,'') as vcEmail,isnull(numPhone,'') + case when numPhoneExtension is null then '' when numPhoneExtension ='' then '' else ', '+ numPhoneExtension end  as Phone,                                  
 Com.numCompanyId,Com.vcCompanyName,(select count(*) from Comments where numCaseID =@numCaseId)  as NoofCases,
 (SELECT COUNT(*) FROM   dbo.GenericDocuments WHERE  numRecID = @numCaseId AND vcDocumentSection = 'CS') AS DocumentCount, Div.numTerID
 from Cases C                                  
 left join AdditionalContactsInformation Addc                                  
 on Addc.numContactId=C.numContactID                                   
 join DivisionMaster Div                                  
 on Div.numDivisionId=Addc.numDivisionId                                  
 join CompanyInfo Com                                   
 on Com.numCompanyID=div.numCompanyID                                  
                                 
 where numCaseId=@numCaseId and C.numDomainID=@numDomainID
GO
/****** Object:  StoredProcedure [dbo].[Usp_CreateCloneItem]    Script Date: 18/06/2013 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created By Manish Anjara
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='Usp_CreateCloneItem')
DROP PROCEDURE Usp_CreateCloneItem
GO
CREATE PROCEDURE [dbo].[Usp_CreateCloneItem]
(
	@numItemCode		BIGINT OUTPUT,
	@numDomainID		NUMERIC(18,0)
)
AS 

BEGIN
--Item 	
--ItemAPI-
--ItemCategory
--ItemDetails
--ItemImages
--ItemTax
--Vendor,
--WareHouseItems
--CFW_FLD_Values_Item
BEGIN TRY
BEGIN TRANSACTION

	DECLARE @numItemGroup AS NUMERIC(18,0)
	DECLARE @bitMatrix AS NUMERIC(18,0)
	DECLARE @vcItemName AS VARCHAR(300)

	SELECT @numItemGroup=numItemGroup,@bitMatrix=bitMatrix,@vcItemName=vcItemName FROM Item WHERE numItemCode = @numItemCode AND numDomainID = @numDomainID

	DECLARE @numNewItemCode AS BIGINT
	
	INSERT INTO dbo.Item 
	(vcItemName,txtItemDesc,charItemType,monListPrice,numItemClassification,bitTaxable,vcSKU,bitKitParent,numVendorID,numDomainID,numCreatedBy,bintCreatedDate,
	bintModifiedDate,numModifiedBy,bitAllowBackOrder,bitSerialized,vcModelID,numItemGroup,numCOGsChartAcntId,numAssetChartAcntId,numIncomeChartAcntId,
	monAverageCost,monCampaignLabourCost,bitFreeShipping,fltLength,fltWidth,fltHeight,fltWeight,vcUnitofMeasure,bitCalAmtBasedonDepItems,bitShowDeptItemDesc,
	bitShowDeptItem,bitAssembly,numBarCodeId,vcManufacturer,numBaseUnit,numPurchaseUnit,numSaleUnit,bitLotNo,IsArchieve,numItemClass,vcPathForImage,
	vcPathForTImage,tintStandardProductIDType,numShipClass,vcExportToAPI,bitAllowDropShip,bitVirtualInventory,bitContainer,bitMatrix)
	SELECT vcItemName,txtItemDesc,charItemType,monListPrice,numItemClassification,bitTaxable,vcSKU,bitKitParent,numVendorID,numDomainID,numCreatedBy,GETDATE(),
	GETDATE(),numModifiedBy,bitAllowBackOrder,bitSerialized,vcModelID,numItemGroup,numCOGsChartAcntId,numAssetChartAcntId,numIncomeChartAcntId,
	0,monCampaignLabourCost,bitFreeShipping,fltLength,fltWidth,fltHeight,fltWeight,vcUnitofMeasure,bitCalAmtBasedonDepItems,bitShowDeptItemDesc,
	bitShowDeptItem,bitAssembly,numBarCodeId,vcManufacturer,numBaseUnit,numPurchaseUnit,numSaleUnit,bitLotNo,IsArchieve,numItemClass,vcPathForImage,
	vcPathForTImage,tintStandardProductIDType,numShipClass,vcExportToAPI,bitAllowDropShip,bitVirtualInventory,bitContainer,bitMatrix
	FROM Item WHERE numItemCode = @numItemCode AND numDomainID = @numDomainID
	 
	SELECT @numNewItemCode  = @@IDENTITY


	IF ISNULL(@bitMatrix,0) = 1
	BEGIN
		DECLARE @vcProductVariation VARCHAR(300)

		WITH data (ProductId, ProductOptionGroupId, ProductOptionId) AS 
		(
			SELECT
				@numItemCode
				,CFW_Fld_Master.Fld_id
				,ListDetails.numListItemID
			FROM
				ItemGroupsDTL
			INNER JOIN
				CFW_Fld_Master
			ON
				ItemGroupsDTL.numOppAccAttrID = CFW_Fld_Master.Fld_id
			INNER JOIN
				ListDetails
			ON
				CFW_Fld_Master.numlistid = ListDetails.numListID
			WHERE
				numItemGroupID = @numItemGroup
		),
		ranked AS (
		/* ranking the group IDs */
			SELECT
				ProductId,
				ProductOptionGroupId,
				ProductOptionId,
				GroupRank = DENSE_RANK() OVER (PARTITION BY ProductId ORDER BY ProductOptionGroupId)
			FROM 
				data
		),
		crossjoined AS (
			/* obtaining all possible combinations */
			SELECT
				ProductId,
				ProductOptionGroupId,
				GroupRank,
				ProductVariant = CAST(CONCAT(ProductOptionGroupId,':',ProductOptionId) AS varchar(250))
			FROM 
				ranked
			WHERE 
				GroupRank = 1
			UNION ALL
			SELECT
				r.ProductId,
				r.ProductOptionGroupId,
				r.GroupRank,
				ProductVariant = CAST(CONCAT(c.ProductVariant,',',r.ProductOptionGroupId,':',r.ProductOptionId) AS VARCHAR(250))
			FROM 
				ranked r
			INNER JOIN 
				crossjoined c 
			ON 
				r.ProductId = c.ProductId
			AND 
				r.GroupRank = c.GroupRank + 1
		  ),
		  maxranks AS (
			/* getting the maximum group rank value for every product */
			SELECT
				ProductId,
				MaxRank = MAX(GroupRank)
			FROM 
				ranked
			GROUP BY 
				ProductId
		  )
		
		SELECT TOP 1  
			@vcProductVariation = c.ProductVariant 
		FROM 
			crossjoined c 
		INNER JOIN 
			maxranks m 
		ON 
			c.ProductId = m.ProductId
			AND c.GroupRank = m.MaxRank
			AND c.ProductVariant NOT IN (SELECT 
											STUFF((SELECT ',' + CONCAT(FLD_ID,':',FLD_Value) FROM ItemAttributes WHERE numDomainID=@numDomainID AND numItemCode=IR.numItemCode FOR XML PATH(''),TYPE).value('(./text())[1]','VARCHAR(MAX)'),1,1,'') AS NameValues
										FROM 
											Item
										INNER JOIN
											ItemAttributes IR
										ON
											IR.numDomainID=@numDomainID
											AND Item.numItemCode =IR.numItemCode
										WHERE 
											Item.numDomainID=@numDomainID
											AND bitMatrix = 1
											AND numItemGroup = @numItemGroup
											AND vcItemName = @vcItemName
										GROUP BY
											IR.numItemCode)

		IF LEN(ISNULL(@vcProductVariation,'')) = 0
		BEGIN
			RAISERROR('ATTRIBUTES_ARE_NOT_AVAILABLE_OR_NO_COMBINATION_LEFT_TO_ADD',16,1)
		END
		ELSE
		BEGIN
			INSERT INTO ItemAttributes
			(
				numDomainID
				,numItemCode
				,FLD_ID
				,FLD_Value
			)
			SELECT
				@numDomainID
				,@numNewItemCode
				,SUBSTRING(OutParam,0,CHARINDEX(':',OutParam,0))
				,SUBSTRING(OutParam,CHARINDEX(':',OutParam,0) + 1,LEN(OutParam))
			FROM
				dbo.SplitString(@vcProductVariation,',')
		END
	END
	 
	INSERT INTO dbo.ItemAPI 
	(WebApiId,numDomainId,numItemID,vcAPIItemID,numCreatedby,dtCreated,numModifiedby,dtModified)
	SELECT WebApiId,numDomainId,@numNewItemCode,vcAPIItemID,numCreatedby,GETDATE(),numModifiedby,GETDATE()
	FROM dbo.ItemAPI WHERE numItemID = @numItemCode AND numDomainId = @numDomainId
	
	INSERT INTO dbo.ItemCategory ( numItemID,numCategoryID )
	SELECT @numNewItemCode, numCategoryID FROM ItemCategory WHERE numItemID = @numItemCode
	
	INSERT INTO dbo.ItemDetails (numItemKitID,numChildItemID,numQtyItemsReq,numWareHouseItemId,numUOMId,vcItemDesc,sintOrder)
	SELECT @numNewItemCode,numChildItemID,numQtyItemsReq,numWareHouseItemId,numUOMId,vcItemDesc,sintOrder FROM dbo.ItemDetails WHERE numItemKitID = @numItemCode
	
	INSERT INTO dbo.ItemImages (numItemCode,vcPathForImage,vcPathForTImage,bitDefault,intDisplayOrder,numDomainId,bitIsImage)
	SELECT @numNewItemCode,vcPathForImage,vcPathForTImage,bitDefault,intDisplayOrder,numDomainId,bitIsImage 
	FROM dbo.ItemImages WHERE numItemCode = @numItemCode  AND numDomainId = @numDomainID
	
	INSERT INTO dbo.ItemTax (numItemCode,numTaxItemID,bitApplicable)
	SELECT @numNewItemCode,numTaxItemID,bitApplicable FROM dbo.ItemTax WHERE numItemCode = @numItemCode
	
	INSERT INTO dbo.Vendor (numVendorID,vcPartNo,numDomainID,monCost,numItemCode,intMinQty)
	SELECT numVendorID,vcPartNo,numDomainID,monCost,@numNewItemCode,intMinQty 
	FROM dbo.Vendor WHERE numItemCode = @numItemCode AND numDomainID = @numDomainID
	
	INSERT INTO dbo.WareHouseItems (numItemID,numWareHouseID,numOnHand,numOnOrder,numReorder,numAllocation,numBackOrder,monWListPrice,vcLocation,vcWHSKU,
	vcBarCode,numDomainID,dtModified,numWLocationID) 
	SELECT @numNewItemCode,numWareHouseID,0,0,0,0,0,monWListPrice,vcLocation,vcWHSKU,vcBarCode,numDomainID,GETDATE(),numWLocationID 
	FROM dbo.WareHouseItems  WHERE numItemID = @numItemCode AND numDomainID = @numDomainID

	IF ISNULL(@bitMatrix,0) = 1 AND (SELECT COUNT(*) FROM ItemAttributes WHERE numDomainID=@numDomainID AND numItemCode=@numNewItemCode) > 0
	BEGIN
		-- INSERT NEW WAREHOUSE ATTRIBUTES
		INSERT INTO CFW_Fld_Values_Serialized_Items
		(
			Fld_ID,
			Fld_Value,
			RecId,
			bitSerialized
		)                                                  
		SELECT 
			Fld_ID,
			ISNULL(CAST(Fld_Value AS VARCHAR),'') AS Fld_Value,
			TEMPWarehouse.numWarehouseItemID,
			0 
		FROM 
			ItemAttributes  
		OUTER APPLY
		(
			SELECT
				numWarehouseItemID
			FROM 
				WarehouseItems 
			WHERE 
				numDomainID=@numDomainID 
				AND numItemID=@numNewItemCode
		) AS TEMPWarehouse
		WHERE
			numDomainID=@numDomainID 
			AND numItemCode=@numNewItemCode
	END
	
	INSERT INTO CFW_FLD_Values_Item (Fld_ID,Fld_Value,RecId)  
	SELECT Fld_ID,Fld_Value,@numNewItemCode FROM CFW_FLD_Values_Item  WHERE RecId = @numItemCode 
COMMIT
END TRY
BEGIN CATCH
	DECLARE @ErrorMessage NVARCHAR(4000)
	DECLARE @ErrorNumber INT
	DECLARE @ErrorSeverity INT
	DECLARE @ErrorState INT
	DECLARE @ErrorLine INT
	DECLARE @ErrorProcedure NVARCHAR(200)

	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;

	SELECT 
		@ErrorMessage = ERROR_MESSAGE(),
		@ErrorNumber = ERROR_NUMBER(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE(),
		@ErrorLine = ERROR_LINE(),
		@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

	RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);
END CATCH	
END
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_DemandForecast_GetChildItems')
DROP PROCEDURE USP_DemandForecast_GetChildItems
GO
CREATE PROCEDURE [dbo].[USP_DemandForecast_GetChildItems]  
(
	@numDomainID NUMERIC(18,0),
	@numDFID NUMERIC(18,0),
	@numOppID NUMERIC(18,0),
	@numOppItemID NUMERIC(18,0),
	@numItemCode NUMERIC(18,0),
	@numQty AS FLOAT,
	@bitKit BIT,
	@bitAssembly BIT
)
AS
BEGIN
	IF @bitKit = 1
	BEGIN
		;WITH CTE(numOppChildItemID,numOppID,numOppItemID,numItemCode,bitKitParent,numItemGroup,numWarehouseID,numWarehouseItemID,numQty) AS
		(
			SELECT 
				numOppChildItemID
				,OKI.numOppId
				,OKI.numOppItemID
				,numChildItemID
				,I.bitKitParent
				,I.numItemGroup
				,WI.numWarehouseID
				,WI.numWarehouseItemID
				,@numQty * numQtyItemsReq_Orig
			FROM 
				OpportunityKitItems OKI
			INNER JOIN
				Item I
			ON
				OKI.numChildItemID = I.numItemCode
			INNER JOIN
				WarehouseItems WI
			ON
				OKI.numWareHouseItemId = WI.numWarehouseItemID
			WHERE
				OKI.numOppID = @numOppID
				AND OKI.numOppItemID = @numOppItemID
			UNION ALL
			SELECT
				OKCI.numOppKitChildItemID
				,OKCI.numOppId
				,OKCI.numOppItemID
				,OKCI.numItemID
				,I.bitKitParent
				,I.numItemGroup
				,WI.numWarehouseID
				,WI.numWarehouseItemID
				,c.numQty * numQtyItemsReq_Orig
			FROM
				OpportunityKitChildItems OKCI
			INNER JOIN	
				CTE c
			ON
				OKCI.numOppChildItemID = c.numOppChildItemID
			INNER JOIN
				Item I
			ON
				OKCI.numItemID = I.numItemCode
			INNER JOIN
				WarehouseItems WI
			ON
				OKCI.numWareHouseItemId = WI.numWarehouseItemID
		)


		SELECT 
			numItemCode
			,(CASE
				WHEN ISNULL(c.numItemGroup,0) > 0 AND LEN(dbo.fn_GetAttributes(numWareHouseItemID,0)) > 0
				THEN
					(CASE 
						WHEN EXISTS (SELECT numWareHouseItemID FROM WareHouseItems WIInner WHERE WIInner.numDomainID=@numDomainID AND  WIInner.numItemID=c.numItemCode AND WIInner.numWareHouseID=c.numWareHouseID AND WIInner.numWareHouseID=-1 AND  dbo.fn_getattributes(WIInner.numWareHouseItemID,0) = dbo.fn_getattributes(c.numWareHouseItemID,0)) 
						THEN
							(SELECT TOP 1 numWareHouseItemID FROM WareHouseItems WIInner WHERE WIInner.numDomainID=@numDomainID AND  WIInner.numItemID=c.numItemCode AND WIInner.numWareHouseID=c.numWareHouseID AND WIInner.numWareHouseID=-1 AND  dbo.fn_getattributes(WIInner.numWareHouseItemID,0) = dbo.fn_getattributes(c.numWareHouseItemID,0))
						ELSE
							c.numWareHouseItemID
					END)
				ELSE
					(SELECT TOP 1 numWareHouseItemID FROM WareHouseItems WHERE numDomainID=@numDomainID AND numItemID=c.numItemCode AND numWareHouseID=c.numWareHouseID AND numWLocationID = -1)
			END) AS numWarehouseItemID
			,CEILING(CAST(numQty AS FLOAT)) AS numQty
		FROM  
			CTE c
		WHERE 
			ISNULL(bitKitParent,0) = 0
	END
	ELSE IF @bitAssembly = 1
	BEGIN
		-- GET ASSEMBLY CHILD ITEMS
		;WITH CTEAssembly(numItemCode, bitAssembly, numItemGroup, numWareHouseID,numWarehouseItemID, numQty) AS 
		( 
			SELECT   
				numChildItemID,
				I.bitAssembly,
				I.numItemGroup,
				ISNULL(WI.numWareHouseID, 0),
				ISNULL(WI.numWareHouseItemID, 0),
				CAST(DTL.numQtyItemsReq * @numQty AS FLOAT)
			FROM
				ItemDetails Dtl
			INNER JOIN
				Item I
			ON
				Dtl.numChildItemID = I.numItemCode
			INNER JOIN
				WareHouseItems WI
			ON
				Dtl.numWareHouseItemId = WI.numWareHouseItemID
			WHERE 
				numItemKitID = @numItemCode
			UNION ALL
			SELECT 
				I.numItemCode,
				I.bitAssembly,
				I.numItemGroup,
				ISNULL(WI.numWareHouseID, 0),
				ISNULL(WI.numWareHouseItemID, 0),
				CAST(DTL.numQtyItemsReq * c.numQty AS FLOAT)
			FROM
				ItemDetails Dtl
			INNER JOIN 
				CTEAssembly c 
			ON 
				Dtl.numItemKitID = c.numItemCode
			INNER JOIN
				Item I
			ON
				Dtl.numChildItemID = I.numItemCode
			INNER JOIN
				WareHouseItems WI
			ON
				Dtl.numWareHouseItemId = WI.numWareHouseItemID
			WHERE    
				Dtl.numChildItemID != c.numItemCode
		)

		SELECT
			numItemCode,
			(CASE
				WHEN ISNULL(c.numItemGroup,0) > 0 AND LEN(dbo.fn_GetAttributes(numWareHouseItemID,0)) > 0
				THEN
					(CASE 
						WHEN EXISTS (SELECT numWareHouseItemID FROM WareHouseItems WIInner WHERE WIInner.numDomainID=@numDomainID AND  WIInner.numItemID=c.numItemCode AND WIInner.numWareHouseID=c.numWareHouseID AND WIInner.numWareHouseID=-1 AND  dbo.fn_getattributes(WIInner.numWareHouseItemID,0) = dbo.fn_getattributes(c.numWareHouseItemID,0)) 
						THEN
							(SELECT TOP 1 numWareHouseItemID FROM WareHouseItems WIInner WHERE WIInner.numDomainID=@numDomainID AND  WIInner.numItemID=c.numItemCode AND WIInner.numWareHouseID=c.numWareHouseID AND WIInner.numWareHouseID=-1 AND  dbo.fn_getattributes(WIInner.numWareHouseItemID,0) = dbo.fn_getattributes(c.numWareHouseItemID,0))
						ELSE
							c.numWareHouseItemID
					END)
				ELSE
					(SELECT TOP 1 numWareHouseItemID FROM WareHouseItems WHERE numDomainID=@numDomainID AND numItemID=c.numItemCode AND numWareHouseID=c.numWareHouseID AND numWLocationID = -1)
			END) AS numWarehouseItemID
			,CEILING(CAST(numQty AS FLOAT)) AS numQty
		FROM
			CTEAssembly c
		WHERE
			ISNULL(bitAssembly,0) = 0
	END
END
/****** Object:  StoredProcedure [dbo].[USP_EcommerceSettings]    Script Date: 03/25/2009 16:46:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj              
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_ecommercesettings')
DROP PROCEDURE usp_ecommercesettings
GO
CREATE PROCEDURE [dbo].[USP_EcommerceSettings]              
@numDomainID as numeric(9),              
@vcPaymentGateWay as int ,                
@vcPGWUserId as varchar(100)='' ,                
@vcPGWPassword as varchar(100),              
@bitShowInStock as bit,              
@bitShowQOnHand as bit,
@bitCheckCreditStatus as BIT,
@numDefaultWareHouseID as numeric(9),
@numRelationshipId as numeric(9),
@numProfileId as numeric(9),
@bitHidePriceBeforeLogin	BIT,
@bitAuthOnlyCreditCard BIT=0,
@bitSendMail BIT = 1,
@vcGoogleMerchantID VARCHAR(1000)  = '',
@vcGoogleMerchantKey VARCHAR(1000) = '',
@IsSandbox BIT ,
@numAuthrizedOrderStatus NUMERIC,
@numSiteID numeric(18, 0),
@vcPaypalUserName VARCHAR(50)  = '',
@vcPaypalPassword VARCHAR(50)  = '',
@vcPaypalSignature VARCHAR(500)  = '',
@IsPaypalSandbox BIT,
@bitSkipStep2 BIT = 0,
@bitDisplayCategory BIT = 0,
@bitShowPriceUsingPriceLevel BIT = 0,
@bitEnableSecSorting BIT = 0,
@bitSortPriceMode   BIT=0,
@numPageSize    INT=15,
@numPageVariant  INT=6,
@bitAutoSelectWarehouse BIT =0,
@bitPreSellUp BIT = 0, 
@bitPostSellUp BIT = 0, 
@dcPostSellDiscount DECIMAL  = 0,
@numDefaultClass NUMERIC(18,0) = 0,
@vcPreSellUp VARCHAR(200)=NULL,
@vcPostSellUp VARCHAR(200)=NULL,
@vcRedirectThankYouUrl VARCHAR(300)=NULL
as              
            
            
if not exists(select 'col1' from eCommerceDTL where numDomainID=@numDomainID AND [numSiteId] = @numSiteID)            
INSERT INTO eCommerceDTL
           (numDomainId,
            vcPaymentGateWay,
            vcPGWManagerUId,
            vcPGWManagerPassword,
            bitShowInStock,
            bitShowQOnHand,
            numDefaultWareHouseID,
            bitCheckCreditStatus,
            [numRelationshipId],
            [numProfileId],
            [bitHidePriceBeforeLogin],
            bitAuthOnlyCreditCard,
            bitSendEmail,
            vcGoogleMerchantID,
            vcGoogleMerchantKey,
            IsSandbox ,
            numAuthrizedOrderStatus,
			numSiteId,
			vcPaypalUserName,
			vcPaypalPassword,
			vcPaypalSignature,
			IsPaypalSandbox,
			bitSkipStep2,
			bitDisplayCategory,
			bitShowPriceUsingPriceLevel,
			bitEnableSecSorting,
			bitSortPriceMode,
			numPageSize,
			numPageVariant,
			bitAutoSelectWarehouse,
			[bitPreSellUp],
			[bitPostSellUp],
			[dcPostSellDiscount],
			numDefaultClass,
			vcPreSellUp,
			vcPostSellUp,
			vcRedirectThankYouUrl
			)

VALUES     (@numDomainID,
            @vcPaymentGateWay,
            @vcPGWUserId,
            @vcPGWPassword,
            @bitShowInStock,
            @bitShowQOnHand,
            @numDefaultWareHouseID,
            @bitCheckCreditStatus,
            @numRelationshipId,
            @numProfileId,
            @bitHidePriceBeforeLogin,
            @bitAuthOnlyCreditCard,
            @bitSendMail,
            @vcGoogleMerchantID,
            @vcGoogleMerchantKey ,
            @IsSandbox,
            @numAuthrizedOrderStatus,
			@numSiteID,
			@vcPaypalUserName,
			@vcPaypalPassword,
			@vcPaypalSignature,
			@IsPaypalSandbox,
			@bitSkipStep2,
			@bitDisplayCategory,
			@bitShowPriceUsingPriceLevel,
			@bitEnableSecSorting,
			@bitSortPriceMode,
			@numPageSize,
			@numPageVariant,
			@bitAutoSelectWarehouse,
			@bitPreSellUp,
			@bitPostSellUp,
			@dcPostSellDiscount,
			@numDefaultClass,
			@vcPreSellUp,
			@vcPostSellUp,
			@vcRedirectThankYouUrl
            )
 else            
 update eCommerceDTL                                   
   set              
 vcPaymentGateWay=@vcPaymentGateWay,                
 vcPGWManagerUId=@vcPGWUserId ,                
 vcPGWManagerPassword= @vcPGWPassword,              
 bitShowInStock=@bitShowInStock ,               
 bitShowQOnHand=@bitShowQOnHand,        
 numDefaultWareHouseID =@numDefaultWareHouseID,
 bitCheckCreditStatus=@bitCheckCreditStatus ,
 numRelationshipId = @numRelationshipId,
 numProfileId = @numProfileId,
 [bitHidePriceBeforeLogin]=@bitHidePriceBeforeLogin,
 bitAuthOnlyCreditCard=@bitAuthOnlyCreditCard,
 bitSendEmail = @bitSendMail,
 vcGoogleMerchantID = @vcGoogleMerchantID ,
 vcGoogleMerchantKey =  @vcGoogleMerchantKey ,
 IsSandbox = @IsSandbox,
 numAuthrizedOrderStatus = @numAuthrizedOrderStatus,
	numSiteId = @numSiteID,
vcPaypalUserName = @vcPaypalUserName ,
vcPaypalPassword = @vcPaypalPassword,
vcPaypalSignature = @vcPaypalSignature,
IsPaypalSandbox = @IsPaypalSandbox,
bitSkipStep2 = @bitSkipStep2,
bitDisplayCategory = @bitDisplayCategory,
bitShowPriceUsingPriceLevel = @bitShowPriceUsingPriceLevel,
bitEnableSecSorting = @bitEnableSecSorting,
bitSortPriceMode=@bitSortPriceMode,
numPageSize=@numPageSize,
numPageVariant=@numPageVariant,
bitAutoSelectWarehouse=@bitAutoSelectWarehouse,
[bitPreSellUp] = @bitPreSellUp,
[bitPostSellUp] = @bitPostSellUp,
[dcPostSellDiscount] = @dcPostSellDiscount,
numDefaultClass=@numDefaultClass,
vcPreSellUp=@vcPreSellUp,
vcPostSellUp=@vcPostSellUp,
vcRedirectThankYouUrl=@vcRedirectThankYouUrl
 where numDomainId=@numDomainID AND [numSiteId] = @numSiteID
/****** Object:  StoredProcedure [dbo].[USP_EditCntInfo]    Script Date: 07/26/2008 16:15:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created By Anoop Jayaraj                                 
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_editcntinfo')
DROP PROCEDURE usp_editcntinfo
GO
CREATE PROCEDURE [dbo].[USP_EditCntInfo]                                                
@numContactID NUMERIC(9) ,    
@numDomainID as numeric(9)  
 ,  
@ClientTimeZoneOffset as int                     
                                              
AS                                                
BEGIN                                                
                                                
  SELECT                        
   A.numContactId, A.vcGivenName, A.vcFirstName,                                                 
                      A.vcLastName, D.numDivisionID, C.numCompanyId,                                                 
                      C.vcCompanyName, D.vcDivisionName, D.numDomainID,  
					  ISNULL((select vcdata from listdetails where numListItemID = C.numCompanyType),'') as vcCompanyTypeName,                                               
                      A.numPhone, A.numPhoneExtension, A.vcEmail,  A.numTeam,                                               
                      A.vcFax, A.numContactType,  A.charSex, A.bintDOB,                                                 
                      A.vcPosition, A.txtNotes, A.numCreatedBy,                                                
                      A.numCell,A.NumHomePhone,A.vcAsstFirstName,                                                
                      A.vcAsstLastName,A.numAsstPhone,A.numAsstExtn,                                                
                      A.vcAsstEmail,A.charSex, A.vcDepartment,                                                 
                      AD.vcStreet AS vcpStreet,                                            
                      AD.vcCity AS vcPCity, 
                      dbo.fn_GetState(AD.numState) as State, dbo.fn_GetListName(AD.numCountry,0) as Country,
                      AD.vcPostalCode AS vcPPostalCode,A.bitOptOut,vcDepartment,                                                
        A.numManagerID, A.vcCategory  , dbo.fn_GetContactName(A.numCreatedBy)+' '+ convert(varchar(20),dateadd(minute,-@ClientTimeZoneOffset,A.bintCreatedDate)) CreatedBy,                                            
          dbo.fn_GetContactName(A.numModifiedBy)+' '+ convert(varchar(20),dateadd(minute,-@ClientTimeZoneOffset,A.bintModifiedDate)) ModifiedBy,vcTitle,vcAltEmail,                                            
  dbo.fn_GetContactName(A.numRecOwner) as RecordOwner,  A.numRecOwner, D.numAssignedTo,  D.numTerID,
  C.vcCompanyName,C.vcWebSite, 
--  dbo.fn_GetState(AddC.vcPState) as State, dbo.fn_GetListName(AddC.vcPCountry,0) as Country,
   A.numEmpStatus,                          
  isnull((select top 1 case when bitEngaged=0 then 'Disengaged' when bitEngaged=1 then 'Engaged' end as ECampStatus  from ConECampaign                          
where  numContactID=@numContactID order by numConEmailCampID desc),'-') as CampStatus ,                        
dbo.GetECamLastAct(@numContactID) as Activity ,       
(select  count(*) from dbo.GenericDocuments   where numRecID=@numContactID and  vcDocumentSection='C') as DocumentCount,
(SELECT count(*)from CompanyAssociations where numDivisionID=@numContactID and bitDeleted=0 ) as AssociateCountFrom,              
(SELECT count(*)from CompanyAssociations where numAssociateFromDivisionID=@numContactID and bitDeleted=0 ) as AssociateCountTo    ,        
vcItemId ,        
vcChangeKey,              
vcCompanyName,      
tintCRMType ,isnull((select top 1 numUserId from  UserMaster where numUserDetailId=A.numContactId),0) as numUserID,
dbo.fn_GetListName(C.numNoOfEmployeesId,0) AS NoofEmp,ISNULL(vcImageName,'') vcImageName
FROM         AdditionalContactsInformation A     
INNER JOIN  DivisionMaster D ON A.numDivisionId = D.numDivisionID     
INNER JOIN  CompanyInfo C ON D.numCompanyID = C.numCompanyId                      
LEFT JOIN AddressDetails AD ON  A.numDomainID = AD.numDomainID AND AD.numRecordID= A.numContactId AND AD.tintAddressOf=1 AND AD.tintAddressType=0 AND AD.bitIsPrimary=1
WHERE     A.numContactId = @numContactID and A.numDomainID=@numDomainID                  
                                   
END
GO
GO
/****** Object:  StoredProcedure [dbo].[USP_GET_DYNAMIC_IMPORT_FIELDS]  ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO

IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_GET_DYNAMIC_IMPORT_FIELDS' ) 
    DROP PROCEDURE USP_GET_DYNAMIC_IMPORT_FIELDS
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--- USP_GET_DYNAMIC_IMPORT_FIELDS 48,2,40,1
CREATE PROCEDURE [dbo].[USP_GET_DYNAMIC_IMPORT_FIELDS]
    (
      @numFormID BIGINT,
      @intMode INT,
      @numImportFileID NUMERIC = 0,
      @numDomainID NUMERIC = 0
    )
AS 
    BEGIN
	-- EXEC USP_GET_DYNAMIC_IMPORT_FIELDS 20,2,0,1  -> Item
	-- EXEC USP_GET_DYNAMIC_IMPORT_FIELDS 20,2,0,72  -> Item
	-- EXEC USP_GET_DYNAMIC_IMPORT_FIELDS 54,2,1,1  -> Item Images
	-- EXEC USP_GET_DYNAMIC_IMPORT_FIELDS 31,2,1,1  -> Item Assembly
	-- EXEC USP_GET_DYNAMIC_IMPORT_FIELDS 54,2,0,1  -> Item Vendor
	-- EXEC USP_GET_DYNAMIC_IMPORT_FIELDS 36,2,1,1  -> Organization 
	-- EXEC USP_GET_DYNAMIC_IMPORT_FIELDS 43,2,1,1  -> Organization Correspondence
	-- EXEC USP_GET_DYNAMIC_IMPORT_FIELDS 48,2,1,1  -> Item WareHouse
	-- EXEC USP_GET_DYNAMIC_IMPORT_FIELDS 98,2,1,1  -> Address Details
		
        IF @intMode = 1 -- TO BIND CATEGORY DROP DOWN
            BEGIN

                SELECT  20 AS [intImportMasterID],
                        'Item' AS [vcImportName]
                UNION ALL
                SELECT  48 AS [intImportMasterID],
                        'Item WareHouse' AS [vcImportName]
                UNION ALL
                SELECT  31 AS [intImportMasterID],
                        'Kit sub-items' AS [vcImportName]
                UNION ALL
                SELECT  54 AS [intImportMasterID],
                        'Item Images' AS [vcImportName]
                UNION ALL
                SELECT  55 AS [intImportMasterID],
                        'Item Vendor' AS [vcImportName]
                UNION ALL
                SELECT  65 AS [intImportMasterID],
                        'Item Web API Details' AS [vcImportName]
                UNION ALL
                SELECT  36 AS [intImportMasterID],
                        'Organization' AS [vcImportName]
                UNION ALL
                SELECT  43 AS [intImportMasterID],
                        'Organization Correspondence' AS [vcImportName]                        
                UNION ALL
                SELECT  98 AS [intImportMasterID],
                        'Address Details' AS [vcImportName]                        		
            END
	
        IF @intMode = 2 -- TO GET DETAILS OF DYNAMIC FORM FIELDS
            BEGIN
	
		
                SELECT DISTINCT
                        intSectionID AS [Id],
                        vcSectionName AS [Data]
                FROM    dbo.DycFormSectionDetail
                WHERE   numFormID = @numFormID			
		
		
		/*SELECT DISTINCT  DMAP.intSectionID AS [Id], 
						 DFSD.vcSectionName AS [Data]					 
		FROM dbo.DycFormField_Mapping DMAP 
		INNER JOIN dbo.DycFormSectionDetail DFSD ON DFSD.intSectionID = DMAP.intSectionID
											   AND DFSD.numFormID = DMAP.numFormID
		WHERE DMAP.numFormID = @numFormID */
		
                SELECT  ROW_NUMBER() OVER ( ORDER BY X.intSectionID, X.numFormFieldID ) AS SrNo,
                        X.*
                FROM    ( SELECT    ISNULL(( SELECT intImportTransactionID
                                             FROM   dbo.Import_File_Field_Mapping FFM
                                                    INNER JOIN Import_File_Master IFM ON IFM.intImportFileID = FFM.intImportFileID
                                                                                         AND IFM.intImportFileID = @numImportFileID
                                             WHERE  FFM.numFormFieldID = DCOL.numFieldID
                                           ), 0) AS [intImportFieldID],
                                    DCOL.numFieldID AS [numFormFieldID],
                                    '' AS [vcSectionNames],
                                    DCOL.vcFieldName AS [vcFormFieldName],
                                    DCOL.vcDbColumnName,
                                    DCOL.intSectionID AS [intSectionID],
                                    0 [IsCustomField],
                                    ISNULL(( SELECT intMapColumnNo
                                             FROM   dbo.Import_File_Field_Mapping FFM
                                             WHERE  1 = 1
                                                    AND FFM.numFormFieldID = DCOL.numFieldID
                                                    AND FFM.intImportFileID = @numImportFileID
                                           ), 0) intMapColumnNo,
                                    DCOL.vcPropertyName,
                                    CASE WHEN ISNULL(DCOL.bitRequired, 0) = 0
                                         THEN ''
                                         ELSE '*'
                                    END AS [vcReqString],
                                    DCOL.bitRequired AS [bitRequired],
                                    CAST(DCOL.numFieldID AS VARCHAR) + '~'
                                    + CAST(0 AS VARCHAR(10)) AS [keyFieldID],
                                    DCOL.tintOrder
                          FROM      dbo.View_DynamicDefaultColumns DCOL
                          WHERE     DCOL.numDomainID = @numDomainID
                                    AND DCOL.numFormID = @numFormID
                                    AND ISNULL(DCOL.bitImport, 0) = 1
                                    AND ISNULL(DCOL.bitDeleted, 0) = 0
                          UNION ALL
                          SELECT    -1,
                                    CFm.Fld_id,
                                    'CustomField',
                                    cfm.Fld_label,
                                    cfm.Fld_label,
                                    DFSD.intSectionId,
                                    1,
                                    ISNULL(( SELECT intMapColumnNo
                                             FROM   dbo.Import_File_Field_Mapping IIFM
                                                    LEFT JOIN dbo.DycFormField_Mapping DMAP ON IIFM.numFormFieldID = DMAP.numFieldID
                                             WHERE  IIFM.intImportFileID = @numImportFileID
                                                    AND IIFM.numFormFieldID = CFM.Fld_id
                                                    AND bitCustomField = 1
                                                    AND ISNULL(DMAP.bitImport, 0) = 1
                                           ), 0) intMapColumnNo,
                                    '' AS [vcPropertyName],
                                    '' AS [vcReqString],
                                    0 AS [bitRequired],
                                    CAST(CFm.Fld_id AS VARCHAR) + '~'
                                    + CAST(1 AS VARCHAR(10)) AS [keyFieldID],
                                    100 AS [tintOrder]
                          FROM      dbo.CFW_Fld_Master CFM
                                    LEFT JOIN dbo.CFW_Loc_Master LOC ON CFM.Grp_id = LOC.Loc_id
                                    LEFT JOIN dbo.DycFormSectionDetail DFSD ON DFSD.Loc_id = LOC.Loc_id
                          WHERE     Grp_id = DFSD.Loc_Id
                                    AND numDomainID = @numDomainID
                                    AND (DFSD.numFormID = @numFormID OR DFSD.numFormID = (CASE WHEN @numFormID = 20 THEN 48 ELSE 20 END))
                        ) X
                ORDER BY X.tintOrder,
                        X.intSectionID        		
        
        
                SELECT  ROW_NUMBER() OVER ( ORDER BY X.intSectionID, X.numFormFieldID ) AS SrNo,
                        X.*
                FROM    ( SELECT    ISNULL(( SELECT intImportTransactionID
                                             FROM   dbo.Import_File_Field_Mapping FFM
                                                    INNER JOIN Import_File_Master IFM ON IFM.intImportFileID = FFM.intImportFileID
                                                                                         AND IFM.intImportFileID = @numImportFileID
                                             WHERE  FFM.numFormFieldID = DCOL.numFieldID
                                           ), 0) AS [intImportFieldID],
                                    DCOL.numFieldID AS [numFormFieldID],
                                    '' AS [vcSectionNames],
                                    DCOL.vcFieldName AS [vcFormFieldName],
                                    DCOL.vcDbColumnName,
                                    DCOL.intSectionID AS [intSectionID],
                                    0 [IsCustomField],
                                    ISNULL(( SELECT intMapColumnNo
                                             FROM   dbo.Import_File_Field_Mapping FFM
                                             WHERE  1 = 1
                                                    AND FFM.numFormFieldID = DCOL.numFieldID
                                                    AND FFM.intImportFileID = @numImportFileID
                                           ), 0) intMapColumnNo,
                                    DCOL.vcPropertyName,
                                    CASE WHEN ISNULL(DCOL.bitRequired, 0) = 0
                                         THEN ''
                                         ELSE '*'
                                    END AS [vcReqString],
                                    DCOL.bitRequired AS [bitRequired],
                                    CAST(DCOL.numFieldID AS VARCHAR) + '~'
                                    + CAST(0 AS VARCHAR(10)) AS [keyFieldID],
                                    ISNULL(DCOL.tintColumn, 0) AS SortOrder,
                                    ISNULL(DCOL.tintRow,0) AS tintRow
                          FROM      dbo.View_DynamicColumns DCOL
                          WHERE     DCOL.numDomainID = @numDomainID
                                    AND DCOL.numFormID = @numFormID
                                    AND ISNULL(DCOL.bitImport, 0) = 1
                                    AND ISNULL(tintPageType, 0) = 4
                          UNION ALL
                          SELECT    -1,
                                    CFm.Fld_id,
                                    'CustomField',
                                    cfm.Fld_label,
                                    cfm.Fld_label,
                                    DFSD.intSectionId,
                                    1,
                                    ISNULL(( SELECT intMapColumnNo
                                             FROM   dbo.Import_File_Field_Mapping IIFM
                                                    LEFT JOIN dbo.DycFormField_Mapping DMAP ON IIFM.numFormFieldID = DMAP.numFieldID
                                             WHERE  IIFM.intImportFileID = @numImportFileID
                                                    AND IIFM.numFormFieldID = CFM.Fld_id
                                                    AND bitCustomField = 1
                                                    AND ISNULL(DMAP.bitImport, 0) = 1
                                           ), 0) intMapColumnNo,
                                    '' AS [vcPropertyName],
                                    '' AS [vcReqString],
                                    0 AS [bitRequired],
                                    CAST(CFm.Fld_id AS VARCHAR) + '~'
                                    + CAST(1 AS VARCHAR(10)) AS [keyFieldID],
                                    DFCD.intColumnNum AS [SortOrder],
                                    ISNULL(DFCD.intRowNum,0) AS tintRow
                          FROM      dbo.CFW_Fld_Master CFM
                                    LEFT JOIN dbo.CFW_Loc_Master LOC ON CFM.Grp_id = LOC.Loc_id
                                    LEFT JOIN dbo.DycFormSectionDetail DFSD ON DFSD.Loc_id = LOC.Loc_id
                                    JOIN DycFormConfigurationDetails DFCD ON DFCD.numFieldID = CFM.Fld_Id
                                                                             AND DFCD.numFormID = @numFormID
                                                                             AND DFCD.bitCustom = 1
                          WHERE     Grp_id = DFSD.Loc_Id
                                    AND DFCD.numDomainID = @numDomainID
                                    AND (DFSD.numFormID = @numFormID OR DFSD.numFormID = (CASE WHEN @numFormID = 20 THEN 48 ELSE 20 END))
                                    AND (DFCD.tintPageType = 4 OR DFCD.tintPageType IS NULL)                          
                        ) X
                ORDER BY X.SortOrder
                        ,X.intSectionID 
            END	
    END

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GET_IMPORT_MAPPED_DATA')
DROP PROCEDURE USP_GET_IMPORT_MAPPED_DATA
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- SELECT * FROM dbo.Import_File_Master
-- USP_GET_IMPORT_MAPPED_DATA 39,1,48
CREATE PROCEDURE [dbo].[USP_GET_IMPORT_MAPPED_DATA] 
(
	@intImportFileID	BIGINT,
	@numDomainID		NUMERIC(18,0),
	@numFormID			NUMERIC(18,0)
)
AS 
BEGIN
	DECLARE @strSQL AS NVARCHAR(MAX)
	DECLARE @strWHERE AS VARCHAR(MAX)
	
	SET @strWHERE = ' WHERE 1=1 '
	
	IF ISNULL(@intImportFileID,0) <> 0
		BEGIN
			SET @strWHERE = @strWHERE + ' AND PARENT.intImportFileID = ' + CONVERT(VARCHAR,@intImportFileID)
		END
		
		SET @strSQL = 'SELECT   PARENT.intImportFileID AS [ImportFileID],
			   					IMPORT.vcFormName AS [ImportType],
								PARENT.vcImportFileName AS [ImportFileName],
								(CASE 
									WHEN PARENT.tintStatus = 0 THEN ''Import Pending ''
									WHEN PARENT.tintStatus = 1 THEN ''Import Completed''
									WHEN PARENT.tintStatus = 2 THEN ''Rollback Pending''	
									WHEN PARENT.tintStatus = 3 THEN ''Rollback Incomplete''	
								END) AS [Status],								
								dbo.FormatedDateTimeFromDate(PARENT.dtCreateDate,numDomainId) AS [CreatedDate],
								dbo.fn_GetContactName(PARENT.numUserContactID) AS [CreatedBy] ,
								ISNULL(
										CASE WHEN ISNULL(tintStatus,0) = 1
											 THEN CASE WHEN ISNULL(numRecordAdded,0) <> 0 AND ISNULL(numRecordUpdated,0) <> 0 AND ISNULL(numErrors,0) = 0
													   THEN CAST(numRecordAdded AS VARCHAR) + '' records added. '' + CAST(numRecordUpdated AS VARCHAR)+ '' records updated.''	
													   WHEN ISNULL(numRecordAdded,0) <> 0 AND ISNULL(numRecordUpdated,0) = 0 AND ISNULL(numErrors,0) = 0
													   THEN CAST(numRecordAdded AS VARCHAR)+ '' records added.''	
													   WHEN ISNULL(numRecordAdded,0) = 0 AND ISNULL(numRecordUpdated,0) <> 0 AND ISNULL(numErrors,0) = 0
													   THEN CAST(numRecordUpdated AS VARCHAR)+ '' records updated.''	
													   WHEN ISNULL(numRecordAdded,0) = 0 AND ISNULL(numRecordUpdated,0) = 0 AND ISNULL(numErrors,0) = 0
													   THEN '' Wrong Data exists.''	
													   ELSE	CAST(numErrors AS VARCHAR) + '' errors occurred while importing data.''
												  END
											 END	  	   
								,''-'') AS [Records],
								PARENT.tintStatus AS [intStatus],
								PARENT.numMasterID AS [MasterID],
								ISNULL(HISTORY.vcHistory_Added_Value,'''') AS [ItemCodes],
								ISNULL(CASE WHEN (ISNULL(tintStatus,0) = 1 OR ISNULL(tintStatus,0) = 3)
											THEN 
											     CASE WHEN ISNULL(numRecordAdded,0) > 0 AND ISNULL(numRecordUpdated,0) = 0 THEN 1 
											          WHEN ISNULL(numErrors,0) >= 0  AND ISNULL(numRecordUpdated,0) = 0 THEN 1 
											          ELSE 0
												 END
									   END,0)	AS [IsForRollback]  	   
						FROM dbo.Import_File_Master PARENT 
						INNER JOIN dbo.DynamicFormMaster IMPORT ON PARENT.numMasterID = IMPORT.numFormId
						LEFT JOIN dbo.Import_History HISTORY ON HISTORY.intImportFileID = PARENT.intImportFileID '
						+ @strWHERE + ' AND PARENT.numDomainId = ' + CONVERT(VARCHAR,@numDomainID) +
						' ORDER BY PARENT.intImportFileID DESC'
		PRINT @strSQL
		EXEC SP_EXECUTESQL @strSQL
			
		-------------------------------------------------------------------------------------------------------------
		
		SELECT * FROM 
		(SELECT DISTINCT PARENT.numFieldID AS [FormFieldID],
						IFFM.intMapColumnNo AS [ImportFieldID],
						IFFM.intImportFileID,
						IFFM.intImportTransactionID,
						CASE WHEN ISNULL(bitCustomField,0) = 1
							 THEN CFW.Fld_label
							 ELSE PARENT.vcDbColumnName
						END AS [dbFieldName],	 
						CASE WHEN ISNULL(bitCustomField,0) = 1
							 THEN CFW.Fld_label
							 ELSE PARENT.vcFieldName 
						END AS [FormFieldName],	 				
						CASE WHEN CFW.fld_Type = 'Drop Down List Box'
							 THEN 'SelectBox'
							 ELSE ISNULL(PARENT.vcAssociatedControlType,'') 
						END	 vcAssociatedControlType,
						ISNULL(PARENT.bitDefault,0) bitDefault,
						ISNULL(PARENT.vcFieldType,'') vcFieldType,
						ISNULL(PARENT.vcListItemType,'') vcListItemType,
						ISNULL(PARENT.vcLookBackTableName,'') vcLookBackTableName,
						CASE WHEN ISNULL(bitCustomField,0) = 1
							 THEN CFW.Fld_label
							 ELSE PARENT.vcOrigDbColumnName
						END AS [vcOrigDbColumnName],
						ISNULL(PARENT.vcPropertyName,'') vcPropertyName,
						ISNULL(bitCustomField,0) AS [bitCustomField],
						PARENT.numDomainID AS [DomainID]
		FROM dbo.View_DynamicDefaultColumns PARENT  
		LEFT JOIN dbo.Import_File_Field_Mapping IFFM ON PARENT.numFieldID = IFFM.numFormFieldID    		
		LEFT JOIN dbo.CFW_Fld_Master CFW ON CFW.numDomainID = PARENT.numDomainID
		WHERE IFFM.intImportFileID = @intImportFileID
		  AND PARENT.numDomainID = @numDomainID
		  AND numFormID = @numFormID
	
		UNION ALL
		
		SELECT DISTINCT IFFM.numFormFieldID AS [FormFieldID],
						IFFM.intMapColumnNo AS [ImportFieldID],
						IFFM.intImportFileID,
						IFFM.intImportTransactionID,
						CFW.Fld_label AS [dbFieldName],	 
						CFW.Fld_label AS [FormFieldName],	 				
						CASE WHEN CFW.fld_Type = 'Drop Down List Box'
							 THEN 'SelectBox'
							 ELSE CFW.fld_Type
						END	 vcAssociatedControlType,
						0 AS bitDefault,
						'R' AS vcFieldType,
						'' AS vcListItemType,
						'' AS  vcLookBackTableName,
						CFW.Fld_label AS [vcOrigDbColumnName],
						'' AS vcPropertyName,
						1 AS [bitCustomField],
						0
						--IFM.numDomainID AS [DomainID]
		FROM dbo.Import_File_Field_Mapping IFFM 
		INNER JOIN dbo.CFW_Fld_Master CFW ON IFFM.numFormFieldID = CFW.Fld_id 
		INNER JOIN dbo.CFW_Loc_Master LOC ON CFW.Grp_id = LOC.Loc_id										 
		INNER JOIN dbo.DycFormSectionDetail DFSD ON DFSD.Loc_id = LOC.Loc_id
		WHERE 1=1
		  --AND DFSD.numDomainID = CFW.numDomainID
		  AND IFFM.intImportFileID = @intImportFileID
		  AND (DFSD.numFormID = @numFormID OR DFSD.numFormID = (CASE WHEN @numFormID = 20 THEN 48 ELSE 20 END))
		  AND Grp_id = DFSD.Loc_Id ) TABLE1
		ORDER BY intImportFileID DESC 
		
	/*
	SELECT * FROM Import_File_Field_Mapping		
	SELECT * FROM Import_File_Master
	SELECT * FROM View_DynamicDefaultColumns	
	SELECT * FROM CFW_Fld_Master
	*/
	/*
		SELECT DISTINCT PARENT.numFieldID AS [FormFieldID],
						IFFM.intMapColumnNo AS [ImportFieldID],
						IFFM.intImportFileID,
						IFFM.intImportTransactionID,
						CASE WHEN ISNULL(bitCustomField,0) = 1
							 THEN CFW.Fld_label
							 ELSE PARENT.vcDbColumnName
						END AS [dbFieldName],	 
						CASE WHEN ISNULL(bitCustomField,0) = 1
							 THEN CFW.Fld_label
							 ELSE PARENT.vcFieldName 
						END AS [FormFieldName],	 				
						CASE WHEN CFW.fld_Type = 'Drop Down List Box'
							 THEN 'SelectBox'
							 ELSE ISNULL(PARENT.vcAssociatedControlType,'') 
						END	 vcAssociatedControlType,
						ISNULL(PARENT.bitDefault,0) bitDefault,
						ISNULL(PARENT.vcFieldType,'') vcFieldType,
						ISNULL(PARENT.vcListItemType,'') vcListItemType,
						ISNULL(PARENT.vcLookBackTableName,'') vcLookBackTableName,
						CASE WHEN ISNULL(bitCustomField,0) = 1
							 THEN CFW.Fld_label
							 ELSE PARENT.vcOrigDbColumnName
						END AS [vcOrigDbColumnName],
						ISNULL(PARENT.vcPropertyName,'') vcPropertyName,
						ISNULL(bitCustomField,0) AS [bitCustomField],
						PARENT.numDomainID AS [DomainID] 
		FROM View_DynamicDefaultColumns PARENT
		LEFT JOIN dbo.Import_File_Field_Mapping IFFM ON PARENT.numFieldID = IFFM.numFormFieldID    		
		LEFT JOIN dbo.CFW_Fld_Master CFW ON CFW.Grp_id = PARENT.numFormId AND CFW.numDomainID = PARENT.numDomainID
		WHERE numFormID = 48 AND PARENT.numDomainID = 1 AND IFFM.intImportFileID = 2 */	
		
--		SELECT  CHILD.numFormFieldID AS [FormFieldID],
--				CHILD.intMapColumnNo AS [ImportFieldID],
--				CHILD.intImportFileID,
--				CHILD.intImportTransactionID,
--				CASE WHEN ISNULL(bitCustomField,0) = 1
--					 THEN CFW.Fld_label
--					 ELSE DYNAMIC_FIELD.vcDbColumnName
--				END AS [dbFieldName],	 
--				--DYNAMIC_FIELD.vcDbColumnName AS [dbFieldName],
--				CASE WHEN ISNULL(bitCustomField,0) = 1
--					 THEN CFW.Fld_label
--					 ELSE DYNAMIC_FIELD.vcFormFieldName
--				END AS [FormFieldName],	 
--				--DYNAMIC_FIELD.vcFormFieldName AS [FormFieldName],
--				CASE WHEN CFW.fld_Type = 'Drop Down List Box'
--					 THEN 'SelectBox'
--					 ELSE ISNULL(DYNAMIC_FIELD.vcAssociatedControlType,'') 
--				END	 vcAssociatedControlType,
--				ISNULL(DYNAMIC_FIELD.bitDefault,0) bitDefault,
--				ISNULL(DYNAMIC_FIELD.vcFieldType,'') vcFieldType,
--				ISNULL(DYNAMIC_FIELD.vcListItemType,'') vcListItemType,
--				ISNULL(DYNAMIC_FIELD.vcLookBackTableName,'') vcLookBackTableName,
--				CASE WHEN ISNULL(bitCustomField,0) = 1
--					 THEN CFW.Fld_label
--					 ELSE DYNAMIC_FIELD.vcOrigDbColumnName
--				END AS [vcOrigDbColumnName],
--				ISNULL(DYNAMIC_FIELD.vcPropertyName,'') vcPropertyName,
--				ISNULL(bitCustomField,0) AS [bitCustomField],
--			    PARENT.numDomainID AS [DomainID]
--		FROM Import_File_Field_Mapping CHILD 
--		INNER JOIN dbo.Import_File_Master PARENT ON PARENT.intImportFileID = CHILD.intImportFileID
--		INNER JOIN dbo.Import_Master IMPORT ON IMPORT.intImportMasterID = PARENT.numMasterID
--		LEFT JOIN DynamicFormFieldMaster DYNAMIC_FIELD ON CHILD.numFormFieldID = DYNAMIC_FIELD.numFormFieldId 
--		LEFT JOIN dbo.DycFormField_Mapping DYNAMIC_FIELD_CHILD ON CHILD.numFormFieldID = DYNAMIC_FIELD_CHILD.numFieldId 
--		INNER JOIN dbo.DycFieldMaster DYNAMIC_FIELD ON DYNAMIC_FIELD_CHILD.= DYNAMIC_FIELD
--		LEFT JOIN dbo.CFW_Fld_Master CFW ON CHILD.numFormFieldID = CFW.Fld_id
--		WHERE CHILD.intImportFileID = @intImportFileID

END		
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_GetBillDetails' ) 
    DROP PROCEDURE USP_GetBillDetails
GO
-- exec USP_GetBillPaymentDetails 3,1,0
CREATE PROCEDURE [dbo].[USP_GetBillDetails]
    @numBillID NUMERIC(18, 0),
    @numDomainID NUMERIC
AS 
    SET NOCOUNT ON
    SET TRANSACTION ISOLATION LEVEL READ COMMITTED
    
	DECLARE @numCurrencyID NUMERIC(18,0)
	SELECT @numCurrencyID=numCurrencyID FROM Domain WHERE numDomainID = @numDomainID

    SELECT  BH.[numBillID],
            [numDivisionID],
            dbo.fn_GetComapnyName(numDivisionID) AS [vcCompanyName],
            BH.[numAccountID],
            BH.[dtBillDate],
            BH.[numTermsID],
            BH.[monAmountDue],
            BH.[dtDueDate],
            BH.[vcMemo],
            BH.[vcReference],
            BH.[monAmtPaid],
            BH.[bitIsPaid],
            BH.[numDomainID],
            BH.[numCreatedBy],
            BH.[dtCreatedDate],
            BH.[numModifiedBy],
            BH.[dtModifiedDate],ISNULL(GJD.numTransactionId,0) AS numTransactionId,
            ISNULL(PO.numProId,0) AS numProjectID,ISNULL(PO.numStageID,0) AS numStageID,
            (SELECT vcTerms FROM BillingTerms WHERE numTermsID = BH.[numTermsID] AND numDomainId = BH.numDomainID)  AS [vcTermName],
			ISNULL(BH.numCurrencyID,@numCurrencyID) numCurrencyID,
			ISNULL(BH.fltExchangeRate,1) fltExchangeRate
    FROM    [dbo].[BillHeader] BH
            LEFT OUTER JOIN General_Journal_Details GJD ON GJD.tintReferenceType=3 
						AND GJD.numReferenceID=BH.numBillID
			LEFT OUTER JOIN ProjectsOpportunities PO ON PO.numBillId=BH.numBillId			
    WHERE   BH.[numBillID] = @numBillID
            AND BH.numDomainID = @numDomainID
    
    
    SELECT  BD.[numBillDetailID],
            BD.[numExpenseAccountID],
            BD.[monAmount],
            BD.[vcDescription],
            BD.[numProjectID],
            BD.[numClassID],
            BD.[numCampaignID],
            BD.numBillID
			,ISNULL(GJD.numTransactionId,0) AS numTransactionId
    FROM    dbo.BillDetails BD
            INNER JOIN BillHeader BH ON BD.numBillID = BH.numBillID
           	LEFT OUTER JOIN General_Journal_Details GJD ON GJD.tintReferenceType=4 AND GJD.numReferenceID=BD.numBillDetailID
    WHERE   BH.[numBillID] = @numBillID
            AND BH.numDomainID = @numDomainID
            
GO


/****** Object:  StoredProcedure [dbo].[USP_GetCaseList1]    Script Date: 07/26/2008 16:16:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--  By Anoop Jayaraj                                                            
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getcaselist1')
DROP PROCEDURE usp_getcaselist1
GO
CREATE PROCEDURE [dbo].[USP_GetCaseList1]                                                               
	 @numUserCntId numeric(9)=0,                                            
	 @tintSortOrder numeric=0,                                                                                
	 @numDomainID numeric(9)=0,                                                            
	 @tintUserRightType tinyint,                                                            
	 @SortChar char(1)='0' ,                                                                                                                   
	 @CurrentPage int,                                                            
	 @PageSize int,                                                            
	 @TotRecs int output,                                                            
	 @columnName as Varchar(50),                                                            
	 @columnSortOrder as Varchar(10),                                                          
	 @numDivisionID as numeric(9)=0,                              
	 @bitPartner as bit=0,
	 @vcRegularSearchCriteria varchar(1000)='',
	 @vcCustomSearchCriteria varchar(1000)='' ,
	 @ClientTimeZoneOffset as INT,                                                          
	 @numStatus AS BIGINT = 0,
	 @SearchText VARCHAR(MAX) = ''
AS        
	
	CREATE TABLE #tempForm (tintOrder TINYINT,vcDbColumnName nvarchar(50),vcOrigDbColumnName nvarchar(50),vcFieldName nvarchar(50),vcAssociatedControlType nvarchar(50),
  vcListItemType varchar(3),numListID numeric(9),vcLookBackTableName varchar(50),bitCustomField BIT,
  numFieldId NUMERIC,bitAllowSorting BIT,bitAllowEdit BIT,bitIsRequired BIT,bitIsEmail BIT,bitIsAlphaNumeric BIT,bitIsNumeric BIT,
  bitIsLengthValidation BIT,intMaxLength int,intMinLength int,bitFieldMessage BIT,vcFieldMessage VARCHAR(500),ListRelID numeric(9),intColumnWidth int,bitAllowFiltering BIT,vcFieldDataType CHAR(1))


	DECLARE @Nocolumns AS TINYINT                
	SET @Nocolumns=0                
 
	SELECT @Nocolumns=isnull(sum(TotalRow),0)  FROM(            
	SELECT COUNT(*) TotalRow from View_DynamicColumns where numFormId=12 and numUserCntID=@numUserCntID and numDomainID=@numDomainID AND tintPageType=1
	UNION 
	SELECT count(*) TotalRow from View_DynamicCustomColumns where numFormId=12 and numUserCntID=@numUserCntID and numDomainID=@numDomainID AND tintPageType=1) TotalRows

	--Check if Master Form Configuration is created by administrator if NoColumns=0
	DECLARE @IsMasterConfAvailable BIT = 0
	DECLARE @numUserGroup NUMERIC(18,0)

	IF @Nocolumns=0
	BEGIN
		SELECT @numUserGroup = numGroupID FROM UserMaster WHERE numUserDetailId = @numUserCntID 

		IF (SELECT COUNT(*) FROM BizFormWizardMasterConfiguration WHERE numDomainID = @numDomainID AND numFormID = 12 AND bitGridConfiguration = 1 AND numGroupID = @numUserGroup) > 0
		BEGIN
			SET @IsMasterConfAvailable = 1
		END
	END


	--If MasterConfiguration is available then load it otherwise load default columns
	IF @IsMasterConfAvailable = 1
	BEGIN
	INSERT INTO #tempForm
	SELECT 
		(intRowNum + 1) as tintOrder, vcDbColumnName,	vcOrigDbColumnName,	ISNULL(vcCultureFieldName,vcFieldName), vcAssociatedControlType,
		vcListItemType, numListID, vcLookBackTableName,	bitCustom, numFieldId, bitAllowSorting,	bitInlineEdit, bitIsRequired,
		bitIsEmail,	bitIsAlphaNumeric, bitIsNumeric, bitIsLengthValidation,	intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage vcFieldMessage,
		ListRelID,intColumnWidth,bitAllowFiltering,vcFieldDataType 
	FROM 
		View_DynamicColumnsMasterConfig
	WHERE
		View_DynamicColumnsMasterConfig.numFormId=12 AND 
		View_DynamicColumnsMasterConfig.numDomainID=@numDomainID AND 
		View_DynamicColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
		View_DynamicColumnsMasterConfig.bitGridConfiguration = 1 AND
		ISNULL(View_DynamicColumnsMasterConfig.bitSettingField,0)=1 AND 
		ISNULL(View_DynamicColumnsMasterConfig.bitCustom,0)=0
	UNION
    SELECT 
		tintRow+1 as tintOrder,vcDbColumnName,vcDbColumnName,vcFieldName, vcAssociatedControlType,'' as vcListItemType,numListID,'',
		bitCustom,numFieldId,bitAllowSorting,bitAllowEdit,bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
		intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage,ListRelID,intColumnWidth,0,''
	FROM 
		View_DynamicCustomColumnsMasterConfig
	WHERE 
		View_DynamicCustomColumnsMasterConfig.numFormId=12 AND 
		View_DynamicCustomColumnsMasterConfig.numDomainID=@numDomainID AND 
		View_DynamicCustomColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
		View_DynamicCustomColumnsMasterConfig.bitGridConfiguration = 1 AND
		ISNULL(View_DynamicCustomColumnsMasterConfig.bitCustom,0)=1
	ORDER BY 
		tintOrder ASC  
	END
	ELSE                                        
	BEGIN
	if @Nocolumns=0
	BEGIN
		INSERT INTO DycFormConfigurationDetails (numFormID,numFieldID,intColumnNum,intRowNum,numDomainID,numUserCntID,numRelCntType,tintPageType,bitCustom,intColumnWidth)
		select 12,numFieldId,0,Row_number() over(order by tintRow desc),@numDomainID,@numUserCntID,NULL,1,0,intColumnWidth
		 FROM View_DynamicDefaultColumns
		 where numFormId=12 and bitDefault=1 AND ISNULL(bitSettingField,0)=1 and numDomainID=@numDomainID
		order by tintOrder asc      
	END

	INSERT INTO #tempForm
	select tintRow+1 as tintOrder,vcDbColumnName,vcOrigDbColumnName,ISNULL(vcCultureFieldName,vcFieldName),                  
	 vcAssociatedControlType,vcListItemType,numListID,vcLookBackTableName                                                 
	,bitCustom,numFieldId,bitAllowSorting,bitInlineEdit,
	bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
	intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage vcFieldMessage,ListRelID,intColumnWidth,bitAllowFiltering,vcFieldDataType  
	 FROM View_DynamicColumns 
	 where numFormId=12 and numUserCntID=@numUserCntID and numDomainID=@numDomainID AND tintPageType=1 
	  AND ISNULL(bitSettingField,0)=1 AND ISNULL(bitCustom,0)=0
       
		   UNION
    
		   select tintRow+1 as tintOrder,vcDbColumnName,vcDbColumnName,vcFieldName,                  
	 vcAssociatedControlType,'' as vcListItemType,numListID,''                                                 
	,bitCustom,numFieldId,bitAllowSorting,bitAllowEdit,
	bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
	intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage,ListRelID,intColumnWidth,0,''
	 from View_DynamicCustomColumns
	where numFormId=12 and numUserCntID=@numUserCntID and numDomainID=@numDomainID AND tintPageType=1 
	 AND ISNULL(bitCustom,0)=1
 
	  order by tintOrder asc  
  
END     

	DECLARE  @strColumns AS VARCHAR(MAX)

	SET @strColumns=' ADC.numContactId,DM.numDivisionID,isnull(DM.numTerID,0) numTerID,cs.numRecOwner,ISNULL(cs.numAssignedTo,0) AS numAssignedTo,DM.tintCRMType,cs.numCaseID,cs.vcCaseNumber'                

	DECLARE @tintOrder as tinyint                                                  
	DECLARE @vcFieldName as varchar(50)                                                  
	DECLARE @vcListItemType as varchar(3)                                             
	DECLARE @vcListItemType1 as varchar(1)                                                 
	DECLARE @vcAssociatedControlType varchar(20)                                                  
	DECLARE @numListID AS numeric(9)                                                  
	DECLARE @vcDbColumnName varchar(20)                      
	DECLARE @WhereCondition varchar(2000)                       
	DECLARE @vcLookBackTableName varchar(2000)                
	DECLARE @bitCustom as bit          
	DECLARE @numFieldId as numeric  
	DECLARE @bitAllowSorting AS CHAR(1)   
	DECLARE @bitAllowEdit AS CHAR(1)                 
	DECLARE @vcColumnName AS VARCHAR(500)                  
    DECLARE @SearchQuery AS VARCHAR(MAX) = ''            
	SET @tintOrder=0                                                  
	SET @WhereCondition =''    
	--set @DefaultNocolumns=  @Nocolumns              
	Declare @ListRelID as numeric(9) 

	  SELECT TOP 1 
			@tintOrder=tintOrder+1,@vcDbColumnName=vcDbColumnName,@vcFieldName=vcFieldName,                
			@vcAssociatedControlType=vcAssociatedControlType,@vcListItemType=vcListItemType,@numListID=numListID,@vcLookBackTableName=vcLookBackTableName,                                               
			@bitCustom=bitCustomField,@numFieldId=numFieldId,@bitAllowSorting=bitAllowSorting,@bitAllowEdit=bitAllowEdit,@ListRelID=ListRelID
	  FROM  
			#tempForm 
	  ORDER BY 
			tintOrder 
	  ASC            
                                        
WHILE @tintOrder>0                                
BEGIN                                                  
 if @bitCustom = 0        
 begin        
    declare @Prefix as varchar(5)                
      if @vcLookBackTableName = 'AdditionalContactsInformation'                
    set @Prefix = 'ADC.'                
      if @vcLookBackTableName = 'DivisionMaster'                
    set @Prefix = 'DM.'                
      if @vcLookBackTableName = 'Cases'                
    set @PreFix ='Cs.'      
    
    SET @vcColumnName= @vcDbColumnName+'~'+ convert(varchar(10),@numFieldId) +'~0'
    
     if @vcAssociatedControlType='SelectBox'                                                  
     begin                                                  
                                     
     if @vcListItemType='LI'                                                   
     begin                                                  
      set @strColumns=@strColumns+',L'+ convert(varchar(3),@tintOrder)+'.vcData'+' ['+ @vcColumnName+']'
	    IF LEN(@SearchText) > 0
		  BEGIN 
			SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + 'L' + CONVERT(VARCHAR(3),@tintOrder) + '.vcData LIKE ''%' + @SearchText + '%'' '     
		  END                                                     
      set @WhereCondition= @WhereCondition +' left Join ListDetails L'+ convert(varchar(3),@tintOrder)+ ' on L'+convert(varchar(3),@tintOrder)+ '.numListItemID='+@vcDbColumnName                                                  
     end                                                  
     else if @vcListItemType='S'                                                   
     begin                                                  
      set @strColumns=@strColumns+',S'+ convert(varchar(3),@tintOrder)+'.vcState'+' ['+ @vcColumnName+']'     
	    IF LEN(@SearchText) > 0
		  BEGIN 
			SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + 'S' + CONVERT(VARCHAR(3),@tintOrder) + '.vcState LIKE ''%' + @SearchText + '%'' '     
		  END                                                
      set @WhereCondition= @WhereCondition +' left join State S'+ convert(varchar(3),@tintOrder)+ ' on S'+convert(varchar(3),@tintOrder)+ '.numStateID='+@vcDbColumnName                                                  
     end                                                  
     else if @vcListItemType='T'                                                   
     begin                                                  
      set @strColumns=@strColumns+',L'+ convert(varchar(3),@tintOrder)+'.vcData'+' ['+ @vcColumnName+']' 
	   IF LEN(@SearchText) > 0
		  BEGIN
			SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + 'L' + CONVERT(VARCHAR(3),@tintOrder) + '.vcData LIKE ''%' + @SearchText + '%'' '
		  END                                                    
      set @WhereCondition= @WhereCondition +' left Join ListDetails L'+ convert(varchar(3),@tintOrder)+ ' on L'+convert(varchar(3),@tintOrder)+ '.numListItemID='+@vcDbColumnName                                                  
     end                 
    ELSE IF   @vcListItemType='U'                                               
		BEGIN        
			SET @strColumns=@strColumns+',dbo.fn_GetContactName('+@Prefix+@vcDbColumnName+') ['+ @vcColumnName+']'
			  IF LEN(@SearchText) > 0
		  BEGIN
			SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + 'dbo.fn_GetContactName(' + @Prefix + @vcDbColumnName + ') LIKE ''%' + @SearchText + '%'' '
		  END                                                     
		END                
    END           
    ELSE IF @vcAssociatedControlType='DateField'                                                  
   begin            
		IF @vcDbColumnName='intTargetResolveDate'
		BEGIN
			 set @strColumns=@strColumns+',case when convert(varchar(11),'+@Prefix+@vcDbColumnName+')= convert(varchar(11),getdate()) then ''<b><font color=red>Today</font></b>'''      
			 set @strColumns=@strColumns+'when convert(varchar(11),'+@Prefix+@vcDbColumnName+')= convert(varchar(11),dateadd(day,-1,getdate())) then ''<b><font color=purple>YesterDay</font></b>'''      
			 set @strColumns=@strColumns+'when convert(varchar(11),'+@Prefix+@vcDbColumnName+')= convert(varchar(11),dateadd(day,1,getdate())) then''<b><font color=orange>Tommorow</font></b>'' '      
			 set @strColumns=@strColumns+'else dbo.FormatedDateFromDate('+@Prefix+@vcDbColumnName+','+convert(varchar(10),@numDomainId)+') end  ['+ @vcColumnName+']'               
			IF LEN(@SearchText) > 0
			BEGIN
			SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + @Prefix + @vcDbColumnName + ' LIKE ''%' + @SearchText + '%'' '
			END
		END
	ELSE
	BEGIN
     set @strColumns=@strColumns+',case when convert(varchar(11),DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+',' +@Prefix+@vcDbColumnName+') )= convert(varchar(11),getdate()) then ''<b><font color=red>Today</font></b>'''      
     set @strColumns=@strColumns+'when convert(varchar(11),DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+',' +@Prefix+@vcDbColumnName+') )= convert(varchar(11),dateadd(day,-1,getdate())) then ''<b><font color=purple>YesterDay</font></b>'''      
     set @strColumns=@strColumns+'when convert(varchar(11),DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+',' +@Prefix+@vcDbColumnName+') )= convert(varchar(11),dateadd(day,1,getdate())) then''<b><font color=orange>Tommorow</font></b>'' ' 
     set @strColumns=@strColumns+'else dbo.FormatedDateFromDate(DateAdd(minute, '+convert(varchar(15),-@ClientTimeZoneOffset)+',' +@Prefix+@vcDbColumnName+'),'+convert(varchar(10),@numDomainId)+') end  ['+ @vcColumnName+']'
	 IF LEN(@SearchText) > 0
		BEGIN
			SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + @Prefix + @vcDbColumnName + ' LIKE ''%' + @SearchText + '%'' '
		END
	END
   -- end      
--  else      
--     set @strSql=@strSql+',dbo.FormatedDateFromDate('+@vcDbColumnName+','++convert(varchar(10),@numDomainId)+') ['+ @vcFieldName+'~'+ @vcDbColumnName+']'            
   END          
    ELSE IF @vcAssociatedControlType='TextBox'                                                  
	   BEGIN           
		 SET @strColumns=@strColumns+','+ case                
		WHEN @vcDbColumnName='numAge' then 'year(getutcdate())-year(bintDOB)'               
		WHEN @vcDbColumnName='numDivisionID' then 'DM.numDivisionID'               
		WHEN @vcDbColumnName='textSubject' then 'convert(varchar(max),textSubject)' 
		WHEN @vcDbColumnName='textDesc' then    'convert(varchar(max),textDesc)'           
		WHEN @vcDbColumnName='textInternalComments' then 'convert(varchar(max),textInternalComments)'
		ELSE @vcDbColumnName end+' ['+ @vcColumnName+']'     
		IF LEN(@SearchText) > 0
		 BEGIN
			SET @SearchQuery = @SearchQuery +  (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + (case                
			WHEN @vcDbColumnName='numAge' then 'year(getutcdate())-year(bintDOB)'               
			WHEN @vcDbColumnName='numDivisionID' then 'DM.numDivisionID'               
			WHEN @vcDbColumnName='textSubject' then 'convert(varchar(max),textSubject)' 
			WHEN @vcDbColumnName='textDesc' then    'convert(varchar(max),textDesc)'           
			WHEN @vcDbColumnName='textInternalComments' then 'convert(varchar(max),textInternalComments)'
			ELSE @vcDbColumnName end) + ' LIKE ''%' + @SearchText + '%'' '
		 END       
	   END  
  ELSE IF  @vcAssociatedControlType='TextArea'                                              
  begin  
       set @strColumns=@strColumns+', '+ @vcDbColumnName +' ['+ @vcColumnName+']'            
   end 
    	else if @vcAssociatedControlType='Popup' and @vcDbColumnName='numShareWith'                                                   
		begin      
			 set @strColumns=@strColumns+',(SELECT SUBSTRING(
	(SELECT '','' + A.vcFirstName+'' ''+A.vcLastName + CASE WHEN ISNULL(SR.numContactType,0)>0 THEN ''('' + dbo.fn_GetListItemName(SR.numContactType) + '')'' ELSE '''' END
	FROM ShareRecord SR JOIN UserMaster UM ON UM.numUserDetailId=SR.numAssignedTo 
							 JOIN AdditionalContactsInformation A ON UM.numUserDetailId=A.numContactID 
		 WHERE SR.numDomainID=Cs.numDomainID AND SR.numModuleID=7 AND SR.numRecordID=Cs.numCaseID
				AND UM.numDomainID=Cs.numDomainID and UM.numDomainID=A.numDomainID FOR XML PATH('''')),2,200000)) ['+ @vcColumnName+']'                
		 end     
 end                                            
 Else                                                    
 Begin        
   SET @vcColumnName= @vcDbColumnName +'~'+ convert(varchar(10),@numFieldId)+'~1'
       
        
    if @vcAssociatedControlType = 'TextBox' or @vcAssociatedControlType = 'TextArea'          
   begin         
           
    set @strColumns= @strColumns+',CFW'+ convert(varchar(3),@tintOrder)+'.Fld_Value  ['+ @vcColumnName + ']'           
    set @WhereCondition= @WhereCondition +' left Join CFW_FLD_Values_Case CFW'+ convert(varchar(3),@tintOrder)+ '         
    on CFW'+ convert(varchar(3),@tintOrder)+ '.Fld_Id='+convert(varchar(10),@numFieldId) +'and CFW'+ convert(varchar(3),@tintOrder)+ '.RecId=cs.numCaseId   '                                                 
   end        
           
   else if @vcAssociatedControlType = 'CheckBox'          
   begin          
             
    set @strColumns= @strColumns+',case when isnull(CFW'+ convert(varchar(3),@tintOrder)+'.Fld_Value,0)=0 then ''No'' when isnull(CFW'+ convert(varchar(3),@tintOrder)+'.Fld_Value,0)=1 then ''Yes'' end   ['+ @vcColumnName + ']'             
    set @WhereCondition= @WhereCondition +' left Join CFW_FLD_Values_Case CFW'+ convert(varchar(3),@tintOrder)+ '           
    on CFW'+ convert(varchar(3),@tintOrder)+ '.Fld_Id='+convert(varchar(10),@numFieldId) +'and CFW'+ convert(varchar(3),@tintOrder)+ '.RecId=cs.numCaseId   '                                                   
   end          
   else if @vcAssociatedControlType = 'DateField'           
   begin        
           
    set @strColumns= @strColumns+',dbo.FormatedDateFromDate(CFW'+ convert(varchar(3),@tintOrder)+'.Fld_Value,'+convert(varchar(10),@numDomainId)+')  ['+ @vcColumnName + ']'           
    set @WhereCondition= @WhereCondition +' left Join CFW_FLD_Values_Case CFW'+ convert(varchar(3),@tintOrder)+ '         
    on CFW'+ convert(varchar(3),@tintOrder)+ '.Fld_Id='+convert(varchar(10),@numFieldId) +'and CFW'+ convert(varchar(3),@tintOrder)+ '.RecId=cs.numCaseId   '                                                 
   end        
   else if @vcAssociatedControlType = 'SelectBox'            
   begin          
    set @vcDbColumnName = 'DCust'+Convert(varchar(10),@numFieldId)        
    set @strColumns=@strColumns+',L'+ convert(varchar(3),@tintOrder)+'.vcData'+' ['+ @vcColumnName + ']'                                                  
    set @WhereCondition= @WhereCondition +' left Join CFW_FLD_Values_Case CFW'+ convert(varchar(3),@tintOrder)+ '         
     on CFW'+ convert(varchar(3),@tintOrder)+ '.Fld_Id='+convert(varchar(10),@numFieldId) +'and CFW'+ convert(varchar(3),@tintOrder)+ '.RecId=cs.numCaseId   '                                                 
    set @WhereCondition= @WhereCondition +' left Join ListDetails L'+ convert(varchar(3),@tintOrder)+ ' on L'+convert(varchar(3),@tintOrder)+ '.numListItemID=CFW'+ convert(varchar(3),@tintOrder)+'.Fld_Value'        
   end           
 End        
                                                
   select top 1 @tintOrder=tintOrder+1,@vcDbColumnName=vcDbColumnName,@vcFieldName=vcFieldName,                
  @vcAssociatedControlType=vcAssociatedControlType,@vcListItemType=vcListItemType,@numListID=numListID,@vcLookBackTableName=vcLookBackTableName                                               
,@bitCustom=bitCustomField,@numFieldId=numFieldId,@bitAllowSorting=bitAllowSorting,@bitAllowEdit=bitAllowEdit,@ListRelID=ListRelID
  from #tempForm WHERE tintOrder > @tintOrder-1 order by tintOrder asc            
 
  if @@rowcount=0 set @tintOrder=0 
           
end    

	DECLARE @strShareRedordWith AS VARCHAR(300);
	SET @strShareRedordWith=' Cs.numCaseID in (select SR.numRecordID FROM ShareRecord SR WHERE SR.numDomainID='+ convert(varchar(15),@numDomainID )+' AND SR.numModuleID=7 AND SR.numAssignedTo='+convert(varchar(15),@numUserCntId)+') '
                                                              
	DECLARE @StrSql AS VARCHAR(MAX) = ''
	                                                        
	 set @StrSql=@StrSql+'  FROM AdditionalContactsInformation ADC                                                             
	 JOIN Cases  Cs                                                              
	   ON ADC.numContactId =Cs.numContactId                                                             
	 JOIN DivisionMaster DM                                                              
	  ON ADC.numDivisionId = DM.numDivisionID                                                             
	  JOIN CompanyInfo CMP                                                             
	 ON DM.numCompanyID = CMP.numCompanyId                                                             
	 left join listdetails lst                                                            
	 on lst.numListItemID= cs.numPriority
	 left join listdetails lst1                                                            
	 on lst1.numListItemID= cs.numStatus                                                           
	 left join AdditionalContactsInformation ADC1                          
	 on Cs.numAssignedTo=ADC1.numContactId                                 
	  ' + ISNULL(@WhereCondition,'')  
	 
	IF @bitPartner=1 
		SET @strSql=@strSql+' left join CaseContacts CCont on CCont.numCaseID=Cs.numCaseID and CCont.bitPartner=1 and CCont.numContactId='+convert(varchar(15),@numUserCntID)+ ''                              

	if @columnName like 'CFW.Cust%'         
	begin        
		set @columnName='CFW.Fld_Value'        
		set @StrSql= @StrSql + ' left Join CFW_FLD_Values_Case CFW on CFW.RecId=cs.numCaseId  and CFW.fld_id= '+replace(@columnName,'CFW.Cust','')
	end                                 
	if @columnName like 'DCust%'        
	begin        
		set @columnName='LstCF.vcData'                                                  
		set @StrSql= @StrSql + ' left Join CFW_FLD_Values_Case CFW on CFW.RecId=cs.numCaseId and CFW.fld_id= '+replace(@columnName,'DCust','')                                                                                           
		set @StrSql= @StrSql + ' left Join ListDetails LstCF on LstCF.numListItemID=CFW.Fld_Value'        
	end 

	       
-------Change Row Color-------
Declare @vcCSOrigDbCOlumnName as varchar(50),@vcCSLookBackTableName as varchar(50),@vcCSAssociatedControlType as varchar(50)
SET @vcCSOrigDbCOlumnName=''
SET @vcCSLookBackTableName=''

--select top 1 @vcCSOrigDbCOlumnName=DFM.vcOrigDbCOlumnName,@vcCSLookBackTableName=DFM.vcLookBackTableName from DycFieldColorScheme DFCS join DycFieldMaster DFM on 
--DFCS.numModuleID=DFM.numModuleID and DFCS.numFieldID=DFM.numFieldID
--where DFCS.numDomainID=@numDomainID and DFM.numModuleID=1

Create table #tempColorScheme(vcOrigDbCOlumnName varchar(50),vcLookBackTableName varchar(50),vcFieldValue varchar(50),vcFieldValue1 varchar(50),vcColorScheme varchar(50),vcAssociatedControlType varchar(50))

insert into #tempColorScheme select DFM.vcOrigDbCOlumnName,DFM.vcLookBackTableName,DFCS.vcFieldValue,DFCS.vcFieldValue1,DFCS.vcColorScheme,DFFM.vcAssociatedControlType 
from DycFieldColorScheme DFCS join DycFormField_Mapping DFFM on DFCS.numFieldID=DFFM.numFieldID
join DycFieldMaster DFM on DFM.numModuleID=DFFM.numModuleID and DFM.numFieldID=DFFM.numFieldID
where DFCS.numDomainID=@numDomainID and DFFM.numFormID=12 AND DFCS.numFormID=12 and isnull(DFFM.bitAllowGridColor,0)=1

IF(SELECT COUNT(*) FROM #tempColorScheme)>0
BEGIN
   select top 1 @vcCSOrigDbCOlumnName=vcOrigDbCOlumnName,@vcCSLookBackTableName=vcLookBackTableName,@vcCSAssociatedControlType=vcAssociatedControlType from #tempColorScheme
END   
---------------------------- 
 
IF len(@vcCSOrigDbCOlumnName)>0 and len(@vcCSLookBackTableName)>0
BEGIN
set @strColumns=@strColumns+',tCS.vcColorScheme'
END
       

IF len(@vcCSOrigDbCOlumnName)>0 and len(@vcCSLookBackTableName)>0
BEGIN
	 if @vcCSLookBackTableName = 'AdditionalContactsInformation'                  
		set @Prefix = 'ADC.'                  
     if @vcCSLookBackTableName = 'DivisionMaster'                  
		set @Prefix = 'DM.'
	 if @vcCSLookBackTableName = 'CompanyInfo'                  
		set @Prefix = 'CMP.' 
	 if @vcCSLookBackTableName = 'Cases'                  
		set @Prefix = 'Cs.'   
 
	IF @vcCSAssociatedControlType='DateField'
	BEGIN
			set @strSql=@strSql+' left join #tempColorScheme tCS on CONVERT(VARCHAR(10),' + @Prefix + @vcCSOrigDbCOlumnName + ',111) >= CONVERT(VARCHAR(10),DateAdd(day,cast(tCS.vcFieldValue as int),GETUTCDATE()),111)
			 and CONVERT(VARCHAR(10),' + @Prefix + @vcCSOrigDbCOlumnName + ',111) <= CONVERT(VARCHAR(10),DateAdd(day,cast(tCS.vcFieldValue1 as int),GETUTCDATE()),111)'
	END
	ELSE IF @vcCSAssociatedControlType='SelectBox' OR @vcCSAssociatedControlType='ListBox' OR @vcCSAssociatedControlType='CheckBox'
	BEGIN
		set @strSql=@strSql+' left join #tempColorScheme tCS on tCS.vcFieldValue=' + @Prefix + @vcCSOrigDbCOlumnName
	END
END

	SET @strSql = @StrSql + ' Where cs.numDomainID='+ convert(varchar(15),@numDomainID ) + ''
 
 
	IF @numStatus <> 0
	BEGIN
		SET @strSql= @strSql + ' AND cs.numStatus = ' + CONVERT(VARCHAR(10),@numStatus)                                      	
	END
	ELSE IF @tintSortOrder <> 5
	BEGIN
 		SET @strSql= @strSql + ' AND cs.numStatus <> 136 ' 
	END
                                                 
	if @numDivisionID <>0 set @strSql=@strSql + ' And div.numDivisionID =' + convert(varchar(15),@numDivisionID)                                                      
	if @SortChar<>'0' set @strSql=@strSql + ' And (ADC.vcFirstName like '''+@SortChar+'%'''                                                              
	if @SortChar<>'0' set @strSql=@strSql + ' or ADC.vcLastName like '''+@SortChar+'%'')'                                                             

	IF @tintSortOrder!=6
	BEGIN
		if @tintUserRightType=1 set @strSql=@strSql + ' AND (Cs.numRecOwner = '+convert(varchar(15),@numUserCntId)+ ' or Cs.numAssignedTo='+convert(varchar(15),@numUserCntID) + 
		+ case when @tintSortOrder=1 THEN ' OR (Cs.numRecOwner = -1 or Cs.numAssignedTo= -1)' ELSE '' end
		+ case when @bitPartner=1 then ' or (CCont.bitPartner=1 and CCont.numContactId='+convert(varchar(15),@numUserCntID)+ ')' else '' end  + ' or ' + @strShareRedordWith +')'                
		else if @tintUserRightType=2 set @strSql=@strSql + ' AND (DM.numTerID in (select numTerritoryID from  UserTerritory where numUserCntID= '+convert(varchar(15),@numUserCntId)+' ) or Cs.numAssignedTo='+convert(varchar(15),@numUserCntID)+ ' or ' + @strShareRedordWith +')'                   
	END
	ELSE
	BEGIN
		SET @strSql=@strSql + ' AND (Cs.numRecOwner = -1 or Cs.numAssignedTo= -1)'
	END                 
                                                   
	if @tintSortOrder=0  set @strSql=@strSql + ' AND (Cs.numRecOwner = '+convert(varchar(15),@numUserCntId)+' or Cs.numAssignedTo = '+convert(varchar(15),@numUserCntId)+ ' or ' + @strShareRedordWith +')'                                                                                                     
	ELSE if @tintSortOrder=2  set @strSql=@strSql + 'And cs.numStatus<>136  ' + ' AND cs.bintCreatedDate> '''+convert(varchar(20),dateadd(day,-7,getutcdate()))+''''                                 
	else if @tintSortOrder=3  set @strSql=@strSql+'And cs.numStatus<>136  ' + ' and cs.numCreatedBy ='+ convert(varchar(15),@numUserCntId)       
	else if @tintSortOrder=4  set @strSql=@strSql+ 'And cs.numStatus<>136  ' + ' and cs.numModifiedBy ='+ convert(varchar(15),@numUserCntId)                                                            
	else if @tintSortOrder=5  set @strSql=@strSql+ 'And cs.numStatus=136  ' + ' and cs.numModifiedBy ='+ convert(varchar(15),@numUserCntId)                                                            
 
	IF @vcRegularSearchCriteria<>'' set @strSql=@strSql+' and ' + @vcRegularSearchCriteria 
	IF @vcCustomSearchCriteria<>'' set @strSql=@strSql+' and cs.numCaseId in (select distinct CFW.RecId from CFW_FLD_Values_Case CFW where ' + @vcCustomSearchCriteria + ')'


	IF LEN(@SearchQuery) > 0
	BEGIN
		SET @StrSql = @StrSql + ' AND (' + @SearchQuery + ') '
	END	
	
	declare @firstRec as integer                                                            
	declare @lastRec as integer                                                            
	set @firstRec= (@CurrentPage-1) * @PageSize                                                            
	set @lastRec= (@CurrentPage*@PageSize+1)  

	-- GETS ROWS COUNT
	DECLARE @strTotal NVARCHAR(MAX) = 'SELECT @TotalRecords = COUNT(*) ' + @StrSql
	exec sp_executesql @strTotal, N'@TotalRecords INT OUT', @TotRecs OUT

	

	-- EXECUTE FINAL QUERY
	DECLARE @strFinal AS VARCHAR(MAX)
	SET @strFinal = CONCAT('SELECT ROW_NUMBER() OVER(ORDER BY ',@columnName,' ',@columnSortOrder,') ID, ',@strColumns,' INTO #tempTable',@StrSql,'; SELECT * FROM #tempTable WHERE ID > ',@firstRec,' and ID <',@lastRec,'; DROP TABLE #tempTable;')
	
	PRINT @strFinal
	EXEC (@strFinal) 

	SELECT * FROM #tempForm

	DROP TABLE #tempForm

	DROP TABLE #tempColorScheme
GO
/****** Object:  StoredProcedure [dbo].[usp_GetCommunicationInfo]    Script Date: 07/26/2008 16:16:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- Modified By Anoop Jayaraj                            
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getcommunicationinfo')
DROP PROCEDURE usp_getcommunicationinfo
GO
CREATE PROCEDURE [dbo].[USP_GetCommunicationInfo]                                              
 @numCommid numeric(9)=0,                            
 @ClientTimeZoneOffset Int                        
AS                          
DECLARE @numFollowUpStatus NUMERIC
SELECT @numFollowUpStatus = [numFollowUpStatus] FROM [DivisionMaster] WHERE [numDivisionID] IN (SELECT [numDivisionID] FROM [Communication] WHERE [numCommId]=@numCommid)
SELECT comm.numCommId, comm.bitTask, bitOutlook,                        
comm.numContactId,                        
comm.numDivisionId, ISNULL(comm.textDetails,'') AS [textDetails], comm.intSnoozeMins, comm.intRemainderMins,                        
comm.numStatus, comm.numActivity, comm.numAssign, comm.tintSnoozeStatus,                        
comm.tintRemStatus, comm.numOppId, comm.numCreatedBy,                        
comm.numModifiedBy, comm.bitClosedFlag, comm.vcCalendarName,                         
DateAdd(minute, -@ClientTimeZoneOffset,dtStartTime ) as dtStartTime,            
DateAdd(minute, -@ClientTimeZoneOffset,dtEndTime ) as dtEndTime,                        
'-' as AssignTo,                            
isnull(dbo.fn_GetContactName(comm.numCreatedBy),'-')+' '+convert(varchar(20),DateAdd(minute, -@ClientTimeZoneOffset,Comm.dtCreatedDate )) as CreatedBy,                            
isnull(dbo.fn_GetContactName(comm.numModifiedBy),'-')+' '+convert(varchar(20),DateAdd(minute, -@ClientTimeZoneOffset,Comm.dtModifiedDate )) as ModifiedBy,                            
isnull(dbo.fn_GetContactName(comm.numCreatedBy),'-') as RecOwner,                            
convert(varchar(20),dtModifiedDate) as ModifiedDate,        
isnull(bitSendEmailTemp,0) as bitSendEmailTemp,        
isnull(numEmailTemplate,0) as numEmailTemplate,        
isnull(tintHours,0) as tintHours,        
isnull(bitAlert,0) as bitAlert,      
Comm.caseid,        
(select top 1 vcCaseNumber from cases where cases.numcaseid= Comm.caseid )as vcCaseName,             
isnull(caseTimeid,0)as caseTimeid,        
isnull(caseExpid,0)as caseExpid   ,    
numActivityId,
isnull(C.[numCorrespondenceID],0) numCorrespondenceID,
ISNULL(C.[numOpenRecordID],0) numOpenRecordID,
ISNULL(C.[tintCorrType],0) tintCorrType,
ISNULL(@numFollowUpStatus,0) AS numFollowUpStatus,ISNULL(Comm.bitFollowUpAnyTime,0) bitFollowUpAnyTime,isnull(C.monMRItemAmount,0) as monMRItemAmount,
ISNULL(clo.numDivisionID,0) AS numLinkedOrganization,
ISNULL(clo.numContactID,0) AS numLinkedContact,
(CASE WHEN Comm.numDivisionId > 0 THEN (SELECT TOP 1 vcData from ListDetails where numListItemID =DivisionMaster.numTerId) ELSE '' END) numTerId,
(CASE WHEN Comm.numDivisionId > 0 THEN DivisionMaster.numTerId ELSE 0 END) numOrgTerId
FROM communication Comm LEFT OUTER JOIN [Correspondence] C ON C.[numCommID]=Comm.[numCommId]
LEFT JOIN CommunicationLinkedOrganization clo ON Comm.numCommId = clo.numCommID
LEFT JOIN DivisionMaster ON Comm.numDivisionId = DivisionMaster.numDivisionID
WHERE  comm.numCommid=@numCommid
GO
/****** Object:  StoredProcedure [dbo].[USP_GetCompSpecificValues]    Script Date: 07/26/2008 16:16:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj        
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getcompspecificvalues')
DROP PROCEDURE usp_getcompspecificvalues
GO
CREATE PROCEDURE [dbo].[USP_GetCompSpecificValues]        
@numCommID as numeric(9)=0,        
@numOppID as numeric(9)=0,        
@numProID as numeric(9)=0,        
@numCasesID as numeric(9)=0,        
@numDivID as numeric(9)=0 output,        
@numCompID as numeric(9)=0 output,        
@numContID as numeric(9)=0 output,        
@tintCRMType as numeric(9)=0 output,        
@numTerID as numeric(9)=0 output,        
@vcCompanyName as varchar(100)='' output,        
@vcContactName as varchar(100)='' output,        
@vcEmail as varchar(100)='' output,        
@vcPhone as varchar(20)='' output,
@charModule as char(1),        
@vcNoOfEmployeesId AS VARCHAR(30)='' OUTPUT,
@numCurrencyID AS numeric(9)=0 OUTPUT,
@bitOnCreditHold AS BIT=0 OUTPUT,
@numBillingDays AS BIGINT = 0 OUTPUT,
@numDefaultExpenseAccountID NUMERIC(18,0) = 0 OUTPUT,
@numAccountClassID NUMERIC(18,0) = 0 OUTPUT,
@numRecordOwner NUMERIC(18,0) = 0 OUTPUT
as        
        
if @charModule='A'         
begin        
 select @numDivID=isnull(D.numDivisionID,0),        
 @numCompID=isnull(D.numCompanyID,0),         
 @numContID=isnull(ADC.numContactID,0),        
  @tintCRMType=isnull(tintCRMType,0),        
 @numTerID=isnull(numTerID,0),        
        @vcCompanyName=isnull(vcCompanyName,''),        
        @vcContactName=isnull(vcFirstName,'')+' '+isnull(vcLastName,''),        
        @vcEmail=isnull(vcEmail,''),        
        @vcPhone=isnull(numPhone,''),
        @vcNoOfEmployeesId=dbo.GetListIemName(Com.numNoOfEmployeesId),
        @numCurrencyID=ISNULL(D.numCurrencyID,0),
        @bitOnCreditHold=ISNULL(D.bitOnCreditHold,0),
        @numBillingDays = ISNULL(D.numBillingDays,0),
        @numDefaultExpenseAccountID = ISNULL(numDefaultExpenseAccountID,0)
 from Communication C        
 join AdditionalContactsInformation ADC        
 on ADC.numContactID=C.numContactID        
 join DivisionMaster D        
 on D.numDivisionID=ADC.numDivisionID        
 join CompanyInfo Com        
        on Com.numCompanyID=D.numCompanyID        
 where numCommID=@numCommID        
end        
else        
        
if @charModule='O'       
begin        
 select @numDivID=isnull(D.numDivisionID,0),        
 @numCompID=isnull(D.numCompanyID,0),         
 @numContID=isnull(ADC.numContactID,0),        
  @tintCRMType=isnull(tintCRMType,0),        
 @numTerID=isnull(numTerID,0),        
 @vcCompanyName=isnull(vcCompanyName,'-'),        
        @vcContactName=isnull(vcFirstName,'-')+' '+isnull(vcLastName,''),        
        @vcEmail=isnull(vcEmail,''),        
        @vcPhone=isnull(numPhone,''),
        @vcNoOfEmployeesId=dbo.GetListIemName(Com.numNoOfEmployeesId),
        @numCurrencyID=ISNULL(D.numCurrencyID,0),
        @bitOnCreditHold=ISNULL(D.bitOnCreditHold,0),
        @numBillingDays = ISNULL(D.numBillingDays,0),
        @numDefaultExpenseAccountID = ISNULL(numDefaultExpenseAccountID,0) 
 from OpportunityMaster O        
 join AdditionalContactsInformation ADC        
 on ADC.numContactID=O.numContactID        
 join DivisionMaster D        
 on D.numDivisionID=ADC.numDivisionID        
 join CompanyInfo Com        
        on Com.numCompanyID=D.numCompanyID        
 where numOppID=@numOppID        
end        
else        
        
if @charModule='P'         
begin        
 select @numDivID=isnull(D.numDivisionID,0),        
 @numCompID=isnull(D.numCompanyID,0),         
 @numContID=isnull(ADC.numContactID,0),        
  @tintCRMType=isnull(tintCRMType,0),        
 @numTerID=isnull(numTerID,0),        
 @vcCompanyName=isnull(vcCompanyName,'-'),        
        @vcContactName=isnull(vcFirstName,'-')+' '+isnull(vcLastName,''),        
        @vcEmail=isnull(vcEmail,''),        
        @vcPhone=isnull(numPhone,''),
        @vcNoOfEmployeesId=dbo.GetListIemName(Com.numNoOfEmployeesId)  ,
        @numCurrencyID=ISNULL(D.numCurrencyID,0),
        @bitOnCreditHold=ISNULL(D.bitOnCreditHold,0),
        @numBillingDays = ISNULL(D.numBillingDays,0),
        @numDefaultExpenseAccountID = ISNULL(numDefaultExpenseAccountID,0)
 from ProjectsMaster P      
 left join AdditionalContactsInformation ADC      
 on P.numCustPrjMgr=ADC.numContactID        
 join DivisionMaster D        
 on D.numDivisionID=P.numDivisionID        
 join CompanyInfo Com        
        on Com.numCompanyID=D.numCompanyID        
 where numProID=@numProID        
end        
else        
        
if @charModule='S'         
begin        
 select @numDivID=isnull(D.numDivisionID,0),        
 @numCompID=isnull(D.numCompanyID,0),         
 @numContID=isnull(ADC.numContactID,0),        
  @tintCRMType=isnull(tintCRMType,0),        
 @numTerID=isnull(numTerID,0),        
 @vcCompanyName=isnull(vcCompanyName,'-'),        
        @vcContactName=isnull(vcFirstName,'-')+' '+isnull(vcLastName,''),        
        @vcEmail=isnull(vcEmail,''),        
        @vcPhone=isnull(numPhone,''),
        @vcNoOfEmployeesId=dbo.GetListIemName(Com.numNoOfEmployeesId) ,
        @numCurrencyID=ISNULL(D.numCurrencyID,0),
        @bitOnCreditHold=ISNULL(D.bitOnCreditHold,0),
        @numBillingDays = ISNULL(D.numBillingDays,0),
        @numDefaultExpenseAccountID = ISNULL(numDefaultExpenseAccountID,0)      
 from Cases C        
 join AdditionalContactsInformation ADC        
 on ADC.numContactID=C.numContactID        
 join DivisionMaster D        
 on D.numDivisionID=ADC.numDivisionID        
 join CompanyInfo Com        
        on Com.numCompanyID=D.numCompanyID        
 where numCaseID=@numCasesID        
end       
else        
        
if @charModule='D'         
begin        
 select @numDivID=isnull(D.numDivisionID,0),        
 @numCompID=isnull(D.numCompanyID,0),         
 @numContID=isnull(ADC.numContactID,0),        
  @tintCRMType=isnull(tintCRMType,0),        
 @numTerID=isnull(numTerID,0),        
 @vcCompanyName=isnull(vcCompanyName,'-'),        
        @vcContactName=isnull(vcFirstName,'-')+' '+isnull(vcLastName,''),        
        @vcEmail=isnull(vcEmail,''),        
        @vcPhone=isnull(numPhone,''),
        @vcNoOfEmployeesId=dbo.GetListIemName(Com.numNoOfEmployeesId) ,
        @numCurrencyID=ISNULL(D.numCurrencyID,0),
        @bitOnCreditHold=ISNULL(D.bitOnCreditHold,0),
        @numBillingDays = ISNULL(D.numBillingDays,0),
        @numDefaultExpenseAccountID = ISNULL(numDefaultExpenseAccountID,0),
		@numAccountClassID = numAccountClassID,
		@numRecordOwner = ISNULL(D.numRecOwner,0)
 from DivisionMaster D        
 join AdditionalContactsInformation ADC        
 on ADC.numDivisionID=D.numDivisionID         
 join CompanyInfo Com        
 on Com.numCompanyID=D.numCompanyID        
 where D.numDivisionID=@numDivID AND ISNULL(ADC.bitPrimaryContact,0)=1      
end       
if @charModule='C'         
begin        
 select @numDivID=isnull(D.numDivisionID,0),        
 @numCompID=isnull(D.numCompanyID,0),         
 @numContID=isnull(ADC.numContactID,0),        
  @tintCRMType=isnull(tintCRMType,0),        
 @numTerID=isnull(numTerID,0),        
 @vcCompanyName=isnull(vcCompanyName,'-'),        
        @vcContactName=isnull(vcFirstName,'-')+' '+isnull(vcLastName,''),        
        @vcEmail=isnull(vcEmail,''),        
        @vcPhone=isnull(numPhone,''),
        @vcNoOfEmployeesId=dbo.GetListIemName(Com.numNoOfEmployeesId)   ,
        @numCurrencyID=ISNULL(D.numCurrencyID,0),
        @bitOnCreditHold=ISNULL(D.bitOnCreditHold,0),
        @numBillingDays = ISNULL(D.numBillingDays,0),
        @numDefaultExpenseAccountID = ISNULL(numDefaultExpenseAccountID,0)  
 from DivisionMaster D        
 join AdditionalContactsInformation ADC        
 on ADC.numDivisionID=D.numDivisionID         
 join CompanyInfo Com        
 on Com.numCompanyID=D.numCompanyID        
 where ADC.numContactID=@numContID      
end
GO
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetECommerceDetails')
DROP PROCEDURE USP_GetECommerceDetails
GO
CREATE PROCEDURE [dbo].[USP_GetECommerceDetails]      
@numDomainID as numeric(9)=0,
@numSiteID AS INTEGER = 0  
as                              
select                               
isnull(vcPaymentGateWay,'0') as vcPaymentGateWay,                
isnull(vcPGWManagerUId,'')as vcPGWManagerUId ,                
isnull(vcPGWManagerPassword,'') as vcPGWManagerPassword,              
isnull(bitShowInStock,0) as bitShowInStock,              
isnull(bitShowQOnHand,0) as bitShowQOnHand,        
--isnull(tintItemColumns,0) as tintItemColumns,
--isnull(numDefaultCategory,0) as numDefaultCategory,  
isnull([numDefaultWareHouseID],0) numDefaultWareHouseID,
isnull(bitCheckCreditStatus,0) as bitCheckCreditStatus,
isnull([numRelationshipId],0) numRelationshipId,
isnull([numProfileId],0) numProfileId,
ISNULL([bitHidePriceBeforeLogin],0) AS bitHidePriceBeforeLogin,
ISNULL([numBizDocForCreditCard],0) AS numBizDocForCreditCard,
ISNULL([numBizDocForCreditTerm],0) AS numBizDocForCreditTerm,
ISNULL(bitAuthOnlyCreditCard,0) AS bitAuthOnlyCreditCard,
ISNULL(numAuthrizedOrderStatus,0) AS numAuthrizedOrderStatus,
ISNULL(bitSendEmail,1) AS bitSendEmail,
ISNULL(vcGoogleMerchantID , '') AS vcGoogleMerchantID,
ISNULL(vcGoogleMerchantKey,'') AS vcGoogleMerchantKey,
ISNULL(IsSandbox,0) AS IsSandbox,
ISNULL(numSiteID,0) AS numSiteID,
ISNULL(vcPaypalUserName,'') AS vcPaypalUserName,
ISNULL(vcPaypalPassword,'') AS vcPaypalPassword,
ISNULL(vcPaypalSignature,'') AS vcPaypalSignature,
ISNULL(IsPaypalSandbox,0) AS IsPaypalSandbox,
ISNULL(bitSkipStep2,0) AS [bitSkipStep2],
ISNULL(bitDisplayCategory,0) AS [bitDisplayCategory],
ISNULL(bitShowPriceUsingPriceLevel,0) AS [bitShowPriceUsingPriceLevel],
ISNULL(bitEnableSecSorting,0) AS [bitEnableSecSorting],
ISNULL(bitSortPriceMode,0) AS [bitSortPriceMode],
ISNULL(numPageSize,0) AS [numPageSize],
ISNULL(numPageVariant,0) AS [numPageVariant],
ISNULL(bitAutoSelectWarehouse,0) AS bitAutoSelectWarehouse,
ISNULL([bitPreSellUp],0) AS [bitPreSellUp],
ISNULL([bitPostSellUp],0) AS [bitPostSellUp],
ISNULL([dcPostSellDiscount],0) AS [dcPostSellDiscount],
ISNULL(numDefaultClass,0) numDefaultClass,
vcPreSellUp,vcPostSellUp,ISNULL(vcRedirectThankYouUrl,'') AS vcRedirectThankYouUrl
FROM eCommerceDTL 
WHERE numDomainID=@numDomainID
AND ([numSiteId] = @numSiteID OR @numSiteID = 0)
/****** Object:  StoredProcedure [dbo].[USP_GetImportConfiguration]    Script Date: 07/26/2008 16:17:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getimportconfiguration')
DROP PROCEDURE usp_getimportconfiguration
GO
set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go
CREATE  PROCEDURE [dbo].[USP_GetImportConfiguration]
@numRelation as numeric,              
@numDomain as numeric,
@ImportType as tinyint             
as                
--    
--if  (select count(*) from RecImportConfg where numDomainid=@numDomain and numRelationShip=@numRelation)=0    
--set @numDomain = 0    
 if @ImportType=1
begin   

--Available fields               
(select vcFormFieldName,convert(varchar(10),numFormFieldId) +'/0'+'/R' as numFormFieldId from DynamicFormFieldMaster where numFormID=9               
and numFormFieldId not in (select numFormFieldId from RecImportConfg where numDomainid=@numDomain and bitCustomFld=0 and cCtype='R' and numRelationShip=@numRelation and Importtype=1)              
              
union              
select fld_label as vcFormFieldName ,convert(varchar(10),fld_id)+ '/1'+'/L' as numFormFieldId  from CFW_Fld_Master            
 join CFW_Fld_Dtl                                          
on Fld_id=numFieldId                                          
left join CFw_Grp_Master                                           
on subgrp=CFw_Grp_Master.Grp_id                                          
where CFW_Fld_Master.grp_id IN (1,12,13,14) /*Leads/Prospects/Accounts*/ and numRelation=@numRelation and CFW_Fld_Master.numDomainID=@numDomain  and fld_id not in (select DTL.numFormFieldId from                   
RecImportConfg dtl where  DTL.numDomainID=@numDomain and bitCustomFld=1 and cCtype='L' and numRelationShip=@numRelation  and Importtype=1)          
          
union              
select fld_label as vcFormFieldName ,convert(varchar(10),fld_id)+ '/1'+'/C' as numFormFieldId  from CFW_Fld_Master            
 join CFW_Fld_Dtl                                          
on Fld_id=numFieldId                                          
left join CFw_Grp_Master                                           
on subgrp=CFw_Grp_Master.Grp_id                                          
where CFW_Fld_Master.grp_id=4 /*Contact Details*/and numRelation=@numRelation and CFW_Fld_Master.numDomainID=@numDomain     
and fld_id not in (select DTL.numFormFieldId from                   
RecImportConfg dtl where  DTL.numDomainID=@numDomain and bitCustomFld=1 and cCtype='C' and numRelationShip=@numRelation and Importtype=1)          
) order by vcFormFieldName       
              
              
             
                
--Added fields          
select HDR.vcFormFieldName,convert(varchar(10),DTL.numFormFieldId) +'/0'+'/'+cCtype as numFormFieldId,intcolumn     
from RecImportConfg DTL             
join DynamicFormFieldMaster HDR on DTL.numFormFieldId = HDR.numFormFieldId            
where DTL.numDomainId=@numDomain and DTL.numRelationShip=@numRelation and bitCustomFld = 0 and Importtype=1          
  union          
select CFW.fld_label as vcFormFieldName,convert(varchar(10),DTL.numFormFieldId) +'/1'+'/'+cCtype as numFormFieldId,intcolumn    
 from RecImportConfg DTL             
join CFW_Fld_Master CFW on DTL.numFormFieldId = CFW.Fld_id            
where DTL.numDomainid=@numDomain and DTL.numRelationShip=@numRelation and bitCustomFld = 1   and Importtype=1        
order by intcolumn  


end
else  if @ImportType=2 OR @ImportType=4
begin   
               
(select vcFormFieldName,convert(varchar(10),numFormFieldId) +'/0'+'/R' as numFormFieldId from DynamicFormFieldMaster where bitDeleted=0 AND numFormID in( 20,27) --,26,27,28
and numFormFieldId not in (select numFormFieldId from RecImportConfg where numDomainid=@numDomain and bitCustomFld=0 and cCtype='R' and Importtype=@ImportType)
AND 
(
	@ImportType=2 OR 
	(vcDbColumnName <> CASE WHEN @ImportType=4 THEN   'numWareHouseID' ELSE vcDbColumnName END 
	AND  vcDbColumnName <> CASE WHEN @ImportType=4 THEN   'numOnHand' ELSE vcDbColumnName END 
	AND  vcDbColumnName <> CASE WHEN @ImportType=4 THEN   'vcLocation' ELSE vcDbColumnName END )
)
              
union              
select fld_label as vcFormFieldName ,convert(varchar(10),fld_id)+ '/1'+'/I' as numFormFieldId  from CFW_Fld_Master                                                    
left join CFw_Grp_Master                                           
on subgrp=CFw_Grp_Master.Grp_id                                          
where CFW_Fld_Master.grp_id=5  and CFW_Fld_Master.numDomainID=@numDomain  and fld_id not in (select DTL.numFormFieldId from                   
RecImportConfg dtl where  DTL.numDomainID=@numDomain and bitCustomFld=1 and cCtype='I'   and Importtype=@ImportType)          
) order by vcFormFieldName       
              
             
select HDR.vcFormFieldName,convert(varchar(10),DTL.numFormFieldId) +'/0'+'/'+cCtype as numFormFieldId,intcolumn     
from RecImportConfg DTL             
join DynamicFormFieldMaster HDR on DTL.numFormFieldId = HDR.numFormFieldId            
where bitDeleted=0 AND DTL.numDomainId=@numDomain  and bitCustomFld = 0 and Importtype=@ImportType         
AND 
(
	@ImportType=2 OR 
	(vcDbColumnName <> CASE WHEN @ImportType=4 THEN   'numWareHouseID' ELSE vcDbColumnName END 
	AND  vcDbColumnName <> CASE WHEN @ImportType=4 THEN   'numOnHand' ELSE vcDbColumnName END 
	AND  vcDbColumnName <> CASE WHEN @ImportType=4 THEN   'vcLocation' ELSE vcDbColumnName END )
)
  union          
select CFW.fld_label as vcFormFieldName,convert(varchar(10),DTL.numFormFieldId) +'/1'+'/'+cCtype as numFormFieldId,intcolumn    
 from RecImportConfg DTL             
join CFW_Fld_Master CFW on DTL.numFormFieldId = CFW.Fld_id            
where DTL.numDomainid=@numDomain and bitCustomFld = 1   and Importtype=@ImportType      
order by intcolumn  


end
else  if @ImportType=3
begin   
(select vcFormFieldName,convert(varchar(10),numFormFieldId)  as numFormFieldId,0 as intcolumn,convert(bit,0) as bitCustomFld,'R' as cCtype,vcAssociatedControlType,vcDbColumnName from DynamicFormFieldMaster where numFormID=25)              
end

else  if @ImportType=5 --Import Assembly Items
begin   
               
select vcFormFieldName,convert(varchar(10),numFormFieldId) as numFormFieldId,0 as intcolumn,convert(bit,0) as bitCustomFld,'R' as cCtype,vcAssociatedControlType,vcDbColumnName from DynamicFormFieldMaster where numFormID=31
 order by numFormFieldId       

end
else  if @ImportType=6 --Import Tax Details
begin   
               
select vcFormFieldName,convert(varchar(10),numFormFieldId) as numFormFieldId,0 as intcolumn,convert(bit,0) as bitCustomFld,'R' as cCtype,vcAssociatedControlType,vcDbColumnName from DynamicFormFieldMaster where numFormID=45
 order by numFormFieldId       

end
else  if @ImportType=7 --Import Update Item Warehouse Information/Matrix Item
begin   
               
select vcFormFieldName,convert(varchar(10),numFormFieldId) as numFormFieldId,[order],convert(bit,0) as bitCustomFld,'R' as cCtype,vcAssociatedControlType,vcDbColumnName,numListID from DynamicFormFieldMaster where numFormID=48
UNION
 select Fld_label as vcFormFieldName,Fld_id as numFormFieldId,100 as [order],convert(bit,0) as bitCustomFld,'C' as cCtype,fld_type as vcAssociatedControlType,'' as vcDbColumnName,CFW_Fld_Master.numListID
 from CFW_Fld_Master join ItemGroupsDTL on numOppAccAttrID=Fld_id where numItemGroupID=@numRelation and tintType=2 
order by [order] 

end

/****** Object:  StoredProcedure [dbo].[USP_GetImportWareHouseItems]    Script Date: 07/26/2008 16:18:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj                              
GO
-- exec USP_GetImportWareHouseItems @numDomainID=1,@numItemGroupID=0,@byteMode=1,@bitSerialized=0,@bitLotNo=0
-- exec USP_GetImportWareHouseItems @numDomainID=1,@numItemGroupID=125,@byteMode=1,@bitSerialized=0,@bitLotNo=0
-- exec USP_GetImportWareHouseItems @numDomainID=1,@numItemGroupID=125,@byteMode=2,@bitSerialized=1,@bitLotNo=0
-- exec USP_GetImportWareHouseItems @numDomainID=1,@numItemGroupID=125,@byteMode=2,@bitSerialized=0,@bitLotNo=1
-- exec USP_GetImportWareHouseItems @numDomainID=150,@numItemGroupID=384,@byteMode=2,@bitSerialized=1,@bitLotNo=0
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_GetImportWareHouseItems' ) 
    DROP PROCEDURE USP_GetImportWareHouseItems
GO
CREATE PROCEDURE [dbo].[USP_GetImportWareHouseItems]
    @numDomainID AS NUMERIC(9) = 0,
    @numItemGroupID AS NUMERIC(9) = 0,
    @byteMode AS TINYINT = 0,
    @bitSerialized AS BIT = 0,
    @bitLotNo AS BIT = 0
AS 
    DECLARE @str AS VARCHAR(2000) ;
    SET @str = ''               
    DECLARE @str1 AS VARCHAR(2000)               
    DECLARE @ColName AS VARCHAR(25)                      
                        
--Create a Temporary table to hold data                                                            
    CREATE TABLE #tempTable
        (
          ID INT IDENTITY
                 PRIMARY KEY,
          vcFormFieldName NVARCHAR(50),
          numFormFieldId NUMERIC(18),
          tintOrder TINYINT,
          cCtype CHAR(1),
          vcAssociatedControlType NVARCHAR(50),
          vcDbColumnName NVARCHAR(50),
          vcLookBackTableName VARCHAR(50)
        )                         
	
    DECLARE @strSQL AS NVARCHAR(MAX)
    DECLARE @strWHERE AS NVARCHAR(MAX)
	
    SET @strWHERE = ' WHERE 1=1 AND numFormID = 48 AND ISNULL(DFFM.bitImport,0) <> 0 '
    SET @strSQL = '                        
    INSERT INTO #tempTable ( vcFormFieldName,numFormFieldId,tintOrder,cCtype,vcAssociatedControlType,vcDbColumnName,vcLookBackTableName )
            SELECT  DFM.vcFieldName AS vcFormFieldName,
                    DFM.numFieldID AS numFormFieldId,
                    ISNULL(DFFM.[order], 0) as tintOrder,
                    ''R'' as cCtype,
                    DFM.vcAssociatedControlType AS vcAssociatedControlType,
                    DFM.vcDbColumnName AS vcDbColumnName,
                    ISNULL(DFM.vcLookBackTableName, '''') AS vcLookBackTableName
            FROM    dbo.DycFieldMaster DFM
            INNER JOIN dycFormField_Mapping DFFM ON DFM.numFieldId = DFFM.numFieldID ' 
            
    IF ISNULL(@numItemGroupID, 0) > 0 
        BEGIN
            SET @strWHERE = @strWHERE
                + ' AND DFM.numFieldID IN (SELECT numFieldId FROM dycFormField_Mapping WHERE numFormID = 48 )  '								      
			
            SET @strSQL = @strSQL + @strWHERE + ' 
            UNION
            SELECT  Fld_label AS vcFormFieldName,
                    Fld_id AS numFormFieldId,
                    100 AS tintOrder,
                    ''C'' AS cCtype,
                    Fld_Type AS vcAssociatedControlType,
                    '''' AS vcDbColumnName,
                    '''' AS vcLookBackTableName
            FROM    CFW_Fld_Master
                    JOIN ItemGroupsDTL ON numOppAccAttrID = Fld_id
            WHERE   numItemGroupID = ' + CAST(@numItemGroupID AS VARCHAR(100))
                + ' AND tintType = 2
            ORDER BY tintOrder ' 
            
        END
    ELSE 
        BEGIN
            SET @strWHERE = @strWHERE
                + ' AND DFM.numFieldID NOT IN (SELECT numFieldId FROM dycFormField_Mapping WHERE numFormID = 48 AND vcFieldName IN (''UPC (M)'',''SKU (M)'',''Serial No'',''Comments'',''Lot Qty''))'
			
            SET @strSQL = @strSQL + @strWHERE + ' ORDER BY tintOrder ' 
            
        END
                  
    PRINT @strSQL
    EXEC SP_EXECUTESQL @strSQL
	
    --SELECT  * FROM    #tempTable
        
    DECLARE @ID AS INT,
        @vcFormFieldName AS NVARCHAR(50),
        @numFormFieldId AS NUMERIC(18),
        @tintOrder AS TINYINT,
        @cCtype AS CHAR(1),
        @vcAssociatedControlType AS NVARCHAR(50),
        @vcDbColumnName AS NVARCHAR(50),
        @vcLookBackTableName VARCHAR(20)

    IF @byteMode = 1 
        BEGIN     
            SELECT TOP 1
                    @ID = ID,
                    @vcFormFieldName = vcFormFieldName,
                    @numFormFieldId = numFormFieldId,
                    @tintOrder = tintOrder,
                    @cCtype = cCtype,
                    @vcAssociatedControlType = vcAssociatedControlType,
                    @vcDbColumnName = vcDbColumnName,
                    @vcLookBackTableName = vcLookBackTableName
            FROM    #tempTable                         
                         
            WHILE @ID > 0                        
                BEGIN 
                    IF @ID > 1 
                        SET @str = @str + ','

                    IF @vcDbColumnName = 'numWareHouseID' 
                        SET @vcDbColumnName = 'vcWareHouse'

                    IF @cCtype = 'R' 
                        IF @vcDbColumnName = 'vcLocation' 
                            BEGIN
                                SET @str = @str + 'WL.' + @vcDbColumnName
                                    + ' as [' + @vcFormFieldName + ']'    
                            END
                        ELSE 
                            BEGIN
                                SET @str = @str + @vcLookBackTableName + '.'
                                    + @vcDbColumnName + ' as ['
                                    + @vcFormFieldName + ']'                                        
                            END

                    ELSE 
                        SET @str = @str + ' dbo.GetCustFldItemsValue('
                            + CONVERT(VARCHAR(15), @numFormFieldId)
                            + ',9,WareHouseItems.numWareHouseItemID,0,'''
                            + @vcAssociatedControlType + ''') as ['
                            + @vcFormFieldName + ']'      
                                                              
                    SELECT TOP 1
                            @ID = ID,
                            @vcFormFieldName = vcFormFieldName,
                            @numFormFieldId = numFormFieldId,
                            @tintOrder = tintOrder,
                            @cCtype = cCtype,
                            @vcAssociatedControlType = vcAssociatedControlType,
                            @vcDbColumnName = vcDbColumnName,
                            @vcLookBackTableName = vcLookBackTableName
                    FROM    #tempTable
                    WHERE   ID > @ID 
   
                    IF @@rowcount = 0 
                        SET @ID = 0                        
                END                  
                      
            SET @str = RTRIM(@str)       
            PRINT 'SQL :' + @str                       
            SET @str1 = ' select ' + @str
                + ' from Item 
						LEFT join WareHouseItems on Item.numItemCode=WareHouseItems.numItemID
						LEFT join Warehouses on Warehouses.numWareHouseID=WareHouseItems.numWareHouseID                                    
						LEFT JOIN WareHouseItmsDTL ON WareHouseItmsDTL.numWareHouseItemID = dbo.WareHouseItems.numWareHouseItemID
						LEFT JOIN dbo.WarehouseLocation WL ON WareHouseItems.numWLocationID = WL.numWLocationID and WareHouseItems.numDomainID = WL.numDomainID 
						where Item.numDomainID='
                + CONVERT(VARCHAR(15), @numDomainID)
                + ' AND Item.numItemGroup='
                + CONVERT(VARCHAR(15), @numItemGroupID)
                + ' ORDER BY dbo.WareHouseItems.numWareHouseItemID DESC'
				   
            PRINT ( @str1 )
            EXEC ( @str1
                ) 
                           
        END

    ELSE 
        IF @byteMode = 2 
            BEGIN
                SELECT TOP 1
                        @ID = ID,
                        @vcFormFieldName = vcFormFieldName,
                        @numFormFieldId = numFormFieldId,
                        @tintOrder = tintOrder,
                        @cCtype = cCtype,
                        @vcAssociatedControlType = vcAssociatedControlType,
                        @vcDbColumnName = vcDbColumnName,
                        @vcLookBackTableName = vcLookBackTableName
                FROM    #tempTable                         
                         
                WHILE @ID > 0                        
                    BEGIN 
                        IF @ID > 1 
                            SET @str = @str + ','

                        IF @vcDbColumnName = 'numWareHouseID' 
                            SET @vcDbColumnName = 'vcWareHouse'

                        IF @cCtype = 'R' 
                            IF @vcDbColumnName = 'vcLocation' 
                                BEGIN
                                    SET @str = @str + 'WL.' + @vcDbColumnName
                                        + ' as [' + @vcFormFieldName + ']'    
                                END
                            ELSE 
                                BEGIN
                                    SET @str = @str + @vcLookBackTableName
                                        + '.' + @vcDbColumnName + ' as ['
                                        + @vcFormFieldName + ']'                                        	
                                END
								
                        ELSE 
                            SET @str = @str + ' dbo.GetCustFldItemsValue('
                                + CONVERT(VARCHAR(15), @numFormFieldId)
                                + ',9,numWareHouseItmsDTLID,0,'''
                                + @vcAssociatedControlType + ''') as ['
                                + @vcFormFieldName + ']'                                        
							
                        SELECT TOP 1
                                @ID = ID,
                                @vcFormFieldName = vcFormFieldName,
                                @numFormFieldId = numFormFieldId,
                                @tintOrder = tintOrder,
                                @cCtype = cCtype,
                                @vcAssociatedControlType = vcAssociatedControlType,
                                @vcDbColumnName = vcDbColumnName,
                                @vcLookBackTableName = vcLookBackTableName
                        FROM    #tempTable
                        WHERE   ID > @ID                        
   
                        IF @@rowcount = 0 
                            SET @ID = 0                        
                    END                        
                
                PRINT @str1       
                SET @str1 = ' SELECT   DISTINCT ' + @str 
							--+ ',vcSerialNo AS ' + CASE WHEN @bitSerialized = 1 THEN '[Serial No]' WHEN @bitLotNo = 1 THEN '[LOT No]' END 
                    + ',WareHouseItmsDTL.vcComments AS Comments'

                IF @bitLotNo = 1 
                    SET @str1 = @str1 + ',WareHouseItmsDTL.numQty AS Qty'

                SET @str1 = @str1
                    + ' FROM ITEM 
						LEFT JOIN WareHouseItems  ON Item.numItemCode=WareHouseItems.numItemID
						LEFT JOIN Warehouses  ON Warehouses.numWareHouseID=WareHouseItems.numWareHouseID      
						LEFT JOIN WareHouseItmsDTL ON WareHouseItmsDTL.numWareHouseItemID = dbo.WareHouseItems.numWareHouseItemID
						LEFT JOIN dbo.WarehouseLocation WL ON WareHouseItems.numWLocationID = WL.numWLocationID and WareHouseItems.numDomainID = WL.numDomainID 
						WHERE Item.numDomainID='
                    + CONVERT(VARCHAR(15), @numDomainID)
                    + ' and Item.numItemGroup='
                    + CONVERT(VARCHAR(15), @numItemGroupID)
                    + '
						AND ISNULL(WareHouseItmsDTL.numQty,0)>0 AND (Item.bitSerialized='
                    + CONVERT(VARCHAR(15), @bitSerialized)
                    + ' AND Item.bitLotNo=' + CONVERT(VARCHAR(15), @bitLotNo)
                    + ')'
                    + ' ORDER BY dbo.WareHouseItems.numWareHouseItemID DESC'
                  
                SET @str1 = REPLACE(@str1, '[Serial No]', '[Serial/Lot #s]')
                SET @str1 = REPLACE(@str1, 'WareHouseItmsDTL.vcSerialNo',
                                    'NULL')
                SET @str1 = REPLACE(@str1, 'WareHouseItmsDTL.vcComments',
                                    'NULL')
                SET @str1 = REPLACE(@str1, 'WareHouseItmsDTL.vcSerialNo',
                                    'NULL')
                SET @str1 = REPLACE(@str1, 'WareHouseItmsDTL.numQty', 'NULL')
                SET @str1 = REPLACE(@str1, 'WareHouseItmsDTL.numQty', 'NULL')
				
                PRINT ( @str1 )           
                EXEC ( @str1
                    )
            END

        ELSE 
            BEGIN	
                SELECT  vcFormFieldName,
                        numFormFieldId,
                        tintOrder,
                        cCtype,
                        vcAssociatedControlType,
                        vcDbColumnName,
                        vcLookBackTableName
                FROM    #tempTable
                ORDER BY tintOrder
            END

    DROP TABLE #tempTable
GO
/****** Object:  StoredProcedure [dbo].[USP_GetItemAttributesEcomm]    Script Date: 07/26/2008 16:17:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj  
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getitemattributesecomm')
DROP PROCEDURE usp_getitemattributesecomm
GO
CREATE PROCEDURE [dbo].[USP_GetItemAttributesEcomm]        
@numItemCode as numeric(9)=0,  
@numWarehouseID as numeric(9)=0       
AS  
BEGIN  
	DECLARE @bitMatrix AS BIT  
	DECLARE @numItemGroup AS NUMERIC(18,0)

	SELECT 
		@bitMatrix=ISNULL(bitMatrix,0)
		,@numItemGroup=ISNULL(numItemGroup,0)
	FROM 
		Item 
	WHERE 
		numItemcode= @numItemCode  

	IF @bitMatrix = 1 AND @numItemGroup > 0
	BEGIN
		SELECT DISTINCT
			CFW_Fld_Master.fld_id
			,Fld_label
			,fld_type
			,CFW_Fld_Master.numlistid
			,vcURL
			,ISNULL(ItemAttributes.FLD_Value,0) AS Fld_Value
		FROM
			ItemGroupsDTL
		INNER JOIN
			CFW_Fld_Master
		ON
			ItemGroupsDTL.numOppAccAttrID=CFW_Fld_Master.Fld_ID
		INNER JOIN
			ItemAttributes
		ON
			ItemAttributes.numItemCode = @numItemCode
			AND ItemAttributes.FLD_ID = CFW_Fld_Master.Fld_ID
		WHERE
			ItemGroupsDTL.numItemGroupID = @numItemGroup
			AND tintType=2
		ORDER BY
			CFW_Fld_Master.Fld_id
	END
	ELSE
	BEGIN
		 SELECT DISTINCT
			CFW_Fld_Master.fld_id
			,Fld_label
			,fld_type
			,CFW_Fld_Master.numlistid
			,vcURL
			,0 AS Fld_Value
		FROM 
			ItemGroupsDTL                   
		JOIN 
			CFW_Fld_Master 
		ON 
			numOppAccAttrID=Fld_ID              
		INNER JOIN 
			Item 
		ON 
			numItemGroup = ItemGroupsDTL.numItemGroupID 
			AND tintType=2  
		LEFT JOIN 
			WareHouseItems WI 
		ON 
			WI.numItemID= numItemcode  
		LEFT JOIN 
			CFW_Fld_Values_Serialized_Items 
		ON 
			RecId=WI.numWareHouseItemID    
		WHERE 
			numItemcode=@numItemCode 
			AND (numWarehouseID=@numWarehouseID OR charItemType <> 'P')
		ORDER BY
			CFW_Fld_Master.Fld_id
	END
END
GO
-- exec Usp_GetItemAttributesForAPI @numItemCode=822625,@numDomainID=170,@WebApiId = 5
/****** Subject:  StoredProcedure [dbo].[Usp_GetItemAttributesForAPI]    Script Date: 12th Jan,2015 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--created by Manish Anjara
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'Usp_GetItemAttributesForAPI' )
    DROP PROCEDURE Usp_GetItemAttributesForAPI
GO
CREATE PROCEDURE [dbo].[Usp_GetItemAttributesForAPI]
    @numItemCode AS NUMERIC(9) = 0 ,
    @numDomainID AS NUMERIC(9) ,
    @WebApiId AS INT
    --@byteMode AS TINYINT = 0
AS
    DECLARE @numDefaultWarehouseID AS NUMERIC(18)
    SELECT  @numDefaultWarehouseID = ISNULL(numwarehouseID, 0)
    FROM    [dbo].[WebAPIDetail] AS WAD
    WHERE   [WAD].[numDomainId] = @numDomainID
            AND [WAD].[WebApiId] = @WebApiId
    IF ISNULL(@numDefaultWarehouseID, 0) = 0
        BEGIN
            SELECT TOP 1
                    *
            FROM    [dbo].[WareHouseItems] AS WHI
            WHERE   [WHI].[numItemID] = @numItemCode
                    AND [WHI].[numDomainID] = @numDomainID
        END


    SELECT  DISTINCT
            FMST.fld_id ,
            Fld_label ,
            ISNULL(FMST.numlistid, 0) AS numlistid ,
            [LD].[numListItemID] ,
            [LD].[vcData]
    INTO    #tmpSpec
    FROM    CFW_Fld_Master FMST
            LEFT JOIN CFw_Grp_Master Tmst ON Tmst.Grp_id = FMST.subgrp
            JOIN CFW_Loc_Master Lmst ON Lmst.Loc_id = FMST.Grp_id
            JOIN [dbo].[ListMaster] AS LM ON [FMST].[numlistid] = LM.[numListID]
            JOIN [dbo].[ListDetails] AS LD ON LD.[numListID] = LM.[numListID]
                                              AND [FMST].[numlistid] = LD.[numListID]
            JOIN [dbo].[ItemGroupsDTL] AS IGD ON IGD.[numOppAccAttrID] = [FMST].[Fld_id]
            JOIN [dbo].[CFW_Fld_Values_Serialized_Items] AS CFVSI ON [CFVSI].[Fld_ID] = [FMST].[Fld_id]
                                                              AND CONVERT(NUMERIC, [CFVSI].[Fld_Value]) = [LD].[numListItemID]
                                                              AND [CFVSI].[RecId] IN (
                                                              SELECT
                                                              numWarehouseItemID
                                                              FROM
                                                              [dbo].[WareHouseItems]
                                                              WHERE
                                                              numwarehouseID = @numDefaultWarehouseID
                                                              AND numDomainID = @numDomainID
                                                              AND numItemID = @numItemCode )
    WHERE   FMST.numDomainID = @numDomainID
            AND Lmst.Loc_id = 9
    ORDER BY [fmst].[Fld_id]

	--SELECT * FROM [#tmpSpec] AS TS
    SELECT DISTINCT
            TS.fld_label [Name] ,
            STUFF(( SELECT  ',' + CAST([vcData] AS VARCHAR(200))
                    FROM    #tmpSpec
                    WHERE   [ts].[Fld_id] = #tmpSpec.[Fld_id]
                  FOR
                    XML PATH('')
                  ), 1, 1, '') [Value]
    FROM    [#tmpSpec] AS TS
    

    DECLARE @bitSerialize AS BIT                      
    DECLARE @str AS NVARCHAR(MAX)                 
    DECLARE @str1 AS NVARCHAR(MAX)               
    DECLARE @ColName AS VARCHAR(25)                      
    SET @str = ''                       
                        
    DECLARE @bitKitParent BIT
    DECLARE @numItemGroupID AS NUMERIC(9)                        
                        
    SELECT  @numItemGroupID = numItemGroup ,
            @bitSerialize = CASE WHEN bitSerialized = 0 THEN bitLotNo
                                 ELSE bitSerialized
                            END ,
            @bitKitParent = ( CASE WHEN ISNULL(bitKitParent, 0) = 1
                                        AND ISNULL(bitAssembly, 0) = 1 THEN 0
                                   WHEN ISNULL(bitKitParent, 0) = 1 THEN 1
                                   ELSE 0
                              END )
    FROM    Item
    WHERE   numItemCode = @numItemCode                        
    SET @ColName = 'numWareHouseItemID,0'
                      
              
--Create a Temporary table to hold data                                                            
    CREATE TABLE #tempTable
        (
          ID INT IDENTITY
                 PRIMARY KEY ,
          numCusFlDItemID NUMERIC(9)
        )                         
                        
    INSERT  INTO #tempTable
            ( numCusFlDItemID
            )
            SELECT DISTINCT
                    ( numOppAccAttrID )
            FROM    ItemGroupsDTL
            WHERE   numItemGroupID = @numItemGroupID
                    AND tintType = 2                       
                          
                 
    DECLARE @ID AS NUMERIC(9)                        
    DECLARE @numCusFlDItemID AS VARCHAR(20)                        
    DECLARE @fld_label AS VARCHAR(100) ,
        @fld_type AS VARCHAR(100)                        
    SET @ID = 0                        
    SELECT TOP 1
            @ID = ID ,
            @numCusFlDItemID = numCusFlDItemID ,
            @fld_label = fld_label ,
            @fld_type = fld_type
    FROM    #tempTable
            JOIN CFW_Fld_Master ON numCusFlDItemID = Fld_ID                        
                         
    WHILE @ID > 0
        BEGIN                        
                          
           -- SET @str = @str + ',  dbo.GetCustFldItems(' + @numCusFlDItemID + ',9,' + @ColName + ') as [' + @fld_label + ']'
	
            --IF @byteMode = 1
            SET @str = @str + ', dbo.GetCustFldItemsValue(' + @numCusFlDItemID + ',9,' + @ColName + ',''' + @fld_type + ''') as [' + @fld_label + '_c]'                                        
                          
            SELECT TOP 1
                    @ID = ID ,
                    @numCusFlDItemID = numCusFlDItemID ,
                    @fld_label = fld_label
            FROM    #tempTable
                    JOIN CFW_Fld_Master ON numCusFlDItemID = Fld_ID
                                           AND ID > @ID                        
            IF @@rowcount = 0
                SET @ID = 0                        
                          
        END                        
                       
--      

    DECLARE @KitOnHand AS FLOAT;
    SET @KitOnHand = 0

    SET @str1 = 'select '

    SET @str1 = @str1 + 'I.numItemCode,'
	
    SET @str1 = @str1
        + 'numWareHouseItemID,isnull(vcWarehouse,'''') + '': '' + isnull(WL.vcLocation,'''') vcWarehouse,W.numWareHouseID,
Case when @bitKitParent=1 then ISNULL(dbo.fn_GetKitInventoryByWarehouse(I.numItemCode, W.numWareHouseID),0) else isnull(numOnHand,0) end as [OnHand],
CAST((dbo.fn_UOMConversion(ISNULL(I.numBaseUnit,0),I.numItemCode,I.numDomainId,ISNULL(I.numPurchaseUnit,0)) * Case when @bitKitParent=1 THEN ISNULL(dbo.fn_GetKitInventoryByWarehouse(I.numItemCode, W.numWareHouseID),0) ELSE isnull(numOnHand,0) end) as numeric(18,2)) as PurchaseOnHand,
CAST((dbo.fn_UOMConversion(ISNULL(I.numBaseUnit,0),I.numItemCode,I.numDomainId,ISNULL(I.numSaleUnit,0)) * Case when @bitKitParent=1 THEN ISNULL(dbo.fn_GetKitInventoryByWarehouse(I.numItemCode, W.numWareHouseID),0) ELSE isnull(numOnHand,0) end) as numeric(18,2)) as SalesOnHand,
Case when @bitKitParent=1 THEN 0 ELSE isnull(numReorder,0) END as [Reorder] 
,Case when @bitKitParent=1 THEN 0 ELSE isnull(numOnOrder,0) END as [OnOrder]
,Case when @bitKitParent=1 THEN 0 ELSE isnull(numAllocation,0) END as [Allocation] 
,Case when @bitKitParent=1 THEN 0 ELSE isnull(numBackOrder,0) END as [BackOrder]
,0 as Op_Flag , isnull(WL.vcLocation,'''') Location,isnull(WL.numWLocationID,0) as numWLocationID,
round(isnull(monWListPrice,0),2) Price,isnull(vcWHSKU,'''') as SKU,isnull(vcBarCode,'''') as BarCode '
        + CASE WHEN @bitSerialize = 1 THEN ' '
               ELSE @str
          END
        + '                   
,(SELECT CASE WHEN ISNULL(subI.bitKitParent,0) = 1 AND ISNULL((SELECT COUNT(*) FROM OpportunityItems 
															   WHERE OpportunityItems.numItemCode = WareHouseItems.numItemID
															   AND WareHouseItems.numWareHouseItemID = OpportunityItems.numWarehouseItmsID),0) = 0 THEN 1
			ELSE 0 
	    END 
FROM Item subI WHERE subI.numItemCode = WareHouseItems.numItemID) [IsDeleteKitWarehouse],@bitKitParent [bitKitParent]
from WareHouseItems                           
join Warehouses W                             
on W.numWareHouseID=WareHouseItems.numWareHouseID 
left join WarehouseLocation WL on WL.numWLocationID = WareHouseItems.numWLocationID
join Item I on I.numItemCode=WareHouseItems.numItemID and WareHouseItems.numDomainId=I.numDomainId
where numItemID=' + CONVERT(VARCHAR(15), @numItemCode)
        + ' AND WareHouseItems.numWarehouseItemID IN (
                                                              SELECT
                                                              numWarehouseItemID
                                                              FROM
                                                              [dbo].[WareHouseItems]
                                                              WHERE
                                                              numwarehouseID = '
        + CONVERT(VARCHAR(15), @numDefaultWarehouseID)
        + '
                                                              AND numDomainID = '
        + CONVERT(VARCHAR(15), @numDomainID)
        + '
                                                              AND numItemID = '
        + CONVERT(VARCHAR(15), @numItemCode) + ' ) '
   
    PRINT ( @str1 )           

    EXECUTE sp_executeSQL @str1, N'@bitKitParent bit', @bitKitParent
                       

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetItemBarcodeList')
DROP PROCEDURE USP_GetItemBarcodeList
GO
CREATE PROCEDURE [dbo].[USP_GetItemBarcodeList]                              
@numDomainID as numeric(9)=0,    
@strItemIdList as text,
@bitAllItems as bit=0,
@tinyBarcodeBasedOn tinyint=0
as                
      
Create table #tempTable (                  
  numItemCode numeric(9)                                      
  )  

IF @bitAllItems=0
   insert into #tempTable select Item FROM dbo.DelimitedSplit8K(@strItemIdList,',')  
else
   insert into #tempTable select numItemCode from item where charItemType='P' and  numDomainID=@numDomainID

declare @str1 as varchar(8000);SET @str1=''               

declare @str as varchar(8000);SET @str=''' '''               

--Create a Temporary table to hold data                                                            
create table #tempCustTable ( ID INT IDENTITY PRIMARY KEY,                                                                      
 numCusFlDItemID numeric(9),
 fld_label varchar(50),
fld_type varchar(50)                                                         
 )                         
 declare @ID as numeric(9)                        
 declare @numCusFlDItemID as varchar(20)                        
 declare @fld_label as varchar(100),@fld_type as varchar(100)                        
 set @ID=0                        

If @tinyBarcodeBasedOn=0 --UPC/Barcode
BEGIN
                        
insert into #tempCustTable(numCusFlDItemID,fld_label,fld_type)                                                            
select distinct numOppAccAttrID,fld_label,fld_type from CFW_Fld_Master join ItemGroupsDTL on numOppAccAttrID=Fld_id where numDomainID=@numDomainID and tintType=2 
            
 select top 1 @ID=ID,@numCusFlDItemID=numCusFlDItemID,@fld_label=fld_label,@fld_type=fld_type from #tempCustTable                         
                         
 while @ID>0                        
 begin    
	IF @ID=1  SET @str='''' + @fld_label
	Else   SET @str= @str + ' + ''  ' + @fld_label  
               
	set @str=@str+ ':'' + dbo.GetCustFldItemsValue('+@numCusFlDItemID+',9,numWareHouseItemID,0,'''+@fld_type+''')'                                        
                          
   select top 1 @ID=ID,@numCusFlDItemID=numCusFlDItemID,@fld_label=fld_label from #tempCustTable                         
   where ID >@ID                        
   if @@rowcount=0 set @ID=0                        
 end 

set @str1 ='select I.numItemCode,I.vcItemName,I.numBarCodeId,'''' as vcWareHouse,'''' as vcAttribute 
			from Item I 
			join #tempTable T on I.numItemCode = T.numItemCode 
			where I.numDomainID='+convert(varchar(15),@numDomainID) + ' 
			and isnull(numItemGroup,0)=0 
			and I.numBarCodeId is not null 
			and len(I.numBarCodeId)>1 
 
			Union

			select I.numItemCode,I.vcItemName,WI.vcBarCode as numBarCodeId,W.vcWareHouse,' + @str + ' as vcAttribute  
			from Item I 
			join #tempTable T on I.numItemCode=T.numItemCode
			join WareHouseItems WI on WI.numDomainID='+convert(varchar(15),@numDomainID) + ' and I.numItemCode=WI.numItemID  
			join Warehouses W on W.numDomainID='+convert(varchar(15),@numDomainID) + ' and W.numWareHouseID=WI.numWareHouseID  
			where I.numDomainID='+convert(varchar(15),@numDomainID) + ' 
			and isnull(numItemGroup,0)>0 
			and WI.vcBarCode is not null 
			and len(WI.vcBarCode)>1'


END

ELSE If @tinyBarcodeBasedOn=1 --SKU
	BEGIN
	set @str1 ='select I.numItemCode,I.vcItemName,I.vcSKU as numBarCodeId,'''' as vcWareHouse,'''' as vcAttribute from Item I join #tempTable T on I.numItemCode=T.numItemCode 
	where I.numDomainID='+convert(varchar(15),@numDomainID) + ' and I.vcSKU is not null and len(I.vcSKU)>1'
	END

ELSE If @tinyBarcodeBasedOn=2 --Vendor Part#
	BEGIN
	set @str1 ='select I.numItemCode,I.vcItemName,Vendor.vcPartNo as numBarCodeId,com.vcCompanyName as vcWareHouse,'''' as vcAttribute
	from Vendor 
	join Item I on I.numItemCode= Vendor.numItemCode
	join #tempTable T on I.numItemCode=T.numItemCode  
	join divisionMaster div on div.numdivisionid=Vendor.numVendorid        
	join companyInfo com on com.numCompanyID=div.numCompanyID        
	WHERE I.numDomainID='+convert(varchar(15),@numDomainID) + ' and Vendor.numDomainID='+convert(varchar(15),@numDomainID) + ' 
	and Vendor.vcPartNo is not null and len(Vendor.vcPartNo)>1'
	END

ELSE If @tinyBarcodeBasedOn=3 --Serial #/LOT #
	BEGIN
	                        
	insert into #tempCustTable(numCusFlDItemID,fld_label,fld_type)                                                            
	select distinct numOppAccAttrID,fld_label,fld_type from CFW_Fld_Master join ItemGroupsDTL on numOppAccAttrID=Fld_id where numDomainID=@numDomainID and tintType=2 
	            
	 select top 1 @ID=ID,@numCusFlDItemID=numCusFlDItemID,@fld_label=fld_label,@fld_type=fld_type from #tempCustTable                         
	                         
	 while @ID>0                        
	 begin    
		IF @ID=1  SET @str='''' + @fld_label
		Else   SET @str= @str + ' + ''  ' + @fld_label  
	               
		set @str=@str+ ':'' + dbo.GetCustFldItemsValue('+@numCusFlDItemID+',9,numWareHouseItmsDTLID,0,'''+@fld_type+''')'                                        
	                          
	   select top 1 @ID=ID,@numCusFlDItemID=numCusFlDItemID,@fld_label=fld_label from #tempCustTable                         
	   where ID >@ID                        
	   if @@rowcount=0 set @ID=0                        
	 end 

	set @str1 ='select I.numItemCode,I.vcItemName,WDTL.vcSerialNo as numBarCodeId,W.vcWareHouse,' + @str + ' as vcAttribute 
	from Item I join #tempTable T on I.numItemCode=T.numItemCode
	join WareHouseItems WI on WI.numDomainID='+convert(varchar(15),@numDomainID) + ' and I.numItemCode=WI.numItemID  
	join Warehouses W on W.numDomainID='+convert(varchar(15),@numDomainID) + ' and W.numWareHouseID=WI.numWareHouseID  
	join WareHouseItmsDTL WDTL on WDTL.numWareHouseItemID=WI.numWareHouseItemID    
	where I.numDomainID='+convert(varchar(15),@numDomainID) + ' and (I.bitSerialized=1 or I.bitLotNo=1) and WDTL.vcSerialNo is not null and len(WDTL.vcSerialNo)>1
	and ISNULL(WDTL.numQty,0) > 0 '

	END

ELSE If @tinyBarcodeBasedOn = 4 --Item Code
	BEGIN
	set @str1 ='SELECT I.numItemCode,I.vcItemName,I.numItemCode AS numBarCodeId,'''' AS vcWareHouse,'''' AS vcAttribute 
				FROM Item I JOIN #tempTable T ON I.numItemCode = T.numItemCode 
				WHERE I.numDomainID = ' + CONVERT(VARCHAR(15),@numDomainID)
	END


print(@str1)           
exec (@str1)  

drop table #tempCustTable
drop table  #tempTable
GO
/****** Object:  StoredProcedure [dbo].[USP_GetItemsAttrWarehouse]    Script Date: 07/26/2008 16:17:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--exec USP_GetWareHouseItems 6                            
--created by anoop jayaraj                              
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getitemsattrwarehouse')
DROP PROCEDURE usp_getitemsattrwarehouse
GO
CREATE PROCEDURE [dbo].[USP_GetItemsAttrWarehouse]                              
@numItemCode as numeric(9)=0,          
@strAtrr as varchar(100),          
@numWarehouseItemID as numeric(9)=0                            
as                              
                              
                          
                              
declare @bitSerialize as bit                      
declare @str as varchar(2000)                    
set @str=''                       
                        
                        
declare @numItemGroupID as numeric(9)                        
                        
select @numItemGroupID=numItemGroup,@bitSerialize=bitSerialized from Item where numItemCode=@numItemCode                           
              
                        
                       
select numWareHouseItemID,vcWarehouse,W.numWareHouseID,isnull(numOnHand,0) as [OnHand] ,isnull(numOnOrder,0) as [OnOrder] ,isnull(numReorder,0) as [Reorder] ,isnull(numAllocation,0) as [Allocation] ,isnull(numBackOrder,0) as [BackOrder],            
 0 as Op_Flag from WareHouseItems                           
 join Warehouses W                             
 on W.numWareHouseID=WareHouseItems.numWareHouseID                                    
 where numWarehouseItemID=@numWarehouseItemID          
          
if @bitSerialize=1          
begin          
declare @strSQL varchar(1000)          
set @strSQL=''          
if @strAtrr!=''            
begin            
            
 Declare @Cnt int            
   Set @Cnt = 1            
  declare @SplitOn char(1)            
  set @SplitOn=','            
  set @strSQL='SELECT recid            
     FROM CFW_Fld_Values_Serialized_Items where  bitSerialized ='+ convert(varchar(1),@bitSerialize)            
   While (Charindex(@SplitOn,@strAtrr)>0)            
   Begin            
    if  @Cnt=1 set @strSQL=@strSQL+' and fld_value=''' + (ltrim(rtrim(Substring(@strAtrr,1,Charindex(@SplitOn,@strAtrr)-1))))+''''            
    else set @strSQL=@strSQL+' or fld_value=''' + (ltrim(rtrim(Substring(@strAtrr,1,Charindex(@SplitOn,@strAtrr)-1))))+''''            
    Set @strAtrr = Substring(@strAtrr,Charindex(@SplitOn,@strAtrr)+1,len(@strAtrr))            
    Set @Cnt = @Cnt + 1            
   End            
  if  @Cnt=1 set @strSQL=@strSQL+' and fld_value=''' + @strAtrr + ''' GROUP BY recid            
   HAVING count(*) > '+convert(varchar(10), @Cnt-1)             
    else set @strSQL=@strSQL+' or fld_value=''' + @strAtrr + ''' GROUP BY recid            
   HAVING count(*) > '+convert(varchar(10), @Cnt-1)            
             
            
            
end           
          
          
 --Create a Temporary table to hold data                                                            
 Create table #tempTable ( ID INT IDENTITY PRIMARY KEY,                                                                      
  numCusFlDItemID numeric(9)                                                         
  )                         
                         
 insert into #tempTable                         
 (numCusFlDItemID)                                                            
 select distinct(numOppAccAttrID) from ItemGroupsDTL where numItemGroupID=@numItemGroupID and tintType=2                    
                           
                         
  declare @ID as numeric(9)                        
  declare @numCusFlDItemID as varchar(20)                        
  declare @fld_label as varchar(100)                        
  set @ID=0                        
  select top 1 @ID=ID,@numCusFlDItemID=numCusFlDItemID,@fld_label=fld_label from #tempTable                         
  join CFW_Fld_Master on numCusFlDItemID=Fld_ID                        
                          
  while @ID>0                        
  begin                        
                           
    set @str=@str+',  dbo.GetCustFldItems('+@numCusFlDItemID+',9,WDTL.numWareHouseItemID,0) as ['+ @fld_label+']'                                        
                           
    select top 1 @ID=ID,@numCusFlDItemID=numCusFlDItemID,@fld_label=fld_label from #tempTable                         
    join CFW_Fld_Master on numCusFlDItemID=Fld_ID and ID >@ID                        
    if @@rowcount=0 set @ID=0                        
                           
  end          
          
          
 set @str='select numWareHouseItmsDTLID,WDTL.numWareHouseItemID, vcSerialNo,0 as Op_Flag,WDTL.vcComments as Comments'+ case when @bitSerialize=1 then @str else '' end +' from WareHouseItmsDTL WDTL                             
 join WareHouseItems WI                             
 on WDTL.numWareHouseItemID=WI.numWareHouseItemID                              
 where ISNULL(WDTL.numQty,0)>0 and WI.numWarehouseItemID='+ convert(varchar(15),@numWarehouseItemID)              
    if @strAtrr!=''  set @str=@str+' and  numWareHouseItmsDTLID in ('+ @strSQL +')'          
                        
 print @str                       
 exec (@str)          
          
          
                      
 select Fld_label,fld_id,fld_type,numlistid,vcURL from #tempTable                         
  join CFW_Fld_Master on numCusFlDItemID=Fld_ID    
                      
 drop table #tempTable             
end            
else        
begin      
 select numWareHouseItmsDTLID,numWareHouseItemID, vcSerialNo,0 as Op_Flag,vcComments as Comments from    WareHouseItmsDTL where      numWareHouseItemID=@numWarehouseItemID    
     
end
GO
/****** Object:  StoredProcedure [dbo].[usp_GetOppserializedinditems_BizDoc]    Script Date: 07/26/2008 16:18:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
                           
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_GetOppserializedinditems_BizDoc')
DROP PROCEDURE usp_GetOppserializedinditems_BizDoc
GO
CREATE PROCEDURE [dbo].[usp_GetOppserializedinditems_BizDoc]                            
@numOppItemCode as numeric(9)=0,      
@numOppID as numeric(9)=0,  
@tintOppType  as TINYINT,
@numBizDocID as numeric(9)=0
                         
as      
declare @numWareHouseItemID as numeric(9)      
declare @numItemID as numeric(9)      
declare @bitSerialize as bit                    
declare @bitLotNo as bit                    

declare @str as varchar(2000)  
declare @strSQL as varchar(2000)                       
declare @ColName as varchar(50)                    
set @str=''        
      
select @numWareHouseItemID=numWarehouseItmsID,@numItemID=numItemID from   OpportunityItems      
join WareHouseItems       
on OpportunityItems.numWarehouseItmsID= WareHouseItems.numWareHouseItemID      
where  numOppItemTcode=@numOppItemCode         
                      
                      
declare @numItemGroupID as numeric(9)                      
                      
select @numItemGroupID=numItemGroup,@bitSerialize=ISNULL(bitSerialized,0),@bitLotNo=ISNULL(bitLotNo,0) from Item where numItemCode=@numItemID                      
set @ColName='numWareHouseItemID,0'                  
            
--Create a Temporary table to hold data                                                          
Create table #tempTable ( ID INT IDENTITY PRIMARY KEY,                                                                    
 numCusFlDItemID numeric(9)                                                       
 )                       
                      
insert into #tempTable                       
(numCusFlDItemID)                                                          
select numOppAccAttrID from ItemGroupsDTL where numItemGroupID=@numItemGroupID and tintType=2     
              
 declare @ID as numeric(9)                      
 declare @numCusFlDItemID as varchar(20)                      
 declare @fld_label as varchar(100),@fld_type as varchar(100)                              
 set @ID=0                      
 select top 1 @ID=ID,@numCusFlDItemID=numCusFlDItemID,@fld_label=fld_label,@fld_type=fld_type from #tempTable                       
 join CFW_Fld_Master on numCusFlDItemID=Fld_ID                                   
 while @ID>0                      
 begin                                    
   set @str=@str+',  dbo.GetCustFldItems('+@numCusFlDItemID+',9,'+@ColName+') as ['+ @fld_label+']'                                      
    
   set @str=@str+',  dbo.GetCustFldItemsValue('+@numCusFlDItemID+',9,'+@ColName+','''+@fld_type+''') as ['+ @fld_label+'Value]'                                        
                    
   select top 1 @ID=ID,@numCusFlDItemID=numCusFlDItemID,@fld_label=fld_label from #tempTable                       
   join CFW_Fld_Master on numCusFlDItemID=Fld_ID and ID >@ID                      
   if @@rowcount=0 set @ID=0                      
                        
 end        
      
select numWareHouseItemID,vcWareHouse,
--CAST((dbo.fn_UOMConversion(ISNULL(I.numBaseUnit,0),Opp.numItemCode,I.numDomainId,ISNULL(opp.numUOMId,0)) * Opp.numUnitHour) as numeric(18,0)) as numUnitHour
ISNULL(u.vcUnitName,'') vcBaseUOMName
,@bitSerialize AS bitSerialize,@bitLotNo AS bitLotNo
--,isnull(OBI.numUnitHour,0) as numUnitHour 
from OpportunityItems Opp     
join WareHouseItems on Opp.numWarehouseItmsID=WareHouseItems.numWareHouseItemID      
join Warehouses on Warehouses.numWareHouseID=WareHouseItems.numWareHouseID 
 join item I on Opp.numItemCode=I.numItemcode  
-- LEFT join OpportunityBizDocs OB on OB.numOppId=Opp.numOppId AND OB.numOppBizDocsId=@numBizDocID 
-- left join OpportunityBizDocItems OBI
--	on OB.numOppBizDocsId=OBI.numOppBizDocId and OBI.numOppItemID=Opp.numoppitemtCode 
 LEFT JOIN  UOM u ON u.numUOMId = I.numBaseUnit       
where Opp.numOppItemTcode=@numOppItemCode    
      
       
      
           
 set @strSQL='select WareHouseItmsDTL.numWareHouseItmsDTLID,numWareHouseItemID,vcSerialNo,0 as Op_Flag,
  isnull(WareHouseItmsDTL.numQty,0) as TotalQty,isnull(OppWarehouseSerializedItem.numQty,0) as UsedQty
  '+  @str  +',1 as bitAdded
 from   OppWarehouseSerializedItem      
 join WareHouseItmsDTL      
 on WareHouseItmsDTL.numWareHouseItmsDTLID= OppWarehouseSerializedItem.numWarehouseItmsDTLID                           
 where numOppID='+ convert(varchar(15),@numOppID)+' and  numWareHouseItemID='+ convert(varchar(15),@numWareHouseItemID) +' and numOppBizDocsId=' + convert(varchar(15),@numBizDocID)                        
 print @strSQL                     
 exec (@strSQL)     
  
  
select Fld_label,fld_id,fld_type,numlistid,vcURL from #tempTable                       
 join CFW_Fld_Master on numCusFlDItemID=Fld_ID                    
drop table #tempTable   
  
if @tintOppType=1  
begin   
 set @strSQL='select numWareHouseItmsDTLID,numWareHouseItemID,vcSerialNo,0 as Op_Flag,
  isnull(numQty,0) as TotalQty,
 cast(0 as numeric(9,0)) as UsedQty '+  @str  +',0 as bitAdded
 from   WareHouseItmsDTL   
    where ISNULL(numQty,0) > 0 AND numWareHouseItemID='+ convert(varchar(15),@numWareHouseItemID) +'  
    and numWareHouseItmsDTLID not in(select numWarehouseItmsDTLID from OppWarehouseSerializedItem where numOppID='+ convert(varchar(15),@numOppID)+' and  numWarehouseItmsID='+ convert(varchar(15),@numWareHouseItemID) +' and numOppBizDocsId=' + convert(varchar(15),@numBizDocID) +')'                    
 print @strSQL                     
 exec (@strSQL)   
end
GO
/****** Object:  StoredProcedure [dbo].[USP_GetOppWareHouseItemAttributes]    Script Date: 07/26/2008 16:18:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetOppWareHouseItemAttributesIds')
DROP PROCEDURE USP_GetOppWareHouseItemAttributesIds
GO
CREATE PROCEDURE [dbo].[USP_GetOppWareHouseItemAttributesIds]
@numRecID as numeric(9)=0
AS
BEGIN
	SELECT dbo.fn_GetAttributesIds(@numRecID)
END
GO
/****** Object:  StoredProcedure [dbo].[usp_GetRMASerializedindItems]    Script Date: 07/26/2008 16:18:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
                           
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetRMASerializedindItems')
DROP PROCEDURE USP_GetRMASerializedindItems
GO
CREATE PROCEDURE [dbo].[USP_GetRMASerializedindItems]                            
@numDomainId as numeric(9)=0,      
@numReturnHeaderID as numeric(9)=0,  
@numReturnItemID  as numeric(9)=0
                         
as      
BEGIN
	
DECLARE @tintReturnType TINYINT,@numOppId NUMERIC(18)
SELECT @tintReturnType=tintReturnType,@numOppId=ISNULL(numOppId,0) FROM ReturnHeader WHERE numDomainId=@numDomainId AND numReturnHeaderID=@numReturnHeaderID
	
declare @numWareHouseItemID as numeric(9),@numItemID as numeric(9),@numOppItemCode AS NUMERIC(9),@numUnitHour FLOAT
declare @bitSerialize as BIT,@bitLotNo as BIT                     

declare @str as varchar(2000),@strSQL as varchar(2000),@ColName as varchar(50)  
set @str=''        
      
select @numWareHouseItemID=OI.numWarehouseItmsID,@numItemID=WI.numItemID,@numOppItemCode=OI.numoppitemtCode,@numUnitHour=RI.numUnitHour from 
ReturnItems RI JOIN OpportunityItems OI ON RI.numOppItemID=OI.numoppitemtCode
join WareHouseItems WI on OI.numWarehouseItmsID= WI.numWareHouseItemID      
where  RI.numReturnHeaderID=@numReturnHeaderID AND RI.numReturnItemID=@numReturnItemID    
AND OI.numOppId=@numOppId    
                      
declare @numItemGroupID as numeric(9)                      
                      
select @numItemGroupID=numItemGroup,@bitSerialize=ISNULL(bitSerialized,0),@bitLotNo=ISNULL(bitLotNo,0) 
from Item where numItemCode=@numItemID                      

set @ColName='numWareHouseItemID,0'                  
            
--Create a Temporary table to hold data                                                          
Create table #tempTable ( ID INT IDENTITY PRIMARY KEY,                                                                    
 numCusFlDItemID numeric(9)                                                       
 )                       
                      
insert into #tempTable                       
(numCusFlDItemID)                                                          
select numOppAccAttrID from ItemGroupsDTL where numItemGroupID=@numItemGroupID and tintType=2     
              
 declare @ID as numeric(9)                      
 declare @numCusFlDItemID as varchar(20)                      
 declare @fld_label as varchar(100),@fld_type as varchar(100)                              
 set @ID=0                      
 select top 1 @ID=ID,@numCusFlDItemID=numCusFlDItemID,@fld_label=fld_label,@fld_type=fld_type from #tempTable                       
 join CFW_Fld_Master on numCusFlDItemID=Fld_ID                                   
 while @ID>0                      
 begin                                    
   set @str=@str+',  dbo.GetCustFldItems('+@numCusFlDItemID+',9,'+@ColName+') as ['+ @fld_label+']'                                      
    
   set @str=@str+',  dbo.GetCustFldItemsValue('+@numCusFlDItemID+',9,'+@ColName+','''+@fld_type+''') as ['+ @fld_label+'Value]'                                        
                    
   select top 1 @ID=ID,@numCusFlDItemID=numCusFlDItemID,@fld_label=fld_label from #tempTable                       
   join CFW_Fld_Master on numCusFlDItemID=Fld_ID and ID >@ID                      
   if @@rowcount=0 set @ID=0                      
                        
 end        
      
select numWareHouseItemID,vcWareHouse,
ISNULL(u.vcUnitName,'') vcBaseUOMName
,@bitSerialize AS bitSerialize,@bitLotNo AS bitLotNo,I.vcItemName,@numUnitHour AS numUnitHour
from OpportunityItems Opp     
join WareHouseItems on Opp.numWarehouseItmsID=WareHouseItems.numWareHouseItemID      
join Warehouses on Warehouses.numWareHouseID=WareHouseItems.numWareHouseID 
 join item I on Opp.numItemCode=I.numItemcode  
 LEFT JOIN  UOM u ON u.numUOMId = I.numBaseUnit       
where Opp.numOppItemTcode=@numOppItemCode    
      
      
      
 set @strSQL='select WID.numWareHouseItmsDTLID,numWareHouseItemID,vcSerialNo,0 as Op_Flag,
  isnull(OSI.numQty,0) 
  as TotalQty,0 as UsedQty
  '+  @str  +',0 as bitAdded
 from OppWarehouseSerializedItem OSI join WareHouseItmsDTL WID     
 on WID.numWareHouseItmsDTLID= OSI.numWarehouseItmsDTLID                           
 where numOppID='+ convert(varchar(15),ISNULL(@numOppID,0))+' and numWareHouseItemID='+ convert(varchar(15),ISNULL(@numWareHouseItemID,0)) 
 print @strSQL                     
 exec (@strSQL)     
  
  
select Fld_label,fld_id,fld_type,numlistid,vcURL from #tempTable                       
 join CFW_Fld_Master on numCusFlDItemID=Fld_ID                    
drop table #tempTable   
  
END
GO

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetSitesDetail')
DROP PROCEDURE USP_GetSitesDetail
GO
CREATE PROCEDURE [dbo].[USP_GetSitesDetail] @numSiteID NUMERIC(9)
AS 
    SELECT  S.[numDomainID],
            D.[numDivisionID],
            ISNULL(S.[bitIsActive], 1),
            ISNULL(E.[numDefaultWareHouseID],0) numDefaultWareHouseID,
            ISNULL(E.[numRelationshipId],0) numRelationshipId,
            ISNULL(E.[numProfileId],0) numProfileId,
            ISNULL(E.[bitHidePriceBeforeLogin],0) AS bitHidePriceBeforeLogin,
			dbo.fn_GetContactEmail(1,0,numAdminID) AS DomainAdminEmail,
			S.vcLiveURL,
			ISNULL(E.bitAuthOnlyCreditCard,0) bitAuthOnlyCreditCard,
			ISNULL(D.numDefCountry,0) numDefCountry,
			ISNULL(E.bitSendEMail,0) bitSendMail,
			ISNULL((SELECT numAuthoritativeSales FROM dbo.AuthoritativeBizDocs WHERE numDomainId=S.numDomainID),0) AuthSalesBizDoc,
			ISNULL((SELECT TOP 1 numBizDocId FROM dbo.eCommercePaymentConfig WHERE numSiteID=@numSiteID AND numPaymentMethodId = 27261 AND bitEnable =1 ),0) numCreditTermBizDocID,
			ISNULL((SELECT TOP 1 numBizDocId FROM dbo.eCommercePaymentConfig WHERE numSiteID=@numSiteID AND numPaymentMethodId =1 AND bitEnable =1 ),0) numCreditCardBizDocID,
			ISNULL((SELECT TOP 1 numBizDocId FROM dbo.eCommercePaymentConfig WHERE numSiteID=@numSiteID AND numPaymentMethodId = 31488 AND bitEnable =1 ),0) numGoogleCheckoutBizDocID,
ISNULL((SELECT TOP 1 numBizDocId FROM dbo.eCommercePaymentConfig WHERE numSiteID=@numSiteID AND numPaymentMethodId = 35141 AND bitEnable =1 ),0) numPaypalCheckoutBizDocID,
ISNULL((SELECT TOP 1 numBizDocId FROM dbo.eCommercePaymentConfig WHERE numSiteID=@numSiteID AND numPaymentMethodId = 84 AND bitEnable =1 ),0) numSalesInquiryBizDocID,
			ISNULL(D.bitSaveCreditCardInfo,0) bitSaveCreditCardInfo,
			ISNULL(bitOnePageCheckout,0) bitOnePageCheckout,
			ISNULL(D.tintBaseTaxOnArea,0) tintBaseTaxOnArea,
			ISNULL(D.numShippingServiceItemID,0) numShippingServiceItemID,
			ISNULL(D.bitAutolinkUnappliedPayment,0) bitAutolinkUnappliedPayment,
			ISNULL(D.numDiscountServiceItemID,0) numDiscountServiceItemID,
			ISNULL(D.tintBaseTax,0) AS [tintBaseTax],
			ISNULL(E.bitSkipStep2,0) AS [bitSkipStep2],
			ISNULL(E.bitDisplayCategory,0) AS [bitDisplayCategory],
			ISNULL(E.bitShowPriceUsingPriceLevel,0) AS [bitShowPriceUsingPriceLevel],
			ISNULL(E.[bitPreSellUp],0) [bitPreSellUp],
			ISNULL(E.[bitPostSellUp],0) [bitPostSellUp],
			ISNULL(E.[dcPostSellDiscount],0) [dcPostSellDiscount],
			ISNULL(E.numDefaultClass,0) numDefaultClass,
			E.vcPreSellUp,E.vcPostSellUp,ISNULL(E.vcRedirectThankYouUrl,'') AS vcRedirectThankYouUrl
    FROM    Sites S
            INNER JOIN [Domain] D ON D.[numDomainId] = S.[numDomainID]
            LEFT OUTER JOIN [eCommerceDTL] E ON D.[numDomainId] = E.[numDomainId] AND S.numSiteID = E.numSiteId
    WHERE   S.[numSiteID] = @numSiteID
 
 

/****** Object:  StoredProcedure [dbo].[USP_GetWareHouseItems]    Script Date: 07/26/2008 16:18:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--exec USP_GetWareHouseItems 6                            
--created by anoop jayaraj                              
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getwarehouseitems')
DROP PROCEDURE usp_getwarehouseitems
GO
CREATE PROCEDURE [dbo].[USP_GetWareHouseItems]                              
@numItemCode as numeric(9)=0,
@byteMode as tinyint=0,
@numWarehouseItemID AS NUMERIC(18,0) = NULL                             
as                              
                              
                          
                              
declare @bitSerialize as bit                      
declare @str as nvarchar(max)                 
declare @str1 as nvarchar(max)               
declare @ColName as varchar(500)                      
set @str=''                       

DECLARE @numDomainID AS INT 
DECLARE @numBaseUnit AS NUMERIC(18,0)
DECLARE @vcUnitName as VARCHAR(100) 
DECLARE @numSaleUnit AS NUMERIC(18,0)
DECLARE @vcSaleUnitName as VARCHAR(100) 
DECLARE @numPurchaseUnit AS NUMERIC(18,0)
DECLARE @vcPurchaseUnitName as VARCHAR(100)
DECLARE @bitLot AS BIT                     
DECLARE @bitKitParent BIT
DECLARE @bitMatrix BIT
declare @numItemGroupID as numeric(9)                        
                        
select 
	@numDomainID=numDomainID,
	@numBaseUnit = ISNULL(numBaseUnit,0),
	@numSaleUnit = ISNULL(numSaleUnit,0),
	@numPurchaseUnit = ISNULL(numPurchaseUnit,0),
	@numItemGroupID=numItemGroup,
	@bitLot = bitLotNo,
	@bitSerialize=CASE WHEN bitSerialized=0 THEN bitLotNo ELSE bitSerialized END,
	@bitKitParent = ( CASE 
						WHEN ISNULL(bitKitParent,0) = 1 AND ISNULL(bitAssembly,0) = 1 THEN 0
                        WHEN ISNULL(bitKitParent,0) = 1 THEN 1
                        ELSE 0
                     END )
	,@bitMatrix=bitMatrix
FROM 
	Item 
WHERE 
	numItemCode=@numItemCode     
	
SELECT @vcUnitName=ISNULL(vcUnitName,'-') FROM UOM u WHERE u.numUOMId=@numBaseUnit
SELECT @vcSaleUnitName=ISNULL(vcUnitName,'-') FROM UOM u WHERE u.numUOMId=@numSaleUnit
SELECT @vcPurchaseUnitName=ISNULL(vcUnitName,'-') FROM UOM u WHERE u.numUOMId=@numPurchaseUnit
	
DECLARE @numSaleUOMFactor AS DECIMAL(28,14)
DECLARE @numPurchaseUOMFactor AS DECIMAL(28,14)

SELECT @numSaleUOMFactor = dbo.fn_UOMConversion(@numBaseUnit,@numItemCode,@numDOmainID,@numSaleUnit)
SELECT @numPurchaseUOMFactor = dbo.fn_UOMConversion(@numBaseUnit,@numItemCode,@numDOmainID,@numPurchaseUnit)


             
              
--Create a Temporary table to hold data                                                            
create table #tempTable 
( 
	ID INT IDENTITY PRIMARY KEY,                                                                      
	numCusFlDItemID NUMERIC(9),
	Fld_Value VARCHAR(20)                                                         
)                


DECLARE @ID AS NUMERIC(18,0)  = 0                       
DECLARE @numCusFlDItemID AS VARCHAR(20)                        
DECLARE @fld_label AS VARCHAR(100),@fld_type AS VARCHAR(100)                        

         

IF @bitMatrix = 1
BEGIN
	SET @ColName='I.numItemCode,1' 

	INSERT INTO #tempTable                         
	(
		numCusFlDItemID
		,Fld_Value
	)                                                            
	SELECT DISTINCT
		FLD_ID
		,CAST(FLD_Value AS VARCHAR)
	FROM
		ItemAttributes
	WHERE
		numDomainID=@numDomainID
		AND numItemCode = @numItemCode

	SELECT TOP 1 
		@ID=ID
		,@numCusFlDItemID=numCusFlDItemID
		,@fld_label=fld_label
		,@fld_type=fld_type 
	FROM 
		#tempTable                         
	JOIN 
		CFW_Fld_Master 
	ON 
		numCusFlDItemID=Fld_ID                        
                         
	 WHILE @ID>0                        
	 BEGIN                        
                          
		SET @str = @str+',  dbo.GetCustFldItems('+@numCusFlDItemID+',9,'+@ColName+') as ['+ @fld_label+']'
	
		IF @byteMode=1                                        
			SET @str = @str+',  dbo.GetCustFldItemsValue('+@numCusFlDItemID+',9,'+@ColName+','''+@fld_type+''') as ['+ @fld_label+'Value]'                                        
                          
	   select top 1 @ID=ID,@numCusFlDItemID=numCusFlDItemID,@fld_label=fld_label from #tempTable                         
	   join CFW_Fld_Master on numCusFlDItemID=Fld_ID and ID >@ID                        
	   if @@rowcount=0 set @ID=0                        
                          
	 END
END
ELSE
BEGIN
	SET @ColName='WareHouseItems.numWareHouseItemID,0' 

	INSERT INTO #tempTable                         
	(
		numCusFlDItemID
		,Fld_Value
	)                                                            
	SELECT DISTINCT
		numOppAccAttrID
		,''
	FROM 
		ItemGroupsDTL 
	WHERE 
		numItemGroupID=@numItemGroupID 
		AND tintType=2    

	SELECT TOP 1 
		@ID=ID
		,@numCusFlDItemID=numCusFlDItemID
		,@fld_label=fld_label
		,@fld_type=fld_type 
	FROM 
		#tempTable                         
	JOIN 
		CFW_Fld_Master 
	ON 
		numCusFlDItemID=Fld_ID                        
                         
	 WHILE @ID>0                        
	 BEGIN                        
                          
		SET @str = @str+',  dbo.GetCustFldItems('+@numCusFlDItemID+',9,'+@ColName+') as ['+ @fld_label+']'
	
		IF @byteMode=1                                        
			SET @str = @str+',  dbo.GetCustFldItemsValue('+@numCusFlDItemID+',9,'+@ColName+','''+@fld_type+''') as ['+ @fld_label+'Value]'                                        
                          
	   select top 1 @ID=ID,@numCusFlDItemID=numCusFlDItemID,@fld_label=fld_label from #tempTable                         
	   join CFW_Fld_Master on numCusFlDItemID=Fld_ID and ID >@ID                        
	   if @@rowcount=0 set @ID=0                        
                          
	 END
END          
                   
                          
                 
                      
                         
                       
--      

DECLARE @KitOnHand AS NUMERIC(9);SET @KitOnHand=0

--IF @bitKitParent=1
	--SELECT @KitOnHand=ISNULL(dbo.fn_GetKitInventory(@numItemCode),0)          
  
set @str1='select '

IF @byteMode=1                                        
		set @str1 =@str1 +'I.numItemCode,'
	
set @str1 =@str1 + 'numWareHouseItemID,WareHouseItems.numWareHouseID,
ISNULL(vcWarehouse,'''') + (CASE WHEN ISNULL(WL.vcLocation,'''') = '''' THEN '''' ELSE '', '' + isnull(WL.vcLocation,'''') END) vcWarehouse,
ISNULL(vcWarehouse,'''') AS vcExternalLocation,
(CASE WHEN WareHouseItems.numWLocationID = -1 THEN ''Global'' ELSE ISNULL(WL.vcLocation,'''') END) AS vcInternalLocation,
W.numWareHouseID,
Case when @bitKitParent=1 then CAST(ISNULL(dbo.fn_GetKitInventoryByWarehouse(I.numItemCode, W.numWareHouseID),0) AS DECIMAL(18,2)) ELSE CAST(ISNULL(numOnHand,0) AS FLOAT) END AS [OnHand],
CAST((@numPurchaseUOMFactor * Case when @bitKitParent=1 THEN ISNULL(dbo.fn_GetKitInventoryByWarehouse(I.numItemCode, W.numWareHouseID),0) ELSE isnull(numOnHand,0) end) as FLOAT) as PurchaseOnHand,
CAST((@numSaleUOMFactor * Case when @bitKitParent=1 THEN ISNULL(dbo.fn_GetKitInventoryByWarehouse(I.numItemCode, W.numWareHouseID),0) ELSE isnull(numOnHand,0) end) as FLOAT) as SalesOnHand,
Case when @bitKitParent=1 THEN 0 ELSE CAST(ISNULL(numReorder,0) AS FLOAT) END as [Reorder] 
,Case when @bitKitParent=1 THEN 0 ELSE CAST(ISNULL(numOnOrder,0) AS FLOAT) END as [OnOrder]
,Case when @bitKitParent=1 THEN 0 ELSE CAST(ISNULL(numAllocation,0) AS FLOAT) END as [Allocation] 
,Case when @bitKitParent=1 THEN 0 ELSE CAST(ISNULL(numBackOrder,0) AS FLOAT) END as [BackOrder],
CAST((@numSaleUOMFactor * Case when @bitKitParent=1 THEN ISNULL(dbo.fn_GetKitInventoryByWarehouse(I.numItemCode, W.numWareHouseID),0) ELSE isnull(numOnHand,0) end) as numeric(18,2)) as OnHandUOM,
CAST((@numPurchaseUOMFactor * Case when @bitKitParent=1 THEN 0 ELSE isnull(numOnOrder,0) END) AS DECIMAL(18,2)) as OnOrderUOM,
CAST((@numPurchaseUOMFactor * Case when @bitKitParent=1 THEN 0 ELSE isnull(numReorder,0) END) AS DECIMAL(18,2)) as ReorderUOM,
CAST((@numSaleUOMFactor * Case when @bitKitParent=1 THEN 0 ELSE isnull(numAllocation,0) END) AS DECIMAL(18,2)) as AllocationUOM,
CAST((@numSaleUOMFactor * Case when @bitKitParent=1 THEN 0 ELSE isnull(numBackOrder,0) END) AS DECIMAL(18,2)) as BackOrderUOM,
@vcUnitName As vcBaseUnit,
@vcSaleUnitName As vcSaleUnit,
@vcPurchaseUnitName As vcPurchaseUnit
,0 as Op_Flag , isnull(WL.vcLocation,'''') Location,isnull(WL.numWLocationID,0) as numWLocationID,
round(isnull(monWListPrice,0),2) Price,isnull(vcWHSKU,'''') as SKU,isnull(vcBarCode,'''') as BarCode '+ @str +'                   
,(SELECT CASE WHEN ISNULL(subI.bitKitParent,0) = 1 AND ISNULL((SELECT COUNT(*) FROM OpportunityItems 
															   WHERE OpportunityItems.numItemCode = WareHouseItems.numItemID
															   AND WareHouseItems.numWareHouseItemID = OpportunityItems.numWarehouseItmsID),0) = 0 THEN 1
			ELSE 0 
	    END 
FROM Item subI WHERE subI.numItemCode = WareHouseItems.numItemID) [IsDeleteKitWarehouse],@bitKitParent [bitKitParent],
(CASE WHEN (SELECT COUNT(*) FROM ItemDetails WHERE numChildItemID=WareHouseItems.numItemID AND numWareHouseItemId = WareHouseItems.numWareHouseItemID) > 0 THEN 1 ELSE 0 END) AS bitChildItemWarehouse
,' + (CASE WHEN @bitSerialize=1 THEN CONCAT('dbo.GetWarehouseSerialLot(',@numDomainID,',WareHouseItems.numWareHouseItemID,',@bitLot,')') ELSE '''''' END) + ' AS vcSerialLot,
CASE 
	WHEN ISNULL(I.numItemGroup,0) > 0 
	THEN dbo.fn_GetAttributes(WareHouseItems.numWareHouseItemID,0)
	ELSE ''''
END AS vcAttribute 
from WareHouseItems                           
join Warehouses W                             
on W.numWareHouseID=WareHouseItems.numWareHouseID 
left join WarehouseLocation WL on WL.numWLocationID =WareHouseItems.numWLocationID
join Item I on I.numItemCode=WareHouseItems.numItemID and WareHouseItems.numDomainId=I.numDomainId
where numItemID='+convert(varchar(15),@numItemCode) + ' AND (WareHouseItems.numWarehouseItemID = ' + CAST(@numWarehouseItemID AS VARCHAR(20)) + ' OR ' + CAST(ISNULL(@numWarehouseItemID,0) AS VARCHAR(20)) + ' = 0 )'              
   
print(@str1)           

EXECUTE sp_executeSQL @str1, N'@bitKitParent bit,@numSaleUOMFactor DECIMAL(28,14), @numPurchaseUOMFactor DECIMAL(28,14), @vcUnitName VARCHAR(100), @vcSaleUnitName VARCHAR(100), @vcPurchaseUnitName VARCHAR(100)', @bitKitParent, @numSaleUOMFactor, @numPurchaseUOMFactor, @vcUnitName, @vcSaleUnitName, @vcPurchaseUnitName
                       
                        
set @str1='select numWareHouseItmsDTLID,WDTL.numWareHouseItemID, vcSerialNo,WDTL.vcComments as Comments,WDTL.numQty, WDTL.numQty as OldQty,ISNULL(W.vcWarehouse,'''') + (CASE WHEN ISNULL(WL.vcLocation,'''') = '''' THEN '''' ELSE '', '' + isnull(WL.vcLocation,'''') END) vcWarehouse '+ case when @bitSerialize=1 then @str else '' end +'
,WDTL.dExpirationDate, WDTL.bitAddedFromPO
from WareHouseItmsDTL WDTL                             
join WareHouseItems                             
on WDTL.numWareHouseItemID=WareHouseItems.numWareHouseItemID                              
join Warehouses W                             
on W.numWareHouseID=WareHouseItems.numWareHouseID  
left join WarehouseLocation WL on WL.numWLocationID =WareHouseItems.numWLocationID
where ISNULL(numQty,0) > 0 and numItemID='+ convert(varchar(15),@numItemCode) + ' AND (WareHouseItems.numWarehouseItemID = ' + CAST(@numWarehouseItemID AS VARCHAR(20)) + ' OR ' + CAST(ISNULL(@numWarehouseItemID,0) AS VARCHAR(20)) + ' = 0 )'                          
print @str1                       
exec (@str1)                       
                      
                      
select Fld_label,fld_id,fld_type,numlistid,vcURL,Fld_Value from #tempTable                         
 join CFW_Fld_Master on numCusFlDItemID=Fld_ID                      
drop table #tempTable


-----------------For Kendo UI testting purpose
--create table #tempColumnConfig ( field varchar(100),                                                                      
-- title varchar(100) , format varchar(100)                                                      
-- )  
--
--insert into #tempColumnConfig
--	select 'vcWarehouse','WareHouse','' union all
--	select 'OnHand','On Hand','' union all
--	select 'Reorder','Re Order','' union all
--	select 'Allocation','Allocation','' union all
--	select 'BackOrder','Back Order','' union all
--	select 'Price','Price','{0:c}'
--
--select * from #tempColumnConfig
--
--drop table #tempColumnConfig

-----------------

--set @str1='select case when COUNT(*)=1 then MIN(numWareHouseItmsDTLID) else 0 end as numWareHouseItmsDTLID,MIN(WDTL.numWareHouseItemID) as numWareHouseItemID, vcSerialNo,0 as Op_Flag,WDTL.vcComments as Comments'+ case when @bitSerialize=1 then @str else '' end +',W.vcWarehouse,COUNT(*) Qty
--from WareHouseItmsDTL WDTL                             
--join WareHouseItems WI                             
--on WDTL.numWareHouseItemID=WI.numWareHouseItemID                              
--join Warehouses W                             
--on W.numWareHouseID=WI.numWareHouseID  
--where (tintStatus is null or tintStatus=0)  and  numItemID='+ convert(varchar(15),@numItemCode) +'
--GROUP BY vcSerialNo,WDTL.vcComments,W.vcWarehouse'
--                          
--print @str1                       
--exec (@str1)                       
              
GO
GO
/****** Object:  StoredProcedure [dbo].[USP_ItemDetails]    Script Date: 02/07/2010 15:31:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
                                         
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_Item_GetAttributes')
DROP PROCEDURE USP_Item_GetAttributes
GO
CREATE PROCEDURE [dbo].[USP_Item_GetAttributes]                                                  
	@numDomainID NUMERIC(18,0)
	,@numItemCode NUMERIC(18,0)
	,@numItemGroup AS NUMERIC(18,0)                                           
AS
BEGIN 
	SELECT 
		CFW_Fld_Master.Fld_label
		,CFW_Fld_Master.fld_id
		,CFW_Fld_Master.fld_type
		,CFW_Fld_Master.numlistid
		,CFW_Fld_Master.vcURL
		,ItemAttributes.FLD_Value 
		,COUNT(LD.numListItemID) numVariations
	FROM 
		CFW_Fld_Master 
	INNER JOIN
		ItemGroupsDTL 
	ON 
		CFW_Fld_Master.Fld_id = ItemGroupsDTL.numOppAccAttrID
		AND ItemGroupsDTL.tintType = 2
	LEFT JOIN
		ItemAttributes
	ON
		CFW_Fld_Master.Fld_id = ItemAttributes.FLD_ID
		AND ItemAttributes.numItemCode = @numItemCode
	LEFT JOIN
		ListDetails LD
	ON
		CFW_Fld_Master.numlistid = LD.numListID
	WHERE
		CFW_Fld_Master.numDomainID = @numDomainId
		AND ItemGroupsDTL.numItemGroupID = @numItemGroup
	GROUP BY
		CFW_Fld_Master.Fld_label
		,CFW_Fld_Master.fld_id
		,CFW_Fld_Master.fld_type
		,CFW_Fld_Master.numlistid
		,CFW_Fld_Master.vcURL
		,ItemAttributes.FLD_Value 
END
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_Item_GetVendors')
DROP PROCEDURE USP_Item_GetVendors
GO
CREATE PROCEDURE [dbo].[USP_Item_GetVendors]
(
	@numDomainID NUMERIC(18,0)
	,@numItemCode NUMERIC(18,0)
	,@ClientTimeZoneOffset AS INT
)
AS
BEGIN
	SELECT
		Vendor.numVendorID
		,ISNULL(CompanyInfo.vcCompanyName,'') vcCompanyName
		,ISNULL(Vendor.monCost,0) as monVendorCost
		,ISNULL(CAST(Vendor.intMinQty AS FLOAT),0) as monVendorCost
		,CASE WHEN LastOrder.vcPOppName IS NULL THEN '-' ELSE CONCAT('<a href=''#'' onclick=''openVendorPriceHistory(',Vendor.numVendorID,');''>',monUnitCOst,'</a> (',CAST(numQty AS FLOAT),') ',dtOrderedDate,' ', vcPOppName) END vcLastOrder
	FROM
		Vendor
	INNER JOIN
		Item
	ON
		Vendor.numItemCode = Item.numItemCode
	INNER JOIN
		DivisionMaster 
	ON
		Vendor.numVendorID = DivisionMaster.numDivisionID
	INNER JOIN
		CompanyInfo
	ON
		DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
	OUTER APPLY
	(
		SELECT TOP 1
			OM.vcPOppName
			,dbo.fn_UOMConversion(OI.numUOMId,@numItemCode,@numItemCode,Item.numBaseUnit) numQty
			,dbo.FormatedDateFromDate(DATEADD(MINUTE,-@ClientTimeZoneOffset,OM.bintCreatedDate),@numDomainID) dtOrderedDate
			,CAST(ISNULL(OI.monPrice,0) AS MONEY) AS monUnitCOst
		FROM 
			OpportunityMaster OM
		INNER JOIN
			OpportunityItems OI
		ON
			OM.numOppId = OI.numOppId
		WHERE
			OM.numDomainId=@numDomainID
			AND OM.numDivisionId = Vendor.numVendorID
			AND OI.numItemCode = @numItemCode
			AND tintOppType = 2
			AND tintOppStatus = 1
			AND ISNULL(bitStockTransfer,0) = 0
		ORDER BY
			OM.numOppId DESC
	) AS LastOrder
	WHERE
		Vendor.numDomainID = @numDomainID
		AND Vendor.numItemCode = @numItemCode
END
GO
/****** Object:  StoredProcedure [dbo].[USP_ItemDetails]    Script Date: 02/07/2010 15:31:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created By Anoop Jayaraj                                          
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_itemdetails')
DROP PROCEDURE usp_itemdetails
GO
CREATE PROCEDURE [dbo].[USP_ItemDetails]                                                  
@numItemCode as numeric(9) ,          
@ClientTimeZoneOffset as int                                               
AS
BEGIN 
   DECLARE @bitItemIsUsedInOrder AS BIT=0
   DECLARE @bitOppOrderExists AS BIT = 0
   
   DECLARE @numDomainId AS NUMERIC
   DECLARE @numItemGroup AS NUMERIC(18,0)
   SELECT @numDomainId=numDomainId,@numItemGroup=numItemGroup FROM Item WHERE numItemCode=@numItemCode
   
	IF GETUTCDATE()<'2013-03-15 23:59:39'
		SET @bitItemIsUsedInOrder=0
	ELSE IF (SELECT COUNT(OI.numOppId) FROM dbo.OpportunityItems OI WHERE OI.numItemCode =@numItemCode) > 0
	BEGIN
		SET @bitItemIsUsedInOrder=1
		SET @bitOppOrderExists = 1
	END
	ELSE IF (SELECT COUNT(OI.numOppId) FROM dbo.OpportunityKitItems OI WHERE OI.numChildItemID =@numItemCode) > 0
	BEGIN
		SET @bitItemIsUsedInOrder=1
		SET @bitOppOrderExists = 1
	END
	ELSE IF (SELECT COUNT(OI.numOppId) FROM dbo.OpportunityKitChildItems OI WHERE OI.numItemID =@numItemCode) > 0
	BEGIN
		SET @bitItemIsUsedInOrder=1
		SET @bitOppOrderExists = 1
	END
	ELSE IF EXISTS(SELECT numItemID FROM dbo.General_Journal_Details WHERE numItemID= @numItemCode and numDomainId=@numDomainId AND chBizDocItems='IA1'   )
		set @bitItemIsUsedInOrder =1
                                               
	SELECT 
		I.numItemCode, vcItemName, ISNULL(txtItemDesc,'') txtItemDesc,CAST(ISNULL(IED.txtDesc,'')  AS VARCHAR(MAX)) vcExtendedDescToAPI, charItemType, 
		CASE 
			WHEN charItemType='P' AND ISNULL(bitAssembly,0) = 1 then 'Assembly'
			WHEN charItemType='P' AND ISNULL(bitKitParent,0) = 1 then 'Kit'
			WHEN charItemType='P' AND ISNULL(bitAsset,0) = 1 AND ISNULL(bitRental,0)=0 then 'Asset'
			WHEN charItemType='P' AND ISNULL(bitAsset,0) = 1 AND ISNULL(bitRental,0)=1 then 'Rental Asset'
			WHEN charItemType='P' AND ISNULL(bitSerialized,0) = 1 AND ISNULL(bitAsset,0) = 0  then 'Serialized'
			WHEN charItemType='P' AND ISNULL(bitSerialized,0) = 1 AND ISNULL(bitAsset,0) = 1 AND ISNULL(bitRental,0)=0 then 'Serialized Asset'
			WHEN charItemType='P' AND ISNULL(bitSerialized,0) = 1 AND ISNULL(bitAsset,0) = 1 AND ISNULL(bitRental,0)=1 then 'Serialized Rental Asset'
			WHEN charItemType='P' AND ISNULL(bitLotNo,0)=1 THEN 'Lot #'
			ELSE CASE WHEN charItemType='P' THEN 'Inventory' ELSE '' END
		END AS InventoryItemType
		,dbo.fn_GetItemChildMembershipCount(@numDomainId,I.numItemCode) AS numChildMembershipCount
		,CASE WHEN ISNULL(bitAssembly,0) = 1 THEN dbo.fn_GetAssemblyPossibleWOQty(I.numItemCode) ELSE 0 END AS numWOQty
		,CASE WHEN charItemType='P' THEN ISNULL((SELECT TOP 1 monWListPrice FROM WarehouseItems WHERE numDomainID=@numDomainID AND numItemID=@numItemCode AND ISNULL(monWListPrice,0) > 0),0) ELSE monListPrice END monListPrice,                   
		numItemClassification, isnull(bitTaxable,0) as bitTaxable, ISNULL(vcSKU,'') AS vcItemSKU, 
		(CASE WHEN I.numItemGroup > 0 THEN ISNULL(W.[vcWHSKU],'') ELSE ISNULL(vcSKU,'') END) AS vcSKU, 
		ISNULL(bitKitParent,0) as bitKitParent, V.numVendorID, V.monCost, I.numDomainID, numCreatedBy, bintCreatedDate, bintModifiedDate,numModifiedBy,
		(SELECT TOP 1 vcPathForImage FROM ItemImages WHERE numItemCode = @numItemCode AND bitDefault = 1 AND bitIsImage = 1 ) AS vcPathForImage ,
		(SELECT TOP 1 vcPathForTImage FROM ItemImages WHERE numItemCode = @numItemCode AND bitDefault = 1 AND bitIsImage = 1) AS  vcPathForTImage
		,ISNULL(bitSerialized,0) as bitSerialized, vcModelID,                   
		(SELECT 
			numItemImageId
			,vcPathForImage
			,vcPathForTImage
			,bitDefault
			,CASE WHEN bitdefault=1 THEN -1 ELSE ISNULL(intDisplayOrder,0) END AS intDisplayOrder 
		FROM 
			ItemImages  
		WHERE 
			numItemCode=@numItemCode 
		ORDER BY 
			CASE WHEN bitdefault=1 THEN -1 ELSE isnull(intDisplayOrder,0) END ASC 
		FOR XML AUTO,ROOT('Images')) AS xmlItemImages,
		(SELECT STUFF((SELECT ',' + CONVERT(VARCHAR(50) , numCategoryID)  FROM dbo.ItemCategory WHERE numItemID = I.numItemCode FOR XML PATH('')), 1, 1, '')) AS vcItemCategories ,
		numItemGroup, numCOGsChartAcntId, numAssetChartAcntId, numIncomeChartAcntId, (CASE WHEN ISNULL(I.bitVirtualInventory,0) = 1 THEN 0 ELSE monAverageCost END) AS monAverageCost,                   
		monCampaignLabourCost,dbo.fn_GetContactName(numCreatedBy)+' ,'+convert(varchar(20),DateAdd(minute, -@ClientTimeZoneOffset,bintCreatedDate )) as CreatedBy ,                                      
		dbo.fn_GetContactName(numModifiedBy)+' ,'+convert(varchar(20),DateAdd(minute, -@ClientTimeZoneOffset,bintModifiedDate )) as ModifiedBy,                      
		sum(numOnHand) as numOnHand,                      
		sum(numOnOrder) as numOnOrder,                      
		sum(numReorder)  as numReorder,                      
		sum(numAllocation)  as numAllocation,                      
		sum(numBackOrder)  as numBackOrder,                   
		isnull(fltWeight,0) as fltWeight,                
		isnull(fltHeight,0) as fltHeight,                
		isnull(fltWidth,0) as fltWidth,                
		isnull(fltLength,0) as fltLength,                
		isnull(bitFreeShipping,0) as bitFreeShipping,              
		isnull(bitAllowBackOrder,0) as bitAllowBackOrder,              
		isnull(vcUnitofMeasure,'Units') as vcUnitofMeasure,      
		isnull(bitShowDeptItem,0) bitShowDeptItem,      
		isnull(bitShowDeptItemDesc,0) bitShowDeptItemDesc,  
		isnull(bitCalAmtBasedonDepItems ,0) bitCalAmtBasedonDepItems,
		isnull(bitAssembly ,0) bitAssembly ,
		isnull(cast(numBarCodeId as varchar(50)),'') as  numBarCodeId ,
		isnull(I.vcManufacturer,'') as vcManufacturer,
		ISNULL(I.numBaseUnit,0) AS numBaseUnit,ISNULL(I.numPurchaseUnit,0) AS numPurchaseUnit,ISNULL(I.numSaleUnit,0) AS numSaleUnit,
		dbo.fn_GetUOMName(ISNULL(I.numBaseUnit,0)) AS vcBaseUnit,dbo.fn_GetUOMName(ISNULL(I.numPurchaseUnit,0)) AS vcPurchaseUnit,dbo.fn_GetUOMName(ISNULL(I.numSaleUnit,0)) AS vcSaleUnit,
		isnull(bitLotNo,0) as bitLotNo,
		ISNULL(IsArchieve,0) AS IsArchieve,
		case when charItemType='P' then 'Inventory Item' when charItemType='S' then 'Service' when charItemType='A' then 'Accessory' when charItemType='N' then 'Non-Inventory Item' end as ItemType,
		isnull(I.numItemClass,0) as numItemClass,
		ISNULL(tintStandardProductIDType,0) tintStandardProductIDType,
		ISNULL(vcExportToAPI,'') vcExportToAPI,
		ISNULL(numShipClass,0) numShipClass,ISNULL(I.bitAllowDropShip,0) AS bitAllowDropShip,
		ISNULL(@bitItemIsUsedInOrder,0) AS bitItemIsUsedInOrder,
		ISNULL((SELECT COUNT(*) FROM ItemDetails WHERE numChildItemID = I.numItemCode),0) AS intParentKitItemCount,
		ISNULL(I.bitArchiveItem,0) AS [bitArchiveItem],
		ISNULL(W.numWareHouseID,0) AS [numWareHouseID],
		ISNULL(W.numWareHouseItemID,0) AS [numWareHouseItemID],
		ISNULL((SELECT COUNT(WI.[numWareHouseItemID]) FROM [dbo].[WareHouseItems] WI WHERE WI.[numItemID] = I.numItemCode AND WI.[numItemID] = @numItemCode),0) AS [intTransCount],
		ISNULL(I.bitAsset,0) AS [bitAsset],
		ISNULL(I.bitRental,0) AS [bitRental],
		ISNULL(W.vcBarCode,ISNULL(I.[numBarCodeId],'')) AS [vcBarCode],
		ISNULL((SELECT COUNT(*) FROM ItemUOMConversion WHERE numItemCode=I.numItemCode),0) AS tintUOMConvCount,
		ISNULL(I.bitVirtualInventory,0) bitVirtualInventory ,
		ISNULL(I.bitContainer,0) bitContainer
		,ISNULL(I.numContainer,0) numContainer
		,ISNULL(I.numNoItemIntoContainer,0) numNoItemIntoContainer
		,ISNULL(I.bitMatrix,0) AS bitMatrix
		,ISNULL(@bitOppOrderExists,0) AS bitOppOrderExists
	FROM 
		Item I       
	left join  
		WareHouseItems W                  
	on 
		W.numItemID=I.numItemCode                
	LEFT JOIN 
		ItemExtendedDetails IED 
	ON 
		I.numItemCode = IED.numItemCode
	LEFT JOIN 
		Vendor V 
	ON 
		I.numVendorID = V.numVendorID AND I.numItemCode = V.numItemCode          
	WHERE 
		I.numItemCode=@numItemCode  
	GROUP BY 
		I.numItemCode, vcItemName, txtItemDesc, charItemType, monListPrice,                   
		numItemClassification, bitTaxable,vcSKU,[W].[vcWHSKU] , bitKitParent,V.numVendorID, V.monCost, I.numDomainID,               
		numCreatedBy, bintCreatedDate, bintModifiedDate,numContainer,   numNoItemIntoContainer,                
		numModifiedBy,bitAllowBackOrder, bitSerialized, vcModelID,                   
		numItemGroup, numCOGsChartAcntId, numAssetChartAcntId, numIncomeChartAcntId, monAverageCost, I.bitVirtualInventory,                   
		monCampaignLabourCost,fltWeight,fltHeight,fltWidth,fltLength,bitFreeShipping,vcUnitofMeasure,bitShowDeptItemDesc,bitShowDeptItem,bitCalAmtBasedonDepItems,bitAssembly,numBarCodeId,I.vcManufacturer
		,I.numBaseUnit,I.numPurchaseUnit,I.numSaleUnit,I.bitContainer,I.bitLotNo,I.IsArchieve,I.numItemClass,I.tintStandardProductIDType,I.vcExportToAPI,I.numShipClass,
		CAST( ISNULL(IED.txtDesc,'')  AS VARCHAR(MAX) ),I.bitAllowDropShip,I.bitArchiveItem,I.bitAsset,I.bitRental,W.[numWarehouseID],W.numWareHouseItemID,W.[vcBarCode],I.bitMatrix
END
--Created By Anoop Jayaraj          
--exec USP_ItemDetailsForEcomm @numItemCode=859064,@numWareHouseID=1074,@numDomainID=172,@numSiteId=90
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_itemdetailsforecomm')
DROP PROCEDURE USP_ItemDetailsForEcomm
GO
CREATE PROCEDURE [dbo].[USP_ItemDetailsForEcomm]
@numItemCode as numeric(9),          
@numWareHouseID as numeric(9),    
@numDomainID as numeric(9),
@numSiteId as numeric(9),
@vcCookieId as VARCHAR(MAX)=null,
@numUserCntID AS NUMERIC(9)=0                                             
as     
/*Removed warehouse parameter, and taking Default warehouse by default from DB*/
    
declare @bitShowInStock as bit        
declare @bitShowQuantity as BIT
declare @bitAutoSelectWarehouse as bit        
--declare @tintColumns as tinyint        
DECLARE @numDefaultWareHouseID as numeric(9)

set @bitShowInStock=0        
set @bitShowQuantity=0        
--set @tintColumns=1        
        
select @bitShowInStock=isnull(bitShowInStock,0),@bitShowQuantity=isnull(bitShowQOnHand,0),@bitAutoSelectWarehouse=ISNULL(bitAutoSelectWarehouse,0)--,@tintColumns=isnull(tintItemColumns,1)        
from eCommerceDTL where numDomainID=@numDomainID
IF @numWareHouseID=0
BEGIN
	SELECT @numDefaultWareHouseID=ISNULL(numDefaultWareHouseID,0) FROM [eCommerceDTL] WHERE [numDomainID]=@numDomainID	
	SET @numWareHouseID =@numDefaultWareHouseID;
END
/*If qty is not found in default warehouse then choose next warehouse with sufficient qty */
IF @bitAutoSelectWarehouse = 1 AND ( SELECT COUNT(*) FROM dbo.WareHouseItems WHERE  numItemID = @numItemCode AND numWarehouseID= @numWareHouseID ) = 0 
BEGIN
	SELECT TOP 1 @numDefaultWareHouseID = numWareHouseID FROM dbo.WareHouseItems WHERE  numItemID = @numItemCode AND numOnHand>0 /*TODO: Pass actuall qty from cart*/ ORDER BY numOnHand DESC 
	IF (ISNULL(@numDefaultWareHouseID,0)>0)
		SET @numWareHouseID =@numDefaultWareHouseID;
END

      DECLARE @UOMConversionFactor AS DECIMAL(18,5)
      
      Select @UOMConversionFactor= ISNULL(dbo.fn_UOMConversion(ISNULL(I.numSaleUnit,0),I.numItemCode,I.numDomainId,ISNULL(I.numBaseUnit,0)),0)
      FROM Item I where numItemCode=@numItemCode   
	   
	  DECLARE @PromotionOffers AS DECIMAL(18,2)=0     
	  IF(@vcCookieId!=null OR @vcCookieId!='')   
	  BEGIN                    
SET @PromotionOffers=(SELECT TOP 1 CASE WHEN P.tintDiscountType=1  
				THEN (I.monListPrice*fltDiscountValue)/100
				WHEN P.tintDiscountType=2
				THEN fltDiscountValue
				WHEN P.tintDiscountType=3
				THEN fltDiscountValue*I.monListPrice
				ELSE 0 END
FROM CartItems AS C
LEFT JOIN PromotionOffer AS P ON P.numProId=C.PromotionID
LEFT JOIN Item I ON I.numItemCode=C.numItemCode
WHERE I.numItemCode=@numItemCode AND C.vcCookieId=@vcCookieId AND C.numUserCntId=@numUserCntID AND C.fltDiscount=0 AND 
(I.numItemCode IN (SELECT PIS.numValue FROM PromotionOfferItems AS PIS WHERE PIS.numProId=P.numProId AND PIS.tintType=1 AND PIS.tintRecordType=6) OR
I.numItemClassification IN (SELECT PIS.numValue FROM PromotionOfferItems AS PIS WHERE PIS.numProId=P.numProId AND PIS.tintType=2 AND PIS.tintRecordType=6))
AND P.numDomainId=@numDomainID 
		AND ISNULL(bitEnabled,0)=1 
		AND ISNULL(bitAppliesToSite,0)=1 
		AND 1 = (CASE WHEN bitRequireCouponCode = 1 THEN CASE WHEN ISNULL(intCouponCodeUsed,0) < ISNULL(tintUsageLimit,0) OR ISNULL(tintUsageLimit,0) = 0  THEN 1 ELSE 0 END ELSE 1 END)
		AND 1 = (CASE WHEN bitNeverExpires = 1 THEN 1 ELSE (CASE WHEN CAST(GETDATE() AS DATE)>=dtValidFrom AND CAST(GETDATE() AS DATE)<=dtValidTo THEN 1 ELSE 0 END) END))
		END
		PRINT @PromotionOffers
 
declare @strSql as varchar(5000)
set @strSql=' With tblItem AS (                  
select I.numItemCode, vcItemName, txtItemDesc, charItemType, ISNULL(I.bitMatrix,0) bitMatrix,
ISNULL(CASE WHEN I.[charItemType]=''P'' 
			THEN CASE WHEN I.bitSerialized = 1 
					  THEN '+ convert(varchar(15),@UOMConversionFactor) + ' * monListPrice 
					  ELSE '+ convert(varchar(15),@UOMConversionFactor) + ' * W.[monWListPrice] 
				 END 
			ELSE '+ convert(varchar(15),@UOMConversionFactor) + ' * monListPrice 
	   END,0) monListPrice,
numItemClassification, bitTaxable, vcSKU, bitKitParent,              
I.numModifiedBy, (SELECT numItemImageId,vcPathForImage,vcPathForTImage,bitDefault,case when bitdefault=1  then -1 else isnull(intDisplayOrder,0) end as intDisplayOrder FROM ItemImages  WHERE numItemCode=I.numItemCode and bitIsImage = 1 order by case when bitdefault=1 then -1 else isnull(intDisplayOrder,0) end  asc FOR XML AUTO,ROOT(''Images''))  vcImages,
(SELECT vcPathForImage FROM ItemImages  WHERE numItemCode=I.numItemCode and bitIsImage = 0 order by intDisplayOrder  asc FOR XML AUTO,ROOT(''Documents''))  vcDocuments ,
 isnull(bitSerialized,0) as bitSerialized, vcModelID,                 
numItemGroup,                   
(isnull(sum(numOnHand),0) / '+ convert(varchar(15),@UOMConversionFactor) + ') as numOnHand,                    
sum(numOnOrder) as numOnOrder,                    
sum(numReorder)  as numReorder,                    
sum(numAllocation)  as numAllocation,                    
sum(numBackOrder)  as numBackOrder,              
isnull(fltWeight,0) as fltWeight,              
isnull(fltHeight,0) as fltHeight,              
isnull(fltWidth,0) as fltWidth,              
isnull(fltLength,0) as fltLength,              
case when charItemType=''P''then isnull(bitFreeShipping,0) else 1 end as bitFreeShipping,
Case When (case when charItemType=''P''then isnull(bitFreeShipping,0) else 1 end )=1 then ''Free shipping'' else ''Calculated at checkout'' end as Shipping,              
isnull(bitAllowBackOrder,0) as bitAllowBackOrder,bitShowInStock,bitShowQOnHand,isnull(numWareHouseItemID,0)  as numWareHouseItemID,    
(case when charItemType<>''S'' and charItemType<>''N'' then         
 (Case when '+ convert(varchar(15),@bitShowInStock) + '=1  then ( Case when numWareHouseItemID is null then ''<font color=red>Out Of Stock</font>'' when bitAllowBackOrder=1 then ''In Stock'' else         
 (Case when isnull(sum(numOnHand),0)>0 then ''In Stock''         
 else ''<font color=red>Out Of Stock</font>'' end ) end)    else '''' end) else '''' end) as InStock,
ISNULL(numSaleUnit,0) AS numUOM,
ISNULL(vcUnitName,'''') AS vcUOMName,
 '+ convert(varchar(15),@UOMConversionFactor) + ' AS UOMConversionFactor,
I.numCreatedBy,
I.[vcManufacturer],
(SELECT  COUNT(*) FROM  Review where vcReferenceId = CONVERT(VARCHAR(50) ,I.numItemCode) and numDomainId = I.numDomainId and numSiteId = '+ CONVERT(VARCHAR(20) , @numSiteId)  +' AND bitHide = 0 ) as ReviewCount ,
(SELECT  COUNT(*) FROM  Ratings where vcReferenceId = CONVERT(VARCHAR(50) ,I.numItemCode) and numDomainId = I.numDomainId and numSiteId = '+ CONVERT(VARCHAR(20) , @numSiteId)  +'  ) as RatingCount ,
(select top 1 vcMetaKeywords  from MetaTags where numReferenceID = I.numItemCode and tintMetaTagFor =2) as vcMetaKeywords,
(select top 1 vcMetaDescription  from MetaTags where numReferenceID = I.numItemCode and tintMetaTagFor =2) as vcMetaDescription ,
(SELECT vcCategoryName  FROM  dbo.Category WHERE numCategoryID =(SELECT TOP 1 numCategoryID FROM dbo.ItemCategory WHERE numItemID = I.numItemCode)) as vcCategoryName,('+(SELECT CAST(ISNULL(@PromotionOffers,0) AS varchar(20)))+') as PromotionOffers

from Item I                  
left join  WareHouseItems W                
on W.numItemID=I.numItemCode and numWareHouseID= '+ convert(varchar(15),@numWareHouseID) + '
left join  eCommerceDTL E          
on E.numDomainID=I.numDomainID     
LEFT JOIN UOM u ON u.numUOMId=I.numSaleUnit    
where I.numItemCode='+ convert(varchar(15),@numItemCode) + '             
group by I.numItemCode, vcItemName, txtItemDesc, charItemType, monListPrice,numItemClassification, bitTaxable, vcSKU, bitKitParent,           
I.numDomainID,I.numModifiedBy,  bitSerialized, vcModelID,numItemGroup, fltWeight,fltHeight,fltWidth,          
fltLength,bitFreeShipping,bitAllowBackOrder,bitShowInStock,bitShowQOnHand ,numWareHouseItemID,vcUnitName,I.numCreatedBy,W.[monWListPrice],I.[vcManufacturer],
numSaleUnit, I.bitMatrix

)'

--,
--


set @strSql=@strSql+' select numItemCode,vcItemName,txtItemDesc,charItemType, bitMatrix, monListPrice,numItemClassification, bitTaxable, vcSKU, bitKitParent,                  
tblItem.numModifiedBy, vcImages,vcDocuments, bitSerialized, vcModelID, numItemGroup,numOnHand,numOnOrder,numReorder,numAllocation, 
numBackOrder, fltWeight, fltHeight, fltWidth, fltLength, bitFreeShipping,bitAllowBackOrder,bitShowInStock,
bitShowQOnHand,numWareHouseItemID,InStock,numUOM,vcUOMName,UOMConversionFactor,tblItem.numCreatedBy,vcManufacturer,ReviewCount,RatingCount,Shipping,isNull(vcMetaKeywords,'''') as vcMetaKeywords ,isNull(vcMetaDescription,'''') as vcMetaDescription,isNull(vcCategoryName,'''') as vcCategoryName,PromotionOffers'     

set @strSql=@strSql+ ' from tblItem'
PRINT @strSql
exec (@strSql)


declare @tintOrder as tinyint                                                  
declare @vcFormFieldName as varchar(50)                                                  
declare @vcListItemType as varchar(1)                                             
declare @vcAssociatedControlType varchar(10)                                                  
declare @numListID AS numeric(9)                                                  
declare @WhereCondition varchar(2000)                       
Declare @numFormFieldId as numeric  
DECLARE @vcFieldType CHAR(1)
                  
set @tintOrder=0                                                  
set @WhereCondition =''                 
                   
              
  CREATE TABLE #tempAvailableFields(numFormFieldId  numeric(9),vcFormFieldName NVARCHAR(50),
        vcFieldType CHAR(1),vcAssociatedControlType NVARCHAR(50),numListID NUMERIC(18),
        vcListItemType CHAR(1),intRowNum INT,vcItemValue VARCHAR(3000))
   
  INSERT INTO #tempAvailableFields (numFormFieldId,vcFormFieldName,vcFieldType,vcAssociatedControlType
        ,numListID,vcListItemType,intRowNum)                         
            SELECT  Fld_id,REPLACE(Fld_label,' ','') AS vcFormFieldName,
                   CASE GRP_ID WHEN 4 then 'C' WHEN 1 THEN 'D' ELSE 'C' END AS vcFieldType,
                    fld_type as vcAssociatedControlType,
                    ISNULL(C.numListID, 0) numListID,
                    CASE WHEN C.numListID > 0 THEN 'L'
                         ELSE ''
                    END vcListItemType,ROW_NUMBER() OVER (ORDER BY Fld_id asc) AS intRowNum
            FROM    CFW_Fld_Master C LEFT JOIN CFW_Validation V ON V.numFieldID = C.Fld_id
            WHERE   C.numDomainID = @numDomainId
                    AND GRP_ID IN (5)

     select top 1 @tintOrder=intRowNum,@numFormFieldId=numFormFieldId,@vcFormFieldName=vcFormFieldName,
				  @vcFieldType=vcFieldType,@vcAssociatedControlType=vcAssociatedControlType,@numListID=numListID,
				  @vcListItemType=vcListItemType
	from #tempAvailableFields order by intRowNum ASC
   
while @tintOrder>0                                                  
begin                                                  
    if @vcAssociatedControlType = 'TextBox' or @vcAssociatedControlType = 'TextArea'
    begin  
		UPDATE #tempAvailableFields SET vcItemValue=(SELECT Fld_Value FROM CFW_FLD_Values_Item WHERE Fld_Id=@numFormFieldId AND RecId=@numItemCode) WHERE numFormFieldId=@numFormFieldId              
    end   
    else if @vcAssociatedControlType = 'CheckBox'
	begin      
		UPDATE #tempAvailableFields SET vcItemValue=(SELECT case when isnull(Fld_Value,0)=0 then 'No' when isnull(Fld_Value,0)=1 then 'Yes' END FROM CFW_FLD_Values_Item WHERE Fld_Id=@numFormFieldId AND RecId=@numItemCode) WHERE numFormFieldId=@numFormFieldId              
   end                
   else if @vcAssociatedControlType = 'DateField'           
   begin   
		UPDATE #tempAvailableFields SET vcItemValue=(SELECT dbo.FormatedDateFromDate(Fld_Value,@numDomainId) FROM CFW_FLD_Values_Item WHERE Fld_Id=@numFormFieldId AND RecId=@numItemCode) WHERE numFormFieldId=@numFormFieldId              
   end                
    else if @vcAssociatedControlType = 'SelectBox'           
   begin 
		UPDATE #tempAvailableFields SET vcItemValue=(SELECT L.vcData FROM CFW_FLD_Values_Item CFW left Join ListDetails L
			on L.numListItemID=CFW.Fld_Value                
			WHERE CFW.Fld_Id=@numFormFieldId AND CFW.RecId=@numItemCode) WHERE numFormFieldId=@numFormFieldId              
end          
               
 
    select top 1 @tintOrder=intRowNum,@numFormFieldId=numFormFieldId,@vcFormFieldName=vcFormFieldName,
				  @vcFieldType=vcFieldType,@vcAssociatedControlType=vcAssociatedControlType,@numListID=numListID,
				  @vcListItemType=vcListItemType
	from #tempAvailableFields WHERE intRowNum > @tintOrder order by intRowNum ASC
 
   if @@rowcount=0 set @tintOrder=0                                                  
end   


  
SELECT * FROM #tempAvailableFields

DROP TABLE #tempAvailableFields

--exec USP_ItemDetailsForEcomm @numItemCode=197611,@numWareHouseID=58,@numDomainID=1,@numSiteId=18
--exec USP_ItemDetailsForEcomm @numItemCode=735364,@numWareHouseID=1039,@numDomainID=156,@numSiteId=104
GO
/****** Object:  StoredProcedure [dbo].[USP_ItemList1]    Script Date: 05/07/2009 18:14:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--- Created By Anoop Jayaraj                                        
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'usp_itemlist1' ) 
    DROP PROCEDURE usp_itemlist1
GO
CREATE PROCEDURE [dbo].[USP_ItemList1]
    @ItemClassification AS NUMERIC(9) = 0,
    @IsKit AS TINYINT,
    @SortChar CHAR(1) = '0',
    @CurrentPage INT,
    @PageSize INT,
    @TotRecs INT OUTPUT, 
    @columnName AS VARCHAR(50),
    @columnSortOrder AS VARCHAR(10),
    @numDomainID AS NUMERIC(9) = 0,
    @ItemType AS CHAR(1),
    @bitSerialized AS BIT,
    @numItemGroup AS NUMERIC(9),
    @bitAssembly AS BIT,
    @numUserCntID AS NUMERIC(9),
    @Where	AS VARCHAR(MAX),
    @IsArchive AS BIT = 0,
    @byteMode AS TINYINT,
	@bitAsset as BIT=0,
	@bitRental as BIT=0,
	@bitContainer AS BIT=0,
	@SearchText VARCHAR(300),
	@vcRegularSearchCriteria varchar(MAX)='',
	@vcCustomSearchCriteria varchar(MAX)=''
AS 
BEGIN	
	DECLARE @numWarehouseID NUMERIC(18,0)
	SELECT TOP 1 @numWarehouseID = numWareHouseID FROM Warehouses WHERE numDomainID = @numDomainID


	CREATE TABLE #tempForm 
	(
		tintOrder TINYINT,vcDbColumnName nvarchar(50),vcOrigDbColumnName nvarchar(50),vcFieldName nvarchar(50),vcAssociatedControlType nvarchar(50),vcListItemType varchar(3),
		numListID numeric(9),vcLookBackTableName varchar(50),bitCustomField BIT,numFieldId NUMERIC,bitAllowSorting BIT,bitAllowEdit BIT,bitIsRequired BIT,bitIsEmail BIT,
		bitIsAlphaNumeric BIT,bitIsNumeric BIT,bitIsLengthValidation BIT,intMaxLength int,intMinLength int,bitFieldMessage BIT,vcFieldMessage VARCHAR(500),
		ListRelID numeric(9),intColumnWidth int,bitAllowFiltering BIT,vcFieldDataType CHAR(1)
	)

	-- FIRST CHECK IF GRID COLUMNS ARE CONFIGURED
	DECLARE  @Nocolumns  AS TINYINT = 0

	SELECT 
		@Nocolumns=ISNULL(SUM(TotalRow),0) 
	FROM
	(            
		SELECT 
			COUNT(*) TotalRow 
		FROM 
			View_DynamicColumns 
		WHERE 
			numFormId=21 
			AND numUserCntID=@numUserCntID 
			AND numDomainID=@numDomainID 
			AND tintPageType=1 
			AND numRelCntType = 0
		UNION 
		SELECT 
			COUNT(*) TotalRow 
		FROM 
			View_DynamicCustomColumns 
		WHERE 
			numFormId=21 
			AND numUserCntID=@numUserCntID 
			AND numDomainID=@numDomainID 
			AND tintPageType=1 
			AND numRelCntType = 0
	) TotalRows

	-- IF GRID COLUMNS ARE NOT CONFIGURES THEN CHECK IF MASTER FORM CONFIGURATION IS CREATED BY ADMINISTRATOR
	DECLARE @IsMasterConfAvailable BIT = 0
	DECLARE @numUserGroup NUMERIC(18,0)

	IF @Nocolumns=0
	BEGIN
		SELECT @numUserGroup = numGroupID FROM UserMaster WHERE numUserDetailId = @numUserCntID 

		IF (SELECT COUNT(*) FROM BizFormWizardMasterConfiguration WHERE numDomainID = @numDomainID AND numFormID = 21 AND numRelCntType=0 AND bitGridConfiguration = 1 AND numGroupID = @numUserGroup) > 0
		BEGIN
			SET @IsMasterConfAvailable = 1
		END
	END

	--IF MASTERCONFIGURATION IS AVAILABLE THEN LOAD IT OTHERWISE LOAD DEFAULT COLUMNS
	IF @IsMasterConfAvailable = 1
	BEGIN
		INSERT INTO 
			#tempForm
		SELECT 
			(intRowNum + 1) as tintOrder, vcDbColumnName,	vcOrigDbColumnName,	ISNULL(vcCultureFieldName,vcFieldName), vcAssociatedControlType,
			vcListItemType, numListID, vcLookBackTableName,	bitCustom, numFieldId, bitAllowSorting,	bitInlineEdit, bitIsRequired,
			bitIsEmail,	bitIsAlphaNumeric, bitIsNumeric, bitIsLengthValidation,	intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage vcFieldMessage,
			ListRelID,intColumnWidth,bitAllowFiltering,vcFieldDataType 
		FROM 
			View_DynamicColumnsMasterConfig
		WHERE
			View_DynamicColumnsMasterConfig.numFormId=21 AND 
			View_DynamicColumnsMasterConfig.numDomainID=@numDomainID AND 
			View_DynamicColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
			View_DynamicColumnsMasterConfig.numRelCntType=0 AND 
			View_DynamicColumnsMasterConfig.bitGridConfiguration = 1 AND
			ISNULL(View_DynamicColumnsMasterConfig.bitSettingField,0)=1 AND 
			ISNULL(View_DynamicColumnsMasterConfig.bitCustom,0)=0
		UNION
		SELECT 
			tintRow+1 as tintOrder,vcDbColumnName,vcDbColumnName,vcFieldName, vcAssociatedControlType,'' as vcListItemType,numListID,'',
			bitCustom,numFieldId,bitAllowSorting,bitAllowEdit,bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
			intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage,ListRelID,intColumnWidth,1,''
		FROM 
			View_DynamicCustomColumnsMasterConfig
		WHERE 
			View_DynamicCustomColumnsMasterConfig.numFormId=21 AND 
			View_DynamicCustomColumnsMasterConfig.numDomainID=@numDomainID AND 
			View_DynamicCustomColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
			View_DynamicCustomColumnsMasterConfig.numRelCntType=0 AND 
			View_DynamicCustomColumnsMasterConfig.bitGridConfiguration = 1 AND
			ISNULL(View_DynamicCustomColumnsMasterConfig.bitCustom,0)=1
		ORDER BY 
			tintOrder ASC  
	END
	ELSE                                        
	BEGIN
		IF @Nocolumns=0
		BEGIN
			INSERT INTO DycFormConfigurationDetails 
			(
				numFormID,numFieldID,intColumnNum,intRowNum,numDomainID,numUserCntID,numRelCntType,tintPageType,bitCustom,intColumnWidth
			)
			SELECT 
				21,numFieldId,0,Row_number() over(order by tintRow desc),@numDomainID,@numUserCntID,0,1,0,intColumnWidth
			FROM 
				View_DynamicDefaultColumns
			WHERE 
				numFormId=21 
				AND bitDefault=1 
				AND ISNULL(bitSettingField,0)=1 
				AND numDomainID=@numDomainID
			ORDER BY 
				tintOrder asc   
		END

		INSERT INTO 
			#tempForm
		SELECT 
			tintRow+1 as tintOrder,vcDbColumnName,vcOrigDbColumnName,ISNULL(vcCultureFieldName,vcFieldName),                  
			vcAssociatedControlType,vcListItemType,numListID,vcLookBackTableName,bitCustom,numFieldId,bitAllowSorting,bitInlineEdit,
			bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,intMaxLength,intMinLength,bitFieldMessage,
			vcFieldMessage vcFieldMessage,ListRelID,intColumnWidth,bitAllowFiltering,vcFieldDataType  
		FROM 
			View_DynamicColumns 
		WHERE 
			numFormId=21 
			AND numUserCntID=@numUserCntID 
			AND numDomainID=@numDomainID 
			AND tintPageType=1 
			AND ISNULL(bitSettingField,0)=1 
			AND ISNULL(bitCustom,0)=0  
			AND numRelCntType = 0
		UNION
		SELECT 
			tintRow+1 as tintOrder,vcDbColumnName,vcDbColumnName,vcFieldName,vcAssociatedControlType,'' as vcListItemType,numListID,'',bitCustom,numFieldId,bitAllowSorting,bitAllowEdit,
			bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
			intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage,ListRelID,intColumnWidth,1,'' 
		FROM 
			View_DynamicCustomColumns
		WHERE 
			numFormId=21 
			AND numUserCntID=@numUserCntID 
			AND numDomainID=@numDomainID 
			AND tintPageType=1 
			AND ISNULL(bitCustom,0)=1 
			AND numRelCntType = 0
		ORDER BY 
			tintOrder asc  
	END   

	IF @byteMode=0
	BEGIN
		DECLARE @strColumns AS VARCHAR(MAX)
		SET @strColumns = ' I.numItemCode,ISNULL(DivisionMaster.numDivisionID,0) numDivisionID,ISNULL(DivisionMaster.tintCRMType,0) tintCRMType, ' + CAST(ISNULL(@numWarehouseID,0) AS VARCHAR) + ' AS numWarehouseID, I.numDomainID,0 AS numRecOwner,0 AS numTerID, I.charItemType, I.numItemCode AS [numItemCode~211~0]'

		DECLARE @tintOrder AS TINYINT                                                  
		DECLARE @vcFieldName AS VARCHAR(50)                                                  
		DECLARE @vcListItemType AS VARCHAR(3)                                             
		DECLARE @vcListItemType1 AS VARCHAR(1)                                                 
		DECLARE @vcAssociatedControlType VARCHAR(10)                                                  
		DECLARE @numListID AS NUMERIC(9)                                                  
		DECLARE @vcDbColumnName VARCHAR(50)                      
		DECLARE @WhereCondition VARCHAR(2000)                       
		DECLARE @vcLookBackTableName VARCHAR(2000)                
		DECLARE @bitCustom AS BIT                  
		DECLARE @numFieldId AS NUMERIC  
		DECLARE @bitAllowSorting AS CHAR(1)   
		DECLARE @bitAllowEdit AS CHAR(1)                   
        DECLARE @vcColumnName AS VARCHAR(200)
		SET @tintOrder = 0                                                  
		SET @WhereCondition = ''                 
                   
		DECLARE @ListRelID AS NUMERIC(9) 
		DECLARE @SearchQuery AS VARCHAR(MAX) = ''

		IF LEN(@SearchText) > 0
		BEGIN
			SET @SearchQuery = ' I.numItemCode LIKE ''%' + @SearchText + '%'' OR I.vcItemName LIKE ''%' + @SearchText + '%'' OR I.vcModelID LIKE ''%' + @SearchText + '%'' OR I.vcSKU LIKE ''%' + @SearchText + '%'' OR cmp.vcCompanyName LIKE ''%' + @SearchText + '%'' OR I.vcManufacturer LIKE ''%' + @SearchText + '%'''
		END


		DECLARE @j INT = 0
  
		SELECT TOP 1
				@tintOrder = tintOrder + 1,
				@vcDbColumnName = vcDbColumnName,
				@vcFieldName = vcFieldName,
				@vcAssociatedControlType = vcAssociatedControlType,
				@vcListItemType = vcListItemType,
				@numListID = numListID,
				@vcLookBackTableName = vcLookBackTableName,
				@bitCustom = bitCustomField,
				@numFieldId = numFieldId,
				@bitAllowSorting = bitAllowSorting,
				@bitAllowEdit = bitAllowEdit,
				@ListRelID = ListRelID
		FROM    #tempForm --WHERE bitCustomField=1
		ORDER BY tintOrder ASC            

		WHILE @tintOrder > 0                                                  
		BEGIN
			IF @bitCustom = 0
			BEGIN
				DECLARE  @Prefix  AS VARCHAR(5)

				IF @vcLookBackTableName = 'Item'
					SET @Prefix = 'I.'
				ELSE IF @vcLookBackTableName = 'CompanyInfo'
					SET @Prefix = 'cmp.'
				ELSE IF @vcLookBackTableName = 'Vendor'
					SET @Prefix = 'V.'

				SET @vcColumnName= @vcDbColumnName+'~'+ convert(varchar(10),@numFieldId) +'~0'

				IF @vcAssociatedControlType='SelectBox' 
				BEGIN
					IF @vcListItemType='LI' 
					BEGIN
						SET @strColumns = @strColumns+',L'+ convert(varchar(3),@tintOrder)+'.vcData'+' ['+ @vcColumnName+']'
						SET @WhereCondition= @WhereCondition +' LEFT JOIN ListDetails L'+ convert(varchar(3),@tintOrder)+ ' ON L'+convert(varchar(3),@tintOrder)+ '.numListItemID='+@vcDbColumnName                                                      
					END
					ELSE IF @vcListItemType = 'UOM'
					BEGIN
						SET @strColumns = @strColumns + ',ISNULL((SELECT vcUnitName FROM UOM WHERE numUOMId = I.' + @vcDbColumnName + '),'''')' + ' ['+ @vcColumnName+']'
					END
					ELSE IF @vcListItemType = 'IG'
					BEGIN
						SET @strColumns = @strColumns+ ',ISNULL((SELECT vcItemGroup FROM ItemGroups WHERE numItemGroupID = I.numItemGroup),'''')' + ' ['+ @vcColumnName+']'  
					END
				END
				ELSE IF @vcDbColumnName = 'vcPathForTImage'
		   		BEGIN
					SET @strColumns = @strColumns + ',ISNULL(II.vcPathForTImage,'''') AS ' + ' ['+ @vcColumnName+']'                  
					SET @WhereCondition = @WhereCondition + ' LEFT JOIN ItemImages II ON II.numItemCode = I.numItemCode AND bitDefault = 1'
				END
				ELSE IF @vcLookBackTableName = 'Item' AND @vcDbColumnName = 'monListPrice'
				BEGIN
					SET @strColumns = @strColumns + ',CASE WHEN I.charItemType=''P'' THEN ISNULL((SELECT TOP 1 monWListPrice FROM WarehouseItems WHERE numDomainID=I.numDomainID AND numItemID=I.numItemCode AND monWListPrice > 0),0.00) ELSE ISNULL(I.monListPrice,0.00) END AS ' + ' ['+ @vcColumnName+']'
				END
				ELSE IF @vcLookBackTableName = 'Item' AND @vcDbColumnName = 'ItemType'
				BEGIN
					SET @strColumns = @strColumns + ',' + '(CASE 
																WHEN I.charItemType=''P'' 
																THEN 
																	CASE 
																		WHEN ISNULL(I.bitAssembly,0) = 1 THEN ''Assembly''
																		WHEN ISNULL(I.bitKitParent,0) = 1 THEN ''Kit''
																		WHEN ISNULL(I.bitAsset,0) = 1 AND ISNULL(I.bitRental,0)=0 THEN ''Asset''
																		WHEN ISNULL(I.bitAsset,0) = 1 AND ISNULL(I.bitRental,0)=1 THEN ''Rental Asset''
																		WHEN ISNULL(I.bitSerialized,0) = 1 AND ISNULL(I.bitAsset,0) = 0  THEN ''Serialized''
																		WHEN ISNULL(I.bitSerialized,0) = 1 AND ISNULL(I.bitAsset,0) = 1 AND ISNULL(I.bitRental,0)=0 THEN ''Serialized Asset''
																		WHEN ISNULL(I.bitSerialized,0) = 1 AND ISNULL(I.bitAsset,0) = 1 AND ISNULL(I.bitRental,0)=1 THEN ''Serialized Rental Asset''
																		WHEN ISNULL(I.bitLotNo,0)=1 THEN ''Lot #''
																		ELSE ''Inventory Item'' 
																	END
																WHEN I.charItemType=''N'' THEN ''Non Inventory Item'' 
																WHEN I.charItemType=''S'' THEN ''Service'' 
																WHEN I.charItemType=''A'' THEN ''Accessory'' 
															END)' + ' AS ' + ' [' + @vcColumnName + ']'  
				END
				ELSE IF @vcLookBackTableName = 'Item' AND @vcDbColumnName = 'monStockValue'
				BEGIN
					SET @strColumns = @strColumns+',WarehouseItems.monStockValue'+' ['+ @vcColumnName+']'                                                      
				END
				ELSE IF @vcLookBackTableName = 'Warehouses' AND @vcDbColumnName = 'vcWareHouse'
				BEGIN
					SET @strColumns = @strColumns + ', (SELECT STUFF((SELECT CONCAT('', '', vcWareHouse) FROM WareHouseItems WI JOIN Warehouses W ON WI.numWareHouseID=W.numWareHouseID WHERE WI.numItemID=I.numItemCode FOR XML PATH('''')), 1, 1, ''''))' + ' AS ' + ' [' + @vcColumnName + ']'
				END
				ELSE IF @vcLookBackTableName = 'WorkOrder' AND @vcDbColumnName = 'WorkOrder'
				BEGIN
					SET @strColumns = @strColumns + ',(CASE WHEN (SELECT SUM(WO.numQtyItemsReq) from WorkOrder WO where WO.numItemCode=I.numItemCode and WO.numWOStatus=0) > 0 THEN 1 ELSE 0 END)' + ' AS ' + ' [' + @vcColumnName + ']'
				END
				ELSE IF @vcDbColumnName = 'vcPriceLevelDetail' 
				BEGIN
					SELECT @strColumns = @strColumns + CONCAT(', dbo.fn_GetItemPriceLevelDetail( ',@numDomainID,',I.numItemCode,',@numWarehouseID,')') +' ['+ @vcColumnName+']' 
				END
				ELSE
				BEGIN
					If @vcDbColumnName <> 'numItemCode' AND @vcDbColumnName <> 'vcPriceLevelDetail' --WE AE ALREADY ADDING COLUMN IN SELECT CLUASE DIRECTLY
					BEGIN
						SET @strColumns = @strColumns + ',' + (CASE @vcLookBackTableName WHEN 'Item' THEN 'I' WHEN 'CompanyInfo' THEN 'cmp' WHEN 'Vendor' THEN 'V' ELSE @vcLookBackTableName END) + '.' + @vcDbColumnName + ' AS ' + ' [' + @vcColumnName + ']' 
					END
				END
			END                              
			ELSE IF @bitCustom = 1 
			BEGIN
				SET @vcColumnName= @vcDbColumnName +'~'+ convert(varchar(10),@numFieldId)+'~1'

				SELECT 
					@vcFieldName = FLd_label,
					@vcAssociatedControlType = fld_type,
					@vcDbColumnName = 'Cust' + CONVERT(VARCHAR(10), Fld_Id)
				FROM 
					CFW_Fld_Master
				WHERE 
					CFW_Fld_Master.Fld_Id = @numFieldId                 
        
				IF @vcAssociatedControlType = 'TextBox' OR @vcAssociatedControlType = 'TextArea' 
				BEGIN
					SET @strColumns = @strColumns + ',CFW'  + CONVERT(VARCHAR(3), @tintOrder)  + '.Fld_Value  [' + @vcColumnName + ']'     
             
					SET @WhereCondition = @WhereCondition + ' left Join CFW_FLD_Values_Item CFW' + CONVERT(VARCHAR(3), @tintOrder)
									+ ' ON CFW' + CONVERT(VARCHAR(3), @tintOrder) + '.Fld_Id='
									+ CONVERT(VARCHAR(10), @numFieldId)
									+ 'and CFW' + CONVERT(VARCHAR(3), @tintOrder)
									+ '.RecId=I.numItemCode   '                                                         
				END   
				ELSE IF @vcAssociatedControlType = 'CheckBox' 
				BEGIN
					SET @strColumns = @strColumns
						+ ',case when isnull(CFW'
						+ CONVERT(VARCHAR(3), @tintOrder)
						+ '.Fld_Value,0)=0 then 0 when isnull(CFW'
						+ CONVERT(VARCHAR(3), @tintOrder)
						+ '.Fld_Value,0)=1 then 1 end   ['
						+  @vcColumnName
						+ ']'               
 
					SET @WhereCondition = @WhereCondition
						+ ' left Join CFW_FLD_Values_Item CFW'
						+ CONVERT(VARCHAR(3), @tintOrder)
						+ ' ON CFW' + CONVERT(VARCHAR(3), @tintOrder) + '.Fld_Id='
						+ CONVERT(VARCHAR(10), @numFieldId)
						+ ' and CFW'
						+ CONVERT(VARCHAR(3), @tintOrder)
						+ '.RecId=I.numItemCode   '                                                     
				END                
				ELSE IF @vcAssociatedControlType = 'DateField' 
				BEGIN
					SET @strColumns = @strColumns
						+ ',dbo.FormatedDateFromDate(CFW'
						+ CONVERT(VARCHAR(3), @tintOrder)
						+ '.Fld_Value,'
						+ CONVERT(VARCHAR(10), @numDomainId)
						+ ')  [' +@vcColumnName + ']'    
					                  
					SET @WhereCondition = @WhereCondition
						+ ' left Join CFW_FLD_Values_Item CFW'
						+ CONVERT(VARCHAR(3), @tintOrder)
						+ ' on CFW' + CONVERT(VARCHAR(3), @tintOrder) + '.Fld_Id='
						+ CONVERT(VARCHAR(10), @numFieldId)
						+ ' and CFW'
						+ CONVERT(VARCHAR(3), @tintOrder)
						+ '.RecId=I.numItemCode   '                                                         
				END                
				ELSE IF @vcAssociatedControlType = 'SelectBox' 
				BEGIN
					SET @vcDbColumnName = 'Cust' + CONVERT(VARCHAR(10), @numFieldId)

					SET @strColumns = @strColumns + ',L'
						+ CONVERT(VARCHAR(3), @tintOrder)
						+ '.vcData' + ' [' + @vcColumnName + ']'          
					                                                    
					SET @WhereCondition = @WhereCondition
						+ ' left Join CFW_FLD_Values_Item CFW'
						+ CONVERT(VARCHAR(3), @tintOrder)
						+ ' on CFW' + CONVERT(VARCHAR(3), @tintOrder) + '.Fld_Id='
						+ CONVERT(VARCHAR(10), @numFieldId)
						+ ' and CFW'
						+ CONVERT(VARCHAR(3), @tintOrder)
						+ '.RecId=I.numItemCode    '     
					                                                    
					SET @WhereCondition = @WhereCondition
						+ ' left Join ListDetails L'
						+ CONVERT(VARCHAR(3), @tintOrder)
						+ ' on L'
						+ CONVERT(VARCHAR(3), @tintOrder)
						+ '.numListItemID=CFW'
						+ CONVERT(VARCHAR(3), @tintOrder)
						+ '.Fld_Value'                
				END
				ELSE
				BEGIN
					SET @strColumns = @strColumns + CONCAT(',dbo.GetCustFldValueItem(',@numFieldId,',I.numItemCode)') + ' [' + @vcColumnName + ']'
				END                
			END        
  
			

			SELECT TOP 1
					@tintOrder = tintOrder + 1,
					@vcDbColumnName = vcDbColumnName,
					@vcFieldName = vcFieldName,
					@vcAssociatedControlType = vcAssociatedControlType,
					@vcListItemType = vcListItemType,
					@numListID = numListID,
					@vcLookBackTableName = vcLookBackTableName,
					@bitCustom = bitCustomField,
					@numFieldId = numFieldId,
					@bitAllowSorting = bitAllowSorting,
					@bitAllowEdit = bitAllowEdit,
					@ListRelID = ListRelID
			FROM    #tempForm
			WHERE   tintOrder > @tintOrder - 1 --AND bitCustomField=1
			ORDER BY tintOrder ASC            
 
			IF @@rowcount = 0 
				SET @tintOrder = 0 
		END  

		DECLARE @strSQL AS VARCHAR(MAX)
		SET @strSQL = ' FROM 
							Item I
						LEFT JOIN
							Vendor V
						ON
							I.numItemCode = V.numItemCode
							AND I.numVendorID = V.numVendorID
						LEFT JOIN
							DivisionMaster
						ON
							V.numVendorID = DivisionMaster.numDivisionID
						LEFT JOIN
							CompanyInfo cmp
						ON
							DivisionMaster.numCompanyId = cmp.numCompanyId
						OUTER APPLY
						(
							SELECT
								SUM(numOnHand) AS numOnHand,
								SUM(numBackOrder) AS numBackOrder,
								SUM(numOnOrder) AS numOnOrder,
								SUM(numAllocation) AS numAllocation,
								SUM(numReorder) AS numReorder,
								SUM(numOnHand) * (CASE WHEN ISNULL(I.bitVirtualInventory,0) = 1 THEN 0 ELSE ISNULL(I.monAverageCost,0) END) AS monStockValue
							FROM
								WareHouseItems
							WHERE
								numItemID = I.numItemCode
						) WarehouseItems ' + @WhereCondition

		IF @columnName = 'OnHand' 
			SET @columnName = 'numOnHand'
		ELSE IF @columnName = 'Backorder' 
			SET @columnName = 'numBackOrder'
		ELSE IF @columnName = 'OnOrder' 
			SET @columnName = 'numOnOrder'
		ELSE IF @columnName = 'OnAllocation' 
			SET @columnName = 'numAllocation'
		ELSE IF @columnName = 'Reorder' 
			SET @columnName = 'numReorder'
		ELSE IF @columnName = 'monStockValue' 
			SET @columnName = 'sum(numOnHand) * (CASE WHEN ISNULL(bitVirtualInventory,0) = 1 THEN 0 ELSE ISNULL(monAverageCost,0) END)'
		ELSE IF @columnName = 'ItemType' 
			SET @columnName = 'charItemType'
		ELSE IF @columnName = 'numItemCode' 
			SET @columnName = 'I.numItemCode'

		DECLARE @column AS VARCHAR(50)              
		SET @column = @columnName
	         
		IF @columnName LIKE '%Cust%' 
		BEGIN
			DECLARE @CustomFieldType AS VARCHAR(10)
			DECLARE @fldId AS VARCHAR(10)
			
			IF CHARINDEX('CFW.Cust',@columnName) > 0
			BEGIN
				SET @fldId = REPLACE(@columnName, 'CFW.Cust', '')
			END
			ELSE
			BEGIN
				SET @fldId = REPLACE(@columnName, 'Cust', '')
			END
			SELECT TOP 1 @CustomFieldType = ISNULL(vcAssociatedControlType,'') FROM View_DynamicCustomColumns WHERE numFieldID = @fldId
            
			IF ISNULL(@CustomFieldType,'') = 'SelectBox' OR ISNULL(@CustomFieldType,'') = 'ListBox'
			BEGIN
				SET @strSQL = @strSQL + ' left Join CFW_FLD_Values_Item CFW on CFW.RecId=i.numItemCode and CFW.fld_id= ' + @fldId + ' ' 
				SET @strSQL = @strSQL + ' left Join ListDetails LstCF on LstCF.numListItemID = CFW.Fld_Value  '            
				SET @columnName = ' LstCF.vcData ' 	
			END
			ELSE
			BEGIN
				SET @strSQL =  @strSQL + ' left Join CFW_FLD_Values_Item CFW on CFW.RecId=i.numItemCode and CFW.fld_id= ' + @fldId + ' '                                                         
				SET @columnName = 'CFW.Fld_Value'                	
			END
		END                                              
                  
		DECLARE @strWhere AS VARCHAR(8000)
		SET @strWhere = CONCAT(' WHERE I.numDomainID = ',@numDomainID)
    
		IF @ItemType = 'P'
		BEGIN
			SET @strWhere = @strWhere + ' AND I.charItemType= ''P''' 
		END
		ELSE IF @ItemType = 'S'
		BEGIN
			SET @strWhere = @strWhere + ' AND I.charItemType= ''S'''
		END
		ELSE IF @ItemType = 'N'    
		BEGIN
			SET @strWhere = @strWhere + ' AND I.charItemType= ''N'''
		END
	    
		IF @bitContainer= '1'
		BEGIN
			SET @strWhere = @strWhere + ' AND I.bitContainer= ' + CONVERT(VARCHAR(2), @bitContainer) + ''  
		END

		IF @bitAssembly = '1' 
		BEGIN
			SET @strWhere = @strWhere + ' AND I.bitAssembly= ' + CONVERT(VARCHAR(2), @bitAssembly) + ''  
		END 
		ELSE IF @ItemType = 'P'
		BEGIN
			SET @strWhere = @strWhere + ' AND ISNULL(I.bitAssembly,0)=0 '
		END

		IF @IsKit = '1' 
		BEGIN
			SET @strWhere = @strWhere + ' AND ISNULL(I.bitAssembly,0)=0 AND I.bitKitParent= ' + CONVERT(VARCHAR(2), @IsKit) + ''  
		END           
		ELSE IF @ItemType = 'P'
		BEGIN
			SET @strWhere = @strWhere + ' AND ISNULL(I.bitKitParent,0)=0 '
		END

		IF @bitSerialized = 1 
		BEGIN
			SET @strWhere = @strWhere + ' AND (I.bitSerialized=1 OR I.bitLotNo=1)'
		END
		--ELSE IF @ItemType = 'P'
		--BEGIN
		--	SET @strWhere = @strWhere + ' AND ISNULL(Item.bitSerialized,0)=0 AND ISNULL(Item.bitLotNo,0)=0 '
		--END

		IF @bitAsset = 1 
		BEGIN
			SET @strWhere = @strWhere + ' AND ISNULL(I.bitAsset,0) = 1'
		END
		ELSE IF @ItemType = 'P'
		BEGIN
			SET @strWhere = @strWhere + ' AND ISNULL(I.bitAsset,0) = 0 '
		END

		IF @bitRental = 1
		BEGIN
			SET @strWhere = @strWhere + ' AND ISNULL(I.bitRental,0) = 1 AND ISNULL(I.bitAsset,0) = 1 '
		END
		ELSE IF @ItemType = 'P'
		BEGIN
			SET @strWhere = @strWhere + ' AND ISNULL(I.bitRental,0) = 0 '
		END

		IF @SortChar <> '0' 
		BEGIN
			SET @strWhere = @strWhere + ' AND I.vcItemName like ''' + @SortChar + '%'''                                       
		END

		IF @ItemClassification <> '0' 
		BEGIN
			SET @strWhere = @strWhere + ' AND I.numItemClassification=' + CONVERT(VARCHAR(15), @ItemClassification)    
		END

		IF @IsArchive = 0 
		BEGIN
			SET @strWhere = @strWhere + ' AND ISNULL(I.IsArchieve,0) = 0'       
		END
        
		IF @numItemGroup > 0 
		BEGIN
			SET @strWhere = @strWhere + ' AND I.numItemGroup = ' + CONVERT(VARCHAR(20), @numItemGroup)  
        END  
		
		IF @numItemGroup = -1 
		BEGIN
			SET @strWhere = @strWhere + ' AND I.numItemGroup in (select numItemGroupID from ItemGroups where numDomainID=' + CONVERT(VARCHAR(20), @numDomainID) + ')'                                      
        END

		IF @vcRegularSearchCriteria<>'' 
		BEGIN
			SET @strWhere = @strWhere +' AND ' + @vcRegularSearchCriteria 
		END
	
		IF @vcCustomSearchCriteria<>'' 
		BEGIN
			SET @strWhere = @strWhere + ' AND ' + @vcCustomSearchCriteria
		END
		
		IF LEN(@SearchQuery) > 0
		BEGIN
			SET @strWhere = @strWhere + ' AND (' + @SearchQuery + ') '
		END

		SET @strWhere = @strWhere + ISNULL(@Where,'')
  
		DECLARE @firstRec AS INTEGER                                        
		DECLARE @lastRec AS INTEGER                                        
		SET @firstRec = ( @CurrentPage - 1 ) * @PageSize                                        
		SET @lastRec = ( @CurrentPage * @PageSize + 1 )

		-- EXECUTE FINAL QUERY
		DECLARE @strFinal AS NVARCHAR(MAX)
		
		IF @CurrentPage = -1 AND @PageSize = -1
		BEGIN
			SET @strFinal = CONCAT('SELECT I.numItemCode, I.vcItemName, I.vcModelID INTO #tempTable ',@strSql + @strWhere,'; SELECT * FROM #tempTable; SELECT @TotalRecords = COUNT(*) FROM #tempTable; DROP TABLE #tempTable;')
		END
		ELSE
		BEGIN
			SET @strFinal = CONCAT('SELECT ROW_NUMBER() OVER(ORDER BY ',@columnName,' ',@columnSortOrder,') ID,I.numItemCode INTO #temp2 ',@strSql, @strWhere,'; SELECT ID, ',@strColumns,' INTO #tempTable ',@strSql, ' JOIN #temp2 tblAllItems ON I.numItemCode = tblAllItems.numItemCode ',@strWhere,' AND tblAllItems.ID > ',@firstRec,' and tblAllItems.ID <',@lastRec,'; SELECT * FROM  #tempTable; DROP TABLE #tempTable; SELECT @TotalRecords = COUNT(*) FROM #temp2; DROP TABLE #temp2;')
		END

		
		PRINT @strFinal
		exec sp_executesql @strFinal, N'@TotalRecords INT OUT', @TotRecs OUT

	END

	--UPDATE  
	--	#tempForm
 --   SET 
	--	vcDbColumnName = (CASE WHEN bitCustomField = 1 THEN vcFieldName + '~' + vcDbColumnName ELSE vcDbColumnName END)
                                  
	SELECT * FROM #tempForm
    DROP TABLE #tempForm
END
--SELECT * FROM Category
--- Alterd By Chintan Prajapati
-- EXEC dbo.USP_ItemsForECommerce 9,60,1,1,15,0,''
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_ItemsForECommerce' ) 
    DROP PROCEDURE USP_ItemsForECommerce
GO
CREATE PROCEDURE [dbo].[USP_ItemsForECommerce]
    @numSiteID AS NUMERIC(9) = 0,
    @numCategoryID AS NUMERIC(18) = 0,
    @numWareHouseID AS NUMERIC(9) = 0,
    @SortBy NVARCHAR(MAX) = 0,
    @CurrentPage INT,
    @PageSize INT,
    @TotRecs INT = 0 OUTPUT,
    @SearchText AS NVARCHAR(MAX) = '',--Comma seperated item ids
    @FilterQuery AS NVARCHAR(MAX) = '',----Comma seperated Attributes Ids
    @FilterRegularCondition AS NVARCHAR(MAX),
    --@FilterCustomCondition AS NVARCHAR(MAX),
    @FilterCustomFields AS NVARCHAR(MAX),
    @FilterCustomWhere AS NVARCHAR(MAX)
   
AS 
    BEGIN
		
		DECLARE @numDomainID AS NUMERIC(9)
		SELECT  @numDomainID = numDomainID
        FROM    [Sites]
        WHERE   numSiteID = @numSiteID
        
        DECLARE @tintDisplayCategory AS TINYINT
		SELECT @tintDisplayCategory = ISNULL(bitDisplayCategory,0) FROM dbo.eCommerceDTL WHERE numSiteID = @numSiteId
		PRINT @tintDisplayCategory 
		
		CREATE TABLE #tmpItemCat(numCategoryID NUMERIC(18,0))
		CREATE TABLE #tmpItemCode(numItemCode NUMERIC(18,0))
		IF LEN(@FilterCustomFields) > 0 AND LEN(@FilterCustomWhere) > 0
		BEGIN
--			DECLARE @strCustomSql AS NVARCHAR(MAX)
--			SET @strCustomSql = 'INSERT INTO #tmpItemCode(numItemCode)
--								 SELECT DISTINCT t2.RecId FROM CFW_Fld_Master t1 
--								 JOIN CFW_FLD_Values_Item AS t2 ON t1.Fld_ID = t2.Fld_ID 
--								 WHERE t1.Grp_id = 5 
--								 AND t1.numDomainID = (SELECT numDomainID FROM dbo.Sites WHERE numSiteID = ' + CONVERT(VARCHAR(10),@numSiteID) + ') 
--								 AND t1.fld_type <> ''Link'' AND ' + @FilterCustomCondition
			
			DECLARE @FilterCustomFieldIds AS NVARCHAR(MAX)
			SET @FilterCustomFieldIds = Replace(@FilterCustomFields,'[','')
			SET @FilterCustomFieldIds = Replace(@FilterCustomFieldIds,']','')
			
			DECLARE @strCustomSql AS NVARCHAR(MAX)
			SET @strCustomSql = 'INSERT INTO #tmpItemCode(numItemCode)
								 SELECT DISTINCT T.RecId FROM 
															(
																SELECT * FROM 
																	(
																		SELECT  t2.RecId, t1.fld_id, t2.Fld_Value FROM CFW_Fld_Master t1 
																		JOIN CFW_FLD_Values_Item AS t2 ON t1.Fld_ID = t2.Fld_ID 
																		WHERE t1.Grp_id = 5 
																		AND t1.numDomainID = ' + CONVERT(VARCHAR(10),@numDomainID) + '
																		AND t1.fld_type <> ''Link'' 
																		AND t1.fld_id IN (' + @FilterCustomFieldIds + ')
																	) p  PIVOT (MAX([Fld_Value]) FOR fld_id IN (' + @FilterCustomFields + ')) AS pvt 
															) T  WHERE 1=1 AND ' + @FilterCustomWhere
								 
			PRINT @strCustomSql
			EXEC SP_EXECUTESQL @strCustomSql								 					 
		END
		
        DECLARE @strSQL NVARCHAR(MAX)
        DECLARE @firstRec AS INTEGER
        DECLARE @lastRec AS INTEGER
        DECLARE @Where NVARCHAR(MAX)
        DECLARE @SortString NVARCHAR(MAX)
        DECLARE @searchPositionColumn NVARCHAR(MAX)
        DECLARE @row AS INTEGER
        DECLARE @data AS VARCHAR(100)
        DECLARE @dynamicFilterQuery NVARCHAR(MAX)
        DECLARE @checkFldType VARCHAR(100)
        DECLARE @count NUMERIC(9,0)
        DECLARE @whereNumItemCode NVARCHAR(MAX)
        DECLARE @filterByNumItemCode VARCHAR(100)
        
		SET @SortString = ''
        set @searchPositionColumn = ''
        SET @strSQL = ''
		SET @whereNumItemCode = ''
		SET @dynamicFilterQuery = ''
		 
        SET @firstRec = ( @CurrentPage - 1 ) * @PageSize
        SET @lastRec = ( @CurrentPage * @PageSize + 1 )
                
        DECLARE @bitAutoSelectWarehouse AS BIT
		SELECT @bitAutoSelectWarehouse = ISNULL([ECD].[bitAutoSelectWarehouse],0) FROM [dbo].[eCommerceDTL] AS ECD WHERE [ECD].[numSiteId] = @numSiteID
		AND [ECD].[numDomainId] = @numDomainID

		DECLARE @vcWarehouseIDs AS VARCHAR(1000)
        IF (@numWareHouseID = 0 OR @bitAutoSelectWarehouse = 1)
            BEGIN				
				IF @bitAutoSelectWarehouse = 1
					BEGIN
						SELECT @vcWarehouseIDs = ISNULL( STUFF((SELECT DISTINCT ',' + CAST(numWarehouseID AS VARCHAR(10)) FROM [dbo].[Warehouses] WHERE [Warehouses].[numDomainID] = @numDomainID FOR XML PATH('')),1, 1, '') , '') 
					END
				
				ELSE
					BEGIN
						SELECT  @numWareHouseID = ISNULL(numDefaultWareHouseID,0)
						FROM    [eCommerceDTL]
						WHERE   [numDomainID] = @numDomainID	

						SET @vcWarehouseIDs = @numWareHouseID
					END
            END
		ELSE
		BEGIN
			SET @vcWarehouseIDs = @numWareHouseID
		END

		PRINT @vcWarehouseIDs

		--PRIAMRY SORTING USING CART DROPDOWN
		DECLARE @Join AS NVARCHAR(MAX)
		DECLARE @SortFields AS NVARCHAR(MAX)
		DECLARE @vcSort AS VARCHAR(MAX)

		SET @Join = ''
		SET @SortFields = ''
		SET @vcSort = ''

		IF @SortBy = ''
			BEGIN
				SET @SortString = ' vcItemName Asc '
			END
		ELSE IF CHARINDEX('~0',@SortBy) > 0 OR CHARINDEX('~1',@SortBy) > 0
			BEGIN
				DECLARE @fldID AS NUMERIC(18,0)
				DECLARE @RowNumber AS VARCHAR(MAX)
				
				SELECT * INTO #fldValues FROM dbo.SplitIDsWithPosition(@SortBy,'~')
				SELECT @RowNumber = RowNumber, @fldID = id FROM #fldValues WHERE RowNumber = 1
				SELECT @vcSort = CASE WHEN Id = 0 THEN 'ASC' ELSE 'DESC' END FROM #fldValues WHERE RowNumber = 2
				
				SET @Join = @Join + ' LEFT JOIN CFW_FLD_Values_Item CFVI' + @RowNumber + ' ON I.numItemCode = CFVI' + @RowNumber + '.RecID AND CFVI' + @RowNumber + '.fld_Id = ' + CONVERT(VARCHAR(10),@fldID)
				---SET @SortString = 'CFVI' + @RowNumber + '.fld_Value' 
				SET @SortString = '[fld_Value' + @RowNumber + '] ' --+ @vcSort
				SET @SortFields = ', CFVI' + @RowNumber + '.fld_Value [fld_Value' + @RowNumber + ']'
			END
		ELSE
			BEGIN
				--SET @SortString = @SortBy	
				
				IF CHARINDEX('monListPrice',@SortBy) > 0
				BEGIN
					DECLARE @bitSortPriceMode AS BIT
					SELECT @bitSortPriceMode = ISNULL(bitSortPriceMode,0) FROM dbo.eCommerceDTL WHERE numSiteId = @numSiteID
					PRINT @bitSortPriceMode
					IF @bitSortPriceMode = 1
					BEGIN
						SET @SortString = ' '
					END
					ELSE
					BEGIN
						SET @SortString = @SortBy	
					END
				END
				ELSE
				BEGIN
					SET @SortString = @SortBy	
				END
				
			END	

		--SECONDARY SORTING USING DEFAULT SETTINGS FROM ECOMMERCE
		SELECT ROW_NUMBER() OVER (ORDER BY Custom,vcFieldName) AS [RowID],* INTO #tempSort FROM (
		SELECT  CONVERT(VARCHAR(9),numFieldID)+'~0' AS numFieldID,
				ISNULL(vcCultureFieldName,vcFieldName) AS vcFieldName,
				0 AS Custom,
				vcDbColumnName 
		FROM View_DynamicColumns
		WHERE numFormID = 84 
		AND ISNULL(bitSettingField,0) = 1 
		AND ISNULL(bitDeleted,0)=0 
		AND ISNULL(bitCustom,0) = 0
		AND numDomainID = @numDomainID
					       
		UNION     

		SELECT  CONVERT(VARCHAR(9),numFieldID)+'~1' AS numFieldID ,
				vcFieldName ,
				1 AS Custom,
				'' AS vcDbColumnName
		FROM View_DynamicCustomColumns          
		WHERE numFormID = 84
		AND numDomainID = @numDomainID
		AND grp_id = 5 
		AND vcAssociatedControlType = 'TextBox' 
		AND ISNULL(bitCustom,0) = 1 
		AND tintPageType=1  
		) TABLE1 ORDER BY Custom,vcFieldName
		
		--SELECT * FROM #tempSort
		DECLARE @DefaultSort AS NVARCHAR(MAX)
		DECLARE @DefaultSortFields AS NVARCHAR(MAX)
		DECLARE @DefaultSortJoin AS NVARCHAR(MAX)
		DECLARE @intCnt AS INT
		DECLARE @intTotalCnt AS INT	
		DECLARE @strColumnName AS VARCHAR(100)
		DECLARE @bitCustom AS BIT
		DECLARE @FieldID AS VARCHAR(100)				

		SET @DefaultSort = ''
		SET @DefaultSortFields = ''
		SET @DefaultSortJoin = ''		
		SET @intCnt = 0
		SET @intTotalCnt = ( SELECT COUNT(*) FROM #tempSort )
		SET @strColumnName = ''
		
		IF (SELECT ISNULL(bitEnableSecSorting,0) FROM EcommerceDtl WHERE numSiteID = @numSiteID ) = 1
		BEGIN
			CREATE TABLE #fldDefaultValues(RowNumber INT,Id VARCHAR(1000))

			WHILE(@intCnt < @intTotalCnt)
			BEGIN
				SET @intCnt = @intCnt + 1
				SELECT @FieldID = numFieldID, @strColumnName = vcDbColumnName, @bitCustom = Custom FROM #tempSort WHERE RowID = @intCnt
				
				--PRINT @FieldID
				--PRINT @strColumnName
				--PRINT @bitCustom

				IF @bitCustom = 0
					BEGIN
						SET @DefaultSort =  @strColumnName + ',' + @DefaultSort 
					END

				ELSE
					BEGIN
						DECLARE @fldID1 AS NUMERIC(18,0)
						DECLARE @RowNumber1 AS VARCHAR(MAX)
						DECLARE @vcSort1 AS VARCHAR(10)
						DECLARE @vcSortBy AS VARCHAR(1000)
						SET @vcSortBy = CONVERT(VARCHAR(100),@FieldID) + '~' + '0'
						--PRINT @vcSortBy

						INSERT INTO #fldDefaultValues(RowNumber,Id) SELECT RowNumber, id FROM dbo.SplitIDsWithPosition(@vcSortBy,'~')
						SELECT @RowNumber1 = RowNumber + 100 + @intCnt, @fldID1 = id FROM #fldDefaultValues WHERE RowNumber = 1
						SELECT @vcSort1 = 'ASC' 
						
						SET @DefaultSortJoin = @DefaultSortJoin + ' LEFT JOIN CFW_FLD_Values_Item CFVI' + @RowNumber1 + ' ON I.numItemCode = CFVI' + @RowNumber1 + '.RecID AND CFVI' + @RowNumber1 + '.fld_Id = ' + CONVERT(VARCHAR(10),@fldID1)
						SET @DefaultSort = '[fld_Value' + @RowNumber1 + '] ' + ',' + @DefaultSort 
						SET @DefaultSortFields = @DefaultSortFields + ', CFVI' + @RowNumber1 + '.fld_Value [fld_Value' + @RowNumber1 + ']'
					END

			END
			IF LEN(@DefaultSort) > 0
			BEGIN
				SET  @DefaultSort = LEFT(@DefaultSort,LEN(@DefaultSort) - 1)
				SET @SortString = @SortString + ',' 
			END
			--PRINT @SortString
			--PRINT @DefaultSort
			--PRINT @DefaultSortFields
		END
--        IF @SortBy = 1 
--            SET @SortString = ' vcItemName Asc '
--        ELSE IF @SortBy = 2 
--            SET @SortString = ' vcItemName Desc '
--        ELSE IF @SortBy = 3 
--            SET @SortString = ' monListPrice Asc '
--        ELSE IF @SortBy = 4 
--            SET @SortString = ' monListPrice Desc '
--        ELSE IF @SortBy = 5 
--            SET @SortString = ' bintCreatedDate Desc '
--        ELSE IF @SortBy = 6 
--            SET @SortString = ' bintCreatedDate ASC '	
--        ELSE IF @SortBy = 7 --sort by relevance, invokes only when search
--            BEGIN
--                 IF LEN(@SearchText) > 0 
--		            BEGIN
--						SET @SortString = ' SearchOrder ASC ';
--					    set @searchPositionColumn = ' ,SearchItems.RowNumber AS SearchOrder ' 						
--					END
--			END
--        ELSE 
--            SET @SortString = ' vcItemName Asc '
		
                                		
        IF LEN(@SearchText) > 0
			BEGIN
				PRINT 1
				IF CHARINDEX('SearchOrder',@SortString) > 0
				BEGIN
					SET @searchPositionColumn = ' ,SearchItems.RowNumber AS SearchOrder ' 
				END

				SET @Where = ' INNER JOIN dbo.SplitIDsWithPosition('''+ @SearchText +''','','') AS SearchItems 
                        ON SearchItems.Id = I.numItemCode  and SC.numSiteID = ' + CONVERT(VARCHAR(15), @numSiteID) + ' WHERE ISNULL(IsArchieve,0) = 0'
				END

		ELSE IF @SearchText = ''

			SET @Where = ' WHERE 1=2 '
			+ ' and SC.numSiteID = ' + CONVERT(VARCHAR(15), @numSiteID)
			+ ' AND ISNULL(IsArchieve,0) = 0'
		ELSE
			BEGIN
				PRINT 2
				SET @Where = ' WHERE 1=1 '
							+ ' and SC.numSiteID = ' + CONVERT(VARCHAR(15), @numSiteID)
							+ ' AND ISNULL(IsArchieve,0) = 0'


				PRINT @tintDisplayCategory							
				PRINT @Where

				IF @tintDisplayCategory = 1
				BEGIN
					
					;WITH CTE AS(
					SELECT C.numCategoryID,C.tintLevel,C.numDepCategory,CASE WHEN tintLevel=1 THEN 1 ELSE 0 END AS bitChild 
						FROM Category C WHERE (numCategoryID = @numCategoryID OR ISNULL(@numCategoryID,0) = 0) AND numDomainID = @numDomainID
					UNION ALL
					SELECT  C.numCategoryID,C.tintLevel,C.numDepCategory,CTE.bitChild FROM Category C JOIN CTE ON 
					C.numDepCategory=CTE.numCategoryID AND CTE.bitChild=1 AND numDomainID = @numDomainID)
					
					INSERT INTO #tmpItemCat(numCategoryID)												 
					SELECT DISTINCT C.numCategoryID FROM CTE C JOIN dbo.ItemCategory IC ON C.numCategoryID = IC.numCategoryID 
														WHERE numItemID IN ( 
																						
													 						SELECT numItemID FROM WareHouseItems WI
																			INNER JOIN dbo.Item I ON I.numItemCode = WI.numItemID AND I.numDomainID = @numDomainID
																			GROUP BY numItemID 
																			HAVING SUM(WI.numOnHand) > 0 
																			UNION ALL
													 						SELECT numItemID FROM WareHouseItems WI
																			INNER JOIN dbo.Item I ON I.numItemCode = WI.numItemID AND I.numDomainID = @numDomainID
																			GROUP BY numItemID 
																			HAVING SUM(WI.numAllocation) > 0
																		 )		
					SELECT numItemID INTO #tmpOnHandItems FROM 
					(													 					
						SELECT numItemID FROM WareHouseItems WI 
						INNER JOIN dbo.Item I ON I.numItemCode = WI.numItemID AND I.numDomainID = @numDomainID
						GROUP BY numItemID 
						HAVING SUM(WI.numOnHand) > 0
						UNION ALL
						SELECT numItemID FROM WareHouseItems WI 
						INNER JOIN dbo.Item I ON I.numItemCode = WI.numItemID AND I.numDomainID = @numDomainID
						GROUP BY numItemID 
						HAVING SUM(WI.numAllocation) > 0				
					) TABLE1
				END

			END

		       SET @strSQL = @strSQL + ' WITH Items AS 
	                                     ( 
	                                     SELECT   
                                               I.numItemCode,
											   I.bitMatrix,
											   I.numItemGroup,
                                               ROW_NUMBER() OVER ( Partition by I.numItemCode Order by I.numItemCode) RowID,
                                               vcItemName,
                                               I.bintCreatedDate,
                                               vcManufacturer,
                                               C.vcCategoryName,
                                               ISNULL(txtItemDesc,'''') txtItemDesc,
                                               ISNULL(CASE  WHEN I.[charItemType] = ''P''
															THEN ( UOM * ISNULL(W1.[monWListPrice], 0) )
															ELSE ( UOM * monListPrice )
													  END, 0) monListPrice,
                                               vcSKU,
                                               fltHeight,
                                               fltWidth,
                                               fltLength,
                                               IM.vcPathForImage,
                                               IM.vcPathForTImage,
                                               vcModelID,
                                               fltWeight,
                                               bitFreeShipping,
                                               UOM AS UOMConversionFactor,
											   UOMPurchase AS UOMPurchaseConversionFactor,
												( CASE WHEN charItemType <> ''S'' AND charItemType <> ''N''
													   THEN ( CASE WHEN bitShowInStock = 1
																   THEN ( CASE WHEN ( ISNULL(W.numOnHand,0)<=0) THEN CASE WHEN ' + CONVERT(VARCHAR(10),@numDomainID) + ' = 172 THEN ''<font color=red>ON HOLD</font>'' ELSE ''<font color=red>Out Of Stock</font>'' END
																			   WHEN bitAllowBackOrder = 1 THEN CASE WHEN ' + CONVERT(VARCHAR(10),@numDomainID) + ' = 172 THEN ''<font color=green>AVAILABLE</font>'' ELSE ''<font color=green>In Stock</font>'' END 
																			   ELSE CASE WHEN ' + CONVERT(VARCHAR(10),@numDomainID) + ' = 172 THEN ''<font color=green>AVAILABLE</font>'' ELSE ''<font color=green>In Stock</font>'' END  
																		  END )
																   ELSE ''''
															  END )
													   ELSE ''''
												  END ) AS InStock, C.vcDescription as CategoryDesc, ISNULL(W1.numWareHouseItemID,0) [numWareHouseItemID] ' 
								               +	@searchPositionColumn + @SortFields + @DefaultSortFields + ',
											   (SELECT COUNT(numProId) 
													FROM 
														PromotionOffer PO
													WHERE 
														numDomainId='+CAST(@numDomainID AS VARCHAR(20))+' 
														AND ISNULL(bitEnabled,0)=1
														AND ISNULL(bitAppliesToSite,0)=1 
														AND ISNULL(bitRequireCouponCode,0)=0
														AND 1 = (CASE WHEN bitRequireCouponCode = 1 THEN CASE WHEN ISNULL(intCouponCodeUsed,0) < ISNULL(tintUsageLimit,0) OR ISNULL(tintUsageLimit,0) = 0  THEN 1 ELSE 0 END ELSE 1 END)
														AND 1 = (CASE WHEN bitNeverExpires = 1 THEN 1 ELSE (CASE WHEN CAST(GETDATE() AS DATE)>=dtValidFrom AND CAST(GETDATE() AS DATE)<=dtValidTo THEN 1 ELSE 0 END) END)
														AND (1 = (CASE 
																	WHEN tintOfferBasedOn = 1 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId=PO.numProId AND tintRecordType=5 AND tintType=1 AND numValue IN (SELECT numOppAccAttrID FROM ItemGroupsDTL WHERE numItemGroupID IN (SELECT numItemGroupID FROM ItemGroups WHERE numProfileItem=I.numItemCode AND numDomainID='+CAST(@numDomainID AS VARCHAR(20))+' ) UNION SELECT I.numItemCode)) > 0 THEN 1 ELSE 0 END)
																	WHEN tintOfferBasedOn = 2 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId=PO.numProId AND tintRecordType=5 AND tintType=2 AND numValue=I.numItemClassification) > 0 THEN 1 ELSE 0 END)
																	ELSE 0
																END)
															OR
															1 = (CASE 
																	WHEN tintDiscoutBaseOn = 1 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId=PO.numProId AND tintRecordType=6 AND tintType=1 AND numValue IN (SELECT numOppAccAttrID FROM ItemGroupsDTL WHERE numItemGroupID IN (SELECT numItemGroupID FROM ItemGroups WHERE numProfileItem=I.numItemCode AND numDomainID='+CAST(@numDomainID AS VARCHAR(20))+' ) UNION SELECT I.numItemCode)) > 0 THEN 1 ELSE 0 END)
																	WHEN tintDiscoutBaseOn = 2 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId=PO.numProId AND tintRecordType=6 AND tintType=2 AND numValue=I.numItemClassification) > 0 THEN 1 ELSE 0 END)
																	WHEN tintDiscoutBaseOn = 3 AND tintOfferBasedOn = 1 THEN (CASE WHEN (SELECT COUNT(SimilarItems.numItemCode) FROM SimilarItems WHERE numParentItemCode IN (SELECT numValue FROM PromotionOfferItems WHERE numProId=PO.numProId AND tintRecordType=5 AND tintType=1 AND SimilarItems.numItemCode=I.numItemCode))>0 THEN 1 ELSE 0 END)
																	WHEN tintDiscoutBaseOn = 3 AND tintOfferBasedOn = 2 THEN (CASE WHEN (SELECT COUNT(SimilarItems.numItemCode) FROM SimilarItems WHERE SimilarItems.numItemCode IN(I.numItemCode) AND numParentItemCode IN (SELECT numItemCode FROM Item WHERE numItemClassification IN (SELECT numValue FROM PromotionOfferItems WHERE numProId=PO.numProId AND tintRecordType=5 AND tintType=2)))>0 THEN 1 ELSE 0 END)
																	ELSE 0
																END)
															)
													)  IsOnSale
								
                                         FROM      
                                                      Item AS I
                                         INNER JOIN   ItemCategory IC   ON   I.numItemCode = IC.numItemID
                                         LEFT JOIN    ItemImages IM     ON   I.numItemCode = IM.numItemCode AND IM.bitdefault=1 AND IM.bitIsImage = 1
                                         INNER JOIN   Category C        ON   IC.numCategoryID = C.numCategoryID
                                         INNER JOIN   SiteCategories SC ON   IC.numCategoryID = SC.numCategoryID
                                         INNER JOIN   eCommerceDTL E    ON   E.numDomainID = I.numDomainID AND E.numSiteId = SC.numSiteID ' + @Join + ' ' + @DefaultSortJoin + ' 
                                         OUTER APPLY (SELECT SUM(numOnHand) numOnHand FROM WareHouseItems W 
													  WHERE  W.numItemID = I.numItemCode
                                                      AND numWareHouseID IN (SELECT [OutParam] FROM dbo.[SplitString](''' + @vcWarehouseIDs + ''','',''))
                                                      AND W.numDomainID = ' + CONVERT(VARCHAR(10),@numDomainID) + ') AS W  
										 OUTER APPLY (SELECT TOP 1 numWareHouseItemID,monWListPrice FROM WareHouseItems W 
													  WHERE  W.numItemID = I.numItemCode 
													  AND W.numDomainID = ' + CONVERT(VARCHAR(10),@numDomainID) + '
													  AND numWareHouseID IN (SELECT [OutParam] FROM dbo.[SplitString](''' + @vcWarehouseIDs + ''','',''))) AS W1
										 OUTER APPLY (SELECT SUM(numAllocation) numAllocation FROM WareHouseItems W2 
													  WHERE  W2.numItemID = I.numItemCode
                                                      AND numWareHouseID IN (SELECT [OutParam] FROM dbo.[SplitString](''' + @vcWarehouseIDs + ''','',''))
                                                      AND W2.numDomainID = ' + CONVERT(VARCHAR(10),@numDomainID) + ') AS W2  			  
										 CROSS APPLY (SELECT dbo.fn_UOMConversion(ISNULL(I.numSaleUnit, 0),I.numItemCode, I.numDomainId,ISNULL(I.numBaseUnit, 0)) AS UOM) AS UOM
										 CROSS APPLY (SELECT dbo.fn_UOMConversion(ISNULL(I.numPurchaseUnit, 0),I.numItemCode, I.numDomainId,ISNULL(I.numBaseUnit, 0)) AS UOMPurchase) AS UOMPurchase '

			IF LEN(@FilterCustomFields) > 0 AND LEN(@FilterCustomWhere) > 0
			BEGIN
				SET @strSQL = @strSQL + ' INNER JOIN #tmpItemCode TP ON TP.numItemCode = I.numItemCode ' 
			END
			
			IF (SELECT COUNT(*) FROM #tmpItemCat) > 0
			BEGIN
				SET @strSQL = @strSQL + ' INNER JOIN #tmpItemCat TC ON TC.numCategoryID = IC.numCategoryID '
				SET @strSQL = @strSQL + ' LEFT JOIN #tmpOnHandItems TOIC ON TOIC.numItemID = I.numItemCode '
			END
			ELSE IF @numCategoryID>0
			BEGIN
				SET @Where = REPLACE(@Where,'WHERE',' WHERE IC.numCategoryID = ' + CONVERT(VARCHAR(10),@numCategoryID) + ' AND ')
			END 
			
			IF @tintDisplayCategory = 1
			BEGIN
				SET @Where = @Where + ' AND 1 = (CASE WHEN (I.charItemType=''P'' AND ISNULL(I.bitKitParent,0) = 0) THEN CASE WHEN (ISNULL(W.numOnHand, 0) > 0 OR ISNULL(W2.numAllocation, 0) > 0) THEN 1 ELSE 0 END ELSE 1 END)'
			END
			
			--PRINT @Where
			SET @strSQL = @strSQL + @Where
			
			IF LEN(@FilterRegularCondition) > 0
			BEGIN
				SET @strSQL = @strSQL + @FilterRegularCondition
			END                                         
			
---PRINT @SortString + ',' + @DefaultSort + ' ' + @vcSort 
	
            IF LEN(@whereNumItemCode) > 0
                                        BEGIN
								              SET @strSQL = @strSQL + @filterByNumItemCode	
								    	END
            SET @strSQL = @strSQL +     '   ) SELECT * INTO #TEMPItems FROM Items
                                        
									DELETE FROM 
										#TEMPItems
									WHERE 
										numItemCode IN (
															SELECT 
																F.numItemCode
															FROM 
																#TEMPItems AS F
															WHERE 
																EXISTS (
																			SELECT 
																				t1.vcItemName, t1.bitMatrix, t1.numItemGroup, Count(t1.numItemCode)
																			FROM 
																				#TEMPItems t1
																			WHERE 
																				t1.vcItemName = F.vcItemName
																				AND t1.bitMatrix = F.bitMatrix
																				AND t1.numItemGroup = F.numItemGroup
																			GROUP BY 
																				t1.vcItemName, t1.bitMatrix, t1.numItemGroup
																			HAVING 
																				Count(t1.numItemCode) > 1
																		)
														)
										AND numItemCode NOT IN (
																				SELECT 
																					Min(numItemCode)
																				FROM 
																					#TEMPItems AS F
																				WHERE 
																					Exists (
																								SELECT 
																									t2.vcItemName, t2.bitMatrix, t2.numItemGroup, Count(t2.numItemCode)
																								FROM 
																									#TEMPItems t2
																								WHERE 
																									t2.vcItemName = F.vcItemName
																								   AND t2.bitMatrix = F.bitMatrix
																								   AND t2.numItemGroup = F.numItemGroup
																								GROUP BY 
																									t2.vcItemName, t2.bitMatrix, t2.numItemGroup
																								HAVING 
																									Count(t2.numItemCode) > 1
																							)
																				GROUP BY 
																					vcItemName, bitMatrix, numItemGroup
																			);  WITH ItemSorted AS (SELECT  ROW_NUMBER() OVER ( ORDER BY  ' + @SortString + @DefaultSort + ' ' + @vcSort + ' ) AS Rownumber,* FROM  #TEMPItems WHERE RowID = 1)'



            DECLARE @fldList AS NVARCHAR(MAX)
			SELECT @fldList=COALESCE(@fldList + ',','') + QUOTENAME(REPLACE(Fld_label,' ','') + '_C') FROM dbo.CFW_Fld_Master WHERE numDomainID=@numDomainID AND Grp_id=5
            
            SET @strSQL = @strSQL +' SELECT  Rownumber,numItemCode,bitMatrix,numItemGroup,txtItemDesc,vcManufacturer,monListPrice,vcItemName,vcCategoryName,vcPathForImage,vcpathForTImage,vcSKU,fltHeight,fltWidth,fltLength,vcModelID,fltWeight,bitFreeShipping,UOMConversionFactor,UOMPurchaseConversionFactor,InStock,CategoryDesc,numWareHouseItemID,IsOnSale
                                     INTO #tempItem FROM ItemSorted ' 

			
            
            SET @strSQL = @strSQL + 'SELECT * INTO #tmpPagedItems FROM #tempItem WHERE Rownumber > ' + CONVERT(VARCHAR(10), @firstRec) + '
                                      AND   Rownumber < ' + CONVERT(VARCHAR(10), @lastRec) + '
                                      order by Rownumber '
                                                                  
            IF LEN(ISNULL(@fldList,''))>0
            BEGIN
				PRINT 'flds1'
				SET @strSQL = @strSQL +'SELECT  Rownumber,numItemCode,txtItemDesc,vcManufacturer,monListPrice,vcItemName,vcCategoryName,vcPathForImage,vcpathForTImage,vcSKU,fltHeight,fltWidth,fltLength,vcModelID,fltWeight,bitFreeShipping,UOMConversionFactor,UOMPurchaseConversionFactor,InStock,CategoryDesc,numWareHouseItemID,IsOnSale,' + @fldList + '
                                        FROM #tmpPagedItems I left join (
			SELECT  t2.RecId, REPLACE(t1.Fld_label,'' '','''') + ''_C'' as Fld_label, 
				CASE t1.fld_type WHEN ''SelectBox'' THEN isnull(dbo.fn_GetListItemName(t2.Fld_Value),'''')
								 WHEN ''CheckBox'' THEN isnull(dbo.fn_getCustomDataValue(t2.Fld_Value,9),'''')
								 WHEN ''DateField'' THEN dbo.FormatedDateFromDate(t2.Fld_Value,' + CAST(@numDomainId AS VARCHAR(18)) +')
								 ELSE Replace(ISNULL(t2.Fld_Value,''0''),''$'',''$$'') end AS Fld_Value FROM CFW_Fld_Master t1 
			JOIN CFW_FLD_Values_Item AS t2 ON t1.Fld_ID = t2.Fld_ID where t1.Grp_id = 5 and t1.numDomainID=' + CAST(@numDomainId AS VARCHAR(18)) +' AND t1.fld_type <> ''Link''
			AND t2.RecId IN (SELECT numItemCode FROM #tmpPagedItems )
			) p PIVOT (MAX([Fld_Value]) FOR Fld_label IN (' + @fldList + ')) AS pvt on I.numItemCode=pvt.RecId order by Rownumber;' 	
			END 
			ELSE
			BEGIN
				PRINT 'flds2'
				SET @strSQL = @strSQL +' SELECT  Rownumber,numItemCode,txtItemDesc,vcManufacturer,monListPrice,vcItemName,vcCategoryName,vcPathForImage,vcpathForTImage,vcSKU,fltHeight,fltWidth,fltLength,vcModelID,fltWeight,bitFreeShipping,UOMConversionFactor,UOMPurchaseConversionFactor,InStock,CategoryDesc,numWareHouseItemID,IsOnSale
                                         FROM  #tmpPagedItems order by Rownumber;'
			END               
                                        
            
--            PRINT(@strSQL)        
--            EXEC ( @strSQL) ; 
			                           
            SET @strSQL = @strSQL + ' SELECT @TotRecs = COUNT(*) FROM #tempItem '                            

			 PRINT  LEN(@strSQL)
			 DECLARE @tmpSQL AS VARCHAR(MAX)
			 SET @tmpSQL = @strSQL
			 PRINT  @tmpSQL
			 
             EXEC sp_executeSQL @strSQL, N'@TotRecs INT OUTPUT', @TotRecs OUTPUT
             
             
        DECLARE @tintOrder AS TINYINT                                                  
		DECLARE @vcFormFieldName AS VARCHAR(50)                                                  
		DECLARE @vcListItemType AS VARCHAR(1)                                             
		DECLARE @vcAssociatedControlType VARCHAR(10)                                                  
		DECLARE @numListID AS NUMERIC(9)                                                  
		DECLARE @WhereCondition VARCHAR(MAX)                       
		DECLARE @numFormFieldId AS NUMERIC  
		DECLARE @vcFieldType CHAR(1)
		                  
		SET @tintOrder=0                                                  
		SET @WhereCondition =''                 
                   
              
		CREATE TABLE #tempAvailableFields(numFormFieldId  NUMERIC(9),vcFormFieldName NVARCHAR(50),
		vcFieldType CHAR(1),vcAssociatedControlType NVARCHAR(50),numListID NUMERIC(18),
		vcListItemType CHAR(1),intRowNum INT,vcItemValue VARCHAR(3000))
		   
		INSERT INTO #tempAvailableFields (numFormFieldId,vcFormFieldName,vcFieldType,vcAssociatedControlType
				,numListID,vcListItemType,intRowNum)                         
		SELECT  Fld_id,REPLACE(Fld_label,' ','') AS vcFormFieldName,
			   CASE GRP_ID WHEN 4 then 'C' WHEN 1 THEN 'D' ELSE 'C' END AS vcFieldType,
				fld_type as vcAssociatedControlType,
				ISNULL(C.numListID, 0) numListID,
				CASE WHEN C.numListID > 0 THEN 'L'
					 ELSE ''
				END vcListItemType,ROW_NUMBER() OVER (ORDER BY Fld_id ASC) AS intRowNum
		FROM    CFW_Fld_Master C LEFT JOIN CFW_Validation V ON V.numFieldID = C.Fld_id
		WHERE   C.numDomainID = @numDomainId
				AND GRP_ID IN (5)

		  
		SELECT * FROM #tempAvailableFields
		
		/*
		DROP TABLE #tempAvailableFields
		DROP TABLE #tmpItemCode
		DROP TABLE #tmpItemCat
		*/	

		IF OBJECT_ID('tempdb..#tempAvailableFields') IS NOT NULL
		DROP TABLE #tempAvailableFields
		
		IF OBJECT_ID('tempdb..#tmpItemCode') IS NOT NULL
		DROP TABLE #tmpItemCode
		
		IF OBJECT_ID('tempdb..#tmpItemCat') IS NOT NULL
		DROP TABLE #tmpItemCat
		
		IF OBJECT_ID('tempdb..#fldValues') IS NOT NULL
		DROP TABLE #fldValues
		
		IF OBJECT_ID('tempdb..#tempSort') IS NOT NULL
		DROP TABLE #tempSort
		
		IF OBJECT_ID('tempdb..#fldDefaultValues') IS NOT NULL
		DROP TABLE #fldDefaultValues
		
		IF OBJECT_ID('tempdb..#tmpOnHandItems') IS NOT NULL
		DROP TABLE #tmpOnHandItems

		IF OBJECT_ID('tempdb..#tmpPagedItems') IS NOT NULL
		DROP TABLE #tmpPagedItems


		IF OBJECT_ID('tempdb..#TEMPItems') IS NOT NULL
		DROP TABLE #TEMPItems
    END


/****** Object:  StoredProcedure [dbo].[USP_LoadAttributesEcommerce]    Script Date: 07/26/2008 16:19:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--exec USP_LoadAttributes 14,66,'834,837'      
--created by anoop jayaraj      
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_loadattributesecommerce')
DROP PROCEDURE usp_loadattributesecommerce
GO
CREATE PROCEDURE [dbo].[USP_LoadAttributesEcommerce]      
@numItemCode as numeric(9)=0,      
@numListID as numeric(9)=0,      
@strAtrr as varchar(1000)='',    
@numWareHouseID as numeric(9)=0      
AS
BEGIN        
	DECLARE @ItemGroup AS NUMERIC(9)      
	DECLARE @strSQL AS VARCHAR(MAX) 
	DECLARE @chrItemType AS VARCHAR(5)
	DECLARE @numDomainID AS NUMERIC(18,0)
	DECLARE @bitMatrix AS BIT
	DECLARE @vcItemName VARCHAR(500)
	
	SELECT 
		@ItemGroup=numItemgroup
		,@chrItemType=charItemType
		,@numDomainID=numDomainID 
		,@bitMatrix=ISNULL(bitMatrix,0)
		,@vcItemName=vcItemName
	FROM
		Item 
	WHERE 
		numItemCode=@numItemCode      
      
      
	IF @strAtrr!=''      
	BEGIN
		DECLARE @Cnt INT = 1
		DECLARE @SplitOn CHAR(1)      
	 
		SET @SplitOn = ','    
		
		IF @bitMatrix = 1
		BEGIN
			SET @strSQL = 'SELECT Item.numItemCode FROM ItemAttributes INNER JOIN Item ON ItemAttributes.numItemCode = Item.numItemCode  WHERE'
		END
		ELSE
		BEGIN  
			SET @strSQL = 'SELECT recid FROM CFW_Fld_Values_Serialized_Items WHERE'  
		END
		    
		While(CHARINDEX(@SplitOn,@strAtrr) > 0)      
		BEGIN      
			IF @Cnt=1 
				SET @strSQL = @strSQL+' fld_value=''' + (ltrim(rtrim(Substring(@strAtrr,1,Charindex(@SplitOn,@strAtrr)-1))))+''''      
			ELSE 
				SET @strSQL=@strSQL+' OR fld_value=''' + (ltrim(rtrim(Substring(@strAtrr,1,Charindex(@SplitOn,@strAtrr)-1))))+''''      

			SET @strAtrr = SUBSTRING(@strAtrr,CHARINDEX(@SplitOn,@strAtrr)+1,len(@strAtrr))      
			
			SET @Cnt = @Cnt + 1      
		End 
		
		IF @bitMatrix = 1
		BEGIN
			IF @Cnt=1 
				SET @strSQL= CONCAT(@strSQL,' fld_value=''',@strAtrr,''' AND ISNULL(bitMatrix,0) = 1 AND ISNULL(Item.numItemGroup,0) = ',@ItemGroup,' AND Item.vcItemName=''',@vcItemName,''' GROUP BY Item.numItemCode HAVING count(*) >',@Cnt-1)      
			ELSE
				SET @strSQL= CONCAT(@strSQL,' OR fld_value=''',@strAtrr,''' AND ISNULL(bitMatrix,0) = 1 AND ISNULL(Item.numItemGroup,0) = ',@ItemGroup,' AND Item.vcItemName=''',@vcItemName,''' GROUP BY Item.numItemCode HAVING count(*) > ', @Cnt-1)
		END
		ELSE
		BEGIN
			IF @Cnt=1 
				SET @strSQL=@strSQL+' fld_value=''' + @strAtrr + ''' GROUP BY recid HAVING count(*) > '+convert(varchar(10), @Cnt-1)      
			ELSE
				SET @strSQL=@strSQL+' OR fld_value=''' + @strAtrr + ''' GROUP BY recid HAVING count(*) > '+convert(varchar(10), @Cnt-1)   
		END   
	END      
    
      
	IF @numItemCode>0      
	BEGIN      
		IF @strAtrr = ''      
		BEGIN
			If @chrItemType <> 'P'
			BEGIN
				SELECT numListItemID,vcData from listdetails where numListID=@numListID AND numDomainID=@numDomainID
			END
			ELSE IF @bitMatrix = 1
			BEGIN
				SELECT 
					numListItemID,vcData 
				FROM 
					ListDetails 
				WHERE 
					numlistitemid IN (
										SELECT 
											DISTINCT(fld_value) 
										FROM 
											Item I
										JOIN
											WareHouseItems W      
										ON
											I.numItemCode=W.numItemID
										JOIN 
											ItemAttributes
										ON 
											I.numItemCode=ItemAttributes.numItemCode      
										JOIN 
											CFW_Fld_Master M 
										ON 
											ItemAttributes.Fld_ID=M.Fld_ID                             
										WHERE 
											I.numDomainID = @numDomainID
											AND W.numDomainID = @numDomainID
											AND M.numListID=@numListID 
											AND vcItemName=@vcItemName
											AND bitMatrix = 1
											AND numItemGroup = @ItemGroup
											AND W.numWareHouseID=@numWareHouseID 
									)      
			END
			ELSE
			BEGIN
				SELECT 
					numListItemID,vcData 
				FROM 
					ListDetails 
				WHERE 
					numlistitemid IN (
										SELECT 
											DISTINCT(fld_value) 
										FROM 
											WareHouseItems W      
										JOIN 
											CFW_Fld_Values_Serialized_Items CSI 
										ON 
											W.numWareHouseItemID=CSI.RecId      
										JOIN 
											CFW_Fld_Master M 
										ON 
											CSI.Fld_ID=M.Fld_ID                        
										JOIN 
											ItemGroupsDTL 
										ON 
											CSI.Fld_ID=ItemGroupsDTL.numOppAccAttrID      
										WHERE 
											M.numListID=@numListID 
											AND tintType=2 
											AND numItemID=@numItemCode 
											AND W.numWareHouseID=@numWareHouseID
									)      
			END
		END
		ELSE      
		BEGIN      
			If @chrItemType <> 'P'
			BEGIN
				select numListItemID,vcData from listdetails where numListID=@numListID AND numDomainID=@numDomainID
			END
			ELSE IF @bitMatrix = 1
			BEGIN
				PRINT 123
				set @strSQL = CONCAT('SELECT 
											numListItemID,vcData 
										FROM 
											ListDetails 
										WHERE 
											numlistitemid IN (
																SELECT 
																	DISTINCT(fld_value) 
																FROM 
																	Item I
																JOIN
																	WareHouseItems W      
																ON
																	I.numItemCode=W.numItemID
																JOIN 
																	ItemAttributes
																ON 
																	I.numItemCode=ItemAttributes.numItemCode      
																JOIN 
																	CFW_Fld_Master M 
																ON 
																	ItemAttributes.Fld_ID=M.Fld_ID                             
																WHERE 
																	I.numDomainID=',@numDomainID,'
																	AND W.numDomainID =',@numDomainID,'
																	AND M.numListID=',@numListID,'
																	AND vcItemName=''',@vcItemName,'''
																	AND bitMatrix = 1
																	AND numItemGroup =',@ItemGroup,'
																	AND W.numWareHouseID=',@numWareHouseID ,'
																	AND I.numItemCode IN (',@strSQL,'))')

				Print @strSQL       
				exec (@strSQL) 
			END
			ELSE
			BEGIN
				set @strSQL ='select numListItemID,vcData from listdetails where numlistitemid in(select distinct(fld_value) from WareHouseItems W      
				join CFW_Fld_Values_Serialized_Items CSI on W.numWareHouseItemID=CSI.RecId      
				join CFW_Fld_Master M on CSI.Fld_ID=M.Fld_ID      
				join ItemGroupsDTL on CSI.Fld_ID=ItemGroupsDTL.numOppAccAttrID     
				where  M.numListID='+convert(varchar(20),@numListID)+' and tintType=2 and CSI.bitSerialized=0 and numItemID='+convert(varchar(20),@numItemCode)+' and W.numWareHouseID='+convert(varchar(20),@numWareHouseID)+' and fld_value!=''0'' and fld_value!=''''     
 
				and W.numWareHouseItemID in ('+@strSQL+'))' 
  
				Print @strSQL       
				exec (@strSQL)      
			END
		END
	END     
	ELSE
	BEGIN
		SELECT 0
	END
END
GO
/****** Object:  StoredProcedure [dbo].[USP_LoadWarehouseAttributesEcomm]    Script Date: 07/26/2008 16:19:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--exec USP_LoadWarehouseAttributes 14,''        
-- exec USP_LoadWarehouseAttributesEcomm @numItemCode='173033',@strAtrr='7270,9708,',@numWareHouseID='63'
--created by anoop jayaraj        
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_loadwarehouseattributesecomm')
DROP PROCEDURE usp_loadwarehouseattributesecomm
GO
CREATE PROCEDURE [dbo].[USP_LoadWarehouseAttributesEcomm]
@numItemCode as varchar(20)='',        
@strAtrr as varchar(1000)='',    
@numWareHouseID as varchar(20)        
AS  
BEGIN
 
	DECLARE @numDomainID AS NUMERIC(18,0)   
       
	DECLARE @strSQL AS VARCHAR(MAX)    
	DECLARE @strAtrrTemp AS VARCHAR(MAX) 
	DECLARE @CharType AS CHAR(1)
	DECLARE @bitMatrix AS BIT
	DECLARE @numItemGroup NUMERIC(18,0)
	DECLARE @vcItemName VARCHAR(500)

    
	SELECT 
		@numDomainID=numDomainID
		,@CharType=charItemType
		,@bitMatrix = ISNULL(bitMatrix,0)
		,@numItemGroup = ISNULL(numItemGroup,0)
		,@vcItemName = vcItemName
	FROM 
		Item 
	WHERE 
		numItemCode=@numItemCode  
  
	SET @strAtrrTemp= @strAtrr       

	IF @strAtrr!=''        
	BEGIN        
		DECLARE @Cnt INT
		DECLARE @vcAttribute AS VARCHAR(100)
	 
		SET @Cnt = 1        
		DECLARE @SplitOn CHAR(1)=','        
	
		IF @bitMatrix = 1
		BEGIN
			SET @strSQL = 'SELECT Item.numItemCode FROM ItemAttributes INNER JOIN Item ON ItemAttributes.numItemCode = Item.numItemCode  WHERE'
		END
		ELSE
		BEGIN  
			SET @strSQL = 'SELECT recid FROM CFW_Fld_Values_Serialized_Items WHERE'  
		END   
	 
		WHILE (Charindex(@SplitOn,@strAtrr)>0)        
		BEGIN
			IF @bitMatrix = 1
			BEGIN
				SET @strSQL=@strSQL + (CASE WHEN @Cnt=1 THEN '' ELSE ' OR ' END) + ' fld_value=CAST(''' + (ltrim(rtrim(Substring(@strAtrr,1,Charindex(@SplitOn,@strAtrr)-1))))+''' AS NUMERIC)'
			END
			ELSE
			BEGIN
				SET @strSQL=@strSQL + (CASE WHEN @Cnt=1 THEN '' ELSE ' OR ' END) + ' fld_value=''' + (ltrim(rtrim(Substring(@strAtrr,1,Charindex(@SplitOn,@strAtrr)-1))))+''''         
			END
			
			SET @strAtrr = SUBSTRING(@strAtrr,CHARINDEX(@SplitOn,@strAtrr)+1,LEN(@strAtrr))
			       
			SET @Cnt = @Cnt + 1        
		End

		IF @bitMatrix = 1
		BEGIN
			SET @strSQL= CONCAT(@strSQL,(CASE WHEN @Cnt=1 THEN '' ELSE ' OR ' END),' fld_value=CAST(''',@strAtrr,''' AS NUMERIC) AND ISNULL(bitMatrix,0) = 1 AND ISNULL(Item.numItemGroup,0) = ',@numItemGroup,' AND Item.vcItemName=''',@vcItemName,'''', ' GROUP BY Item.numItemCode HAVING count(*) > ',@Cnt-1)
		END
		ELSE
		BEGIN
			SET @strSQL=@strSQL + (CASE WHEN @Cnt=1 THEN '' ELSE ' OR ' END) +' fld_value=''' + @strAtrr + ''' GROUP BY recid HAVING count(*) > '+convert(varchar(10), @Cnt-1)  
		END  		    		     
	END 


	IF @strAtrrTemp <> ''
	BEGIN      
		IF @bitMatrix = 1
		BEGIN	
			set @strSQL = 'DECLARE @bitShowInStock AS BIT = 0
						   DECLARE @bitShowQuantity AS BIT = 0   
						   DECLARE @bitAllowBackOrder AS BIT
							
							SELECT 
								@bitShowInStock=isnull(bitShowInStock,0)
								,@bitShowQuantity=isnull(bitShowQOnHand,0) 
							FROM 
								eCommerceDTL 
							WHERE 
								numDomainID='+ CAST(@numDomainID AS VARCHAR) +'
						   
							SELECT @bitAllowBackOrder=bitAllowBackOrder FROM Item Where numItemCode IN (' + @strSQL + ')
						   
							SELECT TOP 1
								numItemID
								,WareHouseItems.numWareHouseItemId
								,ISNULL(sum(numOnHand),0) as numOnHand
								,('+CASE 
									WHEN @CharType<>'S' 
									THEN '(Case 
											when @bitShowInStock=1 then (Case 
																			when @bitAllowBackOrder=1 then ''In Stock'' 
																			else (Case 
																					when isnull(sum(numOnHand),0) > 0 then ''In Stock'' + (Case when @bitShowQuantity=1 then ''(''+convert(varchar(20),isnull(sum(numOnHand),0))+'')'' else '''' end)     
																					else ''<font color=red>Out Of Stock</font>'' 
																					end 
																				) 
																			end
																		)    
											else ''''
											end
										)' 
									ELSE '''' END + ') as InStock
								,monWListPrice  
							FROM 
								WareHouseItems        
							JOIN 
								Warehouses 
							ON 
								Warehouses.numWareHouseID=WareHouseItems.numWareHouseID        
							WHERE 
								numItemID IN ('+ @strSQL +' )
								AND WareHouseItems.numWareHouseID='+@numWareHouseID+' GROUP BY numItemID,numWareHouseItemId,numOnHand,monWListPrice'
		END
		ELSE
		BEGIN
			set @strSQL = '
						   declare @bitShowInStock as bit    
						   declare @bitShowQuantity as bit   
						   declare @bitAllowBackOrder as bit    
							set @bitShowInStock=0    
							set @bitShowQuantity=0  
						   select @bitShowInStock=isnull(bitShowInStock,0),@bitShowQuantity=isnull(bitShowQOnHand,0) from eCommerceDTL where numDomainID='+ CAST(@numDomainID AS VARCHAR) +'
						   select @bitAllowBackOrder=bitAllowBackOrder from item where numItemCode='+@numItemCode+'
						   select numItemID, WareHouseItems.numWareHouseItemId,isnull(sum(numOnHand),0) as numOnHand,('+case when @CharType<>'S' then     
						 '(Case when @bitShowInStock=1 then ( Case when @bitAllowBackOrder=1 then ''In Stock'' else     
						 (Case when isnull(sum(numOnHand),0)>0 then ''In Stock''+(Case when @bitShowQuantity=1 then ''(''+convert(varchar(20),isnull(sum(numOnHand),0))+'')'' else '''' end)     
						 else ''<font color=red>Out Of Stock</font>'' end ) end)    else '''' end)' else '''' end +') as InStock,monWListPrice  from WareHouseItems        
						   join Warehouses on Warehouses.numWareHouseID=WareHouseItems.numWareHouseID        
						   where numItemID='+@numItemCode+' and WareHouseItems.numWareHouseID='+@numWareHouseID+' and numWareHouseItemId in ('+@strSQL+') group by numItemID,numWareHouseItemId,numOnHand,monWListPrice'
		END
	END
	ELSE IF @strAtrrTemp=''        
	BEGIN
		IF @bitMatrix = 1
		BEGIN	
			set @strSQL='
						declare @bitShowInStock as bit    
						declare @bitShowQuantity as bit   
						declare @bitAllowBackOrder as bit    
						set @bitShowInStock=0    
						set @bitShowQuantity=0 
						select @bitShowInStock=isnull(bitShowInStock,0),@bitShowQuantity=isnull(bitShowQOnHand,0) from eCommerceDTL where numDomainID='+ CAST(@numDomainID AS VARCHAR) +'
						select @bitAllowBackOrder=bitAllowBackOrder from item where numItemCode IN (' + @strSQL + ')

						select TOP 1 numItemID, WareHouseItems.numWareHouseItemId,isnull(sum(numOnHand),0) as numOnHand,('+case when @CharType<>'S' then     
						'(Case when @bitShowInStock=1 then ( Case when @bitAllowBackOrder=1 then ''In Stock'' else     
						(Case when isnull(sum(numOnHand),0)>0 then ''In Stock''+(Case when @bitShowQuantity=1 then ''(''+convert(varchar(20),isnull(sum(numOnHand),0))+'')'' else '''' end)     
						else ''<font color=red>Out Of Stock</font>'' end ) end)    else '''' end)' else '''' end +') as InStock,monWListPrice from WareHouseItems        
						join Warehouses on Warehouses.numWareHouseID=WareHouseItems.numWareHouseID        
						where numItemID IN ('+ @strSQL +' )  and WareHouseItems.numWareHouseID='+@numWareHouseID+' group by numItemID,numWareHouseItemId,numOnHand,monWListPrice'
		END
		ELSE 
		BEGIN
			set @strSQL='
						declare @bitShowInStock as bit    
						declare @bitShowQuantity as bit   
						declare @bitAllowBackOrder as bit    
						set @bitShowInStock=0    
						set @bitShowQuantity=0 
						select @bitShowInStock=isnull(bitShowInStock,0),@bitShowQuantity=isnull(bitShowQOnHand,0) from eCommerceDTL where numDomainID='+ CAST(@numDomainID AS VARCHAR) +'
						select @bitAllowBackOrder=bitAllowBackOrder from item where numItemCode='+@numItemCode+'

						select numItemID, WareHouseItems.numWareHouseItemId,isnull(sum(numOnHand),0) as numOnHand,('+case when @CharType<>'S' then     
						'(Case when @bitShowInStock=1 then ( Case when @bitAllowBackOrder=1 then ''In Stock'' else     
						(Case when isnull(sum(numOnHand),0)>0 then ''In Stock''+(Case when @bitShowQuantity=1 then ''(''+convert(varchar(20),isnull(sum(numOnHand),0))+'')'' else '''' end)     
						else ''<font color=red>Out Of Stock</font>'' end ) end)    else '''' end)' else '''' end +') as InStock,monWListPrice from WareHouseItems        
						join Warehouses on Warehouses.numWareHouseID=WareHouseItems.numWareHouseID        
						where numItemID='+@numItemCode +'  and WareHouseItems.numWareHouseID='+@numWareHouseID+' group by numItemID,numWareHouseItemId,numOnHand,monWListPrice'

		END
		   
	END 
	print @strSQL
 exec(@strSQL)              
       
        
	

END
GO
GO
/****** Object:  StoredProcedure [dbo].[USP_ManageItemImages]    Script Date: 04/13/2012 17:45:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ManageItemImages')
DROP PROCEDURE USP_ManageItemImages
GO
/*
BEGIN TRANSACTION
declare @p1 bigint
set @p1=0
exec USP_ManageItemImages @numItemImageId=@p1 output,@numItemCode=195142,@numDomainID=1,@vcPathForImage='i06314.jpg',@vcPathForTImage='i06314.jpg',@bitDefault=0,@intDisplayOrder=0
select @p1

ROLLBACK
*/
CREATE PROCEDURE [dbo].[USP_ManageItemImages]
(
	@numItemImageId  NUMERIC(18)=0 OUTPUT,
	@numItemCode	 NUMERIC(18),
	@numDomainId     NUMERIC(18),
	@vcPathForImage	 VARCHAR(300),
	@vcPathForTImage VARCHAR(300),
	@bitDefault	     BIT,
	@intDisplayOrder INT,
	@bitIsImage	     BIT
)

AS 

BEGIN
  
  IF ISNULL(@bitDefault,0) = 1
	BEGIN
		UPDATE ItemImages SET bitDefault = 0 WHERE numItemCode = @numItemCode AND numDomainId = @numDomainId AND bitIsImage = 1
	END
  		
  IF ISNULL(@numItemImageId,0) = 0
	  BEGIN
	        
		 IF NOT EXISTS(SELECT * FROM ItemImages WHERE numItemCode = @numItemCode AND bitIsImage = 1 AND numDomainId = @numDomainId )
			   BEGIN
				 SET @bitDefault = 1
			   END 
	     ELSE
			     SET @bitDefault = 0
		 
		  IF NOT EXISTS(SELECT * FROM ItemImages WHERE numItemCode = @numItemCode  AND numDomainId = @numDomainId )
				 BEGIN
					SET @intDisplayOrder = 1 	
				 END
		  ELSE
				 BEGIN
				 	SELECT @intDisplayOrder = MAX(ISNULL(intDisplayOrder,0)) + 1 FROM ItemImages 
						WHERE numItemCode = @numItemCode 
						 AND numDomainId = @numDomainId
				 END
	       
		-- PRINT(@intDisplayOrder)
		
		IF((SELECT numItemCode FROM item WHERE numItemCode = @numItemCode AND numDomainId = @numDomainId) > 0)
			 BEGIN
				  INSERT INTO [ItemImages] 
				  ( [numItemCode],[vcPathForImage],[vcPathForTImage],[bitDefault],[intDisplayOrder],[numDomainId] ,[bitIsImage])
				  VALUES
				  ( @numItemCode,@vcPathForImage,ISNULL(@vcPathForTImage,''),@bitDefault,@intDisplayOrder,@numDomainId ,@bitIsImage)
		       
				 SET @numItemImageId = @@IDENTITY   
				 --PRINT @numItemImageId
			 END		  
	   END
   
  ELSE
  
 BEGIN
	
	UPDATE [ItemImages] SET bitDefault = @bitDefault , intDisplayOrder = @intDisplayOrder WHERE numItemImageId = @numItemImageId    
	 END

	 update item set  bintModifiedDate=getutcdate()where numItemCode=@numItemCode  AND numDomainID=@numDomainID
 
END

--exec USP_ManageItemImages @intDisplayOrder = 0 ,@numDomainId = 1 ,@vcPathForImage = "abc" ,@vcPathForTImage = "abc" ,@bitDefault = false ,@numItemCode = 197605
GO
/****** Object:  StoredProcedure [dbo].[USP_ManageItemsAndKits]    Script Date: 02/15/2010 21:44:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created By Anoop Jayaraj                                                          
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_manageitemsandkits')
DROP PROCEDURE usp_manageitemsandkits
GO
CREATE PROCEDURE [dbo].[USP_ManageItemsAndKits]                                                                
@numItemCode as numeric(9) output,                                                                
@vcItemName as varchar(300),                                                                
@txtItemDesc as varchar(1000),                                                                
@charItemType as char(1),                                                                
@monListPrice as money,                                                                
@numItemClassification as numeric(9),                                                                
@bitTaxable as bit,                                                                
@vcSKU as varchar(50),                                                                                                                                      
@bitKitParent as bit,                                                                                   
--@dtDateEntered as datetime,                                                                
@numDomainID as numeric(9),                                                                
@numUserCntID as numeric(9),                                                                                                  
@numVendorID as numeric(9),                                                            
@bitSerialized as bit,                                               
@strFieldList as text,                                            
@vcModelID as varchar(200)='' ,                                            
@numItemGroup as numeric(9)=0,                                      
@numCOGSChartAcntId as numeric(9)=0,                                      
@numAssetChartAcntId as numeric(9)=0,                                      
@numIncomeChartAcntId as numeric(9)=0,                            
@monAverageCost as money,                          
@monLabourCost as money,                  
@fltWeight as float,                  
@fltHeight as float,                  
@fltLength as float,                  
@fltWidth as float,                  
@bitFreeshipping as bit,                
@bitAllowBackOrder as bit,              
@UnitofMeasure as varchar(30)='',              
@strChildItems as text,            
@bitShowDeptItem as bit,              
@bitShowDeptItemDesc as bit,        
@bitCalAmtBasedonDepItems as bit,    
@bitAssembly as BIT,
@intWebApiId INT=null,
@vcApiItemId AS VARCHAR(50)=null,
@numBarCodeId as VARCHAR(25)=null,
@vcManufacturer as varchar(250)=NULL,
@numBaseUnit AS NUMERIC(18)=0,
@numPurchaseUnit AS NUMERIC(18)=0,
@numSaleUnit AS NUMERIC(18)=0,
@bitLotNo as BIT,
@IsArchieve AS BIT,
@numItemClass as numeric(9)=0,
@tintStandardProductIDType AS TINYINT=0,
@vcExportToAPI VARCHAR(50),
@numShipClass NUMERIC,
@ProcedureCallFlag SMALLINT =0,
@vcCategories AS VARCHAR(2000) = '',
@bitAllowDropShip AS BIT=0,
@bitArchiveItem AS BIT = 0,
@bitAsset AS BIT = 0,
@bitRental AS BIT = 0,
@numCategoryProfileID AS NUMERIC(18,0) = 0,
@bitVirtualInventory BIT,
@bitContainer BIT,
@numContainer NUMERIC(18,0)=0,
@numNoItemIntoContainer NUMERIC(18,0),
@bitMatrix BIT = 0 ,
@vcItemAttributes VARCHAR(2000) = ''
AS
BEGIN     
BEGIN TRY
BEGIN TRANSACTION
	DECLARE @bitOppOrderExists AS BIT = 0

	IF ISNULL(@numItemCode,0) > 0
	BEGIN
		IF (SELECT COUNT(OI.numOppId) FROM dbo.OpportunityItems OI WHERE OI.numItemCode =@numItemCode) > 0
		BEGIN
			SET @bitOppOrderExists = 1
		END
		ELSE IF (SELECT COUNT(OI.numOppId) FROM dbo.OpportunityKitItems OI WHERE OI.numChildItemID =@numItemCode) > 0
		BEGIN
			SET @bitOppOrderExists = 1
		END
		ELSE IF (SELECT COUNT(OI.numOppId) FROM dbo.OpportunityKitChildItems OI WHERE OI.numItemID =@numItemCode) > 0
		BEGIN
			SET @bitOppOrderExists = 1
		END
	END

	--INSERT CUSTOM FIELDS BASE ON WAREHOUSEITEMS
	DECLARE @hDoc AS INT     
	                                                                        	
	DECLARE @TempTable TABLE 
	(
		ID INT IDENTITY(1,1),
		Fld_ID NUMERIC(18,0),
		Fld_Value NUMERIC(18,0)
	)   

	IF ISNULL(@bitMatrix,0) = 1 AND ISNULL(@numItemGroup,0) = 0
	BEGIN
		RAISERROR('ITEM_GROUP_NOT_SELECTED',16,1)
		RETURN
	END
	ELSE IF ISNULL(@bitMatrix,0) = 1 AND LEN(ISNULL(@vcItemAttributes,'')) = 0 
	BEGIN
		RAISERROR('ITEM_ATTRIBUTES_NOT_SELECTED',16,1)
		RETURN
	END
	ELSE IF ISNULL(@bitMatrix,0) = 1 
	BEGIN
		DECLARE @bitDuplicate AS BIT = 0
	
		IF DATALENGTH(ISNULL(@vcItemAttributes,''))>2
		BEGIN
			EXEC sp_xml_preparedocument @hDoc OUTPUT, @vcItemAttributes
	                                      
			INSERT INTO @TempTable 
			(
				Fld_ID,
				Fld_Value
			)    
			SELECT
				*          
			FROM 
				OPENXML (@hDoc,'/NewDataSet/CusFlds',2)                                                  
			WITH 
				(Fld_ID NUMERIC(18,0),Fld_Value NUMERIC(18,0)) 
			ORDER BY 
				Fld_ID       

			DECLARE @strAttribute VARCHAR(500) = ''
			DECLARE @i AS INT = 1
			DECLARE @COUNT AS INT 
			DECLARE @numFldID AS NUMERIC(18,0)
			DECLARE @numFldValue AS NUMERIC(18,0)
			SELECT @COUNT = COUNT(*) FROM @TempTable

			WHILE @i <= @COUNT
			BEGIN
				SELECT @numFldID=Fld_ID,@numFldValue=Fld_Value FROM @TempTable WHERE ID=@i

				IF ISNUMERIC(@numFldValue)=1
				BEGIN
					SELECT @strAttribute=@strAttribute+Fld_label FROM CFW_Fld_Master WHERE Fld_id=@numFldID AND Grp_id = 9
                
					IF LEN(@strAttribute) > 0
					BEGIN
						SELECT @strAttribute=@strAttribute+':'+ vcData +',' FROM ListDetails WHERE numListItemID=@numFldValue
					END
				END

				SET @i = @i + 1
			END

			SET @strAttribute = SUBSTRING(@strAttribute,0,LEN(@strAttribute))

			IF
				(SELECT
					COUNT(*)
				FROM
					Item
				INNER JOIN
					ItemAttributes
				ON
					Item.numItemCode = ItemAttributes.numItemCode
					AND ItemAttributes.numDomainID = @numDomainID                     
				WHERE
					Item.numItemCode <> @numItemCode
					AND Item.vcItemName = @vcItemName
					AND Item.numItemGroup = @numItemGroup
					AND Item.numDomainID = @numDomainID
					AND dbo.fn_GetItemAttributes(@numDomainID,Item.numItemCode) = @strAttribute
				) > 0
			BEGIN
				RAISERROR('ITEM_WITH_ATTRIBUTES_ALREADY_EXISTS',16,1)
				RETURN
			END

			EXEC sp_xml_removedocument @hDoc 
		END	
	END
	ELSE IF ISNULL(@bitMatrix,0) = 0
	BEGIN
		-- REMOVE ATTRIBUTES CONFIGURATION OF ITEM AND WAREHOUSE
		DELETE FROM ItemAttributes WHERE numDomainID=@numDomainID AND numItemCode=@numItemCode
	END 
	
	IF ISNULL(@numItemGroup,0) > 0
	BEGIN
		IF (SELECT COUNT(*) FROM ItemGroupsDTL WHERE numItemGroupID = @numItemGroup AND tintType=2) = 0
		BEGIN
			RAISERROR('ITEM_GROUP_NOT_CONFIGURED',16,1)
			RETURN
		END
		ELSE IF EXISTS (SELECT 
							CFM.Fld_id
						FROM 
							CFW_Fld_Master CFM
						LEFT JOIN
							ListDetails LD
						ON
							CFM.numlistid = LD.numListID
						WHERE
							CFM.numDomainID = @numDomainID
							AND CFM.Fld_id IN (SELECT numOppAccAttrID FROM ItemGroupsDTL WHERE numItemGroupID=@numItemGroup AND tintType = 2)
						GROUP BY
							CFM.Fld_id HAVING COUNT(LD.numListItemID) = 0)
		BEGIN
			RAISERROR('ITEM_GROUP_NOT_CONFIGURED',16,1)
			RETURN
		END
	END

	IF @numCOGSChartAcntId=0 SET @numCOGSChartAcntId=NULL
	IF @numAssetChartAcntId=0 SET @numAssetChartAcntId=NULL  
	IF @numIncomeChartAcntId=0 SET @numIncomeChartAcntId=NULL     

	DECLARE @ParentSKU VARCHAR(50) = @vcSKU                        
	DECLARE @ItemID AS NUMERIC(9)
	DECLARE @cnt AS INT

	IF ISNULL(@charItemType,'') = '' OR ISNULL(@charItemType,'') = '0'  
		SET @charItemType = 'N'

	IF @intWebApiId = 2 AND LEN(ISNULL(@vcApiItemId, '')) > 0 AND ISNULL(@numItemGroup, 0) > 0 
    BEGIN 
        -- check wether this id already Mapped in ITemAPI 
        SELECT 
			@cnt = COUNT([numItemID])
        FROM 
			[ItemAPI]
        WHERE 
			[WebApiId] = @intWebApiId
            AND [numDomainId] = @numDomainID
            AND [vcAPIItemID] = @vcApiItemId

        IF @cnt > 0 
        BEGIN
            SELECT 
				@ItemID = [numItemID]
            FROM 
				[ItemAPI]
            WHERE 
				[WebApiId] = @intWebApiId
                AND [numDomainId] = @numDomainID
                AND [vcAPIItemID] = @vcApiItemId
        
			--Update Existing Product coming from API
            SET @numItemCode = @ItemID
        END
    END
	ELSE IF @intWebApiId > 1 AND LEN(ISNULL(@vcSKU, '')) > 0 
    BEGIN
        SET @ParentSKU = @vcSKU
		 
        SELECT 
			@cnt = COUNT([numItemCode])
        FROM 
			[Item]
        WHERE 
			[numDomainId] = @numDomainID
            AND (vcSKU = @vcSKU OR @vcSKU IN (SELECT vcWHSKU FROM dbo.WareHouseItems WHERE numItemID = Item.[numItemCode] AND vcWHSKU = @vcSKU))
            
		IF @cnt > 0 
        BEGIN
            SELECT 
				@ItemID = [numItemCode],
                @ParentSKU = vcSKU
            FROM 
				[Item]
            WHERE 
				[numDomainId] = @numDomainID
                AND (vcSKU = @vcSKU OR @vcSKU IN (SELECT vcWHSKU FROM dbo.WareHouseItems WHERE numItemID = Item.[numItemCode] AND vcWHSKU = @vcSKU))

            SET @numItemCode = @ItemID
        END
		ELSE 
		BEGIN
			-- check wether this id already Mapped in ITemAPI 
			SELECT 
				@cnt = COUNT([numItemID])
			FROM 
				[ItemAPI]
			WHERE 
				[WebApiId] = @intWebApiId
				AND [numDomainId] = @numDomainID
				AND [vcAPIItemID] = @vcApiItemId
                    
			IF @cnt > 0 
			BEGIN
				SELECT 
					@ItemID = [numItemID]
				FROM 
					[ItemAPI]
				WHERE 
					[WebApiId] = @intWebApiId
					AND [numDomainId] = @numDomainID
					AND [vcAPIItemID] = @vcApiItemId
        
				--Update Existing Product coming from API
				SET @numItemCode = @ItemID
			END
		END
    END
                                                         
	IF @numItemCode=0 OR @numItemCode IS NULL
	BEGIN
		INSERT INTO Item 
		(                                             
			vcItemName,txtItemDesc,charItemType,monListPrice,numItemClassification,bitTaxable,vcSKU,bitKitParent,
			numVendorID,numDomainID,numCreatedBy,bintCreatedDate,bintModifiedDate,numModifiedBy,bitSerialized,
			vcModelID,numItemGroup,numCOGsChartAcntId,numAssetChartAcntId,numIncomeChartAcntId,monAverageCost,                          
			monCampaignLabourCost,fltWeight,fltHeight,fltWidth,fltLength,bitFreeShipping,bitAllowBackOrder,vcUnitofMeasure,
			bitShowDeptItem,bitShowDeptItemDesc,bitCalAmtBasedonDepItems,bitAssembly,numBarCodeId,vcManufacturer,numBaseUnit,
			numPurchaseUnit,numSaleUnit,bitLotNo,IsArchieve,numItemClass,tintStandardProductIDType,vcExportToAPI,numShipClass,
			bitAllowDropShip,bitArchiveItem,bitAsset,bitRental,bitVirtualInventory,bitContainer,numContainer,numNoItemIntoContainer,bitMatrix
		)                                                              
		VALUES                                                                
		(                                                                
			@vcItemName,@txtItemDesc,@charItemType,@monListPrice,@numItemClassification,@bitTaxable,@ParentSKU,@bitKitParent,
			@numVendorID,@numDomainID,@numUserCntID,getutcdate(),getutcdate(),@numUserCntID,@bitSerialized,@vcModelID,@numItemGroup,                                      
			@numCOGsChartAcntId,@numAssetChartAcntId,@numIncomeChartAcntId,@monAverageCost,@monLabourCost,@fltWeight,@fltHeight,@fltWidth,                  
			@fltLength,@bitFreeshipping,@bitAllowBackOrder,@UnitofMeasure,@bitShowDeptItem,@bitShowDeptItemDesc,@bitCalAmtBasedonDepItems,    
			@bitAssembly,@numBarCodeId,@vcManufacturer,@numBaseUnit,@numPurchaseUnit,@numSaleUnit,@bitLotNo,@IsArchieve,@numItemClass,
			@tintStandardProductIDType,@vcExportToAPI,@numShipClass,@bitAllowDropShip,@bitArchiveItem ,@bitAsset,@bitRental,
			@bitVirtualInventory,@bitContainer,@numContainer,@numNoItemIntoContainer,@bitMatrix
		)                                             
 
		SET @numItemCode = SCOPE_IDENTITY()
  
		IF  @intWebApiId > 1 
		BEGIN
			 -- insert new product
			 --insert this id into linking table
			 INSERT INTO [ItemAPI] (
	 			[WebApiId],
	 			[numDomainId],
	 			[numItemID],
	 			[vcAPIItemID],
	 			[numCreatedby],
	 			[dtCreated],
	 			[numModifiedby],
	 			[dtModified],vcSKU
			 ) VALUES     
			 (
				@intWebApiId,
				@numDomainID,
				@numItemCode,
				@vcApiItemId,
	 			@numUserCntID,
	 			GETUTCDATE(),
	 			@numUserCntID,
	 			GETUTCDATE(),@vcSKU
	 		) 
		END
 
 
	END         
	ELSE IF @numItemCode>0 AND @intWebApiId > 0 
	BEGIN 
		IF @ProcedureCallFlag = 1 
		BEGIN
			DECLARE @ExportToAPIList AS VARCHAR(30)
			SELECT @ExportToAPIList = vcExportToAPI FROM dbo.Item WHERE numItemCode = @numItemCode

			IF @ExportToAPIList != ''
			BEGIN
				IF NOT EXISTS(select * from (SELECT * FROM dbo.Split(@ExportToAPIList ,','))as A where items= @vcExportToAPI ) 
				BEGIN
					SET @vcExportToAPI = @ExportToAPIList + ',' + @vcExportToAPI
				End
				ELSE
				BEGIN
					SET @vcExportToAPI = @ExportToAPIList 
				End
			END

			--update ExportToAPI String value
			UPDATE dbo.Item SET vcExportToAPI=@vcExportToAPI where numItemCode=@numItemCode  AND numDomainID=@numDomainID
					
			SELECT 
				@cnt = COUNT([numItemID]) 
			FROM 
				[ItemAPI] 
			WHERE  
				[WebApiId] = @intWebApiId 
				AND [numDomainId] = @numDomainID
				AND [vcAPIItemID] = @vcApiItemId
				AND numItemID = @numItemCode

			IF @cnt > 0
			BEGIN
				--update lastUpdated Date 
				UPDATE [ItemAPI] SET [dtModified] =GETUTCDATE() WHERE [WebApiId] =@intWebApiId AND [numDomainId] = @numDomainID
			END
			ELSE
			--Insert ItemAPI mapping
			BEGIN
				INSERT INTO [ItemAPI] 
				(
					[WebApiId],
					[numDomainId],
					[numItemID],
					[vcAPIItemID],
					[numCreatedby],
					[dtCreated],
					[numModifiedby],
					[dtModified],vcSKU
				) VALUES 
				(
					@intWebApiId,
					@numDomainID,
					@numItemCode,
					@vcApiItemId,
					@numUserCntID,
					GETUTCDATE(),
					@numUserCntID,
					GETUTCDATE(),@vcSKU
				)	
			END
		END   
	END                                            
	ELSE IF @numItemCode > 0 AND @intWebApiId <= 0                                                           
	BEGIN
		DECLARE @OldGroupID NUMERIC 
		SELECT @OldGroupID = ISNULL(numItemGroup,0) FROM [Item] WHERE numitemcode = @numItemCode AND [numDomainId] = @numDomainID
	
		DECLARE @monOldAverageCost AS DECIMAL(18,4);SET @monOldAverageCost=0
		SELECT @monOldAverageCost=(CASE WHEN ISNULL(bitVirtualInventory,0) = 1 THEN 0 ELSE ISNULL(monAverageCost,0) END) FROM Item WHERE numItemCode =@numItemCode AND [numDomainId] = @numDomainID
	   
		UPDATE 
			Item 
		SET 
			vcItemName=@vcItemName,txtItemDesc=@txtItemDesc,charItemType=@charItemType,monListPrice= @monListPrice,numItemClassification=@numItemClassification,                                             
			bitTaxable=@bitTaxable,vcSKU = (CASE WHEN ISNULL(@ParentSKU,'') = '' THEN @vcSKU ELSE @ParentSKU END),bitKitParent=@bitKitParent,                                             
			numVendorID=@numVendorID,numDomainID=@numDomainID,bintModifiedDate=getutcdate(),numModifiedBy=@numUserCntID,bitSerialized=@bitSerialized,                                                         
			vcModelID=@vcModelID,numItemGroup=@numItemGroup,numCOGsChartAcntId=@numCOGsChartAcntId,numAssetChartAcntId=@numAssetChartAcntId,                                      
			numIncomeChartAcntId=@numIncomeChartAcntId,monCampaignLabourCost=@monLabourCost,fltWeight=@fltWeight,fltHeight=@fltHeight,fltWidth=@fltWidth,                  
			fltLength=@fltLength,bitFreeShipping=@bitFreeshipping,bitAllowBackOrder=@bitAllowBackOrder,vcUnitofMeasure=@UnitofMeasure,bitShowDeptItem=@bitShowDeptItem,            
			bitShowDeptItemDesc=@bitShowDeptItemDesc,bitCalAmtBasedonDepItems=@bitCalAmtBasedonDepItems,bitAssembly=@bitAssembly,numBarCodeId=@numBarCodeId,
			vcManufacturer=@vcManufacturer,numBaseUnit=@numBaseUnit,numPurchaseUnit=@numPurchaseUnit,numSaleUnit=@numSaleUnit,bitLotNo=@bitLotNo,
			IsArchieve = @IsArchieve,numItemClass=@numItemClass,tintStandardProductIDType=@tintStandardProductIDType,vcExportToAPI=@vcExportToAPI,
			numShipClass=@numShipClass,bitAllowDropShip=@bitAllowDropShip, bitArchiveItem = @bitArchiveItem,bitAsset=@bitAsset,bitRental=@bitRental,
			bitVirtualInventory = @bitVirtualInventory,numContainer=@numContainer,numNoItemIntoContainer=@numNoItemIntoContainer,bitMatrix=@bitMatrix
		WHERE 
			numItemCode=@numItemCode 
			AND numDomainID=@numDomainID

		IF NOT EXISTS(SELECT numItemCode FROM dbo.OpportunityItems WHERE numItemCode = @numItemCode)
		BEGIN
			IF (((SELECT ISNULL(SUM(numOnHand),0) FROM WareHouseItems WHERE numItemID = @numItemCode AND numDomainId=@numDomainID) = 0) AND ((SELECT (CASE WHEN ISNULL(bitVirtualInventory,0) = 1 THEN 0 ELSE ISNULL(monAverageCost,0) END) FROM Item  WHERE numItemCode = @numItemCode AND numDomainId=@numDomainID ) = 0))
			BEGIN
				UPDATE Item SET monAverageCost=@monAverageCost  where numItemCode=@numItemCode  AND numDomainID=@numDomainID	
			END
		END
	
		--If Average Cost changed then add in Tracking
		IF ISNULL(@monAverageCost,0) != ISNULL(@monOldAverageCost,0)
		BEGIN
			DECLARE @numWareHouseItemID NUMERIC(9);SET @numWareHouseItemID=0
			DECLARE @numTotal int;SET @numTotal=0
			SET @i=0
			DECLARE @vcDescription AS VARCHAR(100);SET @vcDescription=''
		
			SELECT @numTotal=COUNT(*) FROM WareHouseItems where numItemID=@numItemCode AND numDomainId=@numDomainId
	     
			WHILE @i < @numTotal
			BEGIN
				SELECT @numWareHouseItemID = min(numWareHouseItemID) FROM WareHouseItems 
					WHERE numItemID=@numItemCode AND numDomainId=@numDomainId and numWareHouseItemID > @numWareHouseItemID 
				
				IF @numWareHouseItemID>0
				BEGIN
					SET @vcDescription='Average Cost Changed Manually OLD : ' + CAST(ISNULL(@monOldAverageCost,0) AS VARCHAR(20))
				
					DECLARE @numDomain AS NUMERIC(18,0)
					SET @numDomain = @numDomainID
					EXEC dbo.USP_ManageWareHouseItems_Tracking
						@numWareHouseItemID = @numWareHouseItemID, --  numeric(9, 0)		
						@numReferenceID = @numItemCode, --  numeric(9, 0)
						@tintRefType = 1, --  tinyint
						@vcDescription = @vcDescription, --  varchar(100)
						@tintMode = 0, --  tinyint
						@numModifiedBy = @numUserCntID, --  numeric(9, 0)
						@numDomainID = @numDomain
				END
		    
				SET @i = @i + 1
			END
		END
 
		DECLARE @bitCartFreeShipping as  bit 
		SELECT @bitCartFreeShipping = bitFreeShipping FROM dbo.CartItems where numItemCode = @numItemCode  AND numDomainID = @numDomainID 
 
		IF @bitCartFreeShipping <> @bitFreeshipping
		BEGIN
			UPDATE dbo.CartItems SET bitFreeShipping = @bitFreeshipping WHERE numItemCode = @numItemCode  AND numDomainID=@numDomainID
		END 
 
		IF @intWebApiId > 1 
		BEGIN
			SELECT 
				@cnt = COUNT([numItemID])
			FROM 
				[ItemAPI]
			WHERE 
				[WebApiId] = @intWebApiId
				AND [numDomainId] = @numDomainID
				AND [vcAPIItemID] = @vcApiItemId

			IF @cnt > 0
			BEGIN
				--update lastUpdated Date 
				UPDATE [ItemAPI] SET [dtModified] =GETUTCDATE() WHERE [WebApiId] =@intWebApiId AND [numDomainId] = @numDomainID
			END
			ELSE --Insert ItemAPI mapping
			BEGIN
				INSERT INTO [ItemAPI] 
				(
	 				[WebApiId],
	 				[numDomainId],
	 				[numItemID],
	 				[vcAPIItemID],
	 				[numCreatedby],
	 				[dtCreated],
	 				[numModifiedby],
	 				[dtModified],vcSKU
				 )
				 VALUES 
				 (
					@intWebApiId,
					@numDomainID,
					@numItemCode,
					@vcApiItemId,
	 				@numUserCntID,
	 				GETUTCDATE(),
	 				@numUserCntID,
	 				GETUTCDATE(),@vcSKU
	 			) 
			END
		END   
		                                                                            
		-- kishan It is necessary to add item into itemTax table . 
		IF	@bitTaxable = 1
		BEGIN
			IF NOT EXISTS(SELECT * FROM dbo.ItemTax WHERE numItemCode = @numItemCode AND numTaxItemID = 0)
			BEGIN
			    INSERT INTO dbo.ItemTax 
				(
					numItemCode,
					numTaxItemID,
					bitApplicable
				)
				VALUES 
				( 
					@numItemCode,
					0,
					1 
				) 						
			END
		END	 
      
		IF @charItemType='S' or @charItemType='N'                                                                       
		BEGIN
			DELETE FROM WareHouseItmsDTL where numWareHouseItemID in (select numWareHouseItemID from WareHouseItems where numItemID=@numItemCode AND [WareHouseItems].[numDomainID] = @numDomainID)
			DELETE FROM WareHouseItems where numItemID=@numItemCode AND [WareHouseItems].[numDomainID] = @numDomainID
		END                                      
   
		IF @bitKitParent=1 OR @bitAssembly = 1              
		BEGIN              
			DECLARE @hDoc1 AS INT                                            
			EXEC sp_xml_preparedocument @hDoc1 OUTPUT, @strChildItems                                                                        
                                             
			UPDATE 
				ItemDetails 
			SET
				numQtyItemsReq=X.QtyItemsReq
				,numWareHouseItemId=X.numWarehouseItmsID
				,numUOMId=X.numUOMId
				,vcItemDesc=X.vcItemDesc
				,sintOrder=X.sintOrder                                                                                          
			From 
			(
				SELECT 
					QtyItemsReq,ItemKitID,ChildItemID,numWarehouseItmsID,numUOMId,vcItemDesc,sintOrder, numItemDetailID As ItemDetailID                             
				FROM 
					OPENXML(@hDoc1,'/NewDataSet/Table1',2)                                                                         
				WITH
				(
					ItemKitID NUMERIC(9),                                                          
					ChildItemID NUMERIC(9),                                                                        
					QtyItemsReq DECIMAL(30, 16),
					numWarehouseItmsID NUMERIC(9),
					numUOMId NUMERIC(9),
					vcItemDesc VARCHAR(1000),
					sintOrder int,
					numItemDetailID NUMERIC(18,0)
				)
			)X                                                                         
			WHERE 
				numItemKitID=X.ItemKitID 
				AND numChildItemID=ChildItemID 
				AND numItemDetailID = X.ItemDetailID

			EXEC sp_xml_removedocument @hDoc1
		 END   
		 ELSE
		 BEGIN
 			DELETE FROM ItemDetails WHERE numItemKitID=@numItemCode
		 END                                                
	END

	IF ISNULL(@numItemGroup,0) = 0 OR ISNULL(@bitMatrix,0) = 1
	BEGIN
		UPDATE WareHouseItems SET monWListPrice = @monListPrice, vcWHSKU=@vcSKU, vcBarCode=@numBarCodeId WHERE numDomainID = @numDomainID AND numItemID = @numItemCode
	END

	IF @numItemCode > 0
	BEGIN
		DELETE 
			IC
		FROM 
			dbo.ItemCategory IC
		INNER JOIN 
			Category 
		ON 
			IC.numCategoryID = Category.numCategoryID
		WHERE 
			numItemID = @numItemCode		
			AND numCategoryProfileID = @numCategoryProfileID

		IF @vcCategories <> ''	
		BEGIN
			INSERT INTO ItemCategory( numItemID , numCategoryID )  SELECT  @numItemCode AS numItemID,ID from dbo.SplitIDs(@vcCategories,',')	
		END
	END

	


	-- SAVE MATRIX ITEM ATTRIBUTES
	IF @numItemCode > 0 AND ISNULL(@bitMatrix,0) = 1 AND LEN(ISNULL(@vcItemAttributes,'')) > 0 AND ISNULL(@bitOppOrderExists,0) <> 1 AND (SELECT COUNT(*) FROM @TempTable) > 0
	BEGIN
		-- DELETE PREVIOUS ITEM ATTRIBUTES
		DELETE FROM ItemAttributes WHERE numDomainID=@numDomainID AND numItemCode=@numItemCode

		-- INSERT NEW ITEM ATTRIBUTES
		INSERT INTO ItemAttributes
		(
			numDomainID
			,numItemCode
			,FLD_ID
			,FLD_Value
		)
		SELECT
			@numDomainID
			,@numItemCode
			,Fld_ID
			,Fld_Value
		FROM
			@TempTable

		IF (SELECT COUNT(*) FROM WarehouseItems WHERE numDomainID=@numDomainID AND numItemID=@numItemCode) > 0
		BEGIN

			-- DELETE PREVIOUS WAREHOUSE ATTRIBUTES
			DELETE FROM CFW_Fld_Values_Serialized_Items WHERE RecId IN (SELECT ISNULL(numWarehouseItemID,0) FROM WarehouseItems WHERE numDomainID=@numDomainID AND numItemID=@numItemCode)

			-- INSERT NEW WAREHOUSE ATTRIBUTES
			INSERT INTO CFW_Fld_Values_Serialized_Items
			(
				Fld_ID,
				Fld_Value,
				RecId,
				bitSerialized
			)                                                  
			SELECT 
				Fld_ID,
				ISNULL(CAST(Fld_Value AS VARCHAR),'') AS Fld_Value,
				TEMPWarehouse.numWarehouseItemID,
				0 
			FROM 
				@TempTable  
			OUTER APPLY
			(
				SELECT
					numWarehouseItemID
				FROM 
					WarehouseItems 
				WHERE 
					numDomainID=@numDomainID 
					AND numItemID=@numItemCode
			) AS TEMPWarehouse
		END
	END	  
COMMIT
END TRY
BEGIN CATCH
	DECLARE @ErrorMessage NVARCHAR(4000)
	DECLARE @ErrorNumber INT
	DECLARE @ErrorSeverity INT
	DECLARE @ErrorState INT
	DECLARE @ErrorLine INT
	DECLARE @ErrorProcedure NVARCHAR(200)

	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;

	SELECT 
		@ErrorMessage = ERROR_MESSAGE(),
		@ErrorNumber = ERROR_NUMBER(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE(),
		@ErrorLine = ERROR_LINE(),
		@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

	RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);
END CATCH        
END
/****** Object:  StoredProcedure [dbo].[USP_SaveJournalDetails]    Script Date: 07/26/2008 16:21:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Create by Siva                                                                                        
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_SaveJournalDetails' ) 
    DROP PROCEDURE USP_SaveJournalDetails
GO
CREATE PROCEDURE [dbo].[USP_SaveJournalDetails]
    @numJournalId AS NUMERIC(9) = 0 ,
    @strRow AS TEXT = '' ,
    @Mode AS TINYINT = 0 ,
    @numDomainId AS NUMERIC(9) = 0                       
--@RecurringMode as tinyint=0,    
AS 
BEGIN           
                
BEGIN TRY 
		-- INCLUDE ENCODING OTHER WISE IT WILL THROW ERROR WHEN SPECIAL CHARACTERS ARE AVAILABLE IN DESCRIPT FIELDS
		IF CHARINDEX('<?xml',@strRow) = 0
		BEGIN
			SET @strRow = CONCAT('<?xml version="1.0" encoding="ISO-8859-1"?>',@strRow)
		END

        BEGIN TRAN  
        
        ---Always set default Currency ID as base currency ID of domain, and Rate =1
        DECLARE @numBaseCurrencyID NUMERIC
        SELECT @numBaseCurrencyID = ISNULL(numCurrencyID,0) FROM dbo.Domain WHERE numDomainId=@numDomainId
        
        IF CONVERT(VARCHAR(100), @strRow) <> '' 
            BEGIN                                                                                                            
                DECLARE @hDoc3 INT                                                                                                                                                                
                EXEC sp_xml_preparedocument @hDoc3 OUTPUT, @strRow                                                                                                             
--                PRINT '---Insert xml into temp table ---'
                    SELECT numTransactionId ,numDebitAmt ,numCreditAmt ,numChartAcntId ,varDescription ,numCustomerId ,/*numDomainId ,*/bitMainDeposit ,bitMainCheck ,bitMainCashCredit ,numoppitemtCode ,chBizDocItems ,vcReference ,numPaymentMethod ,bitReconcile , 
                    ISNULL(NULLIF(numCurrencyID,0),@numBaseCurrencyID) AS numCurrencyID  ,ISNULL(NULLIF(fltExchangeRate,0.0000),1) AS fltExchangeRate ,numTaxItemID ,numBizDocsPaymentDetailsId ,numcontactid ,numItemID ,numProjectID ,numClassID ,numCommissionID ,numReconcileID ,bitCleared ,tintReferenceType ,numReferenceID ,numCampaignID 
                     INTO #temp FROM      OPENXML (@hdoc3,'/ArrayOfJournalEntryNew/JournalEntryNew',2)
												WITH (
												numTransactionId numeric(10, 0),numDebitAmt FLOAT,numCreditAmt FLOAT,numChartAcntId numeric(18, 0),varDescription varchar(1000),numCustomerId numeric(18, 0),/*numDomainId numeric(18, 0),*/bitMainDeposit bit,bitMainCheck bit,bitMainCashCredit bit,numoppitemtCode numeric(18, 0),chBizDocItems nchar(10),vcReference varchar(500),numPaymentMethod numeric(18, 0),bitReconcile bit,numCurrencyID numeric(18, 0),fltExchangeRate float,numTaxItemID numeric(18, 0),numBizDocsPaymentDetailsId numeric(18, 0),numcontactid numeric(9, 0),numItemID numeric(18, 0),numProjectID numeric(18, 0),numClassID numeric(18, 0),numCommissionID numeric(9, 0),numReconcileID numeric(18, 0),bitCleared bit,tintReferenceType tinyint,numReferenceID numeric(18, 0),numCampaignID NUMERIC(18,0)
													)

                 EXEC sp_xml_removedocument @hDoc3 
                                    

/*-----------------------Validation of balancing entry*/

IF EXISTS(SELECT * FROM #temp)
BEGIN
	DECLARE @SumTotal MONEY 
	SELECT @SumTotal = CAST(SUM(numDebitAmt) AS MONEY)-CAST(SUM(numCreditAmt) AS MONEY) FROM #temp

--	PRINT cast( @SumTotal AS DECIMAL(10,4))
	IF cast( @SumTotal AS DECIMAL(10,4)) <> 0.0000
	BEGIN
		 INSERT INTO [dbo].[GenealEntryAudit]
           ([numDomainID]
           ,[numJournalID]
           ,[numTransactionID]
           ,[dtCreatedDate]
           ,[varDescription])
		 SELECT @numDomainID,
				@numJournalId,
				0,
				GETUTCDATE(),
				'IM_BALANCE'

		 RAISERROR ( 'IM_BALANCE', 16, 1 ) ;
		 RETURN;
	END

	
END



DECLARE @numEntryDateSortOrder AS NUMERIC
DECLARE @datEntry_Date AS DATETIME
--Combine Date from datentryDate with time part of current time
SELECT @datEntry_Date=( CAST( CONVERT(VARCHAR,DATEPART(year, datEntry_Date)) + '-' + RIGHT('00000'+ CONVERT(VARCHAR,DATEPART(month, datEntry_Date)),2)+ 
+ '-' + CONVERT(VARCHAR,DATEPART(day, datEntry_Date))
+ ' ' + CONVERT(VARCHAR,DATEPART(hour, GETDATE()))
+ ':' + CONVERT(VARCHAR,DATEPART(minute, GETDATE()))
+ ':' + CONVERT(VARCHAR,DATEPART(second, GETDATE())) AS DATETIME)  
) 
 from dbo.General_Journal_Header WHERE numJournal_Id=@numJournalId AND numDomainId=@numDomainId

SET @numEntryDateSortOrder = convert(numeric,
RIGHT('00000'+ CONVERT(VARCHAR,DATEPART(year, @datEntry_Date)),4)+
RIGHT('00000'+ CONVERT(VARCHAR,DATEPART(month, @datEntry_Date)),2)+
RIGHT('00000'+ CONVERT(VARCHAR,DATEPART(day, @datEntry_Date)),2)+
RIGHT('00000'+ CONVERT(VARCHAR,DATEPART(hour, @datEntry_Date)),2)+
RIGHT('00000'+ CONVERT(VARCHAR,DATEPART(minute, @datEntry_Date)),2)+
RIGHT('00000'+ CONVERT(VARCHAR,DATEPART(second, @datEntry_Date)),2)+
RIGHT('00000'+ CONVERT(VARCHAR,DATEPART(millisecond, @datEntry_Date)),3))

/*-----------------------Validation of balancing entry*/

			

                IF @Mode = 1  /*Edit Journal entries*/
                    BEGIN             
						PRINT '---delete removed transactions ---'
                        DELETE  FROM General_Journal_Details
                        WHERE   numJournalId = @numJournalId
                                AND numTransactionId NOT IN (SELECT numTransactionId FROM #temp as X )
			                                                           
						--Update transactions
						PRINT '---updated existing transactions ---'
                        UPDATE  dbo.General_Journal_Details 
                        SET    		[numDebitAmt] = X.numDebitAmt,
									[numCreditAmt] = X.numCreditAmt,
									[numChartAcntId] = X.numChartAcntId,
									[varDescription] = X.varDescription,
									[numCustomerId] = NULLIF(X.numCustomerId,0),
									[bitMainDeposit] = X.bitMainDeposit,
									[bitMainCheck] = X.bitMainCheck,
									[bitMainCashCredit] = X.bitMainCashCredit,
									[numoppitemtCode] = NULLIF(X.numoppitemtCode,0),
									[chBizDocItems] = X.chBizDocItems,
									[vcReference] = X.vcReference,
									[numPaymentMethod] = X.numPaymentMethod,
									[numCurrencyID] = NULLIF(X.numCurrencyID,0),
									[fltExchangeRate] = X.fltExchangeRate,
									[numTaxItemID] = NULLIF(X.numTaxItemID,0),
									[numBizDocsPaymentDetailsId] = NULLIF(X.numBizDocsPaymentDetailsId,0),
									[numcontactid] = NULLIF(X.numcontactid,0),
									[numItemID] = NULLIF(X.numItemID,0),
									[numProjectID] =NULLIF( X.numProjectID,0),
									[numClassID] = NULLIF(X.numClassID,0),
									[numCommissionID] = NULLIF(X.numCommissionID,0),
									numCampaignID=NULLIF(X.numCampaignID,0),
									numEntryDateSortOrder1=@numEntryDateSortOrder
--									[tintReferenceType] = X.tintReferenceType,
--									[numReferenceID] = X.numReferenceID
                          FROM    #temp as X 
						  WHERE   X.numTransactionId = General_Journal_Details.numTransactionId
			               
		               
					    INSERT INTO [dbo].[GenealEntryAudit]
									([numDomainID]
									,[numJournalID]
									,[numTransactionID]
									,[dtCreatedDate]
									,[varDescription])
						SELECT GJD.numDomainID, GJD.[numJournalId], GJD.[numTransactionId], GETUTCDATE(),[GJD].[varDescription]
						FROM General_Journal_Details AS GJD                                             
						JOIN #temp AS X ON  X.numTransactionId = GJD.numTransactionId
						
                    END 
                IF @Mode = 0 OR @Mode = 1 /*-- Insert journal only*/
                    BEGIN    
						PRINT '---insert existing transactions ---'                                                                                               
                        INSERT  INTO [dbo].[General_Journal_Details]
                                ( [numJournalId] ,
                                  [numDebitAmt] ,
                                  [numCreditAmt] ,
                                  [numChartAcntId] ,
                                  [varDescription] ,
                                  [numCustomerId] ,
                                  [numDomainId] ,
                                  [bitMainDeposit] ,
                                  [bitMainCheck] ,
                                  [bitMainCashCredit] ,
                                  [numoppitemtCode] ,
                                  [chBizDocItems] ,
                                  [vcReference] ,
                                  [numPaymentMethod] ,
                                  [bitReconcile] ,
                                  [numCurrencyID] ,
                                  [fltExchangeRate] ,
                                  [numTaxItemID] ,
                                  [numBizDocsPaymentDetailsId] ,
                                  [numcontactid] ,
                                  [numItemID] ,
                                  [numProjectID] ,
                                  [numClassID] ,
                                  [numCommissionID] ,
                                  [numReconcileID] ,
                                  [bitCleared],
                                  [tintReferenceType],
								  [numReferenceID],[numCampaignID],numEntryDateSortOrder1
	                                
                                )
                                SELECT  @numJournalId ,
                                        [numDebitAmt] ,
                                        [numCreditAmt] ,
                                        CASE WHEN [numChartAcntId]=0 THEN NULL ELSE numChartAcntId END  ,
                                        [varDescription] ,
                                        NULLIF(numCustomerId, 0) ,
                                        @numDomainId ,
                                        [bitMainDeposit] ,
                                        [bitMainCheck] ,
                                        [bitMainCashCredit] ,
                                        NULLIF([numoppitemtCode], 0) ,
                                        [chBizDocItems] ,
                                        [vcReference] ,
                                        [numPaymentMethod] ,
                                        [bitReconcile] ,
                                        NULLIF([numCurrencyID], 0) ,
                                        [fltExchangeRate] ,
                                        NULLIF([numTaxItemID], 0) ,
                                        NULLIF([numBizDocsPaymentDetailsId], 0) ,
                                        NULLIF([numcontactid], 0) ,
                                        NULLIF([numItemID], 0) ,
                                        NULLIF([numProjectID], 0) ,
                                        NULLIF([numClassID], 0) ,
                                        NULLIF([numCommissionID], 0) ,
                                        NULLIF([numReconcileID], 0) ,
                                        [bitCleared],
                                        tintReferenceType,
										NULLIF(numReferenceID,0),NULLIF(numCampaignID,0),@numEntryDateSortOrder
                                        
                                FROM #temp as X    
                                WHERE X.numTransactionId =0

						--Set Default Class If enable User Level Class Accountng 
						DECLARE @numAccountClass AS NUMERIC(18)=0                             
                        SELECT @numAccountClass=ISNULL(numAccountClass,0) FROM General_Journal_Header WHERE numDomainId=@numDomainId AND numJournal_Id=@numJournalId
                        IF @numAccountClass>0
                        BEGIN
							UPDATE General_Journal_Details SET numClassID=@numAccountClass WHERE numJournalId=@numJournalId AND numDomainId=@numDomainId AND ISNULL(numClassID,0) = 0
						END

						INSERT INTO [dbo].[GenealEntryAudit]
									([numDomainID]
									,[numJournalID]
									,[numTransactionID]
									,[dtCreatedDate]
									,[varDescription])
						SELECT @numDomainID, @numJournalId, X.numTransactionId, GETUTCDATE(),X.varDescription
						FROM #temp AS X WHERE X.numTransactionId = 0

                    END 
                    
                    
                    UPDATE dbo.General_Journal_Header 
						SET numAmount=(SELECT SUM(numDebitAmt) FROM dbo.General_Journal_Details WHERE numJournalID=@numJournalID)
						WHERE numJournal_Id=@numJournalId AND numDomainId=@numDomainId
						
					SELECT @SumTotal = CAST(SUM(numDebitAmt) AS MONEY)-CAST(SUM(numCreditAmt) AS MONEY) FROM General_Journal_Details WHERE numJournalId=@numJournalId AND numDomainId=@numDomainId
					IF cast( @SumTotal AS DECIMAL(10,4)) <> 0.0000
					BEGIN
						 INSERT INTO [dbo].[GenealEntryAudit]
						   ([numDomainID]
						   ,[numJournalID]
						   ,[numTransactionID]
						   ,[dtCreatedDate]
						   ,[varDescription])
						 SELECT @numDomainID,
								@numJournalId,
								0,
								GETUTCDATE(),
								'IM_BALANCE'

						 RAISERROR ( 'IM_BALANCE', 16, 1 ) ;
						 RETURN;
					END

                    drop table #temp
                    
                    -- Update leads,prospects to Accounts
					DECLARE @numDivisionID AS NUMERIC(18)
					DECLARE @numUserCntID AS NUMERIC(18)
					
					SELECT TOP 1 @numDivisionID = ISNULL(numCustomerId,0), @numUserCntID = ISNULL(numCreatedBy,0)
					FROM dbo.General_Journal_Details GJD
					JOIN dbo.General_Journal_Header GJH ON GJD.numJournalId = GJH.numJournal_Id AND GJD.numDomainId = GJH.numDomainId
					WHERE numJournalId = @numJournalId 
					AND GJH.numDomainId = @numDomainID
					
					IF @numDivisionID > 0 AND @numUserCntID > 0
					BEGIN
						EXEC dbo.USP_UpdateCRMTypeToAccounts @numDivisionID ,@numDomainID ,@numUserCntID
					END
            END
            
SELECT GJD.numTransactionId,GJD.numEntryDateSortOrder1,Row_number() OVER(ORDER BY GJD.numTransactionId) AS NewSortId  INTO #Temp1 
FROM General_Journal_Details GJD WHERE GJD.numDomainId = @numDomainID and  GJD.numEntryDateSortOrder1  LIKE SUBSTRING(CAST(@numEntryDateSortOrder AS VARCHAR(25)),1,12) + '%'

UPDATE  GJD SET GJD.numEntryDateSortOrder1 = (Temp1.numEntryDateSortOrder1 + Temp1.NewSortId) 
FROM General_Journal_Details GJD INNER JOIN #Temp1 AS Temp1 ON GJD.numTransactionId = Temp1.numTransactionId

IF object_id('TEMPDB.DBO.#Temp1') IS NOT NULL DROP TABLE #Temp1

        
        COMMIT TRAN 
    END TRY 
    BEGIN CATCH		
        DECLARE @strMsg VARCHAR(200)
        SET @strMsg = ERROR_MESSAGE()
        IF ( @@TRANCOUNT > 0 ) 
            BEGIN
				INSERT INTO [dbo].[GenealEntryAudit]
									([numDomainID]
									,[numJournalID]
									,[numTransactionID]
									,[dtCreatedDate]
									,[varDescription])
						SELECT @numDomainID, @numJournalId,0, GETUTCDATE(),@strMsg

                RAISERROR ( @strMsg, 16, 1 ) ;
                ROLLBACK TRAN
                RETURN 1
            END
    END CATCH	

            
--End                                              
---For Recurring Transaction                                              
                                              
--if @RecurringMode=1                                              
--Begin    
--  print 'SIVA--Recurring'    
--  print  '@numDomainId=============='+Convert(varchar(10),@numDomainId)                                     
--  Declare @numMaxJournalId as numeric(9)                                              
--  Select @numMaxJournalId=max(numJournal_Id) From General_Journal_Header Where numDomainId=@numDomainId                                              
--  insert into General_Journal_Details(numJournalId,numDebitAmt,numCreditAmt,numChartAcntId,varDescription,numCustomerId,numDomainId,bitMainCheck,bitMainCashCredit)                                              
--  Select @numMaxJournalId,numDebitAmt,numCreditAmt,numChartAcntId,varDescription,NULLIF(numCustomerId,0),numDomainId,bitMainCheck,bitMainCashCredit from General_Journal_Details Where numJournalId=@numJournalId  And numDomainId=@numDomainId                         
--   
--                   
--/*Commented by chintan Opening balance Will be updated by Trigger*/
----Exec USP_UpdateChartAcntOpnBalanceForJournalEntry @JournalId=@numMaxJournalId,@numDomainId=@numDomainId                                              
--End           
    END
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
                    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_SimpleSearch')
DROP PROCEDURE USP_SimpleSearch
GO
CREATE PROCEDURE [dbo].[USP_SimpleSearch]                             
	@numDomainID NUMERIC (18,0),
	@numUserCntID NUMERIC(18,0),
	@searchText VARCHAR(100),
	@searchType VARCHAR(1),
	@searchCriteria VARCHAR(1),
	@orderType VARCHAR(1),
	@bizDocType AS INT ,
	@numSkip AS INT,@isStartWithSearch as int                                                                  
AS               
	DECLARE @SalesOppRights AS TINYINT                    
	DECLARE @PurchaseOppRights AS TINYINT                   
	DECLARE @SalesOrderRights AS TINYINT
	DECLARE @PurchaseOrderRights AS TINYINT
	DECLARE @BizDocsRights TINYINT           
	DECLARE @strSQL AS VARCHAR(MAX) = ''
	
	IF @searchType = '1' -- Organization
	BEGIN
		IF @searchCriteria = '1' --Organizations
		BEGIN
			EXEC USP_CompanyInfo_Search @numDomainID=@numDomainID,@numUserCntID=@numUserCntID,@isStartWithSearch=@isStartWithSearch,@searchText=@searchText
		END
		ELSE
		BEGIN
			DECLARE @ProspectsRights AS TINYINT                    
			DECLARE @accountsRights AS TINYINT                   
			DECLARE @leadsRights AS TINYINT 
               
			SET @ProspectsRights = ( SELECT TOP 1
											MAX(GA.intViewAllowed) AS intViewAllowed
									 FROM   UserMaster UM INNER JOIN GroupAuthorization GA ON GA.numGroupID = UM.numGroupID INNER JOIN PageMaster PM ON PM.numPageID = GA.numPageID AND PM.numModuleID = GA.numModuleID
									 WHERE  PM.numpageID = 2
											AND PM.numModuleID = 3
											AND UM.numUserID = ( SELECT numUserID
																 FROM   userMaster
																 WHERE  numUserDetailId = @numUserCntID
															   )
									 GROUP BY UM.numUserId ,
											UM.vcUserName ,
											PM.vcFileName ,
											GA.numModuleID ,
											GA.numPageID
								   )                        
                    
			SET @accountsRights = ( SELECT TOP 1
											MAX(GA.intViewAllowed) AS intViewAllowed
									FROM    UserMaster UM INNER JOIN GroupAuthorization GA ON GA.numGroupID = UM.numGroupID INNER JOIN PageMaster PM ON PM.numPageID = GA.numPageID AND PM.numModuleID = GA.numModuleID
									WHERE PM.numpageID = 2
											AND PM.numModuleID = 4
											AND UM.numUserID = ( SELECT numUserID
																 FROM   userMaster
																 WHERE  numUserDetailId = @numUserCntID
															   )
									GROUP BY UM.numUserId ,
											UM.vcUserName ,
											PM.vcFileName ,
											GA.numModuleID ,
											GA.numPageID
								  )                
              
			SET @leadsRights = ( SELECT TOP 1
										MAX(GA.intViewAllowed) AS intViewAllowed
								 FROM   UserMaster UM INNER JOIN GroupAuthorization GA ON GA.numGroupID = UM.numGroupID INNER JOIN PageMaster PM ON PM.numPageID = GA.numPageID AND PM.numModuleID = GA.numModuleID
								 WHERE  PM.numpageID = 2
										AND PM.numModuleID = 2
										AND UM.numUserID = ( SELECT numUserID
															 FROM   userMaster
															 WHERE  numUserDetailId = @numUserCntID
														   )
								 GROUP BY UM.numUserId ,
										UM.vcUserName ,
										PM.vcFileName ,
										GA.numModuleID ,
										GA.numPageID
							   )  

			--GET FIELDS CONFIGURED FOR ORGANIZATION SEARCH
			SELECT * INTO #tempOrgSearch FROM
			(
				SELECT 
					numFieldId,
					vcDbColumnName,
					ISNULL(vcCultureFieldName,vcFieldName) AS vcFieldName,
					vcLookBackTableName,
					tintRow AS tintOrder,
					0 as Custom
				FROM 
					View_DynamicColumns
				WHERE 
					numFormId=97 AND numUserCntID=@numUserCntID AND numDomainID=@numDomainID AND 
					tintPageType=1 AND bitCustom=0  AND ISNULL(bitSettingField,0)=1 AND numRelCntType=0
				UNION   
				SELECT 
					numFieldId,
					vcFieldName,
					vcFieldName,
					'CFW_FLD_Values',
					tintRow AS tintOrder,
					1 as Custom
				FROM 
					View_DynamicCustomColumns   
				WHERE 
					Grp_id=1 AND numFormId=97 AND numUserCntID=@numUserCntID AND numDomainID=@numDomainID AND 
					tintPageType=1 AND bitCustom=1 AND numRelCntType=0
			)Y
			
			DECLARE @searchSQL AS VARCHAR(MAX) = ''

			IF (SELECT COUNT(*) FROM #tempOrgSearch) > 0
			BEGIN
				if(@isStartWithSearch=0)
				begin
				SELECT  
					@searchSQL = STUFF(
										(SELECT ' OR ' + + CONCAT((CASE WHEN vcLookBackTableName = 'AddressDetails' THEN '' ELSE CONCAT(vcLookBackTableName,'.') END),IIF(Custom = 1,'Fld_Value',vcDbColumnName)) + ' LIKE ''%' + @searchText + '%''' FROM #tempOrgSearch FOR XML PATH(''))
										, 1
										, 4
										, ''
										)
				end
				else 
				begin
					SELECT  
					@searchSQL = STUFF(
										(SELECT ' OR ' + + CONCAT((CASE WHEN vcLookBackTableName = 'AddressDetails' THEN '' ELSE CONCAT(vcLookBackTableName,'.') END),IIF(Custom = 1,'Fld_Value',vcDbColumnName)) + ' LIKE '+ @searchText + '%''' FROM #tempOrgSearch FOR XML PATH(''))
										, 1
										, 4
										, ''
										)
				end
			END
			ELSE
			BEGIN
				if(@isStartWithSearch=0)
				begin
					SET @searchSQL = 'CompanyInfo.vcCompanyName LIKE ''%' + @searchText + '%'''
				end
				else
				begin
					SET @searchSQL = 'CompanyInfo.vcCompanyName LIKE '+ @searchText + '%'''
				end
			END

			CREATE TABLE #TEMPOrganization
			(
				numDivisionID NUMERIC(18,0),
				vcCompanyName VARCHAR(500)
			)


			SET @strSQL = 'INSERT INTO 
								#TEMPOrganization
							SELECT
								DivisionMaster.numDivisionID
								,CompanyInfo.vcCompanyName
							FROM
								DivisionMaster 
							INNER JOIN
								CompanyInfo 
							ON
								CompanyInfo.numCompanyId = DivisionMaster.numCompanyID
							LEFT JOIN
								AdditionalContactsInformation
							ON
								AdditionalContactsInformation.numDivisionId = DivisionMaster.numDivisionID
							LEFT JOIN
								OpportunityMaster
							ON
								OpportunityMaster.numDivisionId = DivisionMaster.numDivisionID
							LEFT JOIN
								Cases
							ON
								Cases.numDivisionID = DivisionMaster.numDivisionID
							LEFT JOIN
								ProjectsMaster
							ON
								ProjectsMaster.numDivisionId = DivisionMaster.numDivisionID
							OUTER APPLY 
								(
									SELECT
										isnull(vcStreet,'''') as vcBillStreet, 
										isnull(vcCity,'''') as vcBillCity, 
										isnull(dbo.fn_GetState(numState),'''') as numBillState,
										isnull(vcPostalCode,'''') as vcBillPostCode,
										isnull(dbo.fn_GetListName(numCountry,0),'''')  as numBillCountry
									FROM
										dbo.AddressDetails 
									WHERE 
										numDomainID = ' + CAST(@numDomainID AS VARCHAR) + '
										AND numRecordID=DivisionMaster.numDivisionID 
										AND tintAddressOf=2 
										AND tintAddressType=1 
								)  AD1
							OUTER APPLY
								(
									SELECT
										isnull(vcStreet,'''') as vcShipStreet,
										isnull(vcCity,'''') as vcShipCity,
										isnull(dbo.fn_GetState(numState),'''')  as numShipState,
										isnull(vcPostalCode,'''') as vcShipPostCode, 
										isnull(dbo.fn_GetListName(numCountry,0),'''') numShipCountry
									FROM
										dbo.AddressDetails 
									WHERE 
										numDomainID = ' + CAST(@numDomainID AS VARCHAR) + '
										AND numRecordID= DivisionMaster.numDivisionID 
										AND tintAddressOf=2 
										AND tintAddressType=2	
								) AD2	
							LEFT JOIN
								CFW_FLD_Values
							ON
								CFW_FLD_Values.RecId = DivisionMaster.numDivisionID
								AND CFW_FLD_Values.Fld_ID IN (SELECT numFieldID FROM #tempOrgSearch)
							WHERE
								DivisionMaster.numDomainID = ' + CAST(@numDomainID AS VARCHAR) + ' AND (DivisionMaster.numAssignedTo = ' + CAST(@numUserCntID AS VARCHAR) + ' OR 
								1 = CASE 
									WHEN DivisionMaster.tintCRMType <> 0 and DivisionMaster.tintCRMType=1 THEN ' +
									CASE @ProspectsRights 
										WHEN 0 THEN ' CASE WHEN DivisionMaster.numRecOwner=0 THEN 1 ELSE 0 END'
										WHEN 1 THEN ' CASE WHEN DivisionMaster.numRecOwner= '+convert(varchar(15),@numUserCntID) + ' THEN 1 ELSE 0 END'                        
										WHEN 2 THEN ' CASE WHEN (DivisionMaster.numTerID IN (SELECT numTerritoryID FROM UserTerritory WHERE numUserCntID='+convert(varchar(15),@numUserCntID)+') or d.numTerID=0) THEN 1 ELSE 0 END'
										ELSE '1'
									END + ' WHEN DivisionMaster.tintCRMType<>0 and DivisionMaster.tintCRMType=2 THEN' + 
									CASE @accountsRights 
										WHEN 0 THEN ' CASE WHEN DivisionMaster.numRecOwner=0 THEN 1 ELSE 0 END'
										WHEN 1 THEN ' CASE WHEN DivisionMaster.numRecOwner= '+convert(varchar(15),@numUserCntID) + ' THEN 1 ELSE 0 END'
										WHEN 2 THEN ' CASE WHEN (DivisionMaster.numTerID IN (SELECT numTerritoryID FROM UserTerritory WHERE numUserCntID='+convert(varchar(15),@numUserCntID)+') or d.numTerID=0) THEN 1 ELSE 0 END'
										ELSE '1'
									END + ' WHEN DivisionMaster.tintCRMType=0 THEN' + 
									CASE @leadsRights 
										WHEN 0 THEN ' CASE WHEN DivisionMaster.numRecOwner=0 THEN 1 ELSE 0 END'
										WHEN 1 THEN ' CASE WHEN DivisionMaster.numRecOwner= '+convert(varchar(15),@numUserCntID) + ' THEN 1 ELSE 0 END'
										WHEN 2 THEN ' CASE WHEN (DivisionMaster.numTerID IN (SELECT numTerritoryID FROM UserTerritory WHERE numUserCntID='+convert(varchar(15),@numUserCntID)+') or d.numTerID=0) THEN 1 ELSE 0 END'
										ELSE '1'
									END + ') AND (' + @searchSQL + ')
							GROUP BY
								DivisionMaster.numDivisionID
								,CompanyInfo.vcCompanyName'

			PRINT @strSQL
			EXEC (@strSQL)
			IF @searchCriteria = '2' --Items
			BEGIN
				DECLARE @TEMPORGITEM TABLE
				(
					numOppId NUMERIC(18,0),
					CreatedDate DATETIME,
					vcItemName VARCHAR(500),
					vcPOppName VARCHAR(200),
					vcOppType VARCHAR(50),
					numUnitHour FLOAT,
					monPrice NUMERIC(18,2),
					vcCompanyName  VARCHAR(200),
					tintType TINYINT
				)

				INSERT INTO @TEMPORGITEM
				(
					numOppId,
					CreatedDate,
					vcItemName,
					vcPOppName,
					vcOppType,
					numUnitHour,
					monPrice,
					vcCompanyName,
					tintType
				)
				SELECT
					numOppId,
					bintCreatedDate,
					vcItemName,
					vcPOppName,
					vcOppType,
					CAST(numUnitHour AS FLOAT) numUnitHour,
					CAST(monPrice AS decimal(18,2)) monPrice,
					vcCompanyName,
					tintType
				FROM
				(
					SELECT
						OpportunityMaster.numOppId,
						OpportunityMaster.bintCreatedDate,
						CONCAT(Item.vcItemName,',',Item.vcModelID) AS vcItemName,
						OpportunityMaster.vcPOppName,
						CASE 
							WHEN tintOppType = 1 THEN 'Sales ' + (CASE WHEN tintOppStatus = 1 THEN 'Order' ELSE 'Opportunity' END)
							WHEN tintOppType = 2 THEN 'Purchase ' + (CASE WHEN tintOppStatus = 1 THEN 'Order' ELSE 'Opportunity' END)
						END vcOppType,
						OpportunityItems.numUnitHour,
						OpportunityItems.monPrice,
						TEMP.vcCompanyName,
						1 AS tintType
					FROM
						OpportunityMaster
					INNER JOIN
						#TEMPOrganization TEMP
					ON
						OpportunityMaster.numDivisionId = TEMP.numDivisionID
					INNER JOIN
						OpportunityItems
					ON
						OpportunityMaster.numOppId = OpportunityItems.numOppId
					INNER JOIN
						Item
					ON
						OpportunityItems.numItemCode = Item.numItemCode
					WHERE
						OpportunityMaster.numDomainId = @numDomainID
					UNION
					SELECT
						ReturnHeader.numReturnHeaderID AS numOppID,
						ReturnHeader.dtCreatedDate AS bintCreatedDate,
						CONCAT(Item.vcItemName,',',Item.vcModelID) AS vcItemName,
						ReturnHeader.vcRMA AS vcPOppName,
						CASE 
							WHEN tintReturnType = 1 THEN 'Sales Return'
							WHEN tintReturnType = 2 THEN 'Purchase Return'
						END vcOppType,
						ReturnItems.numUnitHour,
						ReturnItems.monPrice,
						TEMP.vcCompanyName,
						2 AS tintType
					FROM
						ReturnHeader
					INNER JOIN
						#TEMPOrganization TEMP
					ON
						ReturnHeader.numDivisionId = TEMP.numDivisionID
					INNER JOIN	
						ReturnItems
					ON
						ReturnHeader.numReturnHeaderID = ReturnItems.numReturnHeaderID
					INNER JOIN
						Item
					ON
						Item.numItemCode = ReturnItems.numItemCode
					WHERE
						ReturnHeader.numDomainId = @numDomainID AND tintReturnType IN (1,2)
				) TEMPFinal

				SELECT  
					ISNULL(numOppId,0) AS numOppId,
					ISNULL(dbo.FormatedDateFromDate(CreatedDate,@numDomainID),'') bintCreatedDate,
					ISNULL(vcItemName,'') vcItemName,
					ISNULL(vcPOppName,'') vcPOppName,
					ISNULL(vcOppType,'') vcOppType,
					ISNULL(numUnitHour,0)numUnitHour,
					ISNULL(monPrice,0.00) monPrice,
					ISNULL(vcCompanyName,'') vcCompanyName,
					ISNULL(tintType,'') tintType
				FROM 
					@TEMPORGITEM 
				ORDER BY 
					CreatedDate DESC 
				OFFSET @numSkip ROWS FETCH NEXT 10 ROWS ONLY

				SELECT COUNT(numOppId) AS TotalItems FROM @TEMPORGITEM 
			END
			ELSE IF @searchCriteria = '3' -- Opp/Orders
			BEGIN
				SET @SalesOppRights = ( SELECT TOP 1
										MAX(GA.intViewAllowed) AS intViewAllowed
									FROM   UserMaster UM INNER JOIN GroupAuthorization GA ON GA.numGroupID = UM.numGroupID INNER JOIN PageMaster PM ON PM.numPageID = GA.numPageID AND PM.numModuleID = GA.numModuleID
									WHERE  PM.numpageID = 2
										AND PM.numModuleID = 10
										AND UM.numUserID = ( SELECT numUserID
																FROM   userMaster
																WHERE  numUserDetailId = @numUserCntID
															)
									GROUP BY UM.numUserId ,
										UM.vcUserName ,
										PM.vcFileName ,
										GA.numModuleID ,
										GA.numPageID
								)                        
                    
				SET @PurchaseOppRights = ( SELECT TOP 1
												MAX(GA.intViewAllowed) AS intViewAllowed
										FROM    UserMaster UM INNER JOIN GroupAuthorization GA ON GA.numGroupID = UM.numGroupID INNER JOIN PageMaster PM ON PM.numPageID = GA.numPageID AND PM.numModuleID = GA.numModuleID
										WHERE PM.numpageID = 8
												AND PM.numModuleID = 10
												AND UM.numUserID = ( SELECT numUserID
																		FROM   userMaster
																		WHERE  numUserDetailId = @numUserCntID
																	)
										GROUP BY UM.numUserId ,
												UM.vcUserName ,
												PM.vcFileName ,
												GA.numModuleID ,
												GA.numPageID
										)                
              
				SET @SalesOrderRights = ( SELECT TOP 1
											MAX(GA.intViewAllowed) AS intViewAllowed
										FROM   UserMaster UM INNER JOIN GroupAuthorization GA ON GA.numGroupID = UM.numGroupID INNER JOIN PageMaster PM ON PM.numPageID = GA.numPageID AND PM.numModuleID = GA.numModuleID
										WHERE  PM.numpageID = 10
											AND PM.numModuleID = 10
											AND UM.numUserID = ( SELECT numUserID
																	FROM   userMaster
																	WHERE  numUserDetailId = @numUserCntID
																)
										GROUP BY UM.numUserId ,
											UM.vcUserName ,
											PM.vcFileName ,
											GA.numModuleID ,
											GA.numPageID
									)  
		
				SET @PurchaseOrderRights = ( SELECT TOP 1
											MAX(GA.intViewAllowed) AS intViewAllowed
										FROM   UserMaster UM INNER JOIN GroupAuthorization GA ON GA.numGroupID = UM.numGroupID INNER JOIN PageMaster PM ON PM.numPageID = GA.numPageID AND PM.numModuleID = GA.numModuleID
										WHERE  PM.numpageID = 11
											AND PM.numModuleID = 10
											AND UM.numUserID = ( SELECT numUserID
																	FROM   userMaster
																	WHERE  numUserDetailId = @numUserCntID
																)
										GROUP BY UM.numUserId ,
											UM.vcUserName ,
											PM.vcFileName ,
											GA.numModuleID ,
											GA.numPageID
									)
					
				DECLARE @TEMPORGORDER TABLE
				(
					numOppId NUMERIC(18,0),
					CreatedDate DATETIME,
					vcPOppName VARCHAR(200),
					vcOppType VARCHAR(50),
					vcCompanyName  VARCHAR(200),
					tintType TINYINT
				)

				INSERT INTO 
					@TEMPORGORDER
				SELECT
					numOppId,
					bintCreatedDate,
					vcPOppName,
					vcOppType,
					vcCompanyName,
					tintType
				FROM
				(
					SELECT
						OpportunityMaster.numOppId,
						OpportunityMaster.vcPOppName,
						CASE 
							WHEN tintOppType = 1 THEN 'Sales ' + (CASE WHEN tintOppStatus = 1 THEN 'Order' ELSE 'Opportunity' END)
							WHEN tintOppType = 2 THEN 'Purchase ' + (CASE WHEN tintOppStatus = 1 THEN 'Order' ELSE 'Opportunity' END)
						END vcOppType,
						TEMP.vcCompanyName,
						bintCreatedDate,
						1 AS tintType
					FROM
						OpportunityMaster
					INNER JOIN
						#TEMPOrganization TEMP
					ON
						OpportunityMaster.numDivisionId = TEMP.numDivisionID
					WHERE
						numDomainId = @numDomainID
						AND tintOppType <> 0
						AND (OpportunityMaster.numAssignedTo=@numUserCntID OR 1 = (CASE 
								WHEN tintOppType=1 AND ISNULL(tintOppStatus,0)=0
								THEN 
									CASE @SalesOppRights
										WHEN 0 THEN (CASE WHEN ISNULL(OpportunityMaster.numRecOwner,0) = 0 THEN 1 ELSE 0 END)
										WHEN 1 THEN (CASE WHEN ISNULL(OpportunityMaster.numRecOwner,0) = @numUserCntID THEN 1 ELSE 0 END)
										ELSE 1
									END
								WHEN tintOppType=2 AND ISNULL(tintOppStatus,0)=0
								THEN 
									CASE @PurchaseOrderRights
										WHEN 0 THEN (CASE WHEN ISNULL(OpportunityMaster.numRecOwner,0) = 0 THEN 1 ELSE 0 END)
										WHEN 1 THEN (CASE WHEN ISNULL(OpportunityMaster.numRecOwner,0) = @numUserCntID THEN 1 ELSE 0 END)
										ELSE 1
									END
								WHEN tintOppType=1 AND ISNULL(tintOppStatus,0)=1
								THEN 
									CASE @SalesOrderRights
										WHEN 0 THEN (CASE WHEN ISNULL(OpportunityMaster.numRecOwner,0) = 0 THEN 1 ELSE 0 END)
										WHEN 1 THEN (CASE WHEN ISNULL(OpportunityMaster.numRecOwner,0) = @numUserCntID THEN 1 ELSE 0 END)
										ELSE 1
									END
								WHEN tintOppType=2 AND ISNULL(tintOppStatus,0)=1
								THEN 
									CASE @PurchaseOrderRights
										WHEN 0 THEN (CASE WHEN ISNULL(OpportunityMaster.numRecOwner,0) = 0 THEN 1 ELSE 0 END)
										WHEN 1 THEN (CASE WHEN ISNULL(OpportunityMaster.numRecOwner,0) = @numUserCntID THEN 1 ELSE 0 END)
										ELSE 1
									END
								ELSE 1
							END))
					UNION 
					SELECT
						ReturnHeader.numReturnHeaderID AS numOppID,
						ReturnHeader.vcRMA AS vcPOppName,
						CASE 
							WHEN tintReturnType = 1 THEN 'Sales Return'
							WHEN tintReturnType = 2 THEN 'Purchase Return'
						END vcOppType,
						TEMP.vcCompanyName,
						dtCreatedDate AS bintCreatedDate,
						2 AS tintType
					FROM
						ReturnHeader
					INNER JOIN
						#TEMPOrganization TEMP
					ON
						ReturnHeader.numDivisionId = TEMP.numDivisionID
					WHERE
						numDomainId = @numDomainID AND tintReturnType IN (1,2)
				) TEMPFinal

				SELECT
					ISNULL(numOppId,0) numOppId,
					ISNULL(dbo.FormatedDateFromDate(CreatedDate,@numDomainID),'') bintCreatedDate,
					ISNULL(vcPOppName,'') vcPOppName,
					ISNULL(vcOppType,'') vcOppType,
					ISNULL(vcCompanyName,'') vcCompanyName,
					ISNULL(tintType,0) tintType
				FROM
					@TEMPORGORDER
				ORDER BY
					CreatedDate DESC
				OFFSET @numSkip ROWS FETCH NEXT 10 ROWS ONLY	
				
				SELECT COUNT(*) AS TotalRows FROM @TEMPORGORDER			
			END
			ELSE IF @searchCriteria = '4' -- BizDocs
			BEGIN
				SET @SalesOppRights = ( SELECT TOP 1
											MAX(GA.intViewAllowed) AS intViewAllowed
										FROM   UserMaster UM INNER JOIN GroupAuthorization GA ON GA.numGroupID = UM.numGroupID INNER JOIN PageMaster PM ON PM.numPageID = GA.numPageID AND PM.numModuleID = GA.numModuleID
										WHERE  PM.numpageID = 2
											AND PM.numModuleID = 10
											AND UM.numUserID = ( SELECT numUserID
																	FROM   userMaster
																	WHERE  numUserDetailId = @numUserCntID
																)
										GROUP BY UM.numUserId ,
											UM.vcUserName ,
											PM.vcFileName ,
											GA.numModuleID ,
											GA.numPageID
									)                        
                    
				SET @PurchaseOppRights = ( SELECT TOP 1
												MAX(GA.intViewAllowed) AS intViewAllowed
										FROM    UserMaster UM INNER JOIN GroupAuthorization GA ON GA.numGroupID = UM.numGroupID INNER JOIN PageMaster PM ON PM.numPageID = GA.numPageID AND PM.numModuleID = GA.numModuleID
										WHERE PM.numpageID = 8
												AND PM.numModuleID = 10
												AND UM.numUserID = ( SELECT numUserID
																		FROM   userMaster
																		WHERE  numUserDetailId = @numUserCntID
																	)
										GROUP BY UM.numUserId ,
												UM.vcUserName ,
												PM.vcFileName ,
												GA.numModuleID ,
												GA.numPageID
										)                
              
				SET @SalesOrderRights = ( SELECT TOP 1
											MAX(GA.intViewAllowed) AS intViewAllowed
										FROM   UserMaster UM INNER JOIN GroupAuthorization GA ON GA.numGroupID = UM.numGroupID INNER JOIN PageMaster PM ON PM.numPageID = GA.numPageID AND PM.numModuleID = GA.numModuleID
										WHERE  PM.numpageID = 10
											AND PM.numModuleID = 10
											AND UM.numUserID = ( SELECT numUserID
																	FROM   userMaster
																	WHERE  numUserDetailId = @numUserCntID
																)
										GROUP BY UM.numUserId ,
											UM.vcUserName ,
											PM.vcFileName ,
											GA.numModuleID ,
											GA.numPageID
									)  
		
				SET @PurchaseOrderRights = ( SELECT TOP 1
											MAX(GA.intViewAllowed) AS intViewAllowed
										FROM   UserMaster UM INNER JOIN GroupAuthorization GA ON GA.numGroupID = UM.numGroupID INNER JOIN PageMaster PM ON PM.numPageID = GA.numPageID AND PM.numModuleID = GA.numModuleID
										WHERE  PM.numpageID = 11
											AND PM.numModuleID = 10
											AND UM.numUserID = ( SELECT numUserID
																	FROM   userMaster
																	WHERE  numUserDetailId = @numUserCntID
																)
										GROUP BY UM.numUserId ,
											UM.vcUserName ,
											PM.vcFileName ,
											GA.numModuleID ,
											GA.numPageID
									) 

				SET @BizDocsRights = ( SELECT TOP 1
												MAX(GA.intViewAllowed) AS intViewAllowed
											FROM   UserMaster UM INNER JOIN GroupAuthorization GA ON GA.numGroupID = UM.numGroupID INNER JOIN PageMaster PM ON PM.numPageID = GA.numPageID AND PM.numModuleID = GA.numModuleID
											WHERE  PM.numpageID = 6
												AND PM.numModuleID = 10
												AND UM.numUserID = ( SELECT numUserID
																		FROM   userMaster
																		WHERE  numUserDetailId = @numUserCntID
																	)
											GROUP BY UM.numUserId ,
												UM.vcUserName ,
												PM.vcFileName ,
												GA.numModuleID ,
												GA.numPageID
										) 
				DECLARE @TEMPORGBizDoc TABLE
				(
					numOppBizDocsId NUMERIC(18,0),
					CreatedDate DATETIME,
					vcBizDocID VARCHAR(200),
					vcBizDocType VARCHAR(100),
					vcCompanyName  VARCHAR(200)
				)

				INSERT INTO
					@TEMPORGBizDoc
				SELECT 
					numOppBizDocsId,
					OpportunityBizDocs.dtCreatedDate,
					vcBizDocID,
					ListDetails.vcData AS vcBizDocType,
					TEMP.vcCompanyName
				FROM 
					OpportunityBizDocs
				INNER JOIN
					OpportunityMaster
				ON
					OpportunityBizDocs.numOppId = OpportunityMaster.numOppId
				INNER JOIN
					#TEMPOrganization TEMP
				ON
					OpportunityMaster.numDivisionId = TEMP.numDivisionID
				INNER JOIN
					ListDetails
				ON
					ListDetails.numListID = 27
					AND OpportunityBizDocs.numBizDocId = ListDetails.numListItemID
				WHERE
					OpportunityMaster.numDomainId = @numDomainID
					AND (OpportunityMaster.numAssignedTo = @numUserCntID OR 1 = (CASE 
								WHEN tintOppType=1 AND ISNULL(tintOppStatus,0)=0
								THEN 
									CASE @SalesOppRights
										WHEN 0 THEN (CASE WHEN ISNULL(OpportunityMaster.numRecOwner,0) = 0 THEN 1 ELSE 0 END)
										WHEN 1 THEN (CASE WHEN ISNULL(OpportunityMaster.numRecOwner,0) = @numUserCntID THEN 1 ELSE 0 END)
										ELSE 1
									END
								WHEN tintOppType=2 AND ISNULL(tintOppStatus,0)=0
								THEN 
									CASE @PurchaseOrderRights
										WHEN 0 THEN (CASE WHEN ISNULL(OpportunityMaster.numRecOwner,0) = 0 THEN 1 ELSE 0 END)
										WHEN 1 THEN (CASE WHEN ISNULL(OpportunityMaster.numRecOwner,0) = @numUserCntID THEN 1 ELSE 0 END)
										ELSE 1
									END
								WHEN tintOppType=1 AND ISNULL(tintOppStatus,0)=1
								THEN 
									CASE @SalesOrderRights
										WHEN 0 THEN (CASE WHEN ISNULL(OpportunityMaster.numRecOwner,0) = 0 THEN 1 ELSE 0 END)
										WHEN 1 THEN (CASE WHEN ISNULL(OpportunityMaster.numRecOwner,0) = @numUserCntID THEN 1 ELSE 0 END)
										ELSE 1
									END
								WHEN tintOppType=2 AND ISNULL(tintOppStatus,0)=1
								THEN 
									CASE @PurchaseOrderRights
										WHEN 0 THEN (CASE WHEN ISNULL(OpportunityMaster.numRecOwner,0) = 0 THEN 1 ELSE 0 END)
										WHEN 1 THEN (CASE WHEN ISNULL(OpportunityMaster.numRecOwner,0) = @numUserCntID THEN 1 ELSE 0 END)
										ELSE 1
									END
								ELSE 1
							END))
					AND 1 = CASE @BizDocsRights
								WHEN 0 THEN (CASE WHEN ISNULL(OpportunityBizDocs.numCreatedBy,0) = 0 THEN 1 ELSE 0 END)
								WHEN 1 THEN (CASE WHEN ISNULL(OpportunityBizDocs.numCreatedBy,0) = @numUserCntID THEN 1 ELSE 0 END)
								ELSE 1
							END

				SELECT
					ISNULL(numOppBizDocsId,0) numOppBizDocsId,
					ISNULL(dbo.FormatedDateFromDate(CreatedDate,@numDomainID),'') AS dtCreatedDate,
					ISNULL(vcBizDocID,'') vcBizDocID,
					ISNULL(vcBizDocType,'') vcBizDocType,
					ISNULL(vcCompanyName,'') vcCompanyName
				FROM
					@TEMPORGBizDoc
				ORDER BY
					CreatedDate DESC
				OFFSET @numSkip ROWS FETCH NEXT 10 ROWS ONLY
				
				SELECT COUNT(*) AS TotalRows FROM @TEMPORGBizDoc					
			END

			DROP TABLE #tempOrgSearch
			DROP TABLE #TEMPOrganization
		END
	END
	ELSE IF @searchType = '2' -- Item
	BEGIN
		DECLARE @ItemListRights AS TINYINT                    
               
		SET @ItemListRights = ( SELECT TOP 1
										MAX(GA.intViewAllowed) AS intViewAllowed
								FROM  
									UserMaster UM 
								INNER JOIN 
									GroupAuthorization GA 
								ON 
									GA.numGroupID = UM.numGroupID 
								INNER JOIN 
									PageMaster PM 
								ON 
									PM.numPageID = GA.numPageID 
									AND PM.numModuleID = GA.numModuleID
								WHERE 
									PM.numpageID = 9
									AND PM.numModuleID = 37
									AND UM.numUserID = (SELECT numUserID FROM userMaster WHERE numUserDetailId = @numUserCntID)
								GROUP BY 
									UM.numUserId
									,UM.vcUserName
									,PM.vcFileName
									,GA.numModuleID
									,GA.numPageID
							)    

		CREATE TABLE #TempSearchedItem
		(
			numItemCode NUMERIC(18,0), 
			vcItemName VARCHAR(200), 
			vcPathForTImage VARCHAR(200), 
			vcCompanyName VARCHAR(200), 
			monListPrice NUMERIC(18,0), 
			txtItemDesc TEXT, 
			vcModelID VARCHAR(100), 
			numBarCodeId VARCHAR(100), 
			vcBarCode VARCHAR(100), 
			vcSKU VARCHAR(100),
			vcWHSKU VARCHAR(100), 
			vcPartNo VARCHAR(100),
			numWarehouseItemID NUMERIC(18,0), 
			vcAttributes VARCHAR(500)
		)

		DECLARE @strItemSearch AS VARCHAR(1000) = ''
		DECLARE @strAttributeSearch AS VARCHAR(1000) = ''
		DECLARE @strSearch AS VARCHAR(1000)

		IF CHARINDEX(',',@searchText) > 0
		BEGIN
			SET @strItemSearch = LTRIM(RTRIM(SUBSTRING(@searchText,0,CHARINDEX(',',@searchText))))
			SET @strAttributeSearch = SUBSTRING(@searchText,CHARINDEX(',',@searchText) + 1,LEN(@searchText)) 

			IF LEN(@strAttributeSearch) > 0
			BEGIN
				if(@isStartWithSearch=0)
				begin
					SET @strAttributeSearch = 'TEMPAttributes.vcAttributes LIKE ''%' + REPLACE(@strAttributeSearch,',','%'' AND TEMPAttributes.vcAttributes LIKE ''%') + '%'''
				end
				else
				begin
					SET @strAttributeSearch = 'TEMPAttributes.vcAttributes LIKE ''%' + REPLACE(@strAttributeSearch,',','%'' AND TEMPAttributes.vcAttributes LIKE ''%') + '%'''
				end
			END
			ELSE
			BEGIN
				SET @strAttributeSearch = ''
			END
		END
		ELSE
		BEGIN
			SET @strItemSearch = @searchText
		END

		DECLARE @vcCustomDisplayField AS VARCHAR(4000) = ''
		SELECT * INTO #TempItemCustomDisplayFields FROM
		(
			SELECT 
				numFieldId,
				vcFieldName
			FROM 
				View_DynamicCustomColumns   
			WHERE 
				Grp_id=5 AND numFormId=22 AND numUserCntID=@numUserCntID AND numDomainID=@numDomainID AND 
				tintPageType=1 AND bitCustom=1 AND numRelCntType=0
		)TD

		IF (SELECT COUNT(*) FROM #TempItemCustomDisplayFields) > 0
		BEGIN
			SELECT @vcCustomDisplayField = STUFF(
									(SELECT ', ' +  'ISNULL(dbo.GetCustFldValueItem(' + CAST(numFieldId AS VARCHAR) + ',numItemCode),'''') AS [' + vcFieldName + ']' FROM #TempItemCustomDisplayFields FOR XML PATH(''))
									, 1
									, 0
									, ''
								)
		END

		SELECT * INTO #TempItemSearchFields FROM
		(
			SELECT 
				numFieldId,
				vcDbColumnName,
				ISNULL(vcCultureFieldName,vcFieldName) AS vcFieldName,
				vcLookBackTableName,
				tintRow AS tintOrder,
				0 as Custom
			FROM 
				View_DynamicColumns
			WHERE 
				numFormId=22 AND numUserCntID=@numUserCntID AND numDomainID=@numDomainID AND 
				tintPageType=1 AND bitCustom=0  AND ISNULL(bitSettingField,0)=1 AND numRelCntType=1
				AND vcDbColumnName <> 'vcAttributes'
			UNION   
			SELECT 
				numFieldId,
				vcFieldName,
				vcFieldName,
				'CFW_FLD_Values',
				tintRow AS tintOrder,
				1 as Custom
			FROM 
				View_DynamicCustomColumns   
			WHERE 
				Grp_id=5 AND numFormId=22 AND numUserCntID=@numUserCntID AND numDomainID=@numDomainID AND 
				tintPageType=1 AND bitCustom=1 AND numRelCntType=1
		)Y

		IF (SELECT COUNT(*) FROM #TempItemSearchFields) > 0
		BEGIN
		if(@isStartWithSearch=0)
				begin
			SELECT  @strSearch = STUFF(
									(SELECT ' OR ' +  IIF(Custom = 1,'dbo.GetCustFldValueItem('+ CAST(numFieldId AS VARCHAR) +',numItemCode)',vcDbColumnName) + ' LIKE ''%' + @strItemSearch + '%''' FROM #TempItemSearchFields FOR XML PATH(''))
									, 1
									, 3
									, ''
								)
			end
			else
			begin
			SELECT  @strSearch = STUFF(
									(SELECT ' OR ' +  IIF(Custom = 1,'dbo.GetCustFldValueItem('+ CAST(numFieldId AS VARCHAR) +',numItemCode)',vcDbColumnName) + ' LIKE ''' + @strItemSearch + '%''' FROM #TempItemSearchFields FOR XML PATH(''))
									, 1
									, 3
									, ''
								)
			end
		END
		ELSE
		BEGIN
			if(@isStartWithSearch=0)
			begin
				SELECT @strSearch = 'vcItemName LIKE ''%' + @strItemSearch + '%'''
			end
			else
			begin
				SELECT @strSearch = 'vcItemName LIKE ' + @strItemSearch + '%'''
			end
		END

		SET @strSQL = 'SELECT
							numItemCode, vcItemName, vcPathForTImage, vcCompanyName, MIN(monListPrice) monListPrice, txtItemDesc, 
							vcModelID, numBarCodeId, MIN(vcBarCode) vcBarCode, vcSKU, MIN(vcWHSKU) vcWHSKU, vcPartNo,
							bitSerialized, numItemGroup, Isarchieve, charItemType, 0 AS numWarehouseItemID, '''' vcAttributes
						FROM
						(
						SELECT
							Item.numItemCode,
							ISNULL(vcItemName,'''') AS vcItemName,
							ISNULL((SELECT TOP 1 vcPathForTImage from ItemImages WHERE numItemCode = Item.numItemCode),'''') as vcPathForTImage,
							ISNULL(vcCompanyName,'''') AS vcCompanyName,
							ISNULL(monListPrice,''0.00'') AS monListPrice,
							ISNULL(txtItemDesc,'''') AS txtItemDesc,
							ISNULL(vcModelID,'''') AS vcModelID,
							ISNULL(numBarCodeId,'''') AS numBarCodeId,
							ISNULL(vcBarCode,'''') AS vcBarCode,
							ISNULL(vcWHSKU,'''') AS vcWHSKU,
							ISNULL(vcSKU,'''') AS vcSKU,
							ISNULL(vcPartNo,'''') AS vcPartNo,
							ISNULL(bitSerialized,0) AS bitSerialized,
							ISNULL(numItemGroup,0) AS numItemGroup,
							ISNULL(Isarchieve,0) AS Isarchieve,
							ISNULL(charItemType,'''') AS charItemType
						FROM
							Item
						LEFT JOIN
							DivisionMaster 
						ON
							Item.numVendorID = DivisionMaster.numDivisionID
						LEFT JOIN
							CompanyInfo
						ON
							DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
						LEFT JOIN
							Vendor
						ON
							Item.numVendorID = Vendor.numVendorID AND
							Vendor.numItemCode = Item.numItemCode
						LEFT JOIN
							WareHouseItems
						ON
							Item.numItemCode = WareHouseItems.numItemID
						WHERE 
							Item.numDomainID = ' + CAST(@numDomainID AS VARCHAR) + ' AND 1=' +
									CASE @ItemListRights 
										WHEN 0 THEN ' CASE WHEN Item.numCreatedBy=0 THEN 1 ELSE 0 END'
										WHEN 1 THEN ' CASE WHEN Item.numCreatedBy= '+convert(varchar(15),@numUserCntID) + ' THEN 1 ELSE 0 END'                        
										ELSE '1'
									END + '
						)V WHERE ' + @strSearch  + '
						GROUP BY
							numItemCode, vcItemName, vcPathForTImage, vcCompanyName, txtItemDesc, 
							vcModelID, numBarCodeId, vcSKU, bitSerialized, numItemGroup, Isarchieve, charItemType, vcPartNo'
	
		IF LEN(@strAttributeSearch) > 0
		BEGIN
			SET @strSQL = 'SELECT
								numItemCode, vcItemName, vcPathForTImage, vcCompanyName, TEMPAttributes.monWListPrice AS monListPrice, txtItemDesc, 
								vcModelID, numBarCodeId, TEMPAttributes.vcBarCode, vcSKU, TEMPAttributes.vcWHSKU, vcPartNo, TEMPAttributes.numWareHouseItemID,
								TEMPAttributes.vcAttributes
							FROM
								(' + @strSQL + ') TEMP
							OUTER APPLY
							(
								SELECT 
									WHI.numWareHouseItemID,
									WHI.vcBarCode,
									WHI.vcWHSKU,
									WHI.monWListPrice,
									STUFF(
												(SELECT 
												'', '' +  vcAttribute
												FROM  
												(
												SELECT
													CONCAT(	CFM.Fld_label, '' : '',
													(CASE 
														WHEN CFM.Fld_type = ''SelectBox''
														THEN 
															(select vcData from ListDetails where numListID= CFM.numlistid AND numListItemID=CFVSI.Fld_Value)
														ELSE
															CFVSI.Fld_Value
													END)) vcAttribute
												FROM
													CFW_Fld_Values_Serialized_Items CFVSI
												INNER JOIN
													CFW_Fld_Master CFM
												ON
													CFM.Fld_id = CFVSI.Fld_ID
												WHERE
													RecId = WHI.numWareHouseItemID
													AND bitSerialized = TEMP.bitSerialized
												) TEMP
												FOR XML PATH(''''))
											, 1
											, 1
											, ''''
											) AS vcAttributes
								FROM
									dbo.WareHouseItems WHI
								WHERE 
									WHI.numItemID = TEMP.numItemCode
									AND ISNULL(TEMP.numItemGroup,0) > 0
									AND ISNULL(TEMP.Isarchieve, 0) <> 1
									AND TEMP.charItemType NOT IN (''A'')
							) AS TEMPAttributes
							WHERE
								' + @strAttributeSearch
		END

		EXEC ('INSERT INTO #TempSearchedItem SELECT numItemCode, vcItemName, vcPathForTImage, vcCompanyName, monListPrice, txtItemDesc, vcModelID, numBarCodeId, vcBarCode, vcSKU, vcWHSKU, vcPartNo, numWarehouseItemID, vcAttributes FROM (' + @strSQL + ')TEMPFinal')

		IF @searchCriteria = '1' --Items
		BEGIN
			SET @strSQL = 'SELECT * ' + @vcCustomDisplayField + ' FROM #TempSearchedItem ORDER BY numItemCode OFFSET ' + CAST(@numSkip AS VARCHAR(100)) + ' ROWS FETCH NEXT 10 ROWS ONLY'
			EXEC (@strSQL)
			SELECT COUNT(*) AS TotalRows FROM #TempSearchedItem
		END
		ELSE IF @searchCriteria = '3' -- Opp/Orders
		BEGIN   
			SET @SalesOppRights = ( SELECT TOP 1
											MAX(GA.intViewAllowed) AS intViewAllowed
										FROM   UserMaster UM INNER JOIN GroupAuthorization GA ON GA.numGroupID = UM.numGroupID INNER JOIN PageMaster PM ON PM.numPageID = GA.numPageID AND PM.numModuleID = GA.numModuleID
										WHERE  PM.numpageID = 2
											AND PM.numModuleID = 10
											AND UM.numUserID = ( SELECT numUserID
																	FROM   userMaster
																	WHERE  numUserDetailId = @numUserCntID
																)
										GROUP BY UM.numUserId ,
											UM.vcUserName ,
											PM.vcFileName ,
											GA.numModuleID ,
											GA.numPageID
									)                        
                    
			SET @PurchaseOppRights = ( SELECT TOP 1
											MAX(GA.intViewAllowed) AS intViewAllowed
									FROM    UserMaster UM INNER JOIN GroupAuthorization GA ON GA.numGroupID = UM.numGroupID INNER JOIN PageMaster PM ON PM.numPageID = GA.numPageID AND PM.numModuleID = GA.numModuleID
									WHERE PM.numpageID = 8
											AND PM.numModuleID = 10
											AND UM.numUserID = ( SELECT numUserID
																	FROM   userMaster
																	WHERE  numUserDetailId = @numUserCntID
																)
									GROUP BY UM.numUserId ,
											UM.vcUserName ,
											PM.vcFileName ,
											GA.numModuleID ,
											GA.numPageID
									)                
              
			SET @SalesOrderRights = ( SELECT TOP 1
										MAX(GA.intViewAllowed) AS intViewAllowed
									FROM   UserMaster UM INNER JOIN GroupAuthorization GA ON GA.numGroupID = UM.numGroupID INNER JOIN PageMaster PM ON PM.numPageID = GA.numPageID AND PM.numModuleID = GA.numModuleID
									WHERE  PM.numpageID = 10
										AND PM.numModuleID = 10
										AND UM.numUserID = ( SELECT numUserID
																FROM   userMaster
																WHERE  numUserDetailId = @numUserCntID
															)
									GROUP BY UM.numUserId ,
										UM.vcUserName ,
										PM.vcFileName ,
										GA.numModuleID ,
										GA.numPageID
								)  
		
			SET @PurchaseOrderRights = ( SELECT TOP 1
										MAX(GA.intViewAllowed) AS intViewAllowed
									FROM   UserMaster UM INNER JOIN GroupAuthorization GA ON GA.numGroupID = UM.numGroupID INNER JOIN PageMaster PM ON PM.numPageID = GA.numPageID AND PM.numModuleID = GA.numModuleID
									WHERE  PM.numpageID = 11
										AND PM.numModuleID = 10
										AND UM.numUserID = ( SELECT numUserID
																FROM   userMaster
																WHERE  numUserDetailId = @numUserCntID
															)
									GROUP BY UM.numUserId ,
										UM.vcUserName ,
										PM.vcFileName ,
										GA.numModuleID ,
										GA.numPageID
								)  

			DECLARE @TEMPItemOrder TABLE
			(
				numOppId NUMERIC(18,0),
				CreatedDate DATETIME,
				vcPOppName VARCHAR(200),
				vcOppType VARCHAR(50),
				vcCompanyName  VARCHAR(200),
				tintType TINYINT
			)

			INSERT INTO 
				@TEMPItemOrder
			SELECT
				numOppId,
				bintCreatedDate,
				vcPOppName,
				vcOppType,
				vcCompanyName,
				tintType
			FROM
				(
					SELECT
						OpportunityMaster.numOppId,
						OpportunityMaster.vcPOppName,
						CASE 
							WHEN tintOppType = 1 THEN 'Sales ' + (CASE WHEN tintOppStatus = 1 THEN 'Order' ELSE 'Opportunity' END)
							WHEN tintOppType = 2 THEN 'Purchase ' + (CASE WHEN tintOppStatus = 1 THEN 'Order' ELSE 'Opportunity' END)
						END vcOppType,
						CompanyInfo.vcCompanyName,
						OpportunityMaster.bintCreatedDate,
						1 AS tintType
					FROM 
						OpportunityMaster
					INNER JOIN
						(
							SELECT
								numOppID
							FROM 
								OpportunityItems OI
							INNER JOIN
								#TempSearchedItem
							ON
								OI.numItemCode = #TempSearchedItem.numItemCode
								AND (OI.numWarehouseItmsID = #TempSearchedItem.numWarehouseItemID OR ISNULL(#TempSearchedItem.numWarehouseItemID,0) = 0)
							GROUP BY
								numOppID
						) TempOppItems
					ON
						OpportunityMaster.numOppId = TempOppItems.numOppId
					INNER JOIN
						DivisionMaster 
					ON
						OpportunityMaster.numDivisionId = DivisionMaster.numDivisionID
					INNER JOIN
						CompanyInfo
					ON
						DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
					WHERE
						OpportunityMaster.numDomainId =@numDomainID
						AND tintOppType <> 0
						AND 1 = (
								CASE @orderType
								 WHEN 0 THEN 1
								 WHEN 1 THEN (CASE WHEN (tintOppType = 1 AND tintOppStatus=1) THEN 1 ELSE 0 END)
								 WHEN 2 THEN (CASE WHEN (tintOppType = 2 AND tintOppStatus=1) THEN 1 ELSE 0 END)
								 WHEN 3 THEN (CASE WHEN (tintOppType = 1 AND ISNULL(tintOppStatus,0)=0) THEN 1 ELSE 0 END)
								 WHEN 4 THEN (CASE WHEN (tintOppType = 2 AND ISNULL(tintOppStatus,0)=0) THEN 1 ELSE 0 END)
								 WHEN 5 THEN 0
								 WHEN 6 THEN 0
								END
							)
						AND (OpportunityMaster.numAssignedTo=@numUserCntID AND 1 = (CASE 
								WHEN tintOppType=1 AND ISNULL(tintOppStatus,0)=0
								THEN 
									CASE @SalesOppRights
										WHEN 0 THEN (CASE WHEN ISNULL(OpportunityMaster.numRecOwner,0) = 0 THEN 1 ELSE 0 END)
										WHEN 1 THEN (CASE WHEN ISNULL(OpportunityMaster.numRecOwner,0) = @numUserCntID THEN 1 ELSE 0 END)
										WHEN 2 THEN (CASE WHEN (DivisionMaster.numTerID IN (select numTerritoryID  from UserTerritory where numUserCntID=@numUserCntID) OR DivisionMaster.numTerID=0) THEN 1 ELSE 0 END)
										ELSE 1
									END
								WHEN tintOppType=2 AND ISNULL(tintOppStatus,0)=0
								THEN 
									CASE @PurchaseOrderRights
										WHEN 0 THEN (CASE WHEN ISNULL(OpportunityMaster.numRecOwner,0) = 0 THEN 1 ELSE 0 END)
										WHEN 1 THEN (CASE WHEN ISNULL(OpportunityMaster.numRecOwner,0) = @numUserCntID THEN 1 ELSE 0 END)
										WHEN 2 THEN (CASE WHEN (DivisionMaster.numTerID IN (select numTerritoryID  from UserTerritory where numUserCntID=@numUserCntID) OR DivisionMaster.numTerID=0) THEN 1 ELSE 0 END)
										ELSE 1
									END
								WHEN tintOppType=1 AND ISNULL(tintOppStatus,0)=1
								THEN 
									CASE @SalesOrderRights
										WHEN 0 THEN (CASE WHEN ISNULL(OpportunityMaster.numRecOwner,0) = 0 THEN 1 ELSE 0 END)
										WHEN 1 THEN (CASE WHEN ISNULL(OpportunityMaster.numRecOwner,0) = @numUserCntID THEN 1 ELSE 0 END)
										WHEN 2 THEN (CASE WHEN (DivisionMaster.numTerID IN (select numTerritoryID  from UserTerritory where numUserCntID=@numUserCntID) OR DivisionMaster.numTerID=0) THEN 1 ELSE 0 END)
										ELSE 1
									END
								WHEN tintOppType=2 AND ISNULL(tintOppStatus,0)=1
								THEN 
									CASE @PurchaseOrderRights
										WHEN 0 THEN (CASE WHEN ISNULL(OpportunityMaster.numRecOwner,0) = 0 THEN 1 ELSE 0 END)
										WHEN 1 THEN (CASE WHEN ISNULL(OpportunityMaster.numRecOwner,0) = @numUserCntID THEN 1 ELSE 0 END)
										WHEN 2 THEN (CASE WHEN (DivisionMaster.numTerID IN (select numTerritoryID  from UserTerritory where numUserCntID=@numUserCntID) OR DivisionMaster.numTerID=0) THEN 1 ELSE 0 END)
										ELSE 1
									END
								ELSE 1
							END))
					UNION
					SELECT
						ReturnHeader.numReturnHeaderID AS numOppID,
						ReturnHeader.vcRMA AS vcPOppName,
						CASE 
							WHEN tintReturnType = 1 THEN 'Sales Return'
							WHEN tintReturnType = 2 THEN 'Purchase Return'
						END vcOppType,
						CompanyInfo.vcCompanyName,
						ReturnHeader.dtCreatedDate AS bintCreatedDate,
						2 AS tintType
					FROM
						ReturnHeader
					INNER JOIN
						(
							SELECT
								numReturnHeaderID
							FROM 
								ReturnItems RI
							INNER JOIN
								#TempSearchedItem
							ON
								RI.numItemCode = #TempSearchedItem.numItemCode
								AND (RI.numWareHouseItemID = #TempSearchedItem.numWarehouseItemID OR ISNULL(#TempSearchedItem.numWarehouseItemID,0) = 0)
							GROUP BY
								numReturnHeaderID
						) TempReturnItems
					ON
						ReturnHeader.numReturnHeaderID = TempReturnItems.numReturnHeaderID
					INNER JOIN
						DivisionMaster 
					ON
						ReturnHeader.numDivisionId = DivisionMaster.numDivisionID
					INNER JOIN
						CompanyInfo
					ON
						DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
					WHERE
						ReturnHeader.numDomainId = @numDomainID 
						AND 1 = (
								CASE @orderType
								 WHEN 0 THEN (CASE WHEN tintReturnType = 1 OR tintReturnType = 2 THEN 1 ELSE 0 END)
								 WHEN 1 THEN 0
								 WHEN 2 THEN 0
								 WHEN 3 THEN 0
								 WHEN 4 THEN 0
								 WHEN 5 THEN (CASE WHEN tintReturnType = 1 THEN 1 ELSE 0 END)
								 WHEN 6 THEN (CASE WHEN tintReturnType = 2 THEN 1 ELSE 0 END)
								END
							)
				) TEMPFinal

			SELECT 
				ISNULL(numOppId,0) numOppId,
				ISNULL(dbo.FormatedDateFromDate(CreatedDate,@numDomainID),'') bintCreatedDate,
				ISNULL(vcPOppName,'') vcPOppName,
				ISNULL(vcOppType,'') vcOppType,
				ISNULL(vcCompanyName,'') vcCompanyName,
				ISNULL(tintType,0) tintType
			FROM
				@TEMPItemOrder
			ORDER BY
				CreatedDate DESC
			OFFSET @numSkip ROWS FETCH NEXT 10 ROWS ONLY	
				
			SELECT COUNT(*) AS TotalRows FROM @TEMPItemOrder	
		END
		ELSE IF @searchCriteria = '4' -- BizDocs
		BEGIN
			SET @SalesOppRights = ( SELECT TOP 1
											MAX(GA.intViewAllowed) AS intViewAllowed
										FROM   UserMaster UM INNER JOIN GroupAuthorization GA ON GA.numGroupID = UM.numGroupID INNER JOIN PageMaster PM ON PM.numPageID = GA.numPageID AND PM.numModuleID = GA.numModuleID
										WHERE  PM.numpageID = 2
											AND PM.numModuleID = 10
											AND UM.numUserID = ( SELECT numUserID
																	FROM   userMaster
																	WHERE  numUserDetailId = @numUserCntID
																)
										GROUP BY UM.numUserId ,
											UM.vcUserName ,
											PM.vcFileName ,
											GA.numModuleID ,
											GA.numPageID
									)                        
                    
			SET @PurchaseOppRights = ( SELECT TOP 1
											MAX(GA.intViewAllowed) AS intViewAllowed
									FROM    UserMaster UM INNER JOIN GroupAuthorization GA ON GA.numGroupID = UM.numGroupID INNER JOIN PageMaster PM ON PM.numPageID = GA.numPageID AND PM.numModuleID = GA.numModuleID
									WHERE PM.numpageID = 8
											AND PM.numModuleID = 10
											AND UM.numUserID = ( SELECT numUserID
																	FROM   userMaster
																	WHERE  numUserDetailId = @numUserCntID
																)
									GROUP BY UM.numUserId ,
											UM.vcUserName ,
											PM.vcFileName ,
											GA.numModuleID ,
											GA.numPageID
									)                
              
			SET @SalesOrderRights = ( SELECT TOP 1
										MAX(GA.intViewAllowed) AS intViewAllowed
									FROM   UserMaster UM INNER JOIN GroupAuthorization GA ON GA.numGroupID = UM.numGroupID INNER JOIN PageMaster PM ON PM.numPageID = GA.numPageID AND PM.numModuleID = GA.numModuleID
									WHERE  PM.numpageID = 10
										AND PM.numModuleID = 10
										AND UM.numUserID = ( SELECT numUserID
																FROM   userMaster
																WHERE  numUserDetailId = @numUserCntID
															)
									GROUP BY UM.numUserId ,
										UM.vcUserName ,
										PM.vcFileName ,
										GA.numModuleID ,
										GA.numPageID
								)  
		
			SET @PurchaseOrderRights = ( SELECT TOP 1
										MAX(GA.intViewAllowed) AS intViewAllowed
									FROM   UserMaster UM INNER JOIN GroupAuthorization GA ON GA.numGroupID = UM.numGroupID INNER JOIN PageMaster PM ON PM.numPageID = GA.numPageID AND PM.numModuleID = GA.numModuleID
									WHERE  PM.numpageID = 11
										AND PM.numModuleID = 10
										AND UM.numUserID = ( SELECT numUserID
																FROM   userMaster
																WHERE  numUserDetailId = @numUserCntID
															)
									GROUP BY UM.numUserId ,
										UM.vcUserName ,
										PM.vcFileName ,
										GA.numModuleID ,
										GA.numPageID
								) 

			SET @BizDocsRights = ( SELECT TOP 1
											MAX(GA.intViewAllowed) AS intViewAllowed
										FROM   UserMaster UM INNER JOIN GroupAuthorization GA ON GA.numGroupID = UM.numGroupID INNER JOIN PageMaster PM ON PM.numPageID = GA.numPageID AND PM.numModuleID = GA.numModuleID
										WHERE  PM.numpageID = 6
											AND PM.numModuleID = 10
											AND UM.numUserID = ( SELECT numUserID
																	FROM   userMaster
																	WHERE  numUserDetailId = @numUserCntID
																)
										GROUP BY UM.numUserId ,
											UM.vcUserName ,
											PM.vcFileName ,
											GA.numModuleID ,
											GA.numPageID
									) 

			DECLARE @TEMPItemBizDoc TABLE
			(
				numOppBizDocsId NUMERIC(18,0),
				CreatedDate DATETIME,
				vcBizDocID VARCHAR(200),
				vcBizDocType VARCHAR(100),
				vcCompanyName  VARCHAR(200)
			)

			INSERT INTO
				@TEMPItemBizDoc
			SELECT 
				numOppBizDocsId,
				OpportunityBizDocs.dtCreatedDate,
				vcBizDocID,
				ListDetails.vcData AS vcBizDocType,
				CompanyInfo.vcCompanyName
			FROM 
				OpportunityBizDocs
			INNER JOIN
				(
					SELECT
						numOppBizDocID
					FROM 
						OpportunityBizDocItems OBI
					INNER JOIN
						#TempSearchedItem
					ON
						OBI.numItemCode = #TempSearchedItem.numItemCode
						AND (OBI.numWarehouseItmsID = #TempSearchedItem.numWarehouseItemID OR ISNULL(#TempSearchedItem.numWarehouseItemID,0) = 0)
					GROUP BY
						numOppBizDocID
				) TempOppBizDocItems
			ON
				OpportunityBizDocs.numOppBizDocsId = TempOppBizDocItems.numOppBizDocID
			INNER JOIN
				OpportunityMaster
			ON
				OpportunityBizDocs.numOppId = OpportunityMaster.numOppId
			INNER JOIN
				DivisionMaster
			ON
				OpportunityMaster.numDivisionId = DivisionMaster.numDivisionID
			INNER JOIN
				CompanyInfo
			ON
				DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
			INNER JOIN
				ListDetails
			ON
				ListDetails.numListID = 27
				AND OpportunityBizDocs.numBizDocId = ListDetails.numListItemID
			WHERE
				OpportunityMaster.numDomainId = @numDomainID
				AND (OpportunityBizDocs.numBizDocID = @bizDocType OR ISNULL(@bizDocType,0) = 0)
				AND (OpportunityMaster.numAssignedTo=@numUserCntID OR 1 = (CASE 
								WHEN tintOppType=1 AND ISNULL(tintOppStatus,0)=0
								THEN 
									CASE @SalesOppRights
										WHEN 0 THEN (CASE WHEN ISNULL(OpportunityMaster.numRecOwner,0) = 0 THEN 1 ELSE 0 END)
										WHEN 1 THEN (CASE WHEN ISNULL(OpportunityMaster.numRecOwner,0) = @numUserCntID THEN 1 ELSE 0 END)
										WHEN 2 THEN (CASE WHEN (DivisionMaster.numTerID IN (select numTerritoryID  from UserTerritory where numUserCntID=@numUserCntID) OR DivisionMaster.numTerID=0) THEN 1 ELSE 0 END)
										ELSE 1
									END
								WHEN tintOppType=2 AND ISNULL(tintOppStatus,0)=0
								THEN 
									CASE @PurchaseOrderRights
										WHEN 0 THEN (CASE WHEN ISNULL(OpportunityMaster.numRecOwner,0) = 0 THEN 1 ELSE 0 END)
										WHEN 1 THEN (CASE WHEN ISNULL(OpportunityMaster.numRecOwner,0) = @numUserCntID THEN 1 ELSE 0 END)
										WHEN 2 THEN (CASE WHEN (DivisionMaster.numTerID IN (select numTerritoryID  from UserTerritory where numUserCntID=@numUserCntID) OR DivisionMaster.numTerID=0) THEN 1 ELSE 0 END)
										ELSE 1
									END
								WHEN tintOppType=1 AND ISNULL(tintOppStatus,0)=1
								THEN 
									CASE @SalesOrderRights
										WHEN 0 THEN (CASE WHEN ISNULL(OpportunityMaster.numRecOwner,0) = 0 THEN 1 ELSE 0 END)
										WHEN 1 THEN (CASE WHEN ISNULL(OpportunityMaster.numRecOwner,0) = @numUserCntID THEN 1 ELSE 0 END)
										WHEN 2 THEN (CASE WHEN (DivisionMaster.numTerID IN (select numTerritoryID  from UserTerritory where numUserCntID=@numUserCntID) OR DivisionMaster.numTerID=0) THEN 1 ELSE 0 END)
										ELSE 1
									END
								WHEN tintOppType=2 AND ISNULL(tintOppStatus,0)=1
								THEN 
									CASE @PurchaseOrderRights
										WHEN 0 THEN (CASE WHEN ISNULL(OpportunityMaster.numRecOwner,0) = 0 THEN 1 ELSE 0 END)
										WHEN 1 THEN (CASE WHEN ISNULL(OpportunityMaster.numRecOwner,0) = @numUserCntID THEN 1 ELSE 0 END)
										WHEN 2 THEN (CASE WHEN (DivisionMaster.numTerID IN (select numTerritoryID  from UserTerritory where numUserCntID=@numUserCntID) OR DivisionMaster.numTerID=0) THEN 1 ELSE 0 END)
										ELSE 1
									END
								ELSE 1
							END))
				AND 1 = CASE @BizDocsRights
							WHEN 0 THEN (CASE WHEN ISNULL(OpportunityBizDocs.numCreatedBy,0) = 0 THEN 1 ELSE 0 END)
							WHEN 1 THEN (CASE WHEN ISNULL(OpportunityBizDocs.numCreatedBy,0) = @numUserCntID THEN 1 ELSE 0 END)
							WHEN 2 THEN (CASE WHEN (DivisionMaster.numTerID IN (select numTerritoryID  from UserTerritory where numUserCntID=@numUserCntID) OR DivisionMaster.numTerID=0) THEN 1 ELSE 0 END)
							ELSE 1
						END

			SELECT
				ISNULL(numOppBizDocsId,0) numOppBizDocsId,
				ISNULL(dbo.FormatedDateFromDate(CreatedDate,@numDomainID),'') AS dtCreatedDate,
				ISNULL(vcBizDocID,'') vcBizDocID,
				ISNULL(vcBizDocType,'') vcBizDocType,
				ISNULL(vcCompanyName,'') vcCompanyName
			FROM
				@TEMPItemBizDoc
			ORDER BY
				CreatedDate DESC
			OFFSET @numSkip ROWS FETCH NEXT 10 ROWS ONLY
				
			SELECT COUNT(*) AS TotalRows FROM @TEMPItemBizDoc
		END

		
		DROP TABLE #TempItemSearchFields
		DROP TABLE #TempItemCustomDisplayFields
		DROP TABLE #TempSearchedItem
	END
	ELSE IF @searchType = '3' -- Opps/Orders
	BEGIN
		SET @SalesOppRights = ( SELECT TOP 1
										MAX(GA.intViewAllowed) AS intViewAllowed
									FROM   UserMaster UM INNER JOIN GroupAuthorization GA ON GA.numGroupID = UM.numGroupID INNER JOIN PageMaster PM ON PM.numPageID = GA.numPageID AND PM.numModuleID = GA.numModuleID
									WHERE  PM.numpageID = 2
										AND PM.numModuleID = 10
										AND UM.numUserID = ( SELECT numUserID
																FROM   userMaster
																WHERE  numUserDetailId = @numUserCntID
															)
									GROUP BY UM.numUserId ,
										UM.vcUserName ,
										PM.vcFileName ,
										GA.numModuleID ,
										GA.numPageID
								)                        
                    
		SET @PurchaseOppRights = ( SELECT TOP 1
										MAX(GA.intViewAllowed) AS intViewAllowed
								FROM    UserMaster UM INNER JOIN GroupAuthorization GA ON GA.numGroupID = UM.numGroupID INNER JOIN PageMaster PM ON PM.numPageID = GA.numPageID AND PM.numModuleID = GA.numModuleID
								WHERE PM.numpageID = 8
										AND PM.numModuleID = 10
										AND UM.numUserID = ( SELECT numUserID
																FROM   userMaster
																WHERE  numUserDetailId = @numUserCntID
															)
								GROUP BY UM.numUserId ,
										UM.vcUserName ,
										PM.vcFileName ,
										GA.numModuleID ,
										GA.numPageID
								)                
              
		SET @SalesOrderRights = ( SELECT TOP 1
									MAX(GA.intViewAllowed) AS intViewAllowed
								FROM   UserMaster UM INNER JOIN GroupAuthorization GA ON GA.numGroupID = UM.numGroupID INNER JOIN PageMaster PM ON PM.numPageID = GA.numPageID AND PM.numModuleID = GA.numModuleID
								WHERE  PM.numpageID = 10
									AND PM.numModuleID = 10
									AND UM.numUserID = ( SELECT numUserID
															FROM   userMaster
															WHERE  numUserDetailId = @numUserCntID
														)
								GROUP BY UM.numUserId ,
									UM.vcUserName ,
									PM.vcFileName ,
									GA.numModuleID ,
									GA.numPageID
							)  
		
		SET @PurchaseOrderRights = ( SELECT TOP 1
									MAX(GA.intViewAllowed) AS intViewAllowed
								FROM   UserMaster UM INNER JOIN GroupAuthorization GA ON GA.numGroupID = UM.numGroupID INNER JOIN PageMaster PM ON PM.numPageID = GA.numPageID AND PM.numModuleID = GA.numModuleID
								WHERE  PM.numpageID = 11
									AND PM.numModuleID = 10
									AND UM.numUserID = ( SELECT numUserID
															FROM   userMaster
															WHERE  numUserDetailId = @numUserCntID
														)
								GROUP BY UM.numUserId ,
									UM.vcUserName ,
									PM.vcFileName ,
									GA.numModuleID ,
									GA.numPageID
							)  

		PRINT @SalesOppRights
		PRINT @PurchaseOppRights
		PRINT @SalesOrderRights
		PRINT @PurchaseOrderRights
		DECLARE @TEMPORDER TABLE
		(
			numOppId NUMERIC(18,0),
			CreatedDate DATETIME,
			vcPOppName VARCHAR(200),
			vcOppType VARCHAR(50),
			vcCompanyName  VARCHAR(200),
			tintType TINYINT
		)

		INSERT INTO 
			@TEMPORDER
		SELECT
			numOppId,
			bintCreatedDate,
			vcPOppName,
			vcOppType,
			vcCompanyName,
			tintType
		FROM
			(
				SELECT
					OpportunityMaster.numOppId,
					OpportunityMaster.vcPOppName,
					CASE 
						WHEN tintOppType = 1 THEN 'Sales ' + (CASE WHEN tintOppStatus = 1 THEN 'Order' ELSE 'Opportunity' END)
						WHEN tintOppType = 2 THEN 'Purchase ' + (CASE WHEN tintOppStatus = 1 THEN 'Order' ELSE 'Opportunity' END)
					END vcOppType,
					CompanyInfo.vcCompanyName,
					OpportunityMaster.bintCreatedDate,
					1 AS tintType
				FROM 
					OpportunityMaster
				INNER JOIN
					DivisionMaster 
				ON
					OpportunityMaster.numDivisionId = DivisionMaster.numDivisionID
				INNER JOIN
					CompanyInfo
				ON
					DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
				WHERE
					OpportunityMaster.numDomainId =@numDomainID
					AND tintOppType <> 0
					AND (vcPOppName LIKE CASE WHEN @isStartWithSearch=0 THEN N'%' + @searchText + '%' ELSE N'' + @searchText + '%' END OR numOppId LIKE CASE WHEN @isStartWithSearch=0 THEN N'%' + @searchText + '%' ELSE N'' + @searchText + '%' END)
					AND 1 = (
							CASE @orderType
							 WHEN 0 THEN 1
							 WHEN 1 THEN (CASE WHEN (tintOppType = 1 AND tintOppStatus=1) THEN 1 ELSE 0 END)
							 WHEN 2 THEN (CASE WHEN (tintOppType = 2 AND tintOppStatus=1) THEN 1 ELSE 0 END)
							 WHEN 3 THEN (CASE WHEN (tintOppType = 1 AND ISNULL(tintOppStatus,0)=0) THEN 1 ELSE 0 END)
							 WHEN 4 THEN (CASE WHEN (tintOppType = 2 AND ISNULL(tintOppStatus,0)=0) THEN 1 ELSE 0 END)
							 WHEN 5 THEN 0
							 WHEN 6 THEN 0
							END
						)
					AND (OpportunityMaster.numAssignedTo=@numUserCntID OR 1 = (CASE 
								WHEN tintOppType=1 AND ISNULL(tintOppStatus,0)=0
								THEN 
									CASE @SalesOppRights
										WHEN 0 THEN (CASE WHEN ISNULL(OpportunityMaster.numRecOwner,0) = 0 THEN 1 ELSE 0 END)
										WHEN 1 THEN (CASE WHEN ISNULL(OpportunityMaster.numRecOwner,0) = @numUserCntID THEN 1 ELSE 0 END)
										WHEN 2 THEN (CASE WHEN (DivisionMaster.numTerID IN (select numTerritoryID  from UserTerritory where numUserCntID=@numUserCntID) OR DivisionMaster.numTerID=0) THEN 1 ELSE 0 END)
										ELSE 1
									END
								WHEN tintOppType=2 AND ISNULL(tintOppStatus,0)=0
								THEN 
									CASE @PurchaseOrderRights
										WHEN 0 THEN (CASE WHEN ISNULL(OpportunityMaster.numRecOwner,0) = 0 THEN 1 ELSE 0 END)
										WHEN 1 THEN (CASE WHEN ISNULL(OpportunityMaster.numRecOwner,0) = @numUserCntID THEN 1 ELSE 0 END)
										WHEN 2 THEN (CASE WHEN (DivisionMaster.numTerID IN (select numTerritoryID  from UserTerritory where numUserCntID=@numUserCntID) OR DivisionMaster.numTerID=0) THEN 1 ELSE 0 END)
										ELSE 1
									END
								WHEN tintOppType=1 AND ISNULL(tintOppStatus,0)=1
								THEN 
									CASE @SalesOrderRights
										WHEN 0 THEN (CASE WHEN ISNULL(OpportunityMaster.numRecOwner,0) = 0 THEN 1 ELSE 0 END)
										WHEN 1 THEN (CASE WHEN ISNULL(OpportunityMaster.numRecOwner,0) = @numUserCntID THEN 1 ELSE 0 END)
										WHEN 2 THEN (CASE WHEN (DivisionMaster.numTerID IN (select numTerritoryID  from UserTerritory where numUserCntID=@numUserCntID) OR DivisionMaster.numTerID=0) THEN 1 ELSE 0 END)
										ELSE 1
									END
								WHEN tintOppType=2 AND ISNULL(tintOppStatus,0)=1
								THEN 
									CASE @PurchaseOrderRights
										WHEN 0 THEN (CASE WHEN ISNULL(OpportunityMaster.numRecOwner,0) = 0 THEN 1 ELSE 0 END)
										WHEN 1 THEN (CASE WHEN ISNULL(OpportunityMaster.numRecOwner,0) = @numUserCntID THEN 1 ELSE 0 END)
										WHEN 2 THEN (CASE WHEN (DivisionMaster.numTerID IN (select numTerritoryID  from UserTerritory where numUserCntID=@numUserCntID) OR DivisionMaster.numTerID=0) THEN 1 ELSE 0 END)
										ELSE 1
									END
								ELSE 1
							END))
				UNION
				SELECT
					ReturnHeader.numReturnHeaderID AS numOppID,
					ReturnHeader.vcRMA AS vcPOppName,
					CASE 
						WHEN tintReturnType = 1 THEN 'Sales Return'
						WHEN tintReturnType = 2 THEN 'Purchase Return'
					END vcOppType,
					CompanyInfo.vcCompanyName,
					ReturnHeader.dtCreatedDate AS bintCreatedDate,
					2 AS tintType
				FROM
					ReturnHeader
				INNER JOIN
					DivisionMaster 
				ON
					ReturnHeader.numDivisionId = DivisionMaster.numDivisionID
				INNER JOIN
					CompanyInfo
				ON
					DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
				WHERE
					ReturnHeader.numDomainId = @numDomainID 
					AND (vcRMA LIKE CASE WHEN @isStartWithSearch=0 THEN N'%' + @searchText + '%' ELSE N'' + @searchText + '%' END OR ReturnHeader.numReturnHeaderID LIKE  CASE WHEN @isStartWithSearch=0 THEN N'%' + @searchText + '%' ELSE N'' + @searchText + '%' END)
					AND 1 = (
							CASE @orderType
							 WHEN 0 THEN (CASE WHEN tintReturnType = 1 OR tintReturnType = 2 THEN 1 ELSE 0 END)
							 WHEN 1 THEN 0
							 WHEN 2 THEN 0
							 WHEN 3 THEN 0
							 WHEN 4 THEN 0
							 WHEN 5 THEN (CASE WHEN tintReturnType = 1 THEN 1 ELSE 0 END)
							 WHEN 6 THEN (CASE WHEN tintReturnType = 2 THEN 1 ELSE 0 END)
							END
						)
				) TEMPFinal

		SELECT 
			ISNULL(numOppId,0) numOppId,
			ISNULL(dbo.FormatedDateFromDate(CreatedDate,@numDomainID),'') bintCreatedDate,
			ISNULL(vcPOppName,'') vcPOppName,
			ISNULL(vcOppType,'') vcOppType,
			ISNULL(vcCompanyName,'') vcCompanyName,
			ISNULL(tintType,0) tintType
		FROM
			@TEMPORDER
		ORDER BY
			CreatedDate DESC
		OFFSET @numSkip ROWS FETCH NEXT 10 ROWS ONLY	
				
		SELECT COUNT(*) AS TotalRows FROM @TEMPORDER		
	END
	ELSE IF @searchType = '4' -- BizDocs
	BEGIN
		SET @SalesOppRights = ( SELECT TOP 1
											MAX(GA.intViewAllowed) AS intViewAllowed
										FROM   UserMaster UM INNER JOIN GroupAuthorization GA ON GA.numGroupID = UM.numGroupID INNER JOIN PageMaster PM ON PM.numPageID = GA.numPageID AND PM.numModuleID = GA.numModuleID
										WHERE  PM.numpageID = 2
											AND PM.numModuleID = 10
											AND UM.numUserID = ( SELECT numUserID
																	FROM   userMaster
																	WHERE  numUserDetailId = @numUserCntID
																)
										GROUP BY UM.numUserId ,
											UM.vcUserName ,
											PM.vcFileName ,
											GA.numModuleID ,
											GA.numPageID
									)                        
                    
		SET @PurchaseOppRights = ( SELECT TOP 1
										MAX(GA.intViewAllowed) AS intViewAllowed
								FROM    UserMaster UM INNER JOIN GroupAuthorization GA ON GA.numGroupID = UM.numGroupID INNER JOIN PageMaster PM ON PM.numPageID = GA.numPageID AND PM.numModuleID = GA.numModuleID
								WHERE PM.numpageID = 8
										AND PM.numModuleID = 10
										AND UM.numUserID = ( SELECT numUserID
																FROM   userMaster
																WHERE  numUserDetailId = @numUserCntID
															)
								GROUP BY UM.numUserId ,
										UM.vcUserName ,
										PM.vcFileName ,
										GA.numModuleID ,
										GA.numPageID
								)                
              
		SET @SalesOrderRights = ( SELECT TOP 1
									MAX(GA.intViewAllowed) AS intViewAllowed
								FROM   UserMaster UM INNER JOIN GroupAuthorization GA ON GA.numGroupID = UM.numGroupID INNER JOIN PageMaster PM ON PM.numPageID = GA.numPageID AND PM.numModuleID = GA.numModuleID
								WHERE  PM.numpageID = 10
									AND PM.numModuleID = 10
									AND UM.numUserID = ( SELECT numUserID
															FROM   userMaster
															WHERE  numUserDetailId = @numUserCntID
														)
								GROUP BY UM.numUserId ,
									UM.vcUserName ,
									PM.vcFileName ,
									GA.numModuleID ,
									GA.numPageID
							)  
		
		SET @PurchaseOrderRights = ( SELECT TOP 1
									MAX(GA.intViewAllowed) AS intViewAllowed
								FROM   UserMaster UM INNER JOIN GroupAuthorization GA ON GA.numGroupID = UM.numGroupID INNER JOIN PageMaster PM ON PM.numPageID = GA.numPageID AND PM.numModuleID = GA.numModuleID
								WHERE  PM.numpageID = 11
									AND PM.numModuleID = 10
									AND UM.numUserID = ( SELECT numUserID
															FROM   userMaster
															WHERE  numUserDetailId = @numUserCntID
														)
								GROUP BY UM.numUserId ,
									UM.vcUserName ,
									PM.vcFileName ,
									GA.numModuleID ,
									GA.numPageID
							) 

		SET @BizDocsRights = ( SELECT TOP 1
										MAX(GA.intViewAllowed) AS intViewAllowed
									FROM   UserMaster UM INNER JOIN GroupAuthorization GA ON GA.numGroupID = UM.numGroupID INNER JOIN PageMaster PM ON PM.numPageID = GA.numPageID AND PM.numModuleID = GA.numModuleID
									WHERE  PM.numpageID = 6
										AND PM.numModuleID = 10
										AND UM.numUserID = ( SELECT numUserID
																FROM   userMaster
																WHERE  numUserDetailId = @numUserCntID
															)
									GROUP BY UM.numUserId ,
										UM.vcUserName ,
										PM.vcFileName ,
										GA.numModuleID ,
										GA.numPageID
								) 

		DECLARE @TEMPBizDoc TABLE
		(
			numOppBizDocsId NUMERIC(18,0),
			CreatedDate DATETIME,
			vcBizDocID VARCHAR(200),
			vcBizDocType VARCHAR(100),
			vcCompanyName  VARCHAR(200)
		)

		INSERT INTO
			@TEMPBizDoc
		SELECT 
			numOppBizDocsId,
			OpportunityBizDocs.dtCreatedDate,
			vcBizDocID,
			ListDetails.vcData AS vcBizDocType,
			CompanyInfo.vcCompanyName
		FROM 
			OpportunityBizDocs
		INNER JOIN
			OpportunityMaster
		ON
			OpportunityBizDocs.numOppId = OpportunityMaster.numOppId
		INNER JOIN
			DivisionMaster
		ON
			OpportunityMaster.numDivisionId = DivisionMaster.numDivisionID
		INNER JOIN
			CompanyInfo
		ON
			DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
		INNER JOIN
			ListDetails
		ON
			ListDetails.numListID = 27
			AND OpportunityBizDocs.numBizDocId = ListDetails.numListItemID
		WHERE
			OpportunityMaster.numDomainId = @numDomainID
			AND vcBizDocID LIKE CASE WHEN @isStartWithSearch=0 THEN N'%' + @searchText + '%' ELSE N'' + @searchText + '%' END
			AND (OpportunityBizDocs.numBizDocID = @bizDocType OR ISNULL(@bizDocType,0) = 0)
			AND (OpportunityMaster.numAssignedTo = @numUserCntID OR 1 = (CASE 
								WHEN tintOppType=1 AND ISNULL(tintOppStatus,0)=0
								THEN 
									CASE @SalesOppRights
										WHEN 0 THEN (CASE WHEN ISNULL(OpportunityMaster.numRecOwner,0) = 0 THEN 1 ELSE 0 END)
										WHEN 1 THEN (CASE WHEN ISNULL(OpportunityMaster.numRecOwner,0) = @numUserCntID THEN 1 ELSE 0 END)
										WHEN 2 THEN (CASE WHEN (DivisionMaster.numTerID IN (select numTerritoryID  from UserTerritory where numUserCntID=@numUserCntID) OR DivisionMaster.numTerID=0) THEN 1 ELSE 0 END)
										ELSE 1
									END
								WHEN tintOppType=2 AND ISNULL(tintOppStatus,0)=0
								THEN 
									CASE @PurchaseOrderRights
										WHEN 0 THEN (CASE WHEN ISNULL(OpportunityMaster.numRecOwner,0) = 0 THEN 1 ELSE 0 END)
										WHEN 1 THEN (CASE WHEN ISNULL(OpportunityMaster.numRecOwner,0) = @numUserCntID THEN 1 ELSE 0 END)
										WHEN 2 THEN (CASE WHEN (DivisionMaster.numTerID IN (select numTerritoryID  from UserTerritory where numUserCntID=@numUserCntID) OR DivisionMaster.numTerID=0) THEN 1 ELSE 0 END)
										ELSE 1
									END
								WHEN tintOppType=1 AND ISNULL(tintOppStatus,0)=1
								THEN 
									CASE @SalesOrderRights
										WHEN 0 THEN (CASE WHEN ISNULL(OpportunityMaster.numRecOwner,0) = 0 THEN 1 ELSE 0 END)
										WHEN 1 THEN (CASE WHEN ISNULL(OpportunityMaster.numRecOwner,0) = @numUserCntID THEN 1 ELSE 0 END)
										WHEN 2 THEN (CASE WHEN (DivisionMaster.numTerID IN (select numTerritoryID  from UserTerritory where numUserCntID=@numUserCntID) OR DivisionMaster.numTerID=0) THEN 1 ELSE 0 END)
										ELSE 1
									END
								WHEN tintOppType=2 AND ISNULL(tintOppStatus,0)=1
								THEN 
									CASE @PurchaseOrderRights
										WHEN 0 THEN (CASE WHEN ISNULL(OpportunityMaster.numRecOwner,0) = 0 THEN 1 ELSE 0 END)
										WHEN 1 THEN (CASE WHEN ISNULL(OpportunityMaster.numRecOwner,0) = @numUserCntID THEN 1 ELSE 0 END)
										WHEN 2 THEN (CASE WHEN (DivisionMaster.numTerID IN (select numTerritoryID  from UserTerritory where numUserCntID=@numUserCntID) OR DivisionMaster.numTerID=0) THEN 1 ELSE 0 END)
										ELSE 1
									END
								ELSE 1
							END))
				AND 1 = CASE @BizDocsRights
							WHEN 0 THEN (CASE WHEN ISNULL(OpportunityBizDocs.numCreatedBy,0) = 0 THEN 1 ELSE 0 END)
							WHEN 1 THEN (CASE WHEN ISNULL(OpportunityBizDocs.numCreatedBy,0) = @numUserCntID THEN 1 ELSE 0 END)
							WHEN 2 THEN (CASE WHEN (DivisionMaster.numTerID IN (select numTerritoryID  from UserTerritory where numUserCntID=@numUserCntID) OR DivisionMaster.numTerID=0) THEN 1 ELSE 0 END)
							ELSE 1
						END

		SELECT
			ISNULL(numOppBizDocsId,0) numOppBizDocsId,
			ISNULL(dbo.FormatedDateFromDate(CreatedDate,@numDomainID),'') AS dtCreatedDate,
			ISNULL(vcBizDocID,'') vcBizDocID,
			ISNULL(vcBizDocType,'') vcBizDocType,
			ISNULL(vcCompanyName,'') vcCompanyName
		FROM
			@TEMPBizDoc
		ORDER BY
			CreatedDate DESC
		OFFSET @numSkip ROWS FETCH NEXT 10 ROWS ONLY
				
		SELECT COUNT(*) AS TotalRows FROM @TEMPBizDoc
	END
	ELSE IF @searchType = '5' -- Contact
	BEGIN
		DECLARE @ContactsRights TINYINT
		SET @ContactsRights = ( SELECT TOP 1
										MAX(GA.intViewAllowed) AS intViewAllowed
									FROM   UserMaster UM INNER JOIN GroupAuthorization GA ON GA.numGroupID = UM.numGroupID INNER JOIN PageMaster PM ON PM.numPageID = GA.numPageID AND PM.numModuleID = GA.numModuleID
									WHERE  PM.numpageID = 2
										AND PM.numModuleID = 11
										AND UM.numUserID = ( SELECT numUserID
																FROM   userMaster
																WHERE  numUserDetailId = @numUserCntID
															)
									GROUP BY UM.numUserId ,
										UM.vcUserName ,
										PM.vcFileName ,
										GA.numModuleID ,
										GA.numPageID
								) 

		DECLARE @TableContact TABLE
		(
			numContactId NUMERIC(18,0),
			vcFirstName VARCHAR(100),
			vcLastname VARCHAR(100),
			vcFullName VARCHAR(200),
			vcEmail VARCHAR(100),
			numPhone VARCHAR(100),
			numPhoneExtension VARCHAR(100),
			vcCompanyName VARCHAR(200),
			bitPrimaryContact VARCHAR(5)
		)

		INSERT INTO
			@TableContact
		SELECT 
			ADC.numContactId,
			ISNULL(vcFirstName,'') AS vcFirstName,
			ISNULL(vcLastName,'') AS vcLastName,
			ISNULL(vcFirstName,'') + ' ' + ISNULL(vcLastName,'') AS vcFullName,
			ISNULL(vcEmail,'') AS vcEmail,
			ISNULL(ADC.numPhone,'') AS numPhone,
			ISNULL(ADC.numPhoneExtension,'') AS numPhoneExtension,
			ISNULL(CI.vcCompanyName,'') AS vcCompanyName,
			(CASE WHEN ISNULL(bitPrimaryContact,0) = 1 THEN 'Yes' ELSE 'No' END) AS bitPrimaryContact
		FROM 
			AdditionalContactsInformation ADC
		INNER JOIN
			DivisionMaster DM
		ON
			ADC.numDivisionId = DM.numDivisionID
		INNER JOIN
			CompanyInfo CI
		ON
			DM.numCompanyID = CI.numCompanyId
		WHERE 
			ADC.numDomainID = @numDomainID AND
			(vcFirstName LIKE CASE WHEN @isStartWithSearch=0 THEN '%' + @searchText + '%' ELSE '' + @searchText + '%' END
			OR vcLastName LIKE CASE WHEN @isStartWithSearch=0 THEN '%' + @searchText + '%' ELSE '' + @searchText + '%' END
			OR vcEmail LIKE CASE WHEN @isStartWithSearch=0 THEN '%' + @searchText + '%' ELSE '' + @searchText + '%' END
			OR numPhone LIKE CASE WHEN @isStartWithSearch=0 THEN '%' + @searchText + '%' ELSE '' + @searchText + '%' END)
			AND 1 = CASE @ContactsRights
							WHEN 0 THEN (CASE WHEN ISNULL(ADC.numRecOwner,0) = 0 THEN 1 ELSE 0 END)
							WHEN 1 THEN (CASE WHEN ISNULL(ADC.numRecOwner,0) = @numUserCntID THEN 1 ELSE 0 END)
							WHEN 2 THEN (CASE WHEN (DM.numTerID IN (select numTerritoryID  from UserTerritory where numUserCntID=@numUserCntID) OR DM.numTerID=0) THEN 1 ELSE 0 END)
							ELSE 1
						END

		SELECT 
			* 
		FROM 
			@TableContact	
		ORDER BY 
			vcCompanyName ASC, bitPrimaryContact DESC
		OFFSET 
			@numSkip 
		ROWS FETCH NEXT 
			10 
		ROWS ONLY

		SELECT COUNT(*) FROM @TableContact
	END
GO
/****** Object:  StoredProcedure [dbo].[USP_UpdatedomainDetails]    Script Date: 05/07/2009 21:56:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj                                      
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_updatedomaindetails')
DROP PROCEDURE usp_updatedomaindetails
GO
CREATE PROCEDURE [dbo].[USP_UpdatedomainDetails]                                      
@numDomainID as numeric(9)=0,                                      
@vcDomainName as varchar(50)='',                                      
@vcDomainDesc as varchar(100)='',                                      
@bitExchangeIntegration as bit,                                      
@bitAccessExchange as bit,                                      
@vcExchUserName as varchar(100)='',                                      
@vcExchPassword as varchar(100)='',                                      
@vcExchPath as varchar(200)='',                                      
@vcExchDomain as varchar(100)='',                                    
@tintCustomPagingRows as tinyint,                                    
@vcDateFormat as varchar(30)='',                                    
@numDefCountry as numeric(9),                                    
@tintComposeWindow as tinyint,                                    
@sintStartDate as smallint,                                    
@sintNoofDaysInterval as smallint,                                    
@tintAssignToCriteria as tinyint,                                    
@bitIntmedPage as bit,                                    
@tintFiscalStartMonth as tinyint,                                      
@tintPayPeriod as tinyint,                        
@numCurrencyID as numeric(9) ,                
@charUnitSystem as char(1)='',              
@vcPortalLogo as varchar(100)='',            
@tintChrForComSearch as tinyint  ,            
@intPaymentGateWay as int,    
@tintChrForItemSearch as tinyint,
@ShipCompany as numeric(9),
@bitAutoPopulateAddress as bit=null,
@tintPoulateAddressTo as tinyint=null,
@bitMultiCurrency as bit,
@bitCreateInvoice AS tinyint = null,
@numDefaultSalesBizDocID AS NUMERIC(9)=NULL,
@numDefaultPurchaseBizDocID AS NUMERIC(9)=NULL,
@bitSaveCreditCardInfo AS BIT = NULL,
@intLastViewedRecord AS INT=10,
@tintCommissionType AS TINYINT,
@tintBaseTax AS TINYINT,
@bitEmbeddedCost AS BIT,
@numDefaultSalesOppBizDocId NUMERIC=NULL,
@tintSessionTimeOut TINYINT=1,
@tintDecimalPoints TINYINT,
@bitCustomizePortal BIT=0,
@tintBillToForPO TINYINT,
@tintShipToForPO TINYINT,
@tintOrganizationSearchCriteria TINYINT,
@bitDocumentRepositary BIT=0,
@tintComAppliesTo TINYINT=0,
@bitGtoBContact BIT=0,
@bitBtoGContact BIT=0,
@bitGtoBCalendar BIT=0,
@bitBtoGCalendar BIT=0,
@bitExpenseNonInventoryItem BIT=0,
@bitInlineEdit BIT=0,
@bitRemoveVendorPOValidation BIT=0,
@tintBaseTaxOnArea tinyint=0,
@bitAmountPastDue BIT=0,
@monAmountPastDue money,
@bitSearchOrderCustomerHistory BIT=0,
@bitRentalItem as bit=0,
@numRentalItemClass as NUMERIC(9)=NULL,
@numRentalHourlyUOM as NUMERIC(9)=NULL,
@numRentalDailyUOM as NUMERIC(9)=NULL,
@tintRentalPriceBasedOn as tinyint=NULL,
@tintCalendarTimeFormat as tinyint=NULL,
@tintDefaultClassType as tinyint=NULL,
@tintPriceBookDiscount as tinyint=0,
@bitAutoSerialNoAssign as bit=0,
@bitIsShowBalance AS BIT = 0,
@numIncomeAccID NUMERIC(18, 0) = 0,
@numCOGSAccID NUMERIC(18, 0) = 0,
@numAssetAccID NUMERIC(18, 0) = 0,
@IsEnableProjectTracking AS BIT = 0,
@IsEnableCampaignTracking AS BIT = 0,
@IsEnableResourceScheduling AS BIT = 0,
@ShippingServiceItem  AS NUMERIC(9)=0,
@numSOBizDocStatus AS NUMERIC(9)=0,
@bitAutolinkUnappliedPayment AS BIT=0,
@numDiscountServiceItemID AS NUMERIC(9)=0,
@vcShipToPhoneNumber as VARCHAR(50)='',
@vcGAUserEmailId as VARCHAR(50)='',
@vcGAUserPassword as VARCHAR(50)='',
@vcGAUserProfileId as VARCHAR(50)='',
@bitDiscountOnUnitPrice AS BIT=0,
@intShippingImageWidth INT = 0,
@intShippingImageHeight INT = 0,
@numTotalInsuredValue NUMERIC(10,2) = 0,
@numTotalCustomsValue NUMERIC(10,2) = 0,
@vcHideTabs VARCHAR(1000) = '',
@bitUseBizdocAmount BIT = 0,
@bitDefaultRateType BIT = 0,
@bitIncludeTaxAndShippingInCommission  BIT = 0,
@bitPurchaseTaxCredit BIT=0,
@bitLandedCost BIT=0,
@vcLanedCostDefault VARCHAR(10)='',
@bitMinUnitPriceRule BIT = 0,
@numAbovePercent AS NUMERIC(18,2),
@numBelowPercent AS NUMERIC(18,2),
@numBelowPriceField AS NUMERIC(18,0),
@vcUnitPriceApprover AS VARCHAR(100),
@numDefaultSalesPricing AS TINYINT,
@bitReOrderPoint AS BIT = 0,
@numReOrderPointOrderStatus AS NUMERIC(18,0) = 0,
@numPODropShipBizDoc AS NUMERIC(18,0) = 0,
@numPODropShipBizDocTemplate AS NUMERIC(18,0) = 0,
@IsEnableDeferredIncome AS BIT = 0,
@bitApprovalforTImeExpense AS BIT=0,
@numCost AS Numeric(9,0)=0,
@intTimeExpApprovalProcess int=0,
@bitApprovalforOpportunity AS BIT=0,
@intOpportunityApprovalProcess int=0,
@numDefaultSalesShippingDoc NUMERIC(18,0)=0,
@numDefaultPurchaseShippingDoc NUMERIC(18,0)=0,
@bitchkOverRideAssignto AS BIT=0,
@vcPrinterIPAddress VARCHAR(15) = '',
@vcPrinterPort VARCHAR(5) = ''
as                                      
BEGIN
BEGIN TRY
BEGIN TRANSACTION

	IF ISNULL(@bitMinUnitPriceRule,0) = 1 AND ((SELECT COUNT(*) FROM UnitPriceApprover WHERE numDomainID=@numDomainID) = 0 OR ISNULL(@bitApprovalforOpportunity,0)=0 OR ISNULL(@intOpportunityApprovalProcess,0) = 0)
	BEGIN
		RAISERROR('INCOMPLETE_MIN_UNIT_PRICE_CONFIGURATION',16,1)
		RETURN
	END


	If ISNULL(@bitApprovalforTImeExpense,0) = 1 AND (SELECT
														COUNT(*) 
													FROM
														UserMaster
													LEFT JOIN
														ApprovalConfig
													ON
														ApprovalConfig.numUserId = UserMaster.numUserDetailId
													WHERE
														UserMaster.numDomainID=@numDomainID
														AND ISNULL(bitActivateFlag,0) = 1
														AND ISNULL(numLevel1Authority,0) = 0
														AND ISNULL(numLevel2Authority,0) = 0
														AND ISNULL(numLevel3Authority,0) = 0
														AND ISNULL(numLevel4Authority,0) = 0
														AND ISNULL(numLevel5Authority,0) = 0) > 0
	BEGIN
		RAISERROR('INCOMPLETE_TIMEEXPENSE_APPROVAL',16,1)
		RETURN
	END

                                  
 update Domain                                       
   set                                       
   vcDomainName=@vcDomainName,                                      
   vcDomainDesc=@vcDomainDesc,                                      
   bitExchangeIntegration=@bitExchangeIntegration,                                      
   bitAccessExchange=@bitAccessExchange,                                      
   vcExchUserName=@vcExchUserName,                                      
   vcExchPassword=@vcExchPassword,                                      
   vcExchPath=@vcExchPath,                                      
   vcExchDomain=@vcExchDomain,                                    
   tintCustomPagingRows =@tintCustomPagingRows,                                    
   vcDateFormat=@vcDateFormat,                                    
   numDefCountry =@numDefCountry,                                    
   tintComposeWindow=@tintComposeWindow,                                    
   sintStartDate =@sintStartDate,                                    
   sintNoofDaysInterval=@sintNoofDaysInterval,                                    
   tintAssignToCriteria=@tintAssignToCriteria,                                    
   bitIntmedPage=@bitIntmedPage,                                    
   tintFiscalStartMonth=@tintFiscalStartMonth,                            
--   bitDeferredIncome=@bitDeferredIncome,                          
   tintPayPeriod=@tintPayPeriod,
   vcCurrency=isnull((select varCurrSymbol from Currency Where numCurrencyID=@numCurrencyID  ),'')  ,                      
  numCurrencyID=@numCurrencyID,                
  charUnitSystem= @charUnitSystem,              
  vcPortalLogo=@vcPortalLogo ,            
  tintChrForComSearch=@tintChrForComSearch  ,      
  intPaymentGateWay=@intPaymentGateWay,
 tintChrForItemSearch=@tintChrForItemSearch,
 numShipCompany= @ShipCompany,
 bitAutoPopulateAddress = @bitAutoPopulateAddress,
 tintPoulateAddressTo =@tintPoulateAddressTo,
 --bitMultiCompany=@bitMultiCompany ,
 bitMultiCurrency=@bitMultiCurrency,
 bitCreateInvoice= @bitCreateInvoice,
 [numDefaultSalesBizDocId] =@numDefaultSalesBizDocId,
 bitSaveCreditCardInfo=@bitSaveCreditCardInfo,
 intLastViewedRecord=@intLastViewedRecord,
 [tintCommissionType]=@tintCommissionType,
 [tintBaseTax]=@tintBaseTax,
 [numDefaultPurchaseBizDocId]=@numDefaultPurchaseBizDocID,
 bitEmbeddedCost=@bitEmbeddedCost,
 numDefaultSalesOppBizDocId=@numDefaultSalesOppBizDocId,
 tintSessionTimeOut=@tintSessionTimeOut,
 tintDecimalPoints=@tintDecimalPoints,
 bitCustomizePortal=@bitCustomizePortal,
 tintBillToForPO = @tintBillToForPO,
 tintShipToForPO = @tintShipToForPO,
 tintOrganizationSearchCriteria=@tintOrganizationSearchCriteria,
-- vcPortalName=@vcPortalName,
 bitDocumentRepositary=@bitDocumentRepositary,
 --tintComAppliesTo=@tintComAppliesTo,THIS FIELD IS REMOVED FROM GLOBAL SETTINGS
 bitGtoBContact=@bitGtoBContact,
 bitBtoGContact=@bitBtoGContact,
 bitGtoBCalendar=@bitGtoBCalendar,
 bitBtoGCalendar=@bitBtoGCalendar,
 bitExpenseNonInventoryItem=@bitExpenseNonInventoryItem,
 bitInlineEdit=@bitInlineEdit,
 bitRemoveVendorPOValidation=@bitRemoveVendorPOValidation,
tintBaseTaxOnArea=@tintBaseTaxOnArea,
bitAmountPastDue = @bitAmountPastDue,
monAmountPastDue = @monAmountPastDue,
bitSearchOrderCustomerHistory=@bitSearchOrderCustomerHistory,
bitRentalItem=@bitRentalItem,
numRentalItemClass=@numRentalItemClass,
numRentalHourlyUOM=@numRentalHourlyUOM,
numRentalDailyUOM=@numRentalDailyUOM,
tintRentalPriceBasedOn=@tintRentalPriceBasedOn,
tintCalendarTimeFormat=@tintCalendarTimeFormat,
tintDefaultClassType=@tintDefaultClassType,
tintPriceBookDiscount=@tintPriceBookDiscount,
bitAutoSerialNoAssign=@bitAutoSerialNoAssign,
bitIsShowBalance = @bitIsShowBalance,
numIncomeAccID = @numIncomeAccID,
numCOGSAccID = @numCOGSAccID,
numAssetAccID = @numAssetAccID,
IsEnableProjectTracking = @IsEnableProjectTracking,
IsEnableCampaignTracking = @IsEnableCampaignTracking,
IsEnableResourceScheduling = @IsEnableResourceScheduling,
numShippingServiceItemID = @ShippingServiceItem,
numSOBizDocStatus=@numSOBizDocStatus,
bitAutolinkUnappliedPayment=@bitAutolinkUnappliedPayment,
numDiscountServiceItemID=@numDiscountServiceItemID,
vcShipToPhoneNo = @vcShipToPhoneNumber,
vcGAUserEMail = @vcGAUserEmailId,
vcGAUserPassword = @vcGAUserPassword,
vcGAUserProfileId = @vcGAUserProfileId,
bitDiscountOnUnitPrice=@bitDiscountOnUnitPrice,
intShippingImageWidth = @intShippingImageWidth ,
intShippingImageHeight = @intShippingImageHeight,
numTotalInsuredValue = @numTotalInsuredValue,
numTotalCustomsValue = @numTotalCustomsValue,
vcHideTabs = @vcHideTabs,
bitUseBizdocAmount = @bitUseBizdocAmount,
bitDefaultRateType = @bitDefaultRateType,
bitIncludeTaxAndShippingInCommission = @bitIncludeTaxAndShippingInCommission,
/*,bitAllowPPVariance=@bitAllowPPVariance*/
bitPurchaseTaxCredit=@bitPurchaseTaxCredit,
bitLandedCost=@bitLandedCost,
vcLanedCostDefault=@vcLanedCostDefault,
bitMinUnitPriceRule = @bitMinUnitPriceRule,
numDefaultSalesPricing = @numDefaultSalesPricing,
bitReOrderPoint=@bitReOrderPoint,
numReOrderPointOrderStatus=@numReOrderPointOrderStatus,
numPODropShipBizDoc=@numPODropShipBizDoc,
numPODropShipBizDocTemplate=@numPODropShipBizDocTemplate,
IsEnableDeferredIncome=@IsEnableDeferredIncome,
bitApprovalforTImeExpense=@bitApprovalforTImeExpense,
numCost= @numCost,intTimeExpApprovalProcess=@intTimeExpApprovalProcess,
bitApprovalforOpportunity=@bitApprovalforOpportunity,intOpportunityApprovalProcess=@intOpportunityApprovalProcess,
 numDefaultPurchaseShippingDoc=@numDefaultPurchaseShippingDoc,numDefaultSalesShippingDoc=@numDefaultSalesShippingDoc,
 bitchkOverRideAssignto=@bitchkOverRideAssignto,
 vcPrinterIPAddress=@vcPrinterIPAddress,
 vcPrinterPort=@vcPrinterPort
 where numDomainId=@numDomainID

COMMIT
END TRY
BEGIN CATCH
	DECLARE @ErrorMessage NVARCHAR(4000)
	DECLARE @ErrorNumber INT
	DECLARE @ErrorSeverity INT
	DECLARE @ErrorState INT
	DECLARE @ErrorLine INT
	DECLARE @ErrorProcedure NVARCHAR(200)

	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;

	SELECT 
		@ErrorMessage = ERROR_MESSAGE(),
		@ErrorNumber = ERROR_NUMBER(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE(),
		@ErrorLine = ERROR_LINE(),
		@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

	RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);
END CATCH

END
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_WarehouseItems_Save')
DROP PROCEDURE USP_WarehouseItems_Save
GO
CREATE PROCEDURE [dbo].[USP_WarehouseItems_Save]  
	@numDomainID AS NUMERIC(18,0),
	@numUserCntID AS NUMERIC(18,0),
	@numWareHouseItemID AS NUMERIC(18,0) OUTPUT,
	@numWareHouseID AS NUMERIC(18,0),
	@numWLocationID NUMERIC(18,0),
	@numItemCode NUMERIC(18,0),
	@vcLocation AS VARCHAR(250),
	@monWListPrice AS MONEY,
	@numOnHand AS FLOAT,
	@numReorder as FLOAT,
	@vcWHSKU AS VARCHAR(100),
	@vcBarCode AS VARCHAR(100),
	@strFieldList AS TEXT,
	@vcSerialLotNo AS TEXT,
	@bitCreateGlobalLocation BIT = 0
AS
BEGIN
BEGIN TRY
BEGIN TRANSACTION
	DECLARE @bitLotNo AS BIT = 0  
	DECLARE @bitSerialized AS BIT = 0
	DECLARE @bitMatrix AS BIT = 0
	DECLARE @numItemGroup NUMERIC(18,0)
	DECLARE @vcDescription AS VARCHAR(100)
	DECLARE @numGlobalWarehouseItemID NUMERIC(18,0) = 0
	DECLARE @monPrice AS MONEY
	DECLARE @vcSKU AS VARCHAR(200) = ''
	DECLARE @vcUPC AS VARCHAR(200) = ''

	SELECT 
		@bitLotNo=ISNULL(Item.bitLotNo,0),
		@bitSerialized=ISNULL(Item.bitSerialized,0),
		@numItemGroup = ISNULL(Item.numItemGroup,0),
		@monPrice=ISNULL(Item.monListPrice,0),
		@bitMatrix = ISNULL(bitMatrix,0),
		@vcSKU = ISNULL(vcSKU,''),
		@vcUPC = ISNULL(numBarCodeId,'')
	FROM 
		Item 
	WHERE 
		numItemCode=@numItemCode 
		AND numDomainID=@numDomainID

	IF ISNULL(@numWareHouseItemID,0) = 0 
	BEGIN --INSERT
		INSERT INTO WareHouseItems 
		(
			numItemID, 
			numWareHouseID,
			numWLocationID,
			numOnHand,
			numReorder,
			monWListPrice,
			vcLocation,
			vcWHSKU,
			vcBarCode,
			numDomainID,
			dtModified
		)  
		VALUES
		(
			@numItemCode,
			@numWareHouseID,
			@numWLocationID,
			(CASE WHEN @bitLotNo=1 OR @bitSerialized=1 THEN 0 ELSE @numOnHand END),
			@numReorder,
			@monPrice,
			@vcLocation,
			(CASE WHEN @numItemGroup > 0 AND ISNULL(@bitMatrix,0) = 1 THEN @vcSKU ELSE @vcWHSKU END),
			(CASE WHEN @numItemGroup > 0 AND ISNULL(@bitMatrix,0) = 1 THEN @vcUPC ELSE @vcBarCode END),
			@numDomainID,
			GETDATE()
		)  

		SELECT @numWareHouseItemID = SCOPE_IDENTITY()

		-- WAREHOUSE LOCATION LEVEL DIFFERENT ITEM PRICE IS AVAILABLE FOR MATRIX ITEM ONLY
		IF @numItemGroup > 0 AND ISNULL(@bitMatrix,0) = 0
		BEGIN
			UPDATE WareHouseItems SET monWListPrice = @monWListPrice WHERE numDomainID=@numDomainID AND numItemID = @numItemCode AND numWareHouseItemID = @numWareHouseItemID
		END

		IF (SELECT COUNT(*) FROM WareHouseItems WHERE numWareHouseID=@numWareHouseID AND numItemID=@numItemCode AND numWLocationID=-1) = 0
		BEGIN
			INSERT INTO WareHouseItems 
			(
				numItemID, 
				numWareHouseID,
				numWLocationID,
				numOnHand,
				numReorder,
				monWListPrice,
				vcLocation,
				vcWHSKU,
				vcBarCode,
				numDomainID,
				dtModified
			)  
			VALUES
			(
				@numItemCode,
				@numWareHouseID,
				-1,
				0,
				0,
				@monPrice,
				'',
				(CASE WHEN @numItemGroup > 0 AND ISNULL(@bitMatrix,0) = 1 THEN @vcSKU ELSE @vcWHSKU END),
				(CASE WHEN @numItemGroup > 0 AND ISNULL(@bitMatrix,0) = 1 THEN @vcUPC ELSE @vcBarCode END),
				@numDomainID,
				GETDATE()
			)  

			SELECT @numGlobalWarehouseItemID = SCOPE_IDENTITY()

			-- WAREHOUSE LOCATION LEVEL DIFFERENT ITEM PRICE IS AVAILABLE FOR MATRIX ITEM ONLY
			IF @numItemGroup > 0 AND ISNULL(@bitMatrix,0) = 0
			BEGIN
				UPDATE WareHouseItems SET monWListPrice = @monWListPrice, vcWHSKU=@vcSKU, vcBarCode=@vcUPC WHERE numDomainID=@numDomainID AND numItemID = @numItemCode AND numWareHouseItemID = @numGlobalWarehouseItemID
			END
		END

		SET @vcDescription='INSERT WareHouse'
	END
	ELSE 
	BEGIN --UPDATE
		UPDATE 
			WareHouseItems  
		SET 
			numReorder=@numReorder,
			vcLocation=@vcLocation,
			vcWHSKU=(CASE WHEN @numItemGroup > 0 AND ISNULL(@bitMatrix,0) = 1 THEN @vcSKU ELSE @vcWHSKU END),
			vcBarCode=(CASE WHEN @numItemGroup > 0 AND ISNULL(@bitMatrix,0) = 1 THEN @vcUPC ELSE @vcBarCode END),
			dtModified=GETDATE()   
		WHERE 
			numItemID=@numItemCode 
			AND numDomainID=@numDomainID 
			AND numWareHouseItemID=@numWareHouseItemID

		-- WAREHOUSE LOCATION LEVEL DIFFERENT ITEM PRICE IS AVAILABLE FOR MATRIX ITEM ONLY
		IF @numItemGroup > 0 AND ISNULL(@bitMatrix,0) = 0
		BEGIN
			UPDATE WareHouseItems SET monWListPrice = @monWListPrice WHERE numDomainID=@numDomainID AND numItemID = @numItemCode AND numWareHouseItemID = @numWareHouseItemID
		END

		SET @vcDescription='UPDATE WareHouse'
	END

	-- SAVE MATRIX ITEM ATTRIBUTES
	IF DATALENGTH(@strFieldList)>2
	BEGIN
		--INSERT CUSTOM FIELDS BASE ON WAREHOUSEITEMS
		DECLARE @hDoc AS INT     
		EXEC sp_xml_preparedocument @hDoc OUTPUT, @strFieldList                                                                        

		
	    DECLARE @TempTable TABLE 
		(
			ID INT IDENTITY(1,1),
			Fld_ID NUMERIC(18,0),
			Fld_Value VARCHAR(300)
		)   
	                                      
		INSERT INTO @TempTable 
		(
			Fld_ID,
			Fld_Value
		)    
		SELECT
			*          
		FROM 
			OPENXML (@hDoc,'/NewDataSet/CusFlds',2)                                                  
		WITH 
			(Fld_ID NUMERIC(18,0),Fld_Value VARCHAR(300))                                        
	                                                                           

		IF (SELECT COUNT(*) FROM @TempTable) > 0                                        
		BEGIN                           
			DELETE 
				CFW_Fld_Values_Serialized_Items 
			FROM 
				CFW_Fld_Values_Serialized_Items C                                        
			INNER JOIN 
				@TempTable T 
			ON 
				C.Fld_ID=T.Fld_ID 
			WHERE 
				C.RecId= @numWareHouseItemID  
	      
			INSERT INTO CFW_Fld_Values_Serialized_Items
			(
				Fld_ID,
				Fld_Value,
				RecId,
				bitSerialized
			)                                                  
			SELECT 
				Fld_ID,
				ISNULL(Fld_Value,'') AS Fld_Value,
				@numWareHouseItemID,
				0 
			FROM 
				@TempTable  


			IF ISNULL(@bitMatrix,0) = 1
			BEGIN
				DELETE 
					CFW_Fld_Values_Serialized_Items 
				FROM 
					CFW_Fld_Values_Serialized_Items C                                        
				INNER JOIN 
					@TempTable T 
				ON 
					C.Fld_ID=T.Fld_ID 
				WHERE 
					C.RecId= @numGlobalWarehouseItemID  
	      
				INSERT INTO CFW_Fld_Values_Serialized_Items
				(
					Fld_ID,
					Fld_Value,
					RecId,
					bitSerialized
				)                                                  
				SELECT 
					Fld_ID,
					ISNULL(Fld_Value,'') AS Fld_Value,
					@numGlobalWarehouseItemID,
					0 
				FROM 
					@TempTable
			END
		END  

		EXEC sp_xml_removedocument @hDoc 
	END	  

	-- SAVE SERIAL/LOT# 
	IF (@bitSerialized = 1 OR @bitLotNo = 1) AND DATALENGTH(@vcSerialLotNo) > 0
	BEGIN
		DECLARE @bitDuplicateLot BIT
		EXEC  @bitDuplicateLot = USP_WareHouseItmsDTL_CheckForDuplicate @numDomainID,@numWarehouseID,@numWLocationID,@numItemCode,@bitSerialized,@vcSerialLotNo

		IF @bitDuplicateLot = 1
		BEGIN
			RAISERROR('DUPLICATE_SERIALLOT',16,1)
		END

		DECLARE @TempSerialLot TABLE
		(
			vcSerialLot VARCHAR(100),
			numQty INT,
			dtExpirationDate DATETIME
		)

		DECLARE @idoc INT
		EXEC sp_xml_preparedocument @idoc OUTPUT, @vcSerialLotNo;

		INSERT INTO
			@TempSerialLot
		SELECT
			vcSerialLot,
			numQty,
			NULLIF(dtExpirationDate, '1900-01-01 00:00:00.000')
		FROM 
			OPENXML (@idoc, '/SerialLots/SerialLot',2)
		WITH 
			(
				vcSerialLot VARCHAR(100),
				numQty INT,
				dtExpirationDate DATETIME
			);

		INSERT INTO WareHouseItmsDTL
		(
			numWareHouseItemID,
			vcSerialNo,
			numQty,
			dExpirationDate,
			bitAddedFromPO
		)  
		SELECT
			@numWarehouseItemID,
			vcSerialLot,
			numQty,
			dtExpirationDate,
			0
		FROM
			@TempSerialLot
		
		DECLARE @numTotalQty AS INT
		SELECT @numTotalQty = SUM(numQty) FROM @TempSerialLot

		UPDATE WarehouseItems SET numOnHand = ISNULL(numOnHand,0) + @numTotalQty WHERE numWareHouseItemID=@numWareHouseItemID

        SET @vcDescription = CONCAT(@vcDescription + ' (Serial/Lot# Qty Added:', @numTotalQty ,')')
	END
 
	DECLARE @dtDATE AS DATETIME = GETUTCDATE()

	EXEC dbo.USP_ManageWareHouseItems_Tracking
		@numWareHouseItemID = @numWareHouseItemID,
		@numReferenceID = @numItemCode,
		@tintRefType = 1,
		@vcDescription = @vcDescription,
		@numModifiedBy = @numUserCntID,
		@ClientTimeZoneOffset = 0,
		@dtRecordDate = @dtDATE,
		@numDomainID = @numDomainID 

	IF ISNULL(@numGlobalWarehouseItemID,0) > 0
	BEGIN        
		EXEC dbo.USP_ManageWareHouseItems_Tracking
		@numWareHouseItemID = @numGlobalWarehouseItemID,
		@numReferenceID = @numItemCode,
		@tintRefType = 1,
		@vcDescription = @vcDescription,
		@numModifiedBy = @numUserCntID,
		@ClientTimeZoneOffset = 0,
		@dtRecordDate = @dtDATE,
		@numDomainID = @numDomainID 
	END

	UPDATE Item SET bintModifiedDate=@dtDATE,numModifiedBy=@numUserCntID WHERE numItemCode=@numItemCode AND numDomainID=@numDomainID
COMMIT
END TRY
BEGIN CATCH
	DECLARE @ErrorMessage NVARCHAR(4000)
	DECLARE @ErrorNumber INT
	DECLARE @ErrorSeverity INT
	DECLARE @ErrorState INT
	DECLARE @ErrorLine INT
	DECLARE @ErrorProcedure NVARCHAR(200)

	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;

	SET @numWareHouseItemID = 0

	SELECT 
		@ErrorMessage = ERROR_MESSAGE(),
		@ErrorNumber = ERROR_NUMBER(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE(),
		@ErrorLine = ERROR_LINE(),
		@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

	RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);
END CATCH
END

