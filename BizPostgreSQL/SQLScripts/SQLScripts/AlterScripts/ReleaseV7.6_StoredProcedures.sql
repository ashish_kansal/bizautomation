/******************************************************************
Project: Release 7.6 Date: 03.JUN.2017
Comments: STORE PROCEDURES
*******************************************************************/



GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='fn_GetCustFldOrganization')
DROP FUNCTION fn_GetCustFldOrganization
GO
CREATE FUNCTION [dbo].[fn_GetCustFldOrganization](@numFldId numeric(18),@pageId as tinyint,@numRecordId as numeric(9))  
 RETURNS varchar (100)   
AS  
BEGIN  
	DECLARE @vcValue as  varchar(300)  
	DECLARE @vcFldValue AS VARCHAR(MAX)
	DECLARE @fld_type AS VARCHAR(20)

	SELECT @fld_type = Fld_type FROM dbo.CFW_Fld_Master WHERE Fld_id = @numFldId

	SELECT
		@vcFldValue = Fld_Value
	FROM 
		CFW_FLD_Values 
	WHERE 
		Fld_ID=@numFldId AND 
		RecId=@numRecordId  

	IF @fld_type = 'SelectBox' AND ISNUMERIC(@vcFldValue) = 1
	BEGIN
		SET @vcValue = (SELECT vcData FROM dbo.ListDetails WHERE numListItemID = @vcFldValue)
	END
	ELSE IF @fld_type = 'CheckBoxList'
	BEGIN
		SELECT 
			@vcValue = STUFF((SELECT CONCAT(',', vcData) 
		FROM 
			ListDetails 
		WHERE 
			numlistitemid IN (SELECT ISNULL(Id,0) FROM dbo.SplitIDs(@vcFldValue,',')) FOR XML PATH('')), 1, 1, '')
	END
	ELSE IF @fld_type = 'CheckBox'
	BEGIN
		SET @vcValue = (CASE WHEN ISNULL(@vcFldValue,0)=1 THEN 'Yes' ELSE 'No' END)
	END
	ELSE
	BEGIN
		SET @vcValue = @vcFldValue
	END

	IF @vcValue IS NULL 
		SET @vcValue=''  

	RETURN @vcValue  
END
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='fn'AND NAME ='fn_GetCustFldStringValue')
DROP FUNCTION fn_GetCustFldStringValue
GO
CREATE FUNCTION [dbo].[fn_GetCustFldStringValue]
(
	@numFldId NUMERIC(18,0),
	@numRecordID NUMERIC(18,0),
	@vcFldValue VARCHAR(MAX)
)  
RETURNS VARCHAR(500)   
AS  
BEGIN  
	DECLARE @vcValue as  varchar(500)  

	DECLARE @fld_type AS VARCHAR(20)
	SELECT @fld_type = Fld_type FROM dbo.CFW_Fld_Master WHERE Fld_id = @numFldId

	IF @fld_type = 'SelectBox' AND ISNUMERIC(@vcFldValue) = 1
	BEGIN
		SET @vcValue = (SELECT vcData FROM dbo.ListDetails WHERE numListItemID = @vcFldValue)
	END
	ELSE IF @fld_type = 'CheckBoxList'
	BEGIN
		SELECT 
			@vcValue = STUFF((SELECT CONCAT(',', vcData) 
		FROM 
			ListDetails 
		WHERE 
			numlistitemid IN (SELECT ISNULL(Id,0) FROM dbo.SplitIDs(@vcFldValue,',')) FOR XML PATH('')), 1, 1, '')
	END
	ELSE IF @fld_type = 'CheckBox'
	BEGIN
		SET @vcValue = (CASE WHEN ISNULL(@vcFldValue,0)=1 THEN 'Yes' ELSE 'No' END)
	END
	ELSE
	BEGIN
		SET @vcValue = @vcFldValue
	END

	IF @vcValue IS NULL 
		SET @vcValue=''  

	RETURN ISNULL(@vcValue ,'')
END
GO
/****** Object:  StoredProcedure [dbo].[USP_AdvancedSearch]    Script Date: 05/07/2009 22:04:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj                              
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_advancedsearch')
DROP PROCEDURE usp_advancedsearch
GO
CREATE PROCEDURE [dbo].[USP_AdvancedSearch] 
@WhereCondition as varchar(4000)='',                              
@ViewID as tinyint,                              
@AreasofInt as varchar(100)='',                              
@tintUserRightType as tinyint,                              
@numDomainID as numeric(9)=0,                              
@numUserCntID as numeric(9)=0,                              
@numGroupID as numeric(9)=0,                              
@CurrentPage int,                                                                    
@PageSize int,                                                                    
@TotRecs int output,                                                                                                             
@columnSortOrder as Varchar(10),                              
@ColumnSearch as varchar(100)='',                           
@ColumnName as varchar(20)='',                             
@GetAll as bit,                              
@SortCharacter as char(1),                        
@SortColumnName as varchar(20)='',        
@bitMassUpdate as bit,        
@LookTable as varchar(200)='' ,        
@strMassUpdate as varchar(2000)=''                         
as                              
BEGIN
	DECLARE  @tintOrder  AS TINYINT
DECLARE  @vcFormFieldName  AS VARCHAR(50)
DECLARE  @vcListItemType  AS VARCHAR(3)
DECLARE  @vcListItemType1  AS VARCHAR(3)
DECLARE  @vcAssociatedControlType VARCHAR(20)
DECLARE  @numListID  AS NUMERIC(9)
DECLARE  @vcDbColumnName VARCHAR(20)
DECLARE  @vcLookBackTableName VARCHAR(50)
DECLARE  @WCondition VARCHAR(1000)
DECLARE @bitContactAddressJoinAdded AS BIT;
DECLARE @bitBillAddressJoinAdded AS BIT;
DECLARE @bitShipAddressJoinAdded AS BIT;

Declare @zipCodeSearch as varchar(100);SET @zipCodeSearch=''


if CHARINDEX('$SZ$', @WhereCondition)>0
BEGIN
select @zipCodeSearch=SUBSTRING(@WhereCondition, CHARINDEX('$SZ$', @WhereCondition)+4,CHARINDEX('$EZ$', @WhereCondition) - CHARINDEX('$SZ$', @WhereCondition)-4) 

CREATE TABLE #tempAddress(
	[numAddressID] [numeric](18, 0) NOT NULL,
	[vcAddressName] [varchar](50) NULL,
	[vcStreet] [varchar](100) NULL,
	[vcCity] [varchar](50) NULL,
	[vcPostalCode] [varchar](15) NULL,
	[numState] [numeric](18, 0) NULL,
	[numCountry] [numeric](18, 0) NULL,
	[bitIsPrimary] [bit] NOT NULL CONSTRAINT [DF_AddressMaster_bitIsPrimary]  DEFAULT ((0)),
	[tintAddressOf] [tinyint] NOT NULL,
	[tintAddressType] [tinyint] NOT NULL CONSTRAINT [DF_AddressMaster_tintAddressType]  DEFAULT ((0)),
	[numRecordID] [numeric](18, 0) NOT NULL,
	[numDomainID] [numeric](18, 0) NOT NULL)

IF Len(@zipCodeSearch)>0
BEGIN
	Declare @Strtemp as nvarchar(500);
	SET @Strtemp='insert into #tempAddress select * from AddressDetails  where numDomainID=@numDomainID1  
					and vcPostalCode in (SELECT REPLICATE(''0'', 5 - LEN(ZipCode)) + ZipCode  FROM ' + @zipCodeSearch +')';
	
	EXEC sp_executesql @Strtemp,N'@numDomainID1 numeric(18)',@numDomainID1=@numDomainID

set @WhereCondition=replace(@WhereCondition,'$SZ$' + @zipCodeSearch + '$EZ$',
' (ADC.numContactID in (select  numRecordID from #tempAddress where tintAddressOf=1 AND tintAddressType=0)   
or (DM.numDivisionID in (select  numRecordID from #tempAddress where tintAddressOf=2 AND tintAddressType in(1,2))))')
END
END

SET @WCondition = ''
   
IF (@SortCharacter <> '0' AND @SortCharacter <> '')
  SET @ColumnSearch = @SortCharacter
CREATE TABLE #temptable (
  id           INT   IDENTITY   PRIMARY KEY,
  numcontactid VARCHAR(15))


---------------------Find Duplicate Code Start-----------------------------
DECLARE  @Sql           VARCHAR(8000),
         @numMaxFieldId NUMERIC(9),
         @Where         NVARCHAR(1000),
         @OrderBy       NVARCHAR(1000),
         @GroupBy       NVARCHAR(1000),
         @SelctFields   NVARCHAR(4000),
         @InneJoinOn    NVARCHAR(1000)
SET @InneJoinOn = ''
SET @OrderBy = ''
SET @Sql = ''
SET @Where = ''
SET @GroupBy = ''
SET @SelctFields = ''
    
IF (@WhereCondition = 'DUP' OR @WhereCondition = 'DUP-PRIMARY')
BEGIN
	SELECT @numMaxFieldId= MAX([numFieldID]) FROM View_DynamicColumns
	WHERE [numFormID] =24 AND [numDomainId] = @numDomainID
	--PRINT @numMaxFieldId
	WHILE @numMaxFieldId > 0
	BEGIN
		SELECT @vcFormFieldName=[vcFieldName],@vcDbColumnName=[vcDbColumnName],@vcLookBackTableName=[vcLookBackTableName] 
		FROM View_DynamicColumns 
		WHERE [numFieldID] = @numMaxFieldId and [numFormID] =24 AND [numDomainId] = @numDomainID
		--	PRINT @vcDbColumnName
			SET @SelctFields = @SelctFields +  @vcDbColumnName + ','
			IF(@vcLookBackTableName ='AdditionalContactsInformation')
			BEGIN
				SET @InneJoinOn = @InneJoinOn + 'ADC.'+ @vcDbColumnName +' = a1.' + @vcDbColumnName + ' and '
				SET @GroupBy = @GroupBy + 'ADC.'+  @vcDbColumnName + ','
			END
			ELSE IF (@vcLookBackTableName ='DivisionMaster')
			BEGIN
				SET @InneJoinOn = @InneJoinOn + 'DM.'+ @vcDbColumnName +' = a1.' + @vcDbColumnName + ' and '
				SET @GroupBy = @GroupBy + 'DM.'+  @vcDbColumnName + ','
			END
			ELSE
			BEGIN
				SET @InneJoinOn = @InneJoinOn + 'C.' + @vcDbColumnName +' = a1.' + @vcDbColumnName + ' and '
				SET @GroupBy = @GroupBy + 'C.' + @vcDbColumnName + ','
			END
			SET @Where = @Where + 'and LEN(' + @vcDbColumnName + ')>0 '

	
		SELECT @numMaxFieldId = MAX([numFieldID]) FROM View_DynamicColumns 
		WHERE [numFormID] =24 AND [numDomainId] = @numDomainID AND [numFieldID] < @numMaxFieldId
	END
			
			IF(LEN(@Where)>2)
			BEGIN
				SET @Where =RIGHT(@Where,LEN(@Where)-3)
				SET @OrderBy =  LEFT(@GroupBy,(Len(@GroupBy)- 1))
				SET @GroupBy =  LEFT(@GroupBy,(Len(@GroupBy)- 1))
				SET @InneJoinOn = LEFT(@InneJoinOn,LEN(@InneJoinOn)-3)
				SET @SelctFields = LEFT(@SelctFields,(Len(@SelctFields)- 1))
				SET @Sql = @Sql + ' INSERT INTO [#tempTable] ([numContactID])SELECT  ADC.numContactId FROM additionalcontactsinformation ADC JOIN DivisionMaster DM ON ADC.numDivisionId = DM.numDivisionID JOIN CompanyInfo C ON DM.numCompanyID = C.numCompanyId  
					JOIN (SELECT   '+ @SelctFields + '
				   FROM  additionalcontactsinformation ADC JOIN DivisionMaster DM ON ADC.numDivisionId = DM.numDivisionID JOIN CompanyInfo C ON DM.numCompanyID = C.numCompanyId  
				   WHERE  ' + @Where + ' AND C.[numDomainID] = '+convert(varchar(15),@numDomainID)+ ' AND ADC.[numDomainID] = '+convert(varchar(15),@numDomainID)+ ' AND DM.[numDomainID] = '+convert(varchar(15),@numDomainID)+ 
				   ' GROUP BY ' + @GroupBy + '
				   HAVING   COUNT(* ) > 1) a1
					ON  '+ @InneJoinOn + ' where ADC.[numDomainID] = '+convert(varchar(15),@numDomainID) + ' and DM.[numDomainID] = '+convert(varchar(15),@numDomainID) + ' ORDER BY '+ @OrderBy
			END	
			ELSE
			BEGIN
				SET @Where = ' 1=0 '
				SET @Sql = @Sql + ' INSERT INTO [#tempTable] ([numContactID])SELECT  ADC.numContactId FROM additionalcontactsinformation ADC  '
							+ ' Where ' + @Where
			END 	
		   PRINT @Sql
		   EXEC( @Sql)
		   SET @Sql = ''
		   SET @GroupBy = ''
		   SET @SelctFields = ''
		   SET @Where = ''
	
	IF (@WhereCondition = 'DUP-PRIMARY')
      BEGIN
        SET @WCondition = ' AND ADC.numContactId IN (SELECT DISTINCT numContactId FROM #tempTable) and ISNULL(ADC.bitPrimaryContact,0)=1'
      END
    ELSE
      BEGIN
        SET @WCondition = ' AND ADC.numContactId IN (SELECT DISTINCT numContactId FROM #tempTable)'
      END
    
	SET @Sql = ''
	SET @WhereCondition =''
	SET @Where= 'DUP' 
END
-----------------------------End Find Duplicate ------------------------

declare @strSql as varchar(8000)                                                              
 set  @strSql='Select ADC.numContactId                    
  FROM AdditionalContactsInformation ADC                                                   
  JOIN DivisionMaster DM ON ADC.numDivisionId = DM.numDivisionID                                                  
  JOIN CompanyInfo C ON DM.numCompanyID = C.numCompanyId'   

IF @SortColumnName='vcStreet' OR @SortColumnName='vcCity' OR @SortColumnName='vcPostalCode' OR @SortColumnName ='numCountry' OR @SortColumnName ='numState'
   OR @ColumnName='vcStreet' OR @ColumnName='vcCity' OR @ColumnName='vcPostalCode' OR @ColumnName ='numCountry' OR @ColumnName ='numState'	
BEGIN 
	set @strSql= @strSql +' left Join AddressDetails AD on AD.numRecordId= ADC.numContactID and AD.tintAddressOf=1 and AD.tintAddressType=0 AND AD.bitIsPrimary=1 and AD.numDomainID= ADC.numDomainID'	
END 
ELSE IF @SortColumnName='vcBillStreet' OR @SortColumnName='vcBillCity' OR @SortColumnName='vcBillPostCode' OR @SortColumnName ='numBillCountry' OR @SortColumnName ='numBillState'
     OR @ColumnName='vcBillStreet' OR @ColumnName='vcBillCity' OR @ColumnName='vcBillPostCode' OR @ColumnName ='numBillCountry' OR @ColumnName ='numBillState'
BEGIN 	
	set @strSql= @strSql +' left Join AddressDetails AD1 on AD1.numRecordId= DM.numDivisionID and AD1.tintAddressOf=2 and AD1.tintAddressType=1 AND AD1.bitIsPrimary=1 and AD1.numDomainID= ADC.numDomainID '
END	
ELSE IF @SortColumnName='vcShipStreet' OR @SortColumnName='vcShipCity' OR @SortColumnName='vcShipPostCode' OR @SortColumnName ='numShipCountry' OR @SortColumnName ='numShipState'
     OR @ColumnName='vcShipStreet' OR @ColumnName='vcShipCity' OR @ColumnName='vcShipPostCode' OR @ColumnName ='numShipCountry' OR @ColumnName ='numShipState'
BEGIN
	set @strSql= @strSql +' left Join AddressDetails AD2 on AD2.numRecordId= DM.numDivisionID and AD2.tintAddressOf=2 and AD2.tintAddressType=2 AND AD2.bitIsPrimary=1 and AD2.numDomainID= ADC.numDomainID '
END	
                         
  if (@ColumnName<>'' and  @ColumnSearch<>'')                        
  begin                          
 select @vcListItemType=vcListItemType,@numListID=numListID from View_DynamicDefaultColumns where vcDbColumnName=@ColumnName and numFormID=1 and numDomainId=@numDomainId                         
    if @vcListItemType='LI'                               
    begin                      
		IF @numListID=40--Country
		  BEGIN
			IF @ColumnName ='numCountry'
			BEGIN
					set @strSql= @strSql +' left Join ListDetails L1 on L1.numListItemID=AD.numCountry' 
			END
			ELSE IF @ColumnName ='numBillCountry'
			BEGIN
					set @strSql= @strSql +' left Join ListDetails L1 on L1.numListItemID=AD1.numCountry' 
			END
			ELSE IF @ColumnName ='numShipCountry'
			BEGIN
					set @strSql= @strSql +' left Join ListDetails L1 on L1.numListItemID=AD2.numCountry' 
			END
		  END                        
		  ELSE
		  BEGIN
				set @strSql= @strSql +' left Join ListDetails L1 on L1.numListItemID='+@ColumnName                              
		  END
		            
    end                              
    else if @vcListItemType='S'                               
    begin           
       IF @ColumnName ='numState'
		BEGIN
   			set @strSql= @strSql +' left join State S1 on S1.numStateID=AD.numState'                            
		END
		ELSE IF @ColumnName ='numBillState'
		BEGIN
   			set @strSql= @strSql +' left join State S1 on S1.numStateID=AD1.numState'                            
		END
	    ELSE IF @ColumnName ='numShipState'
	    BEGIN
   			set @strSql= @strSql +' left join State S1 on S1.numStateID=AD2.numState'                           
		END
    end                              
    else if @vcListItemType='T'                               
    begin                              
      set @strSql= @strSql +' left Join ListDetails L1 on L1.numListItemID='+@ColumnName                             
    end                           
   end       
           
                      
   if (@SortColumnName<>'')                        
  begin                          
 select @vcListItemType1=vcListItemType,@numListID=numListID from View_DynamicDefaultColumns where vcDbColumnName=@SortColumnName and numFormID=1 and numDomainId=@numDomainId                         
    if @vcListItemType1='LI'                   
    begin     
    IF @numListID=40--Country
		  BEGIN
			IF @SortColumnName ='numCountry'
			BEGIN
					set @strSql= @strSql +' left Join ListDetails L2 on L2.numListItemID=AD.numCountry' 
			END
			ELSE IF @SortColumnName ='numBillCountry'
			BEGIN
					set @strSql= @strSql +' left Join ListDetails L2 on L2.numListItemID=AD1.numCountry' 
			END
			ELSE IF @SortColumnName ='numShipCountry'
			BEGIN
					set @strSql= @strSql +' left Join ListDetails L2 on L2.numListItemID=AD2.numCountry' 
			END
		  END                        
		  ELSE
		  BEGIN
				set @strSql= @strSql +' left Join ListDetails L2 on L2.numListItemID='+@SortColumnName                              
		  END
    end                              
    else if @vcListItemType1='S'           
    begin                
        IF @SortColumnName ='numState'
		BEGIN
   			set @strSql= @strSql +' left join State S2 on S2.numStateID=AD.numState'                            
		END
		ELSE IF @SortColumnName ='numBillState'
		BEGIN
   			set @strSql= @strSql +' left join State S2 on S2.numStateID=AD1.numState'                            
		END
	    ELSE IF @SortColumnName ='numShipState'
	    BEGIN
   			set @strSql= @strSql +' left join State S2 on S2.numStateID=AD2.numState'                           
		END
    end                              
    else if @vcListItemType1='T'                  
    begin                              
      set @strSql= @strSql +' left Join ListDetails L2 on L2.numListItemID='+@SortColumnName                               
    end                           
  end                          
  set @strSql=@strSql+' where DM.numDomainID  = '+convert(varchar(15),@numDomainID)+   @WhereCondition                                  
                 
--if @tintUserRightType=1 set @strSql=@strSql + ' AND (ADC.numRecOwner = '+convert(varchar(15),@numUserCntID)+ ')'                                       
--else if @tintUserRightType=2 set @strSql=@strSql + ' AND (DM.numTerID in (select numTerritoryID from  UserTerritory where numUserCntID= '+convert(varchar(15),@numUserCntID)+' ) or DM.numTerID=0) '
                        
                          
                            
                                            
if    @AreasofInt<>'' set  @strSql=@strSql+ ' and ADC.numContactID in (select numContactID from  AOIContactLink where numAOIID in('+ @AreasofInt+'))'                                 
if (@ColumnName<>'' and  @ColumnSearch<>'')                         
begin                          
  if @vcListItemType='LI'                               
    begin                                
      set @strSql= @strSql +' and L1.vcData like '''+@ColumnSearch +'%'''                             
    end                              
    else if @vcListItemType='S'                               
    begin                                  
      set @strSql= @strSql +' and S1.vcState like '''+@ColumnSearch+'%'''                          
    end                              
    else if @vcListItemType='T'                               
    begin                              
      set @strSql= @strSql +' and T1.vcTerName like '''+@ColumnSearch  +'%'''                            
    end   
    else  
       BEGIN     
		   IF @ColumnName='vcStreet' OR @ColumnName='vcBillStreet' OR @ColumnName='vcShipStreet'
				set @strSql= @strSql +' and vcStreet like '''+@ColumnSearch  +'%'''                            
		   ELSE IF @ColumnName='vcCity' OR @ColumnName='vcBillCity' OR @ColumnName='vcShipCity'
				set @strSql= @strSql +' and vcCity like '''+@ColumnSearch  +'%'''                            
		   ELSE IF @ColumnName='vcPostalCode' OR @ColumnName='vcBillPostCode' OR @ColumnName='vcShipPostCode'
				set @strSql= @strSql +' and vcPostalCode like '''+@ColumnSearch  +'%'''                            
		   else		                      
				set @strSql= @strSql +' and '+@ColumnName+' like '''+@ColumnSearch  +'%'''                            
        END   
                          
end                          
   if (@SortColumnName<>'')                        
  begin                           
    if @vcListItemType1='LI'                               
		begin                                
		  set @strSql= @strSql +' order by L2.vcData '+@columnSortOrder                              
		end                              
    else if @vcListItemType1='S'                               
		begin                                  
		  set @strSql= @strSql +' order by S2.vcState '+@columnSortOrder                              
		end                              
    else if @vcListItemType1='T'                               
		begin                              
		  set @strSql= @strSql +' order by T2.vcTerName '+@columnSortOrder                              
		end  
    else  
       BEGIN     
		   IF @SortColumnName='vcStreet' OR @SortColumnName='vcBillStreet' OR @SortColumnName='vcShipStreet'
				set @strSql= @strSql +' order by vcStreet ' +@columnSortOrder                            
		   ELSE IF @SortColumnName='vcCity' OR @SortColumnName='vcBillCity' OR @SortColumnName='vcShipCity'
				set @strSql= @strSql +' order by vcCity ' +@columnSortOrder                            
		   ELSE IF @SortColumnName='vcPostalCode' OR @SortColumnName='vcBillPostCode' OR @SortColumnName='vcShipPostCode'
				set @strSql= @strSql +' order by vcPostalCode ' +@columnSortOrder
		   else if @SortColumnName='numRecOwner'
				set @strSql= @strSql + ' order by dbo.fn_GetContactName(ADC.numRecOwner) ' +@columnSortOrder	                            
		   else		                      
				set @strSql= @strSql +' order by '+ @SortColumnName +' ' +@columnSortOrder                            
        END  
                                
  END
  ELSE 
	  set @strSql= @strSql +' order by  ADC.numContactID asc '
IF(@Where <>'DUP')
BEGIN
    PRINT @strSql

	INSERT INTO #temptable (numcontactid)
	EXEC( @strSql)
	
	SET @strSql = ''
END

SET @bitBillAddressJoinAdded=0;
SET @bitShipAddressJoinAdded=0;
SET @bitContactAddressJoinAdded=0;
set @tintOrder=0;
set @WhereCondition ='';
set @strSql='select ADC.numContactId,DM.numDivisionID,DM.numTerID,ADC.numRecOwner,DM.tintCRMType '                              
DECLARE @Prefix AS VARCHAR(5)
DECLARE @bitCustom BIT
DECLARE @numFieldGroupID AS INT
DECLARE @vcColumnName AS VARCHAR(200)
DECLARE @numFieldID AS NUMERIC(18,0)

	SELECT TOP 1
		@tintOrder=tintOrder,
		@numFieldID=numFormFieldID,
		@vcDbColumnName=vcDbColumnName,
		@vcFormFieldName=vcFieldName,
		@vcAssociatedControlType=vcAssociatedControlType,
		@vcListItemType=vcListItemType,
		@numListID=numListID,
		@vcLookBackTableName=vcLookBackTableName,
		@bitCustom=bitCustom,
		@numFieldGroupID=numGroupID
	FROM
	(
		SELECT
			A.tintOrder+1 tintOrder,
			numFormFieldID,
			vcDbColumnName,
			vcFieldName,
			vcAssociatedControlType,
			vcListItemType,
			numListID,
			vcLookBackTableName,
			0 AS bitCustom,
			0 AS numGroupID
		FROM 
			AdvSerViewConf A                              
		JOIN 
			View_DynamicDefaultColumns D                               
		ON 
			D.numFieldId=A.numFormFieldId  
			AND d.numformId = 1                           
		WHERE 
			D.numDomainID = @numDomainID 
			AND D.numFormID = 1 
			AND A.numFormID = 1 
			AND tintViewID=@ViewID 
			AND numGroupID=@numGroupID 
			AND A.numDomainID=@numDomainID 
			AND ISNULL(A.bitCustom,0) = 0
			AND d.vcDbColumnName not in ('numBillShipCountry','numBillShipState')
		UNION
		SELECT
			A.tintOrder+1 tintOrder,
			numFormFieldID,
			CONCAT('Cust',numFormFieldID),
			Fld_label,
			c.Fld_type,
			'',
			c.numlistid,
			'',
			1 AS bitCustom,
			Grp_id
		FROM    
			AdvSerViewConf A
		JOIN 
			CFW_Fld_Master C ON C.Fld_id = A.numFormFieldID
		WHERE   
			A.numFormID = 1
			AND A.tintViewID = @ViewID
			AND C.numDomainID = @numDomainID AND A.numFormID = 1
			AND A.numGroupID = @numGroupID
			AND A.numDomainID = @numDomainID
			AND ISNULL(A.bitCustom,0) = 1
	) T1
	ORDER BY 
		tintOrder asc                              

while @tintOrder>0                              
begin                              
     
	
    IF @vcLookBackTableName = 'AdditionalContactsInformation' 
        SET @Prefix = 'ADC.'
    IF @vcLookBackTableName = 'DivisionMaster' 
        SET @Prefix = 'DM.'
     
	IF @bitCustom = 0 
	BEGIN                           
		if @vcAssociatedControlType='SelectBox'                              
		begin                              
                                              
			  if @vcListItemType='LI'                               
			  begin    
				  IF @numListID=40--Country
				  BEGIN
	  				set @strSql=@strSql+',L'+ convert(varchar(3),@tintOrder)+'.vcData'+' ['+ @vcFormFieldName+'~'+ @vcDbColumnName+']'                              
					IF @vcDbColumnName ='numCountry'
					BEGIN
						IF @bitContactAddressJoinAdded=0 
					  BEGIN
							set @WhereCondition= @WhereCondition +' left Join AddressDetails AD on AD.numRecordId= ADC.numContactID and AD.tintAddressOf=1 and AD.tintAddressType=0 AND AD.bitIsPrimary=1 and AD.numDomainID= ADC.numDomainID'	
							SET @bitContactAddressJoinAdded=1;
					  END
					  set @WhereCondition= @WhereCondition +' left Join ListDetails L'+ convert(varchar(3),@tintOrder)+ ' on L'+convert(varchar(3),@tintOrder)+ '.numListItemID=AD.numCountry'	
					END
					ELSE IF @vcDbColumnName ='numBillCountry'
					BEGIN
						IF @bitBillAddressJoinAdded=0 
						BEGIN
							set @WhereCondition= @WhereCondition +' left Join AddressDetails AD1 on AD1.numRecordId= DM.numDivisionID and AD1.tintAddressOf=2 and AD1.tintAddressType=1 AND AD1.bitIsPrimary=1 and AD1.numDomainID= ADC.numDomainID '
					
							SET @bitBillAddressJoinAdded=1;
						END
						set @WhereCondition= @WhereCondition +' left Join ListDetails L'+ convert(varchar(3),@tintOrder)+ ' on L'+convert(varchar(3),@tintOrder)+ '.numListItemID=AD1.numCountry'
					END
					ELSE IF @vcDbColumnName ='numShipCountry'
					BEGIN
						IF @bitShipAddressJoinAdded=0 
						BEGIN
							set @WhereCondition= @WhereCondition +' left Join AddressDetails AD2 on AD2.numRecordId= DM.numDivisionID and AD2.tintAddressOf=2 and AD2.tintAddressType=2 AND AD2.bitIsPrimary=1 and AD2.numDomainID= ADC.numDomainID '
					
							SET @bitShipAddressJoinAdded=1;
						END
						set @WhereCondition= @WhereCondition +' left Join ListDetails L'+ convert(varchar(3),@tintOrder)+ ' on L'+convert(varchar(3),@tintOrder)+ '.numListItemID=AD2.numCountry'
					END
				  END                        
				  ELSE
				  BEGIN
						set @strSql=@strSql+',L'+ convert(varchar(3),@tintOrder)+'.vcData'+' ['+ @vcFormFieldName+'~'+ @vcDbColumnName+']'                              
						set @WhereCondition= @WhereCondition +' left Join ListDetails L'+ convert(varchar(3),@tintOrder)+ ' on L'+convert(varchar(3),@tintOrder)+ '.numListItemID='+@vcDbColumnName	
				  END
			  end                              
	                      
			  else if @vcListItemType='T'                               
			  begin                              
				  set @strSql=@strSql+',L'+ convert(varchar(3),@tintOrder)+'.vcData'+' ['+ @vcFormFieldName+'~'+ @vcDbColumnName+']'                              
			   set @WhereCondition= @WhereCondition +' left Join ListDetails L'+ convert(varchar(3),@tintOrder)+ ' on L'+convert(varchar(3),@tintOrder)+ '.numListItemID='+@vcDbColumnName                              
			  end  
			  else if   @vcListItemType='U'                                                     
				begin           
					set @strSql=@strSql+',dbo.fn_GetContactName('+ @Prefix + @vcDbColumnName+') ['+ @vcFormFieldName+'~'+ @vcDbColumnName+']'
				end   
	
		end  
		ELSE IF @vcAssociatedControlType='ListBox'   
		BEGIN
			if @vcListItemType='S'                               
			  begin                         
				set @strSql=@strSql+',S'+ convert(varchar(3),@tintOrder)+'.vcState'+' ['+ @vcFormFieldName+'~'+ @vcDbColumnName+']'
				IF @vcDbColumnName ='numState'
				BEGIN
					IF @bitContactAddressJoinAdded=0 
					   BEGIN
							set @WhereCondition= @WhereCondition +' left Join AddressDetails AD on AD.numRecordId= ADC.numContactID and AD.tintAddressOf=1 and AD.tintAddressType=0 AND AD.bitIsPrimary=1 and AD.numDomainID= ADC.numDomainID'	
							SET @bitContactAddressJoinAdded=1;
					  END	
					  set @WhereCondition= @WhereCondition +' left join State S'+ convert(varchar(3),@tintOrder)+ ' on S'+convert(varchar(3),@tintOrder)+ '.numStateID=AD.numState'
				END
				ELSE IF @vcDbColumnName ='numBillState'
				BEGIN
					IF @bitBillAddressJoinAdded=0 
					BEGIN
						set @WhereCondition= @WhereCondition +' left Join AddressDetails AD1 on AD1.numRecordId= DM.numDivisionID and AD1.tintAddressOf=2 and AD1.tintAddressType=1 AND AD1.bitIsPrimary=1 and AD1.numDomainID= ADC.numDomainID '
						SET @bitBillAddressJoinAdded=1;
					END
					set @WhereCondition= @WhereCondition +' left join State S'+ convert(varchar(3),@tintOrder)+ ' on S'+convert(varchar(3),@tintOrder)+ '.numStateID=AD1.numState'
				END
				ELSE IF @vcDbColumnName ='numShipState'
				BEGIN
					IF @bitShipAddressJoinAdded=0 
					BEGIN
						set @WhereCondition= @WhereCondition +' left Join AddressDetails AD2 on AD2.numRecordId= DM.numDivisionID and AD2.tintAddressOf=2 and AD2.tintAddressType=2 AND AD2.bitIsPrimary=1 and AD2.numDomainID= ADC.numDomainID '
						SET @bitShipAddressJoinAdded=1;
					END
					set @WhereCondition= @WhereCondition +' left join State S'+ convert(varchar(3),@tintOrder)+ ' on S'+convert(varchar(3),@tintOrder)+ '.numStateID=AD2.numState'
				END
			  end         
		END 
		ELSE IF @vcAssociatedControlType='TextBox'   
		BEGIN
			IF @vcDbColumnName='vcStreet' OR @vcDbColumnName='vcCity' OR @vcDbColumnName='vcPostalCode'
			begin                              
			   set @strSql=@strSql+',AD.'+ @vcDbColumnName+' ['+ @vcFormFieldName+'~'+ @vcDbColumnName+']'                              
			   IF @bitContactAddressJoinAdded=0 
			   BEGIN
					set @WhereCondition= @WhereCondition +' left Join AddressDetails AD on AD.numRecordId= ADC.numContactID and AD.tintAddressOf=1 and AD.tintAddressType=0 AND AD.bitIsPrimary=1 and AD.numDomainID= ADC.numDomainID'	
					SET @bitContactAddressJoinAdded=1;
			   END
			end 
			ELSE IF @vcDbColumnName='vcBillStreet' OR @vcDbColumnName='vcBillCity' OR @vcDbColumnName='vcBillPostCode'
			begin                              
			   set @strSql=@strSql+',AD1.'+ REPLACE(REPLACE(@vcDbColumnName,'Bill',''),'PostCode','PostalCode') +' ['+ @vcFormFieldName+'~'+ @vcDbColumnName+']'                              
			   IF @bitBillAddressJoinAdded=0 
			   BEGIN 
	   				set @WhereCondition= @WhereCondition +' left Join AddressDetails AD1 on AD1.numRecordId= DM.numDivisionID and AD1.tintAddressOf=2 and AD1.tintAddressType=1 AND AD1.bitIsPrimary=1 and AD1.numDomainID= ADC.numDomainID '
	   				SET @bitBillAddressJoinAdded=1;
			   END
			end 
			ELSE IF @vcDbColumnName='vcShipStreet' OR @vcDbColumnName='vcShipCity' OR @vcDbColumnName='vcShipPostCode'
			begin                              
			   set @strSql=@strSql+',AD2.'+ REPLACE(REPLACE(@vcDbColumnName,'Ship',''),'PostCode','PostalCode') +' ['+ @vcFormFieldName+'~'+ @vcDbColumnName+']'                              
			   IF @bitShipAddressJoinAdded=0 
			   BEGIN
	   				set @WhereCondition= @WhereCondition +' left Join AddressDetails AD2 on AD2.numRecordId= DM.numDivisionID and AD2.tintAddressOf=2 and AD2.tintAddressType=2  AND AD2.bitIsPrimary=1 and AD2.numDomainID= ADC.numDomainID '
	   				SET @bitShipAddressJoinAdded=1;
			   END
			end 
			ELSE
			BEGIN
				set @strSql=@strSql+','+ case  when @vcDbColumnName='numAge' then 'year(getutcdate())-year(bintDOB)' when @vcDbColumnName='numDivisionID' then 'DM.numDivisionID' when @vcDbColumnName='bintCreatedDate' then 'dbo.[FormatedDateFromDate](DM.bintCreatedDate,DM.numDomainID)' else @vcDbColumnName end+' ['+ @vcFormFieldName+'~'+ @vcDbColumnName+']'		
			END
		END                       
		ELSE 
			SET @strSql=@strSql+','+ case  when @vcDbColumnName='numAge' then 'year(getutcdate())-year(bintDOB)' when @vcDbColumnName='numDivisionID' then 'DM.numDivisionID' when @vcDbColumnName='bintCreatedDate' then 'dbo.[FormatedDateFromDate](DM.bintCreatedDate,DM.numDomainID)' else @vcDbColumnName end+' ['+ @vcFormFieldName+'~'+ @vcDbColumnName+']'
	END
	ELSE IF @bitCustom = 1                
	BEGIN
		SET @vcColumnName= CONCAT(@vcFormFieldName,'~','Cust',@numFieldId)
			
		IF @vcAssociatedControlType = 'TextBox' or @vcAssociatedControlType = 'TextArea'            
		BEGIN
			SET @strSql= @strSql+',CFW'+ convert(VARCHAR(3),@tintOrder)+'.Fld_Value  ['+ @vcColumnName +']'                   
				
			SET @WhereCondition= @WhereCondition 
								+' left Join ' + (CASE WHEN @numFieldGroupID = 1 THEN 'CFW_FLD_Values' WHEN @numFieldGroupID = 4 THEN 'CFW_FLD_Values_Cont' ELSE 'CFW_FLD_Values' END) + ' CFW'+ convert(varchar(3),@tintOrder)
								+ ' on CFW' + convert(varchar(3),@tintOrder) 
								+ '.Fld_Id='+convert(varchar(10),@numFieldId) 
								+ 'and CFW' + convert(varchar(3),@tintOrder) 
								+ '.RecId= (CASE WHEN ' + CAST(@numFieldGroupID AS VARCHAR) + ' = 1 THEN DM.numDivisionId WHEN ' + CAST(@numFieldGroupID AS VARCHAR) + ' = 4 THEN ADC.numContactID ELSE 0 END) '                                                         
		END   
		ELSE IF @vcAssociatedControlType = 'CheckBox'       
		BEGIN
			set @strSql= @strSql+',case when isnull(CFW' + CONVERT(VARCHAR(3), @tintOrder)+ '.Fld_Value,0)='''' then 0 else  isnull(CFW'
											+ CONVERT(VARCHAR(3), @tintOrder)+ '.Fld_Value,0) end ['+ @vcColumnName + ']'
 
			set @WhereCondition= @WhereCondition +' left Join ' + (CASE WHEN @numFieldGroupID = 1 THEN 'CFW_FLD_Values' WHEN @numFieldGroupID = 4 THEN 'CFW_FLD_Values_Cont' ELSE 'CFW_FLD_Values' END) + ' CFW'+ convert(varchar(3),@tintOrder)+ '             
			on CFW'+ convert(varchar(3),@tintOrder)+ '.Fld_Id='+convert(varchar(10),@numFieldId) +'and CFW'+ convert(varchar(3),@tintOrder)+ '.RecId= (CASE WHEN ' + CAST(@numFieldGroupID AS VARCHAR) + ' = 1 THEN DM.numDivisionId WHEN ' + CAST(@numFieldGroupID AS VARCHAR) + ' = 4 THEN ADC.numContactID ELSE 0 END) '                                                     
		END                
		ELSE IF @vcAssociatedControlType = 'DateField'            
		BEGIN
			set @strSql= @strSql+',dbo.FormatedDateFromDate(CFW'+ convert(varchar(3),@tintOrder)+'.Fld_Value,'+convert(varchar(10),@numDomainId)+')  ['+ @vcColumnName +']'                   
			set @WhereCondition= @WhereCondition +' left Join ' + (CASE WHEN @numFieldGroupID = 1 THEN 'CFW_FLD_Values' WHEN @numFieldGroupID = 4 THEN 'CFW_FLD_Values_Cont' ELSE 'CFW_FLD_Values' END) + ' CFW'+ convert(varchar(3),@tintOrder)+ '                 
			on CFW'+ convert(varchar(3),@tintOrder)+ '.Fld_Id='+convert(varchar(10),@numFieldId) +'and CFW'+ convert(varchar(3),@tintOrder)+ '.RecId= (CASE WHEN ' + CAST(@numFieldGroupID AS VARCHAR) + ' = 1 THEN DM.numDivisionId WHEN ' + CAST(@numFieldGroupID AS VARCHAR) + ' = 4 THEN ADC.numContactID ELSE 0 END) '                                                         
		END                
		ELSE IF @vcAssociatedControlType = 'SelectBox'             
		BEGIN
			set @vcDbColumnName = 'DCust'+Convert(varchar(10),@numFieldId)                
			set @strSql=@strSql+',L'+ convert(varchar(3),@tintOrder)+'.vcData'+' ['+ @vcColumnName +']'                                                          
			set @WhereCondition= @WhereCondition +' left Join ' + (CASE WHEN @numFieldGroupID = 1 THEN 'CFW_FLD_Values' WHEN @numFieldGroupID = 4 THEN 'CFW_FLD_Values_Cont' ELSE 'CFW_FLD_Values' END) + ' CFW'+ convert(varchar(3),@tintOrder)+ '                 
				on CFW'+ convert(varchar(3),@tintOrder)+ '.Fld_Id='+convert(varchar(10),@numFieldId) +'and CFW'+ convert(varchar(3),@tintOrder)+ '.RecId= (CASE WHEN ' + CAST(@numFieldGroupID AS VARCHAR) + ' = 1 THEN DM.numDivisionId WHEN ' + CAST(@numFieldGroupID AS VARCHAR) + ' = 4 THEN ADC.numContactID ELSE 0 END) '                                                        
			set @WhereCondition= @WhereCondition +' left Join ListDetails L'+ convert(varchar(3),@tintOrder)+ ' on L'+convert(varchar(3),@tintOrder)+ '.numListItemID=CFW'+ convert(varchar(3),@tintOrder)+'.Fld_Value'                
		END         
		ELSE IF @vcAssociatedControlType = 'CheckBoxList'
		BEGIN
			PRINT 'Custom1'

			SET @strSql= @strSql+',(SELECT STUFF((SELECT CONCAT('', '', vcData) FROM ListDetails WHERE numListID= ' + CAST(@numListID AS varchar) + ' AND numlistitemid IN (SELECT ISNULL(Id,0) FROM dbo.SplitIDs(CFW'+ convert(VARCHAR(3),@tintOrder)+'.Fld_Value,'','')) FOR XML PATH('''')), 1, 1, ''''))  ['+ @vcColumnName +']'   

			SET @WhereCondition= @WhereCondition 
								+' left Join ' + (CASE WHEN @numFieldGroupID = 1 THEN 'CFW_FLD_Values' WHEN @numFieldGroupID = 4 THEN 'CFW_FLD_Values_Cont' ELSE 'CFW_FLD_Values' END) + ' CFW'+ convert(varchar(3),@tintOrder)
								+ ' on CFW' + convert(varchar(3),@tintOrder) 
								+ '.Fld_Id='+convert(varchar(10),@numFieldId) 
								+ 'and CFW' + convert(varchar(3),@tintOrder) 
								+ '.RecId= (CASE WHEN ' + CAST(@numFieldGroupID AS VARCHAR) + ' = 1 THEN DM.numDivisionId WHEN ' + CAST(@numFieldGroupID AS VARCHAR) + ' = 4 THEN ADC.numContactID ELSE 0 END) '										
		
		END        
	END          
                   
	SELECT TOP 1
		@tintOrder=tintOrder,
		@numFieldID=numFormFieldID,
		@vcDbColumnName=vcDbColumnName,
		@vcFormFieldName=vcFieldName,
		@vcAssociatedControlType=vcAssociatedControlType,
		@vcListItemType=vcListItemType,
		@numListID=numListID,
		@vcLookBackTableName=vcLookBackTableName,
		@bitCustom=bitCustom,
		@numFieldGroupID=numGroupID
	FROM
	(
		SELECT
			A.tintOrder+1 tintOrder, 
			numFormFieldID,
			vcDbColumnName,
			vcFieldName,
			vcAssociatedControlType,
			vcListItemType,
			numListID,
			vcLookBackTableName,
			0 AS bitCustom,
			0 AS numGroupID
		FROM 
			AdvSerViewConf A                              
		JOIN 
			View_DynamicDefaultColumns D                               
		ON 
			D.numFieldId=A.numFormFieldId  
			AND d.numformId = 1                           
		WHERE 
			D.numDomainID = @numDomainID 
			AND D.numFormID = 1 
			AND A.numFormID = 1 
			AND tintViewID=@ViewID 
			AND numGroupID=@numGroupID 
			AND A.numDomainID=@numDomainID 
			AND ISNULL(A.bitCustom,0) = 0
			AND d.vcDbColumnName not in ('numBillShipCountry','numBillShipState')
			AND A.tintOrder > @tintOrder-1
		UNION
		SELECT
			A.tintOrder+1 tintOrder,
			numFormFieldID,
			CONCAT('Cust',numFormFieldID),
			Fld_label,
			c.Fld_type,
			'',
			c.numlistid,
			'',
			1 AS bitCustom,
			Grp_id
		FROM    
			AdvSerViewConf A
		JOIN 
			CFW_Fld_Master C ON C.Fld_id = A.numFormFieldID
		WHERE   
			A.numFormID = 1
			AND A.tintViewID = @ViewID
			AND C.numDomainID = @numDomainID AND A.numFormID = 1
			AND A.numGroupID = @numGroupID
			AND A.numDomainID = @numDomainID
			AND ISNULL(A.bitCustom,0) = 1
			AND A.tintOrder > @tintOrder-1
	) T1
	ORDER BY 
		tintOrder asc                
                                   
                                      
 if @@rowcount=0 set @tintOrder=0                              
end                              

                                                                  
  declare @firstRec as integer                                                                    
  declare @lastRec as integer                                                                    
 set @firstRec= (@CurrentPage-1) * @PageSize                      
 set @lastRec= (@CurrentPage*@PageSize+1)                                                                     
set @TotRecs=(select count(*) from #tempTable)                                                   
                                          
if @GetAll=0         
begin        
        
 if @bitMassUpdate=1            
 begin  

   Declare @strReplace as varchar(8000) = ''
    set @strReplace = case when @LookTable='DivisionMaster' then 'DM.numDivisionID' when @LookTable='AdditionalContactsInformation' then 'ADC.numContactId' when @LookTable='ContactAddress' then 'ADC.numContactId' when @LookTable='ShipAddress' then 'DM.numDivisionID' when @LookTable='BillAddress' then 'DM.numDivisionID' when @LookTable='CompanyInfo' then 'C.numCompanyId' end           
   +' FROM AdditionalContactsInformation ADC inner join DivisionMaster DM on ADC.numDivisionID = DM.numDivisionID                  
   JOIN CompanyInfo C ON DM.numCompanyID = C.numCompanyId                                
    '+@WhereCondition        
    set @strMassUpdate=replace(@strMassUpdate,'@$replace',@strReplace)      
    PRINT CONCAT('MassUPDATE:',@strMassUpdate)
  exec (@strMassUpdate)         
 end         
 
 
 IF (@Where = 'DUP')
 BEGIN
	set @strSql=@strSql+' ,Cast((select top 1 bintCreatedDate from OpportunityMaster where numDivisionId=DM.numDivisionID order by numOppId desc) AS DATE)  AS [Last Order Date~bintCreatedDate] FROM AdditionalContactsInformation ADC
	  JOIN DivisionMaster DM ON ADC.numDivisionId = DM.numDivisionID
	  JOIN CompanyInfo C ON DM.numCompanyID = C.numCompanyId    
	  JOIN OpportunityMaster OM on OM.numDivisionID= DM.numDivisionID                  
	  inner join #tempTable T on T.numContactId=ADC.numContactId ' 
	  + @WhereCondition +'
	  WHERE ID > '+convert(varchar(10),@firstRec)+ ' and ID <'+ convert(varchar(10),@lastRec) +  @WCondition + ' order by C.vcCompanyName, T.ID' -- @OrderBy
	  PRINT @strSql
	  exec(@strSql)                                                                 
	 drop table #tempTable
	 RETURN	
 END
ELSE
BEGIN
	SET @strSql = @strSql + ' ,Cast((select top 1 bintCreatedDate from OpportunityMaster where numDivisionId=DM.numDivisionID order by numOppId desc) AS DATE)  AS [Last Order Date~bintCreatedDate] FROM AdditionalContactsInformation ADC
		JOIN DivisionMaster DM ON ADC.numDivisionId = DM.numDivisionID   
	  JOIN CompanyInfo C ON DM.numCompanyID = C.numCompanyId                              
	   '+@WhereCondition+' inner join #tempTable T on T.numContactId=ADC.numContactId                                                               
	  WHERE ID > '+convert(varchar(10),@firstRec)+ ' and ID <'+ convert(varchar(10),@lastRec)
	  
	   
	SET @strSql=@strSql + ' order by C.vcCompanyName, T.ID' 
END
             
end                           
 else        
 begin                      
     set @strSql=@strSql+' ,OM.bintCreatedDate AS [Last Order Date~bintCreatedDate] FROM AdditionalContactsInformation ADC                                                   
  JOIN DivisionMaster DM ON ADC.numDivisionId = DM.numDivisionID      
   JOIN OpportunityMaster OM on OM.numDivisionID= DM.numDivisionID                                                              
  JOIN CompanyInfo C ON DM.numCompanyID = C.numCompanyId                              
  '+@WhereCondition+' join #tempTable T on T.numContactId=ADC.numContactId'        
  
	   SET @strSql=@strSql + ' order by C.vcCompanyName, T.ID'   
	end       
print @strSql                                                      
exec(@strSql) 
drop table #tempTable


	SELECT
		tintOrder,
		vcDbColumnName,
		vcFieldName,
		vcAssociatedControlType,
		vcListItemType,
		numListID,
		vcLookBackTableName,
		numFieldID,
		numFormFieldID,
		intColumnWidth,
		bitCustom
	FROM
	(
		SELECT
			A.tintOrder+1 tintOrder,
			vcDbColumnName,
			vcFieldName,
			vcAssociatedControlType,
			vcListItemType,
			numListID,
			vcLookBackTableName,
			numFieldID,
			numFormFieldID,
			ISNULL(A.intColumnWidth,0) AS intColumnWidth,
			ISNULL(D.bitCustom,0) AS bitCustom
		FROM 
			AdvSerViewConf A                              
		JOIN 
			View_DynamicDefaultColumns D                               
		ON 
			D.numFieldId=A.numFormFieldId  
			AND d.numformId = 1                           
		WHERE 
			D.numDomainID = @numDomainID 
			AND D.numFormID = 1 
			AND A.numFormID = 1 
			AND tintViewID=@ViewID 
			AND numGroupID=@numGroupID 
			AND A.numDomainID=@numDomainID 
			AND ISNULL(A.bitCustom,0) = 0
			AND d.vcDbColumnName not in ('numBillShipCountry','numBillShipState')
		UNION
		SELECT
			A.tintOrder+1 tintOrder,
			CONCAT('Cust',numFormFieldID),
			Fld_label,
			Fld_type,
			'',
			numListID,
			'',
			numFormFieldID,
			numFormFieldID,
			ISNULL(A.intColumnWidth,0) AS intColumnWidth,
			1 AS bitCustom
		FROM    
			AdvSerViewConf A
		JOIN 
			CFW_Fld_Master C ON C.Fld_id = A.numFormFieldID
		WHERE   
			A.numFormID = 1
			AND A.tintViewID = @ViewID
			AND C.numDomainID = @numDomainID AND A.numFormID = 1
			AND A.numGroupID = @numGroupID
			AND A.numDomainID = @numDomainID
			AND ISNULL(A.bitCustom,0) = 1
	) T1
	ORDER BY 
		tintOrder asc       

IF OBJECT_ID('tempdb..#tempAddress') IS NOT NULL
BEGIN
    DROP TABLE #tempAddress
END
END
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_BankReconcileMaster_DeleteByID')
DROP PROCEDURE USP_BankReconcileMaster_DeleteByID
GO
CREATE PROCEDURE USP_BankReconcileMaster_DeleteByID
	@numDomainID NUMERIC(18,0)
	,@numReconcileID numeric(18,0)
AS
BEGIN
BEGIN TRY
BEGIN TRANSACTION
	UPDATE General_Journal_Details SET bitReconcile=0,bitCleared=0 WHERE numDomainId=@numDomainID AND numReconcileID=@numReconcileID
	DELETE FROM BankReconcileFileData WHERE numDomainID=@numDomainID AND numReconcileID=@numReconcileID
	DELETE FROM BankReconcileMaster WHERE numDomainID=@numDomainID AND numReconcileID=@numReconcileID
COMMIT
END TRY
BEGIN CATCH
	DECLARE @ErrorMessage NVARCHAR(4000)
	DECLARE @ErrorNumber INT
	DECLARE @ErrorSeverity INT
	DECLARE @ErrorState INT
	DECLARE @ErrorLine INT
	DECLARE @ErrorProcedure NVARCHAR(200)

	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;

	SELECT 
		@ErrorMessage = ERROR_MESSAGE(),
		@ErrorNumber = ERROR_NUMBER(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE(),
		@ErrorLine = ERROR_LINE(),
		@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

	RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);
END CATCH
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_CalculateCommissionPaidBizDoc')
DROP PROCEDURE USP_CalculateCommissionPaidBizDoc
GO
CREATE PROCEDURE [dbo].[USP_CalculateCommissionPaidBizDoc]
	-- Add the parameters for the stored procedure here
	 @numComPayPeriodID AS NUMERIC(18,0),
	 @numDomainID AS NUMERIC(18,0),
	 @ClientTimeZoneOffset INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
SET NOCOUNT ON;

BEGIN TRY
BEGIN TRANSACTION
	DECLARE @dtPayStart DATE 
	DECLARE @dtPayEnd DATE

	SELECT @dtPayStart=dtStart,@dtPayEnd=dtEnd FROM CommissionPayPeriod WHERE numComPayPeriodID = @numComPayPeriodID

	--GET COMMISSION RULE OF DOMAIN
	DECLARE @TempCommissionRule TABLE
	(
		ID INT IDENTITY(1,1),
		numComRuleID NUMERIC(18,0),
		tintComBasedOn TINYINT,
		tintComType TINYINT,
		tintComAppliesTo TINYINT,
		tintComOrgType TINYINT,
		tintAssignTo TINYINT
	)

	INSERT INTO
		@TempCommissionRule
	SELECT
		CR.numComRuleID,
		CR.tintComBasedOn,
		CR.tintComType,
		CR.tintComAppliesTo,
		CR.tintComOrgType,
		CR.tintAssignTo
	FROM
		CommissionRules CR
	LEFT JOIN
		PriceBookPriorities PBP
	ON	
		CR.tintComAppliesTo = PBP.Step2Value 
		AND CR.tintComOrgType = PBP.Step3Value
	WHERE
		CR.numDomainID = @numDomainID
		AND ISNULL(PBP.[Priority],0) > 0
	ORDER BY
		ISNULL(PBP.[Priority],0) ASC

	--GET COMMISSION GLOBAL SETTINGS
	DECLARE @numDiscountServiceItemID NUMERIC(18,0)
	DECLARE @numShippingServiceItemID NUMERIC(18,0)
	DECLARE @tintCommissionType TINYINT
	DECLARE @bitIncludeTaxAndShippingInCommission BIT

	SELECT 
		@numDiscountServiceItemID=numDiscountServiceItemID, 
		@numShippingServiceItemID = numShippingServiceItemID, 
		@tintCommissionType=tintCommissionType, 
		@bitIncludeTaxAndShippingInCommission=bitIncludeTaxAndShippingInCommission 
	FROM 
		Domain 
	WHERE 
		numDomainID=@numDomainID

	--TEMPORARY TABLES TO HOLD DATA
	DECLARE @TempBizCommission TABLE
	(
		numUserCntID NUMERIC(18,0),
		numOppID NUMERIC(18,0),
		numOppBizDocID NUMERIC(18,0),
		numOppBizDocItemID NUMERIC(18,0),
		monCommission MONEY,
		numComRuleID NUMERIC(18,0),
		tintComType TINYINT,
		tintComBasedOn TINYINT,
		decCommission FLOAT,
		tintAssignTo TINYINT
	)

	DECLARE @TABLEPAID TABLE
	(
		ID INT IDENTITY(1,1),
		numDivisionID NUMERIC(18,0),
		numRelationship NUMERIC(18,0),
		numProfile NUMERIC(18,0),
		numAssignedTo NUMERIC(18,0),
		numRecordOwner NUMERIC(18,0),
		numItemCode NUMERIC(18,0),
		numItemClassification NUMERIC(18,0),
		numOppID NUMERIC(18,0),
		numOppBizDocID NUMERIC(18,0),
		numOppBizDocItemID NUMERIC(18,0),
		numUnitHour FLOAT,
		monTotAmount MONEY,
		monVendorCost MONEY,
		monAvgCost MONEY,
		numPartner NUMERIC(18,0)
	)

	-- GET FULLY PAID INVOICES WITHIN PAY PERIOD
	INSERT INTO 
		@TABLEPAID
	SELECT 
		OppM.numDivisionID,
		ISNULL(CompanyInfo.numCompanyType,0),
		ISNULL(CompanyInfo.vcProfile,0),
		OppM.numAssignedTo,
		OppM.numRecOwner,
		OppBizItems.numItemCode,
		Item.numItemClassification,
		OppM.numOppID,
		OppBizItems.numOppBizDocID,
		OppBizItems.numOppBizDocItemID,
		ISNULL(OppBizItems.numUnitHour,0),
		ISNULL(OppBizItems.monTotAmount,0),
		(ISNULL(OppMItems.monVendorCost,0) * ISNULL(OppBizItems.numUnitHour,0)),
		(ISNULL(OppMItems.monAvgCost,0) * ISNULL(OppBizItems.numUnitHour,0)),
		OppM.numPartner
	FROM 
		OpportunityBizDocs OppBiz
	INNER JOIN
		OpportunityMaster OppM
	ON
		OppBiz.numOppID = OppM.numOppID
	INNER JOIN
		DivisionMaster 
	ON
		OppM.numDivisionID = DivisionMaster.numDivisionID
	INNER JOIN
		CompanyInfo
	ON
		DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
	INNER JOIN
		OpportunityBizDocItems OppBizItems
	ON
		OppBiz.numOppBizDocsId = OppBizItems.numOppBizDocID
	INNER JOIN
		OpportunityItems OppMItems 
	ON
		OppBizItems.numOppItemID = OppMItems.numoppitemtCode
	INNER JOIN
		Item
	ON
		OppBizItems.numItemCode = Item.numItemCode
	LEFT JOIN
		BizDocComission BDC
	ON
		OppM.numOppId = BDC.numOppID 
		AND OppBiz.numOppBizDocsId = BDC.numOppBizDocId
		AND OppBizItems.numOppBizDocItemID = BDC.numOppBizDocItemID
	CROSS APPLY
	(
		SELECT 
			MAX(DM.dtDepositDate) dtDepositDate
		FROM 
			DepositMaster DM 
		JOIN 
			dbo.DepositeDetails DD
		ON 
			DM.numDepositId=DD.numDepositID 
		WHERE 
			DM.tintDepositePage=2 
			AND DM.numDomainId=@numDomainId
			AND DD.numOppID=OppBiz.numOppID 
			AND DD.numOppBizDocsID =OppBiz.numOppBizDocsID
	) TempDepositMaster
	WHERE
		OppM.numDomainId = @numDomainID 
		AND OppM.tintOppType = 1
		AND OppM.tintOppStatus = 1
		AND OppBiz.bitAuthoritativeBizDocs = 1
		AND ISNULL(OppBiz.monAmountPaid,0) >= OppBiz.monDealAmount
		AND (TempDepositMaster.dtDepositDate IS NOT NULL AND (CAST(DATEADD(MINUTE,-@ClientTimeZoneOffset,TempDepositMaster.dtDepositDate) AS DATE) BETWEEN @dtPayStart AND @dtPayEnd))
		AND OppBizItems.numItemCode NOT IN ( @numDiscountServiceItemID )  /*DO NOT INCLUDE DISCOUNT ITEM*/
		AND 1 = (
					CASE 
						WHEN OppBizItems.numItemCode = @numShippingServiceItemID 
						THEN 
							CASE 
								WHEN ISNULL(@bitIncludeTaxAndShippingInCommission,0) = 1
								THEN	
									1
								ELSE	
									0
							END
						ELSE 
							1 
					END
				)/*CONDITION TO INCLUDE/EXCLUDE INCLUDE SHIPPING ITEM */
		AND BDC.numOppBizDocItemID IS NULL --NO ENTRY AVAILABLE IN BizDocComission TABLE FOR BIZDOC ITEM


		-- LOOP ALL COMMISSION RULES
		DECLARE @i INT = 1
		DECLARE @COUNT INT = 0

		SELECT @COUNT=COUNT(*) FROM @TempCommissionRule

		DECLARE @TEMPITEM TABLE
		(
			ID INT,
			UniqueID INT,
			numUserCntID NUMERIC(18,0),
			numOppID NUMERIC(18,0),
			numOppBizDocID NUMERIC(18,0),
			numOppBizDocItemID NUMERIC(18,0),
			numItemCode NUMERIC(18,0),
			numUnitHour FLOAT,
			monTotAmount MONEY,
			monVendorCost MONEY,
			monAvgCost MONEY
		)

		WHILE @i <= @COUNT
		BEGIN
			DECLARE @numComRuleID NUMERIC(18,0)
			DECLARE @tintComBasedOn TINYINT
			DECLARE @tintComAppliesTo TINYINT
			DECLARE @tintComOrgType TINYINT
			DECLARE @tintComType TINYINT
			DECLARE @tintAssignTo TINYINT
			DECLARE @decCommission MONEY
			DECLARE @numTotalAmount AS MONEY
			DECLARE @numTotalUnit AS FLOAT
			DECLARE @RowCount AS INT

			--CLEAR PREVIOUS VALUES FROM TEMP TABLE
			DELETE FROM @TEMPITEM

			--FETCH COMMISSION RULE
			SELECT @numComRuleID=numComRuleID,@tintComBasedOn=tintComBasedOn,@tintComType=tintComType,@tintComAppliesTo=tintComAppliesTo,@tintComOrgType=tintComOrgType,@tintAssignTo=tintAssignTo FROM @TempCommissionRule WHERE ID=@i

			--FIND ALL BIZ DOC ITEMS WHERE COMMISSION IS NOT PAID AND CURRENT RULE ITEMS, ORGANIZATION AND ASSINGED TO FILTER CAN BE APPLIED
			INSERT INTO
				@TEMPITEM
			SELECT
				ROW_NUMBER() OVER (ORDER BY ID),
				ID,
				(CASE @tintAssignTo WHEN 1 THEN numAssignedTo WHEN 2 THEN numRecordOwner ELSE numPartner END),
				numOppID,
				numOppBizDocID,
				numOppBizDocItemID,
				numItemCode,
				numUnitHour,
				monTotAmount,
				monVendorCost,
				monAvgCost
			FROM
				@TABLEPAID
			WHERE
				1 = (CASE @tintComAppliesTo
						--SPECIFIC ITEMS
						WHEN 1 THEN CASE WHEN (SELECT COUNT(*) FROM CommissionRuleItems WHERE numComRuleID = @numComRuleID AND ISNULL(numValue,0) = numItemCode) > 0 THEN 1 ELSE 0 END
						-- ITEM WITH SPECIFIC CLASSIFICATIONS
						WHEN 2 THEN CASE WHEN (SELECT COUNT(*) FROM CommissionRuleItems WHERE numComRuleID = @numComRuleID AND ISNULL(numValue,0) = numItemClassification) > 0 THEN 1 ELSE 0 END
						-- ALL ITEMS
						ELSE 1
					END)
				AND 1 = (CASE @tintComOrgType
							-- SPECIFIC ORGANIZATION
							WHEN 1 THEN CASE WHEN (SELECT COUNT(*) FROM CommissionRuleOrganization WHERE numComRuleID = @numComRuleID AND ISNULL(numValue,0) = numDivisionID) > 0 THEN 1 ELSE 0 END
							-- ORGANIZATION WITH SPECIFIC RELATIONSHIP AND PROFILE
							WHEN 2 THEN CASE WHEN (SELECT COUNT(*) FROM CommissionRuleOrganization WHERE numComRuleID = @numComRuleID AND ISNULL(numValue,0) = numRelationship AND ISNULL(numProfile,0) = numProfile) > 0 THEN 1 ELSE 0 END
							-- ALL ORGANIZATIONS
							ELSE 1
						END)
				AND 1 = (CASE @tintAssignTo 
							-- ORDER ASSIGNED TO
							WHEN 1 THEN CASE WHEN (SELECT COUNT(*) FROM CommissionRuleContacts WHERE numComRuleID = @numComRuleID AND numValue=numAssignedTo) > 0 THEN 1 ELSE 0 END
							-- ORDER OWNER
							WHEN 2 THEN CASE WHEN (SELECT COUNT(*) FROM CommissionRuleContacts WHERE numComRuleID = @numComRuleID AND numValue=numRecordOwner) > 0 THEN 1 ELSE 0 END
							-- Order Partner
							WHEN 3 THEN CASE WHEN (SELECT COUNT(*) FROM CommissionRuleContacts WHERE numComRuleID = @numComRuleID AND numValue=numPartner) > 0 THEN 1 ELSE 0 END
							-- NO OPTION SELECTED IN RULE
							ELSE 0
						END)

			-- IF BIZDOC ITEMS ARE FOUND AFTER FILTER
			IF (SELECT COUNT(*) FROM @TEMPITEM) > 0
			BEGIN
				--CHECK COMMISSION BASED ON OPTION OF RULE
				--BASED ON AMOUNT SOLD
				IF @tintComBasedOn = 1
				BEGIN
					SELECT @numTotalAmount = SUM(monTotAmount) FROM @TEMPITEM

					IF (SELECT COUNT(*) FROM CommissionRuleDtl WHERE numComRuleID=@numComRuleID AND (@numTotalAmount BETWEEN intFrom AND intTo)) > 0
					BEGIN
						SELECT TOP 1 @decCommission=ISNULL(decCommission,0) FROM CommissionRuleDtl WHERE numComRuleID=@numComRuleID AND (@numTotalAmount BETWEEN intFrom AND intTo)
					END
				END
				--BASED ON UNITS SOLD
				ELSE IF @tintComBasedOn = 2
				BEGIN
					SELECT @numTotalUnit = SUM(numUnitHour) FROM @TEMPITEM

					IF (SELECT COUNT(*) FROM CommissionRuleDtl WHERE numComRuleID=@numComRuleID AND (@numTotalUnit BETWEEN intFrom AND intTo)) > 0
					BEGIN
						SELECT TOP 1 @decCommission=ISNULL(decCommission,0) FROM CommissionRuleDtl WHERE numComRuleID=@numComRuleID AND (@numTotalUnit BETWEEN intFrom AND intTo)
					END
				END

				--IF COMMISSION AMOUNT FOUND AFTER CHECKING COMMISSION FROM AND TO RANGE IN ABOVE STEP
				IF ISNULL(@decCommission,0) > 0
				BEGIN
					SELECT @RowCount=COUNT(*) FROM @TEMPITEM

					--CALCULATE COMMISSION AND STORE IN TEMPARORY TABLE
					INSERT INTO @TempBizCommission 
					(
						numUserCntID,
						numOppID,
						numOppBizDocID,
						numOppBizDocItemID,
						monCommission,
						numComRuleID,
						tintComType,
						tintComBasedOn,
						decCommission,
						tintAssignTo
					)
					SELECT
						numUserCntID,
						numOppID,
						numOppBizDocID,
						numOppBizDocItemID,
						CASE @tintComType 
							WHEN 1 --PERCENT
								THEN 
									CASE @tintCommissionType
										--TOTAL AMOUNT PAID
										WHEN 1 THEN monTotAmount * (@decCommission / CAST(100 AS FLOAT))
										--ITEM GROSS PROFIT (VENDOR COST)
										WHEN 2 THEN (ISNULL(monTotAmount,0) - ISNULL(monVendorCost,0)) * (@decCommission / CAST(100 AS FLOAT))
										--ITEM GROSS PROFIT (AVERAGE COST)
										WHEN 3 THEN(ISNULL(monTotAmount,0) - ISNULL(monAvgCost,0)) * (@decCommission / CAST(100 AS FLOAT))
									END
							ELSE  --FLAT: COMMISSION AMOUNT IS EQUALLY DISTRIBUTED AMONG FOUND ENTERIES
								(@decCommission / @RowCount)
						END,
						@numComRuleID,
						@tintComType,
						@tintComBasedOn,
						@decCommission,
						@tintAssignTo
					FROM 
						@TEMPITEM AS T1
					WHERE
						1 = (CASE @tintCommissionType 
								--TOTAL PAID INVOICE
								WHEN 1 THEN CASE WHEN (ISNULL(monTotAmount,0) > 0) THEN 1 ELSE 0 END 
								-- ITEM GROSS PROFIT (VENDOR COST)
								WHEN 2 THEN CASE WHEN ((ISNULL(monTotAmount,0) - ISNULL(monVendorCost,0)) > 0) THEN 1 ELSE 0 END 
								-- ITEM GROSS PROFIT (AVERAGE COST)
								WHEN 3 THEN CASE WHEN ((ISNULL(monTotAmount,0) - ISNULL(monAvgCost,0)) > 0) THEN 1 ELSE 0 END
								ELSE
									0 
							END) 
						AND (
								SELECT 
									COUNT(*) 
								FROM 
									@TempBizCommission 
								WHERE 
									numUserCntID=T1.numUserCntID 
									AND numOppID=T1.numOppID 
									AND numOppBizDocID = T1.numOppBizDocID 
									AND numOppBizDocItemID = T1.numOppBizDocItemID 
							) = 0
				END
			END

			SET @i = @i + 1

			SET @numComRuleID = 0
			SET @tintComBasedOn = 0
			SET @tintComAppliesTo = 0
			SET @tintComOrgType = 0
			SET @tintComType = 0
			SET @tintAssignTo = 0
			SET @decCommission = 0
			SET @numTotalAmount = 0
			SET @numTotalUnit = 0
			SET @RowCount = 0
		END

		--INSERT CALCULATED ENTERIES IN BIZDOCCOMMISSION TABLE
		INSERT INTO BizDocComission
		(	
			numDomainID,
			numUserCntID,
			numOppBizDocId,
			numComissionAmount,
			numOppBizDocItemID,
			numComRuleID,
			tintComType,
			tintComBasedOn,
			decCommission,
			numOppID,
			bitFullPaidBiz,
			numComPayPeriodID,
			tintAssignTo
		)
		SELECT
			@numDomainID,
			numUserCntID,
			numOppBizDocID,
			monCommission,
			numOppBizDocItemID,
			numComRuleID,
			tintComType,
			tintComBasedOn,
			decCommission,
			numOppID,
			1,
			@numComPayPeriodID,
			tintAssignTo
		FROM
			@TempBizCommission
COMMIT
END TRY
BEGIN CATCH
	DECLARE @ErrorMessage NVARCHAR(4000)
	DECLARE @ErrorNumber INT
	DECLARE @ErrorSeverity INT
	DECLARE @ErrorState INT
	DECLARE @ErrorLine INT
	DECLARE @ErrorProcedure NVARCHAR(200)

	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;

	SELECT 
		@ErrorMessage = ERROR_MESSAGE(),
		@ErrorNumber = ERROR_NUMBER(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE(),
		@ErrorLine = ERROR_LINE(),
		@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

	RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);
END CATCH
END
GO

GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_CalculateCommissionUnPaidBizDoc')
DROP PROCEDURE USP_CalculateCommissionUnPaidBizDoc
GO
CREATE PROCEDURE [dbo].[USP_CalculateCommissionUnPaidBizDoc]
	-- Add the parameters for the stored procedure here
	 @numComPayPeriodID AS NUMERIC(18,0),
	 @numDomainID AS NUMERIC(18,0),
	 @ClientTimeZoneOffset INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
SET NOCOUNT ON;

BEGIN TRY
BEGIN TRANSACTION
	DECLARE @dtPayStart DATE 
	DECLARE @dtPayEnd DATE

	SELECT @dtPayStart=dtStart,@dtPayEnd=dtEnd FROM CommissionPayPeriod WHERE numComPayPeriodID = @numComPayPeriodID

	--GET COMMISSION RULE OF DOMAIN
	DECLARE @TempCommissionRule TABLE
	(
		ID INT IDENTITY(1,1),
		numComRuleID NUMERIC(18,0),
		tintComBasedOn TINYINT,
		tintComType TINYINT,
		tintComAppliesTo TINYINT,
		tintComOrgType TINYINT,
		tintAssignTo TINYINT
	)

	INSERT INTO
		@TempCommissionRule
	SELECT
		CR.numComRuleID,
		CR.tintComBasedOn,
		CR.tintComType,
		CR.tintComAppliesTo,
		CR.tintComOrgType,
		CR.tintAssignTo
	FROM
		CommissionRules CR
	LEFT JOIN
		PriceBookPriorities PBP
	ON	
		CR.tintComAppliesTo = PBP.Step2Value 
		AND CR.tintComOrgType = PBP.Step3Value
	WHERE
		CR.numDomainID = @numDomainID
		AND ISNULL(PBP.[Priority],0) > 0
	ORDER BY
		ISNULL(PBP.[Priority],0) ASC

	--GET COMMISSION GLOBAL SETTINGS
	DECLARE @numDiscountServiceItemID NUMERIC(18,0)
	DECLARE @numShippingServiceItemID NUMERIC(18,0)
	DECLARE @tintCommissionType TINYINT
	DECLARE @bitIncludeTaxAndShippingInCommission BIT

	SELECT 
		@numDiscountServiceItemID=numDiscountServiceItemID, 
		@numShippingServiceItemID = numShippingServiceItemID, 
		@tintCommissionType=tintCommissionType, 
		@bitIncludeTaxAndShippingInCommission=bitIncludeTaxAndShippingInCommission 
	FROM 
		Domain 
	WHERE 
		numDomainID=@numDomainID

	--TEMPORARY TABLES TO HOLD DATA
	DECLARE @TempBizCommission TABLE
	(
		numUserCntID NUMERIC(18,0),
		numOppID NUMERIC(18,0),
		numOppBizDocID NUMERIC(18,0),
		numOppBizDocItemID NUMERIC(18,0),
		monCommission MONEY,
		numComRuleID NUMERIC(18,0),
		tintComType TINYINT,
		tintComBasedOn TINYINT,
		decCommission FLOAT,
		tintAssignTo TINYINT
	)

	DECLARE @TABLEPAID TABLE
	(
		ID INT IDENTITY(1,1),
		numDivisionID NUMERIC(18,0),
		numRelationship NUMERIC(18,0),
		numProfile NUMERIC(18,0),
		numAssignedTo NUMERIC(18,0),
		numRecordOwner NUMERIC(18,0),
		numItemCode NUMERIC(18,0),
		numItemClassification NUMERIC(18,0),
		numOppID NUMERIC(18,0),
		numOppBizDocID NUMERIC(18,0),
		numOppBizDocItemID NUMERIC(18,0),
		numUnitHour FLOAT,
		monTotAmount MONEY,
		monVendorCost MONEY,
		monAvgCost MONEY,
		numPartner NUMERIC(18,0)
	)

	-- GET FULLY PAID INVOICES WITHIN PAY PERIOD
	INSERT INTO 
		@TABLEPAID
	SELECT 
		OppM.numDivisionID,
		ISNULL(CompanyInfo.numCompanyType,0),
		ISNULL(CompanyInfo.vcProfile,0),
		OppM.numAssignedTo,
		OppM.numRecOwner,
		OppBizItems.numItemCode,
		Item.numItemClassification,
		OppM.numOppID,
		OppBizItems.numOppBizDocID,
		OppBizItems.numOppBizDocItemID,
		ISNULL(OppBizItems.numUnitHour,0),
		ISNULL(OppBizItems.monTotAmount,0),
		(ISNULL(OppMItems.monVendorCost,0) * ISNULL(OppBizItems.numUnitHour,0)),
		(ISNULL(OppMItems.monAvgCost,0) * ISNULL(OppBizItems.numUnitHour,0)),
		OppM.numPartner
	FROM 
		OpportunityBizDocs OppBiz
	INNER JOIN
		OpportunityMaster OppM
	ON
		OppBiz.numOppID = OppM.numOppID
	INNER JOIN
		DivisionMaster 
	ON
		OppM.numDivisionID = DivisionMaster.numDivisionID
	INNER JOIN
		CompanyInfo
	ON
		DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
	INNER JOIN
		OpportunityBizDocItems OppBizItems
	ON
		OppBiz.numOppBizDocsId = OppBizItems.numOppBizDocID
	INNER JOIN
		OpportunityItems OppMItems 
	ON
		OppBizItems.numOppItemID = OppMItems.numoppitemtCode
	INNER JOIN
		Item
	ON
		OppBizItems.numItemCode = Item.numItemCode
	LEFT JOIN
		BizDocComission BDC
	ON
		OppM.numOppId = BDC.numOppID 
		AND OppBiz.numOppBizDocsId = BDC.numOppBizDocId
		AND OppBizItems.numOppBizDocItemID = BDC.numOppBizDocItemID
	WHERE
		OppM.numDomainId = @numDomainID 
		AND OppM.tintOppType = 1
		AND OppM.tintOppStatus = 1
		AND OppBiz.bitAuthoritativeBizDocs = 1
		AND ISNULL(OppBiz.monAmountPaid,0) < OppBiz.monDealAmount
		AND (OppBiz.dtFromDate IS NOT NULL AND (CAST(DATEADD(MINUTE,-@ClientTimeZoneOffset,OppBiz.dtFromDate) AS DATE) BETWEEN @dtPayStart AND @dtPayEnd))
		AND OppBizItems.numItemCode NOT IN ( @numDiscountServiceItemID )  /*DO NOT INCLUDE DISCOUNT ITEM*/
		AND 1 = (
					CASE 
						WHEN OppBizItems.numItemCode = @numShippingServiceItemID 
						THEN 
							CASE 
								WHEN ISNULL(@bitIncludeTaxAndShippingInCommission,0) = 1
								THEN	
									1
								ELSE	
									0
							END
						ELSE 
							1 
					END
				)/*CONDITION TO INCLUDE/EXCLUDE INCLUDE SHIPPING ITEM */
		AND BDC.numOppBizDocItemID IS NULL --NO ENTRY AVAILABLE IN BizDocComission TABLE FOR BIZDOC ITEM


		-- LOOP ALL COMMISSION RULES
		DECLARE @i INT = 1
		DECLARE @COUNT INT = 0

		SELECT @COUNT=COUNT(*) FROM @TempCommissionRule

		DECLARE @TEMPITEM TABLE
		(
			ID INT,
			UniqueID INT,
			numUserCntID NUMERIC(18,0),
			numOppID NUMERIC(18,0),
			numOppBizDocID NUMERIC(18,0),
			numOppBizDocItemID NUMERIC(18,0),
			numItemCode NUMERIC(18,0),
			numUnitHour FLOAT,
			monTotAmount MONEY,
			monVendorCost MONEY,
			monAvgCost MONEY
		)

		WHILE @i <= @COUNT
		BEGIN
			DECLARE @numComRuleID NUMERIC(18,0)
			DECLARE @tintComBasedOn TINYINT
			DECLARE @tintComAppliesTo TINYINT
			DECLARE @tintComOrgType TINYINT
			DECLARE @tintComType TINYINT
			DECLARE @tintAssignTo TINYINT
			DECLARE @decCommission MONEY
			DECLARE @numTotalAmount AS MONEY
			DECLARE @numTotalUnit AS FLOAT
			DECLARE @RowCount AS INT

			--CLEAR PREVIOUS VALUES FROM TEMP TABLE
			DELETE FROM @TEMPITEM

			--FETCH COMMISSION RULE
			SELECT @numComRuleID=numComRuleID,@tintComBasedOn=tintComBasedOn,@tintComType=tintComType,@tintComAppliesTo=tintComAppliesTo,@tintComOrgType=tintComOrgType,@tintAssignTo=tintAssignTo FROM @TempCommissionRule WHERE ID=@i

			--FIND ALL BIZ DOC ITEMS WHERE COMMISSION IS NOT PAID AND CURRENT RULE ITEMS, ORGANIZATION AND ASSINGED TO FILTER CAN BE APPLIED
			INSERT INTO
				@TEMPITEM
			SELECT
				ROW_NUMBER() OVER (ORDER BY ID),
				ID,
				(CASE @tintAssignTo WHEN 1 THEN numAssignedTo WHEN 2 THEN numRecordOwner ELSE numPartner END),
				numOppID,
				numOppBizDocID,
				numOppBizDocItemID,
				numItemCode,
				numUnitHour,
				monTotAmount,
				monVendorCost,
				monAvgCost
			FROM
				@TABLEPAID
			WHERE
				1 = (CASE @tintComAppliesTo
						--SPECIFIC ITEMS
						WHEN 1 THEN CASE WHEN (SELECT COUNT(*) FROM CommissionRuleItems WHERE numComRuleID = @numComRuleID AND ISNULL(numValue,0) = numItemCode) > 0 THEN 1 ELSE 0 END
						-- ITEM WITH SPECIFIC CLASSIFICATIONS
						WHEN 2 THEN CASE WHEN (SELECT COUNT(*) FROM CommissionRuleItems WHERE numComRuleID = @numComRuleID AND ISNULL(numValue,0) = numItemClassification) > 0 THEN 1 ELSE 0 END
						-- ALL ITEMS
						ELSE 1
					END)
				AND 1 = (CASE @tintComOrgType
							-- SPECIFIC ORGANIZATION
							WHEN 1 THEN CASE WHEN (SELECT COUNT(*) FROM CommissionRuleOrganization WHERE numComRuleID = @numComRuleID AND ISNULL(numValue,0) = numDivisionID) > 0 THEN 1 ELSE 0 END
							-- ORGANIZATION WITH SPECIFIC RELATIONSHIP AND PROFILE
							WHEN 2 THEN CASE WHEN (SELECT COUNT(*) FROM CommissionRuleOrganization WHERE numComRuleID = @numComRuleID AND ISNULL(numValue,0) = numRelationship AND ISNULL(numProfile,0) = numProfile) > 0 THEN 1 ELSE 0 END
							-- ALL ORGANIZATIONS
							ELSE 1
						END)
				AND 1 = (CASE @tintAssignTo 
							-- ORDER ASSIGNED TO
							WHEN 1 THEN CASE WHEN (SELECT COUNT(*) FROM CommissionRuleContacts WHERE numComRuleID = @numComRuleID AND numValue=numAssignedTo) > 0 THEN 1 ELSE 0 END
							-- ORDER OWNER
							WHEN 2 THEN CASE WHEN (SELECT COUNT(*) FROM CommissionRuleContacts WHERE numComRuleID = @numComRuleID AND numValue=numRecordOwner) > 0 THEN 1 ELSE 0 END
							-- ORDER OWNER
							WHEN 3 THEN CASE WHEN (SELECT COUNT(*) FROM CommissionRuleContacts WHERE numComRuleID = @numComRuleID AND numValue=numPartner) > 0 THEN 1 ELSE 0 END
							-- NO OPTION SELECTED IN RULE
							ELSE 0
						END)

			-- IF BIZDOC ITEMS ARE FOUND AFTER FILTER
			IF (SELECT COUNT(*) FROM @TEMPITEM) > 0
			BEGIN
				--CHECK COMMISSION BASED ON OPTION OF RULE
				--BASED ON AMOUNT SOLD
				IF @tintComBasedOn = 1
				BEGIN
					SELECT @numTotalAmount = SUM(monTotAmount) FROM @TEMPITEM

					IF (SELECT COUNT(*) FROM CommissionRuleDtl WHERE numComRuleID=@numComRuleID AND (@numTotalAmount BETWEEN intFrom AND intTo)) > 0
					BEGIN
						SELECT TOP 1 @decCommission=ISNULL(decCommission,0) FROM CommissionRuleDtl WHERE numComRuleID=@numComRuleID AND (@numTotalAmount BETWEEN intFrom AND intTo)
					END
				END
				--BASED ON UNITS SOLD
				ELSE IF @tintComBasedOn = 2
				BEGIN
					SELECT @numTotalUnit = SUM(numUnitHour) FROM @TEMPITEM

					IF (SELECT COUNT(*) FROM CommissionRuleDtl WHERE numComRuleID=@numComRuleID AND (@numTotalUnit BETWEEN intFrom AND intTo)) > 0
					BEGIN
						SELECT TOP 1 @decCommission=ISNULL(decCommission,0) FROM CommissionRuleDtl WHERE numComRuleID=@numComRuleID AND (@numTotalUnit BETWEEN intFrom AND intTo)
					END
				END

				--IF COMMISSION AMOUNT FOUND AFTER CHECKING COMMISSION FROM AND TO RANGE IN ABOVE STEP
				IF ISNULL(@decCommission,0) > 0
				BEGIN
					SELECT @RowCount=COUNT(*) FROM @TEMPITEM

					--CALCULATE COMMISSION AND STORE IN TEMPARORY TABLE
					INSERT INTO @TempBizCommission 
					(
						numUserCntID,
						numOppID,
						numOppBizDocID,
						numOppBizDocItemID,
						monCommission,
						numComRuleID,
						tintComType,
						tintComBasedOn,
						decCommission,
						tintAssignTo
					)
					SELECT
						numUserCntID,
						numOppID,
						numOppBizDocID,
						numOppBizDocItemID,
						CASE @tintComType 
							WHEN 1 --PERCENT
								THEN 
									CASE @tintCommissionType
										--TOTAL AMOUNT PAID
										WHEN 1 THEN monTotAmount * (@decCommission / CAST(100 AS FLOAT))
										--ITEM GROSS PROFIT (VENDOR COST)
										WHEN 2 THEN (ISNULL(monTotAmount,0) - ISNULL(monVendorCost,0)) * (@decCommission / CAST(100 AS FLOAT))
										--ITEM GROSS PROFIT (AVERAGE COST)
										WHEN 3 THEN(ISNULL(monTotAmount,0) - ISNULL(monAvgCost,0)) * (@decCommission / CAST(100 AS FLOAT))
									END
							ELSE  --FLAT: COMMISSION AMOUNT IS EQUALLY DISTRIBUTED AMONG FOUND ENTERIES
								(@decCommission / @RowCount)
						END,
						@numComRuleID,
						@tintComType,
						@tintComBasedOn,
						@decCommission,
						@tintAssignTo
					FROM 
						@TEMPITEM AS T1
					WHERE
						1 = (CASE @tintCommissionType 
								--TOTAL PAID INVOICE
								WHEN 1 THEN CASE WHEN (ISNULL(monTotAmount,0) > 0) THEN 1 ELSE 0 END 
								-- ITEM GROSS PROFIT (VENDOR COST)
								WHEN 2 THEN CASE WHEN ((ISNULL(monTotAmount,0) - ISNULL(monVendorCost,0)) > 0) THEN 1 ELSE 0 END 
								-- ITEM GROSS PROFIT (AVERAGE COST)
								WHEN 3 THEN CASE WHEN ((ISNULL(monTotAmount,0) - ISNULL(monAvgCost,0)) > 0) THEN 1 ELSE 0 END
								ELSE
									0 
							END) 
						AND (
								SELECT 
									COUNT(*) 
								FROM 
									@TempBizCommission 
								WHERE 
									numUserCntID=T1.numUserCntID 
									AND numOppID=T1.numOppID 
									AND numOppBizDocID = T1.numOppBizDocID 
									AND numOppBizDocItemID = T1.numOppBizDocItemID 
							) = 0
				END
			END

			SET @i = @i + 1

			SET @numComRuleID = 0
			SET @tintComBasedOn = 0
			SET @tintComAppliesTo = 0
			SET @tintComOrgType = 0
			SET @tintComType = 0
			SET @tintAssignTo = 0
			SET @decCommission = 0
			SET @numTotalAmount = 0
			SET @numTotalUnit = 0
			SET @RowCount = 0
		END

		--INSERT CALCULATED ENTERIES IN BIZDOCCOMMISSION TABLE
		INSERT INTO BizDocComission
		(	
			numDomainID,
			numUserCntID,
			numOppBizDocId,
			numComissionAmount,
			numOppBizDocItemID,
			numComRuleID,
			tintComType,
			tintComBasedOn,
			decCommission,
			numOppID,
			bitFullPaidBiz,
			numComPayPeriodID,
			tintAssignTo
		)
		SELECT
			@numDomainID,
			numUserCntID,
			numOppBizDocID,
			monCommission,
			numOppBizDocItemID,
			numComRuleID,
			tintComType,
			tintComBasedOn,
			decCommission,
			numOppID,
			0,
			@numComPayPeriodID,
			tintAssignTo
		FROM
			@TempBizCommission
COMMIT
END TRY
BEGIN CATCH
	DECLARE @ErrorMessage NVARCHAR(4000)
	DECLARE @ErrorNumber INT
	DECLARE @ErrorSeverity INT
	DECLARE @ErrorState INT
	DECLARE @ErrorLine INT
	DECLARE @ErrorProcedure NVARCHAR(200)

	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;

	SELECT 
		@ErrorMessage = ERROR_MESSAGE(),
		@ErrorNumber = ERROR_NUMBER(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE(),
		@ErrorLine = ERROR_LINE(),
		@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

	RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);
END CATCH
END

GO

--Created by anoop jayaraj                                                          
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_checkuseratlogin')
DROP PROCEDURE usp_checkuseratlogin
GO
CREATE PROCEDURE [dbo].[USP_CheckUserAtLogin]                                                              
(                                                                                    
@UserName as varchar(100)='',                                                                        
@vcPassword as varchar(100)='',
@vcLinkedinId as varchar(300)=''                                                                           
)                                                              
as             
BEGIN

DECLARE @listIds VARCHAR(MAX)

SELECT 
	@listIds = COALESCE(@listIds+',' ,'') + CAST(UPA.numUserID AS VARCHAR(100)) 
FROM 
	UserMaster U                              
Join 
	Domain D                              
on 
	D.numDomainID=U.numDomainID
Join  
	Subscribers S                            
on 
	S.numTargetDomainID=D.numDomainID
join
	UnitPriceApprover UPA
ON
	D.numDomainID = UPA.numDomainID
WHERE 
	1 = (CASE 
		WHEN @vcLinkedinId='N' THEN (CASE WHEN vcEmailID=@UserName and vcPassword=@vcPassword THEN 1 ELSE 0 END)
		ELSE (CASE WHEN vcLinkedinId=@vcLinkedinId THEN 1 ELSE 0 END)
		END)
                                                 
/*check subscription validity */
IF EXISTS( SELECT *
FROM UserMaster U                              
 Join Domain D                              
 on D.numDomainID=U.numDomainID
 Join  Subscribers S                            
 on S.numTargetDomainID=D.numDomainID
 WHERE 1 = (CASE 
		WHEN @vcLinkedinId='N' THEN (CASE WHEN vcEmailID=@UserName and vcPassword=@vcPassword THEN 1 ELSE 0 END)
		ELSE (CASE WHEN vcLinkedinId=@vcLinkedinId THEN 1 ELSE 0 END)
		END) and bitActive=1 AND getutcdate() > dtSubEndDate)
BEGIN
	RAISERROR('SUB_RENEW',16,1)
	RETURN;
END

SELECT top 1 U.numUserID,numUserDetailId,D.numCost,isnull(vcEmailID,'') vcEmailID,isnull(numGroupID,0) numGroupID,bitActivateFlag,                              
 isnull(vcMailNickName,'') vcMailNickName,isnull(txtSignature,'') txtSignature,isnull(U.numDomainID,0) numDomainID,                              
 isnull(Div.numDivisionID,0) numDivisionID,isnull(vcCompanyName,'') vcCompanyName,isnull(E.bitExchangeIntegration,0) bitExchangeIntegration,                              
 isnull(E.bitAccessExchange,0) bitAccessExchange,isnull(E.vcExchPath,'') vcExchPath,isnull(E.vcExchDomain,'') vcExchDomain,                              
 isnull(D.vcExchUserName,'') vcExchUserName ,isnull(vcFirstname,'')+' '+isnull(vcLastName,'') as ContactName,                              
 Case when E.bitAccessExchange=0 then  E.vcExchPassword when E.bitAccessExchange=1 then D.vcExchPassword end as vcExchPassword,                          
 tintCustomPagingRows,                         
 vcDateFormat,                         
 numDefCountry,                     
 tintComposeWindow,
 dateadd(day,-sintStartDate,getutcdate()) as StartDate,                        
 dateadd(day,sintNoofDaysInterval,dateadd(day,-sintStartDate,getutcdate())) as EndDate,                        
 tintAssignToCriteria,                         
 bitIntmedPage,                         
 tintFiscalStartMonth,numAdminID,isnull(A.numTeam,0) numTeam ,                
 isnull(D.vcCurrency,'') as vcCurrency, 
 isnull(D.numCurrencyID,0) as numCurrencyID, 
 isnull(D.bitMultiCurrency,0) as bitMultiCurrency,              
 bitTrial,isnull(tintChrForComSearch,0) as tintChrForComSearch,  
isnull(tintChrForItemSearch,0) as tintChrForItemSearch,              
 case when bitSMTPServer= 1 then vcSMTPServer else '' end as vcSMTPServer  ,          
 case when bitSMTPServer= 1 then  isnull(numSMTPPort,0)  else 0 end as numSMTPPort      
,isnull(bitSMTPAuth,0) bitSMTPAuth    
,isnull([vcSmtpPassword],'') vcSmtpPassword    
,isnull(bitSMTPSSL,0) bitSMTPSSL   ,isnull(bitSMTPServer,0) bitSMTPServer,       
isnull(IM.bitImap,0) bitImapIntegration, isnull(charUnitSystem,'E')   UnitSystem, datediff(day,getutcdate(),dtSubEndDate) as NoOfDaysLeft,
ISNULL(bitCreateInvoice,0) bitCreateInvoice,
ISNULL([numDefaultSalesBizDocId],0) numDefaultSalesBizDocId,
ISNULL([numDefaultPurchaseBizDocId],0) numDefaultPurchaseBizDocId,
vcDomainName,
S.numSubscriberID,
D.[tintBaseTax],
u.ProfilePic,
ISNULL(D.[numShipCompany],0) numShipCompany,
ISNULL(D.[bitEmbeddedCost],0) bitEmbeddedCost,
(Select isnull(numAuthoritativeSales,0) From AuthoritativeBizDocs Where numDomainId=D.numDomainID) numAuthoritativeSales,
(Select isnull(numAuthoritativePurchase,0) From AuthoritativeBizDocs Where numDomainId=D.numDomainID) numAuthoritativePurchase,
ISNULL(D.[numDefaultSalesOppBizDocId],0) numDefaultSalesOppBizDocId,
ISNULL(tintDecimalPoints,2) tintDecimalPoints,
ISNULL(D.tintBillToForPO,0) tintBillToForPO, 
ISNULL(D.tintShipToForPO,0) tintShipToForPO,
ISNULL(D.tintOrganizationSearchCriteria,0) tintOrganizationSearchCriteria,
ISNULL(D.tintLogin,0) tintLogin,
ISNULL(bitDocumentRepositary,0) bitDocumentRepositary,
ISNULL(D.vcPortalName,'') vcPortalName,
(SELECT COUNT(*) FROM dbo.Subscribers WHERE getutcdate() between dtSubStartDate and dtSubEndDate and bitActive=1),
ISNULL(D.bitGtoBContact,0) bitGtoBContact,
ISNULL(D.bitBtoGContact,0) bitBtoGContact,
ISNULL(D.bitGtoBCalendar,0) bitGtoBCalendar,
ISNULL(D.bitBtoGCalendar,0) bitBtoGCalendar,
ISNULL(bitExpenseNonInventoryItem,0) bitExpenseNonInventoryItem,
ISNULL(D.bitInlineEdit,0) bitInlineEdit,
ISNULL(U.numDefaultClass,0) numDefaultClass,
ISNULL(D.bitRemoveVendorPOValidation,0) bitRemoveVendorPOValidation,
ISNULL(D.bitTrakDirtyForm,1) bitTrakDirtyForm,
isnull(D.tintBaseTaxOnArea,0) tintBaseTaxOnArea,
ISNULL(D.bitAmountPastDue,0) bitAmountPastDue,
CONVERT(DECIMAL(18,2),ISNULL(D.monAmountPastDue,0)) monAmountPastDue,
ISNULL(D.bitSearchOrderCustomerHistory,0) bitSearchOrderCustomerHistory,
isnull(U.tintTabEvent,0) as tintTabEvent,
isnull(D.bitRentalItem,0) as bitRentalItem,
isnull(D.numRentalItemClass,0) as numRentalItemClass,
isnull(D.numRentalHourlyUOM,0) as numRentalHourlyUOM,
isnull(D.numRentalDailyUOM,0) as numRentalDailyUOM,
isnull(D.tintRentalPriceBasedOn,0) as tintRentalPriceBasedOn,
isnull(D.tintCalendarTimeFormat,24) as tintCalendarTimeFormat,
isnull(D.tintDefaultClassType,0) as tintDefaultClassType,
ISNULL(U.numDefaultWarehouse,0) numDefaultWarehouse,
ISNULL(D.tintPriceBookDiscount,0) tintPriceBookDiscount,
ISNULL(D.bitAutoSerialNoAssign,0) bitAutoSerialNoAssign,
ISNULL(IsEnableProjectTracking,0) AS [IsEnableProjectTracking],
ISNULL(IsEnableCampaignTracking,0) AS [IsEnableCampaignTracking],
ISNULL(IsEnableResourceScheduling,0) AS [IsEnableResourceScheduling],
ISNULL(D.numSOBizDocStatus,0) AS numSOBizDocStatus,
ISNULL(D.bitAutolinkUnappliedPayment,0) AS bitAutolinkUnappliedPayment,
ISNULL(numShippingServiceItemID,0) AS [numShippingServiceItemID],
ISNULL(D.numDiscountServiceItemID,0) AS numDiscountServiceItemID,
ISNULL(D.tintCommissionType,1) AS tintCommissionType,
ISNULL(D.bitDiscountOnUnitPrice,0) AS bitDiscountOnUnitPrice,
ISNULL(D.intShippingImageWidth,0) AS intShippingImageWidth ,
ISNULL(D.intShippingImageHeight,0) AS intShippingImageHeight,
ISNULL(D.numTotalInsuredValue,0) AS numTotalInsuredValue,
ISNULL(D.numTotalCustomsValue,0) AS numTotalCustomsValue,
ISNULL(D.bitUseBizdocAmount,1) AS [bitUseBizdocAmount],
ISNULL(D.bitDefaultRateType,1) AS [bitDefaultRateType],
ISNULL(D.bitIncludeTaxAndShippingInCommission,1) AS [bitIncludeTaxAndShippingInCommission],
ISNULL(D.[bitLandedCost],0) AS bitLandedCost,
ISNULL(D.vcLanedCostDefault,'') AS vcLanedCostDefault,
ISNULL(D.bitMinUnitPriceRule,0) AS bitMinUnitPriceRule,
ISNULL(D.numAbovePercent,0) AS numAbovePercent,
ISNULL(D.numBelowPercent,0) AS numBelowPercent,
ISNULL(D.numBelowPriceField,0) AS numBelowPriceField,
ISNULL(@listIds,'') AS vcUnitPriceApprover,
ISNULL(D.numPODropShipBizDoc,0) AS numPODropShipBizDoc,
ISNULL(D.numPODropShipBizDocTemplate,0) AS numPODropShipBizDocTemplate,
ISNULL(D.IsEnableDeferredIncome,0) AS IsEnableDeferredIncome,
ISNULL(D.bitEnableItemLevelUOM,0) AS bitEnableItemLevelUOM,
ISNULL(D.bitPurchaseTaxCredit,0) AS bitPurchaseTaxCredit,
ISNULL(U.bintCreatedDate,0) AS bintCreatedDate,
TempDefaultPage.vcDefaultNavURL,
ISNULL(D.bitApprovalforTImeExpense,0) AS bitApprovalforTImeExpense,
ISNULL(D.intTimeExpApprovalProcess,0) AS intTimeExpApprovalProcess,ISNULL(D.bitApprovalforOpportunity,0) AS bitApprovalforOpportunity,
ISNULL(D.intOpportunityApprovalProcess,0) AS intOpportunityApprovalProcess,
ISNULL(D.numDefaultSalesShippingDoc,0) AS numDefaultSalesShippingDoc,
ISNULL(D.numDefaultPurchaseShippingDoc,0) AS numDefaultPurchaseShippingDoc,
ISNULL(D.bitMarginPriceViolated,0) AS bitMarginPriceViolated,
ISNULL((SELECT TOP 1 intViewAllowed FROM GroupAuthorization WHERE numGroupID=numGroupID AND numModuleID=2 AND numPageID=2),0) AS tintLeadRights,
ISNULL((SELECT TOP 1 intViewAllowed FROM GroupAuthorization WHERE numGroupID=numGroupID AND numModuleID=3 AND numPageID=2),0) AS tintProspectRights,
ISNULL((SELECT TOP 1 intViewAllowed FROM GroupAuthorization WHERE numGroupID=numGroupID AND numModuleID=4 AND numPageID=2),0) AS tintAccountRights
 FROM UserMaster U                              
 left join ExchangeUserDetails E                              
 on E.numUserID=U.numUserID                              
left join ImapUserDetails IM                              
 on IM.numUserCntID=U.numUserDetailID          
 Join Domain D                              
 on D.numDomainID=U.numDomainID                                           
 left join DivisionMaster Div                                            
 on D.numDivisionID=Div.numDivisionID                               
 left join CompanyInfo C                              
 on C.numCompanyID=Div.numCompanyID                               
 left join AdditionalContactsInformation A                              
 on  A.numContactID=U.numUserDetailId                            
 Join  Subscribers S                            
 on S.numTargetDomainID=D.numDomainID     
 OUTER APPLY
 (
	SELECT  
		TOP 1 ISNULL(REPLACE(SCB.Link,'../',''),'Items/frmItemList.aspx?Page=All Items&ItemGroup=0') vcDefaultNavURL
	FROM    
		TabMaster T
	JOIN 
		GroupTabDetails G ON G.numTabId = T.numTabId
	LEFT JOIN
		ShortCutGrpConf SCUC ON G.numTabId=SCUC.numTabId AND SCUC.numDomainId=U.numDomainID AND SCUC.numGroupId=U.numGroupID AND bitDefaultTab=1
	LEFT JOIN
		ShortCutBar SCB
	ON
		SCB.Id=SCUC.numLinkId
	WHERE   
		(T.numDomainID = U.numDomainID OR bitFixed = 1)
		AND G.numGroupID = U.numGroupID
		AND ISNULL(G.[tintType], 0) <> 1
		AND tintTabType =1
		AND T.numTabID NOT IN (2,68)
	ORDER BY 
		SCUC.bitInitialPage DESC
 )  TempDefaultPage                      
 WHERE 1 = (CASE 
		WHEN @vcLinkedinId='N' THEN (CASE WHEN vcEmailID=@UserName and vcPassword=@vcPassword THEN 1 ELSE 0 END)
		ELSE (CASE WHEN U.vcLinkedinId=@vcLinkedinId THEN 1 ELSE 0 END)
		END) and bitActive IN (1,2) AND getutcdate() between dtSubStartDate and dtSubEndDate
             
select numTerritoryId from UserTerritory UT                              
join UserMaster U                              
on numUserDetailId =UT.numUserCntID                                         
where 1 = (CASE 
		WHEN @vcLinkedinId='N' THEN (CASE WHEN vcEmailID=@UserName and vcPassword=@vcPassword THEN 1 ELSE 0 END)
		ELSE (CASE WHEN vcLinkedinId=@vcLinkedinId THEN 1 ELSE 0 END)
		END)

END
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_CompanyInfo_Search' ) 
    DROP PROCEDURE USP_CompanyInfo_Search
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author: Sandeep Patel
-- Create date: 15 April 2014
-- Description:	Gets company info based on search fileds configured
-- =============================================
CREATE PROCEDURE USP_CompanyInfo_Search
	@numDomainID NUMERIC(18,0),
	@numUserCntID NUMERIC(18,0),
	@isStartWithSearch BIT = 0,
	@searchText VARCHAR(MAX),
	@bitExcludeEmployer BIT = 0
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

SELECT @searchText = REPLACE(@searchText,'''', '''''')

DECLARE @CustomerSelectFields AS VARCHAR(MAX)
DECLARE @CustomerSelectCustomFields AS VARCHAR(MAX)
DECLARE @ProspectsRights AS TINYINT                    
DECLARE @accountsRights AS TINYINT                   
DECLARE @leadsRights AS TINYINT 

SET @CustomerSelectFields = ''
SET @CustomerSelectCustomFields = ''
               
SET @ProspectsRights = ( SELECT TOP 1
                                MAX(GA.intViewAllowed) AS intViewAllowed
                         FROM   UserMaster UM INNER JOIN GroupAuthorization GA ON GA.numGroupID = UM.numGroupID INNER JOIN PageMaster PM ON PM.numPageID = GA.numPageID AND PM.numModuleID = GA.numModuleID
                         WHERE  PM.numpageID = 2
                                AND PM.numModuleID = 3
                                AND UM.numUserID = ( SELECT numUserID
                                                     FROM   userMaster
                                                     WHERE  numUserDetailId = @numUserCntID
                                                   )
                         GROUP BY UM.numUserId ,
                                UM.vcUserName ,
                                PM.vcFileName ,
                                GA.numModuleID ,
                                GA.numPageID
                       )                        
                    
SET @accountsRights = ( SELECT TOP 1
                                MAX(GA.intViewAllowed) AS intViewAllowed
                        FROM    UserMaster UM INNER JOIN GroupAuthorization GA ON GA.numGroupID = UM.numGroupID INNER JOIN PageMaster PM ON PM.numPageID = GA.numPageID AND PM.numModuleID = GA.numModuleID
                        WHERE PM.numpageID = 2
                                AND PM.numModuleID = 4
                                AND UM.numUserID = ( SELECT numUserID
                                                     FROM   userMaster
                                                     WHERE  numUserDetailId = @numUserCntID
                                                   )
                        GROUP BY UM.numUserId ,
                                UM.vcUserName ,
                                PM.vcFileName ,
                                GA.numModuleID ,
                                GA.numPageID
                      )                
              
SET @leadsRights = ( SELECT TOP 1
                            MAX(GA.intViewAllowed) AS intViewAllowed
                     FROM   UserMaster UM INNER JOIN GroupAuthorization GA ON GA.numGroupID = UM.numGroupID INNER JOIN PageMaster PM ON PM.numPageID = GA.numPageID AND PM.numModuleID = GA.numModuleID
                     WHERE  PM.numpageID = 2
                            AND PM.numModuleID = 2
                            AND UM.numUserID = ( SELECT numUserID
                                                 FROM   userMaster
                                                 WHERE  numUserDetailId = @numUserCntID
                                               )
                     GROUP BY UM.numUserId ,
                            UM.vcUserName ,
                            PM.vcFileName ,
                            GA.numModuleID ,
                            GA.numPageID
                   )                   
                    
-- START: Get organization display fields
SELECT * INTO #tempOrgDisplayFields FROM
(
	SELECT 
		numFieldId,
		vcDbColumnName,
		ISNULL(vcCultureFieldName,vcFieldName) AS vcFieldName,
		tintRow AS tintOrder,
		0 as Custom
    FROM 
		View_DynamicColumns
	WHERE 
		numFormId=96 AND numDomainID=@numDomainID AND 
		tintPageType=1 AND bitCustom=0  AND ISNULL(bitSettingField,0)=1 AND numRelCntType=0
	UNION   
	SELECT 
		numFieldId,
		vcFieldName,
		vcFieldName,
		tintRow AS tintOrder,
		1 as Custom
    FROM 
		View_DynamicCustomColumns   
	WHERE 
		Grp_id=1 AND numFormId=96 AND numDomainID=@numDomainID AND 
		tintPageType=1 AND bitCustom=1 AND numRelCntType=0
)X

IF EXISTS (SELECT * FROM #tempOrgDisplayFields)
BEGIN

	SELECT 
		@CustomerSelectFields = COALESCE (@CustomerSelectFields,'') + CAST(vcDbColumnName AS VARCHAR(100)) + ','	
	FROM 
		#tempOrgDisplayFields 
	WHERE
		Custom = 0
		
	-- Removes last , from string
	IF DATALENGTH(@CustomerSelectFields) > 0
		SET @CustomerSelectFields = LEFT(@CustomerSelectFields, LEN(@CustomerSelectFields) - 1)

	IF EXISTS(SELECT * FROM #tempOrgDisplayFields WHERE Custom = 1)
	BEGIN
		SELECT 
			@CustomerSelectCustomFields = COALESCE (@CustomerSelectCustomFields,'') + 'dbo.fn_GetCustFldOrganization(' + CAST(numFieldId AS VARCHAR(100)) + ',1,TEMP1.numDivisionID) AS [' + CAST (vcDbColumnName AS VARCHAR (100)) + '],'	
		FROM 
			#tempOrgDisplayFields 
		WHERE
			Custom = 1
	END

	-- Removes last , from string
	IF DATALENGTH(@CustomerSelectCustomFields) > 0
		SET @CustomerSelectCustomFields = LEFT(@CustomerSelectCustomFields, LEN(@CustomerSelectCustomFields) - 1)
END

-- END: Get organization display fields

-- START: Generate default select statement 
                    
DECLARE @strSQL AS VARCHAR(MAX)    

IF  LEN(@CustomerSelectFields) = 0 AND LEN(@CustomerSelectCustomFields) = 0
BEGIN
	SET @CustomerSelectFields = 'vcCompanyName'
END

SET @strSQL = 'SELECT numDivisionID, numCompanyID' + CASE WHEN CHARINDEX('vcCompanyName',@CustomerSelectFields) = 0 THEN ',vcCompanyName' ELSE '' END  +
	CASE @CustomerSelectFields
	WHEN '' THEN ''
	ELSE ',' + @CustomerSelectFields
	END
	 + 
	CASE @CustomerSelectCustomFields
	WHEN '' THEN ''
	ELSE ',' + @CustomerSelectCustomFields
	END
	 + ' FROM (SELECT 
	CMP.numDomainID,
	CMP.numCompanyID, 
	CMP.vcCompanyName,
	CMP.numCompanyType AS numCompanyTypeID, 
	DM.numDivisionID, 
	DM.vcDivisionName, 
	ISNULL((SELECT vcData FROM dbo.ListDetails WHERE numListID = 1 AND numDomainID = DM.numDomainID AND numListItemID = DM.numStatusID),'''') as numStatusID, 
	ISNULL((SELECT vcData FROM dbo.ListDetails WHERE numListID = 2 AND numDomainID = DM.numDomainID AND numListItemID = CMP.numCompanyRating),'''') as numCompanyRating, 
	ISNULL((SELECT vcData FROM dbo.ListDetails WHERE numListID = 3 AND numDomainID = DM.numDomainID AND numListItemID = CMP.numCompanyCredit),'''') as numCompanyCredit,
	ISNULL((SELECT vcData FROM dbo.ListDetails WHERE numListID = 4 AND numDomainID = DM.numDomainID AND numListItemID = CMP.numCompanyIndustry),'''') as numCompanyIndustry,
	ISNULL((SELECT vcData FROM dbo.ListDetails WHERE numListID = 5 AND (numDomainID = DM.numDomainID OR constFlag = 1) AND numListItemID = CMP.numCompanyType),'''') as numCompanyType,
	ISNULL((SELECT vcData FROM dbo.ListDetails WHERE numListID = 6 AND numDomainID = DM.numDomainID AND numListItemID = CMP.numAnnualRevID),'''') as numAnnualRevID,
	ISNULL((SELECT vcData FROM dbo.ListDetails WHERE numListID = 7 AND numDomainID = DM.numDomainID AND numListItemID = CMP.numNoOfEmployeesID),'''') as numNoOfEmployeesID,
	vcComPhone,
	vcComFax,
	ISNULL((SELECT vcData FROM dbo.ListDetails WHERE numListID = 18 AND numDomainID = DM.numDomainID AND numListItemID = CMP.vcHow),'''') as vcHow,
	ISNULL((SELECT vcData FROM dbo.ListDetails WHERE numListID = 21 AND numDomainID = DM.numDomainID AND numListItemID = CMP.vcProfile),'''') as vcProfile,
	ISNULL((SELECT vcData FROM dbo.ListDetails WHERE numListID = 438 AND numDomainID = DM.numDomainID AND numListItemID = DM.numCompanyDiff),'''') as numCompanyDiff,
	DM.vcCompanyDiff,
	isnull(CMP.txtComments,'''') as txtComments, 
	isnull(CMP.vcWebSite,'''') vcWebSite, 
	isnull(AD1.vcStreet,'''') as vcBillStreet, 
	isnull(AD1.vcCity,'''') as vcBillCity, 
	isnull(dbo.fn_GetState(AD1.numState),'''') as numBillState,
	isnull(AD1.vcPostalCode,'''') as vcBillPostCode,
	isnull(dbo.fn_GetListName(AD1.numCountry,0),'''') as numBillCountry,
	isnull(AD2.vcStreet,'''') as vcShipStreet,
	isnull(AD2.vcCity,'''') as vcShipCity,
	isnull(dbo.fn_GetState(AD2.numState),'''')  as numShipState,
	isnull(AD2.vcPostalCode,'''') as vcShipPostCode, 
	isnull(dbo.fn_GetListName(AD2.numCountry,0),'''') as numShipCountry,
	ISNULL((SELECT vcData FROM dbo.ListDetails WHERE numListID = 78 AND numDomainID = DM.numDomainID AND numListItemID = DM.numTerID),'''') as numTerID
FROM  
(
SELECT
	CMP.numCompanyId
FROM
	dbo.CompanyInfo CMP
INNER JOIN
	dbo.DivisionMaster d
ON
	CMP.numCompanyId = d.numCompanyID
INNER JOIN
	AdditionalContactsInformation AC
ON 
	AC.numDivisionID=d.numDivisionID AND ISNULL(AC.bitPrimaryContact,0)=1
WHERE 
	CMP.numDomainID = ' + CAST(@numDomainID AS VARCHAR(20)) + ' AND d.tintCRMType<>0 and d.tintCRMType=1' +
	CASE @ProspectsRights WHEN 0 THEN ' and (d.numAssignedTo=' + convert(varchar(15),@numUserCntID) + ' OR d.numRecOwner=0) ' ELSE '' END +
    CASE @ProspectsRights WHEN 1 THEN ' and (d.numAssignedTo=' + convert(varchar(15),@numUserCntID) + ' OR d.numRecOwner='+convert(varchar(15),@numUserCntID) + ')' ELSE '' END +                        
    CASE @ProspectsRights WHEN 2 THEN ' and (d.numAssignedTo=' + convert(varchar(15),@numUserCntID) + ' OR (d.numTerID in (select numTerritoryID  from UserTerritory where numUserCntID='+convert(varchar(15),@numUserCntID)+') or d.numTerID=0))' ELSE '' END +
'
UNION
SELECT
	CMP.numCompanyId
FROM
	dbo.CompanyInfo CMP
INNER JOIN
	dbo.DivisionMaster d
ON
	CMP.numCompanyId = d.numCompanyID
INNER JOIN
	AdditionalContactsInformation AC
ON 
	AC.numDivisionID=d.numDivisionID AND ISNULL(AC.bitPrimaryContact,0)=1
WHERE 
	CMP.numDomainID = ' + CAST(@numDomainID AS VARCHAR(20)) + ' AND d.tintCRMType<>0 and d.tintCRMType=2' +
	CASE @accountsRights WHEN 0 THEN ' and (d.numAssignedTo=' + convert(varchar(15),@numUserCntID) + ' OR d.numRecOwner=0) ' ELSE '' END +
    CASE @accountsRights WHEN 1 THEN ' and (d.numAssignedTo=' + convert(varchar(15),@numUserCntID) + ' OR d.numRecOwner='+convert(varchar(15),@numUserCntID) + ')' ELSE '' END +                        
    CASE @accountsRights WHEN 2 THEN ' and (d.numAssignedTo=' + convert(varchar(15),@numUserCntID) + ' OR (d.numTerID in (select numTerritoryID  from UserTerritory where numUserCntID='+convert(varchar(15),@numUserCntID)+') or d.numTerID=0))' ELSE '' END +
'
UNION
SELECT
	CMP.numCompanyId
FROM
	dbo.CompanyInfo CMP
INNER JOIN
	dbo.DivisionMaster d
ON
	CMP.numCompanyId = d.numCompanyID
INNER JOIN
	AdditionalContactsInformation AC
ON 
	AC.numDivisionID=d.numDivisionID AND ISNULL(AC.bitPrimaryContact,0)=1
WHERE 
	CMP.numDomainID = ' + CAST(@numDomainID AS VARCHAR(20)) + ' AND d.tintCRMType=0' +
	CASE @leadsRights WHEN 0 THEN ' and (d.numAssignedTo=' + convert(varchar(15),@numUserCntID) + ' OR d.numRecOwner=0) ' ELSE '' END +
    CASE @leadsRights WHEN 1 THEN ' and (d.numAssignedTo=' + convert(varchar(15),@numUserCntID) + ' OR d.numRecOwner='+convert(varchar(15),@numUserCntID) + ')' ELSE '' END +                        
    CASE @leadsRights WHEN 2 THEN ' and (d.numAssignedTo=' + convert(varchar(15),@numUserCntID) + ' OR (d.numTerID in (select numTerritoryID  from UserTerritory where numUserCntID='+convert(varchar(15),@numUserCntID)+') or d.numTerID=0))' ELSE '' END +
') AS TEMP2
INNER JOIN
	CompanyInfo CMP
ON
	TEMP2.numCompanyId =  CMP.numCompanyId                                                 
INNER JOIN 
	DivisionMaster DM    
ON 
	DM.numCompanyID=CMP.numCompanyID
LEFT JOIN 
	dbo.AddressDetails AD1 
ON 
    AD1.numAddressID =  (
							SELECT 
								MAX(numAddressID) 
							FROM 
								AddressDetails 
							WHERE 
								AddressDetails.numDomainID = DM.numDomainID AND 
								AddressDetails.numRecordID = DM.numDivisionID AND 
								AddressDetails.tintAddressOf = 2 AND 
								AddressDetails.tintAddressType = 1 AND 
								AddressDetails.bitIsPrimary = 1
						  )
LEFT JOIN 
	AddressDetails AD2 
ON 
    AD2.numAddressID = (
							SELECT 
								MAX(numAddressID) 
							FROM 
								AddressDetails 
							WHERE 
								AddressDetails.numDomainID = DM.numDomainID AND 
								AddressDetails.numRecordID = DM.numDivisionID AND 
								AddressDetails.tintAddressOf = 2 AND 
								AddressDetails.tintAddressType = 2 AND 
								AddressDetails.bitIsPrimary = 1
						)) AS TEMP1'
	
-- END: Generate default select statement 

                  
-- START: Generate user defined search fields condition

DECLARE @searchWhereCondition varchar(MAX)
DECLARE @singleValueSearchFields AS varchar(MAX)   
DECLARE @multiValueSearchFields AS varchar(MAX) 
DECLARE @customSearchFields AS varchar(MAX)    


SET @searchWhereCondition = ''
SET @singleValueSearchFields = ''   
SET @multiValueSearchFields = ''
SET @customSearchFields = ''
                

SELECT * INTO #tempOrgSearch FROM
(
	SELECT 
		numFieldId,
		vcDbColumnName,
		ISNULL(vcCultureFieldName,vcFieldName) AS vcFieldName,
		tintRow AS tintOrder,
		0 as Custom
    FROM 
		View_DynamicColumns
	WHERE 
		numFormId=97 AND numDomainID=@numDomainID AND 
		tintPageType=1 AND bitCustom=0  AND ISNULL(bitSettingField,0)=1 AND numRelCntType=0
	UNION   
	SELECT 
		numFieldId,
		vcFieldName,
		vcFieldName,
		tintRow AS tintOrder,
		1 as Custom
    FROM 
		View_DynamicCustomColumns   
	WHERE 
		Grp_id=1 AND numFormId=97 AND numDomainID=@numDomainID AND 
		tintPageType=1 AND bitCustom=1 AND numRelCntType=0
)Y

IF @searchText != ''
BEGIN
	IF (SELECT COUNT(*) FROM #tempOrgSearch) > 0 
	BEGIN
		-- START: Remove white spaces from search values
		DECLARE @finalSearchText VARCHAR(MAX)
		DECLARE @TempTable TABLE (vcValue VARCHAR(MAX))

		WHILE LEN(@searchText) > 0
		BEGIN
			SET @finalSearchText = LEFT(@searchText, 
									ISNULL(NULLIF(CHARINDEX(',', @searchText) - 1, -1),
									LEN(@searchText)))
			SET @searchText = SUBSTRING(@searchText,
										 ISNULL(NULLIF(CHARINDEX(',', @searchText), 0),
										 LEN(@searchText)) + 1, LEN(@searchText))

			INSERT INTO @TempTable (vcValue) VALUES ( @finalSearchText )
		END
		
		--Removes white spaces
		UPDATE
			@TempTable
		SET
			vcValue = LTRIM(RTRIM(vcValue))
	
		--Converting table to comma seperated string
		SELECT @searchText = COALESCE(@searchText,'') +  CONVERT(VARCHAR(MAX),vcValue) + ',' FROM @TempTable
	
		--Remove last comma from final search string
		IF DATALENGTH(@searchText) > 0
			SET @searchText = LEFT(@searchText, LEN(@searchText) - 1)
	
		-- END: Remove white spaces from search values
	
		--START: Gets search fields where there is one-one relation 
		SELECT 
			@singleValueSearchFields = COALESCE(@singleValueSearchFields,'') + '(' + CAST (vcDbColumnName AS VARCHAR (50)) + ' LIKE '+ CASE @isStartWithSearch WHEN 1 THEN '''' ELSE '''%' END + REPLACE(@searchText, ',', '%'' OR ' + vcDbColumnName + ' LIKE  ' + CASE @isStartWithSearch WHEN 1 THEN '''' ELSE '''%' END) + '%'')' + ' OR '	
		FROM 
			#tempOrgSearch 
		WHERE 
			numFieldId NOT IN (51,52,96,148,127) AND
			Custom = 0
			
		
		-- Removes last OR from string
		IF DATALENGTH(@singleValueSearchFields) > 0
			SET @singleValueSearchFields = LEFT(@singleValueSearchFields, LEN(@singleValueSearchFields) - 3)
			
		--END: Gets search fields where there is one-one relation 
		
		
		--START: Gets search fields where there is one-many relation 
		-- 1. CONTACT FIRST NAME
		IF (SELECT COUNT(*) FROM #tempOrgSearch WHERE numFieldId = 51) > 0
			SET @multiValueSearchFields = '(SELECT COUNT(*) FROM AdditionalContactsInformation ACI WHERE ACI.numDivisionID=TEMP1.numDivisionID AND (ACI.vcFirstName LIKE '+ CASE @isStartWithSearch WHEN 1 THEN '''' ELSE '''%' END +  REPLACE(@searchText, ',', '%'' OR ACI.vcFirstName LIKE  '+ CASE @isStartWithSearch WHEN 1 THEN '''' ELSE '''%' END) + '%'')) > 0'	
		
		-- 2. CONTACT LAST NAME
		IF (SELECT COUNT(*) FROM #tempOrgSearch WHERE numFieldId = 52) > 0
		BEGIN
			IF @multiValueSearchFields = ''
				SET @multiValueSearchFields = '(SELECT COUNT(*) FROM AdditionalContactsInformation ACI WHERE ACI.numDivisionID=TEMP1.numDivisionID AND (ACI.vcLastName LIKE '+ CASE @isStartWithSearch WHEN 1 THEN '''' ELSE '''%' END +  REPLACE(@searchText, ',', '%'' OR ACI.vcLastName LIKE  '+ CASE @isStartWithSearch WHEN 1 THEN '''' ELSE '''%' END) + '%'')) > 0'
			ELSE	
				SET @multiValueSearchFields = @multiValueSearchFields + ' OR (SELECT COUNT(*) FROM AdditionalContactsInformation ACI WHERE ACI.numDivisionID=TEMP1.numDivisionID AND (ACI.vcLastName LIKE '+ CASE @isStartWithSearch WHEN 1 THEN '''' ELSE '''%' END +  REPLACE(@searchText, ',', '%'' OR ACI.vcLastName LIKE  '+ CASE @isStartWithSearch WHEN 1 THEN '''' ELSE '''%' END) + '%'')) > 0'
		END	
		
		-- 3. ORDER NAME
		IF (SELECT COUNT(*) FROM #tempOrgSearch WHERE numFieldId = 96) > 0
		BEGIN
			IF @multiValueSearchFields = ''
				SET @multiValueSearchFields = '(SELECT COUNT(*) FROM OpportunityMaster OM WHERE OM.numDivisionId = TEMP1.numDivisionID AND (OM.vcPOppName LIKE '+ CASE @isStartWithSearch WHEN 1 THEN '''' ELSE '''%' END +  REPLACE(@searchText, ',', '%'' OR OM.vcPOppName LIKE  '+ CASE @isStartWithSearch WHEN 1 THEN '''' ELSE '''%' END) + '%'')) > 0'
			ELSE	
				SET @multiValueSearchFields = @multiValueSearchFields + ' OR (SELECT COUNT(*) FROM OpportunityMaster OM WHERE OM.numDivisionId = TEMP1.numDivisionID AND (OM.vcPOppName LIKE '+ CASE @isStartWithSearch WHEN 1 THEN '''' ELSE '''%' END +  REPLACE(@searchText, ',', '%'' OR OM.vcPOppName LIKE  '+ CASE @isStartWithSearch WHEN 1 THEN '''' ELSE '''%' END) + '%'')) > 0'
		END	
		
		-- 4.PROJECT NAME
		IF (SELECT COUNT(*) FROM #tempOrgSearch WHERE numFieldId = 148) > 0
		BEGIN
			IF @multiValueSearchFields = ''
				SET @multiValueSearchFields = '(SELECT COUNT(*) FROM dbo.ProjectsMaster PM WHERE PM.numDivisionId = TEMP1.numDivisionID AND (PM.vcProjectName LIKE '+ CASE @isStartWithSearch WHEN 1 THEN '''' ELSE '''%' END +  REPLACE(@searchText, ',', '%'' OR PM.vcProjectName LIKE  '+ CASE @isStartWithSearch WHEN 1 THEN '''' ELSE '''%' END) + '%'')) > 0'
			ELSE	
				SET @multiValueSearchFields = @multiValueSearchFields + ' OR (SELECT COUNT(*) FROM dbo.ProjectsMaster PM WHERE PM.numDivisionId = TEMP1.numDivisionID AND (PM.vcProjectName LIKE '+ CASE @isStartWithSearch WHEN 1 THEN '''' ELSE '''%' END +  REPLACE(@searchText, ',', '%'' OR PM.vcProjectName LIKE  '+ CASE @isStartWithSearch WHEN 1 THEN '''' ELSE '''%' END) + '%'')) > 0'
		END	
		
		-- 5. CASE NAME
		IF (SELECT COUNT(*) FROM #tempOrgSearch WHERE numFieldId = 127) > 0
		BEGIN		
			IF @multiValueSearchFields = ''
				SET @multiValueSearchFields = '(SELECT COUNT(*) FROM dbo.Cases C WHERE C.numDivisionId = TEMP1.numDivisionID AND (C.textSubject LIKE '+ CASE @isStartWithSearch WHEN 1 THEN '''' ELSE '''%' END +  REPLACE(@searchText, ',', '%'' OR C.textSubject LIKE  '+ CASE @isStartWithSearch WHEN 1 THEN '''' ELSE '''%' END) + '%'')) > 0'
			ELSE	
				SET @multiValueSearchFields = @multiValueSearchFields + ' OR (SELECT COUNT(*) FROM dbo.Cases C WHERE C.numDivisionId = TEMP1.numDivisionID AND (C.textSubject LIKE '+ CASE @isStartWithSearch WHEN 1 THEN '''' ELSE '''%' END +  REPLACE(@searchText, ',', '%'' OR C.textSubject LIKE  '+ CASE @isStartWithSearch WHEN 1 THEN '''' ELSE '''%' END) + '%'')) > 0'
		END	
		
		--END: Gets search fields where there is one-many relation 
		
		--START: Get custom search fields
		
		IF EXISTS (SELECT * FROM #tempOrgSearch WHERE Custom = 1)
		BEGIN
			SELECT 
				@customSearchFields = COALESCE (@customSearchFields,'') + '( dbo.fn_GetCustFldOrganization(' + CAST(numFieldId AS VARCHAR(20)) + ',1,TEMP1.numDivisionID) LIKE '+ CASE @isStartWithSearch WHEN 1 THEN '''' ELSE '''%' END + REPLACE(@searchText, ',', '%'' OR dbo.fn_GetCustFldOrganization(' + CAST(numFieldId AS VARCHAR(20)) + ',1,TEMP1.numDivisionID) LIKE  ' + CASE @isStartWithSearch WHEN 1 THEN '''' ELSE '''%' END) + '%'')' + ' OR '	
			FROM 
				#tempOrgSearch 
			WHERE 
				Custom = 1
		END
		
		-- Removes last OR from string
		IF DATALENGTH(@customSearchFields) > 0
			SET @customSearchFields = LEFT(@customSearchFields, LEN(@customSearchFields) - 3)
		
		--END: Get custom search fields
		
		IF DATALENGTH(@singleValueSearchFields) > 0
			SET @searchWhereCondition = @singleValueSearchFields
			
		IF DATALENGTH(@multiValueSearchFields) > 0
			IF DATALENGTH(@searchWhereCondition) > 0
				SET @searchWhereCondition = @searchWhereCondition + ' OR ' + @multiValueSearchFields
			ELSE
				SET @searchWhereCondition = @multiValueSearchFields
				
		IF DATALENGTH(@customSearchFields) > 0
			IF 	DATALENGTH(@searchWhereCondition) = 0
				SET @searchWhereCondition = @customSearchFields
			ELSE
				SET @searchWhereCondition = @searchWhereCondition + ' OR ' + @customSearchFields
			
	END
	ELSE
	BEGIN
		SET @searchWhereCondition = '(vcCompanyName LIKE '+ CASE @isStartWithSearch WHEN 1 THEN '''' ELSE '''%' END +  REPLACE(@searchText, ',', '%'' OR vcCompanyName LIKE  '+ CASE @isStartWithSearch WHEN 1 THEN '''' ELSE '''%' END) + '%'')'
	END
END

-- END: Generate user defined search fields condition

IF DATALENGTH(@searchWhereCondition) > 0
BEGIN
	SET @strSQL = @strSQL + ' WHERE (' + @searchWhereCondition +')'

	IF @bitExcludeEmployer = 1
	BEGIN
		SET @strSQL = @strSQL + ' AND numCompanyTypeID <> 93'
	END
END
ELSE IF @bitExcludeEmployer = 1
BEGIN
	SET @strSQL = @strSQL + ' WHERE numCompanyTypeID <> 93'
END


    
 
SET @strSQL = @strSQL + ' ORDER BY vcCompanyname'    

PRINT @strSQL
--SELECT @strSQL
EXEC (@strSQL)

END
GO
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ElasticSearch_GetItems')
DROP PROCEDURE dbo.USP_ElasticSearch_GetItems
GO
CREATE PROCEDURE [dbo].[USP_ElasticSearch_GetItems]
	@numDomainID NUMERIC(18,0)
AS 
BEGIN
	DECLARE @CustomerSearchFields AS VARCHAR(MAX) = ''
	DECLARE @CustomerSearchCustomFields AS VARCHAR(MAX) = ''

	DECLARE @CustomerDisplayFields AS VARCHAR(MAX) = ''

	DECLARE @query AS NVARCHAR(MAX);

	----------------------------   Search Fields ---------------------------

	DECLARE @TEMPSearchFields TABLE
	(
		numFieldID NUMERIC(18,0)
		,vcDbColumnName VARCHAR(200)
		,tintOrder TINYINT
		,bitCustom BIT
	)

	INSERT INTO @TEMPSearchFields
	(
		numFieldID
		,vcDbColumnName
		,tintOrder
		,bitCustom
	)
	SELECT    
		numFieldId,
		vcDbColumnName,
        tintRow AS tintOrder,
        0 AS Custom
    FROM 
		View_DynamicColumns
    WHERE     
		numFormId = 22
        AND numDomainID = @numDomainID
        AND tintPageType = 1
        AND bitCustom = 0
        AND ISNULL(bitSettingField, 0) = 1
        AND numRelCntType = 1
    UNION
    SELECT 
		numFieldId,
		vcFieldName,
		tintRow AS tintOrder,
		1 AS Custom
    FROM 
		View_DynamicCustomColumns
    WHERE 
		Grp_id = 5
        AND numFormId = 22
        AND numDomainID = @numDomainID
        AND tintPageType = 1
        AND bitCustom = 1
        AND numRelCntType = 1

	IF NOT EXISTS ( SELECT * FROM @TEMPSearchFields )
    BEGIN
        INSERT INTO @TEMPSearchFields
		(
			numFieldID
			,vcDbColumnName
			,tintOrder
			,bitCustom
		)
        SELECT  
			numFieldId ,
			vcDbColumnName,
			tintorder,
			0
        FROM 
			View_DynamicDefaultColumns
        WHERE 
			numFormId = 22
            AND bitDefault = 1
            AND ISNULL(bitSettingField, 0) = 1
            AND numDomainID = @numDomainID 
    END

	IF EXISTS (SELECT * FROM @TEMPSearchFields)
	BEGIN
		SELECT 
			@CustomerSearchFields = COALESCE (@CustomerSearchFields,'') + CAST(vcDbColumnName AS VARCHAR(100)) + ' AS Search_' + CAST(vcDbColumnName AS VARCHAR(100)) + ', '	
		FROM 
			@TEMPSearchFields 
		WHERE
			ISNULL(bitCustom,0) = 0
		ORDER BY
			tintOrder ASC
		
		-- Removes last , from string
		IF DATALENGTH(@CustomerSearchFields) > 0
			SET @CustomerSearchFields = LEFT(@CustomerSearchFields, LEN(@CustomerSearchFields) - 1)

		IF EXISTS(SELECT * FROM @TEMPSearchFields WHERE ISNULL(bitCustom,0) = 1)
		BEGIN
			SELECT 
				@CustomerSearchCustomFields = COALESCE (@CustomerSearchCustomFields,'') + 'ISNULL(CFW' + CAST(numFieldId AS VARCHAR(100)) + ','''') AS ' + 'Search_CFW' + CAST(numFieldId AS VARCHAR(100)) + ','
			FROM 
				@TEMPSearchFields 
			WHERE
				ISNULL(bitCustom,0) = 1
			ORDER BY
				tintOrder ASC
		END

		-- Removes last , from string
		IF DATALENGTH(@CustomerSearchCustomFields) > 0
			SET @CustomerSearchCustomFields = LEFT(@CustomerSearchCustomFields, LEN(@CustomerSearchCustomFields) - 1)
	END

	IF  LEN(@CustomerSearchFields) = 0 AND LEN(@CustomerSearchCustomFields) = 0
	BEGIN
		SET @CustomerSearchFields = 'vcItemName AS Search_vcItemName'
	END


	----------------------------   Display Fields ---------------------------
	DECLARE @TEMPDisplayFields TABLE
	(
		numFieldID NUMERIC(18,0)
		,vcDbColumnName VARCHAR(200)
		,tintOrder TINYINT
		,bitCustom BIT
	)


	INSERT INTO @TEMPDisplayFields
	(
		numFieldID
		,vcDbColumnName
		,tintOrder
		,bitCustom
	)
	SELECT    
		numFieldId,
		vcDbColumnName,
        tintRow AS tintOrder,
        0 AS Custom
    FROM 
		View_DynamicColumns
    WHERE     
		numFormId = 22
        AND numDomainID = @numDomainID
        AND tintPageType = 1
        AND bitCustom = 0
        AND ISNULL(bitSettingField, 0) = 1
        AND numRelCntType = 0
    UNION
    SELECT 
		numFieldId,
		vcFieldName,
		tintRow AS tintOrder,
		1 AS Custom
    FROM 
		View_DynamicCustomColumns
    WHERE 
		Grp_id = 5
        AND numFormId = 22
        AND numDomainID = @numDomainID
        AND tintPageType = 1
        AND bitCustom = 1
        AND numRelCntType = 0


	IF EXISTS (SELECT * FROM @TEMPDisplayFields)
	BEGIN
		SELECT 
			@CustomerDisplayFields = COALESCE (@CustomerDisplayFields,'') + CONCAT('(CASE WHEN LEN(',vcDbColumnName,') > 0 THEN CONCAT(',(CASE WHEN LEN(@CustomerDisplayFields) > 0 THEN ''', '',' ELSE ''''',' END),vcDbColumnName, ') ELSE '''' END)') + ', '	
		FROM 
			@TEMPDisplayFields 
		WHERE
			ISNULL(bitCustom,0) = 0
		ORDER BY
			tintOrder ASC

		IF EXISTS(SELECT * FROM @TEMPDisplayFields WHERE ISNULL(bitCustom,0) = 1)
		BEGIN
			SELECT 
				@CustomerDisplayFields = COALESCE (@CustomerDisplayFields,'') + CONCAT('(CASE WHEN LEN(',CONCAT('CFW',numFieldId),') > 0 THEN CONCAT(',(CASE WHEN LEN(@CustomerDisplayFields) > 0 THEN ''', '',' ELSE ''''',' END),CONCAT('CFW',numFieldId), ') ELSE '''' END)') + ', '	
			FROM 
				@TEMPDisplayFields 
			WHERE
				ISNULL(bitCustom,0) = 1
			ORDER BY
				tintOrder ASC
		END

		-- Removes last , from string
		IF DATALENGTH(@CustomerDisplayFields) > 0
			SET @CustomerDisplayFields = LEFT(@CustomerDisplayFields, LEN(@CustomerDisplayFields) - 1)
	END

	IF  LEN(@CustomerDisplayFields) = 0
	BEGIN
		SET @CustomerDisplayFields = 'vcItemName'
	END

	--IF LEN(@CustomerDisplayFields) > 0 AND CHARINDEX(',',@CustomerDisplayFields) > 0
	--BEGIN
	--	SET @CustomerDisplayFields = REPLACE(@CustomerDisplayFields,',',','', '',')
	--END

	--IF LEN(@CustomerDisplayCustomFields) > 0 AND CHARINDEX(',',@CustomerDisplayCustomFields) > 0
	--BEGIN
	--	SET @CustomerDisplayCustomFields = REPLACE(@CustomerDisplayCustomFields,',',','', '',')
	--END


	DECLARE @CustomFields AS VARCHAR(1000)

	SELECT @CustomFields = COALESCE (@CustomFields,'') + 'CFW' + CAST(FLD_ID AS VARCHAR) + ', ' FROM CFW_Fld_Master WHERE numDomainID=@numDomainID AND Grp_id=5
	-- Removes last , from string
	IF DATALENGTH(@CustomFields) > 0
		SET @CustomFields = LEFT(@CustomFields, LEN(@CustomFields) - 1)

	SELECT @query = CONCAT('SELECT numItemCode as id,''item'' As module, CONCAT(''Items/frmKitDetails.aspx?ItemCode='',numItemCode) AS url,CONCAT(''<b style="color:#ff9900">Item ('',(CASE charItemType WHEN ''P'' THEN ''Inventory''  WHEN ''S'' THEN ''Service'' WHEN ''A'' THEN ''Asset'' ELSE ''Non Inventory'' END),'':)</b> '',',@CustomerDisplayFields,') AS displaytext, CONCAT(''Item ('',(CASE charItemType WHEN ''P'' THEN ''Inventory''  WHEN ''S'' THEN ''Service'' WHEN ''A'' THEN ''Asset'' ELSE ''Non Inventory'' END),''): '',',@CustomerDisplayFields,') AS text',
	CASE @CustomerSearchFields
	WHEN '' THEN ''
	ELSE ',' + @CustomerSearchFields
	END
	,
	CASE @CustomerSearchCustomFields
	WHEN '' THEN ''
	ELSE ',' + @CustomerSearchCustomFields
	END
	 ,' FROM (SELECT 
				Item.numItemCode
				,charItemType
				,ISNULL(vcItemName,'''') AS vcItemName
				,ISNULL(txtItemDesc,'''') AS txtItemDesc
				,ISNULL(vcModelID,'''') AS vcModelID
				,ISNULL(vcSKU,'''') AS vcSKU
				,ISNULL(numBarCodeId,'''') AS numBarCodeId
				,ISNULL(CompanyInfo.vcCompanyName,'''') vcCompanyName
				,(CASE WHEN charItemType=''p'' THEN ISNULL((SELECT TOP 1 monWListPrice FROM WarehouseItems WHERE numItemID=Item.numItemCode AND ISNULL(monWListPrice,0) > 0),ISNULL(monListPrice,0)) ELSE ISNULL(monListPrice,0) END) AS monListPrice
				,ISNULL(Vendor.vcPartNo,'''') vcPartNo
				,(CASE WHEN numItemGroup > 0 AND ISNULL(bitMatrix,0) = 1 THEN dbo.fn_GetItemAttributes(Item.numDomainID,Item.numItemCode) ELSE '''' END) AS vcAttributes
				,(CASE WHEN charItemType=''p'' AND numItemGroup > 0 AND ISNULL(bitMatrix,0) = 0 THEN STUFF((SELECT '', '' + vcWHSKU FROM WareHouseItems WHERE numItemID = Item.numItemCode FOR XML PATH('''')),1,2,'''') ELSE '''' END) AS vcWHSKU
				,(CASE WHEN charItemType=''p'' AND numItemGroup > 0 AND ISNULL(bitMatrix,0) = 0 THEN STUFF((SELECT '', '' + vcBarCode FROM WareHouseItems WHERE numItemID = Item.numItemCode FOR XML PATH('''')),1,2,'''') ELSE '''' END) AS vcBarCode
				,CustomFields.*
			FROM 
				Item
			LEFT JOIN 
				Vendor
			ON
				Item.numVendorID=Vendor.numVendorID
				AND Item.numItemCode=Vendor.numItemCode
			LEFT JOIN 
				DivisionMaster
			ON
				Vendor.numVendorID = DivisionMaster.numDivisionID
			LEFT JOIN 
				CompanyInfo
			ON
				DivisionMaster.numCompanyID = CompanyInfo.numCompanyId
			OUTER APPLY
			(
				SELECT 
				* 
				FROM 
				(
					SELECT  
						CONCAT(''CFW'',CFW_Fld_Values_Item.Fld_ID) Fld_ID,
						ISNULL(dbo.fn_GetCustFldStringValue(CFW_Fld_Values_Item.Fld_ID, CFW_Fld_Values_Item.RecId, CFW_Fld_Values_Item.Fld_Value),'''') Fld_Value 
					FROM 
						CFW_Fld_Master
					LEFT JOIN 
						CFW_Fld_Values_Item
					ON
						CFW_Fld_Values_Item.Fld_ID = CFW_Fld_Master.Fld_id
					WHERE 
						RecId = Item.numItemCode
				) p PIVOT (MAX([Fld_Value]) FOR Fld_ID  IN (',@CustomFields,') ) AS pvt
			) CustomFields
			WHERE
				Item.numDomainID =',@numDomainID,') TEMP1')

	PRINT @query

	EXEC SP_EXECUTESQL @query
END
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ElasticSearch_GetOrganizations')
DROP PROCEDURE dbo.USP_ElasticSearch_GetOrganizations
GO
CREATE PROCEDURE [dbo].[USP_ElasticSearch_GetOrganizations]
	@numDomainID NUMERIC(18,0)
AS 
BEGIN
	DECLARE @CustomerSearchFields AS VARCHAR(MAX) = ''
	DECLARE @CustomerSearchCustomFields AS VARCHAR(MAX) = ''

	DECLARE @CustomerDisplayFields AS VARCHAR(MAX) = ''

	DECLARE @query AS NVARCHAR(MAX);

	----------------------------   Search Fields ---------------------------

	DECLARE @TEMPSearchFields TABLE
	(
		numFieldID NUMERIC(18,0)
		,vcDbColumnName VARCHAR(200)
		,tintOrder TINYINT
		,bitCustom BIT
	)

	INSERT INTO @TEMPSearchFields
	(
		numFieldID
		,vcDbColumnName
		,tintOrder
		,bitCustom
	)
	SELECT 
		numFieldId,
		vcDbColumnName,
		tintRow AS tintOrder,
		0 as Custom
    FROM 
		View_DynamicColumns
	WHERE 
		numFormId=97 AND numDomainID=@numDomainID AND 
		tintPageType=1 AND bitCustom=0  AND ISNULL(bitSettingField,0)=1 AND numRelCntType=0
	UNION   
	SELECT 
		numFieldId,
		vcFieldName,
		tintRow AS tintOrder,
		1 as Custom
    FROM 
		View_DynamicCustomColumns   
	WHERE 
		Grp_id=1 AND numFormId=97 AND numDomainID=@numDomainID AND 
		tintPageType=1 AND bitCustom=1 AND numRelCntType=0

	IF EXISTS (SELECT * FROM @TEMPSearchFields)
	BEGIN
		SELECT 
			@CustomerSearchFields = COALESCE (@CustomerSearchFields,'') + CAST(vcDbColumnName AS VARCHAR(100)) + ' AS Search_' + CAST(vcDbColumnName AS VARCHAR(100)) + ', '	
		FROM 
			@TEMPSearchFields 
		WHERE
			ISNULL(bitCustom,0) = 0
		ORDER BY
			tintOrder ASC
		
		-- Removes last , from string
		IF DATALENGTH(@CustomerSearchFields) > 0
			SET @CustomerSearchFields = LEFT(@CustomerSearchFields, LEN(@CustomerSearchFields) - 1)

		IF EXISTS(SELECT * FROM @TEMPSearchFields WHERE ISNULL(bitCustom,0) = 1)
		BEGIN
			SELECT 
				@CustomerSearchCustomFields = COALESCE (@CustomerSearchCustomFields,'') + 'ISNULL(CFW' + CAST(numFieldId AS VARCHAR(100)) + ','''') AS ' + 'Search_CFW' + CAST(numFieldId AS VARCHAR(100)) + ','
			FROM 
				@TEMPSearchFields 
			WHERE
				ISNULL(bitCustom,0) = 1
			ORDER BY
				tintOrder ASC
		END

		-- Removes last , from string
		IF DATALENGTH(@CustomerSearchCustomFields) > 0
			SET @CustomerSearchCustomFields = LEFT(@CustomerSearchCustomFields, LEN(@CustomerSearchCustomFields) - 1)
	END

	IF  LEN(@CustomerSearchFields) = 0 AND LEN(@CustomerSearchCustomFields) = 0
	BEGIN
		SET @CustomerSearchFields = 'vcCompanyName AS Search_vcCompanyName'
	END

	----------------------------   Display Fields ---------------------------
	DECLARE @TEMPDisplayFields TABLE
	(
		numFieldID NUMERIC(18,0)
		,vcDbColumnName VARCHAR(200)
		,tintOrder TINYINT
		,bitCustom BIT
	)


	INSERT INTO @TEMPDisplayFields
	(
		numFieldID
		,vcDbColumnName
		,tintOrder
		,bitCustom
	)
	SELECT 
		numFieldId,
		vcDbColumnName,
		tintRow AS tintOrder,
		0 as Custom
    FROM 
		View_DynamicColumns
	WHERE 
		numFormId=96 AND numDomainID=@numDomainID AND 
		tintPageType=1 AND bitCustom=0  AND ISNULL(bitSettingField,0)=1 AND numRelCntType=0
	UNION   
	SELECT 
		numFieldId,
		vcFieldName,
		tintRow AS tintOrder,
		1 as Custom
    FROM 
		View_DynamicCustomColumns   
	WHERE 
		Grp_id=1 AND numFormId=96 AND numDomainID=@numDomainID AND 
		tintPageType=1 AND bitCustom=1 AND numRelCntType=0


	IF EXISTS (SELECT * FROM @TEMPDisplayFields)
	BEGIN
		SELECT 
			@CustomerDisplayFields = COALESCE (@CustomerDisplayFields,'') + CONCAT('(CASE WHEN LEN(',vcDbColumnName,') > 0 THEN CONCAT(',(CASE WHEN LEN(@CustomerDisplayFields) > 0 THEN ''', '',' ELSE ''''',' END),vcDbColumnName, ') ELSE '''' END)') + ', '	
		FROM 
			@TEMPDisplayFields 
		WHERE
			ISNULL(bitCustom,0) = 0
		ORDER BY
			tintOrder ASC
		
		IF EXISTS(SELECT * FROM @TEMPDisplayFields WHERE ISNULL(bitCustom,0) = 1)
		BEGIN
			SELECT 
				@CustomerDisplayFields = COALESCE (@CustomerDisplayFields,'') + CONCAT('(CASE WHEN LEN(',CONCAT('CFW',numFieldId),') > 0 THEN CONCAT(',(CASE WHEN LEN(@CustomerDisplayFields) > 0 THEN ''', '',' ELSE ''''',' END),CONCAT('CFW',numFieldId), ') ELSE '''' END)') + ', '	
			FROM 
				@TEMPDisplayFields 
			WHERE
				ISNULL(bitCustom,0) = 1
			ORDER BY
				tintOrder ASC
		END

		-- Removes last , from string
		IF DATALENGTH(@CustomerDisplayFields) > 0
			SET @CustomerDisplayFields = LEFT(@CustomerDisplayFields, LEN(@CustomerDisplayFields) - 1)
	END

	IF  LEN(@CustomerDisplayFields) = 0
	BEGIN
		SET @CustomerDisplayFields = 'vcCompanyName'
	END

	--IF LEN(@CustomerDisplayFields) > 0 AND CHARINDEX(',',@CustomerDisplayFields) > 0
	--BEGIN
	--	SET @CustomerDisplayFields = REPLACE(@CustomerDisplayFields,',',','', '',')
	--END

	--IF LEN(@CustomerDisplayCustomFields) > 0 AND CHARINDEX(',',@CustomerDisplayCustomFields) > 0
	--BEGIN
	--	SET @CustomerDisplayCustomFields = REPLACE(@CustomerDisplayCustomFields,',',','', '',')
	--END

	DECLARE @CustomFields AS VARCHAR(1000)

	SELECT @CustomFields = COALESCE (@CustomFields,'') + 'CFW' + CAST(FLD_ID AS VARCHAR) + ', ' FROM CFW_Fld_Master WHERE numDomainID=@numDomainID AND Grp_id=1
	-- Removes last , from string
	IF DATALENGTH(@CustomFields) > 0
		SET @CustomFields = LEFT(@CustomFields, LEN(@CustomFields) - 1)

	SELECT @query = CONCAT('SELECT numDivisionID as id,''organization'' As module, ISNULL(tintCRMType,0) AS tintOrgCRMType, numOrgAssignedTo, numOrgRecOwner, numOrgTerID, (CASE tintCRMType WHEN 1 THEN CONCAT(''/prospects/frmProspects.aspx?DivID='',numDivisionID) WHEN 2 THEN CONCAT(''/Account/frmAccounts.aspx?DivID='',numDivisionID) ELSE CONCAT(''/Leads/frmLeads.aspx?DivID='',numDivisionID) END) url,CONCAT(''<b style="color:#0070C0">'',numCompanyType,(CASE WHEN numOrgCompanyType=46 THEN CONCAT('' ('',(CASE tintCRMType WHEN 1 THEN ''Prospect'' WHEN 2 THEN ''Account'' ELSE ''Lead'' END),'')'') ELSE '''' END),'':</b> '',', @CustomerDisplayFields,') AS displaytext, CONCAT(numCompanyType,(CASE WHEN numOrgCompanyType=46 THEN CONCAT('' ('',(CASE tintCRMType WHEN 1 THEN ''Prospect'' WHEN 2 THEN ''Account'' ELSE ''Lead'' END),'')'') ELSE '''' END),'': '',', @CustomerDisplayFields,') AS text',
	CASE @CustomerSearchFields
	WHEN '' THEN ''
	ELSE ',' + @CustomerSearchFields
	END
	,
	CASE @CustomerSearchCustomFields
	WHEN '' THEN ''
	ELSE ',' + @CustomerSearchCustomFields
	END
	 ,' FROM (SELECT
		DivisionMaster.numDivisionID
		,ISNULL(DivisionMaster.numAssignedTo,0) numOrgAssignedTo
		,ISNULL(DivisionMaster.numRecOwner,0) numOrgRecOwner
		,ISNULL(DivisionMaster.numTerID,0) numOrgTerID
		,CompanyInfo.vcCompanyName
		,CompanyInfo.numCompanyType AS numOrgCompanyType
		,DivisionMaster.tintCRMType
		,ISNULL(LDComDiff.vcData,'''') numCompanyDiff
		,ISNULL(DivisionMaster.vcCompanyDiff,'''') vcCompanyDiff
		,ISNULL(LDCMPProfile.vcData,'''') vcProfile
		,ISNULL(LDCMPType.vcData,'''') numCompanyType
		,ISNULL(vcComPhone,'''') vcComPhone
		,ISNULL(LDCMPNoOfEmployees.vcData,'''') numNoOfEmployeesId
		,ISNULL(LDCMPAnnualRevenue.vcData,'''') numAnnualRevID
		,ISNULL(vcWebSite,'''') vcWebSite 
		,ISNULL(LDTerritory.vcData,'''') numTerID
		,ISNULL(LDLeadSource.vcData,'''') vcHow
		,ISNULL(LDCMPIndustry.vcData,'''') numCompanyIndustry
		,ISNULL(LDCMPRating.vcData,'''') numCompanyRating
		,ISNULL(LDDMStatus.vcData,'''') numStatusID
		,ISNULL(LDCMPCreditLimit.vcData,'''') numCompanyCredit
		,ISNULL(vcComFax,'''') vcComFax
		,ISNULL(txtComments,'''') txtComments
		,ISNULL(ADC.vcFirstName,'''') vcFirstName
		,ISNULL(ADC.vcLastName,'''') vcLastName
		,ISNULL(ShippingAddress.vcStreet,'''') vcShipStreet
		,ISNULL(ShippingAddress.vcCity,'''') vcShipCity
		,ISNULL(ShippingAddress.vcPostalCode,'''') vcShipPostCode
		,ISNULL(LDShipCountry.vcData,'''') numShipCountry 
		,ISNULL(ShipState.vcState,'''') numShipState
		,ISNULL(BillingAddress.vcStreet,'''') vcBillStreet
		,ISNULL(BillingAddress.vcCity,'''') vcBillCity
		,ISNULL(BillingAddress.vcPostalCode,'''') vcBillPostCode
		,ISNULL(LDBillCountry.vcData,'''') numBillCountry 
		,ISNULL(BillState.vcState,'''') numBillState 
		,ISNULL(LDDMFollowup.vcData,'''') numFollowUpStatus
		,ISNULL(BillingTerms.vcTerms,'''') numBillingDays
		,DivisionMaster.vcPartnerCode
		,CustomFields.*
	FROM 
		CompanyInfo
	INNER JOIN
		DivisionMaster
	ON
		CompanyInfo.numCompanyId = DivisionMaster.numCompanyID
	LEFT JOIN
		ListDetails LDCMPRating
	ON
		CompanyInfo.numCompanyCredit = LDCMPRating.numListItemID
		AND LDCMPRating.numListID = 2
	LEFT JOIN
		ListDetails LDCMPCreditLimit
	ON
		CompanyInfo.numCompanyRating = LDCMPCreditLimit.numListItemID
		AND LDCMPCreditLimit.numListID = 3
	LEFT JOIN
		ListDetails LDCMPIndustry
	ON
		CompanyInfo.numCompanyIndustry = LDCMPIndustry.numListItemID
		AND LDCMPIndustry.numListID = 4
	LEFT JOIN
		ListDetails LDCMPType
	ON
		CompanyInfo.numCompanyType = LDCMPType.numListItemID
		AND LDCMPType.numListID = 5
	LEFT JOIN
		ListDetails LDCMPAnnualRevenue
	ON
		CompanyInfo.numAnnualRevID = LDCMPAnnualRevenue.numListItemID
		AND LDCMPAnnualRevenue.numListID = 6
	LEFT JOIN
		ListDetails LDCMPNoOfEmployees
	ON
		CompanyInfo.numNoOfEmployeesId = LDCMPNoOfEmployees.numListItemID
		AND LDCMPNoOfEmployees.numListID = 7
	LEFT JOIN
		ListDetails LDCMPProfile
	ON
		CompanyInfo.vcProfile = LDCMPProfile.numListItemID
		AND LDCMPProfile.numListID = 21
	LEFT JOIN
		ListDetails LDDMFollowup
	ON
		DivisionMaster.numFollowUpStatus = LDDMFollowup.numListItemID
		AND LDDMFollowup.numListID = 30
	LEFT JOIN
		ListDetails LDDMStatus
	ON
		DivisionMaster.numStatusID = LDDMStatus.numListItemID
		AND LDDMStatus.numListID = 1
	LEFT JOIN
		BillingTerms
	ON
		DivisionMaster.numBillingDays = BillingTerms.numTermsID
	LEFT JOIN
		ListDetails LDComDiff
	ON 
		DivisionMaster.numCompanyDiff = LDComDiff.numListItemID
		AND LDComDiff.numListID = 438
	LEFT JOIN
		ListDetails LDTerritory
	ON 
		DivisionMaster.numTerID = LDTerritory.numListItemID
		AND LDTerritory.numListID = 78
	LEFT JOIN
		ListDetails LDLeadSource
	ON 
		CompanyInfo.vcHow = LDLeadSource.numListItemID
		AND LDLeadSource.numListID = 18
	OUTER APPLY
	(
		SELECT TOP 1 
			vcFirstName
			,vcLastName
		FROM
			AdditionalContactsInformation
		WHERE
			AdditionalContactsInformation.numDomainID = DivisionMaster.numDomainID
			AND AdditionalContactsInformation.numDivisionID=DivisionMaster.numDivisionID
			AND ISNULL(bitPrimaryContact,0) = 1 
	) ADC
	LEFT JOIN
		AddressDetails BillingAddress
	ON
		BillingAddress.numRecordID = DivisionMaster.numDivisionID 
		AND BillingAddress.tintAddressOf = 2 
		AND BillingAddress.tintAddressType = 1 
		AND BillingAddress.bitIsPrimary = 1
		AND BillingAddress.numDomainID = DivisionMaster.numDomainID
	LEFT JOIN
		ListDetails LDBillCountry
	ON 
		BillingAddress.numCountry = LDBillCountry.numListItemID
		AND LDBillCountry.numListID = 40
	LEFT JOIN
		[State] BillState
	ON 
		BillingAddress.numState = BillState.numStateID
		AND BillState.numDomainID = DivisionMaster.numDomainID
	LEFT JOIN
		AddressDetails ShippingAddress
	ON
		ShippingAddress.numRecordID = DivisionMaster.numDivisionID 
		AND ShippingAddress.tintAddressOf = 2 
		AND ShippingAddress.tintAddressType = 2 
		AND ShippingAddress.bitIsPrimary = 1
		AND ShippingAddress.numDomainID = DivisionMaster.numDomainID
	LEFT JOIN
		ListDetails LDShipCountry
	ON 
		ShippingAddress.numCountry = LDShipCountry.numListItemID
		AND LDShipCountry.numListID = 40
	LEFT JOIN
		[State] ShipState
	ON 
		ShippingAddress.numState = ShipState.numStateID
		AND ShipState.numDomainID = DivisionMaster.numDomainID
	OUTER APPLY
	(
		SELECT 
		* 
		FROM 
		(
			SELECT  
				CONCAT(''CFW'',CFW_Fld_Values.Fld_ID) Fld_ID,
				dbo.fn_GetCustFldStringValue(CFW_FLD_Values.Fld_ID, CFW_FLD_Values.RecId, CFW_FLD_Values.Fld_Value) Fld_Value 
			FROM 
				CFW_Fld_Master
			LEFT JOIN 
				CFW_Fld_Values
			ON
				CFW_Fld_Values.Fld_ID = CFW_Fld_Master.Fld_id
			WHERE 
				RecId = DivisionMaster.numDivisionID
		) p PIVOT (MAX([Fld_Value]) FOR Fld_ID  IN (',@CustomFields,') ) AS pvt
	 ) CustomFields
	WHERE
		CompanyInfo.numDomainID =',@numDomainID,') TEMP1')

	PRINT @query

	EXEC SP_EXECUTESQL @query
END
GO
IF EXISTS(SELECT * FROM sysobjects SYSOBJ WHERE SYSOBJ.xtype='p'AND SYSOBJ.NAME ='USP_GET_DROPDOWN_DATA_FOR_IMPORT')
DROP PROCEDURE USP_GET_DROPDOWN_DATA_FOR_IMPORT
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- USP_GET_DROPDOWN_DATA_FOR_IMPORT 75,1
CREATE PROCEDURE USP_GET_DROPDOWN_DATA_FOR_IMPORT
(
	@intImportFileID    BIGINT,
	@numDomainID		BIGINT
)
AS 
BEGIN
	
	DECLARE @numFormID AS NUMERIC
	
	SELECT @numFormID = numMasterID  FROM dbo.Import_File_Master  WHERE intImportFileID=@intImportFileID
	
	
	SELECT ROW_NUMBER() OVER (ORDER BY intImportFileID) AS [ID],* 
			INTO #tempDropDownData FROM(
					SELECT  PARENT.intImportFileID,
							vcImportFileName,
							PARENT.numDomainID,
							DFFM.numFieldId [numFormFieldId],
							DFFM.vcAssociatedControlType,
							DYNAMIC_FIELD.vcDbColumnName,
							DFFM.vcFieldName [vcFormFieldName],
							FIELD_MAP.intMapColumnNo,
							DYNAMIC_FIELD.numListID,
							0 AS [bitCustomField]					
					FROM Import_File_Master PARENT
					INNER JOIN Import_File_Field_Mapping FIELD_MAP ON PARENT.intImportFileID = FIELD_MAP.intImportFileID
					--INNER JOIN dbo.DynamicFormFieldMaster DYNAMIC_FIELD ON FIELD_MAP.numFormFieldID = DYNAMIC_FIELD.numFormFieldId
					INNER JOIN dbo.DycFormField_Mapping DFFM ON FIELD_MAP.numFormFieldID = DFFM.numFieldId
					INNER JOIN dbo.DycFieldMaster DYNAMIC_FIELD ON DYNAMIC_FIELD.numFieldId = DFFM.numFieldId
					WHERE DFFM.vcAssociatedControlType ='SelectBox'
					  AND PARENT.intImportFileID = @intImportFileID
					  AND PARENT.numDomainID = @numDomainID   
					  AND DFFM.numFormID = @numFormID
					
					UNION ALL
					
						  SELECT    FIELD_MAP.intImportFileID AS [intImportFileID],
									cfm.Fld_label  AS [vcImportFileName],
									numDomainID AS [numDomainID],
									CFm.Fld_id AS [numFormFieldId],
									CASE WHEN CFM.Fld_type = 'Drop Down List Box'
										 THEN 'SelectBox'
										 ELSE CFM.Fld_type 
									END	 AS [vcAssociatedControlType],
									cfm.Fld_label AS [vcDbColumnName],
									cfm.Fld_label AS [vcFormFieldName],
									FIELD_MAP.intMapColumnNo  AS [intMapColumnNo],
									CFM.numlistid AS [numListID],
									1 AS [bitCustomField]
						  FROM      dbo.CFW_Fld_Master CFM
						  INNER JOIN Import_File_Field_Mapping FIELD_MAP ON CFM.Fld_id = FIELD_MAP.numFormFieldID		
						  WHERE FIELD_MAP.intImportFileID = @intImportFileID     
						    AND numDomainID = @numDomainID) TABLE1 WHERE  vcAssociatedControlType = 'SelectBox' 
                    
	SELECT * FROM #tempDropDownData   
	
	DECLARE @intRow AS INT
	SELECT @intRow = COUNT(*) FROM #tempDropDownData
	PRINT @intRow
	
	DECLARE @intCnt AS INT
	DECLARE @numListIDValue AS BIGINT
	DECLARE @numFormFieldIDValue AS BIGINT
	DECLARE @bitCustomField AS BIT
		
	DECLARE @strSQL AS NVARCHAR(MAX)
	DECLARE @vcTableNames AS VARCHAR(MAX) 
	DECLARE @strDBColumnName AS NVARCHAR(1000)
	
	SET @intCnt = 0
	SET @strSQL = '' 
	SET @vcTableNames = ''

	CREATE TABLE #tempGroups
	(
		[Key] NUMERIC(9),
		[Value] VARCHAR(100)
	) 	
	INSERT INTO #tempGroups([Key],[Value]) 
	SELECT 0 AS [Key],'Groups' AS [Value]
	UNION ALL
	SELECT 1 AS [Key],'Prospects' AS [Value]
	UNION ALL
	SELECT 1 AS [Key],'Prospect' AS [Value]
	UNION ALL
	SELECT 1 AS [Key],'Pros' AS [Value]
	UNION ALL
	SELECT 2 AS [Key],'Accounts' AS [Value]
	UNION ALL
	SELECT 2 AS [Key],'Account' AS [Value]
	UNION ALL
	SELECT 2 AS [Key],'Acc' AS [Value]

	--SELECT * FROM #tempGroups
	
	WHILE(@intCnt < @intRow   )
		BEGIN
			SET @intCnt = @intCnt + 1
				
			SELECT  
					@numListIDValue = numListID,
					@strDBColumnName = vcDbColumnName, 
					@numFormFieldIDValue = numFormFieldID,
					@bitCustomField = bitCustomField 	 
			FROM #tempDropDownData 
			WHERE ID = @intCnt
			  AND intImportFileID = @intImportFileID
			
			PRINT 'BIT'
			PRINT @bitCustomField
			PRINT @strDBColumnName
			SELECT @strSQL = CASE  
								WHEN @strDBColumnName = 'numItemGroup'
								THEN @strSQL + 'SELECT ITEM_GROUP.numItemGroupID AS [Key],ITEM_GROUP.vcItemGroup AS [Value] FROM dbo.ItemGroups ITEM_GROUP WHERE ITEM_GROUP.numDomainID = ' + CONVERT(VARCHAR,@numDomainID)
								
								WHEN @strDBColumnName = 'tintCRMType'
								THEN @strSQL + 'SELECT DISTINCT [Key],[Value] FROM #tempGroups '
								
								WHEN @strDBColumnName = 'vcLocation'
								THEN @strSQL + 'SELECT numWLocationID AS [Key],vcLocation AS [Value] FROM dbo.WarehouseLocation WHERE numDomainID = ' + CONVERT(VARCHAR,@numDomainID)
								
								WHEN @strDBColumnName = 'vcExportToAPI'
								THEN @strSQL + 'SELECT DISTINCT WebApiId AS [Key],vcProviderName AS [Value] FROM dbo.WebAPI '
								
								WHEN @strDBColumnName = 'numCategoryID'
								THEN @strSQL + 'SELECT numCategoryID AS [Key], vcCategoryName AS [Value] FROM dbo.Category WHERE numDomainID =' + CONVERT(VARCHAR,@numDomainID)
								
								WHEN @strDBColumnName = 'numGrpID'
								THEN @strSQL + 'SELECT GRP.numGrpID AS [Key],GRP.vcGrpName AS [Value] FROM dbo.Groups GRP ' 
											    			    
								WHEN @strDBColumnName = 'numWareHouseID' --OR @strDBColumnName = 'numWareHouseItemID'															  
								THEN @strSQL + 'SELECT WAREHOUSE.numWareHouseID AS [Key],WAREHOUSE.vcWareHouse AS [Value] FROM dbo.Warehouses WAREHOUSE WHERE WAREHOUSE.numDomainID = '+ CONVERT(VARCHAR,@numDomainID) 
								
								WHEN @strDBColumnName = 'numWareHouseItemID'
								THEN @strSQL + 'SELECT WI.numWareHouseItemID AS [Key],W.vcWareHouse AS [Value] FROM dbo.WareHouseItems WI INNER JOIN dbo.Warehouses W ON WI.numWareHouseID = W.numWareHouseID WHERE W.numDomainID = '+ CONVERT(VARCHAR,@numDomainID)
												
								WHEN @strDBColumnName = 'numChildItemID' OR @strDBColumnName = 'numItemKitID'
								THEN @strSQL + ' SELECT ITEM.numItemCode AS [Key],ITEM.vcItemName AS [Value] FROM dbo.Item ITEM WHERE ITEM.numDomainID = '+ CONVERT(VARCHAR,@numDomainID)
								
								WHEN (@strDBColumnName = 'numUOMId' OR @strDBColumnName = 'numPurchaseUnit' OR @strDBColumnName = 'numSaleUnit' OR @strDBColumnName = 'numBaseUnit') 
								THEN @strSQL + '  SELECT numUOMId AS [Key], vcUnitName AS [Value] FROM dbo.UOM
												  JOIN Domain d ON dbo.UOM.numDomainID = d.numDomainID 
												  WHERE dbo.UOM.numDomainId = ' + CONVERT(VARCHAR(10),@numDomainID) +' 
												  AND bitEnabled=1 
												  AND dbo.UOM.tintUnitType=(CASE WHEN ISNULL(d.charUnitSystem,''E'')=''E'' THEN 1 WHEN d.charUnitSystem=''M'' THEN 2 END) '	
								
								WHEN (@strDBColumnName = 'numIncomeChartAcntId' OR @strDBColumnName = 'numAssetChartAcntId' OR @strDBColumnName = 'numCOGsChartAcntId')
								THEN @strSQL + '  SELECT numAccountId AS [Key], LTRIM(RTRIM(REPLACE(vcAccountName,ISNULL(vcNumber,''''),''''))) AS [Value] FROM dbo.Chart_Of_Accounts WHERE numDomainID= ' + CONVERT(VARCHAR(10),@numDomainID)

								WHEN @strDBColumnName = 'numCampaignID'
								THEN @strSQL + '  SELECT numCampaignID AS [Key], vcCampaignName AS [Value] FROM  CampaignMaster WHERE CampaignMaster.numDomainID = ' + CONVERT(VARCHAR(10),@numDomainID)
								
								WHEN @strDBColumnName = 'numVendorID'
								THEN @strSQL + '  SELECT DM.numDivisionID AS [Key], CI.vcCompanyName AS [Value] FROM dbo.CompanyInfo CI INNER JOIN dbo.DivisionMaster DM ON DM.numCompanyID = CI.numCompanyId 
												  WHERE DM.numDomainID = ' + CONVERT(VARCHAR(10),@numDomainID)
								
								WHEN @strDBColumnName = 'numAssignedTo' OR @strDBColumnName = 'numRecOwner' 
								THEN @strSQL + '  SELECT A.numContactID AS [Key], A.vcFirstName+'' ''+A.vcLastName AS [Value] from UserMaster UM join AdditionalContactsInformation A  on UM.numUserDetailId=A.numContactID  where UM.numDomainID = A.numDomainID AND UM.numDomainID = ' + CONVERT(VARCHAR,@numDomainID)	
								
------								 SELECT A.numContactID,A.vcFirstName+' '+A.vcLastName as vcUserName from UserMaster UM         
------								 join AdditionalContactsInformation A        
------								 on UM.numUserDetailId=A.numContactID          
------								 where UM.numDomainID=@numDomainID and UM.numDomainID=A.numDomainID   
							
								WHEN @strDBColumnName = 'numTermsID'
								THEN @strSQL + 'SELECT DISTINCT numTermsID AS [Key],vcTerms AS [Value] FROM BillingTerms 
												WHERE numDomainID = ' + CONVERT(VARCHAR,@numDomainID) 
												
								WHEN @strDBColumnName = 'numShipState' OR @strDBColumnName = 'numBillState' OR @strDBColumnName = 'numState' OR @strDBColumnName = 'State'
								THEN @strSQL + '  SELECT numStateID AS [Key], vcState AS [Value] FROM State where numDomainID=' + CONVERT(VARCHAR,@numDomainID)
								
								WHEN @strDBColumnName = 'charItemType'
								THEN @strSQL + ' SELECT ''A'' AS [Key], ''asset'' AS [Value]
											     UNION 				
											     SELECT ''P'' AS [Key], ''inventory item'' AS [Value]
											     UNION 				
											     SELECT ''S'' AS [Key], ''service'' AS [Value]
											     UNION 				
											     SELECT ''N'' AS [Key], ''non-inventory item'' AS [Value]'	
								
								WHEN @strDBColumnName = 'tintRuleType'
								THEN @strSQL + ' SELECT 1 AS [Key], ''Deduct from List price'' AS [Value]
											     UNION 				
											     SELECT 2 AS [Key], ''Add to Primary Vendor Cost'' AS [Value]
											     UNION 				
											     SELECT 3 AS [Key], ''Named Price'' AS [Value]'	
											     
								WHEN @strDBColumnName = 'tintDiscountType'
								THEN @strSQL + ' SELECT 1 AS [Key], ''Percentage'' AS [Value]
											     UNION 				
											     SELECT 2 AS [Key], ''Flat Amount'' AS [Value]'												     
											     																		    
								ELSE 
									 @strSQL + ' SELECT LIST.numListItemID AS [Key],vcData AS [Value] FROM ListDetails LIST LEFT JOIN listorder LIST_ORDER ON LIST.numListItemID = LIST_ORDER.numListItemID AND LIST_ORDER.numDomainId = ' + CONVERT(VARCHAR,@numDomainID) + ' WHERE ( LIST.numDomainID = ' + CONVERT(VARCHAR,@numDomainID) + ' OR LIST.constFlag = 1) AND LIST.numListID = ' + CONVERT(VARCHAR,@numListIDValue) + ' AND  (LIST.numDomainID = ' + CONVERT(VARCHAR,@numDomainID) + ' OR constFlag = 1 )  ORDER BY intSortOrder '
						     END						
						     
			SET @vcTableNames = @vcTableNames + CONVERT(VARCHAR,@numFormFieldIDValue) + '_' + @strDBColumnName + ',' 
		END
	
	PRINT @vcTableNames
	PRINT @strSQL	
	
	SELECT @vcTableNames AS TableNames
	EXEC SP_EXECUTESQL @strSQL		
	
	DROP TABLE #tempDropDownData
	DROP TABLE #tempGroups
END
GO
/****** Object:  StoredProcedure [dbo].[USP_GET_PACKAGES]    Script Date: 30/07/2012 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO

IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_GET_PACKAGES' ) 
    DROP PROCEDURE USP_GET_PACKAGES
GO
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE USP_GET_PACKAGES
@DomainId NUMERIC(18,0)
AS 
    BEGIN
		
		SELECT
			0 AS numCustomPackageID,
			0 AS numPackageTypeID,
			'None' AS vcPackageName,
			0.00 AS fltWidth,
			0.00 AS fltHeight,
			0.00 AS fltLength,
			0.00 AS fltTotalWeight,
			0 AS numShippingCompanyID,
			'' AS vcShippingCompany,
			CONVERT(VARCHAR(10),0) + ',' + CONVERT(VARCHAR(10),0.00) +  ',' + CONVERT(VARCHAR(10),0.00) +  ',' + CONVERT(VARCHAR(10),0.00) +  ',' + CONVERT(VARCHAR(10),0.00)  AS [ValueDimension]
UNION 
		SELECT
			numItemCode AS numCustomPackageID,
			0 AS numPackageTypeID,
			vcItemName AS vcPackageName,
			fltWidth,
			fltHeight,
			fltLength,
			fltWeight AS fltTotalWeight,
			0 AS numShippingCompanyID,
			'Custom Box' AS vcShippingCompany,
			CONVERT(VARCHAR(10),0) + ',' + CONVERT(VARCHAR(10),fltWidth) +  ',' + CONVERT(VARCHAR(10),fltHeight) +  ',' + CONVERT(VARCHAR(10),fltLength) +  ',' + CONVERT(VARCHAR(10),fltWeight)  AS [ValueDimension]
		FROM 
			Item 
		where 
			bitContainer=1 AND
			numDomainID=@DomainId

        --SELECT  numCustomPackageID,
        --        numPackageTypeID,
        --        vcPackageName,
        --        fltWidth,
        --        fltHeight,
        --        fltLength,
        --        fltTotalWeight,
        --        ISNULL(numShippingCompanyID,0) AS [numShippingCompanyID],
        --        CASE WHEN numShippingCompanyID = 91 THEN 'Fedex'
        --             WHEN numShippingCompanyID = 88 THEN 'UPS'
        --             WHEN numShippingCompanyID = 90 THEN 'USPS'
        --             ELSE 'Custom Box'
        --        END AS vcShippingCompany,
        --        CONVERT(VARCHAR(10),numPackageTypeID) + ',' + CONVERT(VARCHAR(10),fltWidth) +  ',' + CONVERT(VARCHAR(10),fltHeight) +  ',' + CONVERT(VARCHAR(10),fltLength) +  ',' + CONVERT(VARCHAR(10),fltTotalWeight)  AS [ValueDimension]
        --FROM    dbo.CustomPackages CP
        --ORDER BY vcShippingCompany ASC

    END

--created by anoop jayaraj                                          
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getdomaindetails')
DROP PROCEDURE usp_getdomaindetails
GO
CREATE PROCEDURE [dbo].[USP_GetDomainDetails]                                          
@numDomainID as numeric(9)=0                                          
as           

DECLARE @listIds VARCHAR(MAX)
SELECT @listIds = COALESCE(@listIds+',' ,'') + CAST(numUserID AS VARCHAR(100)) FROM UnitPriceApprover WHERE numDomainID = @numDomainID
   
DECLARE @canChangeDiferredIncomeSelection AS BIT = 1

IF EXISTS(SELECT 
			OB.numOppBizDocsId 
		FROM 
			OpportunityBizDocs OB 
		INNER JOIN 
			OpportunityMaster OM 
		ON 
			OB.numOppId=OM.numOppId 
		WHERE 
			numBizDocId=304 
			AND OM.numDomainId=@numDomainID)
BEGIN
	SET @canChangeDiferredIncomeSelection = 0
END   
   
                               
select                                           
vcDomainName,                                           
isnull(vcDomainDesc,'') vcDomainDesc,                                           
isnull(bitExchangeIntegration,0) bitExchangeIntegration,                                           
isnull(bitAccessExchange,0) bitAccessExchange,                                           
isnull(vcExchUserName,'') vcExchUserName,                                           
isnull(vcExchPassword,'') vcExchPassword,                                           
isnull(vcExchPath,'') vcExchPath,                                           
isnull(vcExchDomain,'') vcExchDomain,                                         
tintCustomPagingRows,                                         
vcDateFormat,                                         
numDefCountry,                                         
tintComposeWindow,                                         
sintStartDate,                                         
sintNoofDaysInterval,                                         
tintAssignToCriteria,                                         
bitIntmedPage,
ISNULL(D.numCost,0) as numCost,                                         
tintFiscalStartMonth,                                    
--isnull(bitDeferredIncome,0) as bitDeferredIncome,                                  
isnull(tintPayPeriod,0) as tintPayPeriod,                                
isnull(numCurrencyID,0) as numCurrencyID,                            
isnull(vcCurrency,'') as vcCurrency,                      
isnull(charUnitSystem,'') as charUnitSystem ,                    
isnull(vcPortalLogo,'') as vcPortalLogo,                  
isnull(tintChrForComSearch,2) as tintChrForComSearch,                
isnull(tintChrForItemSearch,'') as tintChrForItemSearch,            
isnull(intPaymentGateWay,0) as intPaymentGateWay  ,        
 case when bitPSMTPServer= 1 then vcPSMTPServer else '' end as vcPSMTPServer  ,              
 case when bitPSMTPServer= 1 then  isnull(numPSMTPPort,0)  else 0 end as numPSMTPPort          
,isnull(bitPSMTPAuth,0) bitPSMTPAuth        
,isnull([vcPSmtpPassword],'')vcPSmtpPassword        
,isnull(bitPSMTPSSL,0) bitPSMTPSSL     
,isnull(bitPSMTPServer,0) bitPSMTPServer  ,isnull(vcPSMTPUserName,'') vcPSMTPUserName,      
--isnull(numDefaultCategory,0) numDefaultCategory,
isnull(numShipCompany,0)   numShipCompany,
isnull(bitAutoPopulateAddress,0) bitAutoPopulateAddress,
isnull(tintPoulateAddressTo,0) tintPoulateAddressTo,
isnull(bitMultiCompany,0) bitMultiCompany,
isnull(bitMultiCurrency,0) bitMultiCurrency,
ISNULL(bitCreateInvoice,0) bitCreateInvoice,
ISNULL([numDefaultSalesBizDocId],0) numDefaultSalesBizDocId,
ISNULL([bitSaveCreditCardInfo],0) bitSaveCreditCardInfo,
ISNULL([intLastViewedRecord],10) intLastViewedRecord,
ISNULL([tintCommissionType],1) tintCommissionType,
ISNULL([tintBaseTax],2) tintBaseTax,
ISNULL([numDefaultPurchaseBizDocId],0) numDefaultPurchaseBizDocId,
ISNULL(bitEmbeddedCost,0) bitEmbeddedCost,
ISNULL(numDefaultSalesOppBizDocId,0) numDefaultSalesOppBizDocId,
ISNULL(tintSessionTimeOut,1) tintSessionTimeOut,
ISNULL(tintDecimalPoints,2) tintDecimalPoints,
ISNULL(bitCustomizePortal,0) bitCustomizePortal,
ISNULL(tintBillToForPO,0) tintBillToForPO,
ISNULL(tintShipToForPO,0) tintShipToForPO,
ISNULL(tintOrganizationSearchCriteria,1) tintOrganizationSearchCriteria,
ISNULL(tintLogin,0) tintLogin,
lower(ISNULL(vcPortalName,'')) vcPortalName,
ISNULL(bitDocumentRepositary,0) bitDocumentRepositary,
D.numDomainID,
ISNULL(vcPSMTPDisplayName,'') vcPSMTPDisplayName,
ISNULL(bitGtoBContact,0) bitGtoBContact,
ISNULL(bitBtoGContact,0) bitBtoGContact,
ISNULL(bitGtoBCalendar,0) bitGtoBCalendar,
ISNULL(bitBtoGCalendar,0) bitBtoGCalendar,
ISNULL(bitExpenseNonInventoryItem,0) bitExpenseNonInventoryItem,
ISNULL(bitInlineEdit,0) bitInlineEdit,
ISNULL(bitRemoveVendorPOValidation,0) bitRemoveVendorPOValidation,
isnull(D.tintBaseTaxOnArea,0) tintBaseTaxOnArea,
ISNULL(D.bitAmountPastDue,0) bitAmountPastDue,
CONVERT(DECIMAL(18,2),ISNULL(D.monAmountPastDue,0)) monAmountPastDue,
ISNULL(bitSearchOrderCustomerHistory,0) bitSearchOrderCustomerHistory,
isnull(D.bitRentalItem,0) as bitRentalItem,
isnull(D.numRentalItemClass,0) as numRentalItemClass,
isnull(D.numRentalHourlyUOM,0) as numRentalHourlyUOM,
isnull(D.numRentalDailyUOM,0) as numRentalDailyUOM,
isnull(D.tintRentalPriceBasedOn,0) as tintRentalPriceBasedOn,
isnull(D.tintCalendarTimeFormat,24) as tintCalendarTimeFormat,
isnull(D.tintDefaultClassType,0) as tintDefaultClassType,
ISNULL(D.tintPriceBookDiscount,0) tintPriceBookDiscount,
ISNULL(D.bitAutoSerialNoAssign,0) as bitAutoSerialNoAssign,
ISNULL(D.bitIsShowBalance,0) AS [bitIsShowBalance],
ISNULL(numIncomeAccID,0) AS [numIncomeAccID],
ISNULL(numCOGSAccID,0) AS [numCOGSAccID],
ISNULL(numAssetAccID,0) AS [numAssetAccID],
ISNULL(IsEnableProjectTracking,0) AS [IsEnableProjectTracking],
ISNULL(IsEnableCampaignTracking,0) AS [IsEnableCampaignTracking],
ISNULL(IsEnableResourceScheduling,0) AS [IsEnableResourceScheduling],
ISNULL(numShippingServiceItemID,0) AS [numShippingServiceItemID],
ISNULL(D.numSOBizDocStatus,0) AS numSOBizDocStatus,
ISNULL(D.bitAutolinkUnappliedPayment,0) AS bitAutolinkUnappliedPayment,
ISNULL(D.numDiscountServiceItemID,0) AS numDiscountServiceItemID,
ISNULL(D.vcShipToPhoneNo,0) AS vcShipToPhoneNo,
ISNULL(D.vcGAUserEMail,'') AS vcGAUserEMail,
ISNULL(D.vcGAUserPassword,'') AS vcGAUserPassword,
ISNULL(D.vcGAUserProfileId,0) AS vcGAUserProfileId,
ISNULL(D.bitDiscountOnUnitPrice,0) AS bitDiscountOnUnitPrice,
ISNULL(D.intShippingImageWidth,0) AS intShippingImageWidth ,
ISNULL(D.intShippingImageHeight,0) AS intShippingImageHeight,
ISNULL(D.numTotalInsuredValue,0) AS numTotalInsuredValue,
ISNULL(D.numTotalCustomsValue,0) AS numTotalCustomsValue,
ISNULL(D.vcHideTabs,'') AS vcHideTabs,
ISNULL(D.bitUseBizdocAmount,0) AS bitUseBizdocAmount,
ISNULL(D.bitDefaultRateType,0) AS bitDefaultRateType,
ISNULL(D.bitIncludeTaxAndShippingInCommission,0) AS bitIncludeTaxAndShippingInCommission,
/*,ISNULL(D.bitAllowPPVariance,0) AS bitAllowPPVariance*/
ISNULL(D.bitPurchaseTaxCredit,0) AS bitPurchaseTaxCredit,
ISNULL(D.bitLandedCost,0) AS bitLandedCost,
ISNULL(D.vcLanedCostDefault,'') AS vcLanedCostDefault,
ISNULL(D.bitMinUnitPriceRule,0) AS bitMinUnitPriceRule,
ISNULL(D.numAbovePercent,0) AS numAbovePercent,
ISNULL(D.numBelowPercent,0) AS numBelowPercent,
ISNULL(D.numBelowPriceField,0) AS numBelowPriceField,
@listIds AS vcUnitPriceApprover,
ISNULL(D.numDefaultSalesPricing,2) AS numDefaultSalesPricing,
ISNULL(D.bitReOrderPoint,0) AS bitReOrderPoint,
ISNULL(D.numReOrderPointOrderStatus,0) AS numReOrderPointOrderStatus,
ISNULL(D.numPODropShipBizDoc,0) AS numPODropShipBizDoc,
ISNULL(D.numPODropShipBizDocTemplate,0) AS numPODropShipBizDocTemplate,
ISNULL(D.IsEnableDeferredIncome,0) AS IsEnableDeferredIncome,
ISNULL(D.bitApprovalforTImeExpense,0) AS bitApprovalforTImeExpense,
ISNULL(D.intTimeExpApprovalProcess,0) AS intTimeExpApprovalProcess,
ISNULL(D.bitApprovalforOpportunity,0) AS bitApprovalforOpportunity,
ISNULL(D.intOpportunityApprovalProcess,0) AS intOpportunityApprovalProcess,
@canChangeDiferredIncomeSelection AS canChangeDiferredIncomeSelection,
ISNULL(CAST(D.numDefaultSalesShippingDoc AS int),0) AS numDefaultSalesShippingDoc,
ISNULL(CAST(D.numDefaultPurchaseShippingDoc AS int),0) AS numDefaultPurchaseShippingDoc,
ISNULL(D.bitchkOverRideAssignto,0) AS bitchkOverRideAssignto,
ISNULL(D.vcPrinterIPAddress,'') vcPrinterIPAddress,
ISNULL(D.vcPrinterPort,'') vcPrinterPort,
ISNULL(bitCostApproval,0) bitCostApproval
,ISNULL(bitListPriceApproval,0) bitListPriceApproval
,ISNULL(bitMarginPriceViolated,0) bitMarginPriceViolated
,ISNULL(vcSalesOrderTabs,'Sales Orders') AS vcSalesOrderTabs
,ISNULL(vcSalesQuotesTabs,'Sales Quotes') AS vcSalesQuotesTabs
,ISNULL(vcItemPurchaseHistoryTabs,'Item Purchase History') AS vcItemPurchaseHistoryTabs
,ISNULL(vcItemsFrequentlyPurchasedTabs,'Items Frequently Purchased') AS vcItemsFrequentlyPurchasedTabs
,ISNULL(vcOpenCasesTabs,'Open Cases') AS vcOpenCasesTabs
,ISNULL(vcOpenRMATabs,'Open RMAs') AS vcOpenRMATabs
,ISNULL(bitSalesOrderTabs,0) AS bitSalesOrderTabs
,ISNULL(bitSalesQuotesTabs,0) AS bitSalesQuotesTabs
,ISNULL(bitItemPurchaseHistoryTabs,0) AS bitItemPurchaseHistoryTabs
,ISNULL(bitItemsFrequentlyPurchasedTabs,0) AS bitItemsFrequentlyPurchasedTabs
,ISNULL(bitOpenCasesTabs,0) AS bitOpenCasesTabs
,ISNULL(bitOpenRMATabs,0) AS bitOpenRMATabs
,ISNULL(bitSupportTabs,0) AS bitSupportTabs
,ISNULL(vcSupportTabs,'Support') AS vcSupportTabs

,ISNULL(numDefaultSiteID,0) AS numDefaultSiteID
,ISNULL(tintOppStautsForAutoPOBackOrder,0) AS tintOppStautsForAutoPOBackOrder
,ISNULL(tintUnitsRecommendationForAutoPOBackOrder,1) AS tintUnitsRecommendationForAutoPOBackOrder
,ISNULL(bitRemoveGlobalLocation,0) AS bitRemoveGlobalLocation
from Domain D  
left join eCommerceDTL eComm  
on eComm.numDomainID=D.numDomainID  
where (D.numDomainID=@numDomainID OR @numDomainID=-1)


 --SELECT * FROM  RecImportConfg WHERE ImportType=4 

 --SELECT * FROM  dbo.Item 
GO
 IF EXISTS ( SELECT *
             FROM   sysobjects
             WHERE  xtype = 'p'
                    AND NAME = 'USP_GetItemForUpdate' ) 
    DROP PROCEDURE USP_GetItemForUpdate
GO
-- exec USP_GetItemForUpdate 1 
 CREATE PROCEDURE USP_GetItemForUpdate
    (
      @numDomainID NUMERIC(9) = 0
    )
 AS 
    BEGIN
 
 
   
------------Declaration---------------------  
        DECLARE @strSql VARCHAR(MAX),
            @numMaxFieldId NUMERIC(9),
            @from NVARCHAR(MAX),
            @Where NVARCHAR(MAX),
            @OrderBy NVARCHAR(MAX),
            @GroupBy NVARCHAR(MAX),
            @SelectFields NVARCHAR(MAX),
            @InneJoinOn NVARCHAR(MAX)
         
        DECLARE @tintOrder AS TINYINT,
            @vcFieldName AS VARCHAR(300),
            @vcListItemType AS VARCHAR(1),
            @vcListItemType1 AS VARCHAR(1),
            @vcAssociatedControlType VARCHAR(10),
            @numListID AS NUMERIC(9),
            @vcDbColumnName VARCHAR(30),
            @vcLookBackTableName VARCHAR(20),
            @WCondition VARCHAR(1000),
            @bitCustom BIT,
            @fld_id NUMERIC(9)
        SET @InneJoinOn = ''
        SET @OrderBy = ' Order by I.numItemCode '
        SET @Where = ''
        SET @from = ' FROM item I '
        SET @GroupBy = ''
        SET @SelectFields = ''  
        SET @WCondition = ''



        SET @Where = @Where
            + ' WHERE I.charItemType <>''A'' and I.numDomainID='
            + CONVERT(VARCHAR(15), @numDomainID)
	


--SELECT * FROM dbo.View_DynamicColumns WHERE numDomainID = 72 AND numFormID = 20
--- Get Fields setting for Update item for given domain
        SELECT  X.*,
                ROW_NUMBER() OVER ( ORDER BY numFieldID ASC ) AS tintColumn
        INTO    #FielList
        FROM    ( SELECT    vcDbColumnName,
                            vcFieldName,
                            vcAssociatedControlType,
                            vcListItemType,
                            numListID,
                            vcLookBackTableName,
                            bitCustom,
                            numFieldID
                  FROM      dbo.View_DynamicColumns
                  WHERE     numFormId = 20
                            AND numDomainID = @numDomainID
                            AND bitImport = 1
                            AND numFieldID NOT IN (
                            SELECT  numFieldID
                            FROM    dbo.DycFieldMaster
                            WHERE   vcDbColumnName = 'numItemCode'
                                    AND vcLookBackTableName = 'Item' )
                  UNION ALL
                  SELECT    vcDbColumnName,
                            vcFieldName,
                            vcAssociatedControlType,
                            '' vcListItemType,
                            numListID,
                            '' vcLookBackTableName,
                            bitCustom,
                            numFieldID
                  FROM      dbo.View_DynamicCustomColumns
                  WHERE     numDomainID = @numDomainID
                            AND grp_id = 5
                            AND numFormID = 20
                ) X
        ORDER BY tintColumn ASC
--        SELECT  *
--        FROM    #FielList
		
		CREATE TABLE #tempPrice
		(
			RowID			NUMERIC(18,0),
			numItemCode		NUMERIC(18,0),
			PriceLevel		VARCHAR(MAX)
		)
		IF EXISTS(SELECT * FROM #FielList WHERE vcFieldName IN ('Price Level','Price Rule Type','Price Level Discount Type'))
			BEGIN
--				CREATE TABLE #tempPrice
--				(
--					RowID			NUMERIC(18,0),
--					numItemCode		NUMERIC(18,0),
--					PriceLevel		VARCHAR(MAX)
--				)

				INSERT INTO #tempPrice (RowID,numItemCode,PriceLevel)				
				SELECT ROW_NUMBER() OVER (ORDER BY numItemCode) AS RowID, * FROM 
				(
					SELECT I.numItemCode, '' AS [PriceLevel] 
					FROM dbo.Item I
					JOIN dbo.PricingTable PT ON I.numItemCode = PT.numItemCode
					WHERE numDomainID = @numDomainID
				) AS [TABLE1]
				
				DECLARE @intCnt AS INT
				DECLARE @Count AS INT
				DECLARE @numItemCode AS NUMERIC(18,0)
				DECLARE @strValues AS VARCHAR(MAX)
				SET @intCnt = 0
				SELECT @Count = COUNT(*) FROM #tempPrice 

				--SELECT * FROM #tempPrice
				WHILE(@Count > @intCnt)
				BEGIN
					SET @intCnt = @intCnt + 1
					SELECT @numItemCode = numItemCode FROM #tempPrice WHERE RowID = @intCnt
					PRINT @numItemCode
					
					SELECT @strValues = COALESCE(@strValues + ';' + CAST(intFromQty AS VARCHAR(10)) + '-' + CAST(intToQty AS VARCHAR(10)) + ':' + CAST(decDiscount AS VARCHAR(10)), '')  
					FROM dbo.PricingTable WHERE numItemCode = @numItemCode
					PRINT @strValues
					
					IF (LEN(@strValues) > 0)
					BEGIN
						SET @strValues = RIGHT(@strValues, LEN(@strValues) - 1)	
					END
					ELSE
					BEGIN
						SET @strValues = RIGHT(@strValues, 0)
					END
					
					PRINT @strValues
					
					UPDATE #tempPrice SET PriceLevel = ISNULL(@strValues,'') 
					WHERE numItemCode = @numItemCode
					
					SET @strValues = ''
				END								
			END
	
        SET @tintOrder = 0

        SET @strSql = 'SELECT DISTINCT I.numItemCode [Item ID] '

        SELECT TOP 1
                @tintOrder = tintColumn + 1,
                @vcDbColumnName = vcDbColumnName,
                @vcFieldName = vcFieldName,
                @vcAssociatedControlType = vcAssociatedControlType,
                @vcListItemType = vcListItemType,
                @numListID = numListID,
                @vcLookBackTableName = vcLookBackTableName,
                @bitCustom = bitCustom,
                @fld_id = numFieldId
        FROM    #FielList
        ORDER BY tintColumn ASC



        WHILE @tintOrder > 0
            BEGIN
                PRINT @bitCustom
                PRINT @vcAssociatedControlType
                PRINT @vcLookBackTableName
                PRINT @vcDbColumnName
                PRINT @vcListItemType
                IF ISNULL(@bitCustom, 0) = 0 
                    BEGIN

                        IF @vcAssociatedControlType = 'SelectBox' 
                            BEGIN  
                               
                                IF @vcLookBackTableName = 'Warehouses' 
                                    BEGIN
                                        SET @SelectFields = @SelectFields
                                            + ' ,W.vcWarehouse ['
                                            + @vcFieldName + ']'
                                        

										IF CHARINDEX('dbo.WareHouseItems WI',@InneJoinOn) = 0 
                                            SET @InneJoinOn = @InneJoinOn + ' Left JOIN dbo.WareHouseItems WI ON WI.numItemID = I.numItemCode and WI.numDomainID = I.numDomainID '     
                                        
										IF CHARINDEX('dbo.WareHouseItmsDTL',@InneJoinOn) = 0
										BEGIN 
											IF CHARINDEX('dbo.WareHouseItems WI',@InneJoinOn) = 0 
											BEGIN
												SET @InneJoinOn = @InneJoinOn + ' Left JOIN dbo.WareHouseItems WI ON WI.numItemID = I.numItemCode and WI.numDomainID = I.numDomainID '              
											END
	                                            
											SET @InneJoinOn = @InneJoinOn + '  Left JOIN dbo.WareHouseItmsDTL WID ON WID.numWareHouseItemID = WI.numWareHouseItemID  '
                                        END 

                                        IF CHARINDEX('dbo.WareHouses W', @InneJoinOn) = 0 
                                            SET @InneJoinOn = @InneJoinOn + ' Left JOIN dbo.WareHouses W ON W.numWareHouseID = WI.numWareHouseID  '
                                                                                          
     
                                        IF CHARINDEX('#tempPrice TP',@InneJoinOn) = 0 
                                            SET @InneJoinOn = @InneJoinOn + ' LEFT JOIN #tempPrice TP ON TP.numItemCode = I.numItemCode'        
                                                        
                                    END   
                                ELSE 
									IF @vcLookBackTableName = 'WareHouseItems' 
										BEGIN
--											PRINT 'TESTED1'
--											SET @SelectFields = @SelectFields + ' ,WI.' + @vcDbColumnName + ' [' + @vcFieldName + ']'
--											IF CHARINDEX('dbo.WareHouseItems WI',@InneJoinOn) = 0 
--												SET @InneJoinOn = @InneJoinOn + ' Left JOIN dbo.WareHouseItems WI ON WI.numItemID = I.numItemCode and WI.numDomainID = I.numDomainID'
											
											IF @vcDbColumnName = 'vcLocation'
											BEGIN
												--PRINT 'TESTED2'
													SET @SelectFields = @SelectFields + ' ,WL.' + @vcDbColumnName + ' [' + @vcFieldName + ']'
													IF CHARINDEX('WarehouseLocation',@InneJoinOn) = 0 
														IF CHARINDEX('dbo.WareHouseItems WI',@InneJoinOn) = 0 
														BEGIN
															SET @InneJoinOn = @InneJoinOn + ' Left JOIN dbo.WareHouseItems WI ON WI.numItemID = I.numItemCode and WI.numDomainID = I.numDomainID '              
														END
														
														SET @InneJoinOn = @InneJoinOn + ' Left JOIN dbo.WarehouseLocation WL ON WI.numWLocationID = WL.numWLocationID and WI.numDomainID = WL.numDomainID '      
												END	
										END                                                    
											
                                    IF @vcLookBackTableName = 'CompanyAssets' 
                                        BEGIN
		 	--SET @SelectFields = @SelectFields + ' ,CA.'+ @vcDbColumnName +' ['+ @vcFieldName+']'
                                            SET @SelectFields = @SelectFields
                                                + ' ,dbo.fn_GetComapnyName(CA.numDivId) ['
                                                + @vcFieldName + ']'
                                            IF CHARINDEX('dbo.CompanyAssets CA',
                                                         @InneJoinOn) = 0 
                                                SET @InneJoinOn = @InneJoinOn
                                                    + 'LEFT JOIN  dbo.CompanyAssets CA  ON I.numItemCode = CA.numItemCode AND I.numDomainID = CA.numDomainId '
                                        END                              
                                    ELSE 
                                        IF @vcLookBackTableName = 'Category' 
                                            BEGIN
                                                SET @SelectFields = @SelectFields
                                                    + ' ,C.vcCategoryName ['
                                                    + @vcFieldName + ']'
                                                IF CHARINDEX('dbo.Category C',
                                                             @InneJoinOn) = 0 
                                                    SET @InneJoinOn = @InneJoinOn
                                                        + ' Left JOIN ItemCategory IC on IC.numItemID = I.numItemCode LEFT JOIN dbo.Category C ON C.numCategoryID = IC.numCategoryID  '
                                            END
                                        ELSE 
                                            IF @vcLookBackTableName = 'chart_of_accounts' 
                                                BEGIN
                                                    IF @vcDbColumnName = 'numCOGsChartAcntId' 
                                                        SET @SelectFields = @SelectFields
                                                            + ' ,(select vcAccountName from chart_of_Accounts COA WHERE COA.numAccountId=numCOGsChartAcntId) as  ['
                                                            + @vcFieldName
                                                            + ']'
                                                    ELSE 
                                                        IF @vcDbColumnName = 'numAssetChartAcntId' 
                                                            SET @SelectFields = @SelectFields + ' ,(select vcAccountName from chart_of_Accounts COA WHERE COA.numAccountId=numAssetChartAcntId) as  [' + @vcFieldName + ']'
                                                        ELSE 
                                                            IF @vcDbColumnName = 'numIncomeChartAcntId' 
                                                                SET @SelectFields = @SelectFields + ' ,(select vcAccountName from chart_of_Accounts COA where COA.numAccountId=numIncomeChartAcntId) as  [' + @vcFieldName + ']'

                                                END 
                                            ELSE 
                                                IF @vcLookBackTableName = 'DivisionMaster' 
                                                    BEGIN
                                                        SET @SelectFields = @SelectFields
                                                            + ' ,dbo.fn_GetComapnyName(V.numVendorID) ['
                                                            + @vcFieldName
                                                            + ']'
                                                        IF CHARINDEX('dbo.Vendor', @InneJoinOn) = 0 
                                                            SET @InneJoinOn = @InneJoinOn + ' LEFT JOIN dbo.Vendor V ON V.numItemCode = I.numItemCode AND I.numDomainID = V.numDomainID '
                                                    END 
                                                ELSE 
                                                    IF @vcLookBackTableName = 'Vendor' 
                                                        BEGIN
                                                            SET @SelectFields = @SelectFields + ' ,dbo.fn_GetComapnyName(V.numVendorID) [' + @vcFieldName + ']'
                                                            IF CHARINDEX('dbo.Vendor', @InneJoinOn) = 0 
                                                                SET @InneJoinOn = @InneJoinOn + ' LEFT JOIN dbo.Vendor V ON V.numItemCode = I.numItemCode AND I.numDomainID = V.numDomainID '
                                                        END
                                                    ELSE    
                                                    IF @vcLookBackTableName = 'PricingTable' 
                                                        BEGIN
															IF @vcFieldName = 'Price Rule Type'
																BEGIN
																	SET @SelectFields = @SelectFields + '  ,TP.PriceLevel AS [Price Level]'		
																END
                                                            
                                                            IF @vcFieldName = 'Price Rule Type'
																BEGIN
																	SET @SelectFields = @SelectFields + '  , (CASE WHEN (SELECT DISTINCT tintRuleType FROM dbo.PricingTable WHERE numItemCode = TP.numItemCode ) = 1
																												  THEN ''Deduct From List Price''
																												  WHEN (SELECT DISTINCT tintRuleType FROM dbo.PricingTable WHERE numItemCode = TP.numItemCode ) = 2 
																												  THEN ''Add to Primary Vendor Cost''
																												  WHEN (SELECT DISTINCT tintRuleType FROM dbo.PricingTable WHERE numItemCode = TP.numItemCode ) = 3 
																												  THEN ''Named Price''
																												  ELSE ''''
																										      END) AS [Price Rule Type] '
																END
															IF @vcFieldName = 'Price Level Discount Type'
																BEGIN
																	SET @SelectFields = @SelectFields + '  ,(CASE WHEN (SELECT DISTINCT tintDiscountType FROM dbo.PricingTable WHERE numItemCode = TP.numItemCode ) = 1
																												  THEN ''Percentage''
																												  WHEN (SELECT DISTINCT tintDiscountType FROM dbo.PricingTable WHERE numItemCode = TP.numItemCode ) = 2 
																												  THEN ''Flat Amount''
																												  ELSE ''''
																										      END) AS [Price Level Discount Type] '
																END
															
															SET @SelectFields = REPLACE(@SelectFields ,',I.vcPriceLevelDetail [Price Level]','');	
															
                                                            IF CHARINDEX('#tempPrice', @InneJoinOn) = 0 
                                                                SET @InneJoinOn = @InneJoinOn + '  LEFT JOIN #tempPrice TP ON TP.numItemCode = I.numItemCode '
                                                        END               
                                                    ELSE 
                                                        IF @vcListItemType = 'I' --Item group
                                                            BEGIN                              
                                                                SET @SelectFields = @SelectFields + ',IG.vcItemGroup [' + @vcFieldName + ']'                              
                                                                SET @InneJoinOn = @InneJoinOn + ' LEFT JOIN ItemGroups IG ON I.numDomainID = IG.numDomainID AND I.numItemGroup = IG.numItemGroupID '
			   
                                                            END
                                                        ELSE 
                                                            IF @vcListItemType = 'P'--Item Type
                                                                BEGIN
                                                                    SET @SelectFields = @SelectFields + ', CASE I.charItemType WHEN ''p'' THEN ''Inventory Item'' WHEN ''n'' THEN ''Non-Inventory Item'' WHEN ''s'' THEN ''Service'' ELSE '''' END   [' + @vcFieldName + ']'                              
                                                                END
                                                            ELSE 
                                                                IF @vcListItemType = 'L'--Any List Item
                                                                    BEGIN
                                                                        SET @SelectFields = @SelectFields + ',L' + CONVERT(VARCHAR(3), @tintOrder) + '.vcData' + ' [' + @vcFieldName + ']'                              
                                                                        SET @InneJoinOn = @InneJoinOn + ' left Join ListDetails L' + CONVERT(VARCHAR(3), @tintOrder) + ' on L' + CONVERT(VARCHAR(3), @tintOrder) + '.numListItemID=' + @vcDbColumnName                              
                                                                    END 
                                                                ELSE 
                                                                    IF @vcListItemType = 'U' 
                                                                        BEGIN
                                                                            IF @vcDbColumnName = 'numBaseUnit' 
                                                                                SET @SelectFields = @SelectFields + ' ,(select vcUnitName from UOM where numUOMId=ISNULL(I.numBaseUnit,0)) as  [' + @vcFieldName + ']'
                                                                            ELSE 
                                                                                IF @vcDbColumnName = 'numPurchaseUnit' 
                                                                                    SET @SelectFields = @SelectFields + ' ,(select vcUnitName from UOM where numUOMId=ISNULL(I.numPurchaseUnit,0)) as  [' + @vcFieldName + ']'
                                                                                ELSE 
                                                                                    IF @vcDbColumnName = 'numSaleUnit' 
                                                                                        SET @SelectFields = @SelectFields + ' ,(select vcUnitName from UOM where numUOMId=ISNULL(I.numSaleUnit,0)) as  [' + @vcFieldName + ']'
	          
                                                                        END
				
			
                            END
                        ELSE 
                            BEGIN
                                IF @vcLookBackTableName = 'WareHouseItmsDTL' 
                                    BEGIN
                                        SET @SelectFields = @SelectFields + ' ,WID.' + @vcDbColumnName + ' [' + @vcFieldName + ']'
                                            
                                        IF CHARINDEX('dbo.WareHouseItmsDTL',@InneJoinOn) = 0
											BEGIN 
												IF CHARINDEX('dbo.WareHouseItems WI',@InneJoinOn) = 0 
													BEGIN
														SET @InneJoinOn = @InneJoinOn + ' Left JOIN dbo.WareHouseItems WI ON WI.numItemID = I.numItemCode and WI.numDomainID = I.numDomainID '              
													END 
	                                            
												SET @InneJoinOn = @InneJoinOn + '  Left JOIN dbo.WareHouseItmsDTL WID ON WI.numWarehouseItemID = WI.numWarehouseItemID'
                                            END
                                    END
								
								ELSE IF @vcLookBackTableName = 'WareHouseItems' 
                                    BEGIN
                                        SET @SelectFields = @SelectFields + ' ,WI.' + @vcDbColumnName + ' [' + @vcFieldName + ']'
                                        IF CHARINDEX('dbo.WareHouseItems WI',@InneJoinOn) = 0 
                                            SET @InneJoinOn = @InneJoinOn + ' Left JOIN dbo.WareHouseItems WI ON WI.numItemID = I.numItemCode and WI.numDomainID = I.numDomainID'
                                    END
                                    
                                ELSE 
                                    IF @vcLookBackTableName = 'CompanyAssets' 
                                        BEGIN
                                            SET @SelectFields = @SelectFields
                                                + ' ,CA.' + @vcDbColumnName
                                                + ' [' + @vcFieldName + ']'
                                            IF CHARINDEX('dbo.CompanyAssets CA',
                                                         @InneJoinOn) = 0 
                                                SET @InneJoinOn = @InneJoinOn
                                                    + 'LEFT JOIN  dbo.CompanyAssets CA  ON I.numItemCode = CA.numItemCode AND I.numDomainID = CA.numDomainId '
                                        END 
	
                                    ELSE 
                                        IF @vcLookBackTableName = 'Vendor' 
                                            BEGIN
                                                SET @SelectFields = @SelectFields
                                                    + ' ,V.' + @vcDbColumnName
                                                    + ' [' + @vcFieldName
                                                    + ']'
                                                IF CHARINDEX('dbo.Vendor',
                                                             @InneJoinOn) = 0 
                                                    SET @InneJoinOn = @InneJoinOn
                                                        + ' LEFT JOIN dbo.Vendor V ON V.numItemCode = I.numItemCode AND I.numDomainID = V.numDomainID '
                                                      
                                            END      
                                        ELSE 
                                            IF @vcDbColumnName = 'vcSerialNo' 
                                                BEGIN
--			SET @InneJoinOn= @InneJoinOn +'LEFT JOIN dbo.WareHouseItems WI ON I.numItemCode = WI.numItemID AND I.numDomainID = WI.numDomainID
--			LEFT JOIN dbo.WareHouseItmsDTL WD ON WI.numWareHouseItemID = WD.numWareHouseItemID'	
                                                    SET @SelectFields = @SelectFields
                                                        + ' ,0 ['
                                                        + @vcFieldName + ']'
                                                    IF CHARINDEX('dbo.WareHouseItems WI', @InneJoinOn) = 0 
                                                        SET @InneJoinOn = @InneJoinOn
                                                            + ' LEFT JOIN dbo.WareHouseItems WI ON WI.numItemID = I.numItemCode '
                                                END
                                            ELSE 
                                                IF @vcDbColumnName = 'vcPartNo' 
                                                    BEGIN
                                                        SET @SelectFields = @SelectFields
                                                            + ' ,0 ['
                                                            + @vcFieldName
                                                            + ']'
                                                    END
                                                ELSE 
                                                    IF @vcDbColumnName = 'charItemType' 
                                                        BEGIN
                                                            SET @SelectFields = @SelectFields + ' ,case when i.charItemType=''P'' then ''Inventory Item'' when i.charItemType=''S'' then ''Service'' when i.charItemType=''A'' then ''Asset Item'' when i.charItemType=''N'' then ''Non-Inventory Item'' ELSE ''Non-Inventory Item'' END  [' + @vcFieldName + ']'
                                                        END
                                                    ELSE 
                                                        IF @vcDbColumnName = 'txtDesc' 
                                                            BEGIN
                                                                SET @SelectFields = @SelectFields + ' ,CAST(IED.txtDesc AS NVARCHAR(4000)) [' + @vcFieldName + ']'
                                                                SET @InneJoinOn = @InneJoinOn + ' left JOIN dbo.ItemExtendedDetails IED ON IED.numItemCode = I.numItemCode '
                                                            END
														Else IF @vcDbColumnName = 'txtShortDesc' 
                                                            BEGIN
                                                                SET @SelectFields = @SelectFields + ' ,CAST(IED1.txtShortDesc AS NVARCHAR(4000)) [' + @vcFieldName + ']'
                                                                SET @InneJoinOn = @InneJoinOn + ' left JOIN dbo.ItemExtendedDetails IED1 ON IED1.numItemCode = I.numItemCode '
                                                            END
                                                        ELSE 
                                                            IF @vcDbColumnName = 'bitAllowBackOrder'
                                                                OR @vcDbColumnName = 'bitTaxable'
                                                                OR @vcDbColumnName = 'IsArchieve' 
                                                                BEGIN
                                                                    SET @SelectFields = @SelectFields + ' ,Case  I.' + @vcDbColumnName + ' when 1 then ''TRUE'' ELSE ''FALSE'' END [' + @vcFieldName + ']'
                                                                END
                                                            ELSE 
                                                                BEGIN
                                                                    SET @SelectFields = @SelectFields + ' ,I.' + @vcDbColumnName + ' [' + @vcFieldName + ']'
                                                                END
		   
                            END
                    END
                ELSE 
                    IF @bitCustom = 1 
                        BEGIN
		/*--------------Custom field logic-----------*/
		
                            PRINT @tintOrder
                            PRINT @vcAssociatedControlType
                            PRINT @vcFieldName

                            IF @vcAssociatedControlType = 'Drop Down' 
                                BEGIN
                                    SET @SelectFields = @SelectFields + ',L'
                                        + CONVERT(VARCHAR(5), @tintOrder)
                                        + '.vcData [' + @vcFieldName + ']'
                                    SET @InneJoinOn = @InneJoinOn
                                        + ' left join dbo.CFW_FLD_Values_Item CFV'
                                        + CONVERT(VARCHAR(5), @tintOrder)
                                        + ' ON CFV'
                                        + CONVERT(VARCHAR(5), @tintOrder)
                                        + '.RecId = I.numItemCode and CFV'
                                        + CONVERT(VARCHAR(5), @tintOrder)
                                        + '.Fld_Id = '
                                        + CONVERT(VARCHAR(10), @fld_id)
                                        + ' left Join ListDetails L'
                                        + CONVERT(VARCHAR(3), @tintOrder)
                                        + ' on L'
                                        + CONVERT(VARCHAR(3), @tintOrder)
                                        + '.numListItemID = (CASE WHEN ISNUMERIC(CFV'
                                        + CONVERT(VARCHAR(3), @tintOrder)
                                        + '.Fld_Value)=1 THEN CAST(CFV'
                                        + CONVERT(VARCHAR(3), @tintOrder)
                                        + '.Fld_Value AS NUMERIC(9)) ELSE 0 END)'
							--+' left Join ListDetails L'+ convert(varchar(3),@tintOrder)+ ' on L'+convert(varchar(3),@tintOrder)+ '.numListItemID=CFV'+ convert(varchar(3),@tintOrder)+'.Fld_Value'
							--(CASE WHEN ISNUMERIC(CFV19.Fld_Value)=1 THEN CAST(CFV19.Fld_Value AS NUMERIC(9)) ELSE 0 END)
                                END
                            ELSE 
                                BEGIN
                                    SET @SelectFields = @SelectFields + ',CFV'
                                        + CONVERT(VARCHAR(5), @tintOrder)
                                        + '.Fld_Value [' + @vcFieldName + ']'
                                    SET @InneJoinOn = @InneJoinOn
                                        + ' left join dbo.CFW_FLD_Values_Item CFV'
                                        + CONVERT(VARCHAR(5), @tintOrder)
                                        + ' ON CFV'
                                        + CONVERT(VARCHAR(5), @tintOrder)
                                        + '.RecId = I.numItemCode and CFV'
                                        + CONVERT(VARCHAR(5), @tintOrder)
                                        + '.Fld_Id = '
                                        + CONVERT(VARCHAR(10), @fld_id)
                                END


		/*--------------Custom field ends-----------*/     	
                        END
  
  

	  
	  
                IF CHARINDEX('WareHouseItmsDTL', @Where) > 0 
                    BEGIN
                        IF CHARINDEX('dbo.WareHouseItems WI', @InneJoinOn) = 0 
                            SET @InneJoinOn = @InneJoinOn + ' LEFT JOIN dbo.WareHouseItems WI ON WI.numItemID = I.numItemCode '
                    END
  
  SELECT TOP 1
                        @tintOrder = tintColumn + 1,
                        @vcDbColumnName = vcDbColumnName,
                        @vcFieldName = vcFieldName,
                        @vcAssociatedControlType = vcAssociatedControlType,
                        @vcListItemType = vcListItemType,
                        @numListID = numListID,
                        @vcLookBackTableName = vcLookBackTableName,
                        @bitCustom = bitCustom,
                        @fld_id = numFieldId
                FROM    #FielList
                WHERE   tintColumn > @tintOrder - 1
                ORDER BY tintColumn ASC
                IF @@rowcount = 0 
                    SET @tintOrder = 0                              
            END                              

	
        IF CHARINDEX('V.bitIsPrimaryVendor', @SelectFields) > 0 
            BEGIN
                SET @SelectFields = REPLACE(@SelectFields,
                                            'V.bitIsPrimaryVendor',
                                            ' CASE WHEN V.numVendorID = I.numVendorID THEN 1 ELSE 0 END ')
            END

	
        SET @strSql = @strSql + @SelectFields + @from + @InneJoinOn + @Where
            + @OrderBy
	



	

	---- enable or disble group by based on search result view
	--IF CHARINDEX('dbo.WareHouseItems WI',@InneJoinOn)=0
		--SET @GroupBy = ''
	--ELSE 
	--	SET @GroupBy = ' GROUP BY I.numItemCode,T.ID,' + SUBSTRING(@GroupBy, 1, LEN(@GroupBy)-1);
	
	
        SET @strSql = @strSql + @GroupBy /*+ ' order by vcItemName; '*/
        PRINT @strSql
        EXEC ( @strSql
            )
	
	 --IF EXISTS ( SELECT * FROM   sysobjects WHERE  xtype = 'U' AND NAME = '#tempPrice' ) 
	 DROP TABLE #tempPrice

-------------------------------------------------	
    END

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetNamedPriceLevel')
DROP PROCEDURE USP_GetNamedPriceLevel
GO
CREATE PROCEDURE [dbo].[USP_GetNamedPriceLevel]
(
	@numDomainID NUMERIC(18,0)
)
AS 
BEGIN
	IF OBJECT_ID('tempdb..#PriceTbl') IS NOT NULL DROP TABLE #PriceTbl
		
	SELECT DISTINCT ROW_NUMBER() OVER(PARTITION BY pt.numItemCode ORDER BY numPricingID) Id
	INTO #PriceTbl
	FROM [PricingTable] pt
	INNER JOIN Item
	ON Item.numItemCode = pt.numItemCode 
	AND Item.numDomainID = @numDomainID
	WHERE tintRuleType = 3
	ORDER BY [Id] 

	SELECT pt.Id
			,ISNULL(NULLIF(pnt.vcPriceLevelName, ''), CONCAT('Price Level ',pt.Id)) Value
	FROM #PriceTbl pt
	LEFT JOIN PricingNamesTable pnt
	ON pt.Id = pnt.tintPriceLevel
	AND pnt.numDomainID = @numDomainID
END
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetNavigation')
DROP PROCEDURE dbo.USP_GetNavigation
GO
CREATE PROCEDURE [dbo].[USP_GetNavigation]
(
	@numDomainID NUMERIC(18,0),
	@numGroupID NUMERIC(18,0)
)
AS 
BEGIN

	/********  MAIN MENU  ********/

	SELECT  
		T.numTabID,
		ISNULL((SELECT TOP 1 numTabName FROM TabDefault WHERE numDomainid = @numDomainID AND numTabId = T.numTabId AND tintTabType = T.tintTabType),T.numTabName) vcTabName,
		CASE 
			WHEN (SELECT COUNT(*) FROM ShortCutGrpConf WHERE numDomainID=@numDomainID AND numGroupId=@numGroupID AND numTabId=T.numTabId AND ISNULL(bitInitialPage,0)=1) > 0
			THEN
				REPLACE(ISNULL((SELECT TOP 1 Link FROM ShortCutBar WHERE (numDomainid = @numDomainID OR bitDefault=1) AND numTabId = T.numTabId AND id=(SELECT TOP 1 numLinkId FROM ShortCutGrpConf WHERE numDomainID=@numDomainID AND numGroupId=@numGroupID AND numTabId=T.numTabId AND ISNULL(bitInitialPage,0)=1)),''),'../','')
			ELSE 
				ISNULL(vcURL,'')
		END vcURL,
		ISNULL(vcAddURL,'') vcAddURL,
		ISNULL(bitAddIsPopUp,0) AS bitAddIsPopUp,
		numOrder
	FROM    
		TabMaster T
	JOIN 
		GroupTabDetails G ON G.numTabId = T.numTabId
	WHERE   
		(numDomainID = @numDomainID OR bitFixed = 1)
		AND numGroupID = @numGroupID
		AND ISNULL(G.[tintType], 0) <> 1
		AND tintTabType =1
		AND T.numTabID NOT IN (2,68)
	ORDER BY 
		numOrder   


	/********  SUB MENU  ********/

	DECLARE @numTabID NUMERIC(18,0)
	DECLARE @numModuleID AS NUMERIC(18, 0)

	DECLARE @TEMP TABLE
	(
		ID INT IDENTITY(1,1),
		numOrigParentID NUMERIC(18,0),
		numParentID NUMERIC(18,0),
		numListItemID NUMERIC(18,0),
		numTabID NUMERIC(18,0),
		numPageNavID NUMERIC(18,0),
		vcNodeName VARCHAR(500),
		vcURL VARCHAR(1000),
		vcAddURL VARCHAR(1000),
		bitAddIsPopUp BIT,
		[order] int,
		bitVisible BIT,
		vcImageURL VARCHAR(1000)
	)

	BEGIN /******************** OPPORTUNITY/ORDER ***********************/

		SET @numTabID = 1
		SELECT TOP 1 @numModuleID = numModuleID FROM dbo.PageNavigationDTL WHERE numTabID = @numTabID

		INSERT INTO
			@TEMP
		SELECT
			0,
			0,
			0,
			@numTabID,
			PND.numPageNavID,
			vcPageNavName,
			ISNULL(vcNavURL,'') AS vcNavURL,
			ISNULL(PND.vcAddURL,''),
			ISNULL(PND.bitAddIsPopUp,0),
			0,
			1,
			ISNULL(PND.vcImageURL,'')
		FROM 
			PageNavigationDTL PND
		JOIN 
			dbo.TreeNavigationAuthorization TNA 
		ON 
			PND.numPageNavID = TNA.numPageNavID  
			AND PND.numTabID = TNA.numTabID       
		WHERE  
			ISNULL(TNA.bitVisible, 0) = 1
			AND ISNULL(PND.bitVisible, 0) = 1
			AND numModuleID = @numModuleID
			AND TNA.numDomainID = @numDomainID
			AND numGroupID = @numGroupID
			AND PND.numParentID <> 0
		ORDER BY 
			numPageNavID 

		INSERT INTO
			@TEMP
		SELECT  
			0,
			0,
			0,
			@numTabID,
			0,
			ISNULL(vcData,'') + 's' [vcPageNavName],
			'../Opportunity/frmOpenBizDocs.aspx?BizDocName=' + vcData + '&BizDocID=' + CONVERT(VARCHAR(10),LD.numListItemID) AS [vcNavURL],
			'',
			0,
			0,
			1 ,
			''
		FROM 
			dbo.ListDetails LD
		JOIN 
			dbo.TreeNavigationAuthorization TNA 
		ON 
			LD.numListItemID = TNA.numPageNavID 
			AND ISNULL(bitVisible,0) = 1  
		WHERE 
			numListID = 27 
			AND ISNULL(TNA.[numDomainID], 0) = @numDomainID
			AND ISNULL(TNA.[numGroupID], 0) = @numGroupID
		ORDER BY 
			numPageNavID 

	END

	BEGIN /******************** MARKETING ***************************/
		SET @numTabID = 3
		SELECT TOP 1 @numModuleID = numModuleID FROM dbo.PageNavigationDTL WHERE numTabID = @numTabID 	

		INSERT INTO
			@TEMP
		SELECT 
		    ISNULL(PND.numParentID,0),
			PND.numParentID,
			0,
			@numTabID,
			PND.numPageNavID AS numPageNavID,
			vcPageNavName,
			ISNULL(vcNavURL, '') as vcNavURL,
			ISNULL(PND.vcAddURL,''),
			ISNULL(PND.bitAddIsPopUp,0),
			0,
			1,
			ISNULL(PND.vcImageURL,'')
		FROM   
			PageNavigationDTL PND
		JOIN 
			dbo.TreeNavigationAuthorization TNA 
		ON 
			PND.numPageNavID = TNA.numPageNavID
			AND TNA.numTabID=@numTabID
		WHERE 
			ISNULL(TNA.bitVisible, 0) = 1      
			AND ISNULL(PND.bitVisible, 0) = 1
			AND PND.numParentID <> 0
			AND PND.numModuleID = @numModuleID
			AND numGroupID = @numGroupID
		ORDER BY 
			numParentID,
			intSortOrder,
			numPageNavID

		UPDATE 
			t1
		SET 
			t1.numParentID = ISNULL((SELECT ID FROM @TEMP t2 WHERE t2.numPageNavID=t1.numParentID AND t2.numTabID=@numTabID),0)
		FROM
			@TEMP t1
		WHERE 
			numTabID=@numTabID
	END

	BEGIN /******************** SUPPORT/CASE ***************************/

		SET @numTabID = 4
		SELECT TOP 1 @numModuleID = numModuleID FROM dbo.PageNavigationDTL WHERE numTabID = @numTabID 	

		INSERT INTO
			@TEMP
		SELECT 
			ISNULL(PND.numParentID,0),
			PND.numParentID,
			0,
			@numTabID,
			PND.numPageNavID AS numPageNavID,
			vcPageNavName,
			ISNULL(vcNavURL, '') as vcNavURL,
			ISNULL(PND.vcAddURL,''),
			ISNULL(PND.bitAddIsPopUp,0),
			0,
			1,
			ISNULL(PND.vcImageURL,'')
		FROM   
			PageNavigationDTL PND
		LEFT JOIN 
			dbo.TreeNavigationAuthorization TNA 
		ON 
			PND.numPageNavID = TNA.numPageNavID
			AND TNA.numTabID=@numTabID
		WHERE 
			ISNULL(TNA.bitVisible, 1) = 1
			AND ISNULL(PND.bitVisible, 1) = 1
			AND PND.numParentID <> 0
			AND PND.numModuleID = @numModuleID
			AND numGroupID = @numGroupID
		ORDER BY 
			numParentID,
			intSortOrder,
			numPageNavID

		UPDATE 
			t1
		SET 
			t1.numParentID = ISNULL((SELECT ID FROM @TEMP t2 WHERE t2.numPageNavID=t1.numParentID AND t2.numTabID=@numTabID),0)
		FROM
			@TEMP t1
		WHERE 
			numTabID=@numTabID

	END

	BEGIN /******************** REPORTS ***************************/

		SET @numTabID = 6
		SELECT TOP 1 @numModuleID = numModuleID FROM dbo.PageNavigationDTL WHERE numTabID = @numTabID 	

		INSERT INTO
			@TEMP
		SELECT 
		    ISNULL(PND.numParentID,0),
			PND.numParentID,
			0,
			@numTabID,
			PND.numPageNavID AS numPageNavID,
			vcPageNavName,
			ISNULL(vcNavURL, '') as vcNavURL,
			ISNULL(PND.vcAddURL,''),
			ISNULL(PND.bitAddIsPopUp,0),
			0,
			1,
			ISNULL(PND.vcImageURL,'')
		FROM   
			PageNavigationDTL PND
		LEFT JOIN 
			dbo.TreeNavigationAuthorization TNA 
		ON 
			PND.numPageNavID = TNA.numPageNavID
			AND TNA.numTabID=@numTabID
		WHERE 
			ISNULL(TNA.bitVisible, 1) = 1
			AND ISNULL(PND.bitVisible, 1) = 1
			AND PND.numParentID <> 0
			AND PND.numModuleID = @numModuleID
			AND numGroupID = @numGroupID
		ORDER BY 
			numParentID,
			intSortOrder,
			numPageNavID

		UPDATE 
			t1
		SET 
			t1.numParentID = ISNULL((SELECT ID FROM @TEMP t2 WHERE t2.numPageNavID=t1.numParentID AND t2.numTabID=@numTabID),0)
		FROM
			@TEMP t1
		WHERE 
			numTabID=@numTabID

	END

	BEGIN /******************** RELATIONSHIP ***********************/

		SET @numTabID = 7
		SELECT TOP 1 @numModuleID = numModuleID FROM dbo.PageNavigationDTL WHERE numTabID = @numTabID 	

		INSERT INTO 
			@TEMP
		SELECT    
			ISNULL(PND.numParentID,0),
			PND.numParentID AS numParentID,
			0,
			@numTabID,
			PND.numPageNavID AS numPageNavID,
			ISNULL(PND.vcPageNavName, '') AS [vcNodeName],
			ISNULL(vcNavURL, '') AS vcNavURL,
			ISNULL(PND.vcAddURL,''),
			ISNULL(PND.bitAddIsPopUp,0),
			ISNULL(TreeNodeOrder.numOrder,1000) AS numOrder,
			ISNULL(TNA.bitVisible,1) AS bitVisible,
			ISNULL(PND.vcImageURL,'')
		FROM      
			PageNavigationDTL PND
		LEFT JOIN 
			dbo.TreeNavigationAuthorization TNA 
		ON 
			TNA.numPageNavID = PND.numPageNavID AND
			TNA.numDomainID = @numDomainID AND 
			TNA.numGroupID = @numGroupID
			AND TNA.numTabID=@numTabID
		LEFT JOIN
			dbo.TreeNodeOrder 
		ON
			dbo.TreeNodeOrder.numTabID = @numTabID AND
			dbo.TreeNodeOrder.numDomainID = @numDomainID AND
			dbo.TreeNodeOrder.numGroupID = @numGroupID AND
			dbo.TreeNodeOrder.numPageNavID = PND.numPageNavID
		WHERE     
			ISNULL(PND.bitVisible, 0) = 1
			AND PND.numParentID <> 0
			AND numModuleID = @numModuleID

		UPDATE 
			t1
		SET 
			t1.numParentID = ISNULL((SELECT ID FROM @TEMP t2 WHERE t2.numPageNavID=t1.numParentID AND t2.numTabID=@numTabID),0)
		FROM
			@TEMP t1
		WHERE 
			numTabID=@numTabID

		INSERT INTO 
			@TEMP
		SELECT
			0,
			0 AS numParentID,
			ListDetails.numListItemID,
			@numTabID,
			0 AS numPageNavID,
			ListDetails.vcData AS [vcNodeName],
			'../prospects/frmCompanyList.aspx?RelId='+ convert(varchar(10),ListDetails.numListItemID),
			CASE WHEN ListDetails.numListItemID = 93 THEN '' ELSE CONCAT('~/include/frmAddOrganization.aspx?RelID=',ListDetails.numListItemID,'&FormID=36') END,
			CASE WHEN ListDetails.numListItemID = 93 THEN 0 ELSE 1 END,
			ISNULL(TreeNodeOrder.numOrder,2000) AS numOrder,
			ISNULL(dbo.TreeNodeOrder.bitVisible,1) AS bitVisible,
			''
		FROM
			ListDetails
		LEFT JOIN 
			dbo.TreeNodeOrder 
		ON
			dbo.TreeNodeOrder.numListItemID = ListDetails.numListItemID AND
			dbo.TreeNodeOrder.numParentID = 6 AND
			dbo.TreeNodeOrder.numTabID = @numTabID AND
			dbo.TreeNodeOrder.numDomainID = @numDomainID AND
			dbo.TreeNodeOrder.numGroupID = @numGroupID AND
			dbo.TreeNodeOrder.numPageNavID IS NULL
		WHERE
			numListID = 5 AND
			(ListDetails.numDomainID = @numDomainID  OR constFlag = 1) AND
			(ISNULL(bitDelete,0) = 0 OR constFlag = 1) AND
			ListDetails.numListItemID <> 46 

		INSERT INTO 
			@TEMP
		SELECT 
			0,
			(SELECT ID FROM @TEMP WHERE numListItemID = FRDTL.numPrimaryListItemID),
			L2.numListItemID,   
			@numTabID,
			0,
			L2.vcData AS [vcNodeName],
			CONCAT('../prospects/frmCompanyList.aspx?RelId=',FRDTL.numPrimaryListItemID,'&profileid=',numSecondaryListItemID),
			--CONCAT('~/include/frmAddOrganization.aspx?RelID=',FRDTL.numPrimaryListItemID,'&profileid=',numSecondaryListItemID,'&FormID=36'),
			CASE WHEN L2.numListItemID = 37257 THEN '' ELSE CONCAT('~/include/frmAddOrganization.aspx?RelID=',FRDTL.numPrimaryListItemID,'&profileid=',numSecondaryListItemID,'&FormID=36') END,
			--1,
			CASE WHEN L2.numListItemID = 37257 THEN 0 ELSE 1 END,
			ISNULL(TreeNodeOrder.numOrder,3000) AS numOrder,
			ISNULL(dbo.TreeNodeOrder.bitVisible,1) AS bitVisible,
			''
		FROM      
			FieldRelationship FR
		join 
			FieldRelationshipDTL FRDTL on FRDTL.numFieldRelID = FR.numFieldRelID
		join 
			ListDetails L1 on numPrimaryListItemID = L1.numListItemID
		join 
			ListDetails L2 on numSecondaryListItemID = L2.numListItemID
		LEFT JOIN 
			dbo.TreeNodeOrder 
		ON
			dbo.TreeNodeOrder.numListItemID = L2.numListItemID AND
			dbo.TreeNodeOrder.numParentID = FRDTL.numPrimaryListItemID AND
			dbo.TreeNodeOrder.numTabID = @numTabID AND
			dbo.TreeNodeOrder.numDomainID = @numDomainID AND
			dbo.TreeNodeOrder.numGroupID = @numGroupID AND
			dbo.TreeNodeOrder.numPageNavID IS NULL
		WHERE     
			numPrimaryListItemID <> 46
			and FR.numPrimaryListID = 5
			and FR.numSecondaryListID = 21
			and FR.numDomainID = @numDomainID
			and L2.numDomainID = @numDomainID

		INSERT INTO
			@TEMP
		SELECT 
			0,
			(SELECT ID FROM @TEMP WHERE numPageNavID=11),
			L2.numListItemID,
			@numTabID,
			11,
			L2.vcData AS [vcNodeName],
			'../prospects/frmProspectList.aspx?numProfile=' + CONVERT(VARCHAR(10), L2.numListItemID) AS vcNavURL,
			CONCAT('~/include/frmAddOrganization.aspx?RelID=3&profileid=',numSecondaryListItemID,'&FormID=35'),
			1,
			ISNULL(TreeNodeOrder.numOrder,4000) AS numOrder,
			ISNULL(dbo.TreeNodeOrder.bitVisible,1) AS bitVisible,
			''
		FROM    
			FieldRelationship AS FR
		INNER JOIN 
			FieldRelationshipDTL AS FRD ON FRD.numFieldRelID = FR.numFieldRelID
		INNER JOIN 
			ListDetails AS L1 ON L1.numListItemID = FRD.numPrimaryListItemID
		INNER JOIN 
			ListDetails AS L2 ON L2.numListItemID = FRD.numSecondaryListItemID
		LEFT JOIN 
			dbo.TreeNodeOrder 
		ON
			dbo.TreeNodeOrder.numListItemID = L2.numListItemID AND
			dbo.TreeNodeOrder.numParentID = 11 AND
			dbo.TreeNodeOrder.numTabID = @numTabID AND
			dbo.TreeNodeOrder.numDomainID = @numDomainID AND
			dbo.TreeNodeOrder.numGroupID = @numGroupID AND
			dbo.TreeNodeOrder.numPageNavID IS NULL
		WHERE 
			FRD.numPrimaryListItemID = 46
			AND FR.numDomainID = @numDomainID

		INSERT INTO
			@TEMP
		SELECT 
			0,
			(SELECT ID FROM @TEMP WHERE numPageNavID=12),
			L2.numListItemID,
			@numTabID,
			12,
			L2.vcData AS [vcNodeName],
			'../account/frmAccountList.aspx?numProfile=' + CONVERT(VARCHAR(10), L2.numListItemID) AS vcNavURL,
			CONCAT('~/include/frmAddOrganization.aspx?RelID=2&profileid=',numSecondaryListItemID,'&FormID=36'),
			1,
			ISNULL(TreeNodeOrder.numOrder,5000) AS numOrder,
			ISNULL(dbo.TreeNodeOrder.bitVisible,1) AS bitVisible,
			''
		FROM    
			FieldRelationship AS FR
		INNER JOIN 
			FieldRelationshipDTL AS FRD ON FRD.numFieldRelID = FR.numFieldRelID
		INNER JOIN 
			ListDetails AS L1 ON L1.numListItemID = FRD.numPrimaryListItemID
		INNER JOIN 
			ListDetails AS L2 ON L2.numListItemID = FRD.numSecondaryListItemID
		LEFT JOIN 
			dbo.TreeNodeOrder 
		ON
			dbo.TreeNodeOrder.numListItemID = L2.numListItemID AND
			dbo.TreeNodeOrder.numParentID = 12 AND
			dbo.TreeNodeOrder.numTabID = @numTabID AND
			dbo.TreeNodeOrder.numDomainID = @numDomainID AND
			dbo.TreeNodeOrder.numGroupID = @numGroupID AND
			dbo.TreeNodeOrder.numPageNavID IS NULL
		WHERE 
			FRD.numPrimaryListItemID = 46
			AND FR.numDomainID = @numDomainID

		INSERT INTO 
			@TEMP
		SELECT 
			0,
			(SELECT ID FROM @TEMP WHERE numListItemID=FRD.numPrimaryListItemID AND numPageNavID=11),
			L2.numListItemID,
			@numTabID,
			0,
			L2.vcData AS [vcNodeName],
			'../account/frmProspectList.aspx?numProfile=' + CONVERT(VARCHAR(10), FRD.numPrimaryListItemID) + '&numTerritoryID=' + CONVERT(VARCHAR(10), L2.numListItemID) AS vcNavURL,
			CONCAT('~/include/frmAddOrganization.aspx?RelID=3&profileid=',FRD.numPrimaryListItemID,'&FormID=35','&numTerritoryID=',L2.numListItemID),
			1,
			ISNULL(TreeNodeOrder.numOrder,7000) AS numOrder,
			ISNULL(dbo.TreeNodeOrder.bitVisible,1) AS bitVisible,
			''
		FROM    
			FieldRelationship AS FR
		INNER JOIN 
			FieldRelationshipDTL AS FRD ON FRD.numFieldRelID = FR.numFieldRelID
		INNER JOIN 
			ListDetails AS L1 ON L1.numListItemID = FRD.numPrimaryListItemID
		INNER JOIN 
			ListDetails AS L2 ON L2.numListItemID = FRD.numSecondaryListItemID
		LEFT JOIN 
			dbo.TreeNodeOrder 
		ON
			dbo.TreeNodeOrder.numListItemID = L2.numListItemID AND
			dbo.TreeNodeOrder.numParentID = 11 AND
			dbo.TreeNodeOrder.numTabID = @numTabID AND
			dbo.TreeNodeOrder.numDomainID = @numDomainID AND
			dbo.TreeNodeOrder.numGroupID = @numGroupID AND
			dbo.TreeNodeOrder.numPageNavID IS NULL
		WHERE   
			FRD.numPrimaryListItemID IN (SELECT numSecondaryListItemID FROM FieldRelationshipDTL FRDL JOIN FieldRelationship FRP ON FRDL.numFieldRelID = FRP.numFieldRelID WHERE numDomainID=@numDomainID AND numPrimaryListItemID=46)
			AND FR.numDomainID = @numDomainID

		INSERT INTO 
			@TEMP
		SELECT 
			0,
			(SELECT ID FROM @TEMP WHERE numListItemID=FRD.numPrimaryListItemID AND numPageNavID=12),
			L2.numListItemID,
			@numTabID,
			0,
			L2.vcData AS [vcNodeName],
			'../account/frmAccountList.aspx?numProfile=' + CONVERT(VARCHAR(10), FRD.numPrimaryListItemID) + '&numTerritoryID=' + CONVERT(VARCHAR(10), L2.numListItemID) AS vcNavURL,
			CONCAT('~/include/frmAddOrganization.aspx?RelID=2&profileid=',FRD.numPrimaryListItemID,'&FormID=36','&numTerritoryID=',L2.numListItemID),
			1,
			ISNULL(TreeNodeOrder.numOrder,8000) AS numOrder,
			ISNULL(dbo.TreeNodeOrder.bitVisible,1) AS bitVisible,
			''
		FROM    
			FieldRelationship AS FR
		INNER JOIN 
			FieldRelationshipDTL AS FRD ON FRD.numFieldRelID = FR.numFieldRelID
		INNER JOIN 
			ListDetails AS L1 ON L1.numListItemID = FRD.numPrimaryListItemID
		INNER JOIN 
			ListDetails AS L2 ON L2.numListItemID = FRD.numSecondaryListItemID
		LEFT JOIN 
			dbo.TreeNodeOrder 
		ON
			dbo.TreeNodeOrder.numListItemID = L2.numListItemID AND
			dbo.TreeNodeOrder.numParentID = 12 AND
			dbo.TreeNodeOrder.numTabID = @numTabID AND
			dbo.TreeNodeOrder.numDomainID = @numDomainID AND
			dbo.TreeNodeOrder.numGroupID = @numGroupID AND
			dbo.TreeNodeOrder.numPageNavID IS NULL
		WHERE   
			FRD.numPrimaryListItemID IN (SELECT numSecondaryListItemID FROM FieldRelationshipDTL FRDL JOIN FieldRelationship FRP ON FRDL.numFieldRelID = FRP.numFieldRelID WHERE numDomainID=@numDomainID AND numPrimaryListItemID=46)
			AND FR.numDomainID = @numDomainID

		INSERT INTO
			@TEMP
		SELECT 
			0,
			(SELECT ID FROM @TEMP WHERE numPageNavID=13),
			listdetails.numListItemID,
			@numTabID,
			0,
			listdetails.vcData AS [vcNodeName],
			'../contact/frmContactList.aspx?ContactType=' + CONVERT(VARCHAR(10), listdetails.numListItemID) AS vcNavURL,
		   '',
		   0,
			ISNULL(TreeNodeOrder.numOrder,6000) AS numOrder,
			ISNULL(dbo.TreeNodeOrder.bitVisible,1) AS bitVisible,
			''
		FROM    
			listdetails
		LEFT JOIN 
			dbo.TreeNodeOrder 
		ON
			dbo.TreeNodeOrder.numListItemID = listdetails.numListItemID AND
			dbo.TreeNodeOrder.numParentID = 13 AND
			dbo.TreeNodeOrder.numTabID = @numTabID AND
			dbo.TreeNodeOrder.numDomainID = @numDomainID AND
			dbo.TreeNodeOrder.numGroupID = @numGroupID AND
			dbo.TreeNodeOrder.numPageNavID IS NULL
		WHERE   
			numListID = 8
			AND (constFlag = 1 OR listdetails.numDomainID = @numDomainID)

		INSERT INTO
			@TEMP
		SELECT 
			0,
			(SELECT ID FROM @TEMP WHERE numPageNavID=13),
			101 AS numListItemID,
			@numTabID,
			0,
			'Primary Contact' AS [vcNodeName],
			'../Contact/frmcontactList.aspx?ContactType=101',
			'',
			0,
			ISNULL((
						SELECT 
							numOrder 
						FROM 
							dbo.TreeNodeOrder 
						WHERE
							dbo.TreeNodeOrder.numListItemID = 101 AND
							dbo.TreeNodeOrder.numParentID = 13 AND
							dbo.TreeNodeOrder.numTabID = @numTabID AND
							dbo.TreeNodeOrder.numDomainID = @numDomainID AND
							dbo.TreeNodeOrder.numGroupID = @numGroupID AND
							dbo.TreeNodeOrder.numPageNavID IS NULL
					),7000) AS numOrder,
			ISNULL((
						SELECT 
							bitVisible 
						FROM 
							dbo.TreeNodeOrder 
						WHERE
							dbo.TreeNodeOrder.numListItemID = 101 AND
							dbo.TreeNodeOrder.numParentID = 13 AND
							dbo.TreeNodeOrder.numTabID = @numTabID AND
							dbo.TreeNodeOrder.numDomainID = @numDomainID AND
							dbo.TreeNodeOrder.numGroupID = @numGroupID AND
							dbo.TreeNodeOrder.numPageNavID IS NULL
					),1) AS bitVisible,
					''

	END

	BEGIN /******************** DOCUMENTS ***********************/
		SET @numTabID = 8

		INSERT INTO 
			@TEMP
		SELECT 
			0,
			0,
			0,
			@numTabID,
			0,
			'Regular Documents' as vcPageNavName,
			'../Documents/frmRegularDocList.aspx?Category=0',
			'~/Documents/frmGenDocPopUp.aspx',
			1,
			1,
			1,
			''


		INSERT INTO 
			@TEMP
		SELECT  
			0,
			SCOPE_IDENTITY(),
			Ld.numListItemId,
			@numTabID,
			0,
			vcData vcPageNavName,
			'../Documents/frmRegularDocList.aspx?Category=' + cast(Ld.numListItemId as varchar(10)),
			CASE WHEN vcData = 'Email Template' THEN '~/Marketing/frmEmailTemplate.aspx' ELSE '' END,
			CASE WHEN vcData = 'Email Template' THEN 1 ELSE 0 END,
			ISNULL(intSortOrder, LD.sintOrder) SortOrder,
			1,
			''
		FROM    
			ListDetails LD
		LEFT JOIN 
			ListOrder LO 
		ON 
			LD.numListItemID = LO.numListItemID
			AND LO.numDomainId = @numDomainID
		WHERE   
			Ld.numListID = 29
			AND (constFlag = 1 OR Ld.numDomainID = @numDomainID)

	END

	BEGIN /******************** ACCOUNTING **********************/
	
		SET @numTabID = 45
		SELECT TOP 1 @numModuleID = numModuleID FROM dbo.PageNavigationDTL WHERE numTabID = @numTabID

		INSERT INTO 
			@TEMP
		SELECT 
		    ISNULL(PND.numParentID,0),
			PND.numParentID,
			0,
			@numTabID,
			PND.numPageNavID,
			vcPageNavName,
			ISNULL(vcNavURL,'') AS vcNavURL,
			ISNULL(PND.vcAddURL,''),
			ISNULL(PND.bitAddIsPopUp,0),
			0,
			1 ,
			ISNULL(PND.vcImageURL,'')     
		FROM 
			PageNavigationDTL PND
		JOIN 
			dbo.TreeNavigationAuthorization TNA 
		ON 
			PND.numPageNavID = TNA.numPageNavID
			AND TNA.numTabID=@numTabID
		WHERE  
			ISNULL(TNA.bitVisible, 1) = 1
			AND ISNULL(PND.bitVisible, 1) = 1
			AND numModuleID=@numModuleID
			AND TNA.numDomainID = @numDomainID
			AND numGroupID = @numGroupID
			AND numParentID <> 0
		ORDER BY 
			numParentID,PND.numPageNavID 
	
		INSERT INTO 
			@TEMP
		SELECT 
			0,
			(CASE numFinancialViewID 
				WHEN 26770 THEN 96 /*Profit & Loss*/
				WHEN 26769 THEN 176 /*Income & Expense*/
				WHEN 26768 THEN 98 /*Cash Flow Statement*/
				WHEN 26767 THEN 97 /*Balance Sheet*/
				WHEN 26766 THEN 81 /*A/R & A/P Aging*/
				WHEN 26766 THEN 82 /*A/R & A/P Aging*/
				ELSE 0
			END),
			numFRID,
			@numTabID,
			0,
			vcReportName,
			CASE numFinancialViewID 
				WHEN 26770 THEN (SELECT vcNavURL FROM dbo.PageNavigationDTL WHERE numPageNavID =96 AND numModuleID=35)  /*Profit & Loss*/
				WHEN 26769 THEN (SELECT vcNavURL FROM dbo.PageNavigationDTL WHERE numPageNavID =176 AND numModuleID=35) /*Income & Expense*/
				WHEN 26768 THEN (SELECT vcNavURL FROM dbo.PageNavigationDTL WHERE numPageNavID =98 AND numModuleID=35) /*Cash Flow Statement*/
				WHEN 26767 THEN (SELECT vcNavURL FROM dbo.PageNavigationDTL WHERE numPageNavID =97 AND numModuleID=35) /*Balance Sheet*/
				WHEN 26766 THEN (SELECT vcNavURL FROM dbo.PageNavigationDTL WHERE numPageNavID =81 AND numModuleID=35) /*A/R & A/P Aging*/
				WHEN 26766 THEN (SELECT vcNavURL FROM dbo.PageNavigationDTL WHERE numPageNavID =82 AND numModuleID=35) /*A/R & A/P Aging*/
				ELSE ''
			END + 
			CASE CHARINDEX('?',(CASE numFinancialViewID 
				WHEN 26770 THEN (SELECT vcNavURL FROM dbo.PageNavigationDTL WHERE numPageNavID =96 AND numModuleID=35)  /*Profit & Loss*/
				WHEN 26769 THEN (SELECT vcNavURL FROM dbo.PageNavigationDTL WHERE numPageNavID =176 AND numModuleID=35) /*Income & Expense*/
				WHEN 26768 THEN (SELECT vcNavURL FROM dbo.PageNavigationDTL WHERE numPageNavID =98 AND numModuleID=35) /*Cash Flow Statement*/
				WHEN 26767 THEN (SELECT vcNavURL FROM dbo.PageNavigationDTL WHERE numPageNavID =97 AND numModuleID=35) /*Balance Sheet*/
				WHEN 26766 THEN (SELECT vcNavURL FROM dbo.PageNavigationDTL WHERE numPageNavID =81 AND numModuleID=35) /*A/R & A/P Aging*/
				WHEN 26766 THEN (SELECT vcNavURL FROM dbo.PageNavigationDTL WHERE numPageNavID =82 AND numModuleID=35) /*A/R & A/P Aging*/
				ELSE ''
			END)) 
				WHEN 0 THEN '?FRID='
				ELSE '&FRID='
			END + CAST(numFRID AS VARCHAR),
			'',
			0,
			0,
			1,
			''
		FROM 
			dbo.FinancialReport 
		WHERE 
			numDomainID=@numDomainID
	
		UPDATE 
			t1
		SET 
			t1.numParentID = ISNULL((SELECT ID FROM @TEMP t2 WHERE t2.numPageNavID=t1.numParentID AND t2.numTabID=@numTabID),0)
		FROM
			@TEMP t1
		WHERE 
			numTabID=@numTabID
	  
	END

	BEGIN /******************** ITEMS **********************/

		SET @numTabID = 80
		SELECT TOP 1 @numModuleID = numModuleID FROM dbo.PageNavigationDTL WHERE numTabID = @numTabID 	

		INSERT INTO
			@TEMP
		SELECT 
			ISNULL(PND.numParentID,0),
			numParentID,
			0,
			@numTabID,
			PND.numPageNavID,
			vcPageNavName,
			isnull(vcNavURL, '') as vcNavURL,
			ISNULL(PND.vcAddURL,''),
			ISNULL(PND.bitAddIsPopUp,0),
			0,
			1,
			ISNULL(PND.vcImageURL,'')
		FROM 
			PageNavigationDTL PND
		JOIN 
			dbo.TreeNavigationAuthorization TNA 
		ON 
			PND.numPageNavID = TNA.numPageNavID
			AND TNA.numTabID=@numTabID
		WHERE
			ISNULL(TNA.bitVisible, 0) = 1
			AND ISNULL(PND.bitVisible, 0) = 1
			AND PND.numModuleID = @numModuleID
			AND numGroupID = @numGroupID
			AND numParentID <> 0
		ORDER BY 
			numParentID,
			intSortOrder,
			numPageNavID

		UPDATE 
			t1
		SET 
			t1.numParentID = ISNULL((SELECT ID FROM @TEMP t2 WHERE t2.numPageNavID=t1.numParentID AND t2.numTabID=@numTabID),0)
		FROM
			@TEMP t1
		WHERE 
			numTabID=@numTabID
	END

	BEGIN /******************** Activities ***************************/

		SET @numTabID = 36
		SELECT TOP 1 @numModuleID = numModuleID FROM dbo.PageNavigationDTL WHERE numTabID = @numTabID 	

		INSERT INTO
			@TEMP
		SELECT 
			ISNULL(PND.numParentID,0),
			PND.numParentID,
			0,
			@numTabID,
			PND.numPageNavID AS numPageNavID,
			vcPageNavName,
			ISNULL(vcNavURL, '') as vcNavURL,
			ISNULL(PND.vcAddURL,''),
			ISNULL(PND.bitAddIsPopUp,0),
			0,
			1,
			ISNULL(PND.vcImageURL,'')
		FROM   
			PageNavigationDTL PND
		LEFT JOIN 
			dbo.TreeNavigationAuthorization TNA 
		ON 
			PND.numPageNavID = TNA.numPageNavID
			AND TNA.numTabID=@numTabID
		WHERE 
			ISNULL(TNA.bitVisible, 1) = 1
			AND ISNULL(PND.bitVisible, 1) = 1
			AND PND.numParentID <> 0
			AND PND.numModuleID = @numModuleID
			AND numGroupID = @numGroupID
		ORDER BY 
			numParentID,
			intSortOrder,
			numPageNavID

		UPDATE 
			t1
		SET 
			t1.numParentID = ISNULL((SELECT ID FROM @TEMP t2 WHERE t2.numPageNavID=t1.numParentID AND t2.numTabID=@numTabID),0)
		FROM
			@TEMP t1
		WHERE 
			numTabID=@numTabID

	END


	;WITH CTE(ID) AS 
	(
		SELECT
			ID
		FROM
			@TEMP t1
		WHERE 
			ISNULL(numOrigParentID,0) > 0
			AND numOrigParentID NOT IN (SELECT numPageNavID FROM PageNavigationDTL WHERE numParentID=0 AND numTabID=t1.numTabID)
			AND numOrigParentID NOT IN (SELECT numPageNavID FROM @TEMP t2 WHERE t2.numTabID = t1.numTabID)
		UNION ALL
		SELECT 
			t2.ID
		FROM
			@TEMP t2
		JOIN
			CTE c
		ON
			t2.numParentID=c.ID
	)

	DELETE FROM @TEMP WHERE ID IN (SELECT ID FROM CTE)

	
	/** Admin Menu **/
	INSERT INTO 
		@TEMP
	SELECT 
		numParentID,
		numParentID,
		0,
		-1,
		PND.numPageNavID,
		vcPageNavName,
		ISNULL(vcNavURL, '') as vcNavURL,
		'',
		0,
		ISNULL(PND.intSortOrder,0) sintOrder,
		1,
		ISNULL(vcImageURL,'')
	FROM   
		PageNavigationDTL PND
	JOIN 
		dbo.TreeNavigationAuthorization TNA 
	ON 
		PND.numPageNavID = TNA.numPageNavID
		AND PND.numTabID = TNA.numTabID
	WHERE
		ISNULL(TNA.bitVisible, 0) = 1
		AND ISNULL(PND.bitVisible, 0) = 1
		AND PND.numModuleID = 13 --AND bitVisible=1  
		AND TNA.numDomainID = @numDomainID
		AND TNA.numGroupID = @numGroupID
	ORDER BY
		PND.numParentID, 
		PND.intSortOrder,
		(CASE WHEN PND.numPageNavID=79 THEN 2000 ELSE 0 END),	
		numPageNavID   

	-- MAKE FIRST LEVEL CHILD AS PARENT BECUASE WE ARE NOW NOT USING TREE STRUCTURE AND DIPLAYING ITEMS IN MENU
	UPDATE 
		@TEMP
	SET 
		numParentID = 0
	WHERE
		numTabID = -1
		AND numPageNavID = (SELECT numPageNavID FROM @TEMP WHERE numTabID=-1 AND numParentID = 0)

	-- REMOVE ALL CHILD WHOSE PARENT IS HIDDEN
	DELETE FROM @TEMP WHERE numTabID = -1 AND ISNULL(numPageNavID,0) <> 0 AND numParentID NOT IN (SELECT numPageNavID FROM @TEMP WHERE numTabID = -1)

	-- REMOVE ALL CHILD WHOSE PARENT IS HIDDEN
	DELETE FROM @TEMP WHERE numParentID = 0 AND numTabID=-1

	-- UPDATE PARENT DETAIL
	UPDATE 
		t1
	SET 
		t1.numParentID = ISNULL((SELECT ID FROM @TEMP t2 WHERE t2.numTabID=-1 AND t2.numPageNavID=t1.numParentID),0)
	FROM
		@TEMP t1
	WHERE
		t1.numTabID = -1


	/** Advance Search **/
	INSERT INTO 
		@TEMP
	SELECT 
		numParentID,
		numParentID,
		0,
		-3,
		PND.numPageNavID,
		vcPageNavName,
		ISNULL(vcNavURL, '') as vcNavURL,
		'',
		0,
		ISNULL(PND.intSortOrder,0) sintOrder,
		1,
		ISNULL(vcImageURL,'')
	FROM   
		PageNavigationDTL PND
	JOIN 
		dbo.TreeNavigationAuthorization TNA 
	ON 
		PND.numPageNavID = TNA.numPageNavID
		AND PND.numTabID = TNA.numTabID
	WHERE
		ISNULL(TNA.bitVisible, 0) = 1
		AND ISNULL(PND.bitVisible, 0) = 1
		AND PND.numModuleID = 9
		AND TNA.numDomainID = @numDomainID
		AND TNA.numGroupID = @numGroupID
	ORDER BY
		PND.numParentID, 
		sintOrder,
		numPageNavID   

	-- MAKE FIRST LEVEL CHILD AS PARENT BECUASE WE ARE NOW NOT USING TREE STRUCTURE AND DIPLAYING ITEMS IN MENU
	UPDATE 
		@TEMP
	SET 
		numParentID = 0
	WHERE
		numTabID = -3
		AND numPageNavID = (SELECT numPageNavID FROM @TEMP WHERE numTabID=-3 AND numParentID = 0)

	-- REMOVE ALL CHILD WHOSE PARENT IS HIDDEN
	DELETE FROM @TEMP WHERE numTabID = -3 AND ISNULL(numPageNavID,0) <> 0 AND numParentID NOT IN (SELECT numPageNavID FROM @TEMP WHERE numTabID = -3)

	-- REMOVE ALL CHILD WHOSE PARENT IS HIDDEN
	DELETE FROM @TEMP WHERE numParentID = 0 AND numTabID=-3

	-- UPDATE PARENT DETAIL
	UPDATE 
		t1
	SET 
		t1.numParentID = ISNULL((SELECT ID FROM @TEMP t2 WHERE t2.numTabID=-3 AND t2.numPageNavID=t1.numParentID),0)
	FROM
		@TEMP t1
	WHERE
		t1.numTabID = -3

	SELECT * FROM @TEMP WHERE bitVisible=1 ORDER BY numTabID, numParentID, [order]
END

GO
/****** Object:  StoredProcedure [dbo].[USP_GetOpportunityList1]    Script Date: 03/06/2009 00:37:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- created by anoop jayaraj                                                               
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getopportunitylist1')
DROP PROCEDURE usp_getopportunitylist1
GO
CREATE PROCEDURE [dbo].[USP_GetOpportunityList1]                                                                    
	@numUserCntID numeric(9)=0,
	@numDomainID numeric(9)=0,
	@tintUserRightType tinyint=0,
	@tintSortOrder tinyint=4,
	@dtLastDate datetime,
	@OppType as tinyint,
	@CurrentPage int,
	@PageSize int,
	@TotRecs int output,
	@columnName as Varchar(50),
	@columnSortOrder as Varchar(10),
	@numDivisionID as numeric(9)=0,
	@bitPartner as bit=0,
	@ClientTimeZoneOffset as int,
	@inttype as tinyint,
	@vcRegularSearchCriteria varchar(1000)='',
	@vcCustomSearchCriteria varchar(1000)='',
	@SearchText VARCHAR(300) = '',
	@SortChar char(1)='0'                                                                 
AS                
BEGIN
         
	DECLARE @PageId as TINYINT = 0
	DECLARE @numFormId  AS INT
           
	IF @inttype = 1 
	BEGIN           
		SET @PageId =2  
		SET @numFormId=38          
	END
	ELSE IF @inttype = 2            
	BEGIN
		SET @PageId =6 
		SET @numFormId=40           
	END

	CREATE TABLE #tempForm 
	(
		tintOrder TINYINT,vcDbColumnName nvarchar(50),vcOrigDbColumnName nvarchar(50),vcFieldName nvarchar(50),vcAssociatedControlType nvarchar(50),vcListItemType varchar(3),
		numListID numeric(9),vcLookBackTableName varchar(50),bitCustomField BIT,numFieldId NUMERIC,bitAllowSorting BIT,bitAllowEdit BIT,bitIsRequired BIT,bitIsEmail BIT,
		bitIsAlphaNumeric BIT,bitIsNumeric BIT,bitIsLengthValidation BIT,intMaxLength int,intMinLength int,bitFieldMessage BIT,vcFieldMessage VARCHAR(500),ListRelID numeric(9),
		intColumnWidth int,bitAllowFiltering BIT,vcFieldDataType CHAR(1)
	)

	-- FIRST CHECK IF GRID COLUMNS ARE CONFIGURED
	DECLARE @Nocolumns AS TINYINT = 0  

	SELECT 
		@Nocolumns = ISNULL(SUM(TotalRow),0)  
	FROM
	(            
		SELECT 
			COUNT(*) TotalRow 
		FROM 
			View_DynamicColumns 
		WHERE 
			numFormId=@numFormId 
			AND numUserCntID=@numUserCntID 
			AND numDomainID=@numDomainID 
			AND tintPageType=1 
			AND numRelCntType = @numFormId
		UNION 
		SELECT 
			COUNT(*) TotalRow 
		FROM 
			View_DynamicCustomColumns 
		WHERE 
			numFormId=@numFormId 
			AND numUserCntID=@numUserCntID 
			AND numDomainID=@numDomainID 
			AND tintPageType=1 
			AND numRelCntType = @numFormId
	) TotalRows
	
	DECLARE @IsMasterConfAvailable BIT = 0
	DECLARE @numUserGroup NUMERIC(18,0)

	-- IF NoColumns=0 CHECK IF MASTER FORM CONFIGURATION IS CREATED BY ADMINISTRATOR
	IF @Nocolumns=0
	BEGIN
		SELECT @numUserGroup = numGroupID FROM UserMaster WHERE numUserDetailId = @numUserCntID 

		IF (SELECT COUNT(*) FROM BizFormWizardMasterConfiguration WHERE numDomainID = @numDomainID AND numFormID = @numFormId AND numRelCntType=@numFormId AND bitGridConfiguration = 1 AND numGroupID = @numUserGroup) > 0
		BEGIN
			SET @IsMasterConfAvailable = 1
		END
	END

	--IF MASTERCONFIGURATION IS AVAILABLE THEN LOAD IT OTHERWISE LOAD DEFAULT COLUMNS
	IF @IsMasterConfAvailable = 1
	BEGIN
		INSERT INTO 
			#tempForm
		SELECT 
			(intRowNum + 1) as tintOrder, vcDbColumnName,	vcOrigDbColumnName,	ISNULL(vcCultureFieldName,vcFieldName), vcAssociatedControlType,
			vcListItemType, numListID, vcLookBackTableName,	bitCustom, numFieldId, bitAllowSorting,	bitInlineEdit, bitIsRequired,
			bitIsEmail,	bitIsAlphaNumeric, bitIsNumeric, bitIsLengthValidation,	intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage vcFieldMessage,
			ListRelID,intColumnWidth,bitAllowFiltering,vcFieldDataType 
		FROM 
			View_DynamicColumnsMasterConfig
		WHERE
			View_DynamicColumnsMasterConfig.numFormId=@numFormId AND 
			View_DynamicColumnsMasterConfig.numDomainID=@numDomainID AND 
			View_DynamicColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
			View_DynamicColumnsMasterConfig.numRelCntType=@numFormId AND 
			View_DynamicColumnsMasterConfig.bitGridConfiguration = 1 AND
			ISNULL(View_DynamicColumnsMasterConfig.bitSettingField,0)=1 AND 
			ISNULL(View_DynamicColumnsMasterConfig.bitCustom,0)=0
		UNION
		SELECT 
			tintRow+1 as tintOrder,vcDbColumnName,vcDbColumnName,vcFieldName, vcAssociatedControlType,'' as vcListItemType,numListID,'',
			bitCustom,numFieldId,bitAllowSorting,bitAllowEdit,bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,
			intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage,ListRelID,intColumnWidth,0,''
		FROM 
			View_DynamicCustomColumnsMasterConfig
		WHERE 
			View_DynamicCustomColumnsMasterConfig.numFormId=@numFormId AND 
			View_DynamicCustomColumnsMasterConfig.numDomainID=@numDomainID AND 
			View_DynamicCustomColumnsMasterConfig.numAuthGroupID = @numUserGroup AND
			View_DynamicCustomColumnsMasterConfig.numRelCntType=@numFormId AND 
			View_DynamicCustomColumnsMasterConfig.bitGridConfiguration = 1 AND
			ISNULL(View_DynamicCustomColumnsMasterConfig.bitCustom,0)=1
		ORDER BY 
			tintOrder ASC  
	END
	ELSE                                        
	BEGIN 
		IF @Nocolumns=0
		BEGIN
			INSERT INTO DycFormConfigurationDetails 
			(
				numFormID,numFieldID,intColumnNum,intRowNum,numDomainID,numUserCntID,numRelCntType,tintPageType,bitCustom,intColumnWidth
			)
			SELECT 
				@numFormId,numFieldId,0,Row_number() over(order by tintRow desc),@numDomainID,@numUserCntID,@numFormId,1,0,intColumnWidth
			FROM 
				View_DynamicDefaultColumns
			WHERE 
				numFormId=@numFormId 
				AND bitDefault=1 
				AND ISNULL(bitSettingField,0)=1 
				AND numDomainID=@numDomainID
			ORDER BY 
				tintOrder ASC  
		END
  

		INSERT INTO 
			#tempForm
		SELECT 
			tintRow+1 as tintOrder,vcDbColumnName,vcOrigDbColumnName,ISNULL(vcCultureFieldName,vcFieldName),vcAssociatedControlType,vcListItemType,numListID,vcLookBackTableName,
			bitCustom,numFieldId,bitAllowSorting,bitInlineEdit,bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,intMaxLength,intMinLength,bitFieldMessage,
			vcFieldMessage vcFieldMessage,ListRelID,intColumnWidth,bitAllowFiltering,vcFieldDataType  
		FROM 
			View_DynamicColumns 
		WHERE 
			numFormId=@numFormId 
			AND numUserCntID=@numUserCntID 
			AND numDomainID=@numDomainID 
			AND tintPageType=1 
			AND ISNULL(bitSettingField,0)=1 
			AND ISNULL(bitCustom,0)=0  
			AND numRelCntType = @numFormId
		UNION
		SELECT 
			tintRow+1 AS tintOrder,vcDbColumnName,vcDbColumnName,vcFieldName,vcAssociatedControlType,'' as vcListItemType,numListID,'',bitCustom,numFieldId,bitAllowSorting,bitAllowEdit,
			bitIsRequired,bitIsEmail,bitIsAlphaNumeric,bitIsNumeric,bitIsLengthValidation,intMaxLength,intMinLength,bitFieldMessage,vcFieldMessage,ListRelID,intColumnWidth,0,'' 
		FROM 
			View_DynamicCustomColumns
		WHERE 
			numFormId=@numFormId 
			AND numUserCntID=@numUserCntID 
			AND numDomainID=@numDomainID 
			AND tintPageType=1 
			AND ISNULL(bitCustom,0)=1 
			AND numRelCntType = @numFormId
		ORDER BY 
			tintOrder ASC             
	END
	
	DECLARE  @strColumns AS VARCHAR(MAX)
	SET @strColumns = ' ADC.numContactId, DM.numDivisionID, ISNULL(DM.numTerID,0) as numTerID, Opp.numRecOwner, DM.tintCRMType, Opp.numOppID, ISNULL(Opp.numBusinessProcessID,0) AS numBusinessProcessID'
        
	
	DECLARE @tintOrder as tinyint  = 0                                                  
	DECLARE @vcFieldName as varchar(50)                                                      
	DECLARE @vcListItemType as varchar(3)                                                 
	DECLARE @vcListItemType1 as varchar(1)                                                     
	DECLARE @vcAssociatedControlType varchar(10)                                                      
	DECLARE @numListID AS numeric(9)                                                      
	DECLARE @vcDbColumnName varchar(30)                          
	DECLARE @WhereCondition varchar(2000) = ''                          
	DECLARE @vcLookBackTableName varchar(2000)                    
	DECLARE @bitCustom as bit              
	DECLARE @numFieldId as numeric  
	DECLARE @bitAllowSorting AS CHAR(1)   
	DECLARE @bitAllowEdit AS CHAR(1)                 
	DECLARE @vcColumnName AS VARCHAR(500)                  
	Declare @ListRelID AS NUMERIC(9) 
	DECLARE @SearchQuery AS VARCHAR(MAX) = ''   

	SELECT TOP 1
		@tintOrder=tintOrder+1,@vcDbColumnName=vcDbColumnName,@vcFieldName=vcFieldName, @vcAssociatedControlType=vcAssociatedControlType,@vcListItemType=vcListItemType,
		@numListID=numListID,@vcLookBackTableName=vcLookBackTableName,@bitCustom=bitCustomField,@numFieldId=numFieldId,@bitAllowSorting=bitAllowSorting,@bitAllowEdit=bitAllowEdit,
		@ListRelID=ListRelID
	FROM 
		#tempForm 
	ORDER BY 
		tintOrder asc            
                                                   
	WHILE @tintOrder>0                                                      
	BEGIN 
		IF @bitCustom = 0            
		BEGIN 
			DECLARE @Prefix as varchar(5)                                               
			
			IF @vcLookBackTableName = 'AdditionalContactsInformation'                    
				SET @Prefix = 'ADC.'                    
			IF @vcLookBackTableName = 'DivisionMaster'                    
				SET @Prefix = 'DM.'                    
			IF @vcLookBackTableName = 'OpportunityMaster'                    
				SET @PreFix ='Opp.'      
			IF @vcLookBackTableName = 'OpportunityRecurring'
				SET @PreFix = 'OPR.'
			IF @vcLookBackTableName = 'CompanyInfo'
				SET @PreFix = 'cmp.'
			IF @vcLookBackTableName = 'ProjectProgress'
				SET @PreFix = 'PP'

			SET @vcColumnName= @vcDbColumnName+'~'+ convert(varchar(10),@numFieldId) +'~0'
      
			IF @vcAssociatedControlType='SelectBox'                                                      
			BEGIN  
				IF @vcDbColumnName = 'numStatus'
				BEGIN
					SET @strColumns=@strColumns+ ' ,(SELECT COUNT(OpportunityItems.numoppitemtCode) FROM OpportunityItems 
					WHERE OpportunityItems.bitItemPriceApprovalRequired=1 AND OpportunityItems.numOppId=Opp.numOppId) ApprovalMarginCount,ISNULL(Opp.intPromotionApprovalStatus,0) AS intPromotionApprovalStatus '
				END
				ELSE IF @vcDbColumnName='numPartenerSource'
				BEGIN
					SET @strColumns=@strColumns+ ' ,(Opp.numPartner) '+' ['+ @vcColumnName+']'    
					SET @strColumns=@strColumns+ ' ,(SELECT D.vcPartnerCode+''-''+C.vcCompanyName FROM DivisionMaster AS D LEFT JOIN CompanyInfo AS C ON D.numCompanyID=C.numCompanyID WHERE D.numDivisionID=Opp.numPartner) AS vcPartenerSource' 
				END
				ELSE IF @vcDbColumnName = 'numPartenerContact'
				BEGIN
					SET @strColumns=@strColumns+ ' ,(Opp.numPartenerContact) '+' ['+ @vcColumnName+']'    
					SET @strColumns=@strColumns+ ' ,(SELECT A.vcFirstName+'' ''+ A.vcLastName FROM AdditionalContactsInformation AS A WHERE A.numContactId=Opp.numPartenerContact) AS vcPartenerContact' 
				END
				ELSE IF @vcDbColumnName = 'tintSource'
				BEGIN
					SET @strColumns = @strColumns + ',dbo.fn_GetOpportunitySourceValue(ISNULL(Opp.tintSource,0),ISNULL(Opp.tintSourceType,0),Opp.numDomainID) ' + ' [' + @vcColumnName + ']'

					IF LEN(@SearchText) > 0
					BEGIN
						SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + 'dbo.fn_GetOpportunitySourceValue(ISNULL(Opp.tintSource,0),ISNULL(Opp.tintSourceType,0),Opp.numDomainID) LIKE ''%' + @SearchText + '%'''
					END
				END
				ELSE IF @vcDbColumnName = 'numContactId'
				BEGIN
					SET @strColumns = @strColumns + ',dbo.fn_GetContactName('+@Prefix+@vcDbColumnName+') ['+ @vcColumnName+']'     
					
					IF LEN(@SearchText) > 0
					BEGIN
						SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + 'dbo.fn_GetContactName('+@Prefix+@vcDbColumnName+') LIKE ''%' + @SearchText + '%'''
					END             
				END
				ELSE if @vcListItemType='LI'                                                       
				BEGIN
					SET @strColumns = @strColumns+',L'+ convert(varchar(3),@tintOrder)+'.vcData'+' ['+ @vcColumnName+']'                                                      
					
					IF LEN(@SearchText) > 0
					BEGIN
						SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + 'L' + CONVERT(VARCHAR(3),@tintOrder) + '.vcData LIKE ''%' + @SearchText + '%'''
					END

					SET @WhereCondition= @WhereCondition +' left Join ListDetails L'+ convert(varchar(3),@tintOrder)+ ' on L'+convert(varchar(3),@tintOrder)+ '.numListItemID='+@vcDbColumnName                                                      
				END                                                      
				ELSE IF @vcListItemType='S'                                                       
				BEGIN
					SET @strColumns = @strColumns+',S'+ convert(varchar(3),@tintOrder)+'.vcState'+' ['+ @vcColumnName+']'   
					
					IF LEN(@SearchText) > 0
					BEGIN
						SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + 'S'+ convert(varchar(3),@tintOrder)+'.vcState LIKE ''%' + @SearchText + '%'''
					END
					                                                   
					SET @WhereCondition= @WhereCondition +' left join State S'+ convert(varchar(3),@tintOrder)+ ' on S'+convert(varchar(3),@tintOrder)+ '.numStateID='+@vcDbColumnName                                                      
				END     
				ELSE IF @vcListItemType = 'BP'
				BEGIN
					SET @strColumns = @strColumns + ',SPLM.Slp_Name' + ' ['	+ @vcColumnName + ']'

					IF LEN(@SearchText) > 0
					BEGIN
						SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + 'SPLM.Slp_Name LIKE ''%' + @SearchText + '%'''
					END

					SET @WhereCondition = @WhereCondition + ' LEFT JOIN Sales_process_List_Master SPLM on SPLM.Slp_Id = OPP.numBusinessProcessID '
				END
				ELSE IF @vcListItemType='PP'
				BEGIN
					SET @strColumns = @strColumns+', CASE WHEN PP.numOppID >0 then ISNULL(PP.intTotalProgress,0) ELSE dbo.GetListIemName(OPP.numPercentageComplete) END '+' ['+ @vcColumnName+']'

					IF LEN(@SearchText) > 0
					BEGIN
						SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + '(CASE WHEN PP.numOppID >0 then CONCAT(ISNULL(PP.intTotalProgress,0),''%'') ELSE dbo.GetListIemName(OPP.numPercentageComplete) END) LIKE ''%' + @SearchText + '%'''
					END
				END  
				ELSE IF @vcListItemType='T'                                                       
				BEGIN
					SET @strColumns = @strColumns+',L'+ convert(varchar(3),@tintOrder)+'.vcData'+' ['+ @vcColumnName+']' 
					
					IF LEN(@SearchText) > 0
					BEGIN
						SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + 'L'+ convert(varchar(3),@tintOrder)+'.vcData LIKE ''%' + @SearchText + '%'''
					END
					                                                     
					SET @WhereCondition= @WhereCondition +' left Join ListDetails L'+ convert(varchar(3),@tintOrder)+ ' on L'+convert(varchar(3),@tintOrder)+ '.numListItemID='+@vcDbColumnName                                                      
				END                     
				ELSE IF @vcListItemType='U'                                                   
				BEGIN
					SET @strColumns = @strColumns + ',dbo.fn_GetContactName('+@Prefix+@vcDbColumnName+') ['+ @vcColumnName+']'     
					
					IF LEN(@SearchText) > 0
					BEGIN
						SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + 'dbo.fn_GetContactName('+@Prefix+@vcDbColumnName+') LIKE ''%' + @SearchText + '%'''
					END              
				END  
				ELSE IF @vcListItemType = 'WI'
				BEGIN
					SET @strColumns = @strColumns + ', Case when isnull(OL.numSiteID,0)>0 then Sites.vcSiteName else WebAPI.vcProviderName end [' + @vcColumnName + ']'
				  
					IF LEN(@SearchText) > 0
					BEGIN
						SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + '(Case when isnull(OL.numSiteID,0)>0 then Sites.vcSiteName else WebAPI.vcProviderName end) LIKE ''%' + @SearchText + '%'''
					END 

					SET @WhereCondition= @WhereCondition +' left join WebAPI on WebAPI.WebApiId=isnull(OL.numWebApiId,0)
															left join Sites on isnull(OL.numSiteID,0)=Sites.numSIteID and Sites.numDomainID=' + convert(varchar(15),@numDomainID)                                                    
			   END                  
			END               
			ELSE IF @vcAssociatedControlType='DateField'                                                      
			BEGIN  
				SET @strColumns=@strColumns+',case when convert(varchar(11),'+@Prefix+@vcDbColumnName+')= convert(varchar(11),getdate()) then ''<b><font color=red>Today</font></b>'''      
				SET @strColumns=@strColumns+'when convert(varchar(11),'+@Prefix+@vcDbColumnName+')= convert(varchar(11),dateadd(day,-1,getdate())) then ''<b><font color=purple>YesterDay</font></b>'''      
				SET @strColumns=@strColumns+'when convert(varchar(11),'+@Prefix+@vcDbColumnName+')= convert(varchar(11),dateadd(day,1,getdate())) then''<b><font color=orange>Tommorow</font></b>'' '      
				SET @strColumns=@strColumns+'else dbo.FormatedDateFromDate('+@Prefix+@vcDbColumnName+','+convert(varchar(10),@numDomainId)+') end  ['+ @vcColumnName+']'               

				IF LEN(@SearchText) > 0
				BEGIN
					SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + 'dbo.FormatedDateFromDate('+@Prefix+@vcDbColumnName+','+ CONVERT(VARCHAR(10),@numDomainId)+') LIKE ''%' + @SearchText + '%'''
				END
			END              
			ELSE IF @vcAssociatedControlType='TextBox'                                                      
			BEGIN  
				SET @strColumns = @strColumns + ','+ CASE 
														WHEN @vcDbColumnName='numAge' then 'year(getutcdate())-year(bintDOB)'                   
														WHEN @vcDbColumnName='monPAmount' then 'isnull((SELECT varCurrSymbol FROM [Currency] WHERE numCurrencyID = Opp.[numCurrencyID]),'''') + '' ''+ CONVERT(VARCHAR,CONVERT(MONEY,monPAmount),1)'
														WHEN @vcDbColumnName='numDivisionID' then 'DM.numDivisionID'
														WHEN @vcDbColumnName = 'tintOppStatus' THEN 'CASE tintOppStatus WHEN 0 then ''Open'' WHEN 1 THEN ''Closed-Won'' WHEN 2 THEN ''Closed-Lost'' END '
														WHEN @vcDbColumnName = 'tintOppType' THEN 'CASE tintOppType WHEN 1 then ''Sales'' WHEN 2 THEN ''Purchase'' Else ''NA'' END ' 
														ELSE @Prefix + @vcDbColumnName 
													END +' ['+ @vcColumnName+']'  
				
				IF LEN(@SearchText) > 0
				BEGIN
					SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + CASE 
														WHEN @vcDbColumnName='numAge' then 'year(getutcdate())-year(bintDOB)'                   
														WHEN @vcDbColumnName='monPAmount' then 'isnull((SELECT varCurrSymbol FROM [Currency] WHERE numCurrencyID = Opp.[numCurrencyID]),'''') + '' ''+ CONVERT(VARCHAR,CONVERT(MONEY,monPAmount),1)'
														WHEN @vcDbColumnName='numDivisionID' then 'DM.numDivisionID'
														WHEN @vcDbColumnName = 'tintOppStatus' THEN 'CASE tintOppStatus WHEN 0 then ''Open'' WHEN 1 THEN ''Closed-Won'' WHEN 2 THEN ''Closed-Lost'' END '
														WHEN @vcDbColumnName = 'tintOppType' THEN 'CASE tintOppType WHEN 1 then ''Sales'' WHEN 2 THEN ''Purchase'' Else ''NA'' END ' 
														ELSE @Prefix + @vcDbColumnName 
													END + ' LIKE ''%' + @SearchText + '%'''
				END              
			END  
			ELSE IF @vcAssociatedControlType='TextArea'                                              
			BEGIN  
				SET @strColumns=@strColumns+','+  @Prefix + @vcDbColumnName +' ['+ @vcColumnName+']' 

				IF LEN(@SearchText) > 0
				BEGIN
					SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + @Prefix + @vcDbColumnName + ' LIKE ''%' + @SearchText + '%'''
				END     
			END
			ELSE IF @vcAssociatedControlType='Label'                                              
			BEGIN  
				SET @strColumns = @strColumns + ',' + CASE 
												WHEN @vcDbColumnName = 'CalAmount' THEN '[dbo].[getdealamount](Opp.numOppId,Getutcdate(),0)'
												WHEN @vcLookBackTableName = 'OpportunityBizDocs' AND @vcDbColumnName = 'monDealAmount' THEN 'ISNULL((SELECT SUM(ISNULL(monDealAmount,0)) FROM   OpportunityBizDocs BD WHERE  BD.numOppId = Opp.numOppId AND ISNULL(bitAuthoritativeBizDocs,0)=1),0)'
												WHEN @vcLookBackTableName = 'OpportunityBizDocs' AND @vcDbColumnName = 'monAmountPaid' THEN 'ISNULL((SELECT SUM(ISNULL(monAmountPaid,0)) FROM   OpportunityBizDocs BD WHERE  BD.numOppId = Opp.numOppId AND ISNULL(bitAuthoritativeBizDocs,0)=1),0)'
												WHEN @vcLookBackTableName = 'OpportunityBizDocs' AND @vcDbColumnName = 'vcBizDocsList' THEN 'ISNULL((SELECT SUBSTRING((SELECT ''$^$'' + CAST(numOppBizDocsId AS VARCHAR(18)) +''#^#''+ vcBizDocID
												FROM OpportunityBizDocs WHERE numOppId=Opp.numOppId FOR XML PATH('''')),4,200000)),'''')'
												else @Prefix + @vcDbColumnName 
											END +' ['+ @vcColumnName+']'     
											
				IF LEN(@SearchText) > 0
				BEGIN
					SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + CASE 
												WHEN @vcDbColumnName = 'CalAmount' THEN '[dbo].[getdealamount](Opp.numOppId,Getutcdate(),0)'
												WHEN @vcLookBackTableName = 'OpportunityBizDocs' AND @vcDbColumnName = 'monDealAmount' THEN 'ISNULL((SELECT SUM(ISNULL(monDealAmount,0)) FROM   OpportunityBizDocs BD WHERE  BD.numOppId = Opp.numOppId AND ISNULL(bitAuthoritativeBizDocs,0)=1),0)'
												WHEN @vcLookBackTableName = 'OpportunityBizDocs' AND @vcDbColumnName = 'monAmountPaid' THEN 'ISNULL((SELECT SUM(ISNULL(monAmountPaid,0)) FROM   OpportunityBizDocs BD WHERE  BD.numOppId = Opp.numOppId AND ISNULL(bitAuthoritativeBizDocs,0)=1),0)'
												WHEN @vcLookBackTableName = 'OpportunityBizDocs' AND @vcDbColumnName = 'vcBizDocsList' THEN 'ISNULL((SELECT SUBSTRING((SELECT ''$^$'' + CAST(numOppBizDocsId AS VARCHAR(18)) +''#^#''+ vcBizDocID
												FROM OpportunityBizDocs WHERE numOppId=Opp.numOppId FOR XML PATH('''')),4,200000)),'''')'
												else @Prefix + @vcDbColumnName
											END + ' LIKE ''%' + @SearchText + '%'''
				END           
			END
  			ELSE IF @vcAssociatedControlType='Popup' and @vcDbColumnName='numShareWith'                                                   
			BEGIN  
				SET @strColumns = @strColumns+',(SELECT SUBSTRING(
				(SELECT '','' + A.vcFirstName+'' ''+A.vcLastName + CASE WHEN ISNULL(SR.numContactType,0)>0 THEN ''('' + dbo.fn_GetListItemName(SR.numContactType) + '')'' ELSE '''' END
				FROM ShareRecord SR JOIN UserMaster UM ON UM.numUserDetailId=SR.numAssignedTo 
											JOIN AdditionalContactsInformation A ON UM.numUserDetailId=A.numContactID 
						WHERE SR.numDomainID=Opp.numDomainID AND SR.numModuleID=3 AND SR.numRecordID=Opp.numOppId
							AND UM.numDomainID=Opp.numDomainID and UM.numDomainID=A.numDomainID FOR XML PATH('''')),2,200000)) ['+ @vcColumnName+']'    
				
				IF LEN(@SearchText) > 0
				BEGIN
					SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + '(SELECT SUBSTRING(
						(SELECT '','' + A.vcFirstName+'' ''+A.vcLastName + CASE WHEN ISNULL(SR.numContactType,0)>0 THEN ''('' + dbo.fn_GetListItemName(SR.numContactType) + '')'' ELSE '''' END
						FROM ShareRecord SR JOIN UserMaster UM ON UM.numUserDetailId=SR.numAssignedTo 
													JOIN AdditionalContactsInformation A ON UM.numUserDetailId=A.numContactID 
								WHERE SR.numDomainID=Opp.numDomainID AND SR.numModuleID=3 AND SR.numRecordID=Opp.numOppId
									AND UM.numDomainID=Opp.numDomainID and UM.numDomainID=A.numDomainID FOR XML PATH('''')),2,200000)) LIKE ''%' + @SearchText + '%'''
				END            
			END  
		END               
		Else                          
		BEGIN 
			SET @vcColumnName= @vcDbColumnName +'~'+ convert(varchar(10),@numFieldId)+'~1'
               
			IF @vcAssociatedControlType = 'TextBox' or @vcAssociatedControlType = 'TextArea'          
			BEGIN              
				SET @strColumns= @strColumns+',CFW'+ convert(varchar(3),@tintOrder)+'.Fld_Value  ['+ @vcColumnName + ']'               
				SET @WhereCondition= @WhereCondition +' left Join CFW_FLD_Values_Opp CFW'+ convert(varchar(3),@tintOrder)+ '             
				on CFW'+ convert(varchar(3),@tintOrder)+ '.Fld_Id='+convert(varchar(10),@numFieldId) +'and CFW'+ convert(varchar(3),@tintOrder)+ '.RecId=Opp.numOppid   '                                                     
			END            
			ELSE IF @vcAssociatedControlType = 'CheckBox'         
			BEGIN          
				SET @strColumns = @strColumns + ',case when isnull(CFW'+ CONVERT(VARCHAR(3),@tintOrder)+'.Fld_Value,0)=0 then ''No'' when isnull(CFW'+ CONVERT(VARCHAR(3),@tintOrder)+'.Fld_Value,0)=1 then ''Yes'' end   ['+ @vcColumnName + ']'             
				SET @WhereCondition= @WhereCondition +' left Join CFW_FLD_Values_Opp CFW'+ convert(varchar(3),@tintOrder)+ '           
					on CFW'+ convert(varchar(3),@tintOrder)+ '.Fld_Id='+convert(varchar(10),@numFieldId) +'and CFW'+ convert(varchar(3),@tintOrder)+ '.RecId=Opp.numOppid   '                                                   
			END          
			ELSE IF @vcAssociatedControlType = 'DateField'       
			BEGIN            
				set @strColumns= @strColumns+',dbo.FormatedDateFromDate(CFW'+ convert(varchar(3),@tintOrder)+'.Fld_Value,'+convert(varchar(10),@numDomainId)+')  ['+ @vcColumnName + ']'               
				set @WhereCondition= @WhereCondition +' left Join CFW_FLD_Values_Opp CFW'+ convert(varchar(3),@tintOrder)+ '             
				on CFW'+ convert(varchar(3),@tintOrder)+ '.Fld_Id='+convert(varchar(10),@numFieldId) +'and CFW'+ convert(varchar(3),@tintOrder)+ '.RecId=Opp.numOppid    '                                                     
			END            
			ELSE IF @vcAssociatedControlType = 'SelectBox'         
			BEGIN            
				SET @vcDbColumnName = 'DCust'+Convert(varchar(10),@numFieldId)    
				        
				SET @strColumns = @strColumns+',L'+ convert(varchar(3),@tintOrder)+'.vcData'+' ['+ @vcColumnName + ']'                                                      
				SET @WhereCondition = @WhereCondition +' left Join CFW_FLD_Values_Opp CFW'+ convert(varchar(3),@tintOrder)+ '             
									on CFW'+ convert(varchar(3),@tintOrder)+ '.Fld_Id='+convert(varchar(10),@numFieldId) +'and CFW'+ convert(varchar(3),@tintOrder)+ '.RecId=Opp.numOppid     '                                                     
				SET @WhereCondition = @WhereCondition +' left Join ListDetails L'+ convert(varchar(3),@tintOrder)+ ' on L'+convert(varchar(3),@tintOrder)+ '.numListItemID=CFW'+ convert(varchar(3),@tintOrder)+'.Fld_Value' 
			END
			ELSE
			BEGIN
				SET @strColumns = @strColumns + CONCAT(',dbo.GetCustFldValueOpp(',@numFieldId,',opp.numOppID)') + ' [' + @vcColumnName + ']'
			END
		   
			IF LEN(@SearchText) > 0
			BEGIN
				SET @SearchQuery = @SearchQuery + (CASE WHEN LEN(@SearchQuery) > 0 THEN ' OR ' ELSE '' END) + CONCAT('dbo.GetCustFldValueOpp(',@numFieldId,',opp.numOppID) LIKE ''%',@SearchText,'%''')
			END        
		END 
  
		SELECT TOP 1 
			@tintOrder=tintOrder+1,@vcDbColumnName=vcDbColumnName,@vcFieldName=vcFieldName,@vcAssociatedControlType=vcAssociatedControlType,@vcListItemType=vcListItemType,
			@numListID=numListID,@vcLookBackTableName=vcLookBackTableName,@bitCustom=bitCustomField,@numFieldId=numFieldId,@bitAllowSorting=bitAllowSorting,@bitAllowEdit=bitAllowEdit,
			@ListRelID=ListRelID
		FROM 
			#tempForm 
		WHERE 
			tintOrder > @tintOrder-1 
		ORDER BY 
			tintOrder asc            
 
		IF @@rowcount=0 
			SET @tintOrder=0 
	END                            
		
	 
	DECLARE @strShareRedordWith AS VARCHAR(300);
	SET @strShareRedordWith=' Opp.numOppId in (select SR.numRecordID FROM ShareRecord SR WHERE SR.numDomainID=' + CONVERT(VARCHAR(15),@numDomainID) + ' AND SR.numModuleID=3 AND SR.numAssignedTo=' + CONVERT(VARCHAR(15),@numUserCntId) + ') '
                                             
	DECLARE @strSql AS VARCHAR(MAX) = ''                                       
                                                                                 
	SET @strSql = ' FROM 
						OpportunityMaster Opp                                                                     
					INNER JOIN 
						AdditionalContactsInformation ADC                                                                     
					ON
						Opp.numContactId = ADC.numContactId                                  
					INNER JOIN 
						DivisionMaster DM                                                                     
					ON 
						Opp.numDivisionId = DM.numDivisionID AND ADC.numDivisionId = DM.numDivisionID                                                                     
					INNER JOIN 
						CompanyInfo cmp                                                                     
					ON 
						DM.numCompanyID = cmp.numCompanyId                                                                         
					LEFT JOIN 
						AdditionalContactsInformation ADC1 
					ON 
						ADC1.numContactId=Opp.numRecOwner 
					LEFT JOIN 
						ProjectProgress PP 
					ON 
						PP.numOppID = Opp.numOppID
					LEFT JOIN 
						OpportunityLinking OL 
					on 
						OL.numChildOppID=Opp.numOppId ' + @WhereCondition
                                     

	-------Change Row Color-------
	DECLARE @vcCSOrigDbCOlumnName AS VARCHAR(50) = ''
	DECLARE @vcCSLookBackTableName AS VARCHAR(50) = ''
	DECLARE @vcCSAssociatedControlType AS VARCHAR(50) = ''

	CREATE TABLE #tempColorScheme
	(
		vcOrigDbCOlumnName VARCHAR(50),
		vcLookBackTableName VARCHAR(50),
		vcFieldValue VARCHAR(50),
		vcFieldValue1 VARCHAR(50),
		vcColorScheme VARCHAR(50),
		vcAssociatedControlType VARCHAR(50)
	)

	INSERT INTO 
		#tempColorScheme 
	SELECT 
		DFM.vcOrigDbCOlumnName,
		DFM.vcLookBackTableName,
		DFCS.vcFieldValue,
		DFCS.vcFieldValue1,
		DFCS.vcColorScheme,
		DFFM.vcAssociatedControlType 
	FROM 
		DycFieldColorScheme DFCS 
	JOIN 
		DycFormField_Mapping DFFM 
	ON 
		DFCS.numFieldID=DFFM.numFieldID
	JOIN 
		DycFieldMaster DFM 
	ON 
		DFM.numModuleID=DFFM.numModuleID 
		AND DFM.numFieldID=DFFM.numFieldID
	WHERE 
		DFCS.numDomainID=@numDomainID 
		AND DFFM.numFormID=@numFormId 
		AND DFCS.numFormID=@numFormId 
		AND ISNULL(DFFM.bitAllowGridColor,0)=1

	IF(SELECT COUNT(*) FROM #tempColorScheme)>0
	BEGIN
		SELECT TOP 1 @vcCSOrigDbCOlumnName=vcOrigDbCOlumnName,@vcCSLookBackTableName=vcLookBackTableName,@vcCSAssociatedControlType=vcAssociatedControlType FROM #tempColorScheme
	END   

	IF len(@vcCSOrigDbCOlumnName)>0 and len(@vcCSLookBackTableName)>0
	BEGIN
		SET @strColumns = @strColumns + ',tCS.vcColorScheme'
	END

	IF len(@vcCSOrigDbCOlumnName)>0 and len(@vcCSLookBackTableName)>0
	BEGIN
		IF @vcCSLookBackTableName = 'AdditionalContactsInformation'                  
			SET @Prefix = 'ADC.'                  
		IF @vcCSLookBackTableName = 'DivisionMaster'                  
			SET @Prefix = 'DM.'
		IF @vcCSLookBackTableName = 'CompanyInfo'                  
			SET @Prefix = 'CMP.' 
		IF @vcCSLookBackTableName = 'OpportunityMaster'                  
			SET @Prefix = 'Opp.'
		
		IF @vcCSAssociatedControlType='DateField'
		BEGIN
			SET @strSql=@strSql+' LEFT JOIN #tempColorScheme tCS on CONVERT(VARCHAR(10),' + @Prefix + @vcCSOrigDbCOlumnName + ',111) >= CONVERT(VARCHAR(10),DateAdd(day,cast(tCS.vcFieldValue as int),GETUTCDATE()),111)
					and CONVERT(VARCHAR(10),' + @Prefix + @vcCSOrigDbCOlumnName + ',111) <= CONVERT(VARCHAR(10),DateAdd(day,cast(tCS.vcFieldValue1 as int),GETUTCDATE()),111)'
		END
		ELSE IF @vcCSAssociatedControlType='SelectBox' OR @vcCSAssociatedControlType='ListBox' OR @vcCSAssociatedControlType='CheckBox'
		BEGIN
			SET @strSql=@strSql+' LEFT JOIN #tempColorScheme tCS on tCS.vcFieldValue=' + @Prefix + @vcCSOrigDbCOlumnName
		END
	END      

            
	IF @columnName like 'CFW.Cust%'             
	BEGIN
		SET @strSql = @strSql +' LEFT JOIN CFW_Fld_Values_Opp CFW on CFW.RecId=Opp.numOppid  and CFW.fld_id= ' + REPLACE(@columnName,'CFW.Cust','') +' '                                                     
		SET @columnName='CFW.Fld_Value'   
	END
	
	ELSE IF @columnName like 'DCust%'            
	BEGIN            
		SET @strSql = @strSql + ' left Join CFW_Fld_Values_Opp CFW on CFW.RecId=Opp.numOppid   and CFW.fld_id= '+replace(@columnName,'DCust','')                                                                                               
		SET @strSql = @strSql + ' left Join ListDetails LstCF on LstCF.numListItemID=CFW.Fld_Value  '            
		set @columnName='LstCF.vcData'                   
	END
                                        
	IF @bitPartner=1 
	BEGIN
		SET @strSql=@strSql+' LEFT JOIN OpportunityContact OppCont ON OppCont.numOppId=Opp.numOppId'                                                                               
	END

	SET @strSql = @strSql +' WHERE Opp.tintOppstatus=0 AND DM.numDomainID=' + CONVERT(VARCHAR(15),@numDomainID) + ' AND Opp.tintOppType= ' + convert(varchar(1),@OppType)                                                              

	IF @SortChar <> '0' 
		SET @strSql = @strSql + ' And Opp.vcPOppName like ''' + @SortChar + '%'''
                                
	IF @numDivisionID <> 0 
	BEGIN
		SET @strSql = @strSql + ' AND DM.numDivisionID =' + CONVERT(VARCHAR(15),@numDivisionID)                              
    END
	                                                                
	IF @tintUserRightType=1 
	BEGIN
		SET @strSql = @strSql + ' AND (Opp.numRecOwner = '
							+ CONVERT(VARCHAR(15),@numUserCntID) 
							+ ' or Opp.numAssignedTo= '
							+ convert(varchar(15),@numUserCntID)  
							+' or  Opp.numOppId in (select distinct(numOppId) from OpportunityStageDetails where numAssignTo ='
							+ convert(varchar(15),@numUserCntID)+')'
							+ case when @bitPartner=1 then ' or ( OppCont.bitPartner=1 and OppCont.numContactId='+convert(varchar(15),@numUserCntID)+ ')' else '' end 
							+ ' or ' + @strShareRedordWith +')'
    END                                        
	ELSE IF @tintUserRightType=2 
	BEGIN
		SET @strSql = @strSql + ' AND (DM.numTerID in (select numTerritoryID from  UserTerritory where numUserCntID= '
							+ convert(varchar(15),@numUserCntID)+' ) or DM.numTerID=0 or Opp.numAssignedTo= '
							+ convert(varchar(15),@numUserCntID)  +' or  Opp.numOppId in (select distinct(numOppId) from OpportunityStageDetails where numAssignTo ='
							+ convert(varchar(15),@numUserCntID)+')' + ' or ' + @strShareRedordWith +')'                                                                        
	END           
	
	                                                  
	IF @tintSortOrder=1 
	BEGIN 
		SET @strSql = @strSql + ' AND (Opp.numRecOwner = ' 
							+ CONVERT(VARCHAR(15),@numUserCntID) 
							+ ' or Opp.numAssignedTo= '+ convert(varchar(15),@numUserCntID) 
							+ ' or  Opp.numOppId in (select distinct(numOppId) from OpportunityStageDetails where numAssignTo ='
							+ convert(varchar(15),@numUserCntID)+')'
							+ case when @bitPartner=1 then ' or ( OppCont.bitPartner=1 and OppCont.numContactId='+convert(varchar(15),@numUserCntID)+ ')' else '' end 
							+ ' or ' + @strShareRedordWith +')'                        
    END
	ELSE IF @tintSortOrder=2  
	BEGIN
		SET @strSql = @strSql + 'AND (Opp.numRecOwner in (select A.numContactID from AdditionalContactsInformation A where A.numManagerID='
							+ convert(varchar(15),@numUserCntID) 
							+') or Opp.numAssignedTo in (select A.numContactID from AdditionalContactsInformation A where A.numManagerID='
							+ convert(varchar(15),@numUserCntID)  
							+') or  Opp.numOppId in (select distinct(numOppId) from OpportunityStageDetails where numAssignTo in (select A.numContactID from AdditionalContactsInformation A where A.numManagerID='
							+ convert(varchar(15),@numUserCntID)+'))'
							+ case when @bitPartner=1 then ' or ( OppCont.bitPartner=1 and OppCont.numContactId in (select A.numContactID from AdditionalContactsInformation A where A.numManagerID='+ convert(varchar(15),@numUserCntID)+ '))' else '' end 
							+ ' or ' + @strShareRedordWith +')'                                                                    
	END
	ELSE IF @tintSortOrder=4  
	BEGIN
		SET @strSql=@strSql + ' AND Opp.bintCreatedDate> '''+convert(varchar(20),dateadd(day,-8,@dtLastDate))+''''                                                                    
	END
                                                 

	IF @vcRegularSearchCriteria <> '' 
	BEGIN
		SET @strSql = @strSql + ' AND ' + @vcRegularSearchCriteria 
	END

	IF @vcCustomSearchCriteria <> '' 
	BEGIN
		SET @strSql = @strSql + ' AND Opp.numOppid IN (SELECT DISTINCT CFW.RecId FROM CFW_Fld_Values_Opp CFW where ' + @vcCustomSearchCriteria + ')'
	END

	IF LEN(@SearchQuery) > 0
	BEGIN
		SET @strSql = @strSql + ' AND (' + @SearchQuery + ') '
	END

	DECLARE @firstRec AS INTEGER                                                              
	DECLARE @lastRec AS INTEGER    
	                                                 
	SET @firstRec= (@CurrentPage-1) * @PageSize                             
	SET @lastRec= (@CurrentPage*@PageSize+1)                                                                    


    -- GETS ROWS COUNT
	DECLARE @strTotal NVARCHAR(MAX) = 'SELECT @TotalRecords = COUNT(*) ' + @strSql
	exec sp_executesql @strTotal, N'@TotalRecords INT OUT', @TotRecs OUT
               
                              
	-- EXECUTE FINAL QUERY
	DECLARE @strFinal AS VARCHAR(MAX)
	SET @strFinal = CONCAT('SELECT ROW_NUMBER() OVER(ORDER BY ',@columnName,' ',@columnSortOrder,') ID, ',@strColumns,' INTO #tempTable',@strSql,'; SELECT * FROM #tempTable WHERE ID > ',@firstRec,' and ID <',@lastRec,'; DROP TABLE #tempTable;')
	PRINT @strFinal
	EXEC (@strFinal) 

	SELECT * FROM #tempForm

	DROP TABLE #tempForm
	DROP TABLE #tempColorScheme
END
GO
/****** Object:  StoredProcedure [dbo].[USP_GetOpportuntityCommission]    Script Date: 02/28/2009 13:57:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created by Siva
-- [dbo].[USP_GetOpportuntityCommission] 1,1,1,-333,'1/1/2009','1/15/2009'
-- exec USP_GetOpportuntityCommission @numUserId=372,@numUserCntID=82979,@numDomainId=1,@ClientTimeZoneOffset=-330,@dtStartDate='2010-03-01 00:00:00:000',@dtEndDate='2010-04-01 00:00:00:000'
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getopportuntitycommission')
DROP PROCEDURE usp_getopportuntitycommission
GO
CREATE PROCEDURE [dbo].[USP_GetOpportuntityCommission]
(
               @numUserId            AS NUMERIC(9)  = 0,
               @numUserCntID         AS NUMERIC(9)  = 0,
               @numDomainId          AS NUMERIC(9)  = 0,
               @ClientTimeZoneOffset AS INT,
               @dtStartDate          AS DATETIME,
               @dtEndDate            AS DATETIME,
               @numOppBizDocsId AS NUMERIC(9) =0,
               @tintMode AS tinyint,
			   @numComRuleID AS NUMERIC(18,0) = NULL,
			   @numDivisionID NUMERIC(18,0) = NULL
)
AS
  BEGIN
  
  DECLARE @bitIncludeShippingItem AS BIT
  DECLARE @numShippingItemID AS NUMERIC(18,0)
  SELECT @bitIncludeShippingItem=bitIncludeTaxAndShippingInCommission,@numShippingItemID=numShippingServiceItemID FROM Domain WHERE numDomainID = @numDomainId


  IF @tintMode=0  -- Paid Invoice
  BEGIN
		SELECT 
			LD.vcData as BizDoc,
			OBD.numBizDocID,
			OM.numOppId,
			OBD.numOppBizDocsId,
			isnull(OBD.monAmountPaid,0) monAmountPaid,
			OM.bintCreatedDate,
			OM.vcPOppName AS Name,
			Case when OM.tintOppStatus=0 then 'Open' Else 'Close' End as OppStatus, 
			isnull(OBD.monDealAmount,0) as DealAmount,
			Case When (OM.numRecOwner=@numUserCntID and OM.numAssignedTo=@numUserCntID) then  'Deal Owner/Assigned'
				 When OM.numRecOwner=@numUserCntID then 'Deal Owner' 
				 When OM.numAssignedTo=@numUserCntID then 'Deal Assigned' 
			end as EmpRole,
			ISNULL(TempBizDocComission.numComissionAmount,0) as decTotalCommission,
			OBD.vcBizDocID,
			(SELECT SUM(ISNULL(monTotAmount,0)) FROM dbo.OpportunityBizDocItems WHERE numOppBizDocID=OBD.numOppBizDocsId AND 1 = (CASE @bitIncludeShippingItem WHEN 1 THEN 1 ELSE CASE WHEN OpportunityBizDocItems.numItemCode = @numShippingItemID THEN 0 ELSE 1 END END)) AS monSubTotAMount
		FROM
		OpportunityBizDocs OBD 
		INNER JOIN OpportunityMaster OM ON OBD.numOppId =OM.numOppId
		LEFT JOIN ListDetails LD on LD.numListItemID = OBD.numBizDocId 
		CROSS APPLY
		(
			SELECT 
				MAX(DM.dtDepositDate) dtDepositDate
			FROM 
				DepositMaster DM 
			JOIN dbo.DepositeDetails DD	ON DM.numDepositId=DD.numDepositID 
			WHERE 
				DM.tintDepositePage=2 
				AND DM.numDomainId=@numDomainId
				AND DD.numOppID=OBD.numOppID 
				AND DD.numOppBizDocsID =OBD.numOppBizDocsID
		) TEMPDEPOSIT
		CROSS APPLY
		(
			SELECT
				SUM(ISNULL(numComissionAmount,0)) numComissionAmount,
				COUNT(*) AS intCount
			FROM
				BizDocComission BDC
			WHERE
				BDC.numDomainId = @numDomainID
				AND ((BDC.numUserCntID = @numUserCntID AND BDC.tintAssignTo <> 3) OR (BDC.numUserCntID = @numDivisionID AND BDC.tintAssignTo = 3))
				AND BDC.numOppBizDocId = OBD.numOppBizDocsId
				AND bitCommisionPaid=0
				AND (ISNULL(@numComRuleID,0)=0 OR BDC.numComRuleID = @numComRuleID)
		) TempBizDocComission
		WHERE 
			OM.numDomainId=@numDomainId 
			AND OM.tintOppStatus=1 
			AND OM.tintOppType=1 
			AND (OM.numRecOwner=@numUserCntID OR OM.numAssignedTo=@numUserCntID OR OM.numPartenerContact=@numUserCntID OR OM.numPartner=@numDivisionID)
			AND OBD.bitAuthoritativeBizDocs = 1 
			AND isnull(OBD.monAmountPaid,0) >= OBD.monDealAmount 
			AND OBD.numOppBizDocsId IN (SELECT numOppBizDocId FROM BizDocComission WHERE ((numUserCntID = @numUserCntID AND tintAssignTo <> 3) OR (numUserCntID = @numDivisionID AND tintAssignTo = 3)) AND numDomainId=@numDomainId AND bitCommisionPaid=0)
			AND CAST(DATEADD(MINUTE,-@ClientTimeZoneOffset,TEMPDeposit.dtDepositDate) AS DATE) BETWEEN CAST(@dtStartDate AS DATE) AND CAST(@dtEndDate AS DATE)
			AND TempBizDocComission.intCount > 0
  
	END
  ELSE IF @tintMode=1 -- Non Paid Invoice
  BEGIN
		Select 
			vcData as BizDoc,
			oppBiz.numBizDocID,
			Opp.numOppId,
			oppBiz.numOppBizDocsId,
			isnull(oppBiz.monAmountPaid,0) monAmountPaid,
			Opp.bintCreatedDate,
			Opp.vcPOppName AS Name,
			Case when Opp.tintOppStatus=0 then 'Open' Else 'Close' End as OppStatus, 
			isnull(oppBiz.monDealAmount,0) as DealAmount,
            Case 
				When (Opp.numRecOwner=@numUserCntID and Opp.numAssignedTo=@numUserCntID) then  'Deal Owner/Assigned'
				When Opp.numRecOwner=@numUserCntID then 'Deal Owner' 
				When Opp.numAssignedTo=@numUserCntID then 'Deal Assigned' 
			end as EmpRole,
			ISNULL(TempBizDocComission.numComissionAmount,0) as decTotalCommission,oppBiz.vcBizDocID,
			(SELECT SUM(ISNULL(monTotAmount,0)) FROM dbo.OpportunityBizDocItems WHERE numOppBizDocID=oppBiz.numOppBizDocsId AND 1 = (CASE @bitIncludeShippingItem WHEN 1 THEN 1 ELSE CASE WHEN OpportunityBizDocItems.numItemCode = @numShippingItemID THEN 0 ELSE 1 END END)) AS monSubTotAMount
        From OpportunityMaster Opp 
		join OpportunityBizDocs oppBiz on oppBiz.numOppId=opp.numOppId 
        left join ListDetails LD on LD.numListItemID=OPPBIZ.numBizDocId 
		CROSS APPLY
		(
			select 
				SUM(ISNULL(numComissionAmount,0)) numComissionAmount,
				COUNT(*) AS intCount
			from 
				BizDocComission 
			WHERE 
				((numUserCntID = @numUserCntID AND tintAssignTo <> 3) OR (numUserCntID = @numDivisionID AND tintAssignTo = 3))
				AND numDomainId=@numDomainId 
				AND bitCommisionPaid=0 
				and numOppBizDocId=oppBiz.numOppBizDocsId
				AND (ISNULL(@numComRuleID,0)=0 OR numComRuleID = @numComRuleID)
		) TempBizDocComission
        Where 
			oppBiz.bitAuthoritativeBizDocs = 1 AND Opp.tintOppStatus=1 And Opp.tintOppType=1 
			And (Opp.numRecOwner=@numUserCntID or Opp.numAssignedTo=@numUserCntID OR Opp.numPartner=@numDivisionID)
            And Opp.numDomainId=@numDomainId AND oppBiz.bitAuthoritativeBizDocs = 1 AND isnull(oppBiz.monAmountPaid,0) <oppBiz.monDealAmount
			AND ((dateadd(minute,-@ClientTimeZoneOffset,oppBiz.dtFromDate) between @dtStartDate And @dtEndDate)) 
			AND oppBiz.numOppBizDocsId IN (SELECT DISTINCT numOppBizDocId FROM BizDocComission WHERE ((numUserCntID = @numUserCntID AND tintAssignTo <> 3) OR (numUserCntID = @numDivisionID AND tintAssignTo = 3)) AND numDomainId=@numDomainId AND bitCommisionPaid=0) --added by chintan BugID 982
			AND TempBizDocComission.intCount > 0
  END
   ELSE IF @tintMode=2  -- Paid Invoice Detail
   BEGIN
        SELECT * FROM (Select BC.numComissionID,oppBiz.numOppBizDocsId,BC.decCommission,Case when BC.tintComBasedOn = 1 then 'Amount' when BC.tintComBasedOn = 2 then 'Units' end AS BasedOn,
			Case when BC.tintComType = 1 then 'Percentage' when BC.tintComType = 2 then 'Flat' end as CommissionType, 
                        numComissionAmount  as CommissionAmt,I.vcItemName,BDI.numUnitHour,BDI.monPrice,BDI.monTotAmount,ISNULL(OT.monVendorCost, 0)* ISNULL(BDI.[numUnitHour],0) AS VendorCost,
						ISNULL(OT.monAvgCost, 0)* ISNULL(BDI.[numUnitHour],0) AS monAvgCost,
                        (SELECT MAX(DM.dtDepositDate) FROM DepositMaster DM JOIN dbo.DepositeDetails DD
									ON DM.numDepositId=DD.numDepositID WHERE DM.tintDepositePage=2 AND DM.numDomainId=@numDomainId
									AND DD.numOppID=oppBiz.numOppID AND DD.numOppBizDocsID =oppBiz.numOppBizDocsID) AS dtDepositDate          
                              From OpportunityMaster Opp 
							  INNER JOIN OpportunityBizDocs oppBiz on oppBiz.numOppId=opp.numOppId 
                              INNER JOIN BizDocComission BC on BC.numOppBizDocId=oppBiz.numOppBizDocsId and ((BC.numUserCntID = @numUserCntID AND BC.tintAssignTo <> 3) OR (BC.numUserCntID = @numDivisionID AND BC.tintAssignTo = 3))
                              INNER JOIN OpportunityBizDocItems BDI ON oppBiz.numOppBizDocsId=BDI.numOppBizDocID AND BC.numOppBizDocItemID=BDI.numOppBizDocItemID
							  INNER JOIN OpportunityItems OT ON OT.numOppItemtCode=BDI.numOppItemID
							  INNER JOIN Item I ON BDI.numItemCode=I.numItemCode
                              Where oppBiz.bitAuthoritativeBizDocs = 1 and Opp.tintOppStatus=1 And Opp.tintOppType=1 And (Opp.numRecOwner=@numUserCntID OR Opp.numAssignedTo=@numUserCntID OR Opp.numPartner=@numDivisionID)
                              And Opp.numDomainId=@numDomainId 
							  AND oppBiz.numOppBizDocsId=@numOppBizDocsId AND BC.bitCommisionPaid=0 --added by chintan BugID 982
							  AND (ISNULL(@numComRuleID,0)=0 OR BC.numComRuleID=@numComRuleID)
							  AND oppBiz.bitAuthoritativeBizDocs = 1 AND 
							  1=(CASE WHEN @tintMode=2 THEN CASE WHEN isnull(oppBiz.monAmountPaid,0) >=oppBiz.monDealAmount THEN 1 ELSE 0 END
							       WHEN @tintMode=3 THEN CASE WHEN isnull(oppBiz.monAmountPaid,0) <oppBiz.monDealAmount THEN 1 ELSE 0 END END)) A
						WHERE ((dateadd(minute,-@ClientTimeZoneOffset,dtDepositDate) between @dtStartDate And @dtEndDate))  
							     
    END
    ELSE IF @tintMode=3 -- Non Paid Invoice Detail
   BEGIN
        Select BC.numComissionID,oppBiz.numOppBizDocsId,BC.decCommission,Case when BC.tintComBasedOn = 1 then 'Amount' when BC.tintComBasedOn = 2 then 'Units' end AS BasedOn,
			Case when BC.tintComType = 1 then 'Percentage' when BC.tintComType = 2 then 'Flat' end as CommissionType, 
                        numComissionAmount  as CommissionAmt,I.vcItemName,BDI.numUnitHour,BDI.monPrice,BDI.monTotAmount,ISNULL(OT.monVendorCost, 0)* ISNULL(BDI.[numUnitHour],0) AS VendorCost,
						ISNULL(OT.monAvgCost, 0)* ISNULL(BDI.[numUnitHour],0) AS monAvgCost       
                              From OpportunityMaster Opp 
							  INNER JOIN OpportunityBizDocs oppBiz on oppBiz.numOppId=opp.numOppId 
                              INNER JOIN BizDocComission BC on BC.numOppBizDocId=oppBiz.numOppBizDocsId and ((BC.numUserCntID = @numUserCntID AND BC.tintAssignTo <> 3) OR (BC.numUserCntID = @numDivisionID AND BC.tintAssignTo = 3))
                              INNER JOIN OpportunityBizDocItems BDI ON oppBiz.numOppBizDocsId=BDI.numOppBizDocID AND BC.numOppBizDocItemID=BDI.numOppBizDocItemID
							  INNER JOIN OpportunityItems OT ON OT.numOppItemtCode=BDI.numOppItemID
							  INNER JOIN Item I ON BDI.numItemCode=I.numItemCode
                              Where oppBiz.bitAuthoritativeBizDocs = 1 and Opp.tintOppStatus=1 And Opp.tintOppType=1 And (Opp.numRecOwner=@numUserCntID OR Opp.numAssignedTo=@numUserCntID OR Opp.numPartner=@numDivisionID)
                              And Opp.numDomainId=@numDomainId 
							  AND ((dateadd(minute,-@ClientTimeZoneOffset,oppBiz.dtFromDate) between @dtStartDate And @dtEndDate)) 
							  AND oppBiz.numOppBizDocsId=@numOppBizDocsId AND BC.bitCommisionPaid=0 --added by chintan BugID 982
							  AND (ISNULL(@numComRuleID,0)=0 OR BC.numComRuleID=@numComRuleID)
							  AND oppBiz.bitAuthoritativeBizDocs = 1 AND 
							  1=(CASE WHEN @tintMode=2 THEN CASE WHEN isnull(oppBiz.monAmountPaid,0) >=oppBiz.monDealAmount THEN 1 ELSE 0 END
							       WHEN @tintMode=3 THEN CASE WHEN isnull(oppBiz.monAmountPaid,0) <oppBiz.monDealAmount THEN 1 ELSE 0 END END)
    END
  END
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetPriceNamesTable')
DROP PROCEDURE dbo.USP_GetPriceNamesTable
GO
CREATE PROCEDURE [dbo].[USP_GetPriceNamesTable]
(
	@numDomainID NUMERIC(18,0)
)
AS
BEGIN
	SELECT  
		[pricingNamesID],
        [numDomainID],
        [tintPriceLevel],
 		ISNULL(vcPriceLevelName,'') vcPriceLevelName
    FROM 
		[PricingNamesTable]
	WHERE 
		numDomainID = @numDomainID
END
--USP_GetPriceTable 360
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetPriceTable')
DROP PROCEDURE USP_GetPriceTable
GO
CREATE PROCEDURE USP_GetPriceTable
   @numPriceRuleID NUMERIC,
    @numItemCode AS NUMERIC 
	,@numDomainID AS NUMERIC
AS 
BEGIN

	WITH PricingCte AS 
	(
		SELECT  pt.[numPricingID],
		pt.[numPriceRuleID],
		pt.[intFromQty],
		pt.[intToQty],
		pt.[tintRuleType],
		pt.[tintDiscountType],
		CAST(CONVERT(DECIMAL(18, 4), pt.[decDiscount]) AS VARCHAR) decDiscount,
		ROW_NUMBER() OVER(ORDER BY [numPriceRuleID]) as rowId
		FROM    [PricingTable] pt
		WHERE   ISNULL([numPriceRuleID],0) = @numPriceRuleID AND ISNULL(numItemCode,0)=@numItemCode
	)
	SELECT  [numPricingID]
	,[numPriceRuleID]
	,[intFromQty]
	,[intToQty]
	,[tintRuleType]
	,[tintDiscountType]
	,decDiscount
	,ISNULL(NULLIF(pnt.vcPriceLevelName, ''), CONCAT('Price Level ',rowId)) AS [vcName]
	FROM PricingCte
	LEFT JOIN PricingNamesTable pnt ON pnt.tintPriceLevel = PricingCte.rowId AND pnt.numDomainID=@numDomainID
	WHERE rowId IS NOT NULL OR (pnt.vcPriceLevelName IS NOT NULL AND pnt.vcPriceLevelName <> '')
	ORDER BY [numPriceRuleID] 

END

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetSitesDetail')
DROP PROCEDURE USP_GetSitesDetail
GO
CREATE PROCEDURE [dbo].[USP_GetSitesDetail] @numSiteID NUMERIC(9)
AS 
    SELECT  S.[numDomainID],
            D.[numDivisionID],
            ISNULL(S.[bitIsActive], 1),
            ISNULL(E.[numDefaultWareHouseID],0) numDefaultWareHouseID,
            ISNULL(E.[numRelationshipId],0) numRelationshipId,
            ISNULL(E.[numProfileId],0) numProfileId,
            ISNULL(E.[bitHidePriceBeforeLogin],0) AS bitHidePriceBeforeLogin,
			dbo.fn_GetContactEmail(1,0,numAdminID) AS DomainAdminEmail,
			S.vcLiveURL,
			ISNULL(E.bitAuthOnlyCreditCard,0) bitAuthOnlyCreditCard,
			ISNULL(D.numDefCountry,0) numDefCountry,
			ISNULL(E.bitSendEMail,0) bitSendMail,
			ISNULL((SELECT numAuthoritativeSales FROM dbo.AuthoritativeBizDocs WHERE numDomainId=S.numDomainID),0) AuthSalesBizDoc,
			ISNULL((SELECT TOP 1 numBizDocId FROM dbo.eCommercePaymentConfig WHERE numSiteID=@numSiteID AND numPaymentMethodId = 27261 AND bitEnable =1 ),0) numCreditTermBizDocID,
			ISNULL((SELECT TOP 1 numBizDocId FROM dbo.eCommercePaymentConfig WHERE numSiteID=@numSiteID AND numPaymentMethodId =1 AND bitEnable =1 ),0) numCreditCardBizDocID,
			ISNULL((SELECT TOP 1 numBizDocId FROM dbo.eCommercePaymentConfig WHERE numSiteID=@numSiteID AND numPaymentMethodId = 31488 AND bitEnable =1 ),0) numGoogleCheckoutBizDocID,
ISNULL((SELECT TOP 1 numBizDocId FROM dbo.eCommercePaymentConfig WHERE numSiteID=@numSiteID AND numPaymentMethodId = 35141 AND bitEnable =1 ),0) numPaypalCheckoutBizDocID,
ISNULL((SELECT TOP 1 numBizDocId FROM dbo.eCommercePaymentConfig WHERE numSiteID=@numSiteID AND numPaymentMethodId = 84 AND bitEnable =1 ),0) numSalesInquiryBizDocID,
			ISNULL(D.bitSaveCreditCardInfo,0) bitSaveCreditCardInfo,
			ISNULL(bitOnePageCheckout,0) bitOnePageCheckout,
			ISNULL(D.tintBaseTaxOnArea,0) tintBaseTaxOnArea,
			ISNULL(D.numShippingServiceItemID,0) numShippingServiceItemID,
			ISNULL(D.bitAutolinkUnappliedPayment,0) bitAutolinkUnappliedPayment,
			ISNULL(D.numDiscountServiceItemID,0) numDiscountServiceItemID,
			ISNULL(D.tintBaseTax,0) AS [tintBaseTax],
			ISNULL(E.bitSkipStep2,0) AS [bitSkipStep2],
			ISNULL(E.bitDisplayCategory,0) AS [bitDisplayCategory],
			ISNULL(E.bitShowPriceUsingPriceLevel,0) AS [bitShowPriceUsingPriceLevel],
			ISNULL(E.[bitPreSellUp],0) [bitPreSellUp],
			ISNULL(E.[bitPostSellUp],0) [bitPostSellUp],
			ISNULL(E.[dcPostSellDiscount],0) [dcPostSellDiscount],
			ISNULL(E.numDefaultClass,0) numDefaultClass,
			E.vcPreSellUp,E.vcPostSellUp,ISNULL(E.vcRedirectThankYouUrl,'') AS vcRedirectThankYouUrl,
			ISNULL(D.vcSalesOrderTabs,'') AS vcSalesOrderTabs,
			ISNULL(D.vcSalesQuotesTabs,'') AS vcSalesQuotesTabs,
			ISNULL(D.vcItemPurchaseHistoryTabs,'') AS vcItemPurchaseHistoryTabs,
			ISNULL(D.vcItemsFrequentlyPurchasedTabs,'') AS vcItemsFrequentlyPurchasedTabs,
			ISNULL(D.vcOpenCasesTabs,'') AS vcOpenCasesTabs,
			ISNULL(D.vcOpenRMATabs,'') AS vcOpenRMATabs,
			ISNULL(D.bitSalesOrderTabs,0) AS bitSalesOrderTabs,
			ISNULL(D.bitSalesQuotesTabs,0) AS bitSalesQuotesTabs,
			ISNULL(D.bitItemPurchaseHistoryTabs,0) AS bitItemPurchaseHistoryTabs,
			ISNULL(D.bitItemsFrequentlyPurchasedTabs,0) AS bitItemsFrequentlyPurchasedTabs,
			ISNULL(D.bitOpenCasesTabs,0) AS bitOpenCasesTabs,
			ISNULL(D.bitOpenRMATabs,0) AS bitOpenRMATabs,
			ISNULL(D.bitSupportTabs,0) AS bitSupportTabs,
			ISNULL(D.vcSupportTabs,'') AS vcSupportTabs,
			ISNULL(bitHideAddtoCart,0) AS bitHideAddtoCart
    FROM    Sites S
            INNER JOIN [Domain] D ON D.[numDomainId] = S.[numDomainID]
            LEFT OUTER JOIN [eCommerceDTL] E ON D.[numDomainId] = E.[numDomainId] AND S.numSiteID = E.numSiteId
    WHERE   S.[numSiteID] = @numSiteID
 
 

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_Import_SaveContactDetails')
DROP PROCEDURE USP_Import_SaveContactDetails
GO
CREATE PROCEDURE [dbo].[USP_Import_SaveContactDetails]  
	@numDomainID NUMERIC(18,0)
	,@numUserCntID NUMERIC(18,0)
	,@numDivisionID NUMERIC(18,0)
	,@numContactID NUMERIC(18,0)
	,@vcFirstName VARCHAR(50)
	,@vcLastName VARCHAR(50)
	,@vcEmail VARCHAR(80)
	,@numContactType NUMERIC(18,0)
	,@numPhone VARCHAR(15)
	,@numPhoneExtension VARCHAR(7)
	,@numCell VARCHAR(15)
	,@numHomePhone VARCHAR(15)
	,@vcFax VARCHAR(15)
	,@vcCategory NUMERIC(18,0)
	,@numTeam NUMERIC(18,0)
	,@vcPosition NUMERIC(18,0)
	,@vcTitle VARCHAR(100)
	,@vcDepartment NUMERIC(18,0)
	,@txtNotes NVARCHAR(MAX)
	,@bitIsPrimaryContact BIT
	,@vcStreet VARCHAR(100)
	,@vcCity VARCHAR(50)
	,@numState NUMERIC(18,0)
	,@numCountry NUMERIC(18,0)
	,@vcPostalCode VARCHAR(15)
AS
BEGIN
	-- IMPORTANT: PLEASE MAKE SURE NEW FIELD WHICH YOUR ARE ADDING IN STORE PROCEDURE FOR UPDATE ARE ADDED IN SELECT IN PROCEDURE USP_Import_GetContactDetails

	IF LEN(ISNULL(@vcFirstName,'')) = 0
	BEGIN
		SET @vcFirstName = '-'
	END

	IF LEN(ISNULL(@vcLastName,'')) = 0
	BEGIN
		SET @vcLastName = '-'
	END

	IF ISNULL(@numDivisionID,0) > 0
	BEGIN
		IF ISNULL(@numContactID,0) = 0
		BEGIN
			IF(SELECT COUNT(numContactId) FROM AdditionalContactsInformation WHERE numDivisionId=@numDivisionID)=0
			BEGIN
				SET @bitIsPrimaryContact = 1
			END

			IF ISNULL(@numContactType,0) = 0
			BEGIN
				SET @numContactType = 70
			END

			INSERT into AdditionalContactsInformation                                    
			(                  
				numContactType,                  
				vcDepartment,                  
				vcCategory,                  
				vcGivenName,                  
				vcFirstName,                  
				vcLastName,                   
				numDivisionId,                  
				numPhone,                  
				numPhoneExtension,                  
				numCell,                  
				numHomePhone,                          
				vcFax,                  
				vcEmail,                
				vcPosition,                  
				txtNotes,                  
				numCreatedBy,                                    
				bintCreatedDate,             
				numModifiedBy,            
				bintModifiedDate,                   
				numDomainID,               
				numRecOwner,                  
				numTeam,                  
				numEmpStatus,
				vcTitle
				,bitPrimaryContact     
			)                                    
			VALUES                                    
			(                  
				@numContactType,                  
				@vcDepartment,                  
				@vcCategory,                  
				CONCAT(@vcLastName,', ',@vcFirstName,'.eml'),                  
				@vcFirstName ,                  
				@vcLastName,                   
				@numDivisionId ,                  
				@numPhone ,                                    
				@numPhoneExtension,                  
				@numCell ,                  
				@NumHomePhone ,                  
				@vcFax ,                  
				@vcEmail ,                  
				@vcPosition,                                   
				@txtNotes,                  
				@numUserCntID,                  
				GETUTCDATE(),             
				@numUserCntID,                  
				GETUTCDATE(),                   
				@numDomainID,                  
				@numUserCntID,                   
				@numTeam,                  
				658,
				@vcTitle
				,@bitIsPrimaryContact                
			)                                    
                     
			SET @numContactID= SCOPE_IDENTITY()

			INSERT INTO dbo.AddressDetails 
			(
	 			vcAddressName,
	 			vcStreet,
	 			vcCity,
	 			vcPostalCode,
	 			numState,
	 			numCountry,
	 			bitIsPrimary,
	 			tintAddressOf,
	 			tintAddressType,
	 			numRecordID,
	 			numDomainID
			) 
			VALUES
			(
				'Primary'
				,@vcStreet
				,@vcCity
				,@vcPostalCode
				,@numState
				,@numCountry
				,1
				,1
				,0
				,@numContactID
				,@numDomainID
			)                  
		END
		ELSE
		BEGIN
			UPDATE 
				AdditionalContactsInformation 
			SET                                    
				numContactType=@numContactType,                                    
				vcGivenName=CONCAT(@vcLastName,', ',@vcFirstName,'.eml'),
				vcFirstName=@vcFirstName,
				vcLastName =@vcLastName,
				numDivisionId =@numDivisionId,
				numPhone=@numPhone,
				numPhoneExtension=@numPhoneExtension,
				numCell =@numCell,
				NumHomePhone =@NumHomePhone,
				vcFax=@vcFax,
				vcEmail=@vcEmail,
				vcPosition=@vcPosition,
				txtNotes=@txtNotes,
				numModifiedBy=@numUserCntID,
				bintModifiedDate=GETUTCDATE(),
				bitPrimaryContact=@bitIsPrimaryContact
			WHERE 
				numContactId=@numContactId
		END

		DECLARE @numCount AS NUMERIC(18,0) = 0

		IF ISNULL(@vcEmail,'') <> ''
		BEGIN 
			SELECT @numCount = COUNT(*) FROM dbo.EmailMaster WHERE vcEmailID =@vcEmail AND numDomainId=@numDomainID

			IF @numCount = 0 
			BEGIN
				INSERT INTO EmailMaster(vcEmailId,vcName,numDomainId,numContactId)
				VALUES(@vcEmail,@vcFirstName,@numDomainID,@numcontactId)
			END
		END 
		ELSE
		BEGIN
			UPDATE EmailMaster set numContactId=@numcontactId WHERE vcEmailID=@vcEmail and numDomainId=@numDomainID
		END
	END

	SELECT ISNULL(@numContactID,0) AS numContactID             
END
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created By Anoop Jayaraj                                                          
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_Item_ImportPriceLevel')
DROP PROCEDURE USP_Item_ImportPriceLevel
GO
CREATE PROCEDURE [dbo].[USP_Item_ImportPriceLevel]
(
	@numItemCode nUMERIC(18,0)
	,@tintRuleType INT
	,@tintDiscountType INT
	,@monPriceLevel1 MONEY
	,@monPriceLevel2 MONEY
	,@monPriceLevel3 MONEY
	,@monPriceLevel4 MONEY
	,@monPriceLevel5 MONEY
	,@monPriceLevel6 MONEY
	,@monPriceLevel7 MONEY
	,@monPriceLevel8 MONEY
	,@monPriceLevel9 MONEY
	,@monPriceLevel10 MONEY
	,@monPriceLevel11 MONEY
	,@monPriceLevel12 MONEY
	,@monPriceLevel13 MONEY
	,@monPriceLevel14 MONEY
	,@monPriceLevel15 MONEY
	,@monPriceLevel16 MONEY
	,@monPriceLevel17 MONEY
	,@monPriceLevel18 MONEY
	,@monPriceLevel19 MONEY
	,@monPriceLevel20 MONEY
	,@intPriceLevel1FromQty INT
	,@intPriceLevel1ToQty INT
	,@intPriceLevel2FromQty INT
	,@intPriceLevel2ToQty INT
	,@intPriceLevel3FromQty INT
	,@intPriceLevel3ToQty INT
	,@intPriceLevel4FromQty INT
	,@intPriceLevel4ToQty INT
	,@intPriceLevel5FromQty INT
	,@intPriceLevel5ToQty INT
	,@intPriceLevel6FromQty INT
	,@intPriceLevel6ToQty INT
	,@intPriceLevel7FromQty INT
	,@intPriceLevel7ToQty INT
	,@intPriceLevel8FromQty INT
	,@intPriceLevel8ToQty INT
	,@intPriceLevel9FromQty INT
	,@intPriceLevel9ToQty INT
	,@intPriceLevel10FromQty INT
	,@intPriceLevel10ToQty INT
	,@intPriceLevel11FromQty INT
	,@intPriceLevel11ToQty INT
	,@intPriceLevel12FromQty INT
	,@intPriceLevel12ToQty INT
	,@intPriceLevel13FromQty INT
	,@intPriceLevel13ToQty INT
	,@intPriceLevel14FromQty INT
	,@intPriceLevel14ToQty INT
	,@intPriceLevel15FromQty INT
	,@intPriceLevel15ToQty INT
	,@intPriceLevel16FromQty INT
	,@intPriceLevel16ToQty INT
	,@intPriceLevel17FromQty INT
	,@intPriceLevel17ToQty INT
	,@intPriceLevel18FromQty INT
	,@intPriceLevel18ToQty INT
	,@intPriceLevel19FromQty INT
	,@intPriceLevel19ToQty INT
	,@intPriceLevel20FromQty INT
	,@intPriceLevel20ToQty INT
	,@vcPriceLevel1Name VARCHAR(300)
	,@vcPriceLevel2Name VARCHAR(300)
	,@vcPriceLevel3Name VARCHAR(300)
	,@vcPriceLevel4Name VARCHAR(300)
	,@vcPriceLevel5Name VARCHAR(300)
	,@vcPriceLevel6Name VARCHAR(300)
	,@vcPriceLevel7Name VARCHAR(300)
	,@vcPriceLevel8Name VARCHAR(300)
	,@vcPriceLevel9Name VARCHAR(300)
	,@vcPriceLevel10Name VARCHAR(300)
	,@vcPriceLevel11Name VARCHAR(300)
	,@vcPriceLevel12Name VARCHAR(300)
	,@vcPriceLevel13Name VARCHAR(300)
	,@vcPriceLevel14Name VARCHAR(300)
	,@vcPriceLevel15Name VARCHAR(300)
	,@vcPriceLevel16Name VARCHAR(300)
	,@vcPriceLevel17Name VARCHAR(300)
	,@vcPriceLevel18Name VARCHAR(300)
	,@vcPriceLevel19Name VARCHAR(300)
	,@vcPriceLevel20Name VARCHAR(300)
) 
AS
BEGIN
	IF ISNULL(@monPriceLevel1,0) = 0 
	BEGIN
		SET @monPriceLevel1 = -1
	END
	IF ISNULL(@monPriceLevel2,0) = 0 
	BEGIN
		SET @monPriceLevel2 = -1
	END
	IF ISNULL(@monPriceLevel3,0) = 0 
	BEGIN
		SET @monPriceLevel3 = -1
	END
	IF ISNULL(@monPriceLevel4,0) = 0 
	BEGIN
		SET @monPriceLevel4 = -1
	END
	IF ISNULL(@monPriceLevel5,0) = 0 
	BEGIN
		SET @monPriceLevel5 = -1
	END
	IF ISNULL(@monPriceLevel6,0) = 0 
	BEGIN
		SET @monPriceLevel6 = -1
	END
	IF ISNULL(@monPriceLevel7,0) = 0 
	BEGIN
		SET @monPriceLevel7 = -1
	END
	IF ISNULL(@monPriceLevel8,0) = 0 
	BEGIN
		SET @monPriceLevel8 = -1
	END
	IF ISNULL(@monPriceLevel9,0) = 0 
	BEGIN
		SET @monPriceLevel9 = -1
	END
	IF ISNULL(@monPriceLevel10,0) = 0 
	BEGIN
		SET @monPriceLevel10 = -1
	END
	IF ISNULL(@monPriceLevel11,0) = 0 
	BEGIN
		SET @monPriceLevel11 = -1
	END
	IF ISNULL(@monPriceLevel12,0) = 0 
	BEGIN
		SET @monPriceLevel12 = -1
	END
	IF ISNULL(@monPriceLevel13,0) = 0 
	BEGIN
		SET @monPriceLevel13 = -1
	END
	IF ISNULL(@monPriceLevel14,0) = 0 
	BEGIN
		SET @monPriceLevel14 = -1
	END
	IF ISNULL(@monPriceLevel15,0) = 0 
	BEGIN
		SET @monPriceLevel15 = -1
	END
	IF ISNULL(@monPriceLevel16,0) = 0 
	BEGIN
		SET @monPriceLevel16 = -1
	END
	IF ISNULL(@monPriceLevel17,0) = 0 
	BEGIN
		SET @monPriceLevel17 = -1
	END
	IF ISNULL(@monPriceLevel18,0) = 0 
	BEGIN
		SET @monPriceLevel18 = -1
	END
	IF ISNULL(@monPriceLevel19,0) = 0 
	BEGIN
		SET @monPriceLevel19 = -1
	END
	IF ISNULL(@monPriceLevel20,0) = 0 
	BEGIN
		SET @monPriceLevel20 = -1
	END


	IF @monPriceLevel1 = -1
		AND @monPriceLevel2 = -1
		AND @monPriceLevel3 = -1
		AND @monPriceLevel4 = -1
		AND @monPriceLevel5 = -1
		AND @monPriceLevel6 = -1
		AND @monPriceLevel7 = -1
		AND @monPriceLevel8 = -1
		AND @monPriceLevel9 = -1
		AND @monPriceLevel10 = -1
		AND @monPriceLevel11 = -1
		AND @monPriceLevel12 = -1
		AND @monPriceLevel13 = -1
		AND @monPriceLevel14 = -1
		AND @monPriceLevel15 = -1
		AND @monPriceLevel16 = -1
		AND @monPriceLevel17 = -1
		AND @monPriceLevel18 = -1
		AND @monPriceLevel19 = -1
		AND @monPriceLevel20 = -1
	BEGIN
		RETURN
	END

	IF ISNULL(@intPriceLevel1FromQty,0) = 0 AND ISNULL(@intPriceLevel1ToQty,0) = 0
	BEGIN
		SET @intPriceLevel1FromQty = -1
		SET @intPriceLevel1ToQty = -1
	END
	IF ISNULL(@intPriceLevel2FromQty,0) = 0 AND ISNULL(@intPriceLevel2ToQty,0) = 0
	BEGIN
		SET @intPriceLevel2FromQty = -1
		SET @intPriceLevel2ToQty = -1
	END
	IF ISNULL(@intPriceLevel3FromQty,0) = 0 AND ISNULL(@intPriceLevel3ToQty,0) = 0
	BEGIN
		SET @intPriceLevel3FromQty = -1
		SET @intPriceLevel3ToQty = -1
	END
	IF ISNULL(@intPriceLevel4FromQty,0) = 0 AND ISNULL(@intPriceLevel4ToQty,0) = 0
	BEGIN
		SET @intPriceLevel4FromQty = -1
		SET @intPriceLevel4ToQty = -1
	END
	IF ISNULL(@intPriceLevel5FromQty,0) = 0 AND ISNULL(@intPriceLevel5ToQty,0) = 0
	BEGIN
		SET @intPriceLevel5FromQty = -1
		SET @intPriceLevel5ToQty = -1
	END
	IF ISNULL(@intPriceLevel6FromQty,0) = 0 AND ISNULL(@intPriceLevel6ToQty,0) = 0
	BEGIN
		SET @intPriceLevel6FromQty = -1
		SET @intPriceLevel6ToQty = -1
	END
	IF ISNULL(@intPriceLevel7FromQty,0) = 0 AND ISNULL(@intPriceLevel7ToQty,0) = 0
	BEGIN
		SET @intPriceLevel7FromQty = -1
		SET @intPriceLevel7ToQty = -1
	END
	IF ISNULL(@intPriceLevel8FromQty,0) = 0 AND ISNULL(@intPriceLevel8ToQty,0) = 0
	BEGIN
		SET @intPriceLevel8FromQty = -1
		SET @intPriceLevel8ToQty = -1
	END
	IF ISNULL(@intPriceLevel9FromQty,0) = 0 AND ISNULL(@intPriceLevel9ToQty,0) = 0
	BEGIN
		SET @intPriceLevel9FromQty = -1
		SET @intPriceLevel9ToQty = -1
	END
	IF ISNULL(@intPriceLevel10FromQty,0) = 0 AND ISNULL(@intPriceLevel10ToQty,0) = 0
	BEGIN
		SET @intPriceLevel10FromQty = -1
		SET @intPriceLevel10ToQty = -1
	END
	IF ISNULL(@intPriceLevel11FromQty,0) = 0 AND ISNULL(@intPriceLevel11ToQty,0) = 0
	BEGIN
		SET @intPriceLevel11FromQty = -1
		SET @intPriceLevel11ToQty = -1
	END
	IF ISNULL(@intPriceLevel12FromQty,0) = 0 AND ISNULL(@intPriceLevel12ToQty,0) = 0
	BEGIN
		SET @intPriceLevel12FromQty = -1
		SET @intPriceLevel12ToQty = -1
	END
	IF ISNULL(@intPriceLevel13FromQty,0) = 0 AND ISNULL(@intPriceLevel13ToQty,0) = 0
	BEGIN
		SET @intPriceLevel13FromQty = -1
		SET @intPriceLevel13ToQty = -1
	END
	IF ISNULL(@intPriceLevel14FromQty,0) = 0 AND ISNULL(@intPriceLevel14ToQty,0) = 0
	BEGIN
		SET @intPriceLevel14FromQty = -1
		SET @intPriceLevel14ToQty = -1
	END
	IF ISNULL(@intPriceLevel15FromQty,0) = 0 AND ISNULL(@intPriceLevel15ToQty,0) = 0
	BEGIN
		SET @intPriceLevel15FromQty = -1
		SET @intPriceLevel15ToQty = -1
	END
	IF ISNULL(@intPriceLevel16FromQty,0) = 0 AND ISNULL(@intPriceLevel16ToQty,0) = 0
	BEGIN
		SET @intPriceLevel16FromQty = -1
		SET @intPriceLevel16ToQty = -1
	END
	IF ISNULL(@intPriceLevel17FromQty,0) = 0 AND ISNULL(@intPriceLevel17ToQty,0) = 0
	BEGIN
		SET @intPriceLevel17FromQty = -1
		SET @intPriceLevel17ToQty = -1
	END
	IF ISNULL(@intPriceLevel18FromQty,0) = 0 AND ISNULL(@intPriceLevel18ToQty,0) = 0
	BEGIN
		SET @intPriceLevel18FromQty = -1
		SET @intPriceLevel18ToQty = -1
	END
	IF ISNULL(@intPriceLevel19FromQty,0) = 0 AND ISNULL(@intPriceLevel19ToQty,0) = 0
	BEGIN
		SET @intPriceLevel19FromQty = -1
		SET @intPriceLevel19ToQty = -1
	END
	IF ISNULL(@intPriceLevel20FromQty,0) = 0 AND ISNULL(@intPriceLevel20ToQty,0) = 0
	BEGIN
		SET @intPriceLevel20FromQty = -1
		SET @intPriceLevel20ToQty = -1
	END



	IF ISNULL(@intPriceLevel1FromQty,0) = 0 OR ISNULL(@intPriceLevel1ToQty,0) = 0
	BEGIN
		RAISERROR('QTY_FROM_AND_TO_MUST_BE_GREATER_THEN_0_FOR_PRICE_LEVEL_1',16,1)
		RETURN
	END
	IF ISNULL(@intPriceLevel2FromQty,0) = 0 OR ISNULL(@intPriceLevel2ToQty,0) = 0
	BEGIN
		RAISERROR('QTY_FROM_AND_TO_MUST_BE_GREATER_THEN_0_FOR_PRICE_LEVEL_2',16,1)
		RETURN
	END
	IF ISNULL(@intPriceLevel3FromQty,0) = 0 OR ISNULL(@intPriceLevel3ToQty,0) = 0
	BEGIN
		RAISERROR('QTY_FROM_AND_TO_MUST_BE_GREATER_THEN_0_FOR_PRICE_LEVEL_3',16,1)
		RETURN
	END
	IF ISNULL(@intPriceLevel4FromQty,0) = 0 OR ISNULL(@intPriceLevel4ToQty,0) = 0
	BEGIN
		RAISERROR('QTY_FROM_AND_TO_MUST_BE_GREATER_THEN_0_FOR_PRICE_LEVEL_4',16,1)
		RETURN
	END
	IF ISNULL(@intPriceLevel5FromQty,0) = 0 OR ISNULL(@intPriceLevel5ToQty,0) = 0
	BEGIN
		RAISERROR('QTY_FROM_AND_TO_MUST_BE_GREATER_THEN_0_FOR_PRICE_LEVEL_5',16,1)
		RETURN
	END
	IF ISNULL(@intPriceLevel6FromQty,0) = 0 OR ISNULL(@intPriceLevel6ToQty,0) = 0
	BEGIN
		RAISERROR('QTY_FROM_AND_TO_MUST_BE_GREATER_THEN_0_FOR_PRICE_LEVEL_6',16,1)
		RETURN
	END
	IF ISNULL(@intPriceLevel7FromQty,0) = 0 OR ISNULL(@intPriceLevel7ToQty,0) = 0
	BEGIN
		RAISERROR('QTY_FROM_AND_TO_MUST_BE_GREATER_THEN_0_FOR_PRICE_LEVEL_7',16,1)
		RETURN
	END
	IF ISNULL(@intPriceLevel8FromQty,0) = 0 OR ISNULL(@intPriceLevel8ToQty,0) = 0
	BEGIN
		RAISERROR('QTY_FROM_AND_TO_MUST_BE_GREATER_THEN_0_FOR_PRICE_LEVEL_8',16,1)
		RETURN
	END
	IF ISNULL(@intPriceLevel9FromQty,0) = 0 OR ISNULL(@intPriceLevel9ToQty,0) = 0
	BEGIN
		RAISERROR('QTY_FROM_AND_TO_MUST_BE_GREATER_THEN_0_FOR_PRICE_LEVEL_9',16,1)
		RETURN
	END
	IF ISNULL(@intPriceLevel10FromQty,0) = 0 OR ISNULL(@intPriceLevel10ToQty,0) = 0
	BEGIN
		RAISERROR('QTY_FROM_AND_TO_MUST_BE_GREATER_THEN_0_FOR_PRICE_LEVEL_10',16,1)
		RETURN
	END
	IF ISNULL(@intPriceLevel11FromQty,0) = 0 OR ISNULL(@intPriceLevel11ToQty,0) = 0
	BEGIN
		RAISERROR('QTY_FROM_AND_TO_MUST_BE_GREATER_THEN_0_FOR_PRICE_LEVEL_11',16,1)
		RETURN
	END
	IF ISNULL(@intPriceLevel12FromQty,0) = 0 OR ISNULL(@intPriceLevel12ToQty,0) = 0
	BEGIN
		RAISERROR('QTY_FROM_AND_TO_MUST_BE_GREATER_THEN_0_FOR_PRICE_LEVEL_12',16,1)
		RETURN
	END
	IF ISNULL(@intPriceLevel13FromQty,0) = 0 OR ISNULL(@intPriceLevel13ToQty,0) = 0
	BEGIN
		RAISERROR('QTY_FROM_AND_TO_MUST_BE_GREATER_THEN_0_FOR_PRICE_LEVEL_13',16,1)
		RETURN
	END
	IF ISNULL(@intPriceLevel14FromQty,0) = 0 OR ISNULL(@intPriceLevel14ToQty,0) = 0
	BEGIN
		RAISERROR('QTY_FROM_AND_TO_MUST_BE_GREATER_THEN_0_FOR_PRICE_LEVEL_14',16,1)
		RETURN
	END
	IF ISNULL(@intPriceLevel15FromQty,0) = 0 OR ISNULL(@intPriceLevel15ToQty,0) = 0
	BEGIN
		RAISERROR('QTY_FROM_AND_TO_MUST_BE_GREATER_THEN_0_FOR_PRICE_LEVEL_15',16,1)
		RETURN
	END
	IF ISNULL(@intPriceLevel16FromQty,0) = 0 OR ISNULL(@intPriceLevel16ToQty,0) = 0
	BEGIN
		RAISERROR('QTY_FROM_AND_TO_MUST_BE_GREATER_THEN_0_FOR_PRICE_LEVEL_16',16,1)
		RETURN
	END
	IF ISNULL(@intPriceLevel17FromQty,0) = 0 OR ISNULL(@intPriceLevel17ToQty,0) = 0
	BEGIN
		RAISERROR('QTY_FROM_AND_TO_MUST_BE_GREATER_THEN_0_FOR_PRICE_LEVEL_17',16,1)
		RETURN
	END
	IF ISNULL(@intPriceLevel18FromQty,0) = 0 OR ISNULL(@intPriceLevel18ToQty,0) = 0
	BEGIN
		RAISERROR('QTY_FROM_AND_TO_MUST_BE_GREATER_THEN_0_FOR_PRICE_LEVEL_18',16,1)
		RETURN
	END
	IF ISNULL(@intPriceLevel19FromQty,0) = 0 OR ISNULL(@intPriceLevel19ToQty,0) = 0
	BEGIN
		RAISERROR('QTY_FROM_AND_TO_MUST_BE_GREATER_THEN_0_FOR_PRICE_LEVEL_19',16,1)
		RETURN
	END
	IF ISNULL(@intPriceLevel20FromQty,0) = 0 OR ISNULL(@intPriceLevel20ToQty,0) = 0
	BEGIN
		RAISERROR('QTY_FROM_AND_TO_MUST_BE_GREATER_THEN_0_FOR_PRICE_LEVEL_20',16,1)
		RETURN
	END


	DECLARE @Defualt TABLE
	(
		PriceLevelID INT
		,tintRuleType TINYINT
		,tintDiscountType TINYINT
		,monPrice MONEY
		,intFromQty INT
		,intToQty INT
		,vcName VARCHAR(300)
	)

	INSERT INTO @Defualt
	(
		PriceLevelID
		,monPrice
		,intFromQty
		,intToQty
		,vcName
	)
	VALUES
	(1,-1,-1,-1,'-1'),
	(2,-1,-1,-1,'-1'),
	(3,-1,-1,-1,'-1'),
	(4,-1,-1,-1,'-1'),
	(5,-1,-1,-1,'-1'),
	(6,-1,-1,-1,'-1'),
	(7,-1,-1,-1,'-1'),
	(8,-1,-1,-1,'-1'),
	(9,-1,-1,-1,'-1'),
	(10,-1,-1,-1,'-1'),
	(11,-1,-1,-1,'-1'),
	(12,-1,-1,-1,'-1'),
	(13,-1,-1,-1,'-1'),
	(14,-1,-1,-1,'-1'),
	(15,-1,-1,-1,'-1'),
	(16,-1,-1,-1,'-1'),
	(17,-1,-1,-1,'-1'),
	(18,-1,-1,-1,'-1'),
	(19,-1,-1,-1,'-1'),
	(20,-1,-1,-1,'-1')

	-- FIRST REPLACE -1 WITH EXISTING ITEM PRICES, REPLEACE -1 IN FROM AND TO QTY ONLY WHEN RuleType is Deduct From List Price or Add to vendor Cost
	UPDATE
		t1
	SET
		t1.monPrice = ISNULL(t2.decDiscount,t1.monPrice)
		,tintRuleType = (CASE WHEN ISNULL(@tintRuleType,0) <> -1 THEN @tintRuleType ELSE ISNULL(t2.tintRuleType,t1.tintRuleType) END)
		,tintDiscountType = (CASE WHEN ISNULL(@tintDiscountType,0) <> -1 THEN @tintDiscountType ELSE ISNULL(t2.tintDiscountType,t1.tintDiscountType) END)
		,t1.intFromQty = (CASE WHEN t2.tintRuleType = 1 OR t2.tintRuleType = 2 THEN t2.intFromQty ELSE t1.intFromQty END)
		,t1.intToQty = (CASE WHEN t2.tintRuleType = 1 OR t2.tintRuleType = 2 THEN t2.intToQty ELSE t1.intToQty END)
		,t1.vcName = ISNULL(t2.vcName,'')
	FROM
		@Defualt t1
	LEFT JOIN
	(
		SELECT
			ROW_NUMBER() OVER(ORDER BY numPricingID) AS RowID 
			,decDiscount
			,intFromQty
			,intToQty
			,tintRuleType
			,tintDiscountType
			,vcName
		FROM 
			PricingTable WHERE numItemCode=@numItemCode AND ISNULL(numPriceRuleID,0)=0
	) t2
	ON
		t1.PriceLevelID=t2.RowID

	DECLARE @localRuleType TINYINT
	DECLARE @localDiscountType TINYINT

	SELECT TOP 1
		@localRuleType = tintRuleType
		,@localDiscountType = tintDiscountType
	FROM
		@Defualt AS t2
		

	IF ISNULL(@localRuleType,0) = 0
	BEGIN
		RAISERROR('INVALID_PRICE_RULE_TYPE',16,1)
		RETURN
	END
	ELSE IF (@localRuleType = 1 OR @localRuleType = 2) AND @localDiscountType NOT IN (1,2,3)
	BEGIN
		RAISERROR('INVALID_PRICE_LEVEL_DISCOUNT_TYPE',16,1)
		RETURN
	END
	ELSE IF @localRuleType = 3
	BEGIN
		SET @tintDiscountType = 0
	END

	-- REPLACE -1 WITH NEW PRICES IF PROVIDED FROM ITEM IMPORT (IF COLUMN IF NOT AVAILABLE IN IMPORT FILE THAN VALUE WILL BE -1)
	-- REPLACE -1 WITH NEW QTY FROM IF PROVIDED FROM ITEM IMPORT (IF COLUMN IF NOT AVAILABLE IN IMPORT FILE THAN VALUE WILL BE -1)
	-- REPLACE -1 WITH NEW QTY TO IF PROVIDED FROM ITEM IMPORT (IF COLUMN IF NOT AVAILABLE IN IMPORT FILE THAN VALUE WILL BE -1)
	UPDATE 
		@Defualt 
	SET 
		monPrice = (CASE WHEN ISNULL(@monPriceLevel1,0) <> -1 THEN ISNULL(@monPriceLevel1,0) ELSE monPrice END)
		,intFromQty = (CASE WHEN ISNULL(@intPriceLevel1FromQty,0) <> -1 THEN ISNULL(@intPriceLevel1FromQty,0) ELSE intFromQty END)
		,intToQty = (CASE WHEN ISNULL(@intPriceLevel1ToQty,0) <> -1 THEN ISNULL(@intPriceLevel1ToQty,0) ELSE intToQty END)
		,vcName = (CASE WHEN ISNULL(@vcPriceLevel1Name,'') <> '-1' THEN ISNULL(@vcPriceLevel1Name,0) ELSE vcName END)
	WHERE 
		PriceLevelID = 1

	UPDATE 
		@Defualt 
	SET 
		monPrice = (CASE WHEN ISNULL(@monPriceLevel2,0) <> -1 THEN ISNULL(@monPriceLevel2,0) ELSE monPrice END) 
		,intFromQty = (CASE WHEN ISNULL(@intPriceLevel2FromQty,0) <> -1 THEN ISNULL(@intPriceLevel2FromQty,0) ELSE intFromQty END)
		,intToQty = (CASE WHEN ISNULL(@intPriceLevel2ToQty,0) <> -1 THEN ISNULL(@intPriceLevel2ToQty,0) ELSE intToQty END)
		,vcName = (CASE WHEN ISNULL(@vcPriceLevel2Name,'') <> '-1' THEN ISNULL(@vcPriceLevel2Name,0) ELSE vcName END)
	WHERE 
		PriceLevelID = 2

	UPDATE 
		@Defualt 
	SET 
		monPrice = (CASE WHEN ISNULL(@monPriceLevel3,0) <> -1 THEN ISNULL(@monPriceLevel3,0) ELSE monPrice END) 
		,intFromQty = (CASE WHEN ISNULL(@intPriceLevel3FromQty,0) <> -1 THEN ISNULL(@intPriceLevel3FromQty,0) ELSE intFromQty END)
		,intToQty = (CASE WHEN ISNULL(@intPriceLevel3ToQty,0) <> -1 THEN ISNULL(@intPriceLevel3ToQty,0) ELSE intToQty END)
		,vcName = (CASE WHEN ISNULL(@vcPriceLevel3Name,'') <> '-1' THEN ISNULL(@vcPriceLevel3Name,0) ELSE vcName END)
	WHERE 
		PriceLevelID = 3

	UPDATE 
		@Defualt 
	SET 
		monPrice = (CASE WHEN ISNULL(@monPriceLevel4,0) <> -1 THEN ISNULL(@monPriceLevel4,0) ELSE monPrice END)
		,intFromQty = (CASE WHEN ISNULL(@intPriceLevel4FromQty,0) <> -1 THEN ISNULL(@intPriceLevel4FromQty,0) ELSE intFromQty END)
		,intToQty = (CASE WHEN ISNULL(@intPriceLevel4ToQty,0) <> -1 THEN ISNULL(@intPriceLevel4ToQty,0) ELSE intToQty END)
		,vcName = (CASE WHEN ISNULL(@vcPriceLevel4Name,'') <> '-1' THEN ISNULL(@vcPriceLevel4Name,0) ELSE vcName END)
	WHERE 
		PriceLevelID = 4

	UPDATE 
		@Defualt
	SET 
		monPrice = (CASE WHEN ISNULL(@monPriceLevel5,0) <> -1 THEN ISNULL(@monPriceLevel5,0) ELSE monPrice END)
		,intFromQty = (CASE WHEN ISNULL(@intPriceLevel5FromQty,0) <> -1 THEN ISNULL(@intPriceLevel5FromQty,0) ELSE intFromQty END)
		,intToQty = (CASE WHEN ISNULL(@intPriceLevel5ToQty,0) <> -1 THEN ISNULL(@intPriceLevel5ToQty,0) ELSE intToQty END)
		,vcName = (CASE WHEN ISNULL(@vcPriceLevel5Name,'') <> '-1' THEN ISNULL(@vcPriceLevel5Name,0) ELSE vcName END)
	WHERE 
		PriceLevelID = 5
	
	UPDATE
		@Defualt 
	SET 
		monPrice = (CASE WHEN ISNULL(@monPriceLevel6,0) <> -1 THEN ISNULL(@monPriceLevel6,0) ELSE monPrice END)
		,intFromQty = (CASE WHEN ISNULL(@intPriceLevel6FromQty,0) <> -1 THEN ISNULL(@intPriceLevel6FromQty,0) ELSE intFromQty END)
		,intToQty = (CASE WHEN ISNULL(@intPriceLevel6ToQty,0) <> -1 THEN ISNULL(@intPriceLevel6ToQty,0) ELSE intToQty END)
		,vcName = (CASE WHEN ISNULL(@vcPriceLevel6Name,'') <> '-1' THEN ISNULL(@vcPriceLevel6Name,0) ELSE vcName END)
	WHERE 
		PriceLevelID = 6

	UPDATE 
		@Defualt 
	SET 
		monPrice = (CASE WHEN ISNULL(@monPriceLevel7,0) <> -1 THEN ISNULL(@monPriceLevel7,0) ELSE monPrice END)
		,intFromQty = (CASE WHEN ISNULL(@intPriceLevel7FromQty,0) <> -1 THEN ISNULL(@intPriceLevel7FromQty,0) ELSE intFromQty END)
		,intToQty = (CASE WHEN ISNULL(@intPriceLevel7ToQty,0) <> -1 THEN ISNULL(@intPriceLevel7ToQty,0) ELSE intToQty END)
		,vcName = (CASE WHEN ISNULL(@vcPriceLevel7Name,'') <> '-1' THEN ISNULL(@vcPriceLevel7Name,0) ELSE vcName END)
	WHERE 
		PriceLevelID = 7

	UPDATE 
		@Defualt 
	SET 
		monPrice = (CASE WHEN ISNULL(@monPriceLevel8,0) <> -1 THEN ISNULL(@monPriceLevel8,0) ELSE monPrice END)
		,intFromQty = (CASE WHEN ISNULL(@intPriceLevel8FromQty,0) <> -1 THEN ISNULL(@intPriceLevel8FromQty,0) ELSE intFromQty END)
		,intToQty = (CASE WHEN ISNULL(@intPriceLevel8ToQty,0) <> -1 THEN ISNULL(@intPriceLevel8ToQty,0) ELSE intToQty END)
		,vcName = (CASE WHEN ISNULL(@vcPriceLevel8Name,'') <> '-1' THEN ISNULL(@vcPriceLevel8Name,0) ELSE vcName END)
	WHERE
		 PriceLevelID = 8
	
	UPDATE 
		@Defualt 
	SET 
		monPrice = (CASE WHEN ISNULL(@monPriceLevel9,0) <> -1 THEN ISNULL(@monPriceLevel9,0) ELSE monPrice END)
		,intFromQty = (CASE WHEN ISNULL(@intPriceLevel9FromQty,0) <> -1 THEN ISNULL(@intPriceLevel9FromQty,0) ELSE intFromQty END)
		,intToQty = (CASE WHEN ISNULL(@intPriceLevel9ToQty,0) <> -1 THEN ISNULL(@intPriceLevel9ToQty,0) ELSE intToQty END)
		,vcName = (CASE WHEN ISNULL(@vcPriceLevel9Name,'') <> '-1' THEN ISNULL(@vcPriceLevel9Name,0) ELSE vcName END)
	WHERE 
		PriceLevelID = 9
	
	UPDATE 
		@Defualt 
	SET 
		monPrice = (CASE WHEN ISNULL(@monPriceLevel10,0) <> -1 THEN ISNULL(@monPriceLevel10,0) ELSE monPrice END)
		,intFromQty = (CASE WHEN ISNULL(@intPriceLevel10FromQty,0) <> -1 THEN ISNULL(@intPriceLevel10FromQty,0) ELSE intFromQty END)
		,intToQty = (CASE WHEN ISNULL(@intPriceLevel10ToQty,0) <> -1 THEN ISNULL(@intPriceLevel10ToQty,0) ELSE intToQty END)
		,vcName = (CASE WHEN ISNULL(@vcPriceLevel10Name,'') <> '-1' THEN ISNULL(@vcPriceLevel10Name,0) ELSE vcName END)
	WHERE 
		PriceLevelID = 10
	
	UPDATE 
		@Defualt 
	SET 
		monPrice = (CASE WHEN ISNULL(@monPriceLevel11,0) <> -1 THEN ISNULL(@monPriceLevel11,0) ELSE monPrice END)
		,intFromQty = (CASE WHEN ISNULL(@intPriceLevel11FromQty,0) <> -1 THEN ISNULL(@intPriceLevel11FromQty,0) ELSE intFromQty END)
		,intToQty = (CASE WHEN ISNULL(@intPriceLevel11ToQty,0) <> -1 THEN ISNULL(@intPriceLevel11ToQty,0) ELSE intToQty END)
		,vcName = (CASE WHEN ISNULL(@vcPriceLevel11Name,'') <> '-1' THEN ISNULL(@vcPriceLevel11Name,0) ELSE vcName END)
	WHERE 
		PriceLevelID = 11
	
	UPDATE
		@Defualt 
	SET 
		monPrice = (CASE WHEN ISNULL(@monPriceLevel12,0) <> -1 THEN ISNULL(@monPriceLevel12,0) ELSE monPrice END)
		,intFromQty = (CASE WHEN ISNULL(@intPriceLevel12FromQty,0) <> -1 THEN ISNULL(@intPriceLevel12FromQty,0) ELSE intFromQty END)
		,intToQty = (CASE WHEN ISNULL(@intPriceLevel12ToQty,0) <> -1 THEN ISNULL(@intPriceLevel12ToQty,0) ELSE intToQty END)
		,vcName = (CASE WHEN ISNULL(@vcPriceLevel12Name,'') <> '-1' THEN ISNULL(@vcPriceLevel12Name,0) ELSE vcName END)
	WHERE
		PriceLevelID = 12
	
	UPDATE 
		@Defualt 
	SET 
		monPrice = (CASE WHEN ISNULL(@monPriceLevel13,0) <> -1 THEN ISNULL(@monPriceLevel13,0) ELSE monPrice END)
		,intFromQty = (CASE WHEN ISNULL(@intPriceLevel13FromQty,0) <> -1 THEN ISNULL(@intPriceLevel13FromQty,0) ELSE intFromQty END)
		,intToQty = (CASE WHEN ISNULL(@intPriceLevel13ToQty,0) <> -1 THEN ISNULL(@intPriceLevel13ToQty,0) ELSE intToQty END)
		,vcName = (CASE WHEN ISNULL(@vcPriceLevel13Name,'') <> '-1' THEN ISNULL(@vcPriceLevel13Name,0) ELSE vcName END)
	WHERE 
		PriceLevelID = 13
	
	UPDATE 
		@Defualt 
	SET 
		monPrice = (CASE WHEN ISNULL(@monPriceLevel14,0) <> -1 THEN ISNULL(@monPriceLevel14,0) ELSE monPrice END) 
		,intFromQty = (CASE WHEN ISNULL(@intPriceLevel14FromQty,0) <> -1 THEN ISNULL(@intPriceLevel14FromQty,0) ELSE intFromQty END)
		,intToQty = (CASE WHEN ISNULL(@intPriceLevel14ToQty,0) <> -1 THEN ISNULL(@intPriceLevel14ToQty,0) ELSE intToQty END)
		,vcName = (CASE WHEN ISNULL(@vcPriceLevel14Name,'') <> '-1' THEN ISNULL(@vcPriceLevel14Name,0) ELSE vcName END)
	WHERE 
		PriceLevelID = 14
	
	UPDATE 
		@Defualt 
	SET 
		monPrice = (CASE WHEN ISNULL(@monPriceLevel15,0) <> -1 THEN ISNULL(@monPriceLevel15,0) ELSE monPrice END)
		,intFromQty = (CASE WHEN ISNULL(@intPriceLevel15FromQty,0) <> -1 THEN ISNULL(@intPriceLevel15FromQty,0) ELSE intFromQty END)
		,intToQty = (CASE WHEN ISNULL(@intPriceLevel15ToQty,0) <> -1 THEN ISNULL(@intPriceLevel15ToQty,0) ELSE intToQty END)
		,vcName = (CASE WHEN ISNULL(@vcPriceLevel15Name,'') <> '-1' THEN ISNULL(@vcPriceLevel15Name,0) ELSE vcName END)
	WHERE 
		PriceLevelID = 15
	
	UPDATE 
		@Defualt 
	SET 
		monPrice = (CASE WHEN ISNULL(@monPriceLevel16,0) <> -1 THEN ISNULL(@monPriceLevel16,0) ELSE monPrice END)
		,intFromQty = (CASE WHEN ISNULL(@intPriceLevel16FromQty,0) <> -1 THEN ISNULL(@intPriceLevel16FromQty,0) ELSE intFromQty END)
		,intToQty = (CASE WHEN ISNULL(@intPriceLevel16ToQty,0) <> -1 THEN ISNULL(@intPriceLevel16ToQty,0) ELSE intToQty END)
		,vcName = (CASE WHEN ISNULL(@vcPriceLevel16Name,'') <> '-1' THEN ISNULL(@vcPriceLevel16Name,0) ELSE vcName END)
	WHERE 
		PriceLevelID = 16
	
	UPDATE 
		@Defualt 
	SET 
		monPrice = (CASE WHEN ISNULL(@monPriceLevel17,0) <> -1 THEN ISNULL(@monPriceLevel17,0) ELSE monPrice END)
		,intFromQty = (CASE WHEN ISNULL(@intPriceLevel17FromQty,0) <> -1 THEN ISNULL(@intPriceLevel17FromQty,0) ELSE intFromQty END)
		,intToQty = (CASE WHEN ISNULL(@intPriceLevel17ToQty,0) <> -1 THEN ISNULL(@intPriceLevel17ToQty,0) ELSE intToQty END)
		,vcName = (CASE WHEN ISNULL(@vcPriceLevel17Name,'') <> '-1' THEN ISNULL(@vcPriceLevel17Name,0) ELSE vcName END)
	WHERE 
		PriceLevelID = 17
	
	UPDATE 
		@Defualt 
	SET 
		monPrice = (CASE WHEN ISNULL(@monPriceLevel18,0) <> -1 THEN ISNULL(@monPriceLevel18,0) ELSE monPrice END)
		,intFromQty = (CASE WHEN ISNULL(@intPriceLevel18FromQty,0) <> -1 THEN ISNULL(@intPriceLevel18FromQty,0) ELSE intFromQty END)
		,intToQty = (CASE WHEN ISNULL(@intPriceLevel18ToQty,0) <> -1 THEN ISNULL(@intPriceLevel18ToQty,0) ELSE intToQty END)
		,vcName = (CASE WHEN ISNULL(@vcPriceLevel18Name,'') <> '-1' THEN ISNULL(@vcPriceLevel18Name,0) ELSE vcName END)
	WHERE 
		PriceLevelID = 18
	
	UPDATE 
		@Defualt 
	SET 
		monPrice = (CASE WHEN ISNULL(@monPriceLevel19,0) <> -1 THEN ISNULL(@monPriceLevel19,0) ELSE monPrice END)
		,intFromQty = (CASE WHEN ISNULL(@intPriceLevel19FromQty,0) <> -1 THEN ISNULL(@intPriceLevel19FromQty,0) ELSE intFromQty END)
		,intToQty = (CASE WHEN ISNULL(@intPriceLevel19ToQty,0) <> -1 THEN ISNULL(@intPriceLevel19ToQty,0) ELSE intToQty END)
		,vcName = (CASE WHEN ISNULL(@vcPriceLevel19Name,'') <> '-1' THEN ISNULL(@vcPriceLevel19Name,0) ELSE vcName END)
	WHERE 
		PriceLevelID = 19
	
	UPDATE 
		@Defualt 
	SET 
		monPrice = (CASE WHEN ISNULL(@monPriceLevel20,0) <> -1 THEN ISNULL(@monPriceLevel20,0) ELSE monPrice END)
		,intFromQty = (CASE WHEN ISNULL(@intPriceLevel20FromQty,0) <> -1 THEN ISNULL(@intPriceLevel20FromQty,0) ELSE intFromQty END)
		,intToQty = (CASE WHEN ISNULL(@intPriceLevel20ToQty,0) <> -1 THEN ISNULL(@intPriceLevel20ToQty,0) ELSE intToQty END)
		,vcName = (CASE WHEN ISNULL(@vcPriceLevel20Name,'') <> '-1' THEN ISNULL(@vcPriceLevel20Name,0) ELSE vcName END)
	WHERE 
		PriceLevelID = 20
	
	DECLARE @MaxPriceLevelIDWithSomeValue AS INT
	
	SELECT @MaxPriceLevelIDWithSomeValue=MAX(PriceLevelID) FROM @Defualt WHERE (monPrice <> -1 OR intFromQty <> -1 OR intToQty <> -1)

	-- NOW REPLACE PRICE -1 TO 0 IN ALL PRICE LEVELS WHICH ARE LESS THAN @MaxPriceLevelIDWithSomeValue 
	-- (THIS REQUIRED BECAUSE IF USER HAS PROVIDED PRICE LEVEL 1 AND PRICE LEVEL 6 COLUMN IN IMPORT FILE THEN WE HAVE TO ADD 0 FROM PRICE LEVEL 2,3,4,5)
	UPDATE
		@Defualt
	SET
		monPrice = (CASE WHEN monPrice = -1 THEN 0 ELSE monPrice END)
		,intFromQty = (CASE WHEN intFromQty = -1 THEN 0 ELSE intFromQty END)
		,intToQty = (CASE WHEN intToQty = -1 THEN 0 ELSE intToQty END)
		,vcName = (CASE WHEN vcName = '-1' THEN '' ELSE vcName END)
	WHERE
		PriceLevelID <= @MaxPriceLevelIDWithSomeValue
		AND (monPrice = -1 OR intFromQty=-1 OR intToQty=-1)

	IF EXISTS(SELECT * FROM @Defualt WHERE PriceLevelID <= @MaxPriceLevelIDWithSomeValue AND tintRuleType IN (1,2) AND (ISNULL(intFromQty,0) = 0 OR ISNULL(intToQty,0) = 0))
	BEGIN
		RAISERROR('QTY_FROM_AND_TO_MUST_BE_GREATER_THEN_0',16,1)
		RETURN
	END

	IF EXISTS(SELECT * FROM @Defualt WHERE PriceLevelID <= @MaxPriceLevelIDWithSomeValue AND tintRuleType IN (1,2) AND ISNULL(tintDiscountType,0) = 0)
	BEGIN
		RAISERROR('INVALID_PRICE_LEVEL_DISCOUNT_TYPE',16,1)
		RETURN
	END

	BEGIN TRY
	BEGIN TRANSACTION
		DELETE FROM PricingTable WHERE numItemCode=@numItemCode

		INSERT INTO PricingTable
		(
			numPriceRuleID,numItemCode,vcName,decDiscount,tintRuleType,tintDiscountType,intFromQty,intToQty
		)
		SELECT
			0,@numItemCode,vcName,monPrice,tintRuleType,(CASE WHEN tintRuleType = 3 THEN 0 ELSE tintDiscountType END),(CASE WHEN tintRuleType = 3 THEN 0 ELSE intFromQty END),(CASE WHEN tintRuleType = 3 THEN 0 ELSE intToQty END)
		FROM
			@Defualt
		WHERE
			PriceLevelID <= @MaxPriceLevelIDWithSomeValue

	COMMIT
	END TRY
	BEGIN CATCH
		DECLARE @ErrorMessage NVARCHAR(4000)
		DECLARE @ErrorNumber INT
		DECLARE @ErrorSeverity INT
		DECLARE @ErrorState INT
		DECLARE @ErrorLine INT
		DECLARE @ErrorProcedure NVARCHAR(200)

		IF @@TRANCOUNT > 0
			ROLLBACK TRANSACTION;

		SELECT 
			@ErrorMessage = ERROR_MESSAGE(),
			@ErrorNumber = ERROR_NUMBER(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE(),
			@ErrorLine = ERROR_LINE(),
			@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

		RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);
	END CATCH

END
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_Item_SearchForSalesOrder' )
    DROP PROCEDURE USP_Item_SearchForSalesOrder
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Sandeep Patel
-- Create date: 3 March 2014
-- Description:	Search items for sales order
-- =============================================
CREATE PROCEDURE USP_Item_SearchForSalesOrder
    @tintOppType AS TINYINT ,
    @numDomainID AS NUMERIC(9) ,
    @numDivisionID AS NUMERIC(9) = 0 ,
    @str AS VARCHAR(1000) ,
    @numUserCntID AS NUMERIC(9) ,
    @tintSearchOrderCustomerHistory AS TINYINT = 0 ,
    @numPageIndex AS INT ,
    @numPageSize AS INT ,
    @WarehouseID AS NUMERIC(18, 0) = NULL ,
    @TotalCount AS INT OUTPUT
AS
    BEGIN
  SET NOCOUNT ON;

DECLARE @strNewQuery NVARCHAR(MAX)
DECLARE @strSQL AS NVARCHAR(MAX)
SELECT  @str = REPLACE(@str, '''', '''''')

DECLARE @TableRowCount TABLE ( Value INT );
	
SELECT  *
INTO    #Temp1
FROM    ( SELECT    numFieldId ,
                    vcDbColumnName ,
                    ISNULL(vcCultureFieldName, vcFieldName) AS vcFieldName ,
                    tintRow AS tintOrder ,
                    0 AS Custom
          FROM      View_DynamicColumns
          WHERE     numFormId = 22
                    AND numDomainID = @numDomainID
                    AND tintPageType = 1
                    AND bitCustom = 0
                    AND ISNULL(bitSettingField, 0) = 1
                    AND numRelCntType = 0
          UNION
          SELECT    numFieldId ,
                    vcFieldName ,
                    vcFieldName ,
                    tintRow AS tintOrder ,
                    1 AS Custom
          FROM      View_DynamicCustomColumns
          WHERE     Grp_id = 5
                    AND numFormId = 22
                    AND numDomainID = @numDomainID
                    AND tintPageType = 1
                    AND bitCustom = 1
                    AND numRelCntType = 0
        ) X 
  
IF NOT EXISTS ( SELECT  *
                FROM    #Temp1 )
    BEGIN
        INSERT  INTO #Temp1
                SELECT  numFieldId ,
                        vcDbColumnName ,
                        ISNULL(vcCultureFieldName, vcFieldName) AS vcFieldName ,
                        tintorder ,
                        0
                FROM    View_DynamicDefaultColumns
                WHERE   numFormId = 22
                        AND bitDefault = 1
                        AND ISNULL(bitSettingField, 0) = 1
                        AND numDomainID = @numDomainID 
    END

CREATE TABLE #tempItemCode ( numItemCode NUMERIC(9) )
DECLARE @bitRemoveVendorPOValidation AS BIT
SELECT  
	@bitRemoveVendorPOValidation = ISNULL(bitRemoveVendorPOValidation,0)
FROM    
	domain
WHERE  
	numDomainID = @numDomainID

IF @tintOppType > 0
    BEGIN 
        IF CHARINDEX(',', @str) > 0
        BEGIN
			DECLARE @itemSearchText VARCHAR(100)
            DECLARE @vcAttribureSearch VARCHAR(MAX)
			DECLARE @vcAttribureSearchCondition VARCHAR(MAX)

			SET @itemSearchText = LTRIM(RTRIM(SUBSTRING(@str,0,CHARINDEX(',',@str))))

			--GENERATES ATTRIBUTE SEARCH CONDITION
			SET @vcAttribureSearch = SUBSTRING(@str,CHARINDEX(',',@str) + 1,LEN(@str))
	
			DECLARE @attributeSearchText VARCHAR(MAX)
			DECLARE @TempTable TABLE (vcValue VARCHAR(MAX))

			WHILE LEN(@vcAttribureSearch) > 0
			BEGIN
				SET @attributeSearchText = LEFT(@vcAttribureSearch, 
										ISNULL(NULLIF(CHARINDEX(',', @vcAttribureSearch) - 1, -1),
										LEN(@vcAttribureSearch)))
				SET @vcAttribureSearch = SUBSTRING(@vcAttribureSearch,
												ISNULL(NULLIF(CHARINDEX(',', @vcAttribureSearch), 0),
												LEN(@vcAttribureSearch)) + 1, LEN(@vcAttribureSearch))

				INSERT INTO @TempTable (vcValue) VALUES ( @attributeSearchText )
			END
		
			--REMOVES WHITE SPACES
			UPDATE
				@TempTable
			SET
				vcValue = LTRIM(RTRIM(vcValue))
	
			--CONVERTING TABLE TO COMMA SEPERATED STRING AFTER REMOVEING WHITE SPACES
			SELECT @vcAttribureSearch = COALESCE(@vcAttribureSearch,'') +  CONVERT(VARCHAR(MAX),vcValue) + ',' FROM @TempTable
	
			--REMOVE LAST COMMA FROM FINAL SEARCH STRING
			SET @vcAttribureSearch = LTRIM(RTRIM(@vcAttribureSearch))
			IF DATALENGTH(@vcAttribureSearch) > 0
				SET @vcAttribureSearch = LEFT(@vcAttribureSearch, LEN(@vcAttribureSearch) - 1)
	
			--START: GENERATES ATTRIBUTE SEARCH CONDITION
			SELECT 	@vcAttribureSearchCondition = '(TEMPMATRIX.Attributes LIKE ''%' + REPLACE(@vcAttribureSearch, ',', '%'' AND TEMPMATRIX.Attributes LIKE ''%') + '%'')' 

			--LOGIC FOR GENERATING FINAL SQL STRING
			IF @tintOppType = 1 OR ( @bitRemoveVendorPOValidation = 1 AND @tintOppType = 2)
                BEGIN      

				SET @strSQL = 'SELECT
									I.numItemCode,
									I.numVendorID,
									MIN(WHT.numWareHouseItemID) as numWareHouseItemID,
									dbo.fn_GetAttributes(WHT.numWareHouseItemId,I.bitSerialized) AS [Attributes]
								FROM
									dbo.Item I WITH (NOLOCK)
								LEFT JOIN
									dbo.WareHouseItems WHT
								ON
									I.numItemCode = WHT.numItemID
								WHERE 
									((ISNULL(I.numItemGroup,0) > 0 AND I.vcItemName LIKE ''%' + @itemSearchText + '%'') OR (ISNULL(I.numItemGroup,0) = 0 AND I.vcItemName LIKE ''%' + @str + '%''))
									AND ISNULL(I.Isarchieve, 0) <> 1
									AND I.charItemType NOT IN ( ''A'' )
									AND I.numDomainID = ' + CAST(@numDomainID AS VARCHAR(20)) + '
									AND (WHT.numWareHouseID = ' + CAST(ISNULL(@WarehouseID,0) AS VARCHAR(20)) + ' OR ' + CAST(ISNULL(@WarehouseID,0) AS VARCHAR(20)) + ' = 0)'

				--- added Asset validation  by sojan
                IF @bitRemoveVendorPOValidation = 1
                    AND @tintOppType = 2
                    SET @strSQL = @strSQL
                        + ' and ISNULL(I.bitKitParent,0) = 0 AND ISNULL(I.bitAssembly,0) = 0 '

                IF @tintSearchOrderCustomerHistory = 1 AND @numDivisionID > 0
                BEGIN
                    SET @strSQL = @strSQL
                        + ' and I.numItemCode in (select distinct oppI.numItemCode from 
						OpportunityMaster oppM join OpportunityItems oppI on oppM.numOppId=oppI.numOppId where oppM.numDomainID='
                        + CONVERT(VARCHAR(20), @numDomainID)
                        + ' and oppM.numDivisionId='
                        + CONVERT(VARCHAR(20), @numDivisionId)
                        + ')'
                END

				SET @strSQL = @strSQL + ' GROUP BY
										I.numItemCode,
										I.numVendorID,
										dbo.fn_GetAttributes(numWareHouseItemId,I.bitSerialized)'

				SET @strSQL = 
				'
				SELECT    
					SRNO=ROW_NUMBER() OVER( ORDER BY  I.vcItemName),
					I.numItemCode,
					(SELECT TOP 1 vcPathForImage FROM ItemImages WHERE numItemCode = I.numItemCode) AS vcPathForImage ,
					(SELECT TOP 1 vcPathForTImage FROM ItemImages WHERE numItemCode = I.numItemCode) AS vcPathForTImage ,
					ISNULL(vcItemName, '''') vcItemName ,
					CASE WHEN I.[charItemType] = ''P''
					THEN CONVERT(VARCHAR(100), ISNULL((SELECT TOP 1 ROUND(monWListPrice,2) FROM [WareHouseItems] WHERE [numItemID] = I.numItemCode), 0))
					ELSE CONVERT(VARCHAR(100), ROUND(monListPrice, 2))
					END AS monListPrice,
					ISNULL(vcSKU, '''') vcSKU ,
					ISNULL(numBarCodeId, 0) numBarCodeId ,
					ISNULL(vcModelID, '''') vcModelID ,
					ISNULL(txtItemDesc, '''') AS txtItemDesc ,
					ISNULL(C.vcCompanyName, '''') AS vcCompanyName,
					TEMPMATRIX.numWareHouseItemID,
					TEMPMATRIX.Attributes AS [vcAttributes],
					(CASE 
						WHEN ISNULL(I.bitKitParent,0) = 1 
						THEN 
							(
								CASE
									WHEN (SELECT COUNT(*) FROM ItemDetails INNER JOIN Item ON ItemDetails.numChildItemID = Item.numItemCode WHERE numItemKitID=I.numItemCode AND ISNULL(Item.bitKitParent,0) = 1) > 0
									THEN 1
									ELSE 0
								END
							)
						ELSE 0 
					END) bitHasKitAsChild
				FROM  
					(' + @strSQL +') AS TEMPMATRIX
				INNER JOIN    
					Item I
				ON
					TEMPMATRIX.numItemCode = I.numItemCode
				LEFT JOIN 
					DivisionMaster D 
				ON 
					I.numVendorID = D.numDivisionID
				LEFT JOIN 
					CompanyInfo C 
				ON 
					C.numCompanyID = D.numCompanyID
				WHERE  
				  ((ISNULL(I.numItemGroup,0) = 0 AND I.vcItemName LIKE ''%' + @str + '%'') OR ' + @vcAttribureSearchCondition + ')'

				

                INSERT  INTO @TableRowCount
                        EXEC
                            ( 'SELECT COUNT(*) FROM ( ' + @strSQL
                                + ') as t2'
                                    );

                SELECT  @TotalCount = Value
                FROM    @TableRowCount;

                SET @strNewQuery = 'select * from (' + @strSQL
                    + ') as t where SRNO> '
                    + CAST(( ( @numPageIndex - 1 ) * @numPageSize ) AS VARCHAR(10))
                    + ' and SRNO < '
                    + CAST(( ( @numPageIndex * @numPageSize ) + 1 ) AS VARCHAR(10))
                    + ' order by  vcItemName'

				--PRINT @strNewQuery
                EXEC(@strNewQuery)
				END
			ELSE
				BEGIN

				SET @strSQL = 'SELECT
									I.numItemCode,
									I.numVendorID,
									MIN(WHT.numWareHouseItemID) as numWareHouseItemID,
									dbo.fn_GetAttributes(WHT.numWareHouseItemId,I.bitSerialized) AS [Attributes]
								FROM
									dbo.Item I WITH (NOLOCK)
								INNER JOIN
									dbo.Vendor V
								ON
									V.numItemCode = I.numItemCode
								LEFT JOIN
									dbo.WareHouseItems WHT
								ON
									I.numItemCode = WHT.numItemID
								WHERE 
									((ISNULL(I.numItemGroup,0) > 0 AND I.vcItemName LIKE ''%' + @itemSearchText + '%'') OR (ISNULL(I.numItemGroup,0) = 0 AND I.vcItemName LIKE ''%' + @str + '%''))
									AND ISNULL(I.Isarchieve, 0) <> 1
									AND I.charItemType NOT IN ( ''A'' )
									AND I.numDomainID = ' + CAST(@numDomainID AS VARCHAR(20)) + '
									AND (WHT.numWareHouseID = ' + CAST(ISNULL(@WarehouseID,0) AS VARCHAR(20)) + ' OR ' + CAST(ISNULL(@WarehouseID,0) AS VARCHAR(20)) + ' = 0)'

				--- added Asset validation  by sojan
                IF @tintSearchOrderCustomerHistory = 1 AND @numDivisionID > 0
                BEGIN
                    SET @strSQL = @strSQL
                        + ' and I.numItemCode in (select distinct oppI.numItemCode from 
						OpportunityMaster oppM join OpportunityItems oppI on oppM.numOppId=oppI.numOppId where oppM.numDomainID='
                        + CONVERT(VARCHAR(20), @numDomainID)
                        + ' and oppM.numDivisionId='
                        + CONVERT(VARCHAR(20), @numDivisionId)
                        + ')'
                END

				SET @strSQL = @strSQL + ' GROUP BY
										I.numItemCode,
										I.numVendorID,
										dbo.fn_GetAttributes(numWareHouseItemId,I.bitSerialized)'

				SET @strSQL = 
				'
				SELECT    
					SRNO=ROW_NUMBER() OVER( ORDER BY  I.vcItemName),
					I.numItemCode,
					(SELECT TOP 1 vcPathForImage FROM ItemImages WHERE numItemCode = I.numItemCode) AS vcPathForImage ,
					(SELECT TOP 1 vcPathForTImage FROM ItemImages WHERE numItemCode = I.numItemCode) AS vcPathForTImage ,
					ISNULL(vcItemName, '''') vcItemName ,
					CASE WHEN I.[charItemType] = ''P''
					THEN CONVERT(VARCHAR(100), ISNULL((SELECT TOP 1 ROUND(monWListPrice,2) FROM [WareHouseItems] WHERE [numItemID] = I.numItemCode), 0))
					ELSE CONVERT(VARCHAR(100), ROUND(monListPrice, 2))
					END AS monListPrice,
					ISNULL(vcSKU, '''') vcSKU ,
					ISNULL(numBarCodeId, 0) numBarCodeId ,
					ISNULL(vcModelID, '''') vcModelID ,
					ISNULL(txtItemDesc, '''') AS txtItemDesc ,
					ISNULL(C.vcCompanyName, '''') AS vcCompanyName,
					TEMPMATRIX.numWareHouseItemID,
					TEMPMATRIX.Attributes AS [vcAttributes],
					(CASE 
						WHEN ISNULL(I.bitKitParent,0) = 1 
						THEN 
							(
								CASE
									WHEN (SELECT COUNT(*) FROM ItemDetails INNER JOIN Item ON ItemDetails.numChildItemID = Item.numItemCode WHERE numItemKitID=I.numItemCode AND ISNULL(Item.bitKitParent,0) = 1) > 0
									THEN 1
									ELSE 0
								END
							)
						ELSE 0 
					END) bitHasKitAsChild
				FROM  
					(' + @strSQL +') AS TEMPMATRIX
				INNER JOIN    
					Item I
				ON
					TEMPMATRIX.numItemCode = I.numItemCode
				LEFT JOIN 
					DivisionMaster D 
				ON 
					I.numVendorID = D.numDivisionID
				LEFT JOIN 
					CompanyInfo C 
				ON 
					C.numCompanyID = D.numCompanyID
				WHERE  
				 ((ISNULL(I.numItemGroup,0) = 0 AND I.vcItemName LIKE ''%' + @str + '%'') OR ' + @vcAttribureSearchCondition + ')'

                INSERT  INTO @TableRowCount
                        EXEC
                            ( 'SELECT COUNT(*) FROM ( ' + @strSQL
                                + ') as t2'
                                    );

                SELECT  @TotalCount = Value
                FROM    @TableRowCount;

                SET @strNewQuery = 'select * from (' + @strSQL
                    + ') as t where SRNO> '
                    + CAST(( ( @numPageIndex - 1 ) * @numPageSize ) AS VARCHAR(10))
                    + ' and SRNO < '
                    + CAST(( ( @numPageIndex * @numPageSize ) + 1 ) AS VARCHAR(10))
                    + ' order by  vcItemName'

				--PRINT @strNewQuery
                EXEC(@strNewQuery)
			END	
        END
        ELSE
        BEGIN
				DECLARE @vcWareHouseSearch VARCHAR(1000)
				DECLARE @vcVendorSearch VARCHAR(1000)
               

				SET @vcWareHouseSearch = ''
				SET @vcVendorSearch = ''

                DECLARE @tintOrder AS INT
                DECLARE @Fld_id AS NVARCHAR(20)
                DECLARE @Fld_Name AS VARCHAR(20)
                SET @strSQL = ''
                SELECT TOP 1
                        @tintOrder = tintOrder + 1 ,
                        @Fld_id = numFieldId ,
                        @Fld_Name = vcFieldName
                FROM    #Temp1
                WHERE   Custom = 1
                ORDER BY tintOrder
                WHILE @tintOrder > 0
                    BEGIN
                        SET @strSQL = @strSQL + ', dbo.GetCustFldValueItem('
                            + @Fld_id + ', I.numItemCode) as [' + @Fld_Name
                            + ']'

                        SELECT TOP 1
                                @tintOrder = tintOrder + 1 ,
                                @Fld_id = numFieldId ,
                                @Fld_Name = vcFieldName
                        FROM    #Temp1
                        WHERE   Custom = 1
                                AND tintOrder >= @tintOrder
                        ORDER BY tintOrder
                        IF @@rowcount = 0
                            SET @tintOrder = 0
                    END


				--Temp table for Item Search Configuration

                SELECT  *
                INTO    #tempSearch
                FROM    ( SELECT    numFieldId ,
                                    vcDbColumnName ,
                                    ISNULL(vcCultureFieldName, vcFieldName) AS vcFieldName ,
                                    tintRow AS tintOrder ,
                                    0 AS Custom
                          FROM      View_DynamicColumns
                          WHERE     numFormId = 22
                                    AND numDomainID = @numDomainID
                                    AND tintPageType = 1
                                    AND bitCustom = 0
                                    AND ISNULL(bitSettingField, 0) = 1
                                    AND numRelCntType = 1
                          UNION
                          SELECT    numFieldId ,
                                    vcFieldName ,
                                    vcFieldName ,
                                    tintRow AS tintOrder ,
                                    1 AS Custom
                          FROM      View_DynamicCustomColumns
                          WHERE     Grp_id = 5
                                    AND numFormId = 22
                                    AND numDomainID = @numDomainID
                                    AND tintPageType = 1
                                    AND bitCustom = 1
                                    AND numRelCntType = 1
                        ) X 
  
                IF NOT EXISTS ( SELECT  *
                                FROM    #tempSearch )
                    BEGIN
                        INSERT  INTO #tempSearch
                                SELECT  numFieldId ,
                                        vcDbColumnName ,
                                        ISNULL(vcCultureFieldName, vcFieldName) AS vcFieldName ,
                                        tintorder ,
                                        0
                                FROM    View_DynamicDefaultColumns
                                WHERE   numFormId = 22
                                        AND bitDefault = 1
                                        AND ISNULL(bitSettingField, 0) = 1
                                        AND numDomainID = @numDomainID 
                    END

				--Regular Search
                DECLARE @strSearch AS VARCHAR(8000) ,
                    @CustomSearch AS VARCHAR(4000) ,
                    @numFieldId AS NUMERIC(9)
                SET @strSearch = ''
                SELECT TOP 1
                        @tintOrder = tintOrder + 1 ,
                        @Fld_Name = vcDbColumnName ,
                        @numFieldId = numFieldId
                FROM    #tempSearch
                WHERE   Custom = 0
                ORDER BY tintOrder
                WHILE @tintOrder > -1
                    BEGIN
                        IF @Fld_Name = 'vcPartNo'
						BEGIN
                            INSERT  INTO #tempItemCode
                                    SELECT DISTINCT
                                            Item.numItemCode
                                    FROM    Vendor
                                            JOIN Item ON Item.numItemCode = Vendor.numItemCode
                                    WHERE   Item.numDomainID = @numDomainID
                                            AND Vendor.numDomainID = @numDomainID
                                            AND Vendor.vcPartNo IS NOT NULL
                                            AND LEN(Vendor.vcPartNo) > 0
                                            AND vcPartNo LIKE '%' + @str + '%'
							
							SET @vcVendorSearch = 'dbo.Vendor.vcPartNo LIKE ''%' + @str + '%'''
							
						END
                        ELSE IF @Fld_Name = 'vcBarCode'
						BEGIN
                            INSERT  INTO #tempItemCode
                                    SELECT DISTINCT
                                            Item.numItemCode
                                    FROM    Item
                                            JOIN WareHouseItems WI ON WI.numDomainID = @numDomainID
                                                            AND Item.numItemCode = WI.numItemID
                                            JOIN Warehouses W ON W.numDomainID = @numDomainID
                                                            AND W.numWareHouseID = WI.numWareHouseID
                                    WHERE   Item.numDomainID = @numDomainID
                                            AND ISNULL(Item.numItemGroup,
                                                        0) > 0
                                            AND WI.vcBarCode IS NOT NULL
                                            AND LEN(WI.vcBarCode) > 0
                                            AND WI.vcBarCode LIKE '%' + @str + '%'

							SET @vcWareHouseSearch = 'dbo.WareHouseItems.vcBarCode LIKE ''%' + @str + '%'''
						END
                        ELSE IF @Fld_Name = 'vcWHSKU'
						BEGIN
                            INSERT  INTO #tempItemCode
                                    SELECT DISTINCT
                                            Item.numItemCode
                                    FROM    Item
                                            JOIN WareHouseItems WI ON WI.numDomainID = @numDomainID
                                                        AND Item.numItemCode = WI.numItemID
                                            JOIN Warehouses W ON W.numDomainID = @numDomainID
                                                        AND W.numWareHouseID = WI.numWareHouseID
                                    WHERE   Item.numDomainID = @numDomainID
                                            AND ISNULL(Item.numItemGroup,
                                                        0) > 0
                                            AND WI.vcWHSKU IS NOT NULL
                                            AND LEN(WI.vcWHSKU) > 0
                                            AND WI.vcWHSKU LIKE '%'
                                            + @str + '%'

							IF LEN(@vcWareHouseSearch) > 0
								SET @vcWareHouseSearch = @vcWareHouseSearch + ' OR dbo.WareHouseItems.vcWHSKU LIKE ''%' + @str + '%'''
							ELSE
								SET @vcWareHouseSearch = 'dbo.WareHouseItems.vcWHSKU LIKE ''%' + @str + '%'''
						END
                        ELSE
                            SET @strSearch = @strSearch
                                + (CASE @Fld_Name WHEN 'numItemCode' THEN ' I.[' + @Fld_Name ELSE ' [' + @Fld_Name END)  + '] LIKE ''%' + @str
                                + '%'''

                        SELECT TOP 1
                                @tintOrder = tintOrder + 1 ,
                                @Fld_Name = vcDbColumnName ,
                                @numFieldId = numFieldId
                        FROM    #tempSearch
                        WHERE   tintOrder >= @tintOrder
                                AND Custom = 0
                        ORDER BY tintOrder
                        IF @@rowcount = 0
                            BEGIN
                                SET @tintOrder = -1
                            END
                        ELSE
                            IF @Fld_Name != 'vcPartNo'
                                AND @Fld_Name != 'vcBarCode'
                                AND @Fld_Name != 'vcWHSKU'
                                BEGIN
                                    SET @strSearch = @strSearch + ' or '
                                END
                    END

                IF ( SELECT COUNT(*)
                     FROM   #tempItemCode
                   ) > 0
                    SET @strSearch = @strSearch
                        + ' or  I.numItemCode in (select numItemCode from #tempItemCode)' 

				--Custom Search
                SET @CustomSearch = ''
                SELECT TOP 1
                        @tintOrder = tintOrder + 1 ,
                        @Fld_Name = vcDbColumnName ,
                        @numFieldId = numFieldId
                FROM    #tempSearch
                WHERE   Custom = 1
                ORDER BY tintOrder
                WHILE @tintOrder > -1
                    BEGIN
    
                        SET @CustomSearch = @CustomSearch
                            + CAST(@numFieldId AS VARCHAR(10)) 

                        SELECT TOP 1
                                @tintOrder = tintOrder + 1 ,
                                @Fld_Name = vcDbColumnName ,
                                @numFieldId = numFieldId
                        FROM    #tempSearch
                        WHERE   tintOrder >= @tintOrder
                                AND Custom = 1
                        ORDER BY tintOrder
                        IF @@rowcount = 0
                            BEGIN
                                SET @tintOrder = -1
                            END
                        ELSE
                            BEGIN
                                SET @CustomSearch = @CustomSearch + ' , '
                            END
                    END
	
                IF LEN(@CustomSearch) > 0
                    BEGIN
                        SET @CustomSearch = ' I.numItemCode in (SELECT RecId FROM dbo.CFW_FLD_Values_Item WHERE Fld_ID IN ('
                            + @CustomSearch + ') and dbo.GetCustFldValueItem(Fld_ID,RecId) like ''%'
                            + @str + '%'')'

                        IF LEN(@strSearch) > 0
                            SET @strSearch = @strSearch + ' OR '
                                + @CustomSearch
                        ELSE
                            SET @strSearch = @CustomSearch  
                    END


                IF @tintOppType = 1
                    OR ( @bitRemoveVendorPOValidation = 1
                         AND @tintOppType = 2
                       )
                    BEGIN      
                        SET @strSQL = 'select I.numItemCode,
									  (SELECT TOP 1 vcPathForImage from ItemImages WHERE numItemCode = I.numItemCode) as vcPathForImage,
									  (SELECT TOP 1 vcPathForTImage from ItemImages WHERE numItemCode = I.numItemCode) as vcPathForTImage,
									  isnull(vcItemName,'''') vcItemName,
									  CASE 
									  WHEN I.[charItemType]=''P''
									  THEN convert(varchar(100),ISNULL((SELECT TOP 1 ROUND(monWListPrice,2) FROM [WareHouseItems] WHERE [numItemID]=I.numItemCode),0)) 
									  ELSE 
										CONVERT(VARCHAR(100), ROUND(monListPrice, 2)) 
									  END AS monListPrice,
									  isnull(vcSKU,'''') vcSKU,
									  isnull(numBarCodeId,0) numBarCodeId,
									  isnull(vcModelID,'''') vcModelID,
									  isnull(txtItemDesc,'''') as txtItemDesc,
									  isnull(C.vcCompanyName,'''') as vcCompanyName,
									  WHT.numWareHouseItemID,
									  dbo.fn_GetAttributes(WHT.numWareHouseItemId,I.bitSerialized) AS [vcAttributes],
									  (CASE 
											WHEN ISNULL(I.bitKitParent,0) = 1 
											THEN 
												(
													CASE
														WHEN (SELECT COUNT(*) FROM ItemDetails INNER JOIN Item ON ItemDetails.numChildItemID = Item.numItemCode WHERE numItemKitID=I.numItemCode AND ISNULL(Item.bitKitParent,0) = 1) > 0
														THEN 1
														ELSE 0
													END
												)
											ELSE 0 
										END) bitHasKitAsChild,ISNULL(I.bitMatrix,0) AS bitMatrix,ISNULL(I.numItemGroup,0) AS numItemGroup '
									  + @strSQL
									  + ' INTO #TEMPItems FROM Item  I WITH (NOLOCK)
									  Left join DivisionMaster D              
									  on I.numVendorID=D.numDivisionID  
									  left join CompanyInfo C  
									  on C.numCompanyID=D.numCompanyID 
									  LEFT JOIN
											dbo.WareHouseItems WHT
									  ON
											WHT.numItemID = I.numItemCode AND
											WHT.numWareHouseItemID = (' +
																			CASE LEN(@vcWareHouseSearch)
																			WHEN 0 
																			THEN 
																				'SELECT 
																					TOP 1 numWareHouseItemID 
																				FROM 
																					dbo.WareHouseItems 
																				WHERE 
																					dbo.WareHouseItems.numDomainID = ' + CAST(@numDomainID AS VARCHAR(20)) + ' 
																					AND dbo.WareHouseItems.numItemID = I.numItemCode 
																					AND (dbo.WareHouseItems.numWareHouseID = ' +  CAST(ISNULL(@WarehouseID,0) AS VARCHAR(20))  + ' OR ' + CAST(ISNULL(@WarehouseID,0) AS VARCHAR(20)) + ' = 0)'
																			ELSE
																				'CASE 
																					WHEN (SELECT 
																								COUNT(*) 
																							FROM 
																								dbo.WareHouseItems 
																							WHERE 
																								dbo.WareHouseItems.numDomainID = ' + CAST(@numDomainID AS VARCHAR(20)) + ' 
																								AND dbo.WareHouseItems.numItemID = I.numItemCode 
																								AND (dbo.WareHouseItems.numWareHouseID = ' +  CAST(ISNULL(@WarehouseID,0) AS VARCHAR(20))  + ' OR ' + CAST(ISNULL(@WarehouseID,0) AS VARCHAR(20)) + ' = 0)' +
																								' AND (' + @vcWareHouseSearch + ')) > 0 
																					THEN
																						(SELECT 
																							TOP 1 numWareHouseItemID 
																						FROM 
																							dbo.WareHouseItems 
																						WHERE 
																							dbo.WareHouseItems.numDomainID = ' + CAST(@numDomainID AS VARCHAR(20)) + ' 
																							AND dbo.WareHouseItems.numItemID = I.numItemCode 
																							AND (dbo.WareHouseItems.numWareHouseID = ' +  CAST(ISNULL(@WarehouseID,0) AS VARCHAR(20))  + ' OR ' + CAST(ISNULL(@WarehouseID,0) AS VARCHAR(20)) + ' = 0) AND (' + @vcWareHouseSearch + '))
																					ELSE
																						(SELECT 
																							TOP 1 numWareHouseItemID 
																						FROM 
																							dbo.WareHouseItems 
																						WHERE 
																							dbo.WareHouseItems.numDomainID = ' + CAST(@numDomainID AS VARCHAR(20)) + ' 
																							AND dbo.WareHouseItems.numItemID = I.numItemCode 
																							AND (dbo.WareHouseItems.numWareHouseID = ' +  CAST(ISNULL(@WarehouseID,0) AS VARCHAR(20))  + ' OR ' + CAST(ISNULL(@WarehouseID,0) AS VARCHAR(20)) + ' = 0))
																					END' 
																			END
																		+
																	') 
										where ((I.charItemType <> ''P'') OR ((SELECT COUNT(*) FROM dbo.WareHouseItems WHERE numItemID = I.numItemCode AND numDomainID = '
									  + CONVERT(VARCHAR(20), @numDomainID)
									  + ' AND numWareHouseID =  ' +  CAST(ISNULL(@WarehouseID,0) AS VARCHAR(20))  + ' OR ' + CAST(ISNULL(@WarehouseID,0) AS VARCHAR(20)) + ' = 0) > 0)) AND ISNULL(I.Isarchieve,0) <> 1 And I.charItemType NOT IN(''A'') AND  I.numDomainID='
									  + CONVERT(VARCHAR(20), @numDomainID) + ' and ('
									  + @strSearch + ') '   
						     
						--- added Asset validation  by sojan
                        IF @bitRemoveVendorPOValidation = 1
                            AND @tintOppType = 2
                            SET @strSQL = @strSQL
                                + ' and ISNULL(I.bitKitParent,0) = 0 AND ISNULL(I.bitAssembly,0) = 0 '

                        IF @tintSearchOrderCustomerHistory = 1
                            AND @numDivisionID > 0
                            BEGIN
                                SET @strSQL = @strSQL
                                    + ' and I.numItemCode in (select distinct oppI.numItemCode from 
									OpportunityMaster oppM join OpportunityItems oppI on oppM.numOppId=oppI.numOppId where oppM.numDomainID='
                                    + CONVERT(VARCHAR(20), @numDomainID)
                                    + ' and oppM.numDivisionId='
                                    + CONVERT(VARCHAR(20), @numDivisionId)
                                    + ')'
                            END


						 SET @strSQL = @strSQL + 'DELETE FROM 
														#TEMPItems
													WHERE 
														numItemCode IN (
																			SELECT 
																				F.numItemCode
																			FROM 
																				#TEMPItems AS F
																			WHERE 
																				ISNULL(F.bitMatrix,0) = 1 AND
																				EXISTS (
																							SELECT 
																								t1.vcItemName, t1.bitMatrix, t1.numItemGroup, Count(t1.numItemCode)
																							FROM 
																								#TEMPItems t1
																							WHERE 
																								t1.vcItemName = F.vcItemName
																								AND t1.bitMatrix = 1
																								AND t1.numItemGroup = F.numItemGroup
																							GROUP BY 
																								t1.vcItemName, t1.bitMatrix, t1.numItemGroup
																							HAVING 
																								Count(t1.numItemCode) > 1
																						)
																		)
														AND numItemCode NOT IN (
																								SELECT 
																									Min(numItemCode)
																								FROM 
																									#TEMPItems AS F
																								WHERE
																									ISNULL(F.bitMatrix,0) = 1 AND
																									Exists (
																												SELECT 
																													t2.vcItemName, t2.bitMatrix, t2.numItemGroup, Count(t2.numItemCode)
																												FROM 
																													#TEMPItems t2
																												WHERE 
																													t2.vcItemName = F.vcItemName
																												   AND t2.bitMatrix = 1
																												   AND t2.numItemGroup = F.numItemGroup
																												GROUP BY 
																													t2.vcItemName, t2.bitMatrix, t2.numItemGroup
																												HAVING 
																													Count(t2.numItemCode) > 1
																											)
																								GROUP BY 
																									vcItemName, bitMatrix, numItemGroup
																							);'


						SET @strSQL = @strSQL + ' SELECT SRNO=ROW_NUMBER() OVER( ORDER BY vcItemName),(CASE WHEN vcSKU=''' + @str + ''' THEN 1 ELSE 0 END) bitSKUMatch,* INTO #TEMPItemsFinal FROM #TEMPItems;'


                        SET @strSQL = @strSQL + 'SELECT @TotalCount = COUNT(*) FROM #TEMPItemsFinal; select * from #TEMPItemsFinal where SRNO> '
                            + CAST(( ( @numPageIndex - 1 ) * @numPageSize ) AS VARCHAR(10))
                            + ' and SRNO < '
                            + CAST(( ( @numPageIndex * @numPageSize ) + 1 ) AS VARCHAR(10))
                            + ' order by  vcItemName; DROP TABLE #TEMPItemsFinal; DROP TABLE #TEMPItems;'
                        

						--PRINT @strSQL
						EXEC sp_executeSQL @strSQL, N'@TotalCount INT OUTPUT', @TotalCount OUTPUT
                    END      
                ELSE
                    IF @tintOppType = 2
                        BEGIN      
                            SET @strSQL = 'SELECT I.numItemCode,
										  (SELECT TOP 1 vcPathForImage from ItemImages WHERE numItemCode = I.numItemCode) as vcPathForImage,
										  (SELECT TOP 1 vcPathForTImage from ItemImages WHERE numItemCode = I.numItemCode) as vcPathForTImage,
										  ISNULL(vcItemName,'''') vcItemName,
										  convert(varchar(200),round(monListPrice,2)) as monListPrice,
										  isnull(vcSKU,'''') vcSKU,
										  isnull(numBarCodeId,0) numBarCodeId,
										  isnull(vcModelID,'''') vcModelID,
										  isnull(txtItemDesc,'''') as txtItemDesc,
										  isnull(C.vcCompanyName,'''') as vcCompanyName,
										  WHT.numWareHouseItemID,
										  dbo.fn_GetAttributes(WHT.numWareHouseItemId,I.bitSerialized) AS [vcAttributes],
										  (CASE 
												WHEN ISNULL(I.bitKitParent,0) = 1 
												THEN 
													(
														CASE
															WHEN (SELECT COUNT(*) FROM ItemDetails INNER JOIN Item ON ItemDetails.numChildItemID = Item.numItemCode WHERE numItemKitID=I.numItemCode AND ISNULL(Item.bitKitParent,0) = 1) > 0
															THEN 1
															ELSE 0
														END
													)
												ELSE 0 
											END) bitHasKitAsChild,ISNULL(I.bitMatrix,0) AS bitMatrix,ISNULL(I.numItemGroup,0) AS numItemGroup'
										  + @strSQL
										  + ' INTO #TEMPItems from item I  WITH (NOLOCK)
										  INNER JOIN
											dbo.Vendor V
										  ON
											V.numItemCode = I.numItemCode
										  Left join 
											DivisionMaster D              
										  ON 
											V.numVendorID=D.numDivisionID  
										  LEFT join 
											CompanyInfo C  
										  ON 
											C.numCompanyID=D.numCompanyID    
										  LEFT JOIN
											dbo.WareHouseItems WHT
										  ON
											WHT.numItemID = I.numItemCode AND
											WHT.numWareHouseItemID = (' +
																			CASE LEN(@vcWareHouseSearch)
																			WHEN 0 
																			THEN 
																				'SELECT 
																					TOP 1 numWareHouseItemID 
																				FROM 
																					dbo.WareHouseItems 
																				WHERE 
																					dbo.WareHouseItems.numDomainID = ' + CAST(@numDomainID AS VARCHAR(20)) + ' 
																					AND dbo.WareHouseItems.numItemID = I.numItemCode 
																					AND (dbo.WareHouseItems.numWareHouseID = ' +  CAST(ISNULL(@WarehouseID,0) AS VARCHAR(20))  + ' OR ' + CAST(ISNULL(@WarehouseID,0) AS VARCHAR(20)) + ' = 0)'
																			ELSE
																				'CASE 
																					WHEN (SELECT 
																								COUNT(*) 
																							FROM 
																								dbo.WareHouseItems 
																							WHERE 
																								dbo.WareHouseItems.numDomainID = ' + CAST(@numDomainID AS VARCHAR(20)) + ' 
																								AND dbo.WareHouseItems.numItemID = I.numItemCode 
																								AND (dbo.WareHouseItems.numWareHouseID = ' +  CAST(ISNULL(@WarehouseID,0) AS VARCHAR(20))  + ' OR ' + CAST(ISNULL(@WarehouseID,0) AS VARCHAR(20)) + ' = 0)' +
																								' AND (' + @vcWareHouseSearch + ')) > 0 
																					THEN
																						(SELECT 
																							TOP 1 numWareHouseItemID 
																						FROM 
																							dbo.WareHouseItems 
																						WHERE 
																							dbo.WareHouseItems.numDomainID = ' + CAST(@numDomainID AS VARCHAR(20)) + ' 
																							AND dbo.WareHouseItems.numItemID = I.numItemCode 
																							AND (dbo.WareHouseItems.numWareHouseID = ' +  CAST(ISNULL(@WarehouseID,0) AS VARCHAR(20))  + ' OR ' + CAST(ISNULL(@WarehouseID,0) AS VARCHAR(20)) + ' = 0) AND (' + @vcWareHouseSearch + '))
																					ELSE
																						(SELECT 
																							TOP 1 numWareHouseItemID 
																						FROM 
																							dbo.WareHouseItems 
																						WHERE 
																							dbo.WareHouseItems.numDomainID = ' + CAST(@numDomainID AS VARCHAR(20)) + ' 
																							AND dbo.WareHouseItems.numItemID = I.numItemCode 
																							AND (dbo.WareHouseItems.numWareHouseID = ' +  CAST(ISNULL(@WarehouseID,0) AS VARCHAR(20))  + ' OR ' + CAST(ISNULL(@WarehouseID,0) AS VARCHAR(20)) + ' = 0))
																					END' 
																			END
																		+
																	')
										       
										  WHERE (
													(I.charItemType <> ''P'') OR 
													((SELECT COUNT(*) FROM dbo.WareHouseItems WHERE numItemID = I.numItemCode AND numDomainID = '
										 + CONVERT(VARCHAR(20), @numDomainID)
										 + ' AND numWareHouseID =  ' +  CAST(ISNULL(@WarehouseID,0) AS VARCHAR(20))  + ' OR ' + CAST(ISNULL(@WarehouseID,0) AS VARCHAR(20)) + ' = 0) > 0)) AND ISNULL(I.Isarchieve,0) <> 1 and ISNULL(I.bitKitParent,0) = 0  AND ISNULL(I.bitAssembly,0) = 0 And I.charItemType NOT IN(''A'') AND V.numVendorID='
										 + CONVERT(VARCHAR(20), @numDivisionID)
										 + ' and ('+ CASE LEN(@vcVendorSearch)
																			WHEN 0 THEN ''
																			ELSE REPLACE(@vcVendorSearch,'dbo.Vendor','V') + ' OR '
																			END + @strSearch + ') ' 

							--- added Asset validation  by sojan
                            IF @tintSearchOrderCustomerHistory = 1
                                AND @numDivisionID > 0
                                BEGIN
                                    SET @strSQL = @strSQL
                                        + ' and I.numItemCode in (select distinct oppI.numItemCode from 
									OpportunityMaster oppM join OpportunityItems oppI on oppM.numOppId=oppI.numOppId where oppM.numDomainID='
                                        + CONVERT(VARCHAR(20), @numDomainID)
                                        + ' and oppM.numDivisionId='
                                        + CONVERT(VARCHAR(20), @numDivisionId)
                                        + ')'
                                END

							

							 SET @strSQL = @strSQL + 'DELETE FROM 
														#TEMPItems
													WHERE 
														numItemCode IN (
																			SELECT 
																				F.numItemCode
																			FROM 
																				#TEMPItems AS F
																			WHERE
																				ISNULL(F.bitMatrix,0) = 1 AND
																				EXISTS (
																							SELECT 
																								t1.vcItemName, t1.bitMatrix, t1.numItemGroup, Count(t1.numItemCode)
																							FROM 
																								#TEMPItems t1
																							WHERE 
																								t1.vcItemName = F.vcItemName
																								AND t1.bitMatrix = 1
																								AND t1.numItemGroup = F.numItemGroup
																							GROUP BY 
																								t1.vcItemName, t1.bitMatrix, t1.numItemGroup
																							HAVING 
																								Count(t1.numItemCode) > 1
																						)
																		)
														AND numItemCode NOT IN (
																								SELECT 
																									Min(numItemCode)
																								FROM 
																									#TEMPItems AS F
																								WHERE
																									ISNULL(F.bitMatrix,0) = 1 AND
																									Exists (
																												SELECT 
																													t2.vcItemName, t2.bitMatrix, t2.numItemGroup, Count(t2.numItemCode)
																												FROM 
																													#TEMPItems t2
																												WHERE 
																													t2.vcItemName = F.vcItemName
																												   AND t2.bitMatrix = 1
																												   AND t2.numItemGroup = F.numItemGroup
																												GROUP BY 
																													t2.vcItemName, t2.bitMatrix, t2.numItemGroup
																												HAVING 
																													Count(t2.numItemCode) > 1
																											)
																								GROUP BY 
																									vcItemName, bitMatrix, numItemGroup
																							);'


							SET @strSQL = @strSQL + ' SELECT SRNO=ROW_NUMBER() OVER( ORDER BY vcItemName),(CASE WHEN vcSKU=''' + @str + ''' THEN 1 ELSE 0 END) bitSKUMatch,* INTO #TEMPItemsFinal FROM #TEMPItems;'


							SET @strSQL = @strSQL + 'SELECT @TotalCount = COUNT(*) FROM #TEMPItemsFinal; select * from #TEMPItemsFinal where SRNO> '
                            + CAST(( ( @numPageIndex - 1 ) * @numPageSize ) AS VARCHAR(10))
                            + ' and SRNO < '
                            + CAST(( ( @numPageIndex * @numPageSize ) + 1 ) AS VARCHAR(10))
                            + ' order by  vcItemName; DROP TABLE #TEMPItemsFinal; DROP TABLE #TEMPItems;'
                        
							EXEC sp_executeSQL @strSQL, N'@TotalCount INT OUTPUT', @TotalCount OUTPUT
                        END
                    ELSE
                        SELECT  0  
            END
    END
ELSE
    SELECT  0  

SELECT  *
FROM    #Temp1
WHERE   vcDbColumnName NOT IN ( 'vcPartNo', 'vcBarCode', 'vcWHSKU' )ORDER BY tintOrder 
   


IF OBJECT_ID('dbo.Scores', 'U') IS NOT NULL
  DROP TABLE dbo.Scores

IF OBJECT_ID('tempdb..#Temp1') IS NOT NULL
BEGIN
    DROP TABLE #Temp1
END

IF OBJECT_ID('tempdb..#tempSearch') IS NOT NULL
BEGIN
    DROP TABLE #tempSearch
END

IF OBJECT_ID('tempdb..#tempItemCode') IS NOT NULL
BEGIN
    DROP TABLE #tempItemCode
END


    END
--Created By Anoop Jayaraj          
--exec USP_ItemDetailsForEcomm @numItemCode=859064,@numWareHouseID=1074,@numDomainID=172,@numSiteId=90
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_itemdetailsforecomm')
DROP PROCEDURE USP_ItemDetailsForEcomm
GO
CREATE PROCEDURE [dbo].[USP_ItemDetailsForEcomm]
@numItemCode as numeric(9),          
@numWareHouseID as numeric(9),    
@numDomainID as numeric(9),
@numSiteId as numeric(9),
@vcCookieId as VARCHAR(MAX)=null,
@numUserCntID AS NUMERIC(9)=0,
@numDivisionID AS NUMERIC(18,0) = 0                                        
as     
/*Removed warehouse parameter, and taking Default warehouse by default from DB*/
    
declare @bitShowInStock as bit        
declare @bitShowQuantity as BIT
declare @bitAutoSelectWarehouse as bit        
--declare @tintColumns as tinyint        
DECLARE @numDefaultWareHouseID as numeric(9)
DECLARE @numDefaultRelationship AS NUMERIC(18,0)
DECLARE @numDefaultProfile AS NUMERIC(18,0)
DECLARE @tintPreLoginPriceLevel AS TINYINT
DECLARE @tintPriceLevel AS TINYINT
set @bitShowInStock=0        
set @bitShowQuantity=0        
--set @tintColumns=1        
        
IF ISNULL(@numDivisionID,0) > 0
BEGIN
	SELECT @numDefaultRelationship=ISNULL(tintCRMType,0),@numDefaultProfile=ISNULL(vcProfile,0),@tintPriceLevel=ISNULL(tintPriceLevel,0) FROM DivisionMaster INNER JOIN CompanyInfo ON DivisionMaster.numCompanyID=CompanyInfo.numCompanyId WHERE numDivisionID=@numDivisionID AND DivisionMaster.numDomainID=@numDomainID
END     
        
SELECT 
	@bitShowInStock=ISNULL(bitShowInStock,0)
	,@bitShowQuantity=ISNULL(bitShowQOnHand,0)
	,@bitAutoSelectWarehouse=ISNULL(bitAutoSelectWarehouse,0)
	,@numDefaultRelationship=numRelationshipId
	,@numDefaultProfile=numProfileId
	,@tintPreLoginPriceLevel=(CASE WHEN @numDivisionID > 0 THEN (CASE WHEN ISNULL(bitShowPriceUsingPriceLevel,0)=1 THEN ISNULL(@tintPriceLevel,0) ELSE 0 END) ELSE ISNULL(tintPreLoginProceLevel,0) END)
FROM 
	eCommerceDTL 
WHERE 
	numDomainID=@numDomainID




IF @numWareHouseID=0
BEGIN
	SELECT @numDefaultWareHouseID=ISNULL(numDefaultWareHouseID,0) FROM [eCommerceDTL] WHERE [numDomainID]=@numDomainID	
	SET @numWareHouseID =@numDefaultWareHouseID;
END

/*If qty is not found in default warehouse then choose next warehouse with sufficient qty */
IF @bitAutoSelectWarehouse = 1 AND ( SELECT COUNT(*) FROM dbo.WareHouseItems WHERE  numItemID = @numItemCode AND numWarehouseID= @numWareHouseID ) = 0 
BEGIN
	SELECT TOP 1 @numDefaultWareHouseID = numWareHouseID FROM dbo.WareHouseItems WHERE  numItemID = @numItemCode AND numOnHand>0 /*TODO: Pass actuall qty from cart*/ ORDER BY numOnHand DESC 
	IF (ISNULL(@numDefaultWareHouseID,0)>0)
		SET @numWareHouseID =@numDefaultWareHouseID;
END

      DECLARE @UOMConversionFactor AS DECIMAL(18,5)
      
      Select @UOMConversionFactor= ISNULL(dbo.fn_UOMConversion(ISNULL(I.numSaleUnit,0),I.numItemCode,I.numDomainId,ISNULL(I.numBaseUnit,0)),0)
      FROM Item I where numItemCode=@numItemCode   
	   
	  DECLARE @PromotionOffers AS DECIMAL(18,2)=0     
	  IF(@vcCookieId!=null OR @vcCookieId!='')   
	  BEGIN                    
SET @PromotionOffers=(SELECT TOP 1 CASE WHEN P.tintDiscountType=1  
				THEN (I.monListPrice*fltDiscountValue)/100
				WHEN P.tintDiscountType=2
				THEN fltDiscountValue
				WHEN P.tintDiscountType=3
				THEN fltDiscountValue*I.monListPrice
				ELSE 0 END
FROM CartItems AS C
LEFT JOIN PromotionOffer AS P ON P.numProId=C.PromotionID
LEFT JOIN Item I ON I.numItemCode=C.numItemCode
WHERE I.numItemCode=@numItemCode AND C.vcCookieId=@vcCookieId AND C.numUserCntId=@numUserCntID AND C.fltDiscount=0 AND 
(I.numItemCode IN (SELECT PIS.numValue FROM PromotionOfferItems AS PIS WHERE PIS.numProId=P.numProId AND PIS.tintType=1 AND PIS.tintRecordType=6) OR
I.numItemClassification IN (SELECT PIS.numValue FROM PromotionOfferItems AS PIS WHERE PIS.numProId=P.numProId AND PIS.tintType=2 AND PIS.tintRecordType=6))
AND P.numDomainId=@numDomainID 
		AND ISNULL(bitEnabled,0)=1 
		AND ISNULL(bitAppliesToSite,0)=1 
		AND 1 = (CASE WHEN bitRequireCouponCode = 1 THEN CASE WHEN ISNULL(intCouponCodeUsed,0) < ISNULL(tintUsageLimit,0) OR ISNULL(tintUsageLimit,0) = 0  THEN 1 ELSE 0 END ELSE 1 END)
		AND 1 = (CASE WHEN bitNeverExpires = 1 THEN 1 ELSE (CASE WHEN CAST(GETDATE() AS DATE)>=dtValidFrom AND CAST(GETDATE() AS DATE)<=dtValidTo THEN 1 ELSE 0 END) END))
		END
		PRINT @PromotionOffers
 
declare @strSql as varchar(MAX)
set @strSql=' With tblItem AS (                  
select I.numItemCode, vcItemName, txtItemDesc, charItemType, ISNULL(I.bitMatrix,0) bitMatrix,' + 
(CASE WHEN @tintPreLoginPriceLevel > 0 THEN 'ISNULL(PricingOption.monPrice,ISNULL(CASE WHEN I.[charItemType]=''P'' 
			THEN '+ convert(varchar(15),@UOMConversionFactor) + ' * W.[monWListPrice]  
			ELSE '+ convert(varchar(15),@UOMConversionFactor) + ' * monListPrice 
	   END,0))' ELSE 'ISNULL(CASE WHEN I.[charItemType]=''P'' 
			THEN '+ convert(varchar(15),@UOMConversionFactor) + ' * W.[monWListPrice]  
			ELSE '+ convert(varchar(15),@UOMConversionFactor) + ' * monListPrice 
	   END,0)' END)
+'
AS monListPrice,
numItemClassification, bitTaxable, vcSKU, bitKitParent,              
I.numModifiedBy, (SELECT numItemImageId,vcPathForImage,vcPathForTImage,bitDefault,case when bitdefault=1  then -1 else isnull(intDisplayOrder,0) end as intDisplayOrder FROM ItemImages  WHERE numItemCode=I.numItemCode and bitIsImage = 1 order by case when bitdefault=1 then -1 else isnull(intDisplayOrder,0) end  asc FOR XML AUTO,ROOT(''Images''))  vcImages,
(SELECT vcPathForImage FROM ItemImages  WHERE numItemCode=I.numItemCode and bitIsImage = 0 order by intDisplayOrder  asc FOR XML AUTO,ROOT(''Documents''))  vcDocuments ,
 isnull(bitSerialized,0) as bitSerialized, vcModelID,                 
numItemGroup,                   
(isnull(sum(numOnHand),0) / '+ convert(varchar(15),@UOMConversionFactor) + ') as numOnHand,                    
sum(numOnOrder) as numOnOrder,                    
sum(numReorder)  as numReorder,                    
sum(numAllocation)  as numAllocation,                    
sum(numBackOrder)  as numBackOrder,              
isnull(fltWeight,0) as fltWeight,              
isnull(fltHeight,0) as fltHeight,              
isnull(fltWidth,0) as fltWidth,              
isnull(fltLength,0) as fltLength,              
case when charItemType=''P''then isnull(bitFreeShipping,0) else 1 end as bitFreeShipping,
Case When (case when charItemType=''P''then isnull(bitFreeShipping,0) else 1 end )=1 then ''Free shipping'' else ''Calculated at checkout'' end as Shipping,              
isnull(bitAllowBackOrder,0) as bitAllowBackOrder,bitShowInStock,bitShowQOnHand,isnull(numWareHouseItemID,0)  as numWareHouseItemID,    
(case when charItemType<>''S'' and charItemType<>''N'' then         
 (Case when '+ convert(varchar(15),@bitShowInStock) + '=1  then ( Case when numWareHouseItemID is null then ''<font color=red>Out Of Stock</font>'' when bitAllowBackOrder=1 then ''In Stock'' else         
 (Case when isnull(sum(numOnHand),0)>0 then ''In Stock''         
 else ''<font color=red>Out Of Stock</font>'' end ) end)    else '''' end) else '''' end) as InStock,
ISNULL(numSaleUnit,0) AS numUOM,
ISNULL(vcUnitName,'''') AS vcUOMName,
 '+ convert(varchar(15),@UOMConversionFactor) + ' AS UOMConversionFactor,
I.numCreatedBy,
I.numBarCodeId,
I.[vcManufacturer],
(SELECT  COUNT(*) FROM  Review where vcReferenceId = CONVERT(VARCHAR(50) ,I.numItemCode) and numDomainId = I.numDomainId and numSiteId = '+ CONVERT(VARCHAR(20) , @numSiteId)  +' AND bitHide = 0 ) as ReviewCount ,
(SELECT  COUNT(*) FROM  Ratings where vcReferenceId = CONVERT(VARCHAR(50) ,I.numItemCode) and numDomainId = I.numDomainId and numSiteId = '+ CONVERT(VARCHAR(20) , @numSiteId)  +'  ) as RatingCount ,
(select top 1 vcMetaKeywords  from MetaTags where numReferenceID = I.numItemCode and tintMetaTagFor =2) as vcMetaKeywords,
(select top 1 vcMetaDescription  from MetaTags where numReferenceID = I.numItemCode and tintMetaTagFor =2) as vcMetaDescription ,
(SELECT vcCategoryName  FROM  dbo.Category WHERE numCategoryID =(SELECT TOP 1 numCategoryID FROM dbo.ItemCategory WHERE numItemID = I.numItemCode)) as vcCategoryName,('+(SELECT CAST(ISNULL(@PromotionOffers,0) AS varchar(20)))+') as PromotionOffers

from Item I ' + 
(CASE WHEN @tintPreLoginPriceLevel > 0 THEN ' OUTER APPLY(SELECT decDiscount AS monPrice FROM PricingTable WHERE tintRuleType=3 AND numItemCode=I.numItemCode ORDER BY numPricingID OFFSET ' + CAST(@tintPreLoginPriceLevel-1 AS VARCHAR) + ' ROWS FETCH NEXT 1 ROWS ONLY) As PricingOption ' ELSE '' END)
+ '                 
left join  WareHouseItems W                
on W.numItemID=I.numItemCode and numWareHouseID= '+ convert(varchar(15),@numWareHouseID) + '
left join  eCommerceDTL E          
on E.numDomainID=I.numDomainID     
LEFT JOIN UOM u ON u.numUOMId=I.numSaleUnit    
where I.numItemCode='+ convert(varchar(15),@numItemCode) +
CASE WHEN (SELECT COUNT(*) FROM [dbo].[EcommerceRelationshipProfile] WHERE numSiteID=@numSiteID AND numRelationship = @numDefaultRelationship AND numProfile = @numDefaultProfile) > 0
THEN
	CONCAT(' AND I.numItemClassification NOT IN (SELECT numItemClassification FROM EcommerceRelationshipProfile ERP INNER JOIN ECommerceItemClassification EIC ON EIC.numECommRelatiionshipProfileID=ERP.ID WHERE ERP.numSiteID=',@numSiteID,' AND ERP.numRelationship=',@numDefaultRelationship,' AND ERP.numProfile=', @numDefaultProfile,')')
ELSE
	''
END
+ '           
group by I.numItemCode, vcItemName, txtItemDesc, charItemType, monListPrice,numItemClassification, bitTaxable, vcSKU, bitKitParent,           
I.numDomainID,I.numModifiedBy,  bitSerialized, vcModelID,numItemGroup, fltWeight,fltHeight,fltWidth,          
fltLength,bitFreeShipping,bitAllowBackOrder,bitShowInStock,bitShowQOnHand ,numWareHouseItemID,vcUnitName,I.numCreatedBy,I.numBarCodeId,W.[monWListPrice],I.[vcManufacturer],
numSaleUnit, I.bitMatrix ' + (CASE WHEN @tintPreLoginPriceLevel > 0 THEN ',PricingOption.monPrice' ELSE '' END) + ')'

set @strSql=@strSql+' select numItemCode,vcItemName,txtItemDesc,charItemType, bitMatrix, monListPrice,numItemClassification, bitTaxable, vcSKU, bitKitParent,                  
tblItem.numModifiedBy, vcImages,vcDocuments, bitSerialized, vcModelID, numItemGroup,numOnHand,numOnOrder,numReorder,numAllocation, 
numBackOrder, fltWeight, fltHeight, fltWidth, fltLength, bitFreeShipping,bitAllowBackOrder,bitShowInStock,
bitShowQOnHand,numWareHouseItemID,InStock,numUOM,vcUOMName,UOMConversionFactor,tblItem.numCreatedBy,numBarCodeId,vcManufacturer,ReviewCount,RatingCount,Shipping,isNull(vcMetaKeywords,'''') as vcMetaKeywords ,isNull(vcMetaDescription,'''') as vcMetaDescription,isNull(vcCategoryName,'''') as vcCategoryName,PromotionOffers'     

set @strSql=@strSql+ ' from tblItem'
PRINT @strSql
exec (@strSql)


declare @tintOrder as tinyint                                                  
declare @vcFormFieldName as varchar(50)                                                  
declare @vcListItemType as varchar(1)                                             
declare @vcAssociatedControlType varchar(10)                                                  
declare @numListID AS numeric(9)                                                  
declare @WhereCondition varchar(2000)                       
Declare @numFormFieldId as numeric  
DECLARE @vcFieldType CHAR(1)
                  
set @tintOrder=0                                                  
set @WhereCondition =''                 
                   
              
  CREATE TABLE #tempAvailableFields(numFormFieldId  numeric(9),vcFormFieldName NVARCHAR(50),
        vcFieldType CHAR(1),vcAssociatedControlType NVARCHAR(50),numListID NUMERIC(18),
        vcListItemType CHAR(1),intRowNum INT,vcItemValue VARCHAR(3000))
   
  INSERT INTO #tempAvailableFields (numFormFieldId,vcFormFieldName,vcFieldType,vcAssociatedControlType
        ,numListID,vcListItemType,intRowNum)                         
            SELECT  Fld_id,REPLACE(Fld_label,' ','') AS vcFormFieldName,
                   CASE GRP_ID WHEN 4 then 'C' WHEN 1 THEN 'D' ELSE 'C' END AS vcFieldType,
                    fld_type as vcAssociatedControlType,
                    ISNULL(C.numListID, 0) numListID,
                    CASE WHEN C.numListID > 0 THEN 'L'
                         ELSE ''
                    END vcListItemType,ROW_NUMBER() OVER (ORDER BY Fld_id asc) AS intRowNum
            FROM    CFW_Fld_Master C LEFT JOIN CFW_Validation V ON V.numFieldID = C.Fld_id
            WHERE   C.numDomainID = @numDomainId
                    AND GRP_ID IN (5)

     select top 1 @tintOrder=intRowNum,@numFormFieldId=numFormFieldId,@vcFormFieldName=vcFormFieldName,
				  @vcFieldType=vcFieldType,@vcAssociatedControlType=vcAssociatedControlType,@numListID=numListID,
				  @vcListItemType=vcListItemType
	from #tempAvailableFields order by intRowNum ASC
   
while @tintOrder>0                                                  
begin                                                  
    if @vcAssociatedControlType = 'TextBox' or @vcAssociatedControlType = 'TextArea'
    begin  
		UPDATE #tempAvailableFields SET vcItemValue=(SELECT Fld_Value FROM CFW_FLD_Values_Item WHERE Fld_Id=@numFormFieldId AND RecId=@numItemCode) WHERE numFormFieldId=@numFormFieldId              
    end   
    else if @vcAssociatedControlType = 'CheckBox'
	begin      
		UPDATE #tempAvailableFields SET vcItemValue=(SELECT case when isnull(Fld_Value,0)=0 then 'No' when isnull(Fld_Value,0)=1 then 'Yes' END FROM CFW_FLD_Values_Item WHERE Fld_Id=@numFormFieldId AND RecId=@numItemCode) WHERE numFormFieldId=@numFormFieldId              
   end                
   else if @vcAssociatedControlType = 'DateField'           
   begin   
		UPDATE #tempAvailableFields SET vcItemValue=(SELECT dbo.FormatedDateFromDate(Fld_Value,@numDomainId) FROM CFW_FLD_Values_Item WHERE Fld_Id=@numFormFieldId AND RecId=@numItemCode) WHERE numFormFieldId=@numFormFieldId              
   end                
    else if @vcAssociatedControlType = 'SelectBox'           
   begin 
		UPDATE #tempAvailableFields SET vcItemValue=(SELECT L.vcData FROM CFW_FLD_Values_Item CFW left Join ListDetails L
			on L.numListItemID=CFW.Fld_Value                
			WHERE CFW.Fld_Id=@numFormFieldId AND CFW.RecId=@numItemCode) WHERE numFormFieldId=@numFormFieldId              
end          
               
 
    select top 1 @tintOrder=intRowNum,@numFormFieldId=numFormFieldId,@vcFormFieldName=vcFormFieldName,
				  @vcFieldType=vcFieldType,@vcAssociatedControlType=vcAssociatedControlType,@numListID=numListID,
				  @vcListItemType=vcListItemType
	from #tempAvailableFields WHERE intRowNum > @tintOrder order by intRowNum ASC
 
   if @@rowcount=0 set @tintOrder=0                                                  
end   


  
SELECT * FROM #tempAvailableFields

DROP TABLE #tempAvailableFields

--exec USP_ItemDetailsForEcomm @numItemCode=197611,@numWareHouseID=58,@numDomainID=1,@numSiteId=18
--exec USP_ItemDetailsForEcomm @numItemCode=735364,@numWareHouseID=1039,@numDomainID=156,@numSiteId=104
--SELECT * FROM Category
--- Alterd By Chintan Prajapati
-- EXEC dbo.USP_ItemsForECommerce 9,60,1,1,15,0,''
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_ItemsForECommerce' ) 
    DROP PROCEDURE USP_ItemsForECommerce
GO
CREATE PROCEDURE [dbo].[USP_ItemsForECommerce]
    @numSiteID AS NUMERIC(9) = 0,
    @numCategoryID AS NUMERIC(18) = 0,
    @numWareHouseID AS NUMERIC(9) = 0,
    @SortBy NVARCHAR(MAX) = 0,
    @CurrentPage INT,
    @PageSize INT,
    @TotRecs INT = 0 OUTPUT,
    @SearchText AS NVARCHAR(MAX) = '',--Comma seperated item ids
    @FilterQuery AS NVARCHAR(MAX) = '',----Comma seperated Attributes Ids
    @FilterRegularCondition AS NVARCHAR(MAX),
    @FilterCustomFields AS NVARCHAR(MAX),
    @FilterCustomWhere AS NVARCHAR(MAX),
	@numDivisionID AS NUMERIC(18,0) = 0,
	@numManufacturerID AS NUMERIC(18,0)=0
   
AS 
    BEGIN
		
		DECLARE @numDomainID AS NUMERIC(9)
		DECLARE @numDefaultRelationship AS NUMERIC(18,0)
		DECLARE @numDefaultProfile AS NUMERIC(18,0)
		DECLARE @tintPreLoginPriceLevel AS TINYINT
		DECLARE @tintPriceLevel AS TINYINT

		SELECT  @numDomainID = numDomainID
        FROM    [Sites]
        WHERE   numSiteID = @numSiteID
        
        DECLARE @tintDisplayCategory AS TINYINT
		
		IF ISNULL(@numDivisionID,0) > 0
		BEGIN
			SELECT @numDefaultRelationship=ISNULL(tintCRMType,0),@numDefaultProfile=ISNULL(vcProfile,0),@tintPriceLevel=ISNULL(tintPriceLevel,0) FROM DivisionMaster INNER JOIN CompanyInfo ON DivisionMaster.numCompanyID=CompanyInfo.numCompanyId WHERE numDivisionID=@numDivisionID AND DivisionMaster.numDomainID=@numDomainID
		END

		SELECT 
			@tintDisplayCategory = ISNULL(bitDisplayCategory,0)
			,@numDefaultRelationship=numRelationshipId
			,@numDefaultProfile=numProfileId
			,@tintPreLoginPriceLevel=(CASE WHEN @numDivisionID > 0 THEN (CASE WHEN ISNULL(bitShowPriceUsingPriceLevel,0)=1 THEN ISNULL(@tintPriceLevel,0) ELSE 0 END) ELSE ISNULL(tintPreLoginProceLevel,0) END)
		FROM 
			dbo.eCommerceDTL 
		WHERE 
			numSiteID = @numSiteId
		
		CREATE TABLE #tmpItemCat(numCategoryID NUMERIC(18,0))
		CREATE TABLE #tmpItemCode(numItemCode NUMERIC(18,0))
		IF LEN(@FilterCustomFields) > 0 AND LEN(@FilterCustomWhere) > 0
		BEGIN			
			DECLARE @FilterCustomFieldIds AS NVARCHAR(MAX)
			SET @FilterCustomFieldIds = Replace(@FilterCustomFields,'[','')
			SET @FilterCustomFieldIds = Replace(@FilterCustomFieldIds,']','')
			
			DECLARE @strCustomSql AS NVARCHAR(MAX)
			SET @strCustomSql = 'INSERT INTO #tmpItemCode(numItemCode)
								 SELECT DISTINCT T.RecId FROM 
															(
																SELECT * FROM 
																	(
																		SELECT  t2.RecId, t1.fld_id, t2.Fld_Value FROM CFW_Fld_Master t1 
																		JOIN CFW_FLD_Values_Item AS t2 ON t1.Fld_ID = t2.Fld_ID 
																		WHERE t1.Grp_id = 5 
																		AND t1.numDomainID = ' + CONVERT(VARCHAR(10),@numDomainID) + '
																		AND t1.fld_type <> ''Link'' 
																		AND t1.fld_id IN (' + @FilterCustomFieldIds + ')
																	) p  PIVOT (MAX([Fld_Value]) FOR fld_id IN (' + @FilterCustomFields + ')) AS pvt 
															) T  WHERE 1=1 AND ' + @FilterCustomWhere
								 
			PRINT @strCustomSql
			EXEC SP_EXECUTESQL @strCustomSql								 					 
		END
		
        DECLARE @strSQL NVARCHAR(MAX)
        DECLARE @firstRec AS INTEGER
        DECLARE @lastRec AS INTEGER
        DECLARE @Where NVARCHAR(MAX)
        DECLARE @SortString NVARCHAR(MAX)
        DECLARE @searchPositionColumn NVARCHAR(MAX)
        DECLARE @row AS INTEGER
        DECLARE @data AS VARCHAR(100)
        DECLARE @dynamicFilterQuery NVARCHAR(MAX)
        DECLARE @checkFldType VARCHAR(100)
        DECLARE @count NUMERIC(9,0)
        DECLARE @whereNumItemCode NVARCHAR(MAX)
        DECLARE @filterByNumItemCode VARCHAR(100)
        
		SET @SortString = ''
        set @searchPositionColumn = ''
        SET @strSQL = ''
		SET @whereNumItemCode = ''
		SET @dynamicFilterQuery = ''
		 
        SET @firstRec = ( @CurrentPage - 1 ) * @PageSize
        SET @lastRec = ( @CurrentPage * @PageSize + 1 )
                
        DECLARE @bitAutoSelectWarehouse AS BIT
		SELECT @bitAutoSelectWarehouse = ISNULL([ECD].[bitAutoSelectWarehouse],0) FROM [dbo].[eCommerceDTL] AS ECD WHERE [ECD].[numSiteId] = @numSiteID
		AND [ECD].[numDomainId] = @numDomainID

		DECLARE @vcWarehouseIDs AS VARCHAR(1000)
        IF (@numWareHouseID = 0 OR @bitAutoSelectWarehouse = 1)
            BEGIN				
				IF @bitAutoSelectWarehouse = 1
					BEGIN
						SELECT @vcWarehouseIDs = ISNULL( STUFF((SELECT DISTINCT ',' + CAST(numWarehouseID AS VARCHAR(10)) FROM [dbo].[Warehouses] WHERE [Warehouses].[numDomainID] = @numDomainID FOR XML PATH('')),1, 1, '') , '') 
					END
				
				ELSE
					BEGIN
						SELECT  @numWareHouseID = ISNULL(numDefaultWareHouseID,0)
						FROM    [eCommerceDTL]
						WHERE   [numDomainID] = @numDomainID	

						SET @vcWarehouseIDs = @numWareHouseID
					END
            END
		ELSE
		BEGIN
			SET @vcWarehouseIDs = @numWareHouseID
		END

		PRINT @vcWarehouseIDs

		--PRIAMRY SORTING USING CART DROPDOWN
		DECLARE @Join AS NVARCHAR(MAX)
		DECLARE @SortFields AS NVARCHAR(MAX)
		DECLARE @vcSort AS VARCHAR(MAX)

		SET @Join = ''
		SET @SortFields = ''
		SET @vcSort = ''

		IF @SortBy = ''
			BEGIN
				SET @SortString = ' vcItemName Asc '
			END
		ELSE IF CHARINDEX('~0',@SortBy) > 0 OR CHARINDEX('~1',@SortBy) > 0
			BEGIN
				DECLARE @fldID AS NUMERIC(18,0)
				DECLARE @RowNumber AS VARCHAR(MAX)
				
				SELECT * INTO #fldValues FROM dbo.SplitIDsWithPosition(@SortBy,'~')
				SELECT @RowNumber = RowNumber, @fldID = id FROM #fldValues WHERE RowNumber = 1
				SELECT @vcSort = CASE WHEN Id = 0 THEN 'ASC' ELSE 'DESC' END FROM #fldValues WHERE RowNumber = 2
				
				SET @Join = @Join + ' LEFT JOIN CFW_FLD_Values_Item CFVI' + @RowNumber + ' ON I.numItemCode = CFVI' + @RowNumber + '.RecID AND CFVI' + @RowNumber + '.fld_Id = ' + CONVERT(VARCHAR(10),@fldID)
				---SET @SortString = 'CFVI' + @RowNumber + '.fld_Value' 
				SET @SortString = '[fld_Value' + @RowNumber + '] ' --+ @vcSort
				SET @SortFields = ', CFVI' + @RowNumber + '.fld_Value [fld_Value' + @RowNumber + ']'
			END
		ELSE
			BEGIN
				--SET @SortString = @SortBy	
				
				IF CHARINDEX('monListPrice',@SortBy) > 0
				BEGIN
					DECLARE @bitSortPriceMode AS BIT
					SELECT @bitSortPriceMode = ISNULL(bitSortPriceMode,0) FROM dbo.eCommerceDTL WHERE numSiteId = @numSiteID
					PRINT @bitSortPriceMode
					IF @bitSortPriceMode = 1
					BEGIN
						SET @SortString = ' '
					END
					ELSE
					BEGIN
						SET @SortString = @SortBy	
					END
				END
				ELSE
				BEGIN
					SET @SortString = @SortBy	
				END
				
			END	

		--SECONDARY SORTING USING DEFAULT SETTINGS FROM ECOMMERCE
		SELECT ROW_NUMBER() OVER (ORDER BY Custom,vcFieldName) AS [RowID],* INTO #tempSort FROM (
		SELECT  CONVERT(VARCHAR(9),numFieldID)+'~0' AS numFieldID,
				ISNULL(vcCultureFieldName,vcFieldName) AS vcFieldName,
				0 AS Custom,
				vcDbColumnName 
		FROM View_DynamicColumns
		WHERE numFormID = 84 
		AND ISNULL(bitSettingField,0) = 1 
		AND ISNULL(bitDeleted,0)=0 
		AND ISNULL(bitCustom,0) = 0
		AND numDomainID = @numDomainID
					       
		UNION     

		SELECT  CONVERT(VARCHAR(9),numFieldID)+'~1' AS numFieldID ,
				vcFieldName ,
				1 AS Custom,
				'' AS vcDbColumnName
		FROM View_DynamicCustomColumns          
		WHERE numFormID = 84
		AND numDomainID = @numDomainID
		AND grp_id = 5 
		AND vcAssociatedControlType = 'TextBox' 
		AND ISNULL(bitCustom,0) = 1 
		AND tintPageType=1  
		) TABLE1 ORDER BY Custom,vcFieldName
		
		--SELECT * FROM #tempSort
		DECLARE @DefaultSort AS NVARCHAR(MAX)
		DECLARE @DefaultSortFields AS NVARCHAR(MAX)
		DECLARE @DefaultSortJoin AS NVARCHAR(MAX)
		DECLARE @intCnt AS INT
		DECLARE @intTotalCnt AS INT	
		DECLARE @strColumnName AS VARCHAR(100)
		DECLARE @bitCustom AS BIT
		DECLARE @FieldID AS VARCHAR(100)				

		SET @DefaultSort = ''
		SET @DefaultSortFields = ''
		SET @DefaultSortJoin = ''		
		SET @intCnt = 0
		SET @intTotalCnt = ( SELECT COUNT(*) FROM #tempSort )
		SET @strColumnName = ''
		
		IF (SELECT ISNULL(bitEnableSecSorting,0) FROM EcommerceDtl WHERE numSiteID = @numSiteID ) = 1
		BEGIN
			CREATE TABLE #fldDefaultValues(RowNumber INT,Id VARCHAR(1000))

			WHILE(@intCnt < @intTotalCnt)
			BEGIN
				SET @intCnt = @intCnt + 1
				SELECT @FieldID = numFieldID, @strColumnName = vcDbColumnName, @bitCustom = Custom FROM #tempSort WHERE RowID = @intCnt
				
				--PRINT @FieldID
				--PRINT @strColumnName
				--PRINT @bitCustom

				IF @bitCustom = 0
					BEGIN
						SET @DefaultSort =  @strColumnName + ',' + @DefaultSort 
					END

				ELSE
					BEGIN
						DECLARE @fldID1 AS NUMERIC(18,0)
						DECLARE @RowNumber1 AS VARCHAR(MAX)
						DECLARE @vcSort1 AS VARCHAR(10)
						DECLARE @vcSortBy AS VARCHAR(1000)
						SET @vcSortBy = CONVERT(VARCHAR(100),@FieldID) + '~' + '0'
						--PRINT @vcSortBy

						INSERT INTO #fldDefaultValues(RowNumber,Id) SELECT RowNumber, id FROM dbo.SplitIDsWithPosition(@vcSortBy,'~')
						SELECT @RowNumber1 = RowNumber + 100 + @intCnt, @fldID1 = id FROM #fldDefaultValues WHERE RowNumber = 1
						SELECT @vcSort1 = 'ASC' 
						
						SET @DefaultSortJoin = @DefaultSortJoin + ' LEFT JOIN CFW_FLD_Values_Item CFVI' + @RowNumber1 + ' ON I.numItemCode = CFVI' + @RowNumber1 + '.RecID AND CFVI' + @RowNumber1 + '.fld_Id = ' + CONVERT(VARCHAR(10),@fldID1)
						SET @DefaultSort = '[fld_Value' + @RowNumber1 + '] ' + ',' + @DefaultSort 
						SET @DefaultSortFields = @DefaultSortFields + ', CFVI' + @RowNumber1 + '.fld_Value [fld_Value' + @RowNumber1 + ']'
					END

			END
			IF LEN(@DefaultSort) > 0
			BEGIN
				SET  @DefaultSort = LEFT(@DefaultSort,LEN(@DefaultSort) - 1)
				SET @SortString = @SortString + ',' 
			END
			--PRINT @SortString
			--PRINT @DefaultSort
			--PRINT @DefaultSortFields
		END
--        IF @SortBy = 1 
--            SET @SortString = ' vcItemName Asc '
--        ELSE IF @SortBy = 2 
--            SET @SortString = ' vcItemName Desc '
--        ELSE IF @SortBy = 3 
--            SET @SortString = ' monListPrice Asc '
--        ELSE IF @SortBy = 4 
--            SET @SortString = ' monListPrice Desc '
--        ELSE IF @SortBy = 5 
--            SET @SortString = ' bintCreatedDate Desc '
--        ELSE IF @SortBy = 6 
--            SET @SortString = ' bintCreatedDate ASC '	
--        ELSE IF @SortBy = 7 --sort by relevance, invokes only when search
--            BEGIN
--                 IF LEN(@SearchText) > 0 
--		            BEGIN
--						SET @SortString = ' SearchOrder ASC ';
--					    set @searchPositionColumn = ' ,SearchItems.RowNumber AS SearchOrder ' 						
--					END
--			END
--        ELSE 
--            SET @SortString = ' vcItemName Asc '
		
                                		
        IF LEN(@SearchText) > 0
			BEGIN
				PRINT 1
				IF CHARINDEX('SearchOrder',@SortString) > 0
				BEGIN
					SET @searchPositionColumn = ' ,SearchItems.RowNumber AS SearchOrder ' 
				END

				SET @Where = ' INNER JOIN dbo.SplitIDsWithPosition('''+ @SearchText +''','','') AS SearchItems 
                        ON SearchItems.Id = I.numItemCode  and SC.numSiteID = ' + CONVERT(VARCHAR(15), @numSiteID) + ' WHERE ISNULL(IsArchieve,0) = 0'
				END

		ELSE IF @SearchText = ''

			SET @Where = ' WHERE 1=2 '
			+ ' and SC.numSiteID = ' + CONVERT(VARCHAR(15), @numSiteID)
			+ ' AND ISNULL(IsArchieve,0) = 0'
		ELSE
			BEGIN
				PRINT 2
				SET @Where = ' WHERE 1=1 '
							+ ' and SC.numSiteID = ' + CONVERT(VARCHAR(15), @numSiteID)
							+ ' AND ISNULL(IsArchieve,0) = 0'


				PRINT @tintDisplayCategory							
				PRINT @Where

				IF @tintDisplayCategory = 1
				BEGIN
					
					;WITH CTE AS(
					SELECT C.numCategoryID,C.tintLevel,C.numDepCategory,CASE WHEN tintLevel=1 THEN 1 ELSE 0 END AS bitChild 
						FROM Category C WHERE (numCategoryID = @numCategoryID OR ISNULL(@numCategoryID,0) = 0) AND numDomainID = @numDomainID
					UNION ALL
					SELECT  C.numCategoryID,C.tintLevel,C.numDepCategory,CTE.bitChild FROM Category C JOIN CTE ON 
					C.numDepCategory=CTE.numCategoryID AND CTE.bitChild=1 AND numDomainID = @numDomainID)
					
					INSERT INTO #tmpItemCat(numCategoryID)												 
					SELECT DISTINCT C.numCategoryID FROM CTE C JOIN dbo.ItemCategory IC ON C.numCategoryID = IC.numCategoryID 
														WHERE numItemID IN ( 
																						
													 						SELECT numItemID FROM WareHouseItems WI
																			INNER JOIN dbo.Item I ON I.numItemCode = WI.numItemID AND I.numDomainID = @numDomainID
																			GROUP BY numItemID 
																			HAVING SUM(WI.numOnHand) > 0 
																			UNION ALL
													 						SELECT numItemID FROM WareHouseItems WI
																			INNER JOIN dbo.Item I ON I.numItemCode = WI.numItemID AND I.numDomainID = @numDomainID
																			GROUP BY numItemID 
																			HAVING SUM(WI.numAllocation) > 0
																		 )		
					SELECT numItemID INTO #tmpOnHandItems FROM 
					(													 					
						SELECT numItemID FROM WareHouseItems WI 
						INNER JOIN dbo.Item I ON I.numItemCode = WI.numItemID AND I.numDomainID = @numDomainID
						GROUP BY numItemID 
						HAVING SUM(WI.numOnHand) > 0
						UNION ALL
						SELECT numItemID FROM WareHouseItems WI 
						INNER JOIN dbo.Item I ON I.numItemCode = WI.numItemID AND I.numDomainID = @numDomainID
						GROUP BY numItemID 
						HAVING SUM(WI.numAllocation) > 0				
					) TABLE1
				END

			END

		       SET @strSQL = @strSQL + ' WITH Items AS 
	                                     ( 
	                                     SELECT   
                                               I.numItemCode,
											   I.bitMatrix,
											   I.numItemGroup,
                                               ROW_NUMBER() OVER ( Partition by I.numItemCode Order by I.numItemCode) RowID,
                                               vcItemName,
                                               I.bintCreatedDate,
                                               vcManufacturer,
                                               C.vcCategoryName,
                                               ISNULL(txtItemDesc,'''') txtItemDesc,' + 
											   (CASE WHEN @tintPreLoginPriceLevel > 0 THEN 'ISNULL(PricingOption.monPrice,ISNULL(CASE WHEN I.[charItemType] = ''P'' THEN ( UOM * ISNULL(W1.[monWListPrice],0)) ELSE (UOM * monListPrice) END, 0))' ELSE 'ISNULL(CASE WHEN I.[charItemType] = ''P'' THEN ( UOM * ISNULL(W1.[monWListPrice],0)) ELSE (UOM * monListPrice) END, 0)' END)
											   +' AS monListPrice,
                                               vcSKU,
                                               fltHeight,
                                               fltWidth,
                                               fltLength,
                                               IM.vcPathForImage,
                                               IM.vcPathForTImage,
                                               vcModelID,
                                               fltWeight,
                                               bitFreeShipping,
                                               UOM AS UOMConversionFactor,
											   UOMPurchase AS UOMPurchaseConversionFactor,
												( CASE WHEN charItemType <> ''S'' AND charItemType <> ''N''
													   THEN ( CASE WHEN bitShowInStock = 1
																   THEN ( CASE WHEN ( ISNULL(W.numOnHand,0)<=0) THEN CASE WHEN ' + CONVERT(VARCHAR(10),@numDomainID) + ' = 172 THEN ''<font color=red>ON HOLD</font>'' ELSE ''<font color=red>Out Of Stock</font>'' END
																			   WHEN bitAllowBackOrder = 1 THEN CASE WHEN ' + CONVERT(VARCHAR(10),@numDomainID) + ' = 172 THEN ''<font color=green>AVAILABLE</font>'' ELSE ''<font color=green>In Stock</font>'' END 
																			   ELSE CASE WHEN ' + CONVERT(VARCHAR(10),@numDomainID) + ' = 172 THEN ''<font color=green>AVAILABLE</font>'' ELSE ''<font color=green>In Stock</font>'' END  
																		  END )
																   ELSE ''''
															  END )
													   ELSE ''''
												  END ) AS InStock, C.vcDescription as CategoryDesc, ISNULL(W1.numWareHouseItemID,0) [numWareHouseItemID] ' 
								               +	@searchPositionColumn + @SortFields + @DefaultSortFields + ',
											   (SELECT COUNT(numProId) 
													FROM 
														PromotionOffer PO
													WHERE 
														numDomainId='+CAST(@numDomainID AS VARCHAR(20))+' 
														AND ISNULL(bitEnabled,0)=1
														AND ISNULL(bitAppliesToSite,0)=1 
														AND ISNULL(bitRequireCouponCode,0)=0
														AND 1 = (CASE WHEN bitRequireCouponCode = 1 THEN CASE WHEN ISNULL(intCouponCodeUsed,0) < ISNULL(tintUsageLimit,0) OR ISNULL(tintUsageLimit,0) = 0  THEN 1 ELSE 0 END ELSE 1 END)
														AND 1 = (CASE WHEN bitNeverExpires = 1 THEN 1 ELSE (CASE WHEN CAST(GETDATE() AS DATE)>=dtValidFrom AND CAST(GETDATE() AS DATE)<=dtValidTo THEN 1 ELSE 0 END) END)
														AND (1 = (CASE 
																	WHEN tintOfferBasedOn = 1 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId=PO.numProId AND tintRecordType=5 AND tintType=1 AND numValue IN (SELECT numOppAccAttrID FROM ItemGroupsDTL WHERE numItemGroupID IN (SELECT numItemGroupID FROM ItemGroups WHERE numProfileItem=I.numItemCode AND numDomainID='+CAST(@numDomainID AS VARCHAR(20))+' ) UNION SELECT I.numItemCode)) > 0 THEN 1 ELSE 0 END)
																	WHEN tintOfferBasedOn = 2 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId=PO.numProId AND tintRecordType=5 AND tintType=2 AND numValue=I.numItemClassification) > 0 THEN 1 ELSE 0 END)
																	ELSE 0
																END)
															OR
															1 = (CASE 
																	WHEN tintDiscoutBaseOn = 1 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId=PO.numProId AND tintRecordType=6 AND tintType=1 AND numValue IN (SELECT numOppAccAttrID FROM ItemGroupsDTL WHERE numItemGroupID IN (SELECT numItemGroupID FROM ItemGroups WHERE numProfileItem=I.numItemCode AND numDomainID='+CAST(@numDomainID AS VARCHAR(20))+' ) UNION SELECT I.numItemCode)) > 0 THEN 1 ELSE 0 END)
																	WHEN tintDiscoutBaseOn = 2 THEN (CASE WHEN (SELECT COUNT(*) FROM PromotionOfferItems WHERE numProId=PO.numProId AND tintRecordType=6 AND tintType=2 AND numValue=I.numItemClassification) > 0 THEN 1 ELSE 0 END)
																	WHEN tintDiscoutBaseOn = 3 AND tintOfferBasedOn = 1 THEN (CASE WHEN (SELECT COUNT(SimilarItems.numItemCode) FROM SimilarItems WHERE numParentItemCode IN (SELECT numValue FROM PromotionOfferItems WHERE numProId=PO.numProId AND tintRecordType=5 AND tintType=1 AND SimilarItems.numItemCode=I.numItemCode))>0 THEN 1 ELSE 0 END)
																	WHEN tintDiscoutBaseOn = 3 AND tintOfferBasedOn = 2 THEN (CASE WHEN (SELECT COUNT(SimilarItems.numItemCode) FROM SimilarItems WHERE SimilarItems.numItemCode IN(I.numItemCode) AND numParentItemCode IN (SELECT numItemCode FROM Item WHERE numItemClassification IN (SELECT numValue FROM PromotionOfferItems WHERE numProId=PO.numProId AND tintRecordType=5 AND tintType=2)))>0 THEN 1 ELSE 0 END)
																	ELSE 0
																END)
															)
													)  IsOnSale
								
                                         FROM      
                                                      Item AS I
                                         INNER JOIN   ItemCategory IC   ON   I.numItemCode = IC.numItemID
                                         LEFT JOIN    ItemImages IM     ON   I.numItemCode = IM.numItemCode AND IM.bitdefault=1 AND IM.bitIsImage = 1
                                         INNER JOIN   Category C        ON   IC.numCategoryID = C.numCategoryID
                                         INNER JOIN   SiteCategories SC ON   IC.numCategoryID = SC.numCategoryID ' + 
										 (CASE WHEN @tintPreLoginPriceLevel > 0 THEN ' OUTER APPLY(SELECT decDiscount AS monPrice FROM PricingTable WHERE tintRuleType=3 AND numItemCode=I.numItemCode ORDER BY numPricingID OFFSET ' + CAST(@tintPreLoginPriceLevel-1 AS VARCHAR) + ' ROWS FETCH NEXT 1 ROWS ONLY) As PricingOption ' ELSE '' END)
										 + ' INNER JOIN   eCommerceDTL E    ON   E.numDomainID = I.numDomainID AND E.numSiteId = SC.numSiteID ' + @Join + ' ' + @DefaultSortJoin + ' 
                                         OUTER APPLY (SELECT SUM(numOnHand) numOnHand FROM WareHouseItems W 
													  WHERE  W.numItemID = I.numItemCode
                                                      AND numWareHouseID IN (SELECT [OutParam] FROM dbo.[SplitString](''' + @vcWarehouseIDs + ''','',''))
                                                      AND W.numDomainID = ' + CONVERT(VARCHAR(10),@numDomainID) + ') AS W  
										 OUTER APPLY (SELECT TOP 1 numWareHouseItemID,monWListPrice FROM WareHouseItems W 
													  WHERE  W.numItemID = I.numItemCode 
													  AND W.numDomainID = ' + CONVERT(VARCHAR(10),@numDomainID) + '
													  AND numWareHouseID IN (SELECT [OutParam] FROM dbo.[SplitString](''' + @vcWarehouseIDs + ''','',''))) AS W1
										 OUTER APPLY (SELECT SUM(numAllocation) numAllocation FROM WareHouseItems W2 
													  WHERE  W2.numItemID = I.numItemCode
                                                      AND numWareHouseID IN (SELECT [OutParam] FROM dbo.[SplitString](''' + @vcWarehouseIDs + ''','',''))
                                                      AND W2.numDomainID = ' + CONVERT(VARCHAR(10),@numDomainID) + ') AS W2  			  
										 CROSS APPLY (SELECT dbo.fn_UOMConversion(ISNULL(I.numSaleUnit, 0),I.numItemCode, I.numDomainId,ISNULL(I.numBaseUnit, 0)) AS UOM) AS UOM
										 CROSS APPLY (SELECT dbo.fn_UOMConversion(ISNULL(I.numPurchaseUnit, 0),I.numItemCode, I.numDomainId,ISNULL(I.numBaseUnit, 0)) AS UOMPurchase) AS UOMPurchase '

			IF LEN(@FilterCustomFields) > 0 AND LEN(@FilterCustomWhere) > 0
			BEGIN
				SET @strSQL = @strSQL + ' INNER JOIN #tmpItemCode TP ON TP.numItemCode = I.numItemCode ' 
			END
			
			IF (SELECT COUNT(*) FROM #tmpItemCat) > 0
			BEGIN
				SET @strSQL = @strSQL + ' INNER JOIN #tmpItemCat TC ON TC.numCategoryID = IC.numCategoryID '
				SET @strSQL = @strSQL + ' LEFT JOIN #tmpOnHandItems TOIC ON TOIC.numItemID = I.numItemCode '
			END
			ELSE IF @numManufacturerID>0
			BEGIN
				SET @Where = REPLACE(@Where,'WHERE',' WHERE I.numManufacturer = ' + CONVERT(VARCHAR(10),@numManufacturerID) + ' AND ')
			END 
			ELSE IF @numCategoryID>0
			BEGIN
				SET @Where = REPLACE(@Where,'WHERE',' WHERE IC.numCategoryID = ' + CONVERT(VARCHAR(10),@numCategoryID) + ' AND ')
			END 
			
			IF @tintDisplayCategory = 1
			BEGIN
				SET @Where = @Where + ' AND 1 = (CASE WHEN (I.charItemType=''P'' AND ISNULL(I.bitKitParent,0) = 0) THEN CASE WHEN (ISNULL(W.numOnHand, 0) > 0 OR ISNULL(W2.numAllocation, 0) > 0) THEN 1 ELSE 0 END ELSE 1 END)'
			END
			
			IF (SELECT COUNT(*) FROM [dbo].[EcommerceRelationshipProfile] WHERE numSiteID=@numSiteID AND numRelationship = @numDefaultRelationship AND numProfile = @numDefaultProfile) > 0
			BEGIN
				SET @Where = @Where + CONCAT(' AND I.numItemClassification NOT IN (SELECT numItemClassification FROM EcommerceRelationshipProfile ERP INNER JOIN ECommerceItemClassification EIC ON EIC.numECommRelatiionshipProfileID=ERP.ID WHERE ERP.numSiteID=',@numSiteID,' AND ERP.numRelationship=',@numDefaultRelationship,' AND ERP.numProfile=', @numDefaultProfile,')')
			END

			--PRINT @Where
			SET @strSQL = @strSQL + @Where
			
			IF LEN(@FilterRegularCondition) > 0
			BEGIN
				SET @strSQL = @strSQL + @FilterRegularCondition
			END                                         
			
---PRINT @SortString + ',' + @DefaultSort + ' ' + @vcSort 
	
            IF LEN(@whereNumItemCode) > 0
                                        BEGIN
								              SET @strSQL = @strSQL + @filterByNumItemCode	
								    	END
            SET @strSQL = @strSQL +     '   ) SELECT * INTO #TEMPItems FROM Items
                                        
									DELETE FROM 
										#TEMPItems
									WHERE 
										numItemCode IN (
															SELECT 
																F.numItemCode
															FROM 
																#TEMPItems AS F
															WHERE 
																ISNULL(F.bitMatrix,0) = 1 
																AND EXISTS (
																			SELECT 
																				t1.vcItemName, t1.bitMatrix, t1.numItemGroup, Count(t1.numItemCode)
																			FROM 
																				#TEMPItems t1
																			WHERE 
																				t1.vcItemName = F.vcItemName
																				AND t1.bitMatrix = 1
																				AND t1.numItemGroup = F.numItemGroup
																			GROUP BY 
																				t1.vcItemName, t1.bitMatrix, t1.numItemGroup
																			HAVING 
																				Count(t1.numItemCode) > 1
																		)
														)
										AND numItemCode NOT IN (
																				SELECT 
																					Min(numItemCode)
																				FROM 
																					#TEMPItems AS F
																				WHERE
																					ISNULL(F.bitMatrix,0) = 1 
																					AND Exists (
																								SELECT 
																									t2.vcItemName, t2.bitMatrix, t2.numItemGroup, Count(t2.numItemCode)
																								FROM 
																									#TEMPItems t2
																								WHERE 
																									t2.vcItemName = F.vcItemName
																								   AND t2.bitMatrix = 1
																								   AND t2.numItemGroup = F.numItemGroup
																								GROUP BY 
																									t2.vcItemName, t2.bitMatrix, t2.numItemGroup
																								HAVING 
																									Count(t2.numItemCode) > 1
																							)
																				GROUP BY 
																					vcItemName, bitMatrix, numItemGroup
																			);  WITH ItemSorted AS (SELECT  ROW_NUMBER() OVER ( ORDER BY  ' + @SortString + @DefaultSort + ' ' + @vcSort + ' ) AS Rownumber,* FROM  #TEMPItems WHERE RowID = 1)'



            DECLARE @fldList AS NVARCHAR(MAX)
			SELECT @fldList=COALESCE(@fldList + ',','') + QUOTENAME(REPLACE(Fld_label,' ','') + '_C') FROM dbo.CFW_Fld_Master WHERE numDomainID=@numDomainID AND Grp_id=5
            
            SET @strSQL = @strSQL +' SELECT  Rownumber,numItemCode,bitMatrix,numItemGroup,txtItemDesc,vcManufacturer,monListPrice,vcItemName,vcCategoryName,vcPathForImage,vcpathForTImage,vcSKU,fltHeight,fltWidth,fltLength,vcModelID,fltWeight,bitFreeShipping,UOMConversionFactor,UOMPurchaseConversionFactor,InStock,CategoryDesc,numWareHouseItemID,IsOnSale
                                     INTO #tempItem FROM ItemSorted ' 

			
            
            SET @strSQL = @strSQL + 'SELECT * INTO #tmpPagedItems FROM #tempItem WHERE Rownumber > ' + CONVERT(VARCHAR(10), @firstRec) + '
                                      AND   Rownumber < ' + CONVERT(VARCHAR(10), @lastRec) + '
                                      order by Rownumber '
                                                                  
            IF LEN(ISNULL(@fldList,''))>0
            BEGIN
				PRINT 'flds1'
				SET @strSQL = @strSQL +'SELECT  Rownumber,numItemCode,txtItemDesc,vcManufacturer,monListPrice,vcItemName,vcCategoryName,vcPathForImage,vcpathForTImage,vcSKU,fltHeight,fltWidth,fltLength,vcModelID,fltWeight,bitFreeShipping,UOMConversionFactor,UOMPurchaseConversionFactor,InStock,CategoryDesc,numWareHouseItemID,IsOnSale,' + @fldList + '
                                        FROM #tmpPagedItems I left join (
			SELECT  t2.RecId, REPLACE(t1.Fld_label,'' '','''') + ''_C'' as Fld_label, 
				CASE t1.fld_type WHEN ''SelectBox'' THEN isnull(dbo.fn_GetListItemName(t2.Fld_Value),'''')
								 WHEN ''CheckBox'' THEN isnull(dbo.fn_getCustomDataValue(t2.Fld_Value,9),'''')
								 WHEN ''DateField'' THEN dbo.FormatedDateFromDate(t2.Fld_Value,' + CAST(@numDomainId AS VARCHAR(18)) +')
								 ELSE Replace(ISNULL(t2.Fld_Value,''0''),''$'',''$$'') end AS Fld_Value FROM CFW_Fld_Master t1 
			JOIN CFW_FLD_Values_Item AS t2 ON t1.Fld_ID = t2.Fld_ID where t1.Grp_id = 5 and t1.numDomainID=' + CAST(@numDomainId AS VARCHAR(18)) +' AND t1.fld_type <> ''Link''
			AND t2.RecId IN (SELECT numItemCode FROM #tmpPagedItems )
			) p PIVOT (MAX([Fld_Value]) FOR Fld_label IN (' + @fldList + ')) AS pvt on I.numItemCode=pvt.RecId order by Rownumber;' 	
			END 
			ELSE
			BEGIN
				PRINT 'flds2'
				SET @strSQL = @strSQL +' SELECT  Rownumber,numItemCode,txtItemDesc,vcManufacturer,monListPrice,vcItemName,vcCategoryName,vcPathForImage,vcpathForTImage,vcSKU,fltHeight,fltWidth,fltLength,vcModelID,fltWeight,bitFreeShipping,UOMConversionFactor,UOMPurchaseConversionFactor,InStock,CategoryDesc,numWareHouseItemID,IsOnSale
                                         FROM  #tmpPagedItems order by Rownumber;'
			END               
                                        
            
--            PRINT(@strSQL)        
--            EXEC ( @strSQL) ; 
			                           
            SET @strSQL = @strSQL + ' SELECT @TotRecs = COUNT(*) FROM #tempItem '                            

			 PRINT  LEN(@strSQL)
			 DECLARE @tmpSQL AS VARCHAR(MAX)
			 SET @tmpSQL = @strSQL
			 PRINT  @tmpSQL
			 
             EXEC sp_executeSQL @strSQL, N'@TotRecs INT OUTPUT', @TotRecs OUTPUT
             
             
        DECLARE @tintOrder AS TINYINT                                                  
		DECLARE @vcFormFieldName AS VARCHAR(50)                                                  
		DECLARE @vcListItemType AS VARCHAR(1)                                             
		DECLARE @vcAssociatedControlType VARCHAR(10)                                                  
		DECLARE @numListID AS NUMERIC(9)                                                  
		DECLARE @WhereCondition VARCHAR(MAX)                       
		DECLARE @numFormFieldId AS NUMERIC  
		DECLARE @vcFieldType CHAR(1)
		                  
		SET @tintOrder=0                                                  
		SET @WhereCondition =''                 
                   
              
		CREATE TABLE #tempAvailableFields(numFormFieldId  NUMERIC(9),vcFormFieldName NVARCHAR(50),
		vcFieldType CHAR(1),vcAssociatedControlType NVARCHAR(50),numListID NUMERIC(18),
		vcListItemType CHAR(1),intRowNum INT,vcItemValue VARCHAR(3000))
		   
		INSERT INTO #tempAvailableFields (numFormFieldId,vcFormFieldName,vcFieldType,vcAssociatedControlType
				,numListID,vcListItemType,intRowNum)                         
		SELECT  Fld_id,REPLACE(Fld_label,' ','') AS vcFormFieldName,
			   CASE GRP_ID WHEN 4 then 'C' WHEN 1 THEN 'D' ELSE 'C' END AS vcFieldType,
				fld_type as vcAssociatedControlType,
				ISNULL(C.numListID, 0) numListID,
				CASE WHEN C.numListID > 0 THEN 'L'
					 ELSE ''
				END vcListItemType,ROW_NUMBER() OVER (ORDER BY Fld_id ASC) AS intRowNum
		FROM    CFW_Fld_Master C LEFT JOIN CFW_Validation V ON V.numFieldID = C.Fld_id
		WHERE   C.numDomainID = @numDomainId
				AND GRP_ID IN (5)

		  
		SELECT * FROM #tempAvailableFields
		
		/*
		DROP TABLE #tempAvailableFields
		DROP TABLE #tmpItemCode
		DROP TABLE #tmpItemCat
		*/	

		IF OBJECT_ID('tempdb..#tempAvailableFields') IS NOT NULL
		DROP TABLE #tempAvailableFields
		
		IF OBJECT_ID('tempdb..#tmpItemCode') IS NOT NULL
		DROP TABLE #tmpItemCode
		
		IF OBJECT_ID('tempdb..#tmpItemCat') IS NOT NULL
		DROP TABLE #tmpItemCat
		
		IF OBJECT_ID('tempdb..#fldValues') IS NOT NULL
		DROP TABLE #fldValues
		
		IF OBJECT_ID('tempdb..#tempSort') IS NOT NULL
		DROP TABLE #tempSort
		
		IF OBJECT_ID('tempdb..#fldDefaultValues') IS NOT NULL
		DROP TABLE #fldDefaultValues
		
		IF OBJECT_ID('tempdb..#tmpOnHandItems') IS NOT NULL
		DROP TABLE #tmpOnHandItems

		IF OBJECT_ID('tempdb..#tmpPagedItems') IS NOT NULL
		DROP TABLE #tmpPagedItems


		IF OBJECT_ID('tempdb..#TEMPItems') IS NOT NULL
		DROP TABLE #TEMPItems
    END


/****** Object:  StoredProcedure [dbo].[USP_Journal_EntryListForBankReconciliation]    Script Date: 05/07/2009 22:01:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created By Chintan
-- EXEC USP_Journal_EntryListForBankReconciliation 2233,'','',0,1
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_Journal_EntryListForBankReconciliation')
DROP PROCEDURE USP_Journal_EntryListForBankReconciliation
GO
CREATE PROCEDURE [dbo].[USP_Journal_EntryListForBankReconciliation]
               @numChartAcntId AS NUMERIC(9),
               @tintFlag       TINYINT=0,
               @numDomainId    AS NUMERIC(9),
               @bitDateHide AS BIT=0,
               @numReconcileID AS NUMERIC(9),
               @tintReconcile AS TINYINT=0 --0:not Reconcile 1:Reconsile for particular ReconID 2:All reconcile for particular ReconID and all non reconcile
AS
  BEGIN
  
  DECLARE @numCreditAmt NUMERIC(9)
  DECLARE @numDebitAmt NUMERIC(9)
  DECLARE @dtStatementDate AS DATETIME
  
  SELECT @dtStatementDate=dtStatementDate FROM BankReconcileMaster WHERE numReconcileID=@numReconcileID

  create table #TempBankRecon
  (numChartAcntId numeric(18),
  numTransactionId numeric(18),
  bitReconcile bit,
  EntryDate date,
  numDivisionID NUMERIC(18,0),
  CompanyName varchar(500),
  Deposit float,
  Payment float,
  Memo  varchar(250),
  TransactionType varchar(100),
  JournalId numeric(18),
  numCheckHeaderID numeric(18),
  vcReference varchar(1000),
  tintOppType INTEGER,bitCleared BIT,numCashCreditCardId NUMERIC(18),numOppId NUMERIC(18),numOppBizDocsId NUMERIC(18),
  numDepositId NUMERIC(18),numCategoryHDRID NUMERIC(9),tintTEType TINYINT,numCategory NUMERIC(18),numUserCntID NUMERIC(18),dtFromDate DATETIME,
  numBillId NUMERIC(18),numBillPaymentID NUMERIC(18),numLandedCostOppId NUMERIC(18),bitMatched BIT,vcCheckNo VARCHAR(20))
  
  IF (@tintFlag = 1)
	SET @numDebitAmt = 0
  ELSE 	IF (@tintFlag = 2)
	SET @numCreditAmt = 0 
  
  insert into #TempBankRecon
  
    SELECT GJD.numChartAcntId AS numChartAcntId,
		   GJD.numTransactionId,
		   ISNULL(GJD.bitReconcile,0) bitReconcile,	
           GJH.datEntry_Date AS EntryDate,
		   ISNULL(DM.numDivisionID,0),
           CI.vcCompanyName AS CompanyName,
			(case when GJD.numCreditAmt=0 then GJD.numdebitAmt  end) as Deposit,        
			(case when GJD.numdebitAmt=0 then GJD.numCreditAmt end) as Payment,   
           --GJH.[datEntry_Date]  as EntryDate,
           GJD.varDescription as Memo,
            case when isnull(GJH.numCheckHeaderID,0) <> 0 THEN case (select isnull(OBD.numCheckNo,0) from OpportunityBizDocsPaymentDetails OBD where OBD.numBizDocsPaymentDetId=isnull(GJH.numBizDocsPaymentDetId,0))
					when 0 then 'Cash' else 'Checks' end 			
			when isnull(GJH.numCashCreditCardId,0) <> 0 And CCCD.bitMoneyOut=0 then 'Cash'
			when isnull(GJH.numCashCreditCardId,0) <> 0 And CCCD.bitMoneyOut=1 And CCCD.bitChargeBack=1  then 'Charge'
			when isnull(GJH.numOppId,0) <> 0 And isnull(GJH.numOppBizDocsId,0) <> 0 And isnull(GJH.numBizDocsPaymentDetId,0)=0 And Opp.tintOppType=1 then 'BizDocs Invoice'
			when isnull(GJH.numOppId,0) <> 0 And isnull(GJH.numOppBizDocsId,0) <> 0 And isnull(GJH.numBizDocsPaymentDetId,0)=0 And Opp.tintOppType=2 then 'BizDocs Purchase'
			when isnull(GJH.numDepositId,0) <> 0 then 'Deposit'
			when isnull(GJH.numBizDocsPaymentDetId,0)<>0 And Opp.tintOppType=1 then 'Receive Amt'
			when isnull(GJH.numBizDocsPaymentDetId,0)<>0 And Opp.tintOppType=2 then 'Vendor Amt'
			when isnull(GJH.numCategoryHDRID,0)<>0 then 'Time And Expenses'
			When ISNULL(GJH.numBillId,0) <>0 AND isnull(BH.bitLandedCost,0)=1 then 'Landed Cost' 
			When ISNULL(GJH.numBillId,0) <>0 then 'Bill' 
			When ISNULL(GJH.numBillPaymentID,0) <>0 then 'Pay Bill' 
			When GJH.numJournal_Id <>0 then 'Journal' End  as TransactionType,
			GJD.numJournalId AS JournalId,
			isnull(GJH.numCheckHeaderID,0) as numCheckHeaderID,
			ISNULL((SELECT Narration FROM VIEW_BIZPAYMENT WHERE numDomainID=GJH.numDomainID AND numBizDocsPaymentDetId = isnull(GJH.numBizDocsPaymentDetId,0)),GJH.varDescription) vcReference,
			isnull(Opp.tintOppType,0),ISNULL(GJD.bitCleared,0) AS bitCleared,
			ISNULL(GJH.numCashCreditCardId,0) AS numCashCreditCardId,ISNULL(GJH.numOppId,0) AS numOppId,
			isnull(GJH.numOppBizDocsId,0) AS numOppBizDocsId,isnull(GJH.numDepositId,0) AS numDepositId,
			isnull(GJH.numCategoryHDRID,0) AS numCategoryHDRID,TE.tintTEType,TE.numCategory,TE.numUserCntID,TE.dtFromDate,ISNULL(GJH.numBillId,0) AS numBillID,ISNULL(GJH.numBillPaymentID,0) AS numBillPaymentID,
			Case When isnull(BH.bitLandedCost,0)=1 then  isnull(BH.numOppId,0) else 0 end AS numLandedCostOppId,
			0 AS bitMatched
			,ISNULL(CAST(CheckHeader.numCheckNo AS VARCHAR),'')
    FROM   General_Journal_Header GJH
           INNER JOIN General_Journal_Details GJD
             ON GJH.numJournal_Id = GJD.numJournalId
           LEFT OUTER JOIN DivisionMaster AS DM
             ON GJD.numCustomerId = DM.numDivisionID
           LEFT OUTER JOIN CompanyInfo AS CI
             ON DM.numCompanyID = CI.numCompanyId
            Left outer join CashCreditCardDetails CCCD on GJH.numCashCreditCardId=CCCD.numCashCreditId        
			Left outer join (select * from OpportunityMaster WHERE tintOppType=1 ) Opp on GJH.numOppId=Opp.numOppId    
			LEFT OUTER JOIN TimeAndExpense TE ON GJH.numCategoryHDRID = TE.numCategoryHDRID
			LEFT OUTER JOIN BillHeader BH  ON ISNULL(GJH.numBillId,0) = BH.numBillId
			LEFT OUTER JOIN CheckHeader ON GJH.numCheckHeaderID = CheckHeader.numCheckHeaderID
    WHERE  
		   ((GJH.[datEntry_Date] <= @dtStatementDate AND @bitDateHide=1) OR @bitDateHide=0)
           AND GJD.numChartAcntId = @numChartAcntId
           AND GJD.numDomainId = @numDomainId
           AND (GJD.numCreditAmt <> @numCreditAmt OR @numCreditAmt IS null)
           AND (GJD.numDebitAmt <> @numDebitAmt OR @numDebitAmt IS null)
           AND 1=(CASE WHEN @tintReconcile=0 THEN CASE WHEN ISNULL(GJD.bitReconcile,0)=0 THEN 1 ELSE 0 END
					   WHEN @tintReconcile=1 THEN CASE WHEN ISNULL(GJD.numReconcileID,0)=@numReconcileID THEN 1 ELSE 0 END
					   WHEN @tintReconcile=2 THEN CASE WHEN  (ISNULL(GJD.bitReconcile,0)=1 AND ISNULL(GJD.numReconcileID,0)=@numReconcileID) OR ISNULL(GJD.bitReconcile,0)=0 THEN 1 ELSE 0 END END)
			ORDER BY GJH.datEntry_Date

	SELECT
		B1.ID,
		dbo.FormatedDateFromDate(B1.dtEntryDate,@numDomainID) as dtEntryDate,
		ISNULL(vcReference,'') as [vcReference],
		[fltAmount],
		ISNULL(vcPayee,'') as vcPayee,
		ISNULL(vcDescription,'') as vcDescription,
		ISNULL(B1.bitCleared,0) bitCleared,
		ISNULL(B1.bitReconcile,0) bitReconcile,
		0 AS numBizTransactionId,
		0 AS bitMatched,
		0 AS bitDuplicate
	INTO 
		#TempStatememnt
	FROM
		BankReconcileFileData B1
	WHERE
		numReconcileID=@numReconcileID 
		AND ISNULL(B1.bitReconcile,0) = 0

	IF EXISTS (SELECT * FROM BankReconcileMatchRule WHERE numDomainID = @numDomainId AND @numChartAcntId IN (SELECT Id FROM dbo.SplitIDs(vcBankAccounts,',')))
	BEGIN
		DECLARE @TEMP TABLE
		(
			ID INT IDENTITY(1,1)
			,numRuleID NUMERIC(18,0)
		)

		INSERT INTO @TEMP
		(
			numRuleID
		)
		SELECT
			ID
		FROM
			BankReconcileMatchRule
		WHERE
			numDomainID = @numDomainId
			AND @numChartAcntId IN (SELECT Id FROM dbo.SplitIDs(vcBankAccounts,','))
		ORDER BY
			tintOrder


		DECLARE @i AS INT = 1
		DECLARE @numRuleID AS NUMERIC(18,0)
		DECLARE @bitMatchAllConditions AS BIT
		DECLARE @Count AS INT 
		SELECT @Count = COUNT(*) FROM @TEMP

		WHILE @i <= @Count
		BEGIN
			SELECT @numRuleID=numRuleID,@bitMatchAllConditions=bitMatchAllConditions FROM @TEMP T1 INNER JOIN BankReconcileMatchRule BRMR ON T1.numRuleID=BRMR.ID WHERE T1.ID = @i 

			UPDATE
				TS
			SET 
				numBizTransactionId = (CASE WHEN ISNULL(TEMP1.numTransactionId ,0) > 0 THEN TEMP1.numTransactionId ELSE 0 END),
				bitMatched = (CASE WHEN ISNULL(TEMP1.numTransactionId ,0) > 0 THEN 1 ELSE 0 END),
				bitDuplicate = (CASE WHEN ISNULL(TEMP2.ID ,0) > 0 THEN 1 ELSE 0 END)
			FROM
				#TempStatememnt TS
			INNER JOIN
				BankReconcileFileData B1
			ON
				TS.ID = B1.ID
				AND ISNULL(TS.numBizTransactionId,0) = 0
				AND ISNULL(TS.bitMatched,0) = 0
			OUTER APPLY
			(
				SELECT TOP 1
					t1.numTransactionId
				FROM
					#TempBankRecon t1
				OUTER APPLY
				(
					SELECT 
						(CASE 
							WHEN @bitMatchAllConditions = 1
							THEN
								(SELECT
									(CASE WHEN COUNT(*) > 0 THEN 0 ELSE 1 END)
								FROM
								(
									SELECT
										numConditionID
										,(CASE tintConditionOperator
												WHEN 1 --contains
												THEN (CASE WHEN LOWER((CASE tintColumn WHEN 1 THEN B1.vcPayee WHEN 2 THEN B1.vcDescription WHEN 3 THEN B1.vcReference END)) LIKE '%' + LOWER(BRMRC.vcTextToMatch) + '%' AND BRMRC.numDivisionID = t1.numDivisionID THEN 1 ELSE 0 END)
												WHEN 2 --equals 
												THEN (CASE WHEN (CASE tintColumn WHEN 1 THEN B1.vcPayee WHEN 2 THEN B1.vcDescription WHEN 3 THEN B1.vcReference END) = BRMRC.vcTextToMatch AND BRMRC.numDivisionID = t1.numDivisionID THEN 1 ELSE 0 END)
												WHEN 3 --starts with
												THEN (CASE WHEN (CASE tintColumn WHEN 1 THEN B1.vcPayee WHEN 2 THEN B1.vcDescription WHEN 3 THEN B1.vcReference END) LIKE BRMRC.vcTextToMatch + '%' AND BRMRC.numDivisionID = t1.numDivisionID THEN 1 ELSE 0 END)
												WHEN 4 --ends with
												THEN (CASE WHEN (CASE tintColumn WHEN 1 THEN B1.vcPayee WHEN 2 THEN B1.vcDescription WHEN 3 THEN B1.vcReference END) LIKE '%' + BRMRC.vcTextToMatch AND BRMRC.numDivisionID = t1.numDivisionID THEN 1 ELSE 0 END)
												ELSE 
													0
											END) AS isMatched
									FROM
										BankReconcileMatchRuleCondition BRMRC
									WHERE
										numRuleID=@numRuleID
								) T3
								WHERE
									T3.isMatched = 0)
							ELSE
								(SELECT
									(CASE WHEN COUNT(*) > 0 THEN 1 ELSE 0 END)
								FROM
								(
									SELECT
										numConditionID
										,(CASE tintConditionOperator
												WHEN 1 --contains
												THEN (CASE WHEN (CASE tintColumn WHEN 1 THEN B1.vcPayee WHEN 2 THEN B1.vcDescription WHEN 3 THEN B1.vcReference END) LIKE '%' + BRMRC.vcTextToMatch + '%' AND BRMRC.numDivisionID = t1.numDivisionID THEN 1 ELSE 0 END)
												WHEN 2 --equals 
												THEN (CASE WHEN (CASE tintColumn WHEN 1 THEN B1.vcPayee WHEN 2 THEN B1.vcDescription WHEN 3 THEN B1.vcReference END) = BRMRC.vcTextToMatch AND BRMRC.numDivisionID = t1.numDivisionID THEN 1 ELSE 0 END)
												WHEN 3 --starts with
												THEN (CASE WHEN (CASE tintColumn WHEN 1 THEN B1.vcPayee WHEN 2 THEN B1.vcDescription WHEN 3 THEN B1.vcReference END) LIKE BRMRC.vcTextToMatch + '%' AND BRMRC.numDivisionID = t1.numDivisionID THEN 1 ELSE 0 END)
												WHEN 4 --ends with
												THEN (CASE WHEN (CASE tintColumn WHEN 1 THEN B1.vcPayee WHEN 2 THEN B1.vcDescription WHEN 3 THEN B1.vcReference END) LIKE '%' + BRMRC.vcTextToMatch AND BRMRC.numDivisionID = t1.numDivisionID THEN 1 ELSE 0 END)
												ELSE 
													0
											END) AS isMatched
									FROM
										BankReconcileMatchRuleCondition BRMRC
									WHERE
										numRuleID=@numRuleID
								) T4
								WHERE
									T4.isMatched = 1)
						END) AS bitMatched
				) TEMPConditions
				WHERE
					t1.EntryDate = B1.dtEntryDate
					AND TEMPConditions.bitMatched = 1
					AND (t1.Deposit = B1.fltAmount OR t1.Payment*-1=B1.fltAmount)
					AND ISNULL(t1.bitMatched,0) = 0
			) AS TEMP1
			OUTER APPLY
			(
				SELECT TOP 1
					t1.ID
				FROM
					BankReconcileFileData t1
				WHERE
					t1.numReconcileID <> @numReconcileID
					AND t1.numDomainID=@numDomainId
					AND (ISNULL(t1.bitReconcile,0) = 1 OR ISNULL(t1.bitCleared,0) = 1)
					AND t1.dtEntryDate = B1.dtEntryDate
					AND B1.vcPayee = t1.vcPayee
					AND B1.fltAmount = t1.fltAmount
					AND B1.vcReference = t1.vcReference
					AND B1.vcDescription=t1.vcDescription
			) AS TEMP2
			WHERE
				numReconcileID=@numReconcileID
				

			UPDATE #TempBankRecon SET bitMatched = 1 WHERE ISNULL(bitMatched,0)=0 AND numTransactionId IN (SELECT ISNULL(numBizTransactionId,0) FROM #TempStatememnt t1 WHERE ISNULL(t1.numBizTransactionId,0) > 0)

			SET @i = @i + 1
		END
	END

	UPDATE
		TS
	SET 
		numBizTransactionId = (CASE WHEN ISNULL(TEMP1.numTransactionId ,0) > 0 THEN TEMP1.numTransactionId ELSE 0 END),
		bitMatched = (CASE WHEN ISNULL(TEMP1.numTransactionId ,0) > 0 THEN 1 ELSE 0 END),
		bitDuplicate = (CASE WHEN ISNULL(TEMP2.ID ,0) > 0 THEN 1 ELSE 0 END)
	FROM
		#TempStatememnt TS
	INNER JOIN
		BankReconcileFileData B1
	ON
		TS.ID = B1.ID
		AND ISNULL(TS.numBizTransactionId,0) = 0
		AND ISNULL(TS.bitMatched,0) = 0
	OUTER APPLY
	(
		SELECT TOP 1
			t1.numTransactionId
		FROM
			#TempBankRecon t1
		WHERE
			t1.EntryDate = B1.dtEntryDate
			AND (CHARINDEX(UPPER(t1.CompanyName),UPPER(B1.vcPayee)) > 0 OR CHARINDEX(UPPER(t1.CompanyName),UPPER(B1.vcDescription)) > 0 OR (CHARINDEX('CHECK',UPPER(B1.vcDescription)) > 0 AND LEN(t1.vcCheckNo) > 0 AND CHARINDEX(UPPER(t1.vcCheckNo),UPPER(B1.vcDescription)) > 0))
			AND (t1.Deposit = B1.fltAmount OR t1.Payment*-1=B1.fltAmount)
	) AS TEMP1
	OUTER APPLY
	(
		SELECT TOP 1
			t1.ID
		FROM
			BankReconcileFileData t1
		WHERE
			t1.numReconcileID <> @numReconcileID
			AND t1.numDomainID=@numDomainId
			AND (ISNULL(t1.bitReconcile,0) = 1 OR ISNULL(t1.bitCleared,0) = 1)
			AND t1.dtEntryDate = B1.dtEntryDate
			AND B1.vcPayee = t1.vcPayee
			AND B1.fltAmount = t1.fltAmount
			AND B1.vcReference = t1.vcReference
			AND B1.vcDescription=t1.vcDescription
	) AS TEMP2
	WHERE
		numReconcileID=@numReconcileID 
		AND ISNULL(Ts.bitMatched,0) = 0

	UPDATE #TempBankRecon SET bitMatched = 1 WHERE numTransactionId IN (SELECT ISNULL(numBizTransactionId,0) FROM #TempStatememnt t1 WHERE ISNULL(t1.numBizTransactionId,0) > 0)
	

	

	SELECT * FROM #TempBankRecon
	SELECT * FROM #TempStatememnt WHERE ISNULL(bitDuplicate,0)=0


	SELECT 
		(SELECT COUNT(*) FROM #TempStatememnt) AS TotalRecords
		,(SELECT COUNT(*) FROM #TempStatememnt WHERE bitDuplicate=1) AS DuplicateRecords
		,(SELECT COUNT(*) FROM #TempStatememnt WHERE bitMatched=1) AS MatchedRecords

	drop table #TempBankRecon    
	drop table #TempStatememnt       
  END
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ManageBankReconcileMaster')
DROP PROCEDURE USP_ManageBankReconcileMaster
GO
CREATE PROCEDURE USP_ManageBankReconcileMaster
	@tintMode AS TINYINT, 
    @numDomainID numeric(18, 0),
    @numReconcileID numeric(18, 0),
    @numCreatedBy numeric(18, 0),
    @dtStatementDate datetime,
    @monBeginBalance money,
    @monEndBalance money,
    @numChartAcntId numeric(18, 0),
    @monServiceChargeAmount money,
    @numServiceChargeChartAcntId numeric(18, 0),
    @dtServiceChargeDate datetime,
    @monInterestEarnedAmount money,
    @numInterestEarnedChartAcntId numeric(18, 0),
    @dtInterestEarnedDate DATETIME,
    @bitReconcileComplete BIT=0,
	@vcFileName VARCHAR(300)='',
	@vcFileData VARCHAR(MAX) = ''
AS

--Validation of closed financial year
EXEC dbo.USP_ValidateFinancialYearClosingDate @numDomainID,@dtStatementDate
	
	IF @tintMode=1 --SELECT
	BEGIN
		DECLARE @numCreditAmt AS MONEY,@numDebitAmt AS MONEY 
		
			SELECT @numCreditAmt=ISNULL(SUM(numCreditAmt),0),@numDebitAmt=ISNULL(SUM(numDebitAmt),0) 
				FROM General_Journal_Details WHERE numDomainID=@numDomainID AND [numReconcileID] = @numReconcileID 
				AND numChartAcntId=(SELECT numChartAcntId FROM [BankReconcileMaster] WHERE  numDomainID=@numDomainID AND [numReconcileID] = @numReconcileID)
				--AND ISNULL(bitCleared,0)=1
	
			SELECT [numReconcileID],[numDomainID],[numCreatedBy],[dtCreatedDate],[dtStatementDate],[monBeginBalance]
      ,[monEndBalance],[numChartAcntId],[monServiceChargeAmount],[numServiceChargeChartAcntId],[dtServiceChargeDate]
      ,[monInterestEarnedAmount],[numInterestEarnedChartAcntId],[dtInterestEarnedDate],[bitReconcileComplete]
      ,[dtReconcileDate],@numCreditAmt AS Payment,@numDebitAmt AS Deposit,
			dbo.fn_GetContactName(ISNULL(numCreatedBy,0)) AS vcReconciledby,
			dbo.fn_GetChart_Of_AccountsName(ISNULL(numChartAcntId,0)) AS vcAccountName,ISNULL(vcFileName,'') AS vcFileName
			FROM   [BankReconcileMaster] WHERE  numDomainID=@numDomainID AND [numReconcileID] = @numReconcileID
	END
	ELSE IF @tintMode=2 --Insert/Update
	BEGIN
		IF @numReconcileID>0 --Update
			BEGIN
				UPDATE [BankReconcileMaster]
					SET    [dtStatementDate] = @dtStatementDate, [monBeginBalance]=@monBeginBalance, [monEndBalance]=@monEndBalance, [numChartAcntId] = @numChartAcntId, [monServiceChargeAmount] = @monServiceChargeAmount, [numServiceChargeChartAcntId] = @numServiceChargeChartAcntId,dtServiceChargeDate=@dtServiceChargeDate, [monInterestEarnedAmount] = @monInterestEarnedAmount, [numInterestEarnedChartAcntId] = @numInterestEarnedChartAcntId,dtInterestEarnedDate=@dtInterestEarnedDate
					WHERE  [numDomainID] = @numDomainID AND [numReconcileID] = @numReconcileID
			END
		ELSE --Insert
			BEGIN
				INSERT INTO [BankReconcileMaster] ([numDomainID], [numCreatedBy], [dtCreatedDate], [dtStatementDate], [monBeginBalance], [monEndBalance], [numChartAcntId], [monServiceChargeAmount], [numServiceChargeChartAcntId],dtServiceChargeDate,[monInterestEarnedAmount], [numInterestEarnedChartAcntId],dtInterestEarnedDate,vcFileName)
				SELECT @numDomainID, @numCreatedBy, GETUTCDATE(), @dtStatementDate, @monBeginBalance, @monEndBalance, @numChartAcntId, @monServiceChargeAmount, @numServiceChargeChartAcntId,@dtServiceChargeDate,@monInterestEarnedAmount, @numInterestEarnedChartAcntId,@dtInterestEarnedDate,@vcFileName
			
				SET @numReconcileID=SCOPE_IDENTITY()


				IF LEN(@vcFileName) > 0
				BEGIN
					DECLARE @hDocItem int
					EXEC sp_xml_preparedocument @hDocItem OUTPUT, @vcFileData

					INSERT INTO BankReconcileFileData
					(
						[numDOmainID],
						[numReconcileID],
						[dtEntryDate],
						[vcReference],
						[fltAmount],
						[vcPayee],
						[vcDescription]
					)
					SELECT 
						@numDomainID,
						@numReconcileID,
						dtEntryDate,
						vcReference,
						fltAmount,
						vcPayee,
						vcDescription
					FROM 
						OPENXML (@hDocItem,'/BankStatement/Trasactions',2)                                                                          
					WITH                       
					(                                                                          
						dtEntryDate Date, vcReference VARCHAR(500), fltAmount FLOAT, vcPayee VARCHAR(1000),vcDescription VARCHAR(1000)
					)
				END
			END	
			
			SELECT [numReconcileID], [numDomainID], [numCreatedBy], [dtCreatedDate], [dtStatementDate], [monBeginBalance], [monEndBalance], [numChartAcntId], [monServiceChargeAmount], [numServiceChargeChartAcntId], [monInterestEarnedAmount], [numInterestEarnedChartAcntId] 
			FROM   [BankReconcileMaster] WHERE  numDomainID=@numDomainID AND [numReconcileID] = @numReconcileID
	   END
	 ELSE IF @tintMode=3 --Delete
	 BEGIN
	    --Delete Service Charge,Interest Earned and Adjustment entry
	    DELETE FROM General_Journal_Details WHERE numJournalId IN (SELECT numJournal_Id FROM General_Journal_Header WHERE numDomainID=@numDomainID AND ISNULL(numReconcileID,0) = @numReconcileID)
	    DELETE FROM General_Journal_Header WHERE numDomainID=@numDomainID AND ISNULL(numReconcileID,0) = @numReconcileID
	 
	    --Set Journal_Details entry to bitReconcile=0 & bitCleared=0
        UPDATE dbo.General_Journal_Details SET bitReconcile=0,bitCleared=0,numReconcileID=NULL WHERE numDomainID=@numDomainID AND [numReconcileID] = @numReconcileID
        
		DELETE FROM [BankReconcileFileData] WHERE numDomainID=@numDomainID AND [numReconcileID] = @numReconcileID

	 	DELETE FROM [BankReconcileMaster] WHERE numDomainID=@numDomainID AND [numReconcileID] = @numReconcileID
	 END
	 ELSE IF @tintMode=4 -- Select based on numChartAcntId and bitReconcileComplete
	 BEGIN
		SELECT *,(SELECT ISNULL(SUM(numCreditAmt),0) FROM General_Journal_Details WHERE numDomainID=@numDomainID AND [numReconcileID] = BRM.numReconcileID /*AND ISNULL(bitReconcile,0)=1*/ AND numChartAcntId=BRM.numChartAcntId) AS monPayment
		,(SELECT ISNULL(SUM(numDebitAmt),0) FROM General_Journal_Details WHERE numDomainID=@numDomainID AND [numReconcileID] = BRM.numReconcileID /*AND ISNULL(bitReconcile,0)=1*/ AND numChartAcntId=BRM.numChartAcntId) AS monDeposit
	      FROM BankReconcileMaster BRM WHERE  numDomainID=@numDomainID AND numChartAcntId=@numChartAcntId 
			AND ISNULL(bitReconcileComplete,0)=@bitReconcileComplete ORDER BY numReconcileID DESC 
	 END
	 ELSE IF @tintMode=5 -- Complete Bank Reconcile
	 BEGIN
	    UPDATE General_Journal_Details SET bitReconcile=1,bitCleared=0 WHERE numDomainID=@numDomainID AND [numReconcileID] = @numReconcileID

		UPDATE BankReconcileFileData SET bitReconcile=1,bitCleared=0 WHERE numDomainID=@numDomainID AND [numReconcileID] = @numReconcileID AND ISNULL(bitCleared,0)=1
	 
		UPDATE BankReconcileMaster SET bitReconcileComplete=1,dtReconcileDate=GETUTCDATE(),numCreatedBy=@numCreatedBy 
			WHERE numDomainID=@numDomainID AND [numReconcileID] = @numReconcileID
	 END
	ELSE IF @tintMode=6 -- Get Last added entry for numChartAcntId
	BEGIN
	SELECT TOP 1 *
	    FROM BankReconcileMaster BRM WHERE  numDomainID=@numDomainID AND numChartAcntId=@numChartAcntId 
		AND ISNULL(bitReconcileComplete,0)=1 ORDER BY numReconcileID DESC 
	END
	ELSE IF @tintMode = 7 -- Select based on numChartAcntId
	BEGIN
		SELECT *,(SELECT ISNULL(SUM(numCreditAmt),0) FROM General_Journal_Details WHERE numDomainID=@numDomainID AND [numReconcileID] = BRM.numReconcileID /*AND ISNULL(bitReconcile,0)=1*/ AND numChartAcntId=BRM.numChartAcntId) AS monPayment
		,(SELECT ISNULL(SUM(numDebitAmt),0) FROM General_Journal_Details WHERE numDomainID=@numDomainID AND [numReconcileID] = BRM.numReconcileID /*AND ISNULL(bitReconcile,0)=1*/ AND numChartAcntId=BRM.numChartAcntId) AS monDeposit
	      FROM BankReconcileMaster BRM WHERE  numDomainID=@numDomainID AND numChartAcntId=@numChartAcntId 
			ORDER BY numReconcileID DESC 
	END
    
GO
/****** Object:  StoredProcedure [dbo].[USP_ManageItemsAndKits]    Script Date: 02/15/2010 21:44:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Created By Anoop Jayaraj                                                          
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_manageitemsandkits')
DROP PROCEDURE usp_manageitemsandkits
GO
CREATE PROCEDURE [dbo].[USP_ManageItemsAndKits]                                                                
@numItemCode as numeric(9) output,                                                                
@vcItemName as varchar(300),                                                                
@txtItemDesc as varchar(1000),                                                                
@charItemType as char(1),                                                                
@monListPrice as money,                                                                
@numItemClassification as numeric(9),                                                                
@bitTaxable as bit,                                                                
@vcSKU as varchar(50),                                                                                                                                      
@bitKitParent as bit,                                                                                   
--@dtDateEntered as datetime,                                                                
@numDomainID as numeric(9),                                                                
@numUserCntID as numeric(9),                                                                                                  
@numVendorID as numeric(9),                                                            
@bitSerialized as bit,                                               
@strFieldList as text,                                            
@vcModelID as varchar(200)='' ,                                            
@numItemGroup as numeric(9)=0,                                      
@numCOGSChartAcntId as numeric(9)=0,                                      
@numAssetChartAcntId as numeric(9)=0,                                      
@numIncomeChartAcntId as numeric(9)=0,                            
@monAverageCost as money,                          
@monLabourCost as money,                  
@fltWeight as float,                  
@fltHeight as float,                  
@fltLength as float,                  
@fltWidth as float,                  
@bitFreeshipping as bit,                
@bitAllowBackOrder as bit,              
@UnitofMeasure as varchar(30)='',              
@strChildItems as text,            
@bitShowDeptItem as bit,              
@bitShowDeptItemDesc as bit,        
@bitCalAmtBasedonDepItems as bit,    
@bitAssembly as BIT,
@intWebApiId INT=null,
@vcApiItemId AS VARCHAR(50)=null,
@numBarCodeId as VARCHAR(25)=null,
@vcManufacturer as varchar(250)=NULL,
@numBaseUnit AS NUMERIC(18)=0,
@numPurchaseUnit AS NUMERIC(18)=0,
@numSaleUnit AS NUMERIC(18)=0,
@bitLotNo as BIT,
@IsArchieve AS BIT,
@numItemClass as numeric(9)=0,
@tintStandardProductIDType AS TINYINT=0,
@vcExportToAPI VARCHAR(50),
@numShipClass NUMERIC,
@ProcedureCallFlag SMALLINT =0,
@vcCategories AS VARCHAR(2000) = '',
@bitAllowDropShip AS BIT=0,
@bitArchiveItem AS BIT = 0,
@bitAsset AS BIT = 0,
@bitRental AS BIT = 0,
@numCategoryProfileID AS NUMERIC(18,0) = 0,
@bitVirtualInventory BIT,
@bitContainer BIT,
@numContainer NUMERIC(18,0)=0,
@numNoItemIntoContainer NUMERIC(18,0),
@bitMatrix BIT = 0 ,
@vcItemAttributes VARCHAR(2000) = '',
@numManufacturer NUMERIC(18,0)
AS
BEGIN     
BEGIN TRY
BEGIN TRANSACTION

	SET @vcManufacturer = CASE WHEN ISNULL(@numManufacturer,0) > 0 THEN ISNULL((SELECT vcCompanyName FROM DivisionMaster INNER JOIN CompanyInfo ON DivisionMaster.numCompanyID=CompanyInfo.numCompanyId WHERE numDivisionID=@numManufacturer),'')  ELSE ISNULL(@vcManufacturer,'') END

	DECLARE @bitAttributesChanged AS BIT  = 1
	DECLARE @bitOppOrderExists AS BIT = 0

	IF ISNULL(@numItemCode,0) > 0
	BEGIN
		IF (SELECT COUNT(OI.numOppId) FROM dbo.OpportunityItems OI WHERE OI.numItemCode =@numItemCode) > 0
		BEGIN
			SET @bitOppOrderExists = 1
		END
		ELSE IF (SELECT COUNT(OI.numOppId) FROM dbo.OpportunityKitItems OI WHERE OI.numChildItemID =@numItemCode) > 0
		BEGIN
			SET @bitOppOrderExists = 1
		END
		ELSE IF (SELECT COUNT(OI.numOppId) FROM dbo.OpportunityKitChildItems OI WHERE OI.numItemID =@numItemCode) > 0
		BEGIN
			SET @bitOppOrderExists = 1
		END
	END
	ELSE 
		BEGIN
		   IF (ISNULL(@vcItemName,'') = '')
			   BEGIN
				   RAISERROR('ITEM_NAME_NOT_SELECTED',16,1)
				   RETURN
			   END
           ELSE IF (ISNULL(@charItemType,'') = '0')
			   BEGIN
				   RAISERROR('ITEM_TYPE_NOT_SELECTED',16,1)
				   RETURN
			   END
           ELSE
		   BEGIN 
		      If (@charItemType = 'P')
			  BEGIN
				IF ((SELECT COUNT(*) FROM Chart_Of_Accounts WHERE numDomainId=@numDomainID AND numAccountId=@numAssetChartAcntId) = 0)
				BEGIN
					RAISERROR('INVALID_ASSET_ACCOUNT',16,1)
					RETURN
				END
				
			  END

				-- This is common check for CharItemType P and Other
				IF ((SELECT COUNT(*) FROM Chart_Of_Accounts WHERE numDomainId=@numDomainID AND numAccountId=@numCOGSChartAcntId) = 0)
				BEGIN
					RAISERROR('INVALID_COGS_ACCOUNT',16,1)
					RETURN
				END

				IF ((SELECT COUNT(*) FROM Chart_Of_Accounts WHERE numDomainId=@numDomainID AND numAccountId=@numIncomeChartAcntId) = 0)
				BEGIN
					RAISERROR('INVALID_INCOME_ACCOUNT',16,1)
					RETURN
				END
		   END
		END
	--INSERT CUSTOM FIELDS BASE ON WAREHOUSEITEMS
	DECLARE @hDoc AS INT     
	                                                                        	
	DECLARE @TempTable TABLE 
	(
		ID INT IDENTITY(1,1),
		Fld_ID NUMERIC(18,0),
		Fld_Value NUMERIC(18,0)
	)   

	IF ISNULL(@bitMatrix,0) = 1 AND ISNULL(@numItemGroup,0) = 0
	BEGIN
		RAISERROR('ITEM_GROUP_NOT_SELECTED',16,1)
		RETURN
	END
	ELSE IF ISNULL(@bitMatrix,0) = 1 AND LEN(ISNULL(@vcItemAttributes,'')) = 0 
	BEGIN
		RAISERROR('ITEM_ATTRIBUTES_NOT_SELECTED',16,1)
		RETURN
	END
	ELSE IF ISNULL(@bitMatrix,0) = 1 
	BEGIN
		DECLARE @bitDuplicate AS BIT = 0
	
		IF DATALENGTH(ISNULL(@vcItemAttributes,''))>2
		BEGIN
			EXEC sp_xml_preparedocument @hDoc OUTPUT, @vcItemAttributes
	                                      
			INSERT INTO @TempTable 
			(
				Fld_ID,
				Fld_Value
			)    
			SELECT
				*          
			FROM 
				OPENXML (@hDoc,'/NewDataSet/CusFlds',2)                                                  
			WITH 
				(Fld_ID NUMERIC(18,0),Fld_Value NUMERIC(18,0)) 
			ORDER BY 
				Fld_ID       

			DECLARE @strAttribute VARCHAR(500) = ''
			DECLARE @i AS INT = 1
			DECLARE @COUNT AS INT 
			DECLARE @numFldID AS NUMERIC(18,0)
			DECLARE @numFldValue AS NUMERIC(18,0)
			SELECT @COUNT = COUNT(*) FROM @TempTable

			WHILE @i <= @COUNT
			BEGIN
				SELECT @numFldID=Fld_ID,@numFldValue=Fld_Value FROM @TempTable WHERE ID=@i

				IF ISNUMERIC(@numFldValue)=1
				BEGIN
					SELECT @strAttribute=@strAttribute+Fld_label FROM CFW_Fld_Master WHERE Fld_id=@numFldID AND Grp_id = 9
                
					IF LEN(@strAttribute) > 0
					BEGIN
						SELECT @strAttribute=@strAttribute+':'+ vcData +',' FROM ListDetails WHERE numListItemID=@numFldValue
					END
				END

				SET @i = @i + 1
			END

			SET @strAttribute = SUBSTRING(@strAttribute,0,LEN(@strAttribute))

			IF
				(SELECT
					COUNT(*)
				FROM
					Item
				INNER JOIN
					ItemAttributes
				ON
					Item.numItemCode = ItemAttributes.numItemCode
					AND ItemAttributes.numDomainID = @numDomainID                     
				WHERE
					Item.numItemCode <> @numItemCode
					AND Item.vcItemName = @vcItemName
					AND Item.numItemGroup = @numItemGroup
					AND Item.numDomainID = @numDomainID
					AND dbo.fn_GetItemAttributes(@numDomainID,Item.numItemCode) = @strAttribute
				) > 0
			BEGIN
				RAISERROR('ITEM_WITH_ATTRIBUTES_ALREADY_EXISTS',16,1)
				RETURN
			END

			If ISNULL(@numItemCode,0) > 0 AND dbo.fn_GetItemAttributes(@numDomainID,@numItemCode) = @strAttribute
			BEGIN
				SET @bitAttributesChanged = 0
			END

			EXEC sp_xml_removedocument @hDoc 
		END	
	END
	ELSE IF ISNULL(@bitMatrix,0) = 0
	BEGIN
		-- REMOVE ATTRIBUTES CONFIGURATION OF ITEM AND WAREHOUSE
		DELETE FROM ItemAttributes WHERE numDomainID=@numDomainID AND numItemCode=@numItemCode
	END 
	
	IF ISNULL(@numItemGroup,0) > 0
	BEGIN
		IF (SELECT COUNT(*) FROM ItemGroupsDTL WHERE numItemGroupID = @numItemGroup AND tintType=2) = 0
		BEGIN
			RAISERROR('ITEM_GROUP_NOT_CONFIGURED',16,1)
			RETURN
		END
		ELSE IF EXISTS (SELECT 
							CFM.Fld_id
						FROM 
							CFW_Fld_Master CFM
						LEFT JOIN
							ListDetails LD
						ON
							CFM.numlistid = LD.numListID
						WHERE
							CFM.numDomainID = @numDomainID
							AND CFM.Fld_id IN (SELECT numOppAccAttrID FROM ItemGroupsDTL WHERE numItemGroupID=@numItemGroup AND tintType = 2)
						GROUP BY
							CFM.Fld_id HAVING COUNT(LD.numListItemID) = 0)
		BEGIN
			RAISERROR('ITEM_GROUP_NOT_CONFIGURED',16,1)
			RETURN
		END
	END

	IF @numCOGSChartAcntId=0 SET @numCOGSChartAcntId=NULL
	IF @numAssetChartAcntId=0 SET @numAssetChartAcntId=NULL  
	IF @numIncomeChartAcntId=0 SET @numIncomeChartAcntId=NULL     

	DECLARE @ParentSKU VARCHAR(50) = @vcSKU                        
	DECLARE @ItemID AS NUMERIC(9)
	DECLARE @cnt AS INT

	IF ISNULL(@charItemType,'') = '' OR ISNULL(@charItemType,'') = '0'  
		SET @charItemType = 'N'

	IF @intWebApiId = 2 AND LEN(ISNULL(@vcApiItemId, '')) > 0 AND ISNULL(@numItemGroup, 0) > 0 
    BEGIN 
        -- check wether this id already Mapped in ITemAPI 
        SELECT 
			@cnt = COUNT([numItemID])
        FROM 
			[ItemAPI]
        WHERE 
			[WebApiId] = @intWebApiId
            AND [numDomainId] = @numDomainID
            AND [vcAPIItemID] = @vcApiItemId

        IF @cnt > 0 
        BEGIN
            SELECT 
				@ItemID = [numItemID]
            FROM 
				[ItemAPI]
            WHERE 
				[WebApiId] = @intWebApiId
                AND [numDomainId] = @numDomainID
                AND [vcAPIItemID] = @vcApiItemId
        
			--Update Existing Product coming from API
            SET @numItemCode = @ItemID
        END
    END
	ELSE IF @intWebApiId > 1 AND LEN(ISNULL(@vcSKU, '')) > 0 
    BEGIN
        SET @ParentSKU = @vcSKU
		 
        SELECT 
			@cnt = COUNT([numItemCode])
        FROM 
			[Item]
        WHERE 
			[numDomainId] = @numDomainID
            AND (vcSKU = @vcSKU OR @vcSKU IN (SELECT vcWHSKU FROM dbo.WareHouseItems WHERE numItemID = Item.[numItemCode] AND vcWHSKU = @vcSKU))
            
		IF @cnt > 0 
        BEGIN
            SELECT 
				@ItemID = [numItemCode],
                @ParentSKU = vcSKU
            FROM 
				[Item]
            WHERE 
				[numDomainId] = @numDomainID
                AND (vcSKU = @vcSKU OR @vcSKU IN (SELECT vcWHSKU FROM dbo.WareHouseItems WHERE numItemID = Item.[numItemCode] AND vcWHSKU = @vcSKU))

            SET @numItemCode = @ItemID
        END
		ELSE 
		BEGIN
			-- check wether this id already Mapped in ITemAPI 
			SELECT 
				@cnt = COUNT([numItemID])
			FROM 
				[ItemAPI]
			WHERE 
				[WebApiId] = @intWebApiId
				AND [numDomainId] = @numDomainID
				AND [vcAPIItemID] = @vcApiItemId
                    
			IF @cnt > 0 
			BEGIN
				SELECT 
					@ItemID = [numItemID]
				FROM 
					[ItemAPI]
				WHERE 
					[WebApiId] = @intWebApiId
					AND [numDomainId] = @numDomainID
					AND [vcAPIItemID] = @vcApiItemId
        
				--Update Existing Product coming from API
				SET @numItemCode = @ItemID
			END
		END
    END
                                                         
	IF @numItemCode=0 OR @numItemCode IS NULL
	BEGIN
		INSERT INTO Item 
		(                                             
			vcItemName,txtItemDesc,charItemType,monListPrice,numItemClassification,bitTaxable,vcSKU,bitKitParent,
			numVendorID,numDomainID,numCreatedBy,bintCreatedDate,bintModifiedDate,numModifiedBy,bitSerialized,
			vcModelID,numItemGroup,numCOGsChartAcntId,numAssetChartAcntId,numIncomeChartAcntId,monAverageCost,                          
			monCampaignLabourCost,fltWeight,fltHeight,fltWidth,fltLength,bitFreeShipping,bitAllowBackOrder,vcUnitofMeasure,
			bitShowDeptItem,bitShowDeptItemDesc,bitCalAmtBasedonDepItems,bitAssembly,numBarCodeId,vcManufacturer,numBaseUnit,
			numPurchaseUnit,numSaleUnit,bitLotNo,IsArchieve,numItemClass,tintStandardProductIDType,vcExportToAPI,numShipClass,
			bitAllowDropShip,bitArchiveItem,bitAsset,bitRental,bitVirtualInventory,bitContainer,numContainer,numNoItemIntoContainer,bitMatrix,numManufacturer
		)                                                              
		VALUES                                                                
		(                                                                
			@vcItemName,@txtItemDesc,@charItemType,@monListPrice,@numItemClassification,@bitTaxable,@ParentSKU,@bitKitParent,
			@numVendorID,@numDomainID,@numUserCntID,getutcdate(),getutcdate(),@numUserCntID,@bitSerialized,@vcModelID,@numItemGroup,                                      
			@numCOGsChartAcntId,@numAssetChartAcntId,@numIncomeChartAcntId,@monAverageCost,@monLabourCost,@fltWeight,@fltHeight,@fltWidth,                  
			@fltLength,@bitFreeshipping,@bitAllowBackOrder,@UnitofMeasure,@bitShowDeptItem,@bitShowDeptItemDesc,@bitCalAmtBasedonDepItems,    
			@bitAssembly,@numBarCodeId,@vcManufacturer,@numBaseUnit,@numPurchaseUnit,@numSaleUnit,@bitLotNo,@IsArchieve,@numItemClass,
			@tintStandardProductIDType,@vcExportToAPI,@numShipClass,@bitAllowDropShip,@bitArchiveItem ,@bitAsset,@bitRental,
			@bitVirtualInventory,@bitContainer,@numContainer,@numNoItemIntoContainer,@bitMatrix,@numManufacturer
		)                                             
 
		SET @numItemCode = SCOPE_IDENTITY()
  
		IF  @intWebApiId > 1 
		BEGIN
			 -- insert new product
			 --insert this id into linking table
			 INSERT INTO [ItemAPI] (
	 			[WebApiId],
	 			[numDomainId],
	 			[numItemID],
	 			[vcAPIItemID],
	 			[numCreatedby],
	 			[dtCreated],
	 			[numModifiedby],
	 			[dtModified],vcSKU
			 ) VALUES     
			 (
				@intWebApiId,
				@numDomainID,
				@numItemCode,
				@vcApiItemId,
	 			@numUserCntID,
	 			GETUTCDATE(),
	 			@numUserCntID,
	 			GETUTCDATE(),@vcSKU
	 		) 
		END
 
		IF @charItemType = 'P' AND 1 = (CASE WHEN ISNULL(@numItemGroup,0) > 0 THEN (CASE WHEN ISNULL(@bitMatrix,0)=1 THEN 1 ELSE 0 END) ELSE 1 END)
		BEGIN
			
			DECLARE @TEMPWarehouse TABLE
			(
				ID INT IDENTITY(1,1)
				,numWarehouseID NUMERIC(18,0)
			)
		
			INSERT INTO @TEMPWarehouse (numWarehouseID) SELECT numWareHouseID FROM Warehouses WHERE numDomainID=@numDomainID

			DECLARE @j AS INT = 1
			DECLARE @jCount AS INT
			DECLARE @numTempWarehouseID NUMERIC(18,0)
			SELECT @jCount = COUNT(*) FROM @TEMPWarehouse

			WHILE @j <= @jCount
			BEGIN
				SELECT @numTempWarehouseID=numWarehouseID FROM @TEMPWarehouse WHERE ID=@j

				EXEC USP_WarehouseItems_Save @numDomainID,
											@numUserCntID,
											0,
											@numTempWarehouseID,
											0,
											@numItemCode,
											'',
											@monListPrice,
											0,
											0,
											'',
											'',
											'',
											'',
											0
 
				SET @j = @j + 1
			END
		END 
	END         
	ELSE IF @numItemCode>0 AND @intWebApiId > 0 
	BEGIN 
		IF @ProcedureCallFlag = 1 
		BEGIN
			DECLARE @ExportToAPIList AS VARCHAR(30)
			SELECT @ExportToAPIList = vcExportToAPI FROM dbo.Item WHERE numItemCode = @numItemCode

			IF @ExportToAPIList != ''
			BEGIN
				IF NOT EXISTS(select * from (SELECT * FROM dbo.Split(@ExportToAPIList ,','))as A where items= @vcExportToAPI ) 
				BEGIN
					SET @vcExportToAPI = @ExportToAPIList + ',' + @vcExportToAPI
				End
				ELSE
				BEGIN
					SET @vcExportToAPI = @ExportToAPIList 
				End
			END

			--update ExportToAPI String value
			UPDATE dbo.Item SET vcExportToAPI=@vcExportToAPI where numItemCode=@numItemCode  AND numDomainID=@numDomainID
					
			SELECT 
				@cnt = COUNT([numItemID]) 
			FROM 
				[ItemAPI] 
			WHERE  
				[WebApiId] = @intWebApiId 
				AND [numDomainId] = @numDomainID
				AND [vcAPIItemID] = @vcApiItemId
				AND numItemID = @numItemCode

			IF @cnt > 0
			BEGIN
				--update lastUpdated Date 
				UPDATE [ItemAPI] SET [dtModified] =GETUTCDATE() WHERE [WebApiId] =@intWebApiId AND [numDomainId] = @numDomainID
			END
			ELSE
			--Insert ItemAPI mapping
			BEGIN
				INSERT INTO [ItemAPI] 
				(
					[WebApiId],
					[numDomainId],
					[numItemID],
					[vcAPIItemID],
					[numCreatedby],
					[dtCreated],
					[numModifiedby],
					[dtModified],vcSKU
				) VALUES 
				(
					@intWebApiId,
					@numDomainID,
					@numItemCode,
					@vcApiItemId,
					@numUserCntID,
					GETUTCDATE(),
					@numUserCntID,
					GETUTCDATE(),@vcSKU
				)	
			END
		END   
	END                                            
	ELSE IF @numItemCode > 0 AND @intWebApiId <= 0                                                           
	BEGIN
		DECLARE @OldGroupID NUMERIC 
		SELECT @OldGroupID = ISNULL(numItemGroup,0) FROM [Item] WHERE numitemcode = @numItemCode AND [numDomainId] = @numDomainID
	
		DECLARE @monOldAverageCost AS DECIMAL(18,4);SET @monOldAverageCost=0
		SELECT @monOldAverageCost=(CASE WHEN ISNULL(bitVirtualInventory,0) = 1 THEN 0 ELSE ISNULL(monAverageCost,0) END) FROM Item WHERE numItemCode =@numItemCode AND [numDomainId] = @numDomainID
	   
		UPDATE 
			Item 
		SET 
			vcItemName=@vcItemName,txtItemDesc=@txtItemDesc,charItemType=@charItemType,monListPrice= @monListPrice,numItemClassification=@numItemClassification,                                             
			bitTaxable=@bitTaxable,vcSKU = (CASE WHEN ISNULL(@ParentSKU,'') = '' THEN @vcSKU ELSE @ParentSKU END),bitKitParent=@bitKitParent,                                             
			numVendorID=@numVendorID,numDomainID=@numDomainID,bintModifiedDate=getutcdate(),numModifiedBy=@numUserCntID,bitSerialized=@bitSerialized,                                                         
			vcModelID=@vcModelID,numItemGroup=@numItemGroup,numCOGsChartAcntId=@numCOGsChartAcntId,numAssetChartAcntId=@numAssetChartAcntId,                                      
			numIncomeChartAcntId=@numIncomeChartAcntId,monCampaignLabourCost=@monLabourCost,fltWeight=@fltWeight,fltHeight=@fltHeight,fltWidth=@fltWidth,                  
			fltLength=@fltLength,bitFreeShipping=@bitFreeshipping,bitAllowBackOrder=@bitAllowBackOrder,vcUnitofMeasure=@UnitofMeasure,bitShowDeptItem=@bitShowDeptItem,            
			bitShowDeptItemDesc=@bitShowDeptItemDesc,bitCalAmtBasedonDepItems=@bitCalAmtBasedonDepItems,bitAssembly=@bitAssembly,numBarCodeId=@numBarCodeId,
			vcManufacturer=@vcManufacturer,numBaseUnit=@numBaseUnit,numPurchaseUnit=@numPurchaseUnit,numSaleUnit=@numSaleUnit,bitLotNo=@bitLotNo,
			IsArchieve = @IsArchieve,numItemClass=@numItemClass,tintStandardProductIDType=@tintStandardProductIDType,vcExportToAPI=@vcExportToAPI,
			numShipClass=@numShipClass,bitAllowDropShip=@bitAllowDropShip, bitArchiveItem = @bitArchiveItem,bitAsset=@bitAsset,bitRental=@bitRental,
			bitVirtualInventory = @bitVirtualInventory,numContainer=@numContainer,numNoItemIntoContainer=@numNoItemIntoContainer,bitMatrix=@bitMatrix,numManufacturer=@numManufacturer
		WHERE 
			numItemCode=@numItemCode 
			AND numDomainID=@numDomainID

		IF NOT EXISTS(SELECT numItemCode FROM dbo.OpportunityItems WHERE numItemCode = @numItemCode)
		BEGIN
			IF (((SELECT ISNULL(SUM(numOnHand),0) FROM WareHouseItems WHERE numItemID = @numItemCode AND numDomainId=@numDomainID) = 0) AND ((SELECT (CASE WHEN ISNULL(bitVirtualInventory,0) = 1 THEN 0 ELSE ISNULL(monAverageCost,0) END) FROM Item  WHERE numItemCode = @numItemCode AND numDomainId=@numDomainID ) = 0))
			BEGIN
				UPDATE Item SET monAverageCost=@monAverageCost  where numItemCode=@numItemCode  AND numDomainID=@numDomainID	
			END
		END
	
		--If Average Cost changed then add in Tracking
		IF ISNULL(@monAverageCost,0) != ISNULL(@monOldAverageCost,0)
		BEGIN
			DECLARE @numWareHouseItemID NUMERIC(9);SET @numWareHouseItemID=0
			DECLARE @numTotal int;SET @numTotal=0
			SET @i=0
			DECLARE @vcDescription AS VARCHAR(100);SET @vcDescription=''
		
			SELECT @numTotal=COUNT(*) FROM WareHouseItems where numItemID=@numItemCode AND numDomainId=@numDomainId
	     
			WHILE @i < @numTotal
			BEGIN
				SELECT @numWareHouseItemID = min(numWareHouseItemID) FROM WareHouseItems 
					WHERE numItemID=@numItemCode AND numDomainId=@numDomainId and numWareHouseItemID > @numWareHouseItemID 
				
				IF @numWareHouseItemID>0
				BEGIN
					SET @vcDescription='Average Cost Changed Manually OLD : ' + CAST(ISNULL(@monOldAverageCost,0) AS VARCHAR(20))
				
					DECLARE @numDomain AS NUMERIC(18,0)
					SET @numDomain = @numDomainID
					EXEC dbo.USP_ManageWareHouseItems_Tracking
						@numWareHouseItemID = @numWareHouseItemID, --  numeric(9, 0)		
						@numReferenceID = @numItemCode, --  numeric(9, 0)
						@tintRefType = 1, --  tinyint
						@vcDescription = @vcDescription, --  varchar(100)
						@tintMode = 0, --  tinyint
						@numModifiedBy = @numUserCntID, --  numeric(9, 0)
						@numDomainID = @numDomain
				END
		    
				SET @i = @i + 1
			END
		END
 
		DECLARE @bitCartFreeShipping as  bit 
		SELECT @bitCartFreeShipping = bitFreeShipping FROM dbo.CartItems where numItemCode = @numItemCode  AND numDomainID = @numDomainID 
 
		IF @bitCartFreeShipping <> @bitFreeshipping
		BEGIN
			UPDATE dbo.CartItems SET bitFreeShipping = @bitFreeshipping WHERE numItemCode = @numItemCode  AND numDomainID=@numDomainID
		END 
 
		IF @intWebApiId > 1 
		BEGIN
			SELECT 
				@cnt = COUNT([numItemID])
			FROM 
				[ItemAPI]
			WHERE 
				[WebApiId] = @intWebApiId
				AND [numDomainId] = @numDomainID
				AND [vcAPIItemID] = @vcApiItemId

			IF @cnt > 0
			BEGIN
				--update lastUpdated Date 
				UPDATE [ItemAPI] SET [dtModified] =GETUTCDATE() WHERE [WebApiId] =@intWebApiId AND [numDomainId] = @numDomainID
			END
			ELSE --Insert ItemAPI mapping
			BEGIN
				INSERT INTO [ItemAPI] 
				(
	 				[WebApiId],
	 				[numDomainId],
	 				[numItemID],
	 				[vcAPIItemID],
	 				[numCreatedby],
	 				[dtCreated],
	 				[numModifiedby],
	 				[dtModified],vcSKU
				 )
				 VALUES 
				 (
					@intWebApiId,
					@numDomainID,
					@numItemCode,
					@vcApiItemId,
	 				@numUserCntID,
	 				GETUTCDATE(),
	 				@numUserCntID,
	 				GETUTCDATE(),@vcSKU
	 			) 
			END
		END   
		                                                                            
		-- kishan It is necessary to add item into itemTax table . 
		IF	@bitTaxable = 1
		BEGIN
			IF NOT EXISTS(SELECT * FROM dbo.ItemTax WHERE numItemCode = @numItemCode AND numTaxItemID = 0)
			BEGIN
			    INSERT INTO dbo.ItemTax 
				(
					numItemCode,
					numTaxItemID,
					bitApplicable
				)
				VALUES 
				( 
					@numItemCode,
					0,
					1 
				) 						
			END
		END	 
      
		IF @charItemType='S' or @charItemType='N'                                                                       
		BEGIN
			DELETE FROM WareHouseItmsDTL where numWareHouseItemID in (select numWareHouseItemID from WareHouseItems where numItemID=@numItemCode AND [WareHouseItems].[numDomainID] = @numDomainID)
			DELETE FROM WareHouseItems where numItemID=@numItemCode AND [WareHouseItems].[numDomainID] = @numDomainID
		END                                      
   
		IF @bitKitParent=1 OR @bitAssembly = 1              
		BEGIN              
			DECLARE @hDoc1 AS INT                                            
			EXEC sp_xml_preparedocument @hDoc1 OUTPUT, @strChildItems                                                                        
                                             
			UPDATE 
				ItemDetails 
			SET
				numQtyItemsReq=X.QtyItemsReq
				,numWareHouseItemId=X.numWarehouseItmsID
				,numUOMId=X.numUOMId
				,vcItemDesc=X.vcItemDesc
				,sintOrder=X.sintOrder                                                                                          
			From 
			(
				SELECT 
					QtyItemsReq,ItemKitID,ChildItemID,numWarehouseItmsID,numUOMId,vcItemDesc,sintOrder, numItemDetailID As ItemDetailID                             
				FROM 
					OPENXML(@hDoc1,'/NewDataSet/Table1',2)                                                                         
				WITH
				(
					ItemKitID NUMERIC(9),                                                          
					ChildItemID NUMERIC(9),                                                                        
					QtyItemsReq DECIMAL(30, 16),
					numWarehouseItmsID NUMERIC(9),
					numUOMId NUMERIC(9),
					vcItemDesc VARCHAR(1000),
					sintOrder int,
					numItemDetailID NUMERIC(18,0)
				)
			)X                                                                         
			WHERE 
				numItemKitID=X.ItemKitID 
				AND numChildItemID=ChildItemID 
				AND numItemDetailID = X.ItemDetailID

			EXEC sp_xml_removedocument @hDoc1
		 END   
		 ELSE
		 BEGIN
 			DELETE FROM ItemDetails WHERE numItemKitID=@numItemCode
		 END                                                
	END

	IF ISNULL(@numItemGroup,0) = 0 OR ISNULL(@bitMatrix,0) = 1
	BEGIN
		UPDATE WareHouseItems SET monWListPrice = @monListPrice, vcWHSKU=@vcSKU, vcBarCode=@numBarCodeId WHERE numDomainID = @numDomainID AND numItemID = @numItemCode
	END

	IF @numItemCode > 0
	BEGIN
		DELETE 
			IC
		FROM 
			dbo.ItemCategory IC
		INNER JOIN 
			Category 
		ON 
			IC.numCategoryID = Category.numCategoryID
		WHERE 
			numItemID = @numItemCode		
			AND numCategoryProfileID = @numCategoryProfileID

		IF @vcCategories <> ''	
		BEGIN
			INSERT INTO ItemCategory( numItemID , numCategoryID )  SELECT  @numItemCode AS numItemID,ID from dbo.SplitIDs(@vcCategories,',')	
		END
	END

	


	-- SAVE MATRIX ITEM ATTRIBUTES
	IF @numItemCode > 0 AND ISNULL(@bitAttributesChanged,0)=1 AND ISNULL(@bitMatrix,0) = 1 AND LEN(ISNULL(@vcItemAttributes,'')) > 0 AND ISNULL(@bitOppOrderExists,0) <> 1 AND (SELECT COUNT(*) FROM @TempTable) > 0
	BEGIN
		-- DELETE PREVIOUS ITEM ATTRIBUTES
		DELETE FROM ItemAttributes WHERE numDomainID=@numDomainID AND numItemCode=@numItemCode

		-- INSERT NEW ITEM ATTRIBUTES
		INSERT INTO ItemAttributes
		(
			numDomainID
			,numItemCode
			,FLD_ID
			,FLD_Value
		)
		SELECT
			@numDomainID
			,@numItemCode
			,Fld_ID
			,Fld_Value
		FROM
			@TempTable

		IF (SELECT COUNT(*) FROM WarehouseItems WHERE numDomainID=@numDomainID AND numItemID=@numItemCode) > 0
		BEGIN

			-- DELETE PREVIOUS WAREHOUSE ATTRIBUTES
			DELETE FROM CFW_Fld_Values_Serialized_Items WHERE RecId IN (SELECT ISNULL(numWarehouseItemID,0) FROM WarehouseItems WHERE numDomainID=@numDomainID AND numItemID=@numItemCode)

			-- INSERT NEW WAREHOUSE ATTRIBUTES
			INSERT INTO CFW_Fld_Values_Serialized_Items
			(
				Fld_ID,
				Fld_Value,
				RecId,
				bitSerialized
			)                                                  
			SELECT 
				Fld_ID,
				ISNULL(CAST(Fld_Value AS VARCHAR),'') AS Fld_Value,
				TEMPWarehouse.numWarehouseItemID,
				0 
			FROM 
				@TempTable  
			OUTER APPLY
			(
				SELECT
					numWarehouseItemID
				FROM 
					WarehouseItems 
				WHERE 
					numDomainID=@numDomainID 
					AND numItemID=@numItemCode
			) AS TEMPWarehouse
		END
	END	  
COMMIT
END TRY
BEGIN CATCH
	DECLARE @ErrorMessage NVARCHAR(4000)
	DECLARE @ErrorNumber INT
	DECLARE @ErrorSeverity INT
	DECLARE @ErrorState INT
	DECLARE @ErrorLine INT
	DECLARE @ErrorProcedure NVARCHAR(200)

	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;

	SELECT 
		@ErrorMessage = ERROR_MESSAGE(),
		@ErrorNumber = ERROR_NUMBER(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE(),
		@ErrorLine = ERROR_LINE(),
		@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

	RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);
END CATCH        
END
--USP_GetPriceTable 360
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ManagePricingNamesTable')
DROP PROCEDURE USP_ManagePricingNamesTable
GO
CREATE PROCEDURE USP_ManagePricingNamesTable
    @numDomainID NUMERIC,
    @numCreatedBy AS NUMERIC,
    @strItems TEXT
AS 
BEGIN
	DELETE  FROM [PricingNamesTable] WHERE numDomainID= @numDomainID
		
    DECLARE @hDocItem INT

	IF CONVERT(VARCHAR(10), @strItems) <> '' 
    BEGIN
        EXEC sp_xml_preparedocument @hDocItem OUTPUT, @strItems
          
        INSERT  INTO [PricingNamesTable]
        (
            numDomainID,
            [numCreatedBy],
            [dtCreatedDate],
            [tintPriceLevel],
            [vcPriceLevelName]
        )
        SELECT  
			@numDomainID,
			@numCreatedBy,
			GETUTCDATE(),
			X.[tintPriceLevel],
			X.vcPriceLevelName
        FROM    
		( 
			SELECT * FROM 
			OPENXML (@hDocItem, '/NewDataSet/Item', 2)
            WITH ( tintPriceLevel TINYINT, vcPriceLevelName VARCHAR(300))
        ) X

        EXEC sp_xml_removedocument @hDocItem
    END

END
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ManagePricingTable')
DROP PROCEDURE USP_ManagePricingTable
GO
CREATE PROCEDURE USP_ManagePricingTable
    @numPriceRuleID NUMERIC,
    @numItemCode AS NUMERIC,
    @strItems TEXT
AS 
    BEGIN
		
        DELETE  FROM [PricingTable]
        WHERE   ISNULL([numPriceRuleID],0) = @numPriceRuleID AND ISNULL(numItemCode,0)=@numItemCode
		
        DECLARE @hDocItem INT
        IF CONVERT(VARCHAR(10), @strItems) <> '' 
            BEGIN
                EXEC sp_xml_preparedocument @hDocItem OUTPUT, @strItems
          
                INSERT  INTO [PricingTable]
                        (
                          numPriceRuleID,
                          [intFromQty],
                          [intToQty],
                          [tintRuleType],
                          [tintDiscountType],
                          [decDiscount],
						  numItemCode
						  --,vcName	
                        )
                        SELECT  X.numPriceRuleID,
                                X.[intFromQty],
                                X.[intToQty],
                                X.[tintRuleType],
                                CASE ISNULL(X.[tintDiscountType],0) WHEN 0 THEN 1 ELSE ISNULL(X.[tintDiscountType],0) END,
								CAST(CONVERT(DECIMAL(18, 4), X.[decDiscount]) AS VARCHAR) AS decDiscount,
								X.numItemCode
                        FROM    ( SELECT    *
                                  FROM      OPENXML (@hDocItem, '/NewDataSet/Item', 2)
                                            WITH ( numPriceRuleID NUMERIC, intFromQty INT, intToQty INT, tintRuleType TINYINT, tintDiscountType TINYINT, decDiscount DECIMAL(18,4),numItemCode NUMERIC, vcName VARCHAR(300))
                                ) X
                EXEC sp_xml_removedocument @hDocItem
            END
    END
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ManageTransactionHistory')
	DROP PROCEDURE USP_ManageTransactionHistory
GO

CREATE PROCEDURE [dbo].[USP_ManageTransactionHistory]
	@numTransHistoryID numeric(18, 0) OUTPUT,
	@numDomainID numeric(18, 0),
	@numDivisionID numeric(18, 0),
	@numContactID numeric(18, 0),
	@numOppID numeric(18, 0),
	@numOppBizDocsID numeric(18, 0),
	@vcTransactionID varchar(100),
	@tintTransactionStatus tinyint,
	@vcPGResponse varchar(200),
	@tintType tinyint,
	@monAuthorizedAmt money,
	@monCapturedAmt money,
	@monRefundAmt money,
	@vcCardHolder varchar(500),
	@vcCreditCardNo varchar(500),
	@vcCVV2 varchar(200),
	@tintValidMonth tinyint,
	@intValidYear int,
	@vcSignatureFile VARCHAR(100),
	@numUserCntID numeric(18, 0),
	@numCardType NUMERIC(9,0),
	@vcResponseCode VARCHAR(500) = ''
AS

 SET NOCOUNT ON
 SET TRANSACTION ISOLATION LEVEL READ COMMITTED


IF EXISTS(SELECT [numTransHistoryID] FROM [dbo].[TransactionHistory] WHERE [numTransHistoryID] = @numTransHistoryID)
BEGIN
	DECLARE @OldCapturedAmt AS MONEY
	DECLARE @OldRefundAmt AS MONEY
	DECLARE @OldAuthorizedAmt AS MONEY
	
	SELECT @OldAuthorizedAmt = ISNULL(monAuthorizedAmt,0), @OldCapturedAmt = ISNULL(monCapturedAmt,0),@OldRefundAmt = ISNULL(monRefundAmt,0) FROM [dbo].[TransactionHistory] WHERE [numTransHistoryID] = @numTransHistoryID
	
	IF @tintTransactionStatus =2 -- captured
	BEGIN
		SET @OldAuthorizedAmt = @monCapturedAmt
		SET @OldRefundAmt = ISNULL(@monRefundAmt,0) + @OldRefundAmt
	END
	ELSE IF @tintTransactionStatus =5 -- Credited
	BEGIN
		SET @OldRefundAmt = ISNULL(@monRefundAmt,0) + @OldRefundAmt
	END
	
	UPDATE [dbo].[TransactionHistory] SET
		[tintTransactionStatus] = @tintTransactionStatus,
		[monAuthorizedAmt] = @OldAuthorizedAmt,
		[monCapturedAmt] = @OldCapturedAmt,
		[monRefundAmt] = @OldRefundAmt,
		
		[numModifiedBy] = @numUserCntID,
		[dtModifiedDate] = GETUTCDATE()
	WHERE
		[numTransHistoryID] = @numTransHistoryID AND numDomainID=@numDomainID
END
ELSE
BEGIN
	INSERT INTO [dbo].[TransactionHistory] (
		[numDomainID],
		[numDivisionID],
		[numContactID],
		[numOppID],
		[numOppBizDocsID],
		[vcTransactionID],
		[tintTransactionStatus],
		[vcPGResponse],
		[tintType],
		[monAuthorizedAmt],
		[monCapturedAmt],
		[monRefundAmt],
		[vcCardHolder],
		[vcCreditCardNo],
		[vcCVV2],
		[tintValidMonth],
		[intValidYear],
		vcSignatureFile,
		[numCreatedBy],
		[dtCreatedDate],
		[numModifiedBy],
		[dtModifiedDate],
		[numCardType],
		[vcResponseCode]
	) VALUES (
		@numDomainID,
		@numDivisionID,
		@numContactID,
		@numOppID,
		@numOppBizDocsID,
		@vcTransactionID,
		@tintTransactionStatus,
		@vcPGResponse,
		@tintType,
		@monAuthorizedAmt,
		@monCapturedAmt,
		@monRefundAmt,
		@vcCardHolder,
		@vcCreditCardNo,
		@vcCVV2,
		@tintValidMonth,
		@intValidYear,
		@vcSignatureFile,
		@numUserCntID,
		GETUTCDATE(),
		@numUserCntID,
		GETUTCDATE(),
		@numCardType,
		@vcResponseCode
	)
	
	SET @numTransHistoryID = @@IDENTITY
END
/****** Object:  StoredProcedure [dbo].[USP_OppBizDocs]    Script Date: 03/25/2009 15:23:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj                    
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_oppbizdocs')
DROP PROCEDURE usp_oppbizdocs
GO
CREATE PROCEDURE [dbo].[USP_OppBizDocs]
(                        
 @byteMode as tinyint=null,                        
 @numOppId as numeric(9)=null,                        
 @numOppBizDocsId as numeric(9)=null,
 @ClientTimeZoneOffset AS INT=0,
 @numDomainID AS NUMERIC(18,0	) = NULL,
 @numUserCntID AS NUMERIC(18,0) = NULL
)                        
as                        
          
SELECT 
	ISNULL(CompanyInfo.numCompanyType,0) AS numCompanyType,
	ISNULL(CompanyInfo.vcProfile,0) AS vcProfile,
	ISNULL(OpportunityMaster.numAccountClass,0) AS numAccountClass
FROM 
	OpportunityMaster 
JOIN 
	DivisionMaster 
ON 
	DivisionMaster.numDivisionId = OpportunityMaster.numDivisionId
JOIN 
	CompanyInfo
ON 
	CompanyInfo.numCompanyId = DivisionMaster.numCompanyID
WHERE 
	numOppId = @numOppId
		                                  
if @byteMode= 1                        
begin 
BEGIN TRY
BEGIN TRANSACTION
DECLARE @numBizDocID AS INT
DECLARE @bitFulfilled AS BIT
Declare @tintOppType as tinyint
DECLARE @tintShipped AS BIT
DECLARE @bitAuthoritativeBizDoc AS BIT

SELECT @tintOppType=tintOppType,@tintShipped=ISNULL(tintshipped,0) FROM OpportunityMaster WHERE  numOppId=@numOppId  and numDomainID= @numDomainID 
SELECT @numBizDocID = numBizDocId, @bitFulfilled=bitFulfilled, @bitAuthoritativeBizDoc=ISNULL(bitAuthoritativeBizDocs,0) FROM OpportunityBizDocs WHERE numOppBizDocsId = @numOppBizDocsId

IF ISNULL(@tintShipped,0) = 1 AND (@numBizDocID=296 OR ISNULL(@bitAuthoritativeBizDoc,0)=1)
BEGIN
	RAISERROR('NOT_ALLOWED_TO_DELETE_AUTHORITATIVE_BIZDOC_AFTER_ORDER_IS_CLOSED',16,1)
	RETURN
END

--IF it's as sales order and BizDoc is of type of fulfillment and already fulfilled revert items to allocations
IF @tintOppType = 1 AND @numBizDocID = 296 AND @bitFulfilled = 1
BEGIN
	EXEC USP_OpportunityBizDocs_RevertFulFillment @numDomainID,@numUserCntID,@numOppId,@numOppBizDocsId
END


DELETE FROM [ProjectsOpportunities] WHERE numOppBizDocID=@numOppBizDocsId

DELETE FROM [ShippingReportItems] WHERE [numShippingReportId] IN  (SELECT numShippingReportId from [ShippingReport] WHERE [numOppBizDocId] = @numOppBizDocsId)
DELETE FROM [ShippingBox] WHERE [numShippingReportId] IN  (SELECT numShippingReportId from [ShippingReport] WHERE [numOppBizDocId] = @numOppBizDocsId)
DELETE FROM [ShippingReport] WHERE [numOppBizDocId] = @numOppBizDocsId

 --Delete All entries for current bizdoc
EXEC [USP_DeleteEmbeddedCost] @numOppBizDocsId, 0, @numDomainID 
--DELETE FROM [OpportunityBizDocsPaymentDetails] WHERE [numBizDocsPaymentDetId] IN (SELECT  [numBizDocsPaymentDetId] FROM [OpportunityBizDocsDetails] WHERE [numBizDocsPaymentDetId] IN (SELECT [numBizDocsPaymentDetId] FROM [EmbeddedCost] WHERE [numOppBizDocID]=@numOppBizDocsId))
--DELETE FROM [EmbeddedCostItems] WHERE [numEmbeddedCostID] IN (SELECT [numEmbeddedCostID] FROM [EmbeddedCost] WHERE [numOppBizDocID] = @numOppBizDocsId)
--DELETE FROM [EmbeddedCost] WHERE [numOppBizDocID] = @numOppBizDocsId
--DELETE FROM [OpportunityBizDocsDetails] WHERE [numBizDocsPaymentDetId] IN (SELECT [numBizDocsPaymentDetId] FROM [EmbeddedCost] WHERE [numOppBizDocID]=@numOppBizDocsId)


delete  from OpportunityBizDocItems where   numOppBizDocID=  @numOppBizDocsId              

--Delete Deferred BizDoc Recurring Entries
delete from OpportunityRecurring where numOppBizDocID=@numOppBizDocsId and tintRecurringType=4

UPDATE dbo.OpportunityRecurring SET numOppBizDocID=NULL WHERE numOppBizDocID=@numOppBizDocsId

DELETE FROM [RecurringTransactionReport] WHERE [numRecTranBizDocID] = @numOppBizDocsId

--Credit Balance
Declare @monCreditAmount as money;Set @monCreditAmount=0
Declare @numDivisionID as numeric(9);Set @numDivisionID=0

Select @monCreditAmount=isnull(oppBD.monCreditAmount,0),@numDivisionID=Om.numDivisionID,
@numOppId=OM.numOppId 
from OpportunityBizDocs oppBD join OpportunityMaster Om on OM.numOppId=oppBD.numOppId WHERE  numOppBizDocsId = @numOppBizDocsId

delete from [CreditBalanceHistory] where numOppBizDocsId=@numOppBizDocsId

 
IF @tintOppType=1 --Sales
	update DivisionMaster set monSCreditBalance=isnull(monSCreditBalance,0) + isnull(@monCreditAmount,0) where numDivisionID=@numDivisionID and numDomainID=@numDomainID
else IF @tintOppType=2 --Purchase
	update DivisionMaster set monPCreditBalance=isnull(monPCreditBalance,0) + isnull(@monCreditAmount,0) where numDivisionID=@numDivisionID and numDomainID=@numDomainID


delete from DocumentWorkflow where numDocID=@numOppBizDocsId and cDocType='B'      

delete from BizDocAction where numBizActionId in(select numBizActionId from BizActionDetails where numOppBizDocsId =@numOppBizDocsId and btDocType=1) 
delete from BizActionDetails where numOppBizDocsId =@numOppBizDocsId and btDocType=1

delete from OpportunityBizDocs where numOppBizDocsId=@numOppBizDocsId                        

COMMIT TRANSACTION
END TRY
BEGIN CATCH
	DECLARE @ErrorMessage NVARCHAR(4000)
	DECLARE @ErrorNumber INT
	DECLARE @ErrorSeverity INT
	DECLARE @ErrorState INT
	DECLARE @ErrorLine INT
	DECLARE @ErrorProcedure NVARCHAR(200)

	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION

	SELECT 
		@ErrorMessage = ERROR_MESSAGE(),
		@ErrorNumber = ERROR_NUMBER(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE(),
		@ErrorLine = ERROR_LINE(),
		@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

	RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);
END CATCH             
end                        
                        
if @byteMode= 2                        
begin  

DECLARE @BaseCurrencySymbol nVARCHAR(3);SET @BaseCurrencySymbol='$'
SELECT @BaseCurrencySymbol = ISNULL(varCurrSymbol,'$') FROM dbo.Currency 
														   WHERE numCurrencyID IN ( SELECT numCurrencyID FROM dbo.Domain 
																					WHERE numDomainId=@numDomainId)
--PRINT @BaseCurrencySymbol

																					                      
select *, isnull(X.monPAmount,0)-X.monAmountPaid as BalDue from (select  opp.numOppBizDocsId,                        
 opp.numOppId,                        
 opp.numBizDocId,                        
 ISNULL((SELECT TOP 1 numOrientation FROM BizDocTemplate WHERE numBizDocTempID = Opp.numBizDocTempID),0) AS numOrientation, 
 ISNULL((SELECT TOP 1 bitKeepFooterBottom FROM BizDocTemplate WHERE numBizDocTempID = Opp.numBizDocTempID),0) AS bitKeepFooterBottom,                  
 oppmst.vcPOppName,                        
 mst.vcData as BizDoc,                        
 opp.numCreatedBy,                        
DATEADD(minute,-@ClientTimeZoneOffset,opp.dtCreatedDate) CreatedDate,
 opp.vcRefOrderNo,                       
 vcBizDocID,
 CASE WHEN LEN(ISNULL(vcBizDocName,''))=0 THEN vcBizDocID ELSE vcBizDocName END vcBizDocName,      
  [dbo].[GetDealAmount](@numOppId ,getutcdate(),opp.numOppBizDocsId ) as monPAmount,
  CASE 
	WHEN tintOppType=1  
	THEN (CASE 
			WHEN (ISNULL(numAuthoritativeSales,0)=numBizDocId OR numBizDocId=296) 
			THEN 'Authoritative' + CASE WHEN ISNULL(Opp.tintDeferred,0)=1 THEN ' (Deferred)' ELSE '' END 
			WHEN opp.numBizDocId =  304 THEN 'Deferred-Authoritative'
			ELSE 'Non-Authoritative' END ) 
  when tintOppType=2  then (case when isnull(numAuthoritativePurchase,0)=numBizDocId then 'Authoritative' + Case when isnull(Opp.tintDeferred,0)=1 then ' (Deferred)' else '' end else 'Non-Authoritative' end )  end as BizDocType,
  isnull(monAmountPaid,0) monAmountPaid, 
  ISNULL(C.varCurrSymbol,'') varCurrSymbol ,
  ISNULL(Opp.[bitAuthoritativeBizDocs],0) bitAuthoritativeBizDocs,
  ISNULL(oppmst.[tintshipped] ,0) tintshipped,isnull(opp.numShipVia,0) as numShipVia,oppmst.numDivisionId,isnull(Opp.tintDeferred,0) as tintDeferred,
  ISNULL(Opp.numBizDocTempID,0) AS numBizDocTempID,con.numContactid,isnull(con.vcEmail,'') AS vcEmail,ISNULL(Opp.numBizDocStatus,0) AS numBizDocStatus,
--  case when isnumeric(ISNULL(dbo.fn_GetListItemName(isnull(oppmst.intBillingDays,0)),0))=1 
--	   then ISNULL(dbo.fn_GetListItemName(isnull(oppmst.intBillingDays,0)),0) 
--	   else 0 
--  end AS numBillingDaysName,
  CASE WHEN ISNUMERIC((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(oppmst.intBillingDays,0))) = 1
 	   THEN (SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(oppmst.intBillingDays,0))
	   ELSE 0
  END AS numBillingDaysName,
  Opp.dtFromDate,
  opp.dtFromDate AS BillingDate,
  ISNULL(C.fltExchangeRate,1) fltExchangeRateCurrent,
  ISNULL(oppmst.fltExchangeRate,1) fltExchangeRateOfOrder,
  @BaseCurrencySymbol BaseCurrencySymbol,0 AS numReturnHeaderID,0 AS tintReturnType,ISNULL(Opp.fltExchangeRateBizDoc,ISNULL(oppmst.fltExchangeRate,1)) AS fltExchangeRateBizDoc,
  CASE WHEN (SELECT COUNT(*) FROM dbo.ShippingReport WHERE numOppBizDocId = opp.numOppBizDocsId AND numDomainID = @numDomainID) > 0 THEN 1 ELSE 0 END AS [IsShippingReportGenerated],
  --CASE WHEN (SELECT ISNULL(vcTrackingNo,'') FROM dbo.OpportunityBizDocs WHERE numOppBizDocsId = opp.numOppBizDocsId) <> '' THEN 1 ELSE 0 END AS [ISTrackingNumGenerated]
  CASE WHEN (SELECT COUNT(*) FROM dbo.ShippingBox 
			 WHERE numShippingReportId IN (SELECT numShippingReportId FROM dbo.ShippingReport WHERE numOppBizDocId = opp.numOppBizDocsId AND numDomainID = @numDomainID)
			 AND LEN(ISNULL(vcTrackingNumber,'')) > 0) > 0 THEN 1 ELSE 0 END AS [ISTrackingNumGenerated],
  ISNULL((SELECT MAX(numShippingReportId) from ShippingReport SR 
		  WHERE SR.numOppBizDocId = opp.numoppbizdocsid 
		  AND SR.numDomainID = oppmst.numDomainID),0) AS numShippingReportId,
  ISNULL((SELECT MAX(SB.numShippingReportId) FROM [ShippingBox] SB 
		  JOIN dbo.ShippingReport SR ON SB.numShippingReportId = SR.numShippingReportId 
		  where SR.numOppBizDocId = opp.numoppbizdocsid AND SR.numDomainID = oppmst.numDomainID 
		  AND LEN(ISNULL(SB.vcTrackingNumber,''))>0),0) AS numShippingLabelReportId			 
 ,ISNULL(STUFF(( SELECT  ',' +  CASE WHEN LEN(ISNULL(W.[vcWareHouse],'')  + ':' + ISNULL([WL].[vcLocation],'')) > 0 THEN ISNULL(W.[vcWareHouse],'')  + ':' + ISNULL([WL].[vcLocation],'') ELSE '' END
                FROM    [dbo].[WarehouseLocation] AS WL
				JOIN [dbo].[Warehouses] W ON [WL].[numWarehouseID] = [W].[numWareHouseID]
                WHERE  [WL].[numWLocationID] IN (
                        SELECT  WHI.[numWLocationID]
                        FROM    [dbo].[WareHouseItems] AS WHI
                                JOIN [dbo].[OpportunityItems] AS OI ON [WHI].[numWareHouseItemID] = [OI].[numWarehouseItmsID]
                        WHERE   [OI].[numOppId] = oppmst.[numOppId])
              FOR
                XML PATH('')
              ), 1, 1, ''),'') AS [vcLocation],
		(CASE WHEN ISNULL(RecurrenceConfiguration.numRecConfigID,0) = 0 THEN 0 ELSE 1 END) AS bitRecur,
		ISNULL(Opp.bitRecurred,0) AS bitRecurred,
		ISNULL((SELECT COUNT(*) FROM OpportunityBizDocs WHERE numOppID=@numOppID AND ISNULL(numDeferredBizDocID,0) = ISNULL(opp.numOppBizDocsId,0)),0) AS intInvoiceForDiferredBizDoc,
		ISNULL(Opp.bitShippingGenerated,0) AS bitShippingGenerated,
		ISNULL(oppmst.intUsedShippingCompany,0) as intUsedShippingCompany
 FROM                         
 OpportunityBizDocs  opp                        
 left join ListDetails mst                        
 on mst.numListItemID=opp.numBizDocId                        
 join OpportunityMaster oppmst                        
 on oppmst.numOppId= opp.numOppId                                
 left join  AuthoritativeBizDocs  AB
 on AB.numDomainId=  oppmst.numDomainID             
 LEFT OUTER JOIN [Currency] C
  ON C.numCurrencyID = oppmst.numCurrencyID
  LEFT JOIN AdditionalContactsInformation con ON con.numContactid = oppmst.numContactId
  LEFT JOIN
	RecurrenceConfiguration
  ON
	RecurrenceConfiguration.numOppBizDocID = opp.numOppBizDocsId
where opp.numOppId=@numOppId

UNION ALL

select  0 AS numOppBizDocsId,                        
 RH.numOppId,                        
 0 AS numBizDocId,                        
 1 AS numOrientation,  
 0 AS bitKeepFooterBottom,                   
 '' AS vcPOppName,                        
 'RMA' as BizDoc,                        
 RH.numCreatedBy,                        
DATEADD(minute,-@ClientTimeZoneOffset,RH.dtCreatedDate) CreatedDate,
 '' vcRefOrderNo,                       
 RH.vcRMA AS vcBizDocID,
 RH.vcRMA AS vcBizDocName,      
  ISNULL(monAmount,0) as monPAmount,
  'RMA' as BizDocType,
  0 AS monAmountPaid, 
  '' varCurrSymbol ,
  0 bitAuthoritativeBizDocs,
  0 tintshipped,0 as numShipVia,RH.numDivisionId,0 as tintDeferred,
  0 AS numBizDocTempID,con.numContactid,isnull(con.vcEmail,'') AS vcEmail,0 AS numBizDocStatus,
  0 AS numBillingDaysName,RH.dtCreatedDate AS dtFromDate,
  RH.dtCreatedDate AS BillingDate,
  1 fltExchangeRateCurrent,
  1 fltExchangeRateOfOrder,
  @BaseCurrencySymbol BaseCurrencySymbol,RH.numReturnHeaderID,RH.tintReturnType,1 AS fltExchangeRateBizDoc
  ,0 [IsShippingReportGenerated],0 [ISTrackingNumGenerated],
  0 AS numShippingReportId,
  0 AS numShippingLabelReportId			 
  ,ISNULL(STUFF(( SELECT  ',' +  CASE WHEN LEN(ISNULL(W.[vcWareHouse],'')  + ':' + ISNULL([WL].[vcLocation],'')) > 0 THEN ISNULL(W.[vcWareHouse],'')  + ':' + ISNULL([WL].[vcLocation],'') ELSE '' END
                FROM    [dbo].[WarehouseLocation] AS WL
				JOIN [dbo].[Warehouses] W ON [WL].[numWarehouseID] = [W].[numWareHouseID]
                WHERE  [WL].[numWLocationID] IN (
                        SELECT  WHI.[numWLocationID]
                        FROM    [dbo].[WareHouseItems] AS WHI
                                JOIN [dbo].[OpportunityItems] AS OI ON [WHI].[numWareHouseItemID] = [OI].[numWarehouseItmsID]
                        WHERE   [OI].[numOppId] = RH.[numOppId])
              FOR
                XML PATH('')
              ), 1, 1, ''),'') AS [vcLocation],
			  0 AS bitRecur,
			  0 AS bitRecurred,
		0 AS intInvoiceForDiferredBizDoc ,
		0 AS bitShippingGenerated,
		0 as intUsedShippingCompany
from                         
 ReturnHeader RH                        
 LEFT JOIN AdditionalContactsInformation con ON con.numContactid = RH.numContactId
  
where RH.numOppId=@numOppId
)X         
                        
END




GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_OPPGetINItems')
DROP PROCEDURE USP_OPPGetINItems
GO
-- =============================================  
-- Modified by: <Author,,Sachin Sadhu>  
-- Create date: <Create Date,,24thDec2013>  
-- Description: <Description,,add 3 fields:vcBizDocImagePath,vcBizDocFooter,vcPurBizDocFooter>  
-- =============================================  
CREATE PROCEDURE [dbo].[USP_OPPGetINItems]
(
          @byteMode        AS TINYINT  = NULL,
          @numOppId        AS NUMERIC(9)  = NULL,
          @numOppBizDocsId AS NUMERIC(9)  = NULL,
          @numDomainID     AS NUMERIC(9)  = 0,
          @numUserCntID    AS NUMERIC(9)  = 0,
          @ClientTimeZoneOffset INT=0)
AS
  IF @byteMode = 1
    BEGIN
      DECLARE  @BizDcocName  AS VARCHAR(100)
      DECLARE  @OppName  AS VARCHAR(100)
      DECLARE  @Contactid  AS VARCHAR(100)
      DECLARE  @shipAmount  AS MONEY
      DECLARE  @tintOppType  AS TINYINT
      DECLARE  @numlistitemid  AS VARCHAR(15)
      DECLARE  @RecOwner  AS VARCHAR(15)
      DECLARE  @MonAmount  AS VARCHAR(15)
      DECLARE  @OppBizDocID  AS VARCHAR(100)
      DECLARE  @bizdocOwner  AS VARCHAR(15)
      DECLARE  @tintBillType  AS TINYINT
      DECLARE  @tintShipType  AS TINYINT
      DECLARE  @numCustomerDivID AS NUMERIC 
      
      SELECT @RecOwner = numRecOwner,@MonAmount = CONVERT(VARCHAR(20),monPAmount),
             @Contactid = numContactId,@OppName = vcPOppName,
             @tintOppType = tintOppType,@tintBillType = tintBillToType,@tintShipType = tintShipToType
      FROM   OpportunityMaster WHERE  numOppId = @numOppId
      
      -- When Creating PO from SO and Bill type is Customer selected 
      SELECT @numCustomerDivID = ISNULL(numDivisionID,0) FROM dbo.OpportunityMaster WHERE 
			numOppID IN (SELECT  ISNULL(numParentOppID,0) FROM dbo.OpportunityLinking WHERE numChildOppID = @numOppId)
			
      --select @BizDcocName=vcBizDocID from OpportunityBizDocs where numOppBizDocsId=@numOppBizDocsId
      SELECT @BizDcocName = vcdata,
             @shipAmount = monShipCost,
             @numlistitemid = numlistitemid,
             @OppBizDocID = vcBizDocID,
             @bizdocOwner = OpportunityBizDocs.numCreatedBy
      FROM   listdetails JOIN OpportunityBizDocs ON numBizDocId = numlistitemid
      WHERE  numOppBizDocsId = @numOppBizDocsId
      
      SELECT @bizdocOwner AS BizDocOwner,
             @OppBizDocID AS BizDocID,
             @MonAmount AS Amount,
             isnull(@RecOwner,1) AS Owner,
             @BizDcocName AS BizDcocName,
             @numlistitemid AS BizDoc,
             @OppName AS OppName,
             VcCompanyName AS CompName,
             @shipAmount AS ShipAmount,
             dbo.fn_getOPPAddress(@numOppId,@numDomainID,1) AS BillAdd,
             dbo.fn_getOPPAddress(@numOppId,@numDomainID,2) AS ShipAdd,
             AD.VcStreet,
             AD.VcCity,
             isnull(dbo.fn_GetState(AD.numState),'') AS vcBilState,
             AD.vcPostalCode,
             isnull(dbo.fn_GetListItemName(AD.numCountry),
                    '') AS vcBillCountry,
             con.vcFirstName AS ConName,
             ISNULL(con.numPhone,'') numPhone,
             con.vcFax,
             isnull(con.vcEmail,'') AS vcEmail,
             div.numDivisionID,
             numContactid,
             isnull(vcFirstFldValue,'') vcFirstFldValue,
             isnull(vcSecndFldValue,'') vcSecndFldValue,
             isnull(bitTest,0) AS bitTest,
			dbo.fn_getOPPAddress(@numOppId,@numDomainID,1) AS BillToAddressName,
			dbo.fn_getOPPAddress(@numOppId,@numDomainID,1) AS ShipToAddressName,
             CASE 
               WHEN @tintBillType IS NULL THEN Com.vcCompanyName
             	-- When Sales order and bill to is set to customer 
               WHEN @tintBillType = 1 AND @tintOppType = 1 THEN Com.vcCompanyName
             -- When Create PO from SO and Bill to is set to Customer 
			   WHEN @tintBillType = 1 AND @tintOppType = 2 THEN dbo.fn_GetComapnyName(@numCustomerDivID)
               WHEN @tintBillType = 0 THEN (SELECT TOP 1 Com1.vcCompanyName
                                            FROM   companyinfo [Com1]
                                                   JOIN divisionmaster div1
                                                     ON com1.numCompanyID = div1.numCompanyID
                                                   JOIN Domain D1
                                                     ON D1.numDivisionID = div1.numDivisionID
                                                    JOIN dbo.AddressDetails AD1
														ON AD1.numDomainID = div1.numDomainID AND AD1.numRecordID = div1.numDivisionID AND tintAddressOf = 2 AND tintAddressType = 1 AND bitIsPrimary=1
                                            WHERE  D1.numDomainID = @numDomainID)
               WHEN @tintBillType = 2 THEN (SELECT vcBillCompanyName
                                            FROM   OpportunityAddress
                                            WHERE  numOppID = @numOppId)
				WHEN @tintBillType = 3 THEN  Com.vcCompanyName
              END AS BillToCompanyName,
             CASE 
               WHEN @tintShipType IS NULL THEN Com.vcCompanyName
               WHEN @tintShipType = 1 AND @tintOppType = 1 THEN Com.vcCompanyName
				-- When Create PO from SO and Ship to is set to Customer 
   			   WHEN @tintShipType = 1 AND @tintOppType = 2 THEN dbo.fn_GetComapnyName(@numCustomerDivID)
               WHEN @tintShipType = 0 THEN (SELECT TOP 1 Com1.vcCompanyName
                                            FROM   companyinfo [Com1]
                                                   JOIN divisionmaster div1
                                                     ON com1.numCompanyID = div1.numCompanyID
                                                   JOIN Domain D1
                                                     ON D1.numDivisionID = div1.numDivisionID
                                                    JOIN dbo.AddressDetails AD1
													 ON AD1.numDomainID = div1.numDomainID AND AD1.numRecordID = div1.numDivisionID AND tintAddressOf = 2 AND tintAddressType = 2 AND bitIsPrimary=1
                                            WHERE  D1.numDomainID = @numDomainID)
               WHEN @tintShipType = 2 THEN (SELECT vcShipCompanyName
                                            FROM   OpportunityAddress
                                            WHERE  numOppID = @numOppId)
				WHEN @tintShipType = 3 THEN (SELECT vcShipCompanyName
														FROM   OpportunityAddress
														WHERE  numOppID = @numOppId)
             END AS ShipToCompanyName
      FROM   companyinfo [Com] JOIN divisionmaster div ON com.numCompanyID = div.numCompanyID
             JOIN AdditionalContactsInformation con ON con.numdivisionId = div.numdivisionId
             JOIN Domain D ON D.numDomainID = div.numDomainID
             LEFT JOIN PaymentGatewayDTLID PGDTL ON PGDTL.intPaymentGateWay = D.intPaymentGateWay AND D.numDomainID = PGDTL.numDomainID
             LEFT JOIN AddressDetails AD --Billing add
				ON AD.numDomainID=DIV.numDomainID AND AD.numRecordID=div.numDivisionID AND AD.tintAddressOf = 2 AND AD.tintAddressType = 1 AND AD.bitIsPrimary=1
			 LEFT JOIN AddressDetails AD2--Shipping address
				ON AD2.numDomainID=DIV.numDomainID AND AD2.numRecordID=div.numDivisionID AND AD2.tintAddressOf = 2 AND AD2.tintAddressType = 2 AND AD2.bitIsPrimary=1	
      WHERE  numContactid = @Contactid AND Div.numDomainID = @numDomainID
    END
  IF @byteMode = 2
    BEGIN
      SELECT opp.vcBizDocID,
             Mst.fltDiscount AS decDiscount,
             isnull(opp.monAmountPaid,0) AS monAmountPaid,
             isnull(opp.vcComments,'') AS vcComments,
             CONVERT(VARCHAR(20),DATEADD(minute,-@ClientTimeZoneOffset,opp.dtCreatedDate)) dtCreatedDate,
             dbo.fn_GetContactName(opp.numCreatedby) AS numCreatedby,
             dbo.fn_GetContactName(opp.numModifiedBy) AS numModifiedBy,
             CONVERT(VARCHAR(20),DATEADD(minute,-@ClientTimeZoneOffset,opp.dtModifiedDate)) dtModifiedDate,
             CASE 
               WHEN isnull(opp.numViewedBy,0) = 0 THEN ''
               ELSE dbo.fn_GetContactName(opp.numViewedBy)
             END AS numViewedBy,
             CASE 
               WHEN opp.numViewedBy = 0 THEN NULL
               ELSE opp.dtViewedDate
             END AS dtViewedDate,
             (CASE WHEN isnull(Mst.intBillingDays,0) > 0 THEN CAST(1 AS BIT) ELSE isnull(Mst.bitBillingTerms,0) END) AS tintBillingTerms,
             isnull(Mst.intBillingDays,0) AS numBillingDays,
			 ISNULL(BTR.numNetDueInDays,0) AS numBillingDaysName,
			 BTR.vcTerms AS vcBillingTermsName,
             isnull(Mst.bitInterestType,0) AS tintInterestType,
             isnull(Mst.fltInterest,0) AS fltInterest,
             tintOPPType,
             Opp.numBizDocId,
             dbo.fn_GetContactName(numApprovedBy) AS ApprovedBy,
             dtApprovedDate,
             Mst.bintAccountClosingDate,
             tintShipToType,
             tintBillToType,
             tintshipped,
             dtShippedDate bintShippedDate,
             isnull(opp.monShipCost,0) AS monShipCost,
             (SELECT COUNT(* )
              FROM   DocumentWorkflow
              WHERE  numDocID = @numOppBizDocsId
                     AND numContactID = @numUserCntID
                     AND cDocType = 'B'
                     AND tintApprove = 0) AS AppReq,
             (SELECT COUNT(* )
              FROM   DocumentWorkflow
              WHERE  numDocID = @numOppBizDocsId
                     AND cDocType = 'B'
                     AND tintApprove = 1) AS Approved,
             (SELECT COUNT(* )
              FROM   DocumentWorkflow
              WHERE  numDocID = @numOppBizDocsId
                     AND cDocType = 'B'
                     AND tintApprove = 2) AS Declined,
             (SELECT COUNT(* )
              FROM   DocumentWorkflow
              WHERE  numDocID = @numOppBizDocsId
                     AND cDocType = 'B'
                     AND tintApprove = 0) AS Pending,
             ISNULL(BT.vcBizDocFooter,D.vcBizDocFooter) AS BizdocFooter,
             ISNULL(BT.vcBizDocImagePath,D.vcBizDocImagePath) As vcBizDocImagePath, 
             Mst.numDivisionID,
             ISNULL(BT.vcPurBizDocFooter,D.vcPurBizDocFooter) AS PurBizdocFooter, 
             isnull(Mst.fltDiscount,0) AS fltDiscount,
             isnull(Mst.bitDiscountType,0) AS bitDiscountType,
             isnull(Opp.numShipVia,ISNULL(Mst.numShipmentMethod,0)) AS numShipVia,
--             isnull(vcTrackingURL,'') AS vcTrackingURL,
			ISNULL( (SELECT vcShipFieldValue FROM dbo.ShippingFieldValues WHERE numDomainID=MSt.numDomainID AND intShipFieldID IN ( SELECT intShipFieldID FROM dbo.ShippingFields WHERE numListItemID=Opp.numShipVia  AND vcFieldName='Tracking URL')),'')		AS vcTrackingURL,
             isnull(Opp.numBizDocStatus,0) AS numBizDocStatus,
             CASE 
               WHEN Opp.numBizDocStatus IS NULL THEN '-'
               ELSE dbo.fn_GetListItemName(Opp.numBizDocStatus)
             END AS BizDocStatus,
             CASE 
               WHEN ISNULL(Opp.numShipVia,0) = 0 THEN (CASE WHEN Mst.numShipmentMethod IS NULL THEN '-' WHEN Mst.numShipmentMethod = -1 THEN 'Will-call' ELSE dbo.fn_GetListItemName(Mst.numShipmentMethod) END)
			   WHEN Opp.numShipVia = -1 THEN 'Will-call'
               ELSE dbo.fn_GetListItemName(Opp.numShipVia)
             END AS ShipVia,
             ISNULL(C.varCurrSymbol,'') varCurrSymbol,
             (SELECT COUNT(*) FROM [ShippingReport] WHERE numOppBizDocId = opp.numOppBizDocsId )AS ShippingReportCount,
			 isnull(bitPartialFulfilment,0) bitPartialFulfilment,
			 ISNULL(Opp.vcBizDocName,'') vcBizDocName,
			 dbo.[fn_GetContactName](opp.[numModifiedBy])  + ', '
				+ CONVERT(VARCHAR(20),dateadd(MINUTE,-@ClientTimeZoneOffset,Opp.dtModifiedDate)) AS ModifiedBy,
			 dbo.[fn_GetContactName](Mst.[numRecOwner]) + ', ' +  dbo.[fn_GetContactName](Mst.[numAssignedTo]) AS OrderRecOwner,
			 dbo.[fn_GetContactName](DM.[numRecOwner]) + ', ' +  dbo.[fn_GetContactName](DM.[numAssignedTo]) AS AccountRecOwner,
			 Opp.dtFromDate,
			 Mst.tintTaxOperator,
			 dbo.GetAccountedEmbeddedCost(opp.numOppBizDocsID,1) AS monTotalEmbeddedCost,
			 dbo.GetAccountedEmbeddedCost(opp.numOppBizDocsID,2) AS monTotalAccountedCost,
			 ISNULL(BT.bitEnabled,0) bitEnabled,
			 ISNULL(BT.txtBizDocTemplate,'') txtBizDocTemplate, 
			 ISNULL(BT.txtCSS,'') txtCSS,
			 ISNULL(BT.numOrientation,0) numOrientation,
			 ISNULL(BT.bitKeepFooterBottom,0) bitKeepFooterBottom,
			 dbo.fn_GetContactName(Mst.numAssignedTo) AS AssigneeName,
			 ISNULL((SELECT vcEmail FROM dbo.AdditionalContactsInformation WHERE numContactID = Mst.numAssignedTo),'') AS AssigneeEmail,
			 ISNULL((SELECT numPhone FROM dbo.AdditionalContactsInformation WHERE numContactID = Mst.numAssignedTo),'') AS AssigneePhone,
			 dbo.fn_GetComapnyName(Mst.numDivisionId) OrganizationName,
			 dbo.fn_GetContactName(Mst.numContactID)  OrgContactName,

--CASE WHEN Mst.tintOppType = 1 THEN 
--dbo.getCompanyAddress((select ISNULL(DM.numDivisionID,0)  as numDivisionID from AdditionalContactsInformation AC INNER JOIN DivisionMaster DM 
--on AC.numDivisionID = DM.numDivisionID where numContactID = Mst.numCreatedBy),1,Mst.numDomainId)
--               ELSE dbo.getCompanyAddress(Mst.numDivisionId,1,Mst.numDomainId)
--             END AS CompanyBillingAddress1,

CASE WHEN Mst.tintOppType = 1 THEN 
dbo.getCompanyAddress((select ISNULL(AC.numDivisionID,0)  as numDivisionID from AdditionalContactsInformation AC
INNER JOIN Domain D ON D.numDomainID = AC.numDomainID where AC.numContactID = D.numAdminID AND D.numDomainID = Mst.numDomainId),1,Mst.numDomainId)
               ELSE dbo.getCompanyAddress(Mst.numDivisionId,1,Mst.numDomainId)
             END AS CompanyBillingAddress,

CASE WHEN Mst.tintOppType = 1 THEN 
dbo.getCompanyAddress((select ISNULL(AC.numDivisionID,0)  as numDivisionID from AdditionalContactsInformation AC
INNER JOIN Domain D ON D.numDomainID = AC.numDomainID where AC.numContactID = D.numAdminID AND D.numDomainID = Mst.numDomainId),1,Mst.numDomainId)
               ELSE dbo.getCompanyAddress(Mst.numDivisionId,2,Mst.numDomainId)
             END AS CompanyShippingAddress,
--
--			 dbo.getCompanyAddress(Mst.numDivisionId,1,Mst.numDomainId) CompanyBillingAddress,
--			 dbo.getCompanyAddress(Mst.numDivisionId,2,Mst.numDomainId) CompanyShippingAddress,
			 case when ACI.numPhone<>'' then ACI.numPhone +case when ACI.numPhoneExtension<>'' then ' - ' + ACI.numPhoneExtension else '' end  else '' END OrgContactPhone,
			 DM.vcComPhone as OrganizationPhone,
             ISNULL(ACI.vcEmail,'') AS OrgContactEmail,
 			 dbo.[fn_GetContactName](Mst.[numRecOwner]) AS OnlyOrderRecOwner,
 			 ISNULL(Opp.bitAuthoritativeBizDocs,0) bitAuthoritativeBizDocs,
			 ISNULL(Opp.tintDeferred,0) tintDeferred,isnull(Opp.monCreditAmount,0) AS monCreditAmount,
			 isnull(Opp.bitRentalBizDoc,0) as bitRentalBizDoc,isnull(BT.numBizDocTempID,0) as numBizDocTempID,isnull(BT.vcTemplateName,'') as vcTemplateName,
		     [dbo].[GetDealAmount](opp.numOppId ,getutcdate(),opp.numOppBizDocsId ) as monPAmount,
			 --(select top 1 SD.vcSignatureFile from SignatureDetail SD join OpportunityBizDocsDetails OBD on SD.numSignID=OBD.numSignID where SD.numDomainID = @numDomainID and OBD.numDomainID = @numDomainID and OBD.numSignID IS not null and OBD.numBizDocsId=opp.numOppBizDocsId order by OBD.numBizDocsPaymentDetId desc) as vcSignatureFile,
			 isnull(CMP.txtComments,'') as vcOrganizationComments,
			 isnull(Opp.vcTrackingNo,'') AS  vcTrackingNo,isnull(Opp.vcShippingMethod,'') AS  vcShippingMethod,
			 Opp.dtDeliveryDate,
			 CASE WHEN Opp.vcRefOrderNo IS NULL OR LEN(Opp.vcRefOrderNo)=0 THEN ISNULL(Mst.vcOppRefOrderNo,'') ELSE 
			  isnull(Opp.vcRefOrderNo,'') END AS vcRefOrderNo,ISNULL(Mst.numDiscountAcntType,0) AS numDiscountAcntType,
			 ISNULL(opp.numSequenceId,'') AS numSequenceId,
			 CASE WHEN ISNULL(Opp.numBizDocId,0)=296 THEN ISNULL((SELECT SUBSTRING((SELECT '$^$' + dbo.fn_GetListItemName(numBizDocStatus) +'#^#'+ dbo.fn_GetContactName(numUserCntID) +'#^#'+ dbo.FormatedDateTimeFromDate(DateAdd(minute, -@ClientTimeZoneOffset , dtCreatedDate),@numDomainId) + '#^#'+ isnull(DFCS.vcColorScheme,'NoForeColor')
					FROM OppFulfillmentBizDocsStatusHistory OFBS left join DycFieldColorScheme DFCS on OFBS.numBizDocStatus=DFCS.vcFieldValue AND DFCS.numDomainID=@numDomainID
					WHERE numOppId=opp.numOppId and numOppBizDocsId=opp.numOppBizDocsId FOR XML PATH('')),4,200000)),'') ELSE '' END AS vcBizDocsStatusList,ISNULL(Mst.numCurrencyID,0) AS numCurrencyID,
			ISNULL(Opp.fltExchangeRateBizDoc,Mst.fltExchangeRate) AS fltExchangeRateBizDoc,
			com1.vcCompanyName AS EmployerOrganizationName,div1.vcComPhone as EmployerOrganizationPhone,ISNULL(opp.bitAutoCreated,0) AS bitAutoCreated,
			ISNULL(RecurrenceConfiguration.numRecConfigID,0) AS numRecConfigID,
			(CASE WHEN RecurrenceConfiguration.numRecConfigID IS NULL THEN 0 ELSE 1 END) AS bitRecur,
			(CASE WHEN RecurrenceTransaction.numRecTranID IS NULL THEN 0 ELSE 1 END) AS bitRecurred,
			ISNULL(Mst.bitRecurred,0) AS bitOrderRecurred,
			dbo.FormatedDateFromDate(RecurrenceConfiguration.dtStartDate,@numDomainID) AS dtStartDate,
			RecurrenceConfiguration.vcFrequency AS vcFrequency,
			ISNULL(RecurrenceConfiguration.bitDisabled,0) AS bitDisabled,
			ISNULL(opp.bitFulfilled,0) AS bitFulfilled,
			ISNULL(opp.numDeferredBizDocID,0) AS numDeferredBizDocID,
			dbo.GetTotalQtyByUOM(opp.numOppBizDocsId) AS vcTotalQtybyUOM,
			ISNULL(Mst.intUsedShippingCompany,0) AS intUsedShippingCompany,
			dbo.FormatedDateFromDate(Mst.dtReleaseDate,@numDomainID) AS vcReleaseDate,
			(CASE WHEN ISNULL(Mst.numPartner,0) > 0 THEN ISNULL((SELECT CONCAT(DivisionMaster.vcPartnerCode,'-',CompanyInfo.vcCompanyName) FROM DivisionMaster INNER JOIN CompanyInfo ON DivisionMaster.numCompanyID=CompanyInfo.numCompanyId WHERE DivisionMaster.numDivisionID=Mst.numPartner),'') ELSE '' END) AS vcPartner
			,dbo.FormatedDateFromDate(mst.dtReleaseDate,@numDomainID) AS dtReleaseDate,
			(select TOP 1 vcBizDocID from OpportunityBizDocs where numOppId=opp.numOppId and numBizDocId=296) as vcFulFillment,
			(select TOP 1 vcPOppName from OpportunityMaster WHERE numOppId=(select TOP 1 numParentOppID from OpportunityLinking where numChildOppId=opp.numOppId)) AS vcPOName
		FROM   OpportunityBizDocs opp JOIN OpportunityMaster Mst ON Mst.numOppId = opp.numOppId
             JOIN Domain D ON D.numDomainID = Mst.numDomainID
             LEFT OUTER JOIN [Currency] C ON C.numCurrencyID = Mst.numCurrencyID
             LEFT JOIN [DivisionMaster] DM ON DM.numDivisionID = Mst.numDivisionID   
			 LEFT JOIN CompanyInfo CMP ON DM.numCompanyId = CMP.numCompanyId and CMP.numDomainID = @numDomainID  
             LEFT JOIN dbo.AdditionalContactsInformation ACI ON ACI.numContactID = Mst.numContactID
             LEFT JOIN [BizDocTemplate] BT ON BT.numDomainID = @numDomainID AND BT.numBizDocID = Opp.numBizDocID AND BT.numOppType = Mst.tintOppType AND BT.tintTemplateType=0 and BT.numBizDocTempID=opp.numBizDocTempID
             LEFT JOIN BillingTerms BTR ON BTR.numTermsID = ISNULL(Mst.intBillingDays,0)
             JOIN divisionmaster div1 ON Mst.numDivisionID = div1.numDivisionID
			 JOIN companyinfo [Com1] ON com1.numCompanyID = div1.numCompanyID
			 LEFT JOIN RecurrenceConfiguration ON Opp.numOppBizDocsId = RecurrenceConfiguration.numOppBizDocID
			 LEFT JOIN RecurrenceTransaction ON Opp.numOppBizDocsId = RecurrenceTransaction.numRecurrOppBizDocID
      WHERE  opp.numOppBizDocsId = @numOppBizDocsId
             AND Mst.numDomainID = @numDomainID
    END
/****** Object:  StoredProcedure [dbo].[USP_ShoppingCart]    Script Date: 05/07/2009 22:09:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj                  
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_shoppingcart')
DROP PROCEDURE usp_shoppingcart
GO
CREATE PROCEDURE [dbo].[USP_ShoppingCart]                            
@numDivID as numeric(18),                            
@numOppID as numeric(18)=null output ,                                              
@vcPOppName as varchar(200)='' output,  
@numContactId as numeric(18),                            
@numDomainId as numeric(18),
@vcSource VARCHAR(50)=NULL,
@numCampainID NUMERIC(9)=0,
@numCurrencyID as numeric(9),
@numBillAddressId numeric(9),
@numShipAddressId numeric(9),
@bitDiscountType BIT,
@fltDiscount FLOAT,
@numProId NUMERIC(9),
@numSiteID NUMERIC(9),
@tintOppStatus INT,
@monShipCost MONEY,
@tintSource AS BIGINT ,
@tintSourceType AS TINYINT ,                          
@numPaymentMethodId NUMERIC(9),
@txtComments varchar(1000),
@numPartner NUMERIC(18,0),
@txtFuturePassword VARCHAR(50) = NULL,
@intUsedShippingCompany NUMERIC(18,0)=0,
@vcPartenerContact VARCHAR(200)='',
@numShipmentMethod NUMERIC(18,0)=0
as                            
BEGIN TRY
BEGIN TRANSACTION          
declare @CRMType as integer               
declare @numRecOwner as integer       
DECLARE @numPartenerContact NUMERIC(18,0)=0
	
SET @numPartenerContact=(SELECT Top 1 numContactId FROM AdditionalContactsInformation WHERE numDomainId=@numDomainID AND numDivisionId=@numPartner AND vcEmail=@vcPartenerContact)
	
if @numOppID=  0                             
begin                            
select @CRMType=tintCRMType,@numRecOwner=numRecOwner from DivisionMaster where numDivisionID=@numDivID                            
if @CRMType= 0                             
begin                            
UPDATE DivisionMaster                               
   SET tintCRMType=1                              
   WHERE numDivisionID=@numDivID                              
		
end                            
declare @TotalAmount as FLOAT
--declare @intOppTcode as numeric(9) 
--declare @numCurrencyID as numeric(9)
--declare @fltExchangeRate as float                          
--Select @intOppTcode=max(numOppId)from OpportunityMaster                              
--set @intOppTcode =isnull(@intOppTcode,0) + 1        

		IF LEN(ISNULL(@txtFuturePassword,'')) > 0
		BEGIN
			DECLARE @numExtranetID NUMERIC(18,0)

			SELECT @numExtranetID=numExtranetID FROM ExtarnetAccounts WHERE numDivisionID=@numDivID

			UPDATE ExtranetAccountsDtl SET vcPassword=@txtFuturePassword WHERE numExtranetID=@numExtranetID
		END                      

		DECLARE @numTemplateID NUMERIC
		SELECT  @numTemplateID= numMirrorBizDocTemplateId FROM eCommercePaymentConfig WHERE  numSiteId=@numSiteID AND numPaymentMethodId = @numPaymentMethodId AND numDomainID = @numDomainID   

		Declare @fltExchangeRate float                                 
    
   IF @numCurrencyID=0 select @numCurrencyID=isnull(numCurrencyID,0) from Domain where numDomainID=@numDomainId                            
   if @numCurrencyID=0 set   @fltExchangeRate=1
  else set @fltExchangeRate=dbo.GetExchangeRate(@numDomainID,@numCurrencyID)                          
			                       
                            
		-- GET ACCOUNT CLASS IF ENABLED
		DECLARE @numAccountClass AS NUMERIC(18) = 0
	-- GET ACCOUNT CLASS IF ENABLED
		DECLARE @tintDefaultClassType AS INT = 0
		SELECT @tintDefaultClassType = ISNULL(tintDefaultClassType,0) FROM Domain WHERE numDomainId = @numDomainID 

	IF @tintDefaultClassType <> 0
		BEGIN
			SELECT 
				@numAccountClass=ISNULL(numDefaultClass,0) 
			FROM 
				dbo.UserMaster UM 
			JOIN 
				dbo.Domain D 
			ON 
				UM.numDomainID=D.numDomainId
			WHERE 
				D.numDomainId=@numDomainId 
				AND UM.numUserDetailId=@numContactId
		END
		ELSE IF @tintDefaultClassType = 2 --COMPANY
		BEGIN
			SELECT 
				@numAccountClass=ISNULL(numAccountClassID,0) 
			FROM 
				dbo.DivisionMaster DM 
			WHERE 
				DM.numDomainId=@numDomainId 
				AND DM.numDivisionID=@numDivID
		END
		ELSE
		BEGIN
			SET @numAccountClass = 0
		END
		SET @numAccountClass=(SELECT 
								  TOP 1 
								  numDefaultClass from dbo.eCommerceDTL 
								  where 
								  numSiteId=@numSiteID 
								  and numDomainId=@numDomainId)                  
--set @vcPOppName=@vcPOppName+ '-'+convert(varchar(4),year(getutcdate()))  +'-A'+ convert(varchar(10),@intOppTcode)                              
  --SELECT tintSource , tintSourceType FROM dbo.OpportunityMaster
		insert into OpportunityMaster                              
		  (                              
		  numContactId,                              
		  numDivisionId,                              
		  txtComments,                              
		  numCampainID,                              
		  bitPublicFlag,                              
		  tintSource, 
		  tintSourceType ,                               
		  vcPOppName,                              
		  monPAmount,                              
		  numCreatedBy,                              
		  bintCreatedDate,                               
		  numModifiedBy,                              
		  bintModifiedDate,                              
		  numDomainId,                               
		  tintOppType, 
		  tintOppStatus,                   
		  intPEstimatedCloseDate,              
		  numRecOwner,
		  bitOrder,
		  numCurrencyID,
		  fltExchangeRate,fltDiscountTotal,bitDiscountTypeTotal,
		  monShipCost,
  numOppBizDocTempID,numAccountClass,numPartner,intUsedShippingCompany,numPartenerContact,numShipmentMethod,dtReleaseDate
		  )                              
		 Values                              
		  (                              
		  @numContactId,                              
		  @numDivID,                              
		  @txtComments,                              
		  @numCampainID,--  0,
		  0,                              
		  @tintSource,   
		  @tintSourceType,                           
		  ISNULL(@vcPOppName,'SO'),                             
		  0,                                
		  @numContactId,                              
		  getutcdate(),                              
		  @numContactId,                              
		  getutcdate(),        
		  @numDomainId,                              
		  1,             
		  @tintOppStatus,       
		  getutcdate(),                                 
		  @numRecOwner ,
		  1,
		  @numCurrencyID,
		  @fltExchangeRate,@fltDiscount,@bitDiscountType
		  ,@monShipCost,
  @numTemplateID,@numAccountClass,@numPartner,@intUsedShippingCompany,@numPartenerContact,@numShipmentMethod,GETDATE()
  
		  )        
set @numOppID=scope_identity()                             
--Update OppName as per Name Template
		EXEC dbo.USP_UpdateNameTemplateValue 1,@numDomainId,@numOppID
		SELECT @vcPOppName = vcPOppName FROM OpportunityMaster WHERE numOppID= @numOppID
end
delete  from    OpportunityItems where   numOppId= @numOppID                               

--if convert(varchar(10),@strItems) <>''                
--begin               
-- DECLARE @hDocItem int                
-- EXEC sp_xml_preparedocument @hDocItem OUTPUT, @strItems                

	UPDATE 
		P
	SET
		intCouponCodeUsed=ISNULL(intCouponCodeUsed,0)+1
	FROM 
		PromotionOffer AS P
	LEFT JOIN
		CartItems AS C
	ON
		P.numProId=C.PromotionID
	WHERE
	C.vcCoupon=P.txtCouponCode AND 
	(C.fltDiscount>0 OR C.numCartId IN (SELECT numCartId FROM CartItems AS CG WHERE CG.PromotionID=P.numProId AND CG.fltDiscount>0))
		AND numUserCntId =@numContactId


insert into OpportunityItems                                                        
  (numOppId,numItemCode,numUnitHour,monPrice,monTotAmount,vcItemDesc,numWarehouseItmsID,vcType,vcAttributes,vcAttrValues,[vcItemName],[vcModelID],[vcManufacturer],vcPathForTImage, monVendorCost,numUOMId,bitDiscountType,fltDiscount,monTotAmtBefDiscount,numPromotionID,bitPromotionTriggered)                                                        
  --kishan                                                       
 select @numOppID,X.numItemCode,x.numUnitHour * x.decUOMConversionFactor,x.monPrice/x.decUOMConversionFactor,x.monTotAmount,X.vcItemDesc, NULLIF(X.numWarehouseItmsID,0),X.vcItemType,X.vcAttributes,X.vcAttrValues,(SELECT vcItemName FROM item WHERE numItemCode = X.numItemCode),(SELECT vcModelID FROM item WHERE numItemCode = X.numItemCode),(SELECT vcManufacturer FROM item WHERE numItemCode = X.numItemCode),
		(SELECT TOP 1  vcPathForTImage FROM dbo.ItemImages WHERE numItemCode = X.numItemCode AND bitDefault = 1 AND numDomainId = @numDomainId),   (select isnull(VN.monCost,0) from Vendor VN INNER JOIN Item IT ON IT.numItemCode=VN.numItemCode and VN.numVendorID=IT.numVendorID where VN.numItemCode=X.numItemCode), 
		numUOM,bitDiscountType,fltDiscount,monTotAmtBefDiscount,X.PromotionID,X.bitParentPromotion
FROM dbo.CartItems X WHERE numUserCntId =@numContactId

	declare @tintShipped as tinyint      
	DECLARE @tintOppType AS TINYINT
	
select @tintOppStatus=tintOppStatus,@tintOppType=tintOppType,@tintShipped=tintShipped from OpportunityMaster where numOppID=@numOppID              
	if @tintOppStatus=1               
	begin         
	PRINT 'inside USP_UpdatingInventoryonCloseDeal'
		exec USP_UpdatingInventoryonCloseDeal @numOppID,@numContactId
	end              

/*Commented by Chintan..reason:tobe executed only when order is confirmed*/
--exec USP_UpdatingInventoryonCloseDeal @numOppID,@numContactId

-- SELECT * FROM OPENXML (@hDocItem,'/NewDataSet/Table',2)                                                        
-- WITH  (                                                        
--  numoppitemtCode numeric(9),                   
--  numItemCode numeric(9),                                                        
--  numUnitHour numeric(9),                                                        
--  monPrice money,                                                     
--  monTotAmount money,                                                        
--  vcItemDesc varchar(1000),                  
--  numWarehouseItmsID numeric(9),        
--  vcItemType varchar(30),    
--  vcAttrValues varchar(50) ,
--  numUOM NUMERIC(18,0),
--  decUOMConversionFactor DECIMAL(18,5),bitDiscountType BIT,fltDiscount FLOAT,monTotAmtBefDiscount MONEY,
--  vcShippingMethod VARCHAR(100),decShippingCharge MONEY,dtDeliveryDate datetime
--  ))X                
--                
                
-- EXEC sp_xml_removedocument @hDocItem                
                
--end
                             
                  
	select @TotalAmount=sum(monTotAmount)from OpportunityItems where numOppId=@numOppID                            
 update OpportunityMaster set  monPamount=@TotalAmount - @fltDiscount                           
 where numOppId=@numOppID                           
                          
--set @vcPOppName=(select  ISNULL(vcPOppName,numOppId) from OpportunityMaster where numOppId=@numOppID )
                          
	IF(@vcSource IS NOT NULL)
	BEGIN
		insert into OpportunityLinking ([numParentOppID],[numChildOppID],[vcSource],[numParentOppBizDocID],numPromotionId,numSiteID)  values(null,@numOppID,@vcSource,NULL,@numProId,@numSiteID);
	END

	--Add/Update Address Details
	DECLARE @vcStreet  varchar(100),@vcCity  varchar(50),@vcPostalCode  varchar(15),@vcCompanyName  varchar(100),@vcAddressName VARCHAR(50)      
	DECLARE @numState  numeric(9),@numCountry  numeric(9), @numCompanyId  numeric(9) 
	DECLARE @bitIsPrimary BIT;

	--Bill Address
	IF @numBillAddressId>0
	BEGIN
	 SELECT  @vcStreet=ISNULL(vcStreet,''),@vcCity=ISNULL(vcCity,''),@vcPostalCode=ISNULL(vcPostalCode,''),
			@numState=ISNULL(numState,0),@numCountry=ISNULL(numCountry,0),@bitIsPrimary=bitIsPrimary,
			@vcAddressName=vcAddressName
				FROM  dbo.AddressDetails WHERE   numDomainID = @numDomainID AND numAddressID = @numBillAddressId 
            
  
  		EXEC dbo.USP_UpdateOppAddress
		@numOppID = @numOppID, --  numeric(9, 0)
		@byteMode = 0, --  tinyint
		@vcStreet = @vcStreet, --  varchar(100)
		@vcCity = @vcCity, --  varchar(50)
		@vcPostalCode = @vcPostalCode, --  varchar(15)
		@numState = @numState, --  numeric(9, 0)
		@numCountry = @numCountry, --  numeric(9, 0)
		@vcCompanyName = '', --  varchar(100)
		@numCompanyId = 0, --  numeric(9, 0)
		@vcAddressName =@vcAddressName
	
        
	END
  
  
	--Ship Address
	IF @numShipAddressId>0
	BEGIN
   		SELECT  @vcStreet=ISNULL(vcStreet,''),@vcCity=ISNULL(vcCity,''),@vcPostalCode=ISNULL(vcPostalCode,''),
			@numState=ISNULL(numState,0),@numCountry=ISNULL(numCountry,0),@bitIsPrimary=bitIsPrimary,
			@vcAddressName=vcAddressName
				FROM  dbo.AddressDetails WHERE   numDomainID = @numDomainID AND numAddressID = @numShipAddressId 
  
  
		select  @vcCompanyName=vcCompanyName,@numCompanyId=div.numCompanyID from CompanyInfo Com                            
		join divisionMaster Div                            
		on div.numCompanyID=com.numCompanyID                            
		where div.numdivisionID=@numDivID

  		EXEC dbo.USP_UpdateOppAddress
		@numOppID = @numOppID, --  numeric(9, 0)
		@byteMode = 1, --  tinyint
		@vcStreet = @vcStreet, --  varchar(100)
		@vcCity = @vcCity, --  varchar(50)
		@vcPostalCode = @vcPostalCode, --  varchar(15)
		@numState = @numState, --  numeric(9, 0)
		@numCountry = @numCountry, --  numeric(9, 0)
		@vcCompanyName = @vcCompanyName, --  varchar(100)
		@numCompanyId =@numCompanyId, --  numeric(9, 0)
		@vcAddressName =@vcAddressName
	
   	
	END

	--Insert Tax for Division                       
	INSERT dbo.OpportunityMasterTaxItems 
	(
		numOppId,
		numTaxItemID,
		fltPercentage,
		tintTaxType,
		numTaxID
	) 
	SELECT 
		@numOppID,
		TI.numTaxItemID,
		TEMPTax.decTaxValue,
		TEMPTax.tintTaxType,
		numTaxID
	FROM 
		TaxItems TI 
	JOIN 
		DivisionTaxTypes DTT 
	ON 
		TI.numTaxItemID = DTT.numTaxItemID
	OUTER APPLY
	(
		SELECT
			decTaxValue,
			tintTaxType,
			numTaxID
		FROM
			dbo.fn_CalItemTaxAmt(@numDomainID,@numDivID,0,TI.numTaxItemID,@numOppId,0,NULL)
	) AS TEMPTax
	WHERE 
		DTT.numDivisionID=@numDivID AND DTT.bitApplicable=1
	UNION 
	SELECT 
		@numOppID,
		0,
		TEMPTax.decTaxValue,
		TEMPTax.tintTaxType,
		numTaxID
	FROM 
		dbo.DivisionMaster 
	OUTER APPLY
	(
		SELECT
			decTaxValue,
			tintTaxType,
			numTaxID
		FROM
			dbo.fn_CalItemTaxAmt(@numDomainID,@numDivID,0,0,@numOppId,1,NULL)
	) AS TEMPTax
	WHERE 
		bitNoTax=0 
		AND numDivisionID=@numDivID

	-- DELETE ITEM LEVEL CRV TAX TYPES
	DELETE FROM OpportunityMasterTaxItems WHERE numOppId=@numOppId AND numTaxItemID = 1

	-- INSERT ITEM LEVEL CRV TAX TYPES
	INSERT INTO OpportunityMasterTaxItems
	(
		numOppId,
		numTaxItemID,
		fltPercentage,
		tintTaxType,
		numTaxID
	) 
	SELECT 
		OI.numOppId,
		1,
		TEMPTax.decTaxValue,
		TEMPTax.tintTaxType,
		TEMPTax.numTaxID
	FROM 
		OpportunityItems OI
	INNER JOIN
		ItemTax IT
	ON
		IT.numItemCode = OI.numItemCode
	OUTER APPLY
	(
		SELECT
			decTaxValue,
			tintTaxType,
			numTaxID
		FROM
			dbo.fn_CalItemTaxAmt(@numDomainID,@numDivID,OI.numItemCode,IT.numTaxItemID,@numOppId,1,NULL)
	) AS TEMPTax
	WHERE
		OI.numOppId = @numOppID
		AND IT.numTaxItemID = 1 -- CRV TAX
	GROUP BY
		OI.numOppId,
		TEMPTax.decTaxValue,
		TEMPTax.tintTaxType,
		TEMPTax.numTaxID
  
	--Delete Tax for Opportunity Items if item deleted 
	DELETE FROM OpportunityItemsTaxItems WHERE numOppId=@numOppId AND numOppItemID NOT IN (SELECT numoppitemtCode FROM OpportunityItems WHERE numOppId=@numOppID)

	--Insert Tax for Opportunity Items
	INSERT INTO dbo.OpportunityItemsTaxItems 
	(
		numOppId,
		numOppItemID,
		numTaxItemID,
		numTaxID
	) 
	SELECT 
		@numOppId,
		OI.numoppitemtCode,
		TI.numTaxItemID,
		0 
	FROM 
		dbo.OpportunityItems OI 
	JOIN 
		dbo.ItemTax IT 
	ON 
		OI.numItemCode=IT.numItemCode 
	JOIN
		TaxItems TI 
	ON 
		TI.numTaxItemID=IT.numTaxItemID 
	WHERE 
		OI.numOppId=@numOppID 
		AND IT.bitApplicable=1 
		AND OI.numoppitemtCode NOT IN (SELECT numOppItemID FROM OpportunityItemsTaxItems WHERE numOppId=@numOppID)
	UNION
	SELECT 
		@numOppId,
		OI.numoppitemtCode,
		0,
		0 
	FROM 
		dbo.OpportunityItems OI 
	JOIN 
		dbo.Item I 
	ON 
		OI.numItemCode=I.numItemCode
	WHERE 
		OI.numOppId=@numOppID 
		AND I.bitTaxable=1 
		AND OI.numoppitemtCode NOT IN (SELECT numOppItemID FROM OpportunityItemsTaxItems WHERE numOppId=@numOppID)
	UNION
	SELECT
		@numOppId,
		OI.numoppitemtCode,
		1,
		TD.numTaxID
	FROM
		dbo.OpportunityItems OI 
	INNER JOIN
		ItemTax IT
	ON
		IT.numItemCode = OI.numItemCode
	INNER JOIN
		TaxDetails TD
	ON
		TD.numTaxID = IT.numTaxID
		AND TD.numDomainId = @numDomainId
	WHERE
		OI.numOppId = @numOppID
		AND OI.numoppitemtCode NOT IN (SELECT numOppItemID FROM OpportunityItemsTaxItems WHERE numOppId=@numOppID)

COMMIT
END TRY
BEGIN CATCH
	DECLARE @ErrorMessage NVARCHAR(4000)
	DECLARE @ErrorNumber INT
	DECLARE @ErrorSeverity INT
	DECLARE @ErrorState INT
	DECLARE @ErrorLine INT
	DECLARE @ErrorProcedure NVARCHAR(200)

  -- Whoops, there was an error
  IF @@TRANCOUNT > 0
     ROLLBACK

  SELECT 
		@ErrorMessage = ERROR_MESSAGE(),
		@ErrorNumber = ERROR_NUMBER(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE(),
		@ErrorLine = ERROR_LINE(),
		@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

	RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);
END CATCH
/****** Object:  StoredProcedure [dbo].[USP_UpdatedomainDetails]    Script Date: 05/07/2009 21:56:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj                                      
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_updatedomaindetails')
DROP PROCEDURE usp_updatedomaindetails
GO
CREATE PROCEDURE [dbo].[USP_UpdatedomainDetails]                                      
@numDomainID as numeric(9)=0,                                      
@vcDomainName as varchar(50)='',                                      
@vcDomainDesc as varchar(100)='',                                      
@bitExchangeIntegration as bit,                                      
@bitAccessExchange as bit,                                      
@vcExchUserName as varchar(100)='',                                      
@vcExchPassword as varchar(100)='',                                      
@vcExchPath as varchar(200)='',                                      
@vcExchDomain as varchar(100)='',                                    
@tintCustomPagingRows as tinyint,                                    
@vcDateFormat as varchar(30)='',                                    
@numDefCountry as numeric(9),                                    
@tintComposeWindow as tinyint,                                    
@sintStartDate as smallint,                                    
@sintNoofDaysInterval as smallint,                                    
@tintAssignToCriteria as tinyint,                                    
@bitIntmedPage as bit,                                    
@tintFiscalStartMonth as tinyint,                                      
@tintPayPeriod as tinyint,                        
@numCurrencyID as numeric(9) ,                
@charUnitSystem as char(1)='',              
@vcPortalLogo as varchar(100)='',            
@tintChrForComSearch as tinyint  ,            
@intPaymentGateWay as int,    
@tintChrForItemSearch as tinyint,
@ShipCompany as numeric(9),
@bitAutoPopulateAddress as bit=null,
@tintPoulateAddressTo as tinyint=null,
@bitMultiCurrency as bit,
@bitCreateInvoice AS tinyint = null,
@numDefaultSalesBizDocID AS NUMERIC(9)=NULL,
@numDefaultPurchaseBizDocID AS NUMERIC(9)=NULL,
@bitSaveCreditCardInfo AS BIT = NULL,
@intLastViewedRecord AS INT=10,
@tintCommissionType AS TINYINT,
@tintBaseTax AS TINYINT,
@bitEmbeddedCost AS BIT,
@numDefaultSalesOppBizDocId NUMERIC=NULL,
@tintSessionTimeOut TINYINT=1,
@tintDecimalPoints TINYINT,
@bitCustomizePortal BIT=0,
@tintBillToForPO TINYINT,
@tintShipToForPO TINYINT,
@tintOrganizationSearchCriteria TINYINT,
@bitDocumentRepositary BIT=0,
@tintComAppliesTo TINYINT=0,
@bitGtoBContact BIT=0,
@bitBtoGContact BIT=0,
@bitGtoBCalendar BIT=0,
@bitBtoGCalendar BIT=0,
@bitExpenseNonInventoryItem BIT=0,
@bitInlineEdit BIT=0,
@bitRemoveVendorPOValidation BIT=0,
@tintBaseTaxOnArea tinyint=0,
@bitAmountPastDue BIT=0,
@monAmountPastDue money,
@bitSearchOrderCustomerHistory BIT=0,
@bitRentalItem as bit=0,
@numRentalItemClass as NUMERIC(9)=NULL,
@numRentalHourlyUOM as NUMERIC(9)=NULL,
@numRentalDailyUOM as NUMERIC(9)=NULL,
@tintRentalPriceBasedOn as tinyint=NULL,
@tintCalendarTimeFormat as tinyint=NULL,
@tintDefaultClassType as tinyint=NULL,
@tintPriceBookDiscount as tinyint=0,
@bitAutoSerialNoAssign as bit=0,
@bitIsShowBalance AS BIT = 0,
@numIncomeAccID NUMERIC(18, 0) = 0,
@numCOGSAccID NUMERIC(18, 0) = 0,
@numAssetAccID NUMERIC(18, 0) = 0,
@IsEnableProjectTracking AS BIT = 0,
@IsEnableCampaignTracking AS BIT = 0,
@IsEnableResourceScheduling AS BIT = 0,
@ShippingServiceItem  AS NUMERIC(9)=0,
@numSOBizDocStatus AS NUMERIC(9)=0,
@bitAutolinkUnappliedPayment AS BIT=0,
@numDiscountServiceItemID AS NUMERIC(9)=0,
@vcShipToPhoneNumber as VARCHAR(50)='',
@vcGAUserEmailId as VARCHAR(50)='',
@vcGAUserPassword as VARCHAR(50)='',
@vcGAUserProfileId as VARCHAR(50)='',
@bitDiscountOnUnitPrice AS BIT=0,
@intShippingImageWidth INT = 0,
@intShippingImageHeight INT = 0,
@numTotalInsuredValue NUMERIC(10,2) = 0,
@numTotalCustomsValue NUMERIC(10,2) = 0,
@vcHideTabs VARCHAR(1000) = '',
@bitUseBizdocAmount BIT = 0,
@bitDefaultRateType BIT = 0,
@bitIncludeTaxAndShippingInCommission  BIT = 0,
@bitPurchaseTaxCredit BIT=0,
@bitLandedCost BIT=0,
@vcLanedCostDefault VARCHAR(10)='',
@bitMinUnitPriceRule BIT = 0,
@numAbovePercent AS NUMERIC(18,2),
@numBelowPercent AS NUMERIC(18,2),
@numBelowPriceField AS NUMERIC(18,0),
@vcUnitPriceApprover AS VARCHAR(100),
@numDefaultSalesPricing AS TINYINT,
@bitReOrderPoint AS BIT = 0,
@numReOrderPointOrderStatus AS NUMERIC(18,0) = 0,
@numPODropShipBizDoc AS NUMERIC(18,0) = 0,
@numPODropShipBizDocTemplate AS NUMERIC(18,0) = 0,
@IsEnableDeferredIncome AS BIT = 0,
@bitApprovalforTImeExpense AS BIT=0,
@numCost AS Numeric(9,0)=0,
@intTimeExpApprovalProcess int=0,
@bitApprovalforOpportunity AS BIT=0,
@intOpportunityApprovalProcess int=0,
@numDefaultSalesShippingDoc NUMERIC(18,0)=0,
@numDefaultPurchaseShippingDoc NUMERIC(18,0)=0,
@bitchkOverRideAssignto AS BIT=0,
@vcPrinterIPAddress VARCHAR(15) = '',
@vcPrinterPort VARCHAR(5) = '',
@numDefaultSiteID NUMERIC(18,0) = 0,
@vcSalesOrderTabs VARCHAR(200)='',
@vcSalesQuotesTabs VARCHAR(200)='',
@vcItemPurchaseHistoryTabs VARCHAR(200)='',
@vcItemsFrequentlyPurchasedTabs VARCHAR(200)='',
@vcOpenCasesTabs VARCHAR(200)='',
@vcOpenRMATabs VARCHAR(200)='',
@bitSalesOrderTabs BIT=0,
@bitSalesQuotesTabs BIT=0,
@bitItemPurchaseHistoryTabs BIT=0,
@bitItemsFrequentlyPurchasedTabs BIT=0,
@bitOpenCasesTabs BIT=0,
@bitOpenRMATabs BIT=0,
@bitRemoveGlobalLocation BIT = 0,
@bitSupportTabs BIT=0,
@vcSupportTabs VARCHAR(200)=''
as                                      
BEGIN
BEGIN TRY
BEGIN TRANSACTION

	IF ISNULL(@bitMinUnitPriceRule,0) = 1 AND ((SELECT COUNT(*) FROM UnitPriceApprover WHERE numDomainID=@numDomainID) = 0 OR ISNULL(@bitApprovalforOpportunity,0)=0 OR ISNULL(@intOpportunityApprovalProcess,0) = 0)
	BEGIN
		RAISERROR('INCOMPLETE_MIN_UNIT_PRICE_CONFIGURATION',16,1)
		RETURN
	END


	If ISNULL(@bitApprovalforTImeExpense,0) = 1 AND (SELECT
														COUNT(*) 
													FROM
														UserMaster
													LEFT JOIN
														ApprovalConfig
													ON
														ApprovalConfig.numUserId = UserMaster.numUserDetailId
													WHERE
														UserMaster.numDomainID=@numDomainID
														AND numModule=1
														AND ISNULL(numGroupID,0) > 0
														AND ISNULL(bitActivateFlag,0) = 1
														AND ISNULL(numLevel1Authority,0) = 0
														AND ISNULL(numLevel2Authority,0) = 0
														AND ISNULL(numLevel3Authority,0) = 0
														AND ISNULL(numLevel4Authority,0) = 0
														AND ISNULL(numLevel5Authority,0) = 0) > 0
	BEGIN
		RAISERROR('INCOMPLETE_TIMEEXPENSE_APPROVAL',16,1)
		RETURN
	END

                                  
 update Domain                                       
   set                                       
   vcDomainName=@vcDomainName,                                      
   vcDomainDesc=@vcDomainDesc,                                      
   bitExchangeIntegration=@bitExchangeIntegration,                                      
   bitAccessExchange=@bitAccessExchange,                                      
   vcExchUserName=@vcExchUserName,                                      
   vcExchPassword=@vcExchPassword,                                      
   vcExchPath=@vcExchPath,                                      
   vcExchDomain=@vcExchDomain,                                    
   tintCustomPagingRows =@tintCustomPagingRows,                                    
   vcDateFormat=@vcDateFormat,                                    
   numDefCountry =@numDefCountry,                                    
   tintComposeWindow=@tintComposeWindow,                                    
   sintStartDate =@sintStartDate,                                    
   sintNoofDaysInterval=@sintNoofDaysInterval,                                    
   tintAssignToCriteria=@tintAssignToCriteria,                                    
   bitIntmedPage=@bitIntmedPage,                                    
   tintFiscalStartMonth=@tintFiscalStartMonth,                            
--   bitDeferredIncome=@bitDeferredIncome,                          
   tintPayPeriod=@tintPayPeriod,
   vcCurrency=isnull((select varCurrSymbol from Currency Where numCurrencyID=@numCurrencyID  ),'')  ,                      
  numCurrencyID=@numCurrencyID,                
  charUnitSystem= @charUnitSystem,              
  vcPortalLogo=@vcPortalLogo ,            
  tintChrForComSearch=@tintChrForComSearch  ,      
  intPaymentGateWay=@intPaymentGateWay,
 tintChrForItemSearch=@tintChrForItemSearch,
 numShipCompany= @ShipCompany,
 bitAutoPopulateAddress = @bitAutoPopulateAddress,
 tintPoulateAddressTo =@tintPoulateAddressTo,
 --bitMultiCompany=@bitMultiCompany ,
 bitMultiCurrency=@bitMultiCurrency,
 bitCreateInvoice= @bitCreateInvoice,
 [numDefaultSalesBizDocId] =@numDefaultSalesBizDocId,
 bitSaveCreditCardInfo=@bitSaveCreditCardInfo,
 intLastViewedRecord=@intLastViewedRecord,
 [tintCommissionType]=@tintCommissionType,
 [tintBaseTax]=@tintBaseTax,
 [numDefaultPurchaseBizDocId]=@numDefaultPurchaseBizDocID,
 bitEmbeddedCost=@bitEmbeddedCost,
 numDefaultSalesOppBizDocId=@numDefaultSalesOppBizDocId,
 tintSessionTimeOut=@tintSessionTimeOut,
 tintDecimalPoints=@tintDecimalPoints,
 bitCustomizePortal=@bitCustomizePortal,
 tintBillToForPO = @tintBillToForPO,
 tintShipToForPO = @tintShipToForPO,
 tintOrganizationSearchCriteria=@tintOrganizationSearchCriteria,
-- vcPortalName=@vcPortalName,
 bitDocumentRepositary=@bitDocumentRepositary,
 --tintComAppliesTo=@tintComAppliesTo,THIS FIELD IS REMOVED FROM GLOBAL SETTINGS
 bitGtoBContact=@bitGtoBContact,
 bitBtoGContact=@bitBtoGContact,
 bitGtoBCalendar=@bitGtoBCalendar,
 bitBtoGCalendar=@bitBtoGCalendar,
 bitExpenseNonInventoryItem=@bitExpenseNonInventoryItem,
 bitInlineEdit=@bitInlineEdit,
 bitRemoveVendorPOValidation=@bitRemoveVendorPOValidation,
tintBaseTaxOnArea=@tintBaseTaxOnArea,
bitAmountPastDue = @bitAmountPastDue,
monAmountPastDue = @monAmountPastDue,
bitSearchOrderCustomerHistory=@bitSearchOrderCustomerHistory,
bitRentalItem=@bitRentalItem,
numRentalItemClass=@numRentalItemClass,
numRentalHourlyUOM=@numRentalHourlyUOM,
numRentalDailyUOM=@numRentalDailyUOM,
tintRentalPriceBasedOn=@tintRentalPriceBasedOn,
tintCalendarTimeFormat=@tintCalendarTimeFormat,
tintDefaultClassType=@tintDefaultClassType,
tintPriceBookDiscount=@tintPriceBookDiscount,
bitAutoSerialNoAssign=@bitAutoSerialNoAssign,
bitIsShowBalance = @bitIsShowBalance,
numIncomeAccID = @numIncomeAccID,
numCOGSAccID = @numCOGSAccID,
numAssetAccID = @numAssetAccID,
IsEnableProjectTracking = @IsEnableProjectTracking,
IsEnableCampaignTracking = @IsEnableCampaignTracking,
IsEnableResourceScheduling = @IsEnableResourceScheduling,
numShippingServiceItemID = @ShippingServiceItem,
numSOBizDocStatus=@numSOBizDocStatus,
bitAutolinkUnappliedPayment=@bitAutolinkUnappliedPayment,
numDiscountServiceItemID=@numDiscountServiceItemID,
vcShipToPhoneNo = @vcShipToPhoneNumber,
vcGAUserEMail = @vcGAUserEmailId,
vcGAUserPassword = @vcGAUserPassword,
vcGAUserProfileId = @vcGAUserProfileId,
bitDiscountOnUnitPrice=@bitDiscountOnUnitPrice,
intShippingImageWidth = @intShippingImageWidth ,
intShippingImageHeight = @intShippingImageHeight,
numTotalInsuredValue = @numTotalInsuredValue,
numTotalCustomsValue = @numTotalCustomsValue,
vcHideTabs = @vcHideTabs,
bitUseBizdocAmount = @bitUseBizdocAmount,
bitDefaultRateType = @bitDefaultRateType,
bitIncludeTaxAndShippingInCommission = @bitIncludeTaxAndShippingInCommission,
/*,bitAllowPPVariance=@bitAllowPPVariance*/
bitPurchaseTaxCredit=@bitPurchaseTaxCredit,
bitLandedCost=@bitLandedCost,
vcLanedCostDefault=@vcLanedCostDefault,
bitMinUnitPriceRule = @bitMinUnitPriceRule,
numDefaultSalesPricing = @numDefaultSalesPricing,
numPODropShipBizDoc=@numPODropShipBizDoc,
numPODropShipBizDocTemplate=@numPODropShipBizDocTemplate,
IsEnableDeferredIncome=@IsEnableDeferredIncome,
bitApprovalforTImeExpense=@bitApprovalforTImeExpense,
numCost= @numCost,intTimeExpApprovalProcess=@intTimeExpApprovalProcess,
bitApprovalforOpportunity=@bitApprovalforOpportunity,intOpportunityApprovalProcess=@intOpportunityApprovalProcess,
 numDefaultPurchaseShippingDoc=@numDefaultPurchaseShippingDoc,numDefaultSalesShippingDoc=@numDefaultSalesShippingDoc,
 bitchkOverRideAssignto=@bitchkOverRideAssignto,
 vcPrinterIPAddress=@vcPrinterIPAddress,
 vcPrinterPort=@vcPrinterPort,
 numDefaultSiteID=ISNULL(@numDefaultSiteID,0),
  vcSalesOrderTabs=@vcSalesOrderTabs,
 vcSalesQuotesTabs=@vcSalesQuotesTabs,
 vcItemPurchaseHistoryTabs=@vcItemPurchaseHistoryTabs,
 vcItemsFrequentlyPurchasedTabs=@vcItemsFrequentlyPurchasedTabs,
 vcOpenCasesTabs=@vcOpenCasesTabs,
 vcOpenRMATabs=@vcOpenRMATabs,
 bitSalesOrderTabs=@bitSalesOrderTabs,
 bitSalesQuotesTabs=@bitSalesQuotesTabs,
 bitItemPurchaseHistoryTabs=@bitItemPurchaseHistoryTabs,
 bitItemsFrequentlyPurchasedTabs=@bitItemsFrequentlyPurchasedTabs,
 bitOpenCasesTabs=@bitOpenCasesTabs,
 bitOpenRMATabs=@bitOpenRMATabs,
 bitRemoveGlobalLocation=@bitRemoveGlobalLocation,
  bitSupportTabs=@bitSupportTabs,
 vcSupportTabs=@vcSupportTabs
 where numDomainId=@numDomainID
COMMIT

 IF ISNULL(@bitRemoveGlobalLocation,0) = 1
 BEGIN
	DECLARE @TEMPGlobalWarehouse TABLE
	(
		ID INT IDENTITY(1,1)
		,numItemCode NUMERIC(18,0)
		,numWarehouseID NUMERIC(18,0)
		,numWarehouseItemID NUMERIC(28,0)
	)

	INSERT INTO @TEMPGlobalWarehouse
	(
		numItemCode
		,numWarehouseID
		,numWarehouseItemID
	)
	SELECT
		numItemID
		,numWareHouseID
		,numWareHouseItemID
	FROM
		WareHouseItems
	WHERE
		numDomainID=@numDomainID
		AND numWLocationID = -1
		AND ISNULL(numOnHand,0) = 0
		AND ISNULL(numOnOrder,0)=0
		AND ISNULL(numAllocation,0)=0
		AND ISNULL(numBackOrder,0)=0


	DECLARE @i AS INT = 1
	DECLARE @iCount AS INT
	DECLARE @numTempItemCode AS NUMERIC(18,0)
	DECLARE @numTempWareHouseID AS NUMERIC(18,0)
	DECLARE @numTempWareHouseItemID AS NUMERIC(18,0)
	SELECT @iCount=COUNT(*) FROM @TEMPGlobalWarehouse

	WHILE @i <= @iCount
	BEGIN
		SELECT @numTempItemCode=numItemCode,@numTempWareHouseID=numWarehouseID,@numTempWareHouseItemID=numWarehouseItemID FROM @TEMPGlobalWarehouse WHERE ID=@i

		BEGIN TRY
			EXEC USP_AddUpdateWareHouseForItems @numTempItemCode,  
												@numTempWareHouseID,
												@numTempWareHouseItemID,
												'',
												0,
												0,
												0,
												'',
												'',
												@numDomainID,
												'',
												'',
												'',
												0,
												3,
												0,
												0,
												NULL,
												0
		END TRY
		BEGIN CATCH

		END CATCH

		SET @i = @i + 1
	END
 END

END TRY
BEGIN CATCH
	DECLARE @ErrorMessage NVARCHAR(4000)
	DECLARE @ErrorNumber INT
	DECLARE @ErrorSeverity INT
	DECLARE @ErrorState INT
	DECLARE @ErrorLine INT
	DECLARE @ErrorProcedure NVARCHAR(200)

	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;

	SELECT 
		@ErrorMessage = ERROR_MESSAGE(),
		@ErrorNumber = ERROR_NUMBER(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE(),
		@ErrorLine = ERROR_LINE(),
		@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-');

	RAISERROR (@ErrorMessage, @ErrorSeverity, 1, @ErrorNumber, @ErrorSeverity, @ErrorState, @ErrorProcedure, @ErrorLine);
END CATCH

END
--Created By Anoop Jayaraj          
--exec USP_ItemDetailsForEcomm @numItemCode=859064,@numWareHouseID=1074,@numDomainID=172,@numSiteId=90
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_AutoSuggestItemList')
DROP PROCEDURE USP_AutoSuggestItemList
GO
CREATE PROCEDURE [dbo].[USP_AutoSuggestItemList]   
@numDomainID as numeric(9),
@numSiteId as numeric(9),
@vcSubChar as Varchar(400)                             
as     
DECLARE @vcSqlQuery VARCHAR(MAX)=''




SELECT distinct I.numItemCode,I.vcItemName,ISNULL(CASE WHEN I.[charItemType]='P' 
			THEN ISNULL(dbo.fn_UOMConversion(ISNULL(I.numSaleUnit,0),I.numItemCode,I.numDomainId,ISNULL(I.numBaseUnit,0)),0) * W.[monWListPrice]  
			ELSE ISNULL(dbo.fn_UOMConversion(ISNULL(I.numSaleUnit,0),I.numItemCode,I.numDomainId,ISNULL(I.numBaseUnit,0)),0) * I.monListPrice 
	   END,0) AS monListPrice,I.vcModelID,I.vcSKU,IM.vcPathForImage
FROM      
Item AS I
LEFT JOIN   ItemCategory IC   ON   I.numItemCode = IC.numItemID
LEFT JOIN    ItemImages IM     ON   I.numItemCode = IM.numItemCode AND IM.bitdefault=1 AND IM.bitIsImage = 1
LEFT JOIN   Category C        ON   IC.numCategoryID = C.numCategoryID
LEFT JOIN   SiteCategories SC ON   IC.numCategoryID = SC.numCategoryID
LEFT JOIN   eCommerceDTL E    ON   E.numDomainID = I.numDomainID AND E.numSiteId = SC.numSiteID
OUTER APPLY (SELECT TOP 1 numWareHouseItemID,monWListPrice FROM WareHouseItems W 
													  WHERE  W.numItemID = I.numItemCode 
													  AND W.numDomainID = @numDomainID) AS W
WHERE I.numDomainID=@numDomainID  AND E.numSiteId=@numSiteId 
AND (I.numItemCode LIKE '%'+@vcSubChar+'%' OR I.numItemCode LIKE '%'+@vcSubChar+'%' 
OR I.vcItemName LIKE '%'+@vcSubChar+'%' OR I.vcModelID LIKE '%'+@vcSubChar+'%' OR I.vcSKU LIKE '%'+@vcSubChar+'%')





/****** Object:  StoredProcedure [dbo].[USP_GetInvoiceList]    Script Date: 07/26/2008 16:17:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by prasanta Pradhan                  
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetCaseLisInEcommerce')
DROP PROCEDURE USP_GetCaseLisInEcommerce
GO
CREATE PROCEDURE [dbo].[USP_GetCaseLisInEcommerce]                            
 @numDivisionID as numeric(9)=0 ,
 @numDomainID AS NUMERIC(9),
 @CurrentPage INT,
 @PageSize INT,
 @UserId NUMERIC(18,0),
 @CaseStatus INT=0
as                        
 
 DECLARE @firstRec INT=0
 DECLARE @lastRec INT=0

	SET @firstRec = ( @CurrentPage - 1 ) * @PageSize
    SET @lastRec = ( @CurrentPage * @PageSize + 1 )
	SELECT  * FROM (
	SELECT ROW_NUMBER() OVER(ORDER BY numCaseId) AS Rownumber,* FROM (
	select 
		C.numCaseId,
		C.vcCaseNumber,
		[dbo].[FormatedDateFromDate](C.bintCreatedDate,C.numDomainID) AS bintCreatedDate,
		C.textSubject,
		(select TOP 1 vcUserName from UserMaster where numUserDetailId=C.numAssignedTo) AS AssignTo,
		(select TOP 1 vcData from ListDetails WHERE numListItemID=C.numStatus) AS [Status],
		C.textDesc 
	from 
		Cases AS C 
	WHERE 
		1=(CASE WHEN @CaseStatus=0 AND C.numStatus<>136 THEN 1 WHEN C.numStatus=@CaseStatus THEN 1 ELSE 0 END) AND
		C.numDivisionID=@numDivisionID AND 
		C.numContactId=@UserId
		) AS K) AS T
		WHERE T.Rownumber>@firstRec AND T.Rownumber<@lastRec

	select 
		COUNT(C.numCaseId)
	from 
		Cases AS C 
	WHERE 
		1=(CASE WHEN @CaseStatus=0 AND C.numStatus<>136 THEN 1 WHEN C.numStatus=@CaseStatus THEN 1 ELSE 0 END) AND
		C.numDivisionID=@numDivisionID AND 
		C.numContactId=@UserId




/****** Object:  StoredProcedure [dbo].[USP_GetInvoiceList]    Script Date: 07/26/2008 16:17:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by prasanta Pradhan                  
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetOrderItemListInEcommerce')
DROP PROCEDURE USP_GetOrderItemListInEcommerce
GO
CREATE PROCEDURE [dbo].[USP_GetOrderItemListInEcommerce]                            
 @numDivisionID as numeric(9)=0 ,
 @numDomainID AS NUMERIC(9),
 @CurrentPage INT,
 @PageSize INT,
 @UserId NUMERIC(18,0)
as                        
 
 DECLARE @firstRec INT=0
 DECLARE @lastRec INT=0

	SET @firstRec = ( @CurrentPage - 1 ) * @PageSize
    SET @lastRec = ( @CurrentPage * @PageSize + 1 )
	SELECT  * FROM (
	SELECT ROW_NUMBER() OVER(ORDER BY numOppID) AS Rownumber,* FROM (
	select 
		OI.numOppId,
		OI.numItemCode,
		OI.vcItemName,
		O.vcPOppName,
		[dbo].[FormatedDateFromDate](O.bintCreatedDate,O.numDomainId) AS bintCreatedDate,
		OI.numUnitHour,
		(OI.monPrice* O.fltExchangeRate) AS monPrice,
		(OI.monTotAmtBefDiscount* O.fltExchangeRate) AS monTotAmount,
		O.tintOppStatus
	from 
		OpportunityItems AS OI 
	LEFT JOIN 
		OpportunityMaster AS O ON O.numOppId=OI.numOppId
	WHERE
		O.numDivisionID=@numDivisionID AND O.tintOppType= 1 
		--AND O.tintOppStatus=1 AND O.tintShipped=1 
		AND O.numContactId=@UserId
		) AS K) AS T
		WHERE T.Rownumber>@firstRec AND T.Rownumber<@lastRec

	select 
		 COUNT(OI.numItemCode)
	from 
		OpportunityItems AS OI 
	LEFT JOIN 
		OpportunityMaster AS O ON O.numOppId=OI.numOppId
	WHERE
		O.numDivisionID=@numDivisionID AND O.tintOppType= 1 
		--AND O.tintOppStatus=1 AND O.tintShipped=1 
		AND O.numContactId=@UserId






/****** Object:  StoredProcedure [dbo].[USP_GetInvoiceList]    Script Date: 07/26/2008 16:17:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by prasanta Pradhan                  
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetOrderListInEcommerce')
DROP PROCEDURE USP_GetOrderListInEcommerce
GO
CREATE PROCEDURE [dbo].[USP_GetOrderListInEcommerce]                            
 @numDivisionID as numeric(9)=0 ,
 @tintOppType AS TINYINT=1,
 @numDomainID AS NUMERIC(9),
 @CurrentPage INT,
 @PageSize INT,
 @tintShipped INT=0,
 @tintOppStatus INT=0,
 @UserId NUMERIC(18,0)
as                        
 
 DECLARE @firstRec INT=0
 DECLARE @lastRec INT=0
 
IF (@tintOppType = 1 OR  @tintOppType =2 )
BEGIN
	SET @firstRec = ( @CurrentPage - 1 ) * @PageSize
    SET @lastRec = ( @CurrentPage * @PageSize + 1 )
	SELECT  * FROM (
	SELECT ROW_NUMBER() OVER(ORDER BY numOppID) AS Rownumber,* FROM (
	SELECT distinct OM.numOppID,OM.vcPOppName,(SELECT SUBSTRING((SELECT '$^$' + CAST(numOppBizDocsId AS VARCHAR(18)) +'#^#'+ vcBizDocID
										FROM OpportunityBizDocs WHERE numOppId=OM.numOppID AND numBizDOcId=287 FOR XML PATH('')),4,200000)) AS vcBizDoc,

		(SELECT SUBSTRING((SELECT '$^$' + CAST(vcTrackingNo AS VARCHAR(18)) +'#^#'+ vcTrackingUrl
										FROM OpportunityBizDocs WHERE numOppId=OM.numOppID FOR XML PATH('')),4,200000)) AS vcShipping,
		(isnull(dbo.GetDealAmount(OM.numOppID,getdate(),0),0) * OM.fltExchangeRate) as monDealAmount,
		(isnull(dbo.GetPaidAmount(OM.numOppID),0) * OM.fltExchangeRate) AS monAmountPaid,
		(isnull(dbo.GetDealAmount(OM.numOppID,getdate(),0),0) * OM.fltExchangeRate)
		 - (isnull(dbo.GetPaidAmount(OM.numOppID),0) * OM.fltExchangeRate) AS BalanceDue,
		(select  TOP 1 vcData from ListDetails where numListItemID=(SELECT TOP 1 numShipVia FROM OpportunityBizDocs WHERE numOppId=OM.numOppID AND numShipVia>0)) AS vcShippingMethod,
		(select  TOP 1 vcData from ListDetails where numListItemID=(SELECT TOP 1 numShipmentMethod FROM OpportunityMaster where numOppId=OM.numOppID)) AS vcMasterShippingMethod,
		(SELECT TOP 1 numShipmentMethod FROM OpportunityMaster where numOppId=OM.numOppID) AS numOppMasterShipVia,
		(SELECT TOP 1 numShipVia FROM OpportunityBizDocs WHERE numOppId=OM.numOppID  AND numShipVia>0) AS numShipVia,
		(SELECT TOP 1 vcTrackingNo FROM OpportunityBizDocs WHERE numOppId=OM.numOppID  AND numShipVia>0) AS vcTrackingNo,
		(SELECT TOP 1 numShippingReportId from [ShippingReport] WHERE [numOppBizDocId] = (SELECT TOP 1 numOppBizDocsId FROM OpportunityBizDocs WHERE numOppId=OM.numOppID  AND numShipVia>0)) AS numShippingReportId,
		(SELECT TOP 1 numOppBizDocsId FROM OpportunityBizDocs WHERE numOppId=OM.numOppID  AND numShipVia>0) AS numShippingBizDocId,
		[dbo].[FormatedDateFromDate]((SELECT TOP 1 dtDeliveryDate FROM OpportunityBizDocs WHERE numOppId=OM.numOppID  AND numShipVia>0),OM.numDomainId) AS DeliveryDate,
		[dbo].[FormatedDateFromDate]((SELECT TOP 1 dtFromDate FROM OpportunityBizDocs WHERE numOppId=OM.numOppID order by dtFromDate desc),OM.numDomainId) AS BillingDate,

		CASE WHEN (SELECT COUNT(numBizDocId) FROM OpportunityBizDocs WHERE numOppId=OM.numOppID)=1 THEN
		(CASE ISNULL(OM.bitBillingTerms,0)  
                 WHEN 1 THEN [dbo].[FormatedDateFromDate](DATEADD(dd,convert(int,ISNULL((SELECT numNetDueInDays FROM BillingTerms WHERE numTermsID = ISNULL(OM.intBillingDays,0)), 0)),(SELECT TOP 1 dtFromDate FROM OpportunityBizDocs WHERE numOppId=OM.numOppID order by dtFromDate desc)),OM.numDomainId)
                 WHEN 0 THEN [dbo].[FormatedDateFromDate]((SELECT TOP 1 dtFromDate FROM OpportunityBizDocs WHERE numOppId=OM.numOppID order by dtFromDate desc),OM.numDomainId)
               END) 
		ELSE 'Multiple' END AS DueDate,
		[dbo].[FormatedDateFromDate](OM.dtReleaseDate,OM.numDomainId) AS ReleaseDate,
		[dbo].[FormatedDateFromDate](OM.bintCreatedDate,OM.numDomainId) AS CreatedDate,
		(select TOP 1 vcData from ListDetails WHERE numListItemID=OM.numStatus) AS vcOrderStatus,
        dbo.GetCreditTerms(OM.numOppId) as Credit,
		dbo.fn_GetContactName(OM.numCreatedBy)  as CreatedName,
		CONVERT(DATETIME, OM.bintCreatedDate) AS  dtCreatedDate,
		CONVERT(DATETIME, OM.bintCreatedDate) AS  numCreatedDate,
		CASE WHEN 
		(select TOP 1 ISNULL(numPaymentMethod,0) from General_Journal_Details WHERE numJournalId IN (select numJournal_Id from General_Journal_Header where numOppBizDocsId IN(select numBizDocId from OpportunityBizDocs where numOppId=OM.numOppID)))=0
		THEN
		(select TOP 1 vcData from ListDetails WHERE numListItemID IN (
		select ISNULL(numDefaultPaymentMethod,0) from DivisionMaster where numDivisionId=@numDivisionID)) 
		ELSE  (select TOP 1 vcData from ListDetails WHERE numListItemID IN (
			  select numPaymentMethod from General_Journal_Details WHERE numJournalId IN (select numJournal_Id from General_Journal_Header where numOppBizDocsId IN(select numBizDocId from OpportunityBizDocs where numOppId=OM.numOppID))))
		END
		AS PaymentMethod,
		isnull(dbo.GetDealAmount(OM.numOppID,getdate(),0),0) as TotalAmt,
		OM.numContactId,
		ISNULL(OM.tintOppStatus,0) As [tintOppStatus]
		FROM dbo.OpportunityMaster OM 
		LEFT JOIN dbo.OpportunityBizDocs OBD ON OM.numoppID=OBD.numoppID
		LEFT JOIN dbo.DivisionMaster D ON D.numDivisionID = OM.numDivisionId
		LEFT JOIN dbo.CompanyInfo C ON C.numCompanyId = D.numCompanyID
		WHERE OM.numDomainId=@numDomainID AND OM.tintOppType= @tintOppType
		AND D.numDivisionID= @numDivisionID 
		--AND ISNULL(bitAuthoritativeBizDocs,0)=1
		AND OM.tintOppStatus=@tintOppStatus  AND OM.numContactId=@UserId
		AND 1=(CASE WHEN @tintOppStatus=1 AND OM.tintShipped=@tintShipped THEN 1 WHEN @tintOppStatus=0 THEN 1 ELSE 0 END)
		--AND ISNULL(OBD.tintDeferred,0) <> 1 AND  isnull(OBD.monDealAmount * OM.fltExchangeRate,0) > 0 
		--AND isnull(OBD.monDealAmount * OM.fltExchangeRate - OBD.[monAmountPaid] * OM.fltExchangeRate,0) > 0
		) AS K) AS T
		WHERE T.Rownumber>@firstRec AND T.Rownumber<@lastRec

		SELECT DISTINCT COUNT(OM.numOppId) FROM dbo.OpportunityMaster OM 
		LEFT JOIN dbo.DivisionMaster D ON D.numDivisionID = OM.numDivisionId
		LEFT JOIN dbo.CompanyInfo C ON C.numCompanyId = D.numCompanyID
		WHERE OM.numDomainId=@numDomainID AND OM.tintOppType= @tintOppType
		AND D.numDivisionID= @numDivisionID 
		--AND ISNULL(bitAuthoritativeBizDocs,0)=1
		AND OM.tintOppStatus=@tintOppStatus  AND OM.numContactId=@UserId
		AND 1=(CASE WHEN @tintOppStatus=1 AND OM.tintShipped=@tintShipped THEN 1 WHEN @tintOppStatus=0 THEN 1 ELSE 0 END)
		--AND ISNULL(OBD.tintDeferred,0) <> 1 AND  isnull(OBD.monDealAmount * OM.fltExchangeRate,0) > 0 
		--AND isnull(OBD.monDealAmount * OM.fltExchangeRate - OBD.[monAmountPaid] * OM.fltExchangeRate,0) > 0


		----Create a Temporary table to hold data                        
--Create table #tempTable ( ID INT IDENTITY PRIMARY KEY,                         
--      numOppID varchar(15),vcPOppName varchar(100),                  
--      vcBizDocID varchar(100),                
--   BizDocName varchar(100),              
--   CreatedName varchar(100),                       
--      Credit VARCHAR(100),                        
--      numCreatedDate datetime,                        
-- monAmountPaid money,    
-- TotalAmt MONEY,
-- numContactId NUMERIC(9),
-- tintOppStatus TINYINT)                        
--                  
--                  
--declare @strSql as varchar(8000)                        
--set @strSql=
--'SELECT 
--O.numOppID,
--O.vcPOppName,               
--CASE WHEN O.tintOppType = 1 AND O.tintOppStatus = 0 THEN ''Sales Opportunity''
--	 WHEN O.tintOppType = 1 AND O.tintOppStatus = 1 THEN ''Sales Order''
--END as BizDocName,              
--dbo.fn_GetContactName(O.numCreatedBy)  as CreatedName,     
--dbo.GetCreditTerms(O.numOppId) as Credit,                  
--CONVERT(DATETIME, O.bintCreatedDate) AS  dtCreatedDate,
--isnull(O.numPClosingPercent,0)as monAmountPaid,
--isnull(dbo.GetDealAmount(O.numOppID,getdate(),0),0) as TotalAmt,
--O.numContactId,ISNULL(O.tintOppStatus,0) As [tintOppStatus]
--FROM dbo.OpportunityMaster O 
--     JOIN dbo.DivisionMaster D 
--        ON D.numDivisionID = O.numDivisionId
--     JOIN dbo.CompanyInfo C 
--        ON C.numCompanyId = D.numCompanyID
--WHERE O.numDomainId = D.numDomainID '
--      
--set @strSql=@strSql + ' and O.tintOppType= ' + convert(varchar(3),@tintOppType)
--
--if @numDivisionID<>0 set @strSql=@strSql + ' and D.numDivisionID= ' + convert(varchar(15),@numDivisionID)                        
--if @SortChar<>'0' set @strSql=@strSql + ' And vcBizDocID like '''+@SortChar+'%'''   
--IF @bitflag =0
--BEGIN
--	SET @strSql = @strSql + ' And O.tintShipped  =1'                                            
--END 
--ELSE IF @bitflag=1
--BEGIN
--SET @strSql = @strSql + ' And (O.tintOppStatus = 1 OR O.tintOppStatus = 0)'
--	
--END
--
--set @strSql=@strSql +' ORDER BY ' + @columnName +' '+ @columnSortOrder                     
--print @strSql                      
--insert into #tempTable(                        
--      numOppID,                  
-- vcBizDocID,                
-- BizDocName,              
--  CreatedName,                        
--      Credit,                        
--      numCreatedDate,                        
-- monAmountPaid,
-- TotalAmt,
-- numContactId,tintOppStatus)                        
--exec (@strSql)                         
--                        
--  declare @firstRec as integer                         
--  declare @lastRec as integer                        
-- set @firstRec= (@CurrentPage-1) * @PageSize                        
--     set @lastRec= (@CurrentPage*@PageSize+1)                        
--select *,(TotalAmt-monAmountPaid) AS BalanceDue,
--CASE WHEN @tintOppType = 1 AND tintOppStatus = 0 THEN 'Sales Opportunity' 
--	 WHEN @tintOppType = 1 AND tintOppStatus = 1 THEN 'Sales Order' 
--     WHEN @tintOppType = 2 THEN 'Purchase Order' END AS [Type]
-- from #tempTable where ID > @firstRec and ID < @lastRec                        
--set @TotRecs=(select count(*) from #tempTable)                        
--drop table #tempTable
END 
--For bills
IF @tintOppType =  3 
BEGIN
-- SELECT 'Bill' AS [Type],OBD.vcMemo,OBD.vcReference,OBD.monAmount,dbo.FormatedDateFromDate(PD.dtDueDate,OBD.numDomainId) DueDate
-- FROM   dbo.OpportunityBizDocsDetails OBD
--        INNER JOIN dbo.OpportunityBizDocsPaymentDetails PD ON PD.numBizDocsPaymentDetId = OBD.numBizDocsPaymentDetId
--  WHERE OBD.numDivisionID = @numDivisionID AND PD.bitIntegrated=0

SELECT 0 AS numOppId,'Bill' vcPOppName,0 AS numOppBizDocsId,'Bill' + CASE WHEN len(BH.vcReference)=0 THEN '' ELSE '-' + BH.vcReference END AS [vcBizDocID],
						   BH.monAmountDue as monDealAmount,
						   ISNULL(BH.monAmtPaid, 0) as monAmountPaid,
					       ISNULL(BH.monAmountDue, 0) - ISNULL(BH.monAmtPaid, 0) as BalanceDue,
					       [dbo].[FormatedDateFromDate](BH.dtBillDate,BH.numDomainID) AS BillingDate,
						   [dbo].[FormatedDateFromDate](BH.dtDueDate,BH.numDomainID) AS DueDate
						   FROM    
							BillHeader BH 
							WHERE BH.numDomainId=@numDomainID AND BH.numDivisionId = @numDivisionID
							AND ISNULL(BH.monAmountDue, 0) - ISNULL(BH.monAmtPaid, 0) > 0
							
END



--SELECT * FROM Category
--- Alterd By Chintan Prajapati
-- EXEC dbo.USP_ItemsForECommerce 9,60,1,1,15,0,''
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_ItemsAvailablityOpenPO' ) 
    DROP PROCEDURE USP_ItemsAvailablityOpenPO
GO
CREATE PROCEDURE [dbo].[USP_ItemsAvailablityOpenPO]
    @numItemCode NUMERIC(18,0)=0,
    @numDomainID NUMERIC(18,0)=0
   
AS 
    BEGIN
			select distinct
			W.numOnHand,
			W.numOnOrder,
			WH.vcWareHouse +' '+ (CASE WHEN W.numWLocationID=-1 THEN 'Global' WHEN W.numWLocationID=0 THEN '' ELSE WL.vcLocation END) AS vcWareHouse,
			(select TOP 1 bintCreatedDate FROM OpportunityMaster where tintShipped=0 AND tintOppType=2 AND numDomainId=@numDomainID AND numOppId IN (SELECT numOppId FROM OpportunityItems where numItemCode=@numItemCode) ORDER BY bintCreatedDate desc) as bintCreatedDate,
			(select TOP 1 dtDeliveryDate FROM OpportunityBizDocs where dtDeliveryDate IS NOT NULL AND numOppId=(select TOP 1 numOppId FROM OpportunityMaster where tintShipped=0 AND tintOppType=2 AND numDomainId=@numDomainID AND numOppId IN (SELECT numOppId FROM OpportunityItems where numItemCode=@numItemCode))) AS dtDeliveryDate,
			ISNULL(DATEDIFF(DAY,CAST((select TOP 1 bintCreatedDate FROM OpportunityMaster where tintShipped=0 AND numDomainId=@numDomainID AND tintOppType=2 AND numOppId IN (SELECT numOppId FROM OpportunityItems where numItemCode=@numItemCode) ORDER BY bintCreatedDate desc) AS DATE),
			CAST((select TOP 1 dtDeliveryDate FROM OpportunityBizDocs where dtDeliveryDate IS NOT NULL AND numOppId=(select TOP 1 numOppId FROM OpportunityMaster where tintShipped=0 AND tintOppType=2 AND numDomainId=@numDomainID AND numOppId IN (SELECT numOppId FROM OpportunityItems where numItemCode=@numItemCode))) AS DATE)),0) AS LeadDays
	from 
			WarehouseItems  AS W
		LEFT JOIN
			Warehouses AS WH ON WH.numWareHouseId=W.numWareHouseId
		LEFT JOIN
			WarehouseLocation AS WL ON WL.numWLocationID=W.numWLocationID
		WHERE 
			W.numItemID=@numItemCode
    END




GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_ManufacturerList')
DROP PROCEDURE USP_ManufacturerList
GO
-- =============================================  
-- Modified by: <Author,,Sachin Sadhu>  
-- Create date: <Create Date,,24thDec2013>  
-- Description: <Description,,add 3 fields:vcBizDocImagePath,vcBizDocFooter,vcPurBizDocFooter>  
-- =============================================  
CREATE PROCEDURE [dbo].[USP_ManufacturerList]
(
          @numDomainID     AS NUMERIC(9)  = 0,
          @numRelationshipID   AS NUMERIC(9)  = 0,
		  @numShowOnWebField AS NUMERIC(18,0)=0,
		  @numRecordID AS NUMERIC(18,0)=0
)
AS
BEGIN
IF(@numRecordID=0)
BEGIN
	SELECT 
		C.vcCompanyName,C.txtComments,
		(select TOP 1
		  VcFileName
		  from dbo.GenericDocuments          
		  where numRecID=D.numDivisionID       
		  and  vcDocumentSection='A'   and numDomainID=@numDomainID    and vcfiletype IN ('.jpg','.jpeg','.png') 
		 )  AS VcFileName,
		 D.numDivisionID
	FROM 
		CompanyInfo AS C 
	LEFT JOIN
		DivisionMaster AS D
	ON
		C.numCompanyId=D.numCompanyID
	WHERE
		C.numCompanyType=@numRelationshipID AND C.numDomainID=@numDomainID AND
		ISNULL((select TOP 1 Fld_Value from CFW_FLD_Values where  RecId=D.numDivisionID AND Fld_ID=@numShowOnWebField),0)=1
END
ELSE
BEGIN
		SELECT 
		C.vcCompanyName,C.txtComments,
		(select TOP 1
		  VcFileName
		  from dbo.GenericDocuments          
		  where numRecID=@numRecordID
		  and  vcDocumentSection='A'   and numDomainID=@numDomainID    and vcfiletype IN ('.jpg','.jpeg','.png') 
		 )  AS VcFileName,
		 D.numDivisionID
	FROM 
		CompanyInfo AS C 
	LEFT JOIN
		DivisionMaster AS D
	ON
		C.numCompanyId=D.numCompanyID
	WHERE
		C.numDomainID=@numDomainID AND D.numDivisionID=@numRecordID
END
END
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_UpdateShippingItemPrice')
DROP PROCEDURE USP_UpdateShippingItemPrice
GO
CREATE PROCEDURE USP_UpdateShippingItemPrice
    @numOppId NUMERIC(9)=0,
    @numItemCode NUMERIC(9)=0,
	@numItemPrice NUMERIC(18,2)=0
AS 
    BEGIN
       UPDATE 
			OpportunityItems 
	   SET
			monPrice=@numItemPrice,
			monTotAmount=@numItemPrice,
			monTotAmtBefDiscount=@numItemPrice 
		WHERE 
			numOppId=@numOppId and numItemCode=@numItemCode
    END
