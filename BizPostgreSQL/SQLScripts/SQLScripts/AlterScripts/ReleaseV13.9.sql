/******************************************************************
Project: Release 13.9 Date: 26.AUG.2020
Comments: ALTER SCRIPTS
*******************************************************************/

/******************************************** SANDEEP *********************************************/

ALTER TABLE MassSalesFulfillmentConfiguration ADD bitAutoWarehouseSelection BIT DEFAULT 0
ALTER TABLE MassSalesFulfillmentConfiguration ADD bitBOOrderStatus BIT DEFAULT 0
ALTER TABLE MassSalesFulfillmentConfiguration DROP COLUMN numUserCntID
ALTER TABLE OpportunityMaster ADD monMarketplaceShippingCost DECIMAL(20,5)
ALTER TABLE OpportunityItems ADD bitRequiredWarehouseCorrection BIT DEFAULT NULL
ALTER TABLE OpportunityItems ADD numShipFromWarehouse NUMERIC(18,0)
ALTER TABLE Domain ADD vcMarketplaces VARCHAR(500)

UPDATE DycFormField_Mapping SET vcFieldName='Deliver By (Status)',bitAllowSorting=0,bitAllowFiltering=1,vcAssociatedControlType='SelectBox' WHERE numFormID=141 AND numFieldID IN (SELECT numFieldID FROM DycFieldMaster WHERE vcOrigDbColumnName='dtAnticipatedDelivery')

INSERT INTO DycFormField_Mapping
(
	numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,[order],tintRow,tintColumn,bitInResults,bitDeleted,bitDefault,bitSettingField,bitAllowSorting,bitAllowFiltering
)
VALUES
(
	3,97,141,0,0,'Order Source','SelectBox',39,39,1,1,0,0,1,1,1
),
(
	3,199,141,0,1,'Ship-From Location','SelectBox',40,40,1,1,0,0,1,1,0
)

UPDATE DycFormField_Mapping SET bitAllowFiltering=1 WHERE numFormID=141 AND numFieldID=101

-----------------------------------------------------

USE [Production.2014]
GO

/****** Object:  Table [dbo].[eChannelHub]    Script Date: 12-Aug-20 4:40:25 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[eChannelHub](
	[ID] [int] NOT NULL,
	[vcMarketplace] [varchar](100) NOT NULL,
	[vcType] [varchar](100) NOT NULL,
 CONSTRAINT [PK_eChannelHub] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING ON
GO

-------------------------------------------------------------

INSERT INTO eChannelHub (ID,vcMarketplace,vcType) VALUES (1,'Amazon AU','Marketplaces')
INSERT INTO eChannelHub (ID,vcMarketplace,vcType) VALUES (2,'Amazon CAN','Marketplaces')
INSERT INTO eChannelHub (ID,vcMarketplace,vcType) VALUES (3,'Amazon UK','Marketplaces')
INSERT INTO eChannelHub (ID,vcMarketplace,vcType) VALUES (4,'Amazon US','Marketplaces')
INSERT INTO eChannelHub (ID,vcMarketplace,vcType) VALUES (5,'Bol.com','Marketplaces')
INSERT INTO eChannelHub (ID,vcMarketplace,vcType) VALUES (6,'Bonanza','Marketplaces')
INSERT INTO eChannelHub (ID,vcMarketplace,vcType) VALUES (7,'Cdiscount','Marketplaces')
INSERT INTO eChannelHub (ID,vcMarketplace,vcType) VALUES (8,'eBay AU','Marketplaces')
INSERT INTO eChannelHub (ID,vcMarketplace,vcType) VALUES (9,'eBay CAN','Marketplaces')
INSERT INTO eChannelHub (ID,vcMarketplace,vcType) VALUES (10,'eBay UK','Marketplaces')
INSERT INTO eChannelHub (ID,vcMarketplace,vcType) VALUES (11,'eBay US','Marketplaces')
INSERT INTO eChannelHub (ID,vcMarketplace,vcType) VALUES (12,'eCrater','Marketplaces')
INSERT INTO eChannelHub (ID,vcMarketplace,vcType) VALUES (13,'Etsy','Marketplaces')
INSERT INTO eChannelHub (ID,vcMarketplace,vcType) VALUES (14,'Google Express','Marketplaces')
INSERT INTO eChannelHub (ID,vcMarketplace,vcType) VALUES (15,'Hayneedle','Marketplaces')
INSERT INTO eChannelHub (ID,vcMarketplace,vcType) VALUES (16,'Houzz','Marketplaces')
INSERT INTO eChannelHub (ID,vcMarketplace,vcType) VALUES (17,'iOffer','Marketplaces')
INSERT INTO eChannelHub (ID,vcMarketplace,vcType) VALUES (18,'Massgenie','Marketplaces')
INSERT INTO eChannelHub (ID,vcMarketplace,vcType) VALUES (19,'Mercado libre','Marketplaces')
INSERT INTO eChannelHub (ID,vcMarketplace,vcType) VALUES (20,'Newegg','Marketplaces')
INSERT INTO eChannelHub (ID,vcMarketplace,vcType) VALUES (21,'Opensky','Marketplaces')
INSERT INTO eChannelHub (ID,vcMarketplace,vcType) VALUES (22,'Overstock','Marketplaces')
INSERT INTO eChannelHub (ID,vcMarketplace,vcType) VALUES (23,'Pricefalls','Marketplaces')
INSERT INTO eChannelHub (ID,vcMarketplace,vcType) VALUES (24,'Rakuten','Marketplaces')
INSERT INTO eChannelHub (ID,vcMarketplace,vcType) VALUES (25,'Reverb','Marketplaces')
INSERT INTO eChannelHub (ID,vcMarketplace,vcType) VALUES (26,'Sears','Marketplaces')
INSERT INTO eChannelHub (ID,vcMarketplace,vcType) VALUES (27,'Storenvy','Marketplaces')
INSERT INTO eChannelHub (ID,vcMarketplace,vcType) VALUES (28,'Tanga','Marketplaces')
INSERT INTO eChannelHub (ID,vcMarketplace,vcType) VALUES (29,'Tophatter','Marketplaces')
INSERT INTO eChannelHub (ID,vcMarketplace,vcType) VALUES (30,'Trademe','Marketplaces')
INSERT INTO eChannelHub (ID,vcMarketplace,vcType) VALUES (31,'Walmart AU','Marketplaces')
INSERT INTO eChannelHub (ID,vcMarketplace,vcType) VALUES (32,'Walmart CAN','Marketplaces')
INSERT INTO eChannelHub (ID,vcMarketplace,vcType) VALUES (33,'Walmart UK','Marketplaces')
INSERT INTO eChannelHub (ID,vcMarketplace,vcType) VALUES (34,'Walmart US','Marketplaces')
INSERT INTO eChannelHub (ID,vcMarketplace,vcType) VALUES (35,'Wish','Marketplaces')

INSERT INTO eChannelHub (ID,vcMarketplace,vcType) VALUES (36,'Bing Shopping','Comparison Shopping')
INSERT INTO eChannelHub (ID,vcMarketplace,vcType) VALUES (37,'Connexity','Comparison Shopping')
INSERT INTO eChannelHub (ID,vcMarketplace,vcType) VALUES (38,'Gifts.com','Comparison Shopping')
INSERT INTO eChannelHub (ID,vcMarketplace,vcType) VALUES (39,'Google Shopping','Comparison Shopping')
INSERT INTO eChannelHub (ID,vcMarketplace,vcType) VALUES (40,'LensShopper','Comparison Shopping')
INSERT INTO eChannelHub (ID,vcMarketplace,vcType) VALUES (41,'NexTag','Comparison Shopping')
INSERT INTO eChannelHub (ID,vcMarketplace,vcType) VALUES (42,'PriceGrabber','Comparison Shopping')
INSERT INTO eChannelHub (ID,vcMarketplace,vcType) VALUES (43,'PriceRunner','Comparison Shopping')
INSERT INTO eChannelHub (ID,vcMarketplace,vcType) VALUES (44,'Shopping.com','Comparison Shopping')
INSERT INTO eChannelHub (ID,vcMarketplace,vcType) VALUES (45,'Shopzilla','Comparison Shopping')


INSERT INTO eChannelHub (ID,vcMarketplace,vcType) VALUES (46,'3D Cart','3rd Party Shopping Carts')
INSERT INTO eChannelHub (ID,vcMarketplace,vcType) VALUES (47,'AmeriCommerce','3rd Party Shopping Carts') 
INSERT INTO eChannelHub (ID,vcMarketplace,vcType) VALUES (48,'Big Commerce','3rd Party Shopping Carts')
INSERT INTO eChannelHub (ID,vcMarketplace,vcType) VALUES (49,'Magento','3rd Party Shopping Carts')
INSERT INTO eChannelHub (ID,vcMarketplace,vcType) VALUES (50,'OpenCart','3rd Party Shopping Carts')
INSERT INTO eChannelHub (ID,vcMarketplace,vcType) VALUES (51,'OS Commerce','3rd Party Shopping Carts')
INSERT INTO eChannelHub (ID,vcMarketplace,vcType) VALUES (52,'Presta Shop','3rd Party Shopping Carts')
INSERT INTO eChannelHub (ID,vcMarketplace,vcType) VALUES (53,'Shopify','3rd Party Shopping Carts')
INSERT INTO eChannelHub (ID,vcMarketplace,vcType) VALUES (54,'Shopify Plus','3rd Party Shopping Carts')
INSERT INTO eChannelHub (ID,vcMarketplace,vcType) VALUES (55,'Volusion','3rd Party Shopping Carts')
INSERT INTO eChannelHub (ID,vcMarketplace,vcType) VALUES (56,'Woo Commerce','3rd Party Shopping Carts')
INSERT INTO eChannelHub (ID,vcMarketplace,vcType) VALUES (57,'X Cart','3rd Party Shopping Carts')
INSERT INTO eChannelHub (ID,vcMarketplace,vcType) VALUES (58,'ZenCart','3rd Party Shopping Carts')

-------------------------------

USE [Production.2014]
GO

/****** Object:  Table [dbo].[OpportunityMasterShippingRateError]    Script Date: 26-Aug-20 11:57:56 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[OpportunityMasterShippingRateError](
	[ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[numDomainID] [numeric](18, 0) NOT NULL,
	[numOppID] [numeric](18, 0) NOT NULL,
	[intNoOfTimeFailed] [tinyint] NOT NULL,
	[vcErrorMessage] [varchar](max) NOT NULL,
	[dtLastExecutedDate] [datetime] NOT NULL,
 CONSTRAINT [PK_OpportunityMasterShippingRateError] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

SET ANSI_PADDING ON
GO


---------------------------

USE [Production.2014]
GO

/****** Object:  Table [dbo].[MassSalesFulfillmentShippingConfig]    Script Date: 26-Aug-20 12:00:47 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[MassSalesFulfillmentShippingConfig](
	[ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[numDomainID] [numeric](18, 0) NOT NULL,
	[numOrderSource] [numeric](18, 0) NOT NULL,
	[tintSourceType] [tinyint] NOT NULL,
	[tintType] [tinyint] NOT NULL,
	[vcPriorities] [varchar](50) NOT NULL,
	[numShipVia] [numeric](18, 0) NOT NULL,
	[numShipService] [numeric](18, 0) NOT NULL,
	[bitOverride] [bit] NOT NULL,
	[fltWeight] [float] NOT NULL,
	[numShipViaOverride] [numeric](18, 0) NOT NULL,
 CONSTRAINT [PK_MassSalesFulfillmentShippingConfig] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING ON
GO

---------------------------

USE [Production.2014]
GO

/****** Object:  Table [dbo].[MassSalesFulfillmentWM]    Script Date: 26-Aug-20 1:52:47 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[MassSalesFulfillmentWM](
	[ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[numDomainID] [numeric](18, 0) NOT NULL,
	[numOrderSource] [numeric](18, 0) NOT NULL,
	[numCountryID] [numeric](18, 0) NOT NULL,
	[tintSourceType] [tinyint] NOT NULL,
 CONSTRAINT [PK_MassSalesFulfillmentWM] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO


------------------------------------

USE [Production.2014]
GO

/****** Object:  Table [dbo].[MassSalesFulfillmentWMState]    Script Date: 26-Aug-20 1:53:10 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[MassSalesFulfillmentWMState](
	[ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[numMSFWMID] [numeric](18, 0) NOT NULL,
	[numStateID] [numeric](18, 0) NOT NULL,
 CONSTRAINT [PK_MassSalesFulfillmentWMState] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[MassSalesFulfillmentWMState]  WITH CHECK ADD  CONSTRAINT [FK_MassSalesFulfillmentWMState_MassSalesFulfillmentWM] FOREIGN KEY([numMSFWMID])
REFERENCES [dbo].[MassSalesFulfillmentWM] ([ID])
ON DELETE CASCADE
GO

ALTER TABLE [dbo].[MassSalesFulfillmentWMState] CHECK CONSTRAINT [FK_MassSalesFulfillmentWMState_MassSalesFulfillmentWM]
GO


-----------------------

USE [Production.2014]
GO

/****** Object:  Table [dbo].[MassSalesFulfillmentWP]    Script Date: 26-Aug-20 1:54:50 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[MassSalesFulfillmentWP](
	[ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[numMSFWMID] [numeric](18, 0) NOT NULL,
	[numWarehouseID] [numeric](18, 0) NOT NULL,
	[intOrder] [int] NOT NULL,
 CONSTRAINT [PK_MassSalesFulfillmentWP] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[MassSalesFulfillmentWP]  WITH CHECK ADD  CONSTRAINT [FK_MassSalesFulfillmentWP_MassSalesFulfillmentWM] FOREIGN KEY([numMSFWMID])
REFERENCES [dbo].[MassSalesFulfillmentWM] ([ID])
ON DELETE CASCADE
GO

ALTER TABLE [dbo].[MassSalesFulfillmentWP] CHECK CONSTRAINT [FK_MassSalesFulfillmentWP_MassSalesFulfillmentWM]
GO


