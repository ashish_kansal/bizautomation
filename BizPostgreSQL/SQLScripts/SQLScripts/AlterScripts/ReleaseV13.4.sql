/******************************************************************
Project: Release 13.4 Date: 09.APR.2020
Comments: ALTER SCRIPTS
*******************************************************************/

/******************************************** SATVA BIZCART ELASTIC SEARCH *********************************************/


SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[ElasticSearchBizCart](
	[numElasticSearchBizCartID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[vcModule] [varchar](50) NULL,
	[numRecordID] [numeric](18, 0) NULL,
	[vcAction] [varchar](50) NULL,
	[numDomainID] [numeric](18, 0) NULL,
	[numSiteID] [numeric](18, 0) NULL,
 CONSTRAINT [PK_ElasticSearchBizCart] PRIMARY KEY CLUSTERED 
(
	[numElasticSearchBizCartID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE eCommerceDTL ADD bitElasticSearch BIT


/******************************************** SANDEEP *********************************************/

ALTER TABLE Domain ADD bitCloneLocationWithItem BIT
ALTER TABLE OpportunityItems ADD dtPlannedStart DATETIME


/******************************************** PRASHANT *********************************************/


ALTER TABLE Domain ADD bitUseOnlyActionItems BIT DEFAULT 0
UPDATE DynamicFormMaster SET vcFormName='Activity Grid Columns' WHERE  vcFormName='Tickler Grid Columns'
UPDATE BizFormWizardModule SET vcModule='Projects, Cases & Activities' WHERE vcModule='Projects, Cases, & Tickler'