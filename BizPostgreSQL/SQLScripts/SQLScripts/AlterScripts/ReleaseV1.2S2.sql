/******************************************************************
Project: Inventory Adjustment Date: 19.01.2013
Comments: 
*******************************************************************/
BEGIN TRANSACTION
GO
UPDATE dbo.General_Journal_Details SET varDescription= 'Inventory_Adjustment' WHERE numItemID>0 AND numJournalId IN (SELECT numJournal_Id FROM dbo.General_Journal_Header WHERE varDescription LIKE 'Stock Adjustment:%') 

SELECT * FROM dbo.General_Journal_Details WHERE varDescription <> 'Inventory_Adjustment' AND numJournalId IN (SELECT numJournal_Id FROM dbo.General_Journal_Header WHERE varDescription LIKE 'Stock Adjustment:%') 
--UPDATE dbo.General_Journal_Details SET varDescription= 'Inventory_Adjustment' WHERE numItemID>0 AND numJournalId IN (SELECT numJournal_Id FROM dbo.General_Journal_Header WHERE varDescription LIKE 'Stock Adjustment:%') 
ROLLBACK TRANSACTION


/******************************************************************
Project: BACRMUI/Bizservice   Date: 25.01.2013
Comments: General Function used for splitting large string
*******************************************************************/
BEGIN TRANSACTION

CREATE TABLE Numbers
(
	Number INT NOT NULL,
	CONSTRAINT PK_Numbers 
		PRIMARY KEY CLUSTERED (Number)
		WITH FILLFACTOR = 100
)

INSERT INTO Numbers
SELECT
	(a.Number * 256) + b.Number AS Number
FROM 
	(
		SELECT number
		FROM master..spt_values
		WHERE 
			type = 'P'
			AND number <= 255
	) a (Number),
	(
		SELECT number
		FROM master..spt_values
		WHERE 
			type = 'P'
			AND number <= 255
	) b (Number)
GO
ROLLBACK


/******************************************************************
Project: Bizservice   Date: 23.01.2013
Comments: Added Excel Row number into Import File Master to track records for starting interrupt
*******************************************************************/
BEGIN TRANSACTION

ALTER TABLE Import_File_Master ADD numProcessedCSVRowNumber NUMERIC(9)

ROLLBACK


/******************************************************************
Project: BizService   Date: 23.01.2013
Comments: 
----------------- BACRMUI : Add this keys to web.Config ------------------------
*******************************************************************/

<add key="MaxUploadFileSize" value="51200"></add> <!--File size in kb-->
<add key="MaxFileRecords" value="10000"></add> <!--File Records count-->

-- Put this code block into <system.webServer> section in web.config file of BACRMUI (52428800 bytes = 50 MB)
<security>
    <requestFiltering>
        <requestLimits maxAllowedContentLength="52428800" maxUrl="4096" />
    </requestFiltering>
</security>
-----------------------------------------------------------------------------------

/******************************************************************
Project: Bizservice   Date: 23.01.2013
Comments: Update vcPropertyName for 'Bill To Country'
*******************************************************************/
BEGIN TRANSACTION

UPDATE dbo.DycFormField_Mapping SET vcPropertyName = 'Country'
WHERE vcFieldName = 'Bill To Country' AND numFormID = 36

ROLLBACK



/******************************************************************
Project: Bizservice   Date: 21.01.2013
Comments: Added 2 new fields into configuration
*******************************************************************/
BEGIN TRANSACTION

-------- INSERT '' Field in DycFieldMaster
INSERT INTO dbo.DycFieldMaster 
(numModuleID,numDomainID,vcFieldName,vcDbColumnName,vcOrigDbColumnName,vcPropertyName,vcLookBackTableName,vcFieldDataType,vcFieldType,vcAssociatedControlType,vcToolTip,
vcListItemType,numListID,PopupFunctionName,[order],tintRow,tintColumn,bitInResults,bitDeleted,bitAllowEdit,bitDefault,bitSettingField,bitAddField,bitDetailField,
bitAllowSorting,bitWorkFlowField,bitImport,bitExport,bitAllowFiltering,bitInlineEdit,bitRequired,intColumnWidth,intFieldMaxLength) 
SELECT 2,	NULL,	'Relationship Type',	'tintCRMType',	'tintCRMType',	'CRMType',	'DivisionMaster',	'N',	'R',	'SelectBox',	NULL,		'',		0,	'',	0,	0,	0,	0,	0,	0,	0,	1,	0,	0,	0,	0,	0,	0,	0,	0,	0,0,0

-------- INSERT 'Relationship Type','Group' Fields in DycFormField_Mapping
DECLARE @numRelationShipTypeFieldID AS NUMERIC(9)
SELECT @numRelationShipTypeFieldID = MAX(numFieldID) FROM dbo.DycFieldMaster WHERE vcDbColumnName = 'tintCRMType' AND vcFieldName = 'Relationship Type'
PRINT @numRelationShipTypeFieldID

DECLARE @numGroupIDFieldID AS NUMERIC(9)
SELECT @numGroupIDFieldID = MAX(numFieldID) FROM dbo.DycFieldMaster WHERE vcDbColumnName = 'numGrpID' AND vcFieldName = 'Group'
PRINT @numGroupIDFieldID

INSERT INTO dbo.DycFormField_Mapping 
(numModuleID,numFieldID,numDomainID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,PopupFunctionName,[order],tintRow,tintColumn,
bitInResults,bitDeleted,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitWorkFlowField,bitImport,bitExport,bitAllowFiltering,bitRequired,
numFormFieldID,intSectionID,bitAllowGridColor )
SELECT  2,@numRelationShipTypeFieldID,NULL,36,0,0,'Relationship Type','SelectBox','CRMType','',0,0,0,	0,	0,	0,	1,	0,	0,	0,	0,	1,	0,	0,	0,	0,	1,	0
UNION ALL
SELECT 2,@numGroupIDFieldID,NULL,36,1,1,'Group','SelectBox','GroupID',NULL,17,20,2,	1,	0,	0,	1,	0,	0,	0,	0,	1,	NULL,	1,	0,	0,	1,	0


--SELECT * FROM dbo.DycFieldMaster WHERE vcFieldName LIKE '%Relationship Type%'
--SELECT * FROM DycFormField_Mapping WHERE vcFieldName IN ('Relationship Type') AND numFormID = 36
--SELECT * FROM DycFormField_Mapping WHERE vcFieldName IN ('Group') AND numFormID = 36

ROLLBACK




ALTER TABLE dbo.Item ADD
	bitAllowDropShip bit NULL
	
---------------------------------------------------------------------------------------------		
	

INSERT INTO [dbo].[DycFieldMaster]([numModuleID], [numDomainID], [vcFieldName], [vcDbColumnName], [vcOrigDbColumnName], [vcPropertyName], [vcLookBackTableName], [vcFieldDataType], [vcFieldType], [vcAssociatedControlType], 
[vcToolTip], [vcListItemType], [numListID], [PopupFunctionName], [order], [tintRow], [tintColumn], 
[bitInResults], [bitDeleted], [bitAllowEdit], [bitDefault], [bitSettingField], [bitAddField], 
[bitDetailField], [bitAllowSorting],bitWorkFlowField,bitAllowFiltering )
SELECT 3, NULL, N'Tracking No', N'vcTrackingNo', N'vcTrackingNo', 'TrackingNo', N'OpportunityBizDocs', N'V', N'R',
 N'Label', NULL, N'', 0, N'', 16, 14, 1, 1, 0, 0, 0,0, 0, 1, 0,0,0
		
	
		
insert into DycFormField_Mapping (numModuleID,numFieldID,numFormID,vcFieldName,vcAssociatedControlType,PopupFunctionName,bitSettingField,bitAddField,bitDetailField,bitAllowEdit,bitInlineEdit,vcPropertyName,[order],tintRow,tintColumn)
select numModuleID,numFieldID,38,'Tracking No',vcAssociatedControlType,PopupFunctionName,bitSettingField,0,bitDetailField,0,0,vcPropertyName,36,36,1
from DycFieldMaster where [numModuleID]=3 and  [vcDbColumnName]='vcTrackingNo' AND vcLookBackTableName='OpportunityBizDocs'
UNION 
select numModuleID,numFieldID,39,'Tracking No',vcAssociatedControlType,PopupFunctionName,bitSettingField,0,bitDetailField,0,0,vcPropertyName,36,36,1
from DycFieldMaster where [numModuleID]=3 and  [vcDbColumnName]='vcTrackingNo' AND vcLookBackTableName='OpportunityBizDocs'
UNION 
select numModuleID,numFieldID,40,'Tracking No',vcAssociatedControlType,PopupFunctionName,bitSettingField,0,bitDetailField,0,0,vcPropertyName,36,36,1
from DycFieldMaster where [numModuleID]=3 and  [vcDbColumnName]='vcTrackingNo' AND vcLookBackTableName='OpportunityBizDocs'
UNION 
select numModuleID,numFieldID,41,'Tracking No',vcAssociatedControlType,PopupFunctionName,bitSettingField,0,bitDetailField,0,0,vcPropertyName,36,36,1
from DycFieldMaster where [numModuleID]=3 and  [vcDbColumnName]='vcTrackingNo' AND vcLookBackTableName='OpportunityBizDocs'
		
		
-------------Authoritative bizdoc's journal entries are not being created on Billing Date---------------------------------------------------------------------------------		


BEGIN TRANSACTION 


SELECT CONVERT(DATETIME , CONVERT(VARCHAR(10),obd.dtFromDate,111)) dtFromDate,
GJH.numJournal_Id INTO #temp
FROM dbo.OpportunityBizDocs OBD JOIN dbo.General_Journal_Header GJH ON OBD.numOppId=GJH.numOppId
AND obd.numOppBizDocsId=GJH.numOppBizDocsId WHERE 
CONVERT(VARCHAR(10),obd.dtFromDate,111)!=CONVERT(VARCHAR(10),GJH.datEntry_Date,111)
AND isnull(Gjh.numBillID,0)=0 AND isnull(gjh.numBillPaymentID,0)=0 AND isnull(GJH.numPayrollDetailID,0)=0
AND isnull(Gjh.numCheckHeaderID,0)=0 AND isnull(gjh.numReturnID,0)=0 
AND isnull(GJH.numDepositId,0)=0 and monDealAMount=numAmount 
AND OBD.bitAuthoritativeBizDocs=1 AND numJournal_Id!=12264
order by GJH.datEntry_Date 

--SELECT *,CAST(dtFromDate AS SMALLDATETIME) FROM #temp 

UPDATE GJH SET datEntry_Date=CAST(dtFromDate AS SMALLDATETIME)
FROM #temp OBD JOIN dbo.General_Journal_Header GJH ON OBD.numJournal_Id=GJH.numJournal_Id

SELECT CONVERT(VARCHAR(10),obd.dtCreatedDate,111) dtCreatedDate,CONVERT(VARCHAR(10),obd.dtFromDate,111) dtFromDate,CONVERT(VARCHAR(10),GJH.datEntry_Date,111) datEntry_Date
,GJH.* 
FROM dbo.OpportunityBizDocs OBD JOIN dbo.General_Journal_Header GJH ON OBD.numOppId=GJH.numOppId
AND obd.numOppBizDocsId=GJH.numOppBizDocsId WHERE 
CONVERT(VARCHAR(10),obd.dtFromDate,111)!=CONVERT(VARCHAR(10),GJH.datEntry_Date,111)
AND isnull(Gjh.numBillID,0)=0 AND isnull(gjh.numBillPaymentID,0)=0 AND isnull(GJH.numPayrollDetailID,0)=0
AND isnull(Gjh.numCheckHeaderID,0)=0 AND isnull(gjh.numReturnID,0)=0 
AND isnull(GJH.numDepositId,0)=0 and monDealAMount=numAmount 
AND OBD.bitAuthoritativeBizDocs=1 AND numJournal_Id!=12264
order by GJH.datEntry_Date 

DROP TABLE #temp

ROLLBACK TRANSACTION 		


---------------------------------------------------------------------------------------------		

--SELECT * FROM dbo.DycFormConfigurationDetails WHERE numformid=56 AND intColumnNum=0
UPDATE dbo.DycFormConfigurationDetails SET intColumnNum=1 WHERE numformid=56 AND intColumnNum=0

---------------------------------------------------------------------------------------------		


/******************************************************************
Project: Online Marketplace Order Processing    Date: 21/01/2013
Comments: It used to depend on BizDoc items mapping for updating marketplace orders. 
now made it depend on Order Items to update tracking number.
*******************************************************************/
BEGIN TRANSACTION
GO

ALTER TABLE dbo.WebApiDetail ADD
numFBAOrderStatus NUMERIC(18,0) NULL

GO

ALTER TABLE dbo.WebApiDetail ADD
numUnShipmentOrderStatus NUMERIC(18,0) NULL

ROLLBACK TRANSACTION


/******************************************************************
Project: Online Marketplace Order Processing    Date: 21/01/2013
Comments: It used to depend on BizDoc items mapping for updating marketplace orders. 
now made it depend on Order Items to update tracking number.
*******************************************************************/
BEGIN TRANSACTION
GO

sp_Rename 'dbo.WebApiOppItemDetails.numOppBizDocID', 'numOppItemID'

ALTER TABLE dbo.WebApiOppItemDetails
    DROP COLUMN numOppBizDocItemID
GO
ROLLBACK TRANSACTION

/******************************************************************
Project:    Date: 23-1-2013
Comments: Note : After adding Error message it requires to click get Error Message button on Maintanance Page .
*******************************************************************/
--AppleNv web.config replace SMTPLicense with this for sending email of Mirror Bizdoc
<add key="SMTPLicense" value="MN700-D686805186065845A8EFDF190775-AA3C" />

BEGIN TRANSACTION
        ALTER TABLE dbo.SitePages
		ADD bitIsMaintainScroll bit NOT NULL
		CONSTRAINT MaintainScroll_Default_Constraint  DEFAULT 1
GO
ROLLBACK TRANSACTION


BEGIN TRANSACTION
UPDATE dbo.SitePages SET bitIsMaintainScroll = 1 WHERE numSiteID = 60 AND numDomainID = 156
GO
ROLLBACK TRANSACTION

BEGIN TRANSACTION
UPDATE dbo.SitePages SET bitIsMaintainScroll = 0 WHERE numSiteID <> 60 AND numDomainID <> 156
GO
ROLLBACK TRANSACTION


ALTER TABLE eCommercePaymentConfig
ADD  numFailedOrderStatus NUMERIC(18,0) null



--Note Fire only this script for 25-1-2013 Demo
BEGIN TRANSACTION
DELETE FROM dbo.ShippingServiceTypes WHERE numDomainID = 0 AND numRuleID IS NOT NULL
GO
ROLLBACK TRANSACTION
  