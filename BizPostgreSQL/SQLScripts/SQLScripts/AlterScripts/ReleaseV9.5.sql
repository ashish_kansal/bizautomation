/******************************************************************
Project: Release 9.5 Date: 09.APR.2018
Comments: ALTER SCRIPTS
*******************************************************************/

/*************************** SANDEEP *******************************/

ALTER TABLE Domain ADD bitAllowDuplicateLineItems BIT DEFAULT 0
ALTER TABLE [dbo].[OpportunityItemsReleaseDates] ALTER COLUMN [numQty] NUMERIC(18,0) NULL
ALTER TABLE [dbo].[OpportunityItemsReleaseDates] ALTER COLUMN [tintStatus] TINYINT NULL