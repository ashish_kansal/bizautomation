------------------------------
/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
CREATE TABLE dbo.Tmp_CartItems
	(
	numCartId numeric(18, 0) NOT NULL IDENTITY (1, 1) NOT FOR REPLICATION,
	numUserCntId numeric(18, 0) NOT NULL,
	numDomainId numeric(18, 0) NOT NULL,
	vcCookieId varchar(100) NOT NULL,
	numOppItemCode numeric(18, 0) NULL,
	numItemCode numeric(18, 0) NULL,
	numUnitHour numeric(18, 0) NULL,
	monPrice money NULL,
	numSourceId numeric(18, 0) NULL,
	vcItemDesc varchar(2000) NULL,
	numWarehouseId numeric(18, 0) NULL,
	vcItemName varchar(200) NULL,
	vcWarehouse varchar(200) NULL,
	numWarehouseItmsID numeric(18, 0) NULL,
	vcItemType varchar(200) NULL,
	vcAttributes varchar(100) NULL,
	vcAttrValues varchar(100) NULL,
	bitFreeShipping bit NULL,
	numWeight numeric(18, 2) NULL,
	tintOpFlag tinyint NULL,
	bitDiscountType bit NULL,
	fltDiscount decimal(18, 2) NULL,
	monTotAmtBefDiscount money NULL,
	ItemURL varchar(200) NULL,
	numUOM numeric(18, 0) NULL,
	vcUOMName varchar(200) NULL,
	decUOMConversionFactor decimal(18, 0) NULL,
	numHeight numeric(18, 0) NULL,
	numLength numeric(18, 0) NULL,
	numWidth numeric(18, 0) NULL,
	vcShippingMethod varchar(200) NULL,
	numServiceTypeId numeric(18, 0) NULL,
	decShippingCharge money NULL,
	numShippingCompany numeric(18, 0) NULL,
	tintServicetype tinyint NULL,
	dtDeliveryDate datetime NULL,
	monTotAmount money NULL
	)  ON [PRIMARY]
GO
SET IDENTITY_INSERT dbo.Tmp_CartItems ON
GO
IF EXISTS(SELECT * FROM dbo.CartItems)
	 EXEC('INSERT INTO dbo.Tmp_CartItems (numCartId, numUserCntId, numDomainId, vcCookieId, numOppItemCode, numItemCode, numUnitHour, monPrice, numSourceId, vcItemDesc, numWarehouseId, vcItemName, vcWarehouse, numWarehouseItmsID, vcItemType, vcAttributes, vcAttrValues, bitFreeShipping, numWeight, tintOpFlag, bitDiscountType, fltDiscount, monTotAmtBefDiscount, ItemURL, numUOM, vcUOMName, decUOMConversionFactor, numHeight, numLength, numWidth, vcShippingMethod, numServiceTypeId, decShippingCharge, numShippingCompany, tintServicetype, dtDeliveryDate, monTotAmount)
		SELECT numCartId, numUserCntId, numDomainId, vcCookieId, numOppItemCode, numItemCode, numUnitHour, monPrice, numSourceId, vcItemDesc, numWarehouseId, vcItemName, vcWarehouse, numWarehouseItmsID, vcItemType, vcAttributes, vcAttrValues, bitFreeShipping, numWeight, tintOpFlag, bitDiscountType, fltDiscount, monTotAmtBefDiscount, ItemURL, numUOM, vcUOMName, decUOMConversionFactor, numHeight, numLength, numWidth, vcShippingMethod, numServiceTypeId, decShippingCharge, numShippingCompany, tintServicetype, dtDeliveryDate, monTotAmount FROM dbo.CartItems WITH (HOLDLOCK TABLOCKX)')
GO
SET IDENTITY_INSERT dbo.Tmp_CartItems OFF
GO
DROP TABLE dbo.CartItems
GO
EXECUTE sp_rename N'dbo.Tmp_CartItems', N'CartItems', 'OBJECT' 
GO
ALTER TABLE dbo.CartItems ADD CONSTRAINT
	PK_CartDetail PRIMARY KEY CLUSTERED 
	(
	numCartId,
	numUserCntId,
	numDomainId,
	vcCookieId
	) WITH( PAD_INDEX = OFF, FILLFACTOR = 80, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
COMMIT
--------------------------
--
--ALTER TABLE BizDocTemplate ADD
--txtNewBizDocTemplate TEXT NULL
--
--
--UPDATE BizDocTemplate SET txtNewBizDocTemplate = txtBizDocTemplate
--
--
--UPDATE BizDocTemplate SET txtBizDocTemplate = REPLACE(CAST(txtBizDocTemplate AS NVARCHAR(MAX)),
--'#BillTo#','#Customer/VendorBill-toAddress#')
--WHERE txtBizDocTemplate LIKE '%#BillTo#%' AND numOppType = 1
--
--UPDATE BizDocTemplate SET txtBizDocTemplate = REPLACE(CAST(txtBizDocTemplate AS NVARCHAR(MAX)),
--'#ShipTo#','#Customer/VendorShip-toAddress#')
--WHERE txtBizDocTemplate LIKE '%#ShipTo#%' AND numOppType = 1
--
--UPDATE BizDocTemplate SET txtBizDocTemplate = REPLACE(CAST(txtBizDocTemplate AS NVARCHAR(MAX)),
--'#OrganizationName#','#Customer/VendorOrganizationName#')
--WHERE txtBizDocTemplate LIKE '%#OrganizationName#%' AND numOppType = 1
--
--UPDATE BizDocTemplate SET txtBizDocTemplate = REPLACE(CAST(txtBizDocTemplate AS NVARCHAR(MAX)),
--'#OrganizationContactName#','#Customer/VendorOrganizationContactName#')
--WHERE txtBizDocTemplate LIKE '%#OrganizationContactName#%' AND numOppType = 1
--
--UPDATE BizDocTemplate SET txtBizDocTemplate = REPLACE(CAST(txtBizDocTemplate AS NVARCHAR(MAX)),
--'#OrganizationContactEmail#','#Customer/VendorOrganizationContactEmail#')
--WHERE txtBizDocTemplate LIKE '%#OrganizationContactEmail#%' AND numOppType = 1
--
--UPDATE BizDocTemplate SET txtBizDocTemplate = REPLACE(CAST(txtBizDocTemplate AS NVARCHAR(MAX)),
--'#OrganizationContactPhone#','#Customer/VendorOrganizationContactPhone#')
--WHERE txtBizDocTemplate LIKE '%#OrganizationContactPhone#%' AND numOppType = 1
--
--
--UPDATE BizDocTemplate SET txtBizDocTemplate = REPLACE(CAST(txtBizDocTemplate AS NVARCHAR(MAX)),
--'#OrganizationPhone#','#Customer/VendorOrganizationPhone#')
--WHERE txtBizDocTemplate LIKE '%#OrganizationContactPhone#%' AND numOppType = 1
--
--
--UPDATE BizDocTemplate SET txtBizDocTemplate = REPLACE(CAST(txtBizDocTemplate AS NVARCHAR(MAX)),
--'#ChangeBillToAddress(Bill To)#','#Customer/VendorChangeBillToHeader(Bill To)#')
--WHERE txtBizDocTemplate LIKE '%#ChangeBillToAddress(Bill To)#%' AND numOppType = 1
--
--UPDATE BizDocTemplate SET txtBizDocTemplate = REPLACE(CAST(txtBizDocTemplate AS NVARCHAR(MAX)),
--'#ChangeShipToAddress(Ship To)#','#Customer/VendorChangeShipToHeader(Ship To)#')
--WHERE txtBizDocTemplate LIKE '%#ChangeShipToAddress(Ship To)#%' AND numOppType = 1
--
--UPDATE BizDocTemplate SET txtBizDocTemplate = REPLACE(CAST(txtBizDocTemplate AS NVARCHAR(MAX)),
--'#ChangeBillToAddress#','#Customer/VendorChangeBillToHeader#')
--WHERE txtBizDocTemplate LIKE '%#ChangeBillToAddress#%' AND numOppType = 1
--
--UPDATE BizDocTemplate SET txtBizDocTemplate = REPLACE(CAST(txtBizDocTemplate AS NVARCHAR(MAX)),
--'#ChangeShipToAddress#','#Customer/VendorChangeShipToHeader#')
--WHERE txtBizDocTemplate LIKE '%#ChangeShipToAddress#%' AND numOppType = 1
--
--
--UPDATE BizDocTemplate SET txtBizDocTemplate = REPLACE(CAST(txtBizDocTemplate AS NVARCHAR(MAX)),
--'#BillToAddressName#','#Customer/VendorBillToAddressName#')
--WHERE txtBizDocTemplate LIKE '%#BillToAddressName#%' AND numOppType = 1
--
--
--UPDATE BizDocTemplate SET txtBizDocTemplate = REPLACE(CAST(txtBizDocTemplate AS NVARCHAR(MAX)),
--'#ShipToAddressName#','#Customer/VendorShipToAddressName#')
--WHERE txtBizDocTemplate LIKE '%#ShipToAddressName#%' AND numOppType = 1
--
--UPDATE BizDocTemplate SET txtBizDocTemplate = REPLACE(CAST(txtBizDocTemplate AS NVARCHAR(MAX)),
--'#BillToCompanyName#','#Customer/VendorBillToCompanyName#')
--WHERE txtBizDocTemplate LIKE '%#BillToCompanyName#%' AND numOppType = 1
--
--UPDATE BizDocTemplate SET txtBizDocTemplate = REPLACE(CAST(txtBizDocTemplate AS NVARCHAR(MAX)),
--'#ShipToCompanyName#','#Customer/VendorShipToCompanyName#')
--WHERE txtBizDocTemplate LIKE '%#ShipToCompanyName#%' AND numOppType = 1
--
--UPDATE BizDocTemplate SET txtBizDocTemplate = REPLACE(CAST(txtBizDocTemplate AS NVARCHAR(MAX)),
--'#OrganizationComments#','#Customer/VendorOrganizationComments#')
--WHERE txtBizDocTemplate LIKE '%#OrganizationComments#%' AND numOppType = 1
--
--
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
--
--UPDATE BizDocTemplate SET txtBizDocTemplate = REPLACE(CAST(txtBizDocTemplate AS NVARCHAR(MAX)),
--'#BillTo#','#EmployerBill-toAddress#')
--WHERE txtBizDocTemplate LIKE '%#BillTo#%' AND numOppType = 2
--
--
--UPDATE BizDocTemplate SET txtBizDocTemplate = REPLACE(CAST(txtBizDocTemplate AS NVARCHAR(MAX)),
--'#ShipTo#','#EmployerShip-toAddress#')
--WHERE txtBizDocTemplate LIKE '%#ShipTo#%' AND numOppType = 2
--
--UPDATE BizDocTemplate SET txtBizDocTemplate = REPLACE(CAST(txtBizDocTemplate AS NVARCHAR(MAX)),
--'#OrganizationName#','#EmployerOrganizationName#')
--WHERE txtBizDocTemplate LIKE '%#OrganizationName#%' AND numOppType = 2
--
--UPDATE BizDocTemplate SET txtBizDocTemplate = REPLACE(CAST(txtBizDocTemplate AS NVARCHAR(MAX)),
--'#OrganizationContactName#','#EmployerOrganizationContactName#')
--WHERE txtBizDocTemplate LIKE '%#OrganizationContactName#%' AND numOppType = 2
--
--UPDATE BizDocTemplate SET txtBizDocTemplate = REPLACE(CAST(txtBizDocTemplate AS NVARCHAR(MAX)),
--'#OrganizationContactEmail#','#EmployerOrganizationContactEmail#')
--WHERE txtBizDocTemplate LIKE '%#OrganizationContactEmail#%' AND numOppType = 2
--
--UPDATE BizDocTemplate SET txtBizDocTemplate = REPLACE(CAST(txtBizDocTemplate AS NVARCHAR(MAX)),
--'#OrganizationContactPhone#','#EmployerOrganizationContactPhone#')
--WHERE txtBizDocTemplate LIKE '%#OrganizationContactPhone#%' AND numOppType = 2
--
--UPDATE BizDocTemplate SET txtBizDocTemplate = REPLACE(CAST(txtBizDocTemplate AS NVARCHAR(MAX)),
--'#ChangeBillToAddress(Bill To)#','#EmployerChangeBillToHeader(Bill To)#')
--WHERE txtBizDocTemplate LIKE '%#ChangeBillToAddress(Bill To)#%' AND numOppType = 2
--
--UPDATE BizDocTemplate SET txtBizDocTemplate = REPLACE(CAST(txtBizDocTemplate AS NVARCHAR(MAX)),
--'#ChangeShipToAddress(Ship To)#','#EmployerChangeShipToHeader(Ship To)#')
--WHERE txtBizDocTemplate LIKE '%#ChangeShipToAddress(Ship To)#%' AND numOppType = 2
--
--UPDATE BizDocTemplate SET txtBizDocTemplate = REPLACE(CAST(txtBizDocTemplate AS NVARCHAR(MAX)),
--'#ChangeBillToAddress#','#EmployerChangeBillToHeader#')
--WHERE txtBizDocTemplate LIKE '%#ChangeBillToAddress#%' AND numOppType = 2
--
--UPDATE BizDocTemplate SET txtBizDocTemplate = REPLACE(CAST(txtBizDocTemplate AS NVARCHAR(MAX)),
--'#ChangeShipToAddress#','#EmployerChangeShipToHeader#')
--WHERE txtBizDocTemplate LIKE '%#ChangeShipToAddress#%' AND numOppType = 2
--
--
--UPDATE BizDocTemplate SET txtBizDocTemplate = REPLACE(CAST(txtBizDocTemplate AS NVARCHAR(MAX)),
--'#BillToAddressName#','#EmployerBillToAddressName#')
--WHERE txtBizDocTemplate LIKE '%#BillToAddressName#%' AND numOppType = 2
--
--
--UPDATE BizDocTemplate SET txtBizDocTemplate = REPLACE(CAST(txtBizDocTemplate AS NVARCHAR(MAX)),
--'#ShipToAddressName#','#EmployerShipToAddressName#')
--WHERE txtBizDocTemplate LIKE '%#ShipToAddressName#%' AND numOppType = 2
--
--UPDATE BizDocTemplate SET txtBizDocTemplate = REPLACE(CAST(txtBizDocTemplate AS NVARCHAR(MAX)),
--'#BillToCompanyName#','#EmployerBillToCompanyName#')
--WHERE txtBizDocTemplate LIKE '%#BillToCompanyName#%' AND numOppType = 2
--
--UPDATE BizDocTemplate SET txtBizDocTemplate = REPLACE(CAST(txtBizDocTemplate AS NVARCHAR(MAX)),
--'#ShipToCompanyName#','#EmployerShipToCompanyName#')
--WHERE txtBizDocTemplate LIKE '%#ShipToCompanyName#%' AND numOppType = 2
--
--UPDATE BizDocTemplate SET txtBizDocTemplate = REPLACE(CAST(txtBizDocTemplate AS NVARCHAR(MAX)),
--'#OrganizationComments#','#EmployerOrganizationComments#')
--WHERE txtBizDocTemplate LIKE '%#OrganizationComments#%' AND numOppType = 2

------------------------------------------------------------------------------------------------------------------------------------------------------

CREATE TABLE [dbo].[WorkflowAutomation](
	[numAutomationID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[numRuleID] [numeric](18, 0) NOT NULL,
	[numBizDocTypeId] [numeric](18, 0) NULL,
	[numBizDocTemplate] [numeric](18, 0) NULL,
	[numBizDocStatus1] [numeric](18, 0) NULL,
	[numBizDocStatus2] [numeric](18, 0) NULL,
	[numOppSource] [numeric](18, 0) NULL,
	[bitUpdatedTrackingNo] [bit] NULL,
	[tintOppType] [numeric](18, 0) NULL,
	[numOrderStatus] [numeric](18, 0) NULL,
	[numDomainID] [numeric](18, 0) NULL,
	[numUserCntID] [numeric](18, 0) NULL,
	[numEmailTemplate] [numeric](18, 0) NULL,
	[numBizDocCreatedFrom] [numeric](18, 0) NULL,
	[numOpenRecievePayment] [bit] NULL,
	[numCreditCardOption] [int] NULL,
	[dtCreated] [datetime] NOT NULL CONSTRAINT [DF_WorkflowAutomation_dtCreated]  DEFAULT (getdate()),
	[dtModified] [datetime] NULL,
	[bitIsActive] [bit] NULL CONSTRAINT [DF_Table_1_bitIsAvtive]  DEFAULT ((1)),
 CONSTRAINT [PK_WorkflowAutomation] PRIMARY KEY CLUSTERED 
(
	[numAutomationID] ASC
)WITH (IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]

INSERT INTO ListDetails(numListID,vcData,numCreatedBY, bintCreatedDate,bitDelete,numDomainId, constFlag, sintOrder) 
values(31,'Paypal',1,GETDATE(),0,1,1,0)

INSERT INTO PaymentGateway(vcGateWayName, vcFirstFldName, vcSecndFldName, vcThirdFldName, vcToolTip)
VALUES ('PayPal','Username','Password','Signature','Used to receive Paypal Payment from Shopping cart')

ALTER TABLE eCommerceDTL ADD
vcPaypalUserName VARCHAR(50) NULL


ALTER TABLE eCommerceDTL ADD
vcPaypalPassword VARCHAR(50) NULL


ALTER TABLE eCommerceDTL ADD
vcPaypalSignature VARCHAR(500) NULL


ALTER TABLE eCommerceDTL ADD
IsPaypalSandbox BIT NULL


INSERT INTO dbo.ErrorMaster (
	vcErrorCode,
	vcErrorDesc,
	tintApplication
) VALUES ( 
	/* vcErrorCode - varchar(6) */ 'ERR073',
	/* vcErrorDesc - nvarchar(max) */ N'Paypal Merchant Configuration not found. Please contact the Merchant',
	/* tintApplication - tinyint */ 3 )  

INSERT INTO dbo.ErrorMaster (
	vcErrorCode,
	vcErrorDesc,
	tintApplication
) VALUES ( 
	/* vcErrorCode - varchar(6) */ 'ERR074',
	/* vcErrorDesc - nvarchar(max) */ N'Invalid Paypal User Email Or Invalid Merchant Paypal Account Configuration. Please contact the Merchant',
	/* tintApplication - tinyint */ 3 )  
INSERT INTO dbo.ErrorMaster (
	vcErrorCode,
	vcErrorDesc,
	tintApplication
) VALUES ( 
	/* vcErrorCode - varchar(6) */ 'ERR075',
	/* vcErrorDesc - nvarchar(max) */ N'Sorry!., There is a problem in redirecting to Paypal..',
	/* tintApplication - tinyint */ 3 )  
INSERT INTO dbo.ErrorMaster (
	vcErrorCode,
	vcErrorDesc,
	tintApplication
) VALUES ( 
	/* vcErrorCode - varchar(6) */ 'ERR076',
	/* vcErrorDesc - nvarchar(max) */ N'Sorry!., Unable to process your Paypal Payment..Paypal Payment Failure.',
	/* tintApplication - tinyint */ 3 )  


------------------------------------------------------------------------------------------------------------------------------------------------------

/***********************************************************************************************
Project: Redirect Configurations  Date: 03/June/2013
Comments: 
************************************************************************************************/

--INSERT TreeNavigationAuthorization ([numGroupID] ,[numTabID] ,[numPageNavID] ,[bitVisible] ,[numDomainID] ,tintType)
--SELECT numGroupID,-1, 221, 1, numDomainID, 1 
--FROM AuthenticationGroupMaster GR
--WHERE NOT EXISTS (SELECT * FROM TreeNavigationAuthorization c
--WHERE GR.numDomainID = c.numDomainID AND GR.numGroupID = c.numGroupID AND c.numTabID = -1 AND c.numPageNavID = 221)



/***********************************************************************************************
Project: Accounts List setting Bill City and Ship City in Settings: 18/June/2013
Comments: 
************************************************************************************************/

--Update DycFormField_Mapping Set bitSettingField  = 1 where numFormId = 36 and vcFieldName IN ('Ship To City','Bill To City')

--SELECT * from PageNavigationDTL WHERE vcPageNavName Like 'Configuration'
--
--Update PageNavigationDTL SET vcNavURL ='../Marketing/frmEmailBroadcastConfigDetail.aspx' where vcPageNavName = 'Configuration'

--
--/***********************************************************************************************
--Project: Redirect Configurations  Date: 24/May/2013
--Comments: 
--************************************************************************************************/
--INSERT INTO [dbo].[PageNavigationDTL]
--           ([numPageNavID]
--           ,[numModuleID]
--           ,[numParentID]
--           ,[vcPageNavName]
--           ,[vcNavURL]
--           ,[vcImageURL]
--           ,[bitVisible]
--           ,[numTabID])
--     VALUES
--           (221,13,157,'Configure Redirects','../ECommerce/frmRedirectConfigurations.aspx',NULL,1,-1)
--
--SET ANSI_NULLS ON
--GO
--SET QUOTED_IDENTIFIER ON
--GO
--SET ANSI_PADDING ON
--GO
--CREATE TABLE [dbo].[RedirectConfig](
--	[numRedirectConfigID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
--	[numDomainId] [numeric](18, 0) NOT NULL,
--	[numUserCntId] [numeric](18, 0) NOT NULL,
--	[numSiteId] [numeric](18, 0) NULL,
--	[vcOldUrl] [varchar](500) NULL,
--	[vcNewUrl] [varchar](500) NULL,
--	[numRedirectType] [numeric](18, 0) NULL,
--	[numReferenceType] [numeric](18, 0) NULL,
--	[numReferenceNo] [numeric](18, 0) NULL,
--	[dtCreated] [datetime] NULL CONSTRAINT [DF_RedirectConfig_dtCreated]  DEFAULT (getdate()),
--	[dtModified] [datetime] NULL,
--	[bitIsActive] [bit] NULL CONSTRAINT [DF_RedirectConfig_bitIsActive]  DEFAULT ((1)),
-- CONSTRAINT [PK_RedirectConfig] PRIMARY KEY CLUSTERED 
--(
--	[numRedirectConfigID] ASC
--)WITH (IGNORE_DUP_KEY = OFF) ON [PRIMARY]
--) ON [PRIMARY]
--
--GO
--SET ANSI_PADDING ON
--
--/***********************************************************************************************
--Project: Google Analytics Work  Date: 11/May/2013
--Comments: 
--************************************************************************************************/
--
--BEGIN TRANSACTION
--GO
--ALTER TABLE Domain ADD
--vcGAUserEMail VARCHAR(50) NULL
--
--ALTER TABLE Domain ADD
--vcGAUserPassword VARCHAR(50) NULL
--
--ALTER TABLE Domain ADD
--vcGAUserProfileID VARCHAR(50) NULL
--
--
--GO
--ROLLBACK TRANSACTION
--
--
--/***********************************************************************************************
--Project: Marketplace Order Id in Simple and Advanced Search  Date: 04/April/2013
--Comments: 
--************************************************************************************************/
--
--BEGIN TRANSACTION
--GO
--INSERT INTO dbo.DycFormField_Mapping (
-- numModuleID,numFieldID,numFormID,vcFieldName,vcAssociatedControlType,
-- bitAllowFiltering,
-- intSectionID
-- 
--) 
--SELECT numModuleID,numFieldID,6,vcFieldName,vcAssociatedControlType,
-- bitAllowFiltering,1 FROM DycFieldMaster WHERE vcDbColumnName='vcMarketplaceOrderID'
--
--
--INSERT INTO dbo.DycFormField_Mapping (
-- numModuleID,numFieldID,numFormID,vcFieldName,vcAssociatedControlType,
-- bitAllowFiltering,
-- intSectionID
-- 
--) 
--SELECT numModuleID,numFieldID,15,vcFieldName,vcAssociatedControlType,
-- bitAllowFiltering,1 FROM DycFieldMaster WHERE vcDbColumnName='vcMarketplaceOrderID'
--GO
--ROLLBACK TRANSACTION
--
--
--/******************************************************************
--Project:GL Performance tunning  Date: 04/April/2013
--Comments: 
--*******************************************************************/
--
--BEGIN TRANSACTION
--GO
--
--ALTER TABLE General_Journal_Header ADD
--numEntryDateSortOrder NUMERIC NULL
--
--UPDATE dbo.General_Journal_Header SET dbo.General_Journal_Header.numEntryDateSortOrder = CONVERT(numeric,
--RIGHT('00000'+ CONVERT(VARCHAR,DATEPART(year, dbo.General_Journal_Header.datEntry_Date)),4)+
--RIGHT('00000'+ CONVERT(VARCHAR,DATEPART(month, dbo.General_Journal_Header.datEntry_Date)),2)+
--RIGHT('00000'+ CONVERT(VARCHAR,DATEPART(day, dbo.General_Journal_Header.datEntry_Date)),2)+
--RIGHT('00000'+ CONVERT(VARCHAR,DATEPART(hour, dbo.General_Journal_Header.datEntry_Date)),2)+
--RIGHT('00000'+ CONVERT(VARCHAR,DATEPART(minute, dbo.General_Journal_Header.datEntry_Date)),2)+
--RIGHT('00000'+ CONVERT(VARCHAR,DATEPART(second, dbo.General_Journal_Header.datEntry_Date)),2)+
--RIGHT('00000'+ CONVERT(VARCHAR,DATEPART(millisecond, dbo.General_Journal_Header.datEntry_Date)),3))
---- where dbo.General_Journal_Header.numDomainId = 163
--GO
--
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
--DECLARE @RowsToProcess int
--DECLARE @CurrentRow int
-- 
--DECLARE @DuplicateVersions TABLE
--(
--    row_id int NOT NULL PRIMARY KEY IDENTITY(1,1),
--    duplicate_count int,
--    numEntryDateSortOrder NUMERIC
--)
-- 
--INSERT INTO @DuplicateVersions 
--            (   
--                duplicate_count,
--                numEntryDateSortOrder
--            )
--         SELECT COUNT(*) AS duplicate_count,
--               GJH.numEntryDateSortOrder
--           FROM General_Journal_Header GJH
--           --where GJH.numDomainId = 163
--       GROUP BY GJH.numEntryDateSortOrder
--         HAVING COUNT(*) > 1
--       ORDER BY duplicate_count DESC
--
--SELECT @RowsToProcess = COUNT(*) FROM @DuplicateVersions
--SET @CurrentRow = 1
-- 
--WHILE @CurrentRow <= @RowsToProcess
--BEGIN   
--
--Select GJH1.numJournal_ID,GJH1.numEntryDateSortOrder,IDENTITY( int,1,1 ) as NewSort INTO #temp123 from General_Journal_Header GJH1
--   INNER JOIN @DuplicateVersions AS DuplicateVersions 
--ON GJH1.numEntryDateSortOrder = DuplicateVersions.numEntryDateSortOrder 
--WHERE DuplicateVersions.row_id = @CurrentRow
--
--UPDATE  GJH2
--SET GJH2.numEntryDateSortOrder = (Temp1.numEntryDateSortOrder + Temp1.NewSort)  from General_Journal_Header GJH2
--   INNER JOIN #temp123 AS Temp1  
--ON GJH2.numJournal_ID = Temp1.numJournal_ID
--
--DROP TABLE #temp123
--
--SET @CurrentRow = @CurrentRow + 1      
--END      
--
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
--
--
--CREATE NONCLUSTERED INDEX [NC_OpportunityBizDocs_numOppId] ON [dbo].[OpportunityBizDocs] 
--(
-- [numOppId] ASC
--)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
--
--
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
--
--ROLLBACK TRANSACTION
--
--
--/******************************************************************
--Project: Allow Filtering for Marketplace Order ID field  Date: 04/April/2013
--Comments: 
--*******************************************************************/
--
--BEGIN TRANSACTION
--GO
--
--update DycFieldMaster set bitAllowFiltering = 1,vcAssociatedControlType = 'TextBox' where vcFieldName = 'Marketplace Order ID'
--
--update DycFormField_Mapping set bitAllowFiltering = 1,vcAssociatedControlType = 'TextBox' where vcFieldName = 'Marketplace Order ID'
--
--GO
--ROLLBACK TRANSACTION
--
--
--
--/******************************************************************
--Project: Ship To Phone Number Mapping			  Date: 12/March/2013
--Comments: Ship To Phone Number Mapping if Missing in Api Orders
--*******************************************************************/
--
--BEGIN TRANSACTION
--GO
--ALTER TABLE ImportApiOrder ADD
--numOppId NUMERIC NULL
--GO
--ROLLBACK TRANSACTION
--
--
--
--BEGIN TRANSACTION
--GO
--ALTER TABLE Domain ADD
--vcShipToPhoneNo VARCHAR(50) NULL
--GO
--ROLLBACK TRANSACTION
--
--
--
--/******************************************************************
--Project: Update vcMarketplaceOrderId with vcOppRefOrderNo Date: 08/03/2013
--Comments:  
--*******************************************************************/
--BEGIN TRANSACTION
--GO
--
--ALTER TABLE WebApiOrderDetails DROP CONSTRAINT IX_WebApiOrderDetails
--GO
--ROLLBACK TRANSACTION
--
--
--BEGIN TRANSACTION
--GO
--
--ALTER TABLE OpportunityMaster
--ALTER COLUMN vcMarketplaceOrderId VARCHAR(100)
--
--update OpportunityMaster set vcMarketplaceOrderId = vcOppRefOrderNo
--where vcOppRefOrderNo IS NOT NULL
--
--ALTER TABLE WebApiOrderReports DROP CONSTRAINT IX_WebApiOrderReports
--
--GO
--ROLLBACK TRANSACTION
--
--/******************************************************************
--Project: Adding Marketplace Order Report Id field: 07/03/2013
--Comments:  
--*******************************************************************/
--BEGIN TRANSACTION
--GO
--
--ALTER TABLE dbo.OpportunityMaster ADD
--	vcMarketplaceOrderReportId VARCHAR(100) NULL
--GO
--ROLLBACK TRANSACTION
--
--/******************************************************************
--Project: ReImport API Orders : 06/03/2013
--Comments:  
--*******************************************************************/
--
--/****** Object:  Table [dbo].[ImportApiOrder]    Script Date: 06/03/2013 10:09:01 ******/
--SET ANSI_NULLS ON
--GO
--SET QUOTED_IDENTIFIER ON
--GO
--SET ANSI_PADDING ON
--GO
--CREATE TABLE [dbo].[ImportApiOrder](
--	[numImportApiOrderReqId] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
--	[numDomainId] [numeric](18, 0) NOT NULL,
--	[numWebApiId] [numeric](18, 0) NOT NULL,
--	[vcApiOrderId] [varchar](50) NOT NULL,
--	[dtCreatedDate] [datetime] NULL CONSTRAINT [DF_ImportApiOrder_dtCreatedDate]  DEFAULT (getdate()),
--	[dtModifiedDate] [datetime] NULL,
--	[bitIsActive] [bit] NOT NULL CONSTRAINT [DF_ImportApiOrder_bitIsActive]  DEFAULT ((1)),
--	[tintRequestType] [tinyint] NULL,
--	[dtFromDate] [datetime] NULL,
--	[dtToDate] [datetime] NULL,
-- CONSTRAINT [PK_ImportApiOrder] PRIMARY KEY CLUSTERED 
--(
--	[numImportApiOrderReqId] ASC
--)WITH (IGNORE_DUP_KEY = OFF) ON [PRIMARY]
--) ON [PRIMARY]
--
--GO
--SET ANSI_PADDING ON
--
--/******************************************************************
--Project: Adding FBA specific BizDoc and BizDoc Status : 06/03/2013
--Comments:  
--*******************************************************************/
--
--ALTER TABLE dbo.WebApiDetail ADD
--numFBABizDocId NUMERIC(18,0) NULL
--
--
--ALTER TABLE dbo.WebApiDetail ADD
--numFBABizDocStatusId NUMERIC(18,0) NULL
--
--/******************************************************************
--Project: Adding Marketplace Order Id field: 26/02/2013
--Comments:  
--*******************************************************************/
--BEGIN TRANSACTION
--GO
--
--ALTER TABLE dbo.OpportunityMaster ADD
--	vcMarketplaceOrderID VARCHAR(50) NULL
--GO
--ROLLBACK TRANSACTION
--
--BEGIN TRANSACTION
--GO
--
--INSERT INTO [dbo].[DycFieldMaster]([numModuleID], [numDomainID], [vcFieldName], [vcDbColumnName], [vcOrigDbColumnName], [vcPropertyName], [vcLookBackTableName], [vcFieldDataType], [vcFieldType], [vcAssociatedControlType], 
--[vcToolTip], [vcListItemType], [numListID], [PopupFunctionName], [order], [tintRow], [tintColumn], 
--[bitInResults], [bitDeleted], [bitAllowEdit], [bitDefault], [bitSettingField], [bitAddField], 
--[bitDetailField], [bitAllowSorting],bitWorkFlowField,bitAllowFiltering )
--SELECT 3, NULL, N'Marketplace Order ID', N'vcMarketplaceOrderID', N'vcMarketplaceOrderID', 'MarketplaceOrderID', N'OpportunityMaster', N'V', N'R',
-- N'Label', NULL, N'', 0, N'', 1, 1, 2, 1, 0, 1, 0,1, 0, 1, 1,1,0
--		
--insert into DycFormField_Mapping (numModuleID,numFieldID,numFormID,vcFieldName,vcAssociatedControlType,PopupFunctionName,bitSettingField,bitAddField,bitDetailField,bitAllowEdit,bitInlineEdit,vcPropertyName,[order],tintRow,tintColumn)
--select numModuleID,numFieldID,39,'Marketplace Order ID',vcAssociatedControlType,PopupFunctionName,bitSettingField,0,bitDetailField,0,0,vcPropertyName,[order],tintRow,tintColumn
--from DycFieldMaster where [numModuleID]=3 and  [vcDbColumnName]='vcMarketplaceOrderID' AND vcLookBackTableName='OpportunityMaster'
--	
--GO
--ROLLBACK TRANSACTION
--
--/******************************************************************
--Project: Sales Tax Service Item Mapping Date: 19/02/2013
--Comments:  
--*******************************************************************/
--BEGIN TRANSACTION
--GO
--
--ALTER TABLE dbo.WebAPIDetail ADD
--	numSalesTaxItemMapping NUMERIC(18,0) NULL
--GO
--ROLLBACK TRANSACTION
--
--
--/******************************************************************
--Project: BizDoc status to create BizDoc  Date: 30/01/2013
--Comments:  
--*******************************************************************/
--
--BEGIN TRANSACTION
--GO
--
--ALTER TABLE dbo.WebAPIDetail ADD
--	bitEnableItemUpdate BIT NULL
--GO
--
--ALTER TABLE dbo.WebAPIDetail ADD
--	bitEnableInventoryUpdate BIT NULL
--GO
--
--ALTER TABLE dbo.WebAPIDetail ADD
--	bitEnableOrderImport BIT NULL
--GO
--
--ALTER TABLE dbo.WebAPIDetail ADD
--	bitEnableTrackingUpdate BIT NULL
--GO
--
--ALTER TABLE dbo.WebAPIDetail ADD
--	numBizDocStatusId varchar(100) NULL
--GO
--
--ROLLBACK TRANSACTION
--
--/******************************************************************
--Project: FBA Order Import Last Update Flag   Date: 26/01/2013
--Comments:  
--*******************************************************************/
--
--BEGIN TRANSACTION
--GO
--
--ALTER TABLE dbo.WebAPI ADD
--	vcSixteenthFldName VARCHAR(50) NULL
--
--ALTER TABLE dbo.WebAPIDetail ADD
--	vcSixteenthFldValue varchar(100) NULL
--GO
--
--ROLLBACK TRANSACTION
--
--/******************************************************************
--Project: Online Marketplace Shipped Order Status Column Added   Date: 26/01/2013
--Comments:  
--*******************************************************************/
--
--BEGIN TRANSACTION
--GO
--
--ALTER TABLE dbo.WebApiDetail
--    DROP COLUMN numFBAOrderStatus
--
--ALTER TABLE dbo.WebApiDetail ADD
--numUnShipmentOrderStatus NUMERIC(18,0) NULL
--
--GO
--
--ROLLBACK TRANSACTION
--
--/******************************************************************
--Project: Online Marketplace Order Processing    Date: 21/01/2013
--Comments: 
--*******************************************************************/
--
--BEGIN TRANSACTION
--GO
--
--ALTER TABLE dbo.WebApiDetail ADD
--numFBAOrderStatus NUMERIC(18,0) NULL
--GO
--
--ROLLBACK TRANSACTION
--
--/******************************************************************
--Project: Online Marketplace Order Processing    Date: 21/01/2013
--Comments: It used to depend on BizDoc items mapping for updating marketplace orders. 
--now made it depend on Order Items to update tracking number.
--*******************************************************************/
--BEGIN TRANSACTION
--GO
--
--sp_Rename 'dbo.WebApiOppItemDetails.numOppBizDocID', 'numOppItemID'
--
--ALTER TABLE dbo.WebApiOppItemDetails
--    DROP COLUMN numOppBizDocItemID
--GO
--ROLLBACK TRANSACTION
--
--
--/******************************************************************
--Project: E-Banking Integration for Bank Of America    Date: 18/01/2013
--Comments: 
--*******************************************************************/
--BEGIN TRANSACTION
--GO
--INSERT INTO [dbo].[BankMaster]
--           ([tintBankType]
--           ,[vcFIId]
--           ,[vcFIName]
--           ,[vcFIOrganization]
--           ,[vcFIOFXServerUrl]
--           ,[vcBankID]
--           ,[vcOFXAccessKey])
--     VALUES
--           (2,'6812','Bank of America','HAN','https://ofx.bankofamerica.com/cgi-forte/fortecgi?servicename=ofx_2-3&pagename=ofx','','')
--GO
--ROLLBACK TRANSACTION
--
--
--/******************************************************************
--Project: ShippingServiceItemMapping   Date: 07/01/2013
--Comments: 
--*******************************************************************/
--BEGIN TRANSACTION
--GO
--ALTER TABLE Domain ADD
--numShippingServiceItemID NUMERIC(18,0) NULL
--GO
--ROLLBACK TRANSACTION
