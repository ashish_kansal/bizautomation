/******************************************************************
Project: Release 9.9 Date: 02.JUL.2018
Comments: ALTER SCRIPTS
*******************************************************************/

/*************************** SANDEEP *******************************/

DELETE FROM DycFormField_Mapping WHERE numFormID=23 AND numFieldID = 248
DELETE FROM DycFormConfigurationDetails WHERE numFormId=23 AND numFieldID = 248

DELETE FROM DycFormField_Mapping WHERE numFormID=23 AND vcFieldName='Inventory Status'
DELETE FROM DycFormConfigurationDetails WHERE numFormId=23 AND numFieldID IN (SELECT numFieldId FROM DycFormField_Mapping WHERE numFormID=23 AND vcFieldName='Inventory Status')

UPDATE SalesFulfillmentConfiguration SET bitRule1IsActive=0
UPDATE SalesFulfillmentConfiguration SET bitRule4IsActive=0

ALTER TABLE Domain ADD tintCommitAllocation TINYINT DEFAULT 1
UPDATE Domain SET tintCommitAllocation=1
ALTER TABLE DivisionMaster ADD bitAllocateInventoryOnPickList BIT DEFAULT 0
UPDATE DivisionMaster SET bitAllocateInventoryOnPickList=0
ALTER TABLE Domain ADD tintInvoicing TINYINT DEFAULT 1
UPDATE Domain SET tintInvoicing=1

/*************************** PRIYA *******************************/

ALTER TABLE ITEM 
ADD vcASIN VARCHAR(50) NULL

ALTER TABLE DivisionMaster
ADD tintInbound850PickItem INT NULL

---------------------------------------------------------------------

SET IDENTITY_INSERT [dbo].[CFW_Loc_Master] ON

INSERT INTO CFW_Loc_Master
(
	Loc_id,Loc_name,vcFieldType,vcCustomLookBackTableName
)
VALUES
(
	19,'EDI Fields for Orders','O','CFW_Fld_Values_Opp_EDI'
),
(
	20,'EDI Fields for Items','P','CFW_FLD_Values_Item_EDI'
),
(
	21,'EDI Fields for OppItems','O','CFW_Fld_Values_OppItems_EDI'
)


SET IDENTITY_INSERT [dbo].[CFW_Loc_Master] OFF
GO

----------------------------------------------------------------------------------

DELETE FROM ListDetails WHERE numListItemID = 15446 AND numListID = 176 AND vcData = 'Shipment Request (940) Failed'

DELETE FROM ListDetails WHERE numListItemID = 15448 AND numListID = 176 AND vcData = 'Send 856 Failed'

UPDATE ListDetails
SET vcData = 'Send 856 & Invoice (810)' WHERE numListItemID = 15447 AND numListID = 176 AND vcData = 'Send 856'

-----------------------------------------------------------------------------

GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[CFW_Fld_Values_OppItems_EDI](
	[FldDTLID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[Fld_ID] [numeric](18, 0) NULL,
	[Fld_Value] [varchar](max) NULL,
	[RecId] [numeric](18, 0) NULL,
 CONSTRAINT [PK_CFW_Fld_Values_OppItems_EDI] PRIMARY KEY CLUSTERED 
(
	[FldDTLID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

SET ANSI_PADDING ON
GO

ALTER TABLE [dbo].[CFW_Fld_Values_OppItems_EDI]  WITH CHECK ADD  CONSTRAINT [FK_CFW_Fld_Values_OppItems_EDI_CFW_Fld_Master] FOREIGN KEY([Fld_ID])
REFERENCES [dbo].[CFW_Fld_Master] ([Fld_id])
GO

ALTER TABLE [dbo].[CFW_Fld_Values_OppItems_EDI] CHECK CONSTRAINT [FK_CFW_Fld_Values_OppItems_EDI_CFW_Fld_Master]
GO

-------------------------------------------------------------------


BEGIN TRY
BEGIN TRANSACTION

	DECLARE @numFieldID NUMERIC(18,0)

	INSERT INTO DycFieldMaster
		(numModuleID,vcFieldName,vcDbColumnName,vcOrigDbColumnName,vcPropertyName,vcLookBackTableName,vcFieldDataType,vcFieldType,vcAssociatedControlType,[order],tintRow,tintColumn,
		bitInResults,bitDeleted,bitAllowEdit,bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering,numListID)
	VALUES
		(3,'EDI Fields','EDIFields','EDIFields','EDIFields','EDIFields','V','R','HyperLink',46,1,1,1,1,0,0,1,1,1,1,1,0)

	SELECT @numFieldID = SCOPE_IDENTITY()


	INSERT INTO DycFormField_Mapping
		(numModuleID,numFieldID,numFormID,bitAllowEdit,bitInlineEdit,vcFieldName,vcAssociatedControlType,vcPropertyName,[order],tintRow,tintColumn,bitInResults,bitDeleted,
		bitDefault,bitSettingField,bitAddField,bitDetailField,bitAllowSorting,bitAllowFiltering)
	VALUES
		(3,@numFieldID,26,1,1,'EDI Fields','EDIFields','EDIFields',7,1,7,1,0,0,1,1,1,0,1)

COMMIT
END TRY
BEGIN CATCH
	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;

	SELECT 
		ERROR_MESSAGE(),
		ERROR_NUMBER(),
		ERROR_SEVERITY(),
		ERROR_STATE(),
		ERROR_LINE(),
		ISNULL(ERROR_PROCEDURE(), '-');
END CATCH

------------------------------------------------------
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[CFW_Fld_Values_Opp_EDI](
	[FldDTLID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[Fld_ID] [numeric](18, 0) NOT NULL,
	[Fld_Value] [varchar](max) NULL,
	[RecId] [numeric](18, 0) NOT NULL,
	[numModifiedBy] [numeric](18, 0) NULL,
	[bintModifiedDate] [datetime] NULL,
 CONSTRAINT [PK_CFW_Fld_Values_Opp_EDI] PRIMARY KEY CLUSTERED 
(
	[FldDTLID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

SET ANSI_PADDING ON
GO

ALTER TABLE [dbo].[CFW_Fld_Values_Opp_EDI]  WITH CHECK ADD  CONSTRAINT [FK_CFW_Fld_Values_Opp_EDI_CFW_Fld_Master] FOREIGN KEY([Fld_ID])
REFERENCES [dbo].[CFW_Fld_Master] ([Fld_id])
GO

ALTER TABLE [dbo].[CFW_Fld_Values_Opp_EDI] CHECK CONSTRAINT [FK_CFW_Fld_Values_Opp_EDI_CFW_Fld_Master]
GO

---------------------------------------------------------------------
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[CFW_FLD_Values_Item_EDI](
	[FldDTLID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[Fld_ID] [numeric](18, 0) NULL,
	[Fld_Value] [varchar](max) NULL,
	[RecId] [numeric](18, 0) NULL,
 CONSTRAINT [PK_CFW_FLD_Values_Item_EDI] PRIMARY KEY CLUSTERED 
(
	[FldDTLID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

SET ANSI_PADDING ON
GO

ALTER TABLE [dbo].[CFW_FLD_Values_Item_EDI]  WITH CHECK ADD  CONSTRAINT [FK_CFW_FLD_Values_Item_EDI_CFW_Fld_Master] FOREIGN KEY([Fld_ID])
REFERENCES [dbo].[CFW_Fld_Master] ([Fld_id])
GO

ALTER TABLE [dbo].[CFW_FLD_Values_Item_EDI] CHECK CONSTRAINT [FK_CFW_FLD_Values_Item_EDI_CFW_Fld_Master]
GO

-------------------------------------------------------
GO
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TrueCommerceLog](
	[numTCQueueID] [int] IDENTITY(1,1) NOT NULL,
	[FileName] [varchar](50) NULL,
	[PurchaseOrder] [varchar](300) NULL,
	[vcMessage] [varchar](max) NULL,
	[dtDate] [datetime] NULL,
	[numDomainID] [numeric](18, 0) NULL,
 CONSTRAINT [PK_TrueCommerceLog] PRIMARY KEY CLUSTERED 
(
	[numTCQueueID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

SET ANSI_PADDING ON
GO



--------------------------------------------------

ALTER TABLE DomainSFTPDetail ADD vcImportPath VARCHAR(MAX)
ALTER TABLE DomainSFTPDetail ADD vcExportPath VARCHAR(MAX)

--TODO: change domianid to allure domain id
INSERT INTO DomainSFTPDetail
(
	numDomainID,vcUsername,vcPassword,tintType,vcImportPath,vcExportPath
)
VALUES
(
	72,'124085054465','HUGGLE37146',2,'/124085054465/IntegrationFTP/Import/Huggle Hounds/Transaction/','/124085054465/IntegrationFTP/Export/Huggle Hounds/Transaction/'
)