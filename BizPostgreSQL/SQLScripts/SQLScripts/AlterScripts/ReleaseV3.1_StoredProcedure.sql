/****** Object:  UserDefinedFunction [dbo].[fn_GetKitInventoryByWarehouse]    Script Date: 05/30/2014  ******/
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'fn'
                    AND NAME = 'fn_GetKitInventoryByWarehouse' ) 
    DROP FUNCTION fn_GetKitInventoryByWarehouse
GO
CREATE FUNCTION [dbo].[fn_GetKitInventoryByWarehouse]
    (
      @numKitId NUMERIC,
      @numWarehouseID NUMERIC(18, 0)
    )
RETURNS NUMERIC(9)
AS BEGIN          
    DECLARE @OnHand AS NUMERIC(9)       
    SET @OnHand = 0 ;
    WITH    CTE ( numItemKitID, numItemCode, numWarehouseItmsID, numQtyItemsReq, numCalculatedQty )
              AS ( SELECT   CONVERT(NUMERIC(18, 0), 0),
                            numItemCode,
                            ISNULL(Dtl.numWareHouseItemId, 0) AS numWarehouseItmsID,
                            DTL.numQtyItemsReq,
                            CAST(DTL.numQtyItemsReq AS NUMERIC(9, 0)) AS numCalculatedQty
                   FROM     item
                            INNER JOIN ItemDetails Dtl ON numChildItemID = numItemCode
                   WHERE    numItemKitID = @numKitId
                   UNION ALL
                   SELECT   Dtl.numItemKitID,
                            i.numItemCode,
                            ISNULL(Dtl.numWareHouseItemId, 0) AS numWarehouseItmsID,
                            DTL.numQtyItemsReq,
                            CAST(( DTL.numQtyItemsReq * c.numCalculatedQty ) AS NUMERIC(9, 0)) AS numCalculatedQty
                   FROM     item i
                            INNER JOIN ItemDetails Dtl ON Dtl.numChildItemID = i.numItemCode
                            INNER JOIN CTE c ON Dtl.numItemKitID = c.numItemCode
                   WHERE    Dtl.numChildItemID != @numKitId
                 )
        SELECT  @OnHand = CAST(FLOOR(MIN(CASE WHEN ISNULL(numOnHand, 0) = 0
                                              THEN 0
                                              WHEN ISNULL(numOnHand, 0) >= numQtyItemsReq
                                                   AND numQtyItemsReq > 0
                                              THEN ISNULL(numOnHand, 0)
                                                   / numQtyItemsReq
                                              ELSE 0
                                         END)) AS NUMERIC(9))
        FROM    cte c
                LEFT OUTER JOIN [WareHouseItems] WI ON WI.[numItemID] = c.numItemCode
                                                       AND WI.[numWareHouseItemID] = c.numWarehouseItmsID
                LEFT OUTER JOIN Warehouses W ON W.numWareHouseID = WI.numWareHouseID
        WHERE   WI.numWareHouseID = @numWarehouseID
        
    RETURN @OnHand         
          
   END
GO
/****** Object:  StoredProcedure [dbo].[USP_AdvancedSearchOpp]    Script Date: 07/26/2008 16:14:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_advancedsearchopp')
DROP PROCEDURE usp_advancedsearchopp
GO
Create PROCEDURE [dbo].[USP_AdvancedSearchOpp]
@WhereCondition as varchar(4000)='',
@ViewID as tinyint,
@numDomainID as numeric(9)=0,
@numUserCntID as numeric(9)=0,
@numGroupID as numeric(9)=0,
@CurrentPage int,
@PageSize int,                                                                  
@TotRecs int output,                                                                                                           
@columnSortOrder as Varchar(10),    
@ColumnName as varchar(50)='',    
@SortCharacter as char(1),                
@SortColumnName as varchar(50)='',    
@LookTable as varchar(10)='',
@strMassUpdate as varchar(2000)=''
  
as   
  
	  
declare @tintOrder as tinyint                                  
declare @vcFormFieldName as varchar(50)                                  
declare @vcListItemType as varchar(3)                             
declare @vcListItemType1 as varchar(3)                                 
declare @vcAssociatedControlType varchar(10)                                  
declare @numListID AS numeric(9)                                  
declare @vcDbColumnName varchar(30)                                   
declare @ColumnSearch as varchar(10)
DECLARE @vcLookBackTableName VARCHAR(50)
DECLARE @bitIsSearchBizDoc BIT



IF CHARINDEX('OpportunityBizDocs', @WhereCondition) > 0
begin

	SET @bitIsSearchBizDoc = 1
	end
ELSE 
 BEGIN
	SET @bitIsSearchBizDoc = 0
	
 END

if (@SortCharacter<>'0' and @SortCharacter<>'') set @ColumnSearch=@SortCharacter   
Create table #tempTable ( ID INT IDENTITY PRIMARY KEY,                                                                         
      numOppID varchar(15),
      numOppBizDocID varchar(15)
 )  
  
declare @strSql as varchar(8000)
 set  @strSql='select OppMas.numOppId '
 
IF @bitIsSearchBizDoc = 1 --Added to eliminate duplicate records 
	SET @strSql = @strSql + ' ,OpportunityBizDocs.numOppBizDocsId '
ELSE 
	SET @strSql = @strSql + ' ,0 numOppBizDocsId '

IF @SortColumnName ='CalAmount'
BEGIN
	ALTER TABLE #tempTable ADD CalAmount money ;
	SET @strSql = @strSql + ' ,[dbo].[getdealamount](OppMas.numOppId,Getutcdate(),0) as CalAmount'
END	

IF @SortColumnName ='monDealAmount'
BEGIN
	ALTER TABLE #tempTable ADD monDealAmount money ;

	SET @strSql = @strSql + ' ,ISNULL((SELECT SUM(ISNULL(BD.monDealAmount,0)) FROM   OpportunityBizDocs BD WHERE  BD.numOppId = OppMas.numOppId AND ISNULL(bitAuthoritativeBizDocs,0)=1),0) as monDealAmount'
END	

IF @SortColumnName ='monAmountPaid'
BEGIN
	ALTER TABLE #tempTable ADD monAmountPaid money ;

	SET @strSql = @strSql + ' ,ISNULL((SELECT SUM(ISNULL(BD.monAmountPaid,0)) FROM   OpportunityBizDocs BD WHERE  BD.numOppId = OppMas.numOppId AND ISNULL(bitAuthoritativeBizDocs,0)=1),0) AS monAmountPaid'
END	

SET @strSql = @strSql + ' from   
OpportunityMaster OppMas   
Join AdditionalContactsinformation ADC on OppMas.numContactId = ADC.numContactId  
join DivisionMaster DM on Dm.numDivisionId = OppMas.numDivisionId  
JOIN CompanyInfo C ON DM.numCompanyID = C.numCompanyId   
'  
IF @bitIsSearchBizDoc = 1 --Added to eliminate duplicate records 
BEGIN	
	IF CHARINDEX('vcNotes', @WhereCondition) > 0
		BEGIN
	  		SET @strSql = @strSql + ' left join OpportunityBizDocs on OppMas.numOppId = OpportunityBizDocs.numOppId left join OpportunityBizDocItems on OpportunityBizDocItems.numOppBizDocID=OpportunityBizDocs.numOppBizDocsId '
		END
	ELSE
		BEGIN
			SET @strSql = @strSql + ' left join OpportunityBizDocs on OppMas.numOppId = OpportunityBizDocs.numOppId '
		END


END
	
IF @SortColumnName ='numBillCountry' OR @SortColumnName ='numBillState'
     OR @ColumnName ='numBillCountry' OR @ColumnName ='numBillState'
BEGIN 	
	set @strSql= @strSql +' left Join AddressDetails AD1 on AD1.numRecordId= DM.numDivisionID and AD1.tintAddressOf=2 and AD1.tintAddressType=1 AND AD1.bitIsPrimary=1 and AD1.numDomainID= ADC.numDomainID '
END	
ELSE IF @SortColumnName ='numShipCountry' OR @SortColumnName ='numShipState'
     OR @ColumnName ='numShipCountry' OR @ColumnName ='numShipState'
BEGIN
	set @strSql= @strSql +' left Join AddressDetails AD2 on AD2.numRecordId= DM.numDivisionID and AD2.tintAddressOf=2 and AD2.tintAddressType=2 AND AD2.bitIsPrimary=1 and AD2.numDomainID= ADC.numDomainID '
END

   if (@SortColumnName<>'')                        
  begin                              
 select top 1 @vcListItemType1=vcListItemType from View_DynamicDefaultColumns where vcDbColumnName=@SortColumnName and numFormID=15 and numDomainId=@numDomainId
  if @vcListItemType1='LI'                       
  begin
			IF @SortColumnName ='numBillCountry'
			BEGIN
					set @strSql= @strSql +' left Join ListDetails L2 on L2.numListItemID=AD1.numCountry' 
			END
			ELSE IF @SortColumnName ='numShipCountry'
			BEGIN
					set @strSql= @strSql +' left Join ListDetails L2 on L2.numListItemID=AD2.numCountry' 
			END
		  ELSE
		  BEGIN                                    
				set @strSql= @strSql +' left Join ListDetails L2 on L2.numListItemID='+@SortColumnName                                  
		  END
  end  
 else if @vcListItemType1='S'                               
    begin           
		IF @SortColumnName ='numBillState'
		BEGIN
   			set @strSql= @strSql +' left join State S2 on S2.numStateID=AD1.numState'                            
		END
	    ELSE IF @SortColumnName ='numShipState'
	    BEGIN
   			set @strSql= @strSql +' left join State S2 on S2.numStateID=AD2.numState'                           
		END
    end                          
  end                              
  set @strSql=@strSql+' where OppMas.numDomainID  = '+convert(varchar(15),@numDomainID)+   @WhereCondition                                      
     
  
if (@ColumnName<>'' and  @ColumnSearch<>'')                             
begin                              
  if @vcListItemType='LI'                                   
    begin    
			IF @ColumnName ='numBillCountry'
			BEGIN
					set @strSql= @strSql +' left Join ListDetails L1 on L1.numListItemID=AD1.numCountry' 
			END
			ELSE IF @ColumnName ='numShipCountry'
			BEGIN
					set @strSql= @strSql +' left Join ListDetails L1 on L1.numListItemID=AD2.numCountry' 
			END
		  ELSE
		  BEGIN                                
				set @strSql= @strSql +' and L1.vcData like '''+@ColumnSearch +'%'''                                 
			END
		end                                  
 else if @vcListItemType='S'                               
    begin           
		IF @ColumnName ='numBillState'
		BEGIN
   			set @strSql= @strSql +' left join State S1 on S1.numStateID=AD1.numState'                            
		END
	    ELSE IF @ColumnName ='numShipState'
	    BEGIN
   			set @strSql= @strSql +' left join State S1 on S1.numStateID=AD2.numState'                           
		END
    end
    else set @strSql= @strSql +' and '+ 
				case when @ColumnName = 'numAssignedTo' then 'OppMas.'+@ColumnName 
				else @ColumnName end 
				+' like '''+@ColumnSearch  +'%'''                                
                              
end                              
   if (@SortColumnName<>'')                            
  begin     
                          
    if @vcListItemType1='LI'                                   
    begin        
      set @strSql= @strSql +' order by L2.vcData '+@columnSortOrder                                  
    end   
	else if @vcListItemType1='S'                               
		begin                                  
		  set @strSql= @strSql +' order by S2.vcState '+@columnSortOrder                              
		end                                
    ELSE
	BEGIN
		--SET @strSql  = REPLACE(@strSql,'distinct','')
		set @strSql= @strSql +' order by '+ @SortColumnName +' ' +@columnSortOrder
	END  
  end   
  
  
insert into #tempTable exec(@strSql)    
 print @strSql 
 print '===================================================='  
         
         --SELECT * FROM #tempTable               
set @strSql=''                                  
                                  
DECLARE @bitBillAddressJoinAdded AS BIT;
DECLARE @bitShipAddressJoinAdded AS BIT;
SET @bitBillAddressJoinAdded=0;
SET @bitShipAddressJoinAdded=0;
                      
set @tintOrder=0                                  
set @WhereCondition =''                               
set @strSql='select OppMas.numOppId '
IF @bitIsSearchBizDoc = 1 --Added to eliminate duplicate records 
BEGIN
	SET @strSql = @strSql + ' ,OpportunityBizDocs.numOppBizDocsId '
END

select top 1 @tintOrder=A.tintOrder+1,@vcDbColumnName=vcDbColumnName,@vcFormFieldName=vcFieldName,@vcAssociatedControlType=vcAssociatedControlType,@vcListItemType=vcListItemType,@numListID=numListID,@vcLookBackTableName=vcLookBackTableName
from AdvSerViewConf A                                  
join View_DynamicDefaultColumns D                                   
on D.numFieldId=A.numFormFieldId   and d.numformId = 15                               
where D.numDomainID = @numDomainID AND D.numFormID = 15 AND A.numFormID = 15  AND tintViewID=@ViewID and numGroupID=@numGroupID and A.numDomainID=@numDomainID 
order by A.tintOrder asc                                  

while @tintOrder>0                                  
begin                                  

--PRINT @vcDbColumnName
--print @vcAssociatedControlType

 if @vcAssociatedControlType='SelectBox'                                  
        begin                                  
                                                                              
    IF @vcDbColumnName = 'tintSource'
            BEGIN
              SET @strSql = @strSql
                                  + ',dbo.fn_GetOpportunitySourceValue(ISNULL(OppMas.tintSource,0),ISNULL(OppMas.tintSourceType,0),OppMas.numDomainID) '
                                  +' ['+ @vcFormFieldName+'~'+ @vcDbColumnName+']'
				
            END                                            
		  ELSE if @vcListItemType='LI'                                   
		  begin   
IF @numListID=40--Country
		  BEGIN
	  			set @strSql=@strSql+',L'+ convert(varchar(3),@tintOrder)+'.vcData'+' ['+ @vcFormFieldName+'~'+ @vcDbColumnName+']'                              
			IF @vcDbColumnName ='numBillCountry'
			BEGIN
				IF @bitBillAddressJoinAdded=0 
				BEGIN
					set @WhereCondition= @WhereCondition +' left Join AddressDetails AD1 on AD1.numRecordId= DM.numDivisionID and AD1.tintAddressOf=2 and AD1.tintAddressType=1 AND AD1.bitIsPrimary=1 and AD1.numDomainID= ADC.numDomainID '
					
					SET @bitBillAddressJoinAdded=1;
				END
				set @WhereCondition= @WhereCondition +' left Join ListDetails L'+ convert(varchar(3),@tintOrder)+ ' on L'+convert(varchar(3),@tintOrder)+ '.numListItemID=AD1.numCountry'
			END
			ELSE IF @vcDbColumnName ='numShipCountry'
			BEGIN
				IF @bitShipAddressJoinAdded=0 
				BEGIN
					set @WhereCondition= @WhereCondition +' left Join AddressDetails AD2 on AD2.numRecordId= DM.numDivisionID and AD2.tintAddressOf=2 and AD2.tintAddressType=2 AND AD2.bitIsPrimary=1 and AD2.numDomainID= ADC.numDomainID '
					
					SET @bitShipAddressJoinAdded=1;
				END
				set @WhereCondition= @WhereCondition +' left Join ListDetails L'+ convert(varchar(3),@tintOrder)+ ' on L'+convert(varchar(3),@tintOrder)+ '.numListItemID=AD2.numCountry'
			END
		  END                        
		  ELSE
		  BEGIN                               
			set @strSql=@strSql+',L'+ convert(varchar(3),@tintOrder)+'.vcData'+' ['+ @vcFormFieldName+'~'+ @vcDbColumnName+']'
				
			if @vcDbColumnName = 'numSalesOrPurType' AND @numListID=46
				set @WhereCondition= @WhereCondition +' left Join ListDetails L'+ convert(varchar(3),@tintOrder)+ ' on L'+convert(varchar(3),@tintOrder)+ '.numListItemID=OppMas.numSalesOrPurType and OppMas.tintOppType = 2 '                                 		
			else if @vcDbColumnName = 'numSalesOrPurType' AND @numListID=45
				set @WhereCondition= @WhereCondition +' left Join ListDetails L'+ convert(varchar(3),@tintOrder)+ ' on L'+convert(varchar(3),@tintOrder)+ '.numListItemID=OppMas.numSalesOrPurType and OppMas.tintOppType = 1 '                                 
			else
				set @WhereCondition= @WhereCondition +' left Join ListDetails L'+ convert(varchar(3),@tintOrder)+ ' on L'+convert(varchar(3),@tintOrder)+ '.numListItemID='+@vcDbColumnName                                  
		  end                                  
            END                      
		end 
ELSE IF @vcAssociatedControlType='ListBox'   
BEGIN
	if @vcListItemType='S'                               
	  begin                         
		set @strSql=@strSql+',S'+ convert(varchar(3),@tintOrder)+'.vcState'+' ['+ @vcFormFieldName+'~'+ @vcDbColumnName+']'
		IF @vcDbColumnName ='numBillState'
		BEGIN
			IF @bitBillAddressJoinAdded=0 
			BEGIN
				set @WhereCondition= @WhereCondition +' left Join AddressDetails AD1 on AD1.numRecordId= DM.numDivisionID and AD1.tintAddressOf=2 and AD1.tintAddressType=1 AND AD1.bitIsPrimary=1 and AD1.numDomainID= ADC.numDomainID '
				SET @bitBillAddressJoinAdded=1;
			END
			set @WhereCondition= @WhereCondition +' left join State S'+ convert(varchar(3),@tintOrder)+ ' on S'+convert(varchar(3),@tintOrder)+ '.numStateID=AD1.numState'
		END
	    ELSE IF @vcDbColumnName ='numShipState'
	    BEGIN
			IF @bitShipAddressJoinAdded=0 
			BEGIN
				set @WhereCondition= @WhereCondition +' left Join AddressDetails AD2 on AD2.numRecordId= DM.numDivisionID and AD2.tintAddressOf=2 and AD2.tintAddressType=2 AND AD2.bitIsPrimary=1 and AD2.numDomainID= ADC.numDomainID '
				SET @bitShipAddressJoinAdded=1;
			END
			set @WhereCondition= @WhereCondition +' left join State S'+ convert(varchar(3),@tintOrder)+ ' on S'+convert(varchar(3),@tintOrder)+ '.numStateID=AD2.numState'
		END
	  end  
END  
else if @vcAssociatedControlType = 'CheckBox'           
   begin            
               
    set @strSql= @strSql+',case when isnull('+ @vcDbColumnName +',0)=0 then ''No'' when isnull('+ @vcDbColumnName +',0)=1 then ''Yes'' end   ['+ @vcFormFieldName+'~'+ @vcDbColumnName+']'              
 
   end                                  
 else 
		BEGIN
			IF @bitIsSearchBizDoc = 0
			BEGIN
				IF @vcLookBackTableName = 'OpportunityBizDocs' AND @vcDbColumnName != 'monDealAmount' AND @vcDbColumnName != 'monAmountPaid'
				  BEGIN
	  					SET @vcDbColumnName = '''''' --include OpportunityBizDocs tables column as blank string
				  END	
			END
	  
			set @strSql=@strSql+','+ 
				case  
					when @vcLookBackTableName = 'OpportunityMaster' AND @vcDbColumnName='monDealAmount' then 'OppMas.monDealAmount' 
					when @vcDbColumnName='numAge' then 'year(getutcdate())-year(bintDOB)'   
					when @vcDbColumnName='numDivisionID' then 'DM.numDivisionID' 
					when @vcDbColumnName='vcProgress' then  ' dbo.fn_OppTotalProgress(OppMas.numOppId)'
					when @vcDbColumnName='intPEstimatedCloseDate' or @vcDbColumnName='dtDateEntered' or @vcDbColumnName='bintShippedDate'  then  
							'dbo.FormatedDateFromDate('+@vcDbColumnName+','+convert(varchar(10),@numDomainId)+')'  
					WHEN @vcDbColumnName = 'CalAmount' THEN '[dbo].[getdealamount](OppMas.numOppId,Getutcdate(),0)'
					WHEN @vcLookBackTableName = 'OpportunityBizDocs' AND @vcDbColumnName = 'monDealAmount' THEN 'ISNULL((SELECT SUM(ISNULL(BD.monDealAmount,0)) FROM   OpportunityBizDocs BD WHERE  BD.numOppId = OppMas.numOppId AND ISNULL(bitAuthoritativeBizDocs,0)=1),0)'
					WHEN @vcLookBackTableName = 'OpportunityBizDocs' AND @vcDbColumnName = 'monAmountPaid' THEN 'ISNULL((SELECT SUM(ISNULL(BD.monAmountPaid,0)) FROM   OpportunityBizDocs BD WHERE  BD.numOppId = OppMas.numOppId AND ISNULL(bitAuthoritativeBizDocs,0)=1),0)'
					else @vcDbColumnName end+' ['+ @vcFormFieldName+'~'+ @vcDbColumnName+']'	
		END
                   
                                          
 select top 1 @tintOrder=A.tintOrder+1,@vcDbColumnName=vcDbColumnName,@vcFormFieldName=vcFieldName,@vcAssociatedControlType=vcAssociatedControlType,@vcListItemType=vcListItemType,@numListID=numListID,@vcLookBackTableName=vcLookBackTableName
 from AdvSerViewConf A                                  
 join View_DynamicDefaultColumns D                                   
 on D.numFieldId=A.numFormFieldId  and d.numformId = 15    
 where D.numDomainID = @numDomainID AND D.numFormID = 15 AND A.numFormID = 15  AND tintViewID=@ViewID and numGroupID=@numGroupID and A.numDomainID=@numDomainID and A.tintOrder > @tintOrder-1 
 order by A.tintOrder asc                                  

 if @@rowcount=0 set @tintOrder=0                                  
end   
	declare @firstRec as integer                                                                        
	declare @lastRec as integer                                                                        
	set @firstRec= (@CurrentPage-1) * @PageSize                          
	set @lastRec= (@CurrentPage*@PageSize+1)                                                                         
	set @TotRecs=(select count(*) from #tempTable)   


DECLARE @from VARCHAR(8000) 
set @from = ' from OpportunityMaster OppMas   
Join AdditionalContactsinformation ADC on OppMas.numContactId = ADC.numContactId  
join DivisionMaster DM on Dm.numDivisionId = OppMas.numDivisionId  
JOIN CompanyInfo C ON DM.numCompanyID = C.numCompanyId '

IF @bitIsSearchBizDoc = 1 --Added to eliminate duplicate records 

	IF CHARINDEX('vcNotes', @WhereCondition) > 0
		BEGIN
	  		SET @from = @from + ' left join OpportunityBizDocs on OppMas.numOppId = OpportunityBizDocs.numOppId left join OpportunityBizDocItems on OpportunityBizDocItems.numOppBizDocID=OpportunityBizDocs.numOppBizDocsId '
		END
	ELSE
		BEGIN
			SET @from = @from + ' left join OpportunityBizDocs on OppMas.numOppId = OpportunityBizDocs.numOppId '
		END


SET @strSql=@strSql+@from;

 
 
 if LEN(@strMassUpdate)>1
 begin     
	Declare @strReplace as varchar(2000)
    set @strReplace = case when @LookTable='OppMas' then 'OppMas.numOppID' ELSE '' end  + @from
   
    set @strMassUpdate=replace(@strMassUpdate,'@$replace',@strReplace)
    PRINT @strMassUpdate
   exec (@strMassUpdate)         
 end         




SET @strSql=@strSql+ @WhereCondition


	SET @strSql = @strSql +' join #tempTable T on T.numOppID=OppMas.numOppID 
 WHERE ID > '+convert(varchar(10),@firstRec)+ ' and ID <'+ convert(varchar(10),@lastRec)
 IF @bitIsSearchBizDoc = 1 
 BEGIN
 	SET @strSql = @strSql + '  and T.numOppBizDocID = OpportunityBizDocs.numOppBizDocsID '
 END
 
 SET @strSql = @strSql + ' order by ID'
print @strSql
exec(@strSql)                                                                     
drop table #tempTable

 



--Left Join OpportunityItems OppItems on OppItems.numOppId = OppMas.numOppId   
--left join Item on Item.numItemCode = OppItems.numItemCode  
--left join WareHouseItems on Item.numItemCode = WareHouseItems.numItemId  
--left Join WareHouses WareHouse on WareHouse.numWareHouseId =WareHouseItems.numWareHouseId  
--left Join WareHouseItmsDTL WareHouseItemDTL on WareHouseItemDTL.numWareHouseItemID = WareHouse.numWareHouseId
GO
set ANSI_NULLS ON
set QUOTED_IDENTIFIER ON
go

--created by anoop jayaraj   
--EXEC [USP_GetPageNavigation] @numModuleID=9,@numDomainID=1

         
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getpagenavigation')
DROP PROCEDURE usp_getpagenavigation
GO
CREATE PROCEDURE [dbo].[USP_GetPageNavigation]            
@numModuleID as numeric(9),      
@numDomainID as numeric(9),
@numGroupID NUMERIC(18, 0)        
as            
if @numModuleID = 14      
begin
           
select  PND.numPageNavID,
        numModuleID,
        numParentID,
        vcPageNavName,
        isnull(vcNavURL, '') as vcNavURL,
        vcImageURL,
        0 intSortOrder
from    PageNavigationDTL PND
        JOIN dbo.TreeNavigationAuthorization TNA ON PND.numPageNavID = TNA.numPageNavID  --AND PND.numTabID = TNA.numTabID       
 WHERE  1 = 1
        AND ( ISNULL(TNA.bitVisible, 0) = 1
              --OR ISNULL(numParentID, 0) = 0
              --OR ISNULL(TNA.bitVisible, 0) = 0
            )
        AND ISNULL(PND.bitVisible, 0) = 1
        AND numModuleID = 14
        AND TNA.numDomainID = @numDomainID
        AND numGroupID = @numGroupID
union
select  1111 numPageNavID,
        14 numModuleID,
        133 numParentID,
        'Regular Documents' as vcPageNavName,
        '../Documents/frmRegularDocList.aspx?Category=0',
        '../images/tf_note.gif' vcImageURL,
        1
union

--select numListItemId,9 numModuleID,1111 numParentID,vcData vcPageNavName,'../Documents/frmDocList.aspx?TabID=1&Category=' + cast(numListItemId as varchar(10)) vcImageURL ,'../images/tf_note.gif',-2 vcImageURL
-- from ListDetails where numListid=29 and constFlag=1 
--UNION
--
--select numListItemId,9 numModuleID,1111 numParentID,vcData vcPageNavName,'../Documents/frmDocList.aspx?TabID=1&Category=' + cast(numListItemId as varchar(10)) vcImageURL ,'../images/tf_note.gif',-2 vcImageURL
-- from ListDetails where numListid=29 and numDomainID=@numDomainID
-- 
SELECT  Ld.numListItemId,
        14 numModuleID,
        1111 numParentID,
        vcData vcPageNavName,
        '../Documents/frmRegularDocList.aspx?Category='
        + cast(Ld.numListItemId as varchar(10)) vcImageURL,
        '../images/tf_note.gif',
        ISNULL(intSortOrder, LD.sintOrder) SortOrder
FROM    listdetails Ld
        left join listorder LO on Ld.numListItemID = LO.numListItemID
                                  and Lo.numDomainId = @numDomainID
WHERE   Ld.numListID = 29
        and ( constFlag = 1
              or Ld.numDomainID = @numDomainID
            )
--order by ISNULL(intSortOrder,LD.sintOrder)
--union
--   
-- SELECT 0 numPageNavID,9 numModuleID,1111 numParentID, vcData vcPageNavName,'../Documents/frmDocList.aspx?TabID=1&Category='+convert(varchar(10), LD.numListItemID) vcNavURL      
-- ,'../images/Text.gif' vcImageURL,0      
-- FROM listdetails Ld   
-- left join listorder LO on Ld.numListItemID= LO.numListItemID  and lo.numDomainId = @numDomainId        
-- WHERE  LD.numListID=28 and (constFlag=1 or  LD.numDomainID=@numDomainID)    
union
SELECT  ld.numListItemID AS numPageNavID,
        14 numModuleID,
        133 numParentID,
        vcData vcPageNavName,
        '../Documents/frmDocList.aspx?Category='
        + convert(varchar(10), LD.numListItemID) vcNavURL,
        '../images/tf_note.gif' vcImageURL,
        -3
FROM    listdetails Ld
        left join listorder LO on Ld.numListItemID = LO.numListItemID
                                  and lo.numDomainId = @numDomainId
        inner join AuthoritativeBizDocs AB on ( LD.numListItemID = AB.numAuthoritativeSales )
WHERE   LD.numListID = 27
        and ( constFlag = 1
              or LD.numDomainID = @numDomainID
            )
        and AB.numDomainID = @numDomainID
union
SELECT  ld.numListItemID AS numPageNavID,
        14 numModuleID,
        133 numParentID,
        vcData vcPageNavName,
        '../Documents/frmDocList.aspx?Category='
        + convert(varchar(10), LD.numListItemID) vcNavURL,
        '../images/tf_note.gif' vcImageURL,
        -3
FROM    listdetails Ld
        left join listorder LO on Ld.numListItemID = LO.numListItemID
                                  and lo.numDomainId = @numDomainId
        inner join AuthoritativeBizDocs AB on ( ld.numListItemID = AB.numAuthoritativePurchase )
WHERE   LD.numListID = 27
        and ( constFlag = 1
              or LD.numDomainID = @numDomainID
            )
        and AB.numDomainID = @numDomainID
UNION
select  0 numPageNavID,
        14 numModuleID,
        LD.numListItemID AS numParentID,
        LT.vcData as vcPageNavName,
        '../Documents/frmDocList.aspx?Category='
        + convert(varchar(10), Ld.numListItemID) + '&Status='
        + convert(varchar(10), LT.numListItemID) vcNavURL,
        '../images/Text.gif' vcImageURL,
        0
from    ListDetails LT,
        listdetails ld
where   LT.numListID = 11
        and Lt.numDomainID = @numDomainID
        AND LD.numListID = 27
        AND ( ld.constFlag = 1
              or LD.numDomainID = @numDomainID
            )
        and ( LD.numListItemID in ( select  AB.numAuthoritativeSales
                                    from    AuthoritativeBizDocs AB
                                    where   AB.numDomainId = @numDomainID )
              or ld.numListItemID in ( select   AB.numAuthoritativePurchase
                                       from     AuthoritativeBizDocs AB
                                       where    AB.numDomainId = @numDomainID )
            )


--SELECT * FROM AuthoritativeBizDocs where numDomainId=72

 
--UNION
--	SELECT 0 numPageNavID,9 numModuleID,133 numParentID, 'BizDoc Approval Rule' as  vcPageNavName,'../Documents/frmBizDocAppRule.aspx' as vcNavURL      
-- ,'../images/tf_note.gif' vcImageURL,0 
--
--UNION
--	SELECT 0 numPageNavID,9 numModuleID,133 numParentID, 'Order Auto Rule' as  vcPageNavName,'../Documents/frmOrderAutoRules.aspx' as vcNavURL      
-- ,'../images/tf_note.gif' vcImageURL,1  
union
select  2222 numPageNavID,
        14 numModuleID,
        133 numParentID,
        'Other BizDocs' as vcPageNavName,
        '',
        '../images/tf_note.gif' vcImageURL,
        -1
union
SELECT  ld.numListItemID AS numPageNavID,
        14 numModuleID,
        2222 numParentID,
        vcData vcPageNavName,
        '../Documents/frmDocList.aspx?Category='
        + convert(varchar(10), LD.numListItemID) vcNavURL,
        '../images/Text.gif' vcImageURL,
        -1
FROM    listdetails Ld
where   ld.numListID = 27
        and ( constFlag = 1
              or LD.numDomainID = @numDomainID
            )
        and LD.numListItemID not in ( select    AB.numAuthoritativeSales
                                      from      AuthoritativeBizDocs AB
                                      where     AB.numDomainID = @numDomainID )
        and LD.numListItemID not in ( select    AB.numAuthoritativePurchase
                                      from      AuthoritativeBizDocs AB
                                      where     AB.numDomainID = @numDomainID )
union
SELECT  ls.numListItemID AS numPageNavID,
        14 numModuleID,
        ld.numListItemID as numParentID,
        ls.vcData vcPageNavName,
        '../Documents/frmDocList.aspx?Category='
        + convert(varchar(10), LD.numListItemID) vcNavURL,
        null vcImageURL,
        0
FROM    listdetails Ld,
        ( select    *
          from      ListDetails LS
          where     LS.numDomainID = @numDomainID
                    and LS.numListID = 11
        ) LS
where   ld.numListID = 27
        and ( ld.constFlag = 1
              or LD.numDomainID = @numDomainID
            )
        and LD.numListItemID not in ( select    AB.numAuthoritativeSales
                                      from      AuthoritativeBizDocs AB
                                      where     AB.numDomainID = @numDomainID
                                      union
                                      select    AB.numAuthoritativePurchase
                                      from      AuthoritativeBizDocs AB
                                      where     AB.numDomainID = @numDomainID )
order by numParentID,
        intSortOrder,
        numPageNavID

END
ELSE IF @numModuleID = 35 
BEGIN
	select PND.numPageNavID,numModuleID,numParentID,vcPageNavName,isnull(vcNavURL,'') as vcNavURL,vcImageURL ,0 sintOrder      
	from PageNavigationDTL PND
	JOIN dbo.TreeNavigationAuthorization TNA ON PND.numPageNavID = TNA.numPageNavID  --AND PND.numTabID = TNA.numTabID       
	WHERE  1 = 1
        AND ( ISNULL(TNA.bitVisible, 0) = 1
              --OR ISNULL(numParentID, 0) = 0
              --OR ISNULL(TNA.bitVisible, 0) = 0
            ) 
        AND ISNULL(PND.bitVisible, 0) = 1
	AND numModuleID=@numModuleID --AND bitVisible=1  
	AND TNA.numDomainID = @numDomainID
	AND numGroupID = @numGroupID
	    
	UNION 
	 SELECT numFRID,35,CASE numFinancialViewID WHEN 26770 THEN 96 /*Profit & Loss*/
	WHEN 26769 THEN 176 /*Income & Expense*/
	WHEN 26768 THEN 98 /*Cash Flow Statement*/
	WHEN 26767 THEN 97 /*Balance Sheet*/
	WHEN 26766 THEN 81 /*A/R & A/P Aging*/
	WHEN 26766 THEN 82 /*A/R & A/P Aging*/
	ELSE 0
	END 
	  ,vcReportName,CASE numFinancialViewID 
	WHEN 26770 THEN (SELECT vcNavURL FROM dbo.PageNavigationDTL WHERE numPageNavID =96 AND numModuleID=35)  /*Profit & Loss*/
	WHEN 26769 THEN (SELECT vcNavURL FROM dbo.PageNavigationDTL WHERE numPageNavID =176 AND numModuleID=35) /*Income & Expense*/
	WHEN 26768 THEN (SELECT vcNavURL FROM dbo.PageNavigationDTL WHERE numPageNavID =98 AND numModuleID=35) /*Cash Flow Statement*/
	WHEN 26767 THEN (SELECT vcNavURL FROM dbo.PageNavigationDTL WHERE numPageNavID =97 AND numModuleID=35) /*Balance Sheet*/
	WHEN 26766 THEN (SELECT vcNavURL FROM dbo.PageNavigationDTL WHERE numPageNavID =81 AND numModuleID=35) /*A/R & A/P Aging*/
	WHEN 26766 THEN (SELECT vcNavURL FROM dbo.PageNavigationDTL WHERE numPageNavID =82 AND numModuleID=35) /*A/R & A/P Aging*/
	ELSE ''
	END + 
	CASE CHARINDEX('?',(CASE numFinancialViewID 
	WHEN 26770 THEN (SELECT vcNavURL FROM dbo.PageNavigationDTL WHERE numPageNavID =96 AND numModuleID=35)  /*Profit & Loss*/
	WHEN 26769 THEN (SELECT vcNavURL FROM dbo.PageNavigationDTL WHERE numPageNavID =176 AND numModuleID=35) /*Income & Expense*/
	WHEN 26768 THEN (SELECT vcNavURL FROM dbo.PageNavigationDTL WHERE numPageNavID =98 AND numModuleID=35) /*Cash Flow Statement*/
	WHEN 26767 THEN (SELECT vcNavURL FROM dbo.PageNavigationDTL WHERE numPageNavID =97 AND numModuleID=35) /*Balance Sheet*/
	WHEN 26766 THEN (SELECT vcNavURL FROM dbo.PageNavigationDTL WHERE numPageNavID =81 AND numModuleID=35) /*A/R & A/P Aging*/
	WHEN 26766 THEN (SELECT vcNavURL FROM dbo.PageNavigationDTL WHERE numPageNavID =82 AND numModuleID=35) /*A/R & A/P Aging*/
	ELSE ''
	END)) 
	WHEN 0 THEN '?FRID='
	ELSE '&FRID='
	END
	 + CAST(numFRID AS VARCHAR)
	 'url',NULL,0 FROM dbo.FinancialReport WHERE numDomainID=@numDomainID
	order by numParentID,numPageNavID  
END
-------------- ADDED BY MANISH ANJARA ON : 6th April,2012
ELSE IF @numModuleID = -1 AND @numDomainID = -1 
BEGIN 
	SELECT PND.numPageNavID,numModuleID,numParentID,vcPageNavName,isnull(vcNavURL,'') as vcNavURL,vcImageURL ,0 sintOrder      
	FROM PageNavigationDTL PND
	JOIN dbo.TreeNavigationAuthorization TNA ON PND.numPageNavID = TNA.numPageNavID  --AND PND.numTabID = TNA.numTabID       
	WHERE  1 = 1
        AND ( ISNULL(TNA.bitVisible, 0) = 1
              --OR ISNULL(numParentID, 0) = 0
              --OR ISNULL(TNA.bitVisible, 0) = 0
            )
        AND ISNULL(PND.bitVisible, 0) = 1
	--AND TNA.numDomainID = @numDomainID
	AND numGroupID = @numGroupID
	--WHERE bitVisible=1  
	ORDER BY numParentID,numPageNavID 
END
--------------------------------------------------------- 
-------------- ADDED BY MANISH ANJARA ON : 12th Oct,2012
ELSE IF @numModuleID = 10 
BEGIN 
	DECLARE @numParentID AS NUMERIC(18,0)
	SELECT TOP 1 @numParentID = numParentID FROM dbo.PageNavigationDTL WHERE numModuleID = 10
	PRINT @numParentID

	SELECT * FROM (
	SELECT PND.numPageNavID,numModuleID,numParentID,vcPageNavName,isnull(vcNavURL,'') as vcNavURL,vcImageURL ,0 sintOrder      
	FROM PageNavigationDTL PND
	JOIN dbo.TreeNavigationAuthorization TNA ON PND.numPageNavID = TNA.numPageNavID  --AND PND.numTabID = TNA.numTabID       
	WHERE  1 = 1
        AND ( ISNULL(TNA.bitVisible, 0) = 1
              --OR ISNULL(numParentID, 0) = 0
              --OR ISNULL(TNA.bitVisible, 0) = 0
            )
        AND ISNULL(PND.bitVisible, 0) = 1
	AND numModuleID = @numModuleID
	AND TNA.numDomainID = @numDomainID
	AND numGroupID = @numGroupID
	
	UNION ALL

	SELECT  numListItemID AS numPageNavID,
		    (SELECT numModuleID FROM ModuleMaster WHERE vcModuleName = 'Opportunities') AS [numModuleID],
			@numParentID AS numParentID,
			ISNULL(vcData,'') + 's' [vcPageNavName],
			'../Opportunity/frmOpenBizDocs.aspx?BizDocName=' + vcData + '&BizDocID=' + CONVERT(VARCHAR(10),LD.numListItemID) AS [vcNavURL],
			'../images/Text.gif' AS [vcImageURL],
			1 AS [bitVisible] 
	FROM dbo.ListDetails LD
	JOIN dbo.TreeNavigationAuthorization TNA ON LD.numListItemID = TNA.numPageNavID  AND ISNULL(bitVisible,0) = 1  
	WHERE numListID = 27 
	AND ISNULL(TNA.[numDomainID], 0) = @numDomainID
	AND ISNULL(TNA.[numGroupID], 0) = @numGroupID) AS [FINAL]
	ORDER BY numParentID,FINAL.numPageNavID 
END
--------------------------------------------------------- 
--ELSE IF @numModuleID = 13
--	BEGIN
--		select PND.numPageNavID,
--        numModuleID,
--        numParentID,
--        vcPageNavName,
--        isnull(vcNavURL, '') as vcNavURL,
--        vcImageURL,
--        0 sintOrder
-- from   PageNavigationDTL PND
-- WHERE  1 = 1
--        AND ISNULL(PND.bitVisible, 0) = 1
--        AND numModuleID = @numModuleID
-- order by numParentID,
--        numPageNavID 
--	END
--	
--	ELSE IF @numModuleID = 9
--	BEGIN
--			select PND.numPageNavID,
--        numModuleID,
--        numParentID,
--        vcPageNavName,
--        isnull(vcNavURL, '') as vcNavURL,
--        vcImageURL,
--        0 sintOrder
-- from   PageNavigationDTL PND
-- WHERE  1 = 1
--        AND ISNULL(PND.bitVisible, 0) = 1
--        AND numModuleID = @numModuleID
-- order by numParentID,
--        numPageNavID 
--	END

ELSE IF (@numModuleID = 7 OR @numModuleID = 36)
BEGIN
  select    PND.numPageNavID,
            numModuleID,
            numParentID,
            vcPageNavName,
            isnull(vcNavURL, '') as vcNavURL,
            vcImageURL,
            0 sintOrder
  from      PageNavigationDTL PND
  WHERE     1 = 1
            AND ISNULL(PND.bitVisible, 0) = 1
            AND numModuleID = @numModuleID
  order by  numParentID,
            numPageNavID 
END	
else      
begin            
 select PND.numPageNavID,
        PND.numModuleID,
        numParentID,
        vcPageNavName,
        isnull(vcNavURL, '') as vcNavURL,
        vcImageURL,
        0 sintOrder
 from   PageNavigationDTL PND
        JOIN dbo.TreeNavigationAuthorization TNA ON PND.numPageNavID = TNA.numPageNavID  --AND PND.numTabID = TNA.numTabID       
 WHERE  1 = 1
        AND ( ISNULL(TNA.bitVisible, 0) = 1
              --OR ISNULL(numParentID, 0) = 0
              --OR ISNULL(TNA.bitVisible, 0) = 0
            )
        AND ISNULL(PND.bitVisible, 0) = 1
        AND PND.numModuleID = @numModuleID --AND bitVisible=1  
        AND numGroupID = @numGroupID
 order by numParentID,
        numPageNavID      
END

--select * from TreeNavigationAuthorization where numModuleID=13 
--select * from TreeNavigationAuthorization where numTabID=-1 AND numDomainID = 1 AND numGroupID = 1
--select * from TreeNavigationAuthorization where numListID=11 and numDomainId=72
--select * from AuthoritativeBizDocs where numDomainId=72











/****** Object:  StoredProcedure [dbo].[USP_GetShippingReport]    Script Date: 05/07/2009 17:34:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--SELECT * FROM ShippingReport
--SELECT * FROM ShippingReportItems
-- USP_GetShippingReport 1,60754,792
-- exec USP_GetShippingReport @numDomainID=72,@numOppBizDocId=5465,@ShippingReportId=4
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'USP_GetShippingReport' ) 
    DROP PROCEDURE USP_GetShippingReport
GO
CREATE PROCEDURE [dbo].[USP_GetShippingReport]
    @numDomainID NUMERIC(9),
    @numOppBizDocId NUMERIC(9),
    @ShippingReportId NUMERIC(9)
AS 
    BEGIN
  
        SELECT  SRI.numItemCode,
                I.vcItemName,
                I.vcModelID,
                CASE WHEN LEN(BDI.vcItemDesc) > 100
                     THEN CONVERT(VARCHAR(100), BDI.vcItemDesc) + '..'
                     ELSE BDI.vcItemDesc
                END vcItemDesc,
                dbo.FormatedDateFromDate(SRI.dtDeliveryDate, @numDomainID) dtDeliveryDate,
                SRI.tintServiceType,
                ISNULL(I.fltWeight, 0) AS [fltTotalWeight],
                ISNULL(I.[fltHeight], 0) AS [fltHeight],
                ISNULL(I.[fltLength], 0) AS [fltLength],
                ISNULL(I.[fltWidth], 0) AS [fltWidth],
                ISNULL(SRI.monShippingRate,(SELECT monTotAmount FROM dbo.OpportunityItems 
											WHERE SR.numOppID = dbo.OpportunityItems.numOppId
											AND numItemCode = (SELECT numShippingServiceITemID FROM dbo.Domain WHERE numDomainId = @numDomainId))) [monShippingRate],
                SB.vcShippingLabelImage,
                SB.vcTrackingNumber,
                SRI.ShippingReportItemId,
                SRI.intNoOfBox,
                SR.[vcValue3],
                SR.[vcValue4],
                CASE WHEN ISNUMERIC(SR.vcFromState) = 1
                     THEN ( SELECT  vcStateCode
                            FROM    ShippingStateMaster
                            WHERE   vcStateName = ( SELECT TOP 1
                                                            vcState
                                                    FROM    State
                                                    WHERE   numStateID = SR.vcFromState
                                                  )
                                    AND [numShipCompany] = SR.[numShippingCompany]
                          )
                     ELSE ''
                END vcFromState,
                CASE WHEN ISNUMERIC(SR.vcFromCountry) = 1
                     THEN ( SELECT  vcCountryCode
                            FROM    ShippingCountryMaster
                            WHERE   vcCountryName = ( SELECT TOP 1
                                                                vcData
                                                      FROM      ListDetails
                                                      WHERE     numListItemID = SR.vcFromCountry
                                                    )
                                    AND [numShipCompany] = SR.[numShippingCompany]
                          )
                     ELSE ''
                END vcFromCountry,
                CASE WHEN ISNUMERIC(SR.vcToState) = 1
                     THEN ( SELECT  vcStateCode
                            FROM    ShippingStateMaster
                            WHERE   vcStateName = ( SELECT TOP 1
                                                            vcState
                                                    FROM    State
                                                    WHERE   numStateID = SR.vcToState
                                                  )
                                    AND [numShipCompany] = SR.[numShippingCompany]
                          )
                     ELSE ''
                END vcToState,
                CASE WHEN ISNUMERIC(SR.vcToCountry) = 1
                     THEN ( SELECT  vcCountryCode
                            FROM    ShippingCountryMaster
                            WHERE   vcCountryName = ( SELECT TOP 1
                                                                vcData
                                                      FROM      ListDetails
                                                      WHERE     numListItemID = SR.vcToCountry
                                                    )
                                    AND [numShipCompany] = SR.[numShippingCompany]
                          )
                     ELSE ''
                END vcToCountry,
                SR.vcToZip,
                SR.vcFromZip,
                SR.[numShippingCompany],
                SB.[numBoxID],
                SB.[vcBoxName],
                BDI.numOppBizDocItemID,
                SR.tintPayorType,
                ISNULL(SR.vcPayorAccountNo, '') vcPayorAccountNo,
                ISNULL(SR.numPayorCountry, 0) numPayorCountry,
                ( SELECT    vcCountryCode
                  FROM      ShippingCountryMaster
                  WHERE     vcCountryName = ( SELECT TOP 1
                                                        vcData
                                              FROM      ListDetails
                                              WHERE     numListItemID = SR.numPayorCountry
                                            )
                            AND [numShipCompany] = SR.[numShippingCompany]
                ) vcPayorCountryCode,
                SR.vcPayorZip,
                SR.vcFromCity,
                SR.vcFromAddressLine1,
                SR.vcFromAddressLine2,
                ISNULL(SR.vcFromCountry,0) [numFromCountry],
                ISNULL(SR.vcFromState,0) [numFromState],
                SR.vcToCity,
                SR.vcToAddressLine1,
                SR.vcToAddressLine2,
                ISNULL(SR.vcToCountry,0) [numToCountry],
                ISNULL(SR.vcToState,0) [numToState],
                SRI.intBoxQty,
                ISNULL(SB.numServiceTypeID, 0) AS [numServiceTypeID],
                ISNULL(SR.vcFromCompany, '') AS vcFromCompany,
                ISNULL(SR.vcFromName, '') AS vcFromName,
                ISNULL(SR.vcFromPhone, '') AS vcFromPhone,
                ISNULL(SR.vcToCompany, '') AS vcToCompany,
                ISNULL(SR.vcToName, '') AS vcToName,
                ISNULL(SR.vcToPhone, '') AS vcToPhone,
                ISNULL(SB.fltDimensionalWeight, 0) AS [fltDimensionalWeight],
                dbo.fn_GetContactName(SR.numCreatedBy) + ' : '
                + CONVERT(VARCHAR(20), SR.dtCreateDate) AS [CreatedDetails],
                CASE WHEN SR.numShippingCompany = 91 THEN 'Fedex'
                     WHEN SR.numShippingCompany = 88 THEN 'UPS'
                     WHEN SR.numShippingCompany = 90 THEN 'USPS'
                     ELSE 'Other'
                END AS [vcShipCompany],
                CAST(ROUND(( ( SELECT   SUM(fltTotalWeight)
                               FROM     dbo.ShippingReportItems
                               WHERE    numShippingReportId = SR.numShippingReportId
                             ) * intBoxQty ), 2) AS DECIMAL(9, 2)) AS [fltTotalRegularWeight],
                CAST(ROUND(( SELECT SUM(fltDimensionalWeight)
                             FROM   dbo.ShippingBox
                             WHERE  numShippingReportId = SR.numShippingReportId
                           ), 2) AS DECIMAL(9, 2)) AS [fltTotalDimensionalWeight],
                ISNULL(monTotAmount, 0) AS [monTotAmount],
                ISNULL(bitFromResidential,0) [bitFromResidential],
                ISNULL(bitToResidential,0) [bitToResidential],
                ISNULL(IsCOD,0) [IsCOD],
				ISNULL(IsDryIce,0) [IsDryIce],
				ISNULL(IsHoldSaturday,0) [IsHoldSaturday],
				ISNULL(IsHomeDelivery,0) [IsHomeDelivery],
				ISNULL(IsInsideDelevery,0) [IsInsideDelevery],
				ISNULL(IsInsidePickup,0) [IsInsidePickup],
				ISNULL(IsReturnShipment,0) [IsReturnShipment],
				ISNULL(IsSaturdayDelivery,0) [IsSaturdayDelivery],
				ISNULL(IsSaturdayPickup,0) [IsSaturdayPickup],
				ISNULL(IsAdditionalHandling,0) [IsAdditionalHandling],
				ISNULL(vcCODType,0) [vcCODType],
				numOppID,
				ISNULL(numCODAmount,0) [numCODAmount]
        FROM    ShippingReport AS SR
                INNER JOIN ShippingBox SB ON SB.numShippingReportId = SR.numShippingReportId
                INNER JOIN ShippingReportItems AS SRI ON SR.numShippingReportId = SRI.numShippingReportId
                                                         AND SRI.numBoxID = SB.numBoxID
                INNER JOIN OpportunityBizDocItems BDI ON SRI.numItemCode = BDI.numItemCode
                                                         AND SR.numOppBizDocId = BDI.numOppBizDocID
                INNER JOIN Item I ON BDI.numItemCode = I.numItemCode
        WHERE   SR.[numOppBizDocId] = @numOppBizDocId
                AND SR.numDomainId = @numDomainID
                AND SR.[numShippingReportId] = @ShippingReportId
--           AND (SB.bitIsMasterTrackingNo = 1 OR SB.bitIsMasterTrackingNo IS NULL)
           
        SELECT  vcBizDocID
        FROM    [OpportunityBizDocs]
        WHERE   [numOppBizDocsId] = @numOppBizDocId

    END
/****** Object:  StoredProcedure [dbo].[USP_GetWareHouseItems]    Script Date: 07/26/2008 16:18:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--exec USP_GetWareHouseItems 6                            
--created by anoop jayaraj                              
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_getwarehouseitems')
DROP PROCEDURE usp_getwarehouseitems
GO
CREATE PROCEDURE [dbo].[USP_GetWareHouseItems]                              
@numItemCode as numeric(9)=0,
@byteMode as tinyint=0                              
as                              
                              
                          
                              
declare @bitSerialize as bit                      
declare @str as nvarchar(max)                 
declare @str1 as nvarchar(max)               
declare @ColName as varchar(25)                      
set @str=''                       
                        
DECLARE @bitKitParent BIT
declare @numItemGroupID as numeric(9)                        
                        
select @numItemGroupID=numItemGroup,@bitSerialize=CASE WHEN bitSerialized=0 THEN bitLotNo ELSE bitSerialized END,
@bitKitParent = ( CASE WHEN ISNULL(bitKitParent,0) = 1
                               AND ISNULL(bitAssembly,0) = 1 THEN 0
                          WHEN ISNULL(bitKitParent,0) = 1 THEN 1
                          ELSE 0
                     END ) from Item where numItemCode=@numItemCode                        
if @bitSerialize=1 set @ColName='numWareHouseItmsDTLID,1'              
else set @ColName='numWareHouseItemID,0'              
              
--Create a Temporary table to hold data                                                            
create table #tempTable ( ID INT IDENTITY PRIMARY KEY,                                                                      
 numCusFlDItemID numeric(9)                                                         
 )                         
                        
insert into #tempTable                         
(numCusFlDItemID)                                                            
select distinct(numOppAccAttrID) from ItemGroupsDTL where numItemGroupID=@numItemGroupID and tintType=2                       
                          
                 
 declare @ID as numeric(9)                        
 declare @numCusFlDItemID as varchar(20)                        
 declare @fld_label as varchar(100),@fld_type as varchar(100)                        
 set @ID=0                        
 select top 1 @ID=ID,@numCusFlDItemID=numCusFlDItemID,@fld_label=fld_label,@fld_type=fld_type from #tempTable                         
 join CFW_Fld_Master on numCusFlDItemID=Fld_ID                        
                         
 while @ID>0                        
 begin                        
                          
   set @str=@str+',  dbo.GetCustFldItems('+@numCusFlDItemID+',9,'+@ColName+') as ['+ @fld_label+']'
	
	IF @byteMode=1                                        
		set @str=@str+',  dbo.GetCustFldItemsValue('+@numCusFlDItemID+',9,'+@ColName+','''+@fld_type+''') as ['+ @fld_label+'Value]'                                        
                          
   select top 1 @ID=ID,@numCusFlDItemID=numCusFlDItemID,@fld_label=fld_label from #tempTable                         
   join CFW_Fld_Master on numCusFlDItemID=Fld_ID and ID >@ID                        
   if @@rowcount=0 set @ID=0                        
                          
 end                        
                       
--      

DECLARE @KitOnHand AS NUMERIC(9);SET @KitOnHand=0

--IF @bitKitParent=1
	--SELECT @KitOnHand=ISNULL(dbo.fn_GetKitInventory(@numItemCode),0)          
  
set @str1='select '

IF @byteMode=1                                        
		set @str1 =@str1 +'I.numItemCode,'
	
set @str1 =@str1 + 'numWareHouseItemID,isnull(vcWarehouse,'''') + '': '' + isnull(WL.vcLocation,'''') vcWarehouse,W.numWareHouseID,
Case when @bitKitParent=1 then ISNULL(dbo.fn_GetKitInventoryByWarehouse(I.numItemCode, W.numWareHouseID),0) else isnull(numOnHand,0) end as [OnHand],
CAST((dbo.fn_UOMConversion(ISNULL(I.numBaseUnit,0),I.numItemCode,I.numDomainId,ISNULL(I.numPurchaseUnit,0)) * Case when @bitKitParent=1 THEN ISNULL(dbo.fn_GetKitInventoryByWarehouse(I.numItemCode, W.numWareHouseID),0) ELSE isnull(numOnHand,0) end) as numeric(18,2)) as PurchaseOnHand,
CAST((dbo.fn_UOMConversion(ISNULL(I.numBaseUnit,0),I.numItemCode,I.numDomainId,ISNULL(I.numSaleUnit,0)) * Case when @bitKitParent=1 THEN ISNULL(dbo.fn_GetKitInventoryByWarehouse(I.numItemCode, W.numWareHouseID),0) ELSE isnull(numOnHand,0) end) as numeric(18,2)) as SalesOnHand,
Case when @bitKitParent=1 THEN 0 ELSE isnull(numReorder,0) END as [Reorder] 
,Case when @bitKitParent=1 THEN 0 ELSE isnull(numOnOrder,0) END as [OnOrder]
,Case when @bitKitParent=1 THEN 0 ELSE isnull(numAllocation,0) END as [Allocation] 
,Case when @bitKitParent=1 THEN 0 ELSE isnull(numBackOrder,0) END as [BackOrder]
,0 as Op_Flag , isnull(WL.vcLocation,'''') Location,isnull(WL.numWLocationID,0) as numWLocationID,
round(isnull(monWListPrice,0),2) Price,isnull(vcWHSKU,'''') as SKU,isnull(vcBarCode,'''') as BarCode '+ case when @bitSerialize=1 then  ' ' else  @str end +'                   
,(SELECT CASE WHEN ISNULL(subI.bitKitParent,0) = 1 AND ISNULL((SELECT COUNT(*) FROM OpportunityItems 
															   WHERE OpportunityItems.numItemCode = WareHouseItems.numItemID
															   AND WareHouseItems.numWareHouseItemID = OpportunityItems.numWarehouseItmsID),0) = 0 THEN 1
			ELSE 0 
	    END 
FROM Item subI WHERE subI.numItemCode = WareHouseItems.numItemID) [IsDeleteKitWarehouse],@bitKitParent [bitKitParent]
from WareHouseItems                           
join Warehouses W                             
on W.numWareHouseID=WareHouseItems.numWareHouseID 
left join WarehouseLocation WL on WL.numWLocationID =WareHouseItems.numWLocationID
join Item I on I.numItemCode=WareHouseItems.numItemID and WareHouseItems.numDomainId=I.numDomainId
where numItemID='+convert(varchar(15),@numItemCode)                  
   
print(@str1)           

EXECUTE sp_executeSQL @str1, N'@bitKitParent bit',@bitKitParent
                       
                        
set @str1='select numWareHouseItmsDTLID,WDTL.numWareHouseItemID, vcSerialNo,0 as Op_Flag,WDTL.vcComments as Comments,WDTL.numQty,WDTL.numQty as OldQty,W.vcWarehouse '+ case when @bitSerialize=1 then @str else '' end +'
--,WDTL.dExpirationDate 
from WareHouseItmsDTL WDTL                             
join WareHouseItems WI                             
on WDTL.numWareHouseItemID=WI.numWareHouseItemID                              
join Warehouses W                             
on W.numWareHouseID=WI.numWareHouseID  
where (tintStatus is null or tintStatus=0)  and  numItemID='+ convert(varchar(15),@numItemCode)                          
print @str1                       
exec (@str1)                       
                      
                      
select Fld_label,fld_id,fld_type,numlistid,vcURL from #tempTable                         
 join CFW_Fld_Master on numCusFlDItemID=Fld_ID                      
drop table #tempTable


-----------------For Kendo UI testting purpose
--create table #tempColumnConfig ( field varchar(100),                                                                      
-- title varchar(100) , format varchar(100)                                                      
-- )  
--
--insert into #tempColumnConfig
--	select 'vcWarehouse','WareHouse','' union all
--	select 'OnHand','On Hand','' union all
--	select 'Reorder','Re Order','' union all
--	select 'Allocation','Allocation','' union all
--	select 'BackOrder','Back Order','' union all
--	select 'Price','Price','{0:c}'
--
--select * from #tempColumnConfig
--
--drop table #tempColumnConfig

-----------------

--set @str1='select case when COUNT(*)=1 then MIN(numWareHouseItmsDTLID) else 0 end as numWareHouseItmsDTLID,MIN(WDTL.numWareHouseItemID) as numWareHouseItemID, vcSerialNo,0 as Op_Flag,WDTL.vcComments as Comments'+ case when @bitSerialize=1 then @str else '' end +',W.vcWarehouse,COUNT(*) Qty
--from WareHouseItmsDTL WDTL                             
--join WareHouseItems WI                             
--on WDTL.numWareHouseItemID=WI.numWareHouseItemID                              
--join Warehouses W                             
--on W.numWareHouseID=WI.numWareHouseID  
--where (tintStatus is null or tintStatus=0)  and  numItemID='+ convert(varchar(15),@numItemCode) +'
--GROUP BY vcSerialNo,WDTL.vcComments,W.vcWarehouse'
--                          
--print @str1                       
--exec (@str1)                       
              
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='USP_GetWarehouseItemsBySKU')
DROP PROCEDURE USP_GetWarehouseItemsBySKU
GO
/****** Object:  StoredProcedure [dbo].[USP_GetWarehouseItemsBySKU]    Script Date: 11/03/2014 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[USP_GetWarehouseItemsBySKU]
(
	@numItemCode NUMERIC(18,0),
	@numWarehouseID NUMERIC(18,0) = 0,
	@vcSKU VARCHAR(50),
	@numDomainID NUMERIC(18,0),
	@numWareHouseItemID NUMERIC(18,0) = 0
)
AS
BEGIN
	
	SELECT * FROM dbo.WareHouseItems WI 
	WHERE numWareHouseID = @numWarehouseID
	AND numItemID = @numItemCode
	AND numDomainID = @numDomainID
	AND ISNULL(vcWHSKU,'') = @vcSKU
	AND (numWareHouseItemID = @numWareHouseItemID OR @numWareHouseItemID = 0)
	
END
GO
/*
declare @p7 int
set @p7=0
exec USP_ItemList1 @ItemClassification=0,@KeyWord='',@IsKit=0,@SortChar='0',@CurrentPage=1,@PageSize=10,@TotRecs=@p7 
output,@columnName='vcItemName',@columnSortOrder='Asc',@numDomainID=1,@ItemType=' ',@bitSerialized=0,@numItemGroup=0,@bitAssembly=0,@numUserCntID=1,@Where='',@IsArchive=0
select @p7
*/

GO
/****** Object:  StoredProcedure [dbo].[USP_ItemList1]    Script Date: 05/07/2009 18:14:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--- Created By Anoop Jayaraj                                        
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'usp_itemlist1' ) 
    DROP PROCEDURE usp_itemlist1
GO
CREATE PROCEDURE [dbo].[USP_ItemList1]
    @ItemClassification AS NUMERIC(9) = 0,
    @KeyWord AS VARCHAR(1000) = '',
    @IsKit AS TINYINT,
    @SortChar CHAR(1) = '0',
    @CurrentPage INT,
    @PageSize INT,
    @TotRecs INT OUTPUT, 
    @columnName AS VARCHAR(50),
    @columnSortOrder AS VARCHAR(10),
    @numDomainID AS NUMERIC(9) = 0,
    @ItemType AS CHAR(1),
    @bitSerialized AS BIT,
    @numItemGroup AS NUMERIC(9),
    @bitAssembly AS BIT,
    @numUserCntID AS NUMERIC(9),
    @Where	AS VARCHAR(MAX),
    @IsArchive AS BIT = 0,
    @byteMode AS TINYINT
AS 
	
	 DECLARE @Nocolumns AS TINYINT               
    SET @Nocolumns = 0                
 
    SELECT  @Nocolumns = ISNULL(SUM(TotalRow), 0)
    FROM    ( SELECT    COUNT(*) TotalRow
              FROM      View_DynamicColumns
              WHERE     numFormId = 21
                        AND numUserCntID = @numUserCntID
                        AND numDomainID = @numDomainID
                        AND tintPageType = 1 AND ISNULL(numRelCntType,0)=0
              UNION
              SELECT    COUNT(*) TotalRow
              FROM      View_DynamicCustomColumns
              WHERE     numFormId = 21
                        AND numUserCntID = @numUserCntID
                        AND numDomainID = @numDomainID
                        AND tintPageType = 1 AND ISNULL(numRelCntType,0)=0
            ) TotalRows
               
 
 if @Nocolumns=0
BEGIN
INSERT INTO DycFormConfigurationDetails (numFormID,numFieldID,intColumnNum,intRowNum,numDomainID,numUserCntID,numRelCntType,tintPageType,bitCustom,intColumnWidth,numViewID)
select 21,numFieldId,0,Row_number() over(order by tintRow desc),@numDomainID,@numUserCntID,0,1,0,intColumnWidth,0
 FROM    View_DynamicDefaultColumns
                    WHERE   numFormId = 21
                            AND bitDefault = 1
                            AND ISNULL(bitSettingField, 0) = 1
                            AND numDomainID = @numDomainID
                    ORDER BY tintOrder ASC 
END

    CREATE TABLE #tempForm
        (
          tintOrder TINYINT,
          vcDbColumnName NVARCHAR(50),
          vcFieldName NVARCHAR(50),
          vcAssociatedControlType NVARCHAR(50),
          vcListItemType CHAR(3),
          numListID NUMERIC(9),
          vcLookBackTableName VARCHAR(50),
          bitCustomField BIT,
          numFieldId NUMERIC,
          bitAllowSorting BIT,
          bitAllowEdit BIT,
          bitIsRequired BIT,
          bitIsEmail BIT,
          bitIsAlphaNumeric BIT,
          bitIsNumeric BIT,
          bitIsLengthValidation BIT,
          intMaxLength INT,
          intMinLength INT,
          bitFieldMessage BIT,
          vcFieldMessage VARCHAR(500),
          ListRelID NUMERIC(9)
        )


          

            INSERT  INTO #tempForm
                    SELECT  tintRow + 1 AS tintOrder,
                            vcDbColumnName,
                            ISNULL(vcCultureFieldName, vcFieldName),
                            vcAssociatedControlType,
                            vcListItemType,
                            numListID,
                            vcLookBackTableName,
                            bitCustom,
                            numFieldId,
                            bitAllowSorting,
                            bitAllowEdit,
                            bitIsRequired,
                            bitIsEmail,
                            bitIsAlphaNumeric,
                            bitIsNumeric,
                            bitIsLengthValidation,
                            intMaxLength,
                            intMinLength,
                            bitFieldMessage,
                            vcFieldMessage vcFieldMessage,
                            ListRelID
                    FROM    View_DynamicColumns
                    WHERE   numFormId = 21
                            AND numUserCntID = @numUserCntID
                            AND numDomainID = @numDomainID
                            AND tintPageType = 1
                            AND ISNULL(bitSettingField, 0) = 1
                            AND ISNULL(bitCustom, 0) = 0
                            AND ISNULL(numRelCntType,0)=0
                    UNION
                    SELECT  tintRow + 1 AS tintOrder,
                            vcDbColumnName,
                            vcFieldName,
                            vcAssociatedControlType,
                            '' AS vcListItemType,
                            numListID,
                            '',
                            bitCustom,
                            numFieldId,
                            bitAllowSorting,
                            bitAllowEdit,
                            bitIsRequired,
                            bitIsEmail,
                            bitIsAlphaNumeric,
                            bitIsNumeric,
                            bitIsLengthValidation,
                            intMaxLength,
                            intMinLength,
                            bitFieldMessage,
                            vcFieldMessage,
                            ListRelID
                    FROM    View_DynamicCustomColumns
                    WHERE   numFormId = 21
                            AND numUserCntID = @numUserCntID
                            AND numDomainID = @numDomainID
                            AND tintPageType = 1
                            AND ISNULL(bitCustom, 0) = 1
                            AND ISNULL(numRelCntType,0)=0
                    ORDER BY tintOrder ASC      
            
    
            
	IF @byteMode=0
	BEGIN
		
	IF @columnName = 'OnHand' 
        SET @columnName = 'numOnHand'
    ELSE IF @columnName = 'Backorder' 
        SET @columnName = 'numBackOrder'
    ELSE IF @columnName = 'OnOrder' 
        SET @columnName = 'numOnOrder'
    ELSE IF @columnName = 'OnAllocation' 
        SET @columnName = 'numAllocation'
    ELSE IF @columnName = 'Reorder' 
        SET @columnName = 'numReorder'
    ELSE IF @columnName = 'monStockValue' 
        SET @columnName = 'sum(numOnHand) * Isnull(monAverageCost,0)'
    ELSE IF @columnName = 'ItemType' 
        SET @columnName = 'charItemType'

  
    DECLARE @column AS VARCHAR(50)              
    SET @column = @columnName              
    DECLARE @join AS VARCHAR(400)                  
    SET @join = ''           
    IF @columnName LIKE '%Cust%' 
        BEGIN                
			DECLARE @CustomFieldType AS VARCHAR(10)
			DECLARE @fldId AS VARCHAR(10)
			
			SET @fldId = REPLACE(@columnName, 'Cust', '')
			SELECT TOP 1 @CustomFieldType = ISNULL(vcAssociatedControlType,'') FROM View_DynamicCustomColumns WHERE numFieldID = @fldId
            
            IF ISNULL(@CustomFieldType,'') = 'SelectBox' OR ISNULL(@CustomFieldType,'') = 'ListBox'
				BEGIN
					SET @join = @join + ' left Join CFW_FLD_Values_Item CFW on CFW.RecId=item.numItemCode and CFW.fld_id= ' + @fldId + ' ' 
					SET @join = @join + ' left Join ListDetails LstCF on LstCF.numListItemID = CFW.Fld_Value  '            
					SET @columnName = ' LstCF.vcData ' 	
				END
			ELSE
				BEGIN
					SET @join = ' left Join CFW_FLD_Values_Item CFW on CFW.RecId=item.numItemCode and CFW.fld_id= ' + @fldId + ' '                                                         
					SET @columnName = 'CFW.Fld_Value'                	
				END
        END                                                
           
                                      
    DECLARE @firstRec AS INTEGER                                        
    DECLARE @lastRec AS INTEGER                                        
    SET @firstRec = ( @CurrentPage - 1 ) * @PageSize                                        
    SET @lastRec = ( @CurrentPage * @PageSize + 1 )   

    DECLARE @bitLocation AS BIT  
    SELECT  @bitLocation = ( CASE WHEN COUNT(*) > 0 THEN 1
                                  ELSE 0
                             END )
    FROM    View_DynamicColumns
    WHERE   numFormId = 21
            AND numUserCntID = @numUserCntID
            AND numDomainID = @numDomainID
            AND tintPageType = 1
            AND bitCustom = 0
            AND ISNULL(bitSettingField, 0) = 1
            AND vcDbColumnName = 'vcWareHouse'
                                                                   
                                        
    DECLARE @strSql AS VARCHAR(8000)
    DECLARE @strWhere AS VARCHAR(8000)
    SET @strWhere = ' AND 1=1'
    
      SET @strSql = '
declare @numDomain numeric
set @numDomain = ' + CONVERT(VARCHAR(15), @numDomainID) + ';
    INSERT INTO #tempItemList select numItemCode'
  
--    SET @strSql = @strSql + ' from item                     
        
        
    IF @bitAssembly = '1' 
        SET @strWhere = @strWhere + ' and bitAssembly= ' + CONVERT(VARCHAR(2), @bitAssembly) + ''                                    
        
    ELSE 
        IF @IsKit = '1' 
            SET @strWhere = @strWhere + ' and (bitAssembly=0 or  bitAssembly is null) and bitKitParent= ' + CONVERT(VARCHAR(2), @IsKit) + ''               

    IF @ItemType = 'R' 
        SET @strWhere = @strWhere + ' and numItemClassification=(select numRentalItemClass from domain where numDomainId=' + CONVERT(VARCHAR(20), @numDomainID) + ')'            
    ELSE 
        IF @ItemType <> ''  
            SET @strWhere = @strWhere + ' and charItemType= ''' + @ItemType + ''''            

    IF @bitSerialized = 1 
        SET @strWhere = @strWhere + ' and (bitSerialized=1 or bitLotNo=1)'                                                   
        
    IF @SortChar <> '0' 
        SET @strWhere = @strWhere + ' and vcItemName like ''' + @SortChar + '%'''                                       
        
    IF @ItemClassification <> '0' 
        SET @strWhere = @strWhere + ' and numItemClassification=' + CONVERT(VARCHAR(15), @ItemClassification)    
    
    IF @IsArchive = 0 
        SET @strWhere = @strWhere + ' AND ISNULL(Item.IsArchieve,0) = 0'
        
    IF @KeyWord <> '' 
        BEGIN 
            IF CHARINDEX('vcCompanyName', @KeyWord) > 0 
                BEGIN
                    SET @strWhere = @strWhere + ' and item.numItemCode in (select Vendor.numItemCode from Vendor join 
											  divisionMaster div on div.numdivisionid=Vendor.numVendorid 
											  join companyInfo com  on com.numCompanyID=div.numCompanyID  
											  WHERE Vendor.numDomainID=' + CONVERT(VARCHAR(15), @numDomainID) + '  
											  and Vendor.numItemCode= item.numItemCode and com.' + @KeyWord + ')' 
											  
					SET @join = @join + ' LEFT JOIN divisionmaster div on numVendorid=div.numDivisionId '   		
                END
            ELSE 
                SET @strWhere = @strWhere + ' and ' + @KeyWord 
        END
        
    IF @numItemGroup > 0 
        SET @strWhere = @strWhere + ' and numItemGroup = ' + CONVERT(VARCHAR(20), @numItemGroup)    
               
    IF @numItemGroup = -1 
        SET @strWhere = @strWhere + ' and numItemGroup in (select numItemGroupID from ItemGroups where numDomainID=' + CONVERT(VARCHAR(20), @numDomainID) + ')'                                      
        
    SET @strWhere = @strWhere + ISNULL(@Where,'') + ' group by numItemCode '  
  
    IF @bitLocation = 1 
        SET @strWhere = @strWhere + '  ,vcWarehouse'
        
        SET @join = @join + ' LEFT JOIN WareHouseItems ON numItemID = numItemCode '
        SET @join = @join + ' LEFT JOIN Warehouses W ON W.numWareHouseID = WareHouseItems.numWareHouseID '
        
    IF @columnName <> 'sum(numOnHand) * Isnull(monAverageCost,0)' 
        BEGIN
            SET @strWhere = @strWhere + ',' + @columnName   
            
            IF CHARINDEX('LEFT JOIN WareHouseItems',@join) = 0
            BEGIN
				SET @join = @join + ' LEFT JOIN WareHouseItems ON numItemID = numItemCode '
			END	
        END
    
  
    SET @strSql = @strSql + ' FROM Item  ' + @join + '   
	LEFT JOIN ListDetails LD ON LD.numListItemID = Item.numShipClass
	LEFT JOIN ItemCategory IC ON IC.numItemID = Item.numItemCode
	WHERE Item.numDomainID= @numDomain
	AND ( WareHouseItems.numDomainID = ' + CONVERT(VARCHAR(15), @numDomainID) + ' OR WareHouseItems.numWareHouseItemID IS NULL) ' + @strWhere
    
    SET @strSql = @strSql + ' ORDER BY ' + @columnName + ' ' + @columnSortOrder 
 
	PRINT @strSql

	CREATE TABLE #tempItemList (RowNo INT IDENTITY(1,1) NOT NULL,numItemCode NUMERIC(18,0))
	 EXEC (@strSql)
	DELETE FROM #tempItemList WHERE RowNo NOT IN (SELECT MIN(RowNo) FROM #tempItemList GROUP BY numItemCode)

	CREATE TABLE #tempItemList1 (RunningCount INT IDENTITY(1,1) NOT NULL,numItemCode NUMERIC(18,0),TotalRowCount INT)
	INSERT INTO #tempItemList1 (numItemCode) SELECT numItemCode FROM #tempItemList
	UPDATE #tempItemList1 SET TotalRowCount=(SELECT COUNT(*) FROM #tempItemList1)
	
	SET @strSql = ' select                    
 min(RunningCount) as RunningCount,
 min(TotalRowCount) as TotalRowCount,I.numItemCode,I.vcItemName,I.txtItemDesc,I.charItemType,                                     
case when charItemType=''P'' then ''Inventory Item'' when charItemType=''N'' then ''Non Inventory Item'' when charItemType=''S'' then ''Service'' when charItemType=''A'' then ''Accessory'' end as ItemType,                                      
CASE WHEN bitKitParent = 1 THEN ISNULL(dbo.fn_GetKitInventory(I.numItemCode),0) ELSE isnull(sum(numOnHand),0) END  as numOnHand,                  
isnull(sum(numOnOrder),0) as numOnOrder,                                      
isnull(sum(numReorder),0) as numReorder,                  
isnull(sum(numBackOrder),0) as numBackOrder,                  
isnull(sum(numAllocation),0) as numAllocation,                  
vcCompanyName,  
CAST(ISNULL(monAverageCost,0) AS DECIMAL(10,2)) AS monAverageCost ,
CAST(ISNULL(monAverageCost,0) * SUM(numOnHand) AS DECIMAL(10,2)) AS monStockValue,
I.vcModelID,I.monListPrice,I.vcManufacturer,I.numBarCodeId,I.fltLength,I.fltWidth,I.fltHeight,I.fltWeight,I.vcSKU,LD.vcData AS numShipClass ' 

   IF @bitLocation = 1 
        SET @strSql = @strSql + '  ,vcWarehouse'  

SET @strSql = @strSql + ' INTO #tblItem FROM #tempItemList1 as tblItem JOIN ITEM I ON tblItem.numItemCode=I.numItemCode 
left join WareHouseItems on numItemID=I.numItemCode                                  
left join divisionmaster div  on I.numVendorid=div.numDivisionId                                    
left join companyInfo com on com.numCompanyid=div.numcompanyID   
left join Warehouses W  on W.numWareHouseID=WareHouseItems.numWareHouseID 
LEFT JOIN ListDetails LD ON LD.numListItemID = i.numShipClass
    where  RunningCount >' + CONVERT(VARCHAR(15), @firstRec)
        + ' and RunningCount < ' + CONVERT(VARCHAR(15), @lastRec)
          + ' group by I.numItemCode,vcItemName,txtItemDesc,charItemType,vcCompanyName,vcModelID,monListPrice,vcManufacturer,numBarCodeId,fltLength,fltWidth,fltHeight,fltWeight,monAverageCost,I.vcSKU,LD.vcData,bitKitParent '  
          
              IF @bitLocation = 1 
        SET @strSql = @strSql + '  ,vcWarehouse'  
        
        SET @strSql = @strSql + ' order by RunningCount '   
          
    DECLARE @tintOrder AS TINYINT                                                  
    DECLARE @vcFieldName AS VARCHAR(50)                                                  
    DECLARE @vcListItemType AS VARCHAR(3)                                             
    DECLARE @vcListItemType1 AS VARCHAR(1)                                                 
    DECLARE @vcAssociatedControlType VARCHAR(10)                                                  
    DECLARE @numListID AS NUMERIC(9)                                                  
    DECLARE @vcDbColumnName VARCHAR(20)                      
    DECLARE @WhereCondition VARCHAR(2000)                       
    DECLARE @vcLookBackTableName VARCHAR(2000)                
    DECLARE @bitCustom AS BIT                  
    DECLARE @numFieldId AS NUMERIC  
    DECLARE @bitAllowSorting AS CHAR(1)   
    DECLARE @bitAllowEdit AS CHAR(1)                   
                 
    SET @tintOrder = 0                                                  
    SET @WhereCondition = ''                 
                   
   
   
    SET @strSql = @strSql
        + ' select TotalRowCount,RunningCount,temp.numItemCode,vcItemName,txtItemDesc,charItemType,ItemType,numOnHand, LD.vcData AS numShipClass,                  
				   numOnOrder,numReorder,numBackOrder,numAllocation,vcCompanyName,vcModelID,monListPrice,vcManufacturer,numBarCodeId,fltLength,fltWidth,fltHeight,
				   fltWeight,(Select SUM(WO.numQtyItemsReq) from WorkOrder WO where WO.numItemCode=temp.numItemCode and WO.numWOStatus=0) as WorkOrder,
				   monAverageCost, monStockValue,vcSKU'     
  
    IF @bitLocation = 1 
        SET @strSql = @strSql + '  ,vcWarehouse'                                               

    DECLARE @ListRelID AS NUMERIC(9) 
  
    SELECT TOP 1
            @tintOrder = tintOrder + 1,
            @vcDbColumnName = vcDbColumnName,
            @vcFieldName = vcFieldName,
            @vcAssociatedControlType = vcAssociatedControlType,
            @vcListItemType = vcListItemType,
            @numListID = numListID,
            @vcLookBackTableName = vcLookBackTableName,
            @bitCustom = bitCustomField,
            @numFieldId = numFieldId,
            @bitAllowSorting = bitAllowSorting,
            @bitAllowEdit = bitAllowEdit,
            @ListRelID = ListRelID
    FROM    #tempForm --WHERE bitCustomField=1
    ORDER BY tintOrder ASC            

    WHILE @tintOrder > 0                                                  
        BEGIN                                                  
            IF @bitCustom = 0
               BEGIN
               PRINT @vcDbColumnName
			   		IF @vcDbColumnName = 'vcPathForTImage'
		   			BEGIN
					   	SET @strSql = @strSql + ',ISNULL(II.vcPathForTImage,'''') AS [vcPathForTImage]'                   
                        SET @WhereCondition = @WhereCondition + ' LEFT JOIN ItemImages II ON II.numItemCode = temp.numItemCode AND bitDefault = 1'
					END
			   END                              
			   
            ELSE IF @bitCustom = 1 
                BEGIN      
                  
                    SELECT  @vcFieldName = FLd_label,
                            @vcAssociatedControlType = fld_type,
                            @vcDbColumnName = 'Cust'
                            + CONVERT(VARCHAR(10), Fld_Id)
                    FROM    CFW_Fld_Master
                    WHERE   CFW_Fld_Master.Fld_Id = @numFieldId                 
     
              
--    print @vcAssociatedControlType                
                    IF @vcAssociatedControlType = 'TextBox'
                        OR @vcAssociatedControlType = 'TextArea' 
                        BEGIN                
                   
                            SET @strSql = @strSql + ',CFW'  + CONVERT(VARCHAR(3), @tintOrder)  + '.Fld_Value  [' + @vcFieldName + '~' + @vcDbColumnName + ']'                   
                            SET @WhereCondition = @WhereCondition + ' left Join CFW_FLD_Values_Item CFW' + CONVERT(VARCHAR(3), @tintOrder)
                                + '                 
    on CFW' + CONVERT(VARCHAR(3), @tintOrder) + '.Fld_Id='
                                + CONVERT(VARCHAR(10), @numFieldId)
                                + 'and CFW' + CONVERT(VARCHAR(3), @tintOrder)
                                + '.RecId=temp.numItemCode   '                                                         
                        END   
                    ELSE 
                        IF @vcAssociatedControlType = 'CheckBox' 
                            BEGIN            
               
                                SET @strSql = @strSql
                                    + ',case when isnull(CFW'
                                    + CONVERT(VARCHAR(3), @tintOrder)
                                    + '.Fld_Value,0)=0 then ''No'' when isnull(CFW'
                                    + CONVERT(VARCHAR(3), @tintOrder)
                                    + '.Fld_Value,0)=1 then ''Yes'' end   ['
                                    + @vcFieldName + '~' + @vcDbColumnName
                                    + ']'              
 
                                SET @WhereCondition = @WhereCondition
                                    + ' left Join CFW_FLD_Values_Item CFW'
                                    + CONVERT(VARCHAR(3), @tintOrder)
                                    + '             
    on CFW' + CONVERT(VARCHAR(3), @tintOrder) + '.Fld_Id='
                                    + CONVERT(VARCHAR(10), @numFieldId)
                                    + 'and CFW'
                                    + CONVERT(VARCHAR(3), @tintOrder)
                                    + '.RecId=temp.numItemCode   '                                                     
                            END                
                        ELSE 
                            IF @vcAssociatedControlType = 'DateField' 
                                BEGIN              
                   
                                    SET @strSql = @strSql
                                        + ',dbo.FormatedDateFromDate(CFW'
                                        + CONVERT(VARCHAR(3), @tintOrder)
                                        + '.Fld_Value,'
                                        + CONVERT(VARCHAR(10), @numDomainId)
                                        + ')  [' + @vcFieldName + '~'
                                        + @vcDbColumnName + ']'                   
                                    SET @WhereCondition = @WhereCondition
                                        + ' left Join CFW_FLD_Values_Item CFW'
                                        + CONVERT(VARCHAR(3), @tintOrder)
                                        + '                 
    on CFW' + CONVERT(VARCHAR(3), @tintOrder) + '.Fld_Id='
                                        + CONVERT(VARCHAR(10), @numFieldId)
                                        + 'and CFW'
                                        + CONVERT(VARCHAR(3), @tintOrder)
                                        + '.RecId=temp.numItemCode   '                                                         
                                END                
                            ELSE 
                                IF @vcAssociatedControlType = 'SelectBox' 
                                    BEGIN                
                                        SET @vcDbColumnName = 'Cust'
                                            + CONVERT(VARCHAR(10), @numFieldId)                
                                        SET @strSql = @strSql + ',L'
                                            + CONVERT(VARCHAR(3), @tintOrder)
                                            + '.vcData' + ' [' + @vcFieldName
                                            + '~' + @vcDbColumnName + ']'                                                          
                                        SET @WhereCondition = @WhereCondition
                                            + ' left Join CFW_FLD_Values_Item CFW'
                                            + CONVERT(VARCHAR(3), @tintOrder)
                                            + '                 
     on CFW' + CONVERT(VARCHAR(3), @tintOrder) + '.Fld_Id='
                                            + CONVERT(VARCHAR(10), @numFieldId)
                                            + 'and CFW'
                                            + CONVERT(VARCHAR(3), @tintOrder)
                                            + '.RecId=temp.numItemCode    '                                                         
                                        SET @WhereCondition = @WhereCondition
                                            + ' left Join ListDetails L'
                                            + CONVERT(VARCHAR(3), @tintOrder)
                                            + ' on L'
                                            + CONVERT(VARCHAR(3), @tintOrder)
                                            + '.numListItemID=CFW'
                                            + CONVERT(VARCHAR(3), @tintOrder)
                                            + '.Fld_Value'                
                                    END                 
                END          
  
            SELECT TOP 1
                    @tintOrder = tintOrder + 1,
                    @vcDbColumnName = vcDbColumnName,
                    @vcFieldName = vcFieldName,
                    @vcAssociatedControlType = vcAssociatedControlType,
                    @vcListItemType = vcListItemType,
                    @numListID = numListID,
                    @vcLookBackTableName = vcLookBackTableName,
                    @bitCustom = bitCustomField,
                    @numFieldId = numFieldId,
                    @bitAllowSorting = bitAllowSorting,
                    @bitAllowEdit = bitAllowEdit,
                    @ListRelID = ListRelID
            FROM    #tempForm
            WHERE   tintOrder > @tintOrder - 1 --AND bitCustomField=1
            ORDER BY tintOrder ASC            
 
            IF @@rowcount = 0 
                SET @tintOrder = 0 
            
        END                       
      
    PRINT @bitLocation
    SET @strSql = @strSql + ' from #tblItem temp
			LEFT JOIN ListDetails LD ON LD.vcData = temp.numShipClass AND LD.numListID=461 and LD.numDomainID = '  + CONVERT(VARCHAR(20), @numDomainID) + ' '
		+ @WhereCondition
        + ' where  RunningCount >' + CONVERT(VARCHAR(15), @firstRec)
        + ' and RunningCount < ' + CONVERT(VARCHAR(15), @lastRec)
        + ' order by RunningCount'   

    SET @strSql = REPLACE(@strSql, '|', ',') 
    PRINT @strSql
    EXEC ( @strSql)  

    
                              
 
DROP TABLE #tempItemList
DROP TABLE #tempItemList1

END

UPDATE  #tempForm
    SET     vcDbColumnName = CASE WHEN bitCustomField = 1
                                  THEN vcFieldName + '~' + vcDbColumnName
                                  ELSE vcDbColumnName END
                                  
   SELECT  * FROM #tempForm

    DROP TABLE #tempForm
/****** Object:  StoredProcedure [dbo].[USP_OPPBizDocItems]    Script Date: 07/26/2008 16:20:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj                                                                                                                                                        
-- exec USP_OPPBizDocItems @numOppId=12388,@numOppBizDocsId=11988,@numDomainID=110
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   xtype = 'p'
                    AND NAME = 'usp_oppbizdocitems' ) 
    DROP PROCEDURE usp_oppbizdocitems
GO
CREATE PROCEDURE [dbo].[USP_OPPBizDocItems]
    (
      @numOppId AS NUMERIC(9) = NULL,
      @numOppBizDocsId AS NUMERIC(9) = NULL,
      @numDomainID AS NUMERIC(9) = 0      
    )
AS 
    DECLARE @DivisionID AS NUMERIC(9)      
    DECLARE @tintTaxOperator AS TINYINT   
                                                                                                                                                                                                                                                                                                                                          
    SELECT  @DivisionID = numDivisionID, @tintTaxOperator = [tintTaxOperator]           
    FROM    OpportunityMaster
    WHERE   numOppId = @numOppId                                                                                                                                                                          
--Decimal points to show in bizdoc 
    DECLARE @DecimalPoint AS TINYINT
    SELECT  @DecimalPoint = tintDecimalPoints
    FROM    domain
    WHERE   numDomainID = @numDomainID
                                                                                          
    DECLARE @strSQL AS VARCHAR(MAX)                                                                                          
    DECLARE @strSQL1 AS VARCHAR(8000)                                       
    DECLARE @strSQLCusFields VARCHAR(1000)                      
    DECLARE @strSQLEmpFlds VARCHAR(500)                                                                         
    SET @strSQLCusFields = ''                                                                                          
    SET @strSQLEmpFlds = ''                                                                                          
    DECLARE @tintType AS TINYINT   
    DECLARE @tintOppType AS TINYINT                                                      
    DECLARE @intRowNum AS INT                                                  
    DECLARE @numFldID AS VARCHAR(15)                                          
    DECLARE @vcFldname AS VARCHAR(50)                                            
    DECLARE @numBizDocId AS NUMERIC(9)
    DECLARE @numBizDocTempID AS NUMERIC(9) ;
        SET @numBizDocTempID = 0
                                           
    SELECT  @numBizDocTempID = ISNULL(numBizDocTempID, 0),
            @numBizDocId = numBizDocId
    FROM    OpportunityBizDocs
    WHERE   numOppBizDocsId = @numOppBizDocsId
    SELECT  @tintType = tintOppType,
            @tintOppType = tintOppType
    FROM    OpportunityMaster
    WHERE   numOppID = @numOppId                                                                                          
                                                                                   
    IF @tintType = 1 
        SET @tintType = 7
    ELSE 
        SET @tintType = 8                                                                                      
                                                                                
                                                                                        
                                                                                          
 

    SELECT  *
    INTO    #Temp1
    FROM    ( SELECT   Opp.vcitemname AS vcItemName,
                        ISNULL(OBD.numOppBizDocItemID, 0) OppBizDocItemID,
                        CASE WHEN charitemType = 'P' THEN 'Product'
                             WHEN charitemType = 'S' THEN 'Service'
                        END AS charitemType,
                        CASE WHEN charitemType = 'P' THEN 'Inventory'
	                         WHEN charitemType = 'N' THEN 'Non-Inventory'
                             WHEN charitemType = 'S' THEN 'Service'
                        END AS vcItemType,
                        CASE WHEN bitShowDeptItemDesc = 0
                             THEN CONVERT(VARCHAR(2500), OBD.vcitemDesc)
                             WHEN bitShowDeptItemDesc = 1
                                  AND bitKitParent = 1
                                  AND bitAssembly = 0
                             THEN dbo.fn_PopulateKitDesc(opp.numOppId,
                                                         opp.numoppitemtCode)
                             WHEN bitShowDeptItemDesc = 1
                                  AND bitKitParent = 1
                                  AND bitAssembly = 1
                             THEN dbo.fn_PopulateAssemblyDesc(opp.numoppitemtCode, OBD.numUnitHour)
                             ELSE CONVERT(VARCHAR(2500), OBD.vcitemDesc)
                        END AS txtItemDesc,                                      
--vcitemdesc as [desc],                                      
                        dbo.fn_UOMConversion(ISNULL(i.numBaseUnit, 0),
                                             i.numItemCode, @numDomainID,
                                             ISNULL(opp.numUOMId, 0))
                        * OBD.numUnitHour AS numUnitHour,
                        CONVERT(DECIMAL(18, 4), dbo.fn_UOMConversion(ISNULL(opp.numUOMId, 0), i.numItemCode, @numDomainID, ISNULL(i.numBaseUnit, 0))
                        * OBD.monPrice) monPrice,
                        CONVERT(VARCHAR, CONVERT(DECIMAL(18, 4), OBD.monTotAmount)) Amount,
                        OBD.monTotAmount/*Fo calculating sum*/,
--                        CASE WHEN @tintOppType = 1
--                             THEN CASE WHEN bitTaxable = 0 THEN 0
--                                       WHEN bitTaxable = 1
--                                       THEN ( SELECT    fltPercentage
--                                              FROM      OpportunityBizDocTaxItems
--                                              WHERE     numOppBizDocID = @numOppBizDocsId
--                                                        AND numTaxItemID = 0
--                                            )
--                                  END
--                             ELSE 0
--                        END AS Tax,
                        ISNULL(CONVERT(VARCHAR, i.monListPrice), 0) AS monListPrice,
                        i.numItemCode AS ItemCode,
                        opp.numoppitemtCode,
                        L.vcdata AS numItemClassification,
                        CASE WHEN bitTaxable = 0 THEN 'No'
                             ELSE 'Yes'
                        END AS Taxable,
                        numIncomeChartAcntId AS itemIncomeAccount,
                        'NI' AS ItemType,
                        ISNULL(GJH.numJournal_Id, 0) AS numJournalId,
                        ( SELECT TOP 1
                                    ISNULL(GJD.numTransactionId, 0)
                          FROM      General_Journal_Details GJD
                          WHERE     GJH.numJournal_Id = GJD.numJournalId
                                    AND GJD.chBizDocItems = 'NI'
                                    AND GJD.numoppitemtCode = opp.numoppitemtCode
                        ) AS numTransactionId,
                        ISNULL(Opp.vcModelID, '') vcModelID,
                        dbo.USP_GetAttributes(OBD.numWarehouseItmsID,
                                              bitSerialized) AS vcAttributes,
                        ISNULL(vcPartNo, '') AS vcPartNo,
                        ( ISNULL(OBD.monTotAmtBefDiscount, OBD.monTotAmount)
                          - OBD.monTotAmount ) AS DiscAmt,
                        OBD.vcNotes,
                        OBD.vcTrackingNo,
                        OBD.vcShippingMethod,
                        OBD.monShipCost,
                        [dbo].[FormatedDateFromDate](OBD.dtDeliveryDate,
                                                     @numDomainID) dtDeliveryDate,
                        Opp.numWarehouseItmsID,
--ISNULL(I.vcUnitofMeasure,'Units') vcUnitofMeasure,
                        ISNULL(u.vcUnitName, '') numUOMId,
                        ISNULL(Opp.vcManufacturer, '') AS vcManufacturer,
                        ISNULL(V.monCost, 0) AS VendorCost,
                        Opp.vcPathForTImage,
                        i.[fltLength],
                        i.[fltWidth],
                        i.[fltHeight],
                        i.[fltWeight],
                        ISNULL(I.numVendorID, 0) numVendorID,
                        Opp.bitDropShip AS DropShip,
                        SUBSTRING(( SELECT  ' ,' + vcSerialNo
                                            + CASE WHEN ISNULL(I.bitLotNo, 0) = 1
                                                   THEN ' ('
                                                        + CONVERT(VARCHAR(15), oppI.numQty)
                                                        + ')'
                                                   ELSE ''
                                              END
                                    FROM    OppWarehouseSerializedItem oppI
                                            JOIN WareHouseItmsDTL whi ON oppI.numWareHouseItmsDTLID = whi.numWareHouseItmsDTLID
                                    WHERE   oppI.numOppID = Opp.numOppId
                                            AND oppI.numOppItemID = Opp.numoppitemtCode
                                            AND 1=(CASE WHEN @tintOppType=1 then CASE WHEN oppI.numOppBizDocsID=OBD.numOppBizDocID THEN 1 ELSE 0 END
														ELSE 1 END)
                                    ORDER BY vcSerialNo
                                  FOR
                                    XML PATH('')
                                  ), 3, 200000) AS SerialLotNo,
                        ISNULL(opp.numUOMId, 0) AS numUOM,
                        ISNULL(u.vcUnitName, '') vcUOMName,
                        dbo.fn_UOMConversion(opp.numUOMId, Opp.numItemCode,
                                             @numDomainID, NULL) AS UOMConversionFactor,
                        ISNULL(Opp.numSOVendorId, 0) AS numSOVendorId,
                        [dbo].FormatedDateTimeFromDate(OBD.dtRentalStartDate,
                                                       @numDomainID) dtRentalStartDate,
                        [dbo].FormatedDateTimeFromDate(OBD.dtRentalReturnDate,
                                                       @numDomainID) dtRentalReturnDate,
-- Added for packing slip
                        ISNULL(W.vcWareHouse, '') AS Warehouse,
                        CASE WHEN charitemType = 'P' THEN WI.vcWHSKU
                             ELSE i.vcSKU
                        END vcSKU,
                        CASE WHEN charitemType = 'P' THEN WI.vcBarCode
                             ELSE i.numBarCodeId
                        END vcBarcode,
                        ISNULL(opp.numClassID, 0) AS numClassID,
                        ISNULL(opp.numProjectID, 0) AS numProjectID,
                        i.numShipClass,OBD.numOppBizDocID,WL.vcLocation,ISNULL(bitSerialized,0) AS bitSerialized,ISNULL(bitLotNo,0) AS bitLotNo,
                        opp.numOppId,OBD.numUnitHour AS numUnitHourOrig,ISNULL(I.numItemCode,0) AS [numItemCode]
              FROM      OpportunityItems opp
                        JOIN OpportunityBizDocItems OBD ON OBD.numOppItemID = opp.numoppitemtCode
                        LEFT JOIN item i ON opp.numItemCode = i.numItemCode
                        LEFT JOIN ListDetails L ON i.numItemClassification = L.numListItemID
                        LEFT JOIN Vendor V ON V.numVendorID = @DivisionID
                                              AND V.numItemCode = opp.numItemCode
                        LEFT JOIN General_Journal_Header GJH ON opp.numoppid = GJH.numOppId
                                                                AND GJH.numBizDocsPaymentDetId IS NULL
                                                                AND GJH.numOppBizDocsId = @numOppBizDocsId
                        LEFT JOIN UOM u ON u.numUOMId = opp.numUOMId                                    
-- left join General_Journal_Details GJD on GJH.numJournal_Id=GJD.numJournalId And GJD.chBizDocItems='NI' And GJD.numoppitemtCode=opp.numoppitemtCode                                       
                        LEFT JOIN dbo.WareHouseItems WI ON opp.numWarehouseItmsID = WI.numWareHouseItemID
                                                           AND WI.numDomainID = @numDomainID
                        LEFT JOIN dbo.Warehouses W ON WI.numDomainID = W.numDomainID
                                                      AND WI.numWareHouseID = W.numWareHouseID
                        LEFT JOIN dbo.WarehouseLocation WL ON WL.numWLocationID = WI.numWLocationID
              WHERE     opp.numOppId = @numOppId
                        AND ( bitShowDeptItem IS NULL
                              OR bitShowDeptItem = 0
                            )
                        AND OBD.numOppBizDocID = @numOppBizDocsId
       
--For Kit Items where following condition is true( Show dependant items as line items on BizDoc (instead of kit item name))
-- Commented by chintan, Reason: Bug #1829 
/*
union
select Opp.vcitemname as Item,                                      
case when i.charitemType='P' then 'Product' when i.charitemType='S' then 'Service' end as type,                                      
convert(varchar(500),OBD.vcitemDesc) as [Desc],                                      
--vcitemdesc as [desc],                                      
OBD.numUnitHour*intQuantity as Unit,
CONVERT(VARCHAR,CONVERT(DECIMAL(18, 4), OBD.monPrice)) Price,
CONVERT(VARCHAR,CONVERT(DECIMAL(18, 4), OBD.monTotAmount)) Amount,
OBD.monTotAmount/*Fo calculating sum*/,
case when @tintOppType=1 then case when i.bitTaxable =0 then 0 when i.bitTaxable=1 then (select fltPercentage from OpportunityBizDocTaxItems where numOppBizDocID=@numOppBizDocsId and numTaxItemID=0) end else 0 end  as Tax,                                    
isnull(convert(varchar,i.monListPrice),0) as monListPrice,i.numItemCode as ItemCode,opp.numoppitemtCode,                                      
L.vcdata as vcItemClassification,case when i.bitTaxable=0 then 'No' else 'Yes' end as Taxable,i.numIncomeChartAcntId as itemIncomeAccount,                                      
'NI' as ItemType                                      
,isnull(GJH.numJournal_Id,0) as numJournalId,(SELECT TOP 1 isnull(GJD.numTransactionId, 0) FROM General_Journal_Details GJD WHERE GJH.numJournal_Id = GJD.numJournalId And GJD.chBizDocItems = 'NI' And GJD.numoppitemtCode = opp.numoppitemtCode) as numTransactionId,isnull(Opp.vcModelID,'') vcModelID, dbo.USP_GetAttributes(OBD.numWarehouseItmsID,i.bitSerialized) as Attributes,isnull(vcPartNo,'') as Part 
,(isnull(OBD.monTotAmtBefDiscount,OBD.monTotAmount)-OBD.monTotAmount) as DiscAmt,OBD.vcNotes,OBD.vcTrackingNo,OBD.vcShippingMethod,OBD.monShipCost,[dbo].[FormatedDateFromDate](OBD.dtDeliveryDate,@numDomainID) dtDeliveryDate,Opp.numWarehouseItmsID,
--ISNULL(I.vcUnitofMeasure,'Units') vcUnitofMeasure,
ISNULL(u.vcUnitName,'') vcUnitofMeasure,
isnull(i2.vcManufacturer,'') as vcManufacturer,
isnull(V.monCost,0) as VendorCost,
Opp.vcPathForTImage,i.[fltLength],i.[fltWidth],i.[fltHeight],i.[fltWeight],isnull(I.numVendorID,0) numVendorID,Opp.bitDropShip AS DropShip,
SUBSTRING(
(SELECT ',' + vcSerialNo + CASE WHEN isnull(I.bitLotNo,0)=1 THEN ' (' + CONVERT(VARCHAR(15),oppI.numQty) + ')' ELSE '' end 
FROM OppWarehouseSerializedItem oppI JOIN WareHouseItmsDTL whi ON oppI.numWareHouseItmsDTLID=whi.numWareHouseItmsDTLID where oppI.numOppID=Opp.numOppId and oppI.numOppItemID=Opp.numoppitemtCode 
ORDER BY vcSerialNo FOR XML PATH('')),2,200000) AS SerialLotNo,isnull(opp.numUOMId,0) AS numUOM,ISNULL(u.vcUnitName,'') vcUOMName,dbo.fn_UOMConversion(opp.numUOMId,Opp.numItemCode,@numDomainID,null) AS UOMConversionFactor,
ISNULL(Opp.numSOVendorId,0) as numSOVendorId,
[dbo].FormatedDateTimeFromDate(OBD.dtRentalStartDate,@numDomainID) dtRentalStartDate,
[dbo].FormatedDateTimeFromDate(OBD.dtRentalReturnDate,@numDomainID) dtRentalReturnDate
from OpportunityItems opp       
join OpportunityBizDocItems OBD on OBD.numOppItemID= opp.numoppitemtCode                                   
join  OpportunityKitItems OppKitItems      
on OppKitItems.numOppKitItem=opp.numoppitemtCode                                     
join item i on OppKitItems.numChildItem=i.numItemCode      
join item i2 on opp.numItemCode=i2.numItemCode                                      
left join ListDetails L on i.numItemClassification=L.numListItemID
left join Vendor V on V.numVendorID=@DivisionID and V.numItemCode=opp.numItemCode                                       
left join General_Journal_Header GJH on opp.numoppid=GJH.numOppId And GJH.numBizDocsPaymentDetId is null And GJH.numOppBizDocsId=@numOppBizDocsId                                      
-- left join General_Journal_Details GJD on GJH.numJournal_Id=GJD.numJournalId And GJD.chBizDocItems='NI' And GJD.numoppitemtCode=opp.numoppitemtCode
LEFT JOIN  UOM u ON u.numUOMId = opp.numUOMId                                    
where opp.numOppId=@numOppId and i2.bitShowDeptItem=1      and   OBD.numOppBizDocID=@numOppBizDocsId   */
--union
--select case when numcategory=1 then                             
--case when isnull(te.numcontractid,0) = 0 then 'Time' else 'Time(Contract)' end                              
--when numcategory=2 then                             
--case when isnull(te.numcontractid,0) = 0 then 'Expense' else 'Expense(Contract)' end                              
--end  as item,                           
--case when numcategory=1 then 'Time' when numcategory=2 then 'Expense' end                                      
-- as Type,                                      
--convert(varchar(100),txtDesc) as [Desc],                                      
----txtDesc as [Desc],                                      
--convert(decimal(18,2),datediff(minute,te.dtfromdate,te.dttodate))/60 as unit,                                      
--convert(varchar(100),monamount) as Price,                                      
--case when isnull(te.numcontractid,0) = 0                    
--then                    
-- case when numCategory =1  then                    
--   isnull(convert(decimal(18,2),datediff(minute,dtfromdate,dttodate))*monAmount/60,0)                    
--   when numCategory =2  then                    
--   isnull(monamount,0)                    
--                     
--   end                    
--else                                  
--  0                    
--end as amount,                                      
--0.00 as Tax,'' as listPrice,0 as ItemCode,0 as numoppitemtCode,'' as vcItemClassification,                                      
--'No' as Taxable,0 as itemIncomeAccount,'TE' as ItemType                                      
--,0 as numJournalId,0 as numTransactionId ,'' vcModelID,''as Attributes,'' as Part,0 as DiscAmt ,'' as vcNotes,'' as vcTrackingNo                                      
--from timeandexpense TE                            
--                                    
--where                                       
--(numtype= 1)   and   numDomainID=@numDomainID and                                    
--(numOppBizDocsId = @numOppBizDocsId  or numoppid = @numOppId)
            ) X


    IF @DecimalPoint = 1 
        BEGIN
            UPDATE  #Temp1
            SET     monPrice = CONVERT(DECIMAL(18, 1), monPrice),
                    Amount = CONVERT(DECIMAL(18, 1), Amount)
        END
    IF @DecimalPoint = 2 
        BEGIN
            UPDATE  #Temp1
            SET     monPrice = CONVERT(DECIMAL(18, 2), monPrice),
                    Amount = CONVERT(DECIMAL(18, 2), Amount)
        END
    IF @DecimalPoint = 3 
        BEGIN
            UPDATE  #Temp1
            SET     monPrice = CONVERT(DECIMAL(18, 3), monPrice),
                    Amount = CONVERT(DECIMAL(18, 3), Amount)
        END
    IF @DecimalPoint = 4 
        BEGIN
            UPDATE  #Temp1
            SET     monPrice = CONVERT(DECIMAL(18, 4), monPrice),
                    Amount = CONVERT(DECIMAL(18, 4), Amount)
        END

    DECLARE @strSQLUpdate AS VARCHAR(2000);SET @strSQLUpdate=''
   
    EXEC ( 'alter table #Temp1 add [TotalTax] money'
        )

    IF @tintOppType = 1 AND @tintTaxOperator = 1 --add Sales tax
	--set @strSQLUpdate=@strSQLUpdate +',[TotalTax]= dbo.fn_CalSalesTaxAmt('+ convert(varchar(20),@numOppBizDocsId)+','+ convert(varchar(20),@numDomainID)+')*Amount/100'
        SET @strSQLUpdate = @strSQLUpdate
            + ',[TotalTax]= dbo.fn_CalBizDocTaxAmt('
            + CONVERT(VARCHAR(20), @numDomainID) + ',0,'
            + CONVERT(VARCHAR(20), @numOppId) +','+
            + 'numoppitemtCode,1)*Amount/100'
    ELSE IF @tintOppType = 1 AND @tintTaxOperator = 2 -- remove sales tax
            SET @strSQLUpdate = @strSQLUpdate + ',[TotalTax]= 0'
    ELSE IF @tintOppType = 1 
                SET @strSQLUpdate = @strSQLUpdate
                    + ',[TotalTax]= dbo.fn_CalBizDocTaxAmt('
                    + CONVERT(VARCHAR(20), @numDomainID) + ',0,'
                    + CONVERT(VARCHAR(20), @numOppId) +','+
                    + 'numoppitemtCode,1)*Amount/100'
    ELSE 
                SET @strSQLUpdate = @strSQLUpdate + ',[TotalTax]= 0'


 DECLARE @vcTaxName AS VARCHAR(100)
    DECLARE @numTaxItemID AS NUMERIC(9)
    SET @numTaxItemID = 0
    SELECT TOP 1
            @vcTaxName = vcTaxName,
            @numTaxItemID = numTaxItemID
    FROM    TaxItems
    WHERE   numDomainID = @numDomainID


    WHILE @numTaxItemID > 0
        BEGIN

            EXEC ( 'alter table #Temp1 add [' + @vcTaxName + '] money'
                )

            IF @tintOppType = 1 
                SET @strSQLUpdate = @strSQLUpdate + ',[' + @vcTaxName
                    + ']= dbo.fn_CalBizDocTaxAmt('
                    + CONVERT(VARCHAR(20), @numDomainID) + ','
                    + CONVERT(VARCHAR(20), @numTaxItemID) + ','
                    + CONVERT(VARCHAR(20), @numOppId) +','+
                    + 'numoppitemtCode,1)*Amount/100'
            ELSE 
                SET @strSQLUpdate = @strSQLUpdate + ',[' + @vcTaxName + ']= 0'

            SELECT TOP 1
                    @vcTaxName = vcTaxName,
                    @numTaxItemID = numTaxItemID
            FROM    TaxItems
            WHERE   numDomainID = @numDomainID
                    AND numTaxItemID > @numTaxItemID

            IF @@rowcount = 0 
                SET @numTaxItemID = 0


        END

    IF @strSQLUpdate <> '' 
        BEGIN
            SET @strSQLUpdate = 'update #Temp1 set ItemCode=ItemCode'
                + @strSQLUpdate + ' where ItemCode>0'
            PRINT @strSQLUpdate
            EXEC ( @strSQLUpdate
                )
        END

    DECLARE @vcDbColumnName NVARCHAR(50)
    SELECT TOP 1
            @numFldID = numfieldID,
            @vcFldname = vcFieldName,
            @vcDbColumnName = vcDbColumnName,
            @intRowNum = ( tintRow + 1 )
    FROM    View_DynamicCustomColumns
    WHERE   numFormID = @tintType
            AND Grp_id = 5
            AND numDomainID = @numDomainID
            AND numAuthGroupID = @numBizDocId
            AND bitCustom = 1
            AND ISNULL(numRelCntType, 0) = @numBizDocTempID
    ORDER BY tintRow                                                                                          
    WHILE @intRowNum > 0                                                                                  
        BEGIN    
            EXEC
                ( 'alter table #Temp1 add [' + @vcDbColumnName
                  + '] varchar(100)'
                )
	
            SET @strSQLUpdate = 'update #Temp1 set ItemCode=ItemCode,['
                + @vcDbColumnName + ']=dbo.GetCustFldValueItem(' + @numFldID
                + ',ItemCode) where ItemCode>0'
            EXEC ( @strSQLUpdate
                )
		                   
            SELECT TOP 1
                    @numFldID = numfieldID,
                    @vcFldname = vcFieldName,
                    @vcDbColumnName = vcDbColumnName,
                    @intRowNum = ( tintRow + 1 )
            FROM    View_DynamicCustomColumns
            WHERE   numFormID = @tintType
                    AND Grp_id = 5
                    AND numDomainID = @numDomainID
                    AND numAuthGroupID = @numBizDocId
                    AND bitCustom = 1
                    AND tintRow >= @intRowNum
                    AND ISNULL(numRelCntType, 0) = @numBizDocTempID
            ORDER BY tintRow                                                                  
		           
            IF @@rowcount = 0 
                SET @intRowNum = 0                                                                                          
        END


    SELECT  Row_NUmber() OVER ( ORDER BY numoppitemtCode ) RowNumber,
            *
    FROM    #Temp1
    ORDER BY numoppitemtCode ASC 

    DROP TABLE #Temp1

    SELECT  ISNULL(tintOppStatus, 0)
    FROM    OpportunityMaster
    WHERE   numOppID = @numOppId                                                                                                 


IF (SELECT COUNT(*) FROM View_DynamicColumns WHERE   numFormID = @tintType AND numDomainID = @numDomainID
            AND numAuthGroupID = @numBizDocId AND vcFieldType = 'R'AND ISNULL(numRelCntType, 0) = @numBizDocTempID)=0
BEGIN            
	INSERT INTO DycFormConfigurationDetails (numFormID,numFieldID,intColumnNum,intRowNum,numDomainID,numAuthGroupID,numRelCntType)
	select numFormId,numFieldId,1,tintRow,@numDomainID,@numBizDocId,@numBizDocTempID
	 FROM View_DynamicDefaultColumns
	 where numFormId=@tintType and bitDefault=1 and numDomainID=@numDomainID order by tintRow ASC	
	 
	 
	 IF(SELECT COUNT(*) FROM DycFrmConfigBizDocsSumm Ds where numBizDocID=@numBizDocId and Ds.numFormID=@tintType 
	and isnull(Ds.numBizDocTempID,0)=@numBizDocTempID and Ds.numDomainID=@numDomainID)=0
	BEGIN
		INSERT INTO DycFrmConfigBizDocsSumm (numFormID,numBizDocID,numFieldID,numDomainID,numBizDocTempID)
		 SELECT @tintType,@numBizDocId,DFM.numFieldID,@numDomainID,@numBizDocTempID FROM
		 DycFormField_Mapping DFFM 
			JOIN DycFieldMaster DFM on DFFM.numModuleID=DFFM.numModuleID and DFFM.numFieldID=DFM.numFieldID
			where numFormID=16 and DFFM.numDomainID is null
	END

END      


    SELECT  vcFieldName AS vcFormFieldName,
            vcDbColumnName,
            vcFieldDataType,
            tintRow AS intRowNum
    FROM    View_DynamicColumns
    WHERE   numFormID = @tintType
            AND numDomainID = @numDomainID
            AND numAuthGroupID = @numBizDocId
            AND vcFieldType = 'R'
            AND ISNULL(numRelCntType, 0) = @numBizDocTempID
    UNION
    SELECT  vcFieldName AS vcFormFieldName,
            vcDbColumnName,
            'C' vcFieldDataType,
            tintRow AS intRowNum
    FROM    View_DynamicCustomColumns
    WHERE   numFormID = @tintType
            AND Grp_id = 5
            AND numDomainID = @numDomainID
            AND numAuthGroupID = @numBizDocId
            AND bitCustom = 1
            AND ISNULL(numRelCntType, 0) = @numBizDocTempID
    ORDER BY tintRow
GO
/****** Object:  StoredProcedure [dbo].[USP_OPPDetails]    Script Date: 03/25/2009 15:10:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- Created By Anoop Jayaraj
-- [dbo].[USP_OPPDetails] 1362,1,-333
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_oppdetails')
DROP PROCEDURE usp_oppdetails
GO
CREATE PROCEDURE [dbo].[USP_OPPDetails]
(
               @numOppID             AS NUMERIC(9)  = NULL,
               @numDomainID          AS NUMERIC(9)  = 0,
               @ClientTimeZoneOffset AS INT)
AS
	
  SELECT Opp.numoppid,
         numCampainID,
         ADC.vcFirstname + ' ' + ADC.vcLastName AS numContactId,
         isnull(ADC.vcEmail,'') AS vcEmail,
         Opp.txtComments,
         Opp.numDivisionID,
         tintOppType,
         ISNULL(dateadd(MINUTE,-@ClientTimeZoneOffset,bintAccountClosingDate),'') AS bintAccountClosingDate,
         Opp.numContactID AS ContactID,
         tintSource,
         C2.vcCompanyName + Case when isnull(D2.numCompanyDiff,0)>0 then  '  ' + dbo.fn_getlistitemname(D2.numCompanyDiff) + ':' + isnull(D2.vcCompanyDiff,'') else '' end as vcCompanyname,
         D2.tintCRMType,
         Opp.vcPoppName,
         intpEstimatedCloseDate,
         [dbo].[GetDealAmount](@numOppID,getutcdate(),0) AS monPAmount,
         lngPConclAnalysis,
         monPAmount AS OppAmount,
         numSalesOrPurType,
         Opp.tintActive,
         dbo.fn_GetContactName(Opp.numCreatedby)
           + ' '
           + CONVERT(VARCHAR(20),dateadd(MINUTE,-@ClientTimeZoneOffset,Opp.bintCreatedDate)) AS CreatedBy,
         dbo.fn_GetContactName(Opp.numModifiedBy)
           + ' '
           + CONVERT(VARCHAR(20),dateadd(MINUTE,-@ClientTimeZoneOffset,Opp.bintModifiedDate)) AS ModifiedBy,
         dbo.fn_GetContactName(Opp.numRecOwner) AS RecordOwner,
         Opp.numRecOwner,
         isnull(tintshipped,0) AS tintshipped,
         isnull(tintOppStatus,0) AS tintOppStatus,
         dbo.OpportunityLinkedItems(@numOppID) AS NoOfProjects,
         Opp.numAssignedTo,
         Opp.numAssignedBy,
         (SELECT COUNT(* )
          FROM   dbo.GenericDocuments
          WHERE  numRecID = @numOppID
                 AND vcDocumentSection = 'O') AS DocumentCount,
         isnull(OPR.numRecurringId,0) AS numRecurringId,
		 OPR.dtRecurringDate AS dtLastRecurringDate,
         D2.numTerID,
         Link.numOppID AS numParentOppID,
         Link.vcPoppName AS ParentOppName,
         Link.vcSource AS Source,
         Link.numParentProjectID,
         Link.vcProjectName,
         ISNULL(C.varCurrSymbol,'') varCurrSymbol,
         ISNULL(Opp.fltExchangeRate,0) AS [fltExchangeRate],
         ISNULL(C.chrCurrency,'') AS [chrCurrency],
         ISNULL(C.numCurrencyID,0) AS [numCurrencyID],
		 (Select Count(*) from OpportunityBizDocs 
		 where numOppId=@numOppID and bitAuthoritativeBizDocs=1) As AuthBizDocCount,
		 (SELECT COUNT(distinct numChildOppID) FROM [OpportunityLinking] OL WHERE OL.[numParentOppID] = Opp.numOppId) AS ChildCount,
		 (SELECT COUNT(*) FROM [RecurringTransactionReport] RTR WHERE RTR.[numRecTranSeedId] = Opp.numOppId)  AS RecurringChildCount
		 ,Opp.bintCreatedDate,
		 ISNULL(Opp.bitStockTransfer ,0) bitStockTransfer,	ISNULL(Opp.tintTaxOperator,0) AS tintTaxOperator, 
         isnull(Opp.intBillingDays,0) AS numBillingDays,isnull(Opp.bitInterestType,0) AS bitInterestType,
         isnull(opp.fltInterest,0) AS fltInterest,  isnull(Opp.fltDiscount,0) AS fltDiscount,
         isnull(Opp.bitDiscountType,0) AS bitDiscountType,ISNULL(Opp.bitBillingTerms,0) AS bitBillingTerms,ISNULL(OPP.numDiscountAcntType,0) AS numDiscountAcntType,
         (SELECT TOP 1 numCountry FROM fn_getOPPAddressDetails(Opp.numOppId,Opp.numDomainID,2)) AS [numShipCountry],
		 (SELECT TOP 1 numState FROM fn_getOPPAddressDetails(Opp.numOppId,Opp.numDomainID,2)) AS [numShipState],
		 (SELECT TOP 1 vcState FROM fn_getOPPAddressDetails(Opp.numOppId,Opp.numDomainID,2)) AS [vcShipState],
		 (SELECT TOP 1 vcCountry FROM fn_getOPPAddressDetails(Opp.numOppId,Opp.numDomainID,2)) AS [vcShipCountry],
		 (SELECT TOP 1 vcPostalCode FROM fn_getOPPAddressDetails(Opp.numOppId,Opp.numDomainID,2)) AS [vcPostalCode],
		 ISNULL(vcCouponCode,'') AS [vcCouponCode],ISNULL(Opp.bitPPVariance,0) AS bitPPVariance,
		 Opp.dtItemReceivedDate,ISNULL(bitUseShippersAccountNo,0) [bitUseShippersAccountNo], ISNULL(vcShippersAccountNo,'') [vcShippersAccountNo],
		 ISNULL(bitUseMarkupShippingRate,0) [bitUseMarkupShippingRate],
		 ISNULL(numMarkupShippingRate,0) [numMarkupShippingRate],
		 ISNULL(intUsedShippingCompany,0) [intUsedShippingCompany]
  FROM   OpportunityMaster Opp 
         JOIN divisionMaster D2
           ON Opp.numDivisionID = D2.numDivisionID
         LEFT JOIN CompanyInfo C2
           ON C2.numCompanyID = D2.numCompanyID
         LEFT JOIN (SELECT TOP 1 OM.vcPoppName,
                                 OM.numOppID,
                                 numChildOppID,
                                 vcSource,
                                 numParentProjectID,
                                 vcProjectName
                    FROM   OpportunityLinking
                          LEFT JOIN OpportunityMaster OM
                             ON numOppID = numParentOppID
                          LEFT JOIN [ProjectsMaster]
							 ON numParentProjectID = numProId   
                    WHERE  numChildOppID = @numOppID) Link
           ON Link.numChildOppID = Opp.numOppID
         LEFT OUTER JOIN [Currency] C
					ON C.numCurrencyID = Opp.numCurrencyID
		 LEFT OUTER JOIN [OpportunityRecurring] OPR
					ON OPR.numOppId = Opp.numOppId
		 LEFT OUTER JOIN additionalContactsinformation ADC ON
		 ADC.numdivisionId = D2.numdivisionId AND  ADC.numContactId = Opp.numContactId		
  WHERE  Opp.numOppId = @numOppID
         AND Opp.numDomainID = @numDomainID

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_oppdetailsdtlpl')
DROP PROCEDURE usp_oppdetailsdtlpl
GO
CREATE PROCEDURE [dbo].[USP_OPPDetailsDTLPL]
(
               @OpportunityId        AS NUMERIC(9)  = NULL,
               @numDomainID          AS NUMERIC(9),
               @ClientTimeZoneOffset AS INT)
AS
  SELECT Opp.numoppid,numCampainID,
--         (SELECT vcdata
--          FROM   listdetails
--          WHERE  numlistitemid = numCampainID) AS numCampainIDName,
		 CAMP.vcCampaignName AS [numCampainIDName],
         dbo.Fn_getcontactname(Opp.numContactId) numContactIdName,
		 numContactId,
         Opp.txtComments,
         Opp.numDivisionID,
         tintOppType,
         Dateadd(MINUTE,-@ClientTimeZoneOffset,bintAccountClosingDate) AS bintAccountClosingDate,
         dbo.fn_GetOpportunitySourceValue(ISNULL(Opp.tintSource,0),ISNULL(Opp.tintSourceType,0),Opp.numDomainID) AS tintSourceName,
          tintSource,
         Opp.VcPoppName,
         intpEstimatedCloseDate,
         [dbo].[getdealamount](@OpportunityId,Getutcdate(),0) AS CalAmount,
         --monPAmount,
         CONVERT(DECIMAL(10,2),Isnull(monPAmount,0)) AS monPAmount,
		lngPConclAnalysis,
         (SELECT vcdata
          FROM   listdetails
          WHERE  numlistitemid = lngPConclAnalysis) AS lngPConclAnalysisName,
         (SELECT vcdata
          FROM   listdetails
          WHERE  numlistitemid = numSalesOrPurType) AS numSalesOrPurTypeName,
		  numSalesOrPurType,
		 Opp.vcOppRefOrderNo,
		 Opp.vcMarketplaceOrderID,
         C2.vcCompanyName,
         D2.tintCRMType,
         Opp.tintActive,
         dbo.Fn_getcontactname(Opp.numCreatedby)
           + ' '
           + CONVERT(VARCHAR(20),Dateadd(MINUTE,-@ClientTimeZoneOffset,Opp.bintCreatedDate)) AS CreatedBy,
         dbo.Fn_getcontactname(Opp.numModifiedBy)
           + ' '
           + CONVERT(VARCHAR(20),Dateadd(MINUTE,-@ClientTimeZoneOffset,Opp.bintModifiedDate)) AS ModifiedBy,
         dbo.Fn_getcontactname(Opp.numRecOwner) AS RecordOwner,
         Opp.numRecOwner,
         Isnull(tintshipped,0) AS tintshipped,
         Isnull(tintOppStatus,0) AS tintOppStatus,
         [dbo].[GetOppStatus](Opp.numOppId) AS vcDealStatus,
         dbo.Opportunitylinkeditems(@OpportunityId) +
         (SELECT COUNT(*) FROM  dbo.Communication Comm JOIN Correspondence Corr ON Comm.numCommId=Corr.numCommID
			WHERE Comm.numDomainId=Opp.numDomainId AND Corr.numDomainID=Comm.numDomainID AND Corr.tintCorrType IN (1,2,3,4) AND Corr.numOpenRecordID=Opp.numOppId) AS NoOfProjects,
         (SELECT A.vcFirstName
                   + ' '
                   + A.vcLastName
          FROM   AdditionalContactsInformation A
          WHERE  A.numcontactid = opp.numAssignedTo) AS numAssignedToName,
		  opp.numAssignedTo,
         Opp.numAssignedBy,
         (SELECT COUNT(* )
          FROM   GenericDocuments
          WHERE  numRecID = @OpportunityId
                 AND vcDocumentSection = 'O') AS DocumentCount,
         CASE 
           WHEN tintOppStatus = 1 THEN 100
           ELSE isnull((SELECT SUM(tintPercentage)
                 FROM   OpportunityStageDetails
                 WHERE  numOppId = @OpportunityId
                        AND bitStageCompleted = 1),0)
         END AS TProgress,
         (SELECT TOP 1 Isnull(varRecurringTemplateName,'')
          FROM   RecurringTemplate
          WHERE  numRecurringId = OPR.numRecurringId) AS numRecurringIdName,
		 OPR.numRecurringId,
         Link.numOppID AS numParentOppID,
         Link.vcPoppName AS ParentOppName,
         Link.vcSource AS Source,
         ISNULL(C.varCurrSymbol,'') varCurrSymbol,
          ISNULL(Opp.fltExchangeRate,0) AS [fltExchangeRate],
           ISNULL(C.chrCurrency,'') AS [chrCurrency],
         ISNULL(C.numCurrencyID,0) AS [numCurrencyID],
         (SELECT COUNT(*) FROM [OpportunityLinking] OL WHERE OL.[numParentOppID] = Opp.numOppId) AS ChildCount,
         (SELECT COUNT(*) FROM [RecurringTransactionReport] RTR WHERE RTR.[numRecTranSeedId] = Opp.numOppId)  AS RecurringChildCount,
         (SELECT COUNT(*) FROM [ShippingReport] WHERE numOppBizDocId IN (SELECT numOppBizDocsId FROM   OpportunityBizDocs BD WHERE  BD.numOppId = @OpportunityId))AS ShippingReportCount,
         ISNULL(Opp.numStatus ,0) numStatus,
         (SELECT vcdata
          FROM   listdetails
          WHERE  numlistitemid = Opp.numStatus) AS numStatusName,
          (SELECT SUBSTRING(
	(SELECT ','+  A.vcFirstName+' ' +A.vcLastName + CASE WHEN ISNULL(SR.numContactType,0)>0 THEN '(' + dbo.fn_GetListItemName(SR.numContactType) + ')' ELSE '' END
	FROM ShareRecord SR JOIN UserMaster UM ON UM.numUserDetailId=SR.numAssignedTo 
							 JOIN AdditionalContactsInformation A ON UM.numUserDetailId=A.numContactID 
		 WHERE SR.numDomainID=Opp.numDomainID AND SR.numModuleID=3 AND SR.numRecordID=Opp.numOppId
				AND UM.numDomainID=Opp.numDomainID and UM.numDomainID=A.numDomainID FOR XML PATH('')),2,200000)) AS numShareWith,
		ISNULL((SELECT SUM(ISNULL(monDealAmount,0)) FROM   OpportunityBizDocs BD WHERE  BD.numOppId = @OpportunityId AND ISNULL(bitAuthoritativeBizDocs,0)=1),0) AS monDealAmount,
		ISNULL((SELECT SUM(ISNULL(monAmountPaid,0)) FROM   OpportunityBizDocs BD WHERE  BD.numOppId = @OpportunityId AND ISNULL(bitAuthoritativeBizDocs,0)=1),0) AS monAmountPaid,
		ISNULL(Opp.tintSourceType,0) AS tintSourceType,
		ISNULL(Opp.tintTaxOperator,0) AS tintTaxOperator,  
             isnull(Opp.intBillingDays,0) AS numBillingDays,isnull(Opp.bitInterestType,0) AS bitInterestType,
             isnull(opp.fltInterest,0) AS fltInterest,  isnull(Opp.fltDiscount,0) AS fltDiscount,
             isnull(Opp.bitDiscountType,0) AS bitDiscountType,ISNULL(Opp.bitBillingTerms,0) AS bitBillingTerms,
             ISNULL(OPP.numDiscountAcntType,0) AS numDiscountAcntType,
              dbo.fn_getOPPAddress(Opp.numOppId,@numDomainID,1) AS BillingAddress,
            dbo.fn_getOPPAddress(Opp.numOppId,@numDomainID,2) AS ShippingAddress,
             ISNULL((SELECT SUBSTRING((SELECT '$^$' + ISNULL( (SELECT vcShipFieldValue FROM dbo.ShippingFieldValues WHERE numDomainID=Opp.numDomainID AND intShipFieldID IN ( SELECT intShipFieldID FROM dbo.ShippingFields WHERE numListItemID=OBD.numShipVia  AND vcFieldName='Tracking URL')),'') +'#^#'+ RTRIM(LTRIM(vcTrackingNo) )  FROM dbo.OpportunityBizDocs OBD WHERE OBD.numOppId=Opp.numOppID FOR XML PATH('')),4,200000)),'') AS  vcTrackingNo,
            ISNULL(numPercentageComplete,0) numPercentageComplete,
            dbo.GetListIemName(ISNULL(numPercentageComplete,0)) AS PercentageCompleteName,
            ISNULL(numBusinessProcessID,0) AS [numBusinessProcessID],
            CASE WHEN opp.tintOppType=1 THEN dbo.CheckOrderInventoryStatus(Opp.numOppID,Opp.numDomainID) ELSE '' END AS vcInventoryStatus
            ,ISNULL(bitUseShippersAccountNo,0) [bitUseShippersAccountNo], ISNULL(vcShippersAccountNo,'') [vcShippersAccountNo] 
  FROM   OpportunityMaster Opp
         JOIN divisionMaster D2 ON Opp.numDivisionID = D2.numDivisionID
         JOIN CompanyInfo C2 ON C2.numCompanyID = D2.numCompanyID
         LEFT JOIN (SELECT TOP 1 vcPoppName,
                                 numOppID,
                                 numChildOppID,
                                 vcSource,numParentOppID,OpportunityMaster.numDivisionID AS numParentDivisionID 
                    FROM   OpportunityLinking
                          left JOIN OpportunityMaster
                             ON numOppID = numParentOppID
                    WHERE  numChildOppID = @OpportunityId) Link
           ON Link.numChildOppID = Opp.numOppID
           LEFT OUTER JOIN [Currency] C
					ON C.numCurrencyID = Opp.numCurrencyID
			LEFT OUTER JOIN [OpportunityRecurring] OPR
					ON OPR.numOppId = Opp.numOppId
					 LEFT JOIN AddressDetails AD --Billing add
				ON AD.numDomainID=D2.numDomainID AND AD.numRecordID=D2.numDivisionID AND AD.tintAddressOf = 2 AND AD.tintAddressType = 1 AND AD.bitIsPrimary=1
			 LEFT JOIN AddressDetails AD2--Shipping address
				ON AD2.numDomainID=D2.numDomainID AND AD2.numRecordID=D2.numDivisionID AND AD2.tintAddressOf = 2 AND AD2.tintAddressType = 2 AND AD2.bitIsPrimary=1	
		LEFT JOIN dbo.CampaignMaster CAMP ON CAMP.numCampaignID = Opp.numCampainID 		
  WHERE  Opp.numOppId = @OpportunityId
         AND Opp.numDomainID = @numDomainID
/****** Object:  StoredProcedure [dbo].[USP_UserList]    Script Date: 07/26/2008 16:21:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--created by anoop jayaraj    
--Modified By Sachin Sadhu
--Worked on AND-OR condition       
GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='usp_userlist')
DROP PROCEDURE usp_userlist
GO
CREATE PROCEDURE [dbo].[USP_UserList]                  
 @UserName as varchar(100)='',                  
 @tintGroupType as tinyint,                  
 @SortChar char(1)='0',                  
 @CurrentPage int,                  
 @PageSize int,                  
 @TotRecs int output,                  
 @columnName as Varchar(50),                  
 @columnSortOrder as Varchar(10),            
 @numDomainID as numeric(9),          
 @Status as char(1)                  
                  
as                  
                     
Create table #tempTable ( ID INT IDENTITY PRIMARY KEY,                   
      numUserID VARCHAR(15),                  
      vcEmailID varchar(100),                  
      [Name] VARCHAR(100),                  
      vcGroupName varchar(100),                  
 vcDomainName varchar(50),          
 Active varchar(10)                  
 )                  
declare @strSql as varchar(8000)                  
set @strSql='SELECT numUserID, isnull(vcEmailID,''-'') as vcEmailID,                  
ISNULL(ADC.vcfirstname,'''') +'' ''+ ISNULL(ADC.vclastname,'''') as Name,                  
isnull(vcGroupName,''-'')as vcGroupName, vcDomainName, case when bitActivateFlag=1 then ''Active'' else ''Suspended'' end as bitActivateFlag                          
   FROM UserMaster                    
   join Domain                      
   on UserMaster.numDomainID =  Domain.numDomainID                     
    join AdditionalContactsInformation ADC                    
   on ADC.numContactid=UserMaster.numUserDetailId                   
   left join AuthenticationGroupMaster GM                   
   on Gm.numGroupID= UserMaster.numGroupID              
    where  UserMaster.numDomainID='+convert(varchar(15),@numDomainID)+ ' and bitActivateFlag=' +@Status               
                  
if @SortChar<>'0' set @strSql=@strSql + ' And (vcUserName like '''+@SortChar+'%'' OR vcEmailID LIKE '''+@SortChar+'%'')'

if @UserName<>''  set @strSql=@strSql + ' And (vcUserName like '''+@UserName+'%'' OR vcEmailID LIKE '''+@UserName+'%'')'

if @columnName<>'' set @strSql=@strSql + ' ORDER BY ' + @columnName +' '+ @columnSortOrder                  
   print @strSql               
insert into #tempTable(                  
 numUserID,                  
      vcEmailID,                  
      [Name],                  
      vcGroupName,                  
 vcDomainName,Active)                  
exec (@strSql)                   
                  
  declare @firstRec as integer                   
  declare @lastRec as integer                  
 set @firstRec= (@CurrentPage-1) * @PageSize                  
     set @lastRec= (@CurrentPage*@PageSize+1)                  
select * from #tempTable where ID > @firstRec and ID < @lastRec                  
set @TotRecs=(select count(*) from #tempTable)                  
drop table #tempTable            
          
          
select  vcCompanyName,ISNULL(intNoofUsersSubscribed,0) + ISNULL(intNoofPartialSubscribed,0)/2 + ISNULL(intNoofMinimalSubscribed,0)/10 AS intNoofUsersSubscribed,dtSubStartDate,dtSubEndDate,(select count(*) from UserMaster  join Domain                      
   on UserMaster.numDomainID =  Domain.numDomainID                     
    join AdditionalContactsInformation ADC                    
   on ADC.numContactid=UserMaster.numUserDetailId  where Domain.numDomainID=@numDomainID and bitActivateFlag=1) as ActiveSub          
from Subscribers S           
Join Domain D          
on D.numDomainID=S.numTargetDomainID          
join DivisionMaster Div          
on Div.numDivisionID=D.numDivisionID          
join CompanyInfo C          
on C.numCompanyID=Div.numCompanyID          
where D.numDomainID=@numDomainID
GO



--------------------------------------

GO
IF EXISTS(SELECT * FROM sysobjects WHERE xtype='p'AND NAME ='Usp_GetShippingDetailsForOrder')
DROP PROCEDURE Usp_GetShippingDetailsForOrder
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- EXEC Usp_GetShippingDetailsForOrder 95379,170
CREATE PROCEDURE Usp_GetShippingDetailsForOrder
(
	@numOppID		NUMERIC(18),
	@numDomainID	NUMERIC(18)
)
AS 
BEGIN
	SELECT TOP 1 OM.numOppId,OBD.numBizDocId,OBD.numOppBizDocsId, SR.numShippingReportId, SR.numShippingCompany, 
	STUFF ((SELECT ',' + ISNULL(vcTrackingNumber,'') FROM dbo.ShippingBox WHERE numShippingReportId = SR.numShippingReportId FOR XML PATH('')), 1, 1, '') [vcTrackingNumber],
--	CONVERT(VARCHAR(20),(SELECT COUNT(numBoxID) FROM dbo.ShippingBox WHERE numShippingReportId = SR.numShippingReportId)) + ' (' + 
--	(
--		STUFF ((SELECT ', ' + CONVERT(VARCHAR(20),ISNULL(fltLength,0)) + 'x' +  CONVERT(VARCHAR(20),ISNULL(fltWidth,0)) + 'x' + CONVERT(VARCHAR(20),ISNULL(fltHeight,0)) FROM dbo.ShippingBox WHERE numShippingReportId = SR.numShippingReportId FOR XML PATH('')), 1, 1, '')	
--	) + ')' AS [vcBoxDimensions],
	CONVERT(VARCHAR(20),(SELECT COUNT(numBoxID) FROM dbo.ShippingBox WHERE numShippingReportId = SR.numShippingReportId)) AS [numBoxCount],
	(SELECT TOP 1 numBoxID FROM dbo.ShippingBox WHERE numShippingReportId = SR.numShippingReportId) AS [numBoxID],
	CASE WHEN (SELECT COUNT(*) FROM dbo.ShippingReport WHERE dbo.ShippingReport.numOppBizDocId = OBD.numOppBizDocsId AND numDomainID = @numDomainID) > 0 THEN 1 
		 ELSE 0 
	END AS [IsShippingReportGenerated],
	CASE WHEN (SELECT COUNT(*) FROM dbo.ShippingBox 
			   WHERE numShippingReportId IN (SELECT numShippingReportId FROM dbo.ShippingReport WHERE numOppBizDocId = OBD.numOppBizDocsId AND numDomainID = @numDomainID)
			   AND LEN(ISNULL(vcTrackingNumber,'')) > 0) > 0 THEN 1 
		 ELSE 0 
	END AS [ISTrackingNumGenerated],
	LD.vcData as BizDoc,
	CASE WHEN tintOppType = 1  THEN (CASE WHEN ISNULL(numAuthoritativeSales,0) = numBizDocId THEN 'Authoritative' + CASE WHEN ISNULL(OBD.tintDeferred,0) = 1 THEN ' (Deferred)' 
																														 ELSE '' 
																													END 
									 ELSE 'Non-Authoritative' 
									 END ) 
		 WHEN tintOppType = 2  THEN (CASE WHEN ISNULL(numAuthoritativePurchase,0) = numBizDocId THEN 'Authoritative' + CASE WHEN ISNULL(OBD.tintDeferred,0) = 1 THEN ' (Deferred)' 
																															ELSE '' 
																													   END 
										  ELSE 'Non-Authoritative' 
									 END ) 
	END AS BizDocType,
	ISNULL(vcBizDocID,'') [vcBizDocID],
	(
		SELECT SUM(fltTotalWeight) FROM dbo.ShippingBox SBOX WHERE SBOX.numShippingReportId = SR.numShippingReportId
		GROUP BY SBOX.numShippingReportId
	) AS [BoxWeight],
	(
		STUFF ((SELECT '~' + ISNULL(dbo.ShippingBox.vcBoxName,'') + '- Weight: ' + CONVERT(VARCHAR(20),ISNULL(fltTotalWeight,0)) + ' lbs, LxBxH: ' + CONVERT(VARCHAR(20),ISNULL(fltLength,0)) + 'x' +  CONVERT(VARCHAR(20),ISNULL(fltWidth,0)) + 'x' + CONVERT(VARCHAR(20),ISNULL(fltHeight,0)) FROM dbo.ShippingBox WHERE numShippingReportId = SR.numShippingReportId FOR XML PATH('')), 1, 1, '') 
	) AS [WeightPerBox],
	STUFF ((SELECT ', ' + vcBizDocID + '~' + CONVERT(VARCHAR(10), numOppBizDocsId) FROM dbo.OpportunityBizDocs OppBiz WHERE OppBiz.numOppId = OM.numOppId  FOR XML PATH('')), 1, 1, '') [vcBizDocs]
	FROM dbo.OpportunityBizDocs OBD
	JOIN dbo.OpportunityMaster OM ON OBD.numOppId = OM.numOppId
	JOIN dbo.ShippingReport SR ON OBD.numOppBizDocsId = SR.numOppBizDocId AND SR.numOppID = OM.numOppId
	LEFT JOIN ListDetails LD on LD.numListItemID = OBD.numBizDocId 
	LEFT JOIN AuthoritativeBizDocs AB on AB.numDomainId = OM.numDomainID  
	WHERE OM.numDomainId = @numDomainID
	AND OM.numOppId = @numOppID
	ORDER BY SR.numShippingReportId DESC
END

