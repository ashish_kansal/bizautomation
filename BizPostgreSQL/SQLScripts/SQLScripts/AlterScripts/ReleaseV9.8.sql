/******************************************************************
Project: Release 9.8 Date: 06.JUN.2018
Comments: ALTER SCRIPTS
*******************************************************************/

/*************************** SANDEEP *******************************/

USE [Production.2014]
GO

/****** Object:  Table [dbo].[OrgOppAddressModificationHistory]    Script Date: 06-Jun-18 10:33:51 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[OrgOppAddressModificationHistory](
	[ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[tintAddressOf] [tinyint] NOT NULL,
	[tintAddressType] [tinyint] NOT NULL,
	[numRecordID] [numeric](18, 0) NOT NULL,
	[numAddressID] [numeric](18, 0) NULL,
	[numModifiedBy] [numeric](18, 0) NOT NULL,
	[dtModifiedDate] [datetime] NOT NULL,
 CONSTRAINT [PK_OrgOppAddressModificationHistory] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Values= 1:Organization 2:Opp/Order' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'OrgOppAddressModificationHistory', @level2type=N'COLUMN',@level2name=N'tintAddressOf'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Values = 1: Billing Address, 2: Shipping Address' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'OrgOppAddressModificationHistory', @level2type=N'COLUMN',@level2name=N'tintAddressType'
GO





DELETE ReportFieldsList WHERE numReportFieldGroupID=12 AND numFieldID=189

DELETE FROm ReportFieldGroupMappingMaster WHERE numReportFieldGroupID=12 AND numFieldID=189


UPDATE ReportFieldGroupMappingMaster SET vcFieldName = 'From' WHERE numReportFieldGroupID=12 AND vcFieldName='From Warehouse Location'	
UPDATE ReportFieldGroupMappingMaster SET vcFieldName = 'To' WHERE numReportFieldGroupID=12 AND vcFieldName='To Warehouse Location'	

ALTER TABLE MassStockTransfer ADD numToItemCode NUMERIC(18,0)
UPDATE MassStockTransfer SET numToItemCode=numItemCode


INSERT INTO ReportFieldGroupMappingMaster
(
	numReportFieldGroupID,numFieldID,vcFieldName,vcAssociatedControlType,vcFieldDataType,bitAllowSorting,bitAllowGrouping,bitAllowAggregate,bitAllowFiltering
)
VALUES
(
	7,500,'Discount Type','TextBox','V',0,0,0,0
),
(
	7,502,'Price Level Type','TextBox','V',0,0,0,0
)

INSERT INTO ReportFieldGroupMappingMaster
(
	numReportFieldGroupID,numFieldID,vcFieldName,vcAssociatedControlType,vcFieldDataType,bitAllowSorting,bitAllowGrouping,bitAllowAggregate,bitAllowFiltering
)
VALUES
(
	7,(SELECT numFieldId FROM DycFieldMaster WHERE vcDbColumnName='monPriceLevel1' AND vcLookBackTableName='PricingTable'),'Price Level 1','TextBox','M',0,0,0,0
),
(
	7,(SELECT numFieldId FROM DycFieldMaster WHERE vcDbColumnName='monPriceLevel2' AND vcLookBackTableName='PricingTable'),'Price Level 2','TextBox','M',0,0,0,0
),
(
	7,(SELECT numFieldId FROM DycFieldMaster WHERE vcDbColumnName='monPriceLevel3' AND vcLookBackTableName='PricingTable'),'Price Level 3','TextBox','M',0,0,0,0
),
(
	7,(SELECT numFieldId FROM DycFieldMaster WHERE vcDbColumnName='monPriceLevel4' AND vcLookBackTableName='PricingTable'),'Price Level 4','TextBox','M',0,0,0,0
),
(
	7,(SELECT numFieldId FROM DycFieldMaster WHERE vcDbColumnName='monPriceLevel5' AND vcLookBackTableName='PricingTable'),'Price Level 5','TextBox','M',0,0,0,0
),
(
	7,(SELECT numFieldId FROM DycFieldMaster WHERE vcDbColumnName='monPriceLevel6' AND vcLookBackTableName='PricingTable'),'Price Level 6','TextBox','M',0,0,0,0
),
(
	7,(SELECT numFieldId FROM DycFieldMaster WHERE vcDbColumnName='monPriceLevel7' AND vcLookBackTableName='PricingTable'),'Price Level 7','TextBox','M',0,0,0,0
),
(
	7,(SELECT numFieldId FROM DycFieldMaster WHERE vcDbColumnName='monPriceLevel8' AND vcLookBackTableName='PricingTable'),'Price Level 8','TextBox','M',0,0,0,0
),
(
	7,(SELECT numFieldId FROM DycFieldMaster WHERE vcDbColumnName='monPriceLevel9' AND vcLookBackTableName='PricingTable'),'Price Level 9','TextBox','M',0,0,0,0
),
(
	7,(SELECT numFieldId FROM DycFieldMaster WHERE vcDbColumnName='monPriceLevel10' AND vcLookBackTableName='PricingTable'),'Price Level 10','TextBox','M',0,0,0,0
),
(
	7,(SELECT numFieldId FROM DycFieldMaster WHERE vcDbColumnName='monPriceLevel11' AND vcLookBackTableName='PricingTable'),'Price Level 11','TextBox','M',0,0,0,0
),
(
	7,(SELECT numFieldId FROM DycFieldMaster WHERE vcDbColumnName='monPriceLevel12' AND vcLookBackTableName='PricingTable'),'Price Level 12','TextBox','M',0,0,0,0
),
(
	7,(SELECT numFieldId FROM DycFieldMaster WHERE vcDbColumnName='monPriceLevel13' AND vcLookBackTableName='PricingTable'),'Price Level 13','TextBox','M',0,0,0,0
),
(
	7,(SELECT numFieldId FROM DycFieldMaster WHERE vcDbColumnName='monPriceLevel14' AND vcLookBackTableName='PricingTable'),'Price Level 14','TextBox','M',0,0,0,0
),
(
	7,(SELECT numFieldId FROM DycFieldMaster WHERE vcDbColumnName='monPriceLevel15' AND vcLookBackTableName='PricingTable'),'Price Level 15','TextBox','M',0,0,0,0
),
(
	7,(SELECT numFieldId FROM DycFieldMaster WHERE vcDbColumnName='monPriceLevel16' AND vcLookBackTableName='PricingTable'),'Price Level 16','TextBox','M',0,0,0,0
),
(
	7,(SELECT numFieldId FROM DycFieldMaster WHERE vcDbColumnName='monPriceLevel17' AND vcLookBackTableName='PricingTable'),'Price Level 17','TextBox','M',0,0,0,0
),
(
	7,(SELECT numFieldId FROM DycFieldMaster WHERE vcDbColumnName='monPriceLevel18' AND vcLookBackTableName='PricingTable'),'Price Level 18','TextBox','M',0,0,0,0
),
(
	7,(SELECT numFieldId FROM DycFieldMaster WHERE vcDbColumnName='monPriceLevel19' AND vcLookBackTableName='PricingTable'),'Price Level 19','TextBox','M',0,0,0,0
),
(
	7,(SELECT numFieldId FROM DycFieldMaster WHERE vcDbColumnName='monPriceLevel20' AND vcLookBackTableName='PricingTable'),'Price Level 20','TextBox','M',0,0,0,0
)